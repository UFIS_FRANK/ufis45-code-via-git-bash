VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmData 
   Caption         =   "Form1"
   ClientHeight    =   11970
   ClientLeft      =   165
   ClientTop       =   480
   ClientWidth     =   12285
   LinkTopic       =   "Form1"
   ScaleHeight     =   11970
   ScaleWidth      =   12285
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin TABLib.TAB TabBSD 
      Height          =   690
      Left            =   12000
      TabIndex        =   81
      Tag             =   "{=TABLE=}BSDTAB{=FIELDS=}URNO,BSDC,BSDN"
      Top             =   6840
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabDEM 
      Height          =   1950
      Left            =   7140
      TabIndex        =   26
      Tag             =   "{=TABLE=}DEMTAB{=FIELDS=}URNO,DETY,OURI,OURO,URUD"
      Top             =   3720
      Width           =   5460
      _Version        =   65536
      _ExtentX        =   9631
      _ExtentY        =   3440
      _StockProps     =   64
   End
   Begin TABLib.TAB TabALO 
      Height          =   690
      Left            =   7140
      TabIndex        =   10
      Tag             =   "{=TABLE=}ALOTAB{=FIELDS=}URNO,ALOC,ALOD,REFT,ALOT"
      Top             =   2685
      Width           =   5460
      _Version        =   65536
      _ExtentX        =   9631
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabAFT 
      Height          =   1080
      Left            =   270
      TabIndex        =   8
      Tag             =   $"frmData.frx":0000
      Top             =   6240
      Width           =   9105
      _Version        =   65536
      _ExtentX        =   16060
      _ExtentY        =   1905
      _StockProps     =   64
   End
   Begin TABLib.TAB TabTLX 
      Height          =   690
      Left            =   7230
      TabIndex        =   43
      Tag             =   "{=TABLE=}TLXTAB{=FIELDS=}URNO,FLNU,TXT1,TXT2,TTYP,TIME"
      Top             =   9930
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabSPF 
      Height          =   690
      Left            =   7245
      TabIndex        =   41
      Tag             =   "{=TABLE=}SPFTAB{=FIELDS=}URNO,SURN,PRIO,VPFR,VPTO,CODE"
      Top             =   8865
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabDRR 
      Height          =   630
      Left            =   9675
      TabIndex        =   47
      Tag             =   "{=TABLE=}DRRTAB{=FIELDS=}URNO,SCOD,SCOO,AVFR,AVTO,ROSS,ROSL,REMA,SDAY,FCTC,OABS,BSDU"
      Top             =   7845
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   1111
      _StockProps     =   64
   End
   Begin TABLib.TAB TabConfig 
      Height          =   1800
      Left            =   7140
      TabIndex        =   2
      Top             =   435
      Width           =   5445
      _Version        =   65536
      _ExtentX        =   9604
      _ExtentY        =   3175
      _StockProps     =   64
   End
   Begin TABLib.TAB TabRPF 
      Height          =   690
      Left            =   4980
      TabIndex        =   31
      Tag             =   "{=TABLE=}RPFTAB{=FIELDS=}URNO,URUD,FCCO,UPFC"
      Top             =   7830
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabBLT 
      Height          =   690
      Left            =   270
      TabIndex        =   12
      Tag             =   "{=TABLE=}BLTTAB{=FIELDS=}URNO,BNAM"
      Top             =   7800
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabTemp 
      Height          =   630
      Left            =   12105
      TabIndex        =   30
      Top             =   7830
      Width           =   645
      _Version        =   65536
      _ExtentX        =   1138
      _ExtentY        =   1111
      _StockProps     =   64
   End
   Begin TABLib.TAB TabSOR 
      Height          =   690
      Left            =   270
      TabIndex        =   4
      Tag             =   "{=TABLE=}SORTAB{=FIELDS=}URNO,SURN,VPFR,VPTO,CODE"
      Top             =   1590
      Width           =   6705
      _Version        =   65536
      _ExtentX        =   11827
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabJOB 
      Height          =   1500
      Left            =   285
      TabIndex        =   6
      Tag             =   "{=TABLE=}JOBTAB{=FIELDS=}URNO,UAFT,USTF,ACFR,ACTO,STAT,UALO,UAID,DETY,UJTY,TEXT,UDSR,UTPL,JOUR,PLFR,PLTO,UEQU,UPRM,UTPL"
      Top             =   2670
      Width           =   6705
      _Version        =   65536
      _ExtentX        =   11827
      _ExtentY        =   2646
      _StockProps     =   64
   End
   Begin TABLib.TAB TabCIC 
      Height          =   690
      Left            =   270
      TabIndex        =   14
      Tag             =   "{=TABLE=}CICTAB{=FIELDS=}URNO,CNAM"
      Top             =   8820
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabEQU 
      Height          =   690
      Left            =   270
      TabIndex        =   16
      Tag             =   "{=TABLE=}EQUTAB{=FIELDS=}URNO,ENAM"
      Top             =   9885
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabGAT 
      Height          =   690
      Left            =   2625
      TabIndex        =   18
      Tag             =   "{=TABLE=}GATTAB{=FIELDS=}URNO,GNAM"
      Top             =   7830
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabPST 
      Height          =   690
      Left            =   2625
      TabIndex        =   20
      Tag             =   "{=TABLE=}PSTTAB{=FIELDS=}URNO,PNAM"
      Top             =   8835
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabWRO 
      Height          =   690
      Left            =   2625
      TabIndex        =   22
      Tag             =   "{=TABLE=}WROTAB{=FIELDS=}URNO,WNAM"
      Top             =   9900
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabEXT 
      Height          =   690
      Left            =   270
      TabIndex        =   24
      Tag             =   "{=TABLE=}EXTTAB{=FIELDS=}URNO,ENAM"
      Top             =   10935
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabJOD 
      Height          =   690
      Left            =   2625
      TabIndex        =   28
      Tag             =   "{=TABLE=}JODTAB{=FIELDS=}URNO,UJOB,UDEM"
      Top             =   10965
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabSGR 
      Height          =   690
      Left            =   4905
      TabIndex        =   33
      Tag             =   "{=TABLE=}SGRTAB{=FIELDS=}URNO,GRPN"
      Top             =   8820
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabRUD 
      Height          =   690
      Left            =   4890
      TabIndex        =   35
      Tag             =   "{=TABLE=}RUDTAB{=FIELDS=}URNO,UGHS,VREF"
      Top             =   9915
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabSER 
      Height          =   690
      Left            =   4890
      TabIndex        =   37
      Tag             =   "{=TABLE=}SERTAB{=FIELDS=}URNO,SECO,SNAM"
      Top             =   10995
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabJTY 
      Height          =   690
      Left            =   7215
      TabIndex        =   39
      Tag             =   "{=TABLE=}JTYTAB{=FIELDS=}URNO,NAME"
      Top             =   7830
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabCCC 
      Height          =   690
      Left            =   7215
      TabIndex        =   45
      Tag             =   "{=TABLE=}CCCTAB{=FIELDS=}URNO,CICC"
      Top             =   11025
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabODA 
      Height          =   630
      Left            =   9675
      TabIndex        =   49
      Tag             =   "{=TABLE=}ODATAB{=FIELDS=}URNO,SDAC,FREE,TYPE"
      Top             =   8865
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   1111
      _StockProps     =   64
   End
   Begin TABLib.TAB TabSTF 
      Height          =   690
      Left            =   270
      TabIndex        =   0
      Tag             =   "{=TABLE=}STFTAB{=FIELDS=}URNO,PENO,LANM,FINM,PERC,SHNM,TELH,TELP,REMA"
      Top             =   450
      Width           =   6705
      _Version        =   65536
      _ExtentX        =   11827
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabDRG 
      Height          =   630
      Left            =   9705
      TabIndex        =   51
      Tag             =   "{=TABLE=}DRGTAB{=FIELDS=}URNO,SDAY,WGPC,STFU"
      Top             =   10020
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   1111
      _StockProps     =   64
   End
   Begin TABLib.TAB TabSWG 
      Height          =   630
      Left            =   9720
      TabIndex        =   53
      Tag             =   "{=TABLE=}SWGTAB{=FIELDS=}URNO,VPFR,VPTO,CODE,SURN"
      Top             =   11070
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   1111
      _StockProps     =   64
   End
   Begin TABLib.TAB TabREM 
      Height          =   630
      Left            =   9660
      TabIndex        =   55
      Tag             =   "{=TABLE=}REMTAB{=FIELDS=}URNO,RURN,TEXT"
      Top             =   6840
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   1111
      _StockProps     =   64
   End
   Begin TABLib.TAB TabFlightInfo 
      Height          =   630
      Left            =   11790
      TabIndex        =   57
      Tag             =   "{=TABLE=}ODATAB{=FIELDS=}URNO,SDAC,FREE,TYPE"
      Top             =   8850
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   1111
      _StockProps     =   64
   End
   Begin TABLib.TAB tabTPL 
      Height          =   630
      Left            =   11835
      TabIndex        =   59
      Tag             =   "{=TABLE=}TPLTAB{=FIELDS=}URNO,TNAM"
      Top             =   11055
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   1111
      _StockProps     =   64
   End
   Begin TABLib.TAB TabDemTeamleader 
      Height          =   690
      Left            =   285
      TabIndex        =   61
      Tag             =   "{=TABLE=}DEMTAB{=FIELDS=}URNO,DETY,OURI,OURO,URUD,UTPL"
      Top             =   11940
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabRpfTeamleader 
      Height          =   690
      Left            =   2550
      TabIndex        =   63
      Tag             =   "{=TABLE=}RPFTAB{=FIELDS=}URNO,URUD,FCCO,UPFC"
      Top             =   11955
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabJodTeamleader 
      Height          =   690
      Left            =   4935
      TabIndex        =   65
      Tag             =   "{=TABLE=}JODTAB{=FIELDS=}URNO,UJOB,UDEM"
      Top             =   12015
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabJobTeamleader 
      Height          =   690
      Left            =   7320
      TabIndex        =   67
      Tag             =   "{=TABLE=}JOBTAB{=FIELDS=}URNO,UAFT,USTF,JOUR,UDSR"
      Top             =   11985
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabDlgTeamleader 
      Height          =   690
      Left            =   9645
      TabIndex        =   69
      Tag             =   "{=TABLE=}DLGTAB{=FIELDS=}URNO,UJOB,WGPC,FCTC"
      Top             =   12075
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabDrgTeamleader 
      Height          =   690
      Left            =   11940
      TabIndex        =   71
      Tag             =   "{=TABLE=}DRGTAB{=FIELDS=}URNO,STFU,SDAY,WGPC,FCTC"
      Top             =   12030
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabSwgTeamleader 
      Height          =   690
      Left            =   300
      TabIndex        =   73
      Tag             =   "{=TABLE=}SWGTAB{=FIELDS=}URNO,SURN,VPFR,VPTO,CODE"
      Top             =   12975
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabDrrTeamleader 
      Height          =   630
      Left            =   2655
      TabIndex        =   75
      Tag             =   "{=TABLE=}DRRTAB{=FIELDS=}URNO,SDAY"
      Top             =   13080
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   1111
      _StockProps     =   64
   End
   Begin TABLib.TAB TabDLG 
      Height          =   690
      Left            =   11925
      TabIndex        =   77
      Tag             =   "{=TABLE=}DLGTAB{=FIELDS=}URNO,UJOB,WGPC"
      Top             =   9975
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabDRA 
      Height          =   630
      Left            =   4935
      TabIndex        =   80
      Tag             =   "{=TABLE=}DRATAB{=FIELDS=}URNO,ABFR,ABTO,REMA,SDAC,SDAY,STFU"
      Top             =   13050
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   1111
      _StockProps     =   64
   End
   Begin TABLib.TAB TabEquipJOB 
      Height          =   1260
      Left            =   240
      TabIndex        =   83
      Tag             =   "{=TABLE=}JOBTAB{=FIELDS=}URNO,UAFT,USTF,ACFR,ACTO,STAT,UALO,UAID,DETY,UJTY,TEXT,UDSR,UTPL,JOUR,PLFR,PLTO,UEQU"
      Top             =   4560
      Width           =   6705
      _Version        =   65536
      _ExtentX        =   11827
      _ExtentY        =   2222
      _StockProps     =   64
   End
   Begin TABLib.TAB DPXTAB 
      Height          =   630
      Left            =   9600
      TabIndex        =   85
      Tag             =   "{=TABLE=}DPXTAB{=FIELDS=}URNO,NAME,SEAT,REMA"
      Top             =   13080
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   1111
      _StockProps     =   64
   End
   Begin VB.Label Label43 
      Caption         =   "DPXTAB"
      Height          =   210
      Left            =   9600
      TabIndex        =   86
      Top             =   12840
      Width           =   2235
   End
   Begin VB.Label Label42 
      Caption         =   "JOBTAB =>EQUIPMENTJOB"
      Height          =   210
      Left            =   240
      TabIndex        =   84
      Top             =   4320
      Width           =   2235
   End
   Begin VB.Label Label41 
      Caption         =   "BSDTAB:"
      Height          =   210
      Left            =   12000
      TabIndex        =   82
      Top             =   6600
      Width           =   2235
   End
   Begin VB.Label Label40 
      Caption         =   "DRATAB:"
      Height          =   210
      Left            =   4920
      TabIndex        =   79
      Top             =   12840
      Width           =   1815
   End
   Begin VB.Label Label39 
      Caption         =   "DLGTAB:"
      Height          =   210
      Left            =   11925
      TabIndex        =   78
      Top             =   9750
      Width           =   2235
   End
   Begin VB.Label Label38 
      Caption         =   "TabDrrTeamleader"
      Height          =   210
      Left            =   2640
      TabIndex        =   76
      Top             =   12780
      Width           =   2235
   End
   Begin VB.Label Label37 
      Caption         =   "TabSwgTeamleader"
      Height          =   210
      Left            =   285
      TabIndex        =   74
      Top             =   12765
      Width           =   2235
   End
   Begin VB.Label Label36 
      Caption         =   "TabDrgTeamleader"
      Height          =   210
      Left            =   11925
      TabIndex        =   72
      Top             =   11820
      Width           =   2235
   End
   Begin VB.Label Label35 
      Caption         =   "TabDlgTeamleader"
      Height          =   210
      Left            =   9645
      TabIndex        =   70
      Top             =   11850
      Width           =   2235
   End
   Begin VB.Label Label34 
      Caption         =   "TabJobTeamleader"
      Height          =   210
      Left            =   7305
      TabIndex        =   68
      Top             =   11775
      Width           =   2235
   End
   Begin VB.Label Label33 
      Caption         =   "TabJodTeamleader"
      Height          =   210
      Left            =   4920
      TabIndex        =   66
      Top             =   11805
      Width           =   2235
   End
   Begin VB.Label Label32 
      Caption         =   "TabRpfTeamleader"
      Height          =   210
      Left            =   2535
      TabIndex        =   64
      Top             =   11745
      Width           =   2235
   End
   Begin VB.Label Label31 
      Caption         =   "TabDemTeamleader"
      Height          =   210
      Left            =   270
      TabIndex        =   62
      Top             =   11730
      Width           =   2235
   End
   Begin VB.Label Label30 
      Caption         =   "TPLTAB:"
      Height          =   210
      Left            =   11865
      TabIndex        =   60
      Top             =   10845
      Width           =   1140
   End
   Begin VB.Label Label29 
      Caption         =   "tmpFlightInfo:"
      Height          =   210
      Left            =   11790
      TabIndex        =   58
      Top             =   8640
      Width           =   1140
   End
   Begin VB.Label Label28 
      Caption         =   "REMTAB:"
      Height          =   210
      Left            =   9645
      TabIndex        =   56
      Top             =   6600
      Width           =   2235
   End
   Begin VB.Label Label27 
      Caption         =   "SWGTAB:"
      Height          =   210
      Left            =   9750
      TabIndex        =   54
      Top             =   10860
      Width           =   1140
   End
   Begin VB.Label Label26 
      Caption         =   "DRGTAB:"
      Height          =   210
      Left            =   9720
      TabIndex        =   52
      Top             =   9810
      Width           =   1140
   End
   Begin VB.Label Label25 
      Caption         =   "ODATAB:"
      Height          =   210
      Left            =   9675
      TabIndex        =   50
      Top             =   8655
      Width           =   1140
   End
   Begin VB.Label Label24 
      Caption         =   "DRRTAB:"
      Height          =   210
      Left            =   9660
      TabIndex        =   48
      Top             =   7545
      Width           =   2235
   End
   Begin VB.Label Label23 
      Caption         =   "CCCTAB:"
      Height          =   210
      Left            =   7140
      TabIndex        =   46
      Top             =   10785
      Width           =   2235
   End
   Begin VB.Label Label22 
      Caption         =   "TLXTAB:"
      Height          =   210
      Left            =   7200
      TabIndex        =   44
      Top             =   9735
      Width           =   2235
   End
   Begin VB.Label Label21 
      Caption         =   "SPFTAB:"
      Height          =   210
      Left            =   7215
      TabIndex        =   42
      Top             =   8670
      Width           =   2235
   End
   Begin VB.Label Label20 
      Caption         =   "JTYTAB:"
      Height          =   210
      Left            =   7200
      TabIndex        =   40
      Top             =   7620
      Width           =   2235
   End
   Begin VB.Label Label19 
      Caption         =   "SERTAB:"
      Height          =   210
      Left            =   4875
      TabIndex        =   38
      Top             =   10785
      Width           =   2235
   End
   Begin VB.Label Label17 
      Caption         =   "RUDTAB:"
      Height          =   210
      Left            =   4875
      TabIndex        =   36
      Top             =   9705
      Width           =   2235
   End
   Begin VB.Label Label18 
      Caption         =   "SGRTAB:"
      Height          =   210
      Left            =   4890
      TabIndex        =   34
      Top             =   8625
      Width           =   2235
   End
   Begin VB.Label Label16 
      Caption         =   "RPFTAB:"
      Height          =   210
      Left            =   4965
      TabIndex        =   32
      Top             =   7620
      Width           =   2235
   End
   Begin VB.Label Label15 
      Caption         =   "JODTAB:"
      Height          =   210
      Left            =   2610
      TabIndex        =   29
      Top             =   10755
      Width           =   2235
   End
   Begin VB.Label Label14 
      Caption         =   "DEMTAB:"
      Height          =   210
      Left            =   7140
      TabIndex        =   27
      Top             =   3495
      Width           =   2235
   End
   Begin VB.Label Label13 
      Caption         =   "EXTTAB:"
      Height          =   210
      Left            =   270
      TabIndex        =   25
      Top             =   10695
      Width           =   2235
   End
   Begin VB.Label Label12 
      Caption         =   "WROTAB:"
      Height          =   210
      Left            =   2625
      TabIndex        =   23
      Top             =   9660
      Width           =   2235
   End
   Begin VB.Label Label11 
      Caption         =   "PSTTAB:"
      Height          =   210
      Left            =   2625
      TabIndex        =   21
      Top             =   8625
      Width           =   2235
   End
   Begin VB.Label Label10 
      Caption         =   "GATTAB:"
      Height          =   210
      Left            =   2625
      TabIndex        =   19
      Top             =   7635
      Width           =   2235
   End
   Begin VB.Label Label9 
      Caption         =   "EQUTAB:"
      Height          =   210
      Left            =   270
      TabIndex        =   17
      Top             =   9645
      Width           =   2235
   End
   Begin VB.Label Label8 
      Caption         =   "CICTAB:"
      Height          =   210
      Left            =   195
      TabIndex        =   15
      Top             =   8580
      Width           =   2235
   End
   Begin VB.Label Label7 
      Caption         =   "BLTTAB:"
      Height          =   210
      Left            =   270
      TabIndex        =   13
      Top             =   7560
      Width           =   2235
   End
   Begin VB.Label Label6 
      Caption         =   "ALOTAB:"
      Height          =   210
      Left            =   7170
      TabIndex        =   11
      Top             =   2445
      Width           =   2235
   End
   Begin VB.Label Label5 
      Caption         =   "AFTTAB:"
      Height          =   210
      Left            =   270
      TabIndex        =   9
      Top             =   5910
      Width           =   2235
   End
   Begin VB.Label Label3 
      Caption         =   "JOBTAB:"
      Height          =   210
      Left            =   270
      TabIndex        =   7
      Top             =   2430
      Width           =   2235
   End
   Begin VB.Label Label2 
      Caption         =   "SORTAB:"
      Height          =   210
      Left            =   270
      TabIndex        =   5
      Top             =   1305
      Width           =   2235
   End
   Begin VB.Label Label4 
      Caption         =   "Config-File:"
      Height          =   240
      Left            =   7110
      TabIndex        =   3
      Top             =   75
      Width           =   2580
   End
   Begin VB.Label Label1 
      Caption         =   "STFTAB:"
      Height          =   210
      Left            =   270
      TabIndex        =   1
      Top             =   165
      Width           =   2235
   End
End
Attribute VB_Name = "frmData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public strFieldSeparatorTabDatabase As String
Public strFieldSeparatorTabConfig As String
Public strServer As String
Public strHOPO As String
Public strTableExt As String
Public strAccessMethod As String
Public strConfigFile As String
Public strOrgUnit As String
Public strFunction As String
Public strStfUrno As String

Public strFLT As String
Public strGAT As String
Public strBRK As String
Public strSPE As String
Public strCCC As String
Public strCCI As String
Public strFID As String
' kkh added on 06/11/2006
Public strDFJ As String

Private strFrom As String
Private strTo As String

Private Sub Form_Load()
    Dim strTmp As String
    ' set the field separators
    strFieldSeparatorTabDatabase = ","
    strFieldSeparatorTabConfig = "="
    gbTeamLeaderGroup = False

    ' read entries from ceda.ini
    strAccessMethod = "CEDA"
    strHOPO = GetIniEntry("", gsAppName, "GLOBAL", "HOMEAIRPORT", "XXX")
    strServer = GetIniEntry("", gsAppName, "GLOBAL", "HOSTNAME", "XXX")
    strTableExt = GetIniEntry("", gsAppName, "GLOBAL", "TABLEEXTENSION", "TAB")
    strConfigFile = GetIniEntry("", gsAppName, "GLOBAL", "CONFIG_FILE", "XXX")

    ' read the config file
    If ExistFile(strConfigFile) = False Then
        MsgBox "The config-file for the application " & gsAppName & " does not exist!" & vbCrLf & _
                "Please check the setting in ceda.ini:" & vbCrLf & _
                "[" & gsAppName & "]" & vbCrLf & _
                "..." & vbCrLf & _
                "CONFIG_FILE=..." & vbCrLf & vbCrLf & _
                "The application terminates!", vbCritical, "No configuration file"
        mdlMain.ExitApplication
    Else
        ReadConfigFile
    End If
    
    ' get the UTC-difference
    sglUTCOffsetHours = GetUTCOffset
End Sub

Public Sub InitialLoad(bpHandleSplashScreen As Boolean, bpLoadBasicdata As Boolean)
    Dim strWhere As String
    Dim strToday As String
    Dim strCfg As String
    Dim dateFrom As Date
    Dim dateTo As Date
    Dim dateTo2 As Date
    Dim strTmp As String
    Dim ilIdx As Integer
    'igu on 03/07/2009
    Dim i As Integer
    Dim strDPX_UrNo As String
    Dim strDPX_UrNos As String

    If bpLoadBasicdata = True Then
        InitTabs
    End If

    ' loading TPLTAB data
    HandleSplashScreen "Loading TPLTAB (Templates)...", bpHandleSplashScreen
    strWhere = "WHERE TPST='1' AND APPL='RULE_AFT'"
    LoadData tabTPL, strWhere
    frmSelectTemplate.Show vbModal
    
    '-------------
    'Date: 25 May 2009
    'Desc: check for PAX T1, T2, PRM
    'Modi: igu
    '-------------
    If frmSelectTemplate.strTplUrno3 <> "" Then
        gsTplUrno = frmSelectTemplate.strTplUrno3
    Else
        gsTplUrno = frmSelectTemplate.strTplUrno
    End If
    '-------------
    
    '-------------
    'Date: 23 June 2009
    'Desc: get Urno for PRM Info Report
    'Modi: igu
    '-------------
    gsTplUrno3 = frmSelectTemplate.strTplUrno
    '-------------
    
    ' KKH - on 27/12/2006
    gsTplUrno2 = frmSelectTemplate.strTplUrno2

    ' init the splash screen
    If bpHandleSplashScreen = True Then
        frmSplash.Visible = True
        frmSplash.ProgressBar1.Max = 40
        frmSplash.List1.AddItem "Loading data..."
        frmSplash.Refresh
    Else
        frmMain.txtReloading.Text = " Please wait while reloading user data..."
    End If

    ' loading STFTAB data
    HandleSplashScreen "Loading STFTAB (employee data)...", bpHandleSplashScreen
    strWhere = "WHERE PENO = '" & gsUserName & "'"
    LoadData TabSTF, strWhere
    If TabSTF.GetLineCount < 1 Then
        MsgBox "The employee with the personnel number '" & gsUserName & "' does not exist!" & vbCrLf & _
                "The application terminates!", vbCritical, "Employee not found"
        mdlMain.ExitApplication
    End If
    strStfUrno = TabSTF.GetFieldValue(0, "URNO")

    ' loading SORTAB data
    HandleSplashScreen "Loading SORTAB (organizational assignments)...", bpHandleSplashScreen
    strWhere = "WHERE SURN = '" & strStfUrno & "'"
    LoadData TabSOR, strWhere
    strToday = Format(Now, "YYYYMMDD")
    strOrgUnit = GetBestFitRecord(TabSOR, strStfUrno, "SURN", "CODE", strToday)

    ' loading SPFTAB data
    HandleSplashScreen "Loading SPFTAB (connection between staff and function)...", bpHandleSplashScreen
    strWhere = "WHERE SURN = '" & strStfUrno & "' AND PRIO = '1'"
    LoadData TabSPF, strWhere
    strToday = Format(Now, "YYYYMMDD")
    strFunction = GetBestFitRecord(TabSPF, strStfUrno, "SURN", "CODE", strToday)

    ' loading JTYTAB data
    HandleSplashScreen "Loading JTYTAB (job types)...", bpHandleSplashScreen
    strWhere = ""
    If bpLoadBasicdata = True Then
        LoadData TabJTY, strWhere
        ilIdx = GetItemNo(TabJTY.LogicalFieldList, "NAME") - 1
        TabJTY.IndexCreate "NAME", ilIdx
        TabJTY.IndexCreate "URNO", 0
        InitJTY
    End If

    ' loading JOBTAB data
    HandleSplashScreen "Loading JOBTAB (employee jobs)...", bpHandleSplashScreen
    If (GetConfigEntry("JOB_LOAD_TIMEFRAME", strOrgUnit, strCfg) = False) Then
        MsgBoxNoCfgEntry "JOB_LOAD_TIMEFRAME"
    End If
    strFrom = GetItem(strCfg, 1, ",")
    strTo = GetItem(strCfg, 2, ",")
    dateFrom = DateAdd("h", CDbl(strFrom), Now) - (sglUTCOffsetHours / 24)
    dateTo = DateAdd("h", CDbl(strTo), Now) - (sglUTCOffsetHours / 24)
    If (GetConfigEntry("JOB_TYPE_UJTY", strOrgUnit, strCfg) = False) Then
        MsgBoxNoCfgEntry "JOB_TYPE_UJTY"
    End If
    dateTo2 = DateAdd("h", 8, dateTo)
    strWhere = "WHERE USTF = '" & strStfUrno & "' AND (ACTO BETWEEN '" & _
        Format(dateFrom, "YYYYMMDDhhmmss") & "' AND '" & Format(dateTo2, "YYYYMMDDhhmmss") & _
        "') AND (ACFR <= '" & Format(dateTo, "YYYYMMDDhhmmss") & "' AND " & _
        " ACFR >= '" & Format(dateFrom, "YYYYMMDDhhmmss") & "') AND UJTY IN (" & strCfg & ")"
    LoadData TabJOB, strWhere
    'TabJOB.IndexCreate "URNO", 0 'no index here - created after the filtering in "FilterJobs"

    ' loading JODTAB data
    HandleSplashScreen "Loading JODTAB (connection between jobs and demands)...", bpHandleSplashScreen
    strTmp = TabJOB.SelectDistinct(0, "'", "'", ",", True)
    If Len(strTmp) > 0 Then
        strWhere = "WHERE UJOB IN (" & strTmp & ")"
        LoadData TabJOD, strWhere
    End If
    ilIdx = GetItemNo(TabJOD.LogicalFieldList, "UJOB") - 1
    TabJOD.IndexCreate "UJOB", ilIdx

    ' loading DEMTAB data
    HandleSplashScreen "Loading DEMTAB (demands)...", bpHandleSplashScreen
    ilIdx = GetItemNo(TabJOD.LogicalFieldList, "UDEM") - 1
    strTmp = TabJOD.SelectDistinct(CStr(ilIdx), "'", "'", ",", True)
    If Len(strTmp) > 0 Then
        strWhere = "WHERE URNO IN (" & strTmp & ")"
        LoadData TabDEM, strWhere
    End If
    TabDEM.IndexCreate "URNO", 0

    ' loading AFTTAB data
    HandleSplashScreen "Loading AFTTAB (flights)...", bpHandleSplashScreen
    TabTemp.ResetContent
    ilIdx = GetItemNo(TabJOB.LogicalFieldList, "UAFT") - 1
    strTmp = TabJOB.SelectDistinct(CStr(ilIdx), "", "", ",", True)
    ilIdx = GetItemNo(TabDEM.LogicalFieldList, "OURI") - 1
    strTmp = strTmp & "," & TabDEM.SelectDistinct(CStr(ilIdx), "", "", ",", True)
    ilIdx = GetItemNo(TabDEM.LogicalFieldList, "OURO") - 1
    strTmp = strTmp & "," & TabDEM.SelectDistinct(CStr(ilIdx), "", "", ",", True)
    TabTemp.InsertBuffer strTmp, ","
    strTmp = TabTemp.SelectDistinct("0", "", "", ",", True)
    If Len(strTmp) > 0 Then
        While (InStr(strTmp, ",,") > 0)
            strTmp = Replace(strTmp, ",,", ",")
        Wend
        While (Left(strTmp, 1) = ",")
            strTmp = Right(strTmp, Len(strTmp) - 1)
        Wend
        While (Right(strTmp, 1) = ",")
            strTmp = Left(strTmp, Len(strTmp) - 1)
        Wend
        strWhere = "WHERE URNO IN (" & strTmp & ")"
        LoadData TabAFT, strWhere
        TabAFT.IndexCreate "URNO", 0
        TabAFT.IndexCreate "RKEY", 1
    End If

    ' loading REMTAB data
    If Len(strTmp) > 0 Then
        HandleSplashScreen "Loading REMTAB (remarks)...", bpHandleSplashScreen
        strWhere = "WHERE RURN IN (" & strTmp & ") AND APPL='OPSS-PM'"
        LoadData TabREM, strWhere
        ilIdx = GetItemNo(TabREM.LogicalFieldList, "RURN") - 1
        TabREM.IndexCreate "RURN", ilIdx
    End If

    ' loading ALOTAB data
    HandleSplashScreen "Loading ALOTAB (allocation types)...", bpHandleSplashScreen
    strWhere = "WHERE ALOT = '0'"
    If bpLoadBasicdata = True Then
        LoadData TabALO, strWhere
        TabALO.IndexCreate "URNO", 0
    End If

    ' loading BLTTAB data
    HandleSplashScreen "Loading BLTTAB (baggage belts)...", bpHandleSplashScreen
    strWhere = ""
    If bpLoadBasicdata = True Then
        LoadData TabBLT, strWhere
        TabBLT.IndexCreate "URNO", 0
    End If

    ' loading CICTAB data
    HandleSplashScreen "Loading CICTAB (checkin counters)...", bpHandleSplashScreen
    strWhere = ""
    If bpLoadBasicdata = True Then
        LoadData TabCIC, strWhere
        TabCIC.IndexCreate "URNO", 0
    End If

    ' loading CCCTAB data
    HandleSplashScreen "Loading CCCTAB (common checkin counters)...", bpHandleSplashScreen
    strWhere = ""
    If bpLoadBasicdata = True Then
        LoadData TabCCC, strWhere
        TabCCC.IndexCreate "URNO", 0
    End If

    ' loading EQUTAB data
    HandleSplashScreen "Loading EQUTAB (equipments)...", bpHandleSplashScreen
    strWhere = ""
    If bpLoadBasicdata = True Then
        LoadData TabEQU, strWhere
        TabEQU.IndexCreate "URNO", 0
    End If

    ' loading EXTTAB data
    HandleSplashScreen "Loading EXTTAB (exits)...", bpHandleSplashScreen
    strWhere = ""
    If bpLoadBasicdata = True Then
        LoadData TabEXT, strWhere
        TabEXT.IndexCreate "URNO", 0
    End If

    ' loading GATTAB data
    HandleSplashScreen "Loading GATTAB (gates)...", bpHandleSplashScreen
    strWhere = ""
    If bpLoadBasicdata = True Then
        LoadData TabGAT, strWhere
        TabGAT.IndexCreate "URNO", 0
    End If

    ' loading PSTTAB data
    HandleSplashScreen "Loading PSTTAB (parking stands)...", bpHandleSplashScreen
    strWhere = ""
    If bpLoadBasicdata = True Then
        LoadData TabPST, strWhere
        TabPST.IndexCreate "URNO", 0
    End If

    ' loading WROTAB data
    HandleSplashScreen "Loading WROTAB (waiting rooms)...", bpHandleSplashScreen
    strWhere = ""
    If bpLoadBasicdata = True Then
        LoadData TabWRO, strWhere
        TabWRO.IndexCreate "URNO", 0
    End If

    ' loading RPFTAB data
    HandleSplashScreen "Loading RPFTAB (connection between demands and functions)...", bpHandleSplashScreen
    ilIdx = GetItemNo(TabDEM.LogicalFieldList, "URUD") - 1
    strTmp = TabDEM.SelectDistinct(CStr(ilIdx), "'", "'", ",", True)
    strWhere = ""
    If Len(strTmp) > 0 Then
        strWhere = "WHERE URUD IN (" & strTmp & ")"
        LoadData TabRPF, strWhere
    End If
    ilIdx = GetItemNo(TabRPF.LogicalFieldList, "URUD") - 1
    TabRPF.IndexCreate "URUD", ilIdx

    ' loading RUDTAB data
    HandleSplashScreen "Loading RUDTAB (Rule demands)...", bpHandleSplashScreen
    If Len(strTmp) > 0 Then
        strWhere = "WHERE URNO IN (" & strTmp & ")"
        LoadData TabRUD, strWhere
    End If
    TabRUD.IndexCreate "URNO", 0

    ' loading SERTAB data
    HandleSplashScreen "Loading   (Ground handling services)...", bpHandleSplashScreen
    ilIdx = GetItemNo(TabRUD.LogicalFieldList, "UGHS") - 1
    strTmp = TabRUD.SelectDistinct(CStr(ilIdx), "'", "'", ",", True)
    If Len(strTmp) > 0 Then
        strWhere = "WHERE URNO IN (" & strTmp & ")"
        LoadData TabSER, strWhere
    End If
    TabSER.IndexCreate "URNO", 0

    ' loading SGRTAB data
    HandleSplashScreen "Loading SGRTAB (static groups)...", bpHandleSplashScreen
    strWhere = "WHERE TABN = 'PFC'"
    If bpLoadBasicdata = True Then
        LoadData TabSGR, strWhere
        TabSGR.IndexCreate "URNO", 0
    End If

    ' loading DRRTAB data
    HandleSplashScreen "Loading DRRTAB (daily roster records)...", bpHandleSplashScreen
    If (GetConfigEntry("SHIFT_TIMEFRAME", strOrgUnit, strCfg) = False) Then
        MsgBoxNoCfgEntry "SHIFT_TIMEFRAME"
    End If
    strFrom = GetItem(strCfg, 1, ",")
    strTo = GetItem(strCfg, 2, ",")
    dateFrom = DateAdd("d", CDbl(strFrom), Now) ' DRRTAB.SDAY is local
    dateTo = DateAdd("d", CDbl(strTo), Now)
    strFrom = Format(dateFrom, "YYYYMMDD")
    strTo = Format(dateTo, "YYYYMMDD")
    strWhere = "WHERE STFU = '" & strStfUrno & "' AND SDAY between '" & strFrom & "' AND '" & strTo & "' ORDER BY SDAY,ROSL"
    LoadData TabDRR, strWhere
    TabDRR.IndexCreate "URNO", 0

'    ' loading DLGTAB data
'    HandleSplashScreen "Loading DLGTAB (Delegations)...", bpHandleSplashScreen
'    ilIdx = GetItemNo(TabJOB.LogicalFieldList, "JOUR") - 1
'    strTmp = TabJOB.SelectDistinct(ilIdx, "'", "'", ",", True)
'    If Len(strTmp) > 0 Then
'        strWhere = "WHERE UJOB IN (" & strTmp & ")"
'        LoadData TabDLG, strWhere
'    End If
'    ilIdx = GetItemNo(TabDlgTeamleader.LogicalFieldList, "UJOB") - 1
'    TabDlgTeamleader.IndexCreate "UJOB", ilIdx
'
'
'    strWhere = "WHERE STFU = '" & strStfUrno & "' AND SDAY between '" & strFrom & "' AND '" & strTo & "'"
'    LoadData TabDLG, strWhere 'same where-statement as for DRRTAB
'    ilIdx = GetItemNo(TabDRG.LogicalFieldList, "SDAY") - 1
'    TabDRG.IndexCreate "SDAY", ilIdx

    ' loading DRGTAB data
    HandleSplashScreen "Loading DRGTAB (Working group assignments daily)...", bpHandleSplashScreen
    strWhere = "WHERE STFU = '" & strStfUrno & "' AND SDAY between '" & strFrom & "' AND '" & strTo & "'"
    LoadData TabDRG, strWhere 'same where-statement as for DRRTAB
    ilIdx = GetItemNo(TabDRG.LogicalFieldList, "SDAY") - 1
    TabDRG.IndexCreate "SDAY", ilIdx

    ' loading SWGTAB data
    HandleSplashScreen "Loading SWGTAB (Working group assignments)...", bpHandleSplashScreen
    strWhere = "WHERE SURN = '" & strStfUrno & "'"
    LoadData TabSWG, strWhere
    ilIdx = GetItemNo(TabSWG.LogicalFieldList, "VPFR") - 1
    TabSWG.Sort CStr(ilIdx), True, True

    ' loading ODATAB data
    HandleSplashScreen "Loading ODATAB (absences)...", bpHandleSplashScreen
    strWhere = ""
    If bpLoadBasicdata = True Then
        LoadData TabODA, strWhere
        ilIdx = GetItemNo(TabODA.LogicalFieldList, "SDAC") - 1
        TabODA.IndexCreate "SDAC", ilIdx
    End If
    
    If (frmData.GetConfigEntry("VISIBLE_COLUMNS", frmData.strOrgUnit, strTmp) = True) Then
        If InStr(1, strTmp, "TeamLeader") > 0 Then
            gbTeamLeaderGroup = True
            InitialLoadTeamleader
        End If
    End If

    ' loading DRATAB data
    HandleSplashScreen "Loading DRATAB (Daily roster absences)...", bpHandleSplashScreen
    strWhere = "WHERE STFU = '" & strStfUrno & "' AND SDAY between '" & strFrom & "' AND '" & strTo & "'"
    LoadData TabDRA, strWhere
    ilIdx = GetItemNo(TabDRA.LogicalFieldList, "SDAY") - 1
    TabDRA.IndexCreate "SDAY", ilIdx
    ilIdx = GetItemNo(TabDRA.LogicalFieldList, "URNO") - 1
    TabDRA.IndexCreate "URNO", ilIdx
    ilIdx = GetItemNo(TabDRA.LogicalFieldList, "ABFR") - 1
    TabDRA.Sort CStr(ilIdx), True, True
    
    ' KKH on 10/04/2007 display BSDC, BSDN
    ' loading BSDTAB data
    HandleSplashScreen "Loading BSDTAB (Basic Shift Data)...", bpHandleSplashScreen
    strWhere = ""
    If bpLoadBasicdata = True Then
        LoadData TabBSD, strWhere
        TabBSD.IndexCreate "URNO", 0
    End If
    
    'igu on 03/07/2009
    'loading DPXTAB data
    HandleSplashScreen "Loading DPXTAB (passengers)...", bpHandleSplashScreen
    strDPX_UrNos = ""
    For i = 0 To frmData.TabJOB.GetLineCount - 1
        strDPX_UrNo = TabJOB.GetFieldValue(i, "UPRM")
        If Trim(strDPX_UrNo) <> "" And Trim(strDPX_UrNo) <> "0" Then
            strDPX_UrNos = strDPX_UrNos & strDPX_UrNo & ","
        End If
    Next i
    If Trim(strDPX_UrNos) <> "" Then
        strDPX_UrNos = Left(strDPX_UrNos, Len(strDPX_UrNos) - 1)
        strWhere = "WHERE URNO IN (" & strDPX_UrNos & ")"
        LoadData DPXTAB, strWhere
        ilIdx = GetItemNo(DPXTAB.LogicalFieldList, "URNO") - 1
        TabODA.IndexCreate "URNO", ilIdx
    End If
    
    ' that we see all columns
    ResizeTabs

    frmSplash.Visible = False
    frmSplash.Hide
End Sub

Private Sub InitTabs()
    InitTabGeneral TabSTF
    InitTabForCedaConnection TabSTF
    InitTabGeneral TabSOR
    InitTabForCedaConnection TabSOR
    InitTabGeneral TabSPF
    InitTabForCedaConnection TabSPF
    InitTabGeneral TabJOB
    InitTabForCedaConnection TabJTY
    InitTabGeneral TabJTY
    InitTabForCedaConnection TabJOB
    InitTabGeneral TabJOD
    InitTabForCedaConnection TabJOD
    InitTabGeneral TabDEM
    InitTabForCedaConnection TabDEM
    InitTabGeneral TabAFT
    InitTabForCedaConnection TabAFT
    InitTabGeneral TabREM
    InitTabForCedaConnection TabREM
    InitTabGeneral TabALO
    InitTabForCedaConnection TabALO
    InitTabGeneral TabBLT
    InitTabForCedaConnection TabBLT
    InitTabGeneral TabCIC
    InitTabForCedaConnection TabCIC
    InitTabGeneral TabCCC
    InitTabForCedaConnection TabCCC
    InitTabGeneral TabEQU
    InitTabForCedaConnection TabEQU
    InitTabGeneral TabEXT
    InitTabForCedaConnection TabEXT
    InitTabGeneral TabGAT
    InitTabForCedaConnection TabGAT
    InitTabGeneral TabPST
    InitTabForCedaConnection TabPST
    InitTabGeneral TabWRO
    InitTabForCedaConnection TabWRO
    InitTabGeneral TabRPF
    InitTabForCedaConnection TabRPF
    InitTabGeneral TabRUD
    InitTabForCedaConnection TabRUD
    InitTabGeneral TabSER
    InitTabForCedaConnection TabSER
    InitTabGeneral TabSGR
    InitTabForCedaConnection TabSGR
    InitTabGeneral TabDRR
    InitTabForCedaConnection TabDRR
    InitTabGeneral TabDRG
    InitTabForCedaConnection TabDRG
    InitTabGeneral TabSWG
    InitTabForCedaConnection TabSWG
    InitTabGeneral TabODA
    InitTabForCedaConnection TabODA
    InitTabGeneral tabTPL
    InitTabForCedaConnection tabTPL
    InitTabGeneral TabTLX   'no loading at startup
    InitTabForCedaConnection TabTLX
    InitTabGeneral TabDRA
    InitTabForCedaConnection TabDRA
    ' KKH on 10/04/2007 display BSDC, BSDN
    InitTabGeneral TabBSD
    InitTabForCedaConnection TabBSD
    '-------------
    'Date: 03 July 2009
    'Desc: Init TabDPX
    'Modi: igu
    '-------------
    InitTabGeneral DPXTAB
    InitTabForCedaConnection DPXTAB
End Sub

Private Sub ResizeTabs()
    TabSTF.AutoSizeColumns
    TabSOR.AutoSizeColumns
    TabSPF.AutoSizeColumns
    TabJTY.AutoSizeColumns
    TabJOB.AutoSizeColumns
    TabJOD.AutoSizeColumns
    TabDEM.AutoSizeColumns
    TabAFT.AutoSizeColumns
    TabREM.AutoSizeColumns
    TabALO.AutoSizeColumns
    TabBLT.AutoSizeColumns
    TabCIC.AutoSizeColumns
    TabCCC.AutoSizeColumns
    TabEQU.AutoSizeColumns
    TabEXT.AutoSizeColumns
    TabGAT.AutoSizeColumns
    TabPST.AutoSizeColumns
    TabWRO.AutoSizeColumns
    TabRPF.AutoSizeColumns
    TabRUD.AutoSizeColumns
    TabSER.AutoSizeColumns
    TabSGR.AutoSizeColumns
    TabDRR.AutoSizeColumns
    TabDRG.AutoSizeColumns
    TabSWG.AutoSizeColumns
    TabODA.AutoSizeColumns
    tabTPL.AutoSizeColumns
    TabDRA.AutoSizeColumns
    ' KKH on 10/04/2007 display BSDC, BSDN
    TabBSD.AutoSizeColumns
    TabDemTeamleader.AutoSizeColumns
    TabRpfTeamleader.AutoSizeColumns
    TabJodTeamleader.AutoSizeColumns
    TabJobTeamleader.AutoSizeColumns
    TabDlgTeamleader.AutoSizeColumns
    TabDrgTeamleader.AutoSizeColumns
    TabSwgTeamleader.AutoSizeColumns
    TabDrrTeamleader.AutoSizeColumns
    '-------------
    'Date: 23 June 2009
    'Desc: Autosize TabDPX
    'Modi: igu
    '-------------
    DPXTAB.AutoSizeColumns
End Sub

Private Sub HandleSplashScreen(sMessage As String, bpHandleSplashScreen As Boolean)
    If bpHandleSplashScreen = True Then
        frmSplash.List1.AddItem (sMessage)
        frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1
        frmSplash.List1.TopIndex = frmSplash.List1.ListCount - 1
        frmSplash.List1.Refresh
    Else
        frmMain.txtReloading.Text = frmMain.txtReloading.Text & "."
        frmMain.txtReloading.Refresh
    End If
End Sub

Public Sub MsgBoxNoCfgEntry(sParameter As String)
    MsgBox "The parameter '" & sParameter & "' does not exist in the config file!" & vbCrLf & _
            "The application terminates!", vbCritical, "Parameter not found"
    mdlMain.ExitApplication
End Sub

Private Sub InitTabGeneral(ByRef rTab As TABLib.Tab)
    rTab.ResetContent
    rTab.FontName = "Courier New"
    rTab.HeaderFontSize = "18"
    rTab.FontSize = "16"
    rTab.EnableHeaderSizing True
    rTab.ShowHorzScroller True
    rTab.AutoSizeByHeader = True
    rTab.SetFieldSeparator strFieldSeparatorTabDatabase

    Dim ilCnt As Integer
    Dim i As Integer
    Dim strFields As String
    Dim strHeaderLengthString As String
    GetKeyItem strFields, rTab.Tag, "{=FIELDS=}", "{="
    ilCnt = ItemCount(strFields, ",")
    For i = 1 To ilCnt Step 1
        strHeaderLengthString = strHeaderLengthString & "10,"
    Next i
    strHeaderLengthString = Left(strHeaderLengthString, Len(strHeaderLengthString) - 1)
    rTab.HeaderLengthString = strHeaderLengthString

    strFields = Replace(strFields, ",", strFieldSeparatorTabDatabase)
    rTab.HeaderString = strFields
    rTab.LogicalFieldList = strFields
End Sub

Private Sub InitTabForCedaConnection(ByRef rTab As TABLib.Tab)
    'do the initializing for the CEDA-connection
    rTab.CedaServerName = strServer
    rTab.CedaPort = "3357"
    rTab.CedaHopo = strHOPO
    rTab.CedaCurrentApplication = gsAppName
    rTab.CedaTabext = strTableExt
    rTab.CedaUser = gsUserName
    rTab.CedaWorkstation = frmSplash.AATLoginControl1.GetWorkStationName
    rTab.CedaSendTimeout = "120"
    rTab.CedaReceiveTimeout = "240"
    rTab.CedaRecordSeparator = Chr(10)
    rTab.CedaIdentifier = gsAppName
End Sub

Public Sub LoadData(ByRef rTab As TABLib.Tab, ByRef pWhere As String, Optional bReset As Boolean = True)
    Dim strCommand As String
    Dim strTable As String
    Dim strFieldList As String
    Dim strData As String
    Dim strWhere As String

    strCommand = "RT"
    If (bReset = True) Then
        rTab.ResetContent
    End If
    GetKeyItem strTable, rTab.Tag, "{=TABLE=}", "{="
    GetKeyItem strFieldList, rTab.Tag, "{=FIELDS=}", "{="
    strData = ""
    strWhere = pWhere
    If Len(strWhere) = 0 Then
        GetKeyItem strWhere, rTab.Tag, "{=WHERE=}", "{="
    End If
    If rTab.CedaAction(strCommand, strTable, strFieldList, strData, strWhere) = False Then
        MsgBox "Error loading " & strTable & "!" & vbCrLf & _
            "Field list: " & strFieldList & vbCrLf & _
            "Where statement: " & strWhere, vbCritical, "Error reading data (RT)"
            rTab.GetLastCedaError
    End If
End Sub

Private Sub ConnectUfisCom()
    frmSplash.UfisCom1.SetCedaPerameters frmSplash.AATLoginControl1.GetUserName, strHOPO, strTableExt
    frmSplash.UfisCom1.InitCom strServer, strAccessMethod
End Sub

Private Sub DisconnetUfisCom()
    frmSplash.UfisCom1.CleanupCom
End Sub

Private Function GetUTCOffset() As Single
    Dim strUtcArr() As String
    Dim strArr() As String
    Dim CurrStr As String
    Dim tmpStr As String

    Dim istrRet As Integer
    Dim count As Integer
    Dim i As Integer

    GetUTCOffset = 0

    ConnectUfisCom

    If frmSplash.UfisCom1.CallServer("GFR", "", "", "", "[CONFIG]", "360") = 0 Then
        tmpStr = frmSplash.UfisCom1.GetDataBuffer(True)
    End If
    strArr = Split(tmpStr, Chr(10))
    count = UBound(strArr)
    For i = 0 To count
        CurrStr = strArr(i)
        istrRet = InStr(1, CurrStr, "UTCD", 0)
        If istrRet <> 0 Then
            strUtcArr = Split(strArr(i), ",")
            GetUTCOffset = CSng(strUtcArr(1) / 60)
            Exit For
        End If
    Next i
    DisconnetUfisCom
End Function

Private Sub ReadConfigFile()
    TabConfig.ResetContent
    TabConfig.HeaderLengthString = "100,1000"
    TabConfig.EnableHeaderSizing True
    TabConfig.EnableInlineEdit True
    TabConfig.ShowHorzScroller True
    TabConfig.SetFieldSeparator strFieldSeparatorTabConfig
    TabConfig.HeaderString = "Parameter" & strFieldSeparatorTabConfig & "Setting"
    TabConfig.ReadFromFile strConfigFile
End Sub

Public Function GetConfigEntry(ByRef sParameter As String, ByRef sOrgCode As String, ByRef sReturn) As Boolean
    Dim blRet As Boolean
    Dim strDefault As String
    Dim strFoundOrgCode As String
    Dim strValue As String
    Dim strLines As String
    Dim ilCnt As Integer
    Dim i As Integer

    blRet = False
    sReturn = ""

    strLines = TabConfig.GetLinesByColumnValue(0, sParameter, 0)
    If Len(strLines) > 0 Then
        ilCnt = ItemCount(strLines, ",")
        For i = 1 To ilCnt Step 1
            strValue = TabConfig.GetColumnValue(CLng(GetItem(strLines, i, ",")), 1)
            strFoundOrgCode = GetItem(strValue, 1, ";")

            If strFoundOrgCode = sOrgCode Then
                ' we found the value for the OrgUnit of the logged in employee
                sReturn = GetItem(strValue, 2, ";")
                GetConfigEntry = True
                Exit Function
            ElseIf strFoundOrgCode = "DEFAULT" Then
                ' we found the default setting for that parameter
                strDefault = GetItem(strValue, 2, ";")
            End If
        Next i
        sReturn = strDefault
        blRet = True
    End If

    GetConfigEntry = blRet
End Function

Private Function GetBestFitRecord(rTab As TABLib.Tab, sSearchValue As String, sSearchColumn As String, sReturnColumn As String, sReferenceDate As String) As String
    GetBestFitRecord = ""
    Dim llBestFit As Long
    Dim llTmp As Long
    Dim llTo As Long
    Dim llReferenceDate As Long
    Dim i As Integer
    Dim ilCnt As Integer
    Dim llLineNo As Long

    Dim strTmp As String
    Dim strLineNo As String
    Dim ilIdxSearchColumn As String
    Dim ilIdxVPFR As String
    Dim ilIdxVPTO As String

    ' look if columns do exist and remember their position
    ilIdxSearchColumn = GetItemNo(rTab.LogicalFieldList, sSearchColumn) - 1
    ilIdxVPFR = GetItemNo(rTab.LogicalFieldList, "VPFR") - 1
    ilIdxVPTO = GetItemNo(rTab.LogicalFieldList, "VPTO") - 1

    If (ilIdxSearchColumn > -1 And ilIdxVPFR > -1 And ilIdxVPTO > -1 And IsNumeric(sReferenceDate) = True) Then
        llReferenceDate = CLng(sReferenceDate)
        strLineNo = rTab.GetLinesByColumnValue(ilIdxSearchColumn, sSearchValue, 0)
        If Len(strLineNo) > 0 Then
            ilCnt = ItemCount(strLineNo, ",")
            llBestFit = 0

            For i = 1 To ilCnt Step 1
                llLineNo = CLng(GetItem(strLineNo, i, ","))
                strTmp = rTab.GetFieldValue(llLineNo, "VPFR")
                If Len(strTmp) > 8 Then
                    llTmp = CLng(Left(strTmp, 8))
                    If llTmp <= llReferenceDate Then
                        strTmp = rTab.GetFieldValue(llLineNo, "VPTO")
                        If Len(strTmp) > 7 Then
                            llTo = CLng(Left(strTmp, 8))
                        Else
                            llTo = 99999999
                        End If

                        If llTmp > llBestFit And llTo >= llReferenceDate Then
                            llBestFit = llTmp
                            GetBestFitRecord = rTab.GetFieldValue(llLineNo, sReturnColumn)
                        End If
                    End If
                End If
            Next i
        End If
    End If
End Function

Public Function GetJobTimes(sJobUrno As String) As String
    Dim strLineNo  As String
    Dim strRet As String
    Dim strTmp As String
    Dim dateTmp As Date

    strLineNo = TabJOB.GetLinesByIndexValue("UJOB", sJobUrno, 0)
    If IsNumeric(strLineNo) = True Then
        'SHOW_TIME_OF_JOB=DEFAULT;FLT,GAT,CCI,BRK,SPE,CCC,FID)
        Dim strUJTY As String
        Dim strItem As String
        Dim strLineNoJty As String

        strUJTY = TabJOB.GetFieldValue(CLng(strLineNo), "UJTY")
        strLineNoJty = TabJTY.GetLinesByIndexValue("URNO", strUJTY, 0)

        If IsNumeric(strLineNoJty) = True Then
            strItem = "," & TabJTY.GetFieldValue(CLng(strLineNoJty), "NAME") & ","
            If InStr(1, frmMain.strSHOW_TIME_OF_JOB, strItem, vbTextCompare) > 0 Then
                strTmp = TabJOB.GetFieldValue(CLng(strLineNo), "ACFR")
                If Len(strTmp) > 0 Then
                    dateTmp = CedaFullDateToVb(strTmp) + (sglUTCOffsetHours / 24)
                    'dateTmp = DateAdd("h", sglUTCOffsetHours, CedaFullDateToVb(strTmp))
                    strRet = Format(dateTmp, "hh:mm") & "/" & Format(dateTmp, "DD") & "-"
                Else
                     strRet = strRet & "n.a.-"
                End If
                strTmp = TabJOB.GetFieldValue(CLng(strLineNo), "ACTO")
                If Len(strTmp) > 0 Then
                    dateTmp = CedaFullDateToVb(strTmp) + (sglUTCOffsetHours / 24)
                    'dateTmp = DateAdd("h", sglUTCOffsetHours, CedaFullDateToVb(strTmp))
                    strRet = strRet & Format(dateTmp, "hh:mm") & "/" & Format(dateTmp, "DD")
                Else
                    strRet = strRet & "n.a."
                End If
            End If
        End If
    End If
    GetJobTimes = strRet
End Function

Public Function GetJobFunction(sJobUrno As String) As String
    Dim strLineNo As String
    Dim strSearch As String
    Dim strRet As String
    Dim ilDemandCnt As Integer

    strRet = ""

    strLineNo = TabJOD.GetLinesByIndexValue("UJOB", sJobUrno, 0)
    If IsNumeric(strLineNo) = True Then
        ' if there are duplicated demands, take the last on
        ilDemandCnt = ItemCount(strLineNo, ",")
        strLineNo = GetItem(strLineNo, ilDemandCnt, ",")

        strSearch = TabJOD.GetFieldValue(CLng(strLineNo), "UDEM")
        strLineNo = TabDEM.GetLinesByIndexValue("URNO", strSearch, 0)
        If IsNumeric(strLineNo) = True Then
            strSearch = TabDEM.GetFieldValue(CLng(strLineNo), "URUD")
            strLineNo = TabRPF.GetLinesByIndexValue("URUD", strSearch, 0)
            If IsNumeric(strLineNo) = True Then
                strSearch = TabRPF.GetFieldValue(CLng(strLineNo), "FCCO")
                If Len(strSearch) > 0 Then
                    strRet = strSearch
                Else
                    strSearch = TabRPF.GetFieldValue(CLng(strLineNo), "UPFC")
                    strLineNo = TabSGR.GetLinesByIndexValue("URNO", strSearch, 0)
                    If IsNumeric(strLineNo) = True Then
                        strRet = TabSGR.GetFieldValue(CLng(strLineNo), "GRPN")
                    End If
                End If
            End If
        End If
    End If

    ' we take the function of the shift
    If Len(strRet) = 0 Then
        strLineNo = TabJOB.GetLinesByIndexValue("URNO", sJobUrno, 0)
        If IsNumeric(strLineNo) = True Then
            Dim strUdsr As String
            strUdsr = TabJOB.GetFieldValue(CLng(strLineNo), "UDSR")
            strLineNo = TabDRR.GetLinesByIndexValue("URNO", strUdsr, 0)
            If IsNumeric(strLineNo) = True Then
                strRet = TabDRR.GetFieldValue(CLng(strLineNo), "FCTC")
            End If
        End If
    End If

    GetJobFunction = strRet
End Function

Public Function GetJobDuty(sJobUrno As String) As String
    Dim strLineNo As String
    Dim strSearch As String
    Dim ilIdx As Integer
    Dim strUJTY As String
    Dim strAftUrnoInbound As String
    Dim strAftUrnoOutbound As String
    Dim strFlnoInbound As String
    Dim strFlnoOutbound As String
    Dim strFlno As String
    Dim strDETY As String
    Dim ilDemandCnt As Integer

    GetJobDuty = ""

    strLineNo = TabJOB.GetLinesByIndexValue("UJOB", sJobUrno, 0)
    If IsNumeric(strLineNo) = True Then
        strUJTY = TabJOB.GetFieldValue(CLng(strLineNo), "UJTY")
        Select Case strUJTY
            ' kkh added on 06/11/2006
            Case strFLT, strDFJ:
            
                ' get the FLNO of flight of the job
                strLineNo = TabAFT.GetLinesByIndexValue("URNO", TabJOB.GetFieldValue(CLng(strLineNo), "UAFT"), 0)
                If IsNumeric(strLineNo) = True Then
                    'strFlno = TabAFT.GetFieldValue(CLng(strLineNo), "FLNO")
                    'igu on 21/04/2011
                    'take call sign when FLNO is empty
                    strFlno = GetFlnoOrCsgn(TabAFT, CLng(strLineNo))
                End If

                ' look if there is a demand - then take FLNO(s) of the demand
                strLineNo = TabJOD.GetLinesByIndexValue("UJOB", sJobUrno, 0)
                If IsNumeric(strLineNo) = True Then
                    ' if there are duplicated demands, take the last on
                    ilDemandCnt = ItemCount(strLineNo, ",")
                    strLineNo = GetItem(strLineNo, ilDemandCnt, ",")

                    strSearch = TabJOD.GetFieldValue(CLng(strLineNo), "UDEM")
                    strLineNo = TabDEM.GetLinesByIndexValue("URNO", strSearch, 0)
                    If IsNumeric(strLineNo) = True Then
                        ' get AFT-urnos and demand type
                        strAftUrnoInbound = TabDEM.GetFieldValue(CLng(strLineNo), "OURI")
                        strAftUrnoOutbound = TabDEM.GetFieldValue(CLng(strLineNo), "OURO")
                        strDETY = TabDEM.GetFieldValue(CLng(strLineNo), "DETY")

                        ' get inbound FLNO
                        strLineNo = TabAFT.GetLinesByIndexValue("URNO", strAftUrnoInbound, 0)
                        If IsNumeric(strLineNo) = True Then
                            'strFlnoInbound = TabAFT.GetFieldValue(CLng(strLineNo), "FLNO")
                            'igu on 21/04/2011
                            'take call sign when FLNO is empty
                            strFlnoInbound = GetFlnoOrCsgn(TabAFT, CLng(strLineNo))
                        End If

                        ' get outbound FLNO
                        strLineNo = TabAFT.GetLinesByIndexValue("URNO", strAftUrnoOutbound, 0)
                        If IsNumeric(strLineNo) = True Then
                            'strFlnoOutbound = TabAFT.GetFieldValue(CLng(strLineNo), "FLNO")
                            'igu on 21/04/2011
                            'take call sign when FLNO is empty
                            strFlnoOutbound = GetFlnoOrCsgn(TabAFT, CLng(strLineNo))
                        End If

                        If strDETY = "0" Then
                            ' it is a turnaround
                            GetJobDuty = strFlnoInbound & "/" & strFlnoOutbound
                        ElseIf strDETY = "1" Then
                            ' it is an inbound
                            GetJobDuty = strFlnoInbound
                        ElseIf strDETY = "2" Then
                            ' it is an outbound
                            GetJobDuty = strFlnoOutbound
                        End If
                    ' KKH on 12 / 06 /2007
                    Else
                        GetJobDuty = strFlno
                    End If
                Else
                    GetJobDuty = strFlno
                End If
            Case strBRK:
                GetJobDuty = "Break"
            Case strFID:
                GetJobDuty = GetJobServiceName(sJobUrno)
            Case strGAT, strCCI:
                GetJobDuty = GetAlidForJob(TabJOB.GetFieldValue(CLng(strLineNo), "UAID"), TabJOB.GetFieldValue(CLng(strLineNo), "UALO"))
            Case strCCC:
                GetJobDuty = GetCciCounter(TabJOB.GetFieldValue(CLng(strLineNo), "URNO"))
            Case Else: 'e.g. strSPE
                GetJobDuty = TabJOB.GetFieldValue(CLng(strLineNo), "TEXT")
        End Select
    End If
End Function

Private Function GetCciCounter(sJobUrno As String) As String
    Dim strLineNo As String
    Dim strSearch As String
    Dim ilDemandCnt As Integer

    GetCciCounter = ""
    strLineNo = TabJOD.GetLinesByIndexValue("UJOB", sJobUrno, 0)
    If IsNumeric(strLineNo) = True Then
        ' if there are duplicated demands, take the last on
        ilDemandCnt = ItemCount(strLineNo, ",")
        strLineNo = GetItem(strLineNo, ilDemandCnt, ",")

        strSearch = TabJOD.GetFieldValue(CLng(strLineNo), "UDEM")
        strLineNo = TabDEM.GetLinesByIndexValue("URNO", strSearch, 0)
        If IsNumeric(strLineNo) = True Then
            strSearch = TabDEM.GetFieldValue(CLng(strLineNo), "URUD")
            strLineNo = TabRUD.GetLinesByIndexValue("URNO", strSearch, 0)
            If IsNumeric(strLineNo) = True Then
                strSearch = TabRUD.GetFieldValue(CLng(strLineNo), "VREF")
                strLineNo = TabCCC.GetLinesByIndexValue("URNO", strSearch, 0)
                If IsNumeric(strLineNo) = True Then
                    GetCciCounter = "CCI " & TabCCC.GetFieldValue(CLng(strLineNo), "CICC")
                End If
            End If
        End If
    End If
End Function

Private Function GetAlidForJob(sUAID As String, sUALO As String) As String
    Dim strLineNo As String
    Dim strSearch As String

    strLineNo = TabALO.GetLinesByIndexValue("URNO", sUALO, 0)

    If IsNumeric(strLineNo) = True Then
        Dim strTable As String
        strTable = Left(TabALO.GetFieldValue(CLng(strLineNo), "REFT"), 3)

        If strTable = "GAT" Then
            strLineNo = TabGAT.GetLinesByIndexValue("URNO", sUAID, 0)
            If IsNumeric(strLineNo) = True Then
                GetAlidForJob = "Gate: " & TabGAT.GetFieldValue(CLng(strLineNo), "GNAM")
            End If
        ElseIf strTable = "PST" Then
            strLineNo = TabPST.GetLinesByIndexValue("URNO", sUAID, 0)
            If IsNumeric(strLineNo) = True Then
                GetAlidForJob = "Park. Stand : " & TabPST.GetFieldValue(CLng(strLineNo), "PNAM")
            End If
        ElseIf strTable = "CIC" Then
            strLineNo = TabCIC.GetLinesByIndexValue("URNO", sUAID, 0)
            If IsNumeric(strLineNo) = True Then
                GetAlidForJob = "CIC Desk: " & TabCIC.GetFieldValue(CLng(strLineNo), "CNAM")
            End If
        Else
            strLineNo = TabSGR.GetLinesByIndexValue("URNO", sUAID, 0)
            If IsNumeric(strLineNo) = True Then
                GetAlidForJob = TabSGR.GetFieldValue(CLng(strLineNo), "GRPN")
            End If
        End If
    End If
End Function

Public Function GetJobServiceName(ByRef sJobUrno As String) As String
    Dim strLineNo As String
    Dim strSearch As String
    Dim ilDemandCnt As Integer

    'igu on 03/07/2009 to include service code
    GetJobServiceName = ","
    
    strLineNo = TabJOD.GetLinesByIndexValue("UJOB", sJobUrno, 0)
    If IsNumeric(strLineNo) = True Then
        ' if there are duplicated demands, take the last on
        ilDemandCnt = ItemCount(strLineNo, ",")
        strLineNo = GetItem(strLineNo, ilDemandCnt, ",")

        strSearch = TabJOD.GetFieldValue(CLng(strLineNo), "UDEM")
        strLineNo = TabDEM.GetLinesByIndexValue("URNO", strSearch, 0)
        If IsNumeric(strLineNo) = True Then
            strSearch = TabDEM.GetFieldValue(CLng(strLineNo), "URUD")
            strLineNo = TabRUD.GetLinesByIndexValue("URNO", strSearch, 0)
            If IsNumeric(strLineNo) = True Then
                strSearch = TabRUD.GetFieldValue(CLng(strLineNo), "UGHS")
                strLineNo = TabSER.GetLinesByIndexValue("URNO", strSearch, 0)
                If IsNumeric(strLineNo) = True Then
                    'igu on 03/07/2009 to include service code
                    'GetJobServiceName = TabSER.GetFieldValue(CLng(strLineNo), "SNAM")
                    GetJobServiceName = TabSER.GetFieldValue(CLng(strLineNo), "SECO") & "," & _
                        TabSER.GetFieldValue(CLng(strLineNo), "SNAM")
                End If
            End If
        End If
    End If
End Function

Private Sub InitJTY()
    Dim strLineNo As String

    strLineNo = TabJTY.GetLinesByIndexValue("NAME", "FLT", 0)
    If IsNumeric(strLineNo) = True Then strFLT = TabJTY.GetFieldValue(CLng(strLineNo), "URNO")

    strLineNo = TabJTY.GetLinesByIndexValue("NAME", "GAT", 0)
    If IsNumeric(strLineNo) = True Then strGAT = TabJTY.GetFieldValue(CLng(strLineNo), "URNO")

    strLineNo = TabJTY.GetLinesByIndexValue("NAME", "BRK", 0)
    If IsNumeric(strLineNo) = True Then strBRK = TabJTY.GetFieldValue(CLng(strLineNo), "URNO")

    strLineNo = TabJTY.GetLinesByIndexValue("NAME", "SPE", 0)
    If IsNumeric(strLineNo) = True Then strSPE = TabJTY.GetFieldValue(CLng(strLineNo), "URNO")

    strLineNo = TabJTY.GetLinesByIndexValue("NAME", "CCC", 0)
    If IsNumeric(strLineNo) = True Then strCCC = TabJTY.GetFieldValue(CLng(strLineNo), "URNO")

    strLineNo = TabJTY.GetLinesByIndexValue("NAME", "CCI", 0)
    If IsNumeric(strLineNo) = True Then strCCI = TabJTY.GetFieldValue(CLng(strLineNo), "URNO")

    strLineNo = TabJTY.GetLinesByIndexValue("NAME", "FID", 0)
    If IsNumeric(strLineNo) = True Then strFID = TabJTY.GetFieldValue(CLng(strLineNo), "URNO")
    
    'kkh added on 06/11/2006,
    'change in InfoPC.cfg added JOB_TYPE_UJTY=... '2021'
    strLineNo = TabJTY.GetLinesByIndexValue("NAME", "DFJ", 0)
    If IsNumeric(strLineNo) = True Then strDFJ = TabJTY.GetFieldValue(CLng(strLineNo), "URNO")
End Sub

Public Function GetFlightInformation(sJobUrno As String) As String
    Dim strLineNo As String
    Dim strSearch As String
    Dim strAftUrnoInbound As String
    Dim strAftUrnoOutbound As String
    Dim strDETY As String
    Dim strUAFT As String
    Dim strRet As String
    Dim ilDemandCnt As Integer

    GetFlightInformation = ""
    strRet = ""

    strLineNo = TabJOB.GetLinesByIndexValue("UJOB", sJobUrno, 0)
    If IsNumeric(strLineNo) = True Then
        ' get the FLNO of flight of the job
        strUAFT = TabJOB.GetFieldValue(CLng(strLineNo), "UAFT")

        ' look if there is a demand - then take FLNO(s) of the demand
        strLineNo = TabJOD.GetLinesByIndexValue("UJOB", sJobUrno, 0)
        If IsNumeric(strLineNo) = True Then
            ' if there are duplicated demands, take the last on
            ilDemandCnt = ItemCount(strLineNo, ",")
            strLineNo = GetItem(strLineNo, ilDemandCnt, ",")

            strSearch = TabJOD.GetFieldValue(CLng(strLineNo), "UDEM")
            strLineNo = TabDEM.GetLinesByIndexValue("URNO", strSearch, 0)
            If IsNumeric(strLineNo) = True Then
                ' we found a demand - get AFT-urnos and demand type
                strAftUrnoInbound = TabDEM.GetFieldValue(CLng(strLineNo), "OURI")
                strAftUrnoOutbound = TabDEM.GetFieldValue(CLng(strLineNo), "OURO")
                strDETY = TabDEM.GetFieldValue(CLng(strLineNo), "DETY")

                If strDETY = "0" Then
                    ' it is a turnaround
                    ' kkh changed on 18/04/2007 added Terminal information (Terminal)
                    strRet = GetInboundData(strAftUrnoInbound, True, True, False)
                    strRet = strRet & "," & GetOutboundData(strAftUrnoOutbound, False, False)
                ElseIf strDETY = "1" Then
                    ' it is an inbound
                    strRet = GetInboundData(strAftUrnoInbound, True, True, True)
                    'kkh changed on 18/04/2007 added Terminal information (Terminal)
                    'strRet = strRet & ",,,,"
                ElseIf strDETY = "2" Then
                    ' it is an outbound
                    strRet = ",,,,"
                    strRet = strRet & GetOutboundData(strAftUrnoOutbound, True, True)
                Else
                    'kkh changed on 18/04/2007 added Terminal information (Terminal)
                    strRet = ",,,,,,,,,,"
                    'strRet = ",,,,,,,,,"
                End If
            End If
        Else
            ' there is no demand - we have to take UAFT from JOBTAB to get flight information
            'kkh changed on 18/04/2007 added Terminal information (Terminal)
            strRet = GetInboundData(strUAFT, True, True, False)
            strRet = strRet & "," & GetOutboundData(strUAFT, False, False)
        End If
        strRet = strRet & "," & GetAftGate(strUAFT)
    End If
    GetFlightInformation = strRet
End Function
'kkh changed on 18/04/2007 added Terminal information (Terminal)
Private Function GetInboundData(sAftUrno As String, bWithRegn As Boolean, bWithAct As Boolean, bGetTerminal As Boolean)
    Dim strRet As String
    Dim strLineNo As String
    Dim dateTmp As Date
    Dim strTmp As String

    strLineNo = TabAFT.GetLinesByIndexValue("URNO", sAftUrno, 0)
    If IsNumeric(strLineNo) = True Then
        Dim strADID As String
        strADID = TabAFT.GetFieldValue(CLng(strLineNo), "ADID")

        If strADID = "A" Or strADID = "B" Then
            'strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "FLNO") & ","
            'igu on 21/04/2011
            'take call sign when FLNO is empty
            strRet = strRet & GetFlnoOrCsgn(TabAFT, CLng(strLineNo)) & ","
            strTmp = TabAFT.GetFieldValue(CLng(strLineNo), "STOA")
            If Len(strTmp) > 0 Then
                dateTmp = CedaFullDateToVb(strTmp) + (sglUTCOffsetHours / 24)
                'dateTmp = DateAdd("h", sglUTCOffsetHours, CedaFullDateToVb(strTmp))
                strRet = strRet & Format(dateTmp, "hh:mm") & "/" & Format(dateTmp, "DD") & ","
            Else
                strRet = strRet & ","
            End If
            strTmp = TabAFT.GetFieldValue(CLng(strLineNo), "ETAI")
            If Len(strTmp) > 0 Then
                dateTmp = CedaFullDateToVb(strTmp) + (sglUTCOffsetHours / 24)
                'dateTmp = DateAdd("h", sglUTCOffsetHours, CedaFullDateToVb(strTmp))
                strRet = strRet & Format(dateTmp, "hh:mm") & "/" & Format(dateTmp, "DD") & ","
            Else
                strRet = strRet & ","
            End If

            strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "ORG3") & "/"
            strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "ORG4")

            If bWithRegn = True Then
                strRet = strRet & "," & TabAFT.GetFieldValue(CLng(strLineNo), "REGN")
            End If
            If bWithAct = True Then
                strRet = strRet & "," & TabAFT.GetFieldValue(CLng(strLineNo), "ACT3") & "/"
                strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "ACT5")
            End If
            
            'kkh changed on 18/04/2007 added Terminal information (Terminal)
            If bGetTerminal = True Then
                strRet = strRet & ",,,,"
                strRet = strRet & "," & TabAFT.GetFieldValue(CLng(strLineNo), "TGA1")
            End If
        Else
            strRet = strRet & ",,,"
            If bWithRegn = True Then strRet = strRet & ","
            If bWithAct = True Then strRet = strRet & ","
        End If
    Else
        'kkh changed on 18/04/2007 added Terminal information (Terminal)
        strRet = ",,,,"
        If bWithRegn = True Then strRet = strRet & ","
        If bWithAct = True Then strRet = strRet & ","
    End If
    GetInboundData = strRet
End Function

Public Function GetPassengerInfo(ByVal sUPRM As String) As String
    Dim strLineNo As String
    Dim lngLineNo As Long
    
    lngLineNo = -1
    strLineNo = DPXTAB.GetLinesByColumnValue(0, sUPRM, 2)
    If IsNumeric(strLineNo) Then lngLineNo = CLng(strLineNo)
    If lngLineNo > -1 Then
        'GetPassengerInfo = DPXTAB.GetFieldValue(lngLineNo, "NAME") & "," & _
            DPXTAB.GetFieldValue(lngLineNo, "SEAT") & ",," & _
            DPXTAB.GetFieldValue(lngLineNo, "REMA") 'igu on 06 Jan 2010
        'GetPassengerInfo = DPXTAB.GetFieldValue(lngLineNo, "NAME") & "," & _
            DPXTAB.GetFieldValue(lngLineNo, "SEAT") & ",," & _
            CleanString(DPXTAB.GetFieldValue(lngLineNo, "REMA"), FOR_CLIENT, True) 'igu on 06 Jan 2010 'igu on 12 Mar 2010
        GetPassengerInfo = DPXTAB.GetFieldValue(lngLineNo, "NAME") & "," & _
            DPXTAB.GetFieldValue(lngLineNo, "SEAT") & ",," & _
            Replace(CleanString(DPXTAB.GetFieldValue(lngLineNo, "REMA"), _
                                FOR_CLIENT, True), _
                    ",", ";") 'igu on 12 Mar 2010
    Else
        GetPassengerInfo = ",,,"
    End If
End Function

Private Function GetOutboundData(sAftUrno As String, bWithRegn As Boolean, bWithAct As Boolean)
    Dim strRet As String
    Dim strLineNo As String
    Dim dateTmp As Date
    Dim strTmp As String

    GetOutboundData = ""
    strLineNo = TabAFT.GetLinesByIndexValue("URNO", sAftUrno, 0)
    If IsNumeric(strLineNo) = True Then
        Dim strADID As String
        strADID = TabAFT.GetFieldValue(CLng(strLineNo), "ADID")
        If strADID = "D" Or strADID = "B" Then
            If bWithRegn = True Then
                strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "REGN") & ","
            End If
            If bWithAct = True Then
                strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "ACT3") & "/"
                strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "ACT5") & ","
            End If

            'strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "FLNO") & ","
            'igu on 21/04/2011
            'take call sign when FLNO is empty
            strRet = strRet & GetFlnoOrCsgn(TabAFT, CLng(strLineNo)) & ","
            strTmp = TabAFT.GetFieldValue(CLng(strLineNo), "STOD")
            If Len(strTmp) > 0 Then
                'dateTmp = CedaFullDateToVb(strTmp) + (sglUTCOffsetHours / 24)
                dateTmp = DateAdd("h", sglUTCOffsetHours, CedaFullDateToVb(strTmp))
                strRet = strRet & Format(dateTmp, "hh:mm") & "/" & Format(dateTmp, "DD") & ","
            Else
                strRet = strRet & ","
            End If
            strTmp = TabAFT.GetFieldValue(CLng(strLineNo), "ETDI")
            If Len(strTmp) > 0 Then
                'dateTmp = CedaFullDateToVb(strTmp) + (sglUTCOffsetHours / 24)
                dateTmp = DateAdd("h", sglUTCOffsetHours, CedaFullDateToVb(strTmp))
                strRet = strRet & Format(dateTmp, "hh:mm") & "/" & Format(dateTmp, "DD") & ","
            Else
                strRet = strRet & ","
            End If
            strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "DES3") & "/"
            strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "DES4") & ","
            'kkh changed on 18/04/2007 added Terminal information (Terminal)
            strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "TGD1")
        Else
            'igu on 03/07/2009 added Terminal information (Terminal)
            'strRet = strRet & ",,,"
            strRet = strRet & ",,,,"
            If bWithRegn = True Then strRet = strRet & ","
            If bWithAct = True Then strRet = strRet & ","
        End If
    Else
        'igu on 03/07/2009 added Terminal information (Terminal)
        'strRet = strRet & ",,,"
        strRet = strRet & ",,,,"
        If bWithRegn = True Then strRet = strRet & ","
        If bWithAct = True Then strRet = strRet & ","
    End If
    GetOutboundData = strRet
End Function

Public Function GetJobLocation(sJobUrno As String) As String
    Dim strLineNo As String
    GetJobLocation = ""

    strLineNo = TabJOB.GetLinesByIndexValue("UJOB", sJobUrno, 0)
    If IsNumeric(strLineNo) = True Then
        Dim strUALO As String
        Dim strUAID As String

        strUALO = TabJOB.GetFieldValue(CLng(strLineNo), "UALO")
        strUAID = TabJOB.GetFieldValue(CLng(strLineNo), "UAID")
        GetJobLocation = GetAlidForJob(strUAID, strUALO)
    End If
End Function

Public Function GetJobStatus(sJobUrno As String, sSTAT As String) As String
    GetJobStatus = ""

    Dim strSTAT As String

    Dim blPlanned As Boolean
    Dim blInformed As Boolean
    Dim blConfirmed As Boolean 'has the meaning of "started"
    Dim blFinished As Boolean

    If Len(sSTAT) = 0 Then
        Dim strLineNo As String
        strLineNo = TabJOB.GetLinesByIndexValue("UJOB", sJobUrno, 0)
        If IsNumeric(strLineNo) = True Then
            strSTAT = TabJOB.GetFieldValue(CLng(strLineNo), "STAT")
        End If
    Else
        strSTAT = sSTAT
    End If

    GetJobStatusDetails strSTAT, blPlanned, blInformed, blConfirmed, blFinished

    If blPlanned = True Then
        If blInformed = True Then
            GetJobStatus = "Informed"
        Else
            GetJobStatus = "Planned"
        End If
    ElseIf blConfirmed = True Then ' meaning of "started"
        GetJobStatus = "Confirmed"
    ElseIf blFinished = True Then
        GetJobStatus = "Finished"
    Else
        GetJobStatus = ""
    End If
End Function

Public Function SetNextJobStatus(sJobUrno As String) As String
    Dim strLineNo As String
    Dim strRet As String

    strLineNo = TabJOB.GetLinesByIndexValue("URNO", sJobUrno, 0)

    If IsNumeric(strLineNo) = True Then
        Dim strCfg As String
        Dim strSTAT As String
        Dim blPlanned As Boolean
        Dim blInformed As Boolean
        Dim blConfirmed As Boolean 'has the meaning of "started"
        Dim blFinished As Boolean
        Dim strActualSTAT As String
        Dim strNextSTAT As String
        Dim ilIdx As Integer

        strSTAT = TabJOB.GetFieldValue(CLng(strLineNo), "STAT")
        GetJobStatusDetails strSTAT, blPlanned, blInformed, blConfirmed, blFinished
        While Len(strSTAT) < 10
            strSTAT = strSTAT & " " 'it's easier to handle later
        Wend

        ' checking out the actual status
        If blPlanned = True Then
            If blInformed = True Then
                strActualSTAT = "INFORM"
            Else
                strActualSTAT = "PLAN"
            End If
        ElseIf blConfirmed = True Then ' meaning of "started"
            strActualSTAT = "CONFIRM"
        ElseIf blFinished = True Then
            strActualSTAT = "END"
        Else
            strActualSTAT = ""
        End If

        If (GetConfigEntry("WORKFLOW", strOrgUnit, strCfg) = False) Then
            MsgBoxNoCfgEntry "WORKFLOW"
        Else
            ' we have to set the next status depending on the workflow
            ilIdx = GetRealItemNo(strCfg, strActualSTAT)
            If ilIdx > -1 Then
                strNextSTAT = GetRealItem(strCfg, ilIdx + 1, ",")
                If Len(strNextSTAT) > 0 Then
                    Select Case strNextSTAT
                        Case "INFORM":
                            strSTAT = Trim(Left(strSTAT, 4) & "1" & Mid(strSTAT, 6))
                            strRet = "Informed"
                        Case "CONFIRM":
                            strSTAT = Trim("C" & Mid(strSTAT, 2))
                            strRet = "Confirmed"
                        Case "FINISH":
                            strSTAT = Trim("F" & Mid(strSTAT, 2))
                            strRet = "Finished"
                        Case Else:
                    End Select

                    ' set the value internal
                    TabJOB.SetFieldValues CLng(strLineNo), "STAT", strSTAT

                    ' set the values in the database
                    ConnectUfisCom
                    Dim strFieldList As String
                    Dim strData As String
                    Dim strWhere As String
                    Dim strAddFieldList As String
                    strAddFieldList = ""
                    strData = TabJOB.GetFieldValues(CLng(strLineNo), "URNO,STAT")
                    If (strNextSTAT = "CONFIRM") Then
                        strData = strData & "," & gsUserName & "," & GetActualCedaUtcTime("YYYYMMDDhhmmss") & GetJobStatusTime(strNextSTAT, strLineNo, strAddFieldList)
                        strFieldList = "URNO,STAT,USEU,LSTU" & strAddFieldList
                    ElseIf (strNextSTAT = "FINISH") Then
                        strData = strData & "," & gsUserName & "," & GetActualCedaUtcTime("YYYYMMDDhhmmss") & GetJobStatusTime(strNextSTAT, strLineNo, strAddFieldList)
                        strFieldList = "URNO,STAT,USEU,LSTU" & strAddFieldList
                    Else
                        strData = strData & "," & gsUserName & "," & GetActualCedaUtcTime("YYYYMMDDhhmmss")
                        strFieldList = "URNO,STAT,USEU,LSTU"
                    End If
                    
                    strWhere = "WHERE URNO = '" & TabJOB.GetFieldValue(CLng(strLineNo), "URNO") & "'"
                    If frmSplash.UfisCom1.CallServer("URT", "JOBTAB", strFieldList, strData, strWhere, "360") <> 0 Then
                        MsgBox "Failed to update the job status!", vbCritical, "Database error"
                    End If
                    DisconnetUfisCom
                End If
            End If
        End If
    End If

    SetNextJobStatus = strRet
End Function
Private Function GetJobStatusTime(strNextSTAT As String, strLineNo As String, ByRef strAddFieldList As String) As String
    Dim strJobConfirmDefault As String
    Dim strJobFinishDefault As String
    Dim datCurr As Date
    Dim datAcfr As Date
    Dim datActo As Date
    
    datAcfr = CedaFullDateToVb(TabJOB.GetFieldValue(CLng(strLineNo), "ACFR"))
    datActo = CedaFullDateToVb(TabJOB.GetFieldValue(CLng(strLineNo), "ACTO"))
    
    datCurr = CedaFullDateToVb(GetActualCedaUtcTime("YYYYMMDDhhmmss"))
    
    strAddFieldList = ""
    Select Case strNextSTAT
    Case "INFORM":
        GetJobStatusTime = GetActualCedaUtcTime("YYYYMMDDhhmmss")
    Case "CONFIRM":
        If (GetConfigEntry("JOBCONFIRMDEFAULT", strOrgUnit, strJobConfirmDefault) = False) Then
            If datCurr < datActo Then
                strAddFieldList = ",ACFR"
                GetJobStatusTime = "," & GetActualCedaUtcTime("YYYYMMDDhhmmss")
            Else
                GetJobStatusTime = ""
            End If
        ElseIf (strJobConfirmDefault = "PLANNED") Then
            GetJobStatusTime = ""
        Else
            If datCurr < datActo Then
                strAddFieldList = ",ACFR"
                GetJobStatusTime = "," & GetActualCedaUtcTime("YYYYMMDDhhmmss")
            Else
                GetJobStatusTime = ""
            End If
        End If
    Case "FINISH":
        If (GetConfigEntry("JOBFINISHDEFAULT", strOrgUnit, strJobFinishDefault) = False) Then
            If datCurr > datAcfr Then
                strAddFieldList = ",ACTO"
                GetJobStatusTime = "," & GetActualCedaUtcTime("YYYYMMDDhhmmss")
            Else
                GetJobStatusTime = ""
            End If
        ElseIf (strJobFinishDefault = "PLANNED") Then
            GetJobStatusTime = ""
        Else
            If datCurr > datAcfr Then
                strAddFieldList = ",ACTO"
                GetJobStatusTime = "," & GetActualCedaUtcTime("YYYYMMDDhhmmss")
            Else
                GetJobStatusTime = ""
            End If
        End If
    End Select
End Function

Private Function GetJobStatusDetails(sSTAT As String, brPlanned As Boolean, brInformed As Boolean, brConfirmed As Boolean, brFinished As Boolean)
'[0] Job Status 'P'lanned,'C'onfirmed or 'F'inished
'[1] '1' = Manually changed else '0'
'[2] Non-break-jobs:'1' = Ignore this job in automatic allocation (Fix Flag) else '0' Break-jobs:'1' = Coffee break (unpaid break)'0' = Normal break.
'[3] used for CASP process (Softlab)
'[4] Normal Jobs: '1' = job is planned, the emp has been informed of the job but has not yet started else '0'. Pool Jobs: 'A' = employee is absent else '0'.
'[5] '1' = job has been printed else '0'
'[6] - [9] Unused.

    Dim strTmp As String

    brPlanned = False
    brInformed = False
    brConfirmed = False
    brFinished = False

    If Len(sSTAT) > 0 Then
        'checking first character
        strTmp = Left(sSTAT, 1)
        Select Case strTmp
            Case "P":
                brPlanned = True
            Case "C":
                brConfirmed = True
            Case "F":
                brFinished = True
        End Select
    End If

    If Len(sSTAT) > 4 Then
        ' checking the 5th character
        strTmp = Mid(sSTAT, 5, 1)
        If strTmp = "1" Then
            brInformed = True
        End If
    End If
End Function
Public Function GetJobEquipment(sJobUequ As String) As String
    Dim strLineNo As String
    strLineNo = TabEQU.GetLinesByIndexValue("URNO", sJobUequ, 0)
    If IsNumeric(strLineNo) = True Then
        GetJobEquipment = TabEQU.GetFieldValue(CLng(strLineNo), "ENAM")
    End If
End Function

Private Function GetActualCedaUtcTime(sFormat As String) As String
    Dim dateTmp As Date
    Dim strRet As String

    dateTmp = Now - (sglUTCOffsetHours / 24)
    strRet = Format(dateTmp, sFormat)
    GetActualCedaUtcTime = strRet
End Function

Public Function FilterJobs()
    Dim ilIdx As Integer
    Dim ilIdxAvto As Integer
    Dim strCfg As String
    Dim i As Integer
    Dim strTmp As String

    ' sort jobs by start time
    ilIdx = GetItemNo(TabJOB.LogicalFieldList, "ACFR") - 1
    ilIdxAvto = GetItemNo(TabJOB.LogicalFieldList, "AVTO") - 1
    TabJOB.Sort CStr(ilIdx) & "," & CStr(ilIdxAvto), True, True

    ' filtering: JOB_STATUS_DISPLAY (possible: PLANNED,STARTED,CONFIRMED,FINISHED)
    If (GetConfigEntry("JOB_STATUS_DISPLAY", strOrgUnit, strCfg) = False) Then
        MsgBoxNoCfgEntry "JOB_STATUS_DISPLAY"
    Else
        Dim strSTAT As String
        For i = TabJOB.GetLineCount - 1 To 0 Step -1
            strSTAT = TabJOB.GetFieldValue(CLng(i), "STAT")
            strTmp = UCase(GetJobStatus("", strSTAT))
            If InStr(strCfg, strTmp) = 0 Then
                TabJOB.DeleteLine i
            End If
        Next i
    End If

    ' filtering JOB_NUMBER_DISPLAY (e.g.-2,10)
    If (GetConfigEntry("JOB_NUMBER_DISPLAY", strOrgUnit, strCfg) = False) Then
        MsgBoxNoCfgEntry "JOB_NUMBER_DISPLAY"
    Else
        Dim strNow As String
        Dim ilFrom As Integer
        Dim ilTo As Integer

        ilFrom = CInt(GetItem(strCfg, 1, ","))
        ilTo = CInt(GetItem(strCfg, 2, ","))

        ' searching the (best fitting) actual job
        If TabJOB.GetLineCount > 0 Then
            i = 0
            strNow = GetActualCedaUtcTime("YYYYMMDDhhmmss")
            While TabJOB.GetFieldValue(i + 1, "ACFR") < strNow And i <= TabJOB.GetLineCount
                i = i + 1
            Wend

            ' don't show too many jobs in the future
            While (i + ilTo) < TabJOB.GetLineCount - 1
                TabJOB.DeleteLine TabJOB.GetLineCount - 1
            Wend

            ' don't show too many jobs in the past
            While (i + ilFrom) > 0
                TabJOB.DeleteLine 0
                i = i - 1
            Wend
        End If
    End If

    TabJOB.IndexCreate "URNO", 0
    'TabJOB.IndexCreate "UAFT", 1
End Function

Public Function IsAllowedToChangeJobStatus(sJobUrno As String)
    Dim strCfg As String
    Dim blRet As Boolean
    Dim i As Integer

    blRet = False

    'checking the INTERACTION_STATUS (e.g.-1,3)
    If (GetConfigEntry("INTERACTION_STATUS", strOrgUnit, strCfg) = False) Then
        MsgBoxNoCfgEntry "INTERACTION_STATUS"
    Else
        Dim strLineNo As String
        Dim strNow As String
        Dim ilFrom As Integer
        Dim ilTo As Integer

        ilFrom = CInt(GetItem(strCfg, 1, ","))
        ilTo = CInt(GetItem(strCfg, 2, ","))

        If TabJOB.GetLineCount > 0 Then
            ' searching the (best fitting) actual job
            i = 0
            strNow = GetActualCedaUtcTime("YYYYMMDDhhmmss")
            While TabJOB.GetFieldValue(i + 1, "ACFR") < strNow And i <= TabJOB.GetLineCount
                i = i + 1
            Wend

            ' getting the LineNo of sJobUrno
            strLineNo = TabJOB.GetLinesByIndexValue("URNO", sJobUrno, 0)
            If IsNumeric(strLineNo) = True Then
                If (CInt(strLineNo) >= (i + ilFrom)) And (CInt(strLineNo) <= (i + ilTo)) Then
                    blRet = True
                End If
            End If
        End If
    End If
    IsAllowedToChangeJobStatus = blRet
End Function

Public Function GetAftUrnosByJob(sJobUrno As String) As String
    Dim strLineNo As String
    Dim strSearch As String
    Dim strAftUrnoInbound As String
    Dim strAftUrnoOutbound As String

    Dim strUAFT As String
    Dim strRet As String
    Dim ilDemandCnt As Integer

    GetAftUrnosByJob = ""
    strRet = ""

    strLineNo = TabJOB.GetLinesByIndexValue("UJOB", sJobUrno, 0)
    If IsNumeric(strLineNo) = True Then
        ' get the FLNO of flight of the job
        strUAFT = TabJOB.GetFieldValue(CLng(strLineNo), "UAFT")

        ' look if there is a demand - then take FLNO(s) of the demand
        strLineNo = TabJOD.GetLinesByIndexValue("UJOB", sJobUrno, 0)
        If IsNumeric(strLineNo) = True Then
            ' if there are duplicated demands, take the last on
            ilDemandCnt = ItemCount(strLineNo, ",")
            strLineNo = GetItem(strLineNo, ilDemandCnt, ",")

            strSearch = TabJOD.GetFieldValue(CLng(strLineNo), "UDEM")
            strLineNo = TabDEM.GetLinesByIndexValue("URNO", strSearch, 0)
            If IsNumeric(strLineNo) = True Then
                ' we found a demand - get AFT-urnos and demand type
                strAftUrnoInbound = TabDEM.GetFieldValue(CLng(strLineNo), "OURI")
                strAftUrnoOutbound = TabDEM.GetFieldValue(CLng(strLineNo), "OURO")

                If Len(strAftUrnoInbound) > 0 And strAftUrnoInbound <> "0" Then
                    strRet = strAftUrnoInbound
                End If
                If Len(strAftUrnoOutbound) > 0 And strAftUrnoOutbound <> "0" Then
                    If Len(strRet) > 0 Then
                        strRet = strRet & ","
                    End If
                    strRet = strRet & strAftUrnoOutbound
                End If
            Else
                strRet = strUAFT
            End If
        Else
            strRet = strUAFT
        End If
    Else
        strRet = strUAFT
    End If

    GetAftUrnosByJob = strRet
End Function

Public Function GetActualShiftTime() As String
    Dim strRet As String
    Dim strNow As String
    Dim i As Integer
    Dim blFound As Boolean
    Dim dateTmp As Date
    Dim strAVFR As String
    Dim strAVTO As String

    i = 0
    blFound = False
    strNow = Format(Now, "YYYYMMDDHHmmss")

    While i < TabDRR.GetLineCount And blFound = False
        If TabDRR.GetFieldValue(i, "ROSS") = "A" Then
            strAVFR = TabDRR.GetFieldValue(i, "AVFR")
            strAVTO = TabDRR.GetFieldValue(i, "AVTO")
            If ((strAVFR < strNow And strAVTO > strNow) Or (strAVFR > strNow)) Then
                blFound = True
                strRet = Mid(strAVFR, 9, 2) & ":" & Mid(strAVFR, 11, 2) & "/" & Mid(strAVFR, 7, 2) & " - "
                strRet = strRet & Mid(strAVTO, 9, 2) & ":" & Mid(strAVTO, 11, 2) & "/" & Mid(strAVTO, 7, 2)
            End If
        End If
        i = i + 1
    Wend
    GetActualShiftTime = strRet
End Function
' KKH on 10/04/2007 display BSDC, BSDN, change also TabDRR added BSDU
Public Function GetBasicShiftInfo() As String
    Dim i As Integer
    Dim blAdd As Boolean
    Dim strDrrRoss As String
    Dim strDrrSday As String
    Dim strBSDU As String
    Dim strToday As String
        
    For i = 0 To frmData.TabDRR.GetLineCount - 1
        blAdd = False
        strDrrRoss = frmData.TabDRR.GetFieldValue(i, "ROSS")
        strDrrSday = frmData.TabDRR.GetFieldValue(i, "SDAY")
        strToday = Format(Now, "YYYYMMDD")
        If strDrrRoss = "A" And strDrrSday = strToday Then
            blAdd = True
        End If
    
        If blAdd = True Then
            strBSDU = frmData.TabDRR.GetColumnValue(i, 11)
        End If
    Next i
    GetBasicShiftInfo = strBSDU
End Function

Public Function IsFreeOrSleepday(sOdaSdac As String) As Boolean
    Dim strLineNo As String
    Dim blRet As Boolean

    blRet = False
    strLineNo = TabODA.GetLinesByIndexValue("SDAC", sOdaSdac, 0)

    If IsNumeric(strLineNo) = True Then
        Dim strOdaFree As String
        Dim strOdaType As String
        strOdaFree = TabODA.GetFieldValue(CLng(strLineNo), "FREE")
        strOdaType = TabODA.GetFieldValue(CLng(strLineNo), "TYPE")
        If UCase(strOdaFree) = "X" Or UCase(strOdaType) = "S" Then
            blRet = True
        End If
    End If

    IsFreeOrSleepday = blRet
End Function

Public Function GetShiftData(sDrrUrno As String) As String
    Dim strLineNo As String
    Dim strRet As String
    Dim strTmp As String
    Dim strSday As String
    Dim strScod As String

    strLineNo = TabDRR.GetLinesByIndexValue("URNO", sDrrUrno, 0)

    If IsNumeric(strLineNo) = True Then
        ' add the date
        strTmp = TabDRR.GetFieldValue(CLng(strLineNo), "SDAY")
        strSday = strTmp
        If Len(strTmp) > 7 Then
            strRet = strRet & Mid(strTmp, 7, 2) & "-" & Mid(strTmp, 5, 2) & "-" & Left(strTmp, 4)
        End If
        strRet = strRet & ","

        strScod = TabDRR.GetFieldValue(CLng(strLineNo), "SCOD")
        
        If Len(Trim(strScod)) > 0 Then
            ' add the shift code
            strRet = strRet & strScod & ","

            ' add the shift times
            strTmp = TabDRR.GetFieldValue(CLng(strLineNo), "AVFR")
            If Len(strTmp) > 13 Then
                strRet = strRet & Mid(strTmp, 9, 2) & ":" & Mid(strTmp, 11, 2) & "/" & Mid(strTmp, 7, 2)
            End If
            strRet = strRet & "-"
            strTmp = TabDRR.GetFieldValue(CLng(strLineNo), "AVTO")
            If Len(strTmp) > 13 Then
                strRet = strRet & Mid(strTmp, 9, 2) & ":" & Mid(strTmp, 11, 2) & "/" & Mid(strTmp, 7, 2)
            End If
            strRet = strRet & ","
            
            strTmp = TabODA.GetLinesByIndexValue("SDAC", strScod, 0)
            If IsNumeric(strTmp) = True Then
                ' it is an absence -> no function, team
                 strRet = strRet & "-,-,"
            Else
                ' add the function
                strRet = strRet & TabDRR.GetFieldValue(CLng(strLineNo), "FCTC") & ","
        
                ' add the team
                strRet = strRet & GetTeamCode(strSday) & ","
            End If
    
            'add the remark
            strRet = strRet & TabDRR.GetFieldValue(CLng(strLineNo), "REMA")
        Else
            ' shift is "deleted"
            strRet = strRet & "-,-,-,-,"
        End If
    Else
        strRet = ",,,,,"
    End If

    GetShiftData = strRet
End Function
Public Function GetAbsenceData(sDraUrno As String) As String
    Dim strLineNo As String
    Dim strRet As String
    Dim strTmp As String
    Dim strSday As String
    Dim strScod As String

    strLineNo = TabDRA.GetLinesByIndexValue("URNO", sDraUrno, 0)

    If IsNumeric(strLineNo) = True Then
        ' add the date
        strTmp = TabDRA.GetFieldValue(CLng(strLineNo), "SDAY")
        strSday = strTmp
        If Len(strTmp) > 7 Then
            strRet = strRet & Mid(strTmp, 7, 2) & "-" & Mid(strTmp, 5, 2) & "-" & Left(strTmp, 4)
        End If
        strRet = strRet & ","

        strScod = TabDRA.GetFieldValue(CLng(strLineNo), "SDAC")
        
        If Len(Trim(strScod)) > 0 Then
            ' add the absence code
            strRet = strRet & strScod & ","

            ' add the absence times
            strTmp = TabDRA.GetFieldValue(CLng(strLineNo), "ABFR")
            If Len(strTmp) > 13 Then
                strRet = strRet & Mid(strTmp, 9, 2) & ":" & Mid(strTmp, 11, 2) & "/" & Mid(strTmp, 7, 2)
            End If
            strRet = strRet & "-"
            strTmp = TabDRA.GetFieldValue(CLng(strLineNo), "ABTO")
            If Len(strTmp) > 13 Then
                strRet = strRet & Mid(strTmp, 9, 2) & ":" & Mid(strTmp, 11, 2) & "/" & Mid(strTmp, 7, 2)
            End If
            strRet = strRet & ","
            
            ' it is an absence -> no function, team
            strRet = strRet & "-,-,"
            
            'add the remark
            strRet = strRet & TabDRA.GetFieldValue(CLng(strLineNo), "REMA")
        Else
            ' absence is "deleted"
            strRet = strRet & "-,-,-,-,"
        End If
    Else
        strRet = ",,,,,"
    End If

    GetAbsenceData = strRet
End Function

' getting the working group
Public Function GetTeamCode(sDay As String) As String
    Dim strRet As String
    Dim strLineNo As String

    strLineNo = TabDRG.GetLinesByIndexValue("SDAY", sDay, 0)
    If IsNumeric(strLineNo) = True Then
        strRet = TabDRG.GetFieldValue(CLng(strLineNo), "WGPC")
    Else
        strRet = GetBestFitRecord(TabSWG, strStfUrno, "SURN", "CODE", sDay)
    End If

    GetTeamCode = strRet
End Function

Public Function GetTeamCodeByJob(sJobUrno As String) As String
    Dim strLineNo As String
    Dim strUdsr As String
    Dim strSday As String
    Dim strRet As String

    strRet = ""

    strLineNo = TabJOB.GetLinesByIndexValue("URNO", sJobUrno, 0)
    If IsNumeric(strLineNo) = True Then
        strUdsr = TabJOB.GetFieldValue(CLng(strLineNo), "UDSR")
        strLineNo = TabDRR.GetLinesByIndexValue("URNO", strUdsr, 0)
        If IsNumeric(strLineNo) = True Then
            strSday = TabDRR.GetFieldValue(CLng(strLineNo), "SDAY")
            strRet = GetTeamCode(strSday)
        End If
    End If

    GetTeamCodeByJob = strRet
End Function

Public Function GetRecallDays() As String
    Dim strRet As String
    Dim strDrrOabs As String
    Dim strDrrScod As String
    Dim strDrrSday As String
    Dim strLineNo As String
    Dim i As Integer

    For i = 0 To TabDRR.GetLineCount - 1
        strDrrOabs = TabDRR.GetFieldValue(i, "OABS")
        If Len(strDrrOabs) > 0 Then
            strDrrScod = TabDRR.GetFieldValue(i, "SCOD")
            strLineNo = TabODA.GetLinesByIndexValue("SDAC", strDrrScod, 0)
            If strLineNo = "" And strDrrScod <> "" Then
                strDrrSday = TabDRR.GetFieldValue(i, "SDAY")
                If Len(strDrrSday) > 7 Then
                    If Len(strRet) > 0 Then
                        strRet = strRet & ","
                    End If
                    strRet = strRet & Mid(strDrrSday, 7, 2) & "-" & Mid(strDrrSday, 5, 2) & "-" & Left(strDrrSday, 4)
                End If
            End If
        End If
    Next i
    GetRecallDays = strRet
End Function

Public Function GetJobFlightRemark(sJobUrno As String) As String
On Error GoTo Errhdl
    Dim strRet As String
    Dim i As Integer
    Dim ilCnt As Integer
    Dim blArrDep As Boolean
    Dim strAftUrnos As String
    Dim strTmp As String

    blArrDep = False
    strRet = ""

    ' get the urnos of the flight(s) belonging to this job
    strAftUrnos = GetAftUrnosByJob(sJobUrno)
    ilCnt = ItemCount(strAftUrnos, ",")
    If ilCnt > 0 Then
        Dim strLineNo As String
        Dim ilIdx  As Integer

        ilIdx = GetItemNo(TabREM.LogicalFieldList, "RURN") - 1

        If ilCnt > 1 Then
            blArrDep = True 'this job has an inbound and an outbound flight
        End If

        For i = 1 To ilCnt Step 1
            strLineNo = TabREM.GetLinesByIndexValue("RURN", GetItem(strAftUrnos, i, ","), 0)
            If IsNumeric(strLineNo) = True Then
                strTmp = CleanString(TabREM.GetFieldValue(CLng(strLineNo), "TEXT"), FOR_CLIENT, True)
                If blArrDep = True And i = 1 And Len(strTmp) > 0 Then
                    strRet = strRet & "ARR: "
                ElseIf blArrDep = True And i = 2 And Len(strTmp) > 0 Then
                    If Len(strRet) > 0 Then
                        strRet = strRet & ", "
                    End If
                    strRet = strRet & "DEP: "
                End If
                strRet = strRet & strTmp
            End If
        Next i
    End If

    GetJobFlightRemark = strRet
Exit Function
Errhdl:
MsgBox Err.Source

    
End Function

Private Function GetAftGate(ByRef sAftUrno As String) As String
    'GTA1,GTD1
    Dim strLineNo As String
    Dim strRet As String

    strLineNo = frmData.TabAFT.GetLinesByIndexValue("URNO", sAftUrno, 0)
    If IsNumeric(strLineNo) = True Then
        Dim strADID As String
        Dim strGTA1 As String
        Dim strGTD1 As String

        strADID = TabAFT.GetFieldValue(CLng(strLineNo), "ADID")

        If strADID = "A" Or strADID = "B" Then
            strGTA1 = TabAFT.GetFieldValue(CLng(strLineNo), "GTA1")
        End If

        If strADID = "D" Or strADID = "B" Then
            strGTD1 = TabAFT.GetFieldValue(CLng(strLineNo), "GTD1")
        End If

        If Len(strGTA1) > 0 And Len(strGTD1) > 0 Then
            strRet = strGTA1 & "/" & strGTD1
        Else
            If Len(strGTA1) > 0 Then
                strRet = strGTA1
            ElseIf Len(strGTD1) > 0 Then
                strRet = strGTD1
            End If
        End If
    End If
    GetAftGate = strRet
End Function

'-----------------------------------------------------------------------
'MWO: Creates the joblist for the rotation of the Job with urno as parameter
' returns the 2 (or one) AFT urnos as comma delimited string
'-----------------------------------------------------------------------
Public Function CreateFlightInfoJobs(strJobUrno As String) As String
    Dim strRet As String
    Dim llLine As Long
    Dim strRkey As String
    Dim strAftUrnos As String
    Dim strAftUrNo As String
    Dim llItemNo As Long
    Dim llTabLine As Long
    Dim i As Long
    Dim j As Long
    
    strAftUrnos = GetAftUrnosByJob(strJobUrno)
'MWO TO DO: Read all jobs for this strAftUrnos into a tmp tab and walk through this tab
'           instead of TabJOB, because TabJOB contains all job for the current employee
'           and not all jobs for the flight rotation
'           ==> Should read with WHERE UAFT in (strAftUrnos)
    
    
    TabFlightInfo.ResetContent
    TabFlightInfo.HeaderString = "URNO,SERV,TEAM,FUNC,EMPL,ACFR,ACTO"
    TabFlightInfo.LogicalFieldList = TabFlightInfo.HeaderString
    TabFlightInfo.ColumnWidthString = "10,32,10,10,32,14,14"

    'First get the URNOs of the AFT Rotation and store it in strAftUrnos
    TabJOB.SetInternalLineBuffer True
    TabAFT.SetInternalLineBuffer True
    
    
    For i = 0 To ItemCount(strAftUrnos, ",") - 1
        strAftUrNo = GetRealItem(strAftUrnos, i, ",")
        For j = 0 To TabJOB.GetLineCount - 1
'        strRet = TabJOB.GetLinesByIndexValue("UAFT", strAftUrno, 0)
'        llLine = TabJOB.GetNextResultLine
'        While llLine > -1
        If TabJOB.GetFieldValue(j, "UAFT") = strAftUrNo Then
            TabFlightInfo.InsertTextLine ",,,,,,", False
            llTabLine = TabFlightInfo.GetLineCount - 1
            TabFlightInfo.SetFieldValues llTabLine, "URNO", TabJOB.GetFieldValue(j, "URNO")
            TabFlightInfo.SetFieldValues llTabLine, "ACFR", TabJOB.GetFieldValue(j, "ACFR")
            TabFlightInfo.SetFieldValues llTabLine, "ACTO", TabJOB.GetFieldValue(j, "ACTO")
            llLine = TabJOB.GetNextResultLine
        End If
'        Wend
        Next j
    Next i
    TabFlightInfo.Sort "5", True, True ' Sort for ACFR = Job start
    
    TabJOB.SetInternalLineBuffer False
    TabAFT.SetInternalLineBuffer False
    CreateFlightInfoJobs = strAftUrnos
End Function

Private Sub TabFlightInfo_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    TabFlightInfo.Left = 0
    TabFlightInfo.Top = 0
    TabFlightInfo.Width = Me.Width
    TabFlightInfo.Height = Me.Height
End Sub

Private Sub InitialLoadTeamleader()
    Dim strTmp As String
    Dim strWhere As String
    Dim l As Long
    Dim ilIdx As Integer

    InitTabsTeamleader
    Dim bpHandleSplashScreen As Boolean
    bpHandleSplashScreen = True

    ' loading DEMTAB data
    HandleSplashScreen "Loading DEMTAB (teamleader demands)...", bpHandleSplashScreen
    strTmp = TabAFT.SelectDistinct(0, "'", "'", ",", True)
    If Len(strTmp) > 0 And Len(gsTplUrno) > 0 Then
        strWhere = "WHERE (OURI IN (" & strTmp & ") OR OURO IN (" & strTmp & "))"
        ilIdx = GetItemNo(TabJOB.LogicalFieldList, "UTPL") - 1
        strTmp = TabDemTeamleader.SelectDistinct(CStr(ilIdx), "'", "'", ",", True)
        strTmp = strTmp & "'" & gsTplUrno & "'"
        strWhere = strWhere & " AND UTPL in (" & strTmp & ")"
        LoadData TabDemTeamleader, strWhere
    End If

    ' loading RPFTAB data
    HandleSplashScreen "Loading RPFTAB (connection between demands and functions of teamleaders)...", bpHandleSplashScreen
    ilIdx = GetItemNo(TabDemTeamleader.LogicalFieldList, "URUD") - 1
    strTmp = TabDemTeamleader.SelectDistinct(CStr(ilIdx), "'", "'", ",", True)
    If Len(strTmp) > 0 Then
        strWhere = "WHERE URUD IN (" & strTmp & ")"
        If (frmData.GetConfigEntry("FUNCTION_TEAM_LEADER", frmData.strOrgUnit, strTmp) = True) Then
            strTmp = Replace(strTmp, ",", "','")
            strWhere = strWhere & " AND FCCO IN ('" & strTmp & "')"
        End If
        LoadData TabRpfTeamleader, strWhere
    End If
    ilIdx = GetItemNo(TabRpfTeamleader.LogicalFieldList, "URUD") - 1
    TabRpfTeamleader.IndexCreate "URUD", ilIdx

    ' now we delete the demands without teamleader function to avoid unnecessary data loading
    For l = TabDemTeamleader.GetLineCount - 1 To 0 Step -1
        strTmp = TabDemTeamleader.GetFieldValue(l, "URUD")
        If Len(TabRpfTeamleader.GetLinesByIndexValue("URUD", strTmp, 0)) = 0 Then
            TabDemTeamleader.DeleteLine l
        End If
    Next l

    ' loading JODTAB data
    HandleSplashScreen "Loading JODTAB (connection between jobs and demands for teamleaders)...", bpHandleSplashScreen
    strTmp = TabDemTeamleader.SelectDistinct(0, "'", "'", ",", True)
    If Len(strTmp) > 0 Then
        strWhere = "WHERE UDEM IN (" & strTmp & ")"
        LoadData TabJodTeamleader, strWhere
    End If

    ' loading JOBTAB data
    HandleSplashScreen "Loading JOBTAB (teamleader jobs)...", bpHandleSplashScreen
    ilIdx = GetItemNo(TabJodTeamleader.LogicalFieldList, "UJOB") - 1
    strTmp = TabJodTeamleader.SelectDistinct(ilIdx, "'", "'", ",", True)
    If Len(strTmp) > 0 Then
        strWhere = "WHERE URNO IN (" & strTmp & ")"
        LoadData TabJobTeamleader, strWhere
    End If

    ' loading DLGTAB data
    HandleSplashScreen "Loading DLGTAB (teamleader delegations)...", bpHandleSplashScreen
    ilIdx = GetItemNo(TabJobTeamleader.LogicalFieldList, "JOUR") - 1
    strTmp = TabJobTeamleader.SelectDistinct(ilIdx, "'", "'", ",", True)
    If Len(strTmp) > 0 Then
        strWhere = "WHERE UJOB IN (" & strTmp & ")"
        LoadData TabDlgTeamleader, strWhere
    End If
    ilIdx = GetItemNo(TabDlgTeamleader.LogicalFieldList, "UJOB") - 1
    TabDlgTeamleader.IndexCreate "UJOB", ilIdx

    ' loading DRGTAB data
    Dim olFromSday As Date
    Dim olToSday As Date
    Dim strCfg As String
    HandleSplashScreen "Loading DRGTAB (teamleader daily delegations)...", bpHandleSplashScreen
    ' getting the STFUs
    ilIdx = GetItemNo(TabJobTeamleader.LogicalFieldList, "USTF") - 1
    strTmp = TabJobTeamleader.SelectDistinct(ilIdx, "'", "'", ",", True)
    If Len(strTmp) > 0 Then
        ' calculating the timeframe
        If (GetConfigEntry("JOB_LOAD_TIMEFRAME", strOrgUnit, strCfg) = False) Then
            MsgBoxNoCfgEntry "JOB_LOAD_TIMEFRAME"
        End If
        strFrom = GetItem(strCfg, 1, ",")
        strTo = GetItem(strCfg, 2, ",")
        olFromSday = DateAdd("h", CDbl(strFrom), Now) - 1
        olToSday = DateAdd("h", CDbl(strTo), Now) + 1
        strWhere = "WHERE STFU IN (" & strTmp & ") AND SDAY BETWEEN '" & Format(olFromSday, "YYYYMMDD") & _
            "' AND '" & Format(olToSday, "YYYYMMDD") & "'"
        LoadData TabDrgTeamleader, strWhere
    End If
    ilIdx = GetItemNo(TabDrgTeamleader.LogicalFieldList, "STFU") - 1
    TabDrgTeamleader.IndexCreate "STFU", ilIdx

    ' loading SWGTAB data
    If Len(strTmp) > 0 Then
        strWhere = "WHERE SURN IN (" & strTmp & ")"
        LoadData TabSwgTeamleader, strWhere
    End If

    ' loading DRRTAB data
    HandleSplashScreen "Loading DRRTAB (daily roster records of team leaders)...", bpHandleSplashScreen
    ilIdx = GetItemNo(TabJobTeamleader.LogicalFieldList, "UDSR") - 1
    strTmp = TabJobTeamleader.SelectDistinct(ilIdx, "'", "'", ",", True)
    If Len(strTmp) > 0 Then
        strWhere = "WHERE URNO IN (" & strTmp & ")"
        LoadData TabDrrTeamleader, strWhere
    End If
End Sub

Private Sub InitTabsTeamleader()
    InitTabGeneral TabDemTeamleader
    InitTabForCedaConnection TabDemTeamleader
    InitTabGeneral TabRpfTeamleader
    InitTabForCedaConnection TabRpfTeamleader
    InitTabGeneral TabJodTeamleader
    InitTabForCedaConnection TabJodTeamleader
    InitTabGeneral TabJobTeamleader
    InitTabForCedaConnection TabJobTeamleader
    InitTabGeneral TabDlgTeamleader
    InitTabForCedaConnection TabDlgTeamleader
    InitTabGeneral TabDrgTeamleader
    InitTabForCedaConnection TabDrgTeamleader
    InitTabGeneral TabSwgTeamleader
    InitTabForCedaConnection TabSwgTeamleader
    InitTabGeneral TabDrrTeamleader
    InitTabForCedaConnection TabDrrTeamleader
End Sub

Public Function GetTeamLeaderCodeByJob(sJobUrno As String) As String
    If gbTeamLeaderGroup = False Then
        GetTeamLeaderCodeByJob = ""
        Exit Function
    Else
        Dim strAftUrnos As String
        Dim strAftUrNo As String
        Dim strTmp As String
        Dim olTeamCollection As New Collection
        Dim i As Integer
        Dim ilIdx As Integer
        Dim strLineNo As String
        Dim strLine As String
        Dim strJobUtpl As String
        Dim strJobDety As String
        Dim strDemDety As String

        ' get the template of the job
        strLine = TabJOB.GetLinesByIndexValue("URNO", sJobUrno, 0)
        strJobUtpl = TabJOB.GetFieldValue(CLng(strLine), "UTPL")
        strJobDety = TabJOB.GetFieldValue(CLng(strLine), "DETY")

        ' get the AFT URNO
        strAftUrnos = TabJOB.GetFieldValue(CLng(strLine), "UAFT")
        If Len(strAftUrnos) < 5 Then
            GetTeamLeaderCodeByJob = ""
            Exit Function
        End If

        ' getting the demands belonging to the found demands
        strLineNo = ""
        If strJobDety = "0" Then
            ' it is a turnaround
            TabTemp.ResetContent
            ilIdx = GetItemNo(TabDemTeamleader.LogicalFieldList, "OURI") - 1
            strTmp = TabDemTeamleader.GetLinesByColumnValue(ilIdx, strAftUrnos, 0)
            TabTemp.InsertBuffer strTmp, ","
            ilIdx = GetItemNo(TabDemTeamleader.LogicalFieldList, "OURO") - 1
            strTmp = TabDemTeamleader.GetLinesByColumnValue(ilIdx, strAftUrnos, 0)
            TabTemp.InsertBuffer strTmp, ","
            strLineNo = TabTemp.SelectDistinct("0", "", "", ",", True)
' rro: this doesn't work because "GetLinesByMultipleColumnValue" wants ALL columns to be of that
'      value, not only one!
'            strAftUrnos = strAftUrnos & "," & strAftUrnos
'            strTmp = CStr(GetItemNo(TabDemTeamleader.LogicalFieldList, "OURI") - 1) & ","
'            strTmp = strTmp & CStr(GetItemNo(TabDemTeamleader.LogicalFieldList, "OURO") - 1)
'            strLineNo = TabDemTeamleader.GetLinesByMultipleColumnValue(strTmp, strAftUrnos, 0)
        Else
            If strJobDety = "1" Then
                ' it is an inbound
                ilIdx = GetItemNo(TabDemTeamleader.LogicalFieldList, "OURI") - 1
            ElseIf strJobDety = "2" Then
                ' it is an outbound
                ilIdx = GetItemNo(TabDemTeamleader.LogicalFieldList, "OURO") - 1
            End If
            For i = 0 To TabDemTeamleader.GetLineCount - 1
                If TabDemTeamleader.GetColumnValue(i, ilIdx) = strAftUrnos Then
                    strDemDety = TabDemTeamleader.GetFieldValue(i, "DETY")
                    If strDemDety = strJobDety Or strDemDety = "0" Then
                        strLineNo = "," & CStr(i)
                    End If
                End If
            Next i
            If Len(strLineNo) > 0 Then strLineNo = Mid(strLineNo, 2)
        End If

        ' looking for the JODs connecting demands and jobs
        strTmp = ""
        For i = 1 To ItemCount(strLineNo, ",")
            ' collecting DEM-Urnos to get the JOD-records
            strLine = GetItem(strLineNo, i, ",")
            If TabDemTeamleader.GetFieldValue(CLng(strLine), "UTPL") = strJobUtpl Then
                If Len(strTmp) > 0 Then strTmp = strTmp & ","
                strTmp = strTmp & TabDemTeamleader.GetColumnValue(CLng(strLine), 0)
            End If
        Next i
        ilIdx = GetItemNo(TabJodTeamleader.LogicalFieldList, "UDEM") - 1
        strLineNo = TabJodTeamleader.GetLinesByColumnValues(ilIdx, strTmp, 0)

        ' looking for the jobs covering the demands
        strTmp = ""
        For i = 1 To ItemCount(strLineNo, ",")
            ' collecting JOD-Ujobs to get the JOB-records
            If Len(strTmp) > 0 Then strTmp = strTmp & ","
            strTmp = strTmp & TabJodTeamleader.GetFieldValue(CLng(GetItem(strLineNo, i, ",")), "UJOB")
        Next i
        strLineNo = TabJobTeamleader.GetLinesByColumnValues(0, strTmp, 0)
        
        ' now we have the jobs, so that we can get the team codes
        Dim strLineNoTmp As String
        Dim strStfu As String
        Dim strSday As String
        For i = 1 To ItemCount(strLineNo, ",")
            '1. JOB.JOUR <-> DLG.UJOB
            strLine = GetItem(strLineNo, i, ",")
            strTmp = TabJobTeamleader.GetFieldValue(CLng(strLine), "JOUR")
            strLineNoTmp = TabDlgTeamleader.GetLinesByIndexValue("UJOB", strTmp, 0)
            If strLineNoTmp <> "" And IsNumeric(strLineNoTmp) = True Then
                ' found a delegation from OPSS-PM
                ilIdx = GetItemNo(TabDlgTeamleader.LogicalFieldList, "WGPC") - 1
                strTmp = TabDlgTeamleader.GetColumnValue(CLng(strLineNoTmp), ilIdx)
                If DoesKeyExist(olTeamCollection, strTmp) = False Then
                    olTeamCollection.Add strTmp, strTmp
                End If
            Else
                ' maybe we get a teamcode from DRGTAB or SWGTAB
                strTmp = TabJobTeamleader.GetFieldValue(CLng(strLine), "UDSR")
                strLineNoTmp = TabDrrTeamleader.GetLinesByColumnValue(0, strTmp, 0)
                If strLineNoTmp <> "" And IsNumeric(strLineNoTmp) = True Then
                    strSday = TabDrrTeamleader.GetFieldValue(strLineNoTmp, "SDAY")
                    strStfu = TabJobTeamleader.GetFieldValue(CLng(strLine), "USTF")
                    '2. & 3.: JOB.USTF <-> DRG.STFU, JOB.USTF <-> SWG.SURN
                    strTmp = GetTeamCodeByStfu(strSday, strStfu)
                    If strTmp <> "" And DoesKeyExist(olTeamCollection, strTmp) = False Then
                        olTeamCollection.Add strTmp, strTmp
                    End If
                End If
            End If
        Next i

        Dim strRet As String
        For i = 1 To olTeamCollection.count
            strRet = strRet & ";" & olTeamCollection.Item(i)
        Next i
        GetTeamLeaderCodeByJob = Mid(strRet, 2)
    End If
End Function

Private Function DoesKeyExist(ByRef refCollection As Collection, refKey As String) As Boolean
    On Error GoTo DoesNotExist
    refCollection.Item (refKey)
    DoesKeyExist = True
    Exit Function
DoesNotExist:
    DoesKeyExist = False
End Function

Private Function GetTeamCodeByStfu(sSday As String, sStfu As String) As String
    Dim strRet As String
    Dim strLineNo As String
    Dim strLine As String
    Dim i As Integer

    strLineNo = TabDrgTeamleader.GetLinesByIndexValue("STFU", sStfu, 0)
    For i = 1 To ItemCount(strLineNo, ",") Step 1
        strLine = GetItem(strLineNo, i, ",")
        If TabDrgTeamleader.GetFieldValue(CLng(strLine), "SDAY") = sSday Then
            strRet = TabDrgTeamleader.GetFieldValue(CLng(strLine), "WGPC")
        End If
    Next i

    If strRet = "" Then
        strRet = GetBestFitRecord(TabSwgTeamleader, sStfu, "SURN", "CODE", sSday)
    End If

    GetTeamCodeByStfu = strRet
End Function

Public Function UpdatePersonalInformation(sFName As String, sLName As String, sCPhone As String, sHPhone As String, sAddr As String) As Boolean
'    Testing for address with no comma
'    Dim strFieldList As String
'    Dim strData As String
'    Dim strWhere As String
'
'    TabSTF.SetFieldValues 0, "FINM,LANM,TELP,TELH,REMA", sFName & "," & sLName & "," & sCPhone & "," & sHPhone & "," & sAddr
'
'    ConnectUfisCom
'    strData = TabSTF.GetFieldValues(0, "FINM,LANM,TELP,TELH,REMA")
'    strData = strData & "," & gsUserName & "," & GetActualCedaUtcTime("YYYYMMDDhhmmss")
'    strFieldList = "FINM,LANM,TELP,TELH,REMA,USEU,LSTU"
'    strWhere = "WHERE URNO = '" & TabSTF.GetFieldValue(0, "URNO") & "'"
'    If frmSplash.UfisCom1.CallServer("URT", "STFTAB", strFieldList, strData, strWhere, "360") <> 0 Then
'        MsgBox "Failed to update the job status!", vbCritical, "Database error"
'    End If
'    DisconnetUfisCom
    
    Dim strFieldList As String
    Dim strData As String
    Dim strWhere As String

    sAddr = CleanString(sAddr, FOR_SERVER, True)
    TabSTF.SetFieldValues 0, "REMA", sAddr
    TabSTF.SetFieldValues 0, "FINM", sFName
    TabSTF.SetFieldValues 0, "LANM", sLName
    TabSTF.SetFieldValues 0, "TELP", sCPhone
    TabSTF.SetFieldValues 0, "TELH", sHPhone
    ConnectUfisCom
    strData = TabSTF.GetFieldValues(0, "FINM,LANM,TELP,TELH,REMA")
    strData = strData & "," & gsUserName & "," & GetActualCedaUtcTime("YYYYMMDDhhmmss")
    strFieldList = "FINM,LANM,TELP,TELH,REMA,USEU,LSTU"
    strWhere = "WHERE URNO = '" & TabSTF.GetFieldValue(0, "URNO") & "'"
    If frmSplash.UfisCom1.CallServer("URT", "STFTAB", strFieldList, strData, strWhere, "360") <> 0 Then
        MsgBox "Failed to update the job status!", vbCritical, "Database error"
    End If
    DisconnetUfisCom
    
    UpdatePersonalInformation = True
End Function

Public Function GetPersonalInformation() As String
    Dim sRet As String
    TabSTF.CedaAction "RT", "STFTAB", "FINM,LANM,TELP,TELH,REMA", sRet, "WHERE PENO='" & gsUserName & "'"
    sRet = TabSTF.GetFieldValues(0, "FINM,LANM,TELP,TELH,REMA")
    GetPersonalInformation = sRet
End Function
