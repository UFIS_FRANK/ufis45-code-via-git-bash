VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmSelectTemplate 
   Caption         =   "Select Department ..."
   ClientHeight    =   7455
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5865
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7455
   ScaleWidth      =   5865
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   465
      Left            =   2280
      TabIndex        =   2
      Top             =   6795
      Width           =   1230
   End
   Begin TABLib.TAB t 
      Height          =   6225
      Left            =   90
      TabIndex        =   0
      Top             =   360
      Width           =   5640
      _Version        =   65536
      _ExtentX        =   9948
      _ExtentY        =   10980
      _StockProps     =   64
   End
   Begin VB.Label Label1 
      Caption         =   "Please select your Department:"
      Height          =   285
      Left            =   90
      TabIndex        =   1
      Top             =   45
      Width           =   5640
   End
End
Attribute VB_Name = "frmSelectTemplate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public strTplUrno As String
' KKH - on 27/12/2006 added to get both A1 and A2 urno
Public strTplName As String
Public strTplNameTemp As String
Public strTplUrno2 As String
Public strTplUrno3 As String 'igu 25 May 2009, added to get PAX T1, PAX T2 and PRM

'-------------
'Date: 25 May 2009
'Desc: Get TplUrNo by TplName
'Modi: igu
'-------------
Private Function GetTplUrNoByTplName(t As TABLib.Tab, ByVal TplName As String) As String
    Dim strLineNo As String
    Dim lngLineNo As Long
    
    lngLineNo = -1
    strLineNo = t.GetLinesByColumnValue(1, TplName, 2)
    If IsNumeric(strLineNo) Then lngLineNo = CLng(strLineNo)
    If lngLineNo > -1 Then GetTplUrNoByTplName = t.GetFieldValue(lngLineNo, "URNO")
End Function

Private Sub cmdOK_Click()
    Dim llLine As Long
    strTplUrno2 = " "
    
    llLine = t.GetCurrentSelected
    If llLine > -1 Then
'-------------
'Date: 25 May 2009
'Desc: commented
'Modi: igu
'-------------
'        strTplUrno = t.GetFieldValue(llLine, "URNO")
'
'        ' KKH - on 27/12/2006 added to get both A1 and A2 urno
'        strTplName = t.GetFieldValue(llLine, "TNAM")
'        If strTplName = "Apron 1" Or strTplName = "Apron 2" Then
'            For llLine = 0 To t.GetLineCount
'                If strTplName = "Apron 1" Then
'                    If t.GetFieldValue(llLine, "TNAM") = "Apron 2" Then
'                        strTplNameTemp = "Apron 2"
'                        'llLineNo = llLine
'                        strTplUrno2 = t.GetFieldValue(llLine, "URNO")
'                    End If
'                Else
'                    If strTplName = "Apron 2" Then
'                        If t.GetFieldValue(llLine, "TNAM") = "Apron 1" Then
'                            strTplNameTemp = "Apron 1"
'                            'llLineNo = llLine
'                            strTplUrno2 = t.GetFieldValue(llLine, "URNO")
'                        End If
'                    End If
'                End If
'            Next llLine
'        End If
'        Me.Hide
'-------------

        '-------------
        'Date: 25 May 2009
        'Desc: call t_SendLButtonDblClick
        'Modi: igu
        '-------------
        Call t_SendLButtonDblClick(llLine, 0)
    Else
        MsgBox "Please select an entry in the list!!"
    End If
End Sub

Private Sub Form_Load()
    Dim strBuffer As String
    
    t.ResetContent
    t.HeaderString = "URNO,Name"
    t.LogicalFieldList = "URNO,TNAM"
    t.ColumnWidthString = "10,64"
    t.HeaderLengthString = "-1,400"
    
    strBuffer = frmData.tabTPL.GetBuffer(0, frmData.tabTPL.GetLineCount - 1, vbLf)
    t.InsertBuffer strBuffer, vbLf
    t.Sort "1", True, True
    t.SetCurrentSelection 0
End Sub

Private Sub t_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)

    ' KKH - on 27/12/2006 added to get both A1 and A2 urno
    Dim llLine As Long
    strTplUrno2 = " "
    strTplUrno = t.GetFieldValue(LineNo, "URNO")
    strTplName = t.GetFieldValue(LineNo, "TNAM")
    
'-------------
'Date: 25 May 2009
'Desc: commented
'Modi: igu
'-------------
'    If strTplName = "Apron 1" Or strTplName = "Apron 2" Then
'        For llLine = 0 To t.GetLineCount
'            If strTplName = "Apron 1" Then
'                If t.GetFieldValue(llLine, "TNAM") = "Apron 2" Then
'                    strTplNameTemp = "Apron 2"
'                    'llLineNo = llLine
'                    strTplUrno2 = t.GetFieldValue(llLine, "URNO")
'                End If
'            Else
'                If strTplName = "Apron 2" Then
'                    If t.GetFieldValue(llLine, "TNAM") = "Apron 1" Then
'                        strTplNameTemp = "Apron 1"
'                        'llLineNo = llLine
'                        strTplUrno2 = t.GetFieldValue(llLine, "URNO")
'                    End If
'                End If
'            End If
'        Next llLine
'    End If
    
    '-------------
    'Date: 25 May 2009
    'Desc: Apron 1, Apron 2 + Pax Service 1, Pax Service 2, or PRM
    'Modi: igu
    '-------------
    Dim strTplUrnoA As String
    Dim strTplUrnoB As String
    Dim strTplUrnoC As String
    
    strTplUrno3 = ""
    Select Case strTplName
        Case "Apron 1"
            strTplNameTemp = "Apron 2"
            strTplUrno2 = GetTplUrNoByTplName(t, strTplNameTemp)
        Case "Apron 2"
            strTplNameTemp = "Apron 1"
            strTplUrno2 = GetTplUrNoByTplName(t, strTplNameTemp)
        Case "PAX Service T1", "PAX Service T2", "PRM"
            strTplUrnoA = GetTplUrNoByTplName(t, "PAX Service T1")
            strTplUrnoB = GetTplUrNoByTplName(t, "PAX Service T2")
            strTplUrnoC = GetTplUrNoByTplName(t, "PRM")
            strTplUrno3 = strTplUrnoA & ";" & strTplUrnoB & ";" & strTplUrnoC
    End Select
    '-------------
    
    Me.Hide
End Sub
