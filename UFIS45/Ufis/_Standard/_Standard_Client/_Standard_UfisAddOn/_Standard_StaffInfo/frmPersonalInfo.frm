VERSION 5.00
Begin VB.Form frmPersonalInfo 
   Caption         =   "Personal Information"
   ClientHeight    =   2595
   ClientLeft      =   60
   ClientTop       =   375
   ClientWidth     =   8805
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2595
   ScaleWidth      =   8805
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtAddress 
      Height          =   350
      Left            =   1890
      TabIndex        =   8
      Top             =   1680
      Width           =   6795
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5580
      TabIndex        =   9
      Top             =   2160
      Width           =   1515
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7200
      TabIndex        =   10
      Top             =   2160
      Width           =   1485
   End
   Begin VB.TextBox txtHPhone 
      Height          =   350
      Left            =   1890
      TabIndex        =   7
      Top             =   1300
      Width           =   2475
   End
   Begin VB.TextBox txtCPhone 
      Height          =   350
      Left            =   1890
      TabIndex        =   6
      Top             =   900
      Width           =   2475
   End
   Begin VB.TextBox txtLName 
      Height          =   350
      Left            =   1890
      TabIndex        =   5
      Top             =   500
      Width           =   5385
   End
   Begin VB.TextBox txtFName 
      Height          =   350
      Left            =   1890
      TabIndex        =   4
      Top             =   100
      Width           =   6765
   End
   Begin VB.Label Label5 
      Caption         =   "Address"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   570
      TabIndex        =   11
      Top             =   1710
      Width           =   1125
   End
   Begin VB.Label Label4 
      Caption         =   "Home Phone"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   120
      TabIndex        =   3
      Top             =   1320
      Width           =   1605
   End
   Begin VB.Label Label3 
      Caption         =   "Cell Phone"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   150
      TabIndex        =   2
      Top             =   900
      Width           =   1605
   End
   Begin VB.Label Label2 
      Caption         =   "Last Name"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   270
      TabIndex        =   1
      Top             =   510
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "First Name"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   0
      Top             =   150
      Width           =   1635
   End
End
Attribute VB_Name = "frmPersonalInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PCX As Integer
Public PCY As Integer

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdOK_Click()
    Dim bRet As Boolean
    bRet = frmData.UpdatePersonalInformation(txtFName.Text, txtLName.Text, txtCPhone.Text, txtHPhone.Text, txtAddress.Text)
    Unload Me
End Sub

Private Sub Form_Load()
    Dim strRet As String
    Dim strRets() As String
    
    Top = PCY - (Height / 2)
    Left = PCX - (Width / 2)
    
    strRet = frmData.GetPersonalInformation()
    strRets = Split(strRet, ",")
    
    txtFName.Text = strRets(0)
    txtLName.Text = strRets(1)
    txtCPhone.Text = strRets(2)
    txtHPhone.Text = strRets(3)
    txtAddress.Text = CleanString(strRets(4), FOR_CLIENT, True)
End Sub
