﻿namespace UAM
{
    partial class frmAdminTool
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gBoxGroup = new System.Windows.Forms.GroupBox();
            this.lstBoxGroup = new System.Windows.Forms.ListBox();
            this.gBoxUser = new System.Windows.Forms.GroupBox();
            this.lstBoxUser = new System.Windows.Forms.ListBox();
            this.gBoxDepartment = new System.Windows.Forms.GroupBox();
            this.chkLstDeparment = new System.Windows.Forms.CheckedListBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gBoxGroup.SuspendLayout();
            this.gBoxUser.SuspendLayout();
            this.gBoxDepartment.SuspendLayout();
            this.SuspendLayout();
            // 
            // gBoxGroup
            // 
            this.gBoxGroup.Controls.Add(this.lstBoxGroup);
            this.gBoxGroup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.gBoxGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gBoxGroup.Location = new System.Drawing.Point(12, 25);
            this.gBoxGroup.Name = "gBoxGroup";
            this.gBoxGroup.Size = new System.Drawing.Size(376, 208);
            this.gBoxGroup.TabIndex = 5;
            this.gBoxGroup.TabStop = false;
            this.gBoxGroup.Text = "List of Groups :";
            // 
            // lstBoxGroup
            // 
            this.lstBoxGroup.FormattingEnabled = true;
            this.lstBoxGroup.Location = new System.Drawing.Point(6, 13);
            this.lstBoxGroup.Name = "lstBoxGroup";
            this.lstBoxGroup.Size = new System.Drawing.Size(364, 186);
            this.lstBoxGroup.TabIndex = 0;
            this.lstBoxGroup.Click += new System.EventHandler(this.lstBoxGroup_Click);
            // 
            // gBoxUser
            // 
            this.gBoxUser.Controls.Add(this.lstBoxUser);
            this.gBoxUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gBoxUser.Location = new System.Drawing.Point(12, 252);
            this.gBoxUser.Name = "gBoxUser";
            this.gBoxUser.Size = new System.Drawing.Size(376, 208);
            this.gBoxUser.TabIndex = 6;
            this.gBoxUser.TabStop = false;
            this.gBoxUser.Text = "List of User:";
            // 
            // lstBoxUser
            // 
            this.lstBoxUser.FormattingEnabled = true;
            this.lstBoxUser.Location = new System.Drawing.Point(6, 15);
            this.lstBoxUser.Name = "lstBoxUser";
            this.lstBoxUser.Size = new System.Drawing.Size(364, 186);
            this.lstBoxUser.TabIndex = 1;
            // 
            // gBoxDepartment
            // 
            this.gBoxDepartment.Controls.Add(this.chkLstDeparment);
            this.gBoxDepartment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gBoxDepartment.Location = new System.Drawing.Point(414, 25);
            this.gBoxDepartment.Name = "gBoxDepartment";
            this.gBoxDepartment.Size = new System.Drawing.Size(376, 435);
            this.gBoxDepartment.TabIndex = 7;
            this.gBoxDepartment.TabStop = false;
            this.gBoxDepartment.Tag = "";
            this.gBoxDepartment.Text = "List of Departments :";
            // 
            // chkLstDeparment
            // 
            this.chkLstDeparment.FormattingEnabled = true;
            this.chkLstDeparment.Location = new System.Drawing.Point(6, 19);
            this.chkLstDeparment.Name = "chkLstDeparment";
            this.chkLstDeparment.Size = new System.Drawing.Size(364, 409);
            this.chkLstDeparment.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(668, 478);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(72, 27);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(583, 478);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(57, 27);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // frmAdminTool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 525);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.gBoxDepartment);
            this.Controls.Add(this.gBoxUser);
            this.Controls.Add(this.gBoxGroup);
            this.MaximizeBox = false;
            this.Name = "frmAdminTool";
            this.Text = "User Account Manager (Admin Tool)";
            this.Load += new System.EventHandler(this.frmAdminTool_Load);
            this.gBoxGroup.ResumeLayout(false);
            this.gBoxUser.ResumeLayout(false);
            this.gBoxDepartment.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gBoxGroup;
        private System.Windows.Forms.GroupBox gBoxUser;
        private System.Windows.Forms.GroupBox gBoxDepartment;
        private System.Windows.Forms.CheckedListBox chkLstDeparment;
        private System.Windows.Forms.ListBox lstBoxGroup;
        private System.Windows.Forms.ListBox lstBoxUser;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
    }
}