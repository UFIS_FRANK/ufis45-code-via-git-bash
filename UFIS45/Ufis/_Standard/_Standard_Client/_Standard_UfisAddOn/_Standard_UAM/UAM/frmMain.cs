﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Ufis.Utils;
using Ufis.Data;
using UAM.Map;
using UAM.Ctrl;

namespace UAM
{
    public partial class frmMain : Form
    {
        #region My Members
        /// <summary>
        /// The one and only IDatabase member reference within 
        /// the application. This is reused in all other modules.
        /// </summary>
        private IDatabase myDB;
        private string isApplicationPVG = "-";
        public string[] args;
        #endregion My Members
        private ITable myGRP;
        private ITable myUAA;
        private ITable mySEC;
        private ITable mySOR;
        private ITable myUAL;
        private ITable myORG;
        List<BindList> bindList;
        List<string> bindDistinct;
        private AxAATLOGINLib.AxAatLogin LoginControl;
        private CtrlAdmin objAdmin;
        private string strUserID = string.Empty;
        private bool bUserPrivilege = false;
        private bool bActiveDeactive = false;

       public frmMain()
        {
            InitializeComponent();
        }

        [STAThread]
        static void Main(string[] args)
        {
            frmMain olDlg = new frmMain();
            olDlg.args = args;
            Application.Run(olDlg);
        }
        private void frmMain_Load(object sender, EventArgs e)
        {

            if (!LoginProcedure())
            {
                Application.Exit();
            }

            string strUTCConvertion = string.Empty;
            Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
            UT.ServerName = myIni.IniReadValue("GLOBAL", "HOSTNAME");
            UT.Hopo = myIni.IniReadValue("GLOBAL", "HOMEAIRPORT");
            strUTCConvertion = myIni.IniReadValue("GLOBAL", "UTCCONVERTIONS");

            if (UT.UserName.Length == 0)
                UT.UserName = LoginControl.GetUserName();

            isApplicationPVG = LoginControl.GetPrivileges("B_ADMIN_TOOL");


            if (isApplicationPVG == "1")
                btnAdminTool.Enabled = true;
            else if (isApplicationPVG == "0")
                btnAdminTool.Enabled = false;
            else
                btnAdminTool.Visible = false;

            if (strUTCConvertion == "NO")
            {
                UT.UTCOffset = 0;
            }
            else
            {
                DateTime datLocal = DateTime.Now;
                DateTime datUTC = DateTime.UtcNow;
                TimeSpan span = datLocal - datUTC;

                UT.UTCOffset = (short)span.Hours;
            }
            try
            {
                myDB = UT.GetMemDB();
                bool result = myDB.Connect(UT.ServerName, UT.UserName, UT.Hopo, "TAB", "UAM");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Connecting to Server Failed!", "Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

            UT.IsTimeInUtc = false;
            panelUserDetail.Visible = false;
            objAdmin = CtrlAdmin.GetInstance();
            FillPrivilegeDepartment();
        }

        private void FillPrivilegeDepartment()
        {
            string strUserUrno = string.Empty;
            string strGroupUrno = string.Empty;

            mySEC = objAdmin.LoadSECData("WHERE USID = ('" + UT.UserName + "') AND TYPE = 'U' AND STAT = '1'");

            if (mySEC.Count > 0)
            {
                for (int i = 0; i < mySEC.Count; i++)
                {
                    if (i == 0)
                        strUserUrno = mySEC[i]["URNO"];
                    else
                        strUserUrno += "," + mySEC[i]["URNO"];
                }

                strUserUrno = strUserUrno.Replace(",", "','");

                myGRP = objAdmin.LoadGRPData("WHERE FSEC IN ( '" + strUserUrno + "' ) AND TYPE ='G' AND STAT = '1'");

                if (myGRP.Count > 0)
                {
                    for (int i = 0; i < myGRP.Count; i++)
                    {
                        if (i == 0)
                            strGroupUrno = myGRP[i]["FREL"];
                        else
                            strGroupUrno += "," + myGRP[i]["FREL"];
                    }
                }
                strGroupUrno = strGroupUrno.Replace(",", "','");

                myUAA = objAdmin.LoadUAAData("WHERE GRPU IN ('" + strGroupUrno + "' ) ORDER BY DPT1");

                myORG = objAdmin.LoadORGData();

                if (myUAA.Count > 0 && myORG.Count > 0)
                {
                    bindList = new List<BindList>();
                    bindDistinct = new List<string>();
                    for (int i = 0; i < myUAA.Count; i++)
                    {
                        BindList distNonList = new BindList();

                        if (myUAA[i]["DPT1"] == "-")
                        {
                            if (!bindDistinct.Contains("-"))
                            {
                                bindDistinct.Add("-");
                                bindList.Add(new BindList { GRPU = myUAA[i]["GRPU"], DPT1 = "-", DPT2 = "Non Staff" });
                                continue;
                            }
                        }

                        for (int j = 0; j < myORG.Count; j++)
                        {
                            if (myUAA[i]["DPT1"] == myORG[j]["DPT1"])
                            {
                                if (!bindDistinct.Contains(myUAA[i]["DPT1"]))
                                {
                                    bindDistinct.Add(myUAA[i]["DPT1"]);
                                    bindList.Add(new BindList { GRPU = myUAA[i]["GRPU"], DPT1 = myUAA[i]["DPT1"], DPT2 = myUAA[i]["DPT1"] + " - " + myORG[j]["DPTN"] });
                                }
                            }
                        }
                    }

                    lstBoxDepartment.DataSource = bindList;
                    lstBoxDepartment.DisplayMember = "DPT2";
                    lstBoxDepartment.ValueMember = "GRPU";
                    gBoxUserSearch.Visible = true;
                }
                else
                {
                    gBoxUserSearch.Visible = false;
                }
            }
        }

        private void btnAdminTool_Click(object sender, EventArgs e)
        {
            frmAdminTool frmSIN = new frmAdminTool();
            frmSIN.ShowDialog();
            if (frmSIN.bSaveOrCancel)
            {
                FillPrivilegeDepartment();
            }
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            FindUser();
        }

        private bool FindUser()
        {
            strUserID = txtUserID.Text.Trim();

            if (strUserID != "")
            {
                if (UT.UserName != strUserID)
                {
                    mySEC = objAdmin.LoadSECData("WHERE USID = ('" + strUserID + "') AND TYPE = 'U'");

                    if (mySEC.Count > 0)
                    {
                        for (int i = 0; i < mySEC.Count; i++)
                        {
                            txtFullName.Text = CedaUtils.ConvertStringForClient(mySEC[i]["NAME"]);
                            if (mySEC[i]["STAT"] == "0")
                                bActiveDeactive = false;
                            else
                                bActiveDeactive = true;
                        }

                        mySOR = objAdmin.LoadSORData("WHERE SURN IN (SELECT URNO FROM STFTAB WHERE PENO = '" + strUserID + "' )  AND (VPTO=' ' OR VPTO > '" + UT.DateTimeToCeda(DateTime.Now) + "')");

                        if (mySOR.Count > 0)
                        {
                            //Processing for Staff
                            for (int i = 0; i < mySOR.Count; i++)
                            {
                                foreach (BindList dList in bindList)
                                {
                                    if (dList.DPT1 == mySOR[i]["CODE"])
                                    {
                                        bUserPrivilege = true;
                                        break;
                                    }
                                    else
                                    {
                                        bUserPrivilege = false;
                                        continue;
                                    }
                                }
                            }
                            if (bUserPrivilege)
                            {
                                if (bActiveDeactive)
                                    btnActivate.Text = "&Deactivate";
                                else
                                    btnActivate.Text = "&Activate";

                                panelUserDetail.Visible = true;

                                CreateLogResult();
                            }
                            else
                            {
                                MessageBox.Show(this, "No right to access user account", "Invalid User", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                ClearResult();
                            }
                        }
                        else
                        {
                            //Processing for NON Staff 
                            foreach (BindList dList in bindList)
                            {
                                if (dList.DPT1 == "-")
                                {
                                    bUserPrivilege = true;
                                    break;
                                }
                                else
                                {
                                    bUserPrivilege = false;
                                    continue;
                                }
                            }
                            if (bUserPrivilege)
                            {
                                if (bActiveDeactive)
                                    btnActivate.Text = "&Deactivate";
                                else
                                    btnActivate.Text = "&Activate";

                                panelUserDetail.Visible = true;
                                CreateLogResult();
                            }
                            else
                            {
                                MessageBox.Show(this, "No right to access user account", "Invalid User", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                ClearResult();
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show(this, "No right to access user account", "Invalid User", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        ClearResult();
                    }
                }
                else
                {
                    MessageBox.Show(this, "User cannot manage their own account!", "User ID", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    ClearResult();
                }
            }
            else
            {
                MessageBox.Show(this, "Please enter User Personal Number!", "User ID", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                ClearResult();
            }
            return bUserPrivilege;
        }

        private void CreateLogResult()
        {
            lBoxResult.Items.Clear();
            lBoxResult.Items.Add(" ");

            myUAL = objAdmin.LoadUALData("WHERE EVCT IN (SELECT  MAX(EVCT) FROM UALTAB WHERE EVDE='RESET' AND USID='" + strUserID + "') AND  EVDE='RESET' AND  USID='" + strUserID + "'");
            if (myUAL.Count > 0)
            {
                lBoxResult.Items.Add("Number of times password reset : " + myUAL[0]["EVCT"]);
                lBoxResult.Items.Add("Password reset by : " + myUAL[0]["USEC"]);
                lBoxResult.Items.Add("Password reset date: " + UT.CedaDateToDateTime(myUAL[0]["CDAT"]));
                lBoxResult.Items.Add("Remarks of password reset :" + CedaUtils.ConvertStringForClient(myUAL[0]["RMRK"]));
                lBoxResult.Items.Add(" ");
            }
            else
            {
                lBoxResult.Items.Add("No Log details Found for Password reset");
                lBoxResult.Items.Add(" ");
            }
            myUAL = objAdmin.LoadUALData("WHERE EVCT IN (SELECT  MAX(EVCT) FROM UALTAB WHERE EVDE='ACT' AND USID='" + strUserID + "') AND  EVDE='ACT' AND  USID='" + strUserID + "'");
            if (myUAL.Count > 0)
            {
                lBoxResult.Items.Add("Number of times account activated : " + myUAL[0]["EVCT"]);
                lBoxResult.Items.Add("Account activated by : " + myUAL[0]["USEC"]);
                lBoxResult.Items.Add("Account activated date : " + UT.CedaDateToDateTime(myUAL[0]["CDAT"]));
                lBoxResult.Items.Add("Remarks of account activation :" + CedaUtils.ConvertStringForClient(myUAL[0]["RMRK"]));
                lBoxResult.Items.Add(" ");
            }
            else
            {
                lBoxResult.Items.Add("No Log details Found for Account activation");
                lBoxResult.Items.Add(" ");
            }

            myUAL = objAdmin.LoadUALData("WHERE EVCT IN (SELECT  MAX(EVCT) FROM UALTAB WHERE EVDE='DEACT' AND USID='" + strUserID + "') AND  EVDE='DEACT' AND  USID='" + strUserID + "'");
            if (myUAL.Count > 0)
            {
                lBoxResult.Items.Add("Number of times account deactivated : " + myUAL[0]["EVCT"]);
                lBoxResult.Items.Add("Account deactivated by : " + myUAL[0]["USEC"]);
                lBoxResult.Items.Add("Account deactivated date : " + UT.CedaDateToDateTime(myUAL[0]["CDAT"]));
                lBoxResult.Items.Add("Remarks of account deactivation :" + CedaUtils.ConvertStringForClient(myUAL[0]["RMRK"]));
                lBoxResult.Items.Add(" ");
            }
            else
            {
                lBoxResult.Items.Add("No Log details Found for Account deactivation");
                lBoxResult.Items.Add(" ");
            }
        }

        private void btnActivate_Click(object sender, EventArgs e)
        {
            frmConfirm frm;

            if (FindUser())
            {
                if (bActiveDeactive)
                    frm = new frmConfirm(txtUserID.Text, txtFullName.Text, "D", mySEC);
                else
                    frm = new frmConfirm(txtUserID.Text, txtFullName.Text, "A", mySEC);

                frm.ShowDialog();

                if (frm.bSaveOrCancel)
                {
                    ClearResult();
                }
            }
        }

        private void btnResetPwd_Click(object sender, EventArgs e)
        {
            frmConfirm frm;
            if (FindUser())
            {
                frm = new frmConfirm(txtUserID.Text, txtFullName.Text, "R", mySEC);
                frm.ShowDialog();
                if (frm.bSaveOrCancel)
                {
                    ClearResult();
                }
            }
        }

        private void ClearResult()
        {
            txtUserID.Text = string.Empty;
            txtFullName.Text = string.Empty;
            lBoxResult.Items.Clear();
            panelUserDetail.Visible = false;
            bUserPrivilege = false;
            txtUserID.Focus();
        }

        private void txtUserID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnFind_Click(sender, e);
        }
        /// <summary>
        /// This function pops up the login dialog and opens the application or closes the application
        /// depending on the user rights.This is done for the PRF 8523
        /// </summary>
        /// <returns></returns>
        private bool LoginProcedure()
        {
            string LoginAnsw = "";
            string tmpRegStrg = "";
            string strRet = "";

            LoginControl.ApplicationName = "UAM";
            LoginControl.VersionString = Application.ProductVersion;
            LoginControl.InfoCaption = "Info about User Account Manager";
            LoginControl.InfoButtonVisible = true;
            LoginControl.InfoUfisVersion = "Ufis Version 4.5";
            LoginControl.InfoAppVersion = "UAM " + Application.ProductVersion.ToString();
            LoginControl.InfoAAT = "UFIS Airport Solutions GmbH";
            LoginControl.UserNameLCase = true;

            tmpRegStrg = "UAM" + ",";
            tmpRegStrg += "InitModu,InitModu,Initialisieren (InitModu),B,-" +
                ",Buttons,B_ADMIN_TOOL,Admin Button,B,-";

            LoginControl.RegisterApplicationString = tmpRegStrg;

            if (args.Length == 0)
            {
                strRet = LoginControl.ShowLoginDialog();
            }
            else
            {
                string userId = args[0];
                string password = "";
                bool hasUserIdNPwd = false;
                string[] stParams = args[0].Split('|');
                if (stParams.Length >= 4)
                {
                    string[] stTemps = stParams[1].Split(',');
                    userId = stTemps[0];
                    password = stParams[3];
                    hasUserIdNPwd = true;
                }
                else if (args.Length == 2)
                {
                    password = args[1];
                    hasUserIdNPwd = true;
                }
                if (hasUserIdNPwd)
                {
                    strRet = LoginControl.DoLoginSilentMode(userId, password);
                }
                if (strRet != "OK")
                    strRet = LoginControl.ShowLoginDialog();
            }

            LoginAnsw = LoginControl.GetPrivileges("InitModu");
            if (LoginAnsw != "" && strRet != "CANCEL")
            {
                return true;
            }
            else
                return false;
        }

        private void butExit_Click(object sender, EventArgs e)
        {
            DialogResult dlgRes = MessageBox.Show("Are you sure, you want to terminate the application", "Application Close", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dlgRes == DialogResult.Yes) { Application.Exit(); }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult dlgRes = MessageBox.Show("Are you sure, you want to terminate the application", "Application Close", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlgRes == DialogResult.Yes) Application.Exit();
                else e.Cancel = true;
            }
        }
    }
}
