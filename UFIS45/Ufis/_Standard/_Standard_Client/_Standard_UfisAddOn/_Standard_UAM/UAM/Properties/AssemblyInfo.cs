﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("UAM")] 
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("UFIS Airport Solutions GmbH")]
[assembly: AssemblyProduct("UFIS 4.6")]
[assembly: AssemblyCopyright("© 2005, 2008 UFIS Airport Solutions GmbH")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")] 

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")] 
[assembly: AssemblyVersion("4.6.1.0")]