﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Ufis.Data;
using Ufis.Utils;
using UAM.Ctrl;

namespace UAM
{
    public partial class frmConfirm : Form
    {
        private ITable _mySEC;
        private CtrlAdmin objAdmin;
        private string _strUserID = string.Empty;
        private string _strUserName = string.Empty;
        private string _strCaption = string.Empty;
        public bool bSaveOrCancel = false;
        ToolTip toolTip = new ToolTip();

        public frmConfirm(string strUserID, string strUserName, string strCaption, ITable mySEC)
        {
            InitializeComponent();
            _strUserID = strUserID;
            _strUserName = strUserName;
            _strCaption = strCaption;
            _mySEC = mySEC;
            FillInitValue();
        }

        private void FillInitValue()
        {
            txtUserIDValue.Text = _strUserID;
            txtFullName.Text = _strUserName;
            if (_strCaption == "A")
                lblMessage.Text = " Do you really want to Activate* the following user:";
            else if (_strCaption == "D")
                lblMessage.Text = " Do you really want to Deactivate* the following user:";
            else
                lblMessage.Text = " Do you really want to Reset* the password for the following user:";
        }

        private void frmConfirm_Load(object sender, EventArgs e)
        {
            objAdmin = CtrlAdmin.GetInstance();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (_strUserID != string.Empty)
            {
                if (_mySEC.Count == 1)
                {
                    if (_strCaption == "A" || _strCaption == "D")
                    {
                        IRow rowSEC = _mySEC[0];
                        if (_strCaption == "A")
                            rowSEC["STAT"] = "1";
                        else if (_strCaption == "D")
                            rowSEC["STAT"] = "0";
                        rowSEC["HOPO"] = UT.Hopo;
                        rowSEC["REMA"] = CedaUtils.ConvertStringForServer(txtRemarks.Text.Replace("\r\n", ""));
                        rowSEC.Status = State.Modified;
                        CtrlAdmin.myDB.Writer.CallServer("SEC", "TAB", "UAM", "URNO,USID,NAME,STAT,LANG,REMA", rowSEC["URNO"] + "," + rowSEC["USID"] + "," + CedaUtils.ConvertStringForServer(rowSEC["NAME"]) + "," + rowSEC["STAT"] + "," + rowSEC["LANG"] + "," + rowSEC["REMA"], "WHERE UPDSEC", "200");

                        if (_strCaption == "A")
                            MessageBox.Show(this, "User Account Activated successfully!", "Account Activated", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        else if (_strCaption == "D")
                            MessageBox.Show(this, "User Account Deactivated successfully!", "Account Deactivated", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        bSaveOrCancel = true;
                        this.Close();
                    }
                    else
                    {
                        IRow rowSEC = _mySEC[0];
                        rowSEC["PASS"] = "Password";
                        rowSEC["REMA"] = CedaUtils.ConvertStringForServer(txtRemarks.Text.Replace("\r\n", ""));
                        rowSEC.Status = State.Modified;
                        CtrlAdmin.myDB.Writer.CallServer("SEC", "TAB", "UAM", "URNO,PASS,REMA", rowSEC["URNO"] + "," + rowSEC["PASS"] + "," + rowSEC["REMA"], "WHERE SETPWD", "200");

                        MessageBox.Show(this, "New Password is :  Password", "Password Reset successfully!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //MessageBox.Show(this, "Password Reset successfully!", "Password Reset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        bSaveOrCancel = true;
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show(this, "No right to access user account", "Invalid User", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    bSaveOrCancel = false;
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show(this, "No right to access user account", "Invalid User", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                bSaveOrCancel = false;
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            bSaveOrCancel = false;
            this.Close();
        }

        private void txtRemarks_MouseMove(object sender, MouseEventArgs e)
        { 
            toolTip.Show("Maximum 127 Character only!", txtRemarks, 200000);
        }
    }
}
