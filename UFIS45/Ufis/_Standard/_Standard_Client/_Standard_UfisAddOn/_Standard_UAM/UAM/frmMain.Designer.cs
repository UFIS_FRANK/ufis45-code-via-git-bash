﻿namespace UAM
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.btnAdminTool = new System.Windows.Forms.Button();
            this.lblUserID = new System.Windows.Forms.Label();
            this.txtUserID = new System.Windows.Forms.TextBox();
            this.btnFind = new System.Windows.Forms.Button();
            this.panelUserDetail = new System.Windows.Forms.Panel();
            this.btnResetPwd = new System.Windows.Forms.Button();
            this.btnActivate = new System.Windows.Forms.Button();
            this.gBoxLog = new System.Windows.Forms.GroupBox();
            this.txtFullName = new System.Windows.Forms.TextBox();
            this.lblFullName = new System.Windows.Forms.Label();
            this.lBoxResult = new System.Windows.Forms.ListBox();
            this.gBoxDepartment = new System.Windows.Forms.GroupBox();
            this.lstBoxDepartment = new System.Windows.Forms.ListBox();
            this.LoginControl = new AxAATLOGINLib.AxAatLogin();
            this.gBoxUserSearch = new System.Windows.Forms.GroupBox();
            this.butExit = new System.Windows.Forms.Button();
            this.panelUserDetail.SuspendLayout();
            this.gBoxLog.SuspendLayout();
            this.gBoxDepartment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LoginControl)).BeginInit();
            this.gBoxUserSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAdminTool
            // 
            this.btnAdminTool.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdminTool.Location = new System.Drawing.Point(32, 20);
            this.btnAdminTool.Name = "btnAdminTool";
            this.btnAdminTool.Size = new System.Drawing.Size(97, 25);
            this.btnAdminTool.TabIndex = 0;
            this.btnAdminTool.Text = "Admin &Tool";
            this.btnAdminTool.UseVisualStyleBackColor = true;
            this.btnAdminTool.Click += new System.EventHandler(this.btnAdminTool_Click);
            // 
            // lblUserID
            // 
            this.lblUserID.AutoSize = true;
            this.lblUserID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserID.Location = new System.Drawing.Point(29, 28);
            this.lblUserID.Name = "lblUserID";
            this.lblUserID.Size = new System.Drawing.Size(58, 13);
            this.lblUserID.TabIndex = 10;
            this.lblUserID.Text = "User ID :";
            // 
            // txtUserID
            // 
            this.txtUserID.Location = new System.Drawing.Point(119, 25);
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Size = new System.Drawing.Size(200, 20);
            this.txtUserID.TabIndex = 1;
            this.txtUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUserID_KeyDown);
            // 
            // btnFind
            // 
            this.btnFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFind.Location = new System.Drawing.Point(330, 22);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(78, 25);
            this.btnFind.TabIndex = 2;
            this.btnFind.Text = "&Find";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // panelUserDetail
            // 
            this.panelUserDetail.Controls.Add(this.btnResetPwd);
            this.panelUserDetail.Controls.Add(this.btnActivate);
            this.panelUserDetail.Controls.Add(this.gBoxLog);
            this.panelUserDetail.Location = new System.Drawing.Point(357, 90);
            this.panelUserDetail.Name = "panelUserDetail";
            this.panelUserDetail.Size = new System.Drawing.Size(433, 382);
            this.panelUserDetail.TabIndex = 11;
            // 
            // btnResetPwd
            // 
            this.btnResetPwd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResetPwd.Location = new System.Drawing.Point(252, 329);
            this.btnResetPwd.Name = "btnResetPwd";
            this.btnResetPwd.Size = new System.Drawing.Size(122, 25);
            this.btnResetPwd.TabIndex = 6;
            this.btnResetPwd.Text = "&Reset Password";
            this.btnResetPwd.UseVisualStyleBackColor = true;
            this.btnResetPwd.Click += new System.EventHandler(this.btnResetPwd_Click);
            // 
            // btnActivate
            // 
            this.btnActivate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActivate.Location = new System.Drawing.Point(68, 329);
            this.btnActivate.Name = "btnActivate";
            this.btnActivate.Size = new System.Drawing.Size(110, 25);
            this.btnActivate.TabIndex = 5;
            this.btnActivate.Text = "&Activate";
            this.btnActivate.UseVisualStyleBackColor = true;
            this.btnActivate.Click += new System.EventHandler(this.btnActivate_Click);
            // 
            // gBoxLog
            // 
            this.gBoxLog.Controls.Add(this.txtFullName);
            this.gBoxLog.Controls.Add(this.lblFullName);
            this.gBoxLog.Controls.Add(this.lBoxResult);
            this.gBoxLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gBoxLog.Location = new System.Drawing.Point(3, 0);
            this.gBoxLog.Name = "gBoxLog";
            this.gBoxLog.Size = new System.Drawing.Size(430, 314);
            this.gBoxLog.TabIndex = 12;
            this.gBoxLog.TabStop = false;
            this.gBoxLog.Text = "Logs :";
            // 
            // txtFullName
            // 
            this.txtFullName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFullName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFullName.Location = new System.Drawing.Point(116, 30);
            this.txtFullName.Name = "txtFullName";
            this.txtFullName.ReadOnly = true;
            this.txtFullName.Size = new System.Drawing.Size(200, 20);
            this.txtFullName.TabIndex = 3;
            // 
            // lblFullName
            // 
            this.lblFullName.AutoSize = true;
            this.lblFullName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFullName.Location = new System.Drawing.Point(26, 33);
            this.lblFullName.Name = "lblFullName";
            this.lblFullName.Size = new System.Drawing.Size(71, 13);
            this.lblFullName.TabIndex = 14;
            this.lblFullName.Text = "Full Name :";
            // 
            // lBoxResult
            // 
            this.lBoxResult.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lBoxResult.FormattingEnabled = true;
            this.lBoxResult.HorizontalExtent = 950;
            this.lBoxResult.HorizontalScrollbar = true;
            this.lBoxResult.Location = new System.Drawing.Point(26, 70);
            this.lBoxResult.Name = "lBoxResult";
            this.lBoxResult.Size = new System.Drawing.Size(379, 238);
            this.lBoxResult.TabIndex = 15;
            // 
            // gBoxDepartment
            // 
            this.gBoxDepartment.Controls.Add(this.lstBoxDepartment);
            this.gBoxDepartment.Controls.Add(this.LoginControl);
            this.gBoxDepartment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gBoxDepartment.Location = new System.Drawing.Point(12, 90);
            this.gBoxDepartment.Name = "gBoxDepartment";
            this.gBoxDepartment.Size = new System.Drawing.Size(330, 382);
            this.gBoxDepartment.TabIndex = 16;
            this.gBoxDepartment.TabStop = false;
            this.gBoxDepartment.Text = " List of Departments :   ";
            // 
            // lstBoxDepartment
            // 
            this.lstBoxDepartment.FormattingEnabled = true;
            this.lstBoxDepartment.Location = new System.Drawing.Point(3, 19);
            this.lstBoxDepartment.Name = "lstBoxDepartment";
            this.lstBoxDepartment.Size = new System.Drawing.Size(324, 394);
            this.lstBoxDepartment.TabIndex = 17;
            // 
            // LoginControl
            // 
            this.LoginControl.Enabled = true;
            this.LoginControl.Location = new System.Drawing.Point(0, 12);
            this.LoginControl.Name = "LoginControl";
            this.LoginControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("LoginControl.OcxState")));
            this.LoginControl.RightToLeft = false;
            this.LoginControl.Size = new System.Drawing.Size(100, 38);
            this.LoginControl.TabIndex = 18;
            this.LoginControl.Text = null;
            this.LoginControl.Visible = false;
            // 
            // gBoxUserSearch
            // 
            this.gBoxUserSearch.Controls.Add(this.lblUserID);
            this.gBoxUserSearch.Controls.Add(this.txtUserID);
            this.gBoxUserSearch.Controls.Add(this.btnFind);
            this.gBoxUserSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gBoxUserSearch.Location = new System.Drawing.Point(357, 20);
            this.gBoxUserSearch.Name = "gBoxUserSearch";
            this.gBoxUserSearch.Size = new System.Drawing.Size(433, 64);
            this.gBoxUserSearch.TabIndex = 19;
            this.gBoxUserSearch.TabStop = false;
            this.gBoxUserSearch.Text = "Search User : ";
            // 
            // butExit
            // 
            this.butExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butExit.Location = new System.Drawing.Point(666, 486);
            this.butExit.Name = "butExit";
            this.butExit.Size = new System.Drawing.Size(65, 25);
            this.butExit.TabIndex = 21;
            this.butExit.Text = "&Exit";
            this.butExit.UseVisualStyleBackColor = true;
            this.butExit.Click += new System.EventHandler(this.butExit_Click);
            // 
            // frmMain_SATS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(802, 523);
            this.Controls.Add(this.butExit);
            this.Controls.Add(this.gBoxUserSearch);
            this.Controls.Add(this.gBoxDepartment);
            this.Controls.Add(this.panelUserDetail);
            this.Controls.Add(this.btnAdminTool);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmMain_SATS";
            this.Text = "User Account Manager";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.panelUserDetail.ResumeLayout(false);
            this.gBoxLog.ResumeLayout(false);
            this.gBoxLog.PerformLayout();
            this.gBoxDepartment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LoginControl)).EndInit();
            this.gBoxUserSearch.ResumeLayout(false);
            this.gBoxUserSearch.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAdminTool;
        private System.Windows.Forms.Label lblUserID;
        private System.Windows.Forms.TextBox txtUserID;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.Panel panelUserDetail;
        private System.Windows.Forms.GroupBox gBoxDepartment;
        private System.Windows.Forms.GroupBox gBoxLog;
        private System.Windows.Forms.Button btnResetPwd;
        private System.Windows.Forms.Button btnActivate;
        private System.Windows.Forms.ListBox lstBoxDepartment;
        private System.Windows.Forms.GroupBox gBoxUserSearch;
        private System.Windows.Forms.ListBox lBoxResult;
        private System.Windows.Forms.TextBox txtFullName;
        private System.Windows.Forms.Label lblFullName;
        private System.Windows.Forms.Button butExit;

    }
}