﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Ufis.Data;
using UAM.Map;
using Ufis.Utils;
using UAM.Ctrl;

namespace UAM
{
    public partial class frmAdminTool : Form
    {
        private ITable mySEC;
        private ITable myGRP;
        private ITable myUAA;
        private ITable myORG;
        private List<BindList> bindList;
        private List<BindList> bindDepList;
        private CtrlAdmin objAdmin;
        public bool bSaveOrCancel = false;

        public frmAdminTool()
        {
            InitializeComponent();
        }
        private void frmAdminTool_Load(object sender, EventArgs e)
        {
            objAdmin = CtrlAdmin.GetInstance();
            FillListofGroups();
            FillListofDepartments();
        }

        private void FillListofGroups()
        {
            mySEC = objAdmin.LoadSECData("WHERE TYPE = 'G' AND STAT ='1' AND USID LIKE '%UAM%' ORDER BY USID");

            //mySEC = objAdmin.LoadSECData("WHERE TYPE = 'G' AND STAT ='1' ORDER BY USID");

            if (mySEC.Count > 0)
            {
                bindList = new List<BindList>();
                for (int i = 0; i < mySEC.Count; i++)
                {
                    bindList.Add(new BindList { URNO = mySEC[i]["URNO"], USID = mySEC[i]["USID"] });
                }
                lstBoxGroup.DataSource = bindList;
                lstBoxGroup.DisplayMember = "USID";
                lstBoxGroup.ValueMember = "URNO";
            }
            else
            {
                MessageBox.Show(this, "No Group found!", "Group List", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Application.Exit();
            }
        }

        private void lstBoxGroup_Click(object sender, EventArgs e)
        {
            string strUserUrno = "";

            myGRP = objAdmin.LoadGRPData("WHERE FREL = '" + lstBoxGroup.SelectedValue.ToString() + "'");

            if (myGRP.Count > 0)
            {
                bindList = new List<BindList>();
                for (int i = 0; i < myGRP.Count; i++)
                {
                    if (i == 0)
                        strUserUrno = myGRP[i]["FSEC"];
                    else
                        strUserUrno += "," + myGRP[i]["FSEC"];
                }
            }
            strUserUrno = strUserUrno.Replace(",", "','");

            mySEC = objAdmin.LoadSECData("WHERE URNO IN ('" + strUserUrno + "') AND TYPE = 'U' AND STAT = '1' ORDER BY USID");

            bindList = new List<BindList>();

            if (mySEC.Count > 0)
            {
                for (int i = 0; i < mySEC.Count; i++)
                {
                    bindList.Add(new BindList { URNO = mySEC[i]["URNO"], USID = mySEC[i]["USID"] + " - " + CedaUtils.ConvertStringForClient(mySEC[i]["NAME"]) });
                }

                lstBoxUser.DataSource = bindList;
                lstBoxUser.DisplayMember = "USID";
                lstBoxUser.ValueMember = "URNO";
                gBoxUser.Text = "List of Users belong to Group [ " + lstBoxGroup.Text + " ]:";
            }
            else
            {
                lstBoxUser.DataSource = bindList;
                lstBoxUser.DisplayMember = "USID";
                lstBoxUser.ValueMember = "URNO";
                MessageBox.Show(this, "No User belong to the Group!", "User List", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            myUAA = objAdmin.LoadUAAData("WHERE GRPU IN ('" + lstBoxGroup.SelectedValue.ToString() + "' ) ORDER BY DPT1");

            foreach (int checkedItemIndex in chkLstDeparment.CheckedIndices)
            {
                chkLstDeparment.SetItemChecked(checkedItemIndex, false);
            }

            if (myUAA.Count > 0)
            {
                for (int i = 0; i < myUAA.Count; i++)
                {
                    for (int j = 0; j < bindDepList.Count; j++)
                    {
                        if (myUAA[i]["DPT1"] == bindDepList[j].DPT1)
                            chkLstDeparment.SetItemChecked(j, true);
                    }
                }
            }

        }

        private void FillListofDepartments()
        {
            myORG = objAdmin.LoadORGData();
            bindDepList = new List<BindList>();
            if (myORG.Count > 0)
            {
                for (int i = 0; i < myORG.Count; i++)
                {
                    //chkLstDeparment.Items.Add(myORG[i]["DPT1"] + " - " + myORG[i]["DPTN"]);
                    bindDepList.Add(new BindList { DPT1 = myORG[i]["DPT1"], DPTN = myORG[i]["DPT1"] + " - " + CedaUtils.ConvertStringForClient(myORG[i]["DPTN"]) });
                }
                bindDepList.Add(new BindList { DPT1 = "-", DPTN = "Non Staff" });
                chkLstDeparment.DataSource = bindDepList;
                chkLstDeparment.DisplayMember = "DPTN";
                chkLstDeparment.ValueMember = "DPT1";
            }
            else
            {
                chkLstDeparment.Items.Clear();
                MessageBox.Show(this, "No Department found!", "Department List", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                chkLstDeparment.Items.Add("Non Staff");
                btnSave.Enabled = false;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (chkLstDeparment.CheckedItems.Count > 0)
            {
                myUAA = objAdmin.LoadUAAData("WHERE GRPU='" + lstBoxGroup.SelectedValue.ToString() + "'");

                //while (myUAA.Count > 0)
                //{
                //    IRow rowUAA = myUAA[0];
                //    rowUAA.Status = State.Deleted;
                //    myUAA.Save(rowUAA);
                //} 
                for (int i = 0; i < myUAA.Count; i++)
                { 
                    myUAA[i].Status = State.Deleted; 
                }
                myUAA.Save();

                foreach (object itemChecked in chkLstDeparment.CheckedItems)
                {
                    IRow rowUAA = myUAA.CreateEmptyRow();
                    rowUAA["GRPU"] = lstBoxGroup.SelectedValue.ToString();
                    if (itemChecked.ToString() == "Non Staff")
                        rowUAA["DPT1"] = "-";
                    else
                        rowUAA["DPT1"] = ((UAM.Map.BindList)(itemChecked)).DPT1; 
                    rowUAA["CDAT"] = UT.DateTimeToCeda(DateTime.Now);
                    rowUAA["USEC"] = UT.UserName;
                    rowUAA["HOPO"] = UT.Hopo;
                    rowUAA.Status = State.Created;
                    myUAA.Add(rowUAA);
                    myUAA.Save(rowUAA);
                } 
                MessageBox.Show(this, "Saved successfully!", "Department List", MessageBoxButtons.OK, MessageBoxIcon.Information);
                bSaveOrCancel = true;
            }
            else
            {
                MessageBox.Show(this, "Select atleast one Department!", "Department List", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            bSaveOrCancel = false;
            this.Close();
        }

    }
}
