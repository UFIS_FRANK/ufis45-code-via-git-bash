﻿using System;
using System.Collections.Generic;
using System.Text;
using Ufis.Data;
using Ufis.Utils;

namespace UAM.Ctrl
{
    public class CtrlAdmin
    {
        public static IDatabase myDB = null;
        static CtrlAdmin _this = null;

        private IDatabase _myDB;
        private ITable myObj;
        private const string SECTAB_DB_FIELDS = "URNO,USID,NAME,PASS,STAT,LANG,REMA";

        private const string SECTAB_FIELD_LENGTHS = "10,32,50,50,1,4,127";
        private const string GRPTAB_DB_FIELDS = "FSEC,FREL";
        private const string GRPTAB_FIELD_LENGTHS = "10,10";
        private const string UAATAB_DB_FIELDS = "URNO,DPT1,GRPU,CDAT,USEC,HOPO";
        private const string UAATAB_FIELD_LENGTHS = "10,10,10,14,32,10";
        private const string ORGTAB_DB_FIELDS = "URNO,DPT1,DPTN";
        private const string ORGTAB_FIELD_LENGTHS = "10,10,50";
        private const string SORTAB_DB_FIELDS = "CODE, SURN";
        private const string SORTAB_FIELD_LENGTHS = "10,20";
        private const string UALTAB_DB_FIELDS = "URNO,EVCT,USID,EVDE,CDAT,USEC,RMRK,HOPO";
        private const string UALTAB_FIELD_LENGTHS = "10,10,32,10,14,32,127,3"; 

        private CtrlAdmin()
        {
            _myDB = UT.GetMemDB();
            myDB = _myDB;
        }

        public static CtrlAdmin GetInstance()
        {
            if (_this == null) _this = new CtrlAdmin();
            return _this;
        }

        public ITable LoadSECData(string strWhere)
        {
            myObj = _myDB.Bind("SEC", "SEC", SECTAB_DB_FIELDS, SECTAB_FIELD_LENGTHS, SECTAB_DB_FIELDS);
            myObj.Clear();
            myObj.Load(strWhere);
            return myObj;
        }

        public ITable LoadGRPData(string strWhere)
        {
            myObj = _myDB.Bind("GRP", "GRP", GRPTAB_DB_FIELDS, GRPTAB_FIELD_LENGTHS, GRPTAB_DB_FIELDS);
            myObj.Clear();
            myObj.Load(strWhere);
            return myObj;
        }

        public ITable LoadUAAData(string strWhere)
        {
            myObj = _myDB.Bind("UAA", "UAA", UAATAB_DB_FIELDS, UAATAB_FIELD_LENGTHS, UAATAB_DB_FIELDS);
            myObj.Clear();
            myObj.Load(strWhere);
            return myObj;
        }

        public ITable LoadORGData()
        {
            myObj = _myDB.Bind("ORG", "ORG", ORGTAB_DB_FIELDS, ORGTAB_FIELD_LENGTHS, ORGTAB_DB_FIELDS);
            myObj.Clear();
            myObj.Load("ORDER BY DPT1");
            return myObj;
        }

        public ITable LoadSORData(string strWhere)
        {
            myObj = _myDB.Bind("SOR", "SOR", SORTAB_DB_FIELDS, SORTAB_FIELD_LENGTHS, SORTAB_DB_FIELDS);
            myObj.Clear();
            myObj.Load(strWhere);
            return myObj;
        }


        public ITable LoadUALData(string strWhere)
        {
            myObj = _myDB.Bind("UAL", "UAL", UALTAB_DB_FIELDS, UALTAB_FIELD_LENGTHS, UALTAB_DB_FIELDS);
            myObj.Clear();
            myObj.Load(strWhere);
            return myObj;
        } 
    }
}
