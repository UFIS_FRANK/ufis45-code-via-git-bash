﻿using System;
using System.Collections.Generic;
using System.Reflection;
  
namespace UAM.Ctrl
{
    /// <summary>
    /// Represents the utilities for working with CEDA data proccessing.
    /// </summary>
    public class CedaUtils
    {
        private readonly static string _strClientPatchList =
            BuildBinaryString(new int[] { 34, 39, 44, 10, 13, 40, 41 });
        private readonly static string _strNewServerPatchList =
            BuildBinaryString(new int[] { 23, 24, 25, 28, 29, 40, 41 });
        private readonly static string _strOldServerPatchList =
            BuildBinaryString(new int[] { 176, 177, 178, 179, 180, 8250, 339 });

        public enum ConvertMethodEnum
        {
            FOR_CLIENT,
            FOR_SERVER,
            SERVER_TO_CLIENT,
            CLIENT_TO_SERVER
        }

        private static string BuildBinaryString(int[] myAscList)
        {
            string strResult = string.Empty;

            foreach (int myAsc in myAscList)
            {
                strResult += (char)myAsc;
            }

            return strResult;
        }

        private static string ReplaceChars(string text, string oldValue, string newValue)
        {
            string strResult = text;
            int i = 0;

            foreach (char cOldChar in oldValue)
            {
                char cNewChar = newValue[i];
                strResult = strResult.Replace(cOldChar, cNewChar);

                i++;
            }

            return strResult;
        }

        /// <summary>
        /// Converts special characters in a string from client to server and vise-versa.
        /// </summary>
        /// <param name="text">Text/string contains special characters.</param>
        /// <param name="method">The conversion method.</param>
        /// <param name="checkOldBehaviour">Indicates whether to use old character set for conversion.</param>
        /// <returns></returns>
        public static string ConvertString(string text, ConvertMethodEnum method, bool checkOldBehaviour)
        {
            string strResult = text;

            if (method == ConvertMethodEnum.FOR_SERVER)
            {
                strResult = strResult.Replace("\r\n", "\n");
            }

            switch (method)
            {
                case ConvertMethodEnum.FOR_CLIENT:
                case ConvertMethodEnum.SERVER_TO_CLIENT:
                    strResult = ReplaceChars(text, _strNewServerPatchList, _strClientPatchList);
                    if (strResult.Equals(text) && checkOldBehaviour)
                    {//Nothing changed, so we try the old version
                        strResult = ReplaceChars(text, _strOldServerPatchList, _strClientPatchList);
                    }
                    break;
                case ConvertMethodEnum.FOR_SERVER:
                case ConvertMethodEnum.CLIENT_TO_SERVER:
                    if (checkOldBehaviour)
                    {
                        strResult = ReplaceChars(strResult, _strClientPatchList, _strOldServerPatchList);
                    }
                    else
                    {
                        strResult = ReplaceChars(strResult, _strClientPatchList, _strNewServerPatchList);
                    }
                    break;
            }

            if (method == ConvertMethodEnum.FOR_CLIENT)
            {
                strResult = strResult.Replace("\n", "\r\n");
            }

            return strResult;
        }

        /// <summary>
        /// Converts special characters in a string returned from CEDA server to their original characters.
        /// </summary>
        /// <param name="text">Text/string contains special characters.</param>
        /// <returns></returns>
        public static string ConvertStringForClient(string text)
        {
            return ConvertString(text, ConvertMethodEnum.SERVER_TO_CLIENT, true);
        }

        /// <summary>
        /// Converts special characters in a string before sending it to CEDA server.
        /// </summary>
        /// <param name="text">Text/string contains special characters.</param>
        /// <returns></returns>
        public static string ConvertStringForServer(string text)
        {
            return ConvertString(text, ConvertMethodEnum.CLIENT_TO_SERVER, false);
        }

        /// <summary>
        /// Converts special characters in a string before sending it to CEDA server.
        /// </summary>
        /// <param name="text">Text/string contains special characters.</param>
        /// <param name="useOldCharset">Indicates whether to use old character set for conversion.</param>
        /// <returns></returns>
        public static string ConvertStringForServer(string text, bool useOldCharset)
        {
            return ConvertString(text, ConvertMethodEnum.CLIENT_TO_SERVER, useOldCharset);
        }

        /// <summary>
        /// Converts a DateTime object into the "yyyyMMddHHmmss" format
        /// </summary>
        /// <param name="date">The DateTime object</param>
        /// <returns>string in "yyyyMMddHHmmss" format</returns>
        /// <remarks>none.</remarks>
        public static string DateTimeToCedaDateTime(DateTime date)
        {
            return date.ToString("yyyyMMddHHmmss");
        }

        /// <summary>
        /// Converts a DateTime object into the "yyyyMMdd" format
        /// </summary>
        /// <param name="date">The DateTime object</param>
        /// <returns>string in "yyyyMMdd" format</returns>
        /// <remarks>none.</remarks>
        public static string DateTimeToCedaDate(DateTime date)
        {
            return date.ToString("yyyyMMdd");
        }

        /// <summary>
        /// Converts a Ceda date-time format to a DateTime object.
        /// We assume that the format is "yyyyMMddHHmmss"
        /// </summary>
        /// <param name="s">a string containing a date and time to convert, e.g. 20030510231000</param>
        /// <param name="result">When this method returns, contains System.DateTime value equivalent to the date and time contained in s, 
        /// if the conversion succeeded, or System.DateTime.MinValue if the conversion failed. 
        /// The conversion fails if the s parameter is null, or does not contain a valid string representation of 
        /// a date anf time. This parameter is passed uninitialized.</param>
        /// <returns>bool indicates if conversion succeeded</returns>
        /// <remarks>none.</remarks>
        public static bool CedaFullDateToDateTime(string s, out DateTime result)
        {
            return CedaFullDateToDateTime(s, DateTimeKind.Utc, out result);
        }

        /// <summary>
        /// Converts a Ceda date-time format to a DateTime object.
        /// We assume that the format is "yyyyMMddHHmmss"
        /// </summary>
        /// <param name="s">a string containing a date and time to convert, e.g. 20030510231000</param>
        /// <param name="dateTimeKind">Specifies if the date time is in local or UTC date time.</param>
        /// <param name="result">When this method returns, contains System.DateTime value equivalent to the date and time contained in s, 
        /// if the conversion succeeded, or System.DateTime.MinValue if the conversion failed. 
        /// The conversion fails if the s parameter is null, or does not contain a valid string representation of 
        /// a date and time. This parameter is passed uninitialized.</param>
        /// <returns>bool indicates if conversion succeeded</returns>
        /// <remarks>none.</remarks>
        public static bool CedaFullDateToDateTime(string s, DateTimeKind dateTimeKind, out DateTime result)
        {
            System.Globalization.DateTimeStyles dateTimeStyle = System.Globalization.DateTimeStyles.None;

            switch (dateTimeKind)
            {
                case DateTimeKind.Local:
                    dateTimeStyle = System.Globalization.DateTimeStyles.AssumeLocal;
                    break;
                case DateTimeKind.Utc:
                    dateTimeStyle = System.Globalization.DateTimeStyles.AssumeUniversal;
                    break;
            }

            return DateTime.TryParseExact(s, "yyyyMMddHHmmss", null, dateTimeStyle, out result);
        }

        /// <summary>
        /// Converts a Ceda date format to a DateTime object.
        /// We assume that the format is "yyyyMMdd"
        /// </summary>
        /// <param name="s">a string containing a date and time to convert, e.g. 20030510</param>
        /// <param name="result">When this method returns, contains System.DateTime value equivalent to the date and time contained in s, 
        /// if the conversion succeeded, or System.DateTime.MinValue if the conversion failed. 
        /// The conversion fails if the s parameter is null, or does not contain a valid string representation of 
        /// a date anf time. This parameter is passed uninitialized.</param>
        /// <returns>bool indicates if conversion succeeded</returns>
        /// <remarks>none.</remarks>
        public static bool CedaDateToDateTime(string s, out DateTime result)
        {
            return CedaDateToDateTime(s, DateTimeKind.Utc, out result);
        }

        /// <summary>
        /// Converts a Ceda date-time format to a DateTime object.
        /// We assume that the format is "yyyyMMdd"
        /// </summary>
        /// <param name="s">a string containing a date and time to convert, e.g. 20030510</param>
        /// <param name="dateTimeKind">Specifies if the date time is in local or UTC date time.</param>
        /// <param name="result">When this method returns, contains System.DateTime value equivalent to the date and time contained in s, 
        /// if the conversion succeeded, or System.DateTime.MinValue if the conversion failed. 
        /// The conversion fails if the s parameter is null, or does not contain a valid string representation of 
        /// a date and time. This parameter is passed uninitialized.</param>
        /// <returns>bool indicates if conversion succeeded</returns>
        /// <remarks>none.</remarks>
        public static bool CedaDateToDateTime(string s, DateTimeKind dateTimeKind, out DateTime result)
        {
            System.Globalization.DateTimeStyles dateTimeStyle = System.Globalization.DateTimeStyles.None;

            switch (dateTimeKind)
            {
                case DateTimeKind.Local:
                    dateTimeStyle = System.Globalization.DateTimeStyles.AssumeLocal;
                    break;
                case DateTimeKind.Utc:
                    dateTimeStyle = System.Globalization.DateTimeStyles.AssumeUniversal;
                    break;
            }

            return DateTime.TryParseExact(s, "yyyyMMdd", null, dateTimeStyle, out result);
        }

        /// <summary>
        /// Converts a Ceda time string format to a TimeSpan object.
        /// We assume that the format is "HHmm"
        /// </summary>
        /// <param name="s">a string containing a time to convert, e.g. 1431</param>
        /// <param name="result">When this method returns, contains System.TimeSpan value equivalent to the time contained in s, 
        /// if the conversion succeeded, or System.TimeSpan.MinValue if the conversion failed. 
        /// The conversion fails if the s parameter is null, or does not contain a valid string representation of 
        /// a time. This parameter is passed uninitialized.</param>
        /// <returns>bool indicates if conversion succeeded</returns>
        /// <remarks>none.</remarks>
        //public static bool CedaTimeToTimeSpan(string s, out TimeSpan result)
        //{
        //    return TimeSpan.TryParse(s, "%h%m", null, out result);
        //}

        /// <summary>
        /// Converts a TimeSpan object into the "HHmmss" format
        /// </summary>
        /// <param name="timeSpan">The TimeSpan object</param>
        /// <returns>string in "HHmmss" format</returns>
        /// <remarks>none.</remarks>
        public static string TimeSpanToCedaTime(TimeSpan timeSpan)
        {
            return string.Format("{0:hhmm}", timeSpan);
        }

        /// <summary>
        /// Get the dictionary of field as key and data as value from field list and data list.
        /// </summary>
        /// <param name="fieldList">Field list delimited by comma.</param>
        /// <param name="dataList">Data list delimited by comma.</param>
        /// <returns></returns>
        public static Dictionary<string, string> GetFieldDataDictionary(string fieldList, string dataList)
        {
            return GetFieldDataDictionary(fieldList.Split(','), dataList.Split(','));
        }

        /// <summary>
        /// Get the dictionary of field as key and data as value from array of field and array of data.
        /// </summary>
        /// <param name="fieldList">String array of field name.</param>
        /// <param name="dataList">String array of data.</param>
        /// <returns></returns>
        public static Dictionary<string, string> GetFieldDataDictionary(string[] fields, string[] data)
        {
            Dictionary<string, string> dicData = new Dictionary<string, string>();

            int intDataCount = Math.Min(fields.Length, data.Length);
            for (int i = 0; i < intDataCount; i++)
            {
                dicData.Add(fields[i], data[i]);
            }

            return dicData;
        }

        /// <summary>
        /// Get urno from broadcast message.
        /// </summary>
        /// <param name="selection">The selection part of broadcast message.</param>
        /// <param name="fieldList">Field list delimited by comma.</param>
        /// <param name="dataList">Data list delimited by comma.</param>
        /// <returns></returns>
        public static int GetUrnoFromBroadcast(string selection, string fieldList, string dataList)
        {
            return GetUrnoFromBroadcast(selection, GetFieldDataDictionary(fieldList, dataList));
        }

        /// <summary>
        /// Get urno from broadcast message.
        /// </summary>
        /// <param name="selection">The selection part of broadcast message.</param>
        /// <param name="dicFieldData">The dictionary of field as key and data as value.</param>
        /// <returns></returns>
        public static int GetUrnoFromBroadcast(string selection, Dictionary<string, string> dicFieldData)
        {
            int intUrno = -1;
            string strUrno = string.Empty;

            selection = selection.TrimEnd();
            if (selection.Length > 0)
            {
                if (selection.IndexOf('\'') != -1 || selection.IndexOf("WHERE") != -1)
                {
                    intUrno = GetUrnoFromSelection(selection);
                }
            }

            if (intUrno < 0 && dicFieldData.Count > 0)
            {
                string strUrnoValue;
                if (dicFieldData.TryGetValue("URNO", out strUrnoValue))
                {
                    int intUrnoValue;
                    if (int.TryParse(strUrnoValue, out intUrnoValue))
                        intUrno = intUrnoValue;
                }
            }

            return intUrno;
        }

        /// <summary>
        /// Gets urno from selection part of broadcast message.
        /// </summary>
        /// <param name="selection">The selection part of broadcast message.</param>
        /// <returns></returns>
        public static int GetUrnoFromSelection(string selection)
        {
            int intUrno = -1;

            if (selection.IndexOf("URNO") < 0)
                return intUrno;

            string strUrno = string.Empty;
            for (int i = 0; i < selection.Length; i++)
            {
                // remove all non-numeric characters
                if (selection[i] >= '0' && selection[i] <= '9')
                {
                    strUrno += selection[i];
                }
                else if (strUrno.Length > 0)
                {
                    // end of the URNO found so break eg for "WHERE URNO = 119675853\n119675853"
                    break;
                }
            }

            int intUrnoValue;
            if (int.TryParse(strUrno, out intUrnoValue))
                intUrno = intUrnoValue;

            return intUrno;
        }

        #region Get Urno
        /// <summary>
        /// Static list of urnos.
        /// </summary>
        private static List<int> lstUrnos = new List<int>();

        /// <summary>
        /// Get the unique record number.
        /// </summary>
        /// <param name="dataContext">The Ufis.Data.EntityDataContextBase </param>
        /// <returns></returns>
        //public static int GetUrno(EntityDataContextBase dataContext)
        //{
        //    return GetUrno(dataContext, 1);
        //}

        /// <summary>
        /// Get the unique record number.
        /// </summary>
        /// <param name="dataContext">The Ufis.Data.EntityDataContextBase object.</param>
        /// <param name="numberOfUrnos">Number of urnos to retrieve from server.</param>
        /// <returns></returns>
        //public static int GetUrno(EntityDataContextBase dataContext, int numberOfUrnos)
        //{
        //    int intUrno = -1;

        //    if (lstUrnos.Count == 0)
        //    {
        //        CedaGetUrnoCommand cedaCommand = new CedaGetUrnoCommand() { NumberOfRows = numberOfUrnos };

        //        DataRowCollection dataRows = dataContext.ExecuteQueryCommand(cedaCommand);
        //        for (int i = 0; i < dataRows.Count; i++)
        //        {
        //            DataRow dataRow = dataRows[i];
        //            lstUrnos.Add(int.Parse(dataRow.ItemArray[0]));
        //        }
        //    }

        //    if (lstUrnos.Count > 0)
        //    {
        //        intUrno = lstUrnos[0];
        //        lstUrnos.RemoveAt(0);
        //    }

        //    return intUrno;
        //}

        #endregion
    }
}
