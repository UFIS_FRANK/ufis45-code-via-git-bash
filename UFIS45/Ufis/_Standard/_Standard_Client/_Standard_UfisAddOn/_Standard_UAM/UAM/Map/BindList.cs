﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UAM.Map
{
    public class BindList
    {
        public string URNO { get; set; }
        public string USID { get; set; }
        public string DPT1 { get; set; }
        public string DPT2 { get; set; }
        public string DPTN { get; set; }        
        public string GRPU { get; set; }
    }

}
