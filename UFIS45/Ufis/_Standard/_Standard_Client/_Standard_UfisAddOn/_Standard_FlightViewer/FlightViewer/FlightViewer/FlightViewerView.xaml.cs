﻿#region +++++ Source Code Information +++++
/*
 
 *  Developed By        :       Phyoe Khaing Min
 *  Developed On        :       Nov-16-2011
 *  Completed On        :       
 *  Modified By         :       Phyoe Khaing Min
 *  Modified On         :       May-21-2012
 *  Description         :       Get the flight information from Seasonal Table (SSRTAB). Show the flight information according to the Date Range,
 *                              Airport and Airline Filter. You can include or exclude Airport and Airline and also can choose on Arrival and
 *                              Departure or both for Airline by type in at the Airline Text Box (2 or 3 letters of Airline name). If you want to choose
 *                              More than one, you can use comma "," seperator to select multiple airline. That feature can also applied on Airport
 *                              filter and also choose for Origin or Destination or Both.
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Ufis.Entities;
using System.Threading;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Printing;
using FlightViewer.Helpers;
using Ufis.IO;
using Ufis.LoginWindow;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.Drawing.Printing;
using DevExpress.Xpf.Bars;
using DevExpress.Xpf.Editors;

namespace FlightViewer.FlightViewer
{
    
    /// <summary>
    /// Interaction logic for FlightViewerView.xaml
    /// </summary>
    public partial class FlightViewerView : UserControl
    {
        #region +++ Variable Declaration +++
        char cFlight;
        char cAirport;
        public HpParameters param;
        public HpReportLogo Logo;
        public HpFooter Footer;
        string sFooterString = "";
        public FlightViewerViewModel objViewModel;
        DateTime validFrom,validTo;        
        Boolean bAirportEI, bAirlineEI;
        string sDOOP = "";

        // Using a DependencyProperty as the backing store for MyInt.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MyIntProperty =
            DependencyProperty.Register("MyInt", typeof(int), typeof(FlightViewerView), new UIPropertyMetadata(1));

        // Using a DependencyProperty as the backing store for MyData.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LoadingPanelVisibilityProperty =
            DependencyProperty.Register("y LoadingPanelVisibility", typeof(string), typeof(FlightViewerView), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.AffectsRender));
        #endregion

        #region +++ Constructor / Destructor +++
        public int MyInt
        {
            get { return (int)GetValue(MyIntProperty); }
            set { SetValue(MyIntProperty, value); }
        }

        public Visibility LoadingPanelVisibility
        {
            get { return (Visibility)GetValue(LoadingPanelVisibilityProperty); }
            set { SetValue(LoadingPanelVisibilityProperty, value); }
        }
        
        public FlightViewerView()
        {            
            InitializeComponent();
            
            radAFBoth.IsChecked = true;
            radALBth.IsChecked = true;
            if (String.Compare(txtAirport.Text, "", false) == 0)
            {
                radAFBoth.IsEnabled = false;
                radAFOrg.IsEnabled = false;
                radAFDes.IsEnabled = false;
            }
            if (String.Compare(txtAirline.Text, "", false) != 0)
                return;

            radALArr.IsEnabled = false;
            radALDep.IsEnabled = false;
            radALBth.IsEnabled = false;
            
        }

        public class MyGridControl : GridControl
        {
            protected override void OnItemsSourceChanged(object oldValue, object newValue)
            {
                base.OnItemsSourceChanged(oldValue, newValue);
                PopulateColumns();
            }
        }
        #endregion

        #region +++ WPF Control Events +++
        /// <summary>
        /// Get Season date 
        /// </summary>        
        private void dxpcbeSeason_EditValueChanged(object sender, EditValueChangedEventArgs e)
        {
            EntDbSeason objSeason = (EntDbSeason)e.NewValue;
            txtValidFrom.EditValue = objSeason.ValidFrom;
            txtValidTo.EditValue = objSeason.ValidTo;
        }
        /// <summary>
        /// Change column value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dxpgrcSeasonFlightLists_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Value != null) 
            {
                switch (e.Column.FieldName)
                {
                    case "DaysOfOperation":
                        e.DisplayText = e.Value.ToString().Replace("0", "-");
                        break;
                    case "StandardTimeOfArrival":
                    case "StandardTimeOfDeparture":
                        e.DisplayText = string.Format("{0:hh\\:mm}", e.Value);
                        break;
                }
            }
        }        

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            Button objbtn = (Button)sender;
            switch (objbtn.Content.ToString())
            {
                case "+":
                    objbtn.Content = "-";
                    break;
                default:
                    objbtn.Content = "+";
                    break;
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bAirportEI = String.Compare(btnAPei.Content.ToString(), "+", false) == 0 ? true : false;

                bAirlineEI = String.Compare(btnAFei.Content.ToString(), "+", false) == 0 ? true : false;
                if (dxpcbeSeason.SelectedIndex == -1)
                {
                    MessageBox.Show("Please select the season!", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                validFrom = (DateTime)txtValidFrom.EditValue;
                validTo = (DateTime)txtValidTo.EditValue;  
             
                DayOfWeek tDay;
                //Get Data
                if (txtValidFrom.EditValue == null || txtValidTo.EditValue == null)
                {
                    MessageBox.Show("Please specify valid period of season to load!", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                
                else
                {
                    DateTime tvalidFrom = Convert.ToDateTime(validFrom.ToShortDateString());
                    DateTime tvalidTo = Convert.ToDateTime(validTo.ToShortDateString());

                    if (tvalidFrom > tvalidTo)
                    {
                        MessageBox.Show("Invalid Date Input!", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                    //Set date duration to filter of
                    int iDay = (tvalidTo - tvalidFrom).Days + 1;

                    if (iDay < 7)
                    {
                        DateTime tDate = tvalidFrom;

                        while (tDate <= tvalidTo)
                        {
                            tDay = tDate.DayOfWeek;
                            sDOOP = String.Compare(sDOOP, "", false) != 0 ? String.Format("{0}{1}", sDOOP, GetDayNo(tDay.ToString())) : GetDayNo(tDay.ToString()).ToString();
                            TimeSpan tTimeSpan = new TimeSpan(1, 0, 0, 0);
                            tDate = tDate.Add(tTimeSpan);
                        }
                    }
                    else
                    {
                        sDOOP = "1234567";
                    }
                    dxpgrcSeasonFlightLists.ShowLoadingPanel = true;

                    BackgroundWorker bw = new BackgroundWorker();

                    bw.DoWork += Process;
                    bw.RunWorkerCompleted += bgw_RunWorkerCompleted;
                    bw.RunWorkerAsync();

                }
                
            }       
                
            catch (Exception ex)
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                MessageBox.Show(ex.Message);
            }

        }

        private void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            dxpgrcSeasonFlightLists.ShowLoadingPanel = false;
        }        

        private void barButtonPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            param = new HpParameters();


            objViewModel = new FlightViewerViewModel();

            Footer = new HpFooter();

            EntUser objEntUser = new EntUser();
            objEntUser = LoginViewModel.GetLastUser();
            sFooterString = objViewModel.GetParameterValue();

            string strValidFrom = txtValidFrom.EditValue != null ? ((DateTime)txtValidFrom.EditValue).ToString("dd.MM.yyyy") : string.Empty;

            string strValidTo = txtValidTo.EditValue != null ? ((DateTime)txtValidTo.EditValue).ToString("dd.MM.yyyy") : string.Empty;


            string sLogoPath = CedaEnvironment.GetEnvironmentVariable(CedaEnvironment.CedaEnvironmentVariable.UfisSystem);
            sLogoPath = sLogoPath + "\\stdlogo.bmp";

            Logo = new HpReportLogo() { RptLogo = new BitmapImage(new Uri(sLogoPath, UriKind.Absolute)) };
            ((HpReportLogo)Resources["Logo"]).RptLogo = Logo.RptLogo;

            param.MyParameter = String.Compare(txtAirport.Text, "", false) != 0 ? String.Compare(btnAPei.Content.ToString(), "+", false) == 0 ? String.Compare(txtAirline.Text, "", false) != 0 ? String.Compare(btnAFei.Content.ToString(), "+", false) == 0 ? String.Format("Filter Detail - Season = {0} , Period From = {1}, To = {2}", dxpcbeSeason.Text, strValidFrom, strValidTo) + String.Format(" , Airline(s)  in ( {0} )", txtAirline.Text.ToUpper().Replace(" ", ",")) : String.Format("Filter Detail - Season = {0} , Period From = {1}, To = {2}", dxpcbeSeason.Text, strValidFrom, strValidTo) + String.Format(" , Airline(s) not in ( {0} )", txtAirline.Text.ToUpper().Replace(" ", ",")) : String.Format("Filter Detail - Season = {0} , Period From = {1}, To = {2}", dxpcbeSeason.Text, strValidFrom, strValidTo) + String.Format(" , Airport in ( {0} )", txtAirport.Text.ToUpper().Replace(" ", ",")) : String.Compare(txtAirline.Text, "", false) != 0 ? String.Compare(btnAFei.Content.ToString(), "+", false) == 0 ? String.Format("Filter Detail - Season = {0} , Period From = {1}, To = {2}", dxpcbeSeason.Text, strValidFrom, strValidTo) + String.Format(" , Airline(s)  in ( {0} )", txtAirline.Text.ToUpper().Replace(" ", ",")) : String.Format("Filter Detail - Season = {0} , Period From = {1}, To = {2}", dxpcbeSeason.Text, strValidFrom, strValidTo) + String.Format(" , Airline(s) not in ( {0} )", txtAirline.Text.ToUpper().Replace(" ", ",")) : String.Format("Filter Detail - Season = {0} , Period From = {1}, To = {2}", dxpcbeSeason.Text, strValidFrom, strValidTo) + String.Format(" , Airport not in ( {0} )", txtAirport.Text.ToUpper().Replace(" ", ",")) : String.Compare(txtAirline.Text, "", false) != 0 ? String.Compare(btnAFei.Content.ToString(), "+", false) == 0 ? String.Format("Filter Detail - Season = {0} , Period From = {1}, To = {2}", dxpcbeSeason.Text, strValidFrom, strValidTo) + String.Format(" , Airline(s)  in ( {0} )", txtAirline.Text.ToUpper().Replace(" ", ",")) : String.Format("Filter Detail - Season = {0} , Period From = {1}, To = {2}", dxpcbeSeason.Text, strValidFrom, strValidTo) + String.Format(" , Airline(s) not in ( {0} )", txtAirline.Text.ToUpper().Replace(" ", ",")) : String.Format("Filter Detail - Season = {0} , Period From = {1}, To = {2}", dxpcbeSeason.Text, strValidFrom, strValidTo);

            param.ReportFooterString = sFooterString;
            param.GeneratedBy = string.Format("Printed By : {0}", objViewModel.GetLoginUserName(objEntUser.UserId));

            ((HpParameters)Resources["paramet"]).MyParameter = param.MyParameter;
            ((HpParameters)Resources["Footer"]).ReportFooterString = param.ReportFooterString;
            ((HpParameters)Resources["GenBy"]).GeneratedBy = param.GeneratedBy;


            if (dxpgrcSeasonFlightLists.VisibleRowCount > 0)
            {
                PrintableControlLink link = new PrintableControlLink((TableView)dxpgrcSeasonFlightLists.View) { Margins = new Margins(50, 50, 30, 30), Landscape = true };
                link.Margins.Top = 3;
                link.Margins.Left = 1;
                link.Margins.Right = 1;
                link.Margins.Bottom = 3;
                link.PaperKind = PaperKind.A4;
                link.PageHeaderTemplate = (DataTemplate)Resources["detailPrintHeaderTemplate"];
                link.PageFooterTemplate = (DataTemplate)Resources["DetailPrintFooterTemplate"];
                link.ShowPrintPreviewDialog(Window.GetWindow(this));
            }
        }

        private void Radio_Click(object sender, RoutedEventArgs e)
        {
            RadioButton objRdb = (RadioButton)sender;
            switch (objRdb.Name)
            {
                case "radAFBoth":
                    cAirport = 'B';
                    break;
                case "radAFOrg":
                    cAirport = 'O';
                    break;
                case "radAFDes":
                    cAirport = 'D';
                    break;
                case "radALArr":
                    cFlight = 'A';
                    break;
                case "radALBth":
                    cFlight = 'C';
                    break;
                default:
                    cFlight = 'D';
                    break;
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox objTextBox = (TextBox)sender;
            switch (objTextBox.Name)
            {
                case "txtAirline":
                    if (txtAirline.Text.Length > 0)
                    {                        
                        radALBth.IsChecked = true;
                        radALBth.IsEnabled = true;
                        radALArr.IsEnabled = true;
                        radALDep.IsEnabled = true;
                        cFlight = 'C';
                    }
                    else
                    {
                        radALBth.IsEnabled = false;
                        radALArr.IsEnabled = false;
                        radALDep.IsEnabled = false;                        
                    }
                    break;
                default:
                    if (txtAirport.Text.Length > 0)
                    {
                        radAFBoth.IsChecked = true;
                        radAFBoth.IsEnabled = true;
                        radAFOrg.IsEnabled = true;
                        radAFDes.IsEnabled = true;
                        cAirport = 'B';
                    }
                    else
                    {
                        radAFBoth.IsEnabled = false;
                        radAFOrg.IsEnabled = false;
                        radAFDes.IsEnabled = false;
                    }
                    break;

            }
        }
        #endregion

        #region +++ Private Custom functions +++
        /// <summary>
        /// Get Defined Day No of the Week by DayName
        /// </summary>
        /// <param name="sDayOfWeek">System.String containing sDayofWeek</param>
        /// <returns>Return System.int that contain the value of iReturnValue</returns>
        private int GetDayNo(string sDayOfWeek)
        {
            int iReturnValue = 0;
            switch (sDayOfWeek)
            {
                case "Monday":
                    iReturnValue = 1;
                    break;
                case "Tuesday":
                    iReturnValue = 2;
                    break;
                case "Wednesday":
                    iReturnValue = 3;
                    break;
                case "Thursday":
                    iReturnValue = 4;
                    break;
                case "Friday":
                    iReturnValue = 5;
                    break;
                case "Saturday":
                    iReturnValue = 6;
                    break;
                case "Sunday":
                    iReturnValue = 7;
                    break;
            }
            return iReturnValue;
        }

        private void Process(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(2000);
            Dispatcher.BeginInvoke(new Action(() =>
            {
                FetchData();
            }));
        }

        private void FetchData()
        {
            //Search Result....
            FlightViewerViewModel viewModel = (FlightViewerViewModel)DataContext;
            viewModel.SearchSeason(dxpcbeSeason.Text, validFrom, validTo, txtAirport.Text, bAirportEI, txtAirline.Text, bAirlineEI, cAirport, cFlight, String.Compare(sDOOP, "", false) != 0 ? sDOOP : "");
            sDOOP = "";
        }
        #endregion
    }

    public class HeaderFormattingOptions
    {
        public double FontSize { get; set; }
        // other options ...
    }
}
