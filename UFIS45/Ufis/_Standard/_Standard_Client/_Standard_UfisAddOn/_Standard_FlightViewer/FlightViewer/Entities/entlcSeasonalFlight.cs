﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;

namespace FlightViewer.Entities
{
    public class entlcSeasonalFlight : Entity
    {
        private int _Urno;
        public int Urno 
        {
            get
            {
                return _Urno;
            }
            set
            {
                if (_Urno == value)
                    return;

                _Urno = value;
                OnPropertyChanged("Urno");
            }
        }
        private string _AircraftIATACode;
        public string AircraftIATACode 
        {
            get
            {
                return _AircraftIATACode;
            }
            set
            {
                if (String.Compare(_AircraftIATACode, value, false) == 0)
                    return;

                _AircraftIATACode = value;
                OnPropertyChanged("AircraftIATACode");
            }
        }
        private string _AircraftICAOCode;
        public string AircraftICAOCode
        {
            get
            {
                return _AircraftICAOCode;
            }
            set
            {
                if (String.Compare(_AircraftICAOCode, value, false) == 0)
                    return;

                _AircraftICAOCode = value;
                OnPropertyChanged("AircraftICAOCode");
            }
        }

        private string _AirlineIATACodeOfArrival;
        public string AirlineIATACodeOfArrival 
        {
            get
            {
                return _AirlineIATACodeOfArrival;
            }
            set
            {
                if (String.Compare(_AirlineIATACodeOfArrival, value, false) == 0)
                    return;

                _AirlineIATACodeOfArrival = value;
                OnPropertyChanged("AirlineIATACodeOfArrival");
            }
        }
        private string _AirlineIATACodeOfDeparture;
        public string AirlineIATACodeOfDeparture 
        {
            get
            {
                return _AirlineIATACodeOfDeparture;
            }
            set
            {
                if (String.Compare(_AirlineIATACodeOfDeparture, value, false) == 0)
                    return;

                _AirlineIATACodeOfDeparture = value;
                OnPropertyChanged("AirlineIATACodeOfDeparture");
            }
        }
        private string _AirlineICAOCodeOfArrival;
        public string AirlineICAOCodeOfArrival
        {
            get
            {
                return _AirlineICAOCodeOfArrival;
            }
            set
            {
                if (String.Compare(_AirlineICAOCodeOfArrival, value, false) == 0)
                    return;

                _AirlineICAOCodeOfArrival = value;
                OnPropertyChanged("AirlineICAOCodeOfArrival");
            }
        }

        private string _AirlineICAOCodeOfDeparture;
        public string AirlineICAOCodeOfDeparture
        {
            get
            {
                return _AirlineICAOCodeOfDeparture;
            }
            set
            {
                if (String.Compare(_AirlineICAOCodeOfDeparture, value, false) == 0)
                    return;

                _AirlineICAOCodeOfDeparture = value;
                OnPropertyChanged("AirlineICAOCodeOfDeparture");
            }
        }

        private int? _CodeShareCountOfArrival;
        public int? CodeShareCountOfArrival
        {
            get
            {
                return _CodeShareCountOfArrival;
            }
            set
            {
                if (_CodeShareCountOfArrival == value)
                    return;

                _CodeShareCountOfArrival = value;
                OnPropertyChanged("CodeShareCountOfArrival");
            }
        }

        private int? _CodeShareCountOfDeparture;
        public int? CodeShareCountOfDeparture
        {
            get
            {
                return _CodeShareCountOfDeparture;
            }
            set
            {
                if (_CodeShareCountOfDeparture == value)
                    return;

                _CodeShareCountOfDeparture = value;
                OnPropertyChanged("CodeShareCountOfDeparture");
            }
        }

        private string _CodeShareListOfArrival;
        public string CodeShareListOfArrival 
        {
            get
            {
                return _CodeShareListOfArrival;
            }
            set
            {
                if (String.Compare(_CodeShareListOfArrival, value, false) == 0)
                    return;

                _CodeShareListOfArrival = value;
                OnPropertyChanged("CodeShareListOfArrival");
            }
        }
        private string _CodeShareListOfDeparture;
        public string CodeShareListOfDeparture
        {
            get
            {
                return _CodeShareListOfDeparture;
            }
            set
            {
                if (String.Compare(_CodeShareListOfDeparture, value, false) == 0)
                    return;

                _CodeShareListOfDeparture = value;
                OnPropertyChanged("CodeShareListOfDeparture");
            }
        }

        private string _DaysOfOperation;
        public string DaysOfOperation
        {
            get
            {
                return _DaysOfOperation;
            }
            set
            {
                if (String.Compare(_DaysOfOperation, value, false) == 0)
                    return;

                _DaysOfOperation = value;
                OnPropertyChanged("DaysOfOperation");
            }
        }

        private string _DestinationAirportIATACode;
        public string DestinationAirportIATACode
        {
            get
            {
                return _DestinationAirportIATACode;
            }
            set
            {
                if (String.Compare(_DestinationAirportIATACode, value, false) == 0)
                    return;

                _DestinationAirportIATACode = value;
                OnPropertyChanged("DestinationAirportIATACode");
            }
        }

        private string _DestinationAirportICAOCode;
        public string DestinationAirportICAOCode
        {
            get
            {
                return _DestinationAirportICAOCode;
            }
            set
            {
                if (String.Compare(_DestinationAirportICAOCode, value, false) == 0)
                    return;

                _DestinationAirportICAOCode = value;
                OnPropertyChanged("DestinationAirportICAOCode");
            }
        }

        private string _FlightNumberOfArrival;
        public string FlightNumberOfArrival
        {
            get
            {
                return _FlightNumberOfArrival;
            }
            set
            {
                if (String.Compare(_FlightNumberOfArrival, value, false) == 0)
                    return;

                _FlightNumberOfArrival = value;
                OnPropertyChanged("FlightNumberOfArrival");
            }
        }

        private string _FlightNumberOfDeparture;
        public string FlightNumberOfDeparture
        {
            get
            {
                return _FlightNumberOfDeparture;
            }
            set
            {
                if (String.Compare(_FlightNumberOfDeparture, value, false) == 0)
                    return;

                _FlightNumberOfDeparture = value;
                OnPropertyChanged("FlightNumberOfDeparture");
            }
        }

        private string _FlightOperationalType;
        public string FlightOperationalType
        {
            get
            {
                return _FlightOperationalType;
            }
            set
            {
                if (String.Compare(_FlightOperationalType, value, false) == 0)
                    return;

                _FlightOperationalType = value;
                OnPropertyChanged("FlightOperationalType");
            }
        }

        private string _FlightSuffixOfArrival;
        public string FlightSuffixOfArrival
        {
            get
            {
                return _FlightSuffixOfArrival;
            }
            set
            {
                if (String.Compare(_FlightSuffixOfArrival, value, false) == 0)
                    return;

                _FlightSuffixOfArrival = value;
                OnPropertyChanged("FlightSuffixOfArrival");
            }
        }

        private string _FlightSuffixOfDeparture;
        public string FlightSuffixOfDeparture
        {
            get
            {
                return _FlightSuffixOfDeparture;
            }
            set
            {
                if (String.Compare(_FlightSuffixOfDeparture, value, false) == 0)
                    return;

                _FlightSuffixOfDeparture = value;
                OnPropertyChanged("FlightSuffixOfDeparture");
            }
        }

        private int? _Frequency;
        public int? Frequency
        {
            get
            {
                return _Frequency;
            }
            set
            {
                if (_Frequency == value)
                    return;

                _Frequency = value;
                OnPropertyChanged("Frequency");
            }
        }

        private string _FullFlightNumberOfArrival;
        public string FullFlightNumberOfArrival
        {
            get
            {
                return _FullFlightNumberOfArrival;
            }
            set
            {
                if (String.Compare(_FullFlightNumberOfArrival, value, false) == 0)
                    return;

                _FullFlightNumberOfArrival = value;
                OnPropertyChanged("FullFlightNumberOfArrival");
            }
        }

        private string _FullFlightNumberOfDeparture;
        public string FullFlightNumberOfDeparture
        {
            get
            {
                return _FullFlightNumberOfDeparture;
            }
            set
            {
                if (String.Compare(_FullFlightNumberOfDeparture, value, false) == 0)
                    return;

                _FullFlightNumberOfDeparture = value;
                OnPropertyChanged("FullFlightNumberOfDeparture");
            }
        }

        private string _NatureCodeOfArrival;
        public string NatureCodeOfArrival
        {
            get
            {
                return _NatureCodeOfArrival;
            }
            set
            {
                if (String.Compare(_NatureCodeOfArrival, value, false) == 0)
                    return;

                _NatureCodeOfArrival = value;
                OnPropertyChanged("NatureCodeOfArrival");
            }
        }

        private string _NatureCodeOfDeparture;
        public string NatureCodeOfDeparture
        {
            get
            {
                return _NatureCodeOfDeparture;
            }
            set
            {
                if (String.Compare(_NatureCodeOfDeparture, value, false) == 0)
                    return;

                _NatureCodeOfDeparture = value;
                OnPropertyChanged("NatureCodeOfDeparture");
            }
        }

        private int? _NightsStop;
        public int? NightsStop
        {
            get
            {
                return _NightsStop;
            }
            set
            {
                if (_NightsStop == value)
                    return;

                _NightsStop = value;
                OnPropertyChanged("NightsStop");
            }
        }


        private string _OriginAirportIATACode;
        public string OriginAirportIATACode
        {
            get
            {
                return _OriginAirportIATACode;
            }
            set
            {
                if (String.Compare(_OriginAirportIATACode, value, false) == 0)
                    return;

                _OriginAirportIATACode = value;
                OnPropertyChanged("OriginAirportIATACode");
            }
        }

        private string _OriginAirportICAOCode;
        public string OriginAirportICAOCode
        {
            get
            {
                return _OriginAirportICAOCode;
            }
            set
            {
                if (String.Compare(_OriginAirportICAOCode, value, false) == 0)
                    return;

                _OriginAirportICAOCode = value;
                OnPropertyChanged("OriginAirportICAOCode");
            }
        }

        private string _RemarkOfArrival;
        public string RemarkOfArrival
        {
            get
            {
                return _RemarkOfArrival;
            }
            set
            {
                if (String.Compare(_RemarkOfArrival, value, false) == 0)
                    return;

                _RemarkOfArrival = value;
                OnPropertyChanged("RemarkOfArrival");
            }
        }

        private string _RemarkOfDeparture;
        public string RemarkOfDeparture
        {
            get
            {
                return _RemarkOfDeparture;
            }
            set
            {
                if (String.Compare(_RemarkOfDeparture, value, false) == 0)
                    return;

                _RemarkOfDeparture = value;
                OnPropertyChanged("RemarkOfDeparture");
            }
        }

        private string _SeasonKey;
        public string SeasonKey
        {
            get
            {
                return _SeasonKey;
            }
            set
            {
                if (String.Compare(_SeasonKey, value, false) == 0)
                    return;

                _SeasonKey = value;
                OnPropertyChanged("SeasonKey");
            }
        }

        private string _SeasonName;
        public string SeasonName
        {
            get
            {
                return _SeasonName;
            }
            set
            {
                if (String.Compare(_SeasonName, value, false) == 0)
                    return;

                _SeasonName = value;
                OnPropertyChanged("SeasonName");
            }
        }

        private string _ServiceTypeOfArrival;
        public string ServiceTypeOfArrival 
        { 
            get 
            { 
                return _ServiceTypeOfArrival; 
            } 
            set 
            {
                if (String.Compare(_ServiceTypeOfArrival, value, false) == 0)
                    return;

                _ServiceTypeOfArrival = value;
                OnPropertyChanged("ServiceTypeOfArrival");
            } 
        }

        private string _ServiceTypeOfDeparture;
        public string ServiceTypeOfDeparture
        {
            get
            {
                return _ServiceTypeOfDeparture;
            }
            set
            {
                if (String.Compare(_ServiceTypeOfDeparture, value, false) == 0)
                    return;

                _ServiceTypeOfDeparture = value;
                OnPropertyChanged("ServiceTypeOfDeparture");
            }
        }


        private TimeSpan? _StandardTimeOfArrival;
        public TimeSpan? StandardTimeOfArrival
        {
            get
            {
                return _StandardTimeOfArrival;
            }
            set
            {
                if (_StandardTimeOfArrival == value)
                    return;

                _StandardTimeOfArrival = value;
                OnPropertyChanged("StandardTimeOfArrival");
            }
        }

        private TimeSpan? _StandardTimeOfArrivalAtDestination;
        public TimeSpan? StandardTimeOfArrivalAtDestination
        {
            get
            {
                return _StandardTimeOfArrivalAtDestination;
            }
            set
            {
                if (_StandardTimeOfArrivalAtDestination == value)
                    return;

                _StandardTimeOfArrivalAtDestination = value;
                OnPropertyChanged("StandardTimeOfArrivalAtDestination");
            }
        }

        private TimeSpan? _StandardTimeOfDeparture;
        public TimeSpan? StandardTimeOfDeparture
        {
            get
            {
                return _StandardTimeOfDeparture;
            }
            set
            {
                if (_StandardTimeOfDeparture == value)
                    return;

                _StandardTimeOfDeparture = value;
                OnPropertyChanged("StandardTimeOfDeparture");
            }
        }

        private TimeSpan? _StandardTimeOfDepartureFromOrigin;
        public TimeSpan? StandardTimeOfDepartureFromOrigin
        {
            get
            {
                return _StandardTimeOfDepartureFromOrigin;
            }
            set
            {
                if (_StandardTimeOfDepartureFromOrigin == value)
                    return;

                _StandardTimeOfDepartureFromOrigin = value;
                OnPropertyChanged("StandardTimeOfDepartureFromOrigin");
            }
        }

        private string _Url1;
        public string Url1
        {
            get
            {
                return _Url1;
            }
            set
            {
                if (String.Compare(_Url1, value, false) == 0)
                    return;

                _Url1 = value;
                OnPropertyChanged("Url1");
            }
        }

        private string _Url2;
        public string Url2
        {
            get
            {
                return _Url2;
            }
            set
            {
                if (String.Compare(_Url2, value, false) == 0)
                    return;

                _Url1 = value;
                OnPropertyChanged("Url2");
            }
        }


        private string _Url3;
        public string Url3
        {
            get
            {
                return _Url3;
            }
            set
            {
                if (String.Compare(_Url3, value, false) == 0)
                    return;

                _Url1 = value;
                OnPropertyChanged("Url3");
            }
        }


        private DateTime? _ValidFrom;
        public DateTime? ValidFrom
        {
            get
            {
                return _ValidFrom;
            }
            set
            {
                if (_ValidFrom == value)
                    return;

                _ValidFrom = value;
                OnPropertyChanged("ValidFrom");
            }
        }


        private DateTime? _ValidTo;
        public DateTime? ValidTo
        {
            get
            {
                return _ValidTo;
            }
            set
            {
                if (_ValidTo == value)
                    return;

                _ValidTo = value;
                OnPropertyChanged("ValidTo");
            }
        }

        private string _ViaAirportIATACodeOfArrival;
        public string ViaAirportIATACodeOfArrival 
        {
            get
            {
                return _ViaAirportIATACodeOfArrival;
            }
            set
            {
                if (String.Compare(_ViaAirportIATACodeOfArrival, value, false) == 0)
                    return;

                _ViaAirportIATACodeOfArrival = value;
                OnPropertyChanged("ViaAirportIATACodeOfArrival");
            }
        }
        private string _ViaAirportIATACodeOfDeparture;
        public string ViaAirportIATACodeOfDeparture
        {
            get
            {
                return _ViaAirportIATACodeOfDeparture;
            }
            set
            {
                if (String.Compare(_ViaAirportIATACodeOfDeparture, value, false) == 0)
                    return;

                _ViaAirportIATACodeOfDeparture = value;
                OnPropertyChanged("ViaAirportIATACodeOfDeparture");
            }
        }

        private string _ViaAirportICAOCodeOfArrival;
        public string ViaAirportICAOCodeOfArrival
        {
            get
            {
                return _ViaAirportICAOCodeOfArrival;
            }
            set
            {
                if (String.Compare(_ViaAirportICAOCodeOfArrival, value, false) == 0)
                    return;

                _ViaAirportICAOCodeOfArrival = value;
                OnPropertyChanged("ViaAirportICAOCodeOfArrival");
            }
        }

        private string _ViaAirportICAOCodeOfDeparture;
        public string ViaAirportICAOCodeOfDeparture
        {
            get
            {
                return _ViaAirportICAOCodeOfDeparture;
            }
            set
            {
                if (String.Compare(_ViaAirportICAOCodeOfDeparture, value, false) == 0)
                    return;

                _ViaAirportICAOCodeOfDeparture = value;
                OnPropertyChanged("ViaAirportICAOCodeOfDeparture");
            }
        }

        private int? _ViaCountOfArrival;
        public int? ViaCountOfArrival
        {
            get
            {
                return _ViaCountOfArrival;
            }
            set
            {
                if (_ViaCountOfArrival == value)
                    return;

                _ViaCountOfArrival = value;
                OnPropertyChanged("ViaCountOfArrival");
            }
        }

        private int? _ViaCountOfDeparture;
        public int? ViaCountOfDeparture 
        {
            get
            {
                return _ViaCountOfDeparture;
            }
            set
            {
                if (ViaCountOfDeparture == value)
                    return;

                _ViaCountOfDeparture = value;
                OnPropertyChanged("ViaCountOfDeparture");
            }
        }
        private string _ViaListOfArrival;
        public string ViaListOfArrival 
        {
            get
            {
                return _ViaListOfArrival;
            }
            set
            {
                if (String.Compare(_ViaListOfArrival, value, false) == 0)
                    return;

                _ViaListOfArrival = value;
                OnPropertyChanged("ViaListOfArrival");
            }
        }
        private string _ViaListOfDeparture;
        public string ViaListOfDeparture 
        {
            get
            {
                return _ViaListOfDeparture;
            }
            set
            {
                if (String.Compare(_ViaListOfDeparture, value, false) == 0)
                    return;

                _ViaListOfDeparture = value;
                OnPropertyChanged("ViaListOfDeparture");
            }
        }
    }
}
