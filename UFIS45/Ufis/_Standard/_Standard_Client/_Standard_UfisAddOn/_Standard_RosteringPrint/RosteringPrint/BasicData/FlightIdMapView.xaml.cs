﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Ufis.Entities;
using DevExpress.Xpf.Grid;
using System.Windows.Input;
using Ufis.Data;
using RosteringPrint.Helpers;
using System.Windows;

namespace RosteringPrint.BasicData
{
    /// <summary>
    /// Interaction logic for FlightIdMapView.xaml
    /// </summary>
    public partial class FlightIdMapView : UserControl
    {
        public FlightIdMapView()
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            HpDxGrid.CurrentGrid = grid;

            Mouse.OverrideCursor = null;
        }

        private void tableView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            FlightIdMapViewModel viewModel = (FlightIdMapViewModel)DataContext;
            viewModel.ChangeActiveEntity((Entity)e.OldRow);
        }

        private void tableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            int rowHandle = grid.View.GetRowHandleByMouseEventArgs(e);
            if (rowHandle != GridControl.InvalidRowHandle)
            {
                FlightIdMapViewModel viewModel = (FlightIdMapViewModel)DataContext;
                viewModel.Update(null);
            }
        }

        private void grid_CustomRowFilter(object sender, RowFilterEventArgs e)
        {
            if (e.ListSourceRowIndex >= 0)
            {
                EntityCollectionBase<EntDbFlightIdMap> FlightIdMapCollection = (EntityCollectionBase<EntDbFlightIdMap>)grid.ItemsSource;
                EntDbFlightIdMap FlightIdMap = FlightIdMapCollection[e.ListSourceRowIndex];

                if (FlightIdMap.Urno <= 0) //new record
                {
                    e.Visible = true;
                    e.Handled = true;
                }
            }
        }

        private void tableView_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            BasicDataViewModel viewModel = (BasicDataViewModel)DataContext;
            if (viewModel.IsEditing)
            {
                switch (e.Key)
                {
                    case Key.Tab:
                        //Creating a FocusNavigationDirection object and setting it to a
                        // local field that contains the direction selected.
                        FocusNavigationDirection focusDirection;
                        if (Keyboard.Modifiers == ModifierKeys.Shift)
                            focusDirection = FocusNavigationDirection.Previous;
                        else
                            focusDirection = FocusNavigationDirection.Next;

                        MoveControlFocus(focusDirection);

                        e.Handled = true;
                        break;

                    case Key.Return:
                        if (HpBasicData.IsAnEntryElement(Keyboard.FocusedElement))
                        {
                            //enforce to move focus
                            MoveControlFocus(FocusNavigationDirection.Next);
                            viewModel.SaveUpdate(grid);
                            e.Handled = true;
                        }
                        break;

                    case Key.Escape:
                        if (HpBasicData.IsAnEntryElement(Keyboard.FocusedElement))
                        {
                            viewModel.CancelUpdate(grid);
                            e.Handled = true;
                        }
                        break;
                }
            }
        }

        private void MoveControlFocus(FocusNavigationDirection focusDirection)
        {
            // Gets the element with keyboard focus.
            UIElement elementWithFocus = (UIElement)Keyboard.FocusedElement;

            // Change keyboard focus.
            if (elementWithFocus != null)
            {
                // MoveFocus takes a TraveralReqest as its argument.
                TraversalRequest request = new TraversalRequest(focusDirection);

                // Change keyboard focus.
                elementWithFocus.MoveFocus(request);
            }
        }
    }
}
