﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;

namespace RosteringPrint.Entities
{
    public class entlcWorkingHours
    {
        public TimeSpan NightWorkingHours { get; set; }

        public TimeSpan HolidayWorkingHours { get; set; }

    }

    public class entlcPayrollCompensate:Entity
    {
        private long _StaffId;
        private string _PersonalNo;
        private string _StaffName;
        private string _tNightWorkingHrs;
        private string _tHolidayWorkingHrs;

        public long StaffId
        {
            get { return _StaffId; }
            set
            {
                if (_StaffId != value)
                {
                    _StaffId = value;
                    OnPropertyChanged("StaffId");
                }
            }
        }

        public string PersonalNo
        {
            get { return _PersonalNo; }
            set
            {
                if (_PersonalNo != value)
                {
                    _PersonalNo = value;
                    OnPropertyChanged("PersonalNo");
                }
            }
        }
        public string StaffName
        {
            get { return _StaffName; }
            set
            {
                if (_StaffName != value)
                {
                    _StaffName = value;
                    OnPropertyChanged("StaffName");
                }
            }
        }
        public string NightWorkingHours
        {
            get { return _tNightWorkingHrs; }
            set
            {
                if (_tNightWorkingHrs != value)
                {
                    _tNightWorkingHrs = value;
                    OnPropertyChanged("NightWorkingHours");
                }
            }
        }

        public string HolidayWorkingHours
        {
            get { return _tHolidayWorkingHrs; }
            set
            {
                if (_tHolidayWorkingHrs != value)
                {
                    _tHolidayWorkingHrs = value;
                    OnPropertyChanged("HolidayWorkingHours");
                }
            }
        }
    }
}
