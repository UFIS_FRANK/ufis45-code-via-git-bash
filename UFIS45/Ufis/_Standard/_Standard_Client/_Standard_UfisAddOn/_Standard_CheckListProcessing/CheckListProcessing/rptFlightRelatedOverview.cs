using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using Ufis.Utils;
using System.Drawing;

namespace CheckListProcessing
{
	public class rptFlightRelatedOverview : ActiveReport3
	{
		private string myFlno = "";
		private string myArea = "";
		private AxTABLib.AxTAB myTab = null;
		private string myFlightRemark;
		private int currLine = 0;

		public rptFlightRelatedOverview(AxTABLib.AxTAB tab, string Flno, string Area, string FlightRemark)
		{
			myTab = tab;
			myFlno = Flno;
			myArea = Area;
			myFlightRemark = FlightRemark;
			InitializeReport();
		}

		private void rptFlightRelatedOverview_ReportStart(object sender, System.EventArgs eArgs)
		{
			lblArea.Text = "Area: " + myArea;
			lblFlight.Text = "Flight: " + myFlno;
			txtDate.Text = DateTime.Now.ToString();
			//*** Read the logo if exists and set it
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strPrintLogoPath = myIni.IniReadValue("STATUSMANAGER", "PRINTLOGO");
			Image myImage = null;
			try
			{
				if(strPrintLogoPath != "")
				{
					myImage = new Bitmap(strPrintLogoPath);
				}
			}
			catch(Exception)
			{
			}
			if(myImage != null)
				Picture1.Image = myImage;
			//*** END Read the logo if exists and set it
		}

		private void rptFlightRelatedOverview_FetchData(object sender, DataDynamics.ActiveReports.ActiveReport3.FetchEventArgs eArgs)
		{
			cbDone.Checked = false;
			txtText.Text = "";
			txtDateTime.Text = "";

			if(myTab.GetFieldValue(currLine, "CHEK") == "Y")
			{
				cbDone.Checked = true;
			}
			txtText.Text = " " + myTab.GetFieldValue(currLine, "TEXT");
			if(myTab.GetFieldValue(currLine, "CHTM") != "")
			{
				DateTime dat = UT.CedaFullDateToDateTime(myTab.GetFieldValue(currLine, "CHTM"));
				txtDateTime.Text = " " + dat.ToShortDateString() + "/" + dat.ToShortTimeString();
			}

			if(currLine == myTab.GetLineCount())
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
				currLine++;
			}
		}

		private void ReportFooter_BeforePrint(object sender, System.EventArgs eArgs)
		{
			txtFlightRemarks.Text = myFlightRemark;
		}

		#region ActiveReports Designer generated code
		private ReportHeader ReportHeader = null;
		private PageHeader PageHeader = null;
		private Label Label1 = null;
		private Label lblFlight = null;
		private Picture Picture1 = null;
		private Label lblArea = null;
		private Label Label2 = null;
		private Label Label3 = null;
		private Label Label4 = null;
		private Line Line1 = null;
		private Line Line2 = null;
		private Line Line3 = null;
		private Line Line4 = null;
		private Line Line5 = null;
		private Line Line6 = null;
		private Detail Detail = null;
		private CheckBox cbDone = null;
		private TextBox txtText = null;
		private TextBox txtDateTime = null;
		private Line Line7 = null;
		private Line Line8 = null;
		private Line Line9 = null;
		private Line Line10 = null;
		private Line Line11 = null;
		private PageFooter PageFooter = null;
		private Label Label5 = null;
		private Label Label6 = null;
		private TextBox TextBox1 = null;
		private Label Label7 = null;
		private TextBox TextBox2 = null;
		private TextBox txtDate = null;
		private ReportFooter ReportFooter = null;
		private Label Label8 = null;
		private TextBox txtFlightRemarks = null;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "CheckListProcessing.rptFlightRelatedOverview.rpx");
			this.ReportHeader = ((DataDynamics.ActiveReports.ReportHeader)(this.Sections["ReportHeader"]));
			this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
			this.ReportFooter = ((DataDynamics.ActiveReports.ReportFooter)(this.Sections["ReportFooter"]));
			this.Label1 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[0]));
			this.lblFlight = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[1]));
			this.Picture1 = ((DataDynamics.ActiveReports.Picture)(this.PageHeader.Controls[2]));
			this.lblArea = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[3]));
			this.Label2 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[4]));
			this.Label3 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[5]));
			this.Label4 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[6]));
			this.Line1 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[7]));
			this.Line2 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[8]));
			this.Line3 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[9]));
			this.Line4 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[10]));
			this.Line5 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[11]));
			this.Line6 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[12]));
			this.cbDone = ((DataDynamics.ActiveReports.CheckBox)(this.Detail.Controls[0]));
			this.txtText = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[1]));
			this.txtDateTime = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[2]));
			this.Line7 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[3]));
			this.Line8 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[4]));
			this.Line9 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[5]));
			this.Line10 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[6]));
			this.Line11 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[7]));
			this.Label5 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[0]));
			this.Label6 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[1]));
			this.TextBox1 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[2]));
			this.Label7 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[3]));
			this.TextBox2 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[4]));
			this.txtDate = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[5]));
			this.Label8 = ((DataDynamics.ActiveReports.Label)(this.ReportFooter.Controls[0]));
			this.txtFlightRemarks = ((DataDynamics.ActiveReports.TextBox)(this.ReportFooter.Controls[1]));
			// Attach Report Events
			this.ReportStart += new System.EventHandler(this.rptFlightRelatedOverview_ReportStart);
			this.FetchData += new DataDynamics.ActiveReports.ActiveReport3.FetchEventHandler(this.rptFlightRelatedOverview_FetchData);
			this.ReportFooter.BeforePrint += new System.EventHandler(this.ReportFooter_BeforePrint);
		}

		#endregion
	}
}
