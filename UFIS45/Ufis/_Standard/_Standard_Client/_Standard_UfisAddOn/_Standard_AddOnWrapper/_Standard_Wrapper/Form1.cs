using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace _Standard_Wrapper
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private AxBCCOMCLIENTLib.AxBCComClient axBCComClient1;
		private AxUCOMLib.AxUCom axUCom1;
		private AxUFISCOMLib.AxUfisCom axUfisCom1;
		private AxUGANTTLib.AxUGantt axUGantt1;
		private AxTABLib.AxTAB axTAB1;
		private AxAATLOGINLib.AxAatLogin axAatLogin1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
			this.axBCComClient1 = new AxBCCOMCLIENTLib.AxBCComClient();
			this.axUCom1 = new AxUCOMLib.AxUCom();
			this.axUfisCom1 = new AxUFISCOMLib.AxUfisCom();
			this.axUGantt1 = new AxUGANTTLib.AxUGantt();
			this.axTAB1 = new AxTABLib.AxTAB();
			this.axAatLogin1 = new AxAATLOGINLib.AxAatLogin();
			((System.ComponentModel.ISupportInitialize)(this.axBCComClient1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.axUCom1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.axUfisCom1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.axUGantt1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.axTAB1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.axAatLogin1)).BeginInit();
			this.SuspendLayout();
			// 
			// axBCComClient1
			// 
			this.axBCComClient1.Enabled = true;
			this.axBCComClient1.Location = new System.Drawing.Point(72, 88);
			this.axBCComClient1.Name = "axBCComClient1";
			this.axBCComClient1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axBCComClient1.OcxState")));
			this.axBCComClient1.Size = new System.Drawing.Size(100, 50);
			this.axBCComClient1.TabIndex = 0;
			// 
			// axUCom1
			// 
			this.axUCom1.Enabled = true;
			this.axUCom1.Location = new System.Drawing.Point(96, 40);
			this.axUCom1.Name = "axUCom1";
			this.axUCom1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axUCom1.OcxState")));
			this.axUCom1.Size = new System.Drawing.Size(100, 50);
			this.axUCom1.TabIndex = 1;
			// 
			// axUfisCom1
			// 
			this.axUfisCom1.Enabled = true;
			this.axUfisCom1.Location = new System.Drawing.Point(160, 168);
			this.axUfisCom1.Name = "axUfisCom1";
			this.axUfisCom1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axUfisCom1.OcxState")));
			this.axUfisCom1.Size = new System.Drawing.Size(100, 50);
			this.axUfisCom1.TabIndex = 2;
			// 
			// axUGantt1
			// 
			this.axUGantt1.Location = new System.Drawing.Point(288, 128);
			this.axUGantt1.Name = "axUGantt1";
			this.axUGantt1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axUGantt1.OcxState")));
			this.axUGantt1.Size = new System.Drawing.Size(100, 50);
			this.axUGantt1.TabIndex = 3;
			// 
			// axTAB1
			// 
			this.axTAB1.Location = new System.Drawing.Point(8, 0);
			this.axTAB1.Name = "axTAB1";
			this.axTAB1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axTAB1.OcxState")));
			this.axTAB1.Size = new System.Drawing.Size(100, 50);
			this.axTAB1.TabIndex = 4;
			// 
			// axAatLogin1
			// 
			this.axAatLogin1.Enabled = true;
			this.axAatLogin1.Location = new System.Drawing.Point(72, 160);
			this.axAatLogin1.Name = "axAatLogin1";
			this.axAatLogin1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axAatLogin1.OcxState")));
			this.axAatLogin1.Size = new System.Drawing.Size(100, 50);
			this.axAatLogin1.TabIndex = 5;
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(424, 273);
			this.Controls.Add(this.axAatLogin1);
			this.Controls.Add(this.axTAB1);
			this.Controls.Add(this.axUGantt1);
			this.Controls.Add(this.axUfisCom1);
			this.Controls.Add(this.axUCom1);
			this.Controls.Add(this.axBCComClient1);
			this.Name = "Form1";
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.axBCComClient1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.axUCom1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.axUfisCom1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.axUGantt1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.axTAB1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.axAatLogin1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}
	}
}
