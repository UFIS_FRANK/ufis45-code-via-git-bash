REM Create com-application key-files and interop dll's
REM --------------------------------------------------
sn -k AatLogin.snk
AxImp c:\ufis_Bin\release\AatLogin.ocx /keyfile:AatLogin.snk

sn -k Tab.snk
AxImp c:\ufis_Bin\release\Tab.ocx /keyfile:Tab.snk

sn -k TrafficLight.snk
AxImp c:\ufis_Bin\release\TrafficLight.ocx /keyfile:TrafficLight.snk

sn -k UCom.snk
AxImp c:\ufis_Bin\release\UCom.ocx /keyfile:UCom.snk

sn -k UfisCom.snk
AxImp c:\ufis_Bin\release\UfisCom.ocx /keyfile:UfisCom.snk

sn -k UGantt.snk
AxImp c:\ufis_Bin\release\UGantt.ocx /keyfile:UGantt.snk

sn -k BcProxy.snk
TlbImp c:\ufis_Bin\release\BcProxy.tlb /keyfile:BcProxy.snk

sn -k BcComServer.snk
TlbImp c:\ufis_Bin\release\BcComServer.exe /keyfile:BcComServer.snk


sn -k DeicingClass.snk
TlbImp c:\ufis_Bin\release\DeicingClass.dll  /out:AxDeicingClass.dll /keyfile:DeicingClass.snk

REM Create Ufis-Key
REM ----------------------
copy UfisCom.snk Ufis.snk
