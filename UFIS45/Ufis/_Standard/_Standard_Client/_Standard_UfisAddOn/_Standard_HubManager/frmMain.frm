VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmMain 
   Caption         =   "Connection Manager"
   ClientHeight    =   8670
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   13935
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8670
   ScaleWidth      =   13935
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.Timer tmrBcRefresh 
      Interval        =   60000
      Left            =   10230
      Top             =   2520
   End
   Begin VB.Frame fraFilter 
      Caption         =   "Filter flights"
      Height          =   3135
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   9975
      Begin VB.CommandButton btnDoFilter 
         Caption         =   "&Do Filter"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1200
         TabIndex        =   21
         Top             =   2400
         Width           =   1365
      End
      Begin VB.TextBox txtKeycode 
         Height          =   315
         Left            =   1200
         TabIndex        =   20
         Top             =   1920
         Width           =   1335
      End
      Begin VB.TextBox txtAirline 
         Height          =   315
         Left            =   1200
         TabIndex        =   2
         Top             =   360
         Width           =   1335
      End
      Begin VB.TextBox txtOrigin 
         Height          =   315
         Left            =   1200
         TabIndex        =   6
         Top             =   1125
         Width           =   1335
      End
      Begin VB.TextBox txtNature 
         Height          =   315
         Left            =   1200
         TabIndex        =   8
         Top             =   1530
         Width           =   1335
      End
      Begin VB.TextBox txtFlno 
         Height          =   315
         Left            =   1200
         TabIndex        =   4
         Top             =   720
         Width           =   1335
      End
      Begin TABLib.TAB tabFlights 
         Height          =   2775
         Left            =   2760
         TabIndex        =   10
         Top             =   240
         Width           =   6990
         _Version        =   65536
         _ExtentX        =   12330
         _ExtentY        =   4895
         _StockProps     =   64
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Airline:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   150
         TabIndex        =   1
         Top             =   405
         Width           =   735
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Orig/Dest:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   1170
         Width           =   870
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Nature:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1575
         Width           =   735
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Key Code:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   1920
         Width           =   1005
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "FLNO:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   765
         Width           =   735
      End
   End
   Begin VB.Frame fraFlight 
      Height          =   5175
      Left            =   120
      TabIndex        =   11
      Top             =   3360
      Width           =   9975
      Begin VB.CommandButton cmdPrint 
         Caption         =   "&Print"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   120
         TabIndex        =   14
         Top             =   1200
         Width           =   660
      End
      Begin VB.CommandButton cmdSendTelex 
         Caption         =   "SendTelex"
         Height          =   315
         Left            =   840
         TabIndex        =   12
         Top             =   1200
         Width           =   1215
      End
      Begin TABLib.TAB tabTab 
         Height          =   2115
         Index           =   1
         Left            =   120
         TabIndex        =   13
         Top             =   1560
         Width           =   5595
         _Version        =   65536
         _ExtentX        =   9869
         _ExtentY        =   3731
         _StockProps     =   64
      End
      Begin TABLib.TAB tabTab 
         Height          =   915
         Index           =   0
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Width           =   5595
         _Version        =   65536
         _ExtentX        =   9869
         _ExtentY        =   1614
         _StockProps     =   64
      End
   End
   Begin TABLib.TAB tabTmpFlights 
      Height          =   855
      Left            =   10200
      TabIndex        =   16
      Top             =   480
      Visible         =   0   'False
      Width           =   2355
      _Version        =   65536
      _ExtentX        =   4154
      _ExtentY        =   1508
      _StockProps     =   64
   End
   Begin TABLib.TAB tabFlnuBcs 
      Height          =   735
      Left            =   10200
      TabIndex        =   18
      Top             =   1665
      Visible         =   0   'False
      Width           =   2760
      _Version        =   65536
      _ExtentX        =   4868
      _ExtentY        =   1296
      _StockProps     =   64
   End
   Begin VB.Label Label7 
      Caption         =   "tmp TLX-FLNUs"
      Height          =   240
      Left            =   10200
      TabIndex        =   19
      Top             =   1440
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label3 
      Caption         =   "Tmp Fligts to find next dep."
      Height          =   195
      Left            =   10200
      TabIndex        =   17
      Top             =   240
      Visible         =   0   'False
      Width           =   2355
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private omCurrentSelectedUrno As String
'Private WithEvents MyBC As BcProxyLib.BcPrxy 'igu on 06/02/2013
Private WithEvents BcCom As BCCOMSERVERLib.BcComAtl 'igu on 06/02/2013
Attribute BcCom.VB_VarHelpID = -1
Private Declare Function GetCurrentProcessId Lib "kernel32" () As Long 'igu on 06/02/2013
Private strBcData As String 'igu on 06/02/2013
Private intBcRefreshMinutes As Integer 'igu on 05/01/2012

'--------------------
'Filter flight region
'--------------------
Private Sub InitFilterFlights()
    Dim i As Integer

    tabFlights.ResetContent
    tabFlights.HeaderString = "URNO,A/D,FLNO,Sched.,Est.,Act.,ORG/DES"
    tabFlights.LogicalFieldList = "URNO,ADID,FLNO,SCHED,EST,ACTUAL,ORGDES"
    tabFlights.HeaderLengthString = "0,40,80,80,80,80,80"
    tabFlights.ColumnWidthString = "10,2,8,14,14,14,5"
    tabFlights.LifeStyle = True
    
    For i = 3 To 5
        tabFlights.DateTimeSetColumn i
        tabFlights.DateTimeSetColumnFormat i, "YYYYMMDDhhmmss", "hh':'mm'/'DD"
    Next i
End Sub

Private Sub btnDoFilter_Click()
    Dim strFAlc As String
    Dim strFFlno As String
    Dim strFOrig As String
    Dim strFDest As String
    Dim strFNat As String
    Dim strFKeycode As String
    Dim strALC2 As String
    Dim strFlno As String
    Dim strAlc3 As String
    Dim strOrig As String
    Dim strDest As String
    Dim strNat As String
    Dim strKeycode As String
    Dim strAdid As String
    Dim blInsert As Boolean
    Dim strValues As String
    Dim i As Long
    
    strFAlc = txtAirline.Text
    strFFlno = txtFlno.Text
    strFOrig = txtOrigin.Text
    strFNat = txtNature.Text
    strFKeycode = txtKeycode.Text
    
    tabFlights.ResetContent
    For i = 0 To frmData.tabData(0).GetLineCount - 1
        blInsert = True
        strFlno = frmData.tabData(0).GetFieldValue(i, "FLNO")
        strAdid = frmData.tabData(0).GetFieldValue(i, "ADID")
        strALC2 = frmData.tabData(0).GetFieldValue(i, "ALC2")
        strAlc3 = frmData.tabData(0).GetFieldValue(i, "ALC3")
        strOrig = frmData.tabData(0).GetFieldValue(i, "ORG3")
        strDest = frmData.tabData(0).GetFieldValue(i, "DES3")
        strNat = frmData.tabData(0).GetFieldValue(i, "TTYP")
        strKeycode = frmData.tabData(0).GetFieldValue(i, "STEV")
        If strAdid = "A" Then
            If (strFAlc <> "") Then
                If strFAlc <> strALC2 Then
                    If strFAlc <> strAlc3 Then
                        blInsert = False
                    End If
                End If
            End If
            If strFFlno <> "" Then
                If strFFlno <> strFlno Then
                    blInsert = False
                End If
            End If
            If strFOrig <> "" Then
                If strFOrig <> strOrig Then
                    blInsert = False
                End If
            End If
            If strFNat <> "" Then
                If strFNat <> strNat Then
                    blInsert = False
                End If
            End If
            If strFKeycode <> "" Then
                If strFKeycode <> strKeycode Then
                    blInsert = False
                End If
            End If
            If blInsert = True Then ' insert the record
                strValues = frmData.tabData(0).GetFieldValues(i, "URNO,ADID,FLNO,STOA,ETAI,TIFA,ORG3")   '"URNO,A/D,FLNO,Sched.,Est.,Act.,ORG/DES"
                tabFlights.InsertTextLine strValues, False
            End If
        End If
        If strAdid = "D" Then
            If (strFAlc <> "") Then
                If strFAlc <> strALC2 Then
                    If strFAlc <> strAlc3 Then
                        blInsert = False
                    End If
                End If
            End If
            If strFFlno <> "" Then
                If strFFlno <> strFlno Then
                    blInsert = False
                End If
            End If
            If strFOrig <> "" Then
                If strFOrig <> strDest Then
                    blInsert = False
                End If
            End If
            If strFNat <> "" Then
                If strFNat <> strNat Then
                    blInsert = False
                End If
            End If
            If strFKeycode <> "" Then
                If strFKeycode <> strKeycode Then
                    blInsert = False
                End If
            End If
            If blInsert = True Then ' insert the record
                strValues = frmData.tabData(0).GetFieldValues(i, "URNO,ADID,FLNO,STOD,ETDI,TIFD,DES3")   '"URNO,A/D,FLNO,Sched.,Est.,Act.,ORG/DES"
                tabFlights.InsertTextLine strValues, False
            End If
        End If
    Next i
    tabFlights.IndexCreate "URNO", 0
    tabFlights.SetInternalLineBuffer True
    tabFlights.Sort "5", True, True
    tabFlights.Refresh
    
    ''Just for debugging purpose
    'frmData.Show
    '''''
    
End Sub

Private Sub tabFlights_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strUrno As String
    Dim strTif As String
    Dim strAdid As String
    Dim datTif As Date
    
    If LineNo > -1 Then
        strUrno = tabFlights.GetFieldValue(LineNo, "URNO")
        strTif = tabFlights.GetFieldValue(LineNo, "ACTUAL")
        If strTif <> "" Then
            datTif = CedaFullDateToVb(strTif)
            datTif = DateAdd("h", -1, datTif)
        End If
        ''Call Sub
        SelectFlight strUrno
    End If
End Sub

Private Sub tabFlights_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim i As Long
    Dim cnt As Long
    
    If LineNo = -1 Then
        If ColNo = tabFlights.CurrentSortColumn Then
            If tabFlights.SortOrderASC = True Then
                tabFlights.Sort CStr(ColNo), False, True
            Else
                tabFlights.Sort CStr(ColNo), True, True
            End If
        Else
            tabFlights.Sort CStr(ColNo), True, True
        End If
        tabFlights.Refresh
    End If
End Sub

Private Sub tmrBcRefresh_Timer()
    Dim strRet As String

    intBcRefreshMinutes = intBcRefreshMinutes + 1
    If intBcRefreshMinutes = intBcRefreshInterval Then
        'Do we need to reload?
        'find in tabflnubcs
        strRet = tabFlnuBcs.GetLinesByColumnValue(0, omCurrentSelectedUrno, 0)
        If strRet = "" Then
            intBcRefreshMinutes = 0
        Else
            SelectFlight omCurrentSelectedUrno
        End If
    End If
End Sub

Private Sub txtAirline_GotFocus()
    SetTextSelected
    btnDoFilter.Default = True
End Sub

Private Sub txtAirline_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtAirline_LostFocus()
    btnDoFilter.Default = False
End Sub

Private Sub txtFlno_GotFocus()
    SetTextSelected
    btnDoFilter.Default = True
End Sub

Private Sub txtFlno_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtFlno_LostFocus()
    btnDoFilter.Default = False
End Sub

Private Sub txtKeycode_GotFocus()
    SetTextSelected
    btnDoFilter.Default = True
End Sub

Private Sub txtKeycode_LostFocus()
    btnDoFilter.Default = False
End Sub

Private Sub txtNature_GotFocus()
    SetTextSelected
    btnDoFilter.Default = True
End Sub

Private Sub txtNature_LostFocus()
    btnDoFilter.Default = False
End Sub

Private Sub txtOrigin_GotFocus()
    SetTextSelected
    btnDoFilter.Default = True
End Sub

Private Sub txtOrigin_LostFocus()
    btnDoFilter.Default = False
End Sub

Private Sub txtOrigin_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
'---------------------------
'End of filter flight region
'---------------------------

'igu on 23/08/2011: ALTEA
Private Sub CopyToSubLoaTab()
    Dim intFieldNo As Integer
    Dim intFieldCount As Integer
    Dim strHeaderLengthString As String
    Dim strLine As String
    Dim strLogicalFieldList As String
    Dim strDSSN As String
    Dim strFind As String
    Dim strFindKey As String
    Dim llCurrLine As Long
    Dim llLine As Long
    
    strLogicalFieldList = frmData.tabData(3).LogicalFieldList
    intFieldCount = UBound(Split(strLogicalFieldList, ","))
    With frmData.tabSubLOA
        .ResetContent
        .LogicalFieldList = strLogicalFieldList & ",ORDE"
        .HeaderString = strLogicalFieldList & ",ORDER"
        strHeaderLengthString = "100"
        For intFieldNo = 1 To intFieldCount
            strHeaderLengthString = strHeaderLengthString & ",100"
        Next intFieldNo
        .HeaderLengthString = strHeaderLengthString
        
        llLine = frmData.tabData(3).GetNextResultLine
        While llLine <> -1
            strLine = frmData.tabData(3).GetLineValues(llLine) & ","
            strDSSN = frmData.tabData(3).GetFieldValue(llLine, "DSSN")
            Select Case strDSSN
                Case "KRI"
                    strLine = strLine & "1"
                Case "ALT"
                    strLine = strLine & "1"
                Case "PTM"
                    strLine = strLine & "3"
            End Select
            
            If strDSSN = "KRI" Then
                strFindKey = "ALT," & frmData.tabData(3).GetFieldValues(llLine, "FLNO,TYPE,STYP,SSTP,SSST,APC3") 'igu on 03/04/2013
                strFind = frmData.tabSubLOA.GetLinesByMultipleColumnValue("3,1,5,6,7,8,9", strFindKey, 0) 'igu on 03/04/2013
                If strFind = "" Then
                    .InsertTextLine strLine, False
                Else
                    llCurrLine = Val(strFind)
                    .SetFieldValues llCurrLine, .LogicalFieldList, strLine
                End If
            Else
                .InsertTextLine strLine, False
            End If
            
            llLine = frmData.tabData(3).GetNextResultLine
        Wend
        
        .Sort CStr(intFieldCount) & ",2", True, True
        .ShowHorzScroller True
        .AutoSizeByHeader = True
    End With
End Sub

'---------------------------------------------------------
' Checks the connection times for the arrival and the
' departure flight and marks the TabLineNo of tabTab(1) red
'---------------------------------------------------------
Private Function CheckConnectionTimes(strArrUrno As String, strDepUrno As String, _
                                     TabLineNo As Long, TabLineNoDep As Long, _
                                     Optional setConflictColor As Boolean = True, Optional fromInitial As Boolean) As Boolean
    Dim blRet As Boolean
    Dim TabLineNoArr As Long
    Dim linNoConn As String
    Dim strRet As String
    Dim strAdid As String
    Dim strAlc As String
    Dim strAlc_Arr As String
    Dim strALC2 As String
    Dim strAlc3 As String
    Dim strAlc2_Arr As String
    Dim strAlc3_Arr As String
    Dim datStoa As Date
    Dim datStod As Date
    Dim datTifa As Date
    Dim datTifd As Date
    Dim strFLNO_D As String
    Dim strFLNO_A As String
    Dim llLine As Long
    Dim llConnTime As Integer
    Dim myField As String
    Dim myField_Arr As String
    Dim llDiff As Long
    Dim strVal As String
    Dim datArr As Date
    Dim datDep As Date
    Dim strArrTer As String
    Dim strDepTer As String
    Dim strSQTerMCT As String
    Dim strSQMITerMCT As String
    Dim strLine2 As Integer
    Dim column As String
    Dim fieldVal As String
    
    blRet = False
    strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strArrUrno, 0)
    TabLineNoArr = frmData.tabData(0).GetNextResultLine
    If TabLineNoArr <> -1 Then
        strAdid = frmData.tabData(0).GetFieldValue(TabLineNoArr, "ADID")
        If strAdid = "A" Then
            strVal = frmData.tabData(0).GetFieldValue(TabLineNoArr, "STOA")
            If Trim(strVal) <> "" Then
                datStoa = CedaFullDateToVb(frmData.tabData(0).GetFieldValue(TabLineNoArr, "STOA"))
            End If
            strVal = frmData.tabData(0).GetFieldValue(TabLineNoDep, "STOD")
            If Trim(strVal) <> "" Then
                datStod = CedaFullDateToVb(frmData.tabData(0).GetFieldValue(TabLineNoDep, "STOD"))
            End If
            strVal = frmData.tabData(0).GetFieldValue(TabLineNoArr, "ETAI")
            If Trim(strVal) <> "" Then
                datArr = CedaFullDateToVb(frmData.tabData(0).GetFieldValue(TabLineNoArr, "ETAI"))
            Else
                datArr = CedaFullDateToVb(frmData.tabData(0).GetFieldValue(TabLineNoArr, "STOA"))
            End If
            strVal = frmData.tabData(0).GetFieldValue(TabLineNoDep, "ETDI")
            If Trim(strVal) <> "" Then
                datDep = CedaFullDateToVb(frmData.tabData(0).GetFieldValue(TabLineNoDep, "ETDI"))
            Else
                datDep = CedaFullDateToVb(frmData.tabData(0).GetFieldValue(TabLineNoDep, "STOD"))
            End If
            strFLNO_A = frmData.tabData(0).GetFieldValue(TabLineNoArr, "FLNO")
            strFLNO_D = frmData.tabData(0).GetFieldValue(TabLineNoDep, "FLNO")
            'Cater for flight which do not have FLNO, taking call sign instead
'            If strFLNO_D = "" Then
'                strFLNO_D = frmData.tabData(0).GetFieldValue(TabLineNoDep, "CSGN")
'            End If
            strAlc = Mid(strFLNO_A, 1, 3)
            strAlc = Trim(strAlc)
            strAlc_Arr = Mid(strFLNO_D, 1, 3)
            strAlc_Arr = Trim(strAlc_Arr)
            If Len(strAlc) = 2 Then
                myField = "ALC2"
            End If
            If Len(strAlc) = 3 Then
                myField = "ALC3"
            End If
            If Len(strAlc_Arr) = 2 Then
                myField_Arr = "ALC2"
            End If
            If Len(strAlc_Arr) = 3 Then
                myField_Arr = "ALC3"
            End If
                                    
            column = "1,3"
            fieldVal = strAlc + "," + strAlc_Arr
            strRet = frmData.tabData(5).GetLinesByMultipleColumnValue(column, fieldVal, 0)
            'If strRet <> "ERROR;" Then
                'If strRet = -1 Or strRet = 0 Then                         'igu on 17 June 2009
                If strRet = "ERROR;" Or strRet = "-1" Or strRet = "0" Then 'strRet must be compared to string
                    'switch the column value to column = "3,1"
                    column = "3,1"
                    strRet = frmData.tabData(5).GetLinesByMultipleColumnValue(column, fieldVal, 0)
                End If
                'If strRet = -1 Or strRet = 0 Then                         'igu on 17 June 2009
                If strRet = "ERROR;" Or strRet = "-1" Or strRet = "0" Then 'strRet must be compared to string
                    strRet = frmData.tabData(5).GetLinesByIndexValue(myField, strAlc, 0)
                End If
            'End If
            
            llLine = frmData.tabData(5).GetNextResultLine
            If strAlc = strAlc_Arr Then
                strAlc = strAlc
            End If
            If llLine <> -1 Then
                If strAlc = strAlc_Arr Then
                    If Val(frmData.tabData(5).GetFieldValue(llLine, "PSAM")) = 0 Then
                        llConnTime = Val(frmData.tabData(5).GetFieldValue(llLine, "PCON"))
                    Else
                        llConnTime = Val(frmData.tabData(5).GetFieldValue(llLine, "PSAM"))
                    End If
                Else
                    llConnTime = Val(frmData.tabData(5).GetFieldValue(llLine, "PCON"))
                End If
            Else
                llConnTime = Val(strPaxDefault)
            End If
            
            'Solution to cater MI/SQ, SQ/SQ on different terminal
            strSQTerMCT = GetIniEntry("", "HUBMANAGER", "GLOBAL", "SQTERMINAL_MCT", "")
            strSQMITerMCT = GetIniEntry("", "HUBMANAGER", "GLOBAL", "SQMITERMINAL_MCT", "")
            strArrTer = frmData.tabData(0).GetFieldValue(TabLineNoArr, "TGA1")
            strDepTer = frmData.tabData(0).GetFieldValue(TabLineNoDep, "TGD1")
            
            If strSQTerMCT = "" Then strSQTerMCT = "0"
            If strSQMITerMCT = "" Then strSQMITerMCT = "0"
            llDiff = DateDiff("n", datArr, datDep)
            
            If strAlc = "SQ" And strAlc_Arr = "SQ" Then
                'KKH PRF conflict colour appear occasionally
                blRet = SetWarningBground(strArrTer, strDepTer, llDiff, llConnTime, strSQTerMCT, TabLineNo, setConflictColor, fromInitial)
            Else
                If (strAlc = "SQ" And strAlc_Arr = "MI") Or (strAlc = "MI" And strAlc_Arr = "SQ") Then
                    'KKH PRF conflict colour appear occasionally
                    blRet = SetWarningBgroundSQMI(strArrTer, strDepTer, llDiff, llConnTime, strSQMITerMCT, TabLineNo, setConflictColor, fromInitial)
                Else
                    If llDiff < llConnTime Then
                        If fromInitial = False Then
                            If TabLineNo >= 0 And setConflictColor = True Then
                                tabTab(1).SetLineColor TabLineNo, vbBlack, vbRed
                            End If
                        End If
                        blRet = True
                    End If
                End If
            End If
        End If
    End If
    CheckConnectionTimes = blRet
End Function

Private Function SetWarningBground(strArrTer As String, strDepTer As String, strDIFF As Long, strConn As Integer, strConfigVal As String, strTabline As Long, Optional configColour As Boolean = True, Optional fromInitial As Boolean) As Boolean
    Dim blRetW As Boolean
    Dim strTightConn As String
    blRetW = False
    Dim temp As Integer
    strTightConn = GetIniEntry("", "HUBMANAGER", "GLOBAL", "TIGHTCONNECTION", "")
    If strTightConn = "" Then strTightConn = "0"
    If strArrTer <> strDepTer Then
        'kkh set conflict colour as warning if llDiff(transfer time) between llConnTime(MCT) + strConfigVal(XX_TERMINAL_MCT) and (llConnTime(MCT) + strConfigVal(XX_TERMINAL_MCT) + strTightConn)
        If strDIFF < strConn Then
            If fromInitial = False Then
                If strTabline >= 0 And configColour = True Then
                    tabTab(1).SetLineColor strTabline, vbBlack, vbRed
                End If
            End If
            blRetW = True
        Else
            If strDIFF < (strConn + CInt(strConfigVal)) Then
                If fromInitial = False Then
                    If strTabline >= 0 And configColour = True Then
                        'set to bright ORANGE colour on 26/12/2007
                        tabTab(1).SetLineColor strTabline, vbBlack, colBrightOrange  'bright orange
                        'warning colour orange
                        'compressedFlightBarCritColour = True
                    End If
                End If
                blRetW = True
            Else
                If strDIFF < (strConn + CInt(strConfigVal) + CInt(strTightConn)) Then
                    If fromInitial = False Then
                        If strTabline >= 0 And configColour = True Then
                            'set to yellow colour on 26/12/2007
                            tabTab(1).SetLineColor strTabline, vbBlack, colLightYellow  'light yellow
                            blRetW = True
                            'warning colour orange
                            'compressedFlightBarWarnColour = True
                        End If
                    End If
                End If
            End If
        End If
    Else
        If strDIFF < strConn Then
            If fromInitial = False Then
                If strTabline >= 0 And configColour = True Then
                    tabTab(1).SetLineColor strTabline, vbBlack, vbRed
                    'warning colour orange
                    'compressedFlightBarCritColour = True
                End If
            End If
            blRetW = True
        End If
    End If
    SetWarningBground = blRetW
    
End Function

Private Function SetWarningBgroundSQMI(strArrTer As String, strDepTer As String, strDIFF As Long, strConn As Integer, strConfigVal As String, strTabline As Long, Optional configColour As Boolean = True, Optional fromInitial As Boolean) As Boolean
    Dim blRetW As Boolean
    Dim strTightConn As String
    blRetW = False
    Dim temp As Integer
    strTightConn = GetIniEntry("", "HUBMANAGER", "GLOBAL", "TIGHTCONNECTION", "")
    If strTightConn = "" Then strTightConn = "0"
    If strArrTer <> strDepTer Then
        'kkh set orange colour as warning if llDiff(transfer time) between llConnTime(MCT) + strConfigVal(XX_TERMINAL_MCT) and (llConnTime(MCT) + strConfigVal(XX_TERMINAL_MCT) + strTightConn)
        If strDIFF < (strConn + CInt(strConfigVal)) Then
            If fromInitial = False Then
                If strTabline >= 0 And configColour = True Then
                    'set to bright ORANGE colour on 26/12/2007
                    tabTab(1).SetLineColor strTabline, vbBlack, colOrange
                    'warning colour orange
                    'compressedFlightBarCritColour = True
                End If
            End If
            blRetW = True
        Else
            If strDIFF < (strConn + CInt(strConfigVal) + CInt(strTightConn)) Then
                If fromInitial = False Then
                If strTabline >= 0 And configColour = True Then
                    'set to yellow colour on 26/12/2007
                    tabTab(1).SetLineColor strTabline, vbBlack, colLightYellow 'light yellow
                    blRetW = True
                    'warning colour orange
                    'compressedFlightBarWarnColour = True
                End If
            End If
        End If
        End If
    Else
        If strDIFF < strConn Then
            If fromInitial = False Then
                If strTabline >= 0 And configColour = True Then
                    tabTab(1).SetLineColor strTabline, vbBlack, colOrange
                    'warning colour orange
                    'compressedFlightBarCritColour = True
                End If
            End If
            blRetW = True
        End If
    End If
    SetWarningBgroundSQMI = blRetW
    
End Function

'---------------------------------------------------------
' Builds the Timeframe of TIFA and TIFD and sets it to the
' format 00:00 means x hours and y minutes and returns it as
' string
'---------------------------------------------------------
Private Function GetTransferTime(strArrUrno As String, strDepUrno As String) As String
    Dim llLine As Long
    Dim strStoa As String
    Dim strStod As String
    Dim datTifa As Date
    Dim datTifd As Date
    Dim strRet As String
    Dim llDiff As Long
    Dim llHour As Long
    Dim llMinute As Long
    Dim strEtai As String
    Dim datArr As Date
    Dim strEtdi As String
    Dim datDep As Date
    Dim datDiff As Date
    
    'warning colour orange
'    compressedFlightBarWarnColour = False
'    compressedFlightBarCritColour = False
    strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strArrUrno, 0)
    llLine = frmData.tabData(0).GetNextResultLine
    If llLine > -1 Then
        strStoa = frmData.tabData(0).GetFieldValue(llLine, "STOA")
        strEtai = frmData.tabData(0).GetFieldValue(llLine, "ETAI")
    End If
    strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strDepUrno, 0)
    llLine = frmData.tabData(0).GetNextResultLine
    If llLine > -1 Then
        strStod = frmData.tabData(0).GetFieldValue(llLine, "STOD")
        strEtdi = frmData.tabData(0).GetFieldValue(llLine, "ETDI")
    End If
    If strEtai <> "" Then
        datArr = CedaFullDateToVb(strEtai)
    Else
        datArr = CedaFullDateToVb(strStoa)
    End If
    If strEtdi <> "" Then
        datDep = CedaFullDateToVb(strEtdi)
    Else
        datDep = CedaFullDateToVb(strStod)
    End If
'    If CStr(datArr) <> "" And CStr(datDep) <> "" Then
'        llDiff = DateDiff("n", datArr, datDep)
'        llHour = Val(llDiff / 60)
'
'        If (llDiff / 60) < 1 Then
'            llHour = "0"
'        Else
'            llHour = Fix(llDiff / 60)
'        End If
'
'        llMinute = Val(llDiff Mod 60)
'    End If
    If CStr(datArr) <> "" And CStr(datDep) <> "" Then
        llDiff = DateDiff("n", datArr, datDep)
        'llHour = Val(llDiff / 60)
        
        llHour = Fix(llDiff / 60)
        llMinute = Val(llDiff Mod 60)
        'llFinalDiff = llDiff / 60
    End If
    'kkh on 05/11/2008 display minus transfer timing correctly
    If llMinute < 0 Or llHour < 0 Then
        GetTransferTime = "-" + Right("000" & Replace(CStr(llHour), "-", ""), 2) + ":" + Right("000" & Replace(CStr(llMinute), "-", ""), 2)
    Else
        GetTransferTime = Right("000" & CStr(llHour), 2) + ":" + Right("000" & CStr(llMinute), 2)
    End If
    
    'GetTransferTime = Right("000" & CStr(llHour), 2) + ":" + Right("000" & CStr(llMinute), 2)
    
End Function

'kkh on 09/04/2008 GOCC PRF 0000013: Connecting flight data does not match PTM
Private Function GetCorrectDepartureByFlightDay(datStoa As Date, opDay As String, strFlno As String, Optional strAddi As String) As Long
    Dim llAftLine As Long
    Dim strValues As String
    Dim i As Long
    Dim datStod As Date
    Dim datTifa As Date
    Dim strUrno As String
    Dim ret As Long
    Dim strValue As String
    Dim strAdid As String
    Dim dtmFlightDateInUTC As String 'igu on 2 June 2009; store STOD in UTC
    Dim strDayOfFlight As String 'igu on 2 June 2009; store the day of STOD in UTC
    
    tabTmpFlights.ResetContent
    tabTmpFlights.HeaderString = "URNO,FLNO,STOD,ADID"
    tabTmpFlights.LogicalFieldList = "URNO,FLNO,STOD,ADID"
    tabTmpFlights.HeaderLengthString = "60,60,100,40"
    
    ret = -1
    frmData.tabData(0).GetLinesByIndexValue "FLNO", strFlno, 0
    llAftLine = frmData.tabData(0).GetNextResultLine
    'Store it on tabTmpFlights
    While llAftLine <> -1
        strValues = frmData.tabData(0).GetFieldValues(llAftLine, "URNO,FLNO,STOD,ADID")
        tabTmpFlights.InsertTextLine strValues, False
        llAftLine = frmData.tabData(0).GetNextResultLine
    Wend
    tabTmpFlights.Sort "2", True, True
    For i = 0 To tabTmpFlights.GetLineCount - 1
        strValue = tabTmpFlights.GetFieldValue(i, "STOD")
        strValue = Trim(strValue)
        strAdid = tabTmpFlights.GetFieldValue(i, "ADID")
        If opDay <> "" And strAdid = "D" Then
            If strValue <> "" Then
                strValue = Mid(strValue, 7, 2)
                If strValue = opDay Then
                    strUrno = tabTmpFlights.GetFieldValue(i, "URNO")
                    i = tabTmpFlights.GetLineCount
                End If
            End If
        Else
            If strValue <> "" Then
                datStod = CedaFullDateToVb(strValue)
                'kkh on 09/04/2008 GOCC PRF 0000013: Connecting flight data does not match PTM
                If strAddi <> "" Then
                    '--------------
                    'Date: Juni 2nd 2009
                    'Desc: change STOD from local time to UTC
                    'By  : igu
                    '--------------
                    dtmFlightDateInUTC = CedaFullDateToVb(strValue)
                    dtmFlightDateInUTC = DateAdd("h", -UTCOffset, dtmFlightDateInUTC)
                    strDayOfFlight = Format(dtmFlightDateInUTC, "YYYYMMDDhhmmss")
                    strDayOfFlight = Mid(strDayOfFlight, 7, 2)
                    '--------------
                    If strDayOfFlight = strAddi Then
                        If datStod > datStoa And strAdid = "D" Then
                            strUrno = tabTmpFlights.GetFieldValue(i, "URNO")
                            i = tabTmpFlights.GetLineCount
                        End If
                    End If
                Else
                    If datStod > datStoa And strAdid = "D" Then
                          strUrno = tabTmpFlights.GetFieldValue(i, "URNO")
                          i = tabTmpFlights.GetLineCount
                    End If
                End If
            End If
        End If
    Next i
    If strUrno <> "" Then
        frmData.tabData(0).GetLinesByIndexValue "URNO", strUrno, 0
        ret = frmData.tabData(0).GetNextResultLine
    End If
    GetCorrectDepartureByFlightDay = ret
End Function

Private Sub MarkLineAsHold(tabLine As Long, strUrno As String)
    Dim llLine As Long
    Dim strValues As String
    Dim i As Long
    Dim cnt As Long
    
    frmData.tabHold.GetLinesByIndexValue "URNO", strUrno, 0
    llLine = frmData.tabHold.GetNextResultLine
    If llLine = -1 Then
        strValues = strUrno + ",H"
        frmData.tabHold.InsertTextLine strValues, False
        frmData.tabHold.Sort "0", True, True
    End If
    cnt = ItemCount(tabTab(1).LogicalFieldList, ",")
    For i = 0 To cnt - 1
        tabTab(1).ResetCellDecoration tabLine, i
        tabTab(1).SetDecorationObject tabLine, i, "HOLD"
    Next i
    'tabTab(1).SetFieldValues tabLine, "Hold", "H"
    tabTab(1).Refresh
End Sub

'---------------------------------------------------------
' Fetches all corresponding data for the urno(KEY) from
' loatab and puts it into the fields of the transfer tab
'---------------------------------------------------------
Private Sub SetLoaTabInfoToTab(Key As String, opStoa As Date)
    Dim i As Long
    Dim strVal As String
    Dim strEmptLine As String
    Dim llLine As Long
    Dim llDepLine As Long
    Dim llAftLine As Long
    Dim llPaxTot As Long
    Dim strRawFlno As String
    Dim strFlno As String
    Dim strPriorFlno As String
    Dim strFlightDay As String
    Dim strLineValues As String
    Dim llCurrLine As Long
    Dim strAdid As String
    Dim arr() As String
    Dim strSTYP As String
    Dim datStod As Date
    Dim strVALU As String
    Dim strArrUrno As String
    Dim strDepUrno As String
    Dim blCorrectTimeFound As Boolean
    Dim llF As Long, llB As Long, llE As Long, llU As Long
    Dim llBagTotal As Long, llBwtTotal As Long
    Dim strTmpEBag As Long, strTmpFBag As Long, strTmpBBag As Long, strTmpUBag As Long
    Dim strTmpEBwt As Long, strTmpFBwt As Long, strTmpBBwt As Long, strTmpUBwt As Long
    Dim dummy1 As String, dummy2 As String, dummy3 As String
    Dim minDate As Date ' for the detail gantt
    Dim maxDate As Date ' for the detail gantt
    Dim TFStart As Date ' for the detail gantt
    Dim TFEnd As Date ' for the detail gantt
    Dim llCurrDetailLine As Long
    Dim timeframe As Long
    Dim strMinDate As String
    Dim strMaxDate As String
    Dim isFirst As Boolean
    Dim strLoaUrno As String
    'kkh on 16/07/2008 P2 Store Decision into CFITAB
    Dim strRet2 As String
    Dim fieldVal As String
    Dim strCFIAddi As String
    'kkh on 04/07/2008 If already Airbone (AIRB <> "") do not display in the list
    Dim strShowAIRB As String
    Dim arrDel() As String
    Dim cntDel As Integer
    cntDel = 0
    strShowAIRB = GetIniEntry("", "HUBMANAGER", "GLOBAL", "AIRBORNE_SHOW", "")
    'kkh 06/02/2009 APC3
    Dim strLoaApc3 As String
    Dim strLoaAddi As String
       
    'kkh on 06/01/2009 via info
    Dim strVIAL As String
    Dim viaLen As Integer
    Dim tmpVia As String
    
    Dim strRawFlnoCopy As String
    Dim dummyDate As Date
    Dim strLoaTabUrnos As String
    Dim strLUrno As String
    Dim strTmp As String
    Dim strDSSN As String
    Dim strTYPE As String
    Dim tmpPax As Integer
    Dim tmpStrPax As String
    Dim llSRLLine As Long
    Dim strSRLText As String
    
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    Dim str4 As String
    Dim arrTotalBagY() As String
    Dim arrTotalBagF() As String
    Dim arrTotalBagC() As String
    Dim arrTotalBagU() As String
    
    'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights
    Dim strCheckFlno As String
    Dim byPassPTM As Boolean
    Dim strShowKRIPrio As String
    Dim llTempLine As Long 'igu on 23/08/2011: ALTEA
    Dim strPriorDSSN As String 'igu on 19/02/2012
    
    strShowKRIPrio = GetIniEntry("", "HUBMANAGER", "GLOBAL", "KRI_PRIO", "")
    
    strTmpEBag = 0
    strTmpFBag = 0
    strTmpBBag = 0
    strTmpUBag = 0
    
    strTmpEBwt = 0
    strTmpFBwt = 0
    strTmpBBwt = 0
    strTmpUBwt = 0
    
    llBagTotal = 0
    llBwtTotal = 0
            
    llCurrDetailLine = 0
    isFirst = True
    TFStart = Now
    strMinDate = Format(TFStart, "YYYYMMDD000000")
    strMaxDate = Format(TFStart, "YYYYMMDD235959")
    TFStart = CedaFullDateToVb(strMaxDate)
    TFEnd = CedaFullDateToVb(strMinDate)
    tabTab(1).ResetContent
    frmData.tabData(3).GetLinesByIndexValue "FLNU", Key, 0
    'llLine = frmData.tabData(3).GetNextResultLine 'igu on 23/08/2011: ALTEA
    Call CopyToSubLoaTab 'igu on 23/08/2011: ALTEA
    
    strPriorFlno = ""
    llCurrLine = 0
                  
    'strEmptLine = ",,,,,,,,,,,,,,,,,"
    'strEmptLine = ",,,,,,,,,,,,,,,,,,,,,,,"
    strEmptLine = ",,,,,,,,,,,,,,,,,,,,,,,,,"
    tabTab(1).InsertTextLine strEmptLine, False
    strArrUrno = Key
    'While llLine <> -1 'igu on 23/08/2011: ALTEA
    For llLine = 0 To frmData.tabSubLOA.GetLineCount - 1 'igu on 23/08/2011: ALTEA
        blCorrectTimeFound = False
        'PHYOE
        'UFIS-4039
        'Initialise the output parameters from StripAftFlno function
        dummy1 = ""
        dummy2 = ""
        dummy3 = ""
        
'        ' Insert an empty line
'        strRawFlno = frmData.tabData(3).GetFieldValue(llLine, "FLNO")
        'strRawFlno = frmData.tabData(3).GetFieldValue(llLine, "FLNO") 'igu on 23/08/2011: ALTEA
        strRawFlno = frmData.tabSubLOA.GetFieldValue(llLine, "FLNO") 'igu on 23/08/2011: ALTEA
        
        'strLoaUrno = frmData.tabData(3).GetFieldValue(llLine, "RURN") 'igu on 23/08/2011: ALTEA
        strLoaUrno = frmData.tabSubLOA.GetFieldValue(llLine, "RURN") 'igu on 23/08/2011: ALTEA
        strDSSN = frmData.tabSubLOA.GetFieldValue(llLine, "DSSN") 'igu on 09/02/2012
        'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights
        byPassPTM = False
        
        'kkh KRI_PRIO to control whether to show both KRISCOM and PTM message in screen 3
        If strShowKRIPrio = "YES" Then
            If strDSSN = "PTM" Then 'igu on 09/02/2012
            'If frmData.tabSubLOA.GetFieldValue(llLine, "DSSN") = "PTM" Then 'igu on 10/01/2012: ALTEA
            'If frmData.tabData(3).GetFieldValue(llLine, "DSSN") = "PTM" Then
                If Len(strRawFlno) = "6" And Mid(strRawFlno, 3, 1) = 0 Then
                    strCheckFlno = Mid(strRawFlno, 1, 2) + Mid(strRawFlno, 4, 3)
                Else
                    strCheckFlno = strRawFlno
                End If
                For i = 0 To tabTab(1).GetLineCount - 1
                    If strCheckFlno = Replace(tabTab(1).GetFieldValue(i, "FLNO"), " ", "") And strLoaUrno <> tabTab(1).GetFieldValue(i, "LOA_RURN") Then
                        byPassPTM = True
                    End If
                Next i
                byPassPTM = True
            End If
        End If
        
        'kkh 06/02/2009 APC3
        'strLoaApc3 = frmData.tabData(3).GetFieldValue(llLine, "APC3") 'igu on 23/08/2011: ALTEA
        strLoaApc3 = frmData.tabSubLOA.GetFieldValue(llLine, "APC3") 'igu on 23/08/2011: ALTEA
        'strLoaAddi = frmData.tabData(3).GetFieldValue(llLine, "ADDI") 'igu on 23/08/2011: ALTEA
        strLoaAddi = frmData.tabSubLOA.GetFieldValue(llLine, "ADDI") 'igu on 23/08/2011: ALTEA
        arr = Split(strRawFlno, "/")
        If UBound(arr) = 1 Then
            strFlno = arr(0)
            strFlightDay = arr(1)
        Else
            strFlno = strRawFlno
            strFlightDay = ""
        End If
        ''?????????????
        ''PHYOE
        ''Why specific flight no define here?????
        If strFlno = "QF031" Then
            strFlno = strFlno
        End If
        strFlno = StripAftFlno(strFlno, dummy1, dummy2, dummy3)
        'kkh on 21/04/2008
        strLoaAddi = Mid(Trim(strLoaAddi), 14, 2)
        If strLoaAddi <> "" Then
            'kkh 06/02/2009 APC3
            'strRawFlno = strFlno + "/" + strLoaAddi
            strRawFlno = strFlno + "/" + strLoaAddi + "/" + strLoaApc3
        End If
        If UBound(arr) = 1 Then
            If arr(1) <> "" Then
                strRawFlno = strFlno + "/" + strFlightDay
            End If
        End If
        
        If byPassPTM = False Then
            If strRawFlno & strDSSN <> strPriorFlno & strPriorDSSN Then
                'MWOXXX If strFlno <> strPriorFlno Then
                'kkh on 09/04/2008 GOCC PRF 0000013: Connecting flight data does not match PTM
                'llAftLine = GetCorrectDepartureByFlightDay(opStoa, strFlightDay, strFlno)
                'strLoaAddi = Mid(Trim(strLoaAddi), 14, 2)
                llAftLine = GetCorrectDepartureByFlightDay(opStoa, strFlightDay, strFlno, strLoaAddi)
    
                If Trim(strFlightDay) <> "" Then
                    strRawFlnoCopy = strFlno + "/" + strFlightDay
                Else
                    strRawFlnoCopy = strFlno
                End If
                'llAftLine = GetCorrectDepartureLine(Key, opStoa, strFlno)
                If llAftLine <> -1 Then
                    strAdid = frmData.tabData(0).GetFieldValue(llAftLine, "ADID")
                    If strAdid = "D" Then 'only if departure
                        strDepUrno = frmData.tabData(0).GetFieldValue(llAftLine, "URNO")
                            
                        'kkh on 06/01/2009 via info
                        strVIAL = frmData.tabData(0).GetFieldValue(llAftLine, "VIAL")
                        tabTab(1).SetFieldValues llCurrLine, "AFTURNO", strDepUrno
                        tabTab(1).SetFieldValues llCurrLine, "LOA_RURN", strLoaUrno 'Ist die RURN = TelexUrno to find the telex immediately
                        tabTab(1).SetFieldValues llCurrLine, "ACT", frmData.tabData(0).GetFieldValue(llAftLine, "ACT3")
                        tabTab(1).SetFieldValues llCurrLine, "FLNO", strRawFlnoCopy
                        tabTab(1).SetFieldValues llCurrLine, "TGD_D", frmData.tabData(0).GetFieldValue(llAftLine, "TGD1")
                        tabTab(1).SetFieldValues llCurrLine, "GATE", frmData.tabData(0).GetFieldValue(llAftLine, "GTD1")
                        tabTab(1).SetFieldValues llCurrLine, "STD", frmData.tabData(0).GetFieldValue(llAftLine, "STOD")
                        tabTab(1).SetFieldValues llCurrLine, "EST", frmData.tabData(0).GetFieldValue(llAftLine, "ETDI")
                        tabTab(1).SetFieldValues llCurrLine, "ATD", frmData.tabData(0).GetFieldValue(llAftLine, "AIRB")
                        'kkh 06/02/2009 APC3
                        tabTab(1).SetFieldValues llCurrLine, "DES", strLoaApc3
                        tabTab(1).SetFieldValues llCurrLine, "NA", frmData.tabData(0).GetFieldValue(llAftLine, "TTYP")
                        'kkh 17/02/2009
                        If strVIAL <> "" Then
                            viaLen = Len(strVIAL)
                            If viaLen < 120 Then
                                tmpVia = Mid(strVIAL, 2, 3) ' & "/" & lblDep(14).Tag
                            End If
                            If viaLen > 120 And viaLen < 240 Then
                                tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3)
                            End If
                            If viaLen > 240 And viaLen < 360 Then
                                tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3)
                            End If
                            If viaLen > 360 And viaLen < 480 Then
                                tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3) & "/" & Mid(strVIAL, 362, 3)
                            End If
                            If viaLen > 480 And viaLen < 600 Then
                                tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3) & "/" & Mid(strVIAL, 362, 3) & "/" & Mid(strVIAL, 482, 3)
                            End If
                            If viaLen > 600 And viaLen < 720 Then
                                tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3) & "/" & Mid(strVIAL, 362, 3) & "/" & Mid(strVIAL, 482, 3) & "/" & Mid(strVIAL, 602, 3)
                            End If
                            If viaLen > 720 And viaLen < 800 Then
                                tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3) & "/" & Mid(strVIAL, 362, 3) & "/" & Mid(strVIAL, 482, 3) & "/" & Mid(strVIAL, 602, 3) & "/" & Mid(strVIAL, 722, 3)
                            End If
                            If viaLen > 800 And viaLen < 920 Then
                                tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3) & "/" & Mid(strVIAL, 362, 3) & "/" & Mid(strVIAL, 482, 3) & "/" & Mid(strVIAL, 602, 3) & "/" & Mid(strVIAL, 722, 3) & "/" & Mid(strVIAL, 902, 3)
                            End If
                            If viaLen > 920 And viaLen < 1040 Then
                                tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3) & "/" & Mid(strVIAL, 362, 3) & "/" & Mid(strVIAL, 482, 3) & "/" & Mid(strVIAL, 602, 3) & "/" & Mid(strVIAL, 722, 3) & "/" & Mid(strVIAL, 902, 3) & "/" & Mid(strVIAL, 1022, 3)
                            End If
                            tmpVia = tmpVia & "/" & frmData.tabData(0).GetFieldValue(llAftLine, "DES3")
                            tabTab(1).SetFieldValues llCurrLine, "VIAnDES", tmpVia
                        Else
                            tabTab(1).SetFieldValues llCurrLine, "VIAnDES", frmData.tabData(0).GetFieldValue(llAftLine, "DES3")
                        End If
                            
                        tabTab(1).SetFieldValues llCurrLine, "NA", frmData.tabData(0).GetFieldValue(llAftLine, "TTYP")
                        'kkh on 16/07/2008 P2 Store Decision into CFITAB
                        strCFIAddi = ""
                        fieldVal = Key + "," + strDepUrno
                        'strRet2 = frmData.tabData(13).GetLinesByMultipleColumnValue("1,2", fieldVal, 0)
                        strRet2 = frmData.tabData(13).GetLinesByMultipleColumnValue("5,22", fieldVal, 0)
                        
                        If strRet2 <> "" Then
                            If InStr(strRet2, ",") > 0 Then strRet2 = Split(strRet2, ",")(0) 'igu on 27 Jul 2010
                            
                            strCFIAddi = frmData.tabData(13).GetFieldValue(strRet2, "ADDI")
                            tabTab(1).SetFieldValues llCurrLine, "Hold", strCFIAddi
                            tabTab(1).SetFieldValues llCurrLine, "CFI_URNO", frmData.tabData(13).GetFieldValue(strRet2, "URNO")
                        End If
                        If frmData.tabData(0).GetFieldValue(llAftLine, "OFBL") = "" Then
                            strSRLText = GetTransferTime(strArrUrno, strDepUrno)
                        Else
                            strSRLText = ""
                        End If
                        tabTab(1).SetFieldValues llCurrLine, "TRANSFER", strSRLText
                        
                        If strSRLText = "Waiting" Then
                            'tabTab(1).SetFieldValues llCurrLine, "Hold", "H"
                            MarkLineAsHold llCurrLine, strDepUrno
                        End If
                        'KKH PRF conflict colour appear occasionally
                        CheckConnectionTimes strArrUrno, strDepUrno, llCurrLine, llAftLine, , False
                    End If
                Else 'llAftLine = -1
                    'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights
                    tabTab(1).SetFieldValues llCurrLine, "LOA_RURN", strLoaUrno
                    ' Flight was not found ==> set linecolor to yellow
                    tabTab(1).SetLineColor llCurrLine, vbBlack, colYellow
                    tabTab(1).SetFieldValues llCurrLine, "FLNO", strFlno
                End If '//EndIf llAftLine <> -1
            End If '//EndIf strRawFlno <> strPriorFlno
            
            'strSTYP = frmData.tabData(3).GetFieldValue(llLine, "STYP") 'igu on 23/08/2011: ALTEA
            strSTYP = frmData.tabSubLOA.GetFieldValue(llLine, "STYP") 'igu on 23/08/2011: ALTEA
            'strVALU = frmData.tabData(3).GetFieldValue(llLine, "VALU") 'igu on 23/08/2011: ALTEA
            strVALU = frmData.tabSubLOA.GetFieldValue(llLine, "VALU") 'igu on 23/08/2011: ALTEA
            'strDSSN = frmData.tabData(3).GetFieldValue(llLine, "DSSN") 'igu on 23/08/2011: ALTEA
            'strDSSN = frmData.tabSubLOA.GetFieldValue(llLine, "DSSN") 'igu on 23/08/2011: ALTEA 'igu on 09/02/2012
            'strTYPE = frmData.tabData(3).GetFieldValue(llLine, "TYPE") 'igu on 23/08/2011: ALTEA
            strTYPE = frmData.tabSubLOA.GetFieldValue(llLine, "TYPE") 'igu on 23/08/2011: ALTEA
            'kkh 12/06/2008 APC3
            'strLoaApc3 = frmData.tabData(3).GetFieldValue(llLine, "APC3") 'igu on 23/08/2011: ALTEA
            strLoaApc3 = frmData.tabSubLOA.GetFieldValue(llLine, "APC3") 'igu on 23/08/2011: ALTEA
            'If strDSSN = "PTM" Or strDSSN = "KRI" Then 'igu on 23/08/2011: ALTEA
            If strDSSN = "PTM" Or strDSSN = "KRI" Or strDSSN = "ALT" Then 'igu on 23/08/2011: ALTEA
                Select Case (strSTYP)
                    Case "E"
                        tmpPax = Val(tabTab(1).GetFieldValue(llCurrLine, "Y-PAX"))
                        'kkh on 05/05/2008 MANTIS PRF : 0000014
                        'If (strDSSN = "KRI" And strTYPE = "PAX") Or (strDSSN = "PTM" And tmpPax = 0 And strTYPE = "PAX") Then 'igu on 23/08/2011: ALTEA
                        If ((strDSSN = "KRI" Or strDSSN = "ALT") And strTYPE = "PAX") Or (strDSSN = "PTM" And tmpPax = 0 And strTYPE = "PAX") Then 'igu on 23/08/2011: ALTEA
                            tabTab(1).SetFieldValues llCurrLine, "Y-PAX", strVALU
                            'strTmp = frmData.tabData(3).GetFieldValue(llLine, "URNO") 'igu on 23/08/2011: ALTEA
                            strTmp = frmData.tabSubLOA.GetFieldValue(llLine, "URNO") 'igu on 23/08/2011: ALTEA
                            If InStr(strLoaTabUrnos, strTmp) = 0 Then
                                strLoaTabUrnos = strLoaTabUrnos + strTmp + ";"
                            End If
                        End If
                        If strRawFlno = strPriorFlno Then
                            If strTYPE = "BAG" Then
                                strTmpEBag = strVALU
                            End If
                            If strTYPE = "BWT" Then
                                strTmpEBwt = strVALU
                            End If
                        Else
                            strTmpEBag = 0
                            strTmpEBwt = 0
                            If strTYPE = "BAG" Then
                                strTmpEBag = strVALU
                            End If
                            If strTYPE = "BWT" Then
                                strTmpEBwt = strVALU
                            End If
                        End If
                        tabTab(1).SetFieldValues llCurrLine, "Y-PC/WT", CStr(strTmpEBag) + "/" + CStr(strTmpEBwt)
                    Case "F"
                        tmpPax = Val(tabTab(1).GetFieldValue(llCurrLine, "F-PAX"))
                        'kkh on 05/05/2008 MANTIS PRF : 0000014
                        'If (strDSSN = "KRI" And strTYPE = "PAX") Or (strDSSN = "PTM" And tmpPax = 0 And strTYPE = "PAX") Then 'igu on 23/08/2011: ALTEA
                        If ((strDSSN = "KRI" Or strDSSN = "ALT") And strTYPE = "PAX") Or (strDSSN = "PTM" And tmpPax = 0 And strTYPE = "PAX") Then 'igu on 23/08/2011: ALTEA
                            tabTab(1).SetFieldValues llCurrLine, "F-PAX", strVALU
                            'strTmp = frmData.tabData(3).GetFieldValue(llLine, "URNO") 'igu on 23/08/2011: ALTEA
                            strTmp = frmData.tabSubLOA.GetFieldValue(llLine, "URNO") 'igu on 23/08/2011: ALTEA
                            If InStr(strLoaTabUrnos, strTmp) = 0 Then
                                strLoaTabUrnos = strLoaTabUrnos + strTmp + ";"
                            End If
                        End If
                        If strRawFlno = strPriorFlno Then
                            If strTYPE = "BAG" Then
                                strTmpFBag = strVALU
                            End If
                            If strTYPE = "BWT" Then
                                strTmpFBwt = strVALU
                            End If
                        Else
                            strTmpFBag = 0
                            strTmpFBwt = 0
                            If strTYPE = "BAG" Then
                                strTmpFBag = strVALU
                            End If
                            If strTYPE = "BWT" Then
                                strTmpFBwt = strVALU
                            End If
                        End If
                        tabTab(1).SetFieldValues llCurrLine, "F-PC/WT", CStr(strTmpFBag) + "/" + CStr(strTmpFBwt)
                    Case "B"
                        tmpPax = Val(tabTab(1).GetFieldValue(llCurrLine, "C-PAX"))
                        'kkh on 05/05/2008 MANTIS PRF : 0000014
                        'If (strDSSN = "KRI" And strTYPE = "PAX") Or (strDSSN = "PTM" And tmpPax = 0 And strTYPE = "PAX") Then 'igu on 23/08/2011: ALTEA
                        If ((strDSSN = "KRI" Or strDSSN = "ALT") And strTYPE = "PAX") Or (strDSSN = "PTM" And tmpPax = 0 And strTYPE = "PAX") Then 'igu on 23/08/2011: ALTEA
                            tabTab(1).SetFieldValues llCurrLine, "C-PAX", strVALU
                            'strTmp = frmData.tabData(3).GetFieldValue(llLine, "URNO") 'igu on 23/08/2011: ALTEA
                            strTmp = frmData.tabSubLOA.GetFieldValue(llLine, "URNO") 'igu on 23/08/2011: ALTEA
                            If InStr(strLoaTabUrnos, strTmp) = 0 Then
                                strLoaTabUrnos = strLoaTabUrnos + strTmp + ";"
                            End If
                        End If
                        If strRawFlno = strPriorFlno Then
                            If strTYPE = "BAG" Then
                                strTmpBBag = strVALU
                            End If
                            If strTYPE = "BWT" Then
                                strTmpBBwt = strVALU
                            End If
                        Else
                            strTmpBBag = 0
                            strTmpBBwt = 0
                            If strTYPE = "BAG" Then
                                strTmpBBag = strVALU
                            End If
                            If strTYPE = "BWT" Then
                                strTmpBBwt = strVALU
                            End If
                        End If
                        tabTab(1).SetFieldValues llCurrLine, "C-PC/WT", CStr(strTmpBBag) + "/" + CStr(strTmpBBwt)
                    Case "U"
                        tmpPax = Val(tabTab(1).GetFieldValue(llCurrLine, "U-PAX"))
                        'kkh on 05/05/2008 MANTIS PRF : 0000014
                        'If (strDSSN = "KRI" And strTYPE = "PAX") Or (strDSSN = "PTM" And tmpPax = 0 And strTYPE = "PAX") Then 'igu on 23/08/2011: ALTEA
                        If ((strDSSN = "KRI" Or strDSSN = "ALT") And strTYPE = "PAX") Or (strDSSN = "PTM" And tmpPax = 0 And strTYPE = "PAX") Then 'igu on 23/08/2011: ALTEA
                            tabTab(1).SetFieldValues llCurrLine, "U-PAX", strVALU
                            'strTmp = frmData.tabData(3).GetFieldValue(llLine, "URNO") 'igu on 23/08/2011: ALTEA
                            strTmp = frmData.tabSubLOA.GetFieldValue(llLine, "URNO") 'igu on 23/08/2011: ALTEA
                            If InStr(strLoaTabUrnos, strTmp) = 0 Then
                                strLoaTabUrnos = strLoaTabUrnos + strTmp + ";"
                            End If
                        End If
                        If strRawFlno = strPriorFlno Then
                            If strTYPE = "BAG" Then
                                strTmpUBag = strVALU
                            End If
                            If strTYPE = "BWT" Then
                                strTmpUBwt = strVALU
                            End If
                        Else
                            strTmpUBag = 0
                            strTmpUBwt = 0
                            If strTYPE = "BAG" Then
                                strTmpUBag = strVALU
                            End If
                            If strTYPE = "BWT" Then
                                strTmpUBwt = strVALU
                            End If
                        End If
                        tabTab(1).SetFieldValues llCurrLine, "U-PC/WT", CStr(strTmpUBag) + "/" + CStr(strTmpUBwt)
                End Select
            
                llE = Val(tabTab(1).GetFieldValue(llCurrLine, "Y-PAX"))
                llF = Val(tabTab(1).GetFieldValue(llCurrLine, "F-PAX"))
                llB = Val(tabTab(1).GetFieldValue(llCurrLine, "C-PAX"))
                'Customize for GOCC 25/02/2008
                llU = Val(tabTab(1).GetFieldValue(llCurrLine, "U-PAX"))
                llPaxTot = llE + llB + llF + llU
                tabTab(1).SetFieldValues llCurrLine, "T-PAX", CStr(llPaxTot)
                
                str1 = tabTab(1).GetFieldValue(llCurrLine, "Y-PC/WT")
                arrTotalBagY = Split(str1, "/")
                If UBound(arrTotalBagY) > 0 Then
                    llBagTotal = llBagTotal + CLng(arrTotalBagY(0))
                    llBwtTotal = llBwtTotal + CLng(arrTotalBagY(1))
                End If
    
                str1 = tabTab(1).GetFieldValue(llCurrLine, "F-PC/WT")
                arrTotalBagF = Split(str1, "/")
                If UBound(arrTotalBagF) > 0 Then
                    llBagTotal = llBagTotal + CLng(arrTotalBagF(0))
                    llBwtTotal = llBwtTotal + CLng(arrTotalBagF(1))
                End If
    
                str1 = tabTab(1).GetFieldValue(llCurrLine, "C-PC/WT")
                arrTotalBagC = Split(str1, "/")
                If UBound(arrTotalBagC) > 0 Then
                    llBagTotal = llBagTotal + CLng(arrTotalBagC(0))
                    llBwtTotal = llBwtTotal + CLng(arrTotalBagC(1))
                End If
    
                str1 = tabTab(1).GetFieldValue(llCurrLine, "U-PC/WT")
                arrTotalBagU = Split(str1, "/")
                If UBound(arrTotalBagU) > 0 Then
                    llBagTotal = llBagTotal + CLng(arrTotalBagU(0))
                    llBwtTotal = llBwtTotal + CLng(arrTotalBagU(1))
                End If
                
                tabTab(1).SetFieldValues llCurrLine, "T-PC/WT", CStr(llBagTotal) + "/" + CStr(llBwtTotal)
            End If '//EndIf strDSSN = "PTM" Or strDSSN = "KRI"
        
'            strTmpEBag = 0
'            strTmpFBag = 0
'            strTmpBBag = 0
'            strTmpUBag = 0
'
'            strTmpEBwt = 0
'            strTmpFBwt = 0
'            strTmpBBwt = 0
'            strTmpUBwt = 0
    
            llBagTotal = 0
            llBwtTotal = 0
        
        End If '//EndIf byPassPTM = False
    
        'llLine = frmData.tabData(3).GetNextResultLine  'igu on 23/08/2011: ALTEA
        If llLine = frmData.tabSubLOA.GetLineCount - 1 Then 'igu on 23/08/2011: ALTEA
            llTempLine = -1 'igu on 23/08/2011: ALTEA
        Else 'igu on 23/08/2011: ALTEA
            llTempLine = llLine + 1 'igu on 23/08/2011: ALTEA
        End If 'igu on 23/08/2011: ALTEA
        ReDim arr(0)
        strPriorFlno = strRawFlno 'MWOXXXstrFlno
        strPriorDSSN = strDSSN 'igu n 09/02/2012
        '********** To identify equal FLNOs
        'strRawFlno = frmData.tabData(3).GetFieldValue(llLine, "FLNO") 'igu on 23/08/2011: ALTEA
        strRawFlno = frmData.tabSubLOA.GetFieldValue(llTempLine, "FLNO") 'igu on 23/08/2011: ALTEA
        'kkh 12/06/2008 APC3
        'strLoaApc3 = frmData.tabData(3).GetFieldValue(llLine, "APC3") 'igu on 23/08/2011: ALTEA
        strLoaApc3 = frmData.tabSubLOA.GetFieldValue(llTempLine, "APC3") 'igu on 23/08/2011: ALTEA
        'kkh on 21/04/2008
        'strLoaAddi = frmData.tabData(3).GetFieldValue(llLine, "ADDI") 'igu on 23/08/2011: ALTEA
        strLoaAddi = frmData.tabSubLOA.GetFieldValue(llTempLine, "ADDI") 'igu on 23/08/2011: ALTEA
        arr = Split(strRawFlno, "/")
        If UBound(arr) = 1 Then
            strFlno = arr(0)
            strFlightDay = arr(1)
        Else
            strFlno = strRawFlno
        End If
        strFlno = StripAftFlno(strFlno, dummy1, dummy2, dummy3)
        
        'kkh on 21/04/2008
        strLoaAddi = Mid(Trim(strLoaAddi), 14, 2)
        If strLoaAddi <> "" Then
            'kkh 12/06/2008 APC3
            'strRawFlno = strFlno + "/" + strLoaAddi
            strRawFlno = strFlno + "/" + strLoaAddi + "/" + strLoaApc3
        End If
        
        If UBound(arr) = 1 Then
            If arr(1) <> "" Then
                strRawFlno = strFlno + "/" + strFlightDay
            End If
        End If
        
        '**********
        'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights
        If strRawFlno <> strPriorFlno And Len(strLoaTabUrnos) <> 0 Then
        'MWOXXX If strFlno <> strPriorFlno Then
            strLoaTabUrnos = Left(strLoaTabUrnos, Len(strLoaTabUrnos) - 1)
            tabTab(1).SetFieldValues llCurrLine, "L_URNOS", strLoaTabUrnos
            llCurrLine = llCurrLine + 1
            tabTab(1).InsertTextLine strEmptLine, False
            strLoaTabUrnos = ""
        End If
    'Wend 'igu on 23/08/2011: ALTEA
    Next llLine 'igu on 23/08/2011: ALTEA
    
    '------------------
    'igu on 25 Jan 2010
    '------------------
    If pstrShowYellowLines = "NO" Then
        'remove yellow line
        Dim strYellowLines As String
        Dim aYellowLines As Variant
        Dim vYellowLine As Variant
        Dim intDelCount As Integer 'igu on 25 Feb 2010
        
        strYellowLines = tabTab(1).GetLinesByBackColor(vbYellow)
        If strYellowLines <> "" Then
            aYellowLines = Split(strYellowLines, ",")
            intDelCount = 0 'igu on 25 Feb 2010
            For Each vYellowLine In aYellowLines
                tabTab(1).DeleteLine CLng(vYellowLine - intDelCount) 'igu on 25 Feb 2010
                intDelCount = intDelCount + 1 'igu on 25 Feb 2010
            Next vYellowLine
        End If
    End If
    '------------------
    tabTab(1).DeleteLine tabTab(1).GetLineCount - 1
    
    'Sort accroding to STD, transfer time, ETD
    'tabTab(1).Sort "23,6,7", True, False
    tabTab(1).IndexCreate "URNO", 0
    tabTab(1).SetInternalLineBuffer True
    tabTab(1).Sort "6,23,7", True, True
    tabTab(1).Refresh
    
    If tabTab(1).GetLineCount > 0 Then
        tabTab(1).SetCurrentSelection 0
    End If
End Sub
''Show Flight
''Parameter is Flight URNO
Private Sub SelectFlight(ByVal Key As String)
    Dim MyDate As Date
    Dim endTime As Date
    Dim StartTime As Date
    Dim datArrSTOA As Date
    
    Dim ilWhatFlight As Integer '1 = Arrival, 2 = Departure, 3 = Rotation
    
    Dim i As Long
    Dim cnt As Long
    Dim llLine As Long
    Dim llRLine As Long
    Dim llRkeyLine As Long
    
    Dim strTmp As String
    Dim strStoa As String
    Dim strStod As String
    Dim strTIFA As String
    Dim strTIFD As String
    Dim strAdid As String
    Dim strRKEY As String
    Dim strValue As String
    Dim strBarKey As String
    Dim strTabline As String
    Dim strEmptyArr As String
    Dim strEmptyDep As String
    
    Screen.MousePointer = vbHourglass 'igu on 06/01/2012
    
    'Disable timer and clear the flnu buffer that need to be reload
    tmrBcRefresh.Enabled = False
    InitFlnuBcTab
    
    omCurrentSelectedUrno = Key
    
    'Rotation TAB
    ilWhatFlight = 0
    strEmptyArr = ",,,,,,,,,,,"
    strEmptyDep = ",,,,,,,,,,"
    strTabline = String(ItemCount(tabTab(0).HeaderLengthString, ","), ",") 'strEmptyArr + strEmptyDep
    llLine = 0
    tabTab(0).ResetContent
    tabTab(1).ResetContent
    tabTab(1).Refresh
    tabTab(0).InsertTextLine strTabline, False
    
    'Search for RKEY
    strTmp = frmData.tabData(0).GetLinesByIndexValue("URNO", Key, 0)
    llRLine = frmData.tabData(0).GetNextResultLine()
    llRkeyLine = Val(frmData.tabData(0).GetFieldValue(llRLine, "RKEY"))
    strAdid = frmData.tabData(0).GetFieldValue(llRLine, "ADID")
    strTmp = frmData.tabData(0).GetLinesByIndexValue("RKEY", llRkeyLine, 0)
    'cnt = CLng(strTmp)
    While llLine >= 0
        llLine = frmData.tabData(0).GetNextResultLine
        If llLine >= 0 Then
            'Arrival
            If frmData.tabData(0).GetFieldValue(llLine, "ADID") = "A" Or frmData.tabData(0).GetFieldValue(llLine, "ADID") = "B" Then
                ilWhatFlight = ilWhatFlight + 1
                strStoa = frmData.tabData(0).GetFieldValue(llLine, "STOA")
                If Trim(strStoa) <> "" Then
                    datArrSTOA = CedaFullDateToVb(frmData.tabData(0).GetFieldValue(llLine, "STOA"))
                End If
                strValue = frmData.tabData(0).GetFieldValue(llLine, "FLNO")
                tabTab(0).SetFieldValues 0, "FLNO_A", strValue
                
                'kkh on 25/11/2008 cater for delay timing
                strValue = frmData.tabData(0).GetFieldValue(llLine, "STOA")
                strValue = Trim(strValue)
                If Trim(strValue) <> "" Then
                    MyDate = CedaFullDateToVb(strValue)
                    tabTab(0).SetFieldValues 0, "DATE_A", Format(MyDate, "DD.MM.YY")
                    MyDate = CedaFullDateToVb(strValue)
                    tabTab(0).SetFieldValues 0, "STA", Format(MyDate, "hh:mm") + "/" + Mid(strValue, 7, 2)
                    tabTab(0).SetFieldValues 0, "STOA_A", strValue
                End If
                strValue = frmData.tabData(0).GetFieldValue(llLine, "ETAI")
                strValue = Trim(strValue)
                If Trim(strValue) <> "" Then
                    MyDate = CedaFullDateToVb(strValue)
                    tabTab(0).SetFieldValues 0, "ETA", Format(MyDate, "hh:mm") + "/" + Mid(strValue, 7, 2)
                End If
                strValue = frmData.tabData(0).GetFieldValue(llLine, "ONBL")
                strValue = Trim(strValue)
                If Trim(strValue) <> "" Then
                    MyDate = CedaFullDateToVb(strValue)
                    tabTab(0).SetFieldValues 0, "ONBL", Format(MyDate, "hh:mm") + "/" + Mid(strValue, 7, 2)
                End If
                
'                strValue = frmData.tabData(0).GetFieldValue(llLine, "TIFA")
'                strValue = Trim(strValue)
'                If Trim(strValue) <> "" Then
'                    MyDate = CedaFullDateToVb(strValue)
'                    tabTab(0).SetFieldValues 0, "DATE_A", Format(MyDate, "DD.MM.YY")
'                End If
'                strValue = frmData.tabData(0).GetFieldValue(llLine, "STOA")
'                strValue = Trim(strValue)
'                If Trim(strValue) <> "" Then
'                    MyDate = CedaFullDateToVb(strValue)
'                    tabTab(0).SetFieldValues 0, "STOA_A", strValue
'                End If
'                strValue = frmData.tabData(0).GetFieldValue(llLine, "ETAI")
'                strValue = Trim(strValue)
'                If Trim(strValue) <> "" Then
'                    MyDate = CedaFullDateToVb(strValue)
'                    tabTab(0).SetFieldValues 0, "ETA", Format(MyDate, "hh:mm")
'                End If
'                strValue = frmData.tabData(0).GetFieldValue(llLine, "ONBL")
'                strValue = Trim(strValue)
'                If Trim(strValue) <> "" Then
'                    MyDate = CedaFullDateToVb(strValue)
'                    tabTab(0).SetFieldValues 0, "ONBL", Format(MyDate, "hh:mm")
'                End If
                strValue = frmData.tabData(0).GetFieldValue(llLine, "TTYP")
                tabTab(0).SetFieldValues 0, "NA_A", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "ORG3")
                tabTab(0).SetFieldValues 0, "ORG", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "VIA3")
                tabTab(0).SetFieldValues 0, "VIA_A", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "PSTA")
                tabTab(0).SetFieldValues 0, "POS_A", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "TGA1")
                tabTab(0).SetFieldValues 0, "TGA_A", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "GTA1")
                tabTab(0).SetFieldValues 0, "GAT_A", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "ACT3")
                tabTab(0).SetFieldValues 0, "A/C_A", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "REGN")
                tabTab(0).SetFieldValues 0, "REGN_A", strValue
                'kkh on 16/07/2008 P2 Store Decision into CFITAB
                strValue = frmData.tabData(0).GetFieldValue(llLine, "URNO")
                tabTab(0).SetFieldValues 0, "URNO_A", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "STOA")
                tabTab(0).SetFieldValues 0, "FLIGHT_DATE", strValue
            End If
            'Departure
            If (frmData.tabData(0).GetFieldValue(llLine, "ADID") = "D" Or frmData.tabData(0).GetFieldValue(llLine, "ADID") = "B") And (Val(frmData.tabData(0).GetFieldValue(llLine, "RKEY")) = llRkeyLine) Then
                ilWhatFlight = ilWhatFlight + 2
                strValue = frmData.tabData(0).GetFieldValue(llLine, "FLNO")
                tabTab(0).SetFieldValues 0, "FLNO_D", strValue
                
                 'kkh on 25/11/2008 cater for delay timing
                strValue = frmData.tabData(0).GetFieldValue(llLine, "STOD")
                strValue = Trim(strValue)
                If Trim(strValue) <> "" Then
                    MyDate = CedaFullDateToVb(strValue)
                    tabTab(0).SetFieldValues 0, "DATE_D", Format(MyDate, "DD.MM.YY")
                    MyDate = CedaFullDateToVb(strValue)
                    tabTab(0).SetFieldValues 0, "STD", Format(MyDate, "hh:mm") + "/" + Mid(strValue, 7, 2)
                End If
                strValue = frmData.tabData(0).GetFieldValue(llLine, "ETDI")
                strValue = Trim(strValue)
                If Trim(strValue) <> "" Then
                    MyDate = CedaFullDateToVb(strValue)
                    tabTab(0).SetFieldValues 0, "ETD", Format(MyDate, "hh:mm") + "/" + Mid(strValue, 7, 2)
                End If
                strValue = frmData.tabData(0).GetFieldValue(llLine, "OFBL")
                strValue = Trim(strValue)
                If Trim(strValue) <> "" Then
                    MyDate = CedaFullDateToVb(strValue)
                    tabTab(0).SetFieldValues 0, "OFBL", Format(MyDate, "hh:mm") + "/" + Mid(strValue, 7, 2)
                End If
                
'                strValue = frmData.tabData(0).GetFieldValue(llLine, "TIFD")
'                strValue = Trim(strValue)
'                If Trim(strValue) <> "" Then
'                    MyDate = CedaFullDateToVb(strValue)
'                    tabTab(0).SetFieldValues 0, "DATE_D", Format(MyDate, "DD.MM.YY")
'                End If
'                strValue = frmData.tabData(0).GetFieldValue(llLine, "STOD")
'                strValue = Trim(strValue)
'                If Trim(strValue) <> "" Then
'                    MyDate = CedaFullDateToVb(strValue)
'                    tabTab(0).SetFieldValues 0, "STD", Format(MyDate, "hh:mm")
'                End If
'                strValue = frmData.tabData(0).GetFieldValue(llLine, "ETDI")
'                strValue = Trim(strValue)
'                If Trim(strValue) <> "" Then
'                    MyDate = CedaFullDateToVb(strValue)
'                    tabTab(0).SetFieldValues 0, "ETD", Format(MyDate, "hh:mm")
'                End If
'                strValue = frmData.tabData(0).GetFieldValue(llLine, "OFBL")
'                strValue = Trim(strValue)
'                If strValue <> "" Then
'                    If Trim(strValue) <> "" Then
'                        MyDate = CedaFullDateToVb(strValue)
'                        tabTab(0).SetFieldValues 0, "OFBL", Format(MyDate, "hh:mm")
'                    End If
'                End If
                strValue = frmData.tabData(0).GetFieldValue(llLine, "TTYP")
                tabTab(0).SetFieldValues 0, "NA_D", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "VIA3")
                tabTab(0).SetFieldValues 0, "VIA_D", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "DES3")
                tabTab(0).SetFieldValues 0, "DES", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "PSTD")
                tabTab(0).SetFieldValues 0, "POS_D", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "TGD1")
                tabTab(0).SetFieldValues 0, "TGD_D", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "GTD1")
                tabTab(0).SetFieldValues 0, "GAT_D", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "REGN")
                tabTab(0).SetFieldValues 0, "REGN_D", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "URNO")
                tabTab(0).SetFieldValues 0, "URNO_D", strValue
            End If
        End If
    Wend
    'Get LOATAB Info into the tabdata for transfer pax
    tabTab(0).Refresh

    If strAdid = "A" Then
        'KKH cater for Kriscom update
        ReloadLOATABForFlnu Key
        SetLoaTabInfoToTab Key, datArrSTOA
    End If
    
    'set timer value to zero and re-enable it
    intBcRefreshMinutes = 0
    tmrBcRefresh.Enabled = True
    
    Screen.MousePointer = vbDefault 'igu on 06/01/2012
End Sub

'------------------------------------------------
' Reorgs the loatab for a AFT.URNO or a TLX.FLNU
'------------------------------------------------
Private Sub ReloadLOATABForFlnu(ByVal strFlnu As String)
    Dim myTab As TABLib.TAB
    Dim arr As Variant
    Dim arrFields As Variant
    Dim strWhere As String
    Dim i As Integer
    
    Set myTab = frmData.tabData(3)
    arr = Split(myTab.Tag, ";")
    If UBound(arr) = 2 Then
        arrFields = Split(arr(1), ",")
        strWhere = "WHERE FLNU = " & strFlnu & _
               " AND DSSN IN ('PTM','KRI','ALT') AND TYPE IN ('PAX','PXB','BAG','BWT') AND STYP IN ('E','B','F','U') " + _
               " AND SSST=' ' AND (SSTP='TD' OR SSTP=' ' OR SSTP='T') AND FLNO <> ' ' order by APC3, ADDI" 'igu on 23/08/2011: ALTEA
               
        myTab.ResetContent
        myTab.AutoSizeByHeader = True
        myTab.HeaderString = ""
        myTab.HeaderLengthString = ""
        myTab.HeaderString = arr(1)
        myTab.EnableInlineEdit True
        myTab.CedaServerName = strServer
        myTab.CedaPort = "3357"
        myTab.CedaHopo = strHopo
        myTab.CedaCurrentApplication = "HUB-Manager" & "," & frmData.GetApplVersion(True)
        myTab.CedaTabext = "TAB"
        myTab.CedaUser = strCurrentUser
        myTab.CedaWorkstation = frmData.aUfis.WorkStation
        myTab.CedaSendTimeout = "3"
        myTab.CedaReceiveTimeout = "240"
        myTab.CedaRecordSeparator = Chr(10)
        myTab.CedaIdentifier = "HUB-Manager"
        myTab.ShowHorzScroller True
        'myTab.ShowHorzScroller False
        myTab.EnableHeaderSizing True
        For i = 0 To UBound(arrFields)
            If i = 0 Then
                myTab.HeaderLengthString = "80,"
                myTab.ColumnWidthString = "10,"
            Else
                myTab.HeaderLengthString = myTab.HeaderLengthString & "80,"
                myTab.ColumnWidthString = myTab.ColumnWidthString & "10,"
            End If
        Next i
        myTab.LogicalFieldList = arr(1)
        
        myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
        myTab.RedrawTab
        myTab.AutoSizeByHeader = True
        myTab.AutoSizeColumns
        myTab.myTag = myTab.Left & "," & myTab.Top & "," & myTab.Height & "," & myTab.Width
        
        myTab.Sort "1,2,3", True, True
        myTab.IndexCreate "URNO", 0
        myTab.IndexCreate "FLNU", 1
        myTab.SetInternalLineBuffer True
    End If
End Sub

Private Sub InitConnTabs()
    Dim strColors As String
    Dim i As Long
    Dim strColor As String
    Dim strComboValues As String
    Dim strPrivRet As String
    
    strColors = CStr(16761024) + "," + CStr(16761024)
    tabTab(0).ResetContent
    tabTab(0).MainHeader = True
    tabTab(0).ShowHorzScroller (True)
    tabTab(0).HeaderString = "FLNO,STOA,Date,STA,ETA,ONBL,Na,Org,Via,Pos,Ter,Gat,A/C,Regn,FLNO,Date,STD,ETD,OFBL,Na,Via,Des,Pos,Ter,Gat,Urno,Flight_date,UrnoD"
    tabTab(0).LogicalFieldList = "FLNO_A,STOA_A,DATE_A,STA,ETA,ONBL,NA_A,ORG,VIA_A,POS_A,TGA_A,GAT_A,A/C_A,REGN_A,FLNO_D,DATE_D,STD,ETD,OFBL,NA_D,VIA_D,DES,POS_D,TGD_D,GAT_D,URNO_A,FLIGHT_DATE,URNO_D"
    tabTab(0).HeaderLengthString = "55,0,52,48,48,48,40,30,30,30,30,30,30,55,55,52,48,48,48,40,30,30,30,30,30,0,0,0"

    tabTab(0).SetMainHeaderValues "12,14", "Arrival,Departure", strColors
    tabTab(0).FontName = "Arial"
    tabTab(0).FontSize = 14
    tabTab(0).HeaderFontSize = 14
    tabTab(0).EnableHeaderSizing (True)
    tabTab(0).SetTabFontBold True
    tabTab(0).LeftTextOffset = 2
    
    tabTab(1).ResetContent
    tabTab(1).ShowHorzScroller (False)
    tabTab(1).HeaderString = "AFTURNO,LOA_RURN,ACT,FLNO,Ter,Gate,STD,Est.,ATD,Via/Dest,Routing,F-Pax,F-Pc/Wt,C-Pax,C-Pc/Wt,U-Pax,U-Pc/Wt,Y-Pax,Y-Pc/Wt,T-Pax,T-Pc/Wt,Na,Decision,,Transf.,,, "
    tabTab(1).LogicalFieldList = "AFTURNO,LOA_RURN,ACT,FLNO,TGD_D,GATE,STD,EST,ATD,DES,VIAnDES,F-PAX,F-PC/WT,C-PAX,C-PC/WT,U-PAX,U-PC/WT,Y-PAX,Y-PC/WT,T-PAX,T-PC/WT,NA,Hold,COMBO,TRANSFER,DUMMY,L_URNOS,CFI_URNO"
    tabTab(1).HeaderLengthString = "0,0,40,60,30,40,60,60,60,60,60,40,0,40,0,0,0,40,0,40,40,30,60,20,60,500,0,0"
    tabTab(1).ColumnWidthString = "10,10,4,12,14,14,14,14,14,3,3,3,3,3,3,3,3,3,3,3,3,5,100,5,5,100,10"
    
    'tabTab(1).ShowRowSelection = False
    tabTab(1).FontName = "Arial"
    tabTab(1).FontSize = 14
    tabTab(1).HeaderFontSize = 14
    tabTab(1).SetTabFontBold True
    tabTab(1).EnableHeaderSizing True
    tabTab(1).LeftTextOffset = 2
    strColor = vbBlue & "," & vbBlue
    'tabTab(1).CursorDecoration "AFTURNO,LOA_RURN,ACT,FLNO,TGD_D,GATE,STD,EST,ATD,DES,T-PAX,F-PAX,B-PAX,E-PAX,T-PC/WT,F-PC/WT,B-PC/WT,E-PC/WT,NA,Hold,TRANSFER,DUMMY", "B,T", "2,2", strColor
    tabTab(1).CursorDecoration "AFTURNO,LOA_RURN,ACT,FLNO,TGD_D,GATE,STD,EST,ATD,DES,VIAnDES,F-PAX,F-PC/WT,C-PAX,C-PC/WT,U-PAX,U-PC/WT,Y-PAX,Y-PC/WT,T-PAX,T-PC/WT,NA,Hold,TRANSFER,DUMMY", "B,T", "2,2", strColor
    tabTab(1).DefaultCursor = False
    tabTab(1).EnableInlineEdit True
    tabTab(1).InplaceEditUpperCase = False
    '"AFTURNO,LOA_RURN,ACT,FLNO,DATE,STD,EST,ATD,DES,T-PAX,F-PAX,B-PAX,E-PAX,NA,Hold,DUMMY,L_URNOS"
    'focus means not editable
    'tabTab(1).NoFocusColumns = "0,1,2,3,4,5,6,7,8,9,10,22" '''''igu igu change this for temp
    tabTab(1).NoFocusColumns = "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22"
    strColor = CStr(vbYellow)
    tabTab(1).CreateDecorationObject "HOLD", "BTR", "11", strColor
    tabTab(1).CreateDecorationObject "USERENTRY", "L", "2", CStr(vbCyan)
      
    strPrivRet = frmData.ULogin.GetPrivileges("Combo_Decision")
    
    If strPrivRet = "1" Then
'        Handle the "Decision" combobox
'        Reset frmMain DECISION dropdown
'        If bgfrmMainOpen = True Then
'            tabTab(1).ComboResetObject "DECISION"
'        End If
'        Customize for GOCC 25/02/2008

        'If bgfrmMainOpen = False Then
            tabTab(1).ComboResetObject "DECISION"
            tabTab(1).CreateComboObject "DECISION", 23, 1, "", 130
            Dim Decision() As String
            
            Decision = Split(strDecisionOption, ",")
            'strComboValues = " " & vbLf
            For i = 0 To UBound(Decision)
                strComboValues = strComboValues & Decision(i) & vbLf
            Next i
            'strComboValues = strComboValues & " " & vbLf
'            strComboValues = " " & vbLf
'            strComboValues = strComboValues & "Waiting" & vbLf
'            strComboValues = strComboValues & "Not Waiting" & vbLf
'            strComboValues = strComboValues & "Expedite" & vbLf
'            strComboValues = strComboValues & " " & vbLf
            
            tabTab(1).ComboSetColumnLengthString "DECISION", "120"
            tabTab(1).SetComboColumn "DECISION", 23
    
            tabTab(1).ComboAddTextLines "DECISION", strComboValues, vbLf
            tabTab(1).ComboMinWidth = 18
    End If
      
    For i = 6 To 8
        tabTab(1).DateTimeSetColumn i
        tabTab(1).DateTimeSetColumnFormat i, "YYYYMMDDhhmmss", "hh':'mm'/'DD"
    Next i
End Sub

Private Sub InitHubManager()
    Dim strMsg As String
    Dim datFrom As Date
    Dim datTo As Date
    
    datFrom = CedaFullDateToVb(strTimeFrameFrom)
    datTo = CedaFullDateToVb(strTimeFrameTo)
    
    If TimesInUTC = False Then
        UTCOffset = frmData.GetUTCOffset
        datFrom = DateAdd("h", UTCOffset, datFrom)
        datTo = DateAdd("h", UTCOffset, datTo)
    End If
    
    Me.Caption = "HUB-Manager: Loaded [" & datFrom & "] - [" & datTo & "]" & "    Server: " & strServer
    InitConnTabs
'    InitConfigData
'    StoreDepartureUrnoForLoaTab frmData.tabData(3)

    If frmData.tabData(5).GetLineCount = 0 Then
        strMsg = "No connection times could be found." & vbLf
        strMsg = strMsg + "Please insert the values!" & vbLf & vbLf
        strMsg = strMsg + "Now using the default values! " & vbLf
        strMsg = strMsg + "================================" & vbLf
        strMsg = strMsg + " PAX : " + strPaxDefault + " minutes" + vbLf
        MsgBox strMsg, vbExclamation + vbOKOnly, "Warning!"
    End If
    
    InitFlnuBcTab
End Sub

Sub InitFlnuBcTab()
    tabFlnuBcs.ResetContent
    tabFlnuBcs.HeaderString = "FLNU"
    tabFlnuBcs.HeaderLengthString = "200"
    tabFlnuBcs.LogicalFieldList = "FLNU"
    tabFlnuBcs.SetUniqueFields "FLNU"
End Sub

Private Sub SetSecurity()
    Dim strPrivRet As String
        
    strPrivRet = frmData.ULogin.GetPrivileges("Load")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        'cbToolButton(0).Visible = False 'Load button
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Now")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        'cbToolButton(1).Visible = False 'Now button
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Compressed_Flights")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        'cbToolButton(2).Visible = False 'Compressed Flights button
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Hourly_Flights")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        'cbToolButton(3).Visible = False 'Hourly flights button
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Mail_Cargo")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        'cbToolButton(5).Visible = False 'Mail/Cargo button
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Plain_Gannt")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        'cbToolButton(8).Visible = False 'Mail/Cargo button
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Filter")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        'cbToolButton(9).Visible = False 'Filter button
        fraFilter.Enabled = False
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Gate")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        'cbToolButton(10).Visible = False 'Gate button
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Connection_Times")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        'cbToolButton(6).Visible = False 'Connection_Times button
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Hourly_Pax")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        'cbToolButton(7).Visible = False 'Hourly_Pax button
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Setup")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        'cbToolButton(4).Visible = False
    End If
    
    strPrivRet = frmData.ULogin.GetPrivileges("Area_1")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        'rbArea(0).Visible = False
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Area_2")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        'rbArea(1).Visible = False
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Area_3")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        'rbArea(2).Visible = False
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Area_All")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        'rbArea(3).Visible = False
    End If
    
    strPrivRet = frmData.ULogin.GetPrivileges("Telex")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        'cmdTelex.Visible = False
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Print")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        cmdPrint.Visible = False
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Insert")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        'cmdInsert.Visible = False
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Delete")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        'cmdDelete.Visible = False
    End If
    If strAddiFeature = "YES" Then
        strPrivRet = frmData.ULogin.GetPrivileges("Send_Telex")
        If strPrivRet = "-" Or strPrivRet = "0" Then
            cmdSendTelex.Visible = False
        End If
        'strPrivRet = frmData.ULogin.GetPrivileges("Excel")
        'If strPrivRet = "-" Or strPrivRet = "0" Then
        '    cmdExcel.Visible = False
        'End If
        'kkh on 25/07/2008 P2 View Decision Log
        'strPrivRet = frmData.ULogin.GetPrivileges("View_Log")
        'If strPrivRet = "-" Or strPrivRet = "0" Then
        '    cbToolButton(11).Visible = False 'View Log button
        'End If
    Else
        cmdSendTelex.Enabled = False
        'cmdExcel.Enabled = False
        'cbToolButton(11).Enabled = False
    End If
End Sub

Private Sub Form_Load()
    Call InitFilterFlights
    Call InitHubManager
    
    Call SetSecurity
    
    intBcRefreshMinutes = 0
    
    'Set MyBC = New BcProxyLib.BcPrxy  'igu on 06/02/2013
    '-----Set the BcCom-----'
    '---igu on 06/02/2013---'
    Dim pid As Long
    
    pid = GetCurrentProcessId()

    Set BcCom = New BCCOMSERVERLib.BcComAtl

    BcCom.SetPID "HUBMANAGER", pid
    
    BcCom.StartBroadcasting
    '-----Set the BcCom-----'
End Sub

Private Sub Form_Resize()
    Dim intFrameWidth As Integer
    Dim intFilterTabWidth As Integer
    Dim intTabWidth As Integer
    Dim intFrameHeight As Integer
    Dim intTabHeight As Integer

    intFrameWidth = Me.ScaleWidth - 150
    If intFrameWidth > 0 Then
        fraFilter.Width = intFrameWidth
        fraFlight.Width = intFrameWidth
        
        intFilterTabWidth = intFrameWidth - tabFlights.Left - 150
        If intFilterTabWidth > 0 Then
            tabFlights.Width = intFilterTabWidth
        End If
        
        intTabWidth = intFrameWidth - tabTab(0).Left - 150
        If intTabWidth > 0 Then
            tabTab(0).Width = intTabWidth
            tabTab(1).Width = intTabWidth
        End If
    End If
    
    intFrameHeight = Me.ScaleHeight - fraFlight.Top - 150
    If intFrameHeight > 0 Then
        fraFlight.Height = intFrameHeight
        
        intTabHeight = intFrameHeight - tabTab(1).Top - 150
        If intTabHeight > 0 Then
            tabTab(1).Height = intTabHeight
        End If
    End If
End Sub

Private Sub cmdPrint_Click()
   'kkh on 20/07/2008 P2 Print Button RFC
    Dim rpt As New rptConnections
   ' CEDA.INI file path. The path of the BMP file is taken from the parameter 'PRINTLOGO' under 'HUBMANAGER'
   ' Const sFile As String = "D:\Ufis\System\Ceda.ini"
    Dim sString As String
    Dim lSize As Long
    Dim lReturn As Long
    sString = String$(255, " ")
    lSize = Len(sString)
    
    lReturn = GetPrivateProfileString("HUBMANAGER", "PRINTLOGO", "", sString, lSize, DEFAULT_CEDA_INI)
     'lReturn = GetPrivateProfileString("HUBMANAGER", "PRINTLOGO", "", sString, lSize, sFile)
    sString = Trim(sString)
    sString = Left$(sString, Len(sString) - 1)
    
    Dim fso As FileSystemObject
    Set fso = New FileSystemObject
    
    If lReturn > 0 Then
        If fso.FileExists(sString) Then
            rpt.PrintLogo.Picture = LoadPicture(sString)
        Else
            MsgBox "Invalid print Logo bitmap file Path in CEDA.INI", vbExclamation + vbOKOnly, "Error"
        End If
    End If
    rpt.Show
End Sub

Private Sub cmdSendTelex_Click()
    Dim RetVal As String
    Dim MyPath As String
    Dim opened As Boolean
      
    Dim tabLine As Integer
    Dim strRecordIndividual As String
    Dim strRecordAll As String
    Dim strDecision As String
    Dim strRecordDep() As String
    Dim strRecordArr As String
    Dim datTifd As Date
    Dim datEtdi As Date
    Dim i As Integer
    
    Dim strAftUrnoLineCnt As String
    Dim strLine() As String
    Dim strFigure() As String
    Dim strFclassFigure As Integer
    Dim strCclassFigure As Integer
    Dim strYclassFigure As Integer
    Dim strBWFigure() As String
    Dim strBagFigure As Integer
    Dim strWeightFigure As Integer
    Dim k As Integer
    'Dim lineSkip As Integer
    Dim strCurAFTURNO As String
    Dim strPreAFTURNO As String
    
    Dim llTabLine As Long 'igu on 03/02/2012
    
    '---
    'igu on 28/06/2011
    'variables for special requirements
    Dim strArrFlno As String
    Dim strDepUrno As String
    Dim colDepFlights As Collection
    
    If pblnCopyToSpecialRequirements Then
        Set colDepFlights = New Collection
    End If
    '---
     
    opened = False
    tabTab(1).Sort 22, True, True
    
    For tabLine = 0 To tabTab(1).GetLineCount - 1
    
        strDecision = tabTab(1).GetFieldValue(tabLine, "Hold")
        ReDim strRecordDep(tabLine)
        strCurAFTURNO = tabTab(1).GetFieldValue(tabLine, "AFTURNO")
        If strCurAFTURNO <> strPreAFTURNO Then
            If strDecision <> "" Then
                'strAftUrnoLineCnt = tabTab(1).GetLinesByColumnValues(0, tabTab(1).GetFieldValue(tabLine, "AFTURNO"), 0) 'igu on 03/02/2012
                strAftUrnoLineCnt = tabTab(1).GetLinesByIndexValue("URNO", tabTab(1).GetFieldValue(tabLine, "AFTURNO"), 0) 'igu on 03/02/2012
                llTabLine = tabTab(1).GetNextResultLine 'igu on 03/02/2012
                
                'If Len(strAftUrnoLineCnt) > 1 Then 'igu on 03/02/2012
                    'strLine = Split(strAftUrnoLineCnt, ",")
                    
                    '--------------
                    'Date: May 13th 2009
                    'Desc: Reset all Figure Variables to 0
                    'By  : igu
                    '--------------
                    strFclassFigure = 0
                    strCclassFigure = 0
                    strYclassFigure = 0
                    strBagFigure = 0
                    strWeightFigure = 0
                    '--------------
                    'For i = 0 To UBound(strLine) 'igu on 03/02/2012
                    Do While llTabLine > -1  'igu on 03/02/2012
                        'strFigure = Split(tabTab(1).GetFieldValues(strLine(i), "F-PAX,C-PAX,Y-PAX,T-PC/WT"), ",") 'igu on 03/02/2012
                        strFigure = Split(tabTab(1).GetFieldValues(llTabLine, "F-PAX,C-PAX,Y-PAX,T-PC/WT"), ",") 'igu on 03/02/2012
                        For k = 0 To UBound(strFigure)
                            Select Case (k)
                                Case 0: strFclassFigure = Val(strFigure(k)) + strFclassFigure
                                Case 1: strCclassFigure = Val(strFigure(k)) + strCclassFigure
                                Case 2: strYclassFigure = Val(strFigure(k)) + strYclassFigure
                                Case 3: strBWFigure = Split(strFigure(k), "/")
                                        strBagFigure = strBWFigure(0) + strBagFigure
                                        strWeightFigure = strBWFigure(1) + strWeightFigure
                            End Select
                        Next k
                        
                        llTabLine = tabTab(1).GetNextResultLine 'igu on 03/02/2012
                    Loop 'igu on 03/02/2012
                    'Next i 'igu on 03/02/2012
                    
                    strDepUrno = tabTab(1).GetFieldValue(tabLine, "AFTURNO")
                    strRecordDep = Split(tabTab(1).GetFieldValues(tabLine, "FLNO,STD,EST,NA,VIAnDES"), ",")
                    strRecordIndividual = ""
                    
                    For i = 0 To UBound(strRecordDep)
                        Select Case (i)
                            Case 0:
                                strRecordDep(i) = Replace(strRecordDep(i), " ", "")
                                strRecordIndividual = strRecordDep(i)
                            Case 1:
                                If strRecordDep(i) <> "" Then
                                    datTifd = CedaFullDateToVb(strRecordDep(i))
                                    strRecordDep(i) = "STD" + " " + Format(datTifd, "hhmm") + "/" + Format(datTifd, "DD")
                                Else
                                    strRecordDep(i) = ""
                                End If
                                strRecordIndividual = strRecordIndividual + "," + strRecordDep(i)
                            Case 2:
                                If strRecordDep(i) <> "" Then
                                    datEtdi = CedaFullDateToVb(strRecordDep(i))
                                    strRecordDep(i) = "ETD" + " " + Format(datEtdi, "hhmm") + "/" + Format(datEtdi, "DD")
                                Else
                                    strRecordDep(i) = ""
                                End If
                                strRecordIndividual = strRecordIndividual + "," + strRecordDep(i)
                            Case 3:
                                If strRecordDep(i) <> "" Then
                                    strRecordDep(i) = strRecordDep(i)
                                End If
                                strRecordIndividual = strRecordIndividual + "," + strRecordDep(i)
                                'append pax figures
                                strRecordIndividual = strRecordIndividual + "," + CStr(strFclassFigure) + "P"
                                strRecordIndividual = strRecordIndividual + "/" + CStr(strCclassFigure) + "J"
                                strRecordIndividual = strRecordIndividual + "/" + CStr(strYclassFigure) + "Y"
                            Case 4:
                                If strRecordDep(i) <> "" Then
                                    strRecordDep(i) = strRecordDep(i)
                                End If
                                strRecordIndividual = strRecordIndividual + "," + strRecordDep(i)
                                strRecordIndividual = strRecordIndividual + "," + CStr(strBagFigure) + "PCS/" + CStr(strWeightFigure) + "KG"
                        End Select
                    Next i
                 'igu on 03/02/2012 Redundant codes below, clean it
'                Else
'                    'standard content for creating a telex
'                    strDepUrno = tabTab(1).GetFieldValue(tabLine, "AFTURNO")
'                    strRecordDep = Split(tabTab(1).GetFieldValues(tabLine, "FLNO,STD,EST,NA,F-PAX,C-PAX,Y-PAX,DES,T-PC/WT"), ",")
'                    strRecordIndividual = ""
'
'                    For i = 0 To UBound(strRecordDep)
'                        Select Case (i)
'                            Case 0:
'                                'strRecordDep(i) = Trim$(strRecordDep(i))
'                                strRecordDep(i) = Replace(strRecordDep(i), " ", "")
'                                'Debug.Print strRecordDep(i)
'                                strRecordIndividual = strRecordDep(i)
'                            Case 1:
'                                If strRecordDep(i) <> "" Then
'                                    datTifd = CedaFullDateToVb(strRecordDep(i))
'                                    strRecordDep(i) = "STD" + " " + Format(datTifd, "hhmm") + "/" + Format(datTifd, "DD")
'                                Else
'                                    strRecordDep(i) = ""
'                                End If
'                                strRecordIndividual = strRecordIndividual + "," + strRecordDep(i)
'                            Case 2:
'                                If strRecordDep(i) <> "" Then
'                                    datEtdi = CedaFullDateToVb(strRecordDep(i))
'                                    strRecordDep(i) = "ETD" + " " + Format(datEtdi, "hhmm") + "/" + Format(datEtdi, "DD")
'                                Else
'                                    strRecordDep(i) = ""
'                                End If
'                                strRecordIndividual = strRecordIndividual + "," + strRecordDep(i)
'                            Case 3:
'                                If strRecordDep(i) <> "" Then
'                                    strRecordDep(i) = strRecordDep(i)
'                                End If
'                                strRecordIndividual = strRecordIndividual + "," + strRecordDep(i)
'                            Case 4:
'                                If strRecordDep(i) <> "" Then
'                                    strRecordDep(i) = strRecordDep(i) + "P"
'                                End If
'                                strRecordIndividual = strRecordIndividual + "," + strRecordDep(i)
'                            Case 5:
'                                If strRecordDep(i) <> "" Then
'                                    strRecordDep(i) = strRecordDep(i) + "J"
'                                End If
'                                strRecordIndividual = strRecordIndividual + "/" + strRecordDep(i)
'                            Case 6:
'                                If strRecordDep(i) <> "" Then
'                                    strRecordDep(i) = strRecordDep(i) + "Y"
'                                End If
'                                strRecordIndividual = strRecordIndividual + "/" + strRecordDep(i)
'                            Case 7:
'                                If strRecordDep(i) <> "" Then
'                                    strRecordDep(i) = strRecordDep(i)
'                                End If
'                                strRecordIndividual = strRecordIndividual + "," + strRecordDep(i)
'                            Case 8:
'                                If strRecordDep(i) <> "" Then
'                                    strRecordDep(i) = Replace(strRecordDep(i), "/", "PCS/")
'                                End If
'                                strRecordIndividual = strRecordIndividual + "," + strRecordDep(i) + "KG"
'                        End Select
'                    Next i
'                End If
            End If
           
            If strRecordAll = "" Then
                '/////DELAY ARRIVAL ADVICE/////
    '            strRecordArr = "INVIEW OF " + Replace(tabTab(0).GetFieldValue(0, "FLNO_A"), " ", "") + "/" + Mid(CedaFullDateToVb(tabTab(0).GetFieldValue(0, "DATE_A")), 8, 2) + UCase(Mid(CedaFullDateToVb(tabTab(0).GetFieldValue(0, "DATE_A")), 4, 3)) + " " & _
    '            tabTab(0).GetFieldValue(0, "REGN_A") + " " + tabTab(0).GetFieldValue(0, "ORG") + "/" + tabTab(0).GetFieldValue(0, "VIA_A") + "/SIN DLYD ARR"
                'strRecordArr = "INVIEW OF " + Replace(tabTab(0).GetFieldValue(0, "FLNO_A"), " ", "") + "/" + Mid(CedaFullDateToVb(tabTab(0).GetFieldValue(0, "DATE_A")), 8, 2) + UCase(Mid(CedaFullDateToVb(tabTab(0).GetFieldValue(0, "DATE_A")), 4, 3)) + " "
                'strRecordArr = "INVIEW OF " + Replace(tabTab(0).GetFieldValue(0, "FLNO_A"), " ", "") + "/" + Mid(tabTab(0).GetFieldValue(0, "DATE_A"), 1, 2) + Mid(tabTab(0).GetFieldValue(0, "DATE_A"), 4, 2) + " "
                strRecordArr = "INVIEW OF " + Replace(tabTab(0).GetFieldValue(0, "FLNO_A"), " ", "") + "/" + Mid(tabTab(0).GetFieldValue(0, "DATE_A"), 1, 2) + UCase(Mid(CedaFullDateToVb(tabTab(0).GetFieldValue(0, "FLIGHT_DATE")), 4, 3)) + " "
                If tabTab(0).GetFieldValue(0, "REGN_A") <> "" Then
                    strRecordArr = strRecordArr + tabTab(0).GetFieldValue(0, "REGN_A") + " "
                End If
                If tabTab(0).GetFieldValue(0, "ORG") <> "" Then
                    strRecordArr = strRecordArr + tabTab(0).GetFieldValue(0, "ORG") + "/"
                End If
                If tabTab(0).GetFieldValue(0, "VIA_A") <> "" Then
                    strRecordArr = strRecordArr + tabTab(0).GetFieldValue(0, "VIA_A") + "/"
                End If
                strRecordArr = strRecordArr + "SIN DLYD ARR"
                strRecordArr = strRecordArr & vbNewLine
                strRecordArr = strRecordArr + "STA/" + Replace(tabTab(0).GetFieldValue(0, "STA"), ":", "") + " ETA/" + Replace(tabTab(0).GetFieldValue(0, "ETA"), ":", "") + " PSE NOTE THE FLWG"
            
        '        strRecordAll = "QD CGKKDSQ KULKDSQ LHRKDSQ PVGKDSQ SINKDSQ SINKASQ SINOFSQ"
        '        strRecordAll = strRecordAll & vbNewLine & ".SINKDSQ 230301"
                strRecordAll = "ATTN ALL CONCERNED FRM SINKDSQ/ FOR DTM/"
                strRecordAll = strRecordAll & vbNewLine & "/////DELAY ARRIVAL ADVICE/////"
                strRecordAll = strRecordAll & vbNewLine & strRecordArr
            End If
            
            If strRecordIndividual <> "" Then
                If strDecision = "Not Waiting" Then
                    If InStr(strRecordAll, "/////FLIGHT NOT WAITING/////") = 0 Then
                        strRecordAll = strRecordAll & vbNewLine & "/"
                        strRecordAll = strRecordAll & vbNewLine & "/////FLIGHT NOT WAITING/////"
                    End If
                    strRecordAll = strRecordAll & vbNewLine & strRecordIndividual
                    
                    'igu on 28/06/2011
                    'add to dep flights special requirements collection
                    If pblnCopyToSpecialRequirements Then
                        colDepFlights.Add Array(strDepUrno, "NWTG")
                    End If
                End If
                
                If strDecision = "Waiting" Then
                    If InStr(strRecordAll, "/////FLIGHT WAITING/////") = 0 Then
                        strRecordAll = strRecordAll & vbNewLine & "/"
                        strRecordAll = strRecordAll & vbNewLine & "/////FLIGHT WAITING/////"
                    End If
                    strRecordAll = strRecordAll & vbNewLine & strRecordIndividual
                    
                    'igu on 28/06/2011
                    'add to dep flights special requirements collection
                    If pblnCopyToSpecialRequirements Then
                        colDepFlights.Add Array(strDepUrno, "WTG")
                    End If
                End If
            End If
        End If
        strPreAFTURNO = tabTab(1).GetFieldValue(tabLine, "AFTURNO")
    Next tabLine
    If strRecordAll <> "" Then
        'finishing telex content
        strRecordAll = strRecordAll & vbNewLine & "/"
        'strRecordAll = strRecordAll & vbNewLine & "SINRR/RS - PSE RBKD MISCON ON FIRAV FLT IF ANY"
'        strRecordAll = strRecordAll & vbNewLine & "SINLL/LK/LR/LO- DAPO TRANSFER BAGGAGES"
'        strRecordAll = strRecordAll & vbNewLine & "SINKY-PSE ADVISE ALO"
'        strRecordAll = strRecordAll & vbNewLine & "SINKN-PSE UPDATE SYSTEM"
'        strRecordAll = strRecordAll & vbNewLine & "SINKK/KJ - PSE MAAS PAX AND ENSURE ESCORTED FOR DEP IF ANY"
        strRecordAll = strRecordAll & vbNewLine & "RGDS"
        
        'igu on 28/06/2011
        'feed the special requirements variables
        If pblnCopyToSpecialRequirements Then
            strArrFlno = Replace(tabTab(0).GetFieldValue(0, "FLNO_A"), " ", "")
            frmEditTelex.SetSpecialReqVars strArrFlno, colDepFlights
        End If
                    
        frmEditTelex.txtTelexText.Text = Replace(strRecordAll, ",", " ")
        frmEditTelex.Show
        opened = True
    Else
        strRecordAll = "No flight selected/no decision made for departure flight(s)"
        frmEditTelex.txtTelexText.Text = Replace(strRecordAll, ",", " ")
        frmEditTelex.Show
        opened = True
'        frmSendTelex.txtSendTelex.Text = Replace(strRecordAll, ",", " ")
'        frmSendTelex.Show
'        If MsgBox("Do you still want to launch Telexpool ?", vbYesNo, "Note") = vbYes Then
'            opened = True
'        End If
    End If
    'Call telexpool
'    If opened = True Then
'        MyPath = "D:\Ufis\Appl\TelexPool.exe /CONNECTED ULDManager"
'        RetVal = Shell(MyPath, vbNormalFocus)
'    End If
    
    tabTab(1).Sort "6,23,7", True, True
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim frm As Form
    
    If MsgBox("Do you really want to exit the application??", vbYesNo Or vbExclamation) <> vbYes Then
        Cancel = True
    Else
        BcCom.StopBroadcasting
        For Each frm In Forms
            If frm.Name <> Me.Name Then
                Unload frm
            End If
        Next frm
        
        BcCom.StopBroadcasting 'igu on 06/02/2013
    End If
End Sub

Private Sub BcCom_OnBc(ByVal size As Long, ByVal currPackage As Long, ByVal totalPackages As Long, ByVal strData As String) 'igu on 06/02/2013
    If (currPackage = 1) Then
        strBcData = strData
    Else
        strBcData = strBcData & strData
    End If
    
    If (currPackage = totalPackages) Then
        Dim ReqId As String, Dest1 As String, Dest2 As String, Cmd As String, Object As String, Seq As String, Tws As String, Twe As String, Selection As String, Fields As String, Data As String, BcNum As String, Attach As String
        GetKeyItem ReqId, strBcData, "{=USR=}", "{="
        GetKeyItem Dest1, strBcData, "{=USR=}", "{="
        GetKeyItem Dest2, strBcData, "{=WKS=}", "{="
        If (Len(Dest2) > 0) Then
            ReqId = Dest2
        End If

        GetKeyItem Cmd, strBcData, "{=CMD=}", "{="
        GetKeyItem Object, strBcData, "{=TBL=}", "{="
        GetKeyItem Seq, strBcData, "{=XXX=}", "{="
        GetKeyItem Tws, strBcData, "{=TWS=}", "{="
        GetKeyItem Twe, strBcData, "{=TWE=}", "{="
        GetKeyItem Selection, strBcData, "{=WHE=}", "{="
        GetKeyItem Fields, strBcData, "{=FLD=}", "{="
        GetKeyItem Data, strBcData, "{=DAT=}", "{="
        GetKeyItem BcNum, strBcData, "{=BCNUM=}", "{="
        GetKeyItem Attach, strBcData, "{=ATTACH=}", "{="
        
        MyBC_OnBcReceive ReqId, Dest1, Dest2, Cmd, Object, Seq, Tws, Twe, Selection, Fields, Data, BcNum
    End If
End Sub

Private Sub MyBC_OnBcReceive(ByVal ReqId As String, ByVal Dest1 As String, ByVal Dest2 As String, ByVal Cmd As String, ByVal Object As String, ByVal Seq As String, ByVal Tws As String, ByVal Twe As String, ByVal Selection As String, ByVal Fields As String, ByVal Data As String, ByVal BcNum As String)
    Dim strRet As String
    Dim strUrno As String
    Dim llItem As Long
    Dim llTabLine As Long
    Dim ItemNo As Long
    Dim strVal As String
    Dim strValues As String
    Dim strTIFA As String
    Dim strTIFD As String
    Dim strOldTIFA As String
    Dim strOldTIFD As String
    Dim strLineValues As String
    Dim ttyp As String
    Dim strSkey As String
    Dim strAdid As String
    Dim strUsec As String
    Dim strFlnu As String
    Dim strCFIAddi As String
    On Error GoTo ProcError
    'kkh on 16/07/2008 P2 Store Decision into CFITAB
    Dim strTYPE As String

    Dim strLineNo As String 'igu on 27 Jul 2010

    llItem = GetRealItemNo(Fields, "URNO")
    strUrno = GetRealItem(Data, llItem, ",")
    Select Case (Cmd)
        Case "CLO"
            MsgBox "The server had switched." & vbCrLf & "You can restart the application in one minute."
            End
        Case "RAC"
            'handle the join flight
            'igu on 08/02/2013
            Dim arrData As Variant
            Dim intCount As Integer
            Dim i As Integer
            
            arrData = Split(Data, ",")
            intCount = UBound(arrData) + 1
            
            For i = 1 To intCount Step 2
                strUrno = GetItem(Data, i + 1, ",")
                HandleUFR strUrno, "RKEY,URNO", arrData(i - 1) & "," & arrData(i)
            Next i
        Case "DFR"
            strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strUrno, 0)
            llTabLine = frmData.tabData(0).GetNextResultLine
            If llTabLine <> -1 Then
                frmData.tabData(0).DeleteLine llTabLine
            End If
        Case "UFR", "UPS", "UPJ"
            'UTC/LOCAL time
            HandleLoacUTC Fields, Data
            HandleUFR strUrno, Fields, Data 'TO DO 'N'=NOOP,'X'=CANCEL,'D'=DIVERTED behandeln
        Case "ISF"
            'HandleLoacUTC Fields, data
            ItemNo = GetRealItemNo(Fields, "FTYP")
            If ItemNo > -1 Then
                strVal = GetRealItem(Data, ItemNo, ",")
                If InStr(strAllFtyps, strVal) > 0 Then 'Check if relevant FTYP
                    ItemNo = GetRealItemNo(Fields, "TIFA")
                    If ItemNo > -1 Then strTIFA = GetRealItem(Data, ItemNo, ",")
                    ItemNo = GetRealItemNo(Fields, "TIFA")
                    If ItemNo > -1 Then strTIFD = GetRealItem(Data, ItemNo, ",")
                    If (strTIFA >= strTimeFrameFrom And strTIFA <= strTimeFrameTo) Or _
                       (strTIFD >= strTimeFrameFrom And strTIFD <= strTimeFrameTo) Then
                       ItemNo = GetRealItemNo(Fields, "SKEY")
                        If ItemNo > -1 Then
                            strSkey = GetRealItem(Data, ItemNo, ",")
                            strSkey = Trim(strSkey)
                            If strSkey <> "" Then
                                HandleISF strSkey
                            End If
                        End If
                    End If
                End If
            End If
        Case "URT"
            'UTC/LOCAL time
            HandleLoacUTC Fields, Data
            If Object = "TLXTAB" Then
                ItemNo = GetRealItemNo(Fields, "TTYP")
                If ItemNo > -1 Then
                    ttyp = GetRealItem(Data, ItemNo, ",")
                    'If ttyp = "PTM" Or ttyp = "KRIS" Then 'igu on 23/08/2011: ALTEA
                    If ttyp = "PTM" Or ttyp = "KRIS" Or ttyp = "ALT" Then 'igu on 23/08/2011: ALTEA
                        ItemNo = GetRealItemNo(Fields, "FLNU")
                        If ItemNo > -1 Then
                            strUrno = GetRealItem(Data, ItemNo, ",")
                            strUrno = Trim(strUrno)
                            If strUrno <> "" And strUrno <> "0" Then
                                'Is it the selected flight?
                                If tabTab(0).GetFieldValue(0, "URNO_A") = strUrno Then
                                    'Just buffer it in a tab ==> a timer will reread
                                    tabFlnuBcs.InsertTextLine strUrno, False
                                End If
                            End If
                        End If
                    End If
                End If
            End If
            'kkh on 16/07/2008 P2 Store Decision into CFITAB
            If Object = "CFITAB" Then
                ItemNo = GetRealItemNo(Fields, "URNO")
                If ItemNo > -1 Then
                    strUrno = GetRealItem(Data, ItemNo, ",")
                    If strUrno <> "" And strUrno <> "0" Then
                        'llTabLine = frmData.tabData(13).GetLinesByIndexValue("URNO", strUrno, 0) 'igu on 27 Jul 2010
                        strLineNo = frmData.tabData(13).GetLinesByIndexValue("URNO", strUrno, 0) 'igu on 27 Jul 2010
                        If strLineNo = "" Then llTabLine = -1 Else llTabLine = Val(strLineNo) 'igu on 27 Jul 2010
                        If llTabLine > -1 Then
'                            strVal = "ADDI,USEU,LSTU,URNO"
'                            frmData.tabData(13).SetFieldValues llTabLine, strVal, Data
                            frmData.tabData(13).SetFieldValues llTabLine, Fields, Data
                            
                            'check if the arrival flight is currently selected
                            strFlnu = frmData.tabData(13).GetFieldValue(llTabLine, "AURN")
                            If tabTab(0).GetFieldValue(0, "URNO_A") = strFlnu Then 'selected flight
                                strCFIAddi = frmData.tabData(13).GetFieldValue(llTabLine, "ADDI")
                                'find departure urno
                                strFlnu = frmData.tabData(13).GetFieldValue(llTabLine, "DURN")
                                strRet = tabTab(1).GetLinesByIndexValue("URNO", strFlnu, 0)
                                llTabLine = tabTab(1).GetNextResultLine
                                Do While llTabLine > -1
                                    tabTab(1).SetFieldValues llTabLine, "Hold", strCFIAddi
                                    tabTab(1).SetFieldValues llTabLine, "CFI_URNO", strUrno
                                    
                                    llTabLine = tabTab(1).GetNextResultLine
                                Loop
                                tabTab(1).Refresh
                            End If
                        End If
                    End If
                End If
            End If
        Case "IRT"
'            If Object = "SRLTAB" Then
'                itemno = GetRealItemNo(Fields, "USEC")
'                If itemno > -1 Then
'                    strUsec = GetRealItem(Data, itemno, ",")
'                    If strUsec = "HUBMGR" Then
'                        itemno = GetRealItemNo(Fields, "FLNU")
'                        If itemno > -1 Then
'                            strFlnu = Trim(GetRealItem(Data, itemno, ","))
'                            'strFlnu = Trim(strUrno)
'                            strRet = frmData.tabData(10).GetLinesByIndexValue("FLNU", strFlnu, 0)
'                            llTabLine = frmData.tabData(10).GetNextResultLine
'                            If llTabLine > -1 Then
'                                While llTabLine > -1
'                                    frmData.tabData(10).SetFieldValues llTabLine, Fields, Data
'                                    llTabLine = frmData.tabData(10).GetNextResultLine
'                                Wend
'                            Else
'                                frmData.tabData(10).InsertTextLine ",,,,,,,,,,,,,,,", False
'                                frmData.tabData(10).SetFieldValues frmData.tabData(10).GetLineCount - 1, Fields, Data
'                                frmData.tabData(10).Sort "1", True, True
'                            End If
'                        End If
'                    End If
'                End If
'            End If
            HandleLoacUTC Fields, Data
            'kkh on 16/07/2008 P2 Store Decision into CFITAB
            If Object = "CFITAB" Then
                ItemNo = GetRealItemNo(Fields, "TYPE")
                If ItemNo > -1 Then
                    strTYPE = GetRealItem(Data, ItemNo, ",")
                    If strTYPE = "HUB" Then
                        ItemNo = GetRealItemNo(Fields, "URNO")
                        If ItemNo > -1 Then
                            'igu on 27 Jul 2010
                            'check if record already inserted in local TAB
                            '---------------------------------------------
                            strUrno = GetRealItem(Data, ItemNo, ",")
                            'ItemNo = GetRealItemNo(frmData.tabData(13).LogicalFieldList, "URNO")
                            
                            ItemNo = GetRealItemNo(Fields, "ADDI")
                            strCFIAddi = GetRealItem(Data, ItemNo, ",")

                            'strLineNo = frmData.tabData(13).GetLinesByColumnValue(ItemNo, strUrno, 0)
                            strLineNo = frmData.tabData(13).GetLinesByIndexValue("URNO", strUrno, 0)
                            If strLineNo = "" Then
                                llTabLine = -1
                            Else
                                llTabLine = Val(strLineNo)
                            End If
                            If llTabLine > -1 Then 'found, update
                                frmData.tabData(13).SetFieldValues llTabLine, Fields, Data
                            Else 'not found, insert
                                frmData.tabData(13).InsertTextLine ",,,,,,,,,,,,,,,,,,,,,,,,,,,", False
                                frmData.tabData(13).SetFieldValues frmData.tabData(13).GetLineCount - 1, Fields, Data
                            End If
                            frmData.tabData(13).Sort "1", True, True
                            'end of check if record already inserted in local TAB
                            '----------------------------------------------------

                            ItemNo = GetRealItemNo(Fields, "AURN")
                            If ItemNo > -1 Then
                                strFlnu = Trim(GetRealItem(Data, ItemNo, ","))
                                If tabTab(0).GetFieldValue(0, "URNO_A") = strFlnu Then 'selected flight
                                    'find departure urno
                                    ItemNo = GetRealItemNo(Fields, "DURN")
                                    If ItemNo > -1 Then
                                        strFlnu = Trim(GetRealItem(Data, ItemNo, ","))
                                        strRet = tabTab(1).GetLinesByIndexValue("URNO", strFlnu, 0)
                                        llTabLine = tabTab(1).GetNextResultLine
                                        Do While llTabLine > -1
                                            tabTab(1).SetFieldValues llTabLine, "Hold", strCFIAddi
                                            tabTab(1).SetFieldValues llTabLine, "CFI_URNO", strUrno
                                            
                                            llTabLine = tabTab(1).GetNextResultLine
                                        Loop
                                        tabTab(1).Refresh
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Case "SBC"
'            Select Case (Object)
'                Case "LOA:IRT"
'                    If ReqId <> omWKS Then
'                        If Selection <> "" Then
'                            ItemNo = GetRealItemNo(Fields, "URNO")
'                            If ItemNo > -1 Then
'                                strUrno = GetRealItem(Data, ItemNo, ",")
'                                strUrno = Trim(strUrno)
'                            End If 'First we must check if this item exists, then do nothing == own bc
'                            frmData.tabData(3).GetLinesByIndexValue "FLNU", strUrno, 0
'                            'frmData.tabData(3).GetLinesByIndexValue "FLNU", Selection, 0
'                            llTabLine = frmData.tabData(3).GetNextResultLine
'                            If llTabLine = -1 Then
'                                frmData.tabData(3).InsertTextLine Data, False
'                                'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights
'                                'frmData.tabData(3).Sort "1,2,3", True, True  ' This reorgs the indexes as well!
'                                frmData.tabData(3).Sort "3,2", True, True  ' This reorgs the indexes as well!
'                                If Selection = omCurrentSelectedUrno Then
'                                    gantt_OnLButtonDownBar 0, omCurrentSelectedUrno, 0
'                                End If
'                            Else
'                                If Selection = omCurrentSelectedUrno Then
'                                    gantt_OnLButtonDownBar 0, omCurrentSelectedUrno, 0
'                                End If
'                            End If
'                            SingleBarConflictCheck Selection
'                            frmData.tabData(0).GetLinesByIndexValue "URNO", Selection, 0
'                            llTabLine = frmData.tabData(0).GetNextResultLine
'                            If llTabLine > -1 Then
'                                UpdateBar llTabLine
'                            End If
'                        End If
'                    End If
'                Case "LOA:URT"
'                    If ReqId <> omWKS Then
'                        If Selection <> "" Then
'                            ItemNo = GetRealItemNo(Fields, "URNO")
'                            If ItemNo > -1 Then
'                                strUrno = GetRealItem(Data, ItemNo, ",")
'                                strUrno = Trim(strUrno)
'                            End If 'First we must check if this item exists, then do nothing == own bc
'                            frmData.tabData(3).GetLinesByIndexValue "URNO", strUrno, 0
'                            SingleBarConflictCheck (strUrno)
'                            llTabLine = frmData.tabData(3).GetNextResultLine
'                            If llTabLine > -1 Then
'                                frmData.tabData(3).SetFieldValues llTabLine, frmData.tabData(3).LogicalFieldList, Data
'                                If Selection = omCurrentSelectedUrno Then
'                                    gantt_OnLButtonDownBar 0, omCurrentSelectedUrno, 0
'                                End If
'                            End If
'                            SingleBarConflictCheck Selection
'                            frmData.tabData(0).GetLinesByIndexValue "URNO", Selection, 0
'                            llTabLine = frmData.tabData(0).GetNextResultLine
'                            If llTabLine > -1 Then
'                                UpdateBar llTabLine
'                            End If
'                        End If
'                    End If
'                Case "LOA:DRT"
'                    If ReqId <> omWKS Then
'                        If Selection <> "" Then
'                            frmData.tabData(3).GetLinesByIndexValue "URNO", Selection, 0
'                            llTabLine = frmData.tabData(3).GetNextResultLine
'                            If llTabLine > -1 Then
'                                frmData.tabData(3).DeleteLine llTabLine
'                                'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights
'                                'frmData.tabData(3).Sort "1,2,3", True, True  ' This reorgs the indexes as well!
'                                frmData.tabData(3).Sort "3,2", True, True  ' This reorgs the indexes as well!
'                                If Data = omCurrentSelectedUrno Then
'                                    gantt_OnLButtonDownBar 0, omCurrentSelectedUrno, 0
'                                End If
'                            End If
'                            SingleBarConflictCheck Data
'                            frmData.tabData(0).GetLinesByIndexValue "URNO", Data, 0
'                            llTabLine = frmData.tabData(0).GetNextResultLine
'                            If llTabLine > -1 Then
'                                UpdateBar llTabLine
'                            End If
'                        End If
'                    End If
'            End Select
    End Select
ExitSub:
    Exit Sub
    
ProcError:
    Resume ExitSub
End Sub

'-----------------------------------------------------
' 1. Reloads the AFTTAB records for this SKEY, use the
'    frmData.tabReread for tmepory storage and
'    reorgs the AFTdata in frmData.tabData(0)
' 2. checks the flight with FLNU for connection conflicts
'    and redraws the bar in the charts
'-----------------------------------------------------
Public Sub HandleISF(strSkey As String)
    Dim strWhere As String
    Dim strAdid As String
    Dim llLineNo As Long
    Dim llCurrLine As Long
    Dim strRet As String
    Dim cnt As Long
    Dim strUrno As String
    Dim i As Long
    Dim strFieldList As String
    Dim strLineValues As String
    
    frmData.tabReread.ResetContent
    frmData.tabReread.HeaderString = frmData.tabData(0).HeaderString
    frmData.tabReread.HeaderLengthString = frmData.tabData(0).HeaderLengthString
    frmData.tabReread.LogicalFieldList = frmData.tabData(0).LogicalFieldList
    strFieldList = frmData.tabReread.LogicalFieldList
    frmData.tabReread.EnableHeaderSizing True
    
    frmData.tabReread.CedaServerName = strServer
    frmData.tabReread.CedaPort = "3357"
    frmData.tabReread.CedaHopo = strHopo
    frmData.tabReread.CedaCurrentApplication = "HUB-Manager" & "," & frmData.GetApplVersion(True)
    frmData.tabReread.CedaTabext = "TAB"
    frmData.tabReread.CedaUser = strCurrentUser
    frmData.tabReread.CedaWorkstation = frmData.aUfis.WorkStation
    frmData.tabReread.CedaSendTimeout = "3"
    frmData.tabReread.CedaReceiveTimeout = "240"
    frmData.tabReread.CedaRecordSeparator = Chr(10)
    frmData.tabReread.CedaIdentifier = "HUB-Manager"
    
    strWhere = "WHERE (((TIFA BETWEEN '" + strTimeFrameFrom + "' AND '" + strTimeFrameTo + "') AND ADID IN ('A','B')) OR " + _
                        "((TIFD BETWEEN '" + strTimeFrameFrom + "' AND '" + strTimeFrameTo + "') AND ADID IN ('D','B'))) AND " + _
                        "FTYP IN ('S', 'O', 'X') AND ALC2 IN ('SQ','MI') " 'igu on 10/01/2012
    frmData.tabReread.CedaAction "RT", "AFTTAB", frmData.tabReread.HeaderString, "", strWhere
    
    'kkh UTC / local solve!!
    If TimesInUTC = False Then
        ChangetabRereadToLocal
    End If
    
    cnt = frmData.tabReread.GetLineCount - 1
    For i = 0 To cnt
        strUrno = frmData.tabReread.GetFieldValue(i, "URNO")
        strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strUrno, 0)
        llLineNo = frmData.tabData(0).GetNextResultLine
        strLineValues = frmData.tabReread.GetLineValues(i)
        If llLineNo > -1 Then
            frmData.tabData(0).SetFieldValues llLineNo, strFieldList, strLineValues
        Else
            frmData.tabData(0).InsertTextLine strLineValues, False
        End If
    Next i
    frmData.tabData(0).Sort "1,3", True, True 'This reorgs all indexes as well
End Sub

'kkh UTC / local solve!!
Public Sub ChangetabRereadToLocal()
    Dim cnt As Long
    Dim fldCnt As Integer
    Dim i As Long
    Dim j As Long
    Dim strField As String
    Dim strVal As String
    Dim myDat As Date
    
    cnt = frmData.tabReread.GetLineCount - 1
    fldCnt = ItemCount(strAftTimeFields, ",")
    For i = 0 To cnt
        For j = 0 To fldCnt
            strField = GetRealItem(strAftTimeFields, CInt(j), ",")
            strVal = frmData.tabReread.GetFieldValue(i, strField)
            If Trim(strVal) <> "" Then
                myDat = CedaFullDateToVb(strVal)
                If TimesInUTC = False Then
                    myDat = DateAdd("h", UTCOffset, myDat)
                End If
                strVal = Format(myDat, "YYYYMMDDhhmmss")
                frmData.tabReread.SetFieldValues i, strField, strVal
            End If
        Next j
    Next i
    frmData.tabReread.Refresh
End Sub

Private Sub tabtab_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim SortCol As Integer
    Dim LastColno As Integer
    
    If Index = 1 And LineNo = -1 Then
        SortCol = ColNo
        LastColno = tabTab(Index).CurrentSortColumn
        tabTab(Index).ColSelectionRemoveAll
        If SortCol >= 0 Then
            If ColNo <> LastColno Then
                tabTab(Index).Sort CStr(SortCol), True, True
            Else
                If tabTab(Index).SortOrderASC Then
                    tabTab(Index).Sort CStr(SortCol), False, True
                Else
                    tabTab(Index).Sort CStr(SortCol), True, True
                End If
            End If
            tabTab(Index).ColSelectionAdd ColNo
        End If
        'tabTab(Index).AutoSizeColumns
        tabTab(Index).Refresh
    End If
End Sub

Private Sub tabTab_ComboSelChanged(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal OldValue As String, ByVal NewValues As String)
    Dim strTimeValue As String
    Dim strDecisionValue As String
    
    If NewValues = " " Then
        'this is for user to key in free text as decision
        'tabTab(1).SetInplaceEdit LineNo, 19, True
        'tabTab(1).SetInplaceEdit LineNo, 21, True
        
        'kkh on 16/07/2008 P2 Store Decision into CFITAB
        tabTab(1).SetFieldValues LineNo, "Hold", NewValues
        tabTab(1).SetFieldValues LineNo, "COMBO", ""
        tabTab(1).Refresh
        StoreCFITABNew LineNo, Index

    Else
        tabTab(1).SetFieldValues LineNo, "Hold", NewValues
        tabTab(1).SetFieldValues LineNo, "COMBO", ""
        tabTab(1).Refresh
'        strDecisionValue = NewValues
'        strTimeValue = tabTab(1).GetFieldValue(LineNo, "TRANSFER")
'        'StoreSRLTAB LineNo, strDecisionValue, strTimeValue
'
'        'kkh on 16/07/2008 P2 Store Decision into CFITAB
        StoreCFITABNew LineNo, Index
    End If
End Sub

Private Sub tabTab_EditPositionChanged(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal Value As String)
    Dim strUrnoList As String
    Dim strRurn As String
    
    strRurn = tabTab(Index).GetFieldValue(LineNo, "LOA_RURN")
End Sub

Private Sub tabTab_InplaceEditCell(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As String, ByVal OldValue As String)
    Dim strUrnoList As String
    Dim strRurn As String
    Dim llLine As Long
    Dim strUrno As String
    Dim arrUrnos() As String
    Dim strSTYP As String
    Dim i As Integer
    Dim blFound As Boolean
    Dim strWhere As String
    Dim strValues As String
    Dim ilEpax As Integer
    Dim ilBpax As Integer
    Dim ilFpax As Integer
    Dim ilUpax As Integer
    Dim ilIdx As Long
    Dim strTimeValue As String
    Dim strDecisionValue As String

    
    blFound = False
    If NewValue = OldValue Then
        Exit Sub
    End If
    If ColNo = 22 Or ColNo = 24 Then
        'kkh on 16/07/2008 P2 Store Decision into CFITAB
'        strUrno = tabTab(Index).GetFieldValue(LineNo, "AFTURNO")
'        strDecisi`onValue = tabTab(Index).GetFieldValue(LineNo, "Hold")
'        strTimeValue = tabTab(Index).GetFieldValue(LineNo, "TRANSFER")
'        StoreCFITAB LineNo, strDecisionValue, strTimeValue
        'StoreSRLTAB LineNo, strDecisionValue, strTimeValue
        
        StoreCFITABNew LineNo, Index
    Else
        strUrnoList = tabTab(Index).GetFieldValue(LineNo, "L_URNOS")
        arrUrnos = Split(strUrnoList, ";")
        For i = 0 To UBound(arrUrnos)
            strUrno = arrUrnos(i)
            strWhere = "WHERE URNO=" & strUrno
            frmData.tabData(3).GetLinesByIndexValue "URNO", strUrno, 0
            llLine = frmData.tabData(3).GetNextResultLine
            If llLine > -1 Then
                strSTYP = frmData.tabData(3).GetFieldValue(llLine, "STYP")
                'Customize for GOCC 25/02/2008
'                If ColNo = 10 And strSTYP = "F" Then
'                    blFound = True
'                    i = UBound(arrUrnos) + 1
'                End If
'                If ColNo = 11 And strSTYP = "B" Then
'                    blFound = True
'                    i = UBound(arrUrnos) + 1
'                End If
'                If ColNo = 12 And strSTYP = "E" Then
'                    blFound = True
'                    i = UBound(arrUrnos) + 1
'                End If
                If ColNo = 11 And strSTYP = "F" Then
                    blFound = True
                    i = UBound(arrUrnos) + 1
                End If
                If ColNo = 13 And strSTYP = "B" Then
                    blFound = True
                    i = UBound(arrUrnos) + 1
                End If
                If ColNo = 15 And strSTYP = "U" Then
                    blFound = True
                    i = UBound(arrUrnos) + 1
                End If
                If ColNo = 17 And strSTYP = "E" Then
                    blFound = True
                    i = UBound(arrUrnos) + 1
                End If
            End If
        Next i
        If blFound = True And llLine > -1 Then
            frmData.tabData(3).SetFieldValues llLine, "VALU", NewValue
            'LOATAB;URNO,FLNU,FLNO,DSSN,RURN,TYPE,STYP,SSTP,SSST,APC3,VALU,RFLD,RTAB;
            'LOATAB;URNO,FLNU,FLNO,DSSN,RURN,TYPE,STYP,SSTP,SSST,APC3,VALU,ADDI,HOPO;
            'kkh 02/02/2009 HOPO require
            'strValues = frmData.tabData(3).GetFieldValues(llLine, "URNO,FLNU,FLNO,DSSN,RURN,TYPE,STYP,SSTP,SSST,APC3,VALU,RFLD") ',RTAB")
            'strValues = frmData.tabData(3).GetFieldValues(llLine, "URNO,FLNU,FLNO,DSSN,RURN,TYPE,STYP,SSTP,SSST,APC3,VALU,RFLD,HOPO") ',RTAB")
            strValues = frmData.tabData(3).GetFieldValues(llLine, "URNO,FLNU,FLNO,DSSN,RURN,TYPE,STYP,SSTP,SSST,APC3,VALU,ADDI,HOPO") ',RTAB")
            strValues = strValues + ","
            strValues = CleanNullValues(strValues)
            frmData.SetServerParameters
            If frmData.aUfis.CallServer("URT", "LOATAB", frmData.tabData(3).LogicalFieldList, _
                                        strValues, strWhere, "230") <> 0 Then
                MsgBox "Update Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
            Else
                frmData.SetServerParameters
                frmData.aUfis.CallServer "SBC", "LOA:URT", frmData.tabData(3).LogicalFieldList, strValues, omCurrentSelectedUrno, "230"
            End If
            '"AFTURNO,LOA_RURN,ACT,FLNO,DATE,STD,EST,ATD,DES,T-PAX,F-PAX,B-PAX,E-PAX,NA,Hold,DUMMY,L_URNOS"
            ilEpax = Val(tabTab(Index).GetFieldValue(LineNo, "Y-PAX"))
            ilBpax = Val(tabTab(Index).GetFieldValue(LineNo, "C-PAX"))
            ilFpax = Val(tabTab(Index).GetFieldValue(LineNo, "F-PAX"))
            'Customize for GOCC 25/02/2008
            ilUpax = Val(tabTab(Index).GetFieldValue(LineNo, "U-PAX"))
            'tabTab(Index).SetFieldValues LineNo, "T-PAX", CStr(ilEpax + ilBpax + ilFpax)
            tabTab(Index).SetFieldValues LineNo, "T-PAX", CStr(ilEpax + ilBpax + ilFpax + ilUpax)
            tabTab(Index).Refresh
        End If
    End If
End Sub

Public Sub StoreCFITABNew(LineNo As Long, Index As Integer)
    Dim strRet As String
    Dim llLineNo As Long
    Dim lineIDX As Long
    Dim strCdat As String
    Dim datCdat As Date
    'Arr flight info
    Dim strArrUrno As String
    Dim strArrFlno As String
    Dim strDate As String
    Dim strStoa As String
    Dim strRGNA As String
    Dim strORG3 As String
    Dim strVSA3 As String
    Dim strPSTA As String
    Dim strGATA As String
    Dim strTTPA As String
    Dim strTYPE As String
    Dim strTALL As String
    Dim strTCLF As String
    Dim strTCLC As String
    Dim strTCLY As String
    Dim strTOTH As String
    Dim strTimeValue As String
    Dim strTransfertime As String
    Dim strDecisionValue As String
    'Dep flight info
    Dim strDepUrno As String
    Dim strDepFlno As String
    Dim strStod As String
    Dim strDES3 As String
    Dim strGATD As String
    Dim strTTPD As String
    
    Dim strFields As String
    Dim strData As String
    Dim strCfiUrno As String
    Dim strRet2 As String
    Dim strValues As String
    
    Dim aLineNo As Variant 'igu on 27 Jul 2010
    Dim intLineCount As Integer 'igu on 27 Jul 2010
    Dim strWhere As String 'igu on 27 Jul 2010
    Dim i As Integer 'igu on 27 Jul 2010
    
    'kkh on 20/01/2009 check UTC or local before insert into CFITAB and L04TAB
    datCdat = Now
    'If TimesInUTC = False Then
        'UTCOffset = frmData.GetUTCOffset
        datCdat = DateAdd("h", -UTCOffset, datCdat)
    'End If
    strCdat = Format(datCdat, "YYYYMMDDhhmmss")
    
    strArrUrno = tabTab(0).GetFieldValue(0, "URNO_A")
    strArrFlno = tabTab(0).GetFieldValue(0, "FLNO_A")
    'FLNO_A,DATE_A,STA,ETA,ONBL,NA_A,ORG,VIA_A,POS_A,TGA_A,GAT_A,,NA_A,A/C_A,REGN_A
    strStoa = tabTab(0).GetFieldValue(0, "STOA_A")
    strRGNA = tabTab(0).GetFieldValue(0, "REGN_A")
    strORG3 = tabTab(0).GetFieldValue(0, "ORG")
    strVSA3 = tabTab(0).GetFieldValue(0, "VIA_A")
    strPSTA = tabTab(0).GetFieldValue(0, "POS_A")
    strGATA = tabTab(0).GetFieldValue(0, "GAT_A")
    strTTPA = tabTab(0).GetFieldValue(0, "NA_A")
       
    strTALL = tabTab(Index).GetFieldValue(LineNo, "T-PAX")
    strTCLF = tabTab(Index).GetFieldValue(LineNo, "F-PAX")
    strTCLC = tabTab(Index).GetFieldValue(LineNo, "C-PAX")
    strTCLY = tabTab(Index).GetFieldValue(LineNo, "Y-PAX")
    strTOTH = tabTab(Index).GetFieldValue(LineNo, "U-PAX")
    strTimeValue = Replace(tabTab(Index).GetFieldValue(LineNo, "TRANSFER"), ":", "")
    strDecisionValue = tabTab(Index).GetFieldValue(LineNo, "Hold")
    
    strDepUrno = tabTab(1).GetFieldValue(LineNo, "AFTURNO")
    strDepFlno = tabTab(1).GetFieldValue(LineNo, "FLNO")
    'ACT,FLNO,TGD_D,GATE,STD,EST,ATD,DES,NA
    strStod = tabTab(Index).GetFieldValue(LineNo, "STD")
    strDES3 = tabTab(Index).GetFieldValue(LineNo, "DES")
    strGATD = tabTab(Index).GetFieldValue(LineNo, "GATE")
    strTTPD = tabTab(Index).GetFieldValue(LineNo, "NA")
    
    If strDepUrno <> "" And strArrUrno <> "" Then
        strRet2 = ""
        'strRet2 = frmData.tabData(13).GetLinesByMultipleColumnValue("1,2", strArrUrno + "," + strDepUrno, 0)
        strRet2 = frmData.tabData(13).GetLinesByMultipleColumnValue("5,22", strArrUrno + "," + strDepUrno, 0)
        
        'strRet2 = frmData.tabData(13).GetLinesByColumnValue("22", strDepUrno, 0)
        If strRet2 <> "" Then
            'update decision
            strFields = "USEC,CDAT,USEU,LSTU,AURN,FLNA,STOA,RGNA,ORG3,VSA3,PSTA,GATA,TTPA,TYPE,TALL,TCLF,TCLC,TCLY,TOTH,CTMS,ADDI,DURN,FLND,STOD,DES3,GATD,TTPD"
                        
            'strCfiUrno = frmData.tabData(13).GetFieldValue(strRet2, "URNO") 'igu on 27 Jul 2010
            
            'igu on 27 Jul 2010
            'check for duplicate entries
            '---------------------------
            aLineNo = Split(strRet2, ",")
            intLineCount = UBound(aLineNo) + 1
            
            strCfiUrno = ""
            For i = 0 To intLineCount - 1
                strRet2 = aLineNo(i)
                strCfiUrno = strCfiUrno & "," & frmData.tabData(13).GetFieldValue(strRet2, "URNO")
            Next i
            If strCfiUrno <> "" Then strCfiUrno = Mid(strCfiUrno, 2)
                
            If intLineCount = 1 Then
                strWhere = "WHERE URNO = " & strCfiUrno
            Else
                strWhere = "WHERE URNO IN (" & strCfiUrno & ")"
            End If
            
            strRet2 = aLineNo(0)
            'end check for duplicate entries
            '------------------------------
            
            'strData = strTimeValue + "," + strDecisionValue + "," + strCurrentUser + "," + strCdat
            strData = ""
            strData = frmData.tabData(13).GetFieldValue(strRet2, "USEC") & "," & frmData.tabData(13).GetFieldValue(strRet2, "CDAT") & ","
            strData = strData & strCurrentUser & "," & strCdat & ","
            strData = strData & strArrUrno & "," & strArrFlno & "," & strStoa & "," & strRGNA & "," & strORG3 & "," & strVSA3 & "," & strPSTA & "," & strGATA & "," & strTTPA & "," & "HUB" & ","
            strData = strData & strTALL & "," & strTCLF & "," & strTCLC & "," & strTCLY & "," & strTOTH & "," & strTimeValue & "," & strDecisionValue & ","
            strData = strData & strDepUrno & "," & strDepFlno & "," & strStod & "," & strDES3 & "," & strGATD & "," & strTTPD
            If frmData.aUfis.CallServer("URT", "CFITAB", strFields, strData, strWhere, 230) <> 0 Then
                MsgBox "Update Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
            Else
                'frmData.tabData(13).SetFieldValues strRet2, "ADDI,USEU,LSTU", strData
                'CFITAB;URNO,AURN,DURN,ADDI,TYPE,FLNA,FLND,USEU,USEC,CDAT,LSTU;
'                strData = ""
'                strFields = "ADDI,USEU,LSTU"
                'strData = strDecisionValue + "," + strCurrentUser + "," + strCdat
                'frmData.tabData(13).SetFieldValues strRet2, strFields, strData 'igu on 27 Jul 2010
                
                'igu on 27 Jul 2010
                'update all entries
                '------------------
                For i = 0 To intLineCount - 1
                    strRet2 = aLineNo(i)
                    frmData.tabData(13).SetFieldValues strRet2, strFields, strData
                Next i
                'end of update all entries
                '-------------------------
            End If
        Else
            'insert new decision
            'strFields = "URNO,AURN,DURN,ADDI,TYPE,FLNA,FLND,USEU,USEC,CDAT,LSTU"
            strFields = "URNO,USEC,CDAT,USEU,LSTU,AURN,FLNA,STOA,RGNA,ORG3,VSA3,PSTA,GATA,TTPA,TYPE,TALL,TCLF,TCLC,TCLY,TOTH,CTMS,ADDI,DURN,FLND,STOD,DES3,GATD,TTPD"
            strData = ""
            strData = frmData.GetNextUrno & ","
            'strData = strData & strArrUrno & "," & strDepUrno & "," & strDecisionValue & ",HUB," & strArrFlno & "," & strDepFlno & ","
            strData = strData & strCurrentUser & "," & strCdat & ",,,"
            strData = strData & strArrUrno & "," & strArrFlno & "," & strStoa & "," & strRGNA & "," & strORG3 & "," & strVSA3 & "," & strPSTA & "," & strGATA & "," & strTTPA & "," & "HUB" & ","
            strData = strData & strTALL & "," & strTCLF & "," & strTCLC & "," & strTCLY & "," & strTOTH & "," & strTimeValue & "," & strDecisionValue & ","
            strData = strData & strDepUrno & "," & strDepFlno & "," & strStod & "," & strDES3 & "," & strGATD & "," & strTTPD
            
            If frmData.aUfis.CallServer("IRT", "CFITAB", strFields, strData, "", 230) <> 0 Then
                MsgBox "Insert Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
            Else
                'igu on 27 Jul 2010
                'update TAB
                '----------
                frmData.tabData(13).InsertTextLine "", False
                frmData.tabData(13).SetFieldValues frmData.tabData(13).GetLineCount - 1, strFields, strData
                'end of update TAB
                '-----------------
            End If
        End If
    End If
End Sub

Public Sub HandleLoacUTC(Fields As String, Data As String)
    Dim cnt As Long
    Dim fldCnt As Integer
    Dim i As Integer
    Dim strField As String
    Dim strVal As String
    Dim myDat As Date
    Dim idxFld As Long
    Dim OldData As String
    
    OldData = Data
    
    If TimesInUTC = False Then
        fldCnt = ItemCount(strAftTimeFields, ",")
        For i = 0 To fldCnt
            strField = GetRealItem(strAftTimeFields, CInt(i), ",")
            idxFld = GetRealItemNo(Fields, strField)
            If idxFld > -1 Then
                strVal = GetRealItem(Data, idxFld, ",")
                If Trim(strVal) <> "" Then
                    myDat = CedaFullDateToVb(strVal)
                        myDat = DateAdd("h", UTCOffset, myDat)
                    strVal = Format(myDat, "YYYYMMDDhhmmss")
                    SetItem Data, idxFld + 1, ",", strVal
                End If
            End If
        Next i
    End If
End Sub

'-----------------------------------------------------------------------
' For flights updates by broadcasts Commands "UFR", "UPS", "UPJ"
'-----------------------------------------------------------------------
Public Sub HandleUFR(ByVal strUrno As String, ByVal strFields As String, ByVal strData As String)
    Dim blnDelete As Boolean
    Dim strALC2 As String
    Dim strFTYP As String
    Dim strAdid As String
    Dim strTIFA As String
    Dim strTIFD As String
    Dim strRKEY As String
    Dim strURNO_A As String
    Dim strRet As String
    Dim llTabLine As Long
    Dim llArrLine As Long
    Dim llLine As Long
    Dim strArrUrno As String
    Dim strFilterFields As String
    Dim strFilterData As String
    
    Debug.Print strData
    Debug.Print strFields
    
    strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strUrno, 0)
    llTabLine = frmData.tabData(0).GetNextResultLine
    If llTabLine <> -1 Then
        frmData.tabData(0).SetFieldValues llTabLine, strFields, strData
        frmData.tabData(0).Refresh
        
        strAdid = frmData.tabData(0).GetFieldValue(llTabLine, "ADID")
        
        strALC2 = frmData.tabData(0).GetFieldValue(llTabLine, "ALC2")
        blnDelete = Not (strALC2 = "SQ" Or strALC2 = "MI")
        If Not blnDelete Then
            strFTYP = frmData.tabData(0).GetFieldValue(llTabLine, "FTYP")
            strFTYP = Trim(strFTYP)
            blnDelete = Not (strFTYP = "S" Or strFTYP = "O" Or strFTYP = "X")
        End If
        'TODO
        'check timeframe
        'If Not blnDelete Then
            'blnDelete = ??
        'End If
        
        If blnDelete Then
            frmData.tabData(0).DeleteLine llTabLine
            frmData.tabData(0).Sort "1,3", True, True 'This reorgs all indexes as well
        Else
            If strAdid = "D" Then
            'TODO
            '    CheckArrivalsForDeparture strUrno
            End If
        End If
        
        'Find in filter tab
        strRet = tabFlights.GetLinesByIndexValue("URNO", strUrno, 0)
        llLine = tabFlights.GetNextResultLine
        If llLine <> -1 Then
            If strAdid = "A" Then
                strFilterFields = "URNO,ADID,FLNO,STOA,ETAI,TIFA,ORG3"
            Else
                strFilterFields = "URNO,ADID,FLNO,STOD,ETDI,TIFD,DES3"
            End If
            strFilterData = frmData.tabData(0).GetFieldValues(llTabLine, strFilterFields)
            strFilterFields = tabFlights.LogicalFieldList
            tabFlights.SetFieldValues llLine, strFilterFields, strFilterData
            tabFlights.Refresh
        End If
        
        'Find in flight info tab
        If strAdid = "A" Then
            If tabTab(0).GetFieldValue(0, "URNO_A") = strUrno Then
                Call FillInArrivalFlightInfo(llTabLine)
            End If
        ElseIf strAdid = "D" Then
            If tabTab(0).GetFieldValue(0, "URNO_D") = strUrno Then
                strRKEY = GetItem(strData, GetItemNo(strFields, "RKEY"), ",")
                strURNO_A = tabTab(0).GetFieldValue(0, "URNO_A")
                If Trim(strURNO_A) = "" Then
                    If Trim(strRKEY) <> "" And strRKEY <> strUrno Then
                        'Find the arrival
                        strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strRKEY, 0)
                        llArrLine = frmData.tabData(0).GetNextResultLine
                        If llArrLine <> -1 Then
                            Call FillInArrivalFlightInfo(llArrLine)
                        End If
                    Else 'Phyoe (04-Jul-2013) UFIS-2852
                        Call FillInDepartureFlightInfo(llTabLine)
                    End If
                    
                ElseIf (strURNO_A = strRKEY) Then
                    Call FillInDepartureFlightInfo(llTabLine)
                Else
                    tabTab(0).SetFieldValues 0, "FLNO_D,DATE_D,STD,ETD,OFBL,NA_D,VIA_D,DES,POS_D,TGD_D,GAT_D,FLIGHT_DATE,URNO_D", _
                        " , , , , , , , , , , , , "
                End If
            Else
                strRKEY = GetItem(strData, GetItemNo(strFields, "RKEY"), ",")
                If (tabTab(0).GetFieldValue(0, "URNO_A") = strRKEY) Then
                    Call FillInDepartureFlightInfo(llTabLine)
                End If
            End If
            
            'Find in connection tab
            strRet = tabTab(1).GetLinesByIndexValue("URNO", strUrno, 0)
            llLine = tabTab(1).GetNextResultLine
            strArrUrno = tabTab(0).GetFieldValue(0, "URNO_A") 'get the arrival urno
            Do While llLine <> -1
                Call FillInConnectingFlightInfo(strArrUrno, llTabLine, llLine)
                
                llLine = tabTab(1).GetNextResultLine
            Loop
        End If
        tabTab(0).Refresh
    End If
End Sub

Private Sub FillInArrivalFlightInfo(ByVal llLine As Long)
    Dim strStoa As String
    Dim datArrSTOA As Date
    Dim strValue As String
    Dim MyDate As Date
    Dim strArrUrno As String
    Dim strDepUrno As String
    Dim llAftLine As Long
    Dim llCurrLine As Long
    
    strStoa = frmData.tabData(0).GetFieldValue(llLine, "STOA")
    If Trim(strStoa) <> "" Then
        datArrSTOA = CedaFullDateToVb(frmData.tabData(0).GetFieldValue(llLine, "STOA"))
    End If
    strValue = frmData.tabData(0).GetFieldValue(llLine, "FLNO")
    tabTab(0).SetFieldValues 0, "FLNO_A", strValue
                
    'kkh on 25/11/2008 cater for delay timing
    strValue = frmData.tabData(0).GetFieldValue(llLine, "STOA")
    strValue = Trim(strValue)
    If Trim(strValue) <> "" Then
        MyDate = CedaFullDateToVb(strValue)
        tabTab(0).SetFieldValues 0, "DATE_A", Format(MyDate, "DD.MM.YY")
        MyDate = CedaFullDateToVb(strValue)
        tabTab(0).SetFieldValues 0, "STA", Format(MyDate, "hh:mm") + "/" + Mid(strValue, 7, 2)
        tabTab(0).SetFieldValues 0, "STOA_A", strValue
    End If
    strValue = frmData.tabData(0).GetFieldValue(llLine, "ETAI")
    strValue = Trim(strValue)
    If Trim(strValue) <> "" Then
        MyDate = CedaFullDateToVb(strValue)
        tabTab(0).SetFieldValues 0, "ETA", Format(MyDate, "hh:mm") + "/" + Mid(strValue, 7, 2)
    End If
    strValue = frmData.tabData(0).GetFieldValue(llLine, "ONBL")
    strValue = Trim(strValue)
    If Trim(strValue) <> "" Then
        MyDate = CedaFullDateToVb(strValue)
        tabTab(0).SetFieldValues 0, "ONBL", Format(MyDate, "hh:mm") + "/" + Mid(strValue, 7, 2)
    End If
    
    strValue = frmData.tabData(0).GetFieldValue(llLine, "TTYP")
    tabTab(0).SetFieldValues 0, "NA_A", strValue
    strValue = frmData.tabData(0).GetFieldValue(llLine, "ORG3")
    tabTab(0).SetFieldValues 0, "ORG", strValue
    strValue = frmData.tabData(0).GetFieldValue(llLine, "VIA3")
    tabTab(0).SetFieldValues 0, "VIA_A", strValue
    strValue = frmData.tabData(0).GetFieldValue(llLine, "PSTA")
    tabTab(0).SetFieldValues 0, "POS_A", strValue
    strValue = frmData.tabData(0).GetFieldValue(llLine, "TGA1")
    tabTab(0).SetFieldValues 0, "TGA_A", strValue
    strValue = frmData.tabData(0).GetFieldValue(llLine, "GTA1")
    tabTab(0).SetFieldValues 0, "GAT_A", strValue
    strValue = frmData.tabData(0).GetFieldValue(llLine, "ACT3")
    tabTab(0).SetFieldValues 0, "A/C_A", strValue
    strValue = frmData.tabData(0).GetFieldValue(llLine, "REGN")
    tabTab(0).SetFieldValues 0, "REGN_A", strValue
    'kkh on 16/07/2008 P2 Store Decision into CFITAB
    strValue = frmData.tabData(0).GetFieldValue(llLine, "URNO")
    tabTab(0).SetFieldValues 0, "URNO_A", strValue
    strValue = frmData.tabData(0).GetFieldValue(llLine, "STOA")
    tabTab(0).SetFieldValues 0, "FLIGHT_DATE", strValue
    
    tabTab(0).Refresh
    
    'Refresh connectionTime
    strArrUrno = frmData.tabData(0).GetFieldValue(llLine, "URNO")
    For llCurrLine = 0 To tabTab(1).GetLineCount - 1
        strDepUrno = tabTab(1).GetFieldValue(llCurrLine, "AFTURNO")
        strValue = frmData.tabData(0).GetLinesByIndexValue("URNO", strDepUrno, 0)
        llAftLine = frmData.tabData(0).GetNextResultLine
        If llAftLine <> -1 Then
            Call FillInConnectionTimeInfo(strArrUrno, strDepUrno, llAftLine, llCurrLine)
        End If
    Next llCurrLine
    tabTab(1).Refresh
End Sub

Private Sub FillInDepartureFlightInfo(ByVal llLine As Long)
    Dim strValue As String
    Dim MyDate As Date
            
    strValue = frmData.tabData(0).GetFieldValue(llLine, "FLNO")
    tabTab(0).SetFieldValues 0, "FLNO_D", strValue
    
     'kkh on 25/11/2008 cater for delay timing
    strValue = frmData.tabData(0).GetFieldValue(llLine, "STOD")
    strValue = Trim(strValue)
    If Trim(strValue) <> "" Then
        MyDate = CedaFullDateToVb(strValue)
        tabTab(0).SetFieldValues 0, "DATE_D", Format(MyDate, "DD.MM.YY")
        MyDate = CedaFullDateToVb(strValue)
        tabTab(0).SetFieldValues 0, "STD", Format(MyDate, "hh:mm") + "/" + Mid(strValue, 7, 2)
    End If
    strValue = frmData.tabData(0).GetFieldValue(llLine, "ETDI")
    strValue = Trim(strValue)
    If Trim(strValue) <> "" Then
        MyDate = CedaFullDateToVb(strValue)
        tabTab(0).SetFieldValues 0, "ETD", Format(MyDate, "hh:mm") + "/" + Mid(strValue, 7, 2)
    End If
    strValue = frmData.tabData(0).GetFieldValue(llLine, "OFBL")
    strValue = Trim(strValue)
    If Trim(strValue) <> "" Then
        MyDate = CedaFullDateToVb(strValue)
        tabTab(0).SetFieldValues 0, "OFBL", Format(MyDate, "hh:mm") + "/" + Mid(strValue, 7, 2)
    End If
    
    strValue = frmData.tabData(0).GetFieldValue(llLine, "TTYP")
    tabTab(0).SetFieldValues 0, "NA_D", strValue
    strValue = frmData.tabData(0).GetFieldValue(llLine, "VIA3")
    tabTab(0).SetFieldValues 0, "VIA_D", strValue
    strValue = frmData.tabData(0).GetFieldValue(llLine, "DES3")
    tabTab(0).SetFieldValues 0, "DES", strValue
    strValue = frmData.tabData(0).GetFieldValue(llLine, "PSTD")
    tabTab(0).SetFieldValues 0, "POS_D", strValue
    strValue = frmData.tabData(0).GetFieldValue(llLine, "TGD1")
    tabTab(0).SetFieldValues 0, "TGD_D", strValue
    strValue = frmData.tabData(0).GetFieldValue(llLine, "GTD1")
    tabTab(0).SetFieldValues 0, "GAT_D", strValue
    strValue = frmData.tabData(0).GetFieldValue(llLine, "REGN")
    tabTab(0).SetFieldValues 0, "REGN_D", strValue
    strValue = frmData.tabData(0).GetFieldValue(llLine, "URNO")
    tabTab(0).SetFieldValues 0, "URNO_D", strValue
    
    tabTab(0).Refresh
End Sub

Private Sub FillInConnectingFlightInfo(ByVal strArrUrno As String, ByVal llAftLine As Long, ByVal llCurrLine As Long)
    Dim strVIAL As String
    'Dim strRawFlnoCopy As String
    'Dim strLoaApc3 As String
    Dim viaLen As Integer
    Dim tmpVia As String
    Dim strDepUrno As String
    
    'strRawFlnoCopy = frmData.tabData(0).GetFieldValue(llAftLine, "FLNO")
    
    strDepUrno = frmData.tabData(0).GetFieldValue(llAftLine, "URNO")
    strVIAL = frmData.tabData(0).GetFieldValue(llAftLine, "VIAL")
    tabTab(1).SetFieldValues llCurrLine, "ACT", frmData.tabData(0).GetFieldValue(llAftLine, "ACT3")
    'tabTab(1).SetFieldValues llCurrLine, "FLNO", strRawFlnoCopy
    tabTab(1).SetFieldValues llCurrLine, "TGD_D", frmData.tabData(0).GetFieldValue(llAftLine, "TGD1")
    tabTab(1).SetFieldValues llCurrLine, "GATE", frmData.tabData(0).GetFieldValue(llAftLine, "GTD1")
    tabTab(1).SetFieldValues llCurrLine, "STD", frmData.tabData(0).GetFieldValue(llAftLine, "STOD")
    tabTab(1).SetFieldValues llCurrLine, "EST", frmData.tabData(0).GetFieldValue(llAftLine, "ETDI")
    tabTab(1).SetFieldValues llCurrLine, "ATD", frmData.tabData(0).GetFieldValue(llAftLine, "AIRB")
    'kkh 06/02/2009 APC3
    'tabTab(1).SetFieldValues llCurrLine, "DES", strLoaApc3
    tabTab(1).SetFieldValues llCurrLine, "NA", frmData.tabData(0).GetFieldValue(llAftLine, "TTYP")
    'kkh 17/02/2009
    If strVIAL <> "" Then
        viaLen = Len(strVIAL)
        If viaLen < 120 Then
            tmpVia = Mid(strVIAL, 2, 3) ' & "/" & lblDep(14).Tag
        End If
        If viaLen > 120 And viaLen < 240 Then
            tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3)
        End If
        If viaLen > 240 And viaLen < 360 Then
            tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3)
        End If
        If viaLen > 360 And viaLen < 480 Then
            tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3) & "/" & Mid(strVIAL, 362, 3)
        End If
        If viaLen > 480 And viaLen < 600 Then
            tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3) & "/" & Mid(strVIAL, 362, 3) & "/" & Mid(strVIAL, 482, 3)
        End If
        If viaLen > 600 And viaLen < 720 Then
            tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3) & "/" & Mid(strVIAL, 362, 3) & "/" & Mid(strVIAL, 482, 3) & "/" & Mid(strVIAL, 602, 3)
        End If
        If viaLen > 720 And viaLen < 800 Then
            tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3) & "/" & Mid(strVIAL, 362, 3) & "/" & Mid(strVIAL, 482, 3) & "/" & Mid(strVIAL, 602, 3) & "/" & Mid(strVIAL, 722, 3)
        End If
        If viaLen > 800 And viaLen < 920 Then
            tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3) & "/" & Mid(strVIAL, 362, 3) & "/" & Mid(strVIAL, 482, 3) & "/" & Mid(strVIAL, 602, 3) & "/" & Mid(strVIAL, 722, 3) & "/" & Mid(strVIAL, 902, 3)
        End If
        If viaLen > 920 And viaLen < 1040 Then
            tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3) & "/" & Mid(strVIAL, 362, 3) & "/" & Mid(strVIAL, 482, 3) & "/" & Mid(strVIAL, 602, 3) & "/" & Mid(strVIAL, 722, 3) & "/" & Mid(strVIAL, 902, 3) & "/" & Mid(strVIAL, 1022, 3)
        End If
        tmpVia = tmpVia & "/" & frmData.tabData(0).GetFieldValue(llAftLine, "DES3")
        tabTab(1).SetFieldValues llCurrLine, "VIAnDES", tmpVia
    Else
        tabTab(1).SetFieldValues llCurrLine, "VIAnDES", frmData.tabData(0).GetFieldValue(llAftLine, "DES3")
    End If
        
    Call FillInConnectionTimeInfo(strArrUrno, strDepUrno, llAftLine, llCurrLine)
    
    tabTab(1).Refresh
End Sub

Private Sub FillInConnectionTimeInfo(ByVal strArrUrno As String, ByVal strDepUrno As String, ByVal llAftLine As Long, ByVal llCurrLine As Long)
    Dim strSRLText As String
    
    If frmData.tabData(0).GetFieldValue(llAftLine, "OFBL") = "" Then
        strSRLText = GetTransferTime(strArrUrno, strDepUrno)
    Else
        strSRLText = ""
    End If
    tabTab(1).SetFieldValues llCurrLine, "TRANSFER", strSRLText
    
    If strSRLText = "Waiting" Then
        'tabTab(1).SetFieldValues llCurrLine, "Hold", "H"
        MarkLineAsHold llCurrLine, strDepUrno
    End If
    'KKH PRF conflict colour appear occasionally
    CheckConnectionTimes strArrUrno, strDepUrno, llCurrLine, llAftLine, , False
End Sub
