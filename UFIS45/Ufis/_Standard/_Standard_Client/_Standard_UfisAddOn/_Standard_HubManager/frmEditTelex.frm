VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmEditTelex 
   Caption         =   "Telex Editor"
   ClientHeight    =   7680
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12660
   Icon            =   "frmEditTelex.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7680
   ScaleWidth      =   12660
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtTplDesc 
      Height          =   285
      Left            =   2640
      TabIndex        =   65
      Top             =   6960
      Visible         =   0   'False
      Width           =   3500
   End
   Begin VB.TextBox txtTplName 
      Height          =   285
      Left            =   0
      TabIndex        =   64
      Top             =   6960
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.CheckBox chkTemplate 
      Caption         =   "Link"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   6600
      Style           =   1  'Graphical
      TabIndex        =   63
      ToolTipText     =   "Link (save) telex template with address group"
      Top             =   2520
      Visible         =   0   'False
      Width           =   900
   End
   Begin VB.CheckBox chkTemplate 
      Caption         =   "Flights"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   7440
      Style           =   1  'Graphical
      TabIndex        =   62
      Top             =   2520
      Visible         =   0   'False
      Width           =   900
   End
   Begin VB.TextBox NewCallCode 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   8160
      TabIndex        =   60
      Top             =   6240
      Visible         =   0   'False
      Width           =   1905
   End
   Begin VB.Frame fraFlightData 
      Caption         =   "Flight Data"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1515
      Left            =   10200
      TabIndex        =   50
      Top             =   1800
      Visible         =   0   'False
      Width           =   2385
      Begin VB.PictureBox FltInputPanel 
         AutoRedraw      =   -1  'True
         Height          =   855
         Left            =   60
         ScaleHeight     =   795
         ScaleWidth      =   2205
         TabIndex        =   54
         Top             =   570
         Width           =   2265
         Begin VB.PictureBox FltEditPanel 
            AutoRedraw      =   -1  'True
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   375
            Index           =   0
            Left            =   30
            ScaleHeight     =   375
            ScaleWidth      =   2145
            TabIndex        =   55
            TabStop         =   0   'False
            Top             =   30
            Width           =   2145
            Begin VB.CheckBox chkFltCheck 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   780
               Style           =   1  'Graphical
               TabIndex        =   58
               Top             =   30
               Visible         =   0   'False
               Width           =   180
            End
            Begin VB.TextBox txtFltInput 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   1110
               MaxLength       =   7
               TabIndex        =   57
               Text            =   "12MAY04"
               Top             =   30
               Width           =   975
            End
            Begin VB.CheckBox chkFltTick 
               Height          =   210
               Index           =   0
               Left            =   60
               TabIndex        =   56
               Top             =   75
               Width           =   195
            End
            Begin VB.Label lblFltField 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "A/C-Chg"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   195
               Index           =   0
               Left            =   300
               TabIndex        =   59
               Top             =   75
               Width           =   735
            End
         End
      End
      Begin VB.CheckBox chkFltData 
         Caption         =   "Telex"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1305
         Style           =   1  'Graphical
         TabIndex        =   53
         Top             =   240
         Width           =   600
      End
      Begin VB.CheckBox chkFltData 
         Caption         =   "Save"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   705
         Style           =   1  'Graphical
         TabIndex        =   52
         Top             =   240
         Width           =   585
      End
      Begin VB.CheckBox chkFltData 
         Caption         =   "Read"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   51
         Top             =   240
         Width           =   600
      End
   End
   Begin VB.TextBox NewSendUrno 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6120
      TabIndex        =   49
      Top             =   6240
      Visible         =   0   'False
      Width           =   1905
   End
   Begin VB.Frame fraPreview 
      Caption         =   "Telex Text Preview"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2415
      Left            =   8040
      TabIndex        =   47
      Top             =   1920
      Visible         =   0   'False
      Width           =   2715
      Begin VB.TextBox txtPreview 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2025
         Left            =   90
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   48
         Top             =   300
         Width           =   2055
      End
   End
   Begin VB.Frame fraDestAddr 
      Caption         =   "Destination Addresses"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1905
      Left            =   3840
      TabIndex        =   40
      Top             =   1800
      Width           =   6435
      Begin VB.TextBox txtDestAddr 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1245
         Left            =   90
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   46
         Top             =   570
         Width           =   8415
      End
      Begin VB.CheckBox chkTlxAddr 
         Caption         =   "Reload"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1890
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   240
         Width           =   900
      End
      Begin VB.CheckBox chkTlxAddr 
         Caption         =   "Merge"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   44
         Top             =   240
         Width           =   900
      End
      Begin VB.CheckBox chkTlxAddr 
         Caption         =   "Arrange"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   990
         Style           =   1  'Graphical
         TabIndex        =   43
         Top             =   240
         Width           =   900
      End
      Begin VB.CheckBox chkTemplate 
         Caption         =   "Combine"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   2760
         Style           =   1  'Graphical
         TabIndex        =   42
         ToolTipText     =   "Combine (save) telex template and telex addresses"
         Top             =   240
         Visible         =   0   'False
         Width           =   900
      End
      Begin VB.CheckBox chkTemplate 
         Caption         =   "Link"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   2790
         Style           =   1  'Graphical
         TabIndex        =   41
         ToolTipText     =   "Link (save) telex template with address group"
         Top             =   240
         Visible         =   0   'False
         Width           =   900
      End
   End
   Begin VB.Frame fraTelexText 
      Caption         =   "Telex Text"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Left            =   3840
      TabIndex        =   38
      Top             =   3840
      Width           =   6285
      Begin VB.TextBox txtTelexText 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1875
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   39
         Top             =   240
         Width           =   6165
      End
   End
   Begin VB.Frame fraAddress 
      Caption         =   "Address Pool"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4875
      Left            =   0
      TabIndex        =   21
      Top             =   1800
      Width           =   3795
      Begin VB.Frame fraChkAddr 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   90
         TabIndex        =   31
         Top             =   555
         Width           =   3645
         Begin VB.CheckBox chkMvtAddr 
            Caption         =   "Airl."
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   37
            Top             =   0
            Width           =   600
         End
         Begin VB.CheckBox chkMvtAddr 
            Caption         =   "Airp."
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   615
            Style           =   1  'Graphical
            TabIndex        =   36
            Top             =   0
            Width           =   585
         End
         Begin VB.CheckBox chkMvtAddr 
            Caption         =   "Clear"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   2
            Left            =   1215
            Style           =   1  'Graphical
            TabIndex        =   35
            Top             =   0
            Width           =   600
         End
         Begin VB.CheckBox chkMvtAddr 
            Caption         =   "Ins"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   3
            Left            =   1830
            Style           =   1  'Graphical
            TabIndex        =   34
            Top             =   0
            Width           =   585
         End
         Begin VB.CheckBox chkMvtAddr 
            Caption         =   "Upd"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   4
            Left            =   2430
            Style           =   1  'Graphical
            TabIndex        =   33
            Top             =   0
            Width           =   585
         End
         Begin VB.CheckBox chkMvtAddr 
            Caption         =   "Del"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   5
            Left            =   3030
            Style           =   1  'Graphical
            TabIndex        =   32
            Top             =   0
            Width           =   585
         End
      End
      Begin VB.CheckBox chkSelAddr 
         Caption         =   "MVT Tlx"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   240
         Width           =   950
      End
      Begin VB.CheckBox chkSelAddr 
         Caption         =   "Groups"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1040
         Style           =   1  'Graphical
         TabIndex        =   26
         ToolTipText     =   "View / Edit telex address groups"
         Top             =   240
         Width           =   950
      End
      Begin VB.CheckBox chkSelAddr 
         Caption         =   "Templates"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   2000
         Style           =   1  'Graphical
         TabIndex        =   25
         ToolTipText     =   "View / Edit selected telex template category"
         Top             =   240
         Width           =   950
      End
      Begin VB.CheckBox chkTplSel 
         Caption         =   "T"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   2940
         Style           =   1  'Graphical
         TabIndex        =   22
         ToolTipText     =   "View / Edit telex template"
         Top             =   240
         Width           =   250
      End
      Begin TABLib.TAB TemplateTab 
         Height          =   1215
         Left            =   90
         TabIndex        =   23
         Top             =   3360
         Visible         =   0   'False
         Width           =   3615
         _Version        =   65536
         _ExtentX        =   6376
         _ExtentY        =   2143
         _StockProps     =   64
         Enabled         =   0   'False
      End
      Begin TABLib.TAB GroupTab 
         Height          =   1815
         Left            =   90
         TabIndex        =   24
         Top             =   2760
         Width           =   3615
         _Version        =   65536
         _ExtentX        =   6376
         _ExtentY        =   3201
         _StockProps     =   64
      End
      Begin TABLib.TAB AddressTab 
         Height          =   2415
         Left            =   90
         TabIndex        =   28
         Top             =   2130
         Width           =   3615
         _Version        =   65536
         _ExtentX        =   6376
         _ExtentY        =   4260
         _StockProps     =   64
         Enabled         =   0   'False
      End
      Begin TABLib.TAB ApcTab 
         Height          =   585
         Left            =   90
         TabIndex        =   29
         Top             =   1500
         Width           =   3615
         _Version        =   65536
         _ExtentX        =   6376
         _ExtentY        =   1032
         _StockProps     =   64
         Enabled         =   0   'False
      End
      Begin TABLib.TAB AlcTab 
         Height          =   585
         Left            =   90
         TabIndex        =   30
         Top             =   870
         Width           =   3615
         _Version        =   65536
         _ExtentX        =   6376
         _ExtentY        =   1032
         _StockProps     =   64
         Enabled         =   0   'False
      End
   End
   Begin VB.CheckBox chkTest 
      Caption         =   "Tool"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   9240
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   0
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox chkClose 
      Caption         =   "&Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   7680
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox chkSend 
      Caption         =   "Sen&d"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6840
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox chkTransmit 
      Caption         =   "Transmit"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6000
      Style           =   1  'Graphical
      TabIndex        =   16
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox chkCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5160
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox chkOutFormat 
      Caption         =   "Format"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4320
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox chkPreView 
      Caption         =   "Preview"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3480
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox chkUcase 
      Caption         =   "UC"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3000
      Style           =   1  'Graphical
      TabIndex        =   12
      ToolTipText     =   "Convert text to upper case"
      Top             =   0
      Width           =   420
   End
   Begin VB.CheckBox chkLcase 
      Caption         =   "LC"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2520
      Style           =   1  'Graphical
      TabIndex        =   11
      ToolTipText     =   "Allow lower case typing"
      Top             =   0
      Width           =   420
   End
   Begin VB.CheckBox chkCopy 
      Caption         =   "Copy"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1680
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox chkReset 
      Caption         =   "Reset"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   840
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox OnTop 
      Caption         =   "On Top"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   0
      Width           =   855
   End
   Begin VB.Frame fraDblSign 
      Caption         =   "Double Sign [+]"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   9600
      TabIndex        =   6
      Top             =   480
      Width           =   2985
      Begin TABLib.TAB DblSignTab 
         Height          =   915
         Left            =   90
         TabIndex        =   7
         Top             =   240
         Width           =   2805
         _Version        =   65536
         _ExtentX        =   4948
         _ExtentY        =   1614
         _StockProps     =   64
      End
   End
   Begin VB.Frame fraTlxType 
      Caption         =   "Telex Type [+]"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   6840
      TabIndex        =   4
      Top             =   480
      Width           =   2685
      Begin TABLib.TAB TlxTypeTab 
         Height          =   915
         Left            =   90
         TabIndex        =   5
         Top             =   240
         Width           =   2505
         _Version        =   65536
         _ExtentX        =   4419
         _ExtentY        =   1614
         _StockProps     =   64
      End
   End
   Begin VB.Frame fraTlxPrio 
      Caption         =   "Telex Priority [+]"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   3840
      TabIndex        =   2
      Top             =   480
      Width           =   2985
      Begin TABLib.TAB TlxPrioTab 
         Height          =   915
         Left            =   90
         TabIndex        =   3
         Top             =   240
         Width           =   2805
         _Version        =   65536
         _ExtentX        =   4948
         _ExtentY        =   1614
         _StockProps     =   64
      End
   End
   Begin VB.Frame fraOrigAddr 
      Caption         =   "Telex Originator [+]"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   0
      TabIndex        =   0
      Top             =   480
      Width           =   3795
      Begin TABLib.TAB TlxOrigTab 
         Height          =   915
         Left            =   90
         TabIndex        =   1
         Top             =   240
         Width           =   3615
         _Version        =   65536
         _ExtentX        =   6376
         _ExtentY        =   1614
         _StockProps     =   64
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   61
      Top             =   7395
      Width           =   12660
      _ExtentX        =   22331
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   19235
         EndProperty
      EndProperty
   End
   Begin VB.Label lblTestCase 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Test Case"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   10200
      TabIndex        =   20
      Top             =   0
      Visible         =   0   'False
      Width           =   2235
   End
End
Attribute VB_Name = "frmEditTelex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim m_ArrFlno As String
Dim m_DepFlights As Collection

Private Sub chkClose_Click()
    If chkClose.Value = 1 Then
        MeIsVisible = False
        Me.Hide
        chkClose.Value = 0
    End If
End Sub

Private Sub chkMvtAddr_Click(Index As Integer)
Dim tmpLineNo As Integer
    Dim RetVal As Integer
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActDatLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    
    If chkMvtAddr(Index).Value = 1 Then
        chkMvtAddr(Index).backColor = LightGreen
        Select Case Index
            Case 0  'Airline
                SetFirstFreeCell AlcTab
                chkMvtAddr(Index).Value = 0
            Case 1  'Airport
                SetFirstFreeCell ApcTab
                chkMvtAddr(Index).Value = 0
            Case 2  'Clear
                AddressTab.ResetContent
                AddressTab.Refresh
                AlcTab.ResetContent
                AlcTab.InsertTextLine ",,,,,,", False
                AlcTab.Refresh
                ApcTab.ResetContent
                ApcTab.InsertTextLine ",,,,,,", False
                ApcTab.Refresh
                InitDblSignList ""
                StatusBar1.Panels(2).Text = ""
                chkMvtAddr(Index).Value = 0
            Case 3  ' Insert
                If OnTop.Value = 1 Then
                    OnTop.Value = 0
                End If
                If chkSelAddr(0).Tag = 1 Then
                    frmEditGroups.txtGrpName.Text = ""
                    frmEditGroups.txtGrpDesc.Text = ""
                    frmEditGroups.txtGrpAddr.Text = txtDestAddr.Text
                    tmpLineNo = TlxOrigTab.GetCurrentSelected
                    If tmpLineNo >= 0 Then
                        frmEditGroups.txtGrpOwner.Text = TlxOrigTab.GetColumnValue(tmpLineNo, 0)
                    Else
                        frmEditGroups.txtGrpOwner.Text = ""
                    End If
                    frmEditGroups.txtGrpName.Tag = "INS"
                    frmEditGroups.Visible = True
                Else
                    If chkSelAddr(0).Tag = 2 Then
                        EditTemplates.txtTplName.Text = ""
                        EditTemplates.txtTplDesc.Text = ""
                        EditTemplates.txtTplText.Text = ""
                        tmpLineNo = TlxOrigTab.GetCurrentSelected
                        If tmpLineNo >= 0 Then
                            EditTemplates.txtTplOwner.Text = TlxOrigTab.GetColumnValue(tmpLineNo, 0)
                        Else
                            EditTemplates.txtTplOwner.Text = ""
                        End If
                        EditTemplates.txtTplName.Tag = "INS"
                        EditTemplates.Visible = True
                    End If
                End If
                chkMvtAddr(Index).Value = 0
            Case 4  ' Update
                If OnTop.Value = 1 Then
                    OnTop.Value = 0
                End If
                If chkSelAddr(0).Tag = 1 Then
                    tmpLineNo = GroupTab.GetCurrentSelected
                    If tmpLineNo >= 0 Then
                        frmEditGroups.txtGrpName.Text = GroupTab.GetColumnValue(tmpLineNo, 0)
                        frmEditGroups.txtGrpDesc.Text = GroupTab.GetColumnValue(tmpLineNo, 1)
                        frmEditGroups.txtGrpAddr.Text = CleanString(GroupTab.GetColumnValue(tmpLineNo, 2), FOR_CLIENT, False)
                        frmEditGroups.txtGrpOwner.Text = GroupTab.GetColumnValue(tmpLineNo, 4)
                        frmEditGroups.txtGrpName.Tag = GroupTab.GetColumnValue(tmpLineNo, 3)
                        frmEditGroups.Visible = True
                    Else
                        MyMsgBox.CallAskUser 0, 0, 0, "Edit Groups", "No line sected", "hand", "", UserAnswer
                    End If
                Else
                    If chkSelAddr(0).Tag = 2 Then
                        tmpLineNo = TemplateTab.GetCurrentSelected
                        If tmpLineNo >= 0 Then
                            EditTemplates.txtTplName.Text = TemplateTab.GetColumnValue(tmpLineNo, 1)
                            EditTemplates.txtTplDesc.Text = TemplateTab.GetColumnValue(tmpLineNo, 2)
                            EditTemplates.txtTplText.Text = CleanString(TemplateTab.GetColumnValue(tmpLineNo, 3), FOR_CLIENT, False)
                            If Len(TemplateTab.GetColumnValue(tmpLineNo, 7)) > 0 Then
                                EditTemplates.txtTplText.Text = EditTemplates.txtTplText.Text & CleanString(TemplateTab.GetColumnValue(tmpLineNo, 7), FOR_CLIENT, False)
                            End If
                            EditTemplates.txtTplOwner.Text = TemplateTab.GetColumnValue(tmpLineNo, 8)
                            EditTemplates.txtTplName.Tag = TemplateTab.GetColumnValue(tmpLineNo, 4)
                            EditTemplates.txtTplType.Tag = TemplateTab.GetColumnValue(tmpLineNo, 6)
                            EditTemplates.Visible = True
                        Else
                            MyMsgBox.CallAskUser 0, 0, 0, "Edit Templates", "No line sected", "hand", "", UserAnswer
                        End If
                    End If
                End If
                chkMvtAddr(Index).Value = 0
            Case 5  ' Delete
                If chkSelAddr(0).Tag = 1 Then
                    tmpLineNo = GroupTab.GetCurrentSelected
                    If tmpLineNo >= 0 Then
                        If MyMsgBox.CallAskUser(0, 0, 0, "Edit Groups", "Do you really want to delete selected record ?", "ask", "Yes,No;F", UserAnswer) = 1 Then
                            If tmpLineNo = GroupTab.GetLineCount - 1 Then
                                If tmpLineNo <> 0 Then
                                    chkSelAddr(1).Tag = GroupTab.GetColumnValue(tmpLineNo - 1, 3)
                                Else
                                    chkSelAddr(1).Tag = ""
                                End If
                            Else
                                chkSelAddr(1).Tag = GroupTab.GetColumnValue(tmpLineNo + 1, 3)
                            End If
                            ActResult = ""
                            ActCmd = "DRT"
                            ActTable = "TTPTAB"
                            ActFldLst = ""
                            ActCondition = "WHERE URNO = " & GroupTab.GetColumnValue(tmpLineNo, 3)
                            ActOrder = ""
                            ActDatLst = ""
                            'RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                            RetVal = frmData.aUfis.CallServer(ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, "239")
                            LoadGroupData
                        End If
                    Else
                        MyMsgBox.CallAskUser 0, 0, 0, "Edit Groups", "No line sected", "hand", "", UserAnswer
                    End If
                Else
                    If chkSelAddr(0).Tag = 2 Then
                        tmpLineNo = TemplateTab.GetCurrentSelected
                        If tmpLineNo >= 0 Then
                            If MyMsgBox.CallAskUser(0, 0, 0, "Edit Templates", "Do you really want to delete selected record ?", "ask", "Yes,No;F", UserAnswer) = 1 Then
                                If tmpLineNo = TemplateTab.GetLineCount - 1 Then
                                    If tmpLineNo <> 0 Then
                                        chkSelAddr(2).Tag = TemplateTab.GetColumnValue(tmpLineNo - 1, 4)
                                    Else
                                        chkSelAddr(2).Tag = ""
                                    End If
                                Else
                                    chkSelAddr(2).Tag = TemplateTab.GetColumnValue(tmpLineNo + 1, 4)
                                End If
                                ActResult = ""
                                ActCmd = "DRT"
                                ActTable = "TTPTAB"
                                ActFldLst = ""
                                ActCondition = "WHERE URNO = " & TemplateTab.GetColumnValue(tmpLineNo, 4)
                                ActOrder = ""
                                ActDatLst = ""
                                'RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                                LoadTemplateData
                            End If
                        Else
                            MyMsgBox.CallAskUser 0, 0, 0, "Edit Templates", "No line sected", "hand", "", UserAnswer
                        End If
                    End If
                End If
                chkMvtAddr(Index).Value = 0
            Case Else
        End Select
    Else
        chkMvtAddr(Index).backColor = vbButtonFace
    End If
End Sub

Private Sub chkPreView_Click()
    If chkPreView.Value = 1 Then
        chkPreView.backColor = LightGreen
        InitTelexPreview False
        fraPreview.Visible = True
        fraTelexText.Visible = False
        chkTemplate(2).Visible = False
        txtTplName.Visible = False
        txtTplDesc.Visible = False
        txtPreview.SetFocus
    Else
        fraTelexText.Visible = True
        fraPreview.Visible = False
        If chkSelAddr(1).Value = 1 Or chkSelAddr(2).Value = 1 Then
            chkTemplate(2).Visible = True
            If chkTemplate(2).Enabled = True Then
                txtTplName.Visible = True
                txtTplDesc.Visible = True
            End If
        End If
        chkPreView.backColor = vbButtonFace
    End If
End Sub
Private Sub InitTelexPreview(WithFooter As Boolean)
    Select Case MySitaOutType
        Case "SITA_NODE"
            InitTpfNodePreview WithFooter
        Case "SITA_FILE"
            If chkOutFormat.Value = 1 Then
                InitSitaFilePreview WithFooter
            Else
                InitTpfNodePreview WithFooter
            End If
        Case Else
    End Select
End Sub
Private Sub InitSitaFilePreview(WithFooter As Boolean)
    Dim LineNo As Long
    Dim CurLine As Long
    Dim TabAddrList As String
    Dim NewText As String
    Dim tmpData As String
    Dim tmpLine As String
    Dim tmpUrno As String
    Dim tmpDateStamp As String
    Dim iCount As Integer
    Dim tmpTime As String
    
    txtPreview.Text = ""
    NewText = ""
    LineNo = TlxPrioTab.GetCurrentSelected
    tmpData = Trim(TlxPrioTab.GetColumnValue(LineNo, 0))
    If (tmpData <> "") And (tmpData <> "--") Then
        NewText = NewText & "=PRIORITY" & vbNewLine
        NewText = NewText & tmpData & " " & vbNewLine
    End If
    LineNo = DblSignTab.GetCurrentSelected
    tmpData = Trim(DblSignTab.GetColumnValue(LineNo, 0))
    If (tmpData <> "") And (tmpData <> "--") Then
        NewText = NewText & "=DBLSIG" & vbNewLine
        NewText = NewText & tmpData & " " & vbNewLine
    End If
    TabAddrList = GetCleanAddrList(txtDestAddr.Text, 8, ",")
    If TabAddrList <> "" Then
        NewText = NewText & "=DESTINATION TYPE B" & vbNewLine
        iCount = 1
        tmpData = GetItem(TabAddrList, iCount, ",")
        While tmpData <> ""
            tmpData = CleanTrimString(tmpData)
            If tmpData <> "" Then
                NewText = NewText & "STX," & tmpData & vbNewLine
            End If
            iCount = iCount + 1
            tmpData = GetItem(TabAddrList, iCount, ",")
        Wend
    End If
    If MySitaOutOrig Then
        CurLine = TlxOrigTab.GetCurrentSelected
        If CurLine >= 0 Then
            tmpData = Trim(TlxOrigTab.GetColumnValue(CurLine, 0))
            If tmpData <> "" Then
                NewText = NewText & "=ORIGIN" & vbNewLine
                NewText = NewText & tmpData & vbNewLine
            End If
        End If
    End If
    If MySitaOutMsgId Then
        tmpUrno = Trim(NewSendUrno.Text)
        If tmpUrno = "" Then
            UfisServer.UrnoPoolPrepare 1
            tmpUrno = Trim(UfisServer.UrnoTab.GetColumnValue(0, 0))
            If tmpUrno = "" Then tmpUrno = UfisServer.UrnoPoolGetNext
        End If
        NewSendUrno.Text = tmpUrno
        NewText = NewText & "=MSGID" & vbNewLine
        tmpTime = MySetUp.GetUtcServerTime
        tmpTime = Mid(tmpTime, 7, 6)
        NewText = NewText & tmpTime & vbNewLine
        'NewText = NewText & "UF" & tmpUrno & "IS" & vbNewLine
    End If
    If MySitaOutSmi Then
        CurLine = TlxTypeTab.GetCurrentSelected
        If CurLine >= 0 Then
            tmpData = Trim(TlxTypeTab.GetColumnValue(CurLine, 0))
            If tmpData = "FREE" Then tmpData = ""
            If tmpData <> "" Then
                NewText = NewText & "=SMI" & vbNewLine
                NewText = NewText & tmpData & vbNewLine
            End If
        End If
    End If
    tmpData = Trim(txtTelexText.Text)
    If tmpData <> "" Then
        NewText = NewText & "=TEXT" & vbNewLine
        NewText = NewText & tmpData & vbNewLine
    End If
    txtPreview.Text = NewText
End Sub
Private Function CleanTrimString(CurText As String) As String
    Dim UseText As String
    Dim tmpChr As String
    UseText = CurText
    UseText = Replace(UseText, Chr(1), "", 1, -1, vbBinaryCompare)
    UseText = Replace(UseText, Chr(3), "", 1, -1, vbBinaryCompare)
    tmpChr = Left(UseText, 1)
    While (tmpChr = vbLf) Or (tmpChr = vbCr)
        UseText = Mid(UseText, 2)
        tmpChr = Left(UseText, 1)
    Wend
    tmpChr = Right(UseText, 1)
    While (tmpChr = vbLf) Or (tmpChr = vbCr)
        UseText = Left(UseText, Len(UseText) - 1)
        tmpChr = Right(UseText, 1)
    Wend
    tmpChr = vbCr & vbCr
    UseText = Replace(UseText, tmpChr, vbCr, 1, -1, vbBinaryCompare)
    CleanTrimString = UseText
End Function
Private Sub InitTpfNodePreview(WithFooter As Boolean)
    Dim LineNo As Long
    Dim MaxLine As Long
    Dim NewText As String
    Dim tmpData As String
    Dim tmpLine As String
    Dim tmpUrno As String
    Dim tmpTtyp As String
    Dim tmpDateStamp As String
    Dim iCount As Integer
    
    Form_Resize
    
    txtPreview.Text = ""
    NewText = ""
    LineNo = TlxTypeTab.GetCurrentSelected
    tmpTtyp = TlxTypeTab.GetColumnValue(LineNo, 0)
    If tmpTtyp <> "SCOR" Then
        LineNo = TlxPrioTab.GetCurrentSelected
        tmpData = TlxPrioTab.GetColumnValue(LineNo, 0)
        NewText = NewText & tmpData & " "
        tmpLine = GetCleanAddrList(txtDestAddr.Text, 8, " ")
        NewText = NewText & tmpLine & vbNewLine
        
        LineNo = TlxOrigTab.GetCurrentSelected
        tmpData = TlxOrigTab.GetColumnValue(LineNo, 0)
        tmpLine = "." & tmpData & " "
        LineNo = DblSignTab.GetCurrentSelected
        tmpData = Trim(DblSignTab.GetColumnValue(LineNo, 0))
        'tmpData = MySetUp.GetUtcServerTime
        tmpData = GetUtcServerTime
        
        tmpLine = tmpLine & Mid(tmpData, 7, 6) & " "
        tmpDateStamp = Mid(tmpData, 7, 6)
        tmpUrno = Trim(NewSendUrno.Text)
        If tmpUrno = "" Then
            tmpUrno = frmData.GetNextUrno
        End If
        NewSendUrno.Text = tmpUrno
        'UfisServer.UrnoPoolPrepare 1
        'tmpUrno = Trim(UfisServer.UrnoTab.GetColumnValue(0, 0))
        tmpData = DecodeSsimDayFormat(Left(tmpData, 8), "CEDA", "SSIM2")
        tmpDateStamp = tmpDateStamp & " " & Mid(tmpData, 3, 5)
        chkPreView.Tag = tmpDateStamp & " PPK " & Right(tmpUrno, 3)
        NewText = NewText & tmpLine & vbNewLine
    End If
    NewText = NewText & txtTelexText.Text & vbNewLine
    txtPreview.Text = NewText
End Sub

Private Function GetCleanAddrList(CurAddrList As String, AddrPerLine As Long, UseSepa As String) As String
    Dim Result As String
    Dim TxtAddrList As String
    Dim tmpData As String
    Dim itm As Integer
    Dim cnt As Long
    TxtAddrList = CurAddrList
    TxtAddrList = Replace(TxtAddrList, Chr(10), ",", 1, -1, vbBinaryCompare)
    TxtAddrList = Replace(TxtAddrList, Chr(13), ",", 1, -1, vbBinaryCompare)
    TxtAddrList = Replace(TxtAddrList, Chr(9), ",", 1, -1, vbBinaryCompare)
    TxtAddrList = Replace(TxtAddrList, " ", ",", 1, -1, vbBinaryCompare)
    tmpData = TxtAddrList
    TxtAddrList = Replace(tmpData, ",,", ",", 1, -1, vbBinaryCompare)
    While tmpData <> TxtAddrList
        tmpData = TxtAddrList
        TxtAddrList = Replace(tmpData, ",,", ",", 1, -1, vbBinaryCompare)
    Wend
    While Left(TxtAddrList, 1) = ","
        TxtAddrList = Mid(TxtAddrList, 2)
    Wend
    While Right(TxtAddrList, 1) = ","
        TxtAddrList = Left(TxtAddrList, Len(TxtAddrList) - 1)
    Wend
    If AddrPerLine > 0 Then
        Result = ""
        cnt = 0
        itm = 1
        tmpData = GetItem(TxtAddrList, itm, ",")
        While tmpData <> ""
            Result = Result & tmpData & UseSepa
            cnt = cnt + 1
            If cnt = AddrPerLine Then
                Result = Result & vbNewLine
                cnt = 0
            End If
            itm = itm + 1
            tmpData = GetItem(TxtAddrList, itm, ",")
        Wend
    Else
        Result = TxtAddrList
        If UseSepa <> "," Then
            Result = Replace(Result, ",", UseSepa, 1, -1, vbBinaryCompare)
        End If
    End If
    GetCleanAddrList = Result
End Function
Public Function GetUtcServerTime() As String
    Dim UseServerTime
    'UseServerTime = CedaFullDateToVb(Format(Now, "yyyymmddhhmmss"))
    UseServerTime = Now
    'If ServerTimeType <> "UTC" Then
    UseServerTime = DateAdd("h", -UTCOffset, UseServerTime)
    GetUtcServerTime = Format(UseServerTime, "yyyymmddhhmmss")
End Function

Private Sub chkSelAddr_Click(Index As Integer)
  Dim tmpTag As String
    Dim LastIndex As Integer
    If chkSelAddr(Index).Value = 1 Then
        fraDestAddr.Caption = "Destination Addresses"
        tmpTag = chkSelAddr(0).Tag
        If tmpTag <> "" Then
            LastIndex = Val(tmpTag)
            If LastIndex <> Index Then chkSelAddr(LastIndex).Value = 0
        End If
        chkSelAddr(Index).backColor = LightGreen
        chkSelAddr(0).Tag = CStr(Index)
        Select Case Index
            Case 0  ' MVT Telexes
                fraChkAddr.Visible = True
                chkMvtAddr(0).Visible = True
                chkMvtAddr(1).Visible = True
                chkMvtAddr(2).Visible = True
                chkMvtAddr(3).Visible = False
                chkMvtAddr(4).Visible = False
                chkMvtAddr(5).Visible = False
                AlcTab.Visible = True
                ApcTab.Visible = True
                AddressTab.Visible = True
                GroupTab.Visible = False
                TemplateTab.Visible = False
                chkTlxAddr(0).Visible = True
                chkTlxAddr(1).Visible = True
                chkTlxAddr(2).Visible = True
                chkTemplate(0).Visible = False
                chkTemplate(1).Visible = False
                chkTemplate(2).Visible = False
                txtTplName.Visible = False
                txtTplDesc.Visible = False
                txtDestAddr.Enabled = True
            Case 1  ' Groups
                chkSelAddr(1).Tag = ""
                StatusBar1.Panels(2).Text = ""
                LoadGroupData
                fraChkAddr.Visible = True
                chkMvtAddr(0).Visible = False
                chkMvtAddr(1).Visible = False
                chkMvtAddr(2).Visible = False
                chkMvtAddr(3).Visible = True
                chkMvtAddr(4).Visible = True
                chkMvtAddr(5).Visible = True
                chkMvtAddr(3).Enabled = True
                chkMvtAddr(4).Enabled = True
                chkMvtAddr(5).Enabled = True
                AlcTab.Visible = False
                ApcTab.Visible = False
                AddressTab.Visible = False
                GroupTab.Visible = True
                TemplateTab.Visible = False
                chkTlxAddr(0).Visible = False
                chkTlxAddr(1).Visible = False
                chkTlxAddr(2).Visible = False
                chkTemplate(0).Enabled = False
                chkTemplate(0).Visible = True
                chkTemplate(0).Left = 90
                chkTemplate(1).Enabled = False
                chkTemplate(1).Visible = True
                chkTemplate(1).Left = 990
                chkTemplate(1).Top = 240
                chkTemplate(2).Enabled = False
                chkTemplate(2).Visible = True
                chkTemplate(2).Left = 5850
                chkTemplate(2).Top = 2010
                txtTplName.Visible = False
                txtTplDesc.Visible = False
                txtDestAddr.Enabled = True
            Case 2  ' Templates
                StatusBar1.Panels(2).Text = ""
                LoadGroupData
                If chkTplSel(0).Value = 0 And chkTplSel(1).Value = 0 And _
                   chkTplSel(2).Value = 0 Then
                    chkTplSel(0).Value = 1
                End If
                LoadTemplateData
                fraChkAddr.Visible = True
                chkMvtAddr(0).Visible = False
                chkMvtAddr(1).Visible = False
                chkMvtAddr(2).Visible = False
                chkMvtAddr(3).Visible = True
                chkMvtAddr(4).Visible = True
                chkMvtAddr(5).Visible = True
                If chkTplSel(0).Value = 1 Then
                    chkMvtAddr(3).Enabled = True
                    chkMvtAddr(4).Enabled = True
                Else
                    chkMvtAddr(3).Enabled = False
                    chkMvtAddr(4).Enabled = False
                End If
                AlcTab.Visible = False
                ApcTab.Visible = False
                AddressTab.Visible = False
                GroupTab.Visible = False
                TemplateTab.Visible = True
                chkTlxAddr(0).Visible = False
                chkTlxAddr(1).Visible = False
                chkTlxAddr(2).Visible = False
                If Len(txtDestAddr.Text) > 0 Then
                    If chkTplSel(0).Value = 1 Or chkTplSel(1).Value = 1 Then
                        chkTemplate(0).Enabled = True
                    Else
                        chkTemplate(0).Enabled = False
                    End If
                    If chkTplSel(0).Value = 1 Or chkTplSel(2).Value = 1 Then
                        chkTemplate(1).Enabled = True
                    Else
                        chkTemplate(1).Enabled = False
                    End If
                    chkTemplate(2).Enabled = True
                Else
                    chkTemplate(0).Enabled = False
                    chkTemplate(1).Enabled = False
                    chkTemplate(2).Enabled = False
                End If
                chkTemplate(0).Visible = True
                chkTemplate(0).Left = 90
                chkTemplate(1).Visible = True
                chkTemplate(1).Left = 990
                chkTemplate(1).Top = 240
                chkTemplate(2).Visible = True
                chkTemplate(2).Left = 5850
                chkTemplate(2).Top = 2010
                If Len(txtDestAddr.Text) > 0 Then
                    txtTplName.Visible = True
                    txtTplDesc.Visible = True
                    If chkTplSel(0).Value = 1 Then
                        txtTplName.Text = ""
                        txtTplDesc.Text = ""
                    End If
                Else
                    txtTplName.Visible = False
                    txtTplDesc.Visible = False
                End If
                txtTplName.Left = 6800
                txtTplName.Top = 2010
                txtTplDesc.Left = 9200
                txtTplDesc.Top = 2010
                If chkTplSel(2).Value = 1 Then
                    txtDestAddr.Enabled = False
                Else
                    txtDestAddr.Enabled = True
                End If
            Case Else
        End Select
    Else
        chkSelAddr(Index).backColor = vbButtonFace
'        Select Case Index
'            Case 0
                fraChkAddr.Visible = False
                AlcTab.Visible = False
                ApcTab.Visible = False
                AddressTab.Visible = False
                GroupTab.Visible = False
                TemplateTab.Visible = False
'            Case Else
'        End Select
    End If
End Sub

Private Sub chkSend_Click()
    Dim TelexText As String
    Dim ErrMsg As String
    Dim tmpResult As String
    Dim retCod As Integer
    Dim outData As String
    Dim outFields As String
    Dim outSqlKey As String
    Dim tmpTxt1 As String
    Dim tmpTxt2 As String
    Dim tmpTime As String
    Dim tmpTtyp As String
    Dim tmpFlnu As String
    Dim tmpUrno As String
    Dim tmpData As String
    Dim tmpCallCode As String
    Dim TlxRec As String
    Dim tmpSere As String
    Dim LineNo As Long
    Dim SendIt As Boolean
    Dim NewUrno As String
    Dim i As Integer
    Dim TlxTxtFldSize As Integer
    TlxTxtFldSize = 4000
    
    If chkSend.Value = 1 Then
        chkSend.backColor = LightGreen
        chkTransmit.Value = 0
        If MySitaOutType = "SITA_FILE" Then
            chkOutFormat.Value = 1
        Else
            chkPreView.Value = 1
        End If
        SendIt = True
        ErrMsg = ""
        'InitTpfNodePreview False
        If MySitaOutType = "SITA_NODE" Then
            TelexText = Chr(13) & Chr(10) & Chr(1) & Trim(txtPreview.Text) & Chr(13) & Chr(10) & Chr(3)
        Else
            TelexText = Trim(txtPreview.Text)
        End If
'        If MySitaOutType = "SITA_FILE" Then
'            If InStr(TelexText, "=TEXT") = 0 Then
'                ErrMsg = ErrMsg & "Missing keyword: '=TEXT'" & vbNewLine
'                SendIt = False
'            End If
'            If InStr(TelexText, "=DESTINATION TYPE B") = 0 Then
'                ErrMsg = ErrMsg & "Missing keyword: '=DESTINATION TYPE B'" & vbNewLine
'                SendIt = False
'            End If
'        End If
        If SendIt Then
            TelexText = CleanString(TelexText, CLIENT_TO_SERVER, False)
'            If Len(TelexText) > 2000 Then
'                tmpTxt1 = left(TelexText, 2000)
'                tmpTxt2 = Mid(TelexText, 2001, 2000)
'            Else
'                tmpTxt1 = TelexText
'                tmpTxt2 = " "
'            End If
            If Len(TelexText) <= TlxTxtFldSize Then
                tmpTxt1 = TelexText
                tmpTxt2 = " "
            Else
                i = TlxTxtFldSize
                While i > 1 And Mid(TelexText, i, 1) = " "
                    i = i - 1
                Wend
                tmpTxt1 = Mid(TelexText, 1, i)
                tmpTxt2 = Mid(TelexText, i + 1)
            End If
            LineNo = TlxTypeTab.GetCurrentSelected
            tmpTtyp = TlxTypeTab.GetColumnValue(LineNo, 0)
            If tmpTtyp = "FREE" Then tmpTtyp = " "
            outFields = "URNO,SERE,STAT,WSTA,CDAT,TIME,TTYP,TXT1,TXT2"
'            tmpUrno = Trim(NewSendUrno.Text)
'            tmpData = Trim(UfisServer.UrnoTab.GetColumnValue(0, 0))
'            If (tmpUrno = "") Or (tmpUrno = tmpData) Then
'                tmpUrno = UfisServer.UrnoPoolGetNext
'            End If
            
            tmpUrno = Trim(NewSendUrno.Text)
            If tmpUrno = "" Then
                tmpUrno = frmData.GetNextUrno
            End If
            NewUrno = tmpUrno
            tmpCallCode = Trim(NewCallCode.Text)
            If tmpCallCode = "" Then tmpCallCode = "M"
            NewCallCode.Text = tmpCallCode
            outData = tmpUrno & ",S,S"
            outData = outData & "," & tmpCallCode
            tmpTime = GetUtcServerTime
            outData = outData & "," & tmpTime
            outData = outData & "," & tmpTime
            outData = outData & "," & tmpTtyp
            outData = outData & "," & tmpTxt1
            outData = outData & "," & tmpTxt2
            'retCod = UfisServer.CallCeda(tmpResult, "IRT", "TLXTAB", outFields, outData, "          ", "", 0, True, False)

            frmData.SetServerParameters
            frmData.aUfis.UserName = strAppl
                               
            If frmData.aUfis.CallServer("IRT", "TLXTAB", outFields, outData, "  ", "239") <> 0 Then
                MsgBox "Send Telex Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
            Else
                '----
                'igu on 27/06/2011
                'copy to special requirement table if config says so
                If pblnCopyToSpecialRequirements Then
                    If Not CopyToSpecialRequirementsTable(txtTelexText.Text, m_ArrFlno, m_DepFlights) Then
                        MsgBox "Failed to copy to special requirement table!!"
                    End If
                End If
                '----
                
                MyMsgBox.CallAskUser 0, 0, 0, "Send Telex", "The telex is on the way now.", "export", "", UserAnswer
'                If Len(TlxUrnoFromRedirect) > 0 Then
'                    InsertFolderAndLink (NewUrno)
'                End If
'                Me.CurrentRecord.Text = ""
                NewSendUrno.Text = ""
                NewCallCode.Text = ""
            End If
            
'            TlxRec = Me.CurrentRecord.Text
'            If TlxRec <> "" Then
'                outFields = ""
'                tmpSere = GetFieldValue("SERE", TlxRec, DataPool.TlxTabFields)
'                Select Case tmpSere
'                    Case "C"
'                        outFields = "STAT,WSTA"
'                        outData = "A,S"
'                    Case Else
'                        outFields = "WSTA"
'                        outData = "R"
'                End Select
'                If outFields <> "" Then
'                    tmpUrno = GetFieldValue("URNO", TlxRec, DataPool.TlxTabFields)
'                    outSqlKey = "WHERE URNO=" & tmpUrno
'                    retCod = UfisServer.CallCeda(tmpResult, "URT", "TLXTAB", outFields, outData, outSqlKey, "", 0, True, False)
'                End If
'            End If
        Else
            If TelexText = "" Then ErrMsg = "Can't send empty telexes!"
            MyMsgBox.CallAskUser 0, 0, 0, "Send Telex", ErrMsg, "stop", "", UserAnswer
        End If
        chkSend.Value = 0
        TlxUrnoFromRedirect = ""
    Else
        If MySitaOutType = "SITA_FILE" Then
            chkOutFormat.Value = 0
        Else
            chkPreView.Value = 0
        End If
        chkSend.backColor = vbButtonFace
    End If
End Sub
Private Sub Form_Load()
    TlxTypeTab.ResetContent
    TlxTypeTab.FontName = "Courier New"
    TlxTypeTab.HeaderFontSize = 14
    TlxTypeTab.FontSize = 14
    TlxTypeTab.LineHeight = 14
    TlxTypeTab.SetTabFontBold True
    TlxTypeTab.HeaderString = "Type,ST,Message Category               "
    TlxTypeTab.HeaderLengthString = "100,100,100"
    TlxTypeTab.LifeStyle = True
    TlxTypeTab.AutoSizeByHeader = True
    TlxTypeTab.AutoSizeColumns
    
    TlxOrigTab.ResetContent
    TlxOrigTab.FontName = "Courier New"
    TlxOrigTab.HeaderFontSize = 14
    TlxOrigTab.FontSize = 14
    TlxOrigTab.LineHeight = 14
    TlxOrigTab.SetTabFontBold True
    TlxOrigTab.HeaderString = "Address,Remark                      "
    TlxOrigTab.HeaderLengthString = "100,100"
    TlxOrigTab.LifeStyle = True
    TlxOrigTab.AutoSizeByHeader = True
    TlxOrigTab.AutoSizeColumns
    
    TlxPrioTab.ResetContent
    TlxPrioTab.FontName = "Courier New"
    TlxPrioTab.HeaderFontSize = 14
    TlxPrioTab.FontSize = 14
    TlxPrioTab.LineHeight = 14
    TlxPrioTab.SetTabFontBold True
    TlxPrioTab.HeaderString = "Prio,Remark                         "
    TlxPrioTab.HeaderLengthString = "100,100"
    TlxPrioTab.LifeStyle = True
    TlxPrioTab.AutoSizeByHeader = True
    TlxPrioTab.AutoSizeColumns
    
    DblSignTab.ResetContent
    DblSignTab.FontName = "Courier New"
    DblSignTab.HeaderFontSize = 14
    DblSignTab.FontSize = 14
    DblSignTab.LineHeight = 14
    DblSignTab.SetTabFontBold True
    DblSignTab.HeaderString = "LC,Remark                           "
    DblSignTab.HeaderLengthString = "100,100"
    DblSignTab.LifeStyle = True
    DblSignTab.AutoSizeByHeader = True
    DblSignTab.AutoSizeColumns
    
    InitOrigList
    InitTlxTypeList
    InitPrioList
    InitDblSignList ""
    
    AddressTab.ResetContent
    AddressTab.FontName = "Courier New"
    AddressTab.HeaderFontSize = 14
    AddressTab.FontSize = 14
    AddressTab.LineHeight = 14
    AddressTab.SetTabFontBold True
    AddressTab.HeaderString = "ALC,APC,Address,Remark                      ,DS,URNO"
    AddressTab.HeaderLengthString = "100,100,100,100,100,100"
    AddressTab.LogicalFieldList = "ALC3,APC3,TEAD,BEME,PAYE,URNO"
    AddressTab.ColumnWidthString = "3,3,7,100,10,10"
    AddressTab.ColumnAlignmentString = "L,L,L,L,L,L"
    AddressTab.CreateDecorationObject "SelMarker", "L,R,T,B", "2,2,2,2", Str(LightGray) & "," & Str(vbBlack) & "," & Str(LightGray) & "," & Str(vbBlack)
    AddressTab.ShowHorzScroller True
    AddressTab.LifeStyle = True
    AddressTab.AutoSizeByHeader = True
    AddressTab.AutoSizeColumns
    
    AlcTab.ResetContent
    AlcTab.FontName = "Courier New"
    AlcTab.HeaderFontSize = 14
    AlcTab.FontSize = 14
    AlcTab.LineHeight = 14
    AlcTab.SetTabFontBold True
    AlcTab.HeaderString = "-ALC-,-ALC-,-ALC-,-ALC-,-ALC-,-ALC-, "
    AlcTab.HeaderLengthString = "100,100,100,100,100,100,100"
    AlcTab.ColumnAlignmentString = "C,C,C,C,C,C,C"
    AlcTab.HeaderAlignmentString = "C,C,C,C,C,C,C"
    AlcTab.ShowVertScroller False
    AlcTab.ShowRowSelection = False
    AlcTab.InsertTextLine ",,,,,,", False
    AlcTab.EnableInlineEdit True
    AlcTab.LifeStyle = True
    AlcTab.AutoSizeByHeader = True
    AlcTab.AutoSizeColumns
    
    ApcTab.ResetContent
    ApcTab.FontName = "Courier New"
    ApcTab.HeaderFontSize = 14
    ApcTab.FontSize = 14
    ApcTab.LineHeight = 14
    ApcTab.SetTabFontBold True
    ApcTab.HeaderString = "-APC-,-APC-,-APC-,-APC-,-APC-,-APC-, "
    ApcTab.HeaderLengthString = "100,100,100,100,100,100,100"
    ApcTab.ColumnAlignmentString = "C,C,C,C,C,C,C"
    ApcTab.HeaderAlignmentString = "C,C,C,C,C,C,C"
    ApcTab.ShowVertScroller False
    ApcTab.ShowRowSelection = False
    ApcTab.InsertTextLine ",,,,,,", False
    ApcTab.EnableInlineEdit True
    ApcTab.AutoSizeByHeader = True
    ApcTab.LifeStyle = True
    ApcTab.AutoSizeColumns
    ApcTab.Top = AlcTab.Top + 510
    
    AddressTab.Top = ApcTab.Top + 510
    
    GroupTab.ResetContent
    GroupTab.FontName = "Courier New"
    GroupTab.HeaderFontSize = 14
    GroupTab.FontSize = 14
    GroupTab.LineHeight = 14
    GroupTab.SetTabFontBold True
    GroupTab.HeaderString = "Group Name  ,Description                       "
    GroupTab.HeaderLengthString = "100,100"
    GroupTab.ColumnWidthString = "16,32"
    GroupTab.ColumnAlignmentString = "L,L"
    GroupTab.HeaderAlignmentString = "C,C"
    GroupTab.ShowVertScroller True
    GroupTab.ShowHorzScroller True
'    GroupTab.ShowRowSelection = False
    GroupTab.InsertTextLine ",", False
    GroupTab.EnableInlineEdit False
    GroupTab.AutoSizeByHeader = True
    GroupTab.LifeStyle = True
    GroupTab.AutoSizeColumns
    GroupTab.Top = 870
    GroupTab.Height = 4615
    TemplateTab.ResetContent
    TemplateTab.FontName = "Courier New"
    TemplateTab.HeaderFontSize = 14
    TemplateTab.FontSize = 14
    TemplateTab.LineHeight = 14
    TemplateTab.SetTabFontBold True
    TemplateTab.HeaderString = "T,Template    ,Description                       "
    TemplateTab.HeaderLengthString = "100,100,100"
    TemplateTab.ColumnWidthString = "1,16,32"
    TemplateTab.ColumnAlignmentString = "L,L,L"
    TemplateTab.HeaderAlignmentString = "C,C,C"
'    TemplateTab.ShowVertScroller True
'    TemplateTab.ShowHorzScroller True
    'TemplateTab.ShowRowSelection = False
    TemplateTab.InsertTextLine ",", False
    TemplateTab.EnableInlineEdit False
    TemplateTab.AutoSizeByHeader = True
    TemplateTab.LifeStyle = True
    TemplateTab.AutoSizeColumns
'    TemplateTab.top = 870
'    TemplateTab.Height = 4615

    fraPreview.Top = fraAddress.Top
    fraPreview.Left = fraTelexText.Left
    
    MySitaOutType = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SITA_OUT_TYPE", "SITA_NODE")
    
End Sub

Private Sub InitOrigList()
    Dim OrigList As String
    Dim CurAddr As String
    Dim CurBeme As String
    Dim ItemNo As Integer
    Dim LineNo As Long
    Dim LineList As String
    Dim NewLine As String
'    OrigList = MySetUp.HomeTlxAddr
'    ItemNo = 0
'    CurAddr = GetRealItem(OrigList, ItemNo, ",")
'    While CurAddr <> ""
'        CurBeme = ""
'        LineList = UfisServer.BasicData(0).GetLinesByColumnValue(3, CurAddr, 0)
'        If LineList <> "" Then
'            LineNo = Val(LineList)
'            CurBeme = UfisServer.BasicData(0).GetColumnValue(LineNo, 5)
'        End If
'        NewLine = CurAddr & "," & CurBeme
'        TlxOrigTab.InsertTextLine NewLine, False
'        ItemNo = ItemNo + 1
'        CurAddr = GetRealItem(OrigList, ItemNo, ",")
'    Wend
'    NewLine = "SINKDSQ" & "," & "SATS Operations Control Centre SOCC for T2 airlines" 'igu on 09/02/2012
    NewLine = pstrTelexOriginAddress & "," & pstrTelexOriginDescription 'igu on 09/02/2012
    TlxOrigTab.InsertTextLine NewLine, False
    
    TlxOrigTab.AutoSizeColumns
    TlxOrigTab.Refresh
    TlxOrigTab.SetCurrentSelection 0
    
    'InitDblSignList
End Sub

Private Sub InitTlxTypeList()
    Dim i As Integer
    Dim NewLine As String
    Dim CurList As String
    Dim CurType As String
    Dim CurAddi As String
    Dim itm As Integer
    NewLine = "FREE,,Free Text"
    TlxTypeTab.InsertTextLine NewLine, False
'    For i = 0 To LastFolderIndex
'        CurList = TelexPoolHead.fraFolder(i).Tag
'        itm = 1
'        CurType = GetItem(CurList, itm, ",")
'        While CurType <> ""
'            If CurType <> "FREE" Then
'                CurAddi = ""
'                NewLine = CurType & ",," & CurAddi
'                TlxTypeTab.InsertTextLine NewLine, False
'            End If
'            itm = itm + 1
'            CurType = GetItem(CurList, itm, ",")
'        Wend
'    Next
    TlxTypeTab.AutoSizeColumns
    'TlxTypeTab.Sort "0", True, True
    TlxTypeTab.Refresh
    TlxTypeTab.SetCurrentSelection 0
End Sub
Private Sub InitPrioList()
    TlxPrioTab.InsertTextLine "QU,Queue Urgent", False
    TlxPrioTab.InsertTextLine "QD,Queue Deferred", False
    TlxPrioTab.InsertTextLine "QX,Queue Express", False
    TlxPrioTab.InsertTextLine "QK,", False
    TlxPrioTab.InsertTextLine "QL,", False
    TlxPrioTab.InsertTextLine "QS,Emergency", False
    TlxPrioTab.AutoSizeColumns
    TlxPrioTab.Refresh
    TlxPrioTab.SetCurrentSelection 0
End Sub

Private Sub GroupTab_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    If (Selected) And (LineNo >= 0) And (chkSelAddr(1).Value = 1) Then
        txtDestAddr.Text = CleanString(GroupTab.GetColumnValue(LineNo, 2), FOR_CLIENT, False)
        chkSelAddr(1).Tag = GroupTab.GetColumnValue(LineNo, 3)
    End If
End Sub
Private Sub GroupTab_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpLineNo As Integer
    
    If OnTop.Value = 1 Then
        OnTop.Value = 0
    End If
    tmpLineNo = GroupTab.GetCurrentSelected
    If tmpLineNo >= 0 Then
        frmEditGroups.txtGrpName.Text = GroupTab.GetColumnValue(tmpLineNo, 0)
        frmEditGroups.txtGrpDesc.Text = GroupTab.GetColumnValue(tmpLineNo, 1)
        frmEditGroups.txtGrpAddr.Text = CleanString(GroupTab.GetColumnValue(tmpLineNo, 2), FOR_CLIENT, False)
        frmEditGroups.txtGrpOwner.Text = GroupTab.GetColumnValue(tmpLineNo, 4)
        frmEditGroups.txtGrpName.Tag = GroupTab.GetColumnValue(tmpLineNo, 3)
        frmEditGroups.Visible = True
    Else
        MyMsgBox.CallAskUser 0, 0, 0, "Edit Groups", "No line sected", "hand", "", UserAnswer
    End If
End Sub

Private Sub OnTop_Click()
    If OnTop.Value = 1 Then OnTop.backColor = LightGreen Else OnTop.backColor = vbButtonFace
    If OnTop.Value <> 1 Then SetFormOnTop Me, False
    SetAllFormsOnTop True
End Sub

Private Sub Form_Resize()
    Dim NewValue As Long
    If fraFlightData.Visible Then
        NewValue = Me.ScaleHeight - fraFlightData.Top - StatusBar1.Height '- 60
        If NewValue > 1750 Then
            fraFlightData.Height = NewValue
            'FltInputPanel.Height = NewValue - FltInputPanel.top - 90
            'DrawBackGround FltInputPanel, MyLifeStyleValue, False, True
        End If
        NewValue = Me.ScaleWidth - fraFlightData.Width - 60
        If NewValue >= fraDestAddr.Left Then fraFlightData.Left = NewValue
    End If
    NewValue = Me.ScaleWidth - fraDestAddr.Left - 60
    If fraFlightData.Visible Then NewValue = NewValue - fraFlightData.Width - 30
    If NewValue > 1500 Then
        fraTelexText.Width = NewValue
        fraPreview.Width = NewValue
        fraDestAddr.Width = NewValue
        NewValue = NewValue - (txtDestAddr.Left * 2)
        txtTelexText.Width = NewValue
        txtDestAddr.Width = NewValue
        txtPreview.Width = NewValue
    End If
    NewValue = Me.ScaleHeight - fraPreview.Top ' - StatusBar1.Height '- 60
    If NewValue > 1750 Then
        fraPreview.Height = NewValue
        txtPreview.Height = NewValue - txtPreview.Top - 90
    End If
    NewValue = Me.ScaleHeight - fraOrigAddr.Top ' - StatusBar1.Height '- 60
    If NewValue > 1200 Then
        If fraTlxPrio.Tag <> "" Then
            fraTlxPrio.Height = NewValue
            TlxPrioTab.Height = NewValue - TlxPrioTab.Top - 90
        End If
        If fraOrigAddr.Tag <> "" Then
            fraOrigAddr.Height = NewValue
            TlxOrigTab.Height = NewValue - TlxOrigTab.Top - 90
        End If
        If fraTlxType.Tag <> "" Then
            fraTlxType.Height = NewValue
            TlxTypeTab.Height = NewValue - TlxTypeTab.Top - 90
        End If
        If fraDblSign.Tag <> "" Then
            fraDblSign.Height = NewValue
            DblSignTab.Height = NewValue - DblSignTab.Top - 90
        End If
    End If
    NewValue = Me.ScaleHeight - fraTelexText.Top ' - StatusBar1.Height '- 60
    If NewValue > 900 Then
        fraTelexText.Height = NewValue
        txtTelexText.Height = NewValue - txtTelexText.Top - 90
    End If
    NewValue = Me.ScaleHeight - fraAddress.Top ' - StatusBar1.Height '- 60
    If NewValue > 2700 Then
        fraAddress.Height = NewValue
        AddressTab.Height = NewValue - AddressTab.Top - 90
    End If
End Sub

Public Sub LoadGroupData()
    Dim tmpSqlKey As String
    Dim tmpFields As String
    Dim i As Integer
    Dim selLine As Integer
    Dim tlxOrigLineNo As Integer
    
    GroupTab.ResetContent
    GroupTab.Refresh
    Me.MousePointer = 11
    tmpFields = "GRPN,GRDE,GRAL,URNO,TPOW"
    tmpSqlKey = "WHERE TYPE = 'G'"
    tlxOrigLineNo = TlxOrigTab.GetCurrentSelected
    If tlxOrigLineNo >= 0 Then
        tmpSqlKey = tmpSqlKey & " AND TPOW = '" & TlxOrigTab.GetColumnValue(tlxOrigLineNo, 0) & "'"
    End If
    GroupTab.CedaServerName = strServer
    GroupTab.CedaPort = "3357"
    GroupTab.CedaHopo = strHopo
    GroupTab.CedaCurrentApplication = "HUB-Manager" & "," & frmData.GetApplVersion(True)
    GroupTab.CedaTabext = "TAB"
    GroupTab.CedaUser = strCurrentUser
    GroupTab.CedaWorkstation = frmData.aUfis.WorkStation
    GroupTab.CedaSendTimeout = "3"
    GroupTab.CedaReceiveTimeout = "240"
    GroupTab.CedaRecordSeparator = Chr(10)
    GroupTab.CedaIdentifier = "HUB-Manager"
    
    'If CedaIsConnected Then
        GroupTab.CedaAction "RT", "TTPTAB", tmpFields, "", tmpSqlKey
    'End If
    GroupTab.Sort "0", True, True
    GroupTab.AutoSizeColumns
    If chkSelAddr(1).Tag = "" Then
        selLine = -1
    Else
        For i = 0 To GroupTab.GetLineCount - 1
            If GroupTab.GetColumnValue(i, 3) = chkSelAddr(1).Tag Then
                selLine = i
            End If
        Next
    End If
    GroupTab.SetCurrentSelection selLine
    GroupTab.OnVScrollTo selLine
    If selLine >= 0 Then
        txtDestAddr.Text = CleanString(GroupTab.GetColumnValue(selLine, 2), FOR_CLIENT, False)
    End If
    Me.MousePointer = 0
End Sub

Public Sub LoadTemplateData()
    Dim tmpSqlKey As String
    Dim tmpFields As String
    Dim i As Integer
    Dim selLine As Integer
    Dim tlxOrigLineNo As Integer
    
    TemplateTab.ResetContent
    TemplateTab.Refresh
    Me.MousePointer = 11
    tmpFields = "TYPE,TPLN,TPDE,TPTX,URNO,GRAL,TPTP,TPT2,TPOW"
    If chkTplSel(0).Value = 1 Then
        tmpSqlKey = "WHERE TYPE = 'T'"
    Else
        If chkTplSel(1).Value = 1 Then
            tmpSqlKey = "WHERE TYPE = 'C'"
        Else
            If chkTplSel(2).Value = 1 Then
                tmpSqlKey = "WHERE TYPE = 'L'"
            Else
                tmpSqlKey = "WHERE TYPE = 'T'"
            End If
        End If
    End If
    tlxOrigLineNo = TlxOrigTab.GetCurrentSelected
    If tlxOrigLineNo >= 0 Then
        tmpSqlKey = tmpSqlKey & " AND TPOW = '" & TlxOrigTab.GetColumnValue(tlxOrigLineNo, 0) & "'"
    End If
    
    TemplateTab.CedaServerName = strServer
    TemplateTab.CedaPort = "3357"
    TemplateTab.CedaHopo = strHopo
    TemplateTab.CedaCurrentApplication = "HUB-Manager" & "," & frmData.GetApplVersion(True)
    TemplateTab.CedaTabext = "TAB"
    TemplateTab.CedaUser = strCurrentUser
    TemplateTab.CedaWorkstation = frmData.aUfis.WorkStation
    TemplateTab.CedaSendTimeout = "3"
    TemplateTab.CedaReceiveTimeout = "240"
    TemplateTab.CedaRecordSeparator = Chr(10)
    TemplateTab.CedaIdentifier = "HUB-Manager"
    'If CedaIsConnected Then
        TemplateTab.CedaAction "RT", "TTPTAB", tmpFields, "", tmpSqlKey
    'End If
    TemplateTab.Sort "1", True, True
    TemplateTab.AutoSizeColumns
    If chkSelAddr(2).Tag = "" Then
        selLine = 0
    Else
        For i = 0 To TemplateTab.GetLineCount - 1
            If TemplateTab.GetColumnValue(i, 4) = chkSelAddr(2).Tag Then
                selLine = i
            End If
        Next
    End If
    TemplateTab.SetCurrentSelection selLine
    TemplateTab.OnVScrollTo selLine
    txtTelexText.Text = CleanString(TemplateTab.GetColumnValue(selLine, 3), FOR_CLIENT, False)
    If Len(TemplateTab.GetColumnValue(selLine, 7)) > 0 Then
        txtTelexText.Text = txtTelexText.Text & CleanString(TemplateTab.GetColumnValue(selLine, 7), FOR_CLIENT, False)
    End If
    Me.MousePointer = 0
End Sub

Private Sub SetFirstFreeCell(CurTab As TABLib.TAB)
    Dim CurCol As Long
    CurTab.SetFocus
    CurCol = 0
    CurTab.SetInplaceEdit 0, CurCol, False
    CurTab.Tag = "0"
End Sub
Private Sub InitDblSignList(CodeList As String)
    Dim CurItm As Integer
    Dim tmpCode As String
    Dim tmpLine As String
    DblSignTab.ResetContent
    DblSignTab.SetUniqueFields "0"
    DblSignTab.InsertTextLine "--,None", False
    CurItm = 1
    tmpCode = GetItem(CodeList, CurItm, ",")
    While tmpCode <> ""
        tmpLine = tmpCode & ","
        DblSignTab.InsertTextLine tmpLine, False
        CurItm = CurItm + 1
        tmpCode = GetItem(CodeList, CurItm, ",")
    Wend
    DblSignTab.AutoSizeColumns
    DblSignTab.Refresh
    If CodeList <> "" Then
        DblSignTab.SetCurrentSelection 1
    Else
        DblSignTab.SetCurrentSelection 0
    End If
End Sub

Public Sub SetSpecialReqVars(ByVal ArrFlno As String, DepFlights As Collection)
    m_ArrFlno = ArrFlno
    Set m_DepFlights = DepFlights
End Sub

Private Function CopyToSpecialRequirementsTable(ByVal TelexText As String, ByVal ArrFlno As String, DepFlights As Collection) As Boolean
'    'Arr Flight  -> Value: FLNO
'    'Dep Flights -> Value: Array(URNO, decision), Key = "DEP" & Urno of the flight
'
'    Dim strDepFlightUrnos As String
'    Dim vntDep As Variant
'    Dim strDepUrno As String
'    Dim strDepDecision As String
'    Dim strSpecialReq As String
'    Dim strLineNo As String
'    Dim strLINO As String
'    Dim i As Integer
    Dim blnResult As Boolean
'    Dim strCommand As String
'    Dim strTable As String
'    Dim strField As String
'    Dim strData As String
'    Dim strWhere As String
'    Dim strItemData As String
'    Dim strUTCNow As String
'
'    blnResult = False
'    If DepFlights.count > 0 Then
'        TelexText = " EX " & ArrFlno & vbNewLine & TelexText
'        strDepFlightUrnos = ""
'        For i = 1 To DepFlights.count
'            vntDep = DepFlights.Item(i)
'            strDepUrno = vntDep(0)
'
'            strDepFlightUrnos = strDepFlightUrnos & "," & strDepUrno
'        Next i
'
'        strDepFlightUrnos = Mid(strDepFlightUrnos, 2)
'
'        frmData.tabSRL.ResetContent
'        frmData.tabSRL.HeaderString = "URNO,MXLN"
'        frmData.tabSRL.LogicalFieldList = frmData.tabSRL.HeaderString
'        frmData.tabSRL.HeaderLengthString = "10,8"
'
'        strCommand = "RTA"
'        strTable = "SRLTAB"
'        strField = "FLNU,MAX(LINO) AS MXLN"
'        strWhere = "WHERE FLNU IN (" & strDepFlightUrnos & ") GROUP BY FLNU"
'        If frmData.tabSRL.CedaAction(strCommand, strTable, strField, strData, strWhere) Then
'            strUTCNow = Format(DateAdd("h", -UTCOffset, Now), "yyyyMMddhhmmss")
'
'            'insert into specialreq table
'            strCommand = "REL"
'            strTable = "SRLTAB"
'            strField = "TEXT,LINO,USEC,CDAT,AREA,FLNU,URNO"
'            strData = ""
'            strWhere = "LATE,NOBC,NOACTION"
'
'            strData = "*CMD*," & strTable & ",IRT," & CStr(ItemCount(strField, ",")) & "," & strField & vbLf
'            For i = 1 To DepFlights.count
'                vntDep = DepFlights.Item(i)
'                strDepUrno = vntDep(0)
'                strDepDecision = vntDep(1)
'
'                strSpecialReq = strDepDecision & TelexText
'                strSpecialReq = CleanString(strSpecialReq, CLIENT_TO_SERVER, False)
'                strLineNo = frmData.tabSRL.GetLinesByColumnValue(0, strDepUrno, 1)
'                strItemData = ""
'                If InStr(strLineNo, ",") = 0 Then
'                    If strLineNo <> "" Then
'                        strLINO = frmData.tabSRL.GetFieldValue(Val(strLineNo), "MXLN")
'                        strLINO = IncrStrNo(strLINO)
'                    Else
'                        strLINO = "D001.001"
'                    End If
'
'                    strItemData = strSpecialReq & "," & strLINO & "," & _
'                        frmData.aUfis.UserName & "," & strUTCNow & ",A," & _
'                        strDepUrno & "," & frmData.GetNextUrno()
'                End If
'                If strItemData <> "" Then
'                    strData = strData & strItemData & vbLf
'                End If
'            Next i
'
'            blnResult = True
'            'send REL command
'            frmData.aUfis.CallServer strCommand, strTable, strField, strData, strWhere, "0"
'
'            'send BC
'            strCommand = "SBC"
'            strTable = strTable & "/REFR"
'            strField = ""
'            strWhere = frmData.aUfis.WorkStation
'            strWhere = strWhere & "," & CStr(App.ThreadID) & "," & "REQU"
'            strWhere = strWhere & "," & "ROT"
'            For i = 1 To DepFlights.count
'                vntDep = DepFlights.Item(i)
'                strDepUrno = vntDep(0)
'                strData = "0," & strDepUrno
'
'                frmData.aUfis.CallServer strCommand, strTable, strField, strData, strWhere, "0"
'            Next i
'        End If
'    Else
        blnResult = True
'    End If
'
    CopyToSpecialRequirementsTable = blnResult
End Function

Private Function IncrStrNo(ByVal StringNumber As String) As String
    Dim strRet As String
    Dim strChar As String
    Dim strNumber As String
    Dim lngNumber As Long
    Dim intLength As Integer
    Dim intNumLength As Integer
    Dim i As Integer
    
    strRet = StringNumber
    intLength = Len(strRet)
    strNumber = ""
    For i = intLength To 1 Step -1
        strChar = Mid(strRet, i, 1)
        If IsNumeric(strChar) Then
            strNumber = strChar & strNumber
        Else
            Exit For
        End If
    Next i
    
    If strNumber <> "" Then
        intNumLength = Len(strNumber)
        lngNumber = Val(strNumber) + 1
        If Len(strNumber) <= intNumLength Then
            strNumber = Format(lngNumber, String(intNumLength, "0"))
            strRet = Left(strRet, intLength - intNumLength) & strNumber
        End If
    End If
    
    IncrStrNo = strRet
End Function
