VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmSelectTemplate 
   Caption         =   "Select Department ..."
   ClientHeight    =   7455
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5865
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7455
   ScaleWidth      =   5865
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   465
      Left            =   2317
      TabIndex        =   2
      Top             =   6795
      Width           =   1230
   End
   Begin TABLib.TAB t 
      Height          =   6225
      Left            =   90
      TabIndex        =   0
      Top             =   360
      Width           =   5640
      _Version        =   65536
      _ExtentX        =   9948
      _ExtentY        =   10980
      _StockProps     =   64
   End
   Begin VB.Label Label1 
      Caption         =   "Please select your Department:"
      Height          =   285
      Left            =   90
      TabIndex        =   1
      Top             =   45
      Width           =   5640
   End
End
Attribute VB_Name = "frmSelectTemplate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public strTplUrno As String

Private Sub cmdOK_Click()
    Dim llLine As Long
    
    llLine = t.GetCurrentSelected
    If llLine > -1 Then
        strTplUrno = t.GetFieldValue(llLine, "URNO")
        Me.Hide
    Else
        MsgBox "Please select an entry in the list!!"
    End If
End Sub

Private Sub Form_Load()
    Dim strBuffer As String
    
    t.ResetContent
    t.HeaderString = "URNO,Name"
    t.LogicalFieldList = "URNO,TNAM"
    t.ColumnWidthString = "10,64"
    t.HeaderLengthString = "-1,400"
    
    strBuffer = frmData.tabTPL.GetBuffer(0, frmData.tabTPL.GetLineCount - 1, vbLf)
    t.InsertBuffer strBuffer, vbLf
    t.Sort "1", True, True
    t.SetCurrentSelection 0
End Sub

Private Sub t_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    strTplUrno = t.GetFieldValue(LineNo, "URNO")
    Me.Hide
End Sub
