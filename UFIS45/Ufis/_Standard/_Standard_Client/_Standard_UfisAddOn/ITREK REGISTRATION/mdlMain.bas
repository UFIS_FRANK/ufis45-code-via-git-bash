Attribute VB_Name = "mdlMain"
Option Explicit

Global gsAppName As String
Global gsUserName As String
Global gsTplUrno As String

Global gbDebug As Boolean
Global gbTeamLeaderGroup As Boolean
Global gsDebugToday As String

Global sglUTCOffsetHours As Single

Sub Main()
    Dim strRetLogin As String
    Dim strCommand As String

    gsAppName = "ITREK-APPLN"
    strCommand = Command()
    If UCase(GetItem(strCommand, 1, ",")) = "DEBUG" Then
        gbDebug = True
        Dim strTime As String
        strTime = GetItem(strCommand, 3, ",")
        If Len(strTime) > 0 And IsDate(strTime) = True Then
             Date = strTime
        End If
    Else
        gbDebug = False
    End If

    'do the login
    InitLoginControl
    
    If gbDebug = False Then
        strRetLogin = frmSplash.AATLoginControl1.ShowLoginDialog
    Else
        strRetLogin = "OK"
    End If

    ' close connection to server
    frmSplash.UfisCom1.CleanupCom

    'look after the login result
    If strRetLogin = "CANCEL" Or strRetLogin = "ERROR" Then
        End
    Else
        ' save the user name
        gsUserName = frmSplash.AATLoginControl1.GetUserName()

        ' load the data form (not visible)
        Load frmData

        ' check if we are running in debug mode
        If gbDebug = True Then
            gsUserName = GetItem(strCommand, 2, ",")
            frmData.Visible = True
        End If

        ' load the data and create the views
'        frmData.InitialLoad True, True

        ' show the main dialog
        
        MsgBox " Registred. Exiting"
        End
        
'        frmMain.Show
    End If
End Sub

Private Sub InitLoginControl()
    Dim strRegister As String
    ' giving the login control an UfisCom to do the login
    Set frmSplash.AATLoginControl1.UfisComCtrl = frmSplash.UfisCom1

    ' setting some information to be displayed on the login control
    frmSplash.AATLoginControl1.ApplicationName = gsAppName
    frmSplash.AATLoginControl1.VersionString = "4.5.0.23"
    frmSplash.AATLoginControl1.InfoCaption = "Info about InfoPC"
    frmSplash.AATLoginControl1.InfoButtonVisible = True
    frmSplash.AATLoginControl1.InfoUfisVersion = "UFIS Version 4.5"
    frmSplash.AATLoginControl1.InfoAppVersion = CStr("InfoPC 4.5.0.23")
    frmSplash.AATLoginControl1.InfoCopyright = "� 2001-2005 UFIS Airport Solutions GmbH"
    frmSplash.AATLoginControl1.InfoAAT = "UFIS Airport Solutions GmbH"
    frmSplash.AATLoginControl1.UserNameLCase = False 'not automatic upper case letters
    frmSplash.AATLoginControl1.ApplicationName = "ITREK-APPLN"
    frmSplash.AATLoginControl1.LoginAttempts = 3

    ' build the register string for BDPS-SEC
    strRegister = "ITREK-APPLN,InitModu,InitModu,Initialize (InitModu),B,-"
    strRegister = strRegister & ",General,Z_WAP,WAP Access,Z,1"
    strRegister = strRegister & ",General,Z_WEB,WEB Access,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WAP,WAP_ACCESS,WAP Access,Z,1"
    'strRegister = strRegister & ",ITREK-APPLN_WEB,WEB_ADMIN,WEB Admin Access,Z,1"
    'strRegister = strRegister & ",ITREK-APPLN_WEB,WEB_FLIGHTSCREEN,WEB Flight Screen,Z,1"
    'strRegister = strRegister & ",ITREK-APPLN_WEB,WEB_FLIGHTALLSCREEN,WEB All Flight Screen,Z,1"
   
'''    strRegister = strRegister & ",ITREK-APPLN_WEB,WEB_FLIGHTSCREEN,Access Messages,B,1"
'''    strRegister = strRegister & ",ITREK-APPLN_WEB,WEB_FLIGHTSCREEN,Adm-Define PreDef Msg,B,1"
'''    strRegister = strRegister & ",ITREK-APPLN_WEB,WEB_FLIGHTSCREEN,Adm-Define PreDef Rmk,B,1"
'''    strRegister = strRegister & ",ITREK-APPLN_WEB,WEB_FLIGHTSCREEN,Adm-View Msg Logs,B,1"
      
    ' New function / button added by kkh
    strRegister = strRegister & ",ITREK-APPLN_WEB,Access_Messages,Access Messages,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,Adm-Define_PreDef_Msg,Adm-Define PreDef Msg,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,Adm-Define_PreDef_Rmk,Adm-Define PreDef Rmk,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,Adm-View_Msg_Logs,Adm-View Msg Logs,Z,1"
    'added by kkh on 09/05/2008
    strRegister = strRegister & ",ITREK-APPLN_WEB,CPM_Priority,CPM Priority,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,Create_Apron_Service_Rpt-Arrival,Create Apron Service Rpt-Arrival,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,Create_Bag_Present_Timing_Rpt-Arrival,Create Bag Present Timing Rpt-Arrival,Z,1"
    'Added By Phyoe on 05-Feb-2013 (Create Ramp Check List)
    strRegister = strRegister & ",ITREK-APPLN_WEB,Create_RCL,Create RCL,Z,-"
    strRegister = strRegister & ",ITREK-APPLN_WEB,Edit_Apron_Service_Rpt-Arrival,Edit Apron Service Rpt-Arrival,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,Edit_Arr_Flight_Timing-Apron,Edit Arr Flight Timing-Apron,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,Edit_Arr_Flight_Timing-Baggage,Edit Arr Flight Timing-Baggage,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,Edit_Bag_Present_Timing_Rpt-Arrival,Edit Bag Present Timing Rpt-Arrival,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,Edit_Dep_Flight_Timing-Apron,Edit Dep Flight Timing-Apron,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,Edit_Dep_Flight_Timing-Baggage,Edit Dep Flight Timing-Baggage,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,Send_Flight_Message,Send Flight Message,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,View_All_Arr_Flight-Apron,View All Arr Flight-Apron,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,View_All_Arr_Flight-Baggage,View All Arr Flight-Baggage,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,View_All_Dep_Flight-Apron,View All Dep Flight-Apron,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,View_All_Dep_Flight-Baggage,View All Dep Flight-Baggage,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,View_Apron_Service_Rpt-Arrival,View Apron Service Rpt-Arrival,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,View_Assigned_Arr_Flight-Apron,View Assigned Arr Flight-Apron,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,View_Assigned_Arr_Flight-Baggage,View Assigned Arr Flight-Baggage,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,View_Assigned_Dep_Flight-Apron,View Assigned Dep Flight-Apron,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,View_Assigned_Dep_Flight-Baggage,View Assigned Dep Flight-Baggage,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,View_Bag_Present_Timing_Rpt-Arrival,View Bag Present Timing Rpt-Arrival,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,View_Bag_Present_Timing_Summ_Rpt,View Bag Present Timing Summ Rpt,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,View_Benchmark_Apron_Arrival,View Benchmark Apron Arrival,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,View_Benchmark_Baggage_Arrival,View Benchmark Baggage Arrival,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,View_CPM,View CPM,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,View_DLS,View DLS,Z,1"
    'Added By Phyoe on 05-Feb-2013 (View Ramp Check List)
    strRegister = strRegister & ",ITREK-APPLN_WEB,View_RCL,View RCL,Z,-"
    strRegister = strRegister & ",ITREK-APPLN_WEB,View_Flight_Staff-Apron,View Flight Staff-Apron,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,View_Message_Detail,View Message Detail,Z,1"
    strRegister = strRegister & ",ITREK-APPLN_WEB,View_Trips,View Trips,Z,1"
        
    strRegister = strRegister & ",ITREK-APPLN_WEB,Adm-Term/Belt_Setup,Adm-Term/Belt Setup,Z,-"
    strRegister = strRegister & ",ITREK-APPLN_WEB,Adm-Alert_Config,Adm-Alert Config,Z,-"
    strRegister = strRegister & ",ITREK-APPLN_WEB,Bge_Arr,Bge Arr,Z,-"
    
    
    'Subdivision for Bge Main Screen
    strRegister = strRegister & ",ITREK-APPLN_WEB_Baggage,Bge_Arr_Scr,Bge Arr Scr,Z,-"
    strRegister = strRegister & ",ITREK-APPLN_WEB_Baggage,Recv_ULD,Recv ULD,Z,-"
    strRegister = strRegister & ",ITREK-APPLN_WEB_Baggage,First_Bag,First Bag,Z,-"
    strRegister = strRegister & ",ITREK-APPLN_WEB_Baggage,Last_Bag,Last Bag,Z,-"
    strRegister = strRegister & ",ITREK-APPLN_WEB_Baggage,Show_Cntr,Show Cntr,Z,-"
    strRegister = strRegister & ",ITREK-APPLN_WEB_Baggage,Recv_Cntr,Recv Cntr,Z,-"
    strRegister = strRegister & ",ITREK-APPLN_WEB_Baggage,Show_Flight,Show Flight,B,-"
    
     
    frmSplash.AATLoginControl1.RegisterApplicationString = strRegister
    Debug.Print strRegister
    
End Sub

Public Sub ExitApplication()
    End
End Sub
