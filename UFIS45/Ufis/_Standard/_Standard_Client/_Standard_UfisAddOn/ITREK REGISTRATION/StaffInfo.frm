VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMain 
   Caption         =   "Staff Information (InfoPC)"
   ClientHeight    =   8895
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   14985
   Icon            =   "StaffInfo.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8895
   ScaleWidth      =   14985
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdFlightInfo 
      Caption         =   "&Flight Info"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5265
      TabIndex        =   25
      Top             =   7950
      Width           =   2295
   End
   Begin VB.CommandButton cmdTelexViewer 
      Caption         =   "&TelexViewer"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2850
      TabIndex        =   24
      Top             =   7950
      Width           =   2295
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "&Exit"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   12600
      TabIndex        =   23
      Top             =   7950
      Width           =   2295
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7695
      TabIndex        =   22
      Top             =   7950
      Width           =   2295
   End
   Begin VB.CommandButton cmdStatus 
      Caption         =   "&Status"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   450
      TabIndex        =   21
      Top             =   7950
      Width           =   2295
   End
   Begin VB.CommandButton cmdLogout 
      Caption         =   "&Logout"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10140
      TabIndex        =   20
      Top             =   7950
      Width           =   2295
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   4
      Top             =   8520
      Width           =   14985
      _ExtentX        =   26432
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   20770
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            TextSave        =   "09-Feb-07"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            TextSave        =   "5:27 PM"
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame1 
      Height          =   2040
      Left            =   75
      TabIndex        =   1
      Top             =   15
      Width           =   14790
      Begin VB.Label lblFCTC 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   10020
         TabIndex        =   18
         Tag             =   "PERC"
         Top             =   1380
         Width           =   3105
      End
      Begin VB.Label Label6 
         Caption         =   "Function:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   8085
         TabIndex        =   17
         Top             =   1455
         Width           =   1485
      End
      Begin VB.Label lblShiftTimes 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   10020
         TabIndex        =   16
         Tag             =   "PERC"
         Top             =   825
         Width           =   3105
      End
      Begin VB.Label Label5 
         Caption         =   "Shift:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   8085
         TabIndex        =   15
         Top             =   870
         Width           =   1485
      End
      Begin VB.Label lblSTFTAB 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   3
         Left            =   10020
         TabIndex        =   14
         Tag             =   "PENO"
         Top             =   285
         Width           =   3105
      End
      Begin VB.Label Label4 
         Caption         =   "Staff number:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   8085
         TabIndex        =   13
         Top             =   330
         Width           =   2025
      End
      Begin VB.Label lblSTFTAB 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   2
         Left            =   1890
         TabIndex        =   12
         Tag             =   "SHNM"
         Top             =   1380
         Width           =   5985
      End
      Begin VB.Label Label3 
         Caption         =   "Nickname:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   135
         TabIndex        =   11
         Top             =   1455
         Width           =   1485
      End
      Begin VB.Label Label2 
         Caption         =   "First name:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   135
         TabIndex        =   10
         Top             =   330
         Width           =   1710
      End
      Begin VB.Label lblSTFTAB 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   1890
         TabIndex        =   9
         Tag             =   "FINM"
         Top             =   285
         Width           =   5985
      End
      Begin VB.Label lblSTFTAB 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   0
         Left            =   1890
         TabIndex        =   3
         Tag             =   "LANM"
         Top             =   825
         Width           =   5985
      End
      Begin VB.Label Label1 
         Caption         =   "Last name:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   135
         TabIndex        =   2
         Top             =   870
         Width           =   1725
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5535
      Left            =   90
      TabIndex        =   0
      Top             =   2190
      Width           =   14760
      _ExtentX        =   26035
      _ExtentY        =   9763
      _Version        =   393216
      Tabs            =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Job information"
      TabPicture(0)   =   "StaffInfo.frx":030A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label8"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblTEXT"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "TAB_JobInformation"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "txtReloading"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "Shift information"
      TabPicture(1)   =   "StaffInfo.frx":0326
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "TAB_ShiftInformation"
      Tab(1).ControlCount=   1
      Begin VB.TextBox txtReloading 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   2805
         TabIndex        =   19
         Text            =   "Please wait while reloading user data..."
         Top             =   1905
         Visible         =   0   'False
         Width           =   9375
      End
      Begin TABLib.TAB TAB_ShiftInformation 
         Height          =   4065
         Left            =   -74760
         TabIndex        =   6
         Top             =   600
         Width           =   14205
         _Version        =   65536
         _ExtentX        =   25056
         _ExtentY        =   7170
         _StockProps     =   64
      End
      Begin TABLib.TAB TAB_JobInformation 
         Height          =   4065
         Left            =   240
         TabIndex        =   5
         Top             =   600
         Width           =   14205
         _Version        =   65536
         _ExtentX        =   25056
         _ExtentY        =   7170
         _StockProps     =   64
         FontName        =   "Courier New"
      End
      Begin VB.Label lblTEXT 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1980
         TabIndex        =   7
         Top             =   4950
         Width           =   12495
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label8 
         Caption         =   "Flight Remark:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   255
         TabIndex        =   8
         Top             =   4980
         Width           =   1695
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public strVISIBLE_COLUMNS As String
Public strSHOW_TIME_OF_JOB As String

Private Sub cmdClose_Click()
    End
End Sub
'comment by kkh
'Private Sub cmdFlightInfo_Click()
'    Dim strJobUrno As String
'    Dim llLineNo As Long
'    Dim strAftUrno As String
'    Dim strAftUrnos As String
'    Dim rpt As New rptFlightInfo
'    Dim demType As String
'
'    If gbDebug = True Then
'        frmData2.Show
'    End If
'
'
'    llLineNo = TAB_JobInformation.GetCurrentSelected
'    strJobUrno = TAB_JobInformation.GetLineTag(llLineNo)
'    strAftUrnos = frmData.GetAftUrnosByJob(strJobUrno)
'
'    If strJobUrno <> "" And Val(strAftUrnos) <> 0 Then
'        If frmData2.InitialLoad(True, True, strAftUrnos, gsTplUrno) = True Then
'            demType = frmData2.GetJobType(strJobUrno)
'            frmData2.CreateFlightInfoJobs strJobUrno
'            If demType = "0" Then rpt.strAftType = "AD"
'            If demType = "1" Then rpt.strAftType = "A"
'            If demType = "2" Then rpt.strAftType = "D"
'            If rpt.strAftType = "" Then rpt.strAftType = "AD"
'            rpt.strAftUrnos = strAftUrnos
'            rpt.Show , Me
'        Else
'            MsgBox "No relevant data found!"
'        End If
'    Else
'        MsgBox "This is not a flight related job!"
'    End If
'    Unload frmSelectTemplate
'End Sub

Private Sub cmdLogout_Click()
    Unload frmJobDetails
    Unload frmTelexViewer
    Me.Hide

    frmSplash.ProgressBar1.Value = 0
    

    Dim strRetLogin As String

    If gbDebug = False Then
        strRetLogin = frmSplash.AATLoginControl1.ShowLoginDialog
    Else
        strRetLogin = "OK"
    End If

    'look after the login result
    If strRetLogin = "CANCEL" Or strRetLogin = "ERROR" Then
        End
    Else
        Dim i As Integer

        ' save the user name
        If gbDebug = False Then
            gsUserName = frmSplash.AATLoginControl1.GetUserName()
        End If

        ' reset all controls and show the ampty form
        For i = 0 To lblSTFTAB.count - 1
            lblSTFTAB.Item(i).Caption = ""
        Next i
        lblFCTC.Caption = ""
        lblShiftTimes.Caption = ""
        TAB_JobInformation.ResetContent
        TAB_JobInformation.AutoSizeColumns
        TAB_ShiftInformation.ResetContent
        TAB_ShiftInformation.AutoSizeColumns
        SSTab1.Tab = 0
        txtReloading.Visible = True
        Me.Show
        Me.Refresh
        'Screen.MousePointer = vbHourglass

        ' reload the data
        frmData.InitialLoad False, False

        ' build the view
        Form_Load
        Screen.MousePointer = vbDefault

        ' show the main dialog
        txtReloading.Visible = False
    End If
End Sub
' comment by kkh
'Private Sub cmdPrint_Click()
'    If SSTab1.Tab = 0 Then
'        Dim rptJobList As New rptJobs
'        rptJobList.Show , Me
'    ElseIf SSTab1.Tab = 1 Then
'        Dim rptShiftList As New rptShifts
'        rptShiftList.Show , Me
'    End If
'End Sub

Private Sub Form_Load()
    If gbDebug = True Then
        ' disable the logout-button when running in debug-mode
        'frmMain.cmdLogout.Enabled = False
    End If

    '
    ' check the security settings
    '
    SetSecurity

    '
    ' get config-string (VISIBLE_COLUMNS=DEFAULT;No,Job,Team,Fctc,Duty,Service,ARR,STA,ETA,ORG,Regn,ACT,DEP,STD,ETD,DES,Gate,Location,Status)
    '
    strVISIBLE_COLUMNS = ""
    If (frmData.GetConfigEntry("VISIBLE_COLUMNS", frmData.strOrgUnit, strVISIBLE_COLUMNS) = False) Then
        frmData.MsgBoxNoCfgEntry "VISIBLE_COLUMNS"
    End If

    '
    ' get config-string (SHOW_TIME_OF_JOB=DEFAULT;FLT,GAT,CCI,BRK,SPE,CCC,FID)
    '
    strSHOW_TIME_OF_JOB = "" ' used only in frmData.GetJobTimes, but here is a good time for init
    If (frmData.GetConfigEntry("SHOW_TIME_OF_JOB", frmData.strOrgUnit, strSHOW_TIME_OF_JOB) = False) Then
        frmData.MsgBoxNoCfgEntry "SHOW_TIME_OF_JOB"
    Else
        strSHOW_TIME_OF_JOB = "," & strSHOW_TIME_OF_JOB & ","
    End If

    '
    ' initializing "TAB_JobInformation"
    '
    frmMain.StatusBar1.Panels(1).Text = "Building Job Grid ..."
    InitTabJobdetails
    InsertEmployeeInformation
    InsertShiftTimes
    InsertJobData
    TAB_JobInformation.AutoSizeColumns
    TAB_JobInformation.RedrawTab

    '
    ' initializing "TAB_ShiftInformation"
    '
    InitTabShiftData
    InsertShiftData
    TAB_ShiftInformation.RedrawTab

    '
    ' initializing "cmdStatus"-button
    '
'    cmdStatus.ToolTipText = "Status: 'Confirm job', 'Start job' or 'End job'"
'    cmdStatus.Caption = "Status"
'    cmdStatus.Enabled = False
    frmMain.StatusBar1.Panels(1).Text = "Ready."
    SSTab1_Click 1
End Sub

Private Sub cmdStatus_Click()
    Dim llLineNo As Long
    llLineNo = TAB_JobInformation.GetCurrentSelected

    If llLineNo > -1 Then
        ' set the next status of the job in dependancy of the workflow
        Dim strNextJobStatus As String
        strNextJobStatus = frmData.SetNextJobStatus(TAB_JobInformation.GetLineTag(llLineNo))

        ' update the TAB of the view
        Dim ilIdx As Integer
        ilIdx = GetRealItemNo(TAB_JobInformation.LogicalFieldList, "Status")
        If ilIdx > -1 Then
            TAB_JobInformation.SetColumnValue llLineNo, ilIdx, strNextJobStatus
        End If

        ' redraw the TAB and set the new caption of the status button
        TAB_JobInformation.RedrawTab
        TAB_JobInformation_RowSelectionChanged llLineNo, True

        ' do the coloring
        ColorLine llLineNo
        TAB_JobInformation.RedrawTab
    End If
End Sub

Private Sub cmdTelexViewer_Click()
    Dim llLineNo As Long
    llLineNo = TAB_JobInformation.GetCurrentSelected

    If llLineNo > -1 Then
        Dim strAftUrnos As String
        strAftUrnos = frmData.GetAftUrnosByJob(TAB_JobInformation.GetLineTag(llLineNo))
        If Len(strAftUrnos) > 0 And strAftUrnos <> "0" Then
            Load frmTelexViewer
            frmTelexViewer.DisplayTelexInformation strAftUrnos
        Else
            MsgBox "No flights found for this job.", vbInformation, "No Flights"
        End If
    Else
        MsgBox "Please select a job first!", vbInformation, "No job selected"
    End If
End Sub

Private Sub InitTabJobdetails()
    TAB_JobInformation.ResetContent
    TAB_JobInformation.FontName = "Courier New"
    TAB_JobInformation.HeaderFontSize = "16"
    TAB_JobInformation.FontSize = "14"
    TAB_JobInformation.SetTabFontBold True
    TAB_JobInformation.EnableHeaderSizing True
    TAB_JobInformation.LineHeight = 18

    Dim strLogicalFieldList As String
    strLogicalFieldList = "No,Job,Team,TeamLeader,Fctc,Duty,Service,ARR,STA,ETA,ORG,Regn,ACT,DEP,STD,ETD,DES,Gate,Location,Status"

    ' check, which columns to display and build the header length string
    Dim i As Integer
    Dim strTmp As String
    Dim strItem As String
    Dim strHeaderLengthString As String
    strTmp = "," & strVISIBLE_COLUMNS & ","
    For i = 1 To ItemCount(strLogicalFieldList, ",")
        strItem = "," & GetItem(strLogicalFieldList, i, ",") & ","
        If InStr(1, strTmp, strItem, vbTextCompare) > 0 Then
            strHeaderLengthString = strHeaderLengthString & ",10"
        Else
            strHeaderLengthString = strHeaderLengthString & ",-1"
        End If
    Next i
    If Len(strHeaderLengthString) > 0 Then
        strHeaderLengthString = Mid(strHeaderLengthString, 2)
    End If
    TAB_JobInformation.HeaderLengthString = strHeaderLengthString

    TAB_JobInformation.HeaderString = "No,Job time,Team,Leader,Fct.,Duty,Serv.,ARR,STA,ETA,ORG,Regn.,ACT,DEP,STD,ETD,DES,Gate,Location,Status"
    TAB_JobInformation.LogicalFieldList = strLogicalFieldList
    TAB_JobInformation.LeftTextOffset = 2
    TAB_JobInformation.SetMainHeaderValues "11,2,7", "", ""
    TAB_JobInformation.ShowHorzScroller True
    TAB_JobInformation.AutoSizeByHeader = True

    Dim strColors As String
    strColors = CStr(vbBlue) & "," & CStr(vbBlue)
    TAB_JobInformation.CursorDecoration TAB_JobInformation.LogicalFieldList, "B,T", "3,3", strColors
    TAB_JobInformation.DefaultCursor = False
End Sub

Private Sub InsertJobData()
    Dim strJobUrno As String
    Dim strLine As String
    Dim i As Integer

    TAB_JobInformation.ResetContent

    frmData.FilterJobs 'we have to display e.g. only the next one, only the next 4, or anything like that

    For i = 0 To frmData.TabJOB.GetLineCount - 1
        ' getting the job urno
        strJobUrno = frmData.TabJOB.GetColumnValue(i, 0)

        ' No
        strLine = CStr(i + 1) & ".,"

        ' Job time
        strLine = strLine & frmData.GetJobTimes(strJobUrno) & ","

        ' Team code
        strLine = strLine & frmData.GetTeamCodeByJob(strJobUrno) & ","
        
        ' Team leader code
        strLine = strLine & frmData.GetTeamLeaderCodeByJob(strJobUrno) & ","

        ' Fct.
        strLine = strLine & frmData.GetJobFunction(strJobUrno) & ","

        ' Duty
        strLine = strLine & frmData.GetJobDuty(strJobUrno) & ","

        ' Service name
        strLine = strLine & frmData.GetJobServiceName(strJobUrno) & ","

        ' Flight information (ARR,STA,ETA,ORG,Regn.,ACT,DEP,STD,ETD,DES)
        strLine = strLine & frmData.GetFlightInformation(strJobUrno) & ","

        ' Location
        strLine = strLine & frmData.GetJobLocation(strJobUrno) & ","

        ' Status
        strLine = strLine & frmData.GetJobStatus(strJobUrno, "") & ","

        ' insert the line and keep the job-urno in mind
        TAB_JobInformation.InsertTextLine strLine, False
        TAB_JobInformation.SetLineTag TAB_JobInformation.GetLineCount - 1, strJobUrno
        ColorLine (TAB_JobInformation.GetLineCount - 1)
    Next i

End Sub

Private Sub InitTabShiftData()
    TAB_ShiftInformation.ResetContent
    TAB_ShiftInformation.FontName = "Courier New"
    TAB_ShiftInformation.HeaderFontSize = "18"
    TAB_ShiftInformation.FontSize = "16"
    TAB_ShiftInformation.SetTabFontBold True
    TAB_ShiftInformation.EnableHeaderSizing True
    TAB_ShiftInformation.ColumnAlignmentString = "L,C,C,C,C,C,L"
    TAB_ShiftInformation.HeaderAlignmentString = "C,C,C,C,C,C,C"
    TAB_ShiftInformation.LineHeight = 18
    TAB_ShiftInformation.HeaderLengthString = "38,121,129,137,98,73,285"
    TAB_ShiftInformation.HeaderString = "No,Day,Shift code,Shift time,Function,Group,Remark"
    TAB_ShiftInformation.LogicalFieldList = "No,SDAY,SCOD,ShiftTime,FCTC,WGPC,REMA"
    TAB_ShiftInformation.CreateDecorationObject "CHANGE", "BR", "8", vbRed

    TAB_ShiftInformation.LeftTextOffset = 2

    TAB_ShiftInformation.ShowHorzScroller True
    TAB_ShiftInformation.AutoSizeByHeader = True

    Dim strColors As String
    strColors = CStr(vbBlue) & "," & CStr(vbBlue)
    TAB_ShiftInformation.CursorDecoration TAB_ShiftInformation.LogicalFieldList, "B,T", "3,3", strColors
    TAB_ShiftInformation.DefaultCursor = False
End Sub

Private Sub InsertShiftData()
    Dim strDrrUrno As String
    Dim strDrrRoss As String
    Dim strDrrRosl As String
    Dim strDrrSday1 As String
    Dim strDrrSday2 As String
    Dim strLine As String
    Dim i As Integer
    Dim j As Integer
    Dim ilInsertedShifts As Integer
    Dim blAdd As Boolean
    Dim strDraUrno As String
    Dim strLines As String
    Dim ilLineCount As Integer
    Dim strLineNo As String
    Dim ilIdx As Integer

    TAB_ShiftInformation.ResetContent
    ilInsertedShifts = 0

    ' DRR-records are sorted by SDAY,ROSL
    For i = 0 To frmData.TabDRR.GetLineCount - 1
        blAdd = False
        strDrrRoss = frmData.TabDRR.GetFieldValue(i, "ROSS")
        If strDrrRoss = "A" Then
            blAdd = True
        Else
            strDrrRosl = frmData.TabDRR.GetFieldValue(i, "ROSL")
            If strDrrRosl = 1 Then
                strDrrSday1 = frmData.TabDRR.GetFieldValue(i, "SDAY")
                strDrrSday2 = frmData.TabDRR.GetFieldValue(i + 1, "SDAY")
                If strDrrSday1 <> strDrrSday2 Then
                    ' There are no shifts in a higher level than level 1 on that day yet.
                    ' So we have to add this record
                    blAdd = True
                End If
            End If
        End If

        If blAdd = True Then
            ' getting the shift urno
            strDrrUrno = frmData.TabDRR.GetColumnValue(i, 0)

            ' No
            ilInsertedShifts = ilInsertedShifts + 1
            strLine = CStr(ilInsertedShifts) & ".,"

            ' shift data
            strLine = strLine & frmData.GetShiftData(strDrrUrno)

            ' insert the line and keep the job-urno in mind
            TAB_ShiftInformation.InsertTextLine strLine, False
            TAB_ShiftInformation.SetLineTag ilInsertedShifts - 1, strDrrUrno

            ' get the shift day for the absences
            strDrrSday1 = frmData.TabDRR.GetFieldValue(i, "SDAY")

            ' Insert daily roster absences
            ilIdx = GetRealItemNo(frmData.TabDRA.LogicalFieldList, "SDAY")
            strLines = frmData.TabDRA.GetLinesByColumnValue(ilIdx, strDrrSday1, 0)
            ilLineCount = ItemCount(strLines, ",")
            For j = 0 To ilLineCount - 1 Step 1
                strLineNo = GetRealItem(strLines, CLng(j), ",")
                If IsNumeric(strLineNo) = True Then
                    ' getting the shift urno
                    strDraUrno = frmData.TabDRA.GetColumnValue(strLineNo, 0)
    
                    ' No
                    ilInsertedShifts = ilInsertedShifts + 1
                    strLine = CStr(ilInsertedShifts) & ".,"
    
                    ' absence data
                    strLine = strLine & frmData.GetAbsenceData(strDraUrno)
    
                    ' insert the line and keep the absence-urno in mind
                    TAB_ShiftInformation.InsertTextLine strLine, False
                    TAB_ShiftInformation.SetLineTag ilInsertedShifts - 1, strDraUrno
                End If
            Next j
        End If
        
    Next i

    'color actual day
    Dim strNow As String

    strNow = Format(Now, "DD-MM-YYYY")
    ilIdx = GetRealItemNo(TAB_ShiftInformation.LogicalFieldList, "SDAY")
    strLines = TAB_ShiftInformation.GetLinesByColumnValue(ilIdx, strNow, 0)
    ilLineCount = ItemCount(strLines, ",")
    For j = 0 To ilLineCount - 1 Step 1
        strLineNo = GetRealItem(strLines, CLng(j), ",")
        If IsNumeric(strLineNo) = True Then
            TAB_ShiftInformation.SetLineColor CLng(strLineNo), vbBlack, RGB(125, 255, 125)
        End If
    Next j
    
    ' color the recall days
    frmData.TabTemp.ResetContent
    frmData.TabTemp.InsertBuffer frmData.GetRecallDays, ","
    For i = 0 To frmData.TabTemp.GetLineCount - 1 Step 1
        strLineNo = TAB_ShiftInformation.GetLinesByColumnValue(ilIdx, frmData.TabTemp.GetColumnValue(i, 0), 0)
        If IsNumeric(strLineNo) = True Then
            For j = 2 To 6 Step 1
                TAB_ShiftInformation.SetDecorationObject CLng(strLineNo), CLng(j), "CHANGE"
                TAB_ShiftInformation.SetLineStatusValue CLng(strLineNo), 1
            Next j
        End If
    Next i
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    End
End Sub

Private Sub lblSTFTAB_Click(Index As Integer)
    Clipboard.Clear
    Clipboard.SetText lblSTFTAB(Index).Caption
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
    If SSTab1.Tab = 0 Then
        cmdTelexViewer.Enabled = True
        cmdStatus.Enabled = True
    Else
        cmdTelexViewer.Enabled = False
        cmdStatus.Enabled = False
    End If
End Sub

Private Sub TAB_JobInformation_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    If Selected = True Then
        Dim strStatus As String
        Dim ilIdx As Integer

        ' setting the remark text
        lblTEXT.Caption = frmData.GetJobFlightRemark(TAB_JobInformation.GetLineTag(LineNo))

        ' setting the caption of the status button
        ilIdx = GetRealItemNo(TAB_JobInformation.LogicalFieldList, "Status")
        If ilIdx > -1 Then
            If frmData.IsAllowedToChangeJobStatus(TAB_JobInformation.GetLineTag(LineNo)) = True Then
                strStatus = TAB_JobInformation.GetColumnValue(LineNo, ilIdx)
                If strStatus = "Finished" Then
                    cmdStatus.Caption = "Job Finished"
                    cmdStatus.Enabled = False
                ElseIf strStatus = "Informed" Then
                    cmdStatus.Caption = "Confirm Job"
                    cmdStatus.Enabled = True
                ElseIf strStatus = "Confirmed" Then
                    cmdStatus.Caption = "Finish Job"
                    cmdStatus.Enabled = True
                ElseIf strStatus = "Planned" Then
                    cmdStatus.Caption = "Informed"
                    cmdStatus.Enabled = True
                End If
            Else
                cmdStatus.Caption = "Not Allowed"
                cmdStatus.Enabled = False
            End If
        End If
    End If
End Sub

Private Sub TAB_JobInformation_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo > -1 Then
        Dim strPrivRet As String
        strPrivRet = frmSplash.AATLoginControl1.GetPrivileges("Z_JOBDETAIL")
        If strPrivRet <> "0" And strPrivRet <> "-" Then
            frmJobDetails.ShowJobDetails
        End If
    End If
End Sub

Private Sub InsertEmployeeInformation()
    Dim i As Integer
    Dim strTmp As String
    For i = 0 To lblSTFTAB.count - 1
        strTmp = frmData.TabSTF.GetFieldValue(0, lblSTFTAB.Item(i).Tag)
        strTmp = CleanString(strTmp, FOR_CLIENT, True)
        lblSTFTAB.Item(i).Caption = strTmp
    Next i
    lblFCTC.Caption = frmData.strFunction
End Sub

Private Sub InsertShiftTimes()
    lblShiftTimes.Caption = frmData.GetActualShiftTime
End Sub

Private Sub ColorLine(lLineNo As Long)
    Dim strParameter As String
    Dim ilIdx As Integer
    Dim strCfg As String
    Dim llColor As Long

    strParameter = "COLOR_" & UCase(TAB_JobInformation.GetFieldValue(lLineNo, "Status"))
    If (frmData.GetConfigEntry(strParameter, frmData.strOrgUnit, strCfg) = True) Then
        llColor = RGB(GetItem(strCfg, 1, ","), GetItem(strCfg, 2, ","), GetItem(strCfg, 3, ","))
        TAB_JobInformation.SetLineColor lLineNo, vbBlack, llColor
    End If
End Sub


Private Sub SetSecurity()
    Dim strPrivRet As String

    strPrivRet = frmSplash.AATLoginControl1.GetPrivileges("B_STATUS")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        cmdStatus.Visible = False
    End If

    strPrivRet = frmSplash.AATLoginControl1.GetPrivileges("B_TELEX")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        cmdTelexViewer.Visible = False
    End If

    strPrivRet = frmSplash.AATLoginControl1.GetPrivileges("B_PRINT")
    If strPrivRet = "-" Then
        cmdPrint.Visible = False
    ElseIf strPrivRet = "0" Then
        cmdPrint.Enabled = False
    End If

    strPrivRet = frmSplash.AATLoginControl1.GetPrivileges("Z_JOBS")
    If strPrivRet = "-" Then
        SSTab1.TabVisible(0) = False
    ElseIf strPrivRet = "0" Then
        SSTab1.TabEnabled(0) = False
    End If

    strPrivRet = frmSplash.AATLoginControl1.GetPrivileges("Z_SHIFTS")
    If strPrivRet = "-" Then
        SSTab1.TabVisible(1) = False
    ElseIf strPrivRet = "0" Then
        SSTab1.TabEnabled(1) = False
    End If
End Sub
