Attribute VB_Name = "UfisLib"
Option Explicit
Public bLDAPStatus As Boolean
Public strServer As String
Public strHOPO As String
Public strTableExt  As String
Public readFromFile As Boolean
Public strAppl As String
Public strAddiFeature As String
Public strDecisionOption As String
Public pstrShowYellowLines As String 'igu on 25 Jan 2010
Public pblnCopyToSpecialRequirements As Boolean 'igu on 27/06/2011
Public strAllFtyps As String
Public strShowHiddenData As String 'for frmData
Public bgfrmCompressedFlightsOpen As Boolean
Public sGlobalDateFrom As String
Public sGlobalDateTo As String
Public sGlobalTimeFrom As String
Public sGlobalTimeTo As String
Public bRefresh As Boolean

Public strCurrentUser As String
Public strPaxDefault As String
Public strMailDefault As String
Public strCargoDefault As String
Public strAftTimeFields As String ' to be used for UTC to local convertation
Public strTimeFrameFrom As String
Public strTimeFrameTo As String
Public TimesInUTC As Boolean ' True if UTC, false if local time
Public UtcOffset As Integer
Public sFilterName As String 'To Keep the current filter name
Public iFilterIdex As Integer ' To keep the index of current filter
Public sSHOW_FILTER_BUTTON As String 'To show filter buttons (Filter, Reload, Refresh) at the Connex Chart form
Public CreateGanttOnDemandOnly As Boolean
Public AllowOpenCappuccino As Boolean
Public ApplMainPurpose As String
Public ShowNetWorkCheck As Boolean
Public ApplIsHidden As Boolean
Public ApplIsInDesignMode As Boolean
Public MouseOverObjCode As String
Public MouseIsOverTabOcx As Boolean
Public MouseOverTabOcx As TABLib.Tab
Public ActDspRotRkey As String
Public ActDspArrUrno As String
Public ActDspDepUrno As String

Public UfisImbisIsActive As Boolean
Public MaxScreenWidth As Long
Public CurScreenWidth As Long
Public MinScreenTopPos As Long
Public TxtShapeRegnColor1 As Long
Public TxtShapeRegnColor2 As Long
Public TxtShapeBaysColor1 As Long
Public TxtShapeBaysColor2 As Long
Public TxtShapeFtypColor1 As Long
Public TxtShapeFtypColor2 As Long
Public InvertBackGroundText As Boolean

Public MainFocusIndicator As String
Public MainFocusIndex As Integer
Public MainFocusObject As TABLib.Tab
Public MainFocusActive As Boolean
Public FdsKeepAllLinks As Boolean
Public SrcServerColor As Long
Public TgtServerColor As Long
Public CmdLineText As String
Public ApplGotCfgFromFile As Boolean
Public ApplGotCmdLineData As Boolean
Public ApplGlobalDataValues As String
Public FilterFileName As String
Public RestoreFilter As Boolean
Public FilterActionType As Integer
Public FilterAutoReload As Integer
Public FilterAutoLoadNext As Date
Public FilterAutoReset As Boolean
Public FilterAutoLoadIsBusy As Boolean
Public ChartAutoSave As Integer
Public ChartAutoSaveNext As Date
Public ChartAutoSaveInit As Boolean
Public ChartAutoSaveReset As Boolean
Public ChartAutoSaveIsBusy As Boolean
Public ApplStartupFile As String
Public ApplMainCode As String
Public ApplFuncCode As String
Public ApplShortCode As String
Public ApplModuleKey As String
Public DspModuleName As String

Public FileMainCode As String
Public FileFuncCode As String
Public FileShortCode As String
Public FileModuleKey As String
Public FdtInetDefAddr As String
Public FdtInetDefName As String
Public FdtInetUrlAddr As String
Public FdtInetUrlName As String
Public FdtCurUrlName As String
Public AodbReaderSection As String
Public ButtonSectionName As String

Public LoginUserName As String
Public LoginUserPass As String
Public MainLifeStyle As Boolean
Public GridLifeStyle As Boolean
Public EditMaskIsOpen As Boolean
Public ReadMaskIsOpen As Boolean
Public ReadMaskButton As VB.CheckBox
Public ReadMaskTab As TABLib.Tab
Public MyOwnButtonFace As Long
Public MyPrintLogo As String
Public ReadOnlyUser As Boolean
Public FipsIsConnected As Boolean
Public FipsShowsUtc As Boolean
Public ApplicationIsReadyForUse As Boolean
Public SectionUsingSyncCursor As Boolean
Public AodbCallInProgress As Boolean
Public StopAllLoops As Boolean
Public BookMarkSet1 As Long
Public BookMarkSet2 As Long
Public BookMarkTotal As Long
Public BookMarkButtonIdx As Integer
Public MyIntroPicture As String
Public MyIntroPictureNotFound As Boolean
Public ApplIsInTray As Boolean

Public GridChkBoxIdx As Integer

Public CurBcSeqNo As Long
Public TabArrFlightsIdx As Integer
Public TabArrBeltsIdx As Integer
Public TabArrTowingIdx As Integer
Public TabArrCnxPaxIdx As Integer
Public TabArrCnxBagIdx As Integer
Public TabArrCnxUldIdx As Integer
Public TabBasPstTabIdx As Integer
Public TabBasBltTabIdx As Integer
Public TabBlkPosTabIdx As Integer
Public TabBlkBltTabIdx As Integer
Public TabBasGateTabIdx As Integer
Public TabBasCkcTabIdx As Integer

Public TabArrFlightsTab As TABLib.Tab
Public TabArrBeltsTab As TABLib.Tab
Public TabArrTowingTab As TABLib.Tab
Public TabArrCnxPaxTab As TABLib.Tab
Public TabArrCnxBagTab As TABLib.Tab
Public TabArrCnxUldTab As TABLib.Tab
Public TabBasPstTabTab As TABLib.Tab
Public TabBasBltTabTab As TABLib.Tab
Public TabBasGateTabTab As TABLib.Tab 'For Gate Chart
Public TabBasCkcTabTab As TABLib.Tab 'For Check in counter Chart
Public TabBlkPosTabTab As TABLib.Tab
Public TabBlkBltTabTab As TABLib.Tab
Public TabBlkGateTabTab As TABLib.Tab 'For Gate Chart
Public TabBlkCkcTabTab As TABLib.Tab  'For Check In Counter Chart

Public TabDepFlightsIdx As Integer
Public TabDepCnxPaxIdx As Integer
Public TabDepCnxBagIdx As Integer
Public TabDepCnxUldIdx As Integer
Public TabDepFlightsTab As TABLib.Tab
Public TabDepCnxPaxTab As TABLib.Tab
Public TabDepCnxBagTab As TABLib.Tab
Public TabDepCnxUldTab As TABLib.Tab

Public TabRotFlightsTab As TABLib.Tab
Public TabCnxFlightsTab As TABLib.Tab
Public RotationActivated As Boolean
Public DataFilterResult As String
Public DataFilterVpfr As String
Public DataFilterVpto As String
Public SyncOriginator As String
Public StatusPicPath As String

Public MyDateInputFormat As String
Public MyTimeInputFormat As String

Public UfisProjectName As String
Public UfisServerFunction As String

Public PerformingServerCheck As Boolean
Public UfisBroadCastIsAvailable As Boolean
Public UfisBroadCastNotFound As Boolean
Public UfisBcProxyIsConnected As Boolean
Public UfisServerIsAvailable As Boolean
Public CdrhdlIsAvailable As Boolean
Public NetworkCheckSuspended As Boolean
Public NetWorkIsAvailable As Boolean
Public ApplWaitsForNetwork As Boolean
Public ApplWaitsForBcSync As Boolean
Public ApplWaitsForBcSequ As Boolean
Public ApplWaitsForWhat As Integer
Public ApplStartupType As Integer
Public ApplCanBeUsed As Boolean
Public ApplIsInPassiveMode As Boolean
Public ApplAutoLogin As Boolean
Public ApplLoginWasAuto As Boolean
Public UserMustLogIn As Boolean
Public UserIsLoggedIn As Boolean
Public UserWasLoggedIn As Boolean
Public UserLoginFailed As Boolean
Public UserReLoginProc As Boolean
Public UserReLoginStop As Boolean
Public AutoAodbFilterLoad As Boolean
Public AutoAodbFilterDefault As Boolean
Public AutoAodbFilterShow As Boolean
Public ListOfGridTableNames As String
Public ApplUsesUfisAppMng As Boolean

Public ApplIsFdsFile As Boolean
Public ApplIsFdsMain As Boolean
Public ApplIsFdsLoad As Boolean


Public MyMainForm As Form
Public MyTrayForm As Form
Public MyTrayMenu As String
Public MyMsgPosX As Long
Public MyMsgPosY As Long
Public MyTcpIpAdr As String

Public TmpFreeStrg1 As String
Public TmpFreeStrg2 As String

Public BcSpoolReleased As Boolean

Public IfStatusEnabled As Boolean
Public CurIfGrpIdx As Integer
Public CurIfPanIdx As Integer

Public FdsCompLoop As Boolean

'=======================================
'Defines for UFIS Data File Handling
'=======================================
Public Const UFIS_FILE_EXT = ".ufis"
Public Const UFIS_RECOVERY_EXT = ".rcv"
Public Const UFIS_SNAPSHOT_EXT = ".fds"
Public Const UFIS_SCENARIO_EXT = ".scn"
Public Const UFIS_ARCHIVE_EXT = ".arc"
Public Const UFIS_PERIOD_EXT = ".prd"

'=======================================
'Defines for internal ReturnCodes
'=======================================
Public Const RC_NOT_FOUND = -2       'There was no Result
Public Const RC_SUCCESS = True       'All OK
Public Const RC_FAIL = False         'Not OK

'=======================================
'Defines for CC = CallCeda() ReturnCodes
'=======================================
'CEDA Communication
Public Const CC_RC_ORA_ERROR = -12      'DB Transaction failed
Public Const CC_RC_PROC_ERROR = -11     'Ceda Process has Problems
Public Const CC_RC_COM_ERROR = -10      'Something wrong with the Communication
Public Const CC_RC_ACCESS_DENIED = -9   'Transaction or Login rejected
Public Const CC_RC_WRITE_DENIED = -8    'Transaction not permitted
Public Const CC_RC_LOST_CONNECTION = -7 'The Server died
Public Const CC_RC_PROC_CRASH = -6      'The Process died
Public Const CC_RC_NOT_CONNECTED = -5   'Could not connect to the Server
Public Const CC_RC_TIMEOUT = -4         'Got no answer from the Server
Public Const CC_RC_UNEXPECTED = -3      'Got a unexpected answer from the server
Public Const CC_RC_NOT_FOUND = -2       'There was no Result
Public Const CC_RC_QUE_WAIT = -1        'The transaction is put on the Queue
'Internal Errors
Public Const CC_RC_WRONG_BUFFER = -100

'=======================================
'Defines for general purposes
'=======================================
'CleanString()
Public Const INIT_FOR_CLIENT = -1
Public Const INIT_FOR_SERVER = -2
Public Const FOR_CLIENT = 1
Public Const FOR_SERVER = 2
Public Const SERVER_TO_CLIENT = 3
Public Const CLIENT_TO_SERVER = 4

Public Const FOR_INSERT = 1
Public Const FOR_UPDATE = 2
Public Const FOR_DELETE = 3
Public Const FOR_SELECT = 4

Public Const FOR_ALL = 0
Public Const FOR_SEND = -1
Public Const FOR_RECV = -2

'=======================================
'Defines for Windows NT Features
'=======================================
Public Const VER_PLATFORM_WIN32s = 0
Public Const VER_PLATFORM_WIN32_WINDOWS = 1
Public Const VER_PLATFORM_WIN32_NT = 2

Public Const WF_CPU286 = &H2&
Public Const WF_CPU386 = &H4&
Public Const WF_CPU486 = &H8&
Public Const WF_STANDARD = &H10&
Public Const WF_ENHANCED = &H20&
Public Const WF_80x87 = &H400&

Public Const SM_MOUSEPRESENT = 19

Public Const GFSR_SYSTEMRESOURCES = &H0
Public Const GFSR_GDIRESOURCES = &H1
Public Const GFSR_USERRESOURCES = &H2

Public Const MF_POPUP = &H10
Public Const MF_BYPOSITION = &H400
Public Const MF_SEPARATOR = &H800

Public Const SRCCOPY = &HCC0020
Public Const SRCERASE = &H440328
Public Const SRCINVERT = &H660046
Public Const SRCAND = &H8800C6

Public Const HWND_TOPMOST = -1
Public Const HWND_NOTOPMOST = -2
Public Const SWP_NOACTIVATE = &H10
Public Const SWP_SHOWWINDOW = &H40
 
Public Const TWIPS = 1
Public Const pixels = 3
Public Const RES_INFO = 2
Public Const MINIMIZED = 1

Const MAX_COMPUTERNAME_LENGTH = 15

'=======================================
'Windows NT Structures
'=======================================

Type MYVERSION
    lMajorVersion As Long
    lMinorVersion As Long
    lExtraInfo As Long
End Type

Type OSVERSIONINFO
        dwOSVersionInfoSize As Long
        dwMajorVersion As Long
        dwMinorVersion As Long
        dwBuildNumber As Long
        dwPlatformId As Long
        szCSDVersion As String * 128      '  Maintenance string for PSS usage
End Type

Type RECT
    Left As Integer
    Top As Integer
    Right As Integer
    Bottom As Integer
End Type

Public Type SystemInfo
    dwOemId As Long
    dwPageSize As Long
    lpMinimumApplicationAddress As Long
    lpMaximumApplicationAddress As Long
    dwActiveProcessorMask As Long
    dwNumberOfProcessors As Long
    dwProcessorType As Long
    dwAllocationGranularity As Long
    dwReserved As Long
End Type

Public Type MEMORYSTATUS
    dwLength As Long
    dwMemoryLoad As Long
    dwTotalPhys As Long
    dwAvailPhys As Long
    dwTotalPageFile As Long
    dwAvailPageFile As Long
    dwTotalVirtual As Long
    dwAvailVirtual As Long
End Type

' This code was adapted from original system tray module published by Ben Baird.
' Tray Icon add/remove functions implemented within this module.
' Created by E.Spencer (elliot@spnc.demon.co.uk) - This code is public domain.
' Added call back to handle mouse events correctly

Public Declare Function SetForegroundWindow Lib "user32" (ByVal hWnd As Long) As Long
Declare Function Shell_NotifyIcon Lib "shell32.dll" Alias "Shell_NotifyIconA" (ByVal dwMessage As Long, lpData As NOTIFYICONDATA) As Long
Declare Function CallWindowProc Lib "user32" Alias "CallWindowProcA" (ByVal lpPrevWndFunc As Long, _
   ByVal hWnd As Long, ByVal Msg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Public Const WM_RBUTTONDOWN = &H204
Public Const WM_RBUTTONUP = &H205
Public Const WM_ACTIVATEAPP = &H1C
Public Const NIF_ICON = &H2
Public Const NIF_MESSAGE = &H1
Public Const NIF_TIP = &H4
Public Const NIM_ADD = &H0
Public Const NIM_MODIFY = &H1
Public Const NIM_DELETE = &H2
Public Const MAX_TOOLTIP As Integer = 64
Public Const GWL_WNDPROC = (-4)

Type NOTIFYICONDATA
   cbSize As Long
   hWnd As Long
   uID As Long
   uFlags As Long
   uCallbackMessage As Long
   hIcon As Long
   szTip As String * MAX_TOOLTIP
End Type
Public nfIconData As NOTIFYICONDATA
Private FHandle As Long     ' Storage for form handle
Private WndProc As Long     ' Address of our handler
Private Hooking As Boolean  ' Hooking indicator

'=======================================
'Application Defines
'=======================================
'Public Const DEFAULT_CEDA_INI = "C:\UFIS\SYSTEM\CEDA.INI"
Public DEFAULT_CEDA_INI As String
Public UFIS_SYSTEM As String
Public UFIS_HELP As String
Public UFIS_APPL As String
Public UFIS_TMP As String
Public UFIS_UFIS As String
Public myIniPath As String
Public myIniFile As String
Public myIniFullName As String
Public myIniSection As String
'=======================================
'Keyboard Status Flags
'=======================================
Public KeybdFlags As Integer
Public KeybdState As String
Public KeybdIsCtrl As Boolean
Public KeybdIsShift As Boolean
Public KeybdIsAlt As Boolean

'=======================================
'Application Structures
'=======================================
Public Type MaxFrame        'Using Twips
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

'=======================================
'Coordination of Pending Events
'=======================================
Public Const RC_NONE = 0
Public Const RC_PENDING = 1
Public Const RC_IGNORED = 2
Public Const RC_NEWFOCUS = 3
Public Const RELEASE = 0

Public Type PendEvent
    IsActive As Boolean
    Type As Integer
    Form As Form
    Object As Control
End Type
Public PendingEvent As PendEvent


'=======================================
'Windows NT Function Prototypes
'=======================================
Public Const HELP_CONTEXT = &H1
Public Const HELP_QUIT = &H2
Public Const HELP_INDEX = &H3
Public Const HELP_HELPONHELP = &H4
Public Const HELP_SETINDEX = &H5
Public Const HELP_KEY = &H101
Public Const HELP_MULTIKEY = &H201
'Public Const HELP_FILE = "GanttHelp.hlp"

Declare Function WinHelp Lib "user32" Alias "WinHelpA" (ByVal hWnd As Long, ByVal lpHelpFile As String, ByVal wCommand As Long, ByVal dwData As Long) As Long
Declare Function GetWindowsDirectory Lib "kernel32" Alias "GetWindowsDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long
Declare Sub GetSystemInfo Lib "kernel32" (lpSystemInfo As SystemInfo)
Declare Sub GlobalMemoryStatus Lib "kernel32" (lpBuffer As MEMORYSTATUS)
Declare Function GetVersionEx Lib "kernel32" Alias "GetVersionExA" (ByRef lpVersionInformation As OSVERSIONINFO) As Long
Declare Function GetSystemMetrics Lib "user32" (ByVal nIndex As Long) As Long
Declare Function GetDeviceCaps Lib "gdi32" (ByVal hDC As Long, ByVal nIndex As Long) As Long
Declare Function TrackPopupMenu Lib "user32" (ByVal hMenu As Long, ByVal wFlags As Long, ByVal X As Long, ByVal Y As Long, ByVal nReserved As Long, ByVal hWnd As Long, lpReserved As Any) As Long
Declare Function GetMenu Lib "user32" (ByVal hWnd As Long) As Long
Declare Function GetSubMenu Lib "user32" (ByVal hMenu As Long, ByVal nPos As Long) As Long
Declare Function GetDesktopWindow Lib "user32" () As Long
Declare Function GetDC Lib "user32" (ByVal hWnd As Long) As Long
Declare Function ReleaseDC Lib "user32" (ByVal hWnd As Long, ByVal hDC As Long) As Long
Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Long, ByVal X As Long, ByVal Y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal XSrc As Long, ByVal YSrc As Long, ByVal dwRop As Long) As Long
Declare Sub SetWindowPos Lib "user32" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long)
'Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationname As String, lpKeyName As Any, ByVal lpDefault As String, ByVal lpRetunedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Declare Function GetProfileString Lib "kernel32" Alias "GetProfileStringA" (ByVal lpAppName As String, lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long) As Long
Declare Function waveOutGetNumDevs Lib "winmm" () As Long
Declare Function GetSystemDirectory Lib "kernel32" Alias "GetSystemDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long
Declare Function sndPlaySound Lib "winmm" Alias "sndPlaySoundA" (ByVal lpszSoundName As String, ByVal uFlags As Long) As Long
Declare Function FlashWindow Lib "user32" (ByVal hWnd As Long, ByVal bInvert As Long) As Long
Public Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationname As String, ByVal lpKeyName As Any, ByVal lsString As Any, ByVal lplFilename As String) As Long
Public Declare Function GetPrivateProfileInt Lib "kernel32" Alias "GetPriviteProfileIntA" (ByVal lpApplicationname As String, ByVal lpKeyName As String, ByVal nDefault As Long, ByVal lpFileName As String) As Long
Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationname As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Private Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Private Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Private Declare Function HtmlHelpTopic Lib "hhctrl.ocx" Alias _
    "HtmlHelpA" (ByVal hWnd As Long, ByVal lpHelpFile As String, _
    ByVal wCommand As Long, ByVal dwData As Long) As Long

Public Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As Long) As Integer

Public VisibleFrame As Frame
Public CurCedaOcx As Integer
Public CurCedaIdx As Integer
Public CedaIniFile As String
Public HomeAirport As String
Public UtcTimeDiff As Integer
Public UtcTimeDisp As Integer
Public UtcTimeMain As Integer
Public TimeDisplayMode As String
Public CurrentTimeCode As String
Public CurrentTimeZone As String

Public ApplicationIsStarted As Boolean
Public ApplicationIsBusy As Boolean
Public CedaIsConnected As Boolean
Public ServerIsAvailable As Boolean
Public ModalMsgIsOpen As Boolean
Public ShutDownRequested As Boolean
Public AutoArrange As Boolean
Public ErrorIgnored As Boolean
Public UserAnswer As String
Public CedaDataAnswer As String
Public CurMem As Integer
Public SuspendErrMsg As Boolean
Public StopNestedCalls As Boolean
Public InitialMaskDisplay As Boolean
Public HtmHelpIsOpen As Boolean
Public HtmHelpHwnd As Long

Dim WinVersion As Integer
Dim SoundAvailable As Integer

Public Const MonthList = "JAN,FEB,MAR,APR,MAY,JUN,JUL,AUG,SEP,OCT,NOV,DEC"
'=======================================
'Special Color Values
'=======================================
'-------------------------------------
'YELLOW
'-------------------------------------
Public Const LightestYellow = &HC0FFFF
Public Const LightYellow = &H80FFFF
Public Const NormalYellow = &HFFFF&
Public Const DarkYellow = &HC0C0&
'public const DarkerYellow = &H0000C0C0&
Public Const DarkestYellow = &H8080&
'-------------------------------------
'GREEN
'-------------------------------------
Public Const NearlyGreen = &HC0FFC0
Public Const LightestGreen = &HC0FFC0
Public Const LightGreen = &H80FF80
Public Const NormalGreen = &HC000&
Public Const DarkGreen = &H8000&
Public Const DarkestGreen = &H4000&
'-------------------------------------
'RED
'-------------------------------------
Public Const LightestRed = &HC0C0FF
Public Const LightRed = &H8080FF

Public Const LightOcre = &HC0E0FF
'-------------------------------------
'GRAY (GREY)
'-------------------------------------
Public Const LightGray = &HE0E0E0
Public Const NormalGray = &HC0C0C0
Public Const DarkGray = &H808080
Public Const LightGrey = &HE0E0E0
Public Const NormalGrey = &HC0C0C0
Public Const DarkGrey = &H808080
'-------------------------------------
'BLUE
'-------------------------------------
Public Const LightestBlue = &HFFC0C0
Public Const LightBlue = &HFF8080
Public Const NormalBlue = &HFF0000
Public Const DarkBlue = &HC00000
Public Const DarkestBlue = &H800000

Private Declare Function CreateFont Lib "gdi32" Alias "CreateFontA" (ByVal h As Long, ByVal W As Long, ByVal e As Long, ByVal O As Long, ByVal W As Long, ByVal i As Long, ByVal u As Long, ByVal s As Long, ByVal c As Long, ByVal OP As Long, ByVal CP As Long, ByVal Q As Long, ByVal PAF As Long, ByVal f As String) As Long
Private Declare Function SelectObject Lib "gdi32" (ByVal hDC As Long, ByVal hObject As Long) As Long
Private Declare Function DeleteObject Lib "gdi32" (ByVal hObject As Long) As Long


Public Type POINTAPI
    X As Long
    Y As Long
End Type
Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Public Declare Function ScreenToClient Lib "user32" (ByVal hWnd As Long, _
    lpPoint As POINTAPI) As Long




Public Function GetIniEntry(cpFileName As String, cpSection1 As String, cpSection2 As String, cpKeyWord As String, cpDefault As String) As String
    Static CurFile As String
    Static FData As String
    Static FSize As Long
    Dim IniFileName As String
    Dim clHeader As String
    Dim KLook As String
    Dim KSep As String
    Dim KSec As String
    Dim clResult As String
    Dim blLoopEnd As Boolean
    Dim blSectionFound As Boolean
    Dim blBlockFound As Boolean
    Dim ilPos As Integer
    Dim ilCount As Integer
    Dim FPos1 As Long
    Dim FPos2 As Long
    Dim KPos As Long
    Dim DPos As Long
    Dim DLen As Long
    Dim DEnd As Long

    On Error GoTo HandleError
    KSep = Chr$(15)
    Select Case cpKeyWord
        Case "{GET_FILE}"
            If DEFAULT_CEDA_INI = "" Then GetUfisDir
            IniFileName = cpFileName
            If IniFileName = "" Then IniFileName = DEFAULT_CEDA_INI
            If IniFileName <> CurFile Then
                Open IniFileName For Binary As #1
                FSize = LOF(1)
                FData = String$(FSize, 32)
                Get #1, , FData
                Close #1
                FData = FData & vbNewLine & "[END]"
                FData = Replace(FData, vbCr, "", 1, -1, vbBinaryCompare)
                FData = Replace(FData, vbLf, KSep, 1, -1, vbBinaryCompare)
                If Left(FData, 1) <> KSep Then FData = KSep & FData
                If Right(FData, 1) <> KSep Then FData = FData & KSep
                CurFile = IniFileName
                FSize = Len(FData)
            End If
            cpSection1 = CurFile
            cpSection2 = CStr(FSize)
            clResult = FData
        Case "{SET_FILE}"
            CurFile = cpFileName
            FData = cpDefault
            FData = Replace(FData, vbCr, "", 1, -1, vbBinaryCompare)
            FData = Replace(FData, vbLf, KSep, 1, -1, vbBinaryCompare)
            If Left(FData, 1) <> KSep Then FData = KSep & FData
            If Right(FData, 1) <> KSep Then FData = FData & KSep
            FSize = Len(FData)
            clResult = ""
        Case Else
            If DEFAULT_CEDA_INI = "" Then GetUfisDir
            IniFileName = cpFileName
            If IniFileName = "" Then IniFileName = DEFAULT_CEDA_INI
            clResult = ""
            KSep = Chr$(15)
            KSec = KSep & "["
            blLoopEnd = False
            blBlockFound = False
            ilCount = 0
            Do While (Not blLoopEnd) And (ilCount < 2)
                ilCount = ilCount + 1
                If ilCount = 1 Then
                    clHeader = "[" & cpSection1 & "]"
                Else
                    clHeader = "[" & cpSection2 & "]"
                End If
                If clHeader <> "[]" Then
                    blSectionFound = False
                    If IniFileName <> CurFile Then
                        Open IniFileName For Binary As #1
                        FSize = LOF(1)
                        FData = String$(FSize, 32)
                        Get #1, , FData
                        Close #1
                        FData = FData & vbNewLine & "[END]"
                        FData = Replace(FData, vbCr, "", 1, -1, vbBinaryCompare)
                        FData = Replace(FData, vbLf, KSep, 1, -1, vbBinaryCompare)
                        FData = KSep & FData & KSep
                        CurFile = IniFileName
                    End If
                    KPos = 0
                    clHeader = KSep & clHeader
                    KLook = Chr(15) & cpKeyWord
                    FPos1 = InStr(FData, clHeader)
                    If FPos1 > 0 Then
                        FPos2 = FPos1 + 1
                        FPos2 = InStr(FPos2, FData, KSec)
                        KPos = InStr(FPos1, FData, KLook)
                        If KPos >= FPos2 Then KPos = 0
                    End If
                    If KPos > 0 Then
                        DPos = InStr(KPos, FData, "=") + 1
                        DEnd = InStr(DPos, FData, KSep)
                        DLen = DEnd - DPos
                        If DLen > 0 Then
                            clResult = Trim(Mid$(FData, DPos, DLen))
                            If clResult <> "BLOCK_BEGIN" Then
                                blLoopEnd = True
                            Else
                                'Old feature not yet supported
                                blBlockFound = True
                                clResult = ""
                            End If
                        End If
                    End If
                End If
                If clResult = "" Then
                    blLoopEnd = False
                End If
            Loop
            If clResult = "" Then
                clResult = cpDefault
            End If
            If cpKeyWord = "?EXIST?" Then
                If blSectionFound Then
                    clResult = "YES"
                Else
                    clResult = "NO"
                End If
            End If
    End Select
    GetIniEntry = clResult
    Exit Function
HandleError:
    Select Case Err.Number
        Case 53
            If SuspendErrMsg = False Then
                MsgBox "File not found:" & vbNewLine & IniFileName & vbNewLine & "For: " & cpSection1 & "/" & cpKeyWord
            End If
            GetIniEntry = cpDefault
        Case Else
            Resume Next
    End Select
End Function


Public Function GetIniEntryX(cpFileName As String, cpSection1 As String, cpSection2 As String, cpKeyWord As String, cpDefault As String) As String
Dim IniFileName As String
Dim TextLine As String
Dim clHeader As String
Dim clResult As String
Dim blLoopEnd As Boolean
Dim blSectionFound As Boolean
Dim blBlockFound As Boolean
Dim ilPos As Integer
Dim ilCount As Integer
    On Error GoTo HandleError
    If DEFAULT_CEDA_INI = "" Then GetUfisDir
    IniFileName = cpFileName
    If IniFileName = "" Then IniFileName = DEFAULT_CEDA_INI
    clResult = ""
    blLoopEnd = False
    blBlockFound = False
    ilCount = 0
    Do While (Not blLoopEnd) And (ilCount < 2)
        ilCount = ilCount + 1
        If ilCount = 1 Then
            clHeader = "[" & cpSection1 & "]"
        Else
            clHeader = "[" & cpSection2 & "]"
        End If
        If clHeader <> "[]" Then
            blSectionFound = False
            Open IniFileName For Input As #1
            Do While (Not EOF(1)) And (Not blLoopEnd)
                Line Input #1, TextLine
                TextLine = LTrim(TextLine)
                If InStr(TextLine, "[") = 1 Then 'New Section
                    If Not blSectionFound Then
                        If InStr(TextLine, clHeader) = 1 Then
                            blSectionFound = True 'Given Section found
                        End If
                    Else
                        blLoopEnd = True
                    End If
                Else
                    If blSectionFound Then
                        If blBlockFound = True Then
                            If InStr(TextLine, "BLOCK_END") = 1 Then
                                blLoopEnd = True
                            Else
                                clResult = clResult & TextLine & vbNewLine
                            End If
                        Else
                            If InStr(TextLine, cpKeyWord) = 1 Then
                                'KeyWord found
                                ilPos = InStr(TextLine, "=")
                                If ilPos > 0 Then
                                    clResult = Trim(Mid$(TextLine, ilPos + 1))
                                    If clResult = "BLOCK_BEGIN" Then
                                        blBlockFound = True
                                        clResult = ""
                                    Else
                                        blLoopEnd = True
                                    End If
                                Else
                                    'Nothing, we search until the end of section
                                End If
                            End If
                        End If
                    End If
                End If
            Loop
            Close #1
        End If
        If clResult = "" Then
            blLoopEnd = False
        End If
    Loop
    If clResult = "" Then
        clResult = cpDefault
    End If
    If cpKeyWord = "?EXIST?" Then
        If blSectionFound Then
            clResult = "YES"
        Else
            clResult = "NO"
        End If
    End If
    GetIniEntryX = clResult
    Exit Function
HandleError:
    Select Case Err.Number
        Case 53
            If SuspendErrMsg = False Then
                MsgBox "File not found:" & vbNewLine & IniFileName & vbNewLine & "For: " & cpSection1 & "/" & cpKeyWord
            End If
            GetIniEntryX = cpDefault
        Case Else
            Resume Next
    End Select
End Function

Public Function GetItem(cpTxt As String, ipNbr As Integer, cpSep As String) As String
 Dim Result
 Dim ilSepLen As Integer
 Dim ilFirstPos As Integer
 Dim ilLastPos As Integer
 Dim ilItmLen As Integer
 Dim ilItmNbr As Integer
    Result = ""
    If ipNbr > 0 Then
        ilSepLen = Len(cpSep)
        ilFirstPos = 1
        ilLastPos = 1
        ilItmNbr = 0
        While (ilItmNbr < ipNbr) And (ilLastPos > 0)
            ilItmNbr = ilItmNbr + 1
            ilLastPos = InStr(ilFirstPos, cpTxt, cpSep)
            If (ilItmNbr < ipNbr) And (ilLastPos > 0) Then ilFirstPos = ilLastPos + ilSepLen
        Wend
        If ilLastPos < ilFirstPos Then ilLastPos = Len(cpTxt) + 1
        If (ilItmNbr = ipNbr) And (ilLastPos > ilFirstPos) Then
            ilItmLen = ilLastPos - ilFirstPos
            Result = Mid(cpTxt, ilFirstPos, ilItmLen)
        End If
    End If
    GetItem = RTrim(Result)
End Function
'------------------------------------------------------------------------------------
' Same as GetItem but zero based item no and using long values
'------------------------------------------------------------------------------------
Function GetRealItem(cpTxt As String, lpNbr As Long, cpSep As String) As String
    Dim Result
    Dim llSepLen As Long
    Dim llFirstPos As Long
    Dim llLastPos As Long
    Dim llItmLen As Long
    Dim llItmNbr As Long
    
    Result = ""
    If lpNbr >= 0 Then
        llSepLen = Len(cpSep)
        llFirstPos = 1
        llLastPos = 1
        llItmNbr = -1
        While (llItmNbr < lpNbr) And (llLastPos > 0)
            llItmNbr = llItmNbr + 1
            llLastPos = InStr(llFirstPos, cpTxt, cpSep)
            If (llItmNbr < lpNbr) And (llLastPos > 0) Then llFirstPos = llLastPos + llSepLen
        Wend
        If llLastPos < llFirstPos Then llLastPos = Len(cpTxt) + 1
        If (llItmNbr = lpNbr) And (llLastPos > llFirstPos) Then
            llItmLen = llLastPos - llFirstPos
            Result = Mid(cpTxt, llFirstPos, llItmLen)
        End If
    End If
    GetRealItem = RTrim(Result)
End Function
'-------------------------------------------------------------------------
' Returns the item number but also for an itemlist with variable field len
' For the first item "0" will be returned
'-------------------------------------------------------------------------
Function GetRealItemNo(ItemList As String, ItemValue As String) As Integer
    Dim cnt As Integer
    Dim i As Integer
    Dim blFound As Boolean
    Dim strItemList As String
    Dim strVal As String
    Dim pos As Integer
    
    strItemList = "," + ItemList + ","
    strVal = "," + ItemValue + ","
    
    pos = InStr(strItemList, strVal)
    If pos > 0 Then
        If pos = 1 Then
            GetRealItemNo = 0
        Else
            GetRealItemNo = ItemCount(Left(strItemList, pos), ",") - 2
        End If
    Else
        GetRealItemNo = -1
    End If
End Function

Public Function GetItemNo(ItemList As String, ItemValue As String) As Integer
    Dim ItmLen As Integer
    Dim ItmPos As Integer
    ItmLen = Len(ItemValue) + 1
    ItmPos = InStr(ItemList, ItemValue)
    If ItmPos > 0 Then
        GetItemNo = ((ItmPos - 1) \ ItmLen) + 1
    Else
        GetItemNo = -1
    End If
End Function

Public Function GetFieldValue(FieldName As String, DataList As String, FieldList As String) As String
    Dim ItmNbr As Integer
    ItmNbr = GetItemNo(FieldList, FieldName)
    GetFieldValue = GetItem(DataList, ItmNbr, ",")
End Function
Public Function CedaDateAdd(cpDate As String, ipDays As Integer) As String
    Dim tmpDate
        tmpDate = CedaDateToVb(cpDate)
        tmpDate = DateAdd("d", ipDays, tmpDate)
        CedaDateAdd = Format(tmpDate, "yyyymmdd")
End Function
Public Function CedaDateDiff(CurDate1 As String, CurDate2 As String) As Long
    Dim tmpDate1
    Dim tmpDate2
    If (CurDate1 <> "") And (CurDate2 <> "") Then
        tmpDate1 = CedaDateToVb(CurDate1)
        tmpDate2 = CedaDateToVb(CurDate2)
        CedaDateDiff = DateDiff("d", tmpDate1, tmpDate2)
    Else
        CedaDateDiff = 0
    End If
End Function
Public Function CedaDateToVb(cpDate As String) 'as Variant
    If Trim(cpDate) <> "" Then
        CedaDateToVb = DateSerial(Val(Mid(cpDate, 1, 4)), Val(Mid(cpDate, 5, 2)), Val(Mid(cpDate, 7, 2)))
    Else
        CedaDateToVb = ""
    End If
End Function

Public Function CedaTimeToVb(cpDate As String) 'as Variant
Dim ilHH As Integer
Dim ilMM As Integer
Dim ilSS As Integer
    If Trim(cpDate) <> "" Then
        If Len(cpDate) > 8 Then
            CedaTimeToVb = TimeSerial(Val(Mid(cpDate, 9, 2)), Val(Mid(cpDate, 11, 2)), Val(Mid(cpDate, 13, 2)))
        Else
            If InStr(cpDate, ":") > 0 Then
                ilHH = Val(GetItem(cpDate, 1, ":"))
                ilMM = Val(GetItem(cpDate, 2, ":"))
                ilSS = Val(GetItem(cpDate, 3, ":"))
                CedaTimeToVb = TimeSerial(ilHH, ilMM, ilSS)
            Else
                CedaTimeToVb = TimeSerial(Val(Mid(cpDate, 1, 2)), Val(Mid(cpDate, 3, 2)), Val(Mid(cpDate, 5, 2)))
            End If
        End If
    Else
        CedaTimeToVb = ""
    End If
End Function

Public Function CedaFullDateToVb(cpDate As String) 'as Variant
Dim MyDate
Dim myTime
    MyDate = CedaDateToVb(cpDate)
    myTime = CedaTimeToVb(cpDate)
    If (MyDate <> "") And (myTime <> "") Then
        CedaFullDateToVb = MyDate + myTime
    Else
        CedaFullDateToVb = ""
    End If
End Function

Public Function VbDateStrgToCeda(cpDate As String, cpInForm As String) As String
Dim clResult As String
Dim UsedFormat As String
Dim clYear As String
Dim clMon As String
Dim clDay As String
Dim clHour As String
Dim clMin As String
Dim clSec As String
Dim ilInLen As Integer
Dim i As Integer
Dim clCod As String
Dim clChr As String
    clResult = ""
    clYear = ""
    clMon = ""
    clDay = ""
    clHour = ""
    clMin = ""
    clSec = ""
    ilInLen = Len(cpInForm)
    UsedFormat = cpInForm
    For i = 1 To ilInLen
        clCod = Mid(UsedFormat, i, 1)
        clChr = Mid(cpDate, i, 1)
        Select Case clCod
            Case "y"
                clYear = clYear & clChr
            Case "m"
                clMon = clMon & clChr
            Case "d"
                clDay = clDay & clChr
            Case Else
        End Select
    Next
    clResult = clYear & clMon & clDay
    VbDateStrgToCeda = clResult
End Function

Public Function DateInputFormatToCeda(cpDate As String, cpInForm As String) As String
Dim myCedaDate As String
Dim myChkDate As String
    myCedaDate = VbDateStrgToCeda(cpDate, cpInForm)
    myChkDate = Format(CedaDateToVb(myCedaDate), cpInForm)
    If myChkDate <> cpDate Then
        myCedaDate = ""
    End If
    DateInputFormatToCeda = myCedaDate
End Function

Function DeviceColors(hDC As Long) As Single
Const PLANES = 14
Const BITSPIXEL = 12
    DeviceColors = 2 ^ (GetDeviceCaps(hDC, PLANES) * GetDeviceCaps(hDC, BITSPIXEL))
End Function

Function GetSysIni(section, Key)
Dim retval As String, appName As String, worked As Integer
    retval = String$(255, 0)
    worked = GetPrivateProfileString(section, Key, "", retval, Len(retval), "System.ini")
    If worked = 0 Then
        GetSysIni = "unknown"
    Else
        GetSysIni = Left(retval, InStr(retval, Chr(0)) - 1)
    End If
End Function

Function GetWinIni(section, Key)
Dim retval As String, appName As String, worked As Integer
    retval = String$(255, 0)
    worked = GetProfileString(section, Key, "", retval, Len(retval))
    If worked = 0 Then
        GetWinIni = "unknown"
    Else
        GetWinIni = Left(retval, InStr(retval, Chr(0)) - 1)
    End If
End Function

Function SystemDirectory() As String
Dim WinPath As String
    WinPath = String(145, Chr(0))
    SystemDirectory = Left(WinPath, GetSystemDirectory(WinPath, 145))
End Function

Function WindowsDirectory() As String
Dim WinPath As String
    WinPath = String(145, Chr(0))
    WindowsDirectory = Left(WinPath, GetWindowsDirectory(WinPath, 145))
End Function

Function WindowsVersion() As MYVERSION
Dim myOS As OSVERSIONINFO, WinVer As MYVERSION
Dim lResult As Long

    myOS.dwOSVersionInfoSize = Len(myOS)    'should be 148
    
    lResult = GetVersionEx(myOS)
        
    'Fill user type with pertinent info
    WinVer.lMajorVersion = myOS.dwMajorVersion
    WinVer.lMinorVersion = myOS.dwMinorVersion
    WinVer.lExtraInfo = myOS.dwPlatformId
    
    WindowsVersion = WinVer

End Function

Public Sub SetFormOnTop(UsedForm As Form, OnTop As Boolean)
    If OnTop = True Then
       SetWindowPos UsedForm.hWnd, HWND_TOPMOST, UsedForm.Left / 15, _
                    UsedForm.Top / 15, UsedForm.Width / 15, _
                    UsedForm.Height / 15, SWP_NOACTIVATE Or SWP_SHOWWINDOW
    Else
       SetWindowPos UsedForm.hWnd, HWND_NOTOPMOST, UsedForm.Left / 15, _
                    UsedForm.Top / 15, UsedForm.Width / 15, _
                    UsedForm.Height / 15, SWP_NOACTIVATE Or SWP_SHOWWINDOW
    End If
End Sub

Public Function CountVisibleForms() As Integer
Dim i As Integer
Dim cnt As Integer
    cnt = 0
    For i = 0 To Forms.count - 1
        If Forms(i).Visible = True Then cnt = cnt + 1
    Next i
    CountVisibleForms = cnt
End Function
Public Function FormIsVisible(FormName As String) As Boolean
Dim i As Integer
Dim Answ As Boolean
    Answ = False
    For i = 0 To Forms.count - 1
        If (Forms(i).Name = FormName) And (Forms(i).Visible = True) Then
            Answ = True
            Exit For
        End If
    Next i
    FormIsVisible = Answ
End Function
Public Function FormIsLoaded(FormName As String) As Boolean
Dim i As Integer
Dim Answ As Boolean
    Answ = False
    For i = 0 To Forms.count - 1
        If Forms(i).Name = FormName Then
            Answ = True
            Exit For
        End If
    Next i
    FormIsLoaded = Answ
End Function



Public Function CheckDateField(UseObject As Control, UseFormat As String, SetError As Boolean)
    With UseObject
        .Tag = DateInputFormatToCeda(.Text, UseFormat)
        If .Tag = "" Then
            .BackColor = vbRed
            .ForeColor = vbWhite
            CheckDateField = False
            If SetError Then
                PendingEvent.IsActive = True
                Set PendingEvent.Form = Screen.ActiveForm
                Set PendingEvent.Object = UseObject
                PendingEvent.Type = 1
            End If
        Else
            .BackColor = vbWindowBackground
            .ForeColor = vbWindowText
            CheckDateField = True
            If SetError Then PendingEvent.IsActive = False
        End If
    End With
End Function

Public Sub DoNothing()
    'did nothing
End Sub

Public Function GetUrnoFromSqlKey(KeyText As String) As String
Dim Result As String
Dim tmpTxt As String
Dim tmpOffs As Long
    tmpTxt = Trim(KeyText)
    tmpOffs = InStr(tmpTxt, "URNO")
    If tmpOffs > 0 Then
        tmpOffs = tmpOffs + 4
        tmpTxt = LTrim(Mid(tmpTxt, tmpOffs))
        If Left(tmpTxt, 1) = "=" Then
            tmpTxt = LTrim(Mid(tmpTxt, 2))
        Else
            'may be like, <>, o.s.o.
        End If
        If Left(tmpTxt, 1) = "'" Then
            tmpTxt = GetItem(tmpTxt, 2, "'")
        End If
    Else
        'normally the URNO itself
    End If
    Result = GetItem(tmpTxt, 1, " ")
    GetUrnoFromSqlKey = Result
End Function
Public Function CleanNullValues(myString As String)
Dim Result As String
Dim tmpStrg As String
    Result = myString
    If Result = "" Then Result = " "
    Do
        tmpStrg = Result
        Result = Replace(tmpStrg, ",,", ", ,", 1, -1, vbBinaryCompare)
    Loop While Result <> tmpStrg
    If Left(Result, 1) = "," Then Result = " " & Result
    If Right(Result, 1) = "," Then Result = Result & " "
    CleanNullValues = Result
End Function
Public Function CleanString(myText As String, myMethod As Integer, CheckOldVersion As Boolean) As String
Dim Result As String
Static FunctionIsInitialized As Boolean
Static ClientPatchList As String
Static NewServerPatchList As String
Static OldServerPatchList As String
'Until SEP'99 we used ServerPatchValues up from ASCII(176),
'but because these values matched chinese (and greek) characters
'we now use values below ASCII(31).
'But because not all applications (FDIHDL) or installations are changed
'and using the new version of patch values, we here check both.
'This can be forced or suppressed by setting the CheckOldVersion parameter
    If Not FunctionIsInitialized Then
        ClientPatchList = BuildBinaryString("34,39,44,10,13,40,41")
        NewServerPatchList = BuildBinaryString("23,24,25,28,29,40,41")
        OldServerPatchList = BuildBinaryString("176,177,178,179,180,155,156")
        FunctionIsInitialized = True
    End If
    Select Case myMethod
        Case INIT_FOR_CLIENT
            ClientPatchList = BuildBinaryString(myText)
        Case INIT_FOR_SERVER
            If CheckOldVersion Then
                OldServerPatchList = BuildBinaryString(myText)
            Else
                NewServerPatchList = BuildBinaryString(myText)
            End If
        Case FOR_CLIENT, SERVER_TO_CLIENT
            Result = ReplaceChars(myText, NewServerPatchList, ClientPatchList)
            If (Result = myText) And CheckOldVersion Then
                'Nothing changed, so we try the old version
                Result = ReplaceChars(myText, OldServerPatchList, ClientPatchList)
            End If
            'VB needs CR/LF
            If myMethod = FOR_CLIENT Then Result = Replace(Result, vbLf, vbNewLine, 1, -1, vbBinaryCompare)
        Case FOR_SERVER, CLIENT_TO_SERVER
            'the server needs patched LF only
            Result = myText
            If myMethod = FOR_SERVER Then Result = Replace(myText, vbNewLine, vbLf, 1, -1, vbBinaryCompare)
            If CheckOldVersion Then
                Result = ReplaceChars(Result, ClientPatchList, OldServerPatchList)
            Else
                Result = ReplaceChars(Result, ClientPatchList, NewServerPatchList)
            End If
        Case Else
            Result = myText
    End Select
    CleanString = Result
End Function

Public Function BuildBinaryString(myAscList As String) As String
Dim Result As String
Dim ItmNbr As Integer
Dim ItmVal 'as variant
    Result = ""
    ItmNbr = 0
    Do
        ItmNbr = ItmNbr + 1
        ItmVal = GetItem(myAscList, ItmNbr, ",")
        If ItmVal <> "" Then
            Result = Result & Chr(ItmVal)
        End If
    Loop While ItmVal <> ""
    BuildBinaryString = Result
End Function
Public Function ReplaceChars(myText As String, LookFor As String, ReplaceWith As String) As String
Dim Result As String
Dim ilLen As Integer
Dim clLook As String
Dim clPatch As String
Dim i As Integer
    Result = myText
    ilLen = Len(LookFor)
    For i = 1 To ilLen
        clLook = Mid(LookFor, i, 1)
        clPatch = Mid(ReplaceWith, i, 1)
        Result = Replace(Result, clLook, clPatch, 1, -1, vbBinaryCompare)
    Next
    ReplaceChars = Result
End Function
Public Sub CheckFormOnTop(ActForm As Form, SetValue As Boolean)
    If ActForm.Visible = True Then
        If ActForm.OnTop.Value = 1 Then SetFormOnTop ActForm, SetValue
    End If
End Sub
Public Function GetCedaWeekDay(CedaDate As String) As String
    Dim strWkdy As String
    Dim varFday
    If CedaDate <> "" Then
        varFday = CedaDateToVb(Left(CedaDate, 8))
        strWkdy = Trim(Str(Weekday(varFday, vbMonday)))
    End If
    GetCedaWeekDay = strWkdy
End Function

Public Function GetTimePart(CedaDate As String) As String
Dim tmpVal As String
    If Trim(CedaDate) <> "" Then tmpVal = Mid(CedaDate, 9, 2) & ":" & Mid(CedaDate, 11, 2) Else tmpVal = ""
    GetTimePart = tmpVal
End Function

Public Function GetTimeStamp(MinuteOffset As Integer) As String
Dim tmpTime 'as variant
    tmpTime = DateAdd("n", MinuteOffset, Now)
    GetTimeStamp = Format(tmpTime, "yyyymmddhhmmss")
End Function

Public Function CheckValidTime(tmpData As String, tmpAllow As String) As Boolean
    Dim Result As Boolean
    Dim tmpChk As String
    Dim tmpVal As Integer
    tmpVal = Val(tmpData)
    tmpChk = Right("0000" & Trim(Str(tmpVal)), 4)
    If tmpChk = tmpData Then
        Result = True
        If tmpVal < 0 Then Result = False
        tmpVal = Val(Left(tmpChk, 2))
        If tmpVal < 0 Then Result = False
        If tmpVal > 23 Then
            If (tmpVal = 24) And (tmpAllow <> "2400") Then Result = False
            If tmpVal > 24 Then Result = False
        End If
        tmpVal = Val(Right(tmpChk, 2))
        If tmpVal < 0 Then Result = False
        If tmpVal > 59 Then Result = False
    Else
        Result = False
    End If
    CheckValidTime = Result
End Function

Public Function BuildAftFkey(tmpAlc3 As String, tmpFltn As String, tmpFlns As String, tmpDate As String, tmpAdid As String) As String
Dim Result As String
Dim tmpData As String
    Result = "00000XXX#00000000U"
    tmpData = Trim(tmpAlc3)
    If tmpData <> "" Then
        Mid(Result, 6, 3) = tmpData
    End If
    tmpData = Trim(tmpFltn)
    If tmpData <> "" Then
        tmpData = Right("00000" & tmpData, 5)
        Mid(Result, 1, 5) = tmpData
    End If
    tmpData = Trim(tmpFlns)
    If tmpData <> "" Then
        Mid(Result, 9, 1) = tmpData
    End If
    tmpData = Trim(tmpDate)
    If tmpData <> "" Then
        tmpData = Left(tmpData, 8)
        Mid(Result, 10, 8) = tmpData
    End If
    tmpData = Trim(tmpAdid)
    If tmpData <> "" Then
        Mid(Result, 18, 1) = tmpData
    End If
    BuildAftFkey = Result
End Function

Public Sub SetItem(ItemList As String, ItemNbr As Integer, ItemSep As String, NewValue As String)
Dim Result As String
Dim CurItm As Integer
Dim ReqItm As Integer
Dim MaxItm As Integer
    Result = ""
    ReqItm = ItemNbr - 1
    For CurItm = 1 To ReqItm
        Result = Result & GetItem(ItemList, CurItm, ItemSep) & ItemSep
    Next
    Result = Result & NewValue & ItemSep
    MaxItm = ItemCount(ItemList, ItemSep)
    ReqItm = ReqItm + 2
    For CurItm = ReqItm To MaxItm
        Result = Result & GetItem(ItemList, CurItm, ItemSep) & ItemSep
    Next
    ItemList = Left(Result, Len(Result) - Len(ItemSep))
End Sub
Public Function ItemCount(ItemList As String, ItemSep As String) As Long
Dim Result As Long
Dim SepPos As Long
Dim SepLen As Long
    If ItemList <> "" Then
        Result = 1
        SepLen = Len(ItemSep)
        SepPos = 1
        Do
            SepPos = InStr(SepPos, ItemList, ItemSep, vbBinaryCompare)
            If SepPos > 0 Then
                Result = Result + 1
                SepPos = SepPos + SepLen
            End If
        Loop While SepPos > 0
    Else
        Result = 0
    End If
    ItemCount = Result
End Function

Public Function DecodeSsimDayFormat(tmpDate As String, InFormat As String, OutFormat As String) As String
    Dim Result As String
    Dim tmpDay As String
    Dim tmpMonth As String
    Dim tmpYear As String
    Dim tmpMonVal As Integer
    Dim ErrValue As Boolean
    Result = tmpDate
    ErrValue = False
    Select Case InFormat
        Case "SSIM2"
            If tmpDate <> "00XXX00" Then
                tmpDay = Left(tmpDate, 2)
                tmpMonth = Mid(tmpDate, 3, 3)
                tmpMonVal = GetItemNo(MonthList, tmpMonth)
                tmpMonth = Right("00" & Trim(Str(tmpMonVal)), 2)
                tmpYear = Right(tmpDate, 2)
                If tmpYear = "99" Then tmpYear = "19" & tmpYear Else tmpYear = "20" & tmpYear
            Else
                ErrValue = True
            End If
        Case "SSIM4"
            tmpDay = Left(tmpDate, 2)
            tmpMonth = Mid(tmpDate, 3, 3)
            tmpMonVal = GetItemNo(MonthList, tmpMonth)
            tmpMonth = Right("00" & Trim(Str(tmpMonVal)), 2)
            tmpYear = Right(tmpDate, 4)
        Case "CEDA"
            tmpYear = Left(tmpDate, 4)
            tmpMonth = Mid(tmpDate, 5, 2)
            tmpDay = Right(tmpDate, 2)
        Case Else
    End Select
    If Not ErrValue Then
        'here we always have a 4 digit year, 2 digit month and 2 digit day
        Select Case OutFormat
            Case "CEDA"
                Result = tmpYear & tmpMonth & tmpDay
            Case "SSIM2"
                tmpMonVal = Val(tmpMonth)
                tmpMonth = GetItem(MonthList, tmpMonVal, ",")
                tmpYear = Right(tmpYear, 2)
                Result = tmpDay & tmpMonth & tmpYear
            Case Else
        End Select
    End If
    DecodeSsimDayFormat = Result
End Function

Public Function StripAftFlno(AftFlno, AftAirl As String, AftFltn As String, AftFlns As String) As String
    Dim Result As String
    Dim tmpDat As String
    AftFltn = Trim(AftFlno)
    AftAirl = Left(AftFltn, 2)
    AftFltn = Mid(AftFltn, 3)
    tmpDat = Left(AftFltn, 1)
    If tmpDat <> "" Then
        If tmpDat < "0" Or tmpDat > "9" Then
            AftAirl = AftAirl & tmpDat
            AftFltn = Mid(AftFltn, 2)
        End If
    End If
    Result = BuildProperAftFlno(AftAirl, AftFltn, AftFlns)
    StripAftFlno = Result
End Function

Public Function BuildProperAftFlno(AftAirl As String, AftFltn As String, AftFlns As String) As String
Dim Result As String
Dim tmpAirl As String
Dim tmpFltn As String
Dim tmpFlns As String
Dim tmpSffx As String
Dim ilLen As Integer
    tmpAirl = Trim(AftAirl)
    tmpFltn = Trim(AftFltn)
    tmpFlns = Trim(AftFlns)
    If tmpAirl <> "" And tmpFltn <> "" Then
        While Left(tmpFltn, 1) = "0"
            tmpFltn = Mid(tmpFltn, 2)
        Wend
        tmpSffx = Right(tmpFltn, 1)
        If tmpSffx <> "" Then
            If tmpSffx < "0" Or tmpSffx > "9" Then
                tmpFlns = tmpSffx
                tmpFltn = Left(tmpFltn, Len(tmpFltn) - 1)
            End If
        End If
        tmpFltn = RTrim(tmpFltn)
        ilLen = Len(tmpFltn)
        If (ilLen >= 0) And (ilLen < 3) Then tmpFltn = Right("000" & tmpFltn, 3)
        If tmpFlns = "" Then tmpFlns = " "
        Result = Left(tmpAirl & "   ", 3)
        Result = Result & Left(tmpFltn & "     ", 5)
        Result = Result & tmpFlns
        AftAirl = tmpAirl
        AftFltn = tmpFltn
        AftFlns = tmpFlns
    Else
        Result = ""
        AftAirl = ""
        AftFltn = ""
        AftFlns = ""
    End If
    BuildProperAftFlno = RTrim(Result)
End Function

Public Sub SleepSomeSeconds(SecondValue As Long)
Dim StartTime
    StartTime = DateAdd("s", SecondValue, Now)
    While StartTime > Now
        'Sleep
    Wend
End Sub

Public Function CheckValidCedaTime(ChkDateTime As String) As Boolean
    Dim IsValidDate As Boolean
    Dim IsValidTime As Boolean
    Dim Result As Boolean
    Dim tmpDate As String
    Dim tmpTime As String
    Dim tmpSeconds As String
    Dim tmpLen As Integer
    Result = True
    tmpLen = Len(ChkDateTime)
    If (tmpLen = 12) Or (tmpLen = 14) Then
        tmpDate = Left(ChkDateTime, 8)
        IsValidDate = CheckValidDate(tmpDate)
        tmpTime = Mid(ChkDateTime, 9, 4)
        IsValidTime = CheckValidTime(tmpTime, "")
        If tmpLen = 14 Then
            tmpSeconds = Right(ChkDateTime, 2)
        End If
        If Not IsValidDate Then Result = False
        If Not IsValidTime Then Result = False
    Else
        Result = False
    End If
    CheckValidCedaTime = Result
End Function


Public Function CheckValidDate(ChkDate As String) As Boolean
    Dim VbDate 'As Variant
    Dim CedaDate As String
    VbDate = CedaDateToVb(ChkDate)
    CedaDate = Format(VbDate, "yyyymmdd")
    If CedaDate = ChkDate Then
        CheckValidDate = True
    Else
        CheckValidDate = False
    End If
End Function

Public Function GetFileAndPath(UseFileName As String, ActFile As String, ActPath As String) As Integer
    Dim Result As Boolean
    Dim tmpDat As String
    Dim ItmCnt As Integer
    Dim CurItm As Integer
    ItmCnt = ItemCount(UseFileName, "\")
    ActFile = GetItem(UseFileName, ItmCnt, "\")
    ActPath = ""
    ItmCnt = ItmCnt - 1
    For CurItm = 1 To ItmCnt
        ActPath = ActPath & GetItem(UseFileName, CurItm, "\") & "\"
    Next
    CurItm = InStr(ActFile, " ")
    If CurItm > 0 Then
        If CurItm > 1 Then ActPath = ActPath & Left(ActFile, CurItm - 1) & "\"
        ActFile = Mid(ActFile, CurItm + 1)
        ActFile = Trim(ActFile)
    End If
    If Len(ActPath) > 0 Then ActPath = Left(ActPath, Len(ActPath) - 1)
    GetFileAndPath = ItemCount(ActFile, " ")
End Function

Public Sub Sleep(Sekunden%)
    Dim AktuelleZeit As Variant
    AktuelleZeit = Timer
    Do
        'DoEvents
    Loop Until Timer - AktuelleZeit > Sekunden%
End Sub

Public Sub SetTextSelected()
    Dim i As Integer
    Dim MyTextBox As Object
    Set MyTextBox = Screen.ActiveControl
    If TypeName(MyTextBox) = "TextBox" Then
        i = Len(MyTextBox.Text)
        MyTextBox.SelStart = 0
        MyTextBox.SelLength = i
    End If
End Sub


' Add your application to the system tray.
' Param 1 = Handle of form (which deals with sys tray events)
' Param 2 = Icon (form icon - any icon)
' Param 3 = Handle of icon (form icon - any icon)
' Param 4 = Tip for sys tray icon.
'
' Example - AddIconToTray Me.Hwnd, Me.Icon, Me.Icon.Handle, "This is a test tip"
'
Public Sub AddIconToTray(MeHwnd As Long, MeIcon As Long, MeIconHandle As Long, Tip As String)
    With nfIconData
       .hWnd = MeHwnd
       .uID = MeIcon
       .uFlags = NIF_ICON Or NIF_MESSAGE Or NIF_TIP
       .uCallbackMessage = WM_RBUTTONUP
       .hIcon = MeIconHandle
       .szTip = Tip & Chr$(0)
       .cbSize = Len(nfIconData)
    End With
    Shell_NotifyIcon NIM_ADD, nfIconData
End Sub

Public Sub ChangeIconInTray(MeIcon As Long, MeTip As String)
    nfIconData.hIcon = MeIcon
    If MeTip <> "" Then
        nfIconData.szTip = MeTip & Chr$(0)
    End If
    Shell_NotifyIcon NIM_MODIFY, nfIconData
End Sub

' Remove your application from the system tray.
' Call when you quit your application.
'
Public Sub RemoveIconFromTray()
    Shell_NotifyIcon NIM_DELETE, nfIconData
End Sub

' Call this routine to ensure my app gets notified of all events
' Example - Hook Me.hWnd
Public Sub Hook(Lwnd As Long)
    If Hooking = False Then
       FHandle = Lwnd
       WndProc = SetWindowLong(Lwnd, GWL_WNDPROC, AddressOf WindowProc)
       Hooking = True
    End If
End Sub

' Call this routine to transfer event notification back to standard handler
' Example - Unhook
Public Sub Unhook()
    If Hooking = True Then
       SetWindowLong FHandle, GWL_WNDPROC, WndProc
       Hooking = False
    End If
End Sub

Public Function WindowProc(ByVal hw As Long, ByVal uMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
    ' Ensure that its our app thats affected and that its the right event
    If Hooking = True Then
       If uMsg = WM_RBUTTONUP And lParam = WM_RBUTTONDOWN Then
          MyTrayForm.SysTrayMouseEventHandler  ' Pass the event back to the form handler
          WindowProc = True               ' Let windows know we handled it
          Exit Function
       End If
       WindowProc = CallWindowProc(WndProc, hw, uMsg, wParam, lParam) ' Pass it along
    End If
End Function


Public Function GetKeyItem(ByRef rResult, ByRef rString, ByVal vStartSep, ByVal vEndSep) As Boolean
    Dim llPosStart As Long
    Dim llPosEnd As Long
    
    GetKeyItem = False
    rResult = ""
    llPosStart = InStr(1, rString, vStartSep, vbTextCompare)
    If llPosStart > 0 Then
        GetKeyItem = True
        llPosStart = llPosStart + Len(vStartSep)
        llPosEnd = InStr(llPosStart, rString, vEndSep, vbTextCompare)
        If llPosEnd > 0 Then
            rResult = Mid(rString, llPosStart, (llPosEnd - llPosStart))
        Else
            rResult = Right(rString, Len(rString) - llPosStart + 1)
        End If
    End If
End Function

'Public Sub SetControlFonts(MyForm As Form, MyName As String, MySize As Double, SetBold As Boolean)
'    Dim UseFont As StdFont
'    Dim CurCtrl As Control
'    Set UseFont = New StdFont
'    With UseFont
'        If MyName <> "" Then .Name = MyName
'        If MySize > 0 Then .Size = MySize
'        .Bold = SetBold
'    End With
'    Set MyForm.Font = UseFont
'    On Error Resume Next
'    For Each CurCtrl In MyForm.Controls
'        Set CurCtrl.Font = UseFont
'    Next
'End Sub

Public Sub CheckKeyboard(Flags As Integer)
    If Flags <> KeybdFlags Then
        Select Case Flags
            Case 0
                KeybdState = "None"
                KeybdIsShift = False
                KeybdIsCtrl = False
                KeybdIsAlt = False
            Case 1
                KeybdState = "Shift"
                KeybdIsShift = True
                KeybdIsCtrl = False
                KeybdIsAlt = False
            Case 2
                KeybdState = "Ctrl"
                KeybdIsShift = False
                KeybdIsCtrl = True
                KeybdIsAlt = False
            Case 3
                KeybdState = "Shift/Ctrl"
                KeybdIsShift = True
                KeybdIsCtrl = True
                KeybdIsAlt = False
            Case 4
                KeybdState = "Alt"
                KeybdIsShift = False
                KeybdIsCtrl = False
                KeybdIsAlt = True
            Case 5
                KeybdState = "Shift/Alt"
                KeybdIsShift = True
                KeybdIsCtrl = False
                KeybdIsAlt = True
            Case 6
                KeybdState = "Ctrl/Alt"
                KeybdIsShift = False
                KeybdIsCtrl = True
                KeybdIsAlt = True
            Case 7
                KeybdState = "Shift/Ctrl/Alt"
                KeybdIsShift = True
                KeybdIsCtrl = True
                KeybdIsAlt = True
        End Select
        KeybdFlags = Flags
    End If
End Sub

Public Sub DisplayWinHelpFile(X As Form)
    Dim Foo As Long
    Dim HelpFilePath As String
    Dim HelpFileName As String

    On Local Error GoTo Error_Handler
    HelpFileName = App.EXEName & ".chm"
    HelpFilePath = App.Path & "\" & HelpFileName
    Foo = WinHelp(X.hWnd, HelpFilePath, HELP_INDEX, CLng(0))
Error_End:
    Exit Sub
Error_Handler:
    MsgBox Err.Description
    Resume Error_End
End Sub

Public Sub DisplayHtmHelpFile(UseFileName As String, ContextId As String, UseKeyWord As String)
    Static myHelpPath As String
    Static myHelpAppId 'As Variant
    Dim HelpFilePath As String
    Dim HelpFileName As String
    'Context ID prepared for future use. Actually ignored
    If myHelpAppId > 0 Then
        On Error Resume Next
        Err.Description = ""
        AppActivate myHelpAppId, False
        If Err.Description = "" Then
            SendKeys "%{F4}", True
        End If
    End If
    HelpFileName = App.EXEName & ".chm"
    'If myHelpPath = "" Then myHelpPath = GetIniEntry(DEFAULT_CEDA_INI, "GLOBAL", "", "HelpDirectory", "c:\ufis\help")
    If myHelpPath = "" Then myHelpPath = GetIniEntry(DEFAULT_CEDA_INI, "GLOBAL", "", "HelpDirectory", UFIS_HELP)
    HelpFileName = GetIniEntry(DEFAULT_CEDA_INI, App.EXEName, "", "HelpFile", HelpFileName)
    HelpFilePath = myHelpPath & "\" & HelpFileName
    myHelpAppId = Shell("HH " & HelpFilePath, vbNormalFocus)
End Sub

Public Sub ShowHtmlHelp(CurForm As Form, ByVal tHelpFile As String, ByVal tHelpPage As String)
    Static myHelpPath As String
    Dim myHelpAppId 'As Variant
    Dim UseHelpFile As String
    Const HH_DISPLAY_TOPIC = &H0
    On Error Resume Next
    'If myHelpPath = "" Then myHelpPath = GetIniEntry(DEFAULT_CEDA_INI, "GLOBAL", "", "HelpDirectory", "c:\ufis\help")
    If myHelpPath = "" Then myHelpPath = GetIniEntry(DEFAULT_CEDA_INI, "GLOBAL", "", "HelpDirectory", UFIS_HELP)
    UseHelpFile = tHelpFile
    If UseHelpFile = "" Then UseHelpFile = App.EXEName & ".chm"
    UseHelpFile = myHelpPath & "\" & UseHelpFile
    ' open the help page in a modeless window
    myHelpAppId = HtmlHelpTopic(CurForm.hWnd, UseHelpFile, HH_DISPLAY_TOPIC, 0)
    If myHelpAppId > 0 Then
        HtmHelpHwnd = CurForm.hWnd
        HtmHelpIsOpen = True
    Else
        HtmHelpHwnd = 0
        HtmHelpIsOpen = False
    End If
End Sub

Public Sub CloseHtmlHelp()
    Dim myHelpAppId 'As Variant
    Const HHH_CLOSE_ALL = &H12
    On Error Resume Next
    If HtmHelpHwnd > 0 Then
        myHelpAppId = HtmlHelpTopic(HtmHelpHwnd, "", HHH_CLOSE_ALL, 0)
        HtmHelpHwnd = 0
    End If
    HtmHelpIsOpen = False
End Sub

Public Function ExistFile(sSpec As String) As Boolean
    On Error Resume Next
    Call FileLen(sSpec)
    ExistFile = (Err = 0)
End Function

Public Function GetWindowsUserName() As String
    Dim l&, Ergebnis&, Fehler&
    Dim User$, Puffer$

    'Benutzernamen ermitteln
    User = Space(255)
    l = 255
    Ergebnis = GetUserName(User, l)

    If Ergebnis <> 0 Then
        GetWindowsUserName = Left$(User, l - 1)
    Else
        GetWindowsUserName = ""
    End If
End Function

Public Function GetWorkStationName() As String
    Dim buffer$, Result&, l&, CName$
    l = MAX_COMPUTERNAME_LENGTH + 1
    buffer = Space$(l)
    Result = GetComputerName(buffer, l)

    If Result = 1 Then
      CName = Left$(buffer, InStr(1, buffer, Chr$(0)) - 1)
      GetWorkStationName = CName
    End If
End Function

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' -     Loads all strings belonging to all controls of a form.
' -     Depends on the value of the "TAG"
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Public Sub LoadResStrings(frm As Form)
    On Error Resume Next

    Dim ctl As Control
    Dim Obj As Object

    Dim sCtlType As String
    Dim nVal As Integer

    'set the form's caption
    frm.Caption = LoadResString(CInt(frm.Tag))

    For Each ctl In frm.Controls
        sCtlType = TypeName(ctl)
        If sCtlType = "Label" Then
            ctl.Caption = LoadResString(CInt(ctl.Tag))
        ElseIf sCtlType = "Menu" Then
            ctl.Caption = LoadResString(CInt(ctl.Caption))
        ElseIf sCtlType = "TabStrip" Then
            For Each Obj In ctl.Tabs
                Obj.Caption = LoadResString(CInt(Obj.Tag))
                Obj.ToolTipText = LoadResString(CInt(Obj.ToolTipText))
            Next
        ElseIf sCtlType = "Toolbar" Then
            For Each Obj In ctl.Buttons
                Obj.ToolTipText = LoadResString(CInt(Obj.ToolTipText))
            Next
        ElseIf sCtlType = "ListView" Then
            For Each Obj In ctl.ColumnHeaders
                Obj.Text = LoadResString(CInt(Obj.Tag))
            Next
        Else
            nVal = 0
            nVal = Val(ctl.Tag)
            If nVal > 0 Then ctl.Caption = LoadResString(nVal)
            nVal = 0
            nVal = Val(ctl.ToolTipText)
            If nVal > 0 Then ctl.ToolTipText = LoadResString(nVal)
        End If
    Next
End Sub

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' -     connects to CEDA.
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
'BST 2009: Disabled
Public Function XConnectToCeda(ByRef rUfisCom As Object, ByRef sAppl As String, ByRef sHopo As String, ByRef sTableExt As String, ByRef sServer As String, ByRef sConnectType As String) As Boolean
    Dim ret As Integer

    rUfisCom.CleanupCom

    rUfisCom.SetCedaPerameters sAppl, sHopo, sTableExt

    ret = rUfisCom.InitCom(sServer, sConnectType)

    If ret = 0 Then
        MsgBox "Connection to CEDA failed!", vbCritical, "No connection!"
        End
    End If
End Function

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' -     Delivers the content of a file in a string
' -     only for small files
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
'BST 2009: Disabled
Public Function XGetFileContent(ByRef sFileName As String) As String
    Dim filelines As Long
    Dim tmpCount As Long
    Dim InputLine As String
    Dim buffer As String

    If sFileName <> "" Then
        Open sFileName For Input As #1   ' Open file for input.
        Do While Not EOF(1)   ' Check for end of file.
           filelines = filelines + 1
           tmpCount = tmpCount + 1
           Line Input #1, InputLine   ' Read line of data.
           buffer = buffer + InputLine
        Loop
        Close #1   ' Close file.
    End If
    XGetFileContent = buffer
End Function

Public Function MakeCedaDate(ByVal myDat As Date) As String
    Dim s As String
    Dim strDay As String
    Dim strMonth As String
    Dim strYear As String
    Dim strHour As String
    Dim strMinute As String
    Dim strSeconds As String
    
    Dim strDat As String
    
    strDat = myDat
    strYear = Year(myDat) 'Mid(strDat, 7, 4)
    strMonth = Month(myDat) 'Mid(strDat, 4, 2)
    If Len(strMonth) = 1 Then
        strMonth = "0" + strMonth
    End If
    strDay = Day(myDat) 'Mid(strDat, 1, 2)
    If Len(strDay) = 1 Then
        strDay = "0" + strDay
    End If
    strHour = Hour(myDat) 'Mid(strDat, 12, 2)
    If Len(strHour) = 1 Then
        strHour = "0" + strHour
    End If
    strMinute = Minute(myDat) 'Mid(strDat, 15, 2)
    If Len(strMinute) = 1 Then
        strMinute = "0" + strMinute
    End If
    strSeconds = Second(myDat) 'Mid(strDat, 18, 2)
    s = strYear + strMonth + strDay + strHour + strMinute + strSeconds
    MakeCedaDate = s
End Function

Public Sub GetUfisDir()
    Dim EnvString As String
    
    EnvString = Environ("UFISSYSTEM")
    If EnvString <> "" Then
        UFIS_SYSTEM = EnvString
    Else
        UFIS_SYSTEM = "C:\UFIS\SYSTEM"
    End If
    EnvString = Environ("UFISHELP")
    If EnvString <> "" Then
        UFIS_HELP = EnvString
    Else
        UFIS_HELP = "C:\UFIS\HELP"
    End If
    EnvString = Environ("UFISAPPL")
    If EnvString <> "" Then
        UFIS_APPL = EnvString
    Else
        UFIS_APPL = "C:\UFIS\APPL"
    End If
    EnvString = Environ("UFISTMP")
    If EnvString <> "" Then
        UFIS_TMP = EnvString
    Else
        UFIS_TMP = "C:\TMP"
    End If
    EnvString = Environ("CEDA")
    If EnvString <> "" Then
        DEFAULT_CEDA_INI = EnvString
    Else
        DEFAULT_CEDA_INI = "C:\UFIS\SYSTEM\CEDA.INI"
    End If
    UFIS_UFIS = GetItem(DEFAULT_CEDA_INI, 1, "\") & "\" & GetItem(DEFAULT_CEDA_INI, 2, "\")
End Sub

Public Function CheckAndMakeDir(TryPath As String) As Boolean
    Dim retval As Boolean
    Dim CurPath As String
    Dim ChkPath As String
    Dim ChkItem As String
    Dim CurItm As Integer
    Dim ItmCnt As Integer
    Dim i As Integer
    On Error Resume Next
    retval = True
    CurPath = TryPath
    ItmCnt = CInt(ItemCount(CurPath, "\"))
    ChkPath = Dir(CurPath, vbDirectory)
    While ((ChkPath = "") And (Len(CurPath) > 3))
        While ((Len(CurPath) > 3) And (Right(CurPath, 1) <> "\"))
            CurPath = Left(CurPath, Len(CurPath) - 1)
        Wend
        CurPath = Left(CurPath, Len(CurPath) - 1)
        ChkPath = Dir(CurPath, vbDirectory)
    Wend
    CurItm = CInt(ItemCount(CurPath, "\"))
    If CurItm < ItmCnt Then
        CurItm = CurItm + 1
        For i = CurItm To ItmCnt
            ChkItem = GetItem(TryPath, i, "\")
            CurPath = CurPath & "\" & ChkItem
            MkDir CurPath
        Next
        ChkPath = Dir(TryPath, vbDirectory)
        If ChkPath = "" Then retval = False
    End If
    CheckAndMakeDir = retval
End Function
Public Sub LoadCfgFromUseFile()
    Dim tmpCmdLine As String
    Dim fn As Integer
    Dim FSize As Long
    Dim FData As String
    Dim KData As String
    Dim KText As String
    Dim KCode As String
    
    ApplGotCfgFromFile = False
    ApplStartupFile = ""
    tmpCmdLine = UCase(CmdLineText)
    KCode = UCase(UFIS_FILE_EXT)
    If InStr(tmpCmdLine, KCode) > 0 Then ApplGotCfgFromFile = True
    If InStr(tmpCmdLine, ".FDTR") > 0 Then ApplGotCfgFromFile = True

    If ApplGotCfgFromFile Then
        ApplStartupFile = tmpCmdLine
        fn = FreeFile
        Open ApplStartupFile For Binary As #fn
        FSize = LOF(fn)
        FData = String$(FSize, 32)
        Get #fn, , FData
        Close #fn
        GetKeyItem KText, FData, "{=[INI]=}", "{/=[INI]=}"
        GetKeyItem KData, FData, "{=[CFG]=}", "{/=[CFG]=}"
        If KText = "" Then ApplGotCfgFromFile = False
        If KData = "" Then ApplGotCfgFromFile = False
    End If
    If ApplGotCfgFromFile Then
        myIniFullName = KText
        GetIniEntry KText, "", "", "{SET_FILE}", KData
    End If
End Sub

Public Function GetSetGlobalValues(CmdKey As String, CmdCode As String, CmdData As String) As String
    Dim Result As String
    Dim CHead As String
    Dim CTail As String
    Dim CPos1 As Long
    Dim CPos2 As Long
    Dim CLen1 As Long
    Dim CLen2 As Long
    Result = ""
    CHead = "{=(" & CmdCode & ")=}"
    CTail = "{/=(" & CmdCode & ")=}" & vbNewLine
    Select Case CmdKey
        Case "GET"
            GetKeyItem Result, ApplGlobalDataValues, CHead, CTail
        Case "SET"
            CPos1 = InStr(ApplGlobalDataValues, CHead)
            If CPos1 > 0 Then
                CLen1 = CPos1 - 1
                CPos2 = InStr(ApplGlobalDataValues, CTail) + Len(CTail)
                CLen2 = Len(ApplGlobalDataValues) - CPos2 + 1
                ApplGlobalDataValues = Left(ApplGlobalDataValues, CLen1) & Right(ApplGlobalDataValues, CLen2)
            End If
            ApplGlobalDataValues = ApplGlobalDataValues & CHead & CmdData & CTail
        Case "CLR"
            CPos1 = InStr(ApplGlobalDataValues, CHead)
            If CPos1 > 0 Then
                CLen1 = CPos1 - 1
                CPos2 = InStr(ApplGlobalDataValues, CTail) + Len(CTail)
                CLen2 = Len(ApplGlobalDataValues) - CPos2 + 1
                ApplGlobalDataValues = Left(ApplGlobalDataValues, CLen1) & Right(ApplGlobalDataValues, CLen2)
            End If
        Case "PUT"
            ApplGlobalDataValues = CmdData
        Case "INI"
            ApplGlobalDataValues = ""
        Case Else
    End Select
    GetSetGlobalValues = Result
End Function

Public Function EnCryptBuffer(strBuffer As String, KeyString As String) As String
    Static CurPass As String
    Dim PSeed As Long
    Dim StrIn As String
    Dim StrOut As String
    Dim StrPas As String
    Dim k As Long
    Dim j As Long
    Dim l As Long
    Dim m As Integer
    Dim LSize As Long
    Dim PSize As Long
    Dim ba As Byte
    Dim bb As Byte
    Dim bc As Byte
    StrIn = strBuffer
    StrOut = ""
    StrPas = ""
    LSize = Len(StrIn)
    StrOut = String(LSize, 32)
    StrPas = KeyString
    If StrPas = "" Then StrPas = "BERNI"
    PSize = Len(StrPas)
    'If StrPas <> CurPass Then
        PSeed = 0
        For k = 1 To PSize
            PSeed = PSeed + Asc(Mid(StrPas, k, 1))
        Next
    'End If
    Rnd (-1)
    Randomize PSeed
    'Randomize 45
    'CurPass = StrPas
    k = 0
    For j = 1 To LSize
        k = k + 1
        If k > PSize Then k = 1
        m = Asc(Mid(StrIn, j, 1)) Xor Asc(Mid(StrPas, k, 1)) Xor Delta(255, 0)
        'm = (Asc(Mid(StrIn, j, 1))) Xor (Asc(Mid(StrPas, k, 1)))
        Mid(StrOut, j, 1) = Chr(m)
        'ba = Asc(Mid(StrIn, j, 1))
        'bb = Asc(Mid(StrPas, k, 1))
        'bc = ba Xor bb
        'Mid(StrOut, j, 1) = Chr(bc)
    Next
    EnCryptBuffer = StrOut
End Function

Private Function Delta(UpLim As Integer, LowLim As Integer) As Integer
'Random number: LowLim <= Delta <= UpLim
Delta = Int((UpLim - LowLim + 1) * Rnd + LowLim)
End Function
'Save the file and return the keyData lenght
Public Function SaveToUseFile(fn As Integer, KeyWord As String, Context As String, KeyData As String, KPass As String) As Long
    Dim DSize As Long
    Dim SPos As Long
    Dim KHead As String
    Dim KCode As String
    Dim KTail As String
    Dim KSize As String
    Dim FData As String
    Dim KData As String
    Dim tmpData As String
    KHead = "{*SIZE*}"
    KSize = "0000000000"
    KTail = "{/*SIZE*}"
    KCode = KHead & KSize & KTail & vbNewLine
    KData = ""
    KData = KData & "{*CODE*}" & KeyWord & "{/*CODE*}" & vbNewLine
    KData = KData & "{*TEXT*}" & Context & "{/*TEXT*}" & vbNewLine
    KData = KData & "{*DATA*}" & KeyData & "{/*DATA*}" & vbNewLine
    DSize = Len(KCode) + Len(KData)
    KSize = Right(KSize & CStr(DSize), 10)
    SPos = Len(KHead) + 1
    Mid(KCode, SPos) = KSize
    If KPass <> "" Then
        KCode = EnCryptBuffer(KCode, KPass)
        KData = EnCryptBuffer(KData, KPass)
    End If
    FData = KCode & KData
    Put #fn, , FData
    SaveToUseFile = Len(FData)
End Function
'Save to the file and return the saved data
Public Function SaveToFile(fn As Integer, KeyWord As String, Context As String, KeyData As String, KPass As String) As String
    Dim DSize As Long
    Dim SPos As Long
    Dim KHead As String
    Dim KCode As String
    Dim KTail As String
    Dim KSize As String
    Dim FData As String
    Dim KData As String
    Dim tmpData As String
    KHead = "{*SIZE*}"
    KSize = "0000000000"
    KTail = "{/*SIZE*}"
    KCode = KHead & KSize & KTail & vbNewLine
    KData = ""
    KData = KData & "{*CODE*}" & KeyWord & "{/*CODE*}" & vbNewLine
    KData = KData & "{*TEXT*}" & Context & "{/*TEXT*}" & vbNewLine
    KData = KData & "{*DATA*}" & KeyData & "{/*DATA*}" & vbNewLine
    DSize = Len(KCode) + Len(KData)
    KSize = Right(KSize & CStr(DSize), 10)
    SPos = Len(KHead) + 1
    Mid(KCode, SPos) = KSize
    If KPass <> "" Then
        KCode = EnCryptBuffer(KCode, KPass)
        KData = EnCryptBuffer(KData, KPass)
    End If
    FData = KCode & KData
    Put #fn, , FData
    SaveToFile = FData
End Function
Public Function CreatePassKey(UseLen As Integer) As String
    Dim PasChar As String
    Dim NumRnd As Integer
    Dim lMax As Integer
    Dim lCur As Integer
    Dim n As Integer
    lMax = UseLen
    If lMax < 1 Then lMax = 1
    If lMax > 100 Then lMax = 100
    PasChar = ""
    lCur = 0
    While lCur < lMax
        NumRnd = Int(127 * Rnd + 1)
        Select Case NumRnd
            Case 48 To 57, 65 To 90, 97 To 122
                    PasChar = PasChar & Chr(NumRnd)
                    lCur = lCur + 1
            Case Else
        End Select
    Wend
    CreatePassKey = PasChar
End Function

Public Function DrawRotatedText(PicBox As PictureBox, ByVal txt As String, _
    ByVal X As Single, ByVal Y As Single, _
    ByVal font_name As String, ByVal size As Long, _
    ByVal weight As Long, ByVal escapement As Long, _
    ByVal use_italic As Boolean, ByVal use_underline As Boolean, _
    ByVal use_strikethrough As Boolean) As Long

Const CLIP_LH_ANGLES = 16   ' Needed for tilted fonts.
Const PI = 3.14159625
Const PI_180 = PI / 180#

Dim newfont As Long
Dim oldfont As Long

    newfont = CreateFont(size, 0, _
        escapement, escapement, weight, _
        use_italic, use_underline, _
        use_strikethrough, 0, 0, _
        CLIP_LH_ANGLES, 0, 0, font_name)
    
    ' Select the new font.
    oldfont = SelectObject(PicBox.hDC, newfont)
    
    ' Display the text.
    PicBox.CurrentX = X
    PicBox.CurrentY = Y
    PicBox.Print txt;

    ' Restore the original font.
    newfont = SelectObject(PicBox.hDC, oldfont)
    
    ' Free font resources (important!)
    DeleteObject newfont
    DrawRotatedText = PicBox.CurrentX
End Function

Public Function DrawRotated3dText(PicBox As PictureBox, ByVal txt As String, _
    ByVal X As Single, ByVal Y As Single, _
    ByVal font_name As String, ByVal size As Long, _
    ByVal weight As Long, ByVal escapement As Long, _
    ByVal use_italic As Boolean, ByVal use_underline As Boolean, _
    ByVal use_strikethrough As Boolean) As Long
    Dim CurX As Long
    PicBox.ForeColor = vbWhite
    DrawRotatedText PicBox, txt, X, Y, font_name, size, weight, escapement, use_italic, use_underline, use_strikethrough
    PicBox.ForeColor = vbBlack
    CurX = DrawRotatedText(PicBox, txt, X + 15, Y + 15, font_name, size, weight, escapement, use_italic, use_underline, use_strikethrough)
    DrawRotated3dText = CurX
End Function

Public Sub CloneGridLayout(NewTab As TABLib.Tab, UseTab As TABLib.Tab)
    Dim i As Integer
    Dim CurCol As Long
    Dim MaxCol As Long
    Dim tmpData As String
    NewTab.ResetContent
    NewTab.FontName = UseTab.FontName
    NewTab.FontSize = UseTab.FontSize
    NewTab.HeaderFontSize = UseTab.HeaderFontSize
    NewTab.LineHeight = UseTab.LineHeight
    NewTab.LeftTextOffset = UseTab.LeftTextOffset
    NewTab.SetTabFontBold True
    NewTab.MainHeader = UseTab.MainHeader
    'NewTab.GridlineColor = vbBlack
    If GridLifeStyle Then
        NewTab.LifeStyle = UseTab.LifeStyle
        NewTab.CursorLifeStyle = UseTab.CursorLifeStyle
        NewTab.SelectColumnBackColor = UseTab.SelectColumnBackColor
        NewTab.SelectColumnTextColor = UseTab.SelectColumnTextColor
    End If
    NewTab.LogicalFieldList = UseTab.LogicalFieldList
    NewTab.HeaderString = UseTab.HeaderString
    NewTab.HeaderLengthString = UseTab.HeaderLengthString
    NewTab.ColumnWidthString = UseTab.ColumnWidthString
    NewTab.SetMainHeaderValues UseTab.GetMainHeaderRanges, UseTab.GetMainHeaderValues, UseTab.GetMainHeaderColors
    NewTab.SetMainHeaderFont UseTab.HeaderFontSize, False, False, True, 0, UseTab.FontName
    NewTab.ColumnAlignmentString = UseTab.ColumnAlignmentString
    NewTab.HeaderAlignmentString = UseTab.HeaderAlignmentString
    MaxCol = UseTab.GetColumnCount - 1
    For CurCol = 0 To MaxCol
        tmpData = UseTab.DateTimeGetInputFormatString(CurCol)
        If tmpData <> "" Then
            NewTab.DateTimeSetColumn CurCol
            NewTab.DateTimeSetInputFormatString CurCol, tmpData
            NewTab.DateTimeSetOutputFormatString CurCol, UseTab.DateTimeGetOutputFormatString(CurCol)
            NewTab.DateTimeSetUTCOffsetMinutes CurCol, UseTab.DateTimeGetUTCOffsetMinutes(CurCol)
        Else
            NewTab.DateTimeResetColumn CurCol
        End If
    Next
    NewTab.ShowHorzScroller True
    NewTab.ShowVertScroller True
    NewTab.AutoSizeByHeader = True
    NewTab.AutoSizeColumns
End Sub

Public Sub ToggleServerButtons(idx As Integer, SetValue As Integer)
    '-----------------------------------------
    'Sorry I don't like a direct referenece
    'but it was the easiest way ... :-)
    If SetValue = 1 Then
'        MainDialog.chkServerSignal(idx).Visible = True
'        MainDialog.chkServerSignal(idx).ZOrder
'        MainDialog.SignalPanel(idx).Visible = True
'        MainDialog.SignalPanel(idx).ZOrder
    End If
'    MainDialog.chkServerSignal(idx).Value = SetValue
    '-----------------------------------------
End Sub

Public Function JumpToColoredLine(Index As Integer, CurTab As TABLib.Tab, UseColor As Long, LineStatus As Long, JumpToLine As Long, IsShift As Integer) As Long
    Dim CurScroll As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim UseLine As Long
    Dim LookColor As Long
    Dim ForeColor As Long
    Dim BackColor As Long
    Dim LineFound As Boolean
    Dim LoopCount As Integer
    Dim VisLines As Long
    Dim CurColor As Long
    CurColor = -1
    If JumpToLine < 0 Then
        LookColor = UseColor
        CurLine = CurTab.GetCurrentSelected
        If CurLine < 0 Then CurLine = -1
        MaxLine = CurTab.GetLineCount - 1
        LineFound = False
        LoopCount = 0
        Do
            While CurLine <= MaxLine
                CurLine = CurLine + 1
                CurTab.GetLineColor CurLine, ForeColor, BackColor
                If (LineStatus > 0) Then
                    If (LineStatus <> CurTab.GetLineStatusValue(CurLine)) Then BackColor = -1
                End If
                If BackColor = LookColor Then
                    UseLine = CurLine
                    LineFound = True
                    CurLine = MaxLine + 1
                End If
            Wend
            LoopCount = LoopCount + 1
            CurLine = -1
        Loop While (LineFound = False) And (LoopCount < 2)
    Else
        UseLine = JumpToLine
        LineFound = True
    End If
    If LineFound Then
        StopNestedCalls = True
        VisLines = (CurTab.Height / CurTab.LineHeight / 15) - 5
        If UseLine > VisLines Then CurScroll = UseLine - VisLines Else CurScroll = 0
        CurTab.OnVScrollTo CurScroll
        StopNestedCalls = True
        CurTab.SetCurrentSelection UseLine
        StopNestedCalls = False
        CurColor = LookColor
    End If
    JumpToColoredLine = CurColor
End Function


Public Function TranslateColor(ColorText As String) As Long
    Dim tmpColor As Long
    Select Case ColorText
        Case "GRAY1", "GREY1"
            tmpColor = &HFFFFFF
        Case "GRAY2", "GREY2"
            tmpColor = &HE0E0E0
        Case "GRAY3", "GREY3", "GRAY", "GREY"
            tmpColor = &HC0C0C0
        Case "GRAY4", "GREY4"
            tmpColor = &H808080
        Case "GRAY5", "GREY5"
            tmpColor = &H404040
        Case "GRAY6", "GREY6"
            tmpColor = &H0&
        Case "RED1"
            tmpColor = &HC0C0FF
        Case "RED2"
            tmpColor = &H8080FF
        Case "RED3", "RED"
            tmpColor = &HFF&
        Case "RED4"
            tmpColor = &HC0&
        Case "RED5"
            tmpColor = &H80&
        Case "RED6"
            tmpColor = &H40&
        Case "AMBER1"
            tmpColor = &HC0E0FF
        Case "AMBER2"
            tmpColor = &H80C0FF
        Case "AMBER3", "AMBER"
            tmpColor = &H80FF&
        Case "AMBER4"
            tmpColor = &H40C0&
        Case "AMBER5"
            tmpColor = &H4080&
        Case "AMBER6"
            tmpColor = &H404080
        Case "YELLOW1"
            tmpColor = &HC0FFFF
        Case "YELLOW2"
            tmpColor = &H80FFFF
        Case "YELLOW3", "YELLOW"
            tmpColor = &HFFFF&
        Case "YELLOW4"
            tmpColor = &HC0C0&
        Case "YELLOW5"
            tmpColor = &H8080&
        Case "YELLOW6"
            tmpColor = &H4040&
        Case "GREEN1"
            tmpColor = &HC0FFC0
        Case "GREEN2"
            tmpColor = &H80FF80
        Case "GREEN3", "GREEN"
            tmpColor = &HFF00&
        Case "GREEN4"
            tmpColor = &HC000&
        Case "GREEN5"
            tmpColor = &H8000&
        Case "GREEN6"
            tmpColor = &H4000&
        Case "CYAN1"
            tmpColor = &HFFFFC0
        Case "CYAN2"
            tmpColor = &HFFFF80
        Case "CYAN3", "CYAN"
            tmpColor = &HFFFF00
        Case "CYAN4"
            tmpColor = &HC0C000
        Case "CYAN5"
            tmpColor = &H808000
        Case "CYAN6"
            tmpColor = &H404000
        Case "BLUE1"
            tmpColor = &HFFC0C0
        Case "BLUE2"
            tmpColor = &HFF8080
        Case "BLUE3", "BLUE"
            tmpColor = &HFF0000
        Case "BLUE4"
            tmpColor = &HC00000
        Case "BLUE5"
            tmpColor = &H800000
        Case "BLUE6"
            tmpColor = &H400000
        Case "MAGENTA1"
            tmpColor = &HFFC0FF
        Case "MAGENTA2"
            tmpColor = &HFF80FF
        Case "MAGENTA3", "MAGENTA"
            tmpColor = &HFF00FF
        Case "MAGENTA4"
            tmpColor = &HC000C0
        Case "MAGENTA5"
            tmpColor = &H800080
        Case "MAGENTA6"
            tmpColor = &H400040
        Case "DARKBLUE"
            tmpColor = &HC00000
        Case "NORMALBLUE"
            tmpColor = vbBlue
        Case "LIGHTBLUE"
            tmpColor = &HFF0000
        Case "LIGHTGREY"
            tmpColor = LightGray
        Case "NORMALGREY"
            tmpColor = NormalGray
        Case "DARKGREY"
            tmpColor = DarkGray
        Case "YELLOW"
            tmpColor = vbYellow
        Case "DARKAMBER"
            tmpColor = &H40C0&
        Case "LIGHTAMBER"
            tmpColor = &H80FF&
        Case "DARKRED"
            tmpColor = &HC0&
        Case "RED"
            tmpColor = &HFF&
        Case "DARKGREEN"
            tmpColor = &H80FF80
        Case "LIGHTGREEN"
            tmpColor = &HC0FFC0
        Case "DARKYELLOW"
            tmpColor = &H80FFFF
        Case "LIGHTYELLOW"
            tmpColor = &HC0FFFF
        Case "DARK"
            tmpColor = &HC00000
        Case "LIGHT"
            tmpColor = &HFF0000
        Case "WHITE"
            tmpColor = vbWhite
        Case Else
            tmpColor = vbBlack
    End Select
    TranslateColor = tmpColor
End Function



Public Function GetFtypMean(AftFtyp As String) As String
    Select Case AftFtyp
        Case "S"
            GetFtypMean = "SKD"
        Case "O"
            GetFtypMean = "OPS"
        Case "X"
            GetFtypMean = "CXX"
        Case "N"
            GetFtypMean = "NOP"
        Case "D"
            GetFtypMean = "DIV"
        Case "B"
            GetFtypMean = "RTX"
        Case "Z"
            GetFtypMean = "RFL"
        Case "T"
            GetFtypMean = "TOW"
        Case "G"
            GetFtypMean = "GRM"
        Case "R"
            GetFtypMean = "RRO"
        Case " ", ""
            GetFtypMean = "PRG"
        Case Else
            GetFtypMean = AftFtyp
    End Select
End Function

Public Function GetPixelColor(pic As PictureBox) As Long
    'Dim bitmap_info As BITMAPINFO
    'Dim pixels() As Byte
    'Dim bytes_per_scanLine As Integer
    'Dim pad_per_scanLine As Integer
    'Dim wid As Integer
    'Dim hgt As Integer
    Dim PixColor As Long
    PixColor = pic.BackColor

'    ' Prepare the bitmap description.
'    With bitmap_info.bmiHeader
'        .biSize = 40
'        .biWidth = pic.ScaleWidth
'        ' Use negative height to scan top-down.
'        .biHeight = -pic.ScaleHeight
'        .biPlanes = 1
'        .biBitCount = 32
'        .biCompression = BI_RGB
'        bytes_per_scanLine = ((((.biWidth * .biBitCount) + 31) \ 32) * 4)
'        pad_per_scanLine = bytes_per_scanLine - (((.biWidth * .biBitCount) + 7) \ 8)
'        .biSizeImage = bytes_per_scanLine * Abs(.biHeight)
'    End With
'
'    ' Load the bitmap's data.
'    wid = pic.ScaleWidth
'    hgt = pic.ScaleHeight
'    ReDim pixels(1 To 4, 0 To wid - 1, 0 To hgt - 1)
'    GetDIBits pic.hDC, pic.Image, _
'        0, pic.ScaleHeight, pixels(1, 0, 0), _
'        bitmap_info, DIB_RGB_COLORS
        
    GetPixelColor = PixColor
End Function
