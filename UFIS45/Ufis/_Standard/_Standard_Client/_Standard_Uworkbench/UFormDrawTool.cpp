// drawtool.cpp - implementation for drawing tools
//
// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (C) 1992-1998 Microsoft Corporation
// All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.


#include "stdafx.h"
#include "UWorkBench.h"
#include "CCSGlobl.h"
#include "FormDesignerDoc.h"
#include "FormDesignerView.h"
#include "UFormControlObj.h"
#include "UFormDrawTool.h"

/////////////////////////////////////////////////////////////////////////////
// UFormDrawTool implementation

CPtrList UFormDrawTool::c_tools;

static UFormSelectTool			selectTool;
static UFormDrawRectTool		lineTool(line);
static UFormDrawRectTool		rectTool(rect);
static UFormDrawEditTool		editTool(edit);
static UFormDrawCheckboxTool	checkboxTool(checkbox);
static UFormDrawComboboxTool	comboboxTool(combobox);
static UFormDrawButtonTool		buttonTool(button);
static UFormDrawGroupTool		groupTool(group);
static UFormDrawStaticTool		staticToll(c_static);
static UFormDrawGridTool		gridTool(grid);
static UFormDrawRadiobuttonTool radioButtonTool(radiobutton);

CPoint UFormDrawTool::c_down;
UINT UFormDrawTool::c_nDownFlags;
CPoint UFormDrawTool::c_last;
DrawShape UFormDrawTool::c_drawShape = selection;

UFormDrawTool::UFormDrawTool(DrawShape drawShape)
{
	m_drawShape = drawShape;
	c_tools.AddTail(this);
}

UFormDrawTool::UFormDrawTool()
{
	c_tools.AddTail(this);
}


UFormDrawTool* UFormDrawTool::FindTool(DrawShape drawShape)
{
	POSITION pos = c_tools.GetHeadPosition();
	while (pos != NULL)
	{
		UFormDrawTool* pTool = (UFormDrawTool*)c_tools.GetNext(pos);
		if (pTool->m_drawShape == drawShape)
			return pTool;
	}

	return NULL;
}

void UFormDrawTool::OnLButtonDown(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{

	popView->SetCapture();
	c_nDownFlags = nFlags;
	c_down = point;
	c_last = point;
}

void UFormDrawTool::OnLButtonDblClk(FormDesignerView* /*popView*/, UINT /*nFlags*/, const CPoint& /*point*/)
{
}

void UFormDrawTool::OnLButtonUp(FormDesignerView* /*popView*/, UINT /*nFlags*/, const CPoint& point)
{
	ReleaseCapture();

	if (point == c_down)
		c_drawShape = selection;
}

void UFormDrawTool::OnMouseMove(FormDesignerView* /*popView*/, UINT /*nFlags*/, const CPoint& point)
{
	c_last = point;
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}

void UFormDrawTool::OnEditProperties(FormDesignerView* /*popView*/)
{
}

void UFormDrawTool::OnCancel()
{
	c_drawShape = selection;
}
void UFormDrawTool::ShowPropertyDlg(UCtrlProperties* popProperty, UFormControlObj *popCtrlObj, FormDesignerView* popView)
{
	if(popProperty == NULL || popCtrlObj == NULL || popView == NULL)
	{
		return;
	}
	if(pogPropertiesDlg == NULL)
	{
		pogPropertiesDlg = new PropertiesDlg(NULL, popProperty, popCtrlObj, popView->GetDocument()->pomForm->omTable, popView);
	}
	else
	{
		pogPropertiesDlg->SetObjectProperties(popProperty, popCtrlObj, popView->GetDocument()->pomForm->omTable, popView);
	}
}

////////////////////////////////////////////////////////////////////////////
// CResizeTool

enum SelectMode
{
	none,
	netSelect,
	move,
	size
};

SelectMode selectMode = none;
int nDragHandle;

CPoint lastPoint;

UFormSelectTool::UFormSelectTool()
	: UFormDrawTool(selection)
{
}

void UFormSelectTool::OnLButtonDown(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	CPoint local = point;
	popView->ClientToDoc(local);

	UFormControlObj* polObj;
	selectMode = none;

	// Check for resizing (only allowed on single selections)
	if (popView->omSelection.GetCount() == 1)
	{
		polObj = popView->omSelection.GetHead();
		nDragHandle = polObj->HitTest(local, popView, TRUE);
		if (nDragHandle != 0)
			selectMode = size;
	}

	// See if the click was on an object, select and start move if so
	if (selectMode == none)
	{
		polObj = popView->GetDocument()->ObjectAt(local);

		if (polObj != NULL)
		{
			selectMode = move;

			if (!popView->IsSelected(polObj))
			{
				popView->Select(polObj, (nFlags & MK_SHIFT) != 0);

				if (popView->omSelection.GetCount() == 1)
					popView->omSelection.GetHead()->OnOpen(popView);
				else
				{
					ShowPropertyDlg(NULL, NULL, NULL);
/*					if(pogPropertiesDlg != NULL)
					{
						pogPropertiesDlg->SetObjectProperties(NULL, NULL);
					}
*/
				}

/*				if(pogPropertiesDlg != NULL)
				{
					pogPropertiesDlg->SetObjectProperties(polObj->pomProperties);
				}
*/
			}
			// Ctrl+Click clones the selection...
			if ((nFlags & MK_CONTROL) != 0)
				popView->CloneSelection();
		}
	}

	// Click on background, start a net-selection
	if (selectMode == none)
	{
		if ((nFlags & MK_SHIFT) == 0)
			popView->Select(NULL);

		selectMode = netSelect;

		CClientDC dc(popView);
		CRect rect(point.x, point.y, point.x, point.y);
		rect.NormalizeRect();
		dc.DrawFocusRect(rect);
	}

	lastPoint = local;
	UFormDrawTool::OnLButtonDown(popView, nFlags, point);
}

void UFormSelectTool::OnLButtonDblClk(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	if ((nFlags & MK_SHIFT) != 0)
	{
		// Shift+DblClk deselects object...
		CPoint local = point;
		popView->ClientToDoc(local);
		UFormControlObj* polObj = popView->GetDocument()->ObjectAt(local);
		if (polObj != NULL)
			popView->Deselect(polObj);
	}
	else
	{
		// "Normal" DblClk opens properties, or OLE server...
		if (popView->omSelection.GetCount() == 1)
			popView->omSelection.GetHead()->OnOpen(popView);
		else if (popView->omSelection.GetCount() == 0)
		{
			ShowPropertyDlg(popView->GetDocument()->pomForm, (UFormControlObj *)popView, popView);
/*			if(pogPropertiesDlg != NULL)
			{
				pogPropertiesDlg->SetObjectProperties(popView->GetDocument()->pomForm, (UFormControlObj *)popView, popView->GetDocument()->pomForm->omTable);
			}
			else
			{
				pogPropertiesDlg = new PropertiesDlg(NULL, popView->GetDocument()->pomForm, (UFormControlObj *)popView, popView->GetDocument()->pomForm->omTable);
			}
*/
		}
		else
		{
			ShowPropertyDlg(NULL, NULL, NULL);
/*			if(pogPropertiesDlg != NULL)
			{
				pogPropertiesDlg->SetObjectProperties(NULL, NULL);
			}
*/
		}
	}

	UFormDrawTool::OnLButtonDblClk(popView, nFlags, point);
}

void UFormSelectTool::OnEditProperties(FormDesignerView* popView)
{
	if (popView->omSelection.GetCount() == 1)
		popView->omSelection.GetHead()->OnEditProperties(popView);
	else
	{
		ShowPropertyDlg(NULL, NULL, NULL);

/*		if(pogPropertiesDlg != NULL)
		{
			pogPropertiesDlg->SetObjectProperties(NULL, NULL);
		}
*/
	}
}

void UFormSelectTool::OnLButtonUp(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	if (popView->GetCapture() == popView)
	{
		if (selectMode == netSelect)
		{
			CClientDC dc(popView);
			CRect rect(c_down.x, c_down.y, c_last.x, c_last.y);
			rect.NormalizeRect();
			dc.DrawFocusRect(rect);

			popView->SelectWithinRect(rect, TRUE);
		}
		else if (selectMode != none)
		{
			popView->GetDocument()->UpdateAllViews(popView);
		}
	}
/*	if (popView->omSelection.GetCount() == 1)
	{
		UFormControlObj* polObj = popView->omSelection.GetHead();
		if(pogPropertiesDlg != NULL)
		{
			pogPropertiesDlg->SetObjectProperties(polObj->pomProperties, polObj);
		}
	}
*/
	UFormDrawTool::OnLButtonUp(popView, nFlags, point);
}

void UFormSelectTool::OnMouseMove(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	if (popView->GetCapture() != popView)
	{
		if (c_drawShape == selection && popView->omSelection.GetCount() == 1)
		{
			UFormControlObj* polObj = popView->omSelection.GetHead();
			CPoint local = point;
			popView->ClientToDoc(local);
			int nHandle = polObj->HitTest(local, popView, TRUE);
			if (nHandle != 0)
			{
				SetCursor(polObj->GetHandleCursor(nHandle));
				return; // bypass UFormDrawTool
			}
		}
		if (c_drawShape == selection)
			UFormDrawTool::OnMouseMove(popView, nFlags, point);
		return;
	}

	if (selectMode == netSelect)
	{
		CClientDC dc(popView);
		CRect rect(c_down.x, c_down.y, c_last.x, c_last.y);
		rect.NormalizeRect();
		dc.DrawFocusRect(rect);
		rect.SetRect(c_down.x, c_down.y, point.x, point.y);
		rect.NormalizeRect();
		dc.DrawFocusRect(rect);

		UFormDrawTool::OnMouseMove(popView, nFlags, point);
		return;
	}

	CPoint local = point;
	popView->ClientToDoc(local);
	CPoint delta = (CPoint)(local - lastPoint);

	POSITION pos = popView->omSelection.GetHeadPosition();
	while (pos != NULL)
	{
		UFormControlObj* polObj = popView->omSelection.GetNext(pos);
		CRect position = polObj->omPosition;

		if (selectMode == move)
		{
			position += delta;
			polObj->MoveTo(position, popView);
		}
		else if (nDragHandle != 0)
		{
			polObj->MoveHandleTo(nDragHandle, local, popView);
		}
	}

	lastPoint = local;

	if (selectMode == size && c_drawShape == selection)
	{
		c_last = point;
		SetCursor(popView->omSelection.GetHead()->GetHandleCursor(nDragHandle));
		return; // bypass UFormDrawTool
	}

	c_last = point;

	if (c_drawShape == selection)
		UFormDrawTool::OnMouseMove(popView, nFlags, point);
}

////////////////////////////////////////////////////////////////////////////
// UFormDrawRectTool (does rectangles, round-rectangles, and ellipses)

UFormDrawRectTool::UFormDrawRectTool(DrawShape drawShape)
	: UFormDrawTool(drawShape)
{
}

void UFormDrawRectTool::OnLButtonDown(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	UFormDrawTool::OnLButtonDown(popView, nFlags, point);

	CPoint local = point;
	popView->ClientToDoc(local);

	UFormRectControl* polObj = new UFormRectControl(CRect(local, CSize(0, 0)));
	switch (m_drawShape)
	{
	default:
		ASSERT(FALSE); // unsuported shape!

	case rect:
		{
			polObj->omShape = UFormRectControl::rectangle;
			polObj->pomProperties = (UCtrlProperties*) new URectCtrlProperties();
			int ilC = popView->GetDocument()->IncRectCounter();
			CString olC; olC.Format("%d", ilC);
			CString olName = polObj->pomProperties->omName;
			polObj->pomProperties->omName = olName + olC;
			popView->GetDocument()->AddCtrl((UCtrlProperties*)polObj->pomProperties);
		}
		break;

	case line:
		{
			polObj->omShape = UFormRectControl::line;
			polObj->pomProperties = (UCtrlProperties*) new ULineCtrlProperties();
			int ilC = popView->GetDocument()->IncLineCounter();
			CString olC; olC.Format("%d", ilC);
			CString olName = polObj->pomProperties->omName;
			polObj->pomProperties->omName = olName + olC;
			popView->GetDocument()->AddCtrl((UCtrlProperties*)polObj->pomProperties);
		}
		break;
	}
	popView->GetDocument()->Add(polObj);
	popView->Select(polObj);

	selectMode = size;
	nDragHandle = 1;
	lastPoint = local;
}

void UFormDrawRectTool::OnLButtonDblClk(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	UFormDrawTool::OnLButtonDblClk(popView, nFlags, point);
}

void UFormDrawRectTool::OnLButtonUp(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	if (point == c_down)
	{
		// Don't create empty objects...
		UFormControlObj *polObj = popView->omSelection.GetTail();
		popView->GetDocument()->Remove(polObj);
		polObj->Remove();
		selectTool.OnLButtonDown(popView, nFlags, point); // try a select!
	}

	selectTool.OnLButtonUp(popView, nFlags, point);
}

void UFormDrawRectTool::OnMouseMove(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_CROSS));
	selectTool.OnMouseMove(popView, nFlags, point);
}



/////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// UFormDrawEditTool (does edit controls)

UFormDrawEditTool::UFormDrawEditTool(DrawShape nDrawShape)
	: UFormDrawTool(nDrawShape)
{
}

void UFormDrawEditTool::OnLButtonDown(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	UFormDrawTool::OnLButtonDown(popView, nFlags, point);

	CPoint local = point;
	popView->ClientToDoc(local);

	UFormEditControl* polObj = new UFormEditControl(CRect(local, CSize(0, 0)));
	polObj->pomProperties = (UCtrlProperties*) new UEditCtrlProperties();
	popView->GetDocument()->Add(polObj);
	int ilC = popView->GetDocument()->IncEditCounter();
	CString olC; olC.Format("%d", ilC);
	CString olName = polObj->pomProperties->omName;
	polObj->pomProperties->omName = olName + olC;
	popView->GetDocument()->AddCtrl((UCtrlProperties*)polObj->pomProperties);
	popView->Select(polObj);

	selectMode = size;
	nDragHandle = 1;
	lastPoint = local;
}

void UFormDrawEditTool::OnLButtonDblClk(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	UFormDrawTool::OnLButtonDblClk(popView, nFlags, point);
}

void UFormDrawEditTool::OnLButtonUp(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	if (point == c_down)
	{
		// Don't create empty objects...
		UFormControlObj *polObj = popView->omSelection.GetTail();
		popView->GetDocument()->Remove(polObj);
		polObj->Remove();
		selectTool.OnLButtonDown(popView, nFlags, point); // try a select!
	}

	selectTool.OnLButtonUp(popView, nFlags, point);
}

void UFormDrawEditTool::OnMouseMove(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_CROSS));
	selectTool.OnMouseMove(popView, nFlags, point);
}


/////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// UFormDrawStaticTool (does edit controls)

UFormDrawStaticTool::UFormDrawStaticTool(DrawShape nDrawShape)
	: UFormDrawTool(nDrawShape)
{
}

void UFormDrawStaticTool::OnLButtonDown(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	UFormDrawTool::OnLButtonDown(popView, nFlags, point);

	CPoint local = point;
	popView->ClientToDoc(local);

	UFormStaticControl* polObj = new UFormStaticControl(CRect(local, CSize(0, 0)));
	polObj->pomProperties = (UCtrlProperties*) new UStaticCtrlProperties();
	popView->GetDocument()->Add(polObj);
	int ilC = popView->GetDocument()->IncStaticCounter();
	CString olC; olC.Format("%d", ilC);
	CString olName = polObj->pomProperties->omName;
	polObj->pomProperties->omName = olName + olC;
	popView->GetDocument()->AddCtrl((UCtrlProperties*)polObj->pomProperties);
	popView->Select(polObj);

	selectMode = size;
	nDragHandle = 1;
	lastPoint = local;
}

void UFormDrawStaticTool::OnLButtonDblClk(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	UFormDrawTool::OnLButtonDblClk(popView, nFlags, point);
}

void UFormDrawStaticTool::OnLButtonUp(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	if (point == c_down)
	{
		// Don't create empty objects...
		UFormControlObj *polObj = popView->omSelection.GetTail();
		popView->GetDocument()->Remove(polObj);
		polObj->Remove();
		selectTool.OnLButtonDown(popView, nFlags, point); // try a select!
	}

	selectTool.OnLButtonUp(popView, nFlags, point);
}

void UFormDrawStaticTool::OnMouseMove(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_CROSS));
	selectTool.OnMouseMove(popView, nFlags, point);
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// UFormDrawCheckboxTool (does checkbox controls)

UFormDrawCheckboxTool::UFormDrawCheckboxTool(DrawShape nDrawShape)
	: UFormDrawTool(nDrawShape)
{
}

void UFormDrawCheckboxTool::OnLButtonDown(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	UFormDrawTool::OnLButtonDown(popView, nFlags, point);

	CPoint local = point;
	popView->ClientToDoc(local);

	UFormCheckboxControl* polObj = new UFormCheckboxControl(CRect(local, CSize(0, 0)));
	polObj->pomProperties = (UCtrlProperties*)new UCheckboxCtrlProperties();
	int ilC = popView->GetDocument()->IncCheckboxCounter();
	CString olC; olC.Format("%d", ilC);
	CString olName = polObj->pomProperties->omName;
	polObj->pomProperties->omName = olName + olC;
	popView->GetDocument()->AddCtrl((UCtrlProperties*)polObj->pomProperties);

//	UFormCheckboxControl* polObj = new UFormCheckboxControl(CRect(local, CSize(0, 0)));
	popView->GetDocument()->Add(polObj);
	popView->Select(polObj);

	selectMode = size;
	nDragHandle = 1;
	lastPoint = local;
}

void UFormDrawCheckboxTool::OnLButtonDblClk(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	UFormDrawTool::OnLButtonDblClk(popView, nFlags, point);
}

void UFormDrawCheckboxTool::OnLButtonUp(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	if (point == c_down)
	{
		// Don't create empty objects...
		UFormControlObj *polObj = popView->omSelection.GetTail();
		popView->GetDocument()->Remove(polObj);
		polObj->Remove();
		selectTool.OnLButtonDown(popView, nFlags, point); // try a select!
	}

	selectTool.OnLButtonUp(popView, nFlags, point);
}

void UFormDrawCheckboxTool::OnMouseMove(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_CROSS));
	selectTool.OnMouseMove(popView, nFlags, point);
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// UFormDrawRadiobuttonTool (does radiobutton controls)

UFormDrawRadiobuttonTool::UFormDrawRadiobuttonTool(DrawShape nDrawShape)
	: UFormDrawTool(nDrawShape)
{
}

void UFormDrawRadiobuttonTool::OnLButtonDown(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	UFormDrawTool::OnLButtonDown(popView, nFlags, point);

	CPoint local = point;
	popView->ClientToDoc(local);

	UFormRadioButtonControl* polObj = new UFormRadioButtonControl(CRect(local, CSize(0, 0)));
	polObj->pomProperties = (UCtrlProperties*)new URadioCtrlProperties();
	int ilC = popView->GetDocument()->IncRadioCounter();
	CString olC; olC.Format("%d", ilC);
	CString olName = polObj->pomProperties->omName;
	polObj->pomProperties->omName = olName + olC;
	popView->GetDocument()->AddCtrl((UCtrlProperties*)polObj->pomProperties);

//	UFormCheckboxControl* polObj = new UFormCheckboxControl(CRect(local, CSize(0, 0)));
	popView->GetDocument()->Add(polObj);
	popView->Select(polObj);

	selectMode = size;
	nDragHandle = 1;
	lastPoint = local;
}

void UFormDrawRadiobuttonTool::OnLButtonDblClk(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	UFormDrawTool::OnLButtonDblClk(popView, nFlags, point);
}

void UFormDrawRadiobuttonTool::OnLButtonUp(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	if (point == c_down)
	{
		// Don't create empty objects...
		UFormControlObj *polObj = popView->omSelection.GetTail();
		popView->GetDocument()->Remove(polObj);
		polObj->Remove();
		selectTool.OnLButtonDown(popView, nFlags, point); // try a select!
	}

	selectTool.OnLButtonUp(popView, nFlags, point);
}

void UFormDrawRadiobuttonTool::OnMouseMove(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_CROSS));
	selectTool.OnMouseMove(popView, nFlags, point);
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// UFormDrawComboboxTool (does combobox controls)

UFormDrawComboboxTool::UFormDrawComboboxTool(DrawShape nDrawShape)
	: UFormDrawTool(nDrawShape)
{
}

void UFormDrawComboboxTool::OnLButtonDown(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	UFormDrawTool::OnLButtonDown(popView, nFlags, point);

	CPoint local = point;
	popView->ClientToDoc(local);

	UFormComboboxControl* polObj = new UFormComboboxControl(CRect(local, CSize(0, 0)));
	polObj->pomProperties = (UCtrlProperties*) new UComboboxCtrlProperties();
	popView->GetDocument()->Add(polObj);
	int ilC = popView->GetDocument()->IncComboboxCounter();
	CString olC; olC.Format("%d", ilC);
	CString olName = polObj->pomProperties->omName;
	polObj->pomProperties->omName = olName + olC;
	popView->GetDocument()->AddCtrl((UCtrlProperties*)polObj->pomProperties);
	popView->Select(polObj);

	selectMode = size;
	nDragHandle = 1;
	lastPoint = local;
}

void UFormDrawComboboxTool::OnLButtonDblClk(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	UFormDrawTool::OnLButtonDblClk(popView, nFlags, point);
}

void UFormDrawComboboxTool::OnLButtonUp(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	if (point == c_down)
	{
		// Don't create empty objects...
		UFormControlObj *polObj = popView->omSelection.GetTail();
		popView->GetDocument()->Remove(polObj);
		polObj->Remove();
		selectTool.OnLButtonDown(popView, nFlags, point); // try a select!
	}

	selectTool.OnLButtonUp(popView, nFlags, point);
}

void UFormDrawComboboxTool::OnMouseMove(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_CROSS));
	selectTool.OnMouseMove(popView, nFlags, point);
}


//UFormDrawButtonTool
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// UFormDrawComboboxTool (does combobox controls)

UFormDrawButtonTool::UFormDrawButtonTool(DrawShape nDrawShape)
	: UFormDrawTool(nDrawShape)
{
}

void UFormDrawButtonTool::OnLButtonDown(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	UFormDrawTool::OnLButtonDown(popView, nFlags, point);

	CPoint local = point;
	popView->ClientToDoc(local);

	UFormButtonControl* polObj = new UFormButtonControl(CRect(local, CSize(0, 0)));
	polObj->pomProperties = (UCtrlProperties*) new UButtonCtrlProperties();

	popView->GetDocument()->Add(polObj);
	int ilC = popView->GetDocument()->IncButtonCounter();
	CString olC; olC.Format("%d", ilC);
	CString olName = polObj->pomProperties->omName;
	polObj->pomProperties->omName = olName + olC;
	popView->GetDocument()->AddCtrl((UCtrlProperties*)polObj->pomProperties);

	popView->Select(polObj);

	selectMode = size;
	nDragHandle = 1;
	lastPoint = local;
}

void UFormDrawButtonTool::OnLButtonDblClk(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	UFormDrawTool::OnLButtonDblClk(popView, nFlags, point);
}

void UFormDrawButtonTool::OnLButtonUp(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	if (point == c_down)
	{
		// Don't create empty objects...
		UFormControlObj *polObj = popView->omSelection.GetTail();
		popView->GetDocument()->Remove(polObj);
		polObj->Remove();
		selectTool.OnLButtonDown(popView, nFlags, point); // try a select!
	}

	selectTool.OnLButtonUp(popView, nFlags, point);
}

void UFormDrawButtonTool::OnMouseMove(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_CROSS));
	selectTool.OnMouseMove(popView, nFlags, point);
}


//UFormDrawGroupTool
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// UFormDrawGroupTool (does group controls)

UFormDrawGroupTool::UFormDrawGroupTool(DrawShape nDrawShape)
	: UFormDrawTool(nDrawShape)
{
}

void UFormDrawGroupTool::OnLButtonDown(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	UFormDrawTool::OnLButtonDown(popView, nFlags, point);

	CPoint local = point;
	popView->ClientToDoc(local);

	UFormGroupControl* polObj = new UFormGroupControl(CRect(local, CSize(0, 0)));
	polObj->pomProperties = (UCtrlProperties*) new UGroupCtrlProperties();

	popView->GetDocument()->Add(polObj);
	int ilC = popView->GetDocument()->IncButtonCounter();
	CString olC; olC.Format("%d", ilC);
	CString olName = polObj->pomProperties->omName;
	polObj->pomProperties->omName = olName + olC;
	popView->GetDocument()->AddCtrl((UCtrlProperties*)polObj->pomProperties);

	popView->Select(polObj);

	selectMode = size;
	nDragHandle = 1;
	lastPoint = local;
}

void UFormDrawGroupTool::OnLButtonDblClk(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	UFormDrawTool::OnLButtonDblClk(popView, nFlags, point);
}

void UFormDrawGroupTool::OnLButtonUp(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	if (point == c_down)
	{
		// Don't create empty objects...
		UFormControlObj *polObj = popView->omSelection.GetTail();
		popView->GetDocument()->Remove(polObj);
		polObj->Remove();
		selectTool.OnLButtonDown(popView, nFlags, point); // try a select!
	}

	selectTool.OnLButtonUp(popView, nFlags, point);
}

void UFormDrawGroupTool::OnMouseMove(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_CROSS));
	selectTool.OnMouseMove(popView, nFlags, point);
}


//UFormDrawGridTool
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// UFormDrawGridTool (does grid controls)

UFormDrawGridTool::UFormDrawGridTool(DrawShape nDrawShape)
	: UFormDrawTool(nDrawShape)
{
}

void UFormDrawGridTool::OnLButtonDown(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	UFormDrawTool::OnLButtonDown(popView, nFlags, point);

	CPoint local = point;
	popView->ClientToDoc(local);

	UFormGridControl* polObj = new UFormGridControl(CRect(local, CSize(0, 0)));
	polObj->pomProperties = (UCtrlProperties*) new UGridCtrlProperties();

	popView->GetDocument()->Add(polObj);
	int ilC = popView->GetDocument()->IncButtonCounter();
	CString olC; olC.Format("%d", ilC);
	CString olName = polObj->pomProperties->omName;
	polObj->pomProperties->omName = olName + olC;
	popView->GetDocument()->AddCtrl((UCtrlProperties*)polObj->pomProperties);

	popView->Select(polObj);

	selectMode = size;
	nDragHandle = 1;
	lastPoint = local;
}

void UFormDrawGridTool::OnLButtonDblClk(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	UFormDrawTool::OnLButtonDblClk(popView, nFlags, point);
}

void UFormDrawGridTool::OnLButtonUp(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	if (point == c_down)
	{
		// Don't create empty objects...
		UFormControlObj *polObj = popView->omSelection.GetTail();
		popView->GetDocument()->Remove(polObj);
		polObj->Remove();
		selectTool.OnLButtonDown(popView, nFlags, point); // try a select!
	}

	selectTool.OnLButtonUp(popView, nFlags, point);
}

void UFormDrawGridTool::OnMouseMove(FormDesignerView* popView, UINT nFlags, const CPoint& point)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_CROSS));
	selectTool.OnMouseMove(popView, nFlags, point);
}
