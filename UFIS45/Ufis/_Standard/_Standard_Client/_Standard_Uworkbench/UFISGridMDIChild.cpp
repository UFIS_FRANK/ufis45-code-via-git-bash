// UFISGridMDIChild.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "UFISGridMDIChild.h"
#include "UFISGridViewerDoc.h"
#include "PrivList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// UFISGridMDIChild

IMPLEMENT_DYNCREATE(UFISGridMDIChild, CMDIChildWnd)

UFISGridMDIChild::UFISGridMDIChild()
{
	pomGridDef = NULL;
	blIsInit = false;
}

UFISGridMDIChild::~UFISGridMDIChild()
{
}

//-------------------------------------------------------------
// MWO
// Depending on SEC show or hide the toolbar buttons
//-------------------------------------------------------------
void UFISGridMDIChild::SetSecurity(UGridDefinition *popGridDef)
{
	char clStat;
	CString olCopy, olInsert, olDelete, olUpdate, olView;
	olCopy = pomGridDef->GridName + "_COPY";
	olInsert = pomGridDef->GridName + "_INSERT";
	olDelete = pomGridDef->GridName + "_DELETE";
	olUpdate = pomGridDef->GridName + "_UPDATE";
	olView = pomGridDef->GridName + "_VIEWDEF";
	clStat = ogPrivList.GetStat(olCopy);
	if(clStat != '1')
	{
		m_wndToolBar.GetToolBarCtrl().EnableButton(ID_COPY, FALSE);
		m_wndToolBar.GetToolBarCtrl().SetState(ID_COPY, (TBSTATE_INDETERMINATE|~TBSTATE_ENABLED|~TBSTATE_HIDDEN));
	}
	clStat = ogPrivList.GetStat(olInsert);
	if(clStat != '1')
	{
		m_wndToolBar.GetToolBarCtrl().EnableButton(ID_NEW_RECORD, FALSE);
		m_wndToolBar.GetToolBarCtrl().SetState(ID_NEW_RECORD, (TBSTATE_INDETERMINATE|~TBSTATE_ENABLED));
	}
	clStat = ogPrivList.GetStat(olDelete);
	if(clStat != '1')
	{
		m_wndToolBar.GetToolBarCtrl().EnableButton(ID_DELETE, FALSE);
		m_wndToolBar.GetToolBarCtrl().SetState(ID_DELETE, (TBSTATE_INDETERMINATE|~TBSTATE_ENABLED));
	}
	clStat = ogPrivList.GetStat(olUpdate);
	if(clStat != '1')
	{
		m_wndToolBar.GetToolBarCtrl().EnableButton(ID_UPDATE, FALSE);
		m_wndToolBar.GetToolBarCtrl().SetState(ID_UPDATE, (TBSTATE_INDETERMINATE|~TBSTATE_ENABLED));
	}
	clStat = ogPrivList.GetStat(olView);
	if(clStat != '1')
	{
		m_wndToolBar.GetToolBarCtrl().EnableButton(ID_LOAD_CONDITION_DLG, FALSE);
		m_wndToolBar.GetToolBarCtrl().SetState(ID_LOAD_CONDITION_DLG, (TBSTATE_INDETERMINATE|~TBSTATE_ENABLED));
	}
	ShowControlBar ( &m_wndToolBar, TRUE, FALSE );
}

void UFISGridMDIChild::SetDefinition(UGridDefinition *popGridDef, int ipOffset)
{
	pomGridDef = popGridDef;
	//Der Offset ist einfach ermittelt. keine Ahnung warum das nicht anders geht???!!!!
	int ilOffSet = 0;
/*	if(popGridDef->left != -1 && popGridDef->top != -1 && popGridDef->right != -1  && popGridDef->bottom != -1)
	{
		CRect olRect = CRect(popGridDef->left, popGridDef->top-ilOffSet, popGridDef->right, popGridDef->bottom-ilOffSet);
		MoveWindow(&olRect);
	}
*/
/*	if(pomGridDef->AsBasicData == TRUE)
	{
		if(m_wndToolBar.LoadToolBar(IDR_BASICDATA_BAR))
		{
			TRACE0("Failed to create toolbar\n");
			return;      // fail to create
		}
		// TODO: Remove this if you don't want tool tips
	}
	else
	{
		if(!m_wndToolBar.LoadToolBar(IDR_UNIVERSAL_GRID_TOOLBAR))
		{
			TRACE0("Failed to create toolbar\n");
			return;      // fail to create
		}
		
		// TODO: Remove this if you don't want tool tips
	}
*/
//	ShowControlBar ( &m_wndToolBar, TRUE, FALSE );

	blIsInit = true;

}


BEGIN_MESSAGE_MAP(UFISGridMDIChild, CMDIChildWnd)
	//{{AFX_MSG_MAP(UFISGridMDIChild)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_MOVE()
	ON_COMMAND(ID_VIEW, OnView)
	ON_COMMAND(ID_UNIGRID_FILTER, OnFilter)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UFISGridMDIChild message handlers

BOOL UFISGridMDIChild::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CMDIChildWnd::PreCreateWindow(cs);
}

void UFISGridMDIChild::ActivateFrame(int nCmdShow) 
{
	//((CUWorkBenchApp*)AfxGetApp())->pMainFrame->LoadFrame(IDR_UFIS_GRID);
	//CMDIChildWnd::LoadFrame(IDR_UFIS_GRID);
	CMDIChildWnd::ActivateFrame(nCmdShow);
}

int UFISGridMDIChild::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	CUFISGridViewerDoc	*polDoc = (CUFISGridViewerDoc*)GetActiveDocument();
	if(polDoc != NULL)
	{

		if(polDoc->pomGridDef->AsBasicData == TRUE)
		{
			if (!m_wndToolBar.CreateEx(this) ||
				!m_wndToolBar.LoadToolBar(IDR_BASICDATA_BAR))
			{
				TRACE0("Failed to create toolbar\n");
				return -1;      // fail to create
			}
			// TODO: Remove this if you don't want tool tips
			m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
				CBRS_TOOLTIPS | CBRS_FLYBY);

		}
		else
		{
			if (!m_wndToolBar.CreateEx(this) ||
				!m_wndToolBar.LoadToolBar(IDR_UNIVERSAL_GRID_TOOLBAR))
			{
				TRACE0("Failed to create toolbar\n");
				return -1;      // fail to create
			}
			// TODO: Remove this if you don't want tool tips
			m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
				CBRS_TOOLTIPS | CBRS_FLYBY);
		}

		pomGridDef = polDoc->pomGridDef;
		//Der Offset ist einfach ermittelt. keine Ahnung warum das nicht anders geht???!!!!
		int ilOffSet = 0;
		if(pomGridDef->left != -1 && pomGridDef->top != -1 && pomGridDef->right != -1  && pomGridDef->bottom != -1)
		{
			CRect olRect = CRect(pomGridDef->left, pomGridDef->top-ilOffSet, pomGridDef->right, pomGridDef->bottom-ilOffSet);
			MoveWindow(&olRect);
		}

		blIsInit = true;
	}
	return 0;
}



void UFISGridMDIChild::OnView()
{
//	::MessageBox(NULL, "View", "View", MB_OK);
}

void UFISGridMDIChild::OnFilter()
{
//	::MessageBox(NULL, "Filter", "Filter", MB_OK);
}

void UFISGridMDIChild::OnNew()
{
}
void UFISGridMDIChild::OnUpdate()
{
}
void UFISGridMDIChild::OnDelete()
{
}
void UFISGridMDIChild::OnSize(UINT nType, int cx, int cy) 
{
	CMDIChildWnd::OnSize(nType, cx, cy);

	if(blIsInit == true)
	{
		if(pomGridDef != NULL)
		{
			CRect olRect;
			GetWindowRect(&olRect);
			CRect olR2;
			GetClientRect(&olR2);
			//ClientToScreen(&olRect);
			pomGridDef->left = olRect.left;
			pomGridDef->top	 = olRect.top;
			pomGridDef->right = olRect.right;
			pomGridDef->bottom = olRect.bottom;
		}
	}
}

void UFISGridMDIChild::OnMove(int x, int y) 
{
	CMDIChildWnd::OnMove(x, y);
	if(blIsInit == true)
	{
		if(pomGridDef != NULL)
		{
			CRect olRect;
			GetWindowRect(&olRect);
			CRect olR2;
			GetClientRect(&olR2);
			//ClientToScreen(&olRect);
			pomGridDef->left = olRect.left;
			pomGridDef->top	 = olRect.top;
			pomGridDef->right = olRect.right;
			pomGridDef->bottom = olRect.bottom;
		}
	}
}

BOOL UFISGridMDIChild::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
{
	CUFISGridViewerDoc	*polDoc = (CUFISGridViewerDoc*)GetActiveDocument();
//	if(polDoc != NULL)
//	{

//		if(polDoc->pomGridDef->AsBasicData == TRUE)
		{
			if (!m_wndToolBar.CreateEx(this) ||
				!m_wndToolBar.LoadToolBar(IDR_BASICDATA_BAR))
			{
				TRACE0("Failed to create toolbar\n");
				return -1;      // fail to create
			}
			// TODO: Remove this if you don't want tool tips
			m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
				CBRS_TOOLTIPS | CBRS_FLYBY);
		}
/*		else
		{
			if (!m_wndToolBar.CreateEx(this) ||
				!m_wndToolBar.LoadToolBar(IDR_UNIVERSAL_GRID_TOOLBAR))
			{
				TRACE0("Failed to create toolbar\n");
				return -1;      // fail to create
			}
			// TODO: Remove this if you don't want tool tips
			m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
				CBRS_TOOLTIPS | CBRS_FLYBY);
		}

		pomGridDef = polDoc->pomGridDef;
		//Der Offset ist einfach ermittelt. keine Ahnung warum das nicht anders geht???!!!!
		int ilOffSet = 0;
		if(pomGridDef->left != -1 && pomGridDef->top != -1 && pomGridDef->right != -1  && pomGridDef->bottom != -1)
		{
			CRect olRect = CRect(pomGridDef->left, pomGridDef->top-ilOffSet, pomGridDef->right, pomGridDef->bottom-ilOffSet);
			MoveWindow(&olRect);
		}
*/
		blIsInit = true;
//	}
	
	return CMDIChildWnd::OnCreateClient(lpcs, pContext);
}


void UFISGridMDIChild::OnClose() 
{
	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen und/oder Standard aufrufen
	if(pomGridDef != NULL)
	{
		((CUWorkBenchApp*)AfxGetApp())->pMainFrame->RemoveGridMDI(pomGridDef->GridName);
		ogTools.SetTablesForGridAndForm(pomGridDef, false);
	}
	
	CMDIChildWnd::OnClose();
}
