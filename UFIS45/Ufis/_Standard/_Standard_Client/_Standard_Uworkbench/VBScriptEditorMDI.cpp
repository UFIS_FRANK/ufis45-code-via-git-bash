// VBScriptEditorMDI.cpp : implementation file
//

#include "stdafx.h"
#include "uworkbench.h"
#include "VBScriptEditorMDI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// VBScriptEditorMDI

IMPLEMENT_DYNCREATE(VBScriptEditorMDI, CMDIChildWnd)

VBScriptEditorMDI::VBScriptEditorMDI()
{
}

VBScriptEditorMDI::~VBScriptEditorMDI()
{
}

void VBScriptEditorMDI::SetScriptName(CString opScriptName)
{
	omScriptName = opScriptName;
}


BEGIN_MESSAGE_MAP(VBScriptEditorMDI, CMDIChildWnd)
	//{{AFX_MSG_MAP(VBScriptEditorMDI)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// VBScriptEditorMDI message handlers
	

void VBScriptEditorMDI::OnClose() 
{
	((CUWorkBenchApp*)AfxGetApp())->pMainFrame->RemoveScriptMDI(omScriptName);
	
	CMDIChildWnd::OnClose();
}
