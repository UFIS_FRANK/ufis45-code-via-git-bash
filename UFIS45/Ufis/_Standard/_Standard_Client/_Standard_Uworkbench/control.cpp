// Machine generated IDispatch wrapper class(es) created by Microsoft Visual C++

// NOTE: Do not modify the contents of this file.  If this class is regenerated by
//  Microsoft Visual C++, your modifications will be overwritten.


#include "stdafx.h"
#include "control.h"


/////////////////////////////////////////////////////////////////////////////
// CControl properties

/////////////////////////////////////////////////////////////////////////////
// CControl operations

void CControl::SetCancel(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x80010038, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL CControl::GetCancel()
{
	BOOL result;
	InvokeHelper(0x80010038, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void CControl::SetControlSource(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x80018001, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString CControl::GetControlSource()
{
	CString result;
	InvokeHelper(0x80018001, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void CControl::SetControlTipText(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x80010045, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString CControl::GetControlTipText()
{
	CString result;
	InvokeHelper(0x80010045, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void CControl::SetDefault(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x80010037, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL CControl::GetDefault()
{
	BOOL result;
	InvokeHelper(0x80010037, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void CControl::_SetHeight(long Height)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x60020008, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Height);
}

void CControl::_GetHeight(long* Height)
{
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x60020009, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Height);
}

void CControl::SetHeight(float newValue)
{
	static BYTE parms[] =
		VTS_R4;
	InvokeHelper(0x80010006, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

float CControl::GetHeight()
{
	float result;
	InvokeHelper(0x80010006, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
	return result;
}

void CControl::SetHelpContextID(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x80010032, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long CControl::GetHelpContextID()
{
	long result;
	InvokeHelper(0x80010032, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long CControl::GetLayoutEffect()
{
	long result;
	InvokeHelper(0x80018004, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void CControl::_SetLeft(long Left)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x60020011, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Left);
}

void CControl::_GetLeft(long* Left)
{
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x60020012, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Left);
}

void CControl::SetLeft(float newValue)
{
	static BYTE parms[] =
		VTS_R4;
	InvokeHelper(0x80010003, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

float CControl::GetLeft()
{
	float result;
	InvokeHelper(0x80010003, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
	return result;
}

void CControl::SetName(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString CControl::GetName()
{
	CString result;
	InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void CControl::_GetOldHeight(long* OldHeight)
{
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x60020017, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 OldHeight);
}

float CControl::GetOldHeight()
{
	float result;
	InvokeHelper(0x80018005, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
	return result;
}

void CControl::_GetOldLeft(long* OldLeft)
{
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x60020019, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 OldLeft);
}

float CControl::GetOldLeft()
{
	float result;
	InvokeHelper(0x80018006, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
	return result;
}

void CControl::_GetOldTop(long* OldTop)
{
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x6002001b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 OldTop);
}

float CControl::GetOldTop()
{
	float result;
	InvokeHelper(0x80018007, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
	return result;
}

void CControl::_GetOldWidth(long* OldWidth)
{
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x6002001d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 OldWidth);
}

float CControl::GetOldWidth()
{
	float result;
	InvokeHelper(0x80018008, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
	return result;
}

LPDISPATCH CControl::GetObject()
{
	LPDISPATCH result;
	InvokeHelper(0x80018009, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH CControl::GetParent()
{
	LPDISPATCH result;
	InvokeHelper(0x80010008, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void CControl::SetRowSource(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x8001800e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString CControl::GetRowSource()
{
	CString result;
	InvokeHelper(0x8001800e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void CControl::SetRowSourceType(short nNewValue)
{
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x8001800f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

short CControl::GetRowSourceType()
{
	short result;
	InvokeHelper(0x8001800f, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
	return result;
}

void CControl::SetTabIndex(short nNewValue)
{
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x8001000f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

short CControl::GetTabIndex()
{
	short result;
	InvokeHelper(0x8001000f, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
	return result;
}

void CControl::SetTabStop(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x8001000e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL CControl::GetTabStop()
{
	BOOL result;
	InvokeHelper(0x8001000e, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void CControl::SetTag(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x8001000b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString CControl::GetTag()
{
	CString result;
	InvokeHelper(0x8001000b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void CControl::_SetTop(long Top)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6002002b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Top);
}

void CControl::_GetTop(long* Top)
{
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x6002002c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Top);
}

void CControl::SetTop(float newValue)
{
	static BYTE parms[] =
		VTS_R4;
	InvokeHelper(0x80010004, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

float CControl::GetTop()
{
	float result;
	InvokeHelper(0x80010004, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
	return result;
}

void CControl::SetVisible(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x80010007, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL CControl::GetVisible()
{
	BOOL result;
	InvokeHelper(0x80010007, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void CControl::_SetWidth(long Width)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x60020033, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Width);
}

void CControl::_GetWidth(long* Width)
{
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x60020034, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Width);
}

void CControl::SetWidth(float newValue)
{
	static BYTE parms[] =
		VTS_R4;
	InvokeHelper(0x80010005, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

float CControl::GetWidth()
{
	float result;
	InvokeHelper(0x80010005, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
	return result;
}

void CControl::Move(const VARIANT& Left, const VARIANT& Top, const VARIANT& Width, const VARIANT& Height, const VARIANT& Layout)
{
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x80018100, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 &Left, &Top, &Width, &Height, &Layout);
}

void CControl::ZOrder(const VARIANT& zPosition)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x80018105, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 &zPosition);
}

void CControl::SetFocus()
{
	InvokeHelper(0x80018103, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

long CControl::_GethWnd()
{
	long result;
	InvokeHelper(0x6002003b, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long CControl::_GetID()
{
	long result;
	InvokeHelper(0x6002003c, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void CControl::_Move(long Left, long Top, long Width, long Height)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_I4 VTS_I4;
	InvokeHelper(0x6002003d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Left, Top, Width, Height);
}

void CControl::_ZOrder(long zPosition)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6002003e, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 zPosition);
}
