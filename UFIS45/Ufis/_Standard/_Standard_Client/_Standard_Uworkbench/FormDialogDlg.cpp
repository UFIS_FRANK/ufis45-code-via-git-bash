// FormDialogDlg.cpp : implementation file
//
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	UWorkBench - Project
 *	
 *		Runtime Form dialog
 *		
 *
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *
 *		Date		Author			Comment
 *		-------------------------------------------------------------
 *
 *		??/??/??	mwo AAT/ID		Initial version
 *		01/11/00	cla AAT/ID		MFC controls substituted by ActiveX
 *									controls. For step 2 of workbench
 *		15/11/00	cla AAT/ID		Insertion of code into script eng. added
 *		01/02/01	cla	AAT/ID		NCombo of uhi added
 *		19/02/01	cla AAT/ID		AATEdit5 control substitutes CCSEdit
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "stdafx.h"
#include "uworkbench.h"
#include "FormDialogDlg.h"
#include "UCheckBox.h"
#include "UComboBox.h"
#include "UObjectDataDefinitions.h"
#include "UWorkbench.h"
#include "NCombo.h"
#include "CommandButton.h"
#include "ComButtonSink.h"
#include "aatedit5.h"
#include "EditSink.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FormDialogDlg dialog


FormDialogDlg::FormDialogDlg(CWnd* pParent, CString opMode, UFormProperties *popForm, CString opKey, UGridDefinition *popGridDef)
	: CDialog(FormDialogDlg::IDD, pParent)
{
	pomGridDef = popGridDef;
	pomForm = popForm;
	omMode = opMode;
	omKey = opKey;
	imID = 0;
	imCurrentRow = -1;
	//{{AFX_DATA_INIT(FormDialogDlg)
	//}}AFX_DATA_INIT
}

FormDialogDlg::~FormDialogDlg()
{
	/*
	POSITION pos = omControls.GetHeadPosition();
	while (pos != NULL)
	{
		RunTimeCtrl *polCtrl = omControls.GetNext(pos);*/
		/*
		 * Here we have to unadvise and delete the Sink class
		 */
		//polCtrl->Unadvise(IID_IComButtonSink);
		/*delete polCtrl->pomSink;
		delete polCtrl->pomControl;

		delete polCtrl;
	}*/
	omMiniGrids.DeleteAll();
	omBrush.DeleteObject();
	/*
	 * <<<cla: reset script control
	 */
	((CUWorkBenchApp*)AfxGetApp())->m_pScriptHost->pScriptControl->Reset();

}

void FormDialogDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FormDialogDlg)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FormDialogDlg, CDialog)
	//{{AFX_MSG_MAP(FormDialogDlg)
	ON_WM_DESTROY()
	ON_COMMAND(31, DeleteGridLine)
	ON_MESSAGE(GX_ROWCOL_CHANGED, ChangedGridCell)
	ON_MESSAGE(GX_CELL_R_BUTTONCLICKED, ClickedGridRButton)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FormDialogDlg message handlers

BOOL FormDialogDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	if(pomForm == NULL)
		return TRUE;
	bool bmFirstFocus = false;
	CWnd *polFirstFocusObject = NULL;
	CRect olMaxRect; // zum anpassen des Dialogs an das ben�tigte Ausma�
	olMaxRect.top = 0;
	olMaxRect.left = 0;
	LPUNKNOWN polUnk = NULL; //used for the Unknown pointer of the Control

	this->SetWindowText(pomForm->omName);

	/*
	 * cla: Inject the associated code into the Script eng.
	 */
	if(InjectCodeIntoScriptHost(pomForm->omName) != true)
	{
		/*
		 * A serious error occured -> Script code is not loaded
		 * how to recover now
		 */
		AfxMessageBox(_T("A serious error occured while loading the Script code\nTerminating execution of the Form"),MB_OK|MB_ICONSTOP);
		return(FALSE);
	}

	if(pomGridDef != NULL)
	{
		if(omMode == "INSERT")
		{
			RecordSet olEmptyRecord(ogBCD.GetFieldCount(pomForm->omTable));
			omCurrentRecord = olEmptyRecord;
		}
		else // "UPDATE" and "INSERT"
		{
			ogBCD.GetRecord(pomForm->omTable, "URNO", omKey, omCurrentRecord);
		}
	}
	for(int i = 0; i < pomForm->omProperties.GetSize(); i++)
	{
		CRect olTrueRect = pomForm->omProperties[i].omRect;
		olTrueRect.top += 30;
		olTrueRect.bottom += 30;

		if(olMaxRect.right < olTrueRect.right + 30)
		{
			olMaxRect.right = olTrueRect.right + 30;
		}
		if(olMaxRect.bottom < olTrueRect.bottom + 30)
		{
			olMaxRect.bottom = olTrueRect.bottom + 30;
		}

		if(pomForm->omProperties[i].omCtrlType == "button")
		{
			RunTimeCtrl *polCtrl = new RunTimeCtrl();
			/*
			 * <<<cla:
			 * Old code removed
			 * CButton *polObj = new CButton();
			 */

			CCommandButton *polObj = new CCommandButton();
			polObj->Create(_T("Forms.CommandButton.1"),NULL,
		                    WS_VISIBLE,pomForm->omProperties[i].omRect,this,++imID);
			/*
			 * <<<cla:
			 * CWnd Create is different than Create for a COM Control
			 * polObj->Create(pomForm->omProperties[i].omLabel,(WS_CHILD|WS_VISIBLE|WS_TABSTOP|BS_PUSHBUTTON) , olTrueRect, this, ++imID);
			 * polObj->SetFont(& ogMSSansSerif_Regular_8);
			 */
			polObj->SetCaption(pomForm->omProperties[i].omLabel);
			polObj->SetBackColor(pomForm->omProperties[i].omBackColor);
			/*
			 * Calculate width and heigth
			 */
			CPoint olTopLeft;
			int ilWidth,ilHeight;
			olTopLeft = pomForm->omProperties[i].omRect.TopLeft();
			ilWidth = pomForm->omProperties[i].omRect.Width();
			ilHeight = pomForm->omProperties[i].omRect.Height();
			/*
			 * <<<cla:
			 * Workaround for small bug in CCommandButton
			 * or someweher else
			 */
			polObj->SetWindowPos(this,olTopLeft.x,olTopLeft.y,ilWidth,ilHeight,SWP_SHOWWINDOW);
			LPUNKNOWN polUnk = NULL; //used for the Unknown pointer of the Control
			polUnk = polObj->GetControlUnknown();
			/*
			 * <<<cla:
			 * Create the Sink object
			 */
			CComButtonSink *polSink;

			polSink = new CComButtonSink();

			BOOL blRes = polSink->Advise(polUnk,IID_IComButtonSink);
			polCtrl->pomSink = polSink;
			polCtrl->pomControl = (CWnd*)polObj;
			polCtrl->pomProperties = &pomForm->omProperties[i];
			polCtrl->pomFieldInfo = pomForm->GetFieldInfo(pomForm->omProperties[i].omDBField, pomForm->omTable);
			if(bmFirstFocus == false && pomForm->omProperties[i].bmVisible == TRUE){polFirstFocusObject = polObj;bmFirstFocus=true;}
			omControls.AddTail(polCtrl);
			polSink->SetScriptHost(((CUWorkBenchApp*)AfxGetApp())->m_pScriptHost);
			polSink->SetCtrl(polCtrl);
			/*
			 * Add the control to the Script
			 */
			_bstr_t olCtrlName(pomForm->omProperties[i].omName.GetBuffer(pomForm->omProperties[i].omName.GetLength()));
			((CUWorkBenchApp*)AfxGetApp())->m_pScriptHost->
			pScriptControl->AddObject(olCtrlName,(IDispatch*)polUnk,FALSE);

			if(pomForm->omProperties[i].bmVisible == FALSE)
			{
				polObj->ShowWindow(SW_HIDE);
			}
			if(pomForm->omProperties[i].bmEnabled == FALSE)
			{
				polObj->EnableWindow(FALSE);
			}
		}
		if(pomForm->omProperties[i].omCtrlType == "group")
		{
			RunTimeCtrl *polCtrl = new RunTimeCtrl();
			URadioButton *polObj = new URadioButton(this, pomForm->omProperties[i].omBackColor, pomForm->omProperties[i].omFontColor);
			polObj->Create(pomForm->omProperties[i].omLabel,(WS_CHILD|WS_VISIBLE|BS_GROUPBOX) , olTrueRect, this, ++imID);
			polObj->SetFont(& ogMSSansSerif_Regular_8);
			polCtrl->pomControl = (CWnd*)polObj;
			polCtrl->pomProperties = &pomForm->omProperties[i];
			polCtrl->pomFieldInfo = pomForm->GetFieldInfo(pomForm->omProperties[i].omDBField, pomForm->omTable);
			if(bmFirstFocus == false && pomForm->omProperties[i].bmVisible == TRUE){polFirstFocusObject = polObj;bmFirstFocus=true;}
			omControls.AddTail(polCtrl);
			if(pomForm->omProperties[i].bmVisible == FALSE)
			{
				polObj->ShowWindow(SW_HIDE);
			}
			if(pomForm->omProperties[i].bmEnabled == FALSE)
			{
				polObj->EnableWindow(FALSE);
			}
		}
		if(pomForm->omProperties[i].omCtrlType == "edit")
		{
			RunTimeCtrl *polCtrl = new RunTimeCtrl();
			//CCSEdit *polObj = new CCSEdit();

			CAatedit5 *polObj = new CAatedit5();
			
			int ilStyle;
			ilStyle = WS_CHILD|WS_VISIBLE;
			CRect olRect = pomForm->omProperties[i].omRect;
			if(pomForm->omProperties[i].bmEnabled == TRUE)
			{
				ilStyle |= WS_TABSTOP;
			}
			if(pomForm->omProperties[i].bmMultiLine == TRUE)
			{
				ilStyle |= ES_MULTILINE;
			}
			CRect olTrueRect = pomForm->omProperties[i].omRect;
			olTrueRect.top = olTrueRect.top + 30;
			olTrueRect.bottom = olTrueRect.bottom + 30;
//			polObj->CreateEx( WS_EX_CLIENTEDGE,_T("EDIT"), "", ilStyle, olTrueRect, this, ++imID);
//			polObj->SetFont(& ogMSSansSerif_Regular_8);
			//polCtrl->pomControl = (CWnd*)polObj;
			//polCtrl->pomProperties = &pomForm->omProperties[i];

			polObj->Create(_T("AATEDIT5.aatedit5ctrl.1"),_T(" "),WS_BORDER|WS_CHILD|WS_VISIBLE|WS_TABSTOP,
		             olTrueRect,this,++imID);
			//polObj->SetWindowPos(this,olTrueRect.left,olTrueRect.top,100,20,SWP_SHOWWINDOW);
			polCtrl->pomControl = (CWnd*)polObj;
			polCtrl->pomProperties = &pomForm->omProperties[i];

			SetCCSEditTpye(polObj, polCtrl->pomProperties);
			LPUNKNOWN polUnk = NULL; //used for the Unknown pointer of the Control
			polUnk = polObj->GetControlUnknown();
			/*
			 * <<<cla:
			 * Create the Sink object
			 */
			CEditSink *polSink;

			polSink = new CEditSink();
			BOOL blRes = polSink->Advise(polUnk,IID_DAatedit5Events);
			polCtrl->pomSink = polSink;
			polCtrl->pomFieldInfo = pomForm->GetFieldInfo(pomForm->omProperties[i].omDBField, pomForm->omTable);
			if(bmFirstFocus == false && pomForm->omProperties[i].bmVisible == TRUE){polFirstFocusObject = polObj;bmFirstFocus=true;}
			omControls.AddTail(polCtrl);
			polSink->SetScriptHost(((CUWorkBenchApp*)AfxGetApp())->m_pScriptHost);
			polSink->SetCtrl(polCtrl);
			/*
			 * Add the control to the Script
			 */
			CString	olCtrlNameCStr = pomForm->omProperties[i].omName.Right(((pomForm->omProperties[i].omName.GetLength())-
						                                       (pomForm->omProperties[i].omName.Find('.')))-1);

			_bstr_t olCtrlName(olCtrlNameCStr.GetBuffer(olCtrlNameCStr.GetLength()));
			((CUWorkBenchApp*)AfxGetApp())->m_pScriptHost->
			pScriptControl->AddObject(olCtrlName,(IDispatch*)polUnk,FALSE);

			if(pomForm->omProperties[i].bmVisible == FALSE)
			{
				polObj->ShowWindow(SW_HIDE);
			}
			if(pomForm->omProperties[i].bmEnabled == FALSE)
			{
				polObj->EnableWindow(FALSE);
			}
		}
		if(pomForm->omProperties[i].omCtrlType == "checkbox")
		{
			RunTimeCtrl *polCtrl = new RunTimeCtrl();
			UCheckBox *polObj = new UCheckBox(this, pomForm->omProperties[i].omBackColor, pomForm->omProperties[i].omFontColor);
			polObj->Create(pomForm->omProperties[i].omLabel,(WS_CHILD|WS_VISIBLE|WS_TABSTOP|BS_AUTOCHECKBOX ) , olTrueRect, this, ++imID);
			polObj->SetFont(& ogMSSansSerif_Regular_8);
			polCtrl->pomControl = (CWnd*)polObj;
			polCtrl->pomProperties = &pomForm->omProperties[i];
			polCtrl->pomFieldInfo = pomForm->GetFieldInfo(pomForm->omProperties[i].omDBField, pomForm->omTable);
			if(bmFirstFocus == false && pomForm->omProperties[i].bmVisible == TRUE){polFirstFocusObject = polObj;bmFirstFocus=true;}
			omControls.AddTail(polCtrl);
			if(pomForm->omProperties[i].bmVisible == FALSE)
			{
				polObj->ShowWindow(SW_HIDE);
			}
			if(pomForm->omProperties[i].bmEnabled == FALSE)
			{
				polObj->EnableWindow(FALSE);
			}
		}  
		if(pomForm->omProperties[i].omCtrlType == "radio")
		{
			RunTimeCtrl *polCtrl = new RunTimeCtrl();
			URadioButton *polObj = new URadioButton(this, pomForm->omProperties[i].omBackColor, pomForm->omProperties[i].omFontColor);
			
			if(pomForm->omProperties[i].bmIsMaster == TRUE)
			{
				polObj->Create(pomForm->omProperties[i].omLabel,(WS_CHILD|WS_VISIBLE|WS_TABSTOP|BS_AUTORADIOBUTTON|WS_GROUP) , olTrueRect, this, ++imID);
			}
			else
			{
				polObj->Create(pomForm->omProperties[i].omLabel,(WS_CHILD|WS_VISIBLE|WS_TABSTOP|BS_AUTORADIOBUTTON) , olTrueRect, this, ++imID);
			}
			polObj->SetFont(& ogMSSansSerif_Regular_8);
			polCtrl->pomControl = (CWnd*)polObj;
			polCtrl->pomProperties = &pomForm->omProperties[i];
			polCtrl->pomFieldInfo = pomForm->GetFieldInfo(pomForm->omProperties[i].omDBField, pomForm->omTable);
			if(bmFirstFocus == false && pomForm->omProperties[i].bmVisible == TRUE){polFirstFocusObject = polObj;bmFirstFocus=true;}
			omControls.AddTail(polCtrl);
			if(pomForm->omProperties[i].bmVisible == FALSE)
			{
				polObj->ShowWindow(SW_HIDE);
			}
			if(pomForm->omProperties[i].bmEnabled == FALSE)
			{
				polObj->EnableWindow(FALSE);
			}
		}
		if(pomForm->omProperties[i].omCtrlType == "combobox")
		{
			RunTimeCtrl *polCtrl = new RunTimeCtrl();
			CRect olTrueRect = pomForm->omProperties[i].omRect;
			olTrueRect.top = olTrueRect.top + 30;
			olTrueRect.bottom = olTrueRect.top + 150;
			
            /*
			 * <<<cla: Code removed for Workbench step 2
			 *
			 *UComboBox *polObj = new UComboBox(this, pomForm->omProperties[i].omBackColor, pomForm->omProperties[i].omFontColor);
			 */
			CNCombo *polObj = new CNCombo(); 
			polObj->Create(_T("NCOMBO.NComboCtrl.1"),NULL,
		                    WS_VISIBLE|WS_TABSTOP|CBS_DROPDOWN|CBS_AUTOHSCROLL|WS_VSCROLL|CBS_SORT,olTrueRect,this,++imID);

			CRect olCBRect = olTrueRect;
			olCBRect.bottom = olCBRect.top + 150;
//			polObj->Create((WS_VISIBLE|WS_TABSTOP|CBS_DROPDOWN|CBS_AUTOHSCROLL|WS_VSCROLL|CBS_SORT), olCBRect, this, ++imID);
//			polObj->SetFont(& ogMSSansSerif_Regular_8);
			CPoint olTopLeft;
			int ilWidth,ilHeight;
			olTopLeft = pomForm->omProperties[i].omRect.TopLeft();
			ilWidth = pomForm->omProperties[i].omRect.Width();
			ilHeight = pomForm->omProperties[i].omRect.Height();
			/*
			 * <<<cla:
			 * Workaround for small bug in CCommandButton
			 * or someweher else
			 */
			//polObj->SetWindowPos(this,olTopLeft.x,olTopLeft.y,ilWidth,ilHeight+150,SWP_SHOWWINDOW);

			polCtrl->pomControl = (CWnd*)polObj;
			polCtrl->pomProperties = &pomForm->omProperties[i];
			polCtrl->pomFieldInfo = pomForm->GetFieldInfo(pomForm->omProperties[i].omDBField, pomForm->omTable);
			if(bmFirstFocus == false && pomForm->omProperties[i].bmVisible == TRUE){polFirstFocusObject = polObj;bmFirstFocus=true;}
			omControls.AddTail(polCtrl);
			LPUNKNOWN polUnk = NULL; //used for the Unknown pointer of the Control
			polUnk = polObj->GetControlUnknown();

			/*
			 * Add the control to the Script
			 */
			CString	olCtrlNameCStr = pomForm->omProperties[i].omName.Right(((pomForm->omProperties[i].omName.GetLength())-
						                                       (pomForm->omProperties[i].omName.Find('.')))-1);

			_bstr_t olCtrlName(olCtrlNameCStr.GetBuffer(olCtrlNameCStr.GetLength()));
			((CUWorkBenchApp*)AfxGetApp())->m_pScriptHost->
			pScriptControl->AddObject(olCtrlName,(IDispatch*)polUnk,FALSE);


			if(pomForm->omProperties[i].bmVisible == FALSE)
			{
				polObj->ShowWindow(SW_HIDE);
			}
			if(pomForm->omProperties[i].bmEnabled == FALSE)
			{
				polObj->EnableWindow(FALSE);
			}
			if(polCtrl->pomFieldInfo != NULL)
			{
				if(!polCtrl->pomFieldInfo->Refe.IsEmpty())
				{
					CString olTable = ogTools.GetTableName(polCtrl->pomFieldInfo->Refe);
					CString olField = ogTools.GetFieldName(polCtrl->pomFieldInfo->Refe);
					CString olShadow = ogTools.GetFieldName(polCtrl->pomFieldInfo->HiddenRefe);
					if(!olField.IsEmpty() && !olTable.IsEmpty())
					{
						int ilDataCount = ogBCD.GetDataCount(olTable);
						polObj->InsertColumn(polObj->GetColumnCount(),"Col1",250,1,TRUE,TRUE);
						polObj->InsertColumn(polObj->GetColumnCount(),"Col2",0,1,TRUE,FALSE);

						for(int k = 0; k < ilDataCount; k++)
						{
							RecordSet olRecord;
							CString   olValue;
							long olItem;
							ogBCD.GetRecord(olTable, k,  olRecord);
							int ilIdx = -1;
							ilIdx = ogBCD.GetFieldIndex(olTable, olShadow);
							if(ilIdx != -1)
							{
								olValue = olRecord[ilIdx];
								olItem = polObj->InsertItem(polObj->GetItemCount(),"");
								polObj->SetItemText(olItem,0,olValue);

							}
							ilIdx = ogBCD.GetFieldIndex(olTable, olField);
							if(ilIdx != -1)
							{
								olValue = olRecord[ilIdx];
								polObj->SetItemText(olItem,1,olValue);

							}

						}

					}
				}
			}
		}
		if(pomForm->omProperties[i].omCtrlType == "static")
		{
			RunTimeCtrl *polCtrl = new RunTimeCtrl();
			UStatic *polObj = new UStatic(this, pomForm->omProperties[i].omBackColor, pomForm->omProperties[i].omFontColor);
			polObj->Create(pomForm->omProperties[i].omLabel,(WS_CHILD|WS_VISIBLE), olTrueRect, this, ++imID);
			polObj->SetFont(& ogMSSansSerif_Regular_8);
			polCtrl->pomControl = (CWnd*)polObj;
			polCtrl->pomProperties = &pomForm->omProperties[i];
			polCtrl->pomFieldInfo = pomForm->GetFieldInfo(pomForm->omProperties[i].omDBField, pomForm->omTable);
			omControls.AddTail(polCtrl);
			if(pomForm->omProperties[i].bmVisible == FALSE)
			{
				polObj->ShowWindow(SW_HIDE);
			}
			if(pomForm->omProperties[i].bmEnabled == FALSE)
			{
				polObj->EnableWindow(FALSE);
			}
		}
		if(pomForm->omProperties[i].omCtrlType == "grid")
		{
			UGridCtrlProperties *polGridProperty = (UGridCtrlProperties *)&(pomForm->omProperties[i]);
			RunTimeCtrl *polCtrl = new RunTimeCtrl();
			GxGridTab *polObj = new GxGridTab(this, "FORM_DLG");
			polObj->Create((WS_BORDER|WS_CHILD|WS_VISIBLE|WS_VSCROLL|WS_HSCROLL), olTrueRect, this, ++imID);
			polObj->Initialize();
			polCtrl->pomControl = (CWnd*)polObj;
			polCtrl->pomProperties = (UCtrlProperties *)polGridProperty;
			omControls.AddTail(polCtrl);
			if(polGridProperty->bmVisible == FALSE)
			{
				polObj->ShowWindow(SW_HIDE);
			}
			CGXStyle olHeaderStyle;
			polObj->SetRowCount(100);
			polObj->SetColCount(polGridProperty->omHaederlist.GetSize());

			//Column Sizes
			for(int j = 0; j < polGridProperty->omHeadersizes.GetSize(); j++)
			{
				polObj->SetColWidth(j+1,j+1,atoi(polGridProperty->omHeadersizes[j].GetBuffer(0)));
			}
			// Set the grid header text
			for( j = 0; j < polGridProperty->omHaederlist.GetSize(); j++)
			{
				olHeaderStyle.SetValue(polGridProperty->omHaederlist[j]);
				polObj->SetStyleRange(CGXRange(0, j+1, 0, j+1 ), olHeaderStyle);
			}
			if(pomForm->omProperties[i].bmEnabled == FALSE)
			{
				polObj->EnableWindow(FALSE);
			}

		}
		if(pomForm->omProperties[i].omCtrlType == "line")
		{
		}
		if(pomForm->omProperties[i].omCtrlType == "rectangle")
		{
		}
		if(pomForm->omProperties[i].omCtrlType == "listbox")
		{
			//TO DO
		} 
	}
	if(polFirstFocusObject != NULL)
	{
		polFirstFocusObject->SetFocus();
	}
	olMaxRect.right = pomForm->omRect.right;
	olMaxRect.bottom = pomForm->omRect.bottom;
	MoveWindow(&olMaxRect);
	CenterWindow();
	if(pomGridDef != NULL)
	{
		ShowData();
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void FormDialogDlg::OnDestroy() 
{
	POSITION pos = omControls.GetHeadPosition();
	while (pos != NULL)
	{
		RunTimeCtrl *polCtrl = omControls.GetNext(pos);
		/*
		 * Here we have to unadvise and delete the Sink class
		 */
		//polCtrl->Unadvise(IID_IComButtonSink);
		delete polCtrl->pomSink;
		delete polCtrl->pomControl;

		delete polCtrl;
	}

	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	
}
//void FormDialogDlg::SetCCSEditTpye(CCSEdit *popEdit, UCtrlProperties *popProperty)
void FormDialogDlg::SetCCSEditTpye(CAatedit5 *popEdit, UCtrlProperties *popProperty)
{
	bool blMandatory = false;
	popEdit->SetBkColor(popProperty->omBackColor);
	popEdit->SetTextColor(popProperty->omFontColor);
	if(popProperty->omFormatString.IsEmpty())
	{
		if(popProperty->omFieldType == "TRIM")
		{
			if(popProperty->imTextLimit != 0)
			{
				if(popProperty->bmMandatory == TRUE)
					popEdit->SetTypeToString("", popProperty->imTextLimit, 1);
				else
					popEdit->SetTypeToString("", popProperty->imTextLimit, 0);
			}
			else
			{
				popEdit->SetTypeToString("",-1,-1);
			}
		}
		else if(popProperty->omFieldType == "DATE")
		{
			if(popProperty->bmMandatory == TRUE)
			{
				blMandatory = true;
			}
			popEdit->SetTypeToDate(blMandatory);
		}
		else if(popProperty->omFieldType == "TIME")
		{
			if(popProperty->bmMandatory == TRUE)
			{
				blMandatory = true;
			}
			popEdit->SetTypeToTime(blMandatory, false);
		}
		//MIN2HHMM	: DB -> 90min == GUI -> 01:30 or 0130
		else if(popProperty->omFieldType == "MIN2HHMM")
		{
			if(popProperty->bmMandatory == TRUE)
			{
				blMandatory = true;
			}
			popEdit->SetTypeToTime(blMandatory, false);
		}
		//MIN2HH		: DB -> 90min == GUI -> 1.5h
		else if(popProperty->omFieldType == "MIN2HH")
		{
			popEdit->SetTypeToDouble(3,3,-999999.0,999999.0);
		}
		//HHMM2MIN	: DB -> 01:30 or 01300 == GUI 90min
		else if(popProperty->omFieldType == "HHMM2MIN")
		{
			popEdit->SetTypeToInt(-32767,32767);
			if(popProperty->bmMandatory == TRUE )
			{
				popEdit->SetTextLimit(0, 4, false);
				popEdit->SetRange(0, 9999);
			}
			else
			{
				popEdit->SetTextLimit(0, 4, true);
				popEdit->SetRange(0, 9999);
			}
		}
		//HHMM2HH		: DB -> 01:30 or 01300 == GUI -> 1.5h
		else if(popProperty->omFieldType == "HHMM2HH")
		{
			popEdit->SetTypeToDouble(3,3,-999999.0,999999.0);
		}
		//HH2MIN		: DB -> 1.5h == GUI -> 90min
		else if(popProperty->omFieldType == "HH2MIN")
		{
			popEdit->SetTypeToInt(-32767,32767);
			if(popProperty->bmMandatory == TRUE )
			{
				popEdit->SetTextLimit(0, 4, false);
				popEdit->SetRange(0, 9999);
			}
			else
			{
				popEdit->SetTextLimit(0, 4, true);
				popEdit->SetRange(0, 9999);
			}
		}
		//HH2HHMM		: DB -> 1.5h == 01:30 or 0130
		else if(popProperty->omFieldType == "HH2HHMM")
		{
			if(popProperty->bmMandatory == TRUE)
			{
				blMandatory = true;
			}
			popEdit->SetTypeToTime(blMandatory, false);
		}
		else if(popProperty->omFieldType == "INT")
		{
			long ilTo;
			CString olTo;
			for(int ilC = 0; ilC < popProperty->imTextLimit; ilC++)
			{
				olTo += "9";
			}
			ilTo = atol(olTo);
			popEdit->SetTypeToInt(-32767,32767);
			if(popProperty->imTextLimit != 0)
			{
				if(popProperty->bmMandatory == TRUE )
				{
					popEdit->SetTextLimit(0, popProperty->imTextLimit, false);
					popEdit->SetRange(0, ilTo);
				}
				else
				{
					popEdit->SetTextLimit(0, popProperty->imTextLimit, true);
					popEdit->SetRange(0, ilTo);
				}
			}
		}
		else if(popProperty->omFieldType == "LONG")
		{
			long ilTo;
			CString olTo;
			for(int ilC = 0; ilC < popProperty->imTextLimit; ilC++)
			{
				olTo += "9";
			}
			ilTo = atol(olTo);
			popEdit->SetTypeToInt(-32767,32767);
			if(popProperty->imTextLimit != 0)
			{
				if(popProperty->bmMandatory == TRUE )
				{
					popEdit->SetTextLimit(0, popProperty->imTextLimit, false);
					popEdit->SetRange(0, ilTo);
				}
				else
				{
					popEdit->SetTextLimit(0, popProperty->imTextLimit, true);
					popEdit->SetRange(0, ilTo);
				}
			}
		}
		else if(popProperty->omFieldType == "DOUBLE")
		{
			popEdit->SetTypeToDouble(3,3,-999999.0,999999.0);
		}
	}
	else
	{ 
		popEdit->SetRegularExpression(popProperty->omFormatString);
		if(popProperty->bmMandatory == TRUE)
		{
			popEdit->SetTypeToString("", popProperty->imTextLimit, 1);
		}
		else
		{
			popEdit->SetTypeToString("", popProperty->imTextLimit, 0);
		}
	}
	popEdit->SetTextErrColor(RED);
}

bool FormDialogDlg::InsertFormData()
{
	if(pomForm == NULL )//|| omKey == CString(""))
	{
		return false; // Dann macht es keinen Sinn weiter zu machen
	}
	

	CString olErrorText;

	RecordSet olCurrentRecord(ogBCD.GetFieldCount(pomForm->omTable));
	bool olFormOk = true;
	POSITION pos = omControls.GetHeadPosition();
	while (pos != NULL)
	{ 
		RunTimeCtrl *polCtrl = omControls.GetNext(pos);
		if(polCtrl != NULL)
		{
			if(polCtrl->pomProperties != NULL)
			{
				if(!polCtrl->pomProperties->omDBField.IsEmpty())
				{
					CString olValue;
	//********** radio button
					if(polCtrl->pomProperties->omCtrlType == "radio" )
					{
						int ilIdx=-1;
						ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polCtrl->pomProperties->omDBField);
						if(ilIdx != -1)
						{
							if(polCtrl->pomProperties->bmIsMaster == TRUE)
							{
								CCSPtrArray<RunTimeCtrl> olChilds;
								int ilCount;
								int ilIndex=-1;
								bool blIsChecked = false;
								int ilCheckIndex=-1;
								int ilCheck=0;
								ilCount = GetAllChildsForMaster(polCtrl->pomProperties->omName, olChilds);
								olChilds.NewAt(0, *polCtrl);
								ilCount++;
								for( int i = 0; i < ilCount; i++ )
								{
									URadioButton *polRadio = (URadioButton*)olChilds[i].pomControl;
									ilCheck = polRadio->GetCheck();
									if(ilCheck == 1)
									{
										olValue = olChilds[i].pomProperties->omCheckedValue;
										i = ilCount;
									}
								}
								olChilds.DeleteAll();
								olCurrentRecord.Values[ilIdx] = olValue;
							}
						}
					}// CtrlType == radio
					else
					{
						if(HasChildFields(polCtrl) == false) //because childs are handled directly to avoid confusing with field orders
						{
							int ilIdx=-1;
							ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polCtrl->pomProperties->omDBField);
							if(ilIdx != -1)
							{
		//********** checkbox
								if(polCtrl->pomProperties->omCtrlType == "checkbox")
								{
									CButton *polButton = (CButton *)polCtrl->pomControl;
									if(polButton->GetCheck() == 1)
									{
										olValue = polCtrl->pomProperties->omCheckedValue;
									}
									else
									{
										olValue = "0"; //DO DO provide a omFalseValue
									}
									olCurrentRecord.Values[ilIdx] = olValue;
								}
		//********** edit
								else if(polCtrl->pomProperties->omCtrlType == "edit" )
								{
									//CCSEdit *polEdit = (CCSEdit *)polCtrl->pomControl;
									CAatedit5 *polEdit = (CAatedit5 *)polCtrl->pomControl;
									//olValue = polEdit->GetWindowText();
									if(polEdit->GetStatus() == TRUE)
									{
										if(MakeDBValue(polCtrl->pomProperties, olValue, olErrorText) == true)
										{
											olCurrentRecord.Values[ilIdx] = olValue;
										}
									}
									else
									{
										if(polCtrl->pomProperties->bmMandatory == FALSE)
										{
											olErrorText+= polCtrl->pomProperties->omLabel + CString(":   Incorrect Format!\n");
											olFormOk = false;
										}
										else
										{
											if(olValue.IsEmpty())
											{
												olErrorText+= polCtrl->pomProperties->omLabel + CString(":   Field is mandatory!\n");
												olFormOk = false;
											}
											else
											{
												olErrorText+= polCtrl->pomProperties->omLabel + CString(":   Incorrect Format!\n");
												olFormOk = false;
											}
										}
									}
								}
		//********** combobox
								else if(polCtrl->pomProperties->omCtrlType == "combobox")
								{
									/*
									 * <<<cla: Changed for the new combo
									 * ((UComboBox *)polCtrl->pomControl)->GetWindowText(olValue);
									 */
									CNCombo *olCombo = static_cast<CNCombo*>(polCtrl->pomControl);

									/*
									 * Get the data from the Combo
									 */
									long olItemSel = olCombo->GetSelectItem();
									olValue = olCombo->GetItemText(olItemSel,1); //don't take the shadow field

									olCurrentRecord.Values[ilIdx] = olValue;

									if(polCtrl->pomProperties->bmMandatory == TRUE)
									{
										if(olValue.IsEmpty())
										{
											olErrorText+= polCtrl->pomProperties->omLabel + CString(":   Field is mandatory!\n");
											olFormOk = false;
										}
									}
								}
							}
						}//if(ilIdx != -1)
					}//if(HasChildFields(polCtrl) == false) //because childs are handled directly to avoid confusing with field orders
				}//if(!polCtrl->pomProperties->omDBField.IsEmpty())
//********** Child Fields
				else
				{
					if(polCtrl->pomProperties->omCtrlType == "edit")
					{
						if(!polCtrl->pomProperties->omMasterName.IsEmpty())
						{
							if(MakeMasterValue(polCtrl, olCurrentRecord, olErrorText) == false)
							{
								olFormOk = false;
							}
						}
					}
				}//else of if(!polCtrl->pomProperties->omDBField.IsEmpty())
			}
		}
	}
//******* Check the grids
	CStringArray olGridMessageList;
	for( int i = 0; i < omMiniGrids.GetSize(); i++ )
	{
		if( omMiniGrids[i].CheckGrid(olGridMessageList) == false)
		{
			olFormOk = false;
		}
	}
//******* End grid check
	if(olFormOk == true)
	{
		int ilIdx=-1;
		ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, "URNO");
		if(ilIdx != -1)
		{ 
			long llUrno = ogBCD.GetNextUrno();//ogBasicData.GetNextUrno();
			CString olLastErrorMsg;
 			CString olUrno;
			olUrno.Format("%ld", llUrno);
			omKey = olUrno;
		 	olCurrentRecord.Values[ilIdx] = olUrno;
			ogBCD.InsertRecord(pomForm->omTable, olCurrentRecord, true);
			olLastErrorMsg = ogBCD.GetLastError();
			if(!olLastErrorMsg.IsEmpty())
			{
				CString olUserMsg;				
				olUserMsg = PrepareUniqueConstraintMessage(olLastErrorMsg);
				AfxMessageBox(olUserMsg);
				return false;
			}
			else
			{
			// Save the grid values
				for( int i = 0; i < omMiniGrids.GetSize(); i++ )
				{
					omMiniGrids[i].SetForeignValue(olUrno);
					omMiniGrids[i].PerformSave();
				}
			}
		}
	}
	else
	{
		MessageBox(olErrorText, "Error", MB_OK);
	}
	return olFormOk;
}//----------------------InsertFormData()

bool FormDialogDlg::UpdateFormData()
{
	CString olErrorText;
	bool olFormOk = true;
	POSITION pos = omControls.GetHeadPosition();
	while (pos != NULL)
	{ 
		RunTimeCtrl *polCtrl = omControls.GetNext(pos);
		if(polCtrl != NULL)
		{
			if(polCtrl->pomProperties != NULL)
			{
				if(!polCtrl->pomProperties->omDBField.IsEmpty())
				{
					CString olValue;
	//********** radio button
					if(polCtrl->pomProperties->omCtrlType == "radio" )
					{
						int ilIdx=-1;
						ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polCtrl->pomProperties->omDBField);
						if(ilIdx != -1)
						{
							if(polCtrl->pomProperties->bmIsMaster == TRUE)
							{
								CCSPtrArray<RunTimeCtrl> olChilds;
								int ilCount;
								int ilIndex=-1;
								bool blIsChecked = false;
								int ilCheckIndex=-1;
								int ilCheck=0;
								ilCount = GetAllChildsForMaster(polCtrl->pomProperties->omName, olChilds);
								olChilds.NewAt(0, *polCtrl);
								ilCount++;
								for( int i = 0; i < ilCount; i++ )
								{
									URadioButton *polRadio = (URadioButton*)olChilds[i].pomControl;
									ilCheck = polRadio->GetCheck();
									if(ilCheck == 1)
									{
										olValue = olChilds[i].pomProperties->omCheckedValue;
										i = ilCount;
									}
								}
								olChilds.DeleteAll();
								omCurrentRecord.Values[ilIdx] = olValue;
							}
						}
					}
					else
					{
						if(HasChildFields(polCtrl) == false) //because childs are handled directly to avoid confusing with field orders
						{
							int ilIdx=-1;
							ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polCtrl->pomProperties->omDBField);
							if(ilIdx != -1)
							{
		//********** checkbox
								if(polCtrl->pomProperties->omCtrlType == "checkbox")
								{
									CButton *polButton = (CButton *)polCtrl->pomControl;
									if(polButton->GetCheck() == 1)
									{
										olValue = polCtrl->pomProperties->omCheckedValue;
									}
									else
									{
										olValue = "0"; //DO DO provide a omFalseValue
									}
									omCurrentRecord.Values[ilIdx] = olValue;
								}
		//********** edit
								else if(polCtrl->pomProperties->omCtrlType == "edit" )
								{
									//CCSEdit *polEdit = (CCSEdit *)polCtrl->pomControl;
									CAatedit5 *polEdit = (CAatedit5 *)polCtrl->pomControl;
									olValue = polEdit->GetWindowText();
									if(polEdit->GetStatus() == TRUE)
									{
										if(MakeDBValue(polCtrl->pomProperties, olValue, olErrorText) == true)
										{
											omCurrentRecord.Values[ilIdx] = olValue;
										}
									}
									else
									{
										if(polCtrl->pomProperties->bmMandatory == FALSE)
										{
											olErrorText+= polCtrl->pomProperties->omLabel + CString(":   Incorrect Format!\n");
											olFormOk = false;
										}
										else
										{
											if(olValue.IsEmpty())
											{
												olErrorText+= polCtrl->pomProperties->omLabel + CString(":   Field is mandatory!\n");
												olFormOk = false;
											}
											else
											{
												olErrorText+= polCtrl->pomProperties->omLabel + CString(":   Incorrect Format!\n");
												olFormOk = false;
											}
										}
									}
								}
		//********** combobox
								else if(polCtrl->pomProperties->omCtrlType == "combobox")
								{
									/*
									 * <<<cla: Changed for the new combo
									 * ((UComboBox *)polCtrl->pomControl)->GetWindowText(olValue);
									 */
									CNCombo *olCombo = static_cast<CNCombo*>(polCtrl->pomControl);

									/*
									 * Get the data from the Combo
									 */
									long olItemSel = olCombo->GetSelectItem();
									olValue = olCombo->GetItemText(olItemSel,1); //don't take the shadow field

									omCurrentRecord.Values[ilIdx] = olValue;

									if(polCtrl->pomProperties->bmMandatory == TRUE)
									{
										if(olValue.IsEmpty())
										{
											olErrorText+= polCtrl->pomProperties->omLabel + CString(":   Field is mandatory!\n");
											olFormOk = false;
										}
									}
								}
							}//if(ilIdx != -1)
						}//if(HasChildFields(polCtrl) == false) //because childs are handled directly to avoid confusing with field orders
					}
				}//if(!polCtrl->pomProperties->omDBField.IsEmpty())
//********** Child Fields
				else
				{
					if(polCtrl->pomProperties->omCtrlType == "edit")
					{
						if(!polCtrl->pomProperties->omMasterName.IsEmpty())
						{
							if(MakeMasterValue(polCtrl, omCurrentRecord, olErrorText) == false)
							{
								olFormOk = false;
							}
						}
					}
				}//else of if(!polCtrl->pomProperties->omDBField.IsEmpty())
			}
		}
	}
	
//******* Check the grids
	CStringArray olGridMessageList;
	for( int i = 0; i < omMiniGrids.GetSize(); i++ )
	{
		if( omMiniGrids[i].CheckGrid(olGridMessageList) == false)
		{
			olFormOk = false;
		}
	}
//******* End grid check

	if(olFormOk == true)
	{
		CString olUrno;

		int ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, "URNO");
		if(ilIdx != -1)
		{
			olUrno = omCurrentRecord[ilIdx];

			ogBCD.SetRecord(pomForm->omTable, "URNO", olUrno, omCurrentRecord.Values, true );
			CString olLastErrorMsg;
			olLastErrorMsg = ogBCD.GetLastError();
			if(!olLastErrorMsg.IsEmpty())
			{
				CString olUserMsg;				
				olUserMsg = PrepareUniqueConstraintMessage(olLastErrorMsg);
				AfxMessageBox(olUserMsg);
				return false;
			}
			else
			{
				// Save the grid values
				for( int i = 0; i < omMiniGrids.GetSize(); i++ )
				{
					omMiniGrids[i].PerformSave();
				}
			}
			POSITION pos = omControls.GetHeadPosition();
			while (pos != NULL)
			{
				RunTimeCtrl *polCtrl = omControls.GetNext(pos);
				FillControl(polCtrl, omCurrentRecord);
			}// end while (pos != NULL)
		}
	}
	else
	{
		for( i = 0; i < olGridMessageList.GetSize(); i++ )
		{
			olErrorText += olGridMessageList[i];
		}
		MessageBox(olErrorText, "Error", MB_OK);
	}
	return olFormOk;
}

void FormDialogDlg::DeleteFormData()
{
}

void FormDialogDlg::ShowData()
{
	if(omMode == "INSERT")
	{
		RecordSet olEmptyRecord(ogBCD.GetFieldCount(pomForm->omTable));
		omCurrentRecord = olEmptyRecord;
	}

	POSITION pos = omControls.GetHeadPosition();
	while (pos != NULL)
	{
		RunTimeCtrl *polCtrl = omControls.GetNext(pos);
		FillControl(polCtrl, omCurrentRecord);
	}

}
void FormDialogDlg::FillControl(RunTimeCtrl *popCtrl, RecordSet &ropRecord)
{
	if(popCtrl->pomControl == NULL)
	{
		return;
	}
	if(popCtrl->pomProperties != NULL)
	{
		if(popCtrl->pomProperties->omCtrlType == "grid")
		{
			FillGrid(popCtrl, (UGridCtrlProperties *)popCtrl->pomProperties, ropRecord);
		}
		if(popCtrl->pomProperties->omCtrlType == "radio")
		{
			if(popCtrl->pomProperties->bmIsMaster == TRUE)
			{
				MakeMasterChild(popCtrl, ropRecord);
			}
		}
		else
		{
			if(!popCtrl->pomProperties->omDBField.IsEmpty())
			{
				int ilIdx=-1;
				ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, popCtrl->pomProperties->omDBField);
				if(ilIdx != -1)
				{
					CString olValue = ropRecord.Values[ilIdx];
					if(popCtrl->pomProperties->omCtrlType == "checkbox")
					{
						CButton *polButton = (CButton *)popCtrl->pomControl;
						if(olValue == popCtrl->pomProperties->omCheckedValue)
						{
							polButton->SetCheck(1);
						}
						else
						{
							polButton->SetCheck(0);
						}
					}
					else if(popCtrl->pomProperties->omCtrlType == "edit" )
					{
						MakeEditValue(popCtrl, ropRecord);
//						((CCSEdit *)(popCtrl->pomControl))->SetInitText(olValue, false);
						CAatedit5 *polObj = static_cast<CAatedit5*>(popCtrl->pomControl);
						polObj->SetInitText(olValue,FALSE);

					}
					else if(popCtrl->pomProperties->omCtrlType == "combobox")
					{
						CNCombo *polObj = static_cast<CNCombo*>(popCtrl->pomControl);

						//popCtrl->pomControl->SetWindowText(olValue);
						  
						  //polObj->InsertColumn(polObj->GetColumnCount(),"Col1",250,1,TRUE,TRUE);
						  //long olItem = polObj->InsertItem(polObj->GetItemCount(),"");
						  //polObj->SetItemText(olItem,0,olValue);
						polObj->SetSelectItem(polObj->FindItemText(0,olValue));

					}
				}
			}
			else
			{
				if(popCtrl->pomProperties->omCtrlType == "edit")
				{
					if(!popCtrl->pomProperties->omMasterName.IsEmpty())
					{
						MakeMasterChild(popCtrl, ropRecord);
					}
				}
			}
		}
	}// end if(polCtrl->pomProperties != NULL)
}
void FormDialogDlg::FillGrid(RunTimeCtrl *polCtrl, 
							   UGridCtrlProperties *popGridProperties, 
							   RecordSet &ropRecord)
{
	GxGridTab *polGrid = (GxGridTab *)polCtrl->pomControl;
	MiniGridInformation *polGridInfo = NULL;
	polGrid->GetParam()->EnableUndo(FALSE);
	BOOL bLockOld = polGrid->LockUpdate(TRUE);

	int ilRowCount = polGrid->GetRowCount();
	if(ilRowCount > 1)
	{
		polGrid->RemoveRows(1, ilRowCount);
	}
	CCSPtrArray<RecordSet> olGridRecords;
	if(!popGridProperties->omGridTable.IsEmpty())
	{
		if(!popGridProperties->omMasterDetail.IsEmpty())
		{
			CStringArray olRelParts; //Relationparts STF.URNO=SPE.SURN ==> Master [0] = STF.URNO Detail [1] = SPE.SURN
			ExtractItemList(popGridProperties->omMasterDetail, &olRelParts, '=');
			if(olRelParts.GetSize() == 2)
			{
				CString olMasterTable = ogTools.GetTableName(olRelParts[0]);
				CString olMasterField = ogTools.GetFieldName(olRelParts[0]);
				CString olDetailTable = ogTools.GetTableName(olRelParts[1]);
				CString olDetailField = ogTools.GetFieldName(olRelParts[1]);
				if(!olMasterTable.IsEmpty() && !olMasterField.IsEmpty() && !olDetailTable.IsEmpty() && !olDetailField.IsEmpty())
				{
					int ilMasterFieldIdx = ogBCD.GetFieldIndex(olMasterTable, olMasterField);
					if(ilMasterFieldIdx >= 0)
					{
						polGridInfo = new MiniGridInformation();
						CString olKeyValue = ropRecord[ogBCD.GetFieldIndex(olMasterTable, olMasterField)];
						polGridInfo->MakeLines(popGridProperties, polGrid, 
											   olMasterTable, olMasterField, 
											   olDetailTable, olDetailField, olKeyValue);
						omMiniGrids.Add(polGridInfo);

					}
				}
			}
		}
	}
	polGrid->LockUpdate(bLockOld);
	polGrid->Redraw();
	polGrid->Invalidate();
	olGridRecords.DeleteAll();
}

bool FormDialogDlg::HasChildFields(RunTimeCtrl *popCtrl)
{
	CString olFieldName = popCtrl->pomProperties->omName;
	POSITION pos = omControls.GetHeadPosition();
	while (pos != NULL)
	{ 
		RunTimeCtrl *polCtrl = omControls.GetNext(pos);
		if(polCtrl != NULL)
		{
			if(polCtrl->pomProperties != NULL)
			{
				if(olFieldName == polCtrl->pomProperties->omMasterName)
				{
					return true;
				}
			}
		}
	}
	return false;
}
bool FormDialogDlg::MakeMasterValue(RunTimeCtrl *popChild, RecordSet &ropRecord, CString &ropErrorText)
{
	RunTimeCtrl *polMaster = GetCtrlByName(popChild->pomProperties->omMasterName);
	bool blChildOk = true;
	CString olValue;
	if(	popChild->pomProperties->omName == "lstudate")
	{
		int o = 0;
		o++;
	}
	if(polMaster != NULL)
	{
		// Get current value of master field
		CString olMasterValue;
		polMaster->pomControl->GetWindowText(olMasterValue);
		// Check for valid entry
		if(popChild->pomProperties->omCtrlType == "edit" )
		{
			//CCSEdit *polEdit = (CCSEdit *)popChild->pomControl;
			CAatedit5 *polEdit = (CAatedit5 *)popChild->pomControl;
			//olValue = polEdit->GetWindowText();
			if(polEdit->GetStatus() == TRUE)
			{
			}
			else
			{
				if(popChild->pomProperties->bmMandatory == FALSE)
				{
					ropErrorText+= popChild->pomProperties->omLabel + CString(":   Incorrect Format!\n");
					blChildOk = false;
				}
				else
				{
					if(olValue.IsEmpty())
					{
						ropErrorText+= popChild->pomProperties->omLabel + CString(":   Field is mandatory!\n");
						blChildOk = false;
					}
					else
					{
						ropErrorText+= popChild->pomProperties->omLabel + CString(":   Incorrect Format!\n");
						blChildOk = false;
					}
				}
			}
		}
		if( blChildOk == true && !olValue.IsEmpty())
		{
			if(polMaster->pomProperties->omFieldType == "DTIM" && popChild->pomProperties->omFieldType == "DATE")
			{
				int ilIdx=-1;
				ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polMaster->pomProperties->omDBField);
				if(ilIdx != -1)
				{
					if(polMaster->pomProperties->omName.Find(".LSTU") != -1)
					{
						CString olCurrVal = ropRecord[ilIdx];
						CTime olT; olT = CTime::GetCurrentTime();
						if( olCurrVal.GetLength() == 14 )
						{
							olCurrVal = olT.Format("%Y%m%d") + olCurrVal.Right(6);
							ropRecord.Values[ilIdx] = olCurrVal;
						}
						else
						{
							olCurrVal = olT.Format("%Y%m%d      ");
							ropRecord.Values[ilIdx] = olCurrVal;
						}
						ropRecord.Values[ilIdx] = olCurrVal;
					}
					else if(polMaster->pomProperties->omName.Find(".CDAT") != -1)
					{
						CString olCurrVal = ropRecord[ilIdx];
						if(olCurrVal.IsEmpty())
						{
							CTime olT; olT = CTime::GetCurrentTime();
							olCurrVal = olT.Format("%Y%m%d      ");
							ropRecord.Values[ilIdx] = olCurrVal;
						}
						else{;/*DO NOTING*/}
					}
					else
					{
						CString olCurrVal = ropRecord[ilIdx];
						COleDateTime olDate;
						olDate = OleDateStringToDate(olValue);
						if(olDate.GetStatus() == COleDateTime::valid)
						{
							if( olCurrVal.GetLength() == 14 )
							{
								olCurrVal = olDate.Format("%Y%m%d") + olCurrVal.Right(6);
								ropRecord.Values[ilIdx] = olCurrVal;
							}
							else
							{
								olCurrVal = olDate.Format("%Y%m%d      ");
								ropRecord.Values[ilIdx] = olCurrVal;
							}
						}
					}
				}
			}
			if(polMaster->pomProperties->omFieldType == "DTIM" && popChild->pomProperties->omFieldType == "TIME")
			{
				int ilIdx=-1;
				ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polMaster->pomProperties->omDBField);
				if(ilIdx != -1)
				{
					if(polMaster->pomProperties->omName.Find(".LSTU") != -1)
					{
						CString olCurrVal = ropRecord[ilIdx];
						CTime olT; olT = CTime::GetCurrentTime();
						if( olCurrVal.GetLength() == 14 )
						{
							olCurrVal = olCurrVal.Left(8) + olT.Format("%H%M%S");
							ropRecord.Values[ilIdx] = olCurrVal;
						}
						else
						{
							olCurrVal = olT.Format("        %H%M%S");
							ropRecord.Values[ilIdx] = olCurrVal;
						}
						ropRecord.Values[ilIdx] = olCurrVal;
					}
					else if(polMaster->pomProperties->omName.Find(".CDAT") != -1)
					{
						CString olCurrVal = ropRecord[ilIdx];
						if(olCurrVal.IsEmpty())
						{
							CTime olT; olT = CTime::GetCurrentTime();
							olCurrVal = olT.Format("%Y%m%d%H%M%S");
							ropRecord.Values[ilIdx] = olCurrVal;
						}
						else{;/*DO NOTING*/}
					}
					else
					{
						CString olCurrVal = ropRecord[ilIdx];
						CTime olDate;
						olDate = HourStringToDate(olValue,true);
						if(olDate != TIMENULL)
						{
							if( olCurrVal.GetLength() == 14 )
							{
								olCurrVal = olCurrVal.Left(8) + olDate.Format("%H%M%S");
								ropRecord.Values[ilIdx] = olCurrVal;
							}
							else
							{
								olCurrVal = olDate.Format("        %H%M%S");
								ropRecord.Values[ilIdx] = olCurrVal;
							}
						}
					}
				}
			}
		}
		else if( blChildOk == true && olValue.IsEmpty())
		{
			if(polMaster->pomProperties->omFieldType == "DTIM" && popChild->pomProperties->omFieldType == "DATE")
			{
				int ilIdx=-1;
				ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polMaster->pomProperties->omDBField);
				if(ilIdx != -1)
				{
					if(polMaster->pomProperties->omName.Find(".LSTU") != -1)
					{
						CString olCurrVal = ropRecord[ilIdx];
						CTime olT; olT = CTime::GetCurrentTime();
						if( olCurrVal.GetLength() == 14 )
						{
							olCurrVal = olT.Format("%Y%m%d") + olCurrVal.Right(6);
							ropRecord.Values[ilIdx] = olCurrVal;
						}
						else
						{
							olCurrVal = olT.Format("%Y%m%d      ");
							ropRecord.Values[ilIdx] = olCurrVal;
						}
						ropRecord.Values[ilIdx] = olCurrVal;
					}
					else if(polMaster->pomProperties->omName.Find(".CDAT") != -1)
					{
						CString olCurrVal = ropRecord[ilIdx];
						if(olCurrVal.IsEmpty())
						{
							CTime olT; olT = CTime::GetCurrentTime();
							olCurrVal = olT.Format("%Y%m%d      ");
							ropRecord.Values[ilIdx] = olCurrVal;
						}
						else{;/*DO NOTING*/}
					}
					else
					{
						CString olCurrVal = ropRecord[ilIdx];
						if( olCurrVal.GetLength() == 14 )
						{
							olCurrVal = CString("        ") + olCurrVal.Right(6);
							ropRecord.Values[ilIdx] = olCurrVal;
						}
						else
						{
							olCurrVal = CString("              ");
							ropRecord.Values[ilIdx] = olCurrVal;
						}
					}
				}
			}
			if(polMaster->pomProperties->omFieldType == "DTIM" && popChild->pomProperties->omFieldType == "TIME")
			{
				int ilIdx=-1;
				ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polMaster->pomProperties->omDBField);
				if(ilIdx != -1)
				{
					if(polMaster->pomProperties->omName.Find(".LSTU") != -1)
					{
						CString olCurrVal = ropRecord[ilIdx];
						CTime olT; olT = CTime::GetCurrentTime();
						if( olCurrVal.GetLength() == 14 )
						{
							olCurrVal = olCurrVal.Left(8) + olT.Format("%H%M%S");
							ropRecord.Values[ilIdx] = olCurrVal;
						}
						else
						{
							olCurrVal = olT.Format("        %H%M%S");
							ropRecord.Values[ilIdx] = olCurrVal;
						}
						ropRecord.Values[ilIdx] = olCurrVal;
					}
					else if(polMaster->pomProperties->omName.Find(".CDAT") != -1)
					{
						CString olCurrVal = ropRecord[ilIdx];
						if(olCurrVal.IsEmpty())
						{
							CTime olT; olT = CTime::GetCurrentTime();
							olCurrVal = olCurrVal.Left(8) + olT.Format("%H%M%S");
							ropRecord.Values[ilIdx] = olCurrVal;
						}
						else{;/*DO NOTING*/}
					}
					else
					{
						CString olCurrVal = ropRecord[ilIdx];
						if( olCurrVal.GetLength() == 14 )
						{
							olCurrVal = olCurrVal.Left(8) + CString("      ");
							ropRecord.Values[ilIdx] = olCurrVal;
						}
						else
						{
							olCurrVal = CString("              ");
							ropRecord.Values[ilIdx] = olCurrVal;
						}
					}
				}
			}
		}
	}
	return blChildOk;
}

void FormDialogDlg::MakeMasterChild(RunTimeCtrl *popChild, RecordSet &ropRecord)
{
	if(popChild->pomProperties->omCtrlType == "radio")
	{
		if(popChild->pomProperties->bmIsMaster == TRUE)
		{
			CCSPtrArray<RunTimeCtrl> olChilds;
			int ilCount;
			CString olValue;
			int ilIdx=-1;
			ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, popChild->pomProperties->omDBField);
			if(ilIdx != -1)
			{
				olValue = ropRecord[ilIdx];
			}


			ilCount = GetAllChildsForMaster(popChild->pomProperties->omName, olChilds);
			olChilds.NewAt(olChilds.GetSize(), *popChild);
			ilCount++;
			for( int i = 0; i < ilCount; i++ )
			{
				URadioButton *polRadio = (URadioButton*)olChilds[i].pomControl;
				if(olChilds[i].pomProperties->omCheckedValue == olValue)
				{
					polRadio->SetCheck(1);
				}
				else
				{
					polRadio->SetCheck(0);
				}
			}
			olChilds.DeleteAll();
		}
	}
	else
	{
		RunTimeCtrl *polMaster = GetCtrlByName(popChild->pomProperties->omMasterName);
		if(polMaster != NULL)
		{
			if(polMaster->pomProperties->omCtrlType == "edit")
			{
				if(polMaster->pomProperties->omFieldType == "DTIM" && popChild->pomProperties->omFieldType == "DATE")
				{
					int ilIdx=-1;
					ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polMaster->pomProperties->omDBField);
					if(ilIdx != -1)
					{
						CString olValue = ropRecord[ilIdx];
						CTime olTime;
						olTime = DBStringToDateTime(olValue);
						if(olTime != -1)
							olValue = olTime.Format("%d.%m.%Y");
						else
							olValue = CString("");
						//((CCSEdit *)(popChild->pomControl))->SetInitText(olValue, false);
						((CAatedit5 *)(popChild->pomControl))->SetInitText(olValue, false);
						//popChild->pomControl->SetWindowText(olValue);
					}
				}
				if(polMaster->pomProperties->omFieldType == "DTIM" && popChild->pomProperties->omFieldType == "TIME")
				{
					int ilIdx=-1;
					ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polMaster->pomProperties->omDBField);
					if(ilIdx != -1)
					{
						CString olValue = ropRecord[ilIdx];
						CTime olTime;
						olTime = DBStringToDateTime(olValue);
						if(olTime != -1)
							olValue = olTime.Format("%H:%M");
						else
							olValue = CString("");
						//((CCSEdit *)(popChild->pomControl))->SetInitText(olValue, false);
						((CAatedit5 *)(popChild->pomControl))->SetInitText(olValue, false);
						//popChild->pomControl->SetWindowText(olValue);
					}
				}
			}//if omCtrlType == "edit"
		}//if(polMaster != NULL)
	}
}

// searches all child which are connected to opMasterName
// return: amount of hits
int  FormDialogDlg::GetAllChildsForMaster(CString opMasterName, CCSPtrArray<RunTimeCtrl> &ropChilds)
{
	int ilCount = 0;
	POSITION pos = omControls.GetHeadPosition();
	while (pos != NULL)
	{
		RunTimeCtrl *polCtrl = omControls.GetNext(pos);
		if(polCtrl != NULL)
		{
			if(polCtrl->pomProperties->omMasterName == opMasterName)
			{
				ropChilds.NewAt(ropChilds.GetSize(), *polCtrl); 
				ilCount++;
			}
		}
	}
	return ilCount;
}

RunTimeCtrl *FormDialogDlg::GetCtrlByName(CString opName)
{
	POSITION pos = omControls.GetHeadPosition();
	while (pos != NULL)
	{
		RunTimeCtrl *polCtrl = omControls.GetNext(pos);
		if(polCtrl != NULL)
		{
			if(polCtrl->pomProperties->omName == opName)
			{
				return polCtrl; 
			}
		}
	}
	return NULL;

}

void FormDialogDlg::OnOK() 
{
	bool blRet = true;
	try
	{ 
		((CUWorkBenchApp*)AfxGetApp())->m_pScriptHost->
		pScriptControl->ExecuteStatement(_T("OnOK"));
	}
	catch(_com_error & error)
	{

	}
	
	if(omMode == "UPDATE")
	{
		blRet = UpdateFormData();
	}
	if(omMode == "INSERT" || omMode == "COPY")
	{
		blRet = InsertFormData();
	}
	if(blRet == true)
	{
		CDialog::OnOK();
	}
}

void FormDialogDlg::DeleteGridLine()
{
}

LONG FormDialogDlg::ClickedGridRButton(UINT wParam, LONG lParam)
{
	if(lParam == 0)
	{
		GxMiniGridInfo *polMiniGridInfo = (GxMiniGridInfo *)(long)wParam;
		MiniGridInformation *polMiniGridLog = NULL;
		if(polMiniGridInfo != NULL && pomForm != NULL)
		{
			for(int i = 0; i < omMiniGrids.GetSize(); i++)
			{
				if(omMiniGrids[i].pomGrid == polMiniGridInfo->pGrid)
				{
					polMiniGridLog = &omMiniGrids[i];
				}
			}
			//polMiniGridInfo->pGrid->SelectRange(CGXRange(polMiniGridInfo->Row, polMiniGridInfo->Col, polMiniGridInfo->Row, polMiniGridInfo->pGrid->GetColCount()));

		}
		imCurrentRow = (int)polMiniGridInfo->Row-1;
		CMenu menu;
		menu.CreatePopupMenu();
		for (int i = menu.GetMenuItemCount(); i > 0; i--)
			 menu.RemoveMenu(i, MF_BYPOSITION);
		menu.AppendMenu(MF_STRING,31, "Delete Line");	// Confirm

		polMiniGridInfo->pGrid->ClientToScreen(&polMiniGridInfo->pt);
		menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, polMiniGridInfo->pt.x, polMiniGridInfo->pt.y, this, NULL);
	}
	return 0L;
}


LONG FormDialogDlg::ChangedGridCell(UINT wParam, LONG lParam)
{
	GxMiniGridInfo *polMiniGridInfo = (GxMiniGridInfo *)(long)wParam;
	MiniGridInformation *polMiniGridLog = NULL;
	if(polMiniGridInfo->Col == 0 || polMiniGridInfo->Row == 0)
	{
		return 0L; // DO nothing
	}
	if(polMiniGridInfo != NULL && pomForm != NULL)
	{
		for(int i = 0; i < omMiniGrids.GetSize(); i++)
		{
			if(omMiniGrids[i].pomGrid == polMiniGridInfo->pGrid)
			{
				polMiniGridLog = &omMiniGrids[i];
			}
		}
		UGridCtrlProperties *polGridProps;
		polGridProps = GetGrid(polMiniGridInfo->pGrid);
		if(polGridProps != NULL)
		{
			CString olValue;
			olValue = polMiniGridInfo->pGrid->GetValueRowCol(polMiniGridInfo->Row, polMiniGridInfo->Col);
			int ilC = polGridProps->omMandatorylist.GetSize();
			if(polMiniGridLog != NULL)
			{
				CStringArray olValues;
				CString olVal;
				for( i = 0; i < (int)polMiniGridInfo->pGrid->GetColCount(); i++ )
				{
					olVal = polMiniGridInfo->pGrid->GetValueRowCol(polMiniGridInfo->Row, i+1);
					olValues.Add(olVal);
				}
				polMiniGridLog->UpdateLine((polMiniGridInfo->Row-1), olValues);
			}
			if(polGridProps->omMandatorylist[polMiniGridInfo->Col-1] == "1" && olValue.IsEmpty())
			{
				CGXStyle olStyle;
				Beep(440, 100);
			}
		}
	}
	return 0L;
}

UGridCtrlProperties *FormDialogDlg::GetGrid(GxGridTab *popGrid)
{
	POSITION pos = omControls.GetHeadPosition();
	while (pos != NULL)
	{
		RunTimeCtrl *polCtrl = omControls.GetNext(pos);
		if(polCtrl != NULL)
		{
			if(((GxGridTab *)polCtrl->pomControl) == popGrid)
			{
				return ((UGridCtrlProperties*)polCtrl->pomProperties);
			}
		}
	}
	return NULL;
}

bool FormDialogDlg::IsLineOk(int opRow)
{
	bool blRet = true;

	return blRet;
}

void FormDialogDlg::MakeEditValue(RunTimeCtrl *popCtrl, RecordSet &ropRecord)
{
	
	UCtrlProperties *polProperty = (UCtrlProperties *)popCtrl->pomProperties;
	if(polProperty->omCtrlType == "edit")
	{
		UEditCtrlProperties *polProp = (UEditCtrlProperties *)polProperty;
		if(polProp->omFieldType == "DATE")
		{
			int ilIdx=-1;
			ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polProp->omDBField);
			if(ilIdx != -1)
			{
				CString olValue = ropRecord[ilIdx];
				CTime olTime;
				olTime = DBStringToDateTime(olValue);
				if(olTime != -1)
					olValue = olTime.Format("%d.%m.%Y");
				else
					olValue = CString("");
				//((CCSEdit *)(popCtrl->pomControl))->SetInitText(olValue, false);

				((CAatedit5 *)(popCtrl->pomControl))->SetInitText(olValue,false);
			}
		}
		else if(polProp->omFieldType == "TIME")
		{
			int ilIdx=-1;
			ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polProp->omDBField);
			if(ilIdx != -1)
			{
				CString olValue = ropRecord[ilIdx];
				CTime olTime;
				olTime = DBStringToDateTime(olValue);
				if(olTime != -1)
					olValue = olTime.Format("%H:%M");
				else
					olValue = CString("");
//				((CCSEdit *)(popCtrl->pomControl))->SetInitText(olValue, false);
				((CAatedit5 *)(popCtrl->pomControl))->SetInitText(olValue,false);
			}
		}
		else if(polProp->omFieldType == "TRIM")
		{
			CString olValue;
			int ilIdx=-1;
			ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polProp->omDBField);
			if(ilIdx != -1)
			{
				olValue = ropRecord[ilIdx];
//				((CCSEdit *)(popCtrl->pomControl))->SetInitText(olValue, false);
				((CAatedit5 *)(popCtrl->pomControl))->SetInitText(olValue,false);
			}
		}
		else if(polProp->omFieldType == "INT")
		{
			CString olValue;
			int ilIdx=-1;
			ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polProp->omDBField);
			if(ilIdx != -1)
			{
				olValue = ropRecord[ilIdx];
//				((CCSEdit *)(popCtrl->pomControl))->SetInitText(olValue, false);
				((CAatedit5 *)(popCtrl->pomControl))->SetInitText(olValue,false);
			}
		}
		else if(polProp->omFieldType == "LONG")
		{
			CString olValue;
			int ilIdx=-1;
			ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polProp->omDBField);
			if(ilIdx != -1)
			{
				olValue = ropRecord[ilIdx];
				((CAatedit5 *)(popCtrl->pomControl))->SetInitText(olValue,false);
//				((CCSEdit *)(popCtrl->pomControl))->SetInitText(olValue, false);
			}
		}
		else if(polProp->omFieldType == "BOOL")
		{
			CString olValue;
			int ilIdx=-1;
			ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polProp->omDBField);
			if(ilIdx != -1)
			{
				olValue = ropRecord[ilIdx];
				((CAatedit5 *)(popCtrl->pomControl))->SetInitText(olValue,false);
//				((CCSEdit *)(popCtrl->pomControl))->SetInitText(olValue, false);
			}
		}
		//MIN2HHMM	: DB -> 90min == GUI -> 01:30 or 0130
		else if(polProp->omFieldType == "MIN2HHMM")
		{
			int ilIdx=-1;
			CString olValue;
			ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polProp->omDBField);
			if(ilIdx != -1)
			{
				int ilRaw;
				int ilHours, ilMinutes;
				olValue = ropRecord[ilIdx];
				ilRaw = atoi(olValue.GetBuffer(0));
				ilHours = (int)(ilRaw/60);
				ilMinutes = ilRaw % 60;
				olValue.Format("%02d:%02d", ilHours, ilMinutes);
				((CAatedit5 *)(popCtrl->pomControl))->SetInitText(olValue,false);
//				((CCSEdit *)(popCtrl->pomControl))->SetInitText(olValue, false);
			}
		}
		//MIN2HH		: DB -> 90min == GUI -> 1.5h
		else if(polProp->omFieldType == "MIN2HH")
		{
			int ilIdx=-1;
			CString olValue;
			ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polProp->omDBField);
			if(ilIdx != -1)
			{
				int ilRaw;
				float flHours, flMinutes;
				int ilMinutes;
				
				olValue = ropRecord[ilIdx];
				ilRaw = atoi(olValue.GetBuffer(0)); 
				flHours = (ilRaw/60);
				ilMinutes = (ilRaw % 60);
				flMinutes = (float)(ilMinutes/60.);
				flHours+=flMinutes;
				olValue.Format("%3.2f", flHours);
				((CAatedit5 *)(popCtrl->pomControl))->SetInitText(olValue,false);
//				((CCSEdit *)(popCtrl->pomControl))->SetInitText(olValue, false);
			}
		}
		//HHMM2MIN	: DB -> 01:30 or 0130 == GUI 90min
		else if(polProp->omFieldType == "HHMM2MIN")
		{
			int ilIdx=-1;
			CString olValue;
			ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polProp->omDBField);
			if(ilIdx != -1)
			{
				olValue = ropRecord[ilIdx];
				CTime olDate;
				olDate = HourStringToDate(olValue,true);
				if(olDate != TIMENULL)
				{
					int ilMinutes, ilHours;
					ilHours = olDate.GetHour();
					ilMinutes = olDate.GetMinute();
					ilMinutes += (ilHours*60);
					olValue.Format("%d", ilMinutes);
				((CAatedit5 *)(popCtrl->pomControl))->SetInitText(olValue,false);
//					((CCSEdit *)(popCtrl->pomControl))->SetInitText(olValue, false);
				}
			}
		}
		//HHMM2HH		: DB -> 01:30 or 01300 == GUI -> 1.5h
		else if(polProp->omFieldType == "HHMM2HH")
		{
			int ilIdx=-1;
			CString olValue;
			ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polProp->omDBField);
			if(ilIdx != -1)
			{
				olValue = ropRecord[ilIdx];
				CTime olDate;
				olDate = HourStringToDate(olValue,true);
				if(olDate != TIMENULL)
				{
					int ilMinutes;
					float flHours, flMinutes;
					flHours = (float)olDate.GetHour();
					ilMinutes = olDate.GetMinute();
					flMinutes = (float)(ilMinutes/60.);
					flHours = flHours + flMinutes;
					olValue.Format("%3.2f", flHours);
				((CAatedit5 *)(popCtrl->pomControl))->SetInitText(olValue,false);
//					((CCSEdit *)(popCtrl->pomControl))->SetInitText(olValue, false);
				}
			}
		}
		//HH2MIN		: DB -> 1.5h == GUI -> 90min
		else if(polProp->omFieldType == "HH2MIN")
		{
			int ilIdx=-1;
			CString olValue;
			ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polProp->omDBField);
			if(ilIdx != -1)
			{
				float flHours;
				int ilResult;
				olValue = ropRecord[ilIdx];
				flHours = atof(olValue.GetBuffer(0));
				ilResult = (int)(flHours * 60);
				olValue.Format("%d", ilResult);
				((CAatedit5 *)(popCtrl->pomControl))->SetInitText(olValue,false);
//				((CCSEdit *)(popCtrl->pomControl))->SetInitText(olValue, false);
			}
		}
		//HH2HHMM		: DB -> 1.5h == 01:30 or 0130
		else if(polProp->omFieldType == "HH2HHMM")
		{
			int ilIdx=-1;
			CString olValue;
			ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polProp->omDBField);
			if(ilIdx != -1)
			{
				float flRaw;
				int ilHours, ilMinutes;
				olValue = ropRecord[ilIdx];
				flRaw = atof(olValue.GetBuffer(0));
				ilHours = (int)(flRaw);
				ilMinutes = (int)((int)(flRaw*60)%60);
				olValue.Format("%02d:%02d", ilHours, ilMinutes);
				((CAatedit5 *)(popCtrl->pomControl))->SetInitText(olValue,false);
//				((CCSEdit *)(popCtrl->pomControl))->SetInitText(olValue, false);
			}
		}
		else
		{
			CString olValue;
			int ilIdx=-1;
			ilIdx = ogBCD.GetFieldIndex(pomForm->omTable, polProp->omDBField);
			if(ilIdx != -1)
			{
				olValue = ropRecord[ilIdx];
				((CAatedit5 *)(popCtrl->pomControl))->SetInitText(olValue,false);
//				((CCSEdit *)(popCtrl->pomControl))->SetInitText(olValue, false);
			}
		}
	}
}
bool FormDialogDlg::MakeDBValue(UCtrlProperties *popProperty, CString &ropValue, CString &ropErrorText)
{
	if(popProperty->omCtrlType == "edit")
	{
		UEditCtrlProperties *polProp = (UEditCtrlProperties *)popProperty;
		if(polProp->omFieldType == "DATE")
		{
			COleDateTime olDate;
			olDate = OleDateStringToDate(ropValue);
			if(olDate.GetStatus() == COleDateTime::valid)
			{
				ropValue = olDate.Format("%Y%m%d000000");
			}
			else
			{
				ropValue = "";
			}
		}
		else if(polProp->omFieldType == "TIME")
		{
			CTime olDate;
			olDate = HourStringToDate(ropValue,true);
			if(olDate != TIMENULL)
			{
				ropValue = olDate.Format("%Y%m%d%H%M00");
			}
			else
			{
				ropValue = "";
			}
		}
		//MIN2HHMM	: DB -> 90min == GUI -> 01:30 or 0130
		else if(polProp->omFieldType == "MIN2HHMM")
		{
			CTime olDate;
			olDate = HourStringToDate(ropValue,true);
			if(olDate != TIMENULL)
			{
				int ilResult;
				int ilHours, ilMinutes;
				ilHours = olDate.GetHour();
				ilMinutes = olDate.GetMinute();
				ilResult = (ilHours*60)+ilMinutes;
				ropValue.Format("%d", ilResult);
			}
			else
			{
				ropValue = "";
			}
		}
		//MIN2HH		: DB -> 90min == GUI -> 1.5h
		else if(polProp->omFieldType == "MIN2HH")
		{
			float ilRaw = atof(ropValue.GetBuffer(0));
			int ilMinutes;
			ilMinutes = (int)(float)(ilRaw*60);
			ropValue.Format("%d", ilMinutes);
		}
		//HHMM2MIN	: DB -> 01:30 or 0130 == GUI 90min
		else if(polProp->omFieldType == "HHMM2MIN")
		{
			int ilRaw = atoi(ropValue.GetBuffer(0));
			int ilHours = (int)(ilRaw/60);
			int ilMinutes = ilRaw % 60;
			ropValue.Format("%02d%02d", ilHours, ilMinutes);
		}
		//HHMM2HH		: DB -> 01:30 or 01300 == GUI -> 1.5h
		else if(polProp->omFieldType == "HHMM2HH")
		{
			float flRaw = atof(ropValue.GetBuffer(0));
			int ilMinutes, ilHours;
			flRaw = flRaw*60;
			ilHours = (int)(flRaw/60); 
			ilMinutes = ((int)flRaw) % 60;
			ropValue.Format("%02d%02d", ilHours, ilMinutes);
		}
		//HH2MIN		: DB -> 1.5h == GUI -> 90min
		else if(polProp->omFieldType == "HH2MIN")
		{
			float flResult;
			int  ilRaw;
			ilRaw = atoi(ropValue.GetBuffer(0));
			flResult = ilRaw/60.;
			ropValue.Format("%3.2f", flResult);
		}
		//HH2HHMM		: DB -> 1.5h == 01:30 or 0130
		else if(polProp->omFieldType == "HH2HHMM")
		{
			CTime olDate;
			olDate = HourStringToDate(ropValue,true);
			if(olDate != TIMENULL)
			{
				float flResult;
				int ilHours;
				float flMinutes;
				ilHours = olDate.GetHour();
				flMinutes = (float)olDate.GetMinute();
				flMinutes = (float)(flMinutes/60);
				flResult = ilHours+flMinutes;
				ropValue.Format("%3.2f", flResult);
			}
			else
			{
				ropValue = "";
			}
		}

	}
	return true;
}


CString FormDialogDlg::PrepareUniqueConstraintMessage(CString opDBMessage)
{
	CString olUserMessage;
	CStringArray olUniqueList;
	CString olKeyList;
	// unique constraint violation
	if(ogAccessMethod != "ODBC")
	{
		if(opDBMessage.Find("ORA-00001") != -1)
		{
			if(pomGridDef != NULL)
			{
				for(int i = 0; i < pomGridDef->omGridFields.GetSize(); i++)
				{
					if(pomGridDef->omGridFields[i].Unique == "1")
					{
						olUniqueList.Add(pomGridDef->omGridFields[i].Header);
						olKeyList+= pomGridDef->omGridFields[i].Header + "  ";
					}
				}
				if(olUniqueList.GetSize() == 1)
				{
					olUserMessage.Format(LoadStg(IDS_STRING61468), olKeyList.GetBuffer(0));
				}
				else if (olUniqueList.GetSize() > 1)
				{
					olUserMessage.Format(LoadStg(IDS_STRING61467), olKeyList.GetBuffer(0));
				}
				else
				{
					olUserMessage = LoadStg(IDS_STRING61466);
				}
			}
		}
	}
	else
	{
		return opDBMessage;
	}
	return olUserMessage;
}

// ==================================================================
// 
// FUNCTION :  FormDialogDlg::InjectCodeIntoScriptHost()
// 
// * Description : Injects associated vbw code into script host
// 
// 
// * Author : [cla AAT/ID], Created : [16.11.00 10:12:22]
// 
// * Returns : [bool] - succes or not
// 
// * Function parameters : 
// [formName] - name of the form
// 
// ==================================================================
bool FormDialogDlg::InjectCodeIntoScriptHost(CString formName)
{

	CString olVbCode;
	char *posBuf;

	/*
	 * <<<cla: This is a hack for the prototype
	 * cause we assume that the vbw File  is in 
	 * Workbench directory. This is propably not the proper
	 * location
	 */

	formName += _T(".vbw");
	TRY
	{   
		CFile olVbFile(LPCTSTR(formName), CFile::modeRead|CFile::modeNoTruncate|CFile::modeCreate);
		posBuf = new char[olVbFile.GetLength()+1];
		posBuf[olVbFile.GetLength()] = '\0';
		olVbFile.Read(posBuf,olVbFile.GetLength());
		olVbCode = posBuf;
		delete [] posBuf;
		//AfxMessageBox(LPCTSTR(olVbCode));
	}
	CATCH( CFileException, e )
	{   
		AfxMessageBox(_T("File not found!"));
		return(false);
	}END_CATCH

	try
	{ 
		_bstr_t olVbCodeConv(olVbCode.GetBuffer(olVbCode.GetLength()));
		((CUWorkBenchApp*)AfxGetApp())->m_pScriptHost->pScriptControl->AddCode(olVbCodeConv);
	}
	catch(_com_error & error)
	{
		CString olErrorMessage(error.ErrorMessage());
		olErrorMessage += _T("\n");
		olErrorMessage += error.Source();
		olErrorMessage += _T("\n");
		olErrorMessage += error.Description();

		AfxMessageBox(LPCTSTR(olErrorMessage));
		return(false);
	}
	return(true);
}

