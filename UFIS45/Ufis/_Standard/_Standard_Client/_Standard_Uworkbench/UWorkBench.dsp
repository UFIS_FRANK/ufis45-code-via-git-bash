# Microsoft Developer Studio Project File - Name="UWorkBench" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=UWorkBench - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "UWorkBench.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "UWorkBench.mak" CFG="UWorkBench - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "UWorkBench - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "UWorkBench - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName "UWorkBench"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "UWorkBench - Win32 Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 CCSClass.lib ufis32.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "UWorkBench - Win32 Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_AFXDLL" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 ufis32.lib ccsclass.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# SUBTRACT LINK32 /profile /map /nodefaultlib

!ENDIF 

# Begin Target

# Name "UWorkBench - Win32 Release"
# Name "UWorkBench - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\aatedit5.cpp
# End Source File
# Begin Source File

SOURCE=.\ActivationOrderDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AddTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\BasicData.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSGlobl.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCfgData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaInitModuData.Cpp
# End Source File
# Begin Source File

SOURCE=.\ChildFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\CntlEvent.cpp
# End Source File
# Begin Source File

SOURCE=.\ComButtonSink.cpp
# End Source File
# Begin Source File

SOURCE=.\commandbutton.cpp
# End Source File
# Begin Source File

SOURCE=.\ConnectionAdvisor.cpp
# End Source File
# Begin Source File

SOURCE=.\control.cpp
# End Source File
# Begin Source File

SOURCE=.\controls.cpp
# End Source File
# Begin Source File

SOURCE=.\DesignChoiceDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DetailGridWizard.cpp
# End Source File
# Begin Source File

SOURCE=.\EditSink.cpp
# End Source File
# Begin Source File

SOURCE=.\font1.cpp
# End Source File
# Begin Source File

SOURCE=.\FormDesignerDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\FormDesignerMDI.cpp
# End Source File
# Begin Source File

SOURCE=.\FormDesignerView.cpp
# End Source File
# Begin Source File

SOURCE=.\FormDialogDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FormExecuteDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\FormExecuteMDI.cpp
# End Source File
# Begin Source File

SOURCE=.\FormExecuteView.cpp
# End Source File
# Begin Source File

SOURCE=.\FormWizard.cpp
# End Source File
# Begin Source File

SOURCE=.\GridObjectDefinitionDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\GridView.cpp
# End Source File
# Begin Source File

SOURCE=.\GridWizzard1.cpp
# End Source File
# Begin Source File

SOURCE=.\GxGridTab.cpp
# End Source File
# Begin Source File

SOURCE=.\labelcontrol.cpp
# End Source File
# Begin Source File

SOURCE=.\LoginDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\mdccheckbox1.cpp
# End Source File
# Begin Source File

SOURCE=.\mdccombo.cpp
# End Source File
# Begin Source File

SOURCE=.\mdclist.cpp
# End Source File
# Begin Source File

SOURCE=.\mdcoptionbutton.cpp
# End Source File
# Begin Source File

SOURCE=.\MiniGridInformation.cpp
# End Source File
# Begin Source File

SOURCE=.\ncombo.cpp
# End Source File
# Begin Source File

SOURCE=.\ObjectCopyDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\optionframe.cpp
# End Source File
# Begin Source File

SOURCE=.\picture1.cpp
# End Source File
# Begin Source File

SOURCE=.\PrivList.cpp
# End Source File
# Begin Source File

SOURCE=.\PropertiesDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\QueryChildFrame.cpp
# End Source File
# Begin Source File

SOURCE=.\QueryDesignerGridView.cpp
# End Source File
# Begin Source File

SOURCE=.\QueryDesingerTables.cpp
# End Source File
# Begin Source File

SOURCE=.\RegisterDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ScriptHost.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\TableListFrameWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\TabObjectRuntimeDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\TabObjectRuntimeView.cpp
# End Source File
# Begin Source File

SOURCE=.\Tools.cpp
# End Source File
# Begin Source File

SOURCE=.\UCheckBox.cpp
# End Source File
# Begin Source File

SOURCE=.\UComboBox.cpp
# End Source File
# Begin Source File

SOURCE=.\UDBLoadSetDefinitionDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\UDragDropListBox.cpp
# End Source File
# Begin Source File

SOURCE=.\uedit.cpp
# End Source File
# Begin Source File

SOURCE=.\UFISGridMDIChild.cpp
# End Source File
# Begin Source File

SOURCE=.\UFISGridViewerDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\UFISGridViewerView.cpp
# End Source File
# Begin Source File

SOURCE=.\UFormControlObj.cpp
# End Source File
# Begin Source File

SOURCE=.\UFormDrawTool.cpp
# End Source File
# Begin Source File

SOURCE=.\UListControl.cpp
# End Source File
# Begin Source File

SOURCE=.\UListCtrlExtended.cpp
# End Source File
# Begin Source File

SOURCE=.\UniEingabe.cpp
# End Source File
# Begin Source File

SOURCE=.\UQueryDesignerTableView.cpp
# End Source File
# Begin Source File

SOURCE=.\UQueryDesingerDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\UQueryRelationsView.cpp
# End Source File
# Begin Source File

SOURCE=.\UScrollContainerView.cpp
# End Source File
# Begin Source File

SOURCE=.\UserViewDefinitionDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\UUFisObjectsMDI.cpp
# End Source File
# Begin Source File

SOURCE=.\UUFisObjectView.cpp
# End Source File
# Begin Source File

SOURCE=.\UUniListView.cpp
# End Source File
# Begin Source File

SOURCE=.\UUniversalGridDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\UUniversalGridMDI.cpp
# End Source File
# Begin Source File

SOURCE=.\UUniversalGridView.cpp
# End Source File
# Begin Source File

SOURCE=.\UWorkBench.cpp
# End Source File
# Begin Source File

SOURCE=.\hlp\UWorkBench.hpj

!IF  "$(CFG)" == "UWorkBench - Win32 Release"

# PROP Ignore_Default_Tool 1
USERDEP__UWORK="hlp\AfxCore.rtf"	"hlp\AfxPrint.rtf"	"hlp\$(TargetName).hm"	
# Begin Custom Build - Making help file...
OutDir=.\Release
TargetName=UWorkBench
InputPath=.\hlp\UWorkBench.hpj
InputName=UWorkBench

"$(OutDir)\$(InputName).hlp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	start /wait hcw /C /E /M "hlp\$(InputName).hpj" 
	if errorlevel 1 goto :Error 
	if not exist "hlp\$(InputName).hlp" goto :Error 
	copy "hlp\$(InputName).hlp" $(OutDir) 
	goto :done 
	:Error 
	echo hlp\$(InputName).hpj(1) : error: 
	type "hlp\$(InputName).log" 
	:done 
	
# End Custom Build

!ELSEIF  "$(CFG)" == "UWorkBench - Win32 Debug"

# PROP Ignore_Default_Tool 1
USERDEP__UWORK="hlp\AfxCore.rtf"	"hlp\AfxPrint.rtf"	"hlp\$(TargetName).hm"	
# Begin Custom Build - Making help file...
OutDir=.\Debug
TargetName=UWorkBench
InputPath=.\hlp\UWorkBench.hpj
InputName=UWorkBench

"$(OutDir)\$(InputName).hlp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	start /wait hcw /C /E /M "hlp\$(InputName).hpj" 
	if errorlevel 1 goto :Error 
	if not exist "hlp\$(InputName).hlp" goto :Error 
	copy "hlp\$(InputName).hlp" $(OutDir) 
	goto :done 
	:Error 
	echo hlp\$(InputName).hpj(1) : error: 
	type "hlp\$(InputName).log" 
	:done 
	
# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\UWorkBench.rc
# End Source File
# Begin Source File

SOURCE=.\UWorkBenchDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\UWorkBenchView.cpp
# End Source File
# Begin Source File

SOURCE=.\VBScriptEditorDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\VBScriptEditorMDI.cpp
# End Source File
# Begin Source File

SOURCE=.\VBScriptEditorView.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\aatedit5.h
# End Source File
# Begin Source File

SOURCE=.\ActivationOrderDlg.h
# End Source File
# Begin Source File

SOURCE=.\AddTableDlg.h
# End Source File
# Begin Source File

SOURCE=.\BasicData.h
# End Source File
# Begin Source File

SOURCE=.\CCSGLOBL.H
# End Source File
# Begin Source File

SOURCE=.\CedaCfgData.h
# End Source File
# Begin Source File

SOURCE=.\CedaInitModuData.h
# End Source File
# Begin Source File

SOURCE=.\ChildFrm.h
# End Source File
# Begin Source File

SOURCE=.\CntlEvent.h
# End Source File
# Begin Source File

SOURCE=.\ComButtonSink.h
# End Source File
# Begin Source File

SOURCE=.\commandbutton.h
# End Source File
# Begin Source File

SOURCE=.\ConnectionAdvisor.h
# End Source File
# Begin Source File

SOURCE=.\control.h
# End Source File
# Begin Source File

SOURCE=.\controls.h
# End Source File
# Begin Source File

SOURCE=.\CtrlProperties.h
# End Source File
# Begin Source File

SOURCE=.\DesignChoiceDlg.h
# End Source File
# Begin Source File

SOURCE=.\DetailGridWizard.h
# End Source File
# Begin Source File

SOURCE=.\EditSink.h
# End Source File
# Begin Source File

SOURCE=.\font1.h
# End Source File
# Begin Source File

SOURCE=.\FormDesignerDoc.h
# End Source File
# Begin Source File

SOURCE=.\FormDesignerMDI.h
# End Source File
# Begin Source File

SOURCE=.\FormDesignerView.h
# End Source File
# Begin Source File

SOURCE=.\FormDialogDlg.h
# End Source File
# Begin Source File

SOURCE=.\FormExecuteDoc.h
# End Source File
# Begin Source File

SOURCE=.\FormExecuteMDI.h
# End Source File
# Begin Source File

SOURCE=.\FormExecuteView.h
# End Source File
# Begin Source File

SOURCE=.\FormWizard.h
# End Source File
# Begin Source File

SOURCE=.\GridObjectDefinitionDlg.h
# End Source File
# Begin Source File

SOURCE=.\GridView.h
# End Source File
# Begin Source File

SOURCE=.\GridWizzard1.h
# End Source File
# Begin Source File

SOURCE=.\GxGridTab.h
# End Source File
# Begin Source File

SOURCE=.\labelcontrol.h
# End Source File
# Begin Source File

SOURCE=.\LoginDialog.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\mdccheckbox1.h
# End Source File
# Begin Source File

SOURCE=.\mdccombo.h
# End Source File
# Begin Source File

SOURCE=.\mdclist.h
# End Source File
# Begin Source File

SOURCE=.\mdcoptionbutton.h
# End Source File
# Begin Source File

SOURCE=.\MiniGridInformation.h
# End Source File
# Begin Source File

SOURCE=.\ncombo.h
# End Source File
# Begin Source File

SOURCE=.\ObjectCopyDlg.h
# End Source File
# Begin Source File

SOURCE=.\optionframe.h
# End Source File
# Begin Source File

SOURCE=.\picture1.h
# End Source File
# Begin Source File

SOURCE=.\PrivList.h
# End Source File
# Begin Source File

SOURCE=.\PropertiesDlg.h
# End Source File
# Begin Source File

SOURCE=.\QueryChildFrame.h
# End Source File
# Begin Source File

SOURCE=.\QueryDesignerGridView.h
# End Source File
# Begin Source File

SOURCE=.\QueryDesingerTables.h
# End Source File
# Begin Source File

SOURCE=.\RegisterDlg.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h

!IF  "$(CFG)" == "UWorkBench - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Making help include file...
TargetName=UWorkBench
InputPath=.\Resource.h

"hlp\$(TargetName).hm" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	echo. >"hlp\$(TargetName).hm" 
	echo // Commands (ID_* and IDM_*) >>"hlp\$(TargetName).hm" 
	makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>"hlp\$(TargetName).hm" 
	echo. >>"hlp\$(TargetName).hm" 
	echo // Prompts (IDP_*) >>"hlp\$(TargetName).hm" 
	makehm IDP_,HIDP_,0x30000 resource.h >>"hlp\$(TargetName).hm" 
	echo. >>"hlp\$(TargetName).hm" 
	echo // Resources (IDR_*) >>"hlp\$(TargetName).hm" 
	makehm IDR_,HIDR_,0x20000 resource.h >>"hlp\$(TargetName).hm" 
	echo. >>"hlp\$(TargetName).hm" 
	echo // Dialogs (IDD_*) >>"hlp\$(TargetName).hm" 
	makehm IDD_,HIDD_,0x20000 resource.h >>"hlp\$(TargetName).hm" 
	echo. >>"hlp\$(TargetName).hm" 
	echo // Frame Controls (IDW_*) >>"hlp\$(TargetName).hm" 
	makehm IDW_,HIDW_,0x50000 resource.h >>"hlp\$(TargetName).hm" 
	
# End Custom Build

!ELSEIF  "$(CFG)" == "UWorkBench - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Making help include file...
TargetName=UWorkBench
InputPath=.\Resource.h

"hlp\$(TargetName).hm" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	echo. >"hlp\$(TargetName).hm" 
	echo // Commands (ID_* and IDM_*) >>"hlp\$(TargetName).hm" 
	makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>"hlp\$(TargetName).hm" 
	echo. >>"hlp\$(TargetName).hm" 
	echo // Prompts (IDP_*) >>"hlp\$(TargetName).hm" 
	makehm IDP_,HIDP_,0x30000 resource.h >>"hlp\$(TargetName).hm" 
	echo. >>"hlp\$(TargetName).hm" 
	echo // Resources (IDR_*) >>"hlp\$(TargetName).hm" 
	makehm IDR_,HIDR_,0x20000 resource.h >>"hlp\$(TargetName).hm" 
	echo. >>"hlp\$(TargetName).hm" 
	echo // Dialogs (IDD_*) >>"hlp\$(TargetName).hm" 
	makehm IDD_,HIDD_,0x20000 resource.h >>"hlp\$(TargetName).hm" 
	echo. >>"hlp\$(TargetName).hm" 
	echo // Frame Controls (IDW_*) >>"hlp\$(TargetName).hm" 
	makehm IDW_,HIDW_,0x50000 resource.h >>"hlp\$(TargetName).hm" 
	
# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\RunTimeCtrl.h
# End Source File
# Begin Source File

SOURCE=.\ScriptHost.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\TableListFrameWnd.h
# End Source File
# Begin Source File

SOURCE=.\TabObjectRuntimeDoc.h
# End Source File
# Begin Source File

SOURCE=.\TabObjectRuntimeView.h
# End Source File
# Begin Source File

SOURCE=.\Tools.h
# End Source File
# Begin Source File

SOURCE=.\UCheckBox.h
# End Source File
# Begin Source File

SOURCE=.\UComboBox.h
# End Source File
# Begin Source File

SOURCE=.\UDBLoadSetDefinitionDlg.h
# End Source File
# Begin Source File

SOURCE=.\UDragDropListBox.h
# End Source File
# Begin Source File

SOURCE=.\uedit.h
# End Source File
# Begin Source File

SOURCE=.\UFISGridMDIChild.h
# End Source File
# Begin Source File

SOURCE=.\UFISGridViewerDoc.h
# End Source File
# Begin Source File

SOURCE=.\UFISGridViewerView.h
# End Source File
# Begin Source File

SOURCE=.\UFormControlObj.h
# End Source File
# Begin Source File

SOURCE=.\UFormDrawTool.h
# End Source File
# Begin Source File

SOURCE=.\UListControl.h
# End Source File
# Begin Source File

SOURCE=.\UListCtrlExtended.h
# End Source File
# Begin Source File

SOURCE=.\UniEingabe.h
# End Source File
# Begin Source File

SOURCE=.\UObjectDataDefinitions.h
# End Source File
# Begin Source File

SOURCE=.\UQueryDesingerDoc.h
# End Source File
# Begin Source File

SOURCE=.\UQueryRelationsView.h
# End Source File
# Begin Source File

SOURCE=.\UScript.h
# End Source File
# Begin Source File

SOURCE=.\UScrollContainerView.h
# End Source File
# Begin Source File

SOURCE=.\UserViewDefinitionDlg.h
# End Source File
# Begin Source File

SOURCE=.\UUFisObjectsMDI.h
# End Source File
# Begin Source File

SOURCE=.\UUFisObjectView.h
# End Source File
# Begin Source File

SOURCE=.\UUniListView.h
# End Source File
# Begin Source File

SOURCE=.\UUniversalGridDoc.h
# End Source File
# Begin Source File

SOURCE=.\UUniversalGridMDI.h
# End Source File
# Begin Source File

SOURCE=.\UUniversalGridView.h
# End Source File
# Begin Source File

SOURCE=.\UWorkBench.h
# End Source File
# Begin Source File

SOURCE=.\UWorkBenchDoc.h
# End Source File
# Begin Source File

SOURCE=.\UWorkBenchView.h
# End Source File
# Begin Source File

SOURCE=.\VBScriptEditorDoc.h
# End Source File
# Begin Source File

SOURCE=.\VBScriptEditorMDI.h
# End Source File
# Begin Source File

SOURCE=.\VBScriptEditorView.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\abb.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00003.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00004.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00005.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00006.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00007.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00008.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00009.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00010.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00011.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00012.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00013.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00014.bmp
# End Source File
# Begin Source File

SOURCE=.\res\il_class.bmp
# End Source File
# Begin Source File

SOURCE=.\res\mainfram.bmp
# End Source File
# Begin Source File

SOURCE=.\res\radiobut.bmp
# End Source File
# Begin Source File

SOURCE=.\res\res_tool.bmp
# End Source File
# Begin Source File

SOURCE=.\res\resource.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\toolbar1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\universa.bmp
# End Source File
# Begin Source File

SOURCE=.\res\UWorkBench.ico
# End Source File
# Begin Source File

SOURCE=.\res\UWorkBench.rc2
# End Source File
# Begin Source File

SOURCE=.\res\UWorkBenchDoc.ico
# End Source File
# End Group
# Begin Group "Help Files"

# PROP Default_Filter "cnt;rtf"
# Begin Source File

SOURCE=.\hlp\AfxCore.rtf
# End Source File
# Begin Source File

SOURCE=.\hlp\AfxPrint.rtf
# End Source File
# Begin Source File

SOURCE=.\hlp\AppExit.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\Bullet.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\CurArw2.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\CurArw4.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\CurHelp.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\EditCopy.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\EditCut.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\EditPast.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\EditUndo.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\FileNew.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\FileOpen.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\FilePrnt.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\FileSave.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\HlpSBar.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\HlpTBar.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\RecFirst.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\RecLast.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\RecNext.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\RecPrev.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\Scmax.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\ScMenu.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\Scmin.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\UWorkBench.cnt

!IF  "$(CFG)" == "UWorkBench - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Copying contents file...
OutDir=.\Release
InputPath=.\hlp\UWorkBench.cnt
InputName=UWorkBench

"$(OutDir)\$(InputName).cnt" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	copy "hlp\$(InputName).cnt" $(OutDir)

# End Custom Build

!ELSEIF  "$(CFG)" == "UWorkBench - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Copying contents file...
OutDir=.\Debug
InputPath=.\hlp\UWorkBench.cnt
InputName=UWorkBench

"$(OutDir)\$(InputName).cnt" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	copy "hlp\$(InputName).cnt" $(OutDir)

# End Custom Build

!ENDIF 

# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
# Section UWorkBench : {04598FC4-866C-11CF-AB7C-00AA00C08FCF}
# 	2:5:Class:CCommandButton
# 	2:10:HeaderFile:commandbutton.h
# 	2:8:ImplFile:commandbutton.cpp
# End Section
# Section UWorkBench : {978C9E23-D4B0-11CE-BF2D-00AA003F40D0}
# 	2:21:DefaultSinkHeaderFile:labelcontrol.h
# 	2:16:DefaultSinkClass:CLabelControl
# End Section
# Section UWorkBench : {8BD21D43-EC42-11CE-9E0D-00AA006002F3}
# 	2:5:Class:CMdcCheckBox
# 	2:10:HeaderFile:mdccheckbox1.h
# 	2:8:ImplFile:mdccheckbox1.cpp
# End Section
# Section UWorkBench : {8BD21D33-EC42-11CE-9E0D-00AA006002F3}
# 	2:5:Class:CMdcCombo
# 	2:10:HeaderFile:mdccombo.h
# 	2:8:ImplFile:mdccombo.cpp
# End Section
# Section UWorkBench : {5A1A1053-80E5-11D3-9285-0080ADB811C5}
# 	2:21:DefaultSinkHeaderFile:kedit.h
# 	2:16:DefaultSinkClass:CkEdit
# End Section
# Section UWorkBench : {C3865243-FE62-11D4-90BC-00010204AA51}
# 	2:5:Class:CAatedit5
# 	2:10:HeaderFile:aatedit5.h
# 	2:8:ImplFile:aatedit5.cpp
# End Section
# Section UWorkBench : {8BD21D53-EC42-11CE-9E0D-00AA006002F3}
# 	2:5:Class:CMdcOptionButton
# 	2:10:HeaderFile:mdcoptionbutton.h
# 	2:8:ImplFile:mdcoptionbutton.cpp
# End Section
# Section UWorkBench : {8BD21D23-EC42-11CE-9E0D-00AA006002F3}
# 	2:5:Class:CMdcList
# 	2:10:HeaderFile:mdclist.h
# 	2:8:ImplFile:mdclist.cpp
# End Section
# Section UWorkBench : {BEF6E003-A874-101A-8BBA-00AA00300CAB}
# 	2:5:Class:COleFont
# 	2:10:HeaderFile:font1.h
# 	2:8:ImplFile:font1.cpp
# End Section
# Section UWorkBench : {5A1A1052-80E5-11D3-9285-0080ADB811C5}
# 	2:5:Class:CkEdit
# 	2:10:HeaderFile:kedit.h
# 	2:8:ImplFile:kedit.cpp
# End Section
# Section UWorkBench : {04598FC7-866C-11CF-AB7C-00AA00C08FCF}
# 	2:5:Class:CControls
# 	2:10:HeaderFile:controls.h
# 	2:8:ImplFile:controls.cpp
# End Section
# Section UWorkBench : {D7053240-CE69-11CD-A777-00DD01143C57}
# 	2:21:DefaultSinkHeaderFile:commandbutton.h
# 	2:16:DefaultSinkClass:CCommandButton
# End Section
# Section UWorkBench : {7BF80981-BF32-101A-8BBB-00AA00300CAB}
# 	2:5:Class:CPicture
# 	2:10:HeaderFile:picture1.h
# 	2:8:ImplFile:picture1.cpp
# End Section
# Section UWorkBench : {04598FC6-866C-11CF-AB7C-00AA00C08FCF}
# 	2:5:Class:CControl
# 	2:10:HeaderFile:control.h
# 	2:8:ImplFile:control.cpp
# End Section
# Section UWorkBench : {04598FC1-866C-11CF-AB7C-00AA00C08FCF}
# 	2:5:Class:CLabelControl
# 	2:10:HeaderFile:labelcontrol.h
# 	2:8:ImplFile:labelcontrol.cpp
# End Section
# Section UWorkBench : {C3865245-FE62-11D4-90BC-00010204AA51}
# 	2:21:DefaultSinkHeaderFile:aatedit5.h
# 	2:16:DefaultSinkClass:CAatedit5
# End Section
# Section UWorkBench : {8BD21D20-EC42-11CE-9E0D-00AA006002F3}
# 	2:21:DefaultSinkHeaderFile:mdclist.h
# 	2:16:DefaultSinkClass:CMdcList
# End Section
# Section UWorkBench : {8BD21D50-EC42-11CE-9E0D-00AA006002F3}
# 	2:21:DefaultSinkHeaderFile:mdcoptionbutton.h
# 	2:16:DefaultSinkClass:CMdcOptionButton
# End Section
# Section UWorkBench : {8BD21D40-EC42-11CE-9E0D-00AA006002F3}
# 	2:21:DefaultSinkHeaderFile:mdccheckbox1.h
# 	2:16:DefaultSinkClass:CMdcCheckBox
# End Section
# Section UWorkBench : {29B86A70-F52E-11CE-9BCE-00AA00608E01}
# 	2:5:Class:COptionFrame
# 	2:10:HeaderFile:optionframe.h
# 	2:8:ImplFile:optionframe.cpp
# End Section
# Section UWorkBench : {8BD21D30-EC42-11CE-9E0D-00AA006002F3}
# 	2:21:DefaultSinkHeaderFile:mdccombo.h
# 	2:16:DefaultSinkClass:CMdcCombo
# End Section
# Section UWorkBench : {6E182020-F460-11CE-9BCD-00AA00608E01}
# 	2:21:DefaultSinkHeaderFile:optionframe.h
# 	2:16:DefaultSinkClass:COptionFrame
# End Section
# Section UWorkBench : {2747A842-1525-4A47-BB65-683141D3A2CC}
# 	2:21:DefaultSinkHeaderFile:uedit.h
# 	2:16:DefaultSinkClass:CUEdit
# End Section
# Section UWorkBench : {680B5383-2373-4E6B-A1DF-116EB9681363}
# 	2:5:Class:CUEdit
# 	2:10:HeaderFile:uedit.h
# 	2:8:ImplFile:uedit.cpp
# End Section
