#ifndef __UCONTROL_OBJ__
#define __UCONTROL_OBJ__

class FormDesignerDoc;
class FormDesignerView;

#include "CtrlProperties.h"


class UFormControlObj : public CObject
{
public:
	UFormControlObj();
	~UFormControlObj();
	UFormControlObj(const CRect& opPosition);

	CRect				omPosition;
	FormDesignerDoc     *pomDocument;
	UCtrlProperties		*pomProperties;
//Hanlde is one of the trackers resizing points		
	virtual int GetHandleCount();
	virtual CPoint GetHandle(int ipHandle);
	CRect GetHandleRect(int nHandleID, FormDesignerView* popView);
	virtual HCURSOR GetHandleCursor(int ipHandle);
	virtual void SetLineColor(COLORREF color);
	virtual void SetFillColor(COLORREF color);


	virtual void Draw(CDC* pDC, FormDesignerView* popView);
	enum TrackerState { normal, selected, active };
	virtual void DrawTracker(CDC* pDC, TrackerState state);
	virtual void MoveTo(const CRect& positon, FormDesignerView* popView = NULL);
	virtual int HitTest(CPoint point, FormDesignerView* popView, BOOL bSelected);
	virtual BOOL Intersects(const CRect& rect);
	virtual void MoveHandleTo(int ipHandle, CPoint point, FormDesignerView* popView = NULL);
	virtual void OnOpen(FormDesignerView* popView);
	virtual void OnEditProperties(FormDesignerView* popView);
	virtual UFormControlObj * Clone(FormDesignerDoc* popDoc = NULL, UFormControlObj* pObj = NULL);
	virtual void Remove();
	virtual void ShowPropertyDlg(UCtrlProperties* popProperty = NULL, UFormControlObj *popCtrlObj = NULL, FormDesignerView* popView = NULL);
	void Invalidate();

protected:
	BOOL omPen;
	LOGPEN omLogPen;
	BOOL omBrush;
	LOGBRUSH omLogBrush;
};
// special 'list' class for this application (requires afxtempl.h)
typedef CTypedPtrList<CObList, UFormControlObj*> UFormControlObjList;


class UFormEditControl : public UFormControlObj
{
public:
	UFormEditControl();

	UFormEditControl(const CRect& ropPosition);

// Implementation
public:
	virtual void Draw(CDC* popDC, FormDesignerView* popView);
	virtual int GetHandleCount();
	virtual CPoint GetHandle(int ipHandle);
	virtual HCURSOR GetHandleCursor(int ipHandle);
	virtual void MoveHandleTo(int ipHandle, CPoint opPoint, FormDesignerView* popView = NULL);
	virtual BOOL Intersects(const CRect& ropRect);
	virtual void OnEditProperties(FormDesignerView* popView);
	UFormControlObj* Clone(FormDesignerDoc* popDoc, UFormControlObj* pObj = NULL);
//protected:
	enum Shape { rectangle, line };
	Shape omShape;

//	friend class CRectTool;
};


class UFormStaticControl : public UFormControlObj
{
public:
	UFormStaticControl();

	UFormStaticControl(const CRect& ropPosition);

// Implementation
public:
	virtual void Draw(CDC* popDC, FormDesignerView* popView);
	virtual int GetHandleCount();
	virtual CPoint GetHandle(int ipHandle);
	virtual HCURSOR GetHandleCursor(int ipHandle);
	virtual void MoveHandleTo(int ipHandle, CPoint opPoint, FormDesignerView* popView = NULL);
	virtual BOOL Intersects(const CRect& ropRect);
	virtual void OnEditProperties(FormDesignerView* popView);
	UFormControlObj* Clone(FormDesignerDoc* popDoc, UFormControlObj* pObj = NULL);
protected:
	enum Shape { rectangle, line };
	Shape omShape;

//	friend class CRectTool;
};

class UFormCheckboxControl : public UFormControlObj
{
public:
	UFormCheckboxControl();

	UFormCheckboxControl(const CRect& ropPosition);

// Implementation
public:
	virtual void Draw(CDC* popDC, FormDesignerView* popView);
	virtual int GetHandleCount();
	virtual CPoint GetHandle(int ipHandle);
	virtual HCURSOR GetHandleCursor(int ipHandle);
	virtual void MoveHandleTo(int ipHandle, CPoint opPoint, FormDesignerView* popView = NULL);
	virtual BOOL Intersects(const CRect& ropRect);
	virtual void OnEditProperties(FormDesignerView* popView);
	UFormControlObj* Clone(FormDesignerDoc* popDoc, UFormControlObj* pObj = NULL);
protected:
	enum Shape { rectangle, line };
	Shape omShape;

//	friend class CRectTool;
};

class UFormRadioButtonControl : public UFormControlObj
{
public:
	UFormRadioButtonControl();

	UFormRadioButtonControl(const CRect& ropPosition);

// Implementation
public:
	virtual void Draw(CDC* popDC, FormDesignerView* popView);
	virtual int GetHandleCount();
	virtual CPoint GetHandle(int ipHandle);
	virtual HCURSOR GetHandleCursor(int ipHandle);
	virtual void MoveHandleTo(int ipHandle, CPoint opPoint, FormDesignerView* popView = NULL);
	virtual BOOL Intersects(const CRect& ropRect);
	virtual void OnEditProperties(FormDesignerView* popView);
	UFormControlObj* Clone(FormDesignerDoc* popDoc, UFormControlObj* pObj = NULL);
protected:
	enum Shape { rectangle, line };
	Shape omShape;

//	friend class CRectTool;
};


class UFormComboboxControl : public UFormControlObj
{
public:
	UFormComboboxControl();

	UFormComboboxControl(const CRect& ropPosition);

// Implementation
public:
	virtual void Draw(CDC* popDC, FormDesignerView* popView);
	virtual int GetHandleCount();
	virtual BOOL Intersects(const CRect& ropRect);
	virtual void OnEditProperties(FormDesignerView* popView);
	UFormControlObj* Clone(FormDesignerDoc* popDoc, UFormControlObj* pObj = NULL);
protected:
	enum Shape { rectangle, line };
	Shape omShape;

//	friend class CRectTool;
};

class UFormButtonControl : public UFormControlObj
{
public:
	UFormButtonControl();

	UFormButtonControl(const CRect& ropPosition);

// Implementation
public:
	virtual void Draw(CDC* popDC, FormDesignerView* popView);
	virtual int GetHandleCount();
	virtual BOOL Intersects(const CRect& ropRect);
	virtual void OnEditProperties(FormDesignerView* popView);
	UFormControlObj* Clone(FormDesignerDoc* popDoc, UFormControlObj* pObj = NULL);
protected:
	enum Shape { rectangle, line };
	Shape omShape;

//	friend class CRectTool;
};


class UFormGroupControl : public UFormControlObj
{
public:
	UFormGroupControl();

	UFormGroupControl(const CRect& ropPosition);

// Implementation
public:
	virtual void Draw(CDC* popDC, FormDesignerView* popView);
	virtual int GetHandleCount();
	virtual BOOL Intersects(const CRect& ropRect);
	virtual void OnEditProperties(FormDesignerView* popView);
	UFormControlObj* Clone(FormDesignerDoc* popDoc, UFormControlObj* pObj = NULL);
protected:
	enum Shape { rectangle, line };
	Shape omShape;

//	friend class CRectTool;
};



class UFormRectControl : public UFormControlObj
{
protected:
	UFormRectControl();

public:
	UFormRectControl(const CRect& ropPosition);

// Implementation
public:
	virtual void Draw(CDC* popDC, FormDesignerView* popView);
	virtual int GetHandleCount();
	virtual CPoint GetHandle(int ipHandle);
	virtual HCURSOR GetHandleCursor(int ipHandle);
	virtual void MoveHandleTo(int ipHandle, CPoint opPoint, FormDesignerView* popView = NULL);
	virtual BOOL Intersects(const CRect& ropRect);
	virtual void OnEditProperties(FormDesignerView* popView);
	virtual UFormControlObj* Clone(FormDesignerDoc* pDoc, UFormControlObj* pObj = NULL);

//protected:
	enum Shape { rectangle, line };
	Shape omShape;
	CPoint m_roundness; // for roundRect corners

	friend class UFormDrawRectTool;
};


class UFormGridControl : public UFormControlObj
{
public:
	UFormGridControl();

	UFormGridControl(const CRect& ropPosition);

// Implementation
public:
	virtual void Draw(CDC* popDC, FormDesignerView* popView);
	virtual int GetHandleCount();
	virtual BOOL Intersects(const CRect& ropRect);
	virtual void OnEditProperties(FormDesignerView* popView);
	UFormControlObj* Clone(FormDesignerDoc* popDoc, UFormControlObj* pObj = NULL);
protected:
	enum Shape { rectangle, line };
	Shape omShape;

//	friend class CRectTool;
};


#endif //__UCONTROL_OBJ__