#include "stdafx.h"
//#include "CtrlProperties.h"
#include "MiniGridInformation.h"

MiniGridInformation::MiniGridInformation() : CObject()
{
	pomGrid = NULL;
}
MiniGridInformation::~MiniGridInformation()
{
	omGridValues.DeleteAll();
	omColumnInfo.DeleteAll();
}

void MiniGridInformation::MakeLines(UGridCtrlProperties *popGridProperties, 
								   GxGridTab *popGrid, 
								   CString opMasterTable,
								   CString opMasterField,
								   CString opDetailTable,
								   CString opDetailField,
								   CString opMasterValue )
{
	// This looks crazy, but from this point of view we consider the URNO
	// of header record as foreign key ==> so we have to switch the roles
	PrimaryTable = opDetailTable;
	PrimaryField = opMasterField;
	ForeignTable = opMasterTable;
	ForeignField = opDetailField;
	ForeignValue = opMasterValue;

	omTable = PrimaryTable;
	omName = popGridProperties->omName;
	pomGrid = popGrid;
	FieldList.RemoveAll();
	RealFieldList.RemoveAll();
	Types.RemoveAll();
	Mandatorys.RemoveAll();

 	int ilMC = 0,
		ilFC = 0,
		ilTC = 0;
	ilMC = popGridProperties->omMandatorylist.GetSize();
	ilFC = popGridProperties->omFieldlist.GetSize();
	ilTC = popGridProperties->omTypelist.GetSize();

	// This can never happen
	if((ilMC != ilFC) || ( ilMC != ilTC ) || ( ilTC != ilFC))
	{
		AfxMessageBox("Mandatories, Fields, Types do not have the same amount");
		return;
	}

	RealFieldList.Add(PrimaryField);
	RealFieldList.Add(ForeignField);
	RealFieldList.Add(opDetailField);
	for( int i = 0; i < ilTC; i++ )
	{
		FieldList.Add(popGridProperties->omFieldlist[i]);
		RealFieldList.Add(popGridProperties->omFieldlist[i]);
		Types.Add(popGridProperties->omTypelist[i]);
		Mandatorys.Add(popGridProperties->omMandatorylist[i]);
	}
	CCSPtrArray<RecordSet> olRecords;

	int ilRowCount=0;
//1.  Initial insertion of rows
	ogBCD.GetRecords(PrimaryTable, ForeignField, ForeignValue, &olRecords);
	ilRowCount = olRecords.GetSize()+1;
	pomGrid->SetColCount(ilTC);
	//if(olRecords.GetSize() > 0)
	{
		pomGrid->InsertRows( ilRowCount, olRecords.GetSize()+1);//+1 to append an empty row after filled rows
	}

//2. Init of type (celltypes, e.g. Comboboxes, Datecontrols etc.)

	for( i = 0; i < ilTC; i++ )
	{
		COLORREF olColor = COLORREF(RGB(255,255,255));
		if(Mandatorys[i] == "1")
		{
			olColor = COLORREF(RGB(255,255,128));
		}
		if(Types[i] == "DATE")
		{
			pomGrid->SetStyleRange(CGXRange(1, i+1, ilRowCount, i+1),
										CGXStyle()
										.SetControl(GX_IDS_CTRL_DATETIME)
										.SetUserAttribute(GX_IDS_UA_CUSTOMFORMAT,_T("dd.MM.yyyy"))
										.SetUserAttribute(GX_IDS_UA_DATEVALIDMODE, _T("1"))
										.SetValue("")
										.SetInterior(olColor));
		}
		if(Types[i] == "DTIM")
		{
			pomGrid->SetStyleRange(CGXRange(1, i+1, ilRowCount, i+1),
										CGXStyle()
										.SetControl(GX_IDS_CTRL_DATETIME/*GX_IDS_CTRL_DATETIMENOCAL*/)
										.SetUserAttribute(GX_IDS_UA_CUSTOMFORMAT,_T("dd.MM.yyyy-HH:m"))
										.SetUserAttribute(GX_IDS_UA_DATEVALIDMODE, _T("1"))
										.SetValue("")
										.SetInterior(olColor));
//			pomGrid->SetStyleRange(CGXRange(1, i+1, ilRowCount, i+1), CGXStyle().SetValue("")
//																 .SetInterior(olColor));
		}
		if(Types[i] == "TIME")
		{
			pomGrid->SetStyleRange(CGXRange(1, i+1, ilRowCount, i+1),
										CGXStyle()
										.SetControl(GX_IDS_CTRL_DATETIMENOCAL)
										.SetUserAttribute(GX_IDS_UA_CUSTOMFORMAT,_T("HH:MM"))
										.SetUserAttribute(GX_IDS_UA_DATEVALIDMODE, _T("1"))
										.SetValue("")
										.SetInterior(olColor));
//			pomGrid->SetStyleRange(CGXRange(1, i+1, ilRowCount, i+1), CGXStyle().SetValue("")
//																 .SetInterior(olColor));
		}
		if(Types[i] == "TRIM")
		{
			if(!popGridProperties->omLookupfieldList[i].IsEmpty())
			{
				CStringArray olLookups;
				CString olChoiceList;
				CString olStrTab;
				CString olStrFld;
				CStringArray olFldLookupList;
				ExtractItemList(popGridProperties->omLookupfieldList[i], &olLookups, '+');
				for(int j = 0; j < olLookups.GetSize(); j++)
				{
 					olStrTab = ogTools.GetTableName(olLookups[j]);
					if((j+1) == olLookups.GetSize())
					{
						olStrFld += ogTools.GetFieldName(olLookups[j]);
						olFldLookupList.Add(ogTools.GetFieldName(olLookups[j]));
					}
					else
					{
						olStrFld += ogTools.GetFieldName(olLookups[j])+CString(",");
						olFldLookupList.Add(ogTools.GetFieldName(olLookups[j]));
					}
					
				}
				int ilC3 = ogBCD.GetDataCount(olStrTab);
				for(j = 0; j < ilC3; j++)
				{
					RecordSet olTmpRec;
					CString olTmpChoiceList;
					for(int k = 0; k < olFldLookupList.GetSize(); k++)
					{
						ogBCD.GetRecord(olStrTab, j, olTmpRec);
						int ilIdx3 = ogBCD.GetFieldIndex(olStrTab, olFldLookupList[k]);
						if(ilIdx3 >= 0)
						{
							olTmpChoiceList+=olTmpRec[ilIdx3]+CString("\t");
						}
					}
					if(!olTmpChoiceList.IsEmpty())
						olChoiceList+=olTmpChoiceList+CString("\n");
				}
				pomGrid->SetStyleRange(CGXRange(1, i+1, ilRowCount, i+1),
											CGXStyle()
											.SetHorizontalAlignment(DT_LEFT)
											.SetVerticalAlignment(DT_BOTTOM)
											.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
											.SetChoiceList(olChoiceList)
											.SetValue("")
											.SetInterior(olColor));
			}
			else
			{
				//CGXRange(ROWCOL nTop, ROWCOL nLeft, ROWCOL nBottom, ROWCOL nRight);
				pomGrid->SetStyleRange(CGXRange(1, i+1, ilRowCount, i+1), CGXStyle().SetValue("")
																	 .SetInterior(olColor));
			}
		}
	}

// 3. Fill the cells of the grid with current reference
//olRecords
	for( i = 0; i < olRecords.GetSize(); i++)
	{
		MiniGridLine *polLine = new MiniGridLine();
		polLine->FUrno = ForeignValue;

		int ilIdx = -1;
		ilIdx = ilIdx = ogBCD.GetFieldIndex(omTable, "URNO");
		if(ilIdx >= 0)
		{
			polLine->Urno = olRecords[i][ilIdx];
		}
		polLine->Status = STAT_UNCHANGED;
		for(int j = 0; j < Types.GetSize(); j++)
		{
			CString olValue;
			ilIdx = ogBCD.GetFieldIndex(omTable, FieldList[j]);
			if(ilIdx >= 0)
			{
				olValue = olRecords[i][ilIdx];
				if(Types[j] == "DATE")
				{
					CTime olTime;
					olTime = DBStringToDateTime(olValue);
					if(olTime != -1)
						olValue = olTime.Format("%d.%m.%Y");
					else
						olValue = CString("");
				}
				if(Types[j] == "DTIM")
				{
					CTime olTime;
					olTime = DBStringToDateTime(olValue);
					if(olTime != -1)
						olValue = olTime.Format("%d.%m.%Y-%H:%M");
					else
						olValue = CString("");
				}
				if(Types[j] == "TIME")
				{
					CTime olTime;
					olTime = DBStringToDateTime(olValue);
					if(olTime != -1)
						olValue = olTime.Format("%H:%M");
					else
						olValue = CString("");
				}
				if(Types[j] == "TRIM")
				{
					;//Do nothing for the moment
				}
				pomGrid->SetValueRange(CGXRange(i+1, j+1), olValue);
				polLine->Values.Add(olValue);
			}
		}
		omGridValues.Add(polLine);

	}

	olRecords.DeleteAll();
}



int MiniGridInformation::GetFieldIndex(CString opField)
{
	for(int i = 0; i < FieldList.GetSize(); i++)
	{
		if(FieldList[i] == opField)
		{
			return i;
		}
	}
	return -1;
}
//---------------------------------------------------------------------------------------
// Before storing the data into the database, it's neccessary to have the real fieldlist
// 
//---------------------------------------------------------------------------------------
CString MiniGridInformation::GetRealList()
{
	CString olFieldList;
	for(int i = 0; i < RealFieldList.GetSize(); i++)
	{
		if((i+1)==RealFieldList.GetSize())
		{
			olFieldList += RealFieldList[i];
		}
		else
		{
			olFieldList += RealFieldList[i] + CString(",");
		}
	}
	return olFieldList;
}
//---------------------------------------------------------------------------------------
// All values are temporary stored as they appear in the grid
// Together with the type information the values are casted to
// an UFIS-conform string 
// Return is a string containing all values prepared for DB comma separated
//---------------------------------------------------------------------------------------
int MiniGridInformation::GetRealValues(MiniGridLine *popLine, CStringArray &ropValues, CString &ropStatus)
{
	CString olValues;
	if(popLine != NULL)
	{
		ropStatus = popLine->Status;
		ropValues.Add(popLine->Urno);
		ropValues.Add(popLine->FUrno);
		for(int i = 0; i < popLine->Values.GetSize(); i++)
		{
			ropValues.Add(popLine->Values[i]);
		}
	}
	return ropValues.GetSize();
}
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
CString MiniGridInformation::GetUrnoByLine(int ipLine)
{
	for(int i = 0; i < omGridValues.GetSize(); i++)
	{
		if( i == ipLine)
		{
			return omGridValues[i].Urno;
		}
	}
	return CString("");
}

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
int MiniGridInformation::GetLineNo(CString opUrno)
{
	for(int i = 0; i < omGridValues.GetSize(); i++)
	{
		if(omGridValues[i].Urno == opUrno)
		{
			return i;
		}
	}
	return -1;
}

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
void MiniGridInformation::InsertLine(CString opUrno, CString opForeignUrno, CStringArray &opValues)
{
	int  ilEmtpyCount = 0;
	bool blEmptyLine = false;

	for(int i = 0; i < opValues.GetSize(); i++)
	{
		if(opValues[i] == "")
		{
			ilEmtpyCount++;
		}
	}
	if(ilEmtpyCount == opValues.GetSize())
	{
		blEmptyLine = true;
	}
	if(opUrno == "" && blEmptyLine == false)
	{
		long llNextUrno = ogBCD.GetNextUrno();//ogBasicData.GetNextUrno();
		opUrno.Format("%ld", llNextUrno);
	}
	else if(blEmptyLine == true)
	{
		return; //No insertion of an empty line
	}

	MiniGridLine *polLine = new MiniGridLine();
	polLine->Status = "NEW";			// [MINIGRID_STATUS_INDEX]
	polLine->Urno = opUrno;		
	polLine->FUrno = opForeignUrno; 

	for( i = 0; i < opValues.GetSize(); i++)
	{
		polLine->Values.Add(opValues[i]);
	}
	omGridValues.Add(polLine);
}
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
bool MiniGridInformation::UpdateLine(CString opUrno, CString opForeignUrno, CStringArray &opValues)
{
	bool blRet = true;
	bool blWasChanged = false;
	int ilLineIdx;
	ilLineIdx = GetLineNo(opUrno);
	if(ilLineIdx != -1)
	{
		if(omGridValues[ilLineIdx].Status == "")
		{
			InsertLine(opUrno, opForeignUrno, opValues);
			blRet = true;
		}
		else
		{
			if(omGridValues[ilLineIdx].Status == "UNCHANGED")
			{
				omGridValues[ilLineIdx].Status = "CHANGED";
			}
			for(int i = 0; i < opValues.GetSize(); i++)
			{
				if(omGridValues[ilLineIdx].Values.GetSize() < i)
				{
					omGridValues[ilLineIdx].Values.Add(opValues[i]);
				}
				else
				{
					omGridValues[ilLineIdx].Values.SetAt(i, opValues[i]);
				}
			}
			blRet = true;
		}
	}
	else
	{
		blRet = false;
	}
	return blRet;
}

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
bool MiniGridInformation::UpdateLine(int ipLineNo, CStringArray &opValues)
{
	bool blRet = true;
	bool blWasChanged = false;
	CString olUrno;
	olUrno = GetUrnoByLine(ipLineNo);
	if(olUrno == CString(""))
	{
		//long llNextUrno = ogBCD.GetNextUrno();//ogBasicData.GetNextUrno();
		//olUrno.Format("%ld", llNextUrno);
		InsertLine(olUrno, ForeignValue, opValues);
		blRet = true;
	}
	else
	{
		int ilLineIdx = GetLineNo(olUrno);
		if(ilLineIdx >= 0)
		{
			for(int i = 0; i < opValues.GetSize(); i++)
			{
				if(omGridValues[ilLineIdx].Values.GetSize() < i)
				{
					if(omGridValues[ilLineIdx].Status == "UNCHANGED")
						omGridValues[ilLineIdx].Status = "CHANGED";
					omGridValues[ilLineIdx].Values.Add(opValues[i]);
				}
				else
				{
					CString olStatus = omGridValues[ilLineIdx].Status;
					CString olVal = opValues[i];
					if( omGridValues[ilLineIdx].Values[i] != opValues[i] )
						if(omGridValues[ilLineIdx].Status == "UNCHANGED")
							omGridValues[ilLineIdx].Status = "CHANGED";

					omGridValues[ilLineIdx].Values.SetAt(i, opValues[i]);
				}
			}
			blRet = true;
		}
		else
		{
			blRet = false;
		}
	}
	return blRet;
}
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
bool MiniGridInformation::DeleteLine(CString opUrno)
{
	for(int i = 0; i < omGridValues.GetSize(); i++)
	{
		if(omGridValues[i].Urno == opUrno)
		{
			if(omGridValues[i].Status != STAT_NEW)
			{
				omDeletedUrnos.Add(opUrno);
			}
			omGridValues.DeleteAt(i);
			return true;
		}
	}
	return false;
}

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
bool MiniGridInformation::DeleteLine(int ipLineNo)
{
	bool blRet = true;
	if(ipLineNo < omGridValues.GetSize())
	{
		CString olUrno = omGridValues[ipLineNo].Urno;
		{
			if(omGridValues[ipLineNo].Status != STAT_NEW)
			{
				omDeletedUrnos.Add(olUrno);
			}
			omGridValues.DeleteAt(ipLineNo);
		}
	}
	else
	{
		blRet = false;
	}
	return blRet;
}
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
void MiniGridInformation::SetForeignValue(CString opForeignValue)
{
	ForeignValue = opForeignValue;
	for( int i = 0; i < omGridValues.GetSize(); i++ )
	{
		omGridValues[i].FUrno = opForeignValue;
	}
}

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
bool MiniGridInformation::CheckGrid(CStringArray &ropMessageList)
{
	bool blRet = true;
	CString olTmp;
	//omName: Name of Grid ==> for the user a info
	olTmp = omName + CString("\n\n");
	ropMessageList.Add(olTmp);
	for( int i = 0; i < omGridValues.GetSize(); i++ )
	{
		CString olValue;
		//1. Mandatory check 1=Mand 0 not
		int ilMandC = Mandatorys.GetSize();
		int ilValC = omGridValues[i].Values.GetSize();
		for(int j = 0; (j < Mandatorys.GetSize()) && (j < omGridValues[i].Values.GetSize()); j++)
		{
			CString olM = Mandatorys[j];
			CString olV = omGridValues[i].Values[j];
			if(Mandatorys[j] == "1" && omGridValues[i].Values[j].IsEmpty())
			{
				CString olLine;
				olLine.Format("Line: %d Column: %d", (i+1), (j+1));
				olTmp = olLine + CString(" --> Entry is mandatory\n");
				ropMessageList.Add(olTmp);
				blRet = false;
			}
		}
	}
	return blRet;
}

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
bool MiniGridInformation::PerformSave()
{
	bool blRet = true;
	for( int i = 0; i < omGridValues.GetSize(); i++ )
	{
		CStringArray olLineValues;
		//2. Check for types
		for(int j = 0; j < omGridValues[i].Values.GetSize(); j++)
		{
			CString olValue;
			if(Types[j] == "DTIM")
			{
				CStringArray olArr;
				olValue = pomGrid->GetValueRowCol(i+1, j+1);
				ExtractItemList(olValue, &olArr, '-');
				if(olArr.GetSize() == 2)
				{
					olArr[0].TrimRight(); olArr[0].TrimLeft(); olArr[1].TrimLeft(); olArr[1].TrimRight();
					CTime olTime = DateHourStringToDate(olArr[0], olArr[1]);
					if(olTime != TIMENULL)
						olValue = olTime.Format("%Y%m%d%H%M00");
					else
						olValue = "";
				}
			}
			if(Types[j] == "DATE")
			{
				olValue = pomGrid->GetValueRowCol(i+1, j+1);
				CTime olTime = DateStringToDate(olValue);
				if(olTime != TIMENULL)
					olValue = olTime.Format("%Y%m%d000000");
				else
					olValue = "";
			}
			if(Types[j] == "TIME")
			{
				olValue = pomGrid->GetValueRowCol(i+1, j+1);
				CTime olTime = DateStringToDate(olValue);
				if(olTime != TIMENULL)
					olValue = olTime.Format("%Y%m%d%H%M00");
				else
					olValue = "";
			}
			if(Types[j] == "INT")
			{
				olValue = pomGrid->GetValueRowCol(i+1, j+1);
			}
			if(Types[j] == "LONG")
			{
				olValue = pomGrid->GetValueRowCol(i+1, j+1);
			}
			if(Types[j] == "TRIM")
			{
				olValue = pomGrid->GetValueRowCol(i+1, j+1);
				olValue.TrimRight();
			}
			if(Types[j] == "BOOL")
			{
				olValue = pomGrid->GetValueRowCol(i+1, j+1);
			}
			olLineValues.Add(olValue);
		}//omGridValues[i].Values.GetSize()
		if(omGridValues[i].Status == STAT_NEW)
		{
			RecordSet olCurrentRecord(ogBCD.GetFieldCount(omTable));
			int ilIdx=-1;
			//a) URNO
			ilIdx = ogBCD.GetFieldIndex(omTable, PrimaryField);
			if(ilIdx != -1)
			{
				olCurrentRecord.Values[ilIdx] = omGridValues[i].Urno;
			}
			//a) FURNO
			ilIdx = ogBCD.GetFieldIndex(omTable, ForeignField);
			if(ilIdx != -1)
			{
				olCurrentRecord.Values[ilIdx] = omGridValues[i].FUrno;
			}
			//c) Rest of fields
			for( int j = 0; j < FieldList.GetSize(); j++ )
			{
				ilIdx = ogBCD.GetFieldIndex(omTable, FieldList[j]);
				if(ilIdx != -1)
				{
					olCurrentRecord.Values[ilIdx] = olLineValues[j];
				}
			}
			ogBCD.InsertRecord(omTable, olCurrentRecord, true);
		}//Status == STAT_NEW
		if(omGridValues[i].Status == STAT_CHANGED)
		{
			RecordSet olCurrentRecord(ogBCD.GetFieldCount(omTable));
			int ilIdx=-1;
			//a) URNO
			ilIdx = ogBCD.GetFieldIndex(omTable, PrimaryField);
			if(ilIdx != -1)
			{
				olCurrentRecord.Values[ilIdx] = omGridValues[i].Urno;
			}
			//a) FURNO
			ilIdx = ogBCD.GetFieldIndex(omTable, ForeignField);
			if(ilIdx != -1)
			{
				olCurrentRecord.Values[ilIdx] = omGridValues[i].FUrno;
			}
			for( int j = 0; j < FieldList.GetSize(); j++ )
			{
				ilIdx = ogBCD.GetFieldIndex(omTable, FieldList[j]);
				if(ilIdx != -1)
				{
					olCurrentRecord.Values[ilIdx] = olLineValues[j];
				}
			}
			ogBCD.SetRecord(omTable, PrimaryField, omGridValues[i].Urno, olCurrentRecord.Values, true );
		}//Status == STAT_CHANGED
	}

	for( i = 0; i < omDeletedUrnos.GetSize(); i++ )
	{
		if(ogBCD.DeleteRecord(omTable, PrimaryField, omDeletedUrnos[i], true) == false)
		{
			AfxMessageBox("Delete Function: Error Module MiniGridInformation");
		}
	}
	return blRet;
}