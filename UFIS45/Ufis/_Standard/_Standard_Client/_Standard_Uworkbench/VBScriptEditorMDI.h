#if !defined(AFX_VBSCRIPTEDITORMDI_H__6B591860_92B8_11D4_A30F_00500437F607__INCLUDED_)
#define AFX_VBSCRIPTEDITORMDI_H__6B591860_92B8_11D4_A30F_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// VBScriptEditorMDI.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// VBScriptEditorMDI frame

class VBScriptEditorMDI : public CMDIChildWnd
{
	DECLARE_DYNCREATE(VBScriptEditorMDI)
protected:
	VBScriptEditorMDI();           // protected constructor used by dynamic creation

// Attributes
public:

	CString omScriptName;

	void SetScriptName(CString opScriptName);
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(VBScriptEditorMDI)
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~VBScriptEditorMDI();

	// Generated message map functions
	//{{AFX_MSG(VBScriptEditorMDI)
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VBSCRIPTEDITORMDI_H__6B591860_92B8_11D4_A30F_00500437F607__INCLUDED_)
