// QueryDesingerTables.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "QueryDesingerTables.h"
#include "BasicData.h"
#include "AddTableDlg.h"
#include "UWorkBenchDoc.h"
#include "UQueryDesingerDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// QueryDesingerTables

IMPLEMENT_DYNCREATE(QueryDesingerTables, CFormView)

QueryDesingerTables::QueryDesingerTables()
	: CFormView(QueryDesingerTables::IDD)
{
	//{{AFX_DATA_INIT(QueryDesingerTables)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

QueryDesingerTables::~QueryDesingerTables()
{
	omTabWnds.DeleteAll();
}


void QueryDesingerTables::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(QueryDesingerTables)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

void QueryDesingerTables::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
}


BEGIN_MESSAGE_MAP(QueryDesingerTables, CFormView)
	//{{AFX_MSG_MAP(QueryDesingerTables)
	ON_WM_RBUTTONDOWN()
	ON_COMMAND(50, OnAddTables)
	ON_MESSAGE(WM_DESTROY_FLY_WND, OnDestroyFlyWnd)
	ON_WM_CONTEXTMENU()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// QueryDesingerTables diagnostics

#ifdef _DEBUG
void QueryDesingerTables::AssertValid() const
{
	CFormView::AssertValid();
}

void QueryDesingerTables::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// QueryDesingerTables message handlers

void QueryDesingerTables::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	CMenu menu;
	menu.CreatePopupMenu();
	for (int i = menu.GetMenuItemCount(); --i >= 0;)
	{
	  menu.RemoveMenu(i, MF_BYPOSITION);
	}
	menu.AppendMenu(MF_STRING,50, LoadStg(IDS_STRING57608));	// confirm conflict
	ClientToScreen(&point);
	menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
	CFormView::OnRButtonDown(nFlags, point);
}

void QueryDesingerTables::OnAddTables()
{
	CStringArray olTableList;
	AddTableDlg *polDlg = new AddTableDlg(this, &olTableList);
	polDlg->DoModal();
	delete polDlg;
	CreateFlyWindows(olTableList);
}
CRect QueryDesingerTables::CalcNextFreeRect()
{
	CRect olRect;
	CRect olWndRect;
	GetClientRect(&olWndRect);
	int ilTotalW = olWndRect.right - olWndRect.left - 20;
	int ilLeft = 20;
	int ilTop  = 10;
	int ilRight = olWndRect.right - olWndRect.left - 20;
	int ilWidth = 123;
	int ilHeight = 153;
	int ilBuffer = 50;
	int ilPerLine = (int)((ilTotalW)/(ilLeft+ilWidth));
	ilPerLine++;

	int ilCount = omTabWnds.GetSize();
	int ilCurrLine = 1;
	int ilNewLine = (int)((ilCount + 1)/(ilPerLine));
	int ilNewCol = (ilCount)%(ilPerLine-1);
	int ilNLeft, ilNRight, ilNTop, ilNBottom;
	if(ilNewCol == 0)
	{
		ilNLeft = ilLeft;
	}
	else
	{
		ilNLeft  = (ilNewCol)*(ilLeft+ilWidth)+ilLeft;
	}
	ilNRight = ilNLeft+ilWidth;
	if(ilNewLine == 0)
	{
		ilNTop   = ilTop;
	}
	else
	{
		ilNTop   = ilNewLine*(ilTop+ilHeight)+10;
	}
	ilNBottom = ilNTop + ilHeight;
	olRect = CRect(ilNLeft, ilNTop, ilNRight,ilNBottom);

	return olRect;


}
void QueryDesingerTables::OnUpdate( CView* pSender, LPARAM lHint, CObject* pHint )
{
	//CView::OnUpdate( CView* pSender, LPARAM lHint, CObject* pHint );
	UQueryDesingerDoc *polDoc = (UQueryDesingerDoc *)GetDocument();
	for(int i = 0; i < omTabWnds.GetSize(); i++)
	{
		omTabWnds[i].Window->DestroyWindow();
	}
	omTabWnds.DeleteAll();
	if(polDoc != NULL)
	{
		if(polDoc->pomCurrentQuery != NULL)
		{
			CreateFlyWindows(polDoc->pomCurrentQuery->omUsedTables);
		}
	}
}
void QueryDesingerTables::OnTablesChanged()
{
	omTables.RemoveAll();
	for(int i = 0; i < omTabWnds.GetSize(); i++)
	{
		CString olTab;
		olTab = omTabWnds[i].Window->GetName();
		omTables.Add(olTab);
	}
	UQueryDesingerDoc *polDoc = (UQueryDesingerDoc *)GetDocument();
	if(polDoc != NULL)
	{
		polDoc->SetCurrentQueryDesignerTables(omTables);
	}
}

LONG QueryDesingerTables::OnDestroyFlyWnd(UINT wParam, LONG lParam)
{
	TableListFrameWnd *polWnd = (TableListFrameWnd*)lParam;
	for(int i = 0; i < omTabWnds.GetSize(); i++)
	{
		if(polWnd == omTabWnds[i].Window)
		{
			for(int j = 0; j < omTables.GetSize(); j++)
			{
				if(omTabWnds[i].Window->GetName() == omTables[j])
				{
					omTables.RemoveAt(j);
					omTabWnds[i].Window->DestroyWindow();
					UQueryDesingerDoc *polDoc = (UQueryDesingerDoc *)GetDocument();
					if(polDoc != NULL)
					{
						polDoc->SetCurrentQueryDesignerTables(omTables);
					}
					j = omTables.GetSize();
					omTabWnds.DeleteAt(i);
				}
			}
			i = omTabWnds.GetSize();
		}
	}
	return 0L;
}

BOOL QueryDesingerTables::OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CFormView::OnChildNotify(message, wParam, lParam, pLResult);
}

void QueryDesingerTables::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	// TODO: Add your message handler code here
	int o = 0;
	o++;
}

void QueryDesingerTables::OnClose() 
{

	SetActiveWindow();
	CFormView::OnClose();
}

void QueryDesingerTables::CreateFlyWindows(CStringArray &ropList)
{
	for(int i = 0; i < ropList.GetSize(); i++)
	{
		CRect olNewRect;
		olNewRect = CalcNextFreeRect();
		TableListFrameWnd *polFlyFrame = new TableListFrameWnd(this, ropList[i]);
		polFlyFrame->Create(NULL, "",  WS_CHILD| WS_HSCROLL | WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_SYSMENU/*| WS_MAXIMIZEBOX | WS_SYSMENU | WS_MINIMIZEBOX */,
							CRect(0, 0, 0, 0),this, NULL, 0, NULL);



		CRect o; polFlyFrame->GetWindowRect(&o);
		int W = o.right - o.left; 
		int H = o.bottom - o.top;
		FLY_WINDOWS *polFlyWnd = new FLY_WINDOWS;
		polFlyFrame->MoveWindow(&olNewRect);
		polFlyWnd->Rect = olNewRect;
		polFlyWnd->Window = polFlyFrame;
		omTabWnds.Add(polFlyWnd);
	}
	OnTablesChanged();
}