// CntlEvent.cpp: implementation of the CCntlEvent class.
//
//////////////////////////////////////////////////////////////////////
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	UWorkBench - Project
 *	
 *		VB Metod and DISPID will be linked in this class   
 *		
 *
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *
 *		Date		Author			Comment
 *		-------------------------------------------------------------
 *
 *		24/10/00	cla AAT/IR		Initial version
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "stdafx.h"
#include "uworkbench.h"
#include "CntlEvent.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCntlEvent::CCntlEvent()
{

}

CCntlEvent::CCntlEvent(DISPID id, CString evExt, CString vbMethodName)
{
	m_dispID = id;
	m_eventExt = evExt;
	m_vbMethodName = vbMethodName;
}

CCntlEvent::CCntlEvent(const CCntlEvent& rhs)
{
	m_dispID = rhs.m_dispID;
	m_eventExt = rhs.m_eventExt;
	m_vbMethodName = rhs.m_vbMethodName;
}

CCntlEvent::~CCntlEvent()
{

}

// ==================================================================
// 
// FUNCTION :  CCntlEvent::operator=()
// 
// * Description : Assigment operator
// 
// 
// * Author : [cla AAT/IR], Created : [24.10.00 15:37:49]
// 
// * Returns : [CCntlEvent&] -
// 
// * Function parameters : 
// [rhs] - CCntlEvent
// 
// ==================================================================
CCntlEvent& CCntlEvent::operator=(const CCntlEvent& rhs)
{
	m_dispID = rhs.m_dispID;
	m_eventExt = rhs.m_eventExt;
	m_vbMethodName = rhs.m_vbMethodName;
	return(*this);
}

// ==================================================================
// 
// FUNCTION :  CCntlEvent::operator==()
// 
// * Description :	compare operator
// 
// 
// * Author : [cla AAT/IR], Created : [24.10.00 15:43:54]
// 
// * Returns : [bool] -
// 
// * Function parameters : 
// [rhs] - CCntlEvent
// 
// ==================================================================
bool CCntlEvent::operator==(const CCntlEvent& rhs)const
{
	if(rhs.m_dispID == this->m_dispID)
	{
		return true;
	}
	return false;
}

// ==================================================================
// 
// FUNCTION :  CCntlEvent::operator==()
// 
// * Description :
// 
// 
// * Author : [cla AAT/IR], Created : [24.10.00 15:51:07]
// 
// * Returns : [bool] -
// 
// * Function parameters : 
// [rhs] - DISPID
// 
// ==================================================================
bool CCntlEvent::operator==(const DISPID& rhs)const
{
	if(rhs == this->m_dispID)
	{
		return true;
	}
	return false;
}