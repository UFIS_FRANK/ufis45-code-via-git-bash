#if !defined(AFX_SCRIPTPROCEDURE_H__2B153021_EDD2_11D3_A275_00500437F607__INCLUDED_)
#define AFX_SCRIPTPROCEDURE_H__2B153021_EDD2_11D3_A275_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Machine generated IDispatch wrapper class(es) created by Microsoft Visual C++

// NOTE: Do not modify the contents of this file.  If this class is regenerated by
//  Microsoft Visual C++, your modifications will be overwritten.

/////////////////////////////////////////////////////////////////////////////
// CScriptProcedure wrapper class

class CScriptProcedure : public COleDispatchDriver
{
public:
	CScriptProcedure() {}		// Calls COleDispatchDriver default constructor
	CScriptProcedure(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CScriptProcedure(const CScriptProcedure& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	CString GetName();
	long GetNumArgs();
	BOOL GetHasReturnValue();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCRIPTPROCEDURE_H__2B153021_EDD2_11D3_A275_00500437F607__INCLUDED_)
