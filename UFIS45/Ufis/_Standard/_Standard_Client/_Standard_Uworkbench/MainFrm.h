// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__8EF75898_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_)
#define AFX_MAINFRM_H__8EF75898_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CCSGlobl.h"
#include "UObjectDataDefinitions.h"

//#define CFrameWnd		CCJFrameWnd
//#define CToolBar		CCJToolBar
//#define CToolBarCtrl	CCJToolBarCtrl

#include "TabObjectRuntimeDoc.h"
#include "TabObjectRuntimeView.h"
//class TabObjectRuntimeDoc;
#include "UFISGridViewerDoc.h"
#include "UFISGridMDIChild.h"
#include "UFISGridViewerView.h"

struct ActiveMDI
{
	CString Name;
	CMDIChildWnd* pomMDI;
	CUFISGridViewerDoc* pomDoc;
};

class CMainFrame : public CCJMDIFrameWnd//CMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();

// Attributes
public:
	TabObjectRuntimeDoc*	m_pObjectDoc;
	CCJTabCtrlBar			m_wndWorkspace;
	CCJTabCtrlBar			m_wndProperties;
	CImageList				m_TabImages;

	CCSPtrArray<ActiveMDI> omActiveGrids;
	CCSPtrArray<ActiveMDI> omActiveScripts;
// Operations
public:

	// Activation for Grids
	void RemoveGridMDI(CString GridName);
	void AddGridMDI(CString GridName, UFISGridMDIChild* popMDI, CUFISGridViewerDoc* popDoc);
	void ActivateGridMDI(CString GridName);

	//Activation for VB-Script-Editors
	void RemoveScriptMDI(CString opName);
	void AddScriptMDI(CString opName, CMDIChildWnd* popMDI);
	void ActivateScriptMDI(CString opName);

	//Open MDI-Childs
	void OnNewQueryDesingerWindow();
	void OnNewQueryDesingerWindow(UQuery *popQuery);
	void OnNewUniversalGrid(UGridDefinition *popGridDef);
	void OnShowUFISGrid(UGridDefinition *popGridDef);

	void UpdateTabView();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();

	void LoadOtherFrame(UINT ipID){LoadFrame(ipID);}


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CReBar      m_wndReBar;
	CDialogBar      m_wndDlgBar;

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg BOOL OnNcActivate( BOOL bActive );
	afx_msg LONG OnBcAdd(UINT wParam, LONG lParam);
	afx_msg void OnSaveVbw();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__8EF75898_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_)
