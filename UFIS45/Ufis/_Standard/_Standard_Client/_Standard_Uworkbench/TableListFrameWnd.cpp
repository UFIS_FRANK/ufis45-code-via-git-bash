// TableListFrameWnd.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "TableListFrameWnd.h"
//#include "UQueryDesignerTableView.h"
#include "UScrollContainerView.h"
#include "CCSGlobl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// TableListFrameWnd

IMPLEMENT_DYNCREATE(TableListFrameWnd, CFrameWnd)

TableListFrameWnd::TableListFrameWnd()
{
}
TableListFrameWnd::TableListFrameWnd(CWnd *pParent, CString opTable)
{
	pomParent = pParent;
	omTable = opTable;
}

TableListFrameWnd::~TableListFrameWnd()
{
	delete pomList;
}


BEGIN_MESSAGE_MAP(TableListFrameWnd, CFrameWnd)
	//{{AFX_MSG_MAP(TableListFrameWnd)
	ON_WM_SIZE()
	ON_WM_MOVE()
	ON_WM_CREATE()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_LBUTTONDOWN()
	ON_COMMAND(60, OnDeleteTable)
	ON_WM_CLOSE()
	ON_MESSAGE(WM_DND_LBOX_BEGIN_DRAG, OnBeginDrag)
	ON_MESSAGE(WM_DND_LBOX_DRAG_OVER, OnDragOver)
	ON_MESSAGE(WM_DND_LBOX_DROP, OnDrop)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// TableListFrameWnd message handlers

void TableListFrameWnd::OnSize(UINT nType, int cx, int cy) 
{
	CFrameWnd::OnSize(nType, cx, cy);
	
	CRect olRect;
	GetClientRect(&olRect);
	pomList->MoveWindow(&olRect);
	UpdateWindow();	
/*	olRect.InflateRect(1, 1, 1, 1);
	ClientToScreen(&olRect);
	pomParent->ScreenToClient(&olRect);
	pomParent->InvalidateRect(&olRect);
*/	UpdateWindow();

}

void TableListFrameWnd::OnMove(int x, int y) 
{
	CFrameWnd::OnMove(x, y);
}

int TableListFrameWnd::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	pomList	= new UDragDropListBox(this);//CListBox();
	CRect olRect;
	GetClientRect(&olRect);
	pomList->Create(WS_OVERLAPPED|WS_VISIBLE|LBS_SORT|/*LBS_MULTIPLESEL*/LBS_EXTENDEDSEL    | LBS_NOINTEGRALHEIGHT | WS_VSCROLL | WS_TABSTOP ,olRect, this, 0);
	pomList->ShowWindow(SW_SHOWNORMAL);

	CCSPtrArray<RecordSet> olRecords;
	ogBCD.GetRecords("SYS", "TANA", omTable, &olRecords);
	CString olFirst;
	olFirst = CString("*");
	pomList->SetFont(&ogCourier_Regular_8);

	pomList->AddString(olFirst);
	for(int i = 0; i < olRecords.GetSize(); i++)
	{
		CString olSrcStr;
		olSrcStr = olRecords[i].Values[ogBCD.GetFieldIndex("SYS", "FINA")];
//		olSrcStr += CString("    ") + olRecords[i].Values[ogBCD.GetFieldIndex("SYS", "ADDI")];;
		pomList->AddString(olSrcStr );
	}
	olRecords.DeleteAll();
	SetWindowText(omTable);
	SetWindowPos( &wndTop, 0, 0, 0, 0, SWP_SHOWWINDOW);



	return 0;
}

void TableListFrameWnd::OnRButtonDown(UINT nFlags, CPoint point) 
{
	CMenu menu;
	menu.CreatePopupMenu();
	for (int i = menu.GetMenuItemCount(); --i >= 0;)
	{
	  menu.RemoveMenu(i, MF_BYPOSITION);
	}
	menu.AppendMenu(MF_STRING,60, LoadStg(IDS_STRING57610));	// confirm conflict
	ClientToScreen(&point);
	menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
	
	CFrameWnd::OnRButtonDown(nFlags, point);
}

void TableListFrameWnd::OnDeleteTable()
{
	pomParent->PostMessage(WM_DESTROY_FLY_WND, 0, (long)this);
}

void TableListFrameWnd::OnRButtonUp(UINT nFlags, CPoint point) 
{
	CFrameWnd::OnRButtonUp(nFlags, point);
}

void TableListFrameWnd::OnLButtonDown(UINT nFlags, CPoint point) 
{
	
	CFrameWnd::OnLButtonDown(nFlags, point);
}

void TableListFrameWnd::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	pomParent->PostMessage(WM_DESTROY_FLY_WND, 0, (long)this);
	
//	CFrameWnd::OnClose();
}

LONG TableListFrameWnd::OnBeginDrag(UINT wParam, LONG lParam)
{
	CString olText;
	pomList->GetText((int)wParam, olText);
	((/*UQueryDesignerTableView*/UScrollContainerView *)pomParent)->SetRelSourceTable(omTable, olText);
	pomParent->SendMessage(WM_DND_LBOX_BEGIN_DRAG, wParam, lParam);
	return 0L;
}

LONG TableListFrameWnd::OnDragOver(UINT wParam, LONG lParam)
{
	pomParent->SendMessage(WM_DND_LBOX_DRAG_OVER, wParam, lParam);
	return 0L;
}

LONG TableListFrameWnd::OnDrop(UINT wParam, LONG lParam)
{
	CString olText;
	pomList->GetText((int)lParam, olText);
	((/*UQueryDesignerTableView*/UScrollContainerView *)pomParent)->SetRelDestinationTable(omTable, olText);
	pomParent->SendMessage(WM_DND_LBOX_DROP, wParam, lParam);

	return 0L;
}