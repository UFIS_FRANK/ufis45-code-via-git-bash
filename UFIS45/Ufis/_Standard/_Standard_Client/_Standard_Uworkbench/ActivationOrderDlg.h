#if !defined(AFX_ACTIVATIONORDERDLG_H__BF920FD1_853F_11D3_A20A_00500437F607__INCLUDED_)
#define AFX_ACTIVATIONORDERDLG_H__BF920FD1_853F_11D3_A20A_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ActivationOrderDlg.h : header file
//
#include "GxGridTab.h"
#include "CtrlProperties.h"

/////////////////////////////////////////////////////////////////////////////
// ActivationOrderDlg dialog

class ActivationOrderDlg : public CDialog
{
// Construction
public:
	ActivationOrderDlg(CWnd* pParent, UFormProperties *popProperty);   // standard constructor

// Dialog Data
	//{{AFX_DATA(ActivationOrderDlg)
	enum { IDD = IDD_ACTIVATION_ORDER };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	UFormProperties *pomProperty;
	GxGridTab omGrid;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ActivationOrderDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ActivationOrderDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACTIVATIONORDERDLG_H__BF920FD1_853F_11D3_A20A_00500437F607__INCLUDED_)
