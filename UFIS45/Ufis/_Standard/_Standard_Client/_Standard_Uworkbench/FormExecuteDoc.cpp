// FormExecuteDoc.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "FormExecuteDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FormExecuteDoc

IMPLEMENT_DYNCREATE(FormExecuteDoc, CDocument)

FormExecuteDoc::FormExecuteDoc()
{
	imCursorPos = -1;
}

BOOL FormExecuteDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	return TRUE;
}

FormExecuteDoc::~FormExecuteDoc()
{
}


BEGIN_MESSAGE_MAP(FormExecuteDoc, CDocument)
	//{{AFX_MSG_MAP(FormExecuteDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FormExecuteDoc diagnostics

#ifdef _DEBUG
void FormExecuteDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void FormExecuteDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// FormExecuteDoc serialization

void FormExecuteDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// FormExecuteDoc commands

BOOL FormExecuteDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
//	if (!CDocument::OnOpenDocument(lpszPathName))
//		return FALSE;

	pomForm = (UFormProperties *)lpszPathName;
	omMainTable = pomForm->omTable;
	// TODO: Add your specialized creation code here
	
	return TRUE;
}

BOOL FormExecuteDoc::CanCloseFrame(CFrameWnd* pFrame) 
{
//	return CDocument::CanCloseFrame(pFrame);
	return TRUE;
}

bool FormExecuteDoc::GetFirstRecord(RecordSet &opRecord)
{
	int ilDataCount = ogBCD.GetDataCount(omMainTable);
	if(ilDataCount > 0)
	{
		imCursorPos = 0;
		ogBCD.GetRecord(omMainTable, 0,  opRecord);
		omCurrentRecord = opRecord;
		return true;
	}
	return false;
}

bool FormExecuteDoc::GetNextRecord(RecordSet &opRecord)
{
	int ilDataCount = ogBCD.GetDataCount(omMainTable);
	if((ilDataCount > 0) && ((imCursorPos+1) < ilDataCount))
	{
		imCursorPos++;
		ogBCD.GetRecord(omMainTable, imCursorPos,  opRecord);
		omCurrentRecord = opRecord;
		return true;
	}
	return false;
}

bool FormExecuteDoc::GetPrevRecord(RecordSet& opRecord)
{
	int ilDataCount = ogBCD.GetDataCount(omMainTable);
	if((imCursorPos > 0) && (ilDataCount > 0 ))
	{
		imCursorPos--;
		ogBCD.GetRecord(omMainTable, imCursorPos,  opRecord);
		omCurrentRecord = opRecord;
		return true;
	}
	return false;
}

bool FormExecuteDoc::GetLastRecord(RecordSet& opRecord)
{
	int ilDataCount = ogBCD.GetDataCount(omMainTable);
	if(ilDataCount > 0)
	{
		imCursorPos = ilDataCount-1;
		ogBCD.GetRecord(omMainTable, imCursorPos,  opRecord);
		omCurrentRecord = opRecord;
		return true;
	}
	return false;
}

bool FormExecuteDoc::NewRecord()
{
	return true;
}

bool FormExecuteDoc::SaveRecord()
{
	return true;
}

bool FormExecuteDoc::DeleteRecord()
{
	return true;
}
