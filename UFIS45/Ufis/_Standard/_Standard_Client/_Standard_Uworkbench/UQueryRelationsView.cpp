// UQueryRelationsView.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "UQueryRelationsView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// UQueryRelationsView

IMPLEMENT_DYNCREATE(UQueryRelationsView, CFormView)

UQueryRelationsView::UQueryRelationsView()
	: CFormView(UQueryRelationsView::IDD)
{
	//{{AFX_DATA_INIT(UQueryRelationsView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

UQueryRelationsView::~UQueryRelationsView()
{
}

void UQueryRelationsView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(UQueryRelationsView)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(UQueryRelationsView, CFormView)
	//{{AFX_MSG_MAP(UQueryRelationsView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UQueryRelationsView diagnostics

#ifdef _DEBUG
void UQueryRelationsView::AssertValid() const
{
	CFormView::AssertValid();
}

void UQueryRelationsView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// UQueryRelationsView message handlers

void UQueryRelationsView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
	
}

void UQueryRelationsView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	int o = 0;
	o++;
}
