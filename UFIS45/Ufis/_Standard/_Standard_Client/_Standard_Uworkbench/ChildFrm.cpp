// ChildFrm.cpp : implementation of the CChildFrame class
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "UWorkBenchView.h"
#include "ChildFrm.h"
#include "GridView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChildFrame

IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CChildFrame)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChildFrame construction/destruction

CChildFrame::CChildFrame()
{
	// TODO: add member initialization code here
	
}

CChildFrame::~CChildFrame()
{
}

BOOL CChildFrame::OnCreateClient( LPCREATESTRUCT lpcs,
	CCreateContext* pContext)
{
	m_wndSplitter.CreateStatic(this, 3, 3);

	// add the first splitter pane - the default view in column 0
	int cyText = max(lpcs->cy - 70, 20);    // height of text pane

	for(int i = 0; i < 3; i++)
	{
		for(int j = 0; j < 3; j++)
		{
			if(j == 2)
			{
				if (!m_wndSplitter.CreateView(i, j,
					RUNTIME_CLASS(CGridView), CSize(0, cyText), pContext))
				{
					TRACE0("Failed to create second pane\n");
					return FALSE;
				}
			}
			else
			{
				if (!m_wndSplitter.CreateView(i, j,
					RUNTIME_CLASS(CUWorkBenchView), CSize(0, cyText), pContext))
				{
					TRACE0("Failed to create second pane\n");
					return FALSE;
				}
			}
		}
	}
	return TRUE;
/*	return m_wndSplitter.Create( this,
		2, 2,                 // TODO: adjust the number of rows, columns
		CSize( 10, 10 ),      // TODO: adjust the minimum pane size
		pContext );
*/
}

BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CChildFrame diagnostics

#ifdef _DEBUG
void CChildFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CChildFrame message handlers
