// GridDefinitionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "GridDefinitionDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GridDefinitionDlg dialog


GridDefinitionDlg::GridDefinitionDlg(CWnd* pParent /*=NULL*/)
	: CDialog(GridDefinitionDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(GridDefinitionDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void GridDefinitionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GridDefinitionDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GridDefinitionDlg, CDialog)
	//{{AFX_MSG_MAP(GridDefinitionDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GridDefinitionDlg message handlers
