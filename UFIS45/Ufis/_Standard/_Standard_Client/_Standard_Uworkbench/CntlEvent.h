// CntlEvent.h: interface for the CCntlEvent class.
//
//////////////////////////////////////////////////////////////////////
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	UWorkBench - Project
 *	
 *		VB Metod and DISPID will be linked in this class   
 *		
 *
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *
 *		Date		Author			Comment
 *		-------------------------------------------------------------
 *
 *		24/10/00	cla AAT/IR		Initial version
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#if !defined(AFX_CNTLEVENT_H__74C3D910_A9B9_11D4_906A_00010204AA51__INCLUDED_)
#define AFX_CNTLEVENT_H__74C3D910_A9B9_11D4_906A_00010204AA51__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CCntlEvent  
{
public:
	CCntlEvent();
	CCntlEvent(DISPID id, CString evExt, CString vbMethodName);
	CCntlEvent(const CCntlEvent& rhs);
	CCntlEvent& operator=(const CCntlEvent& rhs);
	bool operator==(const CCntlEvent& rhs)const;
	bool operator==(const DISPID& rhs)const;
	virtual ~CCntlEvent();

	DISPID m_dispID;
	CString m_eventExt;
	CString m_vbMethodName;
};

#endif // !defined(AFX_CNTLEVENT_H__74C3D910_A9B9_11D4_906A_00010204AA51__INCLUDED_)
