#if !defined(AFX_UUNILISTVIEW_H__CDDEF228_31E7_11D3_B081_00001C019205__INCLUDED_)
#define AFX_UUNILISTVIEW_H__CDDEF228_31E7_11D3_B081_00001C019205__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UUniListView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// UUniListView view

#include "UListCtrlExtended.h"


class UUniListView : public CListView
{
protected:
	UUniListView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(UUniListView)

// Attributes
public:

// Operations
public:

	void EraseList();
	void OnDeleteItem(UINT nFlags, CPoint point);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UUniListView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~UUniListView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(UUniListView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UUNILISTVIEW_H__CDDEF228_31E7_11D3_B081_00001C019205__INCLUDED_)
