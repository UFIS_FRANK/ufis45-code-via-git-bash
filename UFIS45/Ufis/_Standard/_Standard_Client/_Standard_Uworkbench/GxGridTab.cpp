// GxGridTab.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "GxGridTab.h"
#include "FormDialogDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GxGridTab

/*CCE*/IMPLEMENT_DYNCREATE(GxGridTab, CGXGridWnd)

GxGridTab::GxGridTab():CGXGridWnd()
{
	pomParent = NULL;
	bmAutoGrow = true;
	bmInEndEnditing = false;
}



GxGridTab::GxGridTab(CWnd *popParent, CString opParentType /*= CString("")*/):CGXGridWnd()
{
	pomParent = NULL;
	pomParent = popParent;
	bmAutoGrow = true;
	bmInEndEnditing = false;
	omParentType = opParentType;
}


void GxGridTab::EnableAutoGrow ( bool enable /*=true*/ )
{
	bmAutoGrow = enable;
}

GxGridTab::~GxGridTab()
{
}

void GxGridTab::OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol)
{
	CGXGridWnd::OnClickedButtonRowCol(nRow, nCol);
	if(pomParent != NULL)
	{
		GxMiniGridInfo *polInfo = new GxMiniGridInfo();
		polInfo->Col = nCol;
		polInfo->Row = nRow;
		polInfo->pGrid = this;
		pomParent->SendMessage(GX_CELL_BUTTONCLICKED, (WPARAM)nRow, (LPARAM)nCol);
		delete polInfo;
	}
}
BOOL GxGridTab::OnEndEditing(ROWCOL nRow, ROWCOL nCol)
{
	BOOL blRet = TRUE;
	if(bmInEndEnditing == false)
	{
		bmInEndEnditing = true;
		blRet = CGXGridWnd::OnEndEditing(nRow, nCol);
		CString olStr = GetValueRowCol(nRow, nCol);
		if ( bmAutoGrow == true)
		{
			DoAutoGrow ( nRow, nCol) ;
		}
		if (nRow>=1 )
		{
			CGXStyle olStyle = LookupStyleRowCol ( nRow, nCol );
			if ( bmAutoGrow == true)
			{
				DeleteEmptyRows ( nRow, nCol ) ;
			}
		}
		if(pomParent != NULL)
		{
			GxMiniGridInfo *polInfo = new GxMiniGridInfo();
			polInfo->Col = nCol;
			polInfo->Row = nRow;
			polInfo->pGrid = this;
			pomParent->SendMessage(GX_ROWCOL_CHANGED, (WPARAM)(long)(void*)polInfo, (LPARAM)nCol);
			delete polInfo;
		}
		bmInEndEnditing = false;
	}
	return blRet;

}

BOOL GxGridTab::OnRButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	BOOL blRet = CGXGridWnd::OnRButtonClickedRowCol(nRow, nCol, nFlags, pt);
	if(pomParent != NULL)
	{
		GxMiniGridInfo *polInfo = new GxMiniGridInfo();
		polInfo->Col = nCol;
		polInfo->Row = nRow;
		polInfo->pt = pt;
		polInfo->pGrid = this;
		pomParent->SendMessage(GX_CELL_R_BUTTONCLICKED, (WPARAM)(long)(void*)polInfo, (LPARAM)nCol);
		delete polInfo;
	}
	return blRet;
}


BOOL GxGridTab::OnLeftCell(ROWCOL nRow, ROWCOL nCol, ROWCOL nNewRow, ROWCOL nNewCol)
{
	BOOL blRet = TRUE;

	blRet = CGXGridWnd::OnLeftCell(nRow, nCol, nNewRow, nNewCol);

	if(pomParent != NULL)
	{
		GxMiniGridInfo *polInfo = new GxMiniGridInfo();
		polInfo->Col = nCol;
		polInfo->Row = nRow;
		polInfo->pGrid = this;
		pomParent->SendMessage(GX_ROWCOL_CHANGED, (WPARAM)(long)(void*)polInfo, (LPARAM)nCol);
		delete polInfo;
	}
	return blRet;
}

void GxGridTab::DoAutoGrow ( ROWCOL nRow, ROWCOL nCol) 
{
	ROWCOL ilRowCount = GetRowCount ();
	if ( bmAutoGrow &&( nRow >= ilRowCount ) )
	{	//  es wird bereits in letzter Zeile editiert, d.h. event. Grid verl�ngern
		if(pomParent != NULL)
		{
			if(omParentType == "FORM_DLG")
			{
				FormDialogDlg *polParent = (FormDialogDlg *)pomParent;
				if(polParent->IsLineOk(nRow) == true)
				{
					if ( ! GetValueRowCol ( nRow, nCol).IsEmpty () )
						InsertBottomRow ();
				}
			}
		}
		else
		{
			if ( ! GetValueRowCol ( nRow, nCol).IsEmpty () )
				InsertBottomRow ();
		}
	}
}

BOOL GxGridTab::InsertBottomRow ()
{
	ROWCOL ilRowCount = GetRowCount ();
	//LockUpdate(TRUE);
	BOOL blRet = InsertRows ( ilRowCount+1, 1 );
	CopyStyleLastLineFromPrev ();
	//LockUpdate(FALSE);
	return blRet;
}
void GxGridTab::CopyStyleOfLine ( ROWCOL ipSourceLine, ROWCOL ipDestLine,
									 bool bpResetValues/*=true*/ )
{
	ROWCOL ilColCount = GetColCount ();
	CGXStyle olStyle;

	for ( ROWCOL i=0; i<=ilColCount; i++ )
	{
		ComposeStyleRowCol( ipSourceLine, i, &olStyle );
/*		if ( bpResetValues )
		{
			olStyle.SetIncludeItemDataPtr ( FALSE );
			if ( i>0 )
				olStyle.SetInterior(WHITE);
		}
*/
		SetStyleRange  ( CGXRange ( ipDestLine, i), olStyle );
		if ( bpResetValues )
		{
			SetValueRange ( CGXRange ( ipDestLine, i), "" );
		}
	}
}

void GxGridTab::CopyStyleLastLineFromPrev ( bool bpResetValues/*=true*/ )
{
	ROWCOL ilRowCount = GetRowCount ();
	CopyStyleOfLine ( ilRowCount-1, ilRowCount, bpResetValues );
}

bool GxGridTab::DeleteEmptyRows ( ROWCOL nRow, ROWCOL nCol ) 
{
	ROWCOL  ilRowCount = GetRowCount();
	//  Wenn nicht in der ersten Spalte gel�scht bzw. editiert worden ist
	//  gibt es keine Zeile zu l�schen
	if ( nCol != 1 )
		return false;
	if ( ( nRow>=1 ) && ( nRow<ilRowCount ) &&
		  GetValueRowCol ( nRow, nCol ).IsEmpty() )
	{
		RemoveRows( nRow, nRow );
		//SetRowHeadersText ();
		//RemoveOneRow( nRow );
		return true;
	}
	else 
		return false;
}


BEGIN_MESSAGE_MAP(GxGridTab, CGXGridWnd)
	//{{AFX_MSG_MAP(GxGridTab)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// GxGridTab message handlers

//XXX MWO
/*CCE*/IMPLEMENT_DYNCREATE(GxBrowseTab, CGXBrowserWnd)

GxBrowseTab::GxBrowseTab():CGXBrowserWnd()
{
	pomParent = NULL;
}

GxBrowseTab::GxBrowseTab(CWnd *popParent):CGXBrowserWnd()
{
	pomParent = NULL;
	pomParent = popParent;
}

GxBrowseTab::~GxBrowseTab()
{
}

void GxBrowseTab::OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol)
{
	CGXBrowserGrid::OnClickedButtonRowCol(nRow, nCol);
	if(pomParent != NULL)
	{
		pomParent->SendMessage(GX_CELL_BUTTONCLICKED, (WPARAM)nRow, (LPARAM)nCol);
	}
}
BOOL GxBrowseTab::OnEndEditing(ROWCOL nRow, ROWCOL nCol)
{
	CGXBrowserGrid::OnEndEditing(nRow, nCol);
	CString olStr = GetValueRowCol(nRow, nCol);
	if(pomParent != NULL)
	{
		pomParent->SendMessage(GX_ROWCOL_CHANGED, (WPARAM)nRow, (LPARAM)nCol);
	}
	return TRUE;
}

BEGIN_MESSAGE_MAP(GxBrowseTab, CGXBrowserWnd)
	//{{AFX_MSG_MAP(GxBrowseTab)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// GxBrowseTab message handlers




