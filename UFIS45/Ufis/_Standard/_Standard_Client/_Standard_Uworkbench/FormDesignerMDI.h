#if !defined(AFX_FORMDESIGNERMDI_H__99507046_6CE0_11D3_A1EA_0000B4984BBE__INCLUDED_)
#define AFX_FORMDESIGNERMDI_H__99507046_6CE0_11D3_A1EA_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FormDesignerMDI.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// FormDesignerMDI frame

class FormDesignerMDI : public CMDIChildWnd
{
	DECLARE_DYNCREATE(FormDesignerMDI)
protected:
	FormDesignerMDI();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:
	CToolBar    m_wndToolBar; //IDR_UNIVERSAL_GRID_TOOLBAR
	CReBar      m_wndReBar;

	void OnArrow();
	void OnLabel();
	void OnEdit();  
	void OnGroup();    
	void OnButton();   
	void OnCombobox(); 
	void OnListbox();  
	void OnRadio();    
	void OnCheckbox(); 
	void OnLine();
	void OnRect();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FormDesignerMDI)
	public:
	virtual void ActivateFrame(int nCmdShow = -1);
	virtual BOOL DestroyWindow();
	protected:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~FormDesignerMDI();

	// Generated message map functions
	//{{AFX_MSG(FormDesignerMDI)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FORMDESIGNERMDI_H__99507046_6CE0_11D3_A1EA_0000B4984BBE__INCLUDED_)
