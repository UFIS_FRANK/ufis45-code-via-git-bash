#if !defined(AFX_QUERYCHILDFRAME_H__8EF758C5_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_)
#define AFX_QUERYCHILDFRAME_H__8EF758C5_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// QueryChildFrame.h : header file
//

#include "QueryDesignerGridView.h"
#include "UQueryDesignerTableView.h"

/////////////////////////////////////////////////////////////////////////////
// CQueryChildFrame frame

class CQueryChildFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CQueryChildFrame)
protected:
	CQueryChildFrame();           // protected constructor used by dynamic creation

// Attributes
public:
	CSplitterWnd m_wndSplitter;
	CSplitterWnd m_wndSplitter2;
	bool bmIsInit;
// Operations
public:

	CRuntimeClass *pomTableArea, *pomGridArea, *pomRelationArea;
	UQueryDesignerTableView *pom1;
	CQueryDesignerGridView  *pom2;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CQueryChildFrame)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle = WS_CHILD | WS_VISIBLE | WS_OVERLAPPEDWINDOW, const RECT& rect = rectDefault, CMDIFrameWnd* pParentWnd = NULL, CCreateContext* pContext = NULL);
	protected:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CQueryChildFrame();

	// Generated message map functions
	//{{AFX_MSG(CQueryChildFrame)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_QUERYCHILDFRAME_H__8EF758C5_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_)
