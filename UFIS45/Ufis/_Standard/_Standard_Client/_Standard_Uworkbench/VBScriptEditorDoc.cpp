// VBScriptEditorDoc.cpp : implementation file
//

#include "stdafx.h"
#include "uworkbench.h"
#include "VBScriptEditorDoc.h"
#include "VBScriptEditorView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CVBScriptEditorDoc

IMPLEMENT_DYNCREATE(CVBScriptEditorDoc, CDocument)

BEGIN_MESSAGE_MAP(CVBScriptEditorDoc, CDocument)
	//{{AFX_MSG_MAP(CVBScriptEditorDoc)
//	ON_COMMAND(IDC_SAVE_VBW2, OnSaveVbw2)
	ON_COMMAND(ID_SAVE_VBW, OnSaveVbw)
	ON_COMMAND(ID_SAVE_VBW3, OnSaveVbw3)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVBScriptEditorDoc construction/destruction

CVBScriptEditorDoc::CVBScriptEditorDoc()
{
	// TODO: add one-time construction code here

}

CVBScriptEditorDoc::~CVBScriptEditorDoc()
{
}

BOOL CVBScriptEditorDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	((CVBScriptEditorView*)m_viewList.GetHead())->SetWindowText(NULL);

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CVBScriptEditorDoc serialization

void CVBScriptEditorDoc::Serialize(CArchive& ar)
{
	CVBScriptEditorView* pView = (CVBScriptEditorView*)m_viewList.GetHead();
	ASSERT_VALID(pView);
	ASSERT_KINDOF(CVBScriptEditorView, pView);

	CRichEditCtrl& wndRich = pView->GetRichEditCtrl();
	CString str;

	//Write/Read raw data from file to/from RichEditControl
	if (ar.IsStoring())	{
		wndRich.GetWindowText(str);
		ar.Write((LPVOID)(LPCTSTR)str, str.GetLength()*sizeof(TCHAR));
	} else {
		CFile* pFile = ar.GetFile();
		ASSERT(pFile->GetPosition() == 0);
		DWORD nFileSize = pFile->GetLength();
		ar.Read((LPVOID)str.GetBuffer(nFileSize), nFileSize/sizeof(TCHAR));
		wndRich.SetWindowText(str);
	}

	ASSERT_VALID(this);
}

/////////////////////////////////////////////////////////////////////////////
// CVBScriptEditorDoc diagnostics

#ifdef _DEBUG
void CVBScriptEditorDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CVBScriptEditorDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CVBScriptEditorDoc commands

void CVBScriptEditorDoc::OnCloseDocument() 
{
	OnSaveDocument(omFileName.GetBuffer(0));	
	CDocument::OnCloseDocument();
}

BOOL CVBScriptEditorDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	omFileName = CString(lpszPathName);

	TRY
	{
	CFile olFile(lpszPathName, CFile::modeRead|CFile::modeWrite );

	olFile.Close();
	}
	CATCH( CFileException, e )
	{   
		if(e->m_cause == CFileException::fileNotFound)
		{
			return OnNewDocument();
		}
	}
	END_CATCH
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;
	

	
	return TRUE;
}

void CVBScriptEditorDoc::OnSaveVbw2() 
{
	// TODO: Add your command handler code here
	AfxMessageBox("Bla Bla");	
}

void CVBScriptEditorDoc::OnSaveVbw() 
{
	// TODO: Add your command handler code here
	
}

void CVBScriptEditorDoc::OnSaveVbw3() 
{
	// TODO: Add your command handler code here
	
}
