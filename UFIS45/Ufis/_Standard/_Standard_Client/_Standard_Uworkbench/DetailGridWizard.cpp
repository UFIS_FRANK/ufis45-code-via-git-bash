// DetailGridWizard.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "DetailGridWizard.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DetailGridWizard dialog




DetailGridWizard::DetailGridWizard(CWnd* pParent, UGridCtrlProperties *popProperty)
	: CDialog(DetailGridWizard::IDD, pParent)
{
	pomProperty = popProperty;
	//{{AFX_DATA_INIT(DetailGridWizard)
	v_GridName = _T("");
	v_Masterreference = _T("");
	v_Detailreference = _T("");
	//}}AFX_DATA_INIT
	omPropertyGrid.EnableAutoGrow(false);
}


void DetailGridWizard::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DetailGridWizard)
	DDX_Control(pDX, IDC_DETAILREFERENCE, m_Detailreference);
	DDX_Control(pDX, IDC_MASTERREFERENCE, m_Masterreference);
	DDX_Control(pDX, IDC_TABLES, m_Tables);
	DDX_Control(pDX, IDC_SOURCEFIELDS, m_SourceFields);
	DDX_Control(pDX, IDC_GRID_NAME, m_GridName);
	DDX_Text(pDX, IDC_GRID_NAME, v_GridName);
	DDX_Text(pDX, IDC_MASTERREFERENCE, v_Masterreference);
	DDX_Text(pDX, IDC_DETAILREFERENCE, v_Detailreference);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DetailGridWizard, CDialog)
	//{{AFX_MSG_MAP(DetailGridWizard)
	ON_BN_CLICKED(IDC_SINGLETOLEFT, OnSingletoleft)
	ON_BN_CLICKED(IDC_SINGLETORIGHT, OnSingletoright)
	ON_LBN_SELCHANGE(IDC_SOURCEFIELDS, OnSelchangeSourcefields)
	ON_CBN_SELCHANGE(IDC_TABLES, OnSelchangeTables)
	ON_BN_CLICKED(IDC_MULTITORIGHT, OnMultitoright)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DetailGridWizard message handlers

void DetailGridWizard::OnSingletoleft() 
{
	CString olTable;
	m_Tables.GetWindowText(olTable);
	CRowColArray olRows;

	omPropertyGrid.GetSelectedRows(olRows);
	for(int i = olRows.GetSize()-1; i >= 0; i--)
	//while(olRows.GetSize() > 0)
	{
		if(olRows[i] != 0)
		{
			CString olTab, olField;
			olTab = omPropertyGrid.GetValueRowCol(olRows[i], 1);
			olField = omPropertyGrid.GetValueRowCol(olRows[i], 2);
			if(olTable == olTab)
			{
				m_SourceFields.AddString(olField);
			}
			omPropertyGrid.RemoveRows(olRows[i], olRows[i]);
		}
	}

}

void DetailGridWizard::OnSingletoright() 
{
	CString olTable;
	m_Tables.GetWindowText(olTable);
	if(!olTable.IsEmpty())
	{
		int ilIdx = m_SourceFields.GetCurSel();	
		if(ilIdx != LB_ERR)
		{
			CString olField;
			m_SourceFields.GetText(ilIdx, olField);
			omPropertyGrid.InsertRows(omPropertyGrid.GetRowCount( )+1, 1);
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 1), olTable);
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 2), olField);
			CString olUrno = ogBCD.GetFieldExt("SYS", "TANA", "FINA", olTable, olField, "URNO");
			CString olHeader;
			CString olType;
			CString olRefe;
			CString olReqf;
			RecordSet olRec;
			ogBCD.GetRecord("SYS", "URNO", olUrno, olRec);
			olHeader = olRec[ogBCD.GetFieldIndex("SYS", "LABL")];
			olHeader.TrimLeft();olHeader.TrimRight();
			olType = olRec[ogBCD.GetFieldIndex("SYS", "TYPE")];
			olReqf = olRec[ogBCD.GetFieldIndex("SYS", "REQF")];
			if(olHeader.IsEmpty())
			{
				olHeader = olRec[ogBCD.GetFieldIndex("SYS", "ADDI")];
			}
			olRefe = olRec[ogBCD.GetFieldIndex("SYS", "REFE")];
			if(olRefe ==".")
			{
				olRefe.Empty();
			}
			CString olChoiceList;
			for(int i = 0; i < ogQueries.GetSize(); i++)
			{
				olChoiceList += ogQueries[i].omName + CString("\n");
			}
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 3), olType);
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 4), olHeader);
			omPropertyGrid.SetStyleRange(CGXRange(omPropertyGrid.GetRowCount( ), 5), CGXStyle().SetChoiceList("")
															.SetHorizontalAlignment(DT_CENTER)
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
															.SetControl(GX_IDS_CTRL_CHECKBOX3D));
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 5), _T("1"));
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 6), olRefe);
			omPropertyGrid.SetStyleRange(CGXRange(omPropertyGrid.GetRowCount( ), 7), CGXStyle().SetChoiceList("")
															.SetHorizontalAlignment(DT_CENTER)
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
															.SetControl(GX_IDS_CTRL_CHECKBOX3D));
			if(olReqf == "Y")
			{
				omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 7), _T("1"));
			}
			else
			{
				omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 7), _T("0"));
			}
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 8), _T("80"));
			m_SourceFields.DeleteString(ilIdx);
		}
	}
}

void DetailGridWizard::OnSelchangeSourcefields() 
{
	// TODO: Add your control notification handler code here
	
}

void DetailGridWizard::OnSelchangeTables() 
{
	int ilIdx = m_Tables.GetCurSel();	
	m_SourceFields.ResetContent();
	if(ilIdx != LB_ERR)
	{
		CString olText;
		m_Tables.GetLBText(ilIdx, olText);
		CCSPtrArray<RecordSet> olRecords;

		ogBCD.GetRecords("SYS", "TANA", olText, &olRecords);
		for(int i = 0; i < olRecords.GetSize(); i++)
		{
			m_SourceFields.AddString(olRecords[i].Values[ogBCD.GetFieldIndex("SYS", "FINA")]);
		}
		olRecords.DeleteAll();
	}
}

void DetailGridWizard::OnOK() 
{
	if(GetValues() == true)
	{
		CDialog::OnOK();
	}
}

BOOL DetailGridWizard::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_SourceFields.SetFont(&ogCourier_Regular_8);
	POSITION pos;

	for( pos = ogTanaMap.GetStartPosition(); pos != NULL; )
	{
		CString olKey;
		void *pNul;
		ogTanaMap.GetNextAssoc( pos, olKey, pNul);
		m_Tables.AddString(olKey);
	}

	m_GridName.SetWindowText(pomProperty->omName);
	v_GridName = pomProperty->omName;

	//		omMasterDetail
	CStringArray olMD;
	ExtractItemList(pomProperty->omMasterDetail, &olMD, '=');
	if(olMD.GetSize() == 2)
	{
		v_Masterreference = olMD[0];
		v_Detailreference = olMD[1];
		m_Masterreference.SetWindowText(olMD[0]);
		m_Detailreference.SetWindowText(olMD[1]);
	}
//Fields Grid
	CString olHeader;
	int ilRowCount = pomProperty->omFieldlist.GetSize();
	olHeader = "Table,Field,Type,Header,Visible,Lookup <T.F>+<T.F>..+<T.F>,Mandatory,Columsize";//LoadStg(IDS_STRING61454);
	omPropertyGrid.SubclassDlgItem(IDC_FIELD_PROPERTIES, this);
	omPropertyGrid.Initialize();
	omPropertyGrid.GetParam()->EnableUndo(FALSE);
	omPropertyGrid.LockUpdate(TRUE);
	CGXStyle olHeaderStyle;
	CStringArray olHeaderList;
	ExtractItemList(olHeader, &olHeaderList, ',');
	omPropertyGrid.SetRowCount(ilRowCount);
	imColCount = olHeaderList.GetSize();
	omPropertyGrid.SetColCount(imColCount);

	CGXComboBoxWnd *pWnd = new CGXComboBoxWnd(&omPropertyGrid);
	pWnd->Create(WS_VSCROLL | CBS_DROPDOWN | CBS_AUTOHSCROLL | CBS_SORT, 0);
	pWnd->m_bFillWithChoiceList = TRUE;
	pWnd->m_bWantArrowKeys = TRUE;
	omPropertyGrid.RegisterControl(GX_IDS_CTRL_CBS_DROPDOWN, pWnd);

	CGXCheckBox *polCeckBox = new CGXCheckBox((CGXGridCore*)(&omPropertyGrid), TRUE, GX_IDB_CHECK_95);
	omPropertyGrid.RegisterControl(GX_IDS_CTRL_CHECKBOX3D, polCeckBox);

//Table,Field,Type,Header,Visible,Key,Query
	omPropertyGrid.SetColWidth(1,1,40);
	omPropertyGrid.SetColWidth(2,2,40);
	omPropertyGrid.SetColWidth(3,3,40);
	omPropertyGrid.SetColWidth(4,4,160);
	omPropertyGrid.SetColWidth(5,5,40);
	omPropertyGrid.SetColWidth(6,6,200);
	omPropertyGrid.SetColWidth(7,7,80);
	omPropertyGrid.SetColWidth(8,8,80);

	for(int i = 0; i < olHeaderList.GetSize(); i++)
	{
		olHeaderStyle.SetValue(olHeaderList[i]);
		omPropertyGrid.SetStyleRange(CGXRange(0, i+1, 0, i+1 ), olHeaderStyle);
	}

	CString olChoiceList;
	for( i = 0; i < ogQueries.GetSize(); i++)
	{
		olChoiceList += ogQueries[i].omName + CString("\n");
	}
	for(i = 0; i < pomProperty->omFieldlist.GetSize(); i++)
	{
		omPropertyGrid.SetValueRange(CGXRange(i+1, 1), pomProperty->omGridTable);
		omPropertyGrid.SetValueRange(CGXRange(i+1, 2), pomProperty->omFieldlist[i]);
		omPropertyGrid.SetValueRange(CGXRange(i+1, 3), pomProperty->omTypelist[i]);
		omPropertyGrid.SetValueRange(CGXRange(i+1, 4), pomProperty->omHaederlist[i]);
		omPropertyGrid.SetStyleRange(CGXRange(i+1, 5), CGXStyle().SetChoiceList("")
														.SetHorizontalAlignment(DT_CENTER)
														.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
														.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
														.SetControl(GX_IDS_CTRL_CHECKBOX3D));
		CString olCheck = pomProperty->omVisiblelist[i];
		omPropertyGrid.SetValueRange(CGXRange(i+1, 5), olCheck);
		omPropertyGrid.SetValueRange(CGXRange(i+1, 6), pomProperty->omLookupfieldList[i]);
		omPropertyGrid.SetStyleRange(CGXRange(i+1, 7), CGXStyle().SetChoiceList("")
														.SetHorizontalAlignment(DT_CENTER)
														.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
														.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
														.SetControl(GX_IDS_CTRL_CHECKBOX3D));
		olCheck = pomProperty->omMandatorylist[i];
		omPropertyGrid.SetValueRange(CGXRange(i+1, 7), olCheck);
		omPropertyGrid.SetValueRange(CGXRange(i+1, 8), pomProperty->omHeadersizes[i]);
	}
	CString olTypes;
	olTypes = LoadStg(IDS_FIELD_TYPES);


	omPropertyGrid.LockUpdate(FALSE);
	omPropertyGrid.GetParam()->EnableUndo(TRUE);




	UpdateData(FALSE);


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

bool DetailGridWizard::GetValues()
{
	if(UpdateData() == FALSE)
		return false;

	if(v_GridName.IsEmpty())
	{
		MessageBox(LoadStg(IDS_STRING61452), LoadStg(IDS_STRING61453), MB_OK);
		m_GridName.SetFocus();	
		return false;
	}

	if(v_Masterreference.IsEmpty())
	{
		MessageBox(LoadStg(IDS_STRING61452), LoadStg(IDS_STRING61453), MB_OK);
		m_Masterreference.SetFocus();
		return false;
	}
	if(v_Detailreference.IsEmpty())
	{
		MessageBox(LoadStg(IDS_STRING61452), LoadStg(IDS_STRING61453), MB_OK);
		m_Detailreference.SetFocus();
		return false;
	}

	ogTools.AddFieldToDefaultSet(NULL, v_Detailreference );
	CString olUrnoString = ogTools.GetTableName(v_Detailreference)+CString(".URNO");
	ogTools.AddFieldToDefaultSet(NULL, olUrnoString);
	ogTools.AddFieldToDefaultSet(NULL, v_Masterreference );

	int ilRows = omPropertyGrid.GetRowCount();
	
	pomProperty->omName = v_GridName;
	pomProperty->omFieldlist.RemoveAll();	
	pomProperty->omTypelist.RemoveAll();
	pomProperty->omHaederlist.RemoveAll();
	pomProperty->omVisiblelist.RemoveAll();
	pomProperty->omLookupfieldList.RemoveAll();
	pomProperty->omMandatorylist.RemoveAll();
	pomProperty->omHeadersizes.RemoveAll();
	pomProperty->omMasterDetail = v_Masterreference + CString("=") + v_Detailreference;




	if(omPropertyGrid.GetRowCount() > 0)
	{
		pomProperty->omGridTable = omPropertyGrid.GetValueRowCol(1, 1);
		CString olTabField;
		int ilRealToReadRows = 0;
		for(int i = 0; i < ilRows; i++)
		{
			CString olTab, olField;
			olTab =		omPropertyGrid.GetValueRowCol(i+1, 1);
			if(olTab != pomProperty->omGridTable)
			{
				MessageBox("Only one table ollowed!", "Error", (MB_ICONEXCLAMATION|MB_OK));
				return false;
			}
			olField = omPropertyGrid.GetValueRowCol(i+1, 2);
			pomProperty->omFieldlist.Add(olField);
			pomProperty->omTypelist.Add(omPropertyGrid.GetValueRowCol(i+1, 3));
			pomProperty->omHaederlist.Add(omPropertyGrid.GetValueRowCol(i+1, 4));
			pomProperty->omVisiblelist.Add(omPropertyGrid.GetValueRowCol(i+1, 5));
			CString olLookupField;
			olLookupField = omPropertyGrid.GetValueRowCol(i+1, 6);
			pomProperty->omLookupfieldList.Add(olLookupField);
			pomProperty->omMandatorylist.Add(omPropertyGrid.GetValueRowCol(i+1, 7));
			pomProperty->omHeadersizes.Add(omPropertyGrid.GetValueRowCol(i+1, 8));
			olTabField  = olTab + CString(".") + olField;
			ogTools.AddFieldToDefaultSet(NULL, olTabField );
			ilRealToReadRows++;
 			if(!olLookupField.IsEmpty())
			{
				CStringArray olList;
				ExtractItemList(olLookupField, &olList, '+');
				for(int j = 0; j < olList.GetSize(); j++)
				{
					if(olList[j].GetLength() == 8)
					{
						ilRealToReadRows++;
						ogTools.AddFieldToDefaultSet(NULL, olList[j] );
					}
					else
					{
						MessageBox(LoadStg(IDS_STRING61458), "Error", (MB_ICONEXCLAMATION|MB_OK));
						return false;
					}
				}
			}
		}
	}
	return true;
}

void DetailGridWizard::OnMultitoright() 
{
	BOOL bOld = omPropertyGrid.LockUpdate();
	

	int ilCount = m_SourceFields.GetCount();
	CString olTable;
	m_Tables.GetWindowText(olTable);
	if(!olTable.IsEmpty())
	{
		for(int i = 0; i < ilCount; i++)
		{
			CString olField;
			m_SourceFields.GetText(i, olField);
			int ilRowNo = omPropertyGrid.GetRowCount( )+1;
			omPropertyGrid.InsertRows(omPropertyGrid.GetRowCount( )+1, 1);
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 1), olTable);
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 2), olField);
			CString olUrno = ogBCD.GetFieldExt("SYS", "TANA", "FINA", olTable, olField, "URNO");
			CString olHeader;
			CString olType;
			CString olRefe;
			CString olReqf;
			RecordSet olRec;
			ogBCD.GetRecord("SYS", "URNO", olUrno, olRec);
			olHeader = olRec[ogBCD.GetFieldIndex("SYS", "LABL")];
			olHeader.TrimLeft();olHeader.TrimRight();
			olType = olRec[ogBCD.GetFieldIndex("SYS", "TYPE")];
			olReqf = olRec[ogBCD.GetFieldIndex("SYS", "REQF")];
			if(olHeader.IsEmpty())
			{
				olHeader = olRec[ogBCD.GetFieldIndex("SYS", "ADDI")];
			}
			olRefe = olRec[ogBCD.GetFieldIndex("SYS", "REFE")];
			if(olRefe ==".")
			{
				olRefe.Empty();
			}
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 3), olType);
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 4), olHeader);
			omPropertyGrid.SetStyleRange(CGXRange(omPropertyGrid.GetRowCount( ), 5), CGXStyle().SetChoiceList("")
															.SetHorizontalAlignment(DT_CENTER)
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
															.SetControl(GX_IDS_CTRL_CHECKBOX3D));
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 5), _T("1"));
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 6), olRefe);
			omPropertyGrid.SetStyleRange(CGXRange(omPropertyGrid.GetRowCount( ), 7), CGXStyle().SetChoiceList("")
															.SetHorizontalAlignment(DT_CENTER)
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
															.SetControl(GX_IDS_CTRL_CHECKBOX3D));
			if(olReqf == "Y")
			{
				omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 7), _T("1"));
			}
			else
			{
				omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 7), _T("0"));
			}
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 8), _T("80"));

			//Check for existing table
		}
	}

	m_SourceFields.ResetContent();
	omPropertyGrid.LockUpdate(bOld);
	omPropertyGrid.Redraw();
}
