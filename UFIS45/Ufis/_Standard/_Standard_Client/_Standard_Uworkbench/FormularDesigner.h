#if !defined(AFX_FORMULARDESIGNER_H__99507043_6CE0_11D3_A1EA_0000B4984BBE__INCLUDED_)
#define AFX_FORMULARDESIGNER_H__99507043_6CE0_11D3_A1EA_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FormularDesigner.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// FormularDesigner view

class FormularDesigner : public CScrollView
{
protected:
	FormularDesigner();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(FormularDesigner)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FormularDesigner)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnInitialUpdate();     // first time after construct
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~FormularDesigner();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(FormularDesigner)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FORMULARDESIGNER_H__99507043_6CE0_11D3_A1EA_0000B4984BBE__INCLUDED_)
