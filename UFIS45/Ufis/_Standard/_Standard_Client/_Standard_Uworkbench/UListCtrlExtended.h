#if !defined(AFX_ULISTCTRLEXTENDED_H__CDDEF229_31E7_11D3_B081_00001C019205__INCLUDED_)
#define AFX_ULISTCTRLEXTENDED_H__CDDEF229_31E7_11D3_B081_00001C019205__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UListCtrlExtended.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// UListCtrlExtended window

class UListCtrlExtended : public CListCtrl
{
// Construction
protected:

	// Operation
public:
	UListCtrlExtended();
	~UListCtrlExtended();
	CImageList* SetImageList(CImageList* pImageList, int nImageListType = TVSIL_NORMAL);
	BOOL AddColumn(
		LPCTSTR strItem,int nItem,int nSubItem = -1,
		int nMask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM,
		int nFmt = LVCFMT_LEFT);
	BOOL AddItem(int nItem,int nSubItem,LPCTSTR strItem,int nImageIndex = -1);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ULISTCTRLEXTENDED_H__CDDEF229_31E7_11D3_B081_00001C019205__INCLUDED_)
