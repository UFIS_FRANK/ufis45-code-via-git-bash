#ifndef __MINIGRID_INFORMATION__
#define __MINIGRID_INFORMATION__


#include "RunTimeCtrl.h"
#include "CCSEdit.h"
#include "GxGridTab.h"
#include "UFISGridViewerDoc.h"
#include "CedaBasicData.h"
#include "FormDialogDlg.h"
#include "GxGridTab.h"


#define MINIGRID_STATUS_INDEX		0
#define MINIGRID_URNO_INDEX			1
#define MINIGRID_FOREIGN_INDEX		2
#define MINIGRID_OVERHEAD_OFFSET	3

#define STAT_UNCHANGED	"UNCHANGED"
#define STAT_CHANGED    "CHANGED"
#define STAT_NEW		"NEW"
#define STAT_DELETED    "DELETED"

class ColumnInfo : public CObject
{
public:
	ColumnInfo();
	ColumnInfo(const ColumnInfo &opO)
	{
		this->Column = opO.Column;
		this->Values.RemoveAll();
		for(int i = 0; i < opO.Values.GetSize(); i++ )
		{
			Values.Add(opO.Values[i]);
		}
	}
	const ColumnInfo &operator =(const ColumnInfo &opO) 
	{
		this->Column = opO.Column;
		this->Values.RemoveAll();
		for(int i = 0; i < opO.Values.GetSize(); i++ )
		{
			Values.Add(opO.Values[i]);
		}
		return *this;
	}
	int Column;
	CStringArray Values;
};


class MiniGridLine : public CObject
{
public:
	MiniGridLine()
	{;}
	MiniGridLine(const MiniGridLine &opO)
	{
		this->Values.RemoveAll();
		for( int i = 0; i < opO.Values.GetSize(); i++ )
			this->Values.Add(opO.Values[i]);
		this->Status = opO.Status;
		this->Urno = opO.Urno;
		this->FUrno = opO.FUrno;
		this->ChangedInfo.RemoveAll();
		for( i = 0; i < opO.ChangedInfo.GetSize(); i++ )
			this->ChangedInfo.Add(opO.ChangedInfo[i]);
	}
	const MiniGridLine &operator =(const MiniGridLine &opO) 
	{
		this->Values.RemoveAll();
		for( int i = 0; i < opO.Values.GetSize(); i++ )
			this->Values.Add(opO.Values[i]);
		this->Status = opO.Status;
		this->Urno = opO.Urno;
		this->FUrno = opO.FUrno;
		this->ChangedInfo.RemoveAll();
		for( i = 0; i < opO.ChangedInfo.GetSize(); i++ )
			this->ChangedInfo.Add(opO.ChangedInfo[i]);
		return *this;
	}
	~MiniGridLine(){;};


	CStringArray Values;	// The data

	CString Status;			// Status flag: UNCHANGED, CHANGED, NEW, DELETED

	CString Urno;			// Unique Record Number

	CString FUrno;			// Forgeign URNO as Reference to the header record

	// Info about fields, which have been changed 
	// Only interesting for updates
	CStringArray ChangedInfo; 

};

class MiniGridInformation : public CObject
{
public:
	MiniGridInformation();
	~MiniGridInformation();

	// copy constuctor
	MiniGridInformation(const MiniGridInformation &opO)
	{
		this->pomGrid = opO.pomGrid;
		this->omName = opO.omName;
		this->omTable = opO.omTable;
		this->Types.RemoveAll();
		for(int i = 0; i < opO.Types.GetSize(); i++)
			this->Types.Add(opO.Types[i]);
		this->Mandatorys.RemoveAll();
		for( i = 0; i < opO.Mandatorys.GetSize(); i++)
			this->Mandatorys.Add(opO.Mandatorys[i]);
		this->PrimaryTable = opO.PrimaryTable;
		this->PrimaryField = opO.PrimaryField;
		this->ForeignTable = opO.ForeignTable;
		this->ForeignField = opO.ForeignField;
		this->FieldList.RemoveAll();
		for( i = 0; i < opO.FieldList.GetSize(); i++)
			this->FieldList.Add(opO.FieldList[i]);
		this->RealFieldList.RemoveAll();
		for( i = 0; i < opO.RealFieldList.GetSize(); i++ )
			this->RealFieldList.Add(opO.RealFieldList[i]);
		this->omDeletedUrnos.RemoveAll();
		for( i = 0; i < opO.omDeletedUrnos.GetSize(); i++ )
			this->omDeletedUrnos.Add(opO.omDeletedUrnos[i]);
		this->omGridValues.DeleteAll();
		for( i = 0; i < opO.omGridValues.GetSize(); i++)
			this->omGridValues.NewAt(this->omGridValues.GetSize(), opO.omGridValues[i]);
		this->ForeignValue = opO.ForeignValue;
		this->omColumnInfo.DeleteAll();
		for( i = 0; i < opO.omColumnInfo.GetSize(); i++ )
		{
			ColumnInfo *polInfo = new ColumnInfo();
			omColumnInfo.Add(polInfo);
			polInfo->Column = opO.omColumnInfo[i].Column;
			for( int j = 0; j < opO.omColumnInfo[i].Values.GetSize(); j++ )
			{
				polInfo->Values.Add(opO.omColumnInfo[i].Values[j]);
			}

		}
	}
	
	// overloading =operator
	const MiniGridInformation &operator =(const MiniGridInformation &opO) 
	{
		this->pomGrid = opO.pomGrid;
		this->omName = opO.omName;
		this->omTable = opO.omTable;
		this->Types.RemoveAll();
		for(int i = 0; i < opO.Types.GetSize(); i++)
			this->Types.Add(opO.Types[i]);
		this->Mandatorys.RemoveAll();
		for( i = 0; i < opO.Mandatorys.GetSize(); i++)
			this->Mandatorys.Add(opO.Mandatorys[i]);
		this->PrimaryTable = opO.PrimaryTable;
		this->PrimaryField = opO.PrimaryField;
		this->ForeignTable = opO.ForeignTable;
		this->ForeignField = opO.ForeignField;
		this->FieldList.RemoveAll();
		for( i = 0; i < opO.FieldList.GetSize(); i++)
			this->FieldList.Add(opO.FieldList[i]);
		this->RealFieldList.RemoveAll();
		for( i = 0; i < opO.RealFieldList.GetSize(); i++ )
			this->RealFieldList.Add(opO.RealFieldList[i]);
		this->omDeletedUrnos.RemoveAll();
		for( i = 0; i < opO.omDeletedUrnos.GetSize(); i++ )
			this->omDeletedUrnos.Add(opO.omDeletedUrnos[i]);
		this->omGridValues.DeleteAll();
		for( i = 0; i < opO.omGridValues.GetSize(); i++)
			this->omGridValues.NewAt(this->omGridValues.GetSize(), opO.omGridValues[i]);
		this->ForeignValue = opO.ForeignValue;
		this->omColumnInfo.DeleteAll();
		for( i = 0; i < opO.omColumnInfo.GetSize(); i++ )
		{
			ColumnInfo *polInfo = new ColumnInfo();
			omColumnInfo.Add(polInfo);
			polInfo->Column = opO.omColumnInfo[i].Column;
			for( int j = 0; j < opO.omColumnInfo[i].Values.GetSize(); j++ )
			{
				polInfo->Values.Add(opO.omColumnInfo[i].Values[j]);
			}

		}
		return *this;
	}
	CCSPtrArray<ColumnInfo> omColumnInfo;

	// Grid pointer for easier identification
	GxGridTab *pomGrid;
	// Name of the mini grid ==> property of form
	CString omName;

	// Name of the DB-Table for the grid
	CString omTable;
	// Types for the fields, e.g. URNO,DATE,DTIM,TRIM ...
	// Without URNO and FURNO
	CStringArray Types;
	// Which field is mandatory '1'=Mand. '0'=not Mand.
	// Without URNO and FURNO
	CStringArray Mandatorys;

	//Name of Primary Key
	CString PrimaryTable, PrimaryField;
	//Name of Foreign key
	CString ForeignTable, ForeignField;
	// Forgeign URNO as Reference to the header record
	CString ForeignValue;			
	// List of database fields without primary key (URNO) and foreign key (FURNO)
	// This will be merged with PrimaryName and ForeignName before the records are stored
	CStringArray FieldList;

	// FieldList with UNRO anf FURNO
	CStringArray RealFieldList;

	//Primary keys of deleted records
	CStringArray omDeletedUrnos;

	CCSPtrArray<MiniGridLine> omGridValues;

	void MakeLines(UGridCtrlProperties *popGridProperties, 
		          GxGridTab *popGrid, 
				  CString opMasterTable,
				  CString opMasterField,
				  CString opDetailTable,
				  CString opDetailField,
				  CString opMasterValue
				  );

	int GetFieldIndex(CString opField);
	//---------------------------------------------------------------------------------------
	// Before storing the data into the database, it's neccessary to have the real fieldlist
	//---------------------------------------------------------------------------------------
	CString GetRealList();
	//---------------------------------------------------------------------------------------
	// All values are temporary stored as they appear in the grid
	// Together with the type information the values are casted to
	// an UFIS-conform string 
	// Params: popLine		=> Stored Values
	//		   ropValues	=> return param
	//		   ropStatus	=> return param NEW,CHANGED,UNCHANGED
	// Return size of the return array
	//---------------------------------------------------------------------------------------
	int GetRealValues(MiniGridLine *popLine, CStringArray &ropValues, CString &ropStatus);

	// returns the URNO of the line
	CString GetUrnoByLine(int ipLine);

	// Get the number of the line where URNO exists
	int GetLineNo(CString opUrno);

	// Inserts a new line logically
	void InsertLine(CString opUrno, CString opForeignUrno, CStringArray &opValues);

	// Updates Line data
	bool UpdateLine(CString opUrno, CString opForeignUrno, CStringArray &opValues);
	bool UpdateLine(int ipLineNo, CStringArray &opValues);

	// Delete a logical line
	bool DeleteLine(CString opUrno);
	bool DeleteLine(int ipLineNo);
	// Sets the foreign for all lines
	// this ist exspecially needed after the new insertion of the master record
	// we must set the foreign values after insertion of the master to perform
	// the insertion of the grid lines and keep konstitency
	void SetForeignValue(CString opForeignValue);

	// collects all relevant changes/updates/deletes if everything is successful
	// if not ==> return false and ropMessageList will be filled with hints what's wrong
	// and will be displayed in a messagebox by the parent window
	// ropMessageList.GetSize() > 30 the rest of the lines will be ignored 
	// ==> to avoid to large MsgBox
	bool CheckGrid(CStringArray &ropMessageList);
	// Performs the save into DB
	bool PerformSave();
};

#endif //__MINIGRID_INFORMATION__