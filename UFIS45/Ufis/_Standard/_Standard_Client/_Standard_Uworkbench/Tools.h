#ifndef __TOOLS_H__
#define __TOOLS_H__

#include "UObjectDataDefinitions.h"
#include "CtrlProperties.h"

#define WM_PROPERTY_UPDATE		(WM_USER+9500)


// Tables currently used,
// if a table will be loadd for a second time ==>
// Referencecoubnter will be incremented
// after a table is not longer needed ==> decrement of referencecounter
// if the counter is 0 ==> table will be removed form memory
struct CurrentTableCounter
{
	CString TableName;
	int		ReferenceCounter;
	void CurrentTable(void)
	{
		TableName = CString("");
		ReferenceCounter = 0;
	}
};

class Tools
{
public:
	Tools(){;}
	~Tools();

	CMapStringToPtr		omFieldMap; // List of field in format: TAB.FILD
	CMapStringToPtr		omTabMap; // List of field in format: TAB

	// Array, that contains all readed tables
	CMapStringToPtr		omReadedTables;

	//Tables currently used
	CCSPtrArray<CurrentTableCounter> omCurrentTables;
	// returns whether a table is loaded or not
	bool IsTableReaded(CString opTable);

	// Examines whether a field exists
	bool FieldExists(CString opTabField);

	// Add a new field (and table) to the default loadset
	// TabField-Format: <Table 3 Letter>.<Field>
	// If field does not exists ==> add it
	// otherwise do nothing
	void AddFieldToDefaultSet(BDLoadTable *popTab, CString opTabField);

	// Removes the specified field <Tab 3 Letter>.<Field>
	void RemoveFieldFromDefaultSet(CString opTabField);

	BDLoadTable * GetLoadTabObjFromDefaultSet(CString opTabField);

	void InitDefaultLoadSetInfo();

	UDBLoadSet *GetDefaultLoadSet();

	// Reload all the tables specified in the default load set
	void LoadDataWithDefaultSet(CListBox *popLB = NULL);

	//Reload the specified table
	void ReloadDataForTable(CString opTable, CListBox *popLB = NULL, CString opWhere = CString(""));

	// Substitution of the variable parts of a SQL-Where-clause, e.g. [TIMEFROM]
	void SubstituteWhere(CString &opWhere);

	//Parameter ==> TAB.FIELD  e.g. AFT.STOA ==> returns AFT

	CString GetTableName(CString opObject);

	//Parameter ==> TAB.FIELD  e.g. AFT.STOA ==> returns STOA
	CString GetFieldName(CString opObject);

	//1. Check for the Grid Maintable
	//2. Check for the Form and all childs of popGridDef->FormName
	// return Parameter ==> all used tables concerning the grid/form-combination
	int GetTablesForGridAndForm(UGridDefinition *popGridDef, CStringArray &ropTables);

	// blSetOrRemove == true: Sets all needed tables for grid/form combination
	// blSetOrRemove == false: Removes all not longer needed tables for grid/form combination
	void SetTablesForGridAndForm(UGridDefinition *popGridDef, bool blSetOrRemove = true/* = true => SET false = REMOVE*/);

	// blSetOrRemove == true: Sets all needed tables for formdefinition
	// blSetOrRemove == false: removes all needed tables for formdefinition
	void SetTablesForForm(UFormProperties *popFormDef, bool blSetOrRemove = true/* = true => SET false = REMOVE*/);

	// Removes all not longer needed tables for grid/form combination
	void RemoveTablesForGridAndForm(UGridDefinition *popGridDef);

	// Set a table as used
	void SetUsedTable(CString opTable, CString opWhere = CString(""));

	// removes a table
	// if this reference is the last ==> the table will be removed
	// otherwise the reference-counter will be decremented
	void RemoveUsedTable(CString opTable);

	// retrives the currnt tabel counter for the parameter opTable
	CurrentTableCounter* GetTableCounter(CString opTable);

	// returns the FormDef by Name
	UFormProperties *Tools::GetFormDefByName(CString opName);

	// Sets all DDX types for specified table (IRT,URT,DRT)
	void SetDDXTypesForTable(CString opTable);
	// Get  all DDX types for specified table (IRT,URT,DRT)
	// This enables a grid to handle broadcasts => but therefore it needs the
	// correct DDXTypes for registering at DDX for correct callback for changes
	// if one or more DDXTypes for IRT,URT,DRT are missing 
	// ==> return false
	bool GetDDXTypesForTable(CString opTable, int &ripURTType, int &ripDRTType, int &ripIRTType);
};

#endif //__TOOLS_H__