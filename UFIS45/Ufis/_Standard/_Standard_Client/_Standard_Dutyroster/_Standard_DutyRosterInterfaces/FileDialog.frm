VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Begin VB.Form FileDialog 
   BackColor       =   &H00C0C0C0&
   Caption         =   "UFIS File Manager"
   ClientHeight    =   5370
   ClientLeft      =   1935
   ClientTop       =   1905
   ClientWidth     =   9000
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FileDialog.frx":0000
   LockControls    =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5370
   ScaleWidth      =   9000
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox CollectPanel 
      Align           =   4  'Align Right
      Height          =   5085
      Left            =   4815
      ScaleHeight     =   5025
      ScaleWidth      =   2385
      TabIndex        =   10
      Top             =   0
      Visible         =   0   'False
      Width           =   2445
      Begin TABLib.TAB tabFoundFiles 
         Height          =   855
         Index           =   1
         Left            =   0
         TabIndex        =   11
         Top             =   0
         Width           =   2175
         _Version        =   65536
         _ExtentX        =   3836
         _ExtentY        =   1508
         _StockProps     =   64
      End
   End
   Begin VB.PictureBox RightPanel 
      Align           =   4  'Align Right
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      Height          =   5085
      Left            =   7260
      ScaleHeight     =   5025
      ScaleWidth      =   1680
      TabIndex        =   8
      Top             =   0
      Width           =   1740
      Begin VB.CheckBox chkCollect 
         Caption         =   "Collect"
         Height          =   285
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   17
         Tag             =   "ABOUT"
         Top             =   1320
         Width           =   855
      End
      Begin VB.CheckBox chkSearch 
         Caption         =   "Search"
         Height          =   285
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   16
         Tag             =   "SETUP"
         Top             =   1020
         Width           =   855
      End
      Begin VB.CheckBox chkClose 
         Caption         =   "Cancel"
         Height          =   285
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   15
         Tag             =   "ABOUT"
         Top             =   630
         Width           =   855
      End
      Begin VB.CheckBox chkOk 
         Caption         =   "OK"
         Height          =   285
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   14
         Tag             =   "EXIT"
         Top             =   30
         Width           =   855
      End
      Begin VB.CheckBox chkShow 
         Caption         =   "Show"
         Enabled         =   0   'False
         Height          =   285
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   13
         Tag             =   "SETUP"
         Top             =   330
         Width           =   855
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   6
      Top             =   5085
      Width           =   9000
      _ExtentX        =   15875
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1323
            MinWidth        =   1323
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13996
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox SearchPanel 
      Height          =   1005
      Left            =   0
      ScaleHeight     =   945
      ScaleWidth      =   5115
      TabIndex        =   5
      Top             =   3750
      Visible         =   0   'False
      Width           =   5175
      Begin TABLib.TAB tabFoundFiles 
         Height          =   855
         Index           =   0
         Left            =   0
         TabIndex        =   9
         Top             =   0
         Width           =   2175
         _Version        =   65536
         _ExtentX        =   3836
         _ExtentY        =   1508
         _StockProps     =   64
      End
   End
   Begin VB.PictureBox DirListPanel 
      Height          =   3645
      Left            =   0
      ScaleHeight     =   3585
      ScaleWidth      =   5115
      TabIndex        =   0
      Top             =   0
      Width           =   5175
      Begin VB.FileListBox filList 
         Height          =   1455
         Index           =   1
         Left            =   2820
         TabIndex        =   12
         Top             =   1680
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.ComboBox cboFileType 
         Height          =   315
         Left            =   30
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   3240
         Width           =   2745
      End
      Begin VB.DriveListBox drvList 
         Height          =   315
         Left            =   30
         TabIndex        =   4
         Top             =   30
         Width           =   2745
      End
      Begin VB.DirListBox dirList 
         Height          =   2790
         Left            =   30
         TabIndex        =   3
         Top             =   420
         Width           =   2745
      End
      Begin VB.FileListBox filList 
         Height          =   1260
         Index           =   0
         Left            =   2820
         MultiSelect     =   2  'Extended
         TabIndex        =   2
         Top             =   15
         Width           =   2295
      End
      Begin VB.TextBox txtFileName 
         Height          =   315
         Left            =   2835
         TabIndex        =   1
         Text            =   "*.*"
         Top             =   3240
         Width           =   2265
      End
   End
End
Attribute VB_Name = "FileDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim SearchFlag As Integer   ' Used as flag for cancel and other operations.
Dim CurFileList As Integer
Dim MyResultList As String

Public Function GetFileOpenDialog(MainPanel As Integer, UsedPath As String, TypeList As String, ListItem As Integer, ToolPanel As Integer) As String
    Dim FileTypes As String
    Dim tmpEntry As String
    Dim tmpContext As String
    Dim tmpFilter As String
    Dim tmpMulti As String
    Dim ItemNo As Long
    Dim i As Integer
    MyResultList = ""
    FileTypes = TypeList
    If FileTypes = "" Then FileTypes = "{All Files}{*.*}"
    If UsedPath <> "" Then
        drvList.Drive = UsedPath
        dirList.Path = UsedPath
    End If
    cboFileType.Clear
    ItemNo = 0
    tmpEntry = GetRealItem(FileTypes, ItemNo, "|")
    While tmpEntry <> ""
        tmpContext = Trim(GetRealItem(tmpEntry, 0, "}"))
        tmpContext = Replace(tmpContext, "{", "", 1, -1, vbBinaryCompare)
        tmpFilter = Trim(GetRealItem(tmpEntry, 1, "}"))
        tmpFilter = Replace(tmpFilter, "{", "", 1, -1, vbBinaryCompare)
        tmpMulti = Trim(GetRealItem(tmpEntry, 2, "}"))
        tmpMulti = Replace(tmpMulti, "{", "", 1, -1, vbBinaryCompare)
        tmpEntry = Trim(tmpContext) & Space(100) & "|" & Trim(tmpFilter) & "|" & Trim(tmpMulti)
        cboFileType.AddItem tmpEntry
        ItemNo = ItemNo + 1
        tmpEntry = GetRealItem(FileTypes, ItemNo, "|")
    Wend
    cboFileType.ListIndex = ListItem
'    For i = 0 To fraButtonPanel.UBound
'        fraButtonPanel(i).Visible = False
'    Next
'    If MainPanel >= 0 Then
'        fraButtonPanel(MainPanel).Top = 30
'        fraButtonPanel(MainPanel).Left = 30
'        fraButtonPanel(MainPanel).Visible = True
'    End If
'    For i = 0 To fraToolPanel.UBound
'        fraToolPanel(i).Visible = False
'    Next
'    If ToolPanel >= 0 Then
'        fraToolPanel(ToolPanel).Left = 30
'        If MainPanel >= 0 Then
'            fraToolPanel(ToolPanel).Top = fraButtonPanel(MainPanel).Top + fraButtonPanel(MainPanel).Height
'        End If
'        fraToolPanel(ToolPanel).Visible = True
'    End If
    Me.Show vbModal
    GetFileOpenDialog = MyResultList
End Function

Private Sub CloseFileDialog()
    chkSearch.Value = 0
    chkCollect.Value = 0
    chkClose.Value = 0
    chkOk.Value = 0
    Me.Hide
End Sub

Private Sub cboFileType_Click()
    Dim tmpMulti As String
    tmpMulti = GetRealItem(cboFileType.Text, 2, "|")
    If tmpMulti = "M" Then
        If CurFileList <> 0 Then
            CurFileList = 0
            filList(0).Top = filList(1).Top
            filList(0).Left = filList(1).Left
            filList(0).Height = filList(1).Height
            filList(0).Width = filList(1).Width
            filList(0).Path = filList(1).Path
            filList(0).Visible = True
            filList(1).Visible = False
            chkCollect.Value = 0
            chkCollect.Enabled = True
        End If
    Else
        If CurFileList <> 1 Then
            CurFileList = 1
            filList(1).Top = filList(0).Top
            filList(1).Left = filList(0).Left
            filList(1).Height = filList(0).Height
            filList(1).Width = filList(0).Width
            filList(1).Path = filList(0).Path
            filList(1).Visible = True
            filList(0).Visible = False
            chkCollect.Value = 0
            chkCollect.Enabled = False
        End If
    End If
    txtFileName.Text = GetRealItem(cboFileType.Text, 1, "|")
End Sub

Private Sub chkClose_Click()
    If chkClose.Value = 1 Then
        MyResultList = ""
        CloseFileDialog
    End If
End Sub

Private Sub chkCollect_Click()
    If chkCollect.Value = 1 Then
        chkCollect.BackColor = LightGreen
        tabFoundFiles(1).ResetContent
        tabFoundFiles(1).SetUniqueFields "1,2"
        CollectPanel.Visible = True
        If Me.Width < 8325 Then Me.Width = 8325
        Form_Resize
    Else
        chkCollect.BackColor = vbButtonFace
        CollectPanel.Visible = False
        tabFoundFiles(1).ResetContent
        Form_Resize
    End If
End Sub

Private Sub chkOk_Click()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim SelType As String
    Dim SelPath As String
    Dim SelFile As String
    Dim i As Integer
    If chkOk.Value = 1 Then
        MyResultList = ""
        If chkCollect.Value = 1 Then
            MaxLine = tabFoundFiles(1).GetLineCount - 1
            For CurLine = 0 To MaxLine
                SelType = tabFoundFiles(1).GetFieldValue(CurLine, "TYPE")
                SelPath = tabFoundFiles(1).GetFieldValue(CurLine, "PATH")
                SelFile = tabFoundFiles(1).GetFieldValue(CurLine, "FILE")
                MyResultList = MyResultList & SelPath & "\" & SelFile & vbLf
            Next
        Else
            SelPath = dirList.Path
            For i = 0 To filList(CurFileList).ListCount - 1
                If filList(CurFileList).Selected(i) Then
                    MyResultList = MyResultList & SelPath & "\" & filList(CurFileList).List(i) & vbLf
                End If
            Next i
        End If
        chkOk.Value = 0
        If MyResultList <> "" Then CloseFileDialog
    End If
End Sub

Private Sub chkSearch_Click()
    If chkSearch.Value = 1 Then
        chkSearch.BackColor = LightGreen
        If Me.Height < 5115 Then Me.Height = 5115
        SearchPanel.Top = -1000
        SearchPanel.Visible = True
        Form_Resize
        PerformSearch
        chkSearch.Value = 0
    Else
        chkSearch.BackColor = vbButtonFace
        'SearchPanel.Visible = False
        SearchFlag = False
        'ResetSearch
        Form_Resize
        'txtFileName.SetFocus
    End If
End Sub

Private Sub PerformSearch()
    ' Initialize for search, then perform recursive search.
    Dim FirstPath As String
    Dim DirCount As Integer
    Dim NumFiles As Integer
    Dim Result As Integer
    ' Update dirList.Path if it is different from the currently
    ' selected directory, otherwise perform the search.
    If dirList.Path <> dirList.List(dirList.ListIndex) Then
        dirList.Path = dirList.List(dirList.ListIndex)
        Exit Sub         ' Exit so user can take a look before searching.
    End If

    ResetSearch
    filList(CurFileList).Pattern = txtFileName.Text
    FirstPath = dirList.Path
    DirCount = dirList.ListCount

    ' Start recursive direcory search.
    NumFiles = 0                       ' Reset found files indicator.
    Result = DirDiver(FirstPath, DirCount, "")
    filList(CurFileList).Path = dirList.Path
    'cmdSearch.Caption = "&Reset"
    'cmdSearch.SetFocus
    tabFoundFiles(0).AutoSizeColumns
    
End Sub
Private Function DirDiver(NewPath As String, DirCount As Integer, BackUp As String) As Integer
    '  Recursively search directories from NewPath down...
    '  NewPath is searched on this recursion.
    '  BackUp is origin of this recursion.
    '  DirCount is number of subdirectories in this directory.
    Static FirstErr As Integer
    Dim DirsToPeek As Integer, AbandonSearch As Integer, ind As Integer
    Dim OldPath As String, ThePath As String, entry As String
    Dim RetVal As Integer
    SearchFlag = True           ' Set flag so the user can interrupt.
    DirDiver = False            ' Set to True if there is an error.
    RetVal = DoEvents()         ' Check for events (for instance, if the user chooses Cancel).
    If SearchFlag = False Then
        DirDiver = True
        Exit Function
    End If
    On Local Error GoTo DirDriverHandler
    DirsToPeek = dirList.ListCount                  ' How many directories below this?
    Do While DirsToPeek > 0 And SearchFlag = True
        OldPath = dirList.Path                      ' Save old path for next recursion.
        dirList.Path = NewPath
        If dirList.ListCount > 0 Then
            ' Get to the node bottom.
            dirList.Path = dirList.List(DirsToPeek - 1)
            AbandonSearch = DirDiver((dirList.Path), DirCount%, OldPath)
        End If
        ' Go up one level in directories.
        DirsToPeek = DirsToPeek - 1
        If AbandonSearch = True Then Exit Function
    Loop
    ' Call function to enumerate files.
    If filList(CurFileList).ListCount > 0 Then
        'If Len(dirList.Path) <= 3 Then             ' Check for 2 bytes/character
        '    ThePath = dirList.Path                  ' If at root level, leave as is...
        'Else
        '    ThePath = dirList.Path + "\"            ' Otherwise put "\" before the filename.
        'End If
        ThePath = dirList.Path                  ' If at root level, leave as is...
        For ind = 0 To filList(CurFileList).ListCount - 1        ' Add conforming files in this directory to the list box.
            entry = filList(CurFileList).List(ind) & "," & ThePath
            tabFoundFiles(0).InsertTextLine entry, False
        Next ind
    End If
    If BackUp <> "" Then        ' If there is a superior directory, move it.
        dirList.Path = BackUp
    End If
    Exit Function
DirDriverHandler:
    If Err = 7 Then             ' If Out of Memory error occurs, assume the list box just got full.
        DirDiver = True         ' Create Msg and set return value AbandonSearch.
        MsgBox "You've filled the list box. Abandoning search..."
        Exit Function           ' Note that the exit procedure resets Err to 0.
    Else                        ' Otherwise display error message and quit.
        MsgBox Error
        End
    End If
End Function

Private Sub chkShow_Click()
    Dim ShowFile As String
    On Error Resume Next
    If chkShow.Value = 1 Then
        chkShow.BackColor = LightGreen
        ShowFile = StatusBar1.Panels(2).Text
        If ShowFile <> "" Then
            Shell "WordPad.exe " & ShowFile
        End If
        chkShow.Value = 0
    Else
        chkShow.BackColor = vbButtonFace
    End If
End Sub

Private Sub DirList_Change()
    If dirList.ListCount > 0 Then dirList.TopIndex = dirList.TopIndex + 1
    filList(CurFileList).Path = dirList.Path
End Sub

Private Sub DirList_LostFocus()
    dirList.Path = dirList.List(dirList.ListIndex)
End Sub

Private Sub DrvList_Change()
    On Error GoTo DriveHandler
    dirList.Path = drvList.Drive
    Exit Sub

DriveHandler:
    drvList.Drive = dirList.Path
    Exit Sub
End Sub

Private Sub filList_Click(Index As Integer)
    Dim SelPath As String
    Dim SelFile As String
    Dim NewEntry As String
    SelFile = filList(Index).List(filList(Index).ListIndex)
    SelPath = dirList.Path
    NewEntry = SelPath & "\" & SelFile
    StatusBar1.Panels(2).Text = NewEntry
End Sub

Private Sub filList_DblClick(Index As Integer)
    Dim SelPath As String
    Dim SelFile As String
    Dim NewEntry As String
    SelFile = filList(Index).List(filList(Index).ListIndex)
    SelPath = dirList.Path
    If chkCollect.Value = 1 Then
        NewEntry = "," & SelFile & "," & SelPath & ","
        InsertCollectionTab NewEntry
    Else
        MyResultList = SelPath & "\" & SelFile
        CloseFileDialog
    End If
End Sub
Private Sub InsertCollectionTab(NewEntry As String)
    tabFoundFiles(1).InsertTextLine NewEntry, False
    tabFoundFiles(1).AutoSizeColumns
End Sub

Private Sub Form_Load()
    Dim i As Integer
    CurFileList = 0
    DirListPanel.Move 0, 0
    SearchPanel.Left = 0
    'Me.Height = 4335
    Me.Width = 6255
    tabFoundFiles(0).HeaderString = "Found Files,Path" & Space(1000)
    tabFoundFiles(0).LogicalFieldList = "FILE,PATH,INFO"
    tabFoundFiles(0).HeaderLengthString = "10,10,10"
    tabFoundFiles(0).ColumnWidthString = "32,128,1"
    tabFoundFiles(1).HeaderString = "FP,Collected Files               ,Path" & Space(1000)
    tabFoundFiles(1).LogicalFieldList = "TYPE,FILE,PATH,INFO"
    tabFoundFiles(1).HeaderLengthString = "10,10,10,10"
    tabFoundFiles(1).ColumnWidthString = "2,32,128,1"
    For i = 0 To 1
        tabFoundFiles(i).FontName = "MS Sans Serif"
        tabFoundFiles(i).SetTabFontBold True
        tabFoundFiles(i).AutoSizeByHeader = True
        tabFoundFiles(i).ResetContent
        tabFoundFiles(i).AutoSizeColumns
    Next
    RightPanel.Width = chkOk.Width + 120
    RightPanel.BackColor = vbButtonFace
    Me.Height = Screen.Height + 180
    'DrawBackGround RightPanel, 7, True, True
    Me.Height = 4335
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Select Case UnloadMode
        Case 0
            MyResultList = ""
            Cancel = True
            CloseFileDialog
        Case Else
            Unload Me
    End Select
End Sub

Private Sub Form_Resize()
    Dim NewWidth As Long
    Dim NewHeight As Long
    Dim LeftWidth As Long
    Dim RightWidth As Long
    NewWidth = Me.ScaleWidth - RightPanel.Width - 15
    'DrawBackGround RightPanel, 7, False, True
    If CollectPanel.Visible Then
        NewWidth = NewWidth - CollectPanel.Width
        tabFoundFiles(1).Height = CollectPanel.ScaleHeight
        tabFoundFiles(1).Width = CollectPanel.ScaleWidth
    End If
    If NewWidth > 3000 Then
        DirListPanel.Width = NewWidth
        SearchPanel.Width = NewWidth
        tabFoundFiles(0).Width = NewWidth - 60
        NewWidth = DirListPanel.ScaleWidth - (dirList.Left * 2)
        LeftWidth = (NewWidth / 2) + 300
        drvList.Width = LeftWidth
        dirList.Width = LeftWidth
        cboFileType.Width = LeftWidth
        filList(CurFileList).Left = drvList.Left + LeftWidth + 60
        txtFileName.Left = filList(CurFileList).Left + 15
        RightWidth = NewWidth - filList(CurFileList).Left + 60
        filList(CurFileList).Width = RightWidth
        txtFileName.Width = RightWidth - 30
    End If
    NewHeight = Me.ScaleHeight - StatusBar1.Height
    If SearchPanel.Visible Then
        If NewHeight >= 2325 Then
            If SearchPanel.Top < 0 Then
                If NewHeight > 2325 Then SearchPanel.Height = 2325
                SearchPanel.Top = NewHeight - SearchPanel.Height
                tabFoundFiles(0).Height = SearchPanel.Height - 60
                NewHeight = NewHeight - SearchPanel.Height - 15
            Else
                NewHeight = NewHeight - SearchPanel.Top
                If NewHeight < 1500 Then
                    NewHeight = Me.ScaleHeight - StatusBar1.Height - 1500
                    If NewHeight > 2100 Then
                        SearchPanel.Height = 1500
                        tabFoundFiles(0).Height = SearchPanel.Height - 60
                        SearchPanel.Top = NewHeight
                        NewHeight = NewHeight - 15
                    Else
                        NewHeight = 0
                    End If
                Else
                    SearchPanel.Height = NewHeight
                    tabFoundFiles(0).Height = NewHeight - 60
                    NewHeight = 0
                End If
            End If
        Else
            NewHeight = 0
        End If
    End If
    If NewHeight > 1500 Then
        DirListPanel.Height = NewHeight
        NewHeight = DirListPanel.ScaleHeight
        txtFileName.Top = NewHeight - txtFileName.Height - 30
        cboFileType.Top = txtFileName.Top
        NewHeight = txtFileName.Top - dirList.Top
        dirList.Height = NewHeight
        NewHeight = txtFileName.Top - filList(CurFileList).Top
        filList(CurFileList).Height = NewHeight
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'End
End Sub

Private Sub ResetSearch()
    tabFoundFiles(0).ResetContent
    SearchFlag = False  ' Flag indicating search in progress.
End Sub

Private Sub tabFoundFiles_GotFocus(Index As Integer)
    tabFoundFiles(Index).SetCurrentSelection tabFoundFiles(Index).GetCurrentSelected
End Sub

Private Sub tabFoundFiles_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim tmpText As String
    If (LineNo >= 0) And (Selected) Then
        tmpText = tabFoundFiles(Index).GetFieldValue(LineNo, "PATH") & "\"
        tmpText = tmpText & tabFoundFiles(Index).GetFieldValue(LineNo, "FILE")
        StatusBar1.Panels(2).Text = tmpText
    End If
End Sub

Private Sub tabFoundFiles_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpLine As String
    If LineNo < 0 Then
        Screen.MousePointer = 11
        tabFoundFiles(Index).Sort CStr(ColNo), True, True
        tabFoundFiles(Index).Refresh
        Screen.MousePointer = 0
    Else
        If Index = 0 Then
            If chkCollect.Value = 1 Then
                tmpLine = "," & tabFoundFiles(0).GetLineValues(LineNo)
                InsertCollectionTab tmpLine
                tabFoundFiles(0).DeleteLine LineNo
            Else
                MyResultList = tabFoundFiles(0).GetFieldValue(LineNo, "PATH") & "\"
                MyResultList = MyResultList & tabFoundFiles(0).GetFieldValue(LineNo, "FILE")
                CloseFileDialog
            End If
        End If
    End If
End Sub

Private Sub txtFileName_Change()
    ' Update file list box if user changes pattern.
    On Error Resume Next
    filList(CurFileList).Pattern = txtFileName.Text
End Sub

Private Sub txtFileName_GotFocus()
    txtFileName.SelStart = 0          ' Highlight the current entry.
    txtFileName.SelLength = Len(txtFileName.Text)
End Sub

