VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmDifferences 
   Caption         =   "Differences"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5985
   Icon            =   "frmDifferences.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3090
   ScaleWidth      =   5985
   StartUpPosition =   3  'Windows Default
   Begin VB.VScrollBar VScroll 
      Enabled         =   0   'False
      Height          =   1725
      Index           =   0
      Left            =   4950
      TabIndex        =   4
      Top             =   540
      Width           =   255
   End
   Begin VB.CheckBox chkMain 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Index           =   1
      Left            =   1140
      Style           =   1  'Graphical
      TabIndex        =   3
      Tag             =   " "
      Top             =   60
      Width           =   1065
   End
   Begin VB.CheckBox chkMain 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Index           =   0
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   2
      Tag             =   " "
      Top             =   60
      Width           =   1065
   End
   Begin TABLib.TAB TAB1 
      Height          =   825
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   450
      Width           =   3675
      _Version        =   65536
      _ExtentX        =   6482
      _ExtentY        =   1455
      _StockProps     =   64
   End
   Begin TABLib.TAB TAB1 
      Height          =   825
      Index           =   1
      Left            =   120
      TabIndex        =   1
      Top             =   1410
      Width           =   3675
      _Version        =   65536
      _ExtentX        =   6482
      _ExtentY        =   1455
      _StockProps     =   64
   End
End
Attribute VB_Name = "frmDifferences"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Sub ShowData(CurTab As TABLib.TAB, LineNo As Long, addFields As String)
    Dim lineTag As String
    Dim tabFields As String
    Dim newFields As String
    Dim newData As String
    Dim tmpCmds As String
    Dim ForeColor As Long
    Dim BackColor As Long
    
    'addFields = "PENO,SDAY"
    
    TAB1(0).ResetContent
    TAB1(0).HeaderString = " "
    TAB1(0).LogicalFieldList = "XXXX"
    TAB1(0).HeaderLengthString = "1000"
    InsertEmptyTabLine TAB1(0), -1
    InsertEmptyTabLine TAB1(0), -1
    TAB1(0).SetLineColor 0, vbWhite, vbBlue
    CurTab.GetLineColor LineNo, ForeColor, BackColor
    TAB1(0).SetLineColor 1, ForeColor, BackColor
    TAB1(0).AutoSizeByHeader = False
    
    tmpCmds = CurTab.GetFieldValue(LineNo, "CMDS")
    lineTag = CurTab.GetLineTag(LineNo)
    If (lineTag <> "") Or (tmpCmds = "I") Then
        GetKeyItem newFields, lineTag, "{=FIELDS=}", "{="
        If newFields <> "" Then
            tabFields = addFields & "," & newFields
        Else
            newFields = newDrrTabFields
            tabFields = newFields
        End If
        TAB1(0).HeaderString = tabFields
        TAB1(0).LogicalFieldList = tabFields
        If tmpCmds <> "I" Then
            newData = CurTab.GetFieldValues(LineNo, tabFields)
            TAB1(0).SetFieldValues 0, tabFields, newData
            GetKeyItem newData, lineTag, "{=DATA=}", "{="
            TAB1(0).SetFieldValues 1, newFields, newData
        Else
            newData = CurTab.GetFieldValues(LineNo, newFields)
            TAB1(0).SetFieldValues 1, newFields, newData
        End If
        SetTabCellObjects TAB1(0), 0, "DECO", "CellDiffGry", newFields, False
        SetTabCellObjects TAB1(0), 1, "DECO", "CellDiffGry", newFields, False
        TAB1(0).AutoSizeByHeader = True
    End If
    TAB1(0).AutoSizeColumns
    TAB1(0).Refresh

End Sub
Private Sub Form_Load()
    Dim i As Integer
    Dim newSize As Long
    Dim tmpColorGry As String
    Dim tmpColorYel As String
    Dim tmpColorRed As String
    
    Me.Left = frmMainDialog.Left
    Me.Width = frmMainDialog.Width
    Me.Top = frmMainDialog.Top
    Me.height = 2010

    tmpColorGry = CStr(vbBlack) & "," & CStr(LightGray)
    tmpColorYel = CStr(vbBlack) & "," & CStr(vbYellow)
    tmpColorRed = CStr(vbBlack) & "," & CStr(LightestRed)

    For i = 0 To TAB1.UBound
        TAB1(i).Left = 60
        TAB1(i).ResetContent
        TAB1(i).FontName = "Courier New"
        TAB1(i).HeaderFontSize = 17
        TAB1(i).FontSize = 17
        TAB1(i).LineHeight = 17
        TAB1(i).LeftTextOffset = 0
        TAB1(i).SetTabFontBold True
        TAB1(i).HeaderString = " "
        TAB1(i).HeaderLengthString = "1000"
        TAB1(i).LifeStyle = True
        'TAB1(i).CursorLifeStyle = True
        'TAB1(i).GridLineColor = vbBlack
        TAB1(i).CreateCellObj "Marker", vbBlue, vbWhite, 17, False, False, True, 0, "Courier New"
        TAB1(i).CreateDecorationObject "CellDiffGry", "R,T,B,L", "2,2,2,2", tmpColorGry & "," & tmpColorGry
        TAB1(i).CreateDecorationObject "CellDiffYel", "R,T,B,L", "2,2,2,2", tmpColorYel & "," & tmpColorYel
        TAB1(i).CreateDecorationObject "CellDiffRed", "R,T,B,L", "2,2,2,2", tmpColorRed & "," & tmpColorRed
    Next
    TAB1(0).Top = chkMain(0).Top + chkMain(0).height + 60
    TAB1(0).height = ((TAB1(0).LineHeight * 15) * 3) + 240
    TAB1(0).ShowHorzScroller True
    TAB1(0).ShowVertScroller False
    TAB1(1).Top = TAB1(0).Top + TAB1(0).height + 90
    TAB1(1).Visible = False
    
    VScroll(0).Top = TAB1(0).Top
    VScroll(0).height = TAB1(0).height

End Sub

Private Sub Form_Resize()
    Dim newSize As Long
    newSize = Me.ScaleWidth - VScroll(0).Width - TAB1(0).Left - 60
    VScroll(0).Left = newSize + 60
    If newSize > 300 Then
        TAB1(0).Width = newSize
        TAB1(1).Width = newSize
    End If
    newSize = Me.ScaleHeight - TAB1(1).Top - 60
    If newSize > 300 Then
        TAB1(1).height = newSize
    End If
End Sub
