VERSION 5.00
Object = "{A45D986F-3AAF-4A3B-A003-A6C53E8715A2}#1.0#0"; "Arview2.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmPreview 
   Caption         =   "Set caption"
   ClientHeight    =   7380
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12960
   Icon            =   "frmPreview.frx":0000
   LinkTopic       =   "Form2"
   ScaleHeight     =   7380
   ScaleWidth      =   12960
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdExport 
      Caption         =   "&HTML-Export"
      Height          =   420
      Index           =   3
      Left            =   6405
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   105
      Width           =   1995
   End
   Begin VB.CommandButton cmdExport 
      Caption         =   "&TXT-Export"
      Height          =   420
      Index           =   2
      Left            =   4310
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   105
      Width           =   1995
   End
   Begin VB.CommandButton cmdExport 
      Caption         =   "&PDF-Export"
      Height          =   420
      Index           =   1
      Left            =   2215
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   105
      Width           =   1995
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   10050
      Top             =   120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdExport 
      Caption         =   "&Excel-Export"
      Height          =   420
      Index           =   0
      Left            =   120
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   105
      Width           =   1995
   End
   Begin DDActiveReportsViewer2Ctl.ARViewer2 ARViewer 
      Height          =   6000
      Left            =   240
      TabIndex        =   0
      Top             =   690
      Width           =   12480
      _ExtentX        =   22013
      _ExtentY        =   10583
      SectionData     =   "frmPreview.frx":030A
   End
End
Attribute VB_Name = "frmPreview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Example for using this form:
'----------------------------
'Private Sub btn_CompensationReport_Click()
'    Dim rpt As New rptCompensation
'    rpt.CurrentFile = omFile
'    frmPreview.ARViewer.ReportSource = rpt
'    frmPreview.Caption = "Compensation report"
'    frmPreview.Show
'End Sub
'
'... and you have to add the references of the DLLs to your project
' (Project - References - ActiveReports xxxx Export Filter)

Dim oEXL As ActiveReportsExcelExport.ARExportExcel
Dim oPDF As ActiveReportsPDFExport.ARExportPDF
Dim oTXT As ActiveReportsTextExport.ARExportText
Dim oHTML As ActiveReportsHTMLExport.HTMLexport
Dim bmInitialSized As Boolean

Private Sub cmdExport_Click(Index As Integer)
    Dim strPath As String
    CommonDialog1.DialogTitle = "Save as..."
    On Error Resume Next

    ' setting the file filter
    If Index = 0 Then
        CommonDialog1.Filter = "Excel-files (*.xls)|*.xls|All files (*.*)|*.*"
    ElseIf Index = 1 Then
        CommonDialog1.Filter = "PDF-files (*.pdf)|*.pdf|All files (*.*)|*.*"
    ElseIf Index = 2 Then
        CommonDialog1.Filter = "Text-files (*.txt)|*.txt|All files (*.*)|*.*"
    Else
        CommonDialog1.Filter = "HTML-files (*.html)|*.html|All files (*.*)|*.*"
    End If

    ' show the dialog
    CommonDialog1.ShowSave
    If CommonDialog1.CancelError = False Then
        strPath = CommonDialog1.filename
        ' do the export
        If Index = 0 Then
            Set oEXL = New ActiveReportsExcelExport.ARExportExcel
            oEXL.filename = strPath
            'ARViewer.Pages
            oEXL.Export ARViewer.ReportSource.Pages
        ElseIf Index = 1 Then
            Set oPDF = New ActiveReportsPDFExport.ARExportPDF
            oPDF.filename = strPath
            oPDF.AcrobatVersion = 2
            oPDF.SemiDelimitedNeverEmbedFonts = ""
            oPDF.Export ARViewer.ReportSource.Pages
        ElseIf Index = 2 Then
            Set oTXT = New ActiveReportsTextExport.ARExportText
            oTXT.filename = strPath
            oTXT.PageDelimiter = ";"
            oTXT.TextDelimiter = ","
            oTXT.SuppressEmptyLines = True
            oTXT.Export ARViewer.ReportSource.Pages
        Else
            Set oHTML = New ActiveReportsHTMLExport.HTMLexport
            oHTML.filename = strPath
            oHTML.Export ARViewer.ReportSource.Pages
        End If
    
    
    End If
End Sub

Private Sub Form_Load()
    bmInitialSized = False 'because of Matrox card
    ARViewer.Zoom = 80
End Sub

Private Sub Form_Resize()
    If bmInitialSized = False Then
        bmInitialSized = True
        Me.height = Screen.TwipsPerPixelY * 768
        Me.Width = Screen.TwipsPerPixelX * 1024
    End If

    If Me.WindowState <> vbMinimized Then
        ARViewer.Left = 0
        If Me.height > 500 Then
            ARViewer.height = Me.height - 500 'because of the top buttons
        End If
        If Me.Width > 150 Then
            ARViewer.Width = Me.Width - 150 'because of the scroll bar
        End If
    End If
    'Me.Top = 0
    'Me.Left = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'End
End Sub
