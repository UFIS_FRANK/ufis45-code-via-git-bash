// StaffBalanceDlg.h : header file
//

#if !defined(AFX_TESTAPPLDLG_H__EA4946E7_1ACF_11D4_8FA9_00010204A53B__INCLUDED_)
#define AFX_TESTAPPLDLG_H__EA4946E7_1ACF_11D4_8FA9_00010204A53B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// number of colmuns in grid
#define ACCOUNT_COLCOUNT 14
#define ACCOUNT_ROWCOUNT 30


#include <GridControl.h>
#include <CedaAccData.h>
#include <CedaTdaData.h>
#include <gxwnd.h>


class CGXPrintSupportDialog : public CDialog
{
// Construction
public:
	CGXPrintSupportDialog(LPCTSTR lpszTemplateName, CWnd* pParentWnd = NULL);
	CGXPrintSupportDialog(UINT nIDTemplate, CWnd* pParentWnd = NULL);
	~CGXPrintSupportDialog();

// Attributes
public:
	BOOL    m_bOwnPD;
	CGXPrintDevice*
			m_pPD;

	// Printer Settings
public:
	CGXPrintDevice* GetPrintDevice();
	void SetPrintDevice(CGXPrintDevice* pPD, BOOL bMustDelete = TRUE);
	virtual void OnModifiedPrintDevice();

// Printing
protected:
	virtual void OnPrint(CDC* pDC, CPrintInfo*);
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo);
	virtual BOOL OnPreparePrinting(CPrintInfo*);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo*);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo*);
	virtual BOOL DoPreparePrinting(CPrintInfo* pInfo);
	virtual void OnFilePrint();
	void UpdatePrinterDevice(CGXPrintDevice* pd);
};

/////////////////////////////////////////////////////////////////////////////
// CStaffBalanceDlg dialog
class CStaffBalanceDlg : public CGXPrintSupportDialog
{
// Construction
public:
	CStaffBalanceDlg(CWnd* pParent = NULL);	// standard constructor
	~CStaffBalanceDlg();
	
// Dialog Data
	//{{AFX_DATA(CStaffBalanceDlg)
	enum { IDD = IDD_STAFFBALANCE_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStaffBalanceDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	int GetDaysOfMonth(COleDateTime opDay);
	void CalcAccounts(CString opType, int ipRow);
	void CalcSaldos(int ipRow, CString opType1, CString opType2 = "");
	void FillGrid();
	CGridControl* pomAccount;
	void IniGrid();
	HICON m_hIcon;
	
	void Register();

	//CMapStringToPtr omRow;

	// Generated message map functions
	//{{AFX_MSG(CStaffBalanceDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonPrint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	CString m_RefUrno;
	CString m_Name;
	COleDateTime m_Date;
	long m_Urno;

	// Hier gehen alle BC f�r diese Anwendung rein
	LONG OnBcAdd(UINT /*wParam*/, LONG /*lParam*/); 
	
	void ProcessTdaNew(TDADATA *prpTda);
	void ProcessTdaDelete(TDADATA *prpTda);
	void ProcessTdaChange(TDADATA *prpTda);

	virtual void OnPrint(CDC* pDC, CPrintInfo*);
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo, long nZoom);
	virtual BOOL OnPreparePrinting(CPrintInfo*);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo*);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo*);

	afx_msg void OnHelp();

};
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTAPPLDLG_H__EA4946E7_1ACF_11D4_8FA9_00010204A53B__INCLUDED_)
