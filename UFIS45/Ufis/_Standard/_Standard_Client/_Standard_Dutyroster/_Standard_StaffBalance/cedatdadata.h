#ifndef _CedaTdaData_H_
#define _CedaTdaData_H_
 
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccsglobl.h>
//#include "CedaTdaData.h" // f�r G�ltigkeitspr�fungen

/////////////////////////////////////////////////////////////////////////////

// COMMANDS FOR MEMBER-FUNCTIONS
#define TDA_SEND_DDX	(true)
#define TDA_NO_SEND_DDX	(false)

struct TDADATA 
{
	CTime 	 Cdat; 			// Erstellungsdatum
	char	 Desr[10+2];	// Description
	char     Idur[10+2];			// Reference Urno
	CTime 	 Lstu;  		// Datum letzte �nderung
	long 	 Urno; 			// Eindeutige Datensatz-Nr.
	char 	 Usec[32+2]; 	// Anwender (Ersteller)
	char 	 Useu[32+2]; 	// Anwender (letzte �nderung)
	char     Valu[10+2];	// Value

	//DataCreated by this class
	int      IsChanged;

	TDADATA(void)
	{
		memset(this,'\0',sizeof(*this));
		Urno		=   0;
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Lstu		=	TIMENULL;
	}

}; // end TDADataStruct

// the broadcast CallBack function, has to be outside the CedaTdaData class
void ProcessTdaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//************************************************************************************************************************
//************************************************************************************************************************
// Deklaration CedaTdaData: kapselt den Zugriff auf die Tabelle Tda (Daily Roster
//	Absences - untert�gige Abwesenheit)
//************************************************************************************************************************
//************************************************************************************************************************

class CedaTdaData: public CCSCedaData
{
// Funktionen
public:
	bool ReadTdaData();
    // Konstruktor/Destruktor
	CedaTdaData(CString opTableName = "Tda", CString opExtName = "TAB");
	~CedaTdaData();

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

	// die geladenen Datens�tze
	CCSPtrArray<TDADATA> omData;

	// allgemeine Funktionen
	// Feldliste lesen
	CString GetFieldList(void) {return CString(pcmListOfFields);}

	// Broadcasts empfangen und bearbeiten
	// behandelt die Broadcasts BC_TDA_CHANGE,BC_TDA_DELETE und BC_TDA_NEW
	void ProcessTdaBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Datens�tze einlesen
	bool Read(char *pspWhere = NULL, CMapPtrToPtr *popLoadStfUrnoMap = NULL,
			  bool bpRegisterBC = true, bool bpClearData = true);
	// Datensatz mit bestimmter Urno einlesen
	bool ReadTdaByUrno(long lpUrno, TDADATA *prpTda);
	
	// einen Datensatz aus der Datenbank l�schen
	bool Delete(TDADATA *prpTda, bool bpWithSave = true);
	// einen neuen Datensatz in der Datenbank speichern
	bool Insert(TDADATA *prpTda, bool bpSave = true);
	// einen ge�nderten Datensatz speichern
	bool Update(TDADATA *prpTda, bool bpSave = true);

	
	// Datens�tze suchen
	// Tda nach Urno suchen
	TDADATA* GetTdaByUrno(long lpUrno);

	// kopiert die Feldwerte eines Tda-Datensatzes ohne die Schl�sselfelder (Urno, etc.)
	void CopyTda(TDADATA* popTdaDataSource,TDADATA* popTdaDataTarget);

protected:	
	// Funktionen
	// allgemeine Funktionen
	// Tabellenname einstellen
	bool SetTableNameAndExt(CString opTableAndExtName);
	// Feldliste schreiben
	void SetFieldList(CString opFieldList) {strcpy(pcmListOfFields,opFieldList.GetBuffer(0));}
	// alle internen Arrays zur Datenhaltung leeren und beim BC-Handler abmelden
	bool ClearAll(bool bpUnregister = true);

	// Broadcasts empfangen und bearbeiten
	// beim Broadcast-Handler anmelden
	void Register(void);

	// Kommunikation mit CEDA
	// Kommandos an CEDA senden (die Funktionen der Basisklasse werden �berschrieben
	// wegen des Feldlisten Bugs)
	bool CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
					char *pcpSelection, char *pcpSort, char *pcpData, 
					char *pcpDest = "BUF1",bool bpIsWriteAction = false, 
					long lpID = 0, CCSPtrArray<RecordSet> *pomData = NULL, 
					int ipFieldCount = 0);
    bool CedaAction(char *pcpAction, char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf, char *pcpDest = "BUF1", 
					bool bpIsWriteAction = false, long lpID = 0);
	
	// Datens�tze bearbeiten/speichern
	// speichert einen einzelnen Datensatz
	bool Save(TDADATA *prpTda);
	// einen Broadcast TDA_NEW abschicken und den Datensatz in die interne Datenhaltung aufnehmen
	bool InsertInternal(TDADATA *prpTda, bool bpSendDdx);
	// einen Broadcast TDA_CHANGE versenden
	bool UpdateInternal(TDADATA *prpTda, bool bpSendDdx = true);
	// einen Datensatz aus der internen Datenhaltung l�schen und einen Broadcast senden
	void DeleteInternal(TDADATA *prpTda, bool bpSendDdx = true);

	// Daten
    // Map mit den Datensatz-Urnos der geladenen Datens�tze
	CMapPtrToPtr omUrnoMap;
	// speichert die Feldnamen der Tabelle in sequentieller Form mit Trennzeichen Komma
	char pcmListOfFields[1024];
	// beim Broadcast-Handler angemeldet?
	bool bmIsBCRegistered;

	// ToDo: Minimale und maximales G�ltigkeitsdatum einbauen, damit nicht
	// unn�tig viele Datens�tze intern gehalten werden. Die min.-max.-Daten
	// m�ssen bei jeder Leseaktion (ReadXXX) gepr�ft und wenn n�tig erweitert
	// werden. Wenn dann BCs einlaufen (REL,NEW), kann gepr�ft werden ob
	// die Datens�tze in den BCs �berhaupt relevant sind.

	// min. und max. Tag der Ansichts-relevanten Tdas
	//	COleDateTime omMinDay;
	//	COleDateTime omMaxDay;
};

#endif	// _CedaTdaData_H_
