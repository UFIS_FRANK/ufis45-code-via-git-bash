// StaffBalanceDlg.cpp : implementation file
//

#include <stdafx.h>
#include <StaffBalance.h>
#include <StaffBalanceDlg.h>
#include <CCSParam.h>
#include <CCSGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifndef _GXWND_H_
#include <gxwnd.h>
#endif

#ifndef _GXCTRL_H_
#include <gxctrl.h>
#endif

/////////////////////////////////////////////////////////////////////////////
// Printing from dialogs

#include <afxpriv.h>

// Local function prototype
static void ProcessBC(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//**************************************************************************************
// Proce.s.sBC: globale Funktion als Callback f�r den Broadcast-Handler
//**************************************************************************************

static void ProcessBC(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CStaffBalanceDlg *polStaffBal = (CStaffBalanceDlg *)popInstance;

	if(ipDDXType == TDA_NEW)          polStaffBal->ProcessTdaNew((TDADATA*)vpDataPointer);
	if(ipDDXType == TDA_CHANGE)       polStaffBal->ProcessTdaChange((TDADATA*)vpDataPointer);
	if(ipDDXType == TDA_DELETE)       polStaffBal->ProcessTdaDelete((TDADATA*)vpDataPointer);
}

// I need access to protected members
class CGXWinAppEx: public CWinApp
{
	// I need access to the protected members
	// m_hDevMode and m_hDevNames of CWinApp
	friend class CGXPrintSupportDialog;
};

/////////////////////////////////////////////////////////////////////////////
// Printing Dialog (displays the print status at the screen)

class CGXPrintingDialog : public CDialog
{
public:
	//{{AFX_DATA(CGXPrintingDialog)
	enum { IDD = AFX_IDD_PRINTDLG };
	//}}AFX_DATA
	CGXPrintingDialog::CGXPrintingDialog(CWnd* pParent)
		{
#ifdef _MAC
			// Note! set m_pView *before* CDialog::Create so that
			// CGXPrintingDialog::OnInitDialog can use it.
			m_pView = pParent;
#endif
			Create(CGXPrintingDialog::IDD, pParent);      // modeless !
			// _afxWinState->m_bUserAbort = FALSE;
		}
	virtual ~CGXPrintingDialog() { }

	//virtual BOOL OnInitDialog();
	//virtual void OnCancel();

protected:
#ifdef _MAC
	CWnd*   m_pView;        // the view being printed

	afx_msg int OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message);
#endif

#ifdef _MAC
	//{{AFX_MSG(CGXPrintingDialog)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
#endif
};

CGXPrintSupportDialog::CGXPrintSupportDialog(LPCTSTR lpszTemplateName, CWnd* pParentWnd)
	: CDialog(lpszTemplateName, pParentWnd)
{
	m_bOwnPD = FALSE;
	m_pPD = NULL;
}

CGXPrintSupportDialog::CGXPrintSupportDialog(UINT nIDTemplate, CWnd* pParentWnd)
	: CDialog(nIDTemplate, pParentWnd)
{
	m_bOwnPD = FALSE;
	m_pPD = NULL;
}

CGXPrintSupportDialog::~CGXPrintSupportDialog()
{
	if (m_bOwnPD)
		delete m_pPD;
}
 
/////////////////////////////////////////////////////////////////////////////
// Printing support virtual functions (others in viewpr.cpp)

void CGXPrintSupportDialog::OnPrint(CDC* pDC, CPrintInfo* pInfo)
{
	// Unused:
	pDC, pInfo;
}

void CGXPrintSupportDialog::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo)
{
	ASSERT_VALID(pDC);

	// Unused:
	pDC, pInfo;
	
	// Default to one page printing if doc length not known
	if (pInfo != NULL){
		
		/*CRect olPrintRect;
		GetClientRect(olPrintRect);
		pDC->SetMapMode(MM_ANISOTROPIC);
		pDC->SetWindowExt(400, 450);
		pDC->SetViewportExt(olPrintRect.right, olPrintRect.bottom);
		pDC->SetViewportOrg(0, 0);*/

		pInfo->m_bContinuePrinting =
			(pInfo->GetMaxPage() != 0xffff || (pInfo->m_nCurPage == 1));
	}
}

BOOL CGXPrintSupportDialog::OnPreparePrinting(CPrintInfo* pInfo)
{
	// Unused:
	pInfo;

	// Do print DC initialization here
	// override and call DoPreparePrinting

	return TRUE;
}

void CGXPrintSupportDialog::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// Unused:
	pDC, pInfo;

}

void CGXPrintSupportDialog::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// Unused:
	pDC, pInfo;
}

/////////////////////////////////////////////////////////////////////////////
// Printer Settings Object

CGXPrintDevice* CGXPrintSupportDialog::GetPrintDevice()
{
	return m_pPD;
}

void CGXPrintSupportDialog::SetPrintDevice(CGXPrintDevice* pPD, BOOL bMustDelete)
{
	ASSERT_VALID(pPD);
	ASSERT(pPD->IsKindOf(RUNTIME_CLASS(CGXPrintDevice)));

	if (m_pPD && m_bOwnPD)
		delete m_pPD;

	m_pPD = pPD;
	m_bOwnPD = bMustDelete;
}

void CGXPrintSupportDialog::OnModifiedPrintDevice()
{
	// override this method and call GetDocument()->SetModifiedFlag()
	// if you maintain the printer settings in the document
}

/////////////////////////////////////////////////////////////////////////////
// CView printing commands

void CGXPrintSupportDialog::UpdatePrinterDevice(CGXPrintDevice* pd)
{
	CGXWinAppEx* pApp = (CGXWinAppEx*) AfxGetApp();

	// TRACE0("UpdatePrinterDevice()\n");
	ASSERT(pd);

	// It is a known problem that BoundsChecker eventually
	// complains about freeing un unlocked handle at the
	// next line.
	//
	// The reason for this complaint is that the
	// CWinApp::UpdatePrinterSelection (VC 4.0 or eralier,
	// called from CWinApp::DoPrintDialog) forgets to unlock
	// pApp->m_hDevNames. As this has no further side effects
	// you need not worry about this complaint.
	if (pApp->m_hDevNames)
		::GlobalFree(pApp->m_hDevNames);
	pApp->m_hDevNames = 0;

	if (pApp->m_hDevMode)
		::GlobalFree(pApp->m_hDevMode);
	pApp->m_hDevMode = 0;

	pd->CopyDeviceHandles(pApp->m_hDevNames, pApp->m_hDevMode);
}

BOOL CGXPrintSupportDialog::DoPreparePrinting(CPrintInfo* pInfo)
{
	ASSERT(pInfo != NULL);
	ASSERT(pInfo->m_pPD != NULL);

	CGXPrintDevice* pDevice = GetPrintDevice();

	CGXWaitCursor theWait;

	if (pDevice)
	{
		pDevice->NeedDeviceHandles();
		UpdatePrinterDevice(pDevice);
	}

	CWinApp* pApp = AfxGetApp();
	if (pInfo->m_bPreview || pInfo->m_bDirect)
	{
		if (pInfo->m_pPD->m_pd.hDC == NULL)
		{
			// if no printer set then, get default printer DC and create DC without calling
			//   print dialog.
			if (!pApp->GetPrinterDeviceDefaults(&pInfo->m_pPD->m_pd))
			{
					// bring up dialog to alert the user they need to install a printer.
				if (pApp->DoPrintDialog(pInfo->m_pPD) != IDOK)
					return FALSE;
			}

			if (pInfo->m_pPD->m_pd.hDC == NULL)
			{
				// call CreatePrinterDC if DC was not created by above
				if (pInfo->m_pPD->CreatePrinterDC() == NULL)
					return FALSE;
			}
		}

		// set up From and To page range from Min and Max
		pInfo->m_pPD->m_pd.nFromPage = (WORD)pInfo->GetMinPage();
		pInfo->m_pPD->m_pd.nToPage = (WORD)pInfo->GetMaxPage();
	}
	else
	{
		// otherwise, bring up the print dialog and allow user to change things

		// preset From-To range same as Min-Max range
		pInfo->m_pPD->m_pd.nFromPage = (WORD)pInfo->GetMinPage();
		pInfo->m_pPD->m_pd.nToPage = (WORD)pInfo->GetMaxPage();

		if (pApp->DoPrintDialog(pInfo->m_pPD) != IDOK)
			return FALSE;	// do not print*/
	}

	ASSERT(pInfo->m_pPD != NULL);
	ASSERT(pInfo->m_pPD->m_pd.hDC != NULL);

	pInfo->m_nNumPreviewPages = pApp->m_nNumPreviewPages;
	//VERIFY(pInfo->m_strPageDesc.LoadString(AFX_IDS_PREVIEWPAGEDESC));

	if (pDevice && !pDevice->CompareDeviceHandles(pInfo->m_pPD->m_pd.hDevNames, pInfo->m_pPD->m_pd.hDevMode))
	{
		pDevice->CreateDeviceHandles(pInfo->m_pPD->m_pd.hDevNames, pInfo->m_pPD->m_pd.hDevMode);
		OnModifiedPrintDevice();
	}

	return TRUE;
}

void CGXPrintSupportDialog::OnFilePrint()
{
	// get default print info
	CPrintInfo printInfo;
	ASSERT(printInfo.m_pPD != NULL);    // must be set

	if (GetCurrentMessage()->wParam == ID_FILE_PRINT_DIRECT)
	{
		CCommandLineInfo* pCmdInfo = AfxGetApp()->m_pCmdInfo;

		if (pCmdInfo != NULL)
		{
			if (pCmdInfo->m_nShellCommand == CCommandLineInfo::FilePrintTo)
			{
				printInfo.m_pPD->m_pd.hDC = ::CreateDC(pCmdInfo->m_strDriverName,
					pCmdInfo->m_strPrinterName, pCmdInfo->m_strPortName, NULL);
				if (printInfo.m_pPD->m_pd.hDC == NULL)
				{
					AfxMessageBox(AFX_IDP_FAILED_TO_START_PRINT);
					return;
				}
			}
		}

		printInfo.m_bDirect = TRUE;
	}

	if (OnPreparePrinting(&printInfo))
	{
		// hDC must be set (did you remember to call DoPreparePrinting?)
		ASSERT(printInfo.m_pPD->m_pd.hDC != NULL);

		// gather file to print to if print-to-file selected
		CString strOutput;
		if (printInfo.m_pPD->m_pd.Flags & PD_PRINTTOFILE)
		{
			// construct CFileDialog for browsing
			CString strDef(MAKEINTRESOURCE(AFX_IDS_PRINTDEFAULTEXT));
			CString strPrintDef(MAKEINTRESOURCE(AFX_IDS_PRINTDEFAULT));
			CString strFilter(MAKEINTRESOURCE(AFX_IDS_PRINTFILTER));
			CString strCaption(MAKEINTRESOURCE(AFX_IDS_PRINTCAPTION));
			CFileDialog dlg(FALSE, strDef, strPrintDef,
				OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strFilter);
			dlg.m_ofn.lpstrTitle = strCaption;

			if (dlg.DoModal() != IDOK)
				return;

			// set output device to resulting path name
			strOutput = dlg.GetPathName();
		}

		// set up document info and start the document printing process
		CString strTitle = LoadStg(IDS_STAFFBALANCE);
		// SH: commented out the following lines
		// CDocument* pDoc = GetDocument();
		// if (pDoc != NULL)
		//	strTitle = pDoc->GetTitle();
		// else
		/*	GetParentFrame()->GetWindowText(strTitle);*/
		if (strTitle.GetLength() > 31)
			strTitle.ReleaseBuffer(31);
		DOCINFO docInfo;
		memset(&docInfo, 0, sizeof(DOCINFO));
		docInfo.cbSize = sizeof(DOCINFO);
		docInfo.lpszDocName = strTitle;
		CString strPortName;
		int nFormatID;
		if (strOutput.IsEmpty())
		{
			docInfo.lpszOutput = NULL;
			strPortName = printInfo.m_pPD->GetPortName();
			nFormatID = AFX_IDS_PRINTONPORT;
		}
		else
		{
			docInfo.lpszOutput = strOutput;
			GetFileTitle(strOutput,
				strPortName.GetBuffer(_MAX_PATH), _MAX_PATH);
			nFormatID = AFX_IDS_PRINTTOFILE;
		}

		// setup the printing DC
		CDC dcPrint;
		dcPrint.Attach(printInfo.m_pPD->m_pd.hDC);  // attach printer dc
		dcPrint.m_bPrinting = TRUE;
		OnBeginPrinting(&dcPrint, &printInfo);
		// SH: commented out the following line
		// dcPrint.SetAbortProc(_GXAbortProc);

		// disable main window while printing & init printing status dialog
		AfxGetMainWnd()->EnableWindow(FALSE);
		/*CGXPrintingDialog dlgPrintStatus(this);

		CString strTemp;
		dlgPrintStatus.SetDlgItemText(AFX_IDC_PRINT_DOCNAME, strTitle);
#ifndef _MAC
		dlgPrintStatus.SetDlgItemText(AFX_IDC_PRINT_PRINTERNAME,
			printInfo.m_pPD->GetDeviceName());
		AfxFormatString1(strTemp, nFormatID, strPortName);
		dlgPrintStatus.SetDlgItemText(AFX_IDC_PRINT_PORTNAME, strTemp);
#endif
		dlgPrintStatus.ShowWindow(SW_SHOW);
		dlgPrintStatus.UpdateWindow();*/

		// start document printing process
		if (dcPrint.StartDoc(&docInfo) == SP_ERROR)
		{
			// enable main window before proceeding
			AfxGetMainWnd()->EnableWindow(TRUE);

			// cleanup and show error message
			OnEndPrinting(&dcPrint, &printInfo);
			//dlgPrintStatus.DestroyWindow();
			dcPrint.Detach();   // will be cleaned up by CPrintInfo destructor
			AfxMessageBox(AFX_IDP_FAILED_TO_START_PRINT);
			return;
		}

		// Guarantee values are in the valid range
		UINT nEndPage = printInfo.GetToPage();
		UINT nStartPage = printInfo.GetFromPage();

		if (nEndPage < printInfo.GetMinPage())
			nEndPage = printInfo.GetMinPage();
		if (nEndPage > printInfo.GetMaxPage())
			nEndPage = printInfo.GetMaxPage();

		if (nStartPage < printInfo.GetMinPage())
			nStartPage = printInfo.GetMinPage();
		if (nStartPage > printInfo.GetMaxPage())
			nStartPage = printInfo.GetMaxPage();

		int nStep = (nEndPage >= nStartPage) ? 1 : -1;
		nEndPage = (nEndPage == 0xffff) ? 0xffff : nEndPage + nStep;

		//VERIFY(strTemp.LoadString(AFX_IDS_PRINTPAGENUM));

		// begin page printing loop
		BOOL bError = FALSE;
		for (printInfo.m_nCurPage = nStartPage;
			printInfo.m_nCurPage != nEndPage; printInfo.m_nCurPage += nStep)
		{
			OnPrepareDC(&dcPrint, &printInfo);

			// check for end of print
			if (!printInfo.m_bContinuePrinting)
				break;

			// write current page
			/*TCHAR szBuf[80];
			wsprintf(szBuf, strTemp, printInfo.m_nCurPage);
			dlgPrintStatus.SetDlgItemText(AFX_IDC_PRINT_PAGENUM, szBuf);*/

			// set up drawing rect to entire page (in logical coordinates)
			printInfo.m_rectDraw.SetRect(0, 0,
				dcPrint.GetDeviceCaps(HORZRES),
				dcPrint.GetDeviceCaps(VERTRES));
			dcPrint.DPtoLP(&printInfo.m_rectDraw);

			// attempt to start the current page
			if (dcPrint.StartPage() < 0)
			{
				bError = TRUE;
				break;
			}

			// must call OnPrepareDC on newer versions of Windows because
			// StartPage now resets the device attributes.
			// SH: changed the following line from 
			// if (afxData.bMarked4)
			// to 
			if (GXGetAppData()->bWin4)
				OnPrepareDC(&dcPrint, &printInfo);

			ASSERT(printInfo.m_bContinuePrinting);

			// page successfully started, so now render the page
			OnPrint(&dcPrint, &printInfo);
			// SH: replaced the follwin line 
			// 	if (dcPrint.EndPage() < 0 || !_GXAbortProc(dcPrint.m_hDC, 0))
			// with
			if (dcPrint.EndPage() < 0)
			{
				bError = TRUE;
				break;
			}
		}

		// cleanup document printing process
		if (!bError)
			dcPrint.EndDoc();
		else
			dcPrint.AbortDoc();

		AfxGetMainWnd()->EnableWindow();    // enable main window

		OnEndPrinting(&dcPrint, &printInfo);    // clean up after printing
		//dlgPrintStatus.DestroyWindow();

		dcPrint.Detach();   // will be cleaned up by CPrintInfo destructor

#ifdef _MAC
		// It's common for the user to click on the printing status dialog
		// and not have the click be registered because the abort proc
		// wasn't called often enough. For this reason we call _FlushEvents
		// after printing is done so that those events don't get sent
		// through to the document window and cause unintended changes to
		// the document.

		FlushEvents(everyEvent, 0);
#endif
	}
}
/////////////////////////////////////////////////////////////////////////////
// CStaffBalanceDlg dialog
CStaffBalanceDlg::CStaffBalanceDlg(CWnd* pParent /*=NULL*/)
	: CGXPrintSupportDialog(CStaffBalanceDlg::IDD, pParent)
{
CCS_TRY
	//{{AFX_DATA_INIT(CStaffBalanceDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_Urno = 0,
	m_Date.GetCurrentTime();

	// beim Broadcast-Handler anmelden
	Register();

CCS_CATCH_ALL
}

CStaffBalanceDlg::~CStaffBalanceDlg()
{
CCS_TRY
	if (pomAccount != NULL) delete pomAccount;
	pomAccount = NULL;
CCS_CATCH_ALL
}

void CStaffBalanceDlg::DoDataExchange(CDataExchange* pDX)
{
CCS_TRY
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CStaffBalanceDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	// validation routine for CGXGridWnd controls
	DDV_GXGridWnd(pDX, pomAccount);

CCS_CATCH_ALL
}

BEGIN_MESSAGE_MAP(CStaffBalanceDlg, CDialog)
	//{{AFX_MSG_MAP(CStaffBalanceDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_MESSAGE(WM_BCADD, OnBcAdd)
	ON_BN_CLICKED(ID_BUTTON_PRINT, OnButtonPrint)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStaffBalanceDlg message handlers

BOOL CStaffBalanceDlg::OnInitDialog()
{
CCS_TRY
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu = LoadStg(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	ogCommHandler.RegisterBcWindow(this);
	ogBcHandle.GetBc();

	SetDlgItemText(IDCANCEL, LoadStg(ID_CANCEL));
	SetDlgItemText(ID_BUTTON_PRINT, LoadStg(IDS_PRINT));

	//Load Acc-Data ////
	char pclAccWhere[2000] = " ";
	CString olDateYear, olAccWhere;
	
	olDateYear = m_Date.Format("%Y");
	olAccWhere.Format("WHERE STFU = '%ld' AND YEAR = '%s'" ,m_Urno ,olDateYear);
	strcpy(pclAccWhere, olAccWhere);
	if(ogAccData.Read(pclAccWhere,false) == false){
		ogAccData.omLastErrorMessage.MakeLower();
		if(ogAccData.omLastErrorMessage.Find("no data found") == -1){
			CString olErrorTxt;
			olErrorTxt.Format("%s %d\n%s",LoadStg(ST_READERR), ogAccData.imLastReturnCode, ogAccData.omLastErrorMessage);
			MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
		}
	}

	//Grid initialisieren
	pomAccount = new CGridControl(this, IDC_ACCOUNT_GRID, ACCOUNT_COLCOUNT, ACCOUNT_ROWCOUNT);
	pomAccount->GetParam()->SetLockReadOnly(false);
	IniGrid();
	FillGrid();
	
	CString olTitle;
	olTitle = LoadStg(IDS_UEBERSICHT) + CString(" ") + m_Name + CString(" ") + LoadStg(IDS_BIS) + CString(" ");
	olTitle = olTitle + m_Date.Format("%d.%m.%Y");
	SetWindowText(olTitle);
	
	CGXStyle style;
	//Fu�zeile
	style.SetValue("");
	pomAccount->GetParam()->GetProperties()->GetDataFooter().StoreStyleRowCol(1, 1, &style, gxOverride, 0);
	
	//Kopfzeile
	style.SetValue(olTitle);
	style.SetFont(CGXFont()
					.SetSize(12)
					.SetBold(TRUE));
	pomAccount->GetParam()->GetProperties()->GetDataHeader().StoreStyleRowCol(1, 2, &style, gxOverride, 0);
	pomAccount->GetParam()->SetLockReadOnly(true);

	//Default Printer Settings
	CGXPrintDevice* pDevice = new CGXPrintDevice;
	if (pDevice)
	{
		// Ensure that device info is existent
		pDevice->NeedDeviceHandles( );

		// Now, you can get the device info
		LPDEVNAMES pDevNames;
		DWORD cbSizeDevNames;
		LPDEVMODE pDevMode;
		DWORD cbSizeDevMode;

		pDevice->GetDeviceInfo(pDevNames, cbSizeDevNames, pDevMode, cbSizeDevMode);

		// Now, you can change this structure
		// as documented in the Win31 SDK 

		pDevMode->dmOrientation = DMORIENT_LANDSCAPE;

		// and later call CreateDeviceHandles to apply the changes.

		pDevice->CreateDeviceHandles(pDevNames, cbSizeDevNames, pDevMode, cbSizeDevMode);

		delete pDevMode;
		delete pDevNames;
	}
	SetPrintDevice(pDevice);

	return TRUE;  // return TRUE  unless you set the focus to a control
CCS_CATCH_ALL
	return TRUE;
}

void CStaffBalanceDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
CCS_TRY
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
CCS_CATCH_ALL
}

void CStaffBalanceDlg::OnDestroy()
{
CCS_TRY
	ogDdx.UnRegister(this, NOTUSED);

	WinHelp(0L, HELP_QUIT);
	CDialog::OnDestroy();
CCS_CATCH_ALL
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CStaffBalanceDlg::OnPaint() 
{
CCS_TRY
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
CCS_CATCH_ALL
}

//***************************************************************************
// The system calls this to obtain the cursor to display while the user drags
// the minimized window.
//***************************************************************************

HCURSOR CStaffBalanceDlg::OnQueryDragIcon()
{
CCS_TRY
	return (HCURSOR) m_hIcon;
CCS_CATCH_ALL
}

//***************************************************************************
// In OnBcAdd laufen alle Broadcasts ein
// diese werden dann vom BcHandle weitergeleitet
//***************************************************************************

LONG CStaffBalanceDlg::OnBcAdd(UINT wParam, LONG /*lParam*/)
{
CCS_TRY
	ogBcHandle.GetBc(wParam);
	return TRUE;
CCS_CATCH_ALL
	return TRUE;
}

//***************************************************************************

void CStaffBalanceDlg::IniGrid()
{
CCS_TRY
	//�berschriften setzten
	CString olDate;
	int ilDays = GetDaysOfMonth(m_Date);
	CString olDays;
	olDays.Format("%i", ilDays);
	olDate = LoadStg(IDS_ROW1) + CString(" ") + olDays + m_Date.Format(".%m.%Y");
	pomAccount->SetValueRange(CGXRange(1,1), olDate);
	pomAccount->SetValueRange(CGXRange(1,2), LoadStg(IDS_JAN));
	pomAccount->SetValueRange(CGXRange(1,3), LoadStg(IDS_FEB));
	pomAccount->SetValueRange(CGXRange(1,4), LoadStg(IDS_MRZ));
	pomAccount->SetValueRange(CGXRange(1,5), LoadStg(IDS_APR));
	pomAccount->SetValueRange(CGXRange(1,6), LoadStg(IDS_MAI));
	pomAccount->SetValueRange(CGXRange(1,7), LoadStg(IDS_JUN));
	pomAccount->SetValueRange(CGXRange(1,8), LoadStg(IDS_JUL));
	pomAccount->SetValueRange(CGXRange(1,9), LoadStg(IDS_AUG));
	pomAccount->SetValueRange(CGXRange(1,10), LoadStg(IDS_SEP));
	pomAccount->SetValueRange(CGXRange(1,11), LoadStg(IDS_OKT));
	pomAccount->SetValueRange(CGXRange(1,12), LoadStg(IDS_NOV));
	pomAccount->SetValueRange(CGXRange(1,13), LoadStg(IDS_DEZ));
	pomAccount->SetValueRange(CGXRange(1,14), LoadStg(IDS_MIN));

	//Spaltenbreite und Zeilenh�he k�nnen nicht ver�ndert werden
	pomAccount->GetParam()->EnableTrackColWidth(FALSE);
	pomAccount->GetParam()->EnableTrackRowHeight(FALSE);
    pomAccount->GetParam()->EnableSelection(FALSE);

	//Scrollbars anzeigen wenn n�tig
	pomAccount->SetScrollBarMode(SB_BOTH, gxnAutomatic, TRUE);

	pomAccount->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE)
				.SetControl(GX_IDS_CTRL_STATIC));

	//�berschriften fett
	pomAccount->SetStyleRange(CGXRange().SetRows(1), CGXStyle()
				.SetFont(CGXFont().SetBold(TRUE)));
				//.SetBorders(gxBorderTop, CGXPen().SetWidth(2)));
    pomAccount->SetStyleRange(CGXRange().SetRows(5), CGXStyle()
				.SetFont(CGXFont().SetBold(TRUE))
				.SetBorders(gxBorderTop, CGXPen().SetWidth(2)));
	pomAccount->SetStyleRange(CGXRange().SetRows(11), CGXStyle()
				.SetFont(CGXFont().SetBold(TRUE))
				.SetBorders(gxBorderTop, CGXPen().SetWidth(2)));
	pomAccount->SetStyleRange(CGXRange().SetRows(19), CGXStyle()
				.SetFont(CGXFont().SetBold(TRUE)) 
				.SetBorders(gxBorderTop, CGXPen().SetWidth(2)));

	//Borders
	pomAccount->SetStyleRange(CGXRange().SetRows(9), CGXStyle()
				.SetBorders(gxBorderTop, CGXPen().SetWidth(2)));
	/*pomAccount->SetStyleRange(CGXRange().SetRows(22), CGXStyle()
				.SetBorders(gxBorderTop, CGXPen().SetWidth(2)));*/
	pomAccount->SetStyleRange(CGXRange().SetRows(23), CGXStyle()
				.SetBorders(gxBorderTop, CGXPen().SetWidth(2)));
	/*pomAccount->SetStyleRange(CGXRange().SetRows(24), CGXStyle()
				.SetBorders(gxBorderTop, CGXPen().SetWidth(2)));*/
	pomAccount->SetStyleRange(CGXRange().SetRows(25), CGXStyle()
				.SetBorders(gxBorderTop, CGXPen().SetWidth(2)));
	/*pomAccount->SetStyleRange(CGXRange().SetRows(26), CGXStyle()
				.SetBorders(gxBorderTop, CGXPen().SetWidth(2)));*/
	pomAccount->SetStyleRange(CGXRange().SetRows(27), CGXStyle()
				.SetBorders(gxBorderTop, CGXPen().SetWidth(2)));
	/*pomAccount->SetStyleRange(CGXRange().SetRows(29), CGXStyle()
				.SetBorders(gxBorderTop, CGXPen().SetWidth(2)));*/
	pomAccount->SetStyleRange(CGXRange().SetRows(30), CGXStyle()
				.SetBorders(gxBorderTop, CGXPen().SetWidth(2)));

	pomAccount->SetStyleRange(CGXRange().SetCols(1), CGXStyle()
				.SetBorders(gxBorderRight, CGXPen().SetWidth(1)));
	pomAccount->SetStyleRange(CGXRange().SetCols(4), CGXStyle()
				.SetBorders(gxBorderRight, CGXPen().SetWidth(1)));
	pomAccount->SetStyleRange(CGXRange().SetCols(7), CGXStyle()
				.SetBorders(gxBorderRight, CGXPen().SetWidth(1)));
	pomAccount->SetStyleRange(CGXRange().SetCols(10), CGXStyle()
				.SetBorders(gxBorderRight, CGXPen().SetWidth(1)));
	pomAccount->SetStyleRange(CGXRange().SetCols(13), CGXStyle()
				.SetBorders(gxBorderRight, CGXPen().SetWidth(1)));

	//Saldo-Zeilen gelb
	pomAccount->SetStyleRange(CGXRange().SetRows(22), CGXStyle()
				.SetInterior(RGB(249, 249, 200)));
	pomAccount->SetStyleRange(CGXRange().SetRows(24), CGXStyle()
				.SetInterior(RGB(249, 249, 200)));
	pomAccount->SetStyleRange(CGXRange().SetRows(26), CGXStyle()
				.SetInterior(RGB(249, 249, 200)));
	pomAccount->SetStyleRange(CGXRange().SetRows(29), CGXStyle()
				.SetInterior(RGB(249, 249, 200)));

	/*pomAccount->SetStyleRange(CGXRange(5,4,10,4), CGXStyle()
				.SetBorders(gxBorderRight, CGXPen().SetWidth(1)));
	pomAccount->SetStyleRange(CGXRange(5,7,10,7), CGXStyle()
				.SetBorders(gxBorderRight, CGXPen().SetWidth(1)));
	pomAccount->SetStyleRange(CGXRange(5,10,10,10), CGXStyle()
				.SetBorders(gxBorderRight, CGXPen().SetWidth(1)));*/
	
	//Keine Zeilennummerierung
	pomAccount->HideCols(0, 0);
	pomAccount->HideRows(0, 0);
	pomAccount->SetColWidth(1, 1, pomAccount->Width_LPtoDP(35 * GX_NXAVGWIDTH));
	pomAccount->SetColWidth(2, 14, pomAccount->Width_LPtoDP(8 * GX_NXAVGWIDTH));
	
	//Zentriert
	pomAccount->SetStyleRange(CGXRange(1,2,5,14), CGXStyle()
		        .SetHorizontalAlignment(DT_CENTER));

	//Rechtsb�ndig
	pomAccount->SetStyleRange(CGXRange(6,2,30,14),CGXStyle()
		        .SetHorizontalAlignment(DT_RIGHT));

	//Linksb�ndig
	/*pomAccount->SetStyleRange(CGXRange().SetCols(1),CGXStyle()
		        .SetHorizontalAlignment(DT_LEFT));*/

	//Print Margins
	CGXGridParam* pParam = pomAccount->GetParam();
	ASSERT(pParam->IsKindOf(RUNTIME_CLASS(CGXGridParam)));

	CGXProperties* pProp = pParam->GetProperties();
	ASSERT(pProp->IsKindOf(RUNTIME_CLASS(CGXProperties)));
	
	int ilTop, ilLeft, ilRight, ilBottom;
	pProp->GetMargins(ilTop, ilLeft, ilBottom, ilRight);
	pProp->SetMargins(ilTop,5,ilBottom,5);

	pomAccount->EnableAutoGrow(false);
	pomAccount->EnableSorting(false);
CCS_CATCH_ALL
}


void CStaffBalanceDlg::FillGrid()
{
CCS_TRY
	CString olDescription, olRowCount, olDate, olAusgleichStd;
	double lNormStd, lGeleisteteStd, lAusgleichStd, lPlanStd;
	
	//ROW *olRow;
	
	// Grid mit Daten f�llen.
	//pomAccount->GetParam()->SetLockReadOnly(false);

	//Freie Tage im Monat (5CL-5OP)
	pomAccount->SetValueRange(CGXRange(2,1), LoadStg(IDS_ROW2));
	CalcAccounts("5", 2);
	//Freie Tage im Quartal (8CL-8OP)
	pomAccount->SetValueRange(CGXRange(3,1), LoadStg(IDS_ROW3));
	CalcAccounts("8", 3);
	//Freie Tage im Jahr (11CL-11OP)
	pomAccount->SetValueRange(CGXRange(4,1), LoadStg(IDS_ROW4));
	CalcAccounts("11", 4);

	//Minimale Anzahl freie Tage im Jahr
	//uhi 19.3.01 Parameter lesen
	CTime	olTimeNow = CTime::GetCurrentTime();
	CString olStringNow = olTimeNow.Format("%Y%m%d%H%M%S");
	CString olMinFree = ogCCSParam.GetParamValue(ogRoster,"ID_YEARFREE",olStringNow);
	pomAccount->SetValueRange(CGXRange(4,14), olMinFree);

	//�berschrift: Arbeitsstunde
	int ilDays = GetDaysOfMonth(m_Date);
	CString olDays;
	olDays.Format("%i", ilDays);
	olDate = LoadStg(IDS_ROW5) + CString(" ") + olDays + m_Date.Format(".%m.%Y");	pomAccount->SetValueRange(CGXRange(5,1), olDate);
	pomAccount->SetValueRange(CGXRange(5,3), LoadStg(IDS_QUARTAL1));
	pomAccount->SetValueRange(CGXRange(5,6), LoadStg(IDS_QUARTAL2));
	pomAccount->SetValueRange(CGXRange(5,9), LoadStg(IDS_QUARTAL3));
	pomAccount->SetValueRange(CGXRange(5,12), LoadStg(IDS_QUARTAL4));
	//Normstunden (23CO)
	pomAccount->SetValueRange(CGXRange(6,1), LoadStg(IDS_ROW6));
	CalcAccounts("23", 6);
	
	//Planstunden 26CL
	pomAccount->SetValueRange(CGXRange(7,1), LoadStg(IDS_ROW7));
	//Geleistete Stunden 26CL
	pomAccount->SetValueRange(CGXRange(8,1), LoadStg(IDS_ROW8));
	//Berechnung f�r Planstunden und Geleistete Stunden!
	CalcAccounts("26", 8);

	//uhi 10.8.01 PRF 265
	//Ausgleichstunden Monat = (Planstunden + Geleistete Stunden) - Normstunden
	CString olLoad = "Loading...";
	int		iMonth = m_Date.GetMonth();
	int		iDay   = m_Date.GetDay();
	BOOL	bLastDayOfMonth = ilDays == iDay; 

	pomAccount->SetValueRange(CGXRange(9,1), LoadStg(IDS_ROW9));
	for(int m = 2; m <= 13; m++){
		lNormStd = 0.00;
		lGeleisteteStd = 0.00;
		lPlanStd = 0.00;
		lNormStd = atof(pomAccount->GetValueRowCol(6,m));
		lGeleisteteStd = atof(pomAccount->GetValueRowCol(8,m));
		lPlanStd = atof(pomAccount->GetValueRowCol(7,m));
		lAusgleichStd = (lPlanStd + lGeleisteteStd) - lNormStd;
		olAusgleichStd.Format("%.2f", lAusgleichStd);
		pomAccount->SetValueRange(CGXRange(9,m), olAusgleichStd);
		if ((iMonth == m - 1) && !bLastDayOfMonth)
		{
			pomAccount->SetValueRange(CGXRange(8,m), olLoad);
		}
	}

	//Ausgleichstunden kumulativ (51CL-51OP)
	pomAccount->SetValueRange(CGXRange(10,1), LoadStg(IDS_ROW10));
	CalcAccounts("51", 10);
	
	//�berschrift: Bonus Konti
	olDate = LoadStg(IDS_ROW11) + CString(" ") + m_Date.Format("%d.%m.%Y");
	pomAccount->SetValueRange(CGXRange(11,1), olDate);
	//Arbeitszeit im Stundenlohn (110CO)
	pomAccount->SetValueRange(CGXRange(12,1), LoadStg(IDS_ROW12));
	CalcAccounts("110", 12);
	//�berzeit (116CO)
	pomAccount->SetValueRange(CGXRange(13,1), LoadStg(IDS_ROW13));
	CalcAccounts("116", 13);
	//Nachstunden (135CO)
	pomAccount->SetValueRange(CGXRange(14,1), LoadStg(IDS_ROW14));
	CalcAccounts("135", 14);
	//Sonn- und Nachtpunkte ausbezahlt (137CO)
	pomAccount->SetValueRange(CGXRange(15,1), LoadStg(IDS_ROW15));
	CalcAccounts("137", 15);
	//Zuschlag f�r zwei Stunden-Eins�tze (139CO)
	pomAccount->SetValueRange(CGXRange(16,1), LoadStg(IDS_ROW16));
	CalcAccounts("139", 16);
	//�berzeit Teilzeit + (157CO)
	pomAccount->SetValueRange(CGXRange(17,1), LoadStg(IDS_ROW17));
	CalcAccounts("157", 17);
	//Dienstsonntage (140CO)
	pomAccount->SetValueRange(CGXRange(18,1), LoadStg(IDS_ROW18));
	CalcAccounts("140", 18);
	
	//�berschrift: Konti geplant
	olDate =  LoadStg(IDS_ROW19) + CString(" ") + CString("31.12.") + m_Date.Format("%Y");
	pomAccount->SetValueRange(CGXRange(19,1), olDate);
	//�berzeit (32CL-32OP)
	pomAccount->SetValueRange(CGXRange(20,1), LoadStg(IDS_ROW20));
	CalcAccounts("32", 20);
	//Kompensation (33CL-33OP)
	pomAccount->SetValueRange(CGXRange(21,1), LoadStg(IDS_ROW21));
	CalcAccounts("33", 21);
	//Saldo (32CL-33CL)
	pomAccount->SetValueRange(CGXRange(22,1), LoadStg(IDS_ROW22));
	CalcSaldos(22, "32", "33");
	//Ferienbezug (30CL-30OP)
	pomAccount->SetValueRange(CGXRange(23,1), LoadStg(IDS_ROW23));
	CalcAccounts("30", 23);
	//Saldo (30CL)
	pomAccount->SetValueRange(CGXRange(24,1), LoadStg(IDS_ROW24));
	CalcSaldos(24, "30");
	//Zus�tzlicher Ferienbezug (31CL-31OP)
	pomAccount->SetValueRange(CGXRange(25,1), LoadStg(IDS_ROW25));
	CalcAccounts("31", 25);
	//Saldo (31CL)
	pomAccount->SetValueRange(CGXRange(26,1), LoadStg(IDS_ROW26));
	CalcSaldos(26, "31");
	//Nachstunden geleistet (34CL-34OP)
	pomAccount->SetValueRange(CGXRange(27,1), LoadStg(IDS_ROW27));
	CalcAccounts("34", 27);
	//Nachstunden bezogen (49CL-49OP)
	pomAccount->SetValueRange(CGXRange(28,1), LoadStg(IDS_ROW28));
	CalcAccounts("49", 28);
	//Saldo (34CL-49CL)
	pomAccount->SetValueRange(CGXRange(29,1), LoadStg(IDS_ROW29));
	CalcSaldos(29, "34", "49");
	//Flex Code (44CO)
	pomAccount->SetValueRange(CGXRange(30,1), LoadStg(IDS_ROW30));
	CalcAccounts("44", 30);

	// Anzeige einstellen
	//pomAccount->GetParam()->SetLockReadOnly(true);
CCS_CATCH_ALL
}

void CStaffBalanceDlg::CalcAccounts(CString opType, int ipRow)
{
CCS_TRY
	ACCDATA *pomAcc;
	CString olYear, olResult;
	double lClose, lOpen, lCorrect, lResult;
	
	int		iMonth = m_Date.GetMonth();
	int		iDay   = m_Date.GetDay();
	BOOL	bLastDayOfMonth = GetDaysOfMonth(m_Date) == iDay; 

	CString olLoad = "Loading...";
	CString olNix = "0.00";

	olYear = m_Date.Format("%Y");

	pomAcc = ogAccData.GetAccByKey(m_Urno, olYear, opType);
	if(pomAcc != NULL)
	{
		switch(atoi(opType))
		{
			case 30: case 31:
			{
				//Januar
				lClose = atof(pomAcc->Cl01);
				lOpen = atof(pomAcc->Op01);
				lResult = (lClose-lOpen) * (-1);
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,2), olResult);
				}
				//Februar
				lClose = atof(pomAcc->Cl02);
				lOpen = atof(pomAcc->Op02);
				lResult = (lClose-lOpen) * (-1);
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,3), olResult);
				}
				//M�rz
				lClose = atof(pomAcc->Cl03);
				lOpen = atof(pomAcc->Op03);
				lResult = (lClose-lOpen) * (-1);
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,4), olResult);
				}
				//April
				lClose = atof(pomAcc->Cl04);
				lOpen = atof(pomAcc->Op04);
				lResult = (lClose-lOpen) * (-1);
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,5), olResult);
				}
				//Mai
				lClose = atof(pomAcc->Cl05);
				lOpen = atof(pomAcc->Op05);
				lResult = (lClose-lOpen) * (-1);
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,6), olResult);
				}
				//Juni
				lClose = atof(pomAcc->Cl06);
				lOpen = atof(pomAcc->Op06);
				lResult = (lClose-lOpen) * (-1);
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,7), olResult);
				}
				//Juli
				lClose = atof(pomAcc->Cl07);
				lOpen = atof(pomAcc->Op07);
				lResult = (lClose-lOpen) * (-1);
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,8), olResult);
				}
				//August
				lClose = atof(pomAcc->Cl08);
				lOpen = atof(pomAcc->Op08);
				lResult = (lClose-lOpen) * (-1);
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,9), olResult);
				}
				//September
				lClose = atof(pomAcc->Cl09);
				lOpen = atof(pomAcc->Op09);
				lResult = (lClose-lOpen) * (-1);
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,10), olResult);
				}
				//Oktober
				lClose = atof(pomAcc->Cl10);
				lOpen = atof(pomAcc->Op10);
				lResult = (lClose-lOpen) * (-1);
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,11), olResult);
				}
				//November
				lClose = atof(pomAcc->Cl11);
				lOpen = atof(pomAcc->Op11);
				lResult = (lClose-lOpen) * (-1);
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,12), olResult);
				}
				//Dezember
				lClose = atof(pomAcc->Cl12);
				lOpen = atof(pomAcc->Op12);
				lResult = (lClose-lOpen) * (-1);
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,13), olResult);
				}
				break;
			}
			case 11: case 32:
			case 33: case 34: case 49:
			{
				//Januar
				lClose = atof(pomAcc->Cl01);
				lOpen = atof(pomAcc->Op01);
				lResult = lClose-lOpen;
				if(fabs(lResult) > 0){
					if(opType == "5" || opType == "8" || opType == "11")
						olResult.Format("%.0f", lResult);
					else
						olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,2), olResult);
				}
				//Februar
				lClose = atof(pomAcc->Cl02);
				lOpen = atof(pomAcc->Op02);
				lResult = lClose-lOpen;
				if(fabs(lResult) > 0){
					if(opType == "5" || opType == "8" || opType == "11")
						olResult.Format("%.0f", lResult);
					else
						olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,3), olResult);
				}
				//M�rz
				lClose = atof(pomAcc->Cl03);
				lOpen = atof(pomAcc->Op03);
				lResult = lClose-lOpen;
				if(fabs(lResult) > 0){
					if(opType == "5" || opType == "8" || opType == "11")
						olResult.Format("%.0f", lResult);
					else
						olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,4), olResult);
				}
				//April
				lClose = atof(pomAcc->Cl04);
				lOpen = atof(pomAcc->Op04);
				lResult = lClose-lOpen;
				if(fabs(lResult) > 0){
					if(opType == "5" || opType == "8" || opType == "11")
						olResult.Format("%.0f", lResult);
					else
						olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,5), olResult);
				}
				//Mai
				lClose = atof(pomAcc->Cl05);
				lOpen = atof(pomAcc->Op05);
				lResult = lClose-lOpen;
				if(fabs(lResult) > 0){
					if(opType == "5" || opType == "8" || opType == "11")
						olResult.Format("%.0f", lResult);
					else
						olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,6), olResult);
				}
				//Juni
				lClose = atof(pomAcc->Cl06);
				lOpen = atof(pomAcc->Op06);
				lResult = lClose-lOpen;
				if(fabs(lResult) > 0){
					if(opType == "5" || opType == "8" || opType == "11")
						olResult.Format("%.0f", lResult);
					else
						olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,7), olResult);
				}
				//Juli
				lClose = atof(pomAcc->Cl07);
				lOpen = atof(pomAcc->Op07);
				lResult = lClose-lOpen;
				if(fabs(lResult) > 0){
					if(opType == "5" || opType == "8" || opType == "11")
						olResult.Format("%.0f", lResult);
					else
						olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,8), olResult);
				}
				//August
				lClose = atof(pomAcc->Cl08);
				lOpen = atof(pomAcc->Op08);
				lResult = lClose-lOpen;
				if(fabs(lResult) > 0){
					if(opType == "5" || opType == "8" || opType == "11")
						olResult.Format("%.0f", lResult);
					else
						olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,9), olResult);
				}
				//September
				lClose = atof(pomAcc->Cl09);
				lOpen = atof(pomAcc->Op09);
				lResult = lClose-lOpen;
				if(fabs(lResult) > 0){
					if(opType == "5" || opType == "8" || opType == "11")
						olResult.Format("%.0f", lResult);
					else
						olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,10), olResult);
				}
				//Oktober
				lClose = atof(pomAcc->Cl10);
				lOpen = atof(pomAcc->Op10);
				lResult = lClose-lOpen;
				if(fabs(lResult) > 0){
					if(opType == "5" || opType == "8" || opType == "11")
						olResult.Format("%.0f", lResult);
					else
						olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,11), olResult);
				}
				//November
				lClose = atof(pomAcc->Cl11);
				lOpen = atof(pomAcc->Op11);
				lResult = lClose-lOpen;
				if(fabs(lResult) > 0){
					if(opType == "5" || opType == "8" || opType == "11")
						olResult.Format("%.0f", lResult);
					else
						olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,12), olResult);
				}
				//Dezember
				lClose = atof(pomAcc->Cl12);
				lOpen = atof(pomAcc->Op12);
				lResult = lClose-lOpen;
				if(fabs(lResult) > 0){
					if(opType == "5" || opType == "8" || opType == "11")
						olResult.Format("%.0f", lResult);
					else
						olResult.Format("%.2f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,13), olResult);
				}
				break;
			}
			case 5:
			{
				//Januar
				lClose = atof(pomAcc->Cl01);
				lResult = lClose;
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.0f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,2), olResult);
				}

				//Februar
				lClose = atof(pomAcc->Cl02);
				lResult = lClose;
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.0f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,3), olResult);
				}

				//M�rz
				lClose = atof(pomAcc->Cl03);
				lResult = lClose;
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.0f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,4), olResult);
				}

				//April
				lClose = atof(pomAcc->Cl04);
				lResult = lClose;
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.0f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,5), olResult);
				}

				//Mai
				lClose = atof(pomAcc->Cl05);
				lResult = lClose;
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.0f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,6), olResult);
				}

				//Juni
				lClose = atof(pomAcc->Cl06);
				lResult = lClose;
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.0f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,7), olResult);
				}

				//Juli
				lClose = atof(pomAcc->Cl07);
				lResult = lClose;
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.0f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,8), olResult);
				}

				//August
				lClose = atof(pomAcc->Cl08);
				lResult = lClose;
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.0f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,9), olResult);
				}

				//September
				lClose = atof(pomAcc->Cl09);
				lResult = lClose;
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.0f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,10), olResult);
				}

				//Oktober
				lClose = atof(pomAcc->Cl10);
				lResult = lClose;
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.0f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,11), olResult);
				}

				//November
				lClose = atof(pomAcc->Cl11);
				lResult = lClose;
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.0f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,12), olResult);
				}

				//Dezember
				lClose = atof(pomAcc->Cl12);
				lResult = lClose;
				if(fabs(lResult) > 0)
				{
					olResult.Format("%.0f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,13), olResult);
				}
				break;
			}
			case 8:
			{
				//M�rz
				lClose = atof(pomAcc->Cl03);
				lOpen = atof(pomAcc->Op03);
				lResult = lClose-lOpen;
				if(fabs(lResult) > 0){
					olResult.Format("%.0f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,4), olResult);
				}
				//Juni
				lClose = atof(pomAcc->Cl06);
				lOpen = atof(pomAcc->Op06);
				lResult = lClose-lOpen;
				if(fabs(lResult) > 0){
					olResult.Format("%.0f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,7), olResult);
				}
				//September
				lClose = atof(pomAcc->Cl09);
				lOpen = atof(pomAcc->Op09);
				lResult = lClose-lOpen;
				if(fabs(lResult) > 0){
					olResult.Format("%.0f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,10), olResult);
				}
				//Dezember
				lClose = atof(pomAcc->Cl12);
				lOpen = atof(pomAcc->Op12);
				lResult = lClose-lOpen;
				if(fabs(lResult) > 0){
					olResult.Format("%.0f", lResult);
					pomAccount->SetValueRange(CGXRange(ipRow,13), olResult);
				}
				break;
			}
			case 51:
			{
				//Januar
				lClose = atof(pomAcc->Cl01);
				lResult = lClose;
				olResult.Format("%.2f", lResult);
				pomAccount->SetValueRange(CGXRange(ipRow,2), olResult);

				//Februar
				lClose = atof(pomAcc->Cl02);
				lResult = lClose;
				olResult.Format("%.2f", lResult);
				pomAccount->SetValueRange(CGXRange(ipRow,3), olResult);

				//M�rz
				lClose = atof(pomAcc->Cl03);
				lResult = lClose;
				olResult.Format("%.2f", lResult);
				pomAccount->SetValueRange(CGXRange(ipRow,4), olResult);

				//April
				lClose = atof(pomAcc->Cl04);
				lResult = lClose;
				olResult.Format("%.2f", lResult);
				pomAccount->SetValueRange(CGXRange(ipRow,5), olResult);

				//Mai
				lClose = atof(pomAcc->Cl05);
				lResult = lClose;
				olResult.Format("%.2f", lResult);
				pomAccount->SetValueRange(CGXRange(ipRow,6), olResult);

				//Juni
				lClose = atof(pomAcc->Cl06);
				lResult = lClose;
				olResult.Format("%.2f", lResult);
				pomAccount->SetValueRange(CGXRange(ipRow,7), olResult);

				//Juli
				lClose = atof(pomAcc->Cl07);
				lResult = lClose;
				olResult.Format("%.2f", lResult);
				pomAccount->SetValueRange(CGXRange(ipRow,8), olResult);

				//August
				lClose = atof(pomAcc->Cl08);
				lResult = lClose;
				olResult.Format("%.2f", lResult);
				pomAccount->SetValueRange(CGXRange(ipRow,9), olResult);

				//September
				lClose = atof(pomAcc->Cl09);
				lResult = lClose;
				olResult.Format("%.2f", lResult);
				pomAccount->SetValueRange(CGXRange(ipRow,10), olResult);

				//Oktober
				lClose = atof(pomAcc->Cl10);
				lResult = lClose;
				olResult.Format("%.2f", lResult);
				pomAccount->SetValueRange(CGXRange(ipRow,11), olResult);

				//November
				lClose = atof(pomAcc->Cl11);
				lResult = lClose;
				olResult.Format("%.2f", lResult);
				pomAccount->SetValueRange(CGXRange(ipRow,12), olResult);

				//Dezember
				lClose = atof(pomAcc->Cl12);
				lResult = lClose;
				olResult.Format("%.2f", lResult);
				pomAccount->SetValueRange(CGXRange(ipRow,13), olResult);
				break;
			}
			case 26:
			{
				//Januar
				lClose = atof(pomAcc->Cl01);
				if(lClose > 0){
					olResult.Format("%.2f", lClose);
					if(iMonth > 1 || (iMonth == 1 && bLastDayOfMonth))
						pomAccount->SetValueRange(CGXRange(ipRow,2), olResult);
					else{
						if(iMonth == 1){
							pomAccount->SetValueRange(CGXRange(ipRow,2), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow - 1,2), olLoad);
						}
						else{
							pomAccount->SetValueRange(CGXRange(ipRow - 1,2), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow,2), olNix);
						}
					}
				}
				//Februar
				lClose = atof(pomAcc->Cl02);
				if(lClose > 0){
					olResult.Format("%.2f", lClose);
					if(iMonth > 2 || (iMonth == 2 && bLastDayOfMonth))
						pomAccount->SetValueRange(CGXRange(ipRow,3), olResult);
					else{
						if(iMonth == 2){
							pomAccount->SetValueRange(CGXRange(ipRow,3), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow - 1,3), olLoad);
						}
						else{
							pomAccount->SetValueRange(CGXRange(ipRow - 1,3), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow,3), olNix);
						}
					}
				}
				//M�rz
				lClose = atof(pomAcc->Cl03);
				if(lClose > 0){
					olResult.Format("%.2f", lClose);
					if(iMonth > 3 || (iMonth == 3 && bLastDayOfMonth))
						pomAccount->SetValueRange(CGXRange(ipRow,4), olResult);
					else{
						if(iMonth == 3){
							pomAccount->SetValueRange(CGXRange(ipRow,4), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow - 1,4), olLoad);
						}
						else{
							pomAccount->SetValueRange(CGXRange(ipRow - 1,4), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow,4), olNix);
						}
					}
				}
				//April
				lClose = atof(pomAcc->Cl04);
				if(lClose > 0){
					olResult.Format("%.2f", lClose);
					if(iMonth > 4 || (iMonth == 4 && bLastDayOfMonth))
						pomAccount->SetValueRange(CGXRange(ipRow,5), olResult);
					else{
						if(iMonth == 4){
							pomAccount->SetValueRange(CGXRange(ipRow,5), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow - 1,5), olLoad);
						}
						else{
							pomAccount->SetValueRange(CGXRange(ipRow - 1,5), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow,5), olNix);
						}
					}
				}
				//Mai
				lClose = atof(pomAcc->Cl05);
				if(lClose > 0){
					olResult.Format("%.2f", lClose);
					if(iMonth > 5 || (iMonth == 5 && bLastDayOfMonth))
						pomAccount->SetValueRange(CGXRange(ipRow,6), olResult);
					else{
						if(iMonth == 5){
							pomAccount->SetValueRange(CGXRange(ipRow,6), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow - 1,6), olLoad);
						}
						else{
							pomAccount->SetValueRange(CGXRange(ipRow - 1,6), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow,6), olNix);
						}
					}
				}
				//Juni
				lClose = atof(pomAcc->Cl06);
				if(lClose > 0){
					olResult.Format("%.2f", lClose);
					if(iMonth > 6 || (iMonth == 6 && bLastDayOfMonth))
						pomAccount->SetValueRange(CGXRange(ipRow,7), olResult);
					else{
						if(iMonth == 6){
							pomAccount->SetValueRange(CGXRange(ipRow,7), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow - 1,7), olLoad);
						}
						else{
							pomAccount->SetValueRange(CGXRange(ipRow - 1,7), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow,7), olNix);
						}
					}
				}
				//Juli
				lClose = atof(pomAcc->Cl07);
				if(lClose > 0){
					olResult.Format("%.2f", lClose);
					if(iMonth > 7 || (iMonth == 7 && bLastDayOfMonth))
						pomAccount->SetValueRange(CGXRange(ipRow,8), olResult);
					else{
						if(iMonth == 7){
							pomAccount->SetValueRange(CGXRange(ipRow,8), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow - 1,8), olLoad);
						}
						else{
							pomAccount->SetValueRange(CGXRange(ipRow - 1,8), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow,8), olNix);
						}
					}
				}
				//August
				lClose = atof(pomAcc->Cl08);
				if(lClose > 0){
					olResult.Format("%.2f", lClose);
					if(iMonth > 8 || (iMonth == 8 && bLastDayOfMonth))
						pomAccount->SetValueRange(CGXRange(ipRow,9), olResult);
					else{
						if(iMonth == 8){
							pomAccount->SetValueRange(CGXRange(ipRow,9), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow - 1,9), olLoad);
						}
						else{
							pomAccount->SetValueRange(CGXRange(ipRow - 1,9), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow,9), olNix);
						}
					}
				}
				//September
				lClose = atof(pomAcc->Cl09);
				if(lClose > 0){
					olResult.Format("%.2f", lClose);
					if(iMonth > 9 || (iMonth == 9 && bLastDayOfMonth))
						pomAccount->SetValueRange(CGXRange(ipRow,10), olResult);
					else{
						if(iMonth == 9){
							pomAccount->SetValueRange(CGXRange(ipRow,10), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow - 1,10), olLoad);
						}
						else{
							pomAccount->SetValueRange(CGXRange(ipRow - 1,10), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow,10), olNix);
						}
					}
				}
				//Oktober
				lClose = atof(pomAcc->Cl10);
				if(lClose > 0){
					olResult.Format("%.2f", lClose);
					if(iMonth > 10 || (iMonth == 10 && bLastDayOfMonth))
						pomAccount->SetValueRange(CGXRange(ipRow,11), olResult);
					else{
						if(iMonth == 10){
							pomAccount->SetValueRange(CGXRange(ipRow,11), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow - 1,11), olLoad);
						}
						else{
							pomAccount->SetValueRange(CGXRange(ipRow - 1,11), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow,11), olNix);
						}
					}
				}
				//November
				lClose = atof(pomAcc->Cl11);
				if(lClose > 0){
					olResult.Format("%.2f", lClose);
					if(iMonth > 11 || (iMonth == 11 && bLastDayOfMonth))
						pomAccount->SetValueRange(CGXRange(ipRow,12), olResult);
					else{
						if(iMonth == 11){
							pomAccount->SetValueRange(CGXRange(ipRow,12), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow - 1,12), olLoad);
						}
						else{
							pomAccount->SetValueRange(CGXRange(ipRow - 1,12), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow,12), olNix);
						}
					}
				}
				//Dezember
				lClose = atof(pomAcc->Cl12);
				if(lClose > 0){
					olResult.Format("%.2f", lClose);
					if(iMonth == 12 && bLastDayOfMonth)
						pomAccount->SetValueRange(CGXRange(ipRow,13), olResult);
					else{
						if(iMonth == 12){
							pomAccount->SetValueRange(CGXRange(ipRow,13), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow - 1,13), olLoad);
						}
						else{
							pomAccount->SetValueRange(CGXRange(ipRow - 1,13), olResult);
							pomAccount->SetValueRange(CGXRange(ipRow,13), olNix);
						}
					}
				}

				break;
			}
			case 23: case 44:
			{
				//Januar
				lCorrect = atof(pomAcc->Co01);
				if(lCorrect > 0){
					olResult.Format("%.2f", lCorrect);
					pomAccount->SetValueRange(CGXRange(ipRow,2), olResult);
				}
				//Februar
				lCorrect = atof(pomAcc->Co02);
				if(lCorrect > 0){
					olResult.Format("%.2f", lCorrect);
					pomAccount->SetValueRange(CGXRange(ipRow,3), olResult);
				}
				//M�rz
				lCorrect = atof(pomAcc->Co03);
				if(lCorrect > 0){
					olResult.Format("%.2f", lCorrect);
					pomAccount->SetValueRange(CGXRange(ipRow,4), olResult);
				}
				//April
				lCorrect = atof(pomAcc->Co04);
				if(lCorrect > 0){
					olResult.Format("%.2f", lCorrect);
					pomAccount->SetValueRange(CGXRange(ipRow,5), olResult);
				}
				//Mai
				lCorrect = atof(pomAcc->Co05);
				if(lCorrect > 0){
					olResult.Format("%.2f", lCorrect);
					pomAccount->SetValueRange(CGXRange(ipRow,6), olResult);
				}
				//Juni
				lCorrect = atof(pomAcc->Co06);
				if(lCorrect > 0){
					olResult.Format("%.2f", lCorrect);
					pomAccount->SetValueRange(CGXRange(ipRow,7), olResult);
				}
				//Juli
				lCorrect = atof(pomAcc->Co07);
				if(lCorrect > 0){
					olResult.Format("%.2f", lCorrect);
					pomAccount->SetValueRange(CGXRange(ipRow,8), olResult);
				}
				//August
				lCorrect = atof(pomAcc->Co08);
				if(lCorrect > 0){
					olResult.Format("%.2f", lCorrect);
					pomAccount->SetValueRange(CGXRange(ipRow,9), olResult);
				}
				//September
				lCorrect = atof(pomAcc->Co09);
				if(lCorrect > 0){
					olResult.Format("%.2f", lCorrect);
					pomAccount->SetValueRange(CGXRange(ipRow,10), olResult);
				}
				//Oktober
				lCorrect = atof(pomAcc->Co10);
				if(lCorrect > 0){
					olResult.Format("%.2f", lCorrect);
					pomAccount->SetValueRange(CGXRange(ipRow,11), olResult);
				}
				//November
				lCorrect = atof(pomAcc->Co11);
				if(lCorrect > 0){
					olResult.Format("%.2f", lCorrect);
					pomAccount->SetValueRange(CGXRange(ipRow,12), olResult);
				}
				//Dezember
				lCorrect = atof(pomAcc->Co12);
				if(lCorrect > 0){
					olResult.Format("%.2f", lCorrect);
					pomAccount->SetValueRange(CGXRange(ipRow,13), olResult);
				}
				break;
			}
			case 110: case 116: case 135:
			case 137: case 139: case 140: case 157:{
				//Januar
				lCorrect = atof(pomAcc->Co01);
				if(lCorrect > 0){
					if(opType == "140")
						olResult.Format("%.0f", lCorrect);
					else
						olResult.Format("%.2f", lCorrect);
					if(iMonth > 1)
						pomAccount->SetValueRange(CGXRange(ipRow,2), olResult);
					else
						if(iMonth == 1)
							pomAccount->SetValueRange(CGXRange(ipRow,2), olLoad);
				}
				//Februar
				lCorrect = atof(pomAcc->Co02);
				if(lCorrect > 0){
					if(opType == "140")
						olResult.Format("%.0f", lCorrect);
					else
						olResult.Format("%.2f", lCorrect);
					if(iMonth > 2)
						pomAccount->SetValueRange(CGXRange(ipRow,3), olResult);
					else
						if (iMonth == 2)
							pomAccount->SetValueRange(CGXRange(ipRow,3), olLoad);
				}
				//M�rz
				lCorrect = atof(pomAcc->Co03);
				if(lCorrect > 0){
					if(opType == "140")
						olResult.Format("%.0f", lCorrect);
					else
						olResult.Format("%.2f", lCorrect);
					if(iMonth > 3)
						pomAccount->SetValueRange(CGXRange(ipRow,4), olResult);
					else
						if(iMonth == 3)
							pomAccount->SetValueRange(CGXRange(ipRow,4), olLoad);
				}
				//April
				lCorrect = atof(pomAcc->Co04);
				if(lCorrect > 0){
					if(opType == "140")
						olResult.Format("%.0f", lCorrect);
					else
						olResult.Format("%.2f", lCorrect);
					if(iMonth > 4)
						pomAccount->SetValueRange(CGXRange(ipRow,5), olResult);
					else
						if(iMonth ==4)
							pomAccount->SetValueRange(CGXRange(ipRow,5), olLoad);
				}
				//Mai
				lCorrect = atof(pomAcc->Co05);
				if(lCorrect > 0){
					if(opType == "140")
						olResult.Format("%.0f", lCorrect);
					else
						olResult.Format("%.2f", lCorrect);
					if(iMonth > 5)
						pomAccount->SetValueRange(CGXRange(ipRow,6), olResult);
					else
						if(iMonth == 5)
							pomAccount->SetValueRange(CGXRange(ipRow,6), olLoad);
				}
				//Juni
				lCorrect = atof(pomAcc->Co06);
				if(lCorrect > 0){
					if(opType == "140")
						olResult.Format("%.0f", lCorrect);
					else
						olResult.Format("%.2f", lCorrect);
					if(iMonth > 6)
						pomAccount->SetValueRange(CGXRange(ipRow,7), olResult);
					else
						if(iMonth == 6)
							pomAccount->SetValueRange(CGXRange(ipRow,7), olLoad);
				}
				//Juli
				lCorrect = atof(pomAcc->Co07);
				if(lCorrect > 0){
					if(opType == "140")
						olResult.Format("%.0f", lCorrect);
					else
						olResult.Format("%.2f", lCorrect);
					if(iMonth > 7)
						pomAccount->SetValueRange(CGXRange(ipRow,8), olResult);
					else
						if(iMonth ==7)
							pomAccount->SetValueRange(CGXRange(ipRow,8), olLoad);
				}
				//August
				lCorrect = atof(pomAcc->Co08);
				if(lCorrect > 0){
					if(opType == "140")
						olResult.Format("%.0f", lCorrect);
					else
						olResult.Format("%.2f", lCorrect);
					if(iMonth > 8)
						pomAccount->SetValueRange(CGXRange(ipRow,9), olResult);
					else
						if(iMonth == 8)
							pomAccount->SetValueRange(CGXRange(ipRow,9), olLoad);
				}
				//September
				lCorrect = atof(pomAcc->Co09);
				if(lCorrect > 0){
					if(opType == "140")
						olResult.Format("%.0f", lCorrect);
					else
						olResult.Format("%.2f", lCorrect);
					if(iMonth > 9)
						pomAccount->SetValueRange(CGXRange(ipRow,10), olResult);
					else
						if(iMonth == 9)
							pomAccount->SetValueRange(CGXRange(ipRow,10), olLoad);
				}
				//Oktober
				lCorrect = atof(pomAcc->Co10);
				if(lCorrect > 0){
					if(opType == "140")
						olResult.Format("%.0f", lCorrect);
					else
						olResult.Format("%.2f", lCorrect);
					if(iMonth > 10)
						pomAccount->SetValueRange(CGXRange(ipRow,11), olResult);
					else
						if(iMonth == 10)
							pomAccount->SetValueRange(CGXRange(ipRow,11), olLoad);
				}
				//November
				lCorrect = atof(pomAcc->Co11);
				if(lCorrect > 0){
					if(opType == "140")
						olResult.Format("%.0f", lCorrect);
					else
						olResult.Format("%.2f", lCorrect);
					if(iMonth > 11)
						pomAccount->SetValueRange(CGXRange(ipRow,12), olResult);
					else
						if(iMonth == 11)
							pomAccount->SetValueRange(CGXRange(ipRow,12), olLoad);
				}
				//Dezember
				lCorrect = atof(pomAcc->Co12);
				if(lCorrect > 0){
					if(opType == "140")
						olResult.Format("%.0f", lCorrect);
					else
						olResult.Format("%.2f", lCorrect);
					if(iMonth == 12)
						pomAccount->SetValueRange(CGXRange(ipRow,13), olLoad);
					else
						pomAccount->SetValueRange(CGXRange(ipRow,13), olResult);
				}
				break;
			}		 

			default:
				break;
		}
	}
CCS_CATCH_ALL
}

int CStaffBalanceDlg::GetDaysOfMonth(COleDateTime opDay)
{
CCS_TRY
	if(opDay.GetStatus() != COleDateTime::valid)
		return 0;

	COleDateTime olMonth = COleDateTime(opDay.GetYear(), opDay.GetMonth(),1,0,0,0);
	bool blNextMonth = false;
	int ilDays = 28;
	while(!blNextMonth)
	{
		COleDateTime olTmpDay = olMonth + COleDateTimeSpan(ilDays,0,0,0);
		if(olTmpDay.GetMonth() != opDay.GetMonth())
			blNextMonth = true;
		else
			ilDays++;
	}
	if(ilDays < 0 || ilDays > 31)
		ilDays = 0;
	return ilDays;
CCS_CATCH_ALL
	return 0;
}

void CStaffBalanceDlg::CalcSaldos(int ipRow, CString opType1, CString opType2 /*=""*/)
{
CCS_TRY
	ACCDATA *pomAcc1, *pomAcc2;
	CString olYear, olResult;
	double lValue1, lValue2, lResult;
	
	olYear = m_Date.Format("%Y");

	switch(ipRow){
	case 22: case 29:
	{
		pomAcc1 = ogAccData.GetAccByKey(m_Urno, olYear, opType1);
		pomAcc2 = ogAccData.GetAccByKey(m_Urno, olYear, opType2);
		//Januar
		lValue1 = 0;
		lValue2 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl01);
		if(pomAcc2 != NULL)
			lValue2 = atof(pomAcc2->Cl01);
		lResult = lValue1 - lValue2;
		if(fabs(lResult) > 0){
			olResult.Format("%.2f", lResult);
			pomAccount->SetValueRange(CGXRange(ipRow,2), olResult);
		}
		//Februar
		lValue1 = 0;
		lValue2 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl02);
		if(pomAcc2 != NULL)
			lValue2 = atof(pomAcc2->Cl02);
		lResult = lValue1 - lValue2;
		if(fabs(lResult) > 0){
			olResult.Format("%.2f", lResult);
			pomAccount->SetValueRange(CGXRange(ipRow,3), olResult);
		}
		//M�rz
		lValue1 = 0;
		lValue2 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl03);
		if(pomAcc2 != NULL)
			lValue2 = atof(pomAcc2->Cl03);
		lResult = lValue1 - lValue2;
		if(fabs(lResult) > 0){
			olResult.Format("%.2f", lResult);
			pomAccount->SetValueRange(CGXRange(ipRow,4), olResult);
		}
		//April
		lValue1 = 0;
		lValue2 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl04);
		if(pomAcc2 != NULL)
			lValue2 = atof(pomAcc2->Cl04);
		lResult = lValue1 - lValue2;
		if(fabs(lResult) > 0){
			olResult.Format("%.2f", lResult);
			pomAccount->SetValueRange(CGXRange(ipRow,5), olResult);
		}
		//Mai
		lValue1 = 0;
		lValue2 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl05);
		if(pomAcc2 != NULL)
			lValue2 = atof(pomAcc2->Cl05);
		lResult = lValue1 - lValue2;
		if(fabs(lResult) > 0){
			olResult.Format("%.2f", lResult);
			pomAccount->SetValueRange(CGXRange(ipRow,6), olResult);
		}
		//Juni
		lValue1 = 0;
		lValue2 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl06);
		if(pomAcc2 != NULL)
			lValue2 = atof(pomAcc2->Cl06);
		lResult = lValue1 - lValue2;
		if(fabs(lResult) > 0){
			olResult.Format("%.2f", lResult);
			pomAccount->SetValueRange(CGXRange(ipRow,7), olResult);
		}
		//Juli
		lValue1 = 0;
		lValue2 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl07);
		if(pomAcc2 != NULL)
			lValue2 = atof(pomAcc2->Cl07);
		lResult = lValue1 - lValue2;
		if(fabs(lResult) > 0){
			olResult.Format("%.2f", lResult);
			pomAccount->SetValueRange(CGXRange(ipRow,8), olResult);
		}
		//August
		lValue1 = 0;
		lValue2 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl08);
		if(pomAcc2 != NULL)
			lValue2 = atof(pomAcc2->Cl08);
		lResult = lValue1 - lValue2;
		if(fabs(lResult) > 0){
			olResult.Format("%.2f", lResult);
			pomAccount->SetValueRange(CGXRange(ipRow,9), olResult);
		}
		//September
		lValue1 = 0;
		lValue2 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl09);
		if(pomAcc2 != NULL)
			lValue2 = atof(pomAcc2->Cl09);
		lResult = lValue1 - lValue2;
		if(fabs(lResult) > 0){
			olResult.Format("%.2f", lResult);
			pomAccount->SetValueRange(CGXRange(ipRow,10), olResult);
		}
		//Oktober
		lValue1 = 0;
		lValue2 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl10);
		if(pomAcc2 != NULL)
			lValue2 = atof(pomAcc2->Cl10);
		lResult = lValue1 - lValue2;
		if(fabs(lResult) > 0){
			olResult.Format("%.2f", lResult);
			pomAccount->SetValueRange(CGXRange(ipRow,11), olResult);
		}
		//November
		lValue1 = 0;
		lValue2 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl11);
		if(pomAcc2 != NULL)
			lValue2 = atof(pomAcc2->Cl11);
		lResult = lValue1 - lValue2;
		if(fabs(lResult) > 0){
			olResult.Format("%.2f", lResult);
			pomAccount->SetValueRange(CGXRange(ipRow,12), olResult);
		}
		//Dezember
		lValue1 = 0;
		lValue2 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl12);
		if(pomAcc2 != NULL)
			lValue2 = atof(pomAcc2->Cl12);
		lResult = lValue1 - lValue2;
		if(fabs(lResult) > 0){
			olResult.Format("%.2f", lResult);
			pomAccount->SetValueRange(CGXRange(ipRow,13), olResult);
		}
		break;
	}
	case 24:
	{					 
		pomAcc1 = ogAccData.GetAccByKey(m_Urno, olYear, opType1);
		//Januar
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl01);
		olResult.Format("%.2f", lValue1);
		pomAccount->SetValueRange(CGXRange(ipRow,2), olResult);

		//Februar
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl02);
		olResult.Format("%.2f", lValue1);
		pomAccount->SetValueRange(CGXRange(ipRow,3), olResult);

		//M�rz
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl03);
		olResult.Format("%.2f", lValue1);
		pomAccount->SetValueRange(CGXRange(ipRow,4), olResult);

		//April
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl04);
		olResult.Format("%.2f", lValue1);
		pomAccount->SetValueRange(CGXRange(ipRow,5), olResult);

		//Mai
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl05);
		olResult.Format("%.2f", lValue1);
		pomAccount->SetValueRange(CGXRange(ipRow,6), olResult);

		//Juni
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl06);
		olResult.Format("%.2f", lValue1);
		pomAccount->SetValueRange(CGXRange(ipRow,7), olResult);

		//Juli
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl07);
		olResult.Format("%.2f", lValue1);
		pomAccount->SetValueRange(CGXRange(ipRow,8), olResult);

		//August
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl08);
		olResult.Format("%.2f", lValue1);
		pomAccount->SetValueRange(CGXRange(ipRow,9), olResult);

		//September
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl09);
		olResult.Format("%.2f", lValue1);
		pomAccount->SetValueRange(CGXRange(ipRow,10), olResult);

		//Oktober
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl10);
		olResult.Format("%.2f", lValue1);
		pomAccount->SetValueRange(CGXRange(ipRow,11), olResult);

		//November
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl11);
		olResult.Format("%.2f", lValue1);
		pomAccount->SetValueRange(CGXRange(ipRow,12), olResult);

		//Dezember
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl12);
		olResult.Format("%.2f", lValue1);
		pomAccount->SetValueRange(CGXRange(ipRow,13), olResult);

		break;
	}
	case 26:
	{					 
		pomAcc1 = ogAccData.GetAccByKey(m_Urno, olYear, opType1);
		//Januar
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl01);
		if(lValue1 > 0){
			olResult.Format("%.2f", lValue1);
			pomAccount->SetValueRange(CGXRange(ipRow,2), olResult);
		}
		//Februar
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl02);
		if(lValue1 > 0){
			olResult.Format("%.2f", lValue1);
			pomAccount->SetValueRange(CGXRange(ipRow,3), olResult);
		}
		//M�rz
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl03);
		if(lValue1 > 0){
			olResult.Format("%.2f", lValue1);
			pomAccount->SetValueRange(CGXRange(ipRow,4), olResult);
		}
		//April
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl04);
		if(lValue1 > 0){
			olResult.Format("%.2f", lValue1);
			pomAccount->SetValueRange(CGXRange(ipRow,5), olResult);
		}
		//Mai
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl05);
		if(lValue1 > 0){
			olResult.Format("%.2f", lValue1);
			pomAccount->SetValueRange(CGXRange(ipRow,6), olResult);
		}
		//Juni
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl06);
		if(lValue1 > 0){
			olResult.Format("%.2f", lValue1);
			pomAccount->SetValueRange(CGXRange(ipRow,7), olResult);
		}
		//Juli
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl07);
		if(lValue1 > 0){
			olResult.Format("%.2f", lValue1);
			pomAccount->SetValueRange(CGXRange(ipRow,8), olResult);
		}
		//August
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl08);
		if(lValue1 > 0){
			olResult.Format("%.2f", lValue1);
			pomAccount->SetValueRange(CGXRange(ipRow,9), olResult);
		}
		//September
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl09);
		if(lValue1 > 0){
			olResult.Format("%.2f", lValue1);
			pomAccount->SetValueRange(CGXRange(ipRow,10), olResult);
		}
		//Oktober
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl10);
		if(lValue1 > 0){
			olResult.Format("%.2f", lValue1);
			pomAccount->SetValueRange(CGXRange(ipRow,11), olResult);
		}
		//November
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl11);
		if(lValue1 > 0){
			olResult.Format("%.2f", lValue1);
			pomAccount->SetValueRange(CGXRange(ipRow,12), olResult);
		}
		//Dezember
		lValue1 = 0;
		if(pomAcc1 != NULL)
			lValue1 = atof(pomAcc1->Cl12);
		if(lValue1 > 0){
			olResult.Format("%.2f", lValue1);
			pomAccount->SetValueRange(CGXRange(ipRow,13), olResult);
		}
		break;
	}
	default:
		break;
	}
CCS_CATCH_ALL
}


/////////////////////////////////////////////////////////////////////////////
// Printing support virtual functions (others in viewpr.cpp)

void CStaffBalanceDlg::OnPrint(CDC* pDC, CPrintInfo* pInfo)
{
	pomAccount->OnPrint(pDC, pInfo);
}

void CStaffBalanceDlg::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo, long nZoom)
{
	if (pInfo)
	{
		// Adjust printer DC
		pDC->SetMapMode(MM_ANISOTROPIC);
		pDC->SetWindowExt(96, 96);

		int nlogx = pDC->GetDeviceCaps(LOGPIXELSX);
		int nlogy = pDC->GetDeviceCaps(LOGPIXELSY);

		pDC->SetViewportExt(nlogx, nlogy);

		// Mapping: 72 pixels/unit (like MM_TEXT)
	}
}

BOOL CStaffBalanceDlg::OnPreparePrinting(CPrintInfo* pInfo)
{
	pInfo->SetMaxPage(0xffff);

	pInfo->m_pPD->m_pd.Flags &= ~PD_NOSELECTION;
	pInfo->m_pPD->m_pd.hInstance = AfxGetInstanceHandle();
	
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CStaffBalanceDlg::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	pomAccount->OnBeginPrinting(pDC, pInfo);
}

void CStaffBalanceDlg::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	pomAccount->OnEndPrinting(pDC, pInfo);
}

void CStaffBalanceDlg::OnButtonPrint() 
{
	// TODO: Add your control notification handler code here
	OnFilePrint();
}

void CStaffBalanceDlg::Register()
{
	ogDdx.Register(this, TDA_CHANGE, CString("CStaffBalanceDlg"), CString("TDA_CHANGE"), ProcessBC);
	ogDdx.Register(this, TDA_NEW,	 CString("CStaffBalanceDlg"), CString("TDA_NEW"),	  ProcessBC);
	ogDdx.Register(this, TDA_DELETE, CString("CStaffBalanceDlg"), CString("TDA_DELETE"), ProcessBC);
}

void CStaffBalanceDlg::ProcessTdaNew(TDADATA *prpTda)
{
CCS_TRY
	ACCDATA *pomAcc;
	CString olYear, olResult;
	int ilAccount, ilMonth;
	double lClose, lPlan, lWorked;

	ilMonth = m_Date.GetMonth();

	if(prpTda == NULL)
		return;

	if(prpTda->Idur != m_RefUrno)
		return;

	//Grid mit Daten f�llen.
	pomAccount->GetParam()->SetLockReadOnly(false);

	ilAccount = atoi(prpTda->Desr);
	switch(ilAccount){
		case 26:
		{
			olResult.Format("%.2f", atof(prpTda->Valu));
			pomAccount->SetValueRange(CGXRange(8,ilMonth + 1), olResult);	
			olYear = m_Date.Format("%Y");
			pomAcc = NULL;
			pomAcc = ogAccData.GetAccByKey(m_Urno, olYear, "26");
			if(pomAcc != NULL){
				switch(ilMonth){
					case 1:
					{
						lClose = atof(pomAcc->Cl01);
						break;
					}
					case 2:
					{
						lClose = atof(pomAcc->Cl02);
						break;
					}
					case 3:
					{
						lClose = atof(pomAcc->Cl03);
						break;
					}
					case 4:
					{
						lClose = atof(pomAcc->Cl04);
						break;
					}
					case 5:
					{
						lClose = atof(pomAcc->Cl05);
						break;
					}
					case 6:
					{
						lClose = atof(pomAcc->Cl06);
						break;
					}
					case 7:
					{
						lClose = atof(pomAcc->Cl07);
						break;
					}
					case 8:
					{
						lClose = atof(pomAcc->Cl08);
						break;
					}
					case 9:
					{
						lClose = atof(pomAcc->Cl09);
						break;
					}
					case 10:
					{
						lClose = atof(pomAcc->Cl10);
						break;
					}
					case 11:
					{
						lClose = atof(pomAcc->Cl11);
						break;
					}
					case 12:
					{
						lClose = atof(pomAcc->Cl12);
						break;
					}
					default:
						break;
				}
				
				lWorked = atof(prpTda->Valu);
				lPlan = lClose - lWorked;
				olResult.Format("%.2f", lPlan);
				pomAccount->SetValueRange(CGXRange(7,ilMonth + 1), olResult);

				//wir m�ssen "Balance Hours per Month" auch noch neu berechnen...
				/*double lNormStd, lGeleisteteStd, lAusgleichStd, lPlanStd;
				CString olAusgleichStd;
				lNormStd		= 0.00;
				lPlanStd		= 0.00;
				lGeleisteteStd	= 0.00;

				lNormStd		= atof(pomAccount->GetValueRowCol(6,ilMonth + 1));
				lPlanStd		= atof(pomAccount->GetValueRowCol(7,ilMonth + 1));
				lGeleisteteStd	= atof(pomAccount->GetValueRowCol(8,ilMonth + 1));

				lAusgleichStd	= (lPlanStd + lGeleisteteStd) - lNormStd;
				olAusgleichStd.Format("%.2f", lAusgleichStd);

				pomAccount->SetValueRange(CGXRange(9,ilMonth + 1), olAusgleichStd);

				//... und auch noch die kumulativen Werte
				CalcAccounts("51", 10);*/
			}
			break;
		}
		case 110:
		{
			olResult.Format("%.2f", atof(prpTda->Valu));
			pomAccount->SetValueRange(CGXRange(12,ilMonth + 1), olResult);		
			break;
		}
		case 116:
		{
			olResult.Format("%.2f", atof(prpTda->Valu));
			pomAccount->SetValueRange(CGXRange(13,ilMonth + 1), olResult);
			break;
		}
		case 135:
		{
			olResult.Format("%.2f", atof(prpTda->Valu));
			pomAccount->SetValueRange(CGXRange(14,ilMonth + 1), olResult);
			break;
		}
		case 137:
		{
			olResult.Format("%.2f", atof(prpTda->Valu));
			pomAccount->SetValueRange(CGXRange(15,ilMonth + 1), olResult);
			break;
		}
		case 139:
		{
			olResult.Format("%.2f", atof(prpTda->Valu));
			pomAccount->SetValueRange(CGXRange(16,ilMonth + 1), olResult);
			break;
		}
		case 140:
		{
			olResult.Format("%.0f", atof(prpTda->Valu));
			pomAccount->SetValueRange(CGXRange(18,ilMonth + 1), olResult);
			break;
		}
		case 157:
		{
			olResult.Format("%.2f", atof(prpTda->Valu));
			pomAccount->SetValueRange(CGXRange(17,ilMonth + 1), olResult);
			break;
		}
		default:
			break;
	}

	// Anzeige einstellen
	pomAccount->GetParam()->SetLockReadOnly(true);

	ogTdaData.Delete(prpTda);

	return;
CCS_CATCH_ALL
}

void CStaffBalanceDlg::ProcessTdaDelete(TDADATA *prpTda)
{
CCS_TRY
		return;
CCS_CATCH_ALL
}

void CStaffBalanceDlg::ProcessTdaChange(TDADATA *prpTda)
{
CCS_TRY
		return;
CCS_CATCH_ALL
}