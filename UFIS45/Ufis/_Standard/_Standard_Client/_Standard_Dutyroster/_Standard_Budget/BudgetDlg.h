// BudgetDlg.h : header file
//

#if !defined(AFX_TESTAPPLDLG_H__EA4946E7_1ACF_11D4_8FA9_00010204A53B__INCLUDED_)
#define AFX_TESTAPPLDLG_H__EA4946E7_1ACF_11D4_8FA9_00010204A53B__INCLUDED_

#include <gridcontrol.h>	// Added by ClassView
#include <CedaBudData.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MINROWS 1
/////////////////////////////////////////////////////////////////////////////
// CBudgetDlg dialog

// Struktur zur Ermittlung der Anzahl gleicher Vertr�ge zur Headcount-Berechnung
struct ANZHEADS
{
	long		uhdc;	//Urno HDCTAB
	CString		whpw;	//pers�nliche Wochenstunden
	CString		suph;	//Indikator Zus�tzliche ferien
	CString		stsc;	//Statusanhang
	CString		cot;	//Arbeitsvertrag		
	CString		wrkd;	//Wochenstunden laut Arbeitsvertrag
	int			heads;	//Anzahl Vertr�ge
};


class CGridControl;

class CBudgetDlg : public CDialog
{
// Construction
public:
	CBudgetDlg(CWnd* pParent = NULL);	// standard constructor
	~CBudgetDlg();
// Dialog Data
	//{{AFX_DATA(CBudgetDlg)
	enum { IDD = IDD_BUDGET_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBudgetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void EditRow();
	CGridControl *pomOrg;
	void IniGrid();
	long GetUrno();

	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CBudgetDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnRowNew();
	afx_msg void OnRowEdit();
	afx_msg void OnRowDelete();
	afx_msg void OnRowCalculate();
	afx_msg void OnHelp();
	afx_msg void OnRowCopy();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void NewFOR(CString Dpt1, CString Yemo, CString Budg, CString Actu, long Ubud);
	void DeleteFor(long ilBudUrno);
	void GetForecast(BUDDATA *pBud);
	static void NewUMU(CString Netw, CString Lanm, CString Finm, CString Doem, CString Dodm, CString Mufr, CString Sufr, CString Muto, CString Suto, long Umut);
	int GetDaysOfMonth(COleDateTime opDay);
	void NewMUT(CString Dpt1, CString Ymdy, long Ubud);
	void DeleteMutUmu(long ilBudUrno);
	void DeleteHdcUhd(long ilBudUrno);
	void GetMutation(BUDDATA *pBud);
	void NewUHD(CString Whpw, CString Suph, CString Netw, CString Stsc, CString Head, CString Actu, long Uhdc, CString Budg, CString Diff);
	void NewHDC(CString Dpt1, CString Ymdy, long Ubud);
	void GetHeadcount(BUDDATA *pBud);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTAPPLDLG_H__EA4946E7_1ACF_11D4_8FA9_00010204A53B__INCLUDED_)
