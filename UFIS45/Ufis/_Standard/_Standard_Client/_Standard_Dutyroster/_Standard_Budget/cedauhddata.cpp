// CedaUhdData.cpp - Klasse f�r die Handhabung von Uhd-Daten (Budgetberechnung) 
//
 
#include <stdafx.h>
#include <afxwin.h>
#include <CCSCedaData.h>
#include <CedaUhdData.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <UFIS.h>
#include <CCSBcHandle.h>
#include <CCSGlobl.h>

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Globale Funktionen
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// ProcessUhdCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_UHD_CHANGE, BC_UHD_NEW und BC_UHD_DELETE. Ruft zur eingentlichen Bearbeitung
//	der Nachrichten die Funktion CedaUhdData::ProcessUhdBc() der entsprechenden 
//	Instanz von CedaUhdData auf.	
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessUhdCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Empf�nger ermitteln
	CedaUhdData *polUhdDATA = (CedaUhdData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polUhdDATA->ProcessUhdBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Implementation der Klasse CedaUhdData (Daily Roster Absences - untert�gige Abwesenheit)
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// Konstruktor
//************************************************************************************************************************************************

CedaUhdData::CedaUhdData(CString opTableName, CString opExtName) : CCSCedaData(&ogCommHandler)
{
    // Infostruktur vom Typ CEDARECINFO f�r Uhd-Datens�tze
	// Wird von Vaterklasse CCSCedaData f�r verschiedene Funktionen
	// (CCSCedaData::GetBufferRecord(), CCSCedaData::MakeCedaData())
	// benutzt. Die Datenstruktur beschreibt die Tabellenstruktur.
    BEGIN_CEDARECINFO(UHDDATA, UhdDataRecInfo)
		FIELD_LONG		(Uhdc,"UHDC")	// Urno HDCTAB
		FIELD_CHAR_TRIM	(Whpw,"WHPW")	// Wochenstunden
		FIELD_CHAR_TRIM	(Suph,"SUPH")	// Indikator Zus�tzliche Ferien
		FIELD_CHAR_TRIM	(Netw,"NETW")	// WHPW - SUPH
		FIELD_CHAR_TRIM	(Stsc,"STSC")	// Code Reduzierte Arbeitszeit
		FIELD_CHAR_TRIM	(Head,"HEAD")	// Anzahl Vertr�ge
		FIELD_CHAR_TRIM	(Actu,"ACTU")	// Umrechnung auf Personalmonate
		FIELD_CHAR_TRIM	(Budg,"BUDG")	// Wert aus Stammdaten Budget
		FIELD_CHAR_TRIM	(Diff,"DIFF")	// Differenz BUDG - ACTU
		FIELD_CHAR_TRIM	(Hopo,"HOPO")	// Hopo
		FIELD_LONG   	(Urno,"URNO")	// Urno
	END_CEDARECINFO

	// Infostruktur kopieren
    for (int i = 0; i < sizeof(UhdDataRecInfo)/sizeof(UhdDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&UhdDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	// Tabellenname per Default auf Uhd setzen
	opExtName = pcgTableExt;
	SetTableNameAndExt(opTableName+opExtName);

    // Feldnamen initialisieren
	strcpy(pcmListOfFields,"UHDC,WHPW,SUPH,NETW,STSC,HEAD,ACTU,BUDG,DIFF,HOPO,URNO");
	// Zeiger der Vaterklasse auf Tabellenstruktur setzen
	CCSCedaData::pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

    // Datenarrays initialisieren
	omUrnoMap.InitHashTable(629,117);

	// ToDo: min. / max. Datum f�r Uhds initialisieren
//	omMinDay = omMaxDay = TIMENULL;
}

//************************************************************************************************************************************************
// Destruktor
//************************************************************************************************************************************************

CedaUhdData::~CedaUhdData()
{
	ClearAll(true);
	omRecInfo.DeleteAll();
}

//************************************************************************************************************************************************
// SetTableNameAndExt: stellt den Namen und die Extension ein.
// R�ckgabe:	false	->	Tabellenname mit Extension gr��er als
//							Klassenmember
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaUhdData::SetTableNameAndExt(CString opTableAndExtName)
{
CCS_TRY
	if(opTableAndExtName.GetLength() >= sizeof(pcmTableName)) return false;
	strcpy(pcmTableName,opTableAndExtName.GetBuffer(0));
	return true;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
//	Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
//	Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
//	dann die entsprechende Nachricht bearbeitet. �ber den nach void 
//	konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
//	Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
//	und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
//	entsprechenden Member-Funktion).
// R�ckgabe: keine
//************************************************************************************************************************************************

void CedaUhdData::Register(void)
{
CCS_TRY
	// bereits angemeldet?
	if (bmIsBCRegistered) return;	// ja -> gibt nichts zu tun, terminieren

	// Uhd-Datensatz hat sich ge�ndert
	ogDdx.Register((void *)this,BC_UHD_CHANGE, CString("CedaUhdData"), CString("BC_UHD_CHANGE"),        ProcessUhdCf);
	ogDdx.Register((void *)this,BC_UHD_NEW, CString("CedaUhdData"), CString("BC_UHD_NEW"),		   ProcessUhdCf);
	ogDdx.Register((void *)this,BC_UHD_DELETE, CString("CedaUhdData"), CString("BC_UHD_DELETE"),        ProcessUhdCf);
	// jetzt ist die Instanz angemeldet
	bmIsBCRegistered = true;
CCS_CATCH_ALL
}


//************************************************************************************************************************************************
// ClearAll: aufr�umen. Alle Arrays und PointerMaps werden geleert.
// R�ckgabe:	immer true
//************************************************************************************************************************************************

bool CedaUhdData::ClearAll(bool bpUnregister)
{
CCS_TRY
    // alle Arrays leeren
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
	
	// beim BC-Handler abmelden
	if(bpUnregister && bmIsBCRegistered)
	{
		ogDdx.UnRegister(this,NOTUSED);
		// jetzt ist die Instanz angemeldet
		bmIsBCRegistered = false;
	}
    return true;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// ReadUhdByUrno: liest den Uhd-Datensatz mit der  Urno <lpUrno> aus der 
//	Datenbank.
// R�ckgabe:	boolean Datensatz erfolgreich gelesen?
//************************************************************************************************************************************************

bool CedaUhdData::ReadUhdByUrno(long lpUrno, UHDDATA *prpUhd)
{
CCS_TRY
	char pclWhere[200]="";
	sprintf(pclWhere, "%ld", lpUrno);
	// Datensatz aus Datenbank lesen
	if (CedaAction("RT", pclWhere) == false)
	{
		return false;	// Fehler -> abbrechen
	}
	// Datensatz-Objekt instanziieren...
	UHDDATA *prlUhd = new UHDDATA;
	// und initialisieren
	if (GetBufferRecord(0,prpUhd) == true)
	{  
		return true;	// Aktion erfolgreich
	}
	else
	{
		// Fehler -> aufr�umen und terminieren
		delete prlUhd;
		return false;
	}

	return false;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// Read: liest Datens�tze ein.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaUhdData::Read(char *pspWhere /*=NULL*/, CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/,
					   bool bpRegisterBC /*= true*/, bool bpClearData /*= true*/)
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	// wenn gew�nscht, aufr�umen (und beim Broadcast-Handler abmelden, wenn
	// nach Ende angemeldet werden soll)
	if (bpClearData) ClearAll(bpRegisterBC);
	
    // Datens�tze lesen
	if(pspWhere == NULL)
	{
		if (!CCSCedaData::CedaAction("RT", "")) return false;
	}
	else
	{
		if (!CCSCedaData::CedaAction("RT", pspWhere)) return false;
	}

	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// void-Pointer f�r MA-Urno-Lookup
	void  *prlVoid = NULL;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		UHDDATA *prlUhd = new UHDDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord(ilCountRecords,prlUhd);
		if (!blMoreRecords) break;
		// Datensatz gelesen, Z�hler inkrementieren
		ilCountRecords++;
		// wurde eine Datensatz gelesen und ist (wenn vorhanden) die
		// Mitarbeiter-Urno dieses Datensatzes im MA-Urno-Array enthalten? 
		/*if(blMoreRecords && 
		   ((popLoadStfUrnoMap == NULL) || 
			(popLoadStfUrnoMap->Lookup((void *)prlUhd->Stfu, (void *&)prlVoid) == TRUE)))
		{*/ 
			// Datensatz hinzuf�gen
			if (!InsertInternal(prlUhd, UHD_NO_SEND_DDX)){
				// Fehler -> lokalen Datensatz l�schen
				delete prlUhd;
			}
		/*}
		else
		{
			// kein weiterer Datensatz
			delete prlUhd;
		}*/
	} while (blMoreRecords);
	
	// beim Broadcast-Handler anmelden, wenn gew�nscht
	if (bpRegisterBC) Register();

	// Test: Anzahl der gelesenen Uhd
	TRACE("Read-Uhd: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// Insert: speichert den Datensatz <prpUhd> in der Datenbank und schickt einen
//	Broadcast ab.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaUhdData::Insert(UHDDATA *prpUhd, bool bpSave /*= true*/)
{
CCS_TRY
	// �nderungs-Flag setzen
	prpUhd->IsChanged = DATA_NEW;
	// in Datenbank speichern, wenn erw�nscht
	if(bpSave && Save(prpUhd) == false) return false;
	// Broadcast UHD_NEW abschicken
	InsertInternal(prpUhd,true);
    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// InsertInternal: f�gt den internen Arrays einen Datensatz hinzu. Der Datensatz
//	muss sp�ter oder vorher explizit durch Aufruf von Save() oder Release() in der Datenbank 
//	gespeichert werden. Wenn <bpSendDdx> true ist, wird ein Broadcast gesendet.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaUhdData::InsertInternal(UHDDATA *prpUhd, bool bpSendDdx)
{
CCS_TRY
	// Datensatz intern anf�gen
	omData.Add(prpUhd);
	// Datensatz der Urno-Map hinzuf�gen
	omUrnoMap.SetAt((void *)prpUhd->Urno,prpUhd);
		
	// Broadcast senden?
	if( bpSendDdx == true )
	{
		// ja -> go!
		ogDdx.DataChanged((void *)this,UHD_NEW,(void *)prpUhd);
	}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// Update: sucht den Datensatz mit der Urno <prpUhd->Urno> und speichert ihn
// in der Datenbank.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaUhdData::Update(UHDDATA *prpUhd,bool bpSave /*= true*/)
{
CCS_TRY
	// Datensatz raussuchen
	if (GetUhdByUrno(prpUhd->Urno) != NULL)
	{
		// �nderungsflag setzen
		if (prpUhd->IsChanged == DATA_UNCHANGED)
		{
			prpUhd->IsChanged = DATA_CHANGED;
		}
		// in die Datenbank speichern
		if(bpSave && Save(prpUhd) == false) return false; 
		// Broadcast Bud_CHANGE versenden
		UpdateInternal(prpUhd);
	}
    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// UpdateInternal: �ndert einen Datensatz, der in der internen Datenhaltung
//	enthalten ist.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaUhdData::UpdateInternal(UHDDATA *prpUhd, bool bpSendDdx /*true*/)
{
CCS_TRY
// APO 9.12.99 Updates passieren immer auf Kopie eines Datensatzes der internen
// Datenhaltung, kopieren daher unn�tig.
/*	// Uhd in lokaler Datenhaltung suchen
	UHDDATA *prlUhd = GetUhdByKey(prpUhd->Sday,prpUhd->Stfu,prpUhd->Budn,prpUhd->Rosl);
	// Uhd gefunden?
	if (prlUhd != NULL)
	{
		// ja -> durch Zeiger ersetzen
		*prlUhd = *prpUhd;
	}
*/
	// Broadcast senden, wenn gew�nscht
	if( bpSendDdx == true )
	{
//		TRACE("\nSending %d",UHD_CHANGE);
		ogDdx.DataChanged((void *)this,UHD_CHANGE,(void *)prpUhd);
	}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// DeleteInternal: l�scht einen Datensatz aus den internen Arrays.
// R�ckgabe:	keine
//*********************************************************************************************

void CedaUhdData::DeleteInternal(UHDDATA *prpUhd, bool bpSendDdx /*true*/)
{
CCS_TRY
	// zuerst Broadcast senden, damit alle Module reagieren k�nnen BEVOR
	// der Datensatz gel�scht wird (synchroner Mechanismus)
	if(bpSendDdx == true)
	{
		ogDdx.DataChanged((void *)this,UHD_DELETE,(void *)prpUhd);
	}

	// Urno-Schl�ssel l�schen
	omUrnoMap.RemoveKey((void *)prpUhd->Urno);

	// Datensatz l�schen
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpUhd->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}
CCS_CATCH_ALL
}

//*********************************************************************************************
// Delete: markiert den Datensatz <prpUhd> als gel�scht und l�scht den Datensatz
//	aus der internen Datenhaltung und der Datenbank. 
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaUhdData::Delete(UHDDATA *prpUhd, bool bpWithSave /* true*/)
{
CCS_TRY
	// Flag setzen
	prpUhd->IsChanged = DATA_DELETED;
	
	// speichern
	if(bpWithSave == TRUE)
	{
		if (!Save(prpUhd)) return false;
	}

	// aus der internen Datenhaltung l�schen
	DeleteInternal(prpUhd,true);

	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// ProcessUhdBc: behandelt die einlaufenden Broadcasts.
// R�ckgabe:	keine
//*********************************************************************************************

void  CedaUhdData::ProcessUhdBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY
	// Performance-Trace
	SYSTEMTIME rlPerf;
	GetSystemTime(&rlPerf);
	TRACE("CedaUhdData::ProcessUhdBc: START %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);

	// Datensatz-Urno
	long llUrno;
	// Daten und Info
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;
	UHDDATA *prlUhd = NULL;

	switch(ipDDXType)
	{
	case BC_UHD_NEW: // neuen Datensatz einf�gen
		{
			// einzuf�gender Datensatz
			prlUhd = new UHDDATA;
			GetRecordFromItemList(prlUhd,prlBcStruct->Fields,prlBcStruct->Data);
			InsertInternal(prlUhd, UHD_SEND_DDX);
		}
		break;
	case BC_UHD_CHANGE:	// Datensatz �ndern
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlUhd = GetUhdByUrno(llUrno);
			if(prlUhd != NULL)
			{
				// ja -> Datensatz aktualisieren
				GetRecordFromItemList(prlUhd,prlBcStruct->Fields,prlBcStruct->Data);
				UpdateInternal(prlUhd);
			}
			// nein -> Datensatz interessiert uns nicht
		}
		break;
	case BC_UHD_DELETE:	// Datensatz l�schen
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlUhd = GetUhdByUrno(llUrno);
			if (prlUhd != NULL)
			{
				// ja -> Datensatz l�schen
				DeleteInternal(prlUhd);
			}
		}
		break;
	default:
		break;
	}
	// Performance-Test
	GetSystemTime(&rlPerf);
	TRACE("CedaUhdData::ProcessUhdBc: END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
CCS_CATCH_ALL
}

//*********************************************************************************************
// GetUhdByUrno: sucht den Datensatz mit der Urno <lpUrno>.
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

UHDDATA *CedaUhdData::GetUhdByUrno(long lpUrno)
{
CCS_TRY
	// der Datensatz
	UHDDATA *prpUhd;
	// Datensatz in Urno-Map suchen
	if (omUrnoMap.Lookup((void*)lpUrno,(void *& )prpUhd) == TRUE)
	{
		return prpUhd;	// gefunden -> Zeiger zur�ckgeben
	}
	// nicht gefunden -> R�ckgabe NULL
	return NULL;
CCS_CATCH_ALL
return NULL;
}

//*******************************************************************************
// Save: speichert den Datensatz <prpUhd>.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************

bool CedaUhdData::Save(UHDDATA *prpUhd)
{
CCS_TRY
	bool olRc = true;
	CString olListOfData;
	char pclSelection[1024];
	char pclData[2048];

	if (prpUhd->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpUhd->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpUhd);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpUhd->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpUhd->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpUhd);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpUhd->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpUhd->Urno);
		olRc = CedaAction("DRT",pclSelection);
		break;
	}
	// R�ckgabe: Ergebnis der CedaAction
	return olRc;
CCS_CATCH_ALL
return false;
}

//*************************************************************************************
// CedaAction: ruft die Funktion CedaAction der Basisklasse CCSCedaData auf.
//	Alle Parameter werden durchgereicht. Aufgrund eines internen CEDA-Bugs,
//	der daf�r sorgen kann, dass die Tabellenbeschreibung <pcmListOfFields>
//	zerst�rt wird, muss sicherheitshalber <pcmListOfFields> vor jedem Aufruf
//	von CCSCedaData::CedaAction() gerettet und danach wiederhergestellt werden.
// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
//*************************************************************************************

bool CedaUhdData::CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
							 char *pcpSelection, char *pcpSort, char *pcpData, 
							 char *pcpDest /*= "BUF1"*/,bool bpIsWriteAction /*= false*/, 
							 long lpID /*= 0*/, CCSPtrArray<RecordSet> *pomData /*= NULL*/, 
							 int ipFieldCount /*= 0*/)
{
CCS_TRY
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CCSCedaData::CedaAction(pcpAction,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL
return false;
}

//*************************************************************************************
// CedaAction: kurze Version, Beschreibung siehe oben.
// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
//*************************************************************************************

bool CedaUhdData::CedaAction(char *pcpAction, char *pcpSelection, char *pcpSort,
							 char *pcpData, char *pcpDest /*"BUF1"*/, 
							 bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{
CCS_TRY
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CCSCedaData::CedaAction(pcpAction,CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,(pcpSelection != NULL)? pcpSelection: CCSCedaData::pcmSelection,(pcpSort != NULL)? pcpSort: CCSCedaData::pcmSort,pcpData,pcpDest,bpIsWriteAction, lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// CopyUhdValues: Kopiert die "Nutzdaten" (keine urnos usw.)
// R�ckgabe:	keine
//************************************************************************************************************************************************

void CedaUhdData::CopyUhd(UHDDATA* popUhdDataSource,UHDDATA* popUhdDataTarget)
{
	// Daten kopieren
	/*strcpy(popUhdDataTarget->Rema,popUhdDataSource->Rema);
	strcpy(popUhdDataTarget->Sdac,popUhdDataSource->Sdac);
	popUhdDataTarget->Abfr = popUhdDataSource->Abfr;
	popUhdDataTarget->Abto = popUhdDataSource->Abto;*/
}

bool CedaUhdData::ReadUhdData()
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	ClearAll(true);
	
    if (!CCSCedaData::CedaAction("RT", "")) return false;
	
	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		UHDDATA *prlUhd = new UHDDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord(ilCountRecords,prlUhd);
		// Datensatz gelesen, Z�hler inkrementieren
		if (!blMoreRecords) break;
		ilCountRecords++;
		if (!InsertInternal(prlUhd, UHD_NO_SEND_DDX)){
			// Fehler -> lokalen Datensatz l�schen
			delete prlUhd;
		}
	} while (blMoreRecords);
		
	// Test: Anzahl der gelesenen Uhd
	TRACE("Read-Uhd: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}

