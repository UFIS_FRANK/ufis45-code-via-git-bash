#ifndef _CedaUmuData_H_
#define _CedaUmuData_H_
 
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccsglobl.h>
//#include "CedaDrrData.h" // f�r G�ltigkeitspr�fungen

/////////////////////////////////////////////////////////////////////////////

// COMMANDS FOR MEMBER-FUNCTIONS
#define UMU_SEND_DDX	(true)
#define UMU_NO_SEND_DDX	(false)

// Felddimensionen
#define UMU_NETW_LEN	(4)
#define UMU_LANM_LEN	(40)
#define UMU_FINM_LEN	(30)
#define UMU_DOEM_LEN	(8)
#define UMU_SUFR_LEN	(1)
#define UMU_HOPO_LEN	(3)

// Struktur eines Umu-Datensatzes
struct UMUDATA {
	long			Umut;					//Urno MUTTAB
	char			Netw[UMU_NETW_LEN+2];	//Wochenstunden - Zus�tzliche Ferien
	char			Lanm[UMU_LANM_LEN+2];	//Nachname
	char			Finm[UMU_FINM_LEN+2];	//Vorname
	char			Doem[UMU_DOEM_LEN+2];	//Entrance
	char        	Dodm[UMU_DOEM_LEN+2];	//Leaving
	char			Mufr[UMU_DOEM_LEN+2];	//Bisheriger Wert
	char			Sufr[UMU_SUFR_LEN+2];	//Bisheriger Wert Zus�tzliche Ferien
	char			Muto[UMU_DOEM_LEN+2];	//Neuer Wert
	char			Suto[UMU_SUFR_LEN+2];	//Neuer Wert Zus�tzliche Ferien
	char			Hopo[UMU_HOPO_LEN+2];	//Hopo
	long			Urno;					//Urno;

	//DataCreated by this class
	int			IsChanged;	// Check whether Data has Changed f�r Relaese

	// Initialisation
	UMUDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
		//Abfr.SetStatus(COleDateTime::invalid);		// Abweichung von
		//Abto.SetStatus(COleDateTime::invalid);		// Abweichung bis
	}
};	

// the broadcast CallBack function, has to be outside the CedaUmuData class
void ProcessUmuCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//************************************************************************************************************************
//************************************************************************************************************************
// Deklaration CedaUmuData: kapselt den Zugriff auf die Tabelle Umu (Daily Roster
//	Absences - untert�gige Abwesenheit)
//************************************************************************************************************************
//************************************************************************************************************************

class CedaUmuData: public CCSCedaData
{
// Funktionen
public:
	bool ReadUmuData();
    // Konstruktor/Destruktor
	CedaUmuData(CString opTableName = "UMU", CString opExtName = "TAB");
	~CedaUmuData();

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

	// die geladenen Datens�tze
	CCSPtrArray<UMUDATA> omData;

	// allgemeine Funktionen
	// Feldliste lesen
	CString GetFieldList(void) {return CString(pcmListOfFields);}

	// Broadcasts empfangen und bearbeiten
	// behandelt die Broadcasts BC_UMU_CHANGE,BC_UMU_DELETE und BC_UMU_NEW
	void ProcessUmuBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Lesen/Schreiben von Datens�tzen
	// Datens�tze einlesen
	bool Read(char *pspWhere = NULL, CMapPtrToPtr *popLoadStfUrnoMap = NULL,
			  bool bpRegisterBC = true, bool bpClearData = true);
	// Datensatz mit bestimmter Urno einlesen
	bool ReadUmuByUrno(long lpUrno, UMUDATA *prpUmu);
	
	// einen Datensatz aus der Datenbank l�schen
	bool Delete(UMUDATA *prpUmu, bool bpWithSave = true);
	// einen neuen Datensatz in der Datenbank speichern
	bool Insert(UMUDATA *prpUmu, bool bpSave = true);
	// einen ge�nderten Datensatz speichern
	bool Update(UMUDATA *prpUmu, bool bpSave = true);

	// Datens�tze suchen
	// Umu nach Urno suchen
	UMUDATA* GetUmuByUrno(long lpUrno);
	
	// Manipulation von Datens�tzen
	// kopiert die Feldwerte eines Umu-Datensatzes ohne die Schl�sselfelder (Urno, etc.)
	void CopyUmu(UMUDATA* popUmuDataSource,UMUDATA* popUmuDataTarget);

protected:	
	// Funktionen
	// allgemeine Funktionen
	// Tabellenname einstellen
	bool SetTableNameAndExt(CString opTableAndExtName);
	// Feldliste schreiben
	void SetFieldList(CString opFieldList) {strcpy(pcmListOfFields,opFieldList.GetBuffer(0));}
	// alle internen Arrays zur Datenhaltung leeren und beim BC-Handler abmelden
	bool ClearAll(bool bpUnregister = true);

	// Broadcasts empfangen und bearbeiten
	// beim Broadcast-Handler anmelden
	void Register(void);

	// Kommunikation mit CEDA
	// Kommandos an CEDA senden (die Funktionen der Basisklasse werden �berschrieben
	// wegen des Feldlisten Bugs)
	bool CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
					char *pcpSelection, char *pcpSort, char *pcpData, 
					char *pcpDest = "BUF1",bool bpIsWriteAction = false, 
					long lpID = 0, CCSPtrArray<RecordSet> *pomData = NULL, 
					int ipFieldCount = 0);
    bool CedaAction(char *pcpAction, char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf, char *pcpDest = "BUF1", 
					bool bpIsWriteAction = false, long lpID = 0);
	
	// Datens�tze bearbeiten/speichern
	// speichert einen einzelnen Datensatz
	bool Save(UMUDATA *prpUmu);
	// einen Broadcast UMU_NEW abschicken und den Datensatz in die interne Datenhaltung aufnehmen
	bool InsertInternal(UMUDATA *prpUmu, bool bpSendDdx);
	// einen Broadcast UMU_CHANGE versenden
	bool UpdateInternal(UMUDATA *prpUmu, bool bpSendDdx = true);
	// einen Datensatz aus der internen Datenhaltung l�schen und einen Broadcast senden
	void DeleteInternal(UMUDATA *prpUmu, bool bpSendDdx = true);

	// Daten
    // Map mit den Datensatz-Urnos der geladenen Datens�tze
	CMapPtrToPtr omUrnoMap;
	// speichert die Feldnamen der Tabelle in sequentieller Form mit Trennzeichen Komma
	char pcmListOfFields[1024];
	// beim Broadcast-Handler angemeldet?
	bool bmIsBCRegistered;

	// ToDo: Minimale und maximales G�ltigkeitsdatum einbauen, damit nicht
	// unn�tig viele Datens�tze intern gehalten werden. Die min.-max.-Daten
	// m�ssen bei jeder Leseaktion (ReadXXX) gepr�ft und wenn n�tig erweitert
	// werden. Wenn dann BCs einlaufen (REL,NEW), kann gepr�ft werden ob
	// die Datens�tze in den BCs �berhaupt relevant sind.

	// min. und max. Tag der Ansichts-relevanten Umus
	//	COleDateTime omMinDay;
	//	COleDateTime omMaxDay;
};

#endif	// _CedaUmuData_H_
