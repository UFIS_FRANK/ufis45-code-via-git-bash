# Microsoft Developer Studio Project File - Name="Budget" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Budget - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Budget.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Budget.mak" CFG="Budget - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Budget - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Budget - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName "Budget"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Budget - Win32 Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "C:\Ufis_Bin\Release"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\Budget\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I ".\\" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 C:\Ufis_Bin\Release\ufis32.lib C:\Ufis_Bin\ClassLib\Release\ccsclass.lib Wsock32.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "Budget - Win32 Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "C:\Ufis_Bin\Debug"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\Budget\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "./" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 C:\Ufis_Bin\Debug\ufis32.lib C:\Ufis_Bin\ClassLib\debug\ccsclass.lib Wsock32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "Budget - Win32 Release"
# Name "Budget - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\basicdata.cpp
# End Source File
# Begin Source File

SOURCE=.\Budget.cpp
# End Source File
# Begin Source File

SOURCE=.\Budget.rc
# End Source File
# Begin Source File

SOURCE=.\BudgetDataDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\BudgetDataDlg.h
# End Source File
# Begin Source File

SOURCE=.\BudgetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ccsglobl.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSParam.cpp
# End Source File
# Begin Source File

SOURCE=.\cedabuddata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedacfgdata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedacotdata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedadatahelper.cpp
# End Source File
# Begin Source File

SOURCE=.\cedafordata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedahdcdata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedainitmodudata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedamutdata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedaorgdata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedascodata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedasordata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedastfdata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedaubudata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedauhddata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedaumudata.cpp
# End Source File
# Begin Source File

SOURCE=.\cviewer.cpp
# End Source File
# Begin Source File

SOURCE=.\gridcontrol.cpp
# End Source File
# Begin Source File

SOURCE=.\GUILng.cpp
# End Source File
# Begin Source File

SOURCE=.\initialloaddlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ListBoxDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\logindlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MakeHelp.bat
# End Source File
# Begin Source File

SOURCE=.\privlist.cpp
# End Source File
# Begin Source File

SOURCE=.\registerdlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\WaitDlg.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\basicdata.h
# End Source File
# Begin Source File

SOURCE=.\Budget.h
# End Source File
# Begin Source File

SOURCE=.\hlp\Budget.hm
# End Source File
# Begin Source File

SOURCE=.\BudgetDlg.h
# End Source File
# Begin Source File

SOURCE=.\ccsglobl.h
# End Source File
# Begin Source File

SOURCE=.\CCSParam.h
# End Source File
# Begin Source File

SOURCE=.\cedabuddata.h
# End Source File
# Begin Source File

SOURCE=.\cedacfgdata.h
# End Source File
# Begin Source File

SOURCE=.\cedacotdata.h
# End Source File
# Begin Source File

SOURCE=.\cedadatahelper.h
# End Source File
# Begin Source File

SOURCE=.\cedafordata.h
# End Source File
# Begin Source File

SOURCE=.\cedahdcdata.h
# End Source File
# Begin Source File

SOURCE=.\cedainitmodudata.h
# End Source File
# Begin Source File

SOURCE=.\cedamutdata.h
# End Source File
# Begin Source File

SOURCE=.\cedaorgdata.h
# End Source File
# Begin Source File

SOURCE=.\cedascodata.h
# End Source File
# Begin Source File

SOURCE=.\cedasordata.h
# End Source File
# Begin Source File

SOURCE=.\cedastfdata.h
# End Source File
# Begin Source File

SOURCE=.\cedaubudata.h
# End Source File
# Begin Source File

SOURCE=.\cedauhddata.h
# End Source File
# Begin Source File

SOURCE=.\cedaumudata.h
# End Source File
# Begin Source File

SOURCE=.\cviewer.h
# End Source File
# Begin Source File

SOURCE=.\gridcontrol.h
# End Source File
# Begin Source File

SOURCE=.\GUILng.h
# End Source File
# Begin Source File

SOURCE=.\initialloaddlg.h
# End Source File
# Begin Source File

SOURCE=.\ListBoxDlg.h
# End Source File
# Begin Source File

SOURCE=.\logindlg.h
# End Source File
# Begin Source File

SOURCE=.\privlist.h
# End Source File
# Begin Source File

SOURCE=.\registerdlg.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\WaitDlg.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\abb.bmp
# End Source File
# Begin Source File

SOURCE=.\res\appl.ico
# End Source File
# Begin Source File

SOURCE=.\res\Budget.ico
# End Source File
# Begin Source File

SOURCE=.\res\Budget.rc2
# End Source File
# Begin Source File

SOURCE=.\res\Exclamat.bmp
# End Source File
# Begin Source File

SOURCE=.\res\MonoUfisDE.bmp
# End Source File
# Begin Source File

SOURCE=.\res\UfisLogin.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\hlp\AfxDlg.rtf
# End Source File
# Begin Source File

SOURCE=.\hlp\Budget.log
# End Source File
# Begin Source File

SOURCE=".\cedamutdata,cpp"
# End Source File
# Begin Source File

SOURCE=.\res\filecopy.avi
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=.\hlp\TESTAPPL.HLP
# End Source File
# End Target
# End Project
