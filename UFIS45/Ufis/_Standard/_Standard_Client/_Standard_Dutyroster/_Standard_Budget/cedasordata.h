// CedaSorData.h

#ifndef __CEDASORDATA__
#define __CEDASORDATA__
 
#include <stdafx.h>
#include <CedaStfData.h>
#include <basicdata.h>
#include <afxdisp.h>

// COMMANDS FOR MEMBER-FUNCTIONS
#define SOR_SEND_DDX	(true)
#define SOR_NO_SEND_DDX	(false)

//---------------------------------------------------------------------------------------------------------
struct NEWORG
{
	CString			Org;
	CString			Vpfr;
};

struct SORDATA 
{
	long			Urno;
	long			Surn;
	char			Code[5+1];
	COleDateTime	Vpfr;
	COleDateTime	Vpto;
	char			Lead[1+1];
	char			Odgc[8+1];

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	SORDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Vpfr.SetStatus(COleDateTime::invalid);
		Vpto.SetStatus(COleDateTime::invalid);
	}

}; // end SORDataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaSorData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<SORDATA> omData;

	char pcmListOfFields[2048];
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

// Operations
public:
	bool ReadSorData();
    CedaSorData();
	~CedaSorData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(SORDATA *prpSor);
	bool InsertInternal(SORDATA *prpSor);
	bool Update(SORDATA *prpSor);
	bool UpdateInternal(SORDATA *prpSor);
	bool Delete(long lpUrno);
	bool DeleteInternal(SORDATA *prpSor);
	bool ReadSpecialData(CCSPtrArray<SORDATA> *popSor,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(SORDATA *prpSor);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	SORDATA *GetSorByUrno(long lpUrno);
	void GetSorBySurnWithTime(long lpSurn,CTime opStart,CTime opEnd,CCSPtrArray<SORDATA> *popSorData);
	void CedaSorData::GetSorBySurn(long lpSurn,CCSPtrArray<SORDATA> *popSorData);
	CString GetOrgBySurn(long lpSurn, COleDateTime opDate);
	CString GetOrgBySurnWithTime(long lpSurn, COleDateTime opDate, CCSPtrArray<SORDATA> *popSorData);
	CString GetOrgWithTime(COleDateTime opDate, CCSPtrArray<SORDATA> *popSorData);
	CString GetSorByOrgWithTime(CStringArray *popOrg, COleDateTime opStart, 
								COleDateTime opEnd, CCSPtrArray<SORDATA> *popSorData);
	CString GetEintritteByOrgWithTime(CStringArray *popOrg, COleDateTime opStart, 
								COleDateTime opEnd, CCSPtrArray<SORDATA> *popSorData,
								CMapStringToString *popNewOrg);
	CString GetAustritteByOrgWithTime(CStringArray *popOrg, COleDateTime opStart, 
								COleDateTime opEnd, CCSPtrArray<SORDATA> *popSorData,
								CMapStringToOb *popNewOrg);
	CString GetStfWithoutOrgWithTime(COleDateTime opStart, COleDateTime opEnd, CCSPtrArray<STFDATA> *popStfData);

	// Private methods
private:
    void PrepareSorData(SORDATA *prpSorData);

};

//---------------------------------------------------------------------------------------------------------

//extern CedaSorData ogSorData;

#endif //__CEDASORDATA__
