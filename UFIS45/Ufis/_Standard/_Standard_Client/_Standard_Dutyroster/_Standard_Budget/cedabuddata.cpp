// CedaBudData.cpp - Klasse f�r die Handhabung von Bud-Daten (Budgetberechnung) 
//
 
#include <stdafx.h>
#include <afxwin.h>
#include <CCSCedaData.h>
#include <CedaBudData.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <UFIS.h>
#include <CCSBcHandle.h>
#include <CCSGlobl.h>

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Globale Funktionen
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// ProcessBudCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_Bud_CHANGE, BC_Bud_NEW und BC_Bud_DELETE. Ruft zur eingentlichen Bearbeitung
//	der Nachrichten die Funktion CedaBudData::ProcessBudBc() der entsprechenden 
//	Instanz von CedaBudData auf.	
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessBudCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Empf�nger ermitteln
	CedaBudData *polBUDDATA = (CedaBudData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polBUDDATA->ProcessBudBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Implementation der Klasse CedaBudData (Daily Roster Absences - untert�gige Abwesenheit)
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// Konstruktor
//************************************************************************************************************************************************

CedaBudData::CedaBudData(CString opTableName, CString opExtName) : CCSCedaData(&ogCommHandler)
{
    // Infostruktur vom Typ CEDARECINFO f�r Bud-Datens�tze
	// Wird von Vaterklasse CCSCedaData f�r verschiedene Funktionen
	// (CCSCedaData::GetBufferRecord(), CCSCedaData::MakeCedaData())
	// benutzt. Die Datenstruktur beschreibt die Tabellenstruktur.
    BEGIN_CEDARECINFO(BUDDATA, BudDataRecInfo)
		FIELD_CHAR_TRIM	(Dayx,"DAYX")	// Stichtag
		FIELD_CHAR_TRIM	(Frmo,"FRMO")	// Jahr/Monat von
		FIELD_CHAR_TRIM	(Tomo,"TOMO")	// Jahr/Monat bis
		FIELD_CHAR_TRIM	(Hdco,"HDCO")	// Headcount
		FIELD_CHAR_TRIM	(Muta,"MUTA")	// Mutation
		FIELD_CHAR_TRIM	(Foca,"FOCA")	// Forecast
		FIELD_CHAR_TRIM	(Budg,"BUDG")	// Budget
		FIELD_CHAR_TRIM	(Rema,"REMA")	// Remarks
		FIELD_CHAR_TRIM	(Hopo,"HOPO")	// Hopo
		FIELD_OLEDATE	(Cdat,"CDAT")	// Erstellungsdatum
		FIELD_LONG   	(Urno,"URNO")	// Urno
		FIELD_CHAR_TRIM	(Usec,"USEC")	// Ersteller
		FIELD_CHAR_TRIM	(Useu,"USEU")	// Anwender letzte �nderung
		FIELD_OLEDATE	(Lstu,"LSTU")	// Datum letzte �nderung
	END_CEDARECINFO

	// Infostruktur kopieren
    for (int i = 0; i < sizeof(BudDataRecInfo)/sizeof(BudDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&BudDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	// Tabellenname per Default auf Bud setzen
	opExtName = pcgTableExt;
	SetTableNameAndExt(opTableName+opExtName);

    // Feldnamen initialisieren
	strcpy(pcmListOfFields,"DAYX,FRMO,TOMO,HDCO,MUTA,FOCA,BUDG,REMA,HOPO,CDAT,URNO,USEC,USEU,LSTU");
	// Zeiger der Vaterklasse auf Tabellenstruktur setzen
	CCSCedaData::pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

    // Datenarrays initialisieren
	omUrnoMap.InitHashTable(629,117);

	// ToDo: min. / max. Datum f�r Buds initialisieren
//	omMinDay = omMaxDay = TIMENULL;
}

//************************************************************************************************************************************************
// Destruktor
//************************************************************************************************************************************************

CedaBudData::~CedaBudData()
{
	ClearAll(true);
	omRecInfo.DeleteAll();
}

//************************************************************************************************************************************************
// SetTableNameAndExt: stellt den Namen und die Extension ein.
// R�ckgabe:	false	->	Tabellenname mit Extension gr��er als
//							Klassenmember
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaBudData::SetTableNameAndExt(CString opTableAndExtName)
{
CCS_TRY
	if(opTableAndExtName.GetLength() >= sizeof(pcmTableName)) return false;
	strcpy(pcmTableName,opTableAndExtName.GetBuffer(0));
	return true;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
//	Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
//	Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
//	dann die entsprechende Nachricht bearbeitet. �ber den nach void 
//	konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
//	Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
//	und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
//	entsprechenden Member-Funktion).
// R�ckgabe: keine
//************************************************************************************************************************************************

void CedaBudData::Register(void)
{
CCS_TRY
	// bereits angemeldet?
	if (bmIsBCRegistered) return;	// ja -> gibt nichts zu tun, terminieren

	// Bud-Datensatz hat sich ge�ndert
	ogDdx.Register((void *)this,BC_BUD_CHANGE, CString("CedaBudData"), CString("BC_Bud_CHANGE"),        ProcessBudCf);
	ogDdx.Register((void *)this,BC_BUD_NEW, CString("CedaBudData"), CString("BC_Bud_NEW"),		   ProcessBudCf);
	ogDdx.Register((void *)this,BC_BUD_DELETE, CString("CedaBudData"), CString("BC_Bud_DELETE"),        ProcessBudCf);
	// jetzt ist die Instanz angemeldet
	bmIsBCRegistered = true;
CCS_CATCH_ALL
}


//************************************************************************************************************************************************
// ClearAll: aufr�umen. Alle Arrays und PointerMaps werden geleert.
// R�ckgabe:	immer true
//************************************************************************************************************************************************

bool CedaBudData::ClearAll(bool bpUnregister)
{
CCS_TRY
    // alle Arrays leeren
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
	
	// beim BC-Handler abmelden
	if(bpUnregister && bmIsBCRegistered)
	{
		ogDdx.UnRegister(this,NOTUSED);
		// jetzt ist die Instanz angemeldet
		bmIsBCRegistered = false;
	}
    return true;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// ReadBudByUrno: liest den Bud-Datensatz mit der  Urno <lpUrno> aus der 
//	Datenbank.
// R�ckgabe:	boolean Datensatz erfolgreich gelesen?
//************************************************************************************************************************************************

bool CedaBudData::ReadBudByUrno(long lpUrno, BUDDATA *prpBud)
{
CCS_TRY
	char pclWhere[200]="";
	sprintf(pclWhere, "%ld", lpUrno);
	// Datensatz aus Datenbank lesen
	if (CedaAction("RT", pclWhere) == false)
	{
		return false;	// Fehler -> abbrechen
	}
	// Datensatz-Objekt instanziieren...
	BUDDATA *prlBud = new BUDDATA;
	// und initialisieren
	if (GetBufferRecord(0,prpBud) == true)
	{  
		return true;	// Aktion erfolgreich
	}
	else
	{
		// Fehler -> aufr�umen und terminieren
		delete prlBud;
		return false;
	}

	return false;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// Read: liest Datens�tze ein.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaBudData::Read(char *pspWhere /*=NULL*/, CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/,
					   bool bpRegisterBC /*= true*/, bool bpClearData /*= true*/)
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	// wenn gew�nscht, aufr�umen (und beim Broadcast-Handler abmelden, wenn
	// nach Ende angemeldet werden soll)
	if (bpClearData) ClearAll(bpRegisterBC);
	
    // Datens�tze lesen
	if(pspWhere == NULL)
	{
		if (!CCSCedaData::CedaAction("RT", "")) return false;
	}
	else
	{
		if (!CCSCedaData::CedaAction("RT", pspWhere)) return false;
	}

	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// void-Pointer f�r MA-Urno-Lookup
	void  *prlVoid = NULL;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		BUDDATA *prlBud = new BUDDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord(ilCountRecords,prlBud);
		if (!blMoreRecords) break;
		// Datensatz gelesen, Z�hler inkrementieren
		ilCountRecords++;
		// wurde eine Datensatz gelesen und ist (wenn vorhanden) die
		// Mitarbeiter-Urno dieses Datensatzes im MA-Urno-Array enthalten? 
		/*if(blMoreRecords && 
		   ((popLoadStfUrnoMap == NULL) || 
			(popLoadStfUrnoMap->Lookup((void *)prlBud->Stfu, (void *&)prlVoid) == TRUE)))
		{*/ 
			// Datensatz hinzuf�gen
			if (!InsertInternal(prlBud, BUD_NO_SEND_DDX)){
				// Fehler -> lokalen Datensatz l�schen
				delete prlBud;
			}
		/*}
		else
		{
			// kein weiterer Datensatz
			delete prlBud;
		}*/
	} while (blMoreRecords);
	
	// beim Broadcast-Handler anmelden, wenn gew�nscht
	if (bpRegisterBC) Register();

	// Test: Anzahl der gelesenen Bud
	TRACE("Read-Bud: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// Insert: speichert den Datensatz <prpBud> in der Datenbank und schickt einen
//	Broadcast ab.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaBudData::Insert(BUDDATA *prpBud, bool bpSave /*= true*/)
{
CCS_TRY
	// �nderungs-Flag setzen
	prpBud->IsChanged = DATA_NEW;
	// in Datenbank speichern, wenn erw�nscht
	if(bpSave && Save(prpBud) == false) return false;
	// Broadcast Bud_NEW abschicken
	InsertInternal(prpBud,true);
    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// InsertInternal: f�gt den internen Arrays einen Datensatz hinzu. Der Datensatz
//	muss sp�ter oder vorher explizit durch Aufruf von Save() oder Release() in der Datenbank 
//	gespeichert werden. Wenn <bpSendDdx> true ist, wird ein Broadcast gesendet.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaBudData::InsertInternal(BUDDATA *prpBud, bool bpSendDdx)
{
CCS_TRY
	// Datensatz intern anf�gen
	omData.Add(prpBud);
	// Datensatz der Urno-Map hinzuf�gen
	omUrnoMap.SetAt((void *)prpBud->Urno,prpBud);
		
	// Broadcast senden?
	if( bpSendDdx == true )
	{
		// ja -> go!
		ogDdx.DataChanged((void *)this,BUD_NEW,(void *)prpBud);
	}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// Update: sucht den Datensatz mit der Urno <prpBud->Urno> und speichert ihn
// in der Datenbank.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaBudData::Update(BUDDATA *prpBud,bool bpSave /*= true*/)
{
CCS_TRY
	// Datensatz raussuchen
	if (GetBudByUrno(prpBud->Urno) != NULL)
	{
		// �nderungsflag setzen
		if (prpBud->IsChanged == DATA_UNCHANGED)
		{
			prpBud->IsChanged = DATA_CHANGED;
		}
		// in die Datenbank speichern
		if(bpSave && Save(prpBud) == false) return false; 
		// Broadcast Bud_CHANGE versenden
		UpdateInternal(prpBud);
	}
    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// UpdateInternal: �ndert einen Datensatz, der in der internen Datenhaltung
//	enthalten ist.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaBudData::UpdateInternal(BUDDATA *prpBud, bool bpSendDdx /*true*/)
{
CCS_TRY
// APO 9.12.99 Updates passieren immer auf Kopie eines Datensatzes der internen
// Datenhaltung, kopieren daher unn�tig.
/*	// Bud in lokaler Datenhaltung suchen
	BUDDATA *prlBud = GetBudByKey(prpBud->Sday,prpBud->Stfu,prpBud->Budn,prpBud->Rosl);
	// Bud gefunden?
	if (prlBud != NULL)
	{
		// ja -> durch Zeiger ersetzen
		*prlBud = *prpBud;
	}
*/
	// Broadcast senden, wenn gew�nscht
	if( bpSendDdx == true )
	{
//		TRACE("\nSending %d",Bud_CHANGE);
		ogDdx.DataChanged((void *)this,BUD_CHANGE,(void *)prpBud);
	}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// DeleteInternal: l�scht einen Datensatz aus den internen Arrays.
// R�ckgabe:	keine
//*********************************************************************************************

void CedaBudData::DeleteInternal(BUDDATA *prpBud, bool bpSendDdx /*true*/)
{
CCS_TRY
	// zuerst Broadcast senden, damit alle Module reagieren k�nnen BEVOR
	// der Datensatz gel�scht wird (synchroner Mechanismus)
	if(bpSendDdx == true)
	{
		ogDdx.DataChanged((void *)this,BUD_DELETE,(void *)prpBud);
	}

	// Urno-Schl�ssel l�schen
	omUrnoMap.RemoveKey((void *)prpBud->Urno);

	// Datensatz l�schen
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpBud->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}
CCS_CATCH_ALL
}

//*********************************************************************************************
// Delete: markiert den Datensatz <prpBud> als gel�scht und l�scht den Datensatz
//	aus der internen Datenhaltung und der Datenbank. 
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaBudData::Delete(BUDDATA *prpBud, bool bpWithSave /* true*/)
{
CCS_TRY
	// Flag setzen
	prpBud->IsChanged = DATA_DELETED;
	
	// speichern
	if(bpWithSave == TRUE)
	{
		if (!Save(prpBud)) return false;
	}

	// aus der internen Datenhaltung l�schen
	DeleteInternal(prpBud,true);

	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// ProcessBudBc: behandelt die einlaufenden Broadcasts.
// R�ckgabe:	keine
//*********************************************************************************************

void  CedaBudData::ProcessBudBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY
	// Performance-Trace
	SYSTEMTIME rlPerf;
	GetSystemTime(&rlPerf);
	TRACE("CedaBudData::ProcessBudBc: START %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);

	// Datensatz-Urno
	long llUrno;
	// Daten und Info
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;
	BUDDATA *prlBud = NULL;

	switch(ipDDXType)
	{
	case BC_BUD_NEW: // neuen Datensatz einf�gen
		{
			// einzuf�gender Datensatz
			prlBud = new BUDDATA;
			GetRecordFromItemList(prlBud,prlBcStruct->Fields,prlBcStruct->Data);
			InsertInternal(prlBud, BUD_SEND_DDX);
		}
		break;
	case BC_BUD_CHANGE:	// Datensatz �ndern
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlBud = GetBudByUrno(llUrno);
			if(prlBud != NULL)
			{
				// ja -> Datensatz aktualisieren
				GetRecordFromItemList(prlBud,prlBcStruct->Fields,prlBcStruct->Data);
				UpdateInternal(prlBud);
			}
			// nein -> Datensatz interessiert uns nicht
		}
		break;
	case BC_BUD_DELETE:	// Datensatz l�schen
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlBud = GetBudByUrno(llUrno);
			if (prlBud != NULL)
			{
				// ja -> Datensatz l�schen
				DeleteInternal(prlBud);
			}
		}
		break;
	default:
		break;
	}
	// Performance-Test
	GetSystemTime(&rlPerf);
	TRACE("CedaBudData::ProcessBudBc: END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
CCS_CATCH_ALL
}

//*********************************************************************************************
// GetBudByUrno: sucht den Datensatz mit der Urno <lpUrno>.
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

BUDDATA *CedaBudData::GetBudByUrno(long lpUrno)
{
CCS_TRY
	// der Datensatz
	BUDDATA *prpBud;
	// Datensatz in Urno-Map suchen
	if (omUrnoMap.Lookup((void*)lpUrno,(void *& )prpBud) == TRUE)
	{
		return prpBud;	// gefunden -> Zeiger zur�ckgeben
	}
	// nicht gefunden -> R�ckgabe NULL
	return NULL;
CCS_CATCH_ALL
return NULL;
}

//*******************************************************************************
// Save: speichert den Datensatz <prpBud>.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************

bool CedaBudData::Save(BUDDATA *prpBud)
{
CCS_TRY
	bool olRc = true;
	CString olListOfData;
	char pclSelection[1024];
	char pclData[2048];

	if (prpBud->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpBud->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpBud);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpBud->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpBud->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpBud);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpBud->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpBud->Urno);
		olRc = CedaAction("DRT",pclSelection);
		break;
	}
	// R�ckgabe: Ergebnis der CedaAction
	return olRc;
CCS_CATCH_ALL
return false;
}

//*************************************************************************************
// CedaAction: ruft die Funktion CedaAction der Basisklasse CCSCedaData auf.
//	Alle Parameter werden durchgereicht. Aufgrund eines internen CEDA-Bugs,
//	der daf�r sorgen kann, dass die Tabellenbeschreibung <pcmListOfFields>
//	zerst�rt wird, muss sicherheitshalber <pcmListOfFields> vor jedem Aufruf
//	von CCSCedaData::CedaAction() gerettet und danach wiederhergestellt werden.
// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
//*************************************************************************************

bool CedaBudData::CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
							 char *pcpSelection, char *pcpSort, char *pcpData, 
							 char *pcpDest /*= "BUF1"*/,bool bpIsWriteAction /*= false*/, 
							 long lpID /*= 0*/, CCSPtrArray<RecordSet> *pomData /*= NULL*/, 
							 int ipFieldCount /*= 0*/)
{
CCS_TRY
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CCSCedaData::CedaAction(pcpAction,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL
return false;
}

//*************************************************************************************
// CedaAction: kurze Version, Beschreibung siehe oben.
// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
//*************************************************************************************

bool CedaBudData::CedaAction(char *pcpAction, char *pcpSelection, char *pcpSort,
							 char *pcpData, char *pcpDest /*"BUF1"*/, 
							 bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{
CCS_TRY
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CCSCedaData::CedaAction(pcpAction,CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,(pcpSelection != NULL)? pcpSelection: CCSCedaData::pcmSelection,(pcpSort != NULL)? pcpSort: CCSCedaData::pcmSort,pcpData,pcpDest,bpIsWriteAction, lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// CopyBudValues: Kopiert die "Nutzdaten" (keine urnos usw.)
// R�ckgabe:	keine
//************************************************************************************************************************************************

void CedaBudData::CopyBud(BUDDATA* popBUDDATASource,BUDDATA* popBUDDATATarget)
{
	// Daten kopieren
	/*strcpy(popBUDDATATarget->Rema,popBUDDATASource->Rema);
	strcpy(popBUDDATATarget->Sdac,popBUDDATASource->Sdac);
	popBUDDATATarget->Abfr = popBUDDATASource->Abfr;
	popBUDDATATarget->Abto = popBUDDATASource->Abto;*/
}

bool CedaBudData::ReadBudData()
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	ClearAll(true);
	
    if (!CCSCedaData::CedaAction("RT", "")) return false;
	
	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		BUDDATA *prlBud = new BUDDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord(ilCountRecords,prlBud);
		// Datensatz gelesen, Z�hler inkrementieren
		if (!blMoreRecords) break;
		ilCountRecords++;
		if (!InsertInternal(prlBud, BUD_NO_SEND_DDX)){
			// Fehler -> lokalen Datensatz l�schen
			delete prlBud;
		}
	} while (blMoreRecords);
		
	// Test: Anzahl der gelesenen Bud
	TRACE("Read-Bud: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}

