// cedawisdata.cpp - Klasse f�r die Handhabung von WIS-Daten
//
 
#include <stdafx.h>
#include <afxwin.h>
#include <CCSCedaData.h>
#include <CedaWisData.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <UFIS.h>
#include <CCSBcHandle.h>

// Das globale Objekt
//CedaWisData ogWisData;

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Globale Funktionen
//************************************************************************************************************************************************
//************************************************************************************************************************************************

// globale Callback-Funktion f�r den Broadcast-Handler
//void  ProcessRelWisCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

void  ProcessWisCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//************************************************************************************************************************************************
// ProcessWisCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_WIS_CHANGE, BC_WIS_NEW und BC_WIS_DELETE. Ruft zur eingentlichen Bearbeitung
//	der Nachrichten die Funktion CedaWisData::ProcessWisBc() der entsprechenden 
//	Instanz von CedaWisData auf.	
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessWisCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Sanduhr einblenden, falls es l�nger dauert
	AfxGetApp()->DoWaitCursor(1);
	// Empf�nger ermitteln
	CedaWisData *polWisData = (CedaWisData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polWisData->ProcessWisBc(ipDDXType,vpDataPointer,ropInstanceName);
	// Sanduhr ausblenden
	AfxGetApp()->DoWaitCursor(-1);
}

//************************************************************************************************************************************************
// ProcessRelWisCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_RELOAD_SINGLE_WIS, BC_RELOAD_MULTI_WIS und BC_RELWIS. Ruft zur eingentlichen 
//	Bearbeitung der Nachrichten die Funktion CedaWisData::ProcessWisBc() der 
//	entsprechenden Instanz von CedaWisData auf.
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessRelWisCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{ 
}

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Implementation der Klasse CedaWisData
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// Konstruktor
//************************************************************************************************************************************************

CedaWisData::CedaWisData(CString opTableName, CString opExtName) : CCSCedaData(&ogCommHandler)
{
    // Infostruktur vom Typ CEDARECINFO f�r DRW-Datens�tze
	// Wird von Vaterklasse CCSCedaData f�r verschiedene Funktionen
	// (CCSCedaData::GetBufferRecord(), CCSCedaData::MakeCedaData())
	// benutzt. Die Datenstruktur beschreibt die Tabellenstruktur.
    BEGIN_CEDARECINFO(WISDATA, WisDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")	
		FIELD_CHAR_TRIM	(Hopo,"HOPO")	
		FIELD_DATE		(Lstu,"LSTU")	
		FIELD_CHAR_TRIM	(Orgc,"ORGC")	
		FIELD_CHAR_TRIM	(Rema,"REMA")	
		FIELD_LONG		(Urno,"URNO")	
		FIELD_CHAR_TRIM	(Usec,"USEC")	
		FIELD_CHAR_TRIM	(Useu,"USEU")	
		FIELD_CHAR_TRIM	(Wisc,"WISC")	
		FIELD_CHAR_TRIM	(Wisd,"WISD")	
    END_CEDARECINFO
	// Infostruktur kopieren
    for (int i = 0; i < sizeof(WisDataRecInfo)/sizeof(WisDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&WisDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	// Tabellenname per Default auf WIS setzen
	opExtName = pcgTableExt;
	SetTableNameAndExt(opTableName+opExtName);

    // Feldnamen initialisieren
	strcpy(pcmListOfFields,	"CDAT,HOPO,LSTU,ORGC,REMA,URNO,USEC,USEU,WISC,WISD");
	// Zeiger der Vaterklasse auf Tabellenstruktur setzen
	CCSCedaData::pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

    // Datenarrays initialisieren
	omUrnoMap.InitHashTable(629,117);
	// Array mit den Urnos der Mitarbeiter, f�r die Daten geladen werden,
	// ist vorerst NULL. Der Array muss explizit per CedaWisData::SetLoadStfUrnoMap()
	// gesetzt werden 
	pomLoadStfuUrnoMap = NULL;
	
//	omLastBcNum = 0;
//	omLastBcUrno = 0;
}

//************************************************************************************************************************************************
// Destruktor
//************************************************************************************************************************************************

CedaWisData::~CedaWisData()
{
	ClearAll();
	omRecInfo.DeleteAll();
}

//************************************************************************************************************************************************
// SetTableNameAndExt: stellt den Namen und die Extension ein.
// R�ckgabe:	false	->	Tabellenname mit Extension gr��er als
//							Klassenmember
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaWisData::SetTableNameAndExt(CString opTableAndExtName)
{
	if(opTableAndExtName.GetLength() >= sizeof(pcmTableName)) return false;
	strcpy(pcmTableName,opTableAndExtName.GetBuffer(0));
	return true;
}

//************************************************************************************************************************************************
// Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
//	Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
//	Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
//	dann die entsprechende Nachricht bearbeitet. �ber den nach void 
//	konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
//	Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
//	und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
//	entsprechenden Member-Funktion).
// R�ckgabe: keine
//************************************************************************************************************************************************

void CedaWisData::Register(void)
{
}


//************************************************************************************************************************************************
// ClearAll: aufr�umen. Alle Arrays und PointerMaps werden geleert.
// R�ckgabe:	immer true
//************************************************************************************************************************************************

bool CedaWisData::ClearAll(bool bpUnregister)
{
    omUrnoMap.RemoveAll();
    omKeyMap.RemoveAll();
//	omSearchResult.RemoveAll();
	omData.DeleteAll();
	pomLoadStfuUrnoMap = NULL;
	if(bpUnregister)
	{
		ogDdx.UnRegister(this,NOTUSED);
	//	VIEWWIS olViewWis;
	//	SetView(olViewWis);
	}
    return true;
}

//************************************************************************************************************************************************
// ReadDrwByUrno: 
//************************************************************************************************************************************************

bool CedaWisData::ReadDrwByUrno(long lpUrno, WISDATA *prpDrw)
{
	return (false);
}

//************************************************************************************************************************************************
// Read: liest Datens�tze ein.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaWisData::Read(char *pspWhere /*=NULL*/)
{
	CString olTmp;
  // Select data from the database
	bool ilRc = true;
	// Maps leeren
    omUrnoMap.RemoveAll();
	omKeyMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		TRACE("Read-WIS: Ceda-Error %d \n",ilRc);
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		WISDATA *prlWis = new WISDATA;
		if ((ilRc = GetFirstBufferRecord(prlWis)) == true)
		{
			omData.Add(prlWis);//Update omData
			// Die Maps f�llen
			omUrnoMap.SetAt((void *)prlWis->Urno,prlWis);
			olTmp.Format("%s",prlWis->Wisc);
			omKeyMap.SetAt(olTmp,prlWis);
		}
		else
		{
			delete prlWis;
		}
	}
	TRACE("Read-Wis: %d gelesen\n",ilLc-1);
    return true;
}

//********************************************************************************
// Anhand des Tages und der Mitarbeiter Urno den Datensatz finden
//********************************************************************************

WISDATA* CedaWisData::GetWisByKey(CString opWisc)
{
	CString olTmp;
	WISDATA *prlWis = NULL;

	// such String formatieren
	olTmp.Format("%s",opWisc);

	// Suche in der KeyMap
	if (omKeyMap.Lookup(olTmp,(void *&)prlWis) == TRUE)
	{
		// Datensatz gefunden
		return prlWis;
	}
	// Datensatz nicht gefunden
	return NULL;
}

//*********************************************************************************************
// InsertInternal: f�gt den internen Arrays einen Datensatz hinzu. Der Datensatz
//	muss sp�ter explizit durch Aufruf von Save() oder Release() in der Datenbank 
//	gespeichert werden. Wenn <bpSendDdx> true ist, wird der Broadcast-Handler
//	aktiviert.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaWisData::InsertInternal(WISDATA *prpWis, bool bpSendDdx)
{
	return true;
}

//*************************************************************************************
//
//*************************************************************************************

void CedaWisData::UpdateInternal(WISDATA *prpWis, bool bpSendDdx /*true*/)
{
}

//*************************************************************************************
//
//*************************************************************************************

void CedaWisData::DeleteInternal(WISDATA *prpWis, bool bpSendDdx /*true*/)
{

}

//*************************************************************************************
//
//*************************************************************************************

bool CedaWisData::Delete(WISDATA *prpWis, BOOL bpWithSave)
{
	return true;
}

//*************************************************************************************
//
//*************************************************************************************

bool CedaWisData::Insert(WISDATA *prpWis, BOOL bpWithSave)
{
	return true;
}

//*************************************************************************************
//
//*************************************************************************************

bool CedaWisData::Update(WISDATA *prpWis, BOOL bpWithSave)
{
	return true;
}

//*************************************************************************************
//
//*************************************************************************************

bool CedaWisData::WisExist(long Urno,WISDATA **prpData)
{
	return false;
}

//*************************************************************************************
//
//*************************************************************************************

bool CedaWisData::Reload(CString opDateFrom, CString opDateTo, CString opUrnos)
{
	return true;
}

//*************************************************************************************
// 
//*************************************************************************************

WISDATA* CedaWisData::GetWisByUrno(long lpUrno)
{
	WISDATA *prlWis = NULL;

	// Suche in der KeyMap
	if (omUrnoMap.Lookup((void *)lpUrno,(void *&)prlWis) == TRUE)
	{
		// Datensatz gefunden
		return prlWis;
	}
	// Datensatz nicht gefunden
	return NULL;
}

//*************************************************************************************
//	Record speichern (wobei hier auch ein Record gel�scht werden kann)
//*************************************************************************************

bool CedaWisData::Save(WISDATA *prpWis)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[1024];
	char pclData[2048];

	if (prpWis->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpWis->IsChanged)   // New job, insert into database
	{
	// Neuer Record
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpWis);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpWis->IsChanged = DATA_UNCHANGED;
		break;
	// ge�nderter Record
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpWis->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpWis);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpWis->IsChanged = DATA_UNCHANGED;
		break;
	// gel�schter Record
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpWis->Urno);
		olRc = CedaAction("DRT",pclSelection);
		if (olRc == true)
		{
		
		}
		break;
	}
   return olRc;
}

//*************************************************************************************
//
//*************************************************************************************

bool CedaWisData::IsDataChanged(WISDATA *prpOld, WISDATA *prpNew)
{
	return false;
}

//*************************************************************************************
// CedaAction: ruft die Funktion CedaAction der Basisklasse CCSCedaData auf.
//	Alle Parameter werden durchgereicht. Aufgrund eines internen CEDA-Bugs,
//	der daf�r sorgen kann, dass die Tabellenbeschreibung <pcmListOfFields>
//	zerst�rt wird, muss sicherheitshalber <pcmListOfFields> vor jedem Aufruf
//	von CCSCedaData::CedaAction() gerettet und danach wiederhergestellt werden.
// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
//*************************************************************************************

bool CedaWisData::CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
							 char *pcpSelection, char *pcpSort, char *pcpData, 
							 char *pcpDest /*= "BUF1"*/,bool bpIsWriteAction /*= false*/, 
							 long lpID /*= 0*/, CCSPtrArray<RecordSet> *pomData /*= NULL*/, 
							 int ipFieldCount /*= 0*/)
{
CCS_TRY
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CCSCedaData::CedaAction(pcpAction,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL
return false;
}

//*************************************************************************************
// CedaAction: kurze Version, Beschreibung siehe oben.
// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
//*************************************************************************************

bool CedaWisData::CedaAction(char *pcpAction, char *pcpSelection, char *pcpSort,
							 char *pcpData, char *pcpDest /*"BUF1"*/, 
							 bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{
CCS_TRY
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CCSCedaData::CedaAction(pcpAction,CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,(pcpSelection != NULL)? pcpSelection: CCSCedaData::pcmSelection,(pcpSort != NULL)? pcpSort: CCSCedaData::pcmSort,pcpData,pcpDest,bpIsWriteAction, lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// ProcessWisBc: behandelt die einlaufenden Broadcasts.
// R�ckgabe:	keine
//*********************************************************************************************

void  CedaWisData::ProcessWisBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY
	// Performance-Trace
	SYSTEMTIME rlPerf;
	GetSystemTime(&rlPerf);
	TRACE("CedaWisData::ProcessWisBc: START %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);

	// Datensatz-Urno
	long llUrno;
	// Daten und Info
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;
	WISDATA *prlWis = NULL;

	switch(ipDDXType)
	{
	case BC_WIS_NEW: // neuen Datensatz einf�gen
		{
			// einzuf�gender Datensatz
			prlWis = new WISDATA;
			GetRecordFromItemList(prlWis,prlBcStruct->Fields,prlBcStruct->Data);
			InsertInternal(prlWis, WIS_SEND_DDX);
		}
		break;
	case BC_WIS_CHANGE:	// Datensatz �ndern
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlWis = GetWisByUrno(llUrno);
			if(prlWis != NULL)
			{
				// ja -> Datensatz aktualisieren
				GetRecordFromItemList(prlWis,prlBcStruct->Fields,prlBcStruct->Data);
				UpdateInternal(prlWis);
			}
			// nein -> Datensatz interessiert uns nicht
		}
		break;
	case BC_WIS_DELETE:	// Datensatz l�schen
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlWis = GetWisByUrno(llUrno);
			if (prlWis != NULL)
			{
				// ja -> Datensatz l�schen
				DeleteInternal(prlWis);
			}
		}
		break;
	default:
		break;
	}
	// Performance-Test
	GetSystemTime(&rlPerf);
	TRACE("CedaWisData::ProcessWisBc: END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
CCS_CATCH_ALL
}
