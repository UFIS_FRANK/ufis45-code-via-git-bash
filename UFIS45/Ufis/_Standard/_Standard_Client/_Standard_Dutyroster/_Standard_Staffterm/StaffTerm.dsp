# Microsoft Developer Studio Project File - Name="StaffTerm" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=StaffTerm - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "StaffTerm.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "StaffTerm.mak" CFG="StaffTerm - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "StaffTerm - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "StaffTerm - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName "StaffTerm"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "StaffTerm - Win32 Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "c:\Ufis_Bin\Release"
# PROP Intermediate_Dir "c:\Ufis_Intermediate\StaffTerm\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I ".\\" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 c:\Ufis_Bin\Release\Ufis32.lib c:\Ufis_Bin\Classlib\Release\CCSClass.lib /nologo /subsystem:windows /machine:I386
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "StaffTerm - Win32 Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "c:\Ufis_Bin\Debug"
# PROP Intermediate_Dir "c:\Ufis_Intermediate\StaffTerm\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 c:\Ufis_Bin\Debug\Ufis32.lib c:\Ufis_Bin\Classlib\Debug\CCSClass.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# SUBTRACT LINK32 /nodefaultlib

!ENDIF 

# Begin Target

# Name "StaffTerm - Win32 Release"
# Name "StaffTerm - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\basicdata.cpp
# End Source File
# Begin Source File

SOURCE=.\ccsglobl.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSParam.cpp
# End Source File
# Begin Source File

SOURCE=.\cedaaccdata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedacfgdata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedadrrdata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedadrwdata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedainitmodudata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedascodata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedasecdata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedasordata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedastfdata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedawisdata.cpp
# End Source File
# Begin Source File

SOURCE=.\ChoiceDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\cviewer.cpp
# End Source File
# Begin Source File

SOURCE=.\DPLDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\gridcontrol.cpp
# End Source File
# Begin Source File

SOURCE=.\GUILng.cpp
# End Source File
# Begin Source File

SOURCE=.\initialloaddlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ListBoxDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\logindlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MultipleInput.cpp
# End Source File
# Begin Source File

SOURCE=.\privlist.cpp
# End Source File
# Begin Source File

SOURCE=.\registerdlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StaffTerm.cpp
# End Source File
# Begin Source File

SOURCE=.\StaffTerm.rc
# End Source File
# Begin Source File

SOURCE=.\StaffTermDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\WaitDlg.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\basicdata.h
# End Source File
# Begin Source File

SOURCE=.\ccsglobl.h
# End Source File
# Begin Source File

SOURCE=.\CCSParam.h
# End Source File
# Begin Source File

SOURCE=.\cedaaccdata.h
# End Source File
# Begin Source File

SOURCE=.\cedacfgdata.h
# End Source File
# Begin Source File

SOURCE=.\cedadrrdata.h
# End Source File
# Begin Source File

SOURCE=.\cedadrwdata.h
# End Source File
# Begin Source File

SOURCE=.\cedainitmodudata.h
# End Source File
# Begin Source File

SOURCE=.\cedascodata.h
# End Source File
# Begin Source File

SOURCE=.\cedasecdata.h
# End Source File
# Begin Source File

SOURCE=.\cedasordata.h
# End Source File
# Begin Source File

SOURCE=.\cedastfdata.h
# End Source File
# Begin Source File

SOURCE=.\cedawisdata.h
# End Source File
# Begin Source File

SOURCE=.\ChoiceDlg.h
# End Source File
# Begin Source File

SOURCE=.\cviewer.h
# End Source File
# Begin Source File

SOURCE=.\DPLDlg.h
# End Source File
# Begin Source File

SOURCE=.\gridcontrol.h
# End Source File
# Begin Source File

SOURCE=.\GUILng.h
# End Source File
# Begin Source File

SOURCE=.\initialloaddlg.h
# End Source File
# Begin Source File

SOURCE=.\ListBoxDlg.h
# End Source File
# Begin Source File

SOURCE=.\logindlg.h
# End Source File
# Begin Source File

SOURCE=.\MultipleInput.h
# End Source File
# Begin Source File

SOURCE=.\privlist.h
# End Source File
# Begin Source File

SOURCE=.\registerdlg.h
# End Source File
# Begin Source File

SOURCE=.\resource.h
# PROP Ignore_Default_Tool 1
# End Source File
# Begin Source File

SOURCE=.\StaffTerm.h
# End Source File
# Begin Source File

SOURCE=.\StaffTermDlg.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\WaitDlg.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\abb.bmp
# End Source File
# Begin Source File

SOURCE=.\res\appl.ico
# End Source File
# Begin Source File

SOURCE=.\res\Exclamat.bmp
# End Source File
# Begin Source File

SOURCE=.\res\MonoUfisDE.bmp
# End Source File
# Begin Source File

SOURCE=.\res\StaffTerm.ico
# End Source File
# Begin Source File

SOURCE=.\res\StaffTerm.rc2
# End Source File
# Begin Source File

SOURCE=.\res\UfisLogin.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\res\filecopy.avi
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
