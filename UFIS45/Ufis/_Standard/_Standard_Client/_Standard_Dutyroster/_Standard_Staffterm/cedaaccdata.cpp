// CedaAccData.cpp
 
#include <stdafx.h>
#include <CedaAccData.h>


void ProcessAccCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
//CedaAccData ogAccData;

//--CEDADATA-----------------------------------------------------------------------------------------------
 
CedaAccData::CedaAccData() : CCSCedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(ACCDATA, AccDataRecInfo)
		FIELD_DATE	   (Aato,"AATO")
		FIELD_CHAR_TRIM(Auby,"AUBY")
		FIELD_DATE     (Auda,"AUDA")
		FIELD_DATE     (Cdat,"CDAT")
		FIELD_CHAR_TRIM(Cl01,"CL01")
		FIELD_CHAR_TRIM(Cl02,"CL02")
		FIELD_CHAR_TRIM(Cl03,"CL03")
		FIELD_CHAR_TRIM(Cl04,"CL04")
		FIELD_CHAR_TRIM(Cl05,"CL05")
		FIELD_CHAR_TRIM(Cl06,"CL06")
		FIELD_CHAR_TRIM(Cl07,"CL07")
		FIELD_CHAR_TRIM(Cl08,"CL08")
		FIELD_CHAR_TRIM(Cl09,"CL09")
		FIELD_CHAR_TRIM(Cl10,"CL10")
		FIELD_CHAR_TRIM(Cl11,"CL11")
		FIELD_CHAR_TRIM(Cl12,"CL12")
		FIELD_CHAR_TRIM(Co01,"CO01")
		FIELD_CHAR_TRIM(Co02,"CO02")
		FIELD_CHAR_TRIM(Co03,"CO03")
		FIELD_CHAR_TRIM(Co04,"CO04")
		FIELD_CHAR_TRIM(Co05,"CO05")
		FIELD_CHAR_TRIM(Co06,"CO06")
		FIELD_CHAR_TRIM(Co07,"CO07")
		FIELD_CHAR_TRIM(Co08,"CO08")
		FIELD_CHAR_TRIM(Co09,"CO09")
		FIELD_CHAR_TRIM(Co10,"CO10")
		FIELD_CHAR_TRIM(Co11,"CO11")
		FIELD_CHAR_TRIM(Co12,"CO12")
		FIELD_CHAR_TRIM(Hopo,"HOPO")
		FIELD_DATE     (Lstu,"LSTU")
		FIELD_CHAR_TRIM(Op01,"OP01")
		FIELD_CHAR_TRIM(Op02,"OP02")
		FIELD_CHAR_TRIM(Op03,"OP03")
		FIELD_CHAR_TRIM(Op04,"OP04")
		FIELD_CHAR_TRIM(Op05,"OP05")
		FIELD_CHAR_TRIM(Op06,"OP06")
		FIELD_CHAR_TRIM(Op07,"OP07")
		FIELD_CHAR_TRIM(Op08,"OP08")
		FIELD_CHAR_TRIM(Op09,"OP09")
		FIELD_CHAR_TRIM(Op10,"OP10")
		FIELD_CHAR_TRIM(Op11,"OP11")
		FIELD_CHAR_TRIM(Op12,"OP12")
		FIELD_CHAR_TRIM(Peno,"PENO")
		FIELD_LONG     (Stfu,"STFU")
		FIELD_CHAR_TRIM(Type,"TYPE")
		FIELD_LONG     (Urno,"URNO")
		FIELD_CHAR_TRIM(Usec,"USEC")
		FIELD_CHAR_TRIM(Useu,"USEU")
		FIELD_CHAR_TRIM(Year,"YEAR")
		FIELD_CHAR_TRIM(Yecu,"YECU")
		FIELD_CHAR_TRIM(Yela,"YELA")


	END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(AccDataRecInfo)/sizeof(AccDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&AccDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
	strcpy(pcmTableName,"ACC");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,	"AATO,AUBY,AUDA,CDAT,CL01,CL02,CL03,CL04,CL05,CL06,CL07,CL08,CL09,CL10,CL11,CL12,CO01,"
							"CO02,CO03,CO04,CO05,CO06,CO07,CO08,CO09,CO10,CO11,CO12,HOPO,LSTU,OP01,OP02,OP03,OP04,"
							"OP05,OP06,OP07,OP08,OP09,OP10,OP11,OP12,PENO,STFU,TYPE,URNO,USEC,USEU,YEAR,YECU,YELA");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	pomLoadStfuUrnoMap = NULL;
	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaAccData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaAccData::Register(void)
{
	ogDdx.Register((void *)this,BC_ACC_CHANGE,		CString("CedaAccData"), CString("BC_ACC_CHANGE"),	  ProcessAccCf);
	ogDdx.Register((void *)this,BC_ACC_NEW,			CString("CedaAccData"), CString("BC_ACC_NEW"),		  ProcessAccCf);
	ogDdx.Register((void *)this,BC_ACC_DELETE,		CString("CedaAccData"), CString("BC_ACC_DELETE"),	  ProcessAccCf);
	ogDdx.Register((void *)this,BC_DISABLEACCSAVE,  CString("CedaAccData"), CString("BC_DISABLEACCSAVE"), ProcessAccCf);
	ogDdx.Register((void *)this,BC_ENABLEACCSAVE,	CString("CedaAccData"), CString("BC_ENABLEACCSAVE"),  ProcessAccCf);
	ogDdx.Register((void *)this,BC_RELACC,			CString("CedaAccData"), CString("BC_RELACC"),	  ProcessAccCf);
	
	//ogDdx.Register((void *)this,ACC_CHANGE,		CString("CedaAccData"), CString("BC_ACC_CHANGE"),	 ProcessAccCf);
	//ogDdx.Register((void *)this,ACC_NEW,			CString("CedaAccData"), CString("BC_ACC_NEW"),	 ProcessAccCf);
	//ogDdx.Register((void *)this,ACC_DELETE,		CString("CedaAccData"), CString("BC_ACC_DELETE"),	 ProcessAccCf);
	//ogDdx.Register((void *)this,RELACC,			CString("CedaAccData"), CString("BC_RELACC"),	  ProcessAccCf);
}



//--~CEDADATA----------------------------------------------------------------------------------------------

CedaAccData::~CedaAccData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaAccData::ClearAll(bool bpWithUnRegistration)
{
    omUrnoMap.RemoveAll();
    omKeyMap.RemoveAll();
    omData.DeleteAll();
	pomLoadStfuUrnoMap = NULL;
	if(bpWithUnRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//---------------------------------------------------------------------------

void CedaAccData::SetLoadStfuUrnoMap(CMapPtrToPtr *popLoadStfuUrnoMap)
{
CCS_TRY
	pomLoadStfuUrnoMap = popLoadStfuUrnoMap;
CCS_CATCH_ALL
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaAccData::Read(char *pspWhere /*NULL*/, bool bpDeleteAll /*=true*/)
{
    // Select data from the database
	bool ilRc = true;
 	if(bpDeleteAll)
	{
		omUrnoMap.RemoveAll();
		omKeyMap.RemoveAll();
		omData.DeleteAll();
	}
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	int ilLc = 0;
	if(pomLoadStfuUrnoMap != NULL)
	{
		//Load WITH LoadStfuUrnoMap
		void  *prlVoid = NULL;
		for(ilLc = 0; ilRc == true; ilLc++)
		{
			ACCDATA *prlAcc = new ACCDATA;
			if ((ilRc = GetFirstBufferRecord(prlAcc)) == true)
			{
			
				//TRACE(" Type: %s Stfu: %ld\n",prlAcc->Type,prlAcc->Stfu);

				if(pomLoadStfuUrnoMap->Lookup((void *)prlAcc->Stfu, (void *&)prlVoid) == TRUE)
				{
					omData.Add(prlAcc);//Update omData
					omUrnoMap.SetAt((void *)prlAcc->Urno, prlAcc);

					CString olTmp;
					olTmp.Format("%ld-%s-%s",prlAcc->Stfu, prlAcc->Year, prlAcc->Type);
					omKeyMap.SetAt(olTmp,prlAcc);
				}
				else
				{
					delete prlAcc;
				}
			}
			else
			{
				delete prlAcc;
			}
		}
	}
	else
	{
		//Load WITHOUT LoadStfuUrnoMap
		CString olTmp;
		void *prlVoid;
		for(ilLc = 0; ilRc == true; ilLc++)
		{
			ACCDATA *prlAcc = new ACCDATA;
			if ((ilRc = GetFirstBufferRecord(prlAcc)) == true)
			{
				olTmp.Format("%ld-%s-%s",prlAcc->Stfu, prlAcc->Year, prlAcc->Type);
				if(omKeyMap.Lookup(olTmp, (void *&)prlVoid) == FALSE)
				{
					omData.Add(prlAcc);//Update omData
					omUrnoMap.SetAt((void *)prlAcc->Urno, prlAcc);					
					omKeyMap.SetAt(olTmp,prlAcc);
				}
				else
				{
					UpdateInternal(prlAcc,false);
				}
			}
			else
			{
				delete prlAcc;
			}
		}
	}
	TRACE("Read-Acc: %d gelesen\n",ilLc-1);
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaAccData::Insert(ACCDATA *prpAcc)
{
	prpAcc->IsChanged = DATA_NEW;
	if(Save(prpAcc) == false) return false; //Update Database
	InsertInternal(prpAcc);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaAccData::InsertInternal(ACCDATA *prpAcc, bool bpSendDdx /*=true*/)
{

	CString olTmp;
	olTmp.Format("%ld-%s-%s",prpAcc->Stfu,prpAcc->Year, prpAcc->Type);

	ACCDATA *prlData;
	// übergebener Datensatz NULL oder Datensatz mit diesem Schlüssel
	// (Schichttag,Schichtnummer,MA-Urno) schon vorhanden?

	if(omKeyMap.Lookup(olTmp,(void *&)prlData)  == TRUE){
		return false;
	}

	omData.Add(prpAcc);//Update omData
	omUrnoMap.SetAt((void *)prpAcc->Urno,prpAcc);
	omKeyMap.SetAt(olTmp,prpAcc);

	if(bpSendDdx)
	{
		ogDdx.DataChanged((void *)this, ACC_NEW,(void *)prpAcc ); //Update Viewer
	}
	
   return true;
}


//--DELETE-------------------------------------------------------------------------------------------------

bool CedaAccData::Delete(long lpUrno)
{
	ACCDATA *prlAcc = GetAccByUrno(lpUrno);
	if (prlAcc != NULL)
	{
		prlAcc->IsChanged = DATA_DELETED;
		if(Save(prlAcc) == false) return false; //Update Database
		DeleteInternal(prlAcc);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaAccData::DeleteInternal(ACCDATA *prpAcc, bool bpSendDdx /*=true*/)
{
	if(bpSendDdx)
	{
		ogDdx.DataChanged((void *)this,ACC_DELETE,(void *)prpAcc); //Update Viewer
	}
	omUrnoMap.RemoveKey((void *)prpAcc->Urno);

	CString olTmp;
	olTmp.Format("%ld-%s-%s",prpAcc->Stfu, prpAcc->Year, prpAcc->Type);
	omKeyMap.RemoveKey(olTmp);

	int ilAccCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilAccCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpAcc->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaAccData::Update(ACCDATA *prpAcc,bool bpWithDdx /*true*/)
{
	if (GetAccByUrno(prpAcc->Urno) != NULL)
	{
		if (prpAcc->IsChanged == DATA_UNCHANGED)
		{
			prpAcc->IsChanged = DATA_CHANGED;
		}
		if(Save(prpAcc) == false) return false; //Update Database
		UpdateInternal(prpAcc,bpWithDdx);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaAccData::UpdateInternal(ACCDATA *prpAcc, bool bpSendDdx /*=true*/)
{
	bool blRet = false;
	if(prpAcc==NULL)
		return blRet;
	ACCDATA *prlAcc = GetAccByUrno(prpAcc->Urno);
	if (prlAcc != NULL)
	{		
		*prlAcc = *prpAcc; //Update omData
		if(bpSendDdx)
		{
			ogDdx.DataChanged((void *)this,ACC_CHANGE,(void *)prlAcc); //Update Viewer
		}
		blRet = true;
	}
    return blRet;
}

//--RELOAD-------------------------------------------------------------------

bool CedaAccData::Reload(CString opDateFrom, CString opDateTo, CString opType, CString opUrnos, CString opCmd,bool bpWithUrnos /*false*/)
{
	ACCDATA *prlAcc = NULL;
	CStringArray olUrnoArray;

	CString olYearFr = opDateFrom.Left(4);
	CString olYearTo = opDateTo.Left(4);

	int ilUSize = ExtractItemList(opUrnos,&olUrnoArray, ','); 
	
	CString olTmpYear = olYearFr;
	//Delete all released Data from omData
	while(olTmpYear <= olYearTo && olTmpYear >= olYearFr)
	{
		for(int ilU=0; ilU<ilUSize; ilU++)
		{
			prlAcc = GetAccByKey(atol(olUrnoArray[ilU]), olTmpYear, opType);
			if(prlAcc != NULL)
			{
				DeleteInternal(prlAcc, false);
			}
		}
		int ilTmpYear = atoi(olTmpYear) + 1;
		olTmpYear.Format("%d",ilTmpYear);
	}
	
	CString olTmp;
	//Read all released Data new
	CString olWhere;
	char pclWhere[2000] = " ";
	bool blOk = true;
	while(olUrnoArray.GetSize() > 0)
	{
		int ilUrnos = 0;
		CString olTmpUrnos;
		while(ilUrnos < 99 && olUrnoArray.GetSize() > 0)
		{
			ilUrnos++;
			olTmp.Format("'%s'",olUrnoArray[0]);
			olTmpUrnos += CString(",") + olTmp;
			olUrnoArray.RemoveAt(0);
		}
		olTmpUrnos = olTmpUrnos.Mid(1);

		olWhere.Format("WHERE %s IN (%s) AND TYPE = '%s' AND YEAR BETWEEN '%s' AND '%s'",bpWithUrnos ? "URNO":"STFU",olTmpUrnos, opType, olYearFr, olYearTo);
		strcpy(pclWhere, olWhere);
		blOk = ogAccData.Read(pclWhere,false);
	}

	ReloadInternal();
    return blOk;
}

//--RELOAD INTERNAL----------------------------------------------------------

bool CedaAccData::ReloadInternal()
{
	ogDdx.DataChanged((void *)this,RELACC,NULL); //Update Viewer ect.
    return true;
}


//--GET-BY-URNO--------------------------------------------------------------------------------------------

ACCDATA* CedaAccData::GetAccByUrno(long lpUrno)
{
	ACCDATA  *prlAcc;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlAcc) == TRUE)
	{
		return prlAcc;
	}
	return NULL;
}

//--GET BY KEY---------------------------------------------------------------

ACCDATA *CedaAccData::GetAccByKey(long lpStfUrno, CString opYear, CString opType)
{
	CString olTmp;
	olTmp.Format("%ld-%s-%s",lpStfUrno, opYear, opType);

	ACCDATA  *prlAcc = NULL;
	if (omKeyMap.Lookup(olTmp,(void *&)prlAcc) == TRUE)
	{
		return prlAcc;
	}
	return NULL;
}

//--------------------------------------------------------------------------------------

CString CedaAccData::GetFieldList()
{
	CString olFieldList = CString(pcmFieldList);
	return olFieldList;
}

//--------------------------------------------------------------------------------------

bool CedaAccData::IsDataChanged(ACCDATA *prpOld, ACCDATA *prpNew)
{
	CString olFieldList;
	CString olListOfData;

	olFieldList = GetFieldList();
	MakeCedaData(olListOfData, olFieldList, (void*)prpOld, (void*)prpNew);
	if(olFieldList.IsEmpty())
	{
		return false;
	}
	else
	{
		return true;
	}

}
//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaAccData::ReadSpecialData(CCSPtrArray<ACCDATA> *popAcc,char *pspWhere,char *pspFieldList,bool ipSYS/*=false*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popAcc != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			ACCDATA *prpAcc = new ACCDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpAcc,CString(pclFieldList))) == true)
			{
				popAcc->Add(prpAcc);
			}
			else
			{
				delete prpAcc;
			}
		}
		if(popAcc->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaAccData::Save(ACCDATA *prpAcc)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpAcc->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpAcc->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpAcc);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpAcc->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpAcc->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpAcc);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpAcc->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpAcc->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessAccCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogAccData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//****************************************************************************************
// Broadcasts bearbeiten
//****************************************************************************************

void  CedaAccData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlBcStruct = (struct BcStruct *) vpDataPointer;
	ACCDATA *prlAcc;
	long llUrno;
	CString olSelection;

	switch(ipDDXType)
	{
	default:
		break;
	case BC_ACC_NEW:
		prlAcc = new ACCDATA;
		GetRecordFromItemList(prlAcc,prlBcStruct->Fields,prlBcStruct->Data);
		InsertInternal(prlAcc);
		break;
	case BC_ACC_CHANGE:
		llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
		prlAcc = GetAccByUrno(llUrno);
		if(prlAcc != NULL)
		{
			GetRecordFromItemList(prlAcc,prlBcStruct->Fields,prlBcStruct->Data);
			UpdateInternal(prlAcc);
		}
		break;
	case BC_ACC_DELETE:
		olSelection = (CString)prlBcStruct->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlAcc = GetAccByUrno(llUrno);
		if (prlAcc != NULL)
		{
			DeleteInternal(prlAcc);
		}
		break;
	case BC_DISABLEACCSAVE:
		bmCanSave = false;
		break;
	case BC_ENABLEACCSAVE:
		bmCanSave = true;
		break;
	case BC_RELACC:
		//USED !
		MakeClientString(prlBcStruct->Data);
		CString olData  = prlBcStruct->Data;
		CStringArray olDataArray;
		if(ExtractItemList(olData,&olDataArray, '-') == 5)
		{
			Reload(olDataArray[0], olDataArray[1], olDataArray[2], olDataArray[3], olDataArray[4],true);
		}
		break;
	}
}

//--RELEASE------------------------------------------------------------------

bool CedaAccData::Release(CString opDateFrom, CString opDateTo, CStringArray *popTypeArray , CStringArray *popUrnoArray, CString opCmd /*="R"*/)
{
	CStringArray olUrnoArray;
	olUrnoArray.Copy(*popUrnoArray);

	CString olIRT;
	CString olURT;
	CString olDRT;
	CString olFieldList;
	CString olDataList;
	CString olUpdateString;
	CString olInsertString;
	CString olDeleteString;

	int ilNoUpdates = 0;
	int ilNoDeletes = 0;
	int ilNoInserts = 0;
	CStringArray olFields;
	int ilFields = ExtractItemList(CString(pcmListOfFields),&olFields); 
	olIRT.Format("*CMD*,%s,IRT,%d,", pcmTableName, ilFields);
	olURT.Format("*CMD*,%s,URT,%d,", pcmTableName, ilFields);
	olDRT.Format("*CMD*,%s,DRT,-1,", pcmTableName);

	CString olOrigFieldList;
	int ilCurrentCount = 0;
	int ilTotalCount = 0;

	olOrigFieldList = CString(pcmListOfFields);
	olInsertString = olIRT + CString(pcmListOfFields) + CString("\n");
	olUpdateString = olURT + CString(pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
	olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ CString("\n");;

	POSITION rlPos;
	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		ACCDATA *prlAcc;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlAcc);
		if(prlAcc->IsChanged != DATA_UNCHANGED )
		{
			CString olListOfData;
			CString olCurrentUrno;
			olCurrentUrno.Format("%d",prlAcc->Urno);
			MakeCedaData(&omRecInfo,olListOfData,prlAcc);
			switch(prlAcc->IsChanged)
			{
			case DATA_NEW:
				olInsertString += olListOfData + CString("\n");
				ilCurrentCount++;
				ilTotalCount++;
				ilNoInserts++;
				break;
			case DATA_CHANGED:
				olUpdateString += olListOfData + CString(",") + olCurrentUrno + CString("\n");
				ilNoUpdates++;
				ilTotalCount++;
				ilCurrentCount++;
				break;
			case DATA_DELETED:
				olDeleteString += olCurrentUrno + CString("\n");
				ilCurrentCount++;
				ilTotalCount++;
				ilNoDeletes++;
				break;
			}
			prlAcc->IsChanged = DATA_UNCHANGED;
		}
		if(ilCurrentCount == 50)
		{
			if(ilNoInserts > 0)
			{
				if(CedaAction("REL","LATE","",olInsertString.GetBuffer(0)) == false) return false;
				strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
			if(ilNoUpdates > 0)
			{
				if(CedaAction("REL","LATE","",olUpdateString.GetBuffer(0)) == false) return false;
				strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
			if(ilNoDeletes > 0)
			{
				if(CedaAction("REL","LATE","",olDeleteString.GetBuffer(0)) == false) return false;
				strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
			ilCurrentCount = 0;
			ilNoInserts = 0;
			ilNoUpdates = 0;
			ilNoDeletes = 0;
			olInsertString = olIRT + CString(pcmListOfFields) + CString("\n");
			olUpdateString = olURT + CString(pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
			olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ CString("\n");;
		}
	}
	if(ilNoInserts > 0)
	{
		if(CedaAction("REL","LATE","",olInsertString.GetBuffer(0)) == false) return false;
		strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
	}
	if(ilNoUpdates > 0)
	{
		//if(CedaAction("REL","LATE","",olUpdateString.GetBuffer(0)) == false) return false;
		if(CedaAction("REL","QUICK","",olUpdateString.GetBuffer(0)) == false) return false;
		strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
	}
	if(ilNoDeletes > 0)
	{
		if(CedaAction("REL","LATE","",olDeleteString.GetBuffer(0)) == false) return false;
		strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
	}

	char pclData[800];
	CString olData;
	if(ilTotalCount>0)
	{
		while(olUrnoArray.GetSize() > 0)
		{
			int ilUrnos = 0;
			CString olTmpUrnos;
			while(ilUrnos < 60 && olUrnoArray.GetSize() > 0)
			{
				ilUrnos++;
				olTmpUrnos += CString(",") + olUrnoArray[0];
				olUrnoArray.RemoveAt(0);
			}
			olTmpUrnos = olTmpUrnos.Mid(1);

			for (int i=0;i<popTypeArray->GetSize();i++)
			{
				// Alle übergebenen Kontotypen durchgehen
				olData.Format("%s-%s-%s-%s-%s", opDateFrom, opDateTo, popTypeArray->GetAt(i), olTmpUrnos, opCmd);
				MakeCedaString(olData);
				strcpy(pclData, olData);
				CedaAction("SBC","RELACC", "", "", "", pclData);
			}

		}
		ReloadInternal();
	}
	return true;
}
