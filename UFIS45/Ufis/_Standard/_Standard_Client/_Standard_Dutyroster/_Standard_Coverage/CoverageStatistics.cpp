// CoverageStatistics.cpp : implementation file
//

#include <stdafx.h>
#include <Coverage.h>
#include <CoverageStatistics.h>
#include <CedaBasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern bool check; 

/////////////////////////////////////////////////////////////////////////////
// CCoverageStatistics dialog


CCoverageStatistics::CCoverageStatistics(CWnd* pParent /*=NULL*/,CoverageDiagramViewer *popViewer)
	: CDialog(CCoverageStatistics::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCoverageStatistics)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	if (popViewer!=NULL)
	{
		AttachViewer(popViewer);
	}
	pomParent = pParent;

}


CCoverageStatistics::~CCoverageStatistics()
{
	omShiftArray.RemoveAll();
	omFlightValues.RemoveAll();
}

void CCoverageStatistics::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCoverageStatistics)
	DDX_Control(pDX, IDC_UNDER, m_Under);
	DDX_Control(pDX, IDC_OVER, m_Over);
	DDX_Control(pDX, IDC_TOTIME, m_ToTime);
	DDX_Control(pDX, IDC_TODATE, m_ToDate);
	DDX_Control(pDX, IDC_FROMTIME, m_FromTime);
	DDX_Control(pDX, IDC_FROMDATE, m_FromDate);
	DDX_Control(pDX, IDC_WORKTIME, m_Worktime);
	DDX_Control(pDX, IDC_DEMTIME,  m_Demandtime);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCoverageStatistics, CDialog)
	//{{AFX_MSG_MAP(CCoverageStatistics)
	ON_EN_KILLFOCUS(IDC_TOTIME, OnKillfocusToTime)
	ON_EN_KILLFOCUS(IDC_TODATE, OnKillfocusToDate)
	ON_EN_KILLFOCUS(IDC_FROMTIME, OnKillfocusFromTime)
	ON_EN_KILLFOCUS(IDC_FROMDATE, OnKillfocusFromDate)

	ON_BN_CLICKED(IDC_COST, OnCost)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CCoverageStatistics::UpdateDisplay(CUIntArray &opShiftArray,CUIntArray &opFlightValues) 
{
	CCS_TRY

	CTime olLoadStart = TIMENULL;
	CTime olLoadEnd = TIMENULL;
	pomViewer->GetTimeFrameFromTo(olLoadStart, olLoadEnd);

	if((omLoadStartTime != olLoadStart && olLoadStart != TIMENULL) || (omLoadEndTime != olLoadEnd && olLoadEnd != TIMENULL))
	{
		omLoadStartTime = olLoadStart;
		CString olFromDate = omLoadStartTime.Format("%d.%m.%Y");
		CString olFromTime = omLoadStartTime.Format("%H:%M");
		m_FromTime.SetWindowText(olFromTime);
		m_FromDate.SetWindowText(olFromDate);
		omStartTime = omLoadStartTime;
		omLoadEndTime = olLoadEnd;
		 olFromDate = omLoadEndTime.Format("%d.%m.%Y");
		 olFromTime = omLoadEndTime.Format("%H:%M");
		m_ToTime.SetWindowText(olFromTime);
		m_ToDate.SetWindowText(olFromDate);
		omEndTime = omLoadEndTime;
	}


	CTimeSpan olTimeSpan = (omLoadEndTime - omLoadStartTime);
	imXMinutes = olTimeSpan.GetTotalMinutes();
	CString olStartDate = omStartTime.Format("%Y.%m.%d-%H:%M");
	CString olEndDate = omEndTime.Format("%Y.%m.%d-%H:%M");

	int ilNoOfDemandShift = 0;

	ilNoOfDemandShift = min(imXMinutes ,opFlightValues.GetSize());
	ilNoOfDemandShift = min(ilNoOfDemandShift ,opShiftArray.GetSize());
	omShiftArray.RemoveAll();
	omFlightValues.RemoveAll();
	for(int i=0;i<ilNoOfDemandShift;i++)
	{
		omShiftArray.Add(opShiftArray[i]);
		omFlightValues.Add(opFlightValues[i]);
	}
	imXMinutes = ilNoOfDemandShift;

	Evaluate();

	CCS_CATCH_ALL
}


void CCoverageStatistics::Evaluate()
{

	CTimeSpan olTimeSpan = (omLoadEndTime - omLoadStartTime);
	CTimeSpan olTimeSpan2 = (omEndTime - omStartTime);
	imXMinutes = olTimeSpan.GetTotalMinutes();
		
	imExcess = 0;
	imDeficit = 0;
	imDemand = 0;
	int ilNoOfDemandShift = olTimeSpan2.GetTotalMinutes();

	imOffSet = (omStartTime - omLoadStartTime).GetTotalMinutes();
	if(imOffSet < 0)
		imOffSet = 0;
	
	ilNoOfDemandShift = min(ilNoOfDemandShift + imOffSet ,omFlightValues.GetSize());
	
	for(int i=imOffSet;i<ilNoOfDemandShift;i++)
	{
//		if(omFlightValues.GetAt(i)>0)
		{
//			imDemand += max(omFlightValues.GetAt(i),omShiftArray.GetAt(i)) ;
			imDemand += omFlightValues.GetAt(i) ;
			int ilDiff =  omShiftArray.GetAt(i) - omFlightValues.GetAt(i);
			if(ilDiff>0)
			{
				imExcess += abs(ilDiff);
			}
			else if(ilDiff<0)
			{
				imDeficit += abs(ilDiff);
			}
		}	
	}


	CString olTotalHours;

	olTotalHours.Format("%d",imDemand / 60);
	m_Demandtime.SetWindowText(olTotalHours);

	if(imDemand==0)
	{
		imDemand=1;
	}
	float ilOver  = 100 * ((float)imExcess/(float) imDemand);
	float ilUnder = 100 * ((float)imDeficit/(float)imDemand);
	CString olOver,olUnder;
	olOver.Format("%.2f",ilOver);
	olUnder.Format("%.2f",ilUnder);
	CString olDemand;
	olDemand.Format("%.2d",imDemand);
	m_Over.SetWindowText(olOver);
	m_Under.SetWindowText(olUnder);

	CTimeSpan olWorktime = this->pomViewer->CalculateWorktime(omStartTime,omEndTime);

	olTotalHours.Format("%d",olWorktime.GetTotalHours());
	m_Worktime.SetWindowText(olTotalHours);
}



void CCoverageStatistics::AttachViewer(CoverageDiagramViewer *popViewer)
{
	CCS_TRY
		pomViewer = popViewer;
	CCS_CATCH_ALL
}


/////////////////////////////////////////////////////////////////////////////
// CCoverageStatistics message handlers



BOOL CCoverageStatistics::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CRect rect;
	GetWindowRect(rect);
	if(pomParent!=NULL)
	{
		CRect pRect;
		pomParent->GetWindowRect(pRect);
		int ilWidt = rect.right-rect.left;
		int ilHeigh = rect.bottom-rect.top;
		MoveWindow( pRect.right-ilWidt,pRect.bottom-ilHeigh, ilWidt,ilHeigh);
	}
	else
	{
		MoveWindow( 400,100, (rect.right-rect.left),(rect.bottom-rect.top));
	}

	CWnd *polWnd = GetDlgItem(IDCANCEL);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61214));
	}
	polWnd = GetDlgItem(IDOK);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING998));
	}
	polWnd = GetDlgItem(IDC_ZEITRAUM);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61264));
	}
	polWnd = GetDlgItem(IDC_VON);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61265));
	}
	polWnd = GetDlgItem(IDC_BIS);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61266));
	}
	polWnd = GetDlgItem(IDC_UEBER);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61267));
	}
	polWnd = GetDlgItem(IDC_UNTER);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61268));
	}
	polWnd = GetDlgItem(IDC_WORKTIME_TEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1804));
	}
	polWnd = GetDlgItem(IDC_DEMTIME_TEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1964));
	}


	SetWindowText(LoadStg(IDS_STRING61263));



	pomDemandData=NULL;
	//pomTimeScale=NULL;
	//MoveWindow( 400,100, (rect.right-rect.left),(rect.bottom-rect.top));
	
	// TODO: Add extra initialization here
	if(pomViewer != NULL)
	{
		pomViewer->GetTimeFrameFromTo(omStartTime, omEndTime);
		CTimeSpan olTimeSpan  = omEndTime-omStartTime;
		imXMinutes = olTimeSpan.GetTotalMinutes();
	}
	CString olFromDate = omStartTime.Format("%d.%m.%Y");
	CString olFromTime = omStartTime.Format("%H:%M");
	CString olToDate = omEndTime.Format("%d.%m.%Y");
	CString olToTime = omEndTime.Format("%H:%M");

	omLoadStartTime = omStartTime;
	omLoadEndTime = omEndTime;

	m_ToTime.SetWindowText(olToTime);
	m_ToDate.SetWindowText(olToDate);
	m_FromTime.SetWindowText(olFromTime);
	m_FromDate.SetWindowText(olFromDate);

	


    GetDlgItem(IDC_EDIT1)->SetWindowText("0");
    GetDlgItem(IDC_EDIT24)->SetWindowText("0");			
	GetDlgItem(IDC_EDIT3)->SetWindowText("0");
	GetDlgItem(IDC_EDIT5)->SetWindowText("0");
	GetDlgItem(IDC_EDIT4)->SetWindowText("0");


	
if (check)
{

    GetDlgItem(IDC_EDIT1)->ShowWindow(1);
    GetDlgItem(IDC_EDIT24)->ShowWindow(1);			
	GetDlgItem(IDC_EDIT3)->ShowWindow(1);
	GetDlgItem(IDC_EDIT5)->ShowWindow(1);
	GetDlgItem(IDC_EDIT4)->ShowWindow(1);

/*
	GetDlgItem(IDC_STATIC1)->ShowWindow(1);
    GetDlgItem(IDC_STATIC2)->ShowWindow(0);
	GetDlgItem(IDC_STATIC3)->ShowWindow(0);
    GetDlgItem(IDC_STATIC4)->ShowWindow(0);
	GetDlgItem(IDC_STATIC5)->ShowWindow(0);
    GetDlgItem(IDC_STATIC6)->ShowWindow(0);
	GetDlgItem(IDC_STATIC7)->ShowWindow(0);
    GetDlgItem(IDC_STATIC8)->ShowWindow(0);
	GetDlgItem(IDC_COST)->ShowWindow(0);
*/

}
	
else 
{

    GetDlgItem(IDC_EDIT1)->ShowWindow(0);
    GetDlgItem(IDC_EDIT24)->ShowWindow(0);			
	GetDlgItem(IDC_EDIT3)->ShowWindow(0);
	GetDlgItem(IDC_EDIT5)->ShowWindow(0);
	GetDlgItem(IDC_EDIT4)->ShowWindow(0);
    GetDlgItem(IDC_STATIC1)->ShowWindow(0);
    GetDlgItem(IDC_STATIC2)->ShowWindow(0);
	GetDlgItem(IDC_STATIC3)->ShowWindow(0);
    GetDlgItem(IDC_STATIC4)->ShowWindow(0);
	GetDlgItem(IDC_STATIC5)->ShowWindow(0);
    GetDlgItem(IDC_STATIC6)->ShowWindow(0);
	GetDlgItem(IDC_STATIC7)->ShowWindow(0);
    GetDlgItem(IDC_STATIC8)->ShowWindow(0);
	GetDlgItem(IDC_COST)->ShowWindow(0);

	CWnd::SetWindowPos(NULL,0,0, 216, 298,SWP_NOZORDER|SWP_NOMOVE);

}	
	
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCoverageStatistics::OnKillfocusToTime() 
{
	CString olValue;

	m_ToTime.GetWindowText(olValue);


	CTime olNewStart = HourStringToDate(olValue, omEndTime);

	if(olNewStart == TIMENULL)
	{
		MessageBox(LoadStg(IDS_NO_DATE_TIME), LoadStg(IDS_WARNING), (MB_OK|MB_ICONINFORMATION));
		CString olToTime = omEndTime.Format("%H:%M");
		m_ToTime.SetWindowText(olToTime);
		m_ToTime.SetFocus();
		return;
	}
	if(olNewStart > omLoadEndTime)
	{
		MessageBox(LoadStg(IDS_STRING61252), LoadStg(IDS_WARNING), (MB_OK|MB_ICONINFORMATION));
		CString olToTime = omEndTime.Format("%H:%M");
		m_ToTime.SetWindowText(olToTime);
		m_ToTime.SetFocus();
		return;
	}
	if(olNewStart < omStartTime)
	{
		MessageBox(LoadStg(IDS_STRING61253), LoadStg(IDS_WARNING), (MB_OK|MB_ICONINFORMATION));
		CString olToTime = omEndTime.Format("%H:%M");
		m_ToTime.SetWindowText(olToTime);
		m_ToTime.SetFocus();
		return;
	}
	omEndTime = olNewStart; 
	Evaluate();
}

void CCoverageStatistics::OnKillfocusToDate() 
{
	CString olValue;

	m_ToDate.GetWindowText(olValue);


	CTime olNewStart = DateStringToDate(olValue);

	if(olNewStart == TIMENULL)
	{
		MessageBox(LoadStg(IDS_NO_DATE_TIME), LoadStg(IDS_WARNING), (MB_OK|MB_ICONINFORMATION));
		CString olToDate = omEndTime.Format("%d.%m.%Y");
		m_ToDate.SetWindowText(olToDate);
		m_ToDate.SetFocus();
		return;
	}
		
	m_ToTime.GetWindowText(olValue);

	CTime olNewStart2 = HourStringToDate(olValue, olNewStart);

	if(olNewStart2 > omLoadEndTime)
	{
		MessageBox(LoadStg(IDS_STRING61252), LoadStg(IDS_WARNING), (MB_OK|MB_ICONINFORMATION));
		CString olToDate = omEndTime.Format("%d.%m.%Y");
		m_ToDate.SetWindowText(olToDate);
		m_ToDate.SetFocus();
		return;
	}
	if(olNewStart2 < omStartTime)
	{
//		MessageBox(LoadStg(IDS_STRING61253), LoadStg(IDS_WARNING), (MB_OK|MB_ICONINFORMATION));
		CString olToDate = omStartTime.Format("%d.%m.%Y");
		m_ToDate.SetWindowText(olToDate);
		CString olToTime = omStartTime.Format("%H:%M");
		m_ToTime.SetWindowText(olToTime);
		
		return;
	}
	omEndTime = olNewStart2; 
	Evaluate();
}

void CCoverageStatistics::OnKillfocusFromTime() 
{
	CString olValue;

	m_FromTime.GetWindowText(olValue);

	CTime olNewStart = HourStringToDate(olValue, omStartTime);

	if(olNewStart == TIMENULL)
	{
		MessageBox(LoadStg(IDS_NO_DATE_TIME), LoadStg(IDS_WARNING), (MB_OK|MB_ICONINFORMATION));
		CString olToTime = omStartTime.Format("%H:%M");
		m_FromTime.SetWindowText(olToTime);
		m_FromTime.SetFocus();
		return;
	}
	if(olNewStart < omLoadStartTime)
	{
		MessageBox(LoadStg(IDS_STRING61252), LoadStg(IDS_WARNING), (MB_OK|MB_ICONINFORMATION));
		CString olToTime = omStartTime.Format("%H:%M");
		m_FromTime.SetWindowText(olToTime);
		m_FromTime.SetFocus();
		return;
	}
	if(olNewStart > omEndTime)
	{
		MessageBox(LoadStg(IDS_STRING61253), LoadStg(IDS_WARNING), (MB_OK|MB_ICONINFORMATION));
		CString olToTime = omStartTime.Format("%H:%M");
		m_FromTime.SetWindowText(olToTime);
		m_FromTime.SetFocus();
		return;
	}
	omStartTime = olNewStart; 

	Evaluate();
}

void CCoverageStatistics::OnKillfocusFromDate() 
{
	CString olValue;

	m_FromDate.GetWindowText(olValue);

	CTime DateStringToDate(CString &opString);
	CTime olNewStart = DateStringToDate(olValue);

	if(olNewStart == TIMENULL)
	{
		MessageBox(LoadStg(IDS_NO_DATE_TIME), LoadStg(IDS_WARNING), (MB_OK|MB_ICONINFORMATION));
		CString olToDate = omStartTime.Format("%d.%m.%Y");
		m_FromDate.SetWindowText(olToDate);
		m_FromDate.SetFocus();
		return;
	}
	m_FromTime.GetWindowText(olValue);

	CTime olNewStart2 = HourStringToDate(olValue, olNewStart);

	if(olNewStart2 < omLoadStartTime)
	{
		MessageBox(LoadStg(IDS_STRING61252), LoadStg(IDS_WARNING), (MB_OK|MB_ICONINFORMATION));
		CString olToDate = omStartTime.Format("%d.%m.%Y");
		m_FromDate.SetWindowText(olToDate);
		m_FromDate.SetFocus();
		return;
	}
	if(olNewStart2 > omEndTime)
	{
//		MessageBox(LoadStg(IDS_STRING61253), LoadStg(IDS_WARNING), (MB_OK|MB_ICONINFORMATION));
		CString olToDate = omEndTime.Format("%d.%m.%Y");
		m_FromDate.SetWindowText(olToDate);
		CString olToTime = omEndTime.Format("%H:%M");
		m_FromTime.SetWindowText(olToTime);
		return;
	}
	omStartTime = olNewStart2; 
	Evaluate();
}


void CCoverageStatistics::OnOK() 
{

}

void CCoverageStatistics::OnCost()
{
       
	CString str3;
	GetDlgItem(IDC_WORKTIME)->GetWindowText(str3);
    double worktime = atof((LPCTSTR)str3);
 
	CString str5;
	GetDlgItem(IDC_EDIT1)->GetWindowText(str5);
    double rsuc = atof((LPCTSTR)str5);
  
    double rsc= worktime*rsuc;

	CString str7;
	str7.Format("%.2f",rsc);
	GetDlgItem(IDC_EDIT3)->SetWindowText(str7);



 
	CString str2;
	GetDlgItem(IDC_UNDER)->GetWindowText(str2);
    double under = atof((LPCTSTR)str2);
	under /=100; 

   	CString str4;
	GetDlgItem(IDC_DEMTIME)->GetWindowText(str4);
    double demtime = atof((LPCTSTR)str4);

    CString str6;
	GetDlgItem(IDC_EDIT24)->GetWindowText(str6);
    double osuc = atof((LPCTSTR)str6);

    double osc= under*demtime*osuc;

    
	CString str8;
	str8.Format("%.2f",osc);
	GetDlgItem(IDC_EDIT5)->SetWindowText(str8);


    double tc =osc+rsc;

   CString str9;
	str9.Format("%.2f",tc);
	GetDlgItem(IDC_EDIT4)->SetWindowText(str9);


   

}