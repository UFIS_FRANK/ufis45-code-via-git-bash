#ifndef _DATASET_H_
#define _DATASET_H_
 

#include <ccsglobl.h>
#include <BasicData.h>
#include <CedaFlightData.h>
#include <CedaSdtData.h>
#include <CedaSdgData.h>
#include <CedaMsdData.h>
#include <CedaBsdData.h>
#include <CoverageDiaViewer.h>
#include <ConflictCheck.h>

#define REGEL_T			31
#define REGEL_A			17 //Annahme
#define REGEL_B			17 //Bereitstellung
#define EQUAL			0
#define FIND_IN_FLIGHT	1
#define FIND_IN_RULE	2
/////////////////////////////////////////////////////////////////////////////
// DataSet class

class DataSet
{
// Job creation routine
public:
	void TerminatePredecessor(long lpSdgu);
	DataSet();
	~DataSet();
	void CreateSdtFromBsd(BASICSHIFTS *prpBsd, CString opName);
	void CreateSdtFromDrrSingle(CString olDrrUrno);
	void CreateAutoSdtByBsh(BASICSHIFTS *prpBsh, CString opPfcCode,CTime opShiftStart,
								 CTime opShiftEnd,int ipBreakOffSet, long lpSdgu);
	void CreateSdtFromDrr();
	void CheckAttention();
	void CheckFlightsAfterBroadcast(FLIGHTDATA *prpFlight);
	void CreateSdtFromBsdAndSdg(BASICSHIFTS *prpBsh, long lpSdgu, CTime opLoadStart, CTime opLoadEnd, CString opPfcCode);
	void CreateMsdFromBsdAndSdg(BASICSHIFTS *prpBsh, long lpSdg, CTime opLoadStart, CTime opLoadEnd, CString opPfcCode, bool bpSave = true);
	void CreateVirtuellSdtsBySdgu(long lpSdgu);
	void CreateVirtuellSdtsByMsduAndSdgu(long lpMsdu,long lpSdgu);
    bool DeleteMsdByMsduAndSdgu(long lpMsdu,long lpSdgu,CTime opShiftStart,bool bpWarningDisplay = true, bool bpSave = true);
	void CreateSdtFromSpl(const CString& ropSname,CTime opLoadStart, CTime opLoadEnd);
	void CreateSdtFromGpls(CTime opLoadStart, CTime opLoadEnd);
	void CreateSdtFromSingleGpl(CString opUrno,CString opGsnm,CString opSplu,CString opVafr,CString opVato,CTime opLoadStart,CTime opLoadEnd);
	void CreateSdtFromSingleGsp(CString opUrno,CString opGplu,CString opSplu,CString opVafr,CString opVato,CTime opLoadStart,CTime opLoadEnd);

	void ReleaseAutoSdts(bool blCreateMsd);

private:
	
};

extern DataSet ogDataSet;
#endif	// _DATASET_H_

