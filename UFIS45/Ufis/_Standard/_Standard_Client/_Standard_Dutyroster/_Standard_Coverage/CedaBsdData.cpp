// CedaBsdData.cpp
  
#include <stdafx.h>
#include <CedaBsdData.h>
//#include <iostream.h>
#include <algorithm>


void ProcessBsdCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

static int OrderByEsbg(const BSDDATA **e1, const BSDDATA **e2)
{
	CTime olCurrent = CTime::GetCurrentTime();
	CTime olFrom1 = HourStringToDate(CString((**e1).Esbg),olCurrent);
	CTime olTo1   = HourStringToDate(CString((**e1).Lsen),olCurrent);
	CTime olFrom2 = HourStringToDate(CString((**e2).Esbg),olCurrent);
	CTime olTo2   = HourStringToDate(CString((**e2).Lsen),olCurrent);

#if	0
	if ((**e1).Esbg < (**e2).Esbg)
		return -1;
	else if ((**e1).Esbg > (**e2).Esbg)
		return 1;
	else
	{
		if ((**e1).Lsen < (**e2).Lsen)
			return -1;
		else if ((**e1).Lsen > (**e2).Lsen)
			return 1;
		else
			return 0;
	}
#else
	if (olFrom1 < olFrom2)
		return -1;
	else if (olFrom1 > olFrom2)
		return 1;
	else
	{
		if (olTo1 < olTo2)
			return -1;
		else if (olTo1 > olTo2)
			return 1;
		else
			return 0;
	}
#endif
}

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaBsdData::CedaBsdData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for BSDDataStruct
	BEGIN_CEDARECINFO(BSDDATA,BSDDataRecInfo)
		FIELD_CHAR_TRIM(Bewc,"BEWC")
		FIELD_CHAR_TRIM(Bkd1,"BKD1")
		FIELD_CHAR_TRIM(Bkf1,"BKF1")
		FIELD_CHAR_TRIM(Bkr1,"BKR1")
		FIELD_CHAR_TRIM(Bkt1,"BKT1")
		FIELD_CHAR_TRIM(Bsdc,"BSDC")
		FIELD_CHAR_TRIM(Bsdk,"BSDK")
		FIELD_CHAR_TRIM(Bsdn,"BSDN")
		FIELD_CHAR_TRIM(Bsds,"BSDS")
		FIELD_DATE	   (Cdat,"CDAT")
		FIELD_CHAR_TRIM(Ctrc,"CTRC")
		FIELD_CHAR_TRIM(Esbg,"ESBG")
		FIELD_CHAR_TRIM(Lsen,"LSEN")
		FIELD_DATE	   (Lstu,"LSTU")
		FIELD_CHAR_TRIM(Prfl,"PRFL")
		FIELD_CHAR_TRIM(Sdu1,"SDU1")
		FIELD_CHAR_TRIM(Sex1,"SEX1")
		FIELD_CHAR_TRIM(Ssh1,"SSH1")
		FIELD_CHAR_TRIM(Type,"TYPE")
		FIELD_LONG	   (Urno,"URNO")
		FIELD_CHAR_TRIM(Dpt1,"DPT1")
		FIELD_CHAR_TRIM(Fctc,"FCTC")
		FIELD_CHAR_TRIM(Fdel,"FDEL")
		FIELD_CHAR_TRIM(Rgsb,"RGSB")
		FIELD_DATE	   (Vafr,"VAFR")
		FIELD_DATE	   (Vato,"VATO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		//
		FIELD_CHAR_TRIM	(Rgbc,"RGBC")
	END_CEDARECINFO //(BSDDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(BSDDataRecInfo)/sizeof(BSDDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&BSDDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"BSD");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,"BEWC,BKD1,BKF1,BKR1,BKT1,BSDC,BSDK,BSDN,BSDS,CDAT,CTRC,ESBG,LSEN,LSTU,PRFL,SDU1,SEX1,SSH1,TYPE,URNO,DPT1,FCTC,FDEL,RGSB,VAFR,VATO,USEC,USEU,REMA");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

//--REGISTER----------------------------------------------------------------------------------------------

void CedaBsdData::Register(void)
{
	ogDdx.Register((void *)this,BC_BSD_CHANGE,	CString("BSDDATA"), CString("Bsd-changed"),	ProcessBsdCf);
	ogDdx.Register((void *)this,BC_BSD_NEW,		CString("BSDDATA"), CString("Bsd-new"),		ProcessBsdCf);
	ogDdx.Register((void *)this,BC_BSD_DELETE,	CString("BSDDATA"), CString("Bsd-deleted"),	ProcessBsdCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaBsdData::~CedaBsdData(void)
{
	TRACE("CedaBsdData::~CedaBsdData called\n");
	ClearAll();
	omRecInfo.DeleteAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaBsdData::ClearAll(bool bpWithRegistration)
{
	TRACE("CedaBsdData::ClearAll called\n");

	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}

    omUrnoMap.RemoveAll();
    omBsdcMap.RemoveAll();
    omData.DeleteAll();
	omSelectedData.RemoveAll();
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaBsdData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omBsdcMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		TRACE("Read-Bsd: Ceda-Error %d \n",ilRc);
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		BSDDATA *prlBsd = new BSDDATA;
		if ((ilRc = GetFirstBufferRecord(prlBsd)) == true)
		{
			omData.Add(prlBsd);//Update omData
			omUrnoMap.SetAt((void *)prlBsd->Urno,prlBsd);
			omBsdcMap.SetAt((CString)prlBsd->Bsdc,prlBsd);
		}
		else
		{
			delete prlBsd;
		}
	}
	TRACE("Read-Bsd: %d gelesen\n",ilLc-1);
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaBsdData::Insert(BSDDATA *prpBsd)
{
	prpBsd->IsChanged = DATA_NEW;
	if(Save(prpBsd) == false) return false; //Update Database
	InsertInternal(prpBsd);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaBsdData::InsertInternal(BSDDATA *prpBsd)
{
	bool blRet = false;
	BSDDATA *prlBsd = NULL;
	if (omUrnoMap.Lookup((void *)prpBsd->Urno,(void *& )prlBsd) == FALSE)
	{
		ogDdx.DataChanged((void *)this, BSD_NEW,(void *)prpBsd ); //Update Viewer
		omData.Add(prpBsd);//Update omData
		omUrnoMap.SetAt((void *)prpBsd->Urno,prpBsd);
		omBsdcMap.SetAt((CString)prpBsd->Bsdc,prpBsd);
		blRet =true;
	}
    return blRet;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaBsdData::Delete(long lpUrno)
{
	BSDDATA *prlBsd = GetBsdByUrno(lpUrno);
	if (prlBsd != NULL)
	{
		prlBsd->IsChanged = DATA_DELETED;
		if(Save(prlBsd) == false) return false; //Update Database
		DeleteInternal(prlBsd);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaBsdData::DeleteInternal(BSDDATA *prpBsd)
{
	ogDdx.DataChanged((void *)this,BSD_DELETE,(void *)prpBsd); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpBsd->Urno);
	omBsdcMap.RemoveKey((CString)prpBsd->Bsdc);
	int ilBsdCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilBsdCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpBsd->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaBsdData::Update(BSDDATA *prpBsd)
{
	if (GetBsdByUrno(prpBsd->Urno) != NULL)
	{
		if (prpBsd->IsChanged == DATA_UNCHANGED)
		{
			prpBsd->IsChanged = DATA_CHANGED;
		}
		if(Save(prpBsd) == false) return false; //Update Database
		UpdateInternal(prpBsd);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaBsdData::UpdateInternal(BSDDATA *prpBsd)
{
	BSDDATA *prlBsd = GetBsdByUrno(prpBsd->Urno);
	if (prlBsd != NULL)
	{
		omBsdcMap.RemoveKey((CString)prlBsd->Bsdc);
		*prlBsd = *prpBsd; //Update omData
		omBsdcMap.SetAt((CString)prlBsd->Bsdc,prlBsd);
		ogDdx.DataChanged((void *)this,BSD_CHANGE,(void *)prlBsd); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

BSDDATA *CedaBsdData::GetBsdByUrno(long lpUrno)
{
	BSDDATA  *prlBsd;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlBsd) == TRUE)
	{
		return prlBsd;
	}
	return NULL;
}

//--GET-BY-BSDC--------------------------------------------------------------------------------------------

BSDDATA *CedaBsdData::GetBsdByBsdc(CString opBsdc)
{
	BSDDATA  *prlBsd;
	if (omBsdcMap.Lookup(opBsdc,(void *& )prlBsd) == TRUE)
	{
		return prlBsd;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaBsdData::ReadSpecialData(CCSPtrArray<BSDDATA> *popBsd,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popBsd != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			BSDDATA *prpBsd = new BSDDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpBsd,CString(pclFieldList))) == true)
			{
				popBsd->Add(prpBsd);
			}
			else
			{
				delete prpBsd;
			}
		}
		if(popBsd->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaBsdData::Save(BSDDATA *prpBsd)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpBsd->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpBsd->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpBsd);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpBsd->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpBsd->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpBsd);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpBsd->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpBsd->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	TRACE("Bsd-IRT/URT/DRT: Ceda-Return %d \n",ilRc);
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessBsdCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	BC_TRY
		ogBsdData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
	BC_CATCH_ALL
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaBsdData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlBsdData;
	prlBsdData = (struct BcStruct *) vpDataPointer;
	BSDDATA *prlBsd;
	if(ipDDXType == BC_BSD_NEW)
	{
		prlBsd = new BSDDATA;
		GetRecordFromItemList(prlBsd,prlBsdData->Fields,prlBsdData->Data);
		InsertInternal(prlBsd);
	}
	if(ipDDXType == BC_BSD_CHANGE)
	{
		long llUrno  = 0L;
		CString olSelection = (CString)prlBsdData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlBsdData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlBsd = GetBsdByUrno(llUrno);
		if(prlBsd != NULL)
		{
			GetRecordFromItemList(prlBsd,prlBsdData->Fields,prlBsdData->Data);
			UpdateInternal(prlBsd);
		}
	}
	if(ipDDXType == BC_BSD_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlBsdData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlBsdData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlBsd = GetBsdByUrno(llUrno);
		if (prlBsd != NULL)
		{
			DeleteInternal(prlBsd);
		}
	}
}

void CedaBsdData::MakeBsdSelected(BSDDATA *prpBsd, BOOL bpSelect)
{
	if(prpBsd != NULL)
	{
		int ilCount = omSelectedData.GetSize();
		long lBsdu = prpBsd->Urno;
		UINT *loc;
		prpBsd->IsSelected = bpSelect;

		if(bpSelect == TRUE)
		{			
			if(ilCount>0)
			{
				loc = std::find(&omSelectedData[0],&omSelectedData[0] + ilCount,lBsdu);
				if(loc == (&omSelectedData[0] + ilCount)) // not found
				{
					omSelectedData.Add(prpBsd->Urno);
			//		ogDdx.DataChanged((void *)this,BSD_CHANGE,(void *)prpBsd);
				}
			}
			else
			{
				omSelectedData.Add(prpBsd->Urno);
			//	ogDdx.DataChanged((void *)this,BSD_CHANGE,(void *)prpBsd);
			}
		}
		else
		{
			if(ilCount>0)
			{
				loc = std::find(&omSelectedData[0],&omSelectedData[0] + ilCount,lBsdu);
				if(loc!=(&omSelectedData[0] + ilCount)) // found
				{
					ogDdx.DataChanged((void *)this,BSD_CHANGE,(void *)prpBsd);
					omSelectedData.RemoveAt(loc-&omSelectedData[0]);
				}
			}
		}
	}
}

void CedaBsdData::DeSelectAll()
{
	int ilCount = omSelectedData.GetUpperBound();
	for(int k = 0; k <= ilCount; k++)
	{
		long ilUrno = omSelectedData.GetAt(k);
		BSDDATA *prlBsd = GetBsdByUrno(ilUrno);
		if(prlBsd!=NULL)
		{
			prlBsd->IsSelected = FALSE;
		//	ogDdx.DataChanged((void *)this,BSD_CHANGE,(void *)prlBsd);
		}
	}
	omSelectedData.RemoveAll();
}

bool CedaBsdData::GetSelectedBsd(CCSPtrArray<BSDDATA> *popSelectedBsd)
{
	bool blRet = false;
	int ilCount = omSelectedData.GetUpperBound();
	for(int k=0;k<= ilCount;k++)
	{
		long llUrno = omSelectedData.GetAt(k);
		BSDDATA *prlBsd = GetBsdByUrno(llUrno);
		if(prlBsd!=NULL)
		{
			blRet = true;
			popSelectedBsd->Add(prlBsd);
		}
	}
	return blRet;
}
//---------------------------------------------------------------------------------------------------------
void CedaBsdData::GetAllBsds(CStringArray &ropPstFilter)
{
	int ilCount = omData.GetSize();
	CString olTmp1;
	CString olViewTmp;
	for (int ilLc = 0; ilLc < ilCount;ilLc++)
	{
		olTmp1 = omData[ilLc].Bsdc;
		if(!olTmp1.IsEmpty())
		{
			olViewTmp = olTmp1;
			olViewTmp += "#";
			olViewTmp += olTmp1;
			olViewTmp += ";";
			olViewTmp += omData[ilLc].Dpt1;
			ropPstFilter.Add(olViewTmp);
		}

	}
}

void CedaBsdData::GetAllBsdsOrderedByTime(CCSPtrArray<BSDDATA>& ropBsdList)
{
	ropBsdList.Append(this->omData);
	ropBsdList.Sort(OrderByEsbg);
}
