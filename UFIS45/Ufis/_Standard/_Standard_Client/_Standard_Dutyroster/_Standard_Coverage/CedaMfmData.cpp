// CedaMfmData.cpp
 
#include <stdafx.h>
#include <CedaMfmData.h>
#include <resource.h>


void ProcessMfmCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaMfmData::CedaMfmData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for MFMDATA
	BEGIN_CEDARECINFO(MFMDATA,MfmDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Fm01,"MF01")
		FIELD_CHAR_TRIM	(Fm02,"MF02")
		FIELD_CHAR_TRIM	(Fm03,"MF03")
		FIELD_CHAR_TRIM	(Fm04,"MF04")
		FIELD_CHAR_TRIM	(Fm05,"MF05")
		FIELD_CHAR_TRIM	(Fm06,"MF06")
		FIELD_CHAR_TRIM	(Fm07,"MF07")
		FIELD_CHAR_TRIM	(Fm08,"MF08")
		FIELD_CHAR_TRIM	(Fm09,"MF09")
		FIELD_CHAR_TRIM	(Fm10,"MF10")
		FIELD_CHAR_TRIM	(Fm11,"MF11")
		FIELD_CHAR_TRIM	(Fm12,"MF12")
		FIELD_CHAR_TRIM	(Year,"YEAR")
		FIELD_CHAR_TRIM	(Dpt1,"DPT1")
		FIELD_CHAR_TRIM	(Ctrc,"CTRC")

	END_CEDARECINFO //(MFMDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(MfmDataRecInfo)/sizeof(MfmDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&MfmDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"MFM");
	strcpy(pcmListOfFields,"URNO,CDAT,LSTU,USEC,USEU,MF01,MF02,MF03,MF04,MF05,MF06,MF07,MF08,MF09,MF10,MF11,MF12,YEAR,DPT1");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaMfmData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("URNO");
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("FM01");
	ropFields.Add("FM02");
	ropFields.Add("FM03");
	ropFields.Add("FM04");
	ropFields.Add("FM05");
	ropFields.Add("FM06");
	ropFields.Add("FM07");
	ropFields.Add("FM08");
	ropFields.Add("FM09");
	ropFields.Add("FM10");
	ropFields.Add("FM11");
	ropFields.Add("FM12");
	ropFields.Add("YEAR");
	ropFields.Add("DPT1");
	if (ogBasicData.IsColorCodeAvailable())
	{
		ropFields.Add("CTRC");
	}

	ropDesription.Add(LoadStg(IDS_STRING61416));
	ropDesription.Add(LoadStg(IDS_STRING61463));
	ropDesription.Add(LoadStg(IDS_STRING61467));
	ropDesription.Add(LoadStg(IDS_STRING61470));
	ropDesription.Add(LoadStg(IDS_STRING61471));
	ropDesription.Add(LoadStg(IDS_STRING61530));
	ropDesription.Add(LoadStg(IDS_STRING61531));
	ropDesription.Add(LoadStg(IDS_STRING61532));
	ropDesription.Add(LoadStg(IDS_STRING61533));
	ropDesription.Add(LoadStg(IDS_STRING61534));
	ropDesription.Add(LoadStg(IDS_STRING61535));
	ropDesription.Add(LoadStg(IDS_STRING61536));
	ropDesription.Add(LoadStg(IDS_STRING61537));
	ropDesription.Add(LoadStg(IDS_STRING61538));
	ropDesription.Add(LoadStg(IDS_STRING61539));
	ropDesription.Add(LoadStg(IDS_STRING61540));
	ropDesription.Add(LoadStg(IDS_STRING61541));
	ropDesription.Add(LoadStg(IDS_STRING61542));
	ropDesription.Add(LoadStg(IDS_STRING61543));
	if (ogBasicData.IsColorCodeAvailable())
	{
		ropDesription.Add(LoadStg(IDS_STRING61544));
	}

	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	if (ogBasicData.IsColorCodeAvailable())
	{
		ropType.Add("String");
	}

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaMfmData::Register(void)
{
	ogDdx.Register((void *)this,BC_MFM_CHANGE,	CString("MFMDATA"), CString("Mfm-changed"),	ProcessMfmCf);
	ogDdx.Register((void *)this,BC_MFM_NEW,		CString("MFMDATA"), CString("Mfm-new"),		ProcessMfmCf);
	ogDdx.Register((void *)this,BC_MFM_DELETE,	CString("MFMDATA"), CString("Mfm-deleted"),	ProcessMfmCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaMfmData::~CedaMfmData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaMfmData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaMfmData::Read(char *pspWhere /*NULL*/)
{
	if (ogBasicData.IsColorCodeAvailable())
	{
		strcpy(pcmListOfFields,"URNO,CDAT,LSTU,USEC,USEU,MF01,MF02,MF03,MF04,MF05,MF06,MF07,MF08,MF09,MF10,MF11,MF12,YEAR,DPT1,CTRC");
		pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer
	}

    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		MFMDATA *prlMfm = new MFMDATA;
		if ((ilRc = GetFirstBufferRecord(prlMfm)) == true)
		{
			omData.Add(prlMfm);//Update omData
			omUrnoMap.SetAt((void *)prlMfm->Urno,prlMfm);
		}
		else
		{
			delete prlMfm;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaMfmData::Insert(MFMDATA *prpMfm)
{
	prpMfm->IsChanged = DATA_NEW;
	if(Save(prpMfm) == false) return false; //Update Database
	InsertInternal(prpMfm);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaMfmData::InsertInternal(MFMDATA *prpMfm)
{
	ogDdx.DataChanged((void *)this, MFM_NEW,(void *)prpMfm ); //Update Viewer
	omData.Add(prpMfm);//Update omData
	omUrnoMap.SetAt((void *)prpMfm->Urno,prpMfm);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaMfmData::Delete(long lpUrno)
{
	MFMDATA *prlMfm = GetMfmByUrno(lpUrno);
	if (prlMfm != NULL)
	{
		prlMfm->IsChanged = DATA_DELETED;
		if(Save(prlMfm) == false) return false; //Update Database
		DeleteInternal(prlMfm);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaMfmData::DeleteInternal(MFMDATA *prpMfm)
{
	ogDdx.DataChanged((void *)this,MFM_DELETE,(void *)prpMfm); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpMfm->Urno);
	int ilMfmCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilMfmCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpMfm->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaMfmData::Update(MFMDATA *prpMfm)
{
	if (GetMfmByUrno(prpMfm->Urno) != NULL)
	{
		if (prpMfm->IsChanged == DATA_UNCHANGED)
		{
			prpMfm->IsChanged = DATA_CHANGED;
		}
		if(Save(prpMfm) == false) return false; //Update Database
		UpdateInternal(prpMfm);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaMfmData::UpdateInternal(MFMDATA *prpMfm)
{
	MFMDATA *prlMfm = GetMfmByUrno(prpMfm->Urno);
	if (prlMfm != NULL)
	{
		*prlMfm = *prpMfm; //Update omData
		ogDdx.DataChanged((void *)this,MFM_CHANGE,(void *)prlMfm); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

MFMDATA *CedaMfmData::GetMfmByUrno(long lpUrno)
{
	MFMDATA  *prlMfm;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlMfm) == TRUE)
	{
		return prlMfm;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaMfmData::ReadSpecialData(CCSPtrArray<MFMDATA> *popMfm,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","MFM",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","MFM",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popMfm != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			MFMDATA *prpMfm = new MFMDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpMfm,CString(pclFieldList))) == true)
			{
				popMfm->Add(prpMfm);
			}
			else
			{
				delete prpMfm;
			}
		}
		if(popMfm->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaMfmData::Save(MFMDATA *prpMfm)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpMfm->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpMfm->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpMfm);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpMfm->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpMfm->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpMfm);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpMfm->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpMfm->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessMfmCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogMfmData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaMfmData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlMfmData;
	prlMfmData = (struct BcStruct *) vpDataPointer;
	MFMDATA *prlMfm;
	if(ipDDXType == BC_MFM_NEW)
	{
		prlMfm = new MFMDATA;
		GetRecordFromItemList(prlMfm,prlMfmData->Fields,prlMfmData->Data);
		if(ValidateMfmBcData(prlMfm->Urno)) //Prf: 8795
		{
			InsertInternal(prlMfm);
		}
		else
		{
			delete prlMfm;
		}
	}
	if(ipDDXType == BC_MFM_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlMfmData->Selection);
		prlMfm = GetMfmByUrno(llUrno);
		if(prlMfm != NULL)
		{
			GetRecordFromItemList(prlMfm,prlMfmData->Fields,prlMfmData->Data);
			if(ValidateMfmBcData(prlMfm->Urno)) //Prf: 8795
			{
				UpdateInternal(prlMfm);
			}
			else
			{
				DeleteInternal(prlMfm);
			}
		}
	}
	if(ipDDXType == BC_MFM_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlMfmData->Selection);

		prlMfm = GetMfmByUrno(llUrno);
		if (prlMfm != NULL)
		{
			DeleteInternal(prlMfm);
		}
	}
}

//Prf: 8795
//--ValidateMfmBcData--------------------------------------------------------------------------------------

bool CedaMfmData::ValidateMfmBcData(const long& lrpUrno)
{
	bool blValidateMfmBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<MFMDATA> olMfms;
		if(!ReadSpecialData(&olMfms,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateMfmBcData = false;
		}
	}
	return blValidateMfmBcData;
}

//---------------------------------------------------------------------------------------------------------
bool CedaMfmData::MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, void *pvpDataStruct)
{
    pcpListOfData = "";

	CString olFieldData;

    // Start conversion, field-by-field
    int nfields = pomRecInfo->GetSize();
    for (int fieldno = 0; fieldno < nfields; fieldno++)
    {

        CString s;
        char buf[33];   // ltoa() need buffer 33 bytes
        int type = (*pomRecInfo)[fieldno].Type;
        void *p = (void *)((char *)pvpDataStruct + (*pomRecInfo)[fieldno].Offset);
        int length = (*pomRecInfo)[fieldno].Length;

		if (strstr(this->pcmFieldList,(*pomRecInfo)[fieldno].Name) == NULL)
			continue;

        switch (type) {
        case CEDACHAR:
            s = (length == 0)? CString(" "): CString((char *)p).Left(length);
			MakeCedaString(s);
            olFieldData = s + ",";
            break;
        case CEDAINT:
            olFieldData = CString(itoa(*(int *)p, buf, 10)) + ",";
            break;
        case CEDALONG:
            olFieldData = CString(ltoa(*(long *)p, buf, 10)) + ",";
            break;
        case CEDADATE:  // format: YYYYMMDDHHMMSS
			olFieldData = ((CTime *)p)->Format("%Y%m%d%H%M%S") + ",";
            break;
        case CEDAOLEDATE:  // format: YYYYMMDDHHMMSS
			if(((COleDateTime *)p)->GetStatus() == COleDateTime::valid)
			{
				olFieldData = ((COleDateTime *)p)->Format("%Y%m%d%H%M%S") + ",";
			}
			else
			{
				olFieldData = " ,"; 
			}
            break;
		case CEDADOUBLE:
			char pclBuf[128];
//MWO 18.12.98
			sprintf(pclBuf,"%lf",(double *)p);
//			sprintf(pclBuf,"%.2f",(double *)p);
            olFieldData = CString(pclBuf) + ",";
			break;
        default:    // invalid field type
            return false;
        }
		pcpListOfData += olFieldData;
    }

	if(pcpListOfData.GetLength() > 0)
		pcpListOfData = pcpListOfData.Left(pcpListOfData.GetLength()-1);

	//pcpListOfData.SetAt(pcpListOfData.GetLength()-1,' ');
	//pcpListOfData.TrimRight( );

    return true;
}


//---------------------------------------------------------------------------------------------------------
