// CedaOdaData.cpp
 
#include <stdafx.h>
#include <CedaOdaData.h>
#include <resource.h>


void ProcessOdaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaOdaData::CedaOdaData() : CCSCedaData(&ogCommHandler)
{ 
	// Create an array of CEDARECINFO for ODADataStruct
	BEGIN_CEDARECINFO(ODADATA,ODADataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Ctrc,"CTRC")
		FIELD_CHAR_TRIM	(Dptc,"DPTC")
		FIELD_CHAR_TRIM	(Dura,"DURA")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_CHAR_TRIM	(Sdac,"SDAC")
		FIELD_CHAR_TRIM	(Sdae,"SDAE")
		FIELD_CHAR_TRIM	(Sdak,"SDAK")
		FIELD_CHAR_TRIM	(Sdan,"SDAN")
		FIELD_CHAR_TRIM	(Sdas,"SDAS")
		FIELD_CHAR_TRIM	(Tatp,"TATP")
		FIELD_CHAR_TRIM	(Tsap,"TSAP")
		FIELD_CHAR_TRIM	(Type,"TYPE")
		FIELD_CHAR_TRIM	(Upln,"UPLN")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Free,"FREE")
		FIELD_CHAR_TRIM	(Work,"WORK")
		FIELD_CHAR_TRIM	(Tbsd,"TBSD")
		FIELD_CHAR_TRIM	(Abfr,"ABFR")
		FIELD_CHAR_TRIM	(Abto,"ABTO")
		FIELD_CHAR_TRIM	(Blen,"BLEN")
		FIELD_CHAR_TRIM	(Sdaa,"SDAA")
		FIELD_CHAR_TRIM	(Rgbc,"RGBC")

	END_CEDARECINFO //(ODADataStruct)	

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(ODADataRecInfo)/sizeof(ODADataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&ODADataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"ODA");

	strcpy(pcmListOfFields,"CDAT,CTRC,DPTC,DURA,LSTU,PRFL,REMA,SDAC,SDAE,SDAK,SDAN,SDAS,TATP,TSAP,TYPE,UPLN,URNO,USEC,USEU,FREE,WORK,TBSD,ABFR,ABTO,BLEN,SDAA");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaOdaData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");			ropDesription.Add(LoadStg(IDS_STRING61482));			ropType.Add("Date");
	ropFields.Add("CTRC");			ropDesription.Add(LoadStg(IDS_STRING61483));			ropType.Add("String");
	ropFields.Add("DPTC");			ropDesription.Add(LoadStg(IDS_STRING61484));			ropType.Add("String");
	ropFields.Add("DURA");			ropDesription.Add(LoadStg(IDS_STRING61485));			ropType.Add("String");
	ropFields.Add("LSTU");			ropDesription.Add(LoadStg(IDS_STRING61486));			ropType.Add("Date");
	ropFields.Add("PRFL");			ropDesription.Add(LoadStg(IDS_STRING61487));			ropType.Add("String");
	ropFields.Add("REMA");			ropDesription.Add(LoadStg(IDS_STRING61488));			ropType.Add("String");
	ropFields.Add("SDAC");			ropDesription.Add(LoadStg(IDS_STRING61489));			ropType.Add("String");
	ropFields.Add("SDAK");			ropDesription.Add(LoadStg(IDS_STRING61490));			ropType.Add("String");
	ropFields.Add("SDAN");			ropDesription.Add(LoadStg(IDS_STRING61491));			ropType.Add("String");
	ropFields.Add("SDAS");			ropDesription.Add(LoadStg(IDS_STRING61492));			ropType.Add("String");
	ropFields.Add("URNO");			ropDesription.Add(LoadStg(IDS_STRING61493));			ropType.Add("String");
	ropFields.Add("USEC");			ropDesription.Add(LoadStg(IDS_STRING61494));			ropType.Add("String");
	ropFields.Add("USEU");			ropDesription.Add(LoadStg(IDS_STRING61495));			ropType.Add("String");

	ropFields.Add("TATP");			ropDesription.Add(LoadStg(IDS_STRING61496));			ropType.Add("String");
	ropFields.Add("TSAP");			ropDesription.Add(LoadStg(IDS_STRING61497));			ropType.Add("String");
	ropFields.Add("TYPE");			ropDesription.Add(LoadStg(IDS_STRING61498));			ropType.Add("String");
	ropFields.Add("UPLN");			ropDesription.Add(LoadStg(IDS_STRING61499));			ropType.Add("String");
	ropFields.Add("FREE");			ropDesription.Add(LoadStg(IDS_STRING61500));			ropType.Add("String");

	if (ogBasicData.IsColorCodeAvailable())	
	{
		ropFields.Add("RGBC");			ropDesription.Add(LoadStg(IDS_STRING61501));		ropType.Add("String");
	}
}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaOdaData::Register(void)
{
	ogDdx.Register((void *)this,BC_ODA_CHANGE,	CString("ODADATA"), CString("Oda-changed"),	ProcessOdaCf);
	ogDdx.Register((void *)this,BC_ODA_NEW,		CString("ODADATA"), CString("Oda-new"),		ProcessOdaCf);
	ogDdx.Register((void *)this,BC_ODA_DELETE,	CString("ODADATA"), CString("Oda-deleted"),	ProcessOdaCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaOdaData::~CedaOdaData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaOdaData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaOdaData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	if (ogBasicData.IsColorCodeAvailable())
	{
		strcpy(pcmListOfFields,"CDAT,CTRC,DPTC,DURA,LSTU,PRFL,REMA,SDAC,SDAE,SDAK,SDAN,SDAS,TATP,TSAP,TYPE,UPLN,URNO,USEC,USEU,FREE,WORK,TBSD,ABFR,ABTO,BLEN,SDAA,RGBC");
		pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer
	}

	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		ODADATA *prlOda = new ODADATA;
		if ((ilRc = GetFirstBufferRecord(prlOda)) == true)
		{
			omData.Add(prlOda);//Update omData
			omUrnoMap.SetAt((void *)prlOda->Urno,prlOda);
		}
		else
		{
			delete prlOda;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaOdaData::Insert(ODADATA *prpOda)
{
	prpOda->IsChanged = DATA_NEW;
	if(Save(prpOda) == false) return false; //Update Database
	InsertInternal(prpOda);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaOdaData::InsertInternal(ODADATA *prpOda)
{
	ogDdx.DataChanged((void *)this, ODA_NEW,(void *)prpOda ); //Update Viewer
	omData.Add(prpOda);//Update omData
	omUrnoMap.SetAt((void *)prpOda->Urno,prpOda);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaOdaData::Delete(long lpUrno)
{
	ODADATA *prlOda = GetOdaByUrno(lpUrno);
	if (prlOda != NULL)
	{
		prlOda->IsChanged = DATA_DELETED;
		if(Save(prlOda) == false) return false; //Update Database
		DeleteInternal(prlOda);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaOdaData::DeleteInternal(ODADATA *prpOda)
{
	ogDdx.DataChanged((void *)this,ODA_DELETE,(void *)prpOda); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpOda->Urno);
	int ilOdaCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilOdaCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpOda->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaOdaData::Update(ODADATA *prpOda)
{
	if (GetOdaByUrno(prpOda->Urno) != NULL)
	{
		if (prpOda->IsChanged == DATA_UNCHANGED)
		{
			prpOda->IsChanged = DATA_CHANGED;
		}
		if(Save(prpOda) == false) return false; //Update Database
		UpdateInternal(prpOda);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaOdaData::UpdateInternal(ODADATA *prpOda)
{
	ODADATA *prlOda = GetOdaByUrno(prpOda->Urno);
	if (prlOda != NULL)
	{
		*prlOda = *prpOda; //Update omData
		ogDdx.DataChanged((void *)this,ODA_CHANGE,(void *)prlOda); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

ODADATA *CedaOdaData::GetOdaByUrno(long lpUrno)
{
	ODADATA  *prlOda;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlOda) == TRUE)
	{
		return prlOda;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaOdaData::ReadSpecialData(CCSPtrArray<ODADATA> *popOda,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","ODA",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","ODA",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popOda != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			ODADATA *prpOda = new ODADATA;
			if ((ilRc = GetBufferRecord(ilLc,prpOda,CString(pclFieldList))) == true)
			{
				popOda->Add(prpOda);
			}
			else
			{
				delete prpOda;
			}
		}
		if(popOda->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaOdaData::Save(ODADATA *prpOda)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpOda->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpOda->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpOda);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpOda->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpOda->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpOda);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpOda->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpOda->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessOdaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogOdaData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaOdaData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlOdaData;
	prlOdaData = (struct BcStruct *) vpDataPointer;
	ODADATA *prlOda;
	if(ipDDXType == BC_ODA_NEW)
	{
		prlOda = new ODADATA;
		GetRecordFromItemList(prlOda,prlOdaData->Fields,prlOdaData->Data);
		if(ValidateOdaBcData(prlOda->Urno)) //Prf: 8795
		{
			InsertInternal(prlOda);
		}
		else
		{
			delete prlOda;
		}
	}
	if(ipDDXType == BC_ODA_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlOdaData->Selection);
		prlOda = GetOdaByUrno(llUrno);
		if(prlOda != NULL)
		{
			GetRecordFromItemList(prlOda,prlOdaData->Fields,prlOdaData->Data);
			if(ValidateOdaBcData(prlOda->Urno)) //Prf: 8795
			{
				UpdateInternal(prlOda);
			}
			else
			{
				DeleteInternal(prlOda);
			}
		}
	}
	if(ipDDXType == BC_ODA_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlOdaData->Selection);

		prlOda = GetOdaByUrno(llUrno);
		if (prlOda != NULL)
		{
			DeleteInternal(prlOda);
		}
	}
}

//Prf: 8795
//--ValidateOdaBcData--------------------------------------------------------------------------------------

bool CedaOdaData::ValidateOdaBcData(const long& lrpUrno)
{
	bool blValidateOdaBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<ODADATA> olOdas;
		if(!ReadSpecialData(&olOdas,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateOdaBcData = false;
		}
	}
	return blValidateOdaBcData;
}

//---------------------------------------------------------------------------------------------------------
bool CedaOdaData::MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, void *pvpDataStruct)
{
    pcpListOfData = "";

	CString olFieldData;

    // Start conversion, field-by-field
    int nfields = pomRecInfo->GetSize();
    for (int fieldno = 0; fieldno < nfields; fieldno++)
    {

        CString s;
        char buf[33];   // ltoa() need buffer 33 bytes
        int type = (*pomRecInfo)[fieldno].Type;
        void *p = (void *)((char *)pvpDataStruct + (*pomRecInfo)[fieldno].Offset);
        int length = (*pomRecInfo)[fieldno].Length;

		if (strstr(this->pcmFieldList,(*pomRecInfo)[fieldno].Name) == NULL)
			continue;

        switch (type) {
        case CEDACHAR:
            s = (length == 0)? CString(" "): CString((char *)p).Left(length);
			MakeCedaString(s);
            olFieldData = s + ",";
            break;
        case CEDAINT:
            olFieldData = CString(itoa(*(int *)p, buf, 10)) + ",";
            break;
        case CEDALONG:
            olFieldData = CString(ltoa(*(long *)p, buf, 10)) + ",";
            break;
        case CEDADATE:  // format: YYYYMMDDHHMMSS
			olFieldData = ((CTime *)p)->Format("%Y%m%d%H%M%S") + ",";
            break;
        case CEDAOLEDATE:  // format: YYYYMMDDHHMMSS
			if(((COleDateTime *)p)->GetStatus() == COleDateTime::valid)
			{
				olFieldData = ((COleDateTime *)p)->Format("%Y%m%d%H%M%S") + ",";
			}
			else
			{
				olFieldData = " ,"; 
			}
            break;
		case CEDADOUBLE:
			char pclBuf[128];
//MWO 18.12.98
			sprintf(pclBuf,"%lf",(double *)p);
//			sprintf(pclBuf,"%.2f",(double *)p);
            olFieldData = CString(pclBuf) + ",";
			break;
        default:    // invalid field type
            return false;
        }
		pcpListOfData += olFieldData;
    }

	if(pcpListOfData.GetLength() > 0)
		pcpListOfData = pcpListOfData.Left(pcpListOfData.GetLength()-1);

	//pcpListOfData.SetAt(pcpListOfData.GetLength()-1,' ');
	//pcpListOfData.TrimRight( );

    return true;
}

//---------------------------------------------------------------------------------------------------------
