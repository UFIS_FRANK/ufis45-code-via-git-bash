#ifndef AFX_PSTEMPLATEFILTERPAGE_H__495C9510_A2E4_11D1_BD3B_0000B4392C49__INCLUDED_
#define AFX_PSTEMPLATEFILTERPAGE_H__495C9510_A2E4_11D1_BD3B_0000B4392C49__INCLUDED_

// PsTemplateFilterPage.h : Header-Datei
//
#include <GridFenster.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CPsTemplateFilterPage 

class CPsTemplateFilterPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CPsTemplateFilterPage)

// Konstruktion
public:
	CPsTemplateFilterPage();
	~CPsTemplateFilterPage();

	void SetCaption(const char *pcpCaption);
	char pcmCaption[100];


// Dialogfelddaten
	//{{AFX_DATA(CPsTemplateFilterPage)
	enum { IDD = IDD_PSTEMPFILTER_PAGE };
	CButton	m_RemoveButton;
	CButton	m_AddButton;
	CListBox	m_ContentList;
	CListBox	m_InsertList;
	BOOL		m_FlugBez;
	BOOL		m_NotFlugBez;
	BOOL		m_CommonCheckin;
	//}}AFX_DATA
	int imColCount;
	int imHideColStart;

	bool blIsInit;

	CGridFenster *pomPossilbeList;
	CGridFenster *pomSelectedList;


// Überschreibungen
	// Der Klassen-Assistent generiert virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CPsTemplateFilterPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CPsTemplateFilterPage)
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonRemove();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	CStringArray omPossibleItems;
	CStringArray omSelectedItems;
	CStringArray omButtonValues;


};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_PSFILTERPAGE_H__495C9510_A2E4_11D1_BD3B_0000B4392C49__INCLUDED_
