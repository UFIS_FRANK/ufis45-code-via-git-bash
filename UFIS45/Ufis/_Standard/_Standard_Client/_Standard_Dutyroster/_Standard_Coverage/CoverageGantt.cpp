// stgantt.cpp : implementation file
//  

#include <stdafx.h>
#include <CCSGlobl.h>
#include <Coverage.h>
#include <CCSPtrArray.h>
#include <CCSClientWnd.H>
#include <CCSTimeScale.h>
#include <CViewer.h>
#include <ccsdragdropctrl.h>
#include <CCSBar.h>
// TO DO Mitarbeiterdetail maske integrieren #include "EmpJobs.h"
#include <CovDiagram.h>
#include <ResultTable.h>
#include <CoverageChart.h>
//#include "ButtonListDlg.h"
#include <UniEingabe.h>
#include <Basicdata.h>

#include <BasisschichtenDlg.h>
#include <CheckReferenz.h>

#include <MasterShiftDemand.h>
#include <CoveragePfcSelectDlg.h>
// ------------------#include "staffdia.h"
//#include "MainFrm.h"
#include <ccsddx.h>
//#include "StaffDiaViewer.h"
//#include "jobdv.h"
#include <CoverageGantt.h>
#include <DataSet.h>
#include <sys/timeb.h>
#include <time.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsEnveloped(start1, end1, start2, end2)    ((start1) <= (end2) && (start1) >= (start2) && (end1) <= (end2) && (end1) >= (start2))

////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This block of code is used for substituing the scale calculation
// since the TimeScale has been implemented with a different pixel
// calculation algorithm (Pichet used the floating point calculation,
// which introduce +1 or -1 rounding effect. Furthermore, he still has
// some adjustments may be 2 or 3 pixels for makeing the TimeScale
// display nicely :-). Besides, there are still more few pixels which
// display inappropriately which introduced from the border of the
// GateChart or the other classes of the same kind). To fix all of these
// things, the only fastest way is just adopt the scaling algorithm
// written in the TimeScale and come back to clean up this sometime later.
//
// Flighter a few though, just replace the macro GetX(time) and GetTime(x)
// should be enough, still we had to update the "omDisplayStart" and
// "omDisplayEnd" everytime these value has been changed.
#define	FIX_TIMESCALE_ROUNDING_ERROR
#ifdef	FIX_TIMESCALE_ROUNDING_ERROR
#define GetX(time)	(pomTimeScale->GetXFromTime(time) + imVerticalScaleWidth)
#define GetCTime(x)	(pomTimeScale->GetTimeFromX((x) - imVerticalScaleWidth))
#else
////////////////////////////////////////////////////////////////////////

#endif
////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// CoverageGantt

static void CoverageGanttCf(void *popInstance, int ipDDXType,
	void *vpDataPointer, CString &ropInstanceName);
static int imTopLevel = 0; // Used as help since Gantt is destroyed each time it's scrolled

CoverageGantt::CoverageGantt(CoverageDiagramViewer *popViewer, int ipGroupno, 
    int ipVerticalScaleWidth, int ipVerticalScaleIndent,
    CFont *popVerticalScaleFont, CFont *popGanttChartFont,
    int ipGutterHeight, int ipOverlapHeight,
    COLORREF lpVerticalScaleTextColor, COLORREF lpVerticalScaleBackgroundColor,
    COLORREF lpHighlightVerticalScaleTextColor, COLORREF lpHighlightVerticalScaleBackgroundColor,
    COLORREF lpGanttChartTextColor, COLORREF lpGanttChartBackgroundColor,
    COLORREF lpHighlightGanttChartTextColor, COLORREF lpHighlightGanttChartBackgroundColor)
{
    SetViewer(popViewer, ipGroupno);
    SetVerticalScaleWidth(ipVerticalScaleWidth);
    SetVerticalScaleIndent(ipVerticalScaleIndent);
//    SetFonts(popVerticalScaleFont, popGanttChartFont);
    SetFonts(igFontIndex1, igFontIndex2);

    SetGutters(ipGutterHeight, ipOverlapHeight);
    SetVerticalScaleColors(lpVerticalScaleTextColor, lpVerticalScaleBackgroundColor,
        lpHighlightVerticalScaleTextColor, lpHighlightVerticalScaleBackgroundColor);
    SetGanttChartColors(lpGanttChartTextColor, lpGanttChartBackgroundColor,
        lpHighlightGanttChartTextColor, lpHighlightGanttChartBackgroundColor);

    // Initialize default values
    omDisplayStart = TIMENULL;
    omDisplayEnd = TIMENULL;
    imWindowWidth = 0;          // unessential -- WM_SIZE will initialize this
    bmIsFixedScaling = TRUE;    // unessential -- will be set in method SetDisplayWindow()
    omCurrentTime = TIMENULL;
    omMarkTimeStart = TIMENULL;
    omMarkTimeEnd = TIMENULL;
    pomStatusBar = NULL;
	pomTimeScale = NULL;
	pomDiagramWnd = NULL;
	pomChartWnd = NULL;
	pomCoverageWindow = NULL;
    bmIsMouseInWindow = FALSE;
    bmIsControlKeyDown = FALSE;
	bmMarkLines = FALSE; 
    imHighlightLine = -1;
    imCurrentBar = -1;
	imActiveLine = -1;
	bmActiveBarSet = FALSE;
	bmContextBarSet = FALSE;
	bmActiveLineSet = FALSE;
	pomPreviousBrush = NULL;
	prmPreviousBar = NULL;
	bmTopIndex = 0;
	omLastClickedPosition = CPoint(-1, -1);
	ogDdx.Register(this, COV_MARKBAR,
		CString("STGANTT"), CString("MARK BAR"), CoverageGanttCf);
	//ogDdx.Register(this, BSD_NEW,
	//	CString("STGANTT"), CString("ADD BAR"), CoverageGanttNewBar);
    // Required only if you allow moving/resizing
    SetBorderPrecision(15);
    umResizeMode = HTNOWHERE;
}

CoverageGantt::~CoverageGantt()
{
   CCS_TRY
	imTopLevel = bmTopIndex;
   DestroyWindow();
   ogDdx.UnRegister(this, NOTUSED);
   //MWO DO DO ??pogCoverageGantt = NULL;
   CCS_CATCH_ALL
}

static void CoverageGanttCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
   CCS_TRY
    CoverageGantt *polGantt = (CoverageGantt *)popInstance;
	if (ipDDXType == COV_MARKBAR)
	{
		if(polGantt->imGroupno == BASIC_SHIFT)
		{
			polGantt->ProcessMarkBar(vpDataPointer);
		}
	}
   CCS_CATCH_ALL
}


void CoverageGantt::SetViewer(CoverageDiagramViewer *popViewer, int ipGroupno)
{
   CCS_TRY
    pomViewer = popViewer;
    imGroupno = ipGroupno;
   CCS_CATCH_ALL
}

void CoverageGantt::SetVerticalScaleWidth(int ipWidth)
{
   CCS_TRY
    imVerticalScaleWidth = ipWidth;
   CCS_CATCH_ALL
}

void CoverageGantt::SetVerticalScaleIndent(int ipIndent)
{
   CCS_TRY
    imVerticalScaleIndent = ipIndent;
   CCS_CATCH_ALL
}

//void CoverageGantt::SetFonts(CFont *popVerticalScaleFont, CFont *popGanttChartFont)
void CoverageGantt::SetFonts(int index1, int index2)
{
   CCS_TRY
	int ilIdx2 = index1;
	LOGFONT rlLogFont;
	if(ilIdx2 > 9)
		ilIdx2 = 9;
    pomVerticalScaleFont = &ogScalingFonts[ilIdx2];//popVerticalScaleFont;
    pomGanttChartFont = &ogScalingFonts[index1/*2*/];//&ogSmallFonts_Regular_4;//popGanttChartFont;
	pomGanttChartFont->GetLogFont(&rlLogFont);
	
	if(index2 == 0)
	{
		SetGutters(1, 4);
	}
    // Calculate the normal height of a bar
    CDC dc;
    dc.CreateCompatibleDC(NULL);
    if (pomGanttChartFont)
        dc.SelectObject(pomGanttChartFont);

	
    TEXTMETRIC tm;
    dc.GetTextMetrics(&tm); 
	//imBarHeight = 8;
    imBarHeight = tm.tmHeight + tm.tmInternalLeading + 2;
    //imBarHeight = -rlLogFont.lfHeight - 5;// + 4;
    imLeadingHeight = (tm.tmInternalLeading + 2) / 2;
    dc.DeleteDC();
   CCS_CATCH_ALL
}

void CoverageGantt::SetGutters(int ipGutterHeight, int ipOverlapHeight)
{
   CCS_TRY
    imGutterHeight = ipGutterHeight;
    imOverlapHeight = ipOverlapHeight;
   CCS_CATCH_ALL
}

void CoverageGantt::SetVerticalScaleColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
    COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor)
{
   CCS_TRY
    lmVerticalScaleTextColor = lpTextColor;
    lmVerticalScaleBackgroundColor = lpBackgroundColor;
    lmHighlightVerticalScaleTextColor = lpHighlightTextColor;
    lmHighlightVerticalScaleBackgroundColor = lpHighlightBackgroundColor;
   CCS_CATCH_ALL
}

void CoverageGantt::SetGanttChartColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
    COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor)
{
   CCS_TRY
    lmGanttChartTextColor = lpTextColor;
    lmGanttChartBackgroundColor = lpBackgroundColor;
    lmHighlightGanttChartTextColor = lpHighlightTextColor;
    lmHighlightGanttChartBackgroundColor = lpHighlightBackgroundColor;
   CCS_CATCH_ALL
}

// Attentions:
// Return the height that it's expected for being displayed
// Be careful, this one may be called before the CListBox was created.
//
int CoverageGantt::GetGanttChartHeight()
{
   CCS_TRY
    int ilHeight = 0;
    for (int ilLineno = 0; ilLineno < pomViewer->GetLineCount(imGroupno); ilLineno++)
        ilHeight += GetLineHeight(ilLineno);

    return ilHeight;
   CCS_CATCH_ALL
	return 0;
}

int CoverageGantt::GetLineHeight(int ilLineno)
{
   CCS_TRY
    int ilVisualMaxOverlapLevel = pomViewer->GetVisualMaxOverlapLevel(imGroupno, ilLineno);
    return imBarHeight + (ilVisualMaxOverlapLevel * imOverlapHeight);
    //return /*(2 * imGutterHeight) + */imBarHeight + (max(1, ilMaxOverlapLevel) * imOverlapHeight);
    //MWO int ilMaxOverlapLevel = pomViewer->GetLine(ilLineno)->MaxOverlapLevel;
    //MWO return /*(2 * imGutterHeight) +*/ imBarHeight + (max(1, ilMaxOverlapLevel) * imOverlapHeight) - 2;
   CCS_CATCH_ALL
	return 0;
}

BOOL CoverageGantt::Create(DWORD dwStyle, const RECT &rect, CWnd *pParentWnd, UINT nID)
{
   CCS_TRY
    // Make sure that the viewer is already given
    if (pomViewer == NULL)
        return FALSE;

    // Make sure that all essential style is defined
    // WS_CLIPSIBLINGS is very important for the ListBox so it does not paint outside its window
    dwStyle |= WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_CLIPSIBLINGS;
    dwStyle |= LBS_NOINTEGRALHEIGHT | LBS_OWNERDRAWVARIABLE;
    dwStyle |= LBS_MULTIPLESEL;
	bmTopIndex = imTopLevel;
    // Create the window
    if (CListBox::Create(dwStyle, rect, pParentWnd, nID) == FALSE)
        return FALSE;

    // Create items according to what's in the Viewer
    ResetContent();
    for (int ilLineno = 0; ilLineno < pomViewer->GetLineCount(imGroupno); ilLineno++)
    {
        AddString("");
        SetItemHeight(ilLineno, GetLineHeight(ilLineno));
    }

    return TRUE;
   CCS_CATCH_ALL
	return FALSE;
}

void CoverageGantt::SetStatusBar(CStatusBar *popStatusBar)
{
   CCS_TRY
    pomStatusBar = popStatusBar;
   CCS_CATCH_ALL
}

void CoverageGantt::SetCoverageWindow(CCoverageGraphicWnd *popWnd)
{
   CCS_TRY
	pomCoverageWindow = popWnd;
   CCS_CATCH_ALL
}

void CoverageGantt::SetTimeScale(CCSTimeScale *popTimeScale)
{
   CCS_TRY
    pomTimeScale = popTimeScale;
   CCS_CATCH_ALL
}

void CoverageGantt::SetBorderPrecision(int ipBorderPrecision)
{
   CCS_TRY
    imBorderPreLeft = ipBorderPrecision / 2;
    imBorderPreRight = (ipBorderPrecision+1) / 2;
   CCS_CATCH_ALL
}

void CoverageGantt::SetDisplayWindow(CTime opDisplayStart, CTime opDisplayEnd, BOOL bpFixedScaling)
{
   CCS_TRY
    omDisplayStart = opDisplayStart;
    omDisplayEnd = opDisplayEnd;
    bmIsFixedScaling = bpFixedScaling;

    if (m_hWnd == NULL)
        imWindowWidth = 0;  // window is still not opened
    else
    {
        CRect rect;
        GetWindowRect(&rect);   // can't use client rect (vertical scroll may less its width)
        imWindowWidth = rect.Width();   // however, this is correct only if window has no border
        RepaintGanttChart();    // redraw the entire screen
    }
   CCS_CATCH_ALL
}

void CoverageGantt::SetDisplayStart(CTime opDisplayStart)
{
   CCS_TRY
    if (bmIsFixedScaling)   // must shift both start/end time while keeping the old time span
        omDisplayEnd = opDisplayStart + (omDisplayEnd - omDisplayStart).GetTotalSeconds();
    omDisplayStart = opDisplayStart;
    omMarkTimeStart = omDisplayStart;
    omMarkTimeEnd = omDisplayStart;
    RepaintGanttChart();    // redraw the entire screen
   CCS_CATCH_ALL
}

void CoverageGantt::SetCurrentTime(CTime opCurrentTime)
{                               
   CCS_TRY
    RepaintGanttChart(-1, omCurrentTime, opCurrentTime);
    omCurrentTime = opCurrentTime;
   CCS_CATCH_ALL
}

void CoverageGantt::SetMarkTime(CTime opMarkTimeStart, CTime opMarkTimeEnd)
{
   CCS_TRY
	if(omMarkTimeStart != TIMENULL)
	{
		RepaintGanttChart(-1,
			omMarkTimeStart, omMarkTimeStart );
	}
	if(opMarkTimeStart != TIMENULL)
	{
		RepaintGanttChart(-1,
			opMarkTimeStart, opMarkTimeStart );
	}
   omMarkTimeStart = opMarkTimeStart;

	if(omMarkTimeEnd != TIMENULL)
	{
		RepaintGanttChart(-1,
			omMarkTimeEnd,	omMarkTimeEnd );
	}
	if(opMarkTimeEnd != TIMENULL)
	{
		RepaintGanttChart(-1,
		 opMarkTimeEnd,	opMarkTimeEnd );
	}
   omMarkTimeEnd = opMarkTimeEnd;
   CCS_CATCH_ALL
}

void CoverageGantt::RepaintVerticalScale(int ipLineno, BOOL bpErase)
{
   CCS_TRY
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

    CRect rcPaint;
    if (GetItemRect(ipLineno, &rcPaint) != LB_ERR)  // valid item in list box?
    {
        rcPaint.right = imVerticalScaleWidth - 1;
        InvalidateRect(&rcPaint, bpErase);
    }
   CCS_CATCH_ALL
}

void CoverageGantt::RepaintGanttChart(int ipLineno, CTime opStartTime, CTime opEndTime, BOOL bpErase)
{
   CCS_TRY
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

    CRect rcPaint;
    if (ipLineno == -1) // repaint the whole chart?
        GetClientRect(&rcPaint);
    else
        GetItemRect(ipLineno, &rcPaint);

    // Update the GanttChart body
//	 if(opStartTime == TIMENULL && opEndTime == TIMENULL)
//	 {
//		 rcPaint.left = imVerticalScaleWidth;
//		 rcPaint.right = (int)GetX(opEndTime) + 1;
//	 }
//	 else
//	 {
		int ilX = GetX(opStartTime);
		omDisplayStart = pomTimeScale->GetDisplayStartTime();
		CTimeSpan olTimeSpan  = pomTimeScale->GetDisplayDuration();
		omDisplayEnd = omDisplayStart + olTimeSpan;

		for (int i = 0; i < pomViewer->GetLineCount(imGroupno); i++)
		{
			SetItemHeight(i, GetLineHeight(i));
		}


	/*
		if(ilX<imVerticalScaleWidth && opStartTime>omDisplayStart)
		{
			CString StartTime = opStartTime.Format("%Y.%m.%d - %H:%M");
			CString EndTime   = opEndTime.Format("%Y.%m.%d - %H:%M"); 
			CString DisplayStart = omDisplayStart.Format("%Y.%m.%d - %H:%M");
			CString DisplayEnd   = omDisplayEnd.Format("%Y.%m.%d - %H:%M"); 
			printf("hello");
		}
	*/	
		if (/*ilX < imVerticalScaleWidth || */opStartTime <= omDisplayStart)
			rcPaint.left = imVerticalScaleWidth;
		else
			rcPaint.left = ilX; //(int)GetX(opStartTime);
//	 }
    // Use the client width if user want to repaint to the end of time
    if (opEndTime != TIMENULL && opEndTime <= omDisplayEnd)
        rcPaint.right = (int)GetX(opEndTime) + 1;

    InvalidateRect(&rcPaint, bpErase);
	if(pomCoverageWindow != NULL)
	{
//		pomCoverageWindow->SetTimeFrame(omDisplayStart, omDisplayEnd);
		if(::IsWindow(pomCoverageWindow->GetSafeHwnd()))
		{
			pomCoverageWindow->UpdateWindow();
		}
	}
   CCS_CATCH_ALL
}

void CoverageGantt::RepaintItemHeight(int ipLineno)
{
   CCS_TRY
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

	if(ipLineno < 0)
		return;
    // This repaint method will recalculate the item height
    SetItemHeight(ipLineno, GetLineHeight(ipLineno));

    CRect rcItem, rcPaint;
    GetClientRect(&rcPaint);
    GetItemRect(ipLineno, &rcItem);
    rcPaint.top = rcItem.top;
    InvalidateRect(&rcPaint, TRUE);
   CCS_CATCH_ALL
}


/////////////////////////////////////////////////////////////////////////////
// CoverageGantt implementation

void CoverageGantt::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
   CCS_TRY
    // We have to define MeasureItem() here since MFC places an ASSERT inside
    // the default MeasureItem() of an owner-drawn CListBox.
   CCS_CATCH_ALL
}

void CoverageGantt::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
   CCS_TRY
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// To let the GanttChart know automatically those changes in TimeScale.
// We had to reset the value of omDisplayStart and omDisplayEnd everytime.
	omDisplayStart = GetCTime(imVerticalScaleWidth);
	omDisplayEnd = GetCTime(imWindowWidth);
////////////////////////////////////////////////////////////////////////
    
	int itemID = lpDrawItemStruct->itemID;

    // Attention:
    // This optimization will the speed of displaying GanttChart very a lot.
    // However, this works because we hadn't display any selected or focus items.
    // (We use the "imCurrentItem" variable instead.)
    // So, be careful since this assumption may be changed in the future.
    //
    if (itemID == -1 || itemID > pomViewer->GetLineCount(imGroupno)-1)
        return;
    if (!(lpDrawItemStruct->itemAction & ODA_DRAWENTIRE))
        return; // nothing more to do if listbox just change its selection and/or focus item

    // Drawing routines start here
    CDC dc;
    dc.Attach(lpDrawItemStruct->hDC);
    CRect rcItem(&lpDrawItemStruct->rcItem);
    CRect rcClip;
    dc.GetClipBox(&rcClip);

    // Draw vertical scale if necessary
    if (rcClip.left < imVerticalScaleWidth)
        DrawVerticalScale(&dc, itemID, rcItem);

    // Draw time lines and bars if necessary
    if (imVerticalScaleWidth <= rcClip.right)
    {
		//MWO
		//rcItem.bottom = rcItem.top - imBarHeight;
		// Ende MWO
        CRgn rgn;
        CRect rect(imVerticalScaleWidth, rcItem.top, rcItem.right, rcItem.bottom);
        rgn.CreateRectRgnIndirect(&rect);
        dc.SelectClipRgn(&rgn);
		DrawBackgroundBars(&dc, itemID, rcItem, rcClip);
        //DrawTimeLines(&dc, rcItem.top, rcItem.bottom);
        DrawGanttChart(&dc, itemID, rcItem, rcClip);
        DrawTimeLines(&dc, rcItem.top, rcItem.bottom);
        if (umResizeMode != HTNOWHERE)
            DrawFocusRect(&dc, &omRectResize);
        dc.SelectClipRgn(NULL);
    }

    dc.Detach();

   CCS_CATCH_ALL
}


/////////////////////////////////////////////////////////////////////////////
// CoverageGantt implementation helper functions

void CoverageGantt::DrawVerticalScale(CDC *pDC, int itemID, const CRect &rcItem)
{
}

void CoverageGantt::DrawBackgroundBars(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip)
{
   CCS_TRY
    if (omDisplayStart >= omDisplayEnd) // no space to display?
        return;
	CBrush olBlackBrush(COLORREF(BLACK));

    // Draw each bar if it in the range of the clipped box
    for (int i = 0; i < pomViewer->GetBkBarCount(imGroupno, itemID); i++)
    {
        COV_BKBARDATA *prlBkBar = pomViewer->GetBkBar(imGroupno, itemID, i);

        if (!IsOverlapped(prlBkBar->StartTime, prlBkBar->EndTime, omDisplayStart, omDisplayEnd))
            continue;   // skip bar which is out of time range

        int left = (int)GetX(prlBkBar->StartTime);
        int right = (int)GetX(prlBkBar->EndTime);
        int top = rcItem.top;
        int bottom = rcItem.bottom;
		CCSPtrArray<CCSBarDecoStruct> olDecoData;
		CCSBarDecoStruct rlBarDeco;

        if (!IsOverlapped(left, right, rcClip.left, rcClip.right))
            continue;   // skip bar which is out of clipping region

		rlBarDeco.Rect = CRect(left, top, right, bottom);
		rlBarDeco.Type = BD_CCSBAR;
		rlBarDeco.Text = prlBkBar->Text;
		//rlBarDeco.BkColor = prlBar->BkColor;
		//rlBarDeco.TextColor = prlBar->TextColor;
		rlBarDeco.HorizontalAlignment = BD_CENTER;
		rlBarDeco.VerticalAlignment = BD_CENTER;
		rlBarDeco.MarkerBrush = prlBkBar->MarkerBrush;
		rlBarDeco.MarkerType = MARKFULL;
		rlBarDeco.TextFont = pomGanttChartFont;
		olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);
		CCSGanttBar olPaintBar(pDC,olDecoData);
		olDecoData.DeleteAll();

    }
   CCS_CATCH_ALL
}

void CoverageGantt::DrawGanttChart(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip)
{
   CCS_TRY
    if (omDisplayStart >= omDisplayEnd) // no space to display?
        return;

	CPen olWhite3DPen1(PS_SOLID, 1, COLORREF(WHITE));
	CPen olWhite3DPen2(PS_SOLID, 1, COLORREF(SILVER));
	CPen olGray3DPen1(PS_SOLID, 1, COLORREF(SILVER));
	CPen olGray3DPen2(PS_SOLID, 1, COLORREF(BLACK));

    // Draw each bar if it in the range of the clipped box
	if(imGroupno == BASIC_SHIFT)
	{
		int ilxx = pomViewer->GetLineCount(imGroupno);
		for (int i = 0; i < pomViewer->GetBarCount(imGroupno, itemID); i++)
		{
			CCSPtrArray<CCSBarDecoStruct> olDecoData;
			CCSBarDecoStruct rlBarDeco;
			COV_BARDATA *prlBar = pomViewer->GetBar(imGroupno, itemID, i);
			if(prlBar != NULL)
			{
				if (IsOverlapped(prlBar->StartTime, prlBar->EndTime, omDisplayStart, omDisplayEnd))
				{
					BASICSHIFTS *prlShift = pomViewer->GetBasicShiftByUrno(prlBar->Urno);
					if(prlShift != NULL)
					{
						int left = (int)GetX(prlBar->StartTime);
						int right = (int)GetX(prlBar->EndTime);
						int top = rcItem.top /*MWO+ imGutterHeight*/ + (prlBar->OverlapLevel * imOverlapHeight);
						int bottom = top + imBarHeight-1;

						int OrigLeft = left;
						int OrigRight = right;
						int OrigTop = top;
						int OrigBottom = bottom;
	
						//OLD - GSA prints text in background
						rlBarDeco.Rect = CRect(left, top, right, bottom);
						rlBarDeco.Type = BD_CCSBAR;
						//CString olTxt = CString("  ") + prlBar->Text; 
						rlBarDeco.Text = ""; //olTxt;
						rlBarDeco.BkColor = COLORREF(BLACK);
						//rlBarDeco.TextColor = prlBar->TextColor;
						rlBarDeco.HorizontalAlignment = BD_LEFT;
						rlBarDeco.VerticalAlignment = BD_CENTER;
	
						if(prlShift->IsSelected)
						{
							rlBarDeco.MarkerBrush = ogBrushs[YELLOW_IDX];
						}
						else
						{
							rlBarDeco.MarkerBrush = prlBar->MarkerBrush;
						}
						if(prlBar->IsInvalid)
						{
							rlBarDeco.MarkerBrush = ogBrushs[RED_IDX];
						}
						rlBarDeco.MarkerType = prlBar->MarkerType;
						rlBarDeco.TextFont = pomGanttChartFont;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

						for(int ilDelIdx = 0; ilDelIdx < prlBar->Delegations.GetSize();ilDelIdx++)
						{
							left  = (int)GetX(prlBar->Delegations[ilDelIdx].StartTime);
							right = (int)GetX(prlBar->Delegations[ilDelIdx].EndTime);
							rlBarDeco.Rect = CRect(left, top, right,  bottom);
							rlBarDeco.Type = BD_CCSBAR;
							rlBarDeco.Text = prlBar->Delegations[ilDelIdx].Text;
							rlBarDeco.BkColor = COLORREF(RGB(0, 128,128));
							//rlBarDeco.TextColor = prlBar->TextColor;
							rlBarDeco.HorizontalAlignment = BD_CENTER;
							rlBarDeco.VerticalAlignment = BD_CENTER;
							//rlBarDeco.MarkerBrush = ogBrushs[11];
							rlBarDeco.MarkerBrush = ogBrushs[SILVER_IDX]; 
							rlBarDeco.MarkerType = prlBar->MarkerType;
							rlBarDeco.TextFont = pomGanttChartFont;
							rlBarDeco.FrameType = FRAMEBACKGROUND;
							olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);
						}


						left  = (int)GetX(prlShift->BlFrom + CTimeSpan(prlShift->DayOffSet,0,0,0));
						right = (int)GetX(prlShift->BlTo + CTimeSpan(prlShift->DayOffSet,0,0,0));
						int ilOffSet = (int) ceil((bottom - top)/2);
						rlBarDeco.Rect = CRect(left, top, right, top + ilOffSet);
						rlBarDeco.Type = BD_CCSBAR;
						rlBarDeco.Text = "";
						rlBarDeco.BkColor = COLORREF(RGB(0, 128,128));
						//rlBarDeco.TextColor = prlBar->TextColor;
						rlBarDeco.HorizontalAlignment = BD_CENTER;
						rlBarDeco.VerticalAlignment = BD_CENTER;
						//rlBarDeco.MarkerBrush = ogBrushs[11];
						rlBarDeco.MarkerBrush = ogBrushs[18]; // ORANGE
						rlBarDeco.MarkerType = prlBar->MarkerType;
						rlBarDeco.TextFont = pomGanttChartFont;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

						left  = (int)GetX(prlShift->BreakFrom + CTimeSpan(prlShift->DayOffSet,0,0,0));
						right = (int)GetX(prlShift->BreakTo + CTimeSpan(prlShift->DayOffSet,0,0,0));
				    	ilOffSet = (int) ceil((bottom - top)/2);
						rlBarDeco.Rect = CRect(left, top, right, top + ilOffSet);
						rlBarDeco.Type = BD_CCSBAR;
						rlBarDeco.Text = "";
						rlBarDeco.BkColor = COLORREF(RGB(0, 128,128));
						//rlBarDeco.TextColor = prlBar->TextColor;
						rlBarDeco.HorizontalAlignment = BD_CENTER;
						rlBarDeco.VerticalAlignment = BD_CENTER;
						rlBarDeco.MarkerBrush = ogBrushs[21];
						rlBarDeco.MarkerType = prlBar->MarkerType;
						rlBarDeco.TextFont = pomGanttChartFont;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

									
//3D --Oben und links
						rlBarDeco.Rect = CRect(0, 0, 0, 0);
						rlBarDeco.Type = BD_LINE;
						rlBarDeco.Text = "";
						rlBarDeco.PointFrom = CPoint(OrigLeft, OrigTop);
						rlBarDeco.PointTo = CPoint(OrigRight, OrigTop);
						rlBarDeco.pPen = &olWhite3DPen1;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

						rlBarDeco.Rect = CRect(0, 0, 0, 0);
						rlBarDeco.Type = BD_LINE;
						rlBarDeco.Text = "";
						rlBarDeco.PointFrom = CPoint(OrigLeft+1, OrigTop+1);
						rlBarDeco.PointTo = CPoint(OrigRight-1, OrigTop+1);
						rlBarDeco.pPen = &olWhite3DPen2;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

						rlBarDeco.Rect = CRect(0, 0, 0, 0);
						rlBarDeco.Type = BD_LINE;
						rlBarDeco.Text = "";
						rlBarDeco.PointFrom = CPoint(OrigLeft, OrigTop);
						rlBarDeco.PointTo = CPoint(OrigLeft, OrigBottom);
						rlBarDeco.pPen = &olWhite3DPen1;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

						rlBarDeco.Rect = CRect(0, 0, 0, 0);
						rlBarDeco.Type = BD_LINE;
						rlBarDeco.Text = "";
						rlBarDeco.PointFrom = CPoint(OrigLeft+1, OrigTop+1);
						rlBarDeco.PointTo = CPoint(OrigLeft+1, OrigBottom+1);
						rlBarDeco.pPen = &olWhite3DPen2;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);
				//Unten und rechts
						rlBarDeco.Rect = CRect(0, 0, 0, 0);
						rlBarDeco.Type = BD_LINE;
						rlBarDeco.Text = "";
						rlBarDeco.PointFrom = CPoint(OrigLeft+1, OrigBottom);
						rlBarDeco.PointTo = CPoint(OrigRight, OrigBottom);
						rlBarDeco.pPen = &olGray3DPen2;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

						rlBarDeco.Rect = CRect(0, 0, 0, 0);
						rlBarDeco.Type = BD_LINE;
						rlBarDeco.Text = "";
						rlBarDeco.PointFrom = CPoint(OrigLeft+2, OrigBottom-1);
						rlBarDeco.PointTo = CPoint(OrigRight-1, OrigBottom-1);
						rlBarDeco.pPen = &olGray3DPen1;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

						rlBarDeco.Rect = CRect(0, 0, 0, 0);
						rlBarDeco.Type = BD_LINE;
						rlBarDeco.Text = "";
						rlBarDeco.PointFrom = CPoint(OrigRight, OrigBottom);
						rlBarDeco.PointTo = CPoint(OrigRight, OrigTop-1);
						rlBarDeco.pPen = &olGray3DPen2;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

						rlBarDeco.Rect = CRect(0, 0, 0, 0);
						rlBarDeco.Type = BD_LINE;
						rlBarDeco.Text = "";
						rlBarDeco.PointFrom = CPoint(OrigRight-1, OrigTop+2);
						rlBarDeco.PointTo = CPoint(OrigRight-1, OrigBottom-1);
						rlBarDeco.pPen = &olGray3DPen1;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

						// NEW - GSA always prints text in foreground
						
						rlBarDeco.Rect = CRect(OrigLeft, OrigTop, OrigRight, OrigBottom);
						rlBarDeco.Type = BD_TEXT; //BD_CCSBAR;
						CString olTxt = CString("  ") + prlBar->Text; 
						rlBarDeco.Text = olTxt;
						//rlBarDeco.BkColor = COLORREF(WHITE);
						rlBarDeco.BkColor = COLORREF(BLACK);
						rlBarDeco.TextColor = prlBar->TextColor;
						rlBarDeco.HorizontalAlignment = BD_LEFTOFBAR;
						rlBarDeco.VerticalAlignment = BD_CENTER;
						rlBarDeco.MarkerBrush = prlBar->MarkerBrush;
						rlBarDeco.MarkerType = prlBar->MarkerType;
						rlBarDeco.TextFont = pomGanttChartFont;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);
						
						CCSGanttBar olPaintBar(pDC,olDecoData);
					}
				}//ifIsoverlapped (Balken und Darzustellender Zeitbereich)
			} //if(prlBar != NULL)

			olDecoData.DeleteAll();
		}
	}//if(imGroupno == BASIC_SHIFT)
	else if(imGroupno == SHIFT_DEMAND)
	{
	//	pomViewer->omGroups[SHIFT_DEMAND].omLines
		for ( int i = 0; i < pomViewer->GetBarCount(imGroupno, itemID); i++)
		{
			CCSPtrArray<CCSBarDecoStruct> olDecoData;
			CCSBarDecoStruct rlBarDeco;
			

			COV_BARDATA *prlBar = pomViewer->GetBar(imGroupno, itemID, i);
			if(prlBar != NULL)
			{
				if (IsOverlapped(prlBar->StartTime, prlBar->EndTime, omDisplayStart, omDisplayEnd))
				{
					SDTDATA *prlSdt = NULL;
					if(!pomViewer->GetShiftRoster())
					{
						prlSdt = ogSdtData.GetSdtByUrno(prlBar->Urno);
					}
					else
					{
						if(pomViewer->bmIsRealRoster)
						{
							prlSdt = ogSdtRosterData.GetSdtByUrno(prlBar->Urno);
						}
						else
						{
							prlSdt = ogSdtShiftData.GetSdtByUrno(prlBar->Urno);
						}

					}

					if(prlSdt != NULL)
					{
						int left = (int)GetX(prlBar->StartTime);
						int right = (int)GetX(prlBar->EndTime);
						int top = rcItem.top  + (prlBar->OverlapLevel * imOverlapHeight);
						int bottom = top + imBarHeight-1;

						int OrigLeft = left;
						int OrigRight = right;
						int OrigTop = top;
						int OrigBottom = bottom;

						// OLD GSA
						rlBarDeco.Rect = CRect(left, top, right, bottom);
						rlBarDeco.Type = BD_CCSBAR;
						rlBarDeco.Text = ""; //olTxt;
						rlBarDeco.BkColor = COLORREF(BLACK);
						rlBarDeco.HorizontalAlignment = BD_LEFT;
						rlBarDeco.VerticalAlignment = BD_CENTER;
						rlBarDeco.MarkerBrush = prlBar->MarkerBrush;
						rlBarDeco.MarkerType = prlBar->MarkerType;
						rlBarDeco.TextFont = pomGanttChartFont;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);
						

						// Dynamic shift
						
						if(strcmp(prlSdt->Type,"D")==0 && prlSdt->Sdu1>0)
						{
							CTimeSpan olShiftDura(prlSdt->Sdu1*60); // = CTimeSpan(0,ilHour,ilMin,0);

							if(IsBetween(prlSdt->Sbgi,prlSdt->Esbg,prlSdt->Lsen))
							{
								left  = (int)GetX(prlSdt->Sbgi);	
							}
							else
							{
								
								CTimeSpan olDynDura = prlSdt->Lsen - prlSdt->Esbg;
								CTimeSpan olDiff= olDynDura.GetTotalMinutes()-prlSdt->Sdu1; //olShiftDura;
								prlSdt->Sbgi = prlSdt->Esbg + olDiff;							
								left  = (int)GetX(prlSdt->Sbgi);
							}

							if(IsBetween(prlSdt->Seni,prlSdt->Esbg,prlSdt->Lsen))
							{
 								right = (int)GetX(prlSdt->Seni);
							}
							else
							{
								prlSdt->Seni = (CTime)(prlSdt->Sbgi+olShiftDura);
								right = (int)GetX(prlSdt->Seni);
							}
							
							rlBarDeco.Rect = CRect(left, top, right, bottom);
							rlBarDeco.Type = BD_CCSBAR;
							rlBarDeco.Text = "";
							rlBarDeco.BkColor = COLORREF(RGB(32,32,32));
							rlBarDeco.HorizontalAlignment = BD_CENTER;
							rlBarDeco.VerticalAlignment = BD_CENTER;
							rlBarDeco.MarkerBrush = ogBrushs[11]; // TEAL
							rlBarDeco.MarkerType = prlBar->MarkerType;
							rlBarDeco.TextFont = pomGanttChartFont;
							olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);
						}

						if(pomViewer->bmMaxView == false)
						{

						}
						CTime olFrom = CTime::GetCurrentTime();

						left  = (int)GetX(prlSdt->Bkf1);
						right = (int)GetX(prlSdt->Bkt1);
						int ilOffSet = (int) ceil((bottom - top)/2);
						rlBarDeco.Rect = CRect(left, top, right, top + ilOffSet);
						rlBarDeco.Type = BD_CCSBAR;
						rlBarDeco.Text = "";
						rlBarDeco.BkColor = COLORREF(RGB(0, 128,128));
						rlBarDeco.HorizontalAlignment = BD_CENTER;
						rlBarDeco.VerticalAlignment = BD_CENTER;
						rlBarDeco.MarkerBrush = ogBrushs[18]; // ORANGE
						rlBarDeco.MarkerType = prlBar->MarkerType;
						rlBarDeco.TextFont = pomGanttChartFont;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

						left  = (int)GetX(prlSdt->Brkf);
						right = (int)GetX(prlSdt->Brkt);
						ilOffSet = (int) ceil((bottom - top)/2);
						rlBarDeco.Rect = CRect(left, top, right, top + ilOffSet);
						rlBarDeco.Type = BD_CCSBAR;
						rlBarDeco.Text = "";
						rlBarDeco.BkColor = COLORREF(RGB(0, 128,128));
						rlBarDeco.HorizontalAlignment = BD_CENTER;
						rlBarDeco.VerticalAlignment = BD_CENTER;
						rlBarDeco.MarkerBrush = ogBrushs[21];
						rlBarDeco.MarkerType = prlBar->MarkerType;
						rlBarDeco.TextFont = pomGanttChartFont;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

						
						//3D --Oben und links
						rlBarDeco.Rect = CRect(0, 0, 0, 0);
						rlBarDeco.Type = BD_LINE;
						rlBarDeco.Text = "";
						rlBarDeco.PointFrom = CPoint(OrigLeft, OrigTop);
						rlBarDeco.PointTo = CPoint(OrigRight, OrigTop);
						rlBarDeco.pPen = &olWhite3DPen1;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

						rlBarDeco.Rect = CRect(0, 0, 0, 0);
						rlBarDeco.Type = BD_LINE;
						rlBarDeco.Text = "";
						rlBarDeco.PointFrom = CPoint(OrigLeft+1, OrigTop+1);
						rlBarDeco.PointTo = CPoint(OrigRight-1, OrigTop+1);
						rlBarDeco.pPen = &olWhite3DPen2;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

						rlBarDeco.Rect = CRect(0, 0, 0, 0);
						rlBarDeco.Type = BD_LINE;
						rlBarDeco.Text = "";
						rlBarDeco.PointFrom = CPoint(OrigLeft, OrigTop);
						rlBarDeco.PointTo = CPoint(OrigLeft, OrigBottom);
						rlBarDeco.pPen = &olWhite3DPen1;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

						rlBarDeco.Rect = CRect(0, 0, 0, 0);
						rlBarDeco.Type = BD_LINE;
						rlBarDeco.Text = "";
						rlBarDeco.PointFrom = CPoint(OrigLeft+1, OrigTop+1);
						rlBarDeco.PointTo = CPoint(OrigLeft+1, OrigBottom+1);
						rlBarDeco.pPen = &olWhite3DPen2;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);
				//Unten und rechts
						rlBarDeco.Rect = CRect(0, 0, 0, 0);
						rlBarDeco.Type = BD_LINE;
						rlBarDeco.Text = "";
						rlBarDeco.PointFrom = CPoint(OrigLeft+1, OrigBottom);
						rlBarDeco.PointTo = CPoint(OrigRight, OrigBottom);
						rlBarDeco.pPen = &olGray3DPen2;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

						rlBarDeco.Rect = CRect(0, 0, 0, 0);
						rlBarDeco.Type = BD_LINE;
						rlBarDeco.Text = "";
						rlBarDeco.PointFrom = CPoint(OrigLeft+2, OrigBottom-1);
						rlBarDeco.PointTo = CPoint(OrigRight-1, OrigBottom-1);
						rlBarDeco.pPen = &olGray3DPen1;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

						rlBarDeco.Rect = CRect(0, 0, 0, 0);
						rlBarDeco.Type = BD_LINE;
						rlBarDeco.Text = "";
						rlBarDeco.PointFrom = CPoint(OrigRight, OrigBottom);
						rlBarDeco.PointTo = CPoint(OrigRight, OrigTop-1);
						rlBarDeco.pPen = &olGray3DPen2;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

						rlBarDeco.Rect = CRect(0, 0, 0, 0);
						rlBarDeco.Type = BD_LINE;
						rlBarDeco.Text = "";
						rlBarDeco.PointFrom = CPoint(OrigRight-1, OrigTop+2);
						rlBarDeco.PointTo = CPoint(OrigRight-1, OrigBottom-1);
						rlBarDeco.pPen = &olGray3DPen1;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

						
						// NEW GSA prints text in foreground
						rlBarDeco.Rect = CRect(OrigLeft, OrigTop, OrigRight, OrigBottom);
						rlBarDeco.Type = BD_TEXT; //BD_CCSBAR;
						CString olTxt = CString("  ") + prlBar->Text; 
						rlBarDeco.Text = olTxt;
						rlBarDeco.TextColor = prlBar->TextColor;
						rlBarDeco.HorizontalAlignment = BD_LEFTOFBAR;
						rlBarDeco.VerticalAlignment = BD_CENTER;
						rlBarDeco.MarkerBrush = prlBar->MarkerBrush;
						rlBarDeco.MarkerType = prlBar->MarkerType;
						rlBarDeco.TextFont = pomGanttChartFont;
						olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

						for(int ilDelIdx = 0; ilDelIdx < prlBar->Delegations.GetSize();ilDelIdx++)
						{
							left  = (int)GetX(prlBar->Delegations[ilDelIdx].StartTime);
							right = (int)GetX(prlBar->Delegations[ilDelIdx].EndTime);
							rlBarDeco.Rect = CRect(left, top, right,  bottom);
							rlBarDeco.Type = BD_CCSBAR;
							rlBarDeco.Text = prlBar->Delegations[ilDelIdx].Text;
							rlBarDeco.BkColor = COLORREF(RGB(0, 128,128));
							rlBarDeco.HorizontalAlignment = BD_CENTER;
							rlBarDeco.VerticalAlignment = BD_CENTER;
							rlBarDeco.MarkerBrush = ogBrushs[SILVER_IDX]; 
							rlBarDeco.MarkerType = prlBar->MarkerType;
							rlBarDeco.TextFont = pomGanttChartFont;
							olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);
						}
						for(int ilDelIdx2 = 0; ilDelIdx2 < prlBar->Delegations2.GetSize();ilDelIdx2++)
						{
							left  = (int)GetX(prlBar->Delegations2[ilDelIdx2].StartTime);
							right = (int)GetX(prlBar->Delegations2[ilDelIdx2].EndTime);
							rlBarDeco.Rect = CRect(left, top, right,  bottom);
							rlBarDeco.Type = BD_CCSBAR;
							rlBarDeco.Text = prlBar->Delegations2[ilDelIdx2].Text;
							rlBarDeco.BkColor = COLORREF(RGB(0, 128,128));
							rlBarDeco.HorizontalAlignment = BD_CENTER;
							rlBarDeco.VerticalAlignment = BD_CENTER;
							rlBarDeco.MarkerBrush = ogBrushs[BLUE_IDX]; 
							rlBarDeco.MarkerType = prlBar->MarkerType;
							rlBarDeco.TextFont = pomGanttChartFont;
							olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);
						}
						for(int ilDelIdx3 = 0; ilDelIdx3 < prlBar->Delegations3.GetSize();ilDelIdx3++)
						{
							left  = (int)GetX(prlBar->Delegations3[ilDelIdx3].StartTime);
							right = (int)GetX(prlBar->Delegations3[ilDelIdx3].EndTime);
							rlBarDeco.Rect = CRect(left, top, right,  bottom);
							rlBarDeco.Type = BD_CCSBAR;
							rlBarDeco.Text = prlBar->Delegations3[ilDelIdx3].Text;
							rlBarDeco.BkColor = COLORREF(RGB(0, 128,128));
							rlBarDeco.HorizontalAlignment = BD_CENTER;
							rlBarDeco.VerticalAlignment = BD_CENTER;
							rlBarDeco.MarkerBrush = ogBrushs[RED_IDX]; 
							rlBarDeco.MarkerType = prlBar->MarkerType;
							rlBarDeco.TextFont = pomGanttChartFont;
							olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);
						}
						
						CCSGanttBar olPaintBar(pDC,olDecoData);
					}
				}//ifIsoverlapped (Balken und Darzustellender Zeitbereich)
			} //if(prlBar != NULL)

			olDecoData.DeleteAll();
		}
	}
   CCS_CATCH_ALL
}

void CoverageGantt::DrawTimeLines(CDC *pDC, int top, int bottom, BOOL bpWithMarkLines)
{
   CCS_TRY
    // Draw current time
    if (omCurrentTime != TIMENULL && IsBetween(omCurrentTime, omDisplayStart, omDisplayEnd))
    {
        CPen penRed(PS_SOLID, 0, RGB(255, 0, 0));
        CPen *pOldPen = pDC->SelectObject(&penRed);
        int x = (int)GetX(omCurrentTime);
        pDC->MoveTo(x, top);
        pDC->LineTo(x, bottom);
        pDC->SelectObject(pOldPen);
    }

    // Draw marked time start
	 if (omMarkTimeStart != TIMENULL && IsBetween(omMarkTimeStart, omDisplayStart, omDisplayEnd))
	 {
		  CPen penYellow(PS_SOLID, 0, RGB(255, 255, 0));
		  CPen *pOldPen = pDC->SelectObject(&penYellow);
		  int x = (int)GetX(omMarkTimeStart);
		  pDC->MoveTo(x, top);
		  pDC->LineTo(x, bottom);
		  pDC->SelectObject(pOldPen);
	 }

	 // Draw marked time end
	 if (omMarkTimeEnd != TIMENULL && IsBetween(omMarkTimeEnd, omDisplayStart, omDisplayEnd))
	 {
		  CPen penYellow(PS_SOLID, 0, RGB(255, 255, 0));
		  CPen *pOldPen = pDC->SelectObject(&penYellow);
		  int x = (int)GetX(omMarkTimeEnd);
		  pDC->MoveTo(x, top);
		  pDC->LineTo(x, bottom);
		  pDC->SelectObject(pOldPen);
	 }
   CCS_CATCH_ALL
}

void CoverageGantt::DrawFocusRect(CDC *pDC, const CRect &rect)
{
   CCS_TRY

	CRect rcFocus = rect;
	rcFocus.right++;	// extra pixel to help DrawFocusRect() work correctly
	pDC->DrawFocusRect(&rcFocus);

	//***** Before finish: please insert the correct code for status bar while moving/resizing
    if (pomStatusBar != NULL)
	{
		CString s = GetCTime(rcFocus.left).Format("%H%M") + " - "
			+ GetCTime(rcFocus.right - 1).Format("%H%M");
		pomStatusBar->SetPaneText(0, s);
	}
   CCS_CATCH_ALL
}


/////////////////////////////////////////////////////////////////////////////
// CoverageGantt message handlers

BEGIN_MESSAGE_MAP(CoverageGantt, CWnd)
	//{{AFX_MSG_MAP(CoverageGantt)
	ON_WM_CREATE()
	ON_WM_DESTROY()
    ON_WM_SIZE()
    ON_WM_ERASEBKGND()
    ON_WM_MOUSEMOVE()
    ON_WM_TIMER()
    ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_COMMAND(40, OnMenuNewShift)
	ON_COMMAND(41, OnMenuDeleteDemand)
	ON_COMMAND(42, OnMenuChangeShift)
	ON_COMMAND(43, OnMenuDuplicateShift)
	ON_COMMAND(44, OnMenuCopyShift)
	ON_COMMAND(45, OnMenuNumberShift)
	//ON_COMMAND(44, OnMenuCopyShift)
	ON_COMMAND(46, OnMenuDebugInfo)
	ON_COMMAND(47, OnMenuDebugStatistic)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
   ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnDragBegin)
	ON_MESSAGE(WM_SETTIMELINES, OnDragTime)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

int CoverageGantt::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
   CCS_TRY
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
    m_DragDropTarget.RegisterTarget(this, this);
	
	return 0;
	CCS_CATCH_ALL
	return -1;
}

BOOL CoverageGantt::DestroyWindow()
{
   CCS_TRY
	m_DragDropTarget.Revoke();
	return(CListBox::DestroyWindow());
	CCS_CATCH_ALL
	return FALSE;

}

void CoverageGantt::OnDestroy() 
{
   CCS_TRY
	ogDdx.UnRegister(this, NOTUSED);
	CListBox::OnDestroy();
	CCS_CATCH_ALL
}

void CoverageGantt::OnSize(UINT nType, int cx, int cy)
{
   CCS_TRY
    CListBox::OnSize(nType, cx, cy);

    CRect rect;
    GetWindowRect(&rect);   // don't use cx (vertical scroll may less the width of client rect)

    if (bmIsFixedScaling)   // fixed-scaling mode?
    {
        if (imWindowWidth == 0) // first time initialization?
		{
            imWindowWidth = rect.Width();
			omDisplayEnd = GetCTime(imWindowWidth);   // has to recalculate the right most boundary?
		}
    }
    else    // must be automatic variable-scaling mode
    {
        if (rect.Width() != imWindowWidth) // display window changed?
            InvalidateRect(NULL);   // repaint whole GanttChart
    }
    imWindowWidth = rect.Width();
	CCS_CATCH_ALL
}

// Attention:
// Be careful, this may not work if you try to develop horizontal-scrolling in GanttChart.
// Generally, if the ListBox need to be repainted, it will send the message WM_ERASEBKGND.
// But if you allow horizontal-scrolling, WM_ERASEBKGND will be called before the new
// horizontal position could be detected by dc.GetWindowOrg().x in DrawItem().
// However, this version work fine since there's no such scrolling.
// Then, we can assume that the beginning offset will be 0 all the time.
//
BOOL CoverageGantt::OnEraseBkgnd(CDC* pDC)
{
   CCS_TRY
    CRect rectErase;
    pDC->GetClipBox(&rectErase);

    // Draw the VerticalScale if the user specify it to be displayed
    if (imVerticalScaleWidth > 0)
    {
//		int ilx;
        CBrush brush(lmVerticalScaleBackgroundColor);
        CRect rect(0, rectErase.top, imVerticalScaleWidth, rectErase.bottom);
        pDC->FillRect(&rect, &brush);

        // Draw seperator line between VerticalScale and the body of GanttChart
        CPen penBlack(PS_SOLID, 0, ::GetSysColor(COLOR_WINDOWFRAME));
        CPen *pOldPen = pDC->SelectObject(&penBlack);
		//At the End of VerticalScale
        pDC->MoveTo(imVerticalScaleWidth-2, rectErase.top);
        pDC->LineTo(imVerticalScaleWidth-2, rectErase.bottom);
    }

    // Draw the background of the body of GanttChart
    CBrush brush(lmGanttChartBackgroundColor);
    CRect rect(imVerticalScaleWidth, rectErase.top, rectErase.right, rectErase.bottom);
    pDC->FillRect(&rect, &brush);

    // Draw the vertical current time and marked time lines
	 //if(bmMarkLines == FALSE)
		DrawTimeLines(pDC, rectErase.top, rectErase.bottom);
	 //else
	 //DrawTimeLines(pDC, rectErase.top, rectErase.bottom, TRUE);

    // Tell Windows there's nothing more to do
    return TRUE;
	CCS_CATCH_ALL
	return FALSE;
}

void CoverageGantt::OnMouseMove(UINT nFlags, CPoint point)

{
   CCS_TRY
    ::GetCursorPos(&point);
	 ScreenToClient(&point);    
    if (umResizeMode != HTNOWHERE)  // in SetCapture()?
    {
        OnMovingOrResizing(point, nFlags & MK_LBUTTON);
        return;
    }


	// Checking for dragging a job bar?
	// Notes: Here's the best place for checking for dragging from job bar.
	// We cannot simply regard OnLButtonDown() as the beginning of dragging,
	// since we defined OnLButtonDown() for bringing bar to front or to back.
	if ((nFlags & MK_LBUTTON) &&
		/*imHighlightLine != -1 &&*/ imCurrentBar != -1)
	{
      int itemID = GetItemFromPoint(point);
      int ilBarno = GetBarnoFromPoint(itemID, point);
		if(ilBarno != -1 && itemID != -1)
		{
			if(imGroupno == BASIC_SHIFT)
			{
				DragBsdBarBegin(itemID, imCurrentBar);
			}
			if(imGroupno == SHIFT_DEMAND)
			{
				//DragGhdBarBegin(itemID, ilBarno);
			}
		}
	}

    CListBox::OnMouseMove(nFlags, point);
    UpdateGanttChart(point, nFlags & MK_CONTROL);
	CCS_CATCH_ALL
}

void CoverageGantt::OnTimer(UINT nIDEvent)
{
   CCS_TRY
    if (umResizeMode != HTNOWHERE)  // in SetCapture()?
        return;

    if (nIDEvent == 0)  // the last timer message, clear everything
    {
		bmIsMouseInWindow = FALSE;
        RepaintVerticalScale(imHighlightLine, FALSE);
        imHighlightLine = -1;       // no more selected line
        UpdateBarStatus(-1, -1);    // no more status if mouse is outside of GanttChart
        return;
    }

    CPoint point;
    ::GetCursorPos(&point);
    ScreenToClient(&point);
    UpdateGanttChart(point, ::GetKeyState(VK_CONTROL) & 0x8080);
		// This statement has to be fixed for using both in Windows 3.11 and NT.
		// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
		// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.
	CCS_CATCH_ALL
}

void CoverageGantt::OnLButtonDown(UINT nFlags, CPoint point)
{
   CCS_TRY
	CListBox::OnLButtonDown(nFlags, point);
	// Update the cursor position to make sure that the cursor will always be over
	// the bar we just click on.
	CPoint olMousePosition = point;
	ClientToScreen(&olMousePosition);
	::SetCursorPos(olMousePosition.x, olMousePosition.y);
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));


	
	COV_BARDATA *prlBar;
    int itemID = GetItemFromPoint(point);
    int ilBarno = GetBarnoFromPoint(itemID, point);
    imCurrentBar = GetBarnoFromPoint(itemID, point);
	if(ilBarno==-1 || itemID ==-1)
	{
		ogBsdData.DeSelectAll();
		pomViewer->DeSelectBsh();
		ogSdtData.DeSelectAll();
		ogSdtRosterData.DeSelectAll();
		ogSdtShiftData.DeSelectAll();
		pomViewer->SetMarkTime(0,0);

		//
		GetParent() -> GetParent() -> GetParent()->SendMessage(WM_AD_BUTTON, 0, 0L);
		return;
	}
	prlBar = pomViewer->GetBar( imGroupno, itemID, ilBarno);
	if(prlBar==NULL)
	{
		ogBsdData.DeSelectAll();
		pomViewer->DeSelectBsh();
		ogSdtData.DeSelectAll();
		ogSdtRosterData.DeSelectAll();
		ogSdtShiftData.DeSelectAll();
		pomViewer->SetMarkTime(0,0);
		//
		GetParent() -> GetParent() -> GetParent()->SendMessage(WM_AD_BUTTON, 0, 0L);
		return;
	}
	if(nFlags == MK_LBUTTON)
	{
		switch(imGroupno)
		{
		case BASIC_SHIFT:
			{
				BASICSHIFTS *prlBsh = pomViewer->GetBasicShiftByUrno(prlBar->Urno);
				if(prlBsh != NULL)
				{
					BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(prlBsh->RUrno);
					if(prlBsd != NULL)
					{
						BSDDATA rlBsd = *prlBsd;
						rmActiveBar = *prlBar;
						ogBsdData.DeSelectAll();
						pomViewer->DeSelectBsh();
						ogSdtData.DeSelectAll();
						ogSdtRosterData.DeSelectAll();
						ogSdtShiftData.DeSelectAll();
						if(rlBsd.IsSelected == TRUE)
						{
							ogBsdData.MakeBsdSelected(prlBsd, FALSE);
							prlBsh->IsSelected = false;
						}
						else
						{
							ogBsdData.MakeBsdSelected(prlBsd, TRUE);
							prlBsh->IsSelected = true;
						}
						
						if(prlBsd->IsSelected==TRUE)
						{
							pomViewer->SetMarkTime(prlBsh->From + CTimeSpan(prlBsh->DayOffSet,0,0,0), prlBsh->To + CTimeSpan(prlBsh->DayOffSet,0,0,0));
						}
						else
						{
							pomViewer->SetMarkTime(0,0);
						}

						//
						if (imGroupno == BASIC_SHIFT && pomChartWnd)
						{
							pomChartWnd->enableDisableUpdateDeleteButton(true);
						}
					}
				}
			}
			break;
		case SHIFT_DEMAND:
			{
				SDTDATA *prlSdt = NULL;
				if (pomViewer->bmShiftRoster == false)
				{
					prlSdt = ogSdtData.GetSdtByUrno(prlBar->Urno);
				}
				else
				{
					if(pomViewer->bmIsRealRoster)
					{
						prlSdt = ogSdtRosterData.GetSdtByUrno(prlBar->Urno);
					}
					else
					{
						prlSdt = ogSdtShiftData.GetSdtByUrno(prlBar->Urno);
					}

				}

				if (prlSdt != NULL)
				{
					SDTDATA rlSdt = *prlSdt;
					rmActiveBar = *prlBar;
					ogBsdData.DeSelectAll();
					pomViewer->DeSelectBsh();
					ogSdtData.DeSelectAll();
					ogSdtRosterData.DeSelectAll();
					ogSdtShiftData.DeSelectAll();
					
				
					if(pomViewer->bmShiftRoster==false)
					{
						ogSdtData.MakeSdtSelected(prlSdt, !rlSdt.IsSelected);
					}
					else
					{
						if(pomViewer->bmIsRealRoster)
						{
							ogSdtRosterData.MakeSdtSelected(prlSdt, !rlSdt.IsSelected);
						}
						else
						{
							ogSdtShiftData.MakeSdtSelected(prlSdt, !rlSdt.IsSelected);
						}

					}
					
					
					
					if(prlSdt->IsSelected== TRUE)
					{
						if(pomViewer->GetShiftRoster()==false && strcmp(rlSdt.Type,"D")==0)
						{
							pomViewer->SetMarkTime(rlSdt.Sbgi, rlSdt.Seni);
						}
						else
						{
							pomViewer->SetMarkTime(rlSdt.Esbg, rlSdt.Lsen);
						}
					}
					else
					{
						pomViewer->SetMarkTime(0,0);
					}
				}
			}
			break;
		default:
			break;
		} // end switch
	}
	else if((nFlags & MK_LBUTTON) && (nFlags & MK_SHIFT))
	{
		switch(imGroupno)
		{

/*****************
		case BASIC_SHIFT:
			{
				BASICSHIFTS *prlBsh = pomViewer->GetBasicShiftByUrno(prlBar->Urno);
				if(prlBsh != NULL)
				{
					BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(prlBsh->RUrno	);
					if(prlBsd != NULL)
					{
						rmActiveBar = *prlBar;
						if(prlBsd->IsSelected == TRUE)
						{
							ogBsdData.MakeBsdSelected(prlBsd, FALSE);
							prlBsh->IsSelected = false;
						}
						else
						{
							ogBsdData.MakeBsdSelected(prlBsd, TRUE);
							prlBsh->IsSelected = true;
						}
						pomViewer->SetMarkTime(0,0);
					}
				}
			}
			break;
********************/
		case SHIFT_DEMAND:
			{
				//if(pomViewer->bmShiftRoster==false)
				//{
				SDTDATA *prlSdt = ogSdtData.GetSdtByUrno(prlBar->Urno);
				if(prlSdt != NULL)
				{
					rmActiveBar = *prlBar;
					if(prlSdt->IsSelected == TRUE)
					{
						ogSdtData.MakeSdtSelected(prlSdt, FALSE);
					}
					else
					{
						ogSdtData.MakeSdtSelected(prlSdt, TRUE);
					}
				}
				//}
			}
			break;
		default:
			break;
		} // end switch
	}
	else if (imGroupno==SHIFT_DEMAND && !pomViewer->bmAutoCovIsActive && (nFlags & MK_LBUTTON) && (nFlags & MK_CONTROL) && BeginMovingOrResizing(point))
	{
		SDTDATA *prlSdt = ogSdtData.GetSdtByUrno(prlBar->Urno);
		if(prlSdt != NULL )
		{
			rmActiveBar = *prlBar;
			if(prlSdt->IsSelected == TRUE)
			{
				ogSdtData.MakeSdtSelected(prlSdt, FALSE);
			}
			else
			{
				ogSdtData.MakeSdtSelected(prlSdt, TRUE);					
			}
		}
		else if (pomChartWnd && pomChartWnd->IsBreakOptActive())
		{
			SDTDATA *prlSdt = ogSdtRosterData.GetSdtByUrno(prlBar->Urno);
			if (prlSdt != NULL)
			{
				rmActiveBar = *prlBar;
				if(prlSdt->IsSelected == TRUE)
				{
					ogSdtRosterData.MakeSdtSelected(prlSdt, FALSE);
				}
				else
				{
					ogSdtRosterData.MakeSdtSelected(prlSdt, TRUE);					
				}
			}
		}
	}
	// Id 1-Oct-96
	// This will help us create a virtual double click on a double click after SetCursorPos().
	omLastClickedPosition = point;
	CCS_CATCH_ALL
}

void CoverageGantt::OnLButtonUp(UINT nFlags, CPoint point) 
{
   CCS_TRY
    if (umResizeMode != HTNOWHERE)  // in SetCapture()?
    {
		// This allow a fast way for terminating moving or resizing mode
		if(imGroupno == SHIFT_DEMAND)
		{
			if(bmMovingDyna!=GANTT_NONE)
			{
				OnMovingOrResizing(point, nFlags & MK_LBUTTON);
				SDTDATA *prlSdt = ogSdtData.GetSdtByUrno(rmActiveBar.Urno);
				if (prlSdt == NULL)
				{
					prlSdt = ogSdtRosterData.GetSdtByUrno(rmActiveBar.Urno);
					if (prlSdt != NULL)
					{
						ProcessMarkBar((void*)prlSdt);
						ogSdtRosterData.MakeSdtSelected(prlSdt, FALSE);
					}
				}
				else
				{
					ProcessMarkBar((void*)prlSdt);
					ogSdtData.MakeSdtSelected(prlSdt, FALSE);
				}
			}
		}//groupno == BASIC_SHIFT
	}
	CListBox::OnLButtonUp(nFlags, point);
	CCS_CATCH_ALL
}

void CoverageGantt::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
   CCS_TRY
	TRACE("CoverageGantt::OnLButtonDblClk\n");

   // find click bar no
    int itemID = GetItemFromPoint(point);
    int ilBarno = GetBarnoFromPoint(itemID, point);
    imCurrentBar = GetBarnoFromPoint(itemID, point);
	if(ilBarno==-1 || itemID ==-1)
	{
		ogBsdData.DeSelectAll();
		pomViewer->DeSelectBsh();
		ogSdtData.DeSelectAll();
		ogSdtRosterData.DeSelectAll();
		ogSdtShiftData.DeSelectAll();
		pomViewer->SetMarkTime(0,0);
		return;
	}
	// find the bar
	COV_BARDATA *prlBar;
	prlBar = pomViewer->GetBar( imGroupno, itemID, ilBarno);
	if(prlBar==NULL)
	{
		ogBsdData.DeSelectAll();
		pomViewer->DeSelectBsh();
		ogSdtData.DeSelectAll();
		ogSdtRosterData.DeSelectAll();
		ogSdtShiftData.DeSelectAll();
		pomViewer->SetMarkTime(0,0);
		return;
	}

	// check which chart
	if(nFlags == MK_LBUTTON)
	{
		switch(imGroupno)
		{
		case BASIC_SHIFT:
			{
				BASICSHIFTS *prlBsh = pomViewer->GetBasicShiftByUrno(prlBar->Urno);
				if(prlBsh != NULL)
				{
					BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(prlBsh->RUrno);
					if(prlBsd != NULL)
					{
						BSDDATA rlBsd = *prlBsd;
						rmActiveBar = *prlBar;
						ogBsdData.DeSelectAll();
						pomViewer->DeSelectBsh();
						ogSdtData.DeSelectAll();
						ogSdtRosterData.DeSelectAll();
						ogSdtShiftData.DeSelectAll();
						if(rlBsd.IsSelected == TRUE)
						{
							ogBsdData.MakeBsdSelected(prlBsd, FALSE);
							prlBsh->IsSelected = false;
						}
						else
						{
							ogBsdData.MakeBsdSelected(prlBsd, TRUE);
							prlBsh->IsSelected = true;
						}
						
						if(prlBsd->IsSelected==TRUE)
						{
							pomViewer->SetMarkTime(prlBsh->From + CTimeSpan(prlBsh->DayOffSet,0,0,0), prlBsh->To + CTimeSpan(prlBsh->DayOffSet,0,0,0));
						}
						else
						{
							pomViewer->SetMarkTime(0,0);
						}

						// show sth
						//
						if(ogBasicData.bmShowInsertUpdateDeleteButton == true)
						{
							GetParent() -> GetParent() -> GetParent()->SendMessage(WM_AD_BUTTON, 0, 1L);

							BasisschichtenDlg *polBsdDlg = new BasisschichtenDlg(&rlBsd,true,this);
							polBsdDlg->m_Caption = LoadStg(IDS_STRING61559);
							if (polBsdDlg->DoModal() == IDOK)
							{
								TRACE("CoverageGantt::OnLButtonDblClk BASIC_SHIFT \n");
								//if (IDYES == MessageBox(LoadStg(IDS_STRING924),LoadStg(IDS_STRING154),(MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2)))
								{
									rlBsd.Lstu = CTime::GetCurrentTime();
									strcpy(rlBsd.Useu,pcgUser);
									AfxGetApp()->DoWaitCursor(1);
									if(ogBsdData.Update(&rlBsd)==false)
									{
										CString omErrorTxt;
										CString omUpdateErrTxt = LoadStg(ST_INSERTERR);
										omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogBsdData.imLastReturnCode, ogBsdData.omLastErrorMessage);
										MessageBox(omErrorTxt,LoadStg(IDS_STRING61553),MB_ICONERROR);
									}	
									AfxGetApp()->DoWaitCursor(-1);
								}
							}
						}
					}
				}
			}
			break;
		default:
			break;
		} // end switch
	}
	omLastClickedPosition = point;

//    int itemID = GetItemFromPoint(point);
//    if ((point.x < imVerticalScaleWidth - 2) && itemID != -1)	// in VerticalScale?
//	{
//	}
//	// Pop-up detailed window
//	if(imGroupno == BASIC_SHIFT)
//	{
//		if (itemID != -1)
//		{
//			int ilBarno = GetBarnoFromPoint(itemID, point);
//			if(ilBarno != -1)
//			{
//			}
//		}
//	}
	CCS_CATCH_ALL
}

void CoverageGantt::OnRButtonDown(UINT nFlags, CPoint point) 
{
   CCS_TRY
	int itemID, ilBarno;
	if ((itemID = GetItemFromPoint(point)) == -1)
		return;		// not click in the Gantt window
	if(imGroupno == SHIFT_DEMAND && pomViewer->bmShiftRoster==false && !pomViewer->bmAutoCovIsActive)
	{

		COV_LINEDATA *prlLine = pomViewer->GetLine(SHIFT_DEMAND, itemID);
		if(itemID != -1)
		{
			if ((ilBarno = GetBarnoFromPoint(itemID, point)) != -1)	// on a bar?
			{
			   COV_BARDATA *prlBar = pomViewer->GetBar(SHIFT_DEMAND, itemID, ilBarno);
				if(ilBarno != -1 && prlBar != NULL)
				{
					//bmContextBarSet = TRUE;	// Save bar data for handling after context menu closed
					//rmContextBar = *prlBar;	// copy all data of this bar

					SDTDATA *prlSdt = ogSdtData.GetSdtByUrno(prlBar->Urno);
					if(prlSdt != NULL)
					{
						 rmActiveBar = *prlBar;
						 ogSdtData.MakeSdtSelected(prlSdt, TRUE);
						 ogBsdData.DeSelectAll();
						 CMenu menu;
						 menu.CreatePopupMenu();
						 for (int i = menu.GetMenuItemCount(); --i >= 0;)
						 {
							  menu.RemoveMenu(i, MF_BYPOSITION);
						 }
						 CString olText;
						 for(i=0;i<4;i++)
						 {
							 if(pomViewer->GetMaxView()==true)
							 {
								 switch(i)
								 {
								 case 0:
									 olText = LoadStg(SHIFT_B_COPY);
									 menu.AppendMenu(MF_STRING,44, olText);	// copy shift
									 break;
								 case 1:
									 //olText.LoadStg(COV_SDT_DELETE);
									 olText = LoadStg(SHIFT_B_DELETE);
									 menu.AppendMenu(MF_STRING,41, olText);	// delete shift
									 break;
								 case 2:
									 if (ogBasicData.IsDebugMode())
									 {
										menu.AppendMenu(MF_STRING,46,"Debug Info");
										if (ogBasicData.DebugAutoCoverage())
											menu.AppendMenu(MF_STRING,47,"Debug Shiftgroup Coverage");
									 }
									 break;
								 default:
									 break;
								 }
							 }
							 else
							 {
								 switch(i)
								 {
								 case 1:
									 menu.AppendMenu(MF_STRING,43, LoadStg(IDS_STRING61362));	// (insert) duplicate (specify no. of) shifts
									 break;
								 case 0:
									 menu.AppendMenu(MF_STRING,45, LoadStg(IDS_STRING1687));	// (number) increase or decrease the the number of compressed shifts.
									 break;
								 case 2:
									 if (ogBasicData.IsDebugMode())
									 {
										menu.AppendMenu(MF_STRING,46,"Debug Info");
										if (ogBasicData.DebugAutoCoverage())
											menu.AppendMenu(MF_STRING,47,"Debug Shiftgroup Coverage");
									 }
									 break;
								 default:
									 break;
								 }
							 }
						 }
						 ClientToScreen(&point);
						 menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
					 }
				}//if(ilBarno != -1 && prlBar != NULL)
			}//if ((ilBarno = GetBarnoFromPoint(itemID, point)) != -1)	// on a bar?
		}//if(itemID != -1)
	}
	else if (imGroupno == BASIC_SHIFT)
	{
		COV_LINEDATA *prlLine = pomViewer->GetLine(BASIC_SHIFT, itemID);
		if(itemID != -1)
		{
			if ((ilBarno = GetBarnoFromPoint(itemID, point)) != -1)	// on a bar?
			{
			   COV_BARDATA *prlBar = pomViewer->GetBar(BASIC_SHIFT, itemID, ilBarno);
				if(ilBarno != -1 && prlBar != NULL)
				{
					BASICSHIFTS *prlBsh = pomViewer->GetBasicShiftByUrno(prlBar->Urno);
					if(prlBsh != NULL)
					{
						BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(prlBsh->RUrno	);
					
						if(prlBsd != NULL)
						{
							 rmActiveBar = *prlBar;
							 ogBsdData.MakeBsdSelected(prlBsd, TRUE);

							 ogSdtData.DeSelectAll();
							 CMenu menu;
							 menu.CreatePopupMenu();
							 for (int i = menu.GetMenuItemCount(); --i >= 0;)
							 {
								  menu.RemoveMenu(i, MF_BYPOSITION);
							 }
							 CString olText;
							 for(i = 0;i < 6;i++)
							 {
								 switch(i)
								 {
								 case 0:
									 olText = LoadStg(IDS_STRING567);
//									 menu.AppendMenu(MF_STRING,40, olText);	// create new shift
									 break;
								 case 1:
									 olText = LoadStg(IDS_STRING568);
//									 menu.AppendMenu(MF_STRING,42, olText);	// change shift
									 break;
								 case 2:
									 olText = LoadStg(IDS_STRING1686);
//									 menu.AppendMenu(MF_STRING,43, olText);	// duplicate shift
									 break;
								 case 3:
									 olText = LoadStg(SHIFT_B_COPY);
									 if(pomViewer->GetShiftRoster()==true)
									 {
										 menu.AppendMenu(MF_STRING|MF_GRAYED,44, olText);	// copy shift
									 }
									 else
									 {
										 menu.AppendMenu(MF_STRING,44, olText);	// copy shift
									 }
									 break;
								 case 4:
									 olText = LoadStg(SHIFT_B_DELETE);
//									 menu.AppendMenu(MF_STRING,41, olText);	// delete shift
									 break;
								 case 5:
									 if (ogBasicData.IsDebugMode())
									 {
										menu.AppendMenu(MF_STRING,46,"Debug Info");
										menu.AppendMenu(MF_STRING,47,"Debug Shiftgroup Coverage");
									 }
									 break;
								 default:
									 break;
								 }
							 }
							 ClientToScreen(&point);
							 menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
						}
					 }
				}//if(ilBarno != -1 && prlBar != NULL)
			}//if ((ilBarno = GetBarnoFromPoint(itemID, point)) != -1)	// on a bar?
		}//if(itemID != -1)
	}


	CWnd::OnRButtonDown(nFlags, point);
	CCS_CATCH_ALL
}

/////////////////////////////////////////////////////////////////////////////
// CoverageGantt context menus helper functions

	
/////////////////////////////////////////////////////////////////////////////
// CoverageGantt message handlers helper functions

// Update every necessary GUI elements for each mouse/timer event
void CoverageGantt::UpdateGanttChart(CPoint point, BOOL bpIsControlKeyDown)
{
   CCS_TRY
   int itemID = GetItemFromPoint(point);
   UINT blHitTest = HTNOWHERE;

    // Check if we have to change the highlight line in VerticalScale
	CPoint ptScreen = point;
	ClientToScreen(&ptScreen);
	
	
	if (WindowFromPoint(ptScreen) == this)	// moving inside GanttChart?
    {
        if (!bmIsMouseInWindow) // first time?
        {
            bmIsMouseInWindow = TRUE;
            //((CButtonListDialog *)AfxGetMainWnd())->RegisterTimer(this);    // install the timer
        }
        if (itemID != imHighlightLine)  // move to new bar?
            RepaintVerticalScale(itemID, FALSE);
        if (IsBetween(point.x, 0, imVerticalScaleWidth-1))
		{
            UpdateBarStatus(itemID, -1);    // moving on vertical scale, not a bar
		}
		else
        {
            int ilBarno;
            if (!bpIsControlKeyDown)
                ilBarno = GetBarnoFromPoint(itemID, point);
            else
                ilBarno = GetBarnoFromPoint(itemID, point, imBorderPreLeft, imBorderPreRight);
            UpdateBarStatus(itemID, ilBarno);
        }
    }
    else if (bmIsMouseInWindow) // moving out of GanttChart? (first time only)
    {
        bmIsMouseInWindow = FALSE;
        RepaintVerticalScale(imHighlightLine, FALSE);
        imHighlightLine = -1;       // no more selected line
        UpdateBarStatus(-1, -1);    // no more status if mouse is outside of GanttChart
    }

    // Check if we have to change the icon (when user want to move/resize a bar)
    if (bmIsControlKeyDown && !bpIsControlKeyDown)              // no more Ctrl-key?
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
    else if (!bmIsMouseInWindow || !bpIsControlKeyDown || imCurrentBar == -1)
        ;   // do noting
    else if ((blHitTest = HitTest(itemID, imCurrentBar, point)) == HTCAPTION) // body?
	 {
        int ilBarno = GetBarnoFromPoint(itemID, point);
		if (ilBarno == -1)
			;
		else
		{
		  COV_BARDATA *prlBar;
		  prlBar = pomViewer->GetBar(imGroupno, itemID, ilBarno);
		  SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
		}
	 }
    else if(blHitTest!=HTNOWHERE)
	{
		// left/right border?
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));
	}

    // Update control key state
    bmIsControlKeyDown = bpIsControlKeyDown;
	CCS_CATCH_ALL
}


// Update the frame window's status bar
void CoverageGantt::UpdateBarStatus(int ipLineno, int ipBarno)
{
   CCS_TRY
    CString s="";

    if (ipLineno == imHighlightLine && ipBarno == imCurrentBar)	// user still be on the old bar?
		return;

    /*
	if (ipLineno == -1 )	// there is no more bar status?
        s = "";
    else if (ipBarno == -1)	// user move outside the bar?
        s = "";
	*/
	if (ipLineno != -1 && ipBarno!=-1)
	{
		s = pomViewer->GetBarText(imGroupno, ipLineno, ipBarno) + CString(" ");
		COV_BARDATA *prlBar = pomViewer->GetBar(imGroupno, ipLineno, ipBarno);
		if(prlBar->Type == BASIC_SHIFT)
		{
			BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(prlBar->RUrno);
			if(prlBsd!=NULL)
			{
				switch(prlBsd->Type[0])
				{
				case 'S':
					s += CString("(") + LoadStg(IDS_STRING1683)+CString(")");
					break;
				case 'D':
					s += CString("(")+LoadStg(IDS_STRING1684)+CString(")");
					break;
				case 'E':
					s += CString("(")+LoadStg(IDS_STRING1682)+CString(")");
					break;
				default:
					break;
				}
				s  += prlBar->StartTime.Format(" %H:%M - ") + prlBar->EndTime.Format("%H:%M");
			}
		}
		else if(prlBar->Type == SHIFT_DEMAND)
		{
			SDTDATA *prlSdt = ogSdtData.GetSdtByUrno(prlBar->Urno);
			if(prlSdt==NULL)
			{
				prlSdt = ogSdtRosterData.GetSdtByUrno(prlBar->Urno);
			}
			if(prlSdt==NULL)
			{
				prlSdt = ogSdtShiftData.GetSdtByUrno(prlBar->Urno);
			}
			if(prlSdt!=NULL)
			{
				switch(prlSdt->Type[0])
				{
				case 'S':
					s += CString("(") + LoadStg(IDS_STRING1683)+CString(")") + prlBar->StartTime.Format(" %H:%M - ") + prlBar->EndTime.Format("%H:%M");
					break;
				case 'D':
					s += CString("(")+LoadStg(IDS_STRING1684)+CString(")") + prlSdt->Sbgi.Format(" %H:%M - ") + prlSdt->Seni.Format("%H:%M");;
					break;
				case 'E':
					s += CString("(")+LoadStg(IDS_STRING1682)+CString(")") + prlBar->StartTime.Format(" %H:%M - ") + prlBar->EndTime.Format("%H:%M");;
					break;
				default:
					s += CString("-") + prlBar->StartTime.Format(" %H:%M - ") + prlBar->EndTime.Format("%H:%M");
					break;
				}
				s  += prlBar->StatusText;				
			}
		}
		
	}

	// display status bar
    if (pomStatusBar != NULL)
        pomStatusBar->SetPaneText(0, (LPCSTR)s);

	// display top scale indicator
	if (pomTimeScale != NULL)
	{
		if (ipLineno == -1 ||	// mouse moved out of every lines?
			(ipBarno == -1 && imCurrentBar != -1))	// mouse just leave a bar?
			pomTimeScale->RemoveAllTopScaleIndicator();
		else if ((ipLineno!=-1 && ipBarno!=-1) && (imActiveLine!=ipLineno || ipBarno != imCurrentBar))
		{
			pomTimeScale->RemoveAllTopScaleIndicator();
			int ilIndicatorCount = pomViewer->GetIndicatorCount(imGroupno, ipLineno, ipBarno);
			for (int ilIndicatorno = 0; ilIndicatorno < ilIndicatorCount; ilIndicatorno++)
			{
				COV_INDICATORDATA *prlIndicator = pomViewer->GetIndicator(imGroupno, ipLineno, ipBarno, ilIndicatorno);
				pomTimeScale->AddTopScaleIndicator(prlIndicator->StartTime,
					prlIndicator->EndTime, prlIndicator->Color);
			}
			pomTimeScale->DisplayTopScaleIndicator();
		}
	}

    // remember the bar and update the status bar (if exist)
    imCurrentBar = ipBarno;
	imActiveLine = ipLineno;
	CCS_CATCH_ALL
}

// Start moving/resizing
BOOL CoverageGantt::BeginMovingOrResizing(CPoint point)
{
CCS_TRY
	CTime time1 = GetCTime(point.x-imBorderPreLeft);
    CTime time2 = GetCTime(point.x+imBorderPreRight);   
	if (imGroupno != SHIFT_DEMAND)
		return FALSE;

    int itemID = GetItemFromPoint(point);
    int ilBarno = GetBarnoFromPoint(itemID, point);
    //int ilBarno = GetBarnoFromPoint(itemID, point, imBorderPreLeft, imBorderPreRight);
    UpdateBarStatus(itemID, ilBarno);

    if (ilBarno != -1)
        umResizeMode = HitTest(itemID, ilBarno, point);

    if (umResizeMode != HTNOWHERE)
    {
        COV_BARDATA *prlBar = pomViewer->GetBar(imGroupno, itemID, ilBarno);
		if(prlBar!=NULL)
		{
			SDTDATA *prlSdt = ogSdtData.GetSdtByUrno(prlBar->Urno);
			if(prlSdt != NULL)				
			{
				switch (umResizeMode)
				{
				case HTCAPTION: // moving mode
					if(IsBetween(GetCTime(point.x),prlSdt->Brkf,prlSdt->Brkt))
					{
						bmMovingDyna = GANTT_BREAK;
					}
					else if(IsBetween(GetCTime(point.x),prlSdt->Bkf1,prlSdt->Bkt1))
					{
						bmMovingDyna = GANTT_BREAK_PERIOD;
					}
					else if(strcmp(prlSdt->Type,"D")==0 && IsBetween(GetCTime(point.x),prlSdt->Sbgi,prlSdt->Seni))
					{
						bmMovingDyna = GANTT_SHIFT;
					}
					else
					{
						bmMovingDyna = GANTT_NONE;
						return FALSE;
					}
					break;
				case HTLEFT:   // resize left border
					if(strcmp(prlSdt->Type,"D")==0 && IsBetween(GetCTime(point.x),GetCTime(GetX(prlSdt->Sbgi)-imBorderPreLeft),GetCTime(GetX(prlSdt->Sbgi)+imBorderPreRight)))
					{
						bmMovingDyna = GANTT_SHIFT;
					}
					else if(IsBetween(GetCTime(point.x),GetCTime(GetX(prlSdt->Bkf1)-imBorderPreLeft),GetCTime(GetX(prlSdt->Bkf1)+imBorderPreRight)))
					{
						bmMovingDyna = GANTT_BREAK_PERIOD;
					}
					else if(IsBetween(GetCTime(point.x),GetCTime(GetX(prlSdt->Brkf)-imBorderPreLeft),GetCTime(GetX(prlSdt->Brkf)+imBorderPreRight)))
					{
						bmMovingDyna = GANTT_BREAK;
					}
					else
					{
						bmMovingDyna = GANTT_NONE;
						return FALSE;
					}
					break;
				case HTRIGHT:
					if(strcmp(prlSdt->Type,"D")==0 && IsBetween(GetCTime(point.x),GetCTime(GetX(prlSdt->Seni)-imBorderPreLeft),GetCTime(GetX(prlSdt->Seni)+imBorderPreRight)))
					{
						bmMovingDyna = GANTT_SHIFT;
					}
					else if(IsBetween(GetCTime(point.x),GetCTime(GetX(prlSdt->Bkt1)-imBorderPreLeft),GetCTime(GetX(prlSdt->Bkt1)+imBorderPreRight)))
					{
						bmMovingDyna = GANTT_BREAK_PERIOD;
					}
					else if(IsBetween(GetCTime(point.x),GetCTime(GetX(prlSdt->Brkt)-imBorderPreLeft),GetCTime(GetX(prlSdt->Brkt)+imBorderPreRight)))
					{
						bmMovingDyna = GANTT_BREAK;
					}
					else
					{
						bmMovingDyna = GANTT_NONE;
						return FALSE;
					}
					break;
				default:
					bmMovingDyna = GANTT_NONE;
					return FALSE;
					break;
				}
				
				if (umResizeMode == HTCAPTION)//|| umResizeMode == HTRIGHT)
				{
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZE));
				}
				else
				{
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));
				}
			}
			else if (this->pomChartWnd && this->pomChartWnd->IsBreakOptActive())
			{
				SDTDATA *prlSdt = ogSdtRosterData.GetSdtByUrno(prlBar->Urno);
				if(prlSdt != NULL)				
				{
					switch (umResizeMode)
					{
					case HTCAPTION: // moving mode
						if(IsBetween(GetCTime(point.x),prlSdt->Brkf,prlSdt->Brkt))
						{
							if (prlSdt->StatPlSt[0] == 'A')	// ROSS == 'active' ?
								bmMovingDyna = GANTT_BREAK;
							else
							{
								bmMovingDyna = GANTT_NONE;
								return FALSE;
							}
						}
						else if(IsBetween(GetCTime(point.x),prlSdt->Bkf1,prlSdt->Bkt1))
						{
							bmMovingDyna = GANTT_BREAK_PERIOD;
						}
						else if(strcmp(prlSdt->Type,"D")==0 && IsBetween(GetCTime(point.x),prlSdt->Sbgi,prlSdt->Seni))
						{
							bmMovingDyna = GANTT_SHIFT;
						}
						else
						{
							bmMovingDyna = GANTT_NONE;
							return FALSE;
						}
						break;
					case HTLEFT:   // resize left border
						if(strcmp(prlSdt->Type,"D")==0 && IsBetween(GetCTime(point.x),GetCTime(GetX(prlSdt->Sbgi)-imBorderPreLeft),GetCTime(GetX(prlSdt->Sbgi)+imBorderPreRight)))
						{
							bmMovingDyna = GANTT_SHIFT;
						}
						else if(IsBetween(GetCTime(point.x),GetCTime(GetX(prlSdt->Bkf1)-imBorderPreLeft),GetCTime(GetX(prlSdt->Bkf1)+imBorderPreRight)))
						{
							bmMovingDyna = GANTT_BREAK_PERIOD;
						}
						else if(IsBetween(GetCTime(point.x),GetCTime(GetX(prlSdt->Brkf)-imBorderPreLeft),GetCTime(GetX(prlSdt->Brkf)+imBorderPreRight)))
						{
							if (prlSdt->StatPlSt[0] == 'A')	// ROSS == 'active' ?
								bmMovingDyna = GANTT_BREAK;
							else
							{
								bmMovingDyna = GANTT_NONE;
								return FALSE;
							}
						}
						else
						{
							bmMovingDyna = GANTT_NONE;
							return FALSE;
						}
						break;
					case HTRIGHT:
						if(strcmp(prlSdt->Type,"D")==0 && IsBetween(GetCTime(point.x),GetCTime(GetX(prlSdt->Seni)-imBorderPreLeft),GetCTime(GetX(prlSdt->Seni)+imBorderPreRight)))
						{
							bmMovingDyna = GANTT_SHIFT;
						}
						else if(IsBetween(GetCTime(point.x),GetCTime(GetX(prlSdt->Bkt1)-imBorderPreLeft),GetCTime(GetX(prlSdt->Bkt1)+imBorderPreRight)))
						{
							bmMovingDyna = GANTT_BREAK_PERIOD;
						}
						else if(IsBetween(GetCTime(point.x),GetCTime(GetX(prlSdt->Brkt)-imBorderPreLeft),GetCTime(GetX(prlSdt->Brkt)+imBorderPreRight)))
						{
							if (prlSdt->StatPlSt[0] == 'A')	// ROSS == 'active' ?
								bmMovingDyna = GANTT_BREAK;
							else
							{
								bmMovingDyna = GANTT_NONE;
								return FALSE;
							}
						}
						else
						{
							bmMovingDyna = GANTT_NONE;
							return FALSE;
						}
						break;
					default:
						bmMovingDyna = GANTT_NONE;
						return FALSE;
						break;
					}
					
					if (umResizeMode == HTCAPTION)//|| umResizeMode == HTRIGHT)
					{
						SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZE));
					}
					else
					{
						SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));
					}
				}
				else
				{
					return FALSE;
				}
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}

		SetCapture();
        CRect rcItem;
        GetItemRect(itemID, &rcItem);
        int left = (int)GetX(prlBar->StartTime);
        int right = (int)GetX(prlBar->EndTime);
        int top = rcItem.top /*+ imGutterHeight*/ + (prlBar->OverlapLevel * imOverlapHeight);
        int bottom = top + imBarHeight;
		bmActiveBarSet = TRUE;  // Save bar data for moving and sizing
		//rmActiveBar = *prlBar;	// copy all data of this bar
		lmCurrentSelectedBarUrno = prlBar->Urno;
		prmPreviousBar = prlBar;
        omPointResize = point;

		SDTDATA *prlSdt = ogSdtData.GetSdtByUrno(prlBar->Urno);	
		if (prlSdt == NULL && this->pomChartWnd && this->pomChartWnd->IsBreakOptActive())
		{
			prlSdt = ogSdtRosterData.GetSdtByUrno(prlBar->Urno);	
		}

        switch(bmMovingDyna)
		{
			case GANTT_BREAK_PERIOD:
			left = (int)GetX(prlSdt->Bkf1);
			right = (int)GetX(prlSdt->Bkt1);
			omRectResize = CRect(left, top, right, bottom);
			break;
		case GANTT_SHIFT:
			left = (int)GetX(prlSdt->Sbgi);
			right = (int)GetX(prlSdt->Seni);
			omRectResize = CRect(left, top, right, bottom);
			break;
		case GANTT_BREAK:
			left = (int)GetX(prlSdt->Brkf);
			right = (int)GetX(prlSdt->Brkt);
			omRectResize = CRect(left, top, right, bottom);
			break;
		default:
			break;
		}
		CTime olS = GetCTime(omRectResize.left);
		CTime olE = GetCTime(omRectResize.right);
		CString ol1 = olS.Format("%d.%m.%Y-%H:%M");
		CString ol2 = olE.Format("%d.%m.%Y-%H:%M");
        CClientDC dc(this);
        DrawFocusRect(&dc, &omRectResize);	// first time focus rectangle
        DrawTimeLines(&dc, omRectResize.top, omRectResize.bottom);
    }

    return (umResizeMode != HTNOWHERE);
	CCS_CATCH_ALL
	return FALSE;
}

// Update the resizing/moving rectangle
BOOL CoverageGantt::OnMovingOrResizing(CPoint point, BOOL bpIsLButtonDown)
{
CCS_TRY
	CClientDC dc(this);
	static int ilCounter = 0;
	CTime olCurTime = GetCTime(point.x);
	CTime olStartTime,olEndTime;

	SDTDATA *prlSdt = ogSdtData.GetSdtByUrno(rmActiveBar.Urno);
	if (prlSdt == NULL && this->pomChartWnd && this->pomChartWnd->IsBreakOptActive())
	{
		prlSdt = ogSdtRosterData.GetSdtByUrno(rmActiveBar.Urno);
	}

	if (prlSdt == NULL)
	{
		return FALSE;
	}
	
	if (bpIsLButtonDown)    // still resizing/moving?
	{
		DrawFocusRect(&dc, &omRectResize);	// delete previous focus rectangle
		switch (umResizeMode)
		{
		case HTCAPTION: // moving mode
			if(bmMovingDyna==GANTT_BREAK)
			{
				int ilOffset = point.x - omPointResize.x;
				int ilWidth = omRectResize.right - omRectResize.left;
				if(GetCTime(omRectResize.left+ilOffset)>=prlSdt->Bkf1 && GetCTime(omRectResize.right+ilOffset)<=prlSdt->Bkt1)
				{
					omRectResize.OffsetRect(ilOffset, 0);
				}
				else
				{
					int ilXBkf1	 = GetX(prlSdt->Bkf1);
					int ilXBkt1	 = GetX(prlSdt->Bkt1);
					
					if((omRectResize.left+ilOffset)<ilXBkf1)
					{
						omRectResize.left = ilXBkf1;
						omRectResize.right = omRectResize.left+ilWidth;
					}
					else if((omRectResize.left+ilOffset)>ilXBkt1)
					{
						omRectResize.right= ilXBkt1;
						omRectResize.left = omRectResize.right-ilWidth;
					}
				}
			}
			else if(bmMovingDyna==GANTT_BREAK_PERIOD)
			{
				if(strcmp(prlSdt->Type,"S")==0)
				{
					olStartTime = prlSdt->Esbg;
					olEndTime   = prlSdt->Lsen;
				}
				else if(strcmp(prlSdt->Type,"D")==0)
				{
					olStartTime = prlSdt->Sbgi;
					olEndTime   = prlSdt->Seni;
				}
				
				int ilOffset = point.x - omPointResize.x;
				int ilWidth = omRectResize.right - omRectResize.left;
				if(IsBetween(GetCTime(omRectResize.left+ilOffset),olStartTime,prlSdt->Brkf) && IsBetween(GetCTime(omRectResize.right+ilOffset),prlSdt->Brkt,olEndTime))
				{
					omRectResize.OffsetRect(ilOffset, 0);
				}
				else
				{
					int ilXStart = GetX(olStartTime);
					int ilXEnd   = GetX(olEndTime);
					int ilXBrkf	 = GetX(prlSdt->Brkf);
					int ilXBrkt	 = GetX(prlSdt->Brkt);
					bool blChg = false;
					//TRACE("\n%d BEFORE left=%d right=%d XStart = %d XEnd = %d Brkf=%d Brkt=%d Offset=%d",ilCounter,omRectResize.left,omRectResize.right,ilXStart,ilXEnd,ilXBrkf,ilXBrkt,ilOffset);
					if((omRectResize.left+ilOffset) < ilXStart)
					{
						blChg = true;
						omRectResize.left = ilXStart;
						omRectResize.right = omRectResize.left+ilWidth;
						//TRACE("\n%d LEFT OUT left=%d right=%d",ilCounter,omRectResize.left,omRectResize.right);
					}
					else if((omRectResize.left+ilOffset)> ilXBrkf)
					{
						blChg = true;
						omRectResize.left = ilXBrkf;
						omRectResize.right = omRectResize.left+ilWidth;
						//TRACE("\n%d LEFT IN left=%d right=%d",ilCounter,omRectResize.left,omRectResize.right);
					}

					if(blChg)
					{
						if(omRectResize.right<ilXBrkt)
						{
							omRectResize.right= ilXBrkt;
							omRectResize.left = omRectResize.right-ilWidth;
							//TRACE("\n%d RIGHT IN left=%d right=%d",ilCounter,omRectResize.left,omRectResize.right);
						}
						else if(omRectResize.right>ilXEnd)
						{
							omRectResize.right= ilXEnd;
							omRectResize.left = omRectResize.right-ilWidth;
							//TRACE("\n%d RIGHT OUT left=%d right=%d",ilCounter,omRectResize.left,omRectResize.right);
						}
					}
					else
					{
						if((omRectResize.right+ilOffset)<ilXBrkt)
						{
							omRectResize.right= ilXBrkt;
							omRectResize.left = omRectResize.right-ilWidth;
							//TRACE("\n%d RIGHT IN left=%d right=%d",ilCounter,omRectResize.left,omRectResize.right);
						}
						else if((omRectResize.right+ilOffset)>ilXEnd)
						{
							omRectResize.right= ilXEnd;
							omRectResize.left = omRectResize.right-ilWidth;
							//TRACE("\n%d RIGHT OUT left=%d right=%d",ilCounter,omRectResize.left,omRectResize.right);
						}
					}
					ilCounter++;
				}
			}
			else if(bmMovingDyna==GANTT_SHIFT)
			{
				int ilOffset = point.x - omPointResize.x;
				int ilWidth = omRectResize.right - omRectResize.left;
				if(IsBetween(GetCTime(omRectResize.left+ilOffset),prlSdt->Esbg,prlSdt->Bkf1) && IsBetween(GetCTime(omRectResize.right+ilOffset),prlSdt->Bkt1,prlSdt->Lsen))
				{
					omRectResize.OffsetRect(ilOffset, 0);
				}
				else
				{
					int ilXStart = GetX(prlSdt->Esbg);
					int ilXEnd   = GetX(prlSdt->Lsen);
					int ilXBkf1	 = GetX(prlSdt->Bkf1);
					int ilXBkt1	 = GetX(prlSdt->Bkt1);
					bool blChg =false;
					if((omRectResize.left+ilOffset)<ilXStart)
					{
						blChg = true;
						omRectResize.left = ilXStart;
						omRectResize.right = omRectResize.left+ilWidth;
					}
					else if((omRectResize.left+ilOffset)>ilXBkf1)
					{
						blChg = true;
						omRectResize.left = ilXBkf1;
						omRectResize.right = omRectResize.left+ilWidth;
					}

					if(blChg)
					{
						if((omRectResize.right)<ilXBkt1)
						{
							omRectResize.right= ilXBkt1;
							omRectResize.left = omRectResize.right-ilWidth;
						}
						else if((omRectResize.right)>ilXEnd)
						{
							omRectResize.right= ilXEnd;
							omRectResize.left = omRectResize.right-ilWidth;
						}
					}
					else
					{
						if((omRectResize.right+ilOffset)<ilXBkt1)
						{
							omRectResize.right= GetX(prlSdt->Bkt1);
							omRectResize.left = omRectResize.right-ilWidth;
						}
						else if((omRectResize.right+ilOffset)>ilXEnd)
						{
							omRectResize.right= ilXEnd;
							omRectResize.left = omRectResize.right-ilWidth;
						}
					}
				}
			}
			break;
		case HTLEFT:    // resize left border
			if(bmMovingDyna==GANTT_BREAK)
			{
				if(IsBetween(olCurTime,prlSdt->Bkf1,prlSdt->Brkt))
				{
					omRectResize.left = point.x;
				}
				else if(olCurTime<prlSdt->Bkf1)
				{
					omRectResize.left = GetX(prlSdt->Bkf1);
				}
				else
				{
					omRectResize.left = GetX(prlSdt->Brkt);
				}
			}
			else if(bmMovingDyna==GANTT_BREAK_PERIOD)
			{
				if(strcmp(prlSdt->Type,"S")==0)
				{
					olStartTime = prlSdt->Esbg;
					olEndTime   = prlSdt->Lsen;
				}
				else if(strcmp(prlSdt->Type,"D")==0)
				{
					olStartTime = prlSdt->Sbgi;
					olEndTime   = prlSdt->Seni;
				}
				if(IsBetween(olCurTime,olStartTime,prlSdt->Brkf)==true)
				{
					omRectResize.left = point.x;
				}
				else if(olCurTime < olStartTime)
				{
					omRectResize.left = GetX(olStartTime);
				}					
				else
				{
					omRectResize.left = GetX(prlSdt->Brkf);
				}
			}
			else if(bmMovingDyna==GANTT_SHIFT)
			{
				int ilExt		= prlSdt->Sex1;
				int ilMaxExt	= prlSdt->Sdu1 + prlSdt->Sex1;
				int ilXBkf1		= GetX(prlSdt->Bkf1);
				int ilXEsbg		= GetX(prlSdt->Esbg);
				int ilMinExt	= prlSdt->Sdu1 - prlSdt->Ssh1;
				int ilXMax		= GetX(GetCTime(omRectResize.right) - CTimeSpan(0,int(ilMaxExt/60),int(ilMaxExt%60),0));
				int ilXMin		= GetX(GetCTime(omRectResize.right) - CTimeSpan(0,int(ilMinExt/60),int(ilMinExt%60),0));

				if(IsBetween(point.x,ilXEsbg,ilXBkf1) && IsBetween(point.x,ilXMax,ilXMin))
				{
					omRectResize.left = point.x;
				}
				else if(point.x > ilXBkf1 || point.x > ilXMin) 
				{
					omRectResize.left = min(GetX(prlSdt->Bkf1),ilXMin);
				}
				else
				{
					omRectResize.left = max(GetX(prlSdt->Esbg),ilXMax);
				}
			}
			break;
		case HTRIGHT:   // resize right border
			if(bmMovingDyna==GANTT_BREAK)
			{
				if(IsBetween(olCurTime,prlSdt->Brkf,prlSdt->Bkt1))
				{
					omRectResize.right = point.x;
				}
				else if(olCurTime>prlSdt->Bkt1)
				{
					omRectResize.right = GetX(prlSdt->Bkt1);
				}
				else
				{
					omRectResize.right = GetX(prlSdt->Brkf);
				}
			}
			else if(bmMovingDyna==GANTT_BREAK_PERIOD)
			{
				if(strcmp(prlSdt->Type,"S")==0)
				{
					olStartTime = prlSdt->Esbg;
					olEndTime   = prlSdt->Lsen;
				}
				else if(strcmp(prlSdt->Type,"D")==0)
				{
					olStartTime = prlSdt->Sbgi;
					olEndTime   = prlSdt->Seni;
				}
				if(IsBetween(olCurTime,prlSdt->Brkt,olEndTime)==true)
				{
					omRectResize.right = point.x;
				}
				else if(olCurTime > olEndTime)
				{
					omRectResize.right = GetX(olEndTime);
				}					
				else
				{
					omRectResize.right = GetX(prlSdt->Brkt);
				}
			}
			else if(bmMovingDyna==GANTT_SHIFT)
			{
				int ilExt		= prlSdt->Sex1;
				int ilMaxExt	= prlSdt->Sdu1 + prlSdt->Sex1;
				int ilXBkt1		= GetX(prlSdt->Bkt1);
				int ilXLsen		= GetX(prlSdt->Lsen);
				int ilMinExt	= prlSdt->Sdu1 - prlSdt->Ssh1;
				int ilXMax		= GetX(GetCTime(omRectResize.left) + CTimeSpan(0,int(ilMaxExt/60),int(ilMaxExt%60),0));
				int ilXMin		= GetX(GetCTime(omRectResize.left) + CTimeSpan(0,int(ilMinExt/60),int(ilMinExt%60),0));

				if(IsBetween(point.x,ilXBkt1,ilXLsen) && IsBetween(point.x,ilXMin,ilXMax))
				{
					omRectResize.right = point.x;
				}
				else if(point.x < ilXBkt1 || point.x < ilXMin) 
				{
					omRectResize.right = max(GetX(prlSdt->Bkt1),ilXMin);
				}
				else
				{
					omRectResize.right = min(GetX(prlSdt->Lsen),ilXMax);
				}
			}
			break;
		default:
			break;
		}
		omPointResize = point;
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
	}
	else
	{
		umResizeMode = HTNOWHERE;   // no more moving/resizing

		if (bmActiveBarSet)
		{
			if(imGroupno==SHIFT_DEMAND)
			{
				if(pomViewer->bmMaxView == true)
				{
					SDTDATA *prlSdt = ogSdtData.GetSdtByUrno(rmActiveBar.Urno);
					if (prlSdt == NULL && this->pomChartWnd && this->pomChartWnd->IsBreakOptActive())
					{
						prlSdt = ogSdtRosterData.GetSdtByUrno(rmActiveBar.Urno);
						if (prlSdt != NULL)
						{
							ogSdtRosterData.ChangeTime(rmActiveBar.Urno,GetCTime(omRectResize.left), GetCTime(omRectResize.right), bmMovingDyna,FALSE);
						}
					}
					else
					{
						if (bmMovingDyna==GANTT_BREAK && prlSdt != NULL)
						{
							ogMsdData.ChangeTime(prlSdt->MsdUrno,GetCTime(omRectResize.left), GetCTime(omRectResize.right), bmMovingDyna,TRUE);
						}

						ogSdtData.ChangeTime(rmActiveBar.Urno,GetCTime(omRectResize.left), GetCTime(omRectResize.right), bmMovingDyna);
					}
				}
				else
				{
					SDTDATA *prlActiveSdt = ogSdtData.GetSdtByUrno(rmActiveBar.Urno);
					if (prlActiveSdt == NULL && this->pomChartWnd && this->pomChartWnd->IsBreakOptActive())
					{
						prlActiveSdt = ogSdtRosterData.GetSdtByUrno(rmActiveBar.Urno);
						if (prlActiveSdt == NULL)
							return FALSE;

						ogSdtRosterData.ChangeTime(rmActiveBar.Urno,GetCTime(omRectResize.left), GetCTime(omRectResize.right), bmMovingDyna,FALSE);
					}
					else
					{
						CCSPtrArray<SDTDATA> olSdtData;
						ogSdtData.GetSdtByBsdu(prlActiveSdt->Bsdu,&olSdtData);

						CTime olStart = pomTimeScale->GetDisplayStartTime();
						CTimeSpan olTimeSpan  = pomTimeScale->GetDisplayDuration();
						CTime olEnd = olStart + olTimeSpan;

						int ilCount = olSdtData.GetSize();
						pomViewer->bmMaleAllesNeu_globalgesetzt = false;
						for(int i = 0; i < ilCount; i++)
						{
							SDTDATA rlSdt = olSdtData.GetAt(i);
							if(rlSdt.Esbg==prlActiveSdt->Esbg && rlSdt.Lsen==prlActiveSdt->Lsen)
							{
								if(strcmp(rlSdt.Bsdc,prlActiveSdt->Bsdc)==0 && strcmp(rlSdt.Fctc,prlActiveSdt->Fctc)==0)
								{
									pomViewer->SetSdtFlag(true);

									if (bmMovingDyna==GANTT_BREAK)
									{
										ogMsdData.ChangeTime(rlSdt.MsdUrno,GetCTime(omRectResize.left), GetCTime(omRectResize.right), bmMovingDyna,TRUE);
									}
									
									ogSdtData.ChangeTime(rlSdt.Urno,GetCTime(omRectResize.left), GetCTime(omRectResize.right), bmMovingDyna,FALSE);
								}
							}
						}
						pomViewer->bmMaleAllesNeu_globalgesetzt = true;
						pogCoverageDiagram->UpdateForShiftDemandUpdate(true);
						pogCoverageDiagram->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
					}
				}
			}
		}
		imCurrentBar = -1;
		bmActiveBarSet = FALSE;
	}
	ReleaseCapture();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
    DrawFocusRect(&dc, &omRectResize);
    return (umResizeMode != HTNOWHERE);
	CCS_CATCH_ALL
		return FALSE;
}

// return the item ID of the list box based on the given "point"
int CoverageGantt::GetItemFromPoint(CPoint point)
{
   CCS_TRY
    CRect rcItem;
    for (int itemID = GetTopIndex(); itemID < GetCount(); itemID++)
    {
        GetItemRect(itemID, &rcItem);
        if (rcItem.PtInRect(point))
            return itemID;
    }
    return -1;
	CCS_CATCH_ALL
    return -1;
}

// Return the bar number of the list box based on the given point
int CoverageGantt::GetBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft, int ipPreRight)
{
   CCS_TRY
    if (ipLineno == -1) // this checking was added after some bugs found
        return -1;

    // Calculate possible levels for vertical position
    CRect rcItem;
    GetItemRect(ipLineno, &rcItem);
    point.y -= rcItem.top /*+ imGutterHeight*/;
    int level1 = (point.y < imBarHeight)? 0: ((point.y - imBarHeight) / imOverlapHeight) + 1;
    int level2 = (point.y < 0)? -1: point.y / imOverlapHeight;

    // Calculate possible time for horizontal position
    CTime time1 = GetCTime(point.x-ipPreLeft);
    CTime time2 = GetCTime(point.x+ipPreRight);

    return pomViewer->GetBarnoFromTime(imGroupno, ipLineno, time1, time2, level1, level2);
	CCS_CATCH_ALL
	return -1;
}

// Return the background bar number of the list box based on the given point
int CoverageGantt::GetBkBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft, int ipPreRight)
{
   CCS_TRY
    if (ipLineno == -1) // this checking was added after some bugs found
        return -1;

    // Calculate possible time for horizontal position
    CTime time1 = GetCTime(point.x-ipPreLeft);
    CTime time2 = GetCTime(point.x+ipPreRight);

    return pomViewer->GetBkBarnoFromTime(imGroupno, ipLineno, time1, time2);
	CCS_CATCH_ALL
	return -1;
}

// Return the hit test area code (determine the location of the cursor).
// Possible codes are HTLEFT, HTRIGHT, HTNOWHERE (out-of-bar), HTCAPTION (bar body)
// 
UINT CoverageGantt::HitTest(int ipLineno, int ipBarno, CPoint point)
{
   CCS_TRY
    if (ipLineno == -1)
        return HTNOWHERE;
	else if (ipBarno == -1)
		return HTNOWHERE;
	else if (pomViewer->GetShiftRoster())
	{
		if (pomChartWnd && !pomChartWnd->IsBreakOptActive())
			return HTNOWHERE;
	}

    // Calculate possible time for horizontal position
    CTime time1 = GetCTime(point.x-imBorderPreLeft);
    CTime time2 = GetCTime(point.x+imBorderPreRight);

    // Check against each possible case of hit test areas.
    // Since most people like to drag things to the right, we check right border before left.
    // Be careful, if the bar is very small, the user may not be able to get HTCAPTION or HTLEFT.
    //
    COV_BARDATA *prlBar = pomViewer->GetBar(imGroupno, ipLineno, ipBarno);
	if(imGroupno==SHIFT_DEMAND)
	{
		SDTDATA *prlSdt = ogSdtData.GetSdtByUrno(prlBar->Urno);
		if(prlSdt!=NULL)
		{
			if(IsBetween(prlSdt->Bkf1,time1,time2) || IsBetween(prlSdt->Brkf,time1,time2) || (strcmp(prlSdt->Type,"D")==0 && IsBetween(prlSdt->Sbgi,time1,time2)) )
//				return HTLEFT;
				return HTCAPTION;
			else if(IsBetween(prlSdt->Bkt1,time1,time2) || IsBetween(prlSdt->Brkt,time1,time2) || (strcmp(prlSdt->Type,"D")==0 && IsBetween(prlSdt->Seni,time1,time2)))
//				return HTRIGHT;
				return HTCAPTION;
			else if (IsOverlapped(time1, time2, prlSdt->Bkf1, prlSdt->Bkt1) || strcmp(prlSdt->Type,"D")==0 && IsOverlapped(time1, time2, prlSdt->Sbgi, prlSdt->Seni))
				return HTCAPTION;
		}
		else if (pomChartWnd && pomChartWnd->IsBreakOptActive())
		{
			SDTDATA *prlSdt = ogSdtRosterData.GetSdtByUrno(prlBar->Urno);
			if(prlSdt!=NULL)
			{
				if(IsBetween(prlSdt->Bkf1,time1,time2) || IsBetween(prlSdt->Brkf,time1,time2) || (strcmp(prlSdt->Type,"D")==0 && IsBetween(prlSdt->Sbgi,time1,time2)) )
	//				return HTLEFT;
					return HTCAPTION;
				else if(IsBetween(prlSdt->Bkt1,time1,time2) || IsBetween(prlSdt->Brkt,time1,time2) || (strcmp(prlSdt->Type,"D")==0 && IsBetween(prlSdt->Seni,time1,time2)))
	//				return HTRIGHT;
					return HTCAPTION;
				else if (IsOverlapped(time1, time2, prlSdt->Bkf1, prlSdt->Bkt1) || strcmp(prlSdt->Type,"D")==0 && IsOverlapped(time1, time2, prlSdt->Sbgi, prlSdt->Seni))
					return HTCAPTION;
			}

		}
	}
	else
	{
		if (IsBetween(prlBar->EndTime, time1, time2))
			return HTRIGHT;
		else if (IsBetween(prlBar->StartTime, time1, time2))
			return HTLEFT;
		else if (IsOverlapped(time1, time2, prlBar->StartTime, prlBar->EndTime))
			return HTCAPTION;
	}

    return HTNOWHERE;
	CCS_CATCH_ALL
    return HTNOWHERE;
}

//-DTT Jul.25-----------------------------------------------------------
// Return true if the mouse position in on the vertical scale or a duty bar.
// Also remember the place where the drop happened in "imDropLineno".
BOOL CoverageGantt::IsDropOnDutyBar(CPoint point)
{
   CCS_TRY
	int ilLineno;
	if ((ilLineno = GetItemFromPoint(point)) == -1)
		return FALSE;	// cannot find a line of this diagram
	if (point.x < imVerticalScaleWidth - 2)
		return TRUE;	// mouse is on the vertical scale
	if (GetBkBarnoFromPoint(ilLineno, point) != -1)
		return TRUE;	// mouse is on a background bar

	// If it reaches here, mouse is on the background of the GanttChart.
	CCS_CATCH_ALL
	return FALSE;
}
//----------------------------------------------------------------------


//


LONG CoverageGantt::OnDragTime(UINT wParam, LONG lParam)
{
   CCS_TRY
	if(imGroupno == BASIC_SHIFT)
	{
		BASICSHIFTS *prlBsh = (BASICSHIFTS*)lParam;
		if(prlBsh != NULL)
		{
			pomViewer->SetMarkTime(prlBsh->From + CTimeSpan(prlBsh->DayOffSet,0,0,0), prlBsh->To + CTimeSpan(prlBsh->DayOffSet,0,0,0));
		}
	}
	return 0L;
	CCS_CATCH_ALL
	return 0L;
}

LONG CoverageGantt::OnDragBegin(UINT wParam, LONG lParam)
{
   CCS_TRY
/*	int ilitemID;

	ilitemID = (int)wParam;
	OPENJOBS *prlGhd = NULL;
	prlGhd = (OPENJOBS *)pomTable->GetTextLineData(ilitemID);
	if(prlGhd != NULL)
	{
		omDragDropObject.CreateDWordData(DIT_DUTYFROMTABLE, 1);
		omDragDropObject.AddDWord(prlGhd->LfdKey);

		omDragDropObject.BeginDrag();
	}
*/
	CCS_CATCH_ALL
   return 0L;
}



LONG CoverageGantt::OnDragOver(UINT wParam, LONG lParam)
{
   CCS_TRY
	
	CCSDragDropCtrl *pomDragDropCtrl = &m_DragDropTarget;

    ::GetCursorPos(&omDropPosition);
	ScreenToClient(&omDropPosition);    

	int ilClass = m_DragDropTarget.GetDataClass(); 
	if(imGroupno == SHIFT_DEMAND && pomViewer->bmShiftRoster==false && !pomViewer->bmAutoCovIsActive)
	{
		if (ilClass == DIT_BASIC_SHIFT_FOR_DEMAND )
		{
			long llUrno = pomDragDropCtrl->GetDataDWord(0);
			BASICSHIFTS *prlBsh = pomViewer->GetBasicShiftByUrno(llUrno);
			//BASICSHIFT *prlBsd;
			//prlBsd = ogBsdData.GetBsdByUrno(llUrno);
			if(prlBsh != NULL)
			{
				return 0L;
			}
		}
	}

	return -1L;
	CCS_CATCH_ALL
	return -1L;
}

LONG CoverageGantt::OnDrop(UINT wParam, LONG lParam)
{
   CCS_TRY

	NoBroadcastSupport olNoSupport;

	DROPEFFECT lpDropEffect = wParam;

	CCSDragDropCtrl *pomDragDropCtrl = &m_DragDropTarget;

    ::GetCursorPos(&omDropPosition);
	ScreenToClient(&omDropPosition);    
	CUIntArray olPfcUrnos;
	CUIntArray olGhsUrnos;
	pomViewer->GetGhsList(olGhsUrnos, olPfcUrnos);
	
	int ilClass = m_DragDropTarget.GetDataClass(); 
	if(imGroupno == SHIFT_DEMAND)
	{
		if(olPfcUrnos.GetSize()==0)
		{
			MessageBox(LoadStg(IDS_STRING850),LoadStg(IDS_STRING1734),MB_ICONEXCLAMATION);
			return -1L;
		}
		if (ilClass == DIT_BASIC_SHIFT_FOR_DEMAND )
		{
			long llUrno = pomDragDropCtrl->GetDataDWord(0);
			//BSDDATA *prlBsd;
			BASICSHIFTS *prlBsh = pomViewer->GetBasicShiftByUrno(llUrno);
			SDGDATA *prlSdg = pomViewer->GetCurrentDemand(SHIFT_DEMAND);
						
			if(prlBsh != NULL && (prlSdg != NULL || pomViewer->GetActuellState()))
			{
				CString olDemandName = pomViewer->GetCurrentDemandName(SHIFT_DEMAND);
				CString olName = olDemandName;

				BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(prlBsh->RUrno);
				if(prlBsd == NULL || (strcmp(prlBsd->Type,"D") == 0 && atoi(prlBsd->Sdu1) == 0))
				{
					MessageBox(LoadStg(IDS_STRING1693),LoadStg(IDS_STRING1734),MB_ICONEXCLAMATION);
					return -1L;
				}


				if(lpDropEffect == (lpDropEffect & DROPEFFECT_COPY))
				{
					CString olString = LoadStg(IDS_STRING1687) + CString(":");
					UniEingabe *polDlg = new UniEingabe(this, olString, CString(prlBsh->Code), 12);
					if(polDlg->DoModal() != IDCANCEL)
					{
						int ilCount = atoi(polDlg->m_Eingabe.GetBuffer(0));
						if(prlBsh->Fctc.IsEmpty())
						{
							CoveragePfcSelectDlg olDlg(this, &olPfcUrnos);

							if(olPfcUrnos.GetSize()>1)
							{
								if(olDlg.DoModal() == IDOK)
								{
									//int ilCount = atoi(polDlg->m_Eingabe.GetBuffer(0));
									CWaitCursor olWait;
									for(int i = 0; i < ilCount; i++)
									{
										if(pomViewer->GetActuellState())
										{
											ogDataSet.CreateSdtFromBsd(prlBsh,olDlg.omSelPfcCode);
										}
										else
										{
											ogDataSet.CreateMsdFromBsdAndSdg(prlBsh, prlSdg->Urno, prlBsh->From, prlBsh->To, olDlg.omSelPfcCode, false);
										}
									}
									if(!pomViewer->GetActuellState())
									{
										ogMsdData.Release();
									}
								}
							}
							else if(olPfcUrnos.GetSize()==1)
							{		
								long llUrno = olPfcUrnos.GetAt(0);
								PFCDATA *prlPfc = ogPfcData.GetPfcByUrno(llUrno);
								if(prlPfc!=NULL)
								{
									CString olPfcCode = CString(prlPfc->Fctc);
									CWaitCursor olWait;
									for(int i = 0; i < ilCount; i++)
									{			
										if(pomViewer->GetActuellState())
										{
											ogDataSet.CreateSdtFromBsd(prlBsh,olPfcCode);
										}
										else
										{
											ogDataSet.CreateMsdFromBsdAndSdg(prlBsh, prlSdg->Urno, prlBsh->From, prlBsh->To, olPfcCode, false);
										}
									}
									if(!pomViewer->GetActuellState())
									{
										ogMsdData.Release();
									}
								}
							}
						}
						else
						{
							bool blCont = true;
							if(!pomViewer->CheckBsdbyStdFilter(prlBsh))
							{
								
								if(MessageBox(LoadStg(IDS_STRING61250),LoadStg(IDS_STRING441),MB_YESNO|MB_ICONWARNING) == IDNO)
								{
									blCont = false;
								}
							}
							if(blCont)
							{

								CWaitCursor olWait;
								for(int i = 0; i < ilCount; i++)
								{
									if(pomViewer->GetActuellState())
									{
										ogDataSet.CreateSdtFromBsd(prlBsh,prlBsh->Fctc);
									}
									else
									{
										ogDataSet.CreateMsdFromBsdAndSdg(prlBsh, prlSdg->Urno, prlBsh->From, prlBsh->To, prlBsh->Fctc, false);
									}
								}
								if(!pomViewer->GetActuellState())
								{
									ogMsdData.Release();
								}
							}
						}

						delete polDlg;
					}
				}
				else
				{
					CUIntArray olPfcUrnos;
					CUIntArray olGhsUrnos;

					pomViewer->GetGhsList(olGhsUrnos, olPfcUrnos);
					if(prlBsh->Fctc.IsEmpty())
					{
						if(olPfcUrnos.GetSize()>1)
						{
							CoveragePfcSelectDlg olDlg(this, &olPfcUrnos);

							if(olDlg.DoModal() == IDOK)
							{
								CWaitCursor olWait;
								if(pomViewer->GetActuellState())
								{
									ogDataSet.CreateSdtFromBsd(prlBsh,olDlg.omSelPfcCode);
								}
								else
								{
									ogDataSet.CreateMsdFromBsdAndSdg(prlBsh, prlSdg->Urno, prlBsh->From, prlBsh->To, olDlg.omSelPfcCode, false);
								}
								if(!pomViewer->GetActuellState())
								{
									ogMsdData.Release();
								}
								if(prlSdg != NULL)
									pomViewer->lmSdgu = prlSdg->Urno;
								else
									pomViewer->lmSdgu = 0;
							}
						}
						else if(olPfcUrnos.GetSize() == 1)
						{
							long llUrno = olPfcUrnos.GetAt(0);
							PFCDATA *prlPfc = ogPfcData.GetPfcByUrno(llUrno);
							if(prlPfc!=NULL)
							{
								CString olPfcCode = CString(prlPfc->Fctc);
								CWaitCursor olWait;
								if(pomViewer->GetActuellState())
								{
									ogDataSet.CreateSdtFromBsd(prlBsh,olPfcCode);
								}
								else
								{
									ogDataSet.CreateMsdFromBsdAndSdg(prlBsh, prlSdg->Urno, prlBsh->From, prlBsh->To, olPfcCode, false);
								}
								if(!pomViewer->GetActuellState())
								{
									ogMsdData.Release();
								}
								if(prlSdg != NULL)
									pomViewer->lmSdgu = prlSdg->Urno;
								else
									pomViewer->lmSdgu = 0;

							}
						}
					}
					else
					{

						bool blCont = true;
						if(!pomViewer->CheckBsdbyStdFilter(prlBsh))
						{
							
							if(MessageBox(LoadStg(IDS_STRING61250),LoadStg(IDS_STRING441),MB_YESNO|MB_ICONWARNING) == IDNO)
							{
								blCont = false;
							}
						}
						if(blCont)
						{
							if(pomViewer->GetActuellState())
							{
								ogDataSet.CreateSdtFromBsd(prlBsh,prlBsh->Fctc);
							}
							else
							{
								ogDataSet.CreateMsdFromBsdAndSdg(prlBsh, prlSdg->Urno, prlBsh->From, prlBsh->To, prlBsh->Fctc, false);
							}
							if(!pomViewer->GetActuellState())
							{
								ogMsdData.Release();
							}
							if(prlSdg != NULL)
								pomViewer->lmSdgu = prlSdg->Urno;
							else
								pomViewer->lmSdgu = 0;
						}
					}
				}

				CWaitCursor olWait;
//				ogSdtData.Release();
				pomViewer->SetSdtFlag(true);
				pomViewer->LoadSdtWindow(true);
				if(pogCoverageDiagram != NULL)
				{
					char pclName[100]="";
					strcpy(pclName, olDemandName);
				//	pomChartWnd->SendMessage(WM_COV_RELOADCOMBO, 0, 0);
					pomChartWnd->SendMessage(WM_COV_UPDATECOMBO,0,(LPARAM)(olName.GetBuffer(0)));
				//	pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 1, 0);
				}
			}
		}
		
	}
    return -1L;
CCS_CATCH_ALL
return -1L;
}


void CoverageGantt::ProcessMarkBar(void *prpSdt)
{
CCS_TRY
   if(prpSdt == NULL)
	   return;
   int itemID, ilBarno;
   if(imGroupno == SHIFT_DEMAND)
   {
	   SDTDATA *prlSdt = (SDTDATA *)prpSdt;
	   if(pomViewer->FindBar(prlSdt->Urno, imGroupno, itemID, ilBarno) == TRUE)
	   {
		   switch(bmMovingDyna)
		   {
		   case GANTT_BREAK:
			   pomViewer->SetMarkTime(prlSdt->Brkf, prlSdt->Brkt);
			   break;
		   case GANTT_SHIFT:
			   pomViewer->SetMarkTime(prlSdt->Sbgi, prlSdt->Seni);
			   break;
		   default:
			   break;
		   }
	   }
   }
CCS_CATCH_ALL
}
LONG CoverageGantt::ProcessDropGhdDuty(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	return 0L;
}

void CoverageGantt::DragBsdBarBegin(int ipLineno, int ipBarno)
{
   CCS_TRY
	long llUrno = pomViewer->GetBar(BASIC_SHIFT, ipLineno, ipBarno)->Urno;
	BASICSHIFTS *prlBsh = pomViewer->GetBasicShiftByUrno(llUrno);
	if(prlBsh != NULL)
	{
//		BSDDATA *prlBsd;
//		prlBsd = ogBsdData.GetBsdByUrno(prlBsh->Urno);
//		if(prlBsd != NULL)
		{
			m_DragDropSource.CreateDWordData(DIT_BASIC_SHIFT_FOR_DEMAND, 1);
			m_DragDropSource.AddDWord(prlBsh->Urno);
			m_DragDropSource.BeginDrag();
		}
	}
	CCS_CATCH_ALL
}

void CoverageGantt::OnMenuNewShift()
{
CCS_TRY
	if(imGroupno == SHIFT_DEMAND)
	{
		
	}
	else if(imGroupno == BASIC_SHIFT)
	{

	}
CCS_CATCH_ALL
}

void CoverageGantt::OnMenuChangeShift()
{
CCS_TRY
	   if(imGroupno == SHIFT_DEMAND)
	   {
	   }
	   else if(imGroupno == BASIC_SHIFT)
	   {
	   }
CCS_CATCH_ALL
}

void CoverageGantt::OnMenuDuplicateShift()
{
CCS_TRY

	NoBroadcastSupport olNoSupport;

	if(imGroupno == SHIFT_DEMAND)
	{
		SDTDATA *prlSdt = ogSdtData.GetSdtByUrno(rmActiveBar.Urno);
		
		CTime olLoadStart;
		CTime olLoadEnd;
		pomViewer->GetTimeFrameFromTo(olLoadStart, olLoadEnd);
		//olLoadStart = pomTimeScale->GetTimeFromX(omDropPosition.x);
		CString olText = olLoadStart.Format("%H:%M  %d.%m.%Y");
	   
		olLoadStart = CTime(olLoadStart.GetYear(), olLoadStart.GetMonth(), olLoadStart.GetDay(), 0, 0, 0);
		olText = olLoadStart.Format("%H:%M  %d.%m.%Y");
		
		if(prlSdt != NULL)
		{
			CString olDemandName = pomViewer->GetCurrentDemandName(SHIFT_DEMAND);
			CString olName = olDemandName;
			
			CString olString = LoadStg(IDS_STRING61362) + CString(":");
			UniEingabe *polDlg = new UniEingabe(this, olString, CString(prlSdt->Bsdc), 12);
			if(polDlg->DoModal() != IDCANCEL)
			{
				CUIntArray olPfcUrnos;
				CUIntArray olGhsUrnos;
				int ilCount = atoi(polDlg->m_Eingabe.GetBuffer(0));
				pomViewer->GetGhsList(olGhsUrnos, olPfcUrnos);
				BASICSHIFTS rlBsh;
				BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(prlSdt->Bsdu);
				rlBsh.From = prlSdt->Esbg;
				rlBsh.To = prlSdt->Lsen;
				pomViewer->MakeBshFromBsdData(&rlBsh,prlBsd);

				if(rlBsh.Fctc.IsEmpty())
				{
					CoveragePfcSelectDlg olDlg(this, &olPfcUrnos);
					if(olPfcUrnos.GetSize()>1)
					{
						if(olDlg.DoModal() == IDOK)
						{
							SDGDATA *prlSdg = pomViewer->GetCurrentDemand(SHIFT_DEMAND);
							for(int i = 0; i < ilCount; i++)
							{
								if(pomViewer->GetActuellState())
								{
									ogDataSet.CreateSdtFromBsd(&rlBsh,olDlg.omSelPfcCode);
								}
								else
								{
									ogDataSet.CreateMsdFromBsdAndSdg(&rlBsh, prlSdg->Urno, rlBsh.From, rlBsh.To, olDlg.omSelPfcCode, false);
								}
							}
							if(!pomViewer->GetActuellState())
							{
								ogMsdData.Release();
							}
							pomViewer->SetSdtFlag(true);
						}
					}
					else if(olPfcUrnos.GetSize()==1)
					{		
						long llUrno = olPfcUrnos.GetAt(0);
						PFCDATA *prlPfc = ogPfcData.GetPfcByUrno(llUrno);
						if(prlPfc!=NULL)
						{
							SDGDATA *prlSdg = pomViewer->GetCurrentDemand(SHIFT_DEMAND);
							CString olPfcCode = CString(prlPfc->Fctc);
							for(int i = 0; i < ilCount; i++)
							{
								if(pomViewer->GetActuellState())
								{
									ogDataSet.CreateSdtFromBsd(&rlBsh,olPfcCode);
								}
								else
								{
									ogDataSet.CreateMsdFromBsdAndSdg(&rlBsh, prlSdg->Urno, rlBsh.From, rlBsh.To, olPfcCode, false);
								}
							}
							if(!pomViewer->GetActuellState())
							{
								ogMsdData.Release();
							}
							pomViewer->SetSdtFlag(true);
						}
					}
				}
				else
				{
					SDGDATA *prlSdg = pomViewer->GetCurrentDemand(SHIFT_DEMAND);
					for(int i = 0; i < ilCount; i++)
					{
						if(pomViewer->GetActuellState())
						{
							ogDataSet.CreateSdtFromBsd(&rlBsh,rlBsh.Fctc);
						}
						else
						{
							ogDataSet.CreateMsdFromBsdAndSdg(&rlBsh, prlSdg->Urno, rlBsh.From, rlBsh.To, rlBsh.Fctc, false);
						}
					}
					if(!pomViewer->GetActuellState())
					{
						ogMsdData.Release();
					}
					pomViewer->SetSdtFlag(true);
				}

			}
			delete polDlg;
		}
		if(pogCoverageDiagram != NULL)
		{
			pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 1, 0);
			//pogCoverageDiagram->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
		}
	}
   
CCS_CATCH_ALL
}

void CoverageGantt::OnMenuNumberShift()
{
CCS_TRY

	NoBroadcastSupport olNoSupport;

	if (imGroupno == SHIFT_DEMAND)
	{
		SDTDATA *prlSdt = ogSdtData.GetSdtByUrno(rmActiveBar.Urno);
		
		CTime olLoadStart;
		CTime olLoadEnd;
		pomViewer->GetTimeFrameFromTo(olLoadStart, olLoadEnd);
		//olLoadStart = pomTimeScale->GetTimeFromX(omDropPosition.x);
		CString olText = olLoadStart.Format("%H:%M  %d.%m.%Y");
	   
		olLoadStart = CTime(olLoadStart.GetYear(), olLoadStart.GetMonth(), olLoadStart.GetDay(), 0, 0, 0);
		olText = olLoadStart.Format("%H:%M  %d.%m.%Y");
		
		if (prlSdt != NULL)
		{
			CString olDemandName = pomViewer->GetCurrentDemandName(SHIFT_DEMAND);
			CString olName = olDemandName;
			
			CString olString = LoadStg(IDS_STRING1687) + CString(":");
			CString olDefValue;
			olDefValue.Format("%d",rmActiveBar.Brutto);
			UniEingabe olDlg(this, olString, CString(prlSdt->Bsdc), 12,olDefValue);
			if (olDlg.DoModal() != IDCANCEL)
			{
				int ilCount = atoi(olDlg.m_Eingabe.GetBuffer(0));
				prlSdt = ogSdtData.GetSdtByUrno(rmActiveBar.Urno);
				if (prlSdt != NULL)
				{
					pomViewer->AlterSdtCount(rmActiveBar.Brutto,ilCount,prlSdt->Urno);
				}

			}
		}
		if(pogCoverageDiagram != NULL)
		{
			pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 1, 0);
			//pogCoverageDiagram->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
		}
	}
   
CCS_CATCH_ALL
}
void CoverageGantt::OnButtonDelete()
{
CCS_TRY
	TRACE("CoverageGantt::OnButtonDelete");
	if(imGroupno == BASIC_SHIFT)
	{
		BASICSHIFTS *prlBsh = pomViewer->GetBasicShiftByUrno(rmActiveBar.Urno);
		CTime olLoadStart;
		CTime olLoadEnd;
		
		//pomViewer->GetTimeFrameFromTo(olLoadStart, olLoadEnd);

		if(prlBsh != NULL)
		{
			BSDDATA *activeBsd = ogBsdData.GetBsdByUrno(prlBsh->RUrno);
			if (activeBsd != NULL)
			{
				BSDDATA rlBsd = *activeBsd;
				if (IDYES == MessageBox(LoadStg(IDS_STRING61560),LoadStg(IDS_STRING61561),(MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2)))
				{
					TRACE("delete!");
					// Referenz Check
					CheckReferenz olCheckReferenz;
					//SetStatusText(0,LoadStg(IDS_STRING120));
					int ilCount = olCheckReferenz.Check(_BSD, rlBsd.Urno, rlBsd.Bsdc);
					//SetStatusText();
					bool blDelete = true;
					CString olTxt;
					if(ilCount>0)
					{
						blDelete = false;
						if (!ogBasicData.BackDoorEnabled())
						{
							olTxt.Format("%d %s",ilCount, LoadStg(IDS_STRING61562));
							MessageBox(olTxt,LoadStg(IDS_STRING61563),MB_ICONERROR);
						}
						else
						{
							olTxt.Format("%d %s",ilCount, LoadStg(IDS_STRING61565));
							if(IDYES == MessageBox(olTxt,LoadStg(IDS_STRING61564),(MB_ICONSTOP | MB_YESNOCANCEL | MB_DEFBUTTON2)))
							{
								blDelete = true;
							}
						}
					}
					// END Referenz Check
					if(blDelete)
					{
						AfxGetApp()->DoWaitCursor(1);
						if(ogBsdData.Delete(rlBsd.Urno)==false)
						{
							CString omErrorTxt;
							CString omDeleteErrTxt = LoadStg(ST_DELETEERR);
							omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogBsdData.imLastReturnCode, ogBsdData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING61563),MB_ICONERROR);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
				}
			}
		}
	}
CCS_CATCH_ALL
}
void CoverageGantt::OnButtonUpdate()
{
CCS_TRY
	TRACE("CoverageGantt::OnButtonUpdate");
    if(imGroupno == BASIC_SHIFT)
	{
		BASICSHIFTS *prlBsh = pomViewer->GetBasicShiftByUrno(rmActiveBar.Urno);
		CTime olLoadStart;
		CTime olLoadEnd;
		
		//pomViewer->GetTimeFrameFromTo(olLoadStart, olLoadEnd);

		if(prlBsh != NULL)
		{
			BSDDATA *activeBsd = ogBsdData.GetBsdByUrno(prlBsh->RUrno);

			// show sth
			BSDDATA rlBsd = *activeBsd;
			BasisschichtenDlg *polBsdDlg = new BasisschichtenDlg(&rlBsd,true,this);
			polBsdDlg->m_Caption = LoadStg(IDS_STRING61559);
			if (polBsdDlg->DoModal() == IDOK)
			{
				TRACE("CoverageGantt::OnLButtonDblClk BASIC_SHIFT \n");
				//if (IDYES == MessageBox(LoadStg(IDS_STRING924),LoadStg(IDS_STRING154),(MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2)))
				{
					rlBsd.Lstu = CTime::GetCurrentTime();
					strcpy(rlBsd.Useu,pcgUser);
					AfxGetApp()->DoWaitCursor(1);
					if(ogBsdData.Update(&rlBsd)==false)
					{
						CString omErrorTxt;
						CString omUpdateErrTxt = LoadStg(ST_INSERTERR);
						omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogBsdData.imLastReturnCode, ogBsdData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING61553),MB_ICONERROR);
					}	
					AfxGetApp()->DoWaitCursor(-1);
				}
			}
		}
	}
CCS_CATCH_ALL
}
void CoverageGantt::OnMenuCopyShift()
{
CCS_TRY

	NoBroadcastSupport olNoSupport;

	if(imGroupno == SHIFT_DEMAND)
	{
		BASICSHIFTS rlBsh;
		CCSPtrArray<SDTDATA> olSdtData;
		CWaitCursor olWait;
		if(ogSdtData.GetSelectedSdt(&olSdtData))
		{
			for(int ilCnt = olSdtData.GetSize()-1;ilCnt>=0;ilCnt--)
			{
				SDTDATA *prlSdt = &olSdtData[ilCnt];
				BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(prlSdt->Bsdu);
				rlBsh.From = prlSdt->Esbg;
				rlBsh.To = prlSdt->Lsen;
				pomViewer->MakeBshFromBsdData(&rlBsh,prlBsd);
				SDGDATA *prlSdg = pomViewer->GetCurrentDemand(SHIFT_DEMAND);
				if(pomViewer->GetActuellState())
				{
					ogDataSet.CreateSdtFromBsd(&rlBsh,prlSdt->Fctc);
				}
				else
				{
					ogDataSet.CreateMsdFromBsdAndSdg(&rlBsh, prlSdg->Urno, rlBsh.From, rlBsh.To, prlSdt->Fctc, false);
				}
			}
			if(!pomViewer->GetActuellState())
			{
				ogMsdData.Release();
			}			
			olSdtData.DeleteAll();
		}
		else
		{
			SDTDATA *prlSdt = ogSdtData.GetSdtByUrno(rmActiveBar.Urno);
			BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(prlSdt->Bsdu);
			rlBsh.From = prlSdt->Esbg;
			rlBsh.To = prlSdt->Lsen;
			pomViewer->MakeBshFromBsdData(&rlBsh,prlBsd);
			SDGDATA *prlSdg = pomViewer->GetCurrentDemand(SHIFT_DEMAND);
			if(pomViewer->GetActuellState())
			{
				ogDataSet.CreateSdtFromBsd(&rlBsh,prlSdt->Fctc);
			}
			else
			{
				ogDataSet.CreateMsdFromBsdAndSdg(&rlBsh, prlSdg->Urno, rlBsh.From, rlBsh.To, prlSdt->Fctc, false);
			}
			if(!pomViewer->GetActuellState())
			{
				ogMsdData.Release();
			}
		}
		pomViewer->LoadSdtWindow(true);
		pomViewer->ChangeViewToForHScroll(TIMENULL,TIMENULL);
		if(pogCoverageDiagram != NULL)
		{
			pogCoverageDiagram->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
			pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 1, 0);
		}

	}
	else if(imGroupno == BASIC_SHIFT)
	{
		BASICSHIFTS *prlBsh = pomViewer->GetBasicShiftByUrno(rmActiveBar.Urno);
		CTime olLoadStart;
		CTime olLoadEnd;
		
		//pomViewer->GetTimeFrameFromTo(olLoadStart, olLoadEnd);

		if(prlBsh != NULL)
		{
			BSDDATA *prlActiveBsd = ogBsdData.GetBsdByUrno(prlBsh->RUrno);
			if(prlActiveBsd != NULL)
			{
				CWaitCursor olWait;
				
				//BASICSHIFTS *prlBsh = pomViewer->GetBasicShiftByUrno(llUrno);
				SDGDATA *prlSdg = pomViewer->GetCurrentDemand(SHIFT_DEMAND);
				//prlBsd = ogBsdData.GetBsdByUrno(llUrno);
				if(prlBsh != NULL && (prlSdg != NULL || pomViewer->GetActuellState()))
				{
					CString olDemandName = pomViewer->GetCurrentDemandName(SHIFT_DEMAND);
					CString olName = olDemandName;

					CUIntArray olPfcUrnos;
					CUIntArray olGhsUrnos;
					int ilCount = 1;
					
					pomViewer->GetGhsList(olGhsUrnos, olPfcUrnos);
					CoveragePfcSelectDlg olDlg(this, &olPfcUrnos);

					if(prlBsh->Fctc.IsEmpty())
					{
						if(olPfcUrnos.GetSize()>1)
						{
							if(olDlg.DoModal() == IDOK)
							{
								for(int i = 0; i < ilCount; i++)
								{
									if(pomViewer->GetActuellState())
									{
										ogDataSet.CreateSdtFromBsd(prlBsh,olDlg.omSelPfcCode);
									}
									else
									{
										ogDataSet.CreateMsdFromBsdAndSdg(prlBsh, prlSdg->Urno, prlBsh->From, prlBsh->To, olDlg.omSelPfcCode, false);
									}
								}
								if(!pomViewer->GetActuellState())
								{
									ogMsdData.Release();
								}
							}
						}
						else if(olPfcUrnos.GetSize()==1)
						{
							long llUrno = olPfcUrnos.GetAt(0);
							PFCDATA *prlPfc = ogPfcData.GetPfcByUrno(llUrno);
							if(prlPfc!=NULL)
							{
								CString olPfcCode = CString(prlPfc->Fctc);
								for(int i = 0; i < ilCount; i++)
								{
									if(pomViewer->GetActuellState())
									{
										ogDataSet.CreateSdtFromBsd(prlBsh,olPfcCode);
									}
									else
									{
										ogDataSet.CreateMsdFromBsdAndSdg(prlBsh, prlSdg->Urno, prlBsh->From, prlBsh->To, olPfcCode, false);
									}
								}
								if(!pomViewer->GetActuellState())
								{
									ogMsdData.Release();
								}
							}
						}
					}
					else
					{
						bool blCont = true;
						if(!pomViewer->CheckBsdbyStdFilter(prlBsh))
						{
							
							if(MessageBox(LoadStg(IDS_STRING61250),LoadStg(IDS_STRING441),MB_YESNO|MB_ICONWARNING) == IDNO)
							{
								blCont = false;
							}
						}
						if(blCont)
						{
							for(int i = 0; i < ilCount; i++)
							{
								if(pomViewer->GetActuellState())
								{
									ogDataSet.CreateSdtFromBsd(prlBsh,prlBsh->Fctc);
								}
								else
								{
									ogDataSet.CreateMsdFromBsdAndSdg(prlBsh, prlSdg->Urno, prlBsh->From, prlBsh->To, prlBsh->Fctc, false);
								}
							}
							if(!pomViewer->GetActuellState())
							{
								ogMsdData.Release();
							}
						}
					}
				}
				pomViewer->LoadSdtWindow(true);
				pomViewer->ChangeViewToForHScroll(TIMENULL,TIMENULL);
				if(pogCoverageDiagram != NULL)
				{
					pogCoverageDiagram->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
					pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 1, 0);
				}
			}
		}
	}
//	pomViewer->LoadSdtWindow();
CCS_CATCH_ALL
}
    
void CoverageGantt::OnMenuDeleteDemand()
{
   CCS_TRY

	   NoBroadcastSupport olNoSupport;

	   bool blFirst = true;	
	   bool blRet   = true;

	   if(imGroupno == SHIFT_DEMAND)
	   {
			CCSPtrArray<SDTDATA> olSdtData;
			CWaitCursor olWait;
			if(ogSdtData.GetSelectedSdt(&olSdtData))
			{
				if (MessageBox(LoadStg(IDS_STRING1696),LoadStg(COV_BTN_SHIFTDEMANDS),MB_YESNO|MB_DEFBUTTON2|MB_ICONQUESTION) == IDYES)
				{
					for(int ilCnt=olSdtData.GetSize()-1;ilCnt>=0;ilCnt--)
					{
						SDTDATA rlSdt = olSdtData.GetAt(ilCnt);
						if(pomViewer->GetActuellState())
						{
							ogSdtData.DeleteSdt(&rlSdt,true,false);
						}
						else
						{
							CTime olStartTime = rlSdt.Sbgi;
							if(strcmp(rlSdt.Type,"S")==0)
							{
								olStartTime = rlSdt.Esbg;
							}
							
							if (!ogDataSet.DeleteMsdByMsduAndSdgu(rlSdt.MsdUrno,rlSdt.Sdgu,olStartTime,blFirst,false))
							{
								break;
							}
							else
							{
								blFirst = false;
							}
						}
					}
					if(!pomViewer->GetActuellState())
					{
						ogMsdData.Release();
					}

				}
				olWait.Restore();
				olSdtData.DeleteAll();
				pomViewer->SetMarkTime(0,0);
			}
			else
			{
				SDTDATA *prlSdtData = ogSdtData.GetSdtByUrno(rmActiveBar.Urno);
				pomViewer->SetSdtFlag(true);
				if(prlSdtData != NULL)
				{
					if(pomViewer->GetActuellState())
					{
						ogSdtData.DeleteSdt(prlSdtData,true,false);
					}
					else
					{
						CTime olStartTime = prlSdtData->Sbgi;
						if(strcmp(prlSdtData->Type,"S")==0)
						{
							olStartTime = prlSdtData->Esbg;
						}
							
						ogDataSet.DeleteMsdByMsduAndSdgu(prlSdtData->MsdUrno,prlSdtData->Sdgu,olStartTime,blFirst);
						blFirst = false;
					}
				}
				pomViewer->SetMarkTime(0,0);
				
			}
			pomViewer->LoadSdtWindow(true);
			pomViewer->ChangeViewToForHScroll(TIMENULL,TIMENULL);

			if(pogCoverageDiagram != NULL)
			{
				pogCoverageDiagram->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
				pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 1, 0);
			}

	   }
	   else if(imGroupno == BASIC_SHIFT)
	   {
		   // Check for Referenzen
	   }
CCS_CATCH_ALL
}



void CoverageGantt::OnMenuDebugInfo()
{
CCS_TRY
	if(imGroupno == SHIFT_DEMAND)
	{
		BASICSHIFTS rlBsh;
		CCSPtrArray<SDTDATA> olSdtData;
		if(ogSdtData.GetSelectedSdt(&olSdtData))
		{
			for (int i = 0; i < olSdtData.GetSize(); i++)
			{
				SDTDATA *prlSdt = &olSdtData[i];
			}
		}
		else
		{
			SDTDATA *prlSdt = ogSdtData.GetSdtByUrno(rmActiveBar.Urno);
			BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(prlSdt->Bsdu);
		}
	}
	else if(imGroupno == BASIC_SHIFT)
	{
		BASICSHIFTS *prlBsh = pomViewer->GetBasicShiftByUrno(rmActiveBar.Urno);

		if(prlBsh != NULL)
		{
			CString olTime = prlBsh->From.Format("%d.%m.%Y %H:%M:%S");
			olTime = prlBsh->To.Format("%d.%m.%Y %H:%M:%S");
			BSDDATA *prlActiveBsd = ogBsdData.GetBsdByUrno(prlBsh->RUrno);
			if(prlActiveBsd != NULL)
			{
				int ilPos = GetX(prlBsh->From);
				CTime ilTime = GetCTime(ilPos);
				olTime = ilTime.Format("%d.%m.%Y %H:%M:%S");
			}
		}
	}
CCS_CATCH_ALL
}

void CoverageGantt::OnMenuDebugStatistic()
{
CCS_TRY
	if(imGroupno == SHIFT_DEMAND)
	{
		CMapStringToPtr olBsdGroupStatistic;
		if (pomViewer->GetDebugStatistics(olBsdGroupStatistic))
		{
			CString olName;
			int		ilCount;
			CString	olFmt;
			CString	olMsg;

			for (POSITION pos = olBsdGroupStatistic.GetStartPosition(); pos != NULL;)
			{
				olBsdGroupStatistic.GetNextAssoc(pos,olName,(void *&)ilCount);
				olFmt.Format("ShiftGroup : [%s] = [%d] shifts\n",olName,ilCount);
				olMsg += olFmt;
			}

			::MessageBox(NULL,olMsg,"Shiftgroup coverage",MB_OK|MB_ICONINFORMATION);
		}
	}
CCS_CATCH_ALL
}

// Saves gantt position
void CoverageGantt::SaveTopIndex(int ipIndex)
{
	if(this->GetSafeHwnd()!=NULL)
	{
		int ilLimit = GetCount(); 
		if(ipIndex < ilLimit)
		{
			bmTopIndex = imTopLevel = ipIndex;
		}
		else if(ilLimit>0)
		{
			bmTopIndex = imTopLevel = ilLimit-1;
		}
	}
	else
	{
		bmTopIndex = imTopLevel = 0;
	}
}

// Sets gantt at spec. scoll position!
int CoverageGantt::SetVScrollPos()
{
	if(this->GetSafeHwnd()!=NULL)
	{
		return SetTopIndex(bmTopIndex);
	}
	else
	{
		return -1;
	}
}


void CoverageGantt::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CCS_TRY
	GetParent()->SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
	CCS_CATCH_ALL
}

void CoverageGantt::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CCS_TRY
	GetParent()->SendMessage(WM_KEYUP, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
	CCS_CATCH_ALL
}
