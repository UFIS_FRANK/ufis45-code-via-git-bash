// CedaOacData.cpp
 
#include <stdafx.h>
#include <CedaOacData.h>
#include <resource.h>


void ProcessOacCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaOacData::CedaOacData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for OACDataStruct
	BEGIN_CEDARECINFO(OACDATA,OACDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Ctrc,"CTRC")
		FIELD_CHAR_TRIM	(Sdac,"SDAC")

	END_CEDARECINFO //(OACDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(OACDataRecInfo)/sizeof(OACDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&OACDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"OAC");
	strcpy(pcmListOfFields,"URNO,CTRC,SDAC");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaOacData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("URNO");
	ropFields.Add("CTRC");
	ropFields.Add("SDAC");

	ropDesription.Add(LoadStg(IDS_STRING61468));
	ropDesription.Add(LoadStg(IDS_STRING61489));
	ropDesription.Add(LoadStg(IDS_STRING61491));

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaOacData::Register(void)
{
	ogDdx.Register((void *)this,BC_OAC_CHANGE,	CString("OACDATA"), CString("Oac-changed"),	ProcessOacCf);
	ogDdx.Register((void *)this,BC_OAC_NEW,		CString("OACDATA"), CString("Oac-new"),		ProcessOacCf);
	ogDdx.Register((void *)this,BC_OAC_DELETE,	CString("OACDATA"), CString("Oac-deleted"),	ProcessOacCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaOacData::~CedaOacData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaOacData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaOacData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		OACDATA *prlOac = new OACDATA;
		if ((ilRc = GetFirstBufferRecord(prlOac)) == true)
		{
			omData.Add(prlOac);//Update omData
			omUrnoMap.SetAt((void *)prlOac->Urno,prlOac);
		}
		else
		{
			delete prlOac;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaOacData::Insert(OACDATA *prpOac)
{
	prpOac->IsChanged = DATA_NEW;
	if(Save(prpOac) == false) return false; //Update Database
	InsertInternal(prpOac);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaOacData::InsertInternal(OACDATA *prpOac)
{
	ogDdx.DataChanged((void *)this, OAC_NEW,(void *)prpOac ); //Update Viewer
	omData.Add(prpOac);//Update omData
	omUrnoMap.SetAt((void *)prpOac->Urno,prpOac);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaOacData::Delete(long lpUrno)
{
	OACDATA *prlOac = GetOacByUrno(lpUrno);
	if (prlOac != NULL)
	{
		prlOac->IsChanged = DATA_DELETED;
		if(Save(prlOac) == false) return false; //Update Database
		DeleteInternal(prlOac);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaOacData::DeleteInternal(OACDATA *prpOac)
{
	ogDdx.DataChanged((void *)this,OAC_DELETE,(void *)prpOac); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpOac->Urno);
	int ilOacCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilOacCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpOac->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaOacData::Update(OACDATA *prpOac)
{
	if (GetOacByUrno(prpOac->Urno) != NULL)
	{
		if (prpOac->IsChanged == DATA_UNCHANGED)
		{
			prpOac->IsChanged = DATA_CHANGED;
		}
		if(Save(prpOac) == false) return false; //Update Database
		UpdateInternal(prpOac);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaOacData::UpdateInternal(OACDATA *prpOac)
{
	OACDATA *prlOac = GetOacByUrno(prpOac->Urno);
	if (prlOac != NULL)
	{
		*prlOac = *prpOac; //Update omData
		ogDdx.DataChanged((void *)this,OAC_CHANGE,(void *)prlOac); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

OACDATA *CedaOacData::GetOacByUrno(long lpUrno)
{
	OACDATA  *prlOac;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlOac) == TRUE)
	{
		return prlOac;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaOacData::ReadSpecialData(CCSPtrArray<OACDATA> *popOac,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","OAC",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","OAC",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popOac != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			OACDATA *prpOac = new OACDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpOac,CString(pclFieldList))) == true)
			{
				popOac->Add(prpOac);
			}
			else
			{
				delete prpOac;
			}
		}
		if(popOac->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaOacData::Save(OACDATA *prpOac)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpOac->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpOac->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpOac);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpOac->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpOac->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpOac);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpOac->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpOac->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessOacCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogOacData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaOacData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlOacData;
	prlOacData = (struct BcStruct *) vpDataPointer;
	OACDATA *prlOac;
	if(ipDDXType == BC_OAC_NEW)
	{
		prlOac = new OACDATA;
		GetRecordFromItemList(prlOac,prlOacData->Fields,prlOacData->Data);
		InsertInternal(prlOac);
	}
	if(ipDDXType == BC_OAC_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlOacData->Selection);
		prlOac = GetOacByUrno(llUrno);
		if(prlOac != NULL)
		{
			GetRecordFromItemList(prlOac,prlOacData->Fields,prlOacData->Data);
			UpdateInternal(prlOac);
		}
	}
	if(ipDDXType == BC_OAC_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlOacData->Selection);

		prlOac = GetOacByUrno(llUrno);
		if (prlOac != NULL)
		{
			DeleteInternal(prlOac);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
