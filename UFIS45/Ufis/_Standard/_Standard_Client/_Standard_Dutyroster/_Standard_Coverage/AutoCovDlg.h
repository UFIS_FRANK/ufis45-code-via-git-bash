#if !defined(AFX_AUTOCOVDLG_H__0DCCB531_1D2B_11D5_9779_0050DAE32E8C__INCLUDED_)
#define AFX_AUTOCOVDLG_H__0DCCB531_1D2B_11D5_9779_0050DAE32E8C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AutoCovDlg.h : header file
//
#include <GridFenster.h>
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// CAutoCovDlg dialog

class CAutoCovDlg : public CDialog
{
// Construction
public:
	CAutoCovDlg(CWnd* pParent,CStringArray &opBasicShiftViewList);
	~CAutoCovDlg();
	void CAutoCovDlg::SetData(long lpSdgu,CStringArray &opBasicShiftViewList,CString opSelView,CTime opStartTime, CTime opEndTime );
	void CAutoCovDlg::UpdateData(long lpSdgu,CStringArray &opBasicShiftViewList,CString opSelView);
	void CAutoCovDlg::GetData(CStringArray &opSelectViewList,CTime &opStartTime, CTime &opEndTime);
	BOOL UpdateData(BOOL bSaveAndValidate=TRUE);	
	CDialog::UpdateData;	

	CTime GetStartTime();

	CTime GetEndTime();
 
	bool GetAdaptionFlag();

	int  GetDirection();

	int  GetShiftSelection();

	int  GetShiftDeviation();

	int  GetAdaptIterations();

	int  GetFactor();

	void SetMaxDuration(int ipMaxDur);

	int	 GetMinShiftCoverage();

	bool UseBaseCoverage();

// Dialog Data
	//{{AFX_DATA(CAutoCovDlg)
	enum { IDD = IDD_AUTOCOVDLG };
	CCSEdit	m_AssignEndDate;
	CCSEdit	m_AssignStartDate;
	CCSEdit	m_AssignEndTime;
	CCSEdit	m_AssignStartTime;
	int		m_Adaption;
	int		m_Direction;
	int		m_ShiftSelection;
	int		m_ShiftDeviation;
	int		m_AdaptIterations;
	int		m_Factor;
	int		m_MinShiftCoverage;
	BOOL	m_UseBaseCoverage;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAutoCovDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation

protected:

	// Generated message map functions
	//{{AFX_MSG(CAutoCovDlg)
	afx_msg void OnAdd();
	afx_msg void OnRemove();
	afx_msg void OnGridEndEditing(WPARAM wParam, LPARAM lParam);
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnOvercov();
	afx_msg void OnUndercov();
	afx_msg void OnInternal();
	afx_msg void OnFastadapt();
	afx_msg void OnSimpleadapt();
	afx_msg void OnEvoluate();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	BOOL			FindShift(const CString& ropShift,CStringArray& ropShiftList);
	void			ActivateAdaption(int ipAdaption);
private:
	int				imColCount;
	CStringArray	omPossibleList;
	CStringArray	omSelectedList;
	CGridFenster*	pomPossilbeList;
	CGridFenster*	pomSelectedList;
	CTime			omStartTime;
	CTime			omEndTime;
	int				imMaxDuration;
	int				imStandardShiftDeviation;
	int				imHighShiftDeviation;
	int				imMinStandardShiftCoverage;
	int				imMinHighShiftCoverage;
	long			lmSdgu;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AUTOCOVDLG_H__0DCCB531_1D2B_11D5_9779_0050DAE32E8C__INCLUDED_)
