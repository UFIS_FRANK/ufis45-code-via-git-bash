#if !defined(AFX_BASISSCHICHTENDLG_H__546136B1_7519_11D1_B430_0000B45A33F5__INCLUDED_)
#define AFX_BASISSCHICHTENDLG_H__546136B1_7519_11D1_B430_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// BasisschichtenDlg.h : header file
//

#include <CedaSphData.h>
#include <CCSEdit.h>
#include <CCSColorButton.h>
#include <CedaAsfData.h>
#include <CedaCotData.h>
#include <CedaBsdData.h>

//*** 09.11.99 SHA ***
#include <stgrid.h>
/////////////////////////////////////////////////////////////////////////////
// BasisschichtenDlg dialog

class BasisschichtenDlg : public CDialog
{
// Construction
public:
	BasisschichtenDlg(BSDDATA *popBsd,bool bpIsDynamic = true, CWnd* pParent = NULL);   // standard constructor

	BSDDATA *pomBsd;
	bool bmIsDynamic;

	CString omOldCode;
	bool bmChangeAction;
	CStatic *pomStatus;
// Dialog Data
	//{{AFX_DATA(BasisschichtenDlg)
	enum { IDD = IDD_BASISSCHICHTENDLG };
	CCSEdit	m_VATO_T;
	CCSEdit	m_VATO_D;
	CCSEdit	m_VAFR_T;
	CCSEdit	m_VAFR_D;
	CCSEdit	m_DPT1;
	CCSEdit	m_RGSB;
	CCSEdit	m_FCTC;
	CButton	m_BKR1_S;
	CButton	m_BKR1_A;
	CButton	m_OK;
	CButton m_DYN;
	CButton m_STAT;
	CButton m_EXTRA;
	CCSEdit	m_BKD1;
	CCSEdit	m_BKF1;
	CCSEdit	m_BKT1;
	CCSEdit	m_BSDC;
	CCSEdit	m_BSDK;
	CCSEdit	m_BSDN;
	CCSEdit	m_BSDS;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_CTRC1;
	CCSEdit	m_ESBG;
	CCSEdit	m_LSEN;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_REMA;
	CCSEdit	m_SDU1;
	CCSEdit	m_SEX1;
	CCSEdit	m_SSH1;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CButton	m_JUMPER;
	CString m_Caption;
	CButton	m_RGBC;
	//}}AFX_DATA

	//*** 09.11.99 SHA ***
	CSTGrid		*	omGrdDel;
	CString			omAllFctc;
	CString			omAllDpt1;
	CString			omAllSdac;
	CStringArray	olDeleteUrno;
	COLORREF		omRgbc;
	CBrush			omBrush;	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BasisschichtenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(BasisschichtenDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
    afx_msg LONG OnKillfocus(UINT wParam, LONG lParam);
	afx_msg void OnBFctc();
	afx_msg void OnBDpt1();
	afx_msg void OnBdelete();
	afx_msg void OnBneu();
	afx_msg void OnRgbc();
	afx_msg HBRUSH OnCtlColor( CDC* pDC, CWnd* pWnd, UINT nCtlColor );
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CString Gui2DBmin(CString guimin);
	CString DBmin2Gui(CString dbmin);
	CString GetRegS(CString cVon, CString cBis, CString cPause);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BASISSCHICHTENDLG_H__546136B1_7519_11D1_B430_0000B45A33F5__INCLUDED_)
