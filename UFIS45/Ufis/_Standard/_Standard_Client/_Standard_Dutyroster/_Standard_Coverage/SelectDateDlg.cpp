// SelectDateDlg.cpp : implementation file
//

#include "stdafx.h"
#include "coverage.h"
#include "SelectDateDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectDateDlg dialog


CSelectDateDlg::CSelectDateDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectDateDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectDateDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	this->omDate = -1;
	this->omFrom = -1;
	this->omTo   = -1;
	this->imOleDateWeekDay = -1;
	this->imCalCtrlWeekDay = -1;
}


void CSelectDateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectDateDlg)
	DDX_Control(pDX, IDC_MONTHCALENDAR1, m_CalCtrl);
	//}}AFX_DATA_MAP
}

void CSelectDateDlg::SetDate(const CTime& ropDate)
{
	this->omDate = ropDate;
}

CTime CSelectDateDlg::GetDate() const
{
	return this->omDate;
}


void CSelectDateDlg::SetRange(const CTime& ropFrom,const CTime ropTo)
{
	this->omFrom = ropFrom;
	this->omTo	 = ropTo;
}

void CSelectDateDlg::SetValidWeekDay(int ipWeekDay)
{
	this->imOleDateWeekDay = ipWeekDay;
	this->imCalCtrlWeekDay = ipWeekDay - 1;
}

BEGIN_MESSAGE_MAP(CSelectDateDlg, CDialog)
	//{{AFX_MSG_MAP(CSelectDateDlg)
	ON_NOTIFY(MCN_GETDAYSTATE, IDC_MONTHCALENDAR1, OnGetdaystateMonthcalendar1)
	ON_NOTIFY(MCN_SELCHANGE, IDC_MONTHCALENDAR1, OnSelchangeMonthcalendar1)
	ON_NOTIFY(MCN_SELECT, IDC_MONTHCALENDAR1, OnSelectMonthcalendar1)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_MONTHCALENDAR1, OnReleasedcaptureMonthcalendar1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectDateDlg message handlers

void CSelectDateDlg::OnGetdaystateMonthcalendar1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	NMDAYSTATE *lpNMDayState = (NMDAYSTATE *)pNMHDR;

	if (lpNMDayState->prgDayState == NULL)
	{
		*pResult = 0;
		return;
	}

	if (this->imOleDateWeekDay != -1)
	{
		int nMonths = lpNMDayState->cDayState;

		int ilMonth = -1;
		int i = -1;

		for (COleDateTime olDay = lpNMDayState->stStart; i < nMonths; olDay += COleDateTimeSpan(1,0,0,0))
		{
			if (olDay.GetDayOfWeek() == this->imOleDateWeekDay)
			{
				if (ilMonth == -1 || ilMonth != olDay.GetMonth())
				{
					ilMonth = olDay.GetMonth();
					++i;
					lpNMDayState->prgDayState[i] = 0;
				}
			lpNMDayState->prgDayState[i] |= 1 << (olDay.GetDay() - 1);
			}
		}
	}

	*pResult = 0;
} 

void CSelectDateDlg::OnSelchangeMonthcalendar1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	NMSELCHANGE *lpNMSelChange = (NMSELCHANGE *) pNMHDR;
	COleDateTime olDay = lpNMSelChange->stSelStart;
	if (this->imOleDateWeekDay != -1 && olDay.GetDayOfWeek() != this->imOleDateWeekDay)
	{
		*pResult = 1;
	}
	else
	{
		this->omDate = lpNMSelChange->stSelStart;
		*pResult = 0;
	}
}

void CSelectDateDlg::OnSelectMonthcalendar1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	NMSELCHANGE *lpNMSelChange = (NMSELCHANGE *) pNMHDR;
	COleDateTime olDay = lpNMSelChange->stSelStart;
	if (this->imOleDateWeekDay != -1 && olDay.GetDayOfWeek() != this->imOleDateWeekDay)
	{
		this->GetDlgItem(IDOK)->EnableWindow(FALSE);
		*pResult = 1;
	}
	else
	{
		this->omDate = lpNMSelChange->stSelStart;
		this->GetDlgItem(IDOK)->EnableWindow(TRUE);
		*pResult = 0;
	}
}

void CSelectDateDlg::OnReleasedcaptureMonthcalendar1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	*pResult = 0;
}

void CSelectDateDlg::OnOK() 
{
	// TODO: Add extra validation here
	CDialog::OnOK();
}

BOOL CSelectDateDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	if (this->omDate != -1)
		this->m_CalCtrl.SetCurSel(this->omDate);

	if (this->omFrom != -1 && this->omTo != -1)
	{
		this->m_CalCtrl.SetRange(&this->omFrom,&this->omTo);
	}
	
	this->GetDlgItem(IDOK)->EnableWindow(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
