// AutoCoverage.cpp: implementation of the CAutoCoverage class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <Coverage.h>
#include <AutoCoverage.h>
#include <math.h>
#include <CCSGlobl.h>
#include <CCSParam.h>
#include <ACProgressBar.h>
#include <basicdata.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
static CString CreateCurrentTimeString()
{
	CTime olTime;
	olTime = CTime::GetCurrentTime();
	return olTime.Format("%Y.%m.%d %H:%M:%S");
}

static void CreateBaseShiftMapFromArray(CCSPtrArray<BASESHIFT>& ropBaseShifts,CMapStringToPtr& ropBaseShiftMap)
{
	ropBaseShiftMap.RemoveAll();

	for (int i = 0; i < ropBaseShifts.GetSize(); i++)
	{
		ropBaseShiftMap.SetAt(ropBaseShifts[i].Code,&ropBaseShifts[i]);
	}
}

static int CompareBaseShifts(const BASESHIFT **e1, const BASESHIFT **e2)
{
	if ((**e1).Start < (**e2).Start)
		return -1;
	else if ((**e1).Start > (**e2).Start)
		return 1;
	else
	{
		if ((**e1).End < (**e2).End)
			return -1;
		else if ((**e1).End > (**e2).End)
			return 1;
		else
			return 0;
	}
}


CAutoCoverage::CAutoCoverage()
:omShiftData(TRUE)
{
	pomShiftGroups = NULL;
	omOptShifts.RemoveAll();
	omActShifts.RemoveAll();
	dmBestVal = 0;
	bmInit = true;
	bmEvaluateBreaks = true;
	imEvaluateBreaks = 0;

	CString olCustomer = ogCCSParam.GetParamValue("ROSTER","ID_PROD_CUSTOMER",CTime::GetCurrentTime().Format("%Y%m%d%H%M%S"),true);

	if ("ADR" == olCustomer)
	{
		bmEvaluateSecondShift = true;
	}
	else if ("SIN" == olCustomer)
	{
		bmEvaluateSecondShift = false;
	}
	else
	{
		bmEvaluateSecondShift = true;
	}

	char pclConfigPath[142];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

#if	0
	char pclEvaluateBreaks[256];
	GetPrivateProfileString(pcgAppName, "EVALUATEBREAKS", "1",pclEvaluateBreaks, sizeof pclEvaluateBreaks, pclConfigPath);

	imEvaluateBreaks = atoi(pclEvaluateBreaks);
#else
	imEvaluateBreaks = 1;
#endif
}

CAutoCoverage::~CAutoCoverage()
{

}

void	CAutoCoverage::IgnoreBreaks(bool bpIgnoreBreaks)
{
	this->bmEvaluateBreaks = !bpIgnoreBreaks;
}

bool	CAutoCoverage::IgnoreBreaks()
{
	return !this->bmEvaluateBreaks;
}

// ropDemData		- demand curve - count per minute of how many demands there are
// ropShiftCovData	- list of existing shifts for the demand curve
// ropBaseShifts	- shifts which have the correct function (or none) for the demand curve already adjusted with offsets etc.
// popShiftGroups	- params defined in "Advanced Approximation"
// opFctc			- the demands have been previously split up according to function
void CAutoCoverage::InitValues(CUIntArray &ropDemData,CUIntArray &ropShiftCovData,
						CCSPtrArray <BASESHIFT> &ropBaseShifts,CBaseShiftsGroups *popShiftGroups,CString opFctc)
{
	pomShiftGroups = popShiftGroups;

	omOptShifts.RemoveAll();
	omActShifts.RemoveAll();

	omInDemData.InitData(ropDemData);
	omOutShiftCovData.InitData(ropShiftCovData);
	omShiftData.InitBaseShifts(ropBaseShifts, opFctc);

	bmInit = true;
}


int	 CAutoCoverage::SelectBaseShifts(CBaseShifts *pmpShiftData,bool bpForward,long lpShiftDeviation,int ipCalcTime)
{
	int ilDiff2;
	int ilSelected = 0;

	// loop through all shifts and check if they are suitable
	for (int illc2= 0; illc2 < pmpShiftData->GetShiftCount();illc2++)
	{
		BASESHIFT *prlShift = &pmpShiftData->omBaseShifts[illc2];

		prlShift->Selected = false;
		prlShift->Fitness  = 0;

		// calculate the difference between the current time being checked and the
		// start if the shift (left-right) or the end of the shift (right-left)
		if(bpForward)
		{				
			// the commented out lines means that the shift can be smaller than the demand curve, 
			// this results in corners which are not covered
			// ilDiff2 = abs(prlShift->Start - ilCalcTime);
			// if(ilDiff2 <= lpShiftDeviation)

			ilDiff2 = prlShift->Start - ipCalcTime;
			if (ilDiff2 < 0 || ilDiff2 <= lpShiftDeviation)
//			if (abs(ilDiff2) <= lpShiftDeviation)
			{
				prlShift->Selected = true;
				++ilSelected;
			}
		}
		else
		{
			//	ilDiff2 = abs(prlShift->End - ilCalcTime);
			//	if(ilDiff2 < lpShiftDeviation)

			ilDiff2 = prlShift->End - ipCalcTime;
			if (ilDiff2 >= 0 && ilDiff2 <= lpShiftDeviation)
			{
				prlShift->Selected = true;
				++ilSelected;
			}
		}
	}

	return ilSelected;
}

int	 CAutoCoverage::SelectBaseShifts2(CBaseShifts *pmpShiftData,bool bpForward,long lpShiftDeviation,int ipCalcTime)
{
	int ilDiff2;
	int ilSelected = 0;

	// loop through all shifts and check if they are suitable
	for (int illc2= 0; illc2 < pmpShiftData->GetShiftCount();illc2++)
	{
		BASESHIFT *prlShift = &pmpShiftData->omBaseShifts[illc2];

		prlShift->Selected = false;
		prlShift->Fitness  = 0;

		// calculate the difference between the current time being checked and the
		// start if the shift (left-right) or the end of the shift (right-left)
		if(bpForward)
		{				
			ilDiff2 = prlShift->Start - ipCalcTime;
			if (ilDiff2 < 0 || ilDiff2 <= lpShiftDeviation)
//			if (abs(ilDiff2) <= lpShiftDeviation) 
			{
				prlShift->Selected = true;
				++ilSelected;
			}
		}
		else
		{
			ilDiff2 = abs(prlShift->End - ipCalcTime);
			if (ilDiff2 < lpShiftDeviation)
			{
				prlShift->Selected = true;
				++ilSelected;
			}
		}
	}

	return ilSelected;
}

int	 CAutoCoverage::SelectBaseShifts3(CCSPtrArray<BASESHIFT>& ropShiftData,CMapStringToPtr& ropShiftMap,int ipCalcTime,CCSPtrArray<BASESHIFT>& ropSelectedBaseShifts)
{
	int ilSelected = 0;
	void *polValue = NULL;

	// loop through all shifts and check if they are suitable
	for (int illc2= 0; illc2 < ropShiftData.GetSize();illc2++)
	{
		BASESHIFT *prlShift = &ropShiftData[illc2];

		// initialize shift
		prlShift->Selected = false;
		prlShift->Fitness  = 0;

		if (ipCalcTime >= prlShift->Start && ipCalcTime <= prlShift->End && ropShiftMap.Lookup(prlShift->Code,polValue) == FALSE)
		{
			ropSelectedBaseShifts.Add(prlShift);
			prlShift->Selected = true;
			++ilSelected;
		}
		else if (prlShift->Start <= ipCalcTime)
		{
			prlShift->Selected = false;
		}
		else
		{
			break;
		}
	}

	return ilSelected;
}


/***
int CAutoCoverage::CalcDemDiff(CInputData *pmpInDemData,CInputData *pmpOutShiftCovData,bool bpUnderCover,double dpFactor,BASESHIFT *prpShift,int ipShiftOffSet,int& ipStartPos,int& ipEndPos,int ipBreakStart,int ipBreakOffSet,int ipBreakEnd)
{
	int ilDemDiff	= 0;
	int ilCredit	= 0;

	if (ipStartPos < 0)
	{
		ilCredit+= ipStartPos;
		ipStartPos = 0;
	}

	if (ipEndPos > pmpInDemData->GetInputSize())
	{
		ilCredit+= (pmpInDemData->GetInputSize() - ipEndPos);
		ipEndPos = pmpInDemData->GetInputSize();
	}


	for(int illc3 = ipStartPos; ((illc3 < ipBreakStart + ipBreakOffSet) && (illc3 < pmpInDemData->GetInputSize())) ;illc3++)
	{
		if (bpUnderCover)
		{
			ilDemDiff+= ((int)floor (pmpInDemData->Data[illc3]*dpFactor) - (pmpOutShiftCovData->Data[illc3] + 1));
//			ilDemDiff+= ((int)floor (pmpInDemData->Data[illc3]*dpFactor) - pmpOutShiftCovData->Data[illc3]);
		}
		else
		{
			int ilDem = (int)floor (pmpInDemData->Data[illc3]*dpFactor);
			int ilShift = pmpOutShiftCovData->Data[illc3];
			if((ilDem - (ilShift +1)) >= 0)
			{
				ilDemDiff++;
			}
			else
			{
				ilDemDiff--; 
			}
		}
	}

	int ilNewStart = max(0,ipBreakEnd + ipBreakOffSet);
	for(illc3 = ilNewStart; illc3 < ipEndPos;illc3++)
	{
		// two different methods here of calculating the suitability of the shift
		// one for high coverage the other for standard
		if (bpUnderCover)
		{
			// calculates the surface area below and above the demand curve and adds them all together
			ilDemDiff+=  ((int)floor (pmpInDemData->Data[illc3]*dpFactor) - (pmpOutShiftCovData->Data[illc3] +1));
//			ilDemDiff+=  ((int)floor (pmpInDemData->Data[illc3]*dpFactor) - pmpOutShiftCovData->Data[illc3]);
		}
		else
		{
			// ilDemDiff - increment if the shift is below the demand curve, decrement if it is above the curve
			// this gives a value which indicates how suitable the shift is
			int ilDem = (int)floor (pmpInDemData->Data[illc3]*dpFactor);
			int ilShift = pmpOutShiftCovData->Data[illc3];
			if((ilDem - (ilShift + 1)) >= 0)
			{
				ilDemDiff++;
			}
			else
			{
				ilDemDiff--;
			}
		}
	}

	ilDemDiff += ilCredit;
	if(bpUnderCover)
	{
		// length of the shift
		int ilTimeDiff = ipEndPos - ipStartPos;
		if (ilTimeDiff > 0)
		{
			// percent
			ilDemDiff = (int)floor((double)ilDemDiff/ilTimeDiff);
		}
	}
	else
	{
		// can set this as a parameter
//??		ilDemDiff +=30;
			// percent
			int ilTimeDiff = ipEndPos - ipStartPos;
			ilDemDiff = (int)floor(((double)ilDemDiff/ilTimeDiff)*100.0e0);
	}

	return ilDemDiff;
}

****/

int CAutoCoverage::CalcDemDiff(CInputData *pmpInDemData,CInputData *pmpOutShiftCovData,bool bpUnderCover,double dpFactor,BASESHIFT *prpShift,int ipShiftOffSet,int& ipStartPos,int& ipEndPos,int ipBreakStart,int ipBreakOffSet,int ipBreakEnd,int& ipCoveredWorkingHours,int& ipEvaluationFactor,int ipCalcTime)
{
	int ilDemDiff	= 0;

	// calculate "real" length of the shift
	int ilTimeDiff = ipEndPos - ipStartPos;

	int ilCredit = 0;
	int	ilUnderCovered = 0;
	int	ilOverCovered = 0;

	ipEvaluationFactor = 0;

	if (ipStartPos < 0)
	{
		ilCredit += ipStartPos;
		ipStartPos = 0;
	}

	if (ipEndPos > pmpInDemData->GetInputSize())
	{
		ilCredit += pmpInDemData->GetInputSize() - ipEndPos; 
		ipEndPos = pmpInDemData->GetInputSize();
	}


/****
	for (int illc1 = ipCalcTime; illc1 < ipStartPos; illc1++)
	{
		int ilDem = (int)floor (pmpInDemData->Data[illc1]*dpFactor);
		int ilShift = pmpOutShiftCovData->Data[illc1];

		int ilDiff  = ilDem - ilShift;

		if (ilDiff < 0)
		{
			ilOverCovered += ilDiff;
		}
		else if (ilDiff == 0)
		{
		}
		else // if (ilDiff > 0)	
		{
			ilUnderCovered += ilDiff;
		}
	}
****/

	// subtract break time from shift length
	for (int illc3 = ipStartPos; illc3 < ipBreakEnd + ipBreakOffSet && illc3 < ipEndPos; illc3++)
	{
		if (illc3 >= ipBreakStart + ipBreakOffSet)
			ilTimeDiff--;
	}

	for(illc3 = ipStartPos; ((illc3 < ipBreakStart + ipBreakOffSet) && (illc3 < pmpInDemData->GetInputSize())) ;illc3++)
	{
		int ilDem = (int)floor (pmpInDemData->Data[illc3]*dpFactor);
		int ilShift = pmpOutShiftCovData->Data[illc3];
		int ilDiff  = ilDem - (ilShift + 1);

		if (ilDiff < 0)
		{
			ilOverCovered += ilDiff;
		}
		else if (ilDiff == 0)
		{
			ilDemDiff++;
		}
		else /* if (ilDiff > 0)	*/
		{
			ilDemDiff++;
			if (illc3 >= ipCalcTime)
				ilUnderCovered += ilDiff;
		}

/****
		if((ilDem - (ilShift + 1)) >= 0)
		{
			ilDemDiff++;
			ipEvaluationFactor += ilDem - ilShift + 1;
		}
******/
	}

	int ilNewStart = max(0,ipBreakEnd + ipBreakOffSet);
	for(illc3 = ilNewStart; illc3 < ipEndPos;illc3++)
	{
		int ilDem = (int)floor (pmpInDemData->Data[illc3]*dpFactor);
		int ilShift = pmpOutShiftCovData->Data[illc3];
		int ilDiff  = ilDem - (ilShift + 1);

		if (ilDiff < 0)
		{
			ilOverCovered += ilDiff;
		}
		else if (ilDiff == 0)
		{
			ilDemDiff++;
		}
		else /* if (ilDiff > 0)	*/
		{
			ilDemDiff++;
			if (illc3 >= ipCalcTime)
				ilUnderCovered += ilDiff;
		}

/******
		if((ilDem - (ilShift + 1)) >= 0)
		{
			ilDemDiff++;
			ipEvaluationFactor += ilDem - ilShift + 1;
		}
*******/
	}

	ipCoveredWorkingHours = ilDemDiff;
//	ipEvaluationFactor += ilOverCovered + ilUnderCovered; 
	ipEvaluationFactor += ilUnderCovered; 
	ipEvaluationFactor += ilCredit;

	// compare with "real" length of the shift
	if (ilTimeDiff > 0)
	{
		// percent
		if (ipEvaluationFactor != 0)
			ipEvaluationFactor = (int)floor(((double)ilDemDiff/ipEvaluationFactor)*100.0e0);
		else
			ipEvaluationFactor = 100;

		ilDemDiff = (int)floor(((double)ilDemDiff/ilTimeDiff)*100.0e0);
	}

	return ilDemDiff;
}


double CAutoCoverage::CalcDemDiff3(CInputData *pmpInDemData,CInputData *pmpOutShiftCovData,double dpFactor,BASESHIFT *prpShift,int ipShiftOffSet,int& ipStartPos,int& ipEndPos,int ipBreakStart,int ipBreakOffSet,int ipBreakEnd,int& ipCoveredWorkingHours,double& dpEvaluationFactor,int ipCalcTime)
{
	double dlDemDiff = 0;

	// calculate "real" length of the shift
	int ilTimeDiff = ipEndPos - ipStartPos;

	int ilCredit = 0;
	int	ilUnderCovered = 0;
	int	ilOverCovered = 0;

	dpEvaluationFactor = 0;

	// prevent from underflow and overflow exception
	if (ipStartPos < 0)
	{
		ilCredit += ipStartPos;
		ipStartPos = 0;
	}

	if (ipEndPos > pmpInDemData->GetInputSize())
	{
		ilCredit += pmpInDemData->GetInputSize() - ipEndPos; 
		ipEndPos = pmpInDemData->GetInputSize();
	}


	if (this->bmEvaluateBreaks)
	{
		// subtract break time from shift length
		for (int illc3 = ipStartPos; illc3 < ipBreakEnd + ipBreakOffSet && illc3 < ipEndPos; illc3++)
		{
			if (illc3 >= ipBreakStart + ipBreakOffSet)
				ilTimeDiff--;
		}

		for(illc3 = ipStartPos; ((illc3 < ipBreakStart + ipBreakOffSet) && (illc3 < pmpInDemData->GetInputSize())) ;illc3++)
		{
			int ilDem = (int)floor (pmpInDemData->Data[illc3]*dpFactor);
			int ilShift = pmpOutShiftCovData->Data[illc3];
			int ilDiff  = ilDem - (ilShift + 1);

			if (illc3 < ipCalcTime)
			{
				ilOverCovered += ilDiff;
			}
			else if (ilDiff < 0)
			{
				ilOverCovered += ilDiff;
			}
			else if (ilDiff == 0)
			{
				dlDemDiff++;
			}
			else /* if (ilDiff > 0)	*/
			{
				dlDemDiff++;
				ilUnderCovered += ilDiff;
			}

		}

		int ilNewStart = max(0,ipBreakEnd + ipBreakOffSet);
		for(illc3 = ilNewStart; illc3 < ipEndPos;illc3++)
		{
			int ilDem = (int)floor (pmpInDemData->Data[illc3]*dpFactor);
			int ilShift = pmpOutShiftCovData->Data[illc3];
			int ilDiff  = ilDem - (ilShift + 1);

			if (illc3 < ipCalcTime)
			{
				ilOverCovered += ilDiff;
			}
			else if (ilDiff < 0)
			{
				ilOverCovered += ilDiff;
			}
			else if (ilDiff == 0)
			{
				dlDemDiff++;
			}
			else /* if (ilDiff > 0)	*/
			{
				dlDemDiff++;
				ilUnderCovered += ilDiff;
			}

		}
	}
	else
	{
		for(int illc3 = ipStartPos; illc3 < ipEndPos;illc3++)
		{
			int ilDem = (int)floor (pmpInDemData->Data[illc3]*dpFactor);
			int ilShift = pmpOutShiftCovData->Data[illc3];
			int ilDiff  = ilDem - (ilShift + 1);

			if (illc3 < ipCalcTime)
			{
				ilOverCovered += ilDiff;
			}
			else if (ilDiff < 0)
			{
				ilOverCovered += ilDiff;
			}
			else if (ilDiff == 0)
			{
				dlDemDiff++;
			}
			else /* if (ilDiff > 0)	*/
			{
				dlDemDiff++;
				ilUnderCovered += ilDiff;
			}
		}
	}

	ipCoveredWorkingHours = (int)dlDemDiff;
//	dpEvaluationFactor = abs(ilOverCovered) + abs(ilUnderCovered); 
	dpEvaluationFactor = abs(ilOverCovered); 
	dpEvaluationFactor += abs(ilCredit);

	// compare with "real" length of the shift
	if (ilTimeDiff > 0)
	{
		if (dpEvaluationFactor == 0)
			dpEvaluationFactor = 1;

		dpEvaluationFactor = (dlDemDiff / dpEvaluationFactor) / ilTimeDiff * 100.0e0;

		dlDemDiff = dlDemDiff / ilTimeDiff * 100.0e0;
	}

	return dlDemDiff;
}

bool CAutoCoverage::CalcCoverage(bool bpForward,bool bpToggle,bool bpOverCover,bool bpUnderCover,
								 long lpShiftDeviation,double dpFactor,int ilAdaption,int ipMinShiftCoverage,bool bpUseBaseCoverage)
{

	if (ogBasicData.DebugAutoCoverage())
	{
		FILE *fp = fopen(ogAutoCoverageLog,"wt");
		if (fp != NULL)
		{
			fprintf(fp,"CalculateCoverage start\nParams: Forward=[%d],Toogle=[%d],OverCover=[%d],UnderCover=[%d],ShiftDeviation = [%d],Factor=[%lf],Adaption=[%d],MinCoverage=[%d],UseBaseCoverage=[%d]\n",
						bpForward,bpToggle,bpOverCover,bpUnderCover,lpShiftDeviation,dpFactor,ilAdaption,ipMinShiftCoverage,bpUseBaseCoverage);
			fclose(fp);
		}

	}

	if (!bpUseBaseCoverage)
	{
		switch(ilAdaption)
		{
		case 0:
			return false;
		break;
		case 1:
			return CalcCoverage(&omInDemData,&omOutShiftCovData,
							&omShiftData,pomShiftGroups,
							bpForward,bpToggle,bpOverCover,bpUnderCover,
							lpShiftDeviation,dpFactor,ipMinShiftCoverage);
		break;
		case 2:
			return CalcCoverage2(&omInDemData,&omOutShiftCovData,
							&omShiftData,pomShiftGroups,
							bpForward,bpToggle,bpOverCover,bpUnderCover,
							lpShiftDeviation,dpFactor,ipMinShiftCoverage);
		break;
		}
	}
	else
	{
		switch(ilAdaption)
		{
		case 0:
			return CalcBaseCoverage3(&omInDemData,&omOutShiftCovData,
							&omShiftData,pomShiftGroups,
							bpForward,bpToggle,bpOverCover,bpUnderCover,
							lpShiftDeviation,dpFactor,ipMinShiftCoverage);
		break;
		case 1:
			return CalcBaseCoverage(&omInDemData,&omOutShiftCovData,
							&omShiftData,pomShiftGroups,
							bpForward,bpToggle,bpOverCover,bpUnderCover,
							lpShiftDeviation,dpFactor,ipMinShiftCoverage);
		break;
		case 2:
			return CalcBaseCoverage2(&omInDemData,&omOutShiftCovData,
							&omShiftData,pomShiftGroups,
							bpForward,bpToggle,bpOverCover,bpUnderCover,
							lpShiftDeviation,dpFactor,ipMinShiftCoverage);
		break;
		}
	}

	return false;
}

//
// CalcCoverage()
// standard coverage calculation (fast adaption) without monte-carlo
//
// pmpInDemData			- demand curve to be covered
// pmpOutShiftCovData	- resultant shifts
// pmpShiftData			- shifts which have the correct function (or none) for the demand curve already adjusted with offsets etc.
// pmpShiftGroups		- params defined in "Advanced Approximation"
// bpForward			- direction in which the coverage is being performed (currently not fully implemented) SEE BELOW
// bpToggle				- if true, then bpForward will toggle for each loop thus calculating from left and right meeting in the middle
// bpOvercover
// bpUnderCover
// lpShiftDuration
// dpFactor				- how much of the demand curve is to be covered (100%=half covered/200%=fully covered)
bool CAutoCoverage::CalcCoverage(CInputData *pmpInDemData,
								 CInputData *pmpOutShiftCovData,
								 CBaseShifts *pmpShiftData,
								 CBaseShiftsGroups *pmpShiftGroups,
								 bool bpForward,
								 bool bpToggle,
								 bool bpOverCover,
								 bool bpUnderCover,
								 long lpShiftDeviation,
								 double dpFactor,
								 int ipMinShiftCoverage
								 )
{
	bool blRc = true;

	int ilIndexFw = 0;
	int ilIndexBw = pmpInDemData->GetInputSize()-1;
	int ilActIndex;
	int ilDiff = 0;
	int	ilMaxBound= pmpOutShiftCovData->GetInputSize();

	// direction in which the coverage is being performed (currently not fully implemented) this can be:
	// 1) From left to right
	// 2) from right to left
	// 3) from left and right meeting and stopping in the middle
	// 4) from left and right meeting but not stopping in the middle
	bool blForward = bpForward;

	omOptShifts.RemoveAll();

	// loop through the demand curve minute by minute
	for(int ilMinute = 0; ilMinute < pmpInDemData->GetInputSize();ilMinute++)
	{
////////////////////////////////////////////
/// HERE HERE HERE HERE HERE HERE HERE HERE check if can cross over instead of stopping in the middle
		blForward = true;
		if(blForward) // blForward is toggled below so that we move from the sides towards the middle
		{
			ilActIndex = ilIndexFw;
			ilIndexFw++; // move from left to right
		}
		else
		{
			ilActIndex = ilIndexBw;
			ilIndexBw--; // move from right to left 
		}
		if(pogProgressBarDlg != NULL)
		{
			pogProgressBarDlg->SetProgress(-1,-1);
			if (pogProgressBarDlg->CheckContinue() == false)
			{
				return false;
			}
		}

		if (ogBasicData.DumpAutoCoverage())
		{
			FILE *fp = fopen(ogAutoCoverageLog,"at");
			if (fp != NULL)
			{
				fprintf(fp,"Following demand curve will be used for current time [%d]:\n",ilActIndex);

				for (int ilMinute = 0; ilMinute < pmpInDemData->GetInputSize();ilMinute++)
				{
					fprintf(fp,"Minute [%d] Demand [%d] Covered [%d] \n",ilMinute,pmpInDemData->Data[ilMinute],pmpOutShiftCovData->Data[ilMinute]);
				}

				fclose(fp);
			}
		}

		// calculate the difference between the demand curve and shifts so far
		ilDiff= (int)floor (pmpInDemData->Data[ilActIndex]*dpFactor) - pmpOutShiftCovData->Data[ilActIndex];

		// if the currently allocated shifts are less (lower) than the demand curve requires, try to find
		// a suitable shift, for each man that is missing
		for(int illc = 0; illc < ilDiff; illc++)
		{
			int ilTime = ilActIndex;
			int ilMinTimeDist = 1000000;
			int ilDiff2 = ilMinTimeDist;
			
			/*Berechnung der Offsets*/
			int ilDayOffSet = (int)floor((double)ilTime/1440);

			// the base shifts are only defined for one day reguardless of how many days are to be calculated
			// ilShiftOffset is the count of the actual days calculated so far (ilShiftOffSet = 1440 mins per day)
			int ilShiftOffSet = ilDayOffSet * 1440;
			int ilCalcTime = ilTime - ilShiftOffSet ;
			int ilOptTime = ilCalcTime; //

			// loop through all shifts and check if they are suitable
			if (SelectBaseShifts(pmpShiftData,bpForward,lpShiftDeviation,ilCalcTime) == 0)
				continue;

			if (ogBasicData.DebugAutoCoverage())
			{
				FILE *fp = fopen(ogAutoCoverageLog,"at");
				if (fp != NULL)
				{
					fprintf(fp,"Following basic shifts will be used for demand [%d] at minute [%d:%d]\n",ilDiff,ilTime / 60,ilTime - (ilTime / 60) * 60);

					for (int illc2 = 0; illc2 < pmpShiftData->GetShiftCount();illc2++)
					{
						if (pmpShiftData->omBaseShifts[illc2].Selected)
						{
							fprintf(fp,"Base shift [%s] used\n",pmpShiftData->omBaseShifts[illc2].Code);
						}
					}

					fclose(fp);
				}
			}

			int ilDemMinDiff = -31;
			int ilOptStart;
			int ilOptEnd;
			int ilOptBreakStart;
			int ilOptBreakEnd;
			int ilOptPos;
			int ilCoveredMinWorkingHours;

			bool blShiftFound = false;
			for (int illc2= 0; illc2 < pmpShiftData->GetShiftCount();illc2++)
			{
				BASESHIFT *prlShift = &pmpShiftData->omBaseShifts[illc2];
				if(prlShift->Selected)
				{
					int ilStartPos	= prlShift->Start + ilShiftOffSet;
					int ilBreakStart= prlShift->BreakStart + ilShiftOffSet;
					int ilEndPos	= prlShift->End + 1 + ilShiftOffSet;
					int ilBreakEnd	= prlShift->BreakEnd + 1 + ilShiftOffSet;

					prlShift->ActBreakOffSet = 0;

					// pause optimization
					int ilBreakOffSet = 0;
					int ilBreakPeriodStart	= prlShift->BRStart+ilShiftOffSet;
					int ilBreakPeriodEnd	= prlShift->BREnd+ilShiftOffSet;

					ilBreakOffSet = CalcOptBreakOffSet(	ilBreakPeriodStart, ilBreakPeriodEnd,
														prlShift->BreakEnd - prlShift->BreakStart,
														(int)dpFactor, pmpInDemData, pmpOutShiftCovData);
					if (ilBreakOffSet >= 0)
					{
						prlShift->ActBreakOffSet = ilBreakOffSet;
					}
					else
					{
						ilBreakOffSet = 0;
					}

					int ilCoveredWorkingHours;
					int ilEvaluationFactor;
					int ilDemDiff = CalcDemDiff(pmpInDemData,pmpOutShiftCovData,bpUnderCover,dpFactor,prlShift,ilShiftOffSet,ilStartPos,ilEndPos,ilBreakStart,ilBreakOffSet,ilBreakEnd,ilCoveredWorkingHours,ilEvaluationFactor,ilTime);


					if (ogBasicData.DebugAutoCoverage())
					{
						FILE *fp = fopen(ogAutoCoverageLog,"at");
						if (fp != NULL)
						{
							fprintf(fp,"Base shift [%s] has calculated dem diff [%d] \n",pmpShiftData->omBaseShifts[illc2].Code,ilDemDiff);
							fclose(fp);
						}
					}


					if (ilDemDiff >= ipMinShiftCoverage)
					{
						if (pmpShiftGroups->GetShiftGroupCount() > 0)
						{
							// advanced approximation 
							double dlWeight = pmpShiftGroups->EvalGroup(prlShift->GroupKey);

							double dlOffset = fabs(ilCalcTime - prlShift->Start);
							dlOffset = 999.0e0 / 1440 * dlOffset + 1;

							if (ogBasicData.UseEvaluationFactor())
							{
								ilDemDiff = (int) ceil((double)ilEvaluationFactor*dlWeight/dlOffset);
							}
							else
							{
								ilDemDiff = (int) ceil((double)ilDemDiff*dlWeight/dlOffset);
							}

							if (ogBasicData.DebugAutoCoverage())
							{
								FILE *fp = fopen(ogAutoCoverageLog,"at");
								if (fp != NULL)
								{
									if (ogBasicData.UseEvaluationFactor())
										fprintf(fp,"Base shift [%s] has calculated fitness [%d] for evaluation factor [%d] weight [%lf] and offset [%lf]\n",pmpShiftData->omBaseShifts[illc2].Code,ilDemDiff,ilEvaluationFactor,dlWeight,dlOffset);
									else
										fprintf(fp,"Base shift [%s] has calculated fitness [%d] for weight [%lf] and offset [%lf]\n",pmpShiftData->omBaseShifts[illc2].Code,ilDemDiff,dlWeight,dlOffset);
									fclose(fp);
								}
							}

						}
	
						if (ilDemDiff > 0 && ilDemDiff > ilDemMinDiff)
						{
							ilDemMinDiff	= ilDemDiff;
							ilOptStart		= ilStartPos;
							ilOptBreakStart = ilBreakStart + ilBreakOffSet;
							ilOptBreakEnd	= max(0,ilBreakEnd + ilBreakOffSet);
							ilOptEnd		= ilEndPos;
							ilOptPos		= illc2;
							ilCoveredMinWorkingHours = ilCoveredWorkingHours;
							blShiftFound	= true;
						}
						else
						{
							int ilDummy = 4711;
						}
					}
				}
			}

			if(blShiftFound)
			{
				CString olOptShiftText = "";

				for(int illc3 = ilOptStart; illc3 < ilOptBreakStart && illc3 < ilMaxBound;illc3++)
				{
					pmpOutShiftCovData->Data[illc3]++;
				}
				for( illc3 = ilOptBreakEnd; illc3 < ilOptEnd && illc3 < ilMaxBound;illc3++)
				{
					pmpOutShiftCovData->Data[illc3]++;
				}

// count of how many times this shift has been used
				pmpShiftData->omBaseShifts[ilOptPos].Count++;

// counter for advanced approximation
				pmpShiftGroups->IncreaseActVal(pmpShiftData->omBaseShifts[ilOptPos].GroupKey,ilCoveredMinWorkingHours);

// add this shift to the final list of optimized shift allocatiobn
				olOptShiftText.Format("%ld|%d|%d|%d|%s|%d|%d",
				pmpShiftData->omBaseShifts[ilOptPos].BshUrno,
				pmpShiftData->omBaseShifts[ilOptPos].Start + ilShiftOffSet,
				pmpShiftData->omBaseShifts[ilOptPos].End + ilShiftOffSet,
				pmpShiftData->omBaseShifts[ilOptPos].ActBreakOffSet,
				pmpShiftData->omBaseShifts[ilOptPos].Fctc,
				pmpShiftGroups->GetStartOffset(pmpShiftData->omBaseShifts[ilOptPos].GroupKey),
				pmpShiftGroups->GetEndOffset(pmpShiftData->omBaseShifts[ilOptPos].GroupKey)
				);
				omOptShifts.Add(olOptShiftText);

				if (ogBasicData.DebugAutoCoverage())
				{
					FILE *fp = fopen(ogAutoCoverageLog,"at");
					if (fp != NULL)
					{
						fprintf(fp,"Base shift [%s] used with fitness [%d]\n",pmpShiftData->omBaseShifts[ilOptPos].Code,ilDemMinDiff);
						fclose(fp);
					}
				}

			}
		}

		blForward = (bpToggle)?!blForward:blForward;

	}


	return blRc;
}

// advanced coverage calculation (evolutional) with monte-carlo
bool CAutoCoverage::CalcCoverage2(CInputData *pmpInDemData,CInputData *pmpOutShiftCovData,
						CBaseShifts *pmpShiftData,CBaseShiftsGroups *pmpShiftGroups,
						bool bpForward,bool bpToggle,bool bpOverCover,bool bpUnderCover,
						long lpShiftDeviation,double dpFactor,int ipMinShiftCoverage)
{
	bool blRc = false;

	int ilIndexFw = 0;
	int ilIndexBw = pmpInDemData->GetInputSize() -1;
	int ilActIndex;
	int ilDiff;
	double dlGroupVal;
	bool blInit = true;
	bool blForward = bpForward;
	bool blIsOptimal = false;

	for(int ilCount = 0; ilCount < pmpInDemData->GetInputSize();ilCount++)
	{
		blForward = true;
		if(blForward)
		{
			ilActIndex = ilIndexFw;
			ilIndexFw++;
		}
		else
		{
			ilActIndex = ilIndexBw;
			ilIndexBw--;
		}
		if(pogProgressBarDlg != NULL)
		{
			pogProgressBarDlg->SetProgress(-1,-1);
			if (pogProgressBarDlg->CheckContinue() == false)
			{
				return false;
			}
		}


		if (ogBasicData.DumpAutoCoverage())
		{
			FILE *fp = fopen(ogAutoCoverageLog,"at");
			if (fp != NULL)
			{
				fprintf(fp,"Following demand curve will be used for current time [%d]:\n",ilActIndex);

				for (int ilMinute = 0; ilMinute < pmpInDemData->GetInputSize();ilMinute++)
				{
					fprintf(fp,"Minute [%d] Demand [%d] Covered [%d] \n",ilMinute,pmpInDemData->Data[ilMinute],pmpOutShiftCovData->Data[ilMinute]);
				}

				fclose(fp);
			}
		}


		ilDiff= (int)floor (pmpInDemData->Data[ilActIndex]*dpFactor) - pmpOutShiftCovData->Data[ilActIndex];

		for(int illc = 0; illc < ilDiff; illc++)
		{
			int ilTime = ilActIndex;
			int ilMinTimeDist = 1000000;
			
			int ilDiff2 = ilMinTimeDist;

			/*Berechnung der Offsets*/
			int ilDayOffSet = (int)floor((double)ilTime/1440);
			int ilShiftOffSet = ilDayOffSet * 1440;
			int ilCalcTime = ilTime - ilShiftOffSet ;
			int ilOptTime = ilCalcTime;
				
			// loop through all shifts and check if they are suitable
			if (SelectBaseShifts2(pmpShiftData,bpForward,lpShiftDeviation,ilCalcTime) == 0)
				continue;

			if (ogBasicData.DebugAutoCoverage())
			{
				FILE *fp = fopen(ogAutoCoverageLog,"at");
				if (fp != NULL)
				{
					fprintf(fp,"Following basic shifts will be used for demand [%d] at minute [%d:%d]\n",ilDiff,ilTime / 60,ilTime - (ilTime / 60) * 60);

					for (int illc2 = 0; illc2 < pmpShiftData->GetShiftCount();illc2++)
					{
						if (pmpShiftData->omBaseShifts[illc2].Selected)
						{
							fprintf(fp,"Base shift [%s] used\n",pmpShiftData->omBaseShifts[illc2].Code);
						}
					}

					fclose(fp);
				}
			}

			int ilDemMinDiff = 0;
			
			int ilOptBreakStart;
			int ilOptBreakEnd;
			int ilOptStart;
			int ilOptEnd;
			int ilOptPos;
			int ilCoveredMinWorkingHours;
			bool blShiftFound = false;

			for (int illc2= 0; illc2 < pmpShiftData->GetShiftCount();illc2++)
			{
				if (pmpShiftData->omBaseShifts[illc2].Selected)
				{
					int ilStartPos = pmpShiftData->omBaseShifts[illc2].Start + ilShiftOffSet;
					int ilBreakStart = pmpShiftData->omBaseShifts[illc2].BreakStart + ilShiftOffSet;
					int ilEndPos = pmpShiftData->omBaseShifts[illc2].End+1 + ilShiftOffSet;
					int ilBreakEnd = pmpShiftData->omBaseShifts[illc2].BreakEnd+1 + ilShiftOffSet;

					pmpShiftData->omBaseShifts[illc2].ActBreakOffSet = 0;

					int ilBreakOffSet = CalcOptBreakOffSet(pmpShiftData->omBaseShifts[illc2].BRStart + ilShiftOffSet,
										pmpShiftData->omBaseShifts[illc2].BREnd + ilShiftOffSet,
										pmpShiftData->omBaseShifts[illc2].BreakEnd -
										pmpShiftData->omBaseShifts[illc2].BreakStart,
										(int)dpFactor,pmpInDemData,pmpOutShiftCovData);

					if (ilBreakOffSet >= 0)
					{
						pmpShiftData->omBaseShifts[illc2].ActBreakOffSet = ilBreakOffSet;
					}
					else
					{
						ilBreakOffSet = 0;
					}


					int ilCoveredWorkingHours;
					int	ilEvaluationFactor;
					int ilDemDiff = CalcDemDiff(pmpInDemData,pmpOutShiftCovData,bpUnderCover,dpFactor,&pmpShiftData->omBaseShifts[illc2],ilShiftOffSet,ilStartPos,ilEndPos,ilBreakStart,ilBreakOffSet,ilBreakEnd,ilCoveredWorkingHours,ilEvaluationFactor,ilTime);

					if (ogBasicData.DebugAutoCoverage())
					{
						FILE *fp = fopen(ogAutoCoverageLog,"at");
						if (fp != NULL)
						{
							fprintf(fp,"Base shift [%s] has calculated dem diff [%d]\n",pmpShiftData->omBaseShifts[illc2].Code,ilDemDiff);
							fclose(fp);
						}
					}

					if (ilDemDiff >= ipMinShiftCoverage)
					{
						pmpShiftData->omBaseShifts[illc2].StartPos = ilStartPos;
						pmpShiftData->omBaseShifts[illc2].EndPos   = ilEndPos;
						pmpShiftData->omBaseShifts[illc2].BreakStartPos = ilBreakStart + ilBreakOffSet;
						pmpShiftData->omBaseShifts[illc2].BreakEndPos	= max(0,ilBreakEnd + ilBreakOffSet);
						pmpShiftData->omBaseShifts[illc2].CoveredWorkingHours = ilCoveredWorkingHours;
						
						if(pmpShiftGroups->GetShiftGroupCount() > 0)
						{
							double dlWeight = pmpShiftGroups->EvalGroup(pmpShiftData->omBaseShifts[illc2].GroupKey);

							double dlOffset = fabs(ilCalcTime - pmpShiftData->omBaseShifts[illc2].Start);
							dlOffset = 999.0e0 / 1440 * dlOffset + 1;

							if (ogBasicData.UseEvaluationFactor())
							{
								ilDemDiff = (int) ceil((double)ilEvaluationFactor*dlWeight/dlOffset);
							}
							else
							{
								ilDemDiff = (int) ceil((double)ilDemDiff*dlWeight/dlOffset);
							}

							if (ogBasicData.DebugAutoCoverage())
							{
								FILE *fp = fopen(ogAutoCoverageLog,"at");
								if (fp != NULL)
								{
									if (ogBasicData.UseEvaluationFactor())
										fprintf(fp,"Base shift [%s] has calculated fitness [%d] for evaluation factor [%d] weight [%lf] and offset [%lf]\n",pmpShiftData->omBaseShifts[illc2].Code,ilDemDiff,ilEvaluationFactor,dlWeight,dlOffset);
									else
										fprintf(fp,"Base shift [%s] has calculated fitness [%d] for weight [%lf] and offset [%lf]\n",pmpShiftData->omBaseShifts[illc2].Code,ilDemDiff,dlWeight,dlOffset);

									fclose(fp);
								}
							}

						}

						pmpShiftData->omBaseShifts[illc2].Fitness = ilDemDiff;

						if(ilDemDiff <= 0)
						{
							pmpShiftData->omBaseShifts[illc2].Selected = false;				
						}
						else
						{
							blShiftFound = true;
						}
					}
				}
			}

			if(blShiftFound)
			{	
				srand( (unsigned)time( NULL ) );
				double dlRandom = (double) rand()/RAND_MAX;
				double dlTotalFitness = 0;
				for (illc2= 0; illc2 < pmpShiftData->GetShiftCount();illc2++)
				{
					if(pmpShiftData->omBaseShifts[illc2].Selected)
					{
						dlRandom = (double) rand()/RAND_MAX;
						
						double dlFitness = (double)pmpShiftData->omBaseShifts[illc2].Fitness;
						pmpShiftData->omBaseShifts[illc2].Fitness += dlTotalFitness;
						dlTotalFitness += dlFitness;
					}
				}
					
				dlRandom *= dlTotalFitness;
				bool blNotFound = true;
				for (illc2= 0; illc2 < pmpShiftData->GetShiftCount() && blNotFound;illc2++)
				{
					if(pmpShiftData->omBaseShifts[illc2].Selected)
					{
						if(pmpShiftData->omBaseShifts[illc2].Fitness > dlRandom)
						{
							blNotFound = false;
							
							ilOptStart = pmpShiftData->omBaseShifts[illc2].StartPos;
							ilOptEnd = pmpShiftData->omBaseShifts[illc2].EndPos;
							ilOptBreakStart = pmpShiftData->omBaseShifts[illc2].BreakStartPos;
							ilOptBreakEnd = pmpShiftData->omBaseShifts[illc2].BreakEndPos;
							ilCoveredMinWorkingHours = pmpShiftData->omBaseShifts[illc2].CoveredWorkingHours;
							ilOptPos = illc2;
						}
					}
				}
				
				if(!blNotFound)
				{
					CString olOptShiftText = "";
					long olWorkingHours = 0;

					for(int illc3 = ilOptStart; illc3 < ilOptBreakStart;illc3++)
					{
						pmpOutShiftCovData->Data[illc3]++;
					}
					for( illc3 = ilOptBreakEnd; illc3 < ilOptEnd;illc3++)
					{
						pmpOutShiftCovData->Data[illc3]++;
					}

					pmpShiftData->omBaseShifts[ilOptPos].Count++;
					pmpShiftGroups->IncreaseActVal(pmpShiftData->omBaseShifts[ilOptPos].GroupKey,ilCoveredMinWorkingHours);

					olOptShiftText.Format("%ld|%d|%d|%d|%s|%d|%d",
					pmpShiftData->omBaseShifts[ilOptPos].BshUrno,
					pmpShiftData->omBaseShifts[ilOptPos].Start + ilShiftOffSet,
					pmpShiftData->omBaseShifts[ilOptPos].End + ilShiftOffSet,
					pmpShiftData->omBaseShifts[ilOptPos].ActBreakOffSet,
					pmpShiftData->omBaseShifts[ilOptPos].Fctc,
					pmpShiftGroups->GetStartOffset(pmpShiftData->omBaseShifts[ilOptPos].GroupKey),
					pmpShiftGroups->GetEndOffset(pmpShiftData->omBaseShifts[ilOptPos].GroupKey)
					);
					omActShifts.Add(olOptShiftText);

					if (ogBasicData.DebugAutoCoverage())
					{
						FILE *fp = fopen(ogAutoCoverageLog,"at");
						if (fp != NULL)
						{
							fprintf(fp,"Base shift [%s] used with fitness [%d]\n",pmpShiftData->omBaseShifts[ilOptPos].Code,pmpShiftData->omBaseShifts[ilOptPos].Fitness);
							fclose(fp);
						}
					}

				}
			}
		}

		blForward = (bpToggle)?!blForward:blForward;

	}


	ilDiff = 0;
	for( ilCount = 0; ilCount < pmpInDemData->GetInputSize();ilCount++)
	{
		ilDiff += abs((int)floor (pmpInDemData->Data[ilCount]*dpFactor) 
						- pmpOutShiftCovData->Data[ilCount]);

	}
	
	dlGroupVal = 0;

	for(ilCount = 0; ilCount < pmpShiftGroups->GetShiftGroupCount();ilCount++)
	{
		dlGroupVal += pmpShiftGroups->EvalGroup(ilCount);
	}

	blIsOptimal = false;
	if (bmInit)
	{
		dmBestVal = (dlGroupVal + 1)*ilDiff;
		blIsOptimal = true;
	}
	else if (dmBestVal > (dlGroupVal + 1)*ilDiff)
	{
		dmBestVal = (dlGroupVal + 1)*ilDiff;
		blIsOptimal = true;
	}

	bmInit = false;


	for(ilCount = 0; ilCount < pmpOutShiftCovData->GetInputSize();ilCount++)
	{
		if(blIsOptimal)
		{
			pmpOutShiftCovData->OptData[ilCount] = pmpOutShiftCovData->Data[ilCount];
		}
		pmpOutShiftCovData->Data[ilCount] = pmpOutShiftCovData->InData[ilCount];
	}

	for (ilCount = 0; ilCount < pmpShiftData->GetShiftCount();ilCount++)
	{
		if(blIsOptimal)
		{
			pmpShiftData->omBaseShifts[ilCount].OptCount = pmpShiftData->omBaseShifts[ilCount].Count;
			omOptShifts.RemoveAll();
			for(int illc = 0; illc < omActShifts.GetSize();illc++)
			{
				omOptShifts.Add(omActShifts[illc]);
			}
		}
		pmpShiftData->omBaseShifts[ilCount].Count = 0;
	}
		

	omActShifts.RemoveAll();


	for(ilCount = 0; ilCount < pmpOutShiftCovData->GetInputSize();ilCount++)
	{
		pmpOutShiftCovData->Data[ilCount] = pmpOutShiftCovData->OptData[ilCount];
	}


	for (ilCount = 0; ilCount < pmpShiftData->GetShiftCount();ilCount++)
	{
		pmpShiftData->omBaseShifts[ilCount].Count = pmpShiftData->omBaseShifts[ilCount].OptCount;
	}


	return blIsOptimal;
}


//
// CalcCoverage()
// standard coverage calculation (fast adaption) without monte-carlo
//
// pmpInDemData			- demand curve to be covered
// pmpOutShiftCovData	- resultant shifts
// pmpShiftData			- shifts which have the correct function (or none) for the demand curve already adjusted with offsets etc.
// pmpShiftGroups		- params defined in "Advanced Approximation"
// bpForward			- direction in which the coverage is being performed (currently not fully implemented) SEE BELOW
// bpToggle				- if true, then bpForward will toggle for each loop thus calculating from left and right meeting in the middle
// bpOvercover
// bpUnderCover
// lpShiftDuration
// dpFactor				- how much of the demand curve is to be covered (100%=half covered/200%=fully covered)
bool CAutoCoverage::CalcBaseCoverage(CInputData *pmpInDemData,
									 CInputData *pmpOutShiftCovData,
									 CBaseShifts *pmpShiftData,
									 CBaseShiftsGroups *pmpShiftGroups,
									 bool bpForward,
									 bool bpToggle,
									 bool bpOverCover,
									 bool bpUnderCover,
									 long lpShiftDeviation,
									 double dpFactor,
									 int ipMinShiftCoverage
									 )
{
	bool blRc = true;

	int ilActIndex;
	int ilDiff = 0;
	int	ilMaxBound= pmpOutShiftCovData->GetInputSize();

	// direction in which the coverage is being performed (currently not fully implemented) this can be:
	// 1) From left to right
	// 2) from right to left
	// 3) from left and right meeting and stopping in the middle
	// 4) from left and right meeting but not stopping in the middle
	bool blForward = bpForward;

	omOptShifts.RemoveAll();

	// calculate the peak index
	int ilPeakIndex = -1;
	for (int ilMinute = 0; ilMinute < pmpInDemData->GetInputSize();ilMinute++)
	{
		ilDiff = (int)floor (pmpInDemData->Data[ilMinute]*dpFactor);
		if (ilDiff > ilPeakIndex)
			ilPeakIndex = ilDiff;

	}

	// loop over all peaks
	for (int ilPeak = 0; ilPeak < ilPeakIndex; ilPeak++)
	{
		int ilIndexFw = 0;
		int ilIndexBw = pmpInDemData->GetInputSize()-1;
		// loop through the demand curve minute by minute
		if(pogProgressBarDlg != NULL)
		{
			pogProgressBarDlg->SetProgress(-1,-1);
			if (pogProgressBarDlg->CheckContinue() == false)
			{
				return false;
			}
		}


		if (ogBasicData.DumpAutoCoverage())
		{
			FILE *fp = fopen(ogAutoCoverageLog,"at");
			if (fp != NULL)
			{
				fprintf(fp,"Following demand curve will be used for demand [%d]:\n",ilPeak);

				for (int ilMinute = 0; ilMinute < pmpInDemData->GetInputSize();ilMinute++)
				{
					fprintf(fp,"Minute [%d] Demand [%d] Covered [%d] \n",ilMinute,pmpInDemData->Data[ilMinute],pmpOutShiftCovData->Data[ilMinute]);
				}

				fclose(fp);
			}
		}


		int ilMinIndex = -1;

		for(int ilMinute = 0; ilMinute < pmpInDemData->GetInputSize();ilMinute++)
		{
	////////////////////////////////////////////
	/// HERE HERE HERE HERE HERE HERE HERE HERE check if can cross over instead of stopping in the middle
			blForward = true;
			if(blForward) // blForward is toggled below so that we move from the sides towards the middle
			{
				ilActIndex = ilIndexFw;
				ilIndexFw++; // move from left to right
			}
			else
			{
				ilActIndex = ilIndexBw;
				ilIndexBw--; // move from right to left 
			}

			// calculate the difference between the demand curve and shifts so far
			ilDiff= (int)floor (pmpInDemData->Data[ilActIndex]*dpFactor) - pmpOutShiftCovData->Data[ilActIndex];
			if (ilDiff <= 0 || pmpOutShiftCovData->Data[ilActIndex] != ilPeak || ilMinute < ilMinIndex)
				continue;

			// if the currently allocated shifts are less (lower) than the demand curve requires, try to find
			// a suitable shift, for each man that is missing
			for(int illc = 0; illc < 1; illc++)
			{
				int ilTime = ilActIndex;
				int ilMinTimeDist = 1000000;
				int ilDiff2 = ilMinTimeDist;
				
				/*Berechnung der Offsets*/
				int ilDayOffSet = (int)floor((double)ilTime/1440);

				// the base shifts are only defined for one day reguardless of how many days are to be calculated
				// ilShiftOffset is the count of the actual days calculated so far (ilShiftOffSet = 1440 mins per day)
				int ilShiftOffSet = ilDayOffSet * 1440;
				int ilCalcTime = ilTime - ilShiftOffSet ;
				int ilOptTime = ilCalcTime; //

				// loop through all shifts and check if they are suitable
				if (SelectBaseShifts(pmpShiftData,bpForward,lpShiftDeviation,ilCalcTime) == 0)
					continue;

				if (ogBasicData.DebugAutoCoverage())
				{
					FILE *fp = fopen(ogAutoCoverageLog,"at");
					if (fp != NULL)
					{
						fprintf(fp,"Following basic shifts will be used for demand [%d] at minute [%d:%d]\n",ilPeak,ilTime / 60,ilTime - (ilTime / 60) * 60);

						for (int illc2 = 0; illc2 < pmpShiftData->GetShiftCount();illc2++)
						{
							if (pmpShiftData->omBaseShifts[illc2].Selected)
							{
								fprintf(fp,"Base shift [%s] used\n",pmpShiftData->omBaseShifts[illc2].Code);
							}
						}

						fclose(fp);
					}
				}

				int ilDemMinDiff = -31;
				int ilOptStart;
				int ilOptEnd;
				int ilOptBreakStart;
				int ilOptBreakEnd;
				int ilOptPos;
				int ilCoveredMinWorkingHours;
				bool blShiftFound = false;
				for (int illc2= 0; illc2 < pmpShiftData->GetShiftCount();illc2++)
				{
					BASESHIFT *prlShift = &pmpShiftData->omBaseShifts[illc2];
					if(prlShift->Selected)
					{
						int ilStartPos	= prlShift->Start + ilShiftOffSet;
						int ilBreakStart= prlShift->BreakStart + ilShiftOffSet;
						int ilEndPos	= prlShift->End + 1 + ilShiftOffSet;
						int ilBreakEnd	= prlShift->BreakEnd + 1 + ilShiftOffSet;

						prlShift->ActBreakOffSet = 0;

						// pause optimization
						int ilBreakOffSet = 0;
						int ilBreakPeriodStart	= prlShift->BRStart+ilShiftOffSet;
						int ilBreakPeriodEnd	= prlShift->BREnd+ilShiftOffSet;

						ilBreakOffSet = CalcOptBreakOffSet(	ilBreakPeriodStart, ilBreakPeriodEnd,
															prlShift->BreakEnd - prlShift->BreakStart,
															(int)dpFactor, pmpInDemData, pmpOutShiftCovData);
						if (ilBreakOffSet >= 0)
						{
							prlShift->ActBreakOffSet = ilBreakOffSet;
						}
						else
						{
							ilBreakOffSet = 0;
						}

						int ilCoveredWorkingHours;
						int	ilEvaluationFactor;
						int ilDemDiff = CalcDemDiff(pmpInDemData,pmpOutShiftCovData,bpUnderCover,dpFactor,prlShift,ilShiftOffSet,ilStartPos,ilEndPos,ilBreakStart,ilBreakOffSet,ilBreakEnd,ilCoveredWorkingHours,ilEvaluationFactor,ilTime);

						if (ogBasicData.DebugAutoCoverage())
						{
							FILE *fp = fopen(ogAutoCoverageLog,"at");
							if (fp != NULL)
							{
								fprintf(fp,"Base shift [%s] has calculated dem diff [%d]\n",pmpShiftData->omBaseShifts[illc2].Code,ilDemDiff);
								fclose(fp);
							}
						}

						if (ilDemDiff >= ipMinShiftCoverage)
						{
							if (pmpShiftGroups->GetShiftGroupCount() > 0)
							{
								// advanced approximation 
								double dlWeight = pmpShiftGroups->EvalGroup(prlShift->GroupKey);

								double dlOffset = fabs(ilCalcTime - prlShift->Start);
								dlOffset = 999.0e0 / 1440 * dlOffset + 1;

								if (ogBasicData.UseEvaluationFactor())
								{
									ilDemDiff = (int) ceil((double)ilEvaluationFactor*dlWeight/dlOffset);
								}
								else
								{
									ilDemDiff = (int) ceil((double)ilDemDiff*dlWeight/dlOffset);
								}

								if (ogBasicData.DebugAutoCoverage())
								{
									FILE *fp = fopen(ogAutoCoverageLog,"at");
									if (fp != NULL)
									{
										if (ogBasicData.UseEvaluationFactor())
											fprintf(fp,"Base shift [%s] has calculated fitness [%d] for evaluation factor [%d] weight [%lf] and offset [%lf]\n",pmpShiftData->omBaseShifts[illc2].Code,ilDemDiff,ilEvaluationFactor,dlWeight,dlOffset);
										else
											fprintf(fp,"Base shift [%s] has calculated fitness [%d] for weight [%lf] and offset [%lf]\n",pmpShiftData->omBaseShifts[illc2].Code,ilDemDiff,dlWeight,dlOffset);
										fclose(fp);
									}
								}

							}
		
							if (ilDemDiff > 0 && ilDemDiff > ilDemMinDiff)
							{
								ilDemMinDiff	= ilDemDiff;
								ilOptStart		= ilStartPos;
								ilOptBreakStart = ilBreakStart + ilBreakOffSet;
								ilOptBreakEnd	= max(0,ilBreakEnd + ilBreakOffSet);
								ilOptEnd		= ilEndPos;
								ilOptPos		= illc2;
								ilCoveredMinWorkingHours = ilCoveredWorkingHours;

								blShiftFound	= true;
							}
							else
							{
								int ilDummy = 4711;
							}
						}
					}
				}

				if(blShiftFound)
				{
					CString olOptShiftText = "";

					for(int illc3 = ilOptStart; illc3 < ilOptBreakStart && illc3 < ilMaxBound;illc3++)
					{
						pmpOutShiftCovData->Data[illc3]++;
						ilMinIndex = illc3;
					}

					for( illc3 = ilOptBreakEnd; illc3 < ilOptEnd && illc3 < ilMaxBound;illc3++)
					{
						pmpOutShiftCovData->Data[illc3]++;
						ilMinIndex = illc3;
					}

	// count of how many times this shift has been used
					pmpShiftData->omBaseShifts[ilOptPos].Count++;

	// counter for advanced approximation
					pmpShiftGroups->IncreaseActVal(pmpShiftData->omBaseShifts[ilOptPos].GroupKey,ilCoveredMinWorkingHours);

	/*
					int ilBreakOffSet = CalcOptBreakOffSet(pmpShiftData->omBaseShifts[ilOptPos].BRStart,
											pmpShiftData->omBaseShifts[ilOptPos].BREnd,
											pmpShiftData->omBaseShifts[ilOptPos].BreakEnd -
											pmpShiftData->omBaseShifts[ilOptPos].BreakStart,
											dpFactor,pmpInDemData,pmpOutShiftCovData);

	*/

	// add this shift to the final list of optimized shift allocatiobn
					olOptShiftText.Format("%ld|%d|%d|%d|%s|%d|%d",
					pmpShiftData->omBaseShifts[ilOptPos].BshUrno,
					pmpShiftData->omBaseShifts[ilOptPos].Start + ilShiftOffSet,
					pmpShiftData->omBaseShifts[ilOptPos].End + ilShiftOffSet,
					pmpShiftData->omBaseShifts[ilOptPos].ActBreakOffSet,
					pmpShiftData->omBaseShifts[ilOptPos].Fctc,
					pmpShiftGroups->GetStartOffset(pmpShiftData->omBaseShifts[ilOptPos].GroupKey),
					pmpShiftGroups->GetEndOffset(pmpShiftData->omBaseShifts[ilOptPos].GroupKey)
					);
					omOptShifts.Add(olOptShiftText);

					if (ogBasicData.DebugAutoCoverage())
					{
						FILE *fp = fopen(ogAutoCoverageLog,"at");
						if (fp != NULL)
						{
							fprintf(fp,"Base shift [%s] used with fitness [%d]\n",pmpShiftData->omBaseShifts[ilOptPos].Code,ilDemMinDiff);
							fclose(fp);
						}
					}

				}
			}

			blForward = (bpToggle)?!blForward:blForward;

		}

	}
	return blRc;
}

// advanced coverage calculation (evolutional) with monte-carlo
bool CAutoCoverage::CalcBaseCoverage2(CInputData *pmpInDemData,CInputData *pmpOutShiftCovData,
									CBaseShifts *pmpShiftData,CBaseShiftsGroups *pmpShiftGroups,
									bool bpForward,bool bpToggle,bool bpOverCover,bool bpUnderCover,
									long lpShiftDeviation,double dpFactor,int ipMinShiftCoverage)
{
	bool blRc = false;

	int ilActIndex;
	int ilDiff;
	double dlGroupVal;
	bool blInit = true;
	bool blForward = bpForward;
	bool blIsOptimal = false;

	// calculate the peak index
	int ilPeakIndex = -1;
	for (int ilMinute = 0; ilMinute < pmpInDemData->GetInputSize();ilMinute++)
	{
		ilDiff = (int)floor (pmpInDemData->Data[ilMinute]*dpFactor);
		if (ilDiff > ilPeakIndex)
			ilPeakIndex = ilDiff;

	}

	// loop over all peaks
	for (int ilPeak = 0; ilPeak < ilPeakIndex; ilPeak++)
	{
		int ilIndexFw = 0;
		int ilIndexBw = pmpInDemData->GetInputSize() -1;
		int ilMinIndex = -1;

		for(int ilCount = 0; ilCount < pmpInDemData->GetInputSize();ilCount++)
		{
			blForward = true;
			if(blForward)
			{
				ilActIndex = ilIndexFw;
				ilIndexFw++;
			}
			else
			{
				ilActIndex = ilIndexBw;
				ilIndexBw--;
			}
			if(pogProgressBarDlg != NULL)
			{
				pogProgressBarDlg->SetProgress(-1,-1);
				if (pogProgressBarDlg->CheckContinue() == false)
				{
					return false;
				}
			}


			ilDiff= (int)floor (pmpInDemData->Data[ilActIndex]*dpFactor) - pmpOutShiftCovData->Data[ilActIndex];
			if (ilDiff <= 0 || pmpOutShiftCovData->Data[ilActIndex] != ilPeak || ilActIndex < ilMinIndex)
				continue;

			for(int illc = 0; illc < 1; illc++)
			{
				int ilTime = ilActIndex;
				int ilMinTimeDist = 1000000;
				
				int ilDiff2 = ilMinTimeDist;

				/*Berechnung der Offsets*/
				int ilDayOffSet = (int)floor((double)ilTime/1440);
				int ilShiftOffSet = ilDayOffSet * 1440;
				int ilCalcTime = ilTime - ilShiftOffSet ;
				int ilOptTime = ilCalcTime;
					
				// loop through all shifts and check if they are suitable
				if (SelectBaseShifts2(pmpShiftData,bpForward,lpShiftDeviation,ilCalcTime) == 0)
					continue;

				if (ogBasicData.DebugAutoCoverage())
				{
					FILE *fp = fopen(ogAutoCoverageLog,"at");
					if (fp != NULL)
					{
						fprintf(fp,"Following basic shifts will be used for demand [%d] at minute [%d:%d]\n",ilPeak,ilTime / 60,ilTime - (ilTime / 60) * 60);

						for (int illc2 = 0; illc2 < pmpShiftData->GetShiftCount();illc2++)
						{
							if (pmpShiftData->omBaseShifts[illc2].Selected)
							{
								fprintf(fp,"Base shift [%s] used\n",pmpShiftData->omBaseShifts[illc2].Code);
							}
						}

						fclose(fp);
					}
				}

				int ilDemMinDiff = 0;
				
				int ilOptBreakStart;
				int ilOptBreakEnd;
				int ilOptStart;
				int ilOptEnd;
				int ilOptPos;
				int ilCoveredMinWorkingHours;
				bool blShiftFound = false;

				for (int illc2= 0; illc2 < pmpShiftData->GetShiftCount();illc2++)
				{
					if (pmpShiftData->omBaseShifts[illc2].Selected)
					{
						int ilStartPos = pmpShiftData->omBaseShifts[illc2].Start + ilShiftOffSet;
						int ilBreakStart = pmpShiftData->omBaseShifts[illc2].BreakStart + ilShiftOffSet;
						int ilEndPos = pmpShiftData->omBaseShifts[illc2].End+1 + ilShiftOffSet;
						int ilBreakEnd = pmpShiftData->omBaseShifts[illc2].BreakEnd+1 + ilShiftOffSet;

						pmpShiftData->omBaseShifts[illc2].ActBreakOffSet = 0;

						int ilBreakOffSet = CalcOptBreakOffSet(pmpShiftData->omBaseShifts[illc2].BRStart + ilShiftOffSet,
											pmpShiftData->omBaseShifts[illc2].BREnd + ilShiftOffSet,
											pmpShiftData->omBaseShifts[illc2].BreakEnd -
											pmpShiftData->omBaseShifts[illc2].BreakStart,
											(int)dpFactor,pmpInDemData,pmpOutShiftCovData);

						if (ilBreakOffSet >= 0)
						{
							pmpShiftData->omBaseShifts[illc2].ActBreakOffSet = ilBreakOffSet;
						}
						else
						{
							ilBreakOffSet = 0;
						}


						int ilCoveredWorkingHours;
						int	ilEvaluationFactor;
						int ilDemDiff = CalcDemDiff(pmpInDemData,pmpOutShiftCovData,bpUnderCover,dpFactor,&pmpShiftData->omBaseShifts[illc2],ilShiftOffSet,ilStartPos,ilEndPos,ilBreakStart,ilBreakOffSet,ilBreakEnd,ilCoveredWorkingHours,ilEvaluationFactor,ilTime);

						if (ogBasicData.DebugAutoCoverage())
						{
							FILE *fp = fopen(ogAutoCoverageLog,"at");
							if (fp != NULL)
							{
								fprintf(fp,"Base shift [%s] has calculated dem diff [%d]\n",pmpShiftData->omBaseShifts[illc2].Code,ilDemDiff);
								fclose(fp);
							}
						}

						if (ilDemDiff >= ipMinShiftCoverage)
						{
							pmpShiftData->omBaseShifts[illc2].StartPos = ilStartPos;
							pmpShiftData->omBaseShifts[illc2].EndPos   = ilEndPos;
							pmpShiftData->omBaseShifts[illc2].BreakStartPos = ilBreakStart + ilBreakOffSet;
							pmpShiftData->omBaseShifts[illc2].BreakEndPos	= max(0,ilBreakEnd + ilBreakOffSet);
							pmpShiftData->omBaseShifts[illc2].CoveredWorkingHours = ilCoveredWorkingHours;
							
							if(pmpShiftGroups->GetShiftGroupCount() > 0)
							{
								double dlWeight = pmpShiftGroups->EvalGroup(pmpShiftData->omBaseShifts[illc2].GroupKey);

								double dlOffset = fabs(ilCalcTime - pmpShiftData->omBaseShifts[illc2].Start);
								dlOffset = 999.0e0 / 1440 * dlOffset + 1;

								if (ogBasicData.UseEvaluationFactor())
								{
									ilDemDiff = (int) ceil((double)ilEvaluationFactor*dlWeight/dlOffset);
								}
								else
								{
									ilDemDiff = (int) ceil((double)ilDemDiff*dlWeight/dlOffset);
								}

								if (ogBasicData.DebugAutoCoverage())
								{
									FILE *fp = fopen(ogAutoCoverageLog,"at");
									if (fp != NULL)
									{
										if (ogBasicData.UseEvaluationFactor())
											fprintf(fp,"Base shift [%s] has calculated fitness [%d] for evaluation factor [%d] weight [%lf] and offset [%lf]\n",pmpShiftData->omBaseShifts[illc2].Code,ilDemDiff,ilEvaluationFactor,dlWeight,dlOffset);
										else
											fprintf(fp,"Base shift [%s] has calculated fitness [%d] for weight [%lf] and offset [%lf]\n",pmpShiftData->omBaseShifts[illc2].Code,ilDemDiff,dlWeight,dlOffset);
										fclose(fp);
									}
								}
							}

							pmpShiftData->omBaseShifts[illc2].Fitness = ilDemDiff;

							if(ilDemDiff <= 0)
							{
								pmpShiftData->omBaseShifts[illc2].Selected = false;				
							}
							else
							{
								blShiftFound = true;
							}
						}
					}
				}

				if(blShiftFound)
				{	
					srand( (unsigned)time( NULL ) );
					double dlRandom = (double) rand()/RAND_MAX;
					double dlTotalFitness = 0;
					for (illc2= 0; illc2 < pmpShiftData->GetShiftCount();illc2++)
					{
						if(pmpShiftData->omBaseShifts[illc2].Selected)
						{
							dlRandom = (double) rand()/RAND_MAX;
							
							double dlFitness = (double)pmpShiftData->omBaseShifts[illc2].Fitness;
							pmpShiftData->omBaseShifts[illc2].Fitness += dlTotalFitness;
							dlTotalFitness += dlFitness;
						}
					}
						
					dlRandom *= dlTotalFitness;
					bool blNotFound = true;
					for (illc2= 0; illc2 < pmpShiftData->GetShiftCount() && blNotFound;illc2++)
					{
						if(pmpShiftData->omBaseShifts[illc2].Selected)
						{
							if(pmpShiftData->omBaseShifts[illc2].Fitness > dlRandom)
							{
								blNotFound = false;
								
								ilOptStart = pmpShiftData->omBaseShifts[illc2].StartPos;;
								ilOptEnd = pmpShiftData->omBaseShifts[illc2].EndPos;
								ilOptBreakStart = pmpShiftData->omBaseShifts[illc2].BreakStartPos;
								ilOptBreakEnd = pmpShiftData->omBaseShifts[illc2].BreakEndPos;
								ilCoveredMinWorkingHours = pmpShiftData->omBaseShifts[illc2].CoveredWorkingHours;
								ilOptPos = illc2;
							}
						}
					}
					
					if(!blNotFound)
					{
						CString olOptShiftText = "";

						for(int illc3 = ilOptStart; illc3 < ilOptBreakStart;illc3++)
						{
							pmpOutShiftCovData->Data[illc3]++;
							ilMinIndex = illc3;
						}
						for( illc3 = ilOptBreakEnd; illc3 < ilOptEnd;illc3++)
						{
							pmpOutShiftCovData->Data[illc3]++;
							ilMinIndex = illc3;
						}

						pmpShiftData->omBaseShifts[ilOptPos].Count++;
						pmpShiftGroups->IncreaseActVal(pmpShiftData->omBaseShifts[ilOptPos].GroupKey,ilCoveredMinWorkingHours);

						olOptShiftText.Format("%ld|%d|%d|%d|%s|%d|%d",
						pmpShiftData->omBaseShifts[ilOptPos].BshUrno,
						pmpShiftData->omBaseShifts[ilOptPos].Start + ilShiftOffSet,
						pmpShiftData->omBaseShifts[ilOptPos].End + ilShiftOffSet,
						pmpShiftData->omBaseShifts[ilOptPos].ActBreakOffSet,
						pmpShiftData->omBaseShifts[ilOptPos].Fctc,
						pmpShiftGroups->GetStartOffset(pmpShiftData->omBaseShifts[ilOptPos].GroupKey),
						pmpShiftGroups->GetEndOffset(pmpShiftData->omBaseShifts[ilOptPos].GroupKey)
						);
						omActShifts.Add(olOptShiftText);

						if (ogBasicData.DebugAutoCoverage())
						{
							FILE *fp = fopen(ogAutoCoverageLog,"at");
							if (fp != NULL)
							{
								fprintf(fp,"Base shift [%s] used with fitness [%d]\n",pmpShiftData->omBaseShifts[ilOptPos].Code,pmpShiftData->omBaseShifts[ilOptPos].Fitness);
								fclose(fp);
							}
						}

					}
				}
			}

			blForward = (bpToggle)?!blForward:blForward;

		}
	}

	ilDiff = 0;
	for(int ilCount = 0; ilCount < pmpInDemData->GetInputSize();ilCount++)
	{
		ilDiff += abs((int)floor (pmpInDemData->Data[ilCount]*dpFactor) - pmpOutShiftCovData->Data[ilCount]);
	}
	
	dlGroupVal = 0;

	for(ilCount = 0; ilCount < pmpShiftGroups->GetShiftGroupCount();ilCount++)
	{
		dlGroupVal += pmpShiftGroups->EvalGroup(ilCount);
	}

	blIsOptimal = false;
	if (bmInit)
	{
		dmBestVal = (dlGroupVal + 1)*ilDiff;
		blIsOptimal = true;
	}
	else if (dmBestVal > (dlGroupVal + 1)*ilDiff)
	{
		dmBestVal = (dlGroupVal + 1)*ilDiff;
		blIsOptimal = true;
	}

	bmInit = false;


	for(ilCount = 0; ilCount < pmpOutShiftCovData->GetInputSize();ilCount++)
	{
		if(blIsOptimal)
		{
			pmpOutShiftCovData->OptData[ilCount] = pmpOutShiftCovData->Data[ilCount];
		}
		pmpOutShiftCovData->Data[ilCount] = pmpOutShiftCovData->InData[ilCount];
	}

	for (ilCount = 0; ilCount < pmpShiftData->GetShiftCount();ilCount++)
	{
		if(blIsOptimal)
		{
			pmpShiftData->omBaseShifts[ilCount].OptCount = pmpShiftData->omBaseShifts[ilCount].Count;
			omOptShifts.RemoveAll();
			for(int illc = 0; illc < omActShifts.GetSize();illc++)
			{
				omOptShifts.Add(omActShifts[illc]);
			}
		}
		pmpShiftData->omBaseShifts[ilCount].Count = 0;
	}
		

	omActShifts.RemoveAll();


	for(ilCount = 0; ilCount < pmpOutShiftCovData->GetInputSize();ilCount++)
	{
		pmpOutShiftCovData->Data[ilCount] = pmpOutShiftCovData->OptData[ilCount];
	}


	for (ilCount = 0; ilCount < pmpShiftData->GetShiftCount();ilCount++)
	{
		pmpShiftData->omBaseShifts[ilCount].Count = pmpShiftData->omBaseShifts[ilCount].OptCount;
	}


	return blIsOptimal;
}


bool CAutoCoverage::EvaluateBaseShift(CInputData *pmpInDemData,CInputData *pmpOutShiftCovData,CBaseShiftsGroups *pmpShiftGroups,BASESHIFT *prlShift,int ilShiftOffSet,int ilTime,double dpFactor,EVALUATEDSHIFT& ropEvaluatedShift)
{
	double dlDemMinDiff = INT_MIN;
	int ilOptStart;
	int ilOptEnd;
	int ilOptBreakStart;
	int ilOptBreakEnd;
	int ilCoveredMinWorkingHours;

	bool blShiftFound = false;

	if (prlShift->Selected)
	{
		int ilStartPos	= prlShift->Start + ilShiftOffSet;
		int ilBreakStart= prlShift->BreakStart + ilShiftOffSet;
		int ilEndPos	= prlShift->End + ilShiftOffSet;
		int ilBreakEnd	= prlShift->BreakEnd + ilShiftOffSet;
		prlShift->ActBreakOffSet = 0;

		// pause optimization
		int ilBreakOffSet = 0;
		int ilBreakPeriodStart	= prlShift->BRStart+ilShiftOffSet;
		int ilBreakPeriodEnd	= prlShift->BREnd+ilShiftOffSet;

//		if (this->bmEvaluateBreaks)
		{
			ilBreakOffSet = CalcOptBreakOffSet(	ilBreakPeriodStart, ilBreakPeriodEnd,prlShift->BreakEnd - prlShift->BreakStart,(int)dpFactor, pmpInDemData, pmpOutShiftCovData);
			if (ilBreakOffSet >= 0)
			{
				prlShift->ActBreakOffSet = ilBreakOffSet;
			}
			else
			{
				ilBreakOffSet = 0;
			}
		}

		int ilCoveredWorkingHours;
		double dlEvaluationFactor;
		double dlDemDiff = CalcDemDiff3(pmpInDemData,pmpOutShiftCovData,dpFactor,prlShift,ilShiftOffSet,ilStartPos,ilEndPos,ilBreakStart,ilBreakOffSet,ilBreakEnd,ilCoveredWorkingHours,dlEvaluationFactor,ilTime);

		if (dlDemDiff >= 0)
		{
			if (pmpShiftGroups->GetShiftGroupCount() > 0)
			{
				// advanced approximation 
				double dlWeight = pmpShiftGroups->EvalGroup(prlShift->GroupKey);
				if (ogBasicData.UseEvaluationFactor())
				{
					dlDemDiff = dlEvaluationFactor*dlWeight;
				}
				else
				{
					dlDemDiff = dlDemDiff*dlWeight;
				}

			}
		
			if (dlDemDiff > 0 && dlDemDiff > dlDemMinDiff)
			{
				dlDemMinDiff	= dlDemDiff;
				ilOptStart		= ilStartPos;
				ilOptBreakStart = ilBreakStart + ilBreakOffSet;
				ilOptBreakEnd	= max(0,ilBreakEnd + ilBreakOffSet);
				ilOptEnd		= ilEndPos;
				ilCoveredMinWorkingHours = ilCoveredWorkingHours;

				blShiftFound	= true;
			}
			else
			{
				int ilDummy = 4711;
			}
		}
	}

	if (blShiftFound)
	{
		ropEvaluatedShift.pomShift  = prlShift; 
		ropEvaluatedShift.dlDemDiff = dlDemMinDiff;
		ropEvaluatedShift.ilOptStart = ilOptStart;
		ropEvaluatedShift.ilOptBreakStart = ilOptBreakStart;
		ropEvaluatedShift.ilOptBreakEnd = ilOptBreakEnd;
		ropEvaluatedShift.ilOptEnd = ilOptEnd;
//		ropEvaluatedShift.ilOptPos = ilOptPos;
		ropEvaluatedShift.ilCoveredMinWorkingHours = ilCoveredMinWorkingHours;
	}

	return blShiftFound;

}
// simple coverage calculation
bool CAutoCoverage::CalcBaseCoverage3(CInputData *pmpInDemData,CInputData *pmpOutShiftCovData,
									CBaseShifts *pmpShiftData,CBaseShiftsGroups *pmpShiftGroups,
									bool bpForward,bool bpToggle,bool bpOverCover,bool bpUnderCover,
									long lpShiftDeviation,double dpFactor,int ipMinShiftCoverage)
{

	bool blBreaksEvaluated = false;
	bool blRc = true;

	int ilActIndex;
	int ilDiff = 0;
	int	ilMaxBound= pmpOutShiftCovData->GetInputSize();
	CCSPtrArray<BASESHIFT> olShiftData;

	olShiftData.Append(pmpShiftData->omBaseShifts);
	olShiftData.Sort(CompareBaseShifts);
	if (ogBasicData.DebugAutoCoverage())
	{
		FILE *fp = fopen(ogAutoCoverageLog,"at");
		if (fp != NULL)
		{
			fprintf(fp,"[%s] Following basic shifts will be used for automatic coverage\n",CreateCurrentTimeString());
			for (int illc2 = 0; illc2 < olShiftData.GetSize();illc2++)
			{
				fprintf(fp,"\tBase shift [%s] start at [%d] end at [%d] used\n",olShiftData[illc2].Code,olShiftData[illc2].Start,olShiftData[illc2].End);
			}
			fclose(fp);
		}
	}

	omOptShifts.RemoveAll();

	// calculate the peak index
	int ilPeakIndex = -1;
	for (int ilMinute = 0; ilMinute < pmpInDemData->GetInputSize();ilMinute++)
	{
		ilDiff = (int)floor (pmpInDemData->Data[ilMinute]*dpFactor);
		if (ilDiff > ilPeakIndex)
			ilPeakIndex = ilDiff;

	}

	// loop over all peaks
	for (int ilPeak = 0; ilPeak < ilPeakIndex; ilPeak++)
	{
		int ilIndexFw = 0;
		// loop through the demand curve minute by minute
		if (pogProgressBarDlg != NULL)
		{
			pogProgressBarDlg->SetProgress(-1,-1);
			if (pogProgressBarDlg->CheckContinue() == false)
			{
				return false;
			}
		}


		int ilMinIndex = -1;

		for(int ilMinute = 0; ilMinute < pmpInDemData->GetInputSize();ilMinute++)
		{
			ilActIndex = ilIndexFw;
			ilIndexFw++; // move from left to right

			// calculate the difference between the demand curve and shifts so far to figure out if we must do anything
			ilDiff= (int)floor (pmpInDemData->Data[ilActIndex]*dpFactor) - pmpOutShiftCovData->Data[ilActIndex];
			if (ilDiff <= 0 || pmpOutShiftCovData->Data[ilActIndex] != ilPeak || ilMinute < ilMinIndex)
				continue;


			// if the currently allocated shifts are less (lower) than the demand curve requires, try to find
			// a suitable shift, for each man that is missing
			int ilTime = ilActIndex;
			int ilMinTimeDist = 1000000;
			int ilDiff2 = ilMinTimeDist;
				
			/*Berechnung der Offsets*/
			int ilDayOffSet = (int)floor((double)ilTime/1440);

			// the base shifts are only defined for one day reguardless of how many days are to be calculated
			// ilShiftOffset is the count of the actual days calculated so far (ilShiftOffSet = 1440 mins per day)
			int ilShiftOffSet = ilDayOffSet * 1440;
			int ilCalcTime = ilTime - ilShiftOffSet;
			int ilOptTime = ilCalcTime; //

			// loop through all shifts and check if they are suitable
			CCSPtrArray<BASESHIFT> olSelectedBaseShifts;
			CMapStringToPtr	olSelectedBaseShiftMap;
			if (SelectBaseShifts3(olShiftData,olSelectedBaseShiftMap,ilCalcTime,olSelectedBaseShifts) == 0)
				continue;

			olSelectedBaseShifts.Sort(CompareBaseShifts);
			CreateBaseShiftMapFromArray(olSelectedBaseShifts,olSelectedBaseShiftMap);

			if (ogBasicData.DebugAutoCoverage())
			{
				FILE *fp = fopen(ogAutoCoverageLog,"at");
				if (fp != NULL)
				{
					fprintf(fp,"[%s]Following first basic shifts will be used for demand [%d] at minute [%d - %d:%d]\n",CreateCurrentTimeString(),ilPeak,ilDayOffSet,ilTime / 60,ilTime - (ilTime / 60) * 60);

					for (int illc2 = 0; illc2 < olSelectedBaseShifts.GetSize();illc2++)
					{
						if (olSelectedBaseShifts[illc2].Selected)
						{
							fprintf(fp,"\tBase shift [%s] used\n",olSelectedBaseShifts[illc2].Code);
						}
					}

					fclose(fp);
				}
			}


			CCSPtrArray<EVALUATEDSHIFTPAIR> olEvaluatedShiftPairs;
			for (int illc2= 0; illc2 < olSelectedBaseShifts.GetSize();illc2++)
			{
				BASESHIFT *prlShift = &olSelectedBaseShifts[illc2];
				EVALUATEDSHIFTPAIR *polEvaluatedShiftPair = new EVALUATEDSHIFTPAIR;
				bool blShiftFound = this->EvaluateBaseShift(pmpInDemData,pmpOutShiftCovData,pmpShiftGroups,prlShift,ilShiftOffSet,ilTime,dpFactor,polEvaluatedShiftPair->omEvaluated1);
				if (blShiftFound)
				{
					olEvaluatedShiftPairs.Add(polEvaluatedShiftPair);
				}
				else
				{
					delete polEvaluatedShiftPair;
				}
			}

			if (bmEvaluateSecondShift)
			{
				// save currently covered curve
				CUIntArray olShiftCovData;
				olShiftCovData.Append(pmpOutShiftCovData->Data);

				// Evaluate the following shift
				for (int illc3= 0; illc3 < olEvaluatedShiftPairs.GetSize();illc3++)
				{
					EVALUATEDSHIFTPAIR *polEvaluatedShiftPair = &olEvaluatedShiftPairs[illc3];

					EVALUATEDSHIFT& olEvaluatedShift = polEvaluatedShiftPair->omEvaluated1;

					if (this->bmEvaluateBreaks)
					{
						for(int illc3 = olEvaluatedShift.ilOptStart; illc3 < olEvaluatedShift.ilOptBreakStart && illc3 < ilMaxBound;illc3++)
						{
							pmpOutShiftCovData->Data[illc3]++;
						}

						for( illc3 = olEvaluatedShift.ilOptBreakEnd; illc3 < olEvaluatedShift.ilOptEnd && illc3 < ilMaxBound;illc3++)
						{
							pmpOutShiftCovData->Data[illc3]++;
						}
					}
					else
					{
						for(int illc3 = olEvaluatedShift.ilOptStart; illc3 < olEvaluatedShift.ilOptEnd && illc3 < ilMaxBound;illc3++)
						{
							pmpOutShiftCovData->Data[illc3]++;
						}
					}

					int ilNextTime= polEvaluatedShiftPair->omEvaluated1.ilOptEnd;
					int ilNextCalcTime = ilNextTime - ilShiftOffSet;

					int ilDiff = -1;
					if (ilNextTime < pmpInDemData->Data.GetSize() && ilNextTime < ilMaxBound)
					{
						// calculate the difference between the demand curve and shifts so far to figure out if we must do anything
						ilDiff = (int)floor (pmpInDemData->Data[ilNextTime]*dpFactor) - pmpOutShiftCovData->Data[ilNextTime];
					}

					if (ilDiff > 0 && pmpOutShiftCovData->Data[ilNextTime] == ilPeak)
					{

						// loop through all shifts and check if they are suitable
						CCSPtrArray<BASESHIFT> olSelectedSecondBaseShifts;	 
						if (SelectBaseShifts3(olShiftData,olSelectedBaseShiftMap,ilNextCalcTime,olSelectedSecondBaseShifts) == 0)
							continue;

						olSelectedSecondBaseShifts.Sort(CompareBaseShifts);

						if (ogBasicData.DebugAutoCoverage())
						{
							FILE *fp = fopen(ogAutoCoverageLog,"at");
							if (fp != NULL)
							{
								fprintf(fp,"[%s] Following second basic shifts will be used for first shift [%s] of demand [%d] at minute [%d - %d:%d]\n",CreateCurrentTimeString(),olEvaluatedShift.pomShift->Code,ilPeak,ilDayOffSet,ilTime / 60,ilTime - (ilTime / 60) * 60);

								for (int illc2 = 0; illc2 < olSelectedSecondBaseShifts.GetSize();illc2++)
								{
									if (olSelectedSecondBaseShifts[illc2].Selected)
									{
										fprintf(fp,"Base shift [%s] used\n",olSelectedSecondBaseShifts[illc2].Code);
									}
								}

								fclose(fp);
							}
						}


						for (int illc2= 0; illc2 < olSelectedSecondBaseShifts.GetSize();illc2++)
						{
							BASESHIFT *prlShift = &olSelectedSecondBaseShifts[illc2];
							EVALUATEDSHIFT *polEvaluatedShift = new EVALUATEDSHIFT;
							bool blShiftFound = this->EvaluateBaseShift(pmpInDemData,pmpOutShiftCovData,pmpShiftGroups,prlShift,ilShiftOffSet,ilNextTime,dpFactor,*polEvaluatedShift);
							if (blShiftFound)
							{
								polEvaluatedShiftPair->omEvaluated2.Add(polEvaluatedShift);
							}
							else
							{
								delete polEvaluatedShift;
							}
						}
					}

					pmpOutShiftCovData->Data.RemoveAll();
					pmpOutShiftCovData->Data.Append(olShiftCovData);
						
				}


				// restore currently covered curve
				pmpOutShiftCovData->Data.RemoveAll();
				pmpOutShiftCovData->Data.Append(olShiftCovData);
			}

			// find the best match of all pairs
			int ilOptFirstShift = -1;
			int ilOptSecondShift = -1;
			double dlMinDiffFirstShift  = INT_MIN;
			double dlMinDiffSecondShift = INT_MIN;

			for (int illc4= 0; illc4 < olEvaluatedShiftPairs.GetSize();illc4++)
			{
				EVALUATEDSHIFTPAIR *polEvaluatedShiftPair = &olEvaluatedShiftPairs[illc4];

				if (ogBasicData.DebugAutoCoverage())
				{
					FILE *fp = fopen(ogAutoCoverageLog,"at");
					if (fp != NULL)
					{
						fprintf(fp,"[%s]First base shift [%s] has calculated fitness [%lf] \n",CreateCurrentTimeString(),polEvaluatedShiftPair->omEvaluated1.pomShift->Code,polEvaluatedShiftPair->omEvaluated1.dlDemDiff);
						fclose(fp);
					}
				}

				if (polEvaluatedShiftPair->omEvaluated2.GetSize() == 0)
				{
					double dlCurrDiff = polEvaluatedShiftPair->omEvaluated1.dlDemDiff;
					if (dlCurrDiff > dlMinDiffFirstShift)
					{
						dlMinDiffFirstShift = dlCurrDiff;
						ilOptFirstShift		= illc4;
						ilOptSecondShift	= -1;
					}
				}
				else
				{
					for (int illc5= 0; illc5 < polEvaluatedShiftPair->omEvaluated2.GetSize();illc5++)
					{
						EVALUATEDSHIFT& olEvaluatedShift = polEvaluatedShiftPair->omEvaluated2[illc5];

						if (ogBasicData.DebugAutoCoverage())
						{
							FILE *fp = fopen(ogAutoCoverageLog,"at");
							if (fp != NULL)
							{
								fprintf(fp,"[%s]Second base shift [%s] has calculated fitness [%lf] \n",CreateCurrentTimeString(),olEvaluatedShift.pomShift->Code,olEvaluatedShift.dlDemDiff);
								fclose(fp);
							}
						}


						double dlCurrDiffFirstShift = polEvaluatedShiftPair->omEvaluated1.dlDemDiff;
						double dlCurrDiffSecondShift= olEvaluatedShift.dlDemDiff;

						if (ilOptSecondShift >= 0)	// compare two shifts ?
						{
							if (dlCurrDiffFirstShift+dlCurrDiffSecondShift > dlMinDiffFirstShift + dlMinDiffSecondShift)
							{
								dlMinDiffFirstShift = dlCurrDiffFirstShift;
								dlMinDiffSecondShift= dlCurrDiffSecondShift;
								ilOptFirstShift = illc4;
								ilOptSecondShift= illc5;
							}
						}
						else if (dlCurrDiffFirstShift > dlMinDiffFirstShift)
						{
							dlMinDiffFirstShift = dlCurrDiffFirstShift;
							dlMinDiffSecondShift= dlCurrDiffSecondShift;
							ilOptFirstShift = illc4;
							ilOptSecondShift= illc5;
						}
					}			
				}
			}

			if (ilOptFirstShift >= 0)
			{
				EVALUATEDSHIFT olEvaluatedShift = olEvaluatedShiftPairs[ilOptFirstShift].omEvaluated1;

				if (ogBasicData.DebugAutoCoverage())
				{
					FILE *fp = fopen(ogAutoCoverageLog,"at");
					if (fp != NULL)
					{
						fprintf(fp,"[%s]First base shift [%s] used with fitness [%lf] \n",CreateCurrentTimeString(),olEvaluatedShift.pomShift->Code,olEvaluatedShift.dlDemDiff);
						fclose(fp);
					}
				}

				CString olOptShiftText = "";

				if (this->bmEvaluateBreaks)
				{
					for(int illc3 = olEvaluatedShift.ilOptStart; illc3 < olEvaluatedShift.ilOptBreakStart && illc3 < ilMaxBound;illc3++)
					{
						pmpOutShiftCovData->Data[illc3]++;
					}

					for( illc3 = olEvaluatedShift.ilOptBreakEnd; illc3 < olEvaluatedShift.ilOptEnd && illc3 < ilMaxBound;illc3++)
					{
						pmpOutShiftCovData->Data[illc3]++;
					}

					if (this->imEvaluateBreaks == 0)
					{
						ilMinIndex = olEvaluatedShift.ilOptEnd;
					}
					else if (this->imEvaluateBreaks == 1)
					{
						ilMinIndex = olEvaluatedShift.ilOptBreakStart;
					}
				}
				else
				{
					for(int illc3 = olEvaluatedShift.ilOptStart; illc3 < olEvaluatedShift.ilOptEnd && illc3 < ilMaxBound;illc3++)
					{
						pmpOutShiftCovData->Data[illc3]++;
						ilMinIndex = illc3;
					}
				}

				// count of how many times this shift has been used
				olEvaluatedShift.pomShift->Count++;

				// counter for advanced approximation
				pmpShiftGroups->IncreaseActVal(olEvaluatedShift.pomShift->GroupKey,olEvaluatedShift.ilCoveredMinWorkingHours);


				// add this shift to the final list of optimized shift allocatiobn
				olOptShiftText.Format("%ld|%d|%d|%d|%s|%d|%d",
				olEvaluatedShift.pomShift->BshUrno,
				olEvaluatedShift.pomShift->Start + ilShiftOffSet,
				olEvaluatedShift.pomShift->End + ilShiftOffSet,
				olEvaluatedShift.pomShift->ActBreakOffSet,
				olEvaluatedShift.pomShift->Fctc,
				pmpShiftGroups->GetStartOffset(olEvaluatedShift.pomShift->GroupKey),
				pmpShiftGroups->GetEndOffset(olEvaluatedShift.pomShift->GroupKey)
				);
				omOptShifts.Add(olOptShiftText);

			}


			if (ilOptSecondShift >= 0)
			{

				EVALUATEDSHIFT olEvaluatedShift = olEvaluatedShiftPairs[ilOptFirstShift].omEvaluated2[ilOptSecondShift];

				if (ogBasicData.DebugAutoCoverage())
				{
					FILE *fp = fopen(ogAutoCoverageLog,"at");
					if (fp != NULL)
					{
						fprintf(fp,"[%s]Second base shift [%s] used with fitness [%lf] \n",CreateCurrentTimeString(),olEvaluatedShift.pomShift->Code,olEvaluatedShift.dlDemDiff);
						fclose(fp);
					}
				}

				CString olOptShiftText = "";

				if (this->bmEvaluateBreaks)
				{
					for(int illc3 = olEvaluatedShift.ilOptStart; illc3 < olEvaluatedShift.ilOptBreakStart && illc3 < ilMaxBound;illc3++)
					{
						pmpOutShiftCovData->Data[illc3]++;
					}

					for( illc3 = olEvaluatedShift.ilOptBreakEnd; illc3 < olEvaluatedShift.ilOptEnd && illc3 < ilMaxBound;illc3++)
					{
						pmpOutShiftCovData->Data[illc3]++;
					}

					if (this->imEvaluateBreaks == 0)
					{
						ilMinIndex = olEvaluatedShift.ilOptEnd;
					}

				}
				else
				{
					for(int illc3 = olEvaluatedShift.ilOptStart; illc3 < olEvaluatedShift.ilOptEnd && illc3 < ilMaxBound;illc3++)
					{
						pmpOutShiftCovData->Data[illc3]++;
						ilMinIndex = illc3;
					}
				}

				// count of how many times this shift has been used
				olEvaluatedShift.pomShift->Count++;

				// counter for advanced approximation
				pmpShiftGroups->IncreaseActVal(olEvaluatedShift.pomShift->GroupKey,olEvaluatedShift.ilCoveredMinWorkingHours);


				// add this shift to the final list of optimized shift allocatiobn
				olOptShiftText.Format("%ld|%d|%d|%d|%s|%d|%d",
				olEvaluatedShift.pomShift->BshUrno,
				olEvaluatedShift.pomShift->Start + ilShiftOffSet,
				olEvaluatedShift.pomShift->End + ilShiftOffSet,
				olEvaluatedShift.pomShift->ActBreakOffSet,
				olEvaluatedShift.pomShift->Fctc,
				pmpShiftGroups->GetStartOffset(olEvaluatedShift.pomShift->GroupKey),
				pmpShiftGroups->GetEndOffset(olEvaluatedShift.pomShift->GroupKey)
				);
				omOptShifts.Add(olOptShiftText);

			}

			olEvaluatedShiftPairs.DeleteAll();

		}
	}

	return blRc;
}

void CAutoCoverage::GetAutoCovShifts(CStringArray &ropAutoCovShifts)
{
	for(int illc = 0; illc < omOptShifts.GetSize(); illc++)
	{
		ropAutoCovShifts.Add(omOptShifts[illc]);
	}
}

//
// CalcOptBreakOffSet()
//
// ipBPStart			- Break period start
// ipBPEnd				- Break period end
// ipBDuration			- break duration
// dpFactor				- how much of the demand curve is to be covered (100%=half covered/200%=fully covered)
// pmpInDemData			- demand curve - minute by minute count of demands
// pmpOutShiftCovData	- shifts currently assigned
//
int CAutoCoverage::CalcOptBreakOffSet(int ipBPStart, int ipBPEnd, int ipBDuration,int dpFactor,
									  CInputData *pmpInDemData,CInputData *pmpOutShiftCovData)
{
	int ilClusterStep = 5; // eg. if 5, reposition the breaks by moving them 5 mintues at a time
	int ilBPDuration = ipBPEnd - ipBPStart;
	int ilBlockSize = (int) ceil(((double)ilBPDuration)/((double)ilClusterStep));
	int ilPos,ilDiff;
	CUIntArray olBreakDiff;

	// loop through the demand curve
	int ilMaxBound = pmpInDemData->Data.GetSize();
	for(int illc = 0 ; illc < ilBlockSize;illc++)
	{
		ilPos = ipBPStart + illc * ilClusterStep;
		if(ilPos >= 0 && (ilPos + ipBDuration) < ilMaxBound)
		{
			ilDiff= (int)floor (pmpInDemData->Data[ilPos]*dpFactor) - pmpOutShiftCovData->Data[ilPos];
			olBreakDiff.Add(ilDiff);
		}
	}

	int ilClusterSize = (int) ceil((int)ipBDuration/ilClusterStep);
	int ilSize = olBreakDiff.GetSize();
	
	int ilMinDiff,ilTotalDiff,ilOptPos = 0;

	ilMaxBound = 0;
	for (illc = 0; illc < ilSize;illc++)
	{
		ilTotalDiff = 0;
		ilMaxBound = min(illc+ilClusterSize,ilSize);
		for(int illc2 = illc; illc2 < ilMaxBound;illc2++)
		{
			ilTotalDiff += olBreakDiff[illc2];
		}
		if(illc > 0)
		{
			if(ilTotalDiff < ilMinDiff)
			{
				ilMinDiff = ilTotalDiff;
				ilOptPos = illc;
			}
		}
		else
		{
			ilMinDiff = ilTotalDiff;
			ilOptPos = illc;
		}
	}
	int ilBreakEnd = min(ipBPEnd,ipBPStart + ilOptPos*ilClusterStep + ipBDuration);
	int ilBreakStart = ilBreakEnd - ipBDuration;
	int ilBreakOffSet = ilBreakStart - ipBPStart;

	return ilBreakOffSet;
}





