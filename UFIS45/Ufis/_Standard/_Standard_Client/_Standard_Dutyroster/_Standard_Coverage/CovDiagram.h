#if !defined(AFX_COVDIAGRAM_H__6F918F32_D53A_11D2_A189_0000B4984BBE__INCLUDED_)
#define AFX_COVDIAGRAM_H__6F918F32_D53A_11D2_A189_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CovDiagram.h : header file
//

#include <CoverageDiaViewer.h>
#include <CCSClientWnd.h>
#include <CCS3dStatic.h>
#include <CCSButtonCtrl.h>
#include <CCSTimeScale.h>
#include <CViewer.h>
#include <onflictDlg.h>
#include <SimulationSheet.h>
//#include <ScrollToolBar.h>
#include <ComboBoxBar.h>
#include <DDEML.h>

#include <MasstabDlg.h>
#include <SearchPropertySheet.h>

#include <CoverageStatistics.h>
#include <AutoCovDlg.h>

/////////////////////////////////////////////////////////////////////////////
// CoverageDiagram frame

#define IDC_TIMESCALE       0x4001

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "vscale.cpp" by Pichate May 08,96 18:15
#define IDC_VERTICALSCALE   0x4002

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "gbar.cpp" by Pichate May 08,96 18:15
#define IDC_GANTTBAR        0x4003

//#define IDD_DIAGRAM         0x4004
#define IDD_CHART           0x4005
#define IDC_CHARTBUTTON     0x4006
#define IDD_GANTT           0x4007

#define IDC_PREV            0x4008
#define IDC_NEXT            0x4009
#define IDC_ARRIVAL         0x400a
#define IDC_DEPARTURE       0x400b

#define WM_GANT_HSCROLL		(WM_USER + 11009)


enum 
{
	COV_ZEITRAUM_PAGE,
	COV_FLUGSUCHEN_PAGE,
	COV_GEOMETRIE_PAGE
};

class CoverageChart;
//@Man:
//@Memo: Coverage Diagramm
/*@Doc: 
	Mainframe of the Coverage application
*/
class CovDiagram : public CFrameWnd
{
	DECLARE_DYNCREATE(CovDiagram)
public:
	CovDiagram();           // protected constructor used by dynamic creation
	~CovDiagram();

//	CDialogBar omDialogBar;

	SimulationSheet	*pomSimulation;
	//MWO
	void UpdateTrafficLight(int ipState);
	//END MWO
	// for Multilanguage-ToolTop-Support added 
	//@ManMemo:OnToolTipText 
    /*@Doc:
		for Multilanguage-ToolTop-Support added
    */
	bool OnToolTipText(UINT, NMHDR* pNMHDR, LRESULT* pResult);

	//@ManMemo:Displays and hides the chart windows
    void PositionChild();
	//@ManMemo:Sets the starttime of the timescale
    void SetTSStartTime(CTime opTSStartTime);
	//@ManMemo:Updates the view combo box
	void UpdateComboBox();
	//@ManMemo:ChangeViewTo
	/*@Doc:
		pcpViewName = name of the selected view
		RememberPositions not used anymore
		bpIsFromHScroll = flag if the functions is called from HScroll (performance optimasation)

    */
	void ChangeViewTo(const char *pcpViewName,bool RememberPositions = true,bool bpIsFromHScroll = false);
	//@ManMemo:OnUpdatePrevNext
	/*@Doc:
		without use anymore
    */
	void OnUpdatePrevNext(void);
	void OnSetMarkTime();
	void OnSimulate();
	bool ChartWindowVisible(int ipIndex);

	void CalculateSimFlightDemands(const CUIntArray& ropFlightValues,CUIntArray& ropSimFlightValues);

	void UpdateForShiftDemandUpdate(bool bpRestoreTopIndex);

	bool GetSnapshotData(CString& ropCovr,CString& ropMain,CString& ropDema,CString& ropSdgn,CString& ropIgbr);
	int	 CalculateMaxFlightDemandPeak();
	int	 CalculateMinFlightDemandCut();

// Attributes
public:

	CoverageDiagramViewer omViewer;

	HICON m_hIcon;
	CStatusBar     m_wndStatusBar;
/*** ToolBar mit Scrollen**/	
//	CScrollRebar    m_wndToolBar;
/*** ToolBar ohne Scrollen**/
	CComboBoxBar    m_wndToolBar;


	CSliderCtrl	omTimeSlider;

    CCS3DStatic omTSDate;
	CCSButtonCtrl	omSimButton;
    CButton m_KlebeFkt;
    CCSTimeScale omTimeScale;
    
    CCSClientWnd omClientWnd;
    CStatusBar omStatusBar;
    
    CTime omStartTime;
    CTimeSpan omDuration;
    CTime omTSStartTime;
    CTimeSpan omTSDuration;
    CTimeSpan omTSInterval;
	ConflictDlg *pomConflict;
    CPtrArray omPtrArray;
    int imFirstVisibleChart;
    int imStartTimeScalePos;
    
	CString omCaptionText;
	CString omCurrTimeText;
	bool bmIsViewOpen;



public:
	BOOL bmNoUpdatesNow;
	bool bmNoGraphicUpdateNow;

// Time Band data, use the TIMENULL value for hiding the marker
public:
	void SetTimeBand(CTime opStartTime, CTime opEndTime);
	void UpdateTimeBand();
	void SetCaptionText(void);
	void SetMarkTime(CTime opStartTime, CTime opEndTime);
private:
	CTime omTimeBandStartTime;
	CTime omTimeBandEndTime;
	CStringArray omLoadTimeValues;
	bool bmShowWindows[3];

	int imWindowCount;
	CMasstabDlg omMasstab;

	SearchPropertySheet *pomSearchProbertySheetDlg;
	SearchPropertySheet *pomCreateDemProbertySheetDlg;
	CAutoCovDlg *pomAutoCoverageDlg;
	bool bmSearchIsOpen;
	int imAbsFlag;
private:
    CPoint omMaxTrackSize;
    CPoint omMinTrackSize;
    COLORREF lmBkColor;
    COLORREF lmTextColor;
    COLORREF lmHilightColor;
	bool	 bmBroadcastCheckMsg;
	int		 imBroadcastState;		// the broadcast state of the watch dog
									// 0 = initialized, 1 = data send, 2 = data received
	CString	 omImportBcFilePath;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CovDiagram)
	virtual BOOL DestroyWindow();
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

	LONG OnBcAdd(UINT /*wParam*/, LONG /*lParam*/); 

// Implementation

	// Generated message map functions
	//{{AFX_MSG(CovDiagram)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnBeenden();
	afx_msg void OnDestroy();
	afx_msg void OnAnsicht();
	afx_msg void OnPrintCoverage();
	afx_msg void OnStatistics();
	afx_msg void OnSimulation();
	afx_msg void OnConflicts();
	afx_msg void OnClose();
    afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
    afx_msg void OnPaint();
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnNextChart();
    afx_msg void OnPrevChart();
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg LONG OnDeactivateAutoCov(WPARAM wParam, LPARAM lParam);
	afx_msg LONG OnADButton(WPARAM wParam, LPARAM lParam);
    afx_msg LONG OnActivateAutoCov(WPARAM wParam, LPARAM lParam);
    afx_msg LONG OnPositionChild(WPARAM wParam, LPARAM lParam);
	afx_msg LONG UpdateGraphic(WPARAM wParam, LPARAM lParam);
	afx_msg LONG UpdateBaseShift(WPARAM wParam, LPARAM lParam);
	afx_msg LONG UpdateDemandGantt(WPARAM wParam, LPARAM lParam);
	afx_msg LONG UpdateDemandCombo(WPARAM wParam, LPARAM lParam);
    afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
	afx_msg void OnViewSelChange();
	afx_msg void OnCloseupView();
	afx_msg void OnPrint();
	afx_msg void OnTest();
	afx_msg void OnCheckDemands();
	afx_msg void OnNoResList();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnLoadtimeframe();
	afx_msg void OnMasstab();
	afx_msg void OnSearch();
	afx_msg void OnCreateDem();
	afx_msg void OnAutoCov();
	afx_msg void OnSearchExit();
    afx_msg LONG OnUpdateDemandBtn(WPARAM wParam, LPARAM lParam);
	afx_msg	void OnSetup();	
	afx_msg void OnMsgLoadTimeFrame(WPARAM wParam, LPARAM lParam);
	afx_msg void OnUpdateToolbar(WPARAM wParam, LPARAM lParam);
    afx_msg LONG SelectDemandCombo(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CovDiagram)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()

private:
			void	TranslateMenu();
	CoverageChart*	GetCoverageChart();
			void	ResizeView();
	static	void	ProcessCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
			void	ImportBc();
};

extern CovDiagram *pogCoverageDiagram;
extern CCoverageStatistics *pogCoverageStatistics;

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.


#endif // !defined(AFX_COVDIAGRAM_H__6F918F32_D53A_11D2_A189_0000B4984BBE__INCLUDED_)
