// dataset.cpp - Class for handle each user action
//
// Description:
// This module is separated from some parts of viewers in many diagrams and
// expected to contain the routines for removing dependencies among Ceda????Data
// classes.
//
// The concept is simple. If we want a new kind of job creation or any action
// that the user can perform on the system, we create a new method here, in this
// module. Then, if that action has some more consequents actions (assign a job
// to solve a demand will change the color of the flight, for example) we will
// provide that steps here, in this module too.
//
// Some actions may require the user to make an interactive decision. To aid
// such concept, the first parameter of every functions will be "popParentWindow",
// and every function here will return IDOK or IDYES on success, IDCANCEL or IDNO
// if the user cancel this operation. The difference between IDCANCEL and IDNO is
// the user may select IDCANCEL when he/she still want // in the previous dialogs if exist. But IDNO selection will totally cancel this
// operation, including the in-process dialogs if exist.
//
// Function in this module:
// (still incomplete and need keep on working).
//
//
// Written by:
// Damkerng Thammathakerngkit   July 6, 1996
//

#include <stdafx.h>

#include <DataSet.h>
#include <CCSDDX.h>
#include <CCSGlobl.h>
#include <BasicData.h>
//#include "ButtonListDlg.h"
#include <ConflictCheck.h>
#include <CedaBsdData.h>
#include <CedaSdtData.h>
#include <CedaSpfData.h>
#include <CedaStfData.h>
#include <CedaBasicData.h>
#include <CoverageDiaViewer.h>
#include <CedaPfcData.h>
#include <CedaDrrData.h>
//#include <CedaJobData.h>

//#include "UndoClasses.h"
//#include "UndoManager.h"
#include <sys/timeb.h>
#include <time.h>

extern int flag;

#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))

//UndoManager ogUndoManager(30);

char pcmExamineFlno[100];
static void ProcessFlightCf(void *popInstance, int ipDDXType,
	void *vpDataPointer, CString &ropInstanceName);


// MCU 07.28 have stolen all your macros also, you will find them doubled in conflicts.cpp
//
// To know which rank is higher in comparison of two staff, I provide a colon-separated
// string from the lowest rank to the highest rank. By searching for a string ":XX:"
// in "pclRanks", if XX is the given rank, we could easily see which rank is higher.
// By using the same technic, we could see if the given rank is a manager by searching
// for a string ":XX:" in "pclManagers".
//

// General macros for testing a point against an interval
/////////////////////////////////////////////////////////////////////////////
// DataSet -- Job creation routine

DataSet::DataSet()
{
	CCS_TRY
	ogDdx.Register((void *)this,S_FLIGHT_CHANGE,CString("FLIGHTDATA"), CString("Flight changed"),ProcessFlightCf);
	CCS_CATCH_ALL
}

DataSet::~DataSet()
{
	CCS_TRY
	ogDdx.UnRegister(this,NOTUSED);
	CCS_CATCH_ALL
}

void  ProcessFlightCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CCS_TRY
		((DataSet *)popInstance)->CheckFlightsAfterBroadcast((FLIGHTDATA *)vpDataPointer);
	CCS_CATCH_ALL
}


void DataSet::CreateSdtFromBsd(BASICSHIFTS *prpBsh, CString opPfcCode)
{
	CCS_TRY

	CWaitCursor olWait;

	BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(prpBsh->RUrno);
	if(prpBsh != NULL && prlBsd != NULL)
	{
		SDTDATA *prlSdt = new SDTDATA;
		prlSdt->Urno = ogBasicData.GetNextUrno(); 	// Eindeutige Datensatz-Nr.
		strcpy(prlSdt->Bewc, prlBsd->Bewc); 		// Bewertungsfaktor.code
		strcpy(prlSdt->Bkr1, prlBsd->Bkr1); 		// Pausenlage relativ zu Schichtbeginn oder absolut
		strcpy(prlSdt->Bsdc, prlBsd->Bsdc); 		// Code
		prlSdt->Bsdu = prlBsd->Urno;				// Basisschicht Urno
		strcpy(prlSdt->Bsdk, prlBsd->Bsdk); 		// Kurzbezeichnung
		strcpy(prlSdt->Bsdn, prlBsd->Bsdn); 		// Bezeichnung
		strcpy(prlSdt->Bsds, prlBsd->Bsds); 		// SAP-Code
		strcpy(prlSdt->Ctrc, prlBsd->Ctrc); 		// Vertragsart.code
		strcpy(prlSdt->Type, prlBsd->Type); 		// Flag (statisch/dynamisch)
		strcpy(prlSdt->Usec, ogBasicData.omUserID); 	// Anwender (Ersteller)
		strcpy(prlSdt->Useu, ""); 					// Anwender (letzte Änderung)
		CTime olFrom = CTime::GetCurrentTime();
		prlSdt->Esbg = prpBsh->From + CTimeSpan(prpBsh->DayOffSet,0,0,0);				//HourStringToDate( CString( prpBsh->Esbg), olFrom);
		prlSdt->Lsen = prpBsh->To + CTimeSpan(prpBsh->DayOffSet,0,0,0);					//HourStringToDate( CString( prpBsh->Lsen), olFrom);//olFrom + CTimeSpan(atoi(prlBsd->Sdu1)*60);				
		prlSdt->Sdu1 = atoi(prlBsd->Sdu1);				
		strcpy( prlSdt->Fctc , opPfcCode);

	
		if(strcmp(prlSdt->Type,"D")==0 && prlSdt->Sdu1 > 0)
		{
			prlSdt->Sbgi = prlSdt->Esbg;
			prlSdt->Seni = prlSdt->Sbgi + CTimeSpan(prlSdt->Sdu1*60);
		}
		if(prlSdt->Esbg > prlSdt->Lsen)
		{
			prlSdt->Lsen = prlSdt->Lsen + CTimeSpan(1, 0, 0, 0);
		}
		prlSdt->Lstu = CTime::GetCurrentTime(); 	// Datum letzte Änderung
		prlSdt->Bkf1 = prpBsh->BlFrom  + CTimeSpan(prpBsh->DayOffSet,0,0,0);				//HourStringToDate( CString( prpBsh->Bkf1), olFrom);	
		prlSdt->Bkt1 = prpBsh->BlTo  + CTimeSpan(prpBsh->DayOffSet,0,0,0);				//HourStringToDate( CString( prpBsh->Bkt1), olFrom);						
		prlSdt->Brkf = prpBsh->BreakFrom + CTimeSpan(prpBsh->DayOffSet,0,0,0);			//prlSdt->Bkf1;	//Pause von
		prlSdt->Brkt = prpBsh->BreakTo + CTimeSpan(prpBsh->DayOffSet,0,0,0);				//prlSdt->Bkf1 + CTimeSpan(atoi(prpBsh->Bkd1)*60);//Pause bis


		prlSdt->Sex1 = atoi(prlBsd->Sex1);/*[6]*/ 	// Mögliche Arbeitszeitver-längerung
		prlSdt->Ssh1 = atoi(prlBsd->Ssh1);/*[6]*/ 	// Mögliche Arbeitszeitverkürzung
		prlSdt->Bkd1 = atoi(prlBsd->Bkd1);/*[6]*/ 	// Pausenlänge
		prlSdt->Cdat = CTime::GetCurrentTime(); 	// Erstellungsdatum

		prlSdt->IsVirtuel = false;
		ogSdtData.AddSdtInternal(prlSdt);
		prlSdt->IsChanged = DATA_NEW;
		ogSdtData.UpdateSdt(prlSdt,true,false);
	}


	CCS_CATCH_ALL
}



void DataSet::CreateVirtuellSdtsBySdgu(long lpSdgu)
{

	CCS_TRY

	CWaitCursor olWait;

	CCSPtrArray <MSDDATA> olList;

	SDGDATA *prlSdg = ogSdgData.GetSdgByUrno(lpSdgu);
	if(prlSdg != NULL)
	{
        ogMsdData.GetMsdsBySdgu(lpSdgu,olList);

		prlSdg->IsSelected = true;

		for (int illc = 0; illc < olList.GetSize(); illc++)
		{			
			CreateVirtuellSdtsByMsduAndSdgu(olList[illc].Urno,lpSdgu);
		}
	}

	olList.RemoveAll();


	CCS_CATCH_ALL
}

void DataSet::CreateVirtuellSdtsByMsduAndSdgu(long lpMsdu,long lpSdgu)
{
	CCS_TRY



	CWaitCursor olWait;

    MSDDATA * prlMsd = NULL;
	static CCSPtrArray<TIMEFRAMEDATA> olTimes;
	CTimeSpan olDayOffset;


	int ilDayOffset = -999;


	SDGDATA *prlSdg = ogSdgData.GetSdgByUrno(lpSdgu);
	CTimeSpan olDisplayOffset = 0;
	if(prlSdg != NULL)
	{
		prlMsd = ogMsdData.GetMsdByUrno(lpMsdu);
	    if(prlMsd != NULL)
		{
			BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(prlMsd->Bsdu);
			if(prlBsd != NULL)
			{
				olTimes.DeleteAll();
				ogSdgData.GetTimeFrames(prlSdg->Urno, olTimes);
			
				CTime olStart = prlMsd->Esbg;
				CTime olEnd   = prlMsd->Lsen;

//				for(int j = olTimes.GetSize() -1; j >= 0; j--)
				for(int j = 0; j < olTimes.GetSize(); j++)
				{
					if(IsOverlapped(olStart, olEnd, olTimes[j].StartTime, olTimes[j].EndTime) == TRUE)
					{
						/*olSpan*/ olDayOffset = CTime(olStart.GetYear(), olStart.GetMonth(), olStart.GetDay(), 0, 0, 0) - olTimes[j].StartTime;
						ilDayOffset = /*olSpan*/olDayOffset.GetDays();
						
						break;
					}
				}
				if(ilDayOffset == -999)
				{
					//olTimes.DeleteAll();
					return;
				}
				
				olDayOffset += olDisplayOffset;
			//	olEnd   += olDisplayOffset;


				CTime olBkf1Time = prlMsd->Bkf1;
				CTime olBkt1Time = prlMsd->Bkt1;
				CTime olBrkfTime = prlMsd->Brkf;
				CTime olBrktTime = prlMsd->Brkt;

				CTimeSpan olBkf1Span = olBkf1Time - olStart;
				CTimeSpan olBkt1Span = olBkt1Time - olStart;
				CTimeSpan olBrkfSpan = olBrkfTime - olStart;
				CTimeSpan olBrktSpan = olBrktTime - olStart;

		
				CString olStartTime = CString(prlBsd->Esbg);
				CString olEndTime   = CString(prlBsd->Lsen);

				SDTDATA rlSdtTemplate;
				rlSdtTemplate.Sdgu = prlSdg->Urno; 	// Eindeutige Datensatz-Nr.
			
				strcpy( rlSdtTemplate.Fctc , prlMsd->Fctc);
				strcpy(rlSdtTemplate.Bewc, prlBsd->Bewc); 		// Bewertungsfaktor.code
				strcpy(rlSdtTemplate.Bkr1, prlBsd->Bkr1); 		// Pausenlage relativ zu Schichtbeginn oder absolut
				strcpy(rlSdtTemplate.Bsdc, prlBsd->Bsdc); 		// Code
				rlSdtTemplate.Bsdu = prlBsd->Urno;				// Basisschicht Urno
				strcpy(rlSdtTemplate.Bsdk, prlBsd->Bsdk); 		// Kurzbezeichnung
				strcpy(rlSdtTemplate.Bsdn, prlBsd->Bsdn); 		// Bezeichnung
				strcpy(rlSdtTemplate.Bsds, prlBsd->Bsds); 		// SAP-Code
				strcpy(rlSdtTemplate.Ctrc, prlBsd->Ctrc); 		// Vertragsart.code
				strcpy(rlSdtTemplate.Type, prlBsd->Type); 		// Flag (statisch/dynamisch)
				strcpy(rlSdtTemplate.Usec, ogBasicData.omUserID); 	// Anwender (Ersteller)
				strcpy(rlSdtTemplate.Useu, ogBasicData.omUserID); 	// Anwender (letzte Änderung)
				CTime olFrom = CTime::GetCurrentTime();
				rlSdtTemplate.Lstu = CTime::GetCurrentTime(); 	// Datum letzte Änderung
				
				rlSdtTemplate.Sdu1 = atoi(prlBsd->Sdu1);/*[6]*/ 	// Reguläre Schichtdauer
				rlSdtTemplate.Sex1 = atoi(prlBsd->Sex1);/*[6]*/ 	// Mögliche Arbeitszeitver-längerung
				rlSdtTemplate.Ssh1 = atoi(prlBsd->Ssh1);/*[6]*/ 	// Mögliche Arbeitszeitverkürzung
				rlSdtTemplate.Bkd1 = atoi(prlBsd->Bkd1);/*[6]*/ 	// Pausenlänge
				rlSdtTemplate.Cdat = CTime::GetCurrentTime(); 	// Erstellungsdatum

				rlSdtTemplate.IsVirtuel = true;
				CTimeSpan olSpan(0,0,0,0);
				if(strcmp(prlBsd->Esbg,prlBsd->Lsen)>0)
					olSpan = CTimeSpan(1,0,0,0);


				rlSdtTemplate.MsdUrno = lpMsdu;

				for(  j = olTimes.GetSize()-1; j >=0 ; j--)
				{
					SDTDATA *prlSdt = new SDTDATA;
					
					rlSdtTemplate.Urno = ogBasicData.GetNextUrno(); 	// Eindeutige Datensatz-Nr.
				
					rlSdtTemplate.Esbg = HourStringToDate( olStartTime, olTimes[j].StartTime + olDayOffset);
					rlSdtTemplate.Lsen = HourStringToDate( olEndTime,   olTimes[j].StartTime + olDayOffset) + olSpan;
					rlSdtTemplate.Bkf1 = rlSdtTemplate.Esbg+olBkf1Span;
					rlSdtTemplate.Bkt1 = rlSdtTemplate.Esbg+olBkt1Span;
					rlSdtTemplate.Brkf = rlSdtTemplate.Esbg+olBrkfSpan;
					rlSdtTemplate.Brkt = rlSdtTemplate.Esbg+olBrktSpan;

					rlSdtTemplate.IsVirtuel = true;

					if(strcmp(rlSdtTemplate.Type,"D")==0 && rlSdtTemplate.Sdu1>0)
					{
					CTimeSpan olShiftDura(rlSdtTemplate.Sdu1*60); // = CTimeSpan(0,ilHour,ilMin,0);
						CTimeSpan olDynDura = rlSdtTemplate.Lsen - rlSdtTemplate.Esbg;
						CTimeSpan olDiff= olDynDura.GetTotalMinutes()-rlSdtTemplate.Sdu1; //olShiftDura;
						rlSdtTemplate.Sbgi = rlSdtTemplate.Esbg + olDiff;
						rlSdtTemplate.Seni = (CTime)(rlSdtTemplate.Sbgi+olShiftDura);
					}

					*prlSdt = rlSdtTemplate;
				
					ogSdtData.AddSdtInternal(prlSdt);
					//ogSdtData.UpdateSdt(prlSdt);
					prlSdt->IsChanged = DATA_UNCHANGED;
					

				}
				olTimes.DeleteAll();
			}
		}
	}



	CCS_CATCH_ALL
}

bool DataSet::DeleteMsdByMsduAndSdgu(long lpMsdu,long lpSdgu,CTime opShiftStart,bool bpWarningDisplay, bool bpSave)
{
	CCS_TRY
	
	CString olMsdu;
	olMsdu.Format("%ld",lpMsdu);

	CWaitCursor olWait;

	SDGDATA *prlSdg = ogSdgData.GetSdgByUrno(lpSdgu);
	if(prlSdg != NULL)
	{
		if(ogBasicData.bmCheckMasterWeek && bpWarningDisplay)
		{
			if(!(opShiftStart > prlSdg->Dbeg && opShiftStart < (prlSdg->Dbeg + CTimeSpan(7,0,0,0))))
			{
				if (MessageBox(NULL, LoadStg(IDS_STRING61365), LoadStg(IDS_WARNING), MB_YESNO) == IDNO)
				{
					olWait.Restore();
					return false;
				}

				// restore pointer to ensure that it is valid after possible broadcast handling
				prlSdg = ogSdgData.GetSdgByUrno(lpSdgu);
				olWait.Restore();
			}
		}

		MSDDATA * prlMsd = ogMsdData.GetMsdByUrno(lpMsdu);
	    if(prlMsd != NULL)
		{
			if (bpSave == true)
			{
				ogMsdData.DeleteMsd(prlMsd,true,false);
			}
			else
			{
				prlMsd->IsChanged = DATA_DELETED;
			}
			ogSdtData.DeleteVirtuellSdtsByMsdu(lpMsdu);
			if (prlSdg && prlSdg->Expd == true)
			{
				prlSdg->Expd = false;
				ogSdgData.UpdateSdg(prlSdg,TRUE);
			}
		}
	}
	
		
	CCS_CATCH_ALL

	return true;
}

void DataSet::CreateMsdFromBsdAndSdg(BASICSHIFTS *prpBsh, long lpSdgu, CTime opLoadStart, CTime opLoadEnd, CString opPfcCode, bool bpSave/*=true*/)
{
	CCS_TRY
			
	long llMsdUrno = 0L;
	CTimeSpan olOneDay(1,0,0,0);
	CTimeSpan olSpan(0,0,0,0),olDayOffset;

	CWaitCursor olWait;

	int ilDayOffset = -999;

	SDGDATA *prlSdg = ogSdgData.GetSdgByUrno(lpSdgu);
	BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(prpBsh->RUrno);
	if(prpBsh != NULL && prlBsd != NULL && prlSdg != NULL)
	{
		opLoadStart += CTimeSpan(prpBsh->DayOffSet,0,0,0);
		opLoadEnd += CTimeSpan(prpBsh->DayOffSet,0,0,0);

		CTime olStart = HourStringToDate(CString(prlBsd->Esbg), opLoadStart);
		CTime olEnd   = HourStringToDate(CString(prlBsd->Lsen), opLoadStart);
		if(olStart < prlSdg->Begi)
		{
			MessageBox(NULL, LoadStg(IDS_STRING61365), LoadStg(IDS_WARNING), MB_OK);
			olWait.Restore();
			return;
		}

		if(ogBasicData.bmCheckMasterWeek)
		{
			if(!(olStart > prlSdg->Dbeg && olStart < (prlSdg->Dbeg + CTimeSpan(7,0,0,0))))
			{
				if(MessageBox(NULL, LoadStg(IDS_STRING61365), LoadStg(IDS_WARNING), MB_YESNO) == IDNO)
				{
					olWait.Restore();
					return;
				}
				else
				{
					olWait.Restore();
					prlSdg = ogSdgData.GetSdgByUrno(lpSdgu);
					if (prlSdg == NULL)
						return;
				}
			}
		}
	
		CString olStartTime = CString(prlBsd->Esbg);
		CString olEndTime   = CString(prlBsd->Lsen);

		MSDDATA *prlMsd = new MSDDATA;

		if(prlMsd != NULL)
		{
			prlMsd->Sdgu = prlSdg->Urno;
			strcpy(prlMsd->Fctc,opPfcCode);
			strcpy(prlMsd->Bewc,prlBsd->Bewc);
			strcpy(prlMsd->Bkr1,prlBsd->Bkr1);
			strcpy(prlMsd->Bsdc,prlBsd->Bsdc);
			prlMsd->Bsdu = prlBsd->Urno;
			strcpy(prlMsd->Bsdk,prlBsd->Bsdk);
			strcpy(prlMsd->Bsdn,prlBsd->Bsdn);
			strcpy(prlMsd->Bsds,prlBsd->Bsds);
			strcpy(prlMsd->Ctrc,prlBsd->Ctrc);
			strcpy(prlMsd->Type,prlBsd->Type);
			strcpy(prlMsd->Usec,ogBasicData.omUserID);
			strcpy(prlMsd->Useu,ogBasicData.omUserID);
			
			prlMsd->Lstu = CTime::GetCurrentTime();
		
		    prlMsd->Cdat = prlMsd->Lstu;

			CTimeSpan olBkf1 = prpBsh->BlFrom - prpBsh->From;
			CTimeSpan olBkt1 = prpBsh->BlTo   - prpBsh->From;
			CTimeSpan olBrkf = prpBsh->BreakFrom - prpBsh->From;
			CTimeSpan olBrkt = prpBsh->BreakTo   - prpBsh->From;
		
			prlMsd->Sdu1 = atoi(prlBsd->Sdu1);
			prlMsd->Sex1,atoi(prlBsd->Sex1);
			prlMsd->Bkd1,atoi(prlBsd->Bkd1);

			prlMsd->Urno = ogBasicData.GetNextUrno();

			

			if(strcmp(prlBsd->Esbg,prlBsd->Lsen)>0)
				olSpan = CTimeSpan(1,0,0,0);

			prlMsd->Esbg = olStart;
			olEnd += olSpan;
			prlMsd->Lsen = olEnd;
			
			CTime olDBkf1 = olStart + olBkf1;
			CTime olDBkt1 = olStart + olBkt1;
			CTime olDBrkf = olStart + olBrkf;
			CTime olDBrkt = olStart + olBrkt;

			prlMsd->Bkf1 = olDBkf1;
			prlMsd->Bkt1 = olDBkt1;
			prlMsd->Brkf = olDBrkf;
			prlMsd->Brkt = olDBrkt;
			
		
			int ilSdu1 = atoi(prlBsd->Sdu1);
			if(strcmp(prlBsd->Type,"D")==0 && ilSdu1>0)
			{
				CTimeSpan olShiftDura(ilSdu1*60); // = CTimeSpan(0,ilHour,ilMin,0);
				CTimeSpan olDynDura = olEnd - olStart;
				CTimeSpan olDiff= olDynDura.GetTotalMinutes()-ilSdu1; //olShiftDura;
				CTime olShiftBegin = olStart ;
				prlMsd->Sbgi = olShiftBegin;
				
				CTime olShiftEnd = olShiftBegin + olShiftDura;
				prlMsd->Seni = olShiftEnd;
			}


			ogMsdData.AddMsdInternal(prlMsd);
			prlMsd->IsChanged = DATA_NEW;
			if (bpSave == true)
			{
				ogMsdData.UpdateMsd(prlMsd,true,false);
			}
			
			if(prlSdg->Expd == true)
			{
				prlSdg->Expd = false;
				ogSdgData.UpdateSdg(prlSdg,TRUE);
			}


			CreateVirtuellSdtsByMsduAndSdgu(prlMsd->Urno,lpSdgu);
		}				
	}



	CCS_CATCH_ALL
}

void DataSet::CreateSdtFromBsdAndSdg(BASICSHIFTS *prpBsh, long lpSdgu, CTime opLoadStart, CTime opLoadEnd, CString opPfcCode)
{
	CCS_TRY
	CTimeSpan olOneDay(1,0,0,0);
	static CCSPtrArray<TIMEFRAMEDATA> olTimes;
	CTimeSpan olSpan(0,0,0,0),olDayOffset;

	CWaitCursor olWait;


	int ilDayOffset = -999;

	opLoadStart += CTimeSpan(prpBsh->DayOffSet,0,0,0);
	opLoadEnd += CTimeSpan(prpBsh->DayOffSet,0,0,0);

	BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(prpBsh->RUrno);
	if(prpBsh != NULL && prlBsd != NULL)
	{

		

		olTimes.DeleteAll();
		ogSdgData.GetTimeFrames(lpSdgu, olTimes);
	
		CTime olStart = HourStringToDate(CString(prlBsd->Esbg), opLoadStart);
		CTime olEnd   = HourStringToDate(CString(prlBsd->Lsen), opLoadStart);

		if(olStart < opLoadStart)
		{
			olStart += olOneDay;
			olEnd += olOneDay;
		}
		
		
		for(int j = olTimes.GetSize() -1; j >= 0; j--)
		{
			if(IsOverlapped(opLoadStart, opLoadEnd, olTimes[j].StartTime, olTimes[j].EndTime) == TRUE)
			{
				/*olSpan*/ olDayOffset = CTime(opLoadStart.GetYear(), opLoadStart.GetMonth(), opLoadStart.GetDay(), 0, 0, 0) - olTimes[j].StartTime;
				ilDayOffset = /*olSpan*/olDayOffset.GetDays();
				
				break;
			}
		}
		if(ilDayOffset == -999)
		{
			//olTimes.DeleteAll();
			return;
		}


	
		CString olStartTime = CString(prlBsd->Esbg);
		CString olEndTime   = CString(prlBsd->Lsen);

		SDTDATA rlSdtTemplate;
		rlSdtTemplate.Sdgu = lpSdgu; 	// Eindeutige Datensatz-Nr.
	
		strcpy( rlSdtTemplate.Fctc , opPfcCode);
		strcpy(rlSdtTemplate.Bewc, prlBsd->Bewc); 		// Bewertungsfaktor.code
		strcpy(rlSdtTemplate.Bkr1, prlBsd->Bkr1); 		// Pausenlage relativ zu Schichtbeginn oder absolut
		strcpy(rlSdtTemplate.Bsdc, prlBsd->Bsdc); 		// Code
		rlSdtTemplate.Bsdu = prlBsd->Urno;				// Basisschicht Urno
		strcpy(rlSdtTemplate.Bsdk, prlBsd->Bsdk); 		// Kurzbezeichnung
		strcpy(rlSdtTemplate.Bsdn, prlBsd->Bsdn); 		// Bezeichnung
		strcpy(rlSdtTemplate.Bsds, prlBsd->Bsds); 		// SAP-Code
		strcpy(rlSdtTemplate.Ctrc, prlBsd->Ctrc); 		// Vertragsart.code
		strcpy(rlSdtTemplate.Type, prlBsd->Type); 		// Flag (statisch/dynamisch)
		strcpy(rlSdtTemplate.Usec, ogBasicData.omUserID); 	// Anwender (Ersteller)
		strcpy(rlSdtTemplate.Useu, ogBasicData.omUserID); 	// Anwender (letzte Änderung)
		CTime olFrom = CTime::GetCurrentTime();
		rlSdtTemplate.Lstu = CTime::GetCurrentTime(); 	// Datum letzte Änderung
		CTimeSpan olBkf1 = prpBsh->BlFrom - prpBsh->From;
		CTimeSpan olBkt1 = prpBsh->BlTo   - prpBsh->From;
		CTimeSpan olBrkf = prpBsh->BreakFrom - prpBsh->From;
		CTimeSpan olBrkt = prpBsh->BreakTo   - prpBsh->From;
		
		rlSdtTemplate.Sdu1 = atoi(prlBsd->Sdu1);/*[6]*/ 	// Reguläre Schichtdauer
		rlSdtTemplate.Sex1 = atoi(prlBsd->Sex1);/*[6]*/ 	// Mögliche Arbeitszeitver-längerung
		rlSdtTemplate.Ssh1 = atoi(prlBsd->Ssh1);/*[6]*/ 	// Mögliche Arbeitszeitverkürzung
		rlSdtTemplate.Bkd1 = atoi(prlBsd->Bkd1);/*[6]*/ 	// Pausenlänge
		rlSdtTemplate.Cdat = CTime::GetCurrentTime(); 	// Erstellungsdatum

		rlSdtTemplate.IsVirtuel = true;
		if(strcmp(prlBsd->Esbg,prlBsd->Lsen)>0)
			olSpan = CTimeSpan(1,0,0,0);

		for( j = olTimes.GetSize()-1; j >=0 ; j--)
		{
			SDTDATA *prlSdt = new SDTDATA;
			
			rlSdtTemplate.Urno = ogBasicData.GetNextUrno(); 	// Eindeutige Datensatz-Nr.
	

			
			rlSdtTemplate.Esbg = HourStringToDate( olStartTime, olTimes[j].StartTime + olDayOffset);
			rlSdtTemplate.Lsen = HourStringToDate( olEndTime,   olTimes[j].StartTime + olDayOffset) + olSpan;
			rlSdtTemplate.Bkf1 = rlSdtTemplate.Esbg+olBkf1;
			rlSdtTemplate.Bkt1 = rlSdtTemplate.Esbg+olBkt1;
			rlSdtTemplate.Brkf = rlSdtTemplate.Esbg+olBrkf;
			rlSdtTemplate.Brkt = rlSdtTemplate.Esbg+olBrkt;

			rlSdtTemplate.IsVirtuel = true;

			if(strcmp(rlSdtTemplate.Type,"D")==0 && rlSdtTemplate.Sdu1>0)
			{
			CTimeSpan olShiftDura(rlSdtTemplate.Sdu1*60); // = CTimeSpan(0,ilHour,ilMin,0);
				CTimeSpan olDynDura = rlSdtTemplate.Lsen - rlSdtTemplate.Esbg;
				CTimeSpan olDiff= olDynDura.GetTotalMinutes()-rlSdtTemplate.Sdu1; //olShiftDura;
				rlSdtTemplate.Sbgi = rlSdtTemplate.Esbg + olDiff;
				rlSdtTemplate.Seni = (CTime)(rlSdtTemplate.Sbgi+olShiftDura);
			}

			*prlSdt = rlSdtTemplate;
		
			ogSdtData.AddSdtInternal(prlSdt);
			//ogSdtData.UpdateSdt(prlSdt);
			prlSdt->IsChanged = DATA_NEW;
			

		}
		olTimes.DeleteAll();
	}

	
	CCS_CATCH_ALL
}


void DataSet::CheckAttention()
{
	CCS_TRY
	if(ogConflictData.GetDataSize() == 0)
	{
		//MWO TO DO
		//pogMainWindow->SendMessage(WM_SETATTENTIONBUTTON, UB_RESETREDRAW, MAKELONG(0, 0));
	}
	else
	{
		//MWO TO DO
		//pogMainWindow->SendMessage(WM_SETATTENTIONBUTTON, UB_SETREDRAW, MAKELONG(0, 0));
	}
	CCS_CATCH_ALL
}


void DataSet::CheckFlightsAfterBroadcast(FLIGHTDATA *prpFlight)
{
	CCS_TRY

	BOOL blIsActive = FALSE,
		  blIsA = FALSE,
		  blIsD = FALSE,
		  blIsNoRes = FALSE;

	FLIGHTDATA *prlFlightA = NULL;
	FLIGHTDATA *prlFlightD = NULL;

	FLIGHTDATA *prlTmpFlight = prpFlight;//ogFlightData.GetFlightByUrno(lpFkey); 
	if(prlTmpFlight != NULL)
	{
		if(strcmp(prlTmpFlight->Adid, "D") == 0)
		{
			prlFlightD = prlTmpFlight;
			prlFlightA = ogFlightData.GetJoinFlight(prlTmpFlight);
		}
		else if(strcmp(prlTmpFlight->Adid, "A") == 0)
		{
			prlFlightA = prlTmpFlight;
			prlFlightD = ogFlightData.GetJoinFlight(prlTmpFlight);
		}
	}



	CCS_CATCH_ALL
}





void DataSet::CreateSdtFromDrrSingle(CString lpDrrUrno)
{
	CWaitCursor olWait;

	DRRDATA *prlDrr = ogDrrData.GetDrrByUrno(atol(lpDrrUrno));
	
	
//    JOBDATA  *prlJob2 =NULL;
//	ogJobData.omUrnoMap5.Lookup((void *) prlDrr->Urno, (void *& )prlJob2);	// QDO
   

//	if (prlJob2!=NULL)
//	{
 //   CString s=prlJob2->Acfr.Format("%H%m");
//	AfxMessageBox(s);
//	}


	if (prlDrr == NULL)
		return;

		
	CTimeSpan olOneDay(1,0,0,0);
	SDTDATA *prlSdt = new SDTDATA;
	prlSdt->Urno = atol(lpDrrUrno); 	// Eindeutige Datensatz-Nr.
	prlSdt->Sdgu = 0; 
		
	prlSdt->IsVirtuel = false;
	prlSdt->Esbg = prlDrr->Avfr;
	prlSdt->Lsen = prlDrr->Avto;

	if (prlSdt->Esbg > prlSdt->Lsen)
		prlSdt->Lsen += olOneDay;

	prlSdt->RosterStfu = prlDrr->Stfu;
	prlSdt->Drrn = atoi(prlDrr->Drrn);
	strcpy(prlSdt->Sday,prlDrr->Sday);

	BSDDATA *prlBsd = ogBsdData.GetBsdByBsdc(prlDrr->Scod);
	STFDATA *prlStf = ogStfData.GetStfByUrno(prlSdt->RosterStfu);
	CString olCode = CString(" - ");

	
	if (prlBsd != NULL && prlStf != NULL)
	{
		if (prlStf->Dodm.GetStatus() == COleDateTime::valid)
		{
			CTime olStart(prlStf->Dodm.GetYear(),prlStf->Dodm.GetMonth(),prlStf->Dodm.GetDay(),0,0,0);
			CTime olEnd = olStart + CTimeSpan(1,0,0,0);

			if (prlSdt->Esbg >= olEnd)
			{
				delete prlSdt;
				return;
			}
			else
			{
				olCode = CString(" - ") +  ogBasicData.GetFormattedEmployeeName(prlStf);
			}
		}
		else
		{
			olCode = CString(" - ") +  ogBasicData.GetFormattedEmployeeName(prlStf);
		}
	}

	int ilDrsExits = atoi(prlDrr->Drsf);
	if(ilDrsExits == 1)
	{
		CString olAts1 = ogBCD.GetFieldExt("DRS", "DRRU", "SDAY", lpDrrUrno, prlDrr->Sday, "ATS1");
		if(!olAts1.IsEmpty())
		{
			olCode += ";";
			olCode += olAts1;
		}
		CString olStat = ogBCD.GetFieldExt("DRS", "DRRU", "SDAY", lpDrrUrno, prlDrr->Sday, "STAT");
		if(olStat.Find("S") > -1)
		{
			return;
		}
	}


	strcpy(prlSdt->Enames,(LPCSTR)olCode);
	if (strlen(prlDrr->Ross) > 0)
		strncpy(prlSdt->StatPlSt,prlDrr->Ross,1);

	if (strcmp(prlDrr->Rosl,"L") == 0)
	{
		prlSdt->Planungsstufe = 6;// Langzeitdienstplan
	}
	else
	{
		prlSdt->Planungsstufe = atoi(prlDrr->Rosl);
	}

	if(prlBsd != NULL)
	{
		CString olTmpSday(prlDrr->Sday);
		olTmpSday += "000000";

		CTime olDay = DBStringToDateTime(olTmpSday);
		if(olDay == -1)
			return;

		COleDateTime olOleDay = CTimeToCOleDateTime(olDay);
		CString olPfcCode = prlDrr->Fctc;
		if(olPfcCode.IsEmpty())
		{
		    olPfcCode = prlBsd->Fctc;
			if(olPfcCode.IsEmpty())
			{
				olPfcCode = ogSpfData.GetPfcBySurnWithTime(prlSdt->RosterStfu, olOleDay);
			}
		}
	
		
		
		strcpy(prlSdt->Fctc,(LPCSTR)olPfcCode);
		strcpy(prlSdt->Bewc, prlBsd->Bewc); 		// Bewertungsfaktor.code
		strcpy(prlSdt->Bkr1, prlBsd->Bkr1); 		// Pausenlage relativ zu Schichtbeginn oder absolut
		prlSdt->Bsdu = prlBsd->Urno;				// Basisschicht Urno
		strcpy(prlSdt->Bsds, prlBsd->Bsds); 		// SAP-Code
		strcpy(prlSdt->Ctrc, prlBsd->Ctrc); 		// Vertragsart.code
		strcpy(prlSdt->Type, prlBsd->Type); 		// Flag (statisch/dynamisch)
		strcpy(prlSdt->Bsdk, prlBsd->Bsdk); 		// Kurzbezeichnung
		strcpy(prlSdt->Bsdn, prlBsd->Bsdn); 		// Bezeichnung
		prlSdt->Sdu1 = atoi(prlBsd->Sdu1);/*[6]*/ 	// Reguläre Schichtdauer
		prlSdt->Sex1 = atoi(prlBsd->Sex1);/*[6]*/ 	// Mögliche Arbeitszeitver-längerung
		prlSdt->Ssh1 = atoi(prlBsd->Ssh1);/*[6]*/ 	// Mögliche Arbeitszeitverkürzung
		prlSdt->Bkd1 = atoi(prlBsd->Bkd1);/*[6]*/ 	// Pausenlänge
	
		
		strcpy(prlSdt->Bsdc, prlDrr->Scod); 		// Code
		strcpy(prlSdt->Usec, ogBasicData.omUserID); // Anwender (Ersteller)
		strcpy(prlSdt->Useu, ogBasicData.omUserID); // Anwender (letzte Änderung)
		prlSdt->Lstu = CTime::GetCurrentTime(); 	// Datum letzte Änderung
		
	 
		
		prlSdt->Bkf1 = prlDrr->Sbfr;
	    prlSdt->Bkt1 = prlDrr->Sbto;	
			
		if(prlSdt->Esbg>prlSdt->Bkf1)
		{
			prlSdt->Bkf1 += olOneDay;
		}
		if(prlSdt->Bkt1<prlSdt->Bkf1)
			prlSdt->Bkt1 += olOneDay;
		
		
	
		if (flag==1) 
		{
		
		//	AfxMessageBox("1");
		
		CTimeSpan olOnHour1(0,8,0,0);
		
//		if (prlJob2==NULL)
	//	{
	//         prlSdt->Brkf=NULL;
    //         prlSdt->Brkt=NULL;
	//	} 
		
	//	else
		{
		
//			prlSdt->Brkf =  prlJob2->Acfr + olOnHour1;
		//	prlSdt->Brkf =  prlJob2->Acfr;
			
			prlSdt->Brkt = prlSdt->Brkf + CTimeSpan(0, 0, prlDrr->Sblu, 0);	
	      
		}
    
		}
		
		
		else if (flag==2)
		{
		 //  AfxMessageBox("2");
          prlSdt->Brkf = prlDrr->Sbfr;				
		  prlSdt->Brkt = prlSdt->Brkf + CTimeSpan(0, 0, prlDrr->Sblu, 0);


		
		}
		
		
		
		strcpy(prlSdt->Type ,"R");	
		prlSdt->OrgBrkf = prlSdt->Brkf;
		prlSdt->OrgBrkt = prlSdt->Brkt;
			
		prlSdt->Brkf += CTimeSpan(0,0,prlDrr->Bros,0);
	    prlSdt->Brkt += CTimeSpan(0,0,prlDrr->Bros,0);


   
		

	//*********************** DRD's und DRA's werden auch als SDT's verwendet. Bessere Performance
			
				
		CCSPtrArray <RecordSet> olDrdList;
		CCSPtrArray <RecordSet> olDraList;
		CTime olDate = prlSdt->Esbg;

		CString olStfu;
		olStfu.Format("%ld",prlSdt->RosterStfu );
		int ilDummy = ogBCD.GetDataCount("DRD");
		ogBCD.GetRecordsExt("DRD","STFU","SDAY", olStfu, prlDrr->Sday, &olDrdList);
		int ilDrrn2Idx = ogBCD.GetFieldIndex("DRD", "DRRN");
		int ilDrdfIdx = ogBCD.GetFieldIndex("DRD", "DRDF");
		int ilDrdtIdx = ogBCD.GetFieldIndex("DRD", "DRDT");
		int ilFctcIdx = ogBCD.GetFieldIndex("DRD", "FCTC");
		int ilDrdUrnoIdx = ogBCD.GetFieldIndex("DRD", "URNO");

		for ( int illc2 = 0; illc2 < olDrdList.GetSize(); illc2++)
		{
			if( prlSdt->Drrn == atoi(olDrdList[illc2].Values[ilDrrn2Idx]))
			{
				SDTDATA *prlDrdSdt = new SDTDATA;

				prlDrdSdt->Esbg = DBStringToDateTime(olDrdList[illc2].Values[ilDrdfIdx]) ;
				prlDrdSdt->Lsen = DBStringToDateTime(olDrdList[illc2].Values[ilDrdtIdx]);
				strcpy(prlDrdSdt->Fctc,(LPCSTR)olDrdList[illc2].Values[ilFctcIdx]);
				prlDrdSdt->Sdgu = prlSdt->Urno;	
				strcpy(prlDrdSdt->StatPlSt,prlSdt->StatPlSt);
				if (prlDrdSdt->StatPlSt[0] == 'A')
					prlDrdSdt->Urno = ogBasicData.GetNextUrno(); 
				else
					prlDrdSdt->Urno = atoi(olDrdList[illc2].Values[ilDrdUrnoIdx]); 
				strcpy(prlDrdSdt->Type ,"D");		 
				prlDrdSdt->Cdat = CTime::GetCurrentTime(); 	// Erstellungsdatum
				prlDrdSdt->IsChanged = DATA_UNCHANGED; // Record is only temporary!
				ogSdtRosterData.AddSdtInternal(prlDrdSdt);
				
			}
		}

		ogBCD.GetRecordsExt("DRA","STFU","SDAY", olStfu, prlDrr->Sday, &olDraList);

		int ilDrrn3Idx = ogBCD.GetFieldIndex("DRA", "DRRN");
		int ilAbfrIdx = ogBCD.GetFieldIndex("DRA", "ABFR");
		int ilAbtoIdx = ogBCD.GetFieldIndex("DRA", "ABTO");
		int ilSdacIdx = ogBCD.GetFieldIndex("DRA", "SDAC");
		int ilDraUrnoIdx = ogBCD.GetFieldIndex("DRA", "URNO");

		for ( int illc3 = 0; illc3 < olDraList.GetSize(); illc3++)
		{
			if( prlSdt->Drrn == atoi(olDraList[illc3].Values[ilDrrn3Idx]))
			{
				SDTDATA *prlDraSdt = new SDTDATA;

				prlDraSdt->Esbg = DBStringToDateTime(olDraList[illc3].Values[ilAbfrIdx]);
					
				prlDraSdt->Lsen = DBStringToDateTime(olDraList[illc3].Values[ilAbtoIdx]);

				strcpy(prlDraSdt->StatPlSt,prlSdt->StatPlSt);
				if (prlDraSdt->StatPlSt[0] == 'A')
					prlDraSdt->Urno = ogBasicData.GetNextUrno(); 
				else
					prlDraSdt->Urno = atoi(olDraList[illc3].Values[ilDraUrnoIdx]); 
	
				strcpy(prlDraSdt->Fctc,(LPCSTR)olDraList[illc3].Values[ilSdacIdx]);
				prlDraSdt->Sdgu = prlSdt->Urno;		 
				strcpy(prlDraSdt->Type ,"A");		 
				prlDraSdt->Cdat = CTime::GetCurrentTime(); 	// Erstellungsdatum
				prlDraSdt->IsChanged = DATA_UNCHANGED; // Record is only temporary!
				ogSdtRosterData.AddSdtInternal(prlDraSdt);

			}
		}
		olDrdList.DeleteAll();
		olDraList.DeleteAll();

		prlSdt->Cdat = CTime::GetCurrentTime(); 	// Erstellungsdatum
		prlSdt->IsChanged = DATA_UNCHANGED; // Record is only temporary!
		ogSdtRosterData.AddSdtInternal(prlSdt);
	}
}

void DataSet::CreateSdtFromDrr()
{
	CWaitCursor olWait;

	DWORD	llNow,llLast, llStart;
	double	flDiff;

	llLast = GetTickCount();
	llStart = llLast;

	char clBuffer[128];
	

	
	for(int i = 0; i < ogDrrData.omData.GetSize(); i++)
	{
		sprintf(clBuffer,"%ld",ogDrrData.omData[i].Urno);
		CreateSdtFromDrrSingle(clBuffer);

		
	}

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("DataSet::CreateSdtFromDrr:CreateSdtFromDrrSingle %.3lf sec\n", flDiff/1000.0 );

}


void DataSet::CreateSdtFromSingleGpl(CString opUrno,CString opSplu,CString opGsnm,CString opVafr,CString opVato,CTime opLoadStart,CTime opLoadEnd)
{
	CCS_TRY

	CString olBufr;
	CString olBumo;
	CString olBusa;
	CString olBusu;
	CString olButh;
	CString olBuwe;
	CString olButu;

	CString olGspu;
	CString olNstf;

	CString olSufr;
	CString olSumo;
	CString olSusa;
	CString olSusu;
	CString olSuth;
	CString olSuwe;
	CString olSutu;
	
	CString	olP1fr;
	CString olP1mo;
	CString olP1sa;
	CString olP1su;
	CString olP1th;
	CString olP1we;
	CString olP1tu;

	CString	olP2fr;
	CString olP2mo;
	CString olP2sa;
	CString olP2su;
	CString olP2th;
	CString olP2we;
	CString olP2tu;

	CString	olFctc;

	CWaitCursor olWait;

	CCSPtrArray <RecordSet> olList;
	ogBCD.GetRecords("GSP", "GPLU", opUrno,&olList);

	int ilBufrIdx = ogBCD.GetFieldIndex("GSP", "BUFR");
	int ilBusaIdx = ogBCD.GetFieldIndex("GSP", "BUSA");
	int ilBusuIdx = ogBCD.GetFieldIndex("GSP", "BUSU");
	int ilBumoIdx = ogBCD.GetFieldIndex("GSP", "BUMO");
	int ilButhIdx = ogBCD.GetFieldIndex("GSP", "BUTH");
	int ilButuIdx = ogBCD.GetFieldIndex("GSP", "BUTU");
	int ilBuweIdx = ogBCD.GetFieldIndex("GSP", "BUWE");

	int ilGspuIdx = ogBCD.GetFieldIndex("GSP", "URNO");
	int ilNstfIdx = ogBCD.GetFieldIndex("GSP", "NSTF");
	
	int ilSufrIdx = ogBCD.GetFieldIndex("GSP", "SUFR");
	int ilSusaIdx = ogBCD.GetFieldIndex("GSP", "SUSA");
	int ilSusuIdx = ogBCD.GetFieldIndex("GSP", "SUSU");
	int ilSumoIdx = ogBCD.GetFieldIndex("GSP", "SUMO");
	int ilSuthIdx = ogBCD.GetFieldIndex("GSP", "SUTH");
	int ilSutuIdx = ogBCD.GetFieldIndex("GSP", "SUTU");
	int ilSuweIdx = ogBCD.GetFieldIndex("GSP", "SUWE");
	
	int ilP1frIdx = ogBCD.GetFieldIndex("GSP", "P1FR");
	int ilP1saIdx = ogBCD.GetFieldIndex("GSP", "P1SA");
	int ilP1suIdx = ogBCD.GetFieldIndex("GSP", "P1SU");
	int ilP1moIdx = ogBCD.GetFieldIndex("GSP", "P1MO");
	int ilP1thIdx = ogBCD.GetFieldIndex("GSP", "P1TH");
	int ilP1tuIdx = ogBCD.GetFieldIndex("GSP", "P1TU");
	int ilP1weIdx = ogBCD.GetFieldIndex("GSP", "P1WE");
	
	int ilP2frIdx = ogBCD.GetFieldIndex("GSP", "P2FR");
	int ilP2saIdx = ogBCD.GetFieldIndex("GSP", "P2SA");
	int ilP2suIdx = ogBCD.GetFieldIndex("GSP", "P2SU");
	int ilP2moIdx = ogBCD.GetFieldIndex("GSP", "P2MO");
	int ilP2thIdx = ogBCD.GetFieldIndex("GSP", "P2TH");
	int ilP2tuIdx = ogBCD.GetFieldIndex("GSP", "P2TU");
	int ilP2weIdx = ogBCD.GetFieldIndex("GSP", "P2WE");
	

	for (int illc = 0; illc < olList.GetSize(); illc++)
	{
		olBufr = olList[illc].Values[ilBufrIdx];
		olBumo = olList[illc].Values[ilBumoIdx];
		olBusa = olList[illc].Values[ilBusaIdx];
		olButh = olList[illc].Values[ilButhIdx];
		olBuwe = olList[illc].Values[ilBuweIdx];
		olButu = olList[illc].Values[ilButuIdx];
		olBusu = olList[illc].Values[ilBusuIdx];
		
		olGspu = olList[illc].Values[ilGspuIdx];
		olNstf = olList[illc].Values[ilNstfIdx];
		
		olSufr = olList[illc].Values[ilSufrIdx];
		olSumo = olList[illc].Values[ilSumoIdx];
		olSusa = olList[illc].Values[ilSusaIdx];
		olSuth = olList[illc].Values[ilSuthIdx];
		olSuwe = olList[illc].Values[ilSuweIdx];
		olSutu = olList[illc].Values[ilSutuIdx];
		olSusu = olList[illc].Values[ilSusuIdx];

		olP1fr = olList[illc].Values[ilP1frIdx];
		olP1mo = olList[illc].Values[ilP1moIdx];
		olP1sa = olList[illc].Values[ilP1saIdx];
		olP1th = olList[illc].Values[ilP1thIdx];
		olP1we = olList[illc].Values[ilP1weIdx];
		olP1tu = olList[illc].Values[ilP1tuIdx];
		olP1su = olList[illc].Values[ilP1suIdx];

		olP2fr = olList[illc].Values[ilP2frIdx];
		olP2mo = olList[illc].Values[ilP2moIdx];
		olP2sa = olList[illc].Values[ilP2saIdx];
		olP2th = olList[illc].Values[ilP2thIdx];
		olP2we = olList[illc].Values[ilP2weIdx];
		olP2tu = olList[illc].Values[ilP2tuIdx];
		olP2su = olList[illc].Values[ilP2suIdx];


		CTime olVafrTime = DBStringToDateTime(opVafr);
		CTime olVatoTime = DBStringToDateTime(opVato);
	
		CTime olStartDay = CTime(opLoadStart.GetYear(),opLoadStart.GetMonth(),opLoadStart.GetDay(),0,0,0);
		CTime olEndDay = CTime(opLoadEnd.GetYear(),opLoadEnd.GetMonth(),opLoadEnd.GetDay(),0,0,0);
		CTime olDay;
		int ilDayOfWeek = 0;
		for(olDay = olStartDay; olDay <= olEndDay;olDay += CTimeSpan(1,0,0,0) )
		{
			for (int i = 0; i < 2; i++)	// loop for first shift and second one 
			{
				long llBsdu = 0L;
				long llFctc = 0L;
				
				ilDayOfWeek = olDay.GetDayOfWeek();

				switch(ilDayOfWeek)
				{
				case 1:
					if (i == 0)
					{
						llBsdu = atol(olBusu);
						llFctc = atol(olP1su);
					}
					else
					{
						llBsdu = atol(olSusu);
						llFctc = atol(olP2su);
					}
					break;
				case 2:
					if (i == 0)
					{
						llBsdu = atol(olBumo);
						llFctc = atol(olP1mo);
					}
					else
					{
						llBsdu = atol(olSumo);
						llFctc = atol(olP2mo);
					}
					break;
				case 3:
					if (i == 0)
					{
						llBsdu = atol(olButu);
						llFctc = atol(olP1tu);
					}
					else
					{
						llBsdu = atol(olSutu);
						llFctc = atol(olP2tu);
					}
					break;
				case 4:
					if (i == 0)
					{
						llBsdu = atol(olBuwe);
						llFctc = atol(olP1we);
					}
					else
					{
						llBsdu = atol(olSuwe);
						llFctc = atol(olP2we);
					}
					break;
				case 5:
					if (i == 0)
					{
						llBsdu = atol(olButh);
						llFctc = atol(olP1th);
					}
					else
					{
						llBsdu = atol(olSuth);
						llFctc = atol(olP2th);
					}
					break;
				case 6:
					if (i == 0)
					{
						llBsdu = atol(olBufr);
						llFctc = atol(olP1fr);
					}
					else
					{
						llBsdu = atol(olSufr);
						llFctc = atol(olP2fr);
					}
					break;
				case 7:
					if (i == 0)
					{
						llBsdu = atol(olBusa);
						llFctc = atol(olP1sa);
					}
					else
					{
						llBsdu = atol(olSusa);
						llFctc = atol(olP2sa);
					}
					break;
				}


				BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(llBsdu);
				if(prlBsd == NULL)
				{
					prlBsd = ogAllBsdData.GetBsdByUrno(llBsdu);
					/*
					if(prlBsd != NULL)
					{
						prlBsd = ogBsdData.GetBsdByBsdc(prlBsd->Bsdc);
					}
					*/
				}


				if (prlBsd != NULL && ( olDay <= olVatoTime && olDay >= olVafrTime))
				{
					SDTDATA *prlSdt = new SDTDATA;
					prlSdt->Urno = ogBasicData.GetNextUrno(); 	// Eindeutige Datensatz-Nr.
					strcpy(prlSdt->Bewc, prlBsd->Bewc); 		// Bewertungsfaktor.code
					strcpy(prlSdt->Bkr1, prlBsd->Bkr1); 		// Pausenlage relativ zu Schichtbeginn oder absolut
					strcpy(prlSdt->Bsdc, prlBsd->Bsdc); 		// Code
					prlSdt->Bsdu = prlBsd->Urno;				// Basisschicht Urno
					strcpy(prlSdt->Bsdk, prlBsd->Bsdk); 		// Kurzbezeichnung
					strcpy(prlSdt->Bsdn, prlBsd->Bsdn); 		// Bezeichnung
					strcpy(prlSdt->Bsds, prlBsd->Bsds); 		// SAP-Code
					strcpy(prlSdt->Ctrc, prlBsd->Ctrc); 		// Vertragsart.code
					strcpy(prlSdt->Type, prlBsd->Type); 		// Flag (statisch/dynamisch)
					strcpy(prlSdt->Usec, ogBasicData.omUserID); 	// Anwender (Ersteller)
					strcpy(prlSdt->Useu, "");					// Anwender (letzte Änderung)

					olFctc = ogPfcData.GetFctcByUrno(llFctc);
					if (olFctc.GetLength() == 0)
					{
						strcpy(prlSdt->Fctc,prlBsd->Fctc);
					}
					else
					{
						strcpy(prlSdt->Fctc,olFctc);
					}
					
					CTime olFrom = CTime::GetCurrentTime();
					prlSdt->Esbg = HourStringToDate( CString( prlBsd->Esbg), olDay);			
					prlSdt->Lsen = HourStringToDate( CString( prlBsd->Lsen), olDay);		
						
					prlSdt->Sdu1 = atoi(prlBsd->Sdu1);				
					//strcpy( prlSdt->Fctc , opPfcCode);
					
					if(prlSdt->Esbg > prlSdt->Lsen)
					{
						prlSdt->Lsen += CTimeSpan(1,0,0,0);
					}

								
					if(strcmp(prlSdt->Type,"D")==0 && prlSdt->Sdu1 > 0)
					{
						prlSdt->Sbgi = prlSdt->Esbg;
						prlSdt->Seni = prlSdt->Sbgi + CTimeSpan(prlSdt->Sdu1*60);
					}
					prlSdt->Lstu = CTime::GetCurrentTime(); 	// Datum letzte Änderung
					prlSdt->Bkf1 = HourStringToDate( CString( prlBsd->Bkf1), olDay);	
					prlSdt->Bkt1 = HourStringToDate( CString( prlBsd->Bkt1), olDay);	


					if(prlSdt->Esbg > prlSdt->Bkf1)
					{
						prlSdt->Bkf1 += CTimeSpan(1,0,0,0);
					}
					if(prlSdt->Bkt1 < prlSdt->Bkf1)
					{
						prlSdt->Bkt1 += CTimeSpan(1,0,0,0);
					}

					prlSdt->Brkf = prlSdt->Bkf1;
					prlSdt->Brkt = prlSdt->Bkf1 + CTimeSpan(atoi(prlBsd->Bkd1)*60);//Pause bis

					
					prlSdt->Sex1 = atoi(prlBsd->Sex1);/*[6]*/ 	// Mögliche Arbeitszeitver-längerung
					prlSdt->Ssh1 = atoi(prlBsd->Ssh1);/*[6]*/ 	// Mögliche Arbeitszeitverkürzung
					prlSdt->Bkd1 = atoi(prlBsd->Bkd1);/*[6]*/ 	// Pausenlänge
				
					prlSdt->Sdgu = atol(opUrno);
					prlSdt->GspUrno = atol(olGspu);
					prlSdt->SplUrno = atol(opSplu);
					
					prlSdt->Faktor = atol(olNstf);
					prlSdt->IsVirtuel = false;
					ogSdtShiftData.AddSdtInternal(prlSdt);
				
				
				}
			}
		}
	}

	olList.DeleteAll();

	CCS_CATCH_ALL
}

void DataSet::CreateSdtFromSingleGsp(CString opUrno,CString opGplu,CString opSplu,CString opVafr,CString opVato,CTime opLoadStart,CTime opLoadEnd)
{
	CCS_TRY
	CString olBufr;
	CString olBumo;
	CString olBusa;
	CString olBusu;
	CString olButh;
	CString olBuwe;
	CString olButu;

	CString olGspu;
	CString olNstf;
	
	CString olSufr;
	CString olSumo;
	CString olSusa;
	CString olSusu;
	CString olSuth;
	CString olSuwe;
	CString olSutu;
	
	CString	olP1fr;
	CString olP1mo;
	CString olP1sa;
	CString olP1su;
	CString olP1th;
	CString olP1we;
	CString olP1tu;

	CString	olP2fr;
	CString olP2mo;
	CString olP2sa;
	CString olP2su;
	CString olP2th;
	CString olP2we;
	CString olP2tu;

	CString	olFctc;
	

	CWaitCursor olWait;

	CCSPtrArray <RecordSet> olList;
	ogBCD.GetRecords("GSP", "URNO", opUrno,&olList);

	int ilBufrIdx = ogBCD.GetFieldIndex("GSP", "BUFR");
	int ilBusaIdx = ogBCD.GetFieldIndex("GSP", "BUSA");
	int ilBusuIdx = ogBCD.GetFieldIndex("GSP", "BUSU");
	int ilBumoIdx = ogBCD.GetFieldIndex("GSP", "BUMO");
	int ilButhIdx = ogBCD.GetFieldIndex("GSP", "BUTH");
	int ilButuIdx = ogBCD.GetFieldIndex("GSP", "BUTU");
	int ilBuweIdx = ogBCD.GetFieldIndex("GSP", "BUWE");

	int ilGspuIdx = ogBCD.GetFieldIndex("GSP", "URNO");
	int ilNstfIdx = ogBCD.GetFieldIndex("GSP", "NSTF");
	
	int ilSufrIdx = ogBCD.GetFieldIndex("GSP", "SUFR");
	int ilSusaIdx = ogBCD.GetFieldIndex("GSP", "SUSA");
	int ilSusuIdx = ogBCD.GetFieldIndex("GSP", "SUSU");
	int ilSumoIdx = ogBCD.GetFieldIndex("GSP", "SUMO");
	int ilSuthIdx = ogBCD.GetFieldIndex("GSP", "SUTH");
	int ilSutuIdx = ogBCD.GetFieldIndex("GSP", "SUTU");
	int ilSuweIdx = ogBCD.GetFieldIndex("GSP", "SUWE");
	
	int ilP1frIdx = ogBCD.GetFieldIndex("GSP", "P1FR");
	int ilP1saIdx = ogBCD.GetFieldIndex("GSP", "P1SA");
	int ilP1suIdx = ogBCD.GetFieldIndex("GSP", "P1SU");
	int ilP1moIdx = ogBCD.GetFieldIndex("GSP", "P1MO");
	int ilP1thIdx = ogBCD.GetFieldIndex("GSP", "P1TH");
	int ilP1tuIdx = ogBCD.GetFieldIndex("GSP", "P1TU");
	int ilP1weIdx = ogBCD.GetFieldIndex("GSP", "P1WE");
	
	int ilP2frIdx = ogBCD.GetFieldIndex("GSP", "P2FR");
	int ilP2saIdx = ogBCD.GetFieldIndex("GSP", "P2SA");
	int ilP2suIdx = ogBCD.GetFieldIndex("GSP", "P2SU");
	int ilP2moIdx = ogBCD.GetFieldIndex("GSP", "P2MO");
	int ilP2thIdx = ogBCD.GetFieldIndex("GSP", "P2TH");
	int ilP2tuIdx = ogBCD.GetFieldIndex("GSP", "P2TU");
	int ilP2weIdx = ogBCD.GetFieldIndex("GSP", "P2WE");
	

	for (int illc = 0; illc < olList.GetSize(); illc++)
	{
		olBufr = olList[illc].Values[ilBufrIdx];
		olBumo = olList[illc].Values[ilBumoIdx];
		olBusa = olList[illc].Values[ilBusaIdx];
		olButh = olList[illc].Values[ilButhIdx];
		olBuwe = olList[illc].Values[ilBuweIdx];
		olButu = olList[illc].Values[ilButuIdx];
		olBusu = olList[illc].Values[ilBusuIdx];
		
		olGspu = olList[illc].Values[ilGspuIdx];
		olNstf = olList[illc].Values[ilNstfIdx];

		olSufr = olList[illc].Values[ilSufrIdx];
		olSumo = olList[illc].Values[ilSumoIdx];
		olSusa = olList[illc].Values[ilSusaIdx];
		olSuth = olList[illc].Values[ilSuthIdx];
		olSuwe = olList[illc].Values[ilSuweIdx];
		olSutu = olList[illc].Values[ilSutuIdx];
		olSusu = olList[illc].Values[ilSusuIdx];

		olP1fr = olList[illc].Values[ilP1frIdx];
		olP1mo = olList[illc].Values[ilP1moIdx];
		olP1sa = olList[illc].Values[ilP1saIdx];
		olP1th = olList[illc].Values[ilP1thIdx];
		olP1we = olList[illc].Values[ilP1weIdx];
		olP1tu = olList[illc].Values[ilP1tuIdx];
		olP1su = olList[illc].Values[ilP1suIdx];

		olP2fr = olList[illc].Values[ilP2frIdx];
		olP2mo = olList[illc].Values[ilP2moIdx];
		olP2sa = olList[illc].Values[ilP2saIdx];
		olP2th = olList[illc].Values[ilP2thIdx];
		olP2we = olList[illc].Values[ilP2weIdx];
		olP2tu = olList[illc].Values[ilP2tuIdx];
		olP2su = olList[illc].Values[ilP2suIdx];

		

		CTime olVafrTime = DBStringToDateTime(opVafr);
		CTime olVatoTime = DBStringToDateTime(opVato);
	
		CTime olStartDay = CTime(opLoadStart.GetYear(),opLoadStart.GetMonth(),opLoadStart.GetDay(),0,0,0);
		CTime olEndDay = CTime(opLoadEnd.GetYear(),opLoadEnd.GetMonth(),opLoadEnd.GetDay(),0,0,0);
		CTime olDay;
		int ilDayOfWeek = 0;
		for(olDay = olStartDay; olDay <= olEndDay;olDay += CTimeSpan(1,0,0,0) )
		{
			for (int i = 0; i < 2; i++)	// loop for first shift and second one 
			{
				long llBsdu = 0L;
				long llFctc = 0L;
				
				ilDayOfWeek = olDay.GetDayOfWeek();

				switch(ilDayOfWeek)
				{
				case 1:
					if (i == 0)
					{
						llBsdu = atol(olBusu);
						llFctc = atol(olP1su);
					}
					else
					{
						llBsdu = atol(olSusu);
						llFctc = atol(olP2su);
					}
					break;
				case 2:
					if (i == 0)
					{
						llBsdu = atol(olBumo);
						llFctc = atol(olP1mo);
					}
					else
					{
						llBsdu = atol(olSumo);
						llFctc = atol(olP2mo);
					}
					break;
				case 3:
					if (i == 0)
					{
						llBsdu = atol(olButu);
						llFctc = atol(olP1tu);
					}
					else
					{
						llBsdu = atol(olSutu);
						llFctc = atol(olP2tu);
					}
					break;
				case 4:
					if (i == 0)
					{
						llBsdu = atol(olBuwe);
						llFctc = atol(olP1we);
					}
					else
					{
						llBsdu = atol(olSuwe);
						llFctc = atol(olP2we);
					}
					break;
				case 5:
					if (i == 0)
					{
						llBsdu = atol(olButh);
						llFctc = atol(olP1th);
					}
					else
					{
						llBsdu = atol(olSuth);
						llFctc = atol(olP2th);
					}
					break;
				case 6:
					if (i == 0)
					{
						llBsdu = atol(olBufr);
						llFctc = atol(olP1fr);
					}
					else
					{
						llBsdu = atol(olSufr);
						llFctc = atol(olP2fr);
					}
					break;
				case 7:
					if (i == 0)
					{
						llBsdu = atol(olBusa);
						llFctc = atol(olP1sa);
					}
					else
					{
						llBsdu = atol(olSusa);
						llFctc = atol(olP2sa);
					}
					break;
				}


				BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(llBsdu);
				if(prlBsd == NULL)
				{
					prlBsd = ogAllBsdData.GetBsdByUrno(llBsdu);
					/*
					if(prlBsd != NULL)
					{
						prlBsd = ogBsdData.GetBsdByBsdc(prlBsd->Bsdc);
					}
					*/
				}



				if (prlBsd != NULL && ( olDay <= olVatoTime && olDay >= olVafrTime))
				{
					SDTDATA *prlSdt = new SDTDATA;
					prlSdt->Urno = ogBasicData.GetNextUrno(); 	// Eindeutige Datensatz-Nr.
					strcpy(prlSdt->Bewc, prlBsd->Bewc); 		// Bewertungsfaktor.code
					strcpy(prlSdt->Bkr1, prlBsd->Bkr1); 		// Pausenlage relativ zu Schichtbeginn oder absolut
					strcpy(prlSdt->Bsdc, prlBsd->Bsdc); 		// Code
					prlSdt->Bsdu = prlBsd->Urno;				// Basisschicht Urno
					strcpy(prlSdt->Bsdk, prlBsd->Bsdk); 		// Kurzbezeichnung
					strcpy(prlSdt->Bsdn, prlBsd->Bsdn); 		// Bezeichnung
					strcpy(prlSdt->Bsds, prlBsd->Bsds); 		// SAP-Code
					strcpy(prlSdt->Ctrc, prlBsd->Ctrc); 		// Vertragsart.code
					strcpy(prlSdt->Type, prlBsd->Type); 		// Flag (statisch/dynamisch)
					strcpy(prlSdt->Usec, ogBasicData.omUserID); 	// Anwender (Ersteller)
					strcpy(prlSdt->Useu, "");					// Anwender (letzte Änderung)

					olFctc = ogPfcData.GetFctcByUrno(llFctc);
					if (olFctc.GetLength() == 0)
					{
						strcpy(prlSdt->Fctc,prlBsd->Fctc);
					}
					else
					{
						strcpy(prlSdt->Fctc,olFctc);
					}
					
					CTime olFrom = CTime::GetCurrentTime();
					prlSdt->Esbg = HourStringToDate( CString( prlBsd->Esbg), olDay);			
					prlSdt->Lsen = HourStringToDate( CString( prlBsd->Lsen), olDay);		
						
					prlSdt->Sdu1 = atoi(prlBsd->Sdu1);				
					//strcpy( prlSdt->Fctc , opPfcCode);
					
					if(prlSdt->Esbg > prlSdt->Lsen)
					{
						prlSdt->Lsen += CTimeSpan(1,0,0,0);
					}

								
					if(strcmp(prlSdt->Type,"D")==0 && prlSdt->Sdu1 > 0)
					{
						prlSdt->Sbgi = prlSdt->Esbg;
						prlSdt->Seni = prlSdt->Sbgi + CTimeSpan(prlSdt->Sdu1*60);
					}
					prlSdt->Lstu = CTime::GetCurrentTime(); 	// Datum letzte Änderung
					prlSdt->Bkf1 = HourStringToDate( CString( prlBsd->Bkf1), olDay);	
					prlSdt->Bkt1 = HourStringToDate( CString( prlBsd->Bkt1), olDay);	


					if(prlSdt->Esbg > prlSdt->Bkf1)
					{
						prlSdt->Bkf1 += CTimeSpan(1,0,0,0);
					}
					if(prlSdt->Bkt1 < prlSdt->Bkf1)
					{
						prlSdt->Bkt1 += CTimeSpan(1,0,0,0);
					}

					prlSdt->Brkf = prlSdt->Bkf1;
					prlSdt->Brkt = prlSdt->Bkf1 + CTimeSpan(atoi(prlBsd->Bkd1)*60);//Pause bis

					
					prlSdt->Sex1 = atoi(prlBsd->Sex1);/*[6]*/ 	// Mögliche Arbeitszeitver-längerung
					prlSdt->Ssh1 = atoi(prlBsd->Ssh1);/*[6]*/ 	// Mögliche Arbeitszeitverkürzung
					prlSdt->Bkd1 = atoi(prlBsd->Bkd1);/*[6]*/ 	// Pausenlänge
				
					prlSdt->Sdgu = atol(opGplu);
					prlSdt->GspUrno = atol(olGspu);
					prlSdt->SplUrno = atol(opSplu);
					prlSdt->Faktor = atol(olNstf);
					prlSdt->IsVirtuel = false;
					ogSdtShiftData.AddSdtInternal(prlSdt);
				
				
				}
			}
		}
	}

	olList.DeleteAll();

	CCS_CATCH_ALL
}

void DataSet::CreateSdtFromGpls(CTime opLoadStart, CTime opLoadEnd)
{
	CWaitCursor olWait;

	CStringArray olData;
	CStringArray olValueList;
	CString olValues;

	CString olUrno;
	CString olGsnm;
	CString olSplu;
	CString olVafr;
	CString olVato;
	ogBCD.GetAllFields("GPL", "URNO,GSNM,SPLU,VAFR,VATO", ",", 0, olData);
	for(int i = 0; i < olData.GetSize(); i++)
	{
		olValues = olData[i];
		ExtractItemList(olValues,&olValueList,',');
		if(olValueList.GetSize() > 4)
		{

			olUrno = olValueList[0];
			olGsnm = olValueList[1];
			olSplu = olValueList[2];
			olVafr = olValueList[3];
			olVato = olValueList[4];

			CreateSdtFromSingleGpl(olUrno,olGsnm,olSplu,olVafr,olVato,opLoadStart,opLoadEnd);
		}
	}


}

void DataSet::CreateSdtFromSpl(const CString& ropSnam,CTime opLoadStart, CTime opLoadEnd)
{
	CWaitCursor olWait;

	CString opSplu = ogBCD.GetField("SPL","SNAM",ropSnam,"URNO");	

	CStringArray olData;
	CStringArray olValueList;
	CString olValues;

	CString olUrno;
	CString olGsnm;
	CString olSplu;
	CString olVafr;
	CString olVato;
	ogBCD.GetAllFields("GPL", "URNO,GSNM,SPLU,VAFR,VATO", ",", 0, olData);
	for(int i = 0; i < olData.GetSize(); i++)
	{
		olValues = olData[i];
		ExtractItemList(olValues,&olValueList,',');
		if(olValueList.GetSize() > 4)
		{

			olUrno = olValueList[0];
			olGsnm = olValueList[1];
			olSplu = olValueList[2];
			olVafr = olValueList[3];
			olVato = olValueList[4];

			if (olSplu == opSplu)
			{
				CreateSdtFromSingleGpl(olUrno,olGsnm,olSplu,olVafr,olVato,opLoadStart,opLoadEnd);
			}
		}
	}


}

void DataSet::TerminatePredecessor(long lpSdgu)
{
CCS_TRY
		
	SDGDATA *prlSdg, *prlSdgVorg;
	CString olVorg, olAbge;
	CTime olBegi, olNewEnd;
	CTimeSpan olOneDay(1,0,0,0);
	CCSPtrArray <RecordSet> olList;
	CCSPtrArray <SDTDATA> olSdtData;
	SDTDATA olSdt;
	long lpSdguVorg;

	prlSdg = ogSdgData.GetSdgByUrno(lpSdgu);
	if (prlSdg != NULL){
		olVorg.Format("%d", prlSdg->Dvor);
		olAbge = prlSdg->Abge;
		olBegi = prlSdg->Begi;
		//Gibt es einen Vorgänger bzw. wurde er schon abgelöst
		if(olVorg.IsEmpty() || olAbge == CString("1"))
			return;
	}
	else
		return;
	
	olNewEnd = olBegi - olOneDay;
	//SDGTAB: Repeatings/Ende eintragen
	lpSdguVorg = atol(olVorg);
	prlSdgVorg = ogSdgData.GetSdgByUrno(lpSdguVorg);
	if (prlSdgVorg != NULL){
		//uhi 31.8.00
		prlSdgVorg->Ende = olNewEnd;
		//Repeatings berechnen 
		int ilPeri = atoi(prlSdgVorg->Peri);
		CTime olStart = prlSdgVorg->Begi ;
		CTimeSpan olDays = olNewEnd - olStart + CTimeSpan(0,2,10,0);
		int ilWeeks  = (olDays.GetDays() + 1)/7;
		int ilRepeat = ((ilWeeks - 1)/ilPeri) + 1;
		CString olRepeat;
		olRepeat.Format("%i", ilRepeat);
		strcpy(prlSdgVorg->Dura, olRepeat.GetBuffer(0));
		ogSdgData.UpdateSdg(prlSdgVorg, TRUE);
		ogSdtData.GetSdtByRosu(lpSdguVorg, &olSdtData);
		for(int illc=olSdtData.GetSize()-1; illc>=0; illc--){
			olSdt = olSdtData.GetAt(illc);
			ogSdtData.DeleteSdt(&olSdt);
		}
		CreateVirtuellSdtsBySdgu(prlSdgVorg->Urno);
		ogSdtData.ReleaseVirtuellSdts(prlSdgVorg->Urno);
		CreateVirtuellSdtsBySdgu(prlSdgVorg->Urno);
	}
	else
		return;

	//Vorgänger abgelöst
	strcpy(prlSdg->Abge, CString("1"));
	ogSdgData.UpdateSdg(prlSdg, TRUE);

CCS_CATCH_ALL
}


void DataSet::CreateAutoSdtByBsh(BASICSHIFTS *prpBsh, CString opPfcCode,CTime opShiftStart,
								 CTime opShiftEnd,int ipBreakOffSet, long lpSdgu)
{
	CCS_TRY

	CWaitCursor olWait;

	BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(prpBsh->RUrno);
	if(prpBsh != NULL && prlBsd != NULL)
	{
		SDTDATA *prlSdt = new SDTDATA;
		prlSdt->Urno = ogBasicData.GetNextUrno(); 	// Eindeutige Datensatz-Nr.
		prlSdt->Sdgu = lpSdgu;
		strcpy(prlSdt->Bewc, prlBsd->Bewc); 		// Bewertungsfaktor.code
		strcpy(prlSdt->Bkr1, prlBsd->Bkr1); 		// Pausenlage relativ zu Schichtbeginn oder absolut
		strcpy(prlSdt->Bsdc, prlBsd->Bsdc); 		// Code
		prlSdt->Bsdu = prlBsd->Urno;				// Basisschicht Urno
		strcpy(prlSdt->Bsdk, prlBsd->Bsdk); 		// Kurzbezeichnung
		strcpy(prlSdt->Bsdn, prlBsd->Bsdn); 		// Bezeichnung
		strcpy(prlSdt->Bsds, prlBsd->Bsds); 		// SAP-Code
		strcpy(prlSdt->Ctrc, prlBsd->Ctrc); 		// Vertragsart.code
		strcpy(prlSdt->Type, prlBsd->Type); 		// Flag (statisch/dynamisch)
		strcpy(prlSdt->Usec, ogBasicData.omUserID); 	// Anwender (Ersteller)
		strcpy(prlSdt->Useu, ""); 					// Anwender (letzte Änderung)
		CTime olFrom = CTime::GetCurrentTime();
		prlSdt->Esbg = opShiftStart;			
		prlSdt->Lsen = opShiftEnd;
		prlSdt->Sdu1 = atoi(prlBsd->Sdu1);				
		strcpy( prlSdt->Fctc , opPfcCode);

	
		if(strcmp(prlSdt->Type,"D")==0 && prlSdt->Sdu1 > 0)
		{
			prlSdt->Sbgi = prlSdt->Esbg;
			prlSdt->Seni = prlSdt->Sbgi + CTimeSpan(prlSdt->Sdu1*60);
		}
		if(prlSdt->Esbg > prlSdt->Lsen)
		{
			prlSdt->Lsen = prlSdt->Lsen + CTimeSpan(1, 0, 0, 0);
		}
		prlSdt->Lstu = CTime::GetCurrentTime(); 	// Datum letzte Änderung
		prlSdt->Bkf1 = opShiftStart + (prpBsh->BlFrom - prpBsh->From);				//HourStringToDate( CString( prpBsh->Bkf1), olFrom);	

		prlSdt->Bkt1 = opShiftStart + (prpBsh->BlTo - prpBsh->From);
		prlSdt->Brkf = prlSdt->Bkf1 + CTimeSpan(0,0,ipBreakOffSet,0);

		prlSdt->Brkt = prlSdt->Brkf + (prpBsh->BreakTo - prpBsh->BreakFrom);


		prlSdt->Sex1 = atoi(prlBsd->Sex1);/*[6]*/ 	// Mögliche Arbeitszeitver-längerung
		prlSdt->Ssh1 = atoi(prlBsd->Ssh1);/*[6]*/ 	// Mögliche Arbeitszeitverkürzung
		prlSdt->Bkd1 = atoi(prlBsd->Bkd1);/*[6]*/ 	// Pausenlänge
		prlSdt->Cdat = CTime::GetCurrentTime(); 	// Erstellungsdatum

		if(lpSdgu > 0)
		{
			prlSdt->IsVirtuel = true;
		}
		else
		{
			prlSdt->IsVirtuel = false;
		}
		prlSdt->IsAutoSdt = true;
		
		ogSdtData.AddSdtInternal(prlSdt);
		prlSdt->IsChanged = DATA_NEW;
/*		ogSdtData.UpdateSdt(prlSdt,true,false);*/
	}


	CCS_CATCH_ALL
}


void DataSet::ReleaseAutoSdts(bool blCreateMsd)
{

	CCSPtrArray <SDTDATA> olAutoSdt;

	ogSdtData.GetAllAutoSdts(olAutoSdt);

	if(!blCreateMsd)
	{
		for(int illc = 0; illc < olAutoSdt.GetSize();illc++)
		{
			olAutoSdt[illc].IsAutoSdt = false;
			olAutoSdt[illc].IsChanged = DATA_NEW;
		}
		ogSdtData.Release(&olAutoSdt);
	}
	else
	{
		long llSdgu = 0L;
		MSDDATA *prlMsd = NULL;
		for(int illc = 0; illc < olAutoSdt.GetSize();illc++)
		{
			prlMsd = new MSDDATA;
			if(prlMsd != NULL)
			{
				strcpy(prlMsd->Bewc,olAutoSdt[illc].Bewc);
				strcpy(prlMsd->Bkr1,olAutoSdt[illc].Bkr1);
				strcpy(prlMsd->Bsdc,olAutoSdt[illc].Bsdc);
				strcpy(prlMsd->Bsdk,olAutoSdt[illc].Bsdk);
				strcpy(prlMsd->Bsdn,olAutoSdt[illc].Bsdn);
				strcpy(prlMsd->Bsds,olAutoSdt[illc].Bsds);
				strcpy(prlMsd->Ctrc,olAutoSdt[illc].Ctrc);
				strcpy(prlMsd->Days,olAutoSdt[illc].Days);
				strcpy(prlMsd->Dnam,olAutoSdt[illc].Dnam);
				strcpy(prlMsd->Type,olAutoSdt[illc].Type);
				strcpy(prlMsd->Usec,olAutoSdt[illc].Usec);
				strcpy(prlMsd->Useu,olAutoSdt[illc].Useu);
				strcpy(prlMsd->Fctc,olAutoSdt[illc].Fctc);

				prlMsd->Sdu1 = olAutoSdt[illc].Sdu1;
				prlMsd->Sex1 = olAutoSdt[illc].Sex1;
				prlMsd->Ssh1 = olAutoSdt[illc].Ssh1;
				prlMsd->Bkd1 = olAutoSdt[illc].Bkd1;
				prlMsd->Urno = olAutoSdt[illc].Urno;
				prlMsd->Bsdu = olAutoSdt[illc].Bsdu;
				prlMsd->Brkf = olAutoSdt[illc].Brkf;
				prlMsd->Brkt = olAutoSdt[illc].Brkt;
				prlMsd->Bkf1 = olAutoSdt[illc].Bkf1;
				prlMsd->Bkt1 = olAutoSdt[illc].Bkt1;
				prlMsd->Sbgi = olAutoSdt[illc].Sbgi;
				prlMsd->Seni = olAutoSdt[illc].Seni;
				prlMsd->Cdat = olAutoSdt[illc].Cdat;
				prlMsd->Esbg = olAutoSdt[illc].Esbg;
				prlMsd->Lsen = olAutoSdt[illc].Lsen;
				prlMsd->Lstu = olAutoSdt[illc].Lstu;
				prlMsd->Sdgu = olAutoSdt[illc].Sdgu;
				
				prlMsd->IsChanged = DATA_NEW;

				ogMsdData.AddMsdInternal(prlMsd);
				prlMsd->IsChanged = DATA_NEW;
//!!			ogMsdData.UpdateMsd(prlMsd,true,false);
			
				llSdgu = prlMsd->Sdgu;

				CreateVirtuellSdtsByMsduAndSdgu(prlMsd->Urno,prlMsd->Sdgu);
			}
		}
		
		ogMsdData.Release(llSdgu);

		SDGDATA * prlSdg = ogSdgData.GetSdgByUrno(llSdgu);
		if(prlSdg != NULL && prlSdg->Expd == true)
		{
			prlSdg->Expd = false;
			ogSdgData.UpdateSdg(prlSdg,TRUE);
		}
		ogSdtData.DeleteAutoSdts();
	}
}


