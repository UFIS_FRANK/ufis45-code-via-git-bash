
// cSdtd.cpp - Class for handling Sdt data
//

#include <stdafx.h>
#include <afxwin.h>
#include <CCSCedaData.h>
#include <CCSGlobl.h>
#include <CedaSdgData.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <CCSParam.h>

static int CompareByName(const SDGDATA **e1, const SDGDATA **e2)
{
	return (strcmp((**e2).Dnam,(**e1).Dnam));
}


CedaSdgData::CedaSdgData()
{                  
    // Create an array of CEDARECINFO for STAFFDATA
    BEGIN_CEDARECINFO(SDGDATA, SdtDataRecInfo)
		FIELD_DATE		(Begi, "BEGI")			
		FIELD_DATE		(Ende, "ENDE")
		FIELD_DATE		(Dbeg, "DBEG")
		FIELD_CHAR_TRIM(Days, "DAYS")	
		FIELD_CHAR_TRIM(Dnam, "DNAM")	
		FIELD_CHAR_TRIM(Dura, "DURA")	
		FIELD_CHAR_TRIM(Peri, "PERI")	
		FIELD_CHAR_TRIM(Repi, "REPI")	
		FIELD_LONG     (Dvor, "DVOR")
		FIELD_CHAR_TRIM(Abge, "ABGE")
		FIELD_LONG	   (Urno, "URNO")	
		FIELD_CHAR	   (Flags,0,"EXPD")	
    END_CEDARECINFO
    // Copy the record structure
    for (int i = 0; i < sizeof(SdtDataRecInfo)/sizeof(SdtDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SdtDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
	strcpy(pcmTableName,"SDG");
	strcat(pcmTableName,pcgTableExt);

	strcpy(pcmListOfFields,"BEGI,ENDE,DBEG,DAYS,DNAM,DURA,PERI,REPI,DVOR,ABGE,URNO,EXPD");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	bmOperEnabled = false;

	ClearAll();
}

CedaSdgData::~CedaSdgData()
{
	ogDdx.UnRegister(this,NOTUSED);
	ClearAll();
	
}	


void CedaSdgData::Register(void)
{
	ogDdx.Register(this,BC_SDG_CHANGE,CString("BC_SDG_CHANGE"), CString("SdgDataChange"),ProcessSdgCf);
	ogDdx.Register(this,BC_SDG_NEW,CString("BC_SDG_NEW"),		CString("SdgDataNew"),	 ProcessSdgCf);
	ogDdx.Register(this,BC_SDG_DELETE,CString("BC_SDG_DELETE"), CString("SdgDataDelete"),ProcessSdgCf);
}

bool CedaSdgData::ReadSpecialData(CCSPtrArray<SDGDATA> *popSdt, char *pspWhere, char *pspFieldList, bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[2048] = " ";

	if(strlen(pspFieldList) > 0)
	{
		if( pspFieldList[0] == '*' )
		{
			strcpy(pclFieldList, pcmFieldList);
		}
		else
		{
			strcpy(pclFieldList, pspFieldList);
		}
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSdt != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			SDGDATA *prpSdg = new SDGDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpSdg,CString(pclFieldList))) == true)
			{
				PrepareDataAfterRead(prpSdg);
				popSdt->Add(prpSdg);
			}
			else
			{
				delete prpSdg;
			}
		}
		if(popSdt->GetSize() == 0) return false;
	}
    return true;
}



bool CedaSdgData::Read(char *pspWhere /*NULL*/)
{
    //char where[512];
	//omData.DeleteAll();
	bool ilRc = true;
	char sDay[3]="";
	long Count = 0;
	ClearAll();

	CTime omTime;

	bmOperEnabled = ogCCSParam.GetParamValue(ogGlobal,"ID_OPERATION_FL",CTime::GetCurrentTime().Format("%Y%m%d%H%M%S"),true) == "Y" ? true : false;

    // Select data from the database
	if(pspWhere == NULL)
	{	
		if (CedaAction("RTA", "") == false)
		{
			return false;
		}
	}
	else
	{
		if (CedaAction("RT", pspWhere) == false)
		{
			return false;
		}
	}

    // Load data from CedaData into the dynamic array of record


    for (int ilLc = 0; ilRc == true; ilLc++)
    {
		SDGDATA *prlSdg = new SDGDATA;
		if ((ilRc = GetBufferRecord(ilLc,prlSdg)) == true)
		{
			prlSdg->IsChanged = DATA_UNCHANGED;
			PrepareDataAfterRead(prlSdg);
			AddSdgInternal(prlSdg);
		}
		else
		{
			delete prlSdg;
		}
	}

	omData.Sort(CompareByName);

    return true;
}
bool CedaSdgData::ClearAll()
{
	omOperativeMap.RemoveAll();
	omUrnoMap.RemoveAll();
	omDnamMap.RemoveAll();
    omData.DeleteAll();
    return true;
}


bool CedaSdgData::AddSdgInternal(SDGDATA *prpSdg)
{
	omData.Add(prpSdg);
	omUrnoMap.SetAt((void *)prpSdg->Urno,prpSdg);
	omDnamMap.SetAt((CString)prpSdg->Dnam, prpSdg);
	if (bmOperEnabled)
	{
		if (prpSdg->Operation[0] == 'O')
			omOperativeMap.SetAt((void *)prpSdg->Urno,prpSdg);
	}
	else
	{
		omOperativeMap.SetAt((void *)prpSdg->Urno,prpSdg);
	}
    return true;

}


/////////////////////////////////////////////////////////////////////////////
// Update data methods (called from PrePlanTable class)

bool CedaSdgData::DeleteSdg(SDGDATA *prpSdgData, BOOL bpWithSave)
{

	bool olRc = true;
	ogDdx.DataChanged((void *)this,SDG_DELETE_SELF,(void *)prpSdgData);
	if(prpSdgData->IsChanged == DATA_UNCHANGED)
	{
		prpSdgData->IsChanged = DATA_DELETED;
	}
	if(bpWithSave == TRUE)
	{
		CWaitCursor olWait;
		olRc = (bool) SaveSdg(prpSdgData);
	}

	return olRc;
}

bool CedaSdgData::InsertSdg(SDGDATA *prpSdgData, BOOL bpWithSave)
{
	bool olRc = true;

	SDGDATA *prlSdgData = new SDGDATA;

	*prlSdgData = *prpSdgData;

	AddSdgInternal(prlSdgData);

	
	ogDdx.DataChanged((void *)this, SDG_NEW_SELF ,(void *)prlSdgData);
	prlSdgData->IsChanged = DATA_NEW;
	if(prlSdgData->IsChanged == DATA_UNCHANGED)
	{
		prlSdgData->IsChanged = DATA_CHANGED;
	}
	if(bpWithSave == TRUE)
	{
		CWaitCursor olWait;
		SaveSdg(prlSdgData);
	}

	return olRc;
}

bool CedaSdgData::UpdateSdg(SDGDATA *prpSdgData, BOOL bpWithSave)
{
	bool olRc = true;

	void *polValue;		
	if (this->omOperativeMap.Lookup((void *)prpSdgData->Urno,polValue))
	{
		if (bmOperEnabled)
		{
			if (prpSdgData->Operation[0] != 'O')
			{
				this->omOperativeMap.RemoveKey((void *)prpSdgData->Urno);
			}
		}
		else
		{
			this->omOperativeMap.RemoveKey((void *)prpSdgData->Urno);
		}
	}
	else 
	{
		if (bmOperEnabled)
		{
			if (prpSdgData->Operation[0] == 'O')
			{
				this->omOperativeMap.SetAt((void *)prpSdgData->Urno,prpSdgData);
			}
		}
		else
		{
			this->omOperativeMap.SetAt((void *)prpSdgData->Urno,prpSdgData);
		}
	}
		
	ogDdx.DataChanged((void *)this,SDG_CHANGE_SELF,(void *)prpSdgData);
	if(prpSdgData->IsChanged == DATA_UNCHANGED)
	{
		prpSdgData->IsChanged = DATA_CHANGED;
	}
	if(bpWithSave == TRUE)
	{
		CWaitCursor olWait;
		SaveSdg(prpSdgData);

	}


	return olRc;

}


bool CedaSdgData::SdtExist(long Urno)
{
	// don't read from database anymore, just check internal array
	SDGDATA *prlData;
	bool olRc = true;

	if(omUrnoMap.Lookup((void *)Urno,(void *&)prlData)  == TRUE)
	{
		;//*prpData = prlData;
	}
	else
	{
		olRc = false;
	}
	return olRc;
}

bool CedaSdgData::DnamExist(CString opDnam)
{
	SDGDATA *prlData;
	opDnam.TrimRight();
	if(opDnam.IsEmpty()!=0 || omDnamMap.Lookup(opDnam,(void *&)prlData) == TRUE)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*void CedaSdgData::GetAllDnam(CStringArray &ropList)
{
	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		SDGDATA *prlSdg;
		CString olDnam;
		omDnamMap.GetNextAssoc(rlPos, olDnam, (void *&)prlSdg);
		//ogDdx.DataChanged((void *)this,SDG_CHANGE,(void *)prlSdg);
		ropList.Add(CString(prlSdg->Dnam));
	}
}
*/
/*
void CedaSdgData::GetAllDnam(CStringArray &ropList)
{
	bool ilRc = true;
	char pclFieldList[256] = "DNAM";

	if (CedaAction("RT",pcmTableName, " distinct(DNAM) ", "","",pcgDataBuf) == false)
	{
		return;
	}
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		SDGDATA *prlSdg = new SDGDATA;
		if ((ilRc = GetBufferRecord(ilLc,prlSdg,CString(pclFieldList))) == true)
		{
			ropList.Add(CString(prlSdg->Dnam));
			delete prlSdg;
		}
		else
		{
			delete prlSdg;
		}
	}
}
*/
// the callback function for storing broadcasted fligthdata in FLIGHTDATA array

void  ProcessSdgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	BC_TRY
		ogSdgData.ProcessSdgBc(ipDDXType,vpDataPointer,ropInstanceName);
	BC_CATCH_ALL
}


void  CedaSdgData::ProcessSdgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_SDG_CHANGE :
	case BC_SDG_NEW :
		{
			SDGDATA *prpSdg;
			struct BcStruct *prlSdgData;

			prlSdgData = (struct BcStruct *) vpDataPointer;

			long llUrno;

			if (strstr(prlSdgData->Selection,"WHERE") != NULL)
			{
				llUrno = GetUrnoFromSelection(prlSdgData->Selection);
			}
			else
			{
				llUrno = atol(prlSdgData->Selection);
			}
		
			if (omUrnoMap.Lookup((void*)llUrno,(void *& )prpSdg) == TRUE)
			{
				GetRecordFromItemList(prpSdg,prlSdgData->Fields,prlSdgData->Data);
				ogDdx.DataChanged(this,SDG_CHANGE,prpSdg);
			}
			else
			{
				prpSdg = new SDGDATA;
				GetRecordFromItemList(prpSdg,prlSdgData->Fields,prlSdgData->Data);
				PrepareDataAfterRead(prpSdg);
				AddSdgInternal(prpSdg);
				ogDdx.DataChanged(this,SDG_NEW,prpSdg);
			}
		}
	break;
	case BC_SDG_DELETE :
		{
			SDGDATA *prpSdg;
			struct BcStruct *prlSdgData;

			prlSdgData = (struct BcStruct *) vpDataPointer;

			long llUrno;

			if (strstr(prlSdgData->Selection,"WHERE") != NULL)
			{
				llUrno = GetUrnoFromSelection(prlSdgData->Selection);
			}
			else
			{
				llUrno = atol(prlSdgData->Selection);
			}
		
			
			if (omUrnoMap.Lookup((void *)llUrno,(void *& )prpSdg) == TRUE)
			{
				SDGDATA olSdg(*prpSdg);
				DeleteSdgInternal(prpSdg);
				ogDdx.DataChanged(this, SDG_DELETE,&olSdg);
			}
		}
	break;
	}
}


SDGDATA * CedaSdgData::GetSdgByDnam(CString opDnam)
{

	SDGDATA *prlSdg;

	if (omDnamMap.Lookup(opDnam,(void *& )prlSdg) == TRUE)
	{
		return prlSdg;
	}
	return NULL;

}

long CedaSdgData::GetUrnoByDnam(CString opDnam)
{
	SDGDATA *prlSdg;

	if (omDnamMap.Lookup(opDnam,(void *& )prlSdg) == TRUE)
	{
		return prlSdg->Urno;
	}
	return 0L;
}




SDGDATA * CedaSdgData::GetSdgByUrno(long pcpUrno)
{
	SDGDATA *prlSdg;

	if (omUrnoMap.Lookup((void*)pcpUrno,(void *& )prlSdg) == TRUE)
	{
		return prlSdg;
	}
	return NULL;
}





bool CedaSdgData::DeleteSdgInternal(SDGDATA *prpSdg)
{

	omOperativeMap.RemoveKey((void *)prpSdg->Urno);		
	omUrnoMap.RemoveKey((void *)prpSdg->Urno);
	omDnamMap.RemoveKey((CString)prpSdg->Dnam);

	int ilSdgCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilSdgCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpSdg->Urno)
		{
			omData.DeleteAt(ilLc);
			break;
		}
	}
    return true;
}



bool CedaSdgData::SaveSdg(SDGDATA *prpSdg)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[512];
	char pclData[2048];

	if (prpSdg->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	PrepareDataForWrite(prpSdg);

	switch(prpSdg->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSdg);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpSdg->IsChanged = DATA_UNCHANGED;
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Insert", MB_OK);
		}
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSdg->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSdg);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpSdg->IsChanged = DATA_UNCHANGED;
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Update", MB_OK);
		}
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSdg->Urno);
		olRc = CedaAction("DRT",pclSelection);
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Delete", MB_OK);
		}
		break;
	}

   return olRc;
}







//------------------------------------------------------------------------------
// This function has two different behaviors:
// 1. If the parameter popGhdList is set and has data ==> only these Data are released
// 2. No Parameter ==> All SDT-Data are released
void CedaSdgData::Release(CCSPtrArray<SDGDATA> *popList)
{

	CString olIRT;
	CString olURT;
	CString olDRT;
	CString olFieldList;
	CString olDataList;
	CString olUpdateString;
	CString olInsertString;
	CString olDeleteString;

	int ilNoUpdates = 0;
	int ilNoDeletes = 0;
	int ilNoInserts = 0;
	CStringArray olFields;
	int ilFields = ExtractItemList(CString(ogSdgData.pcmListOfFields),&olFields); 
	olIRT.Format("*CMD*,%s,IRT,%d,", pcmTableName, ilFields);
	olURT.Format("*CMD*,%s,URT,%d,", pcmTableName, ilFields);
	olDRT.Format("*CMD*,%s,DRT,-1,", pcmTableName);

	CString olOrigFieldList;
	int ilCurrentCount = 0;
	if(popList == NULL) //==> Dann alle Speichern
	{
		olOrigFieldList = CString(ogSdgData.pcmListOfFields);
		olInsertString = olIRT + CString(ogSdgData.pcmListOfFields) + CString("\n");
		olUpdateString = olURT + CString(ogSdgData.pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
		olDeleteString = olDRT + CString("[URNO=:VURNO]")+ CString("\n");;

		POSITION rlPos;
		for ( rlPos = ogSdgData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			SDGDATA *prlSdg;
			ogSdgData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdg);
			if(prlSdg->IsChanged != DATA_UNCHANGED )
			{
				CString olListOfData;
				CString olCurrentUrno;
				olCurrentUrno.Format("%d",prlSdg->Urno);
				PrepareDataForWrite(prlSdg);
				MakeCedaData(&omRecInfo,olListOfData,prlSdg);
				switch(prlSdg->IsChanged)
				{
				case DATA_NEW:
					olInsertString += olListOfData + CString("\n");
					ilCurrentCount++;
					ilNoInserts++;
					break;
				case DATA_CHANGED:
					olUpdateString += olListOfData + CString(",") + olCurrentUrno + CString("\n");
					ilNoUpdates++;
					ilCurrentCount++;
					break;
				case DATA_DELETED:
					olDeleteString += olCurrentUrno + CString("\n");
					ilCurrentCount++;
					ilNoDeletes++;
					break;
				}
				prlSdg->IsChanged = DATA_UNCHANGED;
			}
			if(ilCurrentCount == 50)
			{
				if(ilNoInserts > 0)
				{
					CedaAction("REL","LATE","",olInsertString.GetBuffer(0));
					strcpy(ogSdgData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
				}
				if(ilNoUpdates > 0)
				{
					CedaAction("REL","LATE","",olUpdateString.GetBuffer(0));
					strcpy(ogSdgData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
				}
				if(ilNoDeletes > 0)
				{
					CedaAction("REL","LATE","",olDeleteString.GetBuffer(0));
					strcpy(ogSdgData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
				}
				ilCurrentCount = 0;
				ilNoInserts = 0;
				ilNoUpdates = 0;
				ilNoDeletes = 0;
				olInsertString = olIRT + CString(ogSdgData.pcmListOfFields) + CString("\n");
				olUpdateString = olURT + CString(ogSdgData.pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
				olDeleteString = olDRT + CString("[URNO=:VURNO]")+ CString("\n");;
			}
		}//for(rlPos = ......
		if(ilNoInserts > 0)
		{
			CedaAction("REL","LATE","",olInsertString.GetBuffer(0));
			strcpy(ogSdgData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
		}
		if(ilNoUpdates > 0)
		{
			CedaAction("REL","LATE","",olUpdateString.GetBuffer(0));
			strcpy(ogSdgData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
		}
		if(ilNoDeletes > 0)
		{
			CedaAction("REL","LATE","",olDeleteString.GetBuffer(0));
			strcpy(ogSdgData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
		}
	}
	else
	{
		olOrigFieldList = CString(ogSdgData.pcmListOfFields);
		olInsertString = olIRT + CString(ogSdgData.pcmListOfFields) + CString("\n");
		olUpdateString = olURT + CString(ogSdgData.pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
		olDeleteString = olDRT + CString("[URNO=:VURNO]")+ CString("\n");;

		for ( int i = 0; i < popList->GetSize(); i++)
		{
			//SDGDATA *prlSdg = GetSDTByUrno(popList->GetAt(i).Urno);
			SDGDATA rlSDT = popList->GetAt(i);
			//if(prlSdg != NULL)
			{
				if(rlSDT.IsChanged != DATA_UNCHANGED)
				{
					CString olListOfData;
					CString olCurrentUrno;
					olCurrentUrno.Format("%d",rlSDT.Urno);
					MakeCedaData(&omRecInfo,olListOfData,&rlSDT);
					switch(rlSDT.IsChanged)
					{
					case DATA_NEW:
						olInsertString += olListOfData + CString("\n");
						ilNoInserts++;
						ilCurrentCount++;
						break;
					case DATA_CHANGED:
						olUpdateString += olListOfData + CString(",") + olCurrentUrno + CString("\n");
						ilNoUpdates++;
						ilCurrentCount++;
						break;
					case DATA_DELETED:
						olDeleteString += olCurrentUrno + CString("\n");
						ilNoDeletes++;
						ilCurrentCount++;
						break;
					}
					rlSDT.IsChanged = DATA_UNCHANGED;
					if(ilCurrentCount == 50)
					{
						if(ilNoInserts > 0)
						{
							CedaAction("REL","","",olInsertString.GetBuffer(0));
							strcpy(ogSdgData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
						}
						if(ilNoUpdates > 0)
						{
							CedaAction("REL","","",olUpdateString.GetBuffer(0));
							strcpy(ogSdgData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
						}
						if(ilNoDeletes > 0)
						{
							CedaAction("REL","","",olDeleteString.GetBuffer(0));
							strcpy(ogSdgData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
						}
						ilCurrentCount = 0;
						ilNoInserts = 0;
						ilNoUpdates = 0;
						ilNoDeletes = 0;
						olInsertString = olIRT + CString(ogSdgData.pcmListOfFields) + CString("\n");
						olUpdateString = olURT + CString(ogSdgData.pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
						olDeleteString = olDRT + CString("[URNO=:VURNO]")+ CString("\n");;
			
					}
				}
			}
		}//for(rlPos = ......
		if(ilCurrentCount > 0)
		{
			if(ilNoInserts > 0)
			{
				CedaAction("REL","","",olInsertString.GetBuffer(0));
				strcpy(ogSdgData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
			if(ilNoUpdates > 0)
			{
				CedaAction("REL","","",olUpdateString.GetBuffer(0));
				strcpy(ogSdgData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
			if(ilNoDeletes > 0)
			{
				CedaAction("REL","","",olDeleteString.GetBuffer(0));
				strcpy(ogSdgData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
		}
	}
}




void CedaSdgData::GetAllDnam(CStringArray &ropList, CTime opStartTime, CTime opEndTime)
{

	TRACE("\nChart: %s  - %s\n", opStartTime.Format("%d.%m.%Y / %H:%M"), opEndTime.Format("%d.%m.%Y / %H:%M"));

	CTime olStart;
	CTime olEnd;

	CCSPtrArray<TIMEFRAMEDATA> olTimes;

	SDGDATA *prlSdg;

	omData.Sort(CompareByName);

	for(int i = omData.GetSize() -1; i >= 0; i--)
	{
		prlSdg = &omData[i];

		GetTimeFrames(prlSdg->Urno, olTimes);
		for(int j = olTimes.GetSize() -1; j >= 0; j--)
		{

			if(IsOverlapped(opStartTime, opEndTime, olTimes[j].StartTime, olTimes[j].EndTime) == TRUE)
			{
				ropList.Add(CString(prlSdg->Dnam));
				break;
			}
		}
		olTimes.DeleteAll();
	}
}

void CedaSdgData::GetAllDnam(CStringArray & ropList)
{
	SDGDATA *prlSdg;

	omData.Sort(CompareByName);

	for(int i = omData.GetSize() -1; i >= 0; i--)
	{
		prlSdg = &omData[i];

		ropList.Add(prlSdg->Dnam);
	}
}




void CedaSdgData::GetTimeFrames(long lpUrno, CCSPtrArray<TIMEFRAMEDATA> &opTimes)
{
	SDGDATA *prlSdg;
	CStringArray olDates;

	opTimes.DeleteAll();

	CTime olLastDate;
	CCSPtrArray<TIMEFRAMEDATA> olTimes;

	TIMEFRAMEDATA *prlTimeFrame;

	CTimeSpan olOneDay(1,0,0,0);
	CTimeSpan olSevenDay(7,0,0,0);

	prlSdg = GetSdgByUrno(lpUrno);

	if(prlSdg != NULL)
	{
		prlTimeFrame = new TIMEFRAMEDATA;

		prlTimeFrame->StartTime = CTime(prlSdg->Begi.GetYear(), prlSdg->Begi.GetMonth(), prlSdg->Begi.GetDay(), 12,0,0);
		olLastDate = prlTimeFrame->StartTime;		

		prlTimeFrame->EndTime = prlTimeFrame->StartTime;

		for(int j = 1; j < atoi(prlSdg->Days); j++)
		{
			prlTimeFrame->EndTime = prlTimeFrame->EndTime + olOneDay;
		}

		olTimes.Add(prlTimeFrame);


		for(j = 1; j < atoi(prlSdg->Dura); j++)
		{
			prlTimeFrame = new TIMEFRAMEDATA;

//			for(int j = 0; j < atoi(prlSdg->Peri); j++)
//			{
			int ilOffSetDays = (int)(atoi(prlSdg->Peri) * atoi(prlSdg->Days));
			olLastDate += CTimeSpan(ilOffSetDays,0,0,0);
//			}
			prlTimeFrame->StartTime = olLastDate;

			prlTimeFrame->EndTime = prlTimeFrame->StartTime;

			for(int k = 1; k < atoi(prlSdg->Days); k++)
			{
				prlTimeFrame->EndTime = prlTimeFrame->EndTime + olOneDay;
			}
			olTimes.Add(prlTimeFrame);
		}
/****************

		CTime olDate;
		CTimeSpan olSpan;
		ExtractItemList( CString(prlSdg->Repi), &olDates, ';');

		for(int i = 0; i < olDates.GetSize(); i++)
		{
			olDate = DateStringToDate(olDates[i]);
			
			olSpan = olDate - prlSdg->Begi;


			for(j = 0; j < olTimes.GetSize(); j++)
			{
				prlTimeFrame = new TIMEFRAMEDATA;

				prlTimeFrame->StartTime = olTimes[j].StartTime + olSpan;
				prlTimeFrame->EndTime   = olTimes[j].EndTime + olSpan;
				prlTimeFrame->StartTime = CTime(prlTimeFrame->StartTime.GetYear(), prlTimeFrame->StartTime.GetMonth(), prlTimeFrame->StartTime.GetDay(), 0,0,0);
				prlTimeFrame->EndTime   = CTime(prlTimeFrame->EndTime.GetYear(), prlTimeFrame->EndTime.GetMonth(), prlTimeFrame->EndTime.GetDay(), 23,59,59);

				opTimes.Add(prlTimeFrame);
			}
		}
*****************/
		for(int i = olTimes.GetSize() - 1; i >=0 ; i--)
		{
			prlTimeFrame = &olTimes[i];
			prlTimeFrame->StartTime = CTime(prlTimeFrame->StartTime.GetYear(), prlTimeFrame->StartTime.GetMonth(), prlTimeFrame->StartTime.GetDay(), 0,0,0);
			prlTimeFrame->EndTime   = CTime(prlTimeFrame->EndTime.GetYear(), prlTimeFrame->EndTime.GetMonth(), prlTimeFrame->EndTime.GetDay(), 23,59,59);
			opTimes.InsertAt(0, prlTimeFrame);
		}
		olTimes.RemoveAll();
	}

	/*
	TRACE("\n\n-----------------------------------------\n");
	
	for(int q = 0; q < opTimes.GetSize(); q++)
	{
		TRACE("\n %s  %s ", opTimes[q].StartTime.Format("%d.%m.%Y  %H:%M"), opTimes[q].EndTime.Format("%d.%m.%Y  %H:%M"));
	}

	TRACE("\n-----------------------------------------\n");
	*/

}


bool CedaSdgData::CheckSelected(long lpSdgu,bool blSetSelected)
{
	bool blRet = true;
    SDGDATA *prlSdg = GetSdgByUrno(lpSdgu);
	if(prlSdg != NULL)
	{
		blRet = prlSdg->IsSelected;
		/*if(blSetSelected)
		{
			prlSdg->IsSelected = true;
		}*/
		prlSdg->IsSelected = blSetSelected;
	}
	return blRet;
}

bool CedaSdgData::CheckLoaded(long lpSdgu,bool blSetLoaded)
{
	bool blRet = true;
    SDGDATA *prlSdg = GetSdgByUrno(lpSdgu);
	if (prlSdg != NULL)
	{
		blRet = prlSdg->IsLoaded;
		prlSdg->IsLoaded = blSetLoaded;
	}
	return blRet;
}


void CedaSdgData::ResetLoadFlag()
{
	SDGDATA *prlSdg = NULL;

	for (int i = omData.GetSize() -1; i >= 0; i--)
	{
		prlSdg = &omData[i];

		prlSdg->IsLoaded = false;
	}

}

void CedaSdgData::PrepareDataAfterRead(SDGDATA *prpSdg)
{
	prpSdg->Expd = prpSdg->Flags[0] == '1' ? true : false;
	if (bmOperEnabled)
	{
		if (prpSdg->Flags[1] == ' ')
			prpSdg->Operation[0] = 'O';
		else
			prpSdg->Operation[0] = prpSdg->Flags[1];
	}
	else
	{
		prpSdg->Operation[0] = ' ';
	}
	prpSdg->Operation[1] = '\0';
}

void CedaSdgData::PrepareDataForWrite(SDGDATA *prpSdg)
{
	prpSdg->Flags[0] = prpSdg->Expd == true ? '1' : '0';
	prpSdg->Flags[1] = prpSdg->Operation[0];
}