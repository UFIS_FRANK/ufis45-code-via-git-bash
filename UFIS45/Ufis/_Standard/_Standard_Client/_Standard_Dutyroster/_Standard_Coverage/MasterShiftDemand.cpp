// MasterShiftDemand.cpp: Implementierungsdatei
//
 
#include <stdafx.h>
#include <CCSEdit.h>
#include <MasterShiftDemand.h>
#include <CCSTime.h>
#include <BasicData.h>
//#include "CedaSdgData.h"
//#include "CedaBsdData.h"
#include <DataSet.h>
#include <CedaMsdData.h>
#include <CCSParam.h>
#include <CopyShiftRosterRequirementDlg.h>
#include <SelectDateDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld MasterShiftDemand 
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

bool bmIsInit;

static int CompareMsdTimes(const MSDTIME **e1, const MSDTIME **e2)
{
	return ((**e1).Time == (**e2).Time)? 0:
		((**e1).Time > (**e2).Time)? 1: -1;

}

MasterShiftDemand::MasterShiftDemand(CWnd* pParent /*=NULL*/, SDGDATA *prpSdg, CTime opStart , CTime opEnd )
	: CDialog(MasterShiftDemand::IDD, pParent)
{
	CCS_TRY
	//{{AFX_DATA_INIT(MasterShiftDemand)
	//}}AFX_DATA_INIT

	this->EnableToolTips(TRUE);

	if (prpSdg != NULL)
	{
		rmOrgSdg = *prpSdg;
		rmSdg = *prpSdg;
	}

	omLoadStart = opStart;
	omLoadEnd   = opEnd;

	//	Stichtag berechnen
	CTime olTimeNow = CTime::GetCurrentTime();
	CString olStringNow = olTimeNow.Format("%Y%m%d%H%M%S");

	bmOperEnabled = ogCCSParam.GetParamValue(ogGlobal,"ID_OPERATION_FL",CTime::GetCurrentTime().Format("%Y%m%d%H%M%S"),true) == "Y" ? true : false;

	imLastInput = atoi(ogCCSParam.GetParamValue(ogGlobal,"ID_LAST_INPUT",olStringNow));

	imNextMonth = atoi(ogCCSParam.GetParamValue(ogGlobal,"ID_NEXT_MONTH",olStringNow));
	if (imNextMonth <= 0 || imNextMonth > 12)
		imNextMonth = 1;

	bmCheckValidity = false;
		

	bmReadOnly = false;
	bmCopy = false;

	if (rmSdg.Urno > 0)
	{
		int ilYear, ilMonth;
		ilYear = olTimeNow.GetYear();

		if (rmSdg.Operation[0] == 'O' && this->imLastInput >= 0)
		{
			this->bmCheckValidity = true;
			if (olTimeNow.GetDay() <= imLastInput)
			{
				ilMonth = olTimeNow.GetMonth() + imNextMonth;
			}
			else if (olTimeNow.GetDay() > imLastInput)
			{
				ilMonth = olTimeNow.GetMonth() + imNextMonth + 1;	
			}

			if (ilMonth > 12)
			{
				ilMonth = ilMonth - 12;
				ilYear = ilYear + 1;
			}
		}
		else
		{
			this->bmCheckValidity = false;
			ilMonth = olTimeNow.GetMonth();
		}

		omValidInput = CTime(ilYear, ilMonth, 1, 0, 0, 0);

		bmReadOnly = true;
		bmCopy = true;
		
		imWeeks = 10;

		imDays= atoi(rmSdg.Days);
		imDura = atoi(rmSdg.Dura);
		imPeri = atoi(rmSdg.Peri);
		omBegi = rmSdg.Begi;
		omEnde = rmSdg.Ende;
		omDispBegi = rmSdg.Dbeg;
		omDnam = CString(rmSdg.Dnam);
		omDvor.Format("%d", rmSdg.Dvor);
		omAbge = CString(rmSdg.Abge);

		ExtractItemList(rmSdg.Repi, &omRepi, ';');		

	}
	else
	{
		int ilYear, ilMonth;
		ilYear = olTimeNow.GetYear();
		ilMonth = olTimeNow.GetMonth();
								
		omValidInput = CTime(ilYear, ilMonth, 1, 0, 0, 0);


		CTimeSpan omOneDay(1, 0, 0, 0);
		CTimeSpan omSixDays(6, 0, 0, 0);
		
		//	Auf Stichtag folgenden Montag anzeigen
		omBegi = omValidInput;
		while (omBegi.GetDayOfWeek() != 2)
			omBegi = omBegi + omOneDay;
		
		omDispBegi = omBegi; 
		//	Den Sonntag der darauffolgende Woche anzeigen
		omEnde = omBegi + omSixDays;
		
		imWeeks = 10;
		imDays = 7;
		imDura = 1;
		imPeri = 1;
	}

	bmIsInit = false;


	CCS_CATCH_ALL
}

MasterShiftDemand::~MasterShiftDemand()
{
	CCS_TRY
		omDateArray.DeleteAll();
	CCS_CATCH_ALL
}


void MasterShiftDemand::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(MasterShiftDemand)
	DDX_Control(pDX, IDOK, m_CB_OK);
	//uhi 29.8.00
	DDX_Control(pDX, IDC_COPY, m_Copy);
	DDX_Control(pDX, IDC_HELPER, m_Helper);
	//DDX_Control(pDX, IDC_LIST, m_List);
	//DDX_Control(pDX, IDC_DAYS, m_Days);
	DDX_Control(pDX, IDC_BEGI, m_Begi);
	DDX_Control(pDX, IDC_ENDE, m_Ende);
	DDX_Control(pDX, IDC_DISPBEGI, m_DispBegi);
	DDX_Control(pDX, IDC_DNAM, m_Dnam);
	DDX_Control(pDX, IDC_DURA, m_Dura);
	DDX_Control(pDX, IDC_PERI, m_Peri);
	DDX_Control(pDX, IDC_VORG, m_Vorg);
	DDX_Control(pDX, IDC_OPER, m_Oper);
	DDX_Control(pDX, IDC_BUTTON_BEGI, m_BtnBegi);
	DDX_Control(pDX, IDC_BUTTON_ENDE, m_BtnEnde);
	DDX_Control(pDX, IDC_BUTTON_DISPBEGI, m_BtnDispBegi);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(MasterShiftDemand, CDialog)
	//{{AFX_MSG_MAP(MasterShiftDemand)
	ON_WM_CONTEXTMENU()
	ON_WM_KEYDOWN()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_PAINT()
	ON_WM_RBUTTONDOWN()
	ON_EN_KILLFOCUS(IDC_BEGI, OnKillfocusBegi)
	ON_EN_KILLFOCUS(IDC_ENDE, OnKillfocusEnde)
	ON_EN_KILLFOCUS(IDC_DISPBEGI, OnKillfocusDispBegi)
	ON_EN_KILLFOCUS(IDC_DURA, OnKillfocusDura)
	ON_EN_KILLFOCUS(IDC_PERI, OnKillfocusPeri)
	ON_EN_CHANGE(IDC_DURA, OnChangeDura)
	ON_EN_CHANGE(IDC_PERI, OnChangePeri)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_NEW, OnNew)
	ON_BN_CLICKED(IDC_COPY, OnCopy)
	ON_NOTIFY_EX( TTN_NEEDTEXT, 0,OnTtnNeedText)
	ON_MESSAGE(WM_TABLE_IPEDIT_KILLFOCUS, OnTableIPEditKillfocus)
	ON_CBN_SELCHANGE(IDC_OPER, OnSelchangeOper)
	ON_BN_CLICKED(IDC_BUTTON_BEGI, OnBegi)
	ON_BN_CLICKED(IDC_BUTTON_ENDE, OnEnde)
	ON_BN_CLICKED(IDC_BUTTON_DISPBEGI, OnDispBegi)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten MasterShiftDemand 

void MasterShiftDemand::OnOK() 
{
	CCS_TRY
	CString olDnam, olUrno;
	SDGDATA *polSdg;

	// TODO: Zus�tzliche Pr�fung hier einf�gen
	SYSTEMTIME rlLocalTime, rlUtcTime;
	
	//uhi 29.8.00
	bool bmCopyFlag = false;

	GetLocalTime(&rlLocalTime);
	GetSystemTime(&rlUtcTime);
	int i = 0;

	CString olEnde, olPeri, olDura;
	m_Ende.GetWindowText(olEnde);
	m_Peri.GetWindowText(olPeri);
	m_Dura.GetWindowText(olDura);
	
	//Periode leer?
	if(olPeri.IsEmpty())
		return;

	//Ende und Dauer leer?
	if(olEnde.IsEmpty() && olDura.IsEmpty())
		return;

	if(olDura.IsEmpty())
		CalcRepeatings();
	else
		CalcEndTime();

	//Name
	m_Dnam.GetWindowText(omDnam);

	strcpy(rmSdg.Dnam, omDnam);
	omDnam.TrimRight();
	if(omDnam.IsEmpty())
	{
		MessageBox( LoadStg(IDS_STRING878), LoadStg(IDS_WARNING));
		return;
	}

	if(ogSdgData.DnamExist(omDnam))
	{
		if((rmSdg.Urno != rmOrgSdg.Urno) || (rmSdg.Urno == 0))
		{
			MessageBox( LoadStg(IDS_STRING128), LoadStg(IDS_WARNING));
			return;
		}
	}

	//Tage (immer 7)
	sprintf(rmSdg.Days, "%d", imDays);
	//Periode
	sprintf(rmSdg.Peri, "%d", imPeri);
	//Dauer
	sprintf(rmSdg.Dura, "%d", imDura);

	//Anfangsdatum
	CString olTmp;
	m_Begi.GetWindowText(olTmp);
	olTmp.TrimRight();
	if( CheckDDMMYYValid( olTmp))
	{
		omBegi = DateStringToDate(olTmp);
		if(omBegi.GetDayOfWeek() != 2){
			
			MessageBox(LoadStg(IDS_STRING852), LoadStg(IDS_WARNING));
			return;
			
			//uhi 11.9.00 Auf Stichtag folgenden Montag anzeigen
			/*CTimeSpan omOneDay(1, 0, 0, 0);
			omBegi = omValidInput;
			while (omBegi.GetDayOfWeek() != 2)
				omBegi = omBegi + omOneDay;
			m_Begi.SetWindowText(omBegi.Format("%d.%m.%Y"));*/
		}
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING851), LoadStg(IDS_WARNING));
		return;
	}

	//Anfangsdatum >= Stichtag
	if(omBegi <= omValidInput && bmCheckValidity)
	{
		MessageBox(LoadStg(IDS_STRING851) + "\n" + omBegi.Format("%d.%m.%Y"), LoadStg(IDS_WARNING));
		return;
	}

	rmSdg.Begi = omBegi;

	//Endedatum
	m_Ende.GetWindowText(olTmp);
	olTmp.TrimRight();
	if( CheckDDMMYYValid( olTmp))
	{
		omEnde = DateStringToDate(olTmp);
		if(omEnde.GetDayOfWeek() != 1){
			CalcEndTime();
			m_Ende.GetWindowText(olTmp);
			olTmp.TrimRight();
			omEnde = DateStringToDate(olTmp);
			//MessageBox(LoadStg(IDS_STRING855), LoadStg(IDS_WARNING));
			//return;	
		}
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING851) + "\n" + omEnde.Format("%d.%m.%Y"), LoadStg(IDS_WARNING));
		return;
	}

	rmSdg.Ende = omEnde;
	
	//Datum Anzeige ab 
	m_DispBegi.GetWindowText(olTmp);
	olTmp.TrimRight();
	if( CheckDDMMYYValid( olTmp))
	{
		omDispBegi = DateStringToDate(olTmp);
	}
	else
	{
		//omDispBegi = omBegi;
		MessageBox(LoadStg(IDS_STRING851) + "\n" + omDispBegi.Format("%d.%m.%Y"), LoadStg(IDS_WARNING));
		return;
	}

	rmSdg.Dbeg = omDispBegi;

	//Vorg�nger
	int iVor = m_Vorg.GetCurSel();
	if (iVor != CB_ERR){
		m_Vorg.GetLBText(iVor, olDnam);
		
		if(omDnam == olDnam){
			MessageBox(LoadStg(IDS_STRING61392), LoadStg(IDS_WARNING));
			return;	
		}
		
		polSdg = ogSdgData.GetSdgByDnam(olDnam);
		if(polSdg != NULL){
			//uhi 11.9.00 Ende Vorg�nger > Anfang Neu (�berschneidung)
			CTime olEnde = polSdg->Ende;
			CTimeSpan olOneDay(1, 0 ,0 ,0);
			if(omBegi > olEnde + olOneDay){
				MessageBox(LoadStg(IDS_STRING5),LoadStg(IDS_STRING1734), MB_OK);
				return;
			}
			
			olUrno.Format("%d", polSdg->Urno);
			rmSdg.Dvor = polSdg->Urno;
			omDvor = olUrno;
		}
		else
		{
			omDvor = "";
			rmSdg.Dvor = -1;
		}

	}
	else
	{
		omDvor = "";
		rmSdg.Dvor = -1;
	}


	bmBegChanged = false;
	if(rmSdg.Begi != rmOrgSdg.Begi)
	{
		bmBegChanged = true;
	}

	bmSdgChanged = false;
	if(rmSdg.Begi != rmOrgSdg.Begi || rmSdg.Ende != rmOrgSdg.Ende || strcmp(rmSdg.Days, rmOrgSdg.Days)!= 0 || 
		strcmp(rmSdg.Dura,rmOrgSdg.Dura) !=0 || strcmp(rmSdg.Peri, rmOrgSdg.Peri) != 0 ||
		strcmp(rmSdg.Repi, rmOrgSdg.Repi) != 0 || strcmp(rmSdg.Dnam,rmOrgSdg.Dnam) != 0 ||
		rmSdg.Dvor != rmOrgSdg.Dvor)
	{
		rmSdg.Expd =  false;
		bmSdgChanged = true;
	}

	if (bmOperEnabled)
	{
		char olOperation   = m_Oper.GetItemData(m_Oper.GetCurSel());
		if (olOperation != rmSdg.Operation[0])
		{
			rmSdg.Operation[0] = olOperation;
			bmSdgChanged = true;
		}
	}
	else
	{
		rmSdg.Operation[0] = ' ';
	}

	if(rmSdg.Urno == 0)
	{
		rmSdg.Urno = ogBasicData.GetNextUrno();
		strcpy(rmSdg.Abge, CString("0"));
		ogSdgData.InsertSdg(&rmSdg, TRUE);
		//uhi 29.8.00
		if(bmCopy)
			bmCopyFlag = true;
	}
	else
	{
		SDGDATA *prpSdgData = ogSdgData.GetSdgByUrno(rmSdg.Urno);
		if (prpSdgData != NULL)
		{
			if(rmSdg.Dvor != rmOrgSdg.Dvor)
				strcpy(rmSdg.Abge, CString("0"));
			*prpSdgData = rmSdg;
			ogSdgData.UpdateSdg(prpSdgData, TRUE);
		}
	}

	//uhi 17.01.01 Ladezeitraum und Master stimmen nicht �berein
	bool blTreffer = true;
	
	CCSPtrArray<TIMEFRAMEDATA> olTimes;

	ogSdgData.GetTimeFrames(rmSdg.Urno,  olTimes);
	if((omLoadStart != TIMENULL) && (omLoadEnd != TIMENULL)){
		for(int j = olTimes.GetSize() -1; j >= 0; j--){
			blTreffer = false;
			if(IsOverlapped(omLoadStart, omLoadEnd, olTimes[j].StartTime, olTimes[j].EndTime) == TRUE){
				blTreffer = true;
				break;
			}
		}

	}
	olTimes.DeleteAll();

	if(!blTreffer)
		MessageBox(LoadStg(IDS_STRING183), LoadStg(IDS_WARNING));

		/*	{
		if(MessageBox( LoadStg(IDS_STRING182), LoadStg(IDS_WARNING), MB_YESNO) == IDNO){
			return;
		}*/
		
	//uhi 29.8.00
	//Kopieren
	long olOldUrno, olNewUrno, llMsdUrno;
	CCSPtrArray <MSDDATA> olList;
	CString olSdgu, olFctc, olBewc, olBsdc, olBsdu, olBsdk, olBsdn;
	CString	olBsds, olCtrc, olType, olUsec, olUseu, olLstu, olSdu1, olSex1;
	CString	olSsh1, olBkd1, olCdat, olEsbg, olLsen, olBkr1;
	CString olBrkf, olBrkt, olBkf1, olBkt1, olSbgi, olSeni;
	SDGDATA *prlSdg;
	CTime olOldStart, olNewStart;
	CTimeSpan olOffset;

	CWaitCursor olWait;
	if (bmCopyFlag)
	{
		olOldUrno = rmOrgSdg.Urno;
		olNewUrno = rmSdg.Urno;
		
		olOldStart = rmOrgSdg.Begi;
		olNewStart = rmSdg.Begi;
		olOffset = olNewStart - olOldStart;
		CTimeSpan olTotalOffSet = olOffset;
		CTimeSpan olMsdOffSet = olOffset;
		int test = olOffset.GetDays();


	
		ogMsdData.GetMsdsBySdgu(olOldUrno, olList);
		for (int illc=0; illc<olList.GetSize(); illc++)
		{
			for (int ilCopy = 0; ilCopy < this->imCountOfCopies; ilCopy++)
			{
				MSDDATA *prlMsd = new MSDDATA;
				MSDDATA *prlOrgMsd = &olList[illc];

				olTotalOffSet = olOffset;

				if(prlMsd != NULL && prlOrgMsd != NULL)
				{
					*prlMsd = *prlOrgMsd;
					prlMsd->Sdgu = olNewUrno;
					strcpy(prlMsd->Usec,ogBasicData.omUserID);
					strcpy(prlMsd->Useu,ogBasicData.omUserID);
					prlMsd->Lstu = CTime::GetCurrentTime();
					prlMsd->Cdat = prlMsd->Lstu;
		
					CTime olMsdStart = prlMsd->Esbg;
					if(olMsdStart == TIMENULL)
					{
						olMsdStart = prlMsd->Sbgi;
					}

					olMsdOffSet = olMsdStart - olOldStart;

					while(olMsdOffSet.GetDays() > 6)
					{
						olMsdOffSet-= CTimeSpan(7,0,0,0);
						olTotalOffSet-= CTimeSpan(7,0,0,0);
					}


					

					prlMsd->Bkf1 +=olTotalOffSet;
					prlMsd->Bkt1 +=olTotalOffSet;
					prlMsd->Brkf +=olTotalOffSet;
					prlMsd->Brkt +=olTotalOffSet;
					prlMsd->Esbg +=olTotalOffSet;
					prlMsd->Lsen +=olTotalOffSet;
					prlMsd->Sbgi +=olTotalOffSet;
					prlMsd->Seni +=olTotalOffSet;

					llMsdUrno = ogBasicData.GetNextUrno();
					prlMsd->Urno = llMsdUrno;

					
					strcpy(prlMsd->Fctc,ReplaceFunctionWith(prlMsd->Fctc));

					ogMsdData.AddMsdInternal(prlMsd);
					prlMsd->IsChanged = DATA_NEW;
				}
			}
		}

		ogMsdData.Release(olNewUrno);
		olList.RemoveAll();
		
		prlSdg = ogSdgData.GetSdgByUrno(olNewUrno);
		if (prlSdg != NULL)
		{
			prlSdg->Expd = false;
			ogSdgData.UpdateSdg(prlSdg,TRUE);
		}
	}

	CDialog::OnOK();
	CCS_CATCH_ALL
}

void MasterShiftDemand::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen
	
}

BOOL MasterShiftDemand::OnInitDialog() 
{
	CCS_TRY
	long ilUrno;
	SDGDATA *polSdg;
		
	CDialog::OnInitDialog();

	CWnd *polWnd = GetDlgItem(IDCANCEL);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING999));
	}
	polWnd = GetDlgItem(IDOK);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING565));
	}
	polWnd = GetDlgItem(IDC_NEW);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(SHIFT_B_NEW));
	}
	polWnd = GetDlgItem(IDC_BNAME);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61316));
	}
	polWnd = GetDlgItem(IDC_PBEGI);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61317));
	}
	polWnd = GetDlgItem(IDC_PDIST);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61318));
	}
	polWnd = GetDlgItem(IDC_DBEGI);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61319));
	}

	polWnd = GetDlgItem(IDC_DURAD);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61320));
	}
	polWnd = GetDlgItem(IDC_REPLAY);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61321));
	}

	polWnd = GetDlgItem(IDC_OPERATION);
	if (polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1890));
	}
	SetWindowText(LoadStg(IDS_STRING61315));


	SetDlgItemText(IDC_PENDE, LoadStg(IDS_STRING61329));
	SetDlgItemText(IDC_COPY, LoadStg(SHIFT_B_COPY));
	SetDlgItemText(IDC_MVOR, LoadStg(IDS_STRING61359));

	if (bmOperEnabled)
	{
		int ind = m_Oper.AddString(LoadStg(IDS_STRING1891));
		m_Oper.SetItemData(ind,'P');

		ind = m_Oper.AddString(LoadStg(IDS_STRING1892));
		m_Oper.SetItemData(ind,'O');

		ind = m_Oper.AddString(LoadStg(IDS_STRING1893));
		m_Oper.SetItemData(ind,'A');

		if (rmSdg.Urno > 0)
		{
			for (int i = 0; i < m_Oper.GetCount(); i++)
			{
				if (m_Oper.GetItemData(i) == rmSdg.Operation[0])
				{
					m_Oper.SetCurSel(i);
					break;
				}
			}
		}
		else
		{
			for (int i = 0; i < m_Oper.GetCount(); i++)
			{
				if (m_Oper.GetItemData(i) == 'P')
				{
					m_Oper.SetCurSel(i);
					break;
				}
			}
		}
	}
	else
	{
		m_Oper.ShowWindow(SW_HIDE);
		CWnd *polWnd = GetDlgItem(IDC_OPERATION);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
	}

	if (rmSdg.Urno == 0)
	{
		CalcBeginTime();
	}

	omOrgDispBegi = omDispBegi;
	for(int i = 0; i < 50; i++)
	{
		MSDTIME rlMsdTime;
		rlMsdTime.Time = TIMENULL;
		omDateArray.NewAt(0, rlMsdTime);
	}

	omStartTime = omBegi;
	omStartTime = DBStringToDateTime(omStartTime.Format("%Y%m%d120000"));
	omEndeTime = omEnde;
	omEndeTime = DBStringToDateTime(omEndeTime.Format("%Y%m%d120000"));
	

	if(bmReadOnly)
	{
		m_Dnam.SetReadOnly();
		m_BtnBegi.EnableWindow(FALSE);
	}
	
	//uhi 29.8.00
	if(!bmCopy)
		m_Copy.EnableWindow(false);

	CRect olGraphRect;
	m_Helper.GetWindowRect(olGraphRect);
	
	m_Begi.SetWindowText(omBegi.Format("%d.%m.%Y"));
	m_Ende.SetWindowText(omEnde.Format("%d.%m.%Y"));
	m_DispBegi.SetWindowText(omDispBegi.Format("%d.%m.%Y"));
	m_Dnam.SetWindowText(omDnam);

	CString /*olDays,*/ olDura, olPeri;
	//olDays.Format("%d", imDays);
	olDura.Format("%d", imDura);
	olPeri.Format("%d", imPeri);
	//m_Days.SetWindowText(olDays);
	m_Dura.SetWindowText(olDura);
	m_Peri.SetWindowText(olPeri);


	//Nur die im Ladezeitraum relevanten Master anzeigen
	m_Vorg.AddString(CString(""));
	CStringArray olDnams;
	ogSdgData.GetAllDnam(olDnams, omLoadStart, omLoadEnd); 
	for(i=0; i<olDnams.GetSize();i++)
		m_Vorg.AddString(CString(olDnams[i]));
	
	if (!omDvor.IsEmpty()){
		ilUrno = atol(omDvor);
		polSdg = ogSdgData.GetSdgByUrno(ilUrno);
		if(polSdg != NULL){
			//uhi 17.01.01 Vorg�nger auch anzeigen, wenn im Ladezeitraum nicht relevant 
			int iRet = m_Vorg.SelectString(0, polSdg->Dnam);
			if(iRet == CB_ERR){
				m_Vorg.InsertString(0, CString(polSdg->Dnam));
				m_Vorg.SelectString(0, polSdg->Dnam);
			}
		}
		else
			m_Vorg.SetCurSel(0);
	}
	else
		m_Vorg.SetCurSel(0);

	bmIsInit = true;

	Invalidate();
	// TODO: Zus�tzliche Initialisierung hier einf�gen
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
	CCS_CATCH_ALL
	return FALSE;
}

void MasterShiftDemand::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CCS_TRY
	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen und/oder Standard aufrufen
	
	CDialog::OnKeyDown(nChar, nRepCnt, nFlags);
	CCS_CATCH_ALL
}

void MasterShiftDemand::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen und/oder Standard aufrufen
	CCS_TRY
	CDialog::OnLButtonDown(nFlags, point);
	CCS_CATCH_ALL
}

void MasterShiftDemand::OnMouseMove(UINT nFlags, CPoint point) 
{
	CCS_TRY
	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen und/oder Standard aufrufen
	
	CDialog::OnMouseMove(nFlags, point);
	CCS_CATCH_ALL
}

void MasterShiftDemand::OnPaint() 
{
	CCS_TRY
	CPaintDC dc(this); // device context for painting
	

	int i = 0, 
		j = 0, 
		k = 0;
	
	if(omStartTime ==TIMENULL)
	{
		printf("");
		//omStartTime = CTime::GetCurrentTime();
		// Schreibt current time wieder ins Editfeld
		//m_Begi.SetWindowText(omStartTime.Format("%d.%m.%Y"));
	}

	CCSPtrArray<MSDTIME> olDateArray;
	for(i = 0; i < omDateArray.GetSize(); i++)
	{
		if(omDateArray[i].Time != TIMENULL && omDateArray[i].valid == true)
		{
			olDateArray.NewAt(olDateArray.GetSize(), omDateArray[i]);
		}
	}
	MSDTIME rlBaseTime;
	olDateArray.Sort(CompareMsdTimes);
	rlBaseTime.Time = omStartTime; 
	rlBaseTime.valid = true;
	olDateArray.NewAt(0,rlBaseTime);
	if((imDura * imDays * imPeri) == 0)
	{
		imWeeks = 10;
	}
	else
	{
		if(olDateArray.GetSize() == 0)
		{
			imWeeks = (int)((imDura * imDays * imPeri)/7)+1;
		}
		else
		{
			CTime olE = olDateArray[olDateArray.GetSize()-1].Time + CTimeSpan((imDura * imDays * imPeri), 0, 0, 0);
			CTimeSpan olDiff = olE - omStartTime;
			imWeeks = (int)((olDiff.GetDays())/7)+1;
		}
	}

	if (imWeeks == 0)
		return;

	CRect olGraphRect;
	CRect olWndRect;
	GetClientRect(&olWndRect);
	m_Helper.GetWindowRect(&olGraphRect);
	ScreenToClient(&olGraphRect);
	olGraphRect.left = olWndRect.left + 10;
	olGraphRect.right = olWndRect.right - 10;
	CRect olClientRect(olGraphRect/*20, 391, 734,467*/);
//	GetClientRect(&olClientRect);
	CPen olBlackPen, 
		 olBluePen,
		 olWhitePen,
		 olBlueDotPen,
		 *polOldPen;
	int ilWidth = olClientRect.right - olClientRect.left;
	int ilHeight = olClientRect.bottom - olClientRect.top;
	int ilHeader = (int)((olClientRect.bottom - olClientRect.top)/4);
	dc.SetBkMode(TRANSPARENT);
	olBlackPen.CreatePen(PS_SOLID, 1, COLORREF(BLACK));
	olBluePen.CreatePen(PS_SOLID, 1, COLORREF(BLUE));
	olBlueDotPen.CreatePen(PS_DOT, 1, COLORREF(BLUE));
	olWhitePen.CreatePen(PS_SOLID, 1, COLORREF(WHITE));

	polOldPen = dc.SelectObject(&olBlackPen);
	dc.MoveTo(olClientRect.left, olClientRect.top);
	dc.LineTo(olClientRect.left, olClientRect.bottom);
	dc.LineTo(olClientRect.right, olClientRect.bottom);
	dc.LineTo(olClientRect.right, olClientRect.top);
	dc.LineTo(olClientRect.left, olClientRect.top);

//  Calculate The Beginning Date
	CTime olStartTime;
	olStartTime = omStartTime;
	int ilDayOfWeek = omStartTime.GetDayOfWeek();
//	olStartTime -= CTimeSpan(ilDayOfWeek, 0, 0, 0);
	if(ilDayOfWeek == 2)
	{
		ilDayOfWeek = 0;
	}
	else
	{
		ilDayOfWeek--;
	}

//10 Percent from left as startpoint, same from right as stoppoint
	int ilStartX = olClientRect.left + (int)(ilWidth/25);
	int ilEndX = olClientRect.right - (int)(ilWidth/25);
	double ilX = ilStartX;
	double ilWeekStep = ((ilEndX - ilStartX)/imWeeks);
	double ilOneDay = ilWeekStep/7; 
	dc.SelectObject(&olBluePen);
	dc.SelectObject(&ogSmallFonts_Regular_7);
	for(i = 0; i <= imWeeks; i++)
	{
		char clHeader[20]="";
		int ilTop = olClientRect.top;
		
		sprintf(clHeader, "%s", olStartTime.Format("%d.%m"));
		dc.SetTextColor(COLORREF(BLUE));
        dc.TextOut((int) ilX - (dc.GetTextExtent(clHeader, strlen (clHeader)).cx / 2), 
			ilTop + ((dc.GetTextExtent(clHeader, strlen (clHeader)).cy / 2)),
            clHeader, strlen (clHeader));
//		dc.MoveTo((int)ilX, olClientRect.top + (int)((olClientRect.top + ilHeader)/2));
//		dc.LineTo((int)ilX, olClientRect.top + ilHeader);
		olStartTime += CTimeSpan(7, 0, 0, 0);

		ilX += ilWeekStep;
	}

	ilX = ilStartX;
	dc.SelectObject(&olBlueDotPen);
	for(i = 0; i <= imWeeks; i++)
	{
		dc.MoveTo((int)ilX, olClientRect.top + ilHeader);
		dc.LineTo((int)ilX, olClientRect.bottom);
		ilX += ilWeekStep;
	}
	dc.SelectObject(&olWhitePen);
	dc.MoveTo(olClientRect.left+1, olClientRect.top + ilHeader);
	dc.LineTo(olClientRect.right-1, olClientRect.top + ilHeader);
	dc.SelectObject(&olBlackPen);
	dc.MoveTo(olClientRect.left+1, olClientRect.top + ilHeader+1);
	dc.LineTo(olClientRect.right-1, olClientRect.top + ilHeader+1);

//Paint the bars
	ilX = ilStartX;

	CTimeSpan olFirstBarSpan;
	CTime olEndTime = omStartTime + CTimeSpan(imDays, 0, 0, 0); 
	olFirstBarSpan = olEndTime - omStartTime;
	double ilFirstBarLen = (double)((double)olFirstBarSpan.GetDays() * ilOneDay);
	ilX += (ilDayOfWeek%7) * ilOneDay;
	int ilTop = olClientRect.top + ilHeader + 20;
	int ilBottom = olClientRect.bottom -20; 
	int ilBarCount = 0;
	CBrush olAquaBrush(COLORREF(AQUA));
	CBrush olBlackBrush(COLORREF(BLACK));
	dc.SelectObject(&olAquaBrush);


	ilBarCount = imDura /* imPeri*/;
	for(j = 0; j < olDateArray.GetSize(); j++)
	{

		ilX = ilStartX;
		CTimeSpan olDaySpan;
		olDaySpan = olDateArray[j].Time - omStartTime;
		if(j == 0)
		{
			ilX += ((ilDayOfWeek%7) + olDaySpan.GetDays()) * ilOneDay;
		}
		else
		{
			ilX += ((ilDayOfWeek%7) + olDaySpan.GetDays()+1) * ilOneDay;
		}
		for(i = 0; i < ilBarCount; i++)
		{
			int ilRight = (int)ilX + (int)ilFirstBarLen;
			if(ilRight > olClientRect.right)
			{
				ilRight = olClientRect.right-1;
			}
			CRect olR = CRect((int)ilX, ilTop, (int)ilX + (int)ilFirstBarLen, ilBottom);
			dc.FillRect(&olR, &olAquaBrush);
			dc.FrameRect(&olR, &olBlackBrush);
			ilX += imPeri * (int)ilFirstBarLen;
		}
	}
	olDateArray.DeleteAll();
	dc.SelectObject(polOldPen);
	CCS_CATCH_ALL
}

void MasterShiftDemand::OnRButtonDown(UINT nFlags, CPoint point) 
{
	CCS_TRY
	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen und/oder Standard aufrufen
	
	CDialog::OnRButtonDown(nFlags, point);
	CCS_CATCH_ALL
}

void MasterShiftDemand::OnBegi()
{
	CCS_TRY

	CSelectDateDlg olDlg(this);
	olDlg.SetDate(omBegi);
	olDlg.SetValidWeekDay(2);

	if (olDlg.DoModal() == IDOK)
	{
		omBegi = olDlg.GetDate();
		omBegi = DBStringToDateTime(omBegi.Format("%Y%m%d120000"));
		if (omBegi==TIMENULL)
		{
			MessageBox(LoadStg(IDS_STRING851), LoadStg(IDS_WARNING));
			return;

		}
		else if (omStartTime.GetDayOfWeek() != 2)
		{
			MessageBox(LoadStg(IDS_STRING852), LoadStg(IDS_WARNING));
			return;
		}
		
		m_Begi.SetWindowText(omBegi.Format("%d.%m.%Y"));
		omStartTime = omBegi;

		CRect olGraphRect;
		CRect olWndRect;
		GetClientRect(&olWndRect);
		m_Helper.GetWindowRect(&olGraphRect);
		ScreenToClient(&olGraphRect);
		olGraphRect.left = olWndRect.left + 10;
		olGraphRect.right = olWndRect.right - 10;
		olGraphRect.InflateRect(10, 10);

		InvalidateRect(&olGraphRect);
	}
	CCS_CATCH_ALL
}

void MasterShiftDemand::OnKillfocusBegi() 
{
}

void MasterShiftDemand::OnDispBegi()
{
	CCS_TRY

	CSelectDateDlg olDlg(this);
	olDlg.SetDate(omDispBegi);
	olDlg.SetValidWeekDay(2);

	if (olDlg.DoModal() == IDOK)
	{
		omDispBegi = olDlg.GetDate();
		omDispBegi = DBStringToDateTime(omDispBegi.Format("%Y%m%d120000"));
		if (omDispBegi==TIMENULL)
		{
			MessageBox(LoadStg(IDS_STRING851), LoadStg(IDS_WARNING));
			return;

		}

		m_DispBegi.SetWindowText(omDispBegi.Format("%d.%m.%Y"));

		CRect olGraphRect;
		CRect olWndRect;
		GetClientRect(&olWndRect);
		m_Helper.GetWindowRect(&olGraphRect);
		ScreenToClient(&olGraphRect);
		olGraphRect.left = olWndRect.left + 10;
		olGraphRect.right = olWndRect.right - 10;
		olGraphRect.InflateRect(10, 10);

		InvalidateRect(&olGraphRect);
		CCS_CATCH_ALL
	}
}

void MasterShiftDemand::OnKillfocusDispBegi() 
{
}


void MasterShiftDemand::OnEnde()
{
	CCS_TRY

	CSelectDateDlg olDlg(this);
	olDlg.SetDate(omEnde);
	olDlg.SetValidWeekDay(1);

	if (olDlg.DoModal() == IDOK)
	{
		omEnde = olDlg.GetDate();
		omEndeTime = DBStringToDateTime(omEndeTime.Format("%Y%m%d120000"));

		m_Ende.SetWindowText(omEnde.Format("%d.%m.%Y"));

		if (omEndeTime == TIMENULL || omEndeTime.GetDayOfWeek() != 1)
		{
			CalcEndTime();
		}
		else
		{
			CalcRepeatings();
		}
	}
	CCS_CATCH_ALL
}

void MasterShiftDemand::OnKillfocusEnde() 
{
}

void MasterShiftDemand::OnKillfocusDura() 
{
	CCS_TRY
	CString olT;
	m_Dura.GetWindowText(olT);
	imDura = atoi(olT.GetBuffer(0));
	CalcEndTime();
	CRect olGraphRect;
	CRect olWndRect;
	GetClientRect(&olWndRect);
	m_Helper.GetWindowRect(&olGraphRect);
	ScreenToClient(&olGraphRect);
	olGraphRect.left = olWndRect.left + 10;
	olGraphRect.right = olWndRect.right - 10;
	olGraphRect.InflateRect(10,10);
	//ReCheckList();

	InvalidateRect(&olGraphRect);
	CCS_CATCH_ALL
}

void MasterShiftDemand::OnKillfocusPeri() 
{
	CCS_TRY
	CString olT;
	m_Peri.GetWindowText(olT);
	imPeri = atoi(olT.GetBuffer(0));
	CalcEndTime();
	CRect olGraphRect;
	CRect olWndRect;
	GetClientRect(&olWndRect);
	m_Helper.GetWindowRect(&olGraphRect);
	ScreenToClient(&olGraphRect);
	olGraphRect.left = olWndRect.left + 10;
	olGraphRect.right = olWndRect.right - 10;
	olGraphRect.InflateRect(10,10);
	//ReCheckList();

	InvalidateRect(&olGraphRect);
	CCS_CATCH_ALL
}

/*void MasterShiftDemand::OnChangeDays() 
{
	CCS_TRY
	CString olT;
	m_Days.GetWindowText(olT);
	imDays = atoi(olT.GetBuffer(0));
	CRect olGraphRect;
	CRect olWndRect;
	GetClientRect(&olWndRect);
	m_Helper.GetWindowRect(&olGraphRect);
	ScreenToClient(&olGraphRect);
	olGraphRect.left = olWndRect.left + 10;
	olGraphRect.right = olWndRect.right - 10;
	olGraphRect.InflateRect(10,10);
	ReCheckList();

	InvalidateRect(&olGraphRect);
	CCS_CATCH_ALL
}*/

/*void MasterShiftDemand::OnChangeEnde() 
{
	CCS_TRY

	CCS_CATCH_ALL
}*/

void MasterShiftDemand::OnChangeDura() 
{
	CCS_TRY
	CString olT;

	m_Dura.GetWindowText(olT);
	imDura = atoi(olT.GetBuffer(0));
	CRect olGraphRect;
	CRect olWndRect;
	GetClientRect(&olWndRect);
	m_Helper.GetWindowRect(&olGraphRect);
	ScreenToClient(&olGraphRect);
	olGraphRect.left = olWndRect.left + 10;
	olGraphRect.right = olWndRect.right - 10;
	olGraphRect.InflateRect(10,10);
	//ReCheckList();

	InvalidateRect(&olGraphRect);
	CCS_CATCH_ALL
}

void MasterShiftDemand::OnChangePeri() 
{
	CCS_TRY
	CString olT;
	m_Peri.GetWindowText(olT);
	imPeri = atoi(olT.GetBuffer(0));
	CRect olGraphRect;
	CRect olWndRect;
	GetClientRect(&olWndRect);
	m_Helper.GetWindowRect(&olGraphRect);
	ScreenToClient(&olGraphRect);
	olGraphRect.left = olWndRect.left + 10;
	olGraphRect.right = olWndRect.right - 10;
	olGraphRect.InflateRect(10,10);
	//ReCheckList();

	InvalidateRect(&olGraphRect);
	CCS_CATCH_ALL
}

LONG MasterShiftDemand::OnTableIPEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCS_TRY
	CCSIPEDITNOTIFY *prlNotify = (CCSIPEDITNOTIFY*)lParam;

	if(!prlNotify->Text.IsEmpty())
	{
		CTime olDate;
		olDate = DateStringToDate(prlNotify->Text);
		if(olDate != TIMENULL)
		{
			if(CheckTime(olDate) == true)
			{
				MSDTIME rlMsdTime;
				rlMsdTime.Time = olDate;
				omDateArray[prlNotify->Line] = rlMsdTime;
			}
			else
			{
				prlNotify->UserStatus = true;
				prlNotify->Status = true;
				((CCSTable*)prlNotify->SourceTable)->SetTextColumnColor(prlNotify->Line, 0, COLORREF(RED), COLORREF(WHITE));
				prlNotify->Text = prlNotify->Text;

				MSDTIME rlMsdTime;
				rlMsdTime.Time = olDate;
				rlMsdTime.valid = false;
				omDateArray[prlNotify->Line] = rlMsdTime;
			
			}
		}
	}
	//ReCheckList();
	CRect olGraphRect;
	CRect olWndRect;
	GetClientRect(&olWndRect);
	m_Helper.GetWindowRect(&olGraphRect);
	ScreenToClient(&olGraphRect);
	olGraphRect.left = olWndRect.left + 10;
	olGraphRect.right = olWndRect.right - 10;
	olGraphRect.InflateRect(10,10);

	InvalidateRect(&olGraphRect);
	return 0L;
	CCS_CATCH_ALL
	return 0L;
}

/*void MasterShiftDemand::ReCheckList()
{
	CCS_TRY
	CUIntArray olIdxes;
	if(ListHasOverlapping(olIdxes) == true)
	{
		for(int i = 0; i < 50; i++)
		{
			bool blFound = false;
			for(int j = 0; j < olIdxes.GetSize(); j++)
			{
				if(i == (int)olIdxes[j])
				{
					blFound = true;
					j = olIdxes.GetSize();
				}
			}
			if(blFound == true)
			{
				CString olText;
				if(pomTable->GetTextFieldValue(i, 0, olText) == true)
				{
					CTime olDate;
					olDate = DateStringToDate(olText);
					if(olDate != TIMENULL)
					{
						MSDTIME rlMsdTime;
						rlMsdTime.Time = olDate;
						rlMsdTime.valid = false;
						omDateArray[i] = rlMsdTime;
						pomTable->SetTextColumnColor(i, 0, COLORREF(RED), COLORREF(WHITE));
					}
					else
					{
						pomTable->SetTextColumnColor(i, 0, COLORREF(RED), COLORREF(WHITE));
					}
				}
			}
			else
			{
				CString olText;
				if(pomTable->GetTextFieldValue(i, 0, olText) == true)
				{
					CTime olDate;
					olDate = DateStringToDate(olText);
					if(olDate != TIMENULL)
					{
						MSDTIME rlMsdTime;
						rlMsdTime.Time = olDate;
						omDateArray[i] = rlMsdTime;
						pomTable->SetTextColumnColor(i, 0, COLORREF(GREEN), COLORREF(WHITE));
					}
				}
			}
		}
	}//end if
	else
	{
		for(int i = 0; i < 50; i++)
		{
			CString olText;
			if(pomTable->GetTextFieldValue(i, 0, olText) == true)
			{
				CTime olDate;
				olDate = DateStringToDate(olText);
				if(olDate != TIMENULL)
				{
					pomTable->SetTextColumnColor(i, 0, COLORREF(GREEN), COLORREF(WHITE));
					omDateArray[i].valid = true;
				}
				else
				{
					pomTable->SetTextColumnColor(i, 0, COLORREF(RED), COLORREF(WHITE));
				}
			}
		}
	}
	CCS_CATCH_ALL
}*/

bool MasterShiftDemand::CheckTime(CTime opTime, int *ipIdx)
{
	CCS_TRY
	bool blRet = true;
	bool blIdxSet = false;
	int ilIdx = 0;
	CTime olEndTime = omStartTime + CTimeSpan(imDays*imPeri*imDura, 0, 0, 0); 
	CCSPtrArray<MSDTIME> olDateArray;
	for(int i = 0; i < 50; i++)
	{
		if(omDateArray[i].Time != TIMENULL)
		{
			olDateArray.NewAt(olDateArray.GetSize(), omDateArray[i]);
			if(ipIdx != NULL && blIdxSet == false)
			{
				ilIdx = olDateArray.GetSize() - 1;
				blIdxSet = true;
			}
		}
	}
	olDateArray.Sort(CompareMsdTimes);

	if(opTime < olEndTime)
	{
		blRet = false;
	}
	if(ipIdx == NULL)
	{
		for(i = 0; ((i < olDateArray.GetSize()) && (blRet == true)); i++)
		{
			CTime olStart1, olEnd1, olStart2, olEnd2;
			olStart1 = opTime;
			olEnd1 = opTime + CTimeSpan(imDays*imPeri*imDura, 0, 0, 0); 
			olStart2 = olDateArray[i].Time; 
			olEnd2 = olDateArray[i].Time + CTimeSpan(imDays*imPeri*imDura, 0, 0, 0); 

			olEndTime = olDateArray[i].Time + CTimeSpan(imDays*imPeri*imDura, 0, 0, 0); 
			CTime olNewEndTime = opTime + CTimeSpan(imDays*imPeri*imDura, 0, 0, 0); 
			CString olStr1 = olNewEndTime.Format("%d.%m.%Y");
			CString olStr2 = olEndTime.Format("%d.%m.%Y");
			if(IsReallyOverlapped(olStart1, olEnd1, olStart2, olEnd2))
			//if(olNewEndTime < olEndTime || opTime < olEndTime)
			{
				blRet = false;
			}
		}//end for
	}//end if
	else
	{
		for(i = olDateArray.GetSize()-1; ((i >= 0) && (blRet == true)); i--)
		{
			if(ilIdx != i)
			{
				CTime olStart1, olEnd1, olStart2, olEnd2;
				olStart1 = opTime;
				olEnd1 = opTime + CTimeSpan(imDays*imPeri*imDura, 0, 0, 0); 
				olStart2 = olDateArray[i].Time; 
				olEnd2 = olDateArray[i].Time + CTimeSpan(imDays*imPeri*imDura, 0, 0, 0); 
				olEndTime = olDateArray[i].Time + CTimeSpan(imDays*imPeri*imDura, 0, 0, 0); 
				CTime olNewEndTime = opTime + CTimeSpan(imDays*imPeri*imDura, 0, 0, 0); 
				CString olStr1 = olNewEndTime.Format("%d.%m.%Y");
				CString olStr2 = olEndTime.Format("%d.%m.%Y");
				CString olStr3 = opTime.Format("%d.%m.%Y");
				if(IsReallyOverlapped(olStart1, olEnd1, olStart2, olEnd2))
				//if(olNewEndTime < olEndTime || opTime < olEndTime)
				{
					blRet = false;
				}//end if
			}//end if
		}//end else
	}


	olDateArray.DeleteAll();
	return blRet;
	CCS_CATCH_ALL
	return false;
}

bool MasterShiftDemand::ListHasOverlapping(CUIntArray &ropIdxes)
{
	CCS_TRY
	bool blRet = false;
	CCSPtrArray<MSDTIME> olDateArray;
	for(int i = 0; i < 50; i++)
	{
		if(omDateArray[i].Time != TIMENULL)
		{
			olDateArray.NewAt(olDateArray.GetSize(), omDateArray[i]);
		}
	}
	MSDTIME rlBaseTime;
	olDateArray.Sort(CompareMsdTimes);
	rlBaseTime.Time = omStartTime; 
	olDateArray.NewAt(0,rlBaseTime);
	for(i = 1; i < olDateArray.GetSize(); i++)
	{
		CTime olStart1, olEnd1, olStart2, olEnd2;
		olStart1 = olDateArray[i-1].Time;
		olEnd1   = olDateArray[i-1].Time + CTimeSpan(imDays*imPeri*imDura, 0, 0, 0);
		olStart2 = olDateArray[i].Time;
		olEnd2   = olDateArray[i].Time + CTimeSpan(imDays*imPeri*imDura, 0, 0, 0);
		if(IsReallyOverlapped(olStart1, olEnd1, olStart2, olEnd2))
		{
			for(int j = 0; j < 50; j++)
			{
				if(olDateArray[i].Time == omDateArray[j].Time)
				{
					ropIdxes.Add(j);
					blRet = true;
				}
			}
		}
	}
	olDateArray.DeleteAll();
	return blRet;
	CCS_CATCH_ALL
return false;
}

void MasterShiftDemand::OnSize(UINT nType, int cx, int cy) 
{
	CCS_TRY
	CDialog::OnSize(nType, cx, cy);
	if(bmIsInit == true)	
	{
		CRect olGraphRect;
		CRect olWndRect;
		GetClientRect(&olWndRect);
		m_Helper.GetWindowRect(&olGraphRect);
		ScreenToClient(&olGraphRect);
		olGraphRect.left = olWndRect.left + 10;
		olGraphRect.right = olWndRect.right - 10;
		olGraphRect.InflateRect(10,10);

		InvalidateRect(&olGraphRect);
	}
	CCS_CATCH_ALL
}


void MasterShiftDemand::OnCopy() 
{
CCS_TRY
	//uhi 29.8.00
	if(!bmCopy)
		return;

	CopyShiftRosterRequirementDlg olDlg(this);
	if (olDlg.DoModal() == IDOK)
	{
		rmSdg.Urno = 0;

		m_BtnBegi.EnableWindow(TRUE);
		m_Dnam.SetReadOnly(FALSE);
		m_Oper.EnableWindow(TRUE);

		omDnam = olDlg.NewName();
		m_Dnam.SetWindowText(omDnam);
		
		omReplaceFunction = olDlg.ReplaceFunction();
		omWithFunction	  = olDlg.WithFunction();
		imCountOfCopies	  = olDlg.CountOfCopies();

		OnPaint();
	}

CCS_CATCH_ALL
}

void MasterShiftDemand::OnNew() 
{
CCS_TRY
	bmCopy = false;
	m_Copy.EnableWindow(false);

	rmSdg.Urno = 0;
	CTimeSpan omOneDay(1, 0, 0, 0);
	CTimeSpan omSixDays(6, 0, 0, 0);

	//uhi 11.9.00 Auf Stichtag folgenden Montag anzeigen
	omBegi = omValidInput;
	while (omBegi.GetDayOfWeek() != 2)
		omBegi = omBegi + omOneDay;
		
	omDispBegi = omBegi;
	//Sonntag darauffolgende Woche anzeigen
	omEnde = omBegi + omSixDays;

	imWeeks = 10;
	imDays = 7;
	imDura = 1;
	imPeri = 1;

	m_BtnBegi.EnableWindow(TRUE);
	m_Dnam.SetReadOnly(FALSE);
	m_Oper.EnableWindow(TRUE);

	CString olDays, olDura, olPeri;
	olDays.Format("%d", imDays);
	olDura.Format("%d", imDura);
	olPeri.Format("%d", imPeri);
	//m_Days.SetWindowText(olDays);
	m_Dura.SetWindowText(olDura);
	m_Peri.SetWindowText(olPeri);
	omDnam = "";
	m_Dnam.SetWindowText(omDnam);
	m_Begi.SetWindowText(omBegi.Format("%d.%m.%Y"));
	m_Ende.SetWindowText(omEnde.Format("%d.%m.%Y"));
	m_DispBegi.SetWindowText(omDispBegi.Format("%d.%m.%Y"));
	m_Vorg.SetCurSel(0);
	OnPaint();

CCS_CATCH_ALL
}

void MasterShiftDemand::CalcEndTime()
{
CCS_TRY
	CString olStartStr, olPeriStr, olDuraStr;
	int iWeeks;
	CTimeSpan omOneDay(1, 0, 0, 0);
	m_Dura.GetWindowText(olDuraStr);
	m_Peri.GetWindowText(olPeriStr);
	m_Begi.GetWindowText(olStartStr);
	omStartTime = DateStringToDate(olStartStr);
	omStartTime = DBStringToDateTime(omStartTime.Format("%Y%m%d120000"));	
	iWeeks = (atoi(olDuraStr) - 1) * atoi(olPeriStr) + 1;
	CTimeSpan omWeeks((iWeeks * 7), 0, 0, 0);
	omEndeTime = omStartTime +  omWeeks - omOneDay;
	m_Ende.SetWindowText(omEndeTime.Format("%d.%m.%Y"));
CCS_CATCH_ALL
}

void MasterShiftDemand::CalcRepeatings()
{
CCS_TRY
	CString olStartStr, olPeriStr, olEndStr, olDuraStr;
	CTimeSpan omDays;
	int iRepeat, iWeeks, iPeri;
	
	//uhi 11.9.00
	m_Peri.GetWindowText(olPeriStr);
	if (olPeriStr.IsEmpty())
		return;
	iPeri = atoi(olPeriStr);
	if (iPeri < 1)
		return;

	m_Begi.GetWindowText(olStartStr);
	omStartTime = DateStringToDate(olStartStr);
	omStartTime = DBStringToDateTime(omStartTime.Format("%Y%m%d120000"));
	m_Ende.GetWindowText(olEndStr);
	omEndeTime = DateStringToDate(olEndStr);
	omEndeTime = DBStringToDateTime(omEndeTime.Format("%Y%m%d120000"));
	omDays = omEndeTime - omStartTime;
	iWeeks = (omDays.GetDays() + 1)/7;
	iRepeat = ((iWeeks - 1)/iPeri) + 1;
	olDuraStr.Format("%i", iRepeat);
	m_Dura.SetWindowText(olDuraStr);
CCS_CATCH_ALL
}

CString	MasterShiftDemand::ReplaceFunctionWith(const CString& ropFctc)
{
	if (this->omReplaceFunction == LoadStg(IDS_STRING1901))	// --ALL--
	{
		if (this->omWithFunction == LoadStg(IDS_STRING1902)) // --NO REPLACE--
		{
			return ropFctc;
		}
		else
		{
			return this->omWithFunction;
		}
	}
	else if (this->omReplaceFunction == LoadStg(IDS_STRING1902))	// --NO REPLACE--
	{
		return ropFctc;
	}
	else if (this->omReplaceFunction == ropFctc)
	{
		if (this->omWithFunction == LoadStg(IDS_STRING1902)) // --NO REPLACE--
		{
			return ropFctc;
		}
		else
		{
			return this->omWithFunction;
		}
	}
	else
	{
		return ropFctc;
	}
}


BOOL MasterShiftDemand::OnTtnNeedText(UINT id,NMHDR * pTTTStruct,LRESULT * pResult)
{
    TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pTTTStruct;
    UINT nID = pTTTStruct->idFrom;
    if (pTTT->uFlags & TTF_IDISHWND)
    {
        // idFrom is actually the HWND of the tool
		CString olText;
        nID = ::GetDlgCtrlID((HWND)nID);
		switch(nID)
		{
		case IDC_DNAM:
			olText = LoadStg(IDS_STRING1904);
		break;
		case IDC_DISPBEGI:
			olText = LoadStg(IDS_STRING1903);
		break;
		}

		if (olText.GetLength() > 0)
		{
	        strncpy(pTTT->szText,olText,80);
			pTTT->szText[79] = '\0';
			return(TRUE);
		}
    }

    return(FALSE);
}

BOOL MasterShiftDemand::IsOperative()
{
	if (this->bmOperEnabled)
	{
		int ind = m_Oper.GetCurSel();
		if (ind == CB_ERR)
			return FALSE;
		else
			return m_Oper.GetItemData(ind) == 'O';
	}
	else
	{
		return TRUE;
	}
}

void MasterShiftDemand::CalcBeginTime()
{
	CTime olTimeNow = CTime::GetCurrentTime();

	int ilYear, ilMonth;
	ilYear = olTimeNow.GetYear();

	if (this->IsOperative() && this->imLastInput >= 0)
	{
		this->bmCheckValidity = true;
		if (olTimeNow.GetDay() <= imLastInput)
		{
			ilMonth = olTimeNow.GetMonth() + imNextMonth;
		}
		else if (olTimeNow.GetDay() > imLastInput)
		{
			ilMonth = olTimeNow.GetMonth() + imNextMonth + 1;	
		}

		if (ilMonth > 12)
		{
			ilMonth = ilMonth - 12;
			ilYear = ilYear + 1;
		}
	}
	else
	{
		this->bmCheckValidity = false;
		ilMonth = olTimeNow.GetMonth();
	}

	omValidInput = CTime(ilYear, ilMonth, 1, 0, 0, 0);


	CTimeSpan omOneDay(1, 0, 0, 0);
	CTimeSpan omSixDays(6, 0, 0, 0);
		
	//	Auf Stichtag folgenden Montag anzeigen
	omBegi = omValidInput;
	while (omBegi.GetDayOfWeek() != 2)
		omBegi = omBegi + omOneDay;
		
	omDispBegi = omBegi; 
	//	Den Sonntag der darauffolgende Woche anzeigen
	omEnde = omBegi + omSixDays;
}

void MasterShiftDemand::OnSelchangeOper() 
{
	// TODO: Add your control notification handler code here
	if (rmSdg.Urno == 0)
	{
		CalcBeginTime();

		m_Begi.SetWindowText(omBegi.Format("%d.%m.%Y"));
		m_Ende.SetWindowText(omEnde.Format("%d.%m.%Y"));
		m_DispBegi.SetWindowText(omDispBegi.Format("%d.%m.%Y"));

		CalcEndTime();

		CRect olGraphRect;
		CRect olWndRect;
		GetClientRect(&olWndRect);
		m_Helper.GetWindowRect(&olGraphRect);
		ScreenToClient(&olGraphRect);
		olGraphRect.left = olWndRect.left + 10;
		olGraphRect.right = olWndRect.right - 10;
		olGraphRect.InflateRect(10, 10);

		InvalidateRect(&olGraphRect);
	}
}
