// ACProgressBar.cpp : implementation file
//
//

#include <stdafx.h>
#include <coverage.h>
#include <ACProgressBar.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CACProgressBar dialog

int imCurrPos;
int imLoop;
CACProgressBar::CACProgressBar(CWnd* pParent /*=NULL*/, int ipSize /*100*/)
	: CDialog(CACProgressBar::IDD, pParent)
{
	//{{AFX_DATA_INIT(CACProgressBar)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	Create(IDD,pParent);

//	m_ProgressCtrl.SetRange(0,ipSize);
	bmStop = false;
	imLoop = 0;
	imTotalLoops = 0;
}


void CACProgressBar::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CACProgressBar)
	DDX_Control(pDX, IDC_PROGRESS1, m_ProgressCtrl);
	DDX_Control(pDX, IDC_LOOPNUMBER, m_LoopNumber);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CACProgressBar, CDialog)
	//{{AFX_MSG_MAP(CACProgressBar)
	ON_BN_CLICKED(IDC_STOP, OnStop)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CACProgressBar message handlers

void CACProgressBar::OnStop() 
{
	bmStop = true;
}

void CACProgressBar::SetProgress(int ipLoopNumber,int ipProgress)
{

	CString olLPNr = "";
	olLPNr.Format("%d",imTotalLoops);
	if(imCurrPos <= 100)
	{
		m_ProgressCtrl.StepIt();
	}
	else 
	{
		m_ProgressCtrl.SetPos(0);
		imCurrPos=0;
		imLoop++;
	}
	imCurrPos++;
	m_LoopNumber.SetWindowText(olLPNr);
	m_LoopNumber.Invalidate();
	CheckContinue(); 
}

BOOL CACProgressBar::DestroyWindow() 
{
	BOOL blRc = CDialog::DestroyWindow();

	return blRc;
}

bool CACProgressBar::CheckContinue() 
{
	MSG olMsg;
	UINT ilMin = 0;
	UINT ilMax = 0;
	while(::PeekMessage(&olMsg,this->m_hWnd,ilMin,ilMax,PM_NOREMOVE))
	{
		::GetMessage(&olMsg,this->m_hWnd,ilMin,ilMax);
		::TranslateMessage(&olMsg);
		::DispatchMessage(&olMsg);
	}
	return !bmStop;
}


BOOL CACProgressBar::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	imCurrPos = 0;
	ShowWindow(SW_SHOWNORMAL);	

	m_ProgressCtrl.SetStep(1);
	m_ProgressCtrl.SetRange(0, 100);
	m_LoopNumber.SetWindowText("1");
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
