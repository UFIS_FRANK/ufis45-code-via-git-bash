// stviewer.cpp : implementation file
//
// Modification History:
// 19-Sep-96	Damkerng	Change many places since our database is changed.
//							In this module, the old implementation will use the field
//							PRID to keep text for a special job and an illness job.
//							Now, these has been moved to the field TEXT.
//							Check these places out by searching for "Id 19-Sep-96"

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSPtrArray.h>
#include <CViewer.h>
#include <gantt.h>
#include <CCSBar.h>
#include <ccsddx.h>
#include <CoverageDiaViewer.h>
#include <CovDiagram.h>
#include <CedaBsdData.h>
#include <CedaPfcData.h>
#include <DataSet.h>
#include <CedaBasicData.h>
#include <Basicdata.h>
#include <ACProgressBar.h>
#include <CedaDrrData.h>

//_ftime include files
#include <sys/timeb.h>
#include <time.h>

// std template include files
#include <algorithm>
#include <stdio.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

// Local function prototype
static void CoverageDiagramCf(void *popInstance, int ipDDXType,
	void *vpDataPointer, CString &ropInstanceName);

static int CompareSdtByTimes(const SDTDATA **e1, const SDTDATA **e2);
static int Find(char *pcpString,char *pcpSubString);
//static int GetCompressedSdtCount(SDTDATA *prpSdt);
/////////////////////////////////////////////////////////////////////////////
// CoverageDiagramViewer
//
char pcmBarText[4096]="";


CoverageDiagramViewer::CoverageDiagramViewer()
{
	CCS_TRY
	bmMaleAllesNeu_lokalgesetzt = true;
	bmMaleAllesNeu_globalgesetzt = true;
	pomAttachWnd = NULL;
	pomCoverageWnd = NULL;
	omBkBrush.CreateSolidBrush(WHITE/*RGB(222,222,222)NAVY*/);
	omBreakBrush.CreateSolidBrush(RGB(222,222,222));//Light Gray
	//omGeometryFontIndex = GANTT_S_FONT;
	omGeometryTimeSpan = CTimeSpan(0, 10, 0, 0);
	bmGeometryTimeLines = true;
	bmGeometryMinimize = false;
	bmMaxView = false;
	bmAdditiv = true;
	bmIgnoreBreak = true;
	bmShiftRoster = false;
	bmShowActualDsr = false;
	bmRosChanged =false;
	bmIsRealRoster = true;
	omGeometryFontIndex	= 8;
	omStartShowTime = CTime::GetCurrentTime();
	omMarkTimeStart = TIMENULL;
	omMarkTimeEnd   = TIMENULL;
	omStartTime = TIMENULL;
	omEndTime   = TIMENULL;
	bmSimulate	= false;
	imSimType	= SIM_ABSOLUTE;
	imSimValue	= 0;
	imSimOffset	= 0;
	imSimVariant= 0;
	imSimIterations=3;
	imSimPeakHeight=10;
	imSimPeakWidth =20; 
	imSimWidth	= 20;
	imSimHeight	= 0;
	imSimHeightType	= SIM_PERCENT;
	imSimDisplay = 0;
	lmInternalUrno = 0;
		
	bmAutoCovIsActive = false;
	bmEvulationFilterChanged = true;
	bmNoDemUpdates = false;

	bmIsBorderFix = false;
	imMaxVal = 0;
	imMinVal = 0;
	imBorderVal = 10;
	omSdtUrnoMap.RemoveAll();
	omDemUrnoMap.RemoveAll();
	omAssignDemUrnoMap.RemoveAll();
	omFilterFlightUrnoMap.RemoveAll();
	omRealDemUrnoMap.RemoveAll();

	bmActuellState = false;
	lmSdgu = 0;

	bmSdgChanged = false;
	bmCCI = true;

	omViewerFilterEquipmentType = LoadStg(IDS_STRING61216);

	PrepareTSRData();

	ogDdx.Register(this, DRR_CHANGE,
		CString("STVIEWER"), CString("DRR Changed"), CoverageDiagramCf);

	ogDdx.Register(this, DRR_NEW,
		CString("STVIEWER"), CString("DRR New"), CoverageDiagramCf);

	ogDdx.Register(this, DRR_DELETE,
		CString("STVIEWER"), CString("DRR Delete"), CoverageDiagramCf);


	ogDdx.Register(this, ENDDRR_RELEASE,
		CString("STVIEWER"), CString("DRR Release End"), CoverageDiagramCf);	// Release of shift plan

	ogDdx.Register(this, BC_RELDRR,
		CString("STVIEWER"), CString("DRR Release"), CoverageDiagramCf);		// Release of duty roster


	ogDdx.Register(this, GPL_CHANGE,
		CString("STVIEWER"), CString("Gpl Changed"), CoverageDiagramCf);

	ogDdx.Register(this, GPL_NEW,
		CString("STVIEWER"), CString("Gpl New"), CoverageDiagramCf);

	ogDdx.Register(this, GPL_DELETE,
		CString("STVIEWER"), CString("Gpl Delete"), CoverageDiagramCf);


	ogDdx.Register(this, GSP_CHANGE,
		CString("STVIEWER"), CString("GSP Changed"), CoverageDiagramCf);

	ogDdx.Register(this, GSP_NEW,
		CString("STVIEWER"), CString("GSP New"), CoverageDiagramCf);

	ogDdx.Register(this, GSP_DELETE,
		CString("STVIEWER"), CString("GSP Delete"), CoverageDiagramCf);


	ogDdx.Register(this, SPL_CHANGE,
		CString("STVIEWER"), CString("Spl Changed"), CoverageDiagramCf);

	ogDdx.Register(this, SPL_NEW,
		CString("STVIEWER"), CString("Spl New"), CoverageDiagramCf);

	ogDdx.Register(this, SPL_DELETE,
		CString("STVIEWER"), CString("Spl Delete"), CoverageDiagramCf);



	ogDdx.Register(this, MSD_NEW,
		CString("STVIEWER"), CString("MSD New"), CoverageDiagramCf);
	ogDdx.Register(this, MSD_CHANGE,
		CString("STVIEWER"), CString("MSD Change"), CoverageDiagramCf);
	ogDdx.Register(this, MSD_DELETE,
		CString("STVIEWER"), CString("MSD Delete"), CoverageDiagramCf);

	ogDdx.Register(this, MSD_NEW_SELF,
		CString("STVIEWER"), CString("MSD New from self"), CoverageDiagramCf);
	ogDdx.Register(this, MSD_CHANGE_SELF,
		CString("STVIEWER"), CString("MSD Change from self"), CoverageDiagramCf);
	ogDdx.Register(this, MSD_DELETE_SELF,
		CString("STVIEWER"), CString("MSD Delete from self"), CoverageDiagramCf);

	ogDdx.Register(this, BSD_NEW,
		CString("STVIEWER"), CString("BSD New"), CoverageDiagramCf);
	//
	ogDdx.Register(this, BC_BSD_NEW,
		CString("STVIEWER"), CString("BSD New"), CoverageDiagramCf);
	ogDdx.Register(this, BSD_CHANGE,
		CString("STVIEWER"), CString("BSD Changed"), CoverageDiagramCf);
	ogDdx.Register(this, BSD_DELETE,
		CString("STVIEWER"), CString("BSD Deleted"), CoverageDiagramCf);
	ogDdx.Register(this, SDT_NEW,
		CString("STVIEWER"), CString("SDT New"), CoverageDiagramCf);
	ogDdx.Register(this, SDT_CHANGE,
		CString("STVIEWER"), CString("SDT Changed"), CoverageDiagramCf);
	ogDdx.Register(this, SDT_SELCHANGE,
		CString("STVIEWER"), CString("SDT Changed"), CoverageDiagramCf);
	ogDdx.Register(this, SDT_DELETE,
		CString("STVIEWER"), CString("SDT Deleted"), CoverageDiagramCf);

	ogDdx.Register(this, RELSDG,
		CString("STVIEWER"), CString("RELSDG"), CoverageDiagramCf);

	ogDdx.Register(this, SDG_NEW,
		CString("STVIEWER"), CString("SDG New"),	 CoverageDiagramCf);
	ogDdx.Register(this, SDG_CHANGE,
		CString("STVIEWER"), CString("SDG Changed"), CoverageDiagramCf);
	ogDdx.Register(this, SDG_DELETE,
		CString("STVIEWER"), CString("SDG Deleted"), CoverageDiagramCf);

	ogDdx.Register(this, DEM_CFL_READY,
		CString("STVIEWER"), CString("Dem Cfl Ready"), CoverageDiagramCf);

	ogDdx.Register(this, RELDEM,
		CString("STVIEWER"), CString("DEM Release"), CoverageDiagramCf);		// Release of demand

	ogDdx.Register(this, DEM_CHANGE,
		CString("STVIEWER"), CString("Dem Changed"), CoverageDiagramCf);

	/*ogDdx.Register(this, DEM_NEW,
		CString("STVIEWER"), CString("Dem Changed"), CoverageDiagramCf);*/

	ogDdx.Register(this, DEM_DELETE,
		CString("STVIEWER"), CString("Dem Changed"), CoverageDiagramCf);

	ogDdx.Register(this, RELMSD,
		CString("STVIEWER"), CString("RELMSD"), CoverageDiagramCf);
	
	ogDdx.Register(this, BC_RELPBO,
		CString("STVIEWER"), CString("RELPBO"), CoverageDiagramCf);
	
	ogDdx.Register(this, UPDDEM,
		CString("STVIEWER"), CString("UPDDEM"), CoverageDiagramCf);
	
	CCS_CATCH_ALL	
}

CoverageDiagramViewer::~CoverageDiagramViewer()
{
	CCS_TRY
    ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
    omBasicShiftUrnoMap.RemoveAll();
	POSITION rlPos;

	CMapPtrToPtr *polBsdUrnoMap;
	for ( rlPos =  omBasicShiftKeyMap.GetStartPosition(); rlPos != NULL; )
	{
		CString olKey;
		omBasicShiftKeyMap.GetNextAssoc(rlPos, olKey,(void *& ) polBsdUrnoMap);
		polBsdUrnoMap->RemoveAll();
		delete polBsdUrnoMap;
	}

/************/
	CMapPtrToPtr *polShiftDemMap;
	for ( rlPos =  omShiftsDemandMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omShiftsDemandMap.GetNextAssoc(rlPos, (void *&)llUrno,(void *& ) polShiftDemMap);
		polShiftDemMap->RemoveAll();
		delete polShiftDemMap;
	}
	
	omShiftsDemandMap.RemoveAll();
	/*******/
	omShiftNameMap.RemoveAll();
	omShiftNameUrnoArray.DeleteAll();
	omShiftNameArray.RemoveAll();

	omBasicShiftKeyMap.RemoveAll();
	omBasicShiftData.DeleteAll();
	
	omSdtUrnoMap.RemoveAll();
	omDemUrnoMap.RemoveAll();
	omAssignDemUrnoMap.RemoveAll();
	omFilterFlightUrnoMap.RemoveAll();
	omRealDemUrnoMap.RemoveAll();

	omTplTsrMap.RemoveAll();
	omTplTsrArray.DeleteAll();
	omTotalTsrArray.RemoveAll();
	omFilterTsrArray.RemoveAll();

	CMapPtrToPtr *polDemMap;
	for ( rlPos =  omFlightDemMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omFlightDemMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& ) polDemMap);
		polDemMap->RemoveAll();
		delete polDemMap;
	}
	
	omFlightDemMap.RemoveAll();

	omAutoCovShifts.DeleteAll();

	omAutoCovArray.DeleteAll();
	CCS_CATCH_ALL	
}

void CoverageDiagramViewer::Attach(CWnd *popAttachWnd)
{
	CCS_TRY
    pomAttachWnd = popAttachWnd;
	CCS_CATCH_ALL	
}

/////////////////////////////////////////////////////////////////////////////
// CoverageDiagramViewer -- Filtering, Sorting, and Grouping
//
// Requirements:
// The StaffDiagram is the most complex diagram for implement the filtering, sorting and
// grouping. This diagram could be grouped by Pool, grouped by Team, and grouped by ShiftCode.
// However, no matter what the groupping is, each GanttLine in this diagram will represent
// a duty job (job type JOBPOOL). Sorting may be a combination of ShiftBegin, ShiftEnd,
// ShiftCode, Team, Name and Rank. Filtering may be a combination of Pool, Team, ShiftCode,
// and Rank.
//
// Methods for change the view:
// ChangeViewTo		Change view, reload everything from Ceda????Data
//
// PrepareGroupping	Prepare the enumerate group value
// PrepareFilter	Prepare CMapStringToPtr or CMapPtrToPtr for filtering
// PrepareSorter	Prepare sort criterias before sorting take place
//
// IsPassFilter		Return TRUE if the given record satisfies the filter
// CompareGroup		Return 0 or -1 or 1 as the result of comparison of two groups
// CompareLine		Return 0 or -1 or 1 as the result of comparison of two lines
//
//
// Programmer notes:
// At the program start up, we will create a default view for each dialog. This will
// overwrite the <default> view saved in the last time execution.
//
// When the viewer is constructed or everytimes the view changes, we will clear
// everything from the viewer buffer and do the insertion sorting. We will insert
// the record only if it satisfies our filter. Conceptually we will filter the record
// first, then sort in (by insertion sort), and group in.
//
// To let the filter work fast, we will implement a CMapStringToPtr for every filter
// condition. For example, let's say that we have a CStringArray for every possible
// values of a filter. We will have a CMapStringToPtr which could tell us the given
// key value is selected in the filter or not.
//
void CoverageDiagramViewer::ChangeViewTo(const char *pcpViewName,
	CTime opStartTime, CTime opEndTime)
{
	CCS_TRY
	DWORD	llNow,llLast, llStart;
	double	flDiff;

	llLast = GetTickCount();
	llStart = llLast;

	CString olOldDemandName;
	int ilOldDemandLevel = -1,ilOldShiftView = BASE_SHIFT_ALL;
  
	if(omGroups.GetSize() < 3)
	{
		DeleteAll();
		MakeGroups();
	}

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiaViewer::ChangeViewTo:DeleteAll/MakeGroups %.3lf sec\n", flDiff/1000.0 );

	SelectView(pcpViewName);        

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiaViewer::ChangeViewTo:SelectView %.3lf sec\n", flDiff/1000.0 );

	omStartTime = opStartTime;
	omEndTime = opEndTime;

	PrepareFilter();

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiaViewer::ChangeViewTo:PrepareFilter %.3lf sec\n", flDiff/1000.0 );

	FilterFlights();
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiaViewer::ChangeViewTo:FilterFlights %.3lf sec\n", flDiff/1000.0 );

	CalculateDailyBasicShifts();
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiaViewer::ChangeViewTo:CalculateDailyBasicShifts %.3lf sec\n", flDiff/1000.0 );

	LoadGhdWindow();
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiaViewer::ChangeViewTo:LoadGhdWindow %.3lf sec\n", flDiff/1000.0 );

	LoadSdtWindow();
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiaViewer::ChangeViewTo:LoadSdtWindow %.3lf sec\n", flDiff/1000.0 );

	bmLoadGhds = true;
	bmSdtChanged =  true;
	
	DeleteGroupLines();
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiaViewer::ChangeViewTo:DeleteGroupLines %.3lf sec\n", flDiff/1000.0 );

	
	MakeLines();

	
	
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiaViewer::ChangeViewTo:MakeLines %.3lf sec\n", flDiff/1000.0 );
	

	
	MakeBars();

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiaViewer::ChangeViewTo:MakeBars %.3lf sec\n", flDiff/1000.0 );

	/* check on CUT simulation active	*/
	if (this->SimulationActive() && this->SimVariant() == 1)
	{
		CStringArray olFunctions;
		this->DistinctFunctions(olFunctions);
		if (olFunctions.GetSize() > 1)
		{
			this->SimVariant(0);
			this->SimulationActive(false);
			if (this->pomAttachWnd != NULL)
				this->pomAttachWnd->SendMessage(WM_SIMULATE,0,0);
		}
	}

	CCS_CATCH_ALL	
}

void CoverageDiagramViewer::ChangeViewToForHScroll(CTime opStartTime, CTime opEndTime)
{
	CCS_TRY
	if(opStartTime != TIMENULL)
		omStartTime = opStartTime;
	if(opEndTime != TIMENULL)
		omEndTime = opEndTime;

	DeleteGroupLines();

	CalculateDailyBasicShifts();

	MakeLines();

	
	MakeBars();

	CCS_CATCH_ALL	
}

void CoverageDiagramViewer::ChangeViewToForShiftDemandUpdate()
{
	CCS_TRY

		DeleteGroupLines(SHIFT_DEMAND);

		MakeLines(SHIFT_DEMAND);

		MakeBars(SHIFT_DEMAND);

	CCS_CATCH_ALL	
}

BASICSHIFTS * CoverageDiagramViewer::GetBasicShiftByUrno(long pcpUrno)
{
	CCS_TRY
	BASICSHIFTS *prpBsh;

	if (omBasicShiftUrnoMap.Lookup((void*)pcpUrno,(void *& )prpBsh) == TRUE)
	{
		return prpBsh;
	}
	CCS_CATCH_ALL	
	return NULL;
}

void CoverageDiagramViewer::PrepareShiftNameMap()
{

	CStringArray olData;
	CStringArray olValueList;
	CCSPtrArray <RecordSet> olDelList;
	CString olValues;
	int ilPos = 0;
	int ilPos2 = 0;
	ogBCD.GetAllFields("SPL", "URNO,SNAM", ",", 0, olData);
	omShiftNameMap.RemoveAll();
	omShiftNameArray.RemoveAll();
	omShiftNameUrnoArray.DeleteAll();
	for(int i = 0; i < olData.GetSize(); i++)
	{
		olValues = olData[i];
		ExtractItemList(olValues,&olValueList,',');
		if(olValueList.GetSize() > 1)
		{
			CString olGPLValues = "";
			ogBCD.GetRecords("GPL", "SPLU", olValueList[0], &olDelList);
				
			int ilUrnoIdx = ogBCD.GetFieldIndex("GPL", "URNO");
			

			for(int ilRecords = 0; ilRecords < olDelList.GetSize(); ilRecords++)
			{
				CString olUrno = "," + olDelList[ilRecords].Values[ilUrnoIdx] ; 
				olGPLValues += olUrno;
			}
			if(!olGPLValues.IsEmpty())
			{
					olGPLValues =  olGPLValues + ",";
						
					ilPos = omShiftNameUrnoArray.GetSize();
					ilPos2 = omShiftNameArray.GetSize();
					omShiftNameUrnoArray.NewAt(ilPos,olGPLValues);
					omShiftNameArray.SetAtGrow(ilPos2,olValueList[1]);
					omShiftNameMap.SetAt(omShiftNameArray[ilPos2],&omShiftNameUrnoArray[ilPos]);
			}
			olDelList.DeleteAll();

		}
	}
}

void CoverageDiagramViewer::MakeBshFromBsdData(BASICSHIFTS *prpBsh, BSDDATA *prpBsd)
{
	CCS_TRY
	CTime olFrom = CTime(prpBsh->From.GetYear(), prpBsh->From.GetMonth(), prpBsh->From.GetDay(), 0, 0, 0);
	CTime olTo   = CTime(prpBsh->To.GetYear(), prpBsh->To.GetMonth(), prpBsh->To.GetDay(), 23, 59, 59);
	prpBsh->RUrno = prpBsd->Urno;
	prpBsh->From = HourStringToDate( CString( prpBsd->Esbg), olFrom);
	prpBsh->To = HourStringToDate( CString( prpBsd->Lsen), olFrom);//olFrom + CTimeSpan(atoi(prpBsd->Sdu1)*60);				
	if(prpBsh->From > prpBsh->To)
	{
		prpBsh->To = prpBsh->To + CTimeSpan(1, 0, 0, 0);
	}
	//CString olF1, olT1;
	//olF1 = prpBsh->From.Format("%d.%m.%Y-%H:%M");
	//olT1 = prpBsh->To.Format("%d.%m.%Y-%H:%M");
	prpBsh->BlFrom = HourStringToDate( CString( prpBsd->Bkf1), olFrom);	
	prpBsh->BlTo= HourStringToDate( CString( prpBsd->Bkt1), olFrom);						
	prpBsh->BreakFrom = prpBsh->BlFrom;		
	prpBsh->BreakTo = prpBsh->BlFrom + CTimeSpan(atoi(prpBsd->Bkd1)*60);
	if(prpBsh->From > prpBsh->BreakFrom)
	{
		prpBsh->BreakFrom = prpBsh->BreakFrom + CTimeSpan(1, 0, 0, 0);
	}

	if(prpBsh->BreakFrom  > prpBsh->BreakTo)
	{
		prpBsh->BreakTo = prpBsh->BreakTo + CTimeSpan(1, 0, 0, 0);
	}

	if(prpBsh->From > prpBsh->BlFrom)
	{
		prpBsh->BlFrom = prpBsh->BlFrom + CTimeSpan(1, 0, 0, 0);
	}


	if(prpBsh->BlFrom > prpBsh->BlTo)
	{
		prpBsh->BlTo = prpBsh->BlTo + CTimeSpan(1, 0, 0, 0);
	}
	prpBsh->Relative = CString(prpBsd->Bkr1);	
	prpBsh->Code = CString(prpBsd->Bsdc);
	prpBsh->Vafr = prpBsd->Vafr;		
	prpBsh->Vato = prpBsd->Vato;		

	prpBsh->Fctc = CString(prpBsd->Fctc);		

	prpBsh->Stretch = atoi(prpBsd->Sex1);	
	prpBsh->Shrink = atoi(prpBsd->Ssh1);	
	prpBsh->Type = CString(prpBsd->Type);		
	CCS_CATCH_ALL	
}

/*****************************/
void CoverageDiagramViewer::CalculateDailyBasicShifts()
{
	CCS_TRY
	POSITION rlPos;
	//for ( rlPos =  omBasicShiftKeyMap.GetStartPosition(); rlPos != NULL; )hag20030402
	for ( rlPos =  omBasicShiftUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		BASICSHIFTS *prlBasicShift;
		omBasicShiftUrnoMap.GetNextAssoc(rlPos,(void *&)llUrno,(void *& ) prlBasicShift);
		ogBasicData.PushBackUrno(llUrno);
	}

    omBasicShiftUrnoMap.RemoveAll();

	CMapPtrToPtr *polBsdUrnoMap;
	for ( rlPos =  omBasicShiftKeyMap.GetStartPosition(); rlPos != NULL; )
	{
		CString olKey;
		polBsdUrnoMap = NULL;//MWO
		omBasicShiftKeyMap.GetNextAssoc(rlPos, olKey,(void *& ) polBsdUrnoMap);
		if(polBsdUrnoMap != NULL)//MWO
		{
			polBsdUrnoMap->RemoveAll();
			delete polBsdUrnoMap;
		}
	}

	omBasicShiftKeyMap.RemoveAll();

	omBasicShiftData.DeleteAll();

	bmCalcBsdBars = true;
	int ilBsCount = 0;

	if (omStartTime == TIMENULL || omEndTime == TIMENULL)
		return;

	for ( rlPos = ogBsdData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		bool blDuplicate = false;
		BSDDATA *prlBsd;
		ogBsdData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlBsd);
		if(prlBsd != NULL)
		{
			CTime olFrom = CTime(omStartTime.GetYear(), omStartTime.GetMonth(), omStartTime.GetDay(), 0, 0, 0);
			CTime olTo   = CTime(omEndTime.GetYear(), omEndTime.GetMonth(), omEndTime.GetDay(),23, 59, 59);

			BASICSHIFTS *prlBasicShift = new BASICSHIFTS;

			//RST PRF 8889
			lmInternalUrno++;	
			prlBasicShift->Urno = lmInternalUrno;	

			prlBasicShift->RUrno = prlBsd->Urno;
			prlBasicShift->From = HourStringToDate( CString( prlBsd->Esbg), olFrom);
			prlBasicShift->To = HourStringToDate( CString( prlBsd->Lsen), olFrom);
			if(prlBasicShift->From > prlBasicShift->To)
			{
				prlBasicShift->From = prlBasicShift->From - CTimeSpan(1, 0, 0, 0);
				blDuplicate = true;
			}
			
			if (prlBasicShift->From < omStartTime && prlBasicShift->To > omStartTime)
			{
				blDuplicate = true;
			}

			prlBasicShift->BlFrom = HourStringToDate( CString( prlBsd->Bkf1), olFrom);	
			prlBasicShift->BlTo= HourStringToDate( CString( prlBsd->Bkt1), olFrom);	
				

			if(prlBasicShift->BlFrom > prlBasicShift->BlTo)
			{
				prlBasicShift->BlFrom = prlBasicShift->BlFrom - CTimeSpan(1, 0, 0, 0);
			}


			if(prlBasicShift->BlTo > prlBasicShift->To)
			{
				prlBasicShift->BlFrom = prlBasicShift->BlFrom - CTimeSpan(1, 0, 0, 0);
				prlBasicShift->BlTo = prlBasicShift->BlTo - CTimeSpan(1, 0, 0, 0);
			}

			prlBasicShift->BreakFrom = prlBasicShift->BlFrom;		
			prlBasicShift->BreakTo = prlBasicShift->BlFrom + CTimeSpan(atoi(prlBsd->Bkd1)*60);		
			
			prlBasicShift->Relative = CString(prlBsd->Bkr1);	
			prlBasicShift->Code = CString(prlBsd->Bsdc);		
			prlBasicShift->Stretch = atoi(prlBsd->Sex1);	
			prlBasicShift->Shrink = atoi(prlBsd->Ssh1);	
			prlBasicShift->Type = CString(prlBsd->Type);
			prlBasicShift->Vafr = prlBsd->Vafr;		
			prlBasicShift->Vato = prlBsd->Vato;		

			prlBasicShift->Fctc = CString(prlBsd->Fctc);		

			omBasicShiftData.Add(prlBasicShift);
			omBasicShiftUrnoMap.SetAt((void *)prlBasicShift->Urno,prlBasicShift);
				
			if (omBasicShiftKeyMap.Lookup((LPCSTR)prlBasicShift->Code,(void *&) polBsdUrnoMap) == TRUE)
			{
				polBsdUrnoMap->SetAt((void *)prlBasicShift->Urno,prlBasicShift);
			}
			else
			{
				polBsdUrnoMap = new CMapPtrToPtr;
				polBsdUrnoMap->SetAt((void *)prlBasicShift->Urno,prlBasicShift);
				omBasicShiftKeyMap.SetAt((LPCSTR)prlBasicShift->Code,polBsdUrnoMap);
			}

			ilBsCount++;

			if(blDuplicate)
			{
				BASICSHIFTS *prlBasicShift2 = new BASICSHIFTS;

				//RST PRF 8889
				lmInternalUrno++;	
				prlBasicShift2->Urno = lmInternalUrno;	
				
				prlBasicShift2->RUrno = prlBsd->Urno;
				prlBasicShift2->From = prlBasicShift->From + CTimeSpan(1,0,0,0);
				prlBasicShift2->To =  prlBasicShift->To + CTimeSpan(1,0,0,0);
						
				prlBasicShift2->SingleOffSet = 1;
					
				prlBasicShift2->BlFrom = prlBasicShift->BlFrom + CTimeSpan(1,0,0,0);	
				prlBasicShift2->BlTo = prlBasicShift->BlTo + CTimeSpan(1,0,0,0);	
					

				prlBasicShift2->BreakFrom = prlBasicShift->BreakFrom  + CTimeSpan(1,0,0,0);		
				prlBasicShift2->BreakTo = prlBasicShift->BreakTo + CTimeSpan(1,0,0,0);		
					
				prlBasicShift2->Relative = CString(prlBsd->Bkr1);	
				prlBasicShift2->Code = CString(prlBsd->Bsdc);		
				prlBasicShift2->Stretch = atoi(prlBsd->Sex1);	
				prlBasicShift2->Shrink = atoi(prlBsd->Ssh1);	
				prlBasicShift2->Type = CString(prlBsd->Type);
				prlBasicShift2->Vafr = prlBsd->Vafr;		
				prlBasicShift2->Vato = prlBsd->Vato;		

				prlBasicShift2->Fctc = CString(prlBsd->Fctc);		

				omBasicShiftData.Add(prlBasicShift2);
				omBasicShiftUrnoMap.SetAt((void *)prlBasicShift2->Urno,prlBasicShift2);
					
				if (omBasicShiftKeyMap.Lookup((LPCSTR)prlBasicShift2->Code,(void *&) polBsdUrnoMap) == TRUE)
				{
					polBsdUrnoMap->SetAt((void *)prlBasicShift2->Urno,prlBasicShift2);
				}
				else
				{
					polBsdUrnoMap = new CMapPtrToPtr;
					polBsdUrnoMap->SetAt((void *)prlBasicShift2->Urno,prlBasicShift2);
					omBasicShiftKeyMap.SetAt((LPCSTR)prlBasicShift2->Code,polBsdUrnoMap);
				}

				ilBsCount++;
			}

		}
	}

	CCS_CATCH_ALL	
}


/*******************/
void CoverageDiagramViewer::PrepareGrouping()
{
}

void CoverageDiagramViewer::PrepareFilter()
{
	CStringArray olFilterValues;
	CString olText;
	int ilLc,ilCount;

	////Variablen f�r PSFlightPage
	bmArrival  = false;
	bmDepature  = false;
	bmBetrieb  = false;
	bmPlanung  = false;
	bmPrognose  = false;
	bmCancelled  = false;
	bmNoop  = false;

////Variablen f�r PSAptPage
	bmOrigin  = false;
	bmDestination  = false;
	omCMapForApt.RemoveAll();		// Origin/Destination
	bmUseAllApt   = false;


////Variablen f�r PSAltPage
	omCMapForAlt.RemoveAll();		// Airline Types 
	bmUseAllAlt  = false;

////Variablen f�r PSTemplatePage
	omCMapForTemplate.RemoveAll();		// Template 
	bmUseAllTemplate  = false;

////Variablen f�r PSAlcPage
	omCMapForAct.RemoveAll();		// Aircraft Code 
	bmUseAllAct  = false;

////Variablen f�r PSPosPage
	omCMapForPos.RemoveAll();		// Aircraft Code 
	bmUseAllPos  = false;


	omTopViewName = GetViewName();

	
	
    GetFilter("FLIGHT", olFilterValues);

   ilCount = olFilterValues.GetSize();
	for (ilLc = 0; ilLc < ilCount; ilLc++)
	{	
		olText = olFilterValues[ilLc];
		if (olText.Find("ARRI=") == 0)
		{
			if (olText.GetLength() > 5)
			{
				bmArrival = olText.GetAt(5) == '1' ? TRUE : FALSE;
			}
		}
		else if (olText.Find("DEPA=") == 0)
		{
			if (olText.GetLength() > 5)
			{
				bmDepature = olText.GetAt(5) == '1' ? TRUE : FALSE;
			}
		}
		else if (olText.Find("BETR=") == 0)
		{
			if (olText.GetLength() > 5)
			{
				bmBetrieb = olText.GetAt(5) == '1' ? TRUE : FALSE;
			}
		}
		else if (olText.Find("PLAN=") == 0)
		{
			if (olText.GetLength() > 5)
			{
				bmPlanung = olText.GetAt(5) == '1' ? TRUE : FALSE;
			}
		}
		else if (olText.Find("CANC=") == 0)
		{
			if (olText.GetLength() > 5)
			{
				bmCancelled = olText.GetAt(5) == '1' ? TRUE : FALSE;
			}
		}
		else if (olText.Find("PROG=") == 0)
		{
			if (olText.GetLength() > 5)
			{
				bmPrognose = olText.GetAt(5) == '1' ? TRUE : FALSE;
			}
		}
		else if (olText.Find("NOOP=") == 0)
		{
			if (olText.GetLength() > 5)
			{
				bmNoop = olText.GetAt(5) == '1' ? TRUE : FALSE;
			}
		}
		
		
	}
	
	
    GetFilter("APTBUTTON", olFilterValues);

    ilCount = olFilterValues.GetSize();
	for (ilLc = 0; ilLc < ilCount; ilLc++)
	{
		olText = olFilterValues[ilLc];
		if (olText.Find("ORIGIN=") == 0)
		{
			if (olText.GetLength() > 7)
			{
				bmOrigin = olText.GetAt(7) == '1' ? true : false;
			}
		}
		else if (olText.Find("DEST=") == 0)
		{
			if (olText.GetLength() > 5)
			{
				bmDestination = olText.GetAt(5) == '1' ? true : false;
			}
		}
	}

	///////////// filter for aircraft type ///////////////////////
	GetFilter("ALC", olFilterValues);
    ilCount = olFilterValues.GetSize();
	if (ilCount > 0)
	{
		if (olFilterValues[0] == LoadStg(IDS_STRING61216))
		{
			bmUseAllAct = true;
		}
		else
		{
			for (ilLc = 0; ilLc < ilCount; ilLc++)
			{
				omCMapForAct[olFilterValues[ilLc]] = NULL;
			}
		}
	}

	///////////// filter for Template ///////////////////////


	GetFilter("TEMPBUTTON", olFilterValues);

    ilCount = olFilterValues.GetSize();
	for (ilLc = 0; ilLc < ilCount; ilLc++)
	{
		olText = olFilterValues[ilLc];
		if (olText.Find("NOTFLUG=") == 0)
		{
			if (olText.GetLength() > 8)
			{
				bmNotFlug = olText.GetAt(8) == '1' ? true : false;
			}
		}
		else if (olText.Find("FLUG=") == 0)
		{
			if (olText.GetLength() > 5)
			{
				bmFlug = olText.GetAt(5) == '1' ? true : false;
			}
		}
		else if (olText.Find("CCI=") == 0)
		{
			if (olText.GetLength() > 4)
			{
				bmCCI = olText.GetAt(4) == '1' ? true : false;
			}
		}
	}

	GetFilter("TEMPLATE", olFilterValues);

	CString *polValue = NULL;
	CString olValue = "";
	CStringArray olValList;
    ilCount = olFilterValues.GetSize();
	if (ilCount > 0)
	{
		CString olTest = olFilterValues[0];
		if (olFilterValues[0] == LoadStg(IDS_STRING61216))
		{
			bmUseAllTemplate = true;
			for(int ilTsr = 0; ilTsr < omTplTsrArray.GetSize();ilTsr++)
			{
				CString olTest = omTplTsrArray[ilTsr];
				ExtractItemList(omTplTsrArray[ilTsr],&olValList,';');
				for(int ilVals = 0 ; ilVals < olValList.GetSize(); ilVals++)
				{
					omFilterTsrArray.Add(olValList[ilVals]);
				}
			}

		}
		else
		{
			for (ilLc = 0; ilLc < ilCount; ilLc++)
			{
				olTest = olFilterValues[ilLc];
				if(omTplTsrMap.Lookup(olTest,(void *&)polValue)  == TRUE)
				{
					olValue = *polValue;
					ExtractItemList(olValue,&olValList,';');
					for(int ilVals = 0 ; ilVals < olValList.GetSize(); ilVals++)
					{
						omFilterTsrArray.Add(olValList[ilVals]);
					}
				}

				omCMapForTemplate[olFilterValues[ilLc]] = NULL;
			}
		}
	}
	olValList.RemoveAll();
	///////////// filter for airline types ///////////////////////
    GetFilter("ALT", olFilterValues);
    ilCount = olFilterValues.GetSize();
	if (ilCount > 0)
	{
		if (olFilterValues[0] == LoadStg(IDS_STRING61216))
		{
			bmUseAllAlt = true;
		}
		else
		{
			for (ilLc = 0; ilLc < ilCount; ilLc++)
			{
				omCMapForAlt[olFilterValues[ilLc]] = NULL;
			}
		}
	}
	///////////// filter for origin/destination ///////////////////////
    GetFilter("APT", olFilterValues);
    ilCount = olFilterValues.GetSize();
	if (ilCount > 0)
	{
		if (olFilterValues[0] == LoadStg(IDS_STRING61216))
		{
			bmUseAllApt = true;
		}
		else
		{
			for (ilLc = 0; ilLc < ilCount; ilLc++)
			{
				omCMapForApt[olFilterValues[ilLc]] = NULL;
			}
		}
	}


   GetFilter("POS", olFilterValues);
    ilCount = olFilterValues.GetSize();
	if (ilCount > 0)
	{
		if (olFilterValues[0] == LoadStg(IDS_STRING61216))
		{
			bmUseAllPos = true;
		}
		else
		{
			for (ilLc = 0; ilLc < ilCount; ilLc++)
			{
				omCMapForPos[olFilterValues[ilLc]] = NULL;
			}
		}
	}


}

void CoverageDiagramViewer::PrepareEvaluationFilter(CString opViewName)
{
	
	CStringArray olFilterValues;
	CString olText;
	int ilLc,ilCount;
	CString olOldBaseViewName = GetBaseViewName();
	CString olOldViewName = GetViewName();

	bmEvulationFilterChanged = true;

	omEvalViewName = opViewName;

	SetViewerKey("BEWERTUNG");
	SelectView(opViewName);

	omGhsUrnos.RemoveAll();
	omPfcUrnos.RemoveAll();



/// Variablen Checkboxen Personal/Location/Equipment
	
    GetFilter("BEWSELBTN", olFilterValues);

	ilCount = olFilterValues.GetSize();
 
	bmPersonal = false;
	bmLocation = false;
	bmEquipment = false;

	for (ilLc = 0; ilLc < ilCount; ilLc++)
	{
		olText = olFilterValues[ilLc];
		if (olText.Find("PERS=") == 0)
		{
			if (olText.GetLength() > 5)
			{
				bmPersonal = olText.GetAt(5) == '1' ? true : false;
			}
		}
		else if (olText.Find("EQUI=") == 0)
		{
			if (olText.GetLength() > 5)
			{
				bmEquipment = olText.GetAt(5) == '1' ? true : false;
			}
		}
		else if (olText.Find("LTYP=") == 0)
		{
			if (olText.GetLength() > 5)
			{
				bmLocation = olText.GetAt(5) == '1' ? true : false;
			}
		}
	}

	/// Variablen Checkboxen FilterIndex
	
    GetFilter("BEWFIDXBTN", olFilterValues);

	ilCount = olFilterValues.GetSize();
 
	for (ilLc = 0; ilLc < ilCount; ilLc++)
	{
		olText = olFilterValues[ilLc];
		if (olText.Find("FTYP=") == 0)
		{
			if (olText.GetLength() > 5)
			{
				char olType = olText.GetAt(5);
				imViewerFiterIdx = (FilterType) atoi(&olType);
			}
		}
		else if (olText.Find("EQUTYP=") == 0)
		{
			if (olText.GetLength() > 7)
			{
				int ilIndex = 0;
				olText = olText.Mid(7);	
				if (olText != LoadStg(IDS_STRING61216))	// *ALL means nothing here
				{
					omViewerFilterEquipmentType = olText;
				}
				else
				{
					omViewerFilterEquipmentType = LoadStg(IDS_STRING61216);
				}
			}
		}					

	}
	

   GetFilter("BEWPFC", olFilterValues);
   ilCount = olFilterValues.GetSize();

   omPfcCodeList.Empty();

   omCMapForPrf.RemoveAll();
   omPfcMapForGraphic.RemoveAll();
   CString olTmp = "";
	bmUseAllPrf = false;
	bmUseAllPfcForGraphic = false;
	if (ilCount > 0)
	{
		CString olTmpText;
		CStringArray olItemList;
		CStringArray olItemList2;
		if (olFilterValues[0] == LoadStg(IDS_STRING61216))
		{
			bmUseAllPrf = true;
			bmUseAllPfcForGraphic = true;
			CString olTmpText;
			for(int i = 0; i < ogPfcData.omData.GetSize(); i++)
			{
				long llUrno = ogPfcData.omData[i].Urno;
				if(llUrno > 0)
				{
					olTmpText = ogPfcData.omData[i].Fctc ;
					omPfcCodeList += CString(",") + olTmpText;
					omPfcUrnos.Add(ogPfcData.omData[i].Urno);
				}

			}
			ilCount = ogBCD.GetDataCount("SGR");
			CString olTab;
			for( i = 0; i < ilCount; i++)
			{
				olTab = ogBCD.GetField("SGR", i, "TABN");
				if(olTab == "PFC")
				{
					olTmpText = ogBCD.GetField("SGR", i, "URNO") ;
					omPfcUrnos.Add(atol(olTmpText));
					olTmpText = ogBCD.GetField("SGR", i, "GRPN");
					omPfcCodeList += CString(",") + olTmpText;
				}
			}

		}
		else
		{
			for (ilLc = 0; ilLc < ilCount; ilLc++)
			{
				olTmpText = olFilterValues[ilLc];
				CString olFctc;
				omPfcMapForGraphic.Add(olTmpText);
				ExtractItemList(olTmpText, &olItemList, '�');
				if(olItemList.GetSize() > 1)
				{
					olTmp = olItemList[0];
					olFctc = olItemList[1];
					if(olTmp.Find("S") > -1)
					{
						olTmp = olTmp.Left(olTmp.GetLength() -1 );
						ExtractItemList(olFctc, &olItemList2, '%');
						if(olItemList2.GetSize() > 0)
						{
							olFctc = olItemList2[0];
						}
					}
					else if(olTmp.Find("G") > -1)
					{
						olTmp = olTmp.Left(olTmp.GetLength() -1 );
						ExtractItemList(olFctc, &olItemList2, '%');
						if(olItemList2.GetSize() > 1)
						{
							olFctc = olItemList2[1];
						}
					}
					omCMapForPrf.Add(olTmp);
					omPfcUrnos.Add(atol(olTmp));
					omPfcCodeList += CString(",") + olFctc;
				}
			}
			olItemList2.RemoveAll();
		}
	}

	if(!omPfcCodeList.IsEmpty())
	{
		omPfcCodeList+= CString(",");
	}

   GetFilter("BEWGHS", olFilterValues);
   ilCount = olFilterValues.GetSize();
 
   omCMapForRpq.RemoveAll();
   omGhsMapForGraphic.RemoveAll();

	bmUseAllRpq = false;
	bmUseAllGhsForGraphic = false;

	omQualiCodeList.Empty();
	if (ilCount > 0)
	{
		CString olTmpText;
		CStringArray olItemList;
		CStringArray olItemList2;
		if (olFilterValues[0] == LoadStg(IDS_STRING61216))
		{
			bmUseAllRpq = true;
			bmUseAllGhsForGraphic = true;

		}
		else
		{
			for (ilLc = 0; ilLc < ilCount; ilLc++)
			{
				olTmpText = olFilterValues[ilLc];				
				omGhsMapForGraphic.Add(olTmpText);
				ExtractItemList(olTmpText, &olItemList, '�');
				if(olItemList.GetSize() > 1)
				{
					olTmp = olItemList[0];
					if(olTmp.Find("S") || olTmp.Find("G"))
					{
						olTmp = olTmp.Left(olTmp.GetLength() -1 );
					}
					omCMapForRpq.Add(olTmp);
					olTmp = olItemList[1];
					ExtractItemList(olTmp, &olItemList2, '%');
					if(olItemList2.GetSize() > 0)
					{
						omQualiCodeList+= ",";
						omQualiCodeList+= olItemList2[0];				
						
					}
				}
			}
		}
	}
	if(!omQualiCodeList.IsEmpty())
	{
		omQualiCodeList += CString(",");
	}

	GetFilter("BEWEQU", olFilterValues);
    ilCount = olFilterValues.GetSize();

	omCMapForReq.RemoveAll();
    omEquMapForGraphic.RemoveAll();

	bmUseAllReq = false;
	bmUseAllEquForGraphic = false;

	if (ilCount > 0)
	{
		CString olTmpText;
		CStringArray olItemList;
		if (olFilterValues[0] == LoadStg(IDS_STRING61216))
		{

            bmUseAllReq = true;
			bmUseAllEquForGraphic = true;

		}
		else
		{
			for (ilLc = 0; ilLc < ilCount; ilLc++)
			{
				olTmpText = olFilterValues[ilLc];				
				omEquMapForGraphic.Add(olTmpText);
				ExtractItemList(olTmpText, &olItemList, '�');
				if(olItemList.GetSize() > 1)
				{
					olTmp = olItemList[0];
					if(olTmp.Find("S") || olTmp.Find("G"))
					{
						olTmp = olTmp.Left(olTmp.GetLength() -1 );
					}
					omCMapForReq.Add(olTmp);
				}
			}
		}
	}

	GetFilter("BEWLCIC", olFilterValues);   // caonima
    ilCount = olFilterValues.GetSize();

	omCMapForCic.RemoveAll();
    omCicMapForGraphic.RemoveAll();

	bmUseAllCic = false;
	bmUseAllCicForGraphic = false;

	if (ilCount > 0)
	{
		CString olTmpText;
		CStringArray olItemList;
		if (olFilterValues[0] == LoadStg(IDS_STRING61216))
		{
			bmUseAllCic = true;
			bmUseAllCicForGraphic = true;

		}
		else
		{
			for (ilLc = 0; ilLc < ilCount; ilLc++)
			{
				olTmpText = olFilterValues[ilLc];				
				omCicMapForGraphic.Add(olTmpText);
				ExtractItemList(olTmpText, &olItemList, '�');
				if(olItemList.GetSize() > 1)
				{
					olTmp = olItemList[0];
					if(olTmp.Find("S") || olTmp.Find("G"))
					{
						olTmp = olTmp.Left(olTmp.GetLength() -1 );
					}
					omCMapForCic.Add(olTmp);
				}
			}
		}
	}

	GetFilter("BEWLPST", olFilterValues);
    ilCount = olFilterValues.GetSize();

	omCMapForPst.RemoveAll();
    omPstMapForGraphic.RemoveAll();

	bmUseAllPst = false;
	bmUseAllPstForGraphic = false;

	if (ilCount > 0)
	{
		CString olTmpText;
		CStringArray olItemList;
		if (olFilterValues[0] == LoadStg(IDS_STRING61216))
		{
			bmUseAllPst = true;
			bmUseAllPstForGraphic = true;

		}
		else
		{
			for (ilLc = 0; ilLc < ilCount; ilLc++)
			{
				olTmpText = olFilterValues[ilLc];				
				omPstMapForGraphic.Add(olTmpText);
				ExtractItemList(olTmpText, &olItemList, '�');
				if(olItemList.GetSize() > 1)
				{
					olTmp = olItemList[0];
					if(olTmp.Find("S") || olTmp.Find("G"))
					{
						olTmp = olTmp.Left(olTmp.GetLength() -1 );
					}
					omCMapForPst.Add(olTmp);
				}
			}
		}
	}

	GetFilter("BEWLGAT", olFilterValues);
    ilCount = olFilterValues.GetSize();

	omCMapForGat.RemoveAll();
    omGatMapForGraphic.RemoveAll();

	bmUseAllGat = false;
	bmUseAllGatForGraphic = false;

	if (ilCount > 0)
	{
		CString olTmpText;
		CStringArray olItemList;
		if (olFilterValues[0] == LoadStg(IDS_STRING61216))
		{
			bmUseAllGat = true;
			bmUseAllGatForGraphic = true;
		}
		else
		{
			for (ilLc = 0; ilLc < ilCount; ilLc++)
			{
				olTmpText = olFilterValues[ilLc];				
				omGatMapForGraphic.Add(olTmpText);
				ExtractItemList(olTmpText, &olItemList, '�');
				if(olItemList.GetSize() > 1)
				{
					olTmp = olItemList[0];
					if(olTmp.Find("S") || olTmp.Find("G"))
					{
						olTmp = olTmp.Left(olTmp.GetLength() -1 );
					}
					omCMapForGat.Add(olTmp);
				}
			}
		}
	}




	SetViewerKey(olOldBaseViewName);
	SelectView(olOldViewName);


}

void CoverageDiagramViewer::PrepareBaseShiftsFilter(CString opViewName)
{
	
	CStringArray olFilterValues;
	CString olText;
	int ilLc,ilCount;
	CString olOldBaseViewName = GetBaseViewName();
	CString olOldViewName = GetViewName();


	SetViewerKey("BASISSCHICHTEN");
	SelectView(opViewName);

	bmCalcBsdBars = true;

/// Variablen Checkboxen Personal/Location/Equipment
	
    GetFilter("BSH", olFilterValues);

	ilCount = olFilterValues.GetSize();

    omCMapForBsh.RemoveAll();
    CString olTmp = "";
	bmUseAllBsh = false;
	if (ilCount > 0)
	{
		CString olTmpText;
		CStringArray olItemList;
		if (olFilterValues[0] == LoadStg(IDS_STRING61216))
		{
			bmUseAllBsh = true;
		}
		else
		{
			for (ilLc = 0; ilLc < ilCount; ilLc++)
			{
				omCMapForBsh[olFilterValues[ilLc]] = NULL;
			}
		}
	}



	SetViewerKey(olOldBaseViewName);
	SelectView(olOldViewName);


}

void CoverageDiagramViewer::PrepareSorter()
{
}

bool CoverageDiagramViewer::IsPassBshFilter(BASICSHIFTS * prlBsh)
{
	void *p;
	bool blRet = bmUseAllBsh;
	CCS_TRY

	if (bmUseAllBsh == false)
	{
		blRet = (omCMapForBsh.Lookup(CString(prlBsh->Code),p) == TRUE)?true:false;
	}
/*******************************
	if(prlBsh->Vafr != TIMENULL && prlBsh->Vato != TIMENULL)
	{
		if(!(prlBsh->Vafr < omEndTime && prlBsh->Vato > omEndTime))
		{
			return false;
		}
	}
	
****************/
	return blRet;
	CCS_CATCH_ALL
	return false;
}



bool CoverageDiagramViewer::IsPassSdtFilter(SDTDATA *prpSdt)
{
	CCS_TRY
	bool blRet = true;
	CString olTotalFctc = "";

	if(bmShiftRoster)
	{
		if(bmIsRealRoster)
		{
			if(imViewerFiterIdx == QUALIFICATION)
			{
				olTotalFctc = ogSdtRosterData.GetTotalQuali(prpSdt->Urno);
			}
			else
			{
				olTotalFctc = ogSdtRosterData.GetTotalFctc(prpSdt->Urno);
			}

			if(bmShowActualDsr)
			{
				if(*prpSdt->StatPlSt != 'A')
				{
					blRet = false;
				}
			}
			else
			{
				int ilLevel = GetCurrentDemandLevel(SHIFT_DEMAND);
				if(prpSdt->Planungsstufe != ilLevel)
				{
					blRet = false;
				}
			}
		}
		else
		{
			olTotalFctc = LoadStg(IDS_STRING61216);
			blRet = true;
		}

				
	}
	else
	{

		olTotalFctc = ogSdtData.GetTotalFctc(prpSdt->Urno);

		if(bmActuellState)
		{
			blRet = !prpSdt->IsVirtuel;
		}
		else
		{
			blRet = prpSdt->IsVirtuel;
			if(prpSdt->Sdgu != lmSdgu)
			{
				blRet = false;
			}

		}
	}
	if(blRet)
	{
//		if(IsOverlapped(omFrameStart, omFrameEnd, prpSdt->Esbg, prpSdt->Lsen) == FALSE)
		if(IsOverlapped(omStartTime, omEndTime, prpSdt->Esbg, prpSdt->Lsen) == FALSE)
		{
			blRet = false;
		}
		else
		{
			if(olTotalFctc == LoadStg(IDS_STRING61216))
			{
				blRet = true;
			}
			else
			{
				CStringArray olFctcs;
				blRet = false;
				ExtractItemList(olTotalFctc,&olFctcs,',');
				if(imViewerFiterIdx == QUALIFICATION)
				{
					if(bmUseAllRpq)
					{
						blRet = true;
					}
					else
					{
						for(int ilFctc = 0; ilFctc < olFctcs.GetSize() && !blRet ;ilFctc++)
						{
							CString olSingleFctc = ",";
							olSingleFctc += olFctcs[ilFctc];
							olSingleFctc += ",";
							
							if(omQualiCodeList.Find(olSingleFctc) > -1)
							{
								blRet = true;
							}
						}
					}
				}
				else
				{
					for(int ilFctc = 0; ilFctc < olFctcs.GetSize() && !blRet ;ilFctc++)
					{
						CString olSingleFctc = ",";
						olSingleFctc += olFctcs[ilFctc];
						olSingleFctc += ",";
						
						if(omPfcCodeList.Find(olSingleFctc) > -1)
						{
							blRet = true;
						}
					}
				}
			}
		}
	}
	return blRet;
	CCS_CATCH_ALL
	return false;
}

bool CoverageDiagramViewer::IsPassSdtFilterWorktime(SDTDATA *prpSdt)
{
	CCS_TRY
	bool blRet = true;
	CString olTotalFctc = "";

	if(bmShiftRoster)
	{
		if(bmIsRealRoster)
		{
			if(imViewerFiterIdx == QUALIFICATION)
			{
				olTotalFctc = ogSdtRosterData.GetTotalQuali(prpSdt->Urno);
			}
			else
			{
				olTotalFctc = ogSdtRosterData.GetTotalFctc(prpSdt->Urno);
			}

			if(bmShowActualDsr)
			{
				if(*prpSdt->StatPlSt != 'A')
				{
					blRet = false;
				}
			}
			else
			{
				int ilLevel = GetCurrentDemandLevel(SHIFT_DEMAND);
				if(prpSdt->Planungsstufe != ilLevel)
				{
					blRet = false;
				}
			}
		}
		else
		{
			olTotalFctc = LoadStg(IDS_STRING61216);
			blRet = true;
		}

				
	}
	else
	{

		olTotalFctc = ogSdtData.GetTotalFctc(prpSdt->Urno);

		if(bmActuellState)
		{
			blRet = !prpSdt->IsVirtuel;
		}
		else
		{
			blRet = prpSdt->IsVirtuel;
			if(prpSdt->Sdgu != lmSdgu)
			{
				blRet = false;
			}

		}
	}
	if(blRet)
	{
		if(olTotalFctc == LoadStg(IDS_STRING61216))
		{
			blRet = true;
		}
		else
		{
			CStringArray olFctcs;
			blRet = false;
			ExtractItemList(olTotalFctc,&olFctcs,',');
			if(imViewerFiterIdx == QUALIFICATION)
			{
				if(bmUseAllRpq)
				{
					blRet = true;
				}
				else
				{
					for(int ilFctc = 0; ilFctc < olFctcs.GetSize() && !blRet ;ilFctc++)
					{
						CString olSingleFctc = ",";
						olSingleFctc += olFctcs[ilFctc];
						olSingleFctc += ",";
							
						if(omQualiCodeList.Find(olSingleFctc) > -1)
						{
							blRet = true;
						}
					}
				}
			}
			else
			{
				for(int ilFctc = 0; ilFctc < olFctcs.GetSize() && !blRet ;ilFctc++)
				{
					CString olSingleFctc = ",";
					olSingleFctc += olFctcs[ilFctc];
					olSingleFctc += ",";
						
					if(omPfcCodeList.Find(olSingleFctc) > -1)
					{
						blRet = true;
					}
				}
			}
		}
	}
	return blRet;
	CCS_CATCH_ALL
	return false;
}

bool CoverageDiagramViewer::IsSdtVisible(SDTDATA *prpSdt)
{
	CCS_TRY
		bool blRet = false;
	
		if(IsOverlapped(omStartTime, omEndTime, prpSdt->Esbg, prpSdt->Lsen))
		{
			blRet = true;
		}
		return blRet;
	CCS_CATCH_ALL
	return false;
}



int CoverageDiagramViewer::CompareGroup(COV_GROUPDATA *prpGroup1, COV_GROUPDATA *prpGroup2)
{
	// Groups in StaffDiagram always ordered by GroupId, so let's compare them.

	int ilRet = 0;
	CCS_TRY
	if(prpGroup1->GroupNo == prpGroup2->GroupNo)
	{
		ilRet = 0;
	}
	else if(prpGroup1->GroupNo > prpGroup2->GroupNo)
	{
		ilRet = 1;
	}
	else
	{
		ilRet = -1;
	}
	CCS_CATCH_ALL	
	return ilRet;
}

int CoverageDiagramViewer::CompareSdtLine(COV_LINEDATA *prpLine1, COV_LINEDATA *prpLine2)
{
	int ilCompareResult=0;
	CCS_TRY
	if(prpLine1 != NULL && prpLine2 != NULL)
	{
		ilCompareResult = (prpLine1->StartTime == prpLine2->StartTime) ? 
										((prpLine1->EndTime == prpLine2->EndTime) ? 0: 
										(prpLine1->EndTime > prpLine2->EndTime) ? 1: -1):
						  (prpLine1->StartTime > prpLine2->StartTime) ? 1: -1;

		if (ilCompareResult == 0)
		{
			ilCompareResult = prpLine1->Urno == prpLine2->Urno ? 0 :
							  prpLine1->Urno > prpLine2->Urno ? 1 : -1;
		}

	}
	
	return ilCompareResult;
	CCS_CATCH_ALL
	return 0;
}
static int CompareCovLine(const COV_LINEDATA **prpLine1, const COV_LINEDATA **prpLine2)
{
	int ilCompareResult=0;
	CCS_TRY
	

	ilCompareResult = ((*prpLine1)->Index - (*prpLine2)->Index);
	
	return ilCompareResult;
	CCS_CATCH_ALL
	return 0;
}

int CoverageDiagramViewer::CompareBshLine(COV_LINEDATA *prpLine1, COV_LINEDATA *prpLine2)
{
	int ilCompareResult;
	CCS_TRY
	if(prpLine1 != NULL && prpLine2 != NULL)
	{
		ilCompareResult = (prpLine1->StartTime == prpLine2->StartTime) ? 
										((prpLine1->EndTime == prpLine2->EndTime) ? (prpLine1->Index - prpLine2->Index): 
										(prpLine1->EndTime > prpLine2->EndTime) ? 1: -1):
						  (prpLine1->StartTime > prpLine2->StartTime) ? 1: -1;
	}
	CCS_CATCH_ALL	
	return ilCompareResult;
}

static int SortBshLine(const COV_LINEDATA **prpLine1,const COV_LINEDATA **prpLine2)
{
	int ilCompareResult;
	CCS_TRY
	if(prpLine1 != NULL && prpLine2 != NULL)
	{
		ilCompareResult = ((*prpLine1)->StartTime == (*prpLine2)->StartTime) ? 
										(((*prpLine1)->EndTime == (*prpLine2)->EndTime) ? ((*prpLine1)->Index - (*prpLine2)->Index): 
										((*prpLine1)->EndTime > (*prpLine2)->EndTime) ? 1: -1):
						  ((*prpLine1)->StartTime > (*prpLine2)->StartTime) ? 1: -1;
	}
	CCS_CATCH_ALL	
	return ilCompareResult;
}


/////////////////////////////////////////////////////////////////////////////
// CoverageDiagramViewer -- Data for displaying graphical objects
//
// The related graphical class: StaffDiagram, StaffChart, and StaffGantt will use this
// StaffViewer as the source of data for displaying GanttLine and GanttBar objects.
//
// We let the previous section handles all filtering, sorting, and grouping. In this
// section, we will provide a set of methods which will help us to create the data
// represent the graphical GanttLine and GanttChart as easiest as possible.
//
// Before we can display the data correctly, we have to understand the structure of
// the JOBCKI database table. Actually, this table (JOBCKI) is the core of the application.
// We stores every jobs of every job types in it. There are many linking fields across all
// of these possible job types. For better understanding, we will describe them type by
// type. Remember that JOBCKI.URNO will be always unique.
//
//	JOBPOOL		Basically, this job will be created by dragging staff shifts from the
//				PrePlanTable and drop them into anywhere (except TopScaleText) in the
//				StaffDiagram. (Remember that if the PrePlanTable is opened, the StaffDiagram
//				has to work in the "PrePlan" mode.) However, the PDI operators may drag
//				flight managers and drop them to the GateArea button in GateDiagram or to the
//				CciArea button in CciDiagram also.
//
//				This JobPool is the basic job of the employee. The other jobs have to refer
//				to this job with the field JOBCKI.JOUR.
//
//				This job type could be identified by JCTO == "POL". Normally, its JOUR should
//				be zero. Its ALID should be PoolId. We use the field FLUR for URNO of the
//				assigned shift (from PrePlanTable). We also copy PKNO from the shift to this
//				JobPool also. Its PRID should be blanks. Now, special cases, if the user drag
//				flight managers from PrePlanTable and drop them to the GateArea button in
//				GateDiagram, we will store the GateAreaId into the field PRID. If the user
//				drag flight managers from PrePlanTable and drop them to the CciArea button in
//				the CciDiagram, we will store the CciAreaId into the field PRID. (In this
//				application, we can assume that there is no duplicate of URNO in every
//				tables). For the JobPool which created from the reassignment (JOBDETACH), we
//				will store URNO of the previous job to the field JOUR.
//
//	JOBFLIGHT	Basically, this job will be created by dragging staff shifts from the
//				StaffDiagram and drop them into a FlightBar in the GateDiagram.
//				
//				This job type could be identified by JCTO == "FLT". Its JOUR will be the
//				URNO of its JobPool. Its ALID will be the Gate of that flight. Its FLUR will
//				be the URNO of the associated flight. We also store employee ID into the field
//				PKNO. Its PRID must be blank.
//
//				However, there is a special kind of JobFlight. For the flight arrived in the
//				morning (let's say before the early shift), the user may drag some staff from
//				the PrePlanTable and drop them into	a FlightBar directly. This will create a
//				JobFlight also, but without the	associated JobPool. This special JobFlight will
//				not be displayed in the	StaffDiagram but will be displayed as an indicator in
//				the GateDiagram, and will be displayed in the PrePlanTable also.
//
//				For this special JobFlight, its PRID will be not empty (actually should be
//				"Special"). The field FLUR of these special JobFlight will be the URNO of the
//				associated Shift (not the associated JobPool, since we know nothing about it).
//
//	JOBMANAGER	Basically, this job will be created by dragging a flight from the Gate Detail
//				Window and drop it to the TopScaleText in StaffDiagram or GateDiagram. The
//				prerequisites before this type of job creation is that flight manager must
//				has already a JobGateArea assigned to the gate area for that flight.
//
//				This job type could be identified by JCTO == "FMJ". Its JOUR will be the URNO
//				of its JobGateArea. Its ALID will be the Gate of that flight. Its FLUR will
//				be the URNO of the associated flight. We also store employee ID into the field
//				PKNO. Its PRID must be blank.
//
//	JOBCCI		Basically, this job will be created by dragging staff shifts from the
//				StaffDiagram and drop them into the area of GanttChart in the CciDiagram (may
//				be in the vertical scale or in the GanttLine).
//
//				This job type could be identified by JCTO == "CCI". Its JOUR will be the
//				URNO of its JobPool. Its ALID will be the CCI-Desk which the employee was
//				assigned to. We also store employee ID into the field PKNO.
//
//	JOBGATE		Basically, this job will be created by dragging from the StaffDiagram to the
//				vertical scale in GateDiagram. This will assign the employees who are not
//				flight managers to work at the specified gate. (Remember that if the user does
//				this, we will have to perform automatic assignment -- for resolving demands
//				left on that gate.)
//
//				This job type could be identified by JTCO == "GAT". Its JOUR will be the
//				URNO of its JobPool. Its ALID will be the Gate which the employee was assigned
//				to. We also store employee ID into the field PKNO.
//
//	JOBGATEAREA	Basically, this job will be created by dragging from the StaffDiagram to the
//				GateArea button in GateDiagram. This will assign the flight managers to work
//				at the specified gate area.
//
//				This job type could be identified by JTCO == "GTA". Its JOUR will be the URNO
//				of its JobPool. Its ALID will be the GateArea which the employee was assigned
//				to. We also store employee ID into the field PKNO.
//
//	JOBDETACH	Basically, this job will be created by reassignment some part of the duty bar
//				(JobPool) to one of the other Pools. When this reassignment happened, we will
//				create two jobs, one JobDetach for displaying on the old JobPool, and another
//				JobPool for displaying on the new pool.
//
//				This job type could be identified by JTCO == "DET". Its JOUR will be the URNO
//				of its JobPool. Its ALID will be the new PoolId which the employee was re-
//				-assigned to. Its FLUR will be the URNO of the original shift of that
//				reassignment. We also store employee ID into the field PKNO.
//
//	JOBBREAK	Basically, we can think of all these jobs in the same way. JobBreak will be
//	JOBSPECIAL	used for showing a break of an employee during a day. It will be displayed
//	JOBILLNESS	as a white-red stripes pattern bar in the StaffDiagram. JobSpecial is a general
//				purpose job for displaying jobs which not defined before (such as Meeting or
//				Training). JobIllness is just a special kind of JobSpecial. These jobs will
//				have thier JOUR as the URNO of its JobPool. Its PKNO will be the employee ID.
//				For JobSpecial and JobIllness, we will displayed the text which is saved in
//				the JOBCKI.PRID when displaying the bar.
//
// Methods for creating graphical objects:
// MakeGroups			Create groups that has to be displayed.
// MakeLinesAndManagers	Create lines for all pre-created groups (by scanning thru all JobPool)
// MakeBars				Create bars for all lines in all groups (rescanning -- 2nd round)
// MakeLineOrManager	Create a line or a manager depending on the employee's rank
// MakeBar				Create a job bar (which is not a JobPool)
//
// GroupText			Return a string displayed in the chart button
// LineText				Return a string displayed in the left scale text
// BarText				Return a string displayed in the bar of the specified job
//
void CoverageDiagramViewer::MakeGroups()
{
	CCS_TRY
	COV_GROUPDATA rlShiftDemand,
				  rlBasicShift,
				  rlZeroline,
				  rlDemands;


	CString olText;
	/*
	olText.LoadStg(COV_BTN_ZEROLINE);
	rlZeroline.GroupNo = ZEROLINE;
	rlZeroline.GroupName = olText;
    rlZeroline.Text = CString("");
	*/

	rlDemands.GroupNo = DEMANDS;
	olText = LoadStg(COV_BTN_DEMANDS);
	rlDemands.GroupName  = olText;
    rlDemands.Text = CString("");
	
	rlShiftDemand.GroupNo = SHIFT_DEMAND;
	olText = LoadStg(COV_BTN_SHIFTDEMANDS);
	rlShiftDemand.GroupName = olText;
    rlShiftDemand.Text = CString("");
	
	rlBasicShift.GroupNo = BASIC_SHIFT;
	olText = LoadStg(COV_BTN_BASICSHIFTS);
	rlBasicShift.GroupName = olText;
    rlBasicShift.Text = CString("");
	rlBasicShift.ShiftView = BASE_SHIFT_ALL;

	// Create a group for each value found in the corresponding filter
	//omGroups.NewAt(omGroups.GetSize(), rlZeroline);
	omGroups.NewAt(omGroups.GetSize(), rlDemands);
	omGroups.NewAt(omGroups.GetSize(), rlShiftDemand);
	omGroups.NewAt(omGroups.GetSize(), rlBasicShift);
	CCS_CATCH_ALL	
}

void CoverageDiagramViewer::MakeLines()
{
	CCS_TRY
	DWORD	llNow,llLast, llStart;
	double	flDiff;

	llLast = GetTickCount();
	llStart = llLast;

	POSITION rlPos;
	POSITION rlPos2;
	int ilMyCnt=0;
	if(bmCalcBsdBars)
	{
		while (GetLineCount(BASIC_SHIFT) > 0)
		{
			DeleteLine(BASIC_SHIFT, 0);
		}
				
			
		CMapPtrToPtr *polBsdUrnoMap;

		bool blValidShiftFound = false;

		for ( rlPos = omBasicShiftKeyMap.GetStartPosition(); rlPos != NULL; )
		{
			ilMyCnt++;
			CString olKey;
			BASICSHIFTS *prlBasicShift;
			omBasicShiftKeyMap.GetNextAssoc(rlPos, olKey,(void *& ) polBsdUrnoMap);

			blValidShiftFound = false;
			prlBasicShift = NULL;
			if(polBsdUrnoMap != NULL)
			{
				for ( rlPos2 =  polBsdUrnoMap->GetStartPosition(); rlPos2 != NULL ; )
				{
					long  llUrno;
					polBsdUrnoMap->GetNextAssoc(rlPos2, (void *& )llUrno,(void *& ) prlBasicShift);
					if(prlBasicShift != NULL)
					{
						blValidShiftFound = IsBasicShiftValid(prlBasicShift);
						if(blValidShiftFound)
						{
							prlBasicShift->DayOffSet =0;
							if(IsPassBshFilter(prlBasicShift)) 
							{
								MakeBshLine(prlBasicShift);
							}
						}
					}
				}
			}
			
		}
		bmCalcBsdBars = false;
	}


	for(int i = GetLineCount(BASIC_SHIFT); i < 10; i++)
	{
		MakeBsdEmptyLine();
		omGroups[BASIC_SHIFT].Lines.Sort(SortBshLine);
	}

	PositionBsdBars();

	for(int ilLc = 0; ilLc < omGroups[BASIC_SHIFT].Lines.GetSize(); ilLc++)
	{
		omGroups[BASIC_SHIFT].Lines[ilLc].Index = ilLc;
	}

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiaViewer::MakeLines:BASIC_SHIFTS %.3lf sec\n", flDiff/1000.0 );

	ilMyCnt=0;
	if(bmMaxView == true)
	{
		if(bmShiftRoster)
		{
			for (rlPos = omSdtUrnoMap.GetStartPosition(); rlPos != NULL; )
			{
				ilMyCnt++;
				long llUrno;
				SDTDATA *prlSdt;
				omSdtUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);//&olSdtArray[ilIndex];
				if(IsPassSdtFilter(prlSdt) && IsSdtVisible(prlSdt))
				{
					MakeSdtLine(prlSdt);
				}
			}
			llNow  = GetTickCount();
			flDiff = llNow-llLast;
			llLast = llNow;
			TRACE("CovDiaViewer::MakeLines:SHIFT_DEMAND/MakeSdtLine %.3lf sec\n", flDiff/1000.0 );
		}
		else
		{
			for (rlPos = omSdtUrnoMap.GetStartPosition(); rlPos != NULL; )
			{
				ilMyCnt++;
				long llUrno;
				SDTDATA *prlSdt;
				omSdtUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);//&olSdtArray[ilIndex];
				if(IsPassSdtFilter(prlSdt) && IsSdtVisible(prlSdt))
				{
					MakeSdtLine(prlSdt);
				}
			}

			llNow  = GetTickCount();
			flDiff = llNow-llLast;
			llLast = llNow;
			TRACE("CovDiaViewer::MakeLines:SHIFT_DEMAND/MakeSdtLine %.3lf sec\n", flDiff/1000.0 );
		}
	}
	else
	{
		CTime olOldTF = TIMENULL;
		CTime olOldTT = TIMENULL;
		CString olCode;
		int ilCurrentCount = 0;
		if(bmShiftRoster)
		{
			for (rlPos = omSdtUrnoMap.GetStartPosition(); rlPos != NULL; )
			{
				long llUrno;
				SDTDATA *prlSdt;
				omSdtUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);//&olSdtArray[ilIndex];
				if(prlSdt != NULL)
				{
					int ilLineno = -1;
					if(IsPassSdtFilter(prlSdt) && IsSdtVisible(prlSdt) )
					{
						ilCurrentCount++;
						if(FindSdtBarWithCode(SHIFT_DEMAND,prlSdt,ilLineno) == false)
						{
							ilMyCnt++;
							ilLineno = MakeSdtLine(prlSdt);
							if(ilLineno >= 0)
							{
								MakeSdtBar(SHIFT_DEMAND, ilLineno, prlSdt);
							}
							ilCurrentCount = 1;
						}
					}
				}
			}
			llNow  = GetTickCount();
			flDiff = llNow-llLast;
			llLast = llNow;
			TRACE("CovDiaViewer::MakeLines:SHIFT_DEMAND/MakeSdtLine %.3lf sec\n", flDiff/1000.0 );
		}
		else
		{
			for (rlPos = omSdtUrnoMap.GetStartPosition(); rlPos != NULL; )
			{
				ilMyCnt++;
				long llUrno;
				SDTDATA *prlSdt;
				omSdtUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);//&olSdtArray[ilIndex];
				if(prlSdt != NULL)
				{
					int ilLineno = -1;
					if(IsPassSdtFilter(prlSdt) && IsSdtVisible(prlSdt) )
					{
						ilCurrentCount++;
						if(FindSdtBarWithCode(SHIFT_DEMAND,prlSdt,ilLineno) == false)
						{
							ilLineno = MakeSdtLine(prlSdt);
							if(ilLineno >= 0)
							{
								MakeSdtBar(SHIFT_DEMAND, ilLineno, prlSdt);
							}
							ilCurrentCount = 1;
						}
					}
				}
			}
		}

	}
	for(i = GetLineCount(SHIFT_DEMAND); i < 21; i++)
	{
		MakeEmptyLine();
	}

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiaViewer::MakeLines:SHIFT_DEMAND/MakeEmptyLine %.3lf sec\n", flDiff/1000.0 );

	ilMyCnt=0;
	for( ilLc = 0; ilLc < omGroups[SHIFT_DEMAND].Lines.GetSize(); ilLc++)
	{
		ilMyCnt++;
		omGroups[SHIFT_DEMAND].Lines[ilLc].Index = ilLc;
	}
	omGroups[SHIFT_DEMAND].Lines.Sort(CompareCovLine);

	omGroups[SHIFT_DEMAND].LinesMap.RemoveAll();

	for( ilLc = 0; ilLc < omGroups[SHIFT_DEMAND].Lines.GetSize(); ilLc++)
	{
		omGroups[SHIFT_DEMAND].LinesMap.SetAt((void *)omGroups[SHIFT_DEMAND].Lines[ilLc].Urno,(void *)ilLc);
	}


	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiaViewer::MakeLines:SHIFT_DEMAND/Sort %.3lf sec\n", flDiff/1000.0 );

	
	CCS_CATCH_ALL	
}

void CoverageDiagramViewer::MakeLines(int ipGroupno)
{
	CCS_TRY
	DWORD	llNow,llLast, llStart;
	double	flDiff;

	llLast = GetTickCount();
	llStart = llLast;

	POSITION rlPos;
	POSITION rlPos2;
	int ilMyCnt=0;
	if (ipGroupno == BASIC_SHIFT)
	{
		while (GetLineCount(BASIC_SHIFT) > 0)
		{
			DeleteLine(BASIC_SHIFT, 0);
		}
				
			
		CMapPtrToPtr *polBsdUrnoMap;

		bool blValidShiftFound = false;

		for ( rlPos = omBasicShiftKeyMap.GetStartPosition(); rlPos != NULL; )
		{
			ilMyCnt++;
			CString olKey;
			BASICSHIFTS *prlBasicShift;
			omBasicShiftKeyMap.GetNextAssoc(rlPos, olKey,(void *& ) polBsdUrnoMap);

			blValidShiftFound = false;
			prlBasicShift = NULL;
			if(polBsdUrnoMap != NULL)
			{
				for ( rlPos2 =  polBsdUrnoMap->GetStartPosition(); rlPos2 != NULL ; )
				{
					long  llUrno;
					polBsdUrnoMap->GetNextAssoc(rlPos2, (void *& )llUrno,(void *& ) prlBasicShift);
					if(prlBasicShift != NULL)
					{
						blValidShiftFound = IsBasicShiftValid(prlBasicShift);
						if(blValidShiftFound)
						{
							prlBasicShift->DayOffSet =0;
							if(IsPassBshFilter(prlBasicShift)) 
							{
								MakeBshLine(prlBasicShift);
							}
						}
					}
				}
			}
			
		}
		bmCalcBsdBars = false;

		for(int i = GetLineCount(BASIC_SHIFT); i < 10; i++)
		{
			MakeBsdEmptyLine();
			omGroups[BASIC_SHIFT].Lines.Sort(SortBshLine);
		}

		PositionBsdBars();

		for(int ilLc = 0; ilLc < omGroups[BASIC_SHIFT].Lines.GetSize(); ilLc++)
		{
			omGroups[BASIC_SHIFT].Lines[ilLc].Index = ilLc;
		}

		llNow  = GetTickCount();
		flDiff = llNow-llLast;
		llLast = llNow;
		TRACE("CovDiaViewer::MakeLines:BASIC_SHIFTS %.3lf sec\n", flDiff/1000.0 );
	}
	else if (ipGroupno == SHIFT_DEMAND)
	{
		ilMyCnt=0;
		if(bmMaxView == true)
		{
			if(bmShiftRoster)
			{
				for (rlPos = omSdtUrnoMap.GetStartPosition(); rlPos != NULL; )
				{
					ilMyCnt++;
					long llUrno;
					SDTDATA *prlSdt;
					omSdtUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);//&olSdtArray[ilIndex];
					if(IsPassSdtFilter(prlSdt) && IsSdtVisible(prlSdt))
					{
						MakeSdtLine(prlSdt);
					}
				}
				llNow  = GetTickCount();
				flDiff = llNow-llLast;
				llLast = llNow;
				TRACE("CovDiaViewer::MakeLines:SHIFT_DEMAND/MakeSdtLine %.3lf sec\n", flDiff/1000.0 );
			}
			else
			{
				for (rlPos = omSdtUrnoMap.GetStartPosition(); rlPos != NULL; )
				{
					ilMyCnt++;
					long llUrno;
					SDTDATA *prlSdt;
					omSdtUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);//&olSdtArray[ilIndex];
					if(IsPassSdtFilter(prlSdt) && IsSdtVisible(prlSdt))
					{
						MakeSdtLine(prlSdt);
					}
				}

				llNow  = GetTickCount();
				flDiff = llNow-llLast;
				llLast = llNow;
				TRACE("CovDiaViewer::MakeLines:SHIFT_DEMAND/MakeSdtLine %.3lf sec\n", flDiff/1000.0 );
			}
		}
		else
		{
			CTime olOldTF = TIMENULL;
			CTime olOldTT = TIMENULL;
			CString olCode;
			int ilCurrentCount = 0;
			if(bmShiftRoster)
			{
				for (rlPos = omSdtUrnoMap.GetStartPosition(); rlPos != NULL; )
				{
					long llUrno;
					SDTDATA *prlSdt;
					omSdtUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);//&olSdtArray[ilIndex];
					if(prlSdt != NULL)
					{
						int ilLineno = -1;
						if(IsPassSdtFilter(prlSdt) && IsSdtVisible(prlSdt) )
						{
							ilCurrentCount++;
							if(FindSdtBarWithCode(SHIFT_DEMAND,prlSdt,ilLineno) == false)
							{
								ilMyCnt++;
								ilLineno = MakeSdtLine(prlSdt);
								if(ilLineno >= 0)
								{
									MakeSdtBar(SHIFT_DEMAND, ilLineno, prlSdt);
								}
								ilCurrentCount = 1;
							}
						}
					}
				}
				llNow  = GetTickCount();
				flDiff = llNow-llLast;
				llLast = llNow;
				TRACE("CovDiaViewer::MakeLines:SHIFT_DEMAND/MakeSdtLine %.3lf sec\n", flDiff/1000.0 );
			}
			else
			{
				for (rlPos = omSdtUrnoMap.GetStartPosition(); rlPos != NULL; )
				{
					ilMyCnt++;
					long llUrno;
					SDTDATA *prlSdt;
					omSdtUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);//&olSdtArray[ilIndex];
					if(prlSdt != NULL)
					{
						int ilLineno = -1;
						if(IsPassSdtFilter(prlSdt) && IsSdtVisible(prlSdt) )
						{
							ilCurrentCount++;
							if(FindSdtBarWithCode(SHIFT_DEMAND,prlSdt,ilLineno) == false)
							{
								ilLineno = MakeSdtLine(prlSdt);
								if(ilLineno >= 0)
								{
									MakeSdtBar(SHIFT_DEMAND, ilLineno, prlSdt);
								}
								ilCurrentCount = 1;
							}
						}
					}
				}
			}

		}
		for(int i = GetLineCount(SHIFT_DEMAND); i < 21; i++)
		{
			MakeEmptyLine();
		}

		llNow  = GetTickCount();
		flDiff = llNow-llLast;
		llLast = llNow;
		TRACE("CovDiaViewer::MakeLines:SHIFT_DEMAND/MakeEmptyLine %.3lf sec\n", flDiff/1000.0 );

		ilMyCnt=0;
		for(int ilLc = 0; ilLc < omGroups[SHIFT_DEMAND].Lines.GetSize(); ilLc++)
		{
			ilMyCnt++;
			omGroups[SHIFT_DEMAND].Lines[ilLc].Index = ilLc;
		}
		omGroups[SHIFT_DEMAND].Lines.Sort(CompareCovLine);

		omGroups[SHIFT_DEMAND].LinesMap.RemoveAll();

		for( ilLc = 0; ilLc < omGroups[SHIFT_DEMAND].Lines.GetSize(); ilLc++)
		{
			omGroups[SHIFT_DEMAND].LinesMap.SetAt((void *)omGroups[SHIFT_DEMAND].Lines[ilLc].Urno,(void *)ilLc);
		}


		llNow  = GetTickCount();
		flDiff = llNow-llLast;
		llLast = llNow;
		TRACE("CovDiaViewer::MakeLines:SHIFT_DEMAND/Sort %.3lf sec\n", flDiff/1000.0 );
	}
	
	CCS_CATCH_ALL	
}

void CoverageDiagramViewer::PositionBsdBars()
{
	

	CTime olBarStart = TIMENULL;
	CTime olBarEnd = TIMENULL;

	POSITION rlPos;
	CMapPtrToPtr *polBsdUrnoMap;
	
	bool blBarChanged = false;
	COV_LINEDATA *prlLine = NULL;
	COV_BARDATA *prlBar = NULL;
	int ilLineCount = GetLineCount(BASIC_SHIFT);

	for (int ilLC = 0; ilLC < ilLineCount; ilLC++)
	{
		blBarChanged = false;
		prlLine = GetLine(BASIC_SHIFT, ilLC);

		olBarStart = prlLine->StartTime;
		olBarEnd = prlLine->EndTime;

		int ilOffSet = 0;
		if(olBarEnd < omStartTime)
		{
			for(ilOffSet = 0; olBarEnd < omStartTime;ilOffSet++)
			{
				olBarEnd += CTimeSpan(1,0,0,0);
				olBarStart += CTimeSpan(1,0,0,0);
			}
		}
		if(olBarStart > omEndTime)
		{
			for(ilOffSet = 0; olBarEnd > omEndTime;ilOffSet--)
			{
				olBarEnd -= CTimeSpan(1,0,0,0);
				olBarStart -= CTimeSpan(1,0,0,0);
			}
		}
	
		if(prlLine->SingleOffSet == 1)
		{
			if(olBarEnd < omEndTime)
			{
				ilOffSet++;
				olBarEnd += CTimeSpan(1,0,0,0);
				olBarStart += CTimeSpan(1,0,0,0);				
			}
		}



		prlLine->StartTime += CTimeSpan(ilOffSet,0,0,0);
		prlLine->EndTime += CTimeSpan(ilOffSet,0,0,0);
		int ilBars = prlLine->Bars.GetSize();
		for(int ilBarIdx = 0; ilBarIdx < ilBars; ilBarIdx++)
		{
			prlBar = &(prlLine->Bars[ilBarIdx]);

			prlBar->IsInvalid = false;

			BASICSHIFTS *prlShift = GetBasicShiftByUrno(prlBar->Urno);

			BASICSHIFTS *prlNextShift = NULL;
			bool blValidShiftFound = false;
			if(prlShift != NULL)
			{
				prlShift->DayOffSet += ilOffSet;
				
				if(!IsBasicShiftValid(prlShift))
				{
					if(omBasicShiftKeyMap.Lookup((LPCSTR)prlShift->Code,(void *&) polBsdUrnoMap) == TRUE)
					{
						for ( rlPos =  polBsdUrnoMap->GetStartPosition(); rlPos != NULL && !blValidShiftFound; )
						{
							long  llUrno;
							polBsdUrnoMap->GetNextAssoc(rlPos, (void *& )llUrno,(void *& ) prlNextShift);
							if(prlNextShift != NULL)
							{
								prlNextShift->DayOffSet = prlShift->DayOffSet;
								blValidShiftFound = IsBasicShiftValid(prlNextShift);
							}
						}
						if(blValidShiftFound)
						{
							ChangeBsdBar(prlNextShift,prlBar);
							blBarChanged = true;
						}
						else
						{
							prlBar->IsInvalid = true;
						}
					}
				}
							


				if(ilOffSet != 0 && !blBarChanged)
				{
				
					prlBar->StartTime += CTimeSpan(ilOffSet,0,0,0);
					prlBar->EndTime += CTimeSpan(ilOffSet,0,0,0);
					int ilDelSize = prlBar->Delegations.GetSize();
					for(int ilDelIdx = 0; ilDelIdx < ilDelSize; ilDelIdx++)
					{
						prlBar->Delegations[ilDelIdx].StartTime += CTimeSpan(ilOffSet,0,0,0);
						prlBar->Delegations[ilDelIdx].EndTime += CTimeSpan(ilOffSet,0,0,0);
					}
					ilDelSize = prlBar->Delegations2.GetSize();
					for( ilDelIdx = 0; ilDelIdx < ilDelSize; ilDelIdx++)
					{
						prlBar->Delegations2[ilDelIdx].StartTime += CTimeSpan(ilOffSet,0,0,0);
						prlBar->Delegations2[ilDelIdx].EndTime += CTimeSpan(ilOffSet,0,0,0);
					}
					ilDelSize = prlBar->Delegations3.GetSize();
					for( ilDelIdx = 0; ilDelIdx < ilDelSize; ilDelIdx++)
					{
						prlBar->Delegations3[ilDelIdx].StartTime += CTimeSpan(ilOffSet,0,0,0);
						prlBar->Delegations3[ilDelIdx].EndTime += CTimeSpan(ilOffSet,0,0,0);
					}
					ilDelSize = prlBar->Indicators.GetSize();
					for( ilDelIdx = 0; ilDelIdx < ilDelSize; ilDelIdx++)
					{
						prlBar->Indicators[ilDelIdx].StartTime += CTimeSpan(ilOffSet,0,0,0);
						prlBar->Indicators[ilDelIdx].EndTime += CTimeSpan(ilOffSet,0,0,0);
					}

				}
			}
				
		}
	}
	omGroups[BASIC_SHIFT].Lines.Sort(SortBshLine);
}

void CoverageDiagramViewer::MakeBars()
{
	CCS_TRY
    POSITION rlPos;
	int ilCount = 0;

	/************************
    for ( rlPos = omBasicShiftUrnoMap.GetStartPosition(); rlPos != NULL; )
    {
        long llUrno;
		BASICSHIFTS *prlBasicShift;
        omBasicShiftUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlBasicShift);

		if(IsPassBshFilter(prlBasicShift))
		{
			int ilLineno = 0;
			if(FindBshLine(	BASIC_SHIFT, ilLineno, prlBasicShift->Urno))
			{
					MakeBshBar(BASIC_SHIFT, ilLineno, prlBasicShift);
					ilCount++;
			}
		}
    }
	*************/
//MWO TO DO die Balken f�r die SDT Daten erzeugen

	if(bmMaxView == true)
	{
		SDTDATA *prlSdt = NULL;
		long llUrno = 0;
		for(rlPos = omSdtUrnoMap.GetStartPosition();rlPos!=NULL;)
		{
			omSdtUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
		
			if(IsPassSdtFilter(prlSdt))
			{
				int ilLineno = 0;
				if(FindSdtLine(	SHIFT_DEMAND, ilLineno, prlSdt->Urno))
				{
					//	MakeSdtBar(SHIFT_DEMAND, ilLineno, prlSdt);
						ilCount++;
				}
			}
		}
	}
	CCS_CATCH_ALL	
}

void CoverageDiagramViewer::MakeBars(int ipGroupno)
{
	CCS_TRY

    POSITION rlPos;
	int ilCount = 0;

	if (ipGroupno == SHIFT_DEMAND)
	{
		if(bmMaxView == true)
		{
			SDTDATA *prlSdt = NULL;
			long llUrno = 0;
			for(rlPos = omSdtUrnoMap.GetStartPosition();rlPos!=NULL;)
			{
				omSdtUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
			
				if(IsPassSdtFilter(prlSdt))
				{
					int ilLineno = 0;
					if(FindSdtLine(	SHIFT_DEMAND, ilLineno, prlSdt->Urno))
					{
							MakeSdtBar(SHIFT_DEMAND, ilLineno, prlSdt);
							ilCount++;
					}
				}
			}
		}
	}

	CCS_CATCH_ALL	
}

void CoverageDiagramViewer::MakeBshLine(BASICSHIFTS *prpBsh)
{
	CCS_TRY

		int ilLineNo = 0;
		COV_LINEDATA rlLine;
		rlLine.Urno = prpBsh->Urno;		// from Shift
		rlLine.Type = CString("B");		// Shift Demand Table Bar OR
														// Basic Shift Bar
	    rlLine.SingleOffSet = prpBsh->SingleOffSet;
	    rlLine.StartTime = prpBsh->From;
	    rlLine.EndTime   = prpBsh->To;
		int ilGrp = omGroups[BASIC_SHIFT].GroupNo;
		ilLineNo = CreateBshLine(omGroups[BASIC_SHIFT].GroupNo, &rlLine);

		MakeBshBar(BASIC_SHIFT, ilLineNo, prpBsh);
				
	int size = omGroups[BASIC_SHIFT].Lines.GetSize();
	CCS_CATCH_ALL	
}
//MWO TO DO SDT Line
void CoverageDiagramViewer::MakeEmptyLine()
{
	CCS_TRY
		COV_LINEDATA rlLine;
		rlLine.Urno = -1;		// from Shift
		rlLine.Type = CString("D");		// Shift Demand Table Bar OR
														// Basic Shift Bar

		CTime olT = CTime(2020, 12, 31, 0, 0, 0);
	    rlLine.StartTime = olT;
	    rlLine.EndTime   = olT;
		int ilGrp = omGroups[SHIFT_DEMAND].GroupNo;
		CreateBshLine(omGroups[SHIFT_DEMAND].GroupNo, &rlLine);
	CCS_CATCH_ALL	
}

//MWO TO DO SDT Line
void CoverageDiagramViewer::MakeBsdEmptyLine()
{
	CCS_TRY
		COV_LINEDATA rlLine;
		rlLine.Urno = -1;			// from Bsd
		rlLine.Type = CString("B");	// Shift Demand Table Bar OR Basic Shift Bar
		CTime olT = CTime(2020, 12, 31, 0, 0, 0);
	    rlLine.SingleOffSet = 0;
	    rlLine.StartTime = olT;
	    rlLine.EndTime   = olT;
		int ilGrp = omGroups[BASIC_SHIFT].GroupNo;
		CreateBshLine(omGroups[BASIC_SHIFT].GroupNo, &rlLine);
	CCS_CATCH_ALL	
}


int CoverageDiagramViewer::MakeSdtLine(SDTDATA *prpSdt)
{
	CCS_TRY
		COV_LINEDATA rlLine;
		rlLine.Urno = prpSdt->Urno;		// from Shift
		rlLine.Type = CString("D");		// Shift Demand Table Bar OR
														// Basic Shift Bar
	    rlLine.StartTime = prpSdt->Esbg;
	    rlLine.EndTime   = prpSdt->Lsen;
		int ilGrp = omGroups[SHIFT_DEMAND].GroupNo;
		return CreateSdtLine(omGroups[SHIFT_DEMAND].GroupNo, &rlLine);
	CCS_CATCH_ALL	
}


void CoverageDiagramViewer::MakeSdtBarData(COV_BARDATA *prlBar, SDTDATA *prpSdt, int ipCount)
{
	CCS_TRY
		
	CString olMasterName = "";
	prlBar->Urno = prpSdt->Urno;
	//prlBar->Brutto = 1; //ipCount;
	prlBar->Type = SHIFT_DEMAND;

//	memset(pcmBarText, ' ', (size_t)4096);
//	char *currPtr = pcmBarText;
//	int ilLen=0;
	if(bmMaxView == false)
	{
		prlBar->Text="";
		CString olStr = "";
		

		if(bmShiftRoster && !bmIsRealRoster)
		{
			prlBar->Brutto = GetCompressedSdtCount(prpSdt,true);
		}
		else
		{
			if(!bmShiftRoster)
			{
				prlBar->Brutto = GetCompressedSdtCount(prpSdt,false);
			}
			else
			{
				prlBar->Brutto = 1;
			}
		}
		olStr.Format(" [%d]",prlBar->Brutto); 
		char pclFctc[64];
		int ilPos = -1;
		if((ilPos=Find(prpSdt->Fctc," - "))>0)
		{
			strncpy(pclFctc,prpSdt->Fctc,ilPos);
			pclFctc[ilPos]='\0';
		}
		else
		{
			strcpy(pclFctc,prpSdt->Fctc);
		}
		prlBar->Text = CString(prpSdt->Bsdc) + CString("  ") + CString(pclFctc);
		if(bmShiftRoster)
			prlBar->Text += CString(prpSdt->Enames);
		prlBar->Text += olStr;

	}
	else
	{
		if(prpSdt->Sdgu > 0)
		{
			SDGDATA *prlSdg = ogSdgData.GetSdgByUrno(prpSdt->Sdgu);
			if(prlSdg != NULL)
			{
				olMasterName = " - Master: ";
				olMasterName += prlSdg->Dnam;
			}
		}

		CString olStr = "";
		if(bmShiftRoster && !bmIsRealRoster)
		{
			prlBar->Brutto = (int) prpSdt->Faktor ;
			olStr.Format(" [%d]",prlBar->Brutto); 
		}
		prlBar->Text = CString(prpSdt->Bsdc) + CString("  ") + CString(prpSdt->Fctc) + " " + olStr;
		if(bmShiftRoster)
			prlBar->Text += CString(prpSdt->Enames);
	}
	
	

	prlBar->StatusText =  CString("  ") + prpSdt->Bsdn + olMasterName;
	prlBar->StartTime = prpSdt->Esbg;
	prlBar->EndTime = prpSdt->Lsen;
	prlBar->FrameType = FRAMENONE;
	prlBar->MarkerType = MARKFULL;

	if(prpSdt!=NULL && prpSdt->IsSelected==FALSE)
	{
		prlBar->MarkerBrush = ogBrushs[LIME_IDX];
		prlBar->TextColor = ogColors[BLACK_IDX];
		//prlBar->TextColor = ogColors[WHITE_IDX];
	}
	else
	{
		// Highlighted colors here
		prlBar->MarkerBrush = ogBrushs[YELLOW_IDX];
		//prlBar->TextColor = ogColors[WHITE_IDX];
		prlBar->TextColor = ogColors[BLACK_IDX];
	}
	CCS_CATCH_ALL	
}

void CoverageDiagramViewer::MakeBshBarData(COV_BARDATA *prlBar, BASICSHIFTS *prpBsh)
{
	CCS_TRY
	prlBar->Urno = prpBsh->Urno;
	prlBar->RUrno = prpBsh->RUrno;
	prlBar->Type = BASIC_SHIFT;
    prlBar->Text = prpBsh->Code + "/" + prpBsh->Fctc;
	prlBar->StatusText = prpBsh->From.Format("%H:%M - ") + prpBsh->To.Format("%H:%M   ") + prpBsh->Code;
    prlBar->StartTime = prpBsh->From;
    prlBar->EndTime = prpBsh->To;
	prlBar->FrameType = FRAMENONE;
	prlBar->MarkerType = MARKFULL;
	BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(prpBsh->RUrno);

	prlBar->MarkerBrush = ogBrushs[LIME_IDX];
	prlBar->TextColor = ogColors[BLACK_IDX];

	
	CCS_CATCH_ALL	
}

void CoverageDiagramViewer::MakeSdtBar(int ipGroupno, int ipLineno, SDTDATA *prpSdt, int ipCount)
{
	CCS_TRY
	COV_BARDATA rlBar;
	MakeSdtBarData(&rlBar,prpSdt, ipCount);
	int ilBarno = CreateBar(ipGroupno, ipLineno, &rlBar);
	COV_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);

	//MWO ???
	//int ilBarno=0;
	//prlLine->Bars.NewAt(ilBarno, rlBar);
	// We also have to create an indicator for showing the range of time on TimeScale
	COV_INDICATORDATA rlIndicator;
	rlIndicator.StartTime = rlBar.StartTime;
	rlIndicator.EndTime = rlBar.EndTime;
	rlIndicator.Color = ogColors[YELLOW_IDX];
	CreateIndicator(ipGroupno, ipLineno, ilBarno, &rlIndicator);

	COV_DELEGATIONDATA rlDelegation;
	CCSPtrArray <RecordSet> olDelList;
	CString olBsdUrno;
	CTime olDate = prpSdt->Esbg;
	olBsdUrno.Format("%ld",prpSdt->Bsdu);
	ogBCD.GetRecords("DEL", "BSDU", olBsdUrno, &olDelList );
	int ilStartIdx = ogBCD.GetFieldIndex("DEL", "DELF");
	int ilEndIdx = ogBCD.GetFieldIndex("DEL", "DELT");
	int ilFctcIdx = ogBCD.GetFieldIndex("DEL", "FCTC");
	for ( int illc = 0; illc < olDelList.GetSize(); illc++)
	{
		CString olStartTime = olDelList[illc].Values[ilStartIdx];
		CString olEndTime = olDelList[illc].Values[ilEndIdx];
		CString olFctc = olDelList[illc].Values[ilFctcIdx];
					
		CTime olTime = HourStringToDate(olStartTime, olDate);
		if(olTime < olDate)
		{
			olDate = prpSdt->Seni;
			olTime = HourStringToDate(olStartTime, olDate);
		}
		rlDelegation.StartTime = olTime;
		olTime = HourStringToDate(olEndTime, olDate);
		rlDelegation.EndTime = olTime;
		rlDelegation.Text = olFctc;
		rlDelegation.Color = ogColors[SILVER_IDX];
		CreateDelegation(ipGroupno, ipLineno, ilBarno, &rlDelegation);

	}
	olDelList.DeleteAll();
	

	if(bmShiftRoster && bmIsRealRoster)
	{
		COV_DELEGATIONDATA rlDelegation2;
		COV_DELEGATIONDATA rlDelegation3;

		CCSPtrArray <SDTDATA> olSdtList;
		
		ogSdtRosterData.GetSdtsSdguAndType(prpSdt->Urno, 'D',olSdtList);
	
		for(int ilDrd = 0; ilDrd < olSdtList.GetSize(); ilDrd++)
		{
			rlDelegation2.StartTime = olSdtList[ilDrd].Esbg;
			rlDelegation2.EndTime = olSdtList[ilDrd].Lsen;
			rlDelegation2.Text = olSdtList[ilDrd].Fctc;

		
			rlDelegation2.Color = ogColors[BLUE_IDX];
			CreateDelegation2(ipGroupno, ipLineno, ilBarno, &rlDelegation2);		
		}

		olSdtList.RemoveAll();
		ogSdtRosterData.GetSdtsSdguAndType(prpSdt->Urno, 'A',olSdtList);
	
		for(int ilDra = 0; ilDra < olSdtList.GetSize(); ilDra++)
		{
			rlDelegation3.StartTime = olSdtList[ilDra].Esbg;
			rlDelegation3.EndTime = olSdtList[ilDra].Lsen;
			rlDelegation3.Text = olSdtList[ilDra].Fctc;
			rlDelegation3.Color = ogColors[RED_IDX];
			CreateDelegation3(ipGroupno, ipLineno, ilBarno, &rlDelegation3);		
		}

		olSdtList.RemoveAll();
	}

	CCS_CATCH_ALL	
}

void CoverageDiagramViewer::MakeBshBar(int ipGroupno, int ipLineno, BASICSHIFTS *prpBsh)
{
	CCS_TRY
	COV_BARDATA rlBar;
	MakeBshBarData(&rlBar,prpBsh);
	int ilBarno = CreateBar(ipGroupno, ipLineno, &rlBar);
	// We also have to create an indicator for showing the range of time on TimeScale
	COV_INDICATORDATA rlIndicator;
	rlIndicator.StartTime = rlBar.StartTime;
	rlIndicator.EndTime = rlBar.EndTime;
	rlIndicator.Color = ogColors[YELLOW_IDX];
	CreateIndicator(ipGroupno, ipLineno, ilBarno, &rlIndicator);

	COV_DELEGATIONDATA rlDelegation;
	CCSPtrArray <RecordSet> olDelList;
	CString olBsdUrno;
	CTime olDate = prpBsh->From;
	olBsdUrno.Format("%ld",prpBsh->RUrno);
	ogBCD.GetRecords("DEL", "BSDU", olBsdUrno, &olDelList );
	int ilStartIdx = ogBCD.GetFieldIndex("DEL", "DELF");
	int ilEndIdx = ogBCD.GetFieldIndex("DEL", "DELT");
	int ilFctcIdx = ogBCD.GetFieldIndex("DEL", "FCTC");
	for ( int illc = 0; illc < olDelList.GetSize(); illc++)
	{
		CString olStartTime = olDelList[illc].Values[ilStartIdx];
		CString olEndTime = olDelList[illc].Values[ilEndIdx];
		CString olFctc = olDelList[illc].Values[ilFctcIdx];
					
		CTime olTime = HourStringToDate(olStartTime, olDate);
		if(olTime < olDate)
		{
			olDate = prpBsh->To;
			olTime = HourStringToDate(olStartTime, olDate);
		}
		rlDelegation.StartTime = olTime;
		olTime = HourStringToDate(olEndTime, olDate);
		rlDelegation.EndTime = olTime;
		rlDelegation.Text = olFctc;
		rlDelegation.Color = ogColors[SILVER_IDX];
		CreateDelegation(ipGroupno, ipLineno, ilBarno, &rlDelegation);
	}
	olDelList.DeleteAll();
	CCS_CATCH_ALL	
}

CString CoverageDiagramViewer::GroupText(int ipGroupNo)
{
	CCS_TRY
	if(ipGroupNo < omGroups.GetSize()-1)
	{
		return omGroups[ipGroupNo].GroupName;
	}
	CCS_CATCH_ALL	
	return "";	// error, just simply return an empty string
}

CString CoverageDiagramViewer::LineBshText(BASICSHIFTS *prpBsh)
{
	char buf[512];
	CCS_TRY
	sprintf(buf, "%s", prpBsh->Code);
	CCS_CATCH_ALL	
	return CString(buf);
}

CString CoverageDiagramViewer::BarBshText(BASICSHIFTS *prpBsh)
{
	CString olBarText; 
	return olBarText;
}

CString CoverageDiagramViewer::StatusBshText(BASICSHIFTS *prpBsh)
{
	CString olStatusText;
	return olStatusText;
}


bool CoverageDiagramViewer::FindSdtLine(int ipGroupNo, int &ripLineno, long lpUrno)
{
	CCS_TRY
		return GetGroup(ipGroupNo)->LinesMap.Lookup((void *)lpUrno,(void *&)ripLineno) == TRUE;
	CCS_CATCH_ALL
}

bool CoverageDiagramViewer::FindSdtBarWithCode(int ipGroupNo,SDTDATA *prpSdt,int &ripLineno)
{
	bool blFound = false;
	CCS_TRY
	ripLineno = -1;
	int ilLineno = -1;
	int ilLineCount = GetLineCount(ipGroupNo);

	for(ilLineno = 0; ilLineno < ilLineCount/*GetLineCount(ipGroupNo)*/; ilLineno++)
	{
		COV_LINEDATA *prlLine = GetLine(ipGroupNo,ilLineno);
		if(prlLine==NULL)
			continue;
		int ilBarno = GetBarnoFromTime(ipGroupNo, ilLineno, prpSdt->Esbg,prpSdt->Lsen,-1,prlLine->MaxOverlapLevel);
		if(ilBarno>=0)
		{
			COV_BARDATA *prlBar = GetBar(ipGroupNo, ilLineno, ilBarno);
			if(prlBar!=NULL)
			{
				CString olFctc;
				int ilPos = -1;
				if(bmShiftRoster)
				{
					olFctc = CString(prpSdt->Fctc) + CString(prpSdt->Enames);
				}
				else
				{
					olFctc = CString(prpSdt->Fctc);
				}
				CString olSdtText = CString(prpSdt->Bsdc) + CString("  ") + olFctc;
				CString olStr = "";
				if(!bmMaxView)
				{
					olStr.Format(" [%d]",prlBar->Brutto);
				}
				
				if(prlBar->Text.Compare(olSdtText+olStr) == 0)
				{
					ripLineno = ilLineno;
					blFound = true;
					break;
				}
			}
		}
	}
	return blFound;
CCS_CATCH_ALL
return false;
}

bool CoverageDiagramViewer::FindBshLinesForBsd(int ipGroupNo, CUIntArray &ropLines, CUIntArray &ropBars, long lpUrno)
{
	bool blFound = false;
	CCS_TRY
	int ilLineCount = GetLineCount(ipGroupNo);
	for(int i = 0; i < ilLineCount/*GetLineCount(ipGroupNo)*/; i++)
	{
		for (int j = 0; j < GetBarCount(ipGroupNo, i); j++)
		{
			if (GetBar(ipGroupNo, i, j)->RUrno == lpUrno)
			{
				ropLines.Add(i);
				ropBars.Add(j);
				blFound = true;
			}
		}
	}
	CCS_CATCH_ALL
	return blFound;
}

bool CoverageDiagramViewer::FindBshLine(int ipGroupNo, int &ripLineno, long lpUrno)
{
	bool blFound = false;
	CCS_TRY
	int ilLineCount = GetLineCount(ipGroupNo);
	for(ripLineno = 0; ripLineno < ilLineCount/*GetLineCount(ipGroupNo)*/; ripLineno++)
	{
		if (GetLine(ipGroupNo, ripLineno)->Urno == lpUrno)
		{
			blFound = true;
			break;
		}
	}
	CCS_CATCH_ALL
	return blFound;
}

bool CoverageDiagramViewer::FindBarGlobal(long lpUrno, int &ripGroupno, 
								 int &ripLineno, int &ripBarno)
{
	bool blFound = false;
	CCS_TRY
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
	{
		for (ripLineno = 0; ripLineno < GetLineCount(ripGroupno); ripLineno++)
		{
			for (ripBarno = 0; ripBarno < GetBarCount(ripGroupno, ripLineno); ripBarno++)
			{
				if (GetBar(ripGroupno, ripLineno, ripBarno)->Urno == lpUrno)
				{
					blFound = true;
				}
			}
		}
	}

	CCS_CATCH_ALL
	return blFound;
}

BOOL CoverageDiagramViewer::FindBar(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBarno)
{
	CCS_TRY
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
		for (ripLineno = 0; ripLineno < GetLineCount(ripGroupno); ripLineno++)
			for (ripBarno = 0; ripBarno < GetBarCount(ripGroupno, ripLineno); ripBarno++)
				if (GetBar(ripGroupno, ripLineno, ripBarno)->Urno == lpUrno)
					return TRUE;

	CCS_CATCH_ALL
	return FALSE;
}


BOOL CoverageDiagramViewer::FindDutyBar(long lpUrno, int &ripGroupno, int &ripLineno)
{
	CCS_TRY
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
		for (ripLineno = 0; ripLineno < GetLineCount(ripGroupno); ripLineno++)
			if (GetLine(ripGroupno, ripLineno)->Urno == lpUrno)
				return TRUE;

	CCS_CATCH_ALL
	return FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// CoverageDiagramViewer -- Viewer basic operations
//
int CoverageDiagramViewer::GetGroupCount()
{
	CCS_TRY
    return omGroups.GetSize();
	CCS_CATCH_ALL
	return 0;
}

COV_GROUPDATA *CoverageDiagramViewer::GetGroup(int ipGroupno)
{
	CCS_TRY
    return &omGroups[ipGroupno];
	CCS_CATCH_ALL
	return NULL;
}

CString CoverageDiagramViewer::GetGroupText(int ipGroupno)
{
	CCS_TRY
	if(omGroups.GetSize() == 0)
	{
		return CString("");
	}
    return omGroups[ipGroupno].GroupName;
	CCS_CATCH_ALL
	return CString("");
}

CString CoverageDiagramViewer::GetGroupTopScaleText(int ipGroupno)
{
	CString s;
	return s;
}


int CoverageDiagramViewer::GetGroupColorIndex(int ipGroupno,CTime opStart,CTime opEnd)
{
	int ilColorIndex = 0;//FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2);
/*	CBrush *prlBrush = ogBrushs[ilColorIndex];
	int ilLineCount = GetLineCount(ipGroupno);
	for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
	{
		for (int ilBarno = 0; ilBarno < GetBarCount(ipGroupno, ilLineno); ilBarno++)
		{
			COV_BARDATA *prlBar;
			if ((prlBar = GetBar(ipGroupno, ilLineno, ilBarno)) != NULL)
			{
				if (IsOverlapped(opStart, opEnd, prlBar->StartTime, prlBar->EndTime))
				{
					GHDDATA *prlGhd = ogJobs.GetJobByUrno(prlBar->JobUrno);
					if (prlGhd != NULL)
					{
						if ((prlGhd->ConflictType !=  CFI_NOCONFLICT) && (!prlGhd->ConflictConfirmed))
						{
							return FIRSTCONFLICTCOLOR+(CFI_NOTCOVERED*2);
						}
					}
				}
			}
		}
	}
*/
	return ilColorIndex;
}

int CoverageDiagramViewer::GetNextFreeSdtLine()
{
	CCS_TRY
	int ilLineCount = GetLineCount(SHIFT_DEMAND);
	for(int i = 0; i < ilLineCount/*GetLineCount(ipGroupNo)*/; i++)
	{
		if (GetLine(SHIFT_DEMAND, i)->Urno == -1)
		{
			return i;
		}
	}
	CCS_CATCH_ALL
	return -1;
}

int CoverageDiagramViewer::GetLineCount(int ipGroupno)
{
	CCS_TRY
	if(omGroups.GetSize() == 0)
	{
		return 0;
	}
    return omGroups[ipGroupno].Lines.GetSize();
	CCS_CATCH_ALL
	return 0;
}

COV_LINEDATA *CoverageDiagramViewer::GetLine(int ipGroupno, int ipLineno)
{
	CCS_TRY
	if(omGroups.GetSize() == 0)
	{
		return NULL;
	}
	if(ipLineno < omGroups[ipGroupno].Lines.GetSize())
	{
		return &omGroups[ipGroupno].Lines[ipLineno];
	}
	CCS_CATCH_ALL
	return NULL;
}

CString CoverageDiagramViewer::GetLineText(int ipGroupno, int ipLineno)
{
	CCS_TRY
	//return omGroups[ipGroupno].Lines[ipLineno].DsrName;
    return omGroups[ipGroupno].Lines[ipLineno].Text;
	CCS_CATCH_ALL
	return CString("");
}

int CoverageDiagramViewer::GetMaxOverlapLevel(int ipGroupno, int ipLineno)
{
	CCS_TRY
    return omGroups[ipGroupno].Lines[ipLineno].MaxOverlapLevel;
	CCS_CATCH_ALL
	return 0;
}

int CoverageDiagramViewer::GetVisualMaxOverlapLevel(int ipGroupno, int ipLineno)
{
	CCS_TRY
	// For max level 0,1,2 we will use 2 levels, 3,4,5 we will use 5 levels, 6,7,8 we use 8 levels, and so on ...
	return GetMaxOverlapLevel(ipGroupno, ipLineno);//MWO / 3 * 3 + 2;
	CCS_CATCH_ALL
	return 0;
}

int CoverageDiagramViewer::GetBkBarCount(int ipGroupno, int ipLineno)
{
	CCS_TRY
    return omGroups[ipGroupno].Lines[ipLineno].BkBars.GetSize();
	CCS_CATCH_ALL
	return 0;
}

COV_BKBARDATA *CoverageDiagramViewer::GetBkBar(int ipGroupno, int ipLineno, int ipBkBarno)
{
	CCS_TRY
	omGroups[ipGroupno].Lines.Sort(CompareCovLine);
    return &omGroups[ipGroupno].Lines[ipLineno].BkBars[ipBkBarno];
	CCS_CATCH_ALL
	return NULL;
}

CString CoverageDiagramViewer::GetBkBarText(int ipGroupno, int ipLineno, int ipBkBarno)
{
	CCS_TRY
    return omGroups[ipGroupno].Lines[ipLineno].BkBars[ipBkBarno].Text;
	CCS_CATCH_ALL
	return CString("");
}

int CoverageDiagramViewer::GetBarCount(int ipGroupno, int ipLineno)
{
	CCS_TRY
	if(omGroups[ipGroupno].Lines.GetSize() > ipLineno)
		return omGroups[ipGroupno].Lines[ipLineno].Bars.GetSize();
	CCS_CATCH_ALL
	return 0;
}

COV_BARDATA *CoverageDiagramViewer::GetBar(int ipGroupno, int ipLineno, int ipBarno)
{
	CCS_TRY
    return &omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno];
	CCS_CATCH_ALL
	return NULL;
}

CString CoverageDiagramViewer::GetBarText(int ipGroupno, int ipLineno, int ipBarno)
{
	CCS_TRY
	    return omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Text;
	CCS_CATCH_ALL
		return CString("");


}

CString CoverageDiagramViewer::GetStatusBarText(int ipGroupno, int ipLineno, int ipBarno)
{
	CCS_TRY
    return omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].StatusText;
	CCS_CATCH_ALL
	return CString("");
}

int CoverageDiagramViewer::GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno)
{
	CCS_TRY
    return omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.GetSize();
	CCS_CATCH_ALL
	return 0;
}

int CoverageDiagramViewer::GetDelegationCount(int ipGroupno, int ipLineno, int ipBarno)
{
	CCS_TRY
    return omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Delegations.GetSize();
	CCS_CATCH_ALL
	return 0;
}

int CoverageDiagramViewer::GetDelegation2Count(int ipGroupno, int ipLineno, int ipBarno)
{
	CCS_TRY
    return omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Delegations2.GetSize();
	CCS_CATCH_ALL
	return 0;
}

int CoverageDiagramViewer::GetDelegation3Count(int ipGroupno, int ipLineno, int ipBarno)
{
	CCS_TRY
    return omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Delegations3.GetSize();
	CCS_CATCH_ALL
	return 0;
}

COV_INDICATORDATA *CoverageDiagramViewer::GetIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno)
{
	CCS_TRY
    return &omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators[ipIndicatorno];
	CCS_CATCH_ALL
	return NULL;
}

COV_DELEGATIONDATA *CoverageDiagramViewer::GetDelegation(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno)
{
	CCS_TRY
    return &omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Delegations[ipIndicatorno];
	CCS_CATCH_ALL
	return NULL;
}

COV_DELEGATIONDATA *CoverageDiagramViewer::GetDelegation2(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno)
{
	CCS_TRY
    return &omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Delegations2[ipIndicatorno];
	CCS_CATCH_ALL
	return NULL;
}

COV_DELEGATIONDATA *CoverageDiagramViewer::GetDelegation3(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno)
{
	CCS_TRY
    return &omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Delegations3[ipIndicatorno];
	CCS_CATCH_ALL
	return NULL;
}


/////////////////////////////////////////////////////////////////////////////
// CoverageDiagramViewer - COV_BARDATA array maintenance (overlapping version)
//
void CoverageDiagramViewer::DeleteAll()
{
	CCS_TRY
    while (GetGroupCount() > 0)
        DeleteGroup(0);
	CCS_CATCH_ALL
}

int CoverageDiagramViewer::CreateGroup(COV_GROUPDATA *prpGroup)
{
	CCS_TRY
    int ilGroupCount = omGroups.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
		if (CompareGroup(prpGroup, GetGroup(ilGroupno)) <= 0)
            break;  // should be inserted before Groups[ilGroupno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilGroupno = ilGroupCount; ilGroupno > 0; ilGroupno--)
		if (CompareGroup(prpGroup, GetGroup(ilGroupno-1)) >= 0)
            break;  // should be inserted after Groups[ilGroupno-1]
#endif
    
    omGroups.NewAt(ilGroupno, *prpGroup);
	return ilGroupno;
	CCS_CATCH_ALL
	return 0;
}

void CoverageDiagramViewer::DeleteGroup(int ipGroupno)
{
	CCS_TRY
    while (GetLineCount(ipGroupno) > 0)
	{
        DeleteLine(ipGroupno, 0);
	}
    omGroups.DeleteAt(ipGroupno);
	CCS_CATCH_ALL
}


void CoverageDiagramViewer::DeleteGroupLines()
{
	CCS_TRY
	for( int ilGroupno = 0;ilGroupno < GetGroupCount()-1; ilGroupno++)
    while (GetLineCount(ilGroupno) > 0)
	{
        DeleteLine(ilGroupno, 0);
	}
	CCS_CATCH_ALL
}

void CoverageDiagramViewer::DeleteGroupLines(int ipGroupno)
{
	CCS_TRY
    while (GetLineCount(ipGroupno) > 0)
	{
        DeleteLine(ipGroupno, 0);
	}
	CCS_CATCH_ALL
}

int CoverageDiagramViewer::CreateSdtLine(int ipGroupno, COV_LINEDATA *prpLine)
{
	CCS_TRY
    int ilLineCount = omGroups[ipGroupno].Lines.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
		if (CompareSdtLine(prpLine, GetLine(ipGroupno, ilLineno)) <= 0)
            break;  // should be inserted before Lines[ilLineno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
		if (CompareSdtLine(prpLine, GetLine(ipGroupno, ilLineno-1)) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]
#endif

    omGroups[ipGroupno].Lines.NewAt(ilLineno, *prpLine);
    COV_LINEDATA *prlLine = &omGroups[ipGroupno].Lines[ilLineno];
    prlLine->MaxOverlapLevel = 0;   // there is no bar in this line right now
	omGroups[ipGroupno].LinesMap.SetAt((void *)prlLine->Urno,(void *)ilLineno);
    return ilLineno;
	CCS_CATCH_ALL
	return -1;
}

int CoverageDiagramViewer::CreateBshLine(int ipGroupno, COV_LINEDATA *prpLine)
{
	CCS_TRY
    int ilLineCount = omGroups[ipGroupno].Lines.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
		if (CompareBshLine(prpLine, GetLine(ipGroupno, ilLineno)) <= 0)
            break;  // should be inserted before Lines[ilLineno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
		if (CompareBshLine(prpLine, GetLine(ipGroupno, ilLineno-1)) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]
#endif

    omGroups[ipGroupno].Lines.NewAt(ilLineno, *prpLine);
    COV_LINEDATA *prlLine = &omGroups[ipGroupno].Lines[ilLineno];
    prlLine->MaxOverlapLevel = 0;   // there is no bar in this line right now
    return ilLineno;
	CCS_CATCH_ALL
	return 0;
}

void CoverageDiagramViewer::DeleteLine(int ipGroupno, int ipLineno)
{
	CCS_TRY
    while (GetBkBarCount(ipGroupno, ipLineno) > 0)
        DeleteBkBar(ipGroupno, ipLineno, 0);
	// Id 14-Sep-1996
	// Don't simply call DeleteBar() for sake of efficiency
	COV_LINEDATA *prlLine = &omGroups[ipGroupno].Lines[ipLineno];
	while (prlLine->Bars.GetSize() > 0)
	{
		prlLine->Bars[0].Indicators.DeleteAll();	// delete associated indicators first
		prlLine->Bars[0].Delegations.DeleteAll();	// delete associated indicators first
		prlLine->Bars[0].Delegations2.DeleteAll();	// delete associated indicators first
		prlLine->Bars[0].Delegations3.DeleteAll();	// delete associated indicators first
		prlLine->Bars.DeleteAt(0);
	}

    omGroups[ipGroupno].Lines.DeleteAt(ipLineno);
	CCS_CATCH_ALL
}

int CoverageDiagramViewer::CreateBkBar(int ipGroupno, int ipLineno, COV_BKBARDATA *prpBkBar)
{
	CCS_TRY
    int ilBkBarCount = omGroups[ipGroupno].Lines[ipLineno].BkBars.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
// (2.89 seconds vs. 4.21 seconds) for 3000 bars creation.
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilBkBarno = 0; ilBkBarno < ilBarCount; ilBkBarno++)
        if (prpBar->StartTime <= GetBkBar(ipGroupno,ipLineno,ilBkBarno)->StartTime)
            break;  // should be inserted before Bars[ilBkBarno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilBkBarno = ilBkBarCount; ilBkBarno > 0; ilBkBarno--)
        if (prpBkBar->StartTime >= GetBkBar(ipGroupno,ipLineno,ilBkBarno-1)->StartTime)
            break;  // should be inserted after Bars[ilBkBarno-1]
#endif

    omGroups[ipGroupno].Lines[ipLineno].BkBars.NewAt(ilBkBarno, *prpBkBar);
    return ilBkBarno;
	CCS_CATCH_ALL
	return 0;
}

void CoverageDiagramViewer::DeleteBkBar(int ipGroupno, int ipLineno, int ipBkBarno)
{
	CCS_TRY
    omGroups[ipGroupno].Lines[ipLineno].BkBars.DeleteAt(ipBkBarno);
	CCS_CATCH_ALL
}

int CoverageDiagramViewer::CreateBar(int ipGroupno, int ipLineno, COV_BARDATA *prpBar, BOOL bpTopBar)
{
	CCS_TRY
    COV_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);
    int ilBarCount = prlLine->Bars.GetSize();

	// Id 14-Sep-1996
	// I use a new algorithm for bar insertion and new deletion.
	// This new algorithm will work somewhat slower than the previous version, but safer.
	// To give you a brief idea of this algorithm, here is the essential of insertion:
	//
	// Case A: Insert a new bar to be the topmost bar
	//		We will scan every bars, and for each bar which is in the same overlap group
	//		with the new bar, we will shift it downward one level, and update the field
	//		Line.MaxOverlapLevel. This new bar will be the last bar in painting order
	//		(for being topmost), and has overlapping level = 0.
	//
	// Case B: Insert a new bar to be the bottommost bar
	//		We will scan every bars for finding the maximum overlap level of bars which
	//		are in the same overlap group with this new bar (use level -1 if not found).
	//		Then use this maximum overlap level + 1 as overlap level of this new bar.
	//		This new bar will be the first bar in painting order (for being bottommost).
	//
	CUIntArray olBarnoList;
	GetOverlappedBarsFromTime(ipGroupno, ipLineno,
		prpBar->StartTime, prpBar->EndTime, olBarnoList);
	int ilListCount = olBarnoList.GetSize();

	if (bpTopBar)
	{
		for (int ilLc = 0; ilLc < ilListCount; ilLc++)
		{
			int ilBarno = olBarnoList[ilLc];
			COV_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			int ilNewOverlapLevel = ++prlBar->OverlapLevel;
			prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, ilNewOverlapLevel);
		}
		prlLine->Bars.NewAt(ilBarCount, *prpBar);	// to be the last bar in painting order
		prlLine->Bars[ilBarCount].OverlapLevel = 0;
		return ilBarCount;
	}
	else
	{
		int ilNewBarLevel = -1;
		for (int ilLc = 0; ilLc < ilListCount; ilLc++)
		{
			int ilBarno = olBarnoList[ilLc];
			COV_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			ilNewBarLevel = max(ilNewBarLevel, prlBar->OverlapLevel + 1);
				// the +1 means that we have to insert the new bar below this bar
		}
		prlLine->Bars.NewAt(0, *prpBar);	// to be the first bar in painting order
		prlLine->Bars[0].OverlapLevel = ilNewBarLevel;
		prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, ilNewBarLevel);
		return 0;
	}
	CCS_CATCH_ALL
	return 0;
}

void CoverageDiagramViewer::DeleteBar(int ipGroupno, int ipLineno, int ipBarno)
{
	CCS_TRY
	// Delete all indicators of this bar
	while (GetIndicatorCount(ipGroupno, ipLineno, ipBarno) > 0)
		DeleteIndicator(ipGroupno, ipLineno, ipBarno, 0);
	while (GetDelegationCount(ipGroupno, ipLineno, ipBarno) > 0)
		DeleteDelegation(ipGroupno, ipLineno, ipBarno, 0);
	while (GetDelegation2Count(ipGroupno, ipLineno, ipBarno) > 0)
		DeleteDelegation2(ipGroupno, ipLineno, ipBarno, 0);
	while (GetDelegation3Count(ipGroupno, ipLineno, ipBarno) > 0)
		DeleteDelegation3(ipGroupno, ipLineno, ipBarno, 0);

    COV_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);
	COV_BARDATA *prlBar = &prlLine->Bars[ipBarno];

	// Id 14-Sep-1996
	// I use a new algorithm for bar insertion and new deletion.
	// This new algorithm will work somewhat slower than the previous version, but safer.
	// To give you a brief idea of this algorithm, here is the essential of deletion:
	//
	// We will scan every bars, and for each bar which is in the same overlap group with
	// the new bar, we will back up them to a temporary array. Then we remove every bars
	// in this overlapping group, and insert these bar from the temporary array except
	// the one which has to be deleted back to the viewer.
	//
	// In some situation, if this overlapping group controls the maximum overlap level of
	// the line, we may have to rescan every bars in this line to make sure that the
	// maximum overlap level still be correct.
	//
	CUIntArray olBarnoList;
	GetOverlappedBarsFromTime(ipGroupno, ipLineno,
		prlBar->StartTime, prlBar->EndTime, olBarnoList);
	int ilListCount = olBarnoList.GetSize();

	// Save the bar in this overlapping group to a temporary array
	CCSPtrArray<COV_BARDATA> olSavedBars;
	BOOL blMustRecomputeMaxOverlapLevel = FALSE;
	for (int ilLc = 0; ilLc < ilListCount; ilLc++)
	{
		int ilBarno = olBarnoList[ilLc];
		COV_BARDATA *prlBar = &prlLine->Bars[ilBarno];
		blMustRecomputeMaxOverlapLevel |= (prlBar->OverlapLevel == prlLine->MaxOverlapLevel);
			// check if deletion of this bar may change the line height
		if (ilBarno != ipBarno)	// must we save this bar?
			olSavedBars.NewAt(olSavedBars.GetSize(), prlLine->Bars[ilBarno]);
	}
	// Delete all bars (which are already backed up in a temporary array) from the viewer
	while (ilListCount-- > 0)
	{
		int ilBarno = olBarnoList[ilListCount];
		prlLine->Bars.DeleteAt(ilBarno);
	}
	// Then, we insert those bars back to the Gantt chart, except the specified one
	int ilSavedBarCount = olSavedBars.GetSize();
	for (int ilSavedBarno = 0; ilSavedBarno < ilSavedBarCount; ilSavedBarno++)
	{
		CreateBar(ipGroupno, ipLineno, &olSavedBars[ilSavedBarno]);
	}
	olSavedBars.DeleteAll();
	// Then recompute the MaxOverlapLevel if it's necessary
	if (blMustRecomputeMaxOverlapLevel)
	{
		prlLine->MaxOverlapLevel = 0;
	    int ilBarCount = prlLine->Bars.GetSize();
		for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
		{
			COV_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, prlBar->OverlapLevel);
		}
	}
	CCS_CATCH_ALL
}

int CoverageDiagramViewer::CreateIndicator(int ipGroupno, int ipLineno, int ipBarno, COV_INDICATORDATA *prpIndicator)
{
	CCS_TRY
/*    int ilIndicatorCount = omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.GetSize();

    // Search for the position which we want to insert this new bar
    for (int ilIndicatorno = ilIndicatorCount; ilIndicatorno > 0; ilIndicatorno--)
        if (prpIndicator->StartTime >= GetIndicator(ipGroupno,ipLineno,ipBarno,ilIndicatorno-1)->StartTime)
            break;  // should be inserted before Indicators[ilBarno]

    omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.
		NewAt(ilIndicatorno, *prpIndicator);
*/
//MWO ???
	int ilIndicatorCount = 0;
	COV_INDICATORDATA *prlIndicator = new COV_INDICATORDATA;
	*prlIndicator = *prpIndicator;
    omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.Add(prlIndicator);
    return 0;
	CCS_CATCH_ALL
	return 0;
}

int CoverageDiagramViewer::CreateDelegation(int ipGroupno, int ipLineno, int ipBarno, COV_DELEGATIONDATA *prpIndicator)
{
	CCS_TRY
    int ilIndicatorCount = omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Delegations.GetSize();

#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilIndicatorno = 0; ilIndicatorno < ilIndicatorCount; ilIndicatorno++)
        if (prpIndicator->StartTime <= GetDelegation(ipGroupno,ipLineno,ipBarno,ilIndicatorno)->StartTime)
            break;  // should be inserted before Indicators[ilBarno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilIndicatorno = ilIndicatorCount; ilIndicatorno > 0; ilIndicatorno--)
        if (prpIndicator->StartTime >= GetDelegation(ipGroupno,ipLineno,ipBarno,ilIndicatorno-1)->StartTime)
            break;  // should be inserted before Indicators[ilBarno]
#endif

    omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Delegations.
		NewAt(ilIndicatorno, *prpIndicator);
    return ilIndicatorno;
	CCS_CATCH_ALL
	return 0;
}

int CoverageDiagramViewer::CreateDelegation2(int ipGroupno, int ipLineno, int ipBarno, COV_DELEGATIONDATA *prpIndicator)
{
	CCS_TRY
    int ilIndicatorCount = omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Delegations2.GetSize();

#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilIndicatorno = 0; ilIndicatorno < ilIndicatorCount; ilIndicatorno++)
        if (prpIndicator->StartTime <= GetDelegation2(ipGroupno,ipLineno,ipBarno,ilIndicatorno)->StartTime)
            break;  // should be inserted before Indicators[ilBarno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilIndicatorno = ilIndicatorCount; ilIndicatorno > 0; ilIndicatorno--)
        if (prpIndicator->StartTime >= GetDelegation2(ipGroupno,ipLineno,ipBarno,ilIndicatorno-1)->StartTime)
            break;  // should be inserted before Indicators[ilBarno]
#endif

    omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Delegations2.
		NewAt(ilIndicatorno, *prpIndicator);
    return ilIndicatorno;
	CCS_CATCH_ALL
	return 0;
}

int CoverageDiagramViewer::CreateDelegation3(int ipGroupno, int ipLineno, int ipBarno, COV_DELEGATIONDATA *prpIndicator)
{
	CCS_TRY
    int ilIndicatorCount = omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Delegations3.GetSize();

#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilIndicatorno = 0; ilIndicatorno < ilIndicatorCount; ilIndicatorno++)
        if (prpIndicator->StartTime <= GetDelegation3(ipGroupno,ipLineno,ipBarno,ilIndicatorno)->StartTime)
            break;  // should be inserted before Indicators[ilBarno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilIndicatorno = ilIndicatorCount; ilIndicatorno > 0; ilIndicatorno--)
        if (prpIndicator->StartTime >= GetDelegation3(ipGroupno,ipLineno,ipBarno,ilIndicatorno-1)->StartTime)
            break;  // should be inserted before Indicators[ilBarno]
#endif

    omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Delegations3.
		NewAt(ilIndicatorno, *prpIndicator);
    return ilIndicatorno;
	CCS_CATCH_ALL
	return 0;
}


void CoverageDiagramViewer::DeleteIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno)
{
	CCS_TRY
    omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.DeleteAt(ipIndicatorno);
	CCS_CATCH_ALL
}

void CoverageDiagramViewer::DeleteDelegation(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno)
{
	CCS_TRY
    omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Delegations.DeleteAt(ipIndicatorno);
	CCS_CATCH_ALL
}

void CoverageDiagramViewer::DeleteDelegation2(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno)
{
	CCS_TRY
    omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Delegations2.DeleteAt(ipIndicatorno);
	CCS_CATCH_ALL
}

void CoverageDiagramViewer::DeleteDelegation3(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno)
{
	CCS_TRY
    omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Delegations3.DeleteAt(ipIndicatorno);
	CCS_CATCH_ALL
}


/////////////////////////////////////////////////////////////////////////////
// CoverageDiagramViewer - COV_BARDATA calculation routines

// Return the bar number of the list box based on the given period [time1, time2]
int CoverageDiagramViewer::GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2,int ipOverlapLevel1, int ipOverlapLevel2)
{
	CCS_TRY
    for (int i = GetBarCount(ipGroupno, ipLineno)-1; i >= 0; i--)
    {
        COV_BARDATA *prlBar = GetBar(ipGroupno, ipLineno, i);
        if (opTime1 <= prlBar->EndTime && prlBar->StartTime <= opTime2&&
            ipOverlapLevel1 <= prlBar->OverlapLevel && prlBar->OverlapLevel <= ipOverlapLevel2)
            return i;
    }
	CCS_CATCH_ALL
    return -1;
}

// Return the background bar number of the list box based on the given period [time1, time2]
int CoverageDiagramViewer::GetBkBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2)
{
	CCS_TRY
    for (int i = GetBkBarCount(ipGroupno, ipLineno)-1; i >= 0; i--)
    {
        COV_BKBARDATA *prlBkBar = GetBkBar(ipGroupno, ipLineno, i);
        if (opTime1 <= prlBkBar->EndTime && prlBkBar->StartTime <= opTime2)
            return i;
    }
	CCS_CATCH_ALL
    return -1;
}


/////////////////////////////////////////////////////////////////////////////
// CoverageDiagramViewer private helper methods

void CoverageDiagramViewer::GetOverlappedBarsFromTime(int ipGroupno, int ipLineno,
	CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList)
{
	CCS_TRY
    COV_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);
    int ilBarCount = prlLine->Bars.GetSize();

	// Prepare the empty array of the involvement flag for each bar
	CCSPtrArray<BOOL> IsInvolvedBar;
	for (int i = 0; i < ilBarCount; i++)
		IsInvolvedBar.NewAt(i, FALSE);

	// We will loop again and again until there is no more bar in this overlapping found.
	// The overlapping group is at first determine by the given "opTime1" and "opTime2".
	// But if the bar we find is exceed this period of time, we will extend searching
	// to both left side and right side in the next round.
	//
	int nBarsFoundThisRound = -1;
	while (nBarsFoundThisRound != 0)
	{
		nBarsFoundThisRound = 0;
		for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
		{
			CTime olStartTime = prlLine->Bars[ilBarno].StartTime;
			CTime olEndTime = prlLine->Bars[ilBarno].EndTime;
			BOOL blIsOverlapped = IsReallyOverlapped(olStartTime, olEndTime, opTime1, opTime2);
			BOOL blIsWithIn = IsWithIn(olStartTime, olEndTime, opTime1, opTime2);
				// this IsWithIn() fix the real thin bar bug
			BOOL blIsInvolved = (blIsOverlapped || blIsWithIn);

			if (!IsInvolvedBar[ilBarno] && blIsInvolved)	// some more new involved bars?
			{
				IsInvolvedBar[ilBarno] = TRUE;
				nBarsFoundThisRound++;
				opTime1 = min(opTime1, prlLine->Bars[ilBarno].StartTime);
				opTime2 = max(opTime2, prlLine->Bars[ilBarno].EndTime);
			}
		}
	}

	// Create the list of involved bars, then store them to "ropBarnoList"
	ropBarnoList.RemoveAll();
	for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
	{
		if (IsInvolvedBar[ilBarno])
		{
			ropBarnoList.Add(ilBarno);
		}
	}
	IsInvolvedBar.DeleteAll();
	CCS_CATCH_ALL
}

void CoverageDiagramViewer::SetDemandName(int ipGroup, CString opDemandName)
{
	CCS_TRY
	omGroups[ipGroup].DemandName = opDemandName;
	CCS_CATCH_ALL
}

CString CoverageDiagramViewer::GetCurrentDemandName(int ipGroup)
{
	CCS_TRY
	if(ipGroup < omGroups.GetSize())
	{
		return omGroups[ipGroup].DemandName;
	}
	CCS_CATCH_ALL
	return CString("");
}

void CoverageDiagramViewer::SetDemandLevel(int ipGroup, int ipDemandLevel)
{
	CCS_TRY
	omGroups[ipGroup].DemandLevel = ipDemandLevel;
	CCS_CATCH_ALL
}

void CoverageDiagramViewer::SetShiftView(int ipGroup, int ipShiftView)
{
	CCS_TRY
		if(ipGroup<omGroups.GetSize())
		{
			omGroups[ipGroup].ShiftView= ipShiftView;
		}
	CCS_CATCH_ALL
}

int CoverageDiagramViewer::GetShiftView(int ipGroup)
{
	CCS_TRY
	if(ipGroup < omGroups.GetSize())
	{
		int ilShiftView =  omGroups[ipGroup].ShiftView;
		return ilShiftView;
	}
	else
	{
		return -1;
	}
	CCS_CATCH_ALL
		return -1;
}

int CoverageDiagramViewer::GetCurrentDemandLevel(int ipGroup)
{
	CCS_TRY
	if(ipGroup < omGroups.GetSize())
	{
		return omGroups[ipGroup].DemandLevel;
	}
	else
	{
		return -1;
	}
	CCS_CATCH_ALL
		return -1;
}

SDGDATA *CoverageDiagramViewer::GetCurrentDemand(int ipGroup)
{
	CCS_TRY
	SDGDATA *prlSdg;

	if(ipGroup == SHIFT_DEMAND)
	{
		prlSdg = ogSdgData.GetSdgByUrno(lmSdgu);
		return prlSdg;
	}
	CCS_CATCH_ALL
	return NULL;
}



/////////////////////////////////////////////////////////////////////////////
// CoverageDiagramViewer Jobs creation methods

void CoverageDiagramViewer::AllowUpdates(BOOL bpNoUpdatesNow)
{
	CCS_TRY
	bmNoUpdatesNow = bpNoUpdatesNow;
	CCS_CATCH_ALL
}


static void CoverageDiagramCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	//CCS_TRY
    CoverageDiagramViewer *polViewer = (CoverageDiagramViewer *)popInstance;

	if (polViewer->NoDemUpdates() == false)
	{
	//Changes for employees
		if (ipDDXType == BSD_NEW)
			polViewer->ProcessBsdNew((BSDDATA *)vpDataPointer);
		else if (ipDDXType == BC_BSD_NEW)
			polViewer->ProcessBsdNew((BSDDATA *)vpDataPointer);
		else if (ipDDXType == BSD_CHANGE)
			polViewer->ProcessBsdChange((BSDDATA *)vpDataPointer);
		else if (ipDDXType == BSD_DELETE)
			polViewer->ProcessBsdDelete((BSDDATA *)vpDataPointer);
		else if (ipDDXType == MSD_DELETE)
			polViewer->ProcessMsdDelete((MSDDATA *)vpDataPointer,false);
		else if (ipDDXType == MSD_NEW)
			polViewer->ProcessMsdNew((MSDDATA *)vpDataPointer,false);
		else if (ipDDXType == MSD_CHANGE)
			polViewer->ProcessMsdUpdate((MSDDATA *)vpDataPointer,false);
		else if (ipDDXType == MSD_DELETE_SELF)
			polViewer->ProcessMsdDelete((MSDDATA *)vpDataPointer,true);
		else if (ipDDXType == MSD_NEW_SELF)
			polViewer->ProcessMsdNew((MSDDATA *)vpDataPointer,true);
		else if (ipDDXType == MSD_CHANGE_SELF)
			polViewer->ProcessMsdUpdate((MSDDATA *)vpDataPointer,true);
		
		if(polViewer->GetShiftRoster())
		{
 
			if (ipDDXType == DRR_NEW)
				polViewer->ProcessDrrNew((DRRDATA *)vpDataPointer);
			else if (ipDDXType == DRR_CHANGE)
				polViewer->ProcessDrrChange((DRRDATA *)vpDataPointer);
			else if (ipDDXType == DRR_DELETE)
				polViewer->ProcessDrrDelete((DRRDATA *)vpDataPointer);

			else if (ipDDXType == ENDDRR_RELEASE)
				polViewer->ProcessDrrRelEnd(vpDataPointer);
			else if (ipDDXType == BC_RELDRR)
				polViewer->ProcessDrrRelease(vpDataPointer);

			else if (ipDDXType == BC_RELPBO)
				polViewer->ProcessDrrReleasePbo(vpDataPointer);
			
			
			else if (ipDDXType == GPL_CHANGE || ipDDXType == GPL_NEW)
				polViewer->ProcessGplChange((RecordSet *)vpDataPointer);
			else if (ipDDXType == GPL_DELETE)
				polViewer->ProcessGplDelete((RecordSet *)vpDataPointer);

			
			else if (ipDDXType == GSP_CHANGE || ipDDXType == GSP_NEW)
				polViewer->ProcessGspChange((RecordSet *)vpDataPointer);
			else if (ipDDXType == GSP_DELETE)
				polViewer->ProcessGspDelete((RecordSet *)vpDataPointer);

			
			else if (ipDDXType == SPL_CHANGE || ipDDXType == SPL_NEW)
				polViewer->ProcessSplChange((RecordSet *)vpDataPointer);
			else if (ipDDXType == SPL_DELETE)
				polViewer->ProcessSplDelete((RecordSet *)vpDataPointer);

		}		

		if (ipDDXType == SDG_NEW)
			polViewer->ProcessSdgNew((SDGDATA *)vpDataPointer);
		else if (ipDDXType == SDG_CHANGE)
			polViewer->ProcessSdgChange((SDGDATA *)vpDataPointer);
		else if (ipDDXType == SDG_DELETE)
			polViewer->ProcessSdgDelete((SDGDATA *)vpDataPointer);

		else if (ipDDXType == SDT_NEW)
			polViewer->ProcessSdtChange((SDTDATA *)vpDataPointer);
		else if (ipDDXType == SDT_CHANGE)
			polViewer->ProcessSdtChange((SDTDATA *)vpDataPointer);
		else if (ipDDXType == SDT_SELCHANGE)
			polViewer->ProcessSdtSelChange((SDTDATA *)vpDataPointer);
		else if (ipDDXType == SDT_DELETE)
			polViewer->ProcessSdtDelete((SDTDATA *)vpDataPointer);
		else if (ipDDXType == RELSDG)
			polViewer->ProcessSdgRelease((SDGDATA *)vpDataPointer);

		else if (ipDDXType == DEM_CFL_READY )
			polViewer->ProcessDemCflReady(ipDDXType);

		else if (ipDDXType == RELDEM )
			polViewer->ProcessDemRelease(vpDataPointer);

		else if (ipDDXType == DEM_CHANGE )
			polViewer->ProcessDemChanged((DEMDATA*)vpDataPointer);
/*		else if (ipDDXType == DEM_NEW )
			polViewer->ProcessDemChanged((DEMDATA*)vpDataPointer);*/
		else if (ipDDXType == DEM_DELETE )
			polViewer->ProcessDemChanged((DEMDATA*)vpDataPointer);
		else if (ipDDXType == RELMSD)
			polViewer->ProcessMsdRelease((SDGDATA *)vpDataPointer);
		else if (ipDDXType == UPDDEM)
			polViewer->ProcessDemUpdDem(vpDataPointer);


	}
	//CCS_CATCH_ALL
}


void CoverageDiagramViewer::GetGroupsFromViewer(CCSPtrArray<COV_GROUPDATA> &ropGroups)
{
	CCS_TRY
	CCS_CATCH_ALL
}

bool CoverageDiagramViewer::GetTimeFrameFromTo(CTime &ropFrom, CTime &ropTo)
{
	CCS_TRY

	ropFrom = omLoadTimeStart;
	ropTo = omLoadTimeEnd;
	
	return true;
	CCS_CATCH_ALL
	return false;
}

void CoverageDiagramViewer::MakeMasstab()
{
	CCS_TRY
	
	CCS_CATCH_ALL
}

void CoverageDiagramViewer::ProcessMsdDelete(MSDDATA *prpMsd,bool bpFromSelf)
{
	if (prpMsd != NULL)
	{
		if (lmSdgu == prpMsd->Sdgu)	
		{
			if (!bpFromSelf)
			{
				CWaitCursor olWait;
				ogSdtData.DeleteVirtuellSdtsByMsdu(prpMsd->Urno);
				LoadSdtWindow();
				ChangeViewToForHScroll(omStartTime,omEndTime);
				pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
				pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);

				CString olMsg = LoadStg(IDS_STRING1923);
				AfxMessageBox(olMsg,MB_ICONSTOP);			
			}
		}
		else
		{
			ogSdtData.DeleteVirtuellSdtsByMsdu(prpMsd->Urno);
		}
	}

}

void CoverageDiagramViewer::ProcessMsdUpdate(MSDDATA *prpMsd,bool bpFromSelf)
{
	if (lmSdgu != 0 && lmSdgu == prpMsd->Sdgu)
	{
		if (!bpFromSelf)
		{

			CWaitCursor olWait;
			DWORD	llNow,llLast, llStart;
			double	flDiff;

			llLast = GetTickCount();
			llStart = llLast;

			LoadSdtWindow();

			llNow  = GetTickCount();
			flDiff = llNow-llLast;
			llLast = llNow;
			TRACE("CovDiaViewer::ProcessMsdUpdate:LoadSdtWindow %.3lf sec\n", flDiff/1000.0 );

			ChangeViewToForHScroll(omStartTime,omEndTime);

			llNow  = GetTickCount();
			flDiff = llNow-llLast;
			llLast = llNow;
			TRACE("CovDiaViewer::ProcessMsdUpdate:ChangeViewToForHScroll %.3lf sec\n", flDiff/1000.0 );

			pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);

			llNow  = GetTickCount();
			flDiff = llNow-llLast;
			llLast = llNow;
			TRACE("CovDiaViewer::ProcessMsdUpdate:WM_UPDATEDEMANDGANTT %.3lf sec\n", flDiff/1000.0 );

			pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);

			llNow  = GetTickCount();
			flDiff = llNow-llLast;
			llLast = llNow;
			TRACE("CovDiaViewer::ProcessMsdUpdate:WM_UPDATE_COVERAGE %.3lf sec\n", flDiff/1000.0 );

			CString olMsg = LoadStg(IDS_STRING1923);
			AfxMessageBox(olMsg,MB_ICONSTOP);			

		}
	}
}

void CoverageDiagramViewer::ProcessMsdNew(MSDDATA *prpMsd,bool bpFromSelf)
{
	if (prpMsd != NULL)
	{
		if (lmSdgu != 0 && lmSdgu == prpMsd->Sdgu)
		{
			if (!bpFromSelf)
			{

				CWaitCursor olWait;
				ogDataSet.CreateVirtuellSdtsByMsduAndSdgu(prpMsd->Urno,prpMsd->Sdgu);
				LoadSdtWindow();
				ChangeViewToForHScroll(omStartTime,omEndTime);
				pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
				pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);

				CString olMsg = LoadStg(IDS_STRING1923);
				AfxMessageBox(olMsg,MB_ICONSTOP);			
			}
		}
		else
		{
			ogDataSet.CreateVirtuellSdtsByMsduAndSdgu(prpMsd->Urno,prpMsd->Sdgu);
		}
	}
}

void CoverageDiagramViewer::ProcessDrrNew(DRRDATA *prpDrr,bool bpNotifyGraph)
{
	if(bmShiftRoster)
	{
		if (prpDrr != NULL)
		{
			CString olUrno;
			olUrno.Format("%ld",prpDrr->Urno);
			ogDataSet.CreateSdtFromDrrSingle(olUrno);
			if (this->bmIsRealRoster)
			{
				char clRosl;
				switch(this->GetCurrentDemandLevel(SHIFT_DEMAND))
				{
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
					clRosl = this->GetCurrentDemandLevel(SHIFT_DEMAND) + '0';
				break;
				case 6:	// 'L'
					clRosl = 'L';
				break;
				case 7:	// 'A'
					clRosl = 'A';
				break;
				}
			
				if ((clRosl == 'A' && prpDrr->Ross[0] == 'A') || clRosl == prpDrr->Rosl[0])
				{
					LoadSdtWindow();
					ChangeViewToForHScroll(omStartTime,omEndTime);
					pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
					pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
				}
			}
		}
	}
}

bool CoverageDiagramViewer::ProcessDrrChange(DRRDATA *prpDrr,bool bpNotifyGraph)
{
	if(bmShiftRoster)
	{
		if(prpDrr != NULL)
		{
			long llUrno = prpDrr->Urno;
			CCSPtrArray <SDTDATA> olSdtList;

			SDTDATA *prlSdt = ogSdtRosterData.GetSdtByUrno(llUrno);
			if(prlSdt != NULL)
			{
				ogSdtRosterData.GetSdtsSdguAndType(llUrno,'D',olSdtList);
				ogSdtRosterData.GetSdtsSdguAndType(llUrno,'A',olSdtList,true);
				olSdtList.Add(prlSdt);
				for(int illc = 0 ;illc < olSdtList.GetSize();illc++)
				{
					SDTDATA *prlDeleteSdt = &olSdtList[illc];
					ogSdtRosterData.DeleteSdtInternal(prlDeleteSdt);
				}
				
			}

			CString olUrno;
			olUrno.Format("%ld",prpDrr->Urno);
			ogDataSet.CreateSdtFromDrrSingle(olUrno);
			if (this->bmIsRealRoster)
			{
				char clRosl;
				switch(this->GetCurrentDemandLevel(SHIFT_DEMAND))
				{
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
					clRosl = this->GetCurrentDemandLevel(SHIFT_DEMAND) + '0';
				break;
				case 6:	// 'L'
					clRosl = 'L';
				break;
				case 7:	// 'A'
					clRosl = 'A';
				break;
				}
			
				if ((clRosl == 'A' && prpDrr->Ross[0] == 'A') || clRosl == prpDrr->Rosl[0])
				{
					LoadSdtWindow();
					ChangeViewToForHScroll(omStartTime,omEndTime);
					pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
					pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
				}
			}
		}
	}	
	return true;
}


bool CoverageDiagramViewer::ProcessDrrRelEnd(void *vpDataPointer)
{
	CCS_TRY
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;

	if (prlBcStruct != NULL)
	{
		CString olData  = prlBcStruct->Data;
		CStringArray olDataArray;
		if(ExtractItemList(olData,&olDataArray, '-') != 4)
		{
			return false;
		}

		CTime olStart	= DBStringToDateTime((olDataArray[0] +"000000"));
		CTime olEnd		= DBStringToDateTime((olDataArray[1] +"000000"));
		CString olUrnos = olDataArray[2];
		CStringArray olRoslArray;
		ogBasicData.MakeClientString(olDataArray[3]);
		if (ExtractItemList(olDataArray[3],&olRoslArray,',') != 2)
		{
			return false;
		}

		char clRosl;
		switch(this->GetCurrentDemandLevel(SHIFT_DEMAND))
		{
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			clRosl = this->GetCurrentDemandLevel(SHIFT_DEMAND) + '0';
		break;
		case 6:	// 'L'
			clRosl = 'L';
		break;
		case 7:	// 'A'
			clRosl = 'A';
		break;
		}

		
//		if (IsBetween(olStart,omLoadTimeStart,omLoadTimeEnd) || IsBetween(olEnd,omLoadTimeStart,omLoadTimeEnd) || IsWithIn(omLoadTimeStart,omLoadTimeEnd,olStart,olEnd))
		if ((clRosl == 'A' || olRoslArray[1] == clRosl) && IsOverlapped(olStart,olEnd,omLoadTimeStart,omLoadTimeEnd))
		{
			CWaitCursor olWait;
			CString olWhere = "";
			olWhere.Format(" WHERE SDAY BETWEEN '%s' AND '%s' AND ROSL ='%s'", omLoadTimeStart.Format("%Y%m%d"), omLoadTimeEnd.Format("%Y%m%d"),olRoslArray[1]);

			//MWO: 27.03.03
			ogBcHandle.SetFilterRange("DRRTAB", "SDAY", "SDAY", omLoadTimeStart.Format("%Y%m%d"), omLoadTimeEnd.Format("%Y%m%d"));
			//END MWO: 27.03.03
			ogDrrData.Read(olWhere);

			ogSdtRosterData.ClearAll();
			ogDataSet.CreateSdtFromDrr();
			LoadSdtWindow(true);
			ChangeViewToForHScroll(omStartTime,omEndTime);
			pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
			pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);

		}

	}
	CCS_CATCH_ALL
		return false;
}

void CoverageDiagramViewer::ProcessDrrNew(char *pcpFields,char *pcpData)
{
	DRRDATA *prlShift = new DRRDATA;
	ogDrrData.GetRecordFromItemList(&ogDrrData.omRecInfo,prlShift,pcpFields,pcpData);
	ogDrrData.InsertInternal(prlShift,false);
}

bool CoverageDiagramViewer::ProcessDrrChange(long lpUrno,char *pcpFields,char *pcpData)
{
	DRRDATA *prlOldShift = ogDrrData.GetDrrByUrno(lpUrno);
	if (prlOldShift == NULL)
	{
		ProcessDrrNew(pcpFields,pcpData);
	}
	else
	{
		DRRDATA rlShift;
		ogDrrData.GetRecordFromItemList(&ogDrrData.omRecInfo,&rlShift,pcpFields,pcpData);
		ogDrrData.UpdateInternal(&rlShift,false);
	}

	return true;
}

bool CoverageDiagramViewer::ProcessAttachment(CString &ropAttachment)
{
	bool blSuccess = false;
	if (!ropAttachment.IsEmpty())
	{
		CAttachment olAttachment(ropAttachment);
		if (olAttachment.bmAttachmentValid)
		{
			blSuccess = true;
			for(int ilI = 0; ilI < olAttachment.omInsertDataList.GetSize(); ilI++)
			{
				ProcessDrrNew(olAttachment.omFieldList.GetBuffer(0), olAttachment.omInsertDataList[ilI].GetBuffer(0));
			}

			for(int ilU = 0; ilU < olAttachment.omUpdateDataList.GetSize(); ilU++)
			{
				DRRDATA rlShift;
				ogDrrData.GetRecordFromItemList(&ogDrrData.omRecInfo,&rlShift, olAttachment.omFieldList.GetBuffer(0), olAttachment.omUpdateDataList[ilU].GetBuffer(0));
				if(!ProcessDrrChange(rlShift.Urno, olAttachment.omFieldList.GetBuffer(0), olAttachment.omUpdateDataList[ilU].GetBuffer(0)))
				{
					ProcessDrrNew(olAttachment.omFieldList.GetBuffer(0), olAttachment.omUpdateDataList[ilU].GetBuffer(0));
				}
			}

			for(int ilD = 0; ilD < olAttachment.omDeleteDataList.GetSize(); ilD++)
			{
				DRRDATA *prlShift = ogDrrData.GetDrrByUrno(atol(olAttachment.omDeleteDataList[ilD]));
				if (prlShift != NULL)
				{
					ogDrrData.DeleteInternal(prlShift,false);
				}
			}
		}
	}
	return blSuccess;
}

bool CoverageDiagramViewer::ProcessDrrRelease(void *vpDataPointer)
{
	CCS_TRY

	// anything to do ?
	if (!bmIsRealRoster)	
		return true;

	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;

	if (prlBcStruct != NULL)
	{
		CString olData  = prlBcStruct->Data;
		CStringArray olDataArray;
		if(ExtractItemList(olData,&olDataArray, '-') != 4)
		{
			return false;
		}

		CTime olStart	= DBStringToDateTime((olDataArray[0] +"000000"));
		CTime olEnd		= DBStringToDateTime((olDataArray[1] +"000000"));
		CString olUrnos = olDataArray[2];
		CStringArray olRoslArray;
		ogBasicData.MakeClientString(olDataArray[3]);
		if (ExtractItemList(olDataArray[3],&olRoslArray,',') != 2)
		{
			return false;
		}

		if (olRoslArray[0] == "0" && olRoslArray[1] == "1")	// shift plan release will be handled in ProcessDrrRelEnd
		{
			return false;
		}

		char clRosl;
		switch(this->GetCurrentDemandLevel(SHIFT_DEMAND))
		{
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			clRosl = this->GetCurrentDemandLevel(SHIFT_DEMAND) + '0';
		break;
		case 6:	// 'L'
			clRosl = 'L';
		break;
		case 7:	// 'A'
			clRosl = 'A';
		break;
		}

		
//		if (IsBetween(olStart,omLoadTimeStart,omLoadTimeEnd) || IsBetween(olEnd,omLoadTimeStart,omLoadTimeEnd) || IsWithIn(omLoadTimeStart,omLoadTimeEnd,olStart,olEnd))
		if ((clRosl == 'A' || olRoslArray[1] == clRosl) && IsOverlapped(olStart,olEnd,omLoadTimeStart,omLoadTimeEnd))
		{
			CWaitCursor olWait;

			if (!ProcessAttachment(prlBcStruct->Attachment))
			{
				CString olWhere = "";
				olWhere.Format(" WHERE SDAY BETWEEN '%s' AND '%s' AND ROSL ='%s'", omLoadTimeStart.Format("%Y%m%d"), omLoadTimeEnd.Format("%Y%m%d"),olRoslArray[1]);

				//MWO: 27.03.03
				ogBcHandle.SetFilterRange("DRRTAB", "SDAY", "SDAY", omLoadTimeStart.Format("%Y%m%d"), omLoadTimeEnd.Format("%Y%m%d"));
				//END MWO: 27.03.03
				ogDrrData.Read(olWhere);

				ogSdtRosterData.ClearAll();
				ogDataSet.CreateSdtFromDrr();
				LoadSdtWindow(true);
				ChangeViewToForHScroll(omStartTime,omEndTime);
				pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
				pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
			}
			else
			{
				ogSdtRosterData.ClearAll();
				ogDataSet.CreateSdtFromDrr();
				LoadSdtWindow(true);
				ChangeViewToForHScroll(omStartTime,omEndTime);
				pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
				pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
			}


		}

	}
	CCS_CATCH_ALL

	return true;
}

bool CoverageDiagramViewer::ProcessDrrReleasePbo(void *vpDataPointer)
{
	CCS_TRY

	// anything to do ?
	if (!bmIsRealRoster)	
		return true;

	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;

	if (prlBcStruct != NULL)
	{
		CString olData  = prlBcStruct->Data;
		CStringArray olDataArray;
		if(ExtractItemList(olData,&olDataArray, '-') != 2)
		{
			return false;
		}

		CTime olStart	= DBStringToDateTime(olDataArray[0]);
		CTime olEnd		= DBStringToDateTime(olDataArray[1]);

		char clRosl;
		switch(this->GetCurrentDemandLevel(SHIFT_DEMAND))
		{
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			clRosl = this->GetCurrentDemandLevel(SHIFT_DEMAND) + '0';
		break;
		case 6:	// 'L'
			clRosl = 'L';
		break;
		case 7:	// 'A'
			clRosl = 'A';
		break;
		}

		
		if ((clRosl == 'A' || '2' == clRosl) && IsOverlapped(olStart,olEnd,omLoadTimeStart,omLoadTimeEnd))
		{
			CWaitCursor olWait;
			CString olWhere = "";
			olWhere.Format(" WHERE SDAY BETWEEN '%s' AND '%s' AND ROSL ='%s'", omLoadTimeStart.Format("%Y%m%d"), omLoadTimeEnd.Format("%Y%m%d"),"2");

			//MWO: 27.03.03
			ogBcHandle.SetFilterRange("DRRTAB", "SDAY", "SDAY", omLoadTimeStart.Format("%Y%m%d"), omLoadTimeEnd.Format("%Y%m%d"));
			//END MWO: 27.03.03
			ogDrrData.Read(olWhere);

			ogSdtRosterData.ClearAll();
			ogDataSet.CreateSdtFromDrr();
			LoadSdtWindow(true);
			ChangeViewToForHScroll(omStartTime,omEndTime);
			pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
			pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);

		}

	}
	CCS_CATCH_ALL

	return true;
}

bool CoverageDiagramViewer::ProcessDemUpdDem(void *vpDataPointer)
{
	CCS_TRY

	// anything to do ?
	if (!bmIsRealRoster)	
		return true;

	struct BcStruct *prlBcStruct = (struct BcStruct *) vpDataPointer;

	if (prlBcStruct != NULL)
	{
		ogBackTrace += "2,";
		ReloadAllDems();
		ogBackTrace += "2e,";
	}
	CCS_CATCH_ALL

	return true;
}

void CoverageDiagramViewer::ProcessDrrDelete(DRRDATA *prpDrr,bool bpNotifyGraph)
{
	if (bmShiftRoster)
	{
		if(prpDrr != NULL)
		{
			long llUrno = prpDrr->Urno;
			CCSPtrArray <SDTDATA> olSdtList;

			SDTDATA *prlSdt = ogSdtRosterData.GetSdtByUrno(llUrno);
			if(prlSdt != NULL)
			{
				ogSdtRosterData.GetSdtsSdguAndType(llUrno,'D',olSdtList);
				ogSdtRosterData.GetSdtsSdguAndType(llUrno,'A',olSdtList,true);
				olSdtList.Add(prlSdt);
				for(int illc = 0 ;illc < olSdtList.GetSize();illc++)
				{
					SDTDATA *prlDeleteSdt = &olSdtList[illc];
					ogSdtRosterData.DeleteSdtInternal(prlDeleteSdt);
				}
			}

			if (this->bmIsRealRoster)
			{
				char clRosl;
				switch(this->GetCurrentDemandLevel(SHIFT_DEMAND))
				{
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
					clRosl = this->GetCurrentDemandLevel(SHIFT_DEMAND) + '0';
				break;
				case 6:	// 'L'
					clRosl = 'L';
				break;
				case 7:	// 'A'
					clRosl = 'A';
				break;
				}
			
				if ((clRosl == 'A' && prpDrr->Ross[0] == 'A') || clRosl == prpDrr->Rosl[0])
				{
					LoadSdtWindow();
					ChangeViewToForHScroll(omStartTime,omEndTime);
					pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
					pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
				}
			}
		}
	}
}




bool CoverageDiagramViewer::ProcessGplChange(RecordSet *prpGpl,bool bpNotifyGraph)
{
	if (bmShiftRoster)
	{
		if (prpGpl != NULL)
		{
			int ilUrnoIdx = ogBCD.GetFieldIndex("GPL", "URNO");
			int ilGsnmIdx = ogBCD.GetFieldIndex("GPL", "GSNM");
			int ilSpluIdx = ogBCD.GetFieldIndex("GPL", "SPLU");
			int ilVafrIdx = ogBCD.GetFieldIndex("GPL", "VAFR");
			int ilVatoIdx = ogBCD.GetFieldIndex("GPL", "VATO");

			long llUrno = atol(prpGpl->Values[ilUrnoIdx]);
			CCSPtrArray <SDTDATA> olSdtList;

			
			ogSdtShiftData.GetSdtByGplUrno(llUrno,olSdtList);
			
			for(int illc = 0 ;illc < olSdtList.GetSize();illc++)
			{
				SDTDATA *prlDeleteSdt = &olSdtList[illc];
				ogSdtShiftData.DeleteSdtInternal(prlDeleteSdt);
			}
				
			CString olUrno = prpGpl->Values[ilUrnoIdx];
			CString olGsnm = prpGpl->Values[ilGsnmIdx];
			CString olSplu = prpGpl->Values[ilSpluIdx];
			CString olVafr = prpGpl->Values[ilVafrIdx];
			CString olVato = prpGpl->Values[ilVatoIdx];

						
			PrepareShiftNameMap();
			if (!this->bmIsRealRoster)
			{
				if (ogBCD.GetField("SPL","SNAM",this->omSplName,"URNO") == olSplu)
				{
					ogDataSet.CreateSdtFromSingleGpl(olUrno,olSplu,olGsnm,olVafr,olVato,omLoadTimeStart,omLoadTimeEnd);
					LoadSdtWindow();
					ChangeViewToForHScroll(omStartTime,omEndTime);
					pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
					pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
				}
			}
		}
	}	
	return true;
}

void CoverageDiagramViewer::ProcessGplDelete(RecordSet *prpGpl,bool bpNotifyGraph)
{
	if (bmShiftRoster)
	{
		if (prpGpl != NULL)
		{
			int ilUrnoIdx = ogBCD.GetFieldIndex("GPL", "URNO");
			int ilSpluIdx = ogBCD.GetFieldIndex("GPL", "SPLU");
			long llUrno = atol(prpGpl->Values[ilUrnoIdx]);
			CString olSplu = prpGpl->Values[ilSpluIdx];

			CCSPtrArray <SDTDATA> olSdtList;

			ogSdtShiftData.GetSdtByGplUrno(llUrno,olSdtList);
			
			for(int illc = 0 ;illc < olSdtList.GetSize();illc++)
			{
				SDTDATA *prlDeleteSdt = &olSdtList[illc];
				ogSdtShiftData.DeleteSdtInternal(prlDeleteSdt);
			}
				
			if (!this->bmIsRealRoster)
			{
				if (ogBCD.GetField("SPL","SNAM",this->omSplName,"URNO") == olSplu)
				{
					LoadSdtWindow();
					ChangeViewToForHScroll(omStartTime,omEndTime);
					pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
					pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
				}
			}
		}
	}	
}

bool CoverageDiagramViewer::ProcessGspChange(RecordSet *prpGsp,bool bpNotifyGraph)
{
	CString olVato,olVafr,olSplu;
	if (bmShiftRoster)
	{
		if (prpGsp != NULL)
		{
			int ilUrnoIdx = ogBCD.GetFieldIndex("GSP", "URNO");
			int ilGpluIdx = ogBCD.GetFieldIndex("GSP", "GPLU");

			int ilVafrIdx = ogBCD.GetFieldIndex("GPL", "VAFR");
			int ilVatoIdx = ogBCD.GetFieldIndex("GPL", "VATO");
			int ilSpluIdx = ogBCD.GetFieldIndex("GPL", "SPLU");

			long llUrno = atol(prpGsp->Values[ilUrnoIdx]);
			CCSPtrArray <SDTDATA> olSdtList;

			ogSdtShiftData.GetSdtByGspUrno(llUrno,olSdtList);
			
			for(int illc = 0 ;illc < olSdtList.GetSize();illc++)
			{
				SDTDATA *prlDeleteSdt = &olSdtList[illc];
				ogSdtShiftData.DeleteSdtInternal(prlDeleteSdt);
			}
			CString olUrno = prpGsp->Values[ilUrnoIdx];
			CString olGplu = prpGsp->Values[ilGpluIdx];
			CCSPtrArray <RecordSet> olList;
			ogBCD.GetRecords("GPL", "URNO", olGplu,&olList);
			for ( illc = 0; illc < olList.GetSize(); illc++)
			{
				olVafr = olList[illc].Values[ilVafrIdx];
				olVato = olList[illc].Values[ilVatoIdx];
				olSplu = olList[illc].Values[ilSpluIdx];
						
				ogDataSet.CreateSdtFromSingleGsp(olUrno,olGplu,olSplu,olVafr,olVato,omLoadTimeStart,omLoadTimeEnd);
			}

			olList.DeleteAll();

			if (!this->bmIsRealRoster)
			{
				if (ogBCD.GetField("SPL","SNAM",this->omSplName,"URNO") == olSplu)
				{
					LoadSdtWindow();
					ChangeViewToForHScroll(omStartTime,omEndTime);
					pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
					pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
				}
			}
		}
	}	
	return true;
}

void CoverageDiagramViewer::ProcessGspDelete(RecordSet *prpGsp,bool bpNotifyGraph)
{
	
	if (bmShiftRoster)
	{
		if (prpGsp != NULL)
		{
			int ilUrnoIdx = ogBCD.GetFieldIndex("GSP", "URNO");
			int ilGpluIdx = ogBCD.GetFieldIndex("GSP", "GPLU");

			int ilSpluIdx = ogBCD.GetFieldIndex("GPL", "SPLU");

			long llUrno = atol(prpGsp->Values[ilUrnoIdx]);
			CString olGplu = prpGsp->Values[ilGpluIdx];
			CString olSplu;

			CCSPtrArray <RecordSet> olList;
			ogBCD.GetRecords("GPL", "URNO", olGplu,&olList);
			for (int illc = 0; illc < olList.GetSize(); illc++)
			{
				olSplu = olList[illc].Values[ilSpluIdx];
			}

			CCSPtrArray <SDTDATA> olSdtList;

			SDTDATA *prlDeleteSdt = ogSdtShiftData.GetSdtByUrno(llUrno);
			
			if (prlDeleteSdt != NULL)
			{
				ogSdtShiftData.DeleteSdtInternal(prlDeleteSdt);
			}
				
			if (!this->bmIsRealRoster)
			{
				if (ogBCD.GetField("SPL","SNAM",this->omSplName,"URNO") == olSplu)
				{
					LoadSdtWindow();
					ChangeViewToForHScroll(omStartTime,omEndTime);
					pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
					pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
				}
			}
		}
	}	

}

bool CoverageDiagramViewer::ProcessSplChange(RecordSet *prpGsp,bool bpNotifyGraph)
{
	if (bmShiftRoster)
	{
		PrepareShiftNameMap();
		if (!this->bmIsRealRoster)
		{
			pomAttachWnd->SendMessage(WM_DEMAND_UPDATECOMBO,0,0);
		}
	}

	return true;
}

void CoverageDiagramViewer::ProcessSplDelete(RecordSet *prpSpl,bool bpNotifyGraph)
{
	if (bmShiftRoster)
	{
		int ilUrnoIdx = ogBCD.GetFieldIndex("SPL", "URNO");
		if (prpSpl != NULL)
		{
			long llUrno = atol(prpSpl->Values[ilUrnoIdx]);
			ogSdtShiftData.DeleteSdtWithSplu(llUrno);

			PrepareShiftNameMap();

			if (!this->bmIsRealRoster)
			{
				if (ogBCD.GetField("SPL","SNAM",this->omSplName,"URNO") == prpSpl->Values[ilUrnoIdx])
				{
					LoadSdtWindow();
					ChangeViewToForHScroll(omStartTime,omEndTime);
					pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
					pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
					pomAttachWnd->SendMessage(WM_DEMAND_UPDATECOMBO,0,0);
				}
				else
				{
					pomAttachWnd->SendMessage(WM_DEMAND_UPDATECOMBO,0,0);
				}
			}
		}
	}

}



void CoverageDiagramViewer::ProcessBsdNew(BSDDATA *	prpBsd)
{
	CCS_TRY

	CTime olTimeFrom, olTimeTo;
	GetTimeFrameFromTo(olTimeFrom, olTimeTo);

	CMapPtrToPtr *polBsdUrnoMap;
	
	int ilBsCount = 0;
	if(olTimeFrom < olTimeTo)
	{
		CTime olFrom = CTime(olTimeFrom.GetYear(), olTimeFrom.GetMonth(), olTimeFrom.GetDay(), 0, 0, 0);
		CTime olTo   = CTime(olTimeTo.GetYear(), olTimeTo.GetMonth(), olTimeTo.GetDay(), 23, 59, 59);
		
	
		if(prpBsd != NULL)
		{
			BASICSHIFTS *prlBasicShift = new BASICSHIFTS;
			//RST PRF 8889
			lmInternalUrno++;	
			prlBasicShift->Urno = lmInternalUrno;	

			prlBasicShift->RUrno = prpBsd->Urno;
			prlBasicShift->From = HourStringToDate( CString( prpBsd->Esbg), olFrom);
			prlBasicShift->To = HourStringToDate( CString( prpBsd->Lsen), olFrom);//olFrom + CTimeSpan(atoi(prpBsd->Sdu1)*60);				
			if(prlBasicShift->From > prlBasicShift->To)
			{
				prlBasicShift->From = prlBasicShift->From - CTimeSpan(1, 0, 0, 0);
			}
			
			prlBasicShift->BlFrom = HourStringToDate( CString( prpBsd->Bkf1), olFrom);	
			prlBasicShift->BlTo= HourStringToDate( CString( prpBsd->Bkt1), olFrom);	
			

			if(prlBasicShift->BlFrom > prlBasicShift->BlTo)
			{
				prlBasicShift->BlFrom = prlBasicShift->BlFrom - CTimeSpan(1, 0, 0, 0);
			}


			if(prlBasicShift->BlTo > prlBasicShift->To)
			{
				prlBasicShift->BlFrom = prlBasicShift->BlFrom - CTimeSpan(1, 0, 0, 0);
				prlBasicShift->BlTo = prlBasicShift->BlTo - CTimeSpan(1, 0, 0, 0);
			}

			prlBasicShift->BreakFrom = prlBasicShift->BlFrom;		
			prlBasicShift->BreakTo = prlBasicShift->BlFrom + CTimeSpan(atoi(prpBsd->Bkd1)*60);		
		
			prlBasicShift->Relative = CString(prpBsd->Bkr1);	
			prlBasicShift->Code = CString(prpBsd->Bsdc);		
			prlBasicShift->Stretch = atoi(prpBsd->Sex1);	
			prlBasicShift->Shrink = atoi(prpBsd->Ssh1);	
			prlBasicShift->Type = CString(prpBsd->Type);
			prlBasicShift->Vafr = prpBsd->Vafr;		
			prlBasicShift->Vato = prpBsd->Vato;		

			prlBasicShift->Fctc = CString(prpBsd->Fctc);		

			omBasicShiftData.Add(prlBasicShift);
			omBasicShiftUrnoMap.SetAt((void *)prlBasicShift->Urno,prlBasicShift);
			
			if (omBasicShiftKeyMap.Lookup((LPCSTR)prlBasicShift->Code,(void *&) polBsdUrnoMap) == TRUE)
			{
				polBsdUrnoMap->SetAt((void *)prlBasicShift->Urno,prlBasicShift);
			}
			else
			{
				polBsdUrnoMap = new CMapPtrToPtr;
				polBsdUrnoMap->SetAt((void *)prlBasicShift->Urno,prlBasicShift);
				omBasicShiftKeyMap.SetAt((LPCSTR)prlBasicShift->Code,polBsdUrnoMap);
			}
			if(IsPassBshFilter(prlBasicShift))
			{
				bmCalcBsdBars = true;
				ChangeViewToForHScroll();
				int ilLineNo = -1;
				LONG lParam = MAKELONG(ilLineNo, BASIC_SHIFT);
				
				//pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,UD_UPDATEGROUP, lParam);
				pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,UD_INSERTBASICSHIFT, lParam);
			}
		}
	}
	CCS_CATCH_ALL	
}

void CoverageDiagramViewer::ProcessBsdChange(BSDDATA *prpBsd)
{
	CCS_TRY

	POSITION rlPos;

	BASICSHIFTS *prlBasicShift = NULL;
	CMapPtrToPtr *polBsdUrnoMap;

	CTime olTimeFrom, olTimeTo;
	GetTimeFrameFromTo(olTimeFrom, olTimeTo);

	
	CTime olFrom = CTime(olTimeFrom.GetYear(), olTimeFrom.GetMonth(), olTimeFrom.GetDay(), 0, 0, 0);
	CTime olTo   = CTime(olTimeTo.GetYear(), olTimeTo.GetMonth(), olTimeTo.GetDay(), 23, 59, 59);
		
	

	if(prpBsd != NULL)
	{
		if(omBasicShiftKeyMap.Lookup((LPCSTR)prpBsd->Bsdc,(void *&) polBsdUrnoMap) == TRUE)
		{
			CCSPtrArray<BASICSHIFTS> olBsdUrnoArray;
			CString olKey;
			long llUrno;
			for ( rlPos =  polBsdUrnoMap->GetStartPosition(); rlPos != NULL; )
			{
				polBsdUrnoMap->GetNextAssoc(rlPos, (void *& )llUrno,(void *& ) prlBasicShift);
				olBsdUrnoArray.Add(prlBasicShift);
			}

			for (int ilLc = 0; ilLc < olBsdUrnoArray.GetSize(); ilLc++)
			{
				prlBasicShift = &olBsdUrnoArray[ilLc];
				if (prlBasicShift != NULL)
				{
					if(prlBasicShift->RUrno == prpBsd->Urno)
					{

						prlBasicShift->From = HourStringToDate( CString( prpBsd->Esbg), olFrom);
						prlBasicShift->To = HourStringToDate( CString( prpBsd->Lsen), olFrom);//olFrom + CTimeSpan(atoi(prpBsd->Sdu1)*60);				
						if(prlBasicShift->From > prlBasicShift->To)
						{
							prlBasicShift->From = prlBasicShift->From - CTimeSpan(1, 0, 0, 0);
						}
						
						prlBasicShift->BlFrom = HourStringToDate( CString( prpBsd->Bkf1), olFrom);	
						prlBasicShift->BlTo= HourStringToDate( CString( prpBsd->Bkt1), olFrom);	
						

						if(prlBasicShift->BlFrom > prlBasicShift->BlTo)
						{
							prlBasicShift->BlFrom = prlBasicShift->BlFrom - CTimeSpan(1, 0, 0, 0);
						}


						if(prlBasicShift->BlTo > prlBasicShift->To)
						{
							prlBasicShift->BlFrom = prlBasicShift->BlFrom - CTimeSpan(1, 0, 0, 0);
							prlBasicShift->BlTo = prlBasicShift->BlTo - CTimeSpan(1, 0, 0, 0);
						}

						prlBasicShift->BreakFrom = prlBasicShift->BlFrom;		
						prlBasicShift->BreakTo = prlBasicShift->BlFrom + CTimeSpan(atoi(prpBsd->Bkd1)*60);		
					
						prlBasicShift->Relative = CString(prpBsd->Bkr1);	
						prlBasicShift->Code = CString(prpBsd->Bsdc);		
						prlBasicShift->Stretch = atoi(prpBsd->Sex1);	
						prlBasicShift->Shrink = atoi(prpBsd->Ssh1);	
						prlBasicShift->Type = CString(prpBsd->Type);
						prlBasicShift->Vafr = prpBsd->Vafr;		
						prlBasicShift->Vato = prpBsd->Vato;		

						prlBasicShift->Fctc = CString(prpBsd->Fctc);		

						if(IsPassBshFilter(prlBasicShift))
						{
							bmCalcBsdBars = true;
							ChangeViewToForHScroll();
							int ilLineNo = -1;
							LONG lParam = MAKELONG(ilLineNo, BASIC_SHIFT);
				
							 pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,UD_UPDATEGROUP, lParam);
						}

					}
				}
			}
		}	
	}

	CCS_CATCH_ALL
}
void CoverageDiagramViewer::ProcessBsdDelete(BSDDATA *prpBsd)
{
	POSITION rlPos;

	BASICSHIFTS *prlBasicShift = NULL;
	CMapPtrToPtr *polBsdUrnoMap;
	int ilLineNo = -1, i;

	if(prpBsd != NULL)
	{
		if(omBasicShiftKeyMap.Lookup((LPCSTR)prpBsd->Bsdc,(void *&) polBsdUrnoMap) == TRUE)
		{
			CCSPtrArray<BASICSHIFTS> olBsdUrnoArray;
			CString olKey;
			long llUrno;
			for ( rlPos =  polBsdUrnoMap->GetStartPosition(); rlPos != NULL; )
			{
				polBsdUrnoMap->GetNextAssoc(rlPos, (void *& )llUrno,(void *& ) prlBasicShift);
				olBsdUrnoArray.Add(prlBasicShift);
			}

			for (int ilLc = 0; ilLc < olBsdUrnoArray.GetSize(); ilLc++)
			{
				prlBasicShift = &olBsdUrnoArray[ilLc];
				if(prlBasicShift != NULL)
				{
					if(prlBasicShift->RUrno == prpBsd->Urno)
					{
						polBsdUrnoMap->RemoveKey((void *)prlBasicShift->Urno);
						omBasicShiftUrnoMap.RemoveKey((void *)prlBasicShift->Urno);
						if(IsPassBshFilter(prlBasicShift))
						{
							if ( FindBshLine(BASIC_SHIFT, ilLineNo, llUrno) )
							{
								DeleteLine(BASIC_SHIFT, ilLineNo);
								LONG lParam = MAKELONG(ilLineNo, BASIC_SHIFT);
								pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,UD_DELETELINE, lParam);
							}
    					}
					}
				}
			}
			omBasicShiftKeyMap.RemoveKey ((LPCSTR)prpBsd->Bsdc);
			if ( polBsdUrnoMap )
			{
				delete polBsdUrnoMap;
			}
			for ( i=omBasicShiftData.GetSize()-1; i>=0; i-- )
			{
				prlBasicShift = &(omBasicShiftData[i]);
				if ( prlBasicShift && ( prlBasicShift->Code == prpBsd->Bsdc ) )
				{
					omBasicShiftData.DeleteAt(i);
				}
			}
		}
	}
}


void CoverageDiagramViewer::ProcessSdgNew(SDGDATA *prpSdg)
{
	CCS_TRY
		if (this->GetShiftRoster())
			return;
		pomAttachWnd->SendMessage(WM_DEMAND_UPDATECOMBO,	0, 0);
	CCS_CATCH_ALL
}

void CoverageDiagramViewer::ProcessSdgChange(SDGDATA *prpSdg)
{
	CCS_TRY
		if (this->GetShiftRoster())
			return;

		CString olDemName = this->GetCurrentDemandName(SHIFT_DEMAND);
		if (prpSdg->Dnam == olDemName)
		{
			pomAttachWnd->SendMessage(WM_DEMAND_SELECTCOMBO,0,(LPARAM)prpSdg->Dnam);

			CString olMsg = LoadStg(IDS_STRING1923);
			AfxMessageBox(olMsg,MB_ICONSTOP);			
		}
	CCS_CATCH_ALL
}

void CoverageDiagramViewer::ProcessSdgDelete(SDGDATA *prpSdg)
{
	CCS_TRY
		if (this->GetShiftRoster())
			return;

		CString olDemName = this->GetCurrentDemandName(SHIFT_DEMAND);
		if (prpSdg->Dnam == olDemName)
		{
			pomAttachWnd->SendMessage(WM_DEMAND_UPDATECOMBO,0,0);
			pomAttachWnd->SendMessage(WM_DEMAND_SELECTCOMBO,0,(LPARAM)"");

			CString olMsg = LoadStg(IDS_STRING1924);
			AfxMessageBox(olMsg,MB_ICONSTOP);
		}
		else
		{
			pomAttachWnd->SendMessage(WM_DEMAND_UPDATECOMBO,0,0);
		}
	CCS_CATCH_ALL
}


void CoverageDiagramViewer::ProcessDemCflReady(int ipDDXType)
{
	CCS_TRY
	ogBackTrace += "2,";
	ReloadAllDems();
	pomAttachWnd->SendMessage(WM_UPDATETOOLBAR, 0, 0);
	ogBackTrace += "2e,";
	CCS_CATCH_ALL
}

void CoverageDiagramViewer::ProcessDemChanged(DEMDATA *prpDem)
{
	CCS_TRY
	if (!prpDem)
		return;

	ogBackTrace += "2,";

	bool blUpdate = false;
	bool blDemUpdateAllowed = DemUpdateAllowed();

	switch(prpDem->IsChanged)
	{
	case DATA_CHANGED :		// changed demand
		blUpdate = FilterDemand(prpDem,blDemUpdateAllowed);
		ogDdx.DataChanged((void *)this, INFO_DEM_CHANGE,prpDem); //Tell it to other windows
	break;
	case DATA_UNCHANGED :	// new demand	
		blUpdate = FilterDemand(prpDem,blDemUpdateAllowed); 
		ogDdx.DataChanged((void *)this, INFO_DEM_NEW,prpDem); //Tell it to other windows
	break;
	case DATA_DELETED :		// deleted
		blUpdate = DeleteDemand(prpDem,blDemUpdateAllowed);
		ogDdx.DataChanged((void *)this, INFO_DEM_DELETE,prpDem); //Tell it to other windows
	break;
	default:
		;
	}

	ogBackTrace += "3,";

	if (blUpdate)
	{
		if (blDemUpdateAllowed)
		{
			pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
		}
		else
		{
			pomAttachWnd->SendMessage(WM_UPDATEDEMANDBTN, 1, 0);
		}
	}

	ogBackTrace += "2e,";
	CCS_CATCH_ALL
}




void CoverageDiagramViewer::ProcessSdtChange(SDTDATA *prpSdt)
{
	CCS_TRY
	int ilLineno;
	int ilBarno = 0;
	
	if(FindSdtLine(SHIFT_DEMAND, ilLineno, prpSdt->Urno))
	{
		//MWO:P 31.03.03 Performance
		bmMaleAllesNeu_lokalgesetzt = false;
		//MWO:P 31.03.03 Performance
		ProcessSdtDelete(prpSdt);
		//MWO:P 31.03.03 Performance
		bmMaleAllesNeu_lokalgesetzt = true;
		//MWO:P 31.03.03 Performance
		if(prpSdt != NULL)
		{
			omSdtUrnoMap.SetAt((void *)prpSdt->Urno,prpSdt);
		}
		bmReCalcShifts = true;
		if(IsPassSdtFilter(prpSdt))
		{
			COV_BARDATA rlBar;
			ilLineno = MakeSdtLine(prpSdt);
			MakeSdtBarData(&rlBar, prpSdt, 0);
			CreateBar(SHIFT_DEMAND, ilLineno, &rlBar, TRUE);
			if (IsSdtVisible(prpSdt) && ::IsWindow(pomAttachWnd->GetSafeHwnd()))
			{
				 //LONG lParam = ilLineno;
				 LONG lParam = MAKELONG(ilLineno, SHIFT_DEMAND);
				 bmSdtChanged = true;

				//MWO:P 31.03.03 Performance
				if(bmMaleAllesNeu_lokalgesetzt == true && bmMaleAllesNeu_globalgesetzt == true)
				{
					 if(bmMaxView)
					 {
						 pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,UD_INSERTLINE, lParam);
					 }
					 else
					 {
						 pogCoverageDiagram->UpdateForShiftDemandUpdate(true);
					 }
				 	 pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 1);

				}
				//END MWO:P 31.03.03 Performance

			}
		}
		else if(bmMaxView==false && bmSdtChanged)
		{
			//MWO:P 31.03.03 Performance
			if(bmMaleAllesNeu_lokalgesetzt == true && bmMaleAllesNeu_globalgesetzt == true)
			{
				pogCoverageDiagram->UpdateForShiftDemandUpdate(false);
        	 	pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 1);
			}
			//END MWO:P 31.03.03 Performance
		}
		
	}// if (FindBarGlobal(prpGhd->Key, ilLineno, ilBarno))		
	else if(IsPassSdtFilter(prpSdt) )
	{
		if(prpSdt != NULL)
		{
			omSdtUrnoMap.SetAt((void *)prpSdt->Urno,prpSdt);
		}

		bmReCalcShifts = true;

		if(bmMaxView==true)
		{
			if(IsPassSdtFilter(prpSdt))
			{
				ilLineno = MakeSdtLine(prpSdt);
				if(ilLineno >= 0)
				{
					MakeSdtBar(SHIFT_DEMAND, ilLineno, prpSdt);
					if (IsSdtVisible(prpSdt) && ::IsWindow(pomAttachWnd->GetSafeHwnd()))
					{
						bmSdtChanged = true;
						LONG lParam = MAKELONG(ilLineno, SHIFT_DEMAND);
						//MWO:P 31.03.03 Performance
						if(bmMaleAllesNeu_lokalgesetzt == true && bmMaleAllesNeu_globalgesetzt == true)
						{
							pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_INSERTLINE , lParam);
							pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 1);
						}
						//END MWO:P 31.03.03 Performance
					}
				}
			}
		}
		else
		{
			//MWO:P 31.03.03 Performance
			//ChangeViewTo(SelectView(),omStartTime,omEndTime);
			//END MWO:P 31.03.03 Performance

			if (IsSdtVisible(prpSdt) && ::IsWindow(pomAttachWnd->GetSafeHwnd()))
			{
				bmSdtChanged = true;
				//MWO:P 31.03.03 Performance
				if(bmMaleAllesNeu_lokalgesetzt == true && bmMaleAllesNeu_globalgesetzt == true)
				{
					pogCoverageDiagram->UpdateForShiftDemandUpdate(true);
					pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 1);
				}
				//END MWO:P 31.03.03 Performance
			}
		}
	}

	
	CCS_CATCH_ALL
}


void CoverageDiagramViewer::ProcessSdtSelChange(SDTDATA *prpSdt)
{
	CCS_TRY
	int ilLineno;
	int ilBarno = 0;
	if(prpSdt==NULL)
		return;
	if(FindSdtLine(SHIFT_DEMAND, ilLineno, prpSdt->Urno))
	{
		if(IsPassSdtFilter(prpSdt))
		{
			COV_LINEDATA *prlLine = GetLine(SHIFT_DEMAND, ilLineno);			
			int ilBarno = GetBarnoFromTime(SHIFT_DEMAND, ilLineno, prpSdt->Esbg,prpSdt->Lsen,-1,prlLine->MaxOverlapLevel);
			if(ilBarno>=0)
			{
				COV_BARDATA *prlBar = GetBar(SHIFT_DEMAND, ilLineno, ilBarno);
				if(prlBar!=NULL)
				{
					if(prpSdt->IsSelected==FALSE)
					{
						prlBar->MarkerBrush = ogBrushs[LIME_IDX];
						prlBar->TextColor = ogColors[BLACK_IDX];
					}
					else
					{
						prlBar->MarkerBrush = ogBrushs[YELLOW_IDX];
						prlBar->TextColor = ogColors[BLACK_IDX];
					}
				}
			}
			if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
			{
				 LONG lParam = MAKELONG(ilLineno, SHIFT_DEMAND);
				 pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,UD_UPDATELINE, lParam);
			}
		}
	}
	CCS_CATCH_ALL
}



void CoverageDiagramViewer::ProcessSdtDelete(SDTDATA *prpSdt)
{
	CCS_TRY
	int ilLineno;
	/*bmSdtChanged = */

	bmReCalcShifts = true;

	DeleteFromSdtData(prpSdt);
	if (bmMaxView)
	{
		if(FindSdtLine(	SHIFT_DEMAND, ilLineno, prpSdt->Urno))
		{
			bmSdtChanged = true;
			DeleteLine(SHIFT_DEMAND, ilLineno);
			if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
			{
				LONG lParam = MAKELONG(ilLineno, SHIFT_DEMAND);
				pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_DELETELINE , lParam);
				//MWO:P 31.03.03 Performance
				if(bmMaleAllesNeu_lokalgesetzt == true && bmMaleAllesNeu_globalgesetzt == true)
				{
					pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 1);	
				}
				//MWO:P 31.03.03 Performance
			}
		}
	}
	else
	{
		if(::IsWindow(pomAttachWnd->GetSafeHwnd()))
		{
			//MWO:P 31.03.03 Performance
			if(bmMaleAllesNeu_lokalgesetzt == true && bmMaleAllesNeu_globalgesetzt == true)
			{
				pogCoverageDiagram->UpdateForShiftDemandUpdate(true);
			}
			//MWO:P 31.03.03 Performance
		}
	}
	CCS_CATCH_ALL
}

void CoverageDiagramViewer::ProcessSdgRelease(SDGDATA *prpSdg)
{
	CCS_TRY

	if (bmShiftRoster)			// 'DRR's currently displayed ? 
		return;
	else if (bmActuellState)	// 'current' selected ?
	{
		LoadSdtWindow();
		ChangeViewToForHScroll(omStartTime,omEndTime);
		pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
		pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
		
	}
	else if (prpSdg->Urno == this->lmSdgu)	// matching SDG loaded ?
	{
		LoadSdtWindow();
		ChangeViewToForHScroll(omStartTime,omEndTime);
		pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
		pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
	}

	CCS_CATCH_ALL
}

void CoverageDiagramViewer::ProcessMsdRelease(SDGDATA *prpSdg)
{
	CCS_TRY

	CWaitCursor olWait;

	if (prpSdg->Urno == this->lmSdgu)
	{
		ogSdtData.DeleteVirtuellSdtsBySdg(prpSdg->Urno);
		ogDataSet.CreateVirtuellSdtsBySdgu(prpSdg->Urno);
		LoadSdtWindow(true);
		ChangeViewToForHScroll(omStartTime,omEndTime);
		pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
		pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
	}
	CCS_CATCH_ALL
}

bool CoverageDiagramViewer::GetAdditiv()
{
	CCS_TRY
	return bmAdditiv;
	CCS_CATCH_ALL
	return false;
}

void CoverageDiagramViewer::SetAdditiv(bool bpAdditiv)
{
	CCS_TRY
		bmAdditiv = bpAdditiv;
	CCS_CATCH_ALL
}

bool CoverageDiagramViewer::GetMaxView()
{
	return bmMaxView;
}
void CoverageDiagramViewer::SetMaxView(bool bpMaxView)
{
	CCS_TRY
	bmMaxView =  bpMaxView;
	CCS_CATCH_ALL
}

void CoverageDiagramViewer::SetIgnoreBreak(bool bpIgnoreBreak)
{
	CCS_TRY
	bmIgnoreBreak = bpIgnoreBreak;
	CCS_CATCH_ALL
}

void CoverageDiagramViewer::Register()
{
}

void CoverageDiagramViewer::SetShiftRoster(bool bpShiftRoster)
{
	CCS_TRY
	bmShiftRoster = bpShiftRoster;
	CCS_CATCH_ALL
}

bool CoverageDiagramViewer::GetIgnoreBreak() 
{
	return bmIgnoreBreak;
}

bool CoverageDiagramViewer::GetShiftRoster() 
{
	return bmShiftRoster;
}


void CoverageDiagramViewer::ResetFlags()
{
	bmSdtChanged = false;
	bmLoadGhds = false;
}
void CoverageDiagramViewer::GetGhsList(CUIntArray &opGhsUrnos, CUIntArray &opPfcUrnos)
{
	CCS_TRY
	opGhsUrnos.RemoveAll();
	opPfcUrnos.RemoveAll();
	int i = 0;
	for(i = 0; i < omGhsUrnos.GetSize(); i++)
	{
		opGhsUrnos.Add(omGhsUrnos[i]);
	}// end for
	for(i = 0; i < omPfcUrnos.GetSize(); i++)
	{
		opPfcUrnos.Add(omPfcUrnos[i]);
	}//end for
	CCS_CATCH_ALL
}

static int CompareSdtByTimes(const SDTDATA **e1, const SDTDATA **e2)
{
	CCS_TRY
	int ilRet = ((**e1).Esbg == (**e2).Esbg) ? 
									(((**e1).Lsen == (**e2).Lsen) ? 0: 
									((**e1).Lsen > (**e2).Lsen) ? 1: -1):
					  ((**e1).Esbg > (**e2).Esbg) ? 1: -1;
	return ilRet;
	CCS_CATCH_ALL
	return 0;
}

CString CoverageDiagramViewer::GetPfcCodeList()
{
	CCS_TRY
		return omPfcCodeList;
	CCS_CATCH_ALL
	return CString("");
}

void CoverageDiagramViewer::SetMarkTime(CTime opMarkTimeStart, CTime opMarkTimeEnd)
{
CCS_TRY

	omMarkTimeStart = opMarkTimeStart;
	omMarkTimeEnd = opMarkTimeEnd;
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		pomAttachWnd->SendMessage(WM_MARKTIME,0,0);	 
	}
CCS_CATCH_ALL
}


int CoverageDiagramViewer::GetCompressedSdtCount(SDTDATA *prpSdt,bool bpUseFactor)
{
	CCS_TRY
	//CString olRet = "";
	int ilCnt = 0;
	int ilPos;
	if((ilPos = Find(prpSdt->Fctc,"- "))<0)
	{
		ilPos = strlen(prpSdt->Fctc);
	}
	POSITION rlPos;
	long llUrno = 0;
	SDTDATA *prlSdt = NULL;
	if(bmShiftRoster)
	{
		for(rlPos = omSdtUrnoMap.GetStartPosition();rlPos!=NULL;)
		{
			omSdtUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
			if(prpSdt->Bsdu==prlSdt->Bsdu && strncmp(prpSdt->Fctc,prlSdt->Fctc,ilPos)==0 && prpSdt->Esbg==prlSdt->Esbg && prpSdt->Lsen == prlSdt->Lsen)
			{
				if (bpUseFactor)
					ilCnt += prlSdt->Faktor;
				else
					ilCnt++;
			}
		}
	}
	else
	{
		CMapPtrToPtr *polShiftDemMap = NULL;
		
		if(omShiftsDemandMap.Lookup((void *)prpSdt->Urno,(void *&)polShiftDemMap)==TRUE)
		{
			polShiftDemMap->RemoveAll();
		}
		else
		{
			polShiftDemMap = new CMapPtrToPtr;
			omShiftsDemandMap.SetAt((void *)prpSdt->Urno,polShiftDemMap);
		}
		for(rlPos = omSdtUrnoMap.GetStartPosition();rlPos!=NULL;)
		{


			omSdtUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
			if(prpSdt->Bsdu==prlSdt->Bsdu && strncmp(prpSdt->Fctc,prlSdt->Fctc,ilPos)==0 && prpSdt->Esbg==prlSdt->Esbg && prpSdt->Lsen == prlSdt->Lsen)
			{
				if (bpUseFactor)
					ilCnt += prlSdt->Faktor;
				else
					ilCnt++;
				polShiftDemMap->SetAt((void *)prlSdt->Urno,prlSdt);
			}
							

		}
	}
	return ilCnt;
	CCS_CATCH_ALL
	return -1;
}
 


bool CoverageDiagramViewer::GetTimeFrameTimes(CTime &opFrom,CTime &opTo)
{
CCS_TRY

	opFrom = omLoadTimeStart;
	opTo = omLoadTimeEnd;
	return true;
CCS_CATCH_ALL
return false;
}

bool CoverageDiagramViewer::IsGhdChanged()
{
	return bmLoadGhds;
}
bool CoverageDiagramViewer::IsSdtChanged()
{
	return bmSdtChanged;
}
bool CoverageDiagramViewer::ReCalcShifts()
{
	return bmReCalcShifts;
}
void CoverageDiagramViewer::SetDisplayTimes(CTime opStart,CTime opEnd)
{
	//bmLoadGhds = false;
	if(omStartTime != opStart)
	{
		bmLoadGhds = true;
		bmSdtChanged = true;
		omStartTime = opStart;
	}
	
	if(omEndTime != opEnd)
	{
		bmLoadGhds = true;
		bmSdtChanged = true;
		omEndTime = opEnd;
	}
	/*
	if(bmLoadGhds)
	{		
		LoadGhdWindow();
	}
	*/

	/*
	if(bmSdtChanged)
	{
		LoadSdtWindow();
	}
	*/
}


int Find(char *pcpString,char *pcpSubString)
{
	char *pclPtr = NULL;
	if((pclPtr=strstr(pcpString,pcpSubString))!=NULL)
	{
		return (pclPtr-pcpString);
	}
	else
	{
		return -1;
	}
}


void CoverageDiagramViewer::LoadSdtWindow(bool bpSdtChanged)
{
	if(bpSdtChanged)
		bmSdtChanged = true;

	bmReCalcShifts = true;
	//omSdtData.RemoveAll();
	omSdtUrnoMap.RemoveAll();
	if(!bmShiftRoster)
	{
		if(bmActuellState)
		{
			ogSdtData.GetSdtsBetweenTimesAndSdgu(omLoadTimeStart,omLoadTimeEnd,ogSdgData.omOperativeMap,omSdtUrnoMap);
		}
		else
		{
			SDGDATA * prlSdg = ogSdgData.GetSdgByUrno(lmSdgu);
			if(prlSdg != NULL)
			{
				bmSdgChanged = !prlSdg->Expd;
			}
			ogSdtData.GetSdtsBetweenTimesAndSdgu(omLoadTimeStart,omLoadTimeEnd,lmSdgu,omSdtUrnoMap);
		}
	}
	else
	{
		if(bmIsRealRoster)
		{
			int ilLevel = GetCurrentDemandLevel(SHIFT_DEMAND);
			ogSdtRosterData.GetSdtsBetweenTimesAndDrrFilter(omLoadTimeStart,omLoadTimeEnd,omSdtUrnoMap,bmShowActualDsr,ilLevel);
		}
		else
		{


			CString *polValue = NULL;
			CString olValue = "";

			if(omShiftNameMap.Lookup(omSplName,(void *&)polValue)  == TRUE)
			{
				if(polValue != NULL)
				{
					olValue = *polValue;
				}
			}
			ogSdtShiftData.GetSdtsByGplus(olValue,omSdtUrnoMap);
		}
	}
}
		
bool CoverageDiagramViewer::DeleteFromSdtData(SDTDATA *prpSdt)
{
	// Is SDT record currently in omSdtArray?
	if(prpSdt!=NULL)
		omSdtUrnoMap.RemoveKey((void *)prpSdt->Urno);
	return true;
}


bool CoverageDiagramViewer::IsPassDemFilter(DEMDATA *prpDem)
{
	CCS_TRY
	
	void *p;
	
	bool blIsTempOk = false;
	CString olTemplateName = ogDemData.GetTemplateName(prpDem->Urud);
	if(!olTemplateName.IsEmpty())
	{
		
		if (bmUseAllTemplate == false)
		{
			blIsTempOk = (omCMapForTemplate.Lookup(ogDemData.GetTemplateName(prpDem->Urud),p) == TRUE)?true:false;
		}
		else
		{
			blIsTempOk = true;
		}
	}

	if(blIsTempOk)
	{
		blIsTempOk = false;
		CString olTyp = prpDem->Obty;

		if(olTyp == "AFT" )
		{
			CMapPtrToPtr *polDemMap;

			if(prpDem->Ouri > 0)
			{
				if (omFlightDemMap.Lookup((void *)prpDem->Ouri,(void *&) polDemMap) == TRUE)
				{
					polDemMap->SetAt((void *)prpDem->Urno,prpDem);
				}
				else
				{
					polDemMap = new CMapPtrToPtr;
					polDemMap->SetAt((void *)prpDem->Urno,prpDem);
					omFlightDemMap.SetAt((void *)prpDem->Ouri,polDemMap);
				}
			}

			if(prpDem->Ouro > 0)
			{
				if (omFlightDemMap.Lookup((void *)prpDem->Ouro,(void *&) polDemMap) == TRUE)
				{
					polDemMap->SetAt((void *)prpDem->Urno,prpDem);
				}
				else
				{
					polDemMap = new CMapPtrToPtr;
					polDemMap->SetAt((void *)prpDem->Urno,prpDem);
					omFlightDemMap.SetAt((void *)prpDem->Ouro,polDemMap);
				}
			}
		}

		if(bmFlug)
		{
			if (olTyp == "AFT" && prpDem->IsCommonCheckin() == FALSE)
			{

				FLIGHTDATA *prlFlight = NULL;
				if((omFilterFlightUrnoMap.Lookup((void *)prpDem->Ouri,(void *&)prlFlight)  == TRUE) || 
					(omFilterFlightUrnoMap.Lookup((void *)prpDem->Ouro,(void *&)prlFlight)  == TRUE))
				{
					blIsTempOk = true;
				}
				else
				{
					blIsTempOk = false;
				}

				if (prpDem->Ouri == 0 && prpDem->Ouro == 0)
				{
					blIsTempOk = true;
				}

			}
		}

		if(bmNotFlug)
		{
			if (olTyp != "AFT" )
			{
				blIsTempOk = true;
			}
		}

		if (bmCCI)
		{
			// check common checkin demands
			if (prpDem->IsCommonCheckin())
			{
				CStringArray *polFlights = NULL;
				if (ogDemData.omCCIMap.Lookup((void *)prpDem->Urno,(void *&)polFlights) == TRUE)
				{
					FLIGHTDATA *prlFlight = NULL;
					for (int i = 0; i < polFlights->GetSize(); i++)
					{
						long llUrno = atol(polFlights->GetAt(i));
						if (omFilterFlightUrnoMap.Lookup((void *)llUrno,(void *&)prlFlight) == TRUE)
						{
							blIsTempOk = true;							
						}
					}
				}
				else
				{
					blIsTempOk = true;
				}
			}
		}

	}
 
	if(blIsTempOk)
	{
		CString olUrno;
		CString olUrnoList;
		blIsTempOk = false;
		bool blPersFct = false;
		bool blPersQua = false;

		if(bmPersonal && (imViewerFiterIdx < FUNCTION || imViewerFiterIdx > QUALIFICATION))
		{
			imViewerFiterIdx = FUNCTION;
		}

		if(	bmPersonal && prpDem->IsPersonal)
		{
			if(imViewerFiterIdx == FUNCTION && prpDem->HasPfc)
			{
				if(bmUseAllPrf)
				{
					blPersFct = true;
				}
				else
				{
					olUrnoList = ogDemData.GetPfcSingeUrnos(prpDem->Urud);
					for(int illc = 0; illc < omCMapForPrf.GetSize() && !blPersFct; illc++)
					{
						olUrno = omCMapForPrf[illc]  + ",";
						blPersFct = (olUrnoList.Find(olUrno) > -1);
					}
				}
			}
			if(imViewerFiterIdx == QUALIFICATION && prpDem->HasPer)
			{
				if(bmUseAllRpq)
				{
					blPersQua = true;
				}
				else
				{
					olUrnoList = ogDemData.GetPerSingeUrnos(prpDem->Urud);
					for(int illc = 0; illc < omCMapForRpq.GetSize() && !blPersQua; illc++)
					{
						olUrno = omCMapForRpq[illc]  + ",";
						blPersQua = (olUrnoList.Find(olUrno) > -1)?true:false;
					}
				}
			}
			
			blIsTempOk = blPersFct || blPersQua;
		}//
		

	
		bool blPersReq = false;
		if(	bmEquipment && prpDem->IsEquipment)
		{
			if(bmUseAllReq)
			{
				if (prpDem->HasReq)
					blPersReq = ogDemData.DemandHasEquipment(prpDem,omViewerFilterEquipmentType);
			}
			else
			{
				olUrnoList = ogDemData.GetReqSingeUrnos(prpDem->Urud);
				for(int illc = 0; illc < omCMapForReq.GetSize() && !blPersReq; illc++)
				{
					olUrno = omCMapForReq[illc]  + ",";
					blPersReq = (olUrnoList.Find(olUrno) > -1)?true:false;
				}
			}

			blIsTempOk = blPersReq;
		}	

		bool blPersCic = false;
		bool blPersPst = false;
		bool blPersGat = false;
		if(bmLocation && (imViewerFiterIdx < CHECKIN || imViewerFiterIdx > GATES))
		{
			imViewerFiterIdx = CHECKIN;
		}
		if(	bmLocation && prpDem->IsLocation)
		{
			if(imViewerFiterIdx == CHECKIN && prpDem->HasCic)
			{
				if(bmUseAllCic)
				{
					blPersCic = true;
				}
				else
				{
					olUrnoList = ogDemData.GetCicSingeUrnos(prpDem->Urud);
					for(int illc = 0; illc < omCMapForCic.GetSize() && !blPersCic; illc++)
					{
						olUrno = omCMapForCic[illc]  + ",";
						blPersCic = (olUrnoList.Find(olUrno) > -1)?true:false;
					}
				}
			}
			if(imViewerFiterIdx == POSITIONS && prpDem->HasPos)
			{
				if(bmUseAllPst)
				{
					blPersPst = true;
				}
				else
				{
					olUrnoList = ogDemData.GetPstSingeUrnos(prpDem->Urud);
					for(int illc = 0; illc < omCMapForPst.GetSize() && !blPersPst; illc++)
					{
						olUrno = omCMapForPst[illc]  + ",";
						blPersPst = (olUrnoList.Find(olUrno) > -1)?true:false;
					}
				}
			}
			if(imViewerFiterIdx == GATES && prpDem->HasGat)
			{
				if(bmUseAllGat)
				{
					blPersGat = true;
				}
				else
				{
					olUrnoList = ogDemData.GetGatSingeUrnos(prpDem->Urud);
					for(int illc = 0; illc < omCMapForGat.GetSize() && !blPersGat; illc++)
					{
						olUrno = omCMapForGat[illc]  + ",";
						blPersGat = (olUrnoList.Find(olUrno) > -1)?true:false;
					}
				}
			}
			blIsTempOk = (blPersCic || blPersPst || blPersGat);

		}
		
	}

	return blIsTempOk;
	

	CCS_CATCH_ALL
	return false;

}

void CoverageDiagramViewer::LoadGhdWindow()
{
	CCS_TRY
	CMapPtrToPtr olTmpDemUrnoMap;
	olTmpDemUrnoMap.RemoveAll();
	omDemUrnoMap.RemoveAll();
	omAssignDemUrnoMap.RemoveAll();

	POSITION rlPos;

	// clear flight demand map
	CMapPtrToPtr *polDemMap;
	for ( rlPos =  omFlightDemMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omFlightDemMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& ) polDemMap);
		polDemMap->RemoveAll();
		delete polDemMap;
	}
	
	omFlightDemMap.RemoveAll();


	omRealDemUrnoMap.RemoveAll(); 
	ogDemData.GetAllDems(olTmpDemUrnoMap);
	for ( rlPos = olTmpDemUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		DEMDATA *prlDem;
		olTmpDemUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);
		if(prlDem != NULL && IsPassDemFilter(prlDem))
		{
			omDemUrnoMap.SetAt((void *)prlDem->Urno,prlDem);
			if(prlDem->IsPersonal && prlDem->HasPfc)
			{
				omAssignDemUrnoMap.SetAt((void *)prlDem->Urno,prlDem);
			}

		}
	}

	pomAttachWnd->SendMessage(WM_UPDATEDEMANDBTN,0,0);

	CCS_CATCH_ALL
}


bool CoverageDiagramViewer::IsPassFilter(FLIGHTDATA *prpFlight)
{
	void *p;

	bool blIsActOk	= false;
	bool blIsPosOk	= false;
	bool blIsAptOk	= false;
	bool blIsAltOk	= false;

	bool blTime	= false;

	bool blFtypOk = false;
	bool blDemOk  = false;

	if (prpFlight == 0)
		return false;

	if (bmUseAllAct == false)
	{
		blIsActOk = (omCMapForAct.Lookup(CString(prpFlight->Act3),p) == TRUE)?true:false;
		if(!blIsActOk)
		{
			blIsActOk = (omCMapForAct.Lookup(CString(prpFlight->Act5),p) == TRUE)?true:false;
		}

	}
	else
	{
		blIsActOk = true;
	}

	if (bmUseAllPos == false)
	{

		if(strcmp(prpFlight->Adid, "A") == 0)
		{
			blIsPosOk = (omCMapForPos.Lookup(CString(prpFlight->Psta),p) == TRUE)?true:false;
		}
		if(strcmp(prpFlight->Adid, "D") == 0)
		{
			blIsPosOk = (omCMapForPos.Lookup(CString(prpFlight->Pstd),p) == TRUE)?true:false;
		}

	}
	else
	{
		blIsPosOk = true;
	}


	if (bmUseAllApt == false)
	{
		if(bmOrigin)
		{
			blIsAptOk = (omCMapForApt.Lookup(CString(prpFlight->Org3),p) == TRUE)?true:false;
			if(!blIsAptOk)
			{
				blIsAptOk = (omCMapForApt.Lookup(CString(prpFlight->Org4),p) == TRUE)?true:false;
			}
		}
		if (bmDestination == true && blIsAptOk == false)
		{
			blIsAptOk = (omCMapForApt.Lookup(CString(prpFlight->Des3),p) == TRUE)?true:false;
			if(!blIsAptOk)
			{
				blIsAptOk = (omCMapForApt.Lookup(CString(prpFlight->Des4),p) == TRUE)?true:false;
			}

		}
	}
	else
	{
		blIsAptOk = TRUE;
	}


	if (bmUseAllAlt == false)
	{
		blIsAltOk = (omCMapForAlt.Lookup(CString(prpFlight->Alc2),p)== TRUE)?true:false;
		if(!blIsAltOk)
		{
			blIsAltOk = (omCMapForAlt.Lookup(CString(prpFlight->Alc3),p)== TRUE)?true:false;
		}	
	}
	else
	{
		blIsAltOk = TRUE;
	}


	if (bmArrival == true)
	{
		if (IsBetween(prpFlight->Tifa,omLoadTimeStart,omLoadTimeEnd))
		{
			blTime = true;
		}
	}

	if (bmDepature == true)
	{
		if (IsBetween(prpFlight->Tifd,omLoadTimeStart,omLoadTimeEnd))
		{
			blTime = true;
		}
	}

	if (bmArrival == true)
	{
		if (strcmp(prpFlight->Adid,"D") == 0)
		{
			if (ogFlightData.GetJoinFlight(prpFlight))
			{
				blDemOk = true;
			}
		}
		else
		{
			blDemOk = true;
		}
	}

	if (bmDepature == true)
	{
		if (strcmp(prpFlight->Adid,"A") == 0)
		{
			if (ogFlightData.GetJoinFlight(prpFlight))
			{
				blDemOk = true;
			}
		}
		else
		{
			blDemOk = true;
		}
	}

	CString olPossibleVal;
	if (bmBetrieb == true)
	{
		olPossibleVal += "O,R,D";
	}

	if (bmPlanung == true)
	{
		olPossibleVal += "S";
	}
	
	if (bmCancelled == true)
	{
		olPossibleVal += "X";
	}

	if (bmNoop == true)
	{
		olPossibleVal += "N";
	}

	CString olFTyp = prpFlight->Ftyp;
	if (olPossibleVal.Find(prpFlight->Ftyp) > -1)
	{
		blFtypOk = true;
	}

	return (blTime  && blDemOk && blFtypOk && blIsPosOk && blIsActOk && blIsAptOk && blIsAltOk);

}

void CoverageDiagramViewer::FilterFlights()
{
	omFilterFlightUrnoMap.RemoveAll();
		
	int ilFlightCount = ogCovFlightData.omData.GetSize();
	for(int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		FLIGHTDATA *prlFlight = &ogCovFlightData.omData[ilLc];
			
		if(prlFlight != NULL)
		{
			if(IsPassFilter(prlFlight) )
			{
				omFilterFlightUrnoMap.SetAt((void *)prlFlight->Urno,prlFlight);
			}
		}
	}
}

bool CoverageDiagramViewer::FilterDemand(DEMDATA *prpDem,bool bpUpdateAllowed)
{
	bool blUpdate = false;
	bool blPassFilter = IsPassDemFilter(prpDem);

	void *prlVoid;
	if (!omDemUrnoMap.Lookup((void *)prpDem->Urno,prlVoid))
	{
		if (blPassFilter)
		{
			blUpdate = true;
			if (bpUpdateAllowed)
			{
				omDemUrnoMap.SetAt((void *)prpDem->Urno,prpDem);
				if (prpDem->IsPersonal && prpDem->HasPfc)
				{
					omAssignDemUrnoMap.SetAt((void *)prpDem->Urno,prpDem);
				}
			}
		}
	}
	else if (!blPassFilter)
	{
		blUpdate = true;
		if (bpUpdateAllowed)
		{
			omDemUrnoMap.RemoveKey((void *)prpDem->Urno);		
			if (prpDem->IsPersonal && prpDem->HasPfc)
			{
				omAssignDemUrnoMap.RemoveKey((void *)prpDem->Urno);
			}
		}
	}

	return blUpdate;
}

bool CoverageDiagramViewer::DeleteDemand(DEMDATA *prpDem,bool bpUpdateAllowed)
{
	bool blUpdate = false;
	void *prlVoid;

	// deleted demand already recognized ?
	if (!ogDemData.omDeletedUrnoMap.Lookup((void *)prpDem->Urno,prlVoid))
	{
		return blUpdate;
	}

	if (omDemUrnoMap.Lookup((void *)prpDem->Urno,prlVoid))
	{
		blUpdate = true;
		if (bpUpdateAllowed)
		{
			omDemUrnoMap.RemoveKey((void *)prpDem->Urno);		
			if (prpDem->IsPersonal && prpDem->HasPfc)
			{
				omAssignDemUrnoMap.RemoveKey((void *)prpDem->Urno);
			}
		}
	}

	if (bpUpdateAllowed)
	{
		ogDemData.omDeletedUrnoMap.RemoveKey((void *)prpDem->Urno);
	}

	return blUpdate;
}

void CoverageDiagramViewer::SetFilterIdx(FilterType ipFilterIdx)
{
	imViewerFiterIdx = ipFilterIdx;
	LoadGhdWindow();

}

FilterType CoverageDiagramViewer::GetFilterIdx()
{
	return imViewerFiterIdx;
}

void CoverageDiagramViewer::SetLoadTimes(CTime opStart,CTime opEnd)
{
	CCS_TRY

		omLoadTimeStart = opStart;
		omStartShowTime = opStart;
		omLoadTimeEnd = opEnd;

	CCS_CATCH_ALL
}

void CoverageDiagramViewer::SetGeometryTimeSpan(CTimeSpan opGeometryTimeSpan)
{
	CCS_TRY

		omGeometryTimeSpan = opGeometryTimeSpan;

	CCS_CATCH_ALL
}

void CoverageDiagramViewer::SetGeometryValues(bool bpGeometryMinimize,bool bpGeometryTimeLines, int opGeometryFontIndex )
{
	CCS_TRY

		bmGeometryMinimize = bpGeometryMinimize;
		bmGeometryTimeLines = bpGeometryTimeLines;
		omGeometryFontIndex = opGeometryFontIndex;
	CCS_CATCH_ALL
}

	
void CoverageDiagramViewer::SetBorderVal(int ipVal)
{
	imBorderVal = ipVal;
}
void CoverageDiagramViewer::SetBorderFix(bool bpIsFix)
{
	bmIsBorderFix = bpIsFix;
}
int CoverageDiagramViewer::GetBorderVal()
{
	return imBorderVal;
}

bool CoverageDiagramViewer::IsBorderFix()
{
	return bmIsBorderFix;
}

void CoverageDiagramViewer::SetMaxVal(int ipVal)
{
	imMaxVal = ipVal;
}
void CoverageDiagramViewer::SetMinVal(int ipVal)
{
	imMinVal = ipVal;
}

int CoverageDiagramViewer::GetMaxVal()
{
	return imMaxVal;
}
int CoverageDiagramViewer::GetMinVal()
{
	return imMinVal;
}

void CoverageDiagramViewer::SetSimulationFlag(bool blFlag)
{
	bmSimulate = blFlag;	
}

bool CoverageDiagramViewer::GetSimulationFlag()
{
	return bmSimulate;	
}

void CoverageDiagramViewer::SetActuellState(bool bpState)
{
	bmActuellState = bpState;
}
bool CoverageDiagramViewer::GetActuellState()
{
	return bmActuellState;
}


bool CoverageDiagramViewer::CheckBsdbyStdFilter(BASICSHIFTS * prpBsh)
{
	bool blRc = false;

	CString olMainFctc = prpBsh->Fctc;
	if(!olMainFctc.IsEmpty())
	{
		blRc = (omPfcCodeList.Find(olMainFctc) > -1)?true:false;
	}
	if(!blRc)
	{

		CString olBsdUrno;
		olBsdUrno.Format("%ld",prpBsh->RUrno);

		CString olTotalFctc = ogBCD.GetValueList("DEL", "BSDU", olBsdUrno, "FCTC") ;

		CStringArray olFctcs;
		ExtractItemList(olTotalFctc,&olFctcs,',');
		for(int ilFctc = 0; ilFctc < olFctcs.GetSize() && !blRc ;ilFctc++)
		{
			CString olSingleFctc = ",";
			olSingleFctc += olFctcs[ilFctc];
			olSingleFctc += ",";
			
			if(omPfcCodeList.Find(olSingleFctc) > -1)
			{
				blRc = true;
			}
		}
	}
	return blRc;
}



void  CoverageDiagramViewer::DeSelectBsh()
{

	POSITION rlPos;
	BASICSHIFTS *prlBsh;
	long llUrno;
	for ( rlPos = omBasicShiftUrnoMap.GetStartPosition(); rlPos != NULL; )
	{	
		omBasicShiftUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlBsh);
		if(prlBsh != NULL)
		{
			prlBsh->IsSelected = false;
		}
	}
	int ilLineNo = -1;
	LONG lParam = MAKELONG(ilLineNo, BASIC_SHIFT);
	
	pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,UD_UPDATEGROUP, lParam);
}

void CoverageDiagramViewer::PrepareTSRData()
{
	CString olTotalTsr;
	CString olTotalFlda;
	CString olTotalFldd;
	CString olTotalFldt;

	CString olActField;
	CString olUsedFields;
	CString olTmpTsr;
	CString olTplTsrString;
	CString olTplName;
	CString olFire;
	CString olType;
	CString olTsrUrno;
	CString olName;
	CString olDispIdx = "0";
	CStringArray olTmp1Array;
	CStringArray olTmp2Array;
	
	omTotalTsrArray.RemoveAll();

	int ilCount = ogBCD.GetDataCount("TPL");
	for(int ilTpl = 0; ilTpl < ilCount;ilTpl++)
	{
		olTotalFlda = ogBCD.GetField("TPL", ilTpl, "FLDA");
		olTotalFldd = ogBCD.GetField("TPL", ilTpl, "FLDD");
		olTotalFldt = ogBCD.GetField("TPL", ilTpl, "FLDT");
		olTplName = ogBCD.GetField("TPL", ilTpl, "TNAM");
		olTotalFlda.Replace((char)157,';');
		olTotalFldd.Replace((char)157,';');
		olTotalFldt.Replace((char)157,';');

		ExtractItemList(olTotalFlda,&olTmp1Array,';');
		for (int ilTmp1 = 0; ilTmp1 < olTmp1Array.GetSize();ilTmp1++)
		{
			ExtractItemList(olTmp1Array[ilTmp1],&olTmp2Array,'.');
			if(olTmp2Array.GetSize() > 2)
			{
				olActField = olTmp2Array[2];
				if(olUsedFields.Find(olActField) < 0)
				{
					olUsedFields += olActField;
					olType = ogBCD.GetFieldExt("TSR", "BFLD", "BTAB", olActField, "AFT", "TYPE");
					olFire = ogBCD.GetFieldExt("TSR", "BFLD", "BTAB", olActField, "AFT", "FIRE");
					olDispIdx = "0";
					if(olFire.GetLength() > 2)
					{
						if(olFire.GetAt(0) == '1' || ( olFire.GetAt(1) == '1' && olFire.GetAt(2) == '1'))
						{
							olDispIdx = "3";
						}
						else
						{
							if(olFire.GetAt(1) == '1')
							{
								olDispIdx = "1";
							}
							if(olFire.GetAt(2) == '1')
							{
								olDispIdx = "2";
							}
						}
					}

					//  olTmpTsr = olDispIdx + ","+olActField + "," + ogBCD.GetFieldExt("TSR", "BFLD", "BTAB", olActField, "AFT", "NAME") + ",";
					//  Use translation for TSR.NAME from TXTTAB, if existing
					olTsrUrno = ogBCD.GetFieldExt("TSR", "BFLD", "BTAB", olActField, "AFT", "URNO");
					if (!GetNameOfTSRRecord ( olTsrUrno, olName ) )
						TRACE("No name found in TXTTAB for TSR record URNO <%s> !\n", olTsrUrno );
					olTmpTsr = olDispIdx + ","+olActField + "," + olName + ",";				

					olTmpTsr += olType;
					if(!olTplTsrString.IsEmpty())
					{
						olTplTsrString += ";";
					}
					olTplTsrString += olTmpTsr;
				}
			}
		}
		int ilPos = omTplTsrArray.GetSize();

		omTplTsrArray.NewAt(ilPos,olTplTsrString);
		omTplTsrMap.SetAt(olTplName,&omTplTsrArray[ilPos]);
	


	}
	olTmp1Array.RemoveAll();
	olTmp2Array.RemoveAll();


	ilCount = ogBCD.GetDataCount("TSR");

	for (int ilLc = 0; ilLc < ilCount; ilLc++)
	{
		
		olType = ogBCD.GetField("TSR", ilLc, "TYPE");
		olFire = ogBCD.GetField("TSR", ilLc, "FIRE");
		olDispIdx = "0";
		if(olFire.GetLength() > 2)
		{
			if(olFire.GetAt(0) == '1' || ( olFire.GetAt(1) == '1' && olFire.GetAt(2) == '1'))
			{
				olDispIdx = "3";
			}
			else
			{
				if(olFire.GetAt(1) == '1')
				{
					olDispIdx = "1";
				}
				if(olFire.GetAt(2) == '1')
				{
					olDispIdx = "2";
				}
			}
		}
		//  olTotalTsr = olDispIdx + "," + ogBCD.GetFields("TSR", ilLc, "BFLD,NAME") + olType;
		//  Use translation for TSR.NAME from TXTTAB, if existing
		olActField = ogBCD.GetField("TSR", ilLc, "BFLD");
		olTsrUrno = ogBCD.GetField("TSR", ilLc, "URNO");
		if (!GetNameOfTSRRecord ( olTsrUrno, olName ) )
			TRACE("No name found in TXTTAB for TSR record URNO <%s> !\n", olTsrUrno );
		olTotalTsr = olDispIdx + "," + olActField + "," + olName + "," + olType;

		omTotalTsrArray.Add(olTotalTsr );

	}
}

const CStringArray *CoverageDiagramViewer::GetTotalTsrArray()
{
	return &omTotalTsrArray;
}

const CStringArray *CoverageDiagramViewer::GetFilterTsrArray()
{
	return &omFilterTsrArray;
}

const CMapPtrToPtr *CoverageDiagramViewer::GetFlightDemMap()
{
	return &omFlightDemMap;
}

bool CoverageDiagramViewer::IsBasicShiftValid(BASICSHIFTS *prpShift)
{
	bool blRet = true;
	if(!(prpShift->Vafr == TIMENULL && prpShift->Vato == TIMENULL))
	{
		if(prpShift->Vafr == TIMENULL)
		{
			if(prpShift->From + CTimeSpan(prpShift->DayOffSet,0,0,0) > prpShift->Vato)
			{
				blRet = false;
			}
		}
		else if(prpShift->Vato == TIMENULL)
		{
			if(prpShift->To + CTimeSpan(prpShift->DayOffSet,0,0,0) < prpShift->Vafr)
			{
				blRet = false;
			}
		}
		else if(IsOverlapped(prpShift->From + CTimeSpan(prpShift->DayOffSet,0,0,0), prpShift->To + CTimeSpan(prpShift->DayOffSet,0,0,0),prpShift->Vafr, prpShift->Vato) == FALSE)
		{
			blRet = false;
		}
	}
	return blRet;
}

void CoverageDiagramViewer::ChangeBsdBar(BASICSHIFTS *prpShift,COV_BARDATA *prpBar)
{

	prpBar->Delegations.DeleteAll();
	prpBar->Delegations2.DeleteAll();
	prpBar->Delegations3.DeleteAll();
	prpBar->Indicators.DeleteAll();
	prpBar->Urno = prpShift->Urno;
	prpBar->RUrno = prpShift->RUrno;
	prpBar->Type = BASIC_SHIFT;
    prpBar->Text = prpShift->Code + "/" + prpShift->Fctc;
	prpBar->StatusText = prpShift->From.Format("%H:%M - ") + prpShift->To.Format("%H:%M   ") + prpShift->Code;
    prpBar->StartTime = prpShift->From + CTimeSpan(prpShift->DayOffSet,0,0,0);
    prpBar->EndTime = prpShift->To + CTimeSpan(prpShift->DayOffSet,0,0,0);

	COV_INDICATORDATA rlIndicator;
	rlIndicator.StartTime = prpBar->StartTime;
	rlIndicator.EndTime = prpBar->EndTime;
	rlIndicator.Color = ogColors[YELLOW_IDX];

	prpBar->Indicators.New(rlIndicator);
	
	

	COV_DELEGATIONDATA rlDelegation;
	CCSPtrArray <RecordSet> olDelList;
	CString olBsdUrno;
	CTime olDate = prpBar->StartTime;
	olBsdUrno.Format("%ld",prpShift->RUrno);
	ogBCD.GetRecords("DEL", "BSDU", olBsdUrno, &olDelList );
	int ilStartIdx = ogBCD.GetFieldIndex("DEL", "DELF");
	int ilEndIdx = ogBCD.GetFieldIndex("DEL", "DELT");
	int ilFctcIdx = ogBCD.GetFieldIndex("DEL", "FCTC");
	for ( int illc = 0; illc < olDelList.GetSize(); illc++)
	{
		CString olStartTime = olDelList[illc].Values[ilStartIdx];
		CString olEndTime = olDelList[illc].Values[ilEndIdx];
		CString olFctc = olDelList[illc].Values[ilFctcIdx];
					
		CTime olTime = HourStringToDate(olStartTime, olDate);
		if(olTime < olDate)
		{
			olDate = prpBar->EndTime;
			olTime = HourStringToDate(olStartTime, olDate);
		}
		rlDelegation.StartTime = olTime;
		olTime = HourStringToDate(olEndTime, olDate);
		rlDelegation.EndTime = olTime;
		rlDelegation.Text = olFctc;
		rlDelegation.Color = ogColors[SILVER_IDX];
		prpBar->Delegations.New(rlDelegation);
	}
	olDelList.DeleteAll();
}


void CoverageDiagramViewer::ReloadAllDems()
{
	ogBackTrace += "3,";
	ogDemData.ReleaseDeletedData();
	ogBackTrace += "4,";
	FilterFlights();
	ogBackTrace += "5,";
	LoadGhdWindow();
	ogBackTrace += "6,";
	pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
	ogBackTrace += "7,";
	pomAttachWnd->SendMessage(WM_UPDATEDEMANDBTN, 0, 0);
	ogBackTrace += "3e,";
}


void CoverageDiagramViewer::AlterSdtCount(int ilOldCount ,int ilNewCount, long llKeyUrno)
{

	CMapPtrToPtr *polShiftDemMap;   				
	
	int ilDiff = ilNewCount - ilOldCount;
	bool blDeleteAll = (ilNewCount == 0)?true:false;

	if (omShiftsDemandMap.Lookup((void*)llKeyUrno,(void *& )polShiftDemMap) == TRUE)
	{
		if (ilDiff > 0)
		{
			SDGDATA *prlSdg = GetCurrentDemand(SHIFT_DEMAND);
			SDTDATA *prlSdt = ogSdtData.GetSdtByUrno(llKeyUrno);

			if (prlSdt != NULL )
			{
				BASICSHIFTS rlBsh;
				BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(prlSdt->Bsdu);
				rlBsh.From = prlSdt->Esbg;	
				rlBsh.To = prlSdt->Lsen;
				MakeBshFromBsdData(&rlBsh,prlBsd);

				CWaitCursor olDummy;

				for(int i = 0; i < ilDiff; i++)
				{
					if (GetActuellState())
					{
						ogDataSet.CreateSdtFromBsd(&rlBsh,prlSdt->Fctc);
					}
					else
					{
						if (prlSdg != NULL)
						{
							ogDataSet.CreateMsdFromBsdAndSdg(&rlBsh, prlSdg->Urno, rlBsh.From, rlBsh.To, prlSdt->Fctc, false);
							// check validity of pointer due to broadcast handling in function above !
							prlSdg = GetCurrentDemand(SHIFT_DEMAND);
							prlSdt = ogSdtData.GetSdtByUrno(llKeyUrno);

							if (prlSdg != NULL)
							{
								bmSdgChanged = !prlSdg->Expd;
							}
						}
					}
				}
				if(!GetActuellState())
				{
					ogMsdData.Release();
				}
				SetSdtFlag(true);
			}
		}
		if ( ilDiff < 0)
		{
			int ilDelCount = 0;
			bool blFirst = true;
			SDGDATA *prlSdg = GetCurrentDemand(SHIFT_DEMAND);

			POSITION rlPos;
			SDTDATA *prlSdt = NULL;
			for ( rlPos =  polShiftDemMap->GetStartPosition(); rlPos != NULL && (ilDiff < ilDelCount);)
			{
				long llUrno;
				polShiftDemMap->GetNextAssoc(rlPos, (void *&)llUrno,(void *& ) prlSdt);
				if (prlSdt != NULL)
				{
					if (blDeleteAll || prlSdt->Urno != llKeyUrno)
					{

						if(GetActuellState())
						{
							ogSdtData.DeleteSdt(prlSdt,true,false);
						}
						else
						{
							CTime olStartTime = prlSdt->Sbgi;
							if(olStartTime == TIMENULL)
							{
								olStartTime = prlSdt->Esbg;
							}

							//VERIFY(prlSdg != NULL && prlSdg->Urno == prlSdt->Sdgu);
							if (!ogDataSet.DeleteMsdByMsduAndSdgu(prlSdt->MsdUrno,prlSdt->Sdgu,olStartTime,blFirst,false))
							{
								break;
							}
							else
							{
								blFirst = false;
								bmSdgChanged = !prlSdg->Expd;
							}

						}

						ilDelCount--;
					}
				}		
			}
			if(!GetActuellState())
			{
				ogMsdData.Release();
			}

			SetSdtFlag(true);

		}

		polShiftDemMap->RemoveAll();
		omShiftsDemandMap.RemoveKey((void *)llKeyUrno);
		delete polShiftDemMap;
	

	}
	
}


/****Hier stehen Funktionen die f�r die Automatischen Einteilung verwendet werden*/

void CoverageDiagramViewer::AutoAssign(int ipDirection,int ipShiftSelection,int ipShiftDeviation,
									   int ipAdaptIterations,int ilAdaption,int ipFactor,
									   int ipMinShiftCoverage,bool bpUseBaseCoverage,
										CTime opStartTime, CTime opEndTime)
{
	CCS_TRY
	CStringArray olFunctions;


	CWaitCursor olWait;

	CACProgressBar * polACProgressBar = NULL;
	int ilStartIdx = 0;

	CUIntArray olAutoDemandValues;
	CUIntArray olCompleteAutoDemandValues;
	CUIntArray olRealShiftValues;

	CAutoCoverage *prlAutoCov = NULL;
	POSITION rlPos;
	CString olFct;
	bool	blUseSmoothValues = false;
	long llDuration = (opEndTime - opStartTime).GetTotalMinutes();
	long llCompleteDuration = (omLoadTimeEnd - omLoadTimeStart).GetTotalMinutes();

	omAutoCovArray.DeleteAll();


	if( pogProgressBarDlg == NULL)
	{
		pogProgressBarDlg = new CACProgressBar(NULL,ipAdaptIterations +1);
	}

	// demands (in the demand curve) can have different functions - find out how how 
	// many different functions there are and write to olFunctions
	DistinctFunctions(olFunctions);

	bmAutoCovIsActive = true;
	// seperate the different demands so that we have multiple curves each with a single function
	/*Todo Array von CAutoCoverage mit l�nge olFunctions.GetSize() mit new erzeugen*/
	for(int illc = 0; illc < olFunctions.GetSize(); illc++)
	{
		olFct = olFunctions[illc];
		CTimeSpan olDemandDuration;

		// olAutoDemandValues contains a demand value for each minute of the assignment duration
		olAutoDemandValues.RemoveAll();

		// initialize each minute to zero
		olAutoDemandValues.InsertAt(0,0,llDuration);

		// olCompleteAutoDemandValues contains a demand value for each minute of the assignment duration
		olCompleteAutoDemandValues.RemoveAll();

		// initialize each minute to zero
		olCompleteAutoDemandValues.InsertAt(0,0,llCompleteDuration);

		// 
		for ( rlPos =  omAssignDemUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			DEMDATA *prlDem;
			omAssignDemUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);

			// if this demand has the correct function
			if(olFct == ogDemData.GetPfcSingeUrnos(prlDem->Urud))
			{
				// simulation active and must be used for calculation ?
				if (this->bmSimulate && this->imSimDisplay == 0)
				{
					// and this demand is within the allocation period
					if(prlDem->DispDebe < this->omLoadTimeEnd && prlDem->DispDeen > this->omLoadTimeStart)
					{
						if (prlDem->DispDebe <= this->omLoadTimeStart)
						{
							ilStartIdx = 0;
						}
						else
						{
							CTimeSpan TS = prlDem->DispDebe - this->omLoadTimeStart;
							ilStartIdx = TS.GetTotalMinutes(); 
						}
						if(prlDem->DispDeen < this->omLoadTimeEnd)
						{
							olDemandDuration = (ilStartIdx==0) ? prlDem->DispDeen - omLoadTimeStart : prlDem->DispDeen - prlDem->DispDebe;
						}
						else
						{
							olDemandDuration = (ilStartIdx==0) ? omLoadTimeEnd - omLoadTimeStart : omLoadTimeEnd - prlDem->DispDebe;
						}
						int ilCount = olDemandDuration.GetTotalMinutes();
						int ilValueCount = olCompleteAutoDemandValues.GetSize();
						int ilCnt = 0;
						for(int j = 0; j < ilCount; j++)
						{
							if(j+ilStartIdx < ilValueCount)
							{					
								{
									// calculate olCompleteAutoDemandValues - integer list of how many dems there are per minute
									olCompleteAutoDemandValues[j+ilStartIdx]++;						
								}
							}
						}
					}

				}
				else
				{
					// and this demand is within the allocation period
					if(prlDem->DispDebe < opEndTime && prlDem->DispDeen > opStartTime)
					{
						if(prlDem->DispDebe <= opStartTime)
						{
							ilStartIdx = 0;
						}
						else
						{
							CTimeSpan TS = prlDem->DispDebe - opStartTime;
							ilStartIdx = TS.GetTotalMinutes(); 
						}
						if(prlDem->DispDeen < opEndTime)
						{
							olDemandDuration = (ilStartIdx==0) ? prlDem->DispDeen - opStartTime : prlDem->DispDeen - prlDem->DispDebe;
						}
						else
						{
							olDemandDuration = (ilStartIdx==0) ? opEndTime - opStartTime : opEndTime - prlDem->DispDebe;
						}
						int ilCount = olDemandDuration.GetTotalMinutes();
						int ilValueCount = olAutoDemandValues.GetSize();
						int ilCnt = 0;
						for(int j = 0; j < ilCount; j++)
						{
							if(j+ilStartIdx < ilValueCount)
							{					
								{
									// calculate olAutoDemandValues - integer list of how many dems there are per minute
									olAutoDemandValues[j+ilStartIdx]++;						
								}
							}
						}
					}
				}
			}
		}
		
		// must we keep any curve simulation in mind ?
		if (this->bmSimulate && this->imSimDisplay == 0)
		{
			CUIntArray olCompleteAutoDemandCurve;
			olCompleteAutoDemandCurve.Copy(olCompleteAutoDemandValues);

			switch(this->imSimVariant)
			{
			case 0:	
				pomCoverageWnd->CalculateSimOffsetFlightDemands(olCompleteAutoDemandCurve,olCompleteAutoDemandValues);
			break;
			case 1:	
				pomCoverageWnd->CalculateSimCutFlightDemands(olCompleteAutoDemandCurve,olCompleteAutoDemandValues);
			break;
			case 2:	
				pomCoverageWnd->CalculateSimSmoothFlightDemands(olCompleteAutoDemandCurve,olCompleteAutoDemandValues);
			break;
			}

			// now move the resulting area 
			ilStartIdx = (opStartTime - omLoadTimeStart).GetTotalMinutes();
			for (int j = 0; j < llDuration; j++)
			{
				olAutoDemandValues[j] = olCompleteAutoDemandValues[j+ilStartIdx];
			}
		}

		// calculate olRealShiftValues (the existing shifts for the demand curve)
		DistinctShifts(atol(olFct),&olRealShiftValues,opStartTime,opEndTime,false);

		CString olFctc = ogPfcData.GetFctcByUrno(atol(olFct));
		prlAutoCov = new CAutoCoverage;
		if(prlAutoCov != NULL)
		{
			// array of CAutoCoverage objects - one for each demand curve (seperated by funtion)
			// olRealShiftValues - the existing shifts for the demand curve
			// omAutoCovShifts - shifts which have the correct function (or none) for the demand curve already adjusted with offsets etc.
			// omBaseShiftGroup - params defined in "Advanced Approximation"

			prlAutoCov->IgnoreBreaks(this->bmIgnoreBreak);
			prlAutoCov->InitValues(olAutoDemandValues,olRealShiftValues,omAutoCovShifts,&omBaseShiftGroup,olFctc);
			omAutoCovArray.Add(prlAutoCov);
		}

	}

	
	CBaseShiftsGroups	olLastValidShiftGroups(omBaseShiftGroup);
	CBaseShiftsGroups	olLastOptimShiftGroups(omBaseShiftGroup);

	bool blOverCover  = (ipShiftSelection == 0) ? true : false;
	bool blUnderCover = (ipShiftSelection == 1) ? true : false;

	pogProgressBarDlg->imTotalLoops = omAutoCovArray.GetSize() * ipAdaptIterations;
	for (int illc2 = 0; illc2 < omAutoCovArray.GetSize() ; illc2++)
	{
		bool blContinue = true;
		for (illc = 0; illc < ipAdaptIterations  && blContinue; illc++)
		{
			omBaseShiftGroup = olLastValidShiftGroups;

			bool blOptimal = omAutoCovArray[illc2].CalcCoverage(true,true,blOverCover,blUnderCover,(long) ipShiftDeviation, (double)ipFactor/100, ilAdaption,ipMinShiftCoverage,bpUseBaseCoverage);
			if (blOptimal)
				olLastOptimShiftGroups = omBaseShiftGroup;

			blContinue = pogProgressBarDlg->CheckContinue();
			if (blContinue)
				pogProgressBarDlg->imTotalLoops--;
			else
				pogProgressBarDlg->imTotalLoops -= ((ipAdaptIterations - 1) - illc);
		}

		olLastValidShiftGroups = olLastOptimShiftGroups;

	}

	if(pogProgressBarDlg != NULL)
	{
		pogProgressBarDlg->DestroyWindow();
		delete pogProgressBarDlg;	
		pogProgressBarDlg = NULL;
	}

	CStringArray olAutoCovShifts;
	olAutoCovShifts.RemoveAll();
	for (illc = 0; illc < omAutoCovArray.GetSize() ; illc++)
	{	
		omAutoCovArray[illc].GetAutoCovShifts(olAutoCovShifts);
	}

	GenerateAutoCovSdts(olAutoCovShifts,opStartTime);


	LoadSdtWindow();

	BreakPostOptimization(opStartTime,opEndTime,true);


	if (ogBasicData.DebugAutoCoverage())
	{
		CString olMsg;
		CString olFmt;

		for (int i = 0; i < this->omBaseShiftGroup.omBaseShiftsGroups.GetSize(); i++)
		{
			if (this->omBaseShiftGroup.omBaseShiftsGroups[i].UsePercent)
				olFmt.Format("Shift group [%ld], min = [%d], max = [%d], current = [%d] \n",this->omBaseShiftGroup.omBaseShiftsGroups[i].GroupIndex,(int)this->omBaseShiftGroup.omBaseShiftsGroups[i].MinPerc,(int)this->omBaseShiftGroup.omBaseShiftsGroups[i].MaxPerc,(int)this->omBaseShiftGroup.omBaseShiftsGroups[i].ActPerc);
			else
				olFmt.Format("Shift group [%ld], min = [%d], max = [%d], current = [%d] \n",this->omBaseShiftGroup.omBaseShiftsGroups[i].GroupIndex,(int)this->omBaseShiftGroup.omBaseShiftsGroups[i].MinAbs,(int)this->omBaseShiftGroup.omBaseShiftsGroups[i].MaxAbs,(int)this->omBaseShiftGroup.omBaseShiftsGroups[i].ActAbs);
			olMsg += olFmt;
		}

		::MessageBox(NULL,olMsg,"Shiftgroup coverage",MB_OK|MB_ICONINFORMATION);
	}

	CCS_CATCH_ALL
}

void CoverageDiagramViewer::DistinctFunctions(CStringArray &ropFunctions)
{
	CString olUrnoList;
	CString olUrnoList2;
	CString olAllFcts;
	POSITION rlPos;

	ropFunctions.RemoveAll();
	for ( rlPos =  omAssignDemUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		DEMDATA *prlDem;
		omAssignDemUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);
		
		olUrnoList = ogDemData.GetPfcSingeUrnos(prlDem->Urud);
		olUrnoList2 = ogDemData.GetPfcSingeUrnos(prlDem->Urud) + ";";
		if(olAllFcts.Find(olUrnoList2) < 0)
		{
			ropFunctions.Add(olUrnoList);
			olAllFcts += olUrnoList2;
		}
	}
}


void CoverageDiagramViewer::DistinctShifts(long lpUpfc,CUIntArray *ropShiftArray,CTime opStartTime, 
										   CTime opEndTime,bool bpIgnoreFunction,bool bpIgnoreBreak)
{
	CString olFctc = ogPfcData.GetFctcByUrno(lpUpfc);
	CString olSdtFctc = "";
	POSITION rlPos;

	long llDuration = (opEndTime - opStartTime).GetTotalMinutes();

	ropShiftArray->InsertAt(0,0,llDuration );

	for(rlPos = omSdtUrnoMap.GetStartPosition();rlPos!=NULL;)
	{
		SDTDATA *prlDem = NULL; //&pomViewer->omSdtData[ilIndex];
		long llUrno;
		omSdtUrnoMap.GetNextAssoc(rlPos,(void *&)llUrno,(void *&)prlDem);

		olSdtFctc = prlDem->Fctc;
		if(olSdtFctc == olFctc || bpIgnoreFunction)
		{
			int ilStartIdx = 0;
			CTimeSpan olDemandDuration = CTimeSpan(0,0,0,0);// = prlDem->Lsen - omStartTime;
			CTime olStartTime,olEndTime;

		//Teil 1: Die L�nge der Schicht an sich
			olStartTime = prlDem->Esbg;
			olEndTime   = prlDem->Lsen;


			if(olStartTime <= opStartTime && olEndTime < opEndTime) //ragt in sichbaren Bereich
			{
				
				olDemandDuration = olEndTime - opStartTime;
				ilStartIdx = 0;
			}
			else if(olStartTime > opStartTime && olEndTime <= opEndTime) // voll in sichbaren Bereich
			{
				olDemandDuration = olEndTime - olStartTime;
				CTimeSpan TS = olStartTime - opStartTime;
				ilStartIdx = TS.GetTotalMinutes(); 
			}
			else if(olStartTime > opStartTime && olEndTime > opEndTime) // ragt aus sichbaren Bereich
			{
				olDemandDuration = opEndTime - olStartTime;
				CTimeSpan TS = olStartTime - opStartTime;
				ilStartIdx = TS.GetTotalMinutes(); 
			}
			else if(olStartTime < opStartTime && olEndTime > opEndTime) // links/rechts aus Sichtbereich
			{
				olDemandDuration = opEndTime - opStartTime;
				CTimeSpan TS = opEndTime - opStartTime;
				ilStartIdx = 0;//TS.GetTotalMinutes(); 
			}


			//Und die Berechnung der Werte f�r Schicht
			int ilCount = olDemandDuration.GetTotalMinutes();
			int ilValueCount = ropShiftArray->GetSize();
			for(int j = 0; j < ilCount; j++)
			{
				if(j+ilStartIdx < ilValueCount)
				{
					(*ropShiftArray)[j+ilStartIdx]++;
				}
			}
			//Teil 2 Pausen ber�cksichtigen
			if(!bpIgnoreBreak)
			{
				olStartTime = prlDem->Brkf;
				olEndTime   = prlDem->Brkt;


				olDemandDuration = 0;
				ilStartIdx = 0;
				if(olStartTime <= opStartTime && olEndTime < opEndTime) //ragt in sichbaren Bereich
				{
					olDemandDuration = olEndTime - opStartTime;
					ilStartIdx = 0;
				}
				else if(olStartTime > opStartTime && olEndTime <= opEndTime) // voll in sichbaren Bereich
				{
					olDemandDuration = olEndTime - olStartTime;
					CTimeSpan TS = olStartTime - opStartTime;
					ilStartIdx = TS.GetTotalMinutes(); 
				}
				else if(olStartTime > opStartTime && olEndTime > opEndTime) // ragt aus sichbaren Bereich
				{
					olDemandDuration = opEndTime - olStartTime;
					CTimeSpan TS = olStartTime - opStartTime;
					ilStartIdx = TS.GetTotalMinutes(); 
				}
				else if(olStartTime < opStartTime && olEndTime > opEndTime) // links/rechts aus Sichtbereich
				{
					olDemandDuration = opEndTime - opStartTime;
					CTimeSpan TS = opEndTime - opStartTime;
					ilStartIdx = 0;//TS.GetTotalMinutes(); 
				}

				//Abziehen der Pause
				int ilCount = olDemandDuration.GetTotalMinutes();
				int ilValueCount = ropShiftArray->GetSize();
				for(int j = 0; j < ilCount; j++)
				{
					if(j+ilStartIdx < ilValueCount)
					{
						(*ropShiftArray)[j+ilStartIdx]--;
					}
				}
			}
		}
	}
}

void CoverageDiagramViewer::PrepareBaseShifts(CStringArray &ropBaseViews,CTime opAssignStart,CTime opAssignEnd,double dpFactor)
{
	CString olBaseViewText = "";
	CString olBaseView = "";
	int ilGroupID = 0;
	CStringArray olList;
    POSITION rlPos,rlPos2;
	CMapPtrToPtr *polBsdUrnoMap;
    bool blValidShiftFound;
    bool blShiftAdded;
	int	ilShiftStartOffSet = 0;
	int ilShiftEndOffSet = 0;	

	omAutoCovShifts.DeleteAll();

	omBaseShiftGroup.ResetData();

	// calculate total amount of working time currently not covered
	omBaseShiftGroup.SetTotalWorkingHours(this->pomCoverageWnd->CalculateTotalWorkingHours(opAssignStart,opAssignEnd) * dpFactor);

	for ( rlPos = omBasicShiftKeyMap.GetStartPosition(); rlPos != NULL; )
	{
		CString olKey;
		BASICSHIFTS *prlBasicShift;
		omBasicShiftKeyMap.GetNextAssoc(rlPos, olKey,(void *& ) polBsdUrnoMap);

		blValidShiftFound = false;
		prlBasicShift = NULL;
		if(polBsdUrnoMap != NULL)
		{
			for ( rlPos2 =  polBsdUrnoMap->GetStartPosition(); rlPos2 != NULL ; )
			{
				long  llUrno;
				blValidShiftFound = false;
				polBsdUrnoMap->GetNextAssoc(rlPos2, (void *& )llUrno,(void *& ) prlBasicShift);
				if(prlBasicShift != NULL)
				{
					blShiftAdded = false;
					
					for (int illc= 0; illc < ropBaseViews.GetSize() && !blShiftAdded;illc++)
					{
						olBaseViewText = ropBaseViews[illc];
						ExtractItemList(olBaseViewText,&olList,'|');
						if(olList.GetSize() >= 6)
						{
							olBaseView = olList[0];
							ilShiftStartOffSet = atoi(olList[4]);
							ilShiftEndOffSet = atoi(olList[5]);	

							PrepareBaseShiftsFilter(olBaseView);
							blValidShiftFound = IsBasicShiftValid(prlBasicShift);
							if(blValidShiftFound)
							{
								if(IsPassBshFilter(prlBasicShift)) 
								{
									blShiftAdded = true;
									AddAutoCovShifts(prlBasicShift,illc,opAssignStart,ilShiftStartOffSet,ilShiftEndOffSet);							
								}
							}
						}
					}
				}
			}						
		}
	}
			
	for (int illc= 0; illc < ropBaseViews.GetSize() ;illc++)
	{

		olBaseViewText = ropBaseViews[illc];
		ExtractItemList(olBaseViewText,&olList,'|');
		bool blUsePercent = true;
		double dlMinVal,dlMaxVal;

		if(olList.GetSize() >= 6)
		{
			if(olList[1] == "A")
			{
				blUsePercent = false;
			}
			dlMinVal = atof(olList[2]);
			dlMaxVal = atof(olList[3]);
			ilShiftStartOffSet = atoi(olList[4]);
			ilShiftEndOffSet = atoi(olList[5]);	

			omBaseShiftGroup.AddData(dlMinVal,dlMaxVal,blUsePercent,ilShiftStartOffSet,ilShiftEndOffSet);
		}
	}
		
}



void CoverageDiagramViewer::AddAutoCovShifts(BASICSHIFTS * prpBsh,int ipGroupIndex,
											 CTime opAssignStart,int ipShiftStartOffSet,int ipShiftEndOffSet)
{
	int ilLoadOffSet,ilStartOffSet,ilEndOffSet;
	CTimeSpan olBPStart,olBPEnd,olBreakStart,olBreakEnd;
	
	ilLoadOffSet = opAssignStart.GetHour()*60 + opAssignStart.GetMinute();
	ilStartOffSet = prpBsh->From.GetHour()*60 + prpBsh->From.GetMinute() + ipShiftStartOffSet;
	ilEndOffSet = prpBsh->To.GetHour()*60 + prpBsh->To.GetMinute() - ipShiftEndOffSet;

	if(ilStartOffSet >= ilEndOffSet)
	{
		ilStartOffSet -= ipShiftStartOffSet;
	}
	if(ilStartOffSet >= ilEndOffSet)
	{
		ilEndOffSet += ipShiftEndOffSet;
	}


	if(prpBsh->BlFrom < prpBsh->From)
	{
		olBPStart = 0;
		olBPEnd = 0;
		olBreakStart = 0;
		olBreakEnd = 0;
	}
	else
	{
		olBPStart = prpBsh->BlFrom -prpBsh->From;
		olBPEnd = prpBsh->BlTo -prpBsh->From;
		olBreakStart = prpBsh->BreakFrom -prpBsh->From;
		olBreakEnd = prpBsh->BreakTo -prpBsh->From;
	}

	if(ilStartOffSet > ilEndOffSet)
	{
		ilStartOffSet -= 1440;
	}

	// check, if we have a splitted shift (special case see design above)
	if (ilStartOffSet < ilLoadOffSet && ilEndOffSet > ilLoadOffSet)
	{
		// finally normalize shift values relative to load start !
		ilStartOffSet -= ilLoadOffSet;
		ilEndOffSet -= ilLoadOffSet;

		BASESHIFT *prlAutoCovBsh = new BASESHIFT;
		if (prlAutoCovBsh != NULL) 
		{
			prlAutoCovBsh->Code = prpBsh->Code;
			prlAutoCovBsh->Start = ilStartOffSet ;
			prlAutoCovBsh->End = ilEndOffSet;
			prlAutoCovBsh->RealStart = ilStartOffSet - ipShiftStartOffSet;
			prlAutoCovBsh->RealEnd = ilEndOffSet +ipShiftEndOffSet;
			prlAutoCovBsh->BRStart = ilStartOffSet + olBPStart.GetTotalMinutes();
			prlAutoCovBsh->BREnd = ilStartOffSet + olBPEnd.GetTotalMinutes();

			prlAutoCovBsh->BreakStart = ilStartOffSet + olBreakStart.GetTotalMinutes();
			prlAutoCovBsh->BreakEnd = ilStartOffSet + olBreakEnd.GetTotalMinutes();
			prlAutoCovBsh->Fctc = prpBsh->Fctc;
		
			prlAutoCovBsh->BshUrno = prpBsh->Urno;
			prlAutoCovBsh->GroupKey = ipGroupIndex;

			omAutoCovShifts.Add(prlAutoCovBsh);
		}

		// generate second shift
		ilStartOffSet += 1440;
		ilEndOffSet	  += 1440;

		prlAutoCovBsh = new BASESHIFT;
		if (prlAutoCovBsh != NULL) 
		{
			prlAutoCovBsh->Code = prpBsh->Code;
			prlAutoCovBsh->Start = ilStartOffSet ;
			prlAutoCovBsh->End = ilEndOffSet;
			prlAutoCovBsh->RealStart = ilStartOffSet - ipShiftStartOffSet;
			prlAutoCovBsh->RealEnd = ilEndOffSet +ipShiftEndOffSet;
			prlAutoCovBsh->BRStart = ilStartOffSet + olBPStart.GetTotalMinutes();
			prlAutoCovBsh->BREnd = ilStartOffSet + olBPEnd.GetTotalMinutes();

			prlAutoCovBsh->BreakStart = ilStartOffSet + olBreakStart.GetTotalMinutes();
			prlAutoCovBsh->BreakEnd = ilStartOffSet + olBreakEnd.GetTotalMinutes();
			prlAutoCovBsh->Fctc = prpBsh->Fctc;
		
			prlAutoCovBsh->BshUrno = prpBsh->Urno;
			prlAutoCovBsh->GroupKey = ipGroupIndex;

			omAutoCovShifts.Add(prlAutoCovBsh);
		}

	}
	else 
	{
		if (ilLoadOffSet > ilEndOffSet)
		{
			ilStartOffSet += 1440;
			ilEndOffSet += 1440;
		}

		// finally normalize shift values relative to load start !
		ilStartOffSet -= ilLoadOffSet;
		ilEndOffSet -= ilLoadOffSet;

		BASESHIFT *prlAutoCovBsh = new BASESHIFT;
		if (prlAutoCovBsh != NULL) 
		{
			prlAutoCovBsh->Code = prpBsh->Code;
			prlAutoCovBsh->Start = ilStartOffSet ;
			prlAutoCovBsh->End = ilEndOffSet;
			prlAutoCovBsh->RealStart = ilStartOffSet - ipShiftStartOffSet;
			prlAutoCovBsh->RealEnd = ilEndOffSet +ipShiftEndOffSet;
			prlAutoCovBsh->BRStart = ilStartOffSet + olBPStart.GetTotalMinutes();
			prlAutoCovBsh->BREnd = ilStartOffSet + olBPEnd.GetTotalMinutes();

			prlAutoCovBsh->BreakStart = ilStartOffSet + olBreakStart.GetTotalMinutes();
			prlAutoCovBsh->BreakEnd = ilStartOffSet + olBreakEnd.GetTotalMinutes();
			prlAutoCovBsh->Fctc = prpBsh->Fctc;
		
			prlAutoCovBsh->BshUrno = prpBsh->Urno;
			prlAutoCovBsh->GroupKey = ipGroupIndex;

			omAutoCovShifts.Add(prlAutoCovBsh);
		}
	}

}

void CoverageDiagramViewer::DiscardAutoCovChanges()
{
	ogSdtData.DeleteAutoSdts();
	bmAutoCovIsActive = false;
	LoadSdtWindow();
	ChangeViewToForHScroll(omStartTime,omEndTime);
	pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
	pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
}

void CoverageDiagramViewer::ReleaseAutoCovChanges()
{
	bmAutoCovIsActive = false;
	ogDataSet.ReleaseAutoSdts(!bmActuellState);
	LoadSdtWindow();
	ChangeViewToForHScroll(omStartTime,omEndTime);
	pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
	pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
}

void CoverageDiagramViewer::GenerateAutoCovSdts(CStringArray &opAutoCovShifts,CTime opAssignStart)
{
	CString olAutoCovShifts;
	BASICSHIFTS *prpBsh = NULL;
	CTime olShiftStart,olShiftEnd;
	CStringArray olList;
	long llBshUrno,llSdgu = 0;
	int ilStart,ilEnd,ilBOffSet;
	CString olFctc;
	int ilShiftStartOffSet = 0;
	int ilShiftEndOffSet   = 0;

#ifdef	_DEBUG_AUTOCOVERAGE	
	FILE *fp = fopen(CCSLog::GetTmpPath("\\AutoCoverage.lis"),"wt");
#endif
	if(bmActuellState)
	{
		llSdgu = 0;
	}
	else
	{
		llSdgu = lmSdgu;
	}
	for(int illc = 0; illc < opAutoCovShifts.GetSize();illc++)
	{
		olAutoCovShifts = opAutoCovShifts[illc];
		ExtractItemList(olAutoCovShifts,&olList,'|');

		if(olList.GetSize() == 7)
		{
			llBshUrno = atol(olList[0]);
			ilStart = atoi(olList[1]);
			ilEnd = atoi(olList[2]);
			ilBOffSet = atoi(olList[3]);
			olFctc = olList[4];
			ilShiftStartOffSet = atoi(olList[5]);
			ilShiftEndOffSet   = atoi(olList[6]);

			olShiftStart = opAssignStart + CTimeSpan(0,0,ilStart-ilShiftStartOffSet,0) ;
			olShiftEnd = opAssignStart + CTimeSpan(0,0,ilEnd+ilShiftEndOffSet,0);


			prpBsh = GetBasicShiftByUrno(llBshUrno);
			if(prpBsh != NULL)
			{
				ogDataSet.CreateAutoSdtByBsh(prpBsh,olFctc,olShiftStart,olShiftEnd,ilBOffSet,llSdgu);
#ifdef	_DEBUG_AUTOCOVERAGE	
				fprintf(fp,"BSD=[%s],FCTC=[%s],Start=[%s],End=[%s]\n",prpBsh->Code,olFctc,olShiftStart.Format("%d.%m.%Y %H:%M"),olShiftEnd.Format("%d.%m.%Y %H:%M"));
#endif
			}
		}
	}

#ifdef	_DEBUG_AUTOCOVERAGE	
	fclose(fp);
#endif
	
}

// second optimization of breaks
void CoverageDiagramViewer::BreakPostOptimization(CTime opStartTime,CTime opEndTime,bool bpOnlyAutoSdt)
{

	CTimeSpan olDemandDuration;
	CUIntArray olAutoDemandValues;
	CUIntArray olRealShiftValues;
	CStringArray olFunctions;
	CString olFct;

	int ilStartIdx;
	POSITION rlPos;
	long llDuration = (opEndTime - opStartTime).GetTotalMinutes();

	// must check each function curve separetly
	DistinctFunctions(olFunctions);

	for (int i = 0; i < olFunctions.GetSize(); i++)
	{
		olFct = olFunctions[i];
		olAutoDemandValues.RemoveAll();
		olAutoDemandValues.InsertAt(0,0,llDuration);

		for ( rlPos =  omAssignDemUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			DEMDATA *prlDem;
			omAssignDemUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);

			// if this demand has the correct function
			if(olFct == ogDemData.GetPfcSingeUrnos(prlDem->Urud))
			{
				if(prlDem->DispDebe < opEndTime && prlDem->DispDeen > opStartTime)
				{
					if(prlDem->DispDebe <= opStartTime)
					{
						ilStartIdx = 0;
					}
					else
					{
						CTimeSpan TS = prlDem->DispDebe - opStartTime;
						ilStartIdx = TS.GetTotalMinutes(); 
					}

					if(prlDem->DispDeen < opEndTime)
					{
						olDemandDuration = (ilStartIdx==0) ? prlDem->DispDeen - opStartTime : prlDem->DispDeen - prlDem->DispDebe;
					}
					else
					{
						olDemandDuration = (ilStartIdx==0) ? opEndTime - opStartTime : opEndTime - prlDem->DispDebe;
					}

					int ilCount = olDemandDuration.GetTotalMinutes();
					int ilValueCount = olAutoDemandValues.GetSize();
					int ilCnt = 0;
					for(int j = 0; j < ilCount; j++)
					{
						if(j+ilStartIdx < ilValueCount)
						{					
							{
								olAutoDemandValues[j+ilStartIdx]++;						
							}
						}
					}
				}
			}
		}

		DistinctShifts(atol(olFct),&olRealShiftValues,opStartTime,opEndTime,/* bpIgnoreFunction = */false,/* bpIgnoreBreak = */false);
		CString olFctc = ogPfcData.GetFctcByUrno(atol(olFct));

		// since we can't move the breaks of any shift, we must add the breaks for the ones we can move only
		for(rlPos = omSdtUrnoMap.GetStartPosition();rlPos!=NULL;)
		{
			SDTDATA *prlSdt = NULL; 
			long llUrno;
			bool blIsValid = true;
			omSdtUrnoMap.GetNextAssoc(rlPos,(void *&)llUrno,(void *&)prlSdt);


			if (bpOnlyAutoSdt)
			{
				blIsValid = prlSdt->IsAutoSdt;
				blIsValid = blIsValid && prlSdt->Fctc == olFctc;
			}
			else
			{
				blIsValid = prlSdt->StatPlSt[0] == 'A';	// only active ROSS can be moved !
				blIsValid = blIsValid && prlSdt->Fctc == olFctc;
			}

			if (prlSdt->Bkd1 > 0 && (prlSdt->Esbg < opEndTime && prlSdt->Lsen > opStartTime) && blIsValid)
			{
				int ilBreakDuration = prlSdt->Bkd1;
				int ilActBreakStart = max((prlSdt->Brkf - opStartTime).GetTotalMinutes(),0);
				int ilActBreakEnd = max((prlSdt->Brkt - opStartTime).GetTotalMinutes(),0);
				int ilActBreakRangeStart = max((prlSdt->Bkf1 - opStartTime).GetTotalMinutes(),0);
				int ilActBreakRangeEnd = max((prlSdt->Bkt1 - opStartTime).GetTotalMinutes(),0);

				if (ilActBreakEnd > 0 && ilActBreakRangeStart < llDuration)
				{
					// add current break to coverage curve
					for (int illc = max(0,ilActBreakStart); illc < min(ilActBreakEnd,olRealShiftValues.GetSize()); illc++)
					{
						olRealShiftValues[illc]++;
					}
				}			
			}
		}


		for(rlPos = omSdtUrnoMap.GetStartPosition();rlPos!=NULL;)
		{
			SDTDATA *prlSdt = NULL; 
			long llUrno;
			bool blIsValid = true;
			omSdtUrnoMap.GetNextAssoc(rlPos,(void *&)llUrno,(void *&)prlSdt);

			if (bpOnlyAutoSdt)
			{
				blIsValid = prlSdt->IsAutoSdt;
				blIsValid = blIsValid && prlSdt->Fctc == olFctc;
			}
			else
			{
				blIsValid = prlSdt->StatPlSt[0] == 'A';	// only active ROSS can be moved !
				blIsValid = blIsValid && prlSdt->Fctc == olFctc;
			}

			if (prlSdt->Bkd1 > 0 && (prlSdt->Esbg < opEndTime && prlSdt->Lsen > opStartTime) && blIsValid)
			{
				prlSdt->IsAutoSdt = true; 
				int ilBreakDuration = prlSdt->Bkd1;
				int ilActBreakStart = max((prlSdt->Brkf - opStartTime).GetTotalMinutes(),0);
				int ilActBreakEnd = max((prlSdt->Brkt - opStartTime).GetTotalMinutes(),0);
				int ilActBreakRangeStart = max((prlSdt->Bkf1 - opStartTime).GetTotalMinutes(),0);
				int ilActBreakRangeEnd = max((prlSdt->Bkt1 - opStartTime).GetTotalMinutes(),0);

				if (ilActBreakEnd > 0 && ilActBreakRangeStart < llDuration)
				{
					// now calculate new optimized break position
					int ilBreakOffSet = CalcNewBreakOffSet(ilActBreakRangeStart,ilActBreakRangeEnd,ilBreakDuration,olAutoDemandValues,olRealShiftValues);

					int ilNewBreakStart = ilActBreakRangeStart + ilBreakOffSet;
					int ilNewBreakEnd = ilActBreakRangeStart + ilBreakOffSet + ilBreakDuration;

					prlSdt->Brkf = opStartTime + CTimeSpan(0,0,ilNewBreakStart,0);
					prlSdt->Brkt = prlSdt->Brkf + CTimeSpan(0,0,ilBreakDuration,0);

					// remove new optimized break from coverage curve
					for (int illc = max(0,ilNewBreakStart); illc < min(ilNewBreakEnd,olRealShiftValues.GetSize()); illc++)
					{
						olRealShiftValues[illc]--;
					}

				}			
			}
		}
	}

	olAutoDemandValues.RemoveAll();
	olRealShiftValues.RemoveAll();

	LoadSdtWindow();

	ChangeViewToForHScroll(omStartTime,omEndTime);
	pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
	pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
}


int CoverageDiagramViewer::CalcNewBreakOffSet(int ipBPStart, int ipBPEnd, int ipBDuration,CUIntArray &rpInDemData,CUIntArray &rpOutShiftCovData)
{
	//	Cluster step = 2 Minutes
	int ilClusterStep = 2;	

	//	break duration 
	int ilBPDuration = ipBPEnd - ipBPStart;

	//	block size 
	int ilBlockSize = (int) ceil(((double)ilBPDuration)/((double)ilClusterStep));

	int ilPos,ilDiff;

	// the break differences for each cluster
	CUIntArray olBreakDiff;

	// huge size to search for
	int ilMaxBound = rpInDemData.GetSize();

	for(int illc = 0 ; illc < ilBlockSize;illc++)
	{
		ilPos = ipBPStart + illc * ilClusterStep;
		if (ilPos >= 0 && (ilPos + ipBDuration) < ilMaxBound)
		{
			// calculate difference between coverage and demand curve !
			ilDiff= rpOutShiftCovData[ilPos] - rpInDemData[ilPos];
			// store the difference for each block
			olBreakDiff.Add(ilDiff);
		}
	}

	// cluster size is the count how often the break contains the cluster step
	int ilClusterSize = (int) ceil((int)ipBDuration/ilClusterStep);

	int ilSize = olBreakDiff.GetSize();
	
	int ilMaxDiff,ilTotalDiff,ilOptPos = 0;

	ilMaxBound = 0;
	// loop over all break differences (in blocks)
	for (illc = 0; illc < ilSize;illc++)
	{
		ilTotalDiff = 0;
		ilMaxBound = min(illc+ilClusterSize,ilSize);
		// add differences for a single break cluster
		for(int illc2 = illc; illc2 < ilMaxBound;illc2++)
		{
			ilTotalDiff += olBreakDiff[illc2];
		}

		if (illc > 0)
		{
			if (ilTotalDiff > ilMaxDiff)
			{
				ilMaxDiff = ilTotalDiff;
				ilOptPos = illc;
			}
		}
		else
		{
			ilMaxDiff = ilTotalDiff;
			ilOptPos = illc;
		}
	}

	int ilBreakEnd = min(ipBPEnd,ipBPStart + ilOptPos*ilClusterStep + ipBDuration);
	int ilBreakStart = ilBreakEnd - ipBDuration;
	int ilBreakOffSet = ilBreakStart - ipBPStart;

	return ilBreakOffSet;
}


void CoverageDiagramViewer::DiscardDRRBreakChanges()
{
	CCSPtrArray <SDTDATA> olList;
	
	ogSdtRosterData.GetAllAutoSdts(olList);

	for(int illc = 0; illc < olList.GetSize();illc++)
	{
	
		olList[illc].Brkf = olList[illc].OrgBrkf;
		olList[illc].Brkt = olList[illc].OrgBrkt;

		olList[illc].IsAutoSdt = false;
	}
	olList.RemoveAll();
	LoadSdtWindow();
	ChangeViewToForHScroll(omStartTime,omEndTime);
	pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
	pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);		
}

void CoverageDiagramViewer::SaveDRRBreakChanges()
{
	CCSPtrArray <SDTDATA> olList;
	ogSdtRosterData.GetAllAutoSdts(olList);

	for(int illc = 0; illc < olList.GetSize();illc++)
	{
		CString olDRRUrno = "";
		CString olOffSet = "";
		olDRRUrno.Format("%ld",olList[illc].Urno);

		int ilOffSet = (olList[illc].Brkf - olList[illc].OrgBrkf).GetTotalMinutes();

		if(ilOffSet != 0)
		{
			DRRDATA *prlDrr = ogDrrData.GetDrrByUrno(atol(olDRRUrno));

			if (prlDrr != NULL && prlDrr->Bros != ilOffSet)
			{
				DRRDATA olDrr = *prlDrr;
				olDrr.Bros = ilOffSet;
				olDrr.IsChanged = DATA_CHANGED;
				ogDrrData.UpdateInternal(&olDrr,false);
			}
		}

		olList[illc].IsAutoSdt = false;
	}

	olList.RemoveAll();
	ogDrrData.Release();

	LoadSdtWindow();
	ChangeViewToForHScroll(omStartTime,omEndTime);
	pomAttachWnd->SendMessage(WM_UPDATEDEMANDGANTT, 1,0);
	pomAttachWnd->SendMessage(WM_UPDATE_COVERAGE, 0, 0);		
}

CString CoverageDiagramViewer::GetFilterEquipmentType()
{
	return this->omViewerFilterEquipmentType; 
}

bool CoverageDiagramViewer::DemUpdateAllowed()
{
	bool blUpdate = !bmNoDemUpdates;

	// Auto coverage must not be active
	blUpdate = blUpdate && !this->bmAutoCovIsActive;

	// Demand generation should not be active
	blUpdate = blUpdate && (bgCreateDemCount == 0);

	return blUpdate;
}

CTimeSpan CoverageDiagramViewer::CalculateWorktime(CTime opFrom,CTime opTo)
{
	CTimeSpan olWorktime = CTimeSpan(0,0,0,0);
	SDTDATA *prlSdt;
	long llUrno;
	CTime olStartTime;
	CTime olEndTime;
	
	for (POSITION rlPos = omSdtUrnoMap.GetStartPosition(); rlPos != NULL;)
	{
		omSdtUrnoMap.GetNextAssoc(rlPos,(void *&)llUrno,(void *&)prlSdt);
		if (IsPassSdtFilterWorktime(prlSdt))
		{
			if (IsOverlapped(opFrom,opTo,prlSdt->Esbg,prlSdt->Lsen))
			{
				CTimeSpan olDemandDuration = CTimeSpan(0,0,0,0);
				olStartTime = prlSdt->Esbg;
				olEndTime	= prlSdt->Lsen;

				if (olStartTime <= opFrom && olEndTime < opTo)
				{
					olDemandDuration = olEndTime - opFrom;
				}
				else if (olStartTime > opFrom && olEndTime <= opTo)
				{
					olDemandDuration = olEndTime - olStartTime;
				}
				else if (olStartTime > opFrom && olEndTime > opTo)
				{
					olDemandDuration = opTo - olStartTime;
				}
				else if (olStartTime < opFrom && olEndTime > opTo)
				{
					olDemandDuration = opTo - opFrom;

				}

				olWorktime += olDemandDuration;

				if (!bmIgnoreBreak)
				{
					if (IsOverlapped(opFrom,opTo,prlSdt->Brkf,prlSdt->Brkt))
					{
						olStartTime = prlSdt->Brkf;
						olEndTime	= prlSdt->Brkt;

						olDemandDuration  = CTimeSpan(0,0,0,0);
						if (olStartTime <= opFrom && olEndTime < opTo)
						{
							olDemandDuration = olEndTime - opFrom;
						}
						else if (olStartTime > opFrom && olEndTime <= opTo)
						{
							olDemandDuration = olEndTime - olStartTime;
						}
						else if (olStartTime > opFrom && olEndTime > opTo)
						{
							olDemandDuration = opTo - olStartTime;
						}
						else if (olStartTime < opFrom && olEndTime > opTo)
						{
							olDemandDuration = opTo - opFrom;

						}
						
						olWorktime -= olDemandDuration;
					}
				}
			}
		}
	}

	return olWorktime;
}

bool CoverageDiagramViewer::ProcessDemRelease(void *vpDataPointer)
{
	CCS_TRY

	struct BcStruct *prlBcStruct = (struct BcStruct *) vpDataPointer;

	if (prlBcStruct != NULL)
	{
		ogBackTrace += "2,";
		ReloadAllDems();			
		ogBackTrace += "2e,";
	}
	CCS_CATCH_ALL

	return true;
}


bool CoverageDiagramViewer::GetDebugBaseShiftsFilter(CString opViewName,CMapStringToPtr& ropCMapForBsh)
{
	CStringArray olFilterValues;
	CString olText;
	int ilLc,ilCount;
	CString olOldBaseViewName = GetBaseViewName();
	CString olOldViewName = GetViewName();


	SetViewerKey("BASISSCHICHTEN");
	SelectView(opViewName);

    GetFilter("BSH", olFilterValues);

	ilCount = olFilterValues.GetSize();

    ropCMapForBsh.RemoveAll();
    CString olTmp = "";
	bool blUseAllBsh = false;
	if (ilCount > 0)
	{
		CString olTmpText;
		CStringArray olItemList;
		if (olFilterValues[0] == LoadStg(IDS_STRING61216))
		{
			blUseAllBsh = true;
		}
		else
		{
			for (ilLc = 0; ilLc < ilCount; ilLc++)
			{
				ropCMapForBsh[olFilterValues[ilLc]] = NULL;
			}
		}
	}

	SetViewerKey(olOldBaseViewName);
	SelectView(olOldViewName);

	return blUseAllBsh;
}

bool CoverageDiagramViewer::GetDebugStatistics(CMapStringToPtr& ropBsdGroupStatistic)
{
	CString olOldBaseViewName = GetBaseViewName();
	CString olOldViewName = GetViewName();
	CString olViewName;
			
	SetViewerKey("BASISSCHICHTEN");
	CStringArray olStrArr;
	GetViews(olStrArr);
	olViewName = GetViewName();
			
	SetViewerKey(olOldBaseViewName);
	SelectView(olOldViewName);


	for (POSITION pos = this->omSdtUrnoMap.GetStartPosition(); pos != NULL;)
	{
		long llUrno;
		SDTDATA *prlSdt;
		this->omSdtUrnoMap.GetNextAssoc(pos,(void *&)llUrno,(void *&)prlSdt);

		for (int i = 0; i < olStrArr.GetSize(); i++)
		{
			bool blFound = false;
			CMapStringToPtr olMapForBsh;
			if (GetDebugBaseShiftsFilter(olStrArr[i],olMapForBsh))
			{
				blFound = true;
			}
			else
			{
				void *prlBsh;
				blFound = olMapForBsh.Lookup(prlSdt->Bsdc,(void *&)prlBsh);
			}

			if (blFound)
			{									
				int ilSize = 0;
				if (ropBsdGroupStatistic.Lookup(olStrArr[i],(void *&)ilSize) == false)
				{
					ilSize = 1;
					ropBsdGroupStatistic.SetAt(olStrArr[i],(void *&) ilSize);
				}
				else
				{
					++ilSize;
					ropBsdGroupStatistic.SetAt(olStrArr[i],(void *&) ilSize);
				}
			}
		}
	}

	return true;
}

bool CoverageDiagramViewer::GetSdtsBetweenTimesOrderedByBsdc(CTime opStart,CTime opEnd,CMapStringToPtr &opSdtBsdcMap)
{
	bool blRet =false;

	if (opEnd != TIMENULL && opStart != TIMENULL && opEnd > opStart)
	{
		POSITION rlPos;	
		for(rlPos = omSdtUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			SDTDATA *prlSdt;
			long llUrno;
			omSdtUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
			if (IsPassSdtFilterWorktime(prlSdt))
			{
				if (prlSdt->Esbg <= opEnd && prlSdt->Lsen >= opStart && prlSdt->IsChanged != DATA_DELETED)
				{
					CCSPtrArray<SDTDATA> *polArray;
					if (opSdtBsdcMap.Lookup(prlSdt->Bsdc,(void *&)polArray) == FALSE)
					{
						polArray = new CCSPtrArray<SDTDATA>;
						opSdtBsdcMap.SetAt(prlSdt->Bsdc,(void *)polArray);
					}

					polArray->Add(prlSdt);
					blRet = true;
				}
			}
		}
	}
	return blRet;
							
}


void CoverageDiagramViewer::AllDistinctSdtFunctions(CStringArray &ropFunctions)
{
	CString olUrnoList;
	CString olUrnoList2;
	CString olAllFcts;
	POSITION rlPos;

	ropFunctions.RemoveAll();
	for ( rlPos =  omSdtUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		SDTDATA *prlSdt;
		omSdtUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
		if (IsPassSdtFilterWorktime(prlSdt))
		{
			if (olAllFcts.Find(prlSdt->Fctc) < 0)
			{
				ropFunctions.Add(prlSdt->Fctc);
				olAllFcts += prlSdt->Fctc;
			}
		}
	}
}

