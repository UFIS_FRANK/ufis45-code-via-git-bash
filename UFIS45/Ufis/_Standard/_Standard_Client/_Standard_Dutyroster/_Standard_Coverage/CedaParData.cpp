// CedaParData.cpp
 
#include <stdafx.h>
#include <CedaParData.h>
#include <CedaValData.h>
#include <BasicData.h>
//#include <Bdpsuif.h>
#include <resource.h>


void ProcessParCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaParData::CedaParData() : CCSCedaData(&ogCommHandler)
{ 
	// Create an array of CEDARECINFO for ParDataStruct
	BEGIN_CEDARECINFO(PARDATA,ParDataRecInfo)
		FIELD_CHAR_TRIM	(Appl,"APPL")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Name,"NAME")
		FIELD_CHAR_TRIM	(Paid,"PAID")
		FIELD_CHAR_TRIM	(Ptyp,"PTYP")
		FIELD_CHAR_TRIM	(Txid,"TXID")
		FIELD_CHAR_TRIM	(Type,"TYPE")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Valu,"VALU")
	END_CEDARECINFO //(ParDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(ParDataRecInfo)/sizeof(ParDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&ParDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"PAR");
	strcpy(pcmListOfFields,"APPL,CDAT,LSTU,NAME,PAID,PTYP,TXID,TYPE,URNO,USEC,USEU,VALU");
	
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer
	
	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaParData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("APPL");
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("NAME");
	ropFields.Add("PAID");
	ropFields.Add("PTYP");
	ropFields.Add("TXID");
	ropFields.Add("TYPE");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VALU");

	ropDesription.Add(LoadStg(IDS_STRING61406));
	ropDesription.Add(LoadStg(IDS_STRING61407));
	ropDesription.Add(LoadStg(IDS_STRING61408));
	ropDesription.Add(LoadStg(IDS_STRING61409));
	ropDesription.Add(LoadStg(IDS_STRING61410));
	ropDesription.Add(LoadStg(IDS_STRING61411));
	ropDesription.Add(LoadStg(IDS_STRING61412));
	ropDesription.Add(LoadStg(IDS_STRING61413));
	ropDesription.Add(LoadStg(IDS_STRING61414));
	ropDesription.Add(LoadStg(IDS_STRING61415));
	ropDesription.Add(LoadStg(IDS_STRING61416));
	ropDesription.Add(LoadStg(IDS_STRING61417));

	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaParData::Register(void)
{
	ogDdx.Register((void *)this,BC_PAR_CHANGE,	CString("PARDATA"), CString("Par-changed"),	ProcessParCf);
	ogDdx.Register((void *)this,BC_PAR_NEW,		CString("PARDATA"), CString("Par-new"),		ProcessParCf);
	ogDdx.Register((void *)this,BC_PAR_DELETE,	CString("PARDATA"), CString("Par-deleted"),	ProcessParCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaParData::~CedaParData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaParData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaParData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		PARDATA *prlPar = new PARDATA;
		if ((ilRc = GetFirstBufferRecord(prlPar)) == true)
		{
			omData.Add(prlPar);//Update omData
			omUrnoMap.SetAt((void *)prlPar->Urno,prlPar);
		}
		else
		{
			delete prlPar;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaParData::Insert(PARDATA *prpPar)
{
	prpPar->IsChanged = DATA_NEW;
	if(Save(prpPar) == false) return false; //Update Database
	InsertInternal(prpPar);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaParData::InsertInternal(PARDATA *prpPar)
{
	ogDdx.DataChanged((void *)this, PAR_NEW,(void *)prpPar ); //Update Viewer
	omData.Add(prpPar);//Update omData
	omUrnoMap.SetAt((void *)prpPar->Urno,prpPar);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaParData::Delete(long lpUrno)
{
	PARDATA *prlPar = GetParByUrno(lpUrno);
	if (prlPar != NULL)
	{
		prlPar->IsChanged = DATA_DELETED;
		if(Save(prlPar) == false) return false; //Update Database
		DeleteInternal(prlPar);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaParData::DeleteInternal(PARDATA *prpPar)
{
	ogDdx.DataChanged((void *)this,PAR_DELETE,(void *)prpPar); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpPar->Urno);
	int ilParCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilParCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpPar->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaParData::Update(PARDATA *prpPar)
{
	if (GetParByUrno(prpPar->Urno) != NULL)
	{
		if (prpPar->IsChanged == DATA_UNCHANGED)
		{
			prpPar->IsChanged = DATA_CHANGED;
		}
		if(Save(prpPar) == false) return false; //Update Database
		UpdateInternal(prpPar);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaParData::UpdateInternal(PARDATA *prpPar)
{
	PARDATA *prlPar = GetParByUrno(prpPar->Urno);
	if (prlPar != NULL)
	{
		*prlPar = *prpPar; //Update omData
		ogDdx.DataChanged((void *)this,PAR_CHANGE,(void *)prlPar); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

PARDATA *CedaParData::GetParByUrno(long lpUrno)
{
	PARDATA  *prlPar;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlPar) == TRUE)
	{
		return prlPar;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaParData::ReadSpecialData(CCSPtrArray<PARDATA> *popPar,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","PAR",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","PAR",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popPar != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			PARDATA *prpPar = new PARDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpPar,CString(pclFieldList))) == true)
			{
				popPar->Add(prpPar);
			}
			else
			{
				delete prpPar;
			}
		}
		if(popPar->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaParData::Save(PARDATA *prpPar)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpPar->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpPar->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpPar);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpPar->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPar->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpPar);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpPar->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPar->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessParCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogParData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaParData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlParData;
	prlParData = (struct BcStruct *) vpDataPointer;
	PARDATA *prlPar;
	if(ipDDXType == BC_PAR_NEW)
	{
		prlPar = new PARDATA;
		GetRecordFromItemList(prlPar,prlParData->Fields,prlParData->Data);
		if(ValidateParBcData(prlPar->Urno)) //Prf: 8795
		{
			InsertInternal(prlPar);
		}
		else
		{
			delete prlPar;
		}
	}
	if(ipDDXType == BC_PAR_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlParData->Selection);
		prlPar = GetParByUrno(llUrno);
		if(prlPar != NULL)
		{
			GetRecordFromItemList(prlPar,prlParData->Fields,prlParData->Data);
			if(ValidateParBcData(prlPar->Urno)) //Prf: 8795
			{
				UpdateInternal(prlPar);
			}
			else
			{
				DeleteInternal(prlPar);
			}
		}
	}
	if(ipDDXType == BC_PAR_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlParData->Selection);

		prlPar = GetParByUrno(llUrno);
		if (prlPar != NULL)
		{
			DeleteInternal(prlPar);
		}
	}
}

//Prf: 8795
//--ValidateParBcData--------------------------------------------------------------------------------------

bool CedaParData::ValidateParBcData(const long& lrpUrno)
{
	bool blValidateParBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<PARDATA> olPars;
		if(!ReadSpecialData(&olPars,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateParBcData = false;
		}
	}
	return blValidateParBcData;
}

//---------------------------------------------------------------------------
CString CedaParData::GetParValue(const CString& appl, const CString& paramid, CString* ptimestamp)
{
	PARDATA* olMyParam;
	CString olValue;
	olValue.Empty();

	CString olStringNow;
	if (ptimestamp == 0)
	{
		CTime	olTimeNow = CTime::GetCurrentTime();
		olStringNow = olTimeNow.Format("%Y%m%d%H%M%S");
		ptimestamp = &olStringNow;
	}

	olMyParam = GetParById(appl,paramid,*ptimestamp,"","","","","","","","","","");

	if (olMyParam == NULL) 
	{
		// Fehlermeldung anzeigen, wenn gew�nscht.
		CString olDate = "Date: " + ptimestamp->Mid(6,2) + "." + ptimestamp->Mid(4,2) + "." + ptimestamp->Left(4);
		// Zeit formatieren
		CString olTime = "Time: " + ptimestamp->Mid(8,2) + ":" + ptimestamp->Mid(10,2);
//$$		AfxMessageBox(LoadStg(IDS_STRING1845) + "\n" + "Parameter: " + paramid + "\n" + olDate + "\n" + olTime, MB_OK|MB_ICONEXCLAMATION);
	}
	else
	{
		olValue = olMyParam->Valu;
	}
	return olValue;
}

//*************************************************************************************
//
//*************************************************************************************

PARDATA* CedaParData::GetParById(CString appl, const CString& paramid, const CString& timestamp,
								const CString& defaultvalue,const CString& name,const CString& paramtyp , const CString& txid, 
								const CString& type, const CString& datefrom , const CString& dateto,
								const CString& timefrom,const CString& timeto,const CString& freq, bool bpMakeNewDBEntry)
{
	CString olErrorMsg;
	// does key(appname, paramid) exist?
	int i = omData.GetSize() - 1;
	bool isvalid=false;

	// application name can only be 8 Chars (ADO 8.12.99)
	appl = appl.Left(8);
	
	while ((i>=0) && (!isvalid))
	{
		if ((strcmp(appl,omData[i].Appl) == 0) && (strcmp(paramid,omData[i].Paid) == 0))
		{
			if (ogValData.IsValid(omData[i].Urno,timestamp))
				isvalid=true;
			else
			{
				bpMakeNewDBEntry = false;
				i--;
			}
		}
		else i--;
	}

	if (i >= 0) 
	{	// yes -> return address
		return &(omData[i]);
	}
	else if (bpMakeNewDBEntry)
	{
		PARDATA* polData = new PARDATA;
		polData->Urno = ogBasicData.GetNextUrno();	//new URNO
		polData->Cdat = CTime::GetCurrentTime();
		strcpy(polData->Usec,pcgUser);
		strcpy(polData->Appl,appl.Left(8));
		strcpy(polData->Paid,paramid.Left(16));
		strcpy(polData->Name,name.Left(64));
		strcpy(polData->Valu,defaultvalue.Left(64));
		strcpy(polData->Ptyp,paramtyp.Left(16));
		strcpy(polData->Txid,txid.Left(32));
		strcpy(polData->Type,type);

		if (Insert(polData))
		{
			VALDATA *polVal = new VALDATA;
			polVal->Urno = ogBasicData.GetNextUrno();	//new URNO
			polVal->Uval = polData->Urno;
			polVal->Cdat = CTime::GetCurrentTime();
			strcpy(polVal->Usec,pcgUser);
			
			strcpy(polVal->Appl,appl.Left(8));
			strcpy(polVal->Freq,freq);

			if (ogValData.Insert(polVal))
				return polData;
			else
			{
				delete polVal;
				return NULL;
			}
		}
		else
		{
			delete polData;
			return NULL;
		}
	}
	else
		return NULL;
}
