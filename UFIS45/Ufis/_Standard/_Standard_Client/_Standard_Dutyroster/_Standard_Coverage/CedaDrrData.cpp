// CedaDrrData.cpp
  
#include <stdafx.h>
#include <CedaDrrData.h>
#include <algorithm>

#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))

void ProcessDrrCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaDrrData::CedaDrrData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for DRRDataStruct
	BEGIN_CEDARECINFO(DRRDATA,DRRDataRecInfo)
		FIELD_LONG	   (Urno,"URNO")
		FIELD_CHAR_TRIM(Scod,"SCOD")
		FIELD_DATE	   (Avfr,"AVFR")
		FIELD_DATE	   (Avto,"AVTO")
		FIELD_DATE	   (Sbfr,"SBFR")
		FIELD_DATE	   (Sbto,"SBTO")
		FIELD_LONG	   (Sblu,"SBLU")
		FIELD_LONG	   (Stfu,"STFU")
		FIELD_CHAR_TRIM(Ross,"ROSS")
		FIELD_CHAR_TRIM(Rosl,"ROSL")
		FIELD_CHAR_TRIM(Fctc,"FCTC")
		FIELD_CHAR_TRIM(Drsf,"DRSF")
		FIELD_CHAR_TRIM(Drrn,"DRRN")
		FIELD_CHAR_TRIM(Sday,"SDAY")
		FIELD_LONG	   (Bros,"BROS")
	END_CEDARECINFO //(DRRDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(DRRDataRecInfo)/sizeof(DRRDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DRRDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"DRR");
	strcat(pcmTableName,pcgTableExt);

	// initial without BROS field ,will be set prior to READ operation
	strcpy(pcmListOfFields,"URNO,SCOD,AVFR,AVTO,SBFR,SBTO,SBLU,STFU,ROSS,ROSL,FCTC,DRSF,DRRN,SDAY");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
	
	Register();
}

//---------------------------------------------------------------------------------------------------------

//--REGISTER----------------------------------------------------------------------------------------------

void CedaDrrData::Register(void)
{
	ogDdx.Register((void *)this,BC_DRR_CHANGE,	CString("DRRDATA"), CString("Drr-changed"),	ProcessDrrCf);
	ogDdx.Register((void *)this,BC_DRR_NEW,		CString("DRRDATA"), CString("Drr-new"),		ProcessDrrCf);
	ogDdx.Register((void *)this,BC_DRR_DELETE,	CString("DRRDATA"), CString("Drr-deleted"),	ProcessDrrCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaDrrData::~CedaDrrData(void)
{
	TRACE("CedaDrrData::~CedaDrrData called\n");
	ClearAll();
	omRecInfo.DeleteAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaDrrData::ClearAll(bool bpWithRegistration)
{
	TRACE("CedaDrrData::ClearAll called\n");
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}

    omUrnoMap.RemoveAll();
    omData.DeleteAll();
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaDrrData::Read(const char *pspWhere /*NULL*/)
{
	if (ogBasicData.IsBrosAvailable())
		strcpy(pcmListOfFields,"URNO,SCOD,AVFR,AVTO,SBFR,SBTO,SBLU,STFU,ROSS,ROSL,FCTC,DRSF,DRRN,SDAY,BROS");
	else
		strcpy(pcmListOfFields,"URNO,SCOD,AVFR,AVTO,SBFR,SBTO,SBLU,STFU,ROSS,ROSL,FCTC,DRSF,DRRN,SDAY");

    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction2("RT");
	}
	else
	{
		ilRc = CedaAction2("RT", (char *)pspWhere,NULL,pcgDataBuf,"BUF1",false,0,true,NULL,NULL,ogBasicData.GetDrrIndexHint().GetBuffer(0));
	}

	if (ilRc != true)
	{
		TRACE("Read-Drr: Ceda-Error %d \n",ilRc);
		return ilRc;
	}

	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		DRRDATA *prlDrr = new DRRDATA;
		if ((ilRc = GetFirstBufferRecord2(prlDrr)) == true)
		{
			
			prlDrr->IsChanged = DATA_UNCHANGED;
			omData.Add(prlDrr);//Update omData
			omUrnoMap.SetAt((void *)prlDrr->Urno,prlDrr);

              
		}
		else
		{
			delete prlDrr;
		}
	}
	TRACE("Read-Drr: %d gelesen\n",ilLc-1);
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaDrrData::Insert(DRRDATA *prpDrr,bool bpSendDdx)
{
	prpDrr->IsChanged = DATA_NEW;
	if(Save(prpDrr) == false) return false; //Update Database
	InsertInternal(prpDrr,bpSendDdx);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaDrrData::InsertInternal(DRRDATA *prpDrr,bool bpSendDdx)
{
	bool blRet = false;
	DRRDATA *prlDrr = NULL;
	if (omUrnoMap.Lookup((void *)prpDrr->Urno,(void *& )prlDrr) == FALSE)
	{
		omData.Add(prpDrr);//Update omData
		omUrnoMap.SetAt((void *)prpDrr->Urno,prpDrr);
		if (bpSendDdx)
		{
			ogDdx.DataChanged((void *)this, DRR_NEW,(void *)prpDrr ); //Update Viewer
		}
		blRet =true;
	}
    return blRet;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaDrrData::Delete(long lpUrno,bool bpSendDdx)
{
	DRRDATA *prlDrr = GetDrrByUrno(lpUrno);
	if (prlDrr != NULL)
	{
		prlDrr->IsChanged = DATA_DELETED;
		if(Save(prlDrr) == false) return false; //Update Database
		DeleteInternal(prlDrr,bpSendDdx);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaDrrData::DeleteInternal(DRRDATA *prpDrr,bool bpSendDdx)
{
	if (bpSendDdx)
	{
		ogDdx.DataChanged((void *)this,DRR_DELETE,(void *)prpDrr); //Update Viewer
	}

	omUrnoMap.RemoveKey((void *)prpDrr->Urno);
	int ilDrrCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilDrrCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpDrr->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaDrrData::Update(DRRDATA *prpDrr,bool bpSendDdx)
{
	if (GetDrrByUrno(prpDrr->Urno) != NULL)
	{
		if (prpDrr->IsChanged == DATA_UNCHANGED)
		{
			prpDrr->IsChanged = DATA_CHANGED;
		}
		if(Save(prpDrr) == false) return false; //Update Database
		UpdateInternal(prpDrr,bpSendDdx);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaDrrData::UpdateInternal(DRRDATA *prpDrr,bool bpSendDdx)
{
	DRRDATA *prlDrr = GetDrrByUrno(prpDrr->Urno);
	if (prlDrr != NULL)
	{
		*prlDrr = *prpDrr; //Update omData
		if (bpSendDdx)
			ogDdx.DataChanged((void *)this,DRR_CHANGE,(void *)prlDrr); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

DRRDATA *CedaDrrData::GetDrrByUrno(long lpUrno)
{
	DRRDATA  *prlDrr;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlDrr) == TRUE)
	{
		return prlDrr;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaDrrData::ReadSpecialData(CCSPtrArray<DRRDATA> *popDrr,const char *pspWhere,const char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,(char *)pclFieldList,(char *)pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT",pcmTableName,(char *)pclFieldList,(char *)pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popDrr != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			DRRDATA *prpDrr = new DRRDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpDrr,CString(pclFieldList))) == true)
			{
				prpDrr->IsChanged = DATA_UNCHANGED;
				popDrr->Add(prpDrr);
			}
			else
			{
				delete prpDrr;
			}
		}
		if(popDrr->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaDrrData::Save(DRRDATA *prpDrr)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpDrr->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpDrr->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpDrr);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpDrr->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpDrr->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpDrr);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpDrr->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpDrr->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	TRACE("Drr-IRT/URT/DRT: Ceda-Return %d \n",ilRc);
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessDrrCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	BC_TRY
		ogDrrData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
	BC_CATCH_ALL
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaDrrData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlDrrData;
	prlDrrData = (struct BcStruct *) vpDataPointer;
	DRRDATA *prlDrr;
	if(ipDDXType == BC_DRR_NEW)
	{
		prlDrr = new DRRDATA;
		GetRecordFromItemList(prlDrr,prlDrrData->Fields,prlDrrData->Data);
		InsertInternal(prlDrr);
	}
	else if(ipDDXType == BC_DRR_CHANGE)
	{
		long llUrno  = 0L;
		CString olSelection = (CString)prlDrrData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlDrrData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlDrr = GetDrrByUrno(llUrno);
		if(prlDrr != NULL)
		{
			GetRecordFromItemList(prlDrr,prlDrrData->Fields,prlDrrData->Data);
			UpdateInternal(prlDrr);
		}
	}
	else if(ipDDXType == BC_DRR_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlDrrData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlDrrData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlDrr = GetDrrByUrno(llUrno);
		if (prlDrr != NULL)
		{
			DeleteInternal(prlDrr);
		}
	}
}

bool CedaDrrData::Release()
{
	CCS_TRY;

	// Kommandostrings f�r CEDA: INSERT-, UPDATE- und DELETE-Kommando
	CString olIRT, olURT, olDRT;
	// Feldliste: beschreibt die zu speichernden Felder der Tabelle
	CString olFieldList;
	// vollst�ndige Kommandos (Update, Insert und Delete) f�r CedaData::CedaAction()
	// Format:	1.Zeile:		Kommando, Feldliste
	//			1.-n. Zeile:	relevante Felder des Datensatzes (zum L�schen reicht die Urno, sonst alle Felder)
	CString olUpdateString, olInsertString, olDeleteString;
	// Zwischenspeicher
	CString olTmpText;
	// Z�hler f�r Anzahlen der ge�nderten/gel�schten/neuen Datens�tze
	int ilNoOfUpdates = 0, ilNoOfDeletes = 0, ilNoOfInserts = 0;
	// Anzahl der Felder pro Datensatz ermitteln
	CStringArray olFields;	// nur Hilfsarray, wird nicht weiter benutzt
	int ilFields = ExtractItemList(CString(pcmListOfFields),&olFields); 

	// Kommandostrings (Header) generieren
	olIRT.Format("*CMD*,%s,IRT,%d,", pcmTableName, ilFields);
	olURT.Format("*CMD*,%s,URT,%d,", pcmTableName, ilFields);
	olDRT.Format("*CMD*,%s,DRT,-1,", pcmTableName);

	CString olOrigFieldList;
	// Paketz�hler: alle hundert Datens�tze einen Schreibvorgang anstossen
	int ilCurrentCount = 0;
	// Gesamtz�hler, wird am Ende benutzt, um zu pr�fen, ob mind. 1 DS geschrieben wurde
	int ilTotalCount = 0;

	// alte Feldliste speichern
	olOrigFieldList = CString(pcmListOfFields);
	// Strings f�r CedaAction initialisieren
	olInsertString = olIRT + CString(pcmListOfFields) + CString("\n");
	olUpdateString = olURT + CString(pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
	olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ CString("\n");;

	// Datensynchronisation anstossen
	Synchronise("COVERAGE",true);

	// nein -> dann alle intern gecachten Datens�tze Speichern
	POSITION rlPos;
	for (rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		////////////////////////////////////////////////////////////////////////
		//// �nderung pr�fen und Pakete mit Datens�tzen schn�ren
		////////////////////////////////////////////////////////////////////////
		// die Datensatz-Urno
		long llUrno;
		// der Datensatz
		DRRDATA *prlDrr;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDrr);
		if (prlDrr->IsChanged != DATA_UNCHANGED)
		{
			CString olListOfData;
			CString olCurrentUrno;
			olCurrentUrno.Format("%d",prlDrr->Urno);
			// aus dem Datensatz einen String generieren (omResInfo ist Member von CCSCedaData)
			MakeCedaData(&omRecInfo,olListOfData,prlDrr);
			// was soll mit dem Datensatz geschehen
			switch(prlDrr->IsChanged)
			{
			case DATA_NEW:	// neuer DS
				olInsertString += olListOfData + CString("\n");
				ilNoOfInserts++;
				break;
			case DATA_CHANGED:	// ge�nderter DS
				olUpdateString += olListOfData + CString(",") + olCurrentUrno + CString("\n");
				ilNoOfUpdates++;
				break;
			case DATA_DELETED:	// DS l�schen
				olDeleteString += olCurrentUrno + CString("\n");
				ilNoOfDeletes++;
				break;
			}
			// Z�hler inkrementieren
			ilCurrentCount++;
			ilTotalCount++;

			// �nderungsflag zur�cksetzen
			prlDrr->IsChanged = DATA_UNCHANGED;
		}
		////////////////////////////////////////////////////////////////////////
		//// Ende �nderung pr�fen und Pakete mit Datens�tzen schn�ren
		////////////////////////////////////////////////////////////////////////
			
		////////////////////////////////////////////////////////////////////////
		//// 500er Paket in Datenbank speichern
		////////////////////////////////////////////////////////////////////////
		// 500 Datens�tze im Paket?
		if(ilCurrentCount == 500)
		{
			// ja -> Paket speichern
			// neue Datens�tze erzeugt?
			if(ilNoOfInserts > 0)
			{
				// ja -> speichern
				CedaAction("REL","LATE,NOACTION,NOLOG,NOBC","",olInsertString.GetBuffer(0));
			}
			// Datens�tze ge�ndert?
			if(ilNoOfUpdates > 0)
			{
				// ja -> speichern
				CedaAction("REL","LATE,NOACTION,NOLOG,NOBC","",olUpdateString.GetBuffer(0));
			}
			// Datens�tze gel�scht?
			if(ilNoOfDeletes > 0)
			{
				// ja -> aus Datenbank l�schen
				CedaAction("REL","LATE,NOACTION,NOLOG,NOBC","",olDeleteString.GetBuffer(0));
			}
			
			// Z�hler und Strings wieder auf Initialwerte setzen
			ilCurrentCount = 0;
			ilNoOfInserts = 0;
			ilNoOfUpdates = 0;
			ilNoOfDeletes = 0;
			olInsertString = olIRT + CString(pcmListOfFields) + CString("\n");
			olUpdateString = olURT + CString(pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
			olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ CString("\n");;
		}
		////////////////////////////////////////////////////////////////////////
		//// Ende 500er Paket in Datenbank speichern
		////////////////////////////////////////////////////////////////////////
	}//for(rlPos = ......
		
		
	////////////////////////////////////////////////////////////////////////
	//// Rest(von unter Fuenfhundert)-Paket in Datenbank speichern
	////////////////////////////////////////////////////////////////////////
	// neue Datens�tze erzeugt?
	if(ilNoOfInserts > 0)
	{
		// ja -> speichern
		CedaAction("REL","LATE,NOACTION,NOLOG,NOBC","",olInsertString.GetBuffer(0));
	}
	// Datens�tze ge�ndert?
	if(ilNoOfUpdates > 0)
	{
		// ja -> speichern
		CedaAction("REL","LATE,NOACTION,NOLOG,NOBC","",olUpdateString.GetBuffer(0));
	}
	// Datens�tze gel�scht?
	if(ilNoOfDeletes > 0)
	{
		// ja -> aus Datenbank l�schen
		CedaAction("REL","LATE,NOACTION,NOLOG,NOBC","",olDeleteString.GetBuffer(0));
	}
	////////////////////////////////////////////////////////////////////////
	//// Ende Rest(von unter Fuenfhundert)-Paket in Datenbank speichern
	////////////////////////////////////////////////////////////////////////
		
	////////////////////////////////////////////////////////////////////////
	//// Ende Broadcast erzeugen
	////////////////////////////////////////////////////////////////////////
	CTime olStartTime,olEndTime;
	ogBasicData.GetDiagramStartTime(olStartTime,olEndTime);
	CString olData;
	olData.Format("%s-%s",olStartTime.Format("%Y%m%d%H%M%S"),olEndTime.Format("%Y%m%d%H%M%S"));
	CedaAction("SBC","RELPBO","",CCSCedaData::pcmReqId,"",olData.GetBuffer(0));

	CCS_CATCH_ALL;
	return true;
}


bool CedaDrrData::Synchronise(CString opUnknown, bool bpActive)
{
	CCS_TRY;
	CTime olCurrentTime = CTime::GetCurrentTime(); 
	CTime olToTime = olCurrentTime + CTimeSpan(0, 0, 2, 0);
	olToTime = olCurrentTime + CTimeSpan(0, 0, 2, 0);
	CString olStrFrom = CTimeToDBString(olCurrentTime,olCurrentTime);
	CString olStrTo = CTimeToDBString(olToTime,olToTime);
	char pclData[1024]="";
	char pclSelection[1024]="";
	if (bpActive)
	{
		sprintf(pclData, "*CMD*,SYN%s,SYNC,%s,N,%s,%s, ", pcgTableExt, pcmTableName, olStrFrom.GetBuffer(0), olStrTo.GetBuffer(0));
		strcpy(pclSelection, "LATE");
		CedaAction("REL",pclSelection,"",pclData);
		// Synchronisieren OK?
		if(CString(pclSelection).Find("NOT OK") != -1)
		{
			// terminieren
			return false;
		}
	}
	else
	{ 
		// Synchronisation beenden
		sprintf(pclData, "*CMD*,SYN%s,SYNC,%s, ,%s,%s, ", pcgTableExt, pcmTableName, olStrFrom.GetBuffer(0), olStrTo.GetBuffer(0));
		CedaAction("REL","LATE","",pclData);
	}

	return true;
	CCS_CATCH_ALL;
	return false;
}