// CedaCohData.cpp
 
#include <stdafx.h>
#include <CedaCohData.h>
#include <resource.h>


void ProcessCohCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaCohData::CedaCohData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for COHDataStruct
	BEGIN_CEDARECINFO(COHDATA,COHDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Ctrc,"CTRC")
		FIELD_CHAR_TRIM	(Fage,"FAGE")
		FIELD_CHAR_TRIM	(Hold,"HOLD")
		//FIELD_CHAR_TRIM	(Kadc,"KADC")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		//FIELD_CHAR_TRIM	(Zifh,"ZIFH")

	END_CEDARECINFO //(COHDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(COHDataRecInfo)/sizeof(COHDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&COHDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"COH");
	//strcpy(pcmListOfFields,"CDAT,LSTU,CTRC,FAGE,HOLD,KADC,URNO,USEC,USEU,ZIFH");
	strcpy(pcmListOfFields,"CDAT,LSTU,CTRC,FAGE,HOLD,URNO,USEC,USEU");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaCohData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("CTRC");
	ropFields.Add("FAGE");
	ropFields.Add("HOLD");
	//ropFields.Add("KADC");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	//ropFields.Add("ZIFH");

	ropDesription.Add(LoadStg(IDS_STRING61514));
	ropDesription.Add(LoadStg(IDS_STRING61523));
	ropDesription.Add(LoadStg(IDS_STRING61524));
	ropDesription.Add(LoadStg(IDS_STRING61525));
	ropDesription.Add(LoadStg(IDS_STRING61526));
	//ropDesription.Add(LoadStg(IDS_STRING779));
	ropDesription.Add(LoadStg(IDS_STRING61527));
	ropDesription.Add(LoadStg(IDS_STRING61528));
	ropDesription.Add(LoadStg(IDS_STRING61529));
	//ropDesription.Add(LoadStg(IDS_STRING732));

	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	//ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	//ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaCohData::Register(void)
{
	ogDdx.Register((void *)this,BC_COH_CHANGE,	CString("COHDATA"), CString("Coh-changed"),	ProcessCohCf);
	ogDdx.Register((void *)this,BC_COH_NEW,		CString("COHDATA"), CString("Coh-new"),		ProcessCohCf);
	ogDdx.Register((void *)this,BC_COH_DELETE,	CString("COHDATA"), CString("Coh-deleted"),	ProcessCohCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaCohData::~CedaCohData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaCohData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaCohData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		COHDATA *prlCoh = new COHDATA;
		if ((ilRc = GetFirstBufferRecord(prlCoh)) == true)
		{
			omData.Add(prlCoh);//Update omData
			omUrnoMap.SetAt((void *)prlCoh->Urno,prlCoh);
		}
		else
		{
			delete prlCoh;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaCohData::Insert(COHDATA *prpCoh)
{
	prpCoh->IsChanged = DATA_NEW;
	if(Save(prpCoh) == false) return false; //Update Database
	InsertInternal(prpCoh);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaCohData::InsertInternal(COHDATA *prpCoh)
{
	ogDdx.DataChanged((void *)this, COH_NEW,(void *)prpCoh ); //Update Viewer
	omData.Add(prpCoh);//Update omData
	omUrnoMap.SetAt((void *)prpCoh->Urno,prpCoh);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaCohData::Delete(long lpUrno)
{
	COHDATA *prlCoh = GetCohByUrno(lpUrno);
	if (prlCoh != NULL)
	{
		prlCoh->IsChanged = DATA_DELETED;
		if(Save(prlCoh) == false) return false; //Update Database
		DeleteInternal(prlCoh);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaCohData::DeleteInternal(COHDATA *prpCoh)
{
	ogDdx.DataChanged((void *)this,COH_DELETE,(void *)prpCoh); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpCoh->Urno);
	int ilCohCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilCohCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpCoh->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaCohData::Update(COHDATA *prpCoh)
{
	if (GetCohByUrno(prpCoh->Urno) != NULL)
	{
		if (prpCoh->IsChanged == DATA_UNCHANGED)
		{
			prpCoh->IsChanged = DATA_CHANGED;
		}
		if(Save(prpCoh) == false) return false; //Update Database
		UpdateInternal(prpCoh);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaCohData::UpdateInternal(COHDATA *prpCoh)
{
	COHDATA *prlCoh = GetCohByUrno(prpCoh->Urno);
	if (prlCoh != NULL)
	{
		*prlCoh = *prpCoh; //Update omData
		ogDdx.DataChanged((void *)this,COH_CHANGE,(void *)prlCoh); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

COHDATA *CedaCohData::GetCohByUrno(long lpUrno)
{
	COHDATA  *prlCoh;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCoh) == TRUE)
	{
		return prlCoh;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaCohData::ReadSpecialData(CCSPtrArray<COHDATA> *popCoh,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","COH",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","COH",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popCoh != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			COHDATA *prpCoh = new COHDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpCoh,CString(pclFieldList))) == true)
			{
				popCoh->Add(prpCoh);
			}
			else
			{
				delete prpCoh;
			}
		}
		if(popCoh->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaCohData::Save(COHDATA *prpCoh)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpCoh->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpCoh->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpCoh);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpCoh->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpCoh->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpCoh);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpCoh->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpCoh->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessCohCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogCohData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaCohData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlCohData;
	prlCohData = (struct BcStruct *) vpDataPointer;
	COHDATA *prlCoh;
	if(ipDDXType == BC_COH_NEW)
	{
		prlCoh = new COHDATA;
		GetRecordFromItemList(prlCoh,prlCohData->Fields,prlCohData->Data);
		if(ValidateCohBcData(prlCoh->Urno)) //Prf: 8795
		{
			InsertInternal(prlCoh);
		}
		else
		{
			delete prlCoh;
		}
	}
	if(ipDDXType == BC_COH_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlCohData->Selection);
		prlCoh = GetCohByUrno(llUrno);
		if(prlCoh != NULL)
		{
			GetRecordFromItemList(prlCoh,prlCohData->Fields,prlCohData->Data);
			if(ValidateCohBcData(prlCoh->Urno)) //Prf: 8795
			{
				UpdateInternal(prlCoh);
			}
			else
			{
				DeleteInternal(prlCoh);
			}
		}
	}
	if(ipDDXType == BC_COH_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlCohData->Selection);

		prlCoh = GetCohByUrno(llUrno);
		if (prlCoh != NULL)
		{
			DeleteInternal(prlCoh);
		}
	}
}

//Prf: 8795
//--ValidateCohBcData--------------------------------------------------------------------------------------

bool CedaCohData::ValidateCohBcData(const long& lrpUrno)
{
	bool blValidateCohBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<COHDATA> olCohs;
		if(!ReadSpecialData(&olCohs,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateCohBcData = false;
		}
	}
	return blValidateCohBcData;
}

//---------------------------------------------------------------------------------------------------------
