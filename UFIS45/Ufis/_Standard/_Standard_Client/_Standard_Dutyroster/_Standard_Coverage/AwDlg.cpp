// AwDlg.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
//#include <bdpsuif.h>
#include <AwDlg.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AwDlg dialog


AwDlg::AwDlg(CStringArray *popColumn1, 
			 CStringArray *popColumn2, 
			 CStringArray *popUrnos, 
			 CString opHeader1,
			 CString opHeader2,
			 CWnd* pParent /*=NULL*/)
	: CDialog(AwDlg::IDD, pParent)
{
	pomColumn1 = popColumn1;
	pomColumn2 = popColumn2;
	omHeader1  = opHeader1;
	omHeader2  = opHeader2;
	pomUrnos   = popUrnos;

	pomGrid	   = NULL;

	lmReturnUrno = 0;
	bmMultipleSelection = false;

	//{{AFX_DATA_INIT(AwDlg)
	//}}AFX_DATA_INIT
}

AwDlg::~AwDlg()
{
	delete pomGrid;
}
void AwDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AwDlg)
	DDX_Control(pDX, IDOK, m_OK);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AwDlg, CDialog)
	//{{AFX_MSG_MAP(AwDlg)
	ON_MESSAGE(WM_GRID_LBUTTONDBLCLK,	OnGridLButtonDblclk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// AwDlg message handlers

void AwDlg::OnOK() 
{
	CRowColArray olRows;
	int ilSelCount = (int)pomGrid->GetSelectedRows( olRows);
	if (bmMultipleSelection)
	{
		CString olReturnString;
		long	llReturnUrno;

		for (int i = 0; i < ilSelCount; i++)
		{
			if (pomColumn1 != NULL)
			{
				olReturnString = pomGrid->GetValueRowCol(olRows[i], 1);
				llReturnUrno   = atol(pomGrid->GetValueRowCol(olRows[i],3));		
			}
			else
			{
				olReturnString = pomGrid->GetValueRowCol(olRows[i], 1);
				llReturnUrno   = atol(pomGrid->GetValueRowCol(olRows[i],2));		
			}

			this->omReturnStrings.Add(olReturnString);
			this->omReturnUrnos.Add(llReturnUrno);
		}
	}
	else 
	{
		if (ilSelCount != 1)
		{
			MessageBox(LoadStg(IDS_STRING61419),LoadStg(IDS_STRING61420),MB_ICONSTOP);
			return;
		}
		CGXStyle olStyle;

		if (pomColumn1 != NULL)
		{
			omReturnString = pomGrid->GetValueRowCol(olRows[0], 1);
			lmReturnUrno   = atol(pomGrid->GetValueRowCol(olRows[0],3));		
		}
		else
		{
			omReturnString = pomGrid->GetValueRowCol(olRows[0], 1);
			lmReturnUrno   = atol(pomGrid->GetValueRowCol(olRows[0],2));		
		}
	}

	CDialog::OnOK();
}

BOOL AwDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if (pomColumn1 != NULL)
		pomGrid = new CGridControl(this,IDC_GRID,3,pomColumn1->GetSize());
	else
		pomGrid = new CGridControl(this,IDC_GRID,2,pomColumn2->GetSize());

	InitGrid();

	FillGrid();

	return TRUE; 
}

LONG AwDlg::OnGridLButtonDblclk(UINT wParam, LONG lParam) 
{
	GRIDNOTIFY *prlNotify = (GRIDNOTIFY *)lParam;
	OnOK();
	return 0L;
}

void AwDlg::InitGrid()
{
	pomGrid->GetParam()->EnableSelection(GX_SELROW);

	if (pomColumn1 != NULL)
	{
		pomGrid->SetValueRange(CGXRange(0, 1), omHeader1);
		pomGrid->SetValueRange(CGXRange(0, 2), omHeader2);
		pomGrid->SetColWidth(0,0,30);
		pomGrid->SetColWidth(1,1,100);
		pomGrid->SetColWidth(2,2,200);
	}
	else
	{
		pomGrid->SetValueRange(CGXRange(0, 1), omHeader2);
		pomGrid->SetColWidth(0,0,30);
		pomGrid->SetColWidth(1,1,300);
	}


	//Spaltenbreite und Zeilenh�he k�nnen nicht ver�ndert werden
	pomGrid->GetParam()->EnableTrackColWidth(FALSE);
	pomGrid->GetParam()->EnableTrackRowHeight(FALSE);
    pomGrid->GetParam()->EnableSelection(GX_SELROW);

	//Scrollbars anzeigen wenn n�tig
	pomGrid->SetScrollBarMode(SB_BOTH, gxnAutomatic, TRUE);

}

void AwDlg::FillGrid()
{
	// Grid mit Daten f�llen.
	pomGrid->GetParam()->SetLockReadOnly(false);
	pomGrid->SetReadOnly(false);

	//*** FILL THE GRID WITH EXISTING DATA ***
	if (pomColumn1 != NULL)
	{
		for(int i = 0; i < pomColumn1->GetSize(); i++)
		{
			pomGrid->SetValueRange(CGXRange(i+1, 1),pomColumn1->GetAt(i));
			if (i < pomColumn2->GetSize())
				pomGrid->SetValueRange(CGXRange(i+1, 2),pomColumn2->GetAt(i));

			if (i < this->pomUrnos->GetSize())
				pomGrid->SetValueRange(CGXRange(i+1, 3),pomUrnos->GetAt(i));
		}

		pomGrid->HideCols(3,3);
	}
	else
	{
		for(int i = 0; i < pomColumn2->GetSize(); i++)
		{
			pomGrid->SetValueRange(CGXRange(i+1, 1),pomColumn2->GetAt(i));

			if (i < this->pomUrnos->GetSize())
				pomGrid->SetValueRange(CGXRange(i+1, 2),pomUrnos->GetAt(i));
		}

		pomGrid->HideCols(2,2);
		
	}

	// Anzeige einstellen
	pomGrid->GetParam()->SetLockReadOnly(true);
	pomGrid->SetReadOnly(true);
}
