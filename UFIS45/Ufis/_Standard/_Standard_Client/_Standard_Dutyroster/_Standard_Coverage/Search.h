// Search.h: interface for the CSearch class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SEARCH_H__AA7520F3_4575_11D3_94FB_00001C018ACE__INCLUDED_)
#define AFX_SEARCH_H__AA7520F3_4575_11D3_94FB_00001C018ACE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <CedaFlightData.h>
#include <SearchResultsDlg.h>


class CSearch  
{
public:
	CSearch();
	virtual ~CSearch();

	bool SearchFlightData(CString opWhere);
	bool SendCreateDemData(bool bpCreateFdep,CString opWhere,bool bpCreateFid,CString opFidStart,CString opFidEnd,bool bpIsSingleFlight,bool bpRuleDiagnosis);

	void AttachTsrArray(const CStringArray * popTsrArray);

	void AttachFlightDemMap(const CMapPtrToPtr * popFlightDemMap);

private:
	CedaFlightData					omFlights;
	CMapPtrToPtr					omDemMap;
	CMapPtrToPtr					omUrnoMap;
	CCSPtrArray<FLIGHTSEARCHDATA>	omSearchFlight;

	const CMapPtrToPtr				*pomFlightDemMap;
	SearchResultsDlg				*pomResultDlg;
	const CStringArray				*pomTsrArray;
};

#endif // !defined(AFX_SEARCH_H__AA7520F3_4575_11D3_94FB_00001C018ACE__INCLUDED_)
