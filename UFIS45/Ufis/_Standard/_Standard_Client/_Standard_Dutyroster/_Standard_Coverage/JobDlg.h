#if !defined(AFX_JOBDLG_H__A6016CF1_962E_11D1_A3C6_0080AD1DC9A3__INCLUDED_)
#define AFX_JOBDLG_H__A6016CF1_962E_11D1_A3C6_0080AD1DC9A3__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// JobDlg.h : header file
//


#include <CCSEdit.h>
#include <CedaFlightData.h>
#include <CedaDemData.h>
#include <DispoDetailGanttViewer.h>
#include <DispoDetailGantt.h>
#include <CViewer.h>

#include <GridFenster.h>

/////////////////////////////////////////////////////////////////////////////
// CJobDlg dialog

class CJobDlg : public CDialog
{
// Construction
public:
	CJobDlg(CWnd* pParent = NULL);   // standard constructor
	~CJobDlg();
	CString omCalledFrom;

	CBrush omBrush;


	DispoDetailGanttViewer	*pomViewer;
	DispoDetailGantt		*pomGantt;
    CCSTimeScale omTimeScale;
	CCSDragDropCtrl m_DragDropSource;
	CCSDragDropCtrl m_DragDropTarget;
	int imVerticalScaleIndent;
	int imLastSelectedUrno;
    CTimeSpan omDuration;
    CTimeSpan omTSDuration;
	CTime omStartTime;
	CTime omTSStartTime;
	DEMDATA *prmCurrentDem;
	long lmDepUrno;
	long lmArrUrno;
	bool bmNoEditChangeNow;
	CUIntArray omPfcUrnos;
	CUIntArray omGegUrnos;
	
// Dialog Data
	//{{AFX_DATA(CJobDlg)
	enum { IDD = IDD_JOB_DLG };
	CEdit	m_Act5;
	CEdit	m_Redu;
	CButton	m_ArrGroup;
	CButton	m_DepGroup;
	CButton	m_D_FTYP;
	CButton	m_A_FTYP;
	CEdit	m_Regn;
	CEdit	m_Actl;
	CEdit	m_A_ALC3;
	CEdit	m_A_BAGN;
	CEdit	m_A_ETAI;
	CEdit	m_A_FLNS;
	CEdit	m_A_FLTN;
	CEdit	m_A_GTA1;
	CEdit	m_A_ORG3;
	CEdit	m_A_PAX1;
	CEdit	m_A_PSTA;
	CEdit	m_A_STOA;
	CEdit	m_A_VIA3;
	CEdit	m_A_BAGW;
	CEdit	m_D_ALC3;
	CEdit	m_D_BAGN;
	CEdit	m_D_CGOT;
	CEdit	m_D_DES3;
	CEdit	m_D_ETDI;
	CEdit	m_D_FLNS;
	CEdit	m_D_FLTN;
	CEdit	m_D_GTD1;
	CEdit	m_D_MAIL;
	CEdit	m_D_PAX1;
	CEdit	m_D_PSTD;
	CEdit	m_D_VIA3;
	CEdit	m_A_CGOT;
	CEdit	m_A_MAIL;
	CEdit	m_D_STOD;
	CEdit	m_D_BAGW;
	CStatic m_Duties;
	CStatic m_Statusbar;
	CScrollBar m_Scrollbar;
	
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CJobDlg)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
public:
	LONG OnCCSEditKillFocus( UINT wParam, LPARAM lParam);
	void SetOwner(CWnd *popNewOwner);
	void SetCallerViewer(CViewer *popCallerView);
	CViewer *GetCallerViewer();
	// Generated message map functions
	//{{AFX_MSG(CJobDlg)
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
	afx_msg void OnDestroy();

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	
	void InitFlightFields(CWnd *popNewOwner, const CStringArray *popTsrArray,CMapPtrToPtr *popDemMap,FLIGHTDATA *popArrFlight,FLIGHTDATA *popDepFlight, long lpArrUrno, long lpDepUrno,CViewer *popCallerView);
	//BOOL OnInitDialog(CedaFlightData *popCedaFlightData, long lpArrUrno, long lpDepUrno);
	void MakeNewGantt(CTime opStartTime);
	void SetTSStartTime(CTime opTSStartTime);
	void ChangeViewTo();
	void ProcessDemChanges ( int ipDDXType, DEMDATA *prpDem );
private:
	static	void ProcessJobDlgCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
			void UpdateFlightFields(FLIGHTDATA *popArrFlight,FLIGHTDATA *popDepFlight,bool bpUpdate);
private:
	CWnd *pomOwner;
	void ClearFlightFields(void);

	void FillGrids(CString opDisplayedFields);


	CString IntToAsc(int ipInt);
	CViewer *pomCallerView;

	CGridFenster *pomDepatureList;
	CGridFenster *pomArrivalList;

	FLIGHTDATA *prmArrFlight, *prmDepFlight;

	const CStringArray *pomTsrArray;

	CMapPtrToPtr omDemMap;

};

static void DemChangeCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_JOBDLG_H__A6016CF1_962E_11D1_A3C6_0080AD1DC9A3__INCLUDED_)
