// CovDiagram.cpp : implementation file
//

#include <stdafx.h>
#include <resource.h>
#include <CovDiagram.h>
#include <CCSEdit.h> 
//#include <ButtonListDlg.h>
#include <InitialLoadDlg.h>
#include <FlightTablePropertySheet.h>
#include <PrivList.h>
#include <CedaFlightData.h>
#include <CedaBasicData.h>
#include <process.h>
//#include <CedaDsrData.h>
//#include <CedaGhdData.h>
#include <CedaPfcData.h>
#include <CViewer.h>
#include <DataSet.h>
#include <CedaDemData.h>
#include <CoverageChart.h>
//#include <StaffGantt.h>
#include <TimePacket.h>
#include <ConflictCheck.h>
#include <onflictDlg.h>
#include <StringConst.h>
#include <CoverageGraphicWnd.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
//#include <MasterShiftDemand.h>

#include <LoadTimeSpanDlg.h>
#include <NoDemFlights.h>
#include <WaitDlg.h>
#include <SetupSheet.h>
#include <Coverage.h>	//hag20030401


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

HDDEDATA CALLBACK DdeCallback(UINT, UINT, HCONV, HSZ, HSZ, HDDEDATA, DWORD, DWORD);

CCoverageStatistics *pogCoverageStatistics = NULL;
static void CovDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

static void CovDiagramNewBsd(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))


CovDiagram *pogCoverageDiagram;
/////////////////////////////////////////////////////////////////////////////
// CovDiagram
 int igAnsichtPageIndex;
//MWO: make it flexible, because when BCCOM is connected the timer value is 5 minutes
//otherwise 1 minute.
long imTimerValue = 60000;

#define COMBO_BOX_WIDTH  180 
#define TOGGLEBUTTON_WIDTH  100 


//-------------------------------------------------------------------------------------------------------------------

void  ProcessTrafficLight(long BcNum, int ipState)
{
	if(pogCoverageDiagram != NULL)
	{
		pogCoverageDiagram->UpdateTrafficLight(ipState);
	}
}

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


IMPLEMENT_DYNCREATE(CovDiagram, CFrameWnd)

CovDiagram::CovDiagram()
{
	EnableAutomation();

	CCS_TRY
	igAnsichtPageIndex = -1;
	omMaxTrackSize = CPoint(4000/*1024*/, 1500);
	omMinTrackSize = CPoint(1024 / 4, 768 / 4);
	omViewer.SetViewerKey("COVERAGEDIA");
	omViewer.SelectView("<Default>");
    imStartTimeScalePos = 124;
    imStartTimeScalePos++;              // plus one for left border of chart
    imFirstVisibleChart = 0;
	lmBkColor = lgBkColor;
	pogCoverageDiagram = this;
	bmIsViewOpen = false;
	bmNoGraphicUpdateNow = false;
	pomConflict = NULL;
	pomSimulation = NULL;

	omViewer.PrepareEvaluationFilter("<Default>");
	omViewer.PrepareBaseShiftsFilter("<Default>");

	imWindowCount = 3;
	for(int illc1 = 0; illc1 < imWindowCount; illc1++)
	{
		bmShowWindows[illc1] = ogBasicData.bmShowWindows[illc1];
	}

	

	for(int illc = 0 ;illc < ogBasicData.omLoadTimeValues.GetSize(); illc++)
	{
		CString olTmp = ogBasicData.omLoadTimeValues.GetAt(illc);
		omLoadTimeValues.Add(ogBasicData.omLoadTimeValues.GetAt(illc));
	}

	imAbsFlag = (ogBasicData.GetLoadRelFlag())?1:0;
 
	bmSearchIsOpen = false;
	pomSearchProbertySheetDlg = NULL;
	pomCreateDemProbertySheetDlg = NULL;
	pomAutoCoverageDlg = NULL;

	CTime olTime1 = TIMENULL;
	CTime olTime2 = TIMENULL;

	ogBasicData.GetDiagramStartTime(olTime1,olTime2);
	pomSearchProbertySheetDlg = new SearchPropertySheet(LoadStg(IDS_STRING61212),this,0,true,SEARCH,olTime1,olTime2);
	pomCreateDemProbertySheetDlg = new SearchPropertySheet(LoadStg(IDS_STRING61234),this,0,true,CREATEDEM,olTime1,olTime2);
	CStringArray olTmpViews;
	CStringArray olBasicShiftViews;

	CString olOldBaseViewName = omViewer.GetBaseViewName();
	CString olOldViewName = omViewer.GetViewName();
	CString olViewName;
		
	omViewer.SetViewerKey("BASISSCHICHTEN");
	
	omViewer.GetViews(olTmpViews);
	CString olText;
	for (int ilIndex = 0; ilIndex < olTmpViews.GetSize(); ilIndex++)
	{
		olText = olTmpViews[ilIndex] + ";P;0;100";
		olBasicShiftViews.Add(olText);
	}
	
	omViewer.SetViewerKey(olOldBaseViewName);
	omViewer.SelectView(olOldViewName);

	omViewer.SetLoadTimes(olTime1,olTime2);

	ogBcHandle.SetTrafficLightCallBack(ProcessTrafficLight);
	CCS_CATCH_ALL
}

CovDiagram::~CovDiagram()
{
	CCS_TRY

	omLoadTimeValues.RemoveAll();
    omPtrArray.RemoveAll();
	pogCoverageDiagram = NULL;
	if(pogCoverageStatistics!=NULL)
	{
		delete pogCoverageStatistics;
		pogCoverageStatistics=NULL;
	}
	if(pomConflict!=NULL)
	{
		pomConflict->DestroyWindow();
		delete pomConflict;
		pomConflict = NULL;
	}
	if(pomSearchProbertySheetDlg!=NULL)
	{
		if(bmSearchIsOpen)
			pomSearchProbertySheetDlg->DestroyWindow();
		delete pomSearchProbertySheetDlg;
		pomSearchProbertySheetDlg = NULL;
	}
	if(pomCreateDemProbertySheetDlg!=NULL)
	{
		delete pomCreateDemProbertySheetDlg;
		pomCreateDemProbertySheetDlg = NULL;
	}
	if(pomAutoCoverageDlg!=NULL)
	{
		delete pomAutoCoverageDlg;
		pomAutoCoverageDlg = NULL;
	}
	
	if(pomSimulation!=NULL)
	{
		delete pomSimulation;
	}

	CCS_CATCH_ALL
}


BEGIN_MESSAGE_MAP(CovDiagram, CFrameWnd)
	//{{AFX_MSG_MAP(CovDiagram)
	ON_WM_CREATE()
	ON_WM_DESTROY()
    ON_WM_GETMINMAXINFO()
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
    ON_WM_SIZE()
    ON_WM_HSCROLL()
    ON_BN_CLICKED(IDC_PREV, OnNextChart)
    ON_BN_CLICKED(IDC_NEXT, OnPrevChart)
    ON_WM_TIMER()
    ON_MESSAGE(WM_DEACTIVEAUTOCOV, OnDeactivateAutoCov)
	ON_MESSAGE(WM_AD_BUTTON,OnADButton)
    ON_MESSAGE(WM_ACTIVEAUTOCOV, OnActivateAutoCov)
    ON_MESSAGE(WM_POSITIONCHILD, OnPositionChild)
	ON_MESSAGE(WM_UPDATE_COVERAGE, UpdateGraphic)
	ON_MESSAGE(WM_UPDATE_BASESHIFT, UpdateBaseShift)
	ON_MESSAGE(WM_UPDATEDEMANDGANTT, UpdateDemandGantt)
	ON_MESSAGE(WM_DEMAND_UPDATECOMBO, UpdateDemandCombo)
    ON_MESSAGE(WM_UPDATEDIAGRAM, OnUpdateDiagram)
	ON_MESSAGE(WM_BCADD,OnBcAdd)
	ON_MESSAGE(WM_MARKTIME, OnSetMarkTime)
	ON_MESSAGE(WM_SIMULATE, OnSimulate)
	ON_MESSAGE(WM_SEARCH_EXIT, OnSearchExit)
	ON_CBN_SELCHANGE(IDC_VIEW, OnViewSelChange)
	ON_CBN_CLOSEUP(IDC_VIEW, OnCloseupView)
	ON_BN_CLICKED(IDC_RULES, OnCheckDemands)
	ON_BN_CLICKED(IDC_NOREFLIGHTS, OnNoResList)
	ON_BN_CLICKED(IDC_TEST, OnTest)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_CLOSE()
	ON_COMMAND(ID_LOADTIMEFRAME, OnLoadtimeframe)
	ON_COMMAND(ID_MASSTAB, OnMasstab)
	ON_COMMAND(ID_SEARCH, OnSearch)
    ON_MESSAGE(WM_UPDATEDEMANDBTN, OnUpdateDemandBtn)
	ON_COMMAND(ID_SETUP, OnSetup)
    ON_MESSAGE(WM_LOAD_TIMEFRAME,OnMsgLoadTimeFrame)
    ON_MESSAGE(WM_UPDATETOOLBAR,OnUpdateToolbar)
	ON_MESSAGE(WM_DEMAND_SELECTCOMBO, SelectDemandCombo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


BEGIN_DISPATCH_MAP(CovDiagram, CFrameWnd)
	//{{AFX_DISPATCH_MAP(CovDiagram)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_ICovDiagram to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {6F918F31-D53A-11D2-A189-0000B4984BBE}
static const IID IID_ICovDiagram =
{ 0x6f918f31, 0xd53a, 0x11d2, { 0xa1, 0x89, 0x0, 0x0, 0xb4, 0x98, 0x4b, 0xbe } };

BEGIN_INTERFACE_MAP(CovDiagram, CFrameWnd)
	INTERFACE_PART(CovDiagram, IID_ICovDiagram, Dispatch)
END_INTERFACE_MAP()



static void CovDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	CCS_TRY
	CovDiagram *polDiagram = (CovDiagram *)popInstance;


	CCS_CATCH_ALL
}

void CovDiagram::UpdateTrafficLight(int ipState)
{
	CoverageChart* polChart = NULL;
	polChart = GetCoverageChart();

	if(polChart != NULL) //MWO: Hier war Absturz
	{
		KillTimer(3);
		if(ipState == 0)
		{
			polChart->omBCStatus.SetColors(RGB(236, 236,   0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			polChart->omBCStatus.UpdateWindow();
		}
		if(ipState == 1)
		{
			imBroadcastState = 2;
			polChart->omBCStatus.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			polChart->omBCStatus.UpdateWindow();
		}

		SetTimer(3, imTimerValue, NULL);
	}
}
/////////////////////////////////////////////////////////////////////////////
// CovDiagram message handlers
void CovDiagram::PositionChild()
{
	CCS_TRY
    CRect olRect;
    CRect olChartRect;
    CoverageChart *polChart;
    int ilGraphicChartHeight = 0;
	double dlFreeSpace = 1;
    int ilChartHeight;
    int ilActHeight = 0;
    int ilActWindows = 0;
    int ilLastY = 0;
    omClientWnd.GetClientRect(&olRect);

    for (int ilIndex = 1; ilIndex < omPtrArray.GetSize(); ilIndex++)
    {
		if (bmShowWindows[ilIndex])
		{
			polChart = (CoverageChart *) omPtrArray.GetAt(ilIndex);
			if(polChart->GetState() == Minimized)
			{
				ilActHeight += polChart->GetHeight();
			}
			else
			{
				ilActWindows++;
			}
		}
	}
	if(bmShowWindows[0])
	{
		polChart = (CoverageChart *) omPtrArray.GetAt(0);
		if(polChart->GetState() == Minimized)
		{
			ilGraphicChartHeight = polChart->GetHeight();
		}
		else
		{
			switch(imWindowCount)
			{
			case 1: 
				ilGraphicChartHeight = olRect.Height() - ilActHeight;
				break;
			case 2: 
				if(ilActWindows == 0)
					ilGraphicChartHeight = (int) (1 * (olRect.Height() - ilActHeight));
				if(ilActWindows == 1)
					ilGraphicChartHeight = (int) (0.6 * (olRect.Height() - ilActHeight));
				break;
			case 3: 
				if(ilActWindows == 0)
					ilGraphicChartHeight = (int) (1 * (olRect.Height() - ilActHeight));
				if(ilActWindows == 1)
					ilGraphicChartHeight = (int) (0.6 * (olRect.Height() - ilActHeight));
				if(ilActWindows == 2)
					ilGraphicChartHeight = (int) (0.45 * (olRect.Height() - ilActHeight));
				break;
			}
		}
		ilActHeight += ilGraphicChartHeight;
	}


	if(ilActWindows > 0)
	{
		ilChartHeight = (int) (olRect.Height() - ilActHeight)/ilActWindows;
	}
	else
	{
		ilChartHeight = (int) olRect.Height() - ilActHeight;
	}


    for ( ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
    {
		if (bmShowWindows[ilIndex])
		{
			polChart = (CoverageChart *) omPtrArray.GetAt(ilIndex);
        
			polChart->GetClientRect(&olChartRect);
			olChartRect.right = olRect.right;

			olChartRect.top = ilLastY;

			if(polChart->GetState() == Minimized)
			{
				ilLastY += polChart->GetHeight();
			}
			else
			{
				if(ilIndex == 0)
				{
					ilLastY += ilGraphicChartHeight;
				}
				else
				{
					ilLastY += ilChartHeight;
				}
			}
			olChartRect.bottom = ilLastY;
        
			// check
			if ((polChart->GetState() != Minimized) &&
				(olChartRect.top < olRect.bottom) && (olChartRect.bottom > olRect.bottom))
			{
				olChartRect.bottom = olRect.bottom;
				polChart->SetState(Normal);
			}
			//
        
			polChart->MoveWindow(&olChartRect, FALSE);
			ChartWindowVisible(ilIndex);
		}
	}
    
	omClientWnd.Invalidate(TRUE);
	//SetAllStaffAreaButtonsColor();
	OnUpdatePrevNext();
	UpdateTimeBand();
	CCS_CATCH_ALL
}


void CovDiagram::SetTSStartTime(CTime opTSStartTime)
{
	CCS_TRY
    omTSStartTime = opTSStartTime;
    if ((omTSStartTime < omStartTime) ||
		(omTSStartTime > omStartTime + omDuration))
    {
		omTSStartTime = omStartTime;
	}
        
	CTime olTmpStartTime = omTSStartTime + CTimeSpan(0,1,2,0);
    char clBuf[16];
	int ilDayOffWeek = olTmpStartTime.GetDayOfWeek() -1;
	CStringArray olItemList;
	ExtractItemList(LoadStg(IDS_DAYSOFWEEK),&olItemList, '|');
	CString olText = "";
	if(olItemList.GetSize() > ilDayOffWeek)
	{
		olText = olItemList[ilDayOffWeek];
	}


    sprintf(clBuf, "%s %02d%02d%02d",olText,
        olTmpStartTime.GetDay(), olTmpStartTime.GetMonth(), olTmpStartTime.GetYear() % 100);
    omTSDate.SetWindowText(clBuf);
    omTSDate.Invalidate(FALSE);
	CCS_CATCH_ALL
}



void CovDiagram::SetCaptionText(void)
{
	CCS_TRY
	SetWindowText("Coverage-Toolbox"/*LoadStg(IDS_STRING559)*/);
	CCS_CATCH_ALL
}

int CovDiagram::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
//	CCS_TRY
//	lpCreateStruct.


//	CWaitDlg olWaitDlg(NULL,"Preparing Data. Please wait!");			// show the waitdlg

	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;


		m_hIcon = AfxGetApp()->LoadIcon(IDI_UFIS);

		SetIcon(m_hIcon, TRUE);		// Set big icon
		SetIcon(m_hIcon, FALSE);		// Set small icon

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME1))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}


	int index = m_wndToolBar.CommandToIndex(IDC_TOGGLEBUTTON);
	
	CRect olToggleRect;
    m_wndToolBar.GetItemRect(index , &olToggleRect);

	//--- Prepare ComboBox

	// get index of IDP_PLACEHOLDER
	index = m_wndToolBar.CommandToIndex(IDP_PLACEHOLDER);
	
	CRect olComboRect;
    m_wndToolBar.SetButtonInfo(index , IDP_PLACEHOLDER , TBBS_SEPARATOR     , 180);
    m_wndToolBar.GetItemRect(index , &olComboRect);
	
	olComboRect.left = olToggleRect.right + 1;
	olComboRect.right = olComboRect.left + 160;//olComboRect.Width();
	olComboRect.top += 2;
	olComboRect.bottom += 200;
	

	// create combobox
	if(!m_wndToolBar.pomComboBox->Create(WS_CHILD | WS_VISIBLE | CBS_AUTOHSCROLL | WS_VSCROLL | CBS_DISABLENOSCROLL | CBS_DROPDOWNLIST | CBS_HASSTRINGS, 
										 olComboRect, &m_wndToolBar, IDC_VIEW))
	{
		TRACE0("Problem mit Combo Box!!");
		return FALSE;
	};

      m_wndToolBar.pomComboBox->ShowWindow(SW_SHOW);
	  
	//-- exception handling

	  CRect olRect(0,2,60,30);
/********************************	

	CRect olRect(0,2,60,30);

	if (m_wndToolBar.Create(this) )
	{
		//--- AddToggleButton
		
		// get index

		CButton *polButton = m_wndToolBar.AddButton ( "Ansicht",BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE, 
												   olRect, &m_wndToolBar, IDC_TOGGLEBUTTON );
		
		if ( !polButton )
		{
			TRACE0("Problem with Toggle Button!!");
			return FALSE;
		};


		HICON h_icon = AfxGetApp()->LoadIcon(IDI_UFIS);
		polButton->SetIcon(h_icon);
		//--- Add ComboBox
	
		olRect.top = 0;
		olRect.left = 65;
		olRect.right = 280;
		olRect.bottom = 245;
		CComboBox *polCB = m_wndToolBar.AddComboBox ( WS_CHILD | WS_VISIBLE | CBS_AUTOHSCROLL | CBS_DROPDOWNLIST | CBS_HASSTRINGS, 
															 olRect, &m_wndToolBar, IDC_VIEW  );
		
		if ( !polCB )
		{
			TRACE0("Problem with Combo Box!!");
			return FALSE;
		};

		if ( !m_wndToolBar.AddScrollToolBar ( IDR_MAINFRAME ) )
		{
			TRACE0("Failed to create toolbar\n");
			return -1;      // fail to create
		}

		m_wndToolBar.MoveWindow(olRect);

	
	}

	//-- exception handling

**************************/	
	TranslateMenu();

    SetTimer(0, (UINT) 60 * 1000, NULL);
    SetTimer(1, (UINT) 60 * 1000 * 2, NULL);

/*	Cylic demand update no longer necessary
    SetTimer(2, (UINT)      1000 * atoi(ogCfgData.rmUserSetup.DET1), NULL);
    SetTimer(4, (UINT)      1000 * atoi(ogCfgData.rmUserSetup.DET2), NULL);
*/
	ogDdx.Register((void *)this,UPDATE_SETUP,CString("UPDATE_SETUP"), CString("UPDATE_SETUP"),ProcessCf);

	// establish watch dog ?
	char pclConfigPath[512];
	char pclBroadcastCheck[512];

    // retrieve the configuration path from OS environment
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(pcgAppName, "BROADCASTCHECK", "NO",pclBroadcastCheck, sizeof pclBroadcastCheck, pclConfigPath);
	// install watch dog as timer event 3, because timer event 2 is already used by AUTOSCROLL_TIMER_EVENT
	// we will be notified all minute
	if (stricmp(pclBroadcastCheck,"YES") == 0)
	{
		bmBroadcastCheckMsg = true;
	}
	else
	{
		bmBroadcastCheckMsg = false;
	}

	char pclBcMethod[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclBcMethod, sizeof pclBcMethod, pclConfigPath);
	if (stricmp(pclBcMethod,"BCCOM") == 0)
	{
		imTimerValue = 60 * 5000;
	}
	SetTimer(3, imTimerValue, NULL);

	ogBcHandle.AddTableCommand(CString("TIMERS"), CString("TIME"), BROADCAST_CHECK,true);
	ogDdx.Register((void *)this,BROADCAST_CHECK,CString("BROADCAST_CHECK"), CString("Broadcast received"),ProcessCf);
	imBroadcastState = 0;

	char pclImportBc[512];
	GetPrivateProfileString(pcgAppName, "IMPORTBC", "",pclImportBc, sizeof pclImportBc, pclConfigPath);
	if (strlen(pclImportBc) > 0)
	{
		omImportBcFilePath = pclImportBc;

		char pclImportBcTimer[512];
		GetPrivateProfileString(pcgAppName, "IMPORTBCTIMER", "100",pclImportBcTimer, sizeof pclImportBcTimer, pclConfigPath);
	    SetTimer(5, (UINT) atol(pclImportBcTimer) , NULL);
	}
	else
	{
		bmBroadcastCheckMsg = false;
	}

	ogCovFlightData.Register();

    omViewer.Attach(this);
	UpdateComboBox();

    GetClientRect(&olRect);
	CRect olTBRect;
	m_wndToolBar.GetWindowRect(&olTBRect);
	m_wndToolBar.ClientToScreen(&olTBRect);
	ScreenToClient(&olTBRect);

	olRect.top += olTBRect.Height();

	CTime olEndTime;

	ogBasicData.GetDiagramStartTime(omStartTime,olEndTime);
    omTSStartTime = omStartTime;

    CString olS1 = omStartTime.Format("%d.%m.%Y/%H:%M");
	CString olS2 = olEndTime.Format("%d.%m.%Y/%H:%M");
	omCaptionText = "       " + LoadStg(IDS_STRING61211) + olS1 + "  -  " + olS2 +"          ";

    omDuration = olEndTime - omStartTime;
    omTSDuration = min (CTimeSpan(0, ogBasicData.GetDisplayDuration(), 0, 0), omDuration);
    omTSInterval = CTimeSpan(0, 0, 10, 0);

	SetTimeBand(omStartTime,olEndTime);

	//Die wollen local
    CTime olUCT = omTSStartTime + CTimeSpan(0,1,2,0);
    char olBuf[16];

 	int ilDayOffWeek = omTSStartTime.GetDayOfWeek() -1;
	CStringArray olItemList;
	ExtractItemList(LoadStg(IDS_DAYSOFWEEK),&olItemList, '|');
	CString olText = "";
	if(olItemList.GetSize() > ilDayOffWeek)
	{
		olText = olItemList[ilDayOffWeek];
	}

         
    sprintf(olBuf, "%s %02d%02d%02d",olText,
        omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100);
    int ilPos = (imStartTimeScalePos - olRect.left - 80) / 2;
    omTSDate.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(ilPos, 55-5, ilPos + 80, 72-5), this);

	omTimeSlider.Create( TBS_BOTTOM  , CRect(ilPos, 55 +10, ilPos + 80, 72+10), this,IDC_TIMESLIDER );
	omTimeSlider.SetRange(0, 120);
	omTimeSlider.SetTicFreq(10);
    omTimeSlider.SetPos(omTSDuration.GetTotalHours() * ONE_HOUR_POINT);
    omTimeSlider.ShowWindow(SW_SHOW);


	omTimeScale.lmBkColor = lgBkColor;
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE|WS_BORDER,
        CRect(imStartTimeScalePos, 48, olRect.right - (  2), 82), this, 0, NULL);
    omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
    

	omViewer.ChangeViewTo(omViewer.SelectView(), omTSStartTime, omTSStartTime + omTSDuration);


    // must be static
    static UINT ilIndicators[] = { ID_SEPARATOR };
    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,AFX_IDW_STATUS_BAR);
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));

    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH | SBPS_NORMAL, cxWidth);

    olRect.top += 84;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    omClientWnd.Create(NULL, "ClientWnd", WS_CHILD /*| WS_HSCROLL */ | WS_VISIBLE, olRect, this,
        0 /* IDD_CLIENTWND */, NULL);
        

    CoverageChart *polChart = NULL;
    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
    imFirstVisibleChart = 0;
	OnUpdatePrevNext();
    int ilLastY = 0;
    for (int ilI = 0; ilI < 3; ilI++)
    {
		switch(ilI)
		{
		case 0://Coverage
			{
				olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
				polChart = new CoverageChart(COVERAGE_GRAPHIC);
				polChart->SetTimeScale(&omTimeScale);
				polChart->SetViewer(&omViewer, ilI);
				polChart->SetStatusBar(&omStatusBar);
				polChart->SetStartTime(omTSStartTime);
				polChart->SetInterval(omTimeScale.GetDisplayDuration());
				polChart->SetState(Maximized);
				polChart->Create(NULL, ""/*LoadStg(IDS_STRING563)*/, WS_OVERLAPPED | WS_BORDER | WS_CHILD| WS_VISIBLE,
					olRect, &omClientWnd,
					0 /* IDD_CHART */, NULL);
        
				omPtrArray.Add(polChart);
				
				ilLastY += polChart->GetHeight();
			}
			break;
		case 1://Shift-Demands
			{
				olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
				polChart = new CoverageChart(COVERAGE_SHIFTDEMANDS);
				polChart->SetTimeScale(&omTimeScale);
				polChart->SetViewer(&omViewer, ilI);
				polChart->SetStatusBar(&omStatusBar);
				polChart->SetStartTime(omTSStartTime);
				polChart->SetState(Maximized);
				
				polChart->SetTimeFrame(omStartTime, omStartTime + omDuration);
				polChart->SetInterval(omTimeScale.GetDisplayDuration());
				polChart->Create(NULL, ""/*LoadStg(IDS_STRING563)*/, WS_OVERLAPPED | WS_BORDER | WS_CHILD| WS_VISIBLE,
					olRect, &omClientWnd,
					0 /* IDD_CHART */, NULL);
        
				omPtrArray.Add(polChart);

				ilLastY += polChart->GetHeight();
			}
			break;
		case 2://Basic-Shifts
			{
				olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
        
				polChart = new CoverageChart(COVERAGE_BASESHIFTS);
				polChart->SetTimeScale(&omTimeScale);
				polChart->SetViewer(&omViewer, ilI);
				polChart->SetStatusBar(&omStatusBar);
				polChart->SetStartTime(omTSStartTime);
				polChart->SetInterval(omTimeScale.GetDisplayDuration());
				polChart->SetState(Maximized);

				polChart->Create(NULL, ""/*LoadStg(IDS_STRING563)*/, WS_OVERLAPPED | WS_BORDER | WS_CHILD| WS_VISIBLE,
					olRect, &omClientWnd,
					0 /* IDD_CHART */, NULL);
        
				omPtrArray.Add(polChart);

				ilLastY += polChart->GetHeight();
			}
			break;
		default:
			break;
		}
    }

//	olWaitDlg.CloseWindow();
	
    OnTimer(0);

		// This will fix the bug for the horizontal scroll bar in the preplan mode
    long llTSMin = (omTSStartTime - omStartTime).GetTotalMinutes();
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
    SetScrollRange(SB_HORZ, 0, 1000, FALSE);
    SetScrollPos(SB_HORZ, (int) (0), FALSE);


	m_wndToolBar.HideButton(ID_KONFLICT);
	


#ifdef	AUTOCOVERAGE
	m_wndToolBar.DisableButton(ID_AUTOCOV);
#else
	m_wndToolBar.HideButton(ID_AUTOCOV);
#endif

	if(ogPrivList.GetStat("COV_TB_CONFBTN") != '0')
	{
		m_wndToolBar.DisableButton(ID_KONFLICT);
	}
	if(ogPrivList.GetStat("COV_TB_CONFBTN") =='-')
	{
		m_wndToolBar.HideButton(ID_KONFLICT);
	}

	m_wndToolBar.HideButton(ID_KONFLICT);

	if(ogPrivList.GetStat("COV_TB_OPENFLIGHTSBTN") != '1')
	{
		m_wndToolBar.DisableButton(ID_FLIGHT_NO_RES);
	}
	if(ogPrivList.GetStat("COV_TB_OPENFLIGHTSBTN") =='-')
	{
		m_wndToolBar.HideButton(ID_FLIGHT_NO_RES);
	}

	if(ogPrivList.GetStat("COV_TB_SEARCHBTN") !='1')
	{
		m_wndToolBar.DisableButton(ID_SEARCH);
	}
	if(ogPrivList.GetStat("COV_TB_SEARCHBTN") =='-')
	{
		m_wndToolBar.HideButton(ID_SEARCH);
	}

	if(ogPrivList.GetStat("COV_TB_VIEWBTN") !='1')
	{
		m_wndToolBar.DisableButton(IDC_TOGGLEBUTTON);
	}
	if(ogPrivList.GetStat("COV_TB_VIEWBTN") =='-')
	{
		m_wndToolBar.HideButton(IDC_TOGGLEBUTTON);
	}

	if(ogPrivList.GetStat("COV_TB_PRINTBTN") !='1')
	{
		m_wndToolBar.DisableButton(ID_DRUCKEN);
	}
	if(ogPrivList.GetStat("COV_TB_PRINTBTN") =='-')
	{
		m_wndToolBar.HideButton(ID_DRUCKEN);
	}

	if(ogPrivList.GetStat("COV_TB_MASSSTABBTN") !='1')
	{
		m_wndToolBar.DisableButton(ID_MASSTAB);
	}
	if(ogPrivList.GetStat("COV_TB_MASSSTABBTN") =='-')
	{
		m_wndToolBar.HideButton(ID_MASSTAB);
	}

	if(ogPrivList.GetStat("COV_TB_LTIMEFRAMEBTN") !='1')
	{
		m_wndToolBar.DisableButton(ID_LOADTIMEFRAME);
	}
	if(ogPrivList.GetStat("COV_TB_LTIMEFRAMEBTN") =='-')
	{
		m_wndToolBar.HideButton(ID_LOADTIMEFRAME);
	}

	if(ogPrivList.GetStat("COV_TB_EVALUATEBTN") != '1')
	{
		m_wndToolBar.DisableButton(ID_BEWERTUNG);
	}
	if(ogPrivList.GetStat("COV_TB_EVALUATEBTN") == '-')
	{
		m_wndToolBar.HideButton(ID_BEWERTUNG);
	}

	if(ogPrivList.GetStat("COV_TB_STATISTICBTN") != '1')
	{
		m_wndToolBar.DisableButton(ID_STATISTIC);
	}
	if(ogPrivList.GetStat("COV_TB_STATISTICBTN") == '-')
	{
		m_wndToolBar.HideButton(ID_STATISTIC);
	}

	if(ogPrivList.GetStat("COV_TB_SIMULATIONBTN") != '1')
	{
		m_wndToolBar.DisableButton(ID_SIMULATION);
	}
	if(ogPrivList.GetStat("COV_TB_SIMULATIONBTN") == '-')
	{
		m_wndToolBar.HideButton(ID_SIMULATION);
	}
	
	if(ogPrivList.GetStat("COV_TB_SETUPBTN") != '1')
	{
		m_wndToolBar.DisableButton(ID_SETUP);
	}
	if(ogPrivList.GetStat("COV_TB_SETUPBTN") == '-')
	{
		m_wndToolBar.HideButton(ID_SETUP);
	}
	
    PositionChild();


	char pclTmp[712]="";
	sprintf(pclTmp, "%s: %s %s %s", LoadStg(COV_BTN_SHIFTDEMANDS), omTSStartTime.Format("%d.%m.%Y - %H:%M"),pogCoverageDiagram->omCaptionText,pogCoverageDiagram->omCurrTimeText);
	pogCoverageDiagram->SetWindowText(pclTmp);

	MoveWindow(0,0,ogBasicData.imScreenResolutionX,ogBasicData.imScreenResolutionY);
	
	ilPos = omTSDuration.GetTotalHours();
	omTimeSlider.SetPos(ilPos * ONE_HOUR_POINT);
	omViewer.SetGeometryTimeSpan(omTSDuration);

	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
	omTimeScale.Invalidate(TRUE);

	
	ChangeViewTo(omViewer.SelectView(),true,false);//ogCfgData.rmUserSetup.STCV,TRUE);


	return 0;


//	ChangeViewToInit(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
//	CCS_CATCH_ALL
		return -1;
}



void CovDiagram::OnBeenden() 
{
	CCS_TRY
	ogCovFlightData.UnRegister();
	ogCovFlightData.ClearAll();
	
	DestroyWindow();	
	CCS_CATCH_ALL
}

void CovDiagram::OnConflicts()
{
	CCS_TRY
	if(ogCoverageConflictData.GetDataSize() > 0)
	{
		/***********
		if(pomConflict==NULL)
		{
			pomConflict = new ConflictDlg(this, false, &ogCovFlightData, &ogCoverageConflictData, &ogDemData,&omViewer);
		}
		else
		{
			pomConflict->UpdateDisplay();
			pomConflict->ShowWindow(SW_SHOW);
		}
		*****************/
	}
	CCS_CATCH_ALL
}

void CovDiagram::OnPrintCoverage()
{
	CCS_TRY
	// must check here, because disabling this button has no effect on command routing
	if (!this->m_wndToolBar.CheckEnabled(ID_DRUCKEN))
		return;

	CoverageChart *polCov = (CoverageChart *) omPtrArray.GetAt(DEMANDS); // gsa-changes
	CCoverageGraphicWnd *polCoverageGraphic = &polCov->omGraphic;
	polCoverageGraphic->InitializePrinter();
	CCS_CATCH_ALL
}


void CovDiagram::CalculateSimFlightDemands(const CUIntArray& ropFlightValues,CUIntArray& ropSimFlightValues)
{
	CCS_TRY
	CoverageChart *polCov = (CoverageChart *) omPtrArray.GetAt(DEMANDS); // gsa-changes
	CCoverageGraphicWnd *polCoverageGraphic = &polCov->omGraphic;
	switch(omViewer.SimVariant())
	{
	case 0:	
		polCoverageGraphic->CalculateSimOffsetFlightDemands(ropFlightValues,ropSimFlightValues);
	break;
	case 1:	
		polCoverageGraphic->CalculateSimCutFlightDemands(ropFlightValues,ropSimFlightValues);
	break;
	case 2:	
		polCoverageGraphic->CalculateSimSmoothFlightDemands(ropFlightValues,ropSimFlightValues);
	break;
	}

	CCS_CATCH_ALL
}

void CovDiagram::OnAnsicht()
{
	CCS_TRY
	if (!this->m_wndToolBar.CheckEnabled(IDC_TOGGLEBUTTON))
		return;

	SetFocus();

	CTime olEndTime = omStartTime + omDuration;
//	CString olCaption("Ansicht f�r geladenen Zeitraum : ");
	CString olCaption;
	CString olFrom = omStartTime.Format("%d.%m.%Y / %H:%M");
	CString olTo = olEndTime.Format("%d.%m.%Y / %H:%M");
	olCaption = LoadStg(IDS_STRING61204) + olFrom + " - " + olTo;
	FlightTablePropertySheet olDlg("COVERAGEDIA",olCaption, this, &omViewer);

	bmIsViewOpen = true;
	if(olDlg.DoModal() != IDCANCEL)
	{	
		omViewer.LoadGhdWindow();
	
		ChangeViewTo(omViewer.GetViewName(), false);
		
		omViewer.FilterFlights();
		
	}
	UpdateComboBox();

	bmIsViewOpen = false;
	CCS_CATCH_ALL
}

void CovDiagram::OnDestroy() 
{
	CCS_TRY

	// stop all timers
	KillTimer(0);
	KillTimer(1);
	KillTimer(3);

/*	Cylic demand update no longer necessary
	KillTimer(2);
	KillTimer(4);
*/

	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	ogDdx.UnRegister(&omViewer, NOTUSED);

	// Unregister DDX call back function
	TRACE("CovDiagram: DDX Unregistration %s\n",::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogDdx.UnRegister(this, NOTUSED);

	CFrameWnd::OnDestroy();
	CCS_CATCH_ALL
}

void CovDiagram::OnClose() 
{
    // Ignore close -- This makes Alt-F4 keys no effect
}

void CovDiagram::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
	CCS_TRY
    // TODO: Add your message handler code here and/or call default
    CFrameWnd::OnGetMinMaxInfo(lpMMI);

    lpMMI->ptMaxTrackSize = omMaxTrackSize;
    lpMMI->ptMinTrackSize = omMinTrackSize;
	CCS_CATCH_ALL
}

BOOL CovDiagram::OnEraseBkgnd(CDC* pDC) 
{
	CCS_TRY
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);

    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    //return CFrameWnd::OnEraseBkgnd(pDC);
	CCS_CATCH_ALL
    return TRUE;
}

void CovDiagram::OnPaint() 
{
	CCS_TRY
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CRect olRect;
    GetClientRect(&olRect);
/************    
    // draw horizontal line
    CPen olHPen(PS_SOLID, 1, lmHilightColor);
    CPen *polOldPen = dc.SelectObject(&olHPen);
    dc.MoveTo(olRect.left, 47); dc.LineTo(olRect.right, 47);
    dc.MoveTo(olRect.left, 82); dc.LineTo(olRect.right, 82);

    CPen olTPen(PS_SOLID, 1, lmTextColor);
    dc.SelectObject(&olTPen);
    dc.MoveTo(olRect.left, 83); dc.LineTo(olRect.right, 83);
    
    
    // draw vertical line
    dc.SelectObject(&olTPen);
    dc.MoveTo(imStartTimeScalePos - 2, 47);
    dc.LineTo(imStartTimeScalePos - 2, 83);

    dc.SelectObject(&olHPen);
    dc.MoveTo(imStartTimeScalePos - 1, 47);
    dc.LineTo(imStartTimeScalePos - 1, 83);

    dc.SelectObject(polOldPen);
	***************/
    // Do not call CFrameWnd::OnPaint() for painting messages
	CCS_CATCH_ALL
}


void CovDiagram::OnSize(UINT nType, int cx, int cy) 
{
	CCS_TRY

		CFrameWnd::OnSize(nType, cx, cy);
    
		// TODO: Add your message handler code here
		CRect olRect; 
		GetClientRect(&olRect);

		// save current display times
		CTime olStartTime = omTimeScale.GetDisplayStartTime();
		CTimeSpan olDuration = omTimeScale.GetDisplayDuration();

		// resize time scale window
		CRect olTSRect(imStartTimeScalePos, 48, olRect.right - ( 2), 82);
		omTimeScale.MoveWindow(&olTSRect, TRUE);

		// resize client window
	    olRect.top		+= 84;	// for Dialog Bar & TimeScale
		olRect.bottom	-= 20;  // for Horizontal Scroll Bar
		omClientWnd.MoveWindow(&olRect, TRUE);

		// but we will NOT change the time frame currently displayed
		omTimeScale.SetDisplayTimeFrame(omTSStartTime,omTSDuration,omTSInterval);
		omTimeScale.Invalidate(TRUE);

		ResizeView();

	CCS_CATCH_ALL
}

void CovDiagram::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	CCS_TRY
    // TODO: Add your message handler code here and/or call default
    
    //CFrameWnd::OnHScroll(nSBCode, nPos, pScrollBar);


    long llTotalMin;
    int ilPos;
	OnSetMarkTime();
	if(pScrollBar == NULL)
	{
		for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
		{
			CoverageChart *polCoverageChart = (CoverageChart *) omPtrArray.GetAt(ilIndex); // gsa-changes
			CoverageGantt *polStaffGantt = polCoverageChart->GetGanttPtr();
			if(polStaffGantt!=NULL && polStaffGantt->GetSafeHwnd()!=NULL)
			{
				polStaffGantt->SaveTopIndex(polStaffGantt->GetTopIndex());
			}
		}

		switch (nSBCode)
		{
			case SB_LINEUP :
				//OutputDebugString("LineUp\n\r");
            
				llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes(); 
				if(llTotalMin > 0)
					ilPos = GetScrollPos(SB_HORZ) - int (60 * 1000L / llTotalMin);
				else
					ilPos = GetScrollPos(SB_HORZ);
				if (ilPos <= 0)
				{
					ilPos = 0;
					SetTSStartTime(omTSStartTime);
				}
				else
					SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));

				omTimeScale.SetDisplayStartTime(omTSStartTime);
				omTimeScale.Invalidate(TRUE);
				ChangeViewTo(omViewer.SelectView(),true,true);//ogCfgData.rmUserSetup.STCV,TRUE);
				SetScrollPos(SB_HORZ, ilPos, TRUE);
	////////////////////////////////////////////////////////////////////////
	// Damkerng 06/27/96:
	// This thing should be remove. The technic has been changed to use the
	// method Invalidate() not only for the TimeScale, but for the entire
	// omClientWnd also.
				omClientWnd.Invalidate(FALSE);
	////////////////////////////////////////////////////////////////////////

			break;
        
			case SB_LINEDOWN :
				//OutputDebugString("LineDown\n\r");

				llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
				if(llTotalMin > 0)
					ilPos = GetScrollPos(SB_HORZ) + int (60 * 1000L / llTotalMin);
				else
				ilPos = GetScrollPos(SB_HORZ) ;

				if (ilPos >= 1000 || llTotalMin == 0)
				{
					ilPos = 1000;
					SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
				}
				else
					SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));

				omTimeScale.SetDisplayStartTime(omTSStartTime);
				omTimeScale.Invalidate(TRUE);
				ChangeViewTo(omViewer.SelectView(),true,true);//ogCfgData.rmUserSetup.STCV,TRUE);

				SetScrollPos(SB_HORZ, ilPos, TRUE);
	////////////////////////////////////////////////////////////////////////
	// Damkerng 06/27/96:
	// This thing should be remove. The technic has been changed to use the
	// method Invalidate() not only for the TimeScale, but for the entire
	// omClientWnd also.
				omClientWnd.Invalidate(FALSE);
	////////////////////////////////////////////////////////////////////////
			break;
        
			case SB_PAGEUP :
				//OutputDebugString("PageUp\n\r");
            
				llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();  
				if(llTotalMin > 0)
					ilPos = GetScrollPos(SB_HORZ) - int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
				else
					ilPos = GetScrollPos(SB_HORZ);
				if (ilPos <= 0)
				{
					ilPos = 0;
					SetTSStartTime(omStartTime);
				}
				else
					SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

				omTimeScale.SetDisplayStartTime(omTSStartTime);
				omTimeScale.Invalidate(TRUE);
				ChangeViewTo(omViewer.SelectView(),true,true);//ogCfgData.rmUserSetup.STCV,TRUE);

				SetScrollPos(SB_HORZ, ilPos, TRUE);
	////////////////////////////////////////////////////////////////////////
	// Damkerng 06/27/96:
	// This thing should be remove. The technic has been changed to use the
	// method Invalidate() not only for the TimeScale, but for the entire
	// omClientWnd also.
				omClientWnd.Invalidate(FALSE);
	////////////////////////////////////////////////////////////////////////
			break;
        
			case SB_PAGEDOWN :
				//OutputDebugString("PageDown\n\r");
				llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
				if(llTotalMin > 0)
					ilPos = GetScrollPos(SB_HORZ) + int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
				else
					ilPos = GetScrollPos(SB_HORZ);
				if (ilPos >= 1000 || llTotalMin == 0)
				{
					ilPos = 1000;
					CTime olTmpStart ; 
					SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
				}
				else
				{
					SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));
				}

				omTimeScale.SetDisplayStartTime(omTSStartTime);
				omTimeScale.Invalidate(TRUE);

				SetScrollPos(SB_HORZ, ilPos, TRUE);
	////////////////////////////////////////////////////////////////////////
	// Damkerng 06/27/96:
	// This thing should be remove. The technic has been changed to use the
	// method Invalidate() not only for the TimeScale, but for the entire
	// omClientWnd also.
				ChangeViewTo(omViewer.SelectView(),true,true);
				omClientWnd.Invalidate(FALSE);
	////////////////////////////////////////////////////////////////////////
			break; 
        
			case SB_THUMBTRACK /* pressed, any drag time */:
				//OutputDebugString("ThumbTrack\n\r");
				{
				llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
            
				SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin * nPos / 1000), 0));
            
				omTimeScale.SetDisplayStartTime(omTSStartTime);
				omTimeScale.Invalidate(TRUE);
            
				SetScrollPos(SB_HORZ, nPos, TRUE);

				//MWO:P 01.04.03 Performance
				//ChangeViewTo(omViewer.SelectView(),true,true);
				//END MWO:P 01.04.03 Performance
				//char clBuf[64];
				//wsprintf(clBuf, "Thumb Position : %d", nPos);
				//omStatusBar.SetWindowText(clBuf);
				}
			break;

			//case SB_THUMBPOSITION /* released */:
				//OutputDebugString("ThumbPosition\n\r");
			//break;
	////////////////////////////////////////////////////////////////////////
	// Damkerng 06/27/96:
	// This thing should be remove. The technic has been changed to use the
	// method Invalidate() not only for the TimeScale, but for the entire
	// omClientWnd also.
			case SB_THUMBPOSITION:	// the thumb was just released?
				ChangeViewTo(omViewer.SelectView(),true,true);//ogCfgData.rmUserSetup.STCV,TRUE);
				omClientWnd.Invalidate(FALSE);

				return;
	////////////////////////////////////////////////////////////////////////

			case SB_TOP :
			//break;
			case SB_BOTTOM :
				//OutputDebugString("TopBottom\n\r");
			break;
        
			case SB_ENDSCROLL :
				//OutputDebugString("EndScroll\n\r");
				//OutputDebugString("\n\r");
			break;
		}
		
		/*
		{
			CTime olEndTime;
			CTime olStartTime = omTimeScale.GetDisplayStartTime();
			CTimeSpan olTimeSpan  = omTimeScale.GetDisplayDuration();
			CString olStart = olStartTime.Format("%Y.%m.%d - %H:%M");
			int ilMin = olTimeSpan.GetTotalMinutes();
			olEndTime = olStartTime + olTimeSpan;
			
			omViewer.SetDisplayTimes(olStartTime,olEndTime);
			omViewer.LoadGhdWindow();
			omViewer.LoadSdtWindow();
		}
		*/
	}	
	else
	{
		CTime olEndTime = omStartTime + omDuration;
	    omTSDuration = CTimeSpan(0,omTimeSlider.GetPos() / ONE_HOUR_POINT,0,0);
		omTSDuration = min(omTSDuration,omDuration);
		omTSDuration = min(omTSDuration,olEndTime - omTSStartTime);

		int ilPos = omTSDuration.GetTotalHours();
		omTimeSlider.SetPos(ilPos * ONE_HOUR_POINT);
		omViewer.SetGeometryTimeSpan(omTSDuration);

		omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
		omTimeScale.Invalidate(TRUE);

		
		ChangeViewTo(omViewer.SelectView(),true,false);//ogCfgData.rmUserSetup.STCV,TRUE);
	}
 
	CCS_CATCH_ALL
}

void CovDiagram::OnPrevChart()
{
	CCS_TRY
		/********************
    if (imFirstVisibleChart > 0)
    {
        imFirstVisibleChart--;
		OnUpdatePrevNext();
        PositionChild();
    }
	***********/
	CCS_CATCH_ALL
}

void CovDiagram::OnNextChart()
{
	CCS_TRY
		/*************
    if (imFirstVisibleChart < omPtrArray.GetUpperBound())
    {
        imFirstVisibleChart++;
		OnUpdatePrevNext();
        PositionChild();

    }
	*******************/
	CCS_CATCH_ALL
}


void CovDiagram::OnTimer(UINT nIDEvent)
{
	CCS_TRY

	switch(nIDEvent)
	{
	case 0: {
			
				CoverageChart *polChart = NULL ;
				CTime olCurr = CTime::GetCurrentTime();
				omTimeScale.UpdateCurrentTimeLine(olCurr);
				for (int ilI = 0; ilI < 3; ilI++)
				{
					polChart = (CoverageChart *) omPtrArray.GetAt(ilI); // gsa-changes;
					
					//Die wollen local
					polChart->GetGanttPtr()->SetCurrentTime(olCurr);
				}
				CString olWndText = "";
				pogCoverageDiagram->GetWindowText(olWndText);

				int ilPos = olWndText.Find("Local");
				if(ilPos > -1)
				{
					olWndText = olWndText.Left(ilPos);
				}

				CTimeSpan olLocalDiff = CTimeSpan(0, 0, 0, ogBasicData.GetUtcDifference());
				omCurrTimeText.Format("Local: %s  ( UTC: %s)",olCurr.Format("%H:%M"),(olCurr - olLocalDiff).Format("%H:%M"));

				olWndText += omCurrTimeText;


				pogCoverageDiagram->SetWindowText(olWndText);
				break;
			}
	case 2:		// demand update outside demand calculation
			{
				if (!bgCreateDemIsActive && omViewer.NoDemUpdates() == false)
				{
					if (ogBcRecursion == 0)
					{
						omViewer.ReloadAllDems();
					}
				}
				break;
			}
	case 3:		// watch dog event
			{
				switch(imBroadcastState)
				{
				case 0 :	// initialized
					{
						imBroadcastState = 1;	// data sent !
					}
				break;
				case 1 :	// data sent -> no broadcast message received
					{
						KillTimer(3);	// don't get recursive !
						GetCoverageChart()->omBCStatus.SetColors(RED, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
						if (bmBroadcastCheckMsg)
						{
							CString olMessage(LoadStg(IDS_STRING1833)); 
							int ilState = AfxMessageBox(olMessage,MB_YESNO|MB_ICONSTOP);
							SetTimer(3, imTimerValue, NULL);
							if (ilState == IDYES)	// we try it again
							{
								imBroadcastState = 1;
							}
							else
							{
								imBroadcastState = 1;
								bmBroadcastCheckMsg = false;
							}
						}
						else	// retry !
						{
							SetTimer(3, imTimerValue, NULL);
							imBroadcastState = 1;
						}
					}
				break;
				case 2 :	// data received
					{
						imBroadcastState = 1;	// data sent !
						GetCoverageChart()->omBCStatus.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
					}
				break;
				case 3 :	// broadcast not received -> ignore further events
					;
				break;
				}
				break;
			}			
	case 4:		// demand update during calculation
			{
				if (bgCreateDemIsActive && omViewer.NoDemUpdates() == false)
				{
					if (ogBcRecursion == 0)
					{
						omViewer.ReloadAllDems();
					}
				}
				break;
			}
	case 5:		// import broadcasts from file
			{
				ImportBc();
			}
	}

	CFrameWnd::OnTimer(nIDEvent);
	CCS_CATCH_ALL
}

void CovDiagram::SetMarkTime(CTime opStartTime, CTime opEndTime)
{
	CCS_TRY
		CoverageChart *polChart;
		for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
		{
			polChart = (CoverageChart *) omPtrArray.GetAt(ilIndex);
			polChart->SetMarkTime(opStartTime, opEndTime);
		}
		omClientWnd.Invalidate(TRUE);
	CCS_CATCH_ALL
}


LONG CovDiagram::OnPositionChild(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY
    PositionChild();
	CCS_CATCH_ALL
    return 0L;
}

LONG CovDiagram::OnDeactivateAutoCov(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY
#ifdef	AUTOCOVERAGE
    m_wndToolBar.DisableButton(ID_AUTOCOV);
#endif
	CCS_CATCH_ALL
    return 0L;
}

LONG CovDiagram::OnADButton(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY
#ifdef	AUTOCOVERAGE
	bool ad;
	ad = (bool)lParam;
    TRACE("CovDiagram::OnDeactivateAutoCov : %d",ad);
	for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
	{
		CoverageChart *polCoverageChart = (CoverageChart *) omPtrArray.GetAt(ilIndex); // gsa-changes
		if(polCoverageChart!=NULL && polCoverageChart->GetSafeHwnd()!=NULL)
		{
				polCoverageChart->enableDisableUpdateDeleteButton(ad);
		}
	}
#endif
	CCS_CATCH_ALL
    return 0L;
}

LONG CovDiagram::OnActivateAutoCov(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY
#ifdef	AUTOCOVERAGE
    m_wndToolBar.EnableButton(ID_AUTOCOV);
#endif
	CCS_CATCH_ALL
    return 0L;
}

LONG CovDiagram::UpdateGraphic(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY



#ifdef DEBUGGING_COV_DIAGRAM
	SYSTEMTIME rlPerf;
#endif
	CoverageChart *polCov = (CoverageChart *) omPtrArray.GetAt(DEMANDS); // gsa-changes
	if (polCov == NULL)
		return 0L;

	CCoverageGraphicWnd *polCoverageGraphic = &polCov->omGraphic;

	if(bmNoGraphicUpdateNow == false)
	{
//		ogDdx.DataChanged((void *)this,COV_REDRAW,(void *)NULL);
#ifdef DEBUGGING_COV_DIAGRAM
		GetSystemTime(&rlPerf);
		TRACE("CovDiagram::UpdateGraphic [SetTimeFrame] START %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
#endif
		polCoverageGraphic->SetTimeFrame(lParam == 0 ? true : false);
		ogBackTrace += "8,";

#ifdef DEBUGGING_COV_DIAGRAM
		GetSystemTime(&rlPerf);
		TRACE("CovDiagram::UpdateGraphic [SetTimeFrame] END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);	
		TRACE("CovDiagram::UpdateGraphic [UpdateWindow] START %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
#endif
		polCoverageGraphic->UpdateWindow();
		ogBackTrace += "9,";
		//polCoverageGraphic->RedrawWindow();
#ifdef DEBUGGING_COV_DIAGRAM
		GetSystemTime(&rlPerf);
		TRACE("CovDiagram::UpdateGraphic [UpdateWindow] END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
#endif
	}

	polCov->SetPIdxFieldText();
	ogBackTrace += "10,";

	CCS_CATCH_ALL
    return 0L;
}



LONG CovDiagram::UpdateDemandCombo(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY

    CoverageChart *polCoverageChart = (CoverageChart *) omPtrArray.GetAt(2);
	polCoverageChart = (CoverageChart *) omPtrArray.GetAt(SHIFT_DEMAND); // gsa-changes
	polCoverageChart->OnReloadCombo(0,0);

	CCS_CATCH_ALL

	return 0L;
}

LONG CovDiagram::SelectDemandCombo(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY

    CoverageChart *polCoverageChart = (CoverageChart *) omPtrArray.GetAt(2);
	polCoverageChart = (CoverageChart *) omPtrArray.GetAt(SHIFT_DEMAND); // gsa-changes
	char *pclName = (char *)lParam;

//	int ilIndex = polCoverageChart->omComboBox.FindStringExact(-1,pclName);
	for (int ilIndex = 0; ilIndex < polCoverageChart->omComboBox.GetCount(); ilIndex++)
	{
		CString olText;
		polCoverageChart->omComboBox.GetLBText(ilIndex,olText);
		
		if (strlen(pclName) == 0 && olText.GetLength() == 0)
		{
			polCoverageChart->omComboBox.SetCurSel(ilIndex);
			polCoverageChart->OnSelChangeCB();
			break;
		}
		else if (olText == pclName)
		{
			polCoverageChart->omComboBox.SetCurSel(ilIndex);
			polCoverageChart->OnSelChangeCB();
			break;
		}
	}

	CCS_CATCH_ALL

	return 0L;
}

LONG CovDiagram::UpdateDemandGantt(WPARAM wParam, LPARAM lParam)      //  1 0  shabi
{
  CCS_TRY
   
	
	 	  
	  
	DWORD	llNow,llLast, llStart;
	double	flDiff;

	llLast = GetTickCount();
	llStart = llLast;

    CoverageChart *polCoverageChart  = (CoverageChart *) omPtrArray.GetAt(SHIFT_DEMAND); // gsa-changes
    CoverageChart *polCoverageChart2 = (CoverageChart *) omPtrArray.GetAt(BASIC_SHIFT); 

	ChartWindowVisible(BASIC_SHIFT);
	ChartWindowVisible(SHIFT_DEMAND);
	
  

	int ilPos = 0;
    CoverageGantt *polStaffGantt = polCoverageChart->GetGanttPtr();
	polCoverageChart->SetReleaseBtnRed(omViewer.SdgChanged());
	polCoverageChart->ChangeToAutoCov(omViewer.AutoCovIsActive());

	
   

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiagram::UpdateDemandGantt:ChangeToAutoCov %.3lf sec\n", flDiff/1000.0 );

	CRect olRect; 
	omTimeScale.GetClientRect(&olRect);
    


	
	omViewer.ChangeViewTo(omViewer.SelectView(), omTSStartTime, omTSStartTime + omTSDuration);  // caonima shabi

	
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiagram::UpdateDemandGantt:omViewer.ChangeViewTo %.3lf sec\n", flDiff/1000.0 );


	CoverageChart *polCoverageGraphChart = (CoverageChart *) omPtrArray.GetAt(0); // gsa-changes



	polCoverageGraphChart->omGraphic.SetTimeFrame(lParam == 0 ? true : false);  //caonima shabi 
	
	
	
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiagram::UpdateDemandGantt:omGraphic.SetTimeFrame(%ld) %.3lf sec\n",lParam, flDiff/1000.0 );





	polCoverageGraphChart->SetPIdxFieldText();
   
	
	
	if(wParam!=0)
	{
		//ilPos = polStaffGantt->GetScrollPos(SB_VERT);
		ilPos = polStaffGantt->GetTopIndex();
	}

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiagram::UpdateDemandGantt:polGCovGraphChart->SetPIdxFieldText %.3lf sec\n", flDiff/1000.0 );
	
	
	
	
	
	

	
	
	
	
	
	polStaffGantt->ResetContent();

	
	


	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiagram::UpdateDemandGantt:polStaffGantt->ResetContent() %.3lf sec\n", flDiff/1000.0 );
	


	
	for (int ilLineno = 0; ilLineno < omViewer.GetLineCount(SHIFT_DEMAND); ilLineno++)     // line 21
    {
        polStaffGantt->AddString("");
      
       
    	polStaffGantt->SetItemHeight(ilLineno, polStaffGantt->GetLineHeight(ilLineno));
    }

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiagram::UpdateDemandGantt:polStaffGantt->SetItemHeight() %.3lf sec\n", flDiff/1000.0 );

	polStaffGantt->RepaintGanttChart();

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiagram::UpdateDemandGantt:polStaffGantt->RepaintGanttChart() %.3lf sec\n", flDiff/1000.0 );

	if(::IsWindow(omClientWnd.GetSafeHwnd()))
		omClientWnd.Invalidate();

	

	
	if(wParam!=0)
	{
		int ilLimit = polStaffGantt->GetCount(); 
		if(ilPos < ilLimit)
		{
		
			polStaffGantt->SetTopIndex(ilPos);          	//polStaffGantt->SetScrollPos(SB_VERT,ilPos,TRUE);

                 
         



		}
		else if(ilLimit>0)
		{
			polStaffGantt->SetTopIndex(ilLimit-1);


		
		}
	}

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiagram::UpdateDemandGantt:omClientWnd.Invalidate() %.3lf sec\n", flDiff/1000.0 );


  

	

	CCS_CATCH_ALL
	return 0L;
}


void CovDiagram::UpdateForShiftDemandUpdate(bool bpRestoreTopIndex)
{
CCS_TRY
	DWORD	llNow,llLast, llStart;
	double	flDiff;

	llLast = GetTickCount();
	llStart = llLast;

    CoverageChart *polCoverageChart  = (CoverageChart *) omPtrArray.GetAt(SHIFT_DEMAND); // gsa-changes

	ChartWindowVisible(SHIFT_DEMAND);

	int ilPos = 0;
    CoverageGantt *polStaffGantt = polCoverageChart->GetGanttPtr();
	polStaffGantt->LockWindowUpdate();

    omViewer.ChangeViewToForShiftDemandUpdate();

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiagram::UpdateDemandGantt:omViewer.ChangeViewToForShiftDemandUpdate %.3lf sec\n", flDiff/1000.0 );

	CoverageChart *polCoverageGraphChart = (CoverageChart *) omPtrArray.GetAt(0); // gsa-changes

	polCoverageGraphChart->omGraphic.UpdateForShiftDemandUpdate();

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiagram::UpdateDemandGantt:omGraphic.UpdateForShiftDemandUpdate %.3lf sec\n", flDiff/1000.0 );

	polCoverageGraphChart->omGraphic.UpdateWindow();

	polCoverageGraphChart->SetPIdxFieldText();
    if (bpRestoreTopIndex)
	{
		ilPos = polStaffGantt->GetTopIndex();
	}

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiagram::UpdateDemandGantt:polGCovGraphChart->SetPIdxFieldText %.3lf sec\n", flDiff/1000.0 );
	
	polStaffGantt->ResetContent();

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiagram::UpdateDemandGantt:polStaffGantt->ResetContent() %.3lf sec\n", flDiff/1000.0 );
	
    for (int ilLineno = 0; ilLineno < omViewer.GetLineCount(SHIFT_DEMAND); ilLineno++)
    {
        polStaffGantt->AddString("");
        polStaffGantt->SetItemHeight(ilLineno, polStaffGantt->GetLineHeight(ilLineno));
    }

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiagram::UpdateDemandGantt:polStaffGantt->SetItemHeight() %.3lf sec\n", flDiff/1000.0 );

	polStaffGantt->RepaintGanttChart();

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiagram::UpdateDemandGantt:polStaffGantt->RepaintGanttChart() %.3lf sec\n", flDiff/1000.0 );

	if(::IsWindow(omClientWnd.GetSafeHwnd()))
		omClientWnd.Invalidate();

	if (bpRestoreTopIndex)
	{
		int ilLimit = polStaffGantt->GetCount(); 
		if(ilPos < ilLimit)
		{
			polStaffGantt->SetTopIndex(ilPos);
		}
		else if (ilLimit > 0)
		{
			polStaffGantt->SetTopIndex(ilLimit-1);
		}
	}

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovDiagram::UpdateDemandGantt:omClientWnd.Invalidate() %.3lf sec\n", flDiff/1000.0 );

	polStaffGantt->UnlockWindowUpdate();

	CCS_CATCH_ALL
}

LONG CovDiagram::UpdateBaseShift(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY
    CoverageChart *polCoverageChart = (CoverageChart *) omPtrArray.GetAt(BASIC_SHIFT); 
    CoverageGantt *polBaseShift = polCoverageChart->GetGanttPtr();
	CRect olRect; omTimeScale.GetClientRect(&olRect);
    omViewer.ChangeViewTo(omViewer.SelectView(), omTSStartTime, omTSStartTime + omTSDuration);
    polBaseShift->ResetContent();
    for (int ilLineno = 0; ilLineno < omViewer.GetLineCount(BASIC_SHIFT); ilLineno++)
    {
        polBaseShift->AddString("");
        polBaseShift->SetItemHeight(ilLineno, polBaseShift->GetLineHeight(ilLineno));
    }
	
	polBaseShift->RepaintGanttChart();
	CCS_CATCH_ALL
	return 0L;
}

LONG CovDiagram::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY
    CString olStr;
    int ipGroupNo = HIWORD(lParam);
    int ipLineNo = LOWORD(lParam);
	CTime olTime = CTime((time_t) lParam);
#ifdef DEBUGGING_COV_DIAGRAM
	SYSTEMTIME rlPerf;
#endif
	char clBuf[255];
	int ilCount;

    CoverageChart *polCoverageChart = (CoverageChart *) omPtrArray.GetAt(ipGroupNo);
    CoverageGantt *polStaffGantt = polCoverageChart->GetGanttPtr();
	//CoverageChart *polCov = (CoverageChart *) omPtrArray.GetAt(1);
	//CCoverageGraphicWnd *polCoverageGraphic = &polCov->omGraphic;

	//polCoverageGraphic->SetTimeFrame();
	//polCoverageGraphic->UpdateWindow();
	int ilPos = 0,ilLimit = 0;
	
	if(polStaffGantt!=NULL && ::IsWindow(polStaffGantt->m_hWnd))
	{
		//ilPos = polStaffGantt->GetScrollPos(SB_VERT);
		ilPos = polStaffGantt->GetTopIndex();
	}
    switch (wParam)
    {
        // group message


        case UD_UPDATEGROUP :
            olStr = omViewer.GetGroupText(ipGroupNo);
            polCoverageChart->GetChartButtonPtr()->SetWindowText(olStr);
            polCoverageChart->GetChartButtonPtr()->Invalidate(FALSE);
			polStaffGantt->RepaintGanttChart(-1);

            
        break;
        
		case UD_INSERTBASICSHIFT:
			TRACE("UD_INSERTBASICSHIFT");
			PositionChild();
		break;

 
        // line message
        case UD_INSERTLINE :
            polStaffGantt->InsertString(ipLineNo, "");
			polStaffGantt->RepaintItemHeight(ipLineNo);
			
			ilCount = omViewer.GetLineCount(ipGroupNo);
			sprintf(clBuf, "%d", ilCount);
			//polCoverageChart->GetCountTextPtr()->SetWindowText(clBuf);
			//polCoverageChart->GetCountTextPtr()->Invalidate(TRUE);

			//SetStaffAreaButtonColor(ipGroupNo);
            PositionChild();
        break;
        
        case UD_UPDATELINE :
            polStaffGantt->RepaintVerticalScale(ipLineNo);
			polStaffGantt->RepaintItemHeight(ipLineNo);
            polStaffGantt->RepaintGanttChart(ipLineNo);

			#ifdef DEBUGGING_COV_DIAGRAM
			GetSystemTime(&rlPerf);
			TRACE("CovDiagram::OnUpdateDiagram [UD_UPDATELINE] END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
			#endif
			//SetStaffAreaButtonColor(ipGroupNo);
        break;

        case UD_DELETELINE :
            polStaffGantt->DeleteString(ipLineNo);
            
			ilCount = omViewer.GetLineCount(ipGroupNo);
			sprintf(clBuf, "%d", ilCount);
			//polCoverageChart->GetCountTextPtr()->SetWindowText(clBuf);
			//polCoverageChart->GetCountTextPtr()->Invalidate(TRUE);

			//SetStaffAreaButtonColor(ipGroupNo);
			PositionChild();
        break;

        case UD_UPDATELINEHEIGHT :
            polStaffGantt->RepaintItemHeight(ipLineNo);
			//SetStaffAreaButtonColor(ipGroupNo);
            PositionChild();
        break;

    }

	//CTime olFrom,
	//		olTo;	
	//olFrom = omTSStartTime;
	//olTo   = omTSStartTime + omTSDuration;
	
	if(polStaffGantt!=NULL && ::IsWindow(polStaffGantt->m_hWnd))
	{
		ilLimit = polStaffGantt->GetCount();
		if(ilPos<ilLimit)
		{
			polStaffGantt->SetTopIndex(ilPos);
			//polStaffGantt->SetScrollPos(SB_VERT,ilPos,TRUE);
		}
		else if(ilLimit>0)
		{
			polStaffGantt->SetTopIndex(ilLimit-1);
			//polStaffGantt->SetScrollPos(SB_VERT,ilLimit,TRUE);
		}
	}
	

//	polCoverageChart->omGraphic.SetTimeFrame();
	//polCoverageChart->omGraphic.UpdateWindow();
	CCS_CATCH_ALL

    return 0L;
}

void CovDiagram::OnUpdatePrevNext(void)
{
	CCS_TRY
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	SetFocus();

	CCS_CATCH_ALL
}

///////////////////////////////////////////////////////////////////////////////
// Damkerng and Pichet: Date: 5 July 1995

void CovDiagram::UpdateComboBox()
{
	CCS_TRY
	CComboBox *polCB = (CComboBox *) m_wndToolBar.GetDlgItem(IDC_VIEW);
	polCB->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{

		olViewName = omViewer.SelectView();//ogCfgData.rmUserSetup.STCV;
	}

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}

	CCS_CATCH_ALL
}

void CovDiagram::ChangeViewTo(const char *pcpViewName,bool RememberPositions,bool bpIsFromHScroll)
{
	CCS_TRY
	if (bgNoScroll == TRUE)
		return;
	int ilDwRef = m_dwRef;
	CWaitCursor olWait;

	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	CRect olRect; omTimeScale.GetClientRect(&olRect);
	{
		CTime olEndTime;
		CTime olStartTime = omTimeScale.GetDisplayStartTime();
		CTimeSpan olTimeSpan  = omTimeScale.GetDisplayDuration();
		CString olStart = olStartTime.Format("%Y.%m.%d - %H:%M");
		int ilMin = olTimeSpan.GetTotalMinutes();
		int ilMin2 = omTSDuration.GetTotalMinutes();
//		olEndTime = olStartTime + olTimeSpan;
		olEndTime = olStartTime + omTSDuration;
		
		omViewer.SetDisplayTimes(olStartTime,olEndTime);
	}
	if(bpIsFromHScroll)
	{
//		omViewer.ChangeViewToForHScroll(omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
		omViewer.ChangeViewToForHScroll(omTSStartTime, omTSStartTime + omTSDuration);
	}
	else
	{
		omViewer.ChangeViewTo(pcpViewName, omTSStartTime, omTSStartTime + omTSDuration);
	}
    CoverageChart *polChart = NULL ;
    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
	
	imFirstVisibleChart = 0;
	
    int ilLastY = 0;
	
	CTime olTimeFrom, olTimeTo;

	omViewer.GetTimeFrameFromTo(olTimeFrom, olTimeTo);
    for (int ilI = 0; ilI < 3; ilI++)
    {
		polChart = (CoverageChart *) omPtrArray.GetAt(ilI); // gsa-changes;
		polChart->SetStartTime(omTSStartTime);
		polChart->SetInterval(omTimeScale.GetDisplayDuration());
		
		if(ilI == 0)
		{
			polChart->SetTimeFrame(olTimeFrom, olTimeTo);
			polChart->omGraphic.SetTimeFrame();
			polChart->SetPIdxFieldText();
		}
		else if (bpIsFromHScroll && ilI == 1)
		{
			CoverageGantt *polStaffGantt = polChart->GetGanttPtr();
			int ilPos = polStaffGantt->GetTopIndex();
			polStaffGantt->ResetContent();
			
			for (int ilLineno = 0; ilLineno < omViewer.GetLineCount(SHIFT_DEMAND); ilLineno++)
			{
				polStaffGantt->AddString("");
				polStaffGantt->SetItemHeight(ilLineno, polStaffGantt->GetLineHeight(ilLineno));
			}

			polStaffGantt->RepaintGanttChart();

			int ilLimit = polStaffGantt->GetCount(); 
			if(ilPos < ilLimit)
			{
				polStaffGantt->SetTopIndex(ilPos);
			}
			else if(ilLimit>0)
			{
				polStaffGantt->SetTopIndex(ilLimit-1);
			}
		}
		else if (bpIsFromHScroll && ilI == 2)
		{
			CoverageGantt *polBaseShift = polChart->GetGanttPtr();
			int ilPos = polBaseShift->GetTopIndex();
			polBaseShift->ResetContent();
			
			for (int ilLineno = 0; ilLineno < omViewer.GetLineCount(BASIC_SHIFT); ilLineno++)
			{
				polBaseShift->AddString("");
				polBaseShift->SetItemHeight(ilLineno, polBaseShift->GetLineHeight(ilLineno));
			}

			polBaseShift->RepaintGanttChart();

			int ilLimit = polBaseShift->GetCount(); 
			if(ilPos < ilLimit)
			{
				polBaseShift->SetTopIndex(ilPos);
			}
			else if(ilLimit>0)
			{
				polBaseShift->SetTopIndex(ilLimit-1);
			}
		}
		else
		{
			CoverageGantt *polGantt = polChart->GetGanttPtr();
			polGantt->SetFonts(omViewer.GetGeometryFontIndex(), omViewer.GetGeometryFontIndex());
		}

    }

	
	PositionChild();
	bmNoUpdatesNow = FALSE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	SetFocus();
	CCS_CATCH_ALL
}

void CovDiagram::OnViewSelChange()
{
	CCS_TRY
    char clText[64];
    CComboBox *polCB = (CComboBox *) m_wndToolBar.GetDlgItem(IDC_VIEW);
    polCB->GetLBText(polCB->GetCurSel(), clText);

	omViewer.SelectView(clText);

	omViewer.LoadGhdWindow();
	ChangeViewTo(clText, false);

	ChartWindowVisible(BASIC_SHIFT);
	ChartWindowVisible(SHIFT_DEMAND);

	CCS_CATCH_ALL
}

void CovDiagram::OnCloseupView() 
{
	CCS_TRY
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	SetFocus();
	CCS_CATCH_ALL
}


void CovDiagram::OnPrint()
{
	CCS_TRY
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	SetFocus();

	//MWO TO DO Print aktivieren ==> omViewer.PrintDiagram(omPtrArray);
	CCS_CATCH_ALL
}

BOOL CovDiagram::DestroyWindow() 
{
	BOOL blRc;
	CCS_TRY

	if ((bmIsViewOpen))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}

	ogBcHandle.SetTrafficLightCallBack(NULL);

	ogDdx.UnRegister(&omViewer, NOTUSED);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	blRc = CWnd::DestroyWindow();
	CCS_CATCH_ALL
	return blRc;
}

/////////XXXXX
////////////////////////////////////////////////////////////////////////
// CovDiagram keyboard handling

void CovDiagram::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CCS_TRY
	// check if the control key is pressed
    BOOL blIsControl = ::GetKeyState(VK_CONTROL) & 0x8080;
		// This statement has to be fixed for using both in Windows 3.11 and NT.
		// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
		// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.

	switch (nChar)
	{
	case VK_UP:		// move the bottom most gantt chart up/down one line
	case VK_DOWN:
		{
		CoverageChart *polCov = (CoverageChart *) omPtrArray.GetAt(DEMANDS); 
		if(polCov != NULL)
		{
			polCov->omGraphic.SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
		}
		break;
		}
	case VK_LEFT:
		OnHScroll(blIsControl? SB_PAGEUP: SB_LINEUP, 0, NULL);
		break;
	case VK_RIGHT:
		OnHScroll(blIsControl? SB_PAGEDOWN: SB_LINEDOWN, 0, NULL);
		break;
	case VK_HOME:
		SetScrollPos(SB_HORZ, 0, FALSE);
		OnHScroll(SB_LINEUP, 0, NULL);
		break;
	case VK_END:
		SetScrollPos(SB_HORZ, 1000, FALSE);
		OnHScroll(SB_LINEDOWN, 0, NULL);
		break;
	default:
//Fsc		omDialogBar.SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
		CFrameWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		break;
	}
	CCS_CATCH_ALL
}

void CovDiagram::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CCS_TRY
	CFrameWnd::OnKeyUp(nChar, nRepCnt, nFlags);
	CCS_CATCH_ALL
}

////////////////////////////////////////////////////////////////////////
// CovDiagram -- implementation of DDX call back function

static void CovDiagramCf(void *popInstance, enum enumDDXTypes ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	CCS_TRY
	CovDiagram *polDiagram = (CovDiagram *)popInstance;


	CCS_CATCH_ALL
}

void CovDiagram::SetTimeBand(CTime opStartTime, CTime opEndTime)
{
	CCS_TRY
	omTimeBandStartTime = opStartTime;
	omTimeBandEndTime = opEndTime;
	CCS_CATCH_ALL
}

void CovDiagram::UpdateTimeBand()
{
	CCS_TRY
	for (int ilLc = imFirstVisibleChart; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		if (bmShowWindows[ilLc])
		{
			CoverageChart *polChart = (CoverageChart *)omPtrArray[ilLc];
			polChart->omGantt.SetMarkTime(omTimeBandStartTime, omTimeBandEndTime);
		}
	}
	CCS_CATCH_ALL
}

void CovDiagram::OnCheckDemands()
{
	CCS_TRY

	// must check here, because disabling this button has no effect on command routing
	if (!this->m_wndToolBar.CheckEnabled(ID_BEWERTUNG))
		return;

	CoverageChart *polCov = (CoverageChart *) omPtrArray.GetAt(DEMANDS); 
	if(polCov != NULL)
	{
		polCov->omGraphic.OnCompleteCheckDemands();
	}
	CCS_CATCH_ALL
}

void CovDiagram::OnNoResList()
{
	// must check here, because disabling this button has no effect on command routing
	if (!this->m_wndToolBar.CheckEnabled(ID_FLIGHT_NO_RES))
		return;

	NoBroadcastSupport olNoBroadcast;
	CNoDemFlights olNoDemFlights(&omViewer,omViewer.GetFilterTsrArray(),omViewer.GetFlightDemMap(),true);

} 

void CovDiagram::OnStatistics()
{
	CCS_TRY

		// must check here, because disabling this button has no effect on command routing
		if (!this->m_wndToolBar.CheckEnabled(ID_STATISTIC))
			return;

		if(pogCoverageStatistics==NULL)
		{
			pogCoverageStatistics = new CCoverageStatistics(this,&omViewer);
			if(pogCoverageStatistics==NULL)
				return;
			pogCoverageStatistics->Create(IDD_COVERAGE_STATISTICS,this);

			ChangeViewTo(omViewer.SelectView());
		}
		pogCoverageStatistics->ShowWindow(SW_SHOWNORMAL);
	CCS_CATCH_ALL
}

void CovDiagram::OnSimulation()
{
	CCS_TRY
		// must check here, because disabling this button has no effect on command routing
		if (!this->m_wndToolBar.CheckEnabled(ID_SIMULATION))
			return;

		if(pomSimulation==NULL)
		{
			pomSimulation = new SimulationSheet("Banane",this,0,&omViewer);
			if (pomSimulation == NULL)
				return;
		}

		pomSimulation->DoModal();

	CCS_CATCH_ALL
}

void CovDiagram::OnTest()
{
	CCS_TRY
//	MasterShiftDemand *polDlg = new MasterShiftDemand(this);
//	polDlg->DoModal();
//	delete polDlg;
	CCS_CATCH_ALL
}

void CovDiagram::OnSetMarkTime()
{
	int ilSize = omPtrArray.GetSize();
	//CoverageChart *polChart
	for(int ilIndex = 0;ilIndex<ilSize;ilIndex++)
	{
		CoverageChart *polChart = (CoverageChart *) omPtrArray.GetAt(ilIndex);
		if(polChart!=NULL)
		{
			CTime olMarkTimeStart,olMarkTimeEnd;
			omViewer.GetMarkTimes(olMarkTimeStart,olMarkTimeEnd);
			polChart->SetMarkTime(olMarkTimeStart,olMarkTimeEnd);
		}
	}
	omClientWnd.Invalidate(TRUE);
}

void CovDiagram::OnSimulate()
{
	int ilSize = omPtrArray.GetSize();
	int ilIndex = 0;
	OnUpdateToolbar(0,0);

#ifndef	AUTOCOVERAGE
	m_wndToolBar.HideButton(ID_AUTOCOV);
#endif

	m_wndToolBar.RedrawWindow();

	if (ilSize > ilIndex)
	{
		CoverageChart *polChart = (CoverageChart *) omPtrArray.GetAt(ilIndex);
		if(polChart!=NULL)
		{
			if (omViewer.SimulationActive() == true)
				polChart->omIgnoreBreakCB.SetCheck(1);
			else
				polChart->omIgnoreBreakCB.SetCheck(0);

			polChart->omGraphic.SetTimeFrame();
			polChart->SetPIdxFieldText();
			polChart->omGraphic.Invalidate(TRUE);
		}
	}
}




// end bch

//---------------------------------------------------------------------------

void CovDiagram::OnLoadtimeframe() 
{ 
	CCS_TRY

	// must check here, because disabling this button has no effect on command routing
	if (!this->m_wndToolBar.CheckEnabled(ID_LOADTIMEFRAME))
		return;

	CLoadTimeSpanDlg olDlg(NULL,omLoadTimeValues,imAbsFlag);

	if (olDlg.DoModal() == IDOK)
	{
		bmNoGraphicUpdateNow = true;
		ogBcHandle.StopBc();

		ogBasicData.omValidTemplates.RemoveAll();
		for(int illc = 0; illc < olDlg.omTplUrnos.GetSize() ;illc++)
		{
			ogBasicData.omValidTemplates.Add(olDlg.omTplUrnos.GetAt(illc));
		}

		omLoadTimeValues.RemoveAll();
		for(illc = 0; illc < olDlg.omValues.GetSize() ;illc++)
		{
			omLoadTimeValues.Add(olDlg.omValues.GetAt(illc));
		}

		imAbsFlag = olDlg.m_AbsTimeVal ;

		OnMsgLoadTimeFrame(olDlg.omStartTime.GetTime(),olDlg.omEndTime.GetTime());

		bmNoGraphicUpdateNow = false;
		ogBcHandle.StartBc();
	}

	CCS_CATCH_ALL
}

void CovDiagram::OnMsgLoadTimeFrame(WPARAM wParam, LPARAM lParam) 
{ 
	CCS_TRY
	DWORD	llNow,llLast, llStart;
	double	flDiff;
	CTime	olFrom	= CTime((time_t)wParam);
	CTime	olTo	= CTime((time_t)lParam);
	bool	blStopBc = false;
	
	llLast = GetTickCount();
	llStart = llLast;

	ogCoverageConflictData.DeleteAllData();
	CTime olCurTime = CTime::GetCurrentTime();
	if (bmNoGraphicUpdateNow == false)
	{
		blStopBc = true;
		bmNoGraphicUpdateNow = true;
		ogBcHandle.StopBc();
	}

	// update loaded time values
	omLoadTimeValues[0] = CTimeToDBString(olFrom,olFrom);
	omLoadTimeValues[1] = CTimeToDBString(olTo,olTo);

	for(int illc = 0; illc < omLoadTimeValues.GetSize() ;illc++)
	{
		ogBasicData.omLoadTimeValues.Add(omLoadTimeValues.GetAt(illc));
	}

//Very important to attach every CedaGhdData-Class to it's own CedaFlightData-Pointer
//e.g. ogDemData need ogCovFlightData, ogGhdData need ogFlightData 
// and local olGhdData need here ogFlightData
	ogDemData.AttachCedaFlightData(&ogCovFlightData);
	
	CString olWhere = "";
	ogBasicData.GetDispoTimeframeWhereString(olFrom,olTo,olWhere);
	CWaitCursor olWait;
	ogCoverageConflictData.DeleteAllData();

	omStatusBar.SetPaneText(0, LoadStg(IDS_STRING1483)/*"Fl�ge werden geladen....."*/);
	omStatusBar.UpdateWindow();

	llLast = GetTickCount();
		

	ogCovFlightData.ClearAll();
#ifdef DEBUGGING_COV_DIAGRAM
	SYSTEMTIME rlPerf;
	ofstream ofPerf;
	ofPerf.open( CCSLog::GetTmpPath("\\perf.txt"), ios::out);
	
	GetSystemTime(&rlPerf);
	ofPerf <<"CovDiagramm::LoadFlightWithPremis: <Reading Rotations>START->"<<rlPerf.wHour<<":"<<rlPerf.wMinute<<":"<<rlPerf.wSecond<<"."<<rlPerf.wMilliseconds<<endl;
#endif

	ogCovFlightData.ReadRotations(olWhere.GetBuffer(0), true);

	llNow = GetTickCount();
	flDiff = llNow-llLast;
	TRACE( "Lesen der Fluege %.3lf sec\n", flDiff/1000.0 );
		

#ifdef DEBUGGING_COV_DIAGRAM
	GetSystemTime(&rlPerf);
	ofPerf <<"CovDiagramm::LoadFlightWithPremis: <Reading Rotations>END->"<<rlPerf.wHour<<":"<<rlPerf.wMinute<<":"<<rlPerf.wSecond<<"."<<rlPerf.wMilliseconds<<endl;
	TRACE(("CovDiagramm::LoadFlightWithPremis: <Reading Rotations>END  ->%d:%d:%d.%d",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
#endif


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Parameter�bergabe
	omStartTime = olFrom;
	omDuration = olTo - olFrom;
	if ( omDuration < omTSDuration)
		omViewer.SetGeometryTimeSpan(omDuration);

	ogCovFlightData.SetPreSelection(olFrom, olTo, CString(""), true);


	omViewer.SetLoadTimes(olFrom,olTo);

	if (pomSearchProbertySheetDlg != NULL)
	{
		pomSearchProbertySheetDlg->SetLoadStartEnd(olFrom,olTo,bmSearchIsOpen);
	}

	if (pomCreateDemProbertySheetDlg != NULL)
	{
		pomCreateDemProbertySheetDlg->SetLoadStartEnd(olFrom,olTo,bmSearchIsOpen);
	}

		
	///////////////////////////////////////////////////////////////////////////

			
	char pclWhere[2000];
	CTime olUtcStart = olFrom;
	CTime olUtcEnd = olTo;

	//sna start
	sprintf(pclWhere," WHERE ENDE >= '%s' AND BEGI <= '%s'", olFrom.Format("%Y%m%d%H%M00"), olTo.Format("%Y%m%d%H%M00"));
	ogSdgData.Read(pclWhere);

	sprintf(pclWhere," WHERE SDGU IN (SELECT URNO FROM SDGTAB WHERE ENDE >= '%s')",olFrom.Format("%Y%m%d%H%M00"));
	ogMsdData.Read(pclWhere);
	//sna end

	ogBasicData.LocalToUtc(olUtcStart);
	ogBasicData.LocalToUtc(olUtcEnd);

	ogDemData.SetDispoTimeFrame(olUtcStart,olUtcEnd);
		
	ogDemData.ClearAll();

	CString olValidTpls = "";
	//hag20030401 Start
	omStatusBar.SetPaneText(0, LoadStg(IDS_STRING1365)/*"Rules are being loaded....."*/);
	omStatusBar.UpdateWindow();
	theApp.LoadRules ();	
	olValidTpls = theApp.omLoadedTplUrnos;	

	//hag20030401 End

	//MWO: 27.03.03
	ogBcHandle.SetFilterRange("DEMTAB", "DEBE", "DEEN", olUtcStart.Format("%Y%m%d%H%M%S"), olUtcEnd.Format("%Y%m%d%H%M%S"));
	//END MWO: 27.03.03

	llLast = GetTickCount();

	omStatusBar.SetPaneText(0, LoadStg(IDS_STRING1885)/*"Demands are being loaded....."*/);
	omStatusBar.UpdateWindow();
	ogDemData.Read(olUtcStart,olUtcEnd,olValidTpls);
	 
	llNow = GetTickCount();
	flDiff = llNow-llLast;
	TRACE( "Lesen DEMTAB %.3lf sec\n", flDiff/1000.0 );
		
	llLast = llNow;

	sprintf(pclWhere," WHERE ESBG BETWEEN '%s' AND '%s' OR LSEN BETWEEN '%s' AND '%s'", olFrom.Format("%Y%m%d%H%M00"),olTo.Format("%Y%m%d%H%M00"),olFrom.Format("%Y%m%d%H%M00"),olTo.Format("%Y%m%d%H%M00"));
	//MWO: 27.03.03
	ogBcHandle.SetFilterRange("SDTTAB", "ESBG", "LSEN", olFrom.Format("%Y%m%d%H%M%S"), olTo.Format("%Y%m%d%H%M%S"));
	//END MWO: 27.03.03
				
	ogSdgData.ResetLoadFlag();
	ogSdtData.ClearAll();
	omStatusBar.SetPaneText(0, LoadStg(IDS_STRING1886)/*"Shift demands are being loaded....."*/);
	omStatusBar.UpdateWindow();
	ogSdtData.Read(pclWhere);

	llNow = GetTickCount();
	flDiff = llNow-llLast;
	TRACE( "Lesen SDTTAB %.3lf sec\n", flDiff/1000.0 );
		
	llLast = llNow;
	olWhere.Format(" WHERE (VPTO >= '%s' OR VPTO = ' ' ) AND VPFR <= '%s'", olFrom.Format("%Y%m%d%H%M00"), olTo.Format("%Y%m%d%H%M00"));
	//MWO: 27.03.03
	ogBcHandle.SetFilterRange("SDTTAB", "VPFR", "VPTO", olFrom.Format("%Y%m%d%H%M%S"), olTo.Format("%Y%m%d%H%M%S"));
	//END MWO: 27.03.03

	ogBCD.Read("SPE",olWhere);

	llNow = GetTickCount();
	flDiff = llNow-llLast;
	TRACE( "Lesen SPETAB %.3lf sec\n", flDiff/1000.0 );
		
		
	sprintf(pclWhere," WHERE (VATO >= '%s' OR VATO = ' ' ) AND VAFR <= '%s'", olFrom.Format("%Y%m%d%H%M00"), olTo.Format("%Y%m%d%H%M00"));
	//MWO: 27.03.03
	ogBcHandle.SetFilterRange("BSDTAB", "VAFR", "VATO", olFrom.Format("%Y%m%d%H%M%S"), olTo.Format("%Y%m%d%H%M%S"));
	//END MWO: 27.03.03

	omStatusBar.SetPaneText(0, LoadStg(IDS_STRING1887)/*"Basic shift data are bering loaded....."*/);
	omStatusBar.UpdateWindow();
	ogBsdData.Read(pclWhere);
	int ilAddCount = 0;

	ogBsdData.Register();


	if (omViewer.GetShiftRoster())
	{
		llLast = GetTickCount();

		olWhere = "";
		olWhere.Format(" WHERE SDAY <= '%s' AND SDAY >= '%s'", olTo.Format("%Y%m%d"), olFrom.Format("%Y%m%d"));
		//MWO: 27.03.03
		ogBcHandle.SetFilterRange("DRSTAB", "SDAY", "SDAY", olFrom.Format("%Y%m%d"), olTo.Format("%Y%m%d"));
		ogBcHandle.SetFilterRange("DRDTAB", "SDAY", "SDAY", olFrom.Format("%Y%m%d"), olTo.Format("%Y%m%d"));
		ogBcHandle.SetFilterRange("DRATAB", "SDAY", "SDAY", olFrom.Format("%Y%m%d"), olTo.Format("%Y%m%d"));
		//END MWO: 27.03.03

		ogBCD.Read("DRS",olWhere);
				
		ogBCD.Read("DRD",olWhere);

		ogBCD.Read("DRA",olWhere);

		llLast = GetTickCount();

		olWhere = "";
		olWhere.Format(" WHERE VAFR <= '%s' AND VATO >= '%s'", olTo.Format("%Y%m%d%H%M%S"), olFrom.Format("%Y%m%d%H%M%S"));
		//MWO: 27.03.03
		ogBcHandle.SetFilterRange("GPLTAB", "VAFR", "VATO", olFrom.Format("%Y%m%d%H%M%S"), olTo.Format("%Y%m%d%H%M%S"));
		//END MWO: 27.03.03

		omStatusBar.SetPaneText(0, LoadStg(IDS_STRING1869)/*"Basic shift plans are being loaded....."*/);
		omStatusBar.UpdateWindow();
		ogBCD.Read("GPL",olWhere);

		ogSdtShiftData.ClearAll();
		omViewer.PrepareShiftNameMap();

		llLast = GetTickCount();

		if (omViewer.IsRealRoster())
		{
			omViewer.SetDemandLevel(SHIFT_DEMAND,-1);
			ogSdtRosterData.ClearAll();
			
		}
		else
		{
			omViewer.CurrentShiftRosterName("");
			ogSdtShiftData.ClearAll();
		}

	}
	else
	{
		// not "--current--" ?
		if (omViewer.GetCurrentDemandName(SHIFT_DEMAND) != LoadStg(IDS_STRING61249))
			omViewer.SetDemandName(SHIFT_DEMAND,"");
	}

	omStatusBar.SetPaneText(0, "");
	omStatusBar.UpdateWindow();
	if (ogCoverageConflictData.GetDataSize() > 0)
	{
		/****************
		if(pomConflict==NULL)
		{
			pomConflict = new ConflictDlg(this, false, &ogCovFlightData, &ogCoverageConflictData, &ogDemData,&omViewer);
		}
		else
		{
			pomConflict->UpdateDisplay();
			pomConflict->ShowWindow(SW_SHOW);
		}
		******************/
	}


	/////////////////////////////

	CTime olT = omViewer.GetGeometrieStartTime();
	if(olFrom != TIMENULL)
	{	
		olT = olFrom;
	}
	CString olS = olT.Format("%d.%m.%Y-%H:%M");
	CString olS1 = olFrom.Format("%d.%m.%Y/%H:%M");
	CString olS2 = olTo.Format("%d.%m.%Y/%H:%M");
	SetTSStartTime(olT);

	omTSDuration = omViewer.GetGeometryTimeSpan();
	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);

	omTimeScale.Invalidate(TRUE);
	omViewer.LoadGhdWindow();
	omViewer.LoadSdtWindow();


	CoverageChart *polChart;
	for(illc = 0; illc < omPtrArray.GetSize(); illc++)
	{
		polChart = (CoverageChart *) omPtrArray.GetAt(illc);
		polChart->SendMessage(WM_COV_RELOADCOMBO, 0, 0);

	}

	ChangeViewTo(omViewer.GetViewName(), false);
		
	long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	if(llTotalMin > 0)
		nPos = nPos * 1000L / llTotalMin;
	else
		nPos = 0;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);

	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
	omTimeScale.Invalidate(TRUE);

	///////////////////////////////////////////////
	omCaptionText = "       " + LoadStg(IDS_STRING61211) + olS1 + "  -  " + olS2 +"          ";
	
	llNow = GetTickCount();
	flDiff = llNow-llStart;
	TRACE( "Total %.3lf sec\n", flDiff/1000.0 );
		
	if (blStopBc)
	{
		bmNoGraphicUpdateNow = false;
		ogBcHandle.StartBc();
	}
//////////////////////////Setzen der Zeiten


#ifdef DEBUGGING_COV
	ofPerf.close();
#endif
	CCS_CATCH_ALL
}

void CovDiagram::OnMasstab() 
{

	CCS_TRY

	// must check here, because disabling this button has no effect on command routing
	if (!this->m_wndToolBar.CheckEnabled(ID_MASSTAB))
		return;
		
	omMasstab.SetDispStartTime(omTSStartTime);
	omMasstab.SetDispDuration(omTSDuration);
	omMasstab.SetMaxDispStartTime(omStartTime + omDuration);
	omMasstab.SetMinDispStartTime(omStartTime);
	omMasstab.SetGrapicState(bmShowWindows[0]);
	omMasstab.SetShiftState(bmShowWindows[1]);
	omMasstab.SetBaseShiftState(bmShowWindows[2]);

	if(omMasstab.DoModal() == IDOK)
	{
		bmShowWindows[0] = omMasstab.GetGrapicState();
		bmShowWindows[1] = omMasstab.GetShiftState();
		bmShowWindows[2] = omMasstab.GetBaseShiftState();

		int ilWindowCount = 0;
		CoverageChart *polChart;
		for(int illc = 0; illc < omPtrArray.GetSize(); illc++)
		{
		    polChart = (CoverageChart *) omPtrArray.GetAt(illc);
			if (bmShowWindows[illc])
			{
				ChartWindowVisible(illc);
				ilWindowCount++;
			}
			else
			{
				polChart->ShowWindow(SW_HIDE);
			}

		}
		
		imWindowCount = ilWindowCount;

		CTime olEndTime = omStartTime + omDuration;
		omTSStartTime = omMasstab.GetDispStartTime();
		omTSDuration = max(omMasstab.GetDispDuration(),CTimeSpan(0,1,0,0));
		omTSDuration = min(omTSDuration,omDuration);
		omTSDuration = min(omTSDuration,olEndTime - omTSStartTime);

		omViewer.SetMaxVal(omMasstab.GetUpperBound());
		omViewer.SetMinVal(omMasstab.GetLowerBound());

		omViewer.SetBorderFix(omMasstab.IsBorderFix());
		omViewer.SetBorderVal(omMasstab.GetFixValue());

		
		omViewer.SetGeometryValues(omMasstab.GetMin(), omMasstab.GetTimeLines(),omMasstab.GetFont());
		omViewer.SetGeometryTimeSpan(omTSDuration);

		omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
		omTimeScale.Invalidate(TRUE);
	
		ChangeViewTo(omViewer.SelectView());

		// update scroll bar position
		long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
		if (llTotalMin > 0)
		{
			long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
			nPos = nPos * 1000L / llTotalMin;
			SetScrollPos(SB_HORZ, int(nPos), TRUE);
		}

		// update the button showing the current time
		SetTSStartTime(omTSStartTime);
		
		int ilPos = omTSDuration.GetTotalHours();
		omTimeSlider.SetPos(ilPos * ONE_HOUR_POINT);


	}
	CCS_CATCH_ALL
}
void CovDiagram::OnSearch() 
{
	CCS_TRY

	// must check here, because disabling this button has no effect on command routing
	if (!this->m_wndToolBar.CheckEnabled(ID_SEARCH))
		return;

	if(!bmSearchIsOpen && pomSearchProbertySheetDlg != NULL)
	{
		NoBroadcastSupport olNoBroadcast;
		pomSearchProbertySheetDlg->AttachTsrArray(omViewer.GetTotalTsrArray());
		pomSearchProbertySheetDlg->AttachFlightDemMap(omViewer.GetFlightDemMap());
		pomSearchProbertySheetDlg->DoCreate();
		bmSearchIsOpen = true;
	}
	CCS_CATCH_ALL
}

void CovDiagram::OnSearchExit() 
{
	CCS_TRY
	if (pomSearchProbertySheetDlg != NULL && pomSearchProbertySheetDlg->m_hWnd == NULL)
	{
		bmSearchIsOpen = false;
	}
	CCS_CATCH_ALL
}
void CovDiagram::OnCreateDem() 
{
	CCS_TRY
	
	if (bgCreateDemIsActive)
	{
		if (MessageBox( LoadStg(IDS_STRING61254), LoadStg(IDS_WARNING), (MB_YESNO|MB_ICONEXCLAMATION)) == IDNO)
			return;
	}

	if (pomCreateDemProbertySheetDlg != NULL)
	{
		pomCreateDemProbertySheetDlg->DoModal();
	}

	CCS_CATCH_ALL
}

void CovDiagram::OnAutoCov() 
{
#ifdef	AUTOCOVERAGE

	CCS_TRY
	{
		if(	m_wndToolBar.CheckEnabled(ID_AUTOCOV))
		{

			CTime olAssignStart = omTSStartTime;
			CTime olAssignEnd = omTSStartTime + omTSDuration;
			CString olOldBaseViewName = omViewer.GetBaseViewName();
			CString olOldViewName = omViewer.GetViewName();
			CString olViewName;
			
			omViewer.SetViewerKey("BASISSCHICHTEN");
			CStringArray olStrArr;
			omViewer.GetViews(olStrArr);
			olViewName = omViewer.GetViewName();
			
			omViewer.SetViewerKey(olOldBaseViewName);
			omViewer.SelectView(olOldViewName);

			if (pomAutoCoverageDlg == NULL)
			{
				pomAutoCoverageDlg = new CAutoCovDlg(this,olStrArr);
				pomAutoCoverageDlg->SetData(omViewer.CurrentMasterShiftRequirement(),olStrArr,olViewName,olAssignStart,olAssignEnd);
				if(omViewer.CurrentMasterShiftRequirement() > 0)
				{
					pomAutoCoverageDlg->SetMaxDuration(7);
				}
			}
			else
			{
				pomAutoCoverageDlg->UpdateData(omViewer.CurrentMasterShiftRequirement(),olStrArr,olViewName);
			}

			if (pomAutoCoverageDlg->DoModal() == IDOK)
			{
				//MOW 01.08.03: To ensure, that the dialog disappears completely
				MSG olMsg;
				UINT ilMin = 0;
				UINT ilMax = 0;
				while(::PeekMessage(&olMsg,this->m_hWnd,ilMin,ilMax,PM_NOREMOVE))
				{
					::GetMessage(&olMsg,this->m_hWnd,ilMin,ilMax);
					::TranslateMessage(&olMsg);
					::DispatchMessage(&olMsg);
				}
				//END MOW  01.08.03: To ensure, that the dialog disappears completely
				olStrArr.RemoveAll();
				pomAutoCoverageDlg->GetData(olStrArr,olAssignStart,olAssignEnd);
					
				int  ilAdaption = pomAutoCoverageDlg->GetAdaptionFlag();
				int  ilDirection = pomAutoCoverageDlg->GetDirection();
				int  ilShiftSelection = pomAutoCoverageDlg->GetShiftSelection();
				int  ilShiftDeviation = pomAutoCoverageDlg->GetShiftDeviation();
				int  ilAdaptIterations = pomAutoCoverageDlg->GetAdaptIterations();
				int  ilFactor = pomAutoCoverageDlg->GetFactor();
				int	 ilMinShiftCoverage = pomAutoCoverageDlg->GetMinShiftCoverage(); 
				bool blUseBaseCoverage  = pomAutoCoverageDlg->UseBaseCoverage(); 
				if(ilAdaption != 2)
				{
					ilAdaptIterations = 1;
				}

				m_wndToolBar.DisableButton(ID_AUTOCOV);

				double dlFactor  = (double)ilFactor / 100;

				omViewer.PrepareBaseShifts(olStrArr,olAssignStart,olAssignEnd,dlFactor);
				omViewer.AutoAssign(ilDirection,ilShiftSelection,ilShiftDeviation,ilAdaptIterations,
									ilAdaption,ilFactor,ilMinShiftCoverage,blUseBaseCoverage,olAssignStart, olAssignEnd);

			}
		}
	}
	CCS_CATCH_ALL
#endif
}


LONG CovDiagram::OnBcAdd(UINT wParam, LONG /*lParam*/)
{
	TRACE("Broadcast num %d\n",wParam);

	KillTimer(3);

	GetCoverageChart()->omBCStatus.SetColors(RGB(236, 236,   0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	GetCoverageChart()->omBCStatus.UpdateWindow();

	bool blStatus = ogBcHandle.GetBc(wParam);

	imBroadcastState = 2;
	GetCoverageChart()->omBCStatus.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	GetCoverageChart()->omBCStatus.UpdateWindow();

	SetTimer(3, imTimerValue, NULL);

	return TRUE;
}

bool CovDiagram::OnToolTipText(UINT, NMHDR *pNMHDR, LRESULT *pResult)
{
	ASSERT(pNMHDR->code == TTN_NEEDTEXTA || pNMHDR->code == TTN_NEEDTEXTW);
	
	TOOLTIPTEXTA* pTTTA = (TOOLTIPTEXTA*)pNMHDR;
	TOOLTIPTEXTW* pTTTW = (TOOLTIPTEXTW*)pNMHDR;
	
	CString strTipText;
	UINT nID = pNMHDR->idFrom;
	if (pNMHDR->code == TTN_NEEDTEXTA && (pTTTA->uFlags & TTF_IDISHWND) 
		|| pNMHDR->code == TTN_NEEDTEXTW && (pTTTW->uFlags & TTF_IDISHWND))
	{
		nID = ::GetDlgCtrlID((HWND)nID);
	}

	if (nID !=0)
	{
		CString olMsg;
		olMsg.LoadString(nID);		// if this a valid Res-String?
		CString olTxt = LoadStg(nID);

//		olIDS.Format("%u", nID);
		if (olTxt.IsEmpty() && olMsg.IsEmpty())
		{
			return false;
		}
		AfxExtractSubString(strTipText, olTxt, 1, '\n');
	}

#ifndef _UNICODE
	if (pNMHDR->code == TTN_NEEDTEXTA)
		lstrcpyn(pTTTA->szText, strTipText, sizeof(pTTTA->szText));
	else
		_mbstowcsz(pTTTW->szText, strTipText, sizeof(pTTTW->szText));
#else
	if (pNMHDR->code == TTN_NEEDTEXTA)
		_wcstombsz(pTTTA->szText, strTipText, sizeof(pTTTA->szText));
	else
		lstrcpyn(pTTTW->szText, strTipText, sizeof(pTTTW->szText));
#endif

	*pResult = 0;

	::SetWindowPos(pNMHDR->hwndFrom, HWND_TOP, 0, 0, 0, 0,
		SWP_NOACTIVATE|SWP_NOSIZE|SWP_NOMOVE|SWP_NOOWNERZORDER);

	return true;
}


void CovDiagram::TranslateMenu()
{
	CMenu*	polMenu = GetMenu ();

	if ( !polMenu || !polMenu->m_hMenu )
		return;

	//  Submenu "Program"
	CMenu* polSubMenu = polMenu->GetSubMenu(0);
	if (polSubMenu)
	{
		polMenu->ModifyMenu(0, MF_STRING|MF_BYPOSITION,(UINT)polSubMenu->m_hMenu,LoadStg(IDS_STRING61393));

		polSubMenu->ModifyMenu(ID_APP_EXIT, MF_STRING|MF_BYCOMMAND,ID_APP_EXIT,LoadStg(IDS_STRING61394));
	}

	//  Submenu "Views"
	polSubMenu = polMenu->GetSubMenu(1);
	if (polSubMenu)
	{
		polMenu->ModifyMenu(1, MF_STRING|MF_BYPOSITION, (UINT)polSubMenu->m_hMenu,LoadStg(IDS_STRING61395));

		polSubMenu->ModifyMenu(ID_VIEW_TOOLBAR, MF_STRING|MF_BYCOMMAND,ID_VIEW_TOOLBAR,LoadStg(IDS_STRING61396));
		polSubMenu->ModifyMenu(ID_VIEW_STATUS_BAR, MF_STRING|MF_BYCOMMAND,ID_VIEW_STATUS_BAR, LoadStg(IDS_STRING61397));
	}

	//  Submenu "?"
	polSubMenu = polMenu->GetSubMenu(2);
	if (polSubMenu)
	{
		polMenu->ModifyMenu(2,MF_STRING|MF_BYPOSITION, (UINT)polSubMenu->m_hMenu,LoadStg(IDS_STRING61398));

		polSubMenu->ModifyMenu(ID_HELP_FINDER, MF_STRING|MF_BYCOMMAND,ID_HELP_FINDER,LoadStg(IDS_STRING61399));
		polSubMenu->ModifyMenu(ID_APP_ABOUT, MF_STRING|MF_BYCOMMAND,ID_APP_ABOUT,LoadStg(IDS_STRING61400));
	}
}

bool CovDiagram::ChartWindowVisible(int ipIndex)	
{
	if (!bmShowWindows[ipIndex])
		return false;

	CWnd *polWnd = (CWnd *)omPtrArray[ipIndex];

	if (omViewer.GetFilterIdx() == EQUIPMENT || omViewer.GetFilterIdx() >= CHECKIN && omViewer.GetFilterIdx() <= GATES)
	{
#ifdef	AUTOCOVERAGE
		if (!m_wndToolBar.IsButtonHidden(ID_AUTOCOV))
		{
			m_wndToolBar.HideButton(ID_AUTOCOV);
		}
#endif
		if (polWnd == omPtrArray.GetAt(SHIFT_DEMAND) || polWnd == omPtrArray.GetAt(BASIC_SHIFT))
		{
			polWnd->ShowWindow(SW_HIDE);
			return false;
		}
		else if (polWnd == omPtrArray.GetAt(DEMANDS))
		{
			polWnd->ShowWindow(SW_SHOW);
		}

	}
	else
	{
#ifdef	AUTOCOVERAGE
		if (m_wndToolBar.IsButtonHidden(ID_AUTOCOV))
		{
			m_wndToolBar.HideButton(ID_AUTOCOV,FALSE);
		}
#endif
		if (polWnd == omPtrArray.GetAt(DEMANDS) || polWnd == omPtrArray.GetAt(SHIFT_DEMAND) || polWnd == omPtrArray.GetAt(BASIC_SHIFT))
		{
			polWnd->ShowWindow(SW_SHOW);
		}
	}

	return true;

}

LONG CovDiagram::OnUpdateDemandBtn(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY
		if (omPtrArray.GetSize() > 0)
		{
			CoverageChart *pWnd = (CoverageChart *)omPtrArray.GetAt(DEMANDS);
			if (pWnd)
			{
				pWnd->SetUpdateBtnRed(wParam == 1);
			}
		}		
	CCS_CATCH_ALL
    return 0L;

}

void  CovDiagram::ProcessCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CovDiagram *polDiagram = (CovDiagram *)vpInstance;
	if (ipDDXType == BROADCAST_CHECK)
	{
		struct BcStruct *prlRequestData;
		prlRequestData = (struct BcStruct *) vpDataPointer;
//$$		ogBasicData.LogBroadcast(prlRequestData);
	}
	else if (ipDDXType == UPDATE_SETUP)
	{

/*	Cylic demand update no longer necessary
		polDiagram->KillTimer(2);
		polDiagram->KillTimer(4);

	    polDiagram->SetTimer(2, (UINT)      1000 * atoi(ogCfgData.rmUserSetup.DET1), NULL);
		polDiagram->SetTimer(4, (UINT)      1000 * atoi(ogCfgData.rmUserSetup.DET2), NULL);
*/
	}
}

CoverageChart*	CovDiagram::GetCoverageChart()
{
	CoverageChart *polCov = NULL;
	if(omPtrArray.GetSize() == 0)
	{
		polCov = NULL;
	}
	else
	{
		polCov = (CoverageChart *) omPtrArray.GetAt(DEMANDS); // gsa-changes
	}
	return polCov;
}

void CovDiagram::OnSetup()
{
	if (m_wndToolBar.CheckEnabled(ID_SETUP))
	{
		SetupSheet olSheet("",this,0);
		olSheet.DoModal();
	}
}

void CovDiagram::ResizeView()
{
	CCS_TRY

		int ilDwRef = m_dwRef;
		CWaitCursor olWait;

		bmNoUpdatesNow = TRUE;
		omViewer.AllowUpdates(bmNoUpdatesNow);

		CRect olRect; 
		omTimeScale.GetClientRect(&olRect);
/***
		CTime olStartTime	= omTimeScale.GetDisplayStartTime();
		CTimeSpan olTimeSpan= omTimeScale.GetDisplayDuration();
		CString olStart		= olStartTime.Format("%Y.%m.%d - %H:%M");
		int ilMin			= olTimeSpan.GetTotalMinutes();
		int ilMin2			= omTSDuration.GetTotalMinutes();
		CTime	olEndTime	= olStartTime + omTSDuration;
****/
		
//		omViewer.SetDisplayTimes(olStartTime,olEndTime);

//		omViewer.ChangeViewToForHScroll(omTSStartTime, omTSStartTime + omTSDuration);

		CoverageChart *polChart = NULL;
		CRect olPrevRect;
		omClientWnd.GetClientRect(&olRect);
    
		
		imFirstVisibleChart = 0;
		
		int ilLastY = 0;
		
		CTime olTimeFrom, olTimeTo;

		omViewer.GetTimeFrameFromTo(olTimeFrom, olTimeTo);
		for (int ilI = 0; ilI < 3; ilI++)
		{
			polChart = (CoverageChart *) omPtrArray.GetAt(ilI); // gsa-changes;
			polChart->SetStartTime(omTSStartTime);
			polChart->SetInterval(omTimeScale.GetDisplayDuration());
			
			if(ilI == 0)
			{
				polChart->SetTimeFrame(olTimeFrom, olTimeTo);
				//MWO:P 31.03.03 Performance 
				//polChart->omGraphic.SetTimeFrame();
				//END MWO:P 31.03.03 Performance 
				polChart->SetPIdxFieldText();
			}
			else if (ilI == 1)
			{
				CoverageGantt *polStaffGantt = polChart->GetGanttPtr();
				int ilPos = polStaffGantt->GetTopIndex();
				//MWO:P 31.03.03 Performance 
				//polStaffGantt->ResetContent();
				
				//for (int ilLineno = 0; ilLineno < omViewer.GetLineCount(SHIFT_DEMAND); ilLineno++)
				//{
				//	polStaffGantt->AddString("");
				//	polStaffGantt->SetItemHeight(ilLineno, polStaffGantt->GetLineHeight(ilLineno));
				//}
				//END MWO:P 31.03.03 Performance 

				polStaffGantt->RepaintGanttChart();

				int ilLimit = polStaffGantt->GetCount(); 
				if(ilPos < ilLimit)
				{
					polStaffGantt->SetTopIndex(ilPos);
				}
				else if(ilLimit>0)
				{
					polStaffGantt->SetTopIndex(ilLimit-1);
				}
			}
			else
			{
				CoverageGantt *polGantt = polChart->GetGanttPtr();
				polGantt->SetFonts(omViewer.GetGeometryFontIndex(), omViewer.GetGeometryFontIndex());
			}

		}

		
		PositionChild();
		bmNoUpdatesNow = FALSE;
		omViewer.AllowUpdates(bmNoUpdatesNow);

		// Id 25-Sep-96
		// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
		SetFocus();
	CCS_CATCH_ALL
}

void CovDiagram::OnUpdateToolbar(WPARAM wParam, LPARAM lParam) 
{
	if (omViewer.SimulationActive() == true)
	{
		if (bgCreateDemIsActive == true)
			m_wndToolBar.LoadBitmap(IDB_BITMAP4);
		else
			m_wndToolBar.LoadBitmap(IDB_BITMAP2);
	}
	else
	{
		if (bgCreateDemIsActive == true)
			m_wndToolBar.LoadBitmap(IDB_BITMAP3);
		else
			m_wndToolBar.LoadBitmap(IDB_BITMAP1);
	}

	m_wndToolBar.RedrawWindow();
}

BOOL CovDiagram::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	BOOL blResult = TRUE;
	blResult = CFrameWnd::OnCommand(wParam, lParam);
	return blResult;
}

void EvaluateCmd(const char *pclBuffer,int nParams,char *p1,char *p2,char *p3)
{
	const char *pclChar = pclBuffer;
	char pclToken[2048];

	for (int i = 0; i < nParams; i++)
	{
		int	 ilRecursive = 0;
		bool blToken = false;
		int j = 0;
		while(*pclChar)
		{
			if (*pclChar == '<')
			{
				++ilRecursive;
				if (ilRecursive == 1)
				{
					blToken = true;
					++pclChar;
					pclToken[0] = '\0';
				}
				else
				{
					pclToken[j++] = *pclChar;
					++pclChar;
				}
			}
			else if (*pclChar == '>')
			{
				--ilRecursive;
				if (ilRecursive == 0)
				{
					pclToken[j] = '\0';
					if (i == 0)
						strcpy(p1,pclToken);
					else if (i == 1)
						strcpy(p2,pclToken);
					else
						strcpy(p3,pclToken);
					++pclChar;
					break;
				}
				else
				{
					pclToken[j++] = *pclChar;
					++pclChar;
				}
			}
			else if (blToken)
			{
				pclToken[j++] = *pclChar;
				++pclChar;
			}
			else
			{
				++pclChar;
			}

		}
	}
}

void CovDiagram::ImportBc()
{
	static FILE *fp = NULL;
	static int i = 0;
	if (fp == NULL)
	{
		fp = fopen(this->omImportBcFilePath.GetBuffer(0),"rt");
		if (fp == NULL)
		{
			KillTimer(5);
			return;
		}
	}

	static char pclBuffer[102400];

	CString olCmd;
	CString olTable;

	while(!feof(fp))
	{
		if (fgets(pclBuffer,sizeof(pclBuffer),fp) != NULL)
		{
			bool blComplete = false;
			BcStruct olBcStruct;
			char *pclCmd = strstr(pclBuffer,":CMD");
			if (pclCmd != NULL)
			{
				// :CMD
				EvaluateCmd(pclCmd,2,olBcStruct.Cmd,olBcStruct.Object,NULL);

				// :FROM
				if (fgets(pclBuffer,sizeof(pclBuffer),fp) == NULL)
					return;
				EvaluateCmd(pclBuffer,2,olBcStruct.Dest1,olBcStruct.Dest2,NULL);

				// :SPECIAL
				if (fgets(pclBuffer,sizeof(pclBuffer),fp) == NULL)
					return;
				EvaluateCmd(pclBuffer,2,olBcStruct.Tws,olBcStruct.Twe,NULL);

				// :SEL
				if (fgets(pclBuffer,sizeof(pclBuffer),fp) == NULL)
					return;
				EvaluateCmd(pclBuffer,1,olBcStruct.Selection,NULL,NULL);

				// :FLD
				if (fgets(pclBuffer,sizeof(pclBuffer),fp) == NULL)
					return;
				EvaluateCmd(pclBuffer,1,olBcStruct.Fields,NULL,NULL);

				// :DATA
				if (fgets(pclBuffer,sizeof(pclBuffer),fp) == NULL)
					return;
				EvaluateCmd(pclBuffer,1,olBcStruct.Data,NULL,NULL);

				blComplete = true;
			}

			if (blComplete)
			{
				TRACE("Importing [%d] Broadcast\n",++i);
				ogBcHandle.DistributeBc(olBcStruct);
				return;
			}

		}
	}
	
	KillTimer(5);
	fclose(fp);
	fp = NULL;
}

bool CovDiagram::GetSnapshotData(CString& ropCovr,CString& ropMain,CString& ropDema,CString& ropSdgn,CString& ropIgbr)
{
	ropCovr = this->omTSStartTime.Format("%Y%m%d%H%M00");
	ropMain = this->omViewer.SelectView();

	CoverageChart *polChart = this->GetCoverageChart();
	if (!polChart)
		return false;

	int ilIndex = polChart->omComboBox.GetCurSel();
	if (ilIndex == CB_ERR)
		return false;

	polChart->omComboBox.GetLBText(ilIndex,ropDema);

	if (omViewer.GetShiftRoster())
	{
		if (this->omViewer.IsRealRoster())
		{

			polChart = (CoverageChart *)this->omPtrArray.GetAt(COVERAGE_SHIFTDEMANDS);
			if (!polChart)
				return false;
			ilIndex = polChart->omComboBox.GetCurSel();
			if (ilIndex == CB_ERR)
				return false;

			polChart->omComboBox.GetLBText(ilIndex,ropSdgn);
			
		}
		else
		{
			ropSdgn = this->omViewer.CurrentShiftRosterName();
		}
	}
	else
	{
		ropSdgn = this->omViewer.GetCurrentDemandName(SHIFT_DEMAND);
	}

	ropIgbr = this->omViewer.GetIgnoreBreak() == true ? "1" : "0";
	
	return true;
}

int	 CovDiagram::CalculateMaxFlightDemandPeak()
{
	CoverageChart *polCov = (CoverageChart *) omPtrArray.GetAt(DEMANDS); // gsa-changes
	CCoverageGraphicWnd *polCoverageGraphic = &polCov->omGraphic;
	return polCoverageGraphic->CalculateMaxFlightDemandPeak();
}

int	 CovDiagram::CalculateMinFlightDemandCut()
{
	CoverageChart *polCov = (CoverageChart *) omPtrArray.GetAt(DEMANDS); // gsa-changes
	CCoverageGraphicWnd *polCoverageGraphic = &polCov->omGraphic;
	return polCoverageGraphic->CalculateMinFlightDemandCut();
}
