// InputData.cpp: implementation of the CInputData class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <coverage.h>
#include <InputData.h>
#include <stdio.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CInputData::CInputData()
{
	n_Input = 0;
}
	
void CInputData::InitData(CUIntArray &ropInputData)
{
	n_Input = ropInputData.GetSize();

	Data.InsertAt(0,0,n_Input);
	InData.InsertAt(0,0,n_Input);
	OptData.InsertAt(0,0,n_Input);

	for(int illc = 0; illc < n_Input; illc++)
	{
		Data[illc] = ropInputData[illc];
		InData[illc] = ropInputData[illc];
		OptData[illc] = ropInputData[illc];
	}
	
}


CInputData::~CInputData()
{
	Data.RemoveAll();
	InData.RemoveAll();
	OptData.RemoveAll();
}


int CInputData::GetInputSize()
{
	return n_Input;
}


