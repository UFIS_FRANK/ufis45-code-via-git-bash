// FlightTablePropertySheet.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>
#include <Coverage.h>
#include <CCSGlobl.h>

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <FlightTablePropertySheet.h>
#include <StringConst.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CStringArray ogFlightAlcd;

/////////////////////////////////////////////////////////////////////////////
// FlightTablePropertySheet
//

FlightTablePropertySheet::FlightTablePropertySheet(CString opCalledFrom,CString opCaption, CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
	(opCaption, pParentWnd, popViewer, iSelectPage)
{
	AddPage(&m_PsFlightPage);
	AddPage(&m_PsTemplateFilterPage);
	AddPage(&m_PsAltFilterPage);
	AddPage(&m_PsAlcFilterPage);
	AddPage(&m_PsAptFilterPage);
	AddPage(&m_PsPosFilterPage);
	pomViewer = popViewer; 

	m_PsFlightPage.SetCaption(LoadStg(IDS_STRING61205));
	m_PsTemplateFilterPage.SetCaption(LoadStg(IDS_STRING61206));
	m_PsAltFilterPage.SetCaption(LoadStg(IDS_STRING61207));
	m_PsAlcFilterPage.SetCaption(LoadStg(IDS_STRING61208));
	m_PsAptFilterPage.SetCaption(LoadStg(IDS_STRING61209));
	m_PsPosFilterPage.SetCaption(LoadStg(IDS_STRING1389));
} 

void FlightTablePropertySheet::LoadDataFromViewer()
{
	if (pomViewer != NULL)
	{ 
		CStringArray olTest;
		pomViewer->GetFilter("FLIGHT",m_PsFlightPage.omValues);
		pomViewer->GetFilter("APT",m_PsAptFilterPage.omSelectedItems);
		pomViewer->GetFilter("APTBUTTON",m_PsAptFilterPage.omButtonValues);
		pomViewer->GetFilter("ALC",m_PsAlcFilterPage.omSelectedItems);
		pomViewer->GetFilter("ALT",m_PsAltFilterPage.omSelectedItems);
		pomViewer->GetFilter("TEMPLATE",m_PsTemplateFilterPage.omSelectedItems);
		pomViewer->GetFilter("TEMPBUTTON",m_PsTemplateFilterPage.omButtonValues);
		pomViewer->GetFilter("POS",m_PsPosFilterPage.omSelectedItems);

	}
	m_PsFlightPage.SetData();
}

void FlightTablePropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{

	if (pomViewer != NULL)
	{
		m_PsFlightPage.UpdateData(TRUE);
		pomViewer->SetFilter("FLIGHT",m_PsFlightPage.omValues);
		pomViewer->SetFilter("APT",m_PsAptFilterPage.omSelectedItems);
		pomViewer->SetFilter("APTBUTTON",m_PsAptFilterPage.omButtonValues);
		pomViewer->SetFilter("ALC",m_PsAlcFilterPage.omSelectedItems);
		pomViewer->SetFilter("ALT",m_PsAltFilterPage.omSelectedItems);
		pomViewer->SetFilter("TEMPLATE",m_PsTemplateFilterPage.omSelectedItems);
		pomViewer->SetFilter("TEMPBUTTON",m_PsTemplateFilterPage.omButtonValues);
		pomViewer->SetFilter("POS",m_PsPosFilterPage.omSelectedItems);

	}

	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

