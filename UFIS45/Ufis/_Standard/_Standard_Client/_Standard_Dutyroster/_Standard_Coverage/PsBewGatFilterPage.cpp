// PsFilterPage.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <Coverage.h>
#include <PsBewGatFilterPage.h>
#include <cedabasicdata.h>
#include <basicdata.h>
#include <CCSGlobl.h>



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Eigenschaftenseite CPsBewGatFilterPage 

IMPLEMENT_DYNCREATE(CPsBewGatFilterPage, CPropertyPage)

CPsBewGatFilterPage::CPsBewGatFilterPage() : CPropertyPage(CPsBewGatFilterPage::IDD)
{
	//{{AFX_DATA_INIT(CCCPsBewGatFilterPage)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT

		//--- fill combobox with template names
	int ilCount = ogBCD.GetDataCount("GAT");
	CString olTmpText;
	CString olTmpText1;

	for(int i = 0; i < ilCount; i++)
	{
		olTmpText = ogBCD.GetField("GAT", i, "URNO") + "S" +"�";
		olTmpText1 = ogBCD.GetField("GAT", i, "GNAM");

		olTmpText += olTmpText1;
		omPossibleItems.Add(olTmpText);
	}

	ilCount = ogBCD.GetDataCount("SGR");
	CString olTab;
	for( i = 0; i < ilCount; i++)
	{
		olTab = ogBCD.GetField("SGR", i, "TABN");
		if(olTab == "GAT")
		{
			olTmpText = ogBCD.GetField("SGR", i, "URNO") +"G" +"�";
			olTmpText1 = ogBCD.GetField("SGR", i, "GRPN");
			olTmpText += olTmpText1;
			omPossibleItems.Add(olTmpText);
		}
	}

	imHideColStart = 1;
	CStringArray olItemList;
	if(omPossibleItems.GetSize() > 0)
	{
		ExtractItemList(omPossibleItems[0], &olItemList, '%');
	}

	
	imHideColStart = olItemList.GetSize() +1;
	imColCount = imHideColStart +2;

	pomPossilbeList = new CGridFenster(this);
	pomSelectedList = new CGridFenster(this);

	blIsInit = false;

}

CPsBewGatFilterPage::~CPsBewGatFilterPage()
{
	delete pomPossilbeList;
	delete pomSelectedList;
}


void CPsBewGatFilterPage::SetCaption(const char *pcpCaption)
{
	strcpy(pcmCaption,pcpCaption);
	m_psp.pszTitle = pcmCaption;
	m_psp.dwFlags |= PSP_USETITLE;
}

void CPsBewGatFilterPage::DoDataExchange(CDataExchange* pDX)
{
	CString olText;
	CGXStyle olStyle;
	CPropertyPage::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(CPsAptFilterPage)
	DDX_Control(pDX, IDC_BUTTON_REMOVE, m_RemoveButton);
	DDX_Control(pDX, IDC_BUTTON_ADD, m_AddButton);
	DDX_Control(pDX, IDC_CONTENTLIST, m_ContentList);
	DDX_Control(pDX, IDC_INSERTLIST, m_InsertList);
	//}}AFX_DATA_MAP

	if (pDX->m_bSaveAndValidate == FALSE)
	{
		int ilLc;
		
		// Do not update window while processing new data
		m_InsertList.SetRedraw(FALSE);
		m_ContentList.SetRedraw(FALSE);

		// Initialize all possible items into the left list box
		m_InsertList.ResetContent();


		
		m_InsertList.AddString(LoadStg(IDS_STRING61216));
		for (ilLc = 0; ilLc < omPossibleItems.GetSize(); ilLc++)
		{
			CString olTest = omPossibleItems[ilLc];
			m_InsertList.AddString(omPossibleItems[ilLc]);
		}

		// Initialize the selected items into the right list box,
		// also remove the corresponding items from the left list box.
		m_ContentList.ResetContent();
		bool blFoundAlle = false;
		for (ilLc = 0; ilLc < omSelectedItems.GetSize()  && blFoundAlle == false; ilLc++)
		{
			CString olTmpText = omSelectedItems[ilLc];
			if(!olTmpText.IsEmpty())
			{
				if (omSelectedItems[ilLc] == LoadStg(IDS_STRING61216))
				{
					/************ using all *******************/
					m_AddButton.EnableWindow(FALSE);
					blFoundAlle = true;
					/*** remove all entries except "*Alle" from content list **/
					m_ContentList.ResetContent();
					m_ContentList.AddString(LoadStg(IDS_STRING61216));	
					/*** show empty insert list ***/
					m_InsertList.ResetContent();
				
				}
				else
				{
					int ilPos = m_InsertList.FindString(-1, olTmpText);
					if(ilPos > -1)
					{
						m_InsertList.GetText(ilPos,olTmpText);
						m_ContentList.AddString(olTmpText);
						m_InsertList.DeleteString(ilPos);
					}
				}
			}
		}

		m_InsertList.SetRedraw(TRUE);
		m_ContentList.SetRedraw(TRUE);

		if(blIsInit)
		{
			m_AddButton.EnableWindow(TRUE);

			CString olText;
			CString olOrgText;
			int ilCount = m_InsertList.GetCount() + 1;

			pomPossilbeList->SetReadOnly(FALSE);
			pomSelectedList->SetReadOnly(FALSE);

			
			olStyle.SetEnabled(FALSE);
			olStyle.SetReadOnly(TRUE);
		

			CStringArray olItemList;
			pomSelectedList->RemoveRows(1,  pomSelectedList->GetRowCount());
			pomPossilbeList->RemoveRows(1,  pomPossilbeList->GetRowCount());
			
			CString olUrnoText;
			pomPossilbeList->SetRowCount(max(18,ilCount));
			for ( ilLc = 0; ilLc < ilCount-1; ilLc++)
			{
				m_InsertList.GetText(ilLc,olText);
				olOrgText = olText;
				ExtractItemList(olText, &olItemList, '�');
				if(olItemList.GetSize() > 1)
				{
					olText = olItemList[1];
					pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, imHideColStart+1, ilLc+1, imHideColStart+1), olStyle.SetValue(olItemList[0]));

				}

				ExtractItemList(olText, &olItemList, '%');
				for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
					pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
				pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, imHideColStart, ilLc+1, imHideColStart), olStyle.SetValue(olOrgText));

			}

			CGXSortInfoArray sortInfo;
			sortInfo.SetSize(1);							// 1 key only (you can also have more keys)
			sortInfo[0].nRC = 1;							// column nCol is the key
			sortInfo[0].sortType = CGXSortInfo::autodetect; // the grid will determine if the key is a date, numeric or alphanumeric value
			sortInfo[0].sortOrder = CGXSortInfo::ascending; // sort ascending  
				  
			pomPossilbeList->SortRows(CGXRange().SetTable(), sortInfo);		// Call SortRows to let the grid do the sorting

			ilCount = m_ContentList.GetCount()+ 1;
				
			pomSelectedList->SetRowCount(max(18,ilCount));

			for (ilLc = 0; ilLc < ilCount-1; ilLc++)
			{
				m_ContentList.GetText(ilLc,olText);
				olOrgText = olText;
				ExtractItemList(olText, &olItemList, '�');
				if(olItemList.GetSize() > 1)
				{
					olText = olItemList[1];
					pomSelectedList->SetStyleRange(CGXRange(ilLc+1, imHideColStart+1, ilLc+1, imHideColStart+1), olStyle.SetValue(olItemList[0]));

				}
				ExtractItemList(olText, &olItemList, '%');
				for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
					pomSelectedList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
				pomSelectedList->SetStyleRange(CGXRange(ilLc+1, imHideColStart, ilLc+1, imHideColStart), olStyle.SetValue(olOrgText));

			}

			pomPossilbeList->SetReadOnly(TRUE);
			pomSelectedList->SetReadOnly(TRUE);

			pomSelectedList->Redraw();
			pomPossilbeList->Redraw();
		}

	}

	if (pDX->m_bSaveAndValidate == TRUE)
	{
		// Copy data from the list box back to the array of string
		
		CString olValue;
		omSelectedItems.RemoveAll();
		omSelectedItems.SetSize(m_ContentList.GetCount()+ 1);
		for (int ilLc = 0; ilLc < (int)pomSelectedList->GetRowCount(); ilLc++)
		{
			olValue = pomSelectedList->GetValueRowCol(ilLc, imHideColStart);
			if(!olValue.IsEmpty())
			{
				omSelectedItems.Add(olValue);
			}

		}
	}
}

BEGIN_MESSAGE_MAP(CPsBewGatFilterPage, CPropertyPage)
	//{{AFX_MSG_MAP(CCCPsBewGatFilterPage)
	ON_BN_CLICKED(IDC_BUTTON_ADD, OnButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE, OnButtonRemove)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CCCPsBewGatFilterPage 
void CPsBewGatFilterPage::OnButtonAdd() 
{
	CString olText;
	bool blFoundAlle = false;
	CGXStyle olStyle;
	CRowColArray olRows;
	int ilSelCount = (int)pomPossilbeList->GetSelectedRows( olRows);

	
	if(ilSelCount > 0)
	{
		pomPossilbeList->SetReadOnly(FALSE);
	    pomSelectedList->SetReadOnly(FALSE);

		int ilContCount = m_InsertList.GetCount() + 1;
		m_InsertList.ResetContent();
		for (int ilLc = 1 ; ilLc < min(ilContCount+1,(int)pomPossilbeList->GetRowCount()); ilLc++)
		{
			olText = pomPossilbeList->GetValueRowCol(ilLc, imHideColStart);
			m_InsertList.AddString(olText);
		}

		
		int ilFscCount = m_ContentList.GetCount()+ 1;

		CString olComplText;
	// Move selected items from left list box to right list box
		for ( ilLc = olRows.GetSize()-1; 
		ilLc >= 0 && blFoundAlle == false; ilLc--)
		{
			int ilDummy = (int)olRows[ilLc];
			olText = pomPossilbeList->GetValueRowCol(olRows[ilLc], 1);
			if(olText.IsEmpty() && imHideColStart > 2)
			{
				olText = pomPossilbeList->GetValueRowCol(olRows[ilLc], 2);
			}
			olComplText = pomPossilbeList->GetValueRowCol(olRows[ilLc], imHideColStart);
		
			if(ilDummy <= 0 || olText.IsEmpty())
				continue;
			if (olText == LoadStg(IDS_STRING61216))
			{
			
				/************ using all *******************/
				m_AddButton.EnableWindow(FALSE);
				blFoundAlle = true;
				/*** remove all entries except "*Alle" from content list **/
				m_ContentList.ResetContent();
				m_ContentList.AddString(LoadStg(IDS_STRING61216));	
				/*** show empty insert list ***/
				m_InsertList.ResetContent();	
			}
			else
			{
				int iltest = m_InsertList.FindStringExact(-1, olComplText);
				m_ContentList.AddString(olComplText);	// move string from left to right box
				m_InsertList.DeleteString(m_InsertList.FindStringExact(-1, olComplText));
			}
		}
//		pomPossilbeList->Clear(FALSE);


		olStyle.SetEnabled(FALSE);
		olStyle.SetReadOnly(TRUE);
		
		pomPossilbeList->RemoveRows(1,  pomPossilbeList->GetRowCount());

		CStringArray olItemList;
		CString olOrgText;
		int ilCount = m_InsertList.GetCount() + 1;

		int ilRealCount = 0;
		pomPossilbeList->SetRowCount(max(18,ilCount));
		for ( ilLc = 0; ilLc < ilCount-1; ilLc++)
		{
			m_InsertList.GetText(ilLc,olText);
			olOrgText = olText;
			if(!olText.IsEmpty())
			{
				ilRealCount++;
				ExtractItemList(olText, &olItemList, '�');
				if(olItemList.GetSize() > 1)
				{
					olText = olItemList[1];
					pomPossilbeList->SetStyleRange(CGXRange(ilRealCount, imHideColStart+1, ilRealCount, imHideColStart+1), olStyle.SetValue(olItemList[0]));

				}
				ExtractItemList(olText, &olItemList, '%');
				for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
					pomPossilbeList->SetStyleRange(CGXRange(ilRealCount, ilItem+1, ilRealCount, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
				pomPossilbeList->SetStyleRange(CGXRange(ilRealCount, imHideColStart, ilRealCount, imHideColStart), olStyle.SetValue(olOrgText));
			}
		}

		ilRealCount = 0;
		pomSelectedList->RemoveRows(1,  pomSelectedList->GetRowCount());
		ilCount = m_ContentList.GetCount()+ 1;
		pomSelectedList->SetRowCount(max(18,ilCount));
		for (ilLc = 0; ilLc < ilCount-1; ilLc++)
		{
			m_ContentList.GetText(ilLc,olText);
			olOrgText = olText;
			if(!olText.IsEmpty())
			{
				ilRealCount++;
				ExtractItemList(olText, &olItemList, '�');
				if(olItemList.GetSize() > 1)
				{
					olText = olItemList[1];
					pomSelectedList->SetStyleRange(CGXRange(ilRealCount, imHideColStart+1, ilRealCount, imHideColStart+1), olStyle.SetValue(olItemList[0]));

				}
				ExtractItemList(olText, &olItemList, '%');
				for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
					pomSelectedList->SetStyleRange(CGXRange(ilRealCount, ilItem+1, ilRealCount, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
				pomSelectedList->SetStyleRange(CGXRange(ilRealCount, imHideColStart, ilRealCount, imHideColStart), olStyle.SetValue(olOrgText));
			}
		}


	    pomPossilbeList->SetReadOnly(TRUE);
	    pomSelectedList->SetReadOnly(TRUE);
	}
}

void CPsBewGatFilterPage::OnButtonRemove() 
{
	CString olText;
	CGXStyle olStyle;
	CRowColArray olRows;
	bool blFoundAlle = false;

	// Move selected items from right list box to left list box
	int ilSelCount = (int)pomSelectedList->GetSelectedRows( olRows);
	
	if(ilSelCount > 0)
	{

	    pomPossilbeList->SetReadOnly(FALSE);
	    pomSelectedList->SetReadOnly(FALSE);

		int ilContCount = m_ContentList.GetCount()+ 1;
		m_ContentList.ResetContent();
		for (int ilLc = 1 ; ilLc < min(ilContCount+1,(int)pomSelectedList->GetRowCount()); ilLc++)
		{
			olText = pomSelectedList->GetValueRowCol(ilLc, imHideColStart);
			m_ContentList.AddString(olText);
		}


		CString olComplText;

	// Move selected items from left list box to right list box
		for (ilLc = olRows.GetSize()-1; 
		ilLc >= 0 && blFoundAlle == false; ilLc--)
		{
			olText = pomSelectedList->GetValueRowCol(olRows[ilLc], 1);
			if(olText.IsEmpty() && imHideColStart > 2)
			{
				olText = pomSelectedList->GetValueRowCol(olRows[ilLc], 2);
			}
			olComplText = pomSelectedList->GetValueRowCol(olRows[ilLc], imHideColStart);

			int ilDummy = (int)olRows[ilLc];
			if(ilDummy <= 0 || olText.IsEmpty())
				continue;
			if (olText == LoadStg(IDS_STRING61216))
			{
				m_AddButton.EnableWindow(TRUE);
				blFoundAlle = true;
				/*** show empty content list ***/
				m_ContentList.ResetContent();
				/*** rebuild insert list ***/
				m_InsertList.ResetContent();
				m_InsertList.AddString(LoadStg(IDS_STRING61216));
				for (ilLc = 0; ilLc < omPossibleItems.GetSize(); ilLc++)
				{
					m_InsertList.AddString(omPossibleItems[ilLc]);
				}

			}
			else
			{
				int iltest = m_ContentList.FindStringExact(-1, olComplText);
				m_InsertList.AddString(olComplText);	// move string from right to left box
				m_ContentList.DeleteString(m_ContentList.FindStringExact(-1, olComplText));
			}
		}
	
		olStyle.SetEnabled(FALSE);
		olStyle.SetReadOnly(TRUE);
		
		pomPossilbeList->RemoveRows(1,  pomPossilbeList->GetRowCount());

		CStringArray olItemList;

		int ilCount = m_InsertList.GetCount() + 1;

		int ilRealCount = 0;
		CString olOrgText;
		pomPossilbeList->SetRowCount(max(18,ilCount));
		for ( ilLc = 0; ilLc < ilCount-1; ilLc++)
		{
			m_InsertList.GetText(ilLc,olText);
			olOrgText = olText;
			if(!olText.IsEmpty())
			{
				ilRealCount++;

				ExtractItemList(olText, &olItemList, '�');
				if(olItemList.GetSize() > 1)
				{
					olText = olItemList[1];
					pomPossilbeList->SetStyleRange(CGXRange(ilRealCount, imHideColStart+1, ilRealCount, imHideColStart+1), olStyle.SetValue(olItemList[0]));

				}

				ExtractItemList(olText, &olItemList, '%');
				for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
					pomPossilbeList->SetStyleRange(CGXRange(ilRealCount, ilItem+1, ilRealCount, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
				pomPossilbeList->SetStyleRange(CGXRange(ilRealCount, imHideColStart, ilRealCount, imHideColStart), olStyle.SetValue(olOrgText));
			}
		}

		if (blFoundAlle)
		{
			CGXSortInfoArray sortInfo;
			sortInfo.SetSize(1);							// 1 key only (you can also have more keys)
			sortInfo[0].nRC = 1;							// column nCol is the key
			sortInfo[0].sortType = CGXSortInfo::autodetect; // the grid will determine if the key is a date, numeric or alphanumeric value
			sortInfo[0].sortOrder = CGXSortInfo::ascending; // sort ascending  
				  
			pomPossilbeList->SortRows(CGXRange().SetTable(), sortInfo);		// Call SortRows to let the grid do the sorting
		}

		ilRealCount = 0;
		pomSelectedList->RemoveRows(1,  pomSelectedList->GetRowCount());
		ilCount = m_ContentList.GetCount()+ 1;
		pomSelectedList->SetRowCount(max(18,ilCount));
		for (ilLc = 0; ilLc < ilCount-1; ilLc++)
		{
			m_ContentList.GetText(ilLc,olText);
			olOrgText = olText;
			if(!olText.IsEmpty())
			{
				ilRealCount++;
				ExtractItemList(olText, &olItemList, '�');
				if(olItemList.GetSize() > 1)
				{
					olText = olItemList[1];
					pomSelectedList->SetStyleRange(CGXRange(ilRealCount, imHideColStart+1, ilRealCount, imHideColStart+1), olStyle.SetValue(olItemList[0]));

				}
				ExtractItemList(olText, &olItemList, '%');
				for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
					pomSelectedList->SetStyleRange(CGXRange(ilRealCount, ilItem+1, ilRealCount, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
				pomSelectedList->SetStyleRange(CGXRange(ilRealCount, imHideColStart, ilRealCount, imHideColStart), olStyle.SetValue(olOrgText));
			}
		}
	  
		pomPossilbeList->SetReadOnly(TRUE);
	    pomSelectedList->SetReadOnly(TRUE);

	}
}


BOOL CPsBewGatFilterPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();


	CWnd *polWnd = GetDlgItem(IDC_BUTTON_ADD);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61323));
	}
	polWnd = GetDlgItem(IDC_BUTTON_REMOVE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61324));
	}

	if(pomPossilbeList != NULL)
		delete pomPossilbeList;
	if(pomSelectedList != NULL)
		delete pomSelectedList;

	pomPossilbeList = new CGridFenster(this);
	pomSelectedList = new CGridFenster(this);


	int ilLc = omPossibleItems.GetSize() + 2;
//	int i;
	int ilIndex = -1;

	pomSelectedList->SubclassDlgItem(IDC_SELLIST, this);
	pomPossilbeList->SubclassDlgItem(IDC_POSLIST, this);
	pomSelectedList->Initialize();
	pomPossilbeList->Initialize();

	CGXStyle olStyle;

	pomPossilbeList->LockUpdate(TRUE);
	pomPossilbeList->GetParam()->EnableUndo(FALSE);
	pomPossilbeList->GetParam()->EnableTrackColWidth(FALSE);
	pomPossilbeList->GetParam()->EnableTrackRowHeight(FALSE);
//	pomPossilbeList->GetParam()->EnableSelection(GX_SELMULTIPLE  | GX_SELSHIFT   );
	pomPossilbeList->GetParam()->SetNumberedColHeaders(FALSE);

	pomSelectedList->LockUpdate(TRUE);
	pomSelectedList->GetParam()->EnableUndo(FALSE);
	pomSelectedList->GetParam()->EnableTrackColWidth(FALSE);
	pomSelectedList->GetParam()->EnableTrackRowHeight(FALSE);
//	pomSelectedList->GetParam()->EnableSelection(GX_SELMULTIPLE  | GX_SELSHIFT   );
	pomSelectedList->GetParam()->SetNumberedColHeaders(FALSE);

	
	pomPossilbeList->SetColCount(imColCount);

	for(int illc = 0; illc < imColCount; illc++)
		pomPossilbeList->SetColWidth(0,illc,40);
	
	pomPossilbeList->SetColWidth(0,1,195);

	pomPossilbeList->SetColWidth(0,0,30);


	pomSelectedList->SetColCount(imColCount);

	for( illc = 0; illc < imColCount; illc++)
		pomSelectedList->SetColWidth(0,illc,40);


	pomSelectedList->SetColWidth(0,1,195);

	pomSelectedList->SetColWidth(0,0,30);

	pomPossilbeList->SetRowHeight(0, 0, 12);
		
	pomSelectedList->SetRowHeight(0, 0, 12);
		
		
	olStyle.SetEnabled(FALSE);
	olStyle.SetReadOnly(TRUE);

	pomPossilbeList->LockUpdate(FALSE);
	pomSelectedList->LockUpdate(FALSE);
	

	pomSelectedList->HideCols(imHideColStart, imColCount); 
//	pomSelectedList->SetSortQuery(3, 14); 
//	pomSelectedList->SetSortQuery(9, 15); 
	

	pomPossilbeList->HideCols(imHideColStart, imColCount); 


	CString olText;
	CString olOrgText;
	int ilCount = m_InsertList.GetCount() + 1;

	CStringArray olItemList;
	pomPossilbeList->SetRowCount(max(18,ilCount));
	for ( ilLc = 0; ilLc < ilCount-1; ilLc++)
	{
		m_InsertList.GetText(ilLc,olText);
		olOrgText = olText;
		ExtractItemList(olText, &olItemList, '�');
		if(olItemList.GetSize() > 1)
		{
			olText = olItemList[1];
			pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, imHideColStart+1, ilLc+1, imHideColStart+1), olStyle.SetValue(olItemList[0]));

		}

		ExtractItemList(olText, &olItemList, '%');
		for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
			pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
		pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, imHideColStart, ilLc+1, imHideColStart), olStyle.SetValue(olOrgText));

	}
	ilCount = m_ContentList.GetCount()+ 1;
		
	pomSelectedList->SetRowCount(max(18,ilCount));

	for (ilLc = 0; ilLc < ilCount-1; ilLc++)
	{
		m_ContentList.GetText(ilLc,olText);
		olOrgText = olText;
		ExtractItemList(olText, &olItemList, '�');
		if(olItemList.GetSize() > 1)
		{
			olText = olItemList[1];
			pomSelectedList->SetStyleRange(CGXRange(ilLc+1, imHideColStart+1, ilLc+1, imHideColStart+1), olStyle.SetValue(olItemList[0]));

		}
		ExtractItemList(olText, &olItemList, '%');
		for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
			pomSelectedList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
		pomSelectedList->SetStyleRange(CGXRange(ilLc+1, imHideColStart, ilLc+1, imHideColStart), olStyle.SetValue(olOrgText));

	}
	pomSelectedList->Redraw();
	pomPossilbeList->Redraw();
	
	blIsInit = true;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}