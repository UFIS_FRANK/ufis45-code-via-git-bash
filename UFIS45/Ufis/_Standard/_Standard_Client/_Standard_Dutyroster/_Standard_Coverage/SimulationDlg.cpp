// SimulationDlg.cpp : implementation file
//

#include <stdafx.h>
#include <coverage.h>
#include <SimulationDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SimulationDlg dialog


SimulationDlg::SimulationDlg(CWnd* pParent, CoverageDiagramViewer *popViewer/*=NULL*/)
	: CDialog(SimulationDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(SimulationDlg)
	m_DemandVal = _T("");
	//}}AFX_DATA_INIT
	pomParent = pParent;
	pomViewer = popViewer;
}


void SimulationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SimulationDlg)
	DDX_Control(pDX, IDC_SPIN_OFFSET, m_SpinOffset);
	DDX_Control(pDX, IDC_DEMAND, m_Demand);
	DDX_Control(pDX, IDC_SPIN_DEMAND, m_SpinDemand);
	DDX_Control(pDX, IDC_ABS, m_Abs);
	DDX_Control(pDX, IDC_PERCENT, m_Percent);
	DDX_Control(pDX, IDC_CONSTANT, m_Constant);
	DDX_Control(pDX, IDC_MINS, m_Mins);
	DDX_Control(pDX, IDC_HOURS, m_Hours);
	DDX_Control(pDX, IDC_OFFSET, m_Offset);
	DDX_Text(pDX, IDC_DEMAND, m_DemandVal);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SimulationDlg, CDialog)
	//{{AFX_MSG_MAP(SimulationDlg)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_DEMAND, OnDeltaposSpinDemand)
	ON_EN_KILLFOCUS(IDC_DEMAND, OnKillfocusDemand)
	ON_BN_CLICKED(IDC_ABS, OnAbs)
	ON_BN_CLICKED(IDC_PERCENT, OnPercent)
	ON_BN_CLICKED(IDC_CONSTANT, OnConstant)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_OFFSET, OnDeltaposSpinOffset)
	ON_BN_CLICKED(IDC_HOURS, OnHours)
	ON_BN_CLICKED(IDC_MINS, OnMins)
	ON_EN_KILLFOCUS(IDC_OFFSET, OnKillfocusOffset)
	ON_BN_CLICKED(IDC_STOP, OnStop)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SimulationDlg message handlers

void SimulationDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	ShowWindow(SW_HIDE);

	pomViewer->SimulationActive(true);

	CString olText;
	m_Offset.GetWindowText(olText);
	if(m_Mins.GetCheck()==1)
	{
		// Offset in minutes
		pomViewer->SimOffset(atoi(olText));
	}
	else
	{
		// Offset in hours
		pomViewer->SimOffset(atoi(olText)*60);
	}
	
	m_Demand.GetWindowText(olText);
	pomViewer->SimValue(atoi(olText));
	if(m_Abs.GetCheck()==1)
	{
		pomViewer->SimType(SIM_ABSOLUTE);
	}
	else if(m_Percent.GetCheck()==1)
	{
		pomViewer->SimType(SIM_PERCENT);
	}
	else
	{
		pomViewer->SimType(SIM_CONSTANT);
	}
	pomParent->SendMessage(WM_SIMULATE,0,0);	 
	//CDialog::OnOK();
}

BOOL SimulationDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CWnd *polWnd = GetDlgItem(IDC_VERSATZ);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61338));
	}
	polWnd = GetDlgItem(IDC_MINS);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61337));
	}
	polWnd = GetDlgItem(IDC_HOURS);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61339));
	}
	polWnd = GetDlgItem(IDC_DEVIATION);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61340));
	}
	polWnd = GetDlgItem(IDC_ABS);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61341));
	}
	polWnd = GetDlgItem(IDC_PERCENT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61342));
	}
	polWnd = GetDlgItem(IDOK);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61343));
	}
	polWnd = GetDlgItem(IDC_STOP);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61344));
	}


	SetWindowText(LoadStg(IDS_STRING1710));


	// TODO: Add extra initialization here
	m_Abs.SetCheck(1);
	m_Percent.SetCheck(0);
	m_Constant.SetCheck(0);

	m_SpinDemand.SetRange(-9999,9999);
	m_SpinDemand.SetPos(0);
	m_SpinDemand.SetBuddy(GetDlgItem(IDC_DEMAND));
	m_Demand.SetWindowText(CString("0"));

	m_Mins.SetCheck(1);
	m_Hours.SetCheck(0);

	m_SpinOffset.SetRange(-9999,9999);
	m_SpinOffset.SetPos(0);
	m_SpinOffset.SetBuddy(GetDlgItem(IDC_OFFSET));
	m_Offset.SetWindowText(CString("0"));
	ShowWindow(SW_SHOW);	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void SimulationDlg::OnDeltaposSpinDemand(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	int ilMinRange,ilMaxRange;
	// TODO: Add your control notification handler code here
	int ilPos = pNMUpDown->iPos;
	int ilDelta = pNMUpDown->iDelta;
 	CString olText;
	olText.Format("%d",ilPos+ilDelta);
	m_SpinDemand.GetRange(ilMinRange,ilMaxRange);
	if((ilPos+ilDelta)>=ilMinRange && (ilPos+ilDelta)<=ilMaxRange)
	{
		(m_SpinDemand.GetBuddy())->SetWindowText(olText);
	}
	*pResult = 0;
}

void SimulationDlg::OnKillfocusDemand() 
{
	// TODO: Add your control notification handler code here
	CString olText;
	int ilPos ;
	int ilMinRange,ilMaxRange;

	m_SpinDemand.GetRange(ilMinRange,ilMaxRange);
	m_Demand.GetWindowText(olText);
	ilPos = atoi(olText);
	if(ilPos>=ilMinRange && ilPos<=ilMaxRange)
	{
		m_SpinDemand.SetPos(ilPos);
	}
	else if(ilPos>ilMaxRange)
	{
		m_SpinDemand.SetPos(ilMaxRange);
		olText.Format("%d",ilMaxRange);
		m_Demand.SetWindowText(olText);
	}
	else
	{
		m_SpinDemand.SetPos(ilMinRange);
		olText.Format("%d",ilMinRange);
		m_Demand.SetWindowText(olText);
	}
}

void SimulationDlg::OnAbs() 
{
	// TODO: Add your control notification handler code here
	CString olText;
	m_Demand.GetWindowText(olText);
	int ilPos = atoi(olText); //m_SpinDemand.GetPos();
	m_SpinDemand.SetRange(-999,999);
	if(ilPos<-999)
	{
		m_SpinDemand.SetPos(-999);
		(m_SpinDemand).GetBuddy()->SetWindowText(CString("-999"));
	}
	else if(ilPos>999)
	{
		m_SpinDemand.SetPos(999);
		(m_SpinDemand).GetBuddy()->SetWindowText(CString("999"));
	}
	else
	{
		m_SpinDemand.SetPos(ilPos);
	}
}

void SimulationDlg::OnPercent() 
{
	// TODO: Add your control notification handler code here
	CString olText;
	m_Demand.GetWindowText(olText);
	int ilPos = atoi(olText); //m_SpinDemand.GetPos();

	m_SpinDemand.SetRange(-100,999);
	if(ilPos<-100)
	{
		m_SpinDemand.SetPos(-100);
		(m_SpinDemand).GetBuddy()->SetWindowText(CString("-100"));
	}
	else if(ilPos>999)
	{
		m_SpinDemand.SetPos(999);
		(m_SpinDemand).GetBuddy()->SetWindowText(CString("999"));
	}
	else
	{
		m_SpinDemand.SetPos(ilPos);
	}
}

void SimulationDlg::OnConstant() 
{
	// TODO: Add your control notification handler code here
	CString olText;
	m_Demand.GetWindowText(olText);
	int ilPos = atoi(olText); //m_SpinDemand.GetPos();
	m_SpinDemand.SetRange(0,999);
	if(ilPos<0)
	{
		m_SpinDemand.SetPos(0);
		(m_SpinDemand).GetBuddy()->SetWindowText(CString("0"));
	}
	else if(ilPos>999)
	{
		m_SpinDemand.SetPos(999);
		(m_SpinDemand).GetBuddy()->SetWindowText(CString("999"));
	}
	else
	{
		m_SpinDemand.SetPos(ilPos);
	}
}

void SimulationDlg::OnDeltaposSpinOffset(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	int ilMinRange,ilMaxRange;
	// TODO: Add your control notification handler code here
	m_SpinOffset.GetRange(ilMinRange,ilMaxRange);
	
	int ilPos = pNMUpDown->iPos;
	int ilDelta = pNMUpDown->iDelta;
 	CString olText;
	
	olText.Format("%d",ilPos+ilDelta);
	
	if((ilPos+ilDelta)>=ilMinRange && (ilPos+ilDelta)<=ilMaxRange)
	{
		(m_SpinOffset.GetBuddy())->SetWindowText(olText);
	}
	*pResult = 0;
}

void SimulationDlg::OnHours() 
{
	// TODO: Add your control notification handler code here
	CString olText;
	m_Offset.GetWindowText(olText);
	int ilPos = atoi(olText); //m_SpinOffset.GetPos();
	
	m_SpinOffset.SetRange(-999,999);
	if(ilPos<-999)
	{
		m_SpinOffset.SetPos(-999);
		(m_SpinOffset).GetBuddy()->SetWindowText(CString("-999"));
	}
	else if(ilPos>999)
	{
		m_SpinOffset.SetPos(999);
		(m_SpinOffset).GetBuddy()->SetWindowText(CString("999"));
	}
	else
	{
		m_SpinOffset.SetPos(ilPos);
	}
}

void SimulationDlg::OnMins() 
{
	// TODO: Add your control notification handler code here
	CString olText;
	m_Offset.GetWindowText(olText);
	int ilPos = atoi(olText); //m_SpinOffset.GetPos();

	m_SpinOffset.SetRange(-999,999);
	if(ilPos<-999)
	{
		m_SpinOffset.SetPos(-999);
		(m_SpinOffset).GetBuddy()->SetWindowText(CString("-999"));
	}
	else if(ilPos>999)
	{
		m_SpinOffset.SetPos(999);
		(m_SpinOffset).GetBuddy()->SetWindowText(CString("999"));
	}
	else
	{
		m_SpinOffset.SetPos(ilPos);
	}
}

void SimulationDlg::OnKillfocusOffset() 
{
	// TODO: Add your control notification handler code here
	CString olText;
	int ilPos ;
	int ilMinRange,ilMaxRange;

	m_SpinOffset.GetRange(ilMinRange,ilMaxRange);
	m_Offset.GetWindowText(olText);
	ilPos = atoi(olText);
	if(ilPos>=ilMinRange && ilPos<=ilMaxRange)
	{
		m_SpinOffset.SetPos(ilPos);
	}
	else if(ilPos>ilMaxRange)
	{
		m_SpinOffset.SetPos(ilMaxRange);
		olText.Format("%d",ilMaxRange);
		m_Offset.SetWindowText(olText);
	}
	else
	{
		m_SpinOffset.SetPos(ilMinRange);
		olText.Format("%d",ilMinRange);
		m_Offset.SetWindowText(olText);
	}
}

void SimulationDlg::OnStop() 
{
	// TODO: Add your control notification handler code here
	pomViewer->SimulationActive(false);
	ShowWindow(SW_HIDE);
	pomParent->SendMessage(WM_SIMULATE,0,0);	
}
