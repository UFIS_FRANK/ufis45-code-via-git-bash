// BasisschichtenDlg.cpp : implementation file
//

#include <stdafx.h>
//#include <bdpsuif.h>
#include <BasisschichtenDlg.h>
#include <PrivList.h>
#include <CedaOdaData.h>
#include <CedaParData.h>
#include <CheckReferenz.h>

//*** 09.11.99 SHA ***
#include <AwDlg.h>
#include <CedaPfcData.h>
#include <CedaOrgData.h>

#include <CedaBasicData.h>

#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// BasisschichtenDlg dialog


BasisschichtenDlg::BasisschichtenDlg(BSDDATA *popBsd, bool bpIsDynamic/*true*/, CWnd* pParent /*=NULL*/) : CDialog(BasisschichtenDlg::IDD, pParent)
{
	pomBsd = popBsd;
	bmIsDynamic = bpIsDynamic;
	pomStatus = NULL;
	//ogPfcData.Read();   //already read

	//{{AFX_DATA_INIT(BasisschichtenDlg)
	//}}AFX_DATA_INIT
}


void BasisschichtenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(BasisschichtenDlg)
	DDX_Control(pDX, IDC_VATO_T_, m_VATO_T);
	DDX_Control(pDX, IDC_VATO_D_, m_VATO_D);
	DDX_Control(pDX, IDC_VAFR_T_, m_VAFR_T);
	DDX_Control(pDX, IDC_VAFR_D_, m_VAFR_D);
	DDX_Control(pDX, IDC_DPT1_, m_DPT1);
	DDX_Control(pDX, IDC_RGSB_, m_RGSB);
	DDX_Control(pDX, IDC_FCTC_, m_FCTC);
	DDX_Control(pDX, IDC_BKR1_S, m_BKR1_S);
	DDX_Control(pDX, IDC_BKR1_A, m_BKR1_A);
	DDX_Control(pDX, IDOK,		 m_OK);
	DDX_Control(pDX, IDC_DYN,	 m_DYN);
	DDX_Control(pDX, IDC_STAT,   m_STAT);
	DDX_Control(pDX, IDC_EXTRA,	 m_EXTRA);
	DDX_Control(pDX, IDC_BKD1,	 m_BKD1);
	DDX_Control(pDX, IDC_BKF1,   m_BKF1);
	DDX_Control(pDX, IDC_BKT1,   m_BKT1);
	DDX_Control(pDX, IDC_BSDC,   m_BSDC);
	DDX_Control(pDX, IDC_BSDK,   m_BSDK);
	DDX_Control(pDX, IDC_BSDN,   m_BSDN);
	DDX_Control(pDX, IDC_BSDS,   m_BSDS);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_CTRC1,	 m_CTRC1);
	DDX_Control(pDX, IDC_ESBG,	 m_ESBG);
	DDX_Control(pDX, IDC_LSEN,	 m_LSEN);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_REMA,	 m_REMA);
	DDX_Control(pDX, IDC_SDU1,	 m_SDU1);
	DDX_Control(pDX, IDC_SEX1,	 m_SEX1);
	DDX_Control(pDX, IDC_SSH1,	 m_SSH1);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_JUMPER_, m_JUMPER);
	DDX_Control(pDX, IDC_RGBC_,	 m_RGBC);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(BasisschichtenDlg, CDialog)
	//{{AFX_MSG_MAP(BasisschichtenDlg)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnKillfocus)
	ON_BN_CLICKED(IDC_B_FCTC_, OnBFctc)
	ON_BN_CLICKED(IDC_B_DPT1_, OnBDpt1)
	ON_BN_CLICKED(IDC_BDELETE_, OnBdelete)
	ON_BN_CLICKED(IDC_BNEU_, OnBneu)
	ON_BN_CLICKED(IDC_RGBC_, OnRgbc)
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BasisschichtenDlg message handlers
//----------------------------------------------------------------------------------------

BOOL BasisschichtenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CString olStatusText = LoadStg(IDS_STRING61402);
	//pomStatus->SetWindowText(olStatusText);  //set bottom status bar , no need?
	AfxGetApp()->DoWaitCursor(1);
	//SHA20000112
	CString cDmy;

	if(m_Caption == LoadStg(IDS_STRING61403))
		bmChangeAction = true;
	else
		bmChangeAction = false;

	//set top bar title
	m_Caption = LoadStg(IDS_STRING61405) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	// set enable ,disable of this function, no need
	//char clStat = ogPrivList.GetStat("BASISSCHICHTENDLG.m_OK");
	//SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	//pomBsd->Cdat = -1; // set to -1 ,no date display
	m_CDATD.SetInitText(pomBsd->Cdat.Format("%d.%m.%Y"));
	//m_CDATD.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomBsd->Cdat.Format("%H:%M"));
	//m_CDATT.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	pomBsd->Lstu = -1;
	m_LSTUD.SetInitText(pomBsd->Lstu.Format("%d.%m.%Y"));
	//m_LSTUD.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomBsd->Lstu.Format("%H:%M"));
	//m_LSTUT.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomBsd->Usec);
	//m_USEC.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomBsd->Useu);
	//m_USEU.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_BSDC.SetFormat("x|#x|#x|#x|#x|#x|#x|#x|#");
	m_BSDC.SetTextLimit(1,8);
	m_BSDC.SetBKColor(YELLOW);  
	m_BSDC.SetTextErrColor(RED);
	m_BSDC.SetInitText(pomBsd->Bsdc);
	omOldCode = pomBsd->Bsdc;
	//m_BSDC.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_BSDC"));
	//------------------------------------
	m_BSDK.SetTypeToString("X(12)",12,1);
	//m_BSDK.SetBKColor(YELLOW);  
	m_BSDK.SetTextErrColor(RED);
	m_BSDK.SetInitText(pomBsd->Bsdk);
	//m_BSDK.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_BSDK"));
	//------------------------------------
	m_BSDN.SetTypeToString("X(40)",40,1);
	m_BSDN.SetBKColor(YELLOW);  
	m_BSDN.SetTextErrColor(RED);
	m_BSDN.SetInitText(pomBsd->Bsdn);
	//m_BSDN.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_BSDN"));
	//------------------------------------
	m_BSDS.SetFormat("x|#x|#x|#");
	m_BSDS.SetTextLimit(0,3);
	m_BSDS.SetTextErrColor(RED);
	m_BSDS.SetInitText(pomBsd->Bsds);
	//m_BSDS.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_BSDS"));
	//------------------------------------
	m_CTRC1.SetFormat("x|#x|#x|#x|#x|#");
	m_CTRC1.SetTextLimit(0,5);
	m_CTRC1.SetTextErrColor(RED);
	m_CTRC1.SetInitText(pomBsd->Ctrc);

	//m_CTRC1.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_CTRC1"));
	//------------------------------------
	//m_ESBG.SetTypeToTime(true);
	m_ESBG.SetBKColor(YELLOW);  
	m_ESBG.SetTextErrColor(RED);
	//SHA20000112
	m_ESBG.SetFormat("[##':'##]");
	m_ESBG.SetTypeToTime(true);
	//m_ESBG.SetFormat("0|1|2|':'##");
	if(strlen(pomBsd->Esbg) > 0)
	{
		char pclTmp[10]="";
		sprintf(pclTmp, "%s:%s", CString(pomBsd->Esbg).Left(2), CString(pomBsd->Esbg).Right(2));
		m_ESBG.SetInitText(pclTmp);
	}
	//m_ESBG.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_ESBG"));
	//------------------------------------
	//m_LSEN.SetTypeToTime(true);
	m_LSEN.SetBKColor(YELLOW);  
	m_LSEN.SetTextErrColor(RED);
	//SHA20000112
	m_LSEN.SetFormat("##':'##");
	m_LSEN.SetTypeToTime(true);
	if(strlen(pomBsd->Lsen) > 0)
	{
		char pclTmp[10]="";
		sprintf(pclTmp, "%s:%s", CString(pomBsd->Lsen).Left(2), CString(pomBsd->Lsen).Right(2));
		m_LSEN.SetInitText(pclTmp);
	}
	//m_LSEN.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_LSEN"));
	//------------------------------------
//	m_SDU1.SetTypeToInt(0, 9999);
//	m_SDU1.SetFormat("#x|#x|#x|#x|#");
//	m_SDU1.SetFormat("X(5)");

	//uhi 27.3.01
	m_SDU1.SetTypeToString("X(5)",5,1);
	m_SDU1.SetFormat("##':'##");
	m_SDU1.SetBKColor(YELLOW);  
	m_SDU1.SetTextErrColor(RED);

	/*m_SDU1.SetTypeToString("X(5)",5,0);
	m_SDU1.SetTypeToTime(true);
	m_SDU1.SetTextLimit(0,5);

	m_SDU1.SetBKColor(YELLOW);  
	m_SDU1.SetTextErrColor(RED);*/

	CString out = DBmin2Gui(pomBsd->Sdu1);

	//SHA20000112
	//no data means empty field not 00:00
	cDmy=pomBsd->Sdu1;
	if (cDmy=="" || cDmy==" ")
		out="";

//	int ilTmp = atoi(pomBsd->Sdu1);
//	double dlTmp = ilTmp / 60;
//	CString slTmp = "";
//	slTmp.Format("%f",dlTmp);
	m_SDU1.SetInitText(out);
	//m_SDU1.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_SDU1"));
	//------------------------------------
//	m_SEX1.SetTypeToInt(0, 9999);
//	m_SEX1.SetTypeToString("X(5)",5,0);
//	m_SEX1.SetTextErrColor(RED);

//	m_SEX1.SetFormat("[##':'##]");
//	out = DBmin2Gui(pomBsd->Sex1);

	//SHA20000112
	//no data means empty field not 00:00
//	cDmy=pomBsd->Sex1;
//	if (cDmy=="" || cDmy==" ")
//		out="";

//	ilTmp = atoi(pomBsd->Sex1);
//	dlTmp = ilTmp / 60;
//	slTmp = "";
//	slTmp.Format("%f",dlTmp);
//	m_SEX1.SetInitText(out);

	
//	m_SEX1.SetInitText(pomBsd->Sex1);
	//m_SEX1.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_SEX1"));
	//------------------------------------
//	m_SSH1.SetTypeToInt(0, 9999);
//	m_SSH1.SetTypeToString("X(5)",5,0);
//	m_SSH1.SetTextErrColor(RED);
//	m_SSH1.SetFormat("[##':'##]");
//	out = DBmin2Gui(pomBsd->Ssh1);
	
	//SHA20000112
	//no data means empty field not 00:00
//	cDmy=pomBsd->Ssh1;
//	if (cDmy=="" || cDmy==" ")
//		out="";

//	ilTmp = atoi(pomBsd->Ssh1);
//	dlTmp = ilTmp / 60;
//	slTmp = "";
//	slTmp.Format("%f",dlTmp);
//	m_SSH1.SetInitText(out);

//	m_SSH1.SetInitText(pomBsd->Ssh1);
	//m_SSH1.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_SSH1"));
	//------------------------------------
	//m_BKF1.SetTypeToTime(false);
	m_BKF1.SetFormat("[##':'##]");
	m_BKF1.SetTypeToTime(false);
	m_BKF1.SetTextLimit(0,5);

	m_BKF1.SetTextErrColor(RED);
	if(strlen(pomBsd->Bkf1) > 0)
	{
		char pclTmp[10]="";
		sprintf(pclTmp, "%s:%s", CString(pomBsd->Bkf1).Left(2), CString(pomBsd->Bkf1).Right(2));
		m_BKF1.SetInitText(pclTmp);
	}
//	m_BKF1.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_BKF1"));
	//------------------------------------
	//m_BKT1.SetTypeToTime(false);
	m_BKT1.SetFormat("[##':'##]");
	m_BKT1.SetTypeToTime(false);
	m_BKT1.SetTextLimit(0,5);
	m_BKT1.SetTextErrColor(RED);
	if(strlen(pomBsd->Bkt1) > 0)
	{
		char pclTmp[10]="";
		sprintf(pclTmp, "%s:%s", CString(pomBsd->Bkt1).Left(2), CString(pomBsd->Bkt1).Right(2));
		m_BKT1.SetInitText(pclTmp);
	}
//	m_BKT1.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_BKT1"));
	//------------------------------------
//	m_BKD1.SetTypeToInt(0, 9999);
	
	//uhi 27.3.01
	m_BKD1.SetTypeToString("X(5)",5,1);
	m_BKD1.SetFormat("##':'##");
	m_BKD1.SetBKColor(YELLOW);  
	m_BKD1.SetTextErrColor(RED);
	
	out = DBmin2Gui(pomBsd->Bkd1);

	//SHA20000112
	//no data means empty field not 00:00
	cDmy=pomBsd->Bkd1;
	if (cDmy=="" || cDmy==" ")
		out="";

//	ilTmp = atoi(pomBsd->Bkd1);
//	dlTmp = ilTmp / 60;
//	slTmp = "";
//	slTmp.Format("%f",dlTmp);
	m_BKD1.SetInitText(out);

	if (strcmp(pomBsd->Bewc, "J") == 0)
	{
		m_JUMPER.SetCheck(TRUE);
	}
	else
	{
		m_JUMPER.SetCheck(FALSE);
	}

	//------------------------------------
	m_REMA.SetTypeToString("X(60)",60,0);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomBsd->Rema);

	//--------------------------------------
	//clStat = ogPrivList.GetStat("BASISSCHICHTENDLG.m_BKR");  // no need
	m_BKR1_A.SetCheck(1);
	if(strcmp(pomBsd->Bkr1, "A") == 0) 
	{
		m_BKR1_A.SetCheck(1);
		m_BKR1_S.SetCheck(0);
	}
	else if(strcmp(pomBsd->Bkr1, "S") == 0) 
	{
		m_BKR1_S.SetCheck(1);
		m_BKR1_A.SetCheck(0);
	}
//	SetWndStatAll(clStat,m_BKR1_S);
//	SetWndStatAll(clStat,m_BKR1_A);
	
	m_DYN.SetCheck(0);
	m_STAT.SetCheck(0);
	m_EXTRA.SetCheck(0);
	// Type option buttons
	if(strcmp(pomBsd->Type, "D") == 0)
	{
		m_DYN.SetCheck(1);
	}
	else if(strcmp(pomBsd->Type, "S") == 0)
	{
		m_STAT.SetCheck(1);
	}
	else if(strcmp(pomBsd->Type, "E") == 0)
	{
		m_EXTRA.SetCheck(1);
	}
	else
	{
		m_STAT.SetCheck(1);
	}
	//--------------------------------------

	//*** 09.11.99 SHA ***
//#ifdef CLIENT_ZRH
	m_FCTC.SetTypeToString("X(5)",5,0);
	m_FCTC.SetTextErrColor(RED);
	m_FCTC.SetInitText(pomBsd->Fctc);

	m_DPT1.SetTypeToString("X(8)",8,0);
	m_DPT1.SetBKColor(SILVER);
	m_DPT1.SetTextErrColor(RED);
	if (strlen(pomBsd->Fctc) > 0)
	{
		PFCDATA *prlPfc = ogPfcData.GetPfcByFctc(pomBsd->Fctc);
		if (prlPfc != NULL)
		{
			m_DPT1.SetInitText(prlPfc->Dptc);
		}
		else
		{
			m_DPT1.SetInitText("");
		}
	}
	else
	{
		m_DPT1.SetInitText("");
	}

//	m_RGSB.SetTypeToTime(true);
	m_RGSB.SetTypeToDouble(3,2,0.0,999.99);
	m_RGSB.SetTextErrColor(RED);
//	m_RGSB.SetFormat("[##':'##]");
	if(strlen(pomBsd->Rgsb) > 0)
	{
		char pclTmp[10]="";
		sprintf(pclTmp, "%s:%s", CString(pomBsd->Rgsb).Left(2), CString(pomBsd->Rgsb).Right(2));
		m_RGSB.SetInitText(pclTmp);
	}

	m_VAFR_D.SetTypeToDate(true);
	m_VAFR_D.SetTextErrColor(RED);
	m_VAFR_D.SetBKColor(YELLOW);
	CString olTest = pomBsd->Vafr.Format("%d.%m.%Y");
	m_VAFR_D.SetInitText(olTest);
	// - - - - - - - - - - - - - - - - - -
	m_VAFR_T.SetTypeToTime(true);
	m_VAFR_T.SetTextErrColor(RED);
	m_VAFR_T.SetBKColor(YELLOW);
	m_VAFR_T.SetInitText("00:00");
	//m_VAFR_T.SetInitText(pomBsd->Vafr.Format("%H:%M"));
	//------------------------------------
	m_VATO_D.SetTypeToDate();
	m_VATO_D.SetTextErrColor(RED);
	CString olTest2 = pomBsd->Vato.Format("%d.%m.%Y");
	m_VATO_D.SetInitText(olTest2);
	// - - - - - - - - - - - - - - - - - -
	m_VATO_T.SetTypeToTime();
	m_VATO_T.SetTextErrColor(RED);
	m_VATO_T.SetInitText("23:59");
	//m_VATO_T.SetInitText(pomBsd->Vato.Format("%H:%M"));

	if (ogBasicData.IsColorCodeAvailable() && "ATH" == ogParData.GetParValue("ROSTER","ID_PROD_CUSTOMER"))
	{
		if (strlen(pomBsd->Rgbc) > 0)
		{
			sscanf(pomBsd->Rgbc,"%x",&this->omRgbc);
		}
		else
		{
			this->omRgbc = WHITE;
		}

		ogBasicData.SetWindowStat("BASISSCHICHTENDLG.m_RGBC",&this->m_RGBC);

	}
	else
	{
		this->omRgbc = WHITE;

		m_RGBC.ShowWindow(SW_HIDE);
		m_RGBC.EnableWindow(FALSE);
		CWnd *polWnd = GetDlgItem(IDC_EDIT_RGBC);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->EnableWindow(FALSE);
		}
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//~~~~~ INITIALIZE GRID
	omGrdDel = new CSTGrid;
	omGrdDel->SubClassDlgItem(IDC_GRID, this);
	omGrdDel->Initialize();
	omGrdDel->SetColCount(8);		//7 x data + 1 x status

	omGrdDel->GetParam()->EnableTrackRowHeight(FALSE);		//disable rowsizing
	omGrdDel->GetParam()->EnableMoveRows(FALSE);			//disable rowmoving
	omGrdDel->GetParam()->EnableSelection(GX_SELNONE);

	//*** HIDE URNO AND BSDU ***
	omGrdDel->SetColWidth(0,0,20);
	omGrdDel->SetColWidth(3,4,80);

	omGrdDel->HideCols(1,2);
	omGrdDel->HideCols(8,8);

	omGrdDel->SetValueRange(CGXRange(0,1), GetListItem(LoadStg(IDS_STRING61418),1,true,'|'));	// URNO
	omGrdDel->SetValueRange(CGXRange(0,2), GetListItem(LoadStg(IDS_STRING61418),2,true,'|'));	// BSDU
	omGrdDel->SetValueRange(CGXRange(0,4), GetListItem(LoadStg(IDS_STRING61418),3,true,'|'));	// FUNCTION
	omGrdDel->SetValueRange(CGXRange(0,3), GetListItem(LoadStg(IDS_STRING61418),4,true,'|'));	// Org-Unit
	omGrdDel->SetValueRange(CGXRange(0,5), GetListItem(LoadStg(IDS_STRING61418),5,true,'|'));	// Absence
	omGrdDel->SetValueRange(CGXRange(0,6), GetListItem(LoadStg(IDS_STRING61418),6,true,'|'));	// From
	omGrdDel->SetValueRange(CGXRange(0,7), GetListItem(LoadStg(IDS_STRING61418),7,true,'|'));	// To
	omGrdDel->SetValueRange(CGXRange(0,8), "STATUS");


	olStatusText += CString("||||||");
//	pomStatus->SetWindowText(olStatusText);
	
	CString clWhere, olBsdc; 
	long olBsdu;
	int bmCopy = 0;
	
	//uhi 24.04.01
	olBsdc = pomBsd->Bsdc;
	if(olBsdc != "" && pomBsd->Usec[0] == '\0' && pomBsd->Useu[0] == '\0' && pomBsd->Cdat == TIMENULL && pomBsd->Lstu == TIMENULL){
		CCSPtrArray<BSDDATA> olBsdList;
		char clWhere1[100];
		sprintf(clWhere1,"WHERE BSDC='%s'",olBsdc);
		if(ogBsdData.ReadSpecialData(&olBsdList, clWhere1, "URNO,BSDC", false) == true){
			olBsdu = olBsdList[0].Urno;
			clWhere.Format("WHERE BSDU='%d'",olBsdu);
			olBsdList.DeleteAll();
			bmCopy = 1;
		}
		else
			clWhere.Format("WHERE BSDU='%d'",pomBsd->Urno);
	}
	else
		clWhere.Format("WHERE BSDU='%d'",pomBsd->Urno);
	
	ogBCD.SetObject("DEL");
	ogBCD.Read("DEL",clWhere);

	int ilDataCount=ogBCD.GetDataCount("DEL"); //number of rows
	int ilDelCount = ilDataCount;
	omGrdDel->SetRowCount(ilDataCount);

	//*** FILL THE GRID WITH EXISTING DATA ***
	for(int i = 0; i < ilDataCount; i++)
	{
		RecordSet olRecord;
		BOOL blRead=ogBCD.GetRecord("DEL", i,  olRecord);
		
		if (bmCopy == 1){
			omGrdDel->SetValueRange(CGXRange(i+1,1),ogBasicData.GetNextUrno());
			omGrdDel->SetValueRange(CGXRange(i+1,2),pomBsd->Urno);
		}
		else{
			omGrdDel->SetValueRange(CGXRange(i+1,1),olRecord.Values[ogBCD.GetFieldIndex("DEL","URNO")]);
			omGrdDel->SetValueRange(CGXRange(i+1,2),olRecord.Values[ogBCD.GetFieldIndex("DEL","BSDU")]);
		}
		omGrdDel->SetValueRange(CGXRange(i+1,3),olRecord.Values[ogBCD.GetFieldIndex("DEL","DPT1")]);
		omGrdDel->SetValueRange(CGXRange(i+1,4),olRecord.Values[ogBCD.GetFieldIndex("DEL","FCTC")]);
		omGrdDel->SetValueRange(CGXRange(i+1,5),olRecord.Values[ogBCD.GetFieldIndex("DEL","SDAC")]);
		omGrdDel->SetValueRange(CGXRange(i+1,6),olRecord.Values[ogBCD.GetFieldIndex("DEL","DELF")]);
		omGrdDel->SetValueRange(CGXRange(i+1,7),olRecord.Values[ogBCD.GetFieldIndex("DEL","DELT")]);
		if (bmCopy == 1)
			omGrdDel->SetValueRange(CGXRange(i+1,8),"NEW");
		else
			omGrdDel->SetValueRange(CGXRange(i+1,8),"OK");
	}

	//*** COLLECT ALL THE FUNCTIONS ***

	olStatusText += CString("||||||");
	//pomStatus->SetWindowText(olStatusText);

	ogBCD.SetObject("PFC");
	ogBCD.Read("PFC");
	ogBCD.SetSort("PFC","FCTC+",true);
	ilDataCount=ogBCD.GetDataCount("PFC"); //number of rows
	for(i = 0; i < ilDataCount; i++)
	{
		RecordSet olRecord;
		BOOL blRead=ogBCD.GetRecord("PFC", i,  olRecord);
		omAllFctc = omAllFctc + "|" + olRecord.Values[ogBCD.GetFieldIndex("PFC","FCTC")] + "|";
		//omAllFctc = omAllFctc + olRecord.Values[ogBCD.GetFieldIndex("PFC","FCTC")] + "\t" +
			//olRecord.Values[ogBCD.GetFieldIndex("PFC","FCTN")] + "\n";
	}

	//*** COLLECT ALL THE ORGS ***
	olStatusText += CString("||||||");
	//pomStatus->SetWindowText(olStatusText);

	ogBCD.SetObject("ORG");
	ogBCD.Read("ORG");
	ogBCD.SetSort("ORG","DPT1+",true);
	ilDataCount=ogBCD.GetDataCount("ORG"); //number of rows
	for(i = 0; i < ilDataCount; i++)
	{
		RecordSet olRecord;
		BOOL blRead=ogBCD.GetRecord("ORG", i,  olRecord);
		omAllDpt1 = omAllDpt1 + "|" + olRecord.Values[ogBCD.GetFieldIndex("ORG","DPT1")] + "|";
		//omAllDpt1 = omAllDpt1 + olRecord.Values[ogBCD.GetFieldIndex("ORG","DPT1")] + "\t" +
			//olRecord.Values[ogBCD.GetFieldIndex("ORG","DPTN")] + "\n";
	}
	//*** COLLECT ALL THE ABSENCES ***
	olStatusText += CString("||||||");
	//pomStatus->SetWindowText(olStatusText);

	ogBCD.SetObject("ODA");
	ogBCD.Read("ODA");
	ogBCD.SetSort("ODA","SDAC+",true);
	ilDataCount=ogBCD.GetDataCount("ODA"); //number of rows
	for(i = 0; i < ilDataCount; i++)
	{
		RecordSet olRecord;
		BOOL blRead=ogBCD.GetRecord("ODA", i,  olRecord);
		omAllSdac = omAllSdac + "|" + olRecord.Values[ogBCD.GetFieldIndex("ODA","SDAC")] + "|";
		//omAllSdac = omAllSdac + olRecord.Values[ogBCD.GetFieldIndex("ODA","SDAC")] + "\t" +
			//olRecord.Values[ogBCD.GetFieldIndex("ODA","SDAC")] + "\n";
	}


	//*** SET CELLSTYLES ***
	if (ilDelCount>0)
	{

		omGrdDel->SetStyleRange(CGXRange(1,6,ilDelCount,6),
			  CGXStyle()
			  .SetControl(GX_IDS_CTRL_MASKEDIT)
			  .SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("##:##")));
		omGrdDel->SetStyleRange(CGXRange(1,7,ilDelCount,7),
			  CGXStyle()
			  .SetControl(GX_IDS_CTRL_MASKEDIT)
			  .SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("##:##")));
	}     
          

//#endif
	//pomStatus->SetWindowText("OK");
	AfxGetApp()->DoWaitCursor(-1);

	return TRUE;  
}

//----------------------------------------------------------------------------------------

void BasisschichtenDlg::OnOK() 
{

	//SHA2001-01-15
	CString oltempESBG;
	CString oltempLSEN;
	CString oltempSDU1;
	CString oltempBKD1;

	CString olVFROM;
	CString olVTO;
	
	CString ogNoData = LoadStg(IDS_STRING61426);
	CString ogNotFormat = LoadStg(IDS_STRING61427);

	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText, olNewCode;
	bool ilStatus = true;
	if(m_BSDC.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_BSDC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING61425) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING61425) + ogNotFormat;
		}
	}
	if(m_BSDK.GetStatus() == false) 
	{
		/*
		ilStatus = false;
		if(m_BSDK.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING330) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING330) + ogNotFormat;
		}
		*/
	}
	if(m_BSDN.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_BSDN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING61428) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING61428) + ogNotFormat;
		}
	}
	if(m_BSDS.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING61429) + ogNotFormat;
	}

	if(m_ESBG.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_ESBG.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING61430) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING61430) + ogNotFormat;
		}
	}
	if(m_LSEN.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_LSEN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING61431) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING61431) + ogNotFormat;
		}
	}
	if(m_SDU1.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING61432) + ogNotFormat;
	}
//	if(m_SEX1.GetStatus() == false) 
//	{
//		ilStatus = false;
//		olErrorText += LoadStg(IDS_STRING336) + ogNotFormat;
//	}
//	if(m_SSH1.GetStatus() == false) 
//	{
//		ilStatus = false;
//		olErrorText += LoadStg(IDS_STRING337) + ogNotFormat;
//	}
	CString olText;
	m_BKD1.GetWindowText(olText);
	if(olText.IsEmpty())
	{
		m_BKD1.SetWindowText("00:00");
	}
	if(m_BKD1.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING61433) + ogNotFormat;
	}
	if(m_BKF1.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING61434) + ogNotFormat;
	}
	if(m_BKT1.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING61435) + ogNotFormat;
	}
	if(m_REMA.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING61436) + ogNotFormat;
	}

	//*** 09.09.99 SHA ***
	if(m_SDU1.GetWindowTextLength() == 0) 
		m_SDU1.SetInitText("0");

	//*** 09.11.99 SHA ***
//#ifdef CLIENT_ZRH

	if(m_VAFR_D.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFR_D.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING61437) + LoadStg(IDS_STRING61438) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING61437) + LoadStg(IDS_STRING61438) + ogNotFormat;
		}
	}
	if(m_VAFR_T.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFR_T.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING61437) + LoadStg(IDS_STRING61438) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING61437) + LoadStg(IDS_STRING61438) + ogNotFormat;
		}
	}

	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFR_D.GetWindowText(olVafrd);
	m_VAFR_T.GetWindowText(olVafrt);
	m_VATO_D.GetWindowText(olVatod);

		if (olVatod!="")
			m_VATO_T.SetWindowText("2359");
		else
			m_VATO_T.SetWindowText("");

	m_VATO_T.GetWindowText(olVatot);

	if(m_VAFR_D.GetStatus() == true && m_VAFR_T.GetStatus() == true && m_VATO_D.GetStatus() == true && m_VATO_T.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);

		if (olTmpTimeFr == -1)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING61439);
		}
		else
		{
			int dmyYear;int dmyMonth;int dmyDay;int dmyHour;int dmyMinute;int dmySecond;

			dmyYear = olTmpTimeFr.GetYear();
			dmyMonth = olTmpTimeFr.GetMonth();
			dmyDay = olTmpTimeFr.GetDay();
			dmyHour = olTmpTimeFr.GetHour();
			dmyMinute = olTmpTimeFr.GetMinute();
			dmySecond = olTmpTimeFr.GetSecond();

			olVFROM.Format("%04d%02d%02d%02d%02d%02d",dmyYear ,dmyMonth,dmyDay,dmyHour,dmyMinute,dmySecond);
		}

		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if (olVatod != "")
		{
			if (olTmpTimeTo == -1)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING61456);
			}
			else
			{
				int dmyYear;int dmyMonth;int dmyDay;int dmyHour;int dmyMinute;int dmySecond;

				dmyYear = olTmpTimeTo.GetYear();
				dmyMonth = olTmpTimeTo.GetMonth();
				dmyDay = olTmpTimeTo.GetDay();
				dmyHour = olTmpTimeTo.GetHour();
				dmyMinute = olTmpTimeTo.GetMinute();
				dmySecond = olTmpTimeTo.GetSecond();

				olVTO.Format("%04d%02d%02d%02d%02d%02d",dmyYear ,dmyMonth,dmyDay,dmyHour,dmyMinute,dmySecond);		
			}
		}
		else
		{
			olVTO="21990101235900";
		}
		
		
		if (olTmpTimeTo != -1 && olTmpTimeFr != -1 && olTmpTimeTo < olTmpTimeFr)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING61461);
		}
	}


	//*** CHECK THE GRID WITH DELEGATIONS ***
	long iRows=omGrdDel->GetRowCount();

	for(int i = 1; i <= iRows; i++)
	{
		CString clDpt1=omGrdDel->GetValueRowCol(i,3);
		CString clFctc=omGrdDel->GetValueRowCol(i,4);
		CString clSdac=omGrdDel->GetValueRowCol(i,5);
		CString clVon =omGrdDel->GetValueRowCol(i,6);
		CString clBis =omGrdDel->GetValueRowCol(i,7);
		bool ilIntStatus = true;
		if((clFctc.IsEmpty() && !clDpt1.IsEmpty()) || (!clFctc.IsEmpty() && clDpt1.IsEmpty()) && clSdac.IsEmpty())
		{
			ilIntStatus = false;
		}
		if(clFctc.IsEmpty() && clDpt1.IsEmpty() && clSdac.IsEmpty())
		{
			ilIntStatus = false;
		}
		if(clVon.IsEmpty() || clBis.IsEmpty())
		{
			ilIntStatus = false;
		}
		if(!ilIntStatus)
		{
			ilStatus = false;
			olText.Format(LoadStg(IDS_STRING61458),i);
			olErrorText += olText + CString("\n");
		}
		if((!clFctc.IsEmpty() || !clDpt1.IsEmpty())  && !clSdac.IsEmpty())
		{
			ilStatus = false;
			olText.Format(LoadStg(IDS_STRING61460),i);
			olErrorText += olText + CString("\n");
		}
	}

//#endif


	//--------------------------------------------------------------------------------------
	// Pr�ft ob die Pausen in der Schicht liegen und die Pausenzeit in die Pausenlage past
	if(ilStatus)
	{
		CTime olEsbg, olLsen, olBkd1, olBkf1, olBkt1;
		CString olText, olStrEsbg, olStrLsen, olStrBkd1, olStrBkf1, olStrBkt1;
		
		m_ESBG.GetWindowText(olText); 
		olEsbg = CTime(1999,1,1,atoi(olText.Left(2)),atoi(olText.Right(2)),0);
		
		m_LSEN.GetWindowText(olText); 
		olLsen = CTime(1999,1,1,atoi(olText.Left(2)),atoi(olText.Right(2)),0);

		m_BKD1.GetWindowText(olText); 
		olBkd1 = CTime(1999,1,1,atoi(olText.Left(2)),atoi(olText.Right(2)),0);
		olStrBkd1 = olBkd1.Format("%H%M");

		m_BKF1.GetWindowText(olText); 
		olStrBkf1 = olText;
		olBkf1 = CTime(1999,1,1,atoi(olText.Left(2)),atoi(olText.Right(2)),0);

		m_BKT1.GetWindowText(olText); 
		olStrBkt1 = olText;
		olBkt1 = CTime(1999,1,1,atoi(olText.Left(2)),atoi(olText.Right(2)),0);


		if(olEsbg>olLsen)
		{
			olLsen += CTimeSpan(1,0,0,0);
		}
		if(olEsbg>olBkf1)
		{
			olBkf1 += CTimeSpan(1,0,0,0);
		}
		if(olEsbg>olBkt1)
		{
			olBkt1 += CTimeSpan(1,0,0,0);
		}
		
		if((olBkf1 > olLsen || olBkt1 > olLsen || olBkf1 > olBkt1) && (olStrBkd1 != "0000" || (!olStrBkf1.IsEmpty() || !olStrBkt1.IsEmpty())))
		{
			// Pausenlage ist ung�ltig
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING61444) + CString("\n");
		}

		CTimeSpan olGroessePausenlageT = olBkt1 - olBkf1;
		CString  olGroessePausenlage   = olGroessePausenlageT.Format("%D%H%M");
		CString  olGroessePausenlaenge = olBkd1.Format("%H%M");
		if(atoi(olGroessePausenlaenge) > atoi(olGroessePausenlage))
		{
			// Pausenl�nge ist gr��er als Pausenlage
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING61445) + CString("\n");
		}
	}
	//---------------------------------------------------------------------------

	if(ilStatus && m_DYN.GetCheck() == 1)
	{
		CString olSdu,olSex,olSsh;
		int ilSdu,ilSex,ilSsh;
		m_SDU1.GetWindowText(olSdu);
		m_SEX1.GetWindowText(olSex);
		m_SSH1.GetWindowText(olSsh);
		CTime olEsbg,olLsen;
		CString olText;
		
		m_ESBG.GetWindowText(olText); 
		olEsbg = CTime(1999,1,1,atoi(olText.Left(2)),atoi(olText.Right(2)),0);
		
		m_LSEN.GetWindowText(olText); 
		olText = olText.Left(2) + olText.Right(2);
		olLsen = CTime(1999,1,1,atoi(olText.Left(2)),atoi(olText.Right(2)),0);
		if(olEsbg>olLsen)
		{
			olLsen += CTimeSpan(1,0,0,0);
		}

		olSdu = Gui2DBmin(olSdu);
		ilSdu = atoi(olSdu);
//		ilSdu = ilSdu * 60;
		CTimeSpan olSpan = olLsen-olEsbg;

		if(ilSdu<=0 || ilSdu>olSpan.GetTotalMinutes())
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING61556);
		}

		olSex = Gui2DBmin(olSex);
		ilSex = atoi(olSex);
//		ilSex = ilSex * 60;
		if(ilSex<0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING61557);
		}

		olSsh = Gui2DBmin(olSsh);
		ilSsh = atoi(olSsh);
//		ilSsh = ilSsh * 60;
		if(ilSsh<0 || (ilSsh>ilSdu && ilSdu>0))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING61558);
		}
	}



	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olBsdc;
		char clWhere[100];

		//*** 09.11.99 SHA ***
		//*** CHECK IF FUNCTION-CODE IS VALID ***
		if(m_FCTC.GetWindowTextLength() != 0) 
		{
			char clWhere[100];
			CCSPtrArray<PFCDATA> olPfcList;
			CString olFctc;
			m_FCTC.GetWindowText(olFctc);
			sprintf(clWhere,"WHERE FCTC='%s'",olFctc);
			if(ogPfcData.ReadSpecialData(&olPfcList, clWhere, "FCTC", false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING61462);
			}
			olPfcList.DeleteAll();
		}

		//*** CHECK IF ORG-CODE IS VALID ***
		if(m_DPT1.GetWindowTextLength() != 0) 
		{
			char clWhere[100];
			CCSPtrArray<ORGDATA> olOrgList;
			CString olDpt1;
			m_DPT1.GetWindowText(olDpt1);
			sprintf(clWhere,"WHERE DPT1='%s'",olDpt1);
			if(ogOrgData.ReadSpecialData(&olOrgList, clWhere, "DPT1", false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING61447);
			}
			olOrgList.DeleteAll();
		}

		CCSPtrArray<BSDDATA> olBsdList;
		m_BSDC.GetWindowText(olBsdc);
		olNewCode = olBsdc;
		// * OLD VERSION * sprintf(clWhere,"WHERE BSDC='%s'",olBsdc);
		
		// uhi 13.6.01
		sprintf(clWhere,"WHERE BSDC='%s' AND VAFR<='%s' AND (VATO >= '%s' OR VATO = ' ')", olBsdc, olVTO, olVFROM);

		if(ogBsdData.ReadSpecialData(&olBsdList,clWhere,"URNO,BSDC",false) == true)
		{
			if(olBsdList[0].Urno != pomBsd->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING61448) + LoadStg(IDS_STRING61449);
			}
		}
		olBsdList.DeleteAll();

		sprintf(clWhere,"WHERE BSDC='%s' AND VAFR='%s'", olBsdc, olVFROM);

		if(ogBsdData.ReadSpecialData(&olBsdList,clWhere,"URNO,BSDC",false) == true)
		{
			if(olBsdList[0].Urno != pomBsd->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING61448) + LoadStg(IDS_STRING61449);
			}
		}

		olBsdList.DeleteAll();
		
		CCSPtrArray<ODADATA> olOdaCPA;
		sprintf(clWhere,"WHERE SDAC='%s'",olBsdc);
		if(ogOdaData.ReadSpecialData(&olOdaCPA,clWhere,"URNO,SDAC",false) == true)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING61448) + LoadStg(IDS_STRING61450);
		}
		olOdaCPA.DeleteAll();

		if (m_CTRC1.GetWindowTextLength() != 0) 
		{
			char clWhere[100];
			CCSPtrArray<COTDATA> olCotList;
			CString olCtrc;m_CTRC1.GetWindowText(olCtrc);
			sprintf(clWhere,"WHERE CTRC='%s'",olCtrc);
			if(ogCotData.ReadSpecialData(&olCotList, clWhere, "CTRC", false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING61451);
			}
			olCotList.DeleteAll();
		}


		long iRows=omGrdDel->GetRowCount();
		for(int i = 1; i <= iRows; i++)
		{
			CString clDpt1=omGrdDel->GetValueRowCol(i,3);
			clDpt1.MakeUpper();
			clDpt1 = "|"+clDpt1+"|";
			CString clFctc=omGrdDel->GetValueRowCol(i,4);	
			clFctc.MakeUpper();
			clFctc = "|"+clFctc+"|";
			CString clSdac=omGrdDel->GetValueRowCol(i,5);
			clSdac.MakeUpper();
			clSdac = "|"+clSdac+"|";
			int n  =	omAllFctc.Find(clFctc);
			int j  =	omAllDpt1.Find(clDpt1);
			int k  =	omAllSdac.Find(clSdac);
			if(n==-1)
			{
				ilStatus = false;
				olText.Format(LoadStg(IDS_STRING61452),i);
				olErrorText += olText + CString("\n");
			}
			if(j==-1)
			{
				ilStatus = false;
				olText.Format(LoadStg(IDS_STRING61453),i);
				olErrorText += olText + CString("\n");
			}
			if(k==-1)
			{
				ilStatus = false;
				olText.Format(LoadStg(IDS_STRING61454),i);
				olErrorText += olText + CString("\n");
			}
		}
		//---------------------------------------------------------------------------
		// Pr�ft ob die Abordnungen und Abwesenheiten in der Schicht liegen
		for(i = 1; i <= iRows; i++)
		{
			CTime olEsbg, olLsen, olDelFr, olDelTo;
			CString olText;
			
			m_ESBG.GetWindowText(olText); 
			olEsbg = CTime(1999,1,1,atoi(olText.Left(2)),atoi(olText.Right(2)),0);
			
			m_LSEN.GetWindowText(olText); 
			//olText = olText.Left(2) + olText.Right(2);
			olLsen = CTime(1999,1,1,atoi(olText.Left(2)),atoi(olText.Right(2)),0);
			olText = omGrdDel->GetValueRowCol(i,6);
			olDelFr = CTime(1999,1,1,atoi(olText.Left(2)),atoi(olText.Right(2)),0);
			olText = omGrdDel->GetValueRowCol(i,7);
			olDelTo = CTime(1999,1,1,atoi(olText.Left(2)),atoi(olText.Right(2)),0);

			if(olEsbg>olLsen)
			{
				olLsen += CTimeSpan(1,0,0,0);
			}
			if(olEsbg>olDelFr)
			{
				olDelFr += CTimeSpan(1,0,0,0);
			}
			if(olEsbg>olDelTo)
			{
				olDelTo += CTimeSpan(1,0,0,0);
			}

			if(olDelFr > olLsen || olDelTo > olLsen || olDelFr > olDelTo)
			{
				ilStatus = false;
				olText.Format(LoadStg(IDS_STRING61455),i);
				olErrorText += olText + CString("\n");
			}
		}
		//---------------------------------------------------------------------------

	}
    //////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if(ilStatus == true)
	{
		bool blChangeCode = true;
		if(olNewCode != omOldCode && bmChangeAction)
		{
			// Referenz Check
			CheckReferenz olCheckReferenz;
			if(pomStatus != NULL) pomStatus->SetWindowText(LoadStg(IDS_STRING61550));
			int ilCount = olCheckReferenz.Check(_BSD, pomBsd->Urno, omOldCode);
			if(pomStatus != NULL) pomStatus->SetWindowText(LoadStg(IDS_STRING61551));
			CString olTxt;
			if(ilCount>0)
			{
				blChangeCode = false;
				if (!ogBasicData.BackDoorEnabled())
				{
					olTxt.Format("%d %s",ilCount, LoadStg(IDS_STRING61552));
					MessageBox(olTxt,LoadStg(IDS_STRING61553),MB_ICONERROR);
				}
				else
				{
					olTxt.Format("%d %s",ilCount, LoadStg(IDS_STRING61554));
					if(IDYES == MessageBox(olTxt,LoadStg(IDS_STRING61555),(MB_ICONSTOP | MB_YESNOCANCEL | MB_DEFBUTTON2)))
					{
						blChangeCode = true;
					}
				}
			}
			// END Referenz Check
		}

		if(blChangeCode)
		{
			CString olText;
			if (m_JUMPER.GetCheck() == 1)
				strcpy(pomBsd->Bewc,"J");
			else
				strcpy(pomBsd->Bewc," ");

			m_BKD1.GetWindowText(olText);

			oltempBKD1=olText;

			olText = Gui2DBmin(olText);
//			int ilTmp = atoi(olText);
//			ilTmp = ilTmp * 60;
//			olText.Format("%d",ilTmp);
			
			strcpy(pomBsd->Bkd1, olText);
			m_BKF1.GetWindowText(olText); 
			olText = olText.Left(2) + olText.Right(2);
			strcpy(pomBsd->Bkf1, olText);
			m_BKT1.GetWindowText(olText); 
			olText = olText.Left(2) + olText.Right(2);
			strcpy(pomBsd->Bkt1, olText);
			m_BSDC.GetWindowText(olText); strcpy(pomBsd->Bsdc, olText);
			m_BSDK.GetWindowText(olText); strcpy(pomBsd->Bsdk, olText);
			m_BSDN.GetWindowText(olText); strcpy(pomBsd->Bsdn, olText);
			m_BSDS.GetWindowText(olText); strcpy(pomBsd->Bsds, olText);
			m_CTRC1.GetWindowText(olText); strcpy(pomBsd->Ctrc, olText);
			m_ESBG.GetWindowText(olText); 
			
			oltempESBG=olText;

			olText = olText.Left(2) + olText.Right(2);
			strcpy(pomBsd->Esbg, olText);
			m_LSEN.GetWindowText(olText); 

			oltempLSEN=olText;

			olText = olText.Left(2) + olText.Right(2);
			strcpy(pomBsd->Lsen, olText);

			m_SDU1.GetWindowText(olText); 
			olText = Gui2DBmin(olText);

			//NEU: AUTOMATISMUS AUSRECHNEN !
			oltempSDU1 = GetRegS(oltempESBG,oltempLSEN,oltempBKD1);
			m_SDU1.SetWindowText(oltempSDU1);
			m_SDU1.GetStatus();
			olText = Gui2DBmin(oltempSDU1);
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++
			

//			ilTmp = atoi(olText);
//			ilTmp = ilTmp * 60;
//			olText.Format("%d",ilTmp);
			strcpy(pomBsd->Sdu1, olText);

			//			m_SEX1.GetWindowText(olText); 
//			olText = Gui2DBmin(olText);
//			ilTmp = atoi(olText);
//			ilTmp = ilTmp * 60;
//			olText.Format("%d",ilTmp);
//			strcpy(pomBsd->Sex1, olText);
//			m_SSH1.GetWindowText(olText); 
//			olText = Gui2DBmin(olText);
//			ilTmp = atoi(olText);
//			ilTmp = ilTmp * 60;
//			olText.Format("%d",ilTmp);
			
//			strcpy(pomBsd->Ssh1, olText);

			//*** 09.11.99 SHA ***
			m_DPT1.GetWindowText(olText); strcpy(pomBsd->Dpt1, olText);
			m_FCTC.GetWindowText(olText); strcpy(pomBsd->Fctc, olText);
			m_RGSB.GetWindowText(olText); 
			olText = olText.Left(2) + olText.Right(2);
			strcpy(pomBsd->Rgsb, olText);

			pomBsd->Vafr = DateHourStringToDate(olVafrd,olVafrt);
			pomBsd->Vato = DateHourStringToDate(olVatod,olVatot);

			sprintf(pomBsd->Rgbc,"%06x",this->omRgbc);

			//*** SAVE THE GRID WITH DELEGATIONS ***
			CString clDmy;
			long iRows=omGrdDel->GetRowCount();
			ogBCD.SetObject("DEL");

			if (iRows>0) 
				strcpy(pomBsd->Fdel, CString("x"));
			else
				strcpy(pomBsd->Fdel, CString(" "));

			for(i = 1; i <= iRows; i++)
			{
				CString clUrno=omGrdDel->GetValueRowCol(i,1);
				CString clBsdu=omGrdDel->GetValueRowCol(i,2);

//				omGrdDel->SetValueRange(CGXRange(i,2),pomBsd->Urno);
//				clBsdu=omGrdDel->GetValueRowCol(i,2);

				CString clDpt1=omGrdDel->GetValueRowCol(i,3);
				clDpt1.MakeUpper();
				CString clFctc=omGrdDel->GetValueRowCol(i,4);
				clFctc.MakeUpper();
				CString clSdac=omGrdDel->GetValueRowCol(i,5);
				clSdac.MakeUpper();

				CString clVon=omGrdDel->GetValueRowCol(i,6);
				CString clBis=omGrdDel->GetValueRowCol(i,7);
				CString clStatus=omGrdDel->GetValueRowCol(i,8);

				if (clStatus=="NEW" || clStatus=="UPD")
				{
					if (clStatus=="UPD")
					{
						clDmy.Format("WHERE URNO=%s",clUrno);
						ogBCD.DeleteByWhere("DEL",clDmy);
					}
					RecordSet olRecord(ogBCD.GetFieldCount("DEL"));
					olRecord.Values[ogBCD.GetFieldIndex("DEL","URNO")]=clUrno;
					olRecord.Values[ogBCD.GetFieldIndex("DEL","BSDU")]=clBsdu;
					olRecord.Values[ogBCD.GetFieldIndex("DEL","FCTC")]=clFctc;
					olRecord.Values[ogBCD.GetFieldIndex("DEL","DPT1")]=clDpt1;
					olRecord.Values[ogBCD.GetFieldIndex("DEL","SDAC")]=clSdac;
					olRecord.Values[ogBCD.GetFieldIndex("DEL","DELF")]=clVon;
					olRecord.Values[ogBCD.GetFieldIndex("DEL","DELT")]=clBis;
					ogBCD.InsertRecord("DEL",olRecord,FALSE);
				}
			}
			ogBCD.Save("DEL");

			//*** DELETE THE SELECTED DELEGATIONS ***
			for (i=0; i<olDeleteUrno.GetSize(); i++)
			{
				clDmy.Format("WHERE URNO=%s",olDeleteUrno[i]);
				ogBCD.DeleteByWhere("DEL",clDmy);
			}

			if(bmIsDynamic == true)
			{
				strcpy(pomBsd->Type, "D");//Dynamic
			}
			else
			{
				strcpy(pomBsd->Type, "S");//Static
			}
			if(m_BKR1_A.GetCheck() == 1)
			{
				strcpy(pomBsd->Bkr1, "A");
			}
			else
			{
				strcpy(pomBsd->Bkr1, "S");
			}
			if(m_DYN.GetCheck() == 1)
			{
				strcpy(pomBsd->Type, "D");
			}
			if(m_STAT.GetCheck() == 1)
			{
				strcpy(pomBsd->Type, "S");
			}
			if(m_EXTRA.GetCheck() == 1)
			{
				strcpy(pomBsd->Type, "E");
			}
			//wandelt Return in Blank//
			CString olTemp;
			m_REMA.GetWindowText(olTemp);
			for(int i=0;i>-1;)
			{
				i = olTemp.Find("\r\n");
				if(i != -1)
				{
					if(olTemp.GetLength() > (i+2))
						olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
					else
						olTemp = olTemp.Left(i);
				}
			}
			strcpy(pomBsd->Rema,olTemp);
			////////////////////////////

			CDialog::OnOK();
		}
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING61553),MB_ICONERROR);
		m_BSDC.SetFocus();
		m_BSDC.SetSel(0,-1);
	}
}
//----------------------------------------------------------------------------------------
LONG BasisschichtenDlg::OnKillfocus(UINT lParam, LONG wParam) 
{
	TRACE("BasisschichtenDlg::OnKillfocus(UINT lParam, LONG wParam)\n");
	if(lParam == (UINT)m_BKD1.imID) 
	{
		CString olText;
		m_BKD1.GetWindowText(olText);
		if(olText.IsEmpty() || olText == "0")
		{
			m_BKD1.SetWindowText("00:00");
			m_BKD1.GetStatus();
		}
	}
	if(lParam == (UINT)m_ESBG.imID || lParam == (UINT)m_LSEN.imID || lParam == (UINT)m_BKD1.imID) 
	{
		CString olText,oltempESBG,oltempLSEN,oltempBKD1,oltempSDU1;

		m_ESBG.GetWindowText(oltempESBG);
		m_LSEN.GetWindowText(oltempLSEN);
		m_BKD1.GetWindowText(oltempBKD1);

		//NEU: AUTOMATISMUS AUSRECHNEN !
		oltempSDU1 = GetRegS(oltempESBG,oltempLSEN,oltempBKD1);
		m_SDU1.SetWindowText(oltempSDU1);
		m_SDU1.GetStatus();
	}
	else if (lParam == (UINT)m_FCTC.imID)
	{
		CString olText;
		m_FCTC.GetWindowText(olText);
		if (olText.IsEmpty())
		{
			m_DPT1.SetInitText("");
		}
		else
		{
			PFCDATA *prlPfc = ogPfcData.GetPfcByFctc(olText);
			if (prlPfc != NULL)
			{
				m_DPT1.SetInitText(prlPfc->Dptc);
			}
			else
			{
				m_DPT1.SetInitText("");
			}
		}
	}
	return 0L;
}
//----------------------------------------------------------------------------------------
void BasisschichtenDlg::OnBFctc() 
{
	TRACE("BasisschichtenDlg::OnBFctc()\n");
	//*** 09.11.99 SHA ***

	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<PFCDATA> olList;
	AfxGetApp()->DoWaitCursor(1);
	AfxGetApp()->DoWaitCursor(1);
	//uhi 13.3.01    
	CString olOrderBy = "ORDER BY FCTC";
	if(ogPfcData.ReadSpecialData(&olList, olOrderBy.GetBuffer(0), "URNO,FCTC,FCTN", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Fctc));
			olCol2.Add(CString(olList[i].Fctn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING61421), LoadStg(IDS_STRING61422), this);


		if(pomDlg->DoModal() == IDOK)
		{
			m_FCTC.SetInitText(pomDlg->omReturnString);		
			if (pomDlg->omReturnString.GetLength() > 0)
			{
				PFCDATA *prlPfc = ogPfcData.GetPfcByFctc(pomDlg->omReturnString);
				if (prlPfc != NULL)
				{
					m_DPT1.SetInitText(prlPfc->Dptc);
				}
				else
				{
					m_DPT1.SetInitText("");
				}
			}
			else
			{
				m_DPT1.SetInitText("");
			}
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING61423), LoadStg(IDS_STRING61424), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
	

}
//---------------------------------------------------------------------------
void BasisschichtenDlg::OnBDpt1() 
{
	TRACE("BasisschichtenDlg::OnBDpt1()\n");
    // no need this button anymore, so do nothing

	//*** 09.11.99 SHA ***

//	CStringArray olCol1, olCol2, olCol3;
//	CCSPtrArray<ORGDATA> olList;
//	AfxGetApp()->DoWaitCursor(1);
//	AfxGetApp()->DoWaitCursor(1);
//	//uhi 13.3.01    
//	CString olOrderBy = "ORDER BY DPT1";
//	if(ogOrgData.ReadSpecialData(&olList, olOrderBy.GetBuffer(0), "URNO,DPT1,DPTN", false) == true)
//	{
//		AfxGetApp()->DoWaitCursor(-1);
//		for(int i = 0; i < olList.GetSize(); i++)
//		{
//			char pclUrno[20]="";
//			sprintf(pclUrno, "%ld", olList[i].Urno);
//			olCol3.Add(CString(pclUrno));
//			olCol1.Add(CString(olList[i].Dpt1));
//			olCol2.Add(CString(olList[i].Dptn));
//		}
//		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
//		if(pomDlg->DoModal() == IDOK)
//		{
//			m_DPT1.SetInitText(pomDlg->omReturnString);		
//		}
//		delete pomDlg;
//	}
//	else
//	{
//		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
//	}
//	olList.DeleteAll();
//
//	AfxGetApp()->DoWaitCursor(-1);

}
//---------------------------------------------------------------------------
void BasisschichtenDlg::OnBdelete() 
{
	TRACE("BasisschichtenDlg::OnBdelete()\n");
	//*** 10.11.99 SHA ***
	//*** DELETE A DELEGATION FROM SCREEN, DB DELETE ONOK() ***
	ROWCOL iCol,iRow;
	omGrdDel->GetCurrentCell(iRow,iCol);

	if (iRow>0)
	{
		//afxDump << "iRow:" << iRow <<"\n"; //ausk. von ARE
		CString clUrno=omGrdDel->GetValueRowCol(iRow,1);

		olDeleteUrno.Add(clUrno);

		omGrdDel->RemoveRows(iRow,iRow);

		omGrdDel->SetCurrentCell(0,0);
	}
			
}
//---------------------------------------------------------------------------
void BasisschichtenDlg::OnBneu() 
{
	TRACE("BasisschichtenDlg::OnBneu()\n");
	//*** 10.11.99 SHA ***
	//*** CREATE NEW DELEGATION, SAVE ONOK() ***


	long iRows=omGrdDel->GetRowCount()+1;
	omGrdDel->InsertRows(iRows,1);

	omGrdDel->SetValueRange(CGXRange(iRows,2),pomBsd->Urno);
	omGrdDel->SetValueRange(CGXRange(iRows,1),ogBasicData.GetNextUrno());
	omGrdDel->SetValueRange(CGXRange(iRows,8),"NEW");


	omGrdDel->SetStyleRange(CGXRange(1,6,iRows,6),
		  CGXStyle()
		  .SetControl(GX_IDS_CTRL_MASKEDIT)
		  .SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("##:##")));
	omGrdDel->SetStyleRange(CGXRange(1,7,iRows,7),
		  CGXStyle()
		  .SetControl(GX_IDS_CTRL_MASKEDIT)
		  .SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("##:##")));

	omGrdDel->SetCurrentCell(iRows,3,GX_DISPLAYEDITWND|GX_UPDATENOW|GX_SCROLLINVIEW);
}

//---------------------------------------------------------------------------
void BasisschichtenDlg::OnRgbc() 
{
	TRACE("BasisschichtenDlg::OnRgbc()\n");
	CColorDialog olDlg(this->omRgbc,0,this);
	if (olDlg.DoModal() == IDOK)
	{
		this->omRgbc = olDlg.GetColor();
		this->Invalidate();
	}
}

//---------------------------------------------------------------------------
HBRUSH BasisschichtenDlg::OnCtlColor( CDC* pDC, CWnd* pWnd, UINT nCtlColor )
{
	CWnd *polWnd;// = GetDlgItem(IDC_ADD_STANDARD);
	polWnd = GetDlgItem(IDC_EDIT_RGBC);
	if (polWnd && (polWnd->m_hWnd == pWnd->m_hWnd))
	{
		pDC->SetBkColor(this->omRgbc);		
		pDC->SetTextColor(this->omRgbc);

		omBrush.DeleteObject();
		omBrush.CreateSolidBrush(this->omRgbc);

		HBRUSH hr = omBrush;	
		return hr;

	}

	return CDialog::OnCtlColor(pDC,pWnd,nCtlColor);
}


//---------------------------------------------------------------------------
// Umwandlung von 180 min aus DB 
// immer ins Format 03:00 (hh:mm) der Gui
CString BasisschichtenDlg::DBmin2Gui(CString dbmin)
{
	int		ilTmp = 0;
	div_t	divTmp;   
	CString slOut = "";
	

	if (!dbmin.IsEmpty())
	{
		ilTmp = atoi(dbmin);
		divTmp = div( ilTmp, 60 );
		if (divTmp.quot<10)
		{
			if (divTmp.rem<10)
			{
				slOut.Format("0%d:0%d",divTmp.quot,divTmp.rem);
			}
			else
			{
				slOut.Format("0%d:%d",divTmp.quot,divTmp.rem);
			}
		}
		else
		{
			if (divTmp.rem<10)
			{
				slOut.Format("%d:0%d",divTmp.quot,divTmp.rem);
			}
			else
			{
				slOut.Format("%d:%d",divTmp.quot,divTmp.rem);
			}
		}
	}
	else
	{
		slOut ="00:00";
	}

	return slOut;
}
//---------------------------------------------------------------------------
// Umwandlung von 03:00 / 3:00 / 0300 aus der Gui 
// immer ins Format 180 min der DB
CString BasisschichtenDlg::Gui2DBmin(CString guimin)
{
	CString slOut	= "";
	int ilPos		= 0;
	int ilHour		= 0;
	int ilMin		= 0;
	
	ilPos = guimin.Find(":");
	if (ilPos > 0)
	{	// Format hh:mm oder h:mm
		slOut  = guimin.Left(ilPos);	
		ilHour = atoi(slOut);
		ilHour = ilHour * 60;
		slOut  = guimin.Right(guimin.GetLength()-(ilPos+1));
		ilMin  = atoi(slOut);
		ilMin  = ilHour + ilMin;
//		slOut.Format("%d",ilMin);
	}
	else
	{	// Format hhmm
		slOut  = guimin.Left(2);	
		ilHour = atoi(slOut);
		ilHour = ilHour * 60;
		slOut  = guimin.Right(2);	
		ilMin  = ilHour + ilMin;	
	}
	slOut.Format("%d",ilMin);

	return slOut;
}
//---------------------------------------------------------------------------
//Ermittlung der regulaeren Schichtdauer aus anf,ende und pause
//SHA 2001-01-15
CString BasisschichtenDlg::GetRegS(CString cVon, CString cBis, CString cPause)
{

	CTimeSpan ts;
	CTimeSpan Pause(0,atoi(cPause.Left(2)),atoi(cPause.Right(2)),0);
	int vonH;
	int vonM;
	int bisH;
	int bisM;

	vonH=atoi(cVon.Left(2));
	vonM=atoi(cVon.Right(2));
	bisH=atoi(cBis.Left(2));
	bisM=atoi(cBis.Right(2));

	
	if (cVon<cBis)
	{
		//innerhalb eines Tages
		CTime t1( 2000, 1, 1, vonH, vonM, 0 ); 
		CTime t2( 2000, 1, 1, bisH, bisM, 0 ); 
		ts = t2 - t1;
	}
	else
	{
		//�ber tagesgrenze
		CTime t1( 2000, 1, 1, vonH, vonM, 0 ); 
		CTime t2( 2000, 1, 2, bisH, bisM, 0 ); 
		ts = t2 - t1;

	}

	ts=ts-Pause;
	long x=ts.GetTotalMinutes();
	long y=ts.GetHours();
	long yy=ts.GetMinutes();

	CString ret;
	if (x==0)
		ret="";
	else
		ret.Format("%02d:%02d",y,yy);

	return ret;
}
//---------------------------------------------------------------------------

void BasisschichtenDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}
