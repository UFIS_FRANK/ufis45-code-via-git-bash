// CedaSpfData.h

#ifndef __CEDAPSPFDATA__
#define __CEDAPSPFDATA__
 
#include <stdafx.h>
#include <basicdata.h>
#include <afxdisp.h>

//---------------------------------------------------------------------------------------------------------

struct SPFDATA 
{
	long			Urno;
	long			Surn;
	int 			Prio;
	char			Code[7];
	COleDateTime	Vpfr;
	COleDateTime	Vpto;

	//DataCreated by this class
	int      IsChanged; 

	//long, CTime
	SPFDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Vpfr.SetStatus(COleDateTime::invalid);
		Vpto.SetStatus(COleDateTime::invalid);
	}

}; // end SPFDataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaSpfData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<SPFDATA> omData;

	char pcmListOfFields[2048];
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

// OSpfations
public:
    CedaSpfData();
	~CedaSpfData();
//	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(SPFDATA *prpSpf);
	bool InsertInternal(SPFDATA *prpSpf);
	bool Update(SPFDATA *prpSpf);
	bool UpdateInternal(SPFDATA *prpSpf);
	bool Delete(long lpUrno);
	bool DeleteInternal(SPFDATA *prpSpf);
	bool ReadSpecialData(CCSPtrArray<SPFDATA> *popSpf,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(SPFDATA *prpSpf);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	SPFDATA *GetSpfByUrno(long lpUrno);
	void GetSpfBySurnWithTime(long lpSurn,CTime opStart,CTime opEnd,CCSPtrArray<SPFDATA> *popSpfData);
	CString GetPfcBySurnWithTime(long lpSurn, COleDateTime opDate);
	CString GetSpfByPfcWithTime(CStringArray *popPfc,COleDateTime opStart,COleDateTime opEnd,CCSPtrArray<SPFDATA> *popSpfData);

	// Private methods
private:
    void PrepareSpfData(SPFDATA *prpSpfData);

};

//---------------------------------------------------------------------------------------------------------

extern CedaSpfData ogSpfData;

#endif //__CEDAPSPFDATA__
