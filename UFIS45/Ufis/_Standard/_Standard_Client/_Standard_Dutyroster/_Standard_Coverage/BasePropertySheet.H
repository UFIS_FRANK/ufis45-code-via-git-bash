#ifndef __BASEPROPERTYSHEET_H__
#define __BASEPROPERTYSHEET_H__

// BasePropertySheet.h : header file
//
#define ID_BUTTON_SAVE		"Speichern"
#define ID_BUTTON_DELETE	"L�schen"
#define ID_BUTTON_APPLY		"Anwenden"

#include <CViewer.h>
/////////////////////////////////////////////////////////////////////////////
// BasePropertySheet

//@Man:
//@Memo: Provides the basic functionality of filter property sheets
/*@Doc:
  This is the base class for all filter property sheets.
  It handles Saving, deleting and applying of the selected filter
   
*/

class BasePropertySheet : public CPropertySheet
{
	DECLARE_DYNAMIC(BasePropertySheet)

// Construction
public:
	BasePropertySheet(LPCSTR pszCaption, CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
protected:
	CWnd *pomParentWnd;	// require for PropertySheet protection logic

	CViewer *pomViewer;
	CStatic omSeparator;
	static int imCount;

// Operations
public:
    //@ManMemo:Handles the loading of the filter data into the property sheet  
    /*@Doc:
		Empty virtuell function is overloaded in the child class
    */
	virtual void LoadDataFromViewer();
    //@ManMemo:Handles the saving of the filter data from the property sheet  
    /*@Doc:
		Empty virtuell function is overloaded in the child class
    */
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
    //@ManMemo:Empty function without use  
    /*@Doc:
		return IDOK (allways)
    */
	virtual int QueryForDiscardChanges();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BasePropertySheet)
	public:
	virtual int DoModal();
	virtual BOOL OnInitDialog();
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~BasePropertySheet();
	char pcmCaption[100];

	// Generated message map functions
protected:
	//{{AFX_MSG(BasePropertySheet)
	afx_msg void OnViewSelChange();
	afx_msg void OnSave();
	afx_msg void OnDelete();
	afx_msg void OnOK();
	afx_msg void OnCancel();
	afx_msg void OnApply();
	afx_msg void OnComboKillFocus();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Help routines
protected:
	//@ManMemo:Updates the Combobox in the property sheet
    /*@Doc:
		Clears the combobox and refills it with the filter names.
    */
	void UpdateComboBox();
	//@ManMemo:Gets the selected text in the Combobox
    /*@Doc:
		return olViewName 
    */
	CString GetComboBoxText();
	BOOL IsIdentical(CStringArray &ropArray1, CStringArray &ropArray2);
	BOOL IsInArray(CString &ropString, CStringArray &ropArray);
};

/////////////////////////////////////////////////////////////////////////////
#endif //__BASEPROPERTYSHEET_H__