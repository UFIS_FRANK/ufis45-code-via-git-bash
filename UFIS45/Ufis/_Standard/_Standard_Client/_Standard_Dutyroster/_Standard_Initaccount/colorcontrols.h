#if !defined(AFX_COLORCONTROLS_H__5529A6B1_584A_11D2_A41A_006097BD277B__INCLUDED_)
#define AFX_COLORCONTROLS_H__5529A6B1_584A_11D2_A41A_006097BD277B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


//***************************************************************************
/////////////////////////////////////////////////////////////////////////////
// ColorButton: header 
//
// Owner Draw
// 
/////////////////////////////////////////////////////////////////////////////

class CColorButton : public CButton
{
DECLARE_DYNAMIC(CColorButton)
public:
	CColorButton(); 
	virtual ~CColorButton(); 

	void ChangeColor(
		const COLORREF opBackgroundColor	= GetSysColor(COLOR_3DFACE),
		const COLORREF opTextColor			= GetSysColor(COLOR_BTNTEXT),
		const COLORREF opShadowColor		= GetSysColor(COLOR_3DSHADOW),
		const COLORREF opLightColor			= GetSysColor(COLOR_3DLIGHT),
		const COLORREF opDkShadowColor		= GetSysColor(COLOR_3DDKSHADOW),
		const COLORREF opHighlightColor		= GetSysColor(COLOR_3DHIGHLIGHT)
	);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CColorButton)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDIS);
	//}}AFX_VIRTUAL
	
protected:
	void DrawFrame(CDC *opDC, CRect opRect, bool bpBtUp, bool bpFocus);
	void DrawFilledRect(CDC *opDC, CRect opRect);
	void DrawLine(CDC *opDC, CRect EndPoints, COLORREF color);
	void DrawLine(CDC *opDC, long left, long top, long right, long bottom, COLORREF color);
	void DrawButtonText(CDC *opDC, CRect opRect, const char *Buf, bool bpBtUp, bool bpDisabled);

private:

	COLORREF omBackgroundColor;
	COLORREF omDisabledTextColor;
	COLORREF omTextColor;
	COLORREF omHighlightColor;
	COLORREF omLightColor;
	COLORREF omShadowColor;
	COLORREF omDkShadowColor;
	UINT imLevel;
	// Generated message map functions
protected:
	//{{AFX_MSG(CColorButton)
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};

//***************************************************************************

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLORCONTROLS_H__5529A6B1_584A_11D2_A41A_006097BD277B__INCLUDED_)
