#if !defined(AFX_OPENJOBCONTROLDIALOG_H__A4B86D7B_AF02_43C0_BA8F_558163D4EF4F__INCLUDED_)
#define AFX_OPENJOBCONTROLDIALOG_H__A4B86D7B_AF02_43C0_BA8F_558163D4EF4F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OpenJobControlDialog.h : Header-Datei
//

#define JOB_COLCOUNT			1
#define MAX_BC_SELECTION_SIZE	10000
#define MAX_BC_FIELDS_SIZE		10000
#define MAX_BC_DATA_SIZE		10000
#define MAX_BC_TW_SIZE			36
#define MAX_BC_ACTION_SIZE		12
#define MAX_BC_TABLE_SIZE		512

class CGridControl;

typedef struct MyBc
{
	char cReq_id[22];				
	char cUser[12];							// normally CEDA
	char cWks[12];							// workstation, here appl. name
	char cAction[MAX_BC_ACTION_SIZE];		// the command, e.g. GBD or MKB 
	char cTable[MAX_BC_TABLE_SIZE];			// table Name (multiple table are allowed)
	char cSortClause[512];					// sort Clause    
	char cTws[MAX_BC_TW_SIZE];				// time Window Start 
	char cTwe[MAX_BC_TW_SIZE];				// time Window end
	char cSelection[MAX_BC_SELECTION_SIZE];	// selection criterium, normally a WHERE clause
	char cFields[MAX_BC_FIELDS_SIZE];		// DB field, normally database fields
	char cData[MAX_BC_DATA_SIZE];			// response buffer 
	char cDataDest[12];						// RETURN or BUF1 .. BUF4
} MyBc;

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld COpenJobControlDialog 

class COpenJobControlDialog : public CDialog
{
// Konstruktion
public:
	COpenJobControlDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	~COpenJobControlDialog();	// standard destructor

// Dialogfelddaten
	//{{AFX_DATA(COpenJobControlDialog)
	enum { IDD = IDD_DIALOG_OPENJOBCONTROL };
	CButton	m_but_ViewInfo;
	CButton	m_but_Refresh;
	CButton	m_but_RemoveAll;
	CListCtrl	m_list_Results;
	CButton	m_but_OK;
	CButton	m_but_DeleteJob;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(COpenJobControlDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
// functions
	// load language specific string resources
	void LoadStringResources(void);
	// remove all entries from the grid
	void ClearGrid(void);
	// initialize the grid
	void InitOpenJobGrid(void);
	// fill the grid
	bool FillOpenJobGrid(bool bpClearMessageList = true);
	// disable or enable user buttons
	void EnableButtons(bool bpEnable);
	// send bc to handler
	bool SendBC(CString opAction, CString opData = "", CString opSelection = "", 
				CString opFields = "", CString opTable = "", CString opTwStart = "", 
				CString opTwEnd = "");
	// get the open job selection
	bool GetSelectedJobs(CStringArray *popSelectedJobs);

// data
	// the script grid
	CGridControl *pomOpenJobGrid;
	// no of jobs in queue
	int imNoOfOpenJobs;
	// trigger for reading the basic directory from server, only one time after initialization
	bool bmInitOk;
	// the broadcast data
	MyBc omBc;

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(COpenJobControlDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButtonRefresh();
	afx_msg void OnButtonRemoveAll();
	afx_msg void OnButtonDeleteJob();
	afx_msg void OnButtonViewInfo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_OPENJOBCONTROLDIALOG_H__A4B86D7B_AF02_43C0_BA8F_558163D4EF4F__INCLUDED_
