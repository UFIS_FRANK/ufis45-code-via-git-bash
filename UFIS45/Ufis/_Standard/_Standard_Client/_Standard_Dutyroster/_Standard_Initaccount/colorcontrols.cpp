/////////////////////////////////////////////////////////////////////////////
// ColorControls
// for MFC ListBox and Button
// Version: 1.0
// by: ARE
/////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <ColorControls.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//***************************************************************************
/////////////////////////////////////////////////////////////////////////////
// CColorButton: implementation
// MFC Button with optional color
// Version: 1.0
/////////////////////////////////////////////////////////////////////////////

// no automatic class substitution for this file!
#ifdef CColorButton
#undef CColorButton      CColorButton
#endif

// CColorButton
IMPLEMENT_DYNAMIC(CColorButton, CButton)

//---------------------------------------------------------------------------

CColorButton::CColorButton() 
{  
	omBackgroundColor	= GetSysColor(COLOR_3DFACE);
	omTextColor			= GetSysColor(COLOR_BTNTEXT);
	omHighlightColor	= GetSysColor(COLOR_3DHIGHLIGHT);
	omLightColor		= GetSysColor(COLOR_3DLIGHT);
	omDkShadowColor		= GetSysColor(COLOR_3DDKSHADOW);
	omShadowColor		= GetSysColor(COLOR_3DSHADOW);
} 

CColorButton::~CColorButton()
{
} 

BEGIN_MESSAGE_MAP(CColorButton, CButton)
	//{{AFX_MSG_MAP(CColorButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CColorButton message handlers
//---------------------------------------------------------------------------

void CColorButton::ChangeColor(	const COLORREF opBackgroundColor, const COLORREF opTextColor,
								const COLORREF opShadowColor, const COLORREF opLightColor,
								const COLORREF opDkShadowColor, const COLORREF opHighlightColor)
{
	omBackgroundColor	= opBackgroundColor; 
	omTextColor			= opTextColor; 
	omHighlightColor	= opHighlightColor; 
	omLightColor		= opLightColor; 
	omDkShadowColor		= opDkShadowColor; 
	omShadowColor		= opShadowColor; 
	InvalidateRect(NULL);
} 

//---------------------------------------------------------------------------

void CColorButton::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	CDC* pDC = CDC::FromHandle(lpDIS->hDC);

	UINT ilState = lpDIS->itemState; 
	CRect olFocusRect, olButtonRect;
	olFocusRect.CopyRect(&lpDIS->rcItem); 
	olButtonRect.CopyRect(&lpDIS->rcItem); 

	// Set the focus rectangle to just past the border decoration
	olFocusRect.left += 4;
    olFocusRect.right -= 4;
    olFocusRect.top += 4;
    olFocusRect.bottom -= 4;
      
	// Retrieve the button's caption
    const int bufSize = 512;
    TCHAR clBtText[bufSize];
    GetWindowText(clBtText, bufSize);
	bool blFocus = false;
	bool blDisabled = false;
	bool blBtUp = true;

	if(ilState & ODS_FOCUS)
		blFocus = true;

    DrawFilledRect(pDC, olButtonRect); 
    DrawFrame(pDC, olButtonRect, true, blFocus);

	if(blFocus)
	{
		DrawFocusRect(lpDIS->hDC, (LPRECT)&olFocusRect);
		if (ilState & ODS_SELECTED)
		{ 
    		DrawFilledRect(pDC, olButtonRect); 
    		DrawFrame(pDC, olButtonRect, false, blFocus);
			DrawFocusRect(lpDIS->hDC, (LPRECT)&olFocusRect);
			blBtUp = false;
		}
	}
	if(ilState & ODS_DISABLED)
		blDisabled = true;
	DrawButtonText(pDC, olButtonRect, clBtText, blBtUp, blDisabled);
} 

//---------------------------------------------------------------------------

void CColorButton::DrawFrame(CDC *opDC, CRect opRect, bool bpBtUp, bool bpFocus)
{ 
	COLORREF  olRBOColor, olLTOColor, olRBIColor, olLTIColor;
	if(bpFocus)
	{
		DrawLine(opDC, opRect.left, opRect.top, opRect.right, opRect.top, RGB(0, 0, 0));
		DrawLine(opDC, opRect.left, opRect.top, opRect.left, opRect.bottom, RGB(0, 0, 0));
		DrawLine(opDC, opRect.left + 1, opRect.bottom - 1, opRect.right, opRect.bottom - 1, RGB(0, 0, 0));
		DrawLine(opDC, opRect.right - 1, opRect.top + 1, opRect.right - 1, opRect.bottom, RGB(0, 0, 0));
		InflateRect(opRect, -1, -1);
	}
	if(bpBtUp)
	{
		olRBOColor = omDkShadowColor;
		olLTOColor = omHighlightColor;
		olRBIColor = omShadowColor;
		olLTIColor = omLightColor;
	}
	else
	{
		olRBOColor = omShadowColor;
		olLTOColor = omShadowColor;
		olRBIColor = omBackgroundColor;
		olLTIColor = omBackgroundColor;
	}
	DrawLine(opDC, opRect.left, opRect.top, opRect.right, opRect.top, olLTOColor);
	DrawLine(opDC, opRect.left, opRect.top, opRect.left, opRect.bottom, olLTOColor);
	DrawLine(opDC, opRect.left + 1, opRect.bottom - 1, opRect.right, opRect.bottom - 1, olRBOColor);
	DrawLine(opDC, opRect.right - 1, opRect.top + 1, opRect.right - 1, opRect.bottom, olRBOColor);
  	InflateRect(opRect, -1, -1);
	DrawLine(opDC, opRect.left, opRect.top, opRect.right, opRect.top, olLTIColor);
	DrawLine(opDC, opRect.left, opRect.top, opRect.left, opRect.bottom, olLTIColor);
	DrawLine(opDC, opRect.left + 1, opRect.bottom - 1, opRect.right, opRect.bottom - 1, olRBIColor);
	DrawLine(opDC, opRect.right - 1, opRect.top + 1, opRect.right - 1, opRect.bottom, olRBIColor);
}

//---------------------------------------------------------------------------

void CColorButton::DrawFilledRect(CDC *opDC, CRect opRect)
{ 
	CBrush olBkBrush;
	olBkBrush.CreateSolidBrush(omBackgroundColor);
	opDC->FillRect(opRect, &olBkBrush);
}

//---------------------------------------------------------------------------

void CColorButton::DrawLine(CDC *opDC, CRect EndPoints, COLORREF color)
{ 
	CPen newPen;
	newPen.CreatePen(PS_SOLID, 1, color);
	CPen *oldPen = opDC->SelectObject(&newPen);
	opDC->MoveTo(EndPoints.left, EndPoints.top);
	opDC->LineTo(EndPoints.right, EndPoints.bottom);
	opDC->SelectObject(oldPen);
    newPen.DeleteObject();
}

//---------------------------------------------------------------------------

void CColorButton::DrawLine(CDC *opDC, long left, long top, long right, long bottom, COLORREF color)
{ 
	CPen newPen;
	newPen.CreatePen(PS_SOLID, 1, color);
	CPen *oldPen = opDC->SelectObject(&newPen);
	opDC->MoveTo(left, top);
	opDC->LineTo(right, bottom);
	opDC->SelectObject(oldPen);
    newPen.DeleteObject();
}

//---------------------------------------------------------------------------

void CColorButton::DrawButtonText(CDC *opDC, CRect opRect, const char *Buf, bool bpBtUp, bool bpDisabled)
{
    COLORREF prevColor;
	opDC->SetBkMode(TRANSPARENT);
	if(bpDisabled)
	{
		prevColor = opDC->SetTextColor(omHighlightColor);
		opRect.left += 1;
		opRect.right += 1;
		opDC->DrawText(Buf, strlen(Buf), opRect, DT_CENTER|DT_VCENTER|DT_SINGLELINE);
		opDC->SetTextColor(omShadowColor);
		opRect.left -= 1;
		opRect.right -= 1;
		opRect.top -= 1;
		opRect.bottom -= 1;
		opDC->DrawText(Buf, strlen(Buf), opRect, DT_CENTER|DT_VCENTER|DT_SINGLELINE);
	}
	else
	{
		prevColor = opDC->SetTextColor(omTextColor);
		if(bpBtUp)
		{
			opRect.top -= 1;
			opRect.bottom -= 1;
		}
		else
		{
			opRect.left += 1;
			opRect.right += 1;
		}
		opDC->DrawText(Buf, strlen(Buf), opRect, DT_CENTER|DT_VCENTER|DT_SINGLELINE);
	}
	opDC->SetTextColor(prevColor);
}

//***************************************************************************

	
