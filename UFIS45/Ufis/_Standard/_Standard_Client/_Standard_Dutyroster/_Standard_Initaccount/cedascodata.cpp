// CedaScoData.cpp
 
#include <stdafx.h>
#include <CedaScoData.h>
#include <CedaStfData.h>

CedaScoData ogScoData;

void ProcessScoCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

static int CompareTimes(const SCODATA **e1, const SCODATA **e2);
static int CompareSurnAndTimes(const SCODATA **e1, const SCODATA **e2);
static int CompareCodeAndSurnAndVpfr(const SCODATA **e1, const SCODATA **e2);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaScoData::CedaScoData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for SCODataStruct
	BEGIN_CEDARECINFO(SCODATA,SCODataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_LONG		(Surn,"SURN")
		FIELD_CHAR_TRIM	(Code,"CODE")
		FIELD_OLEDATE	(Vpfr,"VPFR")
		FIELD_OLEDATE	(Vpto,"VPTO")
		FIELD_CHAR_TRIM	(Cweh,"CWEH")
	END_CEDARECINFO //(SCODataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(SCODataRecInfo)/sizeof(SCODataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SCODataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"SCO");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,"URNO,SURN,CODE,VPFR,VPTO,CWEH");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaScoData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaScoData::Register(void)
{
	ogDdx.Register((void *)this,BC_SCO_CHANGE,	CString("SCODATA"), CString("Sco-changed"),	ProcessScoCf);
	ogDdx.Register((void *)this,BC_SCO_NEW,		CString("SCODATA"), CString("Sco-new"),		ProcessScoCf);
	ogDdx.Register((void *)this,BC_SCO_DELETE,	CString("SCODATA"), CString("Sco-deleted"),	ProcessScoCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaScoData::~CedaScoData(void)
{
	TRACE("CedaScoData::~CedaScoData called\n");
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaScoData::ClearAll(bool bpWithRegistration)
{
	TRACE("CedaScoData::ClearAll called\n");
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaScoData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction2("RT");
	}
	else
	{
		ilRc = CedaAction2("RT", pspWhere);
	}
	if (ilRc != true)
	{
		TRACE("Read-Sco: Ceda-Error %d \n",ilRc);
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		SCODATA *prlSco = new SCODATA;
		if ((ilRc = GetFirstBufferRecord2(prlSco)) == true)
		{
			if (IsValidSco(prlSco) == true)
			{
				omData.Add(prlSco);//Update omData
				omUrnoMap.SetAt((void *)prlSco->Urno,prlSco);
			}
			else
			{
				delete prlSco;
			}
		}
		else
		{
			delete prlSco;
		}
	}
	TRACE("Read-Sco: %d gelesen\n",ilLc-1);
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaScoData::Insert(SCODATA *prpSco)
{
	prpSco->IsChanged = DATA_NEW;
	if(Save(prpSco) == false) return false; //Update Database
	InsertInternal(prpSco);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaScoData::InsertInternal(SCODATA *prpSco)
{
	ogDdx.DataChanged((void *)this, SCO_NEW,(void *)prpSco ); //Update Viewer
	omData.Add(prpSco);//Update omData
	omUrnoMap.SetAt((void *)prpSco->Urno,prpSco);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaScoData::Delete(long lpUrno)
{
	SCODATA *prlSco = GetScoByUrno(lpUrno);
	if (prlSco != NULL)
	{
		prlSco->IsChanged = DATA_DELETED;
		if(Save(prlSco) == false) return false; //Update Database
		DeleteInternal(prlSco);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaScoData::DeleteInternal(SCODATA *prpSco)
{
	ogDdx.DataChanged((void *)this,SCO_DELETE,(void *)prpSco); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSco->Urno);
	int ilScoCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilScoCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpSco->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaScoData::Update(SCODATA *prpSco)
{
	if (GetScoByUrno(prpSco->Urno) != NULL)
	{
		if (prpSco->IsChanged == DATA_UNCHANGED)
		{
			prpSco->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSco) == false) return false; //Update Database
		UpdateInternal(prpSco);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaScoData::UpdateInternal(SCODATA *prpSco)
{
	SCODATA *prlSco = GetScoByUrno(prpSco->Urno);
	if (prlSco != NULL)
	{
		*prlSco = *prpSco; //Update omData
		ogDdx.DataChanged((void *)this,SCO_CHANGE,(void *)prlSco); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

SCODATA *CedaScoData::GetScoByUrno(long lpUrno)
{
	SCODATA  *prlSco;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSco) == TRUE)
	{
		return prlSco;
	}
	return NULL;
}

//---------------------------------------------------------------------------------------------------------

void CedaScoData::GetScoBySurnWithTime(long lpSurn,CTime opStart,CTime opEnd,CCSPtrArray<SCODATA> *popScoData)
{
	SCODATA  *prlSco;
	COleDateTime olStart,olEnd;

	popScoData->DeleteAll();
	
	CCSPtrArray<SCODATA> olScoData;
	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSco);
		if((prlSco->Surn == lpSurn))
		{
			olScoData.Add(prlSco); 
		}
	}
	int ilSize = olScoData.GetSize();
	if(ilSize > 0)
	{
		olScoData.Sort(CompareTimes);
		
		olStart.SetDateTime(opStart.GetYear(),opStart.GetMonth(),opStart.GetDay(),opStart.GetHour(),opStart.GetMinute(),opStart.GetSecond());
		olEnd.SetDateTime(opEnd.GetYear(),opEnd.GetMonth(),opEnd.GetDay(),opEnd.GetHour(),opEnd.GetMinute(),opEnd.GetSecond());
		SCODATA *prlPrevSco=NULL;
		for(int i=ilSize;--i>=0;)
		{
			prlSco = &olScoData[i];
			
			if(prlSco->Vpfr<olEnd)
			{
				switch(prlSco->Vpto.GetStatus())
				{
				case COleDateTime::valid:
					if(prlSco->Vpto>olStart)
					{
						popScoData->Add(prlSco);
					}
					else
					{
						i=0;
					}
					break;
				case COleDateTime::null:
					if(prlPrevSco!=NULL)
					{
						if(prlPrevSco->Vpfr>olStart)
						{
							popScoData->Add(prlSco);
						}
						else
						{
							i=0;
						}
					}
					else
					{
						popScoData->Add(prlSco);
					}
					break;
				default:
					break;
				}
			}
			prlPrevSco = prlSco;
		}
	}
}


void CedaScoData::GetScoBySurnWithTime(long lpSurn,COleDateTime opStart,COleDateTime opEnd,CCSPtrArray<SCODATA> *popScoData)
{
	SCODATA  *prlSco;

	popScoData->RemoveAll();
	
	CCSPtrArray<SCODATA> olScoData;
	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSco);
		if((prlSco->Surn == lpSurn))
		{
			olScoData.Add(prlSco); 
		}
	}
	int ilSize = olScoData.GetSize();
	if(ilSize > 0)
	{
		olScoData.Sort(CompareTimes);
		
		SCODATA *prlPrevSco=NULL;
		for(int i=ilSize;--i>=0;)
		{
			prlSco = &olScoData[i];
			
			if(prlSco->Vpfr<opEnd)
			{
				switch(prlSco->Vpto.GetStatus())
				{
				case COleDateTime::valid:
					if(prlSco->Vpto>opStart)
					{
						popScoData->Add(prlSco);
					}
					else
					{
						i=0;
					}
					break;
				case COleDateTime::null:
					if(prlPrevSco!=NULL)
					{
						if(prlPrevSco->Vpfr>opStart)
						{
							popScoData->Add(prlSco);
						}
						else
						{
							i=0;
						}
					}
					else
					{
						popScoData->Add(prlSco);
					}
					break;
				default:
					break;
				}
			}
			prlPrevSco = prlSco;
		}
	}
}

//---------------------------------------------------------------------------------------

CString CedaScoData::GetCotBySurnWithTime(long lpSurn, COleDateTime opDate)
{
	CString olCotCode;

	CTime olDay(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),0,0,0);
	CCSPtrArray<SCODATA> olScoData;

	GetScoBySurnWithTime(lpSurn, olDay, olDay, &olScoData);
	int ilScoSize = olScoData.GetSize();

	for(int i=0; i<ilScoSize; i++)
	{
		if(olScoData[i].Vpfr.GetStatus() == COleDateTime::valid)
		{
			if(olScoData[i].Vpfr <= opDate)
			{
				switch(olScoData[i].Vpto.GetStatus())
				{
				case COleDateTime::valid:
					if(olScoData[i].Vpto > opDate)
					{
						olCotCode = olScoData[i].Code;
					}
					else
					{
						olCotCode = "";

					}
					break;
				default:
						olCotCode = olScoData[i].Code;
					break;
				}
			}
		}
	}
	olScoData.RemoveAll();
	return olCotCode;
}

//---------------------------------------------------------------------------------------

long CedaScoData::GetScoUrnoWithTime(COleDateTime opDate, CCSPtrArray<SCODATA> *popScoData)
{
	long llUrno = NULL;

	int ilScoSize = popScoData->GetSize();

	for(int i=0; i<ilScoSize; i++)
	{
		if((*popScoData)[i].Vpfr.GetStatus() == COleDateTime::valid)
		{
			if((*popScoData)[i].Vpfr <= opDate)
			{
				switch((*popScoData)[i].Vpto.GetStatus())
				{
				case COleDateTime::valid:
					if((*popScoData)[i].Vpto > opDate)
					{
						llUrno = (*popScoData)[i].Urno;
					}
					else
					{
						llUrno = NULL;
					}
					break;
				default:
						llUrno = (*popScoData)[i].Urno;
					break;
				}
			}
		}
	}
	return llUrno;
}

//---------------------------------------------------------------------------------------

CString CedaScoData::GetCotWithTime(COleDateTime opDate, CCSPtrArray<SCODATA> *popScoData)
{
	CString olCotCode;

	int ilScoSize = popScoData->GetSize();

	for(int i=0; i<ilScoSize; i++)
	{
		if((*popScoData)[i].Vpfr.GetStatus() == COleDateTime::valid)
		{
			if((*popScoData)[i].Vpfr <= opDate)
			{
				switch((*popScoData)[i].Vpto.GetStatus())
				{
				case COleDateTime::valid:
					if((*popScoData)[i].Vpto > opDate)
					{
						olCotCode = (*popScoData)[i].Code;
					}
					else
					{
						olCotCode = "";

					}
					break;
				default:
						olCotCode = (*popScoData)[i].Code;
					break;
				}
			}
		}
	}
	return olCotCode;
}

//---------------------------------------------------------------------------------------------------------

CString CedaScoData::GetScoByCotWithTime(CStringArray *popCot, COleDateTime opStart, COleDateTime opEnd, CCSPtrArray<SCODATA> *popScoData)
{
	popScoData->DeleteAll();
	CString olUrnos = "";

	if(popCot->GetSize() > 0 && opStart.GetStatus() == COleDateTime::valid && opEnd.GetStatus() == COleDateTime::valid && opStart <= opEnd)
	{
		CString olTmpCode;
		COleDateTime olTmpStart,olTmpEnd;
		CMapStringToPtr olCotMap;
		SCODATA *prlSco = NULL;


		for(int i=0; i<popCot->GetSize(); i++)
		{
			olCotMap.SetAt(popCot->GetAt(i),NULL);
		}

		CCSPtrArray<SCODATA> olScoData;
		POSITION rlPos;
		for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSco);
			olScoData.Add(prlSco); 
		}

		olScoData.Sort(CompareSurnAndTimes);
		int ilSize = olScoData.GetSize();
		SCODATA *prlNextSco = NULL;
		void  *prlVoid = NULL;

		for(i = 0; i < ilSize; i++)
		{
			prlSco = &olScoData[i];
			prlNextSco = NULL;
			if((i+1) < ilSize)
			{
				if(prlSco->Surn == olScoData[i+1].Surn)
					prlNextSco = &olScoData[i+1];
			}

			if(prlSco->Vpfr.GetStatus() == COleDateTime::valid)
			{
				if(prlSco->Vpfr<=opEnd)
				{
					switch(prlSco->Vpto.GetStatus())
					{
					case COleDateTime::valid:
						if(prlSco->Vpto>=opStart)
						{
							olTmpCode.Format("%s",prlSco->Code);
							if(olCotMap.Lookup(olTmpCode,(void *&)prlVoid) == TRUE)
							{
								popScoData->Add(prlSco);
							}
						}
						break;
					case COleDateTime::null:
						if(prlNextSco != NULL)
						{
							if(prlNextSco->Vpfr>=opStart)
							{
								olTmpCode.Format("%s",prlSco->Code);
								if(olCotMap.Lookup(olTmpCode,(void *&)prlVoid) == TRUE)
								{
									popScoData->Add(prlSco);
								}
							}
						}
						else
						{
							olTmpCode.Format("%s",prlSco->Code);
							if(olCotMap.Lookup(olTmpCode,(void *&)prlVoid) == TRUE)
							{
								popScoData->Add(prlSco);
							}
						}
						break;
					default:
						break;
					}
				}
			}
		}
		// Create Stf-Urno-List for return value
		ilSize = popScoData->GetSize();
		olUrnos = ",";
		for(i = 0; i < ilSize; i++)
		{
			CString olTmpUrno;
			olTmpUrno.Format(",%ld,",(*popScoData)[i].Surn);
			if(olUrnos.Find(olTmpUrno) == -1)
			{
				olUrnos += olTmpUrno.Mid(1);
			}
		}
		if(olUrnos.IsEmpty() == FALSE)
		{
			olUrnos = olUrnos.Mid(1,olUrnos.GetLength()-2);
		}
		olCotMap.RemoveAll();
	}
	return olUrnos;
}


//--SAVE---------------------------------------------------------------------------------------------------

bool CedaScoData::Save(SCODATA *prpSco)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSco->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSco->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSco);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSco->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSco->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSco);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSco->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSco->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	TRACE("Sco-IRT/URT/DRT: Ceda-Return %d \n",ilRc);
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessScoCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogScoData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaScoData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlScoData;
	prlScoData = (struct BcStruct *) vpDataPointer;
	SCODATA *prlSco;
	if(ipDDXType == BC_SCO_NEW)
	{
		prlSco = new SCODATA;
		GetRecordFromItemList(prlSco,prlScoData->Fields,prlScoData->Data);
		InsertInternal(prlSco);
	}
	if(ipDDXType == BC_SCO_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlScoData->Selection);
		prlSco = GetScoByUrno(llUrno);
		if(prlSco != NULL)
		{
			GetRecordFromItemList(prlSco,prlScoData->Fields,prlScoData->Data);
			UpdateInternal(prlSco);
		}
	}
	if(ipDDXType == BC_SCO_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlScoData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlScoData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlSco = GetScoByUrno(llUrno);
		if (prlSco != NULL)
		{
			DeleteInternal(prlSco);
		}
	}
	this->omData.Sort(CompareCodeAndSurnAndVpfr);
}

//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------

static int CompareTimes(const SCODATA **e1, const SCODATA **e2)
{
	if((**e1).Vpfr>(**e2).Vpfr)
		return 1;
	else
		return -1;
}

//---------------------------------------------------------------------------------------------------------

static int CompareSurnAndTimes(const SCODATA **e1, const SCODATA **e2)
{
	int ilCompareResult = 0;

	     if((**e1).Surn>(**e2).Surn) ilCompareResult = 1;
	else if((**e1).Surn<(**e2).Surn) ilCompareResult = -1;

	if(ilCompareResult == 0)
	{
		     if((**e1).Vpfr>(**e2).Vpfr) ilCompareResult = 1;
		else if((**e1).Vpfr<(**e2).Vpfr) ilCompareResult = -1;
	}
	return ilCompareResult;
}

static int CompareCodeAndSurnAndVpfr(const SCODATA **e1, const SCODATA **e2)
{
	int ilCompareResult = 0;

	// comparing SCO.CODE
	if((**e1).Code>(**e2).Code)
		ilCompareResult = 1;
	else if((**e1).Code<(**e2).Code)
		ilCompareResult = -1;

	// comparing SCO.SURN
	if(ilCompareResult == 0)
	{
		if((**e1).Surn>(**e2).Surn)
			ilCompareResult = 1;
		else if((**e1).Surn<(**e2).Surn)
			ilCompareResult = -1;
	}

	// comparing SCO.VPFR
	if(ilCompareResult == 0)
	{
		if((**e1).Vpfr>(**e2).Vpfr)
			ilCompareResult = 1;
		else if((**e1).Vpfr<(**e2).Vpfr)
			ilCompareResult = -1;
	}

	return ilCompareResult;
}

//---------------------------------------------------------------------------------------------------------

bool CedaScoData::ReadScoData()
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	ClearAll(true);
	
    if (!CedaAction2("RT", "")) return false;
	
	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		SCODATA *prlSco = new SCODATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord2(ilCountRecords,prlSco);
		// Datensatz gelesen, Z�hler inkrementieren
		if (!blMoreRecords) break;
		ilCountRecords++;
		if (!InsertInternal(prlSco)){ //, SCO_NO_SEND_DDX)){
			// Fehler -> lokalen Datensatz l�schen
			delete prlSco;
		}
	} while (blMoreRecords);
	
	
	// Test: Anzahl der gelesenen Bud
	TRACE("Read-Sco: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}

bool CedaScoData::Initialize(CString opServerName)
{
	omServerName = opServerName;
	Register();
	return true;
}

bool CedaScoData::IsValidSco(SCODATA* popSco)
{
	if (!ogStfData.GetStfByUrno(popSco->Surn))
	{
		// employee does not exist
		return false;
	}

	if (!ogCotData.GetCotByCtrc(CString(popSco->Code)))
	{
		// no contract found
		return false;
	}

	if (popSco->Vpfr.GetStatus() != COleDateTime::valid || popSco->Vpto.GetStatus() == COleDateTime::invalid)
	{
		// timeframe invalid
		return false;
	}

	return true;
}