// InitializeAccountDlg.cpp : implementation file
//

#include <stdafx.h>
#include <InitializeAccount.h>
#include <InitializeAccountDlg.h>
#include <GridControl.h>
#include <waitdlg.h>
#include <Ufis.h>
#include <OpenJobControlDialog.h>
#include <ListBoxDlg.h>

#include <CedaSorData.h>
#include <CedaScoData.h>
#include <CedaCotData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MAX_BC_SELECTION_SIZE	10000
#define MAX_BC_FIELDS			10000
#define MAX_BC_DATA				10000
#define MAX_BC_TW_SIZE			36

void Process_InitializeAccountDlg_Cf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

static bool ValidateChangeTime(CString opInput, CString *popError)
{
	if ((opInput.SpanIncluding("0123456789") != opInput) ||
		(opInput.GetLength() != 4))
	{
		*popError = "Bitte benutzen Sie nur numerische Zeichen. \nDie Engabe muss das Format HHMM haben.";
		return false;
	}
	return true;
}

static bool ValidateChangeDay(CString opInput, CString *popError)
{
	if ((opInput.SpanIncluding("0123456789") != opInput) ||
		(opInput.GetLength() != 2))
	{
		*popError = "Bitte benutzen Sie nur numerische Zeichen. \nDie Engabe muss das Format DD haben.";
		return false;
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CInitializeAccountDlg dialog

CInitializeAccountDlg::CInitializeAccountDlg(CWnd* pParent, CString opStartNightJobAt,
											 CString opCmdLohnbestandteile, 
											 CString opCmdRecalculation,
											 CString opStartMonthlyTimeAndDate,
											 int ipMaxEmplUrnosPerBC)

	: CDialog(CInitializeAccountDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CInitializeAccountDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	pomEmplList = NULL;
	pomStatusBar = NULL;
	pomAccountList = NULL;
	omStartNightJobAt = opStartNightJobAt;

	omVertragStunden.RemoveAll();

	// Start time and day of month if monthly jobs are to be executed.
	// Format: 'DDHHMM' with DD = day of month, HHMM = time.
	// If the date is greater than 28 it is automatically set to 31 which means
	// last day of month. This specific day, which is different each month, must be 
	// calculated by the handler.
	omStartMonthlyTimeAndDate = opStartMonthlyTimeAndDate;
	omCmdLohnbestandteile = opCmdLohnbestandteile;
	omCmdRecalculation = opCmdRecalculation;
	imExecutionMode = EXECUTION_NOW;
	// <omStartMonthlyTimeAndDate> must be valid
	ASSERT(omStartMonthlyTimeAndDate.GetLength() == 6);
	ASSERT(omStartMonthlyTimeAndDate.SpanIncluding("0123456789") == omStartMonthlyTimeAndDate);
	// copy maximum number of employee urnos sended per broadcast
	imMaxEmplUrnosPerBC = ipMaxEmplUrnosPerBC;

	ogDdx.Register((void*)this, BC_XBS2_READY, CString("BC_XBS2_READY"), CString("XBS2 executed"),ProcessAccountCf);

}

CInitializeAccountDlg::~CInitializeAccountDlg()
{
CCS_TRY
	ogDdx.UnRegister(this,NOTUSED);
	// delete grids
	if (pomEmplList != NULL) delete pomEmplList;
	pomEmplList = NULL;
	if (pomAccountList != NULL) delete pomAccountList;
	pomAccountList = NULL;
	// delete status bar
	if (pomStatusBar != NULL) delete pomStatusBar;
	pomStatusBar = NULL;
    // clear the info structs
	omAccountTypeInfoList.DeleteAll();
	omAccountTypeInfoPtrMap.RemoveAll();
	bmUserAbort = false;

	omVertragStunden.RemoveAll();

	omStfData.DeleteAll();

CCS_CATCH_ALL
}

void CInitializeAccountDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInitializeAccountDlg)
	DDX_Control(pDX, IDC_BUTTON_OPENJOBCONTROL, m_but_OpenJobControl);
	DDX_Control(pDX, IDC_STATIC_NOWORLATER, m_stc_NowOrLater);
	DDX_Control(pDX, IDC_S_ORG, m_S_Org);
	DDX_Control(pDX, IDC_SHOW_EMPLOYEE, m_B_Filter);
	DDX_Control(pDX, IDC_C_ORG, m_C_Orgeinheit);
	DDX_Control(pDX, IDC_BUTTON_ABORT, m_b_Abort);
	DDX_Control(pDX, IDC_B_ACTUALIZE, m_b_Actualize);
	DDX_Control(pDX, IDOK, m_b_Ok);
	DDX_Control(pDX, IDC_STC_YEAR, m_stc_Year);
	DDX_Control(pDX, IDC_STATICTIMESPAN, m_stc_TimeSpan);
	DDX_Control(pDX, IDC_STATICEMPLOYEES, m_stc_Employees);
	DDX_Control(pDX, IDC_STATICACCOUNTS, m_stc_Accounts);
	DDX_Control(pDX, IDC_SELECT_ALL_EMPLOYEES, m_b_SelectAll);
	DDX_Control(pDX, IDC_DESELECT_ALL_EMPLOYEES, m_b_DeselectAll);
	DDX_Control(pDX, IDC_FIND_EMPLOYEE, m_b_FindEmployee);
	DDX_Control(pDX, IDC_RB_DATE,	m_R_Day);
	DDX_Control(pDX, IDC_MONTH_1,	m_R_Month01);
	DDX_Control(pDX, IDC_MONTH_2,	m_R_Month02);
	DDX_Control(pDX, IDC_MONTH_3,	m_R_Month03);
	DDX_Control(pDX, IDC_MONTH_4,	m_R_Month04);
	DDX_Control(pDX, IDC_MONTH_5,	m_R_Month05);
	DDX_Control(pDX, IDC_MONTH_6,	m_R_Month06);
	DDX_Control(pDX, IDC_MONTH_7,	m_R_Month07);
	DDX_Control(pDX, IDC_MONTH_8,	m_R_Month08);
	DDX_Control(pDX, IDC_MONTH_9,	m_R_Month09);
	DDX_Control(pDX, IDC_MONTH_10,	m_R_Month10);
	DDX_Control(pDX, IDC_MONTH_11,	m_R_Month11);
	DDX_Control(pDX, IDC_MONTH_12,	m_R_Month12);
	DDX_Control(pDX, IDC_RADIO_NOW,	m_R_Now);
	DDX_Control(pDX, IDC_RADIO_LATER,	m_R_Later);
	DDX_Control(pDX, IDC_RB_ACCOUNT_FILTER_INIT,	m_R_AccFilterInit);
	DDX_Control(pDX, IDC_RB_ACCOUNT_FILTER_RECALC,	m_R_AccFilterRecalc);
	DDX_Control(pDX, IDC_RB_ACCOUNT_FILTER_SALDO,	m_R_AccFilterSaldo);
	DDX_Control(pDX, IDC_RB_LOHNBESTANDTEILE,		m_R_Lohnbestandteile);
	DDX_Control(pDX, IDC_DATE,		m_E_Date);
	DDX_Control(pDX, IDC_YEAR,		m_E_Year);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CInitializeAccountDlg, CDialog)
	//{{AFX_MSG_MAP(CInitializeAccountDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_SELECT_ALL_EMPLOYEES, OnSelectAllEmployees)
	ON_BN_CLICKED(IDC_DESELECT_ALL_EMPLOYEES, OnDeselectAllEmployees)
	ON_BN_CLICKED(IDC_FIND_EMPLOYEE, OnFindEmployee)
	ON_BN_CLICKED(IDC_RB_DATE, OnRbDate)
	ON_BN_CLICKED(IDC_MONTH_1, OnMonth)
	ON_BN_CLICKED(IDC_RB_ACCOUNT_FILTER_INIT, OnAccFilter)
	ON_BN_CLICKED(IDC_B_ACTUALIZE, OnBActualize)
	ON_BN_CLICKED(IDC_BUTTON_ABORT, OnButtonAbort)
	ON_CBN_SELCHANGE(IDC_C_ORG, OnSelchange_C_Org)
	ON_EN_UPDATE(IDC_DATE, OnUpdateDate)
	ON_EN_UPDATE(IDC_YEAR, OnUpdateYear)
	ON_BN_CLICKED(IDC_RADIO_NOW, OnRadioNow)
	ON_BN_CLICKED(IDC_RADIO_LATER, OnRadioLater)
	ON_BN_CLICKED(IDC_RB_LOHNBESTANDTEILE, OnRbLohnbestandteile)
	ON_BN_CLICKED(IDC_RADIO_MONTHLY, OnRadioMonthly)
	ON_BN_CLICKED(IDC_BUTTON_OPENJOBCONTROL, OnButtonOpenjobcontrol)
	ON_BN_CLICKED(IDC_SHOW_EMPLOYEE, OnShowFilteredEmployee)
	ON_MESSAGE(WM_BCADD, OnBcAdd)
	ON_BN_CLICKED(IDC_MONTH_2, OnMonth)
	ON_BN_CLICKED(IDC_MONTH_3, OnMonth)
	ON_BN_CLICKED(IDC_MONTH_4, OnMonth)
	ON_BN_CLICKED(IDC_MONTH_5, OnMonth)
	ON_BN_CLICKED(IDC_MONTH_6, OnMonth)
	ON_BN_CLICKED(IDC_MONTH_7, OnMonth)
	ON_BN_CLICKED(IDC_MONTH_8, OnMonth)
	ON_BN_CLICKED(IDC_MONTH_9, OnMonth)
	ON_BN_CLICKED(IDC_MONTH_10, OnMonth)
	ON_BN_CLICKED(IDC_MONTH_11, OnMonth)
	ON_BN_CLICKED(IDC_MONTH_12, OnMonth)
	ON_BN_CLICKED(IDC_RB_ACCOUNT_FILTER_RECALC, OnRbRecalculation)
	ON_BN_CLICKED(IDC_RB_ACCOUNT_FILTER_SALDO, OnAccFilter)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInitializeAccountDlg message handlers

BOOL CInitializeAccountDlg::OnInitDialog()
{
	CString strMsg;
	char pclConfigPath[256];

	CDialog::OnInitDialog();

	// Wartedialog anzeigen
	CWaitDlg olWaitDlg(this,LoadStg(WAIT_DATA_PREPARE));

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu = LoadStg(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
	    strcpy(pclConfigPath, getenv("CEDA"));

	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pclConfigPath);

	//MWO/RRO 25.03.03
	if (stricmp(pclUseBcProxy,"BCSERV") == 0)
	{
		ogCommHandler.RegisterBcWindow(this);
	}
	else
	{
		//MWO/RRO
		//ogCommHandler.RegisterBcWindow(this);
		ogBcHandle.StartBc(); 
		//END MWO/RRO
	}
	ogBcHandle.GetBc();

	//uhi 13.9.01Parameter lesen
	CTime	olTimeNow = CTime::GetCurrentTime();
	CString olStringNow = olTimeNow.Format("%Y%m%d%H%M%S");

	FillContractMap();

	// initialize statusbar
	pomStatusBar = new UDlgStatusBar(this, 10815, UDSB_SS_SUNKEN);//UDSB_SS_HIGH
	imIDProgress = pomStatusBar->SetProgress(0, 2 ,200);
	imIDStatic   = pomStatusBar->SetStatic("", 3, 190);
	pomStatusBar->SetStaticText(1, LoadStg(IDS_STRING1459));
	// hide abort button
	m_b_Abort.EnableWindow(false);
	m_b_Abort.ShowWindow(SW_HIDE);
	// set up dialog data
	m_E_Date.SetWindowText(ogCmdLineStghArray.GetAt(1));
	m_R_Day.SetCheck(1);

	// setup execution radio buttons
	CString olLaterText, olMonthlyText, olMonthlyDay;
	m_R_Now.SetCheck(1);
	imExecutionMode = EXECUTION_NOW;
	m_R_Later.SetCheck(0);

	// check monthly execution day
	olMonthlyDay = omStartMonthlyTimeAndDate.Left(2);
	if (atoi(LPCTSTR(olMonthlyDay)) >= 29)
	{
		olMonthlyDay = LoadStg(IDS_STRING1903);
	}
	olMonthlyText.Format(LoadStg(IDS_STRING1902),olMonthlyDay,omStartMonthlyTimeAndDate.Mid(2,2),omStartMonthlyTimeAndDate.Right(2));
	m_R_Now.SetWindowText(LoadStg(IDS_STRING1900));
	m_stc_NowOrLater.SetWindowText(LoadStg(IDS_STRING1899));
	olLaterText.Format(LoadStg(IDS_STRING1898),omStartNightJobAt.Left(2),omStartNightJobAt.Right(2));
	m_R_Later.SetWindowText(olLaterText);

	// set filter prior to LoadAccountData()!!!
	m_R_AccFilterInit.SetCheck(1);
	m_R_Lohnbestandteile.SetCheck(0);
	// disable other filter radio buttons
#ifdef _DISABLE_SALDO
	m_R_AccFilterSaldo.ShowWindow(SW_HIDE);
#endif
#ifdef _DISABLE_RECALC
	m_R_AccFilterRecalc.ShowWindow(SW_HIDE);
#endif
#ifdef _DISABLE_SALARY
	m_R_Lohnbestandteile.ShowWindow(SW_HIDE);
#endif

	// load strings
	LoadStringResources();

	// load account data
	LoadAccountData();

	// initialize and fill the grids
	InitGrids();

	// valid time info?
	if (ogCmdLineStghArray.GetAt(1) != "")
	{
		// yes -> use it
		m_E_Date.SetInitText(ogCmdLineStghArray.GetAt(1));	
		m_E_Year.SetInitText(ogCmdLineStghArray.GetAt(1).Right(4));
	}
	else{
		// no -> get current time and use it as initial values
		m_E_Date.SetInitText(COleDateTime::GetCurrentTime().Format("%d.%m.%Y"));	
		m_E_Year.SetInitText(COleDateTime::GetCurrentTime().Format("%Y"));
	}
	
	// initialize date edit control
	m_E_Date.SetBKColor(LTYELLOW);
	m_E_Date.SetTextErrColor(RED);
	m_E_Date.SetTypeToDate(true);

	// initialize year edit control
	m_E_Year.SetBKColor(LTYELLOW);
	m_E_Year.SetTextErrColor(RED);
	m_E_Year.SetTypeToInt(1970, 2070);
	m_E_Year.EnableWindow(FALSE);

	// initialize handle of dialog box to enable key input processing
	((CInitializeAccountApp*)AfxGetApp())->m_hwndDialog = m_hWnd;

	// initialize Organisation unit
	m_C_Orgeinheit.SetFont(&ogCourier_Regular_10);
	FillOrgCombo();
	OnShowFilteredEmployee();

	// check parameter, must be greater 0
	if (imMaxEmplUrnosPerBC <= 0)
	{
		// invalid value -> set to default, do warning
		strMsg.Format(LoadStg(IDS_STRING1950),pclConfigPath,imMaxEmplUrnosPerBC,50,pclConfigPath);
		MessageBox(strMsg,"InitializeAccount",MB_OK | MB_ICONEXCLAMATION);
		imMaxEmplUrnosPerBC = 50;
	}

	ogDdx.Register((void *)this, COT_CHANGE, CString("InitializeAccountDlg"), CString("COT_CHANGE"), Process_InitializeAccountDlg_Cf);
	ogDdx.Register((void *)this, COT_DELETE, CString("InitializeAccountDlg"), CString("COT_DELETE"), Process_InitializeAccountDlg_Cf);
	ogDdx.Register((void *)this, COT_NEW, CString("InitializeAccountDlg"), CString("COT_NEW"), Process_InitializeAccountDlg_Cf);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CInitializeAccountDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

void CInitializeAccountDlg::OnDestroy()
{
	// set handle of dialog box to NULL
	((CInitializeAccountApp*)AfxGetApp())->m_hwndDialog = NULL;

	WinHelp(0L, HELP_QUIT);
	CDialog::OnDestroy();
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CInitializeAccountDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//***************************************************************************
// The system calls this to obtain the cursor to display while the user drags
// the minimized window.
//***************************************************************************

HCURSOR CInitializeAccountDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

//***************************************************************************
// In OnBcAdd laufen alle Broadcasts ein
// diese werden dann vom BcHandle weitergeleitet
//***************************************************************************

LONG CInitializeAccountDlg::OnBcAdd(UINT wParam, LONG /*lParam*/)
{
	ogBcHandle.GetBc(wParam);
	return TRUE;
}

//**********************************************************************************
// InitGrids: Initialize the grids.
// Input:	none
// Return:	none
//**********************************************************************************

void CInitializeAccountDlg::InitGrids()
{
CCS_TRY
	InitEmployeeGrid();
	InitAccountGrid();
CCS_CATCH_ALL
}

//**********************************************************************************
// InitEmployeeGrid: initializes the grid which shows all employees.
// Input:	none
// Return:	none
//**********************************************************************************

void CInitializeAccountDlg::InitEmployeeGrid()
{
	// create the grid
	pomEmplList = new CGridControl (this, IDC_EMPL_GRID,EMPL_COLCOUNT,0);
	pomEmplList->Initialize();

	// allways show vertical scroll bar 
	pomEmplList->SetScrollBarMode(SB_BOTH,gxnAutomatic);

	// set the headlines
	pomEmplList->SetValueRange(CGXRange(0,1), LoadStg(SHIFT_STF_PENO));
	pomEmplList->SetValueRange(CGXRange(0,2), LoadStg(SHIFT_STF_SHNM));
	pomEmplList->SetValueRange(CGXRange(0,3), LoadStg(SHIFT_STF_NAME));
	pomEmplList->SetValueRange(CGXRange(0,4), LoadStg(SHIFT_STF_ORGCODE));
	pomEmplList->SetValueRange(CGXRange(0,5), LoadStg(SHIFT_STF_CONTRACT));

	// setup the column widths
	InitEmployeeColWidths ();

	pomEmplList->Initialize();
	pomEmplList->GetParam()->EnableUndo(FALSE);
	pomEmplList->LockUpdate(TRUE);
	pomEmplList->LockUpdate(FALSE);
	pomEmplList->GetParam()->EnableUndo(TRUE);

	// don't allow the user to change column widths
	//pomEmplList->GetParam()->EnableTrackColWidth(FALSE);

	// prevent single cell selection, only whole lines can be selected
	pomEmplList->GetParam()->EnableSelection(GX_SELROW | GX_SELMULTIPLE | GX_SELSHIFT);

	pomEmplList->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE));
	pomEmplList->EnableAutoGrow (FALSE);
}

//**********************************************************************************
// InitAccountGrid: initializes the grid which shows all accounts.
// Input:	none
// Return:	none
//**********************************************************************************

void CInitializeAccountDlg::InitAccountGrid()
{
	// how many accounts are to be shown
	int ilShowAccountSize = omAccountTypeInfoList.GetSize();
	
	// Grid erzeugen und initialisieren
	pomAccountList = new CGridControl (this,IDC_ACCOUNT_GRID,ACCOUNT_COLCOUNT,ilShowAccountSize);
	pomAccountList->Initialize();

	// allways show vertical scroll bar 
	pomAccountList->SetScrollBarMode(SB_VERT,gxnAutomatic);
	
	// set up column widths
	InitAccountColWidths();
	
	// set headlines
	pomAccountList->SetValueRange(CGXRange(0,1), LoadStg(IDS_STRING1851));	// 'code'
	pomAccountList->SetValueRange(CGXRange(0,2), LoadStg(IDS_STRING1850));	// 'name'

	// anything to do?
	if (ilShowAccountSize <= 0)
		return; // no accounts -> terminate here
	
	// only whole lines are selectable
	pomAccountList->GetParam()->EnableSelection();//GX_SELROW | GX_SELMULTIPLE | GX_SELSHIFT);
	// content is read only ...
	pomAccountList->SetStyleRange(CGXRange().SetCols(0,1),
								  CGXStyle().SetVerticalAlignment(DT_VCENTER).SetReadOnly(true));
	// no automatic insertion of lines
	pomAccountList->EnableAutoGrow(false);
	// now fill the grid
	FillAccountGrid();
}

//**********************************************************************************
// InitEmployeeColWidths: set up column widths of employee grid.
//	The width depends on the number of letters in its cells.
// Input:	none
// Return:	none
//**********************************************************************************

void CInitializeAccountDlg::InitEmployeeColWidths()
{
CCS_TRY
	// numbers
	pomEmplList->SetColWidth ( 0, 0, 27);

	//PENO
	pomEmplList->SetColWidth ( 1, 1, 60);

	//SHNM
	pomEmplList->SetColWidth ( 2, 2, 90);

	//LANM, FINM
	pomEmplList->SetColWidth ( 3, 3, 130);

	//Org Unit
	pomEmplList->SetColWidth ( 4, 4, 100);

	//Contract
	pomEmplList->SetColWidth ( 4, 4, 100);

CCS_CATCH_ALL
}

//**********************************************************************************
// InitAccountColWidths: set up column widths of account grid.
// Input:	none
// Return:	none
//**********************************************************************************

void CInitializeAccountDlg::InitAccountColWidths()
{
CCS_TRY
	pomAccountList->SetColWidth ( 0, 0, 27);
	pomAccountList->SetColWidth ( 1, 1, 40);
	pomAccountList->SetColWidth ( 2, 2, 232);
CCS_CATCH_ALL
}

//**********************************************************************************
// FillEmployeeGrid. Fill the grid with staff data.
// Input:	none
// Return:	none
//**********************************************************************************

void CInitializeAccountDlg::FillEmployeeGrid()
{
CCS_TRY
	CString csFullName;
	CString csUrno;
	CString	csCot;
	CString olValue;
	long llUrno;

	// for getting org code
	COleDateTime olStart;
	COleDateTime olEnd;
	int ilRows, i;

	// get and check start and end time
	if (!GetAndCheckTimeSpan(olStart,olEnd))
	{
		return;
	}

	// remove the employees which should not be initialised
	if (m_R_AccFilterInit.GetCheck() == 1)
	{
		CMapPtrToPtr olStfRemoveUrnoMap;
		SCODATA *rolScoData = NULL;
		COTDATA *rolCotData = NULL;
		CString olCTRC;
		CString olUrno;
		void* rlVoid;

		// be aware: ogScoData.omData is sorted by CTRC!
		int ilScoCnt = ogScoData.omData.GetSize();
		for (i = 0; i < ilScoCnt; i++)
		{
			rolScoData = &ogScoData.omData[i];
			rolCotData = ogCotData.GetCotByCtrc (rolScoData->Code);
			if (rolCotData != NULL)
			{
				if (omVertragStunden.Lookup(rolScoData->Code, olValue))
				{
					if ((rolScoData->Vpfr <= olEnd && (rolScoData->Vpto.GetStatus() != COleDateTime::valid || rolScoData->Vpto >= olStart))
					 || (rolScoData->Vpfr <= olStart && (rolScoData->Vpto.GetStatus() != COleDateTime::valid || rolScoData->Vpto >= olEnd)))
					{
						olStfRemoveUrnoMap.SetAt((void*)rolScoData->Surn, NULL);
					}
				}
			}
		}

		ilRows = omStfData.GetSize();	
		for (i = ilRows - 1; i > -1; i--)
		{
			if (olStfRemoveUrnoMap.Lookup((void*)omStfData[i].Urno, rlVoid))
			{
				omStfData.DeleteAt(i);
			}
		}
	}

	ilRows = pomEmplList->GetRowCount();
	if (ilRows >= 1)
	{
		pomEmplList->RemoveRows(1, ilRows);
	}

	pomEmplList->SetRowCount(omStfData.GetSize());
	pomEmplList->GetParam()->SetLockReadOnly(false);

	// read all staff member from the global object, which is initialized in 
	// CInitializeAccountApp::InitialLoad()
	for (i = 0; i < omStfData.GetSize(); i++ )
	{
		// read the urno
		llUrno = omStfData[i].Urno;
		csUrno.Format ("%ld",omStfData[i].Urno);

		// connect urno with first cell of each line
		pomEmplList->SetStyleRange ( CGXRange(i+1,1), CGXStyle().SetItemDataPtr( (void*)llUrno ) );

		// get the staff number
		pomEmplList->SetValueRange(CGXRange(i+1,1), omStfData[i].Peno);

		// get the nickname
		pomEmplList->SetValueRange(CGXRange(i+1,2), omStfData[i].Shnm );

		// get lastname and first name
		csFullName = omStfData[i].Lanm;
		csFullName += ",";
		csFullName += omStfData[i].Finm;
		pomEmplList->SetValueRange(CGXRange(i+1,3), csFullName);

		// fill in org code
		pomEmplList->SetValueRange(CGXRange(i+1,4), GetEmployeeOrgCode(llUrno,olStart,olEnd));

		//uhi 13.9.01 Additional column contract type
		pomEmplList->SetValueRange(CGXRange(i+1,5), GetEmployeeContract(llUrno,olStart,olEnd));
	}

	pomEmplList->GetParam()->SetLockReadOnly(true);
	pomEmplList->Redraw();
	SortEmployeeGrid(3); //sort by name
	pomEmplList->SetScrollBarMode(SB_VERT,gxnAutomatic);

CCS_CATCH_ALL
}

//**********************************************************************************
// FillAccountGrid. Fill the grid with account data.
// Input:	none
// Return:	int number of entrys in grid
//**********************************************************************************

int CInitializeAccountDlg::FillAccountGrid()
{
CCS_TRY
	// row counter
	int ilRowCount = 0;
	// number of known accounts
	int ilAccountSize = omAccountTypeInfoList.GetSize();

	// enable update
	pomAccountList->GetParam()->SetLockReadOnly(false);

	// get number of rows
	ilRowCount = pomAccountList->GetRowCount();
	if (ilRowCount > 0) 
	{
		// remove all rows
		pomAccountList->RemoveRows(1,ilRowCount);
	}
	// reset row counter
	ilRowCount = 0;

	// add all accounts
	for (int i=0; i<ilAccountSize; i++)
	{
		// check filter
		if (((m_R_AccFilterInit.GetCheck() == 1) && (omAccountTypeInfoList[i].oScbi != "")) ||
			((m_R_AccFilterRecalc.GetCheck() == 1) && (omAccountTypeInfoList[i].oScbr != "")) ||
			((m_R_AccFilterSaldo.GetCheck() == 1) && (omAccountTypeInfoList[i].oScbs != "")))
		{
			// insert Row
			pomAccountList->InsertRows(ilRowCount+1,1);
			// connect the account type no. with first column
			pomAccountList->SetStyleRange (CGXRange(ilRowCount+1,1),
				CGXStyle().SetItemDataPtr((void*)omAccountTypeInfoList[i].iInternNumber));
			pomAccountList->SetValueRange(CGXRange(ilRowCount+1,1), omAccountTypeInfoList[i].oExternKey );
			pomAccountList->SetValueRange(CGXRange(ilRowCount+1,2), omAccountTypeInfoList[i].oName );
			// another row was added
			ilRowCount++;
		}
	}

	// disable update
	pomAccountList->GetParam()->SetLockReadOnly(true);
	pomAccountList->Redraw();

	return (ilAccountSize);
CCS_CATCH_ALL
return 0;
}

//**********************************************************************************
// SortEmployeeGrid: Sort the content of the employee grid.
// Input:	
// Return:	allways true
//**********************************************************************************

bool CInitializeAccountDlg::SortEmployeeGrid(int ipRow)
{
CCS_TRY
	CGXSortInfoArray  sortInfo;
	sortInfo.SetSize( 1 );
	sortInfo[0].sortOrder = CGXSortInfo::ascending;
	sortInfo[0].nRC = ipRow;                       
	sortInfo[0].sortType = CGXSortInfo::autodetect;  
	pomEmplList->SortRows( CGXRange().SetRows(1, omStfData.GetSize()), sortInfo); 

	return true;
CCS_CATCH_ALL
	return false;
}

//**********************************************************************************
// OnSelectAllEmployees: message handler for button 'select all employees'.
// Input:	none
// Return:	none
//**********************************************************************************

void CInitializeAccountDlg::OnSelectAllEmployees() 
{
CCS_TRY
	POSITION area = pomEmplList->GetParam( )->GetRangeList( )->AddTail(new CGXRange);
	pomEmplList->SetSelection(area,1, 1, omStfData.GetSize(),EMPL_COLCOUNT);
CCS_CATCH_ALL
}

//**********************************************************************************
// OnDeselectAllEmployees: message handler for button 'deselect all employees'.
// Input:	none
// Return:	none
//**********************************************************************************

void CInitializeAccountDlg::OnDeselectAllEmployees() 
{
CCS_TRY
	pomEmplList->SetSelection(NULL,0, 0, 0,0);
CCS_CATCH_ALL
}

//**********************************************************************************
// OnFindEmployee: message handler for button 'find employee'.
// Input:	none
// Return:	none
//**********************************************************************************

void CInitializeAccountDlg::OnFindEmployee() 
{
CCS_TRY
	if (pomEmplList	!= NULL)
		pomEmplList->OnShowFindReplaceDialog(TRUE);
CCS_CATCH_ALL
}

//**********************************************************************************
// OnRbDate: message handler for radio button 'date', enables date edit field and
//	disables year edit field.
// Input:	none
// Return:	none
//**********************************************************************************

void CInitializeAccountDlg::OnRbDate() 
{
CCS_TRY
	// Editfeld f�r Eingabe des Enddatums aktivieren
	m_E_Date.EnableWindow(TRUE);
	// Editfeld f�r Eingabe des Jahres deaktivieren
	m_E_Year.EnableWindow(FALSE);
	m_B_Filter.ChangeColor(RED,BLACK,MAROON,RED); 
CCS_CATCH_ALL
}

//**********************************************************************************
// OnMonth: message handler for radio buttons january - december. Enables year 
//	edit field and disables date edit field.
// Input:	none
// Return:	none
//**********************************************************************************

void CInitializeAccountDlg::OnMonth() 
{
CCS_TRY
	// Editfeld f�r Eingabe des Enddatums deaktivieren
	m_E_Date.EnableWindow(FALSE);
	// Editfeld f�r Eingabe des Jahres aktivieren
	m_E_Year.EnableWindow(TRUE);
	m_B_Filter.ChangeColor(RED,BLACK,MAROON,RED); 
CCS_CATCH_ALL
}

//**********************************************************************************
// LoadStringResources: load language specific resources.
// Input:	none
// Return:	none
//**********************************************************************************

void CInitializeAccountDlg::LoadStringResources()
{
CCS_TRY
	SetWindowText(LoadStg(IDS_STRING1854));
	m_stc_Year.SetWindowText(LoadStg(IDS_STRING1852));
	m_S_Org.SetWindowText(LoadStg(IDS_Orgeinheit) + CString(":"));

	//m_stc_Span.SetWindowText(LoadStg(IDS_STRING1853));
	m_stc_Accounts.SetWindowText(LoadStg(IDS_STRING1855));
	m_stc_Employees.SetWindowText(LoadStg(IDS_STRING1856));
	m_stc_TimeSpan.SetWindowText(LoadStg(IDS_STRING1857));
	m_R_Day.SetWindowText(LoadStg(IDS_STRING1853));
	m_R_Month01.SetWindowText(LoadStg(IDS_STRING1859));
	m_R_Month02.SetWindowText(LoadStg(IDS_STRING1860));
	m_R_Month03.SetWindowText(LoadStg(IDS_STRING1861));
	m_R_Month04.SetWindowText(LoadStg(IDS_STRING1862));
	m_R_Month05.SetWindowText(LoadStg(IDS_STRING1863));
	m_R_Month06.SetWindowText(LoadStg(IDS_STRING1864));
	m_R_Month07.SetWindowText(LoadStg(IDS_STRING1865));
	m_R_Month08.SetWindowText(LoadStg(IDS_STRING1866));
	m_R_Month09.SetWindowText(LoadStg(IDS_STRING1867));
	m_R_Month10.SetWindowText(LoadStg(IDS_STRING1868));
	m_R_Month11.SetWindowText(LoadStg(IDS_STRING1869));
	m_R_Month12.SetWindowText(LoadStg(IDS_STRING1870));
	m_b_Ok.SetWindowText(LoadStg(IDS_STRING1875));
	m_b_SelectAll.SetWindowText(LoadStg(IDS_STRING1872));
	m_b_DeselectAll.SetWindowText(LoadStg(IDS_STRING1873));
	m_b_FindEmployee.SetWindowText(LoadStg(IDS_STRING1874));
	m_b_Actualize.SetWindowText(LoadStg(IDS_STRING1871));
	m_R_AccFilterInit.SetWindowText(LoadStg(IDS_STRING1889));
	m_R_AccFilterRecalc.SetWindowText(LoadStg(IDS_STRING1890));
	m_R_AccFilterSaldo.SetWindowText(LoadStg(IDS_STRING1891));
	m_b_Abort.SetWindowText(LoadStg(IDS_STRING1892));
	m_R_Lohnbestandteile.SetWindowText(LoadStg(IDS_STRING1901));
	m_but_OpenJobControl.SetWindowText(LoadStg(IDS_STRING1909));
	m_B_Filter.SetWindowText(LoadStg(IDS_STRING1946));
CCS_CATCH_ALL
}

//**********************************************************************************
// LoadAccountData: load language specific resources.
// Input:	none
// Return:	number of accounts
//**********************************************************************************

int CInitializeAccountDlg::LoadAccountData()
{
CCS_TRY
	// clear the arrays
	omAccountTypeInfoList.DeleteAll();
	omAccountTypeInfoPtrMap.RemoveAll();
	
	ACCOUNTTYPEINFO *prlAccountType =NULL;

	TRACE("\nFound %i Accounts",ogBCD.GetDataCount("ADE"));

	// check all accounts in <ogBCD>
	for (int i=0;i < ogBCD.GetDataCount("ADE"); i++)
	{
//		if (ogBCD.GetField("ADE", i, "SHOW") == "x"){  //Alle anzeigen ARE
			prlAccountType = new ACCOUNTTYPEINFO;
			prlAccountType->iInternNumber	= atol(ogBCD.GetField("ADE", i, "TYPE")); 
			prlAccountType->oExternKey		= ogBCD.GetField("ADE", i, "CODE"); 
			prlAccountType->oName			= ogBCD.GetField("ADE", i, "NAME"); 
			prlAccountType->oShortName		= ogBCD.GetField("ADE", i, "SNAM"); 
			prlAccountType->oFormat			= ogBCD.GetField("ADE", i, "FORM"); 
			prlAccountType->oScbi			= ogBCD.GetField("ADE", i, "SCBI"); 
			prlAccountType->oScbr			= ogBCD.GetField("ADE", i, "SCBR"); 
			prlAccountType->oScbs			= ogBCD.GetField("ADE", i, "SCBS"); 
			omAccountTypeInfoList.Add(prlAccountType);
			omAccountTypeInfoPtrMap.SetAt((void *)prlAccountType->iInternNumber,prlAccountType);
//		}
	}
	return omAccountTypeInfoList.GetSize();
CCS_CATCH_ALL
	return -1;
}

//**********************************************************************************
// OnAccFilter: message handler for filter radio buttons, only accounts
//	with a value for field SCBI, SCBR or SCBS will be shown.
// Input:	none
// Return:	none
//**********************************************************************************

void CInitializeAccountDlg::OnAccFilter() 
{
	TRACE("CInitializeAccountDlg::OnAccFilter()\n");
	// enable account grid
	pomAccountList->EnableWindow(true);
	// later execution enabled
	m_R_Later.EnableWindow(true);
	// if monthly execution was checked, check immediate execution
	m_R_Now.SetCheck(true);
	imExecutionMode = EXECUTION_NOW;

	m_R_Day.EnableWindow(true);

	FillAccountGrid();

	m_B_Filter.ChangeColor(RED,BLACK,MAROON,RED);
}

//**********************************************************************************
// OnOK: message handler for button 'finish'.
// Input:	none
// Return:	none
//**********************************************************************************

void CInitializeAccountDlg::OnOK() 
{
	// let the user acknowledge 
	if (MessageBox(LoadStg(IDS_STRING1907),"InitializeAccount",MB_YESNO | MB_ICONQUESTION) == IDNO)
	{
		return;
	}

	// destroy grid window
	pomEmplList->DestroyWindow();
	CDialog::OnOK();
}

//**********************************************************************************
// CheckInfoForMonthlyJob: check, if the first execution time for a monthly 
//	executed job is valid. The first execution time is calculated using the
//	start time of the time span for scripts adding the month offset and using 
//	the day and time values from CEDA.INI. The resulting time must be in future.
// Input:	COleDateTime opStart		-> start of evaluation time span for scripts
//			COleDateTime &opFirstExec	-> stores the calculated frist execution
//			int ipMonthOffset			-> the offset of execution from the 
//											evaluation time span
// Return:	bool settings ok?
//**********************************************************************************

bool CInitializeAccountDlg::CheckInfoForMonthlyJob(COleDateTime opStart, 
												   COleDateTime &opFirstExec,
												   int ipMonthOffset)
{
CCS_TRY
	// parameters to construct first exec time
	int ilYear, ilMonth, ilDay, ilHour, ilMinute, ilCalcLastDayOfMonth = 0;
	COleDateTime olTimeNow = COleDateTime::GetCurrentTime();
	CString strMsg;

	// if user did not chose monthly execution, no further action is required
	if ((imExecutionMode != EXECUTION_MONTHLY))
	{
		// settings are ok, don't do anything but terminate
		TRACE("CheckInfoForMonthlyJob(): exec-mode is not monthly, returning true.\n");
		return true;
	}
	
	// now construct the first exec time using the user settings
	ilYear = opStart.GetYear();
	ilMonth = opStart.GetMonth();
	ilDay = atoi(LPCTSTR(omStartMonthlyTimeAndDate.Left(2)));
	ilHour = atoi(LPCTSTR(omStartMonthlyTimeAndDate.Mid(2,2)));
	ilMinute = atoi(LPCTSTR(omStartMonthlyTimeAndDate.Right(2)));
	
	// do we have to calculate the last valid day of month?
	if (ilDay >= 29)
	{
		ilCalcLastDayOfMonth = 1;
		ilDay = 1;
	}
	TRACE("CheckInfoForMonthlyJob(): <ilCalcLastDayOfMonth> = %d\n",ilCalcLastDayOfMonth);

	// set up month
	ilMonth += (ipMonthOffset + ilCalcLastDayOfMonth);
	// did we enter a year before?
	if (ilMonth <= 0)
	{
		// yes -> correct values
		ilMonth += 12;
		ilYear--;
	}
	else if (ilMonth > 12) // did we enter the next year?
	{
		// yes -> correct values
		ilMonth -= 12;
		ilYear++;
	}

	TRACE("CheckInfoForMonthlyJob(): constructing check date:\n",ilCalcLastDayOfMonth);
	TRACE("CheckInfoForMonthlyJob(): <ilYear>	= %d\n",ilYear);
	TRACE("CheckInfoForMonthlyJob(): <ilMonth>	= %d\n",ilMonth);
	TRACE("CheckInfoForMonthlyJob(): <ilDay>	= %d\n",ilDay);
	TRACE("CheckInfoForMonthlyJob(): <ilHour>	= %d\n",ilHour);
	TRACE("CheckInfoForMonthlyJob(): <ilMinute>	= %d\n",ilMinute);

	// construct time 
	COleDateTime olCheckTime(ilYear,ilMonth,ilDay,ilHour,ilMinute,0);
	// substract one day to get last day of prior month?
	if (ilCalcLastDayOfMonth)
	{
		TRACE("CheckInfoForMonthlyJob(): correcting check date\n");
		COleDateTimeSpan olOneDay(1,0,0,0);
		olCheckTime -= olOneDay;
	}
	// copy to first exec time 
	opFirstExec = olCheckTime;
	TRACE("CheckInfoForMonthlyJob(): first execution at %s!\n",opFirstExec.Format("%Y%m%d, %H:%M"));

	// now add one hour to current time (in case the handler is busy)
	COleDateTimeSpan olOneHour(0,1,0,0);
	olTimeNow += olOneHour;

	// now check, if first exec time is future
	if (opFirstExec > olTimeNow)
	{
		// everything ok, prompt the date and ask user if it's ok
		MessageBeep((UINT)-1);
		strMsg.Format(LoadStg(IDS_STRING1904),opFirstExec.Format("%d.%m.%Y, %H:%M"));
		if (MessageBox(strMsg,LoadStg(IDS_STRING1905),MB_ICONQUESTION|MB_YESNO) == IDNO)
			return false;
		else 
			return true;
	}
	else
	{
		// message 'please correct your settings...', return false
		MessageBeep((UINT)-1);
		strMsg.Format(LoadStg(IDS_STRING1906),opFirstExec.Format("%d.%m.%Y, %H:%M"));
		MessageBox(strMsg,LoadStg(IDS_STRING1905),MB_ICONEXCLAMATION);
		return false;
	}
CCS_CATCH_ALL
return false;
}

//**********************************************************************************
// OnBActualize: message handler for button 'Actualize'. Extract selections 
//  and send broadcasts to XBSHDL, which will then call the appropriate
//  basic scripts.
// Input:	none
// Return:	none
//**********************************************************************************

void CInitializeAccountDlg::OnBActualize() 
{
CCS_TRY
	CStringArray olEmployeeUrnos, olCommands;
	COleDateTime olStart, olEnd, olFirstExec;
	CString olStartForBc, olEndForBc, olBcSel, 
			olBcAdditionalInfoData, olBcAdditionalInfoFields, 
			olMonthOffset, olFormatMonthOffset, olBcNum;
	int ilEmplCount, ilCommandCount, ilEmplLeft, ilMonthOffset, ilBcNum,
		ilCurrentNoOfEmplInBCList;
	LPMSG olMsg = NULL;
	bool blBcError; // triggers break in broadcast sending
	CString olCommandString = "";

	// show wait cursor
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	// get month offset and convert �t 
	ilMonthOffset = atoi(olMonthOffset.GetBuffer(0));
	if (ilMonthOffset < 0)
		olFormatMonthOffset.Format("%.2d",ilMonthOffset);
	else
		olFormatMonthOffset.Format("+%.2d",ilMonthOffset);

	// get selected accounts, employees and date input
	if (!GetSelectedAccountCommands(&olCommands) ||
		!GetSelectedEmployees(&olEmployeeUrnos) ||
		!GetAndCheckTimeSpan(olStart,olEnd) || 
		!CheckInfoForMonthlyJob(olStart,olFirstExec,ilMonthOffset)) // ATTENTION!!! <olStart> must be valid already.
	{
		// erroneous or missing input -> terminate (functions already displayed error messages)
		return;
	}

	// create date strings for broadcast
	olStartForBc = olStart.Format("%Y%m%d");
	olEndForBc = olEnd.Format("%Y%m%d");

	// compose broadcasts: one bc per employee and script
	// counter for progress bar
	ilEmplCount = olEmployeeUrnos.GetSize();
	ilCommandCount = olCommands.GetSize();

	// initialize broadcast counter (needed by some scripts if there will be more than on bc)
	ilBcNum = 1;

	// disable and hide 'Actualize' button
	m_b_Actualize.EnableWindow(false);
	m_b_Actualize.ShowWindow(SW_HIDE);
	// enable abort button
	m_b_Abort.EnableWindow(true);
	m_b_Abort.ShowWindow(SW_SHOW);
	// reset abortion trigger
	bmUserAbort = false;
	// reset break trigger in case bc sending fails
	blBcError = false;

	//
	// new: sending one event for the accounts to be initialized
	//
	olBcSel = "";
	ilEmplLeft = ilEmplCount;
	ilCurrentNoOfEmplInBCList = 0;

	// collecting the accounts to be initialized in one string
	for (int i = 0; i < ilCommandCount; i++)
	{
		olCommandString = olCommandString + olCommands[i] + "|";
	}
	
	// setting the fields
	olBcAdditionalInfoFields = "FROM,TO,MODE,TIME,DAY,OFFSET,FIRSTEXEC,BCNUM,INIT";

	// setting the data
	if ((imExecutionMode == EXECUTION_MONTHLY))
	{
		olBcAdditionalInfoData.Format("%s,%s,%.1d,%s,%s,%s,%s,",olStartForBc,olEndForBc,imExecutionMode,omStartMonthlyTimeAndDate.Right(4),omStartMonthlyTimeAndDate.Left(2),olFormatMonthOffset,olFirstExec.Format("%Y%m%d%H%M"));
	}
	else
	{
		// use time of night job with current day, no day-, and offset-info necessary
		olFirstExec = COleDateTime::GetCurrentTime();
		olFirstExec.SetDateTime(COleDateTime::GetCurrentTime().GetYear(),
								COleDateTime::GetCurrentTime().GetMonth(),
								COleDateTime::GetCurrentTime().GetDay(),
								atoi(LPCTSTR(omStartNightJobAt.Left(2))),
								atoi(LPCTSTR(omStartNightJobAt.Right(2))),0);
		// if this moment has passed add one day 
		if (olFirstExec < COleDateTime::GetCurrentTime()) 
		{
			olFirstExec += COleDateTimeSpan(1,0,0,0);
		}
		// create info string
		olBcAdditionalInfoData.Format("%s,%s,%.1d,%s,00,+00,%s,",olStartForBc,olEndForBc,imExecutionMode,omStartNightJobAt,olFirstExec.Format("%Y%m%d%H%M"));
	}

	// create employee urno list and send bc(s)
	while ((ilEmplLeft > 0) && !blBcError)
	{
		// next employee urno (reverse order) to string list
		olBcSel += olEmployeeUrnos[ilEmplLeft-1];
		olBcSel += ",";
		ilEmplLeft--;

		// if buffer length is at maximum or last employee urno used or max. employee urnos in buffer
		if ((olBcSel.GetLength() > MAX_BC_SELECTION_SIZE-10+1) || (ilEmplLeft == 0))
		{
			// send bc now
			olBcNum.Format("%d,%s",ilBcNum,olCommandString);

			//Translation:
			// opCmd = TWS
			// opData = Selection
			// opAdditionalInfoFields = Fields
			// opAdditionalInfoData = Data
			// ==>       TWS        Sel.     Fields                    Data
			if (!SendBC("INIT_MUL", olBcSel, olBcAdditionalInfoFields, olBcAdditionalInfoData + olBcNum))
			{
				blBcError = true;
			}

			// reset buffer
			olBcSel = "";

			// increase ordinal bc number
			ilBcNum++;
		}
	}

	//
	// old: sending init-commands for every single account
	//
	// send one bc per account command
/*	
	int ilPercent;
	for (int x = 0; x < ilCommandCount; x++)
	{
		// inform the user what's happening
		pomStatusBar->SetStaticText(1, LoadStg(IDS_STRING1887));
		// set progress bar
		ilPercent = (100*(x+1))/ilCommandCount;
		pomStatusBar->SetProgressPos(imIDProgress,ilPercent);
		// reset data
		olBcSel = "";
		ilEmplLeft = ilEmplCount;
		ilCurrentNoOfEmplInBCList = 0;
		// create normal info string
		olBcAdditionalInfoFields = "FROM,TO,MODE,TIME,DAY,OFFSET,FIRSTEXEC,BCNUM";
		// check execution mode: use <omStartNightJobAt> for time info if mode is 
		// later execution of non-Lohnbestandteile (autsch!)
		if ((imExecutionMode == EXECUTION_MONTHLY))
		{
			olBcAdditionalInfoData.Format("%s,%s,%.1d,%s,%s,%s,%s,",olStartForBc,olEndForBc,imExecutionMode,omStartMonthlyTimeAndDate.Right(4),omStartMonthlyTimeAndDate.Left(2),olFormatMonthOffset,olFirstExec.Format("%Y%m%d%H%M"));
		}
		else
		{
			// use time of night job with current day, no day-, and offset-info necessary
			olFirstExec = COleDateTime::GetCurrentTime();
			olFirstExec.SetDateTime(COleDateTime::GetCurrentTime().GetYear(),
									COleDateTime::GetCurrentTime().GetMonth(),
									COleDateTime::GetCurrentTime().GetDay(),
									atoi(LPCTSTR(omStartNightJobAt.Left(2))),
									atoi(LPCTSTR(omStartNightJobAt.Right(2))),0);
			// if this moment has passed add one day 
			if (olFirstExec < COleDateTime::GetCurrentTime()) 
			{
				olFirstExec += COleDateTimeSpan(1,0,0,0);
			}
			// create info string
			olBcAdditionalInfoData.Format("%s,%s,%.1d,%s,00,+00,%s,",olStartForBc,olEndForBc,imExecutionMode,omStartNightJobAt,olFirstExec.Format("%Y%m%d%H%M"));
		}
		TRACE("\nadditional parameter for broadcast: %s",olBcAdditionalInfoData);

		if ((m_R_Lohnbestandteile.GetCheck() == 1))
		{
			// the script will select the employees itself -> send only one bc
			// convert ordinal bc number to string
			olBcNum.Format("%d",ilBcNum);
			// send bc
			SendBC(olCommands[x],"*",olBcAdditionalInfoFields,olBcAdditionalInfoData+olBcNum);
		}
		else
		{
			// create employee urno list and send bc(s)
			while ((ilEmplLeft > 0) && !blBcError)
			{
				// next employee urno (reverse order) to string list
				olBcSel += olEmployeeUrnos[ilEmplLeft-1];
				olBcSel += ",";
				ilEmplLeft--;
				ilCurrentNoOfEmplInBCList++;
				// if buffer length is at maximum or last employee urno used or 
				// max. employee urnos in buffer
				if ((olBcSel.GetLength() > MAX_BC_SELECTION_SIZE-10+1) ||
					(ilEmplLeft == 0) ||
					(ilCurrentNoOfEmplInBCList == imMaxEmplUrnosPerBC))
				{
					// yes -> send bc now
					// convert ordinal bc number to string
					olBcNum.Format("%d",ilBcNum);
					// debug output (cause olBcSel is too large for using with TRACE(...))
					TRACE("\nOnBActualize(): sending bc number <%d>...",ilBcNum);
					// APO 2001.07.27: SendBC allways returns 1 now (see SendBC for details)
					if (!SendBC(olCommands[x],olBcSel,olBcAdditionalInfoFields,olBcAdditionalInfoData+olBcNum)) blBcError = true;
					// reset buffer and counter
					olBcSel = "";
					ilCurrentNoOfEmplInBCList = 0;
					// increase ordinal bc number
					ilBcNum++;
				}
				// take a look at the message queue, else we might miss a user abortion
				olMsg = NULL;
				while (PeekMessage(olMsg, NULL, 0, 0, PM_REMOVE))
				{
					if (olMsg->message == WM_QUIT) break;
					TranslateMessage(olMsg);         
					DispatchMessage(olMsg);      
				}	
			}
		}
	}

	SendCommand(CString("REFILL"), CString("XBS"));*/
	
	// hide abort button
	m_b_Abort.EnableWindow(false);
	m_b_Abort.ShowWindow(SW_HIDE);
	// reset abortion trigger
	bmUserAbort = false;
	// enable and show 'Actualize' button
	m_b_Actualize.EnableWindow(true);
	m_b_Actualize.ShowWindow(SW_SHOW);

	// reset progress bar
	pomStatusBar->SetProgressPos(imIDProgress,0);
	// inform the user what's happening
	pomStatusBar->SetStaticText(1, LoadStg(IDS_STRING1888));

	// cleanup
	olCommands.RemoveAll();
	olEmployeeUrnos.RemoveAll();

	// show arrow mouse pointer
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
CCS_CATCH_ALL
}

//*********************************************************************************************************************
// GetSelectedEmployees: fill list with selected staff urnos.
// Input:	
// Return:	TRUE	-> everything is ok
//			FALSE	-> no employee is selected
//*********************************************************************************************************************

bool CInitializeAccountDlg::GetSelectedEmployees(CStringArray *popEmployeeUrnos)
{
CCS_TRY
	CGXStyle		olStyle;
	long			ilUrno;
	CString			olUrno;
	CRowColArray	olRowColArray;
	CString			strFormattedError;

	if ((m_R_Lohnbestandteile.GetCheck() == 1))
	{
		return true;
	}

	// remove all entrys from list
	popEmployeeUrnos->RemoveAll();
		
	// read selection from grid
	pomEmplList->GetSelectedRows(olRowColArray,false,false);

	// are there any selected items?
	if (olRowColArray.GetSize() == 0)
	{
		// no -> do error message and terminate
		TRACE ("CInitializeAccountDlg::GetSelectedEmployees(): Keine Mitarbeiter ausgew�hlt.\n");
		MessageBeep((UINT)-1);
		MessageBox(LoadStg(IDS_STRING1877),LoadStg(IDS_STRING1876),MB_ICONEXCLAMATION);
		return false;
	}
	
	// get urnos from grid selection
	for (int i=0;i<olRowColArray.GetSize();i++)
	{
		// get style to get data pointer
		pomEmplList->ComposeStyleRowCol(olRowColArray[i],1, &olStyle );
		// get pointer
		ilUrno = (long)olStyle.GetItemDataPtr();
		if (!ilUrno)
		{
			TRACE ("CInitializeAccountDlg::GetSelectedEmployees(): Urno=0 in Zeile %d\n",olRowColArray[i]);
			MessageBeep((UINT)-1);
			strFormattedError.Format(LoadStg(IDS_STRING1879),olRowColArray[i]);
			MessageBox(strFormattedError,LoadStg(IDS_STRING1881),MB_ICONEXCLAMATION);
			return false;
		}
		olUrno.Format("%d",ilUrno);
		// add to string list
		popEmployeeUrnos->Add(olUrno);
	}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************************************
// GetSelectedAccountCommands: fill list with selected account commands. Each command is stored only
//	one time even if there are more than one selected accounts with the same commands.
// Input:	CStringList *popCommands -> the list which stores the commands
// Return:	TRUE	-> everything is ok
//			FALSE	-> no employee is selected
//*********************************************************************************************************************

bool CInitializeAccountDlg::GetSelectedAccountCommands(CStringArray *popCommands)
{
CCS_TRY
	CGXStyle		olStyle;
	long			ilInternalNumber;
	CString			olCommand;
	CRowColArray	olRowColArray;
	CString			strFormattedError;
	bool			blAccountFound, blCommandFound;
	// number of known accounts
	int ilAccountSize = omAccountTypeInfoList.GetSize();

	// remove all entrys from list
	popCommands->RemoveAll();
		
	// if radio button 'Lohnbestandteile' is checked, we have only one command 
	// to send
	if (m_R_Lohnbestandteile.GetCheck() == 1)
	{
		// add one and only command and terminate function
		popCommands->Add(omCmdLohnbestandteile);
		return true;
	}
	// same for recaculation
	else if (m_R_AccFilterRecalc.GetCheck() == 1)
	{
		// add one and only command and terminate function
		popCommands->Add(omCmdRecalculation);
		return true;
	}

	// read selection from grid
	pomAccountList->GetSelectedRows(olRowColArray,false,false);

	// are there any selected items?
	if (olRowColArray.GetSize() == 0)
	{
		// no -> terminate
		TRACE ("CInitializeAccountDlg::GetSelectedAccounts(): Keine Konten ausgew�hlt.\n");
		MessageBeep((UINT)-1);
		MessageBox(LoadStg(IDS_STRING1878),LoadStg(IDS_STRING1876),MB_ICONEXCLAMATION);
		return false;
	}
	
	// get internal numbers from grid selection
	for (int i=0;i<olRowColArray.GetSize();i++)
	{
		// get style to get data pointer
		pomAccountList->ComposeStyleRowCol(olRowColArray[i],1, &olStyle );
		// get pointer
		ilInternalNumber = (long)olStyle.GetItemDataPtr();
		if (!ilInternalNumber)
		{
			TRACE ("CInitializeAccountDlg::GetSelectedAccounts(): ilInternalNumber=0 in Zeile %d\n",olRowColArray[i]);
			MessageBeep((UINT)-1);
			strFormattedError.Format(LoadStg(IDS_STRING1880),olRowColArray[i]);
			MessageBox(strFormattedError,LoadStg(IDS_STRING1881),MB_ICONEXCLAMATION);
			return false;
		}
//		olInternalNumber.Format("%d",ilInternalNumber);

		// reset loop data
		blAccountFound = false;
		blCommandFound = false;
		olCommand = "";

		// search account in list of all accounts to get command
		for (int i=0; (i<ilAccountSize) && !blAccountFound; i++)
		{
			// account found?
			if (omAccountTypeInfoList[i].iInternNumber == ilInternalNumber)
			{
				// yes -> get command from field according to current filter 
				// (init -> use field ADETAB.SCBI, recalculate -> use ADETAB.SCBR, 
				//  or saldo -> use ADETAB.SCBS)
				if ((m_R_AccFilterInit.GetCheck() == 1) && (omAccountTypeInfoList[i].oScbi != ""))
				{
					// filter is init
					olCommand = omAccountTypeInfoList[i].oScbi;
					blAccountFound = true;
				}
				else if ((m_R_AccFilterRecalc.GetCheck() == 1) && (omAccountTypeInfoList[i].oScbr != ""))
				{
					// filter is recalculate
					olCommand = omAccountTypeInfoList[i].oScbr;
					blAccountFound = true;
				}
				else if ((m_R_AccFilterSaldo.GetCheck() == 1) && (omAccountTypeInfoList[i].oScbs != ""))
				{
					// filter is saldo
					olCommand = omAccountTypeInfoList[i].oScbs;
					blAccountFound = true;
				}
				
				// command must have been found
				ASSERT(olCommand != "");
				
				// check, if command is already in list
				for (int k=0; (k<popCommands->GetSize()) && !blCommandFound; k++)
				{
					if (popCommands->GetAt(k) == olCommand) blCommandFound = true;
				}
				
				// command already in list?
				if (!blCommandFound)
				{
					// no -> add to list
					popCommands->Add(olCommand);
				}
			}
		}
	}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************************************
// GetSelectedEmployees: fill list with selected staff urnos.
// Input:	
// Return:	TRUE	-> everything is ok
//			FALSE	-> no employee is selected
//*********************************************************************************************************************

bool CInitializeAccountDlg::GetAndCheckTimeSpan(COleDateTime &opFrom, COleDateTime &opTo)
{
CCS_TRY
	// 'actualize to date...' selected?
	if(m_R_Day.GetCheck() == 1)
	{
		// yes -> entered date valid?
		if(m_E_Date.GetStatus() == false)
		{
			// no -> do error message and terminate
			MessageBeep((UINT)-1);
			MessageBox(LoadStg(IDS_STRING1882),LoadStg(IDS_STRING1876),MB_ICONEXCLAMATION);
			m_E_Date.SetSel(0,-1);
			m_E_Date.SetFocus();
			return false;
		}
		else	// date is valid
		{
			// buffer for conversion
			CString olTmp;
			// get the date as string from edit control...
			m_E_Date.GetWindowText(olTmp);
			// ...and convert it to a COleDateTime object
			opTo = OleDateStringToDate(olTmp);
			// now setup the start date: 1.1. of selected year
			opFrom.SetDate(opTo.GetYear(),1,1);
		}
	}
	else	// month is selected
	{
		// check the specified year
		if(m_E_Year.GetStatus() == false)
		{
			// erroneous input -> do error message and terminate
			MessageBeep((UINT)-1);
			MessageBox(LoadStg(IDS_STRING1883),LoadStg(IDS_STRING1876),MB_ICONEXCLAMATION);
			m_E_Year.SetSel(0,-1);
			m_E_Year.SetFocus();
			return false;
		}
		else
		{
			// convert year to COleDateTime - object
			CString olTmp;
			m_E_Year.GetWindowText(olTmp);
			int ilTmpToYear = atoi(olTmp);
			int ilTmpToMonth = 0;
			// get the finishing month: to detect the last day
			// in the selected month we add one month to the selected and
			// then substract one day, so we don't have to care about
			// if the month has 28, 30 or 31 days.
			if(m_R_Month01.GetCheck() == 1)
			{
				ilTmpToMonth = 2;
			}
			else if(m_R_Month02.GetCheck() == 1)
			{
				ilTmpToMonth = 3;
			}
			else if(m_R_Month03.GetCheck() == 1)
			{ 
				ilTmpToMonth = 4;
			}
			else if(m_R_Month04.GetCheck() == 1)
			{
				ilTmpToMonth = 5;
			}
			else if(m_R_Month05.GetCheck() == 1)
			{
				ilTmpToMonth = 6;
			}
			else if(m_R_Month06.GetCheck() == 1)
			{
				ilTmpToMonth = 7;
			}
			else if(m_R_Month07.GetCheck() == 1)
			{
				ilTmpToMonth = 8;
			}
			else if(m_R_Month08.GetCheck() == 1)
			{
				ilTmpToMonth = 9;
			}
			else if(m_R_Month09.GetCheck() == 1)
			{
				ilTmpToMonth = 10;
			}
			else if(m_R_Month10.GetCheck() == 1)
			{
				ilTmpToMonth = 11;
			}
			else if(m_R_Month11.GetCheck() == 1)
			{
				ilTmpToMonth = 12;
			}
			else if(m_R_Month12.GetCheck() == 1)
			{
				// special case december: increment the year,
				// for we have to evaluate til 1.1.year+1 
				ilTmpToMonth = 1;
				ilTmpToYear++;
			}
			// ASSERT: valid month selected
			ASSERT(ilTmpToMonth > 0);
			// setup finishing date
			opTo.SetDate(ilTmpToYear,ilTmpToMonth, 1);
			// substract one day
			opTo -= COleDateTimeSpan(1,0,0,0);
			// now setup the start date: 1. of selected month from finishing date
			opFrom.SetDate(opTo.GetYear(),opTo.GetMonth(),1);
		}
	}
	// eyerything's fine
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************************************
// SendBC: send a broadcast to XBSHDL.
// Input: CString <opData>: the data string
// Return:	TRUE	-> everything is ok
//			FALSE	-> error
//*********************************************************************************************************************

bool CInitializeAccountDlg::SendBC(CString opCmd, 
								   CString opData,
								   CString opAdditionalInfoFields,
								   CString opAdditionalInfoData)
{
    static char cReq_id[22];     
    static char cUser[12];					// normally CEDA
    static char cWks[12] = "";				// workstation
    static char cAction[12] = "XBS"; 
    static char cTable[512] = "";			// table Name (multiple table are allowed)
    static char cSortClause[512] = "";		// sort Clause    
    static char cTws[MAX_BC_TW_SIZE] = "0";	// time Window Start 
    static char cTwe[MAX_BC_TW_SIZE];		// time Window end
	// selection criterium, normally a WHERE clause
    static char cSelection[MAX_BC_SELECTION_SIZE];
	static char cFields[MAX_BC_FIELDS] = "";// DB field, normally database fields
	static char cData[MAX_BC_DATA] = "";	// response buffer 
    static char cDataDest[12] = "RETURN";	// RETURN or BUF1 .. BUF4
	int iLastReturnCode;
	CString strError;

	strcpy(cWks,ogCommHandler.pcmReqId);
	strcpy(cTable,"SBC");			// dummy table name needed for broadcast !
	strcat(cTable,pcgTableExt);		// append correct table extension

	// user specific info
	strcpy(cUser,pcgUser);
	sprintf(cTwe,"%s,%s,%s",pcgHome,pcgTableExt,pcgAppl);

	// if recalculation set command to 'XBS2'
	if(m_R_AccFilterRecalc.GetCheck() == 1)
	{
	    strcpy(cAction,"XBS2");
		strcpy(cTws,"RECALC");
	}
	else
	{
		strcpy(cAction,"XBS");
		strcpy(cTws,opCmd);
	}

	// check command length
	if (opCmd.GetLength() >= MAX_BC_TW_SIZE)
	{
		// command is too long -> do error message and terminate
		MessageBox(LoadStg(IDS_STRING1893),LoadStg(IDS_STRING1881),MB_ICONEXCLAMATION);
		return false;
	}

	// check length of data string
	ASSERT(opData.GetLength() <= MAX_BC_SELECTION_SIZE);
	// copy the data
	strcpy(cSelection,opData.GetBuffer(0));

	// check length of additional info field list
	ASSERT(opAdditionalInfoFields.GetLength() <= MAX_BC_FIELDS);
	// copy additional info to the broadcasts fieldlist
	strcpy(cFields,opAdditionalInfoFields.GetBuffer(0));
	// check length of additional info data
	ASSERT(opAdditionalInfoFields.GetLength() <= MAX_BC_DATA);
	// copy additional info to the broadcasts data buffer
	strcpy(cData,opAdditionalInfoData.GetBuffer(0));


	// inform the user
	pomStatusBar->SetStaticText(1, LoadStg(IDS_STRING1887));

	// in case the response takes time
	AfxGetApp()->DoWaitCursor(1);
	TRACE("\nCInitializeAccountDlg::SendBC() calling ::CallCeda() ...\n");
	if ((iLastReturnCode = ::CallCeda(cReq_id,cUser,cWks,cAction,cTable,cSortClause,
									  cTws,cTwe,cSelection,cFields,cData,cDataDest)) != 0)
	{
		// APO 2001/07/27: ignore timeout errors. this is because the handler can't 
		// respond to CallCeda() in time if we sent more than about 80 urnos and we have
		// a total number of more than about 160 urnos, cause the handler will be 
		// busy executing a script 80 times. So the second bc will not be answered in time. 
		// Since there is no way just to put our broadcasts in the queue without waiting
		// for an answer we only have two ways to handle the problem:
		// 1.) make the value for the maximum number of urnos to be sended with one bc
		//     little enough to let the handler respond in time
		// 2.) ignore timeout errors, which is the chosen way.
		if (iLastReturnCode != -4)
		{
			MessageBeep((UINT)-1);
			strError.Format(LoadStg(IDS_STRING1885),cData);
			MessageBox(strError,LoadStg(IDS_STRING1884),MB_ICONEXCLAMATION);
			// restore normal state
			pomStatusBar->SetStaticText(1, LoadStg(IDS_STRING1459));
			return false;
		}
		else
		{
			TRACE("\nSendBC: timeout error!!!\n");
		}
	}
	TRACE("\nCInitializeAccountDlg::SendBC() bc sended\n");
	// switch off the wait cursor
	AfxGetApp()->DoWaitCursor(-1);

	// check the return buffer for errors (ignore if timeout error occurred)
	if ((*cData != '\0') && (strcmp(cData,"OK") && (iLastReturnCode != -4)))
	{
		// an error occurred -> show message
		MessageBeep((UINT)-1);
		strError.Format(LoadStg(IDS_STRING1886),cData);
		MessageBox(strError,LoadStg(IDS_STRING1884),MB_ICONEXCLAMATION);
	}

	// restore normal state
	pomStatusBar->SetStaticText(1, LoadStg(IDS_STRING1459));
	
	return true;
}

//**********************************************************************************
// OnButtonAbort: message handler for button 'Abort'. A boolean is set to true,
//	so OnBActualize can detect a user abortion.
// Input:	none
// Return:	none
//**********************************************************************************

void CInitializeAccountDlg::OnButtonAbort() 
{
	// trigger user abortion
	bmUserAbort = true;
}

//**********************************************************************************
// ProcessChar: check key input for password for secret dialog.
// Input:	event data 
// Return:	none
//**********************************************************************************

void CInitializeAccountDlg::ProcessChar(UINT nChar) 
{
	// counter which is incremented only if correct password is entered
	static int nKeyCount = 0;

	TRACE("CInitializeAccountDlg::OnKeyDown(): key = %c\n",nChar);
	
	// check pressed key
	if ((nChar == 'x') && (nKeyCount == 0) ||
		(nChar == 'b') && (nKeyCount == 1) ||
		(nChar == 's') && (nKeyCount == 2) ||
		(nChar == 'h') && (nKeyCount == 3) ||
		(nChar == 'd') && (nKeyCount == 4) ||
		(nChar == 'l') && (nKeyCount == 5))
	{
		if (nKeyCount == 5) 
		{
			// secret password complete -> show secret dialog
//			ShowMKBINDialog();
			nKeyCount = 0;
		}
		else
		{
			// increment correct key counter
			nKeyCount++;
		}
	}
	else
	{
		// reset counter
		nKeyCount = 0;
	}
}

//******************************************************************************************
// ARE 20.11.00
// FillOrgCombo(): initial filling of ORG-filter combo box
// Input:	none
// Return:	none
//******************************************************************************************

void CInitializeAccountDlg::FillOrgCombo()
{
CCS_TRY
	CString olLine;
	// Eine leeren Zeile einf�gen, f�r alle MA's
	olLine.Format("           %-40s     URNO=ALL_EMPLOYEE", LoadStg(IDS_AlleMittarbeiter));
	m_C_Orgeinheit.AddString(olLine);
	// Eine Zeile einf�gen, f�r alle MA's die keine Funktion haben
	olLine.Format("--------   %-40s     URNO=NO_ORGCODE", LoadStg(IDS_OhneOrgCode));
	m_C_Orgeinheit.AddString(olLine);


	RecordSet *polRecord;
	polRecord = new RecordSet(ogBCD.GetFieldCount("ORG"));

	int ilDpt1Idx = ogBCD.GetFieldIndex("ORG","DPT1");
	int ilDptnIdx = ogBCD.GetFieldIndex("ORG","DPTN");
	int ilUrnoIdx = ogBCD.GetFieldIndex("ORG","URNO");

	for(int i = 0; i < ogBCD.GetDataCount("ORG"); i++)
	{
		ogBCD.GetRecord("ORG",i, *polRecord);
		olLine.Format("%-8s   %-40s     URNO=%s", polRecord->Values[ilDpt1Idx], polRecord->Values[ilDptnIdx], polRecord->Values[ilUrnoIdx]);
		m_C_Orgeinheit.AddString(olLine);
	}
	delete polRecord;

	// default selection: all employees
	m_C_Orgeinheit.SetCurSel(0);
CCS_CATCH_ALL	
}

//******************************************************************************************
// ARE 20.11.00
// OnSelchange_C_Org(): the user changed the ORG-filter -> set red color of filter button.
// Input:	none
// Return:	none
//******************************************************************************************

void CInitializeAccountDlg::OnSelchange_C_Org() 
{
	m_B_Filter.ChangeColor(RED,BLACK,MAROON,RED); 
}

//******************************************************************************************
// ARE 20.11.00
// OnUpdateDate(): the user changed the time settings -> set color of filter button to red, 
//	employee grid has to be re-filled.
// Input:	none
// Return:	none
//******************************************************************************************/

void CInitializeAccountDlg::OnUpdateDate() 
{
	m_B_Filter.ChangeColor(RED,BLACK,MAROON,RED); 
}

//******************************************************************************************
// ARE 20.11.00
// OnUpdateYear(): the user changed the time settings -> set color of filter button to red, 
//	employee grid has to be re-filled.
// Input:	none
// Return:	none
//******************************************************************************************/

void CInitializeAccountDlg::OnUpdateYear() 
{
	m_B_Filter.ChangeColor(RED,BLACK,MAROON,RED); 
}

//******************************************************************************************
// APO 30.11.00
// OnRadioNow(): the user changed the time of execution to immediate execution.
// Input:	none
// Return:	none
//******************************************************************************************/

void CInitializeAccountDlg::OnRadioNow() 
{
	imExecutionMode = EXECUTION_NOW;
}

//******************************************************************************************
// APO 30.11.00
// OnRadioNow(): the user changed the time of execution to execution at night.
// Input:	none
// Return:	none
//******************************************************************************************/

void CInitializeAccountDlg::OnRadioLater() 
{
	imExecutionMode = EXECUTION_LATER;
}

//******************************************************************************************
// APO 30.11.00
// OnRadioMonthly(): the user changed the time of execution to monthly execution.
// Input:	none
// Return:	none
//******************************************************************************************/

void CInitializeAccountDlg::OnRadioMonthly() 
{
	imExecutionMode = EXECUTION_MONTHLY;	
}

//******************************************************************************************
// APO 30.12.00
// OnRbLohnbestandteile(): Lohnbestandteile are to be calculated. In this case we 
//	have to hide the account grid because the user has not to select any account.
//	Lohnbestandteile are virtual accounts, there is no such data in the database.
//	Instead they have to be configured in the handlers initialization script.
//	To execute this command we send the hardcoded XBS-command for 'lohnbestandteile'
//	(which is configurated in CEDA.INI and 'INIT_LB' by default) which
//	must be configured in the handlers init-script by calling  AddXBSCommand().
// Input:	none
// Return:	none
//******************************************************************************************/

void CInitializeAccountDlg::OnRbLohnbestandteile() 
{
CCS_TRY
	return;
	/*
	TRACE("CInitializeAccountDlg::OnRbLohnbestandteile()\n");

	// disable later execution
	if (m_R_Later.GetCheck() == 1)
	{
		m_R_Monthly.SetCheck(true);
		imExecutionMode = EXECUTION_MONTHLY;
		m_R_Later.SetCheck(false);
	}
	m_R_Later.EnableWindow(false);

	m_R_Day.EnableWindow(true);

	// force removal of all accounts from list
	FillAccountGrid();
	
	// hide account grid
	pomAccountList->EnableWindow(false);

	m_B_Filter.ChangeColor(RED,BLACK,MAROON,RED); */

CCS_CATCH_ALL
}

//******************************************************************************************
// APO 30.12.00
// OnRbRecalculation(): Racalculation of accounts has to be done. In this case we 
//	have to hide the account grid because the user must not any accounts.
//	Recalculation is a pseudo account, there is no such data in the database.
//	Instead it has to be configured in the handlers initialization script.
//	To execute this command we send the hardcoded XBS-command for 'recalculation'
//	(which is configurated in CEDA.INI and 'INIT_LB' by default) which
//	must be configured in the handlers init-script by calling  AddXBSCommand().
// Input:	none
// Return:	none
//******************************************************************************************/

void CInitializeAccountDlg::OnRbRecalculation() 
{
CCS_TRY
	TRACE("CInitializeAccountDlg::OnRbRecalculation()\n");

	// disable monthly execution
	m_R_Now.SetCheck (true);
	imExecutionMode = EXECUTION_NOW;

	m_R_Later.EnableWindow(true);

	// disable from - to execution
	if (m_R_Day.GetCheck() == 1)
	{
		m_R_Month01.SetCheck (true);
		m_E_Year.EnableWindow(true);
		m_R_Day.SetCheck (false);
	}
	m_R_Day.EnableWindow(false);
	m_E_Date.EnableWindow(FALSE);

	// force removal of all accounts from list
	FillAccountGrid();
	
	// hide account grid
	pomAccountList->EnableWindow(false);

	m_B_Filter.ChangeColor(RED,BLACK,MAROON,RED); 

CCS_CATCH_ALL
}

//******************************************************************************************
// APO 02.02.01
// OnButtonOpenjobcontrol(): control open jobs. Query open jobs from handler, display
//	them and let the user delete or edit them.
// Input:	none
// Return:	none
//******************************************************************************************/

void CInitializeAccountDlg::OnButtonOpenjobcontrol() 
{
	COpenJobControlDialog olOpenJobDlg;
	olOpenJobDlg.DoModal();
}

//******************************************************************************************
// ARE 20.11.00
// OnShowFilteredEmployee(): the user pressed the filter button.
// Input:	none
// Return:	none
//******************************************************************************************/

void CInitializeAccountDlg::OnShowFilteredEmployee() 
{
CCS_TRY
	CString olSelection, olOrgCode, olTmp, olStfs;
	
	// member struct holding stf data wil be refilled, so clear it first
	omStfData.DeleteAll();

	// read selected org code
	m_C_Orgeinheit.GetWindowText(olSelection);
	COleDateTime olStart, olEnd;
	// employees to show in grid
	CMapStringToPtr olStfUrnoMap;	
	void  *prlVoid = NULL;
	// array of org-codes needed by GetSorByOrgWithTime() (see later on)
	// holds only one org-code at a time here
	CStringArray olOrgCodes; 

	// get and check start and end time
	if (!GetAndCheckTimeSpan(olStart,olEnd)) return;

	TRACE("Start: %s\n",olStart.Format("%d.%m.%Y"));
	TRACE("Ende: %s\n",olEnd.Format("%d.%m.%Y"));

	if(olSelection.GetLength() <= 0)
	{
		MessageBeep(MB_OK);
		return;
	}

	// show wait cursor
	CWaitCursor olDummy;

	// extract code from ORG-combo selection
	olOrgCode = olSelection.Left(8);
	olOrgCode.TrimRight();
	olOrgCodes.Add(olOrgCode);

	COleDateTime olDate;
	olDate = COleDateTime::GetCurrentTime();

	if(olOrgCode == "--------")
	{
		// show all employees without org code (no entry in SORTAB)
		CCSPtrArray<STFDATA> olStfData; 
		// get all employees without org code in period
		olStfs = ogSorData.GetStfWithoutOrgWithTime(olDate, olDate, &olStfData);
		// number of records
		int ilStfSize = olStfData.GetSize();

		STFDATA *prlStf;
		// loop over STF data, get every urno and store it in urno map
		for (int i = 0; i < ilStfSize; i++)
		{
			prlStf = ogStfData.GetStfByUrno(olStfData[i].Urno);
			if (prlStf != NULL)
			{
				STFDATA *prlStfNew = new STFDATA;
				memcpy((void*)prlStfNew, (void*)prlStf, sizeof(STFDATA));
				omStfData.Add(prlStfNew);
			}
		}
	}
	else if(olOrgCode.GetLength() > 0)
	{
		// show only employees with the selected ORG-code
		CCSPtrArray<SORDATA> olSorData;
		// get all employees with matching org code in period
		ogSorData.GetSorByOrgWithTime(&olOrgCodes, olDate, olDate, &olSorData);
		// number of records
		int ilSorSize = olSorData.GetSize();

		STFDATA *prlStf;
		// loop over SOR data, get every employee's urno and store it in urno map
		for (int i = 0; i < ilSorSize; i++)
		{
			prlStf = ogStfData.GetStfByUrno(olSorData[i].Surn);
			if (prlStf != NULL)
			{
				STFDATA *prlStfNew = new STFDATA;
				memcpy((void*)prlStfNew, (void*)prlStf, sizeof(STFDATA));
				omStfData.Add(prlStfNew);
			}
		}
		// cleanup
		olSorData.RemoveAll();

	}
	else
	{
		// show all employees
		int ilStfSize = ogStfData.omData.GetSize();

		// loop over STF data, get every employee's urno and store it in urno map
		for (int i = 0; i < ilStfSize; i++)
		{
			STFDATA *prlStf = new STFDATA;
			*prlStf = ogStfData.omData[i];
			omStfData.Add(prlStf);
		}
	}

	// re-fill employee grid
	FillEmployeeGrid();

	// reset button color
	m_B_Filter.ChangeColor();

CCS_CATCH_ALL	
}

//*********************************************************************************************************************
// APO 14.03.01
// Send_XBS_BC_and_get_result: send a 'XSCR' broadcast to handler and wait for answer with result.
// Input:	CString opScriptName........the full path and name of the script to be executed
//			CString opData..............the employee urno(s), must not be larger than 
//										MAX_BC_SELECTION_SIZE
//			CString opFieldValues.......the field values of the additional info parameters as
//										defined in bc's <cFields>-param (see below)
// Return:	TRUE	-> everything is ok
//			FALSE	-> error
//*********************************************************************************************************************

bool CInitializeAccountDlg::Send_XSCR_BC_and_get_result(CString opScriptName,
														CString opData,
														CString opFieldValues)
{
    static char cReq_id[22];     
    static char cUser[12];					// normally CEDA
    static char cWks[12] = "InitAcc";		// workstation, here appl. name
    static char cAction[12] = "XSCR"; 
    static char cTable[512] = "";			// table Name (multiple table are allowed)
    static char cSortClause[512] = "";		// sort Clause    
    static char cTws[MAX_BC_TW_SIZE] = "0";	// time Window Start 
    static char cTwe[MAX_BC_TW_SIZE];		// time Window end
	// selection criterium, normally a WHERE clause
    static char cSelection[MAX_BC_SELECTION_SIZE];
	static char cFields[MAX_BC_FIELDS] = "";// DB field, normally database fields
	static char cData[MAX_BC_DATA] = "";	// response buffer 
    static char cDataDest[12] = "RETURN";	// RETURN or BUF1 .. BUF4
	int iLastReturnCode;
	CString strError;

	// user specific info
	strcpy(cUser,pcgUser);
	strcpy(cTwe,pcgAppName);
	
	// check parameter <opData>
	if ((opData == "") || (opData.GetLength() > MAX_BC_SELECTION_SIZE))
	{
		// command is too long -> do error message and terminate
		TRACE("Send_XBS_BC_and_get_result() error condition line %d: <opData> == '' or too large\n",__LINE__);
		return false;
	}

	// check parameter <opFieldValues>
	if ((opFieldValues == "") || (opFieldValues.GetLength() > MAX_BC_DATA))
	{
		// command is too long -> do error message and terminate
		TRACE("Send_XBS_BC_and_get_result() error condition line %d: <opFieldValues> == '' or too large\n",__LINE__);
		return false;
	}

	// copy script name 
	strcpy(cSelection,opScriptName.GetBuffer(0));
	// copy employee urnos
	strcpy(cData,opData.GetBuffer(0));
	// copy additional info field values
	strcpy(cFields,opFieldValues.GetBuffer(0));

	// inform the user
	pomStatusBar->SetStaticText(1, LoadStg(IDS_STRING1887));

	// in case the response takes time
	AfxGetApp()->DoWaitCursor(1);
	if ((iLastReturnCode = ::CallCeda(cReq_id,cUser,cWks,cAction,cTable,cSortClause,
									  cTws,cTwe,cSelection,cFields,cData,cDataDest)) != 0)
	{
		MessageBeep((UINT)-1);
		strError.Format(LoadStg(IDS_STRING1885),cData);
		MessageBox(strError,LoadStg(IDS_STRING1884),MB_ICONEXCLAMATION);
		// restore normal state
		pomStatusBar->SetStaticText(1, LoadStg(IDS_STRING1459));
		return false;
	}
	// switch off the wait cursor
	AfxGetApp()->DoWaitCursor(-1);

	// check the return buffer for errors
	if (CString(cData).Left(strlen("Script Basic Error:")) == "Script Basic Error:")
	{
		// an error occurred -> show message
		MessageBeep((UINT)-1);
		strError.Format(LoadStg(IDS_STRING1886),cData);
		MessageBox(strError,LoadStg(IDS_STRING1884),MB_ICONEXCLAMATION);
	}
	else 
	{
		// script result
		TRACE("Send_XBS_BC_and_get_result(): result = '%s'",cData);
	}

	// restore normal state
	pomStatusBar->SetStaticText(1, LoadStg(IDS_STRING1459));
	
	return true;
}


//******************************************************************************************
// APO 29.06.01
// GetEmployeeOrgCode(): get the employee's ORG-code at <olStart>.
// Input:	long lpUrno				-> employee's urno
//			COleDateTime opStart	-> period start
//			COleDateTime opEnd		-> period end
// Return:	CString					-> the ORG-code (first in period if more than one)
//******************************************************************************************/

CString CInitializeAccountDlg::GetEmployeeOrgCode(long lpUrno, COleDateTime opStart, COleDateTime opEnd)
{
	// return string
	CString olOrgCode;

	COleDateTime olDate;
	olDate = COleDateTime::GetCurrentTime();
	
	olOrgCode =  ogSorData.GetOrgBySurnWithTime(lpUrno, olDate);
	return olOrgCode;
}

CString CInitializeAccountDlg::GetEmployeeContract(long lpUrno, 
												  COleDateTime opStart,
												  COleDateTime opEnd)
{
	// return string
	CString olContract; // = "---";
	// struct stores all found org records in period
	CCSPtrArray<SCODATA> olScoData;
	SYSTEMTIME rlStart, rlEnd;

	COleDateTime olDate;
	olDate = COleDateTime::GetCurrentTime();

	// convert COleDateTime to CTime
	if (!opStart.GetAsSystemTime(rlStart) || !opEnd.GetAsSystemTime(rlEnd))
	{
		// error converting objects, can not continue
		TRACE("CInitializeAccountDlg::GetEmployeeContract(): error converting COleDateTime to CTime!!!\n");
		return olContract;
	}

	// construct CTime
	COleDateTime olStart(rlStart);
	COleDateTime olEnd(rlEnd);

	// get org codes
	ogScoData.GetScoBySurnWithTime(lpUrno,olDate,olDate,&olScoData);
	// if org codes found, get first and copy it to return buffer
	// else code will be initial value (= '---')
	if (olScoData.GetSize())
		olContract = olScoData[0].Code;
	
	// cleanup
	olScoData.RemoveAll();

	// return org code
	return olContract;
}

void CInitializeAccountDlg::FillContractMap()
{
	int ilCnt = ogCotData.omData.GetSize();
	COTDATA *rolCotData = NULL;

	omVertragStunden.RemoveAll();
	for (int i = 0; i < ilCnt; i++)
	{
		rolCotData = &ogCotData.omData[i];
		if (strcmp(rolCotData->Inbu, "x") != NULL)
		{
			omVertragStunden.SetAt(CString(rolCotData->Ctrc), NULL);
		}
	}
}

void  CInitializeAccountDlg::ProcessAccountCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CInitializeAccountDlg *polDlg = (CInitializeAccountDlg*)vpInstance;
	ASSERT(polDlg);
	struct BcStruct *prlAccData = (struct BcStruct *) vpDataPointer; 
	ASSERT(prlAccData);
	
	// is it the RECALC command initiated from our workstation ?
	if (strcmp(prlAccData->Cmd,"XBS2") == 0 && strcmp(prlAccData->Tws,"RECALC") == 0 && strcmp(prlAccData->ReqId,ogCommHandler.pcmReqId) == 0)
	{
		CString olCaption(LoadStg(IDS_STRING1952));
		CStringArray olDataArray;
		ExtractItemList(prlAccData->Data,&olDataArray);

		CTime	olFrom,olTo;
		CTime	olStart;
		olFrom = DBStringToDateTime(olDataArray[0] + "000000");
		olTo   = DBStringToDateTime(olDataArray[1] + "000000");
		olStart= DBStringToDateTime(olDataArray[6] + "00");
		CString olMsg;
		olMsg.Format(	LoadStg(IDS_STRING1951),
						olFrom.Format("%d.%m.%Y"),
						olTo.Format("%d.%m.%Y"),
						olStart.Format("%d.%m.%Y %H:%M")
					);

		CStringArray olStfuArray;
		ExtractItemList(prlAccData->Selection,&olStfuArray);
		CStringList olStaffList;
		for (int i = 0; i < olStfuArray.GetSize(); i++)
		{
			STFDATA *prlStf = ogStfData.GetStfByUrno(atol(olStfuArray[i]));
			if (prlStf)
			{
				CString olName;
				olName.Format("%s,%s",prlStf->Lanm,prlStf->Finm);
				olStaffList.AddTail(olName);
			}
		}
		CListBoxDlg olDlg(&olStaffList,olCaption,olMsg,polDlg);
		olDlg.DoModal();
	}
	else if (strcmp(prlAccData->Cmd,"XBS") == 0 && strcmp(prlAccData->Tws,"INIT_MUL") == 0 && strcmp(prlAccData->ReqId,ogCommHandler.pcmReqId) == 0)
	{
		CString olCaption(LoadStg(IDS_STRING1954));
		CStringArray olDataArray;
		ExtractItemList(prlAccData->Data,&olDataArray);

		CTime	olFrom,olTo;
		CTime	olStart;
		olFrom = DBStringToDateTime(olDataArray[0] + "000000");
		olTo   = DBStringToDateTime(olDataArray[1] + "000000");
		olStart= DBStringToDateTime(olDataArray[6] + "00");
		CString olMsg;
		olMsg.Format(	LoadStg(IDS_STRING1953),
						olFrom.Format("%d.%m.%Y"),
						olTo.Format("%d.%m.%Y"),
						olStart.Format("%d.%m.%Y %H:%M")
					);

		CStringArray olStfuArray;
		ExtractItemList(prlAccData->Selection,&olStfuArray);
		CStringList olStaffList;
		for (int i = 0; i < olStfuArray.GetSize(); i++)
		{
			STFDATA *prlStf = ogStfData.GetStfByUrno(atol(olStfuArray[i]));
			if (prlStf)
			{
				CString olName;
				olName.Format("%s,%s",prlStf->Lanm,prlStf->Finm);
				olStaffList.AddTail(olName);
			}
		}
		CListBoxDlg olDlg(&olStaffList,olCaption,olMsg,polDlg);
		olDlg.DoModal();
	}
}

void Process_InitializeAccountDlg_Cf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CCS_TRY;

	
	CInitializeAccountDlg *prlDlg = (CInitializeAccountDlg *)vpInstance;
	switch(ipDDXType)
	{
	default:
		break;
	case COT_CHANGE:
	case COT_DELETE:
	case COT_NEW:
		prlDlg->FillContractMap();
		break;
	}
	CCS_CATCH_ALL;
}


void CInitializeAccountDlg::SendCommand(CString olCommand, CString olAction)
{
	CCS_TRY;
	static char cBlank[2]	= " ";			// for non-important information
	static char cUser[12];					// the name of the actual user
	static char cAction[6];					// the command for the scbhdl (e.g. "XBS2")
	static char cTws[33];					// containing 'real' command (e.g. "REFILL")
	static char cTwe[33]	= "InitAccount";	// name of the application
	strcpy(cUser,pcgUser);
	strcpy(cTws,olCommand.GetBuffer(0));
	strcpy(cAction,olAction.GetBuffer(0));
	::CallCeda	(cBlank, cUser, cBlank, cAction, cBlank, cBlank, cTws, cTwe, cBlank, cBlank, cBlank, "RETURN");
	
	CCS_CATCH_ALL;
}