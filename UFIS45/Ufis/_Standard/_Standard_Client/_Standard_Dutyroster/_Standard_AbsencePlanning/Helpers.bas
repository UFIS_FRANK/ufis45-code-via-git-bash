Attribute VB_Name = "Helpers"
Option Explicit

Dim strWeeks As String
Dim strMondays As String
Dim strSundays As String

Public Sub FillMainGrid()
    strWeeks = frmDocument.TabHeader.GetLineValues(0)
    strMondays = frmDocument.TabHeader.GetLineValues(1)
    strSundays = frmDocument.TabHeader.GetLineValues(2)

    Dim strLine As String
    Dim i As Integer

    For i = 0 To frmHiddenServerConnection.TabSTF.GetLineCount - 1
        strLine = GetLineString(Trim(frmHiddenServerConnection.TabSTF.GetColumnValue(i, 6)))
    Next i
'    If Trim(ogActualViewName) <> "" Then
'    Else ' just add everything ...
'        With frmHiddenServerConnection
'            For i = 0 To (.TabSTF.GetLineCount - 1) Step 1
'                tmpStr = tmpStr + Trim(.TabSTF.GetColumnValue(i, 1)) + "," + _
'                                  Trim(.TabSTF.GetColumnValue(i, 0)) + "," + _
'                                  .TabSTF.GetColumnValue(i, 2) + Chr(10)
'                If (i > 0) And (i Mod 50) = 0 Then
'                    TabMain.InsertBuffer tmpStr, Chr(10)
'                    tmpStr = ""
'                End If
'            Next i
'            TabMain.InsertBuffer tmpStr, Chr(10)
'        End With
'    End If
End Sub

Private Function GetLineString(stftabURNO As String) As String
    Dim strLineNo As String
    Dim strLine As String
    Dim strHelp As String
    Dim strItem As String

    Dim i As Integer

    ' get all absences of the employee
    strLineNo = frmHiddenServerConnection.TabDRR.GetLinesByColumnValue(1, stftabURNO, 1)

    For i = 1 To ItemCount(strLineNo, ",") Step 1
        ' add all absences of the employee to the collection "colDates";
        ' the key is the date of the absence, the item is the kind of the  absence
        Dim colDates As New Collection
        strLine = frmHiddenServerConnection.TabDRR.GetLineValues(GetItem(strLineNo, i, ","))
        strHelp = GetItem(strLine, 4, ",")
        If strHelp <> "0" Then
            strItem = frmHiddenServerConnection.TabODA.GetLinesByColumnValue(2, strHelp, 1)
            colDates.Add Item:=frmHiddenServerConnection.TabODA.GetColumnValue(CInt(strItem), 0), key:=GetItem(strLine, 1, ",")
        End If

        ' Now we've got all absences of the employee. So let's look after the weeks they are in
    Next i
End Function

Public Function GetItem(cpTxt As String, ipNbr As Integer, cpSep As String) As String
    Dim Result
    Dim ilSepLen As Integer
    Dim ilFirstPos As Integer
    Dim ilLastPos As Integer
    Dim ilItmLen As Integer
    Dim ilItmNbr As Integer

    Result = ""
    If ipNbr > 0 Then
        ilSepLen = Len(cpSep)
        ilFirstPos = 1
        ilLastPos = 1
        ilItmNbr = 0
        While (ilItmNbr < ipNbr) And (ilLastPos > 0)
            ilItmNbr = ilItmNbr + 1
            ilLastPos = InStr(ilFirstPos, cpTxt, cpSep)
            If (ilItmNbr < ipNbr) And (ilLastPos > 0) Then ilFirstPos = ilLastPos + ilSepLen
        Wend
        If ilLastPos < ilFirstPos Then ilLastPos = Len(cpTxt) + 1
        If (ilItmNbr = ipNbr) And (ilLastPos > ilFirstPos) Then
            ilItmLen = ilLastPos - ilFirstPos
            Result = Mid(cpTxt, ilFirstPos, ilItmLen)
        End If
    End If
    GetItem = RTrim(Result)
End Function

Public Function GetItemNo(itemlist As String, ItemValue As String) As Integer
    Dim ItmLen As Integer
    Dim ItmPos As Integer
    ItmLen = Len(ItemValue) + 1
    ItmPos = InStr(itemlist, ItemValue)
    If ItmPos > 0 Then
        GetItemNo = ((ItmPos - 1) \ ItmLen) + 1
    Else
        GetItemNo = -1
    End If
End Function

Public Sub SetItem(itemlist As String, ItemNbr As Integer, ItemSep As String, NewValue As String)
    Dim Result As String
    Dim CurItm As Integer
    Dim ReqItm As Integer
    Dim MaxItm As Integer

    Result = ""
    ReqItm = ItemNbr - 1
    For CurItm = 1 To ReqItm
        Result = Result & GetItem(itemlist, CurItm, ItemSep) & ItemSep
    Next
    Result = Result & NewValue & ItemSep
    MaxItm = ItemCount(itemlist, ItemSep)
    ReqItm = ReqItm + 2
    For CurItm = ReqItm To MaxItm
        Result = Result & GetItem(itemlist, CurItm, ItemSep) & ItemSep
    Next
    itemlist = Left(Result, Len(Result) - Len(ItemSep))
End Sub
Public Function ItemCount(itemlist As String, ItemSep As String) As Integer
    Dim Result As Integer
    Dim SepPos As Integer
    Dim SepLen As Integer

    If itemlist <> "" Then
        Result = 1
        SepLen = Len(ItemSep)
        SepPos = 1
        Do
            SepPos = InStr(SepPos, itemlist, ItemSep, vbBinaryCompare)
            If SepPos > 0 Then
                Result = Result + 1
                SepPos = SepPos + SepLen
            End If
        Loop While SepPos > 0
    Else
        Result = 0
    End If
    ItemCount = Result
End Function
