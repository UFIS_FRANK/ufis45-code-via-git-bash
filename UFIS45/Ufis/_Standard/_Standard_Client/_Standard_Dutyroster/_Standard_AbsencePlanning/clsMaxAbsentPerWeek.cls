VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsMaxAbsentPerWeek"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarPatternName As String 'local copy
Private mvarPatternDefaultValue As Integer 'local copy
Private colValues As New Collection


Private Sub Class_Initialize()
    mvarPatternName = "<Default>"
    mvarPatternDefaultValue = 0
End Sub


Public Function GetValue(riYear As Integer, riWeek As Integer) As Integer
    Dim strKey As String
    strKey = CStr(riYear) & "@" & CStr(riWeek)
    If DoesKeyExist(colValues, strKey) <> False Then
        GetValue = colValues.Item(strKey)
    Else
        GetValue = mvarPatternDefaultValue
    End If
End Function


Public Sub AddValue(riYear As Integer, riWeek As Integer, riValue As Integer)
    Dim strKey As String
    strKey = CStr(riYear) & "@" & CStr(riWeek)
    If DoesKeyExist(colValues, strKey) = False Then
        colValues.Add Item:=CStr(riValue), key:=strKey
    End If
End Sub


Public Property Let PatternDefaultValue(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PatternDefaultValue = 5
    mvarPatternDefaultValue = vData
End Property


Public Property Get PatternDefaultValue() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PatternDefaultValue
    PatternDefaultValue = mvarPatternDefaultValue
End Property


Public Property Let PatternName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PatternName = 5
    mvarPatternName = vData
End Property


Public Property Get PatternName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PatternName
    PatternName = mvarPatternName
End Property


Private Function DoesKeyExist(ByRef refCollection As Collection, refKey As String) As Boolean
    On Error GoTo DoesNotExist
    refCollection.Item (refKey)
    DoesKeyExist = True
    Exit Function
DoesNotExist:
    DoesKeyExist = False
    Err.Clear
End Function
