// CedaWorkTime.h: interface for the CedaWorkTime class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CEDAWORKTIME_H__AA858673_218B_11D4_AC03_00E0981D21D7__INCLUDED_)
#define AFX_CEDAWORKTIME_H__AA858673_218B_11D4_AC03_00E0981D21D7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <CCSGLOBL.H>
#include <CedaDrrData.h>
#include <CedaDraData.h>
#include <CedaStfData.h>

// die WTE_x k�nnen kombiniert werden, daher modul 2
#define WTE_NONE			  1		// keins
#define WTE_END				  2		// Ende des Eintrages (Schicht/Free/Regular Free...)
#define WTE_SHIFT			  4		// dies ist eine Schicht
#define WTE_SHIFT_PLUS		  8		// Teilzeit Plus und �berzeit
#define	WTE_FREE			 16		// Freie Zeit
#define	WTE_R_FREE			 32		// Regular Free - Freier Tag
#define	WTE_DRA_FREE		 64		// Untert�gige Abwesenheit
#define	WTE_SLEEP_DAY		 128	// Ausgleichstag/Schlaftag

#define	WTE_SHIFT_ALL	(WTE_SHIFT | WTE_SHIFT_PLUS)
#define WTE_DRA_ENABLED_SHIFT (WTE_SHIFT | WTE_SHIFT_PLUS)
//#define WTE_FREE_ALL	(WTE_FREE  | WTE_R_FREE | WTE_DRA_FREE)
#define	WTE_ALL			(WTE_END | WTE_SHIFT | WTE_SHIFT_PLUS | WTE_FREE | WTE_R_FREE | WTE_DRA_FREE /*| WTE_SLEEP_DAY*/)
#define	WTE_ALL_START	(WTE_SHIFT | WTE_SHIFT_PLUS | WTE_FREE | WTE_R_FREE)

// Operationen:
// R�ckgabeparameter f�r GetDataChange()
enum
{
	OT_NONE = 0					,		// 0
	OT_DRR_CHECK				,		// 1
	OT_DRR_SHIFT_NEW			,		// 2
	OT_DRR_SHIFT_CHANGE			,		// 3
	OT_DRR_SHIFT_DELETE			,		// 4
	OT_DRR_SHIFT_PLUS_NEW		,		// 5
	OT_DRR_SHIFT_PLUS_CHANGE	,		// 6
	OT_DRR_SHIFT_PLUS_DELETE	,		// 7
	OT_DRR_FREE_NEW				,		// 8
	OT_DRR_FREE_CHANGE			,		// 9
	OT_DRR_FREE_DELETE			,		// 10
	OT_DRR_R_FREE_NEW			,		// 11
	OT_DRR_R_FREE_CHANGE		,		// 12
	OT_DRR_R_FREE_DELETE		,		// 13
	OT_DRA_NEW					,		// 14
	OT_DRA_CHANGE				,		// 15
	OT_DRA_DELETE						// 16
};

// Test Rutinen 
/*	ERWEITERUNG DER REGELN:
	1. ID_WOR_Value hinzuf�gen
	2. einf�gen	omRules[ID_WOR_xxx]		= atoi(ogCCSParam.GetParamValue(ogAppl,"ID_WOR_xxx"));  
	3. int CedaWorkTime::CheckOneDayRules(DRRDATA* prpDrr, int ipDDXType) erweitern
		in eine von zwei Gruppen einordnen - Arbeitszeiteinschr�nkung oder Freizeiteinschr�nkung
	4. in Rostering.rc eine Fehlermeldung speichern
*/
enum
{
	ID_WOR_SPACE = 0,		// mu� immer als erster stehen	- 0. Die Arbeitszeiten d�rfen sich nicht �berlappen
	ID_WOR_FREEH	,
	ID_WOR_FREEHT	,
	ID_WOR_RESTH	,
	ID_WOR_RESTDW	,
	ID_WOR_MAWORH	,
	ID_WOR_MAWORHT	,
	ID_WOR_WORHWT	,
	ID_WOR_MIWORH	,
	ID_WOR_MIWORHT	,
	ID_WOR_MAWORD	,
	ID_WOR_MAWORDT	,
	ID_WOR_MAFREED	,
	ID_WOR_MAFREEDT	,
	ID_WOR_MAXIMUMVALUE		// mu� immer als letzter stehen
};

enum
{
	RET_CANCEL = 0,			// nicht OK, aber keine Fehler, z.B. wenn eine Schicht eingef�gt werden soll, aber leer ist, 
							// dann wird sie nicht eingef�gt, jedoch ist es kein Fehlerzustand, einfach Abbruch
	RET_NO_ERROR,			// alles OK
	RET_WARNING,			// Warnung - toleriert
	RET_ERROR,				// Checkfehler - verweigern
	RET_SOFTWARE_ERROR		// Software- und nicht Checkfehler
};

enum
{
	S_REGULAR = 0, 
	S_DELETED, 
	S_INSERTED
};

enum 
{
	TIME_NORMAL = 0,
	TIME_ZERO			// diese Schicht/Abwesenheit hatte keine Zeitangaben, soll ans Ende der Vorherigen angepasst werden
};

/**************************************************************************
Ein Element f�r Array CedaWorkTime.omWorkTimeData
Inhalt: Arbeitswendepunkt
**************************************************************************/

class CWorkTimeEntry 
{
public:
	CWorkTimeEntry();
	~CWorkTimeEntry(){}

	// rundet omTime auf volle Minuten
	void	RoundTime();
	// rundet omSb auf volle Minuten
	void	RoundSb();
	// berechnet absoluten Tag
	int		GetAbsDay();
	// Membervariablen
	COleDateTime		omTime;		// der Zeitpunkt des Ereignisses - size = 8 Byte
	BYTE				cmTimeType;	// TIME_NORMAL oder TIME_ZERO
	COleDateTimeSpan	omSb;		// nur bei WTE_SHIFT, WTE_SHIFT_PLUS - Dauer der Schicht, abz�glich Pause und DRAs
	short				cmType;		// WTE - Typ des Ereignisses - defines siehe oben 
	BYTE				cmState;	// S_REGULAR, S_DELETED, S_INSERTED
	long				lmUrno;		// Urno des urspr�nglichen DRRDATA, DRADATA
};	

/***************************************************************************
Ein Element f�r CedaShiftCheck.omStaffDutyData
CedaWorkTime beh�lt Information �ber Arbeitszeitablauf eines Mitarbeiters in der
angegebenen Zeitperiode in optimierter und zusammengefasster Form
Das erm�glicht schnelles Kontrollieren bei ShiftCheck
***************************************************************************/

class CedaWorkTime
{
public:
	CedaWorkTime(int ipEName = 2);
	CedaWorkTime(long lpStfu, CedaShiftCheck* popShiftCheck, int ipEName = 2);
	CedaWorkTime(CedaWorkTime &poSrc, int ipEName = 2);

	virtual ~CedaWorkTime();

	CedaWorkTime& operator =(CedaWorkTime &poSrc);

	//	UpdateData() speichert �nderungen der Arbeitszeit im Datenarray omWorkTimeData
	int UpdateData(DRRDATA* prpDrr,int ipDDXType);
	int UpdateData(DRADATA* prpDra,int ipDDXType);
	
	int AddErrorMessage(int ipTest,int ipIndex, int ipErr, int ipWTE=WTE_NONE);
	
	void ShowErrorBox(bool bpIsAbsence = false);
	// true, wenn pomWarnings nicht leer ist (d.h. Warnungen vorliegen)
	bool IsWarning();

	// Testfunktion
	bool CheckIt(void* prpVoid, int ipDDXType, bool bpCheckWeek = true);
private:
	// Initialisierung
	void InitWorkTimeData();

	// f�hrt Delete-Befehl durch
	int UpdateDataDrrDelete(DRRDATA* prpDrr);
	// f�hrt New und Change durch
	int UpdateDataDrrNewChange(DRRDATA* prpDrr,int ipDDXType);
	// Updatet omWorkTimeData durch die Elemente der CWorkTimeEntry* popWorkTimeEntry
	// ipDDXType = DRR_CHANGE, DRR_NEW, DRR_DELETE, DRA_CHANGE, DRA_NEW, DRA_DELETE
	int LinkWTArrays(CWorkTimeEntry* popWorkTimeEntry,int ipDDXType);
	// Dienstfunktion: berechnet Anfang- und Endzeiten und initialist CWorkTimeEntry-s Felder
	void SetTimesFromTo(CWorkTimeEntry* popWorkTimeEntry, COleDateTime opFrom, COleDateTime opTo, char* opSblu);
	// Testet einen Datensatz DRRDATA oder DRADATA
	int CheckSingleData(void* prpVoid, int ipDDXType, bool bpCheckWeek = true);
	// Testet alle Datens�tze
	void CheckAllData();
	void CheckAllRules(int* piX);
	// Passt alle evtl. nicht angepasste FREE-Startzeiten an die vorherige Schicht
	void SetFreeStartTimes();

	// Dienstfunktion
	void SetStartOfNextFreeShift(CWorkTimeEntry* popWorkTimeEntry);

	void DeletePoints(CWorkTimeEntry* popWorkTimeEntry, int ipDDXType);

	// abh�ngig vom bmSaveUndo wird der Punkt gel�scht oder nur als gel�schter markiert
	void RemovePoints(int ipStart);
	
	// f�gt Punkte ein
	int InsertPoints(CWorkTimeEntry* popWorkTimeEntry, int ipDDXType);
	
	// l�scht alle Punkte, die als cmState == S_INSERTED markiert sind und
	// widerherstellt alle Punkte, die als cmState == S_DELETED markiert sind
	void RestoreAllRemovedAndInserted();

	// alle Regeln nacheinander werden gepr�ft, 
	int CheckOneDayRules(DRRDATA* prpDrr, DRADATA* prpDra, int ipDataChange, bool bpCheckWeek = true);

	// Stellt fest, welcher Art die �nderung sein soll (sieh oben enum OT_NONE...)
	int GetDataChange(DRRDATA* prpDrr, int ipDDXType);

	// Die Pr�ffunktionen: f�r jede Pr�fung eigene Funktion
	int Check_ID_WOR_FREEHT(DRRDATA* prpDrr, int* piX);
	int Check_ID_WOR_RESTH(DRRDATA* prpDrr, int* piX);
	int Check_ID_WOR_RESTDW(DRRDATA* prpDrr, int* piX);
	int CheckTheWeek_ID_WOR_RESTDW(COleDateTime opDayStart, COleDateTimeSpan opRestH, int* piX);
	int Check_ID_WOR_M_WORHT(DRRDATA* prpDrr, int* piX);
	int Check_ID_WOR_WORHWT(DRRDATA* prpDrr, int* piX);
	int Check_ID_WOR_MAWORDT(DRRDATA* prpDrr, int* piX);
	int Check_ID_WOR_MAFREEDT(DRRDATA* prpDrr, int* piX);

	// Findet Element mit dem n�chstkleineren oder gleichen Zeitpunkt
	// R�ckgabe: index zu omWorkTimeData
	int FindPrevTimePoint(int ipStartPoint, int ipEndPoint, int ipWTE);
	int FindPrevTimePoint(int ipStartPoint, int ipEndPoint, COleDateTime opFind, int ipWTE);
	// Findet Element mit dem n�chstgr�sseren oder gleichen Zeitpunkt
	int FindNextTimePoint(int ipStartPoint, int ipEndPoint, COleDateTime opFind, int ipWTE);
	
	// Findet Element mit dem angegebenen lpUrno ab dem ipStartPoint aufw�rts
	int GetTimePoint(int ipStartPoint, long lpUrno, int ipWTE);
	// stellt fest, ob der Zeitraum opTime ausreichend Zeit f�r mindestens ein R_FREE hat, wenn ja, dann f�r wieviele.
	int GetNumberOfRegFreeTimes(COleDateTimeSpan& opTime, COleDateTimeSpan& opRestH);
	// findet ersten Punkt an dem Tag
	int FindFirstTimePointOfThisDay(int ipStartPoint, int ipEndPoint, COleDateTime opFind, int ipWTE);
	// wenn Teilzeit+ nicht m�glich ist, setzt man bmEnableTeilzeitplusQuestion auf false
	void CheckTeilzeitPlus(int ipTest, int ipIndex, int ipErr);
	// Data Members
public:
	long lmStfu;			// URNO des Mitarbeiters
	bool bmShiftData;		// true - Daten der Klasse sind aus Schichttest (lmStfu ist ein Z�hler)

	// speichert alle Fehlermeldungen, wenn nicht null. Nach dem ablesen m�ssen alle Strings gel�scht werden
	CStringArray* pomErrors;
	// speichert alle Warnungen, wenn nicht null. Nach dem ablesen m�ssen alle Strings gel�scht werden
	CStringArray* pomWarnings;

private:
	int imEName;
	/* omWorkTimeData - Array mit Zeitangaben. 
	Besteht aus nacheinander folgenden CWorkTimeEntry-s, die "Arbeitswendepunkte" enthalten:
	jeder Arbeitswendepunkt ist entweder von einer Schicht zur Pause zwischen zwei Schichten 
	("Nachtruhe", keine Mittagspause): SP = S->P, oder von der Pause zur Schicht
	smType zeigt auf den Typ des Wendepunkts
	Alle Zeitangaben sind absolut, wie COleDateTime und aufsteigend sortiert (0-te ist am fr�hesten)
	*/
	CArray<CWorkTimeEntry, CWorkTimeEntry&> omWorkTimeData;
	
	// wenn bmSaveUndo = true, markieren wir jeden zu l�schenden Punkt mit WTE_DELETED,
	// und jeden neu eingef�gten Punkt mit WTE_INSERTED
	bool bmSaveUndo;
	
	CedaShiftCheck* pomShiftCheck;

};

#endif // !defined(AFX_CEDAWORKTIME_H__AA858673_218B_11D4_AC03_00E0981D21D7__INCLUDED_)
