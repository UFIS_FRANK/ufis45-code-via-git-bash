// DutyShowJobsDlg.cpp : implementation file
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//************************************************************************************************************************************************
// ProcessDutyShowJobsDlgCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_REL_CHANGE, BC_REL_NEW und BC_REL_DELETE. Ruft zur eingentlichen Bearbeitung
//	der Nachrichten die Funktion DutyShowJobsDlg::ProcessDutyShowJobsDlgBc() der entsprechenden 
//	Instanz von DutyShowJobsDlg auf.	
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessDutyShowJobsDlgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Empf�nger ermitteln
	DutyShowJobsDlg *polShowJobs = (DutyShowJobsDlg *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polShowJobs->ProcessDutyShowJobsDlgBc(ipDDXType,vpDataPointer,ropInstanceName);
}

/////////////////////////////////////////////////////////////////////////////
// DutyShowJobsDlg dialog


DutyShowJobsDlg::DutyShowJobsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(DutyShowJobsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(DutyShowJobsDlg)
	//}}AFX_DATA_INIT

	bmIsBCRegistered = false;
	pomGrid = 0;

	pomParent = (DutyReleaseDlg*)pParent;

	prmViewInfo = pomParent->prmViewInfo;
}

/*******************************************************************
Destructor
*******************************************************************/
DutyShowJobsDlg::~DutyShowJobsDlg()
{
CCS_TRY;
	// beim BC-Handler abmelden
	if(bmIsBCRegistered)
	{
		ogDdx.UnRegister(this,NOTUSED);
		// jetzt ist die Instanz abgemeldet
		bmIsBCRegistered = false;
	}

	// Grid l�schen
	if (pomGrid) 
	{
		delete pomGrid;
		pomGrid = 0;
	}
CCS_CATCH_ALL;
}

void DutyShowJobsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DutyShowJobsDlg)
	DDX_Control(pDX, IDC_BUTTON1, m_b_Update);
	DDX_Control(pDX, IDC_BUTTON3, m_b_Delete);
	DDX_Control(pDX, IDC_BUTTON2,	m_b_Find);
	DDX_Control(pDX, IDC_EDIT1, m_e_DayFrom);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DutyShowJobsDlg, CDialog)
	//{{AFX_MSG_MAP(DutyShowJobsDlg)
	ON_BN_CLICKED(IDC_BUTTON2, OnBFind)
	ON_BN_CLICKED(IDC_BUTTON1, OnUpdate)
	ON_BN_CLICKED(IDC_RADIO2, OnReleasedJobs)
	ON_BN_CLICKED(IDC_BUTTON3, OnDeleteJobs)
	ON_BN_CLICKED(IDC_RADIO1, OnOpendJobs)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


//************************************************************************************************************************************************
// Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
//	Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
//	Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
//	dann die entsprechende Nachricht bearbeitet. �ber den nach void 
//	konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
//	Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
//	und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
//	entsprechenden Member-Funktion).
// R�ckgabe: keine
//************************************************************************************************************************************************

void DutyShowJobsDlg::Register(void)
{
CCS_TRY;
	// bereits angemeldet?
	if (bmIsBCRegistered) return;	// ja -> gibt nichts zu tun, terminieren

	// REL-Datensatz hat sich ge�ndert
	DdxRegister((void *)this,BC_REL_CHANGE, "DutyShowJobsDlg", "BC_REL_CHANGE",        ProcessDutyShowJobsDlgCf);
	DdxRegister((void *)this,BC_REL_NEW,    "DutyShowJobsDlg", "BC_REL_NEW",		   ProcessDutyShowJobsDlgCf);
	DdxRegister((void *)this,BC_REL_DELETE, "DutyShowJobsDlg", "BC_REL_DELETE",        ProcessDutyShowJobsDlgCf);
	// jetzt ist die Instanz angemeldet
	bmIsBCRegistered = true;
CCS_CATCH_ALL;
}

/***********************************************************************************
behandelt die Broadcasts BC_REL_CHANGE,BC_REL_DELETE und BC_REL_NEW
wir brauchen die Tabelle neu zu f�llen
***********************************************************************************/
void DutyShowJobsDlg::ProcessDutyShowJobsDlgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	FillGrid(false);
	SortGrid(1);
}

//---------------------------------------------------------------------------

BOOL DutyShowJobsDlg::OnInitDialog() 
{
CCS_TRY;
	CDialog::OnInitDialog();

	// Titel setzen
	SetWindowText(LoadStg(IDS_STRING190));

	// Wartedialog anzeigen
	CWaitDlg olWaitDlg(this,LoadStg(WAIT_DATA_PREPARE));

	HICON m_hIcon = AfxGetApp()->LoadIcon(IDR_ROSTERTYPE);
	SetIcon(m_hIcon, FALSE);

	omDayFrom.SetStatus(COleDateTime::invalid);

	// Texte setzen
	SetDlgItemText(IDC_RADIO1,LoadStg(IDS_STRING_133));	// &Offene Jobs
	SetDlgItemText(IDC_RADIO2,LoadStg(IDS_STRING_134));	// &Abgearbeitete Jobs
	SetDlgItemText(IDC_BUTTON3,LoadStg(IDS_STRING1526));	// &L�schen
	SetDlgItemText(IDC_STATIC1,LoadStg(IDS_STRING_139));	// Ab Datum:
	SetDlgItemText(IDC_BUTTON1,LoadStg(IDS_STRING146));	// A&ktualisieren
	SetDlgItemText(IDC_BUTTON2,LoadStg(IDS_STRING_147));	// &Suchen in Tabelle
	SetDlgItemText(IDOK,LoadStg(SHIFT_B_CANCEL));			// &Beenden				- ist jedoch ein OK!!! halt die Konstante heisst SHIFT_B_CANCEL- ich hab sie nicht erfunden :(

	// Editfeld inizialisieren
	m_e_DayFrom.SetTypeToDate(true);
	m_e_DayFrom.SetTextErrColor(RED);
	m_e_DayFrom.SetInitText("");

	// Grid initialisieren
	IniGrid();

	// beim Broadcast-Handler anmelden
	Register();

	CheckRadioButton( IDC_RADIO1, IDC_RADIO2, IDC_RADIO1 );
	OnOpendJobs();

	return TRUE;
CCS_CATCH_ALL;
return FALSE;
}

/******************************************************************************************************
Selektierte Zeilen ermitteln
******************************************************************************************************/
void DutyShowJobsDlg::GetSelectedItems()
{
CCS_TRY;

	CGXStyle		olStyle;
	long			ilUrno;
	CString			olUrno;
	CRowColArray	olRowColArray;

	// Mitarbeiter-Urno-Liste l�schen
	omSelectedUrnoList.RemoveAll();
		
	// Selektion ausw�hlen
	pomGrid->GetSelectedRows (olRowColArray,false,false);

	// Wurden Zeilen ausgew�hlt
	if (olRowColArray.GetSize() == 0)
	{
		TRACE("Keine Zeilen ausgew�hlt.\n");
		return;
	}
	
	// F�r jede ausgew�hlte Zeile abarbeiten
	for (int i=0;i<olRowColArray.GetSize();i++)
	{
		// Style und damit Datenpointer einer Zelle erhalten
		pomGrid->ComposeStyleRowCol(olRowColArray[i],1, &olStyle );
		// Urno erhalten
		ilUrno = (long)olStyle.GetItemDataPtr();
		if ( !ilUrno )
			TRACE ("Urno=0 in DutyShowJobsDlg f�r Zeile%d\n",olRowColArray[i]);
		else
		{
			// hinzuf�gen
			omSelectedUrnoList.AddTail(ilUrno);
		}
	}
CCS_CATCH_ALL;
}

/******************************************************************************************************
Selektierte Jobs l�schen
******************************************************************************************************/

void DutyShowJobsDlg::DeleteSelectedItems()
{
CCS_TRY;

	CString olDeleteString = "";
	CString olTmp;
	bool blDidAnything = false;
	POSITION olPos;

	CString olTempUrno;
	long llCount = 0;

	for (olPos = omSelectedUrnoList.GetHeadPosition(); olPos != NULL;)
	{
		llCount++;
		olTempUrno.Format(",'%ld'", omSelectedUrnoList.GetNext(olPos));
		olDeleteString += olTempUrno;

		if ((llCount % 100) == 0)
		{
			blDidAnything = true;
			olTmp = olDeleteString.Mid(1);
			olDeleteString.Format("WHERE URNO IN (%s)", olTmp);
			CedaRelData olRelData;

			char *pclDeleteString = (char*) malloc(sizeof(char) + olDeleteString.GetLength()+2);

			if (pclDeleteString != NULL)
			{
				strcpy(pclDeleteString, olDeleteString);
				olRelData.CedaAction("DRT",pclDeleteString);
				free(pclDeleteString);
			}
			olDeleteString = "";
		}
	}

	if (olDeleteString.GetLength() > 0)
	{
		blDidAnything = true;
		olTmp = olDeleteString.Mid(1);
		olDeleteString.Format("WHERE URNO IN (%s)", olTmp);

		CedaRelData olRelData;
		char *pclDeleteString = (char*) malloc(sizeof(char) + olDeleteString.GetLength()+2);

		if (pclDeleteString != NULL)
		{
			strcpy(pclDeleteString, olDeleteString);
			olRelData.CedaAction("DRT",pclDeleteString);
			free(pclDeleteString);
		}
	}

	// Daten neu lesen - Tabelle zeichnen
	if (blDidAnything == true)
	{
		FillGrid(false);
		SortGrid(1);
	}

CCS_CATCH_ALL;
}

//**********************************************************************************
// Grid initialisieren
//**********************************************************************************

void DutyShowJobsDlg::IniGrid ()
{
	// Grid erzeugen
	pomGrid = new CGridControl ( this, IDC_GRID_JOBS, DUTY_SHOW_JOBS_COLCOUNT, 2);
	
	// �berschriften setzen
	pomGrid->SetValueRange(CGXRange(0,1), LoadStg(SHIFT_STF_PENO));
	pomGrid->SetValueRange(CGXRange(0,2), LoadStg(SHIFT_S_STAFF));
	pomGrid->SetValueRange(CGXRange(0,3), LoadStg(IDS_STRING1700));		// "von"
	pomGrid->SetValueRange(CGXRange(0,4), LoadStg(IDS_STRING1701));		// "bis"
	pomGrid->SetValueRange(CGXRange(0,5), LoadStg(IDS_STRING1702));		// "Stufe"

	// Breite der Spalten einstellen
	IniColWidths ();

	// Grid mit Daten f�llen.
	FillGrid(true, false);

	pomGrid->Initialize();
	pomGrid->GetParam()->EnableUndo(FALSE);
	pomGrid->LockUpdate(TRUE);
	pomGrid->LockUpdate(FALSE);
	pomGrid->GetParam()->EnableUndo(TRUE);

	// Verhindert, das die Spaltenbreite ver�ndert wird
	pomGrid->GetParam()->EnableTrackColWidth(FALSE);

	// Es k�nnen nur ganze Zeilen markiert werden.
	pomGrid->GetParam()->EnableSelection(GX_SELROW | GX_SELMULTIPLE | GX_SELSHIFT);

	pomGrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE));
	pomGrid->SetScrollBarMode(SB_VERT,gxnAutomatic);
	pomGrid->EnableAutoGrow ( FALSE );
	pomGrid->SetLbDblClickAction ( WM_COMMAND, IDC_B_MOVETO);
}

/**********************************************************************************
Grid mit Daten f�llen:
bmShowOpendJobs bestimmt, ob offene oder abgearbeitete Jobs gezeigt werden sollen,
omDayFrom - wenn invalid, wird nichts gezeigt
bpRegisterBC - true - RELDATA-Broadcast registrieren
**********************************************************************************/

void DutyShowJobsDlg::FillGrid(bool bpRegisterBC, bool bpShowMessageBox /*=true*/)
{
	CString olSurn;

	CString olWhere = "";
	CString olTmpUrnos;

	omRelData.ClearAll(bpRegisterBC);

	// Liste mit allen ben�tigten RELDATA-s erstellen
	if(bmShowOpendJobs)
	{
		// alle offene Jobs
		olWhere.Format("WHERE RLAT = 'x'");
	}
	else
	{
		// abgearbeitete Jobs
		if(omDayFrom.GetStatus() == COleDateTime::valid)
		{
			// nur wenn die Zeit g�ltig ist, kann gelesen werden, sonst leer
			olWhere.Format("WHERE RLAT > '%s'",omDayFrom.Format("%Y%m%d%H%M%S"));
		}
	}

	if(olWhere.GetLength() != 0)
	{
		// Datens�tze lesen
		if(omRelData.Read(olWhere.GetBuffer(0), NULL, bpRegisterBC, false) == false)
		{
			if(omRelData.LastError() != "No Data Found")
			{
				AfxMessageBox(LoadStg(ST_READERR),MB_ICONSTOP);			// Fehler beim Lesen aus der Datenbank*INTXT*
			}
		}
	}

	// omRelData, wenn nicht leer, hat alle relevanten Datens�tze
	RELDATA		olRelData;
	STFDATA*	olStfData;		
	CString		csFullName;
	CString		csUrno;

	// Zellen entriegeln
	pomGrid->GetParam()->SetLockReadOnly(false);
	// kein Zellenupdate zwischendurch
	pomGrid->LockUpdate(true);
	// Die Zeilenzahl setzen
	pomGrid->SetRowCount(omRelData.GetInternalSize());

	for (int i = 0; i < omRelData.GetInternalSize(); i++)
	{
		// Access-Pointers ermitteln
		olRelData = omRelData.GetInternalData(i);

		olStfData = ogStfData.GetStfByUrno(olRelData.Stfu);
		if(!olStfData) continue;
		
		// Urno mit dem ersten Feld koppeln.						
		pomGrid->SetStyleRange ( CGXRange(i+1,1), CGXStyle().SetItemDataPtr( (void*)olRelData.Urno ) );
		// 1: "Peno"
		pomGrid->SetValueRange(CGXRange(i+1,1), olStfData->Peno);
		// 2: "Name, Vorname"
		csFullName = CBasicData::GetFormatedEmployeeName(prmViewInfo->iEName, olStfData, "", "");
		pomGrid->SetValueRange(CGXRange(i+1,2), csFullName);
		// 3: von
		pomGrid->SetValueRange(CGXRange(i+1,3), olRelData.Rlfr.Format("%d.%m.%Y"));
		// 4: bis
		pomGrid->SetValueRange(CGXRange(i+1,4), olRelData.Rlto.Format("%d.%m.%Y"));
		// 5: Rosl (Freigabestufe)
		pomGrid->SetValueRange(CGXRange(i+1,5), olRelData.Rosl);
	}
	pomGrid->GetParam()->SetLockReadOnly(true);
	pomGrid->LockUpdate(false);
	pomGrid->Redraw();
}

//**********************************************************************************
// Grid initialisieren, Breite richtet sich nach Anzahl der Buchstaben im Titel
//**********************************************************************************

void DutyShowJobsDlg::IniColWidths ()
{
	pomGrid->SetColWidth ( 0, 0, 30);
	pomGrid->SetColWidth ( 1, 1, 105);
	pomGrid->SetColWidth ( 2, 2, 180);
	pomGrid->SetColWidth ( 3, 3, 67);
	pomGrid->SetColWidth ( 4, 4, 67);
	pomGrid->SetColWidth ( 5, 5, 37);
}

//**********************************************************************************
// Sortierung des Grids "per Hand"
//**********************************************************************************

bool DutyShowJobsDlg::SortGrid(int ipRow)
{
	CGXSortInfoArray  sortInfo;
	sortInfo.SetSize( 1 );
	sortInfo[0].sortOrder = CGXSortInfo::ascending;
	sortInfo[0].nRC = ipRow;                       
	sortInfo[0].sortType = CGXSortInfo::autodetect;  
	pomGrid->SortRows( CGXRange().SetRows(1, pomGrid->GetRowCount()), sortInfo); 
	return (true);
}

//**********************************************************************************
// Suchen Dialog aufrufen
//**********************************************************************************

void DutyShowJobsDlg::OnBFind() 
{
	if (pomGrid)
		pomGrid->OnShowFindReplaceDialog(TRUE);
}

/*******************************************************************************
die Liste aktualisieren
*******************************************************************************/
void DutyShowJobsDlg::OnUpdate() 
{
	if(bmShowOpendJobs) return;		// nur bei ShowReleaseJobs

	if(m_e_DayFrom.GetStatus() == false)
	{
		AfxMessageBox(LoadStg(IDS_NO_DATE_TIME),MB_ICONSTOP);
		return;
	}
	
	CString olFrom;
	m_e_DayFrom.GetWindowText(olFrom);	
	omDayFrom = OleDateStringToDate(olFrom);
	
	if (omDayFrom.GetStatus() != COleDateTime::valid)
	{
		AfxMessageBox(LoadStg(IDS_NO_DATE_TIME),MB_ICONSTOP);
		return;
	}

	// Daten neu lesen - Tabelle zeichnen
	FillGrid(false);
	SortGrid(1);
}

/*******************************************************************************
umschalten zu abgearbeiteten Jobs
*******************************************************************************/
void DutyShowJobsDlg::OnReleasedJobs() 
{
	bmShowOpendJobs = false;
	m_b_Delete.EnableWindow(false);

	CString csCode;
	csCode = ogPrivList.GetStat("m_b_Update");

	if(csCode=='1') 
		m_b_Update.EnableWindow(true);

	if(csCode=='0' || csCode=='-')
		m_b_Update.EnableWindow(false);

	csCode = ogPrivList.GetStat("DUTYSHOWJOBSDLG_IDC_Button3");

	if(csCode=='1') 
		m_b_Delete.EnableWindow(true);

	if(csCode=='0' || csCode=='-')
		m_b_Delete.EnableWindow(false);

	m_e_DayFrom.EnableWindow(true);
	m_e_DayFrom.SetFocus();

	// Daten neu lesen - Tabelle zeichnen
	FillGrid(false);
	SortGrid(1);
}

/*******************************************************************************
alle selektierten Jobs l�schen
*******************************************************************************/
void DutyShowJobsDlg::OnDeleteJobs() 
{
	GetSelectedItems();
	DeleteSelectedItems();
}

/*******************************************************************************
umschalten zu offenen Jobs
*******************************************************************************/
void DutyShowJobsDlg::OnOpendJobs() 
{
	bmShowOpendJobs = true;

	CString csCode;
	csCode = ogPrivList.GetStat("m_b_Update");

	if(csCode=='1') 
		m_b_Update.EnableWindow(true);

	if(csCode=='0' || csCode=='-')
		m_b_Update.EnableWindow(false);

	csCode = ogPrivList.GetStat("DUTYSHOWJOBSDLG_IDC_Button3");

	if(csCode=='1') 
		m_b_Delete.EnableWindow(true);

	if(csCode=='0' || csCode=='-')
	{
		m_b_Delete.EnableWindow(false);
		GetDlgItem(IDC_RADIO1)->EnableWindow(false);
		CheckRadioButton(IDC_RADIO1,IDC_RADIO2,IDC_RADIO2);
		m_e_DayFrom.EnableWindow(true);
		m_e_DayFrom.SetFocus();

	}

	// Daten neu lesen - Tabelle zeichnen
	FillGrid(false);
	SortGrid(1);
}
