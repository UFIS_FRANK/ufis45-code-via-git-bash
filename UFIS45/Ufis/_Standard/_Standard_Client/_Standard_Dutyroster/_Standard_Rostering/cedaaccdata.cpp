// CedaAccData.cpp
 
#include <stdafx.h>


void ProcessAccCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
CedaAccData ogAccData;

//--CEDADATA-----------------------------------------------------------------------------------------------
 
CedaAccData::CedaAccData() : CedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(ACCDATA, AccDataRecInfo)
		FIELD_DATE	   (Aato,"AATO")
		FIELD_CHAR_TRIM(Auby,"AUBY")
		FIELD_DATE     (Auda,"AUDA")
		FIELD_DATE     (Cdat,"CDAT")
		FIELD_CHAR_TRIM(Cl01,"CL01")
		FIELD_CHAR_TRIM(Cl02,"CL02")
		FIELD_CHAR_TRIM(Cl03,"CL03")
		FIELD_CHAR_TRIM(Cl04,"CL04")
		FIELD_CHAR_TRIM(Cl05,"CL05")
		FIELD_CHAR_TRIM(Cl06,"CL06")
		FIELD_CHAR_TRIM(Cl07,"CL07")
		FIELD_CHAR_TRIM(Cl08,"CL08")
		FIELD_CHAR_TRIM(Cl09,"CL09")
		FIELD_CHAR_TRIM(Cl10,"CL10")
		FIELD_CHAR_TRIM(Cl11,"CL11")
		FIELD_CHAR_TRIM(Cl12,"CL12")
		FIELD_CHAR_TRIM(Co01,"CO01")
		FIELD_CHAR_TRIM(Co02,"CO02")
		FIELD_CHAR_TRIM(Co03,"CO03")
		FIELD_CHAR_TRIM(Co04,"CO04")
		FIELD_CHAR_TRIM(Co05,"CO05")
		FIELD_CHAR_TRIM(Co06,"CO06")
		FIELD_CHAR_TRIM(Co07,"CO07")
		FIELD_CHAR_TRIM(Co08,"CO08")
		FIELD_CHAR_TRIM(Co09,"CO09")
		FIELD_CHAR_TRIM(Co10,"CO10")
		FIELD_CHAR_TRIM(Co11,"CO11")
		FIELD_CHAR_TRIM(Co12,"CO12")
		FIELD_DATE     (Lstu,"LSTU")
		FIELD_CHAR_TRIM(Op01,"OP01")
		FIELD_CHAR_TRIM(Op02,"OP02")
		FIELD_CHAR_TRIM(Op03,"OP03")
		FIELD_CHAR_TRIM(Op04,"OP04")
		FIELD_CHAR_TRIM(Op05,"OP05")
		FIELD_CHAR_TRIM(Op06,"OP06")
		FIELD_CHAR_TRIM(Op07,"OP07")
		FIELD_CHAR_TRIM(Op08,"OP08")
		FIELD_CHAR_TRIM(Op09,"OP09")
		FIELD_CHAR_TRIM(Op10,"OP10")
		FIELD_CHAR_TRIM(Op11,"OP11")
		FIELD_CHAR_TRIM(Op12,"OP12")
		FIELD_CHAR_TRIM(Peno,"PENO")
		FIELD_LONG     (Stfu,"STFU")
		FIELD_CHAR_TRIM(Type,"TYPE")
		FIELD_LONG     (Urno,"URNO")
		FIELD_CHAR_TRIM(Usec,"USEC")
		FIELD_CHAR_TRIM(Useu,"USEU")
		FIELD_CHAR_TRIM(Year,"YEAR")
		FIELD_CHAR_TRIM(Yecu,"YECU")
		FIELD_CHAR_TRIM(Yela,"YELA")
		FIELD_CHAR_TRIM(Fumo,"FUMO")

	END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(AccDataRecInfo)/sizeof(AccDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&AccDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
	strcpy(pcmTableName,"ACC");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,	"AATO,AUBY,AUDA,CDAT,CL01,CL02,CL03,CL04,CL05,CL06,CL07,CL08,CL09,CL10,CL11,CL12,CO01,"
							"CO02,CO03,CO04,CO05,CO06,CO07,CO08,CO09,CO10,CO11,CO12,LSTU,OP01,OP02,OP03,OP04,"
							"OP05,OP06,OP07,OP08,OP09,OP10,OP11,OP12,PENO,STFU,TYPE,URNO,USEC,USEU,YEAR,YECU,YELA,FUMO");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	pomLoadStfuUrnoMap = NULL;
	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaAccData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaAccData::Register(void)
{
	DdxRegister((void *)this,BC_ACC_CHANGE,		"CedaAccData", "BC_ACC_CHANGE",		ProcessAccCf);
	DdxRegister((void *)this,BC_ACC_NEW,		"CedaAccData", "BC_ACC_NEW",		ProcessAccCf);
	DdxRegister((void *)this,BC_ACC_DELETE,		"CedaAccData", "BC_ACC_DELETE",		ProcessAccCf);
	DdxRegister((void *)this,BC_DISABLEACCSAVE, "CedaAccData", "BC_DISABLEACCSAVE",	ProcessAccCf);
	DdxRegister((void *)this,BC_ENABLEACCSAVE,	"CedaAccData", "BC_ENABLEACCSAVE",	ProcessAccCf);
	DdxRegister((void *)this,BC_RELACC,			"CedaAccData", "BC_RELACC",			ProcessAccCf);
}



//--~CEDADATA----------------------------------------------------------------------------------------------

CedaAccData::~CedaAccData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaAccData::ClearAll(bool bpWithUnRegistration)
{
    omUrnoMap.RemoveAll();
    omKeyMap.RemoveAll();
	omStfuYearMap.RemoveAll();
    omData.DeleteAll();
	pomLoadStfuUrnoMap = NULL;
	if(bpWithUnRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//---------------------------------------------------------------------------

void CedaAccData::SetLoadStfuUrnoMap(CMapPtrToPtr *popLoadStfuUrnoMap)
{
CCS_TRY;
	pomLoadStfuUrnoMap = popLoadStfuUrnoMap;
CCS_CATCH_ALL;
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaAccData::Read(char *pspWhere /*NULL*/, bool bpDeleteAll /*=true*/)
{
    // Select data from the database
	bool		 ilRc = true;
	CString		 olWhere;
	CString		 olUrnosString;
	CStringArray olUrnosStringArray;
 
 	if(bpDeleteAll)
	{
		omUrnoMap.RemoveAll();
		omKeyMap.RemoveAll();
		omStfuYearMap.RemoveAll();
		omData.DeleteAll();
	}
 
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if (pspWhere == NULL)
	{
		olWhere = CString (pspWhere);
	}


	if(pspWhere == NULL)
	{	
		ilRc = CedaAction2("RT");
	}
	else
	{
		ilRc = CedaAction2("RT", pspWhere);
	}

	if (ilRc != true)
	{
		return ilRc;
	}

	// Load data from CedaData into the dynamic array of record
	int ilCountRecord = 0;

	if(pomLoadStfuUrnoMap != NULL)
	{
		//Load WITH LoadStfuUrnoMap
		void  *prlVoid = NULL;
		for(ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			ACCDATA *prlAcc = new ACCDATA;
			if ((ilRc = GetFirstBufferRecord2(prlAcc)) == true)
			{
			
				//TRACE(" Type: %s Stfu: %ld\n",prlAcc->Type,prlAcc->Stfu);

				if(pomLoadStfuUrnoMap->Lookup((void *)prlAcc->Stfu, (void *&)prlVoid) == TRUE)
				{
					omData.Add(prlAcc);//Update omData
					omUrnoMap.SetAt((void *)prlAcc->Urno, prlAcc);

					CString olTmp;
					olTmp.Format("%ld-%s-%s",prlAcc->Stfu, prlAcc->Year, prlAcc->Type);
					//olTmp.Format("%ld-%s",prlAcc->Stfu, prlAcc->Year);
					omKeyMap.SetAt(olTmp,prlAcc);
					olTmp.Format("%ld-%s",prlAcc->Stfu, prlAcc->Year);
					omStfuYearMap.SetAt(olTmp,(void*)0);
#ifdef TRACE_FULL_DATA
					// Datensatz OK, loggen if FULL
					ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
					GetDataFormatted(ogRosteringLogText, prlAcc);
					WriteLogFull("");
#endif TRACE_FULL_DATA
				}
				else
				{
					if(IsTraceLoggingEnabled())
					{
						GetDefectDataString(ogRosteringLogText, (void*)prlAcc);
						WriteInRosteringLog(LOGFILE_TRACE);
					}
					delete prlAcc;
				}
			}
			else
			{
				delete prlAcc;
			}
		}
	}
	else
	{
		//Load WITHOUT LoadStfuUrnoMap
		CString olTmp;
		void *prlVoid;
		for(ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			ACCDATA *prlAcc = new ACCDATA;
			if ((ilRc = GetFirstBufferRecord2(prlAcc)) == true)
			{
				olTmp.Format("%ld-%s-%s",prlAcc->Stfu, prlAcc->Year, prlAcc->Type);
				if(omKeyMap.Lookup(olTmp, (void *&)prlVoid) == FALSE)
				{
					omData.Add(prlAcc);
					omUrnoMap.SetAt((void *)prlAcc->Urno, prlAcc);					
					omKeyMap.SetAt(olTmp,prlAcc);
					olTmp.Format("%ld-%s",prlAcc->Stfu, prlAcc->Year);
					omStfuYearMap.SetAt(olTmp,(void*)0);
				}
				else
				{
					UpdateInternal(prlAcc,false);
				}
			}
			else
			{
				if(IsTraceLoggingEnabled())
				{
					GetDefectDataString(ogRosteringLogText, (void*)prlAcc);
					WriteInRosteringLog(LOGFILE_TRACE);
				}
				delete prlAcc;
			}
		}
	}
	TRACE("Read-Acc: %d gelesen\n",ilCountRecord-1);

	ClearFastSocketBuffer();	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}

	return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaAccData::Insert(ACCDATA *prpAcc)
{
	prpAcc->IsChanged = DATA_NEW;
	if(Save(prpAcc) == false) return false; //Update Database
	InsertInternal(prpAcc);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaAccData::InsertInternal(ACCDATA *prpAcc, bool bpSendDdx /*=true*/)
{

	CString olTmp;
	olTmp.Format("%ld-%s-%s",prpAcc->Stfu,prpAcc->Year, prpAcc->Type);

	ACCDATA *prlData;
	// übergebener Datensatz NULL oder Datensatz mit diesem Schlüssel
	// (Schichttag,Schichtnummer,MA-Urno) schon vorhanden?

	if(omKeyMap.Lookup(olTmp,(void *&)prlData)  == TRUE)
	{
		return false;
	}

	omData.Add(prpAcc);//Update omData
	omUrnoMap.SetAt((void *)prpAcc->Urno,prpAcc);
	omKeyMap.SetAt(olTmp,prpAcc);
	olTmp.Format("%ld-%s",prpAcc->Stfu,prpAcc->Year);
	omStfuYearMap.SetAt(olTmp,prpAcc);

	if(bpSendDdx)
	{
		ogDdx.DataChanged((void *)this, ACC_NEW,(void *)prpAcc ); //Update Viewer
	}
	
   return true;
}


//--DELETE-------------------------------------------------------------------------------------------------

bool CedaAccData::Delete(long lpUrno)
{
	ACCDATA *prlAcc = GetAccByUrno(lpUrno);
	if (prlAcc != NULL)
	{
		prlAcc->IsChanged = DATA_DELETED;
		if(Save(prlAcc) == false) return false; //Update Database
		DeleteInternal(prlAcc);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaAccData::DeleteInternal(ACCDATA *prpAcc, bool bpSendDdx /*=true*/)
{
	// remove that record from all maps, so it will not be found any more
	omUrnoMap.RemoveKey((void *)prpAcc->Urno);
	CString olTmp;
	olTmp.Format("%ld-%s-%s",prpAcc->Stfu, prpAcc->Year, prpAcc->Type);
	omKeyMap.RemoveKey(olTmp);
	olTmp.Format("%ld-%s",prpAcc->Stfu, prpAcc->Year);
	omStfuYearMap.RemoveKey(olTmp);

	// send the internal BC
	if(bpSendDdx)
	{
		ogDdx.DataChanged((void *)this,ACC_DELETE,(void *)prpAcc); //Update Viewer
	}

	// remove the record from omData
	int ilAccCount = omData.GetSize();
	for (int ilCountRecord = 0; ilCountRecord < ilAccCount; ilCountRecord++)
	{
		if (omData[ilCountRecord].Urno == prpAcc->Urno)
		{
			omData.DeleteAt(ilCountRecord);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaAccData::Update(ACCDATA *prpAcc,bool bpWithDdx /*true*/)
{
	if (GetAccByUrno(prpAcc->Urno) != NULL)
	{
		if (prpAcc->IsChanged == DATA_UNCHANGED)
		{
			prpAcc->IsChanged = DATA_CHANGED;
		}
		if(Save(prpAcc) == false) return false; //Update Database
		UpdateInternal(prpAcc,bpWithDdx);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaAccData::UpdateInternal(ACCDATA *prpAcc, bool bpSendDdx /*=true*/)
{
	bool blRet = false;
	if(prpAcc==NULL)
		return blRet;
	ACCDATA *prlAcc = GetAccByUrno(prpAcc->Urno);
	if (prlAcc != NULL)
	{		
		*prlAcc = *prpAcc; //Update omData
		if(bpSendDdx)
		{
			ogDdx.DataChanged((void *)this,ACC_CHANGE,(void *)prlAcc); //Update Viewer
		}
		blRet = true;
	}
    return blRet;
}

// reload ACC-data after recalculation, initialization or any other event
bool CedaAccData::Reload(CString opYear, CString opStfUrnos)
{
	ACCDATA *prlAcc = NULL;
	CStringArray olArrArray;

	CString olStfUrnos;
	CString olYear;
	CString olTmp;
	int ilYear = atoi(opYear);

	// getting year
	olYear = opYear;

	// getting the StfUrnos in the format '1234567890'
	olStfUrnos = opStfUrnos;
	olStfUrnos.Replace(",","','");
	olStfUrnos = "'" + olStfUrnos;
	olStfUrnos += "'";

	// Delete all existing ACC-records for the employees and the year
	long llCntAccRecords = omData.GetSize();
	for (long l = llCntAccRecords - 1; l >= 0; l--)
	{
		prlAcc = &omData[l];
		olTmp.Format("'%ld'", prlAcc->Stfu);
		if (olStfUrnos.Find(olTmp) > -1 && strcmp(prlAcc->Year,olYear.GetBuffer(0)) == NULL)
		{
			DeleteInternal(prlAcc, false);
		}
	}

	// reload records
	CString olWhere;
	if (olStfUrnos[olStfUrnos.GetLength() - 1] == ',')
	{
		olStfUrnos.SetAt(olStfUrnos.GetLength() - 1, ' ');
	}
	olWhere.Format("WHERE STFU IN (%s) AND YEAR in ('%d','%d')",olStfUrnos, ilYear, ilYear + 1);
	bool blRet = ogAccData.Read((char*)(LPCTSTR)olWhere,false);

	if (blRet == true)
	{
		ogDdx.DataChanged((void *)this,RELACC,NULL); //Update Viewer ect.
	}
    return blRet;
}

//--RELOAD INTERNAL----------------------------------------------------------

bool CedaAccData::ReloadInternal()
{
	ogDdx.DataChanged((void *)this,RELACC,NULL); //Update Viewer ect.
    return true;
}


//--GET-BY-URNO--------------------------------------------------------------------------------------------

ACCDATA* CedaAccData::GetAccByUrno(long lpUrno)
{
	ACCDATA  *prlAcc;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlAcc) == TRUE)
	{
		return prlAcc;
	}
	return NULL;
}

//--GET BY KEY---------------------------------------------------------------

ACCDATA *CedaAccData::GetAccByKey(long lpStfUrno, CString opYear, CString opType)
{
	CString olTmp;
	olTmp.Format("%ld-%s-%s",lpStfUrno, opYear, opType);

	ACCDATA  *prlAcc = NULL;
	if (omKeyMap.Lookup(olTmp,(void *&)prlAcc) == TRUE)
	{
		return prlAcc;
	}
	return NULL;
}

//--IS EXISTED---------------------------------------------------------------

bool CedaAccData::IsAccExisted(long lpStfUrno, CString opYear)
{
	CString olTmp;
	olTmp.Format("%ld-%s",lpStfUrno, opYear);

	void* polDummy;
	if (omStfuYearMap.Lookup(olTmp,polDummy))
	{
		return true;
	}
	return false;
}

//--------------------------------------------------------------------------------------

CString CedaAccData::GetFieldList()
{
	CString olFieldList = CString(pcmFieldList);
	return olFieldList;
}

//--------------------------------------------------------------------------------------

bool CedaAccData::IsDataChanged(ACCDATA *prpOld, ACCDATA *prpNew)
{
	CString olFieldList;
	CString olListOfData;

	olFieldList = GetFieldList();
	MakeCedaData(olListOfData, olFieldList, (void*)prpOld, (void*)prpNew);
	if(olFieldList.IsEmpty())
	{
		return false;
	}
	else
	{
		return true;
	}

}
//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaAccData::ReadSpecialData(CCSPtrArray<ACCDATA> *popAcc,char *pspWhere,char *pspFieldList,bool ipSYS/*=false*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popAcc != NULL)
	{
		for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			ACCDATA *prpAcc = new ACCDATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prpAcc,CString(pclFieldList))) == true)
			{
				popAcc->Add(prpAcc);
			}
			else
			{
				delete prpAcc;
			}
		}
		if(popAcc->GetSize() == 0) return false;
	}
	ClearFastSocketBuffer();	
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaAccData::Save(ACCDATA *prpAcc)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpAcc->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpAcc->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpAcc);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpAcc->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpAcc->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpAcc);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpAcc->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpAcc->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpAcc);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("DRT",pclSelection,"",pclData);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessAccCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogAccData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//****************************************************************************************
// Broadcasts bearbeiten
//****************************************************************************************

void  CedaAccData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlBcStruct = (struct BcStruct *) vpDataPointer;
	ACCDATA *prlAcc;
	long llUrno;
	CString olSelection;

		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlBcStruct->Cmd, prlBcStruct->Object, prlBcStruct->Twe, prlBcStruct->Selection, prlBcStruct->Fields, prlBcStruct->Data,prlBcStruct->BcNum);
		WriteInRosteringLog();
	}

	switch(ipDDXType)
	{
	default:
		break;
	case BC_ACC_CHANGE:
		llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
		prlAcc = GetAccByUrno(llUrno);
		if(prlAcc != NULL)
		{
			GetRecordFromItemList(prlAcc,prlBcStruct->Fields,prlBcStruct->Data);
			UpdateInternal(prlAcc);
			break;
		}
	case BC_ACC_NEW:
		prlAcc = new ACCDATA;
		GetRecordFromItemList(prlAcc,prlBcStruct->Fields,prlBcStruct->Data);
		InsertInternal(prlAcc);
		break;
	case BC_ACC_DELETE:
		olSelection = (CString)prlBcStruct->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlAcc = GetAccByUrno(llUrno);
		if (prlAcc != NULL)
		{
			DeleteInternal(prlAcc);
		}
		break;
	case BC_DISABLEACCSAVE:
		bmCanSave = false;
		break;
	case BC_ENABLEACCSAVE:
		bmCanSave = true;
		break;
	case BC_RELACC:
		MakeClientString(prlBcStruct->Data);
		MakeClientString(prlBcStruct->Selection);
		CString olData  = prlBcStruct->Data;
		CString olSelection = prlBcStruct->Selection;

		olData = olData .Left(4);
		Reload(olData, olSelection);
		break;
	}
}

//--RELEASE------------------------------------------------------------------

bool CedaAccData::Release(CString opDateFrom, CString opDateTo, CStringArray *popTypeArray , CStringArray *popUrnoArray, CString opCmd /*="R"*/)
{
	CStringArray olUrnoArray;
	olUrnoArray.Copy(*popUrnoArray);

	CString olIRT;
	CString olURT;
	CString olDRT;
	CString olFieldList;
	CString olDataList;
	CString olUpdateString;
	CString olInsertString;
	CString olDeleteString;

	int ilNoUpdates = 0;
	int ilNoDeletes = 0;
	int ilNoInserts = 0;
	CStringArray olFields;
	int ilFields = ExtractItemList(CString(pcmListOfFields),&olFields); 
	olIRT.Format("*CMD*,%s,IRT,%d,", pcmTableName, ilFields);
	olURT.Format("*CMD*,%s,URT,%d,", pcmTableName, ilFields);
	olDRT.Format("*CMD*,%s,DRT,-1,", pcmTableName);

	CString olOrigFieldList;
	int ilCurrentCount = 0;
	int ilTotalCount = 0;

	olOrigFieldList = CString(pcmListOfFields);
	olInsertString = olIRT + CString(pcmListOfFields) + CString("\n");
	olUpdateString = olURT + CString(pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
	olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ CString("\n");;

	POSITION rlPos;
	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		ACCDATA *prlAcc;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlAcc);
		if(prlAcc->IsChanged != DATA_UNCHANGED )
		{
			CString olListOfData;
			CString olCurrentUrno;
			olCurrentUrno.Format("%d",prlAcc->Urno);
			MakeCedaData(&omRecInfo,olListOfData,prlAcc);
			switch(prlAcc->IsChanged)
			{
			case DATA_NEW:
				olInsertString += olListOfData + CString("\n");
				ilCurrentCount++;
				ilTotalCount++;
				ilNoInserts++;
				break;
			case DATA_CHANGED:
				olUpdateString += olListOfData + CString(",") + olCurrentUrno + CString("\n");
				ilNoUpdates++;
				ilTotalCount++;
				ilCurrentCount++;
				break;
			case DATA_DELETED:
				olDeleteString += olCurrentUrno + CString("\n");
				ilCurrentCount++;
				ilTotalCount++;
				ilNoDeletes++;
				break;
			}
			prlAcc->IsChanged = DATA_UNCHANGED;
		}
		if(ilCurrentCount == 50)
		{
			if(ilNoInserts > 0)
			{
				if(CedaAction("REL","LATE","",olInsertString.GetBuffer(0)) == false) return false;
				strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
			if(ilNoUpdates > 0)
			{
				if(CedaAction("REL","LATE","",olUpdateString.GetBuffer(0)) == false) return false;
				strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
			if(ilNoDeletes > 0)
			{
				if(CedaAction("REL","LATE","",olDeleteString.GetBuffer(0)) == false) return false;
				strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
			ilCurrentCount = 0;
			ilNoInserts = 0;
			ilNoUpdates = 0;
			ilNoDeletes = 0;
			olInsertString = olIRT + CString(pcmListOfFields) + CString("\n");
			olUpdateString = olURT + CString(pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
			olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ CString("\n");;
		}
	}
	if(ilNoInserts > 0)
	{
		if(CedaAction("REL","LATE","",olInsertString.GetBuffer(0)) == false) return false;
		strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
	}
	if(ilNoUpdates > 0)
	{
		//if(CedaAction("REL","LATE","",olUpdateString.GetBuffer(0)) == false) return false;
		if(CedaAction("REL","QUICK","",olUpdateString.GetBuffer(0)) == false) return false;
		strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
	}
	if(ilNoDeletes > 0)
	{
		if(CedaAction("REL","LATE","",olDeleteString.GetBuffer(0)) == false) return false;
		strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
	}

	char pclData[800];
	CString olData;
	if(ilTotalCount>0)
	{
		while(olUrnoArray.GetSize() > 0)
		{
			int ilUrnos = 0;
			CString olTmpUrnos;
			while(ilUrnos < 60 && olUrnoArray.GetSize() > 0)
			{
				ilUrnos++;
				olTmpUrnos += CString(",") + olUrnoArray[0];
				olUrnoArray.RemoveAt(0);
			}
			olTmpUrnos = olTmpUrnos.Mid(1);

			for (int i=0;i<popTypeArray->GetSize();i++)
			{
				// Alle übergebenen Kontotypen durchgehen
				olData.Format("%s-%s-%s-%s-%s", opDateFrom, opDateTo, popTypeArray->GetAt(i), olTmpUrnos, opCmd);
				MakeCedaString(olData);
				strcpy(pclData, olData);
				CedaAction("SBC","RELACC", "", "", "", pclData);
			}

		}
		ReloadInternal();
	}
	return true;
}
