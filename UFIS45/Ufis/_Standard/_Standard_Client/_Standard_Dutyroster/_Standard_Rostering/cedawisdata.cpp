// cedawisdata.cpp - Klasse f�r die Handhabung von WIS-Daten
//
 
#include <stdafx.h>

// Das globale Objekt
CedaWisData ogWisData;

static int CompareCodes(const WISDATA **e1, const WISDATA **e2);

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Globale Funktionen
//************************************************************************************************************************************************
//************************************************************************************************************************************************

// globale Callback-Funktion f�r den Broadcast-Handler
//void  ProcessRelDrrCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//************************************************************************************************************************************************
// ProcessDrrCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_DRR_CHANGE, BC_DRR_NEW und BC_DRR_DELETE. Ruft zur eingentlichen Bearbeitung
//	der Nachrichten die Funktion CedaWisData::ProcessDrrBc() der entsprechenden 
//	Instanz von CedaWisData auf.	
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessWisCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
}

//************************************************************************************************************************************************
// ProcessRelDrrCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_RELOAD_SINGLE_DRR, BC_RELOAD_MULTI_DRR und BC_RELDRR. Ruft zur eingentlichen 
//	Bearbeitung der Nachrichten die Funktion CedaWisData::ProcessDrrBc() der 
//	entsprechenden Instanz von CedaWisData auf.
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessRelWisCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{ 
}

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Implementation der Klasse CedaWisData
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// Konstruktor
//************************************************************************************************************************************************

CedaWisData::CedaWisData(CString opTableName, CString opExtName) : CedaData(&ogCommHandler)
{
    // Infostruktur vom Typ CEDARECINFO f�r DRW-Datens�tze
	// Wird von Vaterklasse CCSCedaData f�r verschiedene Funktionen
	// (CCSCedaData::GetBufferRecord(), CCSCedaData::MakeCedaData())
	// benutzt. Die Datenstruktur beschreibt die Tabellenstruktur.
    BEGIN_CEDARECINFO(WISDATA, WisDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")	
		FIELD_CHAR_TRIM	(Hopo,"HOPO")	
		FIELD_DATE		(Lstu,"LSTU")	
		FIELD_CHAR_TRIM	(Orgc,"ORGC")	
		FIELD_CHAR_TRIM	(Rema,"REMA")	
		FIELD_LONG		(Urno,"URNO")	
		FIELD_CHAR_TRIM	(Usec,"USEC")	
		FIELD_CHAR_TRIM	(Useu,"USEU")	
		FIELD_CHAR_TRIM	(Wisc,"WISC")	
		FIELD_CHAR_TRIM	(Wisd,"WISD")	
    END_CEDARECINFO
	// Infostruktur kopieren
    for (int i = 0; i < sizeof(WisDataRecInfo)/sizeof(WisDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&WisDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	// Tabellenname per Default auf WIS setzen
	opExtName = pcgTableExt;
	SetTableNameAndExt(opTableName+opExtName);

    // Feldnamen initialisieren
	strcpy(pcmListOfFields,	"CDAT,HOPO,LSTU,ORGC,REMA,URNO,USEC,USEU,WISC,WISD");
	// Zeiger der Vaterklasse auf Tabellenstruktur setzen
	CCSCedaData::pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

    // Datenarrays initialisieren
	omUrnoMap.InitHashTable(256);
	// Array mit den Urnos der Mitarbeiter, f�r die Daten geladen werden,
	// ist vorerst NULL. Der Array muss explizit per CedaWisData::SetLoadStfUrnoMap()
	// gesetzt werden 
	pomLoadStfuUrnoMap = NULL;
	
//	omLastBcNum = 0;
//	omLastBcUrno = 0;
}

//************************************************************************************************************************************************
// Destruktor
//************************************************************************************************************************************************

CedaWisData::~CedaWisData()
{
	ClearAll();
	omRecInfo.DeleteAll();
}

//************************************************************************************************************************************************
// SetTableNameAndExt: stellt den Namen und die Extension ein.
// R�ckgabe:	false	->	Tabellenname mit Extension gr��er als
//							Klassenmember
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaWisData::SetTableNameAndExt(CString opTableAndExtName)
{
	if(opTableAndExtName.GetLength() >= sizeof(pcmTableName)) return false;
	strcpy(pcmTableName,opTableAndExtName.GetBuffer(0));
	return true;
}

//************************************************************************************************************************************************
// Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
//	Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
//	Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
//	dann die entsprechende Nachricht bearbeitet. �ber den nach void 
//	konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
//	Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
//	und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
//	entsprechenden Member-Funktion).
// R�ckgabe: keine
//************************************************************************************************************************************************

void CedaWisData::Register(void)
{
}


//************************************************************************************************************************************************
// ClearAll: aufr�umen. Alle Arrays und PointerMaps werden geleert.
// R�ckgabe:	immer true
//************************************************************************************************************************************************

bool CedaWisData::ClearAll(bool bpUnregister)
{
    omUrnoMap.RemoveAll();
    omKeyMap.RemoveAll();
    omWiscMap.RemoveAll();
//	omSearchResult.RemoveAll();
	omData.DeleteAll();
	pomLoadStfuUrnoMap = NULL;
	if(bpUnregister)
	{
		ogDdx.UnRegister(this,NOTUSED);
	//	VIEWDRR olViewDrr;
	//	SetView(olViewDrr);
	}
    return true;
}

//************************************************************************************************************************************************
// ReadDrwByUrno: 
//************************************************************************************************************************************************

bool CedaWisData::ReadDrwByUrno(long lpUrno, WISDATA *prpDrw)
{
	return (false);
}

//************************************************************************************************************************************************
// Read: liest Datens�tze ein.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaWisData::Read(char *pspWhere /*=NULL*/)
{
	CString olTmp;
  // Select data from the database
	bool ilRc = true;
	// Maps leeren
    omUrnoMap.RemoveAll();
	omKeyMap.RemoveAll();
    omWiscMap.RemoveAll();
    omData.DeleteAll();

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{	
		ilRc = CedaAction2("RT");
	}
	else
	{
		ilRc = CedaAction2("RT", pspWhere);
	}
	if (ilRc != true)
	{
		TRACE("Read-WIS: Ceda-Error %d \n",ilRc);
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
	{
		WISDATA *prlWis = new WISDATA;
		if ((ilRc = GetFirstBufferRecord2(prlWis)) == true)
		{
			if(IsValidData(prlWis))
			{
				omData.Add(prlWis);//Update omData
				// Die Maps f�llen
				omUrnoMap.SetAt((void *)prlWis->Urno,prlWis);
				olTmp.Format("%s-%s",prlWis->Wisc, prlWis->Orgc);
				omKeyMap.SetAt(olTmp,prlWis);
				
#ifdef TRACE_FULL_DATA
				// Datensatz OK, loggen if FULL
				ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
				GetDataFormatted(ogRosteringLogText, prlWis);
				WriteLogFull("");
#endif TRACE_FULL_DATA
				
				// f�r CheckCode als nur WISC speichern, dabei ist der Pointer == 0
				olTmp.Format("%s",prlWis->Wisc);
				if (omWiscMap.Lookup(olTmp,(void *&)prlWis) == false)
				{
					// Datensatz noch nicht vorhanden
					omWiscMap.SetAt(olTmp,(void*)NULL);
				}
			}
			else
			{
				// Datensatz defect
				delete prlWis;
			}
		}
		else
		{
			delete prlWis;
		}
	}
	TRACE("Read-Wis: %d gelesen\n",ilCountRecord-1);

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}

	return true;
}

//********************************************************************************
// pr�ft, ob ein Wunschcode vorhanden ist
//********************************************************************************

bool CedaWisData::CheckCode(CString opWisc)
{
	CString olTmp;
	WISDATA *prlWis = NULL;

	// such String formatieren
	olTmp.Format("%s",opWisc);

	// Suche in der KeyMap
	if (omWiscMap.Lookup(olTmp,(void *&)prlWis) == TRUE)
	{
		// Datensatz gefunden
		return true;
	}
	// Datensatz nicht gefunden
	return false;
}

//********************************************************************************
// Anhand den Wunsch- und Organisationscodes den Datensatz finden
//********************************************************************************

WISDATA* CedaWisData::GetWisByKey(CString opWisc, CString opOrgc)
{
	CString olTmp;
	WISDATA *prlWis = NULL;

	// such String formatieren
	olTmp.Format("%s-%s",opWisc, opOrgc);

	// Suche in der KeyMap
	if (omKeyMap.Lookup(olTmp,(void *&)prlWis) == TRUE)
	{
		// Datensatz gefunden
		return prlWis;
	}
	// Datensatz nicht gefunden
	return NULL;
}

//*********************************************************************************************
// InsertInternal: f�gt den internen Arrays einen Datensatz hinzu. Der Datensatz
//	muss sp�ter explizit durch Aufruf von Save() oder Release() in der Datenbank 
//	gespeichert werden. Wenn <bpSendDdx> true ist, wird der Broadcast-Handler
//	aktiviert.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaWisData::InsertInternal(WISDATA *prpDrr, bool bpSendDdx)
{
	return true;
}

//*************************************************************************************
//
//*************************************************************************************

void CedaWisData::UpdateInternal(WISDATA *prpDrr, bool bpSendDdx /*true*/)
{
}

//*************************************************************************************
//
//*************************************************************************************

void CedaWisData::DeleteInternal(WISDATA *prpDrr, bool bpSendDdx /*true*/)
{
	
}

//*************************************************************************************
//
//*************************************************************************************

bool CedaWisData::Delete(WISDATA *prpDrr, BOOL bpWithSave)
{
	return true;
}

//*************************************************************************************
//
//*************************************************************************************

bool CedaWisData::Insert(WISDATA *prpDrr, BOOL bpWithSave)
{
	return true;
}

//*************************************************************************************
//
//*************************************************************************************

bool CedaWisData::Update(WISDATA *prpDrr, BOOL bpWithSave)
{
	return true;
}

//*************************************************************************************
//
//*************************************************************************************

bool CedaWisData::Reload(CString opDateFrom, CString opDateTo, CString opUrnos)
{
	return true;
}

//*************************************************************************************
// 
//*************************************************************************************

WISDATA* CedaWisData::GetWisByUrno(long lpUrno)
{
	WISDATA *prlWis = NULL;

	// Suche in der KeyMap
	if (omUrnoMap.Lookup((void *)lpUrno,(void *&)prlWis) == TRUE)
	{
		// Datensatz gefunden
		return prlWis;
	}
	// Datensatz nicht gefunden
	return NULL;
}

//*************************************************************************************
//	Record speichern (wobei hier auch ein Record gel�scht werden kann)
//*************************************************************************************

bool CedaWisData::Save(WISDATA *prpWis)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[1024];
	char pclData[2048];

	if (prpWis->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpWis->IsChanged)   // New job, insert into database
	{
	// Neuer Record
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpWis);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpWis->IsChanged = DATA_UNCHANGED;
		break;
	// ge�nderter Record
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpWis->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpWis);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpWis->IsChanged = DATA_UNCHANGED;
		break;
	// gel�schter Record
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpWis->Urno);
		olRc = CedaAction("DRT",pclSelection);
		if (olRc == true)
		{
		
		}
		break;
	}
   return olRc;
}

//*************************************************************************************
//
//*************************************************************************************

bool CedaWisData::IsDataChanged(WISDATA *prpOld, WISDATA *prpNew)
{
	return false;
}

//*************************************************************************************
// CedaAction: ruft die Funktion CedaAction der Basisklasse CCSCedaData auf.
//	Alle Parameter werden durchgereicht. Aufgrund eines internen CEDA-Bugs,
//	der daf�r sorgen kann, dass die Tabellenbeschreibung <pcmListOfFields>
//	zerst�rt wird, muss sicherheitshalber <pcmListOfFields> vor jedem Aufruf
//	von CedaData::CedaAction() gerettet und danach wiederhergestellt werden.
// R�ckgabe:	der R�ckgabewert von CedaData::CedaAction()
//*************************************************************************************

bool CedaWisData::CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
							 char *pcpSelection, char *pcpSort, char *pcpData, 
							 char *pcpDest /*= "BUF1"*/,bool bpIsWriteAction /*= false*/, 
							 long lpID /*= 0*/, CCSPtrArray<RecordSet> *pomData /*= NULL*/, 
							 int ipFieldCount /*= 0*/)
{
CCS_TRY;
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CedaData::CedaAction(pcpAction,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL;
return false;
}

//*************************************************************************************
// CedaAction: kurze Version, Beschreibung siehe oben.
// R�ckgabe:	der R�ckgabewert von CedaData::CedaAction()
//*************************************************************************************

bool CedaWisData::CedaAction(char *pcpAction, char *pcpSelection, char *pcpSort,
							 char *pcpData, char *pcpDest /*"BUF1"*/, 
							 bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{
CCS_TRY;
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CedaData::CedaAction(pcpAction,CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,(pcpSelection != NULL)? pcpSelection: CCSCedaData::pcmSelection,(pcpSort != NULL)? pcpSort: CCSCedaData::pcmSort,pcpData,pcpDest,bpIsWriteAction, lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL;
return false;
}

/* ::::::
//uhi 19.4.01
CString? CedaWisData::GetWisByOrg(CString opOrg, CCSPtrArray<WISDATA> *popWisData)
{
CCS_TRY;
	popWisData->DeleteAll();
	CString olUrnos = "", olUrno;
	CString olOrgc = "";

	CCSPtrArray<WISDATA> olWisData;
	WISDATA * prlWis;

	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		WISDATA *prlWis;?
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlWis);
		olOrgc = prlWis->Orgc;
		if (olOrgc == opOrg){
			olWisData.Add(prlWis);
		}
	}

	int ilSize = olWisData.GetSize();
	if(ilSize > 0)
	{
		olWisData.Sort(CompareCodes);
		for(int i=ilSize;--i>=0;)
		{
			prlWis = &olWisData[i];
			popWisData->Add(prlWis);
			olUrno.Format("%i", prlWis->Urno);
			olUrnos += olUrno;
		}
	}

	return olUrnos;
CCS_CATCH_ALL;
	return "";
}*/

/*****************************************************************************************
f�llt den Array popWisData mit allen WISDATAs, bei denen WISDATA.ORGC == opOrg ist
R�ckgabe: Anzahl popWisData-Datens�tze
*****************************************************************************************/
int CedaWisData::GetWisArrayByOrg(CString opOrg, CCSPtrArray<WISDATA> *popWisData)
{
CCS_TRY;
	popWisData->DeleteAll();
	
	WISDATA* prlWis;
	for(int ilCount=0; ilCount<omData.GetSize(); ilCount++)
	{
		prlWis = (WISDATA*)omData.CPtrArray::GetAt(ilCount);

		if(!strcmp(prlWis->Orgc, opOrg))
		{
			popWisData->Add(prlWis);
		}
	}

	popWisData->Sort(CompareCodes);

	return popWisData->GetSize();
CCS_CATCH_ALL;
	return 0;
}

#define ORG_LIST_SEPARATOR		('\'')
/*****************************************************************************************
f�llt den Array popWisData mit allen WISDATAs, bei denen WISDATA.ORGC == ein von opOrgList ist
R�ckgabe: Anzahl popWisData-Datens�tze
*****************************************************************************************/
int CedaWisData::GetWisArrayByOrgList(CString opOrgList, CCSPtrArray<WISDATA> *popWisData, bool bpAllCodes)
{
CCS_TRY;
	popWisData->DeleteAll();

	if(bpAllCodes)
	{
		*popWisData = omData;
	}
	else
	{
		if(opOrgList.IsEmpty()) return 0;
		
		WISDATA* prlWis;
		int ilFind;
		
		int ilSize = omData.GetSize();
		for(int ilCount=0; ilCount<ilSize; ilCount++)
		{
			prlWis = (WISDATA*)omData.CPtrArray::GetAt(ilCount);
			
			ilFind = opOrgList.Find(prlWis->Orgc);
			if(ilFind != -1)
			{
				int ilNext = ilFind + strlen(prlWis->Orgc);
				// wir haben gefunden, aber noch als ,abc, kontrollieren
				if(	(ilFind == 0 || opOrgList[ilFind-1] == ORG_LIST_SEPARATOR) &&
					(ilNext == ilSize || opOrgList[ilNext] == ORG_LIST_SEPARATOR))
				{
					// prlWis->Orgc ist ein String, der genau in opOrgList vertreten ist
					popWisData->Add(prlWis);
				}
			}
		}
	}

	popWisData->Sort(CompareCodes);

	return popWisData->GetSize();
CCS_CATCH_ALL;
	return 0;
}

/*********************************************************************
Orgc soll in der ORGTAB vorhanden sein
*********************************************************************/
bool CedaWisData::IsValidData(WISDATA *prpWis)
{
	CCS_TRY;
	
	if(strlen(prpWis->Orgc) > 0 && !ogOrgData.GetOrgByDpt1(CString(prpWis->Orgc)))
	{
		// ORGCODE nicht gefunden!
		CString olErr;
		olErr.Format("Employee Request Table entry '%s' is defect: code '%s' is not found in Organisational Unit Table\n",
			prpWis->Wisc, prpWis->Orgc);
		ogErrlog += olErr;

		if(GetDefectDataString(ogRosteringLogText, (void *)prpWis), "WIS.ORGC not found in ORGTAB")
		{
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;
	}
	return true;
	CCS_CATCH_ALL;
	return false;
}

static int CompareCodes(const WISDATA **e1, const WISDATA **e2)
{
	if((**e1).Wisc>(**e2).Wisc)
		return 1;
	else
		return -1;
}