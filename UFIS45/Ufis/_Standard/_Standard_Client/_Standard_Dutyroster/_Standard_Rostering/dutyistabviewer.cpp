// DutyIsTabViewer.cpp 
//
#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// Local function prototype
static void DutyIsTabViewerCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//**************************************************************************************
// DutyIsTabViewerCf: globale Funktion als Callback f�r den Broadcast-Handler
//**************************************************************************************

static void DutyIsTabViewerCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	DutyIsTabViewer *polViewer = (DutyIsTabViewer *)popInstance;

	switch(ipDDXType)
	{
	default:
		break;
	case DRA_NEW	:
	case DRA_CHANGE	:
		polViewer->ProcessDraChange((DRADATA *)vpDataPointer);
		break;
	case DRR_NEW	:
	    polViewer->ProcessDrrChange((DRRDATA *)vpDataPointer);
		break;
	case DRR_CHANGE	:		
	    polViewer->ProcessDrrChange((DRRDATA *)vpDataPointer);
ogBackTrace += ",40";
		break;
	case DRR_DELETE	:	
	    polViewer->ProcessDrrDelete((DRRDATA *)vpDataPointer);
		break;
	case RELDRR		:		
		polViewer->ProcessReload();
		break;
	case BC_DRW_NEW	:	
	    polViewer->ProcessDrwChange(ipDDXType,vpDataPointer);
		break;
	case BC_DRW_DELETE	:
	    polViewer->ProcessDrwChange(ipDDXType,vpDataPointer);
		break;
	case BC_DRW_CHANGE	:
	    polViewer->ProcessDrwChange(ipDDXType,vpDataPointer);
		break;
	}
}

//**************************************************************************************
//**************************************************************************************
// Implementation der Klasse DutyIsTabViewer
//**************************************************************************************
//**************************************************************************************

//**************************************************************************************
// Konstruktor
//**************************************************************************************

DutyIsTabViewer::DutyIsTabViewer(CWnd* pParent /*=NULL*/)
{
	pomParent = (DutyRoster_View*)pParent;

    pomDynTab	= NULL;
	prmViewInfo = NULL;
	pomStfData  = NULL;
	pomGroups   = NULL;

	//Insert all Hollidays from HOLTAB in omHollidayKeyMap to use for header colors 
    omHolidayKeyMap.RemoveAll();
	CString olTmp;
	RecordSet *polRecord = new RecordSet(ogBCD.GetFieldCount("HOL"));
	for(int i = 0; i < ogBCD.GetDataCount("HOL"); i++)
	{
		ogBCD.GetRecord("HOL",i, *polRecord);
		olTmp =  polRecord->Values[ogBCD.GetFieldIndex("HOL","HDAY")];
		omHolidayKeyMap.SetAt(olTmp,NULL);
	}
	delete polRecord;

	// beim Broadcast-Handler anmelden
	Register();
}

//**************************************************************************************
// Destruktor
//**************************************************************************************

DutyIsTabViewer::~DutyIsTabViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
	omSelectedField.DeleteAll();
    omHolidayKeyMap.RemoveAll();

}

//**************************************************************************************
// Attach: setzt den Zeiger auf die DynTable und auf die Datenhaltung der DynTable.
// R�ckgabe:	keine
//**************************************************************************************

void DutyIsTabViewer::Attach(CCSDynTable *popTable)
{
    pomDynTab = popTable;
	prmTableData = pomDynTab->GetTableData();
}

//**************************************************************************************
// Register: meldet die Instanz beim Broadcast-Handler an.
// R�ckgabe:	keine
//**************************************************************************************

void DutyIsTabViewer::Register()
{
CCS_TRY;
	DdxRegister(this, DRA_CHANGE,		"DutyIsTabViewer", "DRA_CHANGE",	    DutyIsTabViewerCf);
	DdxRegister(this, DRA_NEW,			"DutyIsTabViewer", "DRA_NEW",		    DutyIsTabViewerCf);

	DdxRegister(this, DRR_CHANGE,		"DutyIsTabViewer", "DRR_CHANGE",	    DutyIsTabViewerCf);
	DdxRegister(this, DRR_NEW,			"DutyIsTabViewer", "DRR_NEW",		    DutyIsTabViewerCf);
	DdxRegister(this, DRR_DELETE,		"DutyIsTabViewer", "DRR_DELETE",		DutyIsTabViewerCf);
	DdxRegister(this, RELDRR,			"DutyIsTabViewer", "RELDRR",			DutyIsTabViewerCf);

    DdxRegister(this, BC_DRW_NEW,          "DutyIsTabViewer", "BC_DRW_NEW",          DutyIsTabViewerCf);
    DdxRegister(this, BC_DRW_CHANGE,       "DutyIsTabViewer", "BC_DRW_CHANGE",       DutyIsTabViewerCf);
	DdxRegister(this, BC_DRW_DELETE,       "DutyIsTabViewer", "BC_DRW_DELETE",       DutyIsTabViewerCf);

CCS_CATCH_ALL;
}

//**************************************************************************************
// Anzahl der Row (Mitarbeiter * Planungstufen) ermitteln
//**************************************************************************************

int DutyIsTabViewer::GetRows()
{
	omPTypes.RemoveAll();	
	// Alle Planungsstufen ermitteln und die Anzahl speichern
	ExtractItemList(prmViewInfo->oPType, &omPTypes, ',');
	int ilNrOfTypes = omPTypes.GetSize();

	// im Prinzip ist das die Anzahl der Rows
	int ilStfSize = 0;
	if(prmViewInfo->iGroup  == 2)
		ilStfSize = pomGroups->GetSize(); //Gruppen-Darstellung
	else
		ilStfSize = pomStfData->GetSize();//Mitarbeiter-Darstellung

	// Anzahl der "Datenobjekte" mit den Planungstufen multiplizieren
	// um ein Anzahl der endgl�tigen Rows zu ermitteln.
	ilStfSize = (ilStfSize*ilNrOfTypes)-1;
	if(ilStfSize < 0) ilStfSize = 0;

	return (ilStfSize);
}

/**************************************************************************************
wenn mindestens ein Feld selektiert ist, wird der erste zur�ckgegeben, sonst 0
**************************************************************************************/
SELECTEDFIELD* DutyIsTabViewer::GetFirstSelectedField()
{
	if(!omSelectedField.GetSize()) 
		return 0;

	imIndex_SelectedField = 0;
	return GetNextSelectedField();
}

/**************************************************************************************
wenn mehr als ein Feld selektiert ist, gibt diese Funktion nacheinander alle 
selektierten Felder, nach dem letzten Feld - 0
**************************************************************************************/
SELECTEDFIELD* DutyIsTabViewer::GetNextSelectedField()
{
	if(imIndex_SelectedField >= omSelectedField.GetSize())
		return 0;

	SELECTEDFIELD* polSelectedField = &omSelectedField[imIndex_SelectedField];
	
	imIndex_SelectedField++;
	
	return polSelectedField;
}

//**************************************************************************************
// 1. Ermittelt die Anzahl der Rows und Columns
// 2. InitTableHeaderArrays(ilColumnOffset);
// 3. MakeTableData(ilColumnOffset,ilRowOffset);
// 4. initialisiert die Scrollbars
// 5. zeichnet die Dyntable neu
//**************************************************************************************

void DutyIsTabViewer::ChangeView(bool bpSetHNull /*=true*/,bool bpSetVNull /*=true*/)
{
	CCS_TRY;
	// Hier ist absolute Vorsicht geboten, falls man das je woanders einbauen will.
	pomStfData  = &pomParent->omStfData;
	prmViewInfo = &pomParent->rmViewInfo;
	pomGroups   = &pomParent->omGroups;
	
	omSelectedField.DeleteAll();

	// Anzahl der Row (Mitarbeiter * Planungstufen) ermitteln
	int ilStfSize = GetRows();

	//*********************************************************************************
	// Anzahl der Tage ermitteln
	int ilDays = 0;

	// G�ltigkeit der Datumsangaben pr�fen
	if(prmViewInfo->oDateFrom.GetStatus() == COleDateTime::valid && prmViewInfo->oDateTo.GetStatus() == COleDateTime::valid &&
		prmViewInfo->oShowDateFrom.GetStatus() == COleDateTime::valid && prmViewInfo->oShowDateTo.GetStatus() == COleDateTime::valid)
	{
		omShowFrom = prmViewInfo->oShowDateFrom;
		omShowTo   = prmViewInfo->oShowDateTo;
		omFrom	   = prmViewInfo->oDateFrom;
		omTo       = prmViewInfo->oDateTo;

		COleDateTimeSpan olDays = omShowTo - omShowFrom;
		imDays = olDays.GetDays()+1;
		ilDays = imDays - 1;
	}
	else
	{
		imDays = 0;
		ilDays = 0;
	}
	//*********************************************************************************

	int ilColumnStartPos = 0;
	int ilRowStartPos = 0;
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);

	if(!bpSetHNull)
	{
		ilColumnStartPos = rlTableInfo.iColumnOffset;
		//Berichtigung des Offset
		int ilColumnDiff = (ilDays+1) - ilColumnStartPos;
		if(ilColumnDiff < rlTableInfo.iColumns)
		{
			ilColumnStartPos = ilColumnStartPos - (rlTableInfo.iColumns - ilColumnDiff);
		}
		if(ilColumnStartPos < 0)
			ilColumnStartPos = 0;
	}
	if(!bpSetVNull)
	{
		ilRowStartPos = rlTableInfo.iRowOffset;
		//Berichtigung des Offset
		int ilRowDiff = (ilStfSize + 1) - ilRowStartPos;
		if(ilRowDiff < rlTableInfo.iRows)
		{
			ilRowStartPos = ilRowStartPos - (rlTableInfo.iRows - ilRowDiff);
		}
		if(ilRowStartPos < 0)
			ilRowStartPos = 0;
	}

	// Scrollbar initialisieren
	pomDynTab->SetHScrollData(0,ilDays,ilColumnStartPos);
	pomDynTab->SetVScrollData(0,ilStfSize,ilRowStartPos);

	// Header der Tabelle f�llen (Text & Farbe) (bei ersten F�llen)
	InitTableHeaderArrays(ilColumnStartPos);
	// f�llt die DynTable mit Daten (bei ersten F�llen)
	MakeTableData(ilColumnStartPos,ilRowStartPos);
	// Dyntable neu zeichnen.
	pomDynTab->InvalidateRect(NULL);
	CCS_CATCH_ALL;
}

//**********************************************************************************************************************
// RedrawBkColorOfField: zeichnet die Hintergrundfarbe eines Feldes neu. Die
//	Koordinaten des Feldes werden berechnet.
// R�ckgabe:	keine
//**********************************************************************************************************************

void DutyIsTabViewer::RedrawBkColorOfField(int ipRow, int ipRowOffset, int ipColumn,
										   int ipColumnOffset, int ipField)
{
CCS_TRY;
	// Datenhaltungsobjekt der CCSDynTable
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	// absolute Bildschirmkoordinaten des Feldes berechnen
	int ilAbsolutRow	= ipRow + ipRowOffset;
	int ilAbsolutColumn = ipColumn + ipColumnOffset;
	// relative (??? wozu ???) Bildschirmkoordinaten berechnen
	int ilRelativRow	= ilAbsolutRow - rlTableInfo.iRowOffset;
	int ilRelativColumn = ilAbsolutColumn - rlTableInfo.iColumnOffset;
	// wenn im sichtbaren Bereich...
	if(ilRelativRow >= 0 && ilRelativRow < rlTableInfo.iRows && ilRelativColumn >= 0 && ilRelativColumn < rlTableInfo.iColumns)
	{
		// ... alte Hintergrundfarbe wiederherstellen
		ChangeFieldBkColor(ilRelativRow, ilRelativColumn, omSelectedField[ipField].iOldBkColor);
	}
CCS_CATCH_ALL;
}

/***************************************************************************
holt selektierte DRRDATA
R�ckgabe: Anzahl DRRs
***************************************************************************/
int DutyIsTabViewer::GetSelectedDrr(int ipField, DRRDATA_SELECTION* popDrrSel)
{
	// Paremeter f�r Feld-Koordinaten
	int ilRow			= omSelectedField[ipField].iRow;
	int ilColumn		= omSelectedField[ipField].iColumn;
	int ilRowOffset		= omSelectedField[ipField].iRowOffset;
	int ilColumnOffset	= omSelectedField[ipField].iColumnOffset;
	int ilStfRow		= 0;	// Mitarbeiter-Position
	int ilPTypeNr		= 0;	// ...als Int
	
	// Feldposition und Planungstyp ermitteln
	CString omPType;
	GetElementAndPType(ilRow+ilRowOffset, ilStfRow, omPType, ilPTypeNr);
	strcpy(popDrrSel->cmPType, (LPCTSTR)omPType);
	
	// g�ltiger Feldwert: MA-Urno oder (je nach View-Einstellung 'Sortierung/Gruppierung')
	// Gruppen-Urno innerhalb der Array-Grenzen der internen Datenhaltung?
	if((ilStfRow >= pomStfData->GetSize() && prmViewInfo->iGroup == 1)||(ilStfRow >= pomGroups->GetSize() && prmViewInfo->iGroup == 2))
	{
		// nein -> zur�ck
		return 0;
	}
	
	// MA-Urno aus dem jeweiligen Array ermitteln
	if(prmViewInfo->iGroup == 2)	// Gruppen-Urno
	{
		popDrrSel->lmStfUrno = (*pomGroups)[ilStfRow].lStfUrno;
	}
	else
	{
		popDrrSel->lmStfUrno = (*pomStfData)[ilStfRow].Urno;	// Mitarbeiter-Urno
	}

	if(popDrrSel->lmStfUrno == 0)
	{
		// kein g�ltiger MA vorhanden, es kann kein DRRDATA geben
		return 0;
	}
	
	// den aktuellen Tag ermitteln aus Starttag der Ansicht und Offset
	COleDateTime olDay = omShowFrom;
	olDay += COleDateTimeSpan(ilColumn+ilColumnOffset,0,0,0);
	strcpy(popDrrSel->omSday, (LPCTSTR)olDay.Format("%Y%m%d"));// volles Datum f�r DRR-Abfrage

	popDrrSel->pomDrrList = new CCSPtrArray<DRRDATA>;	// don't forget to clean later!
	if(!popDrrSel->pomDrrList) return 0;
	
	// DRR-Daten durchsuchen
	return ogDrrData.GetDrrListByKeyWithoutDrrn(CString(popDrrSel->omSday), popDrrSel->lmStfUrno, CString(popDrrSel->cmPType), popDrrSel->pomDrrList);
}

/*****************************************************************************************************************
updatet alle selektierte Drrs und l�scht DRRDATAs aus dem DRRDATA_SELECTION
CString opNewFctc - wenn nicht leer, dann mu� diese Funktion �bernommen werden 
(kommt aus Coverage/Statistik per DragDrop)
******************************************************************************************************************/

bool DutyIsTabViewer::UpdateSelectedDrr(DRRDATA_SELECTION* popDrrSel, DRRDATA* popDrr, int ipCodeType, long lpNewShiftUrno, CString opNewFctc, CString opDrrn/*="1"*/, bool bpAddDrr, int ipShiftCount /*= 1*/)
{
	CCS_TRY;
	if(!popDrrSel || !popDrrSel->lmStfUrno) return true;

	bool blShiftEnabled = true;		// ShiftCheck kann die �nderung verbieten
	bool blRet = true;
	
	if(popDrr == NULL)
	{
		// nein -> erzeugen
		// welchen Planungsstatus soll der neue Datensatz haben?
		CString olRoss = "";
		// ACHTUNG: "L" hier n�tig ?
		// Die nachfolgende vereinfachte Regelpr�fung geht davon aus, da� cmPType nur ein char lang ist, darum _ASSERT()
		_ASSERT(strlen(popDrrSel->cmPType)==1);		
		if ((popDrrSel->cmPType[0] == '2') || (popDrrSel->cmPType[0] == '3') || (popDrrSel->cmPType[0] == '4') || (popDrrSel->cmPType[0] == '5'))
			olRoss = "A";
		
		popDrr = ogDrrData.CreateDrrData(popDrrSel->lmStfUrno, CString(popDrrSel->omSday), CString(popDrrSel->cmPType), olRoss, opDrrn, false/*no InsertInternal()*/);
		if (popDrr == NULL)
		{
			// Datensatz immer noch null -> Fehler
			return false;
		}
		
		// Das Ein-/Austrittsdatum pr�fen
		if(!ogDrrData.IsNewShiftWithinEmployerWorkTime(popDrr->Stfu, lpNewShiftUrno, popDrr)) 
		{
			return false;
		}

		// Schichtcode und alle davon abh�ngigen Zeit-Infos einstellen
		if (!ogDrrData.SetDrrDataScod(popDrr,(ipCodeType==CODE_IS_BSD)/*Basisschicht oder Abwesenheit?*/,
			lpNewShiftUrno, opNewFctc, true, pomParent->rmViewInfo.bCheckMainFuncOnly))
		{
			// -> nicht erfolgreich
			return false;
		}
		
		blRet = pomParent->InsertShift(popDrr, true);

	}
	else
	{
		// Datensatz hinzuf�gen (Drag'n Drop mit gedruckter Control-Taste)?
		// Pr�fen, ob diese Zelle leer ist, wenn ja - ersetzen statt hinzuf�gen
		if (bpAddDrr && popDrr->Avfr.GetStatus() == COleDateTime::valid)
		{
			// ja, Schicht einf�gen
			blRet = pomParent->InsertShiftEx(popDrr, ipCodeType, lpNewShiftUrno, opNewFctc);
		}
		else
		{
			// nein -> Datensatz checken und wenn legal, ersetzen
			
			// Pr�fen ob der Schichtcode ge�ndert werden darf. (DRD und DRA Check und l�schen)
			lpNewShiftUrno = ogDrrData.CheckBSDCode(popDrr, lpNewShiftUrno, popDrr->Bsdu);
			// �nderung erlaubt ?
			if ((lpNewShiftUrno != popDrr->Bsdu) || (opNewFctc != CString(popDrr->Fctc)))
			{
				// Der Schichtcode darf ge�ndert werden
				// Schichtcode und alle davon abh�ngigen Zeit-Infos einstellen
				// Doppeltour wird ebenfalls umgesetzt
				DRRDATA rlDrrUndo;
				DRRDATA *prlDrrData2 = ogDrrData.GetDrrByKey(popDrr->Sday,popDrr->Stfu,"2",popDrr->Rosl);

				// save DRR
				ogDrrData.CopyDrr(&rlDrrUndo, popDrr);

				// try to set the new shift-code
				if(!ogDrrData.SetDrrDataScod(popDrr, (ipCodeType==CODE_IS_BSD),lpNewShiftUrno, opNewFctc, true, pomParent->rmViewInfo.bCheckMainFuncOnly))
					return false;

				// delete the second shift
				if (prlDrrData2 != NULL && opDrrn == "1")
				{
					ogShiftCheck.UpdateData(prlDrrData2, DRR_DELETE);
				}

				// now we can try the ShiftCheck
				// check config file setting
				char pclConfigString[10];
				BOOL EnableTeilzeitplusQuestion = false;
				GetPrivateProfileString(pcgAppName, "EnableOT",  "FALSE",pclConfigString, sizeof pclConfigString, pcgConfigPath);
				if (stricmp(pclConfigString,"TRUE")==0)
					EnableTeilzeitplusQuestion = true;
				blShiftEnabled = ogShiftCheck.DutyRosterCheckIt(popDrr, pomParent->rmViewInfo.bDoConflictCheck, DRR_CHANGE, EnableTeilzeitplusQuestion);
				if(blShiftEnabled)
				{
					// Datensatz �ndern (BC senden)
					ogDrrData.Update(popDrr);
					if (prlDrrData2 != NULL && ipShiftCount == 1)
					{
						ogDrrData.Delete(prlDrrData2,TRUE);
					}
					ogDrrData.CheckAndSetDraIfDrrBsdDelConnected(popDrr, popDrr->Bsdu);
				}
				else
				{
					if(ogShiftCheck.IsTeilzeitplusConfirmed())
					{
						// Diese Schicht ist durchgefallen und wurde nach Teilzeit+ gefragt, Antwort positiv
						// Teilzeit+ setzen, checken und Tagesdialog aufrufen
						blRet = pomParent->SetAndUpdateTeilzeitPlusShift(popDrr, false);
						if (blRet)
						{
							if (prlDrrData2 != NULL)
								ogDrrData.Delete(prlDrrData2,true);
						}
						else
						{
							// restore the second shift in ShiftCheck's internal data holding
							ogShiftCheck.UpdateData(prlDrrData2, DRR_NEW);
						}
					}
					else
					{
						ogDrrData.CopyDrr(popDrr, &rlDrrUndo);

						// restore the second shift in ShiftCheck's internal data holding
						ogShiftCheck.UpdateData(prlDrrData2, DRR_NEW);

						blRet = false;
					}
				}
			}
			else
			{
				DRRDATA *prlDrrData2 = ogDrrData.GetDrrByKey(popDrr->Sday,popDrr->Stfu,"2",popDrr->Rosl);
				if (prlDrrData2 != NULL && ipShiftCount == 1)
				{
					ogDrrData.Delete(prlDrrData2,TRUE);
				}
			}
		}
	}		
	
	return blRet;
	CCS_CATCH_ALL;
	return false;
}

/**********************************************************************************************************************
UpdateMultiDrrOfFields: aktualisiert die DRRs aller selektierten Felder 
	<omSelectedField[]>. Die Felder SCOD der korrepondierenden DRRs erhalten
	den Wert <opNewShiftCode>. Alle ge�nderten und neuen DRRs werden in der
	Datenbank gespeichert.
R�ckgabe:	keine
History:
31.08.00	BDA		�berarbeitet vom single UpdateDrrOfField zu Multiple... wegen der omSelectedField-L�schen-Fehler
CString opNewFctc - wenn nicht leer, dann mu� diese Funktion �bernommen werden 
(kommt aus Coverage/Statistik per DragDrop)
**********************************************************************************************************************/
void DutyIsTabViewer::UpdateMultiDrrOfFields(int ipCodeType,long lpNewShiftUrno, CString opNewFctc, CString opDrrn, bool bpAddDrr, int ipShiftCount /*= 1*/)
{
	CCS_TRY;

	// lpNewShiftUrno checken
	if (ipCodeType == CODE_IS_ODA) 
	{
		// Abwesenheit wurde eingegeben
		if(!ogOdaData.GetOdaByUrno(lpNewShiftUrno))
			return;
	} 
	else if (ipCodeType == CODE_IS_BSD) 
	{
		// Schichtcode wurde eingegeben
		if(!ogBsdData.GetBsdByUrno(lpNewShiftUrno))
			return;
	}
	else
	{
		return;		// diesen Typ kennen wir nicht!
	}
	

	// Anzahl der ausgew�hlten Felder ermitteln
	int ilSelectedFields = omSelectedField.GetSize();
	// Entsprechende Anzahl DRRDATA_SELECTION vorbereiten
	DRRDATA_SELECTION* polDrrSel = (DRRDATA_SELECTION*)malloc(sizeof(DRRDATA_SELECTION)*ilSelectedFields);
	if(!polDrrSel) return;

	// Alle DRRs einlesen
	for(int ilField=0; ilField<ilSelectedFields; ilField++)
	{
		// wenn ein von selektierten Feldern kein Update erlaubt, 
		// wird der entsprechender polDrrSel[ilField] = UINT_MAX gespeichert
		// was sp�ter sein Update verhindert
		GetSelectedDrr(ilField, &polDrrSel[ilField]);
	}	
	
	// damit die ev.erzeugten MessageBoxen nicht �ber dem
	// DragDropDlg zentriert werden sondern �ber dem Hauptfenster:
	AfxGetMainWnd()->SetActiveWindow();

	// alle Felder updaten
	bool blEmptyStfUrno = false;	

	for(ilField=0; ilField<ilSelectedFields; ilField++)
	{
		if(polDrrSel[ilField].lmStfUrno)
		{
			if(!polDrrSel[ilField].pomDrrList) continue;
			int ilDrrListSize = polDrrSel[ilField].pomDrrList->GetSize();
			if(!ilDrrListSize || atoi(opDrrn)>ilDrrListSize)
			{
				if(!UpdateSelectedDrr(&polDrrSel[ilField], 0, ipCodeType, lpNewShiftUrno, opNewFctc, opDrrn, bpAddDrr, ipShiftCount))
				{
					break;
				}
			}
			else
			{
				for(int ilDrrCount=0; ilDrrCount<ilDrrListSize; ilDrrCount++)
				{
					CCSPtrArray<DRRDATA>* polDrrList = polDrrSel[ilField].pomDrrList;
					DRRDATA* polDrr = (DRRDATA*)polDrrList->CPtrArray::GetAt(ilDrrCount);
					//DRRDATA olDrr = polDrrSel[ilField].pomDrrList->GetAt(ilDrrCount);
					if (!strcmp(polDrr->Drrn,opDrrn.GetBuffer(0)))
					{
						if(!UpdateSelectedDrr(&polDrrSel[ilField], polDrr, ipCodeType, lpNewShiftUrno, opNewFctc, opDrrn, bpAddDrr, ipShiftCount))
						{
							break;
						}
					}
				}
			}
		}
		else
		{
			blEmptyStfUrno = true;
		}
	}

	if(blEmptyStfUrno)
	{
		// falls ein von Urnos leer ist, m�ssen wir nachher seine Selektion
		// l�schen, sonst bleibt er weiter als selektierter markiert
		UnselectFields();
	}
	
	// reinigen
	if(polDrrSel != 0)
	{
		// Alle DRRs einlesen
		for(ilField=0; ilField<ilSelectedFields; ilField++)
		{
			// wenn ein von selektierten Feldern kein Update erlaubt, 
			// wird der entsprechender polDrrSel[ilField] = UINT_MAX gespeichert
			// was sp�ter sein Update verhindert
			if(polDrrSel[ilField].pomDrrList != 0)
				delete polDrrSel[ilField].pomDrrList;
		}	
		
		free(polDrrSel);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//// Ende DRR erzeugen, hinzuf�gen oder �ndern
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Nach dem ogShiftCheck.DutyRosterCheckIt() m�ssen wir pr�fen, ob Warnungen vorliegen, wenn ja, ausgeben
	if(pomParent->rmViewInfo.bDoConflictCheck && ogShiftCheck.IsWarning())
	{
		pomParent->ForwardWarnings();
	}

	CCS_CATCH_ALL;
}

/*************************************************************************************
*************************************************************************************/
void DutyIsTabViewer::ProcessDraChange(DRADATA *prpDra)
{
	if (prpDra == NULL)
		return;
	DRRDATA* polDrr = ogDrrData.GetDrrByRoss(prpDra->Sday, prpDra->Stfu, prpDra->Drrn);	// Aktiven Datensatz suchen
	if(polDrr == 0)
		return;

	ProcessDrrChange(polDrr);
}

// ***********************************************************************************
// ProcessDrrChange: bearbeitet den Broadcast DRR_CHANGE (DRR-Datensatz
//	ge�ndert.
// R�ckgabe:	keine
// ***********************************************************************************

void DutyIsTabViewer::ProcessDrrChange(DRRDATA *prpDrr)
{
CCS_TRY;
   
	// Sicherheit zuerst !
	if (prpDrr == NULL)
		return;

//	TRACE("Got DRR Code %s Level %s\n",prpDrr->Scod,prpDrr->Rosl);

	// Zeile und Spalte ermitteln
	int ilRow;
	int ilColumn;
ogBackTrace += ",30";
	if (!GetPositionOfDrr(prpDrr,prpDrr->Rosl,ilRow,ilColumn))
	{
ogBackTrace += ",31";
		bool blReturn = true;
		// DRR nicht gefunden. Sollte ein der aktive DRR ge�ndert worden sein dennoch eintragen
		//---------------------------------------------------------------------------------------
		// ADO 2.5.00 Wenn im Setup gew�nscht, soll im Vorlauf (graue Zone)
		// die aktuelle Stufe angezeigt werden
		// bda: dann setzen wir das "return" aus, d.h. dieser Drr soll weiter bearbeitet werden
		//---------------------------------------------------------------------------------------

		// Setup Einstellung
		if (ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_VORLAUF) == 1)
		{	
ogBackTrace += ",31_1";
			COleDateTime olDay;
			ogDrrData.SdayToOleDateTime(prpDrr, olDay);
ogBackTrace += ",31_2";
			// Ist es ein DRR aus Vorlauf/Nachlauf-Zeit?
			if(olDay < omFrom && olDay >= omShowFrom || olDay > omTo && olDay <= omShowFrom)
			{
ogBackTrace += ",31_3";
				// Wurde ein aktiver DRR ge�ndert?
				if (CString(prpDrr->Ross) == "A")
				{
ogBackTrace += ",31_4";
					/// hat er die Rosl "2"? aber warum?
					if (GetPositionOfDrr(prpDrr,"2",ilRow,ilColumn))
					{
ogBackTrace += ",31_5";
						// Element nicht gefunden -> tu nichts und terminiere
						blReturn = false;
					}
				}
			}
		}
ogBackTrace += ",31_6";
		//---------------------------------------------------------------------------------------
		if(blReturn)
		{
ogBackTrace += ",31_7";
			// Element nicht gefunden -> tu nichts und terminiere
			return;
		}
	}
ogBackTrace += ",32";
	// DRR-Nummer == 1 ?
	bool blIsFirstDrr = (CString(prpDrr->Drrn) == "1");

	// Planungsstufe ermitteln:
	CString olPType;
	int ilPTypeNr = 0;
	int ilStfRow;
	// Attribute des Elements an vorgegebener Position auslesen
	GetElementAndPType(ilRow, ilStfRow, olPType, ilPTypeNr);

	// Datenstruktur der DynTable
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
ogBackTrace += ",33";
	//---------------------------------------------------------------------------------------------------------------
	// Text setzen
	DRRDATA *prlDrr1 = ogDrrData.GetDrrByKey(prpDrr->Sday,prpDrr->Stfu,"1",prpDrr->Rosl);
	DRRDATA *prlDrr2 = ogDrrData.GetDrrByKey(prpDrr->Sday,prpDrr->Stfu,"2",prpDrr->Rosl);
	CString olText = "";
	if (prlDrr1 != NULL)
	{
		if (prlDrr2 != NULL)
		{
			olText = CString(prlDrr1->Scod) + "/" + CString(prlDrr2->Scod);
		}
		else
		{
			olText = CString(prlDrr1->Scod);
		}
	}
	
	if(ilRow<0||ilColumn<0)
		return;
	ChangeFieldText(ilRow - rlTableInfo.iRowOffset, ilColumn - rlTableInfo.iColumnOffset, olText);
	//---------------------------------------------------------------------------------------------------------------
ogBackTrace += ",34";
	// Ermittelung des Zeitpunkts
	COleDateTime olDay = omShowFrom;
	olDay += COleDateTimeSpan(ilColumn, 0, 0, 0);
	// Sollten die �nderungen �u�erhalb des vom Benutzer �nderbaren Bereichs sein, 
	// dann nur Text ver�ndern (+/- Woche)
	if(olDay < omFrom || olDay > omTo) 
	{
		// Zelle neu zeichnen lassen
		pomDynTab->ChangedFieldData(ilRow, ilColumn);
		return;
	}
	
	//---------------------------------------------------------------------------------------------------------------
	// blauer Balken rechts bei mehreren Schichten (nur bei aktueller Stufe anzeigen)
	// In der Schichtplanstufe zweite Schicht anzeigen
ogBackTrace += ",35";	
	ChangeFieldMarkerColor(ilRow - rlTableInfo.iRowOffset,ilColumn - rlTableInfo.iColumnOffset,BPN_NOCOLOR,MARKER_R_B);
	//if (ogDrrData.GetDrrListByKeyWithoutDrrn(olSday,llTmpUrno,opPType) > 1)
	//if (((CString(prpDrr->Ross) == "A") || ((CString(prpDrr->Rosl) == "1")))  && (ogDrrData.GetDrrCountByKeyWithoutDrrn(prpDrr) > 1))
	if (ogDrrData.GetDrrListByKeyWithoutDrrn(CString(prpDrr->Sday), prpDrr->Stfu, CString(prpDrr->Rosl)) > 1)
	{
		ChangeFieldMarkerColor(ilRow - rlTableInfo.iRowOffset,ilColumn - rlTableInfo.iColumnOffset,BPN_LIGHTBLUE,MARKER_R_B);
	}

ogBackTrace += ",36";
	if(!strcmp(prpDrr->Ross, "A") && strlen(prpDrr->Scod) > 0 && ogOdaData.IsODAAndIsRegularFree(prpDrr->Scod) == CODE_IS_BSD &&
		strcmp(prpDrr->Fctc, (LPCTSTR)ogSpfData.GetMainPfcBySurnWithTime(prpDrr->Stfu, olDay))) 
	{
		// das ist ein BSD und kein ODA
		// Aktuelle Funktion in DRR und die Stammfunktion des Mitarbeiters vergleichen
		// die Funktionen sind nicht gleich, markieren!
		
		// graue Ecke links unten, wenn die Funktion der Schicht nicht mit der 
		// Stammfunktion des Mitarbeiters �bereinstimmt
		ChangeFieldMarkerColor(ilRow - rlTableInfo.iRowOffset,ilColumn - rlTableInfo.iColumnOffset,BPN_SILVER,MARKER_LB_T);
	}
	else
	{
		// Ecke l�schen
		ChangeFieldMarkerColor(ilRow - rlTableInfo.iRowOffset,ilColumn - rlTableInfo.iColumnOffset,BPN_NOCOLOR,MARKER_LB_T);
	}
ogBackTrace += ",37";
	if (blIsFirstDrr)
	{
		//---------------------------------------------------------------------------------------------------------------
		// Schwarze Ecke f�r Doppeltouren (nur bei aktueller Stufe anzeigen)
		ChangeFieldMarkerColor(ilRow - rlTableInfo.iRowOffset,ilColumn - rlTableInfo.iColumnOffset, BPN_NOCOLOR,MARKER_RB_T);
		if ((CString(prpDrr->Ross) == "A") && ogDrrData.HasDoubleTour(prpDrr))
		{
			ChangeFieldMarkerColor(ilRow - rlTableInfo.iRowOffset,ilColumn - rlTableInfo.iColumnOffset, BLACK,MARKER_RB_T);
		}

		//---------------------------------------------------------------------------------------------------------------
		// Sollte die Wunschzeile nicht angezeigt werden 
		// Jedoch nur in der Aktiven Planungstufe
		// Zuerst Ecke l�schen
		ChangeFieldMarkerColor(ilRow - rlTableInfo.iRowOffset,ilColumn - rlTableInfo.iColumnOffset, BPN_NOCOLOR,MARKER_LT_T);
		if (!IsTypeActive("W") && (CString(prpDrr->Ross) == "A"))
		{
			// Urno als String ermitteln.
			CString olStfu;
			olStfu.Format("%ld",prpDrr->Stfu);
			// Sollte ein Wunsch Datensatz vorhanden sein f�r diesen Tag und Mitarbeiter, dann 
			// in die linke Ecke ein Dreieck setzten.
			CString olResult = ogBCD.GetFieldExt("DRW", "SDAY","STFU", prpDrr->Sday,olStfu,"URNO");
			if (!olResult.IsEmpty())
			// Gr�ne Ecke links oben
				ChangeFieldMarkerColor(ilRow - rlTableInfo.iRowOffset,ilColumn - rlTableInfo.iColumnOffset, BPN_GREEN,MARKER_LT_T);
			else
				ChangeFieldMarkerColor(ilRow - rlTableInfo.iRowOffset,ilColumn - rlTableInfo.iColumnOffset, BPN_NOCOLOR,MARKER_LT_T);
		}
ogBackTrace += ",38";		
		bool bRedText = false;
		if(CString(prpDrr->Ross) == "A")
		{
			if (strcmp(prpDrr->Expf, "A"))	// sonst ist schon bearbeitet
			{
				if(ogSprData.AreTimesDifferent(prpDrr))
					bRedText = true;	// Zeiten unterscheiden sich vom AZE
				else if(ogDraData.HasDraOfDrrWithoutDel(prpDrr))
					bRedText = true;
				else if(ogDrdData.HasDrdOfDrrWithoutDel(prpDrr))
					bRedText = true;
				else if(ogDrrData.IsExtraShift(prpDrr->Drs4))
					bRedText = true;
				else if(ogDrrData.IsDrrDifferentFromShift(prpDrr))
					bRedText = true;
				else if (strlen(prpDrr->Repaired) > 0)
					bRedText = true;
			}
		}
ogBackTrace += ",39";
		//---------------------------------------------------------------------------------------------------------------
		// Textfarbe setzen, wenn untert�gige Abwesenheit oder Schichtanfang/-ende unterschiedlich
		// oder Extra Schicht (DRS4 = 1, 2, 8)
		if (bRedText)
		{
			ChangeFieldTextColor(ilRow - rlTableInfo.iRowOffset, ilColumn - rlTableInfo.iColumnOffset,RED);
		}
		else
		{
			// keine Abweichung -> Textfarbe schwarz
			ChangeFieldTextColor(ilRow - rlTableInfo.iRowOffset, ilColumn - rlTableInfo.iColumnOffset,BPN_NOCOLOR);
		}

	} // if (blIsFirstDrr)

	//---------------------------------------------------------------------------------------------------------------
	// Pr�fen ob es sich um ein Editierbares Feld handelt, also Planungstufe aktiv oder Urlaubstufe, Langzeitstufe
	if (CString(prpDrr->Ross) == "A" || (olPType == "U") || (olPType == "L"))
	{
		// Feld nicht Readonly setzen 
		SetFieldReadOnly(ilRow - rlTableInfo.iRowOffset,ilColumn - rlTableInfo.iColumnOffset, false);
		// Hintergrundsetzen
		// G�ltiger Arbeitsvertrag vorhanden ?
		if (ogStfData.IsStfNowValid(prpDrr->Stfu,olDay))
			// -> Ja, dann wei�
			ChangeFieldBkColor(ilRow - rlTableInfo.iRowOffset, ilColumn - rlTableInfo.iColumnOffset,BPN_WHITE);
		else
		// -> Nein, dann rosa
			ChangeFieldBkColor(ilRow - rlTableInfo.iRowOffset, ilColumn - rlTableInfo.iColumnOffset,BPN_SILVERREDLIGHT3);
	}
	else
	{
		// nicht aktiv
		//uhi 14.07.00 Hintergrund grau wenn Ross = 'L'
		if (CString(prpDrr->Ross) == "L" )
			ChangeFieldBkColor(ilRow - rlTableInfo.iRowOffset, ilColumn - rlTableInfo.iColumnOffset,BPN_LIGHTSILVER1);
		else
			ChangeFieldBkColor(ilRow - rlTableInfo.iRowOffset, ilColumn - rlTableInfo.iColumnOffset,BPN_SILVERGREENLIGHT);
		// alle Marker l�schen
		ChangeFieldMarkerColor(ilRow - rlTableInfo.iRowOffset,ilColumn - rlTableInfo.iColumnOffset, BPN_NOCOLOR,MARKER_RT_T);
		ChangeFieldMarkerColor(ilRow - rlTableInfo.iRowOffset,ilColumn - rlTableInfo.iColumnOffset, BPN_NOCOLOR,MARKER_RB_T);
		ChangeFieldMarkerColor(ilRow - rlTableInfo.iRowOffset,ilColumn - rlTableInfo.iColumnOffset, BPN_NOCOLOR,MARKER_LT_T);
		// Feld Readonly setzen 
		SetFieldReadOnly(ilRow - rlTableInfo.iRowOffset,ilColumn - rlTableInfo.iColumnOffset, true);
	}

	//---------------------------------------------------------------------------------------------------------------

	// F�r die Schichtplanstufe ist der Hintergrund immer GRAU
	if (olPType == "1")
	{
		// nicht aktiv, grau
		ChangeFieldBkColor(ilRow - rlTableInfo.iRowOffset, ilColumn - rlTableInfo.iColumnOffset,BPN_LIGHTSILVER1);
	}
	
	// Zelle neu zeichnen lassen
	pomDynTab->ChangedFieldData(ilRow, ilColumn);


CCS_CATCH_ALL;
}

/***********************************************************************************
ProcessDrrDelete: bearbeitet den Broadcast DRR_DELETE (DRR-Datensatz
gel�scht). BDA: auch das L�schen von Drrn=1 ist f�r Rosl == "U" m�glich
R�ckgabe:	keine
***********************************************************************************/

void DutyIsTabViewer::ProcessDrrDelete(DRRDATA *prpDrr)
{
CCS_TRY;
	// Sicherheit zuerst !
	if (prpDrr == NULL)
		return;

	// Zeile und Spalte ermitteln
	int ilRow;
	int ilColumn;
	if (!GetPositionOfDrr(prpDrr,prpDrr->Rosl,ilRow,ilColumn))
	{
		// Element nicht gefunden -> tu nichts und terminiere hierher kommt die Funktion bei MultipleSelect 
		// bei der 2. und n�chsten Drrs
		return;
	}

	if(!strcmp(prpDrr->Drrn, "1"))
	{
		if(!strcmp(prpDrr->Rosl, "U"))
		{
			// das L�schen ist nur bei "U" implementiert (man blickt ja kaum durch, wann was gel�scht werden darf;(
			ChangeFieldText(ilRow, ilColumn, CString("-"));
			//return;
		}
		//else
		//{
			// Doch, das darf er sehr wohl !
			// ASSERT: der erste DRR darf nicht gel�scht werden, wenn Rosl != "U" ist
			//ASSERT(CString(prpDrr->Drrn) != "1");
			//return;
		//}
	}
	else if(!strcmp(prpDrr->Drrn, "2"))
	{
		// there is only drrn=1 left
		CString olText;
		DRRDATA *prlDrrData = ogDrrData.GetDrrByKey(prpDrr->Sday,prpDrr->Stfu,"1",prpDrr->Rosl);
		if (prlDrrData != NULL)
			olText = CString(prlDrrData->Scod);
		ChangeFieldText(ilRow, ilColumn, olText);
	}

	// Planungsstufe ermitteln:
	CString olPType;
	int ilPTypeNr = 0;
	int ilStfRow;
	// Attribute des Elements an vorgegebener Position auslesen
	GetElementAndPType(ilRow, ilStfRow, olPType, ilPTypeNr);

	// Datenstruktur der DynTable
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);

	// Ermittelung des Zeitpunkts
	COleDateTime olDay = omShowFrom;
	olDay += COleDateTimeSpan(ilColumn, 0, 0, 0);
	// �nderung im nicht-editierbaren Bereich (+/- Woche) ?
	if(olDay < omFrom || olDay > omTo) 
	{
		// ja -> kein Neuzeichnen n�tig -> abbrechen
		return;
	}

	//---------------------------------------------------------------------------------------------------------------
	// blauer Balken rechts bei mehreren Schichten (nur bei aktueller Stufe anzeigen)
	// In der Schichtplanstufe zweite Schicht anzeigen
	ChangeFieldMarkerColor(ilRow - rlTableInfo.iRowOffset,ilColumn - rlTableInfo.iColumnOffset, BPN_NOCOLOR,MARKER_R_B);
	if (((CString(prpDrr->Ross) == "A") || (CString(prpDrr->Rosl) == "1")) && ((ogDrrData.GetDrrCountByKeyWithoutDrrn(prpDrr)-1) > 1))
	{
		ChangeFieldMarkerColor(ilRow - rlTableInfo.iRowOffset,ilColumn - rlTableInfo.iColumnOffset,BPN_LIGHTBLUE,MARKER_R_B);
	}

	// Zelle neu zeichnen lassen
	pomDynTab->ChangedFieldData(ilRow, ilColumn);
CCS_CATCH_ALL;
}

// alter Code
/*
ACHTUNG!!! Die Funktion geht davon aus, dass der erste DRR
(DRRN == "1") nie gel�scht wird, daher werden nur Ecken und Marker neu 
gezeichnet.
void DutyIsTabViewer::ProcessDrrDelete(DRRDATA *prpDrr)
{
CCS_TRY;
	// Sicherheit zuerst !
	if (prpDrr == NULL)
		return;

	// Zeile und Spalte ermitteln
	int ilRow;
	int ilColumn;
	if (!GetPositionOfDrr(prpDrr,ilRow,ilColumn))
	{
		// Element nicht gefunden -> tu nichts und terminiere
		return;
	}

	// ASSERT: der erste DRR darf nicht gel�scht werden
//	ASSERT(CString(prpDrr->Drrn) != "1");

	// Planungsstufe ermitteln:
	CString olPType;
	int ilPTypeNr = 0;
	int ilStfRow;
	// Attribute des Elements an vorgegebener Position auslesen
	GetElementAndPType(ilRow, ilStfRow, olPType, ilPTypeNr);

	// Datenstruktur der DynTable
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	
	// Ermittelung des Zeitpunkts
	COleDateTime olDay = omShowFrom;
	olDay += COleDateTimeSpan(ilColumn, 0, 0, 0);
	// �nderung im nicht-editierbaren Bereich (+/- Woche) ?
	if(olDay < omFrom || olDay > omTo) 
	{
		// ja -> kein Neuzeichnen n�tig -> abbrechen
		return;
	}

	//---------------------------------------------------------------------------------------------------------------
	// blauer Balken rechts bei mehreren Schichten (nur bei aktueller Stufe anzeigen)
	ChangeFieldMarkerColor(ilRow - rlTableInfo.iRowOffset,ilColumn - rlTableInfo.iColumnOffset, BPN_NOCOLOR,MARKER_R_B);
	if ((CString(prpDrr->Ross) == "A") && ((ogDrrData.GetDrrCountByKeyWithoutDrrn(prpDrr)-1) > 1))
	{
		ChangeFieldMarkerColor(ilRow - rlTableInfo.iRowOffset,ilColumn - rlTableInfo.iColumnOffset,BPN_LIGHTBLUE,MARKER_R_B);
	}

	// Zelle neu zeichnen lassen
	pomDynTab->ChangedFieldData(ilRow, ilColumn);
CCS_CATCH_ALL;
}
*/

// ***********************************************************************************
// GetPositionOfDrr: ermittelt aus dem DRR <prpDrr> die Position des DRR in
//	der DynTable.
// R�ckgabe:	true	->	alles OK
//				false	-> Fehler (ViewInfo ist nicht korrekt oder DS nicht gefunden)
// ***********************************************************************************

bool DutyIsTabViewer::GetPositionOfDrr(DRRDATA *prpDrr, CString olRosl,int &ipRow, int &ipColumn)
{
CCS_TRY;
	if(prmViewInfo == NULL || prmViewInfo->bCorrectViewInfo == false || pomDynTab == NULL)
		return false;

	int ilStfSize;	// Anzahl der MA-Urnos in Tabelle
	int ilPTypeSize = omPTypes.GetSize(); // Anzahl der geladenen Planungsstufen
	int i;	// Z�hlvariable
	// Zeilennummer des zu aktualisiereneden Elementes auf Bildschirm: 
	// (Index des MA * angezeigte Planungsstati) + Planungsstatus
	int ilStfNr = -1; // Position des MA in derzeitiger Reihenfolge
	int ilTypeNr = -1; // Nummer der Planungsstatus-Zeile 

	// pr�fen, ob der Datensatz in der lokalen Datenhaltung vorkommt
	if(ogDrrData.GetDrrByUrno(prpDrr->Urno) == NULL)
	{
		// nein -> terminieren
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////777
	//// Zeile ermitteln
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////777
	// Datensatz geh�rt in die Ansicht
	// Datenstruktur der DynTable
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	// Zeile ermitteln aus MA-Urno und Planungsstufe
	if(prmViewInfo->iGroup == 2) // Sortierung nach Gruppen -> MA-Urno in Gruppen-Array suchen (???)
	{
		ilStfSize = pomGroups->GetSize();
		for(i=0; i<ilStfSize; i++)
		{
			if((*pomGroups)[i].lStfUrno == prpDrr->Stfu)
			{
				// Urno gefunden -> Position und Anzahl ermitteln
				ilStfNr = i;
				i = ilStfSize; // f�r Schleifenabbruch
			}
		}
	}
	else	// Urno im MA-Array suchen
	{
		ilStfSize = pomStfData->GetSize();
		for(i=0; i<ilStfSize; i++)
		{
			if((*pomStfData)[i].Urno == prpDrr->Stfu)
			{
				// Urno gefunden -> Position und Anzahl ermitteln
				ilStfNr = i;
				i = ilStfSize; // f�r Schleifenabbruch
			}
		}
	}

	// Nummer des Planungsstatus ermitteln	
	for(i=0; i<ilPTypeSize; i++)
	{
		if(omPTypes[i] == olRosl)
		{
			// Planungsstatus gefunden
			ilTypeNr = i;
			i = ilPTypeSize; // f�r Schleifenabbruch
		}
	}

	// Urno und Planungstyp gefunden?
	if(ilStfNr < 0 || ilTypeNr < 0)
	{
		// nein -> Abbruch
		return false;
	}

	// Zeile berechnen
	ipRow = ilStfNr*ilPTypeSize+ilTypeNr;
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////777
	//// Ende Zeile ermitteln
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////777
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////777
	//// Spalte ermitteln
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////777
	// Tag ermitteln
	COleDateTime olUpdateDay;	
	if (!CedaDataHelper::DateStringToOleDateTime(prpDrr->Sday,olUpdateDay))
	{
		// Fehler beim Ermitteln und Konvertieren des Tages -> Abbruch
		return false;
	}
	COleDateTimeSpan olDayDiv =  olUpdateDay - omShowFrom; // - Anzeige-Offset
	ipColumn = olDayDiv.GetDays();
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////777
	//// Ende Spalte ermitteln
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////777

	// Spalte und Zeile g�ltig und innerhalb des sichtbaren Bereichs?
	if((ipColumn < 0) || // ung�ltige Spalte?
	   (ipColumn < rlTableInfo.iColumnOffset) || // ausserhalb des sichtbaren Bereichs links? 
	   (ipColumn > rlTableInfo.iColumnOffset + rlTableInfo.iColumns) ||  // ausserhalb des sichtbaren Bereichs rechts? 
	   (ipRow < rlTableInfo.iRowOffset) ||  // // ausserhalb des sichtbaren Bereichs oben? 
	   (ipRow > rlTableInfo.iRowOffset + rlTableInfo.iRows)) // ausserhalb des sichtbaren Bereichs unten? 
	{
		// ung�ltige Spalte oder Zeile oder Spalte ausserhalb des sichtbaren Bereichs ->
		// Abbruch
		return false;
	}
	return true;
CCS_CATCH_ALL;
return false;
}

//-----------------------------------------------------------------------------------------------

void DutyIsTabViewer::HorzTableScroll(MODIFYTAB *prpModiTab)
{
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	if(prpModiTab->iID == rlTableInfo.iID)
	{
		MakeTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
		pomDynTab->ChangedTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
	}
	else
	{
		MakeTableData(prpModiTab->iNewHorzPos,rlTableInfo.iRowOffset);
		pomDynTab->ChangedTableData(prpModiTab->iNewHorzPos,rlTableInfo.iRowOffset);
	}
}

//-----------------------------------------------------------------------------------------------

void DutyIsTabViewer::VertTableScroll(MODIFYTAB *prpModiTab)
{
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	if(prpModiTab->iID == rlTableInfo.iID)
	{
		MakeTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
		pomDynTab->ChangedTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
	}
	else
	{
		MakeTableData(rlTableInfo.iColumnOffset,prpModiTab->iNewVertPos);
		pomDynTab->ChangedTableData(rlTableInfo.iColumnOffset,prpModiTab->iNewVertPos);
	}
}

//-----------------------------------------------------------------------------------------------

void DutyIsTabViewer::TableSizing(/*UINT nSide, */LPRECT lpRect)
{
	pomDynTab->SizeTable(/*nSide, */lpRect);
}

//-----------------------------------------------------------------------------------------------

void DutyIsTabViewer::MoveTable(MODIFYTAB *prpModiTab)
{
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	if(prpModiTab->iID == rlTableInfo.iID)
	{
		if(prmTableData->oSolid.GetSize() > 0 && prmTableData->oHeader.GetSize() > 0)
		{
			MakeTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
		}
	}
	else
	{
		int ilX = 0;
		int ilY = 0;
		//ilX = prpModiTab->oNewRect.left   - prpModiTab->oOldRect.left;
		//ilX = prpModiTab->oNewRect.right  - prpModiTab->oOldRect.right;
		//ilY = prpModiTab->oNewRect.top    - prpModiTab->oOldRect.top;
		//ilY = prpModiTab->oNewRect.bottom - prpModiTab->oOldRect.bottom;

		pomDynTab->MoveTable(ilX,ilY);
	}
}

/***************************************************************************************************************
L�scht Selektierung
***************************************************************************************************************/
void DutyIsTabViewer::UnselectFields()
{
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	// delete all selected fields in actuall window
	for(int ilField=0; ilField<omSelectedField.GetSize(); ilField++)
	{
		int ilAbsolutRow	= omSelectedField[ilField].iRow + omSelectedField[ilField].iRowOffset;
		int ilAbsolutColumn = omSelectedField[ilField].iColumn + omSelectedField[ilField].iColumnOffset;
		int ilRelativRow	= ilAbsolutRow - rlTableInfo.iRowOffset;
		int ilRelativColumn = ilAbsolutColumn - rlTableInfo.iColumnOffset;
		if(ilRelativRow >= 0 && ilRelativRow < rlTableInfo.iRows && ilRelativColumn >= 0 && ilRelativColumn < rlTableInfo.iColumns)
		{
			ChangeFieldBkColor(ilRelativRow, ilRelativColumn, omSelectedField[ilField].iOldBkColor);
			pomDynTab->ChangedFieldData(ilAbsolutRow, ilAbsolutColumn);
		}
	}
	omSelectedField.DeleteAll();
	return;
}
	

/******************************************************************************************************************
Auswahl einzelnen Felder zwecks Multiselektion.
******************************************************************************************************************/

void DutyIsTabViewer::SelectUpdateField(INLINEDATA *prpInlineUpdate)
{
	int ilRow			= prpInlineUpdate->iRow;
	int ilColumn		= prpInlineUpdate->iColumn;
	int ilRowOffset		= prpInlineUpdate->iRowOffset;
	int ilColumnOffset	= prpInlineUpdate->iColumnOffset;
	CString olOldText	= prpInlineUpdate->oOldText;

	int ilActualFieldCode;
	int ilTempFieldCode;
	int ilSelectedFields;

	// Code ermitteln
	ilActualFieldCode = GetFieldCode(ilRow+ilRowOffset);
	if (ilActualFieldCode == -1)
	{
		TRACE("DutyIsTabViewer::SelectUpdateField fehlgeschlagen\n");
		return;
	}

	// Anzahl der bisher selektierten Felder erhalten
	ilSelectedFields = omSelectedField.GetSize();

	// ----------------------------------------------------------------------------------------------
	// Ein Feld wird selektiert
	// ----------------------------------------------------------------------------------------------

	if(prpInlineUpdate->iFlag == SELECT || prpInlineUpdate->iFlag == ONLYSELECT)
	{
		// Wenn es sich nicht um das erste selektierte Feld handelt
		// Es d�rfen nur Felder der selben Planungsstufe gleichzeitig markiert sein

		if (omSelectedField.GetSize() > 0)
		{
			ilTempFieldCode = GetFieldCode(omSelectedField[0].iRow+omSelectedField[0].iRowOffset);

			// Neuer Code passt nicht zum bereits selektiertem Code
			if (ilTempFieldCode != ilActualFieldCode)
			{
				TRACE("DutyIsTabViewer: Multiselect fehlgeschlagen, Code inkompatibel\n");
				pomParent->InsertAttention(LoadStg(IDS_STRING1623));
				return;
			}
		}

		//Sucht ob in der Liste der selektierten Felder diese Feld schon vorhanden ist
		for(int ilField=0; ilField<ilSelectedFields; ilField++)
		{
			if((omSelectedField[ilField].iRow + omSelectedField[ilField].iRowOffset) == (ilRow+ilRowOffset) && (omSelectedField[ilField].iColumn + omSelectedField[ilField].iColumnOffset) == (ilColumnOffset+ilColumn))
			{
				// Feld de-selektieren wenn per Mouseclick angesteuert
				if (prpInlineUpdate->iFlag == SELECT)
				{
					ChangeFieldBkColor(ilRow, ilColumn, omSelectedField[ilField].iOldBkColor);
					omSelectedField.DeleteAt(ilField);
					pomDynTab->ChangedFieldData((ilRow + ilRowOffset), (ilColumn + ilColumnOffset));
					ilField = ilSelectedFields;
				}
				return;
			}
		}

		// Diese aktuell gew�hlte Feld in die Auswahlliste mit aufnehmen.		
		SELECTEDFIELD *polSelectedField = new SELECTEDFIELD;
		polSelectedField->iRow	 			= ilRow;
		polSelectedField->iColumn			= ilColumn;
		polSelectedField->iRowOffset		= ilRowOffset;
		polSelectedField->iColumnOffset		= ilColumnOffset;
		polSelectedField->oOldText			= olOldText;
		polSelectedField->iOldBkColor		= ChangeFieldBkColor(ilRow, ilColumn, BPN_SELECTBLUE);
		// Zur Liste der selektierten Felder hinzuf�gen
		omSelectedField.Add(polSelectedField);
		// Feld neu zeichnen, Hintergrundfarbe hat sich ge�ndert
		pomDynTab->ChangedFieldData((ilRow + ilRowOffset), (ilColumn + ilColumnOffset));
	}

	// ----------------------------------------------------------------------------------------------
	// deselektieren aller Felder
	// ----------------------------------------------------------------------------------------------

	else if(prpInlineUpdate->iFlag == UNSELECT)
	{
		UnselectFields();
	}
	return;
}

//*************************************************************************************
// Testen ob der Text in die passende Planungstufe eingetragen wurde
//*************************************************************************************

bool DutyIsTabViewer::IsTextValidInPosition(CString opText,COleDateTime opDay)
{
	int		ilStfRow = 0;
	CString olPType;
	int		ilPTypeNr = 0;
	bool	blReturn = false;
	
	bool	blIsFreeType = false;
	bool	blIsBSD = false;
	bool	blIsODA = false;
	bool	blIsWIS = false;

	//-----------------------------------------------------------------------------
	// Codes ermitteln
	//-----------------------------------------------------------------------------
	//Pr�fen ob ein g�ltige Schicht Code eingegeben wurde 
	BSDDATA* polBsdData = ogBsdData.GetBsdByBsdcAndDate(opText,opDay);
	if(polBsdData != NULL)
	{
		blIsBSD = true;
	}
	//Pr�fen ob ein g�ltige Abwesenheits-Code eingegeben wurde
	ODADATA* polOdaData = ogOdaData.GetOdaBySdac(opText);
	if(polOdaData != NULL)
	{
		blIsODA = true;
		// Regul�r arbeitsfrei ?
		if (ogOdaData.IsODAAndIsRegularFree(opText) == CODE_IS_ODA_REGULARFREE)
		{
			blIsFreeType = true;
		}
		else
		{
			blIsFreeType = false;
		}
	}
	//Pr�fen ob ein g�ltiger Wunsch-Code eingegeben wurde
	if(ogWisData.CheckCode(opText))
	{
		blIsWIS = true;
	}

	int ilSelectedFields = omSelectedField.GetSize();
	if(ilSelectedFields>0)
	{
		// Attribute des Elements an vorgegebener Position auslesen
		GetElementAndPType(omSelectedField[0].iRow+omSelectedField[0].iRowOffset, ilStfRow, olPType, ilPTypeNr);

		//-------------------------------------------------------------------------
		// In die Abwesenheitszeile darf nur ein Abwensenheitscode eingebenen werden
		//-------------------------------------------------------------------------
		if(olPType == "U" && blIsODA)
		{
			blReturn = true;
		}

		//-------------------------------------------------------------------------
		// Langzeitdienstplan
		//-------------------------------------------------------------------------
		if(olPType == "L")
		{
			// Es d�rfen nur Abwesenheitscodes mit Typ "frei" eigegeben werden
			if (blIsODA && blIsFreeType)
				blReturn = true;

			if (blIsODA && !blIsFreeType)
				blReturn = false;

			// Schicht Codes
			if (blIsBSD)
				blReturn = true;
		}
		//-------------------------------------------------------------------------
		// Wunschcode darf nur in Wunschzeile eigegeben werden.
		//-------------------------------------------------------------------------
		if(olPType == "W" && blIsWIS)
		{
			blReturn = true;			
		}
		//-------------------------------------------------------------------------
		// Schichtplan, Dienstplan, MA-Tagesliste, Tagesendbearbeitung, Nachbearbeitung
		//-------------------------------------------------------------------------
		if((olPType == "1") || (olPType == "2") || (olPType == "3") || (olPType == "4") ||(olPType == "5")) 
		{
			// Schicht Codes
			if (blIsBSD)
				blReturn = true;			

			// Abwesenheitscodes 
			if (blIsODA)
				blReturn = true;
		}
	}
	return (blReturn);
}

//*************************************************************************************
// Pr�fen ob der Text ein 1. Schicht 2. Abwesenheit 3. Wunschcode ist
// Pr�ft ob es sich um einen "freien" Typ handelt.
// Falls Code nicht gefunden wurde wird FALSE zur�ckgegeben.
//
//*************************************************************************************

bool DutyIsTabViewer::GetCodeType(COleDateTime opDay,CString opText, int& ipCodeType, bool& bpIsFreeType)
{
	//int ilCodeType = 0;
	bool blReturn = false;
	bpIsFreeType = false;

	// Planungsstufe auswerten
	int		ilDummy1,ilDummy2;
	CString olPType;
	GetElementAndPType(omSelectedField[0].iRow+omSelectedField[0].iRowOffset, ilDummy1, olPType, ilDummy2);

	//Pr�fen ob ein g�ltiger Wunsch-Code eingegeben wurde.Wunschcode darf nur in Wunschzeile stehen
	if(ogWisData.CheckCode(opText) && olPType == "W")
	{
		ipCodeType = CODE_IS_WIS;
		return true;
	}

	//Pr�fen ob ein g�ltiger Schicht Code eingegeben wurde 
	if (ogBsdData.IsBsdCode(opText))
	{
		ipCodeType = CODE_IS_BSD;
		return true;
	}
	
	//Pr�fen ob ein g�ltiger Schicht Code eingegeben wurde 
	/*BSDDATA* polBsdData = ogBsdData.GetBsdByBsdcAndDate(opText,opDay);
	if(polBsdData != NULL)
	{
		ipCodeType = CODE_IS_BSD;
		blReturn = true;

	}*/
	//Pr�fen ob ein g�ltige Abwesenheits-Code eingegeben wurde
	ODADATA* polOdaData = ogOdaData.GetOdaBySdac(opText);
	if(polOdaData != NULL && olPType != "W")
	{
		ipCodeType = CODE_IS_ODA;
		blReturn = true;
		if (strcmp(polOdaData->Free,"J") == 0)
			bpIsFreeType = true;

	}
	return blReturn;
}

//*************************************************************************************
//
//*************************************************************************************

void DutyIsTabViewer::InlineUpdate(INLINEDATA *prpInlineUpdate)
{
	UINT ilFlag         = prpInlineUpdate->iFlag;   //DRAGDROP_UPDATE,IPEDIT_UPDATE
	int ilRow			= prpInlineUpdate->iRow;
	int ilColumn		= prpInlineUpdate->iColumn;
	int ilRowOffset		= prpInlineUpdate->iRowOffset;
	int ilColumnOffset	= prpInlineUpdate->iColumnOffset;
	CString olOldText	= prpInlineUpdate->oOldText;
	CString olNewText	= prpInlineUpdate->oNewText;
	CString olFctc		= prpInlineUpdate->oFctc;
	long	llNewUrno	= prpInlineUpdate->lNewUrno;

	// Controltaste gedr�ckt? (dann = DROPEFFECT_COPY)
	bool blAddDrr = (prpInlineUpdate->iDropeffect == DROPEFFECT_COPY);

	//-------------------------------------------------------------------------------
	// INFO:omSelectedField.beinhaltet alle Felder, die aktuell markiert worden sind
	//-------------------------------------------------------------------------------

	int ilSelectedFields = omSelectedField.GetSize();

	//-------------------------------------------------------------------------------
	// Sollten Felder markiert worden sein, aber der Drop erfolgte in ein das nicht 
	// markiert wurde, dann werden alle markierten Felder gel�scht und der Drop im
	// aktuellen Feld durchgef�hrt
	//-------------------------------------------------------------------------------

	bool blIsFound = false;

	if (ilFlag == DRAGDROP_UPDATE && ilSelectedFields > 0)
	{
		//Sucht ob in der Liste der selektierten Felder diese Feld schon vorhanden ist
		for (int ilField=0; ilField<ilSelectedFields; ilField++)
		{
			if ((omSelectedField[ilField].iRow + omSelectedField[ilField].iRowOffset) == (ilRow+ilRowOffset) &&
				(omSelectedField[ilField].iColumn + omSelectedField[ilField].iColumnOffset)	== (ilColumnOffset+ilColumn))
			{
				blIsFound = true;
				break;
			}
		}
	}

	if (!blIsFound)
	{
		UnselectFields();
		ilSelectedFields = 0;
	}
	
	//-------------------------------------------------------------------------------
	// Es handelt sich um die Auswahl eines Feldes
	//-------------------------------------------------------------------------------

	if (ilSelectedFields ==  0)
	{
		//-------------------------------------------------------------------------------
		// Das Feld mu� in die Liste der selektierten Felder aufgenommen werden
		SELECTEDFIELD *polSelectedField = new SELECTEDFIELD;
		polSelectedField->iRow	 			= ilRow;
		polSelectedField->iColumn			= ilColumn;
		polSelectedField->iRowOffset		= ilRowOffset;
		polSelectedField->iColumnOffset		= ilColumnOffset;
		polSelectedField->oOldText			= olOldText;
		polSelectedField->iOldBkColor		= GetFieldBkColor(ilRow, ilColumn);
		// Feld zuf�gen
		omSelectedField.Add(polSelectedField);
	}
		
	//-------------------------------------------------------------------------------
	// l�schen einer Schicht, Abwesenheit oder eines Wunsches
	// Falls ein leerer String �bergeben wurde, soll das Feld gel�scht werden
	//-------------------------------------------------------------------------------

	// Datum berechnen
	COleDateTime olDay = omShowFrom;
	olDay += COleDateTimeSpan(ilColumn+ilColumnOffset,0,0,0);

	if (olNewText.GetLength() == 0 && olOldText.GetLength() != 0)
	{
		DeleteCode(olDay,olOldText);
		INLINEDATA rlInlineData;
		rlInlineData.iFlag = UNSELECT;
		SelectUpdateField(&rlInlineData);
		omSelectedField.DeleteAll();
		return;
	}

	//-------------------------------------------------------------------------------
	// Pr�fen ob der neue Code g�ltig ist
	// Pr�fen ob der Text ein 1. Schicht 2. Abwesenheit 3. Wunschcode ist
	//-------------------------------------------------------------------------------
	CString olTmpText;

	int ilPos = olNewText.Find(" (");
	if (ilPos > 0)
	{
		olTmpText = olNewText.Left(ilPos);
		olTmpText.TrimLeft();
	}
	else
	{
		olTmpText = olNewText;
	}

	CStringArray olArr;
	CString olExtract = olTmpText;
	CedaData::ExtractTextLineFast (olArr, olExtract.GetBuffer(0), "/");
	//Art des Codes
	int ilCodeType = 0;
	// "freier" typ
	bool blIsFreeType = false;
	for (int i = 0; i < olArr.GetSize(); i++)
	{
		olTmpText = olArr[i];
		//Art des Codes
		ilCodeType = 0;
		// "freier" typ
		blIsFreeType = false;
		if (!GetCodeType(olDay,olTmpText,ilCodeType,blIsFreeType))
		{
			CString olAttentionText;
			olAttentionText.Format(LoadStg(IDS_STRING1602), olTmpText);
			pomParent->InsertAttention(olAttentionText);
			INLINEDATA rlInlineData;
			rlInlineData.iFlag = UNSELECT;
			SelectUpdateField(&rlInlineData);
			omSelectedField.DeleteAll();
			return;
		}
	}
	
	for (i = 0; i < olArr.GetSize(); i++)
	{
		olTmpText = olArr[i];
		//Art des Codes
		ilCodeType = 0;
		// "freier" typ
		blIsFreeType = false;

	//*************
		if (!GetCodeType(olDay,olTmpText,ilCodeType,blIsFreeType))
		{
			CString olAttentionText;
			olAttentionText.Format(LoadStg(IDS_STRING1602), olTmpText);
			pomParent->InsertAttention(olAttentionText);
			INLINEDATA rlInlineData;
			rlInlineData.iFlag = UNSELECT;
			SelectUpdateField(&rlInlineData);
			omSelectedField.DeleteAll();
			return;
		}

		//-------------------------------------------------------------------------------
		// Sollte keine Urno �bergeben worden sein, also Eingabe per Hand, wird 
		// hier die Urno des Codes ermittelt.
		//-------------------------------------------------------------------------------

		if (llNewUrno == 0 || i > 0)
		{
			// wenn ilCodeType == CODE_IS_WIS, wird kein Urno, sondern nur 1 zur�ckgegeben
			llNewUrno = GetUrnoForCode(olTmpText,ilCodeType,olDay);
		}

		if (ilCodeType != CODE_IS_WIS)
		{
			//-------------------------------------------------------------------------------
			// Testen ob der Code an diesem Zeitpunkt g�ltig ist.
			//-------------------------------------------------------------------------------
			
			// Pr�fen ob diese Urno und/oder Code f�r diesen Zeitpunkt g�ltig ist.
			// Falls nicht, versucht die Funktion eine g�ltige Schicht zu finden,
			// Falls auch das misslingt, wird null zur�ckgegeben.
			llNewUrno = ogBsdData.CheckAndGetValidBsdCode(llNewUrno,olTmpText,ilCodeType,olDay);
			if (llNewUrno == 0)
			{
				CString olAttentionText;
				olAttentionText.Format(LoadStg(IDS_STRING420), olTmpText);
				pomParent->InsertAttention(olAttentionText);
				INLINEDATA rlInlineData;
				rlInlineData.iFlag = UNSELECT;
				SelectUpdateField(&rlInlineData);
				omSelectedField.DeleteAll();
				return;
			}
		}

		//-------------------------------------------------------------------------------
		// Testen ob der Text in die passende Planungstufe eingetragen wurde
		//-------------------------------------------------------------------------------

		if (!IsTextValidInPosition(olTmpText,olDay))
		{
			// Warnung ausgeben
			pomParent->InsertAttention(LoadStg(IDS_STRING1624));
			//Selektion l�schen (nicht an der Oberfl�che, nur intern!)
			INLINEDATA rlInlineData;
			rlInlineData.iFlag = UNSELECT;
			SelectUpdateField(&rlInlineData);
			omSelectedField.DeleteAll();
			return;
		}

	//*************


		// Code preparieren, wenn es aus Statistik - DebitTab kommt: 
		CString olNewFctc = "";
		if (olTmpText.Find(" (") != -1)
		{
			// Das kommt aus DebitTab:
			//		" SHIFT1 (FUNC1, 08:00 - 16:00)"
			// oder " SHIFT1 (08:00 - 16:00)"
			olTmpText.TrimLeft();
			//		olNewText = "SHIFT1 (FUNC1, 08:00 - 16:00)"
			// oder olNewText = "SHIFT1 (08:00 - 16:00)"

			int nPos = olTmpText.Find(" (");

			if (olFctc.GetLength() == 0)
			{
				// Additiv ist aktiviert, keine Funktion gespeichert, Meldung ausgeben
				// olNewText = "SHIFT1 (08:00 - 16:00)"
				olTmpText = olTmpText.Left(nPos);
				// olNewText = "SHIFT1"
				BSDDATA* polBsd = 0;
				if (!llNewUrno)
				{
					polBsd = ogBsdData.GetBsdByBsdc(olTmpText);
					if(!polBsd) return;

					// hier ist nur BSD relevant, alles andere nicht
					llNewUrno = polBsd->Urno;
				}
				else
				{
					BSDDATA* polBsd = ogBsdData.GetBsdByUrno(llNewUrno);
					if(!polBsd) return;
				}

				if (strlen(polBsd->Fctc) > 0)
				{
					olNewFctc = polBsd->Fctc;
				}
				else
				{
					//wenn die Basis-Schicht keine Funktion hat, m�ssen wir eine ausw�hlen
					CStringArray	olStrArr;
					CUIntArray		olPfcUrnos;
					CString olTmp = pomParent->rmViewInfo.oPfcStatFctcs;
					CedaData::ExtractTextLineFast (olStrArr, olTmp.GetBuffer (0), ",");
					PFCDATA *polPfcData;
					for (int ilSize = 0; ilSize < olStrArr.GetSize(); ilSize++)
					{
						olTmp = olStrArr[ilSize];
						if (olTmp.GetLength () > 2)
						{
							// the codes are like 'LA', so we have to cut the last and the first char
							olTmp = olTmp.Mid (1, olTmp.GetLength () - 2);
							polPfcData = ogPfcData.GetPfcByFctc (olTmp);
							if (polPfcData != NULL)
							{
								olPfcUrnos.Add (polPfcData->Urno);
							}
						}
					}

					PfcSelectDlg olDlg(pomParent, &olPfcUrnos);
					if (olPfcUrnos.GetSize() > 1)
					{
						if (olDlg.DoModal() == IDOK)
						{
							PFCDATA* polPfcData = ogPfcData.GetPfcByFctc(olDlg.omSelPfcCode);
							if (polPfcData != 0)
							{
								olNewFctc = polPfcData->Fctc;
							}
						}
						else
						{
							// abgelehnt
							return;
						}
					}
					else if (olPfcUrnos.GetSize() == 1)
					{
						PFCDATA* polPfcData = ogPfcData.GetPfcByUrno(olPfcUrnos[0]);
						if (polPfcData != 0)
						{
							olNewFctc = polPfcData->Fctc;
						}
					}
				}
			}
			else
			{
				// Funktion ist da, mu� rausgeholt werden
				olTmpText = olTmpText.Left(nPos);	// olNewText = "SHIFT1"
				olNewFctc = olFctc;					// olNewFctc = "FUNC1"
			}
		}
		else
		{
			if (olNewText != olOldText)
			{
				BSDDATA *polBsdData = ogBsdData.GetBsdByBsdc (olTmpText);
				if (polBsdData != NULL)
				{
					if (strlen(polBsdData->Fctc) > 0)
					{
						olNewFctc = CString(polBsdData->Fctc);
					}
					else
					{
						//wenn die Basis-Schicht keine Funktion hat, m�ssen wir eine ausw�hlen
						CStringArray	olStrArr;
						CUIntArray		olPfcUrnos;
						CString olTmp = pomParent->rmViewInfo.oPfcStatFctcs;
						CedaData::ExtractTextLineFast (olStrArr, olTmp.GetBuffer (0), ",");
						PFCDATA *polPfcData;
						for (int ilSize = 0; ilSize < olStrArr.GetSize(); ilSize++)
						{
							olTmp = olStrArr[ilSize];
							if (olTmp.GetLength () > 2)
							{
								// the codes are like 'LA', so we have to cut the last and the first char
								olTmp = olTmp.Mid (1, olTmp.GetLength () - 2);
								polPfcData = ogPfcData.GetPfcByFctc (olTmp);
								if (polPfcData != NULL)
								{
									olPfcUrnos.Add (polPfcData->Urno);
								}
							}
						}

						PfcSelectDlg olDlg(pomParent, &olPfcUrnos);
						if (olPfcUrnos.GetSize() > 1)
						{
							if (olDlg.DoModal() == IDOK)
							{
								PFCDATA* polPfcData = ogPfcData.GetPfcByFctc(olDlg.omSelPfcCode);
								if (polPfcData != 0)
								{
									olNewFctc = polPfcData->Fctc;
								}
							}
							else
							{
								// abgelehnt
								return;
							}
						}
						else if (olPfcUrnos.GetSize() == 1)
						{
							PFCDATA* polPfcData = ogPfcData.GetPfcByUrno(olPfcUrnos[0]);
							if (polPfcData != 0)
							{
								olNewFctc = polPfcData->Fctc;
							}
						}
					}
				}
			}
		}


		//-------------------------------------------------------------------------------
		// Eingabe an die Datenbank weitergeben
		//-------------------------------------------------------------------------------

		if (ilCodeType == CODE_IS_WIS)
		{
			// W�nsche
			UpdateDRWData(olTmpText);
		}
		else
		{
			// Schichten und Abwesenheiten.
			CString olDrrn;
			olDrrn.Format("%d",i+1);
			UpdateMultiDrrOfFields(ilCodeType,llNewUrno,olNewFctc,olDrrn,blAddDrr,olArr.GetSize());
		}
	}

	// Alle bisher per Mouse ausgew�hlten Felder l�schen ! Vorsicht !
	UnselectFields();
	omSelectedField.DeleteAll();
	//AfxGetApp()->DoWaitCursor(-1);
}

//************************************************************************************************************************
// Die Eingabe in DRW (W�nsche)  Feld wird in die Datenbank �bertragen
//************************************************************************************************************************

void DutyIsTabViewer::UpdateDRWData(CString opNewText)
{
	TABLEINFO rlTableInfo;
//	CCSPtrArray<DRWDATA>  olDsrList;
	int ilSelectedFields, ilRow, ilColumn, ilRowOffset, ilColumnOffset;
	CString olOldText,olStfUrno,olUrno;
	bool blResult;
		
	pomDynTab->GetTableInfo(rlTableInfo);
	// Abzahl der ausgew�hlten Felder ermitteln
	ilSelectedFields = omSelectedField.GetSize();
	
	// f�r jedes Ausgew�hlte Feld bearbeiten
	for(int ilField=0; ilField<ilSelectedFields; ilField++)
	{
		ilRow			= omSelectedField[ilField].iRow;
		ilColumn		= omSelectedField[ilField].iColumn;
		ilRowOffset		= omSelectedField[ilField].iRowOffset;
		ilColumnOffset	= omSelectedField[ilField].iColumnOffset;
		olOldText		= omSelectedField[ilField].oOldText;
		
		// Hintergrundfarbe zur�cksetzen
		int ilAbsolutRow	= ilRow + ilRowOffset;
		int ilAbsolutColumn = ilColumn + ilColumnOffset;
		int ilRelativRow	= ilAbsolutRow - rlTableInfo.iRowOffset;
		int ilRelativColumn = ilAbsolutColumn - rlTableInfo.iColumnOffset;
		if(ilRelativRow >= 0 && ilRelativRow < rlTableInfo.iRows && ilRelativColumn >= 0 && ilRelativColumn < rlTableInfo.iColumns)
		{
			ChangeFieldBkColor(ilRelativRow, ilRelativColumn, omSelectedField[ilField].iOldBkColor);
		}
	
		// Sollte sich der Text ge�ndert haben
		if(olOldText != opNewText)
		{
			int ilStfRow = 0;
			CString olPType;
			int ilPTypeNr = 0;
			GetElementAndPType(ilRow+ilRowOffset, ilStfRow, olPType, ilPTypeNr);
			// ???
			if((ilStfRow < pomStfData->GetSize() && prmViewInfo->iGroup == 1)||
				(ilStfRow < pomGroups->GetSize() && prmViewInfo->iGroup == 2))
			{
				// Urno des Mitarbeiters finden.
				long llStfUrno = 0;
				if(prmViewInfo->iGroup == 2)
					llStfUrno = (*pomGroups)[ilStfRow].lStfUrno;
				else
					llStfUrno = (*pomStfData)[ilStfRow].Urno;

				olStfUrno.Format("%ld",llStfUrno);
				
				// Datum berechnen
				COleDateTime olDay = omShowFrom;
				olDay += COleDateTimeSpan(ilColumn+ilColumnOffset,0,0,0);
				CString olSday = olDay.Format("%Y%m%d");

				// Exitiert schon ein Datensatz mit diesen Daten ?
				//polDrwData = ogDrwData.GetDrwByKey(olSday,llStfUrno);

				olUrno = ogBCD.GetFieldExt("DRW", "SDAY","STFU", olSday,olStfUrno,"URNO");
				if (olUrno.IsEmpty())
				{
					//-----------------------------------------------------------------
					// Datensatz exitiert noch nicht
					//-----------------------------------------------------------------
					//RecordSet olRecord(ogBCD.GetFieldCount("DRW"));
					int ArraySize = ogBCD.GetFieldCount("DRW")+1;
					RecordSet *olRecord = new RecordSet(ArraySize);
					//RecordSet *olRecord = new RecordSet(ogBCD.GetFieldCount("DRW"));
					
					// Struktur f�llen
					olRecord->Values[ogBCD.GetFieldIndex("DRW","PRIO")] = "0";
					olRecord->Values[ogBCD.GetFieldIndex("DRW","REME")] = "";
					olRecord->Values[ogBCD.GetFieldIndex("DRW","REMI")] = "";
					olRecord->Values[ogBCD.GetFieldIndex("DRW","REMS")] = "";
					olRecord->Values[ogBCD.GetFieldIndex("DRW","SDAY")] = olSday;
					olRecord->Values[ogBCD.GetFieldIndex("DRW","STFU")] = olStfUrno;
					olRecord->Values[ogBCD.GetFieldIndex("DRW","CLOS")] = "";
					olRecord->Values[ogBCD.GetFieldIndex("DRW","WISC")] = opNewText;
					// Ab in die Datenbank
					blResult = ogBCD.InsertRecord("DRW",*olRecord,true);

					olRecord->Values.RemoveAll();
					olRecord->Values.FreeExtra();
					delete olRecord;
				}
				else
				{
					//-----------------------------------------------------------------
					// Datensatz exitiert schon
					//-----------------------------------------------------------------
					// Datensatz exitiert, er mu� modifiziert werden
					// get record 
					//RecordSet olRecord(ogBCD.GetFieldCount("DRW"));
					int ArraySize = ogBCD.GetFieldCount("DRW")+1;					
					RecordSet *olRecord = new RecordSet(ArraySize);
					//RecordSet *olRecord = new RecordSet(ogBCD.GetFieldCount("DRW"));
					ogBCD.GetRecord("DRW","URNO",olUrno,*olRecord);

					olRecord->Values[ogBCD.GetFieldIndex("DRW","WISC")] = opNewText;
					// Ab in die Datenbank
					blResult = ogBCD.SetRecord("DRW","URNO",olUrno,olRecord->Values,true);

					olRecord->Values.RemoveAll();
					olRecord->Values.FreeExtra();
					delete olRecord;
				}

				// Datensatz sichern
				if(!blResult)
				{
					CString olErrorTxt;
					//olErrorTxt.Format("%s %d\n%s",LoadStg(ST_UPDATEERR), ogDrwData.imLastReturnCode, ogDrwData.omLastErrorMessage);
					::MessageBox(NULL, olErrorTxt,LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
				}	
			}
		}
	}
}

//*********************************************************************************
// �nderungen eines DRW Datensatzes in die Tabelle �bergeben
//*********************************************************************************

void DutyIsTabViewer::ProcessReload()
{
	if (prmViewInfo == NULL)
		return;

	if(prmViewInfo->bCorrectViewInfo == false)
		return;

	AfxGetApp()->DoWaitCursor(1);
	pomDynTab->EndIPEditandDragDropAction();
	ChangeView(false,false);
	
	if (pomParent != NULL)
	{
		if (pomParent->pomDebitTabViewer != NULL)
		{
			pomParent->pomDebitTabViewer->ChangeView();
			ogShiftCheck.FillWorkTimeData();
			if(ogShiftCheck.IsWarning())
			{
				// wenn Schichten kollidieren, werden sie nicht gespeichert und in Warnung angegeben
				pomParent->ForwardWarnings();
			}
		}
	}
	RemoveWaitCursor();
}

//*********************************************************************************
// �nderungen eines DRW Datensatzes in die Tabelle �bergeben
//*********************************************************************************

void DutyIsTabViewer::ProcessDrwChange(int ipDDXType,void *vpDataPointer)
{
CCS_TRY;
	if(!vpDataPointer) return;

	

	CString olStfuUrno,olSday,olWisc;
	long ilStfuUrno;
	int ilRow=0, ilColumn=0;
	CString olRosl;

	// Datenpointer auf Recordset casten.
	//RecordSet *polRecord = (RecordSet *) vpDataPointer;
	int ArraySize = ogBCD.GetFieldCount("DRW")+1;
	RecordSet *polRecord = new RecordSet(ArraySize);
	//RecordSet *polRecord = new RecordSet(ogBCD.GetFieldCount("DRW"));
	polRecord = (RecordSet *) vpDataPointer;

	// Ben�tigt zu Berechnung des Offsets
	TABLEINFO rlTableInfo;
	if(!pomDynTab) return;
	pomDynTab->GetTableInfo(rlTableInfo);

	// Auf G�ltigkeit pr�fen
	if (polRecord == NULL)	return;

	//--------------------------------------------------------------------------
	olSday = polRecord->Values[ogBCD.GetFieldIndex("DRW","SDAY")];
	olWisc = polRecord->Values[ogBCD.GetFieldIndex("DRW","WISC")];
	olStfuUrno = polRecord->Values[ogBCD.GetFieldIndex("DRW","STFU")];

	//--------------------------------------------------------------------------
	ilStfuUrno = atol(olStfuUrno);

	//--------------------------------------------------------------------------
	// Die Wunschzeile ist nicht aktiv, es m�ssen nur Ecken gel�scht oder gesetzt
	// werden
	//--------------------------------------------------------------------------
		
	if (!IsTypeActive("W"))
	{
		// Eventuell gr�ne Ecken l�schen
		// Aktuellen DRR finden
		DRRDATA* polDrrData = ogDrrData.GetDrrByRoss(olSday,ilStfuUrno);
		// Wenn es einen aktiven DRR gibt, dort Ver�nderungen durff�hren, sonst in Stufe 2
		if (polDrrData != NULL)	olRosl = polDrrData->Rosl;
		else	olRosl = "2";

		// Dort mu� die gr�ne Ecke gel�scht werden
		if (GetRowAndColumn(ilStfuUrno,olRosl, olSday,ilRow,ilColumn)) 
		{		
			//--------------------------------------------------------------------------
			// Ermittelung des Zeitpunkts
			COleDateTime olDay = omShowFrom;
			olDay += COleDateTimeSpan(ilColumn, 0, 0, 0);

			// Farben und Ecken nur innerhalb des editierbaren Bereichs �ndern
			if(olDay >= omFrom && olDay <= omTo) 
			{
				if (ipDDXType == BC_DRW_DELETE)
				// Gr�ne Ecke links oben l�schen
					ChangeFieldMarkerColor(ilRow,ilColumn,BPN_NOCOLOR,MARKER_LT_T);
				else
					// Gr�ne Ecke links oben setzen
					ChangeFieldMarkerColor(ilRow,ilColumn,BPN_GREEN,MARKER_LT_T);
			}
		}
	}
	//--------------------------------------------------------------------------
	// Wunschzeile wird angezeigt: Text und Ecken setzen.
	//--------------------------------------------------------------------------
	if (IsTypeActive("W"))
	{
		// Row und Column berechnen
		if (!GetRowAndColumn(ilStfuUrno,"W", olSday,ilRow,ilColumn)) 
		// DRW out of Range Changed
			return;

		//--------------------------------------------------------------------------
		// Ermittelung des Zeitpunkts
		COleDateTime olDay = omShowFrom;
		olDay += COleDateTimeSpan(ilColumn+rlTableInfo.iColumnOffset, 0, 0, 0);
			
		// If a field is deleted, the entry must be treated differently
		if (ipDDXType == BC_DRW_DELETE)
		{
			// Text change - delete
			ChangeFieldText(ilRow,ilColumn,"-");
			ChangeFieldMarkerColor(ilRow,ilColumn, BPN_NOCOLOR,MARKER_RT_T);
			//---------------------------------------------------------------------------------------------------------------
		
		}

		else
		{
			// Text ver�ndern
			ChangeFieldText(ilRow,ilColumn,olWisc);
			//---------------------------------------------------------------------------------------------------------------
			// Farben und Ecken nur innerhalb des editierbaren Bereichs �ndern
			if(olDay >= omFrom && olDay <= omTo) 
			{
				// Extramarkierung setzten oder l�schen falls extra Informationen vorhanden sind
				if (HasDrwExtraInformation(olSday,olStfuUrno))
					ChangeFieldMarkerColor(ilRow,ilColumn, BPN_RED,MARKER_RT_T);
				else
					ChangeFieldMarkerColor(ilRow,ilColumn, BPN_NOCOLOR,MARKER_RT_T);
			}
		}
	}
	//--------------------------------------------------------------------------
	// Neu zeichnen
	pomDynTab->ChangedFieldData(ilRow+rlTableInfo.iRowOffset,ilColumn+rlTableInfo.iColumnOffset);

	polRecord->Values.RemoveAll();
	polRecord->Values.FreeExtra();
CCS_CATCH_ALL;
}

//-----------------------------------------------------------------------------------------------


// ***********************************************************************************
// Header der Tabelle werden gesetzt (Farbe und Text)
// ***********************************************************************************

void DutyIsTabViewer::InitTableHeaderArrays(int ipColumnOffset /*=0*/)
{
	TABLEINFO rlTableInfo;
	// Zeiger auf den Tableinfo der Dyntable rhalten
	pomDynTab->GetTableInfo(rlTableInfo);
	// Inhalt der Dyntable l�schen (?)
	pomDynTab->ResetContent();
	
	prmTableData->oSolidHeader.oText		= "";
	prmTableData->oSolidHeader.oTextColor	= BPN_NOCOLOR;
	prmTableData->oSolidHeader.oBkColorNr	= BPN_NOCOLOR;

	// jede Column durchgehen
	for(int ilColumn=0;ilColumn<rlTableInfo.iColumns;ilColumn++)
	{
		AddColumnHeader(ilColumn,ipColumnOffset);
	}
}

//**************************************************************************************
// Setzt Text und Farbe f�r eine Column und f�gt sie ein
// !!noch nicht benutzt!!
//**************************************************************************************

void DutyIsTabViewer::InsertColumnHeader(int ipColumn, int ipColumnOffset)
{
	CString olText; 
	// Header neu zeichnen.
	olText.Empty();
	FIELDDATA *polFieldData = new FIELDDATA;
	polFieldData->oTextColor = BPN_NOCOLOR;
	polFieldData->oBkColorNr = BPN_NOCOLOR;

	if(ipColumn<imDays)
	{
		bool blIsHol = false;
		int ilDayOfWeek = GetDayStrg(ipColumn, olText, blIsHol);
		if(ilDayOfWeek == 1 || ilDayOfWeek == 7 || blIsHol)
			polFieldData->oBkColorNr = BPN_SILVERRED;

		COleDateTime olDay = omShowFrom;
		olDay += COleDateTimeSpan(ipColumn,0,0,0);
		if(prmViewInfo->oSelectDate == olDay && prmViewInfo->bSortDayly == true)
		{
			polFieldData->oBkColorNr = BPN_SILVERGREEN;
		}
	}
	polFieldData->oText = olText;
	prmTableData->oHeader.InsertAt(ipColumn,(void*)polFieldData);
}

//**************************************************************************************
// Setzt Text und Farbe f�r eine Column und f�gt sie am Ende ein
//**************************************************************************************

void DutyIsTabViewer::AddColumnHeader(int ipColumn, int ipColumnOffset)
{
	CString olText; 
	olText.Empty();
	FIELDDATA *polFieldData = new FIELDDATA;
	//olText.Format("%d",ipColumnOffset + ilColumn);//test
	polFieldData->oTextColor	= BPN_NOCOLOR;
	polFieldData->oBkColorNr	= BPN_NOCOLOR;

	// Sollte der Tag im Bereich der angezeigten Tage liegen
	if(ipColumn<imDays)
	{
		// Offensichtlich werden hier nur die Hintergrundfarben gesetzt.
		bool blIsHol = false;
		// Achtung: olText wird hier ver�ndert.
		int ilDayOfWeek = GetDayStrg(ipColumnOffset + ipColumn, olText, blIsHol);
		// Wochenende oder Feiertag, dann die Farbe �ndern.
		if(ilDayOfWeek == 1 || ilDayOfWeek == 7 || blIsHol)
			polFieldData->oBkColorNr = BPN_SILVERRED;

		// Tag nach dem sortiert wird  farblich anders darstellen
		COleDateTime olDay = omShowFrom;
		olDay += COleDateTimeSpan(ipColumnOffset + ipColumn,0,0,0);
		if(prmViewInfo->oSelectDate == olDay && prmViewInfo->bSortDayly == true)
		{
			polFieldData->oBkColorNr = BPN_SILVERGREEN;
		}
	}

	// Text an die Dyntable �bergeben
	polFieldData->oText	= olText;
	prmTableData->oHeader.Add((void*)polFieldData);
}

//-----------------------------------------------------------------------------------------------

void DutyIsTabViewer::MakeTableData(int ipColumnOffset,int ipRowOffset)
{
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	//****************************************************************
	int ilActualRows = prmTableData->oSolid.GetSize();
	//** Add rows by SIZING
	while(rlTableInfo.iRows > ilActualRows)
	{
		AddRow(ilActualRows,rlTableInfo.iColumns,ipColumnOffset,ipRowOffset+ilActualRows);
		ilActualRows = prmTableData->oSolid.GetSize();
	}
	//** Delete rows by SIZING
	while(rlTableInfo.iRows < ilActualRows)
	{
		DelRow(ilActualRows-1);
		ilActualRows = prmTableData->oSolid.GetSize();
	}

	int ilActualColumns = prmTableData->oHeader.GetSize();
	//*** Add columns by SIZING
	while(ilActualRows>0 && rlTableInfo.iColumns > ilActualColumns)
	{
		AddColumn(ilActualColumns,ipColumnOffset+ilActualColumns,ipRowOffset);
		ilActualColumns = prmTableData->oHeader.GetSize();
	}
	//** Delete columns by SIZING
	while(ilActualRows>0 && rlTableInfo.iColumns < ilActualColumns)
	{
		DelColumn(ilActualColumns-1);
		ilActualColumns = prmTableData->oHeader.GetSize();
	}
	//****************************************************************
	int ilColumnDiv = ipColumnOffset-rlTableInfo.iColumnOffset;
	if(ilColumnDiv != 0)
	{
		//** SCROLL right
		if(ilColumnDiv > 0)
		{
			if(ilColumnDiv > prmTableData->oHeader.GetSize())
				ilColumnDiv = prmTableData->oHeader.GetSize();
			for(int i=0; i < ilColumnDiv;i++)
			{
				DelColumn(0);
				AddColumn(prmTableData->oHeader.GetSize(),ipColumnOffset+rlTableInfo.iColumns-ilColumnDiv+i,ipRowOffset);
			}
		}
		//** SCROLL left
		if(ilColumnDiv < 0)
		{
			ilColumnDiv = -(ilColumnDiv);
			if(ilColumnDiv > prmTableData->oHeader.GetSize())
				ilColumnDiv = prmTableData->oHeader.GetSize();
			for(int i=0; i < ilColumnDiv; i++)

			{
				DelColumn(prmTableData->oHeader.GetSize()-1);
				AddColumn(0,ipColumnOffset+ilColumnDiv-i-1,ipRowOffset);
			}
		}
	}
	int ilRowDiv = ipRowOffset-rlTableInfo.iRowOffset;
	if(ilRowDiv != 0)
	{
		//** SCROLL up
		if(ilRowDiv > 0)
		{
			if(ilRowDiv > prmTableData->oSolid.GetSize())
				ilRowDiv = prmTableData->oSolid.GetSize();
			for(int i=0; i < ilRowDiv; i++)
			{
				DelRow(0);
				AddRow(prmTableData->oSolid.GetSize(),rlTableInfo.iColumns,ipColumnOffset,ipRowOffset+rlTableInfo.iRows-ilRowDiv+i);
			}
		}
		//** SCROLL down
		if(ilRowDiv < 0)
		{
			ilRowDiv = -(ilRowDiv);
			if(ilRowDiv > prmTableData->oSolid.GetSize())
				ilRowDiv = prmTableData->oSolid.GetSize();
			for(int i=0; i < ilRowDiv; i++)
			{
				DelRow(prmTableData->oSolid.GetSize()-1);
				AddRow(0,rlTableInfo.iColumns,ipColumnOffset,ipRowOffset+ilRowDiv-i-1);
			}
		}
	}
}

//**************************************************************************************
// Einf�gen eine DatenRow in die Dyntable. (Es werden nur die Daten eingef�gt!)
//**************************************************************************************

void DutyIsTabViewer::AddRow(int ipRow, int ipColumns, int ipColumnNr, int ipRowNr)
{
	CString olText;
	//** Table
	CPtrArray *polTableValue = new CPtrArray;

	CString olPType = "";
	int ilStfNr = 0;
	int ilPTypeNr = 0;
	bool blGetRaT = GetElementAndPType(ipRowNr, ilStfNr, olPType, ilPTypeNr);

	// F�r alle vorhandenen Columns durchgehen
	for(int ilColumn=0;ilColumn<ipColumns;ilColumn++)
	{
		// Text l�schen
		olText.Empty();
		FIELDDATA *polFieldData = new FIELDDATA;
		// Attribute setzen
		polFieldData->oTextColor				= BPN_NOCOLOR;
		polFieldData->oBkColorNr				= BPN_LIGHTSILVER1;
		polFieldData->rIPEAttrib.Style			|= ES_UPPERCASE;
		//polFieldData->rIPEAttrib.Format			= CString("x|#x|#x|#x|#x|#x|#x|#x|#");
		polFieldData->rIPEAttrib.Format			= CString("X(17)");
		polFieldData->rIPEAttrib.TextMaxLenght	= 17;
		polFieldData->rIPEAttrib.TextMinLenght	= 0;
		polFieldData->bInplaceEdit				= false;
		polFieldData->bDrop						= false;
		polFieldData->bDrag						= false;
		polFieldData->iRButtonDownStatus		= RBDS_ALL;

		//polFieldData->iLBeamColorNr		= BPN_SELECTBLUE; // Test
		//polFieldData->iRBeamColorNr		= BPN_SELECTBLUE; // Test
		//polFieldData->iLTTriangleColorNr	= BPN_RED; // Test
		//polFieldData->iRTTriangleColorNr	= BPN_RED; // Test
		//polFieldData->iRBTriangleColorNr	= BPN_RED; // Test
		//polFieldData->iLBTriangleColorNr	= BPN_RED; // Test

		// Zeichnet die dicken Linien 
		if(omPTypes.GetSize() > 1 && ilPTypeNr+1 == omPTypes.GetSize())
		{
			polFieldData->iBLineWidth = NORMAL_LINE;
		}
		
		if(ipColumnNr+ilColumn<imDays)
		{
			bool blIsHol = false;
			int ilDayOfWeek = GetDayStrg(ipColumnNr+ilColumn, olText, blIsHol);
			if(ilDayOfWeek == 1 || ilDayOfWeek == 7 || blIsHol)
			{
				polFieldData->iLLineWidth = NORMAL_LINE;
				polFieldData->iTLineWidth = NORMAL_LINE;
				polFieldData->iRLineWidth = NORMAL_LINE;
				polFieldData->iBLineWidth = NORMAL_LINE;

				polFieldData->iLLineColorNr = BPN_SILVERRED;
				polFieldData->iTLineColorNr = BPN_SILVERRED;
				polFieldData->iRLineColorNr = BPN_SILVERRED;
				if(!(omPTypes.GetSize() > 1 && ilPTypeNr+1 == omPTypes.GetSize()))
					polFieldData->iBLineColorNr = BPN_SILVERRED;
			}
		}

		if(((ilStfNr < pomStfData->GetSize() && prmViewInfo->iGroup == 1)||
			(ilStfNr < pomGroups->GetSize() && prmViewInfo->iGroup == 2))&& 
			ipColumnNr+ilColumn<imDays && blGetRaT)
		{
			// Wert f�r dieses Tabellenelement wird geladen
			GenerateFieldValues(ilStfNr, olPType, ilPTypeNr, ipColumnNr+ilColumn, ipRowNr, polFieldData);
		}
		polTableValue->Add((void*)polFieldData);
	}

	// Schreiben der Daten einer gesamten Row
	prmTableData->oTable.InsertAt(ipRow,(void*)polTableValue);

	
	// ***************************************???
	//** Solid
	olText.Empty();
	FIELDDATA *polFieldData = new FIELDDATA;
	//olText.Format("%d",ipRowNr);//test
	polFieldData->oTextColor = BPN_NOCOLOR;
	polFieldData->oBkColorNr = BPN_NOCOLOR;
	prmTableData->oSolid.InsertAt(ipRow,(void*)polFieldData);
	// ***************************************???
}

//-----------------------------------------------------------------------------------------------

void DutyIsTabViewer::DelRow(int ipRow)
{
	//** Table Text Color

	CPtrArray *polTableValue = (CPtrArray*)prmTableData->oTable[ipRow];
	for(int i = polTableValue->GetSize()-1;i>=0;i--)
	{
		FIELDDATA *polFieldData = (FIELDDATA*)(*polTableValue)[i];
		delete polFieldData;
	}
	polTableValue->RemoveAll();
	delete polTableValue;
	prmTableData->oTable.RemoveAt(ipRow);

	//** Solid Text Color
	FIELDDATA *polFieldData = (FIELDDATA*)prmTableData->oSolid[ipRow];
	delete polFieldData;
	prmTableData->oSolid.RemoveAt(ipRow);

}

//-----------------------------------------------------------------------------------------------

void DutyIsTabViewer::AddColumn(int ipColumns, int ipColumnNr, int ipRowNr)
{
	CString olText; 
	//** Table
	int ilActualRows = prmTableData->oTable.GetSize();
	for(int ilRow = 0; ilRow<ilActualRows; ilRow++)
	{
		olText.Empty();
		CPtrArray *polTableValue = (CPtrArray*)prmTableData->oTable[ilRow];

		FIELDDATA *polFieldData = new FIELDDATA;
		//olText.Format("%d-%d",ipRowNr+ilRow,ipColumnNr);//test
		polFieldData->oTextColor				= BPN_NOCOLOR;
		polFieldData->oBkColorNr				= BPN_LIGHTSILVER1;
		polFieldData->rIPEAttrib.Style			|= ES_UPPERCASE;
		//polFieldData->rIPEAttrib.Format			= CString("x|#x|#x|#x|#x|#x|#x|#x|#");
		polFieldData->rIPEAttrib.Format			= CString("X(17)");
		polFieldData->rIPEAttrib.TextMaxLenght	= 8;
		polFieldData->rIPEAttrib.TextMinLenght	= 0;
		polFieldData->bInplaceEdit				= false;
		polFieldData->bDrop						= false;
		polFieldData->bDrag						= false;
		polFieldData->iRButtonDownStatus		= RBDS_ALL;

		CString olPType = "";
		int ilStfNr = 0;
		int ilPTypeNr = 0;
		bool blGetRaT = GetElementAndPType((ipRowNr+ilRow), ilStfNr, olPType, ilPTypeNr);

		if(omPTypes.GetSize() > 1 && ilPTypeNr+1 == omPTypes.GetSize())
		{
			polFieldData->iBLineWidth = NORMAL_LINE;
		}

		if(ipColumnNr<imDays)
		{
			bool blIsHol = false;
			int ilDayOfWeek = GetDayStrg(ipColumnNr, olText, blIsHol);
			if(ilDayOfWeek == 1 || ilDayOfWeek == 7 || blIsHol)
			{
				polFieldData->iLLineWidth = NORMAL_LINE;
				polFieldData->iTLineWidth = NORMAL_LINE;
				polFieldData->iRLineWidth = NORMAL_LINE;
				polFieldData->iBLineWidth = NORMAL_LINE;

				polFieldData->iLLineColorNr = BPN_SILVERRED;
				polFieldData->iTLineColorNr = BPN_SILVERRED;
				polFieldData->iRLineColorNr = BPN_SILVERRED;
				if(!(omPTypes.GetSize() > 1 && ilPTypeNr+1 == omPTypes.GetSize()))
					polFieldData->iBLineColorNr = BPN_SILVERRED;
			}
		}

		if(((ilStfNr < pomStfData->GetSize() && prmViewInfo->iGroup == 1)||
			(ilStfNr < pomGroups->GetSize() && prmViewInfo->iGroup == 2)) && 
			ipColumnNr<imDays && blGetRaT)
		{
			GenerateFieldValues(ilStfNr, olPType, ilPTypeNr, ipColumnNr, ipRowNr+ilRow, polFieldData);
		}
		polTableValue->InsertAt(ipColumns,(void*)polFieldData);
	}

	// Header neu zeichnen.und einf�gen
	//InsertColumnHeader(ipColumnNr,0);

	olText.Empty();
	FIELDDATA *polFieldData = new FIELDDATA;
	polFieldData->oTextColor = BPN_NOCOLOR;
	polFieldData->oBkColorNr = BPN_NOCOLOR;

	if(ipColumnNr<imDays)
	{
		bool blIsHol = false;
		int ilDayOfWeek = GetDayStrg(ipColumnNr, olText, blIsHol);
		if(ilDayOfWeek == 1 || ilDayOfWeek == 7 || blIsHol)
			polFieldData->oBkColorNr = BPN_SILVERRED;

		COleDateTime olDay = omShowFrom;
		olDay += COleDateTimeSpan(ipColumnNr,0,0,0);   // BDA 09.02.2000 ipColumn -> ipColumnNr olDay zeigt auf die absolute Position und nicht auf die letzte neu einzuf�gende Column
		if(prmViewInfo->oSelectDate == olDay && prmViewInfo->bSortDayly == true)
		{
			polFieldData->oBkColorNr = BPN_SILVERGREEN;
		}
	}
	polFieldData->oText = olText;
	prmTableData->oHeader.InsertAt(ipColumns,(void*)polFieldData);
}

//-----------------------------------------------------------------------------------------------

void DutyIsTabViewer::DelColumn(int ipColumn)
{
	//** Table Text Color
	int ilActualRows = prmTableData->oTable.GetSize();
	for(int ilRow = 0; ilRow<ilActualRows; ilRow++)
	{
		CPtrArray *polTableValue = (CPtrArray*)prmTableData->oTable[ilRow];
		if(!polTableValue || polTableValue->GetSize() <= ipColumn) break;
		FIELDDATA *polFieldData = (FIELDDATA*)(*polTableValue)[ipColumn];
		delete polFieldData;
		polTableValue->RemoveAt(ipColumn);
	}

	//** Header Text Color
	FIELDDATA *polFieldData = (FIELDDATA*)prmTableData->oHeader[ipColumn];
	delete polFieldData;
	prmTableData->oHeader.RemoveAt(ipColumn);
}

//*************************************************************************************************
// Daten f�r einzelnes Tabellen Objekt ermitteln
//*************************************************************************************************

void DutyIsTabViewer::GenerateFieldValues(int ipStfNr, CString opPType, int ipPTypeNr, 
										  int ipColumn, int ipRow, 
										  FIELDDATA *popFieldData)
{
	CString olText,olTmpUrno;
	long llTmpUrno = 0;

	// Gruppierung werden hier beachtet
	if(prmViewInfo->iGroup == 2)
	{
		llTmpUrno = (*pomGroups)[ipStfNr].lStfUrno;
		if(ipStfNr+1 < pomGroups->GetSize())
		{
			if((*pomGroups)[ipStfNr].iColor1Nr != (*pomGroups)[ipStfNr+1].iColor1Nr)
			{
				popFieldData->iBLineWidth = NORMAL_LINE;
			}
		}
	}
	else
	{
		llTmpUrno = (*pomStfData)[ipStfNr].Urno;
	}

	//--------------------------------------------------------------------------------------
	// Ermittelung des Zeitpunkts
	COleDateTime olDay = omShowFrom;
	olDay += COleDateTimeSpan(ipColumn, 0, 0, 0);

	CString olSday = olDay.Format("%Y%m%d");
	olTmpUrno.Format("%ld",llTmpUrno);
	
	DRRDATA *prlDrr = NULL;
	DRRDATA *prlDrr2 = NULL;

	//
	// if configured in the setup, display the grey area at the left/right of the view timeframe
	//
	if (olDay < omFrom || olDay > omTo) 
	{
		if (ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_VORLAUF) == 1)
		{
			prlDrr = ogDrrData.GetDrrByKey(olSday,llTmpUrno,"1",opPType);
			if (prlDrr != NULL)
			{
				// set the field text
				popFieldData->oText = prlDrr->Scod;
				prlDrr2 = ogDrrData.GetDrrByKey(olSday,llTmpUrno,"2",opPType);
				if (prlDrr2 != NULL)
					popFieldData->oText += "/" + CString(prlDrr2->Scod);

				// background grey, no bars, no input allowed
				popFieldData->bInplaceEdit = false;
				popFieldData->bDrop		   = false;
				popFieldData->bDrag		   = false;
			
				
			popFieldData->oBkColorNr = BPN_LIGHTSILVER1;
				
			
				
				
				return;
			}
		}
	}

	if(opPType == "W")
	{
		// Ermittelt alle relevanten Werte f�r die Wunschplanungsstufe
		// Und tr�gt sie in die Dyntable ein.
		GenerateWishValues(llTmpUrno,olTmpUrno,olDay,olSday,popFieldData,ipRow,ipColumn);
		return;
	}

	prlDrr = ogDrrData.GetDrrByKey(olSday,llTmpUrno,"1",opPType);
	if (prlDrr != NULL)
	{
		// Schichtcode (Bezeichnung der Schicht) lesen
		olText = prlDrr->Scod;

		// In der Schichtplanstufe zweite Schicht anzeigen
		if((olDay >= omFrom) && (olDay <= omTo))// && (CString(prlDrr->Rosl) == "1"))
		{
			// blauer Balken rechts, wenn es mehrere Schichten (d.h. mind. DRRN = 2) gibt
			//if (ogDrrData.GetDrrListByKeyWithoutDrrn(olSday,llTmpUrno,opPType) > 1)
			prlDrr2 = ogDrrData.GetDrrByKey(CString(prlDrr->Sday), llTmpUrno, "2", opPType);
			if (prlDrr2 && strlen(prlDrr2->Scod) > 0)
			{
				popFieldData->iRBeamColorNr = BPN_LIGHTBLUE;
				olText += "/" + CString(prlDrr2->Scod);
			}
			else
			{
				popFieldData->iRBeamColorNr = -1;
			}
		}

		// Extra-Informationen f�r aktive DRR anzeigen, falls Sie im editierbaren Bereich liegen
		if((olDay >= omFrom) && (olDay <= omTo) && (CString(prlDrr->Ross) == "A")) 
		{
			if (ogDrrData.HasDoubleTour(prlDrr))
			{
				// schwarze Ecke f�r Doppeltouren (nur bei aktueller Stufe anzeigen)
				popFieldData->iRBTriangleColorNr = BLACK;
			}	

			if(strlen(prlDrr->Scod) > 0 && ogOdaData.IsODAAndIsRegularFree(prlDrr->Scod) == CODE_IS_BSD)
			{
				// das ist ein BSD und kein ODA
				// Aktuelle Funktion in DRR und die Stammfunktion des Mitarbeiters vergleichen
				if(strcmp(prlDrr->Fctc, (LPCTSTR)ogSpfData.GetMainPfcBySurnWithTime(prlDrr->Stfu, olDay))) 
				{
					// die Funktionen sind nicht gleich, markieren!
					
					// graue Ecke links unten, wenn die Funktion der Schicht nicht mit der 
					// Stammfunktion des Mitarbeiters �bereinstimmt
					popFieldData->iLBTriangleColorNr = BPN_SILVER;
				}
			}

			if (!IsTypeActive("W"))
			{
				// gr�ne Ecke, wenn ein Wunsch vorhanden ist, die Wunschzeile jedoch ausgeblendet ist

				// Wunsch vorhanden?
				CString olResult = ogBCD.GetFieldExt("DRW", "SDAY","STFU", olSday,olTmpUrno,"URNO");
				if (!olResult.IsEmpty())
					// ja -> gr�ne Ecke links oben
					popFieldData->iLTTriangleColorNr = BPN_GREEN;
			}
		}
	}
	else
	// Es gibt keinen DRR
	// Die gr�nen Wunschecken sollen jedoch dennoch angezeigt werden, auch wenn es keinen
	//  DRR gibt.
	{
		// DefaultZeichen f�r nicht geplante Tage
		olText = ogCCSParam.GetParamValue(ogAppl,"ID_BLANK_CHAR");
	
		// Ist dies die Dienstplanstufe ?
		if (opPType == "2")
		{
			// Es darf auch keinen anderen, aktiven DRR geben
			if (ogDrrData.GetDrrByRoss(olSday, llTmpUrno) == NULL)
			{
				if (!IsTypeActive("W"))
				{
					// Sollte ein Wunsch Datensatz vorhanden sein f�r diesen Tag und Mitarbeiter, dann 
					// in die linke Ecke ein Dreieck setzten.
					CString olResult = ogBCD.GetFieldExt("DRW", "SDAY","STFU", olSday,olTmpUrno,"URNO");
					if (!olResult.IsEmpty())
						// Gr�ne Ecke links oben
						popFieldData->iLTTriangleColorNr = BPN_GREEN;
				}
			}
		}
	}

	//------------------------------------------
	// Vollst�ndig umgebaut von ADO am 20.1.00
	//------------------------------------------

	// Planungsstatus des Datensatzes pr�fen (Details siehe 
	// CedaDrrData::IsEditableByDutyRoster())
	bool blIsEditable = ogDrrData.IsEditableByDutyRoster(prlDrr,opPType);
	
	//--------------------------------------------------------------------------------------
	//Ist es auserhalb des Benutzer vom definierten Zeitraumes
	//--------------------------------------------------------------------------------------
	if(olDay < omFrom || olDay > omTo || !llTmpUrno) 
	{
		// -> Ja Hintergrund grau, keine Eingaben m�glich
		popFieldData->bInplaceEdit = false;
		popFieldData->bDrop		   = false;
		popFieldData->bDrag		   = false;
		popFieldData->oBkColorNr = BPN_LIGHTSILVER1;
	}
	else
	{
		// -> Nein, Eingaben sind grunds�tzlich m�glich
		//	Ist dieses Feld in der aktuellen (gerade zu beabeitenen) Planungsstufe  
		if(!blIsEditable) 
		{
			// -> Nein, Hintergrund gelb, Eingaben nicht m�glich
			popFieldData->bInplaceEdit = false;
			popFieldData->bDrop		   = false;
			popFieldData->bDrag		   = false;
		
			//uhi 14.07.00 Grauer Hintergrund wenn Planungsstufe nicht aktiv
			if(prlDrr != NULL && prlDrr->Ross == CString("L"))
				popFieldData->oBkColorNr = BPN_LIGHTSILVER1;
			else
				popFieldData->oBkColorNr = BPN_SILVERGREENLIGHT;

			//Ist es die Schichtplanstufe, dann Hintergrung grau
			if ((opPType == "1") || ((opPType == "L")))
			{
				popFieldData->oBkColorNr = BPN_LIGHTSILVER1;
			}
		}
		else
		{
			//----------------------------------------------------------------------------
			// -> Ja, Stufe kann bearbeitet werden

			popFieldData->bInplaceEdit = true;
			popFieldData->bDrop		   = true;
			popFieldData->bDrag		   = false;
			popFieldData->oBkColorNr = BPN_WHITE;
			popFieldData->iRButtonDownStatus = RBDS_ALL;

			// Textfarbe setzen, wenn  kein "Abwesenheits" usw. Feld 
			if (opPType != "U" && opPType != "1")
			{
				bool bRedText = false;
				if(CString(prlDrr->Ross) == "A")
				{
					if(strcmp(prlDrr->Expf, "A"))	// sonst ist schon bearbeitet
					{
						if(ogSprData.AreTimesDifferent(prlDrr))
							bRedText = true;	// Zeiten unterscheiden sich vom AZE
						else if(ogDraData.HasDraOfDrrWithoutDel(prlDrr))
							bRedText = true;
						else if(ogDrdData.HasDrdOfDrrWithoutDel(prlDrr))
							bRedText = true;
						else if(ogDrrData.IsExtraShift(prlDrr->Drs4))
							bRedText = true;
						else if(ogDrrData.IsDrrDifferentFromShift(prlDrr))
							bRedText = true;
						else if (strlen(prlDrr->Repaired) > 0)
							bRedText = true;
					}
				}

				if (bRedText)
					popFieldData->oTextColor = RED;
				else
					// keine Abweichung -> Textfarbe schwarz
					popFieldData->oTextColor = BPN_NOCOLOR;
			}

			if(!llTmpUrno)
			{
				// Kein Mitarbeiter vorhanden
				popFieldData->oBkColorNr = BPN_LIGHTSILVER1;
			}
			else if (!ogStfData.IsStfNowValid(llTmpUrno,olDay))
			{
				// Kein g�ltiger Arbeitsvertrag vorhanden
				popFieldData->oBkColorNr = BPN_SILVERREDLIGHT3;
			}

				//Handelt es sich um ein Feld mit blauem Hintergrund ? (Multiselektion ?)
			if (IsFieldMultiselected(ipRow,ipColumn))
				popFieldData->oBkColorNr = BPN_SELECTBLUE;

		}
	}

	//--------------------------------------------------------------------------------------
	// Text setzen
	popFieldData->oText	= olText;

}

//******************************************************************************
// pr�ft ob ein bestimmter Planungtyp angezeigt werden soll
//******************************************************************************

bool DutyIsTabViewer::IsTypeActive(CString opType)
{
	bool olReturn = false;

	for(int i=0; i<omPTypes.GetSize(); i++)
	{
		if(omPTypes[i] == opType)
		{
			olReturn = true;
			i = omPTypes.GetSize();
		}
	}
	return (olReturn);
}

//******************************************************************************
// Setzt ein Feld auf READONLY
//******************************************************************************

void DutyIsTabViewer::SetFieldReadOnly(int ipRow, int ipColumn, bool bpReadOnly)
{
	if(ipRow < prmTableData->oTable.GetSize())
	{
		CPtrArray *polValue = (CPtrArray*)prmTableData->oTable[ipRow];
		if(ipColumn < polValue->GetSize())
		{
			FIELDDATA *polFieldData = (FIELDDATA*)(*polValue)[ipColumn];
			// TODO: In diesem Feld ist READ ONLY
			if (bpReadOnly)
			{
				polFieldData->bInplaceEdit = false;
				polFieldData->bDrop		   = false;
				polFieldData->bDrag		   = false;
			}
			else
			{
				polFieldData->bInplaceEdit = true;
				polFieldData->bDrop		   = true;
				polFieldData->bDrag		   = true;
			}
		}
	}
}

//******************************************************************************
// 
//******************************************************************************

void DutyIsTabViewer::ChangeFieldText(int ipRow, int ipColumn, CString opText)
{
	if(ipRow < prmTableData->oTable.GetSize())
	{
		CPtrArray *polValue = (CPtrArray*)prmTableData->oTable[ipRow];
		if(ipColumn < polValue->GetSize())
		{
			FIELDDATA *polFieldData = (FIELDDATA*)(*polValue)[ipColumn];
			polFieldData->oText	= opText;
		}
	}
}

//-----------------------------------------------------------------------------------------------

void DutyIsTabViewer::ChangeFieldTextColor(int ipRow, int ipColumn, COLORREF opColor)
{
	if(ipRow < prmTableData->oTable.GetSize())
	{
		CPtrArray *polValue = (CPtrArray*)prmTableData->oTable[ipRow];
		if(ipColumn < polValue->GetSize())
		{
			FIELDDATA *polFieldData = (FIELDDATA*)(*polValue)[ipColumn];
			polFieldData->oTextColor = opColor;
		}
	}
}

//-----------------------------------------------------------------------------------------------

void DutyIsTabViewer::ChangeFieldMarkerColor(int ipRow, int ipColumn, int ipColorNr, int ipMarker)
{
	if(ipRow < prmTableData->oTable.GetSize())
	{
		CPtrArray *polValue = (CPtrArray*)prmTableData->oTable[ipRow];
		if(ipColumn < polValue->GetSize())
		{
			FIELDDATA *polFieldData = (FIELDDATA*)(*polValue)[ipColumn];

			switch(ipMarker)
			{
				case MARKER_L_B:
					polFieldData->iLBeamColorNr = ipColorNr;
					break;
				case MARKER_R_B:
					polFieldData->iRBeamColorNr = ipColorNr;
					break;
				case MARKER_LT_T:
					polFieldData->iLTTriangleColorNr = ipColorNr;
					break;
				case MARKER_RT_T:
					polFieldData->iRTTriangleColorNr = ipColorNr;
					break;
				case MARKER_RB_T:
					polFieldData->iRBTriangleColorNr = ipColorNr;
					break;
				case MARKER_LB_T:
					polFieldData->iLBTriangleColorNr = ipColorNr;
					break;
			}
		}
	}
}

//-----------------------------------------------------------------------------------------------

int DutyIsTabViewer::ChangeFieldBkColor(int ipRow, int ipColumn, int ipBkColorNr)
{
	int ilOldBkColorNr = 0;
	if(ipRow < prmTableData->oTable.GetSize())
	{
		CPtrArray *polValue = (CPtrArray*)prmTableData->oTable[ipRow];
		if(ipColumn < polValue->GetSize())
		{
			FIELDDATA *polFieldData = (FIELDDATA*)(*polValue)[ipColumn];
			ilOldBkColorNr = polFieldData->oBkColorNr;
			polFieldData->oBkColorNr = ipBkColorNr;
		}
	}
	return ilOldBkColorNr;
}

//-----------------------------------------------------------------------------------------------

int DutyIsTabViewer::GetFieldBkColor(int ipRow, int ipColumn)
{
	int ilOldBkColorNr = 0;
	if(ipRow < prmTableData->oTable.GetSize())
	{
		CPtrArray *polValue = (CPtrArray*)prmTableData->oTable[ipRow];
		if(ipColumn < polValue->GetSize())
		{
			FIELDDATA *polFieldData = (FIELDDATA*)(*polValue)[ipColumn];
			ilOldBkColorNr = polFieldData->oBkColorNr;
		}
	}
	return ilOldBkColorNr;
}

//-----------------------------------------------------------------------------------------------

int DutyIsTabViewer::GetDayStrg(int ipDayOffset, CString &opText, bool &bpIsHoliday)
{
	bpIsHoliday = false;
	COleDateTime olDay = omShowFrom;
	olDay += COleDateTimeSpan(ipDayOffset,0,0,0);

	int ilDayOfWeek = olDay.GetDayOfWeek();
	switch(ilDayOfWeek)
	{
		case 1:
			opText = LoadStg(DUTY_SUN);
			break;
		case 2:
			opText = LoadStg(DUTY_MON);
			break;
		case 3:
			opText = LoadStg(DUTY_TUE);
			break;
		case 4:
			opText = LoadStg(DUTY_WED);
			break;
		case 5:
			opText = LoadStg(DUTY_THU);
			break;
		case 6:
			opText = LoadStg(DUTY_FRI);
			break;
		case 7:
			opText = LoadStg(DUTY_SAT);
			break;
	}
	opText += olDay.Format(" %d.%m.");

	//Is the day a holiday? (defined in HOLTAB)
	CString olTmp;
	void  *prlVoid = NULL;
	olTmp = olDay.Format("%Y%m%d000000");
	if (omHolidayKeyMap.Lookup(olTmp,(void *&)prlVoid) == TRUE)
	{
		bpIsHoliday = true;
	}
	return ilDayOfWeek;
	//end
}

//************************************************************************************************
// Ermittelt den Code Typ f�r die angegebende Row
//************************************************************************************************

int DutyIsTabViewer::GetFieldCode(int ipRow)
{
	int ilStfRow = 0;
	CString olPType;
	int ilPTypeNr = 0;
	
	// Code ermitteln
	if (GetElementAndPType(ipRow, ilStfRow, olPType, ilPTypeNr))
	{
		if(olPType == "W") 
			return CODE_IS_WIS;
		if(olPType == "U") 
			return CODE_IS_ODA;

		return CODE_IS_BSD;
	}

	return CODE_UNDEFINED;	
}

//************************************************************************************************
//
//************************************************************************************************

bool DutyIsTabViewer::GetElementAndPType(int ipRowNr, int &ipRow, CString &opPType, int &ipPTypNr)
{
	// Ermittelt die Anzahl der Planungstypen
	int ilPTypeSize = omPTypes.GetSize();
	// Sollten keine Planungtypen vorhanden sein, Funktion beenden.
	if(ilPTypeSize < 1) return false;

	double dlBruch = (double)ipRowNr/(double)ilPTypeSize;
	double dlDivisor = 1.0/(double)ilPTypeSize - 0.0000000000001;
	double dlRow = floor(dlBruch);
	ipRow = (int)dlRow;
	ipPTypNr = (int)(floor(((dlBruch-dlRow)/dlDivisor)));

	if(ipPTypNr>ilPTypeSize) return false;
	opPType = omPTypes[ipPTypNr];

	return true;	
}

//****************************************************************************
// Scroll vertikal, damit angegeben URNO sichtbar wird
//****************************************************************************

bool DutyIsTabViewer::ScrollToVisible(long lpUrno)
{
	int ilRow;
	bool blReturn;
	//MODIFYTAB *prlModifyTab= new MODIFYTAB;

	// Row der �bergebenen URNO finden
	ilRow = GetRowForUrno(lpUrno);

	// Scrollen, falls die Urno gefunden wurde
	if (ilRow != -1)
	{
		// DynTable zum Scrollen veranlassen.
		pomDynTab->ScrollToPosition(ilRow);
		blReturn = true;
	}
	else
	{
		blReturn = false;
	}
	return (blReturn);
}

//****************************************************************************
// Ermitteln der Row f�r eine URNO. -1 falls nicht gefunden.
//****************************************************************************

int	DutyIsTabViewer::GetRowForUrno(long lpUrno)
{
CCS_TRY;

	int ilRowPosition = -1;
	int ilCount;

	// Abh�ngig von der Darstellung m�ssen Datenstrukturen durchsucht werden.
	switch (prmViewInfo->iGroup)
	{
	// "normale" Darstellung
	case 1:
			for (ilCount=0;ilCount<pomStfData->GetSize() && ilRowPosition == -1;ilCount++)
			{
				if ( (*pomStfData)[ilCount].Urno == lpUrno)
					// Position in der Tabellen mit der Anzahl der Plannungstufen multiplizieren
					ilRowPosition = ilCount * omPTypes.GetSize();
			}
		break;
	// Gruppen Darstellung
	case 2:
			for (ilCount=0;ilCount<pomGroups->GetSize();ilCount++)
			{
				// Urno wurde gefunden
				if ((*pomGroups)[ilCount].lStfUrno == lpUrno)
					ilRowPosition = ilCount * omPTypes.GetSize();
			}
		break;
	default:
		break;
	}
	
	return (ilRowPosition);

CCS_CATCH_ALL;
return -1;
}

//****************************************************************************
// l�schen einer Schicht, Abwesenheit oder eines Wunsches 
//****************************************************************************

void DutyIsTabViewer::DeleteCode(COleDateTime opDay, CString opOldText)
{
	int ilCodeType = 0;
	bool blIsFreeType;

	// Sanduhr darstellen
	AfxGetApp()->DoWaitCursor(1);

	// Pr�fen ob der Text ein 1. Schicht 2. Abwesenheit 3. Wunschcode ist
	GetCodeType(opDay,opOldText,ilCodeType,blIsFreeType);

	if (ilCodeType == CODE_IS_WIS)
	{
		DeleteDRWData();
	}
	else //if (ilCodeType == CODE_IS_BSD ||  ilCodeType == CODE_IS_ODA)
	{
		// Datensatz, der ge�ndert oder erzeugt wird
		DRRDATA *prlDrrData = NULL;
	
		int ilRow, ilColumn, ilRowOffset, ilColumnOffset; // Paremeter f�r Feld-Koordinaten
		CString olOldText; // Puffer f�r alten Text / Schichtcode
		long llStfUrno; // Mitarbeiter-Urno
		int ilStfRow = 0;	// Mitarbeiter-Position
		CString olPType;	// Dienstplanstufe
		int ilPTypeNr = 0;	// ...als Int
		COleDateTime olDay;	// der Tag, zu dem das Feld geh�rt als vollst�ndiges Datum und...
		int ilDay;	// ...als Tag des Monats und...
		CString olSday;	// ...als String f�r Datensatzabfragen

		// Feldposition und -wert ermitteln
		ilRow			= omSelectedField[0].iRow;
		ilColumn		= omSelectedField[0].iColumn;
		ilRowOffset		= omSelectedField[0].iRowOffset;
		ilColumnOffset	= omSelectedField[0].iColumnOffset;
		olOldText		= omSelectedField[0].oOldText;
	
		// Feldposition und Planungstyp ermitteln
		GetElementAndPType(ilRow+ilRowOffset, ilStfRow, olPType, ilPTypeNr);

		// g�ltiger Feldwert: MA-Urno oder (je nach View-Einstellung 'Sortierung/Gruppierung')
		// Gruppen-Urno innerhalb der Array-Grenzen der internen Datenhaltung?
		if((ilStfRow >= pomStfData->GetSize() && prmViewInfo->iGroup == 1)||(ilStfRow >= pomGroups->GetSize() && prmViewInfo->iGroup == 2))
		{
			// nein -> terminieren
			RemoveWaitCursor();
			return;
		}

		// MA-Urno aus dem jeweiligem Array ermitteln
		if(prmViewInfo->iGroup == 2)	// Gruppen-Urno
			llStfUrno = (*pomGroups)[ilStfRow].lStfUrno;
		else
			llStfUrno = (*pomStfData)[ilStfRow].Urno;	// Mitarbeiter-Urno
	
		// den aktuellen Tag ermitteln aus Starttag der Ansicht und Offset
		olDay = omShowFrom;
		olDay += COleDateTimeSpan(ilColumn+ilColumnOffset,0,0,0);
		ilDay = olDay.GetDay()-1;
		olSday = olDay.Format("%Y%m%d");// volles Datum f�r DRR-Abfrage

		// handling of the second DRR-record (DRRN=2)
		prlDrrData = ogDrrData.GetDrrByKey(olSday,llStfUrno,"2",olPType);
		if (prlDrrData != NULL)
			ogDrrData.Delete(prlDrrData,TRUE);

		// handling of the first DRR-record (DRRN=1)
		prlDrrData = ogDrrData.GetDrrByKey(olSday,llStfUrno,"1",olPType);
		if (prlDrrData != NULL)
			ogDrrData.ClearDrr(prlDrrData);
	}

	RemoveWaitCursor();
	return;
}

//************************************************************************************************************************
// L�schen eines DRW Datensatzes
//************************************************************************************************************************

void DutyIsTabViewer::DeleteDRWData()
{
	TABLEINFO rlTableInfo;
//	CCSPtrArray<DRWDATA>  olDsrList;
	int ilSelectedFields, ilRow, ilColumn, ilRowOffset, ilColumnOffset;
	CString olOldText,olUrno;
	bool olResult = false;
			
	pomDynTab->GetTableInfo(rlTableInfo);
	// Anzahl der ausgew�hlten Felder ermitteln
	ilSelectedFields = omSelectedField.GetSize();
	
	// f�r jedes Ausgew�hlte Feld bearbeiten
	for(int ilField=0; ilField<ilSelectedFields; ilField++)
	{
		ilRow			= omSelectedField[ilField].iRow;
		ilColumn		= omSelectedField[ilField].iColumn;
		ilRowOffset		= omSelectedField[ilField].iRowOffset;
		ilColumnOffset	= omSelectedField[ilField].iColumnOffset;
		olOldText		= omSelectedField[ilField].oOldText;

	
		int ilStfRow = 0;
		CString olPType;
		CString olStfUrno;
		int ilPTypeNr = 0;
		GetElementAndPType(ilRow+ilRowOffset, ilStfRow, olPType, ilPTypeNr);
		// ???
		if((ilStfRow < pomStfData->GetSize() && prmViewInfo->iGroup == 1)
			||(ilStfRow < pomGroups->GetSize() && prmViewInfo->iGroup == 2))
		{
			// Urno des Mitarbeiters finden.
			long llStfUrno = 0;
			if(prmViewInfo->iGroup == 2)
				llStfUrno = (*pomGroups)[ilStfRow].lStfUrno;
			else
				llStfUrno = (*pomStfData)[ilStfRow].Urno;

			olStfUrno.Format("%ld",llStfUrno);

			// Datum berechnen
			COleDateTime olDay = omShowFrom;
			olDay += COleDateTimeSpan(ilColumn+ilColumnOffset,0,0,0);
			CString olSday = olDay.Format("%Y%m%d");

			// Exitiert ein Datensatz mit diesen Daten ?
			olUrno = ogBCD.GetFieldExt("DRW", "SDAY","STFU", olSday,olStfUrno,"URNO");
			if (!olUrno.IsEmpty())
			{
				olResult = ogBCD.DeleteRecord("DRW","URNO",olUrno, true);
			}

			// Ergenniss OK ?
			if(!olResult)
				{
				CString olErrorTxt;
			//	olErrorTxt.Format("%s %d\n%s",LoadStg(ST_UPDATEERR), ogDrwData.imLastReturnCode, ogDrwData.omLastErrorMessage);
				::MessageBox(NULL, olErrorTxt,LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
			}
		}
	}
}

//**************************************************************************************
// Berechnung der Row und der Column f�r ein Datenelement
//**************************************************************************************

bool DutyIsTabViewer::GetRowAndColumn(long opSearchUrno,CString opPlaningType, CString opSday,
									  int &ipRow,int &ipColumn)
{
	int		ilStfNr = -1;
	int		ilStfSize;
	int		ilTypeNr = -1;
	int		ilUpdateRow;

	// Ben�tigt zu Berechnung des Offsets
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);

	//---------------------------------------------------------------------
	// Abh�ngig von der Gruppierung die Row berechnen
	//---------------------------------------------------------------------
	if(prmViewInfo->iGroup == 2)
	{
		ilStfSize = pomGroups->GetSize();
		for(int i=0; i<ilStfSize; i++)
		{
			if((*pomGroups)[i].lStfUrno == opSearchUrno)
			{
				ilStfNr = i;
				i = ilStfSize;
			}
		}
	}
	else
	{
		ilStfSize = pomStfData->GetSize();
		for(int i=0; i<ilStfSize; i++)
		{
			if((*pomStfData)[i].Urno == opSearchUrno)
			{
				ilStfNr = i;
				i = ilStfSize;
			}
		}
	}
	//---------------------------------------------------------------------
	// Nummer der Planungstufe berechnen
	//---------------------------------------------------------------------
	for(int i=0; i<omPTypes.GetSize(); i++)
	{
		if(omPTypes[i] == opPlaningType)
		{
			ilTypeNr = i;
			i = omPTypes.GetSize();
		}
	}
	if(ilStfNr < 0 || ilTypeNr <0)	return false;


	// ilUpdateRow enstpricht Row + Offset
	ilUpdateRow = (ilStfNr*omPTypes.GetSize())+ilTypeNr;
	ipRow  = ilUpdateRow - rlTableInfo.iRowOffset;
	
	//---------------------------------------------------------------------
	// Column berechnen
	//---------------------------------------------------------------------
	CString olDayString;
	olDayString.Format("%s000000",opSday);
	CTime olDay = DBStringToDateTime(olDayString);
	if(olDay == -1)	return false;

	//int ilDay = olDay.GetDay()-1;
	COleDateTime olUpdateDay;
	olUpdateDay.SetDate(olDay.GetYear(), olDay.GetMonth(), olDay.GetDay());
	COleDateTimeSpan olDayDiv =  olUpdateDay - omShowFrom;
	long llUpdateColumn = olDayDiv.GetDays();

	if(llUpdateColumn  < 0 || ilTypeNr <0)	return false;

	// ilUpdateColumn enstpricht Column + Offset
	ipColumn = llUpdateColumn - rlTableInfo.iColumnOffset;

	if(ipColumn  < 0) return false;

	return true;
}

//**************************************************************************************
// True wenn bereits Felder markiert sind, sonst false
//**************************************************************************************

bool DutyIsTabViewer::IsMultiSelection()
{
	if (omSelectedField.GetSize() == 0)
		return false;
	else
		return true;
}

//**************************************************************************************
// Pr�ft ob ein DRW Datensatz extra Informationen enth�lt
//**************************************************************************************

bool DutyIsTabViewer::HasDrwExtraInformation(CString opSday,CString opStfu)
{
	bool blExtraInformation = false;

	// Strings f�llen
	CString olPrio = ogBCD.GetFieldExt("DRW", "SDAY","STFU", opSday,opStfu,"PRIO");
	CString olReme = ogBCD.GetFieldExt("DRW", "SDAY","STFU", opSday,opStfu,"REME");
	CString olRemi = ogBCD.GetFieldExt("DRW", "SDAY","STFU", opSday,opStfu,"REMI");
	CString olRems = ogBCD.GetFieldExt("DRW", "SDAY","STFU", opSday,opStfu,"REMS");

	// Bemerkungen pr�fen
	if (!olReme.IsEmpty() || !olRemi.IsEmpty() || !olRems.IsEmpty())
	{
		blExtraInformation = true;
	}
	// Priorit�t pr�fen
	if (olPrio != "0" && !olPrio.IsEmpty())
	{
		blExtraInformation = true;
	}
	return (blExtraInformation);
}

//**************************************************************************************
// Ermittelt alle relevanten Werte f�r die Wunschplanungsstufe
//**************************************************************************************

void DutyIsTabViewer::GenerateWishValues(long lpTmpUrno,CString opTmpUrno,COleDateTime olDay,CString olSday,FIELDDATA *popFieldData,int ipRow,int ipColumn)
{
	CString olText;
	// WISC Code f�r diesen Tag und Mitarbeiter erhalten
	CString olWisUrno = ogBCD.GetFieldExt("DRW", "SDAY","STFU", olSday,opTmpUrno,"URNO");

	if (!olWisUrno.IsEmpty())
	{
		olText = ogBCD.GetFieldExt("DRW", "SDAY","STFU", olSday,opTmpUrno,"WISC");
		// Extra Informationen f�r DRW Anzeigen, sollten Sie im editierbaren Bereich
		// liegen
		if(olDay >= omFrom && olDay <= omTo) 
		{
			if (HasDrwExtraInformation(olSday,opTmpUrno))
				popFieldData->iRTTriangleColorNr = BPN_RED;
		}
	}
	else
	{
		// DefaultZeichen f�r nicht geplante Tage
		olText = ogCCSParam.GetParamValue(ogAppl,"ID_BLANK_CHAR");
	}

	//--------------------------------------------------------------------------------------
	//Ist es au�erhalb des Benutzer vom definierten Zeitraumes
	//--------------------------------------------------------------------------------------
	if(olDay < omFrom || olDay > omTo) 
	{
		// -> Au�erhalb
		popFieldData->bInplaceEdit = false;
		popFieldData->bDrop		   = false;
		popFieldData->bDrag		   = false;
		popFieldData->oBkColorNr = BPN_LIGHTSILVER1;
	}
	else
	{
		// -> Innerhalb
		popFieldData->bInplaceEdit = true;
		popFieldData->bDrop		 = true;
		popFieldData->bDrag		 = false;
		popFieldData->iRButtonDownStatus = RBDS_ALL;

		// Hintergrund festlegen: Wei�, Rosa oder Blau
		//Handelt es sich um ein Feld mit blauem Hintergrund ? (Multiselektion ?)

		if (IsFieldMultiselected(ipRow,ipColumn))
		{
			// Blau
			popFieldData->oBkColorNr = BPN_SELECTBLUE;
		}
		else
		{
			// G�ltiger Arbeitsvertrag vorhanden ?
			if (!ogStfData.IsStfNowValid(lpTmpUrno,olDay))
				// Rosa
				popFieldData->oBkColorNr = BPN_SILVERREDLIGHT3;
			else
				// Wei�
				popFieldData->oBkColorNr = BPN_WHITE;
		}
	}
	//--------------------------------------------------------------------------------------
	// Text setzen
	popFieldData->oText	= olText;
}

//**************************************************************************************
// Pr�ft, ob ein Feld durch Multiselektion ausgew�hlt wurde
//**************************************************************************************

bool DutyIsTabViewer::IsFieldMultiselected(int ipRow,int ipColumn)
{
	int ilSelectedFields = omSelectedField.GetSize();
	bool blResult = false;

	for(int ilField=0; ilField<ilSelectedFields; ilField++)
	{
		if((omSelectedField[ilField].iRow + omSelectedField[ilField].iRowOffset) == ipRow && (omSelectedField[ilField].iColumn + omSelectedField[ilField].iColumnOffset) == ipColumn)
		{
			blResult = true;
		}
	}
	return blResult;
}

/**************************************************************************************
Urno zu �bergebenden Code ermitteln
ACHTUNG! f�r W�nsche wird nur 1 zur�ckgegeben, keine Urno
**************************************************************************************/

long DutyIsTabViewer::GetUrnoForCode(CString olNewText,int ilCodeType,COleDateTime olDay)
{
CCS_TRY;
	BSDDATA* polBsd;
	ODADATA* polOda;

	switch (ilCodeType)
	{
	case CODE_IS_WIS:
		// Wunschcode
		if(ogWisData.CheckCode(olNewText))
		{
			// das ist nur um zu checken, ob es ein Wunsch ist
			return 1;	// DUMMY-URNO
		}
		break;
	case CODE_IS_BSD:
		// Schichtcode
		polBsd = ogBsdData.GetBsdByBsdcAndDate(olNewText,olDay);
		if (polBsd != NULL) 
			return polBsd->Urno;
		break;
	case CODE_IS_ODA:
		// Abwesenheitscode
		polOda = ogOdaData.GetOdaBySdac(olNewText);
		if (polOda != NULL) 
			return polOda->Urno;
		break;
	}
	return 0;

CCS_CATCH_ALL;
	return 0;
}

//************************************************************************************************************************
// Zur�cksetzten aller ausgw�hlter DRRs
//************************************************************************************************************************

void DutyIsTabViewer::ClearSelectedDrrData()
{
	TABLEINFO rlTableInfo;
	int ilRow, ilColumn, ilRowOffset, ilColumnOffset;
	CString olOldText,olUrno;
	//bool olResult;
			
	pomDynTab->GetTableInfo(rlTableInfo);
	// Anzahl der ausgew�hlten Felder ermitteln
	//ilSelectedFields = omSelectedField.GetSize();
	
	// f�r jedes Ausgew�hlte Feld bearbeiten
	for(int ilField=0; ilField<omSelectedField.GetSize(); ilField++)
	{
		ilRow			= omSelectedField[ilField].iRow;
		ilColumn		= omSelectedField[ilField].iColumn;
		ilRowOffset		= omSelectedField[ilField].iRowOffset;
		ilColumnOffset	= omSelectedField[ilField].iColumnOffset;
		olOldText		= omSelectedField[ilField].oOldText;
	
		int ilStfRow = 0;
		CString olPType;
		CString olStfUrno;
		int ilPTypeNr = 0;
		GetElementAndPType(ilRow+ilRowOffset, ilStfRow, olPType, ilPTypeNr);
		// G�ltigkeitspr�fung
		if((ilStfRow < pomStfData->GetSize() && prmViewInfo->iGroup == 1)
			||(ilStfRow < pomGroups->GetSize() && prmViewInfo->iGroup == 2))
		{
			// Urno des Mitarbeiters finden.
			long llStfUrno = 0;
			if(prmViewInfo->iGroup == 2)
				llStfUrno = (*pomGroups)[ilStfRow].lStfUrno;
			else
				llStfUrno = (*pomStfData)[ilStfRow].Urno;

			olStfUrno.Format("%ld",llStfUrno);

			// Datum berechnen
			COleDateTime olDay = omShowFrom;
			olDay += COleDateTimeSpan(ilColumn+ilColumnOffset,0,0,0);
			CString olSday = olDay.Format("%Y%m%d");

			// DRR-Datensatz suchen
			DRRDATA* prlDrrData = ogDrrData.GetDrrByKey(olSday,llStfUrno,"1"/*Schichtnummer*/,olPType);

			if (prlDrrData != NULL)
			// DRR zur�cksetzen
				ogDrrData.ClearDrr(prlDrrData);
		}
	}
}




