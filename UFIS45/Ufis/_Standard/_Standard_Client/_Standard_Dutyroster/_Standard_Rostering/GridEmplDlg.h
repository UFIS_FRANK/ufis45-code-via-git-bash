#if !defined(AFX_GRIDEMPLDLG_H__6AAD64E5_9299_11D3_8FB0_00500454BF3F__INCLUDED_)
#define AFX_GRIDEMPLDLG_H__6AAD64E5_9299_11D3_8FB0_00500454BF3F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GridEmplDlg.h : Header-Datei
//

#define EMPLCOLCOUNT 6

class CGridControl;

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CGridEmplDlg 

class CGridEmplDlg : public CDialog
{
// Konstruktion
public:
	CGridEmplDlg(CStringList* popUrnoList, CString* opSelectUrno,CMapStringToOb* popExtraUrnoMap,CWnd* pParent = NULL);   // Standardkonstruktor
	~CGridEmplDlg();   	// Destruktor


// Dialogfelddaten
	//{{AFX_DATA(CGridEmplDlg)
	enum { IDD = IDD_GRID_EMPL_DLG };
	CButton	m_B_MoveTo;
	CButton	m_B_Cancel;
	CButton	m_B_Insert;
	CButton	m_B_Delete;
	CButton	m_B_Find;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CGridEmplDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Zeiger auf aktuelle Auswahl
	CString*			omSelectUrno;
	// Diese Urno Liste wurde übergeben
	CStringList*		pomUrnoList;
	// Liste der zusätzlichen MA
	CMapStringToOb*		pomExtraUrnoMap;	// schnelles Suchen
	// Diese Urno Liste wird lokal kopieren
	CStringList*		pomLocalUrnoList;
	
	// Das Grid
	CGridControl *pomEmplList;

	// Grid initialisieren
	void IniGrid ();
	void IniColWidths();
	void FillGrid();
	bool SortGrid(int ipRow);

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CGridEmplDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnBDelete();
	afx_msg void OnBInsert();
	afx_msg void OnBFind();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg LONG OnGridLButton(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBMoveto();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	DutyRoster_View* pomParent;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_GRIDEMPLDLG_H__6AAD64E5_9299_11D3_8FB0_00500454BF3F__INCLUDED_
