#ifndef AFX_DUTYRELEASEDLG_H__EAB52541_7566_11D2_805D_004095434A85__INCLUDED_
#define AFX_DUTYRELEASEDLG_H__EAB52541_7566_11D2_805D_004095434A85__INCLUDED_

// DutyReleaseDlg.h : Header-Datei
//
#include <stdafx.h>
#include <DutyRoster_View.h>
#include <CedaBsdData.h>
#include <CedaOdaData.h>
#include <UDlgStatusBar.h>

#define RELEASECOLCOUNT 6

class CGridControl;

// Auszug aus STF
struct STFVIEWDATA
{
	CString Finm; 	// Vorname
	CString Lanm; 	// Name
	CString Makr; 	// Mitarbeiterkreis
	CString Peno; 	// Personalnummer
	CString Perc; 	// Kürzel
	CString Prmc; 	// Funktion
	CString	Urno;  	// Eindeutige Datensatz-Nr.
	COleDateTime Dodm; 	// Austrittsdatum
	COleDateTime Doem;	// Eintrittsdatum

};
// end STFVIEWDATA

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld DutyReleaseDlg 

class DutyReleaseDlg : public CDialog
{
// Konstruktion
public:
	DutyReleaseDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	~DutyReleaseDlg();
	VIEWINFO*			prmViewInfo;

private:
	DutyRoster_View*	pomParent;
	CCSPtrArray<STFDATA>	*pomStfData;
	CCSPtrArray<STFVIEWDATA> omStfLines;
	

	// Beinhaltet alle ausgewählten Mitarbeiter
	CList<long,long>	omUrnoList;
	CString				omVpfr;
	CString				omVpto;

	CStringArray		omPTypes;

	UDlgStatusBar*		pomStatusBar;
	int					imIDStatic;
	int					imIDProgress;
	UINT				imStepWithAbsenceLevel;
	CString omStepBeforAbsenceLevel;
	// Das Grid
	CGridControl*		pomGrid;

	// Grid initialisieren
	void IniGrid ();
	void IniColWidths();
	void FillGrid();
	bool SortGrid(int ipRow);
	// Selection auslesen.
	void GetSelectedItems();

	void DoRelease(CString opReleaseString, CString opFromType);

public:

// Dialogfelddaten
	//{{AFX_DATA(DutyReleaseDlg)
	enum { IDD = IDD_DUTY_RELEASE_DLG };
	CButton	m_B_ShowJobs;
	CButton	m_RelNow;
	CButton	m_RelNight;
	CButton	m_b_Find;
	CButton	m_Plan1;
	CButton	m_Plan2;
	CButton	m_Plan3;
	CButton	m_Plan4;
	CButton	m_PlanL;
	CCSEdit	m_DayTo;
	CCSEdit	m_DayFrom;
	CStatic	m_SDayTo;
	CStatic	m_SDayFrom;
	CColorListBox	m_LB_List;
	CStatic	m_S1;
	CStatic	m_S2;
	CStatic	m_S3;
	CStatic	m_S4;
	CStatic	m_S5;
	CStatic	m_SL;
	CStatic	m_SU;
	//}}AFX_DATA

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(DutyReleaseDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

public:
	void InternalCancel();

// Implementierung
protected:

	// Oberfläschen Elemente setzen nach Partab Einträgen
	void ShowSteps();
	// Anzeige der Planungsstufen
	bool bmShow1,bmShow2,bmShow3,bmShow4,bmShow5,bmShowL,bmShowU;

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(DutyReleaseDlg)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnBAll();
	afx_msg void OnBNothing();
	afx_msg void OnPaint();
	afx_msg void OnBFind();
	afx_msg void OnBMax();
	afx_msg void OnBMin();
	afx_msg void OnShowJobs();
	afx_msg void OnUnrelease();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_DUTYRELEASEDLG_H__EAB52541_7566_11D2_805D_004095434A85__INCLUDED_
