// ImportShifts.cpp : implementation file
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImportShifts dialog


CImportShifts::CImportShifts(CWnd* pParent /*=NULL*/)
	: CDialog(CImportShifts::IDD, pParent)
{
	//{{AFX_DATA_INIT(CImportShifts)
	//}}AFX_DATA_INIT
}


void CImportShifts::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CImportShifts)
	DDX_Control(pDX, IDC_EDIT1, m_From);
	DDX_Control(pDX, IDC_EDIT2, m_To);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CImportShifts, CDialog)
	//{{AFX_MSG_MAP(CImportShifts)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImportShifts message handlers

BOOL CImportShifts::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(LoadStg(IDD_IMPORTSHIFTS));
	SetDlgItemText(IDC_S_von,LoadStg(IDC_S_von));
	SetDlgItemText(IDC_S_bis,LoadStg(IDC_S_bis));

	m_From.SetTypeToDate();
	m_From.SetTextErrColor(RED);
	m_From.SetBKColor(LTYELLOW);
	m_From.SetInitText("");
	
	m_To.SetTypeToDate();
	m_To.SetTextErrColor(RED);
	m_To.SetBKColor(LTYELLOW);
	m_To.SetInitText("");

	return TRUE; 
}

void CImportShifts::OnOK() 
{
	CString olFrom, olTo;
	m_From.GetWindowText(olFrom);
	m_To.GetWindowText(olTo);

	CString olErrorText;
	
	COleDateTime olDateFrom, olDateTo;
	bool blOK = true;
	if(m_From.GetStatus() && m_To.GetStatus() && olFrom.GetLength() && olTo.GetLength())
	{
		olDateFrom = OleDateStringToDate(olFrom);
		olDateTo = OleDateStringToDate(olTo);
		if(	olDateFrom.GetStatus() == COleDateTime::valid && 
			olDateTo.GetStatus() == COleDateTime::valid &&
			olDateTo > olDateFrom)
		{
			// Zeitangaben korrekt
		}
		else
		{
			// Fehler 
			blOK = false;
			// Fehler bei der Eingabe von Zeit oder Datum.\n*INTXT*
			olErrorText = LoadStg(IDS_STRING1855);
		}
	}
	else
	{
		// Fehler 
		blOK = false;
		// Fehler bei der Eingabe von Zeit oder Datum.\n*INTXT*
		olErrorText = LoadStg(IDS_STRING1855);
	}

	if(!blOK)
	{
		MessageBox(olErrorText,LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);

		return;
	}

	// Datenimport-Kommando absetzen
	ogDrrData.InitializeShiftImport(olDateFrom, olDateTo);

	CDialog::OnOK();
}
