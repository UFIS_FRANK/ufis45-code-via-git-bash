// CedaData.cpp: implementation of the CedaData class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CedaData::CedaData(CCSCedaCom *popCommHandler/* = NULL*/)
{
//	omServerName = ogCommHandler.pcmRealHostName;
	CCSCedaData::CCSCedaData(popCommHandler);
}

bool CedaData::Initialize(CString opServerName)
{
//	omServerName = opServerName;
	return true;
}

/****************************************************************************************
bda: makes a formatted string with all fields of a data structure for trace and debug output
****************************************************************************************/
bool CedaData::GetDataFormatted(CString &pcpListOfData, void *pvpDataStruct)
{
	CString olFieldData;
	//CString olList;
	//olList.GetBufferSetLength(1024);

    // Start conversion, field-by-field
    int nfields = omRecInfo.GetSize();
    for (int fieldno = 0; fieldno < nfields; fieldno++)
    {
		pcpListOfData += omRecInfo[fieldno].Name;
		pcpListOfData += ": ";

        CString s;
        char buf[33];   // ltoa() need buffer 33 bytes
        int type = omRecInfo[fieldno].Type;
        void *p = (void *)((char *)pvpDataStruct + omRecInfo[fieldno].Offset);
        int length = omRecInfo[fieldno].Length;

		if(p != NULL)
		{
			switch (type) 
			{
			case CEDACHAR:
				s = (length == 0)? CString(" "): CString((char *)p).Left(length);
				MakeCedaString(s);
				olFieldData = s;
				break;
			case CEDAINT:
				olFieldData = CString(itoa(*(int *)p, buf, 10));
				break;
			case CEDALONG:
				olFieldData = CString(ltoa(*(long *)p, buf, 10));
				break;
			case CEDADATE:  // format: DD.MM.YY HH:MM:SS
				olFieldData = ((CTime *)p)->Format("%d.%m.%y %H:%M:%S");
				break;
			case CEDAOLEDATE:  // format: DD.MM.YY HH:MM:SS
				if(((COleDateTime *)p)->GetStatus() == COleDateTime::valid)
				{
					olFieldData = ((COleDateTime *)p)->Format("%d.%m.%y %H:%M:%S");
				}
				else if(((COleDateTime *)p)->GetStatus() == COleDateTime::invalid)
				{
					olFieldData = "invalid         "; 
				}
				else
				{
					olFieldData = "null            "; 
				}
				break;
			case CEDADOUBLE:
				char pclBuf[128];
				//MWO 18.12.98
				sprintf(pclBuf,"%lf",(double *)p);
				//			sprintf(pclBuf,"%.2f",(double *)p);
				olFieldData = CString(pclBuf);
				break;
			default:    // invalid field type
				return false;
			}
		}
		else
		{
			olFieldData = "invalid field   "; 
		}
		pcpListOfData += olFieldData;
		if(fieldno < nfields-1)
			pcpListOfData += ", ";
    }
	
	//pcpListOfData += olList;

	if(pcpListOfData.GetLength() > 0)
		pcpListOfData = pcpListOfData.Left(pcpListOfData.GetLength()-1);
	
	//pcpListOfData.SetAt(pcpListOfData.GetLength()-1,' ');
	//pcpListOfData.TrimRight( );
	
    return true;
}

/****************************************************************************************
bda: makes a specialized string for defect data set as in the GetDataFormatted
****************************************************************************************/
bool CedaData::GetDefectDataString(CString &pcpListOfData, void *pvpDataStruct, LPCTSTR pcpExplanation/*=0*/)
{
	pcpListOfData += pcmTableName;
	pcpListOfData += " defect: ";
	if(pcpExplanation != 0)
	{
		pcpListOfData += pcpExplanation;
		pcpListOfData += "\n";
	}
	if(!GetDataFormatted(pcpListOfData, pvpDataStruct)) return false;
	pcpListOfData += "\n";
	return true;
} 

int CedaData::ExtractTextLineFast(CStringArray &opItems, char *pcpLineText, char *pcpSepa)
{
	int ilSepaLen = strlen(pcpSepa);
	int ilCount = 0;
	char *currPtr;
	currPtr = strstr(pcpLineText, pcpSepa);
	while (currPtr != NULL)
	{
		*currPtr = '\0';
		opItems.Add(pcpLineText);
		ilCount++;
		pcpLineText = currPtr + ilSepaLen;
		currPtr		= strstr(pcpLineText, pcpSepa);
	}
	if(strcmp(pcpLineText, "") != 0)
	{
		opItems.Add(pcpLineText);
		ilCount++;
	}

	return ilCount;
}

/**************************************************************************************
Buffer call of the corresponding CCSCedaData::function
**************************************************************************************/

bool CedaData::CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList, char *pcpSelection, char *pcpSort, char *pcpData, char *pcpDest /*= "BUF1"*/,bool bpIsWriteAction /*= false*/, long lpID /*= 0*/, CCSPtrArray<RecordSet> *pomData /*= NULL*/, int ipFieldCount /*= 0*/)
{
	TRACE("CedaAction long version\n");
	return CCSCedaData::CedaAction(pcpAction, pcpTableName, pcpFieldList, pcpSelection, pcpSort, pcpData, pcpDest, bpIsWriteAction, lpID, pomData, ipFieldCount);
}
	
/*************************************************************************************
// CedaAction: 
sqlhdl allokiert standardm�ssig 2MB, so wenn wir eine gr�ssere Datenmenge anfordern, 
reallokiert sqlhdl 'endlos' den Speicher.
Um das zu umgehen, segmentieren wir unseren SQL-Statement auf llMaxPacketSize,
wobei eine andere Begrenzung unseren SQL-Statement fr�her einschr�nkt: [4096] Byte in CCSClassLib f�r String

// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
*************************************************************************************/
/*	bda 25.09.2001: laut HG ist das Problem bei sqlhdl korrigiert, so dass segmentiertes Lesen �berfl�ssig ist
	CCS_TRY;

	bool	blRet;
	CString	olAction = CString (pcpAction);

	// Feldliste retten
	CString olOrigFieldList = GetFieldList();

	if (olAction != "RT")
	{
		// CedaAction ausf�hren
		blRet = CCSCedaData::CedaAction2(pcpAction,pcpTableName,pcpFieldList,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID,pomData,ipFieldCount);
	}
	else
	{
		CStringArray olWhereArray;
		CString		 olTmpStr			= "";
		int			 ilRecordWidth		= 0;
		long		 llMaxPacketSize	= 2000000;	//load max. 2 MB per CedaAction
		long		 llArraySize;

		// Breite der zu ladenden Felder berechnen
		int nfields = omRecInfo.GetSize();
		for (int fieldno = 0; fieldno < nfields; fieldno++)
		{
			ilRecordWidth += omRecInfo[fieldno].Length;
		}

		//URNOs der zu ladenden Datens�tze ermitteln
		olTmpStr = CString("URNO");
		blRet = CCSCedaData::CedaAction2(pcpAction,pcpTableName,olTmpStr.GetBuffer (0),pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID,pomData,ipFieldCount);

		//where-strings basteln und ins array packen
		olTmpStr = "";
		llArraySize = omDataBuf.GetSize();
		if ((llArraySize * ilRecordWidth) > llMaxPacketSize)
		{
			// Berechnung der Anzahl der URNOs
			long llNbrUrnos = (long)(llMaxPacketSize / ilRecordWidth);

			for (long l = 0; l < llArraySize; l++)
			{
				olTmpStr += "'" + omDataBuf.GetAt (l) + "',";
				if ((l != 0) && (((l % llNbrUrnos) == 0) || ((l % 300) == 0))) //% 300 wg. Begrenzung where-clause in CCSClassLib
				{
					//letztes Komma entfernen
					olTmpStr = olTmpStr.Left (olTmpStr.GetLength() - 1);
					
					//string ins StringArray packen
					olWhereArray.Add (olTmpStr);
					
					//string wieder zur�cksetzen
					olTmpStr = "";
				}
			}
			olTmpStr = olTmpStr.Left (olTmpStr.GetLength() - 1);
			olWhereArray.Add (olTmpStr);

			llArraySize = olWhereArray.GetSize();
			for (l = 0; l < llArraySize; l++)
			{
				olTmpStr = CString("WHERE URNO IN (") + olWhereArray.GetAt(l) + CString(")");
				
				// CedaAction ausf�hren
				if (l == 0)
				{
					blRet = CCSCedaData::CedaAction2(pcpAction,pcpTableName,pcpFieldList,olTmpStr.GetBuffer (0),pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID,pomData,ipFieldCount);
				}
				else
				{
					blRet = CCSCedaData::CedaAction2(pcpAction,pcpTableName,pcpFieldList,olTmpStr.GetBuffer (0),pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID,pomData,ipFieldCount,false);
				}
			}
		}
		else
		{
			blRet = CCSCedaData::CedaAction2(pcpAction,pcpTableName,pcpFieldList,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID,pomData,ipFieldCount);
		}
	}

	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);

	// R�ckgabecode von CedaAction durchreichen
	return blRet;
	CCS_CATCH_ALL;
	return false;

}
*/

/**************************************************************************************
Buffer call of the corresponding CCSCedaData::function
**************************************************************************************/
bool CedaData::CedaAction(char *pcpAction, char *pcpSelection, char *pcpSort, char *pcpData, char *pcpDest /*"BUF1"*/, bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{
	TRACE("CedaAction short version\n");
	return CCSCedaData::CedaAction(pcpAction, pcpSelection, pcpSort, pcpData, pcpDest, bpIsWriteAction, lpID);
}

/**************************************************************************************
Buffer call of the corresponding CCSCedaData::function
**************************************************************************************/
bool CedaData::GetBufferRecord(int ipLineNo, void *pvpDataStruct)
{
	return CCSCedaData::GetBufferRecord(ipLineNo, pvpDataStruct);
}

/**************************************************************************************
Buffer call of the corresponding CCSCedaData::function
**************************************************************************************/
bool CedaData::GetBufferRecord(CCSPtrArray<CEDARECINFO> *pomRecInfo, int ipLineNo, void *pvpDataStruct)
{
	return CCSCedaData::GetBufferRecord(pomRecInfo, ipLineNo, pvpDataStruct);
}

/**************************************************************************************
Buffer call of the corresponding CCSCedaData::function
**************************************************************************************/
bool CedaData::GetBufferRecord(CCSPtrArray<CEDARECINFO> *pomRecInfo, CString record, void *pvpDataStruct)
{
	return CCSCedaData::GetBufferRecord(pomRecInfo, record, pvpDataStruct);
}

/**************************************************************************************
Buffer call of the corresponding CCSCedaData::function
**************************************************************************************/
bool CedaData::GetBufferRecord(int ipLineNo, void *pvpDataStruct, CString &olFieldList)
{
	return CCSCedaData::GetBufferRecord(ipLineNo, pvpDataStruct, olFieldList);
}

/**************************************************************************************
Buffer call of the corresponding CCSCedaData::function
**************************************************************************************/
bool CedaData::GetBufferRecord(CCSPtrArray<CEDARECINFO> *pomRecInfo, int ipLineNo, void *pvpDataStruct, CString &olFieldList)
{
	return CCSCedaData::GetBufferRecord(pomRecInfo, ipLineNo, pvpDataStruct, olFieldList);
}

/**************************************************************************************
Buffer call of the corresponding CCSCedaData::function
**************************************************************************************/
bool CedaData::GetFirstBufferRecord(void *pvpDataStruct)
{
	return CCSCedaData::GetFirstBufferRecord(pvpDataStruct);
}

/**************************************************************************************
Buffer call of the corresponding CCSCedaData::function
**************************************************************************************/
bool CedaData::GetFirstBufferRecord(CCSPtrArray<CEDARECINFO> *pomRecInfo, void *pvpDataStruct)
{
	return CCSCedaData::GetFirstBufferRecord(pomRecInfo, pvpDataStruct);
}

bool CedaData::CedaAction2(char *pcpAction,
    char *pcpTableName, char *pcpFieldList,
    char *pcpSelection, char *pcpSort,
    char *pcpData,
	char *pcpDest,
	bool bpIsWriteAction /* false */,
	long lpID /* = 0*/,
	CCSPtrArray<RecordSet> *pomData,
	int ipFieldCount,
	bool bpDelDataArray /* true */)
{
	ASSERT(strcmp(pcpTableName,pcmTableName) == 0);
	ASSERT(strlen(pcpFieldList) == 0 || strcmp(pcpFieldList,pcmFieldList) == 0);

	bool blRet;
	blRet = CCSCedaData::CedaAction2(pcpAction,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID,bpDelDataArray);
	CString olError = CCSCedaData::omLastErrorMessage;
	if (blRet != true || olError != "")
	{
		if (olError != "No Data Found")
		{
		
			MessageBox(NULL, CCSCedaData::omLastErrorMessage + "\n\nThe Application will terminate.", "Error", MB_OK);
			exit (0);
		}
	}
	return blRet;
}

bool CedaData::CedaAction2(char *pcpAction,char *pcpSelection,char *pcpSort,char *pcpData,char *pcpDest,bool bpIsWriteAction,long lpID, bool bpDelDataArray)
{
	bool blRet;
	blRet = CCSCedaData::CedaAction2(pcpAction,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID,bpDelDataArray);
	CString olError = CCSCedaData::omLastErrorMessage;
	if (blRet != true || olError != "")
	{
		if (olError != "No Data Found")
		{
		
			//here
			MessageBox(NULL, olError + "\n\nThe Application will terminate.", "Error", MB_OK);
			exit (0);
		}
	}
	return blRet;
}

/*
      \item  {\bf RC_SUCCESS	  =  0}    Everthing OK
      \item  {\bf RC_FAIL		  = -1}    General serious error
      \item  {\bf RC_COMM_FAIL	  = -2}    WINSOCK.DLL error
      \item  {\bf RC_INIT_FAIL	  = -3}    Open the log file
      \item  {\bf RC_CEDA_FAIL	  = -4}    CEDA reports an error
      \item  {\bf RC_SHUTDOWN	  = -5}    CEDA reports shutdown
      \item  {\bf RC_ALREADY_INIT = -6}    InitComm called up for a second line
      \item  {\bf RC_NOT_FOUND	  = -7}    Data not found (More than 7 applications are running)
      \item  {\bf RC_DATA_CUT	  = -8}    Data incomplete
      \item  {\bf RC_TIMEOUT	  = -9}    CEDA reports a time-out
*/