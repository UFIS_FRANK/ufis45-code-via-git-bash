#ifndef AFX_SHIFTNEWGSPDLG_H__7891F412_CEC0_11D1_BFCE_004095434A85__INCLUDED_
#define AFX_SHIFTNEWGSPDLG_H__7891F412_CEC0_11D1_BFCE_004095434A85__INCLUDED_

// ShiftNewGSPDlg.h : Header-Datei
//
#include <stdafx.h>
#include <resource.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld ShiftNewGSPDlg 

class ShiftNewGSPDlg : public CDialog
{
// Konstruktion
public:
	ShiftNewGSPDlg (CString *popGPLName, CTime *popGPLVafr, CTime *popGPLVato, CString opTyp, CWnd* pParent = NULL);   // Standardkonstruktor
	CString GetReplacementFunctionUrnoOld();
	CString GetReplacementFunctionUrnoNew();
	CString *pomGPLName;
	CString omTyp;
	CTime *pomGPLVafr;
	CTime *pomGPLVato;
	afx_msg void OnBtnFrom(); 
    afx_msg void OnBtnTo(); 

   //afx_msg void show(); 
// Dialogfelddaten
	//{{AFX_DATA(ShiftNewGSPDlg)
	enum { IDD = IDD_SHIFT_NEWGSP_DLG };
	CComboBox	m_CmbFctcOld;
	CComboBox	m_CmbFctcNew;
	CStatic	m_S_Name;
	CStatic	m_S_Valid;
	CButton	m_B_Cancel;
	CButton	m_B_Ok;
	CCSEdit	m_E_Name;
	CCSEdit	m_E_GPL_Vafr;
	CCSEdit	m_E_GPL_Vato;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(ShiftNewGSPDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(ShiftNewGSPDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void HideFunctionReplacement();
	void FillComboBoxes();
	CString omPfcOld;
	CString omPfcNew;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_SHIFTNEWGSPDLG_H__7891F412_CEC0_11D1_BFCE_004095434A85__INCLUDED_
