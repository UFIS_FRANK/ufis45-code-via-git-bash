// CedaStfData.h

#ifndef __CEDAPSTFDATA__
#define __CEDAPSTFDATA__
 
#include <stdafx.h>
#include <basicdata.h>
#include <afxdisp.h>

//---------------------------------------------------------------------------------------------------------
 
struct STFDATA 
{
	CTime 	 Cdat; 			// Erstellungsdatum
	COleDateTime 	 Dodm; 	// Austrittsdatum
	COleDateTime 	 Doem; 	// Eintrittsdatum
	COleDateTime 	 Dobk; 	// Wieder-Eintrittsdatum
	char 	 Finm[42]; 		// Vorname
	char 	 Lanm[42]; 		// Name
	CTime 	 Lstu;			// Datum letzte �nderung
	char 	 Makr[7];		// Mitarbeiterkreis
	char 	 Peno[22];	 	// Personalnummer
	char 	 Perc[5];		// K�rzel
	char 	 Prfl[3];		// Protokollierungskennung
	char	 Regi[3];		// 'Regelm�ssigkeits'-Indicator (I,R,Blank=nichts bei Aushilfen - in diesem Fall gilt Regi aus COTDATA)
	char 	 Rema[62];	 	// Bemerkungen
	char 	 Shnm[42];	 	// Kurzname 
	char 	 Teld[22];	 	// Telefonnr. dienstlich
	char 	 Telh[22];	 	// Telefonnr. Handy
	char 	 Telp[22];	 	// Telefonnr. privat
	char 	 Tohr[3];		// Soll an SAP-HR �bermittelt werden
	long 	 Urno; 			// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 		// Anwender (Ersteller)
	char 	 Useu[34]; 		// Anwender (letzte �nderung)

	//DataCreated by this class
	int      IsChanged;

	STFDATA(void)
	{
		memset(this,'\0',sizeof(*this));
		Cdat=TIMENULL;
		Lstu=TIMENULL;
		Dodm.SetStatus(COleDateTime::invalid);
		Doem.SetStatus(COleDateTime::invalid);
		Dobk.SetStatus(COleDateTime::invalid);
	}

}; // end STFDataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaStfData: public CedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    // Map mit den Mitarbeiternummern
	CMapStringToPtr omPenoMap;

    CCSPtrArray<STFDATA> omData;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// OStfations
public:
    CedaStfData();
	~CedaStfData();
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(STFDATA *prpStf);
	bool InsertInternal(STFDATA *prpStf);
	bool Update(STFDATA *prpStf);
	bool UpdateInternal(STFDATA *prpStf);
	bool Delete(long lpUrno);
	bool DeleteInternal(STFDATA *prpStf);
	bool ReadSpecialData(CCSPtrArray<STFDATA> *popStf,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(STFDATA *prpStf);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	STFDATA  *GetStfByUrno(long lpUrno);
	STFDATA  *GetStfByPeno(CString opPeno);
	// Pr�ft ob MA zu diesem Zeitpunkt im Unternehmen ist
	bool IsStfNowValid(long lpUrno, COleDateTime opDay);
	// liefert Namen in der Form: NAME,VORNAME zur�ck
	CString GetName(long lpUrno, int ipEName = 1);

	// Private methods
private:
    void PrepareStfData(STFDATA *prpStfData);

};

extern CedaStfData ogStfData;

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPSTFDATA__
