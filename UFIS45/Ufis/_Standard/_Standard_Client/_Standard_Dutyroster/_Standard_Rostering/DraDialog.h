#if !defined(AFX_DRADIALOG_H__06BE54E5_B547_11D3_8FCF_00500454BF3F__INCLUDED_)
#define AFX_DRADIALOG_H__06BE54E5_B547_11D3_8FCF_00500454BF3F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DraDialog.h : Header-Datei
//
#include <stdafx.h>
/*#include "cedadradata.h"
#include <cedadrrdata.h>
#include <ccsedit.h>*/


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDraDialog 

class CDraDialog : public CDialog
{
// Konstruktion
public:
	CDraDialog(CWnd* pParent,DRADATA* popDraData,DRRDATA* popDrrData,CMapPtrToPtr* popDraMap);   

// Dialogfelddaten
	//{{AFX_DATA(CDraDialog)
	enum { IDD = IDD_DRA_DLG };
	CComboBox	m_CB_Special;
	CCSEdit		m_TimeTo;
	CCSEdit		m_TimeFrom;
	CCSEdit		m_DateFrom;
	CCSEdit		m_DateTo;
	CComboBox	m_CB_Absence;
	CEdit		m_Remark;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDraDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

// data
	DRADATA* pomDraData;
	DRRDATA* pomDrrData;
	// Map mit allen zugehörigen DRAs
	CMapPtrToPtr* pomDraMap;
	// Maps
	CMapStringToString omOdaMap;

// functions
	void GetTimeData(DRADATA &opTargetDra);
	bool IsInputValid(COleDateTime opTimeFrom,COleDateTime opTimeTo);
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDraDialog)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void SetButtonText();
	void SetStatic();

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DRADIALOG_H__06BE54E5_B547_11D3_8FCF_00500454BF3F__INCLUDED_
