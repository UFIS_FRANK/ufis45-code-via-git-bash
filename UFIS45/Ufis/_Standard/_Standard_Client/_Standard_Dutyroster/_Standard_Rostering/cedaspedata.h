// CedaSpeData.h

#ifndef __CEDAPSPEDATA__
#define __CEDAPSPEDATA__
 
#include <stdafx.h>
#include <basicdata.h>
#include <afxdisp.h>

//---------------------------------------------------------------------------------------------------------

struct SPEDATA 
{
	long Urno;			// Unique record number.
	long Surn;			// Reference to STF.URNO
	char Code[7];		// Qualification Code ...CODE	
	COleDateTime Vpfr;	// G�ltig von	
	COleDateTime Vpto;	// G�ltig bis

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	SPEDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		//Vpfr=-1,Vpto=-1; //<zB.(FIELD_DATE Felder)
		Vpfr.SetStatus(COleDateTime::invalid);
		Vpto = COleDateTime(2130,31,12,23,59,59);//.SetStatus(COleDateTime::invalid);
	}

}; // end SPEDataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaSpeData: public CedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<SPEDATA> omData;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

// OSpeations
public:
    CedaSpeData();
	~CedaSpeData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(SPEDATA *prpSpe);
	bool InsertInternal(SPEDATA *prpSpe);
	bool Update(SPEDATA *prpSpe);
	bool UpdateInternal(SPEDATA *prpSpe);
	bool Delete(long lpUrno);
	bool DeleteInternal(SPEDATA *prpSpe);
	bool ReadSpecialData(CCSPtrArray<SPEDATA> *popSpe,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(SPEDATA *prpSpe);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	SPEDATA  *GetSpeByUrno(long lpUrno);
	bool IsValidSpe(SPEDATA *popSpe);
	
	//uhi 26.4.01
	void GetSpeBySurn(long lpSurn,CCSPtrArray<SPEDATA> *popSpeData);

	// Private methods
private:
    void PrepareSpeData(SPEDATA *prpSpeData);

};

//---------------------------------------------------------------------------------------------------------

extern CedaSpeData ogSpeData;

#endif //__CEDAPSPEDATA__
