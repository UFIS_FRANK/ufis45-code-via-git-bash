// DutyRosterPropertySheet.h : header file
//
#ifndef _DUTYROSTERPROPERTYSHEET_H_
#define _DUTYROSTERPROPERTYSHEET_H_


#include <BasePropertySheet.h>
#include <PSDutyRoster1ViewPage.h>
#include <PSDutyRoster2ViewPage.h>
#include <PSDutyRoster3ViewPage.h>
#include <PSDutyRoster4ViewPage.h>
#include <PSDutyRoster5ViewPage.h>

#include <PSShiftRoster3ViewPage.h>

#include <Accounts.h>

/////////////////////////////////////////////////////////////////////////////
// DutyRosterPropertySheet

class DutyRosterPropertySheet : public BasePropertySheet
{
// Construction
public:
	DutyRosterPropertySheet(CString opCalledFrom, CWnd* pParentWnd = NULL,	CViewer *popViewer = NULL, CAccounts *popAccounts = NULL, UINT iSelectPage = 0);

// Attributes
public:


	PSDutyRoster1ViewPage omDutyRoster1View;
	PSDutyRoster2ViewPage omDutyRoster2View;
	PSDutyRoster3ViewPage omDutyRoster3View;
	PSDutyRoster4ViewPage omDutyRoster4View;
	PSDutyRoster5ViewPage omDutyRoster5View;
    PSShiftRoster3ViewPage omDutyRoster6View;

	CString omCalledFrom;
	CStringArray omEmployeeList;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int QueryForDiscardChanges();

protected:
	// Zeiger auf das Kontoführungsobjekt aus DutyRoster_View
	CAccounts *pomAccounts;
};

/////////////////////////////////////////////////////////////////////////////

#endif // _DUTYROSTERPROPERTYSHEET_H_
