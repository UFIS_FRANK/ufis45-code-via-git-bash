#ifndef AFX_PSShiftRoster3ViewPage_H__2510E6F2_CD24_11D1_BFCC_004095434A85__INCLUDED_
#define AFX_PSShiftRoster3ViewPage_H__2510E6F2_CD24_11D1_BFCC_004095434A85__INCLUDED_

// PSShiftRoster3ViewPage.h : Header-Datei
//
#include <Ansicht.h>
#include <RosterViewPage.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld PSShiftRoster3ViewPage 

class PSShiftRoster3ViewPage : public RosterViewPage
{
	DECLARE_DYNCREATE(PSShiftRoster3ViewPage)

// Konstruktion
public:
	
	PSShiftRoster3ViewPage();
	~PSShiftRoster3ViewPage();

// Dialogfelddaten
	//{{AFX_DATA(PSShiftRoster3ViewPage)
	enum { IDD = IDD_PSSHIFTROSTER3 };
	CListBox	m_LB_Qualifications;
	//}}AFX_DATA

	BOOL GetData();
	BOOL GetData(CStringArray &opValues);
	void SetData();
	void SetData(CStringArray &opValues);


// Überschreibungen
	// Der Klassen-Assistent generiert virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(PSShiftRoster3ViewPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(PSShiftRoster3ViewPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnAll();
	afx_msg void OnNothing();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	CString omCalledFrom;
	void SetCalledFrom(CString opCalledFrom);

private:
	CString omTitleStrg;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_PSSHIFTROSTER3VIEWPAGE_H__2510E6F2_CD24_11D1_BFCC_004095434A85__INCLUDED_
