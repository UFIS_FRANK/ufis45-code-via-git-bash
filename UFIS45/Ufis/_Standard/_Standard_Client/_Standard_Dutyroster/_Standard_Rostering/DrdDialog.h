#if !defined(AFX_DRDDIALOG_H__FF93A4E5_B38F_11D3_8FCD_00500454BF3F__INCLUDED_)
#define AFX_DRDDIALOG_H__FF93A4E5_B38F_11D3_8FCD_00500454BF3F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DrdDialog.h : Header-Datei
//

#include <cedadrddata.h>
#include <ccsedit.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDrdDialog 

class CDrdDialog : public CDialog
{
// Konstruktion
public:
	CDrdDialog(CWnd* pParent, DRDDATA* popDrdData,DRRDATA* popDrrData,CMapPtrToPtr* popDrdMap, bool bpCheckMainFuncOnly);   // Standardkonstruktor
	~CDrdDialog();
// Dialogfelddaten
	//{{AFX_DATA(CDrdDialog)
	enum { IDD = IDD_DRD_DLG };
	CListBox	m_LB_Perc;
	CListBox	m_LB_PercAdditional;
	CComboBox	m_CB_Org;
	CComboBox	m_CB_Function;
	CCSEdit		m_TimeTo;
	CCSEdit		m_TimeFrom;
	CCSEdit		m_DateFrom;
	CCSEdit		m_DateTo;
	//}}AFX_DATA


// �berschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktions�berschreibungen
	//{{AFX_VIRTUAL(CDrdDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung
	//}}AFX_VIRTUAL

// Implementierung
protected:

// data
	DRDDATA* pomDrdData;
	DRRDATA* pomDrrData;
	// Map mit allen zugeh�rigen DRDs
	CMapPtrToPtr* pomDrdMap;
	// Maps
	CMapStringToString omPfcMap,omOrgMap,omWgpMap,omPerMap;
// functions
	// Eingabe auf G�ltigkeit pr�fen
	bool IsInputValid(COleDateTime opTimeTo,COleDateTime opTimeFrom);
	// Datums- und Zeitwerte auslesen und speichern
	void GetTimeData(DRDDATA &opTargetDrd);
	// Info �ber Fkt., Arbeitsgr., Organisationseinheit und Qualifikationen auslesen und speichern
	void GetJobData(DRDDATA &opTargetDrd);

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDrdDialog)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void FillComboBoxes();
	void FillListBoxes();
	void SetButtonText();
	void SetStatic();
	bool bmCheckMainFuncOnly;	// true - Schichtzuordnung nur f�r Stammfunktionen pr�fen, sonst f�r alle
	CString omSprPermits;
	CMapStringToPtr omSprPermitsMap;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ f�gt unmittelbar vor der vorhergehenden Zeile zus�tzliche Deklarationen ein.

#endif // AFX_DRDDIALOG_H__FF93A4E5_B38F_11D3_8FCD_00500454BF3F__INCLUDED_
