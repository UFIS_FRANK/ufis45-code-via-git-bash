#ifndef AFX_SHIFTCODENUMBERDLG_H__15951C01_CF8D_11D1_BFCF_004095434A85__INCLUDED_
#define AFX_SHIFTCODENUMBERDLG_H__15951C01_CF8D_11D1_BFCF_004095434A85__INCLUDED_

// ShiftCodeNumberDlg.h : Header-Datei
//
#include <stdafx.h>
#include <resource.h>
#include <basicdata.h>
#include <ShiftRoster_View.h>
//#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld ShiftCodeNumberDlg 

class ShiftCodeNumberDlg : public CDialog
{
// Konstruktion
public:
	ShiftCodeNumberDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	~ShiftCodeNumberDlg(void);

// Dialogfelddaten
	//{{AFX_DATA(ShiftCodeNumberDlg)
	enum { IDD = IDD_SHIFT_CODENUMBER_DLG };
	CCSEdit		m_e_WeekFreeSunX;
	CCSEdit		m_e_WeekFreeSun13;
	CCSEdit		m_e_WeekFreeSun4;
	CCSEdit		m_e_WeekFreeX;
	CCSEdit		m_e_WeekFree13;
	CCSEdit		m_e_WeekFree4;
	CCSEdit		m_e_WeekX;
	CCSEdit		m_e_Week13;
	CCSEdit		m_e_Week4;
	CSliderCtrl	m_Slider;
	CSpinButtonCtrl	m_SpinButton;
	CStatic		m_S_Work;
	CStatic		m_S_Free;
	CStatic		m_S_YearDebit;
	CStatic		m_S_YearIs;
	CStatic		m_S_PerWeek;
	CStatic		m_S_PerDay;
	CStatic		m_S_HouersGSP;
	CStatic		m_S_HouersAll;
	CStatic		m_S_MaSchedule;
	CStatic		m_S_MaAssign;
	CStatic		m_S_MaScheduleAll;
	CStatic		m_S_MaAssignAll;
	CStatic		m_G_SPL;
	CStatic		m_G_GPL;
	CCSEdit		m_E_Work;
	CCSEdit		m_E_Free;
	CCSEdit		m_E_YearDebit;
	CCSEdit		m_E_YearIs;
	CCSEdit		m_E_PerWeek;
	CCSEdit		m_E_PerDay;
	CCSEdit		m_E_HouersGSP;
	CCSEdit		m_E_HouersAll;
	CCSEdit		m_E_MaSchedule;
	CCSEdit		m_E_MaAssign;
	CCSEdit		m_E_MaScheduleAll;
	CCSEdit		m_E_MaAssignAll;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(ShiftCodeNumberDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	// Anzahl der Startwochen
	int imWeeks;
	// Daten der einzelnen Wochen
	CCSPtrArray<GSPTABLEDATA> omGSPTableData;
	// Berechnung der Wochenwerte
	float GenerateWeekValue(int ipStartWeek, int ipWeekCount);
	// Berechnung der Anzahl der freien Tage
	int GenerateWeekFreeValue(int ipStartWeek, int ipWeekCount);
	// Berechnung der Anzahl der freien Tage am Sonntag
	int GenerateWeekFreeSunValue(int ipStartWeek, int ipWeekCount);
	
public:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(ShiftCodeNumberDlg)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	afx_msg void OnDeltaposSpinweekx(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeEditx();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void SetValues(CODENUMBERDATA rpCodeNumberData);
	// Setzt die Anzahl der Startwochen
	void SetWeeks(int ipWeeks);
	// Setzen des Pointers für die Daten
	void  SetDataArray(CCSPtrArray<GSPTABLEDATA> opGSPTableData);
	// Alle Werte neu berechnen
	void ReNewAll();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_SHIFTCODENUMBERDLG_H__15951C01_CF8D_11D1_BFCF_004095434A85__INCLUDED_
