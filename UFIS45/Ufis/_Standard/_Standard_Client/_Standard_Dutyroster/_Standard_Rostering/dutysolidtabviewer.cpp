// DutySolidTabViewer.cpp 
//
#include <stdafx.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
//static void DutySolidTabViewerCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//****************************************************************************
// DutySolidTabViewer
//****************************************************************************

DutySolidTabViewer::DutySolidTabViewer(CWnd* pParent /*=NULL*/)
{
	
	pomParent = (DutyRoster_View*) pParent;
	//DdxRegister(this, DUTYSOLIDTABVIEWER_HSCOLL, "DUTYSOLIDTABVIEWER", "DUTYSOLIDTABVIEWER_HSCOLL", DutySolidTabViewerCf);
    //DdxRegister(this, DUTYSOLIDTABVIEWER_VSCOLL, "DUTYSOLIDTABVIEWER", "DUTYSOLIDTABVIEWER_VSCOLL", DutySolidTabViewerCf);

    pomDynTab   = NULL;
	prmTableData = 0;
	prmViewInfo = NULL;
	pomStfData  = NULL;
	pomGroups   = NULL;
	imHighlightedRow = -1;

}

//****************************************************************************
// 
//****************************************************************************

DutySolidTabViewer::~DutySolidTabViewer()
{
	//ogDdx.UnRegister(this, NOTUSED);
}

//****************************************************************************
// 
//****************************************************************************

void DutySolidTabViewer::Attach(CCSDynTable *popTable)
{
    pomDynTab = popTable;
	prmTableData = pomDynTab->GetTableData();
}

//****************************************************************************
// 
//****************************************************************************

void DutySolidTabViewer::ChangeView(bool bpSetHNull /*=true*/,bool bpSetVNull /*=true*/)
{
	CCS_TRY;
	RemoveHighlight();
	// Auweiowei
	pomStfData  = &pomParent->omStfData;
	prmViewInfo = &pomParent->rmViewInfo;
	pomGroups   = &pomParent->omGroups;

	omPTypes.RemoveAll();
	ExtractItemList(prmViewInfo->oPType, &omPTypes, ',');
	int ilNrOfTypes = omPTypes.GetSize();

	int ilStfSize = 0;
	if(prmViewInfo->iGroup  == 2)
		ilStfSize = pomGroups->GetSize(); //Gruppen-Darstellung
	else
		ilStfSize = pomStfData->GetSize();//Mitarbeiter-Darstellung

	ilStfSize = (ilStfSize*ilNrOfTypes)-1;
	if(ilStfSize < 0) ilStfSize = 0;

	int ilColumnStartPos = 0;
	int ilRowStartPos = 0;
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);

	if(!bpSetHNull)
	{
/*		ilColumnStartPos = rlTableInfo.iColumnOffset;
		//Berichtigung des Offset
		int ilColumnDiff = (ilDays+1) - ilColumnStartPos;
		if(ilColumnDiff < rlTableInfo.iColumns)
		{
			ilColumnStartPos = ilColumnStartPos - (rlTableInfo.iColumns - ilColumnDiff);
		}
		if(ilColumnStartPos < 0)
			ilColumnStartPos = 0;*/
	}
	if(!bpSetVNull)
	{
		ilRowStartPos = rlTableInfo.iRowOffset;
		//Berichtigung des Offset
		int ilRowDiff = (ilStfSize + 1) - ilRowStartPos;
		if(ilRowDiff < rlTableInfo.iRows)
		{
			ilRowStartPos = ilRowStartPos - (rlTableInfo.iRows - ilRowDiff);
		}
		if(ilRowStartPos < 0)
			ilRowStartPos = 0;
	}

	pomDynTab->SetHScrollData(0,0,ilColumnStartPos);
	pomDynTab->SetVScrollData(0,ilStfSize,ilRowStartPos);
	
	InitTableArrays(ilColumnStartPos);
	MakeTableData(ilColumnStartPos,ilRowStartPos);
	pomDynTab->InvalidateRect(NULL);
	CCS_CATCH_ALL;
}

void DutySolidTabViewer::HorzTableScroll(MODIFYTAB *prpModiTab)
{
	RemoveHighlight();
	//TRACE("HOR R%03d - C%03d\n",prpModiTab->iNewVertPos,prpModiTab->iNewHorzPos);
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	if(prpModiTab->iID == rlTableInfo.iID)
	{
		MakeTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
		pomDynTab->ChangedTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
	}
	else
	{
		MakeTableData(prpModiTab->iNewHorzPos,rlTableInfo.iRowOffset);
		pomDynTab->ChangedTableData(prpModiTab->iNewHorzPos,rlTableInfo.iRowOffset);
	}
}

//****************************************************************************
// 
//****************************************************************************

void DutySolidTabViewer::VertTableScroll(MODIFYTAB *prpModiTab)
{
	RemoveHighlight();
	//TRACE("VER R%03d - C%03d\n",prpModiTab->iNewVertPos,prpModiTab->iNewHorzPos);
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	if(prpModiTab->iID == rlTableInfo.iID)
	{
		MakeTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
		pomDynTab->ChangedTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
	}
	else
	{
		MakeTableData(rlTableInfo.iColumnOffset,prpModiTab->iNewVertPos);
		pomDynTab->ChangedTableData(rlTableInfo.iColumnOffset,prpModiTab->iNewVertPos);
	}
}

//****************************************************************************
// 
//****************************************************************************

void DutySolidTabViewer::TableSizing(/*UINT nSide, */LPRECT lpRect)
{
	pomDynTab->SizeTable(/*nSide, */lpRect);
}

//****************************************************************************
// 
//****************************************************************************

void DutySolidTabViewer::MoveTable(MODIFYTAB *prpModiTab)
{
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	if(prpModiTab->iID == rlTableInfo.iID)
	{
		if(prmTableData->oSolid.GetSize() > 0 && prmTableData->oHeader.GetSize() > 0)
		{
			MakeTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
		}
	}
	else
	{
		int ilX = 0;
		int ilY = 0;
		//ilX = prpModiTab->oNewRect.left   - prpModiTab->oOldRect.left;
		//ilX = prpModiTab->oNewRect.right  - prpModiTab->oOldRect.right;
		//ilY = prpModiTab->oNewRect.top    - prpModiTab->oOldRect.top;
		//ilY = prpModiTab->oNewRect.bottom - prpModiTab->oOldRect.bottom;

		pomDynTab->MoveTable(ilX,ilY);
	}
}

//****************************************************************************
// 
//****************************************************************************

void DutySolidTabViewer::InlineUpdate(INLINEDATA *prpInlineUpdate)
{
//	prpInlineUpdate->;
}

//****************************************************************************
// 
//****************************************************************************

void DutySolidTabViewer::InitTableArrays(int ipColumnOffset /*=0*/)
{
	CString olText; 
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	pomDynTab->ResetContent();
	
	prmTableData->oSolidHeader.oText		= "";
	prmTableData->oSolidHeader.oTextColor	= BPN_NOCOLOR;
	prmTableData->oSolidHeader.oBkColorNr	= BPN_NOCOLOR;

	for(int ilColumn=0;ilColumn<rlTableInfo.iColumns+1;ilColumn++)
	{
		olText.Empty();

		if(pomStfData->GetSize() > 0)
		{
			switch(ilColumn)
			{
				case 0:
				{
					if(prmViewInfo->iGroup  == 2)
						olText = CString(" ") + LoadStg(IDS_STRING1692); // Group/Function/Prio.
					else if(prmViewInfo->iGroup  == 3)
						olText = CString(" ") + LoadStg(PR_DSR_CARPOOL); // CarPool.
					else
					{
						// "Total: %d*INTXT*"
						olText.Format(LoadStg(IDS_STRING197), pomParent->rmViewInfo.oStfUrnos.GetSize());
					}
					prmTableData->oSolidHeader.oText = olText;
					break;
				}
				case 1:
				{
					FIELDDATA *polFieldData = new FIELDDATA;
					polFieldData->oTextColor = BPN_NOCOLOR;
					polFieldData->oBkColorNr = BPN_NOCOLOR;

					if(prmViewInfo->iEName == 1) //Code: "Initials*INTXT*"
					{
						olText = CString(" ") + LoadStg(SHIFT_SHEET_C_CODE);
					}
					else if(prmViewInfo->iEName == 3) //Shortname: "Nickname*INTXT*"
					{
						olText = CString(" ") + LoadStg(IDS_STRING1807);
					}
					else if(prmViewInfo->iEName == 2 || prmViewInfo->iEName == 4) // "Employees*INTXT*"
					{
						olText = CString(" ") + LoadStg(SHIFT_S_STAFF);
					}
					else
					{
						olText = CString(""); //No Text >> No Group or CarPool.
					}
					polFieldData->oText = olText;
					prmTableData->oHeader.Add((void*)polFieldData);
					break;
				}
				default:
				{
					olText =  CString("");
					break;
				}
			}
		}
	}
}

//****************************************************************************
// 
//****************************************************************************

void DutySolidTabViewer::MakeTableData(int ipColumnOffset,int ipRowOffset)
{
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	//****************************************************************
	int ilActualRows = prmTableData->oSolid.GetSize();
	//** Add rows by SIZING
	while(rlTableInfo.iRows > ilActualRows)
	{
		AddRow(ilActualRows,rlTableInfo.iColumns,ipColumnOffset,ipRowOffset+ilActualRows);
		ilActualRows = prmTableData->oSolid.GetSize();
	}
	//** Delete rows by SIZING
	while(rlTableInfo.iRows < ilActualRows)
	{
		DelRow(ilActualRows-1);
		ilActualRows = prmTableData->oSolid.GetSize();
	}

	int ilActualColumns = prmTableData->oHeader.GetSize();
	//*** Add columns by SIZING
	while(ilActualRows>0 && rlTableInfo.iColumns > ilActualColumns)
	{
		AddColumn(ilActualColumns,ipColumnOffset+ilActualColumns,ipRowOffset);
		ilActualColumns = prmTableData->oHeader.GetSize();
	}
	//** Delete columns by SIZING
	while(ilActualRows>0 && rlTableInfo.iColumns < ilActualColumns)
	{
		DelColumn(ilActualColumns-1);
		ilActualColumns = prmTableData->oHeader.GetSize();
	}
	//****************************************************************
	int ilColumnDiv = ipColumnOffset-rlTableInfo.iColumnOffset;
	if(ilColumnDiv != 0)
	{
		//** SCROLL right
		if(ilColumnDiv > 0)
		{
			if(ilColumnDiv > prmTableData->oHeader.GetSize())
				ilColumnDiv = prmTableData->oHeader.GetSize();
			for(int i=0; i < ilColumnDiv;i++)
			{
				DelColumn(0);
				AddColumn(prmTableData->oHeader.GetSize(),ipColumnOffset+rlTableInfo.iColumns-ilColumnDiv+i,ipRowOffset);
			}
		}
		//** SCROLL left
		if(ilColumnDiv < 0)
		{
			ilColumnDiv = -(ilColumnDiv);
			if(ilColumnDiv > prmTableData->oHeader.GetSize())
				ilColumnDiv = prmTableData->oHeader.GetSize();
			for(int i=0; i < ilColumnDiv; i++)

			{
				DelColumn(prmTableData->oHeader.GetSize()-1);
				AddColumn(0,ipColumnOffset+ilColumnDiv-i-1,ipRowOffset);
			}
		}
	}
	int ilRowDiv = ipRowOffset-rlTableInfo.iRowOffset;
	if(ilRowDiv != 0)
	{
		//** SCROLL up
		if(ilRowDiv > 0)
		{
			if(ilRowDiv > prmTableData->oSolid.GetSize())
				ilRowDiv = prmTableData->oSolid.GetSize();
			for(int i=0; i < ilRowDiv; i++)
			{
				DelRow(0);
				AddRow(prmTableData->oSolid.GetSize(),rlTableInfo.iColumns,ipColumnOffset,ipRowOffset+rlTableInfo.iRows-ilRowDiv+i);
			}
		}
		//** SCROLL down
		if(ilRowDiv < 0)
		{
			ilRowDiv = -(ilRowDiv);
			if(ilRowDiv > prmTableData->oSolid.GetSize())
				ilRowDiv = prmTableData->oSolid.GetSize();
			for(int i=0; i < ilRowDiv; i++)
			{
				DelRow(prmTableData->oSolid.GetSize()-1);
				AddRow(0,rlTableInfo.iColumns,ipColumnOffset,ipRowOffset+ilRowDiv-i-1);
			}
		}
	}
}

//****************************************************************************
// 
//****************************************************************************

void DutySolidTabViewer::AddRow(int ipRow, int ipColumns, int ipColumnNr, int ipRowNr)
{
	CString olText;
	CString olTmp;
	//** Table
	CPtrArray *polTableValue = new CPtrArray;

	CString olPType = "";
	int ilStfNr = 0;
	int ilPTypeNr = 0;
	bool blGetRaT = GetElementAndPType(ipRowNr, ilStfNr, olPType, ilPTypeNr);

	for (int ilColumn=0; ilColumn < ipColumns+1; ilColumn++)
	{
		olText = "";
		FIELDDATA *polFieldData = new FIELDDATA;

		polFieldData->oTextColor = BPN_NOCOLOR;
		polFieldData->oBkColorNr = BPN_NOCOLOR;
		polFieldData->rIPEAttrib.TextMaxLenght = 8;
		polFieldData->rIPEAttrib.TextMinLenght = 1;
		polFieldData->bInplaceEdit = false;
		polFieldData->iRButtonDownStatus = RBDS_ALL;
		polFieldData->iRButtonDownStatus = RBDS_ALL;

		if(prmViewInfo->iGroup == 2 && prmViewInfo->bSortDayly == true)
			polFieldData->bDrop = true;

		//textfarbe Ermitteln
		if     (olPType == "1") polFieldData->oTextColor = GRAY;
		else if(olPType == "2") polFieldData->oTextColor = BLACK;
		else if(olPType == "3") polFieldData->oTextColor = BLUE;
		else if(olPType == "4") polFieldData->oTextColor = PURPLE;
		else if(olPType == "5") polFieldData->oTextColor = ORANGE;
		else if(olPType == "L") polFieldData->oTextColor = FUCHSIA;
		else if(olPType == "U") polFieldData->oTextColor = RED;
		else if(olPType == "W") polFieldData->oTextColor = GREEN;

		if(blGetRaT && omPTypes.GetSize() > 1 && ilPTypeNr+1 == omPTypes.GetSize())
		{
			polFieldData->iBLineWidth = NORMAL_LINE;
		}

		//Text f�r die einzelnen Spalten ermitteln
		switch(ilColumn)
		{
			case 0:
			{
				if(prmViewInfo->iGroup == 2)
				{
					if(ilStfNr < pomGroups->GetSize() && blGetRaT)
					{
						olText = " ";
						olText += (*pomGroups)[ilStfNr].oWgpCode;

						if(!(*pomGroups)[ilStfNr].oWgpCode.IsEmpty() && !(*pomGroups)[ilStfNr].oPfcCode.IsEmpty())
						{
							// wenn Gruppe und Funktion nicht leer sind, ist ein Separator notwendig
							olText += "/";
						}
						olText += (*pomGroups)[ilStfNr].oPfcCode;

						polFieldData->oBkColorNr = (*pomGroups)[ilStfNr].iColor1Nr;
						if(ilStfNr+1 < pomGroups->GetSize())
						{
							if((*pomGroups)[ilStfNr].iColor1Nr != (*pomGroups)[ilStfNr+1].iColor1Nr)
							{
								polFieldData->iBLineWidth = NORMAL_LINE;
							}
						}
					}
				}
				else
				{
					if(ilStfNr < pomStfData->GetSize() && blGetRaT)
					{
						long llStfu = (*pomStfData)[ilStfNr].Urno;
						// Funktionscode und Vertragscode und Wochenarbeitszeit
						CString olActualCot = "";
						CString olFunctionCode = "";
						CString olHoursPerWeek = "";

						CCSPtrArray<SPFDATA> olSpfData;
						int ilSpfCnt = ogSpfData.GetSpfArrayBySurnWithTime(olSpfData, llStfu, prmViewInfo->oSelectDate, prmViewInfo->bCheckMainFuncOnly);
						if(ilSpfCnt > 0)
						{
							olFunctionCode = olSpfData[0].Fctc;
							if(ilSpfCnt > 1)
							{
								// nur wenn prmViewInfo->bCheckMainFuncOnly == false && der MA hat mehr als eine Funktion
								polFieldData->oToolTipText = ogSpfData.FormatFuncs(olSpfData,2);
							}
						}

						// Vertragscode & Wochenarbeitszeit
						olActualCot = ogScoData.GetCotAndCWEHBySurnWithTime(llStfu, prmViewInfo->oSelectDate, &olHoursPerWeek);

						if (strlen(olHoursPerWeek) == 0)
						{
							COTDATA *prlCotData = ogCotData.GetCotByCtrc(olActualCot);
							if (prlCotData != NULL)
							{
								olHoursPerWeek = prlCotData->Whpw;
							}
						}

						if (strlen(olHoursPerWeek) > 2)
						{
							if (strstr(olHoursPerWeek.GetBuffer(0),".") != NULL)
							{
								olHoursPerWeek.Replace(".",":");
							}
							else
							{
								olHoursPerWeek.Insert(2,":");
							}
						}
						olText = olFunctionCode + " / " + olActualCot + " / " + olHoursPerWeek;
					}
				}
				polFieldData->oText = olText;
				polFieldData->iRButtonDownStatus = RBDS_NOTHING;
				prmTableData->oSolid.InsertAt(ipRow,(void*)polFieldData);
				break;
			}
			case 1:
			{
				if(prmViewInfo->iGroup == 2)
				{
					if(ilStfNr < pomGroups->GetSize() && blGetRaT)
					{
						if ((*pomGroups)[ilStfNr].lStfUrno != 0)
						{
							if (prmViewInfo->iEName == 1) //Code/Initials
							{
								olText.Format(" %s",(*pomGroups)[ilStfNr].oStfPerc);
							}
							else if (prmViewInfo->iEName == 2) //Name
							{
								CString olVName,olNName;
								olVName = (*pomGroups)[ilStfNr].oStfFinm;
								olNName = (*pomGroups)[ilStfNr].oStfLanm;
								if(olVName.IsEmpty() == FALSE || olNName.IsEmpty() == FALSE)
								{
									olText.Format(" %s.%s",olVName.Left(1),olNName);
								}
							}
							else if (prmViewInfo->iEName == 3) //Shortname/Nickname
							{
								olText.Format(" %s",(*pomGroups)[ilStfNr].oStfShnm);
							}
							else if (prmViewInfo->iEName == 4) //free configurated
							{
								olText = " " + pogNameConfigurationDlg->GetNameString(
									(*pomGroups)[ilStfNr].oStfFinm.GetBuffer(0),
									(*pomGroups)[ilStfNr].oStfLanm.GetBuffer(0),
									(*pomGroups)[ilStfNr].oStfShnm.GetBuffer(0),
									(*pomGroups)[ilStfNr].oStfPerc.GetBuffer(0),
									(*pomGroups)[ilStfNr].oStfPeno.GetBuffer(0));
							}
						}

						polFieldData->oBkColorNr = (*pomGroups)[ilStfNr].iColor1Nr;
						if(ilStfNr+1 < pomGroups->GetSize())
						{
							if((*pomGroups)[ilStfNr].iColor1Nr != (*pomGroups)[ilStfNr+1].iColor1Nr)
							{
								polFieldData->iBLineWidth = NORMAL_LINE;
							}
						}

						// Italic for manual inserted employees
						CString olUrno;
						CObject* polDummy;
						olUrno.Format("%ld",(*pomGroups)[ilStfNr].lStfUrno);
						if (pomParent->omDispExtraUrnoMap.Lookup(olUrno,polDummy) != NULL)
						{
							polFieldData->iFontStyle = FONT_STYLE_ITALIC;
						}
						else
						{
							polFieldData->iFontStyle = FONT_STYLE_REGULAR;
						}
					}
				}
				else
				{
					if(ilStfNr < pomStfData->GetSize() && blGetRaT)
					{
						olText = CBasicData::GetFormatedEmployeeName(prmViewInfo->iEName, &(*pomStfData)[ilStfNr], " ", "");

						// Italic for manual inserted employees
						CString olUrno;
						CObject* polDummy;
						olUrno.Format("%ld",(*pomStfData)[ilStfNr].Urno);
						if (pomParent->omDispExtraUrnoMap.Lookup(olUrno,polDummy) != NULL)
						{
							polFieldData->iFontStyle = FONT_STYLE_ITALIC;
						}
						else
						{
							polFieldData->iFontStyle = FONT_STYLE_REGULAR;
						}
					}
				}

				polFieldData->oText = olText;
				polTableValue->Add((void*)polFieldData);
				break;
			}
			default:
			{
				polFieldData->oText = "";
				polTableValue->Add((void*)polFieldData);
				break;
			}
		}
		//polTableValue->Add((void*)polFieldData);
	}
	prmTableData->oTable.InsertAt(ipRow,(void*)polTableValue);


	// Solid
	/*olText.Empty();
	FIELDDATA *polFieldData = new FIELDDATA;
	olText.Format("%d",ipRowNr);//test
	polFieldData->oText			= olText;
	polFieldData->oTextColor	= BPN_NOCOLOR;
	polFieldData->oBkColorNr	= BPN_NOCOLOR;

	prmTableData->oSolid.InsertAt(ipRow,(void*)polFieldData);*/
}

//****************************************************************************
// 
//****************************************************************************

void DutySolidTabViewer::DelRow(int ipRow)
{
	//** Table Text Color

	CPtrArray *polTableValue = (CPtrArray*)prmTableData->oTable[ipRow];
	for(int i = polTableValue->GetSize()-1;i>=0;i--)
	{
		FIELDDATA *polFieldData = (FIELDDATA*)(*polTableValue)[i];
		delete polFieldData;
	}
	polTableValue->RemoveAll();
	delete polTableValue;
	prmTableData->oTable.RemoveAt(ipRow);

	//** Solid Text Color
	FIELDDATA *polFieldData = (FIELDDATA*)prmTableData->oSolid[ipRow];
	delete polFieldData;
	prmTableData->oSolid.RemoveAt(ipRow);

}

//****************************************************************************
// 
//****************************************************************************

void DutySolidTabViewer::AddColumn(int ipColumn, int ipColumnNr, int ipRowNr)
{
	//TRACE(" COL R%03d - C%03d\n",ipRowNr,ipColumnNr);
	CString olText; 

	//** Table
	int ilActualRows = prmTableData->oTable.GetSize();
	for(int ilRow = 0; ilRow<ilActualRows; ilRow++)
	{
		CPtrArray *polTableValue = (CPtrArray*)prmTableData->oTable[ilRow];

		FIELDDATA *polFieldData = new FIELDDATA;

		//olText.Format("%d-%d",ipRowNr+ilRow,ipColumnNr);//test
		polFieldData->oText			= olText;
		polFieldData->oTextColor	= BPN_NOCOLOR;
		polFieldData->oBkColorNr	= BPN_NOCOLOR;
		polFieldData->rIPEAttrib.TextMaxLenght = 8;
		polFieldData->rIPEAttrib.TextMinLenght = 1;
		polFieldData->bInplaceEdit = false;
		polFieldData->iRButtonDownStatus = RBDS_ALL;

/*		if     (olPType == "1") polFieldData->oTextColor = GRAY;
		else if(olPType == "2") polFieldData->oTextColor = BLACK;
		else if(olPType == "3") polFieldData->oTextColor = BLUE;
		else if(olPType == "4") polFieldData->oTextColor = PURPLE;
		else if(olPType == "5") polFieldData->oTextColor = ORANGE;
		else if(olPType == "L") polFieldData->oTextColor = FUCHSIA;
		else if(olPType == "U") polFieldData->oTextColor = RED;
		else if(olPType == "W") polFieldData->oTextColor = GREEN;*/

		polTableValue->InsertAt(ipColumn,(void*)polFieldData);
	}

	//** Header
	FIELDDATA *polFieldData = new FIELDDATA;

	olText.Format("%d",ipColumnNr);//test
	polFieldData->oText			= olText;
	polFieldData->oTextColor	= BPN_NOCOLOR;
	polFieldData->oBkColorNr	= BPN_NOCOLOR;

	prmTableData->oHeader.InsertAt(ipColumn,(void*)polFieldData);
}

//****************************************************************************
// 
//****************************************************************************

void DutySolidTabViewer::DelColumn(int ipColumn)
{
	//** Table Text Color
	int ilActualRows = prmTableData->oTable.GetSize();
	for(int ilRow = 0; ilRow<ilActualRows; ilRow++)
	{
		CPtrArray *polTableValue = (CPtrArray*)prmTableData->oTable[ilRow];
		FIELDDATA *polFieldData = (FIELDDATA*)(*polTableValue)[ipColumn];
		delete polFieldData;
		polTableValue->RemoveAt(ipColumn);
	}

	//** Header Text Color
	FIELDDATA *polFieldData = (FIELDDATA*)prmTableData->oHeader[ipColumn];
	delete polFieldData;
	prmTableData->oHeader.RemoveAt(ipColumn);
}

//****************************************************************************
// 
//****************************************************************************

bool DutySolidTabViewer::GetElementAndPType(int ipRowNr, int &ipRow, CString &opPType, int &ipPTypNr)
{
	int ilPTypeSize = omPTypes.GetSize();

	if(ilPTypeSize < 1) return false;

	double dlBruch = (double)ipRowNr/(double)ilPTypeSize;
	double dlDivisor = 1.0/(double)ilPTypeSize - 0.0000000000001;
	double dlRow = floor(dlBruch);
	ipRow = (int)dlRow;

	ipPTypNr = (int)(floor(((dlBruch-dlRow)/dlDivisor)));

	if(ipPTypNr>ilPTypeSize) return false;
	opPType = omPTypes[ipPTypNr];
	return true;	
}

//****************************************************************************
// Scroll vertikal, damit angegeben URNO sichtbar ist
//****************************************************************************

bool DutySolidTabViewer::ScrollToVisible(long lpUrno)
{
	RemoveHighlight();
	int ilRow;
	bool blReturn;
	//MODIFYTAB *prlModifyTab= new MODIFYTAB;

	// Row der �bergebenen URNO finden
	ilRow = GetRowForUrno(lpUrno);

	// Scrollen, falls die Urno gefunden wurde
	if (ilRow != -1)
	{
		// DynTable zum Scrollen veranlassen.
		pomDynTab->ScrollToPosition(ilRow);
		blReturn = true;
	}
	else
	{
		blReturn = false;
	}
	return (blReturn);
}



//****************************************************************************
// Ermitteln der Row f�r eine URNO. -1 falls nicht gefunden.
//****************************************************************************

int	DutySolidTabViewer::GetRowForUrno(long lpUrno)
{
CCS_TRY;

	int ilRowPosition = -1;
	int ilCount;

	// Abh�ngig von der Darstellung m�ssen Datenstrukturen durchsucht werden.
	switch (prmViewInfo->iGroup)
	{
	case 1:
			for (ilCount=0;ilCount<pomStfData->GetSize() && ilRowPosition == -1;ilCount++)
			{
				if ( (*pomStfData)[ilCount].Urno == lpUrno)
					ilRowPosition = ilCount;
			}
		break;
	case 2:
			/*for (int i=0;i<pomStfData->GetSize;i++;)
			{
				// Urno wurde gefunden
				if ((pomStfData[i]->Urno) = lpUrno)
					ilRowPosition =  i;
			}*/
		break;
	case 3:
		/*for (int i=0;i<pomStfData->GetSize;i++;)
			{
				// Urno wurde gefunden
				if ((pomStfData[i]->Urno) = lpUrno)
					ilRowPosition =  i;
			}*/
		break;
	default:
		break;
	}

	/*
	pomGroups->GetSize(); //Gruppen-Darstellung
	pomStfData->GetSize();//Mitarbeiter-Darstellung


	CCSPtrArray<STFDATA> *pomStfData;
	CCSPtrArray<GROUPSTRUCT> *pomGroups;*/

	return (ilRowPosition);

CCS_CATCH_ALL;
return -1;
}

/*******************************************************************************
- MA soll farbig marktiert werden, wenn die Maus �ber die X-beliebige Zeilen dieses MAs l�uft
ipRow == -1 l�scht die Markierung
*******************************************************************************/
void DutySolidTabViewer::HighlightRow(int ipRow, bool bpHighlightOn)
{
	if(prmTableData == 0) return;

	if(bpHighlightOn)
	{
		if(imHighlightedRow != -1)
		{
			if(ipRow == imHighlightedRow)
				return;		// kein Job
			
			// highlighted ausmachen
			RemoveHighlight();
		}
	}

	if(ipRow == -1) 
	{
		if(imHighlightedRow == -1)
			return; // kein Job

		RemoveHighlight();
		return;
	}
	
	if(ipRow >= prmTableData->oTable.GetSize())
		return;

	// reset Background Color
	CPtrArray *polTableValue = (CPtrArray*)prmTableData->oTable[ipRow];
	FIELDDATA *polFieldData;
	for(int i = polTableValue->GetSize()-1;i>=0;i--)
	{
		polFieldData = (FIELDDATA*)(*polTableValue)[i];
		if(bpHighlightOn)
		{
			imOldColorNotHighlightedRow = polFieldData->oBkColorNr;
			polFieldData->oBkColorNr = BPN_YELLOW;
			if(ipRow < prmTableData->oSolid.GetSize())
			{
				((FIELDDATA*)prmTableData->oSolid[ipRow])->oBkColorNr = BPN_YELLOW;
			}
		}
		else
		{
			polFieldData->oBkColorNr = imOldColorNotHighlightedRow;
			if(ipRow < prmTableData->oSolid.GetSize())
			{
				((FIELDDATA*)prmTableData->oSolid[ipRow])->oBkColorNr = imOldColorNotHighlightedRow;
			}
		}
		
	}
	pomDynTab->InvalidateRect(NULL);

	imHighlightedRow = ipRow;
	
	//** Solid Text Color
	//FIELDDATA *polFieldData = (FIELDDATA*)prmTableData->oSolid[ipRow];
}

void DutySolidTabViewer::RemoveHighlight()
{
	if(imHighlightedRow == -1) return;

	// highlighted ausmachen
	HighlightRow(imHighlightedRow, false);
	imHighlightedRow = -1;
}





