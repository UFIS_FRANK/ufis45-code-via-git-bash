// MainFrm.cpp : implementation of the CMainFrame class
//
//////////////////////////////////////////////////////////////////////////////////
//
//		HISTORY
//
//		rdr		12.01.2k	GetMessageString added, overrides the CMDIFrameWnd-fct
//
//////////////////////////////////////////////////////////////////////////////////


#include <stdafx.h>
#include <resource.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//***************************************************************************
// CMainFrame
//***************************************************************************

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_VIEW_DUTYROSTER, OnViewDutyroster)
	ON_COMMAND(ID_VIEW_SHIFTROSTER, OnViewShiftroster)
	ON_COMMAND(ID_FILE_SETUP, OnFileSetup)
	ON_COMMAND(ID_PROGRAMM_STAMMDATEN, OnProgrammStammdaten)
	ON_COMMAND(ID_PROGRAMM_GRUPPIERUNG, OnProgrammGruppierung)
	ON_WM_CLOSE()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_TIMER()
	ON_COMMAND(ID_PROGRAMM_STAFFBALANCE, OnProgrammStaffBalance)
	ON_COMMAND(ID_PROGRAMM_INITACCOUNT, OnProgrammInitAccount)
	ON_COMMAND(ID_PROGRAMM_ABSENCEPLANNING, OnProgrammAbsencePlanning)
	ON_COMMAND(IDM_IMPORTSHIFTS, OnImportShifts)
	ON_MESSAGE(WM_BCADD,OnBcAdd)
	ON_MESSAGE(WM_USER_CHANGECODEDLG, OnChangeCodeDlg)
	ON_COMMAND(ID_SENDTOSAP, OnSendtosap)
	ON_COMMAND(IDM_RELOADBASICDATA, ReloadBasicData)
	//}}AFX_MSG_MAP
	// Global help commands
	ON_COMMAND(ID_HELP_FINDER, CMDIFrameWnd::OnHelpFinder)
	ON_COMMAND(ID_HELP, CMDIFrameWnd::OnHelp)
	ON_COMMAND(ID_CONTEXT_HELP, CMDIFrameWnd::OnContextHelp)
	ON_COMMAND(ID_DEFAULT_HELP, CMDIFrameWnd::OnHelpFinder)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

void  CMainFrame::ProcessTrafficLight(long BcNum, int ipState)
{
	CMainFrame *polMainFrame = DYNAMIC_DOWNCAST(CMainFrame,AfxGetMainWnd());
	if (polMainFrame != NULL)
	{
		polMainFrame->UpdateTrafficLight(ipState);
	}
}

//***************************************************************************
// CMainFrame construction
//***************************************************************************

CMainFrame::CMainFrame()
{
	// BC blinken an.
	m_bActivateBlinker = TRUE;
	// Men� nicht initialisiert
	bmMenuLoaded = false;

	pomCodeDlg = NULL;
	pomBigCodeDlg = NULL;

	bmIsSmall = true;
	bmShowDutyAllCodes = false;
	imSource = UNKNOWN_VIEW;

	imTimerValue = 60000 * 5;

	ogBcHandle.SetTrafficLightCallBack(ProcessTrafficLight);

}

//***************************************************************************
// CMainFrame destruction
//***************************************************************************

CMainFrame::~CMainFrame()
{
	if (pomCodeDlg != NULL)
		delete pomCodeDlg;

	if (pomBigCodeDlg != NULL)
		delete pomBigCodeDlg;
}

//***************************************************************************
// CMainFrame destruction
//***************************************************************************

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// Erzeugung der spezial StatusBar********************************************
	if (!m_wndStatusBar.CreateStatusBar(this, indicators, sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // Fehler beim Erzeugen
	}

	// COUNT, CAPS, NUM, OVR, SCROLL
	m_wndStatusBar.SetMode(1, XSB_BITMAP | DT_CENTER); // BC Blinker

	// Achtung hier mu� der Filename der Bitmapdatei angegeben werden.
	// Siehe Eintragung im Resource Editor
	m_wndStatusBar.SetBitmap(1,"go_m");
	m_wndStatusBar.SetWidth(1,45);


	// establish watch dog ?
	char pclBroadcastCheck[512];
	GetPrivateProfileString(pcgAppName, "BROADCASTCHECK", "YES",pclBroadcastCheck, sizeof pclBroadcastCheck, pcgConfigPath);
	// install watch dog as timer event 3, because timer event 2 is already used by AUTOSCROLL_TIMER_EVENT
	// we will be notified all minute
	if (stricmp(pclBroadcastCheck,"YES") == 0)
	{
		bmBroadcastCheckMsg = true;
	}
	else
	{
		bmBroadcastCheckMsg = false;
	}

	// Der CommHandler schickt alle BCAdd messages an dieses Fenster
	char pclBcMethod[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclBcMethod, sizeof pclBcMethod, pcgConfigPath);

	//MWO/RRO 25.03.03
	if (stricmp(pclBcMethod,"BCSERV") == 0)
	{
		ogCommHandler.RegisterBcWindow(this);
	}
	else
	{
		ogBcHandle.StartBc(); 
		if (stricmp(pclBcMethod,"BCCOM") == 0)
		{
			imTimerValue = 60 * 5000;
		}
	}

	SetTimer(3, imTimerValue, NULL);
	ogDdx.Register((void *)this,BROADCAST_CHECK,CString("BROADCAST_CHECK"), CString("Broadcast received"),ProcessCf);
	imBroadcastState = 0;


	// Warum wird das hier aufgerufen ?
	ogBcHandle.GetBc();


	// Code Dialog erstellen 
	pomBigCodeDlg = new CCodeBigDlg(this);
	pomCodeDlg = new CCodeDlg(IDD_CODE_DLG, this);
	/////////////////////////////////////////////////////////////////////////////	
	return 0;
}

//***************************************************************************
// 
//***************************************************************************

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;

	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
	return TRUE;
}

//***************************************************************************
// �ndert die Zeitpunkte in den Code Dialogen
// Die Zeiten k�nnen vom Schichtplan aus und vom Dienstplan aus ge�ndert werden
// **ACHTUNG: KANN SO ZZ NUR VOM DIENSTPLAN AUFGERUFEN WERDEN.
//***************************************************************************

void CMainFrame::ChangeValidDatesFromDuty(bool bpFromDutyRoster,COleDateTime opDateFrom,COleDateTime opDateTo, CString opOrgList, CString opPfcList, bool bpAllCodes)
{
	CString olTime;

	// G�ltigkeit pr�fen
	if (opDateFrom.GetStatus() == COleDateTime::invalid || opDateTo.GetStatus() == COleDateTime::invalid)
		return;

	// Daten retten (Umschaltung der Views!)
	omDutyDateFrom = opDateFrom;
	omDutyDateTo = opDateTo;

	// bpAllCodes speichern
	bmShowDutyAllCodes = bpAllCodes;

	// Test:
	olTime = "\nCMainframe Ergebniss:" + opDateFrom.Format("%d.%m.%Y") + "-" + opDateTo.Format("%d.%m.%Y");
	TRACE("Neue Zeit:%s\n",olTime);

	if (bpFromDutyRoster)
	{
		// Zeitraum weitergeben
		pomCodeDlg->ChangeValidDates(opDateFrom,opDateTo);
		pomBigCodeDlg->ChangeValidDates(opDateFrom,opDateTo);
	}
	else
	{
		// -> Schichtplan wird nicht per Datum eingeschr�nkt
		COleDateTime olNullTime;
		// Zeitraum weitergeben
		pomCodeDlg->ChangeValidDates(olNullTime,olNullTime);
		pomBigCodeDlg->ChangeValidDates(olNullTime,olNullTime);
	}

	// Org-Codes weitergeben
	pomCodeDlg->SetOrgCodes(opOrgList);
	pomCodeDlg->SetPfcCodes(opPfcList);
	pomBigCodeDlg->SetOrgCodes(opOrgList);
	pomBigCodeDlg->SetPfcCodes(opPfcList);

	if (bmIsSmall)
		// Grid im Dialog neu zeichnen
		pomCodeDlg->ReloadAndShow(bpAllCodes);
	else
		// Grid im Dialog neu zeichnen
		pomBigCodeDlg->ReloadAndShow(bpAllCodes);
}

//***************************************************************************
// Der Active MDI Child Frame wurde gewechselt
//***************************************************************************

void CMainFrame::ActiveFrameChanged(int npSource, CString opOrgList, CString opPfcList)
{
	imSource = npSource;
	bool blAllCodes = false;
	if (imSource == DUTY_VIEW)
	{
		pomCodeDlg->SetShowWish(true);
		pomBigCodeDlg->SetShowWish(true);
		// Weitergabe an Dialog
		pomCodeDlg->SetCaptionInfo(" ("+LoadStg(DUTY_CAPTION)+")");
		// Weitergabe an Dialog
		pomBigCodeDlg->SetCaptionInfo(" ("+LoadStg(DUTY_CAPTION)+")");

		// Zeitraum weitergeben
		pomCodeDlg->ChangeValidDates(omDutyDateFrom,omDutyDateTo);
		pomBigCodeDlg->ChangeValidDates(omDutyDateFrom,omDutyDateTo);
		blAllCodes = bmShowDutyAllCodes;
	} 
	else if (imSource == SHIFT_VIEW)
	{
		pomCodeDlg->SetShowWish(false);
		pomBigCodeDlg->SetShowWish(false);
		// Weitergabe an Dialog
		pomCodeDlg->SetCaptionInfo(" ("+LoadStg(SHIFT_CAPTION)+")");
		// Weitergabe an Dialog
		pomBigCodeDlg->SetCaptionInfo(" ("+LoadStg(SHIFT_CAPTION)+")");

		// -> Schichtplan wird nicht per Datum eingeschr�nkt
		COleDateTime olNullTime;
		// Zeitraum weitergeben
		pomCodeDlg->ChangeValidDates(olNullTime,olNullTime);
		pomBigCodeDlg->ChangeValidDates(olNullTime,olNullTime);
		blAllCodes = true;
	}

	// Neue Org-Codes �bergeben
	pomCodeDlg->SetOrgCodes(opOrgList);
	pomCodeDlg->SetPfcCodes(opPfcList);
	pomBigCodeDlg->SetOrgCodes(opOrgList);
	pomBigCodeDlg->SetPfcCodes(opPfcList);


	// Welcher Dialog ist aktiv
	if (bmIsSmall)
	{
		// Grid im Dialog neu zeichnen
		pomCodeDlg->ReloadAndShow(blAllCodes);
	}
	else
	{
		// Grid im Dialog neu zeichnen
		pomBigCodeDlg->ReloadAndShow(blAllCodes);
	}
}

//***************************************************************************
// Wechseln zwischen dem gro�en Code Dialog und dem kleinen Code Dialog
//***************************************************************************

LONG CMainFrame::OnChangeCodeDlg(UINT wParam, LONG /*lParam*/)
{
	bool blIsSmall;
	bool blAllCodes = false;

	if (imSource == DUTY_VIEW)
	{
		blAllCodes = bmShowDutyAllCodes;
	} 
	else if (imSource == SHIFT_VIEW)
	{
		blAllCodes = true;
	}
	
	// Quelle der Nachricht. Soll der Dialog verkleinert oder vergr��ert werden
	if (wParam == 1)
		blIsSmall = false;
	else
		blIsSmall = true;

	// Ver�nderung n�tig ?
	if (blIsSmall == bmIsSmall)
		return 0L;

	if (bmIsSmall)
	{
		//-----------------------------
		// Dialog von klein auf gro�
		//-----------------------------
		// Dialog ausblenden
		pomCodeDlg->ShowWindow(SW_HIDE);
		// Ladezeitr�ume aktualisieren
		//pomBigCodeDlg->ChangeValidDates(omResultFrom,omResultTo);
		// Grid neu laden
		pomBigCodeDlg->ReloadAndShow(blAllCodes);
		// Dialog anzeigen
		pomBigCodeDlg->ShowWindow(SW_SHOW);

	}
	else
	{	//-----------------------------
		// Dialog von gro� auf klein
		//-----------------------------
		// Dialog ausblenden
		pomBigCodeDlg->ShowWindow(SW_HIDE);
		// Ladezeitr�ume aktualisieren
		//pomCodeDlg->ChangeValidDates(omResultFrom,omResultTo);
		// Grid neu laden
		pomCodeDlg->ReloadAndShow(blAllCodes);
		// Dialog anzeigen
		pomCodeDlg->ShowWindow(SW_SHOW);
	}
	bmIsSmall = blIsSmall;
return 0L;
}

//***************************************************************************
// Sollte ein BC kommen f�r die sich diese Applikation angemeldet hat,
// wird diese Funktion aufgerufen und alle BC werden an den ogBcHandle 
// weitergegeben.
//***************************************************************************

LONG CMainFrame::OnBcAdd(UINT wParam, LONG /*lParam*/)
{
	// Statusblinker in der Statuszeile toggeln wenn gew�nscht
	if (m_bActivateBlinker)
	{
		if (m_wndStatusBar.GetBitmap(1) == "wait_m")
			m_wndStatusBar.SetBitmap(1,"go_m");
		else
			m_wndStatusBar.SetBitmap(1,"wait_m");
	}

	// Hier laufen alle BCs rein. (diese werden dann vom BcHandle weitergeleitet???)
	ogBcHandle.GetBc(wParam);

	imBroadcastState = 2;

	if (m_wndStatusBar.GetBitmap(1) == "wait_m")
		m_wndStatusBar.SetBitmap(1,"go_m");


	return TRUE;
}

//***************************************************************************
// Aufruf der Childframes
//***************************************************************************

void CMainFrame::OnViewDutyroster() 
{
	// Position des ersten Dokuments finden.
	POSITION olPos= (((CRosteringApp*)AfxGetApp())->pomDutyRosterTemplate->GetFirstDocPosition());

	// gibt es schon ein DutyRoster View ? Dann aktivieren und nicht neu erstellen.
	if  (olPos == 0)
		((CRosteringApp*)AfxGetApp())->pomDutyRosterTemplate->OpenDocumentFile(NULL);
	else
	{
		// Aktivieren
		MDINext(); 
	}

	// W�nsche Anzeigen lassen
	pomCodeDlg->SetShowWish(true);
	// W�nsche Anzeigen lassen
	pomBigCodeDlg->SetShowWish(true);


	// Titel setzen
	pomCodeDlg->SetCaptionInfo(" ("+LoadStg(DUTY_CAPTION)+")");
	// Weitergabe an Dialog
	pomBigCodeDlg->SetCaptionInfo(" ("+LoadStg(DUTY_CAPTION)+")");

	// Nur einen Dialog aktivieren
	if (bmIsSmall)
		pomCodeDlg->ShowWindow(SW_SHOW);
	else
		pomBigCodeDlg->ShowWindow(SW_SHOW);
}

//***************************************************************************
// Aufruf der Childframes
//***************************************************************************

void CMainFrame::OnViewShiftroster() 
{
	// Position des ersten Dokuments finden.
	POSITION olPos= (((CRosteringApp*)AfxGetApp())->pomShiftRosterTemplate->GetFirstDocPosition());

	// gibt es schon ein DutyRoster View ? Dann aktivieren und nicht neu erstellen.
	if  (olPos == 0)
		((CRosteringApp*)AfxGetApp())->pomShiftRosterTemplate->OpenDocumentFile(NULL);
	else
	{
		// Aktivieren
		MDINext(); 
	}

	// W�nsche nicht Anzeigen lassen
	pomCodeDlg->SetShowWish(false);
	// W�nsche nicht Anzeigen lassen
	pomBigCodeDlg->SetShowWish(false);


	// Titel setzen
	pomCodeDlg->SetCaptionInfo(" ("+LoadStg(SHIFT_CAPTION)+")");
	// Weitergabe an Dialog
	pomBigCodeDlg->SetCaptionInfo(" ("+LoadStg(SHIFT_CAPTION)+")");

	// -> Schichtplan wird nicht per Datum eingeschr�nkt
	COleDateTime olNullTime;
	// Zeitraum weitergeben
	pomCodeDlg->ChangeValidDates(olNullTime,olNullTime);
	pomBigCodeDlg->ChangeValidDates(olNullTime,olNullTime);

	// Nur einen Dialog aktivieren
	if (bmIsSmall)
		pomCodeDlg->ShowWindow(SW_SHOW);
	else
		pomBigCodeDlg->ShowWindow(SW_SHOW);
}

//***************************************************************************
// Setup Dialog aufrufen
//***************************************************************************

void CMainFrame::OnFileSetup() 
{
	SetupDlg polDlg(this);
	polDlg.DoModal();
}

//***************************************************************************
// Programm zur Verwaltung der Gruppen aufrufen (GRP)
//***************************************************************************

void CMainFrame::OnProgrammGruppierung() 
{
	CString slExcelPath;

	if(!strcmp(pcgGroupingPath, "DEFAULT")) //pclExcelPath, "DEFAULT"))
		MessageBox(LoadStg(IDS_STRING536), LoadStg(IDS_STRING535),MB_ICONERROR);

	char *args[4];
	char slRunTxt[256];
	args[0] = "child";
	sprintf(slRunTxt,"%s %s %s",ogAppl,pcgUser,pcgPasswd);
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;

	// Ausf�hren der Application
	int ilReturn = _spawnv(_P_NOWAIT,pcgGroupingPath, args); //pclExcelPath,args);

	// R�ckgabewert bearbeiten
	if  (ilReturn == -1)
	{
		// Fehlermeldung ausgeben
		slExcelPath = pcgGroupingPath;
		MessageBox(slExcelPath + LoadStg(IDS_STRING536),LoadStg(IDS_STRING535),MB_OK | MB_ICONSTOP);
	}

}

//***************************************************************************
// Programm zur Verwaltung der Stammdaten aufrufen (BDPS-UIF)
//***************************************************************************
void CMainFrame::OnProgrammStammdaten() 
{
	CString slExcelPath;

	if(!strcmp(pcgStammdatenPath, "DEFAULT"))
		MessageBox(LoadStg(IDS_STRING534), LoadStg(IDS_STRING535),MB_ICONERROR);

	CString	olTables = "ALTACTACRAPTRWYTWYPSTGATCICBLTEXTDENMVTNATHAGWROHTYSTYFIDGHSPERORGWAYPFCCOTASFBSDODATEASTFPRCGEG-STRSPHSEA";//Alle

	char *args[4];
	char slRunTxt[256];
	args[0] = "child";
	sprintf(slRunTxt,"%s,%s,%s,%s",ogAppName,pcgUser,pcgPasswd,olTables);
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;

	// Ausf�hren der Application
	int ilReturn = _spawnv(_P_NOWAIT,pcgStammdatenPath, args);

	// R�ckgabewert bearbeiten
	if  (ilReturn == -1)
	{
		// Fehlermeldung ausgeben
		slExcelPath = pcgStammdatenPath;
		MessageBox(slExcelPath + LoadStg(IDS_STRING534),LoadStg(IDS_STRING535),MB_OK | MB_ICONSTOP);
	}
}

//***************************************************************************
// Sicherheitsabfrage beim beenden der Application
//***************************************************************************
void CMainFrame::OnClose() 
{
	if (MessageBox(LoadStg(IDS_STRING11797),"Rostering",MB_YESNO | MB_ICONQUESTION) == IDYES)
	{
		WriteInRosteringLog(LOGFILE_OFF, "---------- Rostering Application was terminated ----------");
		CMDIFrameWnd::OnClose();
	}
}

//***************************************************************************
// Behandlung Doppelclick linke Mousetaste
//***************************************************************************

void CMainFrame::OnLButtonDblClk(UINT nFlags, CPoint point) 
{

	// Behandlung der Clicks in die Statusbar
	switch (m_wndStatusBar.GetPaneAtPosition(point))
	{
	// Infotext 
	case 0:	
			{
				// Umschalten auf Progressbar
				m_wndStatusBar.SavePane(0);
				m_wndStatusBar.SetMode(0, XSB_PROGRESS);
				m_wndStatusBar.SetRange(0, 0, 100);
				SetTimer(0, 10, NULL);
			}
			break;

	// BC Blinker
	case 1:	
			{
				// Aktivieren / Deakivieren
				if (m_bActivateBlinker)	{
					m_bActivateBlinker = FALSE;
					m_wndStatusBar.SetBitmap(1,"disa_m");
				}
				else {
					m_bActivateBlinker = TRUE;
					m_wndStatusBar.SetBitmap(1,"go_m");
				}
			}
			break;
	}
	
	CMDIFrameWnd::OnLButtonDblClk(nFlags, point);
}

//***************************************************************************
// Windows Timer
//***************************************************************************

void CMainFrame::OnTimer(UINT nIDEvent) 
{
	// Statusbartest 
	switch (nIDEvent)
	{
	case 0:	
			{	
				if (m_wndStatusBar.OffsetPos(0, 1) >= 100)
				{
					KillTimer(0);
					m_wndStatusBar.RestorePane(0);
				}
			
			} 
	break;
	case 3:		// watch dog event
			{
				switch(imBroadcastState)
				{
				case 0 :	// initialized
					{
						imBroadcastState = 1;	// data sent !
					}
				break;
				case 1 :	// data sent -> no broadcast message received
					{
						KillTimer(3);	// don't get recursive !
						m_wndStatusBar.SetBitmap(1,"stop_m");
						if (bmBroadcastCheckMsg)
						{
							CString olMessage(LoadStg(IDS_STRING11829)); 
							int ilState = AfxMessageBox(olMessage,MB_YESNO|MB_ICONSTOP);
							SetTimer(3, imTimerValue, NULL);
							if (ilState == IDYES)	// we try it again
							{
								imBroadcastState = 1;
							}
							else
							{
								imBroadcastState = 1;
								bmBroadcastCheckMsg = false;
							}
						}
						else	// retry !
						{
							SetTimer(3, imTimerValue, NULL);
							imBroadcastState = 1;
						}
					}
				break;
				case 2 :	// data received
					{
						imBroadcastState = 1;	// data sent !
						m_wndStatusBar.SetBitmap(1,"go_m");
					}
				break;
				case 3 :	// broadcast not received -> ignore further events
					;
				break;
				}
				break;
			}			

	}
	CMDIFrameWnd::OnTimer(nIDEvent);
}


//		rdr		12.01.2k added
void CMainFrame::GetMessageString(UINT nID, CString &rMessage) const
{
	rMessage = LoadStg(nID);
	rMessage.Replace('\n','\0');
}


//***************************************************************************
// CMainFrame diagnostics
//***************************************************************************

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG


void CMainFrame::OnProgrammStaffBalance() 
{
	//uhi 20.04.01
	CString slExcelPath;

	if(!strcmp(pcgStaffBalancePath, "DEFAULT")) 
	{
		MessageBox(LoadStg(IDS_STRING541), LoadStg(IDS_STRING535),MB_ICONERROR);
		return;
	}

	CString	olTables = "ALTACTACRAPTRWYTWYPSTGATCICBLTEXTDENMVTNATHAGWROHTYSTYFIDGHSPERORGWAYPFCCOTASFBSDODATEASTFPRCGEG-STRSPHSEA";//Alle

	char *args[4];
	char slRunTxt[256];
	args[0] = "child";
	sprintf(slRunTxt,"%s,%s,%s,%s",ogAppName,pcgUser,pcgPasswd,olTables);
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;

	// Ausf�hren der Application
	int ilReturn = _spawnv(_P_NOWAIT,pcgStaffBalancePath, args);

	// R�ckgabewert bearbeiten
	if  (ilReturn == -1)
	{
		// Fehlermeldung ausgeben
		slExcelPath = pcgStaffBalancePath;
		MessageBox(slExcelPath + CString("\n") + LoadStg(IDS_STRING541),LoadStg(IDS_STRING535),MB_OK | MB_ICONSTOP);
	}
}

void CMainFrame::OnProgrammInitAccount() 
{
	//uhi 20.04.01
	CString slExcelPath;

	if(!strcmp(pcgInitAccountPath, "DEFAULT")) 
	{
		MessageBox(LoadStg(IDS_STRING542), LoadStg(IDS_STRING535),MB_ICONERROR);
		return;
	}

	CString	olTables = "ALTACTACRAPTRWYTWYPSTGATCICBLTEXTDENMVTNATHAGWROHTYSTYFIDGHSPERORGWAYPFCCOTASFBSDODATEASTFPRCGEG-STRSPHSEA";//Alle

	char *args[4];
	char slRunTxt[256];
	args[0] = "child";
	sprintf(slRunTxt,"%s,%s,%s,%s",ogAppName,pcgUser,pcgPasswd,CTime::GetCurrentTime().Format("%d.%m.%Y"));
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;

	// Ausf�hren der Application
	int ilReturn = _spawnv(_P_NOWAIT,pcgInitAccountPath, args);

	// R�ckgabewert bearbeiten
	if  (ilReturn == -1)
	{
		// Fehlermeldung ausgeben
		slExcelPath = pcgInitAccountPath;
		MessageBox(slExcelPath + CString("\n") + LoadStg(IDS_STRING542),LoadStg(IDS_STRING535),MB_OK | MB_ICONSTOP);
	}
}

void CMainFrame::OnProgrammAbsencePlanning() 
{
	//bda 9.07.01
	CString slExcelPath;

	if(!strcmp(pcgAbsencePlanningPath, "DEFAULT")) 
	{
		MessageBox(LoadStg(IDS_STRING195), LoadStg(IDS_STRING535),MB_ICONERROR);
		return;
	}

	CString	olTables = "ALTACTACRAPTRWYTWYPSTGATCICBLTEXTDENMVTNATHAGWROHTYSTYFIDGHSPERORGWAYPFCCOTASFBSDODATEASTFPRCGEG-STRSPHSEA";//Alle

	char *args[4];
	args[0] = "child";
	char slRunTxt[256];
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	sprintf(slRunTxt,"ROSTER@%s@%s@1234",pcgUser,pcgPasswd);

	// Ausf�hren der Application
	int ilReturn = _spawnv(_P_NOWAIT,pcgAbsencePlanningPath, args);

	// R�ckgabewert bearbeiten
	if  (ilReturn == -1)
	{
		// Fehlermeldung ausgeben
		slExcelPath = pcgInitAccountPath;
		MessageBox(slExcelPath + CString("\n") + LoadStg(IDS_STRING195),LoadStg(IDS_STRING535),MB_OK | MB_ICONSTOP);
	}
}

void CMainFrame::OnImportShifts() 
{
	CImportShifts olImportShiftsDlg;

	if(olImportShiftsDlg.DoModal() != IDCANCEL)
	{
		return;
	}
}

void CMainFrame::OnSendtosap() 
{
	CSendToSapDlg olSendToSapDlg;
	if (olSendToSapDlg.DoModal() != IDCANCEL)
	{
		return;
	}
}

void CMainFrame::ReloadBasicData()
{
	CString	olWhere;
	CWaitDlg olDlg;
	olDlg.ShowWindow(SW_SHOW);

	olDlg.SetWindowText(LoadStg(IDS_STRING208));
	ogStfData.Read();
	olDlg.SetWindowText(LoadStg(IDS_STRING209));
	ogSorData.Read();
	olDlg.SetWindowText(LoadStg(IDS_STRING210));
	ogScoData.Read();
	olDlg.SetWindowText(LoadStg(IDS_STRING211));
	ogSpeData.Read();
	olDlg.SetWindowText(LoadStg(IDS_STRING212));
	ogSpfData.Read();
	olDlg.SetWindowText(LoadStg(IDS_STRING213));
	ogSwgData.Read();

	olDlg.ShowWindow(SW_HIDE);

	MessageBox(LoadStg(IDS_STRING103));
}

void CMainFrame::UpdateTrafficLight(int ipState)
{

	KillTimer(3);

	if(ipState == 0)	// waiting
	{
		if (m_bActivateBlinker)
		{
			if (m_wndStatusBar.GetBitmap(1) != "wait_m")
				m_wndStatusBar.SetBitmap(1,"wait_m");
		}
	}

	if(ipState == 1)	// ready
	{
		imBroadcastState = 2;
		if (m_bActivateBlinker)
		{
			if (m_wndStatusBar.GetBitmap(1) == "wait_m")
				m_wndStatusBar.SetBitmap(1,"go_m");
		}
	}

	SetTimer(3, imTimerValue, NULL);
}

void  CMainFrame::ProcessCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	; // nothing to do here
}
