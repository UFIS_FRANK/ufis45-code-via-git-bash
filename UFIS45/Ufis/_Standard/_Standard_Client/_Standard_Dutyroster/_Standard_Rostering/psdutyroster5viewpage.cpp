// PSDutyRoster5ViewPage.cpp : implementation file
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PSDutyRoster5ViewPage property page 
//---------------------------------------------------------------------------

IMPLEMENT_DYNCREATE(PSDutyRoster5ViewPage, RosterViewPage)

PSDutyRoster5ViewPage::PSDutyRoster5ViewPage() : RosterViewPage(PSDutyRoster5ViewPage::IDD)
{
	//{{AFX_DATA_INIT(PSDutyRoster5ViewPage)
	//}}AFX_DATA_INIT
	pomAccountList = NULL;
	bmPgpLoaded = false;
	pomAccounts = NULL;

	omTitleStrg = LoadStg(IDS_W_Konten);
	m_psp.pszTitle = omTitleStrg;
	m_psp.dwFlags |= PSP_USETITLE;
}

PSDutyRoster5ViewPage::~PSDutyRoster5ViewPage()
{
	// Grid l�schen
	if (pomAccountList)
		delete pomAccountList;

	pomAccountList = NULL;

}

void PSDutyRoster5ViewPage::DoDataExchange(CDataExchange* pDX)
{
	RosterViewPage::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(PSDutyRoster5ViewPage)
	DDX_Control(pDX, IDC_CHECK_MAINFUNC_ONLY, m_cl_CheckMainFuncOnly);
	DDX_Control(pDX, IDC_C_Konflikt, m_cl_Konflikt);
	//}}AFX_DATA_MAP

	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData();
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData();
	}
}


BEGIN_MESSAGE_MAP(PSDutyRoster5ViewPage, RosterViewPage)
	//{{AFX_MSG_MAP(PSDutyRoster5ViewPage)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PSDutyRoster5ViewPage message handlers

//**********************************************************************************
// 
//**********************************************************************************
void PSDutyRoster5ViewPage::SetCalledFrom(CString opCalledFrom)
{
	omCalledFrom = opCalledFrom;
}

//**********************************************************************************
// 
//**********************************************************************************
void PSDutyRoster5ViewPage::SetAccounts(CAccounts *popAccounts)
{
	pomAccounts = popAccounts;
}

//**********************************************************************************
// 
//**********************************************************************************

BOOL PSDutyRoster5ViewPage::OnInitDialog() 
{
	//Nur auf der Ersten Seite wird OnInitDialog vor SetData aufgerufen !!!!!!!!!
	RosterViewPage::OnInitDialog();

	//*** Set all adjustments to default ************************************

	SetStatic();
	SetButtonText();

	//*** end> Set all adjustments to default *******************************
	return TRUE;
}

//**********************************************************************************
// 
//**********************************************************************************

BOOL PSDutyRoster5ViewPage::GetData()
{
	omValues.RemoveAll();
	return (GetData(omValues));
}

//**********************************************************************************
// GetData: Sammelt die User-Einstellungen in opValues zum Wegschreiben 
//	in die Datenbank.
// R�ckgabe:	false	->	keine Eintr�ge oder Fehler
//				true	->	Eintr�ge ermittelt
//**********************************************************************************

BOOL PSDutyRoster5ViewPage::GetData(CStringArray &opValues)
{
CCS_TRY;
	ACCOUNTTYPEINFO *prlAccount;
	CGXStyle		olStyle;
	UINT			ilAccountInternNumber;
	CString olValue = "";

	int ilRowCount = (int) pomAccountList->GetRowCount();
	if (ilRowCount > 0) 
	{
		//    CString buf;
		
		//------------- 0 ------------------
		for (int ilPos=1; ilPos<=ilRowCount; ilPos++)
		{
			// alle anzuzeigenden Konten ermitteln
			if ((pomAccountList->GetValueRowCol(ilPos,1) != "") && // Kontoname vorhanden
				(pomAccountList->GetValueRowCol(ilPos,3) == "1"))	// ausgew�hlt
			{
				//			TRACE("PSDutyRoster5ViewPage::GetData(): Konto '" + prlAccount->oExternKey +"' wurde ausgew�hlt.\n");
				// DataPtr (= InternNumber des Kontos) der Zelle ermitteln
				pomAccountList->ComposeStyleRowCol(ilPos,1, &olStyle );
				ilAccountInternNumber = (UINT)olStyle.GetItemDataPtr();
				ASSERT(ilAccountInternNumber != 0);
				// Konto-Pointer ermitteln
				prlAccount = pomAccounts->GetAccountTypeInfoByAccountType(ilAccountInternNumber);
				// ExternNumber aus Konto speichern
				//			buf.Format("%d",(prlAccount->oExternKey));
				olValue = olValue + prlAccount->oExternKey + "|";
				//			opValues.Add(prlAccount->oExternKey);
			}
			else
			{
				//			olValue = olValue + "0" + "|";
			}
		}
		
	}
	// Zur Sicherheit Eingef�gt von ADO
	if (olValue.IsEmpty())
		olValue = " ";
		
	opValues.Add(olValue);		//------------- 0 ------------------
	//------------- 1 ------------------
	// Konflikt Check speichern
	olValue.Format("%d",m_cl_Konflikt.GetCheck());
	opValues.Add(olValue);		//------------- 1 ------------------

	//------------- 2 ------------------
	// CheckMainFuncOnly speichern
	olValue.Format("%d",m_cl_CheckMainFuncOnly.GetCheck());
	opValues.Add(olValue);		//------------- 2 ------------------

	return true;
CCS_CATCH_ALL;
return false;
}

//**********************************************************************************
// 
//**********************************************************************************

void PSDutyRoster5ViewPage::SetData()
{
CCS_TRY;
	SetData(omValues);
CCS_CATCH_ALL;
}

//**********************************************************************************
// (Initialisieren) L�d die Daten aus opValues in die PropertyPage
//**********************************************************************************

void PSDutyRoster5ViewPage::SetData(CStringArray &opValues)
{
	CCS_TRY;
	if(!bmPgpLoaded)
	{	
		// Grid initialisieren
		InitGrid();
		//SortGrid(2);
		
		bmPgpLoaded = true;
	}
	
	// Grid mit Daten f�llen.
	pomAccountList->GetParam()->SetLockReadOnly(false);
	
	FillGrid();
	
	pomAccountList->GetParam()->SetLockReadOnly(true);
	pomAccountList->Redraw();
	
	for(int ilValue = 0; ilValue < opValues.GetSize(); ilValue++)
	{
		switch(ilValue)
		{
		default:
			break;
		case 1:
			if(opValues[1]=="1")
			{
				m_cl_Konflikt.SetCheck(1);
			}
			else
			{
				m_cl_Konflikt.SetCheck(0);
			}
			break;
		case 2:
			if(opValues[2]=="1")
			{
				m_cl_CheckMainFuncOnly.SetCheck(1);
			}
			else
			{
				m_cl_CheckMainFuncOnly.SetCheck(0);
			}
			break;
		}
	}
	
	CCS_CATCH_ALL;
}

//**********************************************************************************
// Grid initialisieren
//**********************************************************************************

void PSDutyRoster5ViewPage::InitGrid()
{
CCS_TRY;
	// Wieviel Konten sollen angezeigt werden
	int ilShowAccountSize = 0;
	int ilAccountSize = pomAccounts->omAccountTypeInfoList.GetSize();
	for (int i=0; i<ilAccountSize; i++)
	{
		if (pomAccounts->omAccountTypeInfoList[i].bShowAccount == true)
			ilShowAccountSize++;
	}
	// Grid erzeugen und initialisieren
	pomAccountList = new CGridControl (this,IDC_ACCOUNT_GRID,3,ilShowAccountSize);
	pomAccountList->Initialize();

	// Scrollbar immer anzeigen
	pomAccountList->SetScrollBarMode(SB_VERT,gxnAutomatic);
	
	// Breite der Spalten einstellen
	InitColWidths ();
	
	// �berschriften setzen
	pomAccountList->SetValueRange(CGXRange(0,1), LoadStg(IDS_STRING1882));					//Code
	pomAccountList->SetValueRange(CGXRange(0,2), LoadStg(IDS_STRING60));	//Name
	//pomAccountList->SetValueRange(CGXRange(0,3), LoadStg(IDS_STRING1882));	//Anzeigen
	pomAccountList->SetValueRange(CGXRange(0,3), " ");	//Leer

	// Gibt es �berhaupt Eintr�ge
	if (ilShowAccountSize <= 0)
		return;
	
	// Grid mit Daten f�llen.
	//FillGrid();

	// CheckBoxen setzen, wenn Eintr�ge vorhanden
	pomAccountList->SetStyleRange(CGXRange(1,3,ilShowAccountSize,3),
								  CGXStyle().SetControl(GX_IDS_CTRL_CHECKBOX3D).
								  SetHorizontalAlignment(DT_CENTER));
	
	// Verhindert, das die Spaltenbreite ver�ndert wird
	//pomAccountList->GetParam()->EnableTrackColWidth(true);
	// Es k�nnen nur ganze Zeilen markiert werden.
	pomAccountList->GetParam()->EnableSelection();//GX_SELROW | GX_SELMULTIPLE | GX_SELSHIFT);
	// Tabelle ReadOnly einstellen
	pomAccountList->SetStyleRange(CGXRange().SetCols(0,1),
								  CGXStyle().SetVerticalAlignment(DT_VCENTER).SetReadOnly(true));
	// Spalte 3 (Checkboxen) ReadOnly ausschalten
	pomAccountList->SetStyleRange(CGXRange().SetCols(3),
								  CGXStyle().SetVerticalAlignment(DT_VCENTER).SetReadOnly(false));
	// verhindern, dass automatisch Zeilen eingef�gt werden
	pomAccountList->EnableAutoGrow(false);
	//pomAccountList->LockUpdate(FALSE);
	//pomAccountList->GetParam()->EnableUndo(TRUE);
CCS_CATCH_ALL;
}

//**********************************************************************************
// Grid Breite initialisieren
//**********************************************************************************

void PSDutyRoster5ViewPage::InitColWidths()
{
CCS_TRY;
	CString csTest;
	//int ilFactor = 9;

	pomAccountList->SetColWidth ( 0, 0, 27);
	pomAccountList->SetColWidth ( 1, 1, 40);
	pomAccountList->SetColWidth ( 2, 2, 200);
	/*
	// Breite richtet sich hier nach Anzahl der Buchstaben im Titel
	csTest = LoadStg(IDS_STRING1882);
	pomAccountList->SetColWidth ( 3, 3, csTest.GetLength()*ilFactor);*/
	pomAccountList->SetColWidth ( 3, 3, 30);
CCS_CATCH_ALL;
}

//**********************************************************************************
// FillGrid: f�llt das Grid mit Daten.
// R�ckgabe:	Anzahl der eingef�gten Eintr�ge 
//**********************************************************************************

int PSDutyRoster5ViewPage::FillGrid()
{
CCS_TRY;
	// R�ckgabewert: Anzahl der Eintr�ge im Grid
	int ilCountEntries = 0;

	//actualize entries
	pomAccounts->FillAccountTypeList();

	// Anzahl der bekannten Konten
	int ilAccountSize = pomAccounts->omAccountTypeInfoList.GetSize();

    // Kopie der Kontenliste
	ACCOUNTTYPEINFO* polAccInfo;

	bool blFound;

	// Auslesen welche Konten markiert werden sollen
	CString olSelExternKey = "";
	if (omValues.GetSize() != 0)
		olSelExternKey = omValues[0];

	// zuerst alle selektierte Accounts der Reihe nach
	int llItemCount = GetItemCount(olSelExternKey,'|');

	for (int ilCount = 1; ilCount<llItemCount; ilCount++)
	{
		CString olSelExtKey = GetListItem(olSelExternKey,ilCount,FALSE,'|');

		polAccInfo = pomAccounts->GetAccountTypeInfoByExternKey(olSelExtKey);
		if (polAccInfo && polAccInfo->bShowAccount)
		{
			ilCountEntries++;
			SetAccountRow(ilCountEntries, polAccInfo, true);
		}
	}

	// anschlie�end alle �brigen Accounts nach default-Ordnung
	olSelExternKey = "|" + olSelExternKey;
	
	for (ilCount=0; ilCount<ilAccountSize; ilCount++)
	{
		blFound = false;
		polAccInfo = &pomAccounts->omAccountTypeInfoList[ilCount];
		// Pr�fen ob der Eintrag schon gespeichert ist
		CString olSearch = "|" + polAccInfo->oExternKey + "|";

		if(olSelExternKey.Find(olSearch) >= 0)
		{
			// gefunden, ist bereits gespeichert
			continue;
		}
		// Ins Grid eintragen
		if (polAccInfo->bShowAccount == true)
		{
			ilCountEntries++;
			SetAccountRow(ilCountEntries, polAccInfo, blFound);
		}
	}

	return (ilCountEntries-1);
CCS_CATCH_ALL;
return 0;
}

/***********************************************************************************
Setzt eine Zeile in der Accountsliste
***********************************************************************************/
void PSDutyRoster5ViewPage::SetAccountRow(int ipCountEntries, ACCOUNTTYPEINFO* popAccInfo, bool bpFound)
{
	// Eintrag in Grid setzen und Checkbox einstellen
	pomAccountList->SetStyleRange (CGXRange(ipCountEntries,1),
								   CGXStyle().SetItemDataPtr((void*)popAccInfo->iInternNumber));
	pomAccountList->SetValueRange(CGXRange(ipCountEntries,1), popAccInfo->oExternKey );
	pomAccountList->SetValueRange(CGXRange(ipCountEntries,2), popAccInfo->oName );

	if (bpFound) 
		pomAccountList->SetValueRange(CGXRange(ipCountEntries,3), "1");
	else
		pomAccountList->SetValueRange(CGXRange(ipCountEntries,3), "0");
}

//**********************************************************************************
// 
//**********************************************************************************


void PSDutyRoster5ViewPage::SetStatic()
{
	SetDlgItemText(IDC_S_Konten,LoadStg(IDS_STRING1705));
	if("FRA" == ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER"))
	{
		GetDlgItem(IDC_C_Konflikt)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_MAINFUNC_ONLY)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_S_Konflikt)->ShowWindow(SW_HIDE);
	}
	else
	{
		SetDlgItemText(IDC_S_Konflikt,LoadStg(IDS_STRING1677));
	}

}

void PSDutyRoster5ViewPage::SetButtonText()
{
	if("FRA" != ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER"))
	{
		SetDlgItemText (IDC_C_Konflikt, LoadStg(IDS_STRING63479));			// Konfliktpr�fung aus.*INTXT*
		SetDlgItemText (IDC_CHECK_MAINFUNC_ONLY, LoadStg(IDS_STRING176));	// Schichtzuordnung nur f�r Stammfunktion pr�fen*INTXT*
	}
}
