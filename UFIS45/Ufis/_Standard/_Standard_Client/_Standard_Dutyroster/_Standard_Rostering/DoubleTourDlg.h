#if !defined(AFX_DOUBLETOURDLG_H__C45EE593_C736_11D3_8FDF_00500454BF3F__INCLUDED_)
#define AFX_DOUBLETOURDLG_H__C45EE593_C736_11D3_8FDF_00500454BF3F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DoubleTourDlg.h : Header-Datei
//
#include <cedadrsdata.h>
#include <cedastfdata.h>
#include <cedadrrdata.h>
#include <DutyRoster_View.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDoubleTourDlg 

class CDoubleTourDlg : public CDialog
{
// Konstruktion
public:
	CDoubleTourDlg(CWnd* pParent,DRRDATA* popDrr, DRSDATA* popDrs,CCSPtrArray<STFDATA>* popStfData, DutyRoster_View* pDutyRoster_View); 

// Dialogfelddaten
	//{{AFX_DATA(CDoubleTourDlg)
	enum { IDD = IDD_DOUBLETOUR };
	CListBox	m_LB_Empl2;
	//}}AFX_DATA


// �berschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktions�berschreibungen
	//{{AFX_VIRTUAL(CDoubleTourDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	DRSDATA* pomDrs;
	DRRDATA* pomDrr;
	// Zeiger auf Array mit allen Mitarbeitern
	CCSPtrArray<STFDATA>* pomStfData;
	// Pointer auf DutyRosterView f�r Fehlermeldungen
	DutyRoster_View* pomDutyRoster_View;

	// Maps
	CMapStringToString omStfu2Map;
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDoubleTourDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG

	// Bearbeitung der Daten f�r Mitarbeiter 1
	void EditEmpl1Data(CString olName2);
	// Bearbeitung der Daten f�r Mitarbeiter 2
	bool EditEmpl2Data(CString opName,DRRDATA* popDrr2, DRSDATA* popDrs2);
	// Erzeugung der Daten f�r Mitarbeiter 2
	DRSDATA* CreateEmpl2Drs(DRRDATA* polDrr2,long lpStfu2);
	DRRDATA* CreateEmpl2Drr(long lpStfu2);
	// Erzeugte Daten sichern
	bool SaveData(DRRDATA* popDrr2, DRSDATA* popDrs2);

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ f�gt unmittelbar vor der vorhergehenden Zeile zus�tzliche Deklarationen ein.

#endif // AFX_DOUBLETOURDLG_H__C45EE593_C736_11D3_8FDF_00500454BF3F__INCLUDED_
