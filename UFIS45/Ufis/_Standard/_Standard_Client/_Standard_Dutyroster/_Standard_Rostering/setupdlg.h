#ifndef AFX_SETUPDLG_H__EC726D71_1630_11D2_8D95_0000C002916B__INCLUDED_
#define AFX_SETUPDLG_H__EC726D71_1630_11D2_8D95_0000C002916B__INCLUDED_

// SetupDlg.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld SetupDlg 
#include <stdafx.h>
#include <Resource.h>
#include <CCSGlobl.h>
#include <Rostering.h>
#include <CedaCfgData.h>
//#include "CCSEdit.h"



class SetupDlg : public CDialog
{
// Konstruktion
public:
	SetupDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	~SetupDlg();
// Dialogfelddaten
	//{{AFX_DATA(SetupDlg)
	enum { IDD = IDD_MONITOR_SETUP };
	CSliderCtrl	m_Relation;
	CSliderCtrl	m_TotalWidth;
	CStatic	m_StaticTest;
	CSliderCtrl	m_PlanDays;
	CStatic	m_User;
	CSliderCtrl	m_Staticcolumns;
	CSliderCtrl	m_Debitrows;
	CStatic m_S_Debitrows;
	CStatic m_S_Staticcolumns;
	CStatic m_S_Plandays;
	int		m_Monitors;
	int		m_Sortstatistic;	// 0 - nach Schichtcode, 1 - nach Schichtbeginn
	int		m_DutyRoster;
	int		m_ShiftRoster;
	CCSEdit	m_ViewPl;
	CCSEdit	m_ViewMi;
	BOOL	m_bVorlauf;
	BOOL	m_bScroll;
	//}}AFX_DATA


	 CObList omMonitorButtons1;
	 CObList omMonitorButtons2;
	 CObList omMonitorButtons3;
	//CCSPtrArray<CButton> omMonitorButtons2;
	//CCSPtrArray<CButton> omMonitorButtons3;
// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(SetupDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(SetupDlg)
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	afx_msg void OnRadio3();
	afx_msg void OnRadio4();
	afx_msg void OnRadio5();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void SetStatics();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_SETUPDLG_H__EC726D71_1630_11D2_8D95_0000C002916B__INCLUDED_
