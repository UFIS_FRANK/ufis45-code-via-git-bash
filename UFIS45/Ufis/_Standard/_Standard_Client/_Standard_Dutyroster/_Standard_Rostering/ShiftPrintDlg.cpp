// ShiftPrintDlg.cpp : implementation file
//

#include <stdafx.h>
#include <rostering.h>
#include <ShiftPrintDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CString date;

CString ShiftPrintDlg::omLastComment;

static int SortByWgp(const SWGDATA **e1, const SWGDATA **e2)
{
	WGPDATA *prlWgp1 = ogWgpData.GetWgpByWgpc((**e1).Code);
	WGPDATA *prlWgp2 = ogWgpData.GetWgpByWgpc((**e2).Code);

	return strcmp(prlWgp1->Wgpc,prlWgp2->Wgpc);
}

/////////////////////////////////////////////////////////////////////////////
// ShiftPrintDlg dialog


ShiftPrintDlg::ShiftPrintDlg(CWnd* pParent /*=NULL*/,bool bpPrintBSR)
	: CDialog(ShiftPrintDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(ShiftPrintDlg)
	m_PrintType = -1;
	m_PrintBSRType = -1;
	m_PrintOverviewType = -1;
	m_PrintOrientation = -1;
	m_PrintBSRSHIFTType = -1;
	//}}AFX_DATA_INIT

	pomParent	= (ShiftRoster_View*)pParent;
	this->omStart.SetStatus(COleDateTime::invalid);
	this->omFrom.SetStatus(COleDateTime::invalid);
	this->omTo.SetStatus(COleDateTime::invalid);
	this->imOleDateWeekDay = -1;
	this->imCalCtrlWeekDay = -1;

	this->m_PrintType	 = 0;
	this->m_PrintBSRType = 2;
	this->m_PrintOverviewType = 0;
	this->m_PrintOrientation = 0;

	this->omAt.SetStatus(COleDateTime::invalid);

	this->bmPrintBSR = bpPrintBSR;		
    this->m_PrintBSRSHIFTType = 1;

}


void ShiftPrintDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ShiftPrintDlg)
	DDX_Radio(pDX, IDC_R_BSR,			m_PrintType);
	DDX_Radio(pDX, IDC_R_WORKGROUP,		m_PrintBSRType);
	DDX_Radio(pDX, IDC_R_OVW_WORKGROUP, m_PrintOverviewType);
	DDX_Control(pDX, IDC_EDIT_FROM,		m_PrintOverviewFrom);
	DDX_Control(pDX, IDC_EDIT_TO,		m_PrintOverviewTo);
	DDX_Radio(pDX, IDC_R_PORTRAIT,		m_PrintOrientation);
	DDX_Control(pDX, IDC_EDIT_AT,		m_PrintWorkGroupsAt);
	DDX_Control(pDX, IDC_LB_WRK_GROUPS,	m_PrintWorkGroupsList);
	DDX_Control(pDX, IDC_EDIT_COMMENT,	m_PrintWorkGroupsComment);
    DDX_Radio(pDX, IDC_R_SHIFTCODE, m_PrintBSRSHIFTType);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ShiftPrintDlg, CDialog)
	//{{AFX_MSG_MAP(ShiftPrintDlg)
	ON_BN_CLICKED(IDC_R_BSR, OnRBsr)
	ON_BN_CLICKED(IDC_R_OVERVIEW, OnROverview)
	ON_BN_CLICKED(IDC_BUTTON_TO, OnButtonTo)
	ON_BN_CLICKED(IDC_BUTTON_FROM, OnButtonFrom)
	ON_BN_CLICKED(IDC_BUTTON_AT, OnButtonAt)
	ON_BN_CLICKED(IDC_R_WORKGROUPS, OnRWorkGroups)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ShiftPrintDlg message handlers
void ShiftPrintDlg::ActivatePrintType(int ipPrintType)
{
	CWnd *polWnd = NULL;

	polWnd = this->GetDlgItem(IDC_R_WORKGROUP);
	polWnd->EnableWindow(ipPrintType == 0);
	polWnd = this->GetDlgItem(IDC_R_STAFFNAME);
	polWnd->EnableWindow(ipPrintType == 0);
	polWnd = this->GetDlgItem(IDC_R_NONE);
	polWnd->EnableWindow(ipPrintType == 0);

	polWnd = this->GetDlgItem(IDC_R_SHIFTCODE);	
	polWnd->EnableWindow(ipPrintType == 0);
	polWnd = this->GetDlgItem(IDC_R_SHIFTTIME);
	polWnd->EnableWindow(ipPrintType == 0);

	polWnd = this->GetDlgItem(IDC_R_OVW_WORKGROUP);
	polWnd->EnableWindow(ipPrintType == 1);
	polWnd = this->GetDlgItem(IDC_R_OVW_STAFFNAME);
	polWnd->EnableWindow(ipPrintType == 1);
	polWnd = this->GetDlgItem(IDC_R_OVW_ALL_STAFF);
	polWnd->EnableWindow(ipPrintType == 1);


	polWnd = this->GetDlgItem(IDC_BUTTON_FROM);
	polWnd->EnableWindow(ipPrintType == 1);
	polWnd = this->GetDlgItem(IDC_BUTTON_TO);
	polWnd->EnableWindow(ipPrintType == 1);


	polWnd = this->GetDlgItem(IDC_R_PORTRAIT);
	polWnd->EnableWindow(ipPrintType == 1);
	polWnd = this->GetDlgItem(IDC_R_LANDSCAPE);
	polWnd->EnableWindow(ipPrintType == 1);

	polWnd = this->GetDlgItem(IDC_BUTTON_AT);
	polWnd->EnableWindow(ipPrintType == 2);
	polWnd = this->GetDlgItem(IDC_LB_WRK_GROUPS);
	polWnd->EnableWindow(ipPrintType == 2);
	polWnd = this->GetDlgItem(IDC_EDIT_COMMENT);
	polWnd->EnableWindow(ipPrintType == 2);

}

void ShiftPrintDlg::OnRBsr() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;

	ActivatePrintType(m_PrintType);		
}

void ShiftPrintDlg::OnROverview() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	
	ActivatePrintType(m_PrintType);		
}

void ShiftPrintDlg::OnRWorkGroups() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	
	ActivatePrintType(m_PrintType);		
}

BOOL ShiftPrintDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	CString olStPt;
	this->pomParent->m_E_GPL_StPt.GetWindowText(olStPt);

	CString olVafr;
	this->pomParent->m_E_GPL_Vafr.GetWindowText(olVafr);

	CString olVato;
	this->pomParent->m_E_GPL_Vato.GetWindowText(olVato);


	this->omFrom = OleDateStringToDate(olVafr);
	this->omTo	 = OleDateStringToDate(olVato);
	this->omStart= this->omFrom;
	this->omEnd	 = this->omTo;

	m_PrintOverviewFrom.SetTypeToDate(true);
	m_PrintOverviewFrom.SetBKColor(LTYELLOW);
	m_PrintOverviewFrom.SetTextErrColor(RED);
	m_PrintOverviewFrom.SetReadOnly();
	m_PrintOverviewFrom.SetInitText(omStart.Format("%d.%m.%Y"));	


	m_PrintOverviewTo.SetTypeToDate(true);
	m_PrintOverviewTo.SetBKColor(LTYELLOW);
	m_PrintOverviewTo.SetTextErrColor(RED);
	m_PrintOverviewTo.SetReadOnly();
	m_PrintOverviewTo.SetInitText(omEnd.Format("%d.%m.%Y"));	


	this->omAt = COleDateTime::GetCurrentTime();
	m_PrintWorkGroupsAt.SetTypeToDate(true);
	m_PrintWorkGroupsAt.SetBKColor(LTYELLOW);
	m_PrintWorkGroupsAt.SetTextErrColor(RED);
	m_PrintWorkGroupsAt.SetReadOnly();

	m_PrintWorkGroupsAt.SetInitText(omAt.Format("%d.%m.%Y"));	

	this->m_PrintWorkGroupsComment.SetTypeToString("X(40)",40,0);
	this->m_PrintWorkGroupsComment.SetTextErrColor(RED);
	this->m_PrintWorkGroupsComment.SetInitText(omLastComment);


	ogWgpData.GetWgpList(this->m_PrintWorkGroupsList);		


	if (this->bmPrintBSR)
	{
		UpdateData(FALSE);
		ActivatePrintType(m_PrintType);		
	}
	else
	{
		CWnd *polWnd = NULL;

		polWnd = this->GetDlgItem(IDC_R_BSR);
		polWnd->EnableWindow(false);

		polWnd = this->GetDlgItem(IDC_R_OVERVIEW);
		polWnd->EnableWindow(false);

		m_PrintType = 2;			
		UpdateData(FALSE);
		ActivatePrintType(m_PrintType);		
	}



     m_PrintOverviewFrom.SetWindowText(date); 
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ShiftPrintDlg::OnOK() 
{
	// TODO: Add extra validation here
	if (!UpdateData(TRUE))
		return;

	// save last comment
	this->m_PrintWorkGroupsComment.GetWindowText(omLastComment);

	switch(this->m_PrintType)
	{
	case 0:	
		PrintBasicShiftRoster();
		break;
	case 1:
		PrintShiftRosterOverview();
		break;
	case 2:
		PrintWorkGroups();
		break;
	}
}

void	ShiftPrintDlg::PrintWorkGroups()
{
	//--- look after RosteringPrint.exe: start it, if not started yet or pending
	CString olStartApp = pcgRosteringPrintPath;
	DutyPrintDlg::CheckStartApp(olStartApp);
	
	//--- look after the file
	CString olTmp;
	CString olTmpLine;
	CString olFileName = DutyPrintDlg::GetUniqueFileName("SP_");
	ofstream of;

	//--- open the file
	of.open(olFileName);

	//--- writing the name of the report to print
	olTmp = "WORKGROUPS";
	of << "<=NAME=>" << olTmp << "<=\\=>" << endl;

	this->m_PrintWorkGroupsComment.GetWindowText(olTmp);
	of << "<=TITLE=>" << olTmp << "<=\\=>" << endl;

	//--- getting the user, e.g. "UserXYZ"
	olTmp = pcgUser;
	of << "<=USER=>" << olTmp << "<=\\=>" << endl;

	//--- getting valid at, e.g. "01.04.2002"
	this->m_PrintWorkGroupsAt.GetWindowText(olTmp);
	of << "<=AT=>" << olTmp << "<=\\=>" << endl;


	CStringArray olWgpList;
	if (this->m_PrintWorkGroupsList.GetSelCount() == 0)
	{
		CString olWgpc;
		for (int i = 0; i < this->m_PrintWorkGroupsList.GetCount(); i++)
		{
			this->m_PrintWorkGroupsList.GetText(i,olWgpc);
			olWgpList.Add(olWgpc);
		}
	}
	else
	{
		int nMaxItems = this->m_PrintWorkGroupsList.GetSelCount();
		int* ilIndexes = new int[nMaxItems];
		this->m_PrintWorkGroupsList.GetSelItems(nMaxItems,ilIndexes);
		CString olWgpc;
		for (int i = 0; i < nMaxItems; i++)
		{
			this->m_PrintWorkGroupsList.GetText(ilIndexes[i],olWgpc);
			olWgpList.Add(olWgpc);
		}

		delete[] ilIndexes;
	}

	COleDateTime olStart = COleDateTime(this->omAt.GetYear(),this->omAt.GetMonth(),this->omAt.GetDay(),0,0,0);
	COleDateTime olEnd	 = COleDateTime(this->omAt.GetYear(),this->omAt.GetMonth(),this->omAt.GetDay(),23,59,59);
	CCSPtrArray<SWGDATA> olSwgList;

	ogSwgData.GetSwgByWgpWithTime(&olWgpList,olStart,olEnd,&olSwgList);
	olSwgList.Sort(SortByWgp);

	CString olWgp;
	CString olPfc;
	int ilCurrLine = 0;
	for (int i = 0; i < olSwgList.GetSize(); i++)
	{
		SWGDATA *prlSwg = &olSwgList[i];
		if (prlSwg != NULL)
		{
			WGPDATA *prlWgp = ogWgpData.GetWgpByWgpc(prlSwg->Code);

			if (prlWgp != NULL)
				olWgp = prlWgp->Wgpc;
			else
				olWgp = "";

			// get staff function
			olPfc = ogSpfData.GetMainPfcBySurnWithTime(prlSwg->Surn,olStart);

			STFDATA *prlStf = ogStfData.GetStfByUrno(prlSwg->Surn);
			if (prlStf != NULL)
			{
				
				olTmp.Format("<=%d=>%s,%s,%s,%s<=\\=>",ilCurrLine++,olWgp,olPfc,prlStf->Peno,this->pomParent->GetFormatedEmployeeName(prlStf));
				of << olTmp << endl;
			}

		}
	}

	//--- close the file
	of.close();

	//--- look after RosteringPrint.exe: send filename to it, if started.
	DutyPrintDlg::SendFileName (olStartApp, olFileName);
	CDialog::OnOK();
		
}

void	ShiftPrintDlg::PrintBasicShiftRoster()
{
	int ilCount = this->pomParent->omGSPTableData.GetSize();
	if (ilCount == 0)
	{
		return;
	}

	//--- look after RosteringPrint.exe: start it, if not started yet or pending
	CString olStartApp = pcgRosteringPrintPath;
	DutyPrintDlg::CheckStartApp(olStartApp);
	
	//--- look after the file
	CString olTmp;
	CString olTmpLine;
	CString olFileName = DutyPrintDlg::GetUniqueFileName("SP_");
	ofstream of;
	int ilPos;

	//--- open the file
	of.open(olFileName);

	//--- writing the name of the report to print
	switch(this->m_PrintBSRType)
	{
	case BSR_PRINT_WORKGROUP:
		olTmp = "SHIFTPLAN1";
		break;
	case BSR_PRINT_FIRSTNAME:
		olTmp = "SHIFTPLAN2";
		break;
	case BSR_PRINT_NONE:
		olTmp = "SHIFTPLAN";
		break;
	}

	of << "<=NAME=>" << olTmp << "<=\\=>" << endl;

	//--- getting the title, e.g. "Saisondienstplan Lader"
	this->pomParent->m_E_GPL_Caption.GetWindowText(olTmp);
	ilPos = olTmp.Find ("URNO");
	if (ilPos > -1)
	{
		olTmp = olTmp.Left (ilPos);
		olTmp.TrimRight();
	}
	of << "<=TITLE=>" << olTmp << "<=\\=>" << endl;

	//--- getting the user, e.g. "UserXYZ"
	olTmp = pcgUser;
	of << "<=USER=>" << olTmp << "<=\\=>" << endl;

	//--- getting valid from, e.g. "01.04.2002"
	this->pomParent->m_E_GPL_Vafr.GetWindowText(olTmp);
	of << "<=FROM=>" << olTmp << "<=\\=>" << endl;

	//--- getting valid to, e.g. "28.04.2002"
	this->pomParent->m_E_GPL_Vato.GetWindowText(olTmp);
	of << "<=TO=>" << olTmp << "<=\\=>" << endl;

	//--- getting the organisation code, e.g. "Ladegruppen"
	if (ogBCD.GetFieldIndex("GPL","ORGU") > -1)
	{
		int ilSelItem = this->pomParent->m_CB_OrgUnit.GetCurSel();
		if (ilSelItem != LB_ERR)
		{
			CString olTmpTxt;
			this->pomParent->m_CB_OrgUnit.GetLBText(ilSelItem,olTmpTxt);
			int ilIndex = olTmpTxt.Find("URNO=");
			if (olTmpTxt.GetLength() > ( ilIndex + 5))
			{
				olTmp = olTmpTxt.Left(ilIndex);
				olTmp.TrimRight();
			}
			else
			{
				olTmp = "";
			}
		}
		else
		{
			olTmp = "";
		}
	}
	else
	{
		olTmp = "";	//not used in the shiftplanning yet (27.03.2002)
	}
	of << "<=ORGCODE=>" << olTmp << "<=\\=>" << endl;

	//--- getting the caption of the shiftplan, e.g. "Nr.,Mo.,Di.,Mi.,Do.,Fr.,Sa.,So.,Std."
	this->pomParent->m_S_Week.GetWindowText(olTmp);
	olTmpLine = olTmp + "|";
	this->pomParent->m_S_Mo.GetWindowText(olTmp);
	olTmpLine += olTmp + "|";
	this->pomParent->m_S_Tu.GetWindowText(olTmp);
	olTmpLine += olTmp + "|";
	this->pomParent->m_S_We.GetWindowText(olTmp);
	olTmpLine += olTmp + "|";
	this->pomParent->m_S_Th.GetWindowText(olTmp);
	olTmpLine += olTmp + "|";
	this->pomParent->m_S_Fr.GetWindowText(olTmp);
	olTmpLine += olTmp + "|";
	this->pomParent->m_S_Sa.GetWindowText(olTmp);
	olTmpLine += olTmp + "|";
	this->pomParent->m_S_Su.GetWindowText(olTmp);
	olTmpLine += olTmp + "|";
	this->pomParent->m_S_Houers.GetWindowText(olTmp);
	olTmpLine += olTmp;
	of << "<=CAPTION=>" << olTmpLine << "<=\\=>" << endl;

	//--- getting the shiftplan-lines and writing out line by line
	for (int i = 0; i < ilCount; i++)
	{
		// erste Schichten
		//this->pomParent->GetWeekValues(olTmpLine, i, &this->pomParent->omGSPTableData[i], 1,(BSR_PRINT_OPTIONS)this->m_PrintBSRType);
		this->pomParent->GetWeekValues(olTmpLine, i, &this->pomParent->omGSPTableData[i], 1,(BSR_PRINT_OPTIONS)this->m_PrintBSRType,(BSR_PRINT_OPTIONS_SHIFT)this->m_PrintBSRSHIFTType);
		of << olTmpLine << endl;

		// zweite Schichten
		//this->pomParent->GetWeekValues(olTmpLine, i, &this->pomParent->omGSPTableData[i], 2,(BSR_PRINT_OPTIONS)this->m_PrintBSRType);
		this->pomParent->GetWeekValues(olTmpLine, i, &this->pomParent->omGSPTableData[i], 2,(BSR_PRINT_OPTIONS)this->m_PrintBSRType,(BSR_PRINT_OPTIONS_SHIFT)this->m_PrintBSRSHIFTType);
		if (olTmpLine.GetLength() > 0)
		{
			of << olTmpLine << endl;
		}
	}

	//--- close the file
	of.close();

	//--- look after RosteringPrint.exe: send filename to it, if started.
	DutyPrintDlg::SendFileName (olStartApp, olFileName);
	CDialog::OnOK();

}

void	ShiftPrintDlg::PrintShiftRosterOverview()
{
	int ilCount = this->pomParent->omGSPTableData.GetSize();
	if (ilCount == 0)
	{
		return;
	}

	//--- look after RosteringPrint.exe: start it, if not started yet or pending
	CString olStartApp = pcgRosteringPrintPath;
	DutyPrintDlg::CheckStartApp(olStartApp);
	
	//--- look after the file
	CString olTmp;
	CString olTmpLine;
	CString olFileName = DutyPrintDlg::GetUniqueFileName("SP_");
	ofstream of;
	int ilPos;

	//--- open the file
	of.open(olFileName);

	//--- writing the name of the report to print
	switch(this->m_PrintOverviewType)
	{
	case BSR_OVW_PRINT_WORKGROUP:
		olTmp = "SHIFTOVERVIEW";
		break;
	case BSR_OVW_PRINT_FIRSTNAME:
		olTmp = "SHIFTOVERVIEW1";
		break;
	case BSR_OVW_PRINT_ALLNAMES:
		olTmp = "SHIFTOVERVIEW2";
		break;
	}

	of << "<=NAME=>" << olTmp << "<=\\=>" << endl;

	//--- getting the title, e.g. "Saisondienstplan Lader"
	this->pomParent->m_E_GPL_Caption.GetWindowText(olTmp);
	ilPos = olTmp.Find ("URNO");
	if (ilPos > -1)
	{
		olTmp = olTmp.Left (ilPos);
		olTmp.TrimRight();
	}
	of << "<=TITLE=>" << olTmp << "<=\\=>" << endl;

	//--- getting the user, e.g. "UserXYZ"
	olTmp = pcgUser;
	of << "<=USER=>" << olTmp << "<=\\=>" << endl;

	//--- getting valid from, e.g. "01.04.2002"
	this->m_PrintOverviewFrom.GetWindowText(olTmp);
	of << "<=FROM=>" << olTmp << "<=\\=>" << endl;

	//--- getting valid to, e.g. "28.04.2002"
	this->m_PrintOverviewTo.GetWindowText(olTmp);
	of << "<=TO=>" << olTmp << "<=\\=>" << endl;

	//--- getting the organisation code, e.g. "Ladegruppen"
	if (ogBCD.GetFieldIndex("GPL","ORGU") > -1)
	{
		int ilSelItem = this->pomParent->m_CB_OrgUnit.GetCurSel();
		if (ilSelItem != LB_ERR)
		{
			CString olTmpTxt;
			this->pomParent->m_CB_OrgUnit.GetLBText(ilSelItem,olTmpTxt);
			int ilIndex = olTmpTxt.Find("URNO=");
			if (olTmpTxt.GetLength() > ( ilIndex + 5))
			{
				olTmp = olTmpTxt.Left(ilIndex);
				olTmp.TrimRight();
			}
			else
			{
				olTmp = "";
			}
		}
		else
		{
			olTmp = "";
		}
	}
	else
	{
		olTmp = "";	//not used in the shiftplanning yet (27.03.2002)
	}
	of << "<=ORGCODE=>" << olTmp << "<=\\=>" << endl;


	//--- getting the caption of the shiftplan, e.g. "Nr.,Mo.,Di.,Mi.,Do.,Fr.,Sa.,So.,Std."
	olTmp = "|MONTH";
	olTmpLine = olTmp + "|";

	int ilMonth = -1;
	for (COleDateTime olDay = this->omStart; olDay <= this->omEnd; olDay += COleDateTimeSpan(7,0,0,0))
	{
		if (ilMonth == -1)
		{
			olTmp.Format("%d",olDay.GetMonth());
			olTmpLine += olTmp + "|";
			ilMonth = olDay.GetMonth();
		}
		else if (olDay.GetMonth() != ilMonth)
		{
			olTmp.Format("%d",olDay.GetMonth());
			olTmpLine += olTmp + "|";
			ilMonth = olDay.GetMonth();
		}
	}

	of << "<=CAPTION1=>" << olTmpLine << "<=\\=>" << endl;

	//--- getting the caption 2 of the shiftplan, e.g. "Nr.,Mo.,Di.,Mi.,Do.,Fr.,Sa.,So.,Std."
	olTmp = "S/N|NAME / Week Starting";
	olTmpLine = olTmp + "|";

	ilMonth = -1;
	for (olDay = this->omStart; olDay <= this->omEnd; olDay += COleDateTimeSpan(7,0,0,0))
	{
		// is it a monday ?
		if (olDay.GetDayOfWeek() == 2)
		{
			olTmp.Format("%d",olDay.GetDay());
			if (ilMonth == -1)
			{
				olTmpLine += olTmp;
				ilMonth = olDay.GetMonth();
			}
			else if (ilMonth != olDay.GetMonth())
			{
				olTmpLine += "|" + olTmp;
				ilMonth = olDay.GetMonth();
			}
			else
			{
				olTmpLine += "," + olTmp;
			}
		}
	}

	olTmpLine += "|";

	of << "<=CAPTION2=>" << olTmpLine << "<=\\=>" << endl;


	switch(this->m_PrintOrientation)
	{
	case 0 :
		of << "<=ORIENTATION=>" << "PORTRAIT"  << "<=\\=>" << endl;
		break;
	case 1 :
		of << "<=ORIENTATION=>" << "LANDSCAPE" << "<=\\=>" << endl;
		break;
	}

	int ilStartWeek = -1;

	CString olRowNo;

	for (int ilLine = 0; ilLine <  this->pomParent->omGSPTableData.GetSize();ilLine++)
	{

		CStringArray olUrnoList;
		::ExtractItemList(this->pomParent->omGSPTableData[ilLine].StaffUrno,&olUrnoList,'|');

		GSPTABLEDATA &rolTableData = this->pomParent->omGSPTableData[ilLine];
//		int number = atol(this->pomParent->omGSPTableData[ilLine].Number);
		int number = olUrnoList.GetSize();

		int middle = number / 2;

		if (number > 1 && this->m_PrintOverviewType != BSR_OVW_PRINT_ALLNAMES)
		{
			number = 1;
			middle = 0;
		}

		olRowNo.Format("<=WEEK=>%d",atoi(this->pomParent->omGSPTableData[ilLine].Week));
		of << olRowNo << "<=\\=>" << endl;

		if (number == 0)
		{
			// increment start week to get the correct week
			if (ilStartWeek > -1)
			{
				++ilStartWeek;
				if (ilStartWeek == this->pomParent->omGSPTableData.GetSize())
					ilStartWeek = 0;
			}
		}

		for (int i = 0; i < number; i++)
		{
			this->pomParent->omGSPTableData[ilLine];

			if (i == middle)
			{
				olTmp.Format("%d",atoi(this->pomParent->omGSPTableData[ilLine].Week));
			}
			else
			{
				olTmp = " ";
			}

			olTmpLine = olTmp + "|";

			switch(this->m_PrintOverviewType)
			{
			case BSR_OVW_PRINT_WORKGROUP:
				olTmp = this->pomParent->omGSPTableData[ilLine].Workgroup;
				break;
			case BSR_OVW_PRINT_FIRSTNAME:
				if (olUrnoList.GetSize() > 0)
				{
					STFDATA *prlStf = ogStfData.GetStfByUrno(atol(olUrnoList[0]));
					if (prlStf != NULL)
					{
						olTmp = this->pomParent->GetFormatedEmployeeName(prlStf);
					}
					else
					{ 
						olTmp = " ";
					}
				}
				else
				{
					olTmp = " ";
				}
				break;
			case BSR_OVW_PRINT_ALLNAMES:
				if (i < olUrnoList.GetSize())
				{
					STFDATA *prlStf = ogStfData.GetStfByUrno(atol(olUrnoList[i]));
					if (prlStf != NULL)
					{
						olTmp = this->pomParent->GetFormatedEmployeeName(prlStf);
					}
					else
					{
						olTmp = " ";
					}
				}
				else
				{
					olTmp = " ";
				}
				break;
			}

			olTmpLine += olTmp + "|";

			if (i == 0)
			{
				if (ilStartWeek == -1)
				{
					COleDateTimeSpan olDiff = this->omStart - this->omFrom;
					ilStartWeek = (int)olDiff.GetTotalDays() / 7 + 1;
					ilStartWeek = pomParent->RotateShiftRoster(1,ilStartWeek,this->pomParent->omGSPTableData.GetSize()) - 1;
				}
				else
				{
					++ilStartWeek;
					if (ilStartWeek == this->pomParent->omGSPTableData.GetSize())
						ilStartWeek = 0;
				}
			}

			int  ilWeek = ilStartWeek;
			ilMonth = -1;

			for (COleDateTime olWeekDay = this->omStart; olWeekDay <= this->omEnd; olWeekDay += COleDateTimeSpan(7,0,0,0))
			{
				if (i == middle)
				{
					olTmp.Format("%d",ilWeek+1);
				}
				else
				{
					olTmp = " ";
				}

				++ilWeek;
				if (ilWeek == this->pomParent->omGSPTableData.GetSize())
					ilWeek = 0;


				if (ilMonth == -1)
				{
					olTmpLine += olTmp;
					ilMonth = olWeekDay.GetMonth();
				}
				else if (ilMonth != olWeekDay.GetMonth())
				{
					olTmpLine += "|" + olTmp;
					ilMonth = olWeekDay.GetMonth();
				}
				else
				{
					olTmpLine += "," + olTmp;
				}
			}

			olTmpLine += "|";

			olRowNo.Format("<=DATA=>");
			of << olRowNo << olTmpLine << "<=\\=>" << endl;
		}
	}

	//--- close the file
	of.close();

	//--- look after RosteringPrint.exe: send filename to it, if started.
	DutyPrintDlg::SendFileName (olStartApp, olFileName);
																				
	CDialog::OnOK();
}




void ShiftPrintDlg::OnButtonTo() 
{
	// TODO: Add your control notification handler code here
	CSelectDateDlg olSelectDateDlg(this);

	olSelectDateDlg.SetValidWeekDay(1);

	if (this->omEnd.GetStatus() == COleDateTime::valid)
		olSelectDateDlg.SetDate(this->omEnd);

	if (this->omFrom.GetStatus() == COleDateTime::valid && this->omTo.GetStatus() == COleDateTime::valid)
	{
		olSelectDateDlg.SetRange(this->omFrom,this->omTo);
	}

	if (olSelectDateDlg.DoModal() == IDOK)
	{
		this->omEnd = olSelectDateDlg.GetDate();
		m_PrintOverviewTo.SetInitText(omEnd.Format("%d.%m.%Y"));	

	}
}

void ShiftPrintDlg::OnButtonFrom() 
{
	// TODO: Add your control notification handler code here
	CSelectDateDlg olSelectDateDlg(this);

	olSelectDateDlg.SetValidWeekDay(2);

	if (this->omStart.GetStatus() == COleDateTime::valid)
	{
	
	   CString dd;
		m_PrintOverviewFrom.GetWindowText(dd);
		
		
		int y , m ,d ;
        sscanf(dd,   "%d.%d.%d",   &d,   &m,   &y);   
		
		
		
		omStart.SetDate(y,m,d );
		
			
		
		olSelectDateDlg.SetDate(this->omStart);
	
	
	
	
	}
	if (this->omFrom.GetStatus() == COleDateTime::valid && this->omTo.GetStatus() == COleDateTime::valid)
	{
		olSelectDateDlg.SetRange(this->omFrom,this->omTo);
	}
	
	if (olSelectDateDlg.DoModal() == IDOK)
	{
		this->omStart = olSelectDateDlg.GetDate();
		m_PrintOverviewFrom.SetInitText(omStart.Format("%d.%m.%Y"));	
	}
	
}

void ShiftPrintDlg::OnButtonAt() 
{
	// TODO: Add your control notification handler code here
	CSelectDateDlg olSelectDateDlg(this);

	if (this->omAt.GetStatus() == COleDateTime::valid)
		olSelectDateDlg.SetDate(this->omAt);

	if (olSelectDateDlg.DoModal() == IDOK)
	{
		this->omAt = olSelectDateDlg.GetDate();
		this->m_PrintWorkGroupsAt.SetInitText(omAt.Format("%d.%m.%Y"));	
	}
	
}
