// InfoBoxDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld InfoBoxDlg 
//---------------------------------------------------------------------------

InfoBoxDlg::InfoBoxDlg(CString opHeaderText, CString opFieldText, CWnd* pParent /*=NULL*/, CRect  opRect/*=CRect(0,0,max,max)*/): CDialog(InfoBoxDlg::IDD, pParent)
{
	omHText = opHeaderText;
	omText  = opFieldText;
	omRect  = opRect;

	//{{AFX_DATA_INIT(InfoBoxDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
	CDialog::Create(InfoBoxDlg::IDD, pParent);

}

void InfoBoxDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(InfoBoxDlg)
	DDX_Control(pDX, IDC_TEXT, m_Text);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(InfoBoxDlg, CDialog)
	//{{AFX_MSG_MAP(InfoBoxDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten InfoBoxDlg 
//---------------------------------------------------------------------------

BOOL InfoBoxDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(omHText);
	m_Text.SetWindowText(omText);

	int ilX = (omRect.right  - omRect.left)/2 + omRect.left;
	int ilY = (omRect.bottom - omRect.top)/2 + omRect.top;

	CRect olDlgRect;
	GetWindowRect(&olDlgRect);

	int ilHight = olDlgRect.bottom - olDlgRect.top;
	int ilWidth = olDlgRect.right  - olDlgRect.left;

	olDlgRect.left   = ilX - ilWidth/2;
	olDlgRect.top    = ilY - ilHight/2;
	olDlgRect.right  = olDlgRect.left + ilWidth;
	olDlgRect.bottom = olDlgRect.top + ilHight;

	MoveWindow(&olDlgRect);

	ShowWindow(SW_SHOWNORMAL);
	return TRUE;
}

//---------------------------------------------------------------------------

void InfoBoxDlg::OnCancel()
{
	//do nothing more
}

//---------------------------------------------------------------------------

void InfoBoxDlg::OnOK() 
{
	//do nothing more
}

//---------------------------------------------------------------------------

void InfoBoxDlg::InternalCancel() 
{
	CDialog::OnCancel();
	CDialog::DestroyWindow();
	delete this;
}

//---------------------------------------------------------------------------
