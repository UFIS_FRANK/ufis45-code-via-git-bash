#if !defined(_DUTYISTABVIEWER_H_INCLUDED_)
#define _DUTYISTABVIEWER_H_INCLUDED_

#include <stdafx.h>
/*#include "CViewer.h"
#include <CCSDynTable.h>
#include <CCSPtrArray.h>
#include <DutyRoster_View.h>
#include <ShiftCheck.h>
#include <CedaDrrData.h>
#include <Groups.h>*/

struct DRRDATA_SELECTION 
{
	CCSPtrArray<DRRDATA>* pomDrrList;		// alle DRRs des Tages
	char		cmPType[ROSL_LEN+2];	// Dienstplanstufe
	char 		omSday[SDAY_LEN+2]; 	// Tages-Schl�ssel YYYYMMDD
	long		lmStfUrno; // Mitarbeiter-Urno
};

/////////////////////////////////////////////////////////////////////////////
// DutyIsTabViewer


/////////////////////////////////////////////////////////////////////////////
class DutyIsTabViewer : public CViewer
{
// Constructions
public:
    DutyIsTabViewer(CWnd* pParent = NULL);
    ~DutyIsTabViewer();
	DutyRoster_View *pomParent;

    void Attach(CCSDynTable *popAttachWnd);
    virtual void ChangeView(bool bpSetHNull = true, bool bpSetVNull = true);

// public data processing routines
public:
	// wenn mindestens ein Feld selektiert ist, wird der erste zur�ckgegeben, sonst 0
	SELECTEDFIELD* GetFirstSelectedField();
	// wenn mehr als ein Feld selektiert ist, gibt diese Funktion nacheinander alle 
	// selektierten Felder, nach dem letzten Feld - 0
	SELECTEDFIELD* GetNextSelectedField();
	void UnselectFields();

// Internal data processing routines
private:
	void InitTableHeaderArrays(int ipColumnOffset = 0);
	void MakeTableData(int ipColumnOffset,int ipRowOffset);
	void AddRow(int ipRow, int ipColumns, int ipColumnOffset, int ipRowOffset);
	void DelRow(int ipRow);
	void AddColumn(int ipColumns, int ipColumnNr, int ipRowNr);
	void DelColumn(int ipColumn);
	// Bildschirmkoordinaten eines Feldes berechnen und Hintergrund neu zeichnen
	void RedrawBkColorOfField(int ipRow, int ipRowOffset, int ipColumn, 
							  int ipColumnOffset, int ipField);
	void ChangeFieldTextColor(int ipRow, int ipColumn, COLORREF opColor);
	void ChangeFieldMarkerColor(int ipRow, int ipColumn, int ipColorNr, int ipMarker);
	int ChangeFieldBkColor(int ipRow, int ipColumn, int ipBkColorNr);
	// Setzt ein Feld auf Read Only
	void SetFieldReadOnly(int ipRow, int ipColumn, bool bpReadOnly);
	int GetFieldBkColor(int ipRow, int ipColumn);
	int GetDayStrg(int ipDayOffset, CString &opText, bool &bpIsHoliday);
	void GenerateFieldValues(int ipStfNr, CString opPType, int ipPTypeNr, int ipColumn, int ipRow, FIELDDATA *popFieldData);
	void ChangeFieldText(int ipRow, int ipColumn, CString opText);
	// Anzahl der Row ermitteln
	int GetRows();
	// Setzt �berschrift und Farbe einer Column
	void AddColumnHeader(int ipColumn, int ipColumnOffset);
	// Setzt �berschrift und Farbe einer Column
	void InsertColumnHeader(int ipColumn, int ipColumnOffset);
	// Pr�fen ob der Text ein 1. Schicht 2. Abwesenheit 3. Wunschcode ist
	// Pr�fen ob Code g�ltig und ob es ein "freier" Code ist
	bool GetCodeType(COleDateTime opDay,CString opText, int& ipCodeType, bool& bpIsFreeType);
	// Testen ob der Text in die passende Planungstufe eingetragen wurde
	bool IsTextValidInPosition(CString opNewText,COleDateTime opDay);
	// Die Eingabe in ein DRW (W�nsche) Feld wird in die Datenbank �bertragen
	void UpdateDRWData(CString opNewText);
	// pr�ft ob ein bestimmter Planungtyp angezeigt werden soll
	bool IsTypeActive(CString opType);
	// L�schen einer Schicht, Abwesenheit oder eines Wunsches 
	void DeleteCode(COleDateTime opDay,CString opOldText);
	// Berechnung der Row und Column f�r ein Datenelement
	bool GetRowAndColumn(long opSearchUnro,CString olPlaningType, CString opSday, int &ipRow,int &oiColumn);
	// Pr�ft ob ein DRW Datensatz extra Informationen enth�lt
	bool HasDrwExtraInformation(CString opSday,CString opStfu);
	// updatet alle selektierte Drrs und l�scht DRRDATAs aus dem DRRDATA_SELECTION
	bool UpdateSelectedDrr(DRRDATA_SELECTION* popDrrSel, DRRDATA* popDrr, int ipCodeType, long lpNewShiftUrno, CString opNewFctc, CString opDrrn = "1", bool bpAddDrr = true, int ipShiftCount = 1);
	// holt selektierte DRRDATA
	int GetSelectedDrr(int ipField, DRRDATA_SELECTION* popDrrSel);
	// die DRRs aller in <omSelectedField[]> selektierten Felder �ndern / erzeugen
	void UpdateMultiDrrOfFields(int ipCodeType, long lpNewShiftUrno, CString opNewFctc, CString opDrrn, bool bpAddDrr,int ipShiftCount = 1);
	// beim Broadcast-Handler anmelden
	void Register(void);
	// pr�fen, ob ein DRR angezeigt werden muss und kann
	bool GetPositionOfDrr(DRRDATA *prpDrr, CString olRosl,int &ipRow, int &ipColumn);
	// Erzeugt alle relevanten Daten f�r ein Wunschfeld, und gibt sie an die Dyntable weiter
	void GenerateWishValues(long lpTmpUrno,CString opTmpUrno,COleDateTime olDay,CString olSday,FIELDDATA *popFieldData,int ipRow,int ipColumn);
	// Pr�ft, ob ein Feld durch Multiselektion ausgew�hlt wurde.
	bool IsFieldMultiselected(int ipRow,int ipColumn);
	// Urno f�r vorgegebenen Schichtcode und Planungsstufe ermitteln.
	long GetUrnoForCode(CString olNewText,int ilCodeType,COleDateTime olDay);

// Operations
public:

// Window refreshing routines
public:
	void HorzTableScroll(MODIFYTAB *prpModiTab);
	void VertTableScroll(MODIFYTAB *prpModiTab);
	void TableSizing(/*UINT nSide, */LPRECT lpRect);
	void MoveTable(MODIFYTAB *prpModiTab);
	void SelectUpdateField(INLINEDATA *prpInlineUpdate);
	void InlineUpdate(INLINEDATA *prpInlineUpdate);
	void ProcessReload();

	bool GetElementAndPType(int ipRowNr, int &ipRow, CString &opPType, int &ipPTypNr);
	int	 GetRowForUrno(long lpUrno);
	bool ScrollToVisible(long lpUrno);
	// Neue Daten in der Wunschzeile in die Daten der Tabelle eintragen
	void ProcessDrwChange(int ipDDXType,void *vpDataPointer);
	// Ermittelt den Schichtcode f�r eine Row
	int GetFieldCode(int ipRow);
	// L�schen eines Wunsches 
	void DeleteDRWData();
	bool IsMultiSelection();
	// Broadcast DRA_CHANGE bearbeiten
	void ProcessDraChange(DRADATA *prpDra);
	// Broadcast DRR_CHANGE bearbeiten
	void ProcessDrrChange(DRRDATA *prpDrr);
	// Broadcast DRR_DELETE bearbeiten
	void ProcessDrrDelete(DRRDATA *prpDrr);
	// Broadcast DRR_NEW bearbeiten
	//void ProcessDrrNew(DRRDATA *prpDrr);
	// Setzt alle selektieren DRRs zur�ck
	void ClearSelectedDrrData();

// Attributes
private:
    CCSDynTable *pomDynTab;
	TABLEDATA *prmTableData;
	
	VIEWINFO *prmViewInfo;
	CCSPtrArray<STFDATA> *pomStfData;
	//CCSPtrArray<GROUPSTRUCT> *pomGroups;
	CGroups *pomGroups;

	COleDateTime omFrom;
	COleDateTime omTo;
	COleDateTime omShowFrom;
	COleDateTime omShowTo;
	int imDays;

	CStringArray omPTypes;

	CCSPtrArray<SELECTEDFIELD> omSelectedField; 
	int imIndex_SelectedField;		// interner Z�hler DIESE VARIABLE IST NUR F�R ZWEI FUNKTIONEN BESTIMMT; NICHT BENUTZEN!

public:
	CMapStringToPtr omHolidayKeyMap; //Map for Holidays


// Methods which handle changes (from Data Distributor)
public:

};

#endif //_DUTYISTABVIEWER_H_INCLUDED_
