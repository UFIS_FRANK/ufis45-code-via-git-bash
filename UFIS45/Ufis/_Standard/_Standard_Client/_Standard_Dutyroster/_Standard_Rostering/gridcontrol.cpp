// GridControl.cpp: implementation of the CGridControl class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <resource.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
//	Klasse f�r ein Grid innerhalb eines Dialogs, 
//  Baseclass:	BCGXGridWnd
//////////////////////////////////////////////////////////////////////


BEGIN_MESSAGE_MAP(CGridControl, CGXGridWnd)
	//{{AFX_MSG_MAP(CGridControl)
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDOWN()
//	ON_WM_KILLFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


CGridControl::CGridControl()
{
	bmAutoGrow = true;
	bmSortEnabled = true;
	bmSortAscend = true;
	memset ( &smLButtonDblClick, 0, sizeof(smLButtonDblClick) );
	bmIsDirty = false;
	bmIsDnDEnabled = false;
	bmShowExtraToolTip = false;

	////uhi 18.7.00 Abspeichern Sortierung
	omSortInfo.SetSize(2);
	omSortInfo[0].sortOrder = CGXSortInfo::ascending;
	omSortInfo[1].sortOrder = CGXSortInfo::ascending;
	omSortInfo[0].nRC = 1;
	omSortInfo[1].nRC = 1;
	omSortInfo[0].sortType = CGXSortInfo::autodetect;
	omSortInfo[1].sortType = CGXSortInfo::autodetect;

}

CGridControl::CGridControl ( CWnd *popParent, UINT nID, ROWCOL nCols, ROWCOL nRows, bool bpDeleteSelection/*=true*/ )
{
	umID = nID;
	pomWnd = popParent;
	SubclassDlgItem ( umID, pomWnd );
	Initialize();
	SetParent ( pomWnd );
	GetParam()->EnableUndo(FALSE);
	LockUpdate(TRUE);
	SetRowCount(nRows);
	SetColCount(nCols);
	LockUpdate(FALSE);
	GetParam()->EnableUndo(TRUE);
	bmAutoGrow = true;
	bmSortEnabled = true;
	bmSortAscend = true;
	GetParam()->EnableTrackRowHeight(FALSE);
	memset ( &smLButtonDblClick, 0, sizeof(smLButtonDblClick) );
	bmIsDirty = false;
	bmIsDnDEnabled = false;
	bmShowExtraToolTip = false;
	bmDeleteSelection = bpDeleteSelection;

	//uhi 18.7.00 Abspeichern Sortierung
	omSortInfo.SetSize(2);
	omSortInfo[0].sortOrder = CGXSortInfo::ascending;
	omSortInfo[1].sortOrder = CGXSortInfo::ascending;
	omSortInfo[0].nRC = 1;
	omSortInfo[1].nRC = 1;
	omSortInfo[0].sortType = CGXSortInfo::autodetect;
	omSortInfo[1].sortType = CGXSortInfo::autodetect;
}

CGridControl::~CGridControl()
{

}

BOOL CGridControl::SetValue ( ROWCOL nRow, ROWCOL nCol, CString &cStr ) 
{
	return SetValueRange( CGXRange(nRow, nCol), cStr );
}


const CString& CGridControl::GetValue ( ROWCOL nRow, ROWCOL nCol ) 
{
	return GetValueRowCol ( nRow, nCol );
}

BOOL CGridControl::EnableGrid ( ROWCOL iRow, BOOL enable )
{
	BOOL blRet = false;
	ROWCOL ilColCount = GetColCount ();

	if (iRow >= 1 && ilColCount >= 1)
	{
		CGXStyle olStyle;
		CGXRange olRange(iRow, 1, iRow, ilColCount);

		olStyle.SetInterior(ogColors[enable ? WHITE_IDX : SILVER_IDX]);
		olStyle.SetEnabled(enable);
		blRet = SetStyleRange(olRange, olStyle);
	}
	Redraw();
	return blRet;
}

BOOL CGridControl::IsGridEnabled ( ROWCOL iRow, ROWCOL iCol )
{
	if (iRow >= 1 && iCol >= 1)
	{
		CGXStyle olStyle;
		ComposeStyleRowCol( iRow, iCol, &olStyle );

		return olStyle.GetIncludeEnabled() && olStyle.GetEnabled();
	}
	return false;
}

void CGridControl::EnableAutoGrow ( bool enable /*=true*/ )
{
	bmAutoGrow = enable;
}

BOOL CGridControl::OnEndEditing( ROWCOL nRow, ROWCOL nCol) 
{
	BOOL blRet = CGXGridWnd::OnEndEditing( nRow, nCol) ;
	DoAutoGrow ( nRow, nCol) ;
	CGXControl* polControl;
	polControl = GetControl( nRow, nCol);
	if ( polControl != 0 )
	{
		bmIsDirty |= ( polControl->GetModify () == TRUE );
	}
	return blRet;
}

void CGridControl::DoAutoGrow ( ROWCOL nRow, ROWCOL nCol) 
{
	ROWCOL ilRowCount = GetRowCount ();
	if ( bmAutoGrow &&( nRow >= ilRowCount ) )
	{	//  es wird bereits in letzter Zeile editiert, d.h. event. Grid verl�ngern
		if ( ! GetValueRowCol ( nRow, nCol).IsEmpty () )
		{
			InsertBottomRow ();
		}
	}
}

BOOL CGridControl::InsertBottomRow ()
{
	ROWCOL ilRowCount = GetRowCount ();
	//LockUpdate(TRUE);
	BOOL blRet = InsertRows ( ilRowCount+1, 1 );
	CopyStyleLastLineFromPrev ();
	//LockUpdate(FALSE);
	return blRet;
}

void CGridControl::CopyStyleOfLine ( ROWCOL ipSourceLine, ROWCOL ipDestLine,
									 bool bpResetValues/*=true*/ )
{
	ROWCOL ilColCount = GetColCount ();
	CGXStyle olStyle;

	for ( ROWCOL i=0; i<=ilColCount; i++ )
	{
		ComposeStyleRowCol( ipSourceLine, i, &olStyle );
		if ( bpResetValues )
		{
			olStyle.SetIncludeItemDataPtr ( FALSE );
			if ( i>0 )
				olStyle.SetInterior(WHITE);
		}
		SetStyleRange  ( CGXRange ( ipDestLine, i), olStyle );
		if ( bpResetValues )
		{
			SetValueRange ( CGXRange ( ipDestLine, i), "" );
		}
	}
}

//**************************************************************************************************
// 
//**************************************************************************************************

void CGridControl::CopyStyleLastLineFromPrev ( bool bpResetValues/*=true*/ )
{
	ROWCOL ilRowCount = GetRowCount ();
	CopyStyleOfLine ( ilRowCount-1, ilRowCount, bpResetValues );
}

//**************************************************************************************************
// 
//**************************************************************************************************

BOOL CGridControl::OnRButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	BOOL ok = CGXGridWnd::OnRButtonDblClkRowCol(nRow, nCol, nFlags, pt);
	
	int		*ilWidths;
	ROWCOL  ilRowAnz = GetColCount ();
	
	ilWidths = new int[ilRowAnz+1];
	if ( ilWidths != 0 )
	{
		for ( ROWCOL i=0; i<=ilRowAnz; i++ )
		{
			ilWidths[i] = GetColWidth(i);
		}

 		delete ilWidths;
	}
	return ok;
}

//**************************************************************************************************
// 
//**************************************************************************************************

void CGridControl::OnLButtonUp (UINT nFlags, CPoint point) 
{
	CGXGridWnd::OnLButtonUp(nFlags, point);

    ROWCOL  Row;
	ROWCOL  Col;

	if ( !bmSortEnabled )
		return;
	//--- Betroffene Zelle ermitten
    if ( HitTest ( point, &Row,  &Col, NULL ) != GX_HEADERHIT )
		return;

	//--- Check, ob ung�ltige Spaltennummer
	if ( ( Col > GetColCount( ) ) || ( Col== 0 ) )
		return;
	SortTable ( Row, Col );
}

//**************************************************************************************************
// Such die Datenpointer der ersten Zeile nach der �bergebenden Urno ab.
// R�ckgabe: Row in der die Urno gefunden wurde
//**************************************************************************************************

int CGridControl::FindRowByUrnoDataPointer(long lpUrno)
{
	CGXStyle		olStyle;
	// Alle Rows durchgehen
	for (int i=0; i < GetRowCount();i++)
	{
		// Style und damit Datenpointer einer Zelle erhalten
		ComposeStyleRowCol(i+1,1, &olStyle );

		if (lpUrno == (long) olStyle.GetItemDataPtr())
			return i+1;
	}

	// Nicht gefunden.
	return -1;
}

//**************************************************************************************************
// Von ADO: Sendet ATM ein Drag Begin Event wenn DnD eingeschaltet ist !
//**************************************************************************************************

void CGridControl::OnLButtonDown (UINT nFlags, CPoint point) 
{
	ROWCOL Row;
	ROWCOL Col;

	// Betroffene Zelle ermitten
    int ilHitState = HitTest ( point, &Row,  &Col, NULL );

	// Check, ob ung�ltige Spaltennummer
	if ((Col>GetColCount()) || (Row>GetRowCount()))
		return;

	// Info Struktur f�llen
	GRIDNOTIFY rlNotify;
	rlNotify.idc = umID;
	rlNotify.row = Row;
	rlNotify.col = Col;
	rlNotify.headerrows = GetHeaderRows();
	rlNotify.headercols = GetHeaderCols();
	rlNotify.point = point;
	rlNotify.source = this;

	if (bmIsDnDEnabled)
	{
		// DnD ist nur aus Datenzeilen erlaubt
		if ( (Col==0) || (Row==0) )
			return;

		// "Nutzwert" in die Info Struktur einf�gen
		if (GetRowCount() >= 1)
			rlNotify.value1 = GetValue(Row,1);

		CGXStyle olStyle;

		// Style und damit Datenpointer einer Zelle erhalten
		ComposeStyleRowCol(Row,1, &olStyle );

		// Urno erhalten
		rlNotify.value2 = olStyle.GetItemDataPtr();

		// -> DnD, andere Message senden
		if (ilHitState==GX_CELLHIT)
		{
			// Message senden
			if (pomWnd)
			{
				if (bmDeleteSelection == true)
				{
					// Bisherige Auswahl l�schen.
					SetSelection(NULL);

					// neue Auswahl markieren
					POSITION area = GetParam( )->GetRangeList( )->AddTail(new CGXRange);
					SetSelection (area, Row, 1, Row, GetColCount());
				}

				pomWnd->SendMessage (WM_GRID_DRAGBEGIN, nFlags, (LPARAM)&rlNotify);
			}
		}
	}
	else
	{
		// -> Kein DnD, normale Funktion
		CGXGridWnd::OnLButtonDown(nFlags, point);

		// CheckBox-Event?
		const CGXStyle& olStyle = LookupStyleRowCol(Row,Col);
		if (olStyle.GetControl() == GX_IDS_CTRL_CHECKBOX3D)
		{
			// yo -> toggle checkbox
			if (GetValueRowCol(Row,Col) == "0")
				SetValueRange(CGXRange(Row,Col), "1");
			else
				SetValueRange(CGXRange(Row,Col), "0");
		}

		pomWnd->SendMessage ( WM_GRID_LBUTTONDOWN, nFlags, (LPARAM)&rlNotify );
	}
}

//**************************************************************************************************
// 
//**************************************************************************************************

bool CGridControl::SortTable (ROWCOL ipRowClicked, ROWCOL ipColClicked)
{
	CWaitCursor olDummy;

	ROWCOL ilRowCount = GetRowCount ();

	if (ipRowClicked != 0)
	{
		return false;
	}

	// toggle between sorting in ascending / descending order with each click
	bmSortAscend = !bmSortAscend;
	omSortInfo[0].sortOrder = bmSortAscend ? CGXSortInfo::ascending : CGXSortInfo::descending;
	omSortInfo[0].nRC = ipColClicked;
	omSortInfo[0].sortType = CGXSortInfo::autodetect;
	if (bmAutoGrow && GetValueRowCol (ilRowCount, 1).IsEmpty ())
	{
		ilRowCount--;
	}

	// do the sorting
	SortRows (CGXRange().SetRows(1, ilRowCount), omSortInfo);

	return true;
}

//**************************************************************************************************
// 
//**************************************************************************************************

void CGridControl::EnableSorting ( bool enable/*=true*/ )
{
	bmSortEnabled = enable;
}

//**************************************************************************************************
// 
//**************************************************************************************************

void CGridControl::SetLbDblClickAction ( UINT ipMsg, WPARAM ipWparam, bool bpOnlyInside/*=true*/ )
{
	smLButtonDblClick.bOnlyInside = bpOnlyInside;
	smLButtonDblClick.iMsg = ipMsg;
	smLButtonDblClick.iWparam = ipWparam;
}

//**************************************************************************************************
// 
//**************************************************************************************************

BOOL CGridControl::OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	if ( smLButtonDblClick.iMsg )
	{
		pomWnd->PostMessage ( smLButtonDblClick.iMsg, smLButtonDblClick.iWparam, 0L );
		return TRUE;
	}
	else
	{
		GRIDNOTIFY rlNotify;
		rlNotify.idc = umID;
		rlNotify.row = nRow;
		rlNotify.col = nCol;
		rlNotify.headerrows = GetHeaderRows();
		rlNotify.headercols = GetHeaderCols();
		rlNotify.point = pt;
		pomWnd->SendMessage ( WM_GRID_LBUTTONDBLCLK, nFlags, (LPARAM)&rlNotify );
		return TRUE;
	}
}

//**************************************************************************************************
// 
//**************************************************************************************************

BOOL CGridControl::OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	CGXGridWnd::OnLButtonClickedRowCol(nRow,nCol,nFlags,pt);

	GRIDNOTIFY rlNotify;
	rlNotify.idc = umID;
	rlNotify.row = nRow;
	rlNotify.col = nCol;
	rlNotify.headerrows = GetHeaderRows();
	rlNotify.headercols = GetHeaderCols();
	rlNotify.point = pt;
	pomWnd->SendMessage ( WM_GRID_LBUTTONCLK, nFlags, (LPARAM)&rlNotify );
	return TRUE;
}

//**************************************************************************************************
// 
//**************************************************************************************************

void CGridControl::OnTextNotFound(LPCTSTR)
{
	ROWCOL		ilRow, ilCol, ilColAnz;
	BOOL		blFound = FALSE;
	ilColAnz = GetColCount();
	GX_FR_STATE	*pslState = GXGetLastFRState();

	if ( GetCurrentCell( &ilRow, &ilCol ) && pslState )
	{
		if ( pslState->bNext )		//  d.h Abw�rts suchen
		{
			while ( !blFound && ( ilCol < ilColAnz ) ) //  und noch nicht in letzter Spalte
			{
				ilCol ++;
				ilRow = GetHeaderRows ()+1;
				blFound = FindText( ilRow, ilCol, true ) ;
			}
		}
		else 
		{	//  d.h Aufw�rts suchen
			while ( !blFound &&  ( ilCol > GetHeaderCols () + 1 ) )	
			{
				ilCol --;
				ilRow = GetRowCount ();
				blFound = FindText( ilRow, ilCol, true );
			}
		}
		if ( blFound )
			return;

	}
	MessageBeep(0);
}

//**************************************************************************************************
// 
//**************************************************************************************************

void CGridControl::SetDirtyFlag ( bool dirty/*=true*/ )
{
	bmIsDirty = dirty;

}

//**************************************************************************************************
// 
//**************************************************************************************************

bool CGridControl::IsGridDirty ()
{
	return bmIsDirty ;
}

//**************************************************************************************************
// 
//**************************************************************************************************

BOOL CGridControl::OnDeleteCell(ROWCOL nRow, ROWCOL nCol)
{
	CGXControl	*polControl;
	CString		olWert;
	BOOL		ilRet = FALSE;
	ROWCOL		ilFrozenRows = GetFrozenRows();
	ROWCOL		ilFrozenCols = GetFrozenCols(); 

	bool blfrozen = ( nRow <= ilFrozenRows ) || ( nCol <= ilFrozenCols ) ;

	//  event. Selection auf Row- und Columnheadern entfernen
	SelectRange( CGXRange ().SetRows(0,ilFrozenRows), FALSE );
	SelectRange( CGXRange ().SetCols(0,ilFrozenCols), FALSE );

	//  nur f�r Zellen innerhalb des Grid, alten Wert merken
	if ( !blfrozen )
	{
		polControl = GetControl(nRow, nCol);
		if ( !polControl  || !polControl->GetValue(olWert) )
			olWert = "XXX";
		ilRet = CGXGridWnd::OnDeleteCell(nRow, nCol);
	}
	bmIsDirty |= ( olWert.IsEmpty()==FALSE) ;
	return ilRet&&!blfrozen;
}

//**************************************************************************************************
// 
//**************************************************************************************************

BOOL CGridControl::GetStyleRowCol(ROWCOL nRow, ROWCOL nCol, CGXStyle& style, GXModifyType mt, int nType)
{
	bool blReturn;

	// Basisklasse
	blReturn = CGXGridWnd::GetStyleRowCol(nRow,nCol,style,mt,nType ) != 0;

	// Tooltip nur f�r Spalte 1, Array mu� vorhanden sein
	if ((nCol == 1) && blReturn && (bmShowExtraToolTip) && (nRow != 0))
	{
		CGXStyle olStyle;
		ComposeStyleRowCol(nRow, GetColCount(), &olStyle);
			
		CString olText = olStyle.GetValue();
		style.SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT,olText);
	}
	return blReturn;
}

// ein Bug in CGXGridCore - m_SelRectId bleibt leer nach SetSelection() - das knallt irgendwann!
void CGridControl::SetSelection(POSITION pos, ROWCOL nTop, ROWCOL nLeft, ROWCOL nBottom, ROWCOL nRight)
{
	CGXGridWnd::SetSelection(pos, nTop, nLeft, nBottom, nRight);
	m_SelRectId = pos;
}

/*int CGridControl::GetColWidth(ROWCOL nCol)
{
	int nWidth = CGXGridCore::GetColWidth(nCol);
   // Make the last column fill the rest of the window so that
   // no empty space is visible.
	if (!IsPrinting() && nCol == GetColCount()){
		CRect rect = GetGridRect();
		nCol = GetClientCol(nCol);
        if (nCol > 0)
			nWidth = max(nWidth, rect.Width() - CalcSumOfClientColWidths(0, nCol-1) - 1);
    }
	return nWidth;
}*/

//////////////////////////////////////////////////////////////////////
//	Klasse f�r ein Grid mit �berschrift
//  Baseclass:	CGridControl
//////////////////////////////////////////////////////////////////////

CTitleGridControl::CTitleGridControl()
{
}


CTitleGridControl::CTitleGridControl(CWnd *popParent, UINT nID, ROWCOL nCols, ROWCOL nRows )
	:CGridControl ( popParent, nID, nCols, nRows+1 )
{
	
	// Assign a combo box to a cell
	BOOL ok ;
	CGXStyle style;
	
	SetStyleRange(CGXRange(0,0), 
				  CGXStyle().SetControl(GX_IDS_CTRL_HEADER)  );
	ok = SetFrozenRows ( 1,1 );
	ChangeRowHeaderStyle ( CGXStyle().SetValue("")  );
	ok = SetCoveredCellsRowCol( 0, 0, 0, nCols );
	SetRowHeadersText ();
}

CTitleGridControl::~CTitleGridControl()
{
}


void CTitleGridControl::SetTitle ( CString &opTitle )
{	
	omTitle = opTitle;
	SetValueRange( CGXRange(0,0), omTitle );
}

CString CTitleGridControl::GetTitle ()
{
	return omTitle ;
}

const CString &CTitleGridControl::GetValue ( ROWCOL nRow, ROWCOL nCol ) 
{
	return GetValue ( nRow+1, nCol );	
}

BOOL CTitleGridControl::SetValue ( ROWCOL nRow, ROWCOL nCol, CString &cStr ) 
{
	return SetValueRange( CGXRange(nRow+1, nCol), cStr );
}	


BOOL CTitleGridControl::EnableGrid ( BOOL enable )
{
	BOOL blRet = false;
	
	int ilRowCount = GetRowCount();
	int ilColCount = GetColCount();
	
	if (ilRowCount >= 2 && ilColCount >= 1)
	{
		if ( enable )
			blRet = SetStyleRange(CGXRange(2, 1, ilRowCount, ilColCount), CGXStyle().SetInterior(ogColors[WHITE_IDX]).SetEnabled(enable));
		else
		{
			SetCurrentCell( 1, 0  ) ;
			blRet = SetStyleRange(CGXRange(2, 1, ilRowCount, ilColCount), CGXStyle().SetInterior(ogColors[SILVER_IDX]).SetEnabled(enable));
		}
		//blRet &= SetStyleRange(CGXRange().SetTable(), CGXStyle().SetEnabled(enable));
			
	}
	Redraw();
	return blRet;
}


BOOL CTitleGridControl::InsertBottomRow ()
{
	BOOL blRet = CGridControl::InsertBottomRow ();
	char pclString[11];
	ROWCOL ilRowCount = GetRowCount ();

	CString olStr = itoa(ilRowCount-1,pclString,10);
	SetValue ( ilRowCount-1, 0, olStr) ;

	return blRet;
}


bool CTitleGridControl::SortTable ( ROWCOL ipRowClicked, ROWCOL ipColClicked )
{
	ROWCOL ilRowCount = GetRowCount ();

	if ( ipRowClicked != 1 )
		return false;

	//--- Sortieren je nachdem welcher Header gedr�ckt wurde
	CGXSortInfoArray  sortInfo;
	sortInfo.SetSize( 1 );
		
	// toggle between sorting in ascending / descending order with each click
	bmSortAscend = !bmSortAscend;
	sortInfo[0].sortOrder = bmSortAscend ? CGXSortInfo::ascending : 
										   CGXSortInfo::descending;
	sortInfo[0].nRC = ipColClicked;                       
	sortInfo[0].sortType = CGXSortInfo::autodetect;  
	if ( bmAutoGrow && GetValueRowCol ( ilRowCount, 1 ).IsEmpty () )
		ilRowCount--;
	if ( ilRowCount > 2 )
		SortRows( CGXRange().SetRows(2, ilRowCount), sortInfo); 
	SetRowHeadersText ();
	return true;
}

//  Zeilek�pfe neu beschriften
void CTitleGridControl::SetRowHeadersText ()
{
	char pclString[11];
	CString olStr;
	for ( UINT i=1; i<GetRowCount(); i++ )
	{
		olStr = itoa(i,pclString,10);
		SetValue ( i, 0, olStr);
	}
}

void CTitleGridControl::RemoveOneRow ( ROWCOL nRow )
{
	RemoveRows( nRow, nRow );
	SetRowHeadersText ();
}

BOOL CTitleGridControl::OnStartEditing(ROWCOL nRow, ROWCOL nCol)
{
	if ( (nRow<2) || (nCol<1) )
	{
		TRACE("User tried to edit in Row=%u, Col=%u\n", nRow, nCol );
		return FALSE;
	}
	else
		return CGridControl::OnStartEditing(nRow, nCol);
}

//////////////////////////////////////////////////////////////////////
//	Klasse f�r das CDutyAbsenceOverview Grid
//  Baseclass:	CGridControl
//////////////////////////////////////////////////////////////////////

CDutyAbsenceOverviewGridControl::CDutyAbsenceOverviewGridControl()
{
	this->EnableSorting(false);
	this->EnableAutoGrow(false);
}


CDutyAbsenceOverviewGridControl::CDutyAbsenceOverviewGridControl(CWnd *popParent, UINT nID, ROWCOL nCols, ROWCOL nRows )
	:CGridControl ( popParent, nID, nCols, nRows )
{
	this->EnableSorting(false);
	this->EnableAutoGrow(false);
}

CDutyAbsenceOverviewGridControl::~CDutyAbsenceOverviewGridControl()
{
}


BOOL CDutyAbsenceOverviewGridControl::OnStartSelection(ROWCOL nRow, ROWCOL nCol, UINT flags, CPoint point)
{	
	CGXStyle olStyle;
	this->ComposeStyleRowCol(nRow,nCol,&olStyle);
	TRACE ( "\nOnStartSelection Row<%d> Col<%d>", nRow, nCol );
	return olStyle.GetIncludeItemDataPtr() && olStyle.GetItemDataPtr();
}

BOOL CDutyAbsenceOverviewGridControl::CanChangeSelection(CGXRange* pRange, BOOL bIsDragging, BOOL bKey)
{
	if (pRange)
	{
		CGXRange olRange(pRange);
		ROWCOL nRow,nCol;
		BOOL blOK = olRange.GetFirstCell(nRow,nCol);
		while(blOK)
		{
			CGXStyle olStyle;
			this->ComposeStyleRowCol(nRow,nCol,&olStyle);
			if (!olStyle.GetIncludeItemDataPtr() ||  !olStyle.GetItemDataPtr())
			{
				return FALSE;
			}

			TRACE ( "\nCanChangeSelection Row<%d> Col<%d>", nRow, nCol );
			blOK = olRange.GetNextCell(nRow,nCol);
		}
	}

	return TRUE;
}


BOOL CDutyAbsenceOverviewGridControl::OnLButtonHitRowCol(ROWCOL nHitRow, ROWCOL nHitCol, ROWCOL nDragRow, ROWCOL nDragCol, CPoint point, UINT flags, WORD nHitState)
{
	static CGXRangeList* selList = NULL;

	BOOL blHitInCell = nHitState & GX_HITINCELL;


	if (blHitInCell && nHitState & GX_HITSTART)
	{
		delete selList;
		selList = new CGXRangeList;
		CopyRangeList(*selList, FALSE);
	}

	if (blHitInCell && nHitState & GX_HITEND)
	{
		BOOL blSelected = selList->IsCellInList(nHitRow,nHitCol);
		if (blSelected)
		{
			if (flags & MK_CONTROL)
			{
				this->SelectRange(CGXRange(nHitRow,nHitCol),FALSE,TRUE);	// unselect			
				return TRUE;
			}
			else
			{
				return CGXGridWnd::OnLButtonHitRowCol(nHitRow,nHitCol,nDragRow,nDragCol,point,flags,nHitState);
			}

		}
	}

	return CGXGridWnd::OnLButtonHitRowCol(nHitRow,nHitCol,nDragRow,nDragCol,point,flags,nHitState);
}

BOOL CDutyAbsenceOverviewGridControl::GetStyleRowCol(ROWCOL nRow,ROWCOL nCol,CGXStyle& style,GXModifyType mt,int nType)
{
	BOOL bRet = CGXGridCore::GetStyleRowCol(nRow,nCol,style,mt,nType);
	if (nType == 0 && GetInvertStateRowCol(nRow,nCol,GetParam()->GetRangeList()))
	{
		style.SetInterior(DT_LIGHTBLUE);
		bRet = TRUE;
	}

	return bRet;
}

void CDutyAbsenceOverviewGridControl::DrawInvertCell(CDC *pDC,ROWCOL nRow,ROWCOL nCol,CRect rectItem)
{
	if (m_nNestedDraw == 0)
	{
		CGXRange range;
		if (GetCoveredCellsRowCol(nRow,nCol,range))
			rectItem = CalcRectFromRowCol(range.top,range.left,range.bottom,range.right);
		InvalidateRect(&rectItem);
	}
}
