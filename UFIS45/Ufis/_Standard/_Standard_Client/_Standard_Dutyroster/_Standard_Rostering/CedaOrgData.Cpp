// CedaOrgData.cpp
 
#include <stdafx.h>


void ProcessOrgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaOrgData::CedaOrgData() : CedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(ORGDATA, OrgDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Dpt1,"DPT1")
		FIELD_CHAR_TRIM	(Dpt2,"DPT2")
		FIELD_CHAR_TRIM	(Dptn,"DPTN")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
	END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(OrgDataRecInfo)/sizeof(OrgDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&OrgDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"ORG");
    sprintf(pcmListOfFields,"CDAT,DPT1,DPT2,DPTN,LSTU,PRFL,REMA,URNO,USEC,USEU");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
	Register();
}

//---------------------------------------------------------------------------------------------------------

void CedaOrgData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaOrgData::Register(void)
{
	DdxRegister((void *)this,BC_ORG_CHANGE,	"ORGDATA", "Org-changed",	ProcessOrgCf);
	DdxRegister((void *)this,BC_ORG_NEW,	"ORGDATA", "Org-new",		ProcessOrgCf);
	DdxRegister((void *)this,BC_ORG_DELETE,	"ORGDATA", "Org-deleted",	ProcessOrgCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaOrgData::~CedaOrgData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaOrgData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omDpt1Map.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaOrgData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omDpt1Map.RemoveAll();
    omData.DeleteAll();
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{	
		ilRc = CedaAction2("RT");
	}
	else
	{
		ilRc = CedaAction2("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
	{
		ORGDATA *prlOrg = new ORGDATA;
		if ((ilRc = GetFirstBufferRecord2(prlOrg)) == true)
		{
			omData.Add(prlOrg);//Update omData
			omUrnoMap.SetAt((void *)prlOrg->Urno,prlOrg);
			omDpt1Map.SetAt((CString)prlOrg->Dpt1,prlOrg);
#ifdef TRACE_FULL_DATA
			// Datensatz OK, loggen if FULL
			ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
			GetDataFormatted(ogRosteringLogText, prlOrg);
			WriteLogFull("");
#endif TRACE_FULL_DATA
		}
		else
		{
			delete prlOrg;
		}
	}
    
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}

	return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaOrgData::Insert(ORGDATA *prpOrg)
{
	prpOrg->IsChanged = DATA_NEW;
	if(Save(prpOrg) == false) return false; //Update Database
	InsertInternal(prpOrg);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaOrgData::InsertInternal(ORGDATA *prpOrg)
{
	ogDdx.DataChanged((void *)this, ORG_NEW,(void *)prpOrg ); //Update Viewer
	omData.Add(prpOrg);//Update omData
	omUrnoMap.SetAt((void *)prpOrg->Urno,prpOrg);
	omDpt1Map.SetAt((CString)prpOrg->Dpt1,prpOrg);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaOrgData::Delete(long lpUrno)
{
	ORGDATA *prlOrg = GetOrgByUrno(lpUrno);
	if (prlOrg != NULL)
	{
		prlOrg->IsChanged = DATA_DELETED;
		if(Save(prlOrg) == false) return false; //Update Database
		DeleteInternal(prlOrg);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaOrgData::DeleteInternal(ORGDATA *prpOrg)
{
	ogDdx.DataChanged((void *)this,ORG_DELETE,(void *)prpOrg); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpOrg->Urno);
	omDpt1Map.RemoveKey((CString)prpOrg->Dpt1);
	int ilOrgCount = omData.GetSize();
	for (int ilCountRecord = 0; ilCountRecord < ilOrgCount; ilCountRecord++)
	{
		if (omData[ilCountRecord].Urno == prpOrg->Urno)
		{
			omData.DeleteAt(ilCountRecord);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaOrgData::Update(ORGDATA *prpOrg)
{
	if (GetOrgByUrno(prpOrg->Urno) != NULL)
	{
		if (prpOrg->IsChanged == DATA_UNCHANGED)
		{
			prpOrg->IsChanged = DATA_CHANGED;
		}
		if(Save(prpOrg) == false) return false; //Update Database
		UpdateInternal(prpOrg);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaOrgData::UpdateInternal(ORGDATA *prpOrg)
{
	ORGDATA *prlOrg = GetOrgByUrno(prpOrg->Urno);
	if (prlOrg != NULL)
	{
		omDpt1Map.RemoveKey((CString)prlOrg->Dpt1);
		*prlOrg = *prpOrg; //Update omData
		omDpt1Map.SetAt((CString)prlOrg->Dpt1,prlOrg);
		ogDdx.DataChanged((void *)this,ORG_CHANGE,(void *)prlOrg); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

ORGDATA *CedaOrgData::GetOrgByUrno(long lpUrno)
{
	ORGDATA  *prlOrg;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlOrg) == TRUE)
	{
		return prlOrg;
	}
	return NULL;
}

//--GET-BY-DPT1--------------------------------------------------------------------------------------------

ORGDATA *CedaOrgData::GetOrgByDpt1(CString opDpt1)
{
	ORGDATA  *prlOrg;
	if (omDpt1Map.Lookup(opDpt1,(void *& )prlOrg) == TRUE)
	{
		return prlOrg;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaOrgData::ReadSpecialData(CCSPtrArray<ORGDATA> *popOrg,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS","ORG",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT","ORG",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popOrg != NULL)
	{
		for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			ORGDATA *prpOrg = new ORGDATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prpOrg,CString(pclFieldList))) == true)
			{
				popOrg->Add(prpOrg);
			}
			else
			{
				delete prpOrg;
			}
		}
		if(popOrg->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaOrgData::Save(ORGDATA *prpOrg)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpOrg->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpOrg->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpOrg);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpOrg->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpOrg->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpOrg);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpOrg->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpOrg->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessOrgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogOrgData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaOrgData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlOrgData;
	prlOrgData = (struct BcStruct *) vpDataPointer;
	ORGDATA *prlOrg;
	long llUrno;
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlOrgData->Cmd, prlOrgData->Object, prlOrgData->Twe, prlOrgData->Selection, prlOrgData->Fields, prlOrgData->Data,prlOrgData->BcNum);
		WriteInRosteringLog();
	}

	switch(ipDDXType)
	{
	default:
		break;
	case BC_ORG_CHANGE:
		llUrno = GetUrnoFromSelection(prlOrgData->Selection);
		prlOrg = GetOrgByUrno(llUrno);
		if(prlOrg != NULL)
		{
			GetRecordFromItemList(prlOrg,prlOrgData->Fields,prlOrgData->Data);
			UpdateInternal(prlOrg);
			break;
		}
	case BC_ORG_NEW:
		prlOrg = new ORGDATA;
		GetRecordFromItemList(prlOrg,prlOrgData->Fields,prlOrgData->Data);
		InsertInternal(prlOrg);
		break;
	case BC_ORG_DELETE:
		CString olSelection = (CString)prlOrgData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlOrgData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlOrg = GetOrgByUrno(llUrno);
		if (prlOrg != NULL)
		{
			DeleteInternal(prlOrg);
		}
		break;
	}
}

//---------------------------------------------------------------------------------------------------------
