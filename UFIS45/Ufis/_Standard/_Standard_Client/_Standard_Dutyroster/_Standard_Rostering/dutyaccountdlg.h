#ifndef AFX_DUTYACCOUNTDLG_H__FA6683B1_DDEB_11D2_8E91_00001C034EA0__INCLUDED_
#define AFX_DUTYACCOUNTDLG_H__FA6683B1_DDEB_11D2_8E91_00001C034EA0__INCLUDED_

// DutyAccountDlg.h : Header-Datei
//
#include <stdafx.h>
/*#include "resource.h"
#include <DutyRoster_View.h>
#include <CCSEdit.h>*/

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld DutyAccountDlg 

struct ACCOUNTS 
{
	long	lTYPE;		// Interner KontoNummer
	CString	oNAME;		// Name des Kontos
	CString oSNAM;		// Kurzname des Kontos
	CString oKTYP;		// Konten Typ (D=Dynamisch, S=Statisch)
	CString oFTYP;		// Format Type (D=Tage, H=Stunden, M=Minuten, T=Text, N=Zahlen)
	CString oMaskType;	// wie soll der Dlg. aussehen
	int iForm;			// Nachkommastellen
	bool bEDIT;			// Ist das Konto Editierbar
	CString oDPER;// Y=Jahr | Q=Quartal | M=Monat | S=Singel (ACC.CO01)

	CString oBCDTable;	// mit welscher Tabelle soll der Feldinhald gepr�ft werden 
	CString oBCDField;	// mit welschem Feld aus der Tabelle soll der Feldinhald gepr�ft werden

	ACCOUNTS(void)
	{
		lTYPE		= 0;
		iForm		= 0;
		oNAME		= "";
		oSNAM		= "";
		oKTYP		= "";
		oFTYP		= "";
		oMaskType	= "";
		oDPER		= "";
		oBCDTable	= "";
		oBCDField	= "";
		bEDIT		= false;
	}
};


class DutyAccountDlg : public CDialog
{
// Konstruktion
public:
	DutyAccountDlg(long lpStfUrno, CString opYear, CWnd* pParent = NULL);   // Standardkonstruktor
	~DutyAccountDlg();

private:
	DutyRoster_View *pomParent;
	VIEWINFO *prmViewInfo;
	long lmStfUrno;
	CString omYear;
	CString omType;
	CString omMaskPeriod;
	int imSelectAccountLine;
	
	ACCDATA *prmAccOrig;
	ACCDATA *prmAccTemp;
	STFDATA *prmStf;
	CCSPtrArray<ACCOUNTS> omAccountList;
	ACCOUNTS rmAccount;

	CString ogOldFumo;
	int ipCountMakeAccountMask;
public:

// Dialogfelddaten
	//{{AFX_DATA(DutyAccountDlg)
	enum { IDD = IDD_DUTY_ACCOUNT_DLG };
	CButton	m_C01;
	CButton	m_C02;
	CButton	m_C03;
	CButton	m_C04;
	CButton	m_C05;
	CButton	m_C06;
	CButton	m_C07;
	CButton	m_C08;
	CButton	m_C09;
	CButton	m_C10;
	CButton	m_C11;
	CButton	m_C12;
	CEdit	m_Co01Text;
	CStatic	m_SCo01Text;
	CStatic	m_SCl01;
	CStatic	m_SCl02;
	CStatic	m_SCl03;
	CStatic	m_SCl04;
	CStatic	m_SCl05;
	CStatic	m_SCl06;
	CStatic	m_SCl07;
	CStatic	m_SCl08;
	CStatic	m_SCl09;
	CStatic	m_SCl10;
	CStatic	m_SCl11;
	CStatic	m_SCl12;
	CStatic	m_SCo01;
	CStatic	m_SCo02;
	CStatic	m_SCo03;
	CStatic	m_SCo04;
	CStatic	m_SCo05;
	CStatic	m_SCo06;
	CStatic	m_SCo07;
	CStatic	m_SCo08;
	CStatic	m_SCo09;
	CStatic	m_SCo10;
	CStatic	m_SCo11;
	CStatic	m_SCo12;
	CStatic	m_SOp01;
	CStatic	m_SOp02;
	CStatic	m_SOp03;
	CStatic	m_SOp04;
	CStatic	m_SOp05;
	CStatic	m_SOp06;
	CStatic	m_SOp07;
	CStatic	m_SOp08;
	CStatic	m_SOp09;
	CStatic	m_SOp10;
	CStatic	m_SOp11;
	CStatic	m_SOp12;
	CCSEdit m_CheckEdit; //Wichtiges Feld
	CComboBox	m_C_Account;
	CCSEdit	m_Saldo;
	CCSEdit	m_Start;
	CCSEdit	m_Auby;
	CCSEdit	m_Name;
	CCSEdit	m_Peno;
	CCSEdit	m_AatoD;
	CCSEdit	m_AatoT;
	CCSEdit	m_AudaD;
	CCSEdit	m_AudaT;
	CEdit	m_Cl01;
	CEdit	m_Cl02;
	CEdit	m_Cl03;
	CEdit	m_Cl04;
	CEdit	m_Cl05;
	CEdit	m_Cl06;
	CEdit	m_Cl07;
	CEdit	m_Cl08;
	CEdit	m_Cl09;
	CEdit	m_Cl10;
	CEdit	m_Cl11;
	CEdit	m_Cl12;
	CEdit	m_Co01;
	CEdit	m_Co02;
	CEdit	m_Co03;
	CEdit	m_Co04;
	CEdit	m_Co05;
	CEdit	m_Co06;
	CEdit	m_Co07;
	CEdit	m_Co08;
	CEdit	m_Co09;
	CEdit	m_Co10;
	CEdit	m_Co11;
	CEdit	m_Co12;
	CEdit	m_Op01;
	CEdit	m_Op02;
	CEdit	m_Op03;
	CEdit	m_Op04;
	CEdit	m_Op05;
	CEdit	m_Op06;
	CEdit	m_Op07;
	CEdit	m_Op08;
	CEdit	m_Op09;
	CEdit	m_Op10;
	CEdit	m_Op11;
	CEdit	m_Op12;
	//}}AFX_DATA


// �berschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktions�berschreibungen
	//{{AFX_VIRTUAL(DutyAccountDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(DutyAccountDlg)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnButtoninfo();
	afx_msg void OnSelchangeCAccount();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void InternalCancel();
	void GetAcc(long lpAccount = 0);
	void MakeAccountMask(long lpAccount = 0);
	void SetMaskColumn_Op(ACCOUNTS rpAccount, CString opHeaderText, int ipCmdShow = SW_SHOW, BOOL bpReadOnly = TRUE , int ipShowFlag = SW_SHOW);
	void SetMaskColumn_Co(ACCOUNTS rpAccount, CString opHeaderText, int ipCmdShow = SW_SHOW, BOOL bpReadOnly = TRUE , int ipShowFlag = SW_SHOW);
	void SetMaskColumn_Cl(ACCOUNTS rpAccount, CString opHeaderText, int ipCmdShow = SW_SHOW, BOOL bpReadOnly = TRUE , int ipShowFlag = SW_SHOW);
	void SetDummyFields(long lpAccount);
	void ShowStart(bool bpShow = true);
	void ShowSaldo(bool bpShow = true);
	void ShowFreeText(bool bpShow = true);
	bool CheckData(bool blOnOK);
	bool CheckToFtyp_T();
	CString TrimValue(CString opValue, int ipForm = 0);

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio f�gt zus�tzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_DUTYACCOUNTDLG_H__FA6683B1_DDEB_11D2_8E91_00001C034EA0__INCLUDED_
