#if !defined(AFX_DUTYABSENCEOVERVIEW_H__2CC47F33_A35A_11D5_8086_0001022205E4__INCLUDED_)
#define AFX_DUTYABSENCEOVERVIEW_H__2CC47F33_A35A_11D5_8086_0001022205E4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DutyAbsenceOverview.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DutyAbsenceOverview dialog

class DutyAbsenceOverviewDlg : public CDialog
{
// Construction
public:
	DutyAbsenceOverviewDlg(long lpStfUrno,const COleDateTime& ropTimeFrom,const COleDateTime& ropTimeTo,const CString& ropCode,CWnd* pParent,bool bpUpdateActiveDRR,bool bpUpdateVacancyDRR);   // standard constructor
	~DutyAbsenceOverviewDlg();

// Dialog Data
	//{{AFX_DATA(DutyAbsenceOverviewDlg)
	enum { IDD = IDD_DUTY_ABSENCE_OVERVIEW };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

private:	// Helpers
			BOOL InitGrid();
			BOOL FillGrid();
	static	void ProcessDdx(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
			void OnNewDrr(DRRDATA *popDrr);
			void OnChangeDrr(DRRDATA *popDrr);
			void OnDeleteDrr(DRRDATA *popDrr);
			BOOL FindCellByUrnoDataPtr(long lpUrno,ROWCOL& ipRow,ROWCOL& ipCol);
			BOOL GetCurrentCell(ROWCOL *pipRow,ROWCOL *pipCol);

	
private:	// Implementation data
	CDutyAbsenceOverviewGridControl*		pomGrid;
	int					imRowCount;
	int					imColCount;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DutyAbsenceOverview)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DutyAbsenceOverviewDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnApply();
	afx_msg void OnSelectall();
	afx_msg void OnUnselectall();
	afx_msg void OnRBtnSelect();
	afx_msg void OnRBtnUnselect();
	afx_msg void OnRBtnDelete();
	afx_msg void OnRBtnApply();
	afx_msg	long OnLBtnDblClk(WPARAM wParam,LPARAM lParam);	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	long				lmStfUrno;
	COleDateTime		omTimeFrom;
	COleDateTime		omTimeTo;
	COleDateTimeSpan	omTimeSpan;
	CString				omCode;
	bool				bmUpdateActiveDRR;
	bool				bmUpdateVacancyDRR;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DUTYABSENCEOVERVIEW_H__2CC47F33_A35A_11D5_8086_0001022205E4__INCLUDED_)
