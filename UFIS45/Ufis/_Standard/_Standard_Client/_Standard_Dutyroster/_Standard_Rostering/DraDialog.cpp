// DraDialog.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDraDialog 


CDraDialog::CDraDialog(CWnd* pParent,DRADATA* popDraData,DRRDATA* popDrrData,CMapPtrToPtr* popDraMap)
	: CDialog(CDraDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDraDialog)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT

	pomDraData = popDraData;
	pomDrrData = popDrrData;
	pomDraMap = popDraMap;
}


void CDraDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDraDialog)
		DDX_Control(pDX, IDC_TIME_TO, m_TimeTo);
		DDX_Control(pDX, IDC_TIME_FROM, m_TimeFrom);
		DDX_Control(pDX, IDC_DATE_FROM, m_DateFrom);
		DDX_Control(pDX, IDC_DATE_TO, m_DateTo);
		DDX_Control(pDX, IDC_CB_ABS, m_CB_Absence);
		DDX_Control(pDX, IDC_E_REMARK, m_Remark);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDraDialog, CDialog)
	//{{AFX_MSG_MAP(CDraDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDraDialog 

BOOL CDraDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// -------------------------------------------------------------------------------
	// Edit Felder setzen
	// Zeit und Datum setzen
	m_DateFrom.SetTypeToDate( true);
	m_DateFrom.SetBKColor(YELLOW);
	m_DateFrom.SetTextErrColor(RED);
	m_TimeFrom.SetTypeToTime( true);
	m_TimeFrom.SetBKColor(YELLOW);
	m_TimeFrom.SetTextErrColor(RED);

	if (pomDraData->Abfr.GetStatus() != COleDateTime::valid)
	{
		m_DateFrom.SetInitText("");
		m_TimeFrom.SetInitText("");
	}
	else
	{
		m_DateFrom.SetInitText(pomDraData->Abfr.Format("%d.%m.%Y"));
		m_TimeFrom.SetInitText(pomDraData->Abfr.Format("%H:%M"));
	}
	//--------------------------------------------------------
	m_DateTo.SetTypeToDate(true);
	m_DateTo.SetBKColor(YELLOW);
	m_DateTo.SetTextErrColor(RED);
	m_TimeTo.SetTypeToTime(true);
	m_TimeTo.SetBKColor(YELLOW);
	m_TimeTo.SetTextErrColor(RED);
	if( pomDraData->Abto.GetStatus() != COleDateTime::valid)
	{
		m_DateTo.SetInitText("");
		m_TimeTo.SetInitText("");
	}
	else
	{
		m_DateTo.SetInitText(pomDraData->Abto.Format("%d.%m.%Y"));
		m_TimeTo.SetInitText(pomDraData->Abto.Format("%H:%M"));
	}

	// -------------------------------------------------------------------------------
	// Comboboxen f�llen
	int i;
	CString olFormat;
	m_CB_Absence.SetFont(&ogCourier_Regular_8);
	int ilOda = ogOdaData.omData.GetSize();
	for (i = 0; i < ilOda; i++)
	{
		ODADATA *prlOda = &ogOdaData.omData[i];

		// insert absence only if it is not 'regular free' and no 'sleepday'
		if (strcmp(prlOda->Free, "x") != NULL && strcmp(prlOda->Type, "S") != NULL)
		{
			olFormat.Format("%-5.5s %-40.40s",prlOda->Sdac,prlOda->Sdan);
			m_CB_Absence.AddString( olFormat);

			omOdaMap.SetAt(olFormat,prlOda->Sdac);
			
			// Eintrag in LB selektieren
			if (strcmp(pomDraData->Sdac,prlOda->Sdac) == 0)
			{
				m_CB_Absence.SelectString(0,olFormat);
			}
		}
	}

	// Bemerkung
	m_Remark.LimitText(40);
	m_Remark.SetWindowText(pomDraData->Rema);

	SetWindowText(LoadStg(IDC_S_Abwesenheit));
	SetButtonText();
	SetStatic();

	return TRUE;  
}

//*********************************************************************************
//
//*********************************************************************************

void CDraDialog::OnOK() 
{
	// Between memory for values
	DRADATA olTempDra;
	CString olStringValue,olStringResult;

	// Values read
	GetTimeData(olTempDra);

	// G�ltigkeit pr�fen
	if (!IsInputValid(olTempDra.Abfr,olTempDra.Abto))
		return;

	// Abwesenheits-Codes auslesen
	m_CB_Absence.GetWindowText(olStringValue);

	if (omOdaMap.Lookup( olStringValue, olStringResult))
	{
		//strcpy(olTempDra.Sdac, olStringResult.GetBuffer(0));
		ODADATA *prlOdaData = NULL;
		
		prlOdaData = ogOdaData.GetOdaBySdac(olStringResult);
		if (prlOdaData != NULL)
		{
			strcpy(olTempDra.Sdac, prlOdaData->Sdac);
		      
			olTempDra.Bsdu = prlOdaData->Urno;
		}
	}

	// Bemerkungen
	m_Remark.GetWindowText(olStringValue);
	// Maximal 40 Zeichen
	olStringValue = olStringValue.Left(40);
	strcpy(olTempDra.Rema, olStringValue.GetBuffer(0));

	// Apply Changes
	ogDraData.CopyDra(&olTempDra,pomDraData);

	CDialog::OnOK();
}

//********************************************************************************
// GetTimeData: liest die Datums- und Zeitwerte aus den Editcontrols und
//	speichert sie in <opTargetDra>.
// R�ckgabe:	keine
//********************************************************************************

void CDraDialog::GetTimeData(DRADATA &opTargetDra)
{
CCS_TRY;
	// Zeiten formatieren
	CString olDateFrom,olDateTo,olTimeFrom,olTimeTo,olFormatStringFrom,olFormatStringTo;
	COleDateTime olOleTimeTo,olOleTimeFrom;

	m_DateFrom.GetWindowText(olDateFrom);
	m_DateTo.GetWindowText(olDateTo);
	m_TimeFrom.GetWindowText(olTimeFrom);
	m_TimeTo.GetWindowText(olTimeTo);

	// Ver�ndert auf Format dd.mm.yyyy
	CheckDDMMYYValid(olDateFrom);
	CheckDDMMYYValid(olDateTo);

	// Alle Extra Zeichen (.:) l�schen.
	CedaDataHelper::DeleteExtraChars(olDateFrom);
	CedaDataHelper::DeleteExtraChars(olTimeFrom);
	CedaDataHelper::DeleteExtraChars(olDateTo);
	CedaDataHelper::DeleteExtraChars(olTimeTo);

	// Datum und Zeit "von" formatieren	
	olFormatStringFrom = olDateFrom.Right(4) + olDateFrom.Mid(2,2) + olDateFrom.Left(2) + olFormatStringFrom += olTimeFrom.Left(2) + olTimeFrom.Right(2) +"00";

	// Datum und Zeit "bis" formatieren
	olFormatStringTo = olDateTo.Right(4) + olDateTo.Mid(2,2) + olDateTo.Left(2) + olTimeTo.Left(2) + olTimeTo.Right(2) +"00";

	// String in OleDateTime umwandeln, und bei Erfolg in die Struktur kopieren
	CedaDataHelper::DateTimeStringToOleDateTime(olFormatStringFrom,olOleTimeFrom);
	CedaDataHelper::DateTimeStringToOleDateTime(olFormatStringTo,olOleTimeTo);
	// Zeiten eintragen
	opTargetDra.Abfr = olOleTimeFrom;
	opTargetDra.Abto = olOleTimeTo;
CCS_CATCH_ALL;
}

//********************************************************************************
// Eingabe auf G�ltigkeit pr�fen
//********************************************************************************

bool CDraDialog::IsInputValid(COleDateTime opTimeFrom,COleDateTime opTimeTo)
{
	bool blResult = true;
	CString olErrorString,olStringValue;
	// G�ltigkeit testen, und Fehlermeldung ausgeben
	if(	!m_DateTo.GetStatus()	|| 
		!m_TimeTo.GetStatus()	||
		!m_DateFrom.GetStatus()	|| 
		!m_TimeFrom.GetStatus())
	{
		olErrorString += LoadStg(IDS_STRING1855);
		blResult = false;
	}
	// Zeiteingabe ist g�ltig, weiter pr�fen
	else
	{
		// Pr�fen Ist von kleiner als bis ?
		if (opTimeFrom >= opTimeTo)
		{
			olErrorString += LoadStg(IDS_STRING1856);
			blResult = false;
		}

		// Zeit in der unteren Grenze der Schicht ?
		if (opTimeFrom < pomDrrData->Avfr)
		{
			olErrorString += LoadStg(IDS_STRING1857);
			blResult = false;
		}

		// Zeit in der oberen Grenze der Schicht ?
		if (opTimeTo > pomDrrData->Avto)
		{
			olErrorString += LoadStg(IDS_STRING1858);
			blResult = false;
		}

		// Absences may not overlap
		if (!ogDraData.IsValidDra(opTimeFrom,opTimeTo,pomDraData->Urno,pomDraMap))
		{
			olErrorString += LoadStg(IDS_STRING1880);
			blResult = false;
		}
	}

	// Wurde ein Abwesenheitscode angegeben
	m_CB_Absence.GetWindowText(olStringValue);
	if (olStringValue.IsEmpty())
	{
		olErrorString += LoadStg(IDS_STRING1868);
		blResult = false;
	}

	// Fehlermeldung ausgeben
	if (!blResult)
	{
		m_DateFrom.SetFocus();
		m_DateFrom.SetSel(0,-1);
		MessageBox(olErrorString,"Rostering",MB_ICONWARNING);
	}

	return blResult;
}

void CDraDialog::SetStatic()
{
	SetDlgItemText(IDC_S_Abwesenheit,LoadStg(IDC_S_Abwesenheit));
	SetDlgItemText(IDC_S_von,LoadStg(IDC_S_von));
	SetDlgItemText(IDC_S_bis,LoadStg(IDC_S_bis));
	SetDlgItemText(IDC_S_Bezeichnung,LoadStg(IDC_S_Bezeichnung));
	SetDlgItemText(IDC_S_Bemerkung,LoadStg(IDC_S_Bemerkung));
}

void CDraDialog::SetButtonText()
{
	SetDlgItemText(IDOK,LoadStg(ID_OK));
	SetDlgItemText(IDCANCEL,LoadStg(ID_CANCEL));
}
