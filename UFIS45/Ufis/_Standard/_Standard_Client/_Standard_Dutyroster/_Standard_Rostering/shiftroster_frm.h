//////////////////////////////////////////////////////////////////////////////////
//
//		HISTORY
//
//		rdr		12.01.2k	OnToolTipText added, overrides the CMDIChildWnd-fct
//
//////////////////////////////////////////////////////////////////////////////////
#if !defined(AFX_SHIFTROSTER_FRM_H__978497A3_5C56_11D3_8EF7_00001C034EA0__INCLUDED_)
#define AFX_SHIFTROSTER_FRM_H__978497A3_5C56_11D3_8EF7_00001C034EA0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <combobar.h>

//****************************************************************************
// ShiftRoster_Frm frame
//****************************************************************************

class ShiftRoster_Frm : public CMDIChildWnd
{
	DECLARE_DYNCREATE(ShiftRoster_Frm)
protected:
	ShiftRoster_Frm();           // protected constructor used by dynamic creation

	// spezial Toolbar mit Combobox
	CComboToolBar	m_wndToolBar;

// Attributes
public:

// Operations
public:
	// for Multilanguage-ToolTop-Support added 
	bool OnToolTipText(UINT, NMHDR* pNMHDR, LRESULT* pResult);

	// R�ckgabe der Toolbar
	CComboToolBar* GetToolBar() {return (&m_wndToolBar);}

	// Gibt einen Zeiger auf die Combobox zur�ck
	CComboBox* GetComboBoxView()	{return (&(m_wndToolBar.m_toolBarCombo));}
	CComboBox* GetComboBoxCoverage()	{return (&(m_wndToolBar.m_toolBarCombo2));}
	CComboBox* GetComboBoxShiftplan()	{return (&(m_wndToolBar.m_toolBarCombo3));}

	// Laden von bestimmten Toolbars (mit Save Zeichen und ohne Save Zeichen)
	void LoadToolBar(BOOL bIsSave);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ShiftRoster_Frm)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~ShiftRoster_Frm();

	// Combobox in der ToolBar erstellen.
	BOOL CreateComboBox(UINT nID, int nNumber);

	// Generated message map functions
	//{{AFX_MSG(ShiftRoster_Frm)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnUpdateBCodenumber(CCmdUI* pCmdUI);
	afx_msg void OnUpdateBRelease(CCmdUI* pCmdUI);
	afx_msg void OnUpdateBSaveplan(CCmdUI* pCmdUI);
	afx_msg void OnUpdateBPlan(CCmdUI* pCmdUI);
	afx_msg void OnUpdateBRequirement(CCmdUI* pCmdUI);
	afx_msg void OnUpdateBView(CCmdUI* pCmdUI);
	afx_msg void OnShiftPrint(CCmdUI* pCmdUI);
	afx_msg void OnClose();
	afx_msg void OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHIFTROSTER_FRM_H__978497A3_5C56_11D3_8EF7_00001C034EA0__INCLUDED_)
