// ExcelExportDlg.cpp : implementation file
//

#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ExcelExportDlg dialog


ExcelExportDlg::ExcelExportDlg(CWnd* pParent /*=NULL*/)
	: CDialog(ExcelExportDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(ExcelExportDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	pomParent = (DutyRoster_View*)pParent;
}


void ExcelExportDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ExcelExportDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ExcelExportDlg, CDialog)
	//{{AFX_MSG_MAP(ExcelExportDlg)
	ON_BN_CLICKED(IDC_R_EXCEL_ABSX, OnRExcelAbsx)
	ON_BN_CLICKED(IDC_R_EXCEL_AMEND, OnRExcelAmend)
	ON_BN_CLICKED(IDC_R_EXCEL_BLANK, OnRExcelBlank)
	ON_BN_CLICKED(IDC_R_EXCEL_VIEW, OnRExcelView)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ExcelExportDlg message handlers

BOOL ExcelExportDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// set title of form
	SetWindowText(LoadStg(IDS_STRING135));

	// set text of the radio buttons
	SetDlgItemText(IDC_R_EXCEL_VIEW,LoadStg(IDS_STRING136));
	SetDlgItemText(IDC_R_EXCEL_BLANK,LoadStg(IDS_STRING142));
	SetDlgItemText(IDC_R_EXCEL_ABSX,LoadStg(IDS_STRING143));
	SetDlgItemText(IDC_R_EXCEL_AMEND,LoadStg(IDS_STRING157));

	// set text of check boxes
	SetDlgItemText(IDC_C_SHIFTCODE,LoadStg(IDS_STRING238));
	SetDlgItemText(IDC_C_ABSENCECODE,LoadStg(IDS_STRING239));
	SetDlgItemText(IDC_C_SHIFTFUNCTION,LoadStg(IDS_STRING240));
	SetDlgItemText(IDC_C_SUB1SUB2,LoadStg(IDS_STRING241));
	SetDlgItemText(IDC_C_TEMPABS,LoadStg(IDS_STRING242));

	// set text of OK and CANCEL buttons
	SetDlgItemText(IDOK,LoadStg(IDS_STRING998));
	SetDlgItemText(IDCANCEL,LoadStg(IDS_STRING999));

	HideRadios();
	HandleCheckBoxes();

	return TRUE;
}

void ExcelExportDlg::OnOK() 
{
	CWaitCursor olDummy;
	bool blIsAnythingChecked = false;

	ExcelExport olExport(pomParent);

	// get the selected radio button and trigger the export
	if (((CButton*) GetDlgItem(IDC_R_EXCEL_VIEW))->GetCheck() == 1)
	{
		olExport.Export("VIEW_COMPLETE");
		blIsAnythingChecked = true;
	}
	else if (((CButton*) GetDlgItem(IDC_R_EXCEL_BLANK))->GetCheck() == 1)
	{
		olExport.Export("VIEW_BLANK");
		blIsAnythingChecked = true;
	}
	else if (((CButton*) GetDlgItem(IDC_R_EXCEL_ABSX))->GetCheck() == 1)
	{
		olExport.Export("VIEW_ABSENCE_X");
		blIsAnythingChecked = true;
	}
	else if (((CButton*) GetDlgItem(IDC_R_EXCEL_AMEND))->GetCheck() == 1)
	{
		CString olTmp = "VIEW_LEVEL1DEVIATIONS";
		if (((CButton*) GetDlgItem(IDC_C_SHIFTCODE))->GetCheck() == 1)
			olTmp += CString(",SHIFTCODE");
		if (((CButton*) GetDlgItem(IDC_C_ABSENCECODE))->GetCheck() == 1)
			olTmp += CString(",ABSENCECODE");
		if (((CButton*) GetDlgItem(IDC_C_SHIFTFUNCTION))->GetCheck() == 1)
			olTmp += CString(",SHIFTFUNCTION");
		if (((CButton*) GetDlgItem(IDC_C_SUB1SUB2))->GetCheck() == 1)
			olTmp += CString(",SUB1SUB2");
		if (((CButton*) GetDlgItem(IDC_C_TEMPABS))->GetCheck() == 1)
			olTmp += CString(",TEMPABS");

		olExport.Export(olTmp);
		blIsAnythingChecked = true;
	}

	// was anything checked?
	if (blIsAnythingChecked == false)
	{
		MessageBox(LoadStg(IDS_STRING144), LoadStg(IDS_STRING135),MB_OK | MB_ICONINFORMATION);
	}
	else
	{
		CDialog::OnOK();
	}
}

void ExcelExportDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

// hide reports/exports in dependancy of customer
void ExcelExportDlg::HideRadios()
{
	CRect olRect;
	int ilMoveUp;

	if (ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER") == "SIN")
	{
		ilMoveUp = 0;
	}
	else
	{
		ilMoveUp = 140;
	}

	// resize the form
	GetWindowRect (olRect);
	ClientToScreen (olRect);
	MoveWindow(olRect.left, olRect.top, olRect.Width(), olRect.Height() - ilMoveUp);
	CenterWindow();

	// resize the frame containing the radios
	GetDlgItem(IDC_EXCEL_FRAME)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem(IDC_EXCEL_FRAME)->MoveWindow(olRect.left, olRect.top, olRect.Width(), olRect.Height() - ilMoveUp);

	if (ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER") == "SIN")
	{
		// move the radios
		GetDlgItem(IDC_R_EXCEL_BLANK)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem(IDC_R_EXCEL_BLANK)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

		GetDlgItem(IDC_R_EXCEL_ABSX)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem(IDC_R_EXCEL_ABSX)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

		GetDlgItem(IDC_R_EXCEL_AMEND)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem(IDC_R_EXCEL_AMEND)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

		GetDlgItem(IDC_C_SHIFTCODE)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem(IDC_C_SHIFTCODE)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

		GetDlgItem(IDC_C_ABSENCECODE)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem(IDC_C_ABSENCECODE)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

		GetDlgItem(IDC_C_SHIFTFUNCTION)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem(IDC_C_SHIFTFUNCTION)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

		GetDlgItem(IDC_C_SUB1SUB2)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem(IDC_C_SUB1SUB2)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

		GetDlgItem(IDC_C_TEMPABS)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem(IDC_C_TEMPABS)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());
	}
	else
	{
		// default is export of complete view and blank shifts, so we hide absences with 'X' and the weekly amendmends
		GetDlgItem(IDC_R_EXCEL_ABSX)->ShowWindow(SW_HIDE);

		GetDlgItem(IDC_R_EXCEL_AMEND)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_C_SHIFTCODE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_C_ABSENCECODE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_C_SHIFTFUNCTION)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_C_SUB1SUB2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_C_TEMPABS)->ShowWindow(SW_HIDE);

		// no radios to move, because the hidden radio's are the last ones
	}

	// move the buttons
	GetDlgItem(IDOK)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem(IDOK)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem(IDCANCEL)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem(IDCANCEL)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());
}

void ExcelExportDlg::OnRExcelAbsx() 
{
	HandleCheckBoxes();
}

void ExcelExportDlg::OnRExcelAmend() 
{
	HandleCheckBoxes();
}

void ExcelExportDlg::OnRExcelBlank() 
{
	HandleCheckBoxes();
}

void ExcelExportDlg::OnRExcelView() 
{
	HandleCheckBoxes();
}

void ExcelExportDlg::HandleCheckBoxes()
{
	if (((CButton*) GetDlgItem(IDC_R_EXCEL_AMEND))->GetCheck() == 1)
	{
		GetDlgItem(IDC_C_SHIFTCODE)->EnableWindow(TRUE);
		GetDlgItem(IDC_C_ABSENCECODE)->EnableWindow(TRUE);
		GetDlgItem(IDC_C_SHIFTFUNCTION)->EnableWindow(TRUE);
		GetDlgItem(IDC_C_SUB1SUB2)->EnableWindow(TRUE);
		GetDlgItem(IDC_C_TEMPABS)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_C_SHIFTCODE)->EnableWindow(FALSE);
		GetDlgItem(IDC_C_ABSENCECODE)->EnableWindow(FALSE);
		GetDlgItem(IDC_C_SHIFTFUNCTION)->EnableWindow(FALSE);
		GetDlgItem(IDC_C_SUB1SUB2)->EnableWindow(FALSE);
		GetDlgItem(IDC_C_TEMPABS)->EnableWindow(FALSE);
	}
}
