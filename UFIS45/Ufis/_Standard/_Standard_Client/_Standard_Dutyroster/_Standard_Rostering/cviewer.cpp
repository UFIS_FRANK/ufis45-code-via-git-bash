#include <stdafx.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CViewer

CViewer::CViewer()
{             
	bmNewView = false;
}
         
CViewer::~CViewer()
{
}

void CViewer::SetViewerKey(CString strKey)
{
	CFGDATA rlCfg;
	VIEWDATA rlViewData;
	BOOL blFound = FALSE;
	int ilCount = ogCfgData.omViews.GetSize();

	//Search for the key
	for(int i = 0; i < ilCount; i++)
	{
		CString olKey = ogCfgData.omViews[i].Ckey;
		if( olKey == strKey)
			blFound = TRUE;
	}
	if(blFound == FALSE)
	{
		rlViewData.Ckey = strKey;
		ogCfgData.omViews.NewAt(ogCfgData.omViews.GetSize(), rlViewData);
	}
	m_BaseViewName = strKey;

}

BOOL CViewer::CreateView(CString strView, const CStringArray &possibleFilters, bool bpWithDBDelete)
{
	if( m_BaseViewName.IsEmpty() ) return FALSE;
	int ilCount = ogCfgData.omViews.GetSize();

	//Search for the key
	for(int i = 0; i < ilCount; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			DeleteView(strView, bpWithDBDelete);
			VIEW_VIEWNAMES rlViewNames;
			VIEW_TYPEDATA rlViewTypeData;
			rlViewNames.ViewName = strView;
			rlViewTypeData.Type = CString("FILTER");
			
			for(int ili = 0; ili < possibleFilters.GetSize(); ili++ ) 
			{
				VIEW_TEXTDATA rlTextData;
				rlTextData.Page = possibleFilters.GetAt(ili);
				rlViewTypeData.omTextData.NewAt(rlViewTypeData.omTextData.GetSize(), rlTextData);
			}
			rlViewNames.omTypeData.NewAt(rlViewNames.omTypeData.GetSize(), rlViewTypeData);
			ogCfgData.omViews[i].omNameData.NewAt(ogCfgData.omViews[i].omNameData.GetSize(), rlViewNames);
			return true;
		}
	}
	return false;

}

void CViewer::GetViews(CStringArray &strArray)
{
	int ilCount = ogCfgData.omViews.GetSize();

	strArray.RemoveAll();
	for(int i = 0; i < ilCount; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			int ilC2 = ogCfgData.omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				strArray.Add(ogCfgData.omViews[i].omNameData[j].ViewName);
			}
		}
	}
}

BOOL CViewer::SelectView(CString strView)
{
	int ilC1 = ogCfgData.omViews.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			int ilC2 = ogCfgData.omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(ogCfgData.omViews[i].omNameData[j].ViewName == strView)
				{
					m_ViewName = strView;
					return true;
				}
			}
		}
	}
	return false;
}

CString CViewer::SelectView()
{
	if(m_ViewName != CString(""))
		return m_ViewName;
	for(int i = 0; i < ogCfgData.omViews.GetSize(); i++)
	{
		CString olk = ogCfgData.omViews[i].Ckey;
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			if(ogCfgData.omViews[i].omNameData.GetSize() > 0)
			{
				WriteLogFull("CViewer::SelectView() view found: %s", ogCfgData.omViews[i].omNameData[0].ViewName);
				return ogCfgData.omViews[i].omNameData[0].ViewName;
			}
		}
	}
	return m_ViewName;
}

// return true, if found, else false
bool CViewer::FindView(CString strView)
{
	int ilC1 = ogCfgData.omViews.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			int ilC2 = ogCfgData.omViews[i].omNameData.GetSize();
			for(int j=0; j< ilC2; j++)
			{
				if(ogCfgData.omViews[i].omNameData[j].ViewName == strView)
				{
					return true;
				}
			}
		}
	}
	return false;
}

BOOL CViewer::DeleteView(CString strView, bool bpWithDBDelete)
{

	BOOL blFound = FALSE;
	int ilC1 = ogCfgData.omViews.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			int ilC2 = ogCfgData.omViews[i].omNameData.GetSize();
			for(int j=0; j< ilC2; j++)
			{
				if(ogCfgData.omViews[i].omNameData[j].ViewName == strView)
				{
					VIEW_VIEWNAMES *prlViewName;
					prlViewName = &ogCfgData.omViews[i].omNameData[j];
					if(prlViewName == 0) continue;
					int ilC3 = prlViewName->omTypeData.GetSize()-1;
					for(int k = ilC3; k >= 0; k--)
					{
						prlViewName->omTypeData[k].omValues.RemoveAll();
						int ilC4 = prlViewName->omTypeData[k].omTextData.GetSize()-1;
						for(int l = ilC4; l >= 0; l--)
						{
							prlViewName->omTypeData[k].omTextData[l].omValues.RemoveAll();
						}
						prlViewName->omTypeData[k].omTextData.DeleteAll();
					}
					ogCfgData.omViews[i].omNameData[j].omTypeData.DeleteAll(); //New
					ogCfgData.omViews[i].omNameData.DeleteAt(j);
					blFound = TRUE;
					if(bpWithDBDelete == true)
					{
						ogCfgData.DeleteViewFromDiagram(m_BaseViewName, strView);
					}
					j = ilC2;
					i = ilC1; // let's break
				}
			}
		}
	}
	return blFound;
}

void CViewer::GetFilterPage(CStringArray &strArray)
{
	VIEW_VIEWNAMES *prlViewName;
	VIEW_TYPEDATA rlTypeData;
	strArray.RemoveAll();

	prlViewName = GetActiveView();
	if(prlViewName == NULL)
		return;
	int ilC1 = prlViewName->omTypeData.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(prlViewName->omTypeData[i].Type == "FILTER")
		{
			VIEW_TYPEDATA rlT = prlViewName->omTypeData[i];
			CString rlTTy = rlT.Type;
			int ilC2 = prlViewName->omTypeData[i].omTextData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				strArray.Add(prlViewName->omTypeData[i].omTextData[j].Page);
			}
		}
	}
}

void CViewer::SetFilter(CString strFilter, const CStringArray &opFilter)
{

	VIEW_TEXTDATA *prlTextData;
	prlTextData = GetActiveFilter(strFilter);


		
   if(prlTextData == NULL)
	{
		RAW_VIEWDATA rlRawData;

		strcpy(rlRawData.Ckey,m_BaseViewName);
		strcpy(rlRawData.Name,m_ViewName);
		strcpy(rlRawData.Type,"FILTER");
		strcpy(rlRawData.Page,strFilter);

		VIEW_TYPEDATA *prlTypeData = ogCfgData.FindViewTypeData(&rlRawData);
		if (prlTypeData != NULL)
		{
			VIEW_TEXTDATA rlTextData;
			rlTextData.Page = strFilter;
			prlTypeData->omTextData.NewAt(prlTypeData->omTextData.GetSize(), rlTextData);
			prlTextData = GetActiveFilter(strFilter);
		}
	}
	if (prlTextData != NULL)
	{
  
	    int ilCount = opFilter.GetSize();
		//first we must delete current set filters
		prlTextData->omValues.RemoveAll();
		for(int i = 0; i < ilCount; i++)
		{
			prlTextData->omValues.Add(opFilter.GetAt(i));
		}
	}
}

void CViewer::GetFilter(CString strFilter, CStringArray &opFilter)
{
	opFilter.RemoveAll();
	VIEW_TEXTDATA *prlTextData;
	prlTextData = GetActiveFilter(strFilter);
	if(prlTextData == NULL)
	{
		prlTextData = GetActiveFilter(strFilter,"<Default>");
	}
	if(prlTextData == 0) return;
	for(int i = 0; i < prlTextData->omValues.GetSize(); i++)	
	{
		CString olTmp = prlTextData->omValues[i];
		opFilter.Add(prlTextData->omValues[i]);
	}
}

VIEW_VIEWNAMES * CViewer::GetActiveView()
{
	int ilC1 = ogCfgData.omViews.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			int ilC2 = ogCfgData.omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(ogCfgData.omViews[i].omNameData[j].ViewName == m_ViewName)
				{
					return &ogCfgData.omViews[i].omNameData[j];
				}
			}
		}
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////
// MWO: 09.10.1996 finds a filter
VIEW_TEXTDATA * CViewer::GetActiveFilter(CString opFilter,char *pcpViewName)
{
	CString olViewName;

	if ( pcpViewName != NULL )
	{
		olViewName = pcpViewName;
	}
	else
	{
		olViewName = m_ViewName;
	}
	int ilC1 = ogCfgData.omViews.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			int ilC2 = ogCfgData.omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				
				if(ogCfgData.omViews[i].omNameData[j].ViewName == olViewName)
				{
					int ilC3 = ogCfgData.omViews[i].omNameData[j].omTypeData.GetSize();
					for(int k = 0; k < ilC3; k++)
					{
						if(ogCfgData.omViews[i].omNameData[j].omTypeData[k].Type == "FILTER")
						{
							int ilC4 = ogCfgData.omViews[i].omNameData[j].omTypeData[k].omTextData.GetSize();
							for(int l = 0; l < ilC4; l++)
							{
								
								if(ogCfgData.omViews[i].omNameData[j].omTypeData[k].omTextData[l].Page == opFilter)
								{
									return &ogCfgData.omViews[i].omNameData[j].omTypeData[k].omTextData[l];
								}
							}
						}
					}
				}
			}
		}
	}
	return NULL;
}


CString CViewer::GetBaseViewName()
{
	return m_BaseViewName;
}

CString CViewer::GetViewName()
{
	return m_ViewName;
}


void CViewer::SafeDataToDB(CString opViewName)
{	
	int ilC1 = ogCfgData.omViews.GetSize();
	CWaitCursor olDummy;
	for(int i = 0; i < ilC1; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			ogCfgData.UpdateViewForDiagram(m_BaseViewName, ogCfgData.omViews[i],opViewName);
			i = ilC1;
		}
	}
}
