
#ifndef __CVIEWER_H__
#define __CVIEWER_H__

#include <CedaCfgData.h>
                                 
#ifndef HKEY_CLASSES_ROOT
    #define HKEY_CLASSES_ROOT           (( HKEY ) 0x80000000 )
#endif
#ifndef HKEY_CURRENT_USER
	#define HKEY_CURRENT_USER           (( HKEY ) 0x80000001 )
#endif
#ifndef HKEY_LOCAL_MACHINE
	#define HKEY_LOCAL_MACHINE          (( HKEY ) 0x80000002 )
#endif
#ifndef HKEY_USERS
	#define HKEY_USERS                  (( HKEY ) 0x80000003 )
#endif

struct SELECTEDFIELD
{
	int iRow;
	int iColumn;
	int iRowOffset;
	int iColumnOffset;
	CString oOldText;
	int iOldBkColor;

	SELECTEDFIELD(void)
	{
		iRow          = 0;
		iColumn       = 0;
		iRowOffset    = 0;
		iColumnOffset = 0;
		iOldBkColor	  = 0;
	}
};


//////////////////////////////////////////////////////////////////////////////////
// MWO: 07.10.1996 
// now we have to read, create and set all views from database. The registry
// will not be used any more. We keep the method-call devices in the same manner.
// For calls there will be no changes and it is transparent like nothing has
// changed. the release, which uses the registry is saved as backup for
// possible problems.
//

class CViewer: public CObject
{
public:
    CViewer(); 
    ~CViewer();
    
public:
	CStringArray omGpeSearchConnection;
	void	SetViewerKey(CString strKey);
	BOOL    CreateView(CString strView, const CStringArray &possibleFilters, bool bpWithDBDelete = true);
	void	GetViews(CStringArray &strArray);
    BOOL    SelectView(CString strView);
	CString SelectView();
    BOOL    DeleteView(CString strView, bool bpWithDBDelete = true);

	void	GetFilterPage(CStringArray &strArray);
    void    SetFilter(CString strFilter, const CStringArray &opFilter);
    void	GetFilter(CString strFilter, CStringArray &opFilter);
	bool	FindView(CString strView);

	VIEW_VIEWNAMES * GetActiveView();
	VIEW_TEXTDATA * GetActiveFilter(CString opFilter,char *pcpViewName = NULL);

	void SafeDataToDB(CString opViewName);
	CString GetBaseViewName();
	CString GetViewName();
	bool	bmNewView;	// true if on Save a new view was created, if rewrite an old one - false

private:
	CString	m_BaseViewName;
	CString	m_ViewName;

};

#endif //__CVIEWER_H__
