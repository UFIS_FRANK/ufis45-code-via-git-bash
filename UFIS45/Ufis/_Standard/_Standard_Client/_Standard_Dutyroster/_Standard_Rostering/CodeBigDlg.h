#if !defined(AFX_CODEBIGDLG_H__74D816E3_D7C7_11D3_8FED_00500454BF3F__INCLUDED_)
#define AFX_CODEBIGDLG_H__74D816E3_D7C7_11D3_8FED_00500454BF3F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CodeBigDlg.h : Header-Datei
//
#define COLCOUNT_BSD 10
#define COLCOUNT_ODA 2
#define COLCOUNT_WIS 2


// DnD
#include <CCSDragDropCtrl.h>
//Schichten
#include <cedabsddata.h>

class CGridControl;

///////////////////////////////////////////////////////////////////////////// 
// Dialogfeld CCodeBigDlg 

class CCodeBigDlg : public CDialog
{
// Konstruktion
public:
	CCodeBigDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	~CCodeBigDlg();

	void ChangeValidDates(COleDateTime opFrom,COleDateTime opTo);
	void SetOrgCodes(CString opOrgList);
	void SetPfcCodes(CString opPfcList);
	void ReloadAndShow(bool bpAllCodes);
	void SetCaptionInfo(CString opInfo);
	void SetShowWish(bool bpShowWish);

// Dialogfelddaten
	//{{AFX_DATA(CCodeBigDlg)
	enum { IDD = IDD_CODEBIG_DLG };
	CButton	m_OK;
	CButton	m_B_FindODA;
	CButton	m_B_FindBSD;
	CButton	m_B_FindWIS;
	//}}AFX_DATA

// �berschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktions�berschreibungen
	//{{AFX_VIRTUAL(CCodeBigDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	// Grid initialisieren
	void IniGrid ();
	void IniColWidths();
	void FillWishGrid(bool bpAllCodes=false);
	void FillBsdGrid(bool bpAllCodes=false);
	bool SortGrid(CGridControl* popGrid,int ipRow);
	void FillOdaGrid();

	// Das Grid
	CGridControl	*pomBsdGrid;
	CGridControl	*pomOdaGrid;
	CGridControl	*pomWisGrid;

	// Mu� das Grid neu gef�llt werden
	bool bmIsDirty;

	// Alle Codes m�ssen gezeigt werden
	bool bmAllCodes;

	// Gibt an ob das Grid mit den W�nschen angezeigt werden soll
	bool bmShowWish;

	// String mit allen Org-Codes die angezeigt werden sollen
	CString omOrgList;

	// String mit allen Pfc-Codes die angezeigt werden sollen
	CString omPfcList;

	// Map f�r die Schichtcodes
	CMapStringToString omBsdMap;

	// Objekt f�r D'n'D-Funktionalit�t
	CCSDragDropCtrl omDragDropObject;

	// Von / Bis G�ltigkeit der BSD Schichten
	COleDateTime omFromTime,omToTime;

	// DnD starten
	void OnDragBegin(CString opUrno,CString opCode,CString opSource);

	// Pr�ft ob eine Schicht angezeigt werden soll
	bool CheckBsd(BSDDATA* popBsd, COleDateTime opFrom,COleDateTime opTo, bool bpAllCodes);
	// Pr�fen ob der Extrabutton in der Titelleiste getroffen wurde
	bool IsButtonHit(CPoint point);





	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CCodeBigDlg)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
    afx_msg LONG OnGridDragBegin(WPARAM wParam, LPARAM lParam);
	afx_msg void On_B_FindBSD();
	afx_msg void On_B_FindODA();
	afx_msg void On_B_FindWIS();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnNcLButtonDown( UINT nHitTest, CPoint point );
	afx_msg void OnNcPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ f�gt unmittelbar vor der vorhergehenden Zeile zus�tzliche Deklarationen ein.

#endif // AFX_CODEBIGDLG_H__74D816E3_D7C7_11D3_8FED_00500454BF3F__INCLUDED_
