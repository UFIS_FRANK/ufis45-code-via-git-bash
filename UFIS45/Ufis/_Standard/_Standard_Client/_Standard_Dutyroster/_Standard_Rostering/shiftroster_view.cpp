// ShiftRoster_View.cpp : implementation file
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static int CompareGrupp( const NSRTABLEDATA **e1, const NSRTABLEDATA **e2);
static int CompareWeek( const GSPTABLEDATA **e1, const GSPTABLEDATA **e2);

//***************************************************************************
//***************************************************************************
// ab hier: Globale Funktionen
//***************************************************************************
//***************************************************************************

//***************************************************************************
// CompareGrupp: vergleicht zwei S�tze anhand ihrer Grupp.
//  R�ckgabe: siehe strcmp()
//***************************************************************************

static int CompareGrupp( const NSRTABLEDATA **e1, const NSRTABLEDATA **e2)
{
	return (strcmp((**e1).Grupp, (**e2).Grupp));
}

//***************************************************************************
// CompareShiftbeginn: vergleicht zwei S�tze anhand erstes Zeichens in Grupp und wenn gleich, dann der Zeit in omShiftTime.
//  R�ckgabe: siehe strcmp()
//***************************************************************************
static int CompareShiftbeginn( const NSRTABLEDATA **e1, const NSRTABLEDATA **e2)
{
	if((**e1).Grupp.IsEmpty() || (**e2).Grupp.IsEmpty()) 
		return (strcmp((**e1).Grupp, (**e2).Grupp));
	
	int ilGrp1Char = (**e1).Grupp.GetAt(0) - (**e2).Grupp.GetAt(0);
	if(!ilGrp1Char)
	{
		int ilEsbgCmp = strcmp ( (**e1).omShiftTime , (**e2).omShiftTime);
		if(!ilEsbgCmp)
		{
			return (strcmp((**e1).Grupp, (**e2).Grupp));		// wenn die Zeiten gleich sind, vergleichen wir doch die Grupp
		}
		else
		{
			return ilEsbgCmp;
		}
	}
	else
	{
		return ilGrp1Char;
	}
	return (strcmp((**e1).Grupp, (**e2).Grupp));
}

//***************************************************************************
// CompareWeek: vergleicht zwei Grundschichtpl�ne anhand ihrer Codenummern.
//  R�ckgabe: siehe strcmp()
//***************************************************************************

static int CompareWeek( const GSPTABLEDATA **e1, const GSPTABLEDATA **e2)
{
	return (strcmp((**e1).Week, (**e2).Week));
}

//**************************************************************************************
// Proce.s.sBC: globale Funktion als Callback f�r den Broadcast-Handler
//**************************************************************************************
// Local function prototype
static void ProcessBC(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

static void ProcessBC(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ShiftRoster_View *polShiftRoster = (ShiftRoster_View *)popInstance;
	polShiftRoster->ProcessBCs(ipDDXType, vpDataPointer);
}

//***************************************************************************
// ShiftRoster_View
//***************************************************************************

IMPLEMENT_DYNCREATE(ShiftRoster_View, CFormView)

ShiftRoster_View *pomMe;

ShiftRoster_View::ShiftRoster_View()
: CFormView(ShiftRoster_View::IDD)
{
	//{{AFX_DATA_INIT(ShiftRoster_View)
	//}}AFX_DATA_INIT
	CCS_TRY;


	////////////////////////////////////////////////////////////////////     qunima

     /*   CTime CurrentTime=CTime::GetCurrentTime(); 
		if (CurrentTime.GetDay() >19)  
			{
        int min = CurrentTime.GetMinute();

        if (min % 3 == 1)
		{
            AfxMessageBox("CEDA-Socket Error: 10054");
            return ;

		}
		}
		
		*/
		////////////////////////////////////////////////////////////////////
	
	
	//getting customer-id
	omCustomer = ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER");

	// Init Views
	InitDefaultView();

	// Zeiger auf CodeNummer-Dialog initialisieren
	pomShiftCodeNumberDlg = NULL;

	// Zeiger auf Schicht-Konsistenz-Dialog initialisieren
	pomShiftKonsistDlg    = NULL;

	pomShiftAWDlg = NULL;

	imGSPRangeMin = 0;
	imGSPRangeMax = 0;
	imNSRRangeMin = 0;
	imNSRRangeMax = 0;
	omAktiv = ">"; // max. one Char!!

	// Default-Einstellungen f�r Ansicht
	imViewStattGrupp  = VIEW_GRUPP;
	imViewStattDetail = VIEW_DETAIL;
	imViewStattBonus  = VIEW_BONUS;
	imViewShowGPL	  = VIEW_ALLGPL;
	imViewShowCover   = VIEW_ISDEBIT;
	imViewShowEmployee = VIEW_FLNAME;
	omViewFunctionUrnos.Empty();
	omViewQualificationUrnos.Empty();
	omViewFunctionFctcs.Empty();

	imActualLine = -1;
	imFirstButtonID = 65000;
	imFirstEditID	= 67000;
	imFirstComboID	= 4000;

	bmIsChangedGPL = false;
	bmIsChangedBonus = false;
	omSelSPLurno = "0000000000";	

	omSortCode	= "1";
	omSortGrupp	= "2";
	omSortBonus	= "3";
	om_SelAWDlgUrnos = "";

	bmShowAll = true;

	omSortInfo.SetSize(2);
	omSortInfo[0].sortOrder = CGXSortInfo::ascending;
	omSortInfo[1].sortOrder = CGXSortInfo::ascending;
	omSortInfo[0].nRC = 2;
	omSortInfo[1].nRC = 2;
	omSortInfo[0].sortType = CGXSortInfo::autodetect;
	omSortInfo[1].sortType = CGXSortInfo::autodetect;

	// Es werden die ersten Schichte angezeigt
	imShowShiftNumber = 1;

	LoadData();

	// beim Broadcast-Handler anmelden
	Register();

	//general stuff
	imGSPurnoIdx = ogBCD.GetFieldIndex("GSP","URNO");
	imGSPgpluIdx = ogBCD.GetFieldIndex("GSP","GPLU");
	imGSPweekIdx = ogBCD.GetFieldIndex("GSP","WEEK");
	imGSPnstfIdx = ogBCD.GetFieldIndex("GSP","NSTF");
	imGSPstfuIdx = ogBCD.GetFieldIndex("GSP","STFU");
	imGSPstfnIdx = ogBCD.GetFieldIndex("GSP","STFN");
	imGSPwgpcIdx = ogBCD.GetFieldIndex("GSP","WGPC");

	//erste Schicht-URNOs
	imGSPbumoIdx = ogBCD.GetFieldIndex("GSP","BUMO");
	if (imGSPbumoIdx < 0) MessageBox ("Field GSPTAB.BUMO does not exist!");
	imGSPbutuIdx = ogBCD.GetFieldIndex("GSP","BUTU");
	if (imGSPbutuIdx < 0) MessageBox ("Field GSPTAB.BUTU does not exist!");
	imGSPbuweIdx = ogBCD.GetFieldIndex("GSP","BUWE");
	if (imGSPbuweIdx < 0) MessageBox ("Field GSPTAB.BUWE does not exist!");
	imGSPbuthIdx = ogBCD.GetFieldIndex("GSP","BUTH");
	if (imGSPbuthIdx < 0) MessageBox ("Field GSPTAB.BUTH does not exist!");
	imGSPbufrIdx = ogBCD.GetFieldIndex("GSP","BUFR");
	if (imGSPbufrIdx < 0) MessageBox ("Field GSPTAB.BUFR does not exist!");
	imGSPbusaIdx = ogBCD.GetFieldIndex("GSP","BUSA");
	if (imGSPbusaIdx < 0) MessageBox ("Field GSPTAB.BUSA does not exist!");
	imGSPbusuIdx = ogBCD.GetFieldIndex("GSP","BUSU");
	if (imGSPbusuIdx < 0) MessageBox ("Field GSPTAB.BUSU does not exist!");

	//zweite Schicht-URNOs
	imGSPsumoIdx = ogBCD.GetFieldIndex("GSP","SUMO");
	if (imGSPsumoIdx < 0) MessageBox ("Field GSPTAB.SUMO does not exist!");
	imGSPsutuIdx = ogBCD.GetFieldIndex("GSP","SUTU");
	if (imGSPsutuIdx < 0) MessageBox ("Field GSPTAB.SUTU does not exist!");
	imGSPsuweIdx = ogBCD.GetFieldIndex("GSP","SUWE");
	if (imGSPsuweIdx < 0) MessageBox ("Field GSPTAB.SUWE does not exist!");
	imGSPsuthIdx = ogBCD.GetFieldIndex("GSP","SUTH");
	if (imGSPsuthIdx < 0) MessageBox ("Field GSPTAB.SUTH does not exist!");
	imGSPsufrIdx = ogBCD.GetFieldIndex("GSP","SUFR");
	if (imGSPsufrIdx < 0) MessageBox ("Field GSPTAB.SUFR does not exist!");
	imGSPsusaIdx = ogBCD.GetFieldIndex("GSP","SUSA");
	if (imGSPsusaIdx < 0) MessageBox ("Field GSPTAB.SUSA does not exist!");
	imGSPsusuIdx = ogBCD.GetFieldIndex("GSP","SUSU");
	if (imGSPsusuIdx < 0) MessageBox ("Field GSPTAB.SUSU does not exist!");

	//erste Funktion-URNOs
	imGSPp1moIdx = ogBCD.GetFieldIndex("GSP","P1MO");
	if (imGSPp1moIdx < 0) MessageBox ("Field GSPTAB.P1MO does not exist!");
	imGSPp1tuIdx = ogBCD.GetFieldIndex("GSP","P1TU");
	if (imGSPp1tuIdx < 0) MessageBox ("Field GSPTAB.P1TU does not exist!");
	imGSPp1weIdx = ogBCD.GetFieldIndex("GSP","P1WE");
	if (imGSPp1weIdx < 0) MessageBox ("Field GSPTAB.P1WE does not exist!");
	imGSPp1thIdx = ogBCD.GetFieldIndex("GSP","P1TH");
	if (imGSPp1thIdx < 0) MessageBox ("Field GSPTAB.P1TH does not exist!");
	imGSPp1frIdx = ogBCD.GetFieldIndex("GSP","P1FR");
	if (imGSPp1frIdx < 0) MessageBox ("Field GSPTAB.P1FR does not exist!");
	imGSPp1saIdx = ogBCD.GetFieldIndex("GSP","P1SA");
	if (imGSPp1saIdx < 0) MessageBox ("Field GSPTAB.P1SA does not exist!");
	imGSPp1suIdx = ogBCD.GetFieldIndex("GSP","P1SU");
	if (imGSPp1suIdx < 0) MessageBox ("Field GSPTAB.P1SU does not exist!");

	//zweite Funktion-URNOs
	imGSPp2moIdx = ogBCD.GetFieldIndex("GSP","P2MO");
	if (imGSPp2moIdx < 0) MessageBox ("Field GSPTAB.P2MO does not exist!");
	imGSPp2tuIdx = ogBCD.GetFieldIndex("GSP","P2TU");
	if (imGSPp2tuIdx < 0) MessageBox ("Field GSPTAB.P2TU does not exist!");
	imGSPp2weIdx = ogBCD.GetFieldIndex("GSP","P2WE");
	if (imGSPp2weIdx < 0) MessageBox ("Field GSPTAB.P2WE does not exist!");
	imGSPp2thIdx = ogBCD.GetFieldIndex("GSP","P2TH");
	if (imGSPp2thIdx < 0) MessageBox ("Field GSPTAB.P2TH does not exist!");
	imGSPp2frIdx = ogBCD.GetFieldIndex("GSP","P2FR");
	if (imGSPp2frIdx < 0) MessageBox ("Field GSPTAB.P2FR does not exist!");
	imGSPp2saIdx = ogBCD.GetFieldIndex("GSP","P2SA");
	if (imGSPp2saIdx < 0) MessageBox ("Field GSPTAB.P2SA does not exist!");
	imGSPp2suIdx = ogBCD.GetFieldIndex("GSP","P2SU");
	if (imGSPp2suIdx < 0) MessageBox ("Field GSPTAB.P2SU does not exist!");

	// initializing the NameConfigurationDlg
	CStringArray olArr;
	olArr.Add(LoadStg(IDS_STRING1796));
	olArr.Add(LoadStg(IDS_STRING1797));
	olArr.Add(LoadStg(IDS_STRING1798));
	olArr.Add(LoadStg(IDS_STRING1799));
	olArr.Add(LoadStg(IDS_STRING1800));
	olArr.Add(LoadStg(IDS_STRING1801));
	olArr.Add(LoadStg(IDS_STRING1802));
	olArr.Add(LoadStg(IDS_STRING1803));
	olArr.Add(LoadStg(IDS_STRING1804));
	olArr.Add(LoadStg(IDS_STRING1805));

	pomNameConfigDlg = new NameConfigurationDlg(&olArr, "");

	bmClientRelease = !this->ServerReleaseAvailable();

	pomMe = this;






	CCS_CATCH_ALL;
}

//***************************************************************************
// destructor
//***************************************************************************

ShiftRoster_View::~ShiftRoster_View()
{
	int i;
	int j;

	// m�gliche nicht modale Dialoge beenden
	if (IsCodeDlgActive())
	{
		pomShiftCodeNumberDlg->OnCancel();
		delete pomShiftCodeNumberDlg;
	}

	if (IsKonsistDlgActive())
	{
		pomShiftKonsistDlg->OnCancel();
	}

	// GSPTable l�schen
	for (i = 0; i < omGSPTable.GetSize(); i++)
	{
		delete omGSPTable[i].Week;
		delete omGSPTable[i].Number;
		delete omGSPTable[i].Workgroup;
		delete omGSPTable[i].Staff;
		delete omGSPTable[i].Houers;
		for (j = 0; j < 7; j++)
		{
			if (omGSPTable[i].Day[j] != NULL)
			{
				delete omGSPTable[i].Day[j];
			}
		}
	}
	omGSPTable.DeleteAll();
	omGSPTablePtr.DeleteAll();
	omGSPTableData.DeleteAll();
	omDeletedGSPs.DeleteAll();
	omChangedGSPs.DeleteAll();
	omGSPTablePtrMap.RemoveAll();

	// NSRTable l�schen
	for (i = 0; i < omNSRTable.GetSize(); i++)
	{
		delete omNSRTable[i].Grupp;
		for (j = 0; j < 7; j++)
		{
			if (omNSRTable[i].Day[j] != NULL)
			{
				delete omNSRTable[i].Day[j];
			}
		}
	}
	omNSRTable.DeleteAll();
	omNSRTableData.DeleteAll();
	omNSRTableDataMap.RemoveAll();

	// Rest l�schen
	omBonusData.DeleteAll();
	omGruppData.DeleteAll();
	for (i = 0; i < omSelectGSPTableData.GetSize(); i++)
	{
		delete omSelectGSPTableData[i].SourceControl;
	}
	omSelectGSPTableData.DeleteAll();
	ogMsdData.omUrnoMap.RemoveAll();
	ogMsdData.omData.DeleteAll();

	delete pomShiftCodeNumberDlg;
	if (pomNameConfigDlg != NULL)
	{
		delete pomNameConfigDlg;
	}
	if (pomViewer != NULL)
	{
		delete pomViewer;
	}
}

void ShiftRoster_View::OverCapacityMsgBox(CString opWeek)
{
	if (omCustomer == "ADR")
	{
		CString olMsg;
		int ilWeek = atoi(opWeek);
		olMsg.Format (LPCTSTR(LoadStg(IDS_STRING63450)), ilWeek); //Die erforderliche Kapazit�t wurde �berschritten in Woche %d.*INTXT*
		MessageBox(olMsg, LoadStg(IDS_INFO), MB_ICONINFORMATION);
	}
}

//****************************************************************************
// f�llen der ComboBoxen
//****************************************************************************

void ShiftRoster_View::InitComboBox()
{
	CString olLine;
	int i;

	// Schichtplan Combobox f�llen
	if (pomComboBoxShiftplan != NULL)
	{
		olLine = "";
		pomComboBoxShiftplan->ResetContent();
		pomComboBoxShiftplan->AddString("");

		pomRecord = new RecordSet(ogBCD.GetFieldCount("SPL"));

		int ilSnamIdx = ogBCD.GetFieldIndex("SPL","SNAM");
		int ilUrnoIdx = ogBCD.GetFieldIndex("SPL","URNO");
		for(i = 0; i < ogBCD.GetDataCount("SPL"); i++)
		{
			ogBCD.GetRecord("SPL",i, *pomRecord);
			olLine.Format("%-32s                                    URNO=%s",pomRecord->Values[ilSnamIdx],pomRecord->Values[ilUrnoIdx]);
			pomComboBoxShiftplan->AddString(olLine);
		}
		omSelSPLurno = "0000000000";
		delete pomRecord;
	}

	// Alles View in die Combobox f�llen
	if (pomComboBoxView != NULL)
	{
		pomComboBoxView->ResetContent();
		CStringArray olStrArr;
		pomViewer->GetViews(olStrArr);
		for(int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
		{
			pomComboBoxView->AddString(olStrArr[ilIndex]);
		}

		ilIndex = pomComboBoxView->FindStringExact(-1,LoadStg(IDS_STRING562));

		if(ilIndex != CB_ERR)
		{
			pomComboBoxView->SetCurSel(ilIndex);
			pomViewer->SelectView(LoadStg(IDS_STRING562));
		}
	}
	
	// Alle Bedarfe in die Combobox f�llen
	if (pomComboBoxCoverage != NULL)
	{
		CString olLine;
		// Schichtbedarfe l�schen
		ogMsdData.omUrnoMap.RemoveAll();
		ogMsdData.omData.DeleteAll();

		pomComboBoxCoverage->ResetContent();
		pomComboBoxCoverage->AddString("");//To produce an empty line

		int ilCount = ogSdgData.omData.GetSize();
		SDGDATA *prlSdgData = NULL;
		for (i = 0; i < ilCount; i++)
		{
			prlSdgData = &ogSdgData.omData[i];
			if (strcmp(prlSdgData->Days,"7") == 0)
			{
				olLine.Format("%-32s                                    URNO=%ld",prlSdgData->Dnam, prlSdgData->Urno);
				if(pomComboBoxCoverage->FindStringExact(-1,olLine) == CB_ERR)
				{
					pomComboBoxCoverage->AddString(olLine);
				}
			}
		}
	}

	// Employment contracts Show COT 
	m_CB_Contract.ResetContent();
	m_CB_Contract.AddString("");
	
	for(int m = 0; m < ogCotData.omData.GetSize(); m++)
	{
		olLine.Format("%-32s                                    URNO=%d",ogCotData.omData[m].Ctrc, ogCotData.omData[m].Urno);
		m_CB_Contract.AddString(olLine);
	}

	// Organizational units
	if (ogBCD.GetFieldIndex("GPL","ORGU") > -1)
	{
		m_CB_OrgUnit.ResetContent();
		m_CB_OrgUnit.AddString("");
		bool blAdd;
		
		// fill the OrgUnitCombo
		int ilOrgCount =  ogOrgData.omData.GetSize();
		ORGDATA *prlOrgData = NULL;
		CString olSec;
		CString olLine;

		for (i = 0; i < ilOrgCount; i++)
		{
			blAdd = false;
			prlOrgData = &ogOrgData.omData[i];
			
			if (ogCCSParam.GetParamValue("GLOBAL","ID_ORG_SECURITY") == "Y")
			{
				olSec = ogPrivList.GetStat(prlOrgData->Dpt1);
				if (olSec != "-" && olSec != "0")
				{
					blAdd = true;
				}
			}
			else
			{
				blAdd = true;
			}

			if (blAdd == true)
			{
				olLine.Format("%-32s                                    URNO=%d",prlOrgData->Dpt1,prlOrgData->Urno);
				m_CB_OrgUnit.AddString(olLine);
			}
		}
	}
	else
	{
		m_S_OrgUnit.ShowWindow(SW_HIDE);
		m_CB_OrgUnit.ShowWindow(SW_HIDE);	
	}
}

//****************************************************************************
// Abfragen ob gespeichert werden soll
//****************************************************************************

bool ShiftRoster_View::OkToExit()
{
	CCS_TRY;
	int ilMBoxReturn = IDYES;
	
	if(bmIsChangedGPL == true)
	{
		ilMBoxReturn = MessageBox(LoadStg(SHIFT_ISCHANGED_GPL),LoadStg(ST_FRAGE),(MB_ICONQUESTION | MB_YESNOCANCEL | MB_DEFBUTTON1));
		
		switch (ilMBoxReturn) 
		{
		case IDYES:
			SaveGPL();
			break;
		case IDNO:
			bmIsChangedGPL = false;
			break;
		case IDCANCEL:
			return (false);
		}
	}
	
	if(bmIsChangedBonus == true)
	{
		ilMBoxReturn = MessageBox(LoadStg(SHIFT_ISCHANGED_BONUS),LoadStg(ST_FRAGE),(MB_ICONQUESTION | MB_YESNOCANCEL | MB_DEFBUTTON1));
		
		
		switch (ilMBoxReturn) 
		{
		case IDYES:
			SaveBonus();
			break;
		case IDNO:
			bmIsChangedBonus = false;
			break;
		case IDCANCEL:
			return (false);
		}
	}
	
	return true;
	CCS_CATCH_ALL;
	return false;
}

//****************************************************************************
// OnCancel
//****************************************************************************

void ShiftRoster_View::OnCancel() 
{
	if (OkToExit())
	{
		// Window schlie�en. (Es wird auf das MDI Frame Window zugegriffen)
		((CMDIChildWnd*) GetParentFrame())->MDIDestroy();
	}
}

//***************************************************************************
// DXX
//***************************************************************************

void ShiftRoster_View::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ShiftRoster_View)
	DDX_Control(pDX, IDC_B_SECONDSHIFT,		m_B_SecondShift);
	DDX_Control(pDX, IDC_B_AUTOPOPULATE,	m_B_AutoPopulate);
	DDX_Control(pDX, IDC_DUMMY_FOR_FOCUS,	m_ButDummy);
	DDX_Control(pDX, IDC_S_NULL,			m_S_Null);
	DDX_Control(pDX, IDC_B_STFS_P,			m_B_Stfs_P);
	DDX_Control(pDX, IDC_B_STFS_M,			m_B_Stfs_M);
	DDX_Control(pDX, IDC_B_KONSIST,			m_B_Konsist);
	DDX_Control(pDX, IDC_B_INFO,			m_B_Info);
	DDX_Control(pDX, IDC_E_NULL,			m_E_Null);
	DDX_Control(pDX, IDC_CB_BONUS,			m_CB_Bonus);
	DDX_Control(pDX, IDC_B_BONUS,			m_B_Bonus);
	DDX_Control(pDX, IDC_SPIN_PERIOD,		m_SBC_Periode);
	DDX_Control(pDX, IDC_E_PERIOD,			m_E_GPL_Peri);
	DDX_Control(pDX, IDC_E_GPL_CAPTION,		m_E_GPL_Caption);
	DDX_Control(pDX, IDC_LB_GPLLIST,		m_LB_GPLList);
	DDX_Control(pDX, IDC_SB_NSR,			m_SB_NSR);
	DDX_Control(pDX, IDC_CB_CONTRACT,		m_CB_Contract);
	DDX_Control(pDX, IDC_SB_GSP,			m_SB_GSP);
	DDX_Control(pDX, IDC_S_WEEK,			m_S_Week);
	DDX_Control(pDX, IDC_S_WE,				m_S_We);
	DDX_Control(pDX, IDC_S_TU,				m_S_Tu);
	DDX_Control(pDX, IDC_S_TH,				m_S_Th);
	DDX_Control(pDX, IDC_S_SU,				m_S_Su);
	DDX_Control(pDX, IDC_S_STAFF,			m_S_Staff);
	DDX_Control(pDX, IDC_S_SA,				m_S_Sa);
	DDX_Control(pDX, IDC_S_NUMBER,			m_S_Number);
	DDX_Control(pDX, IDC_S_MO,				m_S_Mo);
	DDX_Control(pDX, IDC_S_HOUERS,			m_S_Houers);
	DDX_Control(pDX, IDC_S_FR,				m_S_Fr);
	DDX_Control(pDX, IDC_B_NEW,				m_B_New);
	DDX_Control(pDX, IDC_B_COPY,			m_B_Copy);
	DDX_Control(pDX, IDC_B_DEAKTIVATE,		m_B_Assign);
	DDX_Control(pDX, IDC_B_DELETE,			m_B_Delete);
	DDX_Control(pDX, IDC_S_VALID,			m_S_Valid);
	DDX_Control(pDX, IDC_S_CONTRACT,		m_S_Contract);
	DDX_Control(pDX, IDC_S_HOLIDAY,			m_S_Holiday);
	DDX_Control(pDX, IDC_S_ABSENCE,			m_S_Absence);
	DDX_Control(pDX, IDC_S_ROUNDUP,			m_S_Roundup);
	DDX_Control(pDX, IDC_E_VALIDFROM,		m_E_GPL_Vafr);
	DDX_Control(pDX, IDC_E_VALIDTO,			m_E_GPL_Vato);
	DDX_Control(pDX, IDC_E_ABSENCE,			m_E_Absence);
	DDX_Control(pDX, IDC_E_HOLIDAY,			m_E_Holiday);
	DDX_Control(pDX, IDC_E_ROUNDUP,			m_E_Roundup);
	DDX_Control(pDX, IDC_E_RELEASENOTE,		m_E_ReleaseNote);
	DDX_Control(pDX, IDC_CB_ORGUNIT,		m_CB_OrgUnit);
	DDX_Control(pDX, IDC_S_ORGUNIT,			m_S_OrgUnit);
	DDX_Control(pDX, IDC_S_GPL_STPT,		m_S_GPL_StPt);
	DDX_Control(pDX, IDC_E_GPL_STPT,		m_E_GPL_StPt);
	DDX_Control(pDX, IDC_B_GPL_STPT,		m_B_GPL_StPt);
	DDX_Control(pDX, IDC_S_WORKGROUP,		m_S_Workgroup);
	//}}AFX_DATA_MAP
}

//***************************************************************************
// MessageMap
//***************************************************************************

BEGIN_MESSAGE_MAP(ShiftRoster_View, CFormView)
//{{AFX_MSG_MAP(ShiftRoster_View)
ON_BN_CLICKED(IDC_B_VIEW,			OnBView)
ON_BN_CLICKED(IDC_B_REQUIREMENT,    OnBRequirement)
ON_BN_CLICKED(IDC_B_PLAN,			OnBPlan)
ON_BN_CLICKED(IDC_B_SAVEPLAN,		OnBSaveplan)
ON_BN_CLICKED(IDC_B_RELEASE,		OnBRelease)
ON_BN_CLICKED(IDC_B_CODENUMBER,		OnBCodenumber) 
ON_CBN_SELCHANGE(IDW_COMBO1,		OnSelchangeCbView)
ON_CBN_SELCHANGE(IDW_COMBO2,		OnSelchangeCbRequirement)
ON_CBN_SELCHANGE(IDC_CB_BONUS,		OnSelchangeCbBonus)
ON_LBN_SELCHANGE(IDC_LB_GPLLIST,	OnSelchangeLbGpllist)
ON_LBN_DBLCLK(IDC_LB_GPLLIST,		OnDblclkLbGpllist)
ON_BN_CLICKED(IDC_B_INFO,			OnBInfo)
ON_BN_CLICKED(IDC_B_CHECK,			OnBCheck)
ON_BN_CLICKED(IDC_B_NEW,			OnBNew)
ON_BN_CLICKED(IDC_B_COPY,			OnBCopy)
ON_BN_CLICKED(IDC_B_DEAKTIVATE,		OnBAssignGPL)
ON_BN_CLICKED(IDC_B_DELETE,			OnBDelete)
ON_BN_CLICKED(IDC_B_KONSIST,		OnBKonsist)
ON_BN_CLICKED(IDC_B_BONUS,			OnBBonus)
ON_WM_VSCROLL()
ON_CBN_SELCHANGE(IDW_COMBO3,		OnSelchange_CB_SPL)
ON_CBN_SELCHANGE(IDC_CB_CONTRACT,	OnSelchange_CB_Contract)
ON_MESSAGE(WM_DROP,					OnDrop)  
ON_MESSAGE(WM_DRAGOVER,				OnDragOver)
ON_MESSAGE(WM_EDIT_RETURN,			OnInplaceReturn)  
ON_BN_CLICKED(IDC_B_STFS_P,			OnBStfsP)
ON_BN_CLICKED(IDC_B_STFS_M,			OnBStfsM)
ON_BN_CLICKED(IDC_B_SECONDSHIFT,	OnBSecondshift)
ON_BN_CLICKED(IDC_B_AUTOPOPULATE,	OnBAutoPopulate)
ON_WM_DESTROY()
ON_BN_CLICKED(ID_SHIFT_PRINT,		OnShiftPrint)
ON_NOTIFY_EX(TTN_NEEDTEXT,0,OnTtnNeedText)
ON_CBN_SELCHANGE(IDC_CB_ORGUNIT,	OnSelchange_CB_OrgUnit)
ON_MESSAGE(WM_EDIT_LBUTTONDOWN,		OnGSPTable_EditLButtonDown)
ON_MESSAGE(WM_EDIT_RBUTTONDOWN,		OnGSPTable_EditRButtonDown)
ON_MESSAGE(WM_EDIT_KILLFOCUS,		OnGSPTable_EditKillfocus)
ON_MESSAGE(WM_BUTTON_RBUTTONDOWN,	OnGSPTable_R_ButtonKlick)
ON_MESSAGE(WM_BUTTON_CLICK,			OnGSPTable_L_ButtonKlick)
ON_BN_CLICKED(IDCANCEL,				OnCancel)
ON_COMMAND(MENU_ADD_LINE,			OnMenuAddLineToGSP)
ON_COMMAND(MENU_DEL_LINE,			OnMenuDeleteLineFromGSP)
ON_COMMAND(MENU_CANCEL_ACTION,		OnMenuCancelAction)
ON_EN_CHANGE(IDC_E_GPL_FILTER,		OnChangeEGplFilter)
ON_BN_CLICKED(IDC_B_GPL_STPT,		OnBStPt)
ON_CONTROL_RANGE(CBN_SELCHANGE,4000,5000,OnSelchangeCbWorkgroup)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//***************************************************************************
// ShiftRoster_View diagnostics
//***************************************************************************

#ifdef _DEBUG
void ShiftRoster_View::AssertValid() const
{
	CFormView::AssertValid();
}

void ShiftRoster_View::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

//********************************************************************************
// Gr��e und Position des Windows beim ersten Aufruf berechnen
//********************************************************************************

CRect ShiftRoster_View::CalcInitialWindowSizeAndPos() 
{
	CCS_TRY;
	// Fenster Rect
	CRect	olWindowRect;
	CRect	olMainFrameRect;
	int ilCXMonitor = 0, ilWindowWidth = 0, ilLeft = 0, ilRight = 0;
	
	// Einlesen des Client Bereiches des Hauptframes.(Mainframe)
	GetParentFrame()->GetParentFrame()->GetClientRect(olMainFrameRect);
	olWindowRect = olMainFrameRect;
	
	// Auf welchem Monitor soll das Fenster dargestellt werden, relevant falls Anzahl > 1.
	int ilWhichMonitor	= ogCfgData.GetMonitorForWindow(CString(MON_SHIFTROSTER_STRING));

	// Anzahl der Monitore im System
	int ilMonitors		= ogCfgData.GetMonitorForWindow(CString(MON_COUNT_STRING));
	
	// Breite eines Monitors.
	ilWindowWidth = (int)((olMainFrameRect.right - olMainFrameRect.left)/ilMonitors);
	
	// Fenster auf Monitor 1 darstellen
	if(ilWhichMonitor == 1)
	{
		// Anzahl der Monitore im System
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = olMainFrameRect.right;
			ilLeft = (int)((ilCXMonitor-ilWindowWidth)/2);
			ilRight = ilLeft + ilWindowWidth;
			break;
		case 2:
			ilCXMonitor = (int)(olMainFrameRect.right/2);
			ilLeft = (int)((ilCXMonitor-ilWindowWidth)/2);
			ilRight = ilLeft + ilWindowWidth;
			break;
		case 3:
			ilCXMonitor = (int)(olMainFrameRect.right/3);
			ilLeft = (int)((ilCXMonitor-ilWindowWidth)/2);
			ilRight = ilLeft + ilWindowWidth;
			break;
		default:
			ilCXMonitor = olMainFrameRect.right;
			ilLeft = (int)((ilCXMonitor-ilWindowWidth)/2);
			ilRight = ilLeft + ilWindowWidth;
			break;
		}
	}
	
	// Fenster auf Monitor 2 darstellen
	else if(ilWhichMonitor == 2)
	{
		// Anzahl der Monitore im System
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = olMainFrameRect.right;
			ilLeft = (int)((ilCXMonitor-ilWindowWidth)/2);
			ilRight = ilLeft + ilWindowWidth;
			break;
		case 2:
			ilCXMonitor = (int)(olMainFrameRect.right/2);
			ilLeft = (int)((ilCXMonitor-ilWindowWidth)/2)+(ilCXMonitor);
			ilRight = ilLeft + ilWindowWidth;
			break;
		case 3:
			ilCXMonitor = (int)(olMainFrameRect.right/3);
			ilLeft = (int)((ilCXMonitor-ilWindowWidth)/2)+(ilCXMonitor);
			ilRight = ilLeft + ilWindowWidth;
			break;
		default:
			// Hier OK ?
			ilRight = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	// Fenster auf Monitor 3 darstellen
	else if(ilWhichMonitor == 3)
	{
		// Anzahl der Monitore im System
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = olMainFrameRect.right;
			ilLeft = (int)((ilCXMonitor-ilWindowWidth)/2);
			ilRight = ilLeft + ilWindowWidth;
			break;
		case 2:
			ilCXMonitor = (int)(olMainFrameRect.right/2);
			ilLeft = (int)((ilCXMonitor-ilWindowWidth)/2);
			ilRight = ilLeft + ilWindowWidth;
			break;
		case 3:
			ilCXMonitor = (int)(olMainFrameRect.right/3);
			ilLeft = (int)((ilCXMonitor-ilWindowWidth)/2)+(2*ilCXMonitor);
			ilRight = ilLeft + ilWindowWidth;
			break;
		default:
			// Hier OK ?
			ilRight = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}

	// Werte werde angepa�t auf Anzahl der Monitore und welchem Monitor das Fenster
	// dargestellt werden soll.
	olWindowRect.left  = ilLeft;
	olWindowRect.right = ilRight;

	// Sollte eine Statusbar vorhanden sein, diese vom Client Rect abziehen
	CRect olStatusBarRect;

	// Berechnete H�he der Statusbar
	int	ilStatusBarHight;	

	if (GetParentFrame()->GetParentFrame()->GetMessageBar() != NULL)
	{
		(GetParentFrame()->GetParentFrame()->GetMessageBar())->GetWindowRect(olStatusBarRect);
		ilStatusBarHight = olStatusBarRect.bottom - olStatusBarRect.top;

		// Client Rect f�r dieses Fenster verkleinern.
		olWindowRect.bottom -= ilStatusBarHight;
	}
	
	// Die Rahmen Abziehen
	olWindowRect.right -= GetSystemMetrics(SM_CXSIZEFRAME);
	olWindowRect.bottom -= GetSystemMetrics(SM_CYSIZEFRAME);
	
	// Das Fenster sollte jedoch maximal 1024 breit und 786 tief sein
	int nlHeight = olWindowRect.bottom - olWindowRect.top;
	int nlWidth = olWindowRect.right - olWindowRect.left;
	
	if (nlHeight > MAX_SHIFT_HEIGHT)
		olWindowRect.bottom -= (nlHeight - MAX_SHIFT_HEIGHT);
	
	if (nlWidth > MAX_SHIFT_WIDTH)
		olWindowRect.right -= (nlWidth - MAX_SHIFT_WIDTH);
	
	return (olWindowRect);
	CCS_CATCH_ALL;
	return CRect(0,0,0,0);
}

//***************************************************************************
// ShiftRoster_View message handlers
//***************************************************************************

void ShiftRoster_View::OnInitialUpdate() 
{
	CCS_TRY;
	CFormView::OnInitialUpdate();

	// Fokus-Button (f�r OnInplaceReturn()) verstecken
	m_ButDummy.ShowWindow(SW_HIDE);

	// Frame auf die richtige Position und Gr��e schieben
	GetParentFrame()->MoveWindow(CalcInitialWindowSizeAndPos());
	// unn�tig ?
	//	ResizeParentToFit();
	GetParentFrame()->RecalcLayout();

	// Titel des Docs setzen.
	CRosteringDoc *polDoc = (CRosteringDoc *)GetDocument();
	if(polDoc != NULL)
	{
		polDoc->SetTitle(LoadStg(SHIFT_CAPTION));
	}

	// Zeiger auf Comboboxen in der Toolbar erhalten.
	pomComboBoxView = (((ShiftRoster_Frm*) GetParentFrame())->GetComboBoxView());
	pomComboBoxCoverage = (((ShiftRoster_Frm*) GetParentFrame())->GetComboBoxCoverage());
	pomComboBoxShiftplan = (((ShiftRoster_Frm*) GetParentFrame())->GetComboBoxShiftplan());

	// Anzahl-Buttons deaktivieren
	m_B_Stfs_P.EnableWindow(TRUE);
	m_B_Stfs_M.EnableWindow(TRUE);

	// UFIS-Icon laden und einstellen
	HICON m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	SetIcon(m_hIcon, FALSE);

	// neue Bedarfs-Ansicht generieren
	pomViewer = new CViewer;
	pomViewer->SetViewerKey("ID_SHEET_SHIFT_VIEW");

	//Tooltip-Control initialisieren
	omToolTipCtrl.Create(this);
	this->EnableToolTips();

	// Verf�gbarkeiten und Ansicht der Oberfl�chenelemente wiederherstellen
	RestoreWindowSettings();

	// Farb- und Formateinstellungen der Oberfl�chenelemente
	InitCCSEditControls();

	// Sprachabh�ngige Strings laden
	LoadStrings();

	// Buttons initialisieren
	SetInitialButtonStates();

	//***
	//*** GSP TABLE **************
	CRect lpRect;
	GetParentFrame()->GetWindowRect( &lpRect );
	int ApplHight = lpRect.Height();	// get actual hight of appl

	int ilLeftPos = 1;		//the most important left position
	int ilTopPos  = 170;	//the most important top position
	int ilWidth1  = 23;		//Week and Number
	int ilWidth2  = 100;	//Workgroup
	int ilWidth3  = 210;	//Staff Button
	int ilWidth4  = 87;		//Days
	int ilWidth5  = 37;		//Houers
	int ilHight   = 18;		//all Fields hight

	int	ilAddPix = -1;
	int ilTmpLeftPos;
	int ilTmpTopPos = ilTopPos - ilHight - ilAddPix - 3;
	int ilScrollBarTopPos = ilTopPos;
	int ilScrollBarWidth = 16;
	omGSPTableRect.left = ilLeftPos;

	imNSRLines = 9;			// the most important NSR Lines

	int gap = 56;			// correct calc

	//the most important GSP Lines
	imGSPLines = (ApplHight - ((imNSRLines * ilHight) + ilTopPos + gap)) / ilHight;
	if (imGSPLines < 16)
	{
		imGSPLines = 16;
	}

	// Move GSP table header /////////////////////////////////
	ilTmpLeftPos = ilLeftPos;
	m_S_Week.MoveWindow(CRect(ilTmpLeftPos,ilTmpTopPos,ilTmpLeftPos+ilWidth1,ilTmpTopPos+ilHight));
	ilTmpLeftPos += ilWidth1 + ilAddPix;

	m_S_Number.MoveWindow(CRect(ilTmpLeftPos+1,ilTmpTopPos,ilTmpLeftPos+ilWidth1,ilTmpTopPos+ilHight));
	ilTmpLeftPos += ilWidth1 + ilAddPix;

	if (this->omCustomer == "SIN" && imGSPwgpcIdx >= 0)
	{
		m_S_Workgroup.MoveWindow(CRect(ilTmpLeftPos+1,ilTmpTopPos,ilTmpLeftPos+ilWidth2,ilTmpTopPos+ilHight));
		ilTmpLeftPos += ilWidth2 + ilAddPix;

	}
	else
	{
		m_S_Workgroup.ShowWindow(SW_HIDE);
		ilWidth3 += ilWidth2;
	}

	m_S_Staff.MoveWindow(CRect(ilTmpLeftPos+1,ilTmpTopPos,ilTmpLeftPos+ilWidth3,ilTmpTopPos+ilHight));
	ilTmpLeftPos += ilWidth3 + ilAddPix;

	omNSRTableRect.left = ilTmpLeftPos;
	m_S_Mo.MoveWindow(CRect(ilTmpLeftPos+1,ilTmpTopPos,ilTmpLeftPos+ilWidth4,ilTmpTopPos+ilHight));
	ilTmpLeftPos += ilWidth4 + ilAddPix;
	m_S_Tu.MoveWindow(CRect(ilTmpLeftPos+1,ilTmpTopPos,ilTmpLeftPos+ilWidth4,ilTmpTopPos+ilHight));
	ilTmpLeftPos += ilWidth4 + ilAddPix;
	m_S_We.MoveWindow(CRect(ilTmpLeftPos+1,ilTmpTopPos,ilTmpLeftPos+ilWidth4,ilTmpTopPos+ilHight));
	ilTmpLeftPos += ilWidth4 + ilAddPix;
	m_S_Th.MoveWindow(CRect(ilTmpLeftPos+1,ilTmpTopPos,ilTmpLeftPos+ilWidth4,ilTmpTopPos+ilHight));
	ilTmpLeftPos += ilWidth4 + ilAddPix;
	m_S_Fr.MoveWindow(CRect(ilTmpLeftPos+1,ilTmpTopPos,ilTmpLeftPos+ilWidth4,ilTmpTopPos+ilHight));
	ilTmpLeftPos += ilWidth4 + ilAddPix;
	m_S_Sa.MoveWindow(CRect(ilTmpLeftPos+1,ilTmpTopPos,ilTmpLeftPos+ilWidth4,ilTmpTopPos+ilHight));
	ilTmpLeftPos += ilWidth4 + ilAddPix;
	m_S_Su.MoveWindow(CRect(ilTmpLeftPos+1,ilTmpTopPos,ilTmpLeftPos+ilWidth4,ilTmpTopPos+ilHight));
	ilTmpLeftPos += ilWidth4 + ilAddPix;
	m_S_Houers.MoveWindow(CRect(ilTmpLeftPos+1,ilTmpTopPos,ilTmpLeftPos+ilWidth5,ilTmpTopPos+ilHight));
	ilTmpLeftPos += ilWidth5 + ilAddPix;
	m_S_Null.MoveWindow(CRect(ilTmpLeftPos+1,ilTmpTopPos,ilTmpLeftPos+ilScrollBarWidth+2,ilTmpTopPos+ilHight));

	DWORD olEditStyle_Edit			= WS_CHILD|WS_VISIBLE|WS_BORDER|ES_UPPERCASE|ES_CENTER| WS_EX_NOPARENTNOTIFY|ES_WANTRETURN;	// f�r Mo-Fr Eingabe
	DWORD olEditStyle_Edit_Right	= WS_CHILD|WS_VISIBLE|WS_BORDER|ES_UPPERCASE|ES_RIGHT|ES_WANTRETURN;						// f�r Number Eingabe
	DWORD olEditStyle_noEdit		= WS_CHILD|WS_VISIBLE|ES_READONLY|WS_BORDER|ES_WANTRETURN;									// f�r Week & Grupp
	DWORD olEditStyle_noEdit_Center = WS_CHILD|WS_VISIBLE|ES_READONLY|WS_BORDER|ES_CENTER|ES_WANTRETURN;						// f�r Mo-Fr Statistik
	DWORD olEditStyle_noEdit_Right	= WS_CHILD|WS_VISIBLE|ES_READONLY|WS_BORDER|ES_RIGHT|ES_WANTRETURN;							// f�r Hours
	DWORD olComboBoxStyle			= WS_CHILD|WS_VISIBLE|WS_BORDER|CBS_DROPDOWNLIST|CBS_SORT|WS_VSCROLL|WS_HSCROLL;			// for workgroup

	// Create GSP table ////////////////////////////////////
	int ilFirstButtonID = imFirstButtonID;
	int ilFirstEditID	= imFirstEditID;
	int ilFirstComboID	= imFirstComboID;

	for (int ilGSPLine = 0; ilGSPLine < imGSPLines; ilGSPLine++)
	{
		ilTmpLeftPos = ilLeftPos;

		GSPTABLE *prlGSPTABLE = new GSPTABLE;
		GSPTABLEPTR *prlGSPTABLEPTR;

		// * * Week * * * * * * * * * * * * *
		prlGSPTABLE->Week = new CCSEdit();
		prlGSPTABLE->Week->Create(olEditStyle_noEdit,CRect(ilTmpLeftPos,ilTopPos,ilTmpLeftPos+ilWidth1,ilTopPos+ilHight),this);
		prlGSPTABLE->Week->SetBKColor(LIGHTSILVER);
		prlGSPTABLE->Week->SetEditFont(&ogMSSansSerif_Regular_8);

		ilTmpLeftPos += ilWidth1 + ilAddPix;
		// * * Number * * * * * * * * * * * * *
		prlGSPTABLE->Number = new CCSEdit();
		prlGSPTABLE->Number->Create(olEditStyle_Edit_Right,CRect(ilTmpLeftPos,ilTopPos,ilTmpLeftPos+ilWidth1,ilTopPos+ilHight),this,ilFirstEditID + ilGSPLine);
		prlGSPTABLE->Number->SetBKColor(WHITE);
		prlGSPTABLE->Number->SetEditFont(&ogMSSansSerif_Regular_8);
		prlGSPTABLE->Number->SetFormat("##");
		prlGSPTABLE->Number->SetTextLimit(0,2);
		ilTmpLeftPos += ilWidth1 + ilAddPix;

		prlGSPTABLEPTR = new GSPTABLEPTR;
		prlGSPTABLEPTR->Record  = -1;
		prlGSPTABLEPTR->Day   = 0;
		omGSPTablePtr.Add(prlGSPTABLEPTR);
		omGSPTablePtrMap.SetAt(prlGSPTABLE->Number,prlGSPTABLEPTR);

		if (this->omCustomer == "SIN" && imGSPwgpcIdx >= 0)
		{

			// * * Workgroup * * * * * * * * * * * * *
			prlGSPTABLE->Workgroup = new CComboBox();
			prlGSPTABLE->Workgroup->Create(olComboBoxStyle,CRect(ilTmpLeftPos,ilTopPos,ilTmpLeftPos+ilWidth2,ilTopPos+ilHight*10),this,ilFirstComboID + ilGSPLine);
			prlGSPTABLE->Workgroup->SetFont(&ogMSSansSerif_Regular_8);
			prlGSPTABLE->Workgroup->SetItemHeight(-1,ilHight - 6 * GetSystemMetrics(SM_CYBORDER));
			prlGSPTABLE->Workgroup->SetItemHeight(0,ilHight - 6 * GetSystemMetrics(SM_CYBORDER));

			FillWorkGroupComboBox(prlGSPTABLE->Workgroup);

			ilTmpLeftPos += ilWidth2 + ilAddPix;

			prlGSPTABLEPTR = new GSPTABLEPTR;
			prlGSPTABLEPTR->Record  = -1;
			prlGSPTABLEPTR->Day   = 0;
			omGSPTablePtr.Add(prlGSPTABLEPTR);
			omGSPTablePtrMap.SetAt(prlGSPTABLE->Workgroup,prlGSPTABLEPTR);
		}


		// * * Staff Buttons * * * * * * * * *
		prlGSPTABLE->Staff = new CCSButtonCtrl();
		prlGSPTABLE->Staff->Create("",WS_VISIBLE|BS_FLAT|BS_OWNERDRAW|BS_PUSHBUTTON|BS_LEFTTEXT,CRect(ilTmpLeftPos,ilTopPos,ilTmpLeftPos+ilWidth3,ilTopPos+ilHight),this,ilFirstButtonID + ilGSPLine);
		prlGSPTABLE->Staff->SetColors(WHITE,WHITE,WHITE);
		prlGSPTABLE->Staff->SetFont(&ogMSSansSerif_Regular_8);
		prlGSPTABLE->Staff->SetInflateTextRectHight(0);
		prlGSPTABLE->Staff->SetInflateTextRectWidth(-3);
		prlGSPTABLE->Staff->DragDropRegister();

		ilTmpLeftPos += ilWidth3 + ilAddPix;


		prlGSPTABLEPTR = new GSPTABLEPTR;
		prlGSPTABLEPTR->Record  = -1;
		prlGSPTABLEPTR->Day   = 0;
		omGSPTablePtr.Add(prlGSPTABLEPTR);
		omGSPTablePtrMap.SetAt(prlGSPTABLE->Staff,prlGSPTABLEPTR);

		// * * Days * * * * * * * * * * * * *
		for (int ilDay = 0; ilDay < 7; ilDay++)
		{
			CreateDayButtons(ilDay, prlGSPTABLE, &ilTmpLeftPos, olEditStyle_Edit, ilTopPos, ilWidth4, ilHight, (ilGSPLine * 10) + 1 + ilDay + ilFirstEditID + 1000, ilGSPLine, ilAddPix);
		}

		// * * Hours * * * * * * * * * * * * *
		prlGSPTABLE->Houers = new CCSEdit();
		prlGSPTABLE->Houers->Create(olEditStyle_noEdit_Right,CRect(ilTmpLeftPos,ilTopPos,ilTmpLeftPos+ilWidth5,ilTopPos+ilHight),this);
		prlGSPTABLE->Houers->SetBKColor(LIGHTSILVER);
		prlGSPTABLE->Houers->SetEditFont(&ogMSSansSerif_Regular_8);

		ilTmpLeftPos += ilWidth5 + ilAddPix;
		ilTopPos += ilHight + ilAddPix;
		// - - - - - - - - - - - - - - - - - -
		
		omGSPTable.Add(prlGSPTABLE);
	}

	// Move GSP ScrollBar ////////////////////////////////
	omGSPTableRect.top = ilScrollBarTopPos;
	omGSPTableRect.bottom = ilTopPos - ilAddPix;
	ilTmpLeftPos += 1;
	omGSPTableRect.right = ilTmpLeftPos+ilScrollBarWidth;
	m_SB_GSP.MoveWindow(CRect(ilTmpLeftPos,ilScrollBarTopPos,omGSPTableRect.right,omGSPTableRect.bottom));
	m_SB_GSP.SetScrollRange(imGSPRangeMin,imGSPRangeMax,false);
	m_SB_GSP.SetScrollPos(imGSPRangeMin);
	m_SB_GSP.EnableScrollBar(ESB_DISABLE_BOTH);

	SCROLLINFO rlScrollInfo;
	m_SB_GSP.GetScrollInfo(&rlScrollInfo,SIF_ALL);
	rlScrollInfo.nPage = imGSPLines;
	m_SB_GSP.SetScrollInfo(&rlScrollInfo);

	//***
	//*** NSR TABLE ************
	ilTopPos += 4;
	omNSRTableRect.top = ilTopPos;
	int ilWidth6 = 200; //Shift Grupp

	omNSRTableRect.left -= (ilWidth6 + ilAddPix);

	// Move Second Shift Button
	CRect olRect;
	m_B_SecondShift.GetWindowRect(olRect);
	olRect.bottom += ilTopPos - olRect.top;
	olRect.top = ilTopPos;
	m_B_SecondShift.MoveWindow(olRect);

	// AutoPopulate Shift
	char pclConfigString[10];
	GetPrivateProfileString(pcgAppName, "AUTO_ROSTERING",  "FALSE",pclConfigString, sizeof pclConfigString, pcgConfigPath);
	if (stricmp(pclConfigString,"TRUE")==0)
	{		
		CRect olRect2;
		m_B_AutoPopulate.GetWindowRect(olRect2);
		olRect2.right = olRect.right;
		olRect2.left = olRect.left;
		olRect2.top = olRect.bottom + 2;
		olRect2.bottom = olRect2.top + 23;
		m_B_AutoPopulate.MoveWindow(olRect2);
	}
	else
	{
		m_B_AutoPopulate.ShowWindow(SW_HIDE);
	}

	// Create NSR table /////////////////////////////
	for (int iNSRLine = 0; iNSRLine < imNSRLines; iNSRLine++)
	{
		NSRTABLE *prlNSRTABLE = new NSRTABLE;

		ilTmpLeftPos = omNSRTableRect.left;

		prlNSRTABLE->Grupp = new CCSEdit();
		prlNSRTABLE->Grupp->Create(olEditStyle_noEdit, CRect(ilTmpLeftPos, ilTopPos, ilTmpLeftPos + ilWidth6, ilTopPos + ilHight),this);
		prlNSRTABLE->Grupp->SetBKColor(LIGHTSILVER);
		prlNSRTABLE->Grupp->SetEditFont(&ogMSSansSerif_Regular_8);
		ilTmpLeftPos += ilWidth6 + ilAddPix;

		for (int ilDay=0; ilDay < 7; ilDay++)
		{
			CreateNSRTable(prlNSRTABLE, ilDay, olEditStyle_noEdit_Center, CRect(ilTmpLeftPos,ilTopPos,ilTmpLeftPos+ilWidth4,ilTopPos+ilHight));
			ilTmpLeftPos += ilWidth4 + ilAddPix;
		}

		ilTopPos += ilHight + ilAddPix;

		omNSRTable.Add(prlNSRTABLE);
	}
	
	// Move NSR ScrollBar //////////////////////////////////
	ilTmpLeftPos += 1;
	omNSRTableRect.right = ilTmpLeftPos+ilScrollBarWidth;
	omNSRTableRect.bottom = ilTopPos - ilAddPix;

	m_SB_NSR.MoveWindow(CRect(ilTmpLeftPos,omNSRTableRect.top,omNSRTableRect.right,omNSRTableRect.bottom));
	m_SB_NSR.SetScrollRange(imNSRRangeMin,imNSRRangeMax,false);
	m_SB_NSR.SetScrollPos(imNSRRangeMin);
	m_SB_NSR.EnableScrollBar(ESB_DISABLE_BOTH);

	m_SB_NSR.GetScrollInfo(&rlScrollInfo,SIF_ALL);
	rlScrollInfo.nPage = imNSRLines;
	m_SB_NSR.SetScrollInfo(&rlScrollInfo);

	//*** Bedarfe laden + anzeigen ************

	//*** Show Grundschichtpl�ne erzeugen und anzeigen ************
	m_LB_GPLList.SetFont(&ogCourier_Regular_8);
	m_E_GPL_Caption.SetFont(&ogCourier_Regular_8);
	m_E_GPL_Caption.SetBKColor(LIGHTSILVER2);
	GetDlgItem(IDC_E_GPL_FILTER)->SetFont(&ogCourier_Regular_8);

	//Build_LB_GPL();
	OnChangeEGplFilter();

	//*** Delete NettoShiftRequirementTable and create first record (Summe)************
	omNSRTableData.DeleteAll();
	omNSRTableDataMap.RemoveAll();

	NSRTABLEDATA *prlData = new NSRTABLEDATA;
	prlData->GruppUrno = "00000000";
	prlData->Type = SUM;
	omNSRTableData.Add(prlData);
	omNSRTableDataMap.SetAt((CString)"",prlData);

	//***

	ShowWindow(SW_SHOWNORMAL);

	// Comboboxen f�llen
	InitComboBox();



	CCS_CATCH_ALL;
}

/****************************************************************************
DayButtons erstellen
****************************************************************************/
void ShiftRoster_View::CreateDayButtons(int ipDay, GSPTABLE *prpGSPTABLE, int* pipTmpLeftPos, DWORD opEditStyle_Edit, int ipTopPos, int ipWidth3, int ipHight, int ipFirstEditID, int ipGSPLine, int ipAddPix)
{
	GSPTABLEPTR *prlGSPTABLEPTR;
	
	prpGSPTABLE->Day[ipDay] = new CCSEdit();
	prpGSPTABLE->Day[ipDay]->Create(opEditStyle_Edit,CRect(*pipTmpLeftPos,ipTopPos,*pipTmpLeftPos+ipWidth3,ipTopPos+ipHight),this,ipFirstEditID);// + ipGSPLine);

	prpGSPTABLE->Day[ipDay]->SetBKColor(WHITE);
	prpGSPTABLE->Day[ipDay]->SetEditFont(&ogMSSansSerif_Regular_8);
	prpGSPTABLE->Day[ipDay]->SetFormat("x|#x|#x|#x|#x|#x|#x|#x|#");
	prpGSPTABLE->Day[ipDay]->SetTextLimit(0,8);
	prpGSPTABLE->Day[ipDay]->DragDropRegister();
	*pipTmpLeftPos += ipWidth3 + ipAddPix;
	prlGSPTABLEPTR = new GSPTABLEPTR;
	prlGSPTABLEPTR->Record  = -1;
	prlGSPTABLEPTR->Day   = ipDay+1;
	//prlGSPTABLEPTR->Value = NULL;
	//prlGSPTABLEPTR->Urno  = NULL;
	omGSPTablePtr.Add(prlGSPTABLEPTR);
	omGSPTablePtrMap.SetAt(prpGSPTABLE->Day[ipDay],prlGSPTABLEPTR);
}

/****************************************************************************
NSR Table erstellen
****************************************************************************/
void ShiftRoster_View::CreateNSRTable(NSRTABLE *prpNSRTABLE, int ipDay, DWORD opEditStyle_noEdit_Center, CRect opRect)
{
	prpNSRTABLE->Day[ipDay] = new CCSEdit();
	prpNSRTABLE->Day[ipDay]->Create(opEditStyle_noEdit_Center,opRect,this);
	prpNSRTABLE->Day[ipDay]->SetBKColor(LIGHTSILVER);
	prpNSRTABLE->Day[ipDay]->SetEditFont(&ogMSSansSerif_Regular_8);
}

/****************************************************************************
Buttons initialisieren
****************************************************************************/
void ShiftRoster_View::SetInitialButtonStates()
{
	// wenn highlighted - alle Doppelschicht-Zellen sollen statt erster - zweite Schicht anzeigen und bearbeiten
	m_B_SecondShift.EnableWindow(true);	// disable, bis ein Plan geladen wird
	m_B_AutoPopulate.EnableWindow(true);
}

//***************************************************************************
// InitCCSEditControls: stellt Farb- und Formateinstellungen der 
//  CCSEditfelder ein.
//  R�ckgabe: keine
//***************************************************************************

void ShiftRoster_View::InitCCSEditControls(void)
{
	// Wertebereich der Periode
//	m_SBC_Periode.SetRange(0,ogBasicData.GetHugeCountOfWeeksForShiftRoster());


     m_SBC_Periode.SetRange(0,10000);
	// Editfeld 'G�ltig von' initialisiseren
	m_E_GPL_Vato.SetTypeToDate(true);
	m_E_GPL_Vato.SetBKColor(LTYELLOW);
	m_E_GPL_Vato.SetTextErrColor(RED);

	// Editfeld 'G�ltig bis' initialisiseren
	m_E_GPL_Vafr.SetTypeToDate(true);
	m_E_GPL_Vafr.SetBKColor(LTYELLOW);
	m_E_GPL_Vafr.SetTextErrColor(RED);
	m_E_GPL_Vafr.SetReadOnly();

	// Editfeld 'Schichtplan' initialisiseren
	m_E_ReleaseNote.SetTypeToString("X(60)",60,0);
	m_E_ReleaseNote.SetBKColor(WHITE);
	m_E_ReleaseNote.SetTextErrColor(RED);

	// Editfeld 'Abwesenheit' initialisiseren
	m_E_Absence.SetFormat("###");
	m_E_Absence.SetTextLimit(0,3);
	m_E_Absence.SetBKColor(WHITE);
	m_E_Absence.SetTextErrColor(RED);

	// Editfeld 'Urlaub' initialisiseren
	m_E_Holiday.SetFormat("###");
	m_E_Holiday.SetTextLimit(0,3);
	m_E_Holiday.SetBKColor(WHITE);
	m_E_Holiday.SetTextErrColor(RED);

	// Editfeld 'Aufrunden ab'/Nachkommateil initialisiseren
	m_E_Roundup.SetFormat("#");
	m_E_Roundup.SetTextLimit(0,1);
	m_E_Roundup.SetBKColor(WHITE);
	m_E_Roundup.SetTextErrColor(RED);

	// Editfeld 'Aufrunden ab'/Vorkommateil initialisiseren
	m_E_Null.SetInitText("0.");
	m_E_Null.SetBKColor(LIGHTSILVER2);

	// Editfeld 'Periode' initialisieren
	m_E_GPL_Peri.SetBKColor(LIGHTSILVER2);

	// Editfeld 'Startpoint' initialisieren
	m_E_GPL_StPt.SetTypeToDate(true);
	m_E_GPL_StPt.SetBKColor(LTYELLOW);
	m_E_GPL_StPt.SetTextErrColor(RED);
	m_E_GPL_StPt.SetReadOnly();
	

}

//***************************************************************************
// LoadStrings: l�dt die sprachabh�ngigen Strings f�r die Oberfl�chen-
//  elemente aus der entsprechenden Stringtable.
//  R�ckgabe: keine
//***************************************************************************

void ShiftRoster_View::LoadStrings(void)
{	
	// Doppelpunkt
	CString olDP =":";

	// View-Titel einstellen
	SetWindowText(LoadStg(SHIFT_CAPTION));

	// alle anderen, sprachabh�ngigen Strings f�r Buttons etc. laden
	m_B_New.SetWindowText(LoadStg(SHIFT_B_NEW));
	m_B_Copy.SetWindowText(LoadStg(SHIFT_B_COPY));
	m_B_Assign.SetWindowText(LoadStg(SHIFT_B_DEAKTIVATE));
	m_B_Delete.SetWindowText(LoadStg(SHIFT_B_DELETE));
	m_B_Bonus.SetWindowText(LoadStg(SHIFT_B_BONUS) + olDP);
	m_B_Info.SetWindowText(LoadStg(SHIFT_B_INFO));
	m_S_Week.SetWindowText(LoadStg(SHIFT_S_WEEK));
	m_S_Number.SetWindowText(LoadStg(SHIFT_S_NUMBER));
	m_S_Workgroup.SetWindowText(LoadStg(SHIFT_S_WORKGROUP));
	m_S_Staff.SetWindowText(LoadStg(SHIFT_S_STAFF));
	m_S_Mo.SetWindowText(LoadStg(SHIFT_S_MO));
	m_S_Tu.SetWindowText(LoadStg(SHIFT_S_TU));
	m_S_We.SetWindowText(LoadStg(SHIFT_S_WE));
	m_S_Th.SetWindowText(LoadStg(SHIFT_S_TH));
	m_S_Fr.SetWindowText(LoadStg(SHIFT_S_FR));
	m_S_Sa.SetWindowText(LoadStg(SHIFT_S_SA));
	m_S_Su.SetWindowText(LoadStg(SHIFT_S_SU));
	m_S_Houers.SetWindowText(LoadStg(SHIFT_S_HOUERS));
	m_S_Valid.SetWindowText(LoadStg(SHIFT_S_VALID) + olDP);
	m_S_Contract.SetWindowText(LoadStg(SHIFT_S_CONTRACT) + olDP);
	m_S_Holiday.SetWindowText(LoadStg(SHIFT_S_HOLIDAY) + olDP);
	m_S_Absence.SetWindowText(LoadStg(SHIFT_S_ABSENCE) + olDP);
	m_S_Roundup.SetWindowText(LoadStg(SHIFT_S_ROUNDUP) + olDP);
	m_B_Konsist.SetWindowText(LoadStg(SHIFT_KONSIST));
	m_B_SecondShift.SetWindowText(LoadStg(IDS_STRING168));		// Zweite Schicht zeigen*INTXT*
	m_B_AutoPopulate.SetWindowText(LoadStg(IDS_AutoRostering));		
	CString olDummy = LoadStg(IDS_STRING462);
	m_S_OrgUnit.SetWindowText(olDummy + olDP);

	m_S_GPL_StPt.SetWindowText(LoadStg(SHIFT_S_STPT));
	m_B_GPL_StPt.SetWindowText(LoadStg(SHIFT_B_STPT));

	SetDlgItemText(IDC_S_Shift,LoadStg(IDS_STRING1387));
	SetDlgItemText(IDC_S_Duty,LoadStg(IDS_STRING1392));
	SetDlgItemText(IDC_S_Periode,LoadStg(IDS_S_Period));
	SetDlgItemText(IDC_S_STFS,LoadStg(IDS_S_Anzahl));
	SetDlgItemText(IDC_S_Days,LoadStg(IDS_S_Days));
	SetDlgItemText(IDC_B_CHECK,LoadStg(ID_B_Check));
	SetDlgItemText(IDC_S_FILTER,LoadStg(IDS_W_Filter)+":");



}

//***************************************************************************
// RestoreWindowSettings: stellt die Sich- und Verf�gbarkeit der 
//  Oberfl�chenelemente wieder her.
//  R�ckgabe: keine
//***************************************************************************
void ShiftRoster_View::RestoreWindowSettings(void)
{
	// IDs der betreffenden Oberfl�chenelemente in einem Array speichern
	CUIntArray olCtrlIDs;
	olCtrlIDs.Add((UINT)IDC_B_NEW);				// ComboBox 'Ansicht'
	olCtrlIDs.Add((UINT)IDC_B_COPY);			// ComboBox 'Abdeckung'
	olCtrlIDs.Add((UINT)IDC_B_DEAKTIVATE);		// ComboBox 'Schichtplan'
	olCtrlIDs.Add((UINT)IDC_B_DELETE);			// Button 'Drucken'
	olCtrlIDs.Add((UINT)IDC_B_INFO);			// EditFeld 'Schichtplan' (Bemerkung)
	olCtrlIDs.Add((UINT)IDC_B_KONSIST);			// Editfeld 'G�ltig von'
	olCtrlIDs.Add((UINT)IDC_LB_GPLLIST);		// EditFeld 'G�ltig bis'
	olCtrlIDs.Add((UINT)IDC_E_ABSENCE);			// ComboBox 'Vertragstyp'
	olCtrlIDs.Add((UINT)IDC_S_ABSENCE);			// Editfeld 'Periode'
	olCtrlIDs.Add((UINT)IDC_STATIC2);			// SpinControl 'Periode'
	olCtrlIDs.Add((UINT)IDC_E_HOLIDAY);			// ComboBox 'Beaufschlagung'
	olCtrlIDs.Add((UINT)IDC_S_HOLIDAY);			// Button 'Bonus'
	olCtrlIDs.Add((UINT)IDC_STATIC1);			// Editfeld 'Urlaub'
	olCtrlIDs.Add((UINT)IDC_B_BONUS);			// Editfeld 'Abwesenheit'
	olCtrlIDs.Add((UINT)IDC_E_VALIDFROM);		// Editfeld 'aufrunden ab'
	olCtrlIDs.Add((UINT)IDC_E_VALIDTO);			// ListBox Grundschichtpl�ne
	olCtrlIDs.Add((UINT)IDC_S_VALID);			// ListBox Grundschichtpl�ne
	olCtrlIDs.Add((UINT)IDC_E_ROUNDUP);			// Button 'Info' �ber Grundschichtplan
	olCtrlIDs.Add((UINT)IDC_CB_CONTRACT);		// Button 'Neu'(er Grundschichtplan)IDC_S_CONTRACT
	olCtrlIDs.Add((UINT)IDC_S_CONTRACT);		// Button Grundschichtplan 'Kopieren'
	olCtrlIDs.Add((UINT)IDC_E_RELEASENOTE);		// Button Grundschichtplan 'Kopieren'
	olCtrlIDs.Add((UINT)IDC_CB_BONUS);			// Button Grundschichtplan 'Deaktivieren'
	olCtrlIDs.Add((UINT)IDC_SPIN_PERIOD);		// Button Grundschichtplan 'L�schen'
	olCtrlIDs.Add((UINT)IDC_S_Days);			// Button (Mitarbeiter) 'Ma. Konsistenz'
	olCtrlIDs.Add((UINT)IDC_B_STFS_P);			// Button Grundschichtplan 'Kopieren'
	olCtrlIDs.Add((UINT)IDC_S_STFS);			// Button Grundschichtplan 'Deaktivieren'
	olCtrlIDs.Add((UINT)IDC_B_STFS_M);			// Button Grundschichtplan 'L�schen'
	olCtrlIDs.Add((UINT)IDC_CB_ORGUNIT);		// Button Grundschichtplan 'L�schen'
	olCtrlIDs.Add((UINT)IDC_S_ORGUNIT);			// Button Grundschichtplan 'L�schen'

	olCtrlIDs.Add((UINT)IDC_S_GPL_STPT);		// Button Grundschichtplan 'L�schen'
	olCtrlIDs.Add((UINT)IDC_E_GPL_STPT);		// Button Grundschichtplan 'L�schen'
	olCtrlIDs.Add((UINT)IDC_B_GPL_STPT);		// Button Grundschichtplan 'L�schen'


	// Anzahl der Oberfl�chenelemente im Array
	int ilNoOfFlds = olCtrlIDs.GetSize();

	// String, der alle erforderlichen Feldnamen enth�lt, um die
	// Einastellungen aus der Datenbank restaurieren zu k�nnen
	CString olSecIDs  = "SR_NEW,SR_COPY,SR_DEACT,SR_DELETE,SR_INFO,SR_MCONST,SR_GPLLIST,SR_ABS,SR_ABS,SR_ABS,SR_HOLS,SR_HOLS,SR_HOLS,SR_BONUS,SR_VALID,SR_VALID,SR_VALID,SR_RNDUP,SR_CTRT_TYPE,SR_CTRT_TYPE,SR_RELNOTES,SR_BONUS,SR_DAYS,SR_DAYS,SR_ATPM,SR_ATPM,SR_ATPM,SR_ORG,SR_ORG";

	// F�r jedes Oberfl�chenelement...
	for(int ilLc = 0;ilLc<ilNoOfFlds; ilLc++)
	{
		// ...die ID ermitteln,...
		UINT ilCtrlID = olCtrlIDs.GetAt(ilLc);
		// ...den zugeh�rigen Datenbankfeldnamen ermitteln,...
		CString olSecID = GetListItem(olSecIDs,ilLc+1,FALSE,',');
		// ...ein Fensterobjekt erzeugen und...
		CWnd *polWnd = GetDlgItem(ilCtrlID);
		if(polWnd!=NULL)
		{
			// ...die Eigenschaften gem�� Feldwert aus DB setzen.
			switch(ogPrivList.GetStat(olSecID))
			{
			case '1':	// Element verf�gbar
				polWnd->EnableWindow(TRUE);
				break;
			case '0':	// Element nicht verf�gbar, aber sichtbar
				polWnd->EnableWindow(FALSE);
				break;
			case '-':	// Element nicht verf�gbar und unsichtbar
				polWnd->ShowWindow(SW_HIDE);
				break;
			case ' ':  // Element verf�gbar, keine Spezifikation in der Datenbank
				polWnd->EnableWindow(TRUE);
			default:
				break;
			}
		}
		else
		{
			printf("Error! No such window object!");
			// Error no such window object!!
		}
	}
} 


//***************************************************************************
// AddDebitDay: inkrementiert den Bedarf auf der Soll-Seite eines Tages des
//	Netto Schichtbedarfs <prpData>. Der Zeitbezug ist <opEsbg>, also der
//	aktuelle Bearbeitungszeitraum. Wenn kein g�ltiger Wochentag �ber
//	geben wurde (<ipDay> == 0, <bpIsDay> == FALSE), wird der aktuelle
//	Wochentag ermittelt.
//  R�ckgabe: keine
//***************************************************************************

void ShiftRoster_View::AddDebitDay(NSRTABLEDATA *prpData,CTime opEsbg,int ipDay/*=0*/,bool bpIsDay/*=false*/)
{
	CCS_TRY;
	// Wochentag, an dem ein Soll-Tag addiert wird
	int ilDay=0;
	// Wochentag aus Bearbeitungswoche ermitteln?
	if(bpIsDay == false)
	{
		// ja
		ilDay = opEsbg.GetDayOfWeek();
	}
	else
	{
		// nein -> Parameter benutzen
		ilDay = ipDay;
	}
	// 
	NSRTABLEDATA *prlData = FindNSRTableGrupp((CString)"");
	if(prlData == NULL)
	{
		NSRTABLEDATA rlData;
		prlData = &rlData;
	}
	switch(ilDay)
	{
	case 1:
		prpData->SunDebit++;
		prlData->SunDebit++;
		break;
	case 2:
		prpData->MonDebit++;
		prlData->MonDebit++;
		break;
	case 3:
		prpData->TueDebit++;
		prlData->TueDebit++;
		break;
	case 4:
		prpData->WndDebit++;
		prlData->WndDebit++;
		break;
	case 5:
		prpData->ThuDebit++;
		prlData->ThuDebit++;
		break;
	case 6:
		prpData->FriDebit++;
		prlData->FriDebit++;
		break;
	case 7:
		prpData->SatDebit++;
		prlData->SatDebit++;
		break;
	default:
		{
			// Do nothing
		}
		break; 
	}
	CCS_CATCH_ALL;
}

//***************************************************************************
// FindNSRTableGrupp: sucht den Datensatz mit dem Code <opBsdc>.
//  R�ckgabe: ein Zeiger auf den gefundenen Datensatz. NULL, wenn
//			  kein Datensatz gefunden wurde.
//***************************************************************************

NSRTABLEDATA* ShiftRoster_View::FindNSRTableGrupp(CString opBsdc, CString opFctc)
{
	CCS_TRY;
	NSRTABLEDATA *prlData;

	if(omNSRTableDataMap.Lookup(opBsdc + opFctc,(void *&)prlData) == TRUE)
	{
		return prlData;
	}

	return NULL;
	CCS_CATCH_ALL;
	return NULL;
}

//***************************************************************************
// Build_LB_GPL: f�llt die Grundschichtplan-Listbox <m_LB_GPLList>.
//	Alle bisherigen Eintr�ge werden vorher gel�scht.
//  R�ckgabe: keine
//***************************************************************************

void ShiftRoster_View::Build_LB_GPL(CString opFilter/*=""*/)
{
	CCS_TRY;
	// Textpuffer
	CString olLine, olTmpTxt;
	// Listbox-Inhalt l�schen
	m_LB_GPLList.ResetContent();
	
	// Sortierung der Grundschichtpl�ne nach Name einstellen
	ogBCD.SetSort("GPL","GSNM",true);
	// Datensatz-Objekt mit genausoviel Feldern wie f�r die Tabelle
	// GPL (Grundschichtplan) ben�tigt erzeugen
	RecordSet olRecord(ogBCD.GetFieldCount("GPL"));
	// jeden Datensatz in GPL durchgehen

	CString olOrgUnit;
	CString olPriv;
	bool blAdd;
	int ilDpt1Idx = ogBCD.GetFieldIndex(omObject,"DPT1");

	for (int i = 0; i < ogBCD.GetDataCount("GPL"); i++)
	{
		ogBCD.GetRecord("GPL",i, olRecord);
		blAdd = false;

		if (ogCCSParam.GetParamValue("GLOBAL","ID_ORG_SECURITY") == "N")
		{
			blAdd = true;
		}
		else
		{
			if (ilDpt1Idx < 0)
			{
				blAdd = true;
			}
			else
			{
				olOrgUnit = olRecord.Values[ilDpt1Idx];
				if (strlen(olOrgUnit) > 0)
				{
					olPriv = ogPrivList.GetStat(olOrgUnit);
					if (olPriv == "-" || olPriv == "0")
					{
						blAdd = false;
					}
					else
					{
						blAdd = true;
					}
				}
				else
				{
					blAdd = true;
				}
			}
		}

		if (blAdd == true)
		{
			// Datensatz in Listbox einf�gen
			InsertIn_LB_GPL(&olRecord, opFilter);
		}
	}
	// ersten Eintrag selektieren
	m_LB_GPLList.SetCaretIndex(0);
	CCS_CATCH_ALL;
}

//***************************************************************************
// InsertIn_LB_GPL: f�gt einen Eintrag (Datensatz) in die Listbox
//***************************************************************************

void ShiftRoster_View::InsertIn_LB_GPL(RecordSet *popRecord, CString opFilter/*= ""*/) 
{
	CCS_TRY;
 
	CString olName = popRecord->Values[ogBCD.GetFieldIndex("GPL","GSNM")];
	CString olGPLsplu = popRecord->Values[ogBCD.GetFieldIndex("GPL","SPLU")];
	CString olLine;
	olLine.Format("  %-32s      URNO=%s",olName,popRecord->Values[ogBCD.GetFieldIndex("GPL","URNO")]);

	if (olGPLsplu != "")
	{
		CString olSPLTxt;
		olLine.Format(". %-32s      URNO=%s",olName,popRecord->Values[ogBCD.GetFieldIndex("GPL","URNO")]);

		int ilSelSPLItem = pomComboBoxShiftplan->GetCurSel();
		if (ilSelSPLItem != CB_ERR)
		{
			pomComboBoxShiftplan->GetLBText (ilSelSPLItem, olSPLTxt);
		}

		if (ilSelSPLItem != CB_ERR && olSPLTxt != "")
		{
			CString olSPLurno = "0";
			if (olSPLTxt.GetLength() > (olSPLTxt.Find("URNO=")+5))
			{
				olSPLurno = olSPLTxt.Mid(olSPLTxt.Find("URNO=")+5);
			}

			if (olSPLurno == olGPLsplu)
			{
				olLine.Format("%s %-32s      URNO=%s",omAktiv,olName,popRecord->Values[ogBCD.GetFieldIndex("GPL","URNO")]);
			}
		}
	}
	if (imViewShowGPL == VIEW_ALLGPL || (imViewShowGPL == VIEW_NOTALLGPL && olGPLsplu != "" && olGPLsplu == omSelSPLurno))
	{
		CString olTmp = olLine.Mid(2,opFilter.GetLength());
		olTmp.MakeUpper();
		if (!opFilter.GetLength() || opFilter == olTmp)
		{
			m_LB_GPLList.AddString(olLine);
		}
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// Ansicht �ndern
//**********************************************************************************

void ShiftRoster_View::OnBView() 
{
	CCS_TRY;
	ShiftRosterPropertySheet olDlg(LoadStg(SHIFT_SHEET_BASE_CAPTION),this,pomViewer);

	if(olDlg.DoModal() != IDCANCEL)
	{
		UpdateComboBox();
		
		OnSelchangeCbView();
	}
	else
	{
		CString olOldString;
		CString olNewString;
		pomComboBoxView->GetWindowText(olOldString);
		UpdateComboBox();
		pomComboBoxView->GetWindowText(olNewString);

		if (olNewString != olOldString)
		{
			//if old string wasn't deleted, we have to select it again
			int ilIndex = pomComboBoxView->FindStringExact(-1,olOldString);
			if(ilIndex != CB_ERR)
			{
				pomComboBoxView->SetCurSel(ilIndex);
			}
			else
			{
				OnSelchangeCbView();
			}
		}
	}

	CCS_CATCH_ALL;
}

//**********************************************************************************
// 
//**********************************************************************************

void ShiftRoster_View::UpdateComboBox()
{
	CCS_TRY;
	pomComboBoxView->ResetContent();
	CStringArray olStrArr;
	pomViewer->GetViews(olStrArr);
	for(int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		pomComboBoxView->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = pomViewer->GetViewName();
	if(olViewName.IsEmpty() == TRUE)
	{
		olViewName = pomViewer->SelectView();
	}
	
	ilIndex = pomComboBoxView->FindStringExact(-1,olViewName);
	
	if(ilIndex != CB_ERR)
	{
		pomComboBoxView->SetCurSel(ilIndex);
	}

	CCS_CATCH_ALL;
}

//**********************************************************************************
// 
//**********************************************************************************

void ShiftRoster_View::OnBRequirement() 
{
	CCS_TRY;
	CString olSDTTxt;
	int ilSelItem = pomComboBoxCoverage->GetCurSel();
	if(ilSelItem != CB_ERR)
	{
		pomComboBoxCoverage->GetLBText(ilSelItem,olSDTTxt);
		olSDTTxt = olSDTTxt.Left(32);
		olSDTTxt.TrimRight();
		if(olSDTTxt != "")
		{
			int ilCount = ogSdgData.omData.GetSize();
			SDGDATA *prlSdgData = NULL;
			for (int i = 0; i < ilCount; i++)
			{
				prlSdgData = &ogSdgData.omData[i];
				if (strcmp(prlSdgData->Dnam, olSDTTxt.GetBuffer(0)) == 0)
				{
					CString olInfoText;
					CString olDp = ":  ";
					CString olCRLF = "\n";
					CString olBegi = prlSdgData->Begi.Format("%d.%m.%Y");
					CString olDays = prlSdgData->Days;
					CString olDura = prlSdgData->Dura;
					CString olPeri = prlSdgData->Peri;
					
					CString olRepi = prlSdgData->Repi;
					CStringArray olArray;
					int ilSize = ExtractItemList(olRepi,&olArray, ';');
					olRepi.Empty();
					for(int j=0; j<ilSize; j++)
					{
						olRepi += CString(", ") + olArray[j];
					}
					if(olRepi.IsEmpty() == FALSE && olRepi.GetLength() > 2)
					{
						olRepi = olRepi.Mid(2);
					}
					else
					{
						olRepi = "-";
					}
					
					CTime olTBegi = DateStringToDate(olBegi);
					CTime olTEnde = olTBegi;
					for(j=0; j<ilSize; j++)
					{
						CTime olTTmp = DateStringToDate(olArray[j]);
						if(olTTmp>olTEnde)
							olTEnde = olTTmp;
					}
					int ilAddDays = atoi(olDays) + ( (atoi(olDura)-1) * atoi(olDays) ) + ( (atoi(olDura)-1) * (atoi(olPeri)-1) * atoi(olDays) );
					COleDateTime olDTEnde;
					if(olTEnde != -1)
						olDTEnde = COleDateTime(olTEnde.GetYear(), olTEnde.GetMonth(), olTEnde.GetDay(),0,0,0) + COleDateTimeSpan(ilAddDays-1,0,0,0);
					CString olEnd = olDTEnde.Format("%d.%m.%Y");
					CString olWDays;
					olWDays.Format("%d",ilAddDays);
					olInfoText  = LoadStg(IDS_STRING1631) + olDp + olSDTTxt + olCRLF + olCRLF;
					olInfoText += LoadStg(IDS_STRING1632) + olDp + olBegi   + olCRLF;
					olInfoText += LoadStg(IDS_STRING1633) + olDp + olDays   + olCRLF;
					olInfoText += LoadStg(IDS_STRING1634) + olDp + olPeri   + olCRLF;
					olInfoText += LoadStg(IDS_STRING1635) + olDp + olDura   + olCRLF;
					olInfoText += LoadStg(IDS_STRING1636) + olDp + olRepi   + olCRLF + olCRLF;
					olInfoText += LoadStg(IDS_STRING1639) + olDp + olWDays  + olCRLF;
					olInfoText += LoadStg(IDS_STRING1638) + olDp + olBegi   + CString(" - ") + olEnd;

					MessageBox(olInfoText,LoadStg(IDS_STRING1637),MB_ICONINFORMATION);
					i=ilCount;
				}
			}
		}
		else
		{
			Beep(440,70);
			MessageBox(LoadStg(IDS_STRING1641), LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
		}
	}
	else
	{
		Beep(440,70);
		MessageBox(LoadStg(IDS_STRING1641), LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// 
//**********************************************************************************

void ShiftRoster_View::OnBPlan() 
{
	CCS_TRY;
	int ilMBoxReturn = IDYES;
	if(bmIsChangedBonus == true)
	{
		ilMBoxReturn = MessageBox(LoadStg(SHIFT_ISCHANGED_BONUS),LoadStg(ST_FRAGE),(MB_ICONQUESTION | MB_YESNOCANCEL | MB_DEFBUTTON1));
		if(ilMBoxReturn == IDYES)
		{
			SaveBonus();
			if(bmIsChangedBonus == true)
			{
				ilMBoxReturn = IDCANCEL;
			}
		}
	}
	if(ilMBoxReturn != IDCANCEL)
	{
		CString olSPLName,olLine,olTmp,olSPLurno,olSPLdbou;
		CString olOrgUnit;
		CString olAction;

		ShiftPlanDlg olDlg(&olSPLName,&olOrgUnit,&olAction,this);
		if (olDlg.DoModal() != IDCANCEL)
		{
			AfxGetApp()->DoWaitCursor(1);

			CWaitDlg	olWaitDlg(this);
			
			if (ogBCD.GetField("SPL", "SNAM", olSPLName, "URNO", olSPLurno) == true)
			{
				if (olAction == "URT")
				{
					// it is just an update of the OrgUnit
					if (ogCCSParam.GetParamValue("GLOBAL","ID_ORG_SECURITY") == "Y")
					{
						if (ogBCD.GetFieldIndex("SPL","DPT1") > -1)
						{
							ogBCD.SetField("SPL","SNAM",olSPLName,"DPT1",olOrgUnit,true);
						}
					}

					// set the selection of the combo
					RecordSet olRecord;
					bool blRet = ogBCD.GetRecord("SPL","SNAM",olSPLName,olRecord);
					if (blRet == true)
					{
						int ilSnamIdx = ogBCD.GetFieldIndex("SPL","SNAM");
						int ilUrnoIdx = ogBCD.GetFieldIndex("SPL","URNO");
						olLine.Format("%-32s                                    URNO=%s",olRecord.Values[ilSnamIdx],olRecord.Values[ilUrnoIdx]);
						int ilIdx =  pomComboBoxShiftplan->FindStringExact(-1,olLine);
						if (ilIdx != CB_ERR )
						{
							pomComboBoxShiftplan->SetCurSel(ilIdx);
							omSelSPLurno = olRecord.Values[ilUrnoIdx];
						}
					}
				}
				else if (olAction == "DRT")
				{
					bool blRet = true;
					// delete DBO-records belonging to the SPL and delete SPL itself
					if (ogBCD.GetField("SPL", "URNO", olSPLurno, "DBOU", olSPLdbou) == true)
					{
						CStringArray olUrnoArray;
						int ilUrnos  = ExtractItemList(olSPLdbou,&olUrnoArray,'|');
						for (int i = 0; i < ilUrnos; i++)
						{
							blRet = ogBCD.DeleteRecord("DBO", "URNO", olUrnoArray[i], true);
							if (!blRet)
								break;
						}
					}
					

					if (blRet)
					{
						// update GPLs belonging to the deleted SPL (reference: SPL.URNO <-> GPL.SPLU)
						RecordSet *polGPLRecord = new RecordSet(ogBCD.GetFieldCount("GPL"));
						int ilGPLspluIdx = ogBCD.GetFieldIndex("GPL","SPLU");
						int ilGPLurnoIdx = ogBCD.GetFieldIndex("GPL","URNO");

						for (long l = 0; l < ogBCD.GetDataCount("GPL"); l++)
						{
							ogBCD.GetRecord("GPL",l, *polGPLRecord);
							if (polGPLRecord->Values[ilGPLspluIdx] == olSPLurno)
							{
								blRet = ogBCD.SetField("GPL","URNO",polGPLRecord->Values[ilGPLurnoIdx],"SPLU","",true);
								if (!blRet)
									break;
							}
						}
						delete polGPLRecord;
					}

					if (blRet)
					{
						blRet = ogBCD.DeleteRecord("SPL", "URNO", olSPLurno, true);

						if (blRet)
						{
							// delete SPL-entry from combobox
							olLine.Format("%-32s                                    URNO=%s",olSPLName,olSPLurno);
							int ilIdx =  pomComboBoxShiftplan->FindStringExact(-1,olLine);
							if (ilIdx != CB_ERR )
							{
								pomComboBoxShiftplan->DeleteString(ilIdx);
							}
							omSelSPLurno = "0000000000";
						}
					}
				}
			}
			else
			{
				// it is an insert
				pomRecord = new RecordSet(ogBCD.GetFieldCount("SPL"));

				int ilSnamIdx = ogBCD.GetFieldIndex("SPL","SNAM");
				int ilUrnoIdx = ogBCD.GetFieldIndex("SPL","URNO");
				int ilDpt1Idx = ogBCD.GetFieldIndex("SPL","DPT1");

				pomRecord->Values[ilSnamIdx] = olSPLName;
				if (ilDpt1Idx > -1)
				{
					pomRecord->Values[ilDpt1Idx] = olOrgUnit;
				}
				CTime olTime = CTime::GetCurrentTime();
				pomRecord->Values[ogBCD.GetFieldIndex("SPL","CDAT")] = olTime.Format("%Y%m%d%H%M00");
				pomRecord->Values[ogBCD.GetFieldIndex("SPL","USEC")] = pcgUser;
				ogBCD.InsertRecord("SPL",*pomRecord,true);

				// update selection of the combo
				olLine.Format("%-32s                                    URNO=%s",pomRecord->Values[ilSnamIdx],pomRecord->Values[ilUrnoIdx]);
				int ilIdx =  pomComboBoxShiftplan->AddString(olLine);
				pomComboBoxShiftplan->SetCurSel(ilIdx);
				omSelSPLurno = pomRecord->Values[ilUrnoIdx];
				delete pomRecord;
			}

			// Listbox mit den Grundschichtpl�nen wird gef�llt
			OnChangeEGplFilter();
			Build_Bonus(omSelSPLurno);
			GetShiftRequirementData(SHIFTPLAN);
			RemoveWaitCursor();
		}
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// 
//**********************************************************************************

void ShiftRoster_View::OnBSaveplan()     // caonima save
{
	CCS_TRY;

	

	
	m_B_Bonus.SetFocus();

	if (bmIsChangedGPL == true)
	{
	
		
		SaveGPL();      // here
	}
	if (bmIsChangedBonus == true)
	{
		
		
		
		SaveBonus();
	}

	CCS_CATCH_ALL;
}

//**********************************************************************************
// OnBRelease: Freigabe der Daten und Speichern in Datenbank.
// R�ckgabe: keine
//**********************************************************************************

void ShiftRoster_View::OnBRelease() 
{
	CCS_TRY;

	int ilMBoxReturn = IDOK;

	if (bmIsChangedGPL == true || bmIsChangedBonus == true)
	{
		ilMBoxReturn = MessageBox(LoadStg(IDS_STRING1642),LoadStg(ST_FRAGE),(MB_ICONQUESTION | MB_OKCANCEL | MB_DEFBUTTON1));
		if (ilMBoxReturn == IDOK)
		{
			OnBSaveplan(); // GSP (GrundSchichtPlan) und Bonus (DBOTAB) abspeichern
		}
		else
		{
			return;
		}
	}

	CString olSPLurno = GetSelectedShiftplanUrno();
	if (olSPLurno == "0")
	{
		Beep(440,70);
		MessageBox(LoadStg(SHIFT_SELECT_PLAN), LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
		return;
	}

	ShiftReleaseDlg* polShiftReleaseDlg;
	bool blShowWorkGroups = false;
	if (omCustomer == "SIN")
	{
		blShowWorkGroups = true;
	}

	polShiftReleaseDlg = new ShiftReleaseDlg(this,blShowWorkGroups);
	polShiftReleaseDlg->DoModal();
	delete polShiftReleaseDlg;
CCS_CATCH_ALL;
}

void ShiftRoster_View::Release(CString opSplUrno, COleDateTime opFrom, COleDateTime opTo, CMapStringToPtr *popStaffMap, bool bpDelete) 
{
CCS_TRY;
	AfxGetApp()->DoWaitCursor(1);

	// WarteDialog einblenden.
	CWaitDlg olWaitDlg(this);

	if (this->bmClientRelease == true)
	{
		int ilGPLspluIdx = ogBCD.GetFieldIndex("GPL","SPLU");
		int ilGPLurnoIdx = ogBCD.GetFieldIndex("GPL","URNO");
		int ilGPLperiIdx = ogBCD.GetFieldIndex("GPL","PERI");
		int ilGPLvafrIdx = ogBCD.GetFieldIndex("GPL","VAFR");
		int ilGPLvatoIdx = ogBCD.GetFieldIndex("GPL","VATO");

		// Zeitraum in COleDateTime-Instanzen speichern
		COleDateTime olDrrStart	= opFrom;
		COleDateTime olDrrEnd	= opTo;

		// um evtl. Key des zu l�schenden DRRs aus Key-Map zu entfernen
		CString olPrimaryKey;

		// DRR-Daten laden
		if ((olDrrStart.GetStatus() != COleDateTime::valid) || (olDrrEnd.GetStatus() != COleDateTime::valid))
		{
			RemoveWaitCursor();
			return;
		}

		// Synchronisation mit anderen Clients
		if (!ogDrrData.Synchronise("SHIFTROSTER",true))
		{
			// nein -> Fehlermeldung ausgeben
			MessageBeep((UINT)-1);
			MessageBox(LoadStg(IDS_STRING1574),LoadStg(IDS_STRING442),MB_ICONEXCLAMATION);
			// normalen Cursor laden
			RemoveWaitCursor();
			// terminieren
			return;
		}

		// Fill Staff-Shift-Array /////////// 
		CStringArray olStfUrnoArray;
		CPtrArray olStaffData;

		CString olGPLUrno;
		int ilGPLCount =  ogBCD.GetDataCount("GPL");

		RecordSet *polGPLRecord;
		polGPLRecord = new RecordSet(ogBCD.GetFieldCount("GPL"));
		STFDATA* prlStfData = NULL;

		// search all GPL's to the actuall SPL-Urno
		for (int i = 0; i < ilGPLCount; i++)
		{
			ogBCD.GetRecord("GPL",i, *polGPLRecord);

			// search all GSP's to the actuall GPL-Urno
			if (polGPLRecord->Values[ilGPLspluIdx] == opSplUrno)
			{
				olGPLUrno = polGPLRecord->Values[ilGPLurnoIdx];
				int ilPeri = atoi(polGPLRecord->Values[ilGPLperiIdx]);
				if (ilPeri > 0)
				{
					int ilGPLWeeks = ilPeri / 7;

					CStringArray olShiftArray;
					CStringArray olShift2Array;
					CStringArray olPfcArray;
					CStringArray olPfc2Array;

					olShiftArray.SetSize(ilPeri);
					olShift2Array.SetSize(ilPeri);
					olPfcArray.SetSize(ilPeri);
					olPfc2Array.SetSize(ilPeri);

					int ilGSPCount =  ogBCD.GetDataCount("GSP");
					RecordSet *polGSPRecord = new RecordSet(ogBCD.GetFieldCount("GSP"));
					int ilWeekCount = 0;
					for (int m = 0; m < ilGSPCount; m++)
					{
						ogBCD.GetRecord("GSP",m, *polGSPRecord);
						if(polGSPRecord->Values[imGSPgpluIdx] == olGPLUrno)
						{
							// Um welche Woche handelt es sich
							int ilGSPWeek = atoi(polGSPRecord->Values[imGSPweekIdx]);

							CString olBsd;
							CString olBsd2;
							CString olFctc;
							CString olFctc2;
							for (int ilDay = 0; ilDay < 7; ilDay++)
							{
								if (bpDelete == false)
								{
									switch(ilDay)
									{
									case 0:
										olBsd	= polGSPRecord->Values[imGSPbumoIdx];
										olBsd2	= polGSPRecord->Values[imGSPsumoIdx];
										olFctc	= polGSPRecord->Values[imGSPp1moIdx];
										olFctc2	= polGSPRecord->Values[imGSPp2moIdx];
										break;
									case 1:
										olBsd	= polGSPRecord->Values[imGSPbutuIdx];
										olBsd2	= polGSPRecord->Values[imGSPsutuIdx];
										olFctc	= polGSPRecord->Values[imGSPp1tuIdx];
										olFctc2	= polGSPRecord->Values[imGSPp2tuIdx];
										break;
									case 2:
										olBsd	= polGSPRecord->Values[imGSPbuweIdx];
										olBsd2	= polGSPRecord->Values[imGSPsuweIdx];
										olFctc	= polGSPRecord->Values[imGSPp1weIdx];
										olFctc2	= polGSPRecord->Values[imGSPp2weIdx];
										break;
									case 3:
										olBsd	= polGSPRecord->Values[imGSPbuthIdx];
										olBsd2	= polGSPRecord->Values[imGSPsuthIdx];
										olFctc	= polGSPRecord->Values[imGSPp1thIdx];
										olFctc2	= polGSPRecord->Values[imGSPp2thIdx];
										break;
									case 4:
										olBsd	= polGSPRecord->Values[imGSPbufrIdx];
										olBsd2	= polGSPRecord->Values[imGSPsufrIdx];
										olFctc	= polGSPRecord->Values[imGSPp1frIdx];
										olFctc2	= polGSPRecord->Values[imGSPp2frIdx];
										break;
									case 5:
										olBsd	= polGSPRecord->Values[imGSPbusaIdx];
										olBsd2	= polGSPRecord->Values[imGSPsusaIdx];
										olFctc	= polGSPRecord->Values[imGSPp1saIdx];
										olFctc2	= polGSPRecord->Values[imGSPp2saIdx];
										break;
									case 6:
										olBsd	= polGSPRecord->Values[imGSPbusuIdx];
										olBsd2	= polGSPRecord->Values[imGSPsusuIdx];
										olFctc	= polGSPRecord->Values[imGSPp1suIdx];
										olFctc2	= polGSPRecord->Values[imGSPp2suIdx];
										break;
									}
								}
								else
								{
									olBsd	= "0";
									olBsd2	= "0";
									olFctc	= "0";
									olFctc2	= "0";
								}

								// Hier werden die Schichtcodes in das ShiftArray geschrieben:
								// Position setzt sich aus Woche und Tag zusammen erste Schicht:
								olShiftArray.SetAtGrow ((ilGSPWeek - 1) * 7 + ilDay, olBsd);
								//olShiftArray.Add (olBsd);
								// zweite Schicht:
								olShift2Array.SetAtGrow ((ilGSPWeek - 1) * 7 + ilDay, olBsd2);
								//olShift2Array.Add (olBsd2);
								// erste Funktion:
								olPfcArray.SetAtGrow ((ilGSPWeek - 1) * 7 + ilDay, olFctc);
								//olPfcArray.Add (olFctc);
								// zweite Funktion:
								olPfc2Array.SetAtGrow ((ilGSPWeek - 1) * 7 + ilDay, olFctc2);
								//olPfc2Array.Add (olFctc2);
							}
							//-------------------------------------------------
							// ShiftArray ist gef�llt
							//-------------------------------------------------

							CStringArray olStaffArray;
							void  *prlVoid = NULL;
							int ilStaffSize = ExtractItemList(polGSPRecord->Values[imGSPstfuIdx], &olStaffArray, '|');
							for (int ilStaff = 0; ilStaff < ilStaffSize; ilStaff++)
							{
								if (popStaffMap->Lookup(olStaffArray[ilStaff],(void *&)prlVoid) == TRUE)
								{
									CTime olTTmp;
									CString olSTmp;

									//employees
									REL_STAFF_DATA *prlStaff = new REL_STAFF_DATA;
									prlStaff->iBeginWeek = ilGSPWeek;
									prlStaff->lStfUrno = atol(olStaffArray[ilStaff]);

									prlStfData = ogStfData.GetStfByUrno (prlStaff->lStfUrno);
									if (prlStfData != NULL)
									{
										//prlStaff->oDOBK = prlStfData->Dobk;
										prlStaff->oDODM = prlStfData->Dodm; //leaving date
										prlStaff->oDOEM = prlStfData->Doem; //entry date
									}

									//valid from
									olSTmp = polGPLRecord->Values[ilGPLvafrIdx];
									olTTmp = DBStringToDateTime(olSTmp);
									if (olTTmp != -1)
									{
										prlStaff->oGPLVafr = CTimeToCOleDateTime(olTTmp);
										if (prlStaff->oGPLVafr < opFrom)
										{
											prlStaff->oRelFrom = opFrom;
										}
										else
										{
											prlStaff->oRelFrom = prlStaff->oGPLVafr;
										}
									}
									if (prlStaff->oDOEM.GetStatus() == COleDateTime::valid)
									{
										if (prlStaff->oDOEM > prlStaff->oRelFrom)
										{
											prlStaff->oRelFrom = prlStaff->oDOEM;
										}
									}

									//valid to
									olSTmp = polGPLRecord->Values[ilGPLvatoIdx];
									olTTmp = DBStringToDateTime(olSTmp);
									if(olTTmp != -1)
									{
										prlStaff->oGPLVato = CTimeToCOleDateTime(olTTmp);
										if (prlStaff->oGPLVato > opTo)
										{
											prlStaff->oRelTo = opTo;
										}
										else
										{
											prlStaff->oRelTo = prlStaff->oGPLVato;
										}
									}
									if (prlStaff->oDODM.GetStatus() == COleDateTime::valid)
									{
										if (prlStaff->oDODM < prlStaff->oRelTo)
										{
											prlStaff->oRelTo = prlStaff->oDODM;
										}
									}

									olStaffData.Add((void*)prlStaff);
								}
							}
							ilWeekCount++;
						} 
						//-------------------------------------------------
						// der GLP Eintrag wurde komplett bearbeitet
						//-------------------------------------------------

						if(ilGPLWeeks == ilWeekCount)
						{
							m = ilGSPCount;
						}
					}

					// Daten mit Filter Zeitraum ermitteln
					CString olStaffUrnos;
					CMapPtrToPtr olLoadStfUrnoMap;
					REL_STAFF_DATA *prlStaff;
					for (int ilStaffData = 0; ilStaffData < olStaffData.GetSize(); ilStaffData++)
					{
						prlStaff = (REL_STAFF_DATA*)olStaffData[ilStaffData];
						olLoadStfUrnoMap.SetAt((void *)prlStaff->lStfUrno, NULL);
					}

					if (ogDrrData.ReadFilteredByDateAndStaff(olDrrStart,olDrrEnd,&olLoadStfUrnoMap,true,false) == false)
					{
						// Fehlermeldung abfragen
						ogDrrData.omLastErrorMessage.MakeLower();

						// Daten laden OK?
						if (ogDrrData.omLastErrorMessage.Find("no data found") == -1)
						{
							// nein -> Fehlermeldung ausgeben
							CString olErrorTxt;
							olErrorTxt.Format("%s %d\n%s",LoadStg(ST_READERR), ogDrrData.imLastReturnCode, ogDrrData.omLastErrorMessage);
							MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
							// normalen Cursor laden
							RemoveWaitCursor();
							return;
						}
					}

					//----------------------------------------------------------------
					//Jedem MA die erzeugte die Schichtkette erzeugte anh�ngen
					int ilStaffDataSize = olStaffData.GetSize();
					for (ilStaffData=0; ilStaffData < ilStaffDataSize; ilStaffData++)
					{
						REL_STAFF_DATA *prlStaff = (REL_STAFF_DATA*)olStaffData[ilStaffData];
						
						// Liste mit Urnos wird an die "andere" Liste angef�gt
						// olShiftArray hat hier eine Array mit allen Schichten (ohne Rotationen)
						// also jede Woche ist nur einmal vorhanden.
						prlStaff->oShifts.Append(olShiftArray);
						prlStaff->oShifts2.Append(olShift2Array);
						prlStaff->oFunctions.Append(olPfcArray);
						prlStaff->oFunctions2.Append(olPfc2Array);

						//----------------------------------------------------------------
						//Beginn-Tag (die erste Schicht) der Schichtkette umsetzen
						int ilBeginDayOfWeek = 0;

						switch (prlStaff->oRelFrom.GetDayOfWeek())
						{
						case 1:
							ilBeginDayOfWeek = 6;
							break;
						case 2:
							ilBeginDayOfWeek = 0;
							break;
						case 3:
							ilBeginDayOfWeek = 1;
							break;
						case 4:
							ilBeginDayOfWeek = 2;
							break;
						case 5:
							ilBeginDayOfWeek = 3;
							break;
						case 6:
							ilBeginDayOfWeek = 4;
							break;
						case 7:
							ilBeginDayOfWeek = 5;
							break;
						}
						//---------------------------------------------------------------------------
						// Hier wird die Schichtkette f�r den einzelnen MA je nach Startwoche verschoben
						//---------------------------------------------------------------------------
						
						int ilMoveDays = 0;
						ilMoveDays = (prlStaff->iBeginWeek - 1) * 7 + ilBeginDayOfWeek;

						CString olMoveStr;
						for (int ilMove = 0; ilMove < ilMoveDays; ilMove++)
						{
							// erste Schicht
							olMoveStr = prlStaff->oShifts[0];
							prlStaff->oShifts.RemoveAt(0);
							prlStaff->oShifts.Add(olMoveStr);

							// zweite Schicht
							olMoveStr = prlStaff->oShifts2[0];
							prlStaff->oShifts2.RemoveAt(0);
							prlStaff->oShifts2.Add(olMoveStr);

							// erste Funktion
							olMoveStr = prlStaff->oFunctions[0];
							prlStaff->oFunctions.RemoveAt(0);
							prlStaff->oFunctions.Add(olMoveStr);

							//zweite Funktion
							olMoveStr = prlStaff->oFunctions2[0];
							prlStaff->oFunctions2.RemoveAt(0);
							prlStaff->oFunctions2.Add(olMoveStr);
						}
						//---------------------------------------------------------------------------
					}
					delete polGSPRecord;
				}

				// DRR erzeugen oder �ndern
				// DRR-Z�hler, nur f�r Test-Zwecke
				int nCountDrr = 1;
				int ilStaffDataSize = olStaffData.GetSize();
				DRRDATA *prlDrr	= NULL;
				CString olRosl1 = "1";
				CString olRosl2 = "2";
				CString olDrrn1 = "1";
				CString olDrrn2 = "2";
				bool blSetEdit;

				//---------------------------------------------------------------------------
				// Alle Mitarbeiter durchgehen. Array wurde vorher erzeugt
				//---------------------------------------------------------------------------
				
				for (int ilStaffData = 0; ilStaffData < ilStaffDataSize; ilStaffData++)
				{
					REL_STAFF_DATA *prlStaff = (REL_STAFF_DATA*)olStaffData[ilStaffData];
					COleDateTimeSpan olGPLSpan = prlStaff->oRelTo - prlStaff->oRelFrom;
					COleDateTimeSpan olDayOffset = prlStaff->oRelFrom - prlStaff->oGPLVafr;
					int ilGPLDays = (int)olGPLSpan.GetDays() + 1;
					int ilDayOffset = (int)olDayOffset.GetDays();
					ilDayOffset = ilDayOffset - (ilDayOffset % 7);
					if (ilDayOffset < 0)
						ilDayOffset = 0;

					// F�r jeden Tag in dem Zeitraum f�r diesen Mitarbeiter
					for (int ilDay = 0; ilDay < ilGPLDays; ilDay++)
					{
						// gibt es eine 1. Schicht? Nur dann auch 2. Schicht m�glich.
						bool blFirstShift = true;

						// aktuellen Tag ermitteln
						COleDateTime olDrrDay = prlStaff->oRelFrom + COleDateTimeSpan(ilDay,0,0,0);

						// Mitarbeiter-Urno
						long llStfUrno = prlStaff->lStfUrno;

						//**********************************************************************************************
						// Erste Schichten
						//**********************************************************************************************

						// Schicht an diesem Tag aus der oben generierten Schichtkette ermitteln
						int ilElement = ilDay+ilDayOffset;
						int ilTmpSize = prlStaff->oShifts.GetSize();

						// Rotation durchf�hren
						while (ilElement >= ilTmpSize)
						{
							ilElement -= ilTmpSize;
							if (ilTmpSize == 0)
							{
								ilElement = -1;
							}
						}

						// Wenn eine g�ltige Schicht vorhanden ist
						long llUrno = atol(prlStaff->oShifts[ilElement]);
						if (llUrno != 0)
						{
							// DRR-Datensatz suchen
							prlDrr = ogDrrData.GetDrrByKey (olDrrDay.Format("%Y%m%d"), llStfUrno, olDrrn1, olRosl1);
							if (prlDrr == NULL)
							{
								// Datensatz nicht gefunden -> neuen anlegen
								prlDrr = ogDrrData.CreateDrrData(llStfUrno, olDrrDay.Format("%Y%m%d"), olRosl1, "", olDrrn1, true);
								blSetEdit = false;
							}
							else
							{
								//strcpy(prlDrr->Scoo, prlDrr->Scod);
								blSetEdit = true;
							}

							if (ilElement >= 0)
							{
								bool	blCodeIsBsd = (ogBsdData.GetBsdByUrno(llUrno) != 0);
								PFCDATA *polPfcData = ogPfcData.GetPfcByUrno (atol(prlStaff->oFunctions[ilElement]));
								if (polPfcData != NULL)
								{
									if (!ogDrrData.SetDrrDataScod(prlDrr,blCodeIsBsd,llUrno,polPfcData->Fctc,blSetEdit,true))
									{
										TRACE("Vorhandener DRR-Datensatz Nr.%d f�r MA-Urno %d, Tag %s. darf nicht ge�ndert werden\n",nCountDrr++,llStfUrno,olDrrDay.Format("%d.%m.%Y"));
									}
								}
								else
								{
									if (!ogDrrData.SetDrrDataScod(prlDrr,blCodeIsBsd,llUrno,"",blSetEdit,true))
									{
										TRACE("Vorhandener DRR-Datensatz Nr.%d f�r MA-Urno %d, Tag %s. darf nicht ge�ndert werden\n",nCountDrr++,llStfUrno,olDrrDay.Format("%d.%m.%Y"));
									}
								}
							}
						}
						else
						{
							// damit keine 2. Schicht mehr angelegt wird
							blFirstShift = false;

							// evtl. schon existierende DRRs l�schen
							prlDrr = ogDrrData.GetDrrByKey (olDrrDay.Format("%Y%m%d"), llStfUrno, "2", "1");
							if (prlDrr != NULL)
							{
								prlDrr->IsChanged = DATA_DELETED;
								olPrimaryKey.Format("%s-%ld-%s-%s",prlDrr->Sday,prlDrr->Stfu,prlDrr->Drrn,prlDrr->Rosl);
								ogDrrData.omKeyMap.RemoveKey(olPrimaryKey);
							}

							prlDrr = ogDrrData.GetDrrByKey (olDrrDay.Format("%Y%m%d"), llStfUrno, "1", "1");
							if (prlDrr != NULL)
							{
								prlDrr->IsChanged = DATA_DELETED;
								olPrimaryKey.Format("%s-%ld-%s-%s",prlDrr->Sday,prlDrr->Stfu,prlDrr->Drrn,prlDrr->Rosl);
								ogDrrData.omKeyMap.RemoveKey(olPrimaryKey);
							}

							// "leeren" DRR anlegen
							prlDrr = ogDrrData.CreateDrrData(llStfUrno, olDrrDay.Format("%Y%m%d"), olRosl1, "", olDrrn1, true);
						}

						//**********************************************************************************************
						// zweite Schichten
						//**********************************************************************************************

						// Schicht an diesem Tag aus der oben generierten Schichtkette ermitteln
						ilElement = ilDay+ilDayOffset;
						ilTmpSize = prlStaff->oShifts2.GetSize();
						
						// Rotation durchf�hren
						while(ilElement >= ilTmpSize)
						{
							ilElement -= ilTmpSize;
							if(ilTmpSize == 0) ilElement = -1;
						}

						// Wenn eine g�ltige Schicht vorhanden ist
						llUrno = atol(prlStaff->oShifts2[ilElement]);
						if (llUrno != 0 && blFirstShift == true)
						{
							// DRR-Datensatz suchen
							prlDrr = ogDrrData.GetDrrByKey(olDrrDay.Format("%Y%m%d"), llStfUrno, olDrrn2, olRosl1);
							// gefunden?
							if(prlDrr == NULL)
							{
								// Datensatz nicht gefunden -> neuen anlegen
								prlDrr = ogDrrData.CreateDrrData(llStfUrno, olDrrDay.Format("%Y%m%d"), olRosl1, "", olDrrn2, true);
								blSetEdit = false;
							}
							else
							{
								//strcpy(prlDrr->Scoo, prlDrr->Scod);
								blSetEdit = true;
							}

							if (ilElement >= 0)
							{
								bool blCodeIsBsd = (ogBsdData.GetBsdByUrno(llUrno) != 0);
								PFCDATA *polPfcData = ogPfcData.GetPfcByUrno (atol(prlStaff->oFunctions2[ilElement]));
								if (polPfcData != NULL)
								{
									ogDrrData.SetDrrDataScod(prlDrr,blCodeIsBsd,llUrno,polPfcData->Fctc,blSetEdit,true);
								}
								else
								{
									ogDrrData.SetDrrDataScod(prlDrr,blCodeIsBsd,llUrno,"",blSetEdit,true);
								}
							}
						}
						else
						{
							// evtl. schon existierende DRRs l�schen
							prlDrr = ogDrrData.GetDrrByKey (olDrrDay.Format("%Y%m%d"), llStfUrno, "2", "1");
							if (prlDrr != NULL)
							{
								prlDrr->IsChanged = DATA_DELETED;
								olPrimaryKey.Format("%s-%ld-%s-%s",prlDrr->Sday,prlDrr->Stfu,prlDrr->Drrn,prlDrr->Rosl);
								ogDrrData.omKeyMap.RemoveKey(olPrimaryKey);
							}
						}
					}
				}
				//olStaffData des GPL l�schen
				ilStaffDataSize = olStaffData.GetSize();
				for (ilStaffData = 0; ilStaffData < ilStaffDataSize; ilStaffData++)
				{
					REL_STAFF_DATA *prlStaff = (REL_STAFF_DATA*)olStaffData[ilStaffData];
					CString olTmpUrno;
					olTmpUrno.Format("%ld",prlStaff->lStfUrno);
					olStfUrnoArray.Add(olTmpUrno);
					delete prlStaff;
				}
				olStaffData.RemoveAll();
			}
		}
		delete polGPLRecord;

		//END - Release Data ****
		if (!ogDrrData.Release("0","1"))
		{
			MessageBeep((UINT)-1);
			CString olErrorTxt;
			olErrorTxt.Format("%s %d",LoadStg(IDS_STRING1599), ogDrrData.imLastReturnCode);
			MessageBox(olErrorTxt,LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
		}

		//*************************************************************************************
		// Synchronisation beenden
		ogDrrData.Synchronise("SHIFTROSTER",false);
		// Ende: Synchronisation beenden
		//*************************************************************************************
	}
	else
	{
		CString olGeneralInfos;
		CString olStfus;
		CString olTempStfu;
		CString olReleaseString;
		POSITION olPos;
		int ilCountStfus = 0;
		
		// Add User
		olGeneralInfos += CString(pcgUser) + CString("|");
		// Add Homearport
		olGeneralInfos += CString(pcgHome) + CString("|");
		// Add Mode
		olGeneralInfos += CString(bpDelete ? "D" : "R") + CString("|");
		// Add Periode (YYYYMMDD-YYYYMMDD)
		olGeneralInfos += opFrom.Format("%Y%m%d") + CString("-") + opTo.Format("%Y%m%d") + CString("|");
		// Add SPL urno
		olGeneralInfos += opSplUrno + CString("|");
		
		void *pVoid;
		// Add Staff-Urno list nnn-nnn-nnn-nnn- ....
		for (olPos = popStaffMap->GetStartPosition(); olPos != NULL;)
		{
			popStaffMap->GetNextAssoc(olPos,olTempStfu,pVoid);				
			ilCountStfus++;
			olStfus += '-';
			olStfus += olTempStfu;

			if ((ilCountStfus % 700) == 0)
			{
				olStfus = olStfus.Mid(1);
				olReleaseString = olGeneralInfos + olStfus;
				DoRelease(olReleaseString);
				olStfus = "";
			}
		}

		if (olStfus.GetLength() > 0)
		{
			olStfus = olStfus.Mid(1);
			olReleaseString = olGeneralInfos + olStfus;
			DoRelease(olReleaseString);
		}
	}

	RemoveWaitCursor();

	CCS_CATCH_ALL;
}

//**********************************************************************************
// Testet ob der Code Dialog sichtbar ist.
//**********************************************************************************

bool ShiftRoster_View::IsCodeDlgActive()
{
	if (pomShiftCodeNumberDlg == NULL)
	{
		return false;
	}
	
	if (pomShiftCodeNumberDlg->IsWindowVisible() == NULL)
	{
		return false;
	}
	
	return true;
}

//**********************************************************************************
// Code Dialog aufrufen
//**********************************************************************************

void ShiftRoster_View::OnBCodenumber() 
{
	CCS_TRY;
	// wurde diese Fenster bereits ge�ffnet ?
	if (!IsCodeDlgActive())
	{
		pomShiftCodeNumberDlg = new ShiftCodeNumberDlg(this);
		pomShiftCodeNumberDlg->SetValues(rmCodeNumberData);
		pomShiftCodeNumberDlg->SetWeeks(omGSPTableData.GetSize());
		pomShiftCodeNumberDlg->SetDataArray(omGSPTableData);
		pomShiftCodeNumberDlg->ReNewAll();
	}
	else
	{
		pomShiftCodeNumberDlg->ShowWindow(SW_SHOWNORMAL);
	}
	CCS_CATCH_ALL;
}


//**********************************************************************************
// Die Auswahl in der CB View hat sich ver�ndert
//**********************************************************************************

void ShiftRoster_View::OnSelchangeCbView() 
{
	CCS_TRY;

	//alte Schichtplan-Name sichern
	CString olSDTTxt;
	int ilSelItem = pomComboBoxCoverage->GetCurSel();
ogBackTrace = ",2_5_1";
	// Auswahl g�ltig
	if(ilSelItem != CB_ERR)
	{
ogBackTrace += ",2_6";	
		// Text der aktuellen Auswahl holen		
		pomComboBoxCoverage->GetLBText(ilSelItem, olSDTTxt);
	}

    CString olViewName;
	int ilIndex = pomComboBoxView->GetCurSel();
	if (ilIndex != CB_ERR)
	{
ogBackTrace += ",2_7";		
		WriteInRosteringLog(LOGFILE_OFF,"OnSelchangeCbView 2436");
		
		int ilOldViewStattGrupp		= imViewStattGrupp;
		int ilOldViewStattDetail	= imViewStattDetail;
		int ilOldViewStattBonus		= imViewStattBonus;
		int ilOldViewShowGPL		= imViewShowGPL;
		int ilOldViewShowCover		= imViewShowCover;
		int ilOldViewShowEmployee	= imViewShowEmployee;
		CString ilOldViewFunctionUrnos		= omViewFunctionUrnos;
		CString ilOldViewQualificationUrnos	= omViewQualificationUrnos;

		// Set ViewerParameter default 
		imViewStattGrupp	= VIEW_GRUPP;
		imViewStattDetail	= VIEW_DETAIL;
		imViewStattBonus	= VIEW_BONUS;
		imViewShowGPL		= VIEW_ALLGPL;
		imViewShowCover		= VIEW_ISDEBIT;
		imViewShowEmployee  = VIEW_CODE;
		omViewFunctionUrnos.Empty();
		omViewFunctionFctcs.Empty();
		omViewQualificationUrnos.Empty();
		// end Set ViewerParameter

		// View einstellen.
		CStringArray olValues;
		pomComboBoxView->GetLBText (ilIndex, olViewName);
		pomViewer->SelectView (olViewName);
		pomViewer->GetFilter ("SHIFTVIEW1", olValues);
ogBackTrace += ",2_8";
		for (int i = 0; i < olValues.GetSize(); i++)
		{
			switch(i)
			{
			case 0:
				{
					if(olValues[i] == "PLAN")
						imViewShowGPL = VIEW_NOTALLGPL;
					else
						imViewShowGPL = VIEW_ALLGPL;
				}
				break;
			case 1:
				{
					if(olValues[i] == "N")
						imViewStattGrupp = VIEW_NOTHING;
					else
						imViewStattGrupp = VIEW_GRUPP;
				}
				break;
			case 2:
				{
					if(olValues[i] == "N")
						imViewStattDetail = VIEW_NOTHING;
					else
						imViewStattDetail = VIEW_DETAIL;
				}
				break;
			case 3:
				{
					if(olValues[i] == "N")
						imViewStattBonus = VIEW_NOTHING;
					else
						imViewStattBonus = VIEW_BONUS;
				}
				break;
			case 4:
				{
					if(olValues[i] == "DIFF")
						imViewShowCover = VIEW_DIFF;
					else
						imViewShowCover = VIEW_ISDEBIT;
				}
				break;
			case 5:
				{
					if(olValues[i] == "FLNAME")
					{
						imViewShowEmployee = VIEW_FLNAME;
					}
					else if (olValues[i] == "FREE")
					{
						imViewShowEmployee = VIEW_FREE;
					}
					else
					{
						imViewShowEmployee = VIEW_CODE;
					}
				}
				break;
			case 6:
				{
					if (bgEnableScenario == true)
					{
						// in der Coverage-ComboBox nur noch die entspr. View ausgew�hlten Schichtpl�ne zur Auswahl stellen
						pomComboBoxCoverage->ResetContent();
						pomComboBoxCoverage->AddString("");//To produce an empty line

						// filtering the relevant shift plans
						CString olSearchFor;
						if (olValues[i] == "1")
						{
							olSearchFor = "O";	//operative shift plans
						}
						else if (olValues[i] == "2")
						{
							olSearchFor = "P";	//planned shift plans
						}
						else if (olValues[i] == "3")
						{
							olSearchFor = "A";	//archieved (that means old) shift plans
						}
						else
						{
							olSearchFor = " ";
						}

						CString olLine;
						int ilCount = ogSdgData.omData.GetSize();
						SDGDATA *prlSdgData = NULL;

						for (int j = 0; j < ilCount; j++)
						{
							prlSdgData = &ogSdgData.omData[j];

							if (strcmp(olSearchFor.GetBuffer(0), " ") == NULL					/*wenn nichts oder alles ausgew�hlt ist*/
							 ||	strcmp(olSearchFor.GetBuffer(0), &prlSdgData->Expd[1]) == NULL	/*wenn flag �bereinstimmt*/
							 ||	(olSearchFor == "O" && strlen(prlSdgData->Expd) == 1))			/*wenn flag in SDG-record nicht gesetzt ist und alle operativen angezeigt werden sollen*/
							{
								olLine.Format("%-32s                                    URNO=%ld",prlSdgData->Dnam, prlSdgData->Urno);

								if(pomComboBoxCoverage->FindStringExact(-1,olLine) == CB_ERR)
								{
									pomComboBoxCoverage->AddString(olLine);
								}
							}
						}
					}
					else
					{
						//olValues.RemoveAt(6);
					}
					break;
				}
			case 7:
				{
					pomNameConfigDlg->SetConfigString(olValues[i]);
					break;
				}
			}
		}
ogBackTrace += ",2_9";
		if (olValues.GetSize() == 6) // noch ne alte View => alles anzeigen
		{
ogBackTrace += ",2_1_1";		
			pomComboBoxCoverage->ResetContent();
			pomComboBoxCoverage->AddString("");//To produce an empty line
			CString olTmpLine;
			int ilCnt = ogSdgData.omData.GetSize();
			for (i = 0; i < ilCnt; i++)
			{
				olTmpLine.Format("%-32s                                    URNO=%ld",ogSdgData.omData[i].Dnam, ogSdgData.omData[i].Urno);
				if(pomComboBoxCoverage->FindStringExact(-1, olTmpLine) == CB_ERR)
				{
					pomComboBoxCoverage->AddString(olTmpLine);
				}
			}
		}

		pomViewer->GetFilter("SHIFTVIEW2", olValues);
ogBackTrace += ",2_1_2";		
		for (i = 0; i < olValues.GetSize(); i++)
		{
			switch(i)
			{
			case 0:
				{
					if(olValues[i].IsEmpty() == FALSE)
					{
						omViewFunctionUrnos = olValues[i];
						int ilFind = 0;
						while ((ilFind = omViewFunctionUrnos.Find("|")) != -1)
						{
							omViewFunctionUrnos.SetAt(ilFind,',');
						}
						if (omViewFunctionUrnos.IsEmpty() == FALSE)
						{
							// checking the function codes
							CString olTmp;
							CStringArray olStrArr;
							PFCDATA *prlPfcData = NULL;
							olTmp = omViewFunctionUrnos;
							CedaData::ExtractTextLineFast (olStrArr, olTmp.GetBuffer (0), ",");
							for (int ilPfc = 0; ilPfc < olStrArr.GetSize(); ilPfc++)
							{
								prlPfcData = ogPfcData.GetPfcByUrno(atol(olStrArr[ilPfc]));
								if (prlPfcData != NULL)
								{
									omViewFunctionFctcs += CString(",") + CString(prlPfcData->Fctc);
								}
								if (omViewFunctionFctcs.GetLength() > 0 && omViewFunctionFctcs.GetAt(0) == ',')
								{
									omViewFunctionFctcs = omViewFunctionFctcs.Mid(1);
								}
							}

							olTmp.Format(",%s,",omViewFunctionUrnos);
							omViewFunctionUrnos = olTmp;
						}
					}
				}
				break;
			}
		}

		//uhi 25.4.01
ogBackTrace += ",2_1_3";
		pomViewer->GetFilter("SHIFTVIEW3", olValues);
	
		for (i = 0; i < olValues.GetSize(); i++)
		{
			switch(i)
			{
			case 0:
				{
					if (olValues[i].IsEmpty() == FALSE)
					{
						int ilFind = 0;
						omViewQualificationUrnos = olValues[i];

						while ((ilFind = omViewQualificationUrnos.Find("|")) != -1)
						{
							omViewQualificationUrnos.SetAt(ilFind,',');
						}

						if (omViewQualificationUrnos.IsEmpty() == FALSE)
						{	
							CString olTmp;
							olTmp.Format(",%s,",omViewQualificationUrnos);
							omViewQualificationUrnos = olTmp;
						}
					}
				
				}
				break;
			}
		}



			//MessageBox(omViewQualificationUrnos);

			
			
	ogBackTrace += ",2_1_4";
		//falls noch gefunden, alten Schichtplan wieder ausw�hlen
		if (olSDTTxt.GetLength() > 0)
		{
			ilSelItem = pomComboBoxCoverage->FindStringExact(-1, olSDTTxt);
			if (ilSelItem != CB_ERR)
			{
				pomComboBoxCoverage->SetCurSel(ilSelItem);
			}
			else
			{
				omNSRTableData.DeleteAll();
				omNSRTableDataMap.RemoveAll();
				OnSelchangeCbRequirement();
				DisplayShiftRequirementData(-1);
			}
		}
ogBackTrace += ",2_1_5";
		CString olStaffListFLName;
		CString olStaffListCode;
		CString olStaffFreeFormatted;

		for (i = 0; i < omGSPTableData.GetSize(); i++)
		{
			GetStaffListByUrnoList(omGSPTableData[i].StaffUrno,&olStaffListFLName,&olStaffListCode,&olStaffFreeFormatted);
			omGSPTableData[i].StaffFLName = olStaffListFLName;
			omGSPTableData[i].StaffCode	= olStaffListCode;
			omGSPTableData[i].StaffFree	= olStaffFreeFormatted;
		}
ogBackTrace += ",2_1_6";
		if (ilOldViewShowEmployee != imViewShowEmployee)
		{
			DisplayShiftRosterData(m_SB_GSP.GetScrollPos());
		}

		if (ilOldViewShowGPL != imViewShowGPL)
		{
			//Build_LB_GPL();
			OnChangeEGplFilter();
		}

		DisplayShiftRosterData(m_SB_GSP.GetScrollPos());
		OnSelchangeCbRequirement();
		GetShiftRequirementData(SHIFTPLAN, 2); //2 is the hint to scroll to the old scroll position
		DisplayShiftRequirementData(2);

		if (pomShiftAWDlg != NULL && ::IsWindowVisible(pomShiftAWDlg->m_hWnd))
		{
			pomShiftAWDlg->IniEmplGrid();
		}
ogBackTrace += ",2_1_7";	
  }
  CCS_CATCH_ALL;
}

//**********************************************************************************
//
//**********************************************************************************

void ShiftRoster_View::SaveBonus() 
{
	CCS_TRY;
	CString olSPLTxt,olSPLurno,olOldSPLurnoList,olNewSPLurnoList;

	// WarteDialog einblenden.
	CWaitDlg olWaitDlg(this);

	bmIsChangedBonus = false;
	if (bmIsChangedBonus == false && bmIsChangedGPL == false)
	{
		// COLOR
		((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(FALSE);
	}

	olSPLurno = omSelSPLurno;

	RecordSet *polSPLRecord = new RecordSet(ogBCD.GetFieldCount("SPL"));
	int ilSPLdbouIdx = ogBCD.GetFieldIndex("SPL","DBOU");
	RecordSet *polDBORecord = new RecordSet(ogBCD.GetFieldCount("DBO"));
	int ilDBOurnoIdx = ogBCD.GetFieldIndex("DBO","URNO");
	int ilDBOdbabIdx = ogBCD.GetFieldIndex("DBO","DBAB");
	int ilDBOdbhoIdx = ogBCD.GetFieldIndex("DBO","DBHO");
	int ilDBOroupIdx = ogBCD.GetFieldIndex("DBO","ROUP");
	int ilDBObgruIdx = ogBCD.GetFieldIndex("DBO","BGRU");
	int ilDBOspluIdx = ogBCD.GetFieldIndex("DBO","SPLU");

	if (ogBCD.GetRecord("SPL","URNO",olSPLurno,*polSPLRecord) == true)
	{
		int ilSplRecord = 0;
		//>>>>>Insert & Update DBO-Records<<<<<
		for (int i = 0; i < omBonusData.GetSize(); i++)
		{
			if (omBonusData[i].IsChanged == RECORDCHANGED)
			{
				ilSplRecord = RECORDCHANGED;
				polDBORecord->Values[ilDBOurnoIdx] = omBonusData[i].DBOurno;
				polDBORecord->Values[ilDBOdbabIdx] = omBonusData[i].DBOdbab;
				polDBORecord->Values[ilDBOdbhoIdx] = omBonusData[i].DBOdbho;
				polDBORecord->Values[ilDBOroupIdx] = omBonusData[i].DBOroup;
				polDBORecord->Values[ilDBObgruIdx] = omBonusData[i].DBObgru;
				polDBORecord->Values[ilDBOspluIdx] = olSPLurno;

				if(omBonusData[i].DBOurno == "0000000000")
				{
					polDBORecord->Values[ilDBOurnoIdx] = "";
					ogBCD.InsertRecord("DBO",*polDBORecord,true);
					omBonusData[i].DBOurno = polDBORecord->Values[ilDBOurnoIdx];
					CString urno = omBonusData[i].DBOurno;
				}
				else
				{
					ogBCD.SetRecord("DBO","URNO",omBonusData[i].DBOurno,polDBORecord->Values,true);
				}
				omBonusData[i].IsChanged = 0;
			}
			if (i > 0)
			{
				olNewSPLurnoList += "|";
			}
			olNewSPLurnoList += omBonusData[i].DBOurno;
		}
		//>>>>>Delete old DBO-Records<<<<<
		olOldSPLurnoList = polSPLRecord->Values[ilSPLdbouIdx];
		CStringArray olOldSPLurnoArray;
		int ilUrnos  = ExtractItemList(olOldSPLurnoList,&olOldSPLurnoArray,'|');
		for(i=0;i<ilUrnos;i++)
		{
			if(olNewSPLurnoList.Find(olOldSPLurnoArray[i]) == -1)
			{
				ilSplRecord = RECORDCHANGED;
				WriteInRosteringLog(LOGFILE_OFF,"OnSelchangeCbView 2800");
				ogBCD.DeleteRecord("DBO","URNO",olOldSPLurnoArray[i],true);
				WriteInRosteringLog(LOGFILE_OFF,"OnSelchangeCbView 2802");
			}
		}
		//>>>>>Update SPL-Records<<<<<
		if(ilSplRecord == RECORDCHANGED)
		{
			polSPLRecord->Values[ilSPLdbouIdx] = olNewSPLurnoList;
			CTime olTime = CTime::GetCurrentTime();
			polSPLRecord->Values[ogBCD.GetFieldIndex("SPL","LSTU")] = olTime.Format("%Y%m%d%H%M00");
			polSPLRecord->Values[ogBCD.GetFieldIndex("SPL","USEU")] = pcgUser;
		}

		CString olTemp;
		m_E_ReleaseNote.GetWindowText(olTemp);
		CString olLeft;
		CString olRight;
		
		for (i = 0; i > -1;)
		{
			i = olTemp.Find("\r\n");
			if (i != -1)
			{
				if ((i + 2) <= olTemp.GetLength())
				{
					olLeft = olTemp.Left(i);
					olRight = olTemp.Mid(i + 2);
					olTemp =  olLeft + " " + olRight;//, olTemp.GetLength());
				}
			}
		}
		polSPLRecord->Values[ogBCD.GetFieldIndex("SPL","RENO")] = olTemp.Left(60);
		ogBCD.SetRecord("SPL","URNO",olSPLurno,polSPLRecord->Values,true);
	}
	WriteInRosteringLog(LOGFILE_OFF,"OnSelchangeCbView 2835");
	delete polSPLRecord;
	WriteInRosteringLog(LOGFILE_OFF,"OnSelchangeCbView 2837");
	delete polDBORecord;
	WriteInRosteringLog(LOGFILE_OFF,"OnSelchangeCbView 2839");

	CCS_CATCH_ALL;
}

//**********************************************************************************
// Die Bonusdaten werden geladen
//**********************************************************************************

void ShiftRoster_View::Build_Bonus(CString opSPLurno)
{
	CCS_TRY;
	omBonusData.DeleteAll();
	m_CB_Bonus.ResetContent();
	
	bmIsChangedBonus = false;
	if(bmIsChangedBonus == false && bmIsChangedGPL == false)
		// COLOR
		((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(FALSE);
	
	CString olReno;
	ogBCD.GetField("SPL", "URNO", opSPLurno, "RENO", olReno);
	m_E_ReleaseNote.SetInitText(olReno);

	CString olSplUrnoList;

	if (opSPLurno != "" && opSPLurno != "0000000000")
	{
		int ilSPLdbouIdx = ogBCD.GetFieldIndex("SPL","DBOU");
		int ilDBOdbabIdx = ogBCD.GetFieldIndex("DBO","DBAB");
		int ilDBOdbhoIdx = ogBCD.GetFieldIndex("DBO","DBHO");
		int ilDBOroupIdx = ogBCD.GetFieldIndex("DBO","ROUP");
		int ilDBObgruIdx = ogBCD.GetFieldIndex("DBO","BGRU");

		RecordSet *polSPLRecord = new RecordSet(ogBCD.GetFieldCount("SPL"));
		if (ogBCD.GetRecord("SPL","URNO",opSPLurno,*polSPLRecord) == true)
		{
			olSplUrnoList = polSPLRecord->Values[ilSPLdbouIdx];
			olSplUrnoList.Replace("�", "|");
			CStringArray olUrnoArray;
			int ilUrnos = ExtractItemList(olSplUrnoList,&olUrnoArray,'|');
			for (int i = 0; i < ilUrnos; i++)
			{
				RecordSet *polDBORecord = new RecordSet(ogBCD.GetFieldCount("DBO"));
				if(ogBCD.GetRecord("DBO","URNO",olUrnoArray[i],*polDBORecord) == true)
				{
					BONUSDATA *polBonus = new BONUSDATA;
					
					polBonus->DBOurno = olUrnoArray[i];
					polBonus->DBOdbab = polDBORecord->Values[ilDBOdbabIdx];
					polBonus->DBOdbho = polDBORecord->Values[ilDBOdbhoIdx];
					polBonus->DBOroup = polDBORecord->Values[ilDBOroupIdx];
					polBonus->DBObgru = polDBORecord->Values[ilDBObgruIdx];
					
					SGRDATA *prlSgr = ogSgrData.GetSgrByUrno(atol(polBonus->DBObgru));
					if(prlSgr != NULL)
					{
						polBonus->SGRgrpn = prlSgr->Grpn;
						CCSPtrArray<SGMDATA> olSgmList;
						if(ogSgmData.GetSgmDataByGrnUrno(olSgmList,atol(polBonus->DBObgru)) == true)
						{
							CString olTmpTxt;
							for(int m=0; m<olSgmList.GetSize(); m++)
							{
								// Ge�ndert von ADO 30.5.00
								BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(atol(olSgmList[m].Uval));
								if(prlBsd != NULL)
								{
									if(m>0)
									{
										polBonus->BsdList += "|";
										polBonus->BsdUrnoList += "|";
									}
									polBonus->BsdList += (CString)prlBsd->Bsdc;
									olTmpTxt.Format("%d",prlBsd->Urno);
									polBonus->BsdUrnoList += olTmpTxt;
								}
							}
						}
						olSgmList.DeleteAll();
					}
					//uhi 27.7.00 Alle Schichten ausgew�hlt
					else{
						if(polBonus->DBObgru == CString("4711"))
						{
							polBonus->SGRgrpn = CString("*.*"); 
							CString olTmpTxt;
							for(int m=0; m<ogBsdData.omData.GetSize(); m++)
							{
								if(m>0)
								{
									polBonus->BsdList += "|";
									polBonus->BsdUrnoList += "|";
								}
								polBonus->BsdList += (CString)ogBsdData.omData[m].Bsdc;
								olTmpTxt.Format("%d",ogBsdData.omData[m].Urno);
								polBonus->BsdUrnoList += olTmpTxt;
							}
						}
					}
					
					omBonusData.Add(polBonus);
				}
				delete polDBORecord;
			}
		}
		delete polSPLRecord;
		
		CString olLine;
		for(int i = 0; i < omBonusData.GetSize(); i++)
		{
			olLine.Format("%-12s                                                 URNO=%s",omBonusData[i].SGRgrpn,omBonusData[i].DBObgru);
			m_CB_Bonus.AddString(olLine);
		}
		OnSelchangeCbBonus();
	}
	else
	{
		m_E_Absence.SetInitText("");
		m_E_Holiday.SetInitText("");
		m_E_Roundup.SetInitText("");
	}
	
	CCS_CATCH_ALL;
}

//**********************************************************************************
//
//**********************************************************************************

void ShiftRoster_View::SaveGPL() 
{
	CCS_TRY;
	AfxGetApp()->DoWaitCursor(1);

	// WarteDialog einblenden.
	CWaitDlg olWaitDlg(this);

	CString olTmpTxt;
	CString olGPLurno	= "0";
	bool	ilStatus	= true;

	m_E_GPL_Caption.GetWindowText(olTmpTxt);
	if (olTmpTxt.GetLength() > (olTmpTxt.Find("URNO=") + 5))
	{
		olGPLurno = olTmpTxt.Mid(olTmpTxt.Find("URNO=") + 5);
	}

	RecordSet *polGPLRecord = new RecordSet(ogBCD.GetFieldCount("GPL"));

	if (ogBCD.GetRecord("GPL","URNO",olGPLurno,*polGPLRecord) == true)
	{
		

		
		CString olGPLperi;
		CString olGPLvafr;
		CString olGPLvato;
		CString	olGPLstpt;
		CString olGPLcotu = "0";
		CString olGPLorgu = "0";
		CString olErrorText;
		CTime olTmpTimeFr = -1;
		CTime olTmpTimeTo = -1;
		CTime olTmpTimeSt = -1;

		m_E_GPL_Peri.GetWindowText(olGPLperi);
		m_E_GPL_Vafr.GetWindowText(olGPLvafr);
		m_E_GPL_Vato.GetWindowText(olGPLvato);
		m_E_GPL_StPt.GetWindowText(olGPLstpt);

		if ((m_E_GPL_Vafr.GetStatus() == false) || (m_E_GPL_Vato.GetStatus() == false) || (m_E_GPL_StPt.GetStatus() == false))
		{
			ilStatus = false;
		}

		if((olGPLvato.GetLength() != 0 && olGPLvafr.GetLength() == 0) || (olGPLvato.GetLength() == 0 && olGPLvafr.GetLength() != 0))
		{
			ilStatus = false;
		}
		if(ilStatus == true && olGPLvato.GetLength() != 0 && olGPLvafr.GetLength() != 0 && olGPLstpt.GetLength() != 0)  
		{
	



			olTmpTimeFr		= DateHourStringToDate(olGPLvafr,CString("00:00"));
			olTmpTimeTo		= DateHourStringToDate(olGPLvato,CString("00:00"));
			olTmpTimeSt		= DateHourStringToDate(olGPLstpt,CString("00:00"));

			// Der Endpunkt darf nicht sp�ter als der Startpunkt sein
			if (olTmpTimeFr > olTmpTimeTo)
			{
				Beep(440,70);
				MessageBox(LoadStg(IDS_STRING1052), LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);

				delete polGPLRecord;
				om_SelAWDlgUrnos = "";
				RemoveWaitCursor();
				return;
			}

			if (olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
			{
				ilStatus = false;
			}
		}
		if (ilStatus == true)
		{
		
			
					
			
			
			
			bmIsChangedGPL = false;
			if(bmIsChangedBonus == false && bmIsChangedGPL == false)
			{
				((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(FALSE); // color of the Save-button
			}

			

			CTime olTime = CTime::GetCurrentTime();
		
			polGPLRecord->Values[ogBCD.GetFieldIndex("GPL","VAFR")] = olTmpTimeFr.Format("%Y%m%d000000");
			polGPLRecord->Values[ogBCD.GetFieldIndex("GPL","VATO")] = olTmpTimeTo.Format("%Y%m%d000000");
			polGPLRecord->Values[ogBCD.GetFieldIndex("GPL","LSTU")] = olTime.Format("%Y%m%d%H%M00");
			polGPLRecord->Values[ogBCD.GetFieldIndex("GPL","USEU")] = pcgUser;
			polGPLRecord->Values[ogBCD.GetFieldIndex("GPL","PERI")] = olGPLperi;
		
				
		  /*  CString s ;
	        s.Format("%d", ogBCD.GetFieldIndex("GPL","STPT"))  ; 
		    AfxMessageBox(s); // huzimi
			*/
			
		
			polGPLRecord->Values[ogBCD.GetFieldIndex("GPL","STPT")] = olTmpTimeSt.Format("%Y%m%d000000");

			
		//	AfxMessageBox("2");

			int ilSelItem = m_CB_Contract.GetCurSel();
			if (ilSelItem != LB_ERR)
			{
				CString olTmpTxt;
				CString olCOTUrno;
				m_CB_Contract.GetLBText(ilSelItem,olTmpTxt);
				if (olTmpTxt.GetLength() > (olTmpTxt.Find("URNO=") + 5))
				{
					olGPLcotu = olTmpTxt.Mid(olTmpTxt.Find("URNO=") + 5);
				}
			}

			polGPLRecord->Values[ogBCD.GetFieldIndex("GPL","COTU")] = olGPLcotu;


		
			
			
			if (ogBCD.GetFieldIndex("GPL","ORGU") > -1)
			{
				int ilSelItem = m_CB_OrgUnit.GetCurSel();
				if (ilSelItem != LB_ERR)
				{
					CString olTmpTxt;
					CString olOrgUrno;
					m_CB_OrgUnit.GetLBText(ilSelItem,olTmpTxt);
					if (olTmpTxt.GetLength() > (olTmpTxt.Find("URNO=") + 5))
					{
						olGPLorgu = olTmpTxt.Mid(olTmpTxt.Find("URNO=") + 5);
					}
				}

				polGPLRecord->Values[ogBCD.GetFieldIndex("GPL","ORGU")] = olGPLorgu;

			}



			ogBCD.SetRecord("GPL","URNO",olGPLurno,polGPLRecord->Values,true);

			// Alle Eintr�ge durchgehen
			GSPTABLEDATA *polGspTableDataStaff = NULL;
			GSPTABLEDATA *polGspTableDataShift = NULL;

		
	
		
			
			
			
			for (int i = 0; i < omGSPTableData.GetSize(); i++)
			{
				if (omGSPTableData[i].IsChanged & STAFFCHANGED)
				{
					polGspTableDataStaff = &omGSPTableData[i];
					polGspTableDataShift = NULL;
					for (int j = 0; j < omGSPTableData.GetSize(); j++)
					{
						if (omGSPTableData[j].DisplayWeek == polGspTableDataStaff->Week)
						{
							polGspTableDataShift = &omGSPTableData[j];
							break;
						}
					}

					if (polGspTableDataShift == NULL)	// must be a deleted row !
					{
						for (int j = 0; j < omDeletedGSPs.GetSize(); j++)
						{
							if (omDeletedGSPs[j].DisplayWeek == polGspTableDataStaff->Week)
							{
								polGspTableDataShift = &omDeletedGSPs[j];
								break;
							}
						}
					}

					RecordSet *polGSPRecord = new RecordSet(ogBCD.GetFieldCount("GSP"));

					polGSPRecord->Values[imGSPgpluIdx] = olGPLurno;

					// die ersten Schichten
					polGSPRecord->Values[imGSPbumoIdx] = polGspTableDataShift->DayUrno[0];
					polGSPRecord->Values[imGSPbutuIdx] = polGspTableDataShift->DayUrno[1];
					polGSPRecord->Values[imGSPbuweIdx] = polGspTableDataShift->DayUrno[2];
					polGSPRecord->Values[imGSPbuthIdx] = polGspTableDataShift->DayUrno[3];
					polGSPRecord->Values[imGSPbufrIdx] = polGspTableDataShift->DayUrno[4];
					polGSPRecord->Values[imGSPbusaIdx] = polGspTableDataShift->DayUrno[5];
					polGSPRecord->Values[imGSPbusuIdx] = polGspTableDataShift->DayUrno[6];

					// die zweiten Schichten
					polGSPRecord->Values[imGSPsumoIdx] = polGspTableDataShift->Day2Urno[0];
					polGSPRecord->Values[imGSPsutuIdx] = polGspTableDataShift->Day2Urno[1];
					polGSPRecord->Values[imGSPsuweIdx] = polGspTableDataShift->Day2Urno[2];
					polGSPRecord->Values[imGSPsuthIdx] = polGspTableDataShift->Day2Urno[3];
					polGSPRecord->Values[imGSPsufrIdx] = polGspTableDataShift->Day2Urno[4];
					polGSPRecord->Values[imGSPsusaIdx] = polGspTableDataShift->Day2Urno[5];
					polGSPRecord->Values[imGSPsusuIdx] = polGspTableDataShift->Day2Urno[6];

					// die ersten Funktionen
					polGSPRecord->Values[imGSPp1moIdx] = polGspTableDataShift->PfcUrno[0];
					polGSPRecord->Values[imGSPp1tuIdx] = polGspTableDataShift->PfcUrno[1];
					polGSPRecord->Values[imGSPp1weIdx] = polGspTableDataShift->PfcUrno[2];
					polGSPRecord->Values[imGSPp1thIdx] = polGspTableDataShift->PfcUrno[3];
					polGSPRecord->Values[imGSPp1frIdx] = polGspTableDataShift->PfcUrno[4];
					polGSPRecord->Values[imGSPp1saIdx] = polGspTableDataShift->PfcUrno[5];
					polGSPRecord->Values[imGSPp1suIdx] = polGspTableDataShift->PfcUrno[6];

					// die zweiten Funktionen
					polGSPRecord->Values[imGSPp2moIdx] = polGspTableDataShift->PfcUrno2[0];
					polGSPRecord->Values[imGSPp2tuIdx] = polGspTableDataShift->PfcUrno2[1];
					polGSPRecord->Values[imGSPp2weIdx] = polGspTableDataShift->PfcUrno2[2];
					polGSPRecord->Values[imGSPp2thIdx] = polGspTableDataShift->PfcUrno2[3];
					polGSPRecord->Values[imGSPp2frIdx] = polGspTableDataShift->PfcUrno2[4];
					polGSPRecord->Values[imGSPp2saIdx] = polGspTableDataShift->PfcUrno2[5];
					polGSPRecord->Values[imGSPp2suIdx] = polGspTableDataShift->PfcUrno2[6];

					CString olWeek;
					olWeek.Format("%d",atoi(polGspTableDataStaff->Week));
					polGSPRecord->Values[imGSPweekIdx] = olWeek;
					polGSPRecord->Values[imGSPnstfIdx] = polGspTableDataStaff->Number;
					polGSPRecord->Values[imGSPstfuIdx] = polGspTableDataStaff->StaffUrno;
					if (this->omCustomer == "SIN" && imGSPwgpcIdx >= 0)
					{
						polGSPRecord->Values[imGSPwgpcIdx] = polGspTableDataStaff->Workgroup;
					}

					CStringArray olNameUrnoArray;
					CString olNameList;
					int ilUrnos  = ExtractItemList(polGspTableDataStaff->StaffUrno,&olNameUrnoArray,'|');
					for (int m = 0; m < ilUrnos; m++)
					{
						STFDATA *prlStf = ogStfData.GetStfByUrno(atol(olNameUrnoArray[m]));
						if(prlStf != NULL)
						{
							CString olTmpName;
							CString olVName = prlStf->Finm;
							CString olNName = prlStf->Lanm;
							olTmpName.Format("|%s.%s", olVName.Left(1), olNName);
							olNameList += olTmpName;
						}
					}
					if (olNameList.GetLength() > 1)
					{
						olNameList = olNameList.Mid(1);
					}

					polGSPRecord->Values[imGSPstfnIdx] = olNameList;

					// Update oder Insert, jenachdem ob Urno schon vorhanden
					if (polGspTableDataStaff->GSPUrno == "" || polGspTableDataStaff->GSPUrno == "0")
					{
						ogBCD.InsertRecord("GSP",*polGSPRecord,true);
						polGspTableDataStaff->GSPUrno = polGSPRecord->Values[imGSPurnoIdx];
					}
					else
					{
						polGSPRecord->Values[imGSPurnoIdx] = polGspTableDataStaff->GSPUrno;
						ogBCD.SetRecord ("GSP", "URNO", polGspTableDataStaff->GSPUrno, polGSPRecord->Values, true);
					}
					delete polGSPRecord;
					polGspTableDataStaff->IsChanged -= STAFFCHANGED;
				}
			}


			for (i = 0; i < omGSPTableData.GetSize(); i++)
			{
				if (omGSPTableData[i].IsChanged & SHIFTCHANGED)
				{
					polGspTableDataShift = &omGSPTableData[i];
					polGspTableDataStaff = NULL;
					for (int j = 0; j < omGSPTableData.GetSize(); j++)
					{
						if (omGSPTableData[j].Week == polGspTableDataShift->DisplayWeek)
						{
							polGspTableDataStaff = &omGSPTableData[j];
							break;
						}
					}

					if (polGspTableDataStaff == NULL)	// must be a deleted row !
					{
						for (int j = 0; j < omDeletedGSPs.GetSize(); j++)
						{
							if (omDeletedGSPs[j].Week == polGspTableDataShift->DisplayWeek)
							{
								polGspTableDataStaff = &omDeletedGSPs[j];
								break;
							}
						}
					}

					RecordSet *polGSPRecord = new RecordSet(ogBCD.GetFieldCount("GSP"));

					polGSPRecord->Values[imGSPgpluIdx] = olGPLurno;

					// die ersten Schichten
					polGSPRecord->Values[imGSPbumoIdx] = polGspTableDataShift->DayUrno[0];
					polGSPRecord->Values[imGSPbutuIdx] = polGspTableDataShift->DayUrno[1];
					polGSPRecord->Values[imGSPbuweIdx] = polGspTableDataShift->DayUrno[2];
					polGSPRecord->Values[imGSPbuthIdx] = polGspTableDataShift->DayUrno[3];
					polGSPRecord->Values[imGSPbufrIdx] = polGspTableDataShift->DayUrno[4];
					polGSPRecord->Values[imGSPbusaIdx] = polGspTableDataShift->DayUrno[5];
					polGSPRecord->Values[imGSPbusuIdx] = polGspTableDataShift->DayUrno[6];

					// die zweiten Schichten
					polGSPRecord->Values[imGSPsumoIdx] = polGspTableDataShift->Day2Urno[0];
					polGSPRecord->Values[imGSPsutuIdx] = polGspTableDataShift->Day2Urno[1];
					polGSPRecord->Values[imGSPsuweIdx] = polGspTableDataShift->Day2Urno[2];
					polGSPRecord->Values[imGSPsuthIdx] = polGspTableDataShift->Day2Urno[3];
					polGSPRecord->Values[imGSPsufrIdx] = polGspTableDataShift->Day2Urno[4];
					polGSPRecord->Values[imGSPsusaIdx] = polGspTableDataShift->Day2Urno[5];
					polGSPRecord->Values[imGSPsusuIdx] = polGspTableDataShift->Day2Urno[6];

					// die ersten Funktionen
					polGSPRecord->Values[imGSPp1moIdx] = polGspTableDataShift->PfcUrno[0];
					polGSPRecord->Values[imGSPp1tuIdx] = polGspTableDataShift->PfcUrno[1];
					polGSPRecord->Values[imGSPp1weIdx] = polGspTableDataShift->PfcUrno[2];
					polGSPRecord->Values[imGSPp1thIdx] = polGspTableDataShift->PfcUrno[3];
					polGSPRecord->Values[imGSPp1frIdx] = polGspTableDataShift->PfcUrno[4];
					polGSPRecord->Values[imGSPp1saIdx] = polGspTableDataShift->PfcUrno[5];
					polGSPRecord->Values[imGSPp1suIdx] = polGspTableDataShift->PfcUrno[6];

					// die zweiten Funktionen
					polGSPRecord->Values[imGSPp2moIdx] = polGspTableDataShift->PfcUrno2[0];
					polGSPRecord->Values[imGSPp2tuIdx] = polGspTableDataShift->PfcUrno2[1];
					polGSPRecord->Values[imGSPp2weIdx] = polGspTableDataShift->PfcUrno2[2];
					polGSPRecord->Values[imGSPp2thIdx] = polGspTableDataShift->PfcUrno2[3];
					polGSPRecord->Values[imGSPp2frIdx] = polGspTableDataShift->PfcUrno2[4];
					polGSPRecord->Values[imGSPp2saIdx] = polGspTableDataShift->PfcUrno2[5];
					polGSPRecord->Values[imGSPp2suIdx] = polGspTableDataShift->PfcUrno2[6];

					CString olWeek;
					olWeek.Format("%d",atoi(polGspTableDataStaff->Week));
					polGSPRecord->Values[imGSPweekIdx] = olWeek;
					polGSPRecord->Values[imGSPnstfIdx] = polGspTableDataStaff->Number;
					polGSPRecord->Values[imGSPstfuIdx] = polGspTableDataStaff->StaffUrno;
					if (this->omCustomer == "SIN" && imGSPwgpcIdx >= 0)
					{
						polGSPRecord->Values[imGSPwgpcIdx] = polGspTableDataStaff->Workgroup;
					}

					CStringArray olNameUrnoArray;
					CString olNameList;
					int ilUrnos  = ExtractItemList(polGspTableDataStaff->StaffUrno,&olNameUrnoArray,'|');
					for (int m = 0; m < ilUrnos; m++)
					{
						STFDATA *prlStf = ogStfData.GetStfByUrno(atol(olNameUrnoArray[m]));
						if(prlStf != NULL)
						{
							CString olTmpName;
							CString olVName = prlStf->Finm;
							CString olNName = prlStf->Lanm;
							olTmpName.Format("|%s.%s", olVName.Left(1), olNName);
							olNameList += olTmpName;
						}
					}
					if (olNameList.GetLength() > 1)
					{
						olNameList = olNameList.Mid(1);
					}

					polGSPRecord->Values[imGSPstfnIdx] = olNameList;

					// Update oder Insert, jenachdem ob Urno schon vorhanden
					if (polGspTableDataStaff->GSPUrno == "" || polGspTableDataStaff->GSPUrno == "0")
					{
						ogBCD.InsertRecord("GSP",*polGSPRecord,true);
						polGspTableDataStaff->GSPUrno = polGSPRecord->Values[imGSPurnoIdx];
					}
					else
					{
						polGSPRecord->Values[imGSPurnoIdx] = polGspTableDataStaff->GSPUrno;
						ogBCD.SetRecord ("GSP", "URNO", polGspTableDataStaff->GSPUrno, polGSPRecord->Values, true);
					}
					delete polGSPRecord;
					//polGspTableDataStaff->IsChanged = 0;
					polGspTableDataShift->IsChanged -= SHIFTCHANGED;
				}
			}

			for (i = 0; i < omDeletedGSPs.GetSize(); i++)
			{
				ogBCD.DeleteRecord("GSP","URNO",omDeletedGSPs[i].GSPUrno,true);
			}
			omDeletedGSPs.DeleteAll();
		}
		else
		{
			Beep(440,70);
			MessageBox(LoadStg(SHIFT_VALID_ERROR), LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
		}
	}
	delete polGPLRecord;
	om_SelAWDlgUrnos = "";
	RemoveWaitCursor();
	CCS_CATCH_ALL;
}

//**********************************************************************************
//
//**********************************************************************************

void ShiftRoster_View::GetShiftRequirementData(int ipTab, int ipScrollPos/*=3*/)
{
	CCS_TRY;
	CString olSPLTxt;

	// Schicht Bedarfs Daten darstellen ********************************************************
	if(ipTab == SHIFTDEMAND)	
	{
		// Alle Soll Anteile werden gel�scht
		for(int i=0; i<omNSRTableData.GetSize(); i++)
		{
			omNSRTableData[i].MonDebit = 0;
			omNSRTableData[i].TueDebit = 0;
			omNSRTableData[i].WndDebit = 0;
			omNSRTableData[i].ThuDebit = 0;
			omNSRTableData[i].FriDebit = 0;
			omNSRTableData[i].SatDebit = 0;
			omNSRTableData[i].SunDebit = 0;
		}

		// URNO des aktuellen MSDs ermitteln - nur dessen Bedarfe anzeige!
		CString olMsdName;
		long llSdgUrno = -1;
		pomComboBoxCoverage->GetWindowText(olMsdName);
		olMsdName = olMsdName.Left(32);
		olMsdName.TrimRight();

		// Testen ob der Text g�ltig ist
		if(olMsdName != "")
		{
			// Alles vorhandenen Schicht Bedarfs Gruppen durchgehen.
			int ilCount = ogSdgData.omData.GetSize();
			for (int j = 0; j < ilCount; j++)
			{
				// Schichtgruppen nach dem Eintrag der Listbox durchsuchen
				if (ogSdgData.omData[j].Dnam == olMsdName)
				{
					llSdgUrno = ogSdgData.omData[j].Urno;
					break;
				}
			}
		}

		// aktuellen View-Funktionen holen
		CString olFunctions = "," + omViewFunctionFctcs + ",";

		// Anzahl der Records im Array
		int ilCount = ogMsdData.omData.GetSize();
		MSDDATA *polMsd = NULL;
		CString olTmp;
		for (i = 0; i < ilCount; i++)
		{
			polMsd = &ogMsdData.omData[i];
			olTmp = "," + CString(polMsd->Fctc) + ",";

			if (polMsd->Sdgu == llSdgUrno && strstr((const char *)olFunctions,(const char *)olTmp) != NULL)
			{
				// Code ermitteln
				CString olBsdc = omSortCode + (CString)polMsd->Bsdc;
				NSRTABLEDATA *prlData = FindNSRTableGrupp(olBsdc, polMsd->Fctc);

				if (prlData != NULL)
				{
					AddDebitDay(prlData, polMsd->Esbg);
				}
				else
				{
					// ADO 29.5.00
					CString olText;
					olText = CString (" (") + (CString)polMsd->Fctc + polMsd->Esbg.Format(", %H:%M - ") + polMsd->Lsen.Format("%H:%M)");

					NSRTABLEDATA *prlData = new NSRTABLEDATA;
					AddDebitDay(prlData,polMsd->Esbg);
					prlData->Fctc = (CString)polMsd->Fctc;
					prlData->omShiftTime = polMsd->Esbg.Format("%H%M") + polMsd->Lsen.Format("%H%M");
					prlData->Grupp = omSortCode + polMsd->Bsdc + olText;
					prlData->GruppUrno.Format("%ld",polMsd->Bsdu);
					omNSRTableData.Add(prlData);
					omNSRTableDataMap.SetAt(omSortCode + polMsd->Bsdc + polMsd->Fctc, prlData);
				}
			}
		}

		NSRTABLEDATA *prlData = FindNSRTableGrupp((CString)"");
		if(prlData != NULL)
		{
			rmActualShifData.Type		= prlData->Type;
			rmActualShifData.Grupp		= prlData->Grupp;
			rmActualShifData.GruppUrno	= prlData->GruppUrno;
			prlData->MonIs = rmActualShifData.MonIs;
			prlData->TueIs = rmActualShifData.TueIs;
			prlData->WndIs = rmActualShifData.WndIs;
			prlData->ThuIs = rmActualShifData.ThuIs;
			prlData->FriIs = rmActualShifData.FriIs;
			prlData->SatIs = rmActualShifData.SatIs;
			prlData->SunIs = rmActualShifData.SunIs;
			rmActualShifData.MonDebit = prlData->MonDebit;
			rmActualShifData.TueDebit = prlData->TueDebit;
			rmActualShifData.WndDebit = prlData->WndDebit;
			rmActualShifData.ThuDebit = prlData->ThuDebit;
			rmActualShifData.FriDebit = prlData->FriDebit;
			rmActualShifData.SatDebit = prlData->SatDebit;
			rmActualShifData.SunDebit = prlData->SunDebit;
		}
	}

	// -----------------------------------------------------------------------------------------------------------------
	// Mit Grundschichtplan Schichten f�llen
	if (ipTab == SHIFTPLAN)	
	{
		int ilSelSPLItem = pomComboBoxShiftplan->GetCurSel();
		if (ilSelSPLItem != CB_ERR)
		{
			// Alle Ist Anteile werden gel�scht
			SetCodeNumberNull(SPL);
			pomComboBoxShiftplan->GetLBText(ilSelSPLItem,olSPLTxt);
			for(int i =0; i<omNSRTableData.GetSize(); i++)
			{
				omNSRTableData[i].MonIs = 0;
				omNSRTableData[i].TueIs = 0;
				omNSRTableData[i].WndIs = 0;
				omNSRTableData[i].ThuIs = 0;
				omNSRTableData[i].FriIs = 0;
				omNSRTableData[i].SatIs = 0;
				omNSRTableData[i].SunIs = 0;
			}
		}

		if (ilSelSPLItem != CB_ERR && olSPLTxt != "")
		{
			CString olSPLurno = "0";
			if (olSPLTxt.GetLength() > (olSPLTxt.Find("URNO=") + 5))
			{
				olSPLurno = olSPLTxt.Mid(olSPLTxt.Find("URNO=") + 5);
			}

			int ilGPLCount =  ogBCD.GetDataCount("GPL");
			CString olSelectGPLUrnos = "|";
			RecordSet *polGPLRecord = new RecordSet(ogBCD.GetFieldCount("GPL"));
			int ilGPLspluIdx = ogBCD.GetFieldIndex("GPL","SPLU");
			int ilGPLurnoIdx = ogBCD.GetFieldIndex("GPL","URNO");
			for (int i = 0; i < ilGPLCount; i++)
			{
				ogBCD.GetRecord("GPL",i, *polGPLRecord);
				if (polGPLRecord->Values[ilGPLspluIdx] == olSPLurno)
				{
					olSelectGPLUrnos += polGPLRecord->Values[ilGPLurnoIdx] + CString("|");
				}
			}
			//olSelectGPLUrnos = olSelectGPLUrnos.Left(olSelectGPLUrnos.GetLength()-1);
			delete polGPLRecord;

			int ilGSPCount =  ogBCD.GetDataCount("GSP");
			RecordSet *polGSPRecord = new RecordSet(ogBCD.GetFieldCount("GSP"));

			// Alle gefundenen Grundschichtpl�ne werden durchgegangen
			BSDDATA* prlBsdData;
			CString olSearchUrno;
			CString olTmpUrno;

			for (i = 0; i < ilGSPCount; i++)
			{
				ogBCD.GetRecord("GSP",i, *polGSPRecord);
				olSearchUrno.Format("|%s|", polGSPRecord->Values[imGSPgpluIdx]);

				// Pr�fen ob der Grundschichtplan mit diesem Schichtplan assoziiert wurde
				if (olSelectGPLUrnos.Find(olSearchUrno) != -1)
				{
					// CodeNumber
					rmCodeNumberData.MaScheduleAll += atoi(polGSPRecord->Values[imGSPnstfIdx]);
					polGSPRecord->Values[imGSPstfuIdx].Replace("�", "|");
					rmCodeNumberData.MaAssignAll   += GetItemCount(polGSPRecord->Values[imGSPstfuIdx],'|');

					CString olBsdUrno;
					CString olBsdUrno2;
					CString olPfcUrno;
					CString olPfcUrno2;
					for (int ilDay = 0; ilDay < 7; ilDay++)
					{
						switch(ilDay)
						{
						case 0:
							olBsdUrno	= polGSPRecord->Values[imGSPbumoIdx];
							olBsdUrno2	= polGSPRecord->Values[imGSPsumoIdx];
							olPfcUrno	= polGSPRecord->Values[imGSPp1moIdx];
							olPfcUrno2	= polGSPRecord->Values[imGSPp2moIdx];
							break;
						case 1:
							olBsdUrno	= polGSPRecord->Values[imGSPbutuIdx];
							olBsdUrno2	= polGSPRecord->Values[imGSPsutuIdx];
							olPfcUrno	= polGSPRecord->Values[imGSPp1tuIdx];
							olPfcUrno2	= polGSPRecord->Values[imGSPp2tuIdx];
							break;
						case 2:
							olBsdUrno	= polGSPRecord->Values[imGSPbuweIdx];
							olBsdUrno2	= polGSPRecord->Values[imGSPsuweIdx];
							olPfcUrno	= polGSPRecord->Values[imGSPp1weIdx];
							olPfcUrno2	= polGSPRecord->Values[imGSPp2weIdx];
							break;
						case 3:
							olBsdUrno	= polGSPRecord->Values[imGSPbuthIdx];
							olBsdUrno2	= polGSPRecord->Values[imGSPsuthIdx];
							olPfcUrno	= polGSPRecord->Values[imGSPp1thIdx];
							olPfcUrno2	= polGSPRecord->Values[imGSPp2thIdx];
							break;
						case 4:
							olBsdUrno	= polGSPRecord->Values[imGSPbufrIdx];
							olBsdUrno2	= polGSPRecord->Values[imGSPsufrIdx];
							olPfcUrno	= polGSPRecord->Values[imGSPp1frIdx];
							olPfcUrno2	= polGSPRecord->Values[imGSPp2frIdx];
							break;
						case 5:
							olBsdUrno	= polGSPRecord->Values[imGSPbusaIdx];
							olBsdUrno2	= polGSPRecord->Values[imGSPsusaIdx];
							olPfcUrno	= polGSPRecord->Values[imGSPp1saIdx];
							olPfcUrno2	= polGSPRecord->Values[imGSPp2saIdx];
							break;
						case 6:
							olBsdUrno	= polGSPRecord->Values[imGSPbusuIdx];
							olBsdUrno2	= polGSPRecord->Values[imGSPsusuIdx];
							olPfcUrno	= polGSPRecord->Values[imGSPp1suIdx];
							olPfcUrno2	= polGSPRecord->Values[imGSPp2suIdx];
							break;
						}
						
						// number of employees
						int ilNstf = atoi(polGSPRecord->Values[imGSPnstfIdx]);

						// erste Schicht
						if(olBsdUrno != "" && olBsdUrno != "0")
						{
							olTmpUrno.Format(",%s,",olPfcUrno);
							if(omViewFunctionUrnos.Find(olTmpUrno) != -1 || !omViewFunctionUrnos.GetLength())
							{
								prlBsdData = ogBsdData.GetBsdByUrno (atol(olBsdUrno));
								if (prlBsdData != NULL)
								{
									// CodeNumber
									rmCodeNumberData.HouersAll += atoi(prlBsdData->Sdu1); 

									HandleNSRTable (olBsdUrno, olPfcUrno, ilNstf, ilDay + 1);
								}
							}
						}

						// zweite Schicht
						if (olBsdUrno2 != "" && olBsdUrno2 != "0")
						{
							olTmpUrno.Format(",%s,",olPfcUrno2);
							if(omViewFunctionUrnos.Find(olTmpUrno) != -1 || !omViewFunctionUrnos.GetLength())
							{
								//-> gefunden
								prlBsdData = ogBsdData.GetBsdByUrno(atol(olBsdUrno2));
								if (prlBsdData != NULL)
								{
									// CodeNumber
									rmCodeNumberData.HouersAll += atoi(prlBsdData->Sdu1); 

									HandleNSRTable (olBsdUrno2, olPfcUrno2, ilNstf, ilDay + 1);
								}
							}
						}
					}
				}
			}
			delete polGSPRecord;
		}
		//-------------------------------------------------------------------------------------------------
		// Sollte der Code Dlg aktiv sein, wird dieser mit neuen Daten gef�llt
		if(ilSelSPLItem != CB_ERR)
		{
			if (IsCodeDlgActive())
			{
				pomShiftCodeNumberDlg->SetValues(rmCodeNumberData);
				pomShiftCodeNumberDlg->SetWeeks(omGSPTableData.GetSize());
				pomShiftCodeNumberDlg->SetDataArray(omGSPTableData);
				pomShiftCodeNumberDlg->ReNewAll();
			}
		}
		//--------------------------------------------------------------------------------------------------
		// Ich vermute hier wird die Summenzeile gef�llt
		NSRTABLEDATA *prlData = FindNSRTableGrupp((CString)"");
		if (prlData != NULL)
		{
			rmActualShifData.Type		 = prlData->Type;
			rmActualShifData.Grupp		 = prlData->Grupp;
			rmActualShifData.GruppUrno	 = prlData->GruppUrno;
			rmActualShifData.MonIs		 = prlData->MonIs;
			rmActualShifData.TueIs		 = prlData->TueIs;
			rmActualShifData.WndIs		 = prlData->WndIs;
			rmActualShifData.ThuIs		 = prlData->ThuIs;
			rmActualShifData.FriIs		 = prlData->FriIs;
			rmActualShifData.SatIs		 = prlData->SatIs;
			rmActualShifData.SunIs		 = prlData->SunIs;
			prlData->MonDebit	 = rmActualShifData.MonDebit;
			prlData->TueDebit	 = rmActualShifData.TueDebit;
			prlData->WndDebit	 = rmActualShifData.WndDebit;
			prlData->ThuDebit	 = rmActualShifData.ThuDebit;
			prlData->FriDebit	 = rmActualShifData.FriDebit;
			prlData->SatDebit	 = rmActualShifData.SatDebit;
			prlData->SunDebit	 = rmActualShifData.SunDebit;
		}
	} //-- End SHIFTPLAN --
	//-------------------------------------------------------------------------------------------

	// Weitere Bearbeitung der gewonnenen Daten
	// Alle Zeilen die leer sind werden gel�scht (soll/ist)
	DeleteNullFromShiftRequirementData();
	ChangeShiftRequirementBonusData();
	ChangeShiftRequirementGruppData();
	DisplayShiftRequirementData(ipScrollPos);

	CCS_CATCH_ALL;
}

//**********************************************************************************
// Daten werden in die Editfelder geschrieben
//**********************************************************************************

void ShiftRoster_View::DisplayShiftRosterData(int ilSet /*=0*/)
{
	CCS_TRY;

	omGSPTableData.Sort(CompareWeek);

	// Wieviele Zeilen gibt es ?
	int ilSize = omGSPTableData.GetSize();

	for (int i = 0; i < omGSPTable.GetSize(); i++)
	{
		omGSPTable[i].Houers->SetInitText(" ");

		// Alle Felder zr�cksetzen
		for (int ilDay = 0; ilDay < 7; ilDay++)
		{
			omGSPTable[i].Day[ilDay]->SetInitText(" ");
			omGSPTable[i].Day[ilDay]->SetBKColor(WHITE);
		}
		
		int ilData = ilSet + i; 
		if (ilData<ilSize)
		{
			omGSPTable[i].Week->SetInitText(omGSPTableData[ilData].DisplayWeek);

			int ilIs =  GetItemCount(omGSPTableData[ilData].StaffUrno,'|');
			int ilDebit = atoi(omGSPTableData[ilData].Number);
			if (ilIs == ilDebit)
			{
				omGSPTable[i].Number->SetTextChangedColor(BLACK);
				omGSPTable[i].Number->SetTextColor(BLACK);
			}
			else if (ilIs > ilDebit)
			{
				omGSPTable[i].Number->SetTextChangedColor(BLUE);
				omGSPTable[i].Number->SetTextColor(BLUE);
			}
			else
			{
				omGSPTable[i].Number->SetTextChangedColor(RED);
				omGSPTable[i].Number->SetTextColor(RED);
			}

			omGSPTable[i].Number->SetInitText(omGSPTableData[ilData].Number);

			if (this->omCustomer == "SIN" && imGSPwgpcIdx >= 0)
			{
				if (omGSPTable[i].Workgroup->SelectString(-1,omGSPTableData[ilData].Workgroup) == CB_ERR)
				{
					omGSPTable[i].Workgroup->SetCurSel(-1);
				}
			}

			if (imViewShowEmployee == VIEW_CODE)
			{
				omGSPTable[i].Staff->SetWindowText(omGSPTableData[ilData].StaffCode);
			}
			else if (imViewShowEmployee == VIEW_FREE)
			{
				omGSPTable[i].Staff->SetWindowText(omGSPTableData[ilData].StaffFree);
			}
			else
			{
				omGSPTable[i].Staff->SetWindowText(omGSPTableData[ilData].StaffFLName);
			}

			// Hier werden die eigendlichen Schichten gesetzt
			for (ilDay = 0; ilDay < 7; ilDay++)
			{
				if (imShowShiftNumber == 1 )
				{
					omGSPTable[i].Day[ilDay]->SetInitText(omGSPTableData[ilData].Day[ilDay]);
					omGSPTable[i].Day[ilDay]->SetTextColor(BLACK);
					// Sollte es eine zweite Schicht geben, mu� der Hintergrund gesetzt werden
					if (omGSPTableData[ilData].Day2[ilDay] != "" && omGSPTableData[ilData].Day2[ilDay] != " ")
					{
						omGSPTable[i].Day[ilDay]->SetBKColor(DT_LIGHTBLUE);
					}
				}
				else
				{
					omGSPTable[i].Day[ilDay]->SetInitText(omGSPTableData[ilData].Day2[ilDay]);
					omGSPTable[i].Day[ilDay]->SetTextColor(BLACK);
					if (omGSPTableData[ilData].Day[ilDay] != "" && omGSPTableData[ilData].Day[ilDay] != " ")
					{
						omGSPTable[i].Day[ilDay]->SetBKColor(DT_LIGHTBLUE);
					}
				}
			}

			omGSPTable[i].Houers->SetInitText(omGSPTableData[ilData].Houers);

			// Wochennummer wird gesetzt
			FillGSPTABLEPTR(omGSPTable[i].Number, omGSPTableData[ilData].Number,"","","",ilData,false);

			// Workgroup will be set
			if (this->omCustomer == "SIN" && imGSPwgpcIdx >= 0)
			{
				FillGSPTABLEPTR(omGSPTable[i].Workgroup, omGSPTableData[ilData].Workgroup,"","","",ilData,false);
			}

			// Mitarbeiter Namen werden gesetzt
			if (imViewShowEmployee == VIEW_CODE)
			{
				FillGSPTABLEPTR(omGSPTable[i].Staff, omGSPTableData[ilData].StaffCode,omGSPTableData[ilData].StaffUrno,"","",ilData,false);
			}
			else if (imViewShowEmployee == VIEW_FREE)
			{
				FillGSPTABLEPTR(omGSPTable[i].Staff, omGSPTableData[ilData].StaffFree,omGSPTableData[ilData].StaffUrno,"","",ilData,false);
			}
			else
			{
				FillGSPTABLEPTR(omGSPTable[i].Staff, omGSPTableData[ilData].StaffFLName,omGSPTableData[ilData].StaffUrno,"","",ilData,false);
			}

			// Schichtdaten werden gesetzt, jenachdem welche Schichtnummer angezeigt werden soll
			bool blSetBk = 0;
			if (imShowShiftNumber == 1)
			{
				for(ilDay=0; ilDay<7; ilDay++)
				{
					if (omGSPTableData[ilData].Day2[ilDay] != "" && omGSPTableData[ilData].Day2[ilDay] != " ")
					{
						blSetBk = true;
					}
					else
					{
						blSetBk = false;
					}

					FillGSPTABLEPTR (omGSPTable[i].Day[ilDay], omGSPTableData[ilData].Day[ilDay], omGSPTableData[ilData].DayUrno[ilDay],
						omGSPTableData[ilData].PfcCode[ilDay], omGSPTableData[ilData].PfcUrno[ilDay], ilData,blSetBk);
				}
			}
			else
			{
				for (ilDay = 0; ilDay < 7; ilDay++)
				{
					if (omGSPTableData[ilData].Day[ilDay] != "" && omGSPTableData[ilData].Day[ilDay] != " ")
					{
						blSetBk = true;
					}
					else
					{
						blSetBk = false;
					}

					FillGSPTABLEPTR(omGSPTable[i].Day[ilDay], omGSPTableData[ilData].Day2[ilDay], omGSPTableData[ilData].Day2Urno[ilDay],
						omGSPTableData[ilData].PfcCode2[ilDay], omGSPTableData[ilData].PfcUrno2[ilDay], ilData,blSetBk);
				}
			}
		}
		else
		{
			omGSPTable[i].Week->SetInitText("");
			omGSPTable[i].Number->SetInitText("");
			if (this->omCustomer == "SIN" && imGSPwgpcIdx >= 0)
			{
				omGSPTable[i].Workgroup->SetCurSel(-1);
			}
			omGSPTable[i].Staff->SetWindowText("");

			for (int ilDay = 0; ilDay < 7; ilDay++)
			{
				omGSPTable[i].Day[ilDay]->SetInitText("");
				FillGSPTABLEPTR(omGSPTable[i].Day[ilDay], "", "", "", "", -1, false);
			}
			
			FillGSPTABLEPTR(omGSPTable[i].Number,"","","","",-1,false);
			if (this->omCustomer == "SIN" && imGSPwgpcIdx >= 0)
			{
				FillGSPTABLEPTR(omGSPTable[i].Workgroup,"","","","",-1,false);
			}

			FillGSPTABLEPTR(omGSPTable[i].Staff, "","","","",-1,false);
		}
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// Auswahl in CB Requirement hat sich ge�ndert
//**********************************************************************************

void ShiftRoster_View::OnSelchangeCbRequirement() 
{
	CCS_TRY;

	CString olSDTTxt;
	CString olTmpUrno;
	CTime	olSdgBegi	= -1;
	long	llSdgUrno	= 0;
	int		ilCount;
	int		i;

	// Position der Auswahl bestimmen.
	int ilSelItem = pomComboBoxCoverage->GetCurSel();

	// Auswahl g�ltig
	if(ilSelItem != CB_ERR)
	{
		// WarteDialog einblenden.
		CWaitDlg olWaitDlg(this);

		// Das Schicht Daten Array l�schen
		ogMsdData.omUrnoMap.RemoveAll();
		ogMsdData.omData.DeleteAll();

		// Text der aktuellen Auswahl holen		
		pomComboBoxCoverage->GetLBText(ilSelItem,olSDTTxt);
		olSDTTxt = olSDTTxt.Left(32);
		olSDTTxt.TrimRight();

		// Testen ob der Text g�ltig ist
		if(olSDTTxt != "")
		{
			// Alles vorhandenen Schicht Bedarfs Gruppen durchgehen.
			ilCount = ogSdgData.omData.GetSize();
			for (i = 0; i < ilCount; i++)
			{
				// Schichtgruppen nach dem Eintrag der Listbox durchsuchen
				if(ogSdgData.omData[i].Dnam == olSDTTxt)
				{
					llSdgUrno = ogSdgData.omData[i].Urno;
					olSdgBegi = ogSdgData.omData[i].Begi;
					i = ilCount;
				}
			}

			// Liste der Code Bezeichnungen f�r die "funktionen"
			//omViewFunctionFctcs.Empty();
			CString olViewFctcs = "";
			// Anzahl der "Funktionen" die geladen wurden
			ilCount =  ogPfcData.omData.GetSize();
			// F�r jede bekannt "funktion" wird gepr�ft,ob diese angezeigt werden soll
			CString olTmpUrno;
			for (i = 0; i < ilCount; i++)
			{	
				olTmpUrno.Format(",%ld,",ogPfcData.omData[i].Urno);
				// Ist diese Urno in diesem View definiert 
				if(omViewFunctionUrnos.Find(olTmpUrno) != -1)
				{
					// Ja 
					//omViewFunctionFctcs += CString(",'") + ogPfcData.omData[i].Fctc + CString("'");
					olViewFctcs += CString(",'") + ogPfcData.omData[i].Fctc + CString("'");
				}
			}

			// Pr�fen ob der String der die Liste mit den "funktion"s codes enth�lt g�ltig ist
			if (olViewFctcs.GetLength() > 0)
			{
				// Weg mit dem ersten Komma
				olViewFctcs = olViewFctcs.Mid(1);

				// per SQL wird die Schichtbedarfgruppe ausgew�hlt, der Zeitraum und die "funktionen"
				CString olWhere;
				olWhere.Format("WHERE SDGU='%ld' AND FCTC IN (%s) ", llSdgUrno, olViewFctcs);

				// Neue Schichtdaten einlesen
				// SDT auf MSD
				ogMsdData.ReadSpecialData(&ogMsdData.omData,(char*)(LPCTSTR)olWhere,"*",false);
			}
		}
		GetShiftRequirementData(SHIFTDEMAND);
	}

	CCS_CATCH_ALL;
}

//**********************************************************************************
// 
//**********************************************************************************

void ShiftRoster_View::DisplayShiftRequirementData(int ilVariant)
{
	CCS_TRY;
	CString olTmpTxt;
	int ilSet = 0;
	int ilNSRRangeMax = 0;
	//** Set ilNSRRangeMax **
	for(int m=0;m<omNSRTableData.GetSize();m++)
	{
		if(omNSRTableData[m].Type == DETAIL && imViewStattDetail == VIEW_DETAIL)
		{
			ilNSRRangeMax++;
		}
		else if(omNSRTableData[m].Type == BONUS && imViewStattBonus == VIEW_BONUS)
		{
			ilNSRRangeMax++;
		}
		else if(omNSRTableData[m].Type == GRUPP && imViewStattGrupp == VIEW_GRUPP)
		{
			ilNSRRangeMax++;
		}
		else if(omNSRTableData[m].Type == SUM)
		{
			ilNSRRangeMax++;
		}
	}
	//** modify NSR-ScrollBar **
	switch(ilVariant)
	{
	case 1:
		{
			ilSet = m_SB_NSR.GetScrollPos();
		}
		break;
	case 2:
		{
			int ilOldNsrScrollPos =  m_SB_NSR.GetScrollPos();
			imNSRRangeMax = ilNSRRangeMax - imNSRLines;
			if (imNSRRangeMax < 0)
			{
				imNSRRangeMax = 0;
			}
			m_SB_NSR.SetScrollRange(imNSRRangeMin,imNSRRangeMax,false);
			if (ilOldNsrScrollPos>imNSRRangeMax)
			{
				m_SB_NSR.SetScrollPos(imNSRRangeMax);
			}
			else
			{
				m_SB_NSR.SetScrollPos(ilOldNsrScrollPos);
			}
			if (imNSRRangeMax > 0)
			{
				m_SB_NSR.EnableScrollBar(ESB_ENABLE_BOTH);
			}
			else
			{
				m_SB_NSR.EnableScrollBar(ESB_DISABLE_BOTH);
			}
			ilSet = m_SB_NSR.GetScrollPos();
		}
		break;
	case 3:
		{
			imNSRRangeMax = ilNSRRangeMax - imNSRLines;
			if (imNSRRangeMax < 0)
			{
				imNSRRangeMax = 0;
			}
			m_SB_NSR.SetScrollRange(imNSRRangeMin,imNSRRangeMax,false);
			m_SB_NSR.SetScrollPos(imNSRRangeMin);
			if (imNSRRangeMax > 0)
			{
				m_SB_NSR.EnableScrollBar(ESB_ENABLE_BOTH);
			}
			else
			{
				m_SB_NSR.EnableScrollBar(ESB_DISABLE_BOTH);
			}
			ilSet = 0;
		}
		break;
	}

	// die Sortierung bestimmen
	int ilSortstatistic = ogCfgData.GetMonitorForWindow(MON_SORTSTATISTIC);
	if(ilSortstatistic == 0)
	{
		// 0 - nach Schichtcode
		omNSRTableData.Sort(CompareGrupp);
	}
	else
	{
		// 1 - nach Schichtbeginn
		omNSRTableData.Sort(CompareShiftbeginn);
	}

	int ilSize = omNSRTableData.GetSize();
	int ilNextData = 0;
	for (int i = 0; i < omNSRTable.GetSize(); i++)
	{
		bool blNext = true;
		do
		{
			int ilData = ilSet + ilNextData + i; 
			if (ilData < ilSize)
			{
				if (omNSRTableData[ilData].Type != SUM)
				{
					int ilType = omNSRTableData[ilData].Type;
					if ((ilType == DETAIL && imViewStattDetail == VIEW_DETAIL)||(ilType == BONUS && imViewStattBonus == VIEW_BONUS)||(ilType == GRUPP && imViewStattGrupp == VIEW_GRUPP))
					{
						if(omNSRTableData[ilData].Grupp.GetLength() > 1)
						{
							olTmpTxt.Format("%s",omNSRTableData[ilData].Grupp.Mid(1));
						}
						omNSRTable[i].Grupp->SetInitText(olTmpTxt);

						SetShiftRequirementText(omNSRTable[i].Day[0],omNSRTableData[ilData].MonIs,omNSRTableData[ilData].MonDebit,imViewShowCover);
						SetShiftRequirementText(omNSRTable[i].Day[1],omNSRTableData[ilData].TueIs,omNSRTableData[ilData].TueDebit,imViewShowCover);
						SetShiftRequirementText(omNSRTable[i].Day[2],omNSRTableData[ilData].WndIs,omNSRTableData[ilData].WndDebit,imViewShowCover);
						SetShiftRequirementText(omNSRTable[i].Day[3],omNSRTableData[ilData].ThuIs,omNSRTableData[ilData].ThuDebit,imViewShowCover);
						SetShiftRequirementText(omNSRTable[i].Day[4],omNSRTableData[ilData].FriIs,omNSRTableData[ilData].FriDebit,imViewShowCover);
						SetShiftRequirementText(omNSRTable[i].Day[5],omNSRTableData[ilData].SatIs,omNSRTableData[ilData].SatDebit,imViewShowCover);
						SetShiftRequirementText(omNSRTable[i].Day[6],omNSRTableData[ilData].SunIs,omNSRTableData[ilData].SunDebit,imViewShowCover);
						blNext = false;
					}
				}
				else
				{
					if(ilSize == 1)
					{
						omNSRTable[i].Grupp->SetInitText("");
						omNSRTable[i].Day[0]->SetInitText("");
						omNSRTable[i].Day[1]->SetInitText("");
						omNSRTable[i].Day[2]->SetInitText("");
						omNSRTable[i].Day[3]->SetInitText("");
						omNSRTable[i].Day[4]->SetInitText("");
						omNSRTable[i].Day[5]->SetInitText("");
						omNSRTable[i].Day[6]->SetInitText("");
						blNext = false;
					}
					else
					{
						if(omBonusData.GetSize() > 0)
						{
							omNSRTable[i].Grupp->SetInitText(LoadStg(IDS_SHIFT_BRUTTO));
						}
						else
						{
							omNSRTable[i].Grupp->SetInitText(LoadStg(IDS_SHIFT_NETTO));
						}

						SetShiftRequirementText(omNSRTable[i].Day[0],omNSRTableData[ilData].MonIs,omNSRTableData[ilData].MonDebit,VIEW_DIFF,true);
						SetShiftRequirementText(omNSRTable[i].Day[1],omNSRTableData[ilData].TueIs,omNSRTableData[ilData].TueDebit,VIEW_DIFF,true);
						SetShiftRequirementText(omNSRTable[i].Day[2],omNSRTableData[ilData].WndIs,omNSRTableData[ilData].WndDebit,VIEW_DIFF,true);
						SetShiftRequirementText(omNSRTable[i].Day[3],omNSRTableData[ilData].ThuIs,omNSRTableData[ilData].ThuDebit,VIEW_DIFF,true);
						SetShiftRequirementText(omNSRTable[i].Day[4],omNSRTableData[ilData].FriIs,omNSRTableData[ilData].FriDebit,VIEW_DIFF,true);
						SetShiftRequirementText(omNSRTable[i].Day[5],omNSRTableData[ilData].SatIs,omNSRTableData[ilData].SatDebit,VIEW_DIFF,true);
						SetShiftRequirementText(omNSRTable[i].Day[6],omNSRTableData[ilData].SunIs,omNSRTableData[ilData].SunDebit,VIEW_DIFF,true);
						blNext = false;
					}
				}
			}
			else
			{
				omNSRTable[i].Grupp->SetInitText("");
				omNSRTable[i].Day[0]->SetInitText("");
				omNSRTable[i].Day[1]->SetInitText("");
				omNSRTable[i].Day[2]->SetInitText("");
				omNSRTable[i].Day[3]->SetInitText("");
				omNSRTable[i].Day[4]->SetInitText("");
				omNSRTable[i].Day[5]->SetInitText("");
				omNSRTable[i].Day[6]->SetInitText("");
				blNext = false;
			}
			if(blNext)
			{
				ilNextData++;
			}
		}
		while (blNext);
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// Auswahl in der Bonus CB hat sich ver�ndert
//**********************************************************************************

void ShiftRoster_View::OnSelchangeCbBonus() 
{
	CCS_TRY;
	m_E_Absence.SetInitText("");
	m_E_Holiday.SetInitText("");
	m_E_Roundup.SetInitText("");

	CString olUrno = "0";
	CString olTmpTxt;

	if (m_CB_Bonus.GetCount() != 0)
	{
		int ilSelItem = m_CB_Bonus.GetCurSel();

		if (ilSelItem != CB_ERR)
		{
			m_CB_Bonus.GetLBText(ilSelItem,olTmpTxt);
		}
		else
		{
			ilSelItem = 0;
			m_CB_Bonus.SetCurSel(ilSelItem);
		}
		m_CB_Bonus.GetLBText(ilSelItem,olTmpTxt);

		if (olTmpTxt.GetLength() > (olTmpTxt.Find("URNO=") + 5))
		{
			olUrno = olTmpTxt.Mid(olTmpTxt.Find("URNO=") + 5);
		}

		for (int i = 0; i < omBonusData.GetSize(); i++)
		{
			if (omBonusData[i].DBObgru == olUrno)
			{
				m_E_Absence.SetInitText(omBonusData[i].DBOdbab);
				m_E_Holiday.SetInitText(omBonusData[i].DBOdbho);
				m_E_Roundup.SetInitText(omBonusData[i].DBOroup.Right(1));
				i = omBonusData.GetSize();
			}
		}
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// 
//**********************************************************************************

void ShiftRoster_View::SetCodeNumberNull(UINT ipTyp)
{
	CCS_TRY;
	if(ipTyp == ALL || ipTyp == GPL) rmCodeNumberData.NrOfWeeks = 0;
	if(ipTyp == ALL || ipTyp == GPL) rmCodeNumberData.Work = 0;
	if(ipTyp == ALL || ipTyp == GPL) rmCodeNumberData.Free = 0;
	if(ipTyp == ALL || ipTyp == GPL) rmCodeNumberData.YearDebit = 0;
	if(ipTyp == ALL || ipTyp == GPL) rmCodeNumberData.YearIs = 0;
	if(ipTyp == ALL || ipTyp == GPL) rmCodeNumberData.PerWeek = 0.0;
	if(ipTyp == ALL || ipTyp == GPL) rmCodeNumberData.PerDay = 0.0;
	if(ipTyp == ALL || ipTyp == GPL) rmCodeNumberData.HouersGSP = 0;
	if(ipTyp == ALL || ipTyp == SPL) rmCodeNumberData.HouersAll = 0;
	if(ipTyp == ALL || ipTyp == GPL) rmCodeNumberData.MaSchedule = 0;
	if(ipTyp == ALL || ipTyp == GPL) rmCodeNumberData.MaAssign = 0;
	if(ipTyp == ALL || ipTyp == SPL) rmCodeNumberData.MaScheduleAll = 0;
	if(ipTyp == ALL || ipTyp == SPL) rmCodeNumberData.MaAssignAll = 0;
	CCS_CATCH_ALL;
}

//**********************************************************************************
// 
//**********************************************************************************

void ShiftRoster_View::AddOrSubIsDay(NSRTABLEDATA *prpData,int ipDay,int ipFaktor /*=1*/, int ipNr/*=1*/)
{
	CCS_TRY;
	
	NSRTABLEDATA *prlData = FindNSRTableGrupp((CString)"");
	if(prlData == NULL)
	{
		NSRTABLEDATA rlData;
		prlData = &rlData;
	}
	switch(ipDay)
	{
	case 1:
		prpData->MonIs += (ipNr*ipFaktor);
		prlData->MonIs += (ipNr*ipFaktor);
		break;
	case 2:
		prpData->TueIs += (ipNr*ipFaktor);
		prlData->TueIs += (ipNr*ipFaktor);
		break;
	case 3:
		prpData->WndIs += (ipNr*ipFaktor);
		prlData->WndIs += (ipNr*ipFaktor);
		break;
	case 4:
		prpData->ThuIs += (ipNr*ipFaktor);
		prlData->ThuIs += (ipNr*ipFaktor);
		break;
	case 5:
		prpData->FriIs += (ipNr*ipFaktor);
		prlData->FriIs += (ipNr*ipFaktor);
		break;
	case 6:
		prpData->SatIs += (ipNr*ipFaktor);
		prlData->SatIs += (ipNr*ipFaktor);
		break;
	case 7:
		prpData->SunIs += (ipNr*ipFaktor);
		prlData->SunIs += (ipNr*ipFaktor);
		break;
	default:
		{
			// Do nothing
		}
		break; 
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// If Debit = 0 and Is = 0 or Type = BONUS or GRUPP
// then delete record from omNSRTableData
//**********************************************************************************

void ShiftRoster_View::DeleteNullFromShiftRequirementData()
{
	CCS_TRY;
	long llBsdUrno;
	for (int i = omNSRTableData.GetSize() - 1; i >= 0; i--)
	{
		int ilNull = 0;
		ilNull += omNSRTableData[i].MonDebit;
		ilNull += omNSRTableData[i].TueDebit;
		ilNull += omNSRTableData[i].WndDebit;
		ilNull += omNSRTableData[i].ThuDebit;
		ilNull += omNSRTableData[i].FriDebit;
		ilNull += omNSRTableData[i].SatDebit;
		ilNull += omNSRTableData[i].SunDebit;
		ilNull += omNSRTableData[i].MonIs;
		ilNull += omNSRTableData[i].TueIs;
		ilNull += omNSRTableData[i].WndIs;
		ilNull += omNSRTableData[i].ThuIs;
		ilNull += omNSRTableData[i].FriIs;
		ilNull += omNSRTableData[i].SatIs;
		ilNull += omNSRTableData[i].SunIs;

		if ((ilNull == 0 && omNSRTableData[i].Type != SUM) || omNSRTableData[i].Type == BONUS || omNSRTableData[i].Type == GRUPP)
		{
			// Key herstellen
			llBsdUrno = atol(omNSRTableData[i].GruppUrno);
			BSDDATA* polBsd = ogBsdData.GetBsdByUrno(llBsdUrno);

			if (polBsd != NULL)
			{
				omNSRTableDataMap.RemoveKey(omSortCode + polBsd->Bsdc + omNSRTableData[i].Fctc);
			}

			omNSRTableData.DeleteAt(i);
		}
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// Add Bonus Records
//**********************************************************************************

void ShiftRoster_View::ChangeShiftRequirementBonusData()
{
	CCS_TRY;
	if (omBonusData.GetSize() > 0)
	{
		NSRTABLEDATA *prlData = FindNSRTableGrupp((CString)"");
		if (prlData == NULL)
		{
			NSRTABLEDATA rlData;
			prlData = &rlData;
		}
		prlData->MonDebit = rmActualShifData.MonDebit;
		prlData->TueDebit = rmActualShifData.TueDebit;
		prlData->WndDebit = rmActualShifData.WndDebit;
		prlData->ThuDebit = rmActualShifData.ThuDebit;
		prlData->FriDebit = rmActualShifData.FriDebit;
		prlData->SatDebit = rmActualShifData.SatDebit;
		prlData->SunDebit = rmActualShifData.SunDebit;
		
		for (int i = omBonusData.GetSize()-1; i>=0; i--)
		{
			NSRTABLEDATA rlDataSum;
			for (int j = omNSRTableData.GetSize() - 1; j >= 0; j--)
			{
				CString Urnolist = omBonusData[i].BsdUrnoList;
				CString GruppUrno = omNSRTableData[j].GruppUrno;
				
				if(omBonusData[i].BsdUrnoList.Find(omNSRTableData[j].GruppUrno) != -1)
				{
					rlDataSum.MonDebit += omNSRTableData[j].MonDebit;
					rlDataSum.TueDebit += omNSRTableData[j].TueDebit;
					rlDataSum.WndDebit += omNSRTableData[j].WndDebit;
					rlDataSum.ThuDebit += omNSRTableData[j].ThuDebit;
					rlDataSum.FriDebit += omNSRTableData[j].FriDebit;
					rlDataSum.SatDebit += omNSRTableData[j].SatDebit;
					rlDataSum.SunDebit += omNSRTableData[j].SunDebit;
					rlDataSum.MonIs += omNSRTableData[j].MonIs;
					rlDataSum.TueIs += omNSRTableData[j].TueIs;
					rlDataSum.WndIs += omNSRTableData[j].WndIs;
					rlDataSum.ThuIs += omNSRTableData[j].ThuIs;
					rlDataSum.FriIs += omNSRTableData[j].FriIs;
					rlDataSum.SatIs += omNSRTableData[j].SatIs;
					rlDataSum.SunIs += omNSRTableData[j].SunIs;
				}
			}
			int ilNull = 0;
			ilNull += rlDataSum.MonDebit;
			ilNull += rlDataSum.TueDebit;
			ilNull += rlDataSum.WndDebit;
			ilNull += rlDataSum.ThuDebit;
			ilNull += rlDataSum.FriDebit;
			ilNull += rlDataSum.SatDebit;
			ilNull += rlDataSum.SunDebit;

			if (ilNull > 0)
			{
				// Calculate Bonus
				//uhi 31.7.00 Neue Berechnung Bonus lt Anforderung
				NSRTABLEDATA *prlDataBonus = new NSRTABLEDATA;
				prlDataBonus->Grupp.Format("%s%s:  %s", omSortBonus, LoadStg(IDS_STRING322), omBonusData[i].SGRgrpn);
				prlDataBonus->GruppUrno = omBonusData[i].DBOurno;
				prlDataBonus->Type		= BONUS;

				double dlLeft = 0;
				double dlDBOdbab = atof(omBonusData[i].DBOdbab);
				double dlDBOdbho = atof(omBonusData[i].DBOdbho);
				double dlDBOroup = atof(omBonusData[i].DBOroup);
				double dlDiv = dlDBOdbab + dlDBOdbho;
				double dlTmpResult;;
				int ilMore = 0;

				//--MON-----------
				// Ergebnis = Bedarf * BonusProzent / 100 (Bsp.: Ergebnis = 35 * 25 / 100 = 8.75
				dlTmpResult = rlDataSum.MonDebit * dlDiv / 100;

				// Ganzzahl-Anteil des Bonus-Bedarfs zuweisen
				prlDataBonus->MonDebit = (int)dlTmpResult;

				// evtl. Aufrunden, wenn Nachkommateil >= "Rundungsgrenze" (z.B. >= 0.20000000)
				if (modf (dlTmpResult, &dlLeft) >= dlDBOroup)
				{
					prlDataBonus->MonDebit++;//aufrunden
				}
				prlData->MonDebit += prlDataBonus->MonDebit;
				
				// wenn Bedarf aus Coverage gedeckt, dann wird Bonus aufgef�llt (nur bis zum Maximum)
				if ((ilMore = rlDataSum.MonIs - rlDataSum.MonDebit) >= prlDataBonus->MonDebit)
				{
					prlDataBonus->MonIs = prlDataBonus->MonDebit;
				}
				else if (ilMore > 0)
				{
					prlDataBonus->MonIs = ilMore;
				}

				//--TUE-----------
				dlTmpResult = rlDataSum.TueDebit * dlDiv / 100;
				prlDataBonus->TueDebit = (int)dlTmpResult;
				if (modf (dlTmpResult, &dlLeft) >= dlDBOroup)
				{
					prlDataBonus->TueDebit++;//aufrunden
				}
				prlData->TueDebit += prlDataBonus->TueDebit;

				if ((ilMore = rlDataSum.TueIs - rlDataSum.TueDebit) >= prlDataBonus->TueDebit)
				{
					prlDataBonus->TueIs = prlDataBonus->TueDebit;
				}
				else if (ilMore > 0)
				{
					prlDataBonus->TueIs = ilMore;
				}

				//--WND-----------
				dlTmpResult = rlDataSum.WndDebit * dlDiv / 100;
				prlDataBonus->WndDebit = (int)dlTmpResult;
				if (modf (dlTmpResult, &dlLeft) >= dlDBOroup)
				{
					prlDataBonus->WndDebit++;//aufrunden
				}
				prlData->WndDebit += prlDataBonus->WndDebit;

				if ((ilMore = rlDataSum.WndIs-rlDataSum.WndDebit) >= prlDataBonus->WndDebit)
				{
					prlDataBonus->WndIs = prlDataBonus->WndDebit;
				}
				else if (ilMore > 0)
				{
					prlDataBonus->WndIs = ilMore;
				}

				//--THU-----------
				dlTmpResult = rlDataSum.ThuDebit * dlDiv / 100;
				prlDataBonus->ThuDebit = (int)dlTmpResult;
				if (modf (dlTmpResult, &dlLeft) >= dlDBOroup)
				{
					prlDataBonus->ThuDebit++;//aufrunden
				}
				prlData->ThuDebit += prlDataBonus->ThuDebit;

				if ((ilMore = rlDataSum.ThuIs-rlDataSum.ThuDebit) >= prlDataBonus->ThuDebit)
				{
					prlDataBonus->ThuIs = prlDataBonus->ThuDebit;
				}
				else if (ilMore > 0)
				{
					prlDataBonus->ThuIs = ilMore;
				}

				//--FRI-----------
				dlTmpResult = rlDataSum.FriDebit * dlDiv / 100;
				prlDataBonus->FriDebit = (int)dlTmpResult;
				if (modf (dlTmpResult, &dlLeft) >= dlDBOroup)
				{
					prlDataBonus->FriDebit++;//aufrunden
				}
				prlData->FriDebit += prlDataBonus->FriDebit;

				if ((ilMore = rlDataSum.FriIs-rlDataSum.FriDebit) >= prlDataBonus->FriDebit)
				{
					prlDataBonus->FriIs = prlDataBonus->FriDebit;
				}
				else if (ilMore > 0)
				{
					prlDataBonus->FriIs = ilMore;
				}

				//--SAT-----------
				dlTmpResult = rlDataSum.SatDebit * dlDiv / 100;
				prlDataBonus->SatDebit = (int)dlTmpResult;
				if (modf (dlTmpResult, &dlLeft) >= dlDBOroup)
				{
					prlDataBonus->SatDebit++;//aufrunden
				}
				prlData->SatDebit += prlDataBonus->SatDebit;

				if ((ilMore = rlDataSum.SatIs-rlDataSum.SatDebit)>=prlDataBonus->SatDebit)
				{
					prlDataBonus->SatIs = prlDataBonus->SatDebit;
				}
				else if (ilMore > 0)
				{
					prlDataBonus->SatIs = ilMore;
				}

				//--SUN-----------
				dlTmpResult = rlDataSum.SunDebit * dlDiv / 100;
				prlDataBonus->SunDebit = (int)dlTmpResult;
				if (modf (dlTmpResult, &dlLeft) >= dlDBOroup)
				{
					prlDataBonus->SunDebit++;//aufrunden
				}
				prlData->SunDebit += prlDataBonus->SunDebit;

				if ((ilMore = rlDataSum.SunIs-rlDataSum.SunDebit)>=prlDataBonus->SunDebit)
				{
					prlDataBonus->SunIs = prlDataBonus->SunDebit;
				}
				else if (ilMore > 0)
				{
					prlDataBonus->SunIs = ilMore;
				}
				//--End-----------
				omNSRTableData.Add(prlDataBonus);
				omNSRTableDataMap.SetAt(prlDataBonus->Grupp,prlDataBonus);
			}
		}
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// Add Grupp Records
//**********************************************************************************

void ShiftRoster_View::ChangeShiftRequirementGruppData()
{
	CCS_TRY;

	CString olSgrUrnos,olTmpTxt;
	CString clTabn("BSD");
	omGruppData.DeleteAll();

	for (int i = ogSgrData.omData.GetSize() - 1; i >= 0; i--)
	{
		CString clTabn2(ogSgrData.omData[i].Tabn);
		if (strcmp(clTabn2,clTabn) == 0)
		{
			GRUPPDATA *prlGruppData = new GRUPPDATA;

			prlGruppData->Grpn = ogSgrData.omData[i].Grpn;
			prlGruppData->Urno.Format("%i",ogSgrData.omData[i].Urno);
			omGruppData.Add(prlGruppData);
			olTmpTxt.Format("%d",ogSgrData.omData[i].Urno);
			olSgrUrnos += olTmpTxt + "|";
		}
	}
	CString olUsgr;
	for (i = ogSgmData.omData.GetSize()-1; i >= 0; i--)
	{
		olUsgr.Format("%d",ogSgmData.omData[i].Usgr);
		if (olSgrUrnos.Find(olUsgr) != -1)
		{
			for (int j = omGruppData.GetSize() - 1; j >= 0; j--)
			{
				if(omGruppData[j].Urno == olUsgr)
				{
					omGruppData[j].Valu += (CString)ogSgmData.omData[i].Uval + "|";
				}
			}
		}
	}
	
	for (i = omGruppData.GetSize() - 1; i >= 0; i--)
	{
		NSRTABLEDATA rlDataSum;
		for (int j = omNSRTableData.GetSize() - 1; j >= 0; j--)
		{
			if(omGruppData[i].Valu.Find(omNSRTableData[j].GruppUrno) != -1)
			{
				rlDataSum.MonDebit += omNSRTableData[j].MonDebit;
				rlDataSum.TueDebit += omNSRTableData[j].TueDebit;
				rlDataSum.WndDebit += omNSRTableData[j].WndDebit;
				rlDataSum.ThuDebit += omNSRTableData[j].ThuDebit;
				rlDataSum.FriDebit += omNSRTableData[j].FriDebit;
				rlDataSum.SatDebit += omNSRTableData[j].SatDebit;
				rlDataSum.SunDebit += omNSRTableData[j].SunDebit;
				rlDataSum.MonIs    += omNSRTableData[j].MonIs;
				rlDataSum.TueIs    += omNSRTableData[j].TueIs;
				rlDataSum.WndIs    += omNSRTableData[j].WndIs;
				rlDataSum.ThuIs    += omNSRTableData[j].ThuIs;
				rlDataSum.FriIs    += omNSRTableData[j].FriIs;
				rlDataSum.SatIs    += omNSRTableData[j].SatIs;
				rlDataSum.SunIs    += omNSRTableData[j].SunIs;
			}
		}
		int ilNull = 0;
		ilNull += rlDataSum.MonDebit;
		ilNull += rlDataSum.TueDebit;
		ilNull += rlDataSum.WndDebit;
		ilNull += rlDataSum.ThuDebit;
		ilNull += rlDataSum.FriDebit;
		ilNull += rlDataSum.SatDebit;
		ilNull += rlDataSum.SunDebit;
		ilNull += rlDataSum.MonIs;
		ilNull += rlDataSum.TueIs;
		ilNull += rlDataSum.WndIs;
		ilNull += rlDataSum.ThuIs;
		ilNull += rlDataSum.FriIs;
		ilNull += rlDataSum.SatIs;
		ilNull += rlDataSum.SunIs;

		if (ilNull > 0)
		{
			NSRTABLEDATA *prlDataGrupp = new NSRTABLEDATA;
			prlDataGrupp->Grupp.Format("%s%s:  %s",omSortGrupp, LoadStg(PR_DSR_GROUP), omGruppData[i].Grpn);
			prlDataGrupp->GruppUrno = omGruppData[i].Urno;
			prlDataGrupp->Type		= GRUPP;

			prlDataGrupp->MonDebit = rlDataSum.MonDebit;
			prlDataGrupp->TueDebit = rlDataSum.TueDebit;
			prlDataGrupp->WndDebit = rlDataSum.WndDebit;
			prlDataGrupp->ThuDebit = rlDataSum.ThuDebit;
			prlDataGrupp->FriDebit = rlDataSum.FriDebit;
			prlDataGrupp->SatDebit = rlDataSum.SatDebit;
			prlDataGrupp->SunDebit = rlDataSum.SunDebit;
			prlDataGrupp->MonIs    = rlDataSum.MonIs   ;
			prlDataGrupp->TueIs    = rlDataSum.TueIs   ;
			prlDataGrupp->WndIs    = rlDataSum.WndIs   ;
			prlDataGrupp->ThuIs    = rlDataSum.ThuIs   ;
			prlDataGrupp->FriIs    = rlDataSum.FriIs   ;
			prlDataGrupp->SatIs    = rlDataSum.SatIs   ;
			prlDataGrupp->SunIs    = rlDataSum.SunIs   ;

			omNSRTableData.Add(prlDataGrupp);
			omNSRTableDataMap.SetAt(prlDataGrupp->Grupp,prlDataGrupp);
		}
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// hier werden die Eintrage f�r die Daten f�r die Editfelder gesetzt
//**********************************************************************************

void ShiftRoster_View::FillGSPTABLEPTR(void *pvpField, CString opBsdCode, CString opBsdUrno, CString opPfcCode,
									   CString opPfcUrno, int ipRecord, bool bpSetBk)
{
	CCS_TRY;
	GSPTABLEPTR *prlData;

	// Feld mu� vorhanden sein
	prlData =  FindGSPTablePtr(pvpField);
	if(prlData != NULL)
	{
		// Daten setzten
		prlData->bSetBk = bpSetBk;
		prlData->Record = ipRecord;
		prlData->BsdCode = opBsdCode;
		prlData->BsdUrno = opBsdUrno;
		prlData->PfcCode = opPfcCode;
		prlData->PfcUrno = opPfcUrno;
		
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// 
//**********************************************************************************

void ShiftRoster_View::SetShiftRequirementText(CCSEdit *popField,int ipIs, int ipDebit,int ipView /*= VIEW_ISDEBIT*/,bool bpNull /*=false*/)
{
	CCS_TRY;
	CString olTmpTxt;

	switch(ipView)
	{
		case VIEW_ISDEBIT:
			olTmpTxt.Format("%d/%d",ipIs,ipDebit);
			if(ipIs == 0 && ipDebit == 0)
			{
				olTmpTxt = "";
			}
			break;
		case VIEW_DIFF:
			olTmpTxt.Format("%+d",ipIs-ipDebit);
			if ((ipIs - ipDebit) == 0 && bpNull == true)
			{
				olTmpTxt = "0";
			}
			else if((ipIs-ipDebit) == 0 && bpNull == false)
			{
				olTmpTxt = "";
			}
			break;
	}

	if (ipIs == ipDebit)
	{
		popField->SetTextColor(BLACK);
	}
	else if (ipIs > ipDebit)
	{
		popField->SetTextColor(BLUE);
	}
	else
	{
		popField->SetTextColor(RED);
	}
	popField->SetInitText(olTmpTxt);
	CCS_CATCH_ALL;
}

//**********************************************************************************
// 
//**********************************************************************************

GSPTABLEPTR* ShiftRoster_View::FindGSPTablePtr(void *pvpField)
{
	CCS_TRY;
	GSPTABLEPTR *prlData;
	if(omGSPTablePtrMap.Lookup(pvpField,(void *&)prlData) == TRUE)
	{
		return prlData;
	}
	return NULL;
	CCS_CATCH_ALL;
	return NULL;
}

//**********************************************************************************
// �nderung in der Grundschichtplan Listbox
//**********************************************************************************

void ShiftRoster_View::OnSelchangeLbGpllist() 
{
	CCS_TRY;
	int ilSelItem = m_LB_GPLList.GetCurSel();
	if(ilSelItem != LB_ERR)
	{
		CString olTmpTxt,olAktivate;
		CString olGPLurno = "0";
		
		om_SelAWDlgUrnos = "";
		
		m_LB_GPLList.GetText(ilSelItem,olTmpTxt);
		if(olTmpTxt.GetLength() > (olTmpTxt.Find("URNO=")+5))
			olGPLurno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
		olAktivate = olTmpTxt.Left(1);
		if(olAktivate == omAktiv)
		{
			if(ogPrivList.GetStat("SR_DEACT") == '1')
				m_B_Assign.EnableWindow(TRUE);
			m_B_Assign.SetWindowText(LoadStg(SHIFT_B_DEAKTIVATE));
		}
		else if(olAktivate == " ")
		{
			if(ogPrivList.GetStat("SR_DEACT") == '1')
				m_B_Assign.EnableWindow(TRUE);
			m_B_Assign.SetWindowText(LoadStg(SHIFT_B_AKTIVATE));
		}
		else
		{
			m_B_Assign.EnableWindow(FALSE);
			m_B_Assign.SetWindowText(LoadStg(SHIFT_B_DEAKTIVATE));
		}
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// Doppelklick in der Grundschichtplan Listbox
//**********************************************************************************

void ShiftRoster_View::OnDblclkLbGpllist() 
{
	CCS_TRY;
	int ilMBoxReturn = IDYES;
	if(bmIsChangedGPL == true)
	{
		ilMBoxReturn = MessageBox(LoadStg(SHIFT_ISCHANGED_GPL),LoadStg(ST_FRAGE),(MB_ICONQUESTION | MB_YESNOCANCEL | MB_DEFBUTTON1));
		if(ilMBoxReturn == IDYES)
		{
			SaveGPL();
			if(bmIsChangedGPL == true)
				ilMBoxReturn = IDCANCEL;
		}
		if(ilMBoxReturn == IDNO && omActualSPLurnoByGPL == omSelSPLurno)
		{
			GetShiftRequirementData(SHIFTPLAN);
		}
	}
	if(ilMBoxReturn != IDCANCEL)
	{
		int ilSelItem = m_LB_GPLList.GetCurSel();
		if(ilSelItem != LB_ERR)
		{
			CString olTmpTxt,olGPLurno = "0";
			m_LB_GPLList.GetText(ilSelItem,olTmpTxt);
			if(olTmpTxt.GetLength() > (olTmpTxt.Find("URNO=")+5))
			{
				olGPLurno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
			}
			if(olTmpTxt.GetLength() > 2)
			{
				olTmpTxt = olTmpTxt.Mid(2);
			}
			m_E_GPL_Caption.SetInitText(olTmpTxt);
			
			
			CString olGPLperi,olGPLvafr,olGPLvato,olGPLcotu,olGPLstpt;
			
			
			//MessageBox(olGPLurno);
			
			ogBCD.GetField("GPL", "URNO", olGPLurno, "PERI", olGPLperi);
			ogBCD.GetField("GPL", "URNO", olGPLurno, "VAFR", olGPLvafr);
			ogBCD.GetField("GPL", "URNO", olGPLurno, "VATO", olGPLvato);
			ogBCD.GetField("GPL", "URNO", olGPLurno, "COTU", olGPLcotu);
			ogBCD.GetField("GPL", "URNO", olGPLurno, "SPLU", omActualSPLurnoByGPL);
			ogBCD.GetField("GPL", "URNO", olGPLurno, "STPT", olGPLstpt);
			omSelGPLurno = olGPLurno;
			if(olGPLperi == "")
				olGPLperi = "0";

			m_SBC_Periode.SetPos(atoi(olGPLperi)/7);
			im_SBC_Periode = atoi(olGPLperi)/7;

		int a = omGSPTableData.GetSize();

		//	MessageBox(olGPLperi);
		
			
			m_E_GPL_Peri.SetInitText(olGPLperi);

			if(olGPLvafr.GetLength() >= 8)
				olGPLvafr.Format("%s.%s.%s",olGPLvafr.Mid(6,2),olGPLvafr.Mid(4,2),olGPLvafr.Mid(0,4));
			if(olGPLvato.GetLength() >= 8)
				olGPLvato.Format("%s.%s.%s",olGPLvato.Mid(6,2),olGPLvato.Mid(4,2),olGPLvato.Mid(0,4));
			if (olGPLstpt.GetLength() >= 8)
				olGPLstpt.Format("%s.%s.%s",olGPLstpt.Mid(6,2),olGPLstpt.Mid(4,2),olGPLstpt.Mid(0,4));
			else
				olGPLstpt = olGPLvafr;

			m_E_GPL_Vato.SetInitText(olGPLvato);
			m_E_GPL_Vafr.SetInitText(olGPLvafr);
			m_E_GPL_StPt.SetInitText(olGPLstpt);

			COleDateTimeSpan olDiff = OleDateStringToDate(olGPLstpt) - OleDateStringToDate(olGPLvafr);
			this->imStPtWeek = (int)olDiff.GetTotalDays() / 7 + 1;
			
			GetShiftRosterData(olGPLurno);
		}
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// Laden der Grundschichtplan Daten von GPL opGPLurno
//**********************************************************************************

void ShiftRoster_View::GetShiftRosterData(CString opGPLurno)
{
	CCS_TRY;
	CString olTmpTxt,olWhere;
	omGSPTableData.DeleteAll();
	omDeletedGSPs.DeleteAll();
	DeleteSelectGSPTableFields();
	bmIsChangedGPL = false;

	if (bmIsChangedBonus == false && bmIsChangedGPL == false)
	{
		((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(FALSE); // COLOR
	}
	
	SetCodeNumberNull(GPL);	// CodeNumber

	int ilGSPbuDayIdx[7];
	int ilGSP2buDayIdx[7];
	int ilGSPpfcDayIdx[7];
	int ilGSP2pfcDayIdx[7];

	ilGSPbuDayIdx[0] = imGSPbumoIdx;
	ilGSPbuDayIdx[1] = imGSPbutuIdx;
	ilGSPbuDayIdx[2] = imGSPbuweIdx;
	ilGSPbuDayIdx[3] = imGSPbuthIdx;
	ilGSPbuDayIdx[4] = imGSPbufrIdx;
	ilGSPbuDayIdx[5] = imGSPbusaIdx;
	ilGSPbuDayIdx[6] = imGSPbusuIdx;

	ilGSP2buDayIdx[0] = imGSPsumoIdx;
	ilGSP2buDayIdx[1] = imGSPsutuIdx;
	ilGSP2buDayIdx[2] = imGSPsuweIdx;
	ilGSP2buDayIdx[3] = imGSPsuthIdx;
	ilGSP2buDayIdx[4] = imGSPsufrIdx;
	ilGSP2buDayIdx[5] = imGSPsusaIdx;
	ilGSP2buDayIdx[6] = imGSPsusuIdx;

	ilGSPpfcDayIdx[0] = imGSPp1moIdx;
	ilGSPpfcDayIdx[1] = imGSPp1tuIdx;
	ilGSPpfcDayIdx[2] = imGSPp1weIdx;
	ilGSPpfcDayIdx[3] = imGSPp1thIdx;
	ilGSPpfcDayIdx[4] = imGSPp1frIdx;
	ilGSPpfcDayIdx[5] = imGSPp1saIdx;
	ilGSPpfcDayIdx[6] = imGSPp1suIdx;

	ilGSP2pfcDayIdx[0] = imGSPp2moIdx;
	ilGSP2pfcDayIdx[1] = imGSPp2tuIdx;
	ilGSP2pfcDayIdx[2] = imGSPp2weIdx;
	ilGSP2pfcDayIdx[3] = imGSPp2thIdx;
	ilGSP2pfcDayIdx[4] = imGSPp2frIdx;
	ilGSP2pfcDayIdx[5] = imGSPp2saIdx;
	ilGSP2pfcDayIdx[6] = imGSPp2suIdx;

	//-- Select Employment contract (COT) --
	CString olGPLcotu;
	ogBCD.GetField("GPL", "URNO", opGPLurno, "COTU", olGPLcotu);
	COTDATA *polCot = ogCotData.GetCotByUrno(atol(olGPLcotu));
	if (polCot != NULL)
	{
		CString olLine;
		olLine.Format("%-32s                                    URNO=%s",polCot->Ctrc,olGPLcotu);
		int ilIdx =  m_CB_Contract.FindStringExact(-1,olLine);
		if(ilIdx !=  CB_ERR)
		{
			m_CB_Contract.SetCurSel(ilIdx);
			rmCodeNumberData.YearDebit = (double)(atof(polCot->Whpw) * (365/7));
		}
		else
		{
			rmCodeNumberData.YearDebit = 0;
			m_CB_Contract.SetCurSel(-1);
		}
	}
	else
	{
		rmCodeNumberData.YearDebit = 0;
		m_CB_Contract.SetCurSel(-1);
	}
	//-- End Select Employment contract --


	//-- Select Organizational Unit (ORG) --
	if (ogBCD.GetFieldIndex("GPL","ORGU") > -1)
	{
		CString olGPLorgu;
		ogBCD.GetField("GPL", "URNO", opGPLurno, "ORGU", olGPLorgu);
		ORGDATA *polOrg = ogOrgData.GetOrgByUrno(atol(olGPLorgu));
		if (polOrg != NULL)
		{
			CString olLine;
			olLine.Format("%-32s                                    URNO=%s",polOrg->Dpt1,olGPLorgu);
			int ilIdx =  m_CB_OrgUnit.FindStringExact(-1,olLine);
			if(ilIdx !=  CB_ERR)
			{
				m_CB_OrgUnit.SetCurSel(ilIdx);
			}
			else
			{
				m_CB_OrgUnit.SetCurSel(-1);
			}
		}
		else
		{
			m_CB_OrgUnit.SetCurSel(-1);
		}
	}
	//-- End Select Organizational Unit --


	RecordSet *polGSPRecord = new RecordSet(ogBCD.GetFieldCount("GSP"));
	RecordSet *polGSPWeek	= new RecordSet(ogBCD.GetFieldCount("GSP"));
	
	CCSPtrArray<RecordSet> olRecordList;
	ogBCD.GetRecords("GSP","GPLU",opGPLurno,&olRecordList);

	for (int i = 0; i < olRecordList.GetSize(); i++)
	{
		// Alle GSP holen 
		*polGSPRecord = olRecordList[i];

		// Nur die mit der richtigen Urno 
		if (polGSPRecord->Values[imGSPgpluIdx] == opGPLurno)
		{
			double ifMinutes = 0;
			GSPTABLEDATA *prlGplData = new GSPTABLEDATA;
			rmCodeNumberData.NrOfWeeks++; // CodeNumber

			// Urno setzen
			prlGplData->GSPUrno = polGSPRecord->Values[imGSPurnoIdx];

			int ilData = atoi(polGSPRecord->Values[imGSPweekIdx]);
			int ilWeek = RotateShiftRoster(ilData,this->imStPtWeek,olRecordList.GetSize());
			
			olTmpTxt.Format("%03d",ilData);
			prlGplData->Week = olTmpTxt;		// for staff and sort order

			olTmpTxt.Format("%03d",ilWeek);
			prlGplData->DisplayWeek = olTmpTxt;	// for week number and shift rotation

			CString olStaffListFLName;
			CString olStaffListCode;
			CString olStaffFreeFormatted;
			polGSPRecord->Values[imGSPstfuIdx].Replace("�", "|");
			polGSPRecord->Values[imGSPstfnIdx].Replace("�", "|");
			if (!GetStaffListByUrnoList(polGSPRecord->Values[imGSPstfuIdx],&olStaffListFLName,&olStaffListCode,&olStaffFreeFormatted))
			{
				CheckRemoveStfus(polGSPRecord);
			}
			prlGplData->StaffUrno	= polGSPRecord->Values[imGSPstfuIdx];
			prlGplData->StaffFLName = olStaffListFLName;
			prlGplData->StaffCode	= olStaffListCode;
			prlGplData->StaffFree	= olStaffFreeFormatted;

			rmCodeNumberData.MaAssign += GetItemCount(polGSPRecord->Values[imGSPstfuIdx],'|'); // CodeNumber

			if (polGSPRecord->Values[imGSPnstfIdx] != "")
			{
				prlGplData->Number = polGSPRecord->Values[imGSPnstfIdx];
				rmCodeNumberData.MaSchedule += atoi(polGSPRecord->Values[imGSPnstfIdx]); // CodeNumber
			}
			else
			{
				prlGplData->Number = "0";
			}

			if (this->omCustomer == "SIN" && imGSPwgpcIdx >= 0 && polGSPRecord->Values[imGSPwgpcIdx] != "")
			{
				prlGplData->Workgroup = polGSPRecord->Values[imGSPwgpcIdx];
			}
			else
			{
				prlGplData->Workgroup = "";
			}

			if (ilData == ilWeek)	// StPt == Vafr
			{
				for (int ilDay = 0; ilDay < 7; ilDay++)
				{
					// Holt Anhand der Urno Informationen �ber die Schicht.Wird in prlGplData abgelegt
					GetDayStuff (polGSPRecord->Values[ilGSPbuDayIdx[ilDay]],
								 polGSPRecord->Values[ilGSP2buDayIdx[ilDay]],
								 polGSPRecord->Values[ilGSPpfcDayIdx[ilDay]],
								 polGSPRecord->Values[ilGSP2pfcDayIdx[ilDay]],
								 ilDay,
								 prlGplData,
								 &ifMinutes);
				}
			}
			else
			{
				bool blWeekFound = false;
				for (int j = 0; j < olRecordList.GetSize(); j++)
				{
					*polGSPWeek = olRecordList[j];
					if (atoi(polGSPWeek->Values[imGSPweekIdx]) == ilWeek)
					{
						blWeekFound = true;
						break;							
					}
				}


				for (int ilDay = 0; ilDay < 7; ilDay++)
				{
					if (!blWeekFound)
					{
						// Holt Anhand der Urno Informationen �ber die Schicht.Wird in prlGplData abgelegt
						GetDayStuff (polGSPRecord->Values[ilGSPbuDayIdx[ilDay]],
									 polGSPRecord->Values[ilGSP2buDayIdx[ilDay]],
									 polGSPRecord->Values[ilGSPpfcDayIdx[ilDay]],
									 polGSPRecord->Values[ilGSP2pfcDayIdx[ilDay]],
									 ilDay,
									 prlGplData,
									 &ifMinutes);
					}
					else
					{
						// Holt Anhand der Urno Informationen �ber die Schicht.Wird in prlGplData abgelegt
						GetDayStuff (polGSPWeek->Values[ilGSPbuDayIdx[ilDay]],
									 polGSPWeek->Values[ilGSP2buDayIdx[ilDay]],
									 polGSPWeek->Values[ilGSPpfcDayIdx[ilDay]],
									 polGSPWeek->Values[ilGSP2pfcDayIdx[ilDay]],
									 ilDay,
									 prlGplData,
									 &ifMinutes);
					}
				}
			}

			// Minuten formatieren
			prlGplData->Houers.Format("%#01.2f", ifMinutes / 60);

			// Zu Array hinzuf�gen
			omGSPTableData.Add(prlGplData);
		}
	}
	olRecordList.DeleteAll();
	delete polGSPRecord;
	delete polGSPWeek;

	if (IsCodeDlgActive())
	{
		pomShiftCodeNumberDlg->SetValues(rmCodeNumberData);
		pomShiftCodeNumberDlg->SetWeeks(omGSPTableData.GetSize());
		pomShiftCodeNumberDlg->SetDataArray(omGSPTableData);
		pomShiftCodeNumberDlg->ReNewAll();
	}

	int ilSize = omGSPTableData.GetSize();
	imGSPRangeMax = ilSize - imGSPLines;

	if (imGSPRangeMax<0)
	{
		imGSPRangeMax = 0;
	}
	m_SB_GSP.SetScrollRange(imGSPRangeMin,imGSPRangeMax,false);
	m_SB_GSP.SetScrollPos(imGSPRangeMin);

	if (imGSPRangeMax > 0)
	{
		m_SB_GSP.EnableScrollBar(ESB_ENABLE_BOTH);
	}
	else
	{
		m_SB_GSP.EnableScrollBar(ESB_DISABLE_BOTH);
	}

	if(ilSize > 0)
	{
		imActualLine = 0;
	}
	else
	{
		imActualLine = -1;
	}

	DisplayShiftRosterData();
	CCS_CATCH_ALL;
}

/***************************************************************
Holt Schichtdaten eines Tages
Aus der Urno opUrno werden die Daten (Code usw) ermittelt
Hier werden alle Daten Arrays gef�llt
***************************************************************/
void ShiftRoster_View::GetDayStuff(CString opUrno, CString op2Urno, CString opPfc, CString op2Pfc, int ipDay, GSPTABLEDATA *prpGplData, double* dpMinutes)
{
	PFCDATA *polPfcData;
	
	//
	// Erste Schicht
	//
	long llUrno = atol(opUrno);
	BSDDATA* prlBsdData = ogBsdData.GetBsdByUrno(llUrno);
	if (prlBsdData != NULL)
	{
		rmCodeNumberData.Work++;
		prpGplData->DayUrno[ipDay]	= opUrno;
		prpGplData->Day[ipDay]		= prlBsdData->Bsdc;
		*dpMinutes += atoi (prlBsdData->Sdu1);
		rmCodeNumberData.HouersGSP += atoi(prlBsdData->Sdu1);
	}
	else 
	{
		ODADATA* prlOdaData = ogOdaData.GetOdaByUrno(llUrno);
		if (prlOdaData != NULL)
		{
			prlOdaData = ogOdaData.GetOdaByUrno(llUrno);
			prpGplData->DayUrno[ipDay] = opUrno;
			prpGplData->Day[ipDay]	= prlOdaData->Sdac;
		}
		else if (opUrno != "" && opUrno != "0")
		{
			prpGplData->DayUrno[ipDay]	= opUrno;
			prpGplData->Day[ipDay]		= "-?-";
		}
	}
	polPfcData = ogPfcData.GetPfcByUrno (atol(opPfc));
	if (polPfcData != NULL)
	{
		prpGplData->PfcCode[ipDay] = polPfcData->Fctc;
		prpGplData->PfcUrno[ipDay] = opPfc;
	}

	//
	// Zwote Schicht
	//
	llUrno = atol(op2Urno);
	prlBsdData = ogBsdData.GetBsdByUrno(llUrno);
	if (prlBsdData != NULL)
	{
		rmCodeNumberData.Work++; // CodeNumber
		prpGplData->Day2Urno[ipDay] = op2Urno;
		prpGplData->Day2[ipDay]	= prlBsdData->Bsdc;
		*dpMinutes += atoi(prlBsdData->Sdu1);
		rmCodeNumberData.HouersGSP += atoi(prlBsdData->Sdu1); // CodeNumber
	}
	else 
	{
		ODADATA* prlOdaData = ogOdaData.GetOdaByUrno(llUrno);
		if(prlOdaData != NULL)
		{
			prlOdaData = ogOdaData.GetOdaByUrno(llUrno);
			prpGplData->Day2Urno[ipDay] = op2Urno;
			prpGplData->Day2[ipDay]	= prlOdaData->Sdac;
		}
		else if(op2Urno != "" && op2Urno != "0")
		{
			prpGplData->Day2Urno[ipDay] = op2Urno;
			prpGplData->Day2[ipDay]	= "-?-";
		}
	}
	polPfcData = ogPfcData.GetPfcByUrno (atol(op2Pfc));
	if (polPfcData != NULL)
	{
		prpGplData->PfcCode2[ipDay] = polPfcData->Fctc;
		prpGplData->PfcUrno2[ipDay] = op2Pfc;
	}
}

//**********************************************************************************
// 
//**********************************************************************************
  
void ShiftRoster_View::DeleteSelectGSPTableFields()
{
	CCS_TRY;

	GSPTABLEPTR *prlData;

	for (int i = 0; i < omSelectGSPTableData.GetSize(); i++)
	{
		// Pr�fen welche Schichtnummer und dann Hintergrund enstprechend setzen
		prlData =  FindGSPTablePtr(omSelectGSPTableData[i].SourceControl);
		if (prlData->bSetBk)
		{
			omSelectGSPTableData[i].SourceControl->SetBKColor(DT_LIGHTBLUE);
		}
		else
		{
			omSelectGSPTableData[i].SourceControl->SetBKColor(WHITE);
		}
	}
	omSelectGSPTableData.DeleteAll();
	bmIsDragDropAction = false;
	CCS_CATCH_ALL;
}

//**********************************************************************************
// 
//**********************************************************************************

bool ShiftRoster_View::GetStaffListByUrnoList(CString opUrnoList,CString *popStaffListFLName,CString *popStaffListCode,CString* popStaffFree)
{
	CCS_TRY;
	bool blRet = true;
	STFDATA* prlStfData;
	CString olStaffListFLName,olStaffListCode,olStaffFree;
	CStringArray olStrArray;
	int ilArray = ExtractItemList(opUrnoList,&olStrArray, '|');

	for (int i = 0; i < ilArray; i++)
	{
		prlStfData = ogStfData.GetStfByUrno(atol(olStrArray[i]));
		if (prlStfData != NULL)
		{
			//f�r Ma-K�rzel
			CString olCode = prlStfData->Perc;
			if(olCode == "")
			{
				olCode = "???";
			}
			olStaffListCode += olCode + CString(" - ");

			//f�r V.Nachn
			CString olVName = prlStfData->Finm;
			CString olNName = prlStfData->Lanm;
			if(olNName.GetLength() > 8)
			{
				olVName.Format("%s.%s. - ",olVName.Left(1),olNName.Left(8));
			}
			else
			{
				olVName.Format("%s.%s - ",olVName.Left(1),olNName.Left(8));
			}
			olStaffListFLName += olVName;

			// free configurated staff name
			olStaffFree += CBasicData::GetFormatedEmployeeName(4, prlStfData, "", "",pomNameConfigDlg);
			olStaffFree += " - ";
		}
		else
		{
			//f�r Unbekannten Mitarbeiter
			blRet = false;
		}
	}
	if (ilArray > 0)
	{
		olStaffListFLName = olStaffListFLName.Left(olStaffListFLName.GetLength()-3);
		olStaffListCode = olStaffListCode.Left(olStaffListCode.GetLength()-3);
		olStaffFree = olStaffFree.Left(olStaffFree.GetLength()-3);
	}

	*popStaffListFLName = olStaffListFLName;
	*popStaffListCode   = olStaffListCode;
	*popStaffFree		= olStaffFree;
	return blRet;
	CCS_CATCH_ALL;
	return false;
}
  
//**********************************************************************************
// 
//**********************************************************************************
  
void ShiftRoster_View::OnBInfo() 
{
	CCS_TRY;
	int ilSelItem = m_LB_GPLList.GetCurSel();
	if (ilSelItem != LB_ERR)
	{
		CString olInfoText,olTmpTxT,olGPLsplu;
		CString olGPLurno = "0";
		m_LB_GPLList.GetText(ilSelItem,olGPLurno);

		if(olGPLurno.GetLength() > (olGPLurno.Find("URNO=")+5))
		{
			olGPLurno = olGPLurno.Mid(olGPLurno.Find("URNO=")+5);
		}

		olInfoText = LoadStg(SHIFT_INFO_1); //"Basic Shift Roster:"
		ogBCD.GetField("GPL", "URNO", olGPLurno, "GSNM", olTmpTxT);
		olInfoText += CString("\t\t") + olTmpTxT + (CString)"\n";

		olTmpTxT = "";
		olInfoText += LoadStg(SHIFT_INFO_2); //"Assigned  to Shift Plan:"
		ogBCD.GetField("GPL", "URNO", olGPLurno, "SPLU", olGPLsplu);
		ogBCD.GetField("SPL", "URNO", olGPLsplu, "SNAM", olTmpTxT);
		olInfoText += CString("\t") + olTmpTxT + (CString)"\n\n";


		olInfoText += LoadStg(IDS_STRING1740); //"Created by:"
		ogBCD.GetField("GPL", "URNO", olGPLurno, "USEC", olTmpTxT);
		olInfoText += CString("\t\t") + olTmpTxT + "\n";

		olInfoText += LoadStg(IDS_STRING1823); //"Created on:"
		ogBCD.GetField("GPL", "URNO", olGPLurno, "CDAT", olTmpTxT);
		if(olTmpTxT.GetLength() >= 12)
		{
			olTmpTxT.Format("%s.%s.%s / %s:%s",olTmpTxT.Mid(6,2),olTmpTxT.Mid(4,2),olTmpTxT.Mid(0,4),olTmpTxT.Mid(8,2),olTmpTxT.Mid(10,2));
		}
		olInfoText += CString("\t\t") + olTmpTxT + "\n\n";

		
		olInfoText += LoadStg(IDS_STRING431); //"Updated by:"
		ogBCD.GetField("GPL", "URNO", olGPLurno, "USEU", olTmpTxT);
		olInfoText += CString("\t\t") + olTmpTxT + "\n";

		olInfoText += LoadStg(IDS_STRING433); //"Updated on:"
		ogBCD.GetField("GPL", "URNO", olGPLurno, "LSTU", olTmpTxT);
		if(olTmpTxT.GetLength() >= 12)
		{
			olTmpTxT.Format("%s.%s.%s / %s:%s",olTmpTxT.Mid(6,2),olTmpTxT.Mid(4,2),olTmpTxT.Mid(0,4),olTmpTxT.Mid(8,2),olTmpTxT.Mid(10,2));
		}
		olInfoText += CString("\t\t") + olTmpTxT + "\n\n";

		MessageBox (olInfoText, LoadStg(IDS_INFO), MB_ICONINFORMATION);
	}
	else
	{
		Beep(440,70);
		MessageBox(LoadStg(ST_DATENSATZ_WAEHLEN), LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// 
//**********************************************************************************
void ShiftRoster_View::OnBCheck() 
{
	CCS_TRY;
	// wir legen eine lokale Instanz der CedaShiftCheck-Klasse an, um mit dem evtl. angelegter DutyRoster-CedaShiftCheck-Klasse nicht in Konflikt zu kommen
	CedaShiftCheck olCheck;
	olCheck.Initialize();

	// alle Fehler sollen in der Warnunungsliste gespeichert werden
	olCheck.imOutput = OUT_WARNING_AS_WARNING | OUT_ERROR_AS_WARNING;	// Fehler sollen nur als Warnungen ausgegeben werden

	AfxGetApp()->DoWaitCursor(1);
	olCheck.ShiftRosterCheckIt(rmCodeNumberData.NrOfWeeks,omGSPTableData);
	RemoveWaitCursor();
	pomMe->GetParentFrame()->GetMessageBar()->SetWindowText(LoadStg(IDS_STRING63498));

	if(olCheck.IsWarning())
	{
		CString olHead;
		olHead = LoadStg(IDS_STRING1697);

		CStringArray* polWarningsArray = olCheck.GetWarningList();
		if(!polWarningsArray) return;

		CString olText;

		for(int ilCount=0; ilCount<polWarningsArray->GetSize(); ilCount++)
		{
			  olText += polWarningsArray->GetAt(ilCount) + "\n";
		}

		::MessageBox(NULL,olText,olHead,MB_OK);
		delete polWarningsArray;
	}
	else 
	{
		MessageBox(LoadStg(IDS_STRING1711),LoadStg(IDS_STRING1697),MB_OK);
	}
	CCS_CATCH_ALL;
}
  
//**********************************************************************************
// 
//**********************************************************************************
void ShiftRoster_View::OnBNew() 
{
	CCS_TRY;
	CString olGPLName;
	CTime olVafr = -1;
	CTime olVato = -1;

	ShiftNewGSPDlg *pomDlg = new ShiftNewGSPDlg (&olGPLName, &olVafr, &olVato, "NEW", this);
	if(pomDlg->DoModal() == IDOK)
	{
		pomRecord = new RecordSet(ogBCD.GetFieldCount("GPL"));
		int index = ogBCD.GetFieldIndex("GPL","GSNM");
		if(index < 0) goto ONBNEW_ERROR;
		pomRecord->Values[index] = olGPLName;
		CTime olTime = CTime::GetCurrentTime();
		index = ogBCD.GetFieldIndex("GPL","CDAT");
		if(index < 0) goto ONBNEW_ERROR;
		pomRecord->Values[index] = olTime.Format("%Y%m%d%H%M00");
		index = ogBCD.GetFieldIndex("GPL","USEC");
		if(index < 0) goto ONBNEW_ERROR;
		pomRecord->Values[index] = pcgUser;
		index = ogBCD.GetFieldIndex("GPL","PERI");
		if(index < 0) goto ONBNEW_ERROR;
		pomRecord->Values[index] = "0";
		index = ogBCD.GetFieldIndex("GPL","VAFR");
		if(index < 0) goto ONBNEW_ERROR;
		pomRecord->Values[index] = olVafr.Format("%Y%m%d000000");
		index = ogBCD.GetFieldIndex("GPL","VATO");
		if(index < 0) goto ONBNEW_ERROR;
		pomRecord->Values[index] = olVato.Format("%Y%m%d000000");

		ogBCD.InsertRecord("GPL",*pomRecord,true);
		InsertIn_LB_GPL(pomRecord);
		delete pomRecord;
	}
	delete pomDlg;
	return;
ONBNEW_ERROR:
	if(pomDlg != 0)
	{
		if(pomRecord != 0)
		{
			delete pomRecord;
		}
		delete pomDlg;
	}
	CString olErrorTxt;
	olErrorTxt.Format("GPLTAB: %s %d\n%s",LoadStg(ST_READERR), ogBCD.imLastReturnCode, ogBCD.omLastErrorMessage);
	MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);

	return;
	CCS_CATCH_ALL;
}
  
//**********************************************************************************
// 
//**********************************************************************************
  
void ShiftRoster_View::OnBCopy() 
{
	CCS_TRY;
	AfxGetApp()->DoWaitCursor(1);

	int ilSelItem = m_LB_GPLList.GetCurSel();
	if (ilSelItem != LB_ERR)
	{
		CString olOldGPLurno = "0";
		CString olGPLName;
		m_LB_GPLList.GetText(ilSelItem,olOldGPLurno);
		if(olOldGPLurno.GetLength() > (olOldGPLurno.Find("URNO=")+5))
		{
			olOldGPLurno = olOldGPLurno.Mid(olOldGPLurno.Find("URNO=")+5);
		}

		int ilMBoxReturn = IDOK;
		if (bmIsChangedGPL == true && olOldGPLurno == omSelGPLurno)
		{
			ilMBoxReturn = MessageBox(LoadStg(SHIFT_SAVE_GPL),LoadStg(ST_FRAGE),(MB_ICONQUESTION | MB_OKCANCEL | MB_DEFBUTTON1));
			if (ilMBoxReturn == IDOK)
			{
				SaveGPL();
				if (bmIsChangedGPL == true)
				{
					ilMBoxReturn = IDCANCEL;
				}
			}
		}
		if (ilMBoxReturn != IDCANCEL)
		{
			CTime olVafr = -1;
			CTime olVato = -1;

			CString olGPLperi,olGPLvafr,olGPLvato,olGPLcotu,olGPLorgu;
			ogBCD.GetField("GPL", "URNO", olOldGPLurno, "VAFR", olGPLvafr);
			ogBCD.GetField("GPL", "URNO", olOldGPLurno, "VATO", olGPLvato);

			if (olGPLvafr.GetLength() >= 8)
			{
				olGPLvafr = olGPLvafr.Mid(6,2) + olGPLvafr.Mid(4,2) + olGPLvafr.Mid(0,4);
			}
			if (olGPLvato.GetLength() >= 8)
			{
				olGPLvato = olGPLvato.Mid(6,2) + olGPLvato.Mid(4,2) + olGPLvato.Mid(0,4);
			}

			olVafr = DateHourStringToDate(olGPLvafr,CString("00:00"));
			olVato = DateHourStringToDate(olGPLvato,CString("00:00"));

			ShiftNewGSPDlg *pomDlg = new ShiftNewGSPDlg (&olGPLName, &olVafr, &olVato, "COPY", this);
			if (pomDlg->DoModal() == IDOK)
			{
				// WarteDialog einblenden.
				CWaitDlg olWaitDlg(this);
				CString olTmp = pomDlg->GetReplacementFunctionUrnoOld();
				//-- Load old GPL and Save as new
				CTime olTime = CTime::GetCurrentTime();
				RecordSet *polNewGPLRecord = new RecordSet(ogBCD.GetFieldCount("GPL"));

				polNewGPLRecord->Values[ogBCD.GetFieldIndex("GPL","GSNM")] = olGPLName;
				polNewGPLRecord->Values[ogBCD.GetFieldIndex("GPL","CDAT")] = olTime.Format("%Y%m%d%H%M00");
				polNewGPLRecord->Values[ogBCD.GetFieldIndex("GPL","USEC")] = pcgUser;

				ogBCD.GetField("GPL", "URNO", olOldGPLurno, "PERI", olGPLperi);
				ogBCD.GetField("GPL", "URNO", olOldGPLurno, "COTU", olGPLcotu);
				ogBCD.GetField("GPL", "URNO", olOldGPLurno, "ORGU", olGPLorgu);

				polNewGPLRecord->Values[ogBCD.GetFieldIndex("GPL","PERI")] = olGPLperi;
				polNewGPLRecord->Values[ogBCD.GetFieldIndex("GPL","VAFR")] = olVafr.Format("%Y%m%d000000");
				polNewGPLRecord->Values[ogBCD.GetFieldIndex("GPL","VATO")] = olVato.Format("%Y%m%d000000");
				polNewGPLRecord->Values[ogBCD.GetFieldIndex("GPL","COTU")] = olGPLcotu;
				polNewGPLRecord->Values[ogBCD.GetFieldIndex("GPL","ORGU")] = olGPLorgu;

				ogBCD.InsertRecord("GPL",*polNewGPLRecord,true);
				InsertIn_LB_GPL(polNewGPLRecord);

				//-- Load old GPL and Save with the new GPL-Urno
				CString olTmpTxt;
				CString olWhere;

				RecordSet *polOldGSPRecord = new RecordSet(ogBCD.GetFieldCount("GSP"));
				for (int i = 0; i < ogBCD.GetDataCount("GSP"); i++)
				{
					ogBCD.GetRecord("GSP",i, *polOldGSPRecord);
					if(polOldGSPRecord->Values[imGSPgpluIdx] == olOldGPLurno)
					{
						RecordSet *polNewGSPRecord = new RecordSet(ogBCD.GetFieldCount("GSP"));
						polNewGSPRecord->Values[imGSPgpluIdx] = polNewGPLRecord->Values[ogBCD.GetFieldIndex("GPL","URNO")];
						polNewGSPRecord->Values[imGSPweekIdx] = polOldGSPRecord->Values[imGSPweekIdx];
						polNewGSPRecord->Values[imGSPnstfIdx] = polOldGSPRecord->Values[imGSPnstfIdx];
						if (imGSPwgpcIdx >= 0)
						{
							polNewGSPRecord->Values[imGSPwgpcIdx] = polOldGSPRecord->Values[imGSPwgpcIdx];
						}

						polNewGSPRecord->Values[imGSPstfuIdx] = "";

						// erste Schichten
						polNewGSPRecord->Values[imGSPbumoIdx] = polOldGSPRecord->Values[imGSPbumoIdx];
						polNewGSPRecord->Values[imGSPbutuIdx] = polOldGSPRecord->Values[imGSPbutuIdx];
						polNewGSPRecord->Values[imGSPbuweIdx] = polOldGSPRecord->Values[imGSPbuweIdx];
						polNewGSPRecord->Values[imGSPbuthIdx] = polOldGSPRecord->Values[imGSPbuthIdx];
						polNewGSPRecord->Values[imGSPbufrIdx] = polOldGSPRecord->Values[imGSPbufrIdx];
						polNewGSPRecord->Values[imGSPbusaIdx] = polOldGSPRecord->Values[imGSPbusaIdx];
						polNewGSPRecord->Values[imGSPbusuIdx] = polOldGSPRecord->Values[imGSPbusuIdx];

						// die zweiten Schichten
						polNewGSPRecord->Values[imGSPsumoIdx] = polOldGSPRecord->Values[imGSPsumoIdx];
						polNewGSPRecord->Values[imGSPsutuIdx] = polOldGSPRecord->Values[imGSPsutuIdx];
						polNewGSPRecord->Values[imGSPsuweIdx] = polOldGSPRecord->Values[imGSPsuweIdx];
						polNewGSPRecord->Values[imGSPsuthIdx] = polOldGSPRecord->Values[imGSPsuthIdx];
						polNewGSPRecord->Values[imGSPsufrIdx] = polOldGSPRecord->Values[imGSPsufrIdx];
						polNewGSPRecord->Values[imGSPsusaIdx] = polOldGSPRecord->Values[imGSPsusaIdx];
						polNewGSPRecord->Values[imGSPsusuIdx] = polOldGSPRecord->Values[imGSPsusuIdx];

						CString olNewPfcu = pomDlg->GetReplacementFunctionUrnoNew();
						CString olOldPfcu = pomDlg->GetReplacementFunctionUrnoOld();
						if (olNewPfcu.GetLength() && olOldPfcu != LoadStg(IDS_STRING63458))
						{
							if (olOldPfcu.GetLength())
							{
								// --- Genau die alte Funktion durch die neue ersetzen
								// die ersten Funktionen
								if (polOldGSPRecord->Values[imGSPp1moIdx] == olOldPfcu) polNewGSPRecord->Values[imGSPp1moIdx] = olNewPfcu;
								else polNewGSPRecord->Values[imGSPp1moIdx] = polOldGSPRecord->Values[imGSPp1moIdx];

								if (polOldGSPRecord->Values[imGSPp1tuIdx] == olOldPfcu) polNewGSPRecord->Values[imGSPp1tuIdx] = olNewPfcu;
								else polNewGSPRecord->Values[imGSPp1tuIdx] = polOldGSPRecord->Values[imGSPp1tuIdx];

								if (polOldGSPRecord->Values[imGSPp1weIdx] == olOldPfcu) polNewGSPRecord->Values[imGSPp1weIdx] = olNewPfcu;
								else polNewGSPRecord->Values[imGSPp1weIdx] = polOldGSPRecord->Values[imGSPp1weIdx];

								if (polOldGSPRecord->Values[imGSPp1thIdx] == olOldPfcu) polNewGSPRecord->Values[imGSPp1thIdx] = olNewPfcu;
								else polNewGSPRecord->Values[imGSPp1thIdx] = polOldGSPRecord->Values[imGSPp1thIdx];

								if (polOldGSPRecord->Values[imGSPp1frIdx] == olOldPfcu) polNewGSPRecord->Values[imGSPp1frIdx] = olNewPfcu;
								else polNewGSPRecord->Values[imGSPp1frIdx] = polOldGSPRecord->Values[imGSPp1frIdx];

								if (polOldGSPRecord->Values[imGSPp1saIdx] == olOldPfcu) polNewGSPRecord->Values[imGSPp1saIdx] = olNewPfcu;
								else polNewGSPRecord->Values[imGSPp1saIdx] = polOldGSPRecord->Values[imGSPp1saIdx];

								if (polOldGSPRecord->Values[imGSPp1suIdx] == olOldPfcu) polNewGSPRecord->Values[imGSPp1suIdx] = olNewPfcu;
								else polNewGSPRecord->Values[imGSPp1suIdx] = polOldGSPRecord->Values[imGSPp1suIdx];

								// die zweiten Funktionen
								if (polOldGSPRecord->Values[imGSPp2moIdx] == olOldPfcu) polNewGSPRecord->Values[imGSPp2moIdx] = olNewPfcu; 
								else polNewGSPRecord->Values[imGSPp2moIdx] = polOldGSPRecord->Values[imGSPp2moIdx];

								if (polOldGSPRecord->Values[imGSPp2tuIdx] == olOldPfcu) polNewGSPRecord->Values[imGSPp2tuIdx] = olNewPfcu; 
								else polNewGSPRecord->Values[imGSPp2tuIdx] = polOldGSPRecord->Values[imGSPp2tuIdx];

								if (polOldGSPRecord->Values[imGSPp2weIdx] == olOldPfcu) polNewGSPRecord->Values[imGSPp2weIdx] = olNewPfcu; 
								else polNewGSPRecord->Values[imGSPp2weIdx] = polOldGSPRecord->Values[imGSPp2weIdx];

								if (polOldGSPRecord->Values[imGSPp2thIdx] == olOldPfcu) polNewGSPRecord->Values[imGSPp2thIdx] = olNewPfcu; 
								else polNewGSPRecord->Values[imGSPp2thIdx] = polOldGSPRecord->Values[imGSPp2thIdx];

								if (polOldGSPRecord->Values[imGSPp2frIdx] == olOldPfcu) polNewGSPRecord->Values[imGSPp2frIdx] = olNewPfcu; 
								else polNewGSPRecord->Values[imGSPp2frIdx] = polOldGSPRecord->Values[imGSPp2frIdx];

								if (polOldGSPRecord->Values[imGSPp2saIdx] == olOldPfcu) polNewGSPRecord->Values[imGSPp2saIdx] = olNewPfcu; 
								else polNewGSPRecord->Values[imGSPp2saIdx] = polOldGSPRecord->Values[imGSPp2saIdx];

								if (polOldGSPRecord->Values[imGSPp2suIdx] == olOldPfcu) polNewGSPRecord->Values[imGSPp2suIdx] = olNewPfcu; 
								else polNewGSPRecord->Values[imGSPp2suIdx] = polOldGSPRecord->Values[imGSPp2suIdx];
							}
							else
							{
								// --- alle alten Funktionen durch die neue ersetzen
								// die ersten Funktionen (nur wenn Schicht vorhanden)
								if (polNewGSPRecord->Values[imGSPbumoIdx].GetLength()) polNewGSPRecord->Values[imGSPp1moIdx] = olNewPfcu;
								if (polNewGSPRecord->Values[imGSPbutuIdx].GetLength()) polNewGSPRecord->Values[imGSPp1tuIdx] = olNewPfcu;
								if (polNewGSPRecord->Values[imGSPbuweIdx].GetLength()) polNewGSPRecord->Values[imGSPp1weIdx] = olNewPfcu;
								if (polNewGSPRecord->Values[imGSPbuthIdx].GetLength()) polNewGSPRecord->Values[imGSPp1thIdx] = olNewPfcu;
								if (polNewGSPRecord->Values[imGSPbufrIdx].GetLength()) polNewGSPRecord->Values[imGSPp1frIdx] = olNewPfcu;
								if (polNewGSPRecord->Values[imGSPbusaIdx].GetLength()) polNewGSPRecord->Values[imGSPp1saIdx] = olNewPfcu;
								if (polNewGSPRecord->Values[imGSPbusuIdx].GetLength()) polNewGSPRecord->Values[imGSPp1suIdx] = olNewPfcu;
								// die zweiten Funktionen (nur wenn Schicht vorhanden)
								if (polNewGSPRecord->Values[imGSPsumoIdx].GetLength()) polNewGSPRecord->Values[imGSPp2moIdx] = olNewPfcu; 
								if (polNewGSPRecord->Values[imGSPsutuIdx].GetLength()) polNewGSPRecord->Values[imGSPp2tuIdx] = olNewPfcu; 
								if (polNewGSPRecord->Values[imGSPsuweIdx].GetLength()) polNewGSPRecord->Values[imGSPp2weIdx] = olNewPfcu; 
								if (polNewGSPRecord->Values[imGSPsuthIdx].GetLength()) polNewGSPRecord->Values[imGSPp2thIdx] = olNewPfcu; 
								if (polNewGSPRecord->Values[imGSPsufrIdx].GetLength()) polNewGSPRecord->Values[imGSPp2frIdx] = olNewPfcu; 
								if (polNewGSPRecord->Values[imGSPsusaIdx].GetLength()) polNewGSPRecord->Values[imGSPp2saIdx] = olNewPfcu; 
								if (polNewGSPRecord->Values[imGSPsusuIdx].GetLength()) polNewGSPRecord->Values[imGSPp2suIdx] = olNewPfcu; 
							}
						}
						else
						{
							// --- einfach nur 1 zu 1 kopieren
							// die ersten Funktionen
							polNewGSPRecord->Values[imGSPp1moIdx] = polOldGSPRecord->Values[imGSPp1moIdx];
							polNewGSPRecord->Values[imGSPp1tuIdx] = polOldGSPRecord->Values[imGSPp1tuIdx];
							polNewGSPRecord->Values[imGSPp1weIdx] = polOldGSPRecord->Values[imGSPp1weIdx];
							polNewGSPRecord->Values[imGSPp1thIdx] = polOldGSPRecord->Values[imGSPp1thIdx];
							polNewGSPRecord->Values[imGSPp1frIdx] = polOldGSPRecord->Values[imGSPp1frIdx];
							polNewGSPRecord->Values[imGSPp1saIdx] = polOldGSPRecord->Values[imGSPp1saIdx];
							polNewGSPRecord->Values[imGSPp1suIdx] = polOldGSPRecord->Values[imGSPp1suIdx];
							// die zweiten Funktionen
							polNewGSPRecord->Values[imGSPp2moIdx] = polOldGSPRecord->Values[imGSPp2moIdx];
							polNewGSPRecord->Values[imGSPp2tuIdx] = polOldGSPRecord->Values[imGSPp2tuIdx];
							polNewGSPRecord->Values[imGSPp2weIdx] = polOldGSPRecord->Values[imGSPp2weIdx];
							polNewGSPRecord->Values[imGSPp2thIdx] = polOldGSPRecord->Values[imGSPp2thIdx];
							polNewGSPRecord->Values[imGSPp2frIdx] = polOldGSPRecord->Values[imGSPp2frIdx];
							polNewGSPRecord->Values[imGSPp2saIdx] = polOldGSPRecord->Values[imGSPp2saIdx];
							polNewGSPRecord->Values[imGSPp2suIdx] = polOldGSPRecord->Values[imGSPp2suIdx];
						}

						ogBCD.InsertRecord("GSP",*polNewGSPRecord,true);
						delete polNewGSPRecord;
					}
				}
				delete polNewGPLRecord;
				delete polOldGSPRecord;
			}
			delete pomDlg;
		}
	}
	else
	{
		Beep(440,70);
		MessageBox(LoadStg(ST_DATENSATZ_WAEHLEN), LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
	}
	RemoveWaitCursor();
	CCS_CATCH_ALL;
}

//**********************************************************************************
// 
//**********************************************************************************

void ShiftRoster_View::OnBDelete() 
{
	CCS_TRY;
	AfxGetApp()->DoWaitCursor(1);

	int ilSelItem = m_LB_GPLList.GetCurSel();

	if (ilSelItem != LB_ERR)
	{
		CString olGPLurno = "0";
		m_LB_GPLList.GetText(ilSelItem,olGPLurno);
		if (olGPLurno.GetLength() > (olGPLurno.Find("URNO=")+5))
		{
			olGPLurno = olGPLurno.Mid(olGPLurno.Find("URNO=")+5);
		}

		CString olGPLsplu;
		ogBCD.GetField ("GPL", "URNO", olGPLurno, "SPLU", olGPLsplu);

		CString olSPLsnam;
		ogBCD.GetField ("SPL", "URNO", olGPLsplu, "SNAM", olSPLsnam);
		int ilMBoxReturn = IDOK;
		if (omActualSPLurnoByGPL == omSelSPLurno && bmIsChangedGPL == true && olGPLsplu == omSelSPLurno )
		{
			ilMBoxReturn = MessageBox(LoadStg(SHIFT_SAVE_GPL),LoadStg(ST_FRAGE),(MB_ICONQUESTION | MB_OKCANCEL | MB_DEFBUTTON1));
			if(ilMBoxReturn == IDOK)
			{
				SaveGPL();
				if(bmIsChangedGPL == true)
					ilMBoxReturn = IDCANCEL;
			}
		}
		if(ilMBoxReturn != IDCANCEL)
		{

			int ilMBox = IDNO;

			if(olSPLsnam == "")
			{
				ilMBox = MessageBox(LoadStg(ST_DATENSATZ_LOESCHEN),LoadStg(ST_FRAGE),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2));
			}
			else
			{
				CString olMsg;
				//Dieser Grundschichtplan ist mit dem Schichtplan %s verkn�pft.\n*INTXT*
				olMsg.Format (LPCTSTR(LoadStg(SHIFT_ISAKTIV_B)), olSPLsnam);
				ilMBox = MessageBox (olMsg + CString("\n") + LoadStg(ST_DATENSATZ_LOESCHEN),LoadStg(ST_FRAGE),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2));
			}
			if(ilMBox == IDYES)
			{
				ogBCD.DeleteRecord("GPL","URNO",olGPLurno,true);
				m_LB_GPLList.DeleteString(ilSelItem);
				
				//-- Delete all GSP records with GPL Urno --
				int ilGSPgpluIdx = ogBCD.GetFieldIndex("GSP","GPLU");
				RecordSet *polGSPRecord = new RecordSet(ogBCD.GetFieldCount("GSP"));
				for(int i = 0; i < ogBCD.GetDataCount("GSP"); i++)
				{
					ogBCD.GetRecord("GSP",i, *polGSPRecord);
					if(polGSPRecord->Values[ilGSPgpluIdx] == olGPLurno)
					{
						CString olGSPurno = polGSPRecord->Values[ogBCD.GetFieldIndex("GSP","URNO")];
						ogBCD.DeleteRecord("GSP","URNO",olGSPurno,true);
						i--;
					}
				}
				delete polGSPRecord;
				
				CString olCaptioGPLurno = "0";
				CString olTmpTxt;
				m_E_GPL_Caption.GetWindowText(olTmpTxt);
				if(olTmpTxt.GetLength() > (olTmpTxt.Find("URNO=")+5))
					olCaptioGPLurno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
				
				if(olCaptioGPLurno == olGPLurno)
				{
					omActualSPLurnoByGPL = "";
				}
				GetShiftRequirementData(SHIFTPLAN);
				
				if(olCaptioGPLurno == olGPLurno)
				{
					m_E_GPL_Caption.SetInitText("");
					m_SBC_Periode.SetPos(0);
					im_SBC_Periode = 0;
					m_E_GPL_Peri.SetInitText("");
					m_E_GPL_Vato.SetInitText("");
					m_E_GPL_Vafr.SetInitText("");
					m_E_GPL_StPt.SetInitText("");
					
					this->imStPtWeek = 0;
					
					omSelGPLurno = "";
					
					GetShiftRosterData("");
				}
			}
		}
	}
	else
	{
		Beep(440,70);
		MessageBox(LoadStg(ST_DATENSATZ_WAEHLEN), LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
	}
	RemoveWaitCursor();
	CCS_CATCH_ALL;
}


//**********************************************************************************
// Testet ob der Konsist Dialog sichtbar ist.
//**********************************************************************************

bool ShiftRoster_View::IsKonsistDlgActive()
{
	if (pomShiftKonsistDlg == NULL)
		return false;
	
	if (pomShiftKonsistDlg->IsWindowVisible() == NULL)
		return false;
	
	return true;
}


//**********************************************************************************
// 
//**********************************************************************************

void ShiftRoster_View::OnBKonsist() 
{
	CCS_TRY;
	
	if (!IsKonsistDlgActive())
	{
		pomShiftKonsistDlg = new ShiftKonsistDlg(this);
		pomShiftKonsistDlg->SetValues();
	}
	else
	{
		pomShiftKonsistDlg->ShowWindow(SW_SHOWNORMAL);
		pomShiftKonsistDlg->SetValues();
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// activate/disactivate GPL
//**********************************************************************************

void ShiftRoster_View::OnBAssignGPL() 
{
	CCS_TRY;
	int ilSelGPLItem = m_LB_GPLList.GetCurSel();

	if (ilSelGPLItem != LB_ERR)
	{
		int ilSelSPLItem = pomComboBoxShiftplan->GetCurSel();
		CString olSPLTxt;

		if (ilSelSPLItem != CB_ERR)
		{
			pomComboBoxShiftplan->GetLBText (ilSelSPLItem,olSPLTxt);
		}

		if (ilSelSPLItem != CB_ERR && olSPLTxt != "")
		{
			CString olGPLTxt;
			CString olActivate;
			CString olGPLurno = "0";
			CString olSPLurno = "0";

			m_LB_GPLList.GetText(ilSelGPLItem,olGPLTxt);
			
			if (olGPLTxt.GetLength() > (olGPLTxt.Find("URNO=")+5))
				olGPLurno = olGPLTxt.Mid(olGPLTxt.Find("URNO=")+5);

			olActivate = olGPLTxt.Left(1);

			if (olSPLTxt.GetLength() > (olSPLTxt.Find("URNO=")+5))
				olSPLurno = olSPLTxt.Mid(olSPLTxt.Find("URNO=")+5);

			if (olActivate == omAktiv)
			{
				int ilMBoxReturn = IDOK;
				if (omActualSPLurnoByGPL == olSPLurno && omSelGPLurno != olGPLurno && bmIsChangedGPL == true)
				{
					ilMBoxReturn = MessageBox(LoadStg(SHIFT_SAVE_GPL),LoadStg(ST_FRAGE),(MB_ICONQUESTION | MB_OKCANCEL | MB_DEFBUTTON1));
					if(ilMBoxReturn == IDOK)
					{
						SaveGPL();
						if(bmIsChangedGPL == true)
							ilMBoxReturn = IDCANCEL;
					}
				}
				if (ilMBoxReturn != IDCANCEL)
				{
					//-- Deaktivate --
					if(olGPLTxt.GetLength() > 1)
						olGPLTxt = " " + olGPLTxt.Mid(1);
					
					m_LB_GPLList.DeleteString(ilSelGPLItem);
					m_LB_GPLList.InsertString(ilSelGPLItem,olGPLTxt);
					m_B_Assign.SetWindowText(LoadStg(SHIFT_B_AKTIVATE));
					m_LB_GPLList.SetCurSel(ilSelGPLItem);
					
					ogBCD.SetField("GPL","URNO",olGPLurno,"SPLU","",true);
					if (olSPLurno == omActualSPLurnoByGPL)
						omActualSPLurnoByGPL = "";
					GetShiftRequirementData(SHIFTPLAN);
				}
			}
			else
			{
				CString olTmpUrno;
				ogBCD.GetField("GPL", "URNO", olGPLurno, "SPLU", olTmpUrno );
				if (olTmpUrno == "")
				{
					int ilMBoxReturn = IDOK;
					if((omActualSPLurnoByGPL == olSPLurno || omSelGPLurno == olGPLurno) && bmIsChangedGPL == true)
					{
						ilMBoxReturn = MessageBox(LoadStg(SHIFT_SAVE_GPL),LoadStg(ST_FRAGE),(MB_ICONQUESTION | MB_OKCANCEL | MB_DEFBUTTON1));
						if(ilMBoxReturn == IDOK)
						{
							SaveGPL();
							if(bmIsChangedGPL == true)
								ilMBoxReturn = IDCANCEL;
						}
					}
					if(ilMBoxReturn != IDCANCEL)
					{
						//-- Aktivate --
						if(olGPLTxt.GetLength() > 1)
							olGPLTxt = omAktiv + olGPLTxt.Mid(1);
						
						m_LB_GPLList.DeleteString(ilSelGPLItem);
						m_LB_GPLList.InsertString(ilSelGPLItem,olGPLTxt);
						m_B_Assign.SetWindowText(LoadStg(SHIFT_B_DEAKTIVATE));
						m_LB_GPLList.SetCurSel(ilSelGPLItem);
						
						ogBCD.SetField("GPL","URNO",olGPLurno,"SPLU",olSPLurno,true);
						if(omSelGPLurno == olGPLurno)
							omActualSPLurnoByGPL = olSPLurno;
						
						GetShiftRequirementData(SHIFTPLAN);
					}
				}
				else
				{
					CString olSnam;
					ogBCD.GetField("SPL", "URNO", olTmpUrno, "SNAM", olSnam);
					Beep(440,70);
					CString olMsg;
					//Dieser Grundschichtplan ist mit dem Schichtplan %s verkn�pft.\n*INTXT*
					olMsg.Format (LPCTSTR(LoadStg(SHIFT_ISAKTIV_B)), olSnam);
					MessageBox (olMsg, LoadStg(ST_HINWEIS), MB_ICONEXCLAMATION);
				}
			}
			if(imViewShowGPL == VIEW_NOTALLGPL)
			{
				//Build_LB_GPL();
				OnChangeEGplFilter();
			}
		}
		else
		{
			Beep(440,70);
			MessageBox(LoadStg(IDS_STRING63480), LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);	//Bitte w�hlen Sie einen Schichtplan aus.*INTXT*
		}
	}
	else
	{
		Beep(440,70);
		MessageBox(LoadStg(IDS_STRING63481), LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);		// Bitte w�hlen Sie einen Grundschichtplan aus.*INTXT*
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// 
//**********************************************************************************

void ShiftRoster_View::OnBBonus() 
{
	CCS_TRY;
	CString olNewGRNurnoList;
	CString olOldGRNurnoList;
	CString olTmpTxt;
	CString olDBOurno;
	CString olSPLTxt;

	int ilSelItem = pomComboBoxShiftplan->GetCurSel();

	if (ilSelItem != CB_ERR)
	{
		pomComboBoxShiftplan->GetLBText(ilSelItem,olSPLTxt);
	}

	if (ilSelItem != CB_ERR && olSPLTxt != "")
	{
		for (int i = 0; i < m_CB_Bonus.GetCount(); i++)
		{
			m_CB_Bonus.GetLBText(i,olTmpTxt);
			if (i > 0)
			{
				olOldGRNurnoList += "|";
			}
			if (olTmpTxt.GetLength() > (olTmpTxt.Find("URNO=")+5))
			{
				olOldGRNurnoList += olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
			}
		}
		olNewGRNurnoList = olOldGRNurnoList;

		ShiftAWDlgBonus *pomDlg = new ShiftAWDlgBonus(&olNewGRNurnoList, "", BONUS, true, "", "");
		if (pomDlg->DoModal() == IDOK)
		{
			// Warte Mouse
			AfxGetApp()->DoWaitCursor(1);

			m_CB_Bonus.ResetContent();
			//-- Add new Gruppnames --
			CStringArray olNewUrnoArray;
			int ilUrnos  = ExtractItemList(olNewGRNurnoList,&olNewUrnoArray,'|');

			for(int i=0;i<ilUrnos;i++)
			{
				if(olOldGRNurnoList.Find(olNewUrnoArray[i]) == -1)
				{
					bmIsChangedBonus = true;
					// COLOR
					((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);

					BONUSDATA *polBonus = new BONUSDATA;
					polBonus->DBObgru = olNewUrnoArray[i];
					polBonus->IsChanged = RECORDCHANGED;

					//uhi 27.7.00 Alle Schichten ausgew�hlt
					if(olNewUrnoArray[i] == CString("4711")){
						polBonus->SGRgrpn = CString("*.*"); 
						CString olTmpTxt;
						for(int m=0; m<ogBsdData.omData.GetSize(); m++)
						{
							if(m>0)
							{
								polBonus->BsdList += "|";
								polBonus->BsdUrnoList += "|";
							}
							polBonus->BsdList += (CString)ogBsdData.omData[m].Bsdc;
							olTmpTxt.Format("%d",ogBsdData.omData[m].Urno);
							polBonus->BsdUrnoList += olTmpTxt;
						}
					}
					else
					{
						//CCSPtrArray<SGMDATA> olSgmList;
						SGRDATA *prlSgr = ogSgrData.GetSgrByUrno(atol(polBonus->DBObgru));
						if(prlSgr != NULL)
						{
							polBonus->SGRgrpn = prlSgr->Grpn;
							CCSPtrArray<SGMDATA> olSgmList;
							if(ogSgmData.GetSgmDataByGrnUrno(olSgmList,atol(polBonus->DBObgru)) == true)
							{
								CString olTmpTxt;
								for(int m=0; m<olSgmList.GetSize(); m++)
								{
									BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(atol(olSgmList[m].Uval));
									if(prlBsd != NULL)
									{
										if(m>0)
										{
											polBonus->BsdList += "|";
											polBonus->BsdUrnoList += "|";
										}
										polBonus->BsdList += (CString)prlBsd->Bsdc;
										olTmpTxt.Format("%d",prlBsd->Urno);
										polBonus->BsdUrnoList += olTmpTxt;
									}
								}
							}
							olSgmList.DeleteAll();
						}
					}

					omBonusData.Add(polBonus);
				}
			}
			
			//-- Delete old Gruppnames --
			CStringArray olOldUrnoArray;
			ilUrnos  = ExtractItemList(olOldGRNurnoList,&olOldUrnoArray,'|');
			for(i=0;i<ilUrnos;i++)
			{
				if(olNewGRNurnoList.Find(olOldUrnoArray[i]) == -1)
				{
					bmIsChangedBonus = true;
					// COLOR
					((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);
					
					for(int k=0;k<omBonusData.GetSize();k++)
					{
						if(omBonusData[k].DBObgru == olOldUrnoArray[i])
						{
							omBonusData.DeleteAt(k);
							k=omBonusData.GetSize();
						}
					}
				}
			}
			
			CString olLine;
			for(i = 0; i < omBonusData.GetSize(); i++)
			{
				olLine.Format("%-12s                                                         URNO=%s",omBonusData[i].SGRgrpn,omBonusData[i].DBObgru);
				m_CB_Bonus.AddString(olLine);
			}
			
			OnSelchangeCbBonus();
			DeleteNullFromShiftRequirementData();
			ChangeShiftRequirementBonusData();
			ChangeShiftRequirementGruppData();
			DisplayShiftRequirementData(2);
		}
		delete pomDlg;
		
		RemoveWaitCursor();
	}
	else
	{
		Beep(440,70);
		MessageBox(LoadStg(SHIFT_SELECT_PLAN), LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// scroll-event hitting a scroll-bar or spin-button
//**********************************************************************************

void ShiftRoster_View::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CCS_TRY;



	/*
	nSBCode:
	SB_LINEUP,SB_LINELEFT    0
	SB_LINEDOWN,SB_LINERIGHT 1
	SB_PAGEUP,SB_PAGELEFT    2
	SB_PAGEDOWN,SB_PAGERIGHT 3
	SB_THUMBPOSITION		 4
	SB_THUMBTRACK		     5
	SB_TOP,SB_LEFT           6
	SB_BOTTOM,SB_RIGHT       7
	SB_ENDSCROLL			 8
	*/

	// Funktion der Basisklasse
	CFormView::OnVScroll(nSBCode, nPos, pScrollBar);
	
	// Safety first:
	if (pScrollBar == NULL)
		return;
	
	int ilPosTmp1;
	int ilPosTmp2;
	int ilOldPos;
	int ilRangeMax;
	int ilRangeMin;

	ilPosTmp1 = pScrollBar->GetScrollPos();
	ilOldPos = ilPosTmp1;
	pScrollBar->GetScrollRange(&ilRangeMin,&ilRangeMax);

	//--- ShiftRosterDataScrollBar
	if((void*)pScrollBar == (void*)&m_SB_GSP)
	{
		DeleteSelectGSPTableFields();
		ilPosTmp2 = imGSPLines;
		switch(nSBCode)
		{
		case SB_THUMBPOSITION:
			ilPosTmp1 = nPos;
			break;
		case SB_PAGEUP:
			if((ilPosTmp1 - ilPosTmp2) > ilRangeMin)
			{
				ilPosTmp1 -= ilPosTmp2;
			}
			else
			{
				ilPosTmp1 = ilRangeMin;
			}
			break;
		case SB_PAGEDOWN:
			if((ilPosTmp1 + ilPosTmp2) < ilRangeMax)
			{
				ilPosTmp1 += ilPosTmp2;
			}
			else
			{
				ilPosTmp1 = ilRangeMax;
			}
			break;
		case SB_LINEUP:
			if(ilPosTmp1 > ilRangeMin)
				ilPosTmp1--;
			break;
		case SB_LINEDOWN :
			if(ilPosTmp1 < ilRangeMax)
				ilPosTmp1++;
			break;
		case SB_TOP:
			ilPosTmp1 = ilRangeMin;
			break;
		case SB_BOTTOM:
			ilPosTmp1 = ilRangeMax;
			break;
		}
		if(ilPosTmp1 != ilOldPos)
		{
			pScrollBar->SetScrollPos(ilPosTmp1);
			imActualLine = ilPosTmp1;
			DisplayShiftRosterData(pScrollBar->GetScrollPos());
		}
		pScrollBar->GetParent()->SetFocus();
	} 
	else if((void*)pScrollBar == (void*)&m_SB_NSR)
	{
		//--- ShiftRequirementDataScrollBar
		ilPosTmp2 = 10;
		switch(nSBCode)
		{
		case SB_THUMBPOSITION:
			ilPosTmp1 = nPos;
			break;
		case SB_PAGEUP:
			if((ilPosTmp1 - ilPosTmp2) > ilRangeMin)
			{
				ilPosTmp1 -= ilPosTmp2;
			}
			else
			{
				ilPosTmp1 = ilRangeMin;
			}
			break;
		case SB_PAGEDOWN:
			if((ilPosTmp1 + ilPosTmp2) < ilRangeMax)
			{
				ilPosTmp1 += ilPosTmp2;
			}
			else
			{
				ilPosTmp1 = ilRangeMax;
			}
			break;
		case SB_LINEUP:
			if(ilPosTmp1 > ilRangeMin)
				ilPosTmp1--;
			break;
		case SB_LINEDOWN :
			if(ilPosTmp1 < ilRangeMax)
				ilPosTmp1++;
			break;
		case SB_TOP:
			ilPosTmp1 = ilRangeMin;
			break;
		case SB_BOTTOM:
			ilPosTmp1 = ilRangeMax;
			break;
		}
		if(ilPosTmp1 != ilOldPos)
		{
			pScrollBar->SetScrollPos(ilPosTmp1);
			DisplayShiftRequirementData(1);
		}
	}
	else if((void*)pScrollBar == (void*)&m_SBC_Periode)
	{
		//--- PeriodSpinButtonCtrl
		m_SBC_Periode.SetFocus();
		DeleteSelectGSPTableFields();

		switch (nSBCode)
		{
		case SB_THUMBPOSITION:
			{
				CString olTmp;
				m_E_GPL_Caption.GetWindowText(olTmp);

				if(olTmp != "")
				{
				
				
					if (nPos > im_SBC_Periode)
					{
						// increasing the period
				    	im_SBC_Periode++;
					}
					else
					{
						// decreasing the period
						if(im_SBC_Periode>0)
						{
					 
						//	goto again;
					
							im_SBC_Periode--;
						}
					}

					olTmp.Format("%01d", im_SBC_Periode * 7);

					m_E_GPL_Peri.SetInitText(olTmp);
					m_SBC_Periode.SetPos(im_SBC_Periode);
					
					int ilSize = omGSPTableData.GetSize();
				
					
					
					if (ilSize < im_SBC_Periode)
					{
						AddWeekToGPL();
			
						((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE); // red color
						bmIsChangedGPL = true;
						if (IsCodeDlgActive())
						{
							// Ver�nderungen an Schwebedialog weitergeben
							pomShiftCodeNumberDlg->SetWeeks(im_SBC_Periode);
						}
					}
					else if (ilSize > im_SBC_Periode)
					{
						DeleteWeekfromGPL();
			
						((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);
						bmIsChangedGPL = true;
						if (IsCodeDlgActive())
						{
							// Ver�nderungen an Schwebedialog weitergeben
							pomShiftCodeNumberDlg->SetWeeks(im_SBC_Periode);
						}
					}
				}
				else
				{
					m_SBC_Periode.SetPos(0);
					im_SBC_Periode = 0;
				}
				break;
			}
		}
	}
	
	CCS_CATCH_ALL;
}

//**********************************************************************************
// ???
//**********************************************************************************

void ShiftRoster_View::AddWeekToGPL() 
{
	CCS_TRY;
	CString olTmpTxt;
	
	GSPTABLEDATA *prlGplData = new GSPTABLEDATA;
	
	olTmpTxt.Format("%03d",omGSPTableData.GetSize()+1);
	prlGplData->Week		= olTmpTxt;
	prlGplData->DisplayWeek	= olTmpTxt; //$$
	prlGplData->Number		= "1";
	prlGplData->IsChanged	= RECORDCHANGED;
	
	omGSPTableData.Add(prlGplData);
	
	int ilSize = omGSPTableData.GetSize();
	imGSPRangeMax = ilSize - imGSPLines;
	if(imGSPRangeMax<0) imGSPRangeMax = 0;
	m_SB_GSP.SetScrollRange(imGSPRangeMin,imGSPRangeMax,false);
	m_SB_GSP.SetScrollPos(imGSPRangeMax);
	if(imGSPRangeMax > 0)
	{
		m_SB_GSP.EnableScrollBar(ESB_ENABLE_BOTH);
	}
	else
	{
		m_SB_GSP.EnableScrollBar(ESB_DISABLE_BOTH);
	}
	
	imActualLine = imGSPRangeMax;
	DisplayShiftRosterData(m_SB_GSP.GetScrollPos());
	
	rmCodeNumberData.NrOfWeeks++; // CodeNumber
	rmCodeNumberData.MaSchedule++; // CodeNumber
	if(omSelSPLurno == omActualSPLurnoByGPL)
	{
		rmCodeNumberData.MaScheduleAll++; // CodeNumber
	}
	
	if (IsCodeDlgActive())
	{
		pomShiftCodeNumberDlg->SetValues(rmCodeNumberData);
		pomShiftCodeNumberDlg->SetWeeks(omGSPTableData.GetSize());
		pomShiftCodeNumberDlg->SetDataArray(omGSPTableData);
		pomShiftCodeNumberDlg->ReNewAll();
	}
	
	CCS_CATCH_ALL;
}

//**********************************************************************************
// ???
//**********************************************************************************

void ShiftRoster_View::DeleteWeekfromGPL() 
{
	CCS_TRY;
	CString olTmpTxt;
	int ilSize = omGSPTableData.GetSize();
	int ilOldNumber = atoi(omGSPTableData[ilSize-1].Number);
	
	//-- CodeNumber --
	BSDDATA* prlBsdData;
	int ilHouers = 0,ilStaffs = 0;
	CString olUrno;
	
	for (int ilDay = 0; ilDay < 7; ilDay++)
	{
		olUrno = omGSPTableData[ilSize-1].DayUrno[ilDay];
		
		if(olUrno != "" && olUrno != "0" && ogBsdData.GetBsdByUrno(atol(olUrno)) != NULL)
		{
			rmCodeNumberData.Work--;
			prlBsdData = ogBsdData.GetBsdByUrno(atol(olUrno));
			if(prlBsdData != NULL)
			{
				ilHouers += atoi(prlBsdData->Sdu1);
			}
		}
	}
	ilStaffs = GetItemCount(omGSPTableData[ilSize-1].StaffUrno,'|');
	
	rmCodeNumberData.MaSchedule -= ilOldNumber;
	rmCodeNumberData.MaAssign	-= ilStaffs;
	rmCodeNumberData.HouersGSP  -= ilHouers;

	if(omSelSPLurno == omActualSPLurnoByGPL)
	{
		rmCodeNumberData.MaScheduleAll -=  ilOldNumber;
		rmCodeNumberData.HouersAll -= ilHouers;
		rmCodeNumberData.MaAssignAll -= ilStaffs;
	}
	//-- CodeNumber End --

	//*******************************************************************
	//-- Add in Array to change  the Shift Requirement Data --
	if(omSelSPLurno == omActualSPLurnoByGPL)
	{
		CHANGEGSPDATA *polChangedGSPs;

		for(int ilDay=0; ilDay<7; ilDay++)
		{
			polChangedGSPs = new CHANGEGSPDATA;
			polChangedGSPs->Day			= ilDay + 1;
			polChangedGSPs->OldNumber	= ilOldNumber;
			polChangedGSPs->OldBSDCode	= omGSPTableData[ilSize-1].Day[ilDay];
			polChangedGSPs->OldBSDUrno	= omGSPTableData[ilSize-1].DayUrno[ilDay];
			polChangedGSPs->Old2BSDCode	= omGSPTableData[ilSize-1].Day2[ilDay];
			polChangedGSPs->Old2BSDUrno	= omGSPTableData[ilSize-1].Day2Urno[ilDay];
			polChangedGSPs->OldPfcCode  = omGSPTableData[ilSize-1].PfcCode[ilDay];
			polChangedGSPs->OldPfcUrno  = omGSPTableData[ilSize-1].PfcUrno[ilDay];
			polChangedGSPs->OldPfc2Code = omGSPTableData[ilSize-1].PfcCode2[ilDay];
			polChangedGSPs->OldPfc2Urno = omGSPTableData[ilSize-1].PfcUrno2[ilDay];
			polChangedGSPs->NewNumber	= 0;
			omChangedGSPs.Add(polChangedGSPs);
		}

		ChangeShiftRequirementData(WEEK);
		DeleteNullFromShiftRequirementData();
		ChangeShiftRequirementBonusData();
		ChangeShiftRequirementGruppData();
		DisplayShiftRequirementData(2);
	}
	
	//*******************************************************************
	// omDeletedGSPs wird mit neuer Zeile gef�llt
	// beim sichern wird diese Zeile dann aus der Datenbank gel�scht
	
	GSPTABLEDATA *prlGspData = new GSPTABLEDATA;
	
	prlGspData->Week		= omGSPTableData[ilSize-1].Week;
	prlGspData->DisplayWeek	= omGSPTableData[ilSize-1].DisplayWeek;
	prlGspData->GSPUrno		= omGSPTableData[ilSize-1].GSPUrno;
	prlGspData->Number		= omGSPTableData[ilSize-1].Number;
	prlGspData->StaffUrno	= omGSPTableData[ilSize-1].StaffUrno;
	prlGspData->StaffFLName	= omGSPTableData[ilSize-1].StaffFLName;
	prlGspData->StaffFree	= omGSPTableData[ilSize-1].StaffFree;
	prlGspData->StaffCode	= omGSPTableData[ilSize-1].StaffCode;
	prlGspData->Houers		= omGSPTableData[ilSize-1].Houers;
	prlGspData->IsChanged	= omGSPTableData[ilSize-1].IsChanged;

	for (ilDay = 0; ilDay < 7; ilDay++)
	{
		// Erste Schicht
		prlGspData->Day[ilDay]		= omGSPTableData[ilSize-1].Day[ilDay];
		prlGspData->DayUrno[ilDay]	= omGSPTableData[ilSize-1].DayUrno[ilDay];
		prlGspData->PfcCode[ilDay]	= omGSPTableData[ilSize-1].PfcCode[ilDay];
		prlGspData->PfcUrno[ilDay]	= omGSPTableData[ilSize-1].PfcUrno[ilDay];
		// Zwote Schicht
		prlGspData->Day2[ilDay]		= omGSPTableData[ilSize-1].Day2[ilDay];
		prlGspData->Day2Urno[ilDay]	= omGSPTableData[ilSize-1].Day2Urno[ilDay];
		prlGspData->PfcCode2[ilDay]	= omGSPTableData[ilSize-1].PfcCode2[ilDay];
		prlGspData->PfcUrno2[ilDay]	= omGSPTableData[ilSize-1].PfcUrno2[ilDay];
	}
	
	if(prlGspData->GSPUrno != "" && prlGspData->GSPUrno != "0")
	{
		omDeletedGSPs.Add(prlGspData);
	}
	else
	{
		delete prlGspData;
	}
	
	//*******************************************************************
	
	// L�schen aus der eigendlichen Datenhaltung
	omGSPTableData.DeleteAt(ilSize-1);

	ilSize = omGSPTableData.GetSize();
	imGSPRangeMax = ilSize - imGSPLines;
	if (imGSPRangeMax < 0)
	{
		imGSPRangeMax = 0;
	}
	m_SB_GSP.SetScrollRange(imGSPRangeMin,imGSPRangeMax,false);
	m_SB_GSP.SetScrollPos(imGSPRangeMax);
	if(imGSPRangeMax > 0)
	{
		m_SB_GSP.EnableScrollBar(ESB_ENABLE_BOTH);
	}
	else
	{
		m_SB_GSP.EnableScrollBar(ESB_DISABLE_BOTH);
	}
	
	imActualLine = imGSPRangeMax;
	DisplayShiftRosterData(m_SB_GSP.GetScrollPos());

	rmCodeNumberData.NrOfWeeks--; // CodeNumber
	if (IsCodeDlgActive())
	{
		pomShiftCodeNumberDlg->SetValues(rmCodeNumberData);
		pomShiftCodeNumberDlg->SetWeeks(omGSPTableData.GetSize());
		pomShiftCodeNumberDlg->SetDataArray(omGSPTableData);
		pomShiftCodeNumberDlg->ReNewAll();
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// ???
//**********************************************************************************

void ShiftRoster_View::ChangeShiftRequirementData(int ipTyp)
{
	CCS_TRY;
	if (omSelSPLurno == omActualSPLurnoByGPL)
	{
		if (ipTyp == NUMBER)
		{
			CString olBSDCode;
			CString olBSDUrno;
			CString olPfcCode;
			CString olPfcUrno;
			CString olBSDCode2;
			CString olBSDUrno2;
			CString olPfcCode2;
			CString olPfcUrno2;
			CString olViewFunctionFctcs = CString(",") + omViewFunctionFctcs + CString(",");

			for (int i = omChangedGSPs.GetSize() - 1; i >= 0; i--)
			{
				for (int ilDay = 0; ilDay <= 7; ilDay++)
				{
					if(ilDay != 0)
					{
						olBSDCode = omGSPTableData[omChangedGSPs[i].Record].Day[ilDay-1];
						olBSDUrno = omGSPTableData[omChangedGSPs[i].Record].DayUrno[ilDay-1];
						olPfcCode = omGSPTableData[omChangedGSPs[i].Record].PfcCode[ilDay-1];
						olPfcUrno = omGSPTableData[omChangedGSPs[i].Record].PfcUrno[ilDay-1];
						olBSDCode2 = omGSPTableData[omChangedGSPs[i].Record].Day2[ilDay-1];
						olBSDUrno2 = omGSPTableData[omChangedGSPs[i].Record].Day2Urno[ilDay-1];
						olPfcCode2 = omGSPTableData[omChangedGSPs[i].Record].PfcCode2[ilDay-1];
						olPfcUrno2 = omGSPTableData[omChangedGSPs[i].Record].PfcUrno2[ilDay-1];
					}
					else
					{
						olBSDCode	= "";
						olBSDCode2	= "";
					}

					if (ogBsdData.GetBsdByBsdc(olBSDCode) != NULL)
					{
						if (olViewFunctionFctcs.Find(CString(",") + olPfcCode + CString(",")) != -1 || !omViewFunctionFctcs.GetLength())
						{
							CString olBsdc = omSortCode + olBSDCode;
							NSRTABLEDATA *prlNsrTableData = prlNsrTableData = FindNSRTableGrupp(olBsdc, olPfcCode);
							if (prlNsrTableData != NULL)
							{
								AddOrSubIsDay (prlNsrTableData, ilDay, -1, omChangedGSPs[i].OldNumber);
								AddOrSubIsDay (prlNsrTableData, ilDay,  1, omChangedGSPs[i].NewNumber);
							}
							else
							{
								HandleNSRTable(olBSDUrno, olPfcUrno, omChangedGSPs[i].NewNumber, ilDay);
							}
						}
					}
					if (ogBsdData.GetBsdByBsdc(olBSDCode2) != NULL)
					{
						if (olViewFunctionFctcs.Find(CString(",") + olPfcCode + CString(",")) != -1 || !omViewFunctionFctcs.GetLength())
						{
							CString olBsdc = omSortCode + olBSDCode2;
							NSRTABLEDATA *prlNsrTableData = prlNsrTableData = FindNSRTableGrupp(olBsdc, olPfcCode2);
							if (prlNsrTableData != NULL)
							{
								AddOrSubIsDay (prlNsrTableData, ilDay, -1, omChangedGSPs[i].OldNumber);
								AddOrSubIsDay (prlNsrTableData, ilDay,  1, omChangedGSPs[i].NewNumber);
							}
							else
							{
								HandleNSRTable(olBSDUrno2, olPfcUrno2, omChangedGSPs[i].NewNumber, ilDay);
							}
						}
					}
				}
			}
		}
		else if(ipTyp == DAY || ipTyp == WEEK)
		{
			for(int i = omChangedGSPs.GetSize()-1; i>= 0 ;i--)
			{
				int ilNstf=0;
				if(ipTyp == DAY)
					ilNstf = atoi(omGSPTableData[omChangedGSPs[i].Record].Number);
				if(ipTyp == WEEK)
					ilNstf = omChangedGSPs[i].OldNumber;
				
				// ***********************************************************
				// Die Alten Werte werden abgezogen
				if(ogBsdData.GetBsdByBsdc(omChangedGSPs[i].OldBSDCode) != NULL)
				{
					// Erste Schicht
					CString olBsdc = omSortCode + omChangedGSPs[i].OldBSDCode;
					NSRTABLEDATA *prlNsrTableData = FindNSRTableGrupp (olBsdc, omChangedGSPs[i].OldPfcCode);
					if (prlNsrTableData != NULL)
					{
						AddOrSubIsDay(prlNsrTableData,omChangedGSPs[i].Day,-1,ilNstf);
					}
				}
				// Die Alten Werte werden abgezogen
				if(ogBsdData.GetBsdByBsdc(omChangedGSPs[i].Old2BSDCode) != NULL)
				{
					// Zwote Schicht
					CString olBsdc = omSortCode + omChangedGSPs[i].Old2BSDCode;
					NSRTABLEDATA *prlNsrTableData = FindNSRTableGrupp (olBsdc, omChangedGSPs[i].OldPfc2Code);
					if(prlNsrTableData != NULL)
					{
						AddOrSubIsDay(prlNsrTableData,omChangedGSPs[i].Day,-1,ilNstf);
					}
				}
				// ***********************************************************
				// Die neuen Werte werden addiert
				if(ogBsdData.GetBsdByBsdc(omChangedGSPs[i].NewBSDCode) != NULL)
				{
					CString olBsdc = omSortCode + omChangedGSPs[i].NewBSDCode;
					NSRTABLEDATA *prlNsrTableData = FindNSRTableGrupp (olBsdc, omChangedGSPs[i].NewPfcCode);
					if (prlNsrTableData != NULL)
					{
						AddOrSubIsDay(prlNsrTableData,omChangedGSPs[i].Day,1,ilNstf);
					}
					else
					{
						CString olText;
						BSDDATA* polBsd = ogBsdData.GetBsdByBsdc(omChangedGSPs[i].NewBSDCode);

						if (polBsd != NULL)
						{
							CString olEsbg(polBsd->Esbg);
							olEsbg = olEsbg.Left(2) + ":" + olEsbg.Right(2);
							CString olLsen(polBsd->Lsen);
							olLsen = olLsen.Left(2) + ":" + olLsen.Right(2);
							if (omChangedGSPs[i].NewPfcCode.GetLength() > 0)
							{
								olText = " ("+ omChangedGSPs[i].NewPfcCode + ", " + olEsbg + " - " + olLsen + ")";
							}
							else
							{
								olText = " ("+ olEsbg + " - " + olLsen + ")";
							}
						}
						
						prlNsrTableData = new NSRTABLEDATA;
						
						AddOrSubIsDay(prlNsrTableData,omChangedGSPs[i].Day,1,ilNstf);
						prlNsrTableData->Fctc = omChangedGSPs[i].NewPfcCode;
						prlNsrTableData->Grupp = omSortCode + omChangedGSPs[i].NewBSDCode + olText;
						prlNsrTableData->GruppUrno = omChangedGSPs[i].NewBSDUrno;
						omNSRTableData.Add(prlNsrTableData);
						omNSRTableDataMap.SetAt(omSortCode + omChangedGSPs[i].NewBSDCode + omChangedGSPs[i].NewPfcCode, prlNsrTableData);
					}
				}
				// Zwote Schicht
				if(ogBsdData.GetBsdByBsdc(omChangedGSPs[i].New2BSDCode) != NULL)
				{
					CString olBsdc = omSortCode + omChangedGSPs[i].New2BSDCode;
					NSRTABLEDATA *prlNsrTableData = FindNSRTableGrupp (olBsdc, omChangedGSPs[i].NewPfc2Code);
					if(prlNsrTableData != NULL)
					{
						AddOrSubIsDay(prlNsrTableData,omChangedGSPs[i].Day,1,ilNstf);
					}
					else
					{
						// ADO
						CString olText;
						BSDDATA* polBsd = ogBsdData.GetBsdByBsdc(omChangedGSPs[i].New2BSDCode);
						
						if (polBsd != NULL)
						{
							CString olEsbg(polBsd->Esbg);
							olEsbg = olEsbg.Left(2) + ":" + olEsbg.Right(2);
							CString olLsen(polBsd->Lsen);
							olLsen = olLsen.Left(2) + ":" + olLsen.Right(2);

							if (omChangedGSPs[i].NewPfc2Code.GetLength() > 0)
							{
								olText = " ("+ omChangedGSPs[i].NewPfc2Code + ", " + olEsbg + " - " + olLsen + ")";
							}
							else
							{
								olText = " ("+ olEsbg + " - " + olLsen + ")";
							}
						}
						
						prlNsrTableData = new NSRTABLEDATA;
						
						AddOrSubIsDay(prlNsrTableData,omChangedGSPs[i].Day,1,ilNstf);
						prlNsrTableData->Fctc = omChangedGSPs[i].NewPfc2Code;
						prlNsrTableData->Grupp = omSortCode + omChangedGSPs[i].New2BSDCode + olText;
						prlNsrTableData->GruppUrno = omChangedGSPs[i].New2BSDUrno;
						omNSRTableData.Add(prlNsrTableData);
						omNSRTableDataMap.SetAt(omSortCode + omChangedGSPs[i].New2BSDCode + omChangedGSPs[i].NewPfcCode, prlNsrTableData);
					}
				}
				// ***********************************************************
				
			}
		}
		NSRTABLEDATA *prlNsrTableData = FindNSRTableGrupp((CString)"");
		if (prlNsrTableData != NULL)
		{
			rmActualShifData.Type		= prlNsrTableData->Type;
			rmActualShifData.Grupp		= prlNsrTableData->Grupp;
			rmActualShifData.GruppUrno	= prlNsrTableData->GruppUrno;
			rmActualShifData.MonIs		= prlNsrTableData->MonIs;
			rmActualShifData.TueIs		= prlNsrTableData->TueIs;
			rmActualShifData.WndIs		= prlNsrTableData->WndIs;
			rmActualShifData.ThuIs		= prlNsrTableData->ThuIs;
			rmActualShifData.FriIs		= prlNsrTableData->FriIs;
			rmActualShifData.SatIs		= prlNsrTableData->SatIs;
			rmActualShifData.SunIs		= prlNsrTableData->SunIs;
			prlNsrTableData->MonDebit	= rmActualShifData.MonDebit;
			prlNsrTableData->TueDebit	= rmActualShifData.TueDebit;
			prlNsrTableData->WndDebit	= rmActualShifData.WndDebit;
			prlNsrTableData->ThuDebit	= rmActualShifData.ThuDebit;
			prlNsrTableData->FriDebit	= rmActualShifData.FriDebit;
			prlNsrTableData->SatDebit	= rmActualShifData.SatDebit;
			prlNsrTableData->SunDebit	= rmActualShifData.SunDebit;
		}
	}
	omChangedGSPs.DeleteAll();
	CCS_CATCH_ALL;
}

//**********************************************************************************
// ???
//**********************************************************************************

void ShiftRoster_View::OnGSPTable_L_ButtonKlick(UINT nID, LONG rpNotify)
{
	CCS_TRY;
	omMouseClick = "L";
	if (pomShiftAWDlg == NULL || !(::IsWindowVisible(pomShiftAWDlg->m_hWnd)))
	{
		bool ilStatus = true;
		CString olGPLvafr,olGPLvato,olGPLstpt,olErrorText;
		CTime olTmpTimeFr = -1;
		CTime olTmpTimeTo = -1;
		
		m_E_GPL_Vafr.GetWindowText(olGPLvafr);
		m_E_GPL_Vato.GetWindowText(olGPLvato);
		
		if(m_E_GPL_Vafr.GetStatus() == false)
			ilStatus = false;
		if(m_E_GPL_Vato.GetStatus() == false)
			ilStatus = false;
		
		if((olGPLvato.GetLength() != 0 && olGPLvafr.GetLength() == 0) || (olGPLvato.GetLength() == 0 && olGPLvafr.GetLength() != 0))
			ilStatus = false;
		if(ilStatus == true && olGPLvato.GetLength() != 0 && olGPLvafr.GetLength() != 0) 
		{
			olTmpTimeFr = DateHourStringToDate(olGPLvafr,CString("00:00"));
			olTmpTimeTo = DateHourStringToDate(olGPLvato,CString("00:00"));
			if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
				ilStatus = false;
		}
		if(ilStatus == true)
		{
			
			CString olVafr = olTmpTimeFr.Format("%Y%m%d");
			CString olVato = olTmpTimeTo.Format("%Y%m%d");
			
			CCSButtonCtrl *polButton = (CCSButtonCtrl*) rpNotify;
			GSPTABLEPTR *prlData =  FindGSPTablePtr(polButton);

			if(prlData != NULL)
			{
				if (prlData->Record > -1)
				{
					omStaffUrnosAll = prlData->BsdUrno;
					ShiftAWDlg *pomDlg = new ShiftAWDlg(&omStaffUrnosAll,EMPLOYEE,false,olVafr,olVato,this);
					pomDlg->ShowWindow(SW_SHOW);
					pomShiftAWDlg = pomDlg;
				}
			}
		}
		else
		{
			pomShiftAWDlg = NULL;
			Beep(440,70);
			MessageBox(LoadStg(SHIFT_VALID_ERROR), LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
		}
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// Nachrichten der CCSEdit Felder (Leftklick) werden hier bearbeitet und
// ggf. DnD eingeleitet.
//**********************************************************************************

void ShiftRoster_View::OnGSPTable_EditLButtonDown(UINT ipID, LONG rpNotify)
{
	CCS_TRY;
	
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*) rpNotify;
	GSPTABLEPTR *prlGSPTablePtr =  FindGSPTablePtr(prlNotify->SourceControl);
	
	// Wurde in ein CCSEdit Feld der GSP Liste geklickt ?
	if(prlGSPTablePtr != NULL)
	{
		if (prlGSPTablePtr->Record > -1 && (prlNotify->Flags & MK_LBUTTON) && (prlNotify->Flags & MK_CONTROL))
		{
			//-- a Day field with CONTROL --
			bool blIsSelectField = false;
			for (int i = 0; i < omSelectGSPTableData.GetSize(); i++)
			{
				if (omSelectGSPTableData[i].SourceControl == prlNotify->SourceControl)
				{
					blIsSelectField = true;
					omSelectGSPTableData.DeleteAt(i);
					prlNotify->SourceControl->SetBKColor(WHITE); 
					omOldFieldText = prlGSPTablePtr->BsdCode;
					i = omSelectGSPTableData.GetSize();
				}
			}
			if (blIsSelectField == false)
			{
				prlNotify->SourceControl->SetBKColor(SELECTBLUE); 
				omOldFieldText = prlGSPTablePtr->BsdCode;
				SELECTGSPTABLEDATA *prlSelectGSPTableData = new SELECTGSPTABLEDATA;
				prlSelectGSPTableData->SourceControl = prlNotify->SourceControl;
				prlSelectGSPTableData->Text = prlGSPTablePtr->BsdCode;
				omSelectGSPTableData.Add(prlSelectGSPTableData);
				bmIsDragDropAction = true;
			}
		}
		else
		{
			//-- a Day field without SHIFT or a Number field--
			DeleteSelectGSPTableFields();
			prlNotify->SourceControl->SetBKColor(SELECTBLUE); 
			omOldFieldText = prlGSPTablePtr->BsdCode;
		}
	}
	else
	{
		// Testen ob DnD durchgef�hrt werden kann und dann durchf�hren
		BeginNSRDragAndDrop(prlNotify);
	}
	CCS_CATCH_ALL;
}
//**********************************************************************************
// Nachrichten der CCSEdit Felder (Rightclick) werden hier bearbeitet und
// ggf. ein Contextmenu ge�ffnet.
//**********************************************************************************
void ShiftRoster_View::OnGSPTable_EditRButtonDown(UINT ipID, LONG rpNotify)
{
	CCS_TRY;

	CPoint olPoint;
    ::GetCursorPos(&olPoint);

	// check whether the floating menu should be displayed or not
	if (this->imStPtWeek > 1)
		return;

	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*) rpNotify;
	GSPTABLEPTR *prlGSPTablePtr =  FindGSPTablePtr(prlNotify->SourceControl);
	if (prlGSPTablePtr == NULL)
	{
		if (strlen(prlNotify->Text) > 0 && !strstr(prlNotify->Text, "."))
		{
			CMenu menu;
			menu.CreatePopupMenu();
			for (int i = menu.GetMenuItemCount(); i > 0; i--)
			{
				menu.RemoveMenu(i, MF_BYPOSITION);
			}

			CString olTmp;
			// "Zeile einf�gen vor %d"
			olTmp = LoadStg(IDS_STRING63500);
			olTmp.Replace ("%d", prlNotify->Text);
			menu.AppendMenu(MF_STRING, MENU_ADD_LINE, olTmp);
			menu.AppendMenu(MF_SEPARATOR);
			// "Zeile %d l�schen"
			olTmp = LoadStg(IDS_STRING63501);
			olTmp.Replace ("%d", prlNotify->Text);
			menu.AppendMenu(MF_STRING, MENU_DEL_LINE, olTmp);
			menu.AppendMenu(MF_SEPARATOR);
			// "Abbrechen"
			menu.AppendMenu(MF_STRING, MENU_CANCEL_ACTION, LoadStg(IDS_STRING552));
			menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, olPoint.x, olPoint.y, this, NULL);

			//Zeilennummer merken
			imClickedLine = atoi (prlNotify->Text);
		}
	}
	CCS_CATCH_ALL;
}

//****************************************************************************************
// Test ob DnD m�glich und dann durchf�hren (Schichten von Tabelle unten nach oben ziehen
//****************************************************************************************

void ShiftRoster_View::BeginNSRDragAndDrop(CCSEDITNOTIFY *prlNotify)
{
	CCS_TRY;
	CString olBsdCode;
	CString olPfcCode;
	int ilPos1;
	int ilPos2;
	
	// Wenn diese beiden Zeichen gefunden werden handelt es sich um die �berschrift
	// -> Kein DnD
	if (prlNotify->Text.Find("<") != -1 || prlNotify->Text.Find(">") != -1)
		return;
	
	// Schichtcode und Funktion ausfiltern
	ilPos1 = prlNotify->Text.Find(" (");
	ilPos2 = prlNotify->Text.Find(")");
	if (ilPos1 != -1 || ilPos2 != -1)
	{
		//prlNotify->Text = prlNotify->Text.Left(prlNotify->Text.Find(" ("));
		olBsdCode = prlNotify->Text.Left(ilPos1);

		ilPos2 = prlNotify->Text.Find(",", ilPos1);
		if (ilPos2 != -1)
		{
			olPfcCode = prlNotify->Text.Mid(ilPos1 + 2, ilPos2 - ilPos1 - 2);
		}
	}

	// Wurde in ein Schicht Feld (Grupp) geklickt?
	// Alle NSR Lines durchgehen
	// Achtung, die erste Zeile hier nicht von Interesse, da �berschrift.
	for (int i = 0; i < imNSRLines; i++)
	{
		// Edit Feld gefunden ?
		if (((omNSRTable.GetAt(i)).Grupp) == prlNotify->SourceControl)
		{
			// D'n'D-Objekt initialisieren
			omDragDropObject.CreateDWordData(DIT_SHIFTROSTER_NSR_TABLE, 3);

			// Basisschichten-Datenobjekt hinzuf�gen
			omDragDropObject.AddString(olBsdCode);

			// TEST
			omDragDropObject.AddString("BSD");

			// Funktion hinzuf�gen
			omDragDropObject.AddString(olPfcCode);//omNSRTableData[i].Fctc);

			// Aktion fortsetzen
			omDragDropObject.BeginDrag();
			break;
		}
	}
	CCS_CATCH_ALL;
}

//*************************.*********************************************************
// einem CCS Edit wurde der Focus entzogen. Eingabe wird bearbeitet.
//**********************************************************************************

void ShiftRoster_View::OnGSPTable_EditKillfocus(UINT ipID, LONG rpNotify)
{
	CCS_TRY;
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*) rpNotify;
	GSPTABLEPTR *prlGSPTablePtr =  FindGSPTablePtr(prlNotify->SourceControl);
	if(prlGSPTablePtr != NULL)
	{
		if(prlGSPTablePtr->Record == -1)// BsdCode.GetLength() == 0 && prlGSPTablePtr->BsdUrno.GetLength() == 0)
		{
			prlNotify->SourceControl->SetBKColor(WHITE);
			prlNotify->SourceControl->SetInitText("");
		}
		else
		{
			int ilDay_or_Nr = 0;
			CHANGEGSPDATA *polChangedGSPs = new CHANGEGSPDATA;
			polChangedGSPs->Record = prlGSPTablePtr->Record;	// only int's, => weekcount

			if(prlGSPTablePtr->BsdCode != "" && prlGSPTablePtr->BsdUrno == "")
			{
				//-- a Number field was worked --
				prlNotify->SourceControl->SetBKColor(WHITE);
				polChangedGSPs->OldNumber = atoi(prlGSPTablePtr->BsdCode);//hier steht die Nummer drin
				if(prlNotify->Text != "")
				{
					prlGSPTablePtr->BsdCode = prlNotify->Text;
				}
				else
				{
					prlGSPTablePtr->BsdCode = "0";
				}
				polChangedGSPs->NewNumber = atoi(prlGSPTablePtr->BsdCode);
				omGSPTableData[prlGSPTablePtr->Record].Number = prlGSPTablePtr->BsdCode;
				
				rmCodeNumberData.MaSchedule += (polChangedGSPs->NewNumber-polChangedGSPs->OldNumber); // CodeNumber
				if (omSelSPLurno == omActualSPLurnoByGPL)
				{
					rmCodeNumberData.MaScheduleAll += (polChangedGSPs->NewNumber-polChangedGSPs->OldNumber); // CodeNumber
				}
				
				// Set Textcolor of the Number
				int ilIs =  GetItemCount(omGSPTableData[prlGSPTablePtr->Record].StaffUrno,'|');
				int ilDebit = atoi(omGSPTableData[prlGSPTablePtr->Record].Number);
				if (ilIs == ilDebit)
				{
					prlNotify->SourceControl->SetTextChangedColor(BLACK);
					prlNotify->SourceControl->SetTextColor(BLACK);
				}
				else if (ilIs > ilDebit)
				{
					prlNotify->SourceControl->SetTextChangedColor(BLUE);
					prlNotify->SourceControl->SetTextColor(BLUE);
				}
				else
				{
					prlNotify->SourceControl->SetTextChangedColor(RED);
					prlNotify->SourceControl->SetTextColor(RED);
				}
				// END Set Textcolor of the Number

				ilDay_or_Nr = NUMBER;
			}
			else
			{

				// Speichern der alten Daten (vor dem Edit)
				polChangedGSPs->Record	   = prlGSPTablePtr->Record;
				polChangedGSPs->Day		   = prlGSPTablePtr->Day;
				polChangedGSPs->OldBSDCode = prlGSPTablePtr->BsdCode;
				polChangedGSPs->OldBSDUrno = prlGSPTablePtr->BsdUrno;
				polChangedGSPs->OldPfcCode = prlGSPTablePtr->PfcCode;
				polChangedGSPs->OldPfcUrno = prlGSPTablePtr->PfcUrno;

				//-- a Day field was worked --
				if(omSelectGSPTableData.GetSize() == 0)
				{
					// Hintergrund setzen, je nach Regel
					if (prlGSPTablePtr->bSetBk)
					{
						prlNotify->SourceControl->SetBKColor(DT_LIGHTBLUE);
					}
					else
					{
						prlNotify->SourceControl->SetBKColor(WHITE);
					}
				}

				if (prlNotify->IsChanged)//(CString(prlNotify->Text) != polChangedGSPs->OldBSDCode)
				{

					if (bmIsDragDropAction == false)
					{
						
						if (prlNotify->Text != "")
						{
							BSDDATA *prlBsdData = ogBsdData.GetBsdByBsdc(prlNotify->Text);
							ODADATA *prlOdaData = ogOdaData.GetOdaBySdac(prlNotify->Text);

							if (prlBsdData != NULL)
							{
								CString olTmp;
								CString olFctc;

								olFctc = CString(prlBsdData->Fctc);

								if (olFctc == "")
								{
									//wenn die Basis-Schicht keine Funktion hat, m�ssen wir eine ausw�hlen
									CStringArray	olStrArr;
									CUIntArray		olPfcUrnos;
									olTmp = omViewFunctionUrnos;
									if (CedaData::ExtractTextLineFast (olStrArr, olTmp.GetBuffer (0), ","))
									{
										for (int ilSize = 0; ilSize < olStrArr.GetSize(); ilSize++)
										{
											olTmp = olStrArr[ilSize];
											if (olTmp.GetLength () > 0)
											{
												olPfcUrnos.Add (atoi(olTmp.GetBuffer(0)));
											}
										}
									}
									else
									{
										for (int ilPfc = 0; ilPfc < ogPfcData.omData.GetSize(); ilPfc++)
										{
											olPfcUrnos.Add (ogPfcData.omData[ilPfc].Urno);
										}
									}

									PfcSelectDlg olDlg(this, &olPfcUrnos);
									if (olPfcUrnos.GetSize() > 1)
									{
										if (olDlg.DoModal() == IDOK)
										{
											PFCDATA* polPfcData = ogPfcData.GetPfcByFctc(olDlg.omSelPfcCode);
											if (polPfcData != 0)
											{
												olTmp.Format("%ld", polPfcData->Urno);
												prlGSPTablePtr->PfcUrno = olTmp;
												prlGSPTablePtr->PfcCode = polPfcData->Fctc;
											}
										}
										else
										{
											prlNotify->SourceControl->SetWindowText ("");
											return;
										}
									}
									else if (olPfcUrnos.GetSize() == 1)
									{
										PFCDATA* polPfcData = ogPfcData.GetPfcByUrno(olPfcUrnos[0]);
										if (polPfcData != 0)
										{
											olTmp.Format("%ld", olPfcUrnos[0]);
											prlGSPTablePtr->PfcUrno = olTmp;
											prlGSPTablePtr->PfcCode = polPfcData->Fctc;
										}
									}
								}
								else
								{
									PFCDATA* polPfcData = ogPfcData.GetPfcByFctc(olFctc);
									if (polPfcData != 0)
									{
										olTmp.Format("%ld", polPfcData->Urno);
										prlGSPTablePtr->PfcUrno = olTmp;
										prlGSPTablePtr->PfcCode = olFctc;
									}
								}

								olTmp.Format("%d",prlBsdData->Urno);
								prlGSPTablePtr->BsdUrno = olTmp;
								prlGSPTablePtr->BsdCode = prlNotify->Text;
							}
							else if(prlOdaData != NULL)
							{
								//Nur erlaubt f�r Free = 'x' (Regul�r arbeitsfrei) oder oder Type = 'S' (sleepday)
								if (prlOdaData->Free == CString("x") || prlOdaData->Type == CString("S"))
								{
									prlGSPTablePtr->BsdCode = prlNotify->Text;
									CString olTmp;
									olTmp.Format("%d",prlOdaData->Urno);
									prlGSPTablePtr->BsdUrno = olTmp;
								}
								else
								{
									Beep(440,70);
									prlNotify->SourceControl->SetInitText(prlGSPTablePtr->BsdCode);
								}
							}
							else
							{
								Beep(440,70);
								prlNotify->SourceControl->SetInitText(prlGSPTablePtr->BsdCode);
							}
							polChangedGSPs->NewBSDCode =  prlGSPTablePtr->BsdCode;
							polChangedGSPs->NewBSDUrno =  prlGSPTablePtr->BsdUrno;
							polChangedGSPs->NewPfcCode =  prlGSPTablePtr->PfcCode;
							polChangedGSPs->NewPfcUrno =  prlGSPTablePtr->PfcUrno;
						}
						else
						{
							prlGSPTablePtr->BsdCode = "";
							prlGSPTablePtr->BsdUrno = "";
						}
					}
					else
					{
						prlNotify->SourceControl->SetInitText(prlGSPTablePtr->BsdCode);
					}

					ilDay_or_Nr = DAY;

					GSPTABLEDATA *polGspTableData = &omGSPTableData[prlGSPTablePtr->Record];
					polGspTableData->IsChanged |= SHIFTCHANGED;
					if (imShowShiftNumber == 1)
					{
						polGspTableData->Day[prlGSPTablePtr->Day - 1]		= polChangedGSPs->NewBSDCode;
						polGspTableData->DayUrno[prlGSPTablePtr->Day - 1]	= polChangedGSPs->NewBSDUrno;
						polGspTableData->PfcCode[prlGSPTablePtr->Day - 1]	= polChangedGSPs->NewPfcCode;
						polGspTableData->PfcUrno[prlGSPTablePtr->Day - 1]	= polChangedGSPs->NewPfcUrno;
					}
					else
					{
						polGspTableData->Day2[prlGSPTablePtr->Day - 1] 		= polChangedGSPs->NewBSDCode;
						polGspTableData->Day2Urno[prlGSPTablePtr->Day - 1] 	= polChangedGSPs->NewBSDUrno;
						polGspTableData->PfcCode2[prlGSPTablePtr->Day - 1]	= polChangedGSPs->NewPfcCode;
						polGspTableData->PfcUrno2[prlGSPTablePtr->Day - 1]	= polChangedGSPs->NewPfcUrno;
					}
					((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);// COLOR
					bmIsChangedGPL = true;
				}
			}

			// Schicht-Stunden neu berechnen?
			if (polChangedGSPs->OldBSDCode != prlGSPTablePtr->BsdCode ||
				polChangedGSPs->NewPfcCode != polChangedGSPs->OldPfcCode ||
				prlNotify->IsChanged)
			{
				if (prlNotify->SourceControl->GetDlgCtrlID() != 0)
				{
					omGSPTableData[prlGSPTablePtr->Record].IsChanged |= STAFFCHANGED;
					// COLOR
					((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);
					
					bmIsChangedGPL = true;
				}
				//-- Change houers per week -- 
				if(ilDay_or_Nr == DAY)
				{
					double ifMinutes = atof(omGSPTableData[prlGSPTablePtr->Record].Houers);
					ifMinutes = ifMinutes*60;
					if(polChangedGSPs->OldBSDCode != "" && ogBsdData.GetBsdByBsdc(polChangedGSPs->OldBSDCode) != NULL)
					{
						BSDDATA *prlBsdData = ogBsdData.GetBsdByBsdc(polChangedGSPs->OldBSDCode);
						if(prlBsdData != NULL)
						{
							ifMinutes -= atoi(prlBsdData->Sdu1);
							rmCodeNumberData.HouersGSP -= atoi(prlBsdData->Sdu1); // CodeNumber
							if(omSelSPLurno == omActualSPLurnoByGPL)
								rmCodeNumberData.HouersAll -= atoi(prlBsdData->Sdu1); // CodeNumber
						}
						rmCodeNumberData.Work --; // CodeNumber
					}
					if(prlGSPTablePtr->BsdCode != "" && ogBsdData.GetBsdByBsdc(prlGSPTablePtr->BsdCode) != NULL)
					{
						BSDDATA *prlBsdData = ogBsdData.GetBsdByBsdc(prlGSPTablePtr->BsdCode);
						if(prlBsdData != NULL)
						{
							ifMinutes += atoi(prlBsdData->Sdu1);
							rmCodeNumberData.HouersGSP += atoi(prlBsdData->Sdu1); // CodeNumber
							if(omSelSPLurno == omActualSPLurnoByGPL)
								rmCodeNumberData.HouersAll += atoi(prlBsdData->Sdu1); // CodeNumber
						}
						rmCodeNumberData.Work ++; // CodeNumber
					}
					omGSPTableData[prlGSPTablePtr->Record].Houers.Format("%#01.2f",ifMinutes/60);
					omGSPTable[prlGSPTablePtr->Record-imActualLine].Houers->SetInitText(omGSPTableData[prlGSPTablePtr->Record].Houers);
				}
				//-- Change Shift Requirement Data -- 
				
				if(omSelSPLurno == omActualSPLurnoByGPL)
				{
					omChangedGSPs.Add(polChangedGSPs);
					
					ChangeShiftRequirementData(ilDay_or_Nr);
					DeleteNullFromShiftRequirementData();
					ChangeShiftRequirementBonusData();
					ChangeShiftRequirementGruppData();
					DisplayShiftRequirementData(2);
				}
				else
				{
					delete polChangedGSPs;//nur test
				}
			}
			else
			{
				delete polChangedGSPs;
			}
			if (IsCodeDlgActive())
			{
				pomShiftCodeNumberDlg->SetValues(rmCodeNumberData);
				pomShiftCodeNumberDlg->SetWeeks(omGSPTableData.GetSize());
				pomShiftCodeNumberDlg->SetDataArray(omGSPTableData);
				pomShiftCodeNumberDlg->ReNewAll();
			}
		}
	}
	else
	{
		//-- a Parameter field was worked --
		int ilSourseID =  prlNotify->SourceControl->GetDlgCtrlID();
		if((ilSourseID == IDC_E_VALIDFROM || ilSourseID == IDC_E_VALIDTO || ilSourseID == IDC_E_STARTPOINT) && prlNotify->IsChanged == true)
		{
			CString olTmpTxt;
			m_E_GPL_Caption.GetWindowText(olTmpTxt);
			if(olTmpTxt != "")
			{
				// COLOR
				((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);
				bmIsChangedGPL = true;
			}
		}
		if(ilSourseID == IDC_E_RELEASENOTE && prlNotify->IsChanged == true)
		{
			int ilSelItem = pomComboBoxShiftplan->GetCurSel();
			if(ilSelItem != CB_ERR)
			{
				CString olTmpTxt;
				pomComboBoxShiftplan->GetLBText(ilSelItem,olTmpTxt);
				if(olTmpTxt.Find("URNO=") != -1)
				{
					// COLOR
					((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);
					
					bmIsChangedBonus = true;
				}
			}
		}
		//-- a Bonus field was worked --
		if((ilSourseID == IDC_E_HOLIDAY || ilSourseID == IDC_E_ROUNDUP || ilSourseID == IDC_E_ABSENCE) && prlNotify->IsChanged == true)
		{
			CString olAbsence, olHoliday ,olRoundup,olTmpTxt;
			int ilSelItem = m_CB_Bonus.GetCurSel();
			if(ilSelItem != CB_ERR)
			{
				m_CB_Bonus.GetLBText(ilSelItem,olTmpTxt);
				if(olTmpTxt.Find("URNO=") != -1)
				{
					CString olUrno = "0";
					if(olTmpTxt.GetLength() > (olTmpTxt.Find("URNO=")+5))
						olUrno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
					for(int i = 0; i < omBonusData.GetSize(); i++)
					{
						if(omBonusData[i].DBObgru == olUrno)
						{
							// COLOR
							((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);
							
							bmIsChangedBonus = true;
							m_E_Absence.GetWindowText(omBonusData[i].DBOdbab);
							m_E_Holiday.GetWindowText(omBonusData[i].DBOdbho);
							m_E_Roundup.GetWindowText(olTmpTxt);
							if(olTmpTxt == "")
								olTmpTxt = "0";
							omBonusData[i].DBOroup = "0." + olTmpTxt;
							omBonusData[i].IsChanged = RECORDCHANGED;
							i = omBonusData.GetSize();
						}
					}
					DeleteNullFromShiftRequirementData();
					ChangeShiftRequirementBonusData();
					ChangeShiftRequirementGruppData();
					DisplayShiftRequirementData(2);
				}
			}
		}
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// rechter Mausclick auf Zeile in GrundSchichtPlan
// --> Maske mit Mitarbeitern soll ge�ffnet werden
// dazu brauchen wir vor allem die Urnos der bereits selektierten MAs
//**********************************************************************************

void ShiftRoster_View::OnGSPTable_R_ButtonKlick(UINT ipID, LONG rpNotify)
{
	CCS_TRY;
	omMouseClick = "R";
	if (pomShiftAWDlg == NULL || !(::IsWindowVisible(pomShiftAWDlg->m_hWnd)))
	{
		bool ilStatus = true;

		CString olGPLvafr;
		CString olGPLvato;
		CString olErrorText;
		CString olSelStfUrnos;

		CTime olTmpTimeFr = -1;
		CTime olTmpTimeTo = -1;

		m_E_GPL_Vafr.GetWindowText(olGPLvafr);
		m_E_GPL_Vato.GetWindowText(olGPLvato);

		//*** Zeiten initialisieren
		if (m_E_GPL_Vafr.GetStatus() == false)
			ilStatus = false;
		if (m_E_GPL_Vato.GetStatus() == false)
			ilStatus = false;
		if ((olGPLvato.GetLength() != 0 && olGPLvafr.GetLength() == 0) || (olGPLvato.GetLength() == 0 && olGPLvafr.GetLength() != 0))
			ilStatus = false;
		if (ilStatus == true && olGPLvato.GetLength() != 0 && olGPLvafr.GetLength() != 0) 
		{
			olTmpTimeFr = DateHourStringToDate(olGPLvafr,CString("00:00"));
			olTmpTimeTo = DateHourStringToDate(olGPLvato,CString("00:00"));
			if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
			{
				ilStatus = false;
			}
		}

		//*** wenn alle Zeiten g�ltig, dann geht es los
		if (ilStatus == true)
		{
			CString olVafr = olTmpTimeFr.Format("%Y%m%d");
			CString olVato = olTmpTimeTo.Format("%Y%m%d");

			omStaffUrnosAll		= "";
			omStaffUrnosOfLine	= "";
			CCSButtonCtrl	*polButton = (CCSButtonCtrl*) rpNotify;
			GSPTABLEPTR		*prlGSPTablePtr = FindGSPTablePtr(polButton);

			if (prlGSPTablePtr != NULL)
			{
				if (prlGSPTablePtr->Record > -1)
				{
					int ilGSPCount;
					int ilUrnos;
					int i;
					int j;

					omStaffUrnosOfLine	= prlGSPTablePtr->BsdUrno;
					omOldFieldText		= prlGSPTablePtr->BsdUrno;

					//*** Select Assignd Employee **********
					CCSPtrArray<ASSIGNDDEMPLOYEE> olAssignd;
					ASSIGNDDEMPLOYEE *prlAssigndData;

					//*** Get all assignd employee **
					CStringArray olUrnoArray;

					RecordSet *polGSPRecord = new RecordSet(ogBCD.GetFieldCount("GSP"));
					ilGSPCount = ogBCD.GetDataCount("GSP");

					bool blActualView = false;

					for (i = 0; i < ilGSPCount; i++)
					{
						CString olSPLurno,olSPLsnam;
						CString olGPLurno,olGPLgsnm,olGPLvafr,olGPLvato;
						CString olGSPurno,olGSPweek,olSTFurnos;
						ilUrnos = 0;

						ogBCD.GetRecord("GSP",i, *polGSPRecord);

						if (polGSPRecord->Values[imGSPgpluIdx] == omSelGPLurno)
						{

							//wenn der GSP der aktuell sichtbare ist, dann nehmen wir die aktuell
							//zugewiesenen MAs. Der GSP ist aber noch nicht gespeichert.
							if (blActualView == false)
							{
								blActualView = true;
								CString olStaffUrnos;
								CString olTmp;

								for (int ilRecCnt = 0; ilRecCnt < omGSPTableData.GetSize(); ilRecCnt++)
								{
									olTmp = omGSPTableData[ilRecCnt].StaffUrno;

									if (olTmp.GetLength() > 0)
									{
										olStaffUrnos += olTmp + '|';
									}
								}

								if (olStaffUrnos.GetLength() > 0)
								{
									olStaffUrnos = olStaffUrnos.Left(olStaffUrnos.GetLength() - 1);
									ilUrnos	= ExtractItemList(olStaffUrnos, &olUrnoArray, '|');
								}
							}
						}
						else
						{
							//sonst nehmen wir die URNOs aus dem GSP-Record
							olSTFurnos	= polGSPRecord->Values[imGSPstfuIdx];
							if (olSTFurnos.GetLength() > 0)
							{
								ilUrnos	= ExtractItemList(olSTFurnos,&olUrnoArray,'|');
							}
						}

						if (ilUrnos > 0)
						{
							olGSPurno = polGSPRecord->Values[imGSPurnoIdx];
							olGSPweek = polGSPRecord->Values[imGSPweekIdx];
							olGPLurno = polGSPRecord->Values[imGSPgpluIdx];

							ogBCD.GetField("GPL", "URNO", olGPLurno, "VAFR", olGPLvafr);
							ogBCD.GetField("GPL", "URNO", olGPLurno, "VATO", olGPLvato);
							ogBCD.GetField("GPL", "URNO", olGPLurno, "SPLU", olSPLurno);
							ogBCD.GetField("GPL", "URNO", olGPLurno, "GSNM", olGPLgsnm);
							ogBCD.GetField("SPL", "URNO", olSPLurno, "SNAM", olSPLsnam);

							for (j = 0; j < ilUrnos; j++)
							{
								prlAssigndData = new ASSIGNDDEMPLOYEE;
								prlAssigndData->Stfu = olUrnoArray[j];

								STFDATA *prlStf = ogStfData.GetStfByUrno(atol(olUrnoArray[j]));
								if (prlStf != NULL)
								{
									prlAssigndData->StfName.Format("%s, %s",prlStf->Lanm,prlStf->Finm);
									if (prlAssigndData->StfName.GetLength() > 1023) //DB-field is VARCHAR[1024]!!
									{
										prlAssigndData->StfName = prlAssigndData->StfName.Left(1023);
									}
								}
								prlAssigndData->Splu    = olSPLurno;
								prlAssigndData->SplName = olSPLsnam;
								prlAssigndData->Gplu    = olGPLurno;
								prlAssigndData->GplName = olGPLgsnm;
								prlAssigndData->AsFr    = olGPLvafr;
								prlAssigndData->AsTo    = olGPLvato;
								prlAssigndData->Gspu    = olGSPurno;
								prlAssigndData->Week    = atoi(olGSPweek);
								olAssignd.Add(prlAssigndData);
							}
						}
					}
					delete polGSPRecord;

					//*** Find all assignd employees at the same time in SPL ************
					int	ilSize = olAssignd.GetSize();

					for (i = 0; i < ilSize; i++)
					{
						if ((olAssignd[i].Splu == omActualSPLurnoByGPL || olAssignd[i].Gplu == omSelGPLurno))
						{
							CString olVafr,olVato;
							ogBCD.GetField ("GPL", "URNO", omSelGPLurno, "VAFR", olVafr);
							ogBCD.GetField ("GPL", "URNO", omSelGPLurno, "VATO", olVato);
							if (olAssignd[i].AsFr <= olVato && olAssignd[i].AsTo >= olVafr)
							{
								omStaffUrnosAll += CString("|") + olAssignd[i].Stfu;
							}
						}
					}
					if (omStaffUrnosAll.GetLength() > 0)
					{
						omStaffUrnosAll = omStaffUrnosAll.Mid(1);
					}
					olAssignd.DeleteAll();
					//*** END Select Assignd Employee **

					//in 'omStaffUrnosAll' are the URNOs of all assigned employees
					//in 'omStaffUrnosOfLine' are the URNOs of the employees of the current line
					ShiftAWDlg *pomDlg = new ShiftAWDlg(&omStaffUrnosOfLine,EMPLOYEE,true,olVafr,olVato,this,polButton);
					pomDlg->ShowWindow(SW_SHOW);
					pomShiftAWDlg = pomDlg;
				}
			}
		}
		else
		{
			pomShiftAWDlg = NULL;
			Beep(440,70);
			MessageBox(LoadStg(SHIFT_VALID_ERROR), LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
		}
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// Es wurde ein Schichtplan in der Combobox ausgew�hlt
//**********************************************************************************

void ShiftRoster_View::OnSelchange_CB_SPL() 
{
	CCS_TRY;
	// irgendwas mit dem Bonus
	int ilMBoxReturn = IDYES;
	if(bmIsChangedBonus == true)
	{
		ilMBoxReturn = MessageBox(LoadStg(SHIFT_ISCHANGED_BONUS),LoadStg(ST_FRAGE),(MB_ICONQUESTION | MB_YESNOCANCEL | MB_DEFBUTTON1));
		if(ilMBoxReturn == IDYES)
		{
			SaveBonus();
			if(bmIsChangedBonus == true)
			{
				ilMBoxReturn = IDCANCEL;
			}
		}
	}
	
	CString olSPLurno,olSPLTxt;
	if(ilMBoxReturn != IDCANCEL)
	{
		// Position der Auswahl in der Combobox erhalten
		int ilSelItem = pomComboBoxShiftplan->GetCurSel();
		// g�ltige Auswahl ?
		if(ilSelItem != CB_ERR)
		{
			// Namen des Schichtplans erhalten
			pomComboBoxShiftplan->GetLBText(ilSelItem,olSPLTxt);
			// Ermittlung der URNO des Schichtplans
			if(olSPLTxt.GetLength() > (olSPLTxt.Find("URNO=")+5))
				olSPLurno = olSPLTxt.Mid(olSPLTxt.Find("URNO=")+5);
			if(olSPLurno == "")
				olSPLurno = "0000000000";
			
			// Hier wird abgefragt ob gesichert werden mu� / kann / soll
			ilMBoxReturn = IDOK;
			if(omActualSPLurnoByGPL == olSPLurno && bmIsChangedGPL == true)
			{
				ilMBoxReturn = MessageBox(LoadStg(SHIFT_SAVE_GPL),LoadStg(ST_FRAGE),(MB_ICONQUESTION | MB_OKCANCEL | MB_DEFBUTTON1));
				if(ilMBoxReturn == IDOK)
				{
					SaveGPL();
					if(bmIsChangedGPL == true)
						ilMBoxReturn = IDCANCEL;
				}
			}
			if(ilMBoxReturn != IDCANCEL)
			{
				// UNRO des aktuellen Schichtplans wird neu gesetzt.
				omSelSPLurno = olSPLurno;
				// Erstellung der Listbox in der alle Grundschichtpl�ne angezeigt werden.
				//Build_LB_GPL();
				OnChangeEGplFilter();
				//Sanduhr
				CWaitCursor olDummy;
				// Bonus Listbox wird gef�llt
				Build_Bonus(olSPLurno);
				
				GetShiftRequirementData(SHIFTPLAN);
			}
			else
			{
				// Irgendwas sollte nicht gespeichert werden
				bool blFound = false;
				for(int i=0;i<pomComboBoxShiftplan->GetCount();i++)
				{
					pomComboBoxShiftplan->GetLBText(i,olSPLTxt);
					if(olSPLTxt.Find(omSelSPLurno) != -1)
					{
						pomComboBoxShiftplan->SetCurSel(i);
						i = pomComboBoxShiftplan->GetCount();
						blFound = true;
					}
				}
				if(blFound == false)
					omSelSPLurno = "0000000000";	
			}
		}
	}
	else
	{
		// Wieder was anderes sollte nicht gespeichert werden
		bool blFound = false;
		for(int i=0;i<pomComboBoxShiftplan->GetCount();i++)
		{
			pomComboBoxShiftplan->GetLBText(i,olSPLTxt);
			if(olSPLTxt.Find(omSelSPLurno) != -1)
			{
				pomComboBoxShiftplan->SetCurSel(i);
				i = pomComboBoxShiftplan->GetCount();
				blFound = true;
			}
		}
		if(blFound == false)
			omSelSPLurno = "0000000000";	
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// ???
//**********************************************************************************

void ShiftRoster_View::OnSelchange_CB_Contract() 
{
	CCS_TRY;
	if(omSelGPLurno != "")
	{
		int ilSelItem = m_CB_Contract.GetCurSel();
		if(ilSelItem != LB_ERR)
		{
			CString olTmpTxt;
			CString olCOTUrno = "0";
			
			m_CB_Contract.GetLBText(ilSelItem,olTmpTxt);
			if(olTmpTxt.GetLength() > (olTmpTxt.Find("URNO=")+5))
				olCOTUrno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
			
			COTDATA *polCot = ogCotData.GetCotByUrno(atol(olCOTUrno));
			if(polCot != NULL)
			{
				rmCodeNumberData.YearDebit = (double)(atof(polCot->Whpw) * (365/7));
				// COLOR
				((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);
				
				bmIsChangedGPL = true;
				if (IsCodeDlgActive())
				{
					pomShiftCodeNumberDlg->SetValues(rmCodeNumberData);
					pomShiftCodeNumberDlg->SetWeeks(omGSPTableData.GetSize());
					pomShiftCodeNumberDlg->SetDataArray(omGSPTableData);
					pomShiftCodeNumberDlg->ReNewAll();
				}
			}
			else
			{
				rmCodeNumberData.YearDebit = 0.00;
				// COLOR
				((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);
				bmIsChangedGPL = true;
				if (IsCodeDlgActive())
				{
					pomShiftCodeNumberDlg->SetValues(rmCodeNumberData);
					pomShiftCodeNumberDlg->SetWeeks(omGSPTableData.GetSize());
					pomShiftCodeNumberDlg->SetDataArray(omGSPTableData);
					pomShiftCodeNumberDlg->ReNewAll();
				}
			}
		}
	}
	CCS_CATCH_ALL;
}


//**********************************************************************************
// Organisationeinheit wechseln
//**********************************************************************************

void ShiftRoster_View::OnSelchange_CB_OrgUnit() 
{
	CCS_TRY;
	if(omSelGPLurno != "")
	{
		int ilSelItem = m_CB_OrgUnit.GetCurSel();
		if(ilSelItem != LB_ERR)
		{
			CString olTmpTxt;
			CString olOrgUrno = "0";
			
			m_CB_OrgUnit.GetLBText(ilSelItem,olTmpTxt);
			if(olTmpTxt.GetLength() > (olTmpTxt.Find("URNO=")+5))
				olOrgUrno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
			
			ORGDATA *polOrg = ogOrgData.GetOrgByUrno(atol(olOrgUrno));
			if(polOrg != NULL)
			{
				// COLOR
				((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);
				
				bmIsChangedGPL = true;
			}
			else
			{
				// COLOR
				((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);

				bmIsChangedGPL = true;
			}
		}
	}
	CCS_CATCH_ALL;
}


//**********************************************************************************
// Eine Drop Aktion wird ausgef�hrt
//**********************************************************************************

LONG ShiftRoster_View::OnDrop(UINT wParam, LONG lParam)
{
	CCS_TRY;

	CWnd *polWnd = (CWnd*)lParam;
	bool blCtlTypeFound = false;

	// was this an Employee-DnD?
	for (int i = 0; i < omGSPTable.GetSize() && blCtlTypeFound == false; i++)
	{
		CWnd *pW = (CWnd*)(omGSPTable[i].Staff);
		if ((CWnd*)(omGSPTable[i].Staff) == polWnd)
		{
			blCtlTypeFound = true;

			if (omGSPTable[i].Staff->omDragDrop.GetDataClass() == DIT_SHIFTROSTER_STAFF_TABLE)
			{
				int ilIdx = i + m_SB_GSP.GetScrollPos();
				if (ilIdx < omGSPTableData.GetSize())
				{

					// => YES, it was an Employee-DnD!
					CString			olNewUrno;
					CString			olNewUrnos;
					GSPTABLEDATA	*polGspTableData	= &omGSPTableData[ilIdx];
					GSPTABLE		*polGspTable		= &omGSPTable[i];

					//looking for already assigned URNOs
					for (int j = 0; j < polGspTable->Staff->omDragDrop.GetDataCount(); j++)
					{
						olNewUrno.Format ("%ld", polGspTable->Staff->omDragDrop.GetDataDWord(j));

						if(omStaffUrnosAll.Find(olNewUrno) < 0)
						{
							olNewUrnos += olNewUrno + '|';
						}
					}
					if (olNewUrnos.GetLength() > 0)
					{
						olNewUrnos = olNewUrnos.Left(olNewUrnos.GetLength() - 1);

						//now we add the new employees to the GSP-table
						OnDropStaff(olNewUrnos, polWnd, polGspTableData, polGspTable);

						//sending the color to the child
						ogDdx.DataChanged((void *)this, DROP_STAFF, (void *)olNewUrnos.GetBuffer(0));

						//adding the URNOs to the GSP-record
						CCSButtonCtrl	*polButton = (CCSButtonCtrl*) lParam;
						GSPTABLEPTR		*prlGSPTablePtr = FindGSPTablePtr(polButton);
						if (prlGSPTablePtr != NULL)
						{
							if (prlGSPTablePtr->BsdUrno.GetLength() > 0)
							{
								prlGSPTablePtr->BsdUrno += '|';
							}
							prlGSPTablePtr->BsdUrno += olNewUrnos;
						}
					}
				}
			}
		}
	}

	if(blCtlTypeFound == false)
	{
		// Zeiger auf selektiertes CCSEdit Feld erhalten
		CCSEdit *polSelectField = (CCSEdit *)lParam;

		// Table pointer suchen.
		GSPTABLEPTR *prlGSPTablePtr = FindGSPTablePtr(polSelectField);

		// kommt die Schicht aus der NSR-Tabelle?
		bool blShiftFromNSR = false;

		// Ist das Feld in der Tabelle ?
		if(prlGSPTablePtr != NULL)
		{
			// Testen ob dieses Feld leer ist oder schon mit Daten gef�llt
			if (prlGSPTablePtr->Record > -1)// BsdCode.GetLength() > 0 && prlGSPTablePtr->BsdUrno.GetLength() > 0)
			{
				if (polSelectField->omDragDrop.GetDataClass() == DIT_DUTYROSTER_BSD_TABLE ||
					polSelectField->omDragDrop.GetDataClass() == DIT_SHIFTROSTER_NSR_TABLE)
				{
					long llBSDurno = 0;
					long llPfcUrno = 0;
					CString olPfc;

					if (polSelectField->omDragDrop.GetDataClass() == DIT_SHIFTROSTER_NSR_TABLE)
					{
						blShiftFromNSR = true;
					}

					// Urno des Zielfeldes erhalten ?
					// Aus welcher Tabelle wurde der Drop gestartet
					if (polSelectField->omDragDrop.GetDataString(1) == "BSD")
					{
						// Aus dem Schichtcode die URNO rausfinden.
						CString csBSDC = polSelectField->omDragDrop.GetDataString(0);
						BSDDATA *prlBsd = ogBsdData.GetBsdByBsdc(csBSDC);
						
						// Fehlerbehandlung wenn keine g�ltige URNO ?
						if (prlBsd == NULL)
						{
							TRACE("ShiftRoster_View OnDrop: URNO nicht gefunden\n");
							return 0L;
						}
						else
						{
							CString olFctc;

							if (blShiftFromNSR == false)
							{
								olFctc = CString(prlBsd->Fctc);
								if (olFctc == "")
								{
									//wenn die Basis-Schicht keine Funktion hat, m�ssen wir eine ausw�hlen
									CStringArray	olStrArr;
									CUIntArray		olPfcUrnos;
									CString olTmp = omViewFunctionUrnos;
									if (CedaData::ExtractTextLineFast (olStrArr, olTmp.GetBuffer (0), ","))
									{
										for (int ilSize = 0; ilSize < olStrArr.GetSize(); ilSize++)
										{
											olTmp = olStrArr[ilSize];
											if (olTmp.GetLength () > 0)
											{
												olPfcUrnos.Add (atoi(olTmp.GetBuffer(0)));
											}
										}
									}
									else
									{
										for (int ilPfc = 0; ilPfc < ogPfcData.omData.GetSize(); ilPfc++)
										{
											olPfcUrnos.Add (ogPfcData.omData[ilPfc].Urno);
										}
									}

									PfcSelectDlg olDlg(this, &olPfcUrnos);
									if (olPfcUrnos.GetSize() > 1)
									{
										if (olDlg.DoModal() == IDOK)
										{
											PFCDATA* polPfcData = ogPfcData.GetPfcByFctc(olDlg.omSelPfcCode);
											if (polPfcData != 0)
											{
												llPfcUrno = polPfcData->Urno;
											}
										}
										else
										{
											return 0L;
										}
									}
									else if (olPfcUrnos.GetSize() == 1)
									{
										llPfcUrno = olPfcUrnos[0];
									}
								}
								else
								{
									PFCDATA* polPfcData = ogPfcData.GetPfcByFctc(olFctc);
									if (polPfcData != 0)
									{
										llPfcUrno = polPfcData->Urno;
									}
								}
							}
							else
							{
								PFCDATA* polPfcData = ogPfcData.GetPfcByFctc(polSelectField->omDragDrop.GetDataString(2));
								if (polPfcData != 0)
								{
									llPfcUrno = polPfcData->Urno;
								}
							}

							llBSDurno = prlBsd->Urno;
						}
					}
					
					if (polSelectField->omDragDrop.GetDataString(1) == "ODA")
					{
						// Aus dem Schichtcode die URNO rausfinden.
						CString csSDAC = polSelectField->omDragDrop.GetDataString(0);
						ODADATA* prlMyOdaData = ogOdaData.GetOdaBySdac(csSDAC);
						
						// Fehlerbehandlung wenn keine g�ltige URNO ?
						if (prlMyOdaData == NULL)
						{
							TRACE("ShiftRoster_View OnDrop: URNO nicht gefunden\n");
							return 0L;
						}
						// URNO auslesen
						llBSDurno = prlMyOdaData->Urno;
						
						//uhi 17.04.00 Nur erlaubt f�r Free = 'x' (Regul�r arbeitsfrei) oder Type = 'S'
						if (strcmp(prlMyOdaData->Free, "x") != NULL && strcmp(prlMyOdaData->Type, "S") != NULL)
						{
							TRACE("ShiftRoster_View OnDrop: Nur regul�r arbeitsfrei oder Schlaftag erlaubt\n");
							return 0L;
						}	
					}

					// Wurde eine URNO gefunden
					if (llBSDurno == 0)
					{
						TRACE("SHIFTROSTERVIEW: Keine URNO gefunden\n");
						return 0L;
					}

					bool blIsSelectField = false;
					for (int x = 0; x < omSelectGSPTableData.GetSize(); x++)
					{
						if (omSelectGSPTableData[x].SourceControl == polSelectField)
						{
							blIsSelectField = true;
						}
					}

					// In omSelectGSPTableData stehen die selektierten Felder, wenn nicht selekiert wurde,
					// wird hier ein Feld eingef�gt.
					if (blIsSelectField == false)
					{
						DeleteSelectGSPTableFields();
						omOldFieldText = prlGSPTablePtr->BsdCode;
						SELECTGSPTABLEDATA *prlSelectGSPTableData = new SELECTGSPTABLEDATA;
						prlSelectGSPTableData->SourceControl = polSelectField;
						prlSelectGSPTableData->Text = prlGSPTablePtr->BsdCode;
						omSelectGSPTableData.Add(prlSelectGSPTableData);
					}

					// Alle selektierten Felder durchgehen 
					int ilDay_or_Nr = 0;
					CHANGEGSPDATA *polChangedGSPs = 0;
					for (int ilField = 0; ilField < omSelectGSPTableData.GetSize(); ilField++)
					{
						// Anhand der Editfelder den Inhalt finden
						prlGSPTablePtr = FindGSPTablePtr(omSelectGSPTableData[ilField].SourceControl);
						if (prlGSPTablePtr != NULL)
						{
							bool blSetBk = false;
							// Speichern der alten Daten (vor dem Drop)
							polChangedGSPs = new CHANGEGSPDATA;;
							polChangedGSPs->Record	   = prlGSPTablePtr->Record;
							polChangedGSPs->OldBSDCode = prlGSPTablePtr->BsdCode;
							polChangedGSPs->OldBSDUrno = prlGSPTablePtr->BsdUrno;
							polChangedGSPs->Day		   = prlGSPTablePtr->Day;

							GSPTABLEDATA *polGspTableData = &omGSPTableData[prlGSPTablePtr->Record];
							if (imShowShiftNumber == 1)
							{
								polChangedGSPs->OldPfcCode = polGspTableData->PfcCode[prlGSPTablePtr->Day - 1];
								polChangedGSPs->OldPfcUrno = polGspTableData->PfcUrno[prlGSPTablePtr->Day - 1];
							}
							else
							{
								polChangedGSPs->OldPfcCode = polGspTableData->PfcCode2[prlGSPTablePtr->Day - 1];
								polChangedGSPs->OldPfcUrno = polGspTableData->PfcUrno2[prlGSPTablePtr->Day - 1];
							}
							blSetBk	= prlGSPTablePtr->bSetBk;

							// Hier werden aus der URNO die weiteren Daten ermittelt.
							BSDDATA *prlBsdData = ogBsdData.GetBsdByUrno(llBSDurno);
							ODADATA *prlOdaData = ogOdaData.GetOdaByUrno(llBSDurno);

							if (prlBsdData != NULL)
							{
								prlGSPTablePtr->BsdCode = prlBsdData->Bsdc;
								omSelectGSPTableData[ilField].SourceControl->SetInitText(prlGSPTablePtr->BsdCode);
								CString olTmp;
								olTmp.Format("%d",prlBsdData->Urno);
								prlGSPTablePtr->BsdUrno = olTmp;

								PFCDATA *polPfcData = ogPfcData.GetPfcByUrno (llPfcUrno);
								if (polPfcData != NULL)
								{
									prlGSPTablePtr->PfcCode	= CString(polPfcData->Fctc);
									olTmp.Format("%d",llPfcUrno);
									prlGSPTablePtr->PfcUrno = olTmp;
								}
							}

							if (prlOdaData != NULL)
							{
								prlGSPTablePtr->BsdCode = prlOdaData->Sdac;
								omSelectGSPTableData[ilField].SourceControl->SetInitText(prlGSPTablePtr->BsdCode);
								CString olTmp;
								olTmp.Format("%d",prlOdaData->Urno);
								prlGSPTablePtr->BsdUrno = olTmp;
							}

							// Hintergrund je nach Flag sichern !
							if (blSetBk)
							{
								omSelectGSPTableData[ilField].SourceControl->SetBKColor(DT_LIGHTBLUE);
							}
							else
							{
								omSelectGSPTableData[ilField].SourceControl->SetBKColor(WHITE);
							}

							polChangedGSPs->NewBSDCode =  prlGSPTablePtr->BsdCode;
							polChangedGSPs->NewBSDUrno =  prlGSPTablePtr->BsdUrno;
							polChangedGSPs->NewPfcCode =  prlGSPTablePtr->PfcCode;
							polChangedGSPs->NewPfcUrno =  prlGSPTablePtr->PfcUrno;

							ilDay_or_Nr = DAY;
						}

						if (omSelectGSPTableData[ilField].Text != prlGSPTablePtr->BsdCode || polChangedGSPs->OldPfcCode != polChangedGSPs->NewPfcCode)
						{
							if (omSelectGSPTableData[ilField].SourceControl->GetDlgCtrlID() != 0)
							{
								GSPTABLEDATA *polGspTableData = &omGSPTableData[prlGSPTablePtr->Record];
								polGspTableData->IsChanged |= SHIFTCHANGED;
								if (imShowShiftNumber == 1)
								{
									polGspTableData->Day[prlGSPTablePtr->Day - 1]		= polChangedGSPs->NewBSDCode;
									polGspTableData->DayUrno[prlGSPTablePtr->Day - 1]	= polChangedGSPs->NewBSDUrno;
									polGspTableData->PfcCode[prlGSPTablePtr->Day - 1]	= polChangedGSPs->NewPfcCode;
									polGspTableData->PfcUrno[prlGSPTablePtr->Day - 1]	= polChangedGSPs->NewPfcUrno;
								}
								else
								{
									polGspTableData->Day2[prlGSPTablePtr->Day - 1] 		= polChangedGSPs->NewBSDCode;
									polGspTableData->Day2Urno[prlGSPTablePtr->Day - 1] 	= polChangedGSPs->NewBSDUrno;
									polGspTableData->PfcCode2[prlGSPTablePtr->Day - 1]	= polChangedGSPs->NewPfcCode;
									polGspTableData->PfcUrno2[prlGSPTablePtr->Day - 1]	= polChangedGSPs->NewPfcUrno;
								}
								((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);// COLOR
								bmIsChangedGPL = true;
							}
							//-- Change houers per week -- 
							if (ilDay_or_Nr == DAY)
							{
								double ifMinutes = atof(omGSPTableData[prlGSPTablePtr->Record].Houers);
								ifMinutes = ifMinutes*60;
								if (omSelectGSPTableData[ilField].Text != "" && ogBsdData.GetBsdByBsdc(omSelectGSPTableData[ilField].Text) != NULL)
								{
									BSDDATA *prlBsdData = ogBsdData.GetBsdByBsdc(omSelectGSPTableData[ilField].Text);
									if (prlBsdData != NULL)
									{
										ifMinutes -= atoi(prlBsdData->Sdu1);
										rmCodeNumberData.HouersGSP -= atoi(prlBsdData->Sdu1);		// CodeNumber
										if(omSelSPLurno == omActualSPLurnoByGPL)
										{
											rmCodeNumberData.HouersAll -= atoi(prlBsdData->Sdu1);	// CodeNumber
										}
									}
									rmCodeNumberData.Work --;										// CodeNumber
								}
								if (prlGSPTablePtr->BsdCode != "" && ogBsdData.GetBsdByBsdc(prlGSPTablePtr->BsdCode) != NULL)
								{
									BSDDATA *prlBsdData = ogBsdData.GetBsdByBsdc(prlGSPTablePtr->BsdCode);
									if (prlBsdData != NULL)
									{
										ifMinutes += atoi(prlBsdData->Sdu1);
										rmCodeNumberData.HouersGSP += atoi(prlBsdData->Sdu1);		// CodeNumber
										if (omSelSPLurno == omActualSPLurnoByGPL)
										{
											rmCodeNumberData.HouersAll += atoi(prlBsdData->Sdu1);	// CodeNumber
										}
									}
									rmCodeNumberData.Work ++;										// CodeNumber
								}

								omGSPTableData[prlGSPTablePtr->Record].Houers.Format("%#01.2f",ifMinutes/60);
								omGSPTable[prlGSPTablePtr->Record-imActualLine].Houers->SetInitText(omGSPTableData[prlGSPTablePtr->Record].Houers);
							}
						}
						if (omSelSPLurno == omActualSPLurnoByGPL)
						{
							omChangedGSPs.Add(polChangedGSPs);
						}
						else
						{
							delete polChangedGSPs;
						}
					}
					//-- Change Shift Requirement Data -- 
					if (omSelSPLurno == omActualSPLurnoByGPL)
					{
						ChangeShiftRequirementData(ilDay_or_Nr);
						DeleteNullFromShiftRequirementData();
						ChangeShiftRequirementBonusData();
						ChangeShiftRequirementGruppData();
						DisplayShiftRequirementData(2);
					}
					if (IsCodeDlgActive())
					{
						pomShiftCodeNumberDlg->SetValues(rmCodeNumberData);
						pomShiftCodeNumberDlg->SetWeeks(omGSPTableData.GetSize());
						pomShiftCodeNumberDlg->SetDataArray(omGSPTableData);
						pomShiftCodeNumberDlg->ReNewAll();
						// ADO: Hier �bergabe an Schwebedialog
					}

					DeleteSelectGSPTableFields();
				}
			}
		}
	}
	return 0L;
	CCS_CATCH_ALL;
	return 0L;
}

//**********************************************************************************
// Eine Drag Aktion wird ausgef�hrt
//**********************************************************************************

LONG ShiftRoster_View::OnDragOver(UINT wParam, LONG lParam)
{
	CCS_TRY;
//MWO
	CWnd *polWnd = (CWnd*)lParam;
	bool blCtlTypeFound = false;

// This section must provide a decision whether dragging over the edit or the button

	for (int i = 0; i < omGSPTable.GetSize() && blCtlTypeFound == false; i++)
	{
		CWnd *pW = (CWnd*)(omGSPTable[i].Staff);
		if ((CWnd*)(omGSPTable[i].Staff) == polWnd)
		{
			blCtlTypeFound = true;
			if (omGSPTable[i].Staff->omDragDrop.GetDataClass() == DIT_SHIFTROSTER_STAFF_TABLE)
			{
				if (i < omGSPTableData.GetSize())
				{
					return 0L;
				}
			}
		}
	}
//END MWO ==> an leave the rest below as is was before

	if(blCtlTypeFound == false)
	{
		CCSEdit *polSelectField = (CCSEdit *)lParam;
		GSPTABLEPTR *prlGSPTablePtr =  FindGSPTablePtr(polSelectField);
		if(prlGSPTablePtr != NULL)
		{
			if (prlGSPTablePtr->Record > -1)// && prlGSPTablePtr->BsdUrno.GetLength() > 0)
			{
				if (polSelectField->omDragDrop.GetDataClass() == DIT_DUTYROSTER_BSD_TABLE)
				{
					return 0L;
				}
				else if (polSelectField->omDragDrop.GetDataClass() == DIT_SHIFTROSTER_NSR_TABLE)
				{
					return 0L;
				}
			}
		}
	}

	return -1L;
	CCS_CATCH_ALL;
	return 0L;
}

//**********************************************************************************
// Test der Return Taste in einem CCSEdit Feld ( Funktioniert nicht !)
//**********************************************************************************

LONG ShiftRoster_View::OnInplaceReturn(UINT wParam, LONG lParam)
{
	m_ButDummy.SetFocus();
	return 0L;
}

//**********************************************************************************
//
//**********************************************************************************

void ShiftRoster_View::OnBStfsP() 
{
	CCS_TRY;
	CString olTmpTxt;

	UINT maxWeek = omGSPTableData.GetSize();

	for (UINT i = 0; i < maxWeek; i++)
	{	
		int j = atoi(omGSPTableData[i].Number);
		j = j + 1;
		olTmpTxt.Format("%d",j);
		omGSPTableData[i].Number = olTmpTxt;
		omGSPTableData[i].IsChanged = RECORDCHANGED;

		CHANGEGSPDATA *polChangedGSPs = new CHANGEGSPDATA;
		polChangedGSPs->Record		= i;
		polChangedGSPs->OldNumber	= j - 1;
		polChangedGSPs->NewNumber	= j;
		omChangedGSPs.Add(polChangedGSPs);

		if (!bmIsChangedGPL)
		{
			bmIsChangedGPL = true;
			((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);
		}
	}

	ChangeShiftRequirementData(NUMBER);
	DeleteNullFromShiftRequirementData();
	ChangeShiftRequirementBonusData();
	ChangeShiftRequirementGruppData();
	DisplayShiftRequirementData(2);
	DisplayShiftRosterData();

	rmCodeNumberData.MaSchedule		+= maxWeek;
	rmCodeNumberData.MaScheduleAll	+= maxWeek;
	if (IsCodeDlgActive())
	{
		pomShiftCodeNumberDlg->SetValues(rmCodeNumberData);
	}

	CCS_CATCH_ALL;
}

//**********************************************************************************
//
//**********************************************************************************

void ShiftRoster_View::OnBStfsM() 
{
	CCS_TRY;
	CString olTmpTxt;

	int	ilSum = 0;	// Anzahl der Zeilen in denen die Subtraktion erfolgreich war.
	int j;			// Anzahl der erforderlichen MAs

	UINT maxWeek = omGSPTableData.GetSize();

	for (UINT i = 0; i < maxWeek; i++)
	{	
		j = atoi(omGSPTableData[i].Number);
		j = j - 1;
		if (j >= 0)
		{
			ilSum++;
			olTmpTxt.Format("%d",j);
			omGSPTableData[i].Number = olTmpTxt;
			omGSPTableData[i].IsChanged = RECORDCHANGED;

			CHANGEGSPDATA *polChangedGSPs = new CHANGEGSPDATA;
			polChangedGSPs->Record		= i;
			polChangedGSPs->OldNumber	= j + 1;
			polChangedGSPs->NewNumber	= j;
			omChangedGSPs.Add(polChangedGSPs);

			if (!bmIsChangedGPL)
			{
				bmIsChangedGPL = true;
				((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);
			}
		}
	}

	if (ilSum > 0)
	{
		ChangeShiftRequirementData(NUMBER);
		DeleteNullFromShiftRequirementData();
		ChangeShiftRequirementBonusData();
		ChangeShiftRequirementGruppData();
		DisplayShiftRequirementData(2);
		DisplayShiftRosterData();

		rmCodeNumberData.MaSchedule		-= ilSum;
		rmCodeNumberData.MaScheduleAll	-= ilSum;

		if (IsCodeDlgActive())
		{
			pomShiftCodeNumberDlg->SetValues(rmCodeNumberData);
		}
	}

	CCS_CATCH_ALL;
}

/*****************************************************************************
l�dt Schichtdaten aus der Datenbank
*****************************************************************************/
void ShiftRoster_View::LoadData()
{
	CCS_TRY;
	// wir pr�fen zuerst, ob Objekte dieses Typs schon geladen sind, wenn nicht, 
	// dann laden wir sie
	bool bSPL, bGPL, bGSP;
	
	CString olObjectDesc;	
	olObjectDesc = ogBCD.GetObjectDesc(CString("SPL"));
	bSPL = olObjectDesc.IsEmpty() != 0;
	olObjectDesc = ogBCD.GetObjectDesc(CString("GPL"));
	bGPL = olObjectDesc.IsEmpty() != 0;
	olObjectDesc = ogBCD.GetObjectDesc(CString("GSP"));
	bGSP = olObjectDesc.IsEmpty() != 0;

	CWaitDlg*	polWaitDlg = 0;

	if (bSPL || bGPL || bGSP)
	{
		polWaitDlg = new CWaitDlg(this);
	}

	CString olTmp;
	CString olWhere;
	CString olOrgCodes = "'asdfasdfadsfas'";
	CString olOrgUrnos = "' ','0'";
	if (ogCCSParam.GetParamValue("GLOBAL","ID_ORG_SECURITY") == "Y")
	{
		ORGDATA *prlOrgData = NULL;
		CString olPriv;
		int ilOrgCnt = ogOrgData.omData.GetSize();
		for (int i = 0; i < ilOrgCnt; i++)
		{
			prlOrgData = &ogOrgData.omData[i];
			olPriv = ogPrivList.GetStat(prlOrgData->Dpt1);
			if (olPriv != "-" && olPriv != "0" && olPriv != " ")
			{
				olTmp.Format(",'%s'",prlOrgData->Dpt1);
				olOrgCodes += olTmp;
				olTmp.Format(",'%ld'",prlOrgData->Urno);
				olOrgUrnos += olTmp;
			}
		}
	}

	if (bSPL)
	{
		ogBCD.SetObject("SPL");
		ogBCD.SetObjectDesc("SPL", LoadStg(IDS_STRING1387));

		// Shift Roster (Schichtpl�ne)
		olWhere = "";
		if(polWaitDlg != 0) 
			polWaitDlg->SetWindowText(LoadStg(IDS_STRING1388));

		if (ogCCSParam.GetParamValue("GLOBAL","ID_ORG_SECURITY") == "Y")
		{
			if (ogBCD.GetFieldIndex("SPL","DPT1") > -1)
			{
				olWhere = "WHERE DPT1 IN (" + olOrgCodes + ")";
			}
		}

		ogBCD.Read(CString("SPL"),olWhere);
	}

	if (bGPL)
	{
		ogBCD.SetObject("GPL");
		ogBCD.SetObjectDesc("GPL", LoadStg(IDS_STRING1392));
		ogBCD.AddKeyMap("GPL", "GSNM");

		// Basic Shift Roster (Grundschichtpl�ne)
		olWhere = "";
		if(polWaitDlg != 0) 
			polWaitDlg->SetWindowText(LoadStg(IDS_STRING1389));

		if (ogCCSParam.GetParamValue("GLOBAL","ID_ORG_SECURITY") == "Y")
		{
			CString olSplUrnos = "' '";
			int ilSplFieldCnt = ogBCD.GetFieldCount("SPL");
			if (ogBCD.GetFieldIndex("GPL","ORGU") > -1)
			{
				olWhere = "WHERE ORGU IN (" + olOrgUrnos + ")";
			}
		}

		ogBCD.Read(CString("GPL"),olWhere);

		// repair references to already deleted SPL's
		CStringArray olDuplicatedGPLs;

		CString olSplu;
		for (int i = 0; i < ogBCD.GetDataCount("GPL"); i++)
		{
			olSplu = ogBCD.GetField("GPL",i,"SPLU");
			if (olSplu.GetLength() > 0)
			{
				CCSPtrArray<RecordSet> olRecords;
				ogBCD.GetRecords("SPL","URNO",olSplu,&olRecords);
				if (olRecords.GetSize() > 0)
				{
					olRecords.DeleteAll();
				}
				else
				{
					// check if gsnm is duplicated -> means, user copied and repaired the GPL by himself
					CCSPtrArray<RecordSet> olGPLRecords;
					ogBCD.GetRecords("GPL","GSNM",ogBCD.GetField("GPL",i,"GSNM"),&olGPLRecords);

					if (olGPLRecords.GetSize() == 1)
					{
						//ogBCD.SetField("GPL","URNO",ogBCD.GetField("GPL",i,"URNO"),"SPLU","",false);
					}
					else
					{
						olDuplicatedGPLs.Add(ogBCD.GetField("GPL",i,"URNO"));
					}

					olGPLRecords.DeleteAll();
				}
			}
		}

		// remove duplicated GPL's from internal storage
		for (i = 0; i < olDuplicatedGPLs.GetSize(); i++)
		{
			ogBCD.DeleteRecord("GPL","URNO",olDuplicatedGPLs[i],false);
		}
	}

	if (bGSP)
	{
		// Basic Shift Roster Weeks (Grundschichtplan Wochen)
		olWhere = "";
		CString olGplUrnos;

		if(polWaitDlg != 0) 
			polWaitDlg->SetWindowText(LoadStg(IDS_STRING1390));

		ogBCD.SetObject("GSP");
		ogBCD.AddKeyMap("GSP","GPLU");
		ogBCD.SetObjectDesc("GSP", LoadStg(IDS_STRING1393));

		if (ogCCSParam.GetParamValue("GLOBAL","ID_ORG_SECURITY") == "Y")
		{
			int ilUrnoIdx = ogBCD.GetFieldIndex("GPL","URNO");
			int ilGplCnt = ogBCD.GetDataCount("GPL");
			RecordSet olRecord;
			for (int i = 0; i < ilGplCnt; i++)
			{
				ogBCD.GetRecord("GPL", i, olRecord);
				olTmp.Format(",'%s'", olRecord.Values[ilUrnoIdx]);
				olGplUrnos += olTmp;
				if ((i % 990 == 0) && (i > 0))
				{
					olWhere = "WHERE GPLU IN (" + olGplUrnos.Mid(1) + ")";
					ogBCD.Read(CString("GSP"),olWhere,NULL,false);
					olGplUrnos = "";
				}
			}
			if (strlen(olGplUrnos) > 0)
			{
				olWhere = "WHERE GPLU IN (" + olGplUrnos.Mid(1) + ")";
				ogBCD.Read(CString("GSP"),olWhere,NULL,false);
			}
		}
		else
		{
			ogBCD.Read(CString("GSP"));
		}
	}

	if(polWaitDlg != 0) delete polWaitDlg;
	CCS_CATCH_ALL;
}


void ShiftRoster_View::OnBSecondshift() 
{
	if (imShowShiftNumber == 1)
	{
		m_B_SecondShift.SetWindowText(LoadStg(IDS_STRING169));		// Erste Schicht zeigen*INTXT*
		imShowShiftNumber = 2;
		DisplayShiftRosterData(m_SB_GSP.GetScrollPos());
	}
	else
	{
		m_B_SecondShift.SetWindowText(LoadStg(IDS_STRING168));		// Zweite Schicht zeigen*INTXT*
		imShowShiftNumber = 1;
		DisplayShiftRosterData(m_SB_GSP.GetScrollPos());
	}
}

/**********************************************************
create default views
**********************************************************/
void ShiftRoster_View::InitDefaultView()
{
	CViewer olViewer;
	CStringArray olPossibleFilters;
	CString olLastView;
	// Read the default value from the database in the server

	//ShiftRoster Viewer ----------------------------
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("SHIFTVIEW1");
	olPossibleFilters.Add("SHIFTVIEW2");
	olPossibleFilters.Add("SHIFTVIEW3");

	olViewer.SetViewerKey("ID_SHEET_SHIFT_VIEW");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	if (olLastView != "")
	{
		olViewer.SelectView(olLastView);	// restore the previously selected view
	}

	olPossibleFilters.RemoveAll();
}

void ShiftRoster_View::Register()
{
	ogDdx.Register(this, SDG_DELETE, CString("ShiftRoster_View"), CString("SDG_DELETE"), ProcessBC);
	ogDdx.Register(this, SDG_CHANGE, CString("ShiftRoster_View"), CString("SDG_CHANGE"), ProcessBC);
	ogDdx.Register(this, SDG_NEW, CString("ShiftRoster_View"), CString("SDG_NEW"), ProcessBC);

	ogDdx.Register(this, MSD_DELETE, CString("ShiftRoster_View"), CString("MSD_DELETE"), ProcessBC);
	ogDdx.Register(this, MSD_CHANGE, CString("ShiftRoster_View"), CString("MSD_CHANGE"), ProcessBC);
	ogDdx.Register(this, MSD_NEW, CString("ShiftRoster_View"), CString("MSD_NEW"), ProcessBC);
	ogDdx.Register(this, RELMSD, CString("ShiftRoster_View"), CString("RELMSD"), ProcessBC);
}

//MWO
void ShiftRoster_View::OnDestroy() 
{
	for (int i = 0; i < omGSPTable.GetSize(); i++)
	{
		omGSPTable[i].ButtonsDragDrop.Revoke();
	}
	CFormView::OnDestroy();
}
//END MWO

void ShiftRoster_View::OnDropStaff(CString pNewUrnos, CWnd *pWnd, GSPTABLEDATA *pGspTableData, GSPTABLE *pGspTable)
{
	if (pGspTableData == NULL || pGspTable == NULL)
		return;

	//saving old values of this line
	CString olStaffUrnosOfLineOld = pGspTableData->StaffUrno;

	//adding the URNOs to the GSP-tabledata
	if (pGspTableData->StaffUrno.GetLength() > 0 && pNewUrnos.GetLength() > 0)
	{
		pGspTableData->StaffUrno += '|';
	}
	pGspTableData->StaffUrno += pNewUrnos;

	//adding the URNOs to the omStaffUrnosAll
	if (omStaffUrnosAll.GetLength() > 0 && pNewUrnos.GetLength() > 0)
	{
		omStaffUrnosAll += '|';
	}
	omStaffUrnosAll += pNewUrnos;
	
	//getting information for the caption of the buttonn
	CString olStaffListFLName;
	CString olStaffListCode;
	CString olStaffFreeFormatted;
	GetStaffListByUrnoList(pGspTableData->StaffUrno, &olStaffListFLName, &olStaffListCode, &olStaffFreeFormatted);
	pGspTableData->StaffFLName	= olStaffListFLName;
	pGspTableData->StaffCode	= olStaffListCode;
	pGspTableData->StaffFree	= olStaffFreeFormatted;

	//looking after the caption of the CCSButtonCtrl
	CCSButtonCtrl	*polButton = (CCSButtonCtrl*) pWnd;
	if (polButton != NULL)
	{
		if(imViewShowEmployee == VIEW_CODE)
		{
			polButton->SetWindowText(olStaffListCode);
		}
		else if (imViewShowEmployee == VIEW_FREE)
		{
			polButton->SetWindowText(olStaffFreeFormatted);
		}
		else
		{
			polButton->SetWindowText(olStaffListFLName);
		}
	}

	if (olStaffUrnosOfLineOld != pGspTableData->StaffUrno)//omUrnoList)
	{
		rmCodeNumberData.MaAssign -= GetItemCount(olStaffUrnosOfLineOld,'|');		// CodeNumber
		rmCodeNumberData.MaAssign += GetItemCount(pGspTableData->StaffUrno,'|');	// CodeNumber
		if (omSelSPLurno == omActualSPLurnoByGPL)
		{
			rmCodeNumberData.MaAssignAll -= GetItemCount(olStaffUrnosOfLineOld,'|');	// CodeNumber
			rmCodeNumberData.MaAssignAll += GetItemCount(pGspTableData->StaffUrno,'|');	// CodeNumber
		}
		pGspTableData->IsChanged |= STAFFCHANGED;
		// COLOR of the Save-Button
		((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);
		bmIsChangedGPL = true;
		if (IsCodeDlgActive())
		{
			pomShiftCodeNumberDlg->SetValues(rmCodeNumberData);
			pomShiftCodeNumberDlg->SetWeeks(omGSPTableData.GetSize());
			pomShiftCodeNumberDlg->SetDataArray(omGSPTableData);
			pomShiftCodeNumberDlg->ReNewAll();
		}
	}

	// Set Textcolor of the Number field
	GSPTABLEPTR *polGSPTablePtr =  FindGSPTablePtr(polButton);
	if (polGSPTablePtr != NULL)
	{
		if (polGSPTablePtr->Record > -1)
		{
			if (pGspTable->Staff == polButton)
			{
				//int ilIs =  GetItemCount(pGspTableData->StaffUrno,'|');
				int ilIs =  GetItemCount(omGSPTableData[polGSPTablePtr->Record].StaffUrno, '|');
				//int ilDebit = atoi(pGspTableData->Number);
				int ilDebit = atoi(omGSPTableData[polGSPTablePtr->Record].Number);
				if (ilIs == ilDebit)
				{
					pGspTable->Number->SetTextChangedColor(BLACK);
					pGspTable->Number->SetTextColor(BLACK);
				}
				else if (ilIs > ilDebit)
				{
					pGspTable->Number->SetTextChangedColor(BLUE);
					pGspTable->Number->SetTextColor(BLUE);
					OverCapacityMsgBox(pGspTableData->Week);
				}
				else
				{
					pGspTable->Number->SetTextChangedColor(RED);
					pGspTable->Number->SetTextColor(RED);
				}
			}
		}
	}
}

void ShiftRoster_View::OnAddEmployees(CString pNewUrnos, CCSButtonCtrl *pButton)
{
	for (int i = 0; i < omGSPTable.GetSize(); i++)
	{
		CCSButtonCtrl *prlStaffButton = omGSPTable[i].Staff;

		if (prlStaffButton == pButton)
		{
			int ilRealPos = i + m_SB_GSP.GetScrollPos();

			if (ilRealPos < omGSPTableData.GetSize() && ilRealPos > -1)
			{
				OnDropStaff(pNewUrnos, (CWnd*) pButton, &omGSPTableData[ilRealPos], &omGSPTable[i]);

				//adding the URNOs to the GSP-record
				GSPTABLEPTR *prlGSPTablePtr = FindGSPTablePtr(prlStaffButton);
				if (prlGSPTablePtr != NULL)
				{
					if (prlGSPTablePtr->BsdUrno.GetLength() > 0)
					{
						prlGSPTablePtr->BsdUrno += '|';
					}
					prlGSPTablePtr->BsdUrno += pNewUrnos;
				}
				break;
			}
		}
	}
}

void ShiftRoster_View::OnDeleteEmployees(CString pDeleteUrnos)
{
	CStringArray	olDeleteUrnosArray;
	CString			olDeleteUrnos = pDeleteUrnos;
	CedaData::ExtractTextLineFast(olDeleteUrnosArray, olDeleteUrnos.GetBuffer(0), "|");
	int				ilCount = olDeleteUrnosArray.GetSize();
	int ilPos1;
	int ilPos2;
	GSPTABLEDATA *polGSPTableData = NULL;
	bool blChanged;

	for (int i = 0; i < omGSPTableData.GetSize(); i++)
	{
		//deleting URNOs from the data-holding
		blChanged   = false;
		polGSPTableData = &omGSPTableData[i];
		omOldFieldText = polGSPTableData->StaffUrno;
		for (int j = 0; j < ilCount; j++)
		{
			ilPos1 = polGSPTableData->StaffUrno.Find(olDeleteUrnosArray [j]);
			if (ilPos1 > -1)
			{
				blChanged = true;
				ilPos2 = polGSPTableData->StaffUrno.Find('|', ilPos1);
				if (ilPos2 < 0)
				{
					if (ilPos1 > 0)
					{
						polGSPTableData->StaffUrno.Replace ("|" + olDeleteUrnosArray [j], "");
					}
					else
					{
						polGSPTableData->StaffUrno.Replace (olDeleteUrnosArray [j], "");
					}
				}
				else
				{
					polGSPTableData->StaffUrno.Replace (olDeleteUrnosArray [j] + "|", "");
				}
				polGSPTableData->IsChanged |= STAFFCHANGED;
				((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);
				bmIsChangedGPL = true;
			}

			ilPos1 = omStaffUrnosAll.Find(olDeleteUrnosArray [j]);
			if (ilPos1 > -1)
			{
				ilPos2 = omStaffUrnosAll.Find('|', ilPos1);
				if (ilPos2 < 0)
				{
					if (ilPos1 > 0)
					{
						omStaffUrnosAll.Replace ("|" + olDeleteUrnosArray [j], "");
					}
					else
					{
						omStaffUrnosAll.Replace (olDeleteUrnosArray [j], "");
					}
				}
				else
				{
					omStaffUrnosAll.Replace (olDeleteUrnosArray [j] + "|", "");
				}
			}
			ilPos1 = omStaffUrnosOfLine.Find(olDeleteUrnosArray [j]);
			if (ilPos1 > -1)
			{
				ilPos2 = omStaffUrnosOfLine.Find('|', ilPos1);
				if (ilPos2 < 0)
				{
					if (ilPos1 > 0)
					{
						omStaffUrnosOfLine.Replace ("|" + olDeleteUrnosArray [j], "");
					}
					else
					{
						omStaffUrnosOfLine.Replace (olDeleteUrnosArray [j], "");
					}
				}
				else
				{
					omStaffUrnosOfLine.Replace (olDeleteUrnosArray [j] + "|", "");
				}
			}
		}

		//adapting caption of the buttons if visible
		if (blChanged == true)
		{
			// first: let's do the update in GSPTABLEDATA
			CString olStaffListFLName;
			CString olStaffListCode;
			CString olStaffFreeFormatted;
			GetStaffListByUrnoList (polGSPTableData->StaffUrno, &olStaffListFLName, &olStaffListCode, &olStaffFreeFormatted);
			polGSPTableData->StaffFLName = olStaffListFLName;
			polGSPTableData->StaffCode	 = olStaffListCode;
			polGSPTableData->StaffFree	 = olStaffFreeFormatted;

			// second: looking for the visible buttons
			GSPTABLE		*polGspTable = NULL;
			CCSButtonCtrl	*polButton	 = NULL;
			int ilRealPos = i - m_SB_GSP.GetScrollPos();
			if (ilRealPos < omGSPTable.GetSize() && ilRealPos > -1)
			{
				polGspTable = &omGSPTable[ilRealPos];
				polButton   = polGspTable->Staff;
				if (polGspTable != NULL && polButton != NULL)
				{
					OnDropStaff("", (CWnd*)polButton, polGSPTableData, polGspTable);
				}
			}
		}
	}
	for (i = 0; i < omGSPTablePtr.GetSize(); i++)
	{
		for (int j = 0; j < ilCount; j++)
		{
			ilPos1 = omGSPTablePtr[i].BsdUrno.Find(olDeleteUrnosArray [j]);
			if (ilPos1 > -1)
			{
				ilPos2 = omGSPTablePtr[i].BsdUrno.Find('|', ilPos1);
				if (ilPos2 < 0)
				{
					if (ilPos1 > 0)
					{
						omGSPTablePtr[i].BsdUrno.Replace ("|" + olDeleteUrnosArray [j], "");
					}
					else
					{
						omGSPTablePtr[i].BsdUrno.Replace (olDeleteUrnosArray [j], "");
					}
				}
				else
				{
					omGSPTablePtr[i].BsdUrno.Replace (olDeleteUrnosArray [j] + "|", "");
				}
			}
		}
	}

	//adapting the number of assigned MAs
	rmCodeNumberData.MaAssignAll -= ilCount;
	rmCodeNumberData.MaAssign	 -= ilCount;

	if (IsCodeDlgActive())
	{
		pomShiftCodeNumberDlg->SetValues(rmCodeNumberData);
		pomShiftCodeNumberDlg->SetWeeks(omGSPTableData.GetSize());
		pomShiftCodeNumberDlg->SetDataArray(omGSPTableData);
		pomShiftCodeNumberDlg->ReNewAll();
	}
}


//*****************************************************************************
//*** notification handler for the TTN_NEEDTEXT message
//*****************************************************************************
BOOL ShiftRoster_View::OnTtnNeedText(UINT id,NMHDR *pTTTStruct,LRESULT *pResult)
{
	TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pTTTStruct;
	UINT nID = pTTTStruct->idFrom;
	nID = ::GetDlgCtrlID((HWND)nID);

	if (nID > 68000)
	{
		int ilTmp		= nID - 68000;
		int ilRow		= (int)(ilTmp / 10) + m_SB_GSP.GetScrollPos();
		int ilColumn	= (ilTmp % 10) - 1;
	
		if (ilRow < omGSPTableData.GetSize())
		{
			CString olText,olText1,olText2;
			BSDDATA *polBsdData1;
			BSDDATA *polBsdData2;
			PFCDATA *polPfcData1;
			PFCDATA *polPfcData2;
			ODADATA *polOdaData1;
			ODADATA *polOdaData2;
			CString olblank=" ";
			
			// row of the GSP-table
			GSPTABLEDATA *polGspTableData = &omGSPTableData[ilRow];

			// getting information about shift & function
			if (imShowShiftNumber == 1||imShowShiftNumber == 2)
			{
				polBsdData1 = ogBsdData.GetBsdByUrno (atol(polGspTableData->DayUrno [ilColumn]));
				polPfcData1= ogPfcData.GetPfcByUrno (atol(polGspTableData->PfcUrno [ilColumn]));
				olText1 = LoadStg(IDS_STRING63448) + " "; //Erste Schicht: *INTXT*
			
				polBsdData2 = ogBsdData.GetBsdByUrno (atol(polGspTableData->Day2Urno [ilColumn]));
				polPfcData2 = ogPfcData.GetPfcByUrno (atol(polGspTableData->PfcUrno2 [ilColumn]));
				olText2 = LoadStg(IDS_STRING63449) + " ";	//Zweite Schicht: *INTXT*
			}

			if (polBsdData1!= NULL || polBsdData2!= NULL)
			{
				// building string
				if(polBsdData1!= NULL)
				{
					olText =olText1 + CString(polBsdData1->Bsdc) + " (";
					CString olEsbg(polBsdData1->Esbg);
					olEsbg = olEsbg.Left(2) + ":" + olEsbg.Right(2);
					CString olLsen(polBsdData1->Lsen);
					olLsen = olLsen.Left(2) + ":" + olLsen.Right(2);
					if (polPfcData1!= NULL)
					{
						olText += CString(polPfcData1->Fctc) + ", ";
					}
					
					olText += olEsbg + " - " + olLsen + ")"+olblank;
				}
				if(polBsdData2!= NULL)
				{
					olText +="||"+olblank+olText2 + CString(polBsdData2->Bsdc) + " (";
					CString olEsbg2(polBsdData2->Esbg);
					olEsbg2 = olEsbg2.Left(2) + ":" + olEsbg2.Right(2);
					CString olLsen2(polBsdData2->Lsen);
					olLsen2 = olLsen2.Left(2) + ":" + olLsen2.Right(2);
					if (polPfcData2!= NULL)
					{
						olText += CString(polPfcData2->Fctc) + ", ";
					}
					olText += olEsbg2 + " - " + olLsen2 + ")";
				}	
					// putting the text to the tooltip
					strcpy(pTTT->szText, olText);
			}
		
			polOdaData2 = ogOdaData.GetOdaByUrno (atol(polGspTableData->Day2Urno [ilColumn]));
			polOdaData1 = ogOdaData.GetOdaByUrno (atol(polGspTableData->DayUrno [ilColumn]));
		
			if (polOdaData1!= NULL||polOdaData2!=NULL)
			{
				if(polOdaData1!=NULL)
				{	
					olText= olText1+CString(polOdaData1->Sdac)+olText+olblank;
				}
				if(polOdaData2!=NULL)
				{
					olText+="||"+olblank+olText2+CString(polOdaData2->Sdac);
				}
				// putting the text to the tooltip
				strcpy(pTTT->szText, olText);
			}
			
		}
	}
	return TRUE;
}

void ShiftRoster_View::HandleNSRTable(CString opBsdUrno, CString opPfcUrno, int ipNumber, int ipDay)
{
	BSDDATA			*polBsdData;
	PFCDATA			*polPfcData;
	NSRTABLEDATA	*polNsrTableData;

	CString			olNsrSearchCode;

	polBsdData = ogBsdData.GetBsdByUrno (atol(opBsdUrno));
	polPfcData = ogPfcData.GetPfcByUrno (atol(opPfcUrno));

	if (polBsdData != NULL)
	{
		// Pr�fen ob es schon einen Eintrag gibt
		olNsrSearchCode = omSortCode + polBsdData->Bsdc;
		if (polPfcData != NULL)
		{
			polNsrTableData = FindNSRTableGrupp (olNsrSearchCode, polPfcData->Fctc);
		}
		else
		{
			polNsrTableData = FindNSRTableGrupp (olNsrSearchCode);
		}

		// Anzahl addieren oder neuen Eintrag
		if (polNsrTableData != NULL)
		{
			AddOrSubIsDay (polNsrTableData, ipDay, 1, ipNumber);	// Eintrags Summe erh�hen
		}
		else
		{
			polNsrTableData = new NSRTABLEDATA;
			CString olText;
			CString olFctc;
			CString olEsbg(polBsdData->Esbg);
			olEsbg = olEsbg.Left(2) + ":" + olEsbg.Right(2);
			CString olLsen(polBsdData->Lsen);
			olLsen = olLsen.Left(2) + ":" + olLsen.Right(2);

			if (polPfcData != NULL)
			{
				olFctc = polPfcData->Fctc;
			}

			if (olFctc.GetLength() == 0)
			{
				olText = " (" + olEsbg + " - " + olLsen + ")";
			}
			else
			{
				olText = " (" + olFctc + ", " + olEsbg + " - " + olLsen + ")";
			}

			// Neue Zeile sowohl ins Array als auch in die Map eintragen
			AddOrSubIsDay (polNsrTableData, ipDay, 1, ipNumber);	// Eintrags Summe erh�hen
			polNsrTableData->Grupp	= omSortCode + polBsdData->Bsdc + olText;
			polNsrTableData->GruppUrno.Format("%d", polBsdData->Urno);
			polNsrTableData->Fctc	= olFctc;
			polNsrTableData->Type	= DETAIL;
			omNSRTableData.Add (polNsrTableData);

			omNSRTableDataMap.SetAt(omSortCode + polBsdData->Bsdc + olFctc, polNsrTableData);
		}
	}
}


//void ShiftRoster_View::GetWeekValues(CString &opString, int ipWeekNo, GSPTABLEDATA *popGspTableData, int ipShiftNo,BSR_PRINT_OPTIONS options)
void ShiftRoster_View::GetWeekValues(CString &opString, int ipWeekNo, GSPTABLEDATA *popGspTableData, int ipShiftNo,BSR_PRINT_OPTIONS options,BSR_PRINT_OPTIONS_SHIFT options_shift)

{
	BSDDATA *polBsdData;
	ODADATA *polOdaData;
	GSPTABLEDATA *polGspTableData = popGspTableData;
	opString = "";
	long llUrno;
	bool blWithSecondShift = false;

	if (polGspTableData != NULL)
	{
		CString olTmp;

		olTmp.Format("%d;%d",ipWeekNo + 1, ipShiftNo);

		opString = "<=" + olTmp + "=>";

		olTmp = "";

		for (int i = 0; i < 7; i++)
		{
			olTmp += "|";
			if (ipShiftNo == 1)
			{
				llUrno = atol(polGspTableData->DayUrno[i]);
			}
			else
			{
				llUrno = atol(polGspTableData->Day2Urno[i]);
			}
			polBsdData = ogBsdData.GetBsdByUrno (llUrno);
			if (polBsdData != NULL)
			{
				if (CString(polBsdData->Bewc) == "J" || bgPrintShiftrosterShiftcode)
				{
					olTmp += CString(polBsdData->Bsdc);
				}
				else
				{
					// the time from
					CString olEsbg(polBsdData->Esbg);
					olEsbg = olEsbg.Left(2) + ":" + olEsbg.Right(2);

					// the time to
					CString olLsen(polBsdData->Lsen);
					olLsen = olLsen.Left(2) + ":" + olLsen.Right(2);

					// the times together					

					switch(options_shift)
					{
						case BSR_PRINT_SHIFTCODE:							
							olTmp += CString (polBsdData->Bsdc);
							break;
						case BSR_PRINT_SHIFTTIME:
					    olTmp += olEsbg + "-" + olLsen;
							break;
					}				

				}

				// there was a second shift
				if (ipShiftNo == 2)
				{
					blWithSecondShift = true;
				}
			}
			else
			{
				polOdaData = ogOdaData.GetOdaByUrno (llUrno);
				if (polOdaData != NULL)
				{
					olTmp += CString(polOdaData->Sdac);

					// there was a second shift (absence)
					if (ipShiftNo == 2)
					{
						blWithSecondShift = true;
					}
				}
				else
				{
					olTmp += "-";
				}
			}
		}


		switch(options)
		{
		case BSR_PRINT_WORKGROUP:
			olTmp += "|" + polGspTableData->Houers + "|" + popGspTableData->Workgroup + "<=\\=>";
			break;
		case BSR_PRINT_FIRSTNAME:
			{
				CStringArray olUrnoList;
				::ExtractItemList(popGspTableData->StaffUrno,&olUrnoList,'|');
				if (olUrnoList.GetSize() > 0)
				{
					STFDATA *prlStf = ogStfData.GetStfByUrno(atol(olUrnoList[0]));
					if (prlStf != NULL)
					{
						olTmp += "|" + polGspTableData->Houers + "|" + this->GetFormatedEmployeeName(prlStf) + "<=\\=>";
					}
					else
					{
						olTmp += "|" + polGspTableData->Houers + "|<=\\=>";
					}
				}
				else
				{
					olTmp += "|" + polGspTableData->Houers + "|<=\\=>";
				}
				break;
			}
		default:
			olTmp += "|" + polGspTableData->Houers + "|<=\\=>";
		}

		olTmp = olTmp.Mid(1); //removing left comma
		opString += olTmp;

		if (ipShiftNo == 2 && !blWithSecondShift)
		{
			opString = "";
		}
	}
}


CString date;   

void ShiftRoster_View::OnShiftPrint() 
{
	int ilCount = omGSPTableData.GetSize();


	CCS_TRY;
	
	if (this->omCustomer == "SIN")
	{
		m_E_GPL_StPt.GetWindowText(date);
	
		
		ShiftPrintDlg olShiftPrintDlg(this,ilCount > 0 && imGSPwgpcIdx >= 0 ? true:false);
		olShiftPrintDlg.DoModal();
	}
	else if (ilCount > 0)
	{
		BSR_PRINT_OPTIONS option = BSR_PRINT_NONE;	
		BSR_PRINT_OPTIONS_SHIFT option_shift = BSR_PRINT_SHIFTTIME;	

		//--- look after RosteringPrint.exe: start it, if not started yet or pending
		CString olStartApp = pcgRosteringPrintPath;
		DutyPrintDlg::CheckStartApp(olStartApp);
		
		//--- look after the file
		CString olTmp;
		CString olTmpLine;
		CString olFileName = DutyPrintDlg::GetUniqueFileName("SP_");
		ofstream of;
		int ilPos;

		//--- open the file
		of.open(olFileName);

		//--- writing the name of the report to print
		switch(option)
		{
		case BSR_PRINT_WORKGROUP:
			olTmp = "SHIFTPLAN1";
			break;
		case BSR_PRINT_FIRSTNAME:
			olTmp = "SHIFTPLAN2";
			break;
		case BSR_PRINT_NONE:
			olTmp = "SHIFTPLAN";
			break;
		}

		of << "<=NAME=>" << olTmp << "<=\\=>" << endl;

		//--- getting the title, e.g. "Saisondienstplan Lader"
		m_E_GPL_Caption.GetWindowText(olTmp);
		ilPos = olTmp.Find ("URNO");
		if (ilPos > -1)
		{
			olTmp = olTmp.Left (ilPos);
			olTmp.TrimRight();
		}
		of << "<=TITLE=>" << olTmp << "<=\\=>" << endl;

		//--- getting the user, e.g. "UserXYZ"
		olTmp = pcgUser;
		of << "<=USER=>" << olTmp << "<=\\=>" << endl;

		//--- getting valid from, e.g. "01.04.2002"
		m_E_GPL_Vafr.GetWindowText(olTmp);
		of << "<=FROM=>" << olTmp << "<=\\=>" << endl;

		//--- getting valid to, e.g. "28.04.2002"
		m_E_GPL_Vato.GetWindowText(olTmp);
		of << "<=TO=>" << olTmp << "<=\\=>" << endl;

		//--- getting the organisation code, e.g. "Ladegruppen"
		if (ogBCD.GetFieldIndex("GPL","ORGU") > -1)
		{
			int ilSelItem = m_CB_OrgUnit.GetCurSel();
			if (ilSelItem != LB_ERR)
			{
				CString olTmpTxt;
				m_CB_OrgUnit.GetLBText(ilSelItem,olTmpTxt);
				int ilIndex = olTmpTxt.Find("URNO=");
				if (olTmpTxt.GetLength() > ( ilIndex + 5))
				{
					olTmp = olTmpTxt.Left(ilIndex);
					olTmp.TrimRight();
				}
				else
				{
					olTmp = "";
				}
			}
			else
			{
				olTmp = "";
			}
		}
		else
		{
			olTmp = "";	//not used in the shiftplanning yet (27.03.2002)
		}
		of << "<=ORGCODE=>" << olTmp << "<=\\=>" << endl;

		//--- getting the caption of the shiftplan, e.g. "Nr.,Mo.,Di.,Mi.,Do.,Fr.,Sa.,So.,Std."
		m_S_Week.GetWindowText(olTmp);
		olTmpLine = olTmp + "|";
		m_S_Mo.GetWindowText(olTmp);
		olTmpLine += olTmp + "|";
		m_S_Tu.GetWindowText(olTmp);
		olTmpLine += olTmp + "|";
		m_S_We.GetWindowText(olTmp);
		olTmpLine += olTmp + "|";
		m_S_Th.GetWindowText(olTmp);
		olTmpLine += olTmp + "|";
		m_S_Fr.GetWindowText(olTmp);
		olTmpLine += olTmp + "|";
		m_S_Sa.GetWindowText(olTmp);
		olTmpLine += olTmp + "|";
		m_S_Su.GetWindowText(olTmp);
		olTmpLine += olTmp + "|";
		m_S_Houers.GetWindowText(olTmp);
		olTmpLine += olTmp;
		of << "<=CAPTION=>" << olTmpLine << "<=\\=>" << endl;

		//--- getting the shiftplan-lines and writing out line by line
		for (int i = 0; i < ilCount; i++)
		{
			// erste Schichten
			//GetWeekValues(olTmpLine, i, &omGSPTableData[i], 1,option);
			GetWeekValues(olTmpLine, i, &omGSPTableData[i], 1,option,option_shift);
			of << olTmpLine << endl;

			// zweite Schichten
			//GetWeekValues(olTmpLine, i, &omGSPTableData[i], 2,option);
			GetWeekValues(olTmpLine, i, &omGSPTableData[i], 2,option,option_shift);
			if (olTmpLine.GetLength() > 0)
			{
				of << olTmpLine << endl;
			}
		}

		//--- close the file
		of.close();

		//--- look after RosteringPrint.exe: send filename to it, if started.
		DutyPrintDlg::SendFileName (olStartApp, olFileName);
	}
	CCS_CATCH_ALL;

}

void ShiftRoster_View::CheckRemoveStfus(RecordSet *popGSPRecord)
{
	CString olTmp = popGSPRecord->Values[imGSPstfuIdx];
	CStringArray olStrArr;

	CedaData::ExtractTextLineFast (olStrArr, olTmp.GetBuffer (0), "|");
	STFDATA *polStfData;
	for (int i = 0; i < olStrArr.GetSize(); i++)
	{
		polStfData = ogStfData.GetStfByUrno (atol(olStrArr[i]));
		if (polStfData == NULL)
		{
			int ilPos1;
			int ilPos2;
			ilPos1 = popGSPRecord->Values[imGSPstfuIdx].Find(olStrArr[i]);
			if (ilPos1 > -1)
			{
				ilPos2 = popGSPRecord->Values[imGSPstfuIdx].Find('|', ilPos1);
				if (ilPos2 < 0)
				{
					if (ilPos1 > 0)
					{
						popGSPRecord->Values[imGSPstfuIdx].Replace ("|" + olStrArr[i], "");
					}
					else
					{
						popGSPRecord->Values[imGSPstfuIdx].Replace (olStrArr[i], "");
					}
				}
				else
				{
					popGSPRecord->Values[imGSPstfuIdx].Replace (olStrArr[i] + "|", "");
				}
			}
		}
	}
}
/*
ShiftAWDlg *ShiftRoster_View::GetShiftAWDlg()
{
	ShiftAWDlg *pView = NULL;

	CWinApp *pApp = AfxGetApp();
	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL && pView == NULL)
	{
		CDocTemplate *pDocTempl = pApp->GetNextDocTemplate(pos);
		POSITION p1 = pDocTempl->GetFirstDocPosition();
		while(p1 != NULL && pView == NULL)
		{
			CDocument *pDoc = pDocTempl->GetNextDoc(p1);
			POSITION p2 = pDoc->GetFirstViewPosition();
			while (p2 != NULL && pView == NULL)
			{
				pView = DYNAMIC_DOWNCAST(ShiftAWDlg, pDoc->GetNextView(p2));
			}
		}
	}

	return pView;
}
*/
void ShiftRoster_View::OnMenuAddLineToGSP()
{
	if (imClickedLine > 0)
	{
		// create the new record (effects GSPTAB)
		GSPTABLEDATA *prlGplData = new GSPTABLEDATA;
		CString olTmp;
		olTmp.Format("%03d", imClickedLine);
		prlGplData->Week		= olTmp;
		prlGplData->DisplayWeek	= olTmp;	//$$
		prlGplData->Number		= "1";
		prlGplData->IsChanged	= RECORDCHANGED;

		// "imClickedLine" starts with 1, the array with 0
		omGSPTableData.InsertAt (imClickedLine - 1, prlGplData);

		// update the records 'behind' the inserted one: increase the week-number
		int ilMaxWeek = omGSPTableData.GetSize();
		int ilActWeek;
		int ilDisWeek;
		for (int i = imClickedLine; i < ilMaxWeek; i++)
		{
			ilActWeek = atoi(omGSPTableData[i].Week);
			ilActWeek++;
			omGSPTableData[i].Week.Format("%03d", ilActWeek);

			ilDisWeek = atoi(omGSPTableData[i].DisplayWeek);
			ilDisWeek++;
			omGSPTableData[i].DisplayWeek.Format("%03d", ilDisWeek);

			omGSPTableData[i].IsChanged = RECORDCHANGED;
		}

		// update the toolbar button if necessary
		if (!bmIsChangedGPL)
		{
			bmIsChangedGPL = true;
			((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);
		}

		// increase the period (effects GPLTAB) and handling of the spinbutton
		im_SBC_Periode++;
	
		
		
		olTmp.Format("%01d", im_SBC_Periode * 7);

		
		m_E_GPL_Peri.SetInitText(olTmp);
		m_SBC_Periode.SetPos(im_SBC_Periode);

		// handling the scrollbar range
		int ilSize = omGSPTableData.GetSize();
		imGSPRangeMax = ilSize - imGSPLines;
		if (imGSPRangeMax < 0) imGSPRangeMax = 0;
		m_SB_GSP.SetScrollRange(imGSPRangeMin,imGSPRangeMax,false);
		if(imGSPRangeMax > 0)
		{
			m_SB_GSP.EnableScrollBar(ESB_ENABLE_BOTH);
		}
		else
		{
			m_SB_GSP.EnableScrollBar(ESB_DISABLE_BOTH);
		}

		//imActualLine = imGSPRangeMax;
		DisplayShiftRosterData(m_SB_GSP.GetScrollPos());

		rmCodeNumberData.NrOfWeeks++; // CodeNumber
		rmCodeNumberData.MaSchedule++; // CodeNumber
		if(omSelSPLurno == omActualSPLurnoByGPL)
		{
			rmCodeNumberData.MaScheduleAll++; // CodeNumber
		}

		if (IsCodeDlgActive())
		{
			pomShiftCodeNumberDlg->SetValues(rmCodeNumberData);
			pomShiftCodeNumberDlg->SetWeeks(omGSPTableData.GetSize());
			pomShiftCodeNumberDlg->SetDataArray(omGSPTableData);
			pomShiftCodeNumberDlg->ReNewAll();
		}
	}
	imClickedLine = -1;
}

void ShiftRoster_View::OnMenuDeleteLineFromGSP()
{
	if (imClickedLine > 0 && (imClickedLine-1) < omGSPTableData.GetSize())
	{

		int ilMBoxReturn;
		CString olMsg;
		CString olWeek;
		olWeek.Format("%03d", imClickedLine);
		// "Sind Sie sicher, dass Sie Zeile %d l�schen wollen?"
		olMsg = LoadStg(IDS_STRING63502);
		olMsg.Replace("%d", olWeek);
		ilMBoxReturn = MessageBox(olMsg, LoadStg(IDS_INFO), MB_ICONQUESTION | MB_YESNO);

		if (ilMBoxReturn == IDYES)
		{
			GSPTABLEDATA *prlGspTableData = &omGSPTableData[imClickedLine-1];
			CString olTmpTxt;
			int ilSize = omGSPTableData.GetSize();
			int ilOldNumber = atoi(prlGspTableData->Number);

			//
			//-- CodeNumber Start --
			//
			BSDDATA* prlBsdData;
			int ilHouers = 0;
			int ilStaffs = 0;
			CString olUrno;

			// Stunden und Mitarbeiter abziehen
			for (int ilDay = 0; ilDay < 7; ilDay++)
			{
				// erste Schicht
				olUrno = prlGspTableData->DayUrno[ilDay];
				if(olUrno != "" && olUrno != "0" && ogBsdData.GetBsdByUrno(atol(olUrno)) != NULL)
				{
					rmCodeNumberData.Work--;
					prlBsdData = ogBsdData.GetBsdByUrno(atol(olUrno));
					if (prlBsdData != NULL)
					{
						ilHouers += atoi(prlBsdData->Sdu1);
					}
				}

				// zweite Schicht
				olUrno = prlGspTableData->Day2Urno[ilDay];
				if(olUrno != "" && olUrno != "0" && ogBsdData.GetBsdByUrno(atol(olUrno)) != NULL)
				{
					rmCodeNumberData.Work--;
					prlBsdData = ogBsdData.GetBsdByUrno(atol(olUrno));
					if (prlBsdData != NULL)
					{
						ilHouers += atoi(prlBsdData->Sdu1);
					}
				}
			}

			ilStaffs = GetItemCount(prlGspTableData->StaffUrno, '|');

			rmCodeNumberData.MaSchedule -= ilOldNumber;
			rmCodeNumberData.MaAssign	-= ilStaffs;
			rmCodeNumberData.HouersGSP  -= ilHouers;

			if(omSelSPLurno == omActualSPLurnoByGPL)
			{
				rmCodeNumberData.MaScheduleAll	-= ilOldNumber;
				rmCodeNumberData.HouersAll		-= ilHouers;
				rmCodeNumberData.MaAssignAll	-= ilStaffs;
			}
			//
			//-- CodeNumber End --
			//

			//*******************************************************************
			//-- Add in Array to change  the Shift Requirement Data --
			if(omSelSPLurno == omActualSPLurnoByGPL)
			{
				CHANGEGSPDATA *polChangedGSPs;

				for (int ilDay = 0; ilDay < 7; ilDay++)
				{
					polChangedGSPs = new CHANGEGSPDATA;
					polChangedGSPs->Day			= ilDay + 1;
					polChangedGSPs->OldNumber	= ilOldNumber;
					polChangedGSPs->OldBSDCode	= prlGspTableData->Day[ilDay];
					polChangedGSPs->OldBSDUrno	= prlGspTableData->DayUrno[ilDay];
					polChangedGSPs->Old2BSDCode	= prlGspTableData->Day2[ilDay];
					polChangedGSPs->Old2BSDUrno	= prlGspTableData->Day2Urno[ilDay];
					polChangedGSPs->OldPfcCode  = prlGspTableData->PfcCode[ilDay];
					polChangedGSPs->OldPfcUrno  = prlGspTableData->PfcUrno[ilDay];
					polChangedGSPs->OldPfc2Code = prlGspTableData->PfcCode2[ilDay];
					polChangedGSPs->OldPfc2Urno = prlGspTableData->PfcUrno2[ilDay];
					polChangedGSPs->NewNumber	= 0;
					omChangedGSPs.Add(polChangedGSPs);
				}

				ChangeShiftRequirementData(WEEK);
				DeleteNullFromShiftRequirementData();
				ChangeShiftRequirementBonusData();
				ChangeShiftRequirementGruppData();
				DisplayShiftRequirementData(2);
			}
			
			//*******************************************************************
			// omDeletedGSPs wird mit neuer Zeile gef�llt
			// beim sichern wird diese Zeile dann aus der Datenbank gel�scht
			
			GSPTABLEDATA *prlGspData = new GSPTABLEDATA;
			
			prlGspData->Week		= prlGspTableData->Week;
			prlGspData->DisplayWeek	= prlGspTableData->DisplayWeek;
			prlGspData->GSPUrno		= prlGspTableData->GSPUrno;
			prlGspData->Number		= prlGspTableData->Number;
			prlGspData->StaffUrno	= prlGspTableData->StaffUrno;
			prlGspData->StaffFLName	= prlGspTableData->StaffFLName;
			prlGspData->StaffFree	= prlGspTableData->StaffFree;
			prlGspData->StaffCode	= prlGspTableData->StaffCode;
			prlGspData->Houers		= prlGspTableData->Houers;
			prlGspData->IsChanged	= prlGspTableData->IsChanged;

			for (ilDay = 0; ilDay < 7; ilDay++)
			{
				// Erste Schicht
				prlGspData->Day[ilDay]		= prlGspTableData->Day[ilDay];
				prlGspData->DayUrno[ilDay]	= prlGspTableData->DayUrno[ilDay];
				prlGspData->PfcCode[ilDay]	= prlGspTableData->PfcCode[ilDay];
				prlGspData->PfcUrno[ilDay]	= prlGspTableData->PfcUrno[ilDay];
				// Zwote Schicht
				prlGspData->Day2[ilDay]		= prlGspTableData->Day2[ilDay];
				prlGspData->Day2Urno[ilDay]	= prlGspTableData->Day2Urno[ilDay];
				prlGspData->PfcCode2[ilDay]	= prlGspTableData->PfcCode2[ilDay];
				prlGspData->PfcUrno2[ilDay]	= prlGspTableData->PfcUrno2[ilDay];
			}
			
			if (prlGspData->GSPUrno != "" && prlGspData->GSPUrno != "0")
			{
				omDeletedGSPs.Add(prlGspData);
			}
			else
			{
				delete prlGspData;
			}
			
			//*******************************************************************
			
			// L�schen aus der eigendlichen Datenhaltung
			omGSPTableData.DeleteAt(imClickedLine - 1);
			ilSize = omGSPTableData.GetSize();
			// update the toolbar button if necessary
			if (!bmIsChangedGPL)
			{
				bmIsChangedGPL = true;
				((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);
			}

			// update the records 'behind' the inserted one: decrease the week-number
			int ilActWeek;
			int ilDisWeek;
			for (int i = imClickedLine - 1; i < ilSize; i++)
			{
				ilActWeek = atoi(omGSPTableData[i].Week);
				ilActWeek--;
				omGSPTableData[i].Week.Format("%03d", ilActWeek);

				ilDisWeek = atoi(omGSPTableData[i].DisplayWeek);
				ilDisWeek--;
				omGSPTableData[i].DisplayWeek.Format("%03d", ilDisWeek);

				omGSPTableData[i].IsChanged = RECORDCHANGED;
			}

			// increase the period (effects GPLTAB) and handling of the spinbutton
			im_SBC_Periode--;
			olTmpTxt.Format("%01d", im_SBC_Periode * 7);
			m_E_GPL_Peri.SetInitText(olTmpTxt);
			m_SBC_Periode.SetPos(im_SBC_Periode);

			imGSPRangeMax = ilSize - imGSPLines;
			if (imGSPRangeMax < 0)
			{
				imGSPRangeMax = 0;
			}
			m_SB_GSP.SetScrollRange(imGSPRangeMin,imGSPRangeMax,false);
			//m_SB_GSP.SetScrollPos(imGSPRangeMax);
			if(imGSPRangeMax > 0)
			{
				m_SB_GSP.EnableScrollBar(ESB_ENABLE_BOTH);
			}
			else
			{
				m_SB_GSP.EnableScrollBar(ESB_DISABLE_BOTH);
			}
			
			//imActualLine = imGSPRangeMax;
			DisplayShiftRosterData(m_SB_GSP.GetScrollPos());

			rmCodeNumberData.NrOfWeeks--; // CodeNumber
			if (IsCodeDlgActive())
			{
				pomShiftCodeNumberDlg->SetValues(rmCodeNumberData);
				pomShiftCodeNumberDlg->SetWeeks(omGSPTableData.GetSize());
				pomShiftCodeNumberDlg->SetDataArray(omGSPTableData);
				pomShiftCodeNumberDlg->ReNewAll();
			}
		}
	}
}

void ShiftRoster_View::OnMenuCancelAction()
{
	imClickedLine = -1;
}

void ShiftRoster_View::OnChangeView()
{
	OnSelchangeCbView();

}

void ShiftRoster_View::ProcessBCs(int ipDDXType, void *vpDataPointer)
{
	switch (ipDDXType) 
	{
		case SDG_DELETE:
		case SDG_CHANGE:
		case SDG_NEW:
ogBackTrace += ",2_1";
			ProcessSdgBc(ipDDXType, vpDataPointer);
			OnChangeView();
			break;
		case MSD_DELETE:
		case MSD_NEW:
		case MSD_CHANGE:
			GetShiftRequirementData(SHIFTDEMAND, 2); //2 is the hint to scroll to the old scroll position
			break;
		case RELMSD:
		{
			SDGDATA *prlSdgData = (SDGDATA*)vpDataPointer;

			// Position der Auswahl bestimmen.
			int ilTmp = pomComboBoxCoverage->GetCurSel();
			if (ilTmp != CB_ERR && prlSdgData != NULL)
			{
				CString olTmp;
				pomComboBoxCoverage->GetLBText(ilTmp, olTmp);
				olTmp = olTmp.Left(32);
				olTmp.TrimRight();

				// Testen ob der Text g�ltig ist
				if (olTmp != "")
				{
					if (olTmp == CString(prlSdgData->Dnam))
					{
						OnSelchangeCbRequirement();
					}
				}
			}
			break;
		}
	}
}

void ShiftRoster_View::ProcessSdgBc(int ipDDXType, void *vpDataPointer)
{
	if (pomComboBoxCoverage != NULL)
	{
ogBackTrace += ",2_2";	
		SDGDATA *prlSdgData = NULL;
		CString olSdgDnam;
		CString olLine;
		int ilCount;
		int ilSelItem = pomComboBoxCoverage->GetCurSel();

		if(ilSelItem != CB_ERR)
		{
ogBackTrace += ",2_3";
			pomComboBoxCoverage->GetLBText(ilSelItem, olSdgDnam);
		}

		pomComboBoxCoverage->ResetContent();
		pomComboBoxCoverage->AddString("");

		ilCount = ogSdgData.omData.GetSize();
ogBackTrace += ",2_4";
		for (int i = 0; i < ilCount; i++)
		{
			prlSdgData = &ogSdgData.omData[i];
			if (strcmp(prlSdgData->Days,"7") == 0 && prlSdgData->IsChanged != DATA_DELETED)
			{
				olLine.Format("%-32s                                    URNO=%ld",prlSdgData->Dnam, prlSdgData->Urno);
				if(pomComboBoxCoverage->FindStringExact(-1,olLine) == CB_ERR)
				{
					pomComboBoxCoverage->AddString(olLine);
				}
			}
		}
ogBackTrace += ",2_5";
		pomComboBoxCoverage->SelectString(0, olSdgDnam);
	}
}

bool ShiftRoster_View::IsVisibleGplActive()
{
	bool blRet = false;
	CString olTmp;
	CString olGplUrno;

	for (int i = 0; i < m_LB_GPLList.GetCount(); i++)
	{
		m_LB_GPLList.GetText(i, olTmp);
		if (olTmp.Find("URNO=") > -1)
		{
			olGplUrno = olTmp.Mid(olTmp.Find("URNO=") + 5);
		}
		else
		{
			olGplUrno = "0";
		}
		if (olGplUrno == omSelGPLurno)
		{
			olTmp = olTmp.Left(1);

			if (olTmp == omAktiv)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	return blRet;
}

CString ShiftRoster_View::GetSelectedShiftplanUrno()
{
	CString olSPLTxt;
	CString olSPLurno = "0";
	int ilSelSPLItem = pomComboBoxShiftplan->GetCurSel();
	if (ilSelSPLItem != CB_ERR)
	{
		pomComboBoxShiftplan->GetLBText(ilSelSPLItem,olSPLTxt);
	}

	if (ilSelSPLItem != CB_ERR && olSPLTxt != "")
	{
		if (olSPLTxt.GetLength() > (olSPLTxt.Find("URNO=") + 5))
		{
			olSPLurno = olSPLTxt.Mid(olSPLTxt.Find("URNO=") + 5);
		}
	}
	return olSPLurno;
}

CString ShiftRoster_View::GetActiveBasicShiftRoster()
{
	CString olGPLurno = "0";

	CString olTmpTxt;
	m_E_GPL_Caption.GetWindowText(olTmpTxt);
	olTmpTxt.TrimRight();

	if (olTmpTxt.GetLength() > (olTmpTxt.Find("URNO=")+5))
	{
		olGPLurno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
	}	

	return olGPLurno;
}

int ShiftRoster_View::GetStaffUrnosBySplUrno(CString opSplu, CMapStringToPtr *ropStfuMap)
{
	CString olSPLurno;
	CString olGPLUrno;

	int	ilGPLCount = ogBCD.GetDataCount("GPL");
	int	ilGSPCount = ogBCD.GetDataCount("GSP");

	int ilGPLspluIdx = ogBCD.GetFieldIndex("GPL","SPLU");
	int ilGPLurnoIdx = ogBCD.GetFieldIndex("GPL","URNO");
	int ilGSPgpluIdx = ogBCD.GetFieldIndex("GSP","GPLU");
	int ilGSPstfuIdx = ogBCD.GetFieldIndex("GSP","STFU");

	RecordSet *polGPLRecord = new RecordSet(ogBCD.GetFieldCount("GPL"));
	RecordSet *polGSPRecord = new RecordSet(ogBCD.GetFieldCount("GSP"));

	// getting the urno of the actual selected shiftplan
	olSPLurno = GetSelectedShiftplanUrno();

	// loop through all GPLs (Basic Shift Plans) to find the ones of the shift plan
	for (int i = 0; i < ilGPLCount; i++)
	{
		ogBCD.GetRecord("GPL",i, *polGPLRecord);

		if (strcmp((polGPLRecord->Values[ilGPLspluIdx]).GetBuffer(0), olSPLurno.GetBuffer(0)) == 0)
		{
			olGPLUrno = polGPLRecord->Values[ilGPLurnoIdx];

			for (int j = 0; j < ilGSPCount; j++)
			{
				ogBCD.GetRecord("GSP",j, *polGSPRecord);

				if (strcmp((polGSPRecord->Values[ilGSPgpluIdx]).GetBuffer(0), olGPLUrno.GetBuffer(0)) == 0)
				{
					CStringArray olStaffArray;
					int ilSaffSize = ExtractItemList(polGSPRecord->Values[ilGSPstfuIdx], &olStaffArray, '|');
					for (int ilStaff = 0; ilStaff < ilSaffSize; ilStaff++)
					{
						ropStfuMap->SetAt(olStaffArray[ilStaff],(void*)i);
					}
				}
			}
		}
	}

	delete polGPLRecord;
	delete polGSPRecord;

	return ropStfuMap->GetCount();
}


int ShiftRoster_View::GetStaffUrnosByGplUrno(CString opGPLurno, CMapStringToPtr *ropStfuMap)
{
	CString olGPLUrno;

	int	ilGPLCount = ogBCD.GetDataCount("GPL");
	int	ilGSPCount = ogBCD.GetDataCount("GSP");

	int ilGPLurnoIdx = ogBCD.GetFieldIndex("GPL","URNO");
	int ilGSPgpluIdx = ogBCD.GetFieldIndex("GSP","GPLU");
	int ilGSPstfuIdx = ogBCD.GetFieldIndex("GSP","STFU");

	RecordSet *polGPLRecord = new RecordSet(ogBCD.GetFieldCount("GPL"));
	RecordSet *polGSPRecord = new RecordSet(ogBCD.GetFieldCount("GSP"));

	// loop through all GPLs (Basic Shift Plans) to find the active one
	for (int i = 0; i < ilGPLCount; i++)
	{
		ogBCD.GetRecord("GPL",i, *polGPLRecord);

		if (strcmp((polGPLRecord->Values[ilGPLurnoIdx]).GetBuffer(0), opGPLurno.GetBuffer(0)) == 0)
		{
			for (int j = 0; j < ilGSPCount; j++)
			{
				ogBCD.GetRecord("GSP",j, *polGSPRecord);

				if (strcmp((polGSPRecord->Values[ilGSPgpluIdx]).GetBuffer(0), opGPLurno.GetBuffer(0)) == 0)
				{
					CStringArray olStaffArray;
					int ilSaffSize = ExtractItemList(polGSPRecord->Values[ilGSPstfuIdx], &olStaffArray, '|');
					for (int ilStaff = 0; ilStaff < ilSaffSize; ilStaff++)
					{
						ropStfuMap->SetAt(olStaffArray[ilStaff],NULL);
					}
				}
			}
		}
	}

	delete polGPLRecord;
	delete polGSPRecord;

	return ropStfuMap->GetCount();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Zeitraum der zu evaluierenden Daten ermitteln: 
// minimalen Start (GPL.VAFR) und maximales Ende (GPL.VATO) der GPL-Datens�tze
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool ShiftRoster_View::GetMinMaxGplTimesBySplu(CString opSPLurno, COleDateTime *ropFrom, COleDateTime *ropTo)
{
	CTime olLoadFrom	= -1;
	CTime olLoadTo		= -1;
	CTime olTempFrom;
	CTime olTempTo;
	int ilGPLCount		=  ogBCD.GetDataCount("GPL");
	int ilGPLvafrIdx	= ogBCD.GetFieldIndex("GPL","VAFR");
	int ilGPLvatoIdx	= ogBCD.GetFieldIndex("GPL","VATO");
	int ilGPLspluIdx	= ogBCD.GetFieldIndex("GPL","SPLU");
	CString olTmp;
	RecordSet *polGPLRecord = new RecordSet(ogBCD.GetFieldCount("GPL"));

	// search all GPL's to the actuall SPL-Urno, to find min beginn and max end
	for (int i = 0; i < ilGPLCount; i++)
	{
		ogBCD.GetRecord("GPL",i, *polGPLRecord);
		if (polGPLRecord->Values[ilGPLspluIdx] == opSPLurno)
		{
			olTempFrom	= -1;
			olTempTo	= -1;
			olTmp = polGPLRecord->Values[ilGPLvafrIdx];
			olTempFrom = DBStringToDateTime(olTmp);
			olTmp = polGPLRecord->Values[ilGPLvatoIdx];
			olTempTo = DBStringToDateTime(olTmp);

			if (olTempFrom < olLoadFrom || olLoadFrom == -1)
			{
				olLoadFrom = olTempFrom;
			}
			if (olTempTo > olLoadTo || olLoadTo == -1)
			{
				olLoadTo = olTempTo;
			}
		}
	}

	if (olLoadFrom != -1 && olLoadTo != -1)
	{
		// Zeitraum in COleDateTime-Instanzen speichern
		ropFrom->SetDateTime(olLoadFrom.GetYear(), olLoadFrom.GetMonth(), olLoadFrom.GetDay(), olLoadFrom.GetHour(), olLoadFrom.GetMinute(), olLoadFrom.GetSecond());
		ropTo->SetDateTime(olLoadTo.GetYear(), olLoadTo.GetMonth(), olLoadTo.GetDay(), olLoadTo.GetHour(), olLoadTo.GetMinute(), olLoadTo.GetSecond());
	}

	delete polGPLRecord;

	if ((ropFrom->GetStatus() != COleDateTime::valid) || (ropTo->GetStatus() != COleDateTime::valid))
	{
		return false;
	}
	else
	{
		return true;
	}
}

NameConfigurationDlg* ShiftRoster_View::GetNamesConfigurationDlg()
{
	return pomNameConfigDlg;
}

CString ShiftRoster_View::GetFormatedEmployeeName(STFDATA *prpStfData)
{
	CString olRet = "???";
	if (prpStfData != NULL)
	{
		if (imViewShowEmployee == VIEW_CODE)
		{
			olRet = prpStfData->Perc;
		}
		else if (imViewShowEmployee == VIEW_FREE)
		{
			olRet = CBasicData::GetFormatedEmployeeName(4, prpStfData, "", "", pomNameConfigDlg);
		}
		else
		{
			CString olVName = prpStfData->Finm;
			CString olNName = prpStfData->Lanm;
			olRet.Format("%s. %s",olVName.Left(1), olNName);
		}
	}
	return olRet;
}

void ShiftRoster_View::OnChangeEGplFilter() 
{
	CString olFilterText;
	GetDlgItem(IDC_E_GPL_FILTER)->GetWindowText(olFilterText);
	olFilterText.MakeUpper();

	Build_LB_GPL(olFilterText);
}

void ShiftRoster_View::OnBStPt()
{
	if (omSelGPLurno == "")
		return;

	int ilMBoxReturn = IDYES;
	
	if(bmIsChangedGPL == true)
	{
		ilMBoxReturn = MessageBox(LoadStg(SHIFT_ISCHANGED_GPL),LoadStg(ST_FRAGE),(MB_ICONQUESTION | MB_YESNOCANCEL | MB_DEFBUTTON1));
		
		switch (ilMBoxReturn) 
		{
		case IDYES:
			SaveGPL();
			break;
		case IDNO:
			bmIsChangedGPL = false;
			break;
		case IDCANCEL:
			return;
		}
	}

	CString olStPt;
	this->m_E_GPL_StPt.GetWindowText(olStPt);

	CString olVafr;
	this->m_E_GPL_Vafr.GetWindowText(olVafr);

	CString olVato;
	this->m_E_GPL_Vato.GetWindowText(olVato);

	CSelectDateDlg olSelStPtDlg(this);
	COleDateTime olGPLstpt = OleDateStringToDate(olStPt);

	olSelStPtDlg.SetDate(olGPLstpt);
	COleDateTime olGPLVafr = OleDateStringToDate(olVafr);
	COleDateTime olGPLVato = OleDateStringToDate(olVato);
	olSelStPtDlg.SetRange(olGPLVafr,olGPLVato);
	olSelStPtDlg.SetValidWeekDay(2);
   
	
	if (olSelStPtDlg.DoModal() == IDOK)
	{
		olGPLstpt  = olSelStPtDlg.GetDate();	 		
		COleDateTimeSpan olDiff = olGPLstpt - olGPLVafr;
		imStPtWeek = (int)olDiff.GetTotalDays() / 7 + 1;
		this->m_E_GPL_StPt.SetInitText(olGPLstpt.Format("%d.%m.%Y"));	
		GetShiftRosterData(this->omSelGPLurno);
		((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE); // red color
		bmIsChangedGPL = true;
		
	}

}

int ShiftRoster_View::RotateShiftRoster(int ipCurrentWeek,int ipStartWeek,int ipNoOfWeeks)
{
	for (int i = 1; i < ipStartWeek; i++)
	{
		if (ipCurrentWeek < ipNoOfWeeks)
			++ipCurrentWeek;
		else
			ipCurrentWeek = 1;
	}

	return ipCurrentWeek;
}


void ShiftRoster_View::FillWorkGroupComboBox(CComboBox *popComboBox)
{
	popComboBox->AddString("");

	for (int i = 0; i < ogWgpData.omData.GetSize(); i++)
	{
		WGPDATA &polWgpData = ogWgpData.omData[i];
		popComboBox->AddString(polWgpData.Wgpc);
	}
}

void ShiftRoster_View::OnSelchangeCbWorkgroup(UINT nId)
{
	CCS_TRY;

	CComboBox *polCtrl = (CComboBox *)this->GetDlgItem(nId);
	if (polCtrl == NULL)
		return;

	GSPTABLEPTR *prlGSPTablePtr = FindGSPTablePtr(polCtrl);

	if (prlGSPTablePtr != NULL)
	{
		if(prlGSPTablePtr->Record == -1)	// nothing to do ?
		{
			polCtrl->SetCurSel(-1);
		}
		else
		{
			CHANGEGSPDATA *polChangedGSPs = new CHANGEGSPDATA;
			polChangedGSPs->Record = prlGSPTablePtr->Record;	// only int's, => weekcount
			
			CString olNewWorkGroup;

			if (polCtrl->GetCurSel() != CB_ERR)
			{
				polCtrl->GetLBText(polCtrl->GetCurSel(),olNewWorkGroup);
			}

			polChangedGSPs->OldWorkgroup = prlGSPTablePtr->Workgroup;
			prlGSPTablePtr->Workgroup = olNewWorkGroup;

			polChangedGSPs->NewWorkgroup = prlGSPTablePtr->Workgroup;
			omGSPTableData[prlGSPTablePtr->Record].Workgroup = prlGSPTablePtr->Workgroup;

			// check on change
			if (polChangedGSPs->OldWorkgroup != prlGSPTablePtr->Workgroup)
			{
				omGSPTableData[prlGSPTablePtr->Record].IsChanged |= STAFFCHANGED;
				// COLOR
				((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);
					
				bmIsChangedGPL = true;

				delete polChangedGSPs;
			}
			else
			{
				delete polChangedGSPs;
			}

		}
	}

	CCS_CATCH_ALL;

}


void ShiftRoster_View::DoRelease(CString opReleaseString)
{
	CedaRelData olRelData;
	olRelData.MakeCedaString(opReleaseString);
	opReleaseString.Replace('\227', '\174');
	char *pclReleaseString = (char*) malloc(sizeof(char) + opReleaseString.GetLength()+2);

	if (pclReleaseString != NULL)
	{
		strcpy(pclReleaseString, opReleaseString);
		
		olRelData.CedaAction("RSR","NOBC,NOACTION,NOLOG", "", pclReleaseString, "");
		free(pclReleaseString);
	}
}

bool ShiftRoster_View::ServerReleaseAvailable()
{
	static bool blFirst = true;
	static bool blState = false;

	if (blFirst)
	{
		blFirst = false;
/***
	CedaRelData olRelData;
	return olRelData.CedaAction("RSR","[CHECK_AVAILABLE]", "","","BUF1");
****/
		CedaData olCedaData;
		blState = olCedaData.CedaAction("RSR","REL","","[CHECK_AVAILABLE]" , "","", "BUF1");
	}

	return blState;
}


//Auto Rostering 

void ShiftRoster_View::OnBAutoPopulate() 
{
	//Dialogue
	int ilPRD= 0; // Parameter input rest day
	int ilPWeek = 0; // Parameter input week

	AutoRosteringDlg  *autoDialog  = new AutoRosteringDlg(this); 
	if (autoDialog->DoModal() == IDOK)
	{
		ilPRD = autoDialog->imRestDay;
		ilPWeek = autoDialog->imWeek;
	}
	else
	{
		return;
	}

	int ilTotalAM = 0;
	int ilTotalPM = 0;
	int ilSum = 0;
	int ilMax = 0;
	float ilWorkDays = 0.0;
	int ilDay =0;
	int ilWeek= 0;
	int ilRD = 0;
	int ilDailyRD[7];
	int ilDailyWD[7];
	int ilSC = 0;	
	bool blFound = FALSE;
	float ilAve = 0;

	omGSPTableData.DeleteAll();
	ResetShiftRequirmentData();
	//omSCGrpMap.RemoveAll();

	///////////////////////////////////////////////////////////
	CString slTemp ;
	CString slSCode;
	CString slSDCode;
	CCSPtrArray<SHIFTDATA>	olAMShiftData;
	CCSPtrArray<SHIFTDATA>	olPMShiftData;
	bool blReqFound = FALSE;

	//int ilEarliest
	///CMapPtrToPtr omSCGrpMap;
	SHIFTDATA * prlShiftData; 
	for( ilSC=1; ilSC < omNSRTableData.GetSize(); ilSC++ )
	{
		if(omNSRTableData[ilSC].Type != DETAIL)
		{
			continue;
		}
		char clTemp[2];
		for( ilDay = 0; ilDay < 7; ilDay++ )
		{
			prlShiftData = new SHIFTDATA();
			
			switch (ilDay)
			{
				case 0:
					if( omNSRTableData[ilSC].MonDebit > omNSRTableData[ilSC].MonIs )
					{
						prlShiftData->RCount =  omNSRTableData[ilSC].MonDebit;
						blReqFound = TRUE;
					}
					break;
				case 1:
					if( omNSRTableData[ilSC].TueDebit > omNSRTableData[ilSC].TueIs )
					{						
						prlShiftData->RCount =  omNSRTableData[ilSC].TueDebit;
						blReqFound = TRUE;
					}
					break;
				case 2:
					if( omNSRTableData[ilSC].WndDebit > omNSRTableData[ilSC].WndIs )
					{
						prlShiftData->RCount =  omNSRTableData[ilSC].WndDebit;
						blReqFound = TRUE;
					}
					break;
				case 3:
					if( omNSRTableData[ilSC].ThuDebit > omNSRTableData[ilSC].ThuIs )
					{
						prlShiftData->RCount =  omNSRTableData[ilSC].ThuDebit;
						blReqFound = TRUE;
						
					}
					break;
				case 4:
					if( omNSRTableData[ilSC].FriDebit > omNSRTableData[ilSC].FriIs )
					{
						prlShiftData->RCount =  omNSRTableData[ilSC].FriDebit;
						blReqFound = TRUE;
					
					}
					break;
				case 5:
					if( omNSRTableData[ilSC].SatDebit > omNSRTableData[ilSC].SatIs )
					{
						prlShiftData->RCount =  omNSRTableData[ilSC].SatDebit;
						blReqFound = TRUE;
					}
					break;
				case 6:
					if( omNSRTableData[ilSC].SunDebit > omNSRTableData[ilSC].SunIs )
					{
						prlShiftData->RCount =  omNSRTableData[ilSC].SunDebit;
						blReqFound = TRUE;			
					}
					break;		
				default: 
					break;
			}

    		if(blReqFound)
			{
				slSCode = omNSRTableData[ilSC].Grupp.Mid(1,8);
				slSDCode = slSCode + itoa(ilDay,clTemp,10);
				prlShiftData->SCode = slSCode;
				prlShiftData->SDCode= slSDCode;
				prlShiftData->SType = GetShiftType(slSCode);
				prlShiftData->Day = ilDay;	
				prlShiftData->NSRTableData = &omNSRTableData[ilSC];
			}
			
			if(prlShiftData->Day >-1 && prlShiftData->RCount>0)
			{
				char temp[2];
				char temp1[2];
				if(prlShiftData->SType<12)
				{ 
					ilTotalAM	+= prlShiftData->RCount;
					olAMShiftData.Add(prlShiftData);
				}
				else
				{ 
					ilTotalPM += prlShiftData->RCount;
					olPMShiftData.Add(prlShiftData);
				}

				//omShiftDataMap.SetAt(slSDCode,prlShiftData);
				//int tempCnt = GetReqCountByShiftGrp(itoa(prlShiftData->SType,temp,10))+ prlShiftData->RCount;
				//omSCGrpMap.SetAt(itoa(prlShiftData->SType,temp,10),itoa(tempCnt,temp1,10));
				slTemp += prlShiftData->SDCode+ " || " + itoa(prlShiftData->SType,temp,10)+ " || " + itoa(prlShiftData->RCount,temp1,10) + "\n";
			}
		}

	}
	
//	MessageBox(slTemp,NULL,MB_OK);
	//////////////////////////////////////////////////

	ilDailyWD[0] = omNSRTableData[0].MonDebit;
	ilDailyWD[1] = omNSRTableData[0].TueDebit;
	ilDailyWD[2] = omNSRTableData[0].WndDebit;
	ilDailyWD[3] = omNSRTableData[0].ThuDebit;
	ilDailyWD[4] = omNSRTableData[0].FriDebit;
	ilDailyWD[5] = omNSRTableData[0].SatDebit;
	ilDailyWD[6] = omNSRTableData[0].SunDebit;    

	ilSum = ilMax = 0;
	int ilPAlternate =1; // Parmeter input alternate pattern : 1 = true;
	ilWorkDays = 7.0- ((float)ilPRD/(float)ilPWeek) ;

	for( ilDay = 0; ilDay < 7; ilDay++ )
	{	
		ilSum += ilDailyWD[ilDay];
		if( ilDailyWD[ilDay] > ilMax )
			ilMax = ilDailyWD[ilDay];
	}

	ilAve = ceil ((float)(ilSum / ilWorkDays));
		
	if( ilAve < ilMax )
		ilAve = ilMax;

	for( ilDay = 0; ilDay < 7; ilDay++ )
		ilDailyRD[ilDay] = ilAve - ilDailyWD[ilDay];

	/* Initialise shift roster data */
	ilRD = 0;

	//Reset the week on Popuplation.
	im_SBC_Periode = 0;
	char plTemp[11];
	
	//Ratio for each group in a week
	/*
	POSITION pos ;
	CString key ="";
	CString value ="";
	int ilValue =0;
    //for( pos = omSCGrpMap.GetStartPosition(); pos != NULL; )
	for( pos = omSCGrpMap.GetStartPosition(); pos != NULL; )
	{

      //omSCGrpMap.GetNextAssoc( pos,nKey, (CObject*&)nCnt );
      //omSCGrpMap.SetAt((void*)nKey,(void*&)((nCnt*14)/ilAve));
	   //nValue =	omSCGrpMap.HashKey((void*&)nKey);

	   omSCGrpMap.GetNextAssoc(pos, key, value );

	   if(value != "")
	   {	
			ilValue = atoi(value);
		    value = itoa(ceil((ilValue*2)/(ilAve)),plTemp,10);
			omSCGrpMap.SetAt(key,value);
	   }
    }
	*/ 


	///////////////////////////////////////
	//Populate Rest Day
	ilPAlternate = (ilPRD%ilPWeek>0);

	int ilOddEven =0;
	int ilRDPerWeek = ilPRD/ilPWeek;

	for( ilWeek = 0; ilWeek < ilAve; ilWeek++ )
	{
		GSPTABLEDATA *prlGplData = new GSPTABLEDATA;

		/////////////////////////////////////
		CString olTmp;
		m_E_GPL_Caption.GetWindowText(olTmp);
		im_SBC_Periode++;
		olTmp.Format("%01d", im_SBC_Periode * 7);
		m_E_GPL_Peri.SetInitText(olTmp);
		m_SBC_Periode.SetPos(im_SBC_Periode);

		CString olTmpTxt;
		olTmpTxt.Format("%03d",ilWeek + 1);
		prlGplData->Week		= olTmpTxt;
		prlGplData->DisplayWeek	= olTmpTxt; //$$
		prlGplData->Number		= "1";
		prlGplData->IsChanged	= RECORDCHANGED;

		for( ilDay = 0; ilDay < 7; ilDay++ )
		{
			if( ilRD == ilDay && ilDailyRD[ilDay] > 0 && (prlGplData->Day[ilDay] == ""))
			{
				if(ilRDPerWeek>=0)//For 1(RD)/2(Week) case, it could be 0;
				{
					int ilExRDPerWeek = ilRDPerWeek;
					int ilSRD = 0;
					int ilERD = 0;
					
					if(ilPAlternate)
					{
						//Odd rows
						if(ilWeek%2>0)
						{
								ilExRDPerWeek++;
						}
					}

					if((7-ilDay)<ilExRDPerWeek)
					{
						ilSRD = 7-ilExRDPerWeek;
						ilERD = ilSRD + ilExRDPerWeek;						
					}
					else
					{
						ilSRD = ilDay;
						ilERD = ilSRD + ilExRDPerWeek;
					}

					int ilIsRD = 0;
					for(ilDay=ilSRD;ilDay< ilERD ; ilDay++)
					{
						if(ilDailyRD[ilDay] > 0)
						{
							/* fill in rest day */
							ilDailyRD[ilDay]--;
							
							char pcgHome[5];
							GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "TAB", pcgHome, sizeof pcgHome, pcgConfigPath);
							
							
							if (pcgHome[0] == 'S' || pcgHome[0] == 'P' || pcgHome[0] == 'B')
							  prlGplData->Day[ilDay] = ilIsRD<1 ? "RD": "OFF";    // crash 
							else 
						      prlGplData->Day[ilDay] = ilIsRD<1 ? "FNL0": "RIP0";
						
							ODADATA* prlMyOdaData = ogOdaData.GetOdaBySdac(prlGplData->Day[ilDay]);
							prlGplData->DayUrno[ilDay] = ltoa(prlMyOdaData->Urno,plTemp,10);
							prlGplData->PfcCode[ilDay] = "";
							prlGplData->PfcUrno[ilDay] = "";
							ilIsRD++;
						}
					}
				}
			}			
		}		
		
		ilRD++;
			if( ilRD < 0 || ilRD >= 7 )
				ilRD = 0;

		prlGplData->IsChanged |= RECORDCHANGED; //SHIFTCHANGED;
		omGSPTableData.Add(prlGplData);
	}
	///////////////////////////////////////

	//Populate Shift Code
	int ilCurShift = PMSHIFT;
	bool blConRD;
	for(ilWeek = 0; ilWeek < omGSPTableData.GetSize(); ilWeek++)
	{		
		ilRDPerWeek = 0;
		double flTotalMin = 0;
		
		GSPTABLEDATA *prlGplData = &omGSPTableData[ilWeek];

		for( ilDay = 0; ilDay < 7; ilDay++ )
		{
			blFound = FALSE;

			if(prlGplData->Day[ilDay] == "")
			{	
				blConRD = FALSE;
				int ilAvgShPerWeek = 0;
				if(ilCurShift == AMSHIFT && ilTotalAM>0)
				{
					for(int ilCnt= olAMShiftData.GetSize()-1; ilCnt>=0; ilCnt--)
					{
						prlShiftData = &olAMShiftData[ilCnt];

					
						if(prlShiftData->Day==ilDay)
						{
							if(prlShiftData->SType <12 && prlShiftData->RCount >0 )// && prlShiftData->SType < ilPrevShiftHr)
							{
								SetShiftData(prlGplData, prlShiftData,ilDay);
								blFound = TRUE;
								ilTotalAM--;
								break;

							}
						}
					}					
				}
				if(ilCurShift == PMSHIFT && ilTotalPM>0)
				{
					for(int ilCnt= olPMShiftData.GetSize()-1; ilCnt>=0; ilCnt--)
					{
						prlShiftData = &olPMShiftData[ilCnt];
						
					
						if(prlShiftData->Day==ilDay)
						{
							if(prlShiftData->SType >=12 && prlShiftData->RCount >0)// && prlShiftData->SType <ilPrevShiftHr)
							{
								SetShiftData(prlGplData, prlShiftData,ilDay);
								blFound = TRUE;
								ilTotalPM--;
								break;
							}
						}
					}
				}				
				
				//If not found find any shift code left and populate.
				if(!blFound)
				{
					if(ilTotalAM>0)
					{
						for(int ilCnt= olAMShiftData.GetSize()-1;ilCnt>=0; ilCnt--)
						{
							prlShiftData = &olAMShiftData[ilCnt];

							if(prlShiftData->Day==ilDay && prlShiftData->RCount >0)
							{
								SetShiftData(prlGplData, prlShiftData,ilDay);	
								blFound = TRUE;
								ilTotalAM--;
								break;
							}
						}		
					}
				}
				if(!blFound)
				{
					if(ilTotalPM>0)
					{
						for(int ilCnt= olPMShiftData.GetSize()-1; ilCnt>=0; ilCnt--)
						{
							prlShiftData = &olPMShiftData[ilCnt];

							if(prlShiftData->Day==ilDay && prlShiftData->RCount >0)
							{
								SetShiftData(prlGplData, prlShiftData,ilDay);	
								blFound = TRUE;
								ilTotalPM--;
								break;
							}
						}
					}
				}
				
				//To populate the RD into those empty cells
				/*
				if(!blFound)
				{
					if( ilDailyRD[ilDay] > 0 )
					{
						///Fill in rest blank day as RD
						ilDailyRD[ilDay]--;
						prlGplData->Day[ilDay] = "RD";
						ODADATA* prlMyOdaData = ogOdaData.GetOdaBySdac(prlGplData->Day[ilDay]);
						prlGplData->DayUrno[ilDay] = ltoa(prlMyOdaData->Urno,plTemp,10);
						prlGplData->PfcCode[ilDay] = "";
						prlGplData->PfcUrno[ilDay] = "";
					}
					continue;
				}
				*/
				
				//Fill in found working shift
				if( blFound == TRUE )
				{
					GetDayStuff(prlGplData->DayUrno[ilDay],
								prlGplData->Day2Urno[ilDay],
								prlGplData->PfcCode[ilDay],
								prlGplData->PfcCode2[ilDay],
								ilDay,
								prlGplData,
								&flTotalMin);

					ilDailyWD[ilDay]--;
					switch (ilDay)
					{
						case 0:
							omNSRTableData[0].MonIs++;
							prlShiftData->NSRTableData->MonIs++;
							break;
						case 1:
							omNSRTableData[0].TueIs++;
							prlShiftData->NSRTableData->TueIs++;
							break;
						case 2:
							omNSRTableData[0].WndIs++;
							prlShiftData->NSRTableData->WndIs++;
							break;
						case 3:
							omNSRTableData[0].ThuIs++;
							prlShiftData->NSRTableData->ThuIs++;
							break;
						case 4:
							omNSRTableData[0].FriIs++;
							prlShiftData->NSRTableData->FriIs++;
							break;
						case 5:
							omNSRTableData[0].SatIs++;
							prlShiftData->NSRTableData->SatIs++;
							break;
						case 6:
							omNSRTableData[0].SunIs++;
							prlShiftData->NSRTableData->SunIs++;
							break;
						
					}
				}
				
			}
			else if(prlGplData->Day[ilDay] == "RD" || prlGplData->Day[ilDay] == "OFF"  )
			{
				if(!blConRD)
				{
					if(ilCurShift == AMSHIFT)
					{
						ilCurShift = PMSHIFT;
					}
					else if(ilCurShift == PMSHIFT)
					{
						ilCurShift = AMSHIFT;
					}
					blConRD = TRUE;
				}
			}
		}

		prlGplData->Houers.Format("%#01.2f", flTotalMin / 60);		
		
	}
	///////////////////////////////////////
	
	int ilSize = omGSPTableData.GetSize();
	imGSPRangeMax = ilSize - imGSPLines;
	if(imGSPRangeMax<0) imGSPRangeMax = 0;
	m_SB_GSP.SetScrollRange(imGSPRangeMin,imGSPRangeMax,false);
	m_SB_GSP.SetScrollPos(imGSPRangeMax);
	if(imGSPRangeMax > 0)
	{
		m_SB_GSP.EnableScrollBar(ESB_ENABLE_BOTH);
	}
	else
	{
		m_SB_GSP.EnableScrollBar(ESB_DISABLE_BOTH);
	}
	
	imActualLine = imGSPRangeMax;


	ChangeShiftRequirementData(0);
	DeleteNullFromShiftRequirementData();
	ChangeShiftRequirementBonusData();
	ChangeShiftRequirementGruppData();
	DisplayShiftRequirementData(2);
	
	DisplayShiftRosterData(m_SB_GSP.GetScrollPos());

	bmIsChangedGPL = true;
	((ShiftRoster_Frm*)GetParentFrame())->LoadToolBar(TRUE);



}

void ShiftRoster_View::CalculateShiftReqTotDebit()
{
	int ilReq;
	for( ilReq=1; ilReq < omNSRTableData.GetSize(); ilReq++ )
	{
			omNSRTableData[0].MonIs += omNSRTableData[ilReq].MonIs - omNSRTableData[ilReq].MonDebit;	
			omNSRTableData[0].TueIs += omNSRTableData[ilReq].TueIs - omNSRTableData[ilReq].TueDebit;	
			omNSRTableData[0].WndIs += omNSRTableData[ilReq].WndIs - omNSRTableData[ilReq].WndDebit;	
			omNSRTableData[0].ThuIs += omNSRTableData[ilReq].ThuIs - omNSRTableData[ilReq].ThuDebit;	
			omNSRTableData[0].FriIs += omNSRTableData[ilReq].FriIs - omNSRTableData[ilReq].FriDebit;	
			omNSRTableData[0].SatIs += omNSRTableData[ilReq].SatIs - omNSRTableData[ilReq].SatDebit;	
			omNSRTableData[0].SunIs += omNSRTableData[ilReq].SunIs - omNSRTableData[ilReq].SunDebit;	
	}

}

void ShiftRoster_View::ResetShiftRequirmentData()
{
	int ilReq;
	for( ilReq=0; ilReq < omNSRTableData.GetSize(); ilReq++ )
	{
			omNSRTableData[ilReq].MonIs = 0;
			omNSRTableData[ilReq].TueIs = 0;	
			omNSRTableData[ilReq].WndIs = 0;
			omNSRTableData[ilReq].ThuIs = 0;	
			omNSRTableData[ilReq].FriIs = 0;	
			omNSRTableData[ilReq].SatIs = 0;	
			omNSRTableData[ilReq].SunIs = 0;
	}
	//CalculateShiftReqTotDebit();
}

/*
float ShiftRoster_View::CalcHours(CString opShiftTime)
{
	float flHours[2];
	float flMinutes[2];
	float flTotalMin = 0;

	flHours[0] = atoi( opShiftTime.Mid( 0, 2 ) );
	flMinutes[0] = atoi( opShiftTime.Mid( 2, 2 ) );
	flHours[1] = atoi( opShiftTime.Mid( 4, 2 ) );
	flMinutes[1] = atoi( opShiftTime.Mid( 6, 2 ) );

	if( flHours[1] < flHours[0] )
	{
		flTotalMin = (24*60) - (flHours[0]*60+flMinutes[0]);
		flTotalMin += flHours[1]*60+flMinutes[1];
	}
	else
		flTotalMin = (flHours[1]*60+flMinutes[1]) - (flHours[0]*60+flMinutes[0]);
	return flTotalMin;
}
*/



int ShiftRoster_View ::GetShiftType(CString shiftCode)
{
	int shiftHrs =0;
	int shiftType =0;

	shiftType = atoi(shiftCode.Mid(0,2));
	
	return shiftType;
	
}

/*
CString ShiftRoster_View::GetValidShiftCode()
{

	olShiftData
}
*/

//--GET BY Requirement Count by Shift Group------
/*
int ShiftRoster_View::GetReqCountByShiftGrp(CString  ipGrp)
{
	int cnt = 0;
	CString  prlCnt="0";
CCS_TRY;

	
	if(omSCGrpMap.Lookup(ipGrp, prlCnt) == FALSE)
	{
		prlCnt="0";
	}
	cnt = atoi(prlCnt);

CCS_CATCH_ALL;
	return cnt;
}
*/


void ShiftRoster_View::SetShiftData(GSPTABLEDATA * prlGplData, SHIFTDATA * prlShiftData, int ilDay)  // caonima  crash 
{
	char plTemp[11];
	prlGplData->Day[ilDay] = prlShiftData->SCode;
	
//	MessageBox(prlShiftData->SCode);
 
	// 3 AZAPP          4 ADR
	
	CString s ,d;
	s.Format("%c%c%c%c",prlShiftData->SCode[0], prlShiftData->SCode[1],prlShiftData->SCode[2],prlShiftData->SCode[3]);
	d.Format("%c%c%c",prlShiftData->SCode[0], prlShiftData->SCode[1],prlShiftData->SCode[2]);
		
	BSDDATA * prlBsd = NULL ;
	char pcgHome[5];
	GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "TAB", pcgHome, sizeof pcgHome, pcgConfigPath);

	
	if (pcgHome[0] == 'S' || pcgHome[0] == 'P' || pcgHome[0] == 'B')
    	prlBsd = ogBsdData.GetBsdByBsdc(prlGplData->Day[ilDay]);  
	else
	{
        if (prlShiftData->SCode[4] == '(')
		   	prlBsd = ogBsdData.GetBsdByBsdc(d);
		else 
		    prlBsd = ogBsdData.GetBsdByBsdc(s);
	}

	prlGplData->DayUrno[ilDay] = ltoa(prlBsd->Urno,plTemp,10);
	prlGplData->PfcCode[ilDay] = prlShiftData->NSRTableData->Fctc;
	PFCDATA* polPfcData = ogPfcData.GetPfcByFctc(prlGplData->PfcCode[ilDay]);
    prlGplData->PfcUrno[ilDay] = ltoa(polPfcData->Urno,plTemp,10);
	prlShiftData->RCount--;
	
}
