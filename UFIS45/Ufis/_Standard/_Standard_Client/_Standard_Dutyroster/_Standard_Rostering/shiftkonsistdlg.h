#ifndef AFX_SHIFTKONSISTDLG_H__F605AF92_220D_11D2_800C_004095434A85__INCLUDED_
#define AFX_SHIFTKONSISTDLG_H__F605AF92_220D_11D2_800C_004095434A85__INCLUDED_

// ShiftKonsistDlg.h : Header-Datei
//
#include <resource.h>
#include <basicdata.h>
#include <ShiftRoster_View.h>

// All assignd employees
struct ASSIGNDDEMPLOYEE
{
	CString	Stfu;  		// URNO des Mitarbeiters (aus STF)
	CString	StfName;  	// V+NName des Mitarbeiters (aus STF)

	CString	Splu;  		// URNO des Schichtplans (aus SPL)
	CString	SplName;  	// Name des Schichtplans (aus SPL)

	CString	Gplu;  		// URNO des Grundschichtplans (aus GPL)
	CString	GplName;  	// Name des Grundschichtplans (aus GPL)
	CString AsFr; 		// Assignd from
	CString AsTo; 		// Assignd to

	CString	Gspu;  		// URNO der Grundschichtplan-Woche(aus GSP)
	int	Week;	 		// Nr. der Grundschichtplan-Woche(aus GSP)
	bool IsDouble;	

	ASSIGNDDEMPLOYEE(void)
	{
		Week = 0;
		IsDouble = false;
	}
};
// End ASSIGNDDEMPLOYEE
/////////////////////////////////////////////////////////////////////////////
// Dialogfeld ShiftKonsistDlg 

class ShiftKonsistDlg : public CDialog
{
// Konstruktion
public:
	ShiftKonsistDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	~ShiftKonsistDlg(void);

// Dialogfelddaten
	//{{AFX_DATA(ShiftKonsistDlg)
	enum { IDD = IDD_SHIFT_KONSIST_DLG };
	CStatic	m_S_H1;
	CStatic	m_S_H2;
	CStatic	m_S_H3;
	CStatic	m_S_H4;
	CStatic	m_S_H5;
	CListBox	m_LB_List;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(ShiftKonsistDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(ShiftKonsistDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	virtual void OnCancel();
	void SetValues();


	CCSPtrArray<ASSIGNDDEMPLOYEE> omAssignd;

protected:
	ShiftRoster_View *pomParent;


};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_SHIFTKONSISTDLG_H__F605AF92_220D_11D2_800C_004095434A85__INCLUDED_
