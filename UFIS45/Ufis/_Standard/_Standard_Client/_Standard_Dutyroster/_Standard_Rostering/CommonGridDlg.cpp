// CCommonGridDlg.cpp : implementation file
//

#include <stdafx.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define CMNGRID_MINROWS 17
#define WIDTH_OFFSET 100

/////////////////////////////////////////////////////////////////////////////

CCommonGridDlg::CCommonGridDlg(CString opCaption, CStringArray *popColumn1, 
			 CStringArray *popColumn2, 
			 CStringArray *popUrnos, 
			 CString opHeader1,
			 CString opHeader2,
			 CWnd* pParent /*=NULL*/,
			 bool bpShowSearch /*= false*/)
			: CDialog(CCommonGridDlg::IDD, pParent)
{
	omCaption =		opCaption;
	pomColumn1 =	popColumn1;
	pomColumn2 =	popColumn2;
	omHeader1 =		opHeader1;
	omHeader2 =		opHeader2;
	pomUrnos =		popUrnos;
	bmShowSearch =	bpShowSearch;

	omReturnString.Empty();
	pomGrid = NULL;
	
	//{{AFX_DATA_INIT(CCommonGridDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

//************************************************************************************
//
//************************************************************************************

CCommonGridDlg::~CCommonGridDlg()
{
	if (pomGrid != NULL)
		delete pomGrid;
}

//************************************************************************************
//
//************************************************************************************

void CCommonGridDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCommonGridDlg)
	DDX_Control(pDX, IDOK, m_OK);
	//}}AFX_DATA_MAP
}

//************************************************************************************
//
//************************************************************************************

BEGIN_MESSAGE_MAP(CCommonGridDlg, CDialog)
	//{{AFX_MSG_MAP(CCommonGridDlg)
	ON_MESSAGE(WM_GRID_LBUTTONDOWN,		OnGridLButton)
	ON_BN_CLICKED(IDC_B_SEARCH, OnBSearch)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//************************************************************************************
//
//************************************************************************************

void CCommonGridDlg::OnOK() 
{
	long			ilUrno;
	CString			olUrno;
	CRowColArray	olRowColArray;
	CGXStyle		olStyle;

	// Aktuelle Auswahl ermitteln und zur�ckgeben
	pomGrid->GetSelectedRows (olRowColArray);

	// Wurden Zeilen ausgew�hlt
	if (olRowColArray.GetSize() == 0)
	{
		TRACE ("Keine Zeilen ausgew�hlt.\n");
	}

	// Style und damit Datenpointer einer Zelle erhalten
	pomGrid->ComposeStyleRowCol(olRowColArray[0],1, &olStyle );

	// Urno erhalten
	ilUrno = (long)olStyle.GetItemDataPtr();
	if (!ilUrno)
	{
		TRACE ("Urno=0 in CCommonGridDlg f�r Zeile%d\n",olRowColArray[0]);
	}
	else
	{
		olUrno.Format( "%ld", ilUrno );
		omReturnString = olUrno;
	}

	CDialog::OnOK();
}

//************************************************************************************
//
//************************************************************************************

BOOL CCommonGridDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// Sollten die Array verschieden gro� sein, hier abbrechen.
	if (!((pomColumn1->GetSize() == pomUrnos->GetSize()) && (pomColumn2->GetSize() == pomUrnos->GetSize())))
	{
		TRACE("CCommonGridDlg: Arrays sind verschieden gro�\n");
		CDialog::EndDialog(IDCANCEL);
		return (true);
	}

	// Grid initialisieren
	IniGrid();
	SortGrid(2);
	
	// Texte setzen
	SetWindowText(omCaption);
	GetDlgItem(IDOK)->SetWindowText(LoadStg(ID_OK));
	GetDlgItem(IDCANCEL)->SetWindowText(LoadStg(ID_CANCEL));

	if (bmShowSearch == false)
	{
		GetDlgItem(IDC_B_SEARCH)->ShowWindow(SW_HIDE);
	}
	else
	{
		GetDlgItem(IDC_B_SEARCH)->SetWindowText(LoadStg(IDS_STRING1002));

		// --- resizing the form
		CRect olRect;
		GetWindowRect (olRect);
		ClientToScreen (olRect);
		MoveWindow(olRect.left, olRect.top, olRect.Width()+WIDTH_OFFSET, olRect.Height());
		CenterWindow();

		// --- resizing the grid
		GetDlgItem(IDC_GRID)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem(IDC_GRID)->MoveWindow(olRect.left, olRect.top, olRect.Width()+WIDTH_OFFSET, olRect.Height());

		// --- moving OK-button
		GetDlgItem (IDOK)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem (IDOK)->MoveWindow(olRect.left+WIDTH_OFFSET, olRect.top, olRect.Width(), olRect.Height());

		// --- moving CANCEL-button
		GetDlgItem (IDCANCEL)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem (IDCANCEL)->MoveWindow(olRect.left+WIDTH_OFFSET, olRect.top, olRect.Width(), olRect.Height());

		// --- moving SEARCH-button
		GetDlgItem (IDC_B_SEARCH)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem (IDC_B_SEARCH)->MoveWindow(olRect.left+WIDTH_OFFSET, olRect.top, olRect.Width(), olRect.Height());
	}

	return (true);  
}

//**********************************************************************************
// Grid initialisieren
//**********************************************************************************

void CCommonGridDlg::IniGrid ()
{
	// Grid erzeugen
	pomGrid = new CGridControl ( this,IDC_GRID,2,__max( pomColumn1->GetSize(),CMNGRID_MINROWS));
	// Header setzen
	pomGrid->SetValueRange(CGXRange(0,1), omHeader1);
	pomGrid->SetValueRange(CGXRange(0,2), omHeader2);

	// Breite der Spalten einstellen
	IniColWidths ();

	// Grid mit Daten f�llen.
	FillGrid();

	pomGrid->Initialize();
	pomGrid->GetParam()->EnableUndo(FALSE);
	pomGrid->LockUpdate(TRUE);
	pomGrid->LockUpdate(FALSE);
	pomGrid->GetParam()->EnableUndo(TRUE);

	// Verhindert, das die Spaltenbreite ver�ndert wird
	pomGrid->GetParam()->EnableTrackColWidth(FALSE);
	// Es k�nnen nur ganze Zeilen markiert werden.
	//pomGrid->GetParam()->EnableSelection(GX_SELROW);
	pomGrid->GetParam()->EnableSelection(GX_SELNONE);
	
	pomGrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE)
				.SetControl(GX_IDS_CTRL_STATIC)
				);
	pomGrid->EnableAutoGrow ( FALSE );
	// DoppelClick Acktion festlegen
	pomGrid->SetLbDblClickAction ( WM_COMMAND, IDOK);
}

//**********************************************************************************
// Breite der Columns setzen
//**********************************************************************************

void CCommonGridDlg::IniColWidths()
{
	pomGrid->SetColWidth ( 0, 0, 34);
	pomGrid->SetColWidth ( 1, 1, 100);
	if (bmShowSearch == true)
	{
		pomGrid->SetColWidth ( 2, 2, 140+WIDTH_OFFSET);
	}
	else
	{
		pomGrid->SetColWidth ( 2, 2, 140);
	}
}

//**********************************************************************************
// Grid mit Daten f�llen
//**********************************************************************************

void CCommonGridDlg::FillGrid()
{
CCS_TRY;
	long		ilUrno;
	CString		cslUrno;

	for (int i=0; i<pomColumn1->GetSize(); i++)
	{
		// Urno auslesen						
		cslUrno = pomUrnos->GetAt(i);
		ilUrno = atol(cslUrno);
		// Urno mit erstem Feld koppeln.						
		pomGrid->SetStyleRange (CGXRange(i+1,1), CGXStyle().SetItemDataPtr( (void*)ilUrno ) );
		// Datenfelder f�llen
		
		
		pomGrid->SetValueRange(CGXRange(i+1,1),	pomColumn1->GetAt(i) );
		pomGrid->SetValueRange(CGXRange(i+1,2), pomColumn2->GetAt(i) );



	}

	// Rows auff�llen, damit die "optik stimmt"
	for (i; i<CMNGRID_MINROWS;i++)
	{
		ilUrno = atol("0");
		// Urno mit erstem Feld koppeln.						
		pomGrid->SetStyleRange (CGXRange(i+1,1), CGXStyle().SetItemDataPtr( (void*)ilUrno ) );
		// Datenfelder f�llen
		pomGrid->SetValueRange(CGXRange(i+1,1),"");
		pomGrid->SetValueRange(CGXRange(i+1,2),"");
	}

CCS_CATCH_ALL;
}

//**********************************************************************************
// Sortierung des Grids "per Hand"
//**********************************************************************************

bool CCommonGridDlg::SortGrid(int ipRow)
{
	CGXSortInfoArray  sortInfo;
	sortInfo.SetSize( 1 );
	sortInfo[0].sortOrder = CGXSortInfo::ascending;
	sortInfo[0].nRC = ipRow;                       
	sortInfo[0].sortType = CGXSortInfo::autodetect;  
	pomGrid->SortRows( CGXRange().SetRows(1, pomColumn1->GetSize()), sortInfo); 
	
	return (true);
}

//**********************************************************************************
// Left Button Click in das Grid
//**********************************************************************************

LONG CCommonGridDlg::OnGridLButton(WPARAM wParam, LPARAM lParam)
{
	GRIDNOTIFY* rlNotify = (GRIDNOTIFY*) lParam;

	// Bisherige Auswahl l�schen.
	pomGrid->SetSelection(NULL);

	// neue Auswahl markieren
	POSITION area = pomGrid->GetParam( )->GetRangeList( )->AddTail(new CGXRange);
	pomGrid->SetSelection(area,rlNotify->row,0,rlNotify->row,2);

	return 0L;
}

void CCommonGridDlg::OnBSearch() 
{
	if (pomGrid)
	{
		pomGrid->OnShowFindReplaceDialog(TRUE);
	}
}
