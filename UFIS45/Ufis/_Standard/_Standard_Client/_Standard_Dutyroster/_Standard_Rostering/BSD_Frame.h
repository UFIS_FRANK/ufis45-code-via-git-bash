#if !defined(AFX_BSD_FRAME_H__74D50E03_82DA_11D3_8F9D_00500454BF3F__INCLUDED_)
#define AFX_BSD_FRAME_H__74D50E03_82DA_11D3_8F9D_00500454BF3F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BSD_Frame.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Rahmen BSD_Frame 

class BSD_Frame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(BSD_Frame)
protected:
	BSD_Frame();           // Dynamische Erstellung verwendet geschützten Konstruktor

// Attribute
public:

// Operationen
public:

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(BSD_Frame)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementierung
protected:
	virtual ~BSD_Frame();

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(BSD_Frame)
		// HINWEIS - Der Klassen-Assistent fügt hier Member-Funktionen ein und entfernt diese.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_BSD_FRAME_H__74D50E03_82DA_11D3_8F9D_00500454BF3F__INCLUDED_
