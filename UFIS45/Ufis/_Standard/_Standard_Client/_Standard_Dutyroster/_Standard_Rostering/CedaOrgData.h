// CedaOrgData.h

#ifndef __CEDAORGDATA__
#define __CEDAORGDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct ORGDATA 
{
	CTime 	 Cdat; 		// Erstellungsdatum
	char 	 Dpt1[8+2]; // Code
	char 	 Dpt2[8+2]; 	// �bergeordnete Einheit. Code
	char 	 Dptn[42]; 	// Bezeichnung
	CTime 	 Lstu;		// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Rema[62]; 	// Bemerkung
	long 	 Urno;		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)

	// not used char 	 Odgl[6]; 	// average duration of full tour
	// not used char 	 Odkl[6]; 	// average duration of short tour

	// not used char 	 Odsl[6]; 	// average course tour duration

	//DataCreated by this class
	int		 IsChanged;	// Check whether Data has Changed f�r Relaese

	ORGDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Lstu		=	TIMENULL;
	}

}; // end OrgDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaOrgData: public CedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    CMapStringToPtr omDpt1Map;

    CCSPtrArray<ORGDATA> omData;

// Operations
public:
    CedaOrgData();
	~CedaOrgData();
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(ORGDATA *prpOrg);
	bool InsertInternal(ORGDATA *prpOrg);
	bool Update(ORGDATA *prpOrg);
	bool UpdateInternal(ORGDATA *prpOrg);
	bool Delete(long lpUrno);
	bool DeleteInternal(ORGDATA *prpOrg);
	bool ReadSpecialData(CCSPtrArray<ORGDATA> *popOrg,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(ORGDATA *prpOrg);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	ORGDATA  *GetOrgByUrno(long lpUrno);
	ORGDATA *GetOrgByDpt1(CString opDpt1);


	// Private methods
private:
    void PrepareOrgData(ORGDATA *prpOrgData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAORGDATA__
