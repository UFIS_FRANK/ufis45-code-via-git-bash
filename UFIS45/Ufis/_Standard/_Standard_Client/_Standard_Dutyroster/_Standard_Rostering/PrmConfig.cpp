#include <stdafx.h>
#include <resource.h>
#include <ccsglobl.h>
#include <PrmConfig.h>
#include <PrivList.h>


PrmConfig ogPrmConfig;


PrmConfig::PrmConfig(void)
{

//   GetPrivateProfileString("GLOBAL]", "PRM-HandlingAgents", "", cgPrmHandlingAgents, sizeof pclTmpText, olConfigFileName);
   UseHag = true;
   prmHag = NULL;
//   prmHagData = new(CedaHAGData);

  // prmHagData->Read();
}

void PrmConfig::Initialize(void)
{

//   GetPrivateProfileString("GLOBAL]", "PRM-HandlingAgents", "", cgPrmHandlingAgents, sizeof pclTmpText, olConfigFileName);
   UseHag = true;
   prmHag = NULL;
   prmHagData = new(CedaHAGData);

   prmHagData->Read();
}


HAGDATA *PrmConfig::getHandlingAgent()
{
	if (prmHag == NULL)
	{
		ReadHandlingAgentsList();
	}
	return prmHag;
}

void PrmConfig::ReadHandlingAgentsList()
{
	CStringArray olShortNames;

	prmHagData->GetShortNameList(olShortNames);
	for (int i = 0; i < olShortNames.GetSize(); i++)
	{
		CString tmpString = olShortNames[i];
		if (CheckHandlingAgent(olShortNames[i])) 
		{
			prmHag = prmHagData->GetHagByHsna(olShortNames[i]);
			break;
		}
	}
}
                 

char *PrmConfig::getHandlingAgentShortName()
{
	if (prmHag == NULL)
	{
		if (*cmHandlingAgentsList =='\0')
		{
			ReadHandlingAgentsList();
		}
	}
	
	if (prmHag != NULL)
	{
		return(prmHag->Hsna);
	}
	return NULL;
}

bool PrmConfig::CheckHandlingAgent(CString opHandlingAgent)
{
	return ogPrmPrivList.GetStat(CString("PRM-Handling Agents-" + opHandlingAgent)) == '1';
}
   
