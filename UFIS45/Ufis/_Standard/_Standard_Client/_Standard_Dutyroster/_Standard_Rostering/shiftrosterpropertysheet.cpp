// ShiftRosterPropertySheet.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// ShiftRosterPropertySheet
//

ShiftRosterPropertySheet::ShiftRosterPropertySheet(CString opCalledFrom, CWnd* pParentWnd,CViewer *popViewer, UINT iSelectPage)
						: BasePropertySheet(opCalledFrom, pParentWnd, popViewer, iSelectPage)
{
	omCalledFrom = opCalledFrom;

	AddPage(&omShiftRoster2View);
	omShiftRoster2View.SetCalledFrom(omCalledFrom);
	AddPage(&omShiftRosterView);
	omShiftRosterView.SetCalledFrom(omCalledFrom);
	//uhi 25.4.01
	AddPage(&omShiftRoster3View);
	omShiftRoster3View.SetCalledFrom(omCalledFrom);

}

/////////////////////////////////////////////////////////////////////////////
//---------------------------------------------------------------------------

void ShiftRosterPropertySheet::LoadDataFromViewer()
{
	pomViewer->GetFilter("SHIFTVIEW2", omShiftRoster2View.omValues);
	pomViewer->GetFilter("SHIFTVIEW1", omShiftRosterView.omValues);
	//uhi 25.4.01
	pomViewer->GetFilter("SHIFTVIEW3", omShiftRoster3View.omValues);
	
}

//---------------------------------------------------------------------------

void ShiftRosterPropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	pomViewer->SetFilter("SHIFTVIEW2", omShiftRoster2View.omValues);
	pomViewer->SetFilter("SHIFTVIEW1", omShiftRosterView.omValues);
	//uhi 25.4.01
	pomViewer->SetFilter("SHIFTVIEW3", omShiftRoster3View.omValues);

	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}

	
}

//---------------------------------------------------------------------------

int ShiftRosterPropertySheet::QueryForDiscardChanges()
{
	return IDOK;
}

//---------------------------------------------------------------------------
