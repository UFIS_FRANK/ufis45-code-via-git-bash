#include <stdafx.h>

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Globale Funktionen
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// ProcessDrsCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_DRS_CHANGE, BC_DRS_NEW und BC_DRS_DELETE. Ruft zur eingentlichen Bearbeitung
//	der Nachrichten die Funktion CedaDrsData::ProcessDrsBc() der entsprechenden 
//	Instanz von CedaDrsData auf.	
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessDrsCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Empf�nger ermitteln
	CedaDrsData *polDrsData = (CedaDrsData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polDrsData->ProcessDrsBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Implementation der Klasse CedaDrsData (Daily Roster Supplements - untert�gige Erg�nzung)
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// Konstruktor
//************************************************************************************************************************************************

CedaDrsData::CedaDrsData(CString opTableName, CString opExtName) : CedaData(&ogCommHandler)
{
    // Infostruktur vom Typ CEDARECINFO f�r DRS-Datens�tze
	// Wird von Vaterklasse CCSCedaData f�r verschiedene Funktionen
	// (CCSCedaData::GetBufferRecord(), CCSCedaData::MakeCedaData())
	// benutzt. Die Datenstruktur beschreibt die Tabellenstruktur.
    BEGIN_CEDARECINFO(DRSDATA, DrsDataRecInfo)
		FIELD_LONG		(Ats1,"ATS1")	// MA-Urno des zugeordneten MAs
		FIELD_LONG		(Drru,"DRRU")	// Urno des zugeordneten DRR-Datensatzes
		// bda 04.10.2000: die Drsi-Felder sind nach DRR/DRA verschoben
		// FIELD_CHAR_TRIM	(Drs1,"DRS1")	// Zusatzinfo zur Schicht
		// FIELD_CHAR_TRIM	(Drs2,"DRS2")	// Prozent der Arbeitsunf�higkeit
		// FIELD_CHAR_TRIM	(Drs3,"DRS3")	// Zusatzinfo zur Schicht / vor Schicht
		// FIELD_CHAR_TRIM	(Drs4,"DRS4")	// Zusatzinfo zur Schicht / Basisschicht
		FIELD_CHAR_TRIM	(Sday,"SDAY")	// Schichttag
		FIELD_CHAR_TRIM	(Stat,"STAT")	// Status des MAs
		FIELD_CHAR_TRIM	(Uses,"USES")	// MA Letzter Schichttausch
		FIELD_LONG		(Stfu,"STFU")	// MA-Urno
		FIELD_LONG		(Urno,"URNO")	// DS-Urno
    END_CEDARECINFO

	// Infostruktur kopieren
    for (int i = 0; i < sizeof(DrsDataRecInfo)/sizeof(DrsDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DrsDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	// Tabellenname per Default auf DRS setzen
	opExtName = pcgTableExt;
	SetTableNameAndExt(opTableName+opExtName);

    // Feldnamen initialisieren
	// bda 04.10.2000: die Drsi-Felder sind nach DRR/DRA verschoben
	// strcpy(pcmListOfFields,"ATS1,DRRU,DRS1,DRS2,DRS3,DRS4,SDAY,STAT,USES,STFU,URNO");
	strcpy(pcmListOfFields,"ATS1,DRRU,SDAY,STAT,USES,STFU,URNO");                                                            // caonima

 
	
	// Zeiger der Vaterklasse auf Tabellenstruktur setzen
	CCSCedaData::pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

    // Datenarrays initialisieren
	omUrnoMap.InitHashTable(256);
	omDrruMap.InitHashTable(256);

	// ToDo: min. / max. Datum f�r DRSs initialisieren
//	omMinDay = omMaxDay = TIMENULL;

	// BCs anmelden
	Register();
}


//************************************************************************************************************************************************
// Destruktor
//************************************************************************************************************************************************

CedaDrsData::~CedaDrsData()
{
	ClearAll(true);
	omRecInfo.DeleteAll();
}

//************************************************************************************************************************************************
// SetTableNameAndExt: stellt den Namen und die Extension ein.
// R�ckgabe:	false	->	Tabellenname mit Extension gr��er als
//							Klassenmember
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaDrsData::SetTableNameAndExt(CString opTableAndExtName)
{
CCS_TRY;
	if(opTableAndExtName.GetLength() >= sizeof(pcmTableName)) return false;
	strcpy(pcmTableName,opTableAndExtName.GetBuffer(0));
	return true;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
//	Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
//	Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
//	dann die entsprechende Nachricht bearbeitet. �ber den nach void 
//	konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
//	Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
//	und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
//	entsprechenden Member-Funktion).
// R�ckgabe: keine
//************************************************************************************************************************************************

void CedaDrsData::Register(void)
{
CCS_TRY;
	// bereits angemeldet?
	if (bmIsBCRegistered) return;	// ja -> gibt nichts zu tun, terminieren

	// DRS-Datensatz hat sich ge�ndert
	DdxRegister((void *)this,BC_DRS_CHANGE, "CedaDrsData", "BC_DRS_CHANGE",        ProcessDrsCf);
	DdxRegister((void *)this,BC_DRS_NEW,    "CedaDrsData", "BC_DRS_NEW",		   ProcessDrsCf);
	DdxRegister((void *)this,BC_DRS_DELETE, "CedaDrsData", "BC_DRS_DELETE",        ProcessDrsCf);
	// jetzt ist die Instanz angemeldet
	bmIsBCRegistered = true;
CCS_CATCH_ALL;
}


//************************************************************************************************************************************************
// ClearAll: aufr�umen. Alle Arrays und PointerMaps werden geleert.
// R�ckgabe:	immer true
//************************************************************************************************************************************************

bool CedaDrsData::ClearAll(bool bpUnregister)
{
CCS_TRY;
    // alle Arrays leeren
	omUrnoMap.RemoveAll();
	omDrruMap.RemoveAll();
	omData.DeleteAll();
	
	// beim BC-Handler abmelden
	if(bpUnregister && bmIsBCRegistered)
	{
		ogDdx.UnRegister(this,NOTUSED);
		// jetzt ist die Instanz angemeldet
		bmIsBCRegistered = false;
	}
    return true;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// ReadDrsByUrno: liest den DRS-Datensatz mit der  Urno <lpUrno> aus der 
//	Datenbank.
// R�ckgabe:	boolean Datensatz erfolgreich gelesen?
//************************************************************************************************************************************************

bool CedaDrsData::ReadDrsByUrno(long lpUrno, DRSDATA *prpDrs)
{
CCS_TRY;
	CString olWhere;
	olWhere.Format("%ld", lpUrno);
	// Datensatz aus Datenbank lesen
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, olWhere);
		WriteInRosteringLog();
	}

	if (CedaAction2("RT", (char*)(LPCTSTR)olWhere) == false)
	{
		ClearFastSocketBuffer();	
		return false;	// Fehler -> abbrechen
	}
	// Datensatz-Objekt instanziieren...
	DRSDATA *prlDrs = new DRSDATA;
	// und initialisieren
	if (GetBufferRecord2(0,prpDrs) == true)
	{  
		ClearFastSocketBuffer();	
		return true;	// Aktion erfolgreich
	}
	else
	{
		// Fehler -> aufr�umen und terminieren
		ClearFastSocketBuffer();	
		delete prlDrs;
		return false;
	}

	return false;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// ReadFilteredByDateAndStaff: liest Datens�tze ein, deren Feld SDAY (g�ltig am)
//	innerhalb des Zeitraums <opStart> bis <opEnd> liegen. Die Parameter werden auf 
//	G�ltigkeit gepr�ft. 
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaDrsData::ReadFilteredByDateAndStaff(COleDateTime opStart, COleDateTime opEnd,
											 CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/,
											 bool bpRegisterBC /*= true*/,
											 bool bpClearData /*= true*/)
{
CCS_TRY;
	// G�ltigkeit der Datumsangaben pr�fen
	if ((opStart.GetStatus() != COleDateTime::valid) ||
		(opEnd.GetStatus() != COleDateTime::valid) || 
		(opStart >= opEnd) )
	{
		// Fehler -> terminieren
		return false;
	}

	// Puffer f�r WHERE-Statement
	CString olWhere;

	// Anzahl der Urnos
	int ilCountStfUrnos = -1;
	if ((popLoadStfUrnoMap != NULL) && ((ilCountStfUrnos = popLoadStfUrnoMap->GetCount()) == 0))
	{
		// es gibt nichts zu tun, kein Fehler
		return true;	
	}
	
	// keine Urno-Liste oder Urno-Liste �bergeben aber Anzahl gr��er als max. in WHERE-Clause erlaubte MA-Urnos?
	if ((popLoadStfUrnoMap == NULL) || (ilCountStfUrnos > MAX_WHERE_STAFFURNO_IN))
	{
		// mehr Urnos als in WHERE-Clause passen -> alles laden und lokal filtern
		olWhere.Format("WHERE SDAY BETWEEN '%s' AND '%s'",opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"));
		// Datens�tze einlesen und Ergebnis zur�ckgeben
		
	
		return Read(olWhere.GetBuffer(0),popLoadStfUrnoMap,bpRegisterBC,bpClearData);

			
	}
	else
	{
		// MA-Urnos mit in WHERE-Clause aufnehmen
		POSITION pos;			// zum Iterieren
		void *plDummy;			// das Objekt, immer NULL und daher obsolet
		long llStfUrno;			// die Urno als long
		CString	olBuf;			// um die Urno in einen String zu konvertieren
		CString olStfUrnoList;	// die Urno-Liste
		// alle Urnos ermitteln
		for(pos = popLoadStfUrnoMap->GetStartPosition(); pos != NULL;)   
		{
			// n�chste Urno
			popLoadStfUrnoMap->GetNextAssoc(pos,(void*&)llStfUrno,plDummy);
			// umwandeln in String
			olBuf.Format("%ld",llStfUrno);
			// ab in die Liste
			olStfUrnoList += ",'" + olBuf + "'";
		}
		// f�hrendes Komma aus Urno-Liste entfernen, WHERE-Statement generieren
		olWhere.Format("WHERE STFU IN (%s) AND SDAY BETWEEN '%s' AND '%s'",olStfUrnoList.Mid(1),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"));
		// Datens�tze einlesen und Ergebnis zur�ckgeben


			
		return Read(olWhere.GetBuffer(0),NULL,bpRegisterBC,bpClearData);   // caonima
			
	}
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// Read: liest Datens�tze ein.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaDrsData::Read(char *pspWhere /*=NULL*/, CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/,
					   bool bpRegisterBC /*= true*/, bool bpClearData /*= true*/)
{
CCS_TRY;
	// Return-Code f�r Funktionsaufrufe
	//bool ilRc = true;

	// wenn gew�nscht, aufr�umen (und beim Broadcast-Handler abmelden, wenn
	// nach Ende angemeldet werden soll)

	if (bpClearData) ClearAll(bpRegisterBC);
	
    // Datens�tze lesen
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{
	
		
		if (!CedaData::CedaAction2("RT", "")) return false;
	}
	else
	{
      

		if (!CedaData::CedaAction2("RT", pspWhere)) return false;           // caonima
	}

	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecord = 0;
	// void-Pointer f�r MA-Urno-Lookup
	void  *prlVoid = NULL;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		DRSDATA *prlDrs = new DRSDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord2(ilCountRecord,prlDrs);
		// Datensatz gelesen, Z�hler inkrementieren
		ilCountRecord++;
		// wurde eine Datensatz gelesen und ist (wenn vorhanden) die
		// Mitarbeiter-Urno dieses Datensatzes im MA-Urno-Array enthalten? 
		if(blMoreRecords && 
		   ((popLoadStfUrnoMap == NULL) || 
			(popLoadStfUrnoMap->Lookup((void *)prlDrs->Stfu, (void *&)prlVoid) == TRUE)))
		{ 
			// Datensatz hinzuf�gen
			if (!InsertInternal(prlDrs, DRS_NO_SEND_DDX))
			{
				// Fehler -> lokalen Datensatz l�schen
				if(IsTraceLoggingEnabled())
				{
					GetDefectDataString(ogRosteringLogText, (void*)prlDrs);
					WriteInRosteringLog(LOGFILE_TRACE);
				}
				delete prlDrs;
			}
#ifdef TRACE_FULL_DATA
			else
			{
				// Datensatz OK, loggen if FULL
				ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
				GetDataFormatted(ogRosteringLogText, prlDrs);
				WriteLogFull("");
			}
#endif TRACE_FULL_DATA
		}
		else
		{
			// kein weiterer Datensatz
			delete prlDrs;
		}
	} while (blMoreRecords);
	ClearFastSocketBuffer();	
	
	// beim Broadcast-Handler anmelden, wenn gew�nscht
	if (bpRegisterBC) Register();

	// Test: Anzahl der gelesenen DRS
	TRACE("Read-Drs: %d gelesen\n",ilCountRecord);

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord, pcmTableName);
		WriteInRosteringLog();
	}
	


    return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// Insert: speichert den Datensatz <prpDrs> in der Datenbank und schickt einen
//	Broadcast ab.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrsData::Insert(DRSDATA *prpDrs, bool bpSave /*= true*/)
{
CCS_TRY;
	// �nderungs-Flag setzen
	prpDrs->IsChanged = DATA_NEW;
	// in Datenbank speichern, wenn erw�nscht
	if(bpSave && Save(prpDrs) == false) return false;
	// Broadcast DRS_NEW abschicken
	InsertInternal(prpDrs,true);
    return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// InsertInternal: f�gt den internen Arrays einen Datensatz hinzu. Der Datensatz
//	muss sp�ter oder vorher explizit durch Aufruf von Save() oder Release() in der Datenbank 
//	gespeichert werden. Wenn <bpSendDdx> true ist, wird ein Broadcast gesendet.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrsData::InsertInternal(DRSDATA *prpDrs, bool bpSendDdx)
{
CCS_TRY;

	DRSDATA *prlData;
	// �bergebener Datensatz NULL oder Datensatz mit diesem Schl�ssel
	// schon vorhanden?
	if(omUrnoMap.Lookup((void *)prpDrs->Urno,(void *&)prlData)  == TRUE){
		return false;
	}

	// Datensatz intern anf�gen
	omData.Add(prpDrs);
	// Datensatz der Urno-Map hinzuf�gen
	omUrnoMap.SetAt((void *)prpDrs->Urno,prpDrs);
	// Datensatz der Drru-Map hinzuf�gen
	omDrruMap.SetAt((void *)prpDrs->Drru,prpDrs);
		
	// Broadcast senden?
	if( bpSendDdx == true )
	{
		// ja -> go!
		ogDdx.DataChanged((void *)this,DRS_NEW,(void *)prpDrs);
	}
	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// Update: sucht den Datensatz mit der Urno <prpDrs->Urno> und speichert ihn
//	in der Datenbank.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrsData::Update(DRSDATA *prpDrs)
{
CCS_TRY;
	// Datensatz raussuchen
	if (GetDrsByUrno(prpDrs->Urno) != NULL)
	{
		// �nderungsflag setzen
		if (prpDrs->IsChanged == DATA_UNCHANGED)
		{
			prpDrs->IsChanged = DATA_CHANGED;
		}
		// in die Datenbank speichern
		if(Save(prpDrs) == false) return false; 
		// Broadcast DRS_CHANGE versenden
		UpdateInternal(prpDrs);
	}
    return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// UpdateInternal: �ndert einen Datensatz, der in der internen Datenhaltung
//	enthalten ist.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrsData::UpdateInternal(DRSDATA *prpDrs, bool bpSendDdx /*true*/)
{
CCS_TRY;
// APO 9.12.99 Updates passieren immer auf Kopie eines Datensatzes der internen
// Datenhaltung, kopieren daher unn�tig.
	// DRS in lokaler Datenhaltung suchen
/*	DRSDATA *prlDrs = GetDrsByKey(prpDrs->Sday,prpDrs->Stfu,prpDrs->Drsn,prpDrs->Rosl);
	// DRS gefunden?
	if (prlDrs != NULL)
	{
		// ja -> durch Zeiger ersetzen
		*prlDrs = *prpDrs;
	}
*/
	// Broadcast senden, wenn gew�nscht
	if( bpSendDdx == true )
	{
		ogDdx.DataChanged((void *)this,DRS_CHANGE,(void *)prpDrs);
	}
	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// DeleteInternal: l�scht einen Datensatz aus den internen Arrays.
// R�ckgabe:	keine
//*********************************************************************************************

void CedaDrsData::DeleteInternal(DRSDATA *prpDrs, bool bpSendDdx /*true*/)
{
CCS_TRY;
	// Broadcast senden BEVOR der Datensatz gel�scht wird
	if(bpSendDdx == true)
	{
		ogDdx.DataChanged((void *)this,DRS_DELETE,(void *)prpDrs);
	}

	// Urno-Schl�ssel l�schen
	omUrnoMap.RemoveKey((void *)prpDrs->Urno);
	// Drru-Schl�ssel l�schen
	omDrruMap.RemoveKey((void *)prpDrs->Drru);

	// Datensatz l�schen
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpDrs->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}
CCS_CATCH_ALL;
}

//*********************************************************************************************
// RemoveInternalByDrrUrno: entfernt den DRS aus der internen Datenhaltung, der
//	dem DRR <lpDrrUrno> zugeordnet ist. Der DRS wird NICHT aus der Datenbank
//	gel�scht.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrsData::RemoveInternalByDrrUrno(long lpDrrUrno,bool bpSendBC /*= false*/)
{
CCS_TRY;
	DRSDATA *prlDrs = GetDrsByDrru(lpDrrUrno);
	if (prlDrs != NULL)
	{
		// DRS gefunden -> entfernen
		DeleteInternal(prlDrs,bpSendBC);
	}
	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// DeleteDrsByDrr: l�scht den DRS aus der Datenbank, der dem DRR mit der Urno 
//	<lpDrrUrno> zugeordnet ist.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrsData::DeleteDrsByDrrUrno(long lpDrrUrno)
{
CCS_TRY;
	DRSDATA *prlDrs = GetDrsByDrru(lpDrrUrno);
	if (prlDrs != NULL)
	{
		// DRS gefunden -> l�schen
		Delete(prlDrs,true);
	}
	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// Delete: markiert den Datensatz <prpDrs> als gel�scht und l�scht den Datensatz
//	aus der internen Datenhaltung und der Datenbank. 
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrsData::Delete(DRSDATA *prpDrs, BOOL bpWithSave)
{
CCS_TRY;
	// Flag setzen
	prpDrs->IsChanged = DATA_DELETED;
	
	// speichern
	if(bpWithSave == TRUE)
	{
		if (!Save(prpDrs)) return false;
	}

	// aus der internen Datenhaltung l�schen
	DeleteInternal(prpDrs,true);

	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// ProcessDrsBc: behandelt die einlaufenden Broadcasts.
// R�ckgabe:	keine
//*********************************************************************************************

void  CedaDrsData::ProcessDrsBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY;
	// Performance-Trace
	SYSTEMTIME rlPerf;
	GetSystemTime(&rlPerf);
	TRACE("CedaDrsData::ProcessDrsBc: START %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);

	// Datensatz-Urno
	long llUrno;
	// Daten und Info
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;
	DRSDATA *prlDrs = NULL;

		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlBcStruct->Cmd, prlBcStruct->Object, prlBcStruct->Twe, prlBcStruct->Selection, prlBcStruct->Fields, prlBcStruct->Data,prlBcStruct->BcNum);
		WriteInRosteringLog();
	}

	switch(ipDDXType)
	{
	case BC_DRS_CHANGE:	// Datensatz �ndern
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				// APO 17.04.01
				// search for something like 'urno=...' in string. if '=' is not found,
				// <ilFirst> will be -1 + 1 = 0. else <ilFirst> is the index of the first
				// char of the urno part in the string.
				int ilFirst = olSelection.Find("=")+1;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlDrs = GetDrsByUrno(llUrno);
			if(prlDrs != NULL)
			{
				// APO 17.04.01: remove entry from DRRU-Map BEFORE DRRU will be overwritten
				omDrruMap.RemoveKey((void *)prlDrs->Drru);
				// Datensatz aktualisieren mit neuen Feldwerten aus BC
				GetRecordFromItemList(prlDrs,prlBcStruct->Fields,prlBcStruct->Data);
				// internen Update-BC senden
				UpdateInternal(prlDrs);
				// APO 17.04.01: now re-enter record to DRRU-map with new DRRU
				omDrruMap.SetAt((void *)prlDrs->Drru,prlDrs);
				break;
			}
			// nein -> gehe zum Insert, !!! weil es kann sein, dass diese �nderung den Mitarbeiter in View laden soll
		}
	case BC_DRS_NEW: // neuen Datensatz einf�gen
		{
			// einzuf�gender Datensatz
			prlDrs = new DRSDATA;
			GetRecordFromItemList(prlDrs,prlBcStruct->Fields,prlBcStruct->Data);
			InsertInternal(prlDrs, DRS_SEND_DDX);
		}
		break;
	case BC_DRS_DELETE:	// Datensatz l�schen
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlDrs = GetDrsByUrno(llUrno);
			if (prlDrs != NULL)
			{
				// ja -> Datensatz l�schen
				DeleteInternal(prlDrs);
			}
		}
		break;
	default:
		break;
	}
	// Performance-Test
	GetSystemTime(&rlPerf);
	TRACE("CedaDrsData::ProcessDrsBc: END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
CCS_CATCH_ALL;
}

//*********************************************************************************************
// GetDrsByUrno: sucht den Datensatz mit der Urno <lpUrno>.
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

DRSDATA *CedaDrsData::GetDrsByUrno(long lpUrno)
{
CCS_TRY;
	// der Datensatz
	DRSDATA *prpDrs;
	// Datensatz in Urno-Map suchen
	if (omUrnoMap.Lookup((void*)lpUrno,(void *& )prpDrs) == TRUE)
	{
		return prpDrs;	// gefunden -> Zeiger zur�ckgeben
	}
	// nicht gefunden -> R�ckgabe NULL
	return NULL;
CCS_CATCH_ALL;
return NULL;
}

//*********************************************************************************************
// GetDrsByDrru: sucht den Datensatz mit der verkn�pften DRR-Urno <lpDrru>.
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

DRSDATA *CedaDrsData::GetDrsByDrru(long lpDrru)
{
CCS_TRY;
	// der Datensatz
	DRSDATA *prpDrs;
	// Datensatz in Urno-Map suchen
	if (omDrruMap.Lookup((void*)lpDrru,(void *& )prpDrs) == TRUE)
	{
		return prpDrs;	// gefunden -> Zeiger zur�ckgeben
	}
	// nicht gefunden -> R�ckgabe NULL
	return NULL;
CCS_CATCH_ALL;
return NULL;
}

//*********************************************************************************************
// ChangeDrrUrno: sucht den Datensatz mit der DRR-Urno <lpSrcDrru> und setzt den 
//	Wert auf <lpNewDrru>.
// R�ckgabe:	bool Datensatz gefunden?
//*********************************************************************************************

bool CedaDrsData::ChangeDrrUrno(long lpSrcDrru, long lpNewDrru)
{
CCS_TRY;
	// Datensatz suchen
	DRSDATA *prlDrs = GetDrsByDrru(lpSrcDrru);
	// Datensatz gefunden?
	if (prlDrs == NULL)
	{
		// nein -> terminieren
		return false;
	}
	
	// Datensatz aus interner  Datenhaltung l�schen (weil sonst die Keys nicht mehr stimmen)
	omDrruMap.RemoveKey((void *)prlDrs->Drru);
	// Drru �ndern
	prlDrs->Drru = lpNewDrru;
	prlDrs->IsChanged = DATA_CHANGED;
	// Datensatz der Drru-Map hinzuf�gen
	omDrruMap.SetAt((void *)prlDrs->Drru,prlDrs);
	
	// Datensatz speichern, keinen Broadcast senden
	return Save(prlDrs);
CCS_CATCH_ALL;
return false;
}

//*******************************************************************************
// Save: speichert den Datensatz <prpDrs>.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************

bool CedaDrsData::Save(DRSDATA *prpDrs)
{
CCS_TRY;
	bool olRc = true;
	CString olListOfData;
	char pclSelection[1024];
	char pclData[2048];

	if (prpDrs->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpDrs->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpDrs);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpDrs->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpDrs->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpDrs);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpDrs->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpDrs->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpDrs);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("DRT",pclSelection,"",pclData);
		break;
	}
	// R�ckgabe: Ergebnis der CedaAction
	return olRc;
CCS_CATCH_ALL;
return false;
}

//*************************************************************************************
// CedaAction: ruft die Funktion CedaAction der Basisklasse CCSCedaData auf.
//	Alle Parameter werden durchgereicht. Aufgrund eines internen CEDA-Bugs,
//	der daf�r sorgen kann, dass die Tabellenbeschreibung <pcmListOfFields>
//	zerst�rt wird, muss sicherheitshalber <pcmListOfFields> vor jedem Aufruf
//	von CedaData::CedaAction() gerettet und danach wiederhergestellt werden.
// R�ckgabe:	der R�ckgabewert von CedaData::CedaAction()
//*************************************************************************************

bool CedaDrsData::CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
							 char *pcpSelection, char *pcpSort, char *pcpData, 
							 char *pcpDest /*= "BUF1"*/,bool bpIsWriteAction /*= false*/, 
							 long lpID /*= 0*/, CCSPtrArray<RecordSet> *pomData /*= NULL*/, 
							 int ipFieldCount /*= 0*/)
{
CCS_TRY;
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CedaData::CedaAction(pcpAction,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID);
	// Feldliste wiederherstellen
	ClearFastSocketBuffer();	
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL;
return false;
}

//*************************************************************************************
// CedaAction: kurze Version, Beschreibung siehe oben.
// R�ckgabe:	der R�ckgabewert von CedaData::CedaAction()
//*************************************************************************************

bool CedaDrsData::CedaAction(char *pcpAction, char *pcpSelection, char *pcpSort,
							 char *pcpData, char *pcpDest /*"BUF1"*/, 
							 bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{
CCS_TRY;
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CedaData::CedaAction(pcpAction,CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,(pcpSelection != NULL)? pcpSelection: CCSCedaData::pcmSelection,(pcpSort != NULL)? pcpSort: CCSCedaData::pcmSort,pcpData,pcpDest,bpIsWriteAction, lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// CreateDrsData: initialisiert eine Struktur vom Typ DRSDATA.
// R�ckgabe:	ein Zeiger auf den ge�nderten / erzeugten Datensatz oder
//				NULL, wenn ein Fehler auftrat.
//************************************************************************************************************************************************

DRSDATA* CedaDrsData::CreateDrsData(long lpStfUrno, CString opDay, long olDrru)
{
CCS_TRY;
	// Objekt erzeugen
	DRSDATA *prlDrsData = new DRSDATA;
	// �nderungsflag setzen
	prlDrsData->IsChanged = DATA_NEW;
	// Schichttag einstellen
	strcpy(prlDrsData->Sday,opDay.GetBuffer(0));
	// Mitarbeiter-Urno
	prlDrsData->Stfu = lpStfUrno;
	// n�chste freie Datensatz-Urno ermitteln und speichern
	prlDrsData->Urno = ogBasicData.GetNextUrno();
	// DRRN setzen (Nummer des DRS bei mehreren Schichten an einem Tag)
	prlDrsData->Drru = olDrru;
	// Zeiger auf Datensatz zur�ck
	return prlDrsData;
CCS_CATCH_ALL;
return NULL;
}

//************************************************************************************************************************************************
// CopyDrsValues : Kopiert die "Nutzdaten" (keine urnos usw.)
//************************************************************************************************************************************************

void CedaDrsData::CopyDrsValues(DRSDATA* popDrsDataSource,DRSDATA* popDrsDataTarget)
{
CCS_TRY;
	// Daten kopieren
	popDrsDataTarget->Stfu = popDrsDataSource->Stfu;
	popDrsDataTarget->Ats1 = popDrsDataSource->Ats1;
	popDrsDataTarget->Drru = popDrsDataSource->Drru;
	// bda 04.10.2000: die Drsi-Felder sind nach DRR/DRA verschoben
	// strcpy(popDrsDataTarget->Drs1,popDrsDataSource->Drs1);
	// strcpy(popDrsDataTarget->Drs2,popDrsDataSource->Drs2);
	// strcpy(popDrsDataTarget->Drs3,popDrsDataSource->Drs3);
	// strcpy(popDrsDataTarget->Drs4,popDrsDataSource->Drs4);
	strcpy(popDrsDataTarget->Sday,popDrsDataSource->Sday);
	strcpy(popDrsDataTarget->Stat,popDrsDataSource->Stat);
CCS_CATCH_ALL;
}

