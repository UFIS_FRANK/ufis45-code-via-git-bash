// FidasNewDocumentDialog.cpp : implementation file
//

#include <stdafx.h>
#include <fidas.h>
#include <FidasNewDocumentDialog.h>
#include <FidasUtilities.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFidasNewDocumentDialog dialog


CFidasNewDocumentDialog::CFidasNewDocumentDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CFidasNewDocumentDialog::IDD, pParent)
{
	EnableAutomation();

	//{{AFX_DATA_INIT(CFidasNewDocumentDialog)
	m_GroupName = _T("");
	//}}AFX_DATA_INIT
	bmSaveAs = FALSE;
}


void CFidasNewDocumentDialog::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CDialog::OnFinalRelease();
}

void CFidasNewDocumentDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFidasNewDocumentDialog)
	DDX_Control(pDX, IDC_EDIT_GROUP_NAME, m_EditGroupName);
	DDX_Control(pDX, IDOK, m_ButtonOK);
	DDX_Text(pDX, IDC_EDIT_GROUP_NAME, m_GroupName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFidasNewDocumentDialog, CDialog)
	//{{AFX_MSG_MAP(CFidasNewDocumentDialog)
	ON_EN_CHANGE(IDC_EDIT_GROUP_NAME, OnChangeEditGroupName)
	ON_EN_UPDATE(IDC_EDIT_GROUP_NAME, OnUpdateEditGroupName)
	ON_EN_KILLFOCUS(IDC_EDIT_GROUP_NAME, OnKillfocusEditGroupName)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CFidasNewDocumentDialog, CDialog)
	//{{AFX_DISPATCH_MAP(CFidasNewDocumentDialog)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IFidasNewDocumentDialog to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {C3385C7D-5A35-11D5-8124-00010215BFDE}
static const IID IID_IFidasNewDocumentDialog =
{ 0xc3385c7d, 0x5a35, 0x11d5, { 0x81, 0x24, 0x0, 0x1, 0x2, 0x15, 0xbf, 0xde } };

BEGIN_INTERFACE_MAP(CFidasNewDocumentDialog, CDialog)
	INTERFACE_PART(CFidasNewDocumentDialog, IID_IFidasNewDocumentDialog, Dispatch)
END_INTERFACE_MAP()


void CFidasNewDocumentDialog::SetDocList(const CStringVector& ropDocList)
{
	omDocMap.RemoveAll();
	for (int i = 0; i < ropDocList.size(); i++)
	{
		CString olGroup = ropDocList[i];
		olGroup.MakeUpper();
		omDocMap.SetAt(olGroup,NULL);
	}
}

void CFidasNewDocumentDialog::SetMode(BOOL bpSaveAs,const CString& ropName)
{
	bmSaveAs = bpSaveAs;
	omSaveName = ropName;
	omSaveName.MakeUpper();
}

BOOL CFidasNewDocumentDialog::NameIsValid(const CString& ropName) const
{
	CString olCurrentName(ropName);
	olCurrentName.MakeUpper();

	void *ptr;
	if (!olCurrentName.GetLength())
	{
		return FALSE;
	}
	else if (bmSaveAs && olCurrentName == omSaveName)
	{
		return TRUE;
	}
	else if (!omDocMap.Lookup(olCurrentName,ptr))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/////////////////////////////////////////////////////////////////////////////
// CFidasNewDocumentDialog message handlers

BOOL CFidasNewDocumentDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	SetWindowText(CFidasUtilities::GetString(IDS_SAVE_DOCUMENT));

	CWnd *pWnd = GetDlgItem(IDC_SAVE_DOCUMENT_STATIC);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_SAVE_DOCUMENT_STATIC));

	pWnd = GetDlgItem(IDOK);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_OK));
	
	pWnd = GetDlgItem(IDCANCEL);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_CANCEL));
	

	if (!NameIsValid(m_GroupName))
	{
		m_ButtonOK.EnableWindow(FALSE);
	}
	else
	{
		m_ButtonOK.EnableWindow(TRUE);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFidasNewDocumentDialog::OnChangeEditGroupName() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	CString olCurrentName;
	m_EditGroupName.GetWindowText(olCurrentName);

	if (!CFidasUtilities::IsValidString(olCurrentName,30))
	{
		TRACE ("due to internal processing, this character is forbidden!\n");
		MessageBox(CFidasUtilities::GetString(IDS_ERROR_INVALID_CHARACTER),CFidasUtilities::GetString(IDS_ERROR_TITLE),MB_ICONHAND + MB_OK);
		UpdateData(FALSE);
	}
	else
	{
		UpdateData(TRUE);
	}

	if (!NameIsValid(olCurrentName))
	{
		m_ButtonOK.EnableWindow(FALSE);
	}
	else
	{
		m_ButtonOK.EnableWindow(TRUE);
	}
}

void CFidasNewDocumentDialog::OnUpdateEditGroupName() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function to send the EM_SETEVENTMASK message to the control
	// with the ENM_UPDATE flag ORed into the lParam mask.
	
	// TODO: Add your control notification handler code here
	CString olCurrentName;
	m_EditGroupName.GetWindowText(olCurrentName);

	if (!NameIsValid(olCurrentName))
	{
		m_ButtonOK.EnableWindow(FALSE);
	}
	else
	{
		m_ButtonOK.EnableWindow(TRUE);
	}
}

void CFidasNewDocumentDialog::OnKillfocusEditGroupName() 
{
	// TODO: Add your control notification handler code here
	
}
