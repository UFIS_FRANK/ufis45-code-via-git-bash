#if !defined(AFX_FIDASFIELDDEFINITIONPROPERTIES_H__845D5FB4_5BE4_11D5_8127_00010215BFDE__INCLUDED_)
#define AFX_FIDASFIELDDEFINITIONPROPERTIES_H__845D5FB4_5BE4_11D5_8127_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FidasFieldDefinitionProperties.h : header file
//

class CFieldDef;
class CFieldDesc;

#include <vector>
typedef std::vector<CFieldDef> CFieldDefVector;
typedef std::vector<CFieldDesc> CFieldDescVector;

/////////////////////////////////////////////////////////////////////////////
// CFidasFieldDefinitionProperties dialog

class CFidasFieldDefinitionProperties : public CDialog
{
// Construction
public:
	CFidasFieldDefinitionProperties(CWnd* pParent = NULL);   // standard constructor
	
	void SetFieldDefinition(const CString& ropGroup,CFieldDef *prpFieldDef);
	void SetNameList(const CFieldDefVector& ropNameList);	
	void SetMode(BOOL bpSaveAs,const CString& ropName);	
private:	
// Dialog Data
	//{{AFX_DATA(CFidasFieldDefinitionProperties)
	enum { IDD = IDD_FIELDDEF_PROPERTIES };
	CEdit	m_EditName;
	CButton	m_ButtonOK;
	CComboBox	m_ListLanguage;
	CComboBox	m_ComboFieldDefs;
	CButton	m_ButtonRemove;
	CButton	m_ButtonEdit;
	CButton	m_ButtonAdd;
	CString	m_Description;
	CString	m_Group;
	CString	m_Name;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFidasFieldDefinitionProperties)
	public:
	virtual void OnFinalRelease();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFidasFieldDefinitionProperties)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonEdit();
	afx_msg void OnButtonRemove();
	afx_msg void OnSelchangeComboFielddefs();
	virtual void OnOK();
	afx_msg void OnChangeEditName();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CFidasFieldDefinitionProperties)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
private: // helpers
	BOOL			NameIsValid(const CString& ropName) const;	
private:
	CFieldDef*				pomFieldDef;
	CFieldDescVector*		pomFieldDescs;
	CMapStringToPtr			omNameMap;
	BOOL					bmSaveAs;
	CString					omSaveName;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIDASFIELDDEFINITIONPROPERTIES_H__845D5FB4_5BE4_11D5_8127_00010215BFDE__INCLUDED_)
