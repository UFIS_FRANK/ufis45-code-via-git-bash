#if !defined(AFX_FIDASFIELDCONTENTDIALOG_H__564A0CC9_65FF_11D5_8133_00010215BFDE__INCLUDED_)
#define AFX_FIDASFIELDCONTENTDIALOG_H__564A0CC9_65FF_11D5_8133_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FidasFieldContentDialog.h : header file
//
#include <vector>

class CFieldDesc;
class CFieldInfo;

typedef std::vector<CString> CStringVector;
typedef std::vector<CFieldInfo> CFieldInfoVector;

/////////////////////////////////////////////////////////////////////////////
// CFidasFieldContentDialog dialog

class CFidasFieldContentDialog : public CDialog
{
// Construction
public:
	CFidasFieldContentDialog(CWnd* pParent = NULL);   // standard constructor
	void SetFieldDescription(CFieldDesc *prpFieldDesc);
private:
// Dialog Data
	//{{AFX_DATA(CFidasFieldContentDialog)
	enum { IDD = IDD_UFIS_FIELDCONTENT };
	BOOL	bmRange;
	CString	omSeparator;
	int		imRangeStart;
	int		imRangeEnd;
	CString	omSecondField;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFidasFieldContentDialog)
	public:
	virtual void OnFinalRelease();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFidasFieldContentDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnUfisFielddefCancelButton();
	afx_msg void OnUfisFielddefOkButton();
	afx_msg void OnUfisFielddefSelectsecondfieldButton();
	afx_msg void OnChangeUfisFielddefSeparatedbyEdit();
	afx_msg void OnUfisFielddefRangeCheck();
	afx_msg void OnChangeUfisFielddefEditEnd();
	afx_msg void OnChangeUfisFielddefEditStart();
	afx_msg void OnKillfocusUfisFielddefEditEnd();
	afx_msg void OnKillfocusUfisFielddefEditStart();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CFidasFieldContentDialog)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
private:
	CFieldDesc*			pomFieldDesc;
	int					imRangeMin;
	int					imRangeMax;
	CFieldInfoVector	omFieldInfos;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIDASFIELDCONTENTDIALOG_H__564A0CC9_65FF_11D5_8133_00010215BFDE__INCLUDED_)
