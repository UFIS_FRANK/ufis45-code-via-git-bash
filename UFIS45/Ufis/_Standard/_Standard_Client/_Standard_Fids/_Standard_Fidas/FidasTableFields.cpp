// FidasUtilities.cpp : Defines the class behaviors for the application.
//

#include <stdafx.h>
#include <FidasTableFields.h>
#include <FidasUtilities.h>
#include <MainFrm.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFidasTableFields

CFidasTableFields::CFidasTableFields(const CString& ropTableName)
{
	ASSERT(ropTableName.GetLength());
	omTableName = ropTableName;
}

CFidasTableFields::~CFidasTableFields()
{
}

BOOL CFidasTableFields::LoadTableFields(CUfisCom *popUfisCom)
{
	ASSERT(popUfisCom);

	CString olTableName		= omTableName+CFidasUtilities::TableExtension();
	CString olWhere;
	olWhere.Format("where LTNA='%s'",olTableName);
//	pMainFrm->omUfisCom.CallServer("RT","TABTAB","LTNA,LNAM","",olWhere,"240");

	CString olSysTableName	= "SYS" + CFidasUtilities::TableExtension();
	olWhere.Format("where TANA = '%s'",omTableName);
	if (popUfisCom->CallServer("RT",olSysTableName,"FINA,LABL,STAT,TYPE,FELE","",olWhere,"240") == 0)
	{
		for (int ilC = 0; ilC < popUfisCom->GetBufferCount(); ilC++)
		{
			CString olBuffer(popUfisCom->GetBufferLine(ilC));		
			TRACE("Reading line [%d] : [%s]\n",ilC,olBuffer);

			CStringArray olItems;
			if (CFidasUtilities::ExtractTextLineFast(olItems,olBuffer,",") == 5)
			{
				CString olStat = olItems[2];
				if (olStat.GetLength() && (olStat[0] == 'P' || olStat[0] == 'Y'))
				{
					CFieldInfo olInfo;
					olInfo.csName = olItems[0];
					olInfo.csName.TrimRight();

					olInfo.csTranslation = olItems[1];
					olInfo.csTranslation.TrimRight();

					if (!olInfo.csTranslation.GetLength())
						olInfo.csTranslation = olInfo.csName;

					olInfo.csType = olItems[3];
					olInfo.csType.TrimRight();

					int ic = sscanf(olItems[4],"%d",&olInfo.ciLength);
					VERIFY(ic == 1);

					omFieldInfos.push_back(olInfo);
				}
			}
		}
	}

#if	0	
	CString olTxtTableName	= "TXT" + CFidasUtilities::TableExtension();
	for (int i = 0; i < omFieldNames.size(); i++)
	{
		olWhere.Format("WHERE COCO='%s' AND TXID='%s",CFidasUtilities::Language(),omTableName+'.'+omFieldNames[i]);
		if (popUfisCom->CallServer("RT",olTxtTableName,"STRG","",olWhere,"240") == 0)
		{
			for (int ilC = 0; ilC < popUfisCom->GetBufferCount(); ilC++)
			{
				CString olBuffer(popUfisCom->GetBufferLine(ilC));		
				TRACE("Reading line [%d] : [%s]\n",ilC,olBuffer);
				if (olBuffer.GetLength())
				{
					CString& rolBuffer = omFieldDescriptions.at(i);
					rolBuffer = olBuffer;
				}
			}
		}
	}
#endif
	return TRUE;
}

int	CFidasTableFields::GetFieldNames(CStringVector& ropNames) const
{
	for (int i = 0; i < omFieldInfos.size(); i++)
	{
		const CFieldInfo& rolInfo = omFieldInfos.at(i);
		ropNames.push_back(rolInfo.csName);
	}
	return ropNames.size();
}

int	CFidasTableFields::GetFieldDescriptions(CStringVector& ropDesc) const
{
	for (int i = 0; i < omFieldInfos.size(); i++)
	{
		const CFieldInfo& rolInfo = omFieldInfos.at(i);
		ropDesc.push_back(rolInfo.csTranslation);
	}
	return ropDesc.size();
}

int	CFidasTableFields::GetFieldInfos(CFieldInfoVector& ropInfo) const
{
	ropInfo = omFieldInfos;
	return ropInfo.size();
}
