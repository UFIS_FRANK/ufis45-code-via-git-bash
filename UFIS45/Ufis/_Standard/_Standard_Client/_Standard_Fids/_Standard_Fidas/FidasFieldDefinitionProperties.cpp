// FidasFieldDefinitionProperties.cpp : implementation file
//

#include <stdafx.h>
#include <fidas.h>
#include <FidasFieldDefinitionProperties.h>
#include <FidasFieldTypesDialog.h>
#include <FidasUtilities.h>
#include <FidasLogoDialog.h>
#include <FidasSelectTableFieldDialog.h>
#include <FidasDateTimeFieldDialog.h>
#include <FidasStaticFieldDialog.h>
#include <FidasRemarkDialog.h>
#include <FidasFieldContentDialog.h>
#include <FidasAirportDialog.h>
#include <FidasAirlineDialog.h>

#include <CFDDoc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFidasFieldDefinitionProperties dialog


CFidasFieldDefinitionProperties::CFidasFieldDefinitionProperties(CWnd* pParent /*=NULL*/)
	: CDialog(CFidasFieldDefinitionProperties::IDD, pParent)
{
	EnableAutomation();

	//{{AFX_DATA_INIT(CFidasFieldDefinitionProperties)
	m_Description = _T("");
	m_Group = _T("");
	m_Name = _T("");
	//}}AFX_DATA_INIT
	pomFieldDef		= NULL;
	pomFieldDescs	= new CFieldDescVector;
	bmSaveAs		= FALSE;
}


void CFidasFieldDefinitionProperties::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CDialog::OnFinalRelease();
}

void CFidasFieldDefinitionProperties::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFidasFieldDefinitionProperties)
	DDX_Control(pDX, IDC_EDIT_NAME, m_EditName);
	DDX_Control(pDX, IDOK, m_ButtonOK);
	DDX_Control(pDX, IDC_COMBO_LANGUAGE, m_ListLanguage);
	DDX_Control(pDX, IDC_COMBO_FIELDDEFS, m_ComboFieldDefs);
	DDX_Control(pDX, IDC_BUTTON_REMOVE, m_ButtonRemove);
	DDX_Control(pDX, IDC_BUTTON_EDIT, m_ButtonEdit);
	DDX_Control(pDX, IDC_BUTTON_ADD, m_ButtonAdd);
	DDX_Text(pDX, IDC_EDIT_DESCRIPTION, m_Description);
	DDX_Text(pDX, IDC_EDIT_GROUP, m_Group);
	DDX_Text(pDX, IDC_EDIT_NAME, m_Name);
	DDV_MaxChars(pDX, m_Name, 30);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFidasFieldDefinitionProperties, CDialog)
	//{{AFX_MSG_MAP(CFidasFieldDefinitionProperties)
	ON_BN_CLICKED(IDC_BUTTON_ADD, OnButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_EDIT, OnButtonEdit)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE, OnButtonRemove)
	ON_CBN_SELCHANGE(IDC_COMBO_FIELDDEFS, OnSelchangeComboFielddefs)
	ON_EN_CHANGE(IDC_EDIT_NAME, OnChangeEditName)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CFidasFieldDefinitionProperties, CDialog)
	//{{AFX_DISPATCH_MAP(CFidasFieldDefinitionProperties)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IFidasFieldDefinitionProperties to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {845D5FB3-5BE4-11D5-8127-00010215BFDE}
static const IID IID_IFidasFieldDefinitionProperties =
{ 0x845d5fb3, 0x5be4, 0x11d5, { 0x81, 0x27, 0x0, 0x1, 0x2, 0x15, 0xbf, 0xde } };

BEGIN_INTERFACE_MAP(CFidasFieldDefinitionProperties, CDialog)
	INTERFACE_PART(CFidasFieldDefinitionProperties, IID_IFidasFieldDefinitionProperties, Dispatch)
END_INTERFACE_MAP()


void CFidasFieldDefinitionProperties::SetFieldDefinition(const CString& ropGroup,CFieldDef *prpFieldDef)
{
	pomFieldDef = prpFieldDef;

	// save field description for undo
	if (pomFieldDef && pomFieldDef->FieldDesc())
	{
		*pomFieldDescs = *(pomFieldDef->FieldDesc());
	}

	m_Group     = ropGroup;
}

void CFidasFieldDefinitionProperties::SetNameList(const CFieldDefVector& ropDocList)
{
	omNameMap.RemoveAll();
	for (int i = 0; i < ropDocList.size(); i++)
	{
		CString olGroup = ropDocList[i].GetName();
		olGroup.MakeUpper();
		omNameMap.SetAt(olGroup,NULL);
	}
}

void CFidasFieldDefinitionProperties::SetMode(BOOL bpSaveAs,const CString& ropName)
{
	bmSaveAs   = bpSaveAs;
	omSaveName = ropName;
	omSaveName.MakeUpper();
}

BOOL CFidasFieldDefinitionProperties::NameIsValid(const CString& ropName) const
{
	CString olCurrentName(ropName);
	olCurrentName.MakeUpper();

	if (m_ComboFieldDefs.GetCount() == 0)
		return FALSE;

	void *ptr;
	if (!olCurrentName.GetLength())
	{
		return FALSE;
	}
	else if (bmSaveAs && olCurrentName == omSaveName)
	{
		return TRUE;
	}
	else if (!omNameMap.Lookup(olCurrentName,ptr))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/////////////////////////////////////////////////////////////////////////////
// CFidasFieldDefinitionProperties message handlers

BOOL CFidasFieldDefinitionProperties::OnInitDialog() 
{
	ASSERT(pomFieldDef);

	if (!pomFieldDef)
		return FALSE;

	m_Name  = pomFieldDef->GetName();
	m_Description = pomFieldDef->GetDescription();

	CDialog::OnInitDialog();
	// TODO: Add extra initialization here
	SetWindowText(CFidasUtilities::GetString(IDS_FIELDDEF_PROPERTIES));
	CWnd *pWnd = GetDlgItem(IDC_STATIC_NAME);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_FIELDDEF_PROPERTIES_STATIC_NAME));

	pWnd = GetDlgItem(IDC_STATIC_GROUP);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_FIELDDEF_PROPERTIES_STATIC_GROUP));

	pWnd = GetDlgItem(IDC_STATIC_DESCRIPTION);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_FIELDDEF_PROPERTIES_STATIC_DESCRIPTION));

	pWnd = GetDlgItem(IDC_STATIC_LANGUAGE);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_FIELDDEF_PROPERTIES_STATIC_LANGUAGE));

	pWnd = GetDlgItem(IDC_STATIC_FIELDDEFS);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_FIELDDEF_PROPERTIES_STATIC_FIELDDEFS));

	pWnd = GetDlgItem(IDC_BUTTON_ADD);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_FIELDDEF_PROPERTIES_BUTTON_ADD));

	pWnd = GetDlgItem(IDC_BUTTON_EDIT);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_FIELDDEF_PROPERTIES_BUTTON_EDIT));

	pWnd = GetDlgItem(IDC_BUTTON_REMOVE);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_FIELDDEF_PROPERTIES_BUTTON_REMOVE));

	pWnd = GetDlgItem(IDOK);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_OK));
	
	pWnd = GetDlgItem(IDCANCEL);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_CANCEL));
	

	int ind = m_ListLanguage.AddString(CFidasUtilities::GetString(IDS_LANGUAGE_LATIN));
	m_ListLanguage.SetItemData(ind,0);

	ind = m_ListLanguage.AddString(CFidasUtilities::GetString(IDS_LANGUAGE_ARABIC));
	m_ListLanguage.SetItemData(ind,1);

	for (int i = 0; i < m_ListLanguage.GetCount();i++)
	{
		if (m_ListLanguage.GetItemData(i) == pomFieldDef->GetLang())
		{
			m_ListLanguage.SetCurSel(i);
			break;
		}
	}

	CString olCurrentName;
	m_EditName.GetWindowText(olCurrentName);

	std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
	if (polDescs)
	{
		pomFieldDef->SetFieldDesc(polDescs);
		for (int i = 0; i < polDescs->size(); i++)
		{
			CFieldDesc *polDesc = &polDescs->at(i);
			ASSERT(polDesc);
			int ind = m_ComboFieldDefs.AddString(polDesc->ToString());
			ASSERT (ind != CB_ERR);
			m_ComboFieldDefs.SetItemData(ind,i);
		}

		if (m_ComboFieldDefs.GetCount())
			m_ComboFieldDefs.SetCurSel(0);
	}
	else
	{
		polDescs = new std::vector<CFieldDesc>;
		pomFieldDef->SetFieldDesc(polDescs);
	}

	if (!olCurrentName.GetLength())
	{
		m_ButtonOK.EnableWindow(FALSE);
	}
	else
	{
		m_ButtonOK.EnableWindow(TRUE);
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFidasFieldDefinitionProperties::OnButtonAdd() 
{
	// TODO: Add your control notification handler code here
	CFidasFieldTypesDialog dlg(this);
	if (dlg.DoModal() != IDOK)
		return;
	switch(dlg.GetSelectedType())
	{
	case CDpyType::N:	// NORMAL
		{
			CFieldDesc olDesc;
			CDpyType_N *polDpyType = new CDpyType_N;
			olDesc.SetDpyType(polDpyType);

			CFieldRef *polFldRef = new CFieldRef;
			polFldRef->SetStart(0);
			polFldRef->SetEnd(0);
			olDesc.SetFieldRef(polFldRef);

			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(&olDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
			ASSERT(polDescs);
			polDescs->push_back(olDesc);
			pomFieldDef->SetFieldDesc(polDescs);

			int ind = m_ComboFieldDefs.AddString(olDesc.ToString());
			ASSERT(ind != CB_ERR);
			m_ComboFieldDefs.SetItemData(ind,polDescs->size()-1);
			m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::M:	// NORMAL WITHOUT BLANKS
		{
			CFieldDesc olDesc;
			CDpyType_M *polDpyType = new CDpyType_M;
			olDesc.SetDpyType(polDpyType);

			CFieldRef *polFldRef = new CFieldRef;
			polFldRef->SetStart(0);
			polFldRef->SetEnd(0);
			olDesc.SetFieldRef(polFldRef);

			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(&olDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
			ASSERT(polDescs);
			polDescs->push_back(olDesc);
			pomFieldDef->SetFieldDesc(polDescs);

			int ind = m_ComboFieldDefs.AddString(olDesc.ToString());
			ASSERT(ind != CB_ERR);
			m_ComboFieldDefs.SetItemData(ind,polDescs->size()-1);
			m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::J:	// FLIGHT NUMBER WITH CODE SHARES
		{
			CFieldDesc olDesc;
			CDpyType_J *polDpyType = new CDpyType_J;
			olDesc.SetDpyType(polDpyType);

			CFieldRef *polFldRef = new CFieldRef;
			polFldRef->SetStart(0);
			polFldRef->SetEnd(0);
			olDesc.SetFieldRef(polFldRef);

			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(&olDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
			ASSERT(polDescs);
			polDescs->push_back(olDesc);
			pomFieldDef->SetFieldDesc(polDescs);

			int ind = m_ComboFieldDefs.AddString(olDesc.ToString());
			ASSERT(ind != CB_ERR);
			m_ComboFieldDefs.SetItemData(ind,polDescs->size()-1);
			m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::S:	// STATIC
		{
			CFieldDesc olDesc;
			CDpyType_S *polDpyType = new CDpyType_S;
			olDesc.SetDpyType(polDpyType);

			CFidasStaticFieldDialog dlg(this);
			dlg.SetFieldDescription(&olDesc);
			if (dlg.DoModal() != IDOK)
				break;

			std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
			ASSERT(polDescs);
			polDescs->push_back(olDesc);
			pomFieldDef->SetFieldDesc(polDescs);
			int ind = m_ComboFieldDefs.AddString(olDesc.ToString());
			ASSERT(ind != CB_ERR);
			m_ComboFieldDefs.SetItemData(ind,polDescs->size()-1);
			m_ComboFieldDefs.SetCurSel(ind);
		}

	break;
	case CDpyType::L:	// LOGO
		{
			CFieldDesc olDesc;
			CDpyType_L *polDpyType = new CDpyType_L;
			olDesc.SetDpyType(polDpyType);

			CFieldRef *polFldRef = new CFieldRef;
			polFldRef->SetStart(0);
			polFldRef->SetEnd(0);
			olDesc.SetFieldRef(polFldRef);


			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(&olDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			CFidasLogoDialog dlg(this);
			dlg.SetFieldDescription(&olDesc);
			if (dlg.DoModal() != IDOK)
				break;

			std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
			ASSERT(polDescs);
			polDescs->push_back(olDesc);
			pomFieldDef->SetFieldDesc(polDescs);

			int ind = m_ComboFieldDefs.AddString(olDesc.ToString());
			ASSERT(ind != CB_ERR);
			m_ComboFieldDefs.SetItemData(ind,polDescs->size()-1);
			m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::LJ:	// LOGO WITH CODE SHARES
		{
			CFieldDesc olDesc;
			CDpyType_LJ *polDpyType = new CDpyType_LJ;
			olDesc.SetDpyType(polDpyType);

			CFieldRef *polFldRef = new CFieldRef;
			polFldRef->SetStart(0);
			polFldRef->SetEnd(0);
			olDesc.SetFieldRef(polFldRef);


			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(&olDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			CFidasLogoDialog dlg(this);
			dlg.SetFieldDescription(&olDesc);
			if (dlg.DoModal() != IDOK)
				break;

			std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
			ASSERT(polDescs);
			polDescs->push_back(olDesc);
			pomFieldDef->SetFieldDesc(polDescs);

			int ind = m_ComboFieldDefs.AddString(olDesc.ToString());
			ASSERT(ind != CB_ERR);
			m_ComboFieldDefs.SetItemData(ind,polDescs->size()-1);
			m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::T:	// TIME
		{
			CFieldDesc olDesc;
			CDpyType_T *polDpyType = new CDpyType_T;
			olDesc.SetDpyType(polDpyType);

			CFieldRef *polFldRef = new CFieldRef;
			polFldRef->SetStart(0);
			polFldRef->SetEnd(0);
			olDesc.SetFieldRef(polFldRef);

			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(&olDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			FidasDateTimeFieldDialog dlg(this);
			dlg.SetFieldDescription(&olDesc);
			if (dlg.DoModal() != IDOK)
				break;

			std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
			ASSERT(polDescs);
			polDescs->push_back(olDesc);
			pomFieldDef->SetFieldDesc(polDescs);

			int ind = m_ComboFieldDefs.AddString(olDesc.ToString());
			ASSERT(ind != CB_ERR);
			m_ComboFieldDefs.SetItemData(ind,polDescs->size()-1);
			m_ComboFieldDefs.SetCurSel(ind);
			
		}
	break;
	case CDpyType::D:	// DATE
		{
			CFieldDesc olDesc;
			CDpyType_D *polDpyType = new CDpyType_D;
			olDesc.SetDpyType(polDpyType);

			CFieldRef *polFldRef = new CFieldRef;
			polFldRef->SetStart(0);
			polFldRef->SetEnd(0);
			olDesc.SetFieldRef(polFldRef);

			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(&olDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			FidasDateTimeFieldDialog dlg(this);
			dlg.SetFieldDescription(&olDesc);
			if (dlg.DoModal() != IDOK)
				break;

			std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
			ASSERT(polDescs);
			polDescs->push_back(olDesc);
			pomFieldDef->SetFieldDesc(polDescs);

			int ind = m_ComboFieldDefs.AddString(olDesc.ToString());
			ASSERT(ind != CB_ERR);
			m_ComboFieldDefs.SetItemData(ind,polDescs->size()-1);
			m_ComboFieldDefs.SetCurSel(ind);
			
		}
	break;
	case CDpyType::DT:	// DATE TIME
		{
			CFieldDesc olDesc;
			CDpyType_DT *polDpyType = new CDpyType_DT;
			olDesc.SetDpyType(polDpyType);

			CFieldRef *polFldRef = new CFieldRef;
			polFldRef->SetStart(0);
			polFldRef->SetEnd(0);
			olDesc.SetFieldRef(polFldRef);

			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(&olDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			FidasDateTimeFieldDialog dlg(this);
			dlg.SetFieldDescription(&olDesc);
			if (dlg.DoModal() != IDOK)
				break;

			std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
			ASSERT(polDescs);
			polDescs->push_back(olDesc);
			pomFieldDef->SetFieldDesc(polDescs);

			int ind = m_ComboFieldDefs.AddString(olDesc.ToString());
			ASSERT(ind != CB_ERR);
			m_ComboFieldDefs.SetItemData(ind,polDescs->size()-1);
			m_ComboFieldDefs.SetCurSel(ind);
			
		}
	break;
	case CDpyType::CT:	// CURRENT TIME
		{
			CFieldDesc olDesc;
			CDpyType_CT *polDpyType = new CDpyType_CT;
			olDesc.SetDpyType(polDpyType);

			CFieldRef *polFldRef = new CFieldRef;
			polFldRef->SetStart(0);
			polFldRef->SetEnd(0);
			olDesc.SetFieldRef(polFldRef);

			FidasDateTimeFieldDialog dlg(this);
			dlg.SetFieldDescription(&olDesc);
			if (dlg.DoModal() != IDOK)
				break;

			std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
			ASSERT(polDescs);
			polDescs->push_back(olDesc);
			pomFieldDef->SetFieldDesc(polDescs);

			int ind = m_ComboFieldDefs.AddString(olDesc.ToString());
			ASSERT(ind != CB_ERR);
			m_ComboFieldDefs.SetItemData(ind,polDescs->size()-1);
			m_ComboFieldDefs.SetCurSel(ind);
			
		}
	break;
	case CDpyType::CD:	// CURRENT DATE
		{
			CFieldDesc olDesc;
			CDpyType_CD *polDpyType = new CDpyType_CD;
			olDesc.SetDpyType(polDpyType);

			CFieldRef *polFldRef = new CFieldRef;
			polFldRef->SetStart(0);
			polFldRef->SetEnd(0);
			olDesc.SetFieldRef(polFldRef);

			FidasDateTimeFieldDialog dlg(this);
			dlg.SetFieldDescription(&olDesc);
			if (dlg.DoModal() != IDOK)
				break;

			std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
			ASSERT(polDescs);
			polDescs->push_back(olDesc);
			pomFieldDef->SetFieldDesc(polDescs);

			int ind = m_ComboFieldDefs.AddString(olDesc.ToString());
			ASSERT(ind != CB_ERR);
			m_ComboFieldDefs.SetItemData(ind,polDescs->size()-1);
			m_ComboFieldDefs.SetCurSel(ind);
			
		}
	break;
	case CDpyType::CDT:	// CURRENT DATE TIME
		{
			CFieldDesc olDesc;
			CDpyType_CDT *polDpyType = new CDpyType_CDT;
			olDesc.SetDpyType(polDpyType);

			CFieldRef *polFldRef = new CFieldRef;
			polFldRef->SetStart(0);
			polFldRef->SetEnd(0);
			olDesc.SetFieldRef(polFldRef);

			FidasDateTimeFieldDialog dlg(this);
			dlg.SetFieldDescription(&olDesc);
			if (dlg.DoModal() != IDOK)
				break;

			std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
			ASSERT(polDescs);
			polDescs->push_back(olDesc);
			pomFieldDef->SetFieldDesc(polDescs);

			int ind = m_ComboFieldDefs.AddString(olDesc.ToString());
			ASSERT(ind != CB_ERR);
			m_ComboFieldDefs.SetItemData(ind,polDescs->size()-1);
			m_ComboFieldDefs.SetCurSel(ind);
			
		}
	break;
	case CDpyType::TT:	// CURRENT TIME if referenced field is empty
		{
			CFieldDesc olDesc;
			CDpyType_TT *polDpyType = new CDpyType_TT;
			olDesc.SetDpyType(polDpyType);

			CFieldRef *polFldRef = new CFieldRef;
			polFldRef->SetStart(0);
			polFldRef->SetEnd(0);
			olDesc.SetFieldRef(polFldRef);

			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(&olDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			FidasDateTimeFieldDialog dlg(this);
			dlg.SetFieldDescription(&olDesc);
			if (dlg.DoModal() != IDOK)
				break;

			std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
			ASSERT(polDescs);
			polDescs->push_back(olDesc);
			pomFieldDef->SetFieldDesc(polDescs);

			int ind = m_ComboFieldDefs.AddString(olDesc.ToString());
			ASSERT(ind != CB_ERR);
			m_ComboFieldDefs.SetItemData(ind,polDescs->size()-1);
			m_ComboFieldDefs.SetCurSel(ind);
			
		}
	break;
	case CDpyType::TD:	// CURRENT DATE if referenced field is empty
		{
			CFieldDesc olDesc;
			CDpyType_TD *polDpyType = new CDpyType_TD;
			olDesc.SetDpyType(polDpyType);

			CFieldRef *polFldRef = new CFieldRef;
			polFldRef->SetStart(0);
			polFldRef->SetEnd(0);
			olDesc.SetFieldRef(polFldRef);

			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(&olDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			FidasDateTimeFieldDialog dlg(this);
			dlg.SetFieldDescription(&olDesc);
			if (dlg.DoModal() != IDOK)
				break;

			std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
			ASSERT(polDescs);
			polDescs->push_back(olDesc);
			pomFieldDef->SetFieldDesc(polDescs);

			int ind = m_ComboFieldDefs.AddString(olDesc.ToString());
			ASSERT(ind != CB_ERR);
			m_ComboFieldDefs.SetItemData(ind,polDescs->size()-1);
			m_ComboFieldDefs.SetCurSel(ind);
			
		}
	break;
	case CDpyType::TDT:	// CURRENT DATE TIME if referenced field is empty
		{
			CFieldDesc olDesc;
			CDpyType_TDT *polDpyType = new CDpyType_TDT;
			olDesc.SetDpyType(polDpyType);

			CFieldRef *polFldRef = new CFieldRef;
			polFldRef->SetStart(0);
			polFldRef->SetEnd(0);
			olDesc.SetFieldRef(polFldRef);

			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(&olDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			FidasDateTimeFieldDialog dlg(this);
			dlg.SetFieldDescription(&olDesc);
			if (dlg.DoModal() != IDOK)
				break;

			std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
			ASSERT(polDescs);
			polDescs->push_back(olDesc);
			pomFieldDef->SetFieldDesc(polDescs);

			int ind = m_ComboFieldDefs.AddString(olDesc.ToString());
			ASSERT(ind != CB_ERR);
			m_ComboFieldDefs.SetItemData(ind,polDescs->size()-1);
			m_ComboFieldDefs.SetCurSel(ind);
			
		}
	break;
	case CDpyType::R:	// Separator field
		{
			CFieldDesc olDesc;
			CDpyType_R *polDpyType = new CDpyType_R;
			olDesc.SetDpyType(polDpyType);

			CFieldRef *polFldRef = new CFieldRef;
			polFldRef->SetStart(0);
			polFldRef->SetEnd(0);
			olDesc.SetFieldRef(polFldRef);

			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(&olDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			CFidasFieldContentDialog dlg(this);
			dlg.SetFieldDescription(&olDesc);
			if (dlg.DoModal() != IDOK)
				break;

			std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
			ASSERT(polDescs);
			polDescs->push_back(olDesc);
			pomFieldDef->SetFieldDesc(polDescs);

			int ind = m_ComboFieldDefs.AddString(olDesc.ToString());
			ASSERT(ind != CB_ERR);
			m_ComboFieldDefs.SetItemData(ind,polDescs->size()-1);
			m_ComboFieldDefs.SetCurSel(ind);
			
		}
	break;
	case CDpyType::B:	// REMARK 
		{
			CFieldDesc olDesc;
			CDpyType_B *polDpyType = new CDpyType_B;
			olDesc.SetDpyType(polDpyType);

			CFieldRef *polFldRef = new CFieldRef;
			polFldRef->SetStart(0);
			polFldRef->SetEnd(0);
			olDesc.SetFieldRef(polFldRef);

			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(&olDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			CFidasRemarkDialog dlg(this);
			dlg.SetFieldDescription(&olDesc);
			if (dlg.DoModal() != IDOK)
				break;

			std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
			ASSERT(polDescs);
			polDescs->push_back(olDesc);
			pomFieldDef->SetFieldDesc(polDescs);

			int ind = m_ComboFieldDefs.AddString(olDesc.ToString());
			ASSERT(ind != CB_ERR);
			m_ComboFieldDefs.SetItemData(ind,polDescs->size()-1);
			m_ComboFieldDefs.SetCurSel(ind);
			
		}
	break;
	case CDpyType::A:	// AIRPORTS
		{
			CFieldDesc olDesc;
			CDpyType_A *polDpyType = new CDpyType_A;
			olDesc.SetDpyType(polDpyType);

			CFieldRef *polFldRef = new CFieldRef;
			polFldRef->SetStart(0);
			polFldRef->SetEnd(0);
			olDesc.SetFieldRef(polFldRef);

			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(&olDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			CFidasAirportDialog dlg(this);
			dlg.SetFieldDescription(&olDesc);
			if (dlg.DoModal() != IDOK)
				break;

			std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
			ASSERT(polDescs);
			polDescs->push_back(olDesc);
			pomFieldDef->SetFieldDesc(polDescs);

			int ind = m_ComboFieldDefs.AddString(olDesc.ToString());
			ASSERT(ind != CB_ERR);
			m_ComboFieldDefs.SetItemData(ind,polDescs->size()-1);
			m_ComboFieldDefs.SetCurSel(ind);
			
		}
	break;
	case CDpyType::AL:	// AIRLINES
		{
			CFieldDesc olDesc;
			CDpyType_AL *polDpyType = new CDpyType_AL;
			olDesc.SetDpyType(polDpyType);

			CFieldRef *polFldRef = new CFieldRef;
			polFldRef->SetStart(0);
			polFldRef->SetEnd(0);
			olDesc.SetFieldRef(polFldRef);

			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(&olDesc);
			if (dlg2.DoModal() != IDOK)
				break;

#if	0
			CFidasAirlineDialog dlg(this);
			dlg.SetFieldDescription(&olDesc);
			if (dlg.DoModal() != IDOK)
				break;
#else
			polDpyType->SetFieldName("ALFN");
#endif
			std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
			ASSERT(polDescs);
			polDescs->push_back(olDesc);
			pomFieldDef->SetFieldDesc(polDescs);

			int ind = m_ComboFieldDefs.AddString(olDesc.ToString());
			ASSERT(ind != CB_ERR);
			m_ComboFieldDefs.SetItemData(ind,polDescs->size()-1);
			m_ComboFieldDefs.SetCurSel(ind);
			
		}
	break;
	default:
		AfxMessageBox("Format not supported yet");
	}

	OnSelchangeComboFielddefs();
}

void CFidasFieldDefinitionProperties::OnButtonEdit() 
{
	// TODO: Add your control notification handler code here
	int ind = m_ComboFieldDefs.GetCurSel();
	if (ind < 0)
		return;

	int ilIndex = m_ComboFieldDefs.GetItemData(ind);
	std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
	ASSERT(polDescs && ilIndex >= 0 && ilIndex < polDescs->size());

	CFieldDesc *polDesc = &polDescs->at(ilIndex);
	ASSERT(polDesc);

	pomFieldDef->SetFieldDesc(polDescs);
	const CDpyType *polType = polDesc->DpyType();
	ASSERT(polType);

	switch(polType->GetType())
	{
	case CDpyType::N:	// NORMAL
		{
			CFidasSelectTableFieldDialog dlg(this);
			dlg.SetFieldDescription(polDesc);
			if (dlg.DoModal() != IDOK)
				break;
			m_ComboFieldDefs.DeleteString(ind);
			m_ComboFieldDefs.InsertString(ind,polDesc->ToString());
			m_ComboFieldDefs.SetItemData(ind,ilIndex);
			if (m_ComboFieldDefs.GetCurSel() == CB_ERR)
				m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::M:	// NORMAL WITHOUT BLANKS
		{
			CFidasSelectTableFieldDialog dlg(this);
			dlg.SetFieldDescription(polDesc);
			if (dlg.DoModal() != IDOK)
				break;
			m_ComboFieldDefs.DeleteString(ind);
			m_ComboFieldDefs.InsertString(ind,polDesc->ToString());
			m_ComboFieldDefs.SetItemData(ind,ilIndex);
			if (m_ComboFieldDefs.GetCurSel() == CB_ERR)
				m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::J:	// FLIGHTNUMBER WITH CODE SHARES
		{
			CFidasSelectTableFieldDialog dlg(this);
			dlg.SetFieldDescription(polDesc);
			if (dlg.DoModal() != IDOK)
				break;
			m_ComboFieldDefs.DeleteString(ind);
			m_ComboFieldDefs.InsertString(ind,polDesc->ToString());
			m_ComboFieldDefs.SetItemData(ind,ilIndex);
			if (m_ComboFieldDefs.GetCurSel() == CB_ERR)
				m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::S:	// STATIC
		{
			CFidasStaticFieldDialog dlg(this);
			dlg.SetFieldDescription(polDesc);
			if (dlg.DoModal() != IDOK)
				break;
			m_ComboFieldDefs.DeleteString(ind);
			m_ComboFieldDefs.InsertString(ind,polDesc->ToString());
			m_ComboFieldDefs.SetItemData(ind,ilIndex);
			if (m_ComboFieldDefs.GetCurSel() == CB_ERR)
				m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::L:	// LOGO
		{
			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(polDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			CFidasLogoDialog dlg(this);
			dlg.SetFieldDescription(polDesc);
			if (dlg.DoModal() != IDOK)
				break;

			m_ComboFieldDefs.DeleteString(ind);
			m_ComboFieldDefs.InsertString(ind,polDesc->ToString());
			m_ComboFieldDefs.SetItemData(ind,ilIndex);
			if (m_ComboFieldDefs.GetCurSel() == CB_ERR)
				m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::LJ:	// LOGO WITH CODE SHARES
		{
			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(polDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			CFidasLogoDialog dlg(this);
			dlg.SetFieldDescription(polDesc);
			if (dlg.DoModal() != IDOK)
				break;

			m_ComboFieldDefs.DeleteString(ind);
			m_ComboFieldDefs.InsertString(ind,polDesc->ToString());
			m_ComboFieldDefs.SetItemData(ind,ilIndex);
			if (m_ComboFieldDefs.GetCurSel() == CB_ERR)
				m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::T:	// TIME
		{
			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(polDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			FidasDateTimeFieldDialog dlg(this);
			dlg.SetFieldDescription(polDesc);
			if (dlg.DoModal() != IDOK)
				break;

			m_ComboFieldDefs.DeleteString(ind);
			m_ComboFieldDefs.InsertString(ind,polDesc->ToString());
			m_ComboFieldDefs.SetItemData(ind,ilIndex);
			if (m_ComboFieldDefs.GetCurSel() == CB_ERR)
				m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::D:	// DATE
		{
			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(polDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			FidasDateTimeFieldDialog dlg(this);
			dlg.SetFieldDescription(polDesc);
			if (dlg.DoModal() != IDOK)
				break;

			m_ComboFieldDefs.DeleteString(ind);
			m_ComboFieldDefs.InsertString(ind,polDesc->ToString());
			m_ComboFieldDefs.SetItemData(ind,ilIndex);
			if (m_ComboFieldDefs.GetCurSel() == CB_ERR)
				m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::DT:	// DATE TIME
		{
			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(polDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			FidasDateTimeFieldDialog dlg(this);
			dlg.SetFieldDescription(polDesc);
			if (dlg.DoModal() != IDOK)
				break;

			m_ComboFieldDefs.DeleteString(ind);
			m_ComboFieldDefs.InsertString(ind,polDesc->ToString());
			m_ComboFieldDefs.SetItemData(ind,ilIndex);
			if (m_ComboFieldDefs.GetCurSel() == CB_ERR)
				m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::CT:	// CURRENT TIME
		{
			FidasDateTimeFieldDialog dlg(this);
			dlg.SetFieldDescription(polDesc);
			if (dlg.DoModal() != IDOK)
				break;

			m_ComboFieldDefs.DeleteString(ind);
			m_ComboFieldDefs.InsertString(ind,polDesc->ToString());
			m_ComboFieldDefs.SetItemData(ind,ilIndex);
			if (m_ComboFieldDefs.GetCurSel() == CB_ERR)
				m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::CD:	// CURRENT DATE
		{
			FidasDateTimeFieldDialog dlg(this);
			dlg.SetFieldDescription(polDesc);
			if (dlg.DoModal() != IDOK)
				break;

			m_ComboFieldDefs.DeleteString(ind);
			m_ComboFieldDefs.InsertString(ind,polDesc->ToString());
			m_ComboFieldDefs.SetItemData(ind,ilIndex);
			if (m_ComboFieldDefs.GetCurSel() == CB_ERR)
				m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::CDT:	// CURRENT DATE TIME
		{
			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(polDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			FidasDateTimeFieldDialog dlg(this);
			dlg.SetFieldDescription(polDesc);
			if (dlg.DoModal() != IDOK)
				break;

			m_ComboFieldDefs.DeleteString(ind);
			m_ComboFieldDefs.InsertString(ind,polDesc->ToString());
			m_ComboFieldDefs.SetItemData(ind,ilIndex);
			if (m_ComboFieldDefs.GetCurSel() == CB_ERR)
				m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::TT:	// CURRENT TIME if referenced field is empty
		{
			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(polDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			FidasDateTimeFieldDialog dlg(this);
			dlg.SetFieldDescription(polDesc);
			if (dlg.DoModal() != IDOK)
				break;

			m_ComboFieldDefs.DeleteString(ind);
			m_ComboFieldDefs.InsertString(ind,polDesc->ToString());
			m_ComboFieldDefs.SetItemData(ind,ilIndex);
			if (m_ComboFieldDefs.GetCurSel() == CB_ERR)
				m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::TD:	// CURRENT DATE if referenced field is empty
		{
			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(polDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			FidasDateTimeFieldDialog dlg(this);
			dlg.SetFieldDescription(polDesc);
			if (dlg.DoModal() != IDOK)
				break;

			m_ComboFieldDefs.DeleteString(ind);
			m_ComboFieldDefs.InsertString(ind,polDesc->ToString());
			m_ComboFieldDefs.SetItemData(ind,ilIndex);
			if (m_ComboFieldDefs.GetCurSel() == CB_ERR)
				m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::TDT:	// CURRENT DATE TIME if referenced field is empty
		{
			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(polDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			FidasDateTimeFieldDialog dlg(this);
			dlg.SetFieldDescription(polDesc);
			if (dlg.DoModal() != IDOK)
				break;

			m_ComboFieldDefs.DeleteString(ind);
			m_ComboFieldDefs.InsertString(ind,polDesc->ToString());
			m_ComboFieldDefs.SetItemData(ind,ilIndex);
			if (m_ComboFieldDefs.GetCurSel() == CB_ERR)
				m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::R:	// CONTENT
		{
			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(polDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			CFidasFieldContentDialog dlg(this);
			dlg.SetFieldDescription(polDesc);
			if (dlg.DoModal() != IDOK)
				break;

			m_ComboFieldDefs.DeleteString(ind);
			m_ComboFieldDefs.InsertString(ind,polDesc->ToString());
			m_ComboFieldDefs.SetItemData(ind,ilIndex);
			if (m_ComboFieldDefs.GetCurSel() == CB_ERR)
				m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::B:	// REMARK 
		{
			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(polDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			CFidasRemarkDialog dlg(this);
			dlg.SetFieldDescription(polDesc);
			if (dlg.DoModal() != IDOK)
				break;

			m_ComboFieldDefs.DeleteString(ind);
			m_ComboFieldDefs.InsertString(ind,polDesc->ToString());
			m_ComboFieldDefs.SetItemData(ind,ilIndex);
			if (m_ComboFieldDefs.GetCurSel() == CB_ERR)
				m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::A:	// AIRPORTS
		{
			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(polDesc);
			if (dlg2.DoModal() != IDOK)
				break;

			CFidasAirportDialog dlg(this);
			dlg.SetFieldDescription(polDesc);
			if (dlg.DoModal() != IDOK)
				break;

			m_ComboFieldDefs.DeleteString(ind);
			m_ComboFieldDefs.InsertString(ind,polDesc->ToString());
			m_ComboFieldDefs.SetItemData(ind,ilIndex);
			if (m_ComboFieldDefs.GetCurSel() == CB_ERR)
				m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	case CDpyType::AL:	// AIRLINES
		{
			CFidasSelectTableFieldDialog dlg2(this);
			dlg2.SetFieldDescription(polDesc);
			if (dlg2.DoModal() != IDOK)
				break;
#if	0
			CFidasAirlineDialog dlg(this);
			dlg.SetFieldDescription(polDesc);
			if (dlg.DoModal() != IDOK)
				break;
#endif
			m_ComboFieldDefs.DeleteString(ind);
			m_ComboFieldDefs.InsertString(ind,polDesc->ToString());
			m_ComboFieldDefs.SetItemData(ind,ilIndex);
			if (m_ComboFieldDefs.GetCurSel() == CB_ERR)
				m_ComboFieldDefs.SetCurSel(ind);
		}
	break;
	default:
		AfxMessageBox("Format not supported yet");
	}

	OnSelchangeComboFielddefs();

}

void CFidasFieldDefinitionProperties::OnButtonRemove() 
{
	// TODO: Add your control notification handler code here
	int ind = m_ComboFieldDefs.GetCurSel();
	if (ind < 0)
		return;
	
	int ilIndex = (m_ComboFieldDefs.GetItemData(ind));

	std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
	ASSERT(polDescs);
	ASSERT(ilIndex >= 0 && ilIndex < polDescs->size());

	polDescs->erase(polDescs->begin()+ilIndex);
	m_ComboFieldDefs.DeleteString(ind);
	m_ComboFieldDefs.SetCurSel(-1);

	pomFieldDef->SetFieldDesc(polDescs);

	if (m_ComboFieldDefs.GetCurSel() < 0)
	{
		if (m_ComboFieldDefs.GetCount())
		{
			m_ComboFieldDefs.SetCurSel(0);
			OnSelchangeComboFielddefs();
		}
		else
		{
			m_ComboFieldDefs.SetWindowText("");
			OnSelchangeComboFielddefs();
		}
	}
}

void CFidasFieldDefinitionProperties::OnSelchangeComboFielddefs() 
{
	// TODO: Add your control notification handler code here
	CString olCurrentName;
	m_EditName.GetWindowText(olCurrentName);

	if (!NameIsValid(olCurrentName))
	{
		m_ButtonOK.EnableWindow(FALSE);
	}
	else
	{
		m_ButtonOK.EnableWindow(TRUE);
	}
}

void CFidasFieldDefinitionProperties::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);

	pomFieldDef->SetName(m_Name);
	pomFieldDef->SetDescription(m_Description);
	int ind = m_ListLanguage.GetCurSel();
	if (ind >= 0)
	{
		pomFieldDef->SetLang(m_ListLanguage.GetItemData(ind));
	}

	// delete field definitions saved for undo
	delete pomFieldDescs;
	CDialog::OnOK();
}

void CFidasFieldDefinitionProperties::OnChangeEditName() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	CString olCurrentName;
	m_EditName.GetWindowText(olCurrentName);

	if (!CFidasUtilities::IsValidString(olCurrentName,30))
	{
		TRACE ("due to internal processing, this character is forbidden!\n");
		MessageBox(CFidasUtilities::GetString(IDS_ERROR_INVALID_CHARACTER),CFidasUtilities::GetString(IDS_ERROR_TITLE),MB_ICONHAND + MB_OK);
		UpdateData(FALSE);
	}
	else
	{
		UpdateData(TRUE);
	}

	if (!NameIsValid(olCurrentName))
	{
		m_ButtonOK.EnableWindow(FALSE);
	}
	else
	{
		m_ButtonOK.EnableWindow(TRUE);
	}
	
}

void CFidasFieldDefinitionProperties::OnCancel() 
{
	// TODO: Add extra cleanup here
	ASSERT(pomFieldDef);
	ASSERT(pomFieldDescs);

	std::vector<CFieldDesc> *polDescs = pomFieldDef->GetFieldDesc();
	if (polDescs)
	{
		delete polDescs;
	}

	pomFieldDef->SetFieldDesc(pomFieldDescs);

	CDialog::OnCancel();
}
