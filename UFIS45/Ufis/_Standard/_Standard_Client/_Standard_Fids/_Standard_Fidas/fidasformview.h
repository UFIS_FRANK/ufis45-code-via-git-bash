//{{AFX_INCLUDES()
#include <tab.h>
//}}AFX_INCLUDES
#if !defined(AFX_FIDASTESTVIEW_H__8DE198B2_5B00_11D5_8126_00010215BFDE__INCLUDED_)
#define AFX_FIDASTESTVIEW_H__8DE198B2_5B00_11D5_8126_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FidasTestView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFidasFormView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CFidasFormView : public CFormView
{
protected:
	CFidasFormView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CFidasFormView)

// Form Data
public:
	//{{AFX_DATA(CFidasFormView)
	enum { IDD = IDD_FORMVIEW };
	CTAB	m_TabCtrl;
	//}}AFX_DATA

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFidasFormView)
	public:
	virtual void OnFinalRelease();
	virtual void OnInitialUpdate();
	virtual CScrollBar* GetScrollBarCtrl(int nBar) const;
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CFidasFormView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CFidasFormView)
	afx_msg void OnSendLButtonClickTabctrl1(long LineNo, long ColNo);
	afx_msg void OnSendLButtonDblClickTabctrl1(long LineNo, long ColNo);
	afx_msg void OnSendRButtonClickTabctrl1(long LineNo, long ColNo);
	afx_msg void OnSendMouseMoveTabctrl1(long LineNo, long ColNo, LPCTSTR Flags);
	afx_msg void OnColumnSelectionChangedTabctrl1(long ColNo, BOOL Selected);
	afx_msg void OnOnVScrollTabctrl1(long LineNo);
	afx_msg void OnInplaceEditCellTabctrl1(long LineNo, long ColNo, LPCTSTR NewValue, LPCTSTR OldValue);
	afx_msg void OnOnHScrollTabctrl1(long ColNo);
	afx_msg void OnRowSelectionChangedTabctrl1(long LineNo, BOOL Selected);
	afx_msg void OnHitKeyOnLineTabctrl1(short Key, long LineNo);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CFidasFormView)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIDASTESTVIEW_H__8DE198B2_5B00_11D5_8126_00010215BFDE__INCLUDED_)
