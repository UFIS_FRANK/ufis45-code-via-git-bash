#if !defined(AFX_FIDASLOGODIALOG_H__DB78A157_5EED_11D5_812C_00010215BFDE__INCLUDED_)
#define AFX_FIDASLOGODIALOG_H__DB78A157_5EED_11D5_812C_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FidasLogoDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFidasLogoDialog dialog
class CFieldDesc;

class CFidasLogoDialog : public CDialog
{
// Construction
public:
	CFidasLogoDialog(CWnd* pParent = NULL);   // standard constructor
	void SetFieldDescription(CFieldDesc *prpFieldDesc);

private:
// Dialog Data
	//{{AFX_DATA(CFidasLogoDialog)
	enum { IDD = IDD_UFIS_LOGO };
		// NOTE: the ClassWizard will add data members here
	CStatic	m_FileSpec_Static;
	CEdit	m_FileSpec_Edit;
	CButton	m_OK_Button;
	CButton	m_Logo_Frame;
	CStatic	m_FileExt_Static;
	CComboBox	m_FileExt_Combo;
	CButton	m_Cancel_Button;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFidasLogoDialog)
	public:
	virtual void OnFinalRelease();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFidasLogoDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnUfisLogoOkButton();
	afx_msg void OnUfisLogoCancelButton();
	afx_msg void OnChangeUfisLogoFilespecEdit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CFidasLogoDialog)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
private:
	CFieldDesc	*pomFieldDesc;
	static	CString	omLogoText;
	static	CString omGoodText;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIDASLOGODIALOG_H__DB78A157_5EED_11D5_812C_00010215BFDE__INCLUDED_)
