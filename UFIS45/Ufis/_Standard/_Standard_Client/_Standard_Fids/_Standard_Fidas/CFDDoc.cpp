/*
 * AAB/AAT UFIS Display Handler
 *
 * Implementation of FIDAS document constituting classes
 * j. heilig June 15 2001
 * 
 * Jun 22 01 released to test (jhe)
 *
 */
  
#include <CFDDoc.h>

//===== CFieldRef

CFieldRef::CFieldRef()
: start(0), end(0)
{
}

CFieldRef::CFieldRef(const CFieldRef &c)
: csTableName(c.csTableName), csFieldName(c.csFieldName),
  start(c.start), end(c.end)
{
}

CFieldRef::CFieldRef(char *pb)
: start(0), end(0)
{
	istrstream instr(pb);
// get field name part T.N[(s,e)]/ ...

// table name
	CTokenZ tzTN(instr,".");
	const char *p = tzTN;
	if (!p || !*p || instr.peek() != '.')
		return;
	csTableName = p;

// field name
	CTokenZ tzFN(instr,"(", ".");
	p = tzFN;
	if (!p || !*p)
		return;
	csFieldName = p;

// set start and end index if present
	if (instr.peek() == '(')
	{
		int s, e;
		char c;
		instr >> c >> s >> c >> e;
		if(!instr.fail() && s > 0 && e >= s)
		{
			start = s;
			end = e;
		}
	}
}

CFieldRef::~CFieldRef()
{
}

CString CFieldRef::ToString() const
{
ostrstream ostr;
	ostr << csTableName << "." << csFieldName;
	if(start > 0 && end >= start)
		ostr << "(" << start << "," << end << ")";
	ostr << '\0';
char *p = ostr.str();
CString s(p);
	delete p;
	return s;
}

const CString &CFieldRef::GetTableName() const
{
	return csTableName;
}

void CFieldRef::SetTableName(const CString &s)
{
	csTableName = s;
}

const CString &CFieldRef::GetFieldName() const
{
	return csFieldName;
}

void CFieldRef::SetFieldName(const CString &s)
{
	csFieldName = s;
}

int CFieldRef::GetStart() const
{
	return start;
}

void CFieldRef::SetStart(int s)
{
	start = s;
}

int CFieldRef::GetEnd() const
{
	return end;
}

void CFieldRef::SetEnd(int e)
{
	end = e;
}

/*
 * Two field definitions are equal if their description, language and
 * field descriptions are equal. The names need not match!
 */

int CFieldRef::operator == (const CFieldRef &arg) const
{

	return(csTableName == arg.csTableName && csFieldName == arg.csFieldName &&
		   start == arg.start && end == arg.end );
}

int CFieldRef::operator != (const CFieldRef &arg) const
{
	return !(*this == arg);
}


CFieldRef &CFieldRef::operator=(const CFieldRef &a)
{
	if(&a == this)
		return *this;
	csTableName = a.csTableName;
	csFieldName = a.csFieldName;
	start = a.start;
	end = a.end;
	return *this;
}

//===== CFieldDesc

CFieldDesc::CFieldDesc()
: pFieldRef(NULL), pDpyType(NULL), iSize(0)
{
}

CFieldDesc::CFieldDesc(const CFieldDesc &c)
: pFieldRef(NULL), pDpyType(NULL), iSize(c.iSize)
{
	if(c.pFieldRef)
		pFieldRef = new CFieldRef(*(c.pFieldRef));
	if(c.pDpyType)
		pDpyType = c.pDpyType->Clone(c.pDpyType);
}

CFieldDesc::CFieldDesc(char *pb)
: pFieldRef(NULL), pDpyType(NULL), iSize(0)
{
char *p;
	istrstream is(pb);
// get field reference 
	CTokenZ tzFLD(is,"/");
	if(tzFLD.Length() == 0)
		return;
	p = tzFLD.DupZ();
	pFieldRef = new CFieldRef(p);
	delete p;
// get display type
	CTokenZ tzDT(is,"/","/");
	if(tzDT.Length() == 0)
		return;
	p = tzDT.DupZ();
	pDpyType = CDpyType::Create(p);
	delete p;
// get optional size spec
	if(is.peek() != '/')
		return;
	char c;
	int s;
	is >> c >> s;
	if(!is.fail())
		iSize = s;
}

CFieldDesc::~CFieldDesc()
{
	delete pFieldRef;
	delete pDpyType;
}

CString CFieldDesc::ToString() const
{
ostrstream ostr;
	if(pFieldRef == NULL)
		ostr << "?.?";
	else
		ostr << pFieldRef->ToString();
	ostr << "/";
	if(pDpyType == NULL)
		ostr << "?";
	else
		ostr << pDpyType->ToString();
	if(iSize > 0)
		ostr << "/" << iSize;
	ostr << '\0';

char *p = ostr.str();
CString s(p);
	delete p;
	return s;
}

int CFieldDesc::GetSize() const
{
	return iSize;
}

void CFieldDesc::SetSize(int s)
{
	iSize = s;
}

CFieldDesc &CFieldDesc::operator=(const CFieldDesc &a)
{
	if(&a == this)
		return *this;

	delete pFieldRef;
	if(a.pFieldRef)
		pFieldRef = new CFieldRef(*(a.pFieldRef));
	else 
		pFieldRef = NULL;

	delete pDpyType;
	if(a.pDpyType)
		pDpyType = a.pDpyType->Clone(a.pDpyType);
	else
		pDpyType = NULL;
	return *this;
}

const CFieldRef *CFieldDesc::FieldRef() const
{
	return pFieldRef;
}

CFieldRef *CFieldDesc::GetFieldRef()
{
CFieldRef *p;
	p = pFieldRef;
	pFieldRef = NULL;
	return p;
}

void CFieldDesc::SetFieldRef(CFieldRef *p)
{
	delete pFieldRef;
	pFieldRef = p;
}

const CDpyType *CFieldDesc::DpyType() const
{
	return pDpyType;
}

CDpyType *CFieldDesc::GetDpyType()
{
CDpyType *p;
	p = pDpyType;
	pDpyType = NULL;
	return p;
}

void CFieldDesc::SetDpyType(CDpyType *p)
{
	delete pDpyType;
	pDpyType = p;
}
	
/*
 * Two field descriptions are equal if their field references, display types
 * and size specifications are equal
 */

int CFieldDesc::operator == (const CFieldDesc &arg) const
{

	if(!pFieldRef && arg.pFieldRef || pFieldRef && !arg.pFieldRef)
		return 0;
	if (pFieldRef && arg.pFieldRef && *pFieldRef != *arg.pFieldRef)
		return 0;
	if(!pDpyType && arg.pDpyType || pDpyType && !arg.pDpyType)
		return 0;
	if(pDpyType && arg.pDpyType && *pDpyType != *arg.pDpyType)
		return 0;
	if(iSize != arg.iSize)
		return 0;
	return 1;
}

int CFieldDesc::operator != (const CFieldDesc &arg) const
{
	return !(*this == arg);
}

//===== CFieldDef

// (static converters between DB and appl representation of language)

int CFieldDef::Language(const CString &l)
{
	return l == "arabic" ? 1 : 0 ;
}

CString CFieldDef::Language(int l)
{
	return CString(l == 1 ? "arabic" : "latin");
}

CFieldDef::CFieldDef()
: imLanguage(0), omDescList(NULL)
{
}

CFieldDef::CFieldDef(const CFieldDef &c)
: imLanguage(c.imLanguage), omName(c.omName), omDesc(c.omDesc), omDescList(NULL)
{
	if(c.omDescList)
	{
/*****
		std::vector<CFieldDesc>::iterator it;
		for(it=omDescList->begin(); it!=omDescList->end(); ++it)
*****/
		omDescList = new std::vector<CFieldDesc>( * c.omDescList);
		std::vector<CFieldDesc>::iterator it1, it2;
		it1 = c.omDescList->begin();
		it2 = omDescList->begin();
	}	
}

CFieldDef::CFieldDef(const CString &n,const CString &d,const CString &f, int l)
: imLanguage(l), omName(n), omDesc(d), omDescList(NULL)
{
	DescFromString(f);
}

CFieldDef::~CFieldDef()
{
	delete omDescList;
}

void CFieldDef::DescFromString(const CString &fstr)
{
	const char *pb = fstr;
	int l = strlen(fstr);
	char *buff = new char [l+1];
	Assert(buff);
	strcpy(buff, pb);

	istrstream instr(buff);

	for(;;)
	{
		CTokenZ tzFD(instr,"{");
		if(tzFD.Length() == 0)
			break;
		char *p = tzFD.DupZ();
		CFieldDesc *pdesc = new CFieldDesc(p);
		delete p;
		if(omDescList == NULL)
		{
			omDescList = new std::vector<CFieldDesc>;
			Assert(omDescList);
		}
		omDescList->push_back(*pdesc);
		delete pdesc;
		if(instr.peek() != '{')
			break;
		CTokenZ tzIT(instr,"}","{");
		if(tzIT != "ITEM")
			break;
		if(instr.peek() != '}')
			break;
		instr.ignore(1);
	}
	delete buff;
}

CString CFieldDef::DescToString() const
{
CString s;
int ix;
std::vector<CFieldDesc>::iterator it;
	if (omDescList == NULL)
	{
		s = "";
	}
	else
	{
		for(ix=0, it=omDescList->begin(); it!=omDescList->end(); ++ix, ++it)
		{
			if(ix > 0)
				s += "{ITEM}";
			s += it->ToString();
		}
	}

	return s;
}

int CFieldDef::GetLang() const
{
	return imLanguage;
}

void CFieldDef::SetLang(int l)
{
	imLanguage = l;
}

const CString &CFieldDef::GetName() const
{
	return omName;
}

void CFieldDef::SetName(const CString &s)
{
	omName = s;
}

const CString &CFieldDef::GetDescription() const
{
	return omDesc;
}

void CFieldDef::SetDescription(const CString &s)
{
	omDesc = s;
}

const std::vector<CFieldDesc> *CFieldDef::FieldDesc() const // readonly access
{
	return omDescList;
}

std::vector<CFieldDesc> *CFieldDef::GetFieldDesc()		// passes ownership
{
std::vector<CFieldDesc> *p;
	p = omDescList;
	omDescList = NULL;
	return p;
}

void CFieldDef::SetFieldDesc(std::vector<CFieldDesc> *p) // takes ownership
{
	delete omDescList;
	omDescList = p;
}

/*
 * Two field definitions are equal if their description, language and
 * field descriptions are equal. The names need not match!
 */

int CFieldDef::operator == (const CFieldDef &arg) const
{
std::vector<CFieldDesc>::const_iterator itt, ita;
	if(imLanguage != arg.imLanguage || omDesc != arg.omDesc)
		return 0;
	if(!omDescList && arg.omDescList || omDescList && !arg.omDescList)
		return 0;
	if(!omDescList && !arg.omDescList)
		return 1;
	for(itt=omDescList->begin(); itt!=omDescList->end(); ++itt)
		for(ita=arg.omDescList->begin(); ita!=arg.omDescList->end(); ++ita)
			if(*itt != *ita)
				return 0;
	return 1; 
}

int CFieldDef::operator != (const CFieldDef &arg) const
{
	return !(*this == arg);
}

CFieldDef &CFieldDef::operator=(const CFieldDef &a)
{
	if(&a == this)
		return *this;
	imLanguage = a.imLanguage;
	omName = a.omName;
	omDesc = a.omDesc;
	
	if(a.omDescList)
	{
		if(omDescList == NULL)
			omDescList = new std::vector<CFieldDesc>;
		*omDescList = *(a.omDescList); // STL has op=
	}
	else if (omDescList)
	{
		delete omDescList;
		omDescList = NULL;
	}

	return *this;
}
//===== CFieldDefDoc

CUfisDB *CFieldDefDoc::pDB = NULL;
CStringVector CFieldDefDoc::GroupList;
CPEDTable CFieldDefDoc::PEDTable;

int CFieldDefDoc::SetDatabase(CUfisDB &db)
{
	Assert(pDB==NULL);
	pDB = &db;


	if(pDB->GetPEDTable(PEDTable))
		return -1;
	build_doclist();
	return 0;
}

void CFieldDefDoc::build_doclist()
{
CPEDTable_it pr;
CStringVector_it pg;

	GroupList.empty();
	for(pr=PEDTable.begin(); pr!=PEDTable.end(); ++pr)
	{
		for(pg=GroupList.begin(); pg!=GroupList.end(); ++pg)
		{
			if(*pg == pr->csGroup)
				break;
		}
		if (pg == GroupList.end())
			GroupList.push_back(pr->csGroup);
	}
	return;
}

int CFieldDefDoc::GetDocList(CStringVector &vec)
{
//std::vector<CString>::iterator pg;
CStringVector_it pg;
	vec.empty();
	for(pg=GroupList.begin(); pg!=GroupList.end(); ++pg)
		vec.push_back(*pg);
	return 0;
}

CFieldDefDoc::CFieldDefDoc(const char *szDocName)
: omName(szDocName), omFieldList(NULL)
{
CPEDTable_it ti;

	omFieldList = new std::vector<CFieldDef>;
	for(ti=PEDTable.begin(); ti!=PEDTable.end(); ++ti)
	{
		if(ti->csGroup == szDocName)
		{
			CFieldDef tmp(ti->csName, ti->csDesc, ti->csDef,
                    CFieldDef::Language(ti->csLang));
			omFieldList->push_back(tmp);
		}
	}
}

CFieldDefDoc::~CFieldDefDoc()
{
	delete omFieldList;
}

int CFieldDefDoc::SaveDoc(const char *szDocName)
{
	if(omFieldList == NULL)	// NULL list not allowed
		return -1;
	if(szDocName == NULL)	// use doc name if no name spec'd
		szDocName = omName;

CPEDTable_it tx;
std::vector<CFieldDef>::iterator fx;

/*
 * find out which records to update (the same names, different contents)
 */
	for(tx=PEDTable.begin(); tx!=PEDTable.end(); ++tx)
	{
		for(fx=omFieldList->begin(); fx!=omFieldList->end(); ++fx)
			if(tx->csName == fx->GetName() && tx->csGroup == szDocName)
				break;
		if(fx != omFieldList->end())	// record present on both lists
		{
			if(fx->DescToString() != tx->csDef ||			// different definition
			   fx->GetDescription() != tx->csDesc ||		// or different description
			   fx->Language(fx->GetLang()) != tx->csLang)	// or different language
			{
				CPEDRec rec(*tx);
				rec.csDef = fx->DescToString();
				rec.csDesc = fx->GetDescription();
				rec.csLang = fx->Language(fx->GetLang());
				aatDebug("SaveDoc: Update N=" << rec.csName << " G=" << rec.csGroup);
				if(pDB->UpdatePEDRec(rec))	// error on update rec
					return -1;
				*tx = rec;	// replace in table if Ok
			}
		}
	}
/*
 * find out which records to insert (those not present in the table)
 */
	for(fx=omFieldList->begin(); fx!=omFieldList->end(); ++fx)
	{
		for(tx=PEDTable.begin(); tx!=PEDTable.end(); ++tx)
			if(tx->csName == fx->GetName() && tx->csGroup == szDocName)
				break;
		if(tx == PEDTable.end())	// doc record not found in table
		{
			CPEDRec rec(fx->GetName(), fx->GetDescription(), fx->DescToString(),
                        szDocName, fx->Language(fx->GetLang()));
			aatDebug("SaveDoc: Insert N=" << rec.csName << " G=" << rec.csGroup);
			if(pDB->InsertPEDRec(rec))	// error on insert rec
				return -2;
			PEDTable.push_back(rec);	// insert into table if Ok
		}
	}
/*
 * find out which records to remove from the table (those having the group name
 * as the document but not present in the document)
 */
	int restart;
	do
	{
		restart = 0;
		for(tx=PEDTable.begin(); tx!=PEDTable.end(); ++tx)
		{
			if(tx->csGroup != szDocName)
				continue;
			for(fx=omFieldList->begin(); fx!=omFieldList->end(); ++fx)
				if(tx->csName == fx->GetName())
					break;
			if(fx == omFieldList->end()) // table record not found in doc 
			{
				aatDebug("SaveDoc: Delete N=" << tx->csName << " G=" << tx->csGroup);
				if(pDB->DeletePEDRec(*tx))	// error on remove rec
					return -3;
				PEDTable.erase(tx);
				restart = 1; // what is the safe way to cont with iterator?
				break;
			}
		}
	} while(restart);
/*
 * Update document list
 */
	omName = szDocName;
	build_doclist();
	pDB->PEDTableProc();
	return 0;
}

const CString &CFieldDefDoc::GetDocName() const
{
	return omName;
}

const std::vector<CFieldDef> *CFieldDefDoc::FieldDef() const
{
	return omFieldList;
}

std::vector<CFieldDef> *CFieldDefDoc::GetFieldDef()
{
std::vector<CFieldDef> *p;
	p = omFieldList;
	omFieldList = NULL;
	return p;
}

void CFieldDefDoc::SetFieldDef(std::vector<CFieldDef> *p)
{
	delete omFieldList;
	omFieldList = p;
}


 
