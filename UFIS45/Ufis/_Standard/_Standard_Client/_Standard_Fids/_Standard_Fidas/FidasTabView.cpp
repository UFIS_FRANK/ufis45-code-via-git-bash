// FidasTestView.cpp : implementation file
//

#include <stdafx.h>
#include <fidas.h>
#include <FidasTabView.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFidasTabView

IMPLEMENT_DYNCREATE(CFidasTabView, CCtrlView)

CFidasTabView::CFidasTabView()
: CCtrlView("AfxOleControl42",AFX_WS_DEFAULT_VIEW)
{
	//{{AFX_DATA_INIT(CFidasTabView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CTAB& CFidasTabView::GetTabCtrl() const
{
	return (CTAB&)m_TabCtrl;	
}

BEGIN_MESSAGE_MAP(CFidasTabView, CCtrlView)
	//{{AFX_MSG_MAP(CFidasTabView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFidasTabView message handlers

BOOL CFidasTabView::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class

	BOOL blResult = CCtrlView::PreCreateWindow(cs);

	return blResult;
}

int CFidasTabView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CCtrlView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	CRect olRect;
	GetClientRect(&olRect);
	BOOL blCreated = m_TabCtrl.Create(NULL,WS_VISIBLE|WS_CHILD,olRect,this,ID_TABCONTROL);
	ASSERT(blCreated);
	
	CString olFieldList = "Name,Description";
	m_TabCtrl.ResetContent();
	m_TabCtrl.SetFontName("Arial");
	m_TabCtrl.SetFontSize(12);
	m_TabCtrl.SetHeaderLengthString("70,100");
	m_TabCtrl.SetHeaderString(olFieldList);
	m_TabCtrl.RedrawTab();
	
	return 0;
}

void CFidasTabView::DrawItem(LPDRAWITEMSTRUCT)
{
	ASSERT(FALSE);
}

BOOL CFidasTabView::OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam,
	LRESULT* pResult)
{
	if (message != WM_DRAWITEM)
		return CCtrlView::OnChildNotify(message, wParam, lParam, pResult);

	ASSERT(pResult == NULL);       // no return value expected
	UNUSED(pResult); // unused in release builds

	DrawItem((LPDRAWITEMSTRUCT)lParam);
	return TRUE;
}
