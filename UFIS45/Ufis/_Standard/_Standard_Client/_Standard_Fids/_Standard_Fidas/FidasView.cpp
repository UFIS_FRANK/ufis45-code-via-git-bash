// FidasView.cpp : implementation of the CFidasView class
//

#include <stdafx.h>
#include <Fidas.h>

#include <FidasFieldDefinitionProperties.h>
#include <FidasDoc.h>
#include <FidasView.h>
#include <FidasUtilities.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const UINT PRINTMARGIN	= 2;

/////////////////////////////////////////////////////////////////////////////
// CFidasView

IMPLEMENT_DYNCREATE(CFidasView, CView)

BEGIN_MESSAGE_MAP(CFidasView, CView)
	//{{AFX_MSG_MAP(CFidasView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND(ID_PROPERTIES, OnProperties)
	ON_COMMAND(ID_INSERT_FIELDDEF, OnInsertFielddef)
	ON_COMMAND(ID_COPY_FIELDDEF,   OnCopyFielddef)
	ON_COMMAND(ID_REMOVE_FIELDDEF, OnRemoveFielddef)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateEditCut)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdateEditPaste)
	ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, OnUpdateEditUndo)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT_PREVIEW, OnUpdateFilePrintPreview)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

BEGIN_EVENTSINK_MAP(CFidasView, CView)
	ON_EVENT(CFidasView,ID_TABCONTROL,1,OnSendLButtonClick,VTS_I4 VTS_I4)
	ON_EVENT(CFidasView,ID_TABCONTROL,2,OnSendLButtonDblClick,VTS_I4 VTS_I4)
	ON_EVENT(CFidasView,ID_TABCONTROL,3,OnSendRButtonClick,VTS_I4 VTS_I4)
	ON_EVENT(CFidasView,ID_TABCONTROL,4,OnSendMouseMove,VTS_I4 VTS_I4 VTS_BSTR)
	ON_EVENT(CFidasView,ID_TABCONTROL,5,OnColumnSelectionChanged,VTS_I4 VTS_BOOL)
	ON_EVENT(CFidasView,ID_TABCONTROL,6,OnVScroll,VTS_I4)
	ON_EVENT(CFidasView,ID_TABCONTROL,7,OnInplaceEditCell,VTS_I4 VTS_I4 VTS_BSTR VTS_BSTR)
	ON_EVENT(CFidasView,ID_TABCONTROL,8,OnHScroll,VTS_I4)
	ON_EVENT(CFidasView,ID_TABCONTROL,9,OnRowSelectionChanged,VTS_I4 VTS_BOOL)
	ON_EVENT(CFidasView,ID_TABCONTROL,10,OnHitKeyOnLine,VTS_I2 VTS_I4)
END_EVENTSINK_MAP()



/////////////////////////////////////////////////////////////////////////////
// CFidasView construction/destruction

CFidasView::CFidasView()
: omPrintCtrl(this)
{
	// TODO: add construction code here

}

CFidasView::~CFidasView()
{
}

BOOL CFidasView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CFidasView drawing

void CFidasView::OnDraw(CDC* pDC)
{
	CFidasDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
//	omTabCtrl.RedrawTab();
}

/////////////////////////////////////////////////////////////////////////////
// CFidasView printing

BOOL CFidasView::OnPreparePrinting(CPrintInfo* pInfo)
{
	imLinesTotal = omTabCtrl.GetLineCount();
	if (!omPrintCtrl.OnPreparePrinting(pInfo,CFidasPrint::PRINT_LANDSCAPE,60,500,100))
		return FALSE;
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CFidasView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// TODO: add extra initialization before printing
	omPrintCtrl.OnBeginPrinting(pDC,pInfo);	
}

void CFidasView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// TODO: add cleanup after printing
	omPrintCtrl.OnEndPrinting(pDC,pInfo);	
}

void CFidasView::OnPrint(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	PrintPageHeader(GetDocument(),pDC,pInfo->m_nCurPage);
	PrintPage(GetDocument(),pDC,pInfo->m_nCurPage);
	PrintPageFooter(GetDocument(),pDC,pInfo->m_nCurPage);
	
}

void CFidasView::PrintLine(CFidasDoc *pDoc,UINT nLine)
{
	ASSERT(pDoc);
	const std::vector<CFieldDef> *polDefs = pDoc->omFieldDefDoc.FieldDef();
	if (polDefs && nLine < polDefs->size())
	{
		const CFieldDef& def = polDefs->at(nLine);

		std::vector<CFidasPrint::PRINTELEDATA> olLine;
		CFidasPrint::PRINTELEDATA	olData;

		if (nLine == 0)
		{
			olData.Alignment	= CFidasPrint::PRINT_LEFT;
			olData.Length		= 300;
			olData.FrameLeft	= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.FrameRight	= CFidasPrint::PRINT_NOFRAME;
			olData.FrameTop		= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.FrameBottom	= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.pFont		= &omPrintCtrl.omSmallFont_Bold;
			olData.Text			= def.GetName();

			olLine.push_back(olData);

			olData.Alignment	= CFidasPrint::PRINT_LEFT;
			olData.Length		= 800;
			olData.FrameLeft	= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.FrameRight	= CFidasPrint::PRINT_NOFRAME;
			olData.FrameTop		= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.FrameBottom	= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.pFont		= &omPrintCtrl.omSmallFont_Bold;
			olData.Text			= def.GetDescription();

			olLine.push_back(olData);

			olData.Alignment	= CFidasPrint::PRINT_LEFT;
			olData.Length		= 100;
			olData.FrameLeft	= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.FrameRight	= CFidasPrint::PRINT_NOFRAME;
			olData.FrameTop		= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.FrameBottom	= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.pFont		= &omPrintCtrl.omSmallFont_Bold;
			olData.Text			= def.Language(def.GetLang());

			olLine.push_back(olData);

			olData.Alignment	= CFidasPrint::PRINT_LEFT;
			olData.Length		= 1500;
			olData.FrameLeft	= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.FrameRight	= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.FrameTop		= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.FrameBottom	= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.pFont		= &omPrintCtrl.omSmallFont_Bold;
			olData.Text			= def.DescToString();

			olLine.push_back(olData);

		}
		else
		{
			olData.Alignment	= CFidasPrint::PRINT_LEFT;
			olData.Length		= 300;
			olData.FrameLeft	= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.FrameRight	= CFidasPrint::PRINT_NOFRAME;
			olData.FrameTop		= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.FrameBottom	= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.pFont		= &omPrintCtrl.omSmallFont_Bold;
			olData.Text			= def.GetName();

			olLine.push_back(olData);

			olData.Alignment	= CFidasPrint::PRINT_LEFT;
			olData.Length		= 800;
			olData.FrameLeft	= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.FrameRight	= CFidasPrint::PRINT_NOFRAME;
			olData.FrameTop		= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.FrameBottom	= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.pFont		= &omPrintCtrl.omSmallFont_Bold;
			olData.Text			= def.GetDescription();

			olLine.push_back(olData);

			olData.Alignment	= CFidasPrint::PRINT_LEFT;
			olData.Length		= 100;
			olData.FrameLeft	= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.FrameRight	= CFidasPrint::PRINT_NOFRAME;
			olData.FrameTop		= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.FrameBottom	= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.pFont		= &omPrintCtrl.omSmallFont_Bold;
			olData.Text			= def.Language(def.GetLang());

			olLine.push_back(olData);

			olData.Alignment	= CFidasPrint::PRINT_LEFT;
			olData.Length		= 1500;
			olData.FrameLeft	= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.FrameRight	= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.FrameTop		= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.FrameBottom	= CFidasPrint::PRINT_FRAMEMEDIUM;
			olData.pFont		= &omPrintCtrl.omSmallFont_Bold;
			olData.Text			= def.DescToString();

			olLine.push_back(olData);
		}

		
		omPrintCtrl.PrintLine(olLine);
	}
}

void CFidasView::PrintPageHeader(CFidasDoc *pDoc,CDC *pDC,UINT nPageNumber)
{
	CString olHeader1(CFidasUtilities::GetString(IDS_PRINT_HEADER_1));

	CTime olCurrent(CTime::GetCurrentTime());

	CString olHeader2 = olCurrent.Format(CFidasUtilities::GetString(IDS_PRINT_HEADER_2));

	CString olHeader3;
	olHeader3.Format(CFidasUtilities::GetString(IDS_PRINT_HEADER_3),pDoc->omFieldDefDoc.GetDocName());

	omPrintCtrl.PrintHeader(olHeader1,olHeader2,olHeader3);
}

void CFidasView::PrintPage(CFidasDoc *pDoc,CDC *pDC,UINT nPageNumber)
{
	if (imLinesTotal != 0)
	{
		UINT ilStart = (nPageNumber - 1) * omPrintCtrl.imMaxLinesPerPage;
		UINT ilEnd	 = min(imLinesTotal -1,ilStart +  omPrintCtrl.imMaxLinesPerPage - 1);
		for (UINT i = ilStart;i <= ilEnd; i++)
		{
			PrintLine(pDoc,i);
		}
	}
}

void CFidasView::PrintPageFooter(CFidasDoc *pDoc,CDC *pDC,UINT nPageNumber)
{
	CString olFooter1(CFidasUtilities::GetString(IDS_PRINT_FOOTER_1));
	CString olFooter2(CFidasUtilities::GetString(IDS_PRINT_FOOTER_2));

	omPrintCtrl.PrintFooter(olFooter1,olFooter2);
}


/////////////////////////////////////////////////////////////////////////////
// CFidasView diagnostics

#ifdef _DEBUG
void CFidasView::AssertValid() const
{
	CView::AssertValid();
}

void CFidasView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CFidasDoc* CFidasView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CFidasDoc)));
	return (CFidasDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CFidasView message handlers

int CFidasView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here

	// create UFIS TAB control window
	CRect olRect;
	GetWindowRect(&olRect);
//	olRect.DeflateRect(CSize(50,50));

	BOOL blCreated = omTabCtrl.Create(NULL,WS_VISIBLE|WS_CHILD,olRect,this,ID_TABCONTROL);
	ASSERT(blCreated);
	
    omTabCtrl.ResetContent();
	CString olFieldList = "Name\rDescription";
	omTabCtrl.SetFieldSeparator("\r");
    omTabCtrl.SetFontName("Arial");
    omTabCtrl.SetFontSize(12);
	CString olHeaderLength;
	olHeaderLength.Format("%d,%d",(int) ((double)olRect.Width() / 3),(int) ((double)olRect.Width() * 2.0e0 / 3.0e0));
    omTabCtrl.SetHeaderString(olFieldList);
	omTabCtrl.SetHeaderLengthString(olHeaderLength);

	omTabCtrl.RedrawTab();

	return 0;
}

void CFidasView::OnSize(UINT nType, int cx, int cy) 
{
	CView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	omTabCtrl.MoveWindow(0,0,cx,cy);

	CString olHeaderLength;
	olHeaderLength.Format("%d,%d",(int) ((double)cx / 3),(int) ((double)cx * 2.0e0 / 3.0e0));
    omTabCtrl.SetHeaderLengthString(olHeaderLength);
	omTabCtrl.RedrawTab();
}


void CFidasView::OnSendLButtonClick(long LineNo, long ColNo)
{
}

void CFidasView::OnSendLButtonDblClick(long LineNo, long ColNo)
{
	CFidasDoc *pDoc = GetDocument();
	ASSERT(pDoc);
	
	std::vector<CFieldDef> *polDefs = pDoc->omFieldDefDoc.GetFieldDef();
	ASSERT(polDefs);
	CFidasFieldDefinitionProperties dlg(this);
	dlg.SetFieldDefinition(pDoc->omFieldDefDoc.GetDocName(),&polDefs->at(LineNo));
	dlg.SetNameList(*polDefs);
	dlg.SetMode(TRUE,polDefs->at(LineNo).GetName());
	pDoc->omFieldDefDoc.SetFieldDef(polDefs);
	if (dlg.DoModal() == IDOK)
	{
		pDoc->SetModifiedFlag(TRUE);
		pDoc->UpdateAllViews(NULL);			
	}

}

void CFidasView::OnSendRButtonClick(long LineNo, long ColNo)
{
	CPoint olPoint;
	GetCursorPos(&olPoint);

	CMenu olMenu;
	if (!olMenu.CreatePopupMenu())
		return;

	// header
	if (LineNo < 0)
	{
		// insert
		olMenu.AppendMenu(MF_STRING,ID_INSERT_FIELDDEF,"&Insert");
		return;
	}
	else
	{
		// insert
		olMenu.AppendMenu(MF_STRING,ID_INSERT_FIELDDEF,"&Insert");
		// edit
//!!		olMenu.AppendMenu(MF_STRING,ID_COPY_FIELDDEF,"&Copy");
		// remove
		olMenu.AppendMenu(MF_STRING,ID_REMOVE_FIELDDEF,"&Remove");
		// properties
		olMenu.AppendMenu(MF_STRING,ID_PROPERTIES,"&Properties");
	}

	olMenu.TrackPopupMenu(TPM_LEFTBUTTON|TPM_RIGHTBUTTON,olPoint.x,olPoint.y,this);
}

void CFidasView::OnSendMouseMove(long LineNo, long ColNo, const char *Flags)
{

}

void CFidasView::OnColumnSelectionChanged(long ColNo, BOOL Selected)
{
}

void CFidasView::OnVScroll(long LineNo)
{
}

void CFidasView::OnInplaceEditCell(long LineNo, long ColNo,const char *NewValue,const char *OldValue)
{
}

void CFidasView::OnOnHScroll(long ColNo)
{
}

void CFidasView::OnRowSelectionChanged(long LineNo,BOOL Selected)
{

}

void CFidasView::OnHitKeyOnLine(short Key, long LineNo)
{
}

void CFidasView::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView) 
{
	// TODO: Add your specialized code here and/or call the base class
	CView::OnActivateView(bActivate, pActivateView, pDeactiveView);
}


void CFidasView::OnProperties() 
{
	// TODO: Add your command handler code here
	CFidasDoc *pDoc = GetDocument();
	ASSERT(pDoc);
	
	int ind = omTabCtrl.GetCurrentSelected();
	if (ind < 0)
		return;

	std::vector<CFieldDef> *polDefs = pDoc->omFieldDefDoc.GetFieldDef();
	ASSERT(polDefs);
	CFidasFieldDefinitionProperties dlg(this);
	dlg.SetFieldDefinition(pDoc->omFieldDefDoc.GetDocName(),&polDefs->at(ind));
	dlg.SetNameList(*polDefs);
	dlg.SetMode(TRUE,polDefs->at(ind).GetName());
	pDoc->omFieldDefDoc.SetFieldDef(polDefs);
	if (dlg.DoModal() == IDOK)
	{
		pDoc->SetModifiedFlag(TRUE);
		pDoc->UpdateAllViews(NULL);			
	}
}

void CFidasView::OnInsertFielddef() 
{
	CFidasDoc *pDoc = GetDocument();
	ASSERT(pDoc);

	CFieldDef def;
	CFidasFieldDefinitionProperties dlg(this);
	dlg.SetFieldDefinition(pDoc->omFieldDefDoc.GetDocName(),&def);
	const std::vector<CFieldDef> *polDefs = pDoc->omFieldDefDoc.FieldDef();
	if (polDefs)
	{
		dlg.SetNameList(*polDefs);
	}

	dlg.SetMode(FALSE,"");

	if (dlg.DoModal() == IDOK)
	{
		std::vector<CFieldDef> *polDefs = pDoc->omFieldDefDoc.GetFieldDef();
		if (!polDefs)
			polDefs = new std::vector<CFieldDef>;
		ASSERT(polDefs);
		polDefs->push_back(def);
		pDoc->omFieldDefDoc.SetFieldDef(polDefs);
		pDoc->SetModifiedFlag(TRUE);
		pDoc->UpdateAllViews(NULL);			
	}
}

void CFidasView::OnCopyFielddef() 
{
	CFidasDoc *pDoc = GetDocument();
	ASSERT(pDoc);
	
	int ind = omTabCtrl.GetCurrentSelected();
	if (ind < 0)
		return;
#if	0
	std::vector<CFieldDef> *polDefs = pDoc->omFieldDefDoc.GetFieldDef();
	ASSERT(polDefs);
	CFidasFieldDefinitionProperties dlg(this);
	dlg.SetFieldDefinition(pDoc->omFieldDefDoc.GetDocName(),&polDefs->at(ind));
	dlg.SetNameList(*polDefs);
	dlg.SetMode(TRUE,polDefs->at(ind).GetName());
	pDoc->omFieldDefDoc.SetFieldDef(polDefs);
	if (dlg.DoModal() == IDOK)
	{
		pDoc->SetModifiedFlag(TRUE);
		pDoc->UpdateAllViews(NULL);			
	}
#endif
}

void CFidasView::OnRemoveFielddef() 
{
	CFidasDoc *pDoc = GetDocument();
	ASSERT(pDoc);

	int ind = omTabCtrl.GetCurrentSelected();
	if (ind < 0)
		return;

	std::vector<CFieldDef> *polDefs = pDoc->omFieldDefDoc.GetFieldDef();
	ASSERT(polDefs);
	polDefs->erase(polDefs->begin() + ind);
	pDoc->omFieldDefDoc.SetFieldDef(polDefs);
	pDoc->SetModifiedFlag(TRUE);
	pDoc->UpdateAllViews(NULL);			
}


BOOL CFidasView::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo) 
{
	// accept Properties event only when anything is selected	
	if (nID == ID_PROPERTIES && nCode == 0)
	{
		if (!omTabCtrl.GetLines() || omTabCtrl.GetCurrentSelected() < 0)
		{
			CDocument *pDoc = GetDocument();
			if (pDoc)
			{
				pDoc->OnCmdMsg(nID,nCode,pExtra,pHandlerInfo);
			}
			return TRUE;
		}
	}

	return CView::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

void CFidasView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: Add your specialized code here and/or call the base class
	if (pSender != this)
	{
		omTabCtrl.ResetContent();
		CFidasDoc *pDoc = GetDocument();
		ASSERT (pDoc);
		const std::vector<CFieldDef> *polDefs = pDoc->omFieldDefDoc.FieldDef();
		if (polDefs)
		{
			for (int i = 0; i < polDefs->size(); i++)
			{
				const CFieldDef& def = polDefs->at(i);
				CString olString;
				olString.Format("%s\r%s",def.GetName(),def.GetDescription());
				omTabCtrl.InsertTextLine(olString,FALSE);						
			}
		}
		omTabCtrl.RedrawTab();
	}
}

void CFidasView::OnEditCopy() 
{
	// TODO: Add your command handler code here
	
}

void CFidasView::OnUpdateEditCopy(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
	
}

void CFidasView::OnEditCut() 
{
	// TODO: Add your command handler code here
	
}

void CFidasView::OnUpdateEditCut(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
	
}

void CFidasView::OnEditPaste() 
{
	// TODO: Add your command handler code here
	
}

void CFidasView::OnUpdateEditPaste(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
	
}

void CFidasView::OnEditUndo() 
{
	// TODO: Add your command handler code here
	
}

void CFidasView::OnUpdateEditUndo(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
}


void CFidasView::OnUpdateFilePrintPreview(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);	
}
