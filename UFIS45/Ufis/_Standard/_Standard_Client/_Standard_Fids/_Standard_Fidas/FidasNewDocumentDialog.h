#if !defined(AFX_FIDASNEWDOCUMENTDIALOG_H__C3385C7E_5A35_11D5_8124_00010215BFDE__INCLUDED_)
#define AFX_FIDASNEWDOCUMENTDIALOG_H__C3385C7E_5A35_11D5_8124_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FidasNewDocumentDialog.h : header file
//

#include <vector>
typedef std::vector<CString> CStringVector;

/////////////////////////////////////////////////////////////////////////////
// CFidasNewDocumentDialog dialog

class CFidasNewDocumentDialog : public CDialog
{
// Construction
public:
	CFidasNewDocumentDialog(CWnd* pParent = NULL);   // standard constructor
	void	SetDocList(const CStringVector& ropDocList);
	void	SetMode(BOOL bpSaveAs,const CString& ropName);
// Dialog Data
	//{{AFX_DATA(CFidasNewDocumentDialog)
	enum { IDD = IDD_SAVE_DOCUMENT };
	CEdit	m_EditGroupName;
	CButton	m_ButtonOK;
	CString	m_GroupName;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFidasNewDocumentDialog)
	public:
	virtual void OnFinalRelease();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFidasNewDocumentDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeEditGroupName();
	afx_msg void OnUpdateEditGroupName();
	afx_msg void OnKillfocusEditGroupName();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CFidasNewDocumentDialog)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
private: // helpers	
	BOOL	NameIsValid(const CString& ropName) const;
private:
	CMapStringToPtr	omDocMap;
	BOOL			bmSaveAs;
	CString			omSaveName;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIDASNEWDOCUMENTDIALOG_H__C3385C7E_5A35_11D5_8124_00010215BFDE__INCLUDED_)
