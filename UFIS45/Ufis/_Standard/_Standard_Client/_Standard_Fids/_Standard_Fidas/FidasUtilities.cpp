// FidasUtilities.cpp : Defines the class behaviors for the application.
//

#include <stdafx.h>
#include <FidasUtilities.h>
#include <MainFrm.h>
#include <CDpyType.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/*
typedef struct	TableFields
{
	const char*		pcmFieldName;
	int				imFieldLength;
	UINT			imFieldDescription;
	int				imTypes;
}	TableFields;
		

static TableFields	omFDDTab[] =	{
	"AIRB",		14,	IDS_FDDTAB_AIRB,	CDpyType::T|CDpyType::TT,
	"ETOF",		14,	IDS_FDDTAB_ETOF,	CDpyType::T|CDpyType::TT,
	"LAND",		14,	IDS_FDDTAB_LAND,	CDpyType::T|CDpyType::TT,
	"TIFF",		14,	IDS_FDDTAB_TIFF,	CDpyType::T|CDpyType::TT,
	"OFBL",		14,	IDS_FDDTAB_OFBL,	CDpyType::T|CDpyType::TT,
	"ONBL",		14,	IDS_FDDTAB_ONBL,	CDpyType::T|CDpyType::TT,
	"FLNO",		 9,	IDS_FDDTAB_FLNO,	CDpyType::N|CDpyType::L,
	"BLT1",		 5,	IDS_FDDTAB_BLT1,	CDpyType::N|CDpyType::L,
	"BLT2",		 5,	IDS_FDDTAB_BLT2,	CDpyType::N|CDpyType::L,
	"REMP",		 4,	IDS_FDDTAB_REMP,	CDpyType::N|CDpyType::L,
	"APC3",		 3,	IDS_FDDTAB_APC3,	CDpyType::N|CDpyType::L,
	"WRO1",		 5,	IDS_FDDTAB_WRO1,	CDpyType::N|CDpyType::L,
	"VIA3",		 3,	IDS_FDDTAB_VIA3,	CDpyType::N|CDpyType::L,
	"GTD1",		 5,	IDS_FDDTAB_GTD1,	CDpyType::N|CDpyType::L,
	"GTD2",		 5,	IDS_FDDTAB_GTD2,	CDpyType::N|CDpyType::L,
	"CKIF",		 5,	IDS_FDDTAB_CKIF,	CDpyType::N|CDpyType::L,
	"CKIT",		 5,	IDS_FDDTAB_CKIT,	CDpyType::N|CDpyType::L,
	"FTYP",		 1,	IDS_FDDTAB_FTYP,	CDpyType::N|CDpyType::L,
	"RMTI",		14,	IDS_FDDTAB_RMTI,	CDpyType::T|CDpyType::TT,
	NULL,		0,	0,					0

									};

static TableFields	omFLVTab[] =	{
	"AIRB",		14,	IDS_FDDTAB_AIRB,	CDpyType::T|CDpyType::TT,
	"ETOF",		14,	IDS_FDDTAB_ETOF,	CDpyType::T|CDpyType::TT,
	"LAND",		14,	IDS_FDDTAB_LAND,	CDpyType::T|CDpyType::TT,
	"TIFF",		14,	IDS_FDDTAB_TIFF,	CDpyType::T|CDpyType::TT,
	"OFBL",		14,	IDS_FDDTAB_OFBL,	CDpyType::T|CDpyType::TT,
	"ONBL",		14,	IDS_FDDTAB_ONBL,	CDpyType::T|CDpyType::TT,
	"FLNO",		 9,	IDS_FDDTAB_FLNO,	CDpyType::N|CDpyType::L,
	"BLT1",		 5,	IDS_FDDTAB_BLT1,	CDpyType::N|CDpyType::L,
	"BLT2",		 5,	IDS_FDDTAB_BLT2,	CDpyType::N|CDpyType::L,
	"REMP",		 4,	IDS_FDDTAB_REMP,	CDpyType::N|CDpyType::L,
	"APC3",		 3,	IDS_FDDTAB_APC3,	CDpyType::N|CDpyType::L,
	"WRO1",		 5,	IDS_FDDTAB_WRO1,	CDpyType::N|CDpyType::L,
	"VIA3",		 3,	IDS_FDDTAB_VIA3,	CDpyType::N|CDpyType::L,
	"GTD1",		 5,	IDS_FDDTAB_GTD1,	CDpyType::N|CDpyType::L,
	"GTD2",		 5,	IDS_FDDTAB_GTD2,	CDpyType::N|CDpyType::L,
	"CKIF",		 5,	IDS_FDDTAB_CKIF,	CDpyType::N|CDpyType::L,
	"CKIT",		 5,	IDS_FDDTAB_CKIT,	CDpyType::N|CDpyType::L,
	"FTYP",		 1,	IDS_FDDTAB_FTYP,	CDpyType::N|CDpyType::L,
	"RMTI",		14,	IDS_FDDTAB_RMTI,	CDpyType::T|CDpyType::TT,
	NULL,		0,	0,					0

									};
*/
/////////////////////////////////////////////////////////////////////////////
// CFidasUtilities

CString AirportTablename;
CString AirlineTablename;

CStringVector CFidasUtilities::omDebugInfo;

bool CFidasUtilities::Initialize()
{
	char pclTmp[128];
	
	GetPrivateProfileString(ApplicationName(),"AirportTablename", "APT", pclTmp, sizeof pclTmp, ConfigurationPath());
	AirportTablename = pclTmp;																					

	GetPrivateProfileString(ApplicationName(),"AirlineTablename", "ALT", pclTmp, sizeof pclTmp,ConfigurationPath());	
	AirlineTablename = pclTmp;																				
	return true;																
}

CString CFidasUtilities::ConfigurationPath()
{
	static CString olConfigPath;
	if (!olConfigPath.GetLength())
	{
		if (getenv("CEDA") == NULL) 
			olConfigPath = "C:\\UFIS\\SYSTEM\\CEDA.INI"; 
		else 
			olConfigPath = getenv("CEDA");
	}

	return olConfigPath;
		
}

CString CFidasUtilities::HomeAirport()
{
	static CString olHomeAirport;
	if (!olHomeAirport.GetLength())
	{
		char pclHopo[256];
		GetPrivateProfileString("GLOBAL", "HOMEAIRPORT"	  , "",pclHopo, sizeof pclHopo,ConfigurationPath());
		olHomeAirport = pclHopo;
	}

	return olHomeAirport;
}

CString CFidasUtilities::TableExtension()
{
	static CString olTableExtension;
	if (!olTableExtension.GetLength())
	{
		char pclTabExt[256];
		GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "",pclTabExt, sizeof pclTabExt, ConfigurationPath());
		olTableExtension = pclTabExt;
	}

	return olTableExtension;
}

CString CFidasUtilities::HostName()
{
	static CString olTableExtension;
	if (!olTableExtension.GetLength())
	{
		char pclTabExt[256];
		GetPrivateProfileString(ApplicationName(), "HOSTNAME", "",pclTabExt, sizeof pclTabExt, ConfigurationPath());
		olTableExtension = pclTabExt;
	}

	return olTableExtension;
}

CString CFidasUtilities::Language()
{
	static CString olLanguage;
	if (!olLanguage.GetLength())
	{
		char pclTabExt[256];
		GetPrivateProfileString("GLOBAL", "LANGUAGE", "",pclTabExt, sizeof pclTabExt, ConfigurationPath());
		olLanguage = pclTabExt;
	}

	return olLanguage;
}

CString CFidasUtilities::ApplicationName()
{
	return "FIDAS";
}

BOOL CFidasUtilities::DisplayDebugInfo()
{
	static int blDisplayDebugInfo = -1;
	if (blDisplayDebugInfo == -1)
	{
		char pclTabExt[256];
		GetPrivateProfileString(ApplicationName(), "DEBUG", "NO",pclTabExt, sizeof pclTabExt, ConfigurationPath());
		if (stricmp(pclTabExt,"YES") == 0)
			blDisplayDebugInfo = TRUE;
		else
			blDisplayDebugInfo = FALSE;
	}

	return blDisplayDebugInfo;
}

CString CFidasUtilities::GetString(UINT upId)
{

	CString olString;
	olString.LoadString(upId);
	int ind = olString.Find("*INTXT*",0);
	if (ind > 0)
		olString = olString.Left(ind);
	return olString;
}

int CFidasUtilities::ExtractTextLineFast(CStringArray &opItems,const char *pcpLineText,const char *pcpSepa)
{
	ASSERT(pcpLineText);
	ASSERT(pcpSepa);

	int ilSepaLen = strlen(pcpSepa);
	int ilCount = 0;
	char *currPtr;
	currPtr = strstr(pcpLineText, pcpSepa);
	while (currPtr != NULL)
	{
		*currPtr = '\0';
		opItems.Add(pcpLineText);
		ilCount++;
		pcpLineText = currPtr + ilSepaLen;
		currPtr		= strstr(pcpLineText, pcpSepa);
	}
	if(strcmp(pcpLineText, "") != 0)
	{
		opItems.Add(pcpLineText);
		ilCount++;
	}

	return ilCount;
}

CString CFidasUtilities::GetString(const CDpyType::Type& repType)
{ 
	switch(repType)
	{
	case CDpyType::N:
		return CFidasUtilities::GetString(IDS_CDPY_TYPE_N);
	break;
	case CDpyType::M:
		return CFidasUtilities::GetString(IDS_CDPY_TYPE_M);
	break;
	case CDpyType::J:
		return CFidasUtilities::GetString(IDS_CDPY_TYPE_J);
	break;
	case CDpyType::S:
		return CFidasUtilities::GetString(IDS_CDPY_TYPE_S);
	break;
	case CDpyType::L:
		return CFidasUtilities::GetString(IDS_CDPY_TYPE_L);
	break;
	case CDpyType::LJ:
		return CFidasUtilities::GetString(IDS_CDPY_TYPE_LJ);
	break;
	case CDpyType::T:
		return CFidasUtilities::GetString(IDS_CDPY_TYPE_T);
	break;
	case CDpyType::D:
		return CFidasUtilities::GetString(IDS_CDPY_TYPE_D);
	break;
	case CDpyType::DT:
		return CFidasUtilities::GetString(IDS_CDPY_TYPE_DT);
	break;
	case CDpyType::CT:
		return CFidasUtilities::GetString(IDS_CDPY_TYPE_CT);
	break;
	case CDpyType::CD:
		return CFidasUtilities::GetString(IDS_CDPY_TYPE_CD);
	break;
	case CDpyType::CDT:
		return CFidasUtilities::GetString(IDS_CDPY_TYPE_CDT);
	break;
	case CDpyType::TT:
		return CFidasUtilities::GetString(IDS_CDPY_TYPE_TT);
	break;
	case CDpyType::TD:
		return CFidasUtilities::GetString(IDS_CDPY_TYPE_TD);
	break;
	case CDpyType::TDT:
		return CFidasUtilities::GetString(IDS_CDPY_TYPE_TDT);
	break;
	case CDpyType::R:
		return CFidasUtilities::GetString(IDS_CDPY_TYPE_R);
	break;
	case CDpyType::B:
		return CFidasUtilities::GetString(IDS_CDPY_TYPE_B);
	break;
	case CDpyType::A:
		return CFidasUtilities::GetString(IDS_CDPY_TYPE_A);
	break;
	case CDpyType::AL:
		return CFidasUtilities::GetString(IDS_CDPY_TYPE_AL);
	break;
	default :
		return "";
	}
}

BOOL CFidasUtilities::IsValidString(const CString& ropString,int ipMaxChars)
{
	bool isvalid = ropString.GetLength() <= ipMaxChars;

	for (int t = ropString.GetLength() - 1; (t >= 0) && (isvalid); t--)
	{
		isvalid = (((ropString[t] >= 'a') && (ropString[t] <= 'z')) ||
				   ((ropString[t] >= 'A') && (ropString[t] <= 'Z')) ||
				   ((ropString[t] >= '0') && (ropString[t] <= '9')) ||
					(ropString[t] == '_'));
	}

	return isvalid;

}

BOOL CFidasUtilities::IsValidSeparatorString(const CString& ropString,int ipMaxChars)
{
	bool isvalid = ropString.GetLength() <= ipMaxChars;

	for (int t = ropString.GetLength() - 1; (t >= 0) && (isvalid); t--)
	{
		isvalid = ropString[t] != '[' && ropString[t] != ']';
	}

	return isvalid;

}

BOOL CFidasUtilities::GetTableList(const CDpyType::Type& repType,CStringVector& ropTableNames)
{
	CMainFrame *pMainFrm = (CMainFrame *)AfxGetMainWnd();
	if (!pMainFrm)
		return FALSE;

	ropTableNames.push_back("FDDTAB");
	ropTableNames.push_back("FLVTAB");
	ropTableNames.push_back("DEVTAB");/*added by JHI 05062002*/
	ropTableNames.push_back("FDVTAB");/*added by JHI 27072002*/

	return TRUE;	
}

BOOL CFidasUtilities::GetFieldList(const CDpyType::Type& repType,const CString& ropTable,CFieldInfoVector& ropFieldInfos)
{
	CMainFrame *pMainFrm = (CMainFrame *)AfxGetMainWnd();
	if (!pMainFrm)
		return FALSE;

	CFieldInfoVector olFieldInfos;

	pMainFrm->pomUfisDatabase->GetFieldInfos(ropTable,olFieldInfos);
	
	for (int i = 0; i < olFieldInfos.size();i++)
	{
		const CFieldInfo& olInfo = olFieldInfos.at(i);

		switch(repType)
		{
		case CDpyType::T:
		case CDpyType::CT:
		case CDpyType::TT:
			ropFieldInfos.push_back(olInfo);
		break;
		case CDpyType::D:
		case CDpyType::CD:
		case CDpyType::TD:
			ropFieldInfos.push_back(olInfo);
		break;
		case CDpyType::DT:
		case CDpyType::CDT:
		case CDpyType::TDT:
			ropFieldInfos.push_back(olInfo);
		break;
		case CDpyType::B:
				if (ropTable == "FIDTAB")
				{
					if (olInfo.csName.Find('B') == 0)
						ropFieldInfos.push_back(olInfo);
				}
				else
				{
					ropFieldInfos.push_back(olInfo);
				}
		break;
		case CDpyType::A:
			if (ropTable == "FDDTAB")
			{
				ropFieldInfos.push_back(olInfo);
			}
			else if (ropTable == "FLVTAB")
			{
				ropFieldInfos.push_back(olInfo);
			}
			else if (ropTable == "APTTAB")
			{
				if (olInfo.csName.Find('A') == 0)
					ropFieldInfos.push_back(olInfo);
			}
			else
			{
				ropFieldInfos.push_back(olInfo);
			}
		break;
		case CDpyType::AL:
			if (ropTable == "FDDTAB")
			{
				ropFieldInfos.push_back(olInfo);
			}
			else if (ropTable == "FLVTAB")
			{
				ropFieldInfos.push_back(olInfo);
			}
			else
			{
				ropFieldInfos.push_back(olInfo);
			}
		break;
		case CDpyType::J:
		case CDpyType::LJ:
			if (olInfo.csName == "FLNO")
				ropFieldInfos.push_back(olInfo);
		break;
		default :
			ropFieldInfos.push_back(olInfo);
		}
	}

	return TRUE;
}

void CFidasUtilities::LogDebugInfo(const char *popFormat,...)
{
	ASSERT(popFormat);
	if (!DisplayDebugInfo())
		return;

	va_list list;
	va_start (list,popFormat);

	static char olBuffer[4096];
	vsprintf(olBuffer,popFormat,list);
	va_end(list);
	ASSERT(strlen(olBuffer) < sizeof(olBuffer) - 1);
	omDebugInfo.push_back(olBuffer);

	TRACE(olBuffer);
}

void CFidasUtilities::GetDebugInfo(CListBox& ropDebugInfo)
{
	ropDebugInfo.ResetContent();

	for (int i = 0; i < omDebugInfo.size(); i++)
	{
		ropDebugInfo.AddString(omDebugInfo[i]);
	}

	ropDebugInfo.Invalidate();
}
