#if !defined(AFX_FIDASAIRPORTDIALOG_H__C3385C77_5A35_11D5_8124_00010215BFDE__INCLUDED_)
#define AFX_FIDASAIRPORTDIALOG_H__C3385C77_5A35_11D5_8124_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FidasAirportDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFidasAirportDialog dialog
#include <vector>

class CFieldDesc;
class CFieldInfo;

typedef std::vector<CString> CStringVector;
typedef std::vector<CFieldInfo> CFieldInfoVector;

class CFidasAirportDialog : public CDialog
{
// Construction
public:
	CFidasAirportDialog(CWnd* pParent = NULL);   // standard constructor
	void SetFieldDescription(CFieldDesc *prpFieldDesc);

// Dialog Data
	//{{AFX_DATA(CFidasAirportDialog)
	enum { IDD = IDD_UFIS_AIRPORT };
	CButton	m_OK_Button;
	CListBox	m_Available_List;
	CListBox	m_Display_List;
	CString	omSeparator;
	int		imOriginOnly;
	int		imStandard;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFidasAirportDialog)
	public:
	virtual void OnFinalRelease();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFidasAirportDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnUfisAirportCancelButton();
	afx_msg void OnUfisAirportOkButton();
	afx_msg void OnUfisAirportAddButton();
	afx_msg void OnUfisAirportRemoveButton();
	afx_msg void OnChangeUfisSeparatorEdit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CFidasAirportDialog)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
private: // implementation data
	CFieldDesc	*pomFieldDesc;
	CFieldInfoVector	omFieldInfos;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIDASAIRPORTDIALOG_H__C3385C77_5A35_11D5_8124_00010215BFDE__INCLUDED_)
