#if !defined(AFX_FIDASREMARKDIALOG_H__FA5AF4B7_6536_11D5_8132_00010215BFDE__INCLUDED_)
#define AFX_FIDASREMARKDIALOG_H__FA5AF4B7_6536_11D5_8132_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FidasRemarkDialog.h : header file
//

#include <FidasColorButton.h>
#include <vector>

class CFieldDesc;
class CFieldInfo;

typedef std::vector<CString> CStringVector;
typedef std::vector<CFieldInfo> CFieldInfoVector;


/////////////////////////////////////////////////////////////////////////////
// CFidasRemarkDialog dialog

class CFidasRemarkDialog : public CDialog
{
// Construction
public:
	CFidasRemarkDialog(CWnd* pParent = NULL);   // standard constructor
	void SetFieldDescription(CFieldDesc *prpFieldDesc);

private:
// Dialog Data
	//{{AFX_DATA(CFidasRemarkDialog)
	enum { IDD = IDD_UFIS_REMARK };
	CButton	m_OK_Button;
	CListBox	m_Available_List;
	CListBox	m_Fieldlist_List;
	CButton	m_ShowTime_Check;
	//}}AFX_DATA
	CFidasColorButton m_Select_BlinkColor_Button;	// wes 74
	CFidasColorButton m_Select_BlinkColor_Button2;	// wes 74


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFidasRemarkDialog)
	public:
	virtual void OnFinalRelease();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFidasRemarkDialog)
	afx_msg void OnUfisRemarkCancelButton();
	afx_msg void OnUfisRemarkOkButton();
	afx_msg void OnUfisRemarkButtonBlinkcolor();
	afx_msg void OnUfisRemarkButtonBlinkcolor2();
	afx_msg void OnUfisRemarkButtonRemovefield();
	afx_msg void OnUfisRemarkButtonAddfieldtolist();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CFidasRemarkDialog)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
private:
	CFieldDesc*			pomFieldDesc;
	CFieldInfoVector	omFieldInfos;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIDASREMARKDIALOG_H__FA5AF4B7_6536_11D5_8132_00010215BFDE__INCLUDED_)
