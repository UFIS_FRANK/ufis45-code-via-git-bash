/*
 * AAB/AAT UFIS Display Handler
 *
 * FIDAS display type definitions starter version -- j. heilig June 7 2001
 * 
 * June 14 2001 design review and 1st implementation j.heilig
 *
 * Jun 22 01 released to test (jhe)
 *
 */

#ifndef CDpyType_included
#define CDpyType_included

#include <aatlib.h>

//===== Base class

class CDpyType
{
public:
	static CDpyType *Create(char *);
	enum Type { any,N,S,L,T,D,DT,CT,CD,CDT,TT,TD,TDT,R,B,A,AL,M,J,LJ} ;
	virtual ~CDpyType() {};
	Type GetType() const {return type;}
	virtual CString ToString() const = 0;
	virtual CDpyType *Clone(const CDpyType *) = 0;
	virtual int operator == (const CDpyType &) const = 0;
	virtual int operator != (const CDpyType &a) const { return !(*this == a); }
protected:
	CDpyType(Type t) : type(t) {};
private:
	CDpyType();
	Type type;
// blocker
	CDpyType &operator=(const CDpyType &);
	CDpyType(const CDpyType &);
};

//===== types N, M, J

class CDpyType_N : public CDpyType
{
public:
	CDpyType_N(Type t) : CDpyType(t) {};
	CDpyType_N() : CDpyType(N) {};
	CDpyType_N(const CDpyType_N &c) : CDpyType(N) {}
	CDpyType *Clone(const CDpyType *p);
	CString ToString() const;
	int operator==(const CDpyType &) const;
};

class CDpyType_M : public CDpyType_N
{
public:
	CDpyType_M() : CDpyType_N(M) {};
	CDpyType_M(const CDpyType_M &c) : CDpyType_N(M) {}
};

class CDpyType_J : public CDpyType_N
{
public:
	CDpyType_J() : CDpyType_N(J) {};
	CDpyType_J(const CDpyType_J &c) : CDpyType_N(J) {}
};

//===== type S

class CDpyType_S : public CDpyType
{
public:
	CDpyType_S() : CDpyType(S) {};
	CDpyType_S(const CDpyType_S &c) : CDpyType(S) {csText = c.csText;}
	CDpyType *Clone(const CDpyType *p);
	const CString &GetText() const { return csText; }
	void SetText(const CString &s) {csText = s; }
	CString ToString() const;
	int operator==(const CDpyType &) const;
private:
	CString csText;
};


//===== type L, LJ

class CDpyType_L : public CDpyType
{
public:
	CDpyType_L(Type t) : CDpyType(t) {};
	CDpyType_L() : CDpyType(L) {};
	CDpyType_L(const CDpyType_L &c) : CDpyType(L) {csExt = c.csExt;}
	CDpyType *Clone(const CDpyType *p);
	const CString &GetExtension() const { return csExt; }
	void SetExtension(const CString &e) {csExt = e; }
	CString ToString() const;
	int operator==(const CDpyType &) const;
protected:
	CString csExt;
};

class CDpyType_LJ : public CDpyType_L
{
public:
	CDpyType_LJ() : CDpyType_L(LJ) {};
	CDpyType_LJ(const CDpyType_LJ &c) : CDpyType_L(LJ) {csExt=c.csExt;}
};

//===== types T, D, DT, CT, CD, CDT, TT, TD, TDT (could use template)

class CDpyType_TimeDate : public CDpyType 
{ 
protected:
	CDpyType_TimeDate(Type t) : CDpyType(t) {};
	CDpyType_TimeDate(const CDpyType_TimeDate &c);
public:
	CDpyType *Clone(const CDpyType *p);
	const CString &GetFormat() const { return csFmt; } 
	void SetFormat(const CString &f) {csFmt = f; }; 
	CString ToString() const;
	int operator==(const CDpyType &) const;
private: 
	CString csFmt;
};


class CDpyType_T : public CDpyType_TimeDate
{
public:
	CDpyType_T() : CDpyType_TimeDate(T) {};
};

class CDpyType_D : public CDpyType_TimeDate
{
public:
	CDpyType_D() : CDpyType_TimeDate(D) {};
};

class CDpyType_DT : public CDpyType_TimeDate
{
public:
	CDpyType_DT() : CDpyType_TimeDate(DT) {};
};

class CDpyType_CT : public CDpyType_TimeDate
{
public:
	CDpyType_CT() : CDpyType_TimeDate(CT) {};
};

class CDpyType_CD : public CDpyType_TimeDate
{
public:
	CDpyType_CD() : CDpyType_TimeDate(CD) {};
};

class CDpyType_CDT : public CDpyType_TimeDate
{
public:
	CDpyType_CDT() : CDpyType_TimeDate(CDT) {};
};

class CDpyType_TT : public CDpyType_TimeDate
{
public:
	CDpyType_TT() : CDpyType_TimeDate(TT) {};
};

class CDpyType_TD : public CDpyType_TimeDate
{
public:
	CDpyType_TD() : CDpyType_TimeDate(TD) {};
};

class CDpyType_TDT : public CDpyType_TimeDate
{
public:
	CDpyType_TDT() : CDpyType_TimeDate(TDT) {};
};


//===== type R

class CDpyType_R : public CDpyType
{
public:
	CDpyType_R() : CDpyType(R), start(0), end(0) {};
	CDpyType_R(const CDpyType_R &);
	CDpyType *Clone(const CDpyType *p);
	const CString &GetSeparator() const { return csSep; }
	void SetSeparator(const CString &s) {csSep = s; }
	const CString &GetTableName() const { return csTableName; }
	void SetTableName(const CString &s) {csTableName = s; }
	const CString &GetFieldName() const { return csFieldName; }
	void SetFieldName(const CString &s) {csFieldName = s; }
	int GetStart() const {return start; }
	void SetStart(int v) { start=v; }
	int GetEnd() const {return end; }
	void SetEnd(int v) { end=v; }
	CString ToString() const;
	int operator==(const CDpyType &) const;
private:
	CString csSep;
	CString csTableName;
	CString csFieldName;
	int start, end;
};


//===== type B

class CDpyType_B : public CDpyType
{
public:
	CDpyType_B();
	CDpyType_B(const CDpyType_B &);
	CDpyType *Clone(const CDpyType *p);
	~CDpyType_B();
	const CString &GetRemarkType() const { return csType; }
	void SetRemarkType(const CString &s) { csType = s; }
	int GetBlinkC() const { return iBlinkC; }
	void SetBlinkC(int c) {iBlinkC = c; }
	char GetTimeConversion() const { return cTimeConversion; }
	void SetTimeConversion(char c) { cTimeConversion = c; }
	const CStringVector *FieldNames() const { return pFNVector; }
	CStringVector *GetFieldNames();
	void SetFieldNames(CStringVector *);
	CString ToString() const;
	int operator==(const CDpyType &) const;
private:
	CString csType;
	int iBlinkC;
	char cTimeConversion;
	CStringVector *pFNVector;
};

//===== type A

class CDpyType_A : public CDpyType
{
public:
	CDpyType_A();
	CDpyType_A(const CDpyType_A &);
	CDpyType *Clone(const CDpyType *p);
	~CDpyType_A();
	int GetNAirports() const { return iNAirp; }
	void SetNAirports(int n) {iNAirp = n; }
	char GetSequence() const { return cSeq; }
	void SetSequence(char s) { cSeq = s; }
	const CString &GetSeparator() const { return csSep; }
	void SetSeparator(const CString &s) { csSep = s; }
	const CStringVector *FieldNames() const { return pFNVector; }
	CStringVector *GetFieldNames();
	void SetFieldNames(CStringVector *);
	CString ToString() const;
	int operator==(const CDpyType &) const;
private:
	int iNAirp;
	char cSeq;
	CString csSep;
	CStringVector *pFNVector;
};

//===== type AL

class CDpyType_AL : public CDpyType
{
public:
	CDpyType_AL() : CDpyType(AL) {};
	CDpyType_AL(const CDpyType_AL &);
	CDpyType *Clone(const CDpyType *p);
	const CString &GetFieldName() const { return csFieldName; }
	void SetFieldName(const CString &n) {csFieldName = n; }
	CString ToString() const;
	int operator==(const CDpyType &) const;
private:
	CString csFieldName;
};

#endif // CDpyType_included
