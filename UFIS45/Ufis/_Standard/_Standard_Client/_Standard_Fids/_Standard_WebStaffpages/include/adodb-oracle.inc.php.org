<?php
/*
V0.60 08 Nov 2000 (c) 2000 John Lim. All rights reserved.
  Released under Lesser GPL library license. See License.txt.
  Latest version is available at http://php.weblogs.com/
*/
// select table_name from cat -- MetaTables

class ADODB_oracle extends ADODBConnection {
        var $databaseType = "oracle";
        var $replaceQuote = "\'"; // string to use to replace quotes
       // var $fmtDate = "'d-M-Y'";
       // var $fmtTimeStamp = "'Y-m-d, h:i:sA'";
        var $concat_operator='||';
        function ADODB_oracle() {
        }

	// format and return date string in database date format
	function DBDate($d)
	{
		return 'TO_DATE('.date($this->fmtDate,$d).",'YYYY-MM-DD')";
	}
	
	// format and return date string in database timestamp format
	function DBTimeStamp($ts)
	{
		return 'TO_DATE('.date($this->fmtTimeStamp,$d).",'YYYY-MM-DD, HH:RR:SSAM')";
	}
	
        function BeginTrans()
	{      
               $this->autoCommit = false;
               OCIcommit($this->_connectionID);
               return true;
	}
	
	function CommitTrans()
	{
               $ret = OCIcommit($this->_connectionID);
	       OCIcommit($this->_connectionID);
	       return $ret;
	}
	
	function RollbackTrans()
	{
                $ret = OCIrollback($this->_connectionID);
                OCIcommit($this->_connectionID);
		return $ret;
	}
        
        function SelectDB($dbName) {
               return false;
        }

	/* there seems to be a bug in the oracle extension -- always returns ORA-00000 - no error */
        function ErrorMsg() {
                $this->_errorMsg = OCIerror($this->_connectionID);
                return $this->_errorMsg;
        }
	
	function ErrorNo() {
                return OCIerror($this->_connectionID);
        }
	

        // returns true or false
        function _connect($argHostname, $argUsername, $argPassword, $argDatabasename)
        {
                $this->_connectionID = OCIlogon($argUsername,$argPassword, $argDatabasename);
                if ($this->_connectionID === false)
									return false;
                if ($this->autoCommit) OCIcommit($this->_connectionID);
									return true;
        }
        // returns true or false
        function _pconnect($argHostname, $argUsername, $argPassword, $argDatabasename)
        {
                 $this->_connectionID = OCIplogon($argUsername,$argPassword, $argDatabasename);
                if ($this->_connectionID === false) return false;
                if ($this->autoCommit) OCIcommit($this->_connectionID);
                return true;
        }

        // returns query ID if successful, otherwise false
        function _query($sql,$inputarr)
        {
           $curs = OCINewCursor($this->_connectionID);
		 if ($curs === false) return false;
		 
		 $stmt  =  @OCIParse($this->_connectionID,$sql);  
        if  (!$stmt)  {  
        return  false;  
        }  
        if  (@OCIExecute($stmt))  {  
        return  $stmt;  
        }  

//if (!OCIparse($this->_connectionID,$sql)) return false;
//if (OCIExecute($curs)) return $curs;
      @OCIFreeCursor($curs);  
        return  false;  

        }

        // returns true or false
        function _close()
        {
				       return @OCIFreeCursor($this->_connectionID);
        }
}

/*--------------------------------------------------------------------------------------
         Class Name: Recordset
--------------------------------------------------------------------------------------*/
class ADORecordset_oracle extends ADORecordSet {

        var $databaseType = "oracle";
	var $bind;
	
        function ADORecordset_oracle($queryID)
        {
		$this->_queryID = $queryID;
		$this->fields = array();
		if ($queryID) {
			$this->_currentRow = 0;
			$this->EOF = !$this->_fetch();
			@$this->_initrs();
		} else {
			$this->_numOfRows = 0;
			$this->_numOfFields = 0;
			$this->EOF = true;
		}
		
 		return $this->_queryID;
        }



        /*        Returns: an object containing field information.
                Get column information in the Recordset object. fetchField() can be used in order to obtain information about
                fields in a certain query result. If the field offset isn't specified, the next field that wasn't yet retrieved by
                fetchField() is retrieved.        */

        function FetchField($fieldOffset = 0)
        {
                 $fld = new ADODBFieldObject;
                 $fld->name = OCIcolumnname($this->_queryID, $fieldOffset);
                 $fld->type = OCIcolumntype($this->_queryID, $fieldOffset);
                 $fld->max_length = OCIcolumnsize($this->_queryID, $fieldOffset);
                 return $fld;
        }

	/* Use associative array to get fields array */
	function Fields($colname)
	{
		if (!$this->bind) {
			$this->bind = array();
			for ($i=1; $i < $this->_numOfFields+1; $i++) {
				$o = $this->FetchField($i);
				// We must declare $i-1 because when we ask a filed 
				//  by name it starts the array from 0
				$this->bind[strtoupper($o->name)] = $i-1;

			}
		}
//		 echo strtoupper($colname);
		 return $this->fields[$this->bind[strtoupper($colname)]];

	}
	
        function _initrs()
        {
                $this->_numOfRows = -1;
                $this->_numOfFields = @OCInumcols($this->_queryID);
        }

	
        function _seek($row)
        {
                return false;
        }

        function _fetch($ignore_fields=false) {
                return @OCIfetchinto($this->_queryID,&$this->fields,OCI_NUM+OCI_RETURN_LOBS);
        }

        function _fetchall() {
				$nrows=@OCIFetchStatement($this->_queryID,$results);

				return $results;				
        }
		
        /*        close() only needs to be called if you are worried about using too much memory while your script
                is running. All associated result memory for the specified result identifier will automatically be freed.        */

        function _close() {
				 return @OCIFreeCursor($this->_connectionID);
        }

        function MetaType($t,$len=-1)
        {
                switch (strtoupper($t)) {
                case 'VARCHAR':
                case 'VARCHAR2':
                case 'CHAR':
                        if ($len <= $this->blobSize) return 'C';
                case 'LONG':
                case 'LONG RAW':
                        return 'B';

                case 'DATE': return 'D';

                //case 'T': return 'T';

                //case 'L': return 'L';

                default: return 'N';
                }
        }
}
?>
