#!/bin/bash
sqlplus /nolog << EOF
set echo on
set termout on
spool crdbUserAID.lst
connect system/celicatyt as sysdba
REM connect system/manager

create user aid
identified by aid
default tablespace ceda02
temporary tablespace temp
profile default
/
grant connect,resource,dba to aid identified by aid;
/
spool off
