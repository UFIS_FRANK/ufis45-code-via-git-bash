#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Interface/flrhdl.c 1.16 2012/09/05 2012/9/5 10:41:49 fya Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  Read flight data file or table and send IFR/UFR to FLIGHT process     */
/*  To prevent slowness in process, this program is intention for 1       */
/*  interface per process                                                 */
/*                                                                        */
/*  Program       : FLRHDL.H                                              */
/*                  FLIGHT READER HANDLER                                 */
/*  Revision date :                                                       */
/*                                                                        */
/*  Author        :  MEI                                                  */
/*                                                                        */
/*  HISTORY       :                                                       */
/*  MEI 2011-11-17: Code Creation                                         */
/*  MEI 2012-01-16: When too many backlog files, process run out of memory */
/*  MEI 2012-01-31: Send new command "LOA" to new process "LOAHDL" for load calculation - v1.3 */
/*  MEI 2012-03-02: Configurable flight process to send flight update - v1.4 */
/*  DKA 2012-03-06: v.1.5: Send FLTN to FLIGHT at least 3 char wide, zero pad if necessary. */
/*                  This is to prevent FLTHDL from updating/creating CSGN non- */
/*                  zero padded.                                          */ 
/*  MEI 2012-03-07: Remove ADID and hhmmss in flight search - v1.6        */
/*  MEI 2012-03-08: The same file is not seen being renamed. Remove file if error. - v1.7        */
/*  MEI 2012-03-14: Cater to exception when ALC is not valid and allow update to only config fields - v1.8 */
/*  MEI 2012-03-20: Configurable flight insert - v1.9 */
/*  MEI 2012-03-26: Hard code flight search to exclude T and G - v1.10 */
/*  MEI 2012-03-27: Bug in insertload, affecting blank values inserted - v1.11 */
/*  MEI 2012-03-27: To enable loahdl to calculate, include E2 and infant - v1.12 */
/*  MEI 2012-04-02: Process drops FLTN with suffix - v1.13 */
/*  MEI 2012-04-03: ICD mentioned if REGN available, ignore ACT3 - v1.14 */
/*  MEI 2012-04-09: Ignore FTYP recv for dep flight - v1.15 */
/*  FYA 2012-08-31: UFIS-2209 Only when AFTTAB.DQCS does not equal LC,C and S, then any updates from flrhdl interface will be applied to LOATAB. */
/* ********************************************************************** */
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS45 (c) ABB AAT/I skeleton.c 45.1.0 / 11.12.2002 HEB";

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "syslib.h"
#include "db_if.h"
#include "dirutil.h"
#include <time.h>
#include <cedatime.h>


#ifdef _HPUX_SOURCE
    extern int daylight;
    extern long timezone;
    extern char *tzname[2];
#else
    extern int _daylight;
    extern long _timezone;
    extern char *_tzname[2];
#endif

#define     F   0
#define     J   1
#define     Y   2
#define     E2  3
#define     I   4

#define     BAG     0
#define     CARGO   1
#define     MAIL    2

int debug_level = 0;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;

static char cgProcessName[20] = "\0";
static char cgFlightProcessName[20] = "\0";
static char  cgConfigFile[512] = "\0";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */
static char pcgTables[30][20];
static long lgEvtCnt = 0;

typedef struct 
{
    char pclName[6];
    char pclValue[1026];
} AFTREC;


typedef struct 
{
    int ilNumAftFields;
    int ilNumAftUpdFields;
    int ilNumAftUpdOnlyIfBlankFields;
    int ilSeqValue;
    int blIsFile;
    int blLogErrorInFile;
    int blMapStatFtyp;
    int blProcessFltInsert;
    int blIgnoreEmptyFld;
    char pclIntFilePath[128];
    char pclIntFileName[128];
    char pclAftFields[1024];    
    char pclAftUpdFields[1024];
    char pclAftUpdOnlyIfBlankFields[1024];
    char pclFieldType[512];
    char pclSeqKey[12];
    char pclDssnKey[8];
    char pclDepIgnoreFtypRecv[20];
} DATA_NODE;

typedef struct 
{
    int ilStatLen;
    char pclStat[128];
    char pclFtyp[128];
} MAP_STAT_FTYP_NODE;

DATA_NODE rgDATA;
MAP_STAT_FTYP_NODE rgMapStatus;
AFTREC *rgAFT;

static int igMySleepTimer;
static int igNumFileToPauseReading;
static int bgSearchVia;
static int igFlight;
static int igLoahdl;
static int bgRegnAvail;
/******************************************************************************/
/* Function prototypes                                                        */
/******************************************************************************/
static int  Init_Process();
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer);
static int  Reset(void);                       /* Reset program          */
static void Terminate(int ipSleep);            /* Terminate program      */
static void HandleSignal(int);                 /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/
static int HandleData(EVENT *prpEvent);       /* Handles event data     */
static int GetCommand(char *pcpCommand, int *pipCmd);

static int ProbeData();
static int RunSQL( char *pcpSelection, char *pcpData );
static int IsFileExist();
static int GetSeqValues( char *pcpSeqKey, int *ipSeqNumber );
static int UpdateSequence( char *pcpSeqKeys, int ipSeqNum );
static int Init_DataNode( DATA_NODE *rpData );
static int ValidDT( char *pcpDateTimeStr );
static int Init_AFTFields();
static int Reset_AFTFields();
static int Upd_AFTField( char *pcpFieldName, char *pcpFieldValue );
static int Get_AFTValue( char *pcpFieldName, char *pcpFieldValue );
//static int ProcessFlight();
static int ProcessLoad( int *pipBook, int *pipConfig, int *pipActual, int *pipLoad );
static int InsertLoad( char *pcpFlnu, char *pcpType, char *pcpStyp, 
                       char *pcpSstp, char *pcpSsst, int ipValue );
static int ProcessFile( char *pcpFileName );
static int TrimSpace( char *pcpInStr );

//Frank UFIS-2209
int ChkGCFTABToDetermineFreeze(void);
static int igValueFromGCFTABForFreeze = 0;
static int ProcessFlight(char *pcpDQCS);
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int ilRc = RC_SUCCESS;          /* Return code          */
    int ilCnt = 0;
    int ilOldDebugLevel = 0;


    INITIALIZE;         /* General initialization   */

    debug_level = DEBUG;

    strcpy(cgProcessName,argv[0]);

    dbg(TRACE,"MAIN: version <%s>",sccs_version);

    /* Attach to the MIKE queues */
    do
    {
        ilRc = init_que();
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    do
    {
        ilRc = init_db();
        if (ilRc != RC_SUCCESS)
        {
            check_ret(ilRc);
            dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        } /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
        sleep(60);
        exit(2);
    }else{
        dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */

    /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    { 
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    ilRc = SendRemoteShutdown(mod_id);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */

    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */

    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
            ilRc = Init_Process();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"Init_Process: init failed!");
            } /* end of if */

    } else {
        Terminate(30);
    }/* end of if */



    dbg(TRACE,"=====================");
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"=====================");
    

    for(;;)
    {
        ilRc = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* Acknowledge the item */
        ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
        if( ilRc != RC_SUCCESS ) 
        {
            /* handle que_ack error */
            HandleQueErr(ilRc);
        } /* fi */

        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */

        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRc == RC_SUCCESS )
        {          
            lgEvtCnt++;

            switch( prgEvent->command )
            {
                case    HSB_STANDBY :
                        ctrl_sta = prgEvent->command;
                        HandleQueues();
                        break;  
                case    HSB_COMING_UP   :
                        ctrl_sta = prgEvent->command;
                        HandleQueues();
                        break;  
                case    HSB_ACTIVE  :
                        ctrl_sta = prgEvent->command;
                        break;  
                case    HSB_ACT_TO_SBY  :
                        ctrl_sta = prgEvent->command;
                        /* CloseConnection(); */
                        HandleQueues();
                        break;  
                case    HSB_DOWN    :
                        /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                        ctrl_sta = prgEvent->command;
                        Terminate(1);
                        break;  
                case    HSB_STANDALONE  :
                        ctrl_sta = prgEvent->command;
                        ResetDBCounter();
                        break;  
                case    REMOTE_DB :
                        /* ctrl_sta is checked inside */
                        HandleRemoteDB(prgEvent);
                        break;
                case    SHUTDOWN    :
                        /* process shutdown - maybe from uutil */
                        Terminate(1);
                        break;
                    
                case    RESET       :
                        ilRc = Reset();
                        break;
                    
                case    EVENT_DATA  :
                        if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                        {
                            ilRc = HandleData(prgEvent);
                            if(ilRc != RC_SUCCESS)
                            {
                                HandleErr(ilRc);
                            }/* end of if */
                        }
                        else
                        {
                            dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                            DebugPrintItem(TRACE,prgItem);
                            DebugPrintEvent(TRACE,prgEvent);
                        }/* end of if */
                        break;
                
                case    TRACE_ON :
                        dbg_handle_debug(prgEvent->command);
                        break;
                case    TRACE_OFF :
                        dbg_handle_debug(prgEvent->command);
                        break;
            } /* end switch */
        } 
        else 
        {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */

        ilRc = ProbeData();
        /* no files to be read, so sleep a while */
        if( ilRc != RC_SUCCESS )
        {
            dbg( DEBUG, "Sleeping for <%d> seconds....", igMySleepTimer );
            sleep( igMySleepTimer );       
        }
    } /* end for */
    
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
               CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
    dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
    dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
        clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}


static int Init_Process()
{
    int ilRc = RC_SUCCESS;         /* Return code */
    int ilOldDebugLevel = 0;
    char clSection[64] = "\0";
    char clKeyword[64] = "\0";
    char clBreak[24] = "\0";
    char pclTmpStr[1024] = "\0";
    char pclSection[64] = "\0";
    char *pclFunc = "Init_Process";


    if(ilRc == RC_SUCCESS)
    {
        /* read HomeAirPort from SGS.TAB */
        ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
        if (ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"<Init_Process> EXTAB,SYS,HOMEAP not found in SGS.TAB");
            Terminate(30);
        } else {
            dbg(TRACE,"<Init_Process> home airport <%s>",cgHopo);
        }
    }

    
    if(ilRc == RC_SUCCESS)
    {

        ilRc = tool_search_exco_data("ALL","TABEND", cgTabEnd);
        if (ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"<Init_Process> EXTAB,ALL,TABEND not found in SGS.TAB");
            Terminate(30);
        } else {
            dbg(TRACE,"<Init_Process> table extension <%s>",cgTabEnd);
       }
    }

    bgSearchVia = FALSE;
    igLoahdl = tool_get_q_id( "loahdl" );
    dbg( TRACE, "LOAHDL MOD_ID = <%d>", igLoahdl );


    /** Reading MAIN **/
    dbg( TRACE, ".................................................." );
    dbg( TRACE, "............ READING CONFIG SECTION .............." );
    dbg( TRACE, ".................................................." );
    ilRc = ReadConfigEntry( "MAIN", "SECTION", pclSection );
    if( ilRc != RC_SUCCESS )
    {
        dbg( DEBUG, "<%s> ERROR!!!! NOTHING TO BE DONE!!!", pclFunc );
        dbg( DEBUG, "<%s> EXIT!", pclFunc );
        Terminate(30);
    }

    ilRc = ReadConfigEntry( "MAIN", "SLEEP_TIMER_WHEN_NO_FILE", pclTmpStr );
    igMySleepTimer = atoi(pclTmpStr);
    if( igMySleepTimer <= 0 )
        igMySleepTimer = 3;
    dbg( TRACE, "ilRc <%d> igMySleepTimer = <%d>", ilRc, igMySleepTimer );

    ilRc = ReadConfigEntry( "MAIN", "NUM_FILE_TO_PAUSE_READING", pclTmpStr );
    igNumFileToPauseReading = atoi(pclTmpStr);
    if( igNumFileToPauseReading <= 0 )
        igNumFileToPauseReading = 10;
    dbg( TRACE, "ilRc <%d> igNumFileToPauseReading = <%d>", ilRc, igNumFileToPauseReading );

    ilRc = ReadConfigEntry( "MAIN", "SEND_UPD_TO", cgFlightProcessName );
    if( strlen(cgFlightProcessName) <= 0 )
        strcpy( cgFlightProcessName, "flight" );
    dbg( TRACE, "ilRc <%d> cgFlightProcessName = <%s>", ilRc, cgFlightProcessName );

    igFlight = tool_get_q_id( cgFlightProcessName );
    dbg( TRACE, "FLIGHT MOD_ID = <%d>", igFlight ); 
    /** End Reading MAIN **/
    

    Init_DataNode( &rgDATA );
    
    /** Reading SECTION **/
    ilRc = ReadConfigEntry( pclSection, "IS_FILE", pclTmpStr );
    if( ilRc == RC_SUCCESS && pclTmpStr[0] == 'Y' )
        rgDATA.blIsFile = TRUE;
    dbg( TRACE, "<%s> IS_FILE = <%s>", pclSection, rgDATA.blIsFile == TRUE ? "TRUE" : "FALSE" );


    ilRc = ReadConfigEntry( pclSection, "LOG_ERROR_IN_FILE", pclTmpStr );
    if( ilRc == RC_SUCCESS && pclTmpStr[0] == 'Y' )
        rgDATA.blLogErrorInFile = TRUE;
    dbg( TRACE, "<%s> LOG_ERROR_IN_FILE = <%s>", pclSection, rgDATA.blLogErrorInFile == TRUE ? "TRUE" : "FALSE" );


    rgDATA.ilSeqValue = 0;
    ilRc = ReadConfigEntry( pclSection, "SEQUENCE_KEY", rgDATA.pclSeqKey );
    dbg( TRACE, "<%s> ilRc <%d> SEQUENCE_KEY = <%s>", pclSection, ilRc, rgDATA.pclSeqKey );
    if( strlen(rgDATA.pclSeqKey) <= 0 )
    {
        dbg( TRACE, "<%s> No sequence key. Read ALL files", pclSection );
        rgDATA.ilSeqValue = -1;
    }

    ilRc = ReadConfigEntry( pclSection, "DSSN_KEY", rgDATA.pclDssnKey );
    dbg( TRACE, "<%s> ilRc <%d> DSSN_KEY = <%s>", pclSection, ilRc, rgDATA.pclDssnKey );

    ilRc = ReadConfigEntry( pclSection, "INTERFACE_PATH", rgDATA.pclIntFilePath );
    dbg( TRACE, "<%s> ilRc <%d> INTERFACE_PATH = <%s>", pclSection, ilRc, rgDATA.pclIntFilePath );

    ilRc = ReadConfigEntry( pclSection, "FILE_NAME", rgDATA.pclIntFileName );
    dbg( TRACE, "<%s> ilRc <%d> FILE_NAME = <%s>", pclSection, ilRc, rgDATA.pclIntFileName );

    ilRc = ReadConfigEntry( pclSection, "AFT_FIELDS", rgDATA.pclAftFields );
    dbg( TRACE, "<%s> ilRc <%d> AFT_FIELDS = <%s>", pclSection, ilRc, rgDATA.pclAftFields );

    ilRc = ReadConfigEntry( pclSection, "UPD_FIELDS", rgDATA.pclAftUpdFields );
    dbg( TRACE, "<%s> ilRc <%d> UPD_FIELDS = <%s>", pclSection, ilRc, rgDATA.pclAftUpdFields );

    ilRc = ReadConfigEntry( pclSection, "UPD_ONLY_IF_BLANK", rgDATA.pclAftUpdOnlyIfBlankFields );
    dbg( TRACE, "<%s> ilRc <%d> UPD_ONLY_IF_BLANK = <%s>", pclSection, ilRc, rgDATA.pclAftUpdOnlyIfBlankFields );
    
    ilRc = ReadConfigEntry( pclSection, "FIELD_TYPE", rgDATA.pclFieldType );
    dbg( TRACE, "<%s> ilRc <%d> FIELD_TYPE = <%s>", pclSection, ilRc, rgDATA.pclFieldType );

    ilRc = ReadConfigEntry( pclSection, "PROCESS_FLIGHT_INSERT", pclTmpStr ); 
    rgDATA.blProcessFltInsert = FALSE;
    if( ilRc == RC_SUCCESS && pclTmpStr[0] == 'Y' )
        rgDATA.blProcessFltInsert = TRUE;
    dbg( TRACE, "<%s> PROCESS_FLIGHT_INSERT = <%s>", pclSection, rgDATA.blProcessFltInsert == TRUE ? "TRUE" : "FALSE" );

    ilRc = ReadConfigEntry( pclSection, "IGNORE_EMPTY_FIELD", pclTmpStr ); 
    rgDATA.blIgnoreEmptyFld = FALSE;
    if( ilRc == RC_SUCCESS && pclTmpStr[0] == 'Y' )
        rgDATA.blIgnoreEmptyFld = TRUE;
    dbg( TRACE, "<%s> IGNORE_EMPTY_FIELD = <%s>", pclSection, rgDATA.blIgnoreEmptyFld == TRUE ? "TRUE" : "FALSE" );

    ilRc = ReadConfigEntry( pclSection, "DEP_IGNORE_RECV_FTYP", rgDATA.pclDepIgnoreFtypRecv );
    dbg( TRACE, "<%s> ilRc <%d> DEP_IGNORE_RECV_FTYP = <%s>", pclSection, ilRc, rgDATA.pclDepIgnoreFtypRecv );

    
    ilRc = ReadConfigEntry( pclSection, "MAP_STAT_FTYP", pclTmpStr );
    if( ilRc == RC_SUCCESS && pclTmpStr[0] == 'Y' )
    {
        strcpy( pclSection, "STATUS_MAPPING" );
        rgDATA.blMapStatFtyp = TRUE;
        memset( &rgMapStatus, 0, sizeof(MAP_STAT_FTYP_NODE) );
        ReadConfigEntry( pclSection, "LENGTH_STAT", pclTmpStr ); rgMapStatus.ilStatLen = atoi(pclTmpStr);
        ReadConfigEntry( pclSection, "STAT", rgMapStatus.pclStat ); 
        ReadConfigEntry( pclSection, "FTYP", rgMapStatus.pclFtyp ); 

        dbg( TRACE, "<%s> STAT SIZE = <%d>", pclSection, rgMapStatus.ilStatLen );
        dbg( TRACE, "<%s> MAP STAT = <%s>", pclSection, rgMapStatus.pclStat );
        dbg( TRACE, "<%s> MAP FTYP = <%s>", pclSection, rgMapStatus.pclFtyp );
    }
    else
        dbg( TRACE, "<%s> MAP_STAT_FTYP = <FALSE>", pclSection );


    /** End Reading SECTION **/

 
    /** Init Structure **/
    if( rgDATA.ilSeqValue != -1 )
    {
        GetSeqValues( rgDATA.pclSeqKey, &rgDATA.ilSeqValue );
        dbg( TRACE, "<%s> Expecting SEQUENCE_VALUE = <%d>", pclSection, rgDATA.ilSeqValue );
    }

    rgDATA.ilNumAftFields = get_no_of_items( rgDATA.pclAftFields );
    dbg( TRACE, "<%s> NUMBER_OF_AFT_FIELDS = <%d>", pclSection, rgDATA.ilNumAftFields );

    rgDATA.ilNumAftUpdFields = get_no_of_items( rgDATA.pclAftUpdFields );
    dbg( TRACE, "<%s> NUMBER_OF_AFT_UPD_FIELDS = <%d>", pclSection, rgDATA.ilNumAftUpdFields );

    rgDATA.ilNumAftUpdOnlyIfBlankFields = get_no_of_items( rgDATA.pclAftUpdOnlyIfBlankFields );
    dbg( TRACE, "<%s> NUMBER_OF_AFT_UPD_ONLY_IF_BLANK_FIELDS = <%d>", pclSection, rgDATA.ilNumAftUpdOnlyIfBlankFields );

    rgAFT = (AFTREC *) malloc( sizeof(AFTREC) * rgDATA.ilNumAftFields );
    Init_AFTFields();
    /** End Init Structure **/

    ilRc = ReadConfigEntry( "MAIN", "DEBUG_LEVEL", pclTmpStr );
    debug_level = TRACE;
    if( !strcmp( pclTmpStr, "OFF" ) )
        debug_level = 0;
    else if( !strcmp( pclTmpStr, "DEBUG" ) )
        debug_level = DEBUG;
    dbg( TRACE, "DEBUG_LEVEL <%d>", debug_level );
    
    //Frank UFIS-2209
    ChkGCFTABToDetermineFreeze();

    return( RC_SUCCESS );
    
} /* end of initialize */


/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int ilRc = RC_SUCCESS;              /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRc;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
    /* unset SIGCHLD ! DB-Child will terminate ! */

    dbg(TRACE,"Terminate: now DB logoff ...");

    signal(SIGCHLD,SIG_IGN);

    logoff();

    dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


    dbg(TRACE,"Terminate: now leaving ...");

    fclose(outp);

    sleep(ipSleep < 1 ? 1 : ipSleep);
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

    switch(pipSig)
    {
    default :
        Terminate(1);
        break;
    } /* end of switch */

    exit(1);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    switch(pipErr) {
    case    QUE_E_FUNC  :   /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :   /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND  :   /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET   :   /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX   :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE  :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES  :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP  :   /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL  :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG :   /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :   /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :   /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default         :   /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int ilRc = RC_SUCCESS;          /* Return code */
    int ilBreakOut = FALSE;
    
    do
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text; 

        if( ilRc == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRc != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRc);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_COMING_UP   :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_ACTIVE  :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;  

            case    HSB_ACT_TO_SBY  :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;  
    
            case    HSB_STANDALONE  :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                ilBreakOut = TRUE;
                break;  

            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;

            case    SHUTDOWN    :
                Terminate(1);
                break;
                        
            case    RESET       :
                ilRc = Reset();
                break;
                        
            case    EVENT_DATA  :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;

            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;

            default         :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
    } while (ilBreakOut == FALSE);

            ilRc = Init_Process();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"init failed!");
            } 
     

} /* end of HandleQueues */
    



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
    int    ilRc           = RC_SUCCESS;         /* Return code */
    int      ilCmd          = 0;
    int ili;
    BC_HEAD *prlBchead       = NULL;
    CMDBLK  *prlCmdblk       = NULL;
    char    *pclSelection    = NULL;
    char    *pclFields       = NULL;
    char    *pclData         = NULL;
    char    *pclRow          = NULL;
    char        clTable[34];

    prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
    pclData      = pclFields + strlen(pclFields) + 1;


    strcpy(clTable,prlCmdblk->obj_name);

    dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

    /****************************************/
    DebugPrintBchead(TRACE,prlBchead);
    DebugPrintCmdblk(TRACE,prlCmdblk);
    /* 20030630 JIM: may core!: dbg(TRACE,"Command: <%s>,prlCmdblk->command"); */
    dbg(TRACE,"Command: <%s>",prlCmdblk->command);
    dbg(TRACE,"originator follows event = %p ",prpEvent);

    dbg(TRACE,"originator<%d>",prpEvent->originator);
    dbg(TRACE,"selection follows Selection = %p ",pclSelection);
    dbg(TRACE,"selection <%s>",pclSelection);
    dbg(TRACE,"fields    <%s>",pclFields);
    dbg(TRACE,"data      <%s>",pclData);
    /****************************************/
    dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

    return(RC_SUCCESS);
    
} /* end of HandleData */

static int ProbeData()
{
    int ilRc; 
    int ilNumFiles;
    int ilCount;
    int ilFileCount;
    char pclInterfaceFile[512] = "\0";
    char pclFiles[5120] = "\0";
    char *pclFunc = "ProbeData";

    ilCount = 0;
    for( ;; )
    {
        if( rgDATA.ilSeqValue != -1 )
        {
            sprintf( pclInterfaceFile, "%s", rgDATA.pclIntFileName );
            ilRc = IsFileExist( pclInterfaceFile, rgDATA.ilSeqValue );
            if( ilRc != RC_SUCCESS )
                return RC_FAIL;

            ilRc = ProcessFile( pclInterfaceFile );
            if( ilRc != RC_SUCCESS )
                return RC_FAIL;

            /* to prevent no chance to read UFIS queue if files keep coming in */
            ilCount++;
            if( ilCount >= igNumFileToPauseReading )
                return RC_SUCCESS;      
        }
        else
        {
            dbg( TRACE, "%s: Scanning dir <%s>", pclFunc, rgDATA.pclIntFilePath );
            /* scan directory */
            ilNumFiles = getfiles_in_dir( rgDATA.pclIntFilePath, rgDATA.pclIntFileName, pclFiles, igNumFileToPauseReading, SORT_BY_FILENAME );
            if( ilNumFiles <= 0 )
                return RC_FAIL;
            ilFileCount = 1;
            dbg( TRACE, "%s: NUMBER OF FILENAMES READ FROM THE DIRECTORY <%d>", pclFunc, ilNumFiles );
            if( ilNumFiles > igNumFileToPauseReading )
                ilNumFiles = igNumFileToPauseReading;
            dbg( TRACE, "%s: NUMBER OF FILENAMES TO BE PROCESSED NOW <%d>", pclFunc, ilNumFiles );
            while( ilFileCount <= ilNumFiles )
            {
                get_real_item( pclInterfaceFile, pclFiles, ilFileCount );
                ilRc = ProcessFile( pclInterfaceFile );
                if( ilRc != RC_SUCCESS )
                    return RC_FAIL;
                ilFileCount++;
            }
            return RC_SUCCESS;
        }


    }
    return ilRc;
}

static int RunSQL( char *pcpSelection, char *pcpData )
{
    int ilRc = DB_SUCCESS;
    short slSqlFunc = 0;
    short slSqlCursor = 0;
    char pclErrBuff[128];
    char *pclFunc = "RunSQL";

    dbg( DEBUG, "%s: SQL <%s>", pclFunc, pcpSelection );
    ilRc = sql_if ( START, &slSqlCursor, pcpSelection, pcpData );
    close_my_cursor ( &slSqlCursor );

    if( ilRc == DB_ERROR )
    {
        get_ora_err( ilRc, &pclErrBuff[0] );
        dbg( TRACE, "%s OraError! <%s>", pclFunc, &pclErrBuff[0] );
    }
    return ilRc;
}

static int IsFileExist( char *pcpFileName, int ipSeqValue )
{
    struct stat rlFileStat;
    int ilRc;
    char pclFileStr[1024];
    char *pclFunc = "IsFileExist";

    memset( &rlFileStat, 0, sizeof(struct stat) );
    sprintf( pclFileStr, "%s_%3.3d", pcpFileName, ipSeqValue );
    dbg( DEBUG, "%s: Probing file <%s>.....", pclFunc, pclFileStr );
    ilRc = stat( pclFileStr, &rlFileStat );
    if( ilRc == -1 )
    {
        dbg( DEBUG, "%s: error <%d> - <%s>", pclFunc, errno, strerror(errno) );
        return RC_FAIL;
    }
    return RC_SUCCESS;
}

static int ProcessFile( char *pcpFileName )
{
    FILE *rlFileptr;
    int rlBookSEAT[5], rlConfSEAT[5], rlActualSEAT[5];
    int rlLOAD[5];
    int ilRc;
    int ilNumData;
    int ilCount, ilLen;
    int ili, ilj, ilk, ilm;
    char pclFileName[512] = "\0";
    char pclNewFileName[512] = "\0";
    char pclBadFileName[512] = "\0";
    char pclOneFieldType[32] = "\0";
    char pclOneLine[2048] = "\0";
    char pclOneDataStr[2048] = "\0";
    char pclTmpStr[1024] = "\0", pclTmp2 [32];
    char *pclPtr;
    char *pclFunc = "ProcessFile";
    
    //Frank UFIS-2209
    char pclDQCS[16] = "\0";

    sprintf( pclFileName, "%s/%s", rgDATA.pclIntFilePath, pcpFileName );
    sprintf( pclNewFileName, "%s/arc/%s", rgDATA.pclIntFilePath, pcpFileName );
    sprintf( pclBadFileName, "%s/bad/%s", rgDATA.pclIntFilePath, pcpFileName );
    if( rgDATA.ilSeqValue != -1 )
    {
        sprintf( pclFileName, "%s/%s_%3.3d", rgDATA.pclIntFilePath, rgDATA.pclIntFileName, rgDATA.ilSeqValue );
        sprintf( pclNewFileName, "%s/arc/%s_%3.3d", rgDATA.pclIntFilePath, rgDATA.pclIntFileName, rgDATA.ilSeqValue );
    }

    dbg( TRACE, "PROCESSING FILE <%s>........", pclFileName );
    rlFileptr = fopen( pclFileName, "r" );
    if( rlFileptr == NULL )
    {
        dbg( TRACE, "%s: ERROR!!! %d-error<%s> for file <%s>", 
                    pclFunc, errno, strerror(errno), pclFileName );
        return RC_FAIL;
    }
    while( !feof(rlFileptr) )
    {
        pclPtr = fgets( pclOneLine, sizeof(pclOneLine), rlFileptr );
        if( pclPtr == NULL )
            break;
        dbg( DEBUG, "=========================  NEW LINE ========================" );
        dbg( DEBUG, "<%s>", pclOneLine );
        dbg( DEBUG, "============================================================" );
        memset( &rlBookSEAT, -1, sizeof(rlBookSEAT) );
        memset( &rlConfSEAT, -1, sizeof(rlConfSEAT) );
        memset( &rlActualSEAT, -1, sizeof(rlActualSEAT) );
        memset( &rlLOAD, -1, sizeof(rlLOAD) );

        Reset_AFTFields();
        /*Upd_AFTField( "FTYP", "O" );*/

        ilNumData = get_no_of_items( pclOneLine );
        for( ili = 0; ili < ilNumData; ili++ )
        {
            get_real_item( pclOneDataStr, pclOneLine, ili+1 );
            get_real_item( pclOneFieldType, rgDATA.pclFieldType, ili+1 );
            dbg( DEBUG, "%s: Data <%s> Field Type <%s>", pclFunc, pclOneDataStr, pclOneFieldType );
            if( strstr( pclOneFieldType, "datetime" ) != NULL  )
            {
                if( strlen(pclOneDataStr) == 12 )
                    strcat( pclOneDataStr, "00" );  /* for the seconds */
                if( ValidDT(pclOneDataStr) == FALSE )
                {
                    dbg( TRACE, "%s: Invalid DATETIME data <%s>", pclFunc, pclOneDataStr );
                    break;
                }
                pclOneFieldType[4] = '\0'; 
            }
            
            if ( !strcmp( pclOneFieldType, "ALC2" ) )
            {
                ilRc = syslibSearchDbData("ALTTAB","ALC2",pclOneDataStr,"URNO",pclTmpStr,&ilCount,"\n") ;
                if( ilRc != RC_SUCCESS || ilCount == 0 )
                {
                    dbg( TRACE, "%s: Invalid ALC <%s> - not found in basic data", pclFunc, pclOneDataStr );
                    break;
                }
            }
            else if ( !strcmp( pclOneFieldType, "REGN" ) )
            {
                ilLen = strlen(pclOneDataStr);
                ilk = 0;
                for( ilj = 0; ilj < ilLen; ilj++ )
                {
                    if( isalnum(pclOneDataStr[ilj]) )
                        pclTmpStr[ilk++] = pclOneDataStr[ilj];
                }
                pclTmpStr[ilk] = '\0';
                strcpy( pclOneDataStr, pclTmpStr );
                bgRegnAvail = FALSE;
                if( strlen(pclOneDataStr) > 0 )
                    bgRegnAvail = TRUE;
            }
            else if ( !strcmp( pclOneFieldType, "ORG3prev" ) ||
                      !strcmp( pclOneFieldType, "DES3next" ) )
            {
                bgSearchVia = TRUE;
                pclOneFieldType[4] = '\0';      
            }
            else if ( !strcmp( pclOneFieldType, "STAT" ) )
            {
                if( rgDATA.blMapStatFtyp == TRUE )
                {
                    ilj = get_item_no( rgMapStatus.pclStat, pclOneDataStr, rgMapStatus.ilStatLen+1 ) + 1; 
                    if( ilj > 0 )
                    {
                        get_real_item( pclTmpStr, rgMapStatus.pclFtyp, ilj );               
                        dbg( DEBUG, "node# = %d STYP <%s> -> FTYP <%s>", ilj, pclOneDataStr, pclTmpStr );
                        Upd_AFTField( "FTYP", pclTmpStr );
                    }
                }
            }
            if( !strcmp( pclOneFieldType, "ORG3" ) )
            {
                if( !strcmp( pclOneDataStr, cgHopo ) )
                    Upd_AFTField( "ADID", "D" );
            }
            else if( !strcmp( pclOneFieldType, "DES3" ) )
            {
                if( !strcmp( pclOneDataStr, cgHopo ) )
                    Upd_AFTField( "ADID", "A" );
            }               
            else if( !strcmp( pclOneFieldType, "FLTN") )
            {
                ilLen = strlen(pclOneDataStr);
                sprintf( pclTmpStr, "%3.3d", atoi(pclOneDataStr) );
                if( !isdigit(pclOneDataStr[ilLen-1]) )
                    sprintf( pclTmpStr, "%3.3d%c", atoi(pclOneDataStr), pclOneDataStr[ilLen-1] );
                strcpy( pclOneDataStr, pclTmpStr );
            }

            if( strlen(pclOneFieldType) == 4 )  
                Upd_AFTField( pclOneFieldType, pclOneDataStr );

            if ( !strcmp( pclOneFieldType, "stand" ) )
            {
                ilRc = Get_AFTValue( "ADID", pclTmpStr );
                if( ilRc == RC_SUCCESS )
                {
                    if( pclTmpStr[0] == 'A' )
                        Upd_AFTField( "PSTA", pclOneDataStr );
                    else
                        Upd_AFTField( "PSTD", pclOneDataStr );
                }
            }
            /* Pax info */
            if( strstr( pclOneFieldType, "Pax" ) != NULL )
            {
                if( pclOneFieldType[0] == 'F' ) ilj = F;
                else if( pclOneFieldType[0] == 'J' ) ilj = J;
                else if( pclOneFieldType[0] == 'Y' ) ilj = Y;

                if( strstr( pclOneFieldType, "conf" ) != NULL )
                {
                    rlConfSEAT[ilj] = 0;
                    if( strlen(pclOneDataStr) > 0 ) 
                        rlConfSEAT[ilj] = atoi(pclOneDataStr);
                }
                else if( strstr( pclOneFieldType, "book" ) != NULL )
                {
                    rlBookSEAT[ilj] = 0;
                    if( strlen(pclOneDataStr) > 0 ) 
                        rlBookSEAT[ilj] = atoi(pclOneDataStr);
                }
                else if( strstr( pclOneFieldType, "actual" ) != NULL )
                {
                    rlActualSEAT[ilj] = 0;
                    if( strlen(pclOneDataStr) > 0 ) 
                        rlActualSEAT[ilj] = atoi(pclOneDataStr);
                }
            }
            /* Load info */
            if( strstr( pclOneFieldType, "Wt" ) != NULL )
            {
                if( strstr( pclOneFieldType, "bag" ) != NULL ) ilj = BAG;
                else if( strstr( pclOneFieldType, "cargo" ) != NULL ) ilj = CARGO;
                else if( strstr( pclOneFieldType, "mail" ) != NULL ) ilj = MAIL;
                rlLOAD[ilj] = 0;
                if( strlen(pclOneDataStr) > 0 ) 
                    rlLOAD[ilj] = atoi(pclOneDataStr);
            }
        } /* for loop for 1 record */
        //ilRc = ProcessFlight();
        //Frank UFIS-2209
        ilRc = ProcessFlight(pclDQCS);
        if( ilRc == RC_SUCCESS )
        {
        		if( igValueFromGCFTABForFreeze == 1 )
	        	{
	        		//ilRc = ChkAFTTAB_DQCSToDetermineUpdate(pclUrnoForDQCSChk);
	        		/*
	        		if(strncmp(pclDQCS," ",1)==0 || strlen(pclDQCS)==0)
	        		{
	        			dbg( TRACE,"%s pclDQCS<%s> is invalid",pclFunc,pclDQCS);
	        		}
	        		else
	        		{
	        		*/
		        		if(strstr(pclDQCS,"LC") || strncmp(pclDQCS,"C",1) == 0 || strncmp(pclDQCS,"S",1) == 0)
								{
									dbg( TRACE,"%s pclDQCS<%s> is LC, C or S->do not insert LOATAB",pclFunc,pclDQCS);
								}
								else
								{
									dbg( TRACE,"%s pclDQCS<%s> is not LC, C or S->insert LOATAB",pclFunc,pclDQCS);
									ProcessLoad( rlBookSEAT, rlConfSEAT, rlActualSEAT, rlLOAD );
								}
							//} 
	        	}
	        	else
	        	{
	        		dbg(TRACE,"igValueFromGCFTABForFreeze is 0 -> do not freeze data");
	        	}
    		}
    } /* while loop for file */
    fclose( rlFileptr );

    ilRc = rename( pclFileName, pclNewFileName );
    if( ilRc < 0 )
    {
        dbg( TRACE, "%s: ERROR! Cannot rename file <%s> errno <%d>-<%s>", pclFunc, pclFileName, errno, strerror(errno) );
        ilRc = remove( pclFileName );
        if( ilRc < 0 )
        {
            dbg( TRACE, "%s: ERROR! Cannot remove file <%s> errno <%d>-<%s>", pclFunc, pclFileName, errno, strerror(errno) );
            sprintf( pclTmpStr, "mv -f %s %s", pclFileName, pclBadFileName );
            system( pclTmpStr );
        }
    }
    if( rgDATA.ilSeqValue != -1 )
    {
        rgDATA.ilSeqValue++;
        UpdateSequence( rgDATA.pclSeqKey, rgDATA.ilSeqValue );
    }

    return RC_SUCCESS;
} /* ProcessFile */


static int GetSeqValues( char *pcpSeqKey, int *ipSeqNumber )
{
    int ilRc;
    int ilSeqNumber;
    int ilMaxNumber;
    char pclSqlData[128] = "\0";
    char pclSqlBuf[1024] = "\0";
    char pclOneDataItem[32] = "\0";
    char pclTmpStr[32] = "\0";
    char *pclFunc = "GetSeqValues";

    sprintf( pclSqlBuf, "SELECT ACNU,MAXN FROM NUMTAB WHERE KEYS = '%s'", pcpSeqKey );
    ilRc = RunSQL( pclSqlBuf, pclSqlData );
    if( ilRc != DB_SUCCESS )
    {
        dbg( TRACE, "CANNOT FIND SEQUENCE NUMBER IN NUMTAB" );
        *ipSeqNumber = 0;
        return RC_FAIL;
    }

    get_fld( pclSqlData,FIELD_1,STR,20,pclOneDataItem ); ilSeqNumber = atoi(pclTmpStr);
    get_fld( pclSqlData,FIELD_2,STR,20,pclOneDataItem ); ilMaxNumber = atoi(pclTmpStr);

    if( ilSeqNumber <= 0 || ilSeqNumber >= ilMaxNumber)
    {
        ilSeqNumber = 1;
        UpdateSequence( pcpSeqKey, 1 );
    }
    *ipSeqNumber = ilSeqNumber;
    dbg( DEBUG, "%s: SEQ KEY <%s> SEQ NUM = <%d>", pclFunc, pcpSeqKey, *ipSeqNumber );
    return RC_SUCCESS;
}

static int UpdateSequence( char *pcpSeqKeys, int ipSeqNum )
{
    int ilRc;
    char pclSqlBuf[1024];
    char pclSqlData[128];

    sprintf( pclSqlBuf, "UPDATE NUMTAB SET ACNU = %d WHERE KEYS = '%s'",
                        ipSeqNum, pcpSeqKeys );
    ilRc = RunSQL( pclSqlBuf, pclSqlData );
    dbg( DEBUG, "ilRc <%d>", ilRc );
    return ilRc;
}

static int Init_DataNode( DATA_NODE *rpData )
{
    rpData->ilNumAftFields = 0;
    rpData->ilSeqValue = 0;
    rpData->blIsFile = FALSE;
    rpData->blLogErrorInFile = FALSE;
    rpData->blMapStatFtyp = FALSE;
    rpData->pclIntFileName[0] = '\0';
    rpData->pclFieldType[0] = '\0';
    rpData->pclSeqKey[0] = '\0';
    rpData->pclDssnKey[0] = '\0';
} 

static int ValidDT( char *pcpDateTimeStr )
{
    int ilLen;
    int ili;

    ilLen = strlen(pcpDateTimeStr);
    if( ilLen == 0 ) /* meaning no date time data, so is ok */
        return TRUE;
    if( ilLen != 14 )
        return FALSE;
    for( ili = 0; ili < 14; ili++ )
    {
        if( !isdigit(pcpDateTimeStr[ili]) )
            return FALSE;
    }
    return TRUE;
}

static int Init_AFTFields()
{
    int ilRc;
    int ili;

    for( ili = 0; ili < rgDATA.ilNumAftFields; ili++ )
    {
        get_real_item( rgAFT[ili].pclName, rgDATA.pclAftFields, ili+1 );
        rgAFT[ili].pclValue[0] = '\0';
    }
    dbg( TRACE, "%d AFT NODES INITIALISE", ili );
    return ilRc;
}

static int Reset_AFTFields()
{
    int ilRc;
    int ili;

    for( ili = 0; ili < rgDATA.ilNumAftFields; ili++ )
    {
        rgAFT[ili].pclValue[0] = '\0';
    }
    dbg( DEBUG, "%d AFT VALUES RESET", ili );
    return ilRc;
}

static int Upd_AFTField( char *pcpFieldName, char *pcpFieldValue )
{
    int ilRc;
    int ili;
    char *pclFunc = "Upd_AFTValue";

    for( ili = 0; ili < rgDATA.ilNumAftFields; ili++ )
    {
        if( !strcmp( rgAFT[ili].pclName, pcpFieldName ) )
        {
            strcpy( rgAFT[ili].pclValue, pcpFieldValue );
            dbg( DEBUG, "%s: AFT Field <%s> Value <%s>", pclFunc, pcpFieldName, pcpFieldValue );
            break;
        }
    }
    return ilRc;
}

static int Get_AFTValue( char *pcpFieldName, char *pcpFieldValue )
{
    int ilRc;
    int ili;
    char *pclFunc = "Get_AFTValue";

    for( ili = 0; ili < rgDATA.ilNumAftFields; ili++ )
    {
        if( !strcmp( rgAFT[ili].pclName, pcpFieldName ) )
        {
            strcpy( pcpFieldValue, rgAFT[ili].pclValue );
            dbg( DEBUG, "%s: AFT Field <%s> Value <%s>", pclFunc, pcpFieldName, pcpFieldValue );
            return RC_SUCCESS;
        }
    }
    return RC_FAIL;
}

static int ProcessFlight(char *pcpDQCS)
{
    int ilRc;
    int ili;
    int ilLen;
    int ilFltnLen;
    char pclSqlBuf[1024] = "\0";
    char pclSqlData[1024] = "\0";
    char pclAftFields[1024] = "\0";
    char pclAftData[2048] = "\0";
    char pclTmpStr[1024] = "\0";
    char pclUrno[20] = "\0";
    char pclAlc2[8] = "\0";
    char pclFlno[12] = "\0";
    char pclAdid[8] = "\0";
    char pclOrg3[8] = "\0";
    char pclDes3[8] = "\0";
    char pclFltn[20] = "\0";
    char pclOneField[20] = "\0";
    char pclOneData[1028] = "\0";
    char pclStod[20] = "\0";
    char pclStoa[20] = "\0";
    char pclCmd[8] = "\0";
    char pclFlns[4] = "\0";
    char *pclFunc = "ProcessFlight";

    ilRc = Get_AFTValue( "ALC2", pclAlc2 ); if( ilRc != RC_SUCCESS ) return ilRc;
    ilRc = Get_AFTValue( "ADID", pclAdid ); if( ilRc != RC_SUCCESS ) return ilRc;
    ilRc = Get_AFTValue( "FLNO", pclFlno ); 
    ilRc = Get_AFTValue( "FLTN", pclFltn ); 
    ilRc = Get_AFTValue( "FLNS", pclFlns ); 
    ilFltnLen = strlen(pclFltn);
    if( ilFltnLen > 0 && strlen(pclFlns) <= 0 )
    {
        if( !isdigit(pclFltn[ilFltnLen-1]) )
        {
            sprintf( pclFlns, "%c", pclFltn[ilFltnLen-1] );
            memcpy( pclTmpStr, pclFltn, ilFltnLen-1 );
            Upd_AFTField( "FLTN", pclTmpStr );
            strcpy( pclFltn, pclTmpStr );
        }
        else
            strcpy( pclFlns, " " );
        Upd_AFTField( "FLNS", pclFlns );
    }
    if( strlen(pclFlno) <= 0 ) 
    {
        if( ilFltnLen <= 0 ) 
            return ilRc;
        sprintf( pclFlno, "%-3.3s%3.3d%c", pclAlc2, atoi(pclFltn), pclFlns[0] );
        Upd_AFTField( "FLNO", pclFlno );
    }
    strcpy( pclTmpStr, " " );
    if( rgDATA.ilNumAftUpdOnlyIfBlankFields > 0 )
    {
        sprintf( pclTmpStr, ",%s", rgDATA.pclAftUpdOnlyIfBlankFields );
        
    }
    
    //UFIS-2209
    strcat(pclTmpStr,",DQCS");
    
    sprintf( pclSqlBuf, "SELECT URNO%s FROM AFTTAB WHERE "
                        "FLNO = '%s' AND FTYP NOT IN ('T','G') ", pclTmpStr, pclFlno );
    if( pclAdid[0] == 'D' )
    {
        ilRc = Get_AFTValue( "DES3", pclDes3 ); if( ilRc != RC_SUCCESS ) return ilRc;
        ilRc = Get_AFTValue( "STOD", pclStod ); if( ilRc != RC_SUCCESS ) return ilRc;
        sprintf( pclTmpStr, "AND ORG3 = '%s' AND STOD like '%8.8s%%'", cgHopo, pclStod );
    }
    else
    {
        ilRc = Get_AFTValue( "ORG3", pclOrg3 ); if( ilRc != RC_SUCCESS ) return ilRc;
        ilRc = Get_AFTValue( "STOA", pclStoa ); if( ilRc != RC_SUCCESS ) return ilRc;
        sprintf( pclTmpStr, "AND DES3 = '%s' AND STOA like '%8.8s%%'", cgHopo, pclStoa );
    }
    strcat( pclSqlBuf, pclTmpStr );
    ilRc = RunSQL( pclSqlBuf, pclSqlData );
    dbg( DEBUG, "%s ilRc = <%d>",pclFunc,ilRc );

    memset( pclAftFields, 0, sizeof(pclAftFields) );
    memset( pclAftData, 0, sizeof(pclAftData) );
    
    if( ilRc != RC_SUCCESS )
    {
        if( rgDATA.blProcessFltInsert == FALSE )
        {
            dbg( DEBUG, "%s", "INSERT OFF" );
            return RC_NOT_FOUND;
        }
        strcpy( pclCmd, "IFR" );
        GetNextValues( pclUrno, 1 );
        Upd_AFTField( "URNO", pclUrno );
        for( ili = 0; ili < rgDATA.ilNumAftFields; ili++ )
        {
            get_real_item( pclOneField, rgDATA.pclAftFields, ili+1 );
            ilRc = Get_AFTValue( pclOneField, pclTmpStr );
            if( ilRc == RC_SUCCESS )
            {
                if( !strcmp(pclOneField, "FTYP") && strlen(pclTmpStr) <= 0 )
                    strcpy( pclTmpStr, "O" );
                strcat( pclAftFields, pclOneField );
                strcat( pclAftFields, "," );
                strcat( pclAftData, pclTmpStr );
                strcat( pclAftData, "," );
            }
        }
    }
    else
    {
        strcpy( pclCmd, "UFR" );
        get_fld( pclSqlData,FIELD_1,STR,20,pclUrno ); 
        Upd_AFTField( "URNO", pclUrno );
        for( ili = 0; ili < rgDATA.ilNumAftUpdFields; ili++ )
        {
            get_real_item( pclOneField, rgDATA.pclAftUpdFields, ili+1 );
            ilRc = Get_AFTValue( pclOneField, pclTmpStr );
            if( ilRc == RC_SUCCESS )
            {
                if( !strcmp(pclOneField, "ACT3") && bgRegnAvail == TRUE )
                    continue;
                if( rgDATA.blIgnoreEmptyFld == TRUE && strlen(pclTmpStr) <= 0 )
                    continue;
                if( !strcmp(pclOneField, "FTYP") && pclAdid[0] == 'D' && 
                    strstr(rgDATA.pclDepIgnoreFtypRecv,pclTmpStr) != NULL )
                    continue;
                strcat( pclAftFields, pclOneField );
                strcat( pclAftFields, "," );
                strcat( pclAftData, pclTmpStr );
                strcat( pclAftData, "," );
            }
        }
        for( ili = 0; ili < rgDATA.ilNumAftUpdOnlyIfBlankFields; ili++ )
        {
            get_real_item( pclOneField, rgDATA.pclAftUpdOnlyIfBlankFields, ili+1 );
            get_fld( pclSqlData,FIELD_1+ili+1,STR,1024,pclOneData ); TrimSpace( pclOneData );
            dbg( DEBUG, "EXISTING AFT data FIELD <%s> DATA <%s>", pclOneField, pclOneData );
            if( strlen(pclOneData) > 0 )
                continue;
            ilRc = Get_AFTValue( pclOneField, pclTmpStr );
            if( ilRc == RC_SUCCESS )
            {
                if( !strcmp(pclOneField, "ACT3") && bgRegnAvail == TRUE )
                    continue;
                if( rgDATA.blIgnoreEmptyFld == TRUE && strlen(pclTmpStr) <= 0 )
                    continue;
                if( !strcmp(pclOneField, "FTYP") && pclAdid[0] == 'D' && 
                    strstr(rgDATA.pclDepIgnoreFtypRecv,pclTmpStr) != NULL )
                    continue;
                strcat( pclAftFields, pclOneField );
                strcat( pclAftFields, "," );
                strcat( pclAftData, pclTmpStr );
                strcat( pclAftData, "," );
            }
        }
        //UFIS-2099
        memset(pclOneData,0,sizeof(pclOneData));
        get_fld( pclSqlData,FIELD_1+rgDATA.ilNumAftUpdOnlyIfBlankFields+1,STR,1024,pclOneData ); 
        TrimSpace( pclOneData );
    }
    ilLen = strlen(pclAftFields); pclAftFields[ilLen-1] = '\0';
    ilLen = strlen(pclAftData); pclAftData[ilLen-1] = '\0';
    dbg( DEBUG, "To be sent to FLIGHT\n CMD <%s> \n FIELDS <%s> \n DATA <%s>\n",
                pclCmd, pclAftFields, pclAftData );
    
    sprintf( pclTmpStr, "WHERE URNO = %s", pclUrno );
    SendCedaEvent( igFlight, 0, mod_name, " ",  " ", " ", pclCmd, "AFTTAB", 
                   pclTmpStr, pclAftFields, pclAftData, " ", 3, NETOUT_NO_ACK );
               
    //Frank UFIS-2209
    memset(pcpDQCS,0,sizeof(pcpDQCS));
    strcpy(pcpDQCS,pclOneData);
    TrimSpace(pcpDQCS);
    dbg(TRACE,"%s pcpDQCS<%s>",pclFunc,pcpDQCS);
    
    return RC_SUCCESS;
} 

static int ProcessLoad( int *pipBook, int *pipConfig, int *pipActual, int *pipLoad )
{
    int ilRc;
    char pclFields[512] = "\0";
    char pclData[512] = "\0";
    char pclFlnu[20] = "\0";
    char pclSqlBuf[1024] = "\0";
    char pclSqlData[1024] = "\0";

    ilRc = Get_AFTValue( "URNO", pclFlnu ); 
    if( ilRc != RC_SUCCESS || atoi(pclFlnu) <= 0 ) 
        return RC_FAIL;

    sprintf( pclSqlBuf, "DELETE FROM LOATAB WHERE DSSN = '%s' AND FLNU = %s", rgDATA.pclDssnKey, pclFlnu );
    ilRc = RunSQL( pclSqlBuf, pclSqlData );
    dbg( DEBUG, "ilRc = <%d>", ilRc );
    InsertLoad( pclFlnu, "PXB", "F", " ", " ", pipBook[F] );
    InsertLoad( pclFlnu, "PXB", "B", " ", " ", pipBook[J] );
    InsertLoad( pclFlnu, "PXB", "E", " ", " ", pipBook[Y] );
    /* fake one */
    if( pipBook[F] >= 0 || pipBook[J] >= 0 || pipBook[Y] >= 0 )
    {
        InsertLoad( pclFlnu, "PXB", "2", " ", " ", 0 );
        InsertLoad( pclFlnu, "PXB", "T", " ", "I", 0 );
    }

    InsertLoad( pclFlnu, "PCF", "F", " ", " ", pipConfig[F] );
    InsertLoad( pclFlnu, "PCF", "B", " ", " ", pipConfig[J] );
    InsertLoad( pclFlnu, "PCF", "E", " ", " ", pipConfig[Y] );

    InsertLoad( pclFlnu, "PAX", "F", " ", " ", pipActual[F] );
    InsertLoad( pclFlnu, "PAX", "B", " ", " ", pipActual[J] );
    InsertLoad( pclFlnu, "PAX", "E", " ", " ", pipActual[Y] );

    InsertLoad( pclFlnu, "LOA", "B", " ", " ", pipLoad[BAG] );
    InsertLoad( pclFlnu, "LOA", "C", " ", " ", pipLoad[CARGO] );
    InsertLoad( pclFlnu, "LOA", "M", " ", " ", pipLoad[MAIL] );

    if( igLoahdl != RC_FAIL )
    {
        strcpy( pclFields, "DSSN,FLNU" );
        sprintf( pclData, "%s,%s", rgDATA.pclDssnKey, pclFlnu );
        SendCedaEvent( igLoahdl, 0, mod_name, " ",  " ", " ", "LOA", "LOATAB", 
                       " ", pclFields, pclData, " ", 3, NETOUT_NO_ACK );
        dbg( TRACE, "Send to LOAHDL FLD <%s> DATA <%s>", pclFields, pclData );
    }
}

static int InsertLoad( char *pcpFlnu, char *pcpType, char *pcpStyp, 
                       char *pcpSstp, char *pcpSsst, int ipValue )
{
    int ilRc;
    char pclUrno[20] = "\0";
    char pclValue[32] = "\0";
    char pclSqlBuf[1024] = "\0";
    char pclSqlData[1024] = "\0";
    char pclLoaFields[1024] = "\0";
    char pclLoaValues[1024] = "\0";

    /*strcpy( pclValue, " " );
    if( ipValue != -1 )
        sprintf( pclValue, "%d", ipValue );*/
    if( ipValue == -1 )
        return RC_SUCCESS;
    sprintf( pclValue, "%d", ipValue );
    GetNextValues( pclUrno, 1 );
    strcpy( pclLoaFields, "URNO,FLNU,DSSN,HOPO,TYPE,STYP,SSTP,SSST,VALU" );
    sprintf( pclLoaValues, "%s,%s,'%s','%s','%s','%s','%s','%s','%s'",
                          pclUrno, pcpFlnu, rgDATA.pclDssnKey, cgHopo, 
                          pcpType, pcpStyp, pcpSstp, pcpSsst, pclValue );
    sprintf( pclSqlBuf, "INSERT INTO LOATAB (%s) VALUES (%s)", pclLoaFields, pclLoaValues );
    ilRc = RunSQL( pclSqlBuf, pclSqlData );
    if( ilRc != DB_SUCCESS )
    {
        dbg( DEBUG, "Insert Fail!" );
        return RC_FAIL;
    }
    return RC_SUCCESS;
}

int TrimSpace( char *pcpInStr )
{
    int ili = 0;
    int ilLen;
    char *pclOutStr = NULL;
    char *pclP;
    char *pclFunc = "TrimSpace";

    ilLen = strlen( pcpInStr );
    if( ilLen <= 0 )
        return;

    pclOutStr = (char *)malloc(ilLen + 10);
    pclP = pcpInStr;

    /* Trim front spaces */
    while( pclP && *pclP == ' ' )
        pclP++;

    while( *pclP )
    {
       pclOutStr[ili++] = *pclP;
       if( *pclP != ' ' )
           pclP++;
       else
       {
           while( *pclP == ' ' )
               pclP++;
       }
    }
    /* Trim back space */
    if( pclOutStr[ili-1] == ' ' )
        ili--;
    pclOutStr[ili] = '\0';
    strcpy( pcpInStr, pclOutStr );
    if( pclOutStr != NULL )
        free( (char *)pclOutStr );
}

int ChkGCFTABToDetermineFreeze(void)
{
	int ilRc;
	char pclValueFromGCFTAB[16] = "\0";
	char *pclFunc = "ChkGCFTABToDetermineFreeze";
	char pclSqlData[128] = "\0";
  char pclSqlBuf[1024] = "\0";
  
  sprintf( pclSqlBuf, "SELECT VALU FROM GCFTAB WHERE APNA='GLOBAL' AND SECT='DQC' AND PARA='FREEZE' AND USEC='UFIS$ADMIN' AND HOPO='AUH'");
  
  dbg( TRACE, "%s %s",pclFunc,pclSqlBuf );
  
  ilRc = RunSQL(pclSqlBuf, pclSqlData);
  
  if( ilRc != DB_SUCCESS )
  {
      dbg( TRACE, "%s CANNOT FIND URNO and VALU IN GCFTAB",pclFunc );
      return RC_FAIL;
  }
  
  get_fld( pclSqlData,FIELD_1,STR,20,pclValueFromGCFTAB );
  TrimSpace(pclValueFromGCFTAB);
	
	dbg(TRACE,"pclValueFromGCFTAB<%s>",pclValueFromGCFTAB);
	
	if( strncmp(pclValueFromGCFTAB,"Y",1) == 0 || strncmp(pclValueFromGCFTAB,"y",1) == 0)
	{
		igValueFromGCFTABForFreeze = 1;
	}
	else
	{
		igValueFromGCFTABForFreeze = 0;
	}
	dbg(TRACE,"igValueFromGCFTABForFreeze<%d>",igValueFromGCFTABForFreeze);
	
	return ilRc;    	
}
/*
int ChkAFTTAB_DQCSToDetermineUpdate(char *pcpUrnoForDQCSChk)
{
	int ilRc;
	char pclDQCSFromAFTTAB[16] = "\0";
	char *pclFunc = "ChkAFTTAB_DQCSToDetermineUpdate";
	char pclSqlData[128] = "\0";
  char pclSqlBuf[1024] = "\0";
  
  if ( pcpUrnoForDQCSChk[0] == '\0' )
  {
    dbg (TRACE, "%s URNO empty - doing nothing", pclFunc);
    return;
  }
  
  sprintf( pclSqlBuf, "SELECT DQCS FROM AFTTAB WHERE URNO='%s'",pcpUrnoForDQCSChk);
  
  dbg( TRACE, "%s %s",pclFunc,pclSqlBuf );
  
  ilRc = RunSQL( pclSqlBuf, pclSqlData );
  
  if( ilRc != DB_SUCCESS )
  {
      dbg( TRACE, "%s CANNOT FIND DQCS IN AFTTAB",pclFunc );
      return RC_FAIL;
  }
  
  get_fld( pclSqlData,FIELD_1,STR,20,pclDQCSFromAFTTAB );
  TrimSpace(pclDQCSFromAFTTAB);
  
	//dbg(TRACE,"pclDQCSFromAFTTAB<%s>",pclDQCSFromAFTTAB);
	
	//to be modified
	if( strncmp(pclDQCSFromAFTTAB,"LC",2) == 0 || strncmp(pclDQCSFromAFTTAB,"C",1) == 0 || strncmp(pclDQCSFromAFTTAB,"S",1) == 0 )
	{
		dbg( TRACE, "%s pclDQCSFromAFTTAB<%s> is LC, C or S->do not insert LOATAB",pclFunc,pclDQCSFromAFTTAB);
		return RC_FAIL;
	}
	else
	{
		dbg( TRACE, "%s pclDQCSFromAFTTAB<%s> is not LC, C or S->insert LOATAB",pclFunc,pclDQCSFromAFTTAB);
		return RC_SUCCESS;
	}   	
}
//Frank UFIS-2209
*/

