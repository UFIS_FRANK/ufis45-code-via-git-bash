#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Interface/pophdl.c 1.02 2012/12/20 18:43:14ICT won Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* UFIS POP3 handler (client)                                                 */
/*                                                                            */
/* Author         : C. Stybert                                                */
/* Date           : 27 Oct 2010                                               */
/* Description    : This handler provides a client interface to a POP3 server */
/*                  It forwards the mail bodies to fdihdl for parsing         */
/*                  The mail check is triggered by ntisch                     */
/*                                                                            */
/* Update history :                                                           */
/* 20030630 JIM: may core!: dbg(TRACE,"Command: <%s>,prlCmdblk->command")     */
/* 20121219: WON: Extended variable size from 2048 to 32768 to avoid crash on */
/*                long emails                                                 */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_version[] ="@(#) UFIS45 (c) UFIS pophdl.c 0.02 / 20.12.2012 CST";
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <cedatime.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include "helpful.h"
#include "libspopc.h"



#ifdef _HPUX_SOURCE
    extern int daylight;
    extern long timezone;
    extern char *tzname[2];
#else
    extern int _daylight;
    extern long _timezone;
    extern char *_tzname[2];
#endif

int debug_level = 0;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;

static char cgProcessName[20] = "\0";
static char  cgConfigFile[512] = "\0";
static char  cgHopo[8] = "\0";                    /* default home airport    */
static char  cgTabEnd[8] ="\0";                   /* default table extension */
static char cgMyservername[64];
static char cgUsername[64];
static char cgPassword[64];
static popsession* gMySession;
static char    pcgTwStart[128] = "";
static char    pcgTwEnd[128] = "";
static char    pcgTabEnd[128] = "";
static char cgSendCommand[32] = "";
static int    igModID_fdihdl;
static int  igPriority = 3;
static char cgTelexType[32] = "";




static long lgEvtCnt = 0;

/******************************************************************************/
/* Function prototypes                                                          */
/******************************************************************************/
int  Init_Process();
int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
               char *pcpCfgBuffer);


int        Reset(void);                       /* Reset program          */
void    Terminate(int ipSleep);            /* Terminate program      */
void    HandleSignal(int);                 /* Handles signals        */
void    HandleErr(int);                    /* Handles general errors */
void    HandleQueErr(int);                 /* Handles queuing errors */
void    HandleQueues(void);                /* Waiting for Sts.-switch*/
int  FindTextBlocks(char *pcpRawMail, char **pcpPointers);
int  SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
             char *pcpTwStart, char *pcpTwEnd,
             char *pcpSelection,char *pcpFields,char *pcpData,
             char *pcpAddStruct,int ipAddStructSize);
int generateTelex12Prefix(char *strHeader, char *strMsg);
int passout();

/* External functions */

extern int    SetSignals(void (*HandleSignal)(int));


int    HandleData(EVENT *prpEvent);       /* Handles event data     */
int  GetCommand(char *pcpCommand, int *pipCmd);

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int    ilRc = RC_SUCCESS;            /* Return code            */
    int    ilCnt = 0;
    int ilOldDebugLevel = 0;


    INITIALIZE;            /* General initialization    */

    debug_level = TRACE;
    SetSignals(HandleSignal);

    strcpy(cgProcessName,argv[0]);

    dbg(TRACE,"MAIN: version <%s>",mks_version);

    /* Attach to the queues */
    do
    {
        ilRc = init_que();
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */

    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
            ilRc = Init_Process();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"Init_Process: init failed!");
            } /* end of if */

    } else {
        Terminate(30);
    }/* end of if */



    dbg(TRACE,"=====================");
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"=====================");
    

    for(;;)
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* Acknowledge the item */
        ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
        if( ilRc != RC_SUCCESS ) 
        {
            /* handle que_ack error */
            HandleQueErr(ilRc);
        } /* fi */

        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */

        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRc == RC_SUCCESS )
        {
            
            lgEvtCnt++;

            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                /* CloseConnection(); */
                HandleQueues();
                break;    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                break;    
            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;
            case    SHUTDOWN    :
                /* process shutdown - maybe from uutil */
                Terminate(1);
                break;
                    
            case    RESET        :
                ilRc = Reset();
                break;
                    
            case    EVENT_DATA    :
                if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                {
                    ilRc = HandleData(prgEvent);
                    if(ilRc != RC_SUCCESS)
                    {
                        HandleErr(ilRc);
                    }/* end of if */
                }else{
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
                

            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            case    12345:
                passout();
                break;
            default            :
                dbg(TRACE,"MAIN: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */

        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
        
    } /* end for */
    
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

int Init_Process()
{
    int      ilRc = RC_SUCCESS;            /* Return code */
    char    pclDebugLevel[128];
    char    clModID[16] = "";

    debug_level = TRACE;
    
    /* Set the configuration file name */
    
    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"Config File is <%s>",cgConfigFile);
    
    ilRc = iGetConfigEntry(cgConfigFile,"MAIN","DEBUG_LEVEL",CFG_STRING,pclDebugLevel);
    if (strcmp(pclDebugLevel,"DEBUG") == 0)
    {
        debug_level = DEBUG;
    }
    else
    {
        if (strcmp(pclDebugLevel,"TRACE") == 0)
        {
            debug_level = TRACE;
        }
        else
        {
            if (strcmp(pclDebugLevel,"NULL") == 0)
            {
                debug_level = 0;
            }
            else
            {
                debug_level = TRACE;
            }
        }
    }

    /* Get the mail configuration */
    
    ilRc = iGetConfigEntry(cgConfigFile,"MAIL","MAIL_SERVER",CFG_STRING,cgMyservername);
    
    if (ilRc == RC_SUCCESS) {
        ilRc = iGetConfigEntry(cgConfigFile,"MAIL","MAIL_USER",CFG_STRING,cgUsername);
        
        if (ilRc == RC_SUCCESS) {
            ilRc = iGetConfigEntry(cgConfigFile,"MAIL","MAIL_PASSWD",CFG_STRING,cgPassword);
            
            if (ilRc == RC_SUCCESS) {
                dbg(TRACE,"The following mail configuration was found:");
                dbg(TRACE,"Mail server: <%s>",cgMyservername);
                dbg(TRACE,"Mail user  : <%s>",cgUsername);
                dbg(TRACE,"Mail Pass  : <%s>",cgPassword);
                
                ilRc = iGetConfigEntry(cgConfigFile,"MAIL","MODID",CFG_STRING,clModID);            
                igModID_fdihdl = atoi(clModID);
                iGetConfigEntry(cgConfigFile,"MAIL","TWS",CFG_STRING,pcgTwStart);
                iGetConfigEntry(cgConfigFile,"MAIL","TWE",CFG_STRING,pcgTwEnd);
            }
            if (ilRc == RC_SUCCESS) {
                iGetConfigEntry(cgConfigFile,"MAIL","SEND_CMD",CFG_STRING,cgSendCommand);
                iGetConfigEntry(cgConfigFile,"MAIL","TELEX_TYPE",CFG_STRING,cgTelexType);
                dbg(TRACE,"The following telex configuration was found:");
                dbg(TRACE,"Command to FDIHDL: <%s>",cgSendCommand);
                dbg(TRACE,"Telex Type       : <%s>",cgTelexType);
            }

        }
    }
    
    if (ilRc != RC_SUCCESS) {
        dbg(TRACE,"Reading mail configuration failed");
        return ilRc;
    }
        
    /* Initialize the mail access environment */
    
    libspopc_init();
    
    if (ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"Init_Process failed");
    }

        return(ilRc);
    
} /* end of Init_Process */


/******************************************************************************/
/* The configuration access routine                                                 */
/******************************************************************************/


int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
               CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
    dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
    dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
        clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}


/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/

int Reset()
{
    int    ilRc = RC_SUCCESS;                /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    /* Cleanup the mail access environment */
    
    libspopc_clean();

    /* Re-initialize the handler */
    
    Init_Process();
    
    return ilRc;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
void Terminate(int ipSleep)
{
    /* unset SIGCHLD ! DB-Child will terminate ! */

    signal(SIGCHLD,SIG_IGN);
    
    /* Cleanup the mail access environment */
    
    libspopc_clean();

    dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


    fclose(outp);

    sleep(ipSleep < 1 ? 1 : ipSleep);

    dbg(TRACE,"Terminate: now leaving ...");
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

void HandleSignal(int pipSig)
{
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

    switch(pipSig)
    {
    default    :
        Terminate(1);
        break;
    } /* end of switch */

    exit(1);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
void HandleErr(int pipErr)
{
    dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
void HandleQueErr(int pipErr)
{
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET    :    /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
void HandleQueues()
{
    int    ilRc = RC_SUCCESS;            /* Return code */
    int    ilBreakOut = FALSE;
    
    do
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;    

        if( ilRc == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRc != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRc);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;    

            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;    
    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                ilBreakOut = TRUE;
                break;    

            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;

            case    SHUTDOWN    :
                Terminate(1);
                break;
                        
            case    RESET        :
                ilRc = Reset();
                break;
                        
            case    EVENT_DATA    :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;

            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;

            default            :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
    } while (ilBreakOut == FALSE);

            ilRc = Init_Process();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"pophdl: init failed!");
            } 
     

} /* end of HandleQueues */
    



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
int HandleData(EVENT *prpEvent)
{
        int         ilRc                 = RC_SUCCESS;                  /* Return code */
        int     ilCmd            = 0;

        /* Variables for the POP3 access library */

        char      *pclMailError                  = NULL;
        int                     ilLast                                   = 0;
        int                     ilCnt                                            = 0;
        int                     ilSize                                   = 0;
        char            *pclMsg                                  = 0;

        BC_HEAD *prlBchead       = NULL;
        CMDBLK  *prlCmdblk       = NULL;
        char    *pclSelection    = NULL;
        char    *pclFields       = NULL;
        char    *pclData         = NULL;
        char    *pclRow          = NULL;
        char            clTable[34];
        char            *pclTextBlocks[16];
        char strPrefix[32728];

        prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
        prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
        pclSelection = prlCmdblk->data;
        pclFields    = pclSelection + strlen(pclSelection) + 1;
        pclData      = pclFields + strlen(pclFields) + 1;


        strcpy(clTable,prlCmdblk->obj_name);

        dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);
        /****************************************/
        DebugPrintBchead(TRACE,prlBchead);
        DebugPrintCmdblk(TRACE,prlCmdblk);
        /* 20030630 JIM: may core!: dbg(TRACE,"Command: <%s>,prlCmdblk->command"); */
        dbg(TRACE,"Command: <%s>",prlCmdblk->command);
        dbg(TRACE,"originator follows event = %p ",prpEvent);

        dbg(TRACE,"originator<%d>",prpEvent->originator);
        dbg(TRACE,"selection follows Selection = %p ",pclSelection);
        dbg(TRACE,"selection <%s>",pclSelection);
        dbg(TRACE,"fields    <%s>",pclFields);
        dbg(TRACE,"data      <%s>",pclData);
        /****************************************/



        dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);
    
        /* Check if is is a proper event (a MAIL command) */

        if (strcmp(prlCmdblk->command, "MAIL") == 0)
        {
            char *strPtr;
            
            dbg(TRACE, "---------- HandleData:: Mail Start ----------");
            /* Connect to the mail server */
            strPtr=cgPassword;
            dbg(TRACE,"User[%s] Passwd[%s]\n", cgUsername, cgPassword);
            CDecode(&strPtr);
        
            dbg(TRACE,"New User[%s] Passwd[%s], New[%s]\n", cgUsername, cgPassword, strPtr);
            
            pclMailError = popbegin(cgMyservername,cgUsername,strPtr,&gMySession);
            if (pclMailError != NULL)
            {
                /* Error connecting to the mail server */
                dbg(TRACE," Mail server connect failed. Error code: <%s>",pclMailError);
                ilRc = RC_FAIL;
            }
            else
            {
                dbg(TRACE," Mail server connected.  Now Retrieving Email");
            
                /* Connect was successful. The mail information is stored in gMySession */
                /* Now loop through the messages and forward them to fdihdl */
                ilLast = gMySession->last;

                /* Make sure that messages are not deleted automatically when read */
                popsetundel(gMySession);

                for (ilCnt = 1 ; ilCnt <= ilLast ; ilCnt++)
                {
                	memset(&strPrefix, 0, sizeof(strPrefix));
                    ilSize = popmsgsize(gMySession,ilCnt);
                    pclMsg = popgetmsg(gMySession,ilCnt);

                    /* Send the message to fdihdl */
                    if (pclMsg != NULL)
                    {
                        dbg(DEBUG,"Message received: <%s>",pclMsg);
                        ilRc = FindTextBlocks(pclMsg,pclTextBlocks);
                        if (ilRc > 0)
                        {
                            dbg(TRACE,"First text block <%s>",pclTextBlocks[ilRc]);
                        }

                        if ( !strcmp(cgTelexType, "TELEX,12") )
                        {
                            generateTelex12Prefix(strPrefix, pclMsg);
                            strcat(strPrefix, pclTextBlocks[1]);
                        }

                        dbg(TRACE,"Sending email no. [%d] to FDIHDL \n<%s>",ilCnt,strPrefix);
                        ilRc = SendEvent(cgSendCommand,igModID_fdihdl,igPriority,"",pcgTwStart,pcgTwEnd,
                                         cgTelexType,"",strPrefix,"",0);

                        if (ilRc != RC_SUCCESS)
                        {
                             /* We were unable to queue the mail. Close the connection and report the error */
                             dbg(TRACE,"Queuing error. Closing the connection to the Mail server: <%i>", ilRc);

                             popend(gMySession);
                             return RC_FAIL;
                        } /* end if */

                        /* Cleanup before getting the next message */
                        popdelmsg(gMySession,ilCnt);
                        free(pclMsg);
                        pclMsg = NULL;
                    } /* end if */
                    else
                    {
                        dbg(TRACE,"Error retrieving message <%i> out of <%i>", ilCnt,ilLast);
                        return RC_FAIL;
                    } /* end if */
                } /* end for */
                        /* Close the mailserver session */
                popend(gMySession);
            } /* end else */
            dbg(TRACE, "---------- HandleData:: Mail End ----------");
        } /* end if */
        else
        {
            dbg(TRACE,"Invalid command: <%s>",prlCmdblk->command);
            ilRc = RC_FAIL;
        } /* end else */

        return(ilRc);
} /* end of HandleData */


/*********************************************************************
Function : SendEvent()
Paramter : IN: pcpCmd = command for cmdblk->command
           IN: ipModID = process-ID where the event is send to
       IN: ipPriority = priority for sending ( 1- 5, usuallay 3)
       IN: pcpTable = Name (3 letters) of cmdblk->obj_name (if
           necessary), will be expanded with "pcgTabEnd".
       IN: pcpTwStart = cmdblk->twstart
       IN: pcpTwEnd = cmdblk->twend (always HOMEAP,TABEND,processname)
       IN: pcpSelection = selection for event (cmdblk->data)
       IN: pcpFields = fieldlist (corresponding to pcpdata)
       IN: pcpData = datalist (comma separated, corresponding to 
                                   pcpFields)
       IN: pcpAddStruct = additional structure to be transmitted
       IN: ipAddStructSize = size of the additional structure
Return Code: RC_SUCCESS, RC_FAIL
Result:
Description: Sends an event to another CEDA-process using que(QUE_PUT).
             Sends the event in standard CEDA-format (BCHEAD,CMDBLK,
         selection,fieldlist,datalist) or sends a different
         data structure (special) at CMDBLK->data. !! Sends always
         only one type, standard OR special, with one event !!
*********************************************************************/

int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
             char *pcpTwStart, char *pcpTwEnd,
             char *pcpSelection,char *pcpFields,char *pcpData,
             char *pcpAddStruct,int ipAddStructSize)
{
  int     ilRc             = RC_FAIL;
  int     ilLen            = 0;
  EVENT   *prlOutEvent  = NULL;
  BC_HEAD *prlOutBCHead = NULL;
  CMDBLK  *prlOutCmdblk = NULL;

  if (pcpAddStruct == NULL)
    ipAddStructSize = 0;
  if (ipAddStructSize == 0)
    pcpAddStruct = NULL;

  /* size-calculation for prlOutEvent */
  ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
    strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 
    ipAddStructSize + 128;

  /* memory for prlOutEvent */
  if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
    {
      dbg(TRACE,"SendEvent: cannot malloc <%d>-bytes for outgoing event!",ilLen);
      prlOutEvent = NULL;
    }else{
      /* clear whole outgoing event */
      memset((void*)prlOutEvent, 0x00, ilLen);

      /* set event structure... */
      prlOutEvent->type         = SYS_EVENT;
      prlOutEvent->command        = EVENT_DATA;
      prlOutEvent->originator   = (short)mod_id;
      prlOutEvent->retry_count  = 0;
      prlOutEvent->data_offset  = sizeof(EVENT);
      prlOutEvent->data_length  = ilLen - sizeof(EVENT); 

      /* BC_HEAD-Structure... */
      prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
      /* prlOutBCHead->rc = (short)RC_SUCCESS;*/
      prlOutBCHead->rc = (short)NETOUT_NO_ACK;/*spaeter nur bei disconnect*/
      strncpy(prlOutBCHead->dest_name,mod_name,10);
      strncpy(prlOutBCHead->recv_name, "EXCO",10);
 
      /* Cmdblk-Structure... */
      prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
      strcpy(prlOutCmdblk->command,pcpCmd);
      if (pcpTable != NULL)
    {
      strcpy(prlOutCmdblk->obj_name,pcpTable);
      strcat(prlOutCmdblk->obj_name,pcgTabEnd);
    }
        
      /* setting tw_x entries */
      strncpy(prlOutCmdblk->tw_start,pcpTwStart,32);
      strncpy(prlOutCmdblk->tw_end,pcpTwEnd,32);
        
      /* means that no additional structure is used */
      /* STANDARD CEDA-ipcs between CEDA-processes */
      if (pcpAddStruct == NULL)
    {
      /* setting selection inside event */
      strcpy(prlOutCmdblk->data,pcpSelection);
      /* setting field-list inside event */
      strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1,pcpFields);
      /* setting data-list inside event */
      strcpy((prlOutCmdblk->data + (strlen(pcpSelection)+1) + (strlen(pcpFields)+1)),pcpData);
    }else{
      /*an additional structure is used and will be copied to */
      /*cmdblk + sizeof(CMDBLK).!!! No STANDARD CEDA-ipcs is used !!!! */
      memcpy(prlOutCmdblk->data,(char*)pcpAddStruct,ipAddStructSize);    
    }

      /*DebugPrintEvent(DEBUG,prlOutEvent);*/ 
      /*snapit((char*)prlOutEvent,ilLen,outp);*/
      dbg(DEBUG,"SendEvent: sending event to mod_id <%d>",ipModID);

      if (ipModID != 0)
    {
      if ((ilRc = que(QUE_PUT,ipModID,mod_id,ipPriority,ilLen,(char*)prlOutEvent))
          != RC_SUCCESS)
        {
          dbg(TRACE,"SendEvent: QUE_PUT returns: <%d>", ilRc);
          Terminate(1);
        }
    }else{
      dbg(TRACE,"SendEvent: mod_id = <%d>! Can't send!",ipModID);
    }
      /* free memory */
      free((void*)prlOutEvent); 
    }
  return ilRc;
} /* end of SendEvent() */

int FindTextBlocks(char *pcpRawMail, char **pcpPointers)
{
    /* This routine locates all the text/plain blocks */
    /* It splits the Raw mail into blocks (by adding a NULL character */
    /* and sets the pointers to the beginning of the blocks in pcpPointers */
    /* pcpPointers should be able to hold a total of 16 pointers */
    /* We start the index with 1 and NOT 0 */
    
    int    ilRc;
    static char pclMultiTag[64] = "Content-Type: multipart";
    static char pclTextTag[64] = "Content-Type: text/plain";
    char pclBoundaryTag [64] = "boundary=\""; /* DONT make this variable static!!! */
    static char pclLineBreak[16] = "\r\n\r\n";
    int blIsMultiPart;
    int blRepeat;
    char *pclStart;
    char *pclEnd;
    char *pclTemp;
    char *pclBoundarySearch;
    char *pclBlocks[128];
    char pclBoundaryName[256];
    int ilNumberOfBlocks = 0;
    char pclDebugString[32];

    /* Chek if it is a multipart message */
    
    pclStart = strstr(pcpRawMail,pclMultiTag);
    
    if (pclStart == NULL) 
    {
        /* Apparently not a multipart message.*/
        blIsMultiPart = FALSE;
    } /* end if */
    else
    {
        blIsMultiPart = TRUE;
    } /* end else */
    
    pclStart = pcpRawMail;
    blRepeat = TRUE;
    
    /* Find the plain text blocks */
    
    while (blRepeat == TRUE && ilNumberOfBlocks < 16)
    {
    
        pclTemp = strstr(pclStart,pclTextTag);
    
        if (pclTemp == NULL)
        {
            if (ilNumberOfBlocks == 0)
            {
                /* No text found. Return zero blocks */
            
                dbg(TRACE,"No text/plain content found in the mail");
                return 0;
            } /* end if */
            else
            {
                /* No more text in the message */
                
                blRepeat = FALSE;
                break;
            } /* end else */
        } /* end if */
        
        /* Find the beginning of the text body (<cr><lf><cr><lf>) */
    
        pclStart = pclTemp;
        pclTemp = strstr(pclStart,pclLineBreak);
        
        strncpy(pclDebugString,pclStart,31);
        pclDebugString[31] = 0x00;
        dbg(DEBUG,"pclStart pointing at: <%s>",pclDebugString);
        
        if (pclTemp == NULL)
        {
            if (ilNumberOfBlocks == 0)
            {
                /* No text found. Return zero blocks */
            
                dbg(TRACE,"No text/plain content found in mail");
                return 0;
            } /* end if */
            else
            {
                /* No more text in the message */
                
                blRepeat = FALSE;
                break;
            } /* end else */
        } /* end if */
        
        /* Found a text block. Find the end of it */
        /* Simple messages are easy... The end of the message */
        /* For multipart we have to find the boundary marker */
        
        ilNumberOfBlocks++;
        pcpPointers[ilNumberOfBlocks] = pclTemp + 4;
        
        if (blIsMultiPart == FALSE)
        {
            /* This is all that we have. Break out */
                    return ilNumberOfBlocks;
        } /* end if */
        
        /* A multipart message. Look for "\r\n--" (the beginning of the boundary marker) */
        
        pclStart = pclTemp + 4;
        pclTemp = strstr(pclStart,"\r\n--");
        
        if (pclTemp == NULL)
        {
            /* Bad message format. Stop processing the message */

            dbg(TRACE,"Missing boundary marker. Returning what ever we have at this point");
            return ilNumberOfBlocks;
        } /* end if */
        
        /* Found the "\r\n--", make sure that it is a boundary marker */
        
        pclTemp += 4;
        pclEnd = strstr(pclTemp,"\r\n");
        strncat(pclBoundaryTag,pclTemp, pclEnd - pclTemp);
        
        /* check if the boundary is defined in the message */
        
        dbg(TRACE,"Boundary searched for <%s>",pclBoundaryTag);    
        pclBoundarySearch = strstr(pcpRawMail,pclBoundaryTag);
        
        if (pclBoundarySearch == NULL)
        {
            /* A boundary definition was not found */
            
            dbg(TRACE,"A boundary prefix was not found in the message body. Text block will be set to the end of message");
                    
            blRepeat = FALSE;
        } /* end if */
        else
        {
            /* Split the raw mail here and adjust pointers */
            
            *(pclTemp - 2) = 0x00;
            pclStart = pclEnd + 2;
        } /* end else */    
    } /* end while */

    /* Finished. Return the number of text blocks detected */
    
    dbg(TRACE,"A total of <%i> text blocks were found",ilNumberOfBlocks);
    return ilNumberOfBlocks;
} /* end of FindTextBlocks */

int generateTelex12Prefix(char *strHeader, char *strMsg)
{
    int iRet=0;
    char strEmail[256];
    char strMsgID[21];
    char *strPtr;
    int iCnt=0;

    memset(strEmail, 0, sizeof(strEmail));
    memset(strMsgID, 0, sizeof(strMsgID));
    strPtr=strstr(strMsg, "Von: ");
    if ( strPtr!=NULL )
    {
        strPtr+=5;
        while (strPtr[0]!=' ')
        {
            strEmail[iCnt++]=strPtr[0];
            strPtr++;
        }
    }

    iCnt=0;
    strPtr=strstr(strMsg, "COMMS REF:");
    if ( strPtr!=NULL )
    {
        strPtr+=10;
        while ( !isdigit(strPtr[0]) )
           strPtr++;

        while ( isdigit(strPtr[0]) )
        {
            strMsgID[iCnt++]=strPtr[0];
            strPtr++;
        }
    }
    sprintf(strHeader, "=ORIGIN\n%s\n"
                       "=MSGID\n%s\n"
                       "=TEXT\n",
            strEmail, strMsgID);
    return iRet;
}

int passout()
{
    char  *pclCfgPath = NULL;
    char  pclCfgFile[1024];
    char *strPtr;
    int iRet=0;

    /* get the config path */
    if ((pclCfgPath = getenv("CFG_PATH")) == NULL)
    {
        dbg(DEBUG,"ERROR: missing environment CFG_PATH ...");
        return RC_FAIL;
    }

    strcpy(pclCfgFile, pclCfgPath);
    if (pclCfgFile[strlen(pclCfgFile)-1] != '/')
        strcat(pclCfgFile, "/");

    strcat(pclCfgFile, mod_name);
    strcat(pclCfgFile, ".cfg");

    iRet= iGetConfigEntry(cgConfigFile,"MAIL","MAIL_PASSWD",CFG_STRING,pclCfgFile);
    if (iRet!=RC_SUCCESS) 
    {
        dbg(DEBUG,"ERROR: missing email password\n");
        return RC_FAIL;
    }

    strPtr=cgPassword;
     CDecode(&strPtr);
    dbg(TRACE,"Passwd[%s]\n", strPtr);

    return RC_SUCCESS;
}


