#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Interface/goshdl.c 1.4 2013/06/18 10:33:37SIN cst Exp  $";
#endif /* _DEF_mks_version */

/******************************************************************************/
/*                                                                            */
/* UFIS AS GOSHDL.C                                                           */
/*                                                                            */
/* Author         : Christian Stybert                                         */
/* Date           : September 2009                                            */
/* Description    : Process to receive and send messages from/to SAFEGATE GOS */
/*                                                                            */
/* Update history :                                                           */
/* 20120321 MEI: Implemented "RECEIVER" and "KEEPALIVE" work flow             */
/* 20130618 CST: DXBDCA-176 Fixed TWS/TWE overwritten by incoming queue events*/
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_goshdl[]="%Z% UFIS 4.5 (c) ABB AAT/I %M% %I% / %E% %U% / AKL";
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <netdb.h>
#include <time.h>
#include "debugrec.h"
#include "hsbsub.h"
#include "tools.h"
#include "helpful.h"
#include "timdef.h"
#include "goshdl.h"
 

#define XS_BUFF  128
#define S_BUFF   512
#define M_BUFF   1024
#define L_BUFF   2048
#define XL_BUFF  4096
#define XXL_BUFF 8192 



/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
FILE *outp       = NULL;
int  debug_level = TRACE;
/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/
extern int  SetSignals(void (*HandleSignal)(int));
extern int  DebugPrintItem(int,ITEM *);
extern int  DebugPrintEvent(int,EVENT *);
extern void snap(char*,int,FILE*);
extern void HandleRemoteDB(EVENT*);
extern int  GetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel,char *pcpDef,char *pcpTrim);
extern int  GetNoOfElements(char *s, char c);
extern long nap(long);

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM *prgItem        = NULL;         /* The queue item pointer  */
static EVENT *prgEvent      = NULL;         /* The event pointer       */
static int  igItemLen       = 0;            /* length of incoming item */
static int  igInitOK        = FALSE;        /* Flag for init */

static char pcgConfFile[S_BUFF];            /* buffer for config-file name */
static int  igModID_Rcvr;                   /* MOD-ID of Receiver  */

static char pcgHomeAp[XS_BUFF];             /* buffer for home airport */
static char pcgTabEnd[XS_BUFF];             /* buffer for TABEND */
static char pcgTwStart[XS_BUFF] = "";
static char pcgTwEnd[XS_BUFF];


/*entry's from configfile*/

static char pcgConfigFile[512];
static char pcgCfgBuffer[512];
static EVENT *prgOutEvent = NULL;
static char pcgServerChars[110];
static char pcgClientChars[110];
static char pcgUfisConfigFile[512];
static char pcgKeepAliveFormat[M_BUFF];
static char pcgSendAckFormat[M_BUFF];
static char pcgReceiver[20];

typedef struct 
{
    int ilModId;
    short ilPrio;
    short ilToBeActed;
    char pclProcName[12];
    char pclSndCmd[8];
} SNDCMD;

#ifndef _SOLARIS
static struct sockaddr_in my_client;
#else
static struct sockaddr my_client;
#endif

SNDCMD rgConnectAct;
static BOOL bgSocketOpen = FALSE;       /* global flag for connection state */
static BOOL bgAlarm = FALSE;            /* global flag for time-out socket-read */
static int  igSock = 0;                 /* global tcp/ip socket for send/receive */
static int  igMsgCounter = 0;           /* counter for message numbers */
static int  igRecvTimeout = 0;          /* number of secs to wait for a valid telegram */
static int  igWaitForAck;               /* Time to wait for an ACK */
static int  igMaxSends;                 /* Max retry count (send) */
static int  igKeepAlive;                /* The Keep Alive interval (0 = no Keep Alive) */
static int  igKeepAliveCounter;         /* The Keep Alive counter */
static int  igSendAck;                  /* Flag for sending ACKs (0 or 1) */
static BOOL bgRemoveSuffix = FALSE;     /* Flag to remove suffix from stand numbers. DXB specific */
static BOOL bgRemoveZero = FALSE;       /* Flag to remove zeros from stand numbers. DXB specific */
static int  igStandTagLen;              /* Length of the stand tag. DXB specific */
static CFG  prgCfg;
static char pcgGOS_Host[64];            /* buffer for the host that is connected */
static char pcgRecvBuffer[M_BUFF];      /* global buffer for socket-read */
static char pcgSendBuffer[M_BUFF];      /* global buffer for socket-write */
static int  igHeaderSize;
static FILE *pgReceiveLogFile = NULL;   /* log file pointer for all received messages */
static FILE *pgSendLogFile = NULL;      /* log file pointer for all sent messages */
static char pcgTestMessage[M_BUFF] = "\0";


static int  Init_Handler();
static int  Reset(void);                        /* Reset program          */
static void Terminate(int);                     /* Terminate program      */
static void HandleSignal(int);                  /* Handles signals        */
static void HandleErr(int);                     /* Handles general errors */
static void HandleQueErr(int);                  /* Handles queuing errors */
static int  HandleInternalData(void);           /* Handles event data     */
static void HandleQueues(void);                 /* Waiting for Sts.-switch*/

/******************************************************************************/
/* Function prototypes                                                        */
/******************************************************************************/

static int  GetQueues();
static int  SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
                char *pcpTwStart,char* pcpTwEnd,
                char *pcpSelection,char *pcpFields,char *pcpData,
                char *pcpAddStruct,int ipAddstructSize); 
static int  GetConfig();
static void CloseTCP(void);
static int  CheckData(char *pcpData, int ipIdx);
static int  GetData(char *pcpMethod, int ipIdx, char *pcpName);
static int  poll_q_and_sock();
static int  Receive_data(int ipSock, int ipAlarm, char *pcpCalledBy);
static int  Send_data(int ipSock, char *pcpData);
static void snapit(void *pcpBuffer, long lpDataLen, FILE *pcpDbgFile);
static int  GetCfgEntry(char *pcpFile,char *pcpSection,char *pcpEntry,short spType,char **pcpDest,
                int ipValueType,char *pcpDefVal);
static int  CheckValue(char *pcpEntry,char *pcpValue,int ipType);
static int  SendKeepAliveMsg();
static int  SendAckMsg(int MsgNo);
static int  GetMsgno(char *pcpBuff);

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/

MAIN
{
    int     ilRc = RC_SUCCESS;      /* Return code              */
    int     ilCnt = 0;
    int     ilItemFlag=TRUE; 
    time_t  now = 0;
    INITIALIZE;                     /* General initialization   */

    /* signal handling of SIGPIPE,SIGCHLD,SIGALRM,SIGTERM */
    SetSignals(HandleSignal);

    dbg(TRACE,"------------------------------------------");
    dbg(TRACE,"MAIN: version <%s>",sccs_goshdl);

    /* Attach to the MIKE queues */
    do
    {
        ilRc = init_que();
        
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end if */
    
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    } /* end if */
    else
    {
        dbg(TRACE,"MAIN: init_que() OK!");
        dbg(TRACE,"MAIN: mod_id   <%d>",mod_id);
        dbg(TRACE,"MAIN: mod_name <%s>",mod_name);
    }/* end else */

    *pcgConfFile = 0x00;
    sprintf(pcgConfFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    ilRc = TransferFile(pcgConfFile);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
    } /* end if */
    
    dbg(TRACE,"MAIN: Binary-file = <%s>",pcgConfFile);
    ilRc = SendRemoteShutdown(mod_id);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    }
    
    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: waiting for status switch ...");
        HandleQueues();
        dbg(TRACE,"MAIN: now running ...");
    }/* end of if */
    
    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        dbg(TRACE,"------------------------------------------");
        if(igInitOK == FALSE)
        {
            ilRc = Init_Handler();
            
            if(ilRc == RC_SUCCESS)
            {
                dbg(TRACE,"");
                dbg(TRACE,"------------------------------------------");
                dbg(TRACE,"MAIN: initializing OK");
                igInitOK = TRUE;
            } 
        }
    }
    else
    {
        Terminate(1);
    }
    dbg(TRACE,"------------------------------------------");
    
    if (igInitOK == TRUE)
    {
        now = time(NULL);
        
        while(TRUE)
        {
            if ((ilRc = poll_q_and_sock()) == RC_SUCCESS)
            {
            }
        now = time(NULL);
        } /* end while */
    } /* end if Initialized OK */
    else
    {
        dbg(TRACE,"MAIN: Init_Handler() failed with <%d> Sleeping 30 sec.! Then terminating ...",ilRc);
        sleep(30);
    } /* end else Initilized OK */
    exit(0);
    return 0;
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int Init_Handler()
{
    int ilRc = RC_SUCCESS;            /* Return code */

    GetQueues();
    /* reading default home-airport from sgs.tab */
    memset(pcgHomeAp,0x00,sizeof(pcgHomeAp));
    ilRc = tool_search_exco_data("SYS","HOMEAP",pcgHomeAp);
    if (ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"Init_Handler : No HOMEAP entry in sgs.tab: EXTAB! Please add!");
        return RC_FAIL;
    }
    else
    {
        dbg(TRACE,"Init_Handler : HOMEAP = <%s>",pcgHomeAp);
    }
    /* reading default table-extension from sgs.tab */
    memset(pcgTabEnd,0x00,sizeof(pcgTabEnd));
    ilRc = tool_search_exco_data("ALL","TABEND",pcgTabEnd);
    if (ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"Init_Handler : No TABEND entry in sgs.tab: EXTAB! Please add!");
        return RC_FAIL;
    }
    else
    {
        dbg(TRACE,"Init_Handler : TABEND = <%s>",pcgTabEnd);
        memset(pcgTwEnd,0x00,XS_BUFF);
        sprintf(pcgTwEnd,"%s,%s,%s",pcgHomeAp,pcgTabEnd,mod_name);
        dbg(TRACE,"Init_Handler : TW_END = <%s>",pcgTwEnd);
    }
    
    if (ilRc = GetConfig() == RC_FAIL)
    {
        dbg(TRACE,"Init_handler: Configuration error, terminating");
        Terminate(30);
    } /* end if */
    
    if (strlen(prgCfg.recv_log) > 0)
    {
        pgReceiveLogFile = fopen(prgCfg.recv_log,"w");
        
        if (!pgReceiveLogFile)
        {
            dbg(TRACE,"Init_Handler : ReceiveLog: <%> not opened! fopen() returns <%s>.",
                prgCfg.recv_log,strerror(errno));
        } /* end if */
    } /* end if */
    
    if (strlen(prgCfg.send_log) > 0)
    {
        pgSendLogFile = fopen(prgCfg.send_log,"w");
        
        if (!pgSendLogFile)
        {
            dbg(TRACE,"Init_Handler : SendLog: <%> not opened! fopen() returns <%s>.",
            prgCfg.send_log,strerror(errno));
        } /* end if */
    } /* end if */


    /* Initiate opening of the Server socket */
    
    ilRc = OpenServerSocket();
    
    return(ilRc);
} /* end of Init_Handler */

/*********************************************************************
Function : GetQueues()
Paramter :
Return Code: RC_SUCCESS,RC_FAIL
Result:
Description: Gets all necessary queue-ID's for CEDA-internal
             communication!
*********************************************************************/

static int GetQueues()
{
    int ilRc = RC_FAIL;

    /* get mod-id of exchdi */
    if ((igModID_Rcvr = tool_get_q_id(pcgReceiver)) == RC_NOT_FOUND ||
        igModID_Rcvr == RC_FAIL || igModID_Rcvr == 0)
    {
        dbg(TRACE,"GetQueues   : tool_get_q_id(%s) returns: <%d>",pcgReceiver, igModID_Rcvr);
        igModID_Rcvr = 7920;
        ilRc = RC_FAIL;
    }
    else
    {
        dbg(TRACE,"GetQueues   : <%s> mod_id <%d>",pcgReceiver,igModID_Rcvr);
        ilRc = RC_SUCCESS;
    } 
    return ilRc;
} /* end of GetQueues */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/

static int Reset()
{

    int ilRc = RC_SUCCESS;    /* Return code */
    
    dbg(TRACE,"Reset: now reseting ...");

    return ilRc;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/

static void Terminate(int ipSleep)
{
    dbg(TRACE,"Terminate: now leaving ...");
    
    CloseTCP();
    
    if (pgReceiveLogFile != NULL)
    {
        fclose(pgReceiveLogFile);
    } /* end if */
    
    
    if (pgSendLogFile != NULL)
    {
        fclose(pgSendLogFile);
    } /* end if */

    sleep(ipSleep);
  
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
    switch(pipSig)
    {
        case SIGALRM:
            bgAlarm = TRUE;
            break;
        case SIGPIPE:
            if (igSock != 0)
                CloseTCP();
                break;
        case SIGCHLD:
            break;
        case SIGTERM:
            Terminate(1);
            break;
        default    :
            Terminate(10);
        break;
    } /* end of switch */
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/

static void HandleErr(int pipErr)
{
    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/

static void HandleQueErr(int pipErr)
{
    int    ilRc = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
            dbg(TRACE,"<%d> : msgsnd failed",pipErr);
            break;
    case    QUE_E_GET    :    /* Error using msgrcv */
            if(pipErr != 4)        
             dbg(DEBUG,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    : 
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :   unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        /*dbg(TRACE,"<%d> : no messages on queue",pipErr);*/
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    case    QUE_E_PRIORITY    :
        dbg(TRACE,"<%d> : wrong priority was send ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/

static void HandleQueues()
{
    int ilRc = RC_SUCCESS;            /* Return code */
    int ilBreakOut = FALSE;
    
    do{
        memset(prgItem,0x00,igItemLen);
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;
            
        if( ilRc == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
            
            if( ilRc != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRc);
            } /* fi */
        
            switch( prgEvent->command )
            {
                case    HSB_STANDBY    :
                    ctrl_sta = prgEvent->command;
                    break;    
                case    HSB_COMING_UP    :
                    ctrl_sta = prgEvent->command;
                    break;    
                case    HSB_ACTIVE    :
                    ctrl_sta = prgEvent->command;
                    ilBreakOut = TRUE;
                    break;    
                case    HSB_ACT_TO_SBY    :
                    ctrl_sta = prgEvent->command;
                    break;    
                case    HSB_DOWN    :
                    /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                    ctrl_sta = prgEvent->command;
                    Terminate(10);
                    break;    
                case    HSB_STANDALONE    :
                    ctrl_sta = prgEvent->command;
                    ResetDBCounter();
                    ilBreakOut = TRUE;
                    break;    
                case    REMOTE_DB :
                    /* ctrl_sta is checked inside */
                    HandleRemoteDB(prgEvent);
                    break;
                case    SHUTDOWN    :
                    Terminate(1);
                    break;
                case    RESET        :
                    ilRc = Reset();
                    break;
                case    EVENT_DATA    :
                    dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                    break;
                case    TRACE_ON :
                    dbg_handle_debug(prgEvent->command);
                    break;
                case    TRACE_OFF :
                    dbg_handle_debug(prgEvent->command);
                    break;
                default            :
                    dbg(TRACE,"HandleQueues: unknown event");
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        }
        else
        {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
    } while (ilBreakOut == FALSE);
    
    if(igInitOK == FALSE)
    {
        ilRc = Init_Handler();
        
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"HandleQueues: Init_Handler() OK!");
            igInitOK = TRUE;
        } /* end if */
        else
        {
            dbg(TRACE,"HandleQueues: Init_Handler() failed!");
            igInitOK = FALSE;
        } /* end else */
    }/* end of if */
} /* end of HandleQueues */

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/

static int HandleInternalData()
{
    int  ilRC = RC_SUCCESS;         /* Return code */
    char *pclSelection = NULL;
    char *pclFields = NULL;
    char *pclData = NULL;
    char *pclTmpPtr=NULL;
    BC_HEAD *bchd = NULL;           /* Broadcast header*/
    CMDBLK  *cmdblk = NULL; 
    int ilLen;

    bchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
    cmdblk= (CMDBLK  *) ((char *)bchd->data);

/*
	  Must conserve the TWS/TWE from configulation. Reference directly from event	

    ilRC = iGetConfigEntry(pcgConfigFile,"MAIN","TWS",CFG_STRING,pcgTwStart);
    
    if (ilRC != RC_SUCCESS)
    {
        strcpy(pcgTwStart,cmdblk->tw_start);
    } */ /* end if */
/*    strcpy(pcgTwEnd,cmdblk->tw_end); */
 

    /***************************************/
    /* DebugPrintItem(DEBUG,prgItem);      */
    /* DebugPrintEvent(DEBUG,prgEvent);    */
    /***************************************/
    
    pclSelection = cmdblk->data;
    pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
    pclData = (char *)pclFields + strlen(pclFields) + 1;

    dbg(DEBUG,"========================= START ===============================");
    
    if (strcmp(cmdblk->command,"XMLO") == 0)
    {
        dbg(DEBUG,"Command:   <%s>",cmdblk->command);
        dbg(DEBUG,"Selection: <%s>",pclSelection);
        dbg(DEBUG,"Fields:    <%s>",pclFields);
        dbg(DEBUG,"Data:      \n<%s>",pclData);
        dbg(DEBUG,"TwStart:   <%s>",cmdblk->tw_start);
        dbg(DEBUG,"TwEnd:     <%s>",cmdblk->tw_end);

        ilRC = Send_data(igSock,pclData);
    } /* end if */
    else
    {
        dbg(TRACE,"HandleInternalData: Invalid command <%s>",cmdblk->command);
        ilRC = RC_FAIL;
    } /* end else */
    
  dbg(DEBUG,"========================= END ================================");
  return ilRC;
} /* end of HandleInternalData() */

/*********************************************************************
Function : SendEvent()
Paramter : IN: pcpCmd = command for cmdblk->command
           IN: ipModID = process-ID where the event is send to
       IN: ipPriority = priority for sending ( 1- 5, usuallay 3)
       IN: pcpTable = Name (3 letters) of cmdblk->obj_name (if
           necessary), will be expanded with "pcgTabEnd".
       IN: pcpTwStart = cmdblk->twstart
       IN: pcpTwEnd = cmdblk->twend (always HOMEAP,TABEND,processname)
       IN: pcpSelection = selection for event (cmdblk->data)
       IN: pcpFields = fieldlist (corresponding to pcpdata)
       IN: pcpData = datalist (comma separated, corresponding to 
                                   pcpFields)
       IN: pcpAddStruct = additional structure to be transmitted
       IN: ipAddStructSize = size of the additional structure
Return Code: RC_SUCCESS, RC_FAIL
Result:
Description: Sends an event to another CEDA-process using que(QUE_PUT).
             Sends the event in standard CEDA-format (BCHEAD,CMDBLK,
         selection,fieldlist,datalist) or sends a different
         data structure (special) at CMDBLK->data. !! Sends always
         only one type, standard OR special, with one event !!
*********************************************************************/

static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
             char *pcpTwStart, char *pcpTwEnd,
             char *pcpSelection,char *pcpFields,char *pcpData,
             char *pcpAddStruct,int ipAddStructSize)
{
  int     ilRc             = RC_FAIL;
  int     ilLen            = 0;
  EVENT   *prlOutEvent  = NULL;
  BC_HEAD *prlOutBCHead = NULL;
  CMDBLK  *prlOutCmdblk = NULL;

  if (pcpAddStruct == NULL)
    ipAddStructSize = 0;
  if (ipAddStructSize == 0)
    pcpAddStruct = NULL;

  /* size-calculation for prlOutEvent */
  ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
    strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 
    ipAddStructSize + 128;

  /* memory for prlOutEvent */
  if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
    {
      dbg(TRACE,"SendEvent: cannot malloc <%d>-bytes for outgoing event!",ilLen);
      prlOutEvent = NULL;
    }else{
      /* clear whole outgoing event */
      memset((void*)prlOutEvent, 0x00, ilLen);

      /* set event structure... */
      prlOutEvent->type         = SYS_EVENT;
      prlOutEvent->command      = EVENT_DATA;
      prlOutEvent->originator   = (short)mod_id;
      prlOutEvent->retry_count  = 0;
      prlOutEvent->data_offset  = sizeof(EVENT);
      prlOutEvent->data_length  = ilLen - sizeof(EVENT); 

      /* BC_HEAD-Structure... */
      prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
      /* prlOutBCHead->rc = (short)RC_SUCCESS;*/
      prlOutBCHead->rc = (short)NETOUT_NO_ACK;/*spaeter nur bei disconnect*/
      strncpy(prlOutBCHead->dest_name,mod_name,10);
      strncpy(prlOutBCHead->recv_name, "EXCO",10);
 
      /* Cmdblk-Structure... */
      prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
      strcpy(prlOutCmdblk->command,pcpCmd);
      if (pcpTable != NULL)
    {
      strcpy(prlOutCmdblk->obj_name,pcpTable);
      strcat(prlOutCmdblk->obj_name,pcgTabEnd);
    }
        
      /* setting tw_x entries */
      strncpy(prlOutCmdblk->tw_start,pcpTwStart,32);
      strncpy(prlOutCmdblk->tw_end,pcpTwEnd,32);
        
      /* means that no additional structure is used */
      /* STANDARD CEDA-ipcs between CEDA-processes */
      if (pcpAddStruct == NULL)
    {
      /* setting selection inside event */
      strcpy(prlOutCmdblk->data,pcpSelection);
      /* setting field-list inside event */
      strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1,pcpFields);
      /* setting data-list inside event */
      strcpy((prlOutCmdblk->data + (strlen(pcpSelection)+1) + (strlen(pcpFields)+1)),pcpData);
    }else{
      /*an additional structure is used and will be copied to */
      /*cmdblk + sizeof(CMDBLK).!!! No STANDARD CEDA-ipcs is used !!!! */
      memcpy(prlOutCmdblk->data,(char*)pcpAddStruct,ipAddStructSize);    
    }

      /*DebugPrintEvent(DEBUG,prlOutEvent);*/ 
      /*snapit((char*)prlOutEvent,ilLen,outp);*/
      dbg(DEBUG,"SendEvent: sending event to mod_id <%d>",ipModID);

      if (ipModID != 0)
    {
      if ((ilRc = que(QUE_PUT,ipModID,mod_id,ipPriority,ilLen,(char*)prlOutEvent))
          != RC_SUCCESS)
        {
          dbg(TRACE,"SendEvent: QUE_PUT returns: <%d>", ilRc);
          Terminate(1);
        }
    }else{
      dbg(TRACE,"SendEvent: mod_id = <%d>! Can't send!",ipModID);
    }
      /* free memory */
      free((void*)prlOutEvent); 
    }
  return ilRc;
} /* end of SendEvent() */

/* ******************************************************************** */
/* Following the GetConfig function                                     */
/* Reads the .cfg file and fill in the configuration structure          */
/* ******************************************************************** */

static int GetConfig()
{
    int     ilRC = RC_SUCCESS;
    int     ilI;
    int     ilJ;
    char    pclTmpBuf[128];
    char    pclDebugLevel[128];
    int     ilLen;
    char    pclBuffer[10];
    char    pclClieBuffer[100];
    char    pclServBuffer[100];
    char    clKeepAliveFormatF[256];
    char    clAckFormatF[256];
    char    pclTmpStr[128];
    FILE    *fplFp;

    sprintf(pcgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"Config File is <%s>",pcgConfigFile);


    ilRC = iGetConfigEntry(pcgConfigFile,"MAIN","DEBUG_LEVEL",CFG_STRING,pclDebugLevel);
    if (strcmp(pclDebugLevel,"DEBUG") == 0)
    {
        debug_level = DEBUG;
    }
    else
    {
        if (strcmp(pclDebugLevel,"TRACE") == 0)
        {
            debug_level = TRACE;
        }
        else
        {
            if (strcmp(pclDebugLevel,"NULL") == 0)
            {
                debug_level = 0;
            }
            else
            {
                debug_level = TRACE;
            }
        }
    }

    dbg(TRACE,"DEBUG_LEVEL = %s",pclDebugLevel);

    /* Get Client and Server chars */
    sprintf(pcgUfisConfigFile,"%s/ufis_ceda.cfg",getenv("CFG_PATH")); 
    ilRC = iGetConfigEntry(pcgUfisConfigFile,"CHAR_PATCH","CLIENT_CHARS",
                CFG_STRING, pclClieBuffer);
    if (ilRC == RC_SUCCESS)
    {
        ilRC = iGetConfigEntry(pcgUfisConfigFile,"CHAR_PATCH","SERVER_CHARS",
                CFG_STRING, pclServBuffer);
        if (ilRC == RC_SUCCESS)
        {
            ilI = GetNoOfElements(pclClieBuffer,',');
            if (ilI == GetNoOfElements(pclServBuffer,','))  
            {
                memset(pcgServerChars,0x00,100*sizeof(char));
                memset(pcgClientChars,0x00,100*sizeof(char));
                for (ilJ=0; ilJ < ilI; ilJ++) 
                {
                    GetDataItem(pclBuffer,pclServBuffer,ilJ+1,',',"","\0\0");
                    pcgServerChars[ilJ*2] = atoi(pclBuffer);    
                    pcgServerChars[ilJ*2+1] = ',';
                } /* end for */
                pcgServerChars[(ilJ-1)*2+1] = 0x00;
                for (ilJ=0; ilJ < ilI; ilJ++) 
                {
                    GetDataItem(pclBuffer,pclClieBuffer,ilJ+1,',',"","\0\0");
                    pcgClientChars[ilJ*2] = atoi(pclBuffer);    
                    pcgClientChars[ilJ*2+1] = ',';
                } /* end for */
                pcgClientChars[(ilJ-1)*2+1] = 0x00;
                dbg(DEBUG,"New Clientchars <%s> dec <%s>",pcgClientChars,pclClieBuffer);
                dbg(DEBUG,"New Serverchars <%s> dec <%s>",pcgServerChars,pclServBuffer);
                dbg(DEBUG,"Serverchars <%d> ",strlen(pcgServerChars));
                dbg(DEBUG,"%s  and count <%d> ",pclBuffer,strlen(pcgServerChars));
                dbg(DEBUG,"ilI <%d>",ilI);
            } /* end if */
        } /* end if */
        else
        {
            ilRC = RC_SUCCESS;
            dbg(DEBUG,"Use standard (old) serverchars");    
        } /* end else */
    } /* end if */
    else
    {
        dbg(DEBUG,"Use standard (old) serverchars");    
        ilRC = RC_SUCCESS;
    } /* end else */
    if (ilRC != RC_SUCCESS)
    {
        strcpy(pcgClientChars,"\042\047\054\012\015");
        strcpy(pcgServerChars,"\260\261\262\263\263");
        ilRC = RC_SUCCESS;
    }
		/* 20130618 CST: DXBDCA-176 */
	   iGetConfigEntry(pcgConfigFile,"MAIN","TWS",CFG_STRING,pcgTwStart);
		dbg(TRACE,"TwStart = <%s>",pcgTwStart);


    dbg(TRACE,"Now read parameters for interface");
            
    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","MODE",CFG_STRING,&prgCfg.mode,CFG_ALPHA,"REAL"))
                        != RC_SUCCESS)
        return RC_FAIL;

    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","TYPE",CFG_STRING,&prgCfg.type,CFG_PRINT,"SERVER"))
                        != RC_SUCCESS)
        { }
    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","HOST1",CFG_STRING,&prgCfg.host1,CFG_PRINT,""))
                        != RC_SUCCESS)
        return RC_FAIL;
    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","HOST2",CFG_STRING,&prgCfg.host2,CFG_PRINT,""))
                        != RC_SUCCESS)
        return RC_FAIL;
    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","SERVICE_PORT",CFG_STRING,&prgCfg.service_port,CFG_PRINT,"EXCO_DGS"))
                        != RC_SUCCESS)
        return RC_FAIL;
    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","WAIT_FOR_ACK",CFG_STRING,&prgCfg.wait_for_ack,CFG_NUM,"2"))
                        != RC_SUCCESS)
        return RC_FAIL;
    else
        igWaitForAck = atoi(prgCfg.wait_for_ack);
    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","RECV_TIMEOUT",CFG_STRING,&prgCfg.recv_timeout,CFG_NUM,"60"))
                        != RC_SUCCESS)
        return RC_FAIL;
    else
        igRecvTimeout = atoi(prgCfg.recv_timeout);
    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","MAX_SENDS",CFG_STRING,&prgCfg.max_sends,CFG_NUM,"2"))
                        != RC_SUCCESS)
        return RC_FAIL;
    else
        igMaxSends = atoi(prgCfg.max_sends);
    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","RECV_LOG",CFG_STRING,&prgCfg.recv_log,CFG_PRINT,""))
                        != RC_SUCCESS)
        return RC_FAIL;
    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","SEND_LOG",CFG_STRING,&prgCfg.send_log,CFG_PRINT,""))
                        != RC_SUCCESS)
        return RC_FAIL;
    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","TRY_RECONNECT",CFG_STRING,&prgCfg.try_reconnect,CFG_NUM,"60"))
                        != RC_SUCCESS)
        return RC_FAIL;
    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","KEEP_ALIVE",CFG_STRING,&prgCfg.keep_alive,CFG_NUM,"0"))
                        != RC_SUCCESS)
        return RC_FAIL;
    else
        igKeepAlive = atoi(prgCfg.keep_alive);
     
    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","KEEP_ALIVE_FORMAT",CFG_STRING,&prgCfg.keep_alive_format,CFG_PRINT,"tcpkeepalive.txt"))
                        != RC_SUCCESS)
        return RC_FAIL;
    else
        sprintf(clKeepAliveFormatF,"%s/%s",getenv("CFG_PATH"),prgCfg.keep_alive_format);

    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","HEADER_PREFIX",CFG_STRING,&prgCfg.header_prefix,CFG_PRINT,"#"))
                        != RC_SUCCESS)
        return RC_FAIL;
    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","HEADER_SIZE",CFG_STRING,&prgCfg.header_size,CFG_NUM,"6"))
                        != RC_SUCCESS)
        return RC_FAIL;
    else
        igHeaderSize = atoi(prgCfg.header_size);
    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","SEND_ACK",CFG_STRING,&prgCfg.send_ack,CFG_NUM,"1"))
                        != RC_SUCCESS)
        return RC_FAIL;
    else
        igSendAck = atoi(prgCfg.send_ack);
    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","ACK_FORMAT",CFG_STRING,&prgCfg.ack_format,CFG_PRINT,"tcpack.txt"))
                        != RC_SUCCESS)
        return RC_FAIL;
    else
        sprintf(clAckFormatF,"%s/%s",getenv("CFG_PATH"),prgCfg.ack_format);
        
    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","MSGNO_KEYWORD",CFG_STRING,&prgCfg.msgno_keyword,CFG_PRINT,"msgno="))
                        != RC_SUCCESS)
        return RC_FAIL;
        
    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","FTYP_KEYWORD",CFG_STRING,&prgCfg.ftyp_keyword,CFG_PRINT,""))
                        != RC_SUCCESS)
        return RC_FAIL;
    
    /* Special Position name handling. Remove trailing characters and leading zero */
    /* A01R becomes A1. Safegate is not able to handle the complex names           */

    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","REMOVE_SUFFIX",CFG_STRING,&prgCfg.remove_suffix,CFG_PRINT,"NO"))
                        != RC_SUCCESS)
        return RC_FAIL;
        
    if (strcmp(prgCfg.remove_suffix,"YES") == 0)
    {
        bgRemoveSuffix = TRUE;
    } /* end if */
    
    /* This is the list of stand positions that should not have the suffix removed */
    
    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","KEEP_SUFFIX",CFG_STRING,&prgCfg.keep_suffix,CFG_PRINT,","))
                        != RC_SUCCESS)
        return RC_FAIL;



    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","REMOVE_ZERO",CFG_STRING,&prgCfg.remove_zero,CFG_PRINT,"NO"))
                        != RC_SUCCESS)
        return RC_FAIL;
        
    if (strcmp(prgCfg.remove_zero,"YES") == 0)
    {
        bgRemoveZero = TRUE;
    } /* end if */

    if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","STAND_TAG",CFG_STRING,&prgCfg.stand_tag,CFG_PRINT,"stand>"))
                        != RC_SUCCESS)
        return RC_FAIL;
    igStandTagLen = strlen(prgCfg.stand_tag);

    /* Load the KeepAlive and Ack formats */
    
    if(igKeepAlive)
    {
        if ((fplFp = (FILE *)fopen(clKeepAliveFormatF,"r")) == (FILE *)NULL)
        {
            dbg(TRACE,"KeepAlive Format File <%s> does not exist",clKeepAliveFormatF);
            return RC_FAIL;
        } /* end if */
        else
        {
            ilLen = fread(pcgKeepAliveFormat,1,1023,fplFp);
            pcgKeepAliveFormat[ilLen] = '\0';
            fclose(fplFp);
        } /* end else */
        if( strlen(pcgKeepAliveFormat) <= 0 )
        {
            dbg(TRACE,"KeepAlive File <%s> is empty",clKeepAliveFormatF);
            igKeepAlive = FALSE;
        }
        igKeepAliveCounter = 0;
    } /* end if */
     
    if(igSendAck)
    {
        if ((fplFp = (FILE *) fopen(clAckFormatF,"r")) == (FILE *)NULL)
        {
            dbg(TRACE,"SendAck Format File <%s> does not exist",clAckFormatF);
            return RC_FAIL;
        } /* end if */
        else
        {
            ilLen = fread(pcgSendAckFormat,1,1023,fplFp);
            pcgSendAckFormat[ilLen] = '\0';
            fclose(fplFp);
        } /* end else */
    } /* end if */


    /***************************************************/
    /* New config added for EFPS in Dubai              */
    /***************************************************/
    ilRC = iGetConfigEntry( pcgConfigFile,"MAIN","RECEIVER",CFG_STRING, pcgReceiver );
    if( ilRC != RC_SUCCESS || strlen(pcgReceiver) <= 0 )
        strcpy( pcgReceiver, "exchdi" );
    igModID_Rcvr = tool_get_q_id(pcgReceiver);
    dbg(TRACE,"RECEIVER <%s>-ID <%d>",pcgReceiver, igModID_Rcvr);

    memset( &rgConnectAct, 0, sizeof(SNDCMD) );
    rgConnectAct.ilToBeActed = FALSE;
    ilRC = iGetConfigEntry( pcgConfigFile,"MAIN","CONNECTED_ACT",CFG_STRING, pclTmpStr );
    if( !strcmp( pclTmpStr, "YES" ) )
    {
        rgConnectAct.ilToBeActed = TRUE;
        ilRC = iGetConfigEntry( pcgConfigFile,"CONNECTED_ACT","proc_name",CFG_STRING,rgConnectAct.pclProcName );
        if( ilRC != RC_SUCCESS ) rgConnectAct.ilToBeActed = FALSE;
    
        ilRC = iGetConfigEntry( pcgConfigFile,"CONNECTED_ACT","snd_cmd",CFG_STRING,rgConnectAct.pclSndCmd );
        if( ilRC != RC_SUCCESS ) rgConnectAct.ilToBeActed = FALSE;

        ilRC = iGetConfigEntry( pcgConfigFile,"CONNECTED_ACT","snd_prio",CFG_STRING,pclTmpStr );
        rgConnectAct.ilPrio = atoi( pclTmpStr );
        if( rgConnectAct.ilPrio <= 0 || rgConnectAct.ilPrio > 5 )
            rgConnectAct.ilToBeActed = FALSE;
        if( ilRC != RC_SUCCESS ) rgConnectAct.ilToBeActed = FALSE;
        if( rgConnectAct.ilToBeActed == TRUE )
        {
            rgConnectAct.ilModId = tool_get_q_id( rgConnectAct.pclProcName );
            if( rgConnectAct.ilModId == RC_NOT_FOUND || rgConnectAct.ilModId == RC_FAIL || rgConnectAct.ilModId == 0)
                rgConnectAct.ilToBeActed = FALSE;
            else
            {
                dbg( TRACE,"[CONNECTED_ACT] proc_name <%s>",rgConnectAct.pclProcName );
                dbg( TRACE,"[CONNECTED_ACT] snd_cmd <%s>",rgConnectAct.pclSndCmd );
                dbg( TRACE,"[CONNECTED_ACT] snd_prio <%d>",rgConnectAct.ilPrio );
                dbg( TRACE,"[CONNECTED_ACT] mod_id <%d>",rgConnectAct.ilModId );
            }
        }
    }
    return RC_SUCCESS;
} /* End of GetConfig() */

/* ******************************************************************** */
/* Following the poll_q_and_sock function                               */
/* Waits for input on the socket and polls the QCP for messages         */
/* ******************************************************************** */

static int poll_q_and_sock() 
{
    int ilRc;
    int ilRc_Connect = RC_FAIL;
    char pclTmpBuf[128];
    int ilI;

    do
    {
        nap(100); /* waiting because of NOT-waiting QUE_GETBIGNW */
        /*---------------------------*/
        /* now looking on ceda-queue */
        /*---------------------------*/

        while ((ilRc = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *) &prgItem)) == RC_SUCCESS)
        {
            /* depending on the size of the received item  */
            /* a realloc could be made by the que function */
            /* so do never forget to set event pointer !!! */
            
            prgEvent = (EVENT *) prgItem->text;

            /* Acknowledge the item */
            ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
            
            if (ilRc != RC_SUCCESS) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRc);
            } /* end if */

            switch(prgEvent->command)
            {
                case HSB_STANDBY    :
                    ctrl_sta = prgEvent->command;
                    dbg(TRACE,"PQS: received HSB_STANDBY event!");
                    HandleQueues();
                    break;  
                case HSB_COMING_UP  :
                    ctrl_sta = prgEvent->command;
                    dbg(TRACE,"PQS: received HSB_COMING_UP event!");
                    HandleQueues();
                    break;  
                case HSB_ACTIVE :
                    ctrl_sta = prgEvent->command;
                    dbg(TRACE,"PQS: received HSB_ACTIVE event!");
                    break;  
                case HSB_ACT_TO_SBY :
                    ctrl_sta = prgEvent->command;
                    dbg(TRACE,"PQS: received HSB_ACT_TO_SBY event!");
                    HandleQueues();
                    break;  
                case HSB_DOWN   :
                    /* whole system shutdown - do not further use que(), */
                    /* send_message() or timsch() ! */
                    ctrl_sta = prgEvent->command;
                    dbg(TRACE,"PQS: received HSB_DOWN event!");
                    Terminate(FALSE);
                    break;  
                case HSB_STANDALONE :
                    ctrl_sta = prgEvent->command;
                    dbg(TRACE,"PQS: received HSB_STANDALONE event!");
                    break;  
                case REMOTE_DB :
                    /* ctrl_sta is checked inside */
                    /*HandleRemoteDB(prgEvent);*/
                    break;
                case SHUTDOWN   :
                    /* process shutdown - maybe from uutil */
                    dbg(TRACE,"PQS: received SHUTDOWN event!");
                    Terminate(FALSE);
                    break;
                case RESET      :
                    dbg(TRACE,"PQS: received RESET event!");
                    ilRc = Reset();
                    if (ilRc == RC_FAIL)
                    Terminate(FALSE);
                    break;
                case EVENT_DATA :
                    if ((ctrl_sta == HSB_STANDALONE) ||
                        (ctrl_sta == HSB_ACTIVE) ||
                        (ctrl_sta == HSB_ACT_TO_SBY))
                    {
                        if (bgSocketOpen == TRUE ||
                            strcmp(prgCfg.mode,"TEST") == 0)
                        {
                            dbg(TRACE,"Calling HandleInternalData");
                            ilRc = HandleInternalData();
                            if (ilRc!=RC_SUCCESS)
                            {
                                dbg(TRACE,"PQS: HandleInternalData failed <%d>",ilRc);
                                HandleErr(ilRc);
                            } /* end if HandleInternalData */
                        } /* end if SocketOpen */
                        else
                        {
                            dbg(TRACE,"PQS: No GOS-connection! Ignoring event!");
                        } /* end else SocketOpen */
                    } /* end if HSB status */
                    else
                    {
                        dbg(TRACE,"poll_q_and_sock: wrong HSB-status <%d>",ctrl_sta);
                        DebugPrintItem(TRACE,prgItem);
                        DebugPrintEvent(TRACE,prgEvent);
                    } /* end else HSB status */
                    break;
                case TRACE_ON :
                    dbg_handle_debug(prgEvent->command);
                    break;
                case TRACE_OFF :
                    dbg_handle_debug(prgEvent->command);
                    break;
                default:
                    dbg(TRACE,"MAIN: unknown event");
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        }/* end while */
         
        ilRc = RC_FAIL; /* proceed */

        if (bgSocketOpen == TRUE)
        {
            
            ilRc = Receive_data(igSock,READ_TIMEOUT,"MAIN");
           
            if (ilRc == RC_NOT_FOUND )
                ilRc = SendKeepAliveMsg();
        } /* end if SocketOpen */
        else
        {
            ilRc = OpenServerSocket();
        } /* end else SocketOpen */

        /* now trying to reconnect if there was a failure */
        
        if (ilRc == RC_FAIL)
        {
            CloseTCP(); /* for cleaning up the connections */
            ilRc_Connect = OpenServerSocket();
        } /* end if */
    } while (ilRc != RC_SUCCESS);
    return ilRc;
} /* end of poll_q_and_sock() */

/* ***************************************************************** */
/* The Receive_data routine                                          */
/* Rec data from GOS into global buffer (and into a file )           */
/* ***************************************************************** */
static int Receive_data(int ipSock,int ipTimeOut,char *pcpCalledBy)
{
    int     ilRC = RC_SUCCESS;
    char    pclFunc[] = "Receive_data:";
    int     ilBytes;
    int     ilNo;
    int     ilOffset;
    int     ilTotal;
    int     ilMsgNo = 0;
    char    *pclP;
    char    pclTempBuff[M_BUFF] = "\0";

    memset(pcgRecvBuffer,0x00,sizeof(pcgRecvBuffer));

    /* Read a possible header */

    ilTotal = 0;    
    alarm(ipTimeOut);

    while (ilTotal < igHeaderSize && bgAlarm == FALSE)
    {
        ilNo = read(ipSock,&pcgRecvBuffer[ilTotal],igHeaderSize);
    
        if (ilNo <= 0 && bgAlarm == FALSE)
        {
            /* <0 means failure at read if no alarm appeared, =0 means connection lost*/
        
            dbg(TRACE,"Receive_data: OS-ERROR read(%d) on socket <%d>",ilNo,ipSock);
            dbg(TRACE,"Receive_data: ERRNO (%d)=(%s)",errno,strerror(errno));
            return RC_FAIL;
        } /* end if */
    
        if (ilNo < 0 && bgAlarm == TRUE)
        {
            /* <0 means no data read inside READ_TIMEOUT time */
        
            bgAlarm = FALSE;
            return RC_NOT_FOUND;
        } /* end if */
        ilTotal += ilNo;
    } /* end while */   
    alarm(0);
    bgAlarm = FALSE;
    
    if (ilTotal == 0)
    {
        return RC_NOT_FOUND;
    } /* end if */
    
    dbg(DEBUG,"========================= START ===============================");
    
    dbg(TRACE,"Receive_data: Header read <%s>",&pcgRecvBuffer[0]);
    
    ilOffset = strlen(prgCfg.header_prefix);
    
    if (strncmp(prgCfg.header_prefix,&pcgRecvBuffer[0],ilOffset) != 0)
    {
        dbg(TRACE,"Receive_data: Header prefix <%s> not found in reveived message",prgCfg.header_prefix);
        return RC_FAIL;
    } /* end if */
    
    /* Read the size of the message */
    
    ilBytes = atoi(&pcgRecvBuffer[ilOffset]);
    dbg(TRACE,"Receive_data: Message is <%i> bytes long",ilBytes);
    
    /* Read the remaining message */
    
    ilTotal = 0;
    alarm(ipTimeOut);
    
    while (ilTotal < ilBytes && bgAlarm == FALSE)
    {
        ilNo = read(ipSock,&pcgRecvBuffer[ilTotal + igHeaderSize],ilBytes - ilTotal);
    
        if (ilNo <= 0 && bgAlarm == FALSE)
        {
            /* <0 means failure at read if no alarm appeared, =0 means connection lost*/
        
            dbg(TRACE,"Receive_data (2): OS-ERROR read(%d) on socket <%d>",ilNo,ipSock);
            dbg(TRACE,"Receive_data (2): ERRNO (%d)=(%s)",errno,strerror(errno));
            return RC_FAIL;
        } /* end if */
    
        if (ilNo < 0 && bgAlarm == TRUE)
        {
            /* <0 means no data read inside READ_TIMEOUT time. Fatal at this point */
        
            dbg(TRACE,"Receive_data: Remaining message timeout. Closing socket");
            bgAlarm = FALSE;
            return RC_FAIL;
        } /* end if */
        ilTotal += ilNo;
    } /* end while */   
    alarm(0);
    bgAlarm = FALSE;
    
    /* We successfully read the complete message */
    
    dbg(TRACE,"Complete message: \n %s",pcgRecvBuffer);
    igKeepAliveCounter = 0;   /* Reset since it is active socket */

    /* Do we have special handling for DXB stands */

    if (bgRemoveZero)
    {
        pclP = strstr(pcgRecvBuffer,prgCfg.stand_tag);
        
        if (pclP != 0)
        {
            pclP += igStandTagLen;
            
            /* Check if the stand is of the format "<character><digit>" and length is 2 */
            
            if (isalpha(*pclP) && isdigit(*(pclP+1)) && (*(pclP+2) == '<'))
            {
                /* We got a stand to convert!!! */
                
                strcpy(pclTempBuff,(pclP+1));
                *(pclP+1) = '0';
                strcpy((pclP+2),pclTempBuff);
                
                dbg(TRACE,"Receive_data: A zero was inserted in the stand name");
            } /* end if */
        } /* end if */
    } /* end if (DXB stand name handling) */
    
    /* Queue to the receiver */

    ilRC = SendEvent("XMLI",igModID_Rcvr,3,"",pcgTwStart,pcgTwEnd,
                    "","",&pcgRecvBuffer[igHeaderSize],"",0);
    /* Send the ACK */
    
    if (ilRC == RC_SUCCESS)
        {
            ilMsgNo = GetMsgno(pcgRecvBuffer);      
            SendAckMsg(ilMsgNo);
    } /* end if */
    
    return ilRC;
} /* end of Receive_data() */

/* ***************************************************************** */
/* The OpenServerSocket routine                                      */
/* ***************************************************************** */

int OpenServerSocket()
{
    int ilRC;
    int ilLen;
    static BOOL blAcceptSocketOpen = FALSE;
    static int  ilAcceptSocket;
    
    if (blAcceptSocketOpen)
    {
        /* Socket is open, check for connection request */

        alarm(CONNECT_TIMEOUT);
        ilLen = sizeof(my_client);
        igSock = accept(ilAcceptSocket,(struct sockaddr*)&my_client,&ilLen);
        alarm(0);
        
        if (igSock < 0)
        {
            if (errno == EINTR)
            {
                dbg(DEBUG,"OpenServerSocket: accept timeout: %s:",strerror(errno));
                ilRC = RC_SUCCESS;
            } /* end if */
            else 
            {
                dbg(TRACE,"OpenServerSocket: accept failed: %s:",strerror(errno));
                close(ilAcceptSocket);
                blAcceptSocketOpen = FALSE;
                ilRC = RC_FAIL;
            } /* end else */
        } /* end if */
        else
        {
            bgSocketOpen = TRUE;
            close (ilAcceptSocket);
            blAcceptSocketOpen = FALSE;
            dbg(TRACE,"OpenServerSocket: Client connected");
            ilRC = RC_SUCCESS;
            
            //Frank@pdehdl
            if( rgConnectAct.ilToBeActed == TRUE )
            {
                SendEvent( rgConnectAct.pclSndCmd, rgConnectAct.ilModId, rgConnectAct.ilPrio,
                           "","","", "","","","",0);
                dbg( TRACE,"OpenServerSocket: Send <%s> to <%s><%d>", 
                           rgConnectAct.pclSndCmd, rgConnectAct.pclProcName, rgConnectAct.ilModId );
            }
            //Frank@pdehdl
        } /* end else */
    } /* end if ibAcceptSocketOpen */
    else
    {
        if (bgSocketOpen)
        {
            dbg(TRACE,"OpenServerSocket: Client socket already open");
            ilRC = RC_SUCCESS;
        } /* end if */
        else
        {
            ilAcceptSocket = tcp_create_socket(SOCK_STREAM,prgCfg.service_port);
            
            if (ilAcceptSocket < 0)
            {
                dbg(TRACE,"OpenServerSocket: Create Accept socket failed: %s.",strerror(errno));
                ilRC = RC_FAIL;
            } /* end if */
            else
            {
                listen(ilAcceptSocket,20);
                blAcceptSocketOpen = TRUE;
                dbg(TRACE,"OpenServerSocket: Accept socket opened.");
                ilRC = RC_SUCCESS;
            } /* end else */
        } /* end else bgSocketOpen */
    } /* end else ibAcceptSocketOpen */
    
    return ilRC;
} /* end OpenServerSocket */

/* ***************************************************************** */
/* The CloseTCP routine                                              */
/* ***************************************************************** */

static void CloseTCP()
{
   if (igSock != 0)
   {
        close(igSock);
        dbg(TRACE,"CloseTCP: connection closed!");
   } /* end if */
   igSock = 0;
   bgSocketOpen = FALSE;
} /* end of CloseTCP() */


/* **************************************************************** */
/* The snapit routine                                               */
/* snaps data if the debug-level is set to DEBUG                    */
/* **************************************************************** */

static void snapit(void *pcpBuffer,long lpDataLen,FILE *pcpDbgFile)
{
    if (debug_level==DEBUG)
    snap((char*)pcpBuffer,(long)lpDataLen,(FILE*)pcpDbgFile);
} /* end of snapit() */

/* ******************************************************************** */
/* The GetCfgEntry() routine                                            */
/* ******************************************************************** */

static int GetCfgEntry(char *pcpFile,char *pcpSection,char *pcpEntry,short spType,char **pcpDest,
                       int ipValueType,char *pcpDefVal)
{
    int ilRc = RC_SUCCESS;
    char pclCfgLineBuffer[L_BUFF];

    memset(pclCfgLineBuffer,0x00,L_BUFF);
    
    if ((ilRc=iGetConfigRow(pcpFile,pcpSection,pcpEntry,spType,pclCfgLineBuffer)) != RC_SUCCESS)
    {
        dbg(TRACE,"GetCfgEntry: reading entry <%s> failed.",pcpEntry);
        dbg(TRACE,"GetCfgEntry: EMPTY <%s>! Use default <%s>.",pcpEntry,pcpDefVal);
        strcpy(pclCfgLineBuffer,pcpDefVal);
    } /* end if */
    else
    {
        if (strlen(pclCfgLineBuffer) < 1)
        {
            dbg(TRACE,"GetCfgEntry: EMPTY <%s>! Use default <%s>.",pcpEntry,pcpDefVal);
            strcpy(pclCfgLineBuffer,pcpDefVal);
        } /* end if */
    } /* end else */
    *pcpDest = malloc(strlen(pclCfgLineBuffer)+1);
    strcpy(*pcpDest,pclCfgLineBuffer);
    dbg(TRACE,"GetCfgEntry: %s = <%s>",pcpEntry,*pcpDest);
    
    if ((ilRc = CheckValue(pcpEntry,*pcpDest,ipValueType)) != RC_SUCCESS)
    {
        dbg(TRACE,"GetCfgEntry: please correct value <%s>!",pcpEntry);
    }
    return ilRc;
} /* end of GetCfgEntry() */

/* ******************************************************************** */
/* The CheckValue() routine                                             */
/* Validates configuration values                                       */
/* ******************************************************************** */

static int CheckValue(char *pcpEntry,char *pcpValue,int ipType)
{
    int ilRc = RC_SUCCESS;

    switch(ipType)
    {
        case CFG_NUM:
            while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
            {
                if (isdigit(*pcpValue)==0)
                {
                    dbg(TRACE,"CheckValue : NOT A NUMBER! <%s>!",pcpEntry);
                    ilRc = RC_FAIL;
                } /* end if */
                pcpValue++;
            } /* end while */
            break;
        case CFG_ALPHA:
            while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
            {
                if (isalpha(*pcpValue)==0)
                {
                    dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!",*pcpValue,pcpEntry);
                    ilRc = RC_FAIL;
                } /* end if */
                pcpValue++;
            } /* end while */
            break;
        case CFG_ALPHANUM:
            while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
            {
                if (isalnum(*pcpValue)==0)
                {
                    dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!",*pcpValue,pcpEntry);
                    ilRc = RC_FAIL;
                } /* end if */
                pcpValue++;
            } /* end while */
            break;
        case CFG_PRINT:
            while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
            {
                if (isprint(*pcpValue)==0)
                {
                    dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!",*pcpValue,pcpEntry);
                    ilRc = RC_FAIL;
                } /* end if */
                pcpValue++;
            } /* end while */
            break;
        default:
            break;
    } /* end switch */
      return ilRc;
} /* end of CheckValue() */

/* **************************************************************** */
/* The Send_data routine                                            */
/* Sends telegrams to the GOS-system                                */
/* **************************************************************** */

static int Send_data(int ipSock,char *pcpData)
{
    int ilRc = RC_SUCCESS;
    int ilBytes = 0;
    char pclCurrentTime[32];
    char *pclP;
    char *pclPend;
    char pclStandName[16];
    int ilStandNameLength = 0;
    BOOL blKeepSuffix = FALSE;

    /* ipSock = opened socket */
    /* ilBytes = number of bytes that have to be transmitted. */
    ilBytes = strlen(pcpData);

    /* For DXB GOS interface. Replace FTYP O with and A */
    
    if (strlen(prgCfg.ftyp_keyword) != 0)
    {
        pclP = strstr(pcpData,prgCfg.ftyp_keyword);
        
        if (pclP != 0)
        {
            pclP += strlen(prgCfg.ftyp_keyword);
            
            if ((*pclP == 'O') || (*pclP == 'T'))
            {
                *pclP = 'A';
            } /* end if */
        } /* end if */
    } /* end if */
    
    /* For DXB GOS interface. Remove leading zero in stand name (B01 -> B1) */
    /* BUT, check for standnames where the suffix must be retained first */
    
    pclP = strstr(pcpData,prgCfg.stand_tag);
    
    if (pclP != 0)
    {
        pclP += igStandTagLen;
        pclPend = strstr(pclP,"<");
        
        if (pclPend !=0)
        {
            ilStandNameLength = pclPend - pclP;
            strncpy(pclStandName,pclP,ilStandNameLength);
            pclStandName[ilStandNameLength] = 0x00;
            
            if (strstr(prgCfg.keep_suffix,pclStandName) != 0)
            {
                blKeepSuffix = TRUE;
            } /* end if */
            else
            {
                blKeepSuffix = FALSE;
            } /* end else */
        } /* end if */
    } /* end if */
        
    if (bgRemoveZero)
    {
        pclP = strstr(pcpData,prgCfg.stand_tag);
        
        if (pclP != 0)
        {
            pclP += (igStandTagLen + 1);
            
            if (*pclP == '0')
            {
                strcpy(pclP,pclP+1);
                ilBytes--;
            } /* end if */
        } /* end if */
    } /* end if */

    if (bgRemoveSuffix && (blKeepSuffix == FALSE))
    {       
        pclP = strstr(pcpData,prgCfg.stand_tag);
                
        if (pclP != 0)
        {
            pclP += igStandTagLen;
            pclPend = strstr(pclP,"<");
        
            if (pclPend != 0)
            {
                pclPend--;
            
                if (isalpha(*pclPend))
                {
                    /* Strip the suffix */
                    strcpy(pclPend,pclPend+1);
                    ilBytes--;
                } /* end if */
            } /* end if */
        } /* end if */
    } /* end if */


    dbg(TRACE,"Building header. Bytes required <%i>", ilBytes);
    dbg(TRACE,"Header info: <%s> <%i>",prgCfg.header_prefix,(igHeaderSize - strlen(prgCfg.header_prefix)));

    /* Build header */
    if (ilBytes > 0)
    {
        sprintf(pcgSendBuffer,"%s%-*d",prgCfg.header_prefix,igHeaderSize - strlen(prgCfg.header_prefix),ilBytes);
    } /* end if */
    else
    {
        dbg(TRACE,"Send_data: Invalid message size (no data) <%i>",ilBytes);
        return RC_FAIL;
    } /* end else */
    
    strcat(pcgSendBuffer,pcpData);

    if (ipSock != 0)
    {
        errno = 0;
        alarm(WRITE_TIMEOUT);
        ilRc = write(ipSock,pcgSendBuffer,ilBytes + igHeaderSize);
        alarm(0);

        if (ilRc == -1)
        {
            if (bgAlarm == FALSE)
            {
                dbg(TRACE,"Send_data: Write failed: Socket <%d>! <%d>=<%s>",
                    ipSock,errno,strerror(errno));
                ilRc = RC_FAIL;
            } /* end if */
        else
        {
           bgAlarm = FALSE;
           ilRc = RC_FAIL;
        }
        CloseTCP();
     }
     else
     {
        dbg(DEBUG,"Send_data: wrote <%d> Bytes to socket Nr.<%d>.",ilRc,ipSock);
        snapit((char *)pcgSendBuffer,ilBytes,outp);
        fflush(outp);
        if (pgSendLogFile)
        {
           fwrite(pcgSendBuffer,sizeof(char),ilBytes,pgSendLogFile);
           fwrite("\n",sizeof(char),1,pgSendLogFile);
           fflush(pgSendLogFile);
        }
        ilRc = RC_SUCCESS;
        igKeepAliveCounter = 0;   /* Reset since it is active socket */
     }
  }
  else
  {
     if (strcmp(prgCfg.mode,"TEST") == 0)
     {
        dbg(DEBUG,"Send_data: wrote <%d> Bytes to dummy-socket.",ilBytes);
        snapit((char *)pcgSendBuffer,ilBytes,outp);
        fflush(outp);
        if (pgSendLogFile)
        {
           fwrite(pcgSendBuffer,sizeof(char),ilBytes,pgSendLogFile);
           fwrite("\n",sizeof(char),1,pgSendLogFile);
           fflush(pgSendLogFile);
        }
        ilRc = RC_SUCCESS;
     }
     else
     {
        dbg(TRACE,"Send_data: No connection to <%s>! Can't send!",pcgGOS_Host);
        ilRc = RC_FAIL;
     }
  }
  return ilRc;
} /* end of Send_data */

/* **************************************************************** */
/* The Send_KeepAlive routine                                       */
/* To be implemented in the future                                  */
/* MEI: 21-MAR-2012
/* **************************************************************** */

static int SendKeepAliveMsg()
{
    int ilRC;
    char *pclFunc = "SendKeepAliveMsg";

    if( igKeepAlive && igSock > 0 )
    {
        igKeepAliveCounter += READ_TIMEOUT;
        if( igKeepAliveCounter < igKeepAlive )
            return RC_SUCCESS;
        dbg(DEBUG,"========================= START <%s> ===============================", pclFunc );
        igKeepAliveCounter = 0;
        ilRC = Send_data(igSock,pcgKeepAliveFormat);
        dbg(DEBUG,"========================= END <%s> ================================", pclFunc );
    }
    return ilRC;
} /* end of SendKeepAliveMsg() */

/* **************************************************************** */
/* The Send Ack routine                                             */
/* Acknowledge a message from the peer                              */
/* **************************************************************** */

static int SendAckMsg(int ipMsgNo)
{
    int ilRC = RC_SUCCESS;
    char pclFunc[] = "SendAckMsg:";
    char pclDataBuf[M_BUFF];

    dbg(DEBUG,"========================= START ===============================");
    dbg(DEBUG,"%s Send Ack Msg",pclFunc);
    sprintf(pclDataBuf,pcgSendAckFormat,ipMsgNo);
    ilRC = Send_data(igSock,pclDataBuf);
    dbg(DEBUG,"========================= END ================================");

    return ilRC;
} /* End of SendAckMsg */

/* **************************************************************** */
/* The Get Msgno routine                                            */
/* Extracts the message number from a message based on a keyword    */
/* **************************************************************** */

static int GetMsgno(char *pcpMsg)
{
    int     ilMsgNo = -1;
    char    *pclP = NULL;
    int     ilLoop;
    BOOL    blFound = FALSE;

    /* First find the keyword */
    
    pclP = strstr(pcpMsg,prgCfg.msgno_keyword);
    
    if (pclP == NULL)
    {
        dbg(TRACE,"GetMsgno: Message number keyword not found: <%s>",prgCfg.msgno_keyword);
        ilMsgNo = -1;
    }
    else
        /* Search for the first digit */
        
        pclP += strlen(prgCfg.msgno_keyword);
        
        for (ilLoop = 0; ilLoop < 16 && blFound == FALSE; ilLoop++)
        {
            pclP+= ilLoop;
            
            if (isdigit(*pclP))
            {
                /* Found a digit, translate sequence to integer */
                
                ilMsgNo = atoi(pclP);
                blFound = TRUE;
            } /* end if */
        } /* end for */
                

    return ilMsgNo;
} /* End of GetMsgno() */
