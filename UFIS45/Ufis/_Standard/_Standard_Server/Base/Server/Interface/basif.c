#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Interface/basif.c 1.2 2004/10/04 19:10:17SGT jim Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  RKL                                                      */
/* Date           :  16.06.00                                                 */
/* Description    :  basif - Building Automation System Interface             */
/*                                                                            */
/* History :  16.06.00  RKL Written                                           */
/*            04.07.00  RKL Insert Ceda-Event handling                        */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
/* 20041004 JIM: replaced sccs_version by mks_version to allow compilation */
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "helpful.h"
#include "send.h"
#include "netin.h"
#include "tcputil.h"
#include "buffer2.h"
#include "basif.h"

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */      
/*FILE *outp       = NULL;*/
int  debug_level = DEBUG;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;

static int   igWaitTime	   = 0;
static BOOL  bgBIG_ENDIAN  = FALSE;		/* global Flag for BIG_ENDIAN Byteorder */
static BOOL  bgAlarm = FALSE;			/* global flag for timed-out socket-read */

static char  cgConfigFile[512];

static time_t tgRecvTimeOut	 = 0;		/* time until to basif waits for a valid telegram */
static time_t tgNextConnect	 = 0;		/* time for trying to establish a con. to bas*/

static char pcgHomeAp[20];  			/* buffer for name of the home-airport */
static char pcgTabEnd[20];  			/* buffer for name of the table extension */

static char pcgRecvName[64] ="";		/* BC-Head used as WKS-Name */
static char pcgDestName[64] ="";		/* BC-Head used as UserName */
static char pcgTwStart[64] ="";			/* BC-Head used as Stamp    */
static char pcgTwEnd[64] ="";			/* BC-Head used as Stamp    */

static BASMain rgCFG;

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int 	Init_basif();
static int	Reset(void);                       /* Reset program          */
static void	Terminate(int); 				          /* Terminate program      */
static void	HandleSignal(int);                 /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void	HandleQueErr(int);                 /* Handles queuing errors */
static int	HandleData(void);                  /* Handles event data     */
static void 	HandleQueues(void);                /* Waiting for Sts.-switch*/

static int OpenConnectionBAS (void);
static void CloseConnectionBAS (void);
static int Send_Data(int,int,char *,int);
static int Receive_Data(int,int ,int,char*);
static int SetRegisterBit(int,int,WORD*);
static int HandleAnswer(BASMain *, char *, char *,char *, char*);

static int Set_Output(BASTcpInfo *,WORD *);
static int Reset_AllOUT(BASTcpInfo *);
static int Read_Register(BASTcpInfo *,WORD *,WORD *);
static int PrintRegister(WORD);
static int HandleBASCommand(BASMain *, char *);
static int BASSetCmd(BASMain *, int);
static int HandleInputChanged(BASMain *, WORD ,WORD);
static int ConvertData(WORD *, int);
static int TestByteOrder(BOOL *);

/******************************************************************************/
/* Buffer Function prototypes	                                                */
/******************************************************************************/
static int CreateBuffer(BUFFER*,int,int);
static int CopyToBuffer(BUFFER*,char*);
static int AppendBuffer(BUFFER*,char*);
static int ClearBuffer (BUFFER*);
static int FreeTheBuffer  (BUFFER*);

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	int	ilCnt = 0;
	WORD	ilInRegister = 0;
	WORD	ilOutRegister = 0;
	WORD    ilLastPeriodInRegister = 0;
	WORD    ilRegisterBitChanged = 0;

	/* Handles Signal */
	(void)SetSignals(HandleSignal);
	
	INITIALIZE;			/* General initialization	*/
	dbg(TRACE,"MAIN: version <%s>",mks_version);
	/* Attach to the MIKE queues */
	do
	{
		ilRC = init_que();
		if(ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRC != RC_SUCCESS));
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}else{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */

	sprintf(cgConfigFile,"%s/basif",getenv("BIN_PATH"));
	ilRC = TransferFile(cgConfigFile);
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */
	/* uncomment if necessary */
	/* sprintf(cgConfigFile,"%s/basif.cfg",getenv("CFG_PATH")); */
	/* ilRC = TransferFile(cgConfigFile); */
	/* if(ilRC != RC_SUCCESS) */
	/* { */
	/* 	dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile); */
	/* } */ /* end of if */
	ilRC = SendRemoteShutdown(mod_id);
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */
	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */
	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		if(igInitOK == FALSE)
		{
			ilRC = Init_basif();
			if(ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"Init_basif: init failed!-> Terminating !!");
				Terminate(60);
			} /* end of if */
		}/* end of if */
	} else {
		Terminate(0);
	}/* end of if */
	dbg(TRACE,"MAIN: initializing OK");
	for(;;)
	{
		ilRC = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRC == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRC != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRC);
			} /* fi */
			
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnectionBAS(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(0);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;

				break;	

			case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
				Terminate(0);
				break;
					
			case	RESET		:
				ilRC = Reset();
				break;
					
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRC = HandleData();
					if(ilRC != RC_SUCCESS)
					{
						HandleErr(ilRC);
					}/* end of if */
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRC);
		} /* end else */

		/* Check all inputs */
		if((ilRC = Read_Register(&rgCFG.rInfo, &ilInRegister, &ilOutRegister)) == RC_SUCCESS)
		{
			ilRegisterBitChanged = 0;

			ilRegisterBitChanged = ilLastPeriodInRegister;
			ilRegisterBitChanged ^= ilInRegister;

			if(ilRegisterBitChanged > 0)
			{
				/* one or more of Inputs changed */
				dbg(TRACE,"MAIN: Input <0x%04x> changed",ilRegisterBitChanged);
				
				ilRC = HandleInputChanged(&rgCFG,ilInRegister,ilRegisterBitChanged);
				
			}

			ilLastPeriodInRegister = ilInRegister;
			
		} 

		sleep(igWaitTime);
		
	} /* end for */
	
	exit(0);
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_basif()
{
	char	*pclFct="Init_basif";
	int	ilRC = RC_SUCCESS;			/* Return code */
	int	ilTmpInt = -1;
	int 	ilTmpOut = -1;
	int	ilTmpOutVal = -1;
	int	ilCurCmd = 0;
	int	ilCurOut = 0;
	int 	ilAnzOut = 0;
	int	ilAnzVal = 0;

	/* char	*pclCfgPath = NULL; */

	char	*pclCfgPath = "/etc";

	char	*pclS = NULL;
	char	pclCfgFile[iMIN_BUF_SIZE];
	char	pclAllCommands[2*iMAX_BUF_SIZE];
	char	pclTmpBuf[iMAX_BUF_SIZE];
	char	pclTmpOut[iMAX_BUF_SIZE];
	char	pclTmpOutVal[iMAX_BUF_SIZE];
	char	pclCedaEventSection[iMAX_BUF_SIZE];

	strcpy(pcgDestName,"BASIF");
	strcpy(pcgRecvName,"EXCO");

	/* Testing Byteorder BIG_ENDIAN Modell */ 
	ilRC = TestByteOrder(&bgBIG_ENDIAN);

        /* reading default home-airport from sgs.tab */
	memset(pcgHomeAp,0x00,sizeof(pcgHomeAp));
	ilRC = tool_search_exco_data("SYS","HOMEAP",pcgHomeAp);
	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"%s: No HOMEAP entry in sgs.tab: EXTAB! Please add!",pclFct);
		return RC_FAIL;
	}
	else
	{
		dbg(TRACE,"%s: HOMEAP = <%s>",pclFct, pcgHomeAp);
	}
	
	/* reading default table-extension from sgs.tab */
	memset(pcgTabEnd,0x00,sizeof(pcgTabEnd));
	ilRC = tool_search_exco_data("ALL","TABEND",pcgTabEnd);
	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"%s: No TABEND entry in sgs.tab: EXTAB! Please add!",pclFct);
		return RC_FAIL;
	}
	else
	{
		dbg(TRACE,"%s: TABEND = <%s>",pclFct,pcgTabEnd);
	}

	/* now reading from configfile or from database */
	dbg(DEBUG,"%s: ---- START INIT ----",pclFct);

/*
	if ((pclCfgPath = getenv("CFG_PATH")) == NULL)
	{
		dbg(TRACE,"%s: ERROR: missing environment CFG_PATH...", pclFct);
		dbg(DEBUG,"%s: ---- END ----", pclFct);
		return RC_FAIL;
	}
*/


	strcpy(pclCfgFile, pclCfgPath);
	if (pclCfgFile[strlen(pclCfgFile)-1] != '/')
		strcat(pclCfgFile, "/");
	strcat(pclCfgFile, "basif.cfg");
	dbg(DEBUG,"%s: CFG-File: <%s>", pclFct, pclCfgFile);

	/* -------------------------------------------------------------- */
	/* Start reading ConfigFile  -------------------------------------*/
	/* -------------------------------------------------------------- */


	/* -------------------------------------------------------------- */
	/* Reading hostname  -------------------------------------------- */
	/* -------------------------------------------------------------- */
	pclTmpBuf[0] = 0x00;
	if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "hostname", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
	{
		dbg(TRACE,"%s: ERROR - reading hostname in section MAIN...", pclFct);
		return RC_FAIL;
	}
	StringUPR((UCHAR*)pclTmpBuf);
        strcpy(rgCFG.rInfo.cHostname,pclTmpBuf);
	dbg(DEBUG,"%s: hostname    : <%s>", pclFct, rgCFG.rInfo.cHostname);

	/* -------------------------------------------------------------- */
	/* Reading Servicename  ----------------------------------------- */
	/* -------------------------------------------------------------- */
        pclTmpBuf[0] = 0x00;
	if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "servicename", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
	{
		dbg(TRACE,"%s: ERROR - reading servicename in section MAIN...", pclFct);
		return RC_FAIL;
	}
	StringUPR((UCHAR*)pclTmpBuf);
        strcpy(rgCFG.rInfo.cServicename,pclTmpBuf);
	dbg(DEBUG,"%s: Servivename : <%s>", pclFct, rgCFG.rInfo.cServicename);

	/* -------------------------------------------------------------- */
	/* Reading Recv_Timeout  ---------------------------------------- */
	/* -------------------------------------------------------------- */
        pclTmpBuf[0] = 0x00;
	if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "recv_timeout", CFG_INT, (char*)&rgCFG.rInfo.iRecvTimeout)) != RC_SUCCESS)
	{
		dbg(TRACE,"%s: ERROR - reading recv_timeout in section MAIN... ", pclFct);
		dbg(TRACE,"%s:         setting default recv_timeout <60 sec>", pclFct);
		/* Setting Default 60 Sec. */
		rgCFG.rInfo.iRecvTimeout = 60;
		
	}
	dbg(DEBUG,"%s: recv_timeout: <%d>", pclFct, rgCFG.rInfo.iRecvTimeout);

	/* -------------------------------------------------------------- */
	/* Reading general wait_time  ----------------------------------- */
	/* -------------------------------------------------------------- */
        pclTmpBuf[0] = 0x00;
	if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "wait_time", CFG_INT, (char*)&igWaitTime)) != RC_SUCCESS)
	{
		dbg(TRACE,"%s: ERROR - reading wait_time in section MAIN...", pclFct);
		dbg(TRACE,"%s:         setting default wait_time <1 sec>", pclFct);
		/* Setting default 1 Sec */
		igWaitTime = 1;
		
	}
	dbg(DEBUG,"%s: wait_time   : <%d>", pclFct, igWaitTime);

	/* -------------------------------------------------------------- */
	/* reading debug_level  ----------------------------------------- */
	/* -------------------------------------------------------------- */
	pclTmpBuf[0] = 0x00;
	if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "debug_level", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
	{
		dbg(TRACE,"%s: Warning - missing debug_level in section MAIN...", pclFct);
		dbg(TRACE,"%s: ...       Setting to default TRACE", pclFct);
		strcpy(pclTmpBuf,"TRACE");
	}
	/* which Type? */
	StringUPR((UCHAR*)pclTmpBuf);
	if (!strcmp(pclTmpBuf,"TRACE"))
		debug_level = TRACE;
	else if (!strcmp(pclTmpBuf,"DEBUG"))
		debug_level = DEBUG;
	else if (!strcmp(pclTmpBuf,"OFF"))
		debug_level = 0;
	else
	{
		dbg(TRACE,"%s: ERROR - unknown debug_level <%s> in section <%s>",
		    pclFct, pclTmpBuf, rgCFG.prCmds[ilCurCmd].cCommand);
		dbg(TRACE,"%s: ... Setting to default TRACE", pclFct);
		strcpy(pclTmpBuf,"TRACE");
		debug_level = TRACE;
		
	}
	dbg(DEBUG,"%s: debug_level : <%s>", pclFct, pclTmpBuf);



	/* -------------------------------------------------------------- */
	/* reading commands  -------------------------------------------- */
	/* -------------------------------------------------------------- */
	memset((void*)pclAllCommands, 0x00, 2*iMAX_BUF_SIZE);
	if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "commands", CFG_STRING, pclAllCommands)) != RC_SUCCESS)
	{
		dbg(TRACE,"%s: missing commands in section MAIN...", pclFct);
	}
	else
	{
		StringUPR((UCHAR*)pclAllCommands);

		if ((rgCFG.iNoOfCmds = GetNoOfElements(pclAllCommands, cCOMMA)) <= 0)
		{
			dbg(TRACE,"%s: missing commands in section MAIN...",pclFct);
		}
		else
		{
			dbg(TRACE,"%s: found %d commands in MAIN section", pclFct, rgCFG.iNoOfCmds);
			/* get memory */
			if ((rgCFG.prCmds = (BASCmds*)malloc(rgCFG.iNoOfCmds*sizeof(BASCmds))) == NULL)
			{
				/* nix speicher, nix schaffe */
				dbg(TRACE,"%s: ERROR: malloc failure...",pclFct);
				dbg(DEBUG,"%s: ---- END ----",pclFct);
				return RC_FAIL;
			}
			memset((void*)rgCFG.prCmds, 0x00, rgCFG.iNoOfCmds*sizeof(BASCmds));

			/*==========================================*/
			/* read all commands and corresponding data */
			/*==========================================*/
			for (ilCurCmd = 0; ilCurCmd<rgCFG.iNoOfCmds; ilCurCmd++)
			{
				strcpy(rgCFG.prCmds[ilCurCmd].cCommand, GetDataField(pclAllCommands, ilCurCmd, cCOMMA));
				dbg(TRACE,"%s: ---- START <%s> ----", pclFct, rgCFG.prCmds[ilCurCmd].cCommand);

				/* Setting Setion to Valid */
				rgCFG.prCmds[ilCurCmd].iValid = TRUE;				

				/* -------------------------------------------------------------- */
				/* Reading Type of Section   ------------------------------------ */
				/* -------------------------------------------------------------- */
				pclTmpBuf[0] = 0x00;
				if ((ilRC = iGetConfigEntry(pclCfgFile, rgCFG.prCmds[ilCurCmd].cCommand, "type", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
				{
					/* default */
					dbg(TRACE,"%s: missing Type in section <%s>", pclFct, rgCFG.prCmds[ilCurCmd].cCommand);
					rgCFG.prCmds[ilCurCmd].iValid = FALSE;
				}

				/* which Type? */
				StringUPR((UCHAR*)pclTmpBuf);
				if (!strcmp(pclTmpBuf,"CHECK"))
					rgCFG.prCmds[ilCurCmd].iType = TYPE_CHECK;
				else if (!strcmp(pclTmpBuf,"SET"))
					rgCFG.prCmds[ilCurCmd].iType = TYPE_SET;
				else
				{
					dbg(TRACE,"%s: unknown Type <%s> in section <%s> Valid Types [SET|CHECK]",
										 pclFct, pclTmpBuf, rgCFG.prCmds[ilCurCmd].cCommand);
					rgCFG.prCmds[ilCurCmd].iValid = FALSE;
					
				}
				dbg(DEBUG,"%s: Type: <%s>", pclFct, pclTmpBuf);

				/* -------------------------------------------------------------- */
				/* Reading Logical Name of Section   ------------------------------------ */
				/* -------------------------------------------------------------- */
				pclTmpBuf[0] = 0x00;
				if ((ilRC = iGetConfigEntry(pclCfgFile, rgCFG.prCmds[ilCurCmd].cCommand, "logical_name", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
				{
					/* default = Command */
					strcpy(pclTmpBuf,rgCFG.prCmds[ilCurCmd].cCommand);
					dbg(TRACE,"%s: setting logical_name to command in section <%s>", pclFct, rgCFG.prCmds[ilCurCmd].cCommand);
			
				}

				/* which Logical Name? */
				StringUPR((UCHAR*)pclTmpBuf);
				strcpy(rgCFG.prCmds[ilCurCmd].cLogicalName,pclTmpBuf);
				dbg(TRACE,"%s: logical_name set to <%s>",pclFct,rgCFG.prCmds[ilCurCmd].cLogicalName);

				if((rgCFG.prCmds[ilCurCmd].iType == TYPE_SET) && (rgCFG.prCmds[ilCurCmd].iValid == TRUE))
				{
					/* -------------------------------------------------------------- */
					/* Reading Configuration for Sectiontype SET_OUTPUT  ------------ */
					/* -------------------------------------------------------------- */

					/* -------------------------------------------------------------- */
					/* Reading OUTPUT Number for Section                 ------------ */
					/* -------------------------------------------------------------- */
					pclTmpOut[0] = 0x00;
					if ((ilRC = iGetConfigEntry(pclCfgFile, rgCFG.prCmds[ilCurCmd].cCommand, "output_number", CFG_STRING, pclTmpOut)) != RC_SUCCESS)
					{
						dbg(TRACE,"%s: missing output_number in section <%s>", pclFct, rgCFG.prCmds[ilCurCmd].cCommand);
						rgCFG.prCmds[ilCurCmd].iValid = FALSE;
					}
					strcpy(rgCFG.prCmds[ilCurCmd].cOutputNr,pclTmpOut);
				
					if ((ilAnzOut = GetNoOfElements(pclTmpOut, cCOMMA)) <= 0)
					{
						dbg(TRACE,"%s: ERROR- GetNoOfElements for output_number failed in section <%s>",pclFct, rgCFG.prCmds[ilCurCmd].cCommand);
					}
					else
					{
						dbg(TRACE,"%s: found %d Output Numbers in section <%s>", pclFct, ilAnzOut, rgCFG.prCmds[ilCurCmd].cCommand);
						dbg(DEBUG,"%s: output_number: <%s>", pclFct, rgCFG.prCmds[ilCurCmd].cOutputNr);
						/* -------------------------------------------------------------- */
						/* Reading OUTPUT Values for Section                 ------------ */
						/* -------------------------------------------------------------- */
						pclTmpOutVal[0] = 0x00;
						if ((ilRC = iGetConfigEntry(pclCfgFile, rgCFG.prCmds[ilCurCmd].cCommand, "output_value", CFG_STRING, pclTmpOutVal)) != RC_SUCCESS)
						{
							dbg(TRACE,"%s: missing output_value in section <%s>", pclFct, rgCFG.prCmds[ilCurCmd].cCommand);
							rgCFG.prCmds[ilCurCmd].iValid = FALSE;
						}
						strcpy(rgCFG.prCmds[ilCurCmd].cOutputValue,pclTmpOutVal);
				
						if ((ilAnzVal = GetNoOfElements(pclTmpOutVal, cCOMMA)) <= 0)
						{
							dbg(TRACE,"%s: ERROR- GetNoOfElements for output_value failed in section <%s>",pclFct, rgCFG.prCmds[ilCurCmd].cCommand);
						}
						else
						{
							dbg(TRACE,"%s: found %d Output Values in section <%s>", pclFct, ilAnzVal, rgCFG.prCmds[ilCurCmd].cCommand);
							dbg(DEBUG,"%s: output_value: <%s>", pclFct, rgCFG.prCmds[ilCurCmd].cOutputValue);
							if (ilAnzOut != ilAnzVal)
							{
								dbg(TRACE,"%s: ERROR - different Number of Output and Values in section <%s>", pclFct, rgCFG.prCmds[ilCurCmd].cCommand);
								rgCFG.prCmds[ilCurCmd].iValid = FALSE;
							}
						}



						
						rgCFG.prCmds[ilCurCmd].iOutRegisterBit   = 0;
						rgCFG.prCmds[ilCurCmd].iOutRegisterValue = 0;      
						for (ilCurOut = 0; ilCurOut<ilAnzOut; ilCurOut++)
						{
							ilTmpOut    = atoi( GetDataField(pclTmpOut    , ilCurOut, cCOMMA));
							ilTmpOutVal = atoi( GetDataField(pclTmpOutVal , ilCurOut, cCOMMA));

							/* Check Output Number is valid */
							if((ilTmpOut < 0) && (ilTmpOut >= 12))
							{
								dbg(TRACE,"%s: ERROR - No valid Outputnumber in section <%s>", pclFct, rgCFG.prCmds[ilCurCmd].cCommand);
								dbg(TRACE,"%s:         Valid Values 0 to 11", pclFct);
								rgCFG.prCmds[ilCurCmd].iValid = FALSE;
	
							}
							else
							{

								/* Set Bit in OutRegister */
								if((ilRC = SetRegisterBit(ilTmpOut,1,(WORD*)&rgCFG.prCmds[ilCurCmd].iOutRegisterBit)) != RC_SUCCESS)
								{
									dbg(TRACE,"%s: ERROR - SetRegisterBit failed for Output_number <%d> in section <%s>", 
										   pclFct, ilTmpOut,rgCFG.prCmds[ilCurCmd].cCommand);
									rgCFG.prCmds[ilCurCmd].iValid = FALSE;
									ilRC = RC_FAIL;
								}
							}

							/* Check Valid Output Value */
							if((ilTmpOutVal < 0) || (ilTmpOutVal > 1))
							{
								dbg(TRACE,"%s: ERROR - <%d> No valid Outputvalue in section <%s>", pclFct, rgCFG.prCmds[ilCurCmd].cCommand);
								dbg(TRACE,"%s:         Valid Values 0 or 1", pclFct);
								rgCFG.prCmds[ilCurCmd].iValid = FALSE;
							}
							else
							{

								/* Set Bit in OutRegisterValue */
								if((ilRC = SetRegisterBit(ilTmpOut,ilTmpOutVal,(WORD*)&rgCFG.prCmds[ilCurCmd].iOutRegisterValue)) != RC_SUCCESS)
								{
									dbg(TRACE,"%s: ERROR - SetRegisterBit failed for output_number <%d> in section <%s>", 
										   pclFct, ilTmpOut,rgCFG.prCmds[ilCurCmd].cCommand);
									rgCFG.prCmds[ilCurCmd].iValid = FALSE;
									ilRC = RC_FAIL;
								}
	
							}


						} /* end of for */
					 
					dbg(TRACE,"%s: OutRegisterBit   set to <0x%04x>",pclFct,rgCFG.prCmds[ilCurCmd].iOutRegisterBit);
					ilRC = PrintRegister(rgCFG.prCmds[ilCurCmd].iOutRegisterBit);
					dbg(TRACE,"%s: OutRegisterValue set to <0x%04x>",pclFct,rgCFG.prCmds[ilCurCmd].iOutRegisterValue);
					ilRC = PrintRegister(rgCFG.prCmds[ilCurCmd].iOutRegisterValue);
					}

				}
				else if((rgCFG.prCmds[ilCurCmd].iType == TYPE_CHECK) && (rgCFG.prCmds[ilCurCmd].iValid == TRUE))
				{
					/* -------------------------------------------------------------- */
					/* Reading Configuration for Sectiontype TYPE_CHECK  ------------ */
					/* -------------------------------------------------------------- */
					/* -------------------------------------------------------------- */
					/* Reading Input Number for Section  ---------------------------- */
					/* -------------------------------------------------------------- */
					ilTmpInt = -1;
					pclTmpBuf[0] = 0x00;
					if ((ilRC = iGetConfigEntry(pclCfgFile, rgCFG.prCmds[ilCurCmd].cCommand, "input_number", CFG_INT, (char*)&ilTmpInt)) != RC_SUCCESS)
					{
						if ((ilRC = iGetConfigEntry(pclCfgFile, rgCFG.prCmds[ilCurCmd].cCommand, "ceda_event", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
						{
							dbg(TRACE,"%s: you must be configure input_number or ceda_event in section <%s>", pclFct, rgCFG.prCmds[ilCurCmd].cCommand);
							rgCFG.prCmds[ilCurCmd].iValid = FALSE;
						}
						
					}
					else
					{
						/* Check Input Number is valid */
						if((ilTmpInt >= 0) && (ilTmpInt < 12))
						{
							rgCFG.prCmds[ilCurCmd].iInputNr = ilTmpInt;
						}
						else
						{
							dbg(TRACE,"%s: ERROR - No valid Input in section <%s>", pclFct, rgCFG.prCmds[ilCurCmd].cCommand);
							dbg(TRACE,"%s:         Valid Values 0 to 11", pclFct);
							rgCFG.prCmds[ilCurCmd].iValid = FALSE;

						}
						dbg(TRACE,"%s: InputNumber: <%d>", pclFct, rgCFG.prCmds[ilCurCmd].iInputNr);
					
						/* Set Bit in INRegister */
						rgCFG.prCmds[ilCurCmd].iInRegisterBit = 0;
						if((ilRC = SetRegisterBit(rgCFG.prCmds[ilCurCmd].iInputNr,1,(WORD*)&rgCFG.prCmds[ilCurCmd].iInRegisterBit)) != RC_SUCCESS)
						{
							rgCFG.prCmds[ilCurCmd].iValid = FALSE;
							ilRC = RC_FAIL;
						}
						dbg(TRACE,"%s: InRegisterBit set to <0x%04x>",pclFct,rgCFG.prCmds[ilCurCmd].iInRegisterBit);
						ilRC = PrintRegister(rgCFG.prCmds[ilCurCmd].iInRegisterBit);
					}

					/* -------------------------------------------------------------- */
					/* Reading Command to send of Section   ------------------------- */
					/* -------------------------------------------------------------- */
					rgCFG.prCmds[ilCurCmd].cCommandSend[0] = 0x00;
					if ((ilRC = iGetConfigEntry(pclCfgFile, rgCFG.prCmds[ilCurCmd].cCommand, "command_send", CFG_STRING, rgCFG.prCmds[ilCurCmd].cCommandSend)) != RC_SUCCESS)
					{
						if ((ilRC = iGetConfigEntry(pclCfgFile, rgCFG.prCmds[ilCurCmd].cCommand, "ceda_event", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
						{
							dbg(TRACE,"%s: ERROR - you must be configure command_send or ceda_event in section <%s>", pclFct, rgCFG.prCmds[ilCurCmd].cCommand);
							rgCFG.prCmds[ilCurCmd].iValid = FALSE;
						}
						else
						{
							/* no command_send needed */
							rgCFG.prCmds[ilCurCmd].cCommandSend[0] = 0x00;
						}
					}

					dbg(TRACE,"%s: command_send: <%s>", pclFct, rgCFG.prCmds[ilCurCmd].cCommandSend);
					/* -------------------------------------------------------------- */
					/* Reading Mod-Id for Section                        ------------ */
					/* -------------------------------------------------------------- */
					rgCFG.prCmds[ilCurCmd].iModId = -1;
					if ((ilRC = iGetConfigEntry(pclCfgFile, rgCFG.prCmds[ilCurCmd].cCommand, "mod_id", CFG_INT, (char*)&rgCFG.prCmds[ilCurCmd].iModId)) != RC_SUCCESS)
					{
						dbg(TRACE,"%s: ERROR - reading mod_id in section <%s>", pclFct, rgCFG.prCmds[ilCurCmd].cCommand);
						rgCFG.prCmds[ilCurCmd].iValid = FALSE;
						return RC_FAIL;
					}

					dbg(TRACE,"%s: mod_id : <%d>", pclFct, rgCFG.prCmds[ilCurCmd].iModId);

	
					/* -------------------------------------------------------------- */
					/* Reading Ceda-Event Data for Section               ------------ */
					/* -------------------------------------------------------------- */
					pclCedaEventSection[0] = 0x00;
					if ((ilRC = iGetConfigEntry(pclCfgFile, rgCFG.prCmds[ilCurCmd].cCommand, "ceda_event", CFG_STRING, pclCedaEventSection)) != RC_SUCCESS)
					{
						dbg(TRACE,"%s: There is no ceda_event section in <%s>", pclFct, rgCFG.prCmds[ilCurCmd].cCommand);
						rgCFG.prCmds[ilCurCmd].prCedaEvent = NULL;
						ilRC = RC_SUCCESS;
					}
					else
					{
						dbg(TRACE,"%s: There is a ceda_event <%s> in section <%s> ...", pclFct,pclCedaEventSection,rgCFG.prCmds[ilCurCmd].cCommand);
						dbg(TRACE,"%s: ... Reading ceda_event <%s>", pclFct,pclCedaEventSection);

						if ((rgCFG.prCmds[ilCurCmd].prCedaEvent = (BASCedaEvent*)malloc(sizeof(BASCedaEvent))) == NULL)
						{
							/* nix speicher, nix schaffe */
							dbg(TRACE,"%s: ERROR: malloc failure...",pclFct);
							dbg(DEBUG,"%s: ---- END ----",pclFct);
							return RC_FAIL;
						}
						memset((void*)rgCFG.prCmds[ilCurCmd].prCedaEvent, 0x00,sizeof(BASCedaEvent));


						/* -------------------------------------------------------------- */
						/* Reading Command of Event------------------------------------ */
						/* -------------------------------------------------------------- */
						pclTmpBuf[0] = 0x00;
						if ((ilRC = iGetConfigEntry(pclCfgFile, pclCedaEventSection, "command", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
						{
							dbg(TRACE,"%s: Error - There is no command in section <%s>",pclFct, pclCedaEventSection);
							rgCFG.prCmds[ilCurCmd].prCedaEvent->cCommand[0] = 0x00;
							rgCFG.prCmds[ilCurCmd].iValid = FALSE;
							return RC_FAIL;
			
						}
						else
						{
							/* which Command? */
							StringUPR((UCHAR*)pclTmpBuf);
							strcpy(rgCFG.prCmds[ilCurCmd].prCedaEvent->cCommand,pclTmpBuf);
						}
						dbg(TRACE,"%s: Command   set to <%s>",pclFct,rgCFG.prCmds[ilCurCmd].prCedaEvent->cCommand);

						/* -------------------------------------------------------------- */
						/* Reading Tablename of Event------------------------------------ */
						/* -------------------------------------------------------------- */
						pclTmpBuf[0] = 0x00;
						if ((ilRC = iGetConfigEntry(pclCfgFile, pclCedaEventSection, "tablename", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
						{
							dbg(TRACE,"%s: There is no tablename in section <%s>",pclFct, pclCedaEventSection);
							rgCFG.prCmds[ilCurCmd].prCedaEvent->cTablename[0] = 0x00;
			
						}
						else
						{
							/* which Tablename? */
							StringUPR((UCHAR*)pclTmpBuf);
							strcpy(rgCFG.prCmds[ilCurCmd].prCedaEvent->cTablename,pclTmpBuf);
						}
						dbg(TRACE,"%s: Tablename set to <%s>",pclFct,rgCFG.prCmds[ilCurCmd].prCedaEvent->cTablename);

						/* -------------------------------------------------------------- */
						/* Reading Selection of Event  ---------------------------------- */
						/* -------------------------------------------------------------- */
						pclTmpBuf[0] = 0x00;
						if ((ilRC = iGetConfigRow(pclCfgFile, pclCedaEventSection, "selection", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
						{
							dbg(TRACE,"%s: There is no selection in section <%s>",pclFct,pclCedaEventSection);
							rgCFG.prCmds[ilCurCmd].prCedaEvent->cSelection[0] = 0x00;
			
						}
						else
						{
							/* which Selection ? */
							strcpy(rgCFG.prCmds[ilCurCmd].prCedaEvent->cSelection,pclTmpBuf);
						}
						dbg(TRACE,"%s: Selection set to <%s>",pclFct,rgCFG.prCmds[ilCurCmd].prCedaEvent->cSelection);

						/* -------------------------------------------------------------- */
						/* Reading null-Condition of Event  ----------------------------- */
						/* -------------------------------------------------------------- */
						pclTmpBuf[0] = 0x00;
						if ((ilRC = iGetConfigRow(pclCfgFile, pclCedaEventSection, "null", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
						{
							dbg(TRACE,"%s: There are no null in section <%s>", pclCedaEventSection);
							rgCFG.prCmds[ilCurCmd].prCedaEvent->cNull[0] = 0x00;
							ilRC = RC_SUCCESS;
			
						}
						else
						{
							/* which section ? */
							strcpy(rgCFG.prCmds[ilCurCmd].prCedaEvent->cNull,pclTmpBuf);
						}
						dbg(TRACE,"%s: Null      set to <%s>",pclFct,rgCFG.prCmds[ilCurCmd].prCedaEvent->cNull);

						/* -------------------------------------------------------------- */
						/* Reading NotNull of Event  ------------------------------------ */
						/* -------------------------------------------------------------- */
						pclTmpBuf[0] = 0x00;
						if ((ilRC = iGetConfigRow(pclCfgFile, pclCedaEventSection, "not_null", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
						{
							dbg(TRACE,"%s: There are no not_null in section <%s>", pclFct,pclCedaEventSection);
							rgCFG.prCmds[ilCurCmd].prCedaEvent->cNotNull[0] = 0x00;
			
						}
						else
						{
							/* which data ? */
							strcpy(rgCFG.prCmds[ilCurCmd].prCedaEvent->cNotNull,pclTmpBuf);
						}
						dbg(TRACE,"%s: NotNull   set to <%s>",pclFct,rgCFG.prCmds[ilCurCmd].prCedaEvent->cNotNull);

					}


				} 
				else 
				{
					dbg(TRACE,"%s: ERROR - No Valid Type in section <%s>", pclFct, rgCFG.prCmds[ilCurCmd].cCommand);
					return RC_FAIL;
				}
			

				dbg(TRACE,"%s: Section <%s> valid set to <%d>", pclFct, rgCFG.prCmds[ilCurCmd].cCommand,rgCFG.prCmds[ilCurCmd].iValid);
				dbg(TRACE,"%s: ---- END   <%s> ----", pclFct, rgCFG.prCmds[ilCurCmd].cCommand);
			} /* end of for */
		}
	}

	/* if there is an Init-Section set the Outputs of the COM-SRV */
	for(ilCurCmd=0;ilCurCmd<rgCFG.iNoOfCmds;ilCurCmd++)
	{
		if((strcmp(rgCFG.prCmds[ilCurCmd].cLogicalName,"INIT")) == 0)
		{
			dbg(TRACE,"%s: Found INIT-Section -> setting outputs",pclFct);
			if((ilRC = HandleBASCommand(&rgCFG,rgCFG.prCmds[ilCurCmd].cLogicalName)) == RC_FAIL)
			{
				dbg(TRACE,"%s: ERROR - HANDLE COMMAND <%s>", 
				           pclFct,rgCFG.prCmds[ilCurCmd].cLogicalName);
			}
			break;
		}
	}

/* Check all Section are valid */
/* to do */

	igInitOK = TRUE;

	return(ilRC);
	
} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRC = RC_SUCCESS;				/* Return code */
	int	ilCmd ;

	dbg(TRACE,"Reset: now resetting");
	
	/* Free the CedaEvent Memory */
	for(ilCmd=0; ilCmd < rgCFG.iNoOfCmds; ilCmd++)
	{
		/* delete event memory */
		if (rgCFG.prCmds[ilCmd].prCedaEvent != NULL)
		{
			free(rgCFG.prCmds[ilCmd].prCedaEvent);
			rgCFG.prCmds[ilCmd].prCedaEvent = NULL;
		}
	} /* end for */		
	
	/* delete structure memory */
	if (rgCFG.prCmds != NULL)
	{
		free(rgCFG.prCmds);
		rgCFG.prCmds = NULL;
	}

	/* re-init the process... */
	igInitOK = FALSE;
	ilRC = Init_basif();
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"<Reset> Init_basif: init failed!");
		Terminate(45);
	} 

	dbg(TRACE,"<Reset> finished");
	return ilRC;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	int ilRc = 0;
	if (ipSleep > 0)
	{
		dbg(TRACE,"Terminate: sleeping %d sec. before terminating.",ipSleep);
		sleep(ipSleep);
	}

	if (igInitOK == TRUE)
	{
		CloseConnectionBAS();
		dbg(TRACE,"Terminate: TCP/IP-connections closed.");
	}

	dbg(TRACE,"Terminate: now leaving ...");
	
	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int ipSig)
{
	int	ilRC = RC_SUCCESS;			/* Return code */

	switch(ipSig)
	{
		case SIGTERM:
			dbg(TRACE,"HandleSignal: Received Signal <%d>(SIGTERM).",ipSig);
			Terminate(0);
			break;
		case SIGALRM:
			dbg(DEBUG,"HandleSignal: Received Signal<%d>(SIGALRM)",ipSig);
			bgAlarm = TRUE;
			break;

		default	:
	    dbg(TRACE,"HandleSignal: signal <%d> received",ipSig);
			Terminate(60);
			break;
	} /* end of switch */

	
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	int	ilRC = RC_SUCCESS;
	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	int	ilRC = RC_SUCCESS;
	
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		/* dbg(TRACE,"<%d> : no messages on queue",pipErr);*/
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	
		if( ilRC == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRC != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRC);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(0);
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;

				ilBreakOut = TRUE;
				break;	

			case	SHUTDOWN	:
				Terminate(0);
				break;
						
			case	RESET		:
				ilRC = Reset();
				break;
						
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRC);
		} /* end else */
	} while (ilBreakOut == FALSE);
	if(igInitOK == FALSE)
	{
			ilRC = Init_basif();
			if(ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"Init_basif: init failed!");
			} /* end of if */
	}/* end of if */
	/* OpenConnection(); */
} /* end of HandleQueues */
	

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData()
{
	char *pclFct="HandleData";
	int    ilRC = RC_SUCCESS;			/* Return code */
	int ilQueId = 0;
	int ilNoAck = FALSE;
	int ilTmpDbgLev = 0;

	char pclActCmd[16] = "";
	char pclTable[16];

	char *pclHlpPtr = NULL;
	WORD ilInRegister  = 0;
	WORD ilOutRegister = 0;

	BC_HEAD *prlBchd = NULL;
	CMDBLK  *prlCmdblk = NULL;
	static BUFFER prlSelect = { 0,0,0,NULL };
	static BUFFER prlFields = { 0,0,0,NULL };
	static BUFFER prlData   = { 0,0,0,NULL };

	ilRC = CreateBuffer(&prlSelect,1024,1024);
	ilRC = CreateBuffer(&prlFields,1024,1024);
	ilRC = CreateBuffer(&prlData  ,1024,1024);


  dbg(TRACE,"%s: ========== BASIF EVENT BEGIN ==========", pclFct );

  /* Queue-id of event sender */
  ilQueId = prgEvent->originator; 

  /* get broadcast header */
  prlBchd = (BC_HEAD *)((char *)prgEvent + sizeof(EVENT));

  /* Save Global Elements of BC_HEAD */
  memset(pcgRecvName, '\0', (sizeof(prlBchd->recv_name)+1)); /* Workstation */
  strncpy(pcgRecvName, prlBchd->recv_name, sizeof(prlBchd->recv_name));
  strcpy(pcgDestName,prlBchd->dest_name);                    /* UserLoginName */
 
  /* get command block  */
  prlCmdblk = (CMDBLK *)((char *)prlBchd->data);

  /* Save Global Elements of CMDBLK */
  strcpy(pcgTwStart,prlCmdblk->tw_start);
  strcpy(pcgTwEnd,prlCmdblk->tw_end);
 
  /*  3 Strings in CMDBLK -> data  */

  /*  GET SELECTION */  
  pclHlpPtr = (char *)prlCmdblk->data;
  ilRC = CopyToBuffer(&prlSelect,pclHlpPtr);
  
  /*  GET FIELDS */
  pclHlpPtr = pclHlpPtr + strlen(prlSelect.data) + 1;
  ilRC = CopyToBuffer(&prlFields,pclHlpPtr);
  
  /*  GET DATA */
  pclHlpPtr = (char *)pclHlpPtr + strlen(prlFields.data) + 1;
  ilRC = CopyToBuffer(&prlData,pclHlpPtr);


  strcpy (pclTable, prlCmdblk->obj_name);                    /*  table name  */
  strcpy (pclActCmd, prlCmdblk->command);                /*  actual command  */


  dbg(TRACE,"%s: CMD <%s> TBL <%s>", pclFct,prlCmdblk->command,prlCmdblk->obj_name);
  dbg(TRACE,"%s: FROM <%d> WKS <%s> USR <%s>",pclFct,
             ilQueId,prlBchd->recv_name,prlBchd->dest_name);

  strncpy(pcgTwStart, prlCmdblk->tw_start, sizeof(prlCmdblk->tw_start));
  pcgTwEnd[sizeof(prlCmdblk->tw_start)] = 0x00;
  strncpy(pcgTwEnd, prlCmdblk->tw_end, sizeof(prlCmdblk->tw_end));
  pcgTwEnd[sizeof(prlCmdblk->tw_end)] = 0x00;
  dbg(TRACE,"%s: SPECIAL INFO TWS <%s> TWE <%s>",pclFct,pcgTwStart,pcgTwEnd);
  /* dbg(TRACE,"%s: SEL <%s>",pclFct,prlSelect.data);*/
  /* dbg(TRACE,"%s: FLD <%s>",pclFct,prlFields.data);*/
  /* dbg(TRACE,"%s: DAT <%s>",pclFct,prlData.data);*/

  /*  get acknowlege status  */
  if (prlBchd->rc == NETOUT_NO_ACK)
  {
     ilNoAck = TRUE;
  } /* end if */
  else 
  {
     ilNoAck = FALSE;
  } /* end else */
 

  /*  GO TO FUNCTION REPRESENTS INTERFACENAME  */
  if (strcmp(pclActCmd, "RESET") == 0 ) 
  {
     ilRC = Reset_AllOUT((BASTcpInfo*)&rgCFG.rInfo);
  } /* end else if */
  else if ((strcmp(pclActCmd, "RDREG") == 0 ) || (strcmp(pclActCmd, "READ_REGISTER") == 0 ))
  {
	ilRC = Read_Register(&rgCFG.rInfo, &ilInRegister, &ilOutRegister);
	ilTmpDbgLev = debug_level;
	debug_level = TRACE;
	dbg(TRACE,"%s: Input  Registervalue <0x%04x>",pclFct,ilInRegister);
	ilRC = PrintRegister(ilInRegister);

	dbg(TRACE,"%s: Output Registervalue <0x%04x>",pclFct,ilOutRegister);
	ilRC = PrintRegister(ilOutRegister);
	debug_level = ilTmpDbgLev;

  } /* end else if */
  else if (strstr(",RT,",pclActCmd) != NULL )  /* Answer from ... */ 
  {
	if((ilRC = HandleAnswer(&rgCFG, pcgTwStart,prlSelect.data,prlFields.data,prlData.data)) == RC_FAIL)
	{
		dbg(TRACE,"%s: ERROR - HANDLE Answer from %d ", 
		pclFct, ilQueId);
	}
	
  } /* end else if */
  else if (strcmp(pclActCmd, "XBAS") == 0 ) 
  {
	if((ilRC = HandleBASCommand(&rgCFG, prlFields.data)) == RC_FAIL)
	{
		dbg(TRACE,"%s: ERROR - HANDLE COMMAND XBAS<%s> from %d ", 
		pclFct,prlFields.data, ilQueId);
	}
	
  } /* end else if */
  else
  {
     if((ilRC = HandleBASCommand(&rgCFG, pclActCmd)) == RC_FAIL)
     {
       dbg(TRACE,"%s: ERROR - HANDLE COMMAND <%s> from %d ", 
                 pclFct,pclActCmd, ilQueId);
     }
     

  } /* end else */ 


  dbg(TRACE,"%s: ========== BASIF EVENT READY ==========", pclFct);
		
	return ilRC;
	
} /* end of HandleData */



/*****************************************************************************/ 
/* Parameter:                                                                */
/* In    : int   ipBitNo    - Bit number to set                              */
/* In    : int   ipValue    - Value 0/1 of Bit                               */
/* Out   : WORD  ipRegister - 16-Bit Register                                */
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description:   Setting Bit ipBitNo to ipValue(0/1) in 16-Bit Register     */
/*                (ipBitNo from 0 to 15)                                     */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*           17.06.00  rkl  Written                                          */
/*                                                                           */
/*****************************************************************************/
static int SetRegisterBit(int ipBitNo, int ipValue, WORD *ipRegister)
{
	char *pclFct  ="SetRegisterBit";
	int    ilRC  = RC_SUCCESS;
  

	 
	 dbg(DEBUG,"%s: Parameter: ipBitNo <%d> ipValue <%d> ipRegister <0x%04x>",pclFct,ipBitNo,ipValue,*ipRegister);
	 

	if((ipBitNo >= 0) || (ipBitNo <=15))
	{


		if(ipValue == 1)
		{
			*ipRegister |= ((0x0001) << ipBitNo);
		}
		else if (ipValue== 0)
		{
			*ipRegister &= ( ~((0x0001) << ipBitNo));
		}
		else
		{
			dbg(TRACE,"%s: ERROR - Invalid BitValue <%d> valid [0/1]",pclFct,ipValue); 
			ilRC = RC_FAIL;
		}
	}
	else
	{
		dbg(TRACE,"%s: ERROR - Invalid Bit Number <%d> valid [0 ... 15]",pclFct,ipBitNo); 

		ilRC = RC_FAIL;
	}

  
	 dbg(DEBUG,"%s: Registervalue  set to <0x%04x>",pclFct, *ipRegister);
	 
	return ilRC;
} /* end of SetRegisterBit() */




/*****************************************************************************/ 
/* Parameter:                                                                */
/* In    : BASTcpInfo  *prpInfo    - Pointer to Tcp Info Structure           */
/* Out   : WORD        *rpRegister - Register Values                         */
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description:  Opens TCP-Connection to "EXCO_BAS" and send REG_REQUEST     */
/*               and Read the answer and return the Registervalues from      */
/*               Input                                                       */
/*                                                                           */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*           18.06.00  rkl  Written                                          */
/*                                                                           */
/*****************************************************************************/
static int Read_Register(BASTcpInfo *prpInfo, WORD *rpInRegister, WORD *rpOutRegister)
{
	char *pclFct  ="Read_Register";
	int    ilRC  = RC_SUCCESS;


	REQUEST		rlRegReq;    /* Structure for Request */
        STATE_REG	rlRegState;  /* Structure of Register State */

	/* Connection to BAS */
	if ((ilRC = OpenConnectionBAS()) != RC_SUCCESS)
	{
		dbg(TRACE,"%s: ERROR - No Connection to BAS",pclFct);
		prpInfo->iConnect = FALSE;
		return RC_FAIL;
	}
	else
	{
		/* Building Packet Register Request to BAS COM-Server*/
		rlRegReq.head.sendseq = 0;
		rlRegReq.head.recvseq = 0;
		rlRegReq.head.type   = REG_REQUEST;
		rlRegReq.head.length = sizeof(REQUEST);

		/* Send_Data to BAS COM-SRV */
		/* dbg(DEBUG,"%s: Send Request for Register State",pclFct);*/
		
		
		ilRC = ConvertData((WORD*) &rlRegReq,sizeof(REQUEST));

		if ((ilRC = Send_Data(prpInfo->iSocket, prpInfo->iRecvTimeout,(char *)&rlRegReq,sizeof(REQUEST))) != RC_SUCCESS)
		{
			dbg(TRACE,"%s: ERROR - can't send Data ", pclFct);
			CloseConnectionBAS();
			ilRC = RC_FAIL;
		}
	}        

	if(ilRC == RC_SUCCESS)
	{
		/* Receive Data from BAS COM-SRV */
		if ((ilRC = Receive_Data(prpInfo->iSocket, prpInfo->iRecvTimeout ,sizeof(STATE_REG),(char *)&rlRegState)) != RC_SUCCESS)
		{
			dbg(TRACE,"%s: ERROR - can't read Data ", pclFct);
			CloseConnectionBAS();
			ilRC = RC_FAIL;
		}
		else
		{
			ilRC = ConvertData((WORD*) &rlRegState,sizeof(STATE_REG));

			/* dbg(DEBUG,"%s: Received Register State",pclFct);*/
			/* dbg(DEBUG,"%s: head: sendseq <%d> recvseq <%d> type <0x%04x> length <%d>",pclFct,*/
			/*		       rlRegState.head.sendseq,rlRegState.head.recvseq,rlRegState.head.type,rlRegState.head.length);*/
			/* dbg(DEBUG,"%s: Data: driver_id<%d> input_reg <0x%04x> output_reg <0x%04x>",pclFct,*/
		/*		       rlRegState.driver_id,rlRegState.input_reg,rlRegState.output_reg);*/
			if(rlRegState.head.type != REG_STATE)
			{
				dbg(TRACE,"%s: ERROR - wrong type <%d> for REG_STATE",pclFct,rlRegState.head.type);
				ilRC = RC_FAIL;
			}
			else
			{
				*rpInRegister  = rlRegState.input_reg;
				*rpOutRegister = rlRegState.output_reg;
				ilRC = RC_SUCCESS;
			}

		}

	
	}


	


	return ilRC;

} /* end of Read_Register() */


/*****************************************************************************/ 
/* Parameter:                                                                */
/* In    : BASMain  *prpCFG        - Pointer to Cfg                          */
/* In    : WORD      ipInRegister  - InRegister                              */	
/* In    : WORD      ipRegisterChg - Register  Changed                       */	
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description:                                                              */
/*                                                                           */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*           26.01.00  rkl  Written                                          */
/*                                                                           */
/*****************************************************************************/
static int HandleInputChanged(BASMain *prpCFG, WORD ipInRegister, WORD ipRegisterChanged)
{
	char *pclFct  ="HandleInputChanged";
	int    ilRC  = RC_SUCCESS;
	int ilCmd;
	int ilFound = FALSE;
	char pclDataValue[8] = "";

	/* Search in Configuration for Command */
	for(ilCmd=0; ilCmd < prpCFG->iNoOfCmds; ilCmd++)
	{
		/* Check InputRegisterBit in Configuration and Changed Register are the same */

		if((ipRegisterChanged & prpCFG->prCmds[ilCmd].iInRegisterBit) &&
		   ((prpCFG->prCmds[ilCmd].iType == TYPE_CHECK)) &&
		   (prpCFG->prCmds[ilCmd].iValid == TRUE)) 
		{
			dbg(TRACE,"%s: Match Input Changed - LogicalName <%s>",pclFct,prpCFG->prCmds[ilCmd].cLogicalName);
			ilFound = TRUE;

			if( prpCFG->prCmds[ilCmd].iValid == FALSE)
			{
				dbg(TRACE,"%s: ERROR - Section <%s> is not valid - check Configuration",
					pclFct,prpCFG->prCmds[ilCmd].cCommand);
				ilRC = RC_FAIL;
			}
			else
			{	
				/* Fill the structure */
				/* check input value */
				if(prpCFG->prCmds[ilCmd].iInRegisterBit & ipInRegister)
                	          strcpy(pclDataValue,"1");
				else
	                          strcpy(pclDataValue,"0");


				/* Send Command to destination with Mod-ID */
				dbg(DEBUG,"%s: Send message To ...",pclFct);
				dbg(DEBUG,"%s: QUE: <%d> DestName: <%s> RecvName:<%s>",
		  			pclFct ,prpCFG->prCmds[ilCmd].iModId,pcgDestName,pcgRecvName);
				dbg(DEBUG,"%s: TWS: <%s> TWE: <%s>", pclFct,pcgTwStart, pcgTwEnd);
				dbg(DEBUG,"%s: CMD: <%s> TBL: <%s> SEL: <%s>",
					pclFct, prpCFG->prCmds[ilCmd].cCommandSend,"","");
				dbg(DEBUG,"%s: FLD: <%s> DATA: <%s>",
					pclFct,prpCFG->prCmds[ilCmd].cLogicalName, pclDataValue);


		   		ilRC = SendCedaEvent(prpCFG->prCmds[ilCmd].iModId, 0, pcgDestName, pcgRecvName,
        		                     pcgTwStart, pcgTwEnd, prpCFG->prCmds[ilCmd].cCommandSend,"",
                		             "",prpCFG->prCmds[ilCmd].cLogicalName, pclDataValue, "",0,ilRC);
                            
 				if(ilRC == RC_FAIL)
				{
					dbg(TRACE,"%s:ERROR - Function SendCedaEvent failed <%d>",pclFct,ilRC);
    				} /* end if */
    
			}    


		}
	

        

	} /* end of for */

	if(ilFound == FALSE)
	{
		dbg(TRACE,"%s: ERROR - Command for Changed Registerbit not found",pclFct);
		ilRC = RC_FAIL;
	}
 
	return ilRC;

} /* end of HandleInputChanged() */



/*****************************************************************************/ 
/* Parameter:                                                                */
/* In    : BASTcpInfo  *prpInfo    - Pointer to Tcp Info Structure           */
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description:  Opens TCP-Connection to XBAS and send REG_REQUEST           */
/*               to Reset All Output  on BAS COM-Server                      */
/*                                                                           */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*           18.06.00  rkl  Written                                          */
/*                                                                           */
/*****************************************************************************/
static int Reset_AllOUT(BASTcpInfo *prpInfo)
{
	char *pclFct  ="Reset_AllOUT";
	int    ilRC  = RC_SUCCESS;


	REQUEST		rlRegReq;    /* Structure for Register Request */
	SET_OUT 	rlSetOut;

	if ((ilRC = OpenConnectionBAS()) != RC_SUCCESS)
	{
		dbg(TRACE,"%s: ERROR - No Connection to BAS",pclFct);
		return RC_FAIL;
	}
	else
	{
		/* Building Structure for Resetting to BAS COM-Server*/
		rlSetOut.head.sendseq = 0;
		rlSetOut.head.recvseq = 0;
		rlSetOut.head.type   = SET_BIT;
		rlSetOut.head.length = sizeof(SET_OUT);
		rlSetOut.set_bits = 0xffff;
		rlSetOut.value    = 0x0000;

		/* Send_Data to BAS COM-SRV */
		ilRC = ConvertData((WORD*) &rlSetOut,sizeof(SET_OUT));

		dbg(DEBUG,"%s: Send Data for Reset all Output",pclFct);
		if ((ilRC = Send_Data(prpInfo->iSocket, prpInfo->iRecvTimeout,(char *)&rlSetOut,sizeof(SET_OUT))) != RC_SUCCESS)
		{
			dbg(TRACE,"%s: ERROR - can't send Data ", pclFct);
			CloseConnectionBAS();
			ilRC = RC_FAIL;
		}
	}        

	return ilRC;

} /* reset_AllOUT() */

/*****************************************************************************/ 
/* Parameter:                                                                */
/* In    : BASMain  *prpCFG     - Pointer to Cfg                             */
/* In    : char     *pcpActCmd  - pointer to Actual Command                  */	
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description:                                                              */
/*                                                                           */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*           26.01.00  rkl  Written                                          */
/*                                                                           */
/*****************************************************************************/
static int HandleBASCommand(BASMain *prpCFG, char *pcpActCmd)
{
	char *pclFct  ="HandleBASCommand";
	int    ilRC  = RC_SUCCESS;
	int ilCmd;
	int ilFound = FALSE;


	/* Search in Configuration for LogicalName(Command) */
	for(ilCmd=0; ilCmd < prpCFG->iNoOfCmds; ilCmd++)
	{
		if((strcmp(pcpActCmd,prpCFG->prCmds[ilCmd].cLogicalName)) == 0)
		{
			dbg(TRACE,"%s: Logical Name <%s> found in Configuration",pclFct,pcpActCmd);
			ilFound = TRUE;
      			break;
		}
	
	} /* end of for */

	if(ilFound == FALSE)
	{
		dbg(TRACE,"%s: ERROR - Locical Name <%s> not found in Configuration",pclFct,pcpActCmd);
		ilRC = RC_FAIL;
	}
	else
	{
		if( prpCFG->prCmds[ilCmd].iValid == FALSE)
		{
			dbg(TRACE,"%s: ERROR - Section <%s> is not valid - check Configuration",
				pclFct,prpCFG->prCmds[ilCmd].cCommand);
			ilRC = RC_FAIL;
		}
		else
		{
			/* Type ==  ? */
			if (prpCFG->prCmds[ilCmd].iType == TYPE_SET)
			{
				dbg(TRACE,"%s: calling BASSetCmd(%s) ",pclFct,prpCFG->prCmds[ilCmd].cLogicalName);
				if((ilRC = BASSetCmd(prpCFG,ilCmd)) != RC_SUCCESS)
				{
					dbg(TRACE,"%s: ERROR - BASSetCmd() failed",pclFct);
					ilRC = RC_FAIL;
				}
			}
			else if (prpCFG->prCmds[ilCmd].iType == TYPE_CHECK)
			{
				dbg(TRACE,"%s: Check for Ceda_event ",pclFct);
				if(prpCFG->prCmds[ilCmd].prCedaEvent == NULL)
				{
					dbg(TRACE,"%s: ERROR - No Ceda_event found ",pclFct);

				}
				else
				{ 
									
					/* Send Command to destination with Mod-ID */
					dbg(TRACE,"%s: Send message To ...",pclFct);
					dbg(TRACE,"%s: QUE: <%d> DestName: <%s> RecvName:<%s>",
		  				pclFct ,prpCFG->prCmds[ilCmd].iModId,"EXCO","BASIF");
					dbg(TRACE,"%s: TWS: <%s> TWE: <%s>", pclFct,prpCFG->prCmds[ilCmd].cLogicalName, pcgTwEnd);
					dbg(TRACE,"%s: CMD: <%s>  TBL: <%s>",pclFct,
							prpCFG->prCmds[ilCmd].prCedaEvent->cCommand,
							prpCFG->prCmds[ilCmd].prCedaEvent->cTablename);
					dbg(TRACE,"%s:\nSEL : <%s>\nFLD : <%s>\nDATA: <%s>",pclFct,
							prpCFG->prCmds[ilCmd].prCedaEvent->cSelection,
							"COUNT(*)","");


			   		ilRC = SendCedaEvent(prpCFG->prCmds[ilCmd].iModId, 0, "EXCO", "BASIF",
							prpCFG->prCmds[ilCmd].cLogicalName, pcgTwEnd, 
							prpCFG->prCmds[ilCmd].prCedaEvent->cCommand,
							prpCFG->prCmds[ilCmd].prCedaEvent->cTablename,
							prpCFG->prCmds[ilCmd].prCedaEvent->cSelection,
							"COUNT(*)","", "",0,ilRC);
                            
 					if(ilRC == RC_FAIL)
					{
						dbg(TRACE,"%s:ERROR - Function SendCedaEvent failed <%d>",pclFct,ilRC);
    				} /* end if */
    




				}

			}
			else
			{
				dbg(TRACE,"%s: ERROR - Command <%s> Unkown Type ",pclFct,
				          prpCFG->prCmds[ilCmd].cCommand);
				ilRC = RC_FAIL;
			}
		}

	}        
 
	return ilRC;

} /* end of HandleBASCommand() */


/*****************************************************************************/ 
/* Parameter:                                                                */
/* In    : BASMain *prpCFG  - Pointer to Configuration                       */
/* In    : int   ipCmd    - Command Number                                   */	
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description:                                                              */
/*                                                                           */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*           26.01.00  rkl  Written                                          */
/*                                                                           */
/*****************************************************************************/
static int BASSetCmd(BASMain *prpCFG, int ipCmd)
{
	char *pclFct  ="BASSetCmd";
	int    ilRC  = RC_SUCCESS;

       	REQUEST		rlRegReq;    /* Structure for Register Request */
	SET_OUT 	rlSetOut;


	/* Connection to BAS */
	if ((ilRC = OpenConnectionBAS()) != RC_SUCCESS)
	{
		dbg(TRACE,"%s: ERROR - No Connection to BAS",pclFct);
		return RC_FAIL;
	}
	else
	{
		/* Building Packet for setting Output on BAS COM-Server*/
		rlSetOut.head.sendseq= 0;
		rlSetOut.head.recvseq= 0;
		rlSetOut.head.type   = SET_BIT;
		rlSetOut.head.length = 12;
		rlSetOut.set_bits = prpCFG->prCmds[ipCmd].iOutRegisterBit;
		rlSetOut.value    = prpCFG->prCmds[ipCmd].iOutRegisterValue;

		/* Send_Data to BAS COM-SRV */

		ilRC = ConvertData((WORD*) &rlSetOut,sizeof(SET_OUT));

		if ((ilRC = Send_Data(prpCFG->rInfo.iSocket, prpCFG->rInfo.iRecvTimeout,(char *)&rlSetOut,sizeof(SET_OUT))) != RC_SUCCESS)
		{
			dbg(TRACE,"%s: ERROR - can't send Data ", pclFct);
			CloseConnectionBAS();
			ilRC = RC_FAIL;
		}

	}        
 
	return ilRC;

} /* end of BASSetCmd() */

/*****************************************************************************/ 
/* Parameter:                                                                */
/* In    : WORD     pipRegister - 16-Bit Register to Print                   */	
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description:                                                              */
/*                                                                           */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*           26.01.00  rkl  Written                                          */
/*                                                                           */
/*****************************************************************************/
static int PrintRegister(WORD pipRegister)
{
	char *pclFct  ="PrintRegister";
	char *pclBitStrg = "%s: Bit  |0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|";
	char *pclBitFmt  = "%s: Value|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%2d|%2d|%2d|%2d|%2d|%2d|";
	int    ilRC  = RC_SUCCESS;
	int i;
	int ilTmpDbgLev = 0;
	WORD ilHlpReg = 0;
	int alBitValue[16];



	for(i=0;i<16;i++)
	{
		ilHlpReg = pipRegister;
		ilHlpReg = ilHlpReg >> i;
		if( (ilHlpReg & 0x0001) )
			alBitValue[i] = 1;
		else
			alBitValue[i] = 0; 
	}

	ilTmpDbgLev = debug_level;
	dbg(TRACE,pclBitStrg,pclFct);
	dbg(TRACE,pclBitFmt,pclFct,
			alBitValue[0],alBitValue[1],alBitValue[2],alBitValue[3],
			alBitValue[4],alBitValue[5],alBitValue[6],alBitValue[7],
			alBitValue[8],alBitValue[9],alBitValue[10],alBitValue[11],
			alBitValue[12],alBitValue[13],alBitValue[14],alBitValue[15]);
	
	debug_level = ilTmpDbgLev;
	
	return ilRC;

} /* end of PrintRegister() */

/* ****************************************************************** */
/* The OpenConnectionBAS routine                                      */
/* ****************************************************************** */
static int OpenConnectionBAS (void)
{
	char *pclFct = "OpenConnectionBAS";
  	int	ilRC = RC_FAIL;	/*Return code*/

	if ((rgCFG.rInfo.iConnect==FALSE) && (rgCFG.rInfo.iSocket == 0))
	{
		/* try to establish the connection */
		if (rgCFG.rInfo.cHostname != 0x00)  
		{
	
			rgCFG.rInfo.iSocket = tcp_create_socket(SOCK_STREAM, NULL); /* create the socket*/
			if (rgCFG.rInfo.iSocket < 0)
			{
				dbg(TRACE,"%s: ERROR - Create socket failed: %s.",pclFct,strerror(errno));
				ilRC = RC_FAIL;
			}
			else
			{
				dbg(DEBUG,"%s: Create socket <ID=%d> successfull.",pclFct,rgCFG.rInfo.iSocket);
				alarm(rgCFG.rInfo.iRecvTimeout);
				ilRC = tcp_open_connection(rgCFG.rInfo.iSocket,rgCFG.rInfo.cServicename,rgCFG.rInfo.cHostname);
				alarm(0);
				if ((ilRC != RC_SUCCESS) ||(bgAlarm == TRUE))
				{
					dbg(TRACE,"%s: ERROR - connect to <%s> failed: <%s> ",pclFct,
							rgCFG.rInfo.cHostname,strerror(errno)); 
					shutdown(rgCFG.rInfo.iSocket,2);
					close(rgCFG.rInfo.iSocket);

					bgAlarm = FALSE;	
					rgCFG.rInfo.iSocket  = 0;
					rgCFG.rInfo.iConnect = FALSE;
					ilRC = RC_FAIL;
				}
				else
				{
					rgCFG.rInfo.iConnect = TRUE;
				
					ilRC = RC_SUCCESS;
					dbg(TRACE,"%s: open connection to <%s><%d> successfull.",pclFct,
							rgCFG.rInfo.cHostname); 
				}
			}
		}
		else
		{
			dbg(TRACE,"%s: ERROR - wrong hostname <%s> entry!",rgCFG.rInfo.cHostname);
		}
	}
	else
	{
		dbg(DEBUG,"%s: Already connected",pclFct);
		ilRC = RC_SUCCESS;
	}

	dbg(DEBUG,"%s: return <%d>",pclFct,ilRC);
	return ilRC;
} 





/* ***************************************************************** */
/* The CloseConnectionBAS routine                                    */
/* ***************************************************************** */
static void CloseConnectionBAS(void)
{
	char *pclFct = "CloseConnectionBAS";


	dbg(TRACE,"%s: closing TCP/IP connection to BAS !",pclFct);

	if (rgCFG.rInfo.iSocket != 0)
	{
		shutdown(rgCFG.rInfo.iSocket,2);
		close(rgCFG.rInfo.iSocket);
	}
	rgCFG.rInfo.iSocket = 0;
	rgCFG.rInfo.iConnect = FALSE;

} /* end of CloseConnectionBAS() */

/* **************************************************************** */
/* The Send_data routine                                            */
/* Sends Data to the BAS-system                                     */
/* **************************************************************** */
static int Send_Data(int ipSock,int ipTimeout,char *pcpData,int ipLen)
{
	char *pclFct = "Send_Data";
  	int ilRC=RC_SUCCESS;
  	int ilBytes = 0;
		
 	/* ipSock = opened socket */
	/* ilBytes = number of bytes that have to be transmitted. */
	ilBytes = ipLen;
	if (ipSock != 0)
	{
		errno = 0;
		alarm(ipTimeout);
		ilRC = write(ipSock,pcpData,ilBytes);/*unix*/
		alarm(0);
		if (ilRC == -1)
		{
			if (bgAlarm == FALSE)
			{
				dbg(TRACE,"%s: Write failed: Socket <%d>! <%d>=<%s>"
					,pclFct,ipSock,errno,strerror(errno)); 
				ilRC = RC_FAIL;
			}
			else
			{
				dbg(TRACE,"%s: ERROR - Timeout :write failed: Socket <%d>! <%d>=<%s>",
					        pclFct,ipSock,errno,strerror(errno)); 
				bgAlarm = FALSE;
				ilRC = RC_FAIL;
			}
		}
		else
		{
			dbg(DEBUG,"%s: wrote <%d> Bytes to socket Nr.<%d>.",pclFct,ilRC,ipSock); 
			ilRC = RC_SUCCESS;
		}
	}

	return ilRC;
} /* end of Send_Data() */ 

/* ***************************************************************** */
/* The Receive_data routine                                          */
/* Recv data from BAS                                                */
/* ***************************************************************** */
static int Receive_Data(int ipSock,int ipTimeOut,int ipBytesToRead,char *prpRecvData)
{
	char *pclFct = "Receive_Data";
  	int ilRC=RC_SUCCESS;

	char *pclHlpPtr = NULL;

	int ilNrOfBytes = 0;
	int ilRemainingBytesToRead = 0;
	int ilBytesAlreadyRead=0;
	time_t tlNow = 0;
	

	ilNrOfBytes = 0;
	ilBytesAlreadyRead = 0;
	ilRemainingBytesToRead = ipBytesToRead-ilBytesAlreadyRead;


	pclHlpPtr = prpRecvData;                
	do
	{
		ilNrOfBytes = 0;
		bgAlarm = FALSE;

		errno = 0;
		alarm(ipTimeOut);
		ilNrOfBytes=read(ipSock,pclHlpPtr,1);
		alarm(0);

		/* <0 means failure at read if no alarm appeared, =0 means connection lost*/
		if (ilNrOfBytes <= 0 && bgAlarm == FALSE)
		{
			ilRC = RC_FAIL;
			dbg(TRACE,"%s: OS-ERROR read(%d) on socket <%d>",pclFct,ilNrOfBytes,ipSock);
			dbg(TRACE,"%s: ERRNO (%d)=(%s)",pclFct,errno,strerror(errno));
		}
		/* <0 means no data read inside READ_TIMEOUT time */
		if (ilNrOfBytes <= 0 && bgAlarm == TRUE)
		{
			dbg(TRACE,"%s: ERROR - Receive Timeout",pclFct);
			bgAlarm = FALSE;
			ilRC = RC_NOT_FOUND;
		}
		/* if the read byte is a start of message sign */
		if (ilNrOfBytes==1)
		{
			ilRemainingBytesToRead--;
			pclHlpPtr++;
			ilRC = RC_SUCCESS;
		}

	}while(ilRC==RC_SUCCESS && ilRC!=RC_NOT_FOUND && 
               ilRC!=RC_FAIL && ilRemainingBytesToRead>0 && bgAlarm == FALSE);

  
  return ilRC;
}


/******************************************************************************/


/*****************************************************************************/ 
/* Function:   CreateBuffer()                                                */
/*****************************************************************************/ 
/* Parameter:                                                                */
/* In    : BUFFER  *pcpBuffer   - Pointer to Buffer                          */
/* In    : int     *pcpSize     - Buffersize in Byte                         */
/* In    : int     *pcpStepsize - Stepsize in Byte for realloc               */
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description: Allocate Memory for Bufferhandling                           */
/*                                                                           */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*           26.01.00  rkl  Written                                          */
/*                                                                           */
/*****************************************************************************/
static int CreateBuffer( BUFFER *prpBuffer, int ipSize, int ipStepsize)
{
  char *pclFct = "CreateBuffer";
  int    ilRC  = RC_SUCCESS;
  
  /* dbg(TRACE,"%s: ipSize<%d> ipStepsize<%d>",pclFct,ipSize,ipStepsize);*/
  
  if ((prpBuffer->data == NULL) && (prpBuffer->size == 0))    /* No Buffer */
  { 
	dbg(TRACE,"%s: create Buffer ipSize<%d> ipStepsize<%d>",pclFct,ipSize,ipStepsize); 
    prpBuffer->data = (char *) calloc(1,ipSize);
    if(prpBuffer->data == NULL)
    {
      dbg(TRACE,"%s: ERROR - calloc() failed ",pclFct);
      ilRC = RC_FAIL;
    }
    else
    {
      prpBuffer->size = ipSize;
      prpBuffer->stepsize = ipStepsize;
      prpBuffer->used = 0;
    }
  } 
  else if ((prpBuffer->data != NULL) && (ipSize > prpBuffer->size )) /* Buffer exists*/
  { 
    dbg(TRACE,"%s: realloc Buffer - Size<%d> ",pclFct,ipSize); 
    prpBuffer->data = (char *) realloc(prpBuffer->data,ipSize);
    if(prpBuffer->data == NULL)
    {
    	dbg(TRACE,"%s: ERROR - calloc() failed ",pclFct);
    	ilRC = RC_FAIL;
    }
    else
    {
      prpBuffer->size = ipSize;
      prpBuffer->stepsize = ipStepsize;
      prpBuffer->used = 0;
    }
  } 
  else
  {
    /* dbg(TRACE,"%s: ERROR - Wrong Parameters !",pclFct);*/
    ilRC = RC_FAIL;
  }
  
  return ilRC;

} /* end of CreateBuffer */


/*****************************************************************************/ 
/* Function:   CopyBuffer()                                                  */
/*****************************************************************************/ 
/* Parameter:                                                                */
/* In    : BUFFER  *pcpBuffer - Pointer to Buffer                            */
/* In    : char    *pcpData   - pointer to Data                              */
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description: Copy Data to Buffer; Check Size - realloc Memory if needed   */
/*                                                                           */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*           26.01.00  rkl  Written                                          */
/*                                                                           */
/*****************************************************************************/
static int CopyToBuffer(BUFFER *prpBuffer, char *pcpData)
{
  char *pclFct = "CopyToBuffer";
  int    ilRC  = RC_SUCCESS;
  int    ilDataLength = 0;
  int    ilNofStp     = 0;
  
  ilDataLength = strlen(pcpData);
  
  if(ilDataLength > prpBuffer->size)   /* Buffer needs malloc or realloc */
  {
  
    if( prpBuffer->stepsize > 0)             /* alloc in steps of StepSize */
    { 
      ilNofStp = ilDataLength / prpBuffer->stepsize;
      if ((ilDataLength % (prpBuffer->stepsize)) > 0)
      {
        ilNofStp++;
      }
      prpBuffer->size = ilNofStp * (prpBuffer->stepsize);
    
    } /* end if */

    if ((prpBuffer->data == NULL) && (prpBuffer->size == 0))    /* No Buffer */
    {  
      prpBuffer->data = (char *) calloc(1,prpBuffer->size);
    } /* end if */		        
    else if ((prpBuffer->data != NULL) && (prpBuffer->size > 0)) /* Buffer exists */
    {
      prpBuffer->data = (char *) realloc(prpBuffer->data,prpBuffer->size);
    } /* end else if */
  }
  
  strcpy(prpBuffer->data,pcpData);
  prpBuffer->used = ilDataLength;
  
  return ilRC;

} /* end of CopyToBuffer */


/*****************************************************************************/ 
/* Function:   AppendBuffer()                                                */
/*****************************************************************************/ 
/* Parameter:                                                                */
/* In    : BUFFER  *pcpBuffer - Pointer to Buffer                            */
/* In    : char    *pcpData   - pointer to Data                              */
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description: Append Data to Buffer; Check Size - realloc Memory if needed */
/*                                                                           */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*           26.01.00  rkl  Written                                          */
/*                                                                           */
/*****************************************************************************/
static int AppendBuffer( BUFFER *prpBuffer, char *pcpData)
{
  char *pclFct = "AppendBuffer";
  int    ilRC  = RC_SUCCESS;
  int    ilDataLength = 0;
  int    ilNewSize    = 0;
  int    ilNofStp     = 0;
  
  dbg(TRACE,"%s: Buffer size<%d> used<%d> stepsize<%d>\n",pclFct,
              prpBuffer->size,
              prpBuffer->used,
              prpBuffer->stepsize);        

  ilDataLength = strlen(pcpData);
  if(ilDataLength > (prpBuffer->size - prpBuffer->used)) 
  {
    ilNewSize =  prpBuffer->used + ilDataLength;
    dbg(TRACE,"Buffer to small need<%d>\n",ilNewSize);
    /* Buffer needs malloc or realloc */
    if( prpBuffer->stepsize > 0)             /* alloc in steps of StepSize */
    { 
      ilNofStp = ilNewSize / prpBuffer->stepsize;
      if ((ilNewSize % (prpBuffer->stepsize)) > 0)

      {
        ilNofStp++;
      }
      ilNewSize = ilNofStp * (prpBuffer->stepsize);
      dbg(TRACE,"New Buffersize <%d>\n",ilNewSize);
      
    } /* end if */
    
    if ((prpBuffer->data == NULL) && (prpBuffer->size == 0))    /* No Buffer */
    {  
      prpBuffer->data = (char *) calloc(1,ilNewSize);
      prpBuffer->size = ilNewSize;
    } /* end if */		        
    else if ((prpBuffer->data != NULL) && (prpBuffer->size > 0)) /* There is an Buffer */
    {
      dbg(TRACE,"Realloc Memory size<%d>\n",ilNewSize);
      prpBuffer->data = (char *) realloc(prpBuffer->data,ilNewSize);
      prpBuffer->size = ilNewSize;
      
    } /* end else if */
  }
  
  strcat(prpBuffer->data,pcpData);
  prpBuffer->used += ilDataLength;
  
  return ilRC;

} /* end of CopyToBuffer */


/*****************************************************************************/ 
/* Function:   ClearBuffer()                                                 */
/*****************************************************************************/ 
/* Parameter:                                                                */
/* In    : BUFFER  *pcpBuffer   - Pointer to Buffer                          */
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description: Clear Data Area - Set Data to '\0' and used Size to 0        */
/*                                                                           */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*           26.01.00  rkl  Written                                          */
/*                                                                           */
/*****************************************************************************/
static int ClearBuffer( BUFFER *prpBuffer)
{
  char *pclFct  ="ClearBuffer";
  int    ilRC  = RC_SUCCESS;
  
  
  if ((prpBuffer->data != NULL) && (prpBuffer->size != 0))    /* Buffer */
  {  
    memset(prpBuffer->data,'\0',prpBuffer->size);
    prpBuffer->used = 0;
  } /* end if */
  else
  {
    /* dbg(TRACE,"%s: ERROR - Wrong Parameters send !",pclFct);*/
    ilRC = RC_FAIL;
  }
  
  return ilRC;

} /* end of ClearBuffer */



/*****************************************************************************/ 
/* Function:   FreeTheBuffer()                                               */
/*****************************************************************************/ 
/* Parameter:                                                                */
/* In    : BUFFER  *pcpBuffer   - Pointer to Buffer                          */
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description: Free Buffer                                                  */
/*                                                                           */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*           26.01.00  rkl  Written                                          */
/*                                                                           */
/*****************************************************************************/
static int FreeTheBuffer( BUFFER *prpBuffer)
{
  char *pclFct  ="FreeTheBuffer";
  int    ilRC  = RC_SUCCESS;
  
  
  free(prpBuffer->data);   /* Free Buffer */
  prpBuffer->data = NULL;
  prpBuffer->size = 0;  
  prpBuffer->used = 0;

  return ilRC;

} /* end of FreeTheBuffer */

/*****************************************************************************/ 
/* Parameter:                                                                */
/* In/Out : Word  *pipData[]  - Pointer to data                              */
/* In	  : int    ipSize     - Size in byte of data                         */
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description:   convert data change Test for BIG_ENDIAN Byteorder          */
/*                ParameterFlag will be set to TRUE if BIG_ENDIAN Byteorder  */
/*                                                                           */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*           26.01.00  rkl  Written                                          */
/*                                                                           */
/*****************************************************************************/
static int ConvertData(WORD *pipData, int ipSize)
{
	char	*pclFct	="ConvertData";
	int	ilRC	= RC_SUCCESS;
	int	i;
	int	ilNoOfElem = 0;
	WORD *pipPtr = NULL;

	if(bgBIG_ENDIAN == TRUE)
	{
		pipPtr = pipData;
		ilNoOfElem = ipSize/ sizeof(WORD);

		for(i=0;i<ilNoOfElem; i++)
		{
			/* Convert WORD (short) from BIG_ENDIAN to LITTLE_ENDIAN */
			*pipPtr = (WORD)((((short)(*pipPtr) & 0x00ff) << 8) | (((short)(*pipPtr) & 0xff00) >> 8 ));
			pipPtr++;
		} 
	}
	return ilRC;

}



/*****************************************************************************/ 
/* Parameter:                                                                */
/* In    : BOOL  *pipBIG_ENDIAN  - Pointer to FLAG                           */
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description:   Test for BIG_ENDIAN Byteorder                              */
/*                ParameterFlag will be set to TRUE if BIG_ENDIAN Byteorder  */
/*                                                                           */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*           26.01.00  rkl  Written                                          */
/*                                                                           */
/*****************************************************************************/
static int TestByteOrder(BOOL *pipBIG_ENDIAN)
{
	char	*pclFct	="TestByteOrder";
	int	ilRC	= RC_SUCCESS;

	/* Test for BIG_ENDIAN or LITTLE_ENDIAN Byteorder */
        union {
		WORD w;
		char c[sizeof(WORD)];
	} x_endian;

	x_endian.w = 1;
	if(x_endian.c[(sizeof(WORD))-1] == 1)
	{
		dbg(TRACE,"%s: Byteorder BIG_ENDIAN - Set BIG_ENDIAN Flag to TRUE",pclFct);
		*pipBIG_ENDIAN = TRUE;		
	} 
	else if(x_endian.c[0] == 1)
	{
		dbg(TRACE,"%s: Byteorder is LITTLE_ENDIAN - Set BIG_ENDIAN Flag to FALSE",pclFct);
		*pipBIG_ENDIAN = FALSE;		
	}
	else
	{
		dbg(TRACE,"%s: Unknown Byteorder - Set BIG_ENDIAN Flag to FALSE ",pclFct);
		*pipBIG_ENDIAN = FALSE;		
	}

  return ilRC;

}







/*****************************************************************************/ 
/* Parameter:                                                                */
/* In    : BASMain  *prpCFG    - Pointer to Configuration                    */
/* In    : char     *pcpLogCmd - Pointer to logical Commad                   */
/* In    : char     *pcpSelect - Pointer to Selection                        */
/* In    : char     *pcpFields - Pointer to Fields                           */
/* In    : char     *pcpData   - Pointer to Data                             */
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description:   Handle Answer from a Ceda-event, Compares LogCmd with      */
/*                logical Names in Configuration                             */
/*                                                                           */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*          04.07.00  rkl  Written                                           */
/*                                                                           */
/*****************************************************************************/
static int HandleAnswer(BASMain *prpCFG, char *pcpLogName, char *pcpSelect,
                        char *pcpFields, char *pcpData)
{
	char	*pclFct ="HandleAnswer";
	int	ilRC  = RC_SUCCESS;
	int ilFound = FALSE;
	int ilCmd = 0;
	
	/* Search in Configuration for LogicalName(Command) */
	for(ilCmd=0; ilCmd < prpCFG->iNoOfCmds; ilCmd++)
	{
		if((strcmp(pcpLogName,prpCFG->prCmds[ilCmd].cLogicalName)) == 0)
		{
			dbg(TRACE,"%s: Logical Name <%s> found in Configuration",pclFct,pcpLogName);
			ilFound = TRUE;

			if((strcmp(pcpData,"0")) == 0)
			{
				/* all Monitor/Display are UP */
				if((ilRC = HandleBASCommand(prpCFG, prpCFG->prCmds[ilCmd].prCedaEvent->cNull)) == RC_FAIL)
				{
					dbg(TRACE,"%s: ERROR - HANDLE COMMAND Null <%s>", 
					          pclFct,prpCFG->prCmds[ilCmd].prCedaEvent->cNull);
				}
			}
			else if((strcmp(pcpData,"0")) != 0)
			{
				/* One or more Monitor/Displays are down */
				if((ilRC = HandleBASCommand(prpCFG, prpCFG->prCmds[ilCmd].prCedaEvent->cNotNull)) == RC_FAIL)
				{
					dbg(TRACE,"%s: ERROR - HANDLE COMMAND <%s>", 
					          pclFct,prpCFG->prCmds[ilCmd].prCedaEvent->cNotNull);
				}
			}
			else
			{
				dbg(TRACE,"%s: ERROR - HANDLE ANSWER - Unknown Logical Name <%s>", 
				          pclFct,pcpLogName);
			}
		}	
	} /* end of for */

	if(ilFound == FALSE)
	{
		dbg(TRACE,"%s: ERROR - LogName <%s> not found",pclFct,pcpLogName );
		ilRC = RC_FAIL;
	}

  return ilRC;
} /* end of HandleAnswer() */

/*****************************************************************************/ 
/* Parameter:                                                                */
/* In    : BUFFER  *pcpBuffer   - Pointer to Buffer                          */
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description:                                                              */
/*                                                                           */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*           26.01.00  rkl  Written                                          */
/*                                                                           */
/*****************************************************************************/
static int Function(BUFFER *prpBuffer)
{
  char *pclFct  ="Function";
  int    ilRC  = RC_SUCCESS;


  return ilRC;
}


/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
