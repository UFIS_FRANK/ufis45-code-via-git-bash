#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Interface/dysif.c 1.4 2011/06/01 21:06:27SGT gfo Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB AAT/I Program dysif.c    	                                            */
/*                                                                            */
/* Author         : Joern Weerts                                              */
/* Date           : 06.07.2000                                                */
/* Description    : Interface to the dynamic signage of Unitechnik    				*/
/*                  for New Athens International Airport /										*/
/*                  Greece                                                    */
/* Update history :                                           								*/
/*                  jwe 06.07.2000                                            */
/*                  started with interface programming                        */
/*                  date/who                                                  */
/*                  action                                                    */
/******************************************************************************/
/* 20041004 JIM: replaced sccs_version by mks_version to allow compilation */
/* *******  System Header Files (unix includes) ****** */
/*                                                                            */
/* source-code-control-system version string                                  */
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE

#include "dysif.h"
/******************************************************************************/
/* External variables and functions                                           */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */      
/*FILE *outp       = NULL;*/
int  debug_level = TRACE;
int  igDebugLevel = 0;

extern int nap(unsigned long time);
extern int snap(char*,long,FILE*);
extern int get_real_item(char *,char *,int);
extern void BuildItemBuffer(char *, char *, int, char *);
extern int GetDataItem(char*,char*,int,char,char*,char*);
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static int igCnt = 0;
static ITEM  *prgItem      = NULL;    		/* The queue item pointer  */
static int igItemLen     	 = 0;       		/* length of incoming item */
static EVENT *prgEvent     = NULL;    		/* The event pointer       */
static CFG  prgCfg;												/* pointer to cfg-file structure */
static FTPConfig prgFtp;									/* struct for hyper-dynamic programming of FTPHDL */
static char pcgProcessName[20];      			/* buffer for my own process name */ 
static char pcgDYS_Host[64];    					/* buffer for the host that is connected */ 
static char pcgHomeAp[20];      					/* buffer for name of the home-airport */
static char pcgTabEnd[20];      					/* buffer for name of the table extension */
static char pcgAftTable[20];							/* buffer for AFT-table to use */
static int igHeadLen 		   = 0;						/* length of telegram header */
static int igTailLen 		   = 0;						/* length of telegram header */
static int igAckLen 		   = 0;						/* length of telegram header */
static int igInitOK   		 = FALSE;				/* init flag */
static int igSock					 = 0;						/* global tcp/ip socket for receive */
static int igMsgCounter 	 = 0;						/* counter for message numbers */
static int igWaitForAck 	 = 0;						/* seconds for waiting for acknowledge */
static int igMaxSends 	 	 = 0;						/* maximum number of sends for telegram */
static int igModID_Flight  = 0;						/* MOD-ID of Flight */
static int igModID_Router  = 0;						/* MOD-ID of Router  */
static int igModID_Ftphdl  = 0;						/* MOD-ID of Ftphdl  */
static int igRecvTimeout	 = 0;						/* number of sec. to wait for a valid telegram */
static int igUtcTimeDiff = 0;
static time_t tgRecvTimeOut	 = 0;					/* time until to dysif waits for a valid telegram */
static time_t tgNextConnect	 = 0;					/* time for trying to establish a con. to dys*/
static time_t tgNextArrivalInfoCheck = 0;	/* time for sending arrival display info dys*/
static time_t tgInterval = 0;							/* time for sending arrival display info dys*/
static time_t tgAutoDelete = 0;						/* time-difference after which automatically flights are removed from display*/
static char pcgConfFile[S_BUFF];					/* buffer for filename */
static char pcg_HSB_ConfFile[S_BUFF];			/* buffer for filename */
static char	*pcgValid_SAI_Fields = "b1ba,b1ea,b2ba,b2ea,flno,blt1,blt2,org3,psta";
static char	*pcgValid_SSCI_Fields = "AFTTAB.FLNO,AFTTAB.STOD,AFTTAB.ETOD,CHATAB.DES3,AFTTAB.PSTD,CHATAB.INDI,CHATAB.LNAM,CHATAB.FCLA,CHATAB.STOB,CHATAB.STOE,CHATAB.TIFB,CHATAB.TIFE";
static char	*pcgValid_SACI_Fields = "AFTTAB.FLNO,AFTTAB.STOD,AFTTAB.ETOD,CHATAB.DES3,AFTTAB.PSTD,CHATAB.INDI,CHATAB.LNAM,CHATAB.FCLA,CHATAB.STOB,CHATAB.STOE,CHATAB.TIFB,CHATAB.TIFE,CHATAB.TISB,CHATAB.TISE";
static char	*pcgRurnClause = " (CHATAB.RURN=AFTTAB.URNO) ";
static char	*pcg_SSCI_OrderClause = " CHATAB.URNO,DECODE(CHATAB.INDI,'N','G',CHATAB.INDI)";
static char	*pcg_SACI_OrderClause = " DECODE(CHATAB.INDI,'N','G',CHATAB.INDI),CHATAB.URNO";
static char	*pcgValid_CXX_Fields = "org3,adid,stod,tifd,ftyp,flno";

static LPLISTHEADER prgRecvMsgList = NULL;/* pointer to internal message list */

static char prgSendBuffer[MAX_TELEGRAM_LEN]; /* global used buffer for socket-write */ 
static char prgRecvBuffer[MAX_TELEGRAM_LEN]; /* global used buffer for socket-read */ 
static TAIL prgEnd;													/* telegram end struct */

static BOOL bgSocketOpen	= FALSE;		/* global flag for connection-state */
static BOOL bgAlarm 					= FALSE;		/* global flag for timed-out socket-read */
static BOOL bgWaitForAck			= FALSE;		/* global flag for ack-waiting */
static BOOL bgUseHopo					= FALSE;		/* global flag for use of HOPO database field */

static FILE *pgReceiveLogFile = NULL;			/* filepointer for all received telegrams*/
static FILE *pgSendLogFile 		= NULL;			/* filepointer for all send telegrams*/
static int igSendArrivalCount = 0;
/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
/*----------------*/
/* MAIN functions */
/*----------------*/
static int Init_dysif(void);		/* main initializing */	
static int Reset(void);         /* Reset program          */
static int HandleInternalData(void);        /* Handles data from CEDA */
static int HandleExternalData(char *pcpData);/* handles data from DYS */
static void	Terminate(BOOL bpSleep);        /* Terminate program      */
static void	HandleSignal(int);              /* Handles signals        */
static void	HandleErr(int);                 /* Handles general errors */
static void	HandleQueErr(int);              /* Handles queuing errors */
static void HandleQueues(void);             /* Waiting for Sts.-switch*/
static int ConnectTCP(char *pcpHost,int ipTry);
static void CloseTCP();
static int ReadCfg(void);
static int GetCfgEntry(char *pcpFile,char *pcpSection,char *pcpEntry,short spType,
												char **pcpDest,int ipValueType,char *pcpDefVal);
static int CheckValue(char *pcpEntry,char *pcpValue,int ipType);
static int CheckCfg(void);
static void FreeCfgMem(void);
static int poll_q_and_sock ();
static int Receive_data(int ipSock,int ipAlarm,char *pcpCalledBy);
static int Send_data(int ipSock,char *pcpData,int ipLen);
static int WaitForAck(int ipSock,char *pcpMsg,int ipLen);
static int SendEvent(char *pcpCmd,char *pcpSelection,char *pcpFields,char *pcpData,
										char *pcpAddStruct,int ipAddLen,int ipModIdSend,int ipModIdRecv,
										int ipPriority,char *pcpTable,char *pcpType,char *pcpFile);
static int ProcessIntData(char *pcpFlights,int ipType,char *pcpSendBuffer,char *pcpFile);
static int Convert_SAI(int ipCalledBy,char *pcpData,int ipLoop,char *pcpResultBuf);
static int Convert_SSCI(char *pcpData);
static int Convert_SACI(char *pcpData,int *ipType,int ipTelegramType);
static int Convert_CXX(char *pcpData);
static char *GetViaByNumber(char *pcpVial,int ipViaNo, int ipVia34);
static int SendArrivalFlightInfo();
static int CreateChuteInfoFiles();
static int SendFileViaFtp(char *pcpHost,char *pcpFile);
static int ProcessBeltUpdate(char *pcpData,char *pcpFields,char *pcpSelection);
/*---------------------*/
/* formating functions */
/*---------------------*/
static void GetMsgTimeStamp(char *pcpTimeStr);
static void TrimRight(char *pcpBuffer);
static int FormatDate(char *pcpOldDate,char *pcpFormat,char *pcpNewDate);
static int StrToTime(int ipUtcLocal,char *pcpTime,time_t *pclTime);
static void TimeToStr(int ipUtcLocal,char *pcpTime,time_t lpTime,int ipType);
static void CleanMessage(char *pcpMsg,int ipMsgLen);
static void TrimNewLine(char *pcpString);
static int MakeExtFlno(char *pcpFlno);
static void TrimAll(char *pcpBuffer);
/*-------------------*/
/* logging functions */
/*-------------------*/
static void snapit(void *pcpBuffer,long lpDataLen,FILE *pcpDbgFile);
static void StoreRecvMessage(char *pcpMsg); /* stores incoming messages */
static void HandleStoredData(int ipTimesToDo);/* Handles stored, received messages */
/*-----------------------------------------*/
/* functions for internal created messages */
/*-----------------------------------------*/
static int PrepareData(int ipCmd, char *pcpEventData,time_t tpDayToSend);
static void MakeSingleEntry(char *pcpSrc,int ipLen,int ipAdjust,char cpFill,char *pcpDest);
static void GetMsgNumber(char *pcpMsgNumber);
static void MakeEnd(void);
static int MakeSendBuffer(int ipGetNr,int ipCmd, char *pcpData,char *pcpSendBuffer);
/*---------------------------------*/
/* functions for external messages */
/*---------------------------------*/
static int SetDisplayValue(time_t tpBeltOpen,time_t tpBeltClose,char *cpShow);
static int CheckHeader(char *pcpData,int *ipRecvCmd);
static int ProcessExtData(char *pcpData,int ipCmd,BOOL bpAck);
static int Send_FTF_Msg();
static void Send_Ack_Msg();
static void Send_New_Alive();
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;

	INITIALIZE;			/* General initialization	*/

	/* handles signal		*/
	(void)SetSignals(HandleSignal);

  /* copy process-name to global buffer */
  memset(pcgProcessName,0x00,sizeof(pcgProcessName));
	strcpy(pcgProcessName, argv[0]);                            

	dbg(TRACE,"MAIN: version <%s>",mks_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));
	
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}
	else
	{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}
	/* Attach to the DB */
	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
		dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
		sleep(6);
		ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRc != RC_SUCCESS));
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}
	else
	{
		dbg(TRACE,"MAIN: init_db()  OK!");
	} /* end of if */
	/**********************************************************/
	/* following lines for identical binaries on HSB-machines */
	/**********************************************************/
	*pcgConfFile = '\0'; 
	sprintf(pcgConfFile,"%s/%s",getenv("BIN_PATH"),pcgProcessName);
	ilRc = TransferFile(pcgConfFile); 
	if(ilRc != RC_SUCCESS) 
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
	}
	
	sprintf(pcg_HSB_ConfFile,"%s/hsb.dat",getenv("CFG_PATH"));
	/*sprintf(pcg_HSB_ConfFile,"%s/hsb.jwe",getenv("CFG_PATH"));*/
	/********************************************************************/
	/*For identical cfg-files on HSB-machines uncomment following lines */
	/********************************************************************/
	*pcgConfFile = '\0';
	sprintf(pcgConfFile,"%s/%s.cfg",getenv("CFG_PATH"),pcgProcessName);
	ilRc = TransferFile(pcgConfFile); 
	if(ilRc != RC_SUCCESS) 
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
	} 
	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	}
	
	/* now waiting if I'm STANDBY */
	if ((ctrl_sta != HSB_STANDALONE) &&
			(ctrl_sta != HSB_ACTIVE) &&
			(ctrl_sta != HSB_ACT_TO_SBY))
	{
		HandleQueues();
	}

	dbg(TRACE,"--------------------------------");
	if ((ctrl_sta == HSB_STANDALONE) ||
			(ctrl_sta == HSB_ACTIVE) ||
			(ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		dbg(TRACE,"--------------------------------");
		if(igInitOK == FALSE)
		{
			ilRc = Init_dysif();
			debug_level = TRACE;
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"--------------------------------");
				dbg(TRACE,"MAIN: Init_dysif() failed! Terminating!");
				Terminate(TRUE);
			}
		}
	} 
	else
	{
		dbg(TRACE,"MAIN: wrong HSB-state! Terminating!");
		Terminate(FALSE);
	}
	
	if (ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: initializing OK");
		dbg(TRACE,"--------------------------------");
		debug_level = igDebugLevel;
		
		if (strcmp(prgCfg.mode,"TEST") == 0)
		{
				SendArrivalFlightInfo();
				ilRc = CreateChuteInfoFiles();
		}

		while(TRUE)
		{
			if ((ilRc = poll_q_and_sock()) == RC_SUCCESS)
			{
				if ((ilRc = HandleExternalData(prgRecvBuffer)) != RC_SUCCESS)
				{
					dbg(TRACE,"MAIN: HandleExternalData() failed!");
				}
			}
		}
	}
	else
	{
		dbg(TRACE,"MAIN: Init_dysif() invalid! Terminating!");
	}
	Terminate(TRUE);
	return(0);
} /* end of MAIN */
/*********************************************************************
Function	 :Init_dysif
Paramter	 :IN: ipInitPhase = which parameters should be initalized 
Returnvalue:RC_SUCCES,RC_FAIL
Description:initializes the interface with all necessary values
*********************************************************************/
static int Init_dysif()
{
	int	ilRc = RC_FAIL;			/* Return code */
	
	igHeadLen = sizeof(HEAD);	
	igTailLen = sizeof(TAIL);	
	igAckLen = sizeof(ACK_EXT);	

	/* get mod-id of flight */
	if ((igModID_Flight = tool_get_q_id("flight")) == RC_NOT_FOUND ||
			igModID_Flight == RC_FAIL)
	{
		dbg(TRACE,"Init_dysif: tool_get_q_id(flight) returns: <%d>",igModID_Flight);
		return RC_FAIL;
	}
	else
	{
		dbg(TRACE,"Init_dysif : flight mod_id <%d>",igModID_Flight);
	}

	/* get mod-id of router */
	if ((igModID_Router = tool_get_q_id("router")) == RC_NOT_FOUND ||
			igModID_Router == RC_FAIL)
	{
		dbg(TRACE,"Init_dysif: tool_get_q_id(router) returns: <%d>",igModID_Router);
		return RC_FAIL;
	}
	else
	{
		dbg(TRACE,"Init_dysif : router mod_id <%d>",igModID_Router);
	}

	/* get mod-id of ftphdl */
	if ((igModID_Ftphdl = tool_get_q_id("ftphdl")) == RC_NOT_FOUND ||
			igModID_Ftphdl == RC_FAIL)
	{
		dbg(TRACE,"Init_dysif: tool_get_q_id(ftphdl) returns: <%d>",igModID_Ftphdl);
		return RC_FAIL;
	}
	else
	{
		dbg(TRACE,"Init_dysif : ftphdl mod_id <%d>",igModID_Ftphdl);
	}

	/* reading default home-airport from sgs.tab */
	memset(pcgHomeAp,0x00,sizeof(pcgHomeAp));
	ilRc = tool_search_exco_data("SYS","HOMEAP",pcgHomeAp);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"Init_dysif : No HOMEAP entry in sgs.tab: EXTAB! Please add!");
		return RC_FAIL;
	}
	else
	{
		dbg(TRACE,"Init_dysif : HOMEAP = <%s>",pcgHomeAp);
	}
	
	/* reading default table-extension from sgs.tab */
	memset(pcgTabEnd,0x00,sizeof(pcgTabEnd));
	ilRc = tool_search_exco_data("ALL","TABEND",pcgTabEnd);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"Init_dysif : No TABEND entry in sgs.tab: EXTAB! Please add!");
		return RC_FAIL;
	}
	else
	{
		*pcgAftTable = 0x00;
		strcpy(pcgAftTable,"AFT");
		dbg(TRACE,"Init_dysif : TABEND = <%s>",pcgTabEnd);
		strcat(pcgAftTable,pcgTabEnd);
		dbg(TRACE,"Init_dysif : AFT-table = <%s>",pcgAftTable);
		if (strcmp(pcgTabEnd,"TAB") == 0)
		{
			bgUseHopo = TRUE;
			dbg(TRACE,"Init_dysif : use HOPO-field!");
		}
	}

	memset(prgSendBuffer,0x00,MAX_TELEGRAM_LEN);
	memset(prgRecvBuffer,0x00,MAX_TELEGRAM_LEN);
	MakeEnd();	

	if ((prgRecvMsgList=ListInit(prgRecvMsgList,sizeof(MSG_RECV))) == NULL)
	{
		dbg(TRACE,"Init_dysif : couldn't create internal Recv.-Msg-List!");
		return RC_FAIL;
	}

	/* now reading from configfile */
	if (*pcgConfFile != 0x00)
	{
		if ((ilRc = ReadCfg()) == RC_SUCCESS)
		{
			if ((ilRc = CheckCfg()) == RC_SUCCESS)
			{
				igInitOK = TRUE;
				if (strlen(prgCfg.recv_log) > 0)
				{
					pgReceiveLogFile = fopen(prgCfg.recv_log,"w");	
					if ( ! pgReceiveLogFile )
					{
						dbg(TRACE,"Init_dysif : ReceiveLog: <%> not opened! fopen() returns <%s>."
							,prgCfg.recv_log,strerror(errno));
					}
				}
				
				if (strlen(prgCfg.send_log) > 0)
				{
					pgSendLogFile = fopen(prgCfg.send_log,"w");	
					if ( ! pgSendLogFile )
					{
						dbg(TRACE,"Init_dysif : SendLog: <%> not opened! fopen() returns <%s>."
							,prgCfg.send_log,strerror(errno));
					}
				}
				tgRecvTimeOut = time(0) + (time_t)(igRecvTimeout - READ_TIMEOUT);
			}
			else
			{
				dbg(TRACE,"Init_dysif : CheckCfg() failed!");
			}
		}
		else
		{
			dbg(TRACE,"Init_dysif : ReadCfg() failed!");
		}
	}
	return(ilRc);
} /* end of initialize */

/*********************************************************************
Function	 :Reset()
Paramter	 :IN:                                                   
Returnvalue:RC_SUCCESS,RC_FAIL
Description:rereads the config-file,reinitializes the recv. and send
						log-files and clears the receive and the send buffers
*********************************************************************/
static int Reset(void)
{
	int	ilRc = RC_SUCCESS;
	
	dbg(TRACE,"Reset: now resetting ...");
	dbg(TRACE,"Reset: closing TCP-IP connection.",ilRc);
	CloseTCP();
	
	tgNextConnect = 0;

	memset(prgSendBuffer,0x00,MAX_TELEGRAM_LEN);
	memset(prgRecvBuffer,0x00,MAX_TELEGRAM_LEN);

	FreeCfgMem();

	ListDestroy(prgRecvMsgList);

	dbg(TRACE,"Reset: closing Log-files.");
	if(pgReceiveLogFile != NULL)
		fclose(pgReceiveLogFile);
	if(pgSendLogFile != NULL)
		fclose(pgSendLogFile);

	igMsgCounter = 0;
	
	if ((prgRecvMsgList=ListInit(prgRecvMsgList,sizeof(MSG_RECV))) == NULL)
	{
		dbg(TRACE,"Init_dysif : couldn't create internal Recv.-Msg-List!");
		return RC_FAIL;
	}

	if (*pcgConfFile != '\0')
	{
		if ((ilRc = ReadCfg()) != RC_SUCCESS)
		{
			dbg(TRACE,"Reset: ReadCfg() failed!");
			ilRc = RC_FAIL;
		}
		else
		{
			if ((ilRc = CheckCfg()) != RC_SUCCESS)
			{
				dbg(TRACE,"Reset: CheckCfg() failed!");
				ilRc = RC_FAIL;
			}
			else
			{
				if (strlen(prgCfg.recv_log) > 0)
				{
					pgReceiveLogFile = fopen(prgCfg.recv_log,"w");	
					if (!pgReceiveLogFile )
					{
						dbg(TRACE,"Reset: ReceiveLog <%> not opened",prgCfg.recv_log);
					}
				}
				
				if (strlen(prgCfg.send_log) > 0)
				{
					pgSendLogFile = fopen(prgCfg.send_log,"w");	
					if (!pgSendLogFile)
					{
						dbg(TRACE,"Reset: Send_log <%> not opened",prgCfg.send_log);
					}
				}
			}
		}
	}
	dbg(TRACE,"Reset: ... finished!");
	return ilRc;
} 
/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(BOOL bpSleep)
{
	int ilRc = 0;
	if (bpSleep == TRUE)
	{
		dbg(TRACE,"Terminate: sleeping 60 sec. before terminating.");
		sleep(60);
	}

	if (igInitOK == TRUE)
	{
		CloseTCP();
		dbg(TRACE,"Terminate: TCP/IP-connections closed.");

		if(pgReceiveLogFile != NULL)
			fclose(pgReceiveLogFile);
		if(pgSendLogFile != NULL)
			fclose(pgSendLogFile);
		dbg(TRACE,"Terminate: Log-files closed.");

		FreeCfgMem();
		dbg(TRACE,"Terminate: Config-memory freed.");

		ListDestroy(prgRecvMsgList);
		dbg(TRACE,"Terminate: Unacked messages deleted.");
	}
	dbg(TRACE,"Terminate: now leaving ......");
	exit(0);
} 

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int ipSig)
{
	switch (ipSig)
	{
		case SIGPIPE:
			dbg(TRACE,"HandleSignal: Received Signal <%d>(SIGPIPE).",ipSig);
			CloseTCP();
			break;
		case SIGTERM:
			dbg(TRACE,"HandleSignal: Received Signal <%d>(SIGTERM).",ipSig);
			Terminate(FALSE);
			break;
		case SIGALRM:
			/*dbg(DEBUG,"HandleSignal: Received Signal<%d>(SIGALRM)",ipSig);*/
			bgAlarm = TRUE;
			break;
		case SIGCHLD:
			dbg(TRACE,"HandleSignal: Received Signal<%d>(SIGCHLD)",ipSig);
			break;
		default:
			dbg(TRACE,"HandleSignal: Received Signal<%d>",ipSig);
			Terminate(FALSE);
			break;
	} 
} 

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr)
	{
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	case	QUE_E_PRIORITY:
		dbg(TRACE,"<%d> : invalid priority was send ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
	return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	dbg(TRACE,"HandleQueues: now entering ...");
	do
	{
		/*memset(prgItem,0x00,igItemLen);*/
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	
		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_STANDBY event!");
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_COMING_UP event!");
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_ACTIVE event!");
				dbg(TRACE,"HandleQueues: Terminating for new init ...");
				Terminate(FALSE);
				ilBreakOut = TRUE;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_ACT_TO_SBY event!");
				break;	
			case	HSB_DOWN	:
				/* 	whole system shutdown - do not further use que(), */
				/*	send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_DOWN event!");
				Terminate(FALSE);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_STANDALONE event!");
				dbg(TRACE,"HandleQueues: Terminating for new init ...");
				Terminate(FALSE);
				ilBreakOut = TRUE;
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				/*HandleRemoteDB(prgEvent);*/
				break;
			case	SHUTDOWN	:
				dbg(TRACE,"HandleQueues: received SHUTDOWN event!");
				Terminate(FALSE);
				break;
			case	RESET		:
				dbg(TRACE,"HandleQueues: received RESET event!");
				ilRc = Reset();
				if (ilRc == RC_FAIL)
				{
					Terminate(FALSE);
				}
				break;
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong HSB-status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		}
		else
		{
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	}while (ilBreakOut == FALSE);

	if(igInitOK == FALSE)
	{
			ilRc = Init_dysif();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Init_dysif: init phase 2 failed!");
			} /* end of if */
	}/* end of if */
	dbg(TRACE,"HandleQueues: ... now leaving");
} /* end of HandleQueues */
	

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleInternalData()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilDataLen = 0;			/* Return code */
	int	ilType = 0;			/* Return code */
	char *pclSelection = NULL;
	char *pclFields = NULL;
	char *pclData = NULL;
	FTPConfig *pclFtp = NULL;
  BC_HEAD *bchd = NULL;		/* Broadcast header		*/
  CMDBLK  *cmdblk = NULL;	/* Command Block 		*/
	char pclLocalFile[S_BUFF];
	char pclTwStart[S_BUFF];
	char pclSendBuffer[XL_BUFF];
	char pclTmpBuff[XL_BUFF];
	char pclAftUrno[XXS_BUFF];

  bchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  cmdblk= (CMDBLK  *) ((char *)bchd->data);

	dbg(TRACE,"");
	dbg(TRACE,"--- END/START HandleInternalData ---");

	dbg(TRACE,"HID: From<%d> Cmd<%s> Table<%s> Twstart<%s>",
			prgEvent->originator,cmdblk->command,cmdblk->obj_name,cmdblk->tw_start);
	
	/*DebugPrintItem(DEBUG,prgItem);*/
	/*DebugPrintEvent(DEBUG,prgEvent);*/

	pclSelection = cmdblk->data;
	pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
	pclData = (char*)pclFields + strlen(pclFields) + 1;

	dbg(DEBUG,"HID: Selection: <%s>",pclSelection); 
	dbg(DEBUG,"HID: Fields   : <%s>",pclFields); 
	dbg(DEBUG,"HID: Data     : <%s>",pclData);

	/******************************************************************************/
	/* receiving command from NTISCH for sending the daily-chute assignments to DYSI */
	/******************************************************************************/
	if (strcmp(cmdblk->command,"SSCI") == 0)
	{
		dbg(DEBUG,"HID: <SSCI> command received.");
		if ((ilRc = PrepareData(SSCI,"",0)) != RC_SUCCESS)
		{
			dbg(TRACE,"HID: PrepareData(SSCI) failed!");
		}
	}

	/***************************************************************/
	/* receiving update from ACTION for sending a new info to DYS */
	/***************************************************************/
	/* (taking it only if I wasn't the originator */
	if ((strncmp(cmdblk->obj_name,"CHA",3) == 0 &&
			prgEvent->originator!=mod_id))
	{
		/* update on chatab */
		if (strcmp(cmdblk->command,"URT") == 0 || strcmp(cmdblk->command,"IRT")==0)
		{
			TrimNewLine(pclFields);
			if (strncmp(pclFields,"URNO",4) == 0 )
			{
				TrimNewLine(pclData);
				dbg(DEBUG,"HID: <%s>-info from ACTION DATA=<%s>.",cmdblk->command,pclData); 
				ilRc = PrepareData(SACI,pclData,0);
			}
			else
			{
				dbg(TRACE,"HID: invalid field=<%s> received! expecting URNO as first field!",pclFields);
				dbg(TRACE,"HID: Assuming invalid ACTION configuration!"); 
			}
		}
	}

	/* update on AFTTAB related fields */
	if ((strncmp(cmdblk->obj_name,"AFT",3) == 0 &&
			prgEvent->originator!=mod_id))
	{
		/* update on afttab */
		/* position change or estimated time change */
		if (strcmp(cmdblk->command,"FTU") == 0 &&
				(strstr(pclFields,"PSTD") != NULL || strstr(pclFields,"pstd")!= NULL ||
				strstr(pclFields,"FLNO") != NULL || strstr(pclFields,"flno") != NULL ||
				strstr(pclFields,"STOD") != NULL || strstr(pclFields,"stod") != NULL ||
				strstr(pclFields,"ETOD") != NULL || strstr(pclFields,"etod") != NULL))
		{
				/* Stripping urno from selection for Preparation of data */
				/* Taking the old-data from pcldata because only updates are */
				/* processed and urno only changes on insert */
				memset(pclAftUrno,0x00,XXS_BUFF);
				GetDataItem(pclAftUrno,pclSelection,2,'\n',"","");
				TrimAll(pclAftUrno);
				dbg(DEBUG,"HID: <%s>-info from ACTION URNO=<%s>.",cmdblk->command,pclAftUrno); 
				ilRc = PrepareData(FTU,pclAftUrno,0);
		}

		/* cancelation of a flight occured */
		/* update on afttab FTYP */
		if (strcmp(cmdblk->command,"CXX") == 0)
		{
			TrimNewLine(pclSelection);
			dbg(DEBUG,"HID: <CXX>-info from ACTION SEL=<%s>.",pclSelection); 
			memset(pclTmpBuff,0x00,XL_BUFF);
			sprintf(pclTmpBuff," WHERE URNO = %s ",pclSelection);
			/*ilRc = PrepareData(CXX,pclSelection,0);*/
			ilRc = PrepareData(CXX,pclTmpBuff,0);
		}

		/* Belt change occured */
		/* update on afttab belts */
		if (strcmp(cmdblk->command,"BLT")==0 &&
			 (strstr(pclFields,"BLT") !=NULL || strstr(pclFields,"blt") !=NULL) ||
			 (strstr(pclFields,"B1BA")!=NULL || strstr(pclFields,"b1ba")!=NULL) ||
			 (strstr(pclFields,"B1EA")!=NULL || strstr(pclFields,"b1ea")!=NULL) ||
			 (strstr(pclFields,"B2BA")!=NULL || strstr(pclFields,"b2ba")!=NULL) ||
			 (strstr(pclFields,"B2EA")!=NULL || strstr(pclFields,"b2ea")!=NULL))
		{
			TrimNewLine(pclSelection);
			dbg(DEBUG,"HID: <BLT>-info from ACTION SEL=<%s>.",pclSelection);
			ilRc = ProcessBeltUpdate(pclData,pclFields,pclSelection);
		}
	}

	/**********************************/
	/* receiving answer from a FTPHDL */
	/**********************************/
	if (strcmp(cmdblk->command,"FTP") == 0)
	{
		dbg(DEBUG,"HID: answer from FTPHDL."); 
		pclFtp = (FTPConfig*) ((char*)pclData + strlen(pclData) + 1);
		if (pclFtp->iFtpRC==FTP_SUCCESS) 
		{
			dbg(TRACE,"HID: FTP to <%s> OK!",pclFtp->pcHostName);
			{
				if (strcmp(pclFtp->pcHostName,pcgDYS_Host)==0)
				{
					dbg(DEBUG,"HID: Send_FTF_Msg()!"); 
					if ((ilRc = Send_FTF_Msg(pclFtp->pcRemoteFileName)) != RC_SUCCESS)
					{
						dbg(TRACE,"HID: Send_FTF_Msg() returns <%d>",ilRc);
					}
				}
			}
		}
		else
		{
			dbg(TRACE,"HID: FTPHDL error <%d>!",pclFtp->iFtpRC);
			dbg(TRACE,"HID: check configuration and connection!");
		}
	}
  return ilRc;
} 

/* ********************************************************************/
/* The TRimNewLine() routine																					 */
/* ********************************************************************/
static void TrimNewLine(char *pcpString)
{
	char *pclPointer = NULL;
	if ((pclPointer=strchr(pcpString,'\n')) != NULL)
	{
		*pclPointer = '\0';
	}
}
/* ********************************************************************/
/* The ReadCfg() routine																					 */
/* ********************************************************************/
static int ReadCfg(void)
{
	int ilRc = RC_SUCCESS;

	memset(&prgCfg,0x00,sizeof(CFG));
	
	dbg(TRACE,"--------------------------------");
	dbg(TRACE,"ReadCfg: file <%s>",pcgConfFile);
	dbg(TRACE,"--------------------------------");
	/* reading [MAIN] section from cfg-file */
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","MODE",CFG_STRING,&prgCfg.mode,CFG_ALPHA,"REAL"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","MY_SYS_ID",CFG_STRING,&prgCfg.my_sys_id,CFG_ALPHANUM,"F0"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","DYS_HOST1",CFG_STRING,&prgCfg.dys_host1,CFG_PRINT,""))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","DYS_HOST2",CFG_STRING,&prgCfg.dys_host2,CFG_PRINT,""))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","SERVICE_PORT",CFG_STRING,&prgCfg.service_port,CFG_PRINT,"EXCO_DYS"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","WAIT_FOR_ACK",CFG_STRING,&prgCfg.wait_for_ack,CFG_NUM,"2"))
			!= RC_SUCCESS)
	{
		return RC_FAIL;
	}
	else
	{
		igWaitForAck = atoi(prgCfg.wait_for_ack);
	}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","RECV_TIMEOUT",CFG_STRING,&prgCfg.recv_timeout,CFG_NUM,"60"))
			!= RC_SUCCESS)
	{
		return RC_FAIL;
	}
	else
	{
		igRecvTimeout = atoi(prgCfg.recv_timeout);
	}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","MAX_SENDS",CFG_STRING,&prgCfg.max_sends,CFG_NUM,"2"))
			!= RC_SUCCESS)
	{
		return RC_FAIL;
	}
	else
	{
		igMaxSends = atoi(prgCfg.max_sends);
	}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","RECV_LOG",CFG_STRING,&prgCfg.recv_log,CFG_PRINT,"/ceda/debug/dysif.log"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","SEND_LOG",CFG_STRING,&prgCfg.send_log,CFG_PRINT,"/ceda/debug/dysif.log"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","TRY_RECONNECT",CFG_STRING,&prgCfg.try_reconnect,CFG_NUM,"60"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","REMOTE_SYS_ID",CFG_STRING,&prgCfg.remote_sys_id,CFG_ALPHA,"BD"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","DB_ARR_FILTER",CFG_STRING,&prgCfg.db_arr_filter,CFG_PRINT," "))
		!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","DB_DEP_FILTER",CFG_STRING,&prgCfg.db_dep_filter,CFG_PRINT," "))
		!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","FS_OFFSET",CFG_STRING,&prgCfg.fs_offset,CFG_NUM,"7"))
		!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","FS_LOCAL_PATH",CFG_STRING,&prgCfg.fs_local_path,CFG_PRINT,"/ceda/tmp"))
		!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","FS_REMOTE_PATH",CFG_STRING,&prgCfg.fs_remote_path,CFG_PRINT," "))
		!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","FTP_CLIENT_OS",CFG_STRING,&prgCfg.ftp_client_os,CFG_ALPHA,"UNIX"))
		!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","FTP_USER",CFG_STRING,&prgCfg.ftp_user,CFG_ALPHANUM,"none"))
		!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","FTP_PASS",CFG_STRING,&prgCfg.ftp_pass,CFG_IGNORE,"none"))
		!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","AFTER_FTP",CFG_STRING,&prgCfg.after_ftp,CFG_ALPHA,"D"))
		!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","ARR_FROM",CFG_STRING,&prgCfg.arr_from,CFG_NUM,"60"))
		!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","ARR_TO",CFG_STRING,&prgCfg.arr_to,CFG_NUM,"120"))
		!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","CHECK_INTERVAL",CFG_STRING,&prgCfg.check_interval,CFG_NUM,"120"))
		!= RC_SUCCESS)
		{return RC_FAIL;}
		else
		{	tgInterval=(time_t)(atoi(prgCfg.check_interval));}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","TIME_BELTOPEN",CFG_STRING,&prgCfg.time_beltopen,CFG_NUM,"0"))
		!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","TIME_BELTCLOSE",CFG_STRING,&prgCfg.time_beltclose,CFG_NUM,"0"))
		!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","AUTO_DELETE",CFG_STRING,&prgCfg.auto_delete,CFG_NUM,"0"))
		== RC_SUCCESS)
		{ tgAutoDelete=SECONDS_PER_MINUTE * ((time_t)(atoi(prgCfg.auto_delete)));}
	dbg(TRACE,"--------------------------------");
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","DEBUG_LEVEL",CFG_STRING,&prgCfg.debug_level,CFG_NUM,"0"))
		== RC_SUCCESS)
	{
		if (atoi(prgCfg.debug_level)==0)
		{
			dbg(TRACE,"GetCfgEntry: debug info = OFF after init-phase!");
			debug_level = 0;
		}
		else
		{
			if (atoi(prgCfg.debug_level)==1)
			{
				dbg(TRACE,"GetCfgEntry: debug info = TRACE after init-phase!");
				debug_level = TRACE;
			}
			else
			{
				if (atoi(prgCfg.debug_level)==2)
				{
					dbg(TRACE,"GetCfgEntry: debug info = DEBUG after init-phase!");
					debug_level = DEBUG;
				}
				else
				{
					dbg(TRACE,"GetCfgEntry: debug info = OFF after init-phase!");
					debug_level = 0;
				}
			}
		}
	}
	else
	{
		dbg(TRACE,"GetCfgEntry: debug info = OFF after init-phase!");
		debug_level = 0;
	}
	igDebugLevel = debug_level;
	return ilRc;
}
/* *********************************************************************/
/* The GetCfgEntry() routine   																				 */
/* *********************************************************************/
static int GetCfgEntry(char *pcpFile,char *pcpSection,char *pcpEntry,short spType,
												char **pcpDest,int ipValueType,char *pcpDefVal)
{
	int ilRc = RC_SUCCESS;
	char pclCfgLineBuffer[L_BUFF];
	
	memset(pclCfgLineBuffer,0x00,L_BUFF);
	if ((ilRc=iGetConfigRow(pcpFile,pcpSection,pcpEntry,spType,
													pclCfgLineBuffer)) != RC_SUCCESS)
	{
		dbg(TRACE,"GetCfgEntry: reading entry <%s> failed.",pcpEntry);
	}
	else
	{
		if (strlen(pclCfgLineBuffer) < 1)
		{
			dbg(TRACE,"GetCfgEntry: EMPTY %s! Use default <%s>.",pcpEntry,pcpDefVal);
			strcpy(pclCfgLineBuffer,pcpDefVal);
		}
		*pcpDest = malloc(strlen(pclCfgLineBuffer)+1);
		strcpy(*pcpDest,pclCfgLineBuffer);
		dbg(TRACE,"GetCfgEntry: %s = <%s>",pcpEntry,*pcpDest);
		if ((ilRc = CheckValue(pcpEntry,*pcpDest,ipValueType)) != RC_SUCCESS)
		{
			dbg(TRACE,"GetCfgEntry: please correct value <%s>!",pcpEntry); 	
		}
	}
	return ilRc;
}
/* ******************************************************************** */
/* The CheckValue() routine																						*/
/* ******************************************************************** */
static int CheckValue(char *pcpEntry,char *pcpValue,int ipType)
{
	int ilRc = RC_SUCCESS;

	switch(ipType)
	{
		case CFG_NUM:
			while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
			{
				if (isdigit(*pcpValue)==0)
				{
					dbg(TRACE,"CheckValue : NOT A NUMBER! <%s>!"
						,pcpEntry); 
					ilRc = RC_FAIL;
				}
				pcpValue++;
			}
			break;
		case CFG_ALPHA:
			while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
			{
				if (isalpha(*pcpValue)==0)
				{
					dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!"
						,*pcpValue,pcpEntry); 
					ilRc = RC_FAIL;
				}
				pcpValue++;
			} 
			break;
		case CFG_ALPHANUM:
			while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
			{
				if (isalnum(*pcpValue)==0)
				{
					dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!"
						,*pcpValue,pcpEntry); 
					ilRc = RC_FAIL;
				}
				pcpValue++;
			}
			break;
		case CFG_PRINT:
			while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
			{
				if (isprint(*pcpValue)==0)
				{
					dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!"
						,*pcpValue,pcpEntry); 
					ilRc = RC_FAIL;
				}
				pcpValue++;
			}
			break;
		default:
			break;
	}
	return ilRc;
}
/* ******************************************************************** */
/* The CheckCfg() routine																						*/
/* ******************************************************************** */
static int CheckCfg(void)
{
	int ilRc = RC_SUCCESS;
	if (*prgCfg.dys_host1 == 0x00 || *prgCfg.dys_host2 == 0x00)
	{
		dbg(TRACE,"CheckCfg: missing DYS_HOST-name in <%s.cfg>!",pcgProcessName);
		return RC_FAIL;
	}
	/* MODE */
	if (strcmp(prgCfg.mode,"REAL") != 0 &&
			strcmp(prgCfg.mode,"TEST") != 0)
	{
			dbg(TRACE,"CheckCfg: MODE <%s> unknown!",prgCfg.mode);
			dbg(TRACE,"CheckCfg: Now using default \"REAL\"!");
			*prgCfg.mode = 0x00;
			strcpy(prgCfg.mode,"REAL");
	}
	/* service to use */
	/* client operating system */
	if (strcmp(prgCfg.ftp_client_os,"WIN") != 0 &&
			strcmp(prgCfg.ftp_client_os,"UNIX")!= 0)
	{
			dbg(TRACE,"CheckCfg: FTP_CLIENT_OS <%s> unknown!",prgCfg.ftp_client_os);
			dbg(TRACE,"CheckCfg: Now using default \"UNIX\"!");
			*prgCfg.ftp_client_os = 0x00;
			strcpy(prgCfg.ftp_client_os,"UNIX");
	}

	if (strcmp(prgCfg.after_ftp,"D") != 0 &&
			strcmp(prgCfg.after_ftp,"R") != 0 &&
			strcmp(prgCfg.after_ftp,"K") != 0) 
	{
			dbg(TRACE,"CheckCfg: AFTER_FTP <%s> invalid!",prgCfg.after_ftp);
			dbg(TRACE,"CheckCfg: Now using default \"D\" (for delete)!");
			*prgCfg.after_ftp = 0x00;
			strcpy(prgCfg.after_ftp,"D");
	}
	return ilRc;
}
/* ******************************************************************** */
/* The FreeCfgMem() routine	  	                                    */
/* ******************************************************************** */
static void FreeCfgMem(void)
{
	/* entries from dysif.cfg file*/
	free(prgCfg.mode);
	free(prgCfg.my_sys_id);
	free(prgCfg.dys_host1);
	free(prgCfg.dys_host2);
	free(prgCfg.service_port);
	free(prgCfg.wait_for_ack);
	free(prgCfg.recv_timeout);
	free(prgCfg.max_sends);
	free(prgCfg.recv_log);
	free(prgCfg.send_log);
	free(prgCfg.try_reconnect);
	free(prgCfg.remote_sys_id);
	free(prgCfg.db_arr_filter);
	free(prgCfg.db_dep_filter);
	free(prgCfg.fs_offset);
	free(prgCfg.fs_local_path);
	free(prgCfg.fs_remote_path);
	free(prgCfg.ftp_client_os);
	free(prgCfg.ftp_user);
	free(prgCfg.ftp_pass);
	free(prgCfg.after_ftp);
	free(prgCfg.arr_from);
	free(prgCfg.arr_to);
	free(prgCfg.check_interval);
	free(prgCfg.time_beltopen);
	free(prgCfg.time_beltclose);
	free(prgCfg.auto_delete);
}
/* ****************************************************************** */
/* The ConnectTCP routine							*/
/* ****************************************************************** */
static int ConnectTCP (char *pcpHost,int ipTry)
{
  int	ilRc = RC_FAIL;	/*Return code*/
	time_t tlNow = 0;

	tlNow = time(NULL);
	if (*pcpHost != 0x00)
	{
		/* try to establish the connection */
		if ((bgSocketOpen==FALSE) && (tlNow > tgNextConnect))
		{
			dbg(DEBUG,"ConnectTCP: Try Nr.%d!",ipTry);
	
			igSock = tcp_create_socket(SOCK_STREAM, NULL); /* create the socket*/
			if (igSock < 0)
			{
				dbg(TRACE,"ConnectTCP: Create socket failed: %s.",strerror(errno));
				ilRc = RC_FAIL;
			}
			else
			{
				dbg(DEBUG,"ConnectTCP: Create socket <ID=%d> successfull.",igSock);
				alarm(CONNECT_TIMEOUT);
				ilRc = tcp_open_connection(igSock,prgCfg.service_port,pcpHost);
				alarm(0);
				if (ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"ConnectTCP: connect to <%s> failed: <%s> ",
							pcpHost,strerror(errno)); 
					shutdown(igSock,2);
					close(igSock);
					bgSocketOpen = FALSE;
					ilRc = RC_FAIL;
					if (ipTry == 2)
					{
						tlNow = time(NULL);
						tgNextConnect = tlNow + atol(prgCfg.try_reconnect);
						dbg(DEBUG,"ConnectTCP: next try in %ld sec.!",(tgNextConnect-tlNow));
					}
				}
				else
				{
					bgSocketOpen = TRUE;
					igMsgCounter = 0;
					ilRc = RC_SUCCESS;
					dbg(TRACE,"ConnectTCP: open connection to <%s> successfull.",
							pcpHost); 
					tlNow = time(NULL);
					tgRecvTimeOut = tlNow + (time_t)(igRecvTimeout - READ_TIMEOUT);
				}
			}
		}
	}
	else
	{
		dbg(TRACE,"ConnectTCP: wrong DYS_HOST <%s> entry!",pcpHost);
	}
	return ilRc;
} 
/* ******************************************************************** */
/* Following the poll_q_and_sock function				*/
/* Waits for input on the socket and polls the QCP for messages*/
/* ******************************************************************** */
static int poll_q_and_sock () 
{
  int ilRc;
  int ilRc_Connect = RC_FAIL;
	time_t tlNow; 

  do
	{
		nap(100); /* waiting because of NOT-waiting QUE_GETBIGNW */
		/*---------------------------*/
		/* now looking on ceda-queue */
		/*---------------------------*/
		ilRc = RC_NOT_FOUND;
		if (ilRc == RC_NOT_FOUND)
    {
      while ((ilRc=que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen
											,(char *) &prgItem)) == RC_SUCCESS)
			{
				/* depending on the size of the received item  */
				/* a realloc could be made by the que function */
				/* so do never forget to set event pointer !!! */
				prgEvent = (EVENT *) prgItem->text;

				/* Acknowledge the item */
				ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
				if( ilRc != RC_SUCCESS ) 
				{
					/* handle que_ack error */
					HandleQueErr(ilRc);
				}

				switch(prgEvent->command)
				{
				case	HSB_STANDBY	:
					ctrl_sta = prgEvent->command;
					dbg(TRACE,"PQS: received HSB_STANDBY event!");
					HandleQueues();
					break;	
				case	HSB_COMING_UP	:
					ctrl_sta = prgEvent->command;
					dbg(TRACE,"PQS: received HSB_COMING_UP event!");
					HandleQueues();
					break;	
				case	HSB_ACTIVE	:
					ctrl_sta = prgEvent->command;
					dbg(TRACE,"PQS: received HSB_ACTIVE event!");
					break;	
				case	HSB_ACT_TO_SBY	:
					ctrl_sta = prgEvent->command;
					dbg(TRACE,"PQS: received HSB_ACT_TO_SBY event!");
					HandleQueues();
					break;	
				case	HSB_DOWN	:
					/* 	whole system shutdown - do not further use que(), */
					/*	send_message() or timsch() ! */
					ctrl_sta = prgEvent->command;
					dbg(TRACE,"PQS: received HSB_DOWN event!");
					Terminate(FALSE);
					break;	
				case	HSB_STANDALONE	:
					ctrl_sta = prgEvent->command;
					dbg(TRACE,"PQS: received HSB_STANDALONE event!");
					/*ResetDBCounter();*/
					break;	
				case	REMOTE_DB :
					/* ctrl_sta is checked inside */
					/*HandleRemoteDB(prgEvent);*/
					break;
				case	SHUTDOWN	:
					/* process shutdown - maybe from uutil */
					dbg(TRACE,"PQS: received SHUTDOWN event!");
					CloseTCP();
					Terminate(FALSE);
					break;
				case	RESET		:
					dbg(TRACE,"PQS: received RESET event!");
					ilRc = Reset();
					if (ilRc == RC_FAIL)
					{
						Terminate(FALSE);
					}
					break;
				case	EVENT_DATA	:
					if((ctrl_sta == HSB_STANDALONE) ||
						(ctrl_sta == HSB_ACTIVE) ||
						(ctrl_sta == HSB_ACT_TO_SBY))
					{
						if (bgSocketOpen == TRUE ||
								strcmp(prgCfg.mode,"TEST") == 0)
						{
							ilRc = HandleInternalData();
							if(ilRc!=RC_SUCCESS)
							{
								dbg(TRACE,"PQS: HandleInternalData failed <%d>",ilRc);
								HandleErr(ilRc);
							}
						}
						else
						{
							dbg(TRACE,"PQS: No DYS-connection! Ignoring event!");
						}
					}
					else
					{
						dbg(TRACE,"poll_q_and_sock: wrong HSB-status <%d>",ctrl_sta);
						DebugPrintItem(TRACE,prgItem);
						DebugPrintEvent(TRACE,prgEvent);
					}/* end of if */
					break;
				case	TRACE_ON :
					dbg_handle_debug(prgEvent->command);
					break;
				case	TRACE_OFF :
					dbg_handle_debug(prgEvent->command);
					break;
				default			:
					dbg(TRACE,"MAIN: unknown event");
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;
				} /* end switch */
      }/* end while */ 
      ilRc = RC_FAIL; /* proceed */
    }

		if (bgSocketOpen==TRUE)
		{
			/*----------------------------------*/
			/* now looking for stored messages  */
			/*----------------------------------*/
			if (prgRecvMsgList->Count > 0)
			{
				dbg(DEBUG,"PQS: Stored messages (%d)",prgRecvMsgList->Count);
				HandleStoredData(prgRecvMsgList->Count);	
			}
			/*-----------------------*/
			/* now looking on socket */
			/*-----------------------*/
			/* We will only look on the socket if there are no more stored */
			/* messages. If there are messages on the socket, these will be stored */
			/* during the "WaitForAck"-function.*/
			if (prgRecvMsgList->Count < 1)
			{
				ilRc = Receive_data(igSock,READ_TIMEOUT,"MAIN");
			}

			/*------------------------------------------------------------------------*/
			/* now checking if the arrival flight information needs to be sent or not */
			/*------------------------------------------------------------------------*/
			tlNow = time(0);
			if (tlNow>tgNextArrivalInfoCheck)
			{
				SendArrivalFlightInfo();
				tgNextArrivalInfoCheck=tlNow + tgInterval;
			}
    }
		else
		{
			ilRc = RC_FAIL;
		}

		/* now trying to reconnect if there was a failure */
		if (ilRc == RC_FAIL)
		{
			CloseTCP(); /* for cleaning up the connections */

			/* toggeling between the two DYS-Servers to find the Master-Server */
			if ((ilRc_Connect = ConnectTCP(prgCfg.dys_host1,1)) != RC_SUCCESS)
			{
				if ((ilRc_Connect = ConnectTCP(prgCfg.dys_host2,2)) == RC_SUCCESS)
				{
					strcpy(pcgDYS_Host,prgCfg.dys_host2);
				}
				else
				{
					strcpy(pcgDYS_Host,prgCfg.dys_host1);
				}
			}
			else
			{
				strcpy(pcgDYS_Host,prgCfg.dys_host1);
			}

			if ((ilRc_Connect==RC_SUCCESS))
			{
				SendArrivalFlightInfo();
				ilRc = CreateChuteInfoFiles();
			}
		}
  }while (ilRc!=RC_SUCCESS);
	return ilRc;
} 
/* ***************************************************************** */
/* The Receive_data routine                                         */
/* Rec data from DYS into global buffer (and into a file )      */
/* ***************************************************************** */
static int Receive_data(int ipSock,int ipTimeOut,char *pcpCalledBy)
{
  int ilRc;
	int ilAlive = FALSE;
	int ilEomByte = 0;
	int ilNrOfBytes = 0;
	int ilBytesToRead = 0;
	int ilBytesAlreadyRead=0;
  char pclTmpBuffer[10];
	time_t tlNow = 0;

	/*dbg(DEBUG,"Receive_data: entering!");*/
	memset(prgRecvBuffer,0x00,sizeof(MAX_TELEGRAM_LEN));
	do
	{
		ilNrOfBytes = 0;
		bgAlarm = FALSE;
		memset(pclTmpBuffer,0x00,sizeof(pclTmpBuffer));

		errno = 0;
		alarm(ipTimeOut);
			ilNrOfBytes=read(ipSock,&pclTmpBuffer[0],1);
		alarm(0);

		/* <0 means failure at read if no alarm appeared, =0 means connection lost*/
		if (ilNrOfBytes <= 0 && bgAlarm == FALSE)
			{
				ilRc = RC_FAIL;
				dbg(TRACE,"Receive_data: OS-ERROR read(%d) on socket <%d>",ilNrOfBytes,ipSock);
				dbg(TRACE,"Receive_data: ERRNO (%d)=(%s)",errno,strerror(errno));
			}
		/* <0 means no data read inside READ_TIMEOUT time */
		if (ilNrOfBytes < 0 && bgAlarm == TRUE)
			{ilRc = RC_NOT_FOUND;}
		/* if the read byte is a start of message sign */
		if (ilNrOfBytes==1 && pclTmpBuffer[0]==SOM)
			{ilRc = RC_SUCCESS;}
		/* if the read byte is NOT a start of message sign */
		/* dummy-ilRc for not leaving the loop */
		if (ilNrOfBytes==1 && pclTmpBuffer[0]!=SOM)
			{ilRc = 1618;}
	}while(ilRc!=RC_SUCCESS && ilRc!=RC_NOT_FOUND && ilRc!=RC_FAIL);
	
	tlNow = time(0);
	if (ilRc == RC_SUCCESS)
	{
		tgRecvTimeOut = tlNow + (time_t)(igRecvTimeout - READ_TIMEOUT);
	}	
	if (tlNow > tgRecvTimeOut)
	{
		ilRc = RC_FAIL;
		dbg(TRACE,"Receive_data: RECV_TIMEOUT <%d> sec. reached! Disconnecting!",igRecvTimeout);
	}
	
	/*----------------------------------------*/
	/* reading the header of DYS-telegram */
	/*----------------------------------------*/
	if (ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"");
		dbg(TRACE,"--- START HandleExternalData ---");
		/* now copy SOM sign to global receive buffer */
		strncpy(prgRecvBuffer,pclTmpBuffer,1);
		ilNrOfBytes = 0;
		ilBytesAlreadyRead = 1;
		ilBytesToRead = igHeadLen-ilBytesAlreadyRead;
		do
		{
			alarm(ipTimeOut);
			ilNrOfBytes=read(ipSock,(prgRecvBuffer+ilBytesAlreadyRead),ilBytesToRead);
			alarm(0);
			if (ilNrOfBytes <= 0)
			{ if (ilNrOfBytes == 0)
				{ ilRc = RC_NOT_FOUND; }
				else
				{ if (bgAlarm == FALSE)
					{ ilRc = RC_FAIL;
						dbg(TRACE,"Receive_data: error (%d) (%s)!",
											errno,strerror(errno)); }
					else
					{ ilRc = RC_NOT_FOUND;
						bgAlarm = FALSE; }
				}
			}
			else
			{ 
				if (strncmp((char*)&prgRecvBuffer[1],ALIVE_ID,ALIVE_LEN) == 0)
				{
					ilAlive = TRUE;
    			ilRc = RC_SUCCESS;
				}
				if (ilAlive == FALSE)
				{
					if (ilNrOfBytes != igHeadLen-1)
						dbg(DEBUG,"Receive_data: read <%d> more header-bytes.",ilBytesToRead);
					ilBytesToRead -= ilNrOfBytes;
					ilBytesAlreadyRead += ilNrOfBytes;
					ilRc = RC_SUCCESS;
				}
			}
		}while (ilRc==RC_SUCCESS && ilBytesToRead>0 && ilAlive==FALSE);
		
		/*--------------------------------------*/
		/* reading data of the DYS-telegram */
		/*--------------------------------------*/
		if (ilRc == RC_SUCCESS && ilAlive == FALSE)
		{
			ilNrOfBytes = 0;
			ilBytesToRead = 1;
			ilBytesAlreadyRead = 0;
			do
			{
				alarm(ipTimeOut);
				ilNrOfBytes = 
					read(ipSock,(&prgRecvBuffer[igHeadLen]+ilBytesAlreadyRead)
						,ilBytesToRead);
				alarm(0);
				if (ilNrOfBytes <= 0)
				{
					if (ilNrOfBytes == 0)
					{
						ilRc = RC_NOT_FOUND;
					}
					else
					{
						if (bgAlarm == FALSE) 
						{
							ilRc = RC_FAIL;
							dbg(TRACE,"read: error (%d) (%s)!",
									errno,strerror(errno));
						}
						else
						{
							ilRc = RC_NOT_FOUND;
							bgAlarm = FALSE;
						}
					}
				}
				else
				{
					ilBytesAlreadyRead += ilNrOfBytes;
					ilRc = RC_SUCCESS;
				}
			}while((prgRecvBuffer[igHeadLen+ilBytesAlreadyRead-1] != EOT)  &&
							(ilBytesAlreadyRead < MAX_TELEGRAM_LEN-1));

			/* Setting end-mark of the telegram */ 
			ilEomByte = igHeadLen+ilBytesAlreadyRead-1;
			prgRecvBuffer[ilEomByte] = 0x00;
			snapit((char*)prgRecvBuffer,ilEomByte+1,outp);

			/* Now cleaning message from unallowed bytes (0x00 and '_' - bytes) */
			/* until one byte before the end-NULL(0x00)-byte */
			CleanMessage(prgRecvBuffer,ilEomByte);
		}
	}
 	
	 
	if (ilRc==RC_SUCCESS && ilAlive == FALSE)
  {
		dbg(DEBUG,"Receive_data: <%s>. Bytes <%d+1>. Socket <%d>."
			,pcpCalledBy,ilEomByte,ipSock);
    if (pgReceiveLogFile)
    {
			fwrite(prgRecvBuffer,sizeof(char),ilEomByte,pgReceiveLogFile);
			fwrite("\n",sizeof(char),1,pgReceiveLogFile);
			fflush(pgReceiveLogFile);
    }
    ilRc = RC_SUCCESS;
  }
  return ilRc;
}
/* ***************************************************************** */
/* The CloseTCP routine                                    */
/* ***************************************************************** */
static void CloseTCP()
{
	time_t tlNow = 0;
	*pcgDYS_Host = 0x00;

	if (igSock != 0)
	{
		dbg(DEBUG,"CloseTCP: closing TCP/IP connection!");
		shutdown(igSock,2);
		close(igSock);
		tlNow = time(NULL);
		tgNextConnect = tlNow + atol(prgCfg.try_reconnect);
		dbg(TRACE,"CloseTCP: connection closed! Wait (%s) sec. before reconnect"
			,prgCfg.try_reconnect);
	}
	igSock = 0;
	bgSocketOpen = FALSE;
	/* upon reconnect we have to send the arrival infos */
	tgNextArrivalInfoCheck = 0;	
}
/* ***************************************************************** */
/* The MakeEnd routine                                          */
/* ***************************************************************** */
static void MakeEnd()
{	
	memset((char*)&prgEnd,0x00,igTailLen);
	prgEnd.tail[0] = 0x55; /* 'U' like aggreed with Mannesmann */
	prgEnd.tail[1] = 0x55; /* 'U' like aggreed with Mannesmann */
	prgEnd.tail[2] = 0xD; 
	prgEnd.tail[3] = EOT; /* EOT = End Of Telegram define (char)0x03 */
	prgEnd.tail[4] = 0x00;
}
/* ******************************************************************** */
/* The HandleStoredData routine                                   */
/* ******************************************************************** */
static void HandleStoredData(int ipTimesToDo)
{
	int ilRc;
	int ilRecvCmd=0;
	int ilCnt=0;
	LPLISTELEMENT prlEle = NULL;
	MSG_RECV* prlMsg = NULL;
			
	while((prlEle=ListFindFirst(prgRecvMsgList)) && (ilCnt<ipTimesToDo))
	{
		prlMsg = (MSG_RECV*)prlEle->Data;
		dbg(TRACE,"HandleStoredData: (%d) Nr.<%s> <%x>"
			,prlMsg->ilMsgNr,prlMsg->msg,prlMsg->msg);
		if (ilRc=CheckHeader(prlMsg->msg,&ilRecvCmd) == RC_SUCCESS)
		{
			if (ilRc = ProcessExtData(prlMsg->msg,ilRecvCmd,FALSE)!=RC_SUCCESS)
			{
				dbg(TRACE,"HandleStoredData: ProcessExtData() failed!");
			}
		}
		free(prlMsg->msg);
		ListDeleteFirst(prgRecvMsgList);		
		ilCnt++;
	}
}
/* **************************************************************** */
/* The snapit routine                                           */
/* snaps data if the debug-level is set to DEBUG                */
/* **************************************************************** */
static void snapit(void *pcpBuffer,long lpDataLen,FILE *pcpDbgFile)
{
	if (debug_level==DEBUG)
	{
		snap((char*)pcpBuffer,(long)lpDataLen,(FILE*)pcpDbgFile);
	}
}
/*********************************************************************
Function	 :CleanMessage(char *pcpMsg,int ipMsgLen)
Paramter	 :IN:pcpMsg = Message received directly after the socket read.
						IN:ipMsgLen=length of the message
Returnvalue:none
Description:Replaces unallowed bytes from the original telegram
						(especially '\0' and '_') with "allowed" blanks
*********************************************************************/
static void CleanMessage(char *pcpMsg,int ipMsgLen)
{
	int ilCnt = 0;
	while(ilCnt < ipMsgLen)
	{
		
		/* '_' are allowed till Dec. 2000 */
		/*if (pcpMsg[ilCnt]=='_' || pcpMsg[ilCnt]==0x00)*/
		if (pcpMsg[ilCnt]==0x00)
		{
			pcpMsg[ilCnt]=' ';
		}
		ilCnt++;
	}
}
/* **************************************************************** */
/* The HandleExternalData routine                                   */
/* Checks the data wich have been received from the ADS-system     */
/* **************************************************************** */
static int HandleExternalData(char *pcpData)
{	
	int ilRc = RC_SUCCESS;
	int ilRecvCmd = 0;

	if (bgSocketOpen == TRUE)
	{
		if ((ilRc = CheckHeader(pcpData,&ilRecvCmd)) == RC_SUCCESS)
		{
			if (ilRecvCmd == MSG_ALV)
			{
				dbg(DEBUG,"HandleExternalData: ALIVE-telegram (%d) received!",MSG_ALV);
				Send_New_Alive();
				ilRc = RC_SUCCESS;
			}
			else
			{
				ilRc = ProcessExtData(pcpData,ilRecvCmd,TRUE);
			}
		}
	}
	dbg(TRACE,"--- END   HandleExternalData ---");
	return ilRc;
}
/* **************************************************************** */
/* The CheckHeader routine                                          */
/* Checks the structure and contents of the received telegram-header*/
/* and sets the received command to an integer value                */
/* **************************************************************** */
static int CheckHeader(char *pcpData,int *ipRecvCmd)
{
	int ilRc = RC_SUCCESS;
	char pclCmd[5];
	char pclSender[5];
	char pclReceiver[5];
	HEAD pclRecvHead;

	if (pcpData[0] == SOM)
	{
		memset((char*)&pclRecvHead,0x00,igHeadLen);
		memcpy((char*)&pclRecvHead,pcpData,igHeadLen);
		memset(pclCmd,0x00,sizeof(pclCmd));
		strncpy(pclCmd,pclRecvHead.t_type,CMD_LEN);
		if ((ilRc = CheckValue("Telegram type",pclCmd,CFG_NUM)) == RC_SUCCESS)
		{
			*ipRecvCmd = atoi(pclCmd);
			dbg(TRACE,"CheckHeader: Command <%d> received!",*ipRecvCmd);
			memset(pclSender,0x00,5);
			strncpy(pclSender,pclRecvHead.sender,2);
			if (strncmp(pclSender,prgCfg.remote_sys_id,2) == 0)
			{
				memset(pclReceiver,0x00,sizeof(pclReceiver));
				strncpy(pclReceiver,pclRecvHead.receiver,2);
				if (strncmp(pclReceiver,prgCfg.my_sys_id,2) == 0)
				{
					if (*ipRecvCmd != MSG_ACK &&
							*ipRecvCmd != MSG_ALV)
					{
							dbg(TRACE,"CheckHeader: unknown command <%d> received!",*ipRecvCmd);
							*ipRecvCmd = 0;
							ilRc = RC_FAIL;
					}
					else
					{
						tgRecvTimeOut = time(0) + (time_t)(igRecvTimeout - READ_TIMEOUT);
					}
				}
				else
				{
					dbg(TRACE,"CheckHeader: wrong MY_SYS_ID <%s> received!",pclReceiver);
					ilRc = RC_FAIL;
				}
			}
			else
			{
				dbg(TRACE,"CheckHeader: wrong REMOTE_SYS_ID <%s> received!",pclSender);
				ilRc = RC_FAIL;
			}
		}
		else
		{
			dbg(TRACE,"CheckHeader: invalid telegram type <%s> received!",pclCmd);
			ilRc = RC_FAIL;
		}
	}
	else
	{
		dbg(TRACE,"CheckHeader: wrong SOM-sign received!");
		ilRc = RC_FAIL;
	}
	return ilRc;
}
/* *****************************************************************/
/* The ProcessExtData routine                                 	*/
/* Checks the data of the received telgram                         */
/* *****************************************************************/
static int ProcessExtData(char *pcpData,int ipCmd,BOOL bpAck)
{
	int ilRc = RC_SUCCESS;
	int ilDataLen = 0;
	char pclRecvData[MAX_TELEGRAM_LEN];

	memset(pclRecvData,0x00,MAX_TELEGRAM_LEN);
	ilDataLen=(int)strlen((char*)&pcpData[igHeadLen])+1;
	strncpy(pclRecvData,&pcpData[igHeadLen],ilDataLen);
	switch(ipCmd)
	{
		case MSG_ACK:
			dbg(TRACE,"ProcessExtData: ACK received!");
			break;
		default:
			dbg(TRACE,"ProcessExtData: unknown command <%d> received!",ipCmd);
			break;
	}
	return ilRc;
}
/* ******************************************************************** */
/* The Send_New_Alive()         										*/
/* ******************************************************************** */
static void Send_New_Alive()
{
	int ilDataLen = 0;
	int ilRc = RC_SUCCESS;
	ALIVE rlAliveMsg;
	
	memset(&rlAliveMsg,0x00,sizeof(ALIVE));
	/* HEADER */
	*(rlAliveMsg.head.start) = SOM;
	*(rlAliveMsg.head.cycle_no) = '0';
	sprintf(rlAliveMsg.head.t_type,"%02d",MSG_ALV);
	strncpy(rlAliveMsg.head.sender,prgCfg.my_sys_id,2);
	strncpy(rlAliveMsg.head.receiver,prgCfg.remote_sys_id,2);
	strncat((char*)&rlAliveMsg.head.order_no,"  ",2);
	/* TAIL */
	GetMsgTimeStamp(rlAliveMsg.tail.time);
	strncpy(rlAliveMsg.tail.tail,prgEnd.tail,4);
	ilDataLen = strlen((char*)&rlAliveMsg);

	if ((ilRc = Send_data(igSock,(char*)&rlAliveMsg,ilDataLen))==RC_FAIL)
	{
		CloseTCP();
	}
}
/* ******************************************************************** */
/* The Send_Ack_Msg()         										*/
/* ******************************************************************** */
static void Send_Ack_Msg()
{
	int ilDataLen = 0;
	int ilRc = RC_SUCCESS;
	HEAD *pclRecvHead;
	ACK_EXT rlAckMsg;
	memset((char*)&rlAckMsg,0x00,igAckLen);

	/* HEADER */
	*(rlAckMsg.head.start) = SOM;
	*(rlAckMsg.head.cycle_no) = '0';
	sprintf(rlAckMsg.head.t_type,"%02d",MSG_ACK);
	strncpy(rlAckMsg.head.sender,prgCfg.my_sys_id,2);
	strncpy(rlAckMsg.head.receiver,prgCfg.remote_sys_id,2);
	pclRecvHead = (HEAD*)prgRecvBuffer;
	strncpy(rlAckMsg.head.order_no,pclRecvHead->order_no,2);
	/* TAIL */
	GetMsgTimeStamp(rlAckMsg.tail.time);
	strncpy(rlAckMsg.tail.tail,prgEnd.tail,4);
	ilDataLen = strlen((char*)&rlAckMsg);

	if ((ilRc = Send_data(igSock,(char*)&rlAckMsg,ilDataLen))==RC_FAIL)
	{
		CloseTCP();
	}
}
/* ******************************************************************** */
/* The Send_FTF_Msg()         										*/
/* ******************************************************************** */
static int Send_FTF_Msg(char *pcpFileName)
{
	int ilDataLen = 0;
	int ilLen = 0;
	int ilRc = RC_SUCCESS;
	char pclFileName[16];
	char pclSendBuffer[XL_BUFF];

	memset(pclFileName,0x00,16);
	memset(pclSendBuffer,0x00,XL_BUFF);
	ilLen = strlen(pcpFileName);
	pcpFileName[ilLen-4] = 0x00;
	strcat(pcpFileName,".TBL");
	strncpy(pclFileName,pcpFileName,12);

	if ((ilRc = MakeSendBuffer(TRUE,MSG_FTF,pclFileName,pclSendBuffer)) == RC_SUCCESS)
	{
		ilDataLen = (int)strlen(pclSendBuffer);
		dbg(DEBUG,"Send_FTF_Msg: <%s>",pclSendBuffer);
		if ((ilRc=Send_data(igSock,pclSendBuffer,ilDataLen))==RC_FAIL)
		{
			CloseTCP();
		}
		else
		{
			ilRc = WaitForAck(igSock,pclSendBuffer,ilDataLen);
		}
	}
	return ilRc;
}
/* ***************************************************************** */
/* The GetMsgNumber routine                                          */
/* ***************************************************************** */
static void GetMsgNumber(char *pcpMsgNumber) 
{
	*pcpMsgNumber  = '\0';
	if (igMsgCounter > 99)
	{
		/*dbg(DEBUG,"GetMsgNumber: reseting MSG-telegram number to zero.");*/
		igMsgCounter = 1;
	}
	sprintf(pcpMsgNumber ,"%02d",igMsgCounter);
	/*dbg(DEBUG,"GetMsgNumber: <%d> => (%s)",igMsgCounter,pcpMsgNumber);*/
	igMsgCounter++;	
}
/* ***************************************************************** */
/* The GetMsgTimeStamp routine                                          */
/* ***************************************************************** */
static void GetMsgTimeStamp(char *pcpTimeStr) 
{
	char pclNow[20];
	*pcpTimeStr = 0x00;

	TimeToStr(0,pclNow,0,4);
	strncpy(pcpTimeStr,pclNow,9);
}
/* ******************************************************************** */
/* The FormatDate() routine                                         */
/* ******************************************************************** */
static int FormatDate(char *pcpOldDate,char *pcpFormat,char *pcpNewDate)
{
	int ilRc = RC_SUCCESS;
	int ilCfgChar = 0;
	int ilRightChar = 0;
	int ilDateChar = 0;
	char pclOldDate[20];
	char pclValidLetters[] = "YMDHIS";
	char pclRightFormat[20];
	char *pclTmpDate = NULL;
	
	TrimRight(pcpOldDate);
	memset(pclOldDate,0x00,sizeof(pclOldDate));
	memset(pclRightFormat,0x00,sizeof(pclRightFormat));
	*pcpNewDate = 0x00;
	
	if (strlen(pcpOldDate) > 14)
	{
		return RC_FAIL;	
	}else
	{
		strcpy(pclOldDate,pcpOldDate);
		TrimRight(pclOldDate);
	}

	/* adding '0' until the length of the date is 14byte */ 
	if (strlen(pclOldDate) < 14)
	{
		while(strlen(pclOldDate) < 14)
		{
			pclOldDate[strlen(pclOldDate)] = '0';
			pclOldDate[strlen(pclOldDate)+1] = 0x00;
		}
	}
	
	/* removing all unallowed letters from pcpFormat-string */
	ilCfgChar=0;
	while(ilCfgChar < (int)strlen(pcpFormat))
	{
		if (strchr(pclValidLetters,pcpFormat[ilCfgChar]) != NULL)
		{
			pclRightFormat[ilRightChar] = pcpFormat[ilCfgChar];
			ilRightChar++;
		}
		ilCfgChar++;
	}

	/* now formatting CEDA-time format from pclOldDate to right format */	
	if ((pclTmpDate =
			GetPartOfTimeStamp(pclOldDate,pclRightFormat)) != NULL)
	{
		/* now changing the layout like it is in the cfg-file */	
		ilCfgChar = 0;
		ilRightChar = 0;
		ilDateChar = 0;
		while(ilCfgChar < (int)strlen(pcpFormat))
		{
			if (strchr(pclValidLetters,pcpFormat[ilCfgChar]) != NULL)
			{
				(pcpNewDate)[ilDateChar] = pclTmpDate[ilRightChar];
				ilRightChar++;
				ilDateChar++;
			}
			else
			{
				(pcpNewDate)[ilDateChar] = pcpFormat[ilCfgChar];
				ilDateChar++;
			}
			ilCfgChar++;
		}
		(pcpNewDate)[strlen(pcpFormat)] = 0x00;
	}
	else
	{
		ilRc = RC_FAIL;
	}
	/*dbg(DEBUG,"FormatDate: Old=<%s> New=<%s>",pclOldDate,pcpNewDate);*/
	return ilRc;	
}
/* ******************************************************************** */
/* The TrimRight() routine																						*/
/* ******************************************************************** */
static void TrimRight(char *pcpBuffer)
{
    int i = 0;
    for (i = strlen(pcpBuffer); i > 0 && isspace(pcpBuffer[i-1]); i--)  
        ;
    pcpBuffer[i] = '\0';    
}
/* **************************************************************** */
/* The Send_data routine                                            */
/* Sends (header & data & tail) to the DYS-system                   */
/* **************************************************************** */
static int Send_data(int ipSock,char *pcpData,int ipLen)
{
  int ilRc=RC_SUCCESS;
  int ilBytes = 0;
		
  /* ipSock = opened socket */
	/* ilBytes = number of bytes that have to be transmitted. */
	ilBytes = ipLen;
	if (ipSock != 0)
	{
		errno = 0;
		alarm(WRITE_TIMEOUT);
		ilRc = write(ipSock,pcpData,ilBytes);/*unix*/
		alarm(0);

		if (ilRc == -1)
		{
			if (bgAlarm == FALSE)
			{
				dbg(TRACE,"Send_data: Write failed: Socket <%d>! <%d>=<%s>"
					,ipSock,errno,strerror(errno)); 
				ilRc = RC_FAIL;
			}
			else
			{
				bgAlarm = FALSE;
				ilRc = RC_FAIL;
			}
		}
		else
		{
			dbg(DEBUG,"Send_data: wrote <%d> Bytes to socket Nr.<%d>.",ilRc,ipSock); 
			/*snapit((char *)pcpData,ilBytes+1,outp);*/
			fflush(outp);
			if (pgSendLogFile)
			{
				fwrite(pcpData,sizeof(char),ilBytes
							,pgSendLogFile);
				fwrite("\n",sizeof(char),1,pgSendLogFile);
				fflush(pgSendLogFile);
			}
			ilRc = RC_SUCCESS;
		}
	}
	else
	{
		if (strcmp(prgCfg.mode,"TEST") == 0)
		{
			dbg(DEBUG,"Send_data: wrote <%d> Bytes to dummy-socket.",ilBytes); 
			/*snapit((char *)pcpData,ilBytes+1,outp);*/
			fflush(outp);
			if (pgSendLogFile)
			{
				fwrite(pcpData,sizeof(char),ilBytes
							,pgSendLogFile);
				fwrite("\n",sizeof(char),1,pgSendLogFile);
				fflush(pgSendLogFile);
			}
			ilRc = RC_SUCCESS;
		}
		else
		{
			dbg(TRACE,"Send_data: No connection to <%s>! Can't send!"
				,prgCfg.remote_sys_id);
				ilRc = RC_FAIL;
		}
	}
  return ilRc;
} 
/* ******************************************************************** */
/*                       										*/
/* ******************************************************************** */
static int WaitForAck(int ipSock,char *pcpMsg,int ipLen)
{
	int ilRc;
	int ilCnt = 1; /* send-counter set to 1 because telegram has already been send once */
	int ilRecvCmd = 0;

	dbg(DEBUG,"WaitForAck: TRUE! Socket <%d>",ipSock);
	do
	{
		if ((ilRc=Receive_data(ipSock,igWaitForAck,"WFA")) == RC_SUCCESS)
		{
			if ((ilRc=CheckHeader(prgRecvBuffer,&ilRecvCmd)) == RC_SUCCESS)
			{
				if (ilRecvCmd!=MSG_ACK && ilRecvCmd!=MSG_ALV)
				{
					StoreRecvMessage(prgRecvBuffer);
				}
				else
				{
					if (ilRecvCmd==MSG_ALV)
					{
						dbg(DEBUG,"WaitForAck: received ALIVE-telegram!");
						Send_New_Alive();
					}
					else /* received message was an ACK */
					{
						dbg(DEBUG,"WaitForAck: FALSE! Socket <%d>",ipSock);
						bgWaitForAck = FALSE;
						ilRc = RC_SUCCESS;
					}
				}
			}
		}
		else
		{
			if (ilRc == RC_NOT_FOUND)
			{
				dbg(TRACE,"WaitForAck: last message wasn't acked!");
				bgWaitForAck = TRUE;
			}
			else
			{
				dbg(TRACE,"WaitForAck: Receive_data() returns <%d>",ilRc);
				bgWaitForAck = FALSE;
			}
		}

		/* resending telegram if no MSG_ACK was received */
		if (bgWaitForAck==TRUE && ilCnt<igMaxSends)
		{
			if ((ilRc=Send_data(igSock,pcpMsg,ipLen))==RC_FAIL)
			{
				dbg(TRACE,"WaitForAck: resending with Send_data() returns <%d>",ilRc);
				ilRc = RC_FAIL;
				bgWaitForAck = FALSE;
				CloseTCP();
			}
			else /* increasing send-counter */
			{
				ilCnt++;
				ilRc = RC_FAIL;
				dbg(DEBUG,"WaitForAck: <%d.> send of max.<%d> sends done!",ilCnt,igMaxSends);
			}
		}
	}while(bgWaitForAck==TRUE && ilCnt<igMaxSends);
	/* setting to FALSE, so that one unacked messages doesn't disturb operation */
	bgWaitForAck = FALSE;
	if (ilRc == RC_FAIL)
	{
		if (strcmp(prgCfg.mode,"TEST") == 0)
			return RC_SUCCESS;
		else
			return RC_FAIL;
	}
	else
	{
		return RC_SUCCESS;
	}
}
/* ****************************************************************** */
/*                                                                    */
/*                                                                    */
/* ****************************************************************** */
static void StoreRecvMessage(char *pcpMsg)
{
	int ilLen = 0;
	int ilDataLen = 0;
	MSG_RECV rlMsg;
	char pclMsgNo[16];

	Send_Ack_Msg();

	memset(pclMsgNo,0x00,sizeof(pclMsgNo));
	strncpy(pclMsgNo,&pcpMsg[72],8);

	memset(&rlMsg,0x00,sizeof(MSG_RECV));
	ilDataLen = strlen(pcpMsg);
	ilLen = ilDataLen+5; /* +5 just for saftey */
	if ((rlMsg.msg=(char*)malloc(ilLen)) != NULL)
	{
		igCnt++;
		rlMsg.ilMsgNr = igCnt;
		memset(rlMsg.msg,0x00,ilLen);
		memcpy(rlMsg.msg,pcpMsg,ilDataLen);

		/* appending message to message list*/
		if (ListAppend(prgRecvMsgList,&rlMsg) == NULL)
		{
			dbg(TRACE,"StoreRecvMessage: ListAppend failed!");
			Terminate(FALSE);
		}
		else
		{
			dbg(TRACE,"StoreRecvMessage: (%d) Nr.<%s> <%x>"
				,rlMsg.ilMsgNr,pclMsgNo,rlMsg.msg);
		}
	}
	else
	{
		dbg(TRACE,"StoreRecvMessage: No memory for storeing availiable!");
	}
}
/* ****************************************************************** */
/* The PrepareData routine                                       */
/* prepares the data after the command for sending                    */
/* ****************************************************************** */
static int PrepareData(int ipCmd,char *pcpEventData,time_t tpDayToSend)
{
	int ilCnt = 0;
	int	ilType = 0;		
	int	ilDataLen = 0;	
	int ilRc = RC_SUCCESS;
	char pclDBSelection[L_BUFF];
	char *pclDBFields = NULL;
	time_t tlNow = time(NULL);
	time_t tlCurrentDay = time(NULL);
	time_t tlTimeFrom = time(NULL);
	time_t tlTimeTo = time(NULL);
	char pclNow[XS_BUFF];
	char pclUrno[XS_BUFF];
	char pclTimeFrom[XS_BUFF];
	char pclTimeTo[XS_BUFF];
	char pclLocalStamp[20];
	char pclLocalFile[S_BUFF];
  char pclSqlBuf[L_BUFF];
	char pclSendBuffer[XL_BUFF];
  char pclTmpSqlAnswer[XL_BUFF];
  char pclData[DATABLK_SIZE];
	short slFkt = 0;
	short slCursor = 0;
	
	switch(ipCmd)
	{
		case SAI:
				/*-----------------------*/
				/* setting the selection */
				/*-----------------------*/
				memset(pclDBSelection,0x00,L_BUFF);
				memset(pclTimeFrom,0x00,XS_BUFF);
				memset(pclTimeTo,0x00,XS_BUFF);

				/*TimeToStr(1,pclNow,tlNow,0);	
				StrToTime(1,pclNow,&tlNow);*/
				tlTimeFrom = tlNow - (SECONDS_PER_MINUTE*atoi(prgCfg.arr_from));
				/*TimeToStr(1,pclTimeFrom,tlTimeFrom,1)*/
				tlTimeTo = tlNow + (SECONDS_PER_MINUTE*atoi(prgCfg.arr_to));
				TimeToStr(1,pclTimeFrom,tlTimeFrom,1);	
				TimeToStr(1,pclTimeTo,tlTimeTo,1);	
				/* should HomePort(HOPO) database field be used or not ? */
				if (bgUseHopo == TRUE)
				{
					sprintf(pclDBSelection,"WHERE HOPO = '%s' AND TIFA BETWEEN '%s' AND '%s' %s "
						,pcgHomeAp,pclTimeFrom,pclTimeTo,prgCfg.db_arr_filter);
				}
				else
				{
					sprintf(pclDBSelection,"WHERE TIFA BETWEEN '%s' AND '%s' %s"
						,pclTimeFrom,pclTimeTo,prgCfg.db_arr_filter);
				}
				memset(pclLocalFile,0x00,S_BUFF);
				sprintf(pclLocalFile,"%s.TBL",pclLocalStamp);
				sprintf(pclSqlBuf,"SELECT %s FROM AFT%s %s",pcgValid_SAI_Fields,pcgTabEnd,pclDBSelection);
				slFkt = START; 
				slCursor = 0;
				memset(pclTmpSqlAnswer,0x00,XL_BUFF);
				memset(pclData,0x00,DATABLK_SIZE);
				dbg(DEBUG,"PrepareData: (SAI)-SQL <%s>",pclSqlBuf);
				while ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))==RC_SUCCESS)
				{
					ilCnt++;
					BuildItemBuffer(pclTmpSqlAnswer,pcgValid_SAI_Fields,0,",");
					strcat(pclTmpSqlAnswer,"\n");
					strcat(pclData,pclTmpSqlAnswer);
					slFkt = NEXT;
				}
				close_my_cursor(&slCursor);
				/* delete the last "\n" for not confusing the GetNoOfElements()-function */
				pclData[strlen(pclData)-1] = 0x00;

				if (ilCnt > 0 && (ilRc==NOTFOUND || ilRc==RC_SUCCESS))
				{
					memset(pclSendBuffer,0x00,XL_BUFF);
					if ((ilRc = ProcessIntData(pclData,SAI,pclSendBuffer,pclLocalFile)) != RC_SUCCESS)
					{
						dbg(TRACE,"PrepareData: ProcessIntData(SAI) failed!");
					}
				}
				else
				{
					dbg(TRACE,"PrepareData: sql_if() failed with = <%d> ",ilRc);
					dbg(TRACE,"PrepareData: Nr. of flights found = <%d> ",ilCnt);
				}
				break;
		case SSCI:
				if (tpDayToSend == 0)
				{
					tpDayToSend = tlCurrentDay + (time_t)(SECONDS_PER_DAY * atoi(prgCfg.fs_offset));
				}
				TimeToStr(1,pclLocalStamp,tpDayToSend,1);
				pclLocalStamp[8] = 0x00;
				memset(pclLocalFile,0x00,S_BUFF);
				sprintf(pclLocalFile,"%s.TBL",pclLocalStamp);
				strncat(pclLocalStamp,"%",1);

				/*dbg(DEBUG,"PrepareData: Date <%s>",pclLocalStamp);*/
				memset(pclDBSelection,0x00,L_BUFF);

				/* HomePort(HOPO) has not to be used because rurn references already the correct AFT-record*/
				sprintf(pclDBSelection,"WHERE %s AND (CHA%s.STOB LIKE '%s' OR CHA%s.STOE LIKE '%s') %s"
					,pcgRurnClause,pcgTabEnd,pclLocalStamp,pcgTabEnd,pclLocalStamp,prgCfg.db_dep_filter);

				sprintf(pclSqlBuf,"SELECT %s FROM AFT%s,CHA%s %s ORDER BY %s"
					,pcgValid_SSCI_Fields,pcgTabEnd,pcgTabEnd,pclDBSelection,pcg_SSCI_OrderClause);
				slFkt = START; 
				slCursor = 0;
				memset(pclTmpSqlAnswer,0x00,XL_BUFF);
				memset(pclData,0x00,DATABLK_SIZE);
				dbg(DEBUG,"PrepareData: (SSCI)-SQL<%s>",pclSqlBuf);
				while ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))==RC_SUCCESS)
				{
					ilCnt++;
					BuildItemBuffer(pclTmpSqlAnswer,pcgValid_SSCI_Fields,0,",");
					strcat(pclTmpSqlAnswer,"\n");
					strcat(pclData,pclTmpSqlAnswer);
					slFkt = NEXT;
				}
				close_my_cursor(&slCursor);
				/* delete the last "\n" for not confusing the GetNoOfElements()-function */
				pclData[strlen(pclData)-1] = 0x00;
				if (ilCnt > 0 && (ilRc==NOTFOUND || ilRc==RC_SUCCESS))
				{
					memset(pclSendBuffer,0x00,XL_BUFF);
					if ((ilRc = ProcessIntData(pclData,SSCI,pclSendBuffer,pclLocalFile)) == RC_SUCCESS)
					{
						/* transfering flight table to the DYS-system using FTPHDL */
						if ((ilRc = SendFileViaFtp(pcgDYS_Host,pclLocalFile)) != RC_SUCCESS)
						{
							dbg(TRACE,"PrepareData: SendFileViaFtp() to <%s> failed!",pcgDYS_Host); 
						}
					}
				}
				else
				{
					dbg(TRACE,"PrepareData: sql_if() failed with = <%d> ",ilRc);
					dbg(TRACE,"PrepareData: Nr. of flights found = <%d> ",ilCnt);
				}
				break;
		case FTU:
				sprintf(pclSqlBuf,"SELECT %s FROM AFT%s,CHA%s WHERE AFT%s.URNO=%s AND %s %s"
					,pcgValid_SACI_Fields,pcgTabEnd,pcgTabEnd,pcgTabEnd,pcpEventData,pcgRurnClause,prgCfg.db_dep_filter);
				slFkt = START; 
				slCursor = 0;
				memset(pclTmpSqlAnswer,0x00,XL_BUFF);
				memset(pclData,0x00,DATABLK_SIZE);
				dbg(DEBUG,"PrepareData: (SACI)-SQL<%s>",pclSqlBuf);
				while ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))==RC_SUCCESS)
				{
					ilCnt++;
					BuildItemBuffer(pclTmpSqlAnswer,pcgValid_SACI_Fields,0,",");
					strcat(pclTmpSqlAnswer,"\n");
					strcat(pclData,pclTmpSqlAnswer);
					slFkt = NEXT;
				}
				close_my_cursor(&slCursor);
				/* delete the last "\n" for not confusing the GetNoOfElements()-function */
				pclData[strlen(pclData)-1] = 0x00;
				if (ilCnt > 0 && (ilRc==NOTFOUND || ilRc==RC_SUCCESS))
				{
						dbg(DEBUG,"PrepareData: now calling PID(FTU)!");
						memset(pclSendBuffer,0x00,XL_BUFF);
						if ((ilRc = ProcessIntData(pclData,FTU,pclSendBuffer,""))
																			!= RC_SUCCESS)
						{
							dbg(TRACE,"PrepareData: ProcessIntData(SACI,FTU) failed!");
						}
				}
				else
				{
					dbg(TRACE,"PrepareData: sql_if() failed with = <%d> ",ilRc);
					dbg(TRACE,"PrepareData: Nr. of flights found = <%d> ",ilCnt);
				}
				break;
		case SACI:
				/*sprintf(pclSqlBuf,"SELECT %s FROM AFT%s,CHA%s WHERE CHA%s.URNO=%s AND %s %s ORDER BY %s"*/
					/*,pcgValid_SACI_Fields,pcgTabEnd,pcgTabEnd,pcgTabEnd,pcpEventData,pcgRurnClause,prgCfg.db_dep_filter,pcg_SACI_OrderClause);*/
				sprintf(pclSqlBuf,"SELECT %s FROM AFT%s,CHA%s WHERE CHA%s.URNO=%s AND %s %s"
					,pcgValid_SACI_Fields,pcgTabEnd,pcgTabEnd,pcgTabEnd,pcpEventData,pcgRurnClause,prgCfg.db_dep_filter);
				slFkt = START; 
				slCursor = 0;
				memset(pclTmpSqlAnswer,0x00,XL_BUFF);
				memset(pclData,0x00,DATABLK_SIZE);
				dbg(DEBUG,"PrepareData: (SACI)-SQL<%s>",pclSqlBuf);
				if ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))==RC_SUCCESS)
				{
					ilCnt++;
					BuildItemBuffer(pclTmpSqlAnswer,pcgValid_SACI_Fields,0,",");
					strcat(pclData,pclTmpSqlAnswer);
				}
				close_my_cursor(&slCursor);
				if (ilCnt > 0 && (ilRc==NOTFOUND || ilRc==RC_SUCCESS))
				{
						memset(pclSendBuffer,0x00,XL_BUFF);
						if ((ilRc = ProcessIntData(pclData,SACI,pclSendBuffer,""))
																			!= RC_SUCCESS)
						{
							dbg(TRACE,"PrepareData: ProcessIntData(SACI) failed!");
						}
				}
				else
				{
					dbg(TRACE,"PrepareData: sql_if() failed with = <%d> ",ilRc);
					dbg(TRACE,"PrepareData: Nr. of flights found = <%d> ",ilCnt);
				}
				break;
			case CXX:
				sprintf(pclSqlBuf,"SELECT %s FROM AFT%s %s"
					,pcgValid_CXX_Fields,pcgTabEnd,pcpEventData);
				slFkt = START; 
				slCursor = 0;
				memset(pclTmpSqlAnswer,0x00,XL_BUFF);
				memset(pclData,0x00,DATABLK_SIZE);
				dbg(DEBUG,"PrepareData: (CXX)-SQL <%s>",pclSqlBuf);
				if ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))==RC_SUCCESS)
				{
					ilCnt++;
					BuildItemBuffer(pclTmpSqlAnswer,pcgValid_CXX_Fields,0,",");
					strcat(pclData,pclTmpSqlAnswer);
				}
				close_my_cursor(&slCursor);
				if (ilCnt > 0 && (ilRc==NOTFOUND || ilRc==RC_SUCCESS))
				{
					memset(pclSendBuffer,0x00,XL_BUFF);
					if ((ilRc = ProcessIntData(pclData,CXX,pclSendBuffer,""))
																		!= RC_SUCCESS)
					{
						dbg(TRACE,"PrepareData: ProcessIntData(CXX) failed!");
					}
				}
				else
				{
					dbg(TRACE,"PrepareData: sql_if() failed with = <%d> ",ilRc);
					dbg(TRACE,"PrepareData: Nr. of flights found = <%d> ",ilCnt);
				}
				break;
		default:
				dbg(TRACE,"PrepareData: unknown command <%d> received!",ipCmd);
			break;
	}
	return ilRc;
}
/* **************************************************************** */
/* The SendEvent routine                                         */
/* prepares an internal CEDA-event                                  */
/* **************************************************************** */
static int SendEvent(char *pcpCmd,char *pcpSelection,char *pcpFields, char *pcpData,
											char *pcpAddStruct, int ipAddLen,int ipModIdSend,int ipModIdRecv,
											int ipPriority,char *pcpTable,char *pcpType,char *pcpFile)
{
	int     ilRc        		= RC_FAIL;
	int     ilLen        		= 0;
  EVENT   *prlOutEvent    = NULL;
  BC_HEAD *prlOutBCHead   = NULL;
  CMDBLK  *prlOutCmdblk   = NULL;

	if (ipModIdSend == 0)
	{
		dbg(TRACE,"SendEvent: Can't send event to ModID=<0> !");
	}
	else
	{
		/* size-calculation for prlOutEvent */
		ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) +
						strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + ipAddLen + 10;
		
		/*dbg(DEBUG,"Total-Len = %d+%d+%d+%d+%d+%d+%d+%d = %d",sizeof(EVENT),sizeof(BC_HEAD),sizeof(CMDBLK),strlen(pcpSelection),strlen(pcpFields),strlen(pcpData),ipAddLen,10,ilLen);*/

		/* memory for prlOutEvent */
		if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
		{
			dbg(TRACE,"SendEvent: cannot malloc <%d>-bytes for outgoing event!",ilLen);
			prlOutEvent = NULL;
		}
		else
		{
			/* clear whole outgoing event */
			memset((void*)prlOutEvent, 0x00, ilLen);

			/* set event structure... */
			prlOutEvent->type	     		= SYS_EVENT;
			prlOutEvent->command   		= EVENT_DATA;

			prlOutEvent->originator  = (short)mod_id;
			prlOutEvent->retry_count  = 0;
			prlOutEvent->data_offset  = sizeof(EVENT);
			prlOutEvent->data_length  = ilLen - sizeof(EVENT); 

			/* BC_HEAD-Structure... */
			prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
			prlOutBCHead->rc = (short)RC_SUCCESS;
			strncpy(prlOutBCHead->dest_name,pcgProcessName,10);
			strncpy(prlOutBCHead->recv_name, "EXCO",10);

			/* Cmdblk-Structure... */
			prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
			strcpy(prlOutCmdblk->command,pcpCmd);
			strcpy(prlOutCmdblk->obj_name,pcpTable);
			strcat(prlOutCmdblk->obj_name,pcgTabEnd);
			
			/* setting tw_start entries */
			sprintf(prlOutCmdblk->tw_start,"%s,%s",pcpType,pcpFile);
			
			/* setting tw_end entries */
			sprintf(prlOutCmdblk->tw_end,"%s,%s,%s",pcgHomeAp,pcgTabEnd,pcgProcessName);
			
			/* setting selection inside event */
			strcpy(prlOutCmdblk->data,pcpSelection);

			/* setting field-list inside event */
			strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1,pcpFields);

			/* setting data-list inside event */
			strcpy((prlOutCmdblk->data +
						(strlen(pcpSelection)+1) +
						(strlen(pcpFields)+1)),pcpData);

			if (pcpAddStruct != NULL)
			{
				memcpy((prlOutCmdblk->data +
							(strlen(pcpSelection)+1) +
							(strlen(pcpFields)+1)) + 
							(strlen(pcpData)+1),pcpAddStruct,ipAddLen);
			}

			/*DebugPrintEvent(DEBUG,prlOutEvent);*/ 
			/*snapit((char*)prlOutEvent,ilLen,outp);*/

			dbg(DEBUG,"SendEvent: <%d> --<%s>--> <%d>",prlOutEvent->originator,pcpCmd,ipModIdSend);

			if ((ilRc = que(QUE_PUT,ipModIdSend,ipModIdRecv,ipPriority,ilLen,(char*)prlOutEvent))
				!= RC_SUCCESS)
			{
				dbg(TRACE,"SendEvent: QUE_PUT to <%d> returns: <%d>",ipModIdSend,ilRc);
				HandleQueErr(ilRc);
			}
			/* free memory */
			free((void*)prlOutEvent); 
		}
	}
	return ilRc;
}
/* ******************************************************************** */
/* The TimeToStr() routine																	*/
/* ******************************************************************** */
static void TimeToStr(int ipUtcLocal,char *pcpTime,time_t lpTime,int ipType)
{
	int ilRc = 0;
	char pclTime[20];
	struct tm *_tm;
	struct tm rlTm;

	*pclTime = 0x00;
	*pcpTime = 0x00;
	if (lpTime == 0)
	{
		lpTime = time(NULL);
	}
	switch(ipUtcLocal)
	{
		case 0:
			dbg(DEBUG,"TimeToStr: LOC");
			_tm = (struct tm *) localtime(&lpTime);
			break;
		case 1:
			dbg(DEBUG,"TimeToStr: UTC");
			_tm = (struct tm *) gmtime(&lpTime);
			break;
	}
	rlTm = *_tm;
	switch(ipType)
	{
		case 0: /* Returns "now" in CEDA-format YYYYMMDDHHMMSS */
			/* Unusal format because of sccs !! */
			strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTm);
			dbg (DEBUG,"NOW-TimeToStr GFO : return <%s>",pcpTime);
			break;
		case 1:/* Returns lpTime in CEDA-format YYYYMMDDHHMMSS */
			/* Unusal format because of sccs !! */
			strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTm);
			/*dbg(DEBUG,"CEDA-TimeToStr: return <%s>",pcpTime);*/
			break;
		case 2:
			/* Unusal format because of sccs !! */
			strftime(pcpTime,15,"%" "a%" "d%" "m%" "y",&rlTm);
			strftime(pclTime,15,"%" "d%" "m%" "y",&rlTm);
			strcpy((char*)&pcpTime[2],pclTime);
			/* according to spec. the day must be all in uppercase letters */
			/* converting second byte from strftime() to uppercase */
			pcpTime[1] = toupper(pcpTime[1]);
			/*dbg(DEBUG,"DATESTAMP-TimeToStr: return <%s>",pcpTime);*/
			break;
		case 3:
			strftime(pcpTime,15,"%" "u",&rlTm);
			/*dbg(DEBUG,"DAY-TimeToStr : return <%s>",pcpTime);*/
			break;
		case 4:
			strftime(pcpTime,15,"%" "d%" "H%" "M%" "S",&rlTm);
			memset(pclTime,0x00,20);	
			strncpy(pclTime,pcpTime,2);
			strncat(pclTime,".",1);
			strncat(pclTime,(char*)&pcpTime[2],6);
			strncpy(pcpTime,pclTime,10);
			/*dbg(DEBUG,"TAIL-TimeToStr : return <%s>",pcpTime);*/
			break;
		default:
			dbg(TRACE,"TimeToStr : CEDA-ERROR: unknown time-type received!");
			break;
	}
}
/* ***************************************************************** */
/* The ProcessIntData routine                               */
/* ***************************************************************** */
static int ProcessIntData(char *pcpFlights,int ipType,char *pcpSendBuffer,char *pcpFile)
{
	int ilRc = RC_FAIL;
	int ilCnt = 0;
	int ilLines = 0;
	int ilCurLine = 0;
	int ilDataLen = 0;
	int ilType = 0;
	int ilBelt = 0;
	FILE *fp = NULL;
	char pclTmpBuf[XL_BUFF];
	char pclResultBuf[XL_BUFF];
	char *pclTmpPtr = NULL;
	BOOL blConvert = TRUE;
	char pclFileName[S_BUFF];

	/* count number of datalines */
	if ((ilLines = (int)GetNoOfElements(pcpFlights, '\n')) <= 0)
	{
		dbg(TRACE,"PID: GetNoOfElements() returns <%d>",ilLines);
	}
	else
	{
		dbg(DEBUG,"PID: <%d> record(s) found!",ilLines);
		switch(ipType)
		{
			case SSCI:
				dbg(DEBUG,"PID: record type <SSCI>!");
				memset(pclFileName,0x00,S_BUFF);
				dbg(DEBUG,"PID: create file <%s/%s>",prgCfg.fs_local_path,pcpFile);
				sprintf(pclFileName,"%s/%s",prgCfg.fs_local_path,pcpFile);
				fp = fopen (pclFileName,"w");
				if (fp == NULL)
				{
					dbg(TRACE,"PID: open file <%s> failed! Error: <%s>"
						,pclFileName,strerror(errno));
					blConvert = FALSE;
				}
				break;
			case SAI:
				dbg(DEBUG,"PID: record type <SAI>!");
				break;
			case FTU:
				dbg(DEBUG,"PID: record type <FTU>!");
				break;
			case SACI:
				dbg(DEBUG,"PID: record type <SACI>!");
				break;
			case CXX:
				dbg(DEBUG,"PID: record type <CXX>!");
				break;
			default: 
				dbg(TRACE,"PID: unknown record type received !");
				blConvert = FALSE;
				break;
		}

		if (blConvert==TRUE)
		{
			/* set temporary pointer to data area */
			pclTmpPtr = pcpFlights; 
			/* over all lines */
		  for (ilCurLine=0; ilCurLine<ilLines; ilCurLine++)
		  {
				/* get next data for writing... */
			  memset((void*)pclTmpBuf, 0x00, XL_BUFF);
			  if ((pclTmpPtr = CopyNextField(pclTmpPtr,'\n', pclTmpBuf)) == NULL)
			  {
			    dbg(TRACE,"PID: CopyNextField returns NULL!");
			  }
				else
				{
					switch(ipType)
					{
						case SAI: /* SendArrivalInformation */
							/* doing this two times because of the two baggage belts and possible old data */
							for (ilCnt=0;ilCnt<=1;ilCnt++)
							{
			  				memset((void*)pclResultBuf, 0x00, XL_BUFF);
								if ((ilRc = Convert_SAI(0,pclTmpBuf,ilCnt,pclResultBuf)) == RC_SUCCESS)
								{
									*pcpSendBuffer = 0x00;
									if ((ilRc = MakeSendBuffer(TRUE,MSG_SAI,pclResultBuf,
																pcpSendBuffer)) == RC_SUCCESS)
									{
										ilDataLen = strlen(pcpSendBuffer);
										if ((ilRc = Send_data(igSock,pcpSendBuffer,ilDataLen))==RC_FAIL)
										{
											CloseTCP();
										}
										else
										{
											if ((ilRc = WaitForAck(igSock,pcpSendBuffer,ilDataLen))
												== RC_FAIL)
											{
												dbg(TRACE,"PID: WaitForAck() including resend failed!");
												igSendArrivalCount++;
												if (igSendArrivalCount < 3)
												{
													dbg(TRACE,"PID: Wait <%ld> sec. [CHECK_INTERVAL] to repeat operation!",tgInterval);
												}
												else
												{
													dbg(TRACE,"PID: SERIOUS DATAFLOW PROBLEMS!");	
													dbg(TRACE,"PID: 3 following failures during SAI-operation!"); 
													dbg(TRACE,"PID: check network/network-performance!"); 
													CloseTCP();
													ilCurLine = ilLines; 
													ilRc = RC_FAIL;
												}
											}
											else
											{
												igSendArrivalCount = 0;	
											}
										}
									}
									else
									{
										dbg(TRACE,"PID: MakeSendBuffer() <SAI> failed!");
									}
								}
								else
								{
									dbg(TRACE,"PID: Convert_SAI() failed!");
								}
								ilRc = RC_SUCCESS;
							}
							break;
						case SSCI: /* SendScheduledChuteInformation */
							if ((ilRc = Convert_SSCI(pclTmpBuf)) == RC_SUCCESS)
							{
								*pcpSendBuffer = 0x00;
								if ((ilRc = MakeSendBuffer(TRUE,MSG_SSCI,pclTmpBuf,
															pcpSendBuffer)) == RC_SUCCESS)
								{
									if (fprintf(fp,"%s\n",pcpSendBuffer) > 0)
										{ilRc = RC_SUCCESS;}
								}
								else
								{
									dbg(TRACE,"PID: MakeSendBuffer() <SSCI> failed!");
								}
							}
							else
							{
								dbg(TRACE,"PID: Convert_SSCI() failed!");
							}
							break;

						/* FTU or SACI messages contain the same data => same handling of data */
						case FTU: /* Resend the chute information because of flight information changes */
						case SACI: /* SendActualChuteInformation */
							if ((ilRc = Convert_SACI(pclTmpBuf,&ilType,ipType)) == RC_SUCCESS)
							{
								*pcpSendBuffer = 0x00;
								if ((ilRc = MakeSendBuffer(TRUE,ilType,pclTmpBuf,
															pcpSendBuffer)) == RC_SUCCESS)
								{
									ilDataLen = strlen(pcpSendBuffer);
									if ((ilRc = Send_data(igSock,pcpSendBuffer,ilDataLen))==RC_FAIL)
									{
										CloseTCP();
									}
									else
									{
										ilRc = WaitForAck(igSock,pcpSendBuffer,ilDataLen);
									}
								}
								else
								{
									dbg(TRACE,"PID: MakeSendBuffer() <SACI> failed!");
								}
							}
							else
							{
								dbg(TRACE,"PID: Convert_SACI() failed!");
							}
							ilRc = RC_SUCCESS;
							break;
						case CXX: /* create a flight cancelled message */
							if ((ilRc = Convert_CXX(pclTmpBuf)) == RC_SUCCESS)
							{
								*pcpSendBuffer = 0x00;
								if ((ilRc = MakeSendBuffer(TRUE,MSG_CXX,pclTmpBuf,
															pcpSendBuffer)) == RC_SUCCESS)
								{
									ilDataLen = strlen(pcpSendBuffer);
									if ((ilRc = Send_data(igSock,pcpSendBuffer,ilDataLen))==RC_FAIL)
									{
										CloseTCP();
									}
									else
									{
										ilRc = WaitForAck(igSock,pcpSendBuffer,ilDataLen);
									}
								}
								else
								{
									dbg(TRACE,"PID: MakeSendBuffer() <CXX> failed!");
								}
							}
							break;
						default:
							dbg(TRACE,"PID: unknown record type received! Can't convert to message!");
							break;

					}
				}
			}
		}

		switch(ipType)
		{
			case SSCI:
				dbg(DEBUG,"PID: close file <%s/%s>",prgCfg.fs_local_path,pcpFile);
				if (fclose(fp)!=0)
				{
					dbg(TRACE,"PID: couldn't close file <%s>",pclFileName);
					ilRc = RC_FAIL;
				}
				break;
			default:
				break;
		}
	} 
	return ilRc;
}
/* *****************************************************************/
/* The Convert_SAI() routine                                */
/* Converts CEDA-data to the DYS-data telegram (data-file) format  */
/* *****************************************************************/
static int Convert_SAI(int ipCalledBy, char *pcpData,int ipLoop,char *pcpResultBuf)
{
	int ilRc = RC_FAIL;
	int ilCnt = 0;
	int ilFields = 0;
	int ilBytes = 0;
	int ilValidFields = 0;
	int ilActField = 0;
	int ilNoSend = FALSE;
	AFI_OUT prlAfiExt;
	char *pclTmpPtr = NULL;
	char pclTmpBuf[L_BUFF];
	char pclB1ba[XS_BUFF];
	char pclB1ea[XS_BUFF];
	char pclB2ba[XS_BUFF];
	char pclB2ea[XS_BUFF];
	time_t tlB1ba = 0;
	time_t tlB1ea = 0;
	time_t tlB2ba = 0;
	time_t tlB2ea = 0;
	
	time_t tlStod = 0;
	time_t tlEtod = 0;
	
	ilFields = (int)GetNoOfElements(pcpData, ',');
	ilValidFields = (int)GetNoOfElements(pcgValid_SAI_Fields, ',');

	if ((ilFields < 0) || (ilFields != ilValidFields))
	{
		dbg(TRACE,"SAI: invalid Nr. of data-fields <%d>",ilFields);
	}
	else
	{
		pclTmpPtr = pcpData;
		memset((char*)&prlAfiExt,0x00,sizeof(AFI_OUT));

		memset(pclB1ba,0x00,XS_BUFF);
		memset(pclB1ea,0x00,XS_BUFF);
		memset(pclB2ba,0x00,XS_BUFF);
		memset(pclB2ea,0x00,XS_BUFF);

		/* over all elements */
		for (ilActField=0; ilActField<ilFields; ilActField++)
		{
			/* get next data */
			memset((void*)pclTmpBuf, 0x00, L_BUFF);
			if ((pclTmpPtr = CopyNextField(pclTmpPtr,',',pclTmpBuf)) == NULL)
			{
				dbg(TRACE,"SAI: CopyNextField returns NULL!");
			}
			else
			{
				/* handle/convert field data */
				/* b1ba,b1ea,b2ba,b2ea,flno,blt1,org3,psta */
				switch(ilActField)
				{
					case 0: /* b1ba */
						/*dbg(DEBUG,"SAI: B1BA=<%s>",pclTmpBuf);*/
						if (strlen(pclTmpBuf)>0)
						{
							strncpy(pclB1ba,pclTmpBuf,14);
							TrimRight(pclB1ba);
							//GFO Changed 0 -> 1
                                                        StrToTime(1,pclB1ba,&tlB1ba);
						}
						break;
					case 1: /* b1ea */
						/*dbg(DEBUG,"SAI: B1EA=<%s>",pclTmpBuf);*/
						if (strlen(pclTmpBuf)>0)
						{
							strncpy(pclB1ea,pclTmpBuf,14);
							TrimRight(pclB1ea);
                                                        //GFO Changed 0 -> 1
							StrToTime(1,pclB1ea,&tlB1ea);
						}
						break;
					case 2: /* b2ba */
						/*dbg(DEBUG,"SAI: B2BA=<%s>",pclTmpBuf);*/
						if (strlen(pclTmpBuf)>0)
						{
							strncpy(pclB2ba,pclTmpBuf,14);
							TrimRight(pclB2ba);
                                                        //GFO Changed 0 -> 1
							StrToTime(1,pclB2ba,&tlB2ba);
						}
						break;
					case 3: /* b2ea */
						/*dbg(DEBUG,"SAI: B2EA=<%s>",pclTmpBuf);*/
						if (strlen(pclTmpBuf)>0)
						{
							strncpy(pclB2ea,pclTmpBuf,14);
							TrimRight(pclB2ea);
                                                        //GFO Changed 0 -> 1
							StrToTime(1,pclB2ea,&tlB2ea);
						}
						break;
					case 4: /* flno */
						dbg(DEBUG,"SAI: FLNO=<%s>",pclTmpBuf);
						/*dbg(DEBUG,"SAI: Flight-Nr.=<%s>",pclTmpBuf);*/
						if (strlen(pclTmpBuf) > 0)
						{
							MakeExtFlno(pclTmpBuf);
				 			MakeSingleEntry(pclTmpBuf,8,LEFT,' ',prlAfiExt.flno);
						}
						else
						{
							ilNoSend = TRUE;	
						}
						break;
					case 5: /* blt1 */
						/* first loop for first belt */
						if (ipLoop == 0)
						{
							dbg(DEBUG,"SAI: BLT1=<%s>",pclTmpBuf);
							if (strlen(pclTmpBuf)>0)
							{
								MakeSingleEntry(pclTmpBuf,2,LEFT,' ',prlAfiExt.belt);
							}
							else
							{
								ilNoSend = TRUE;	
							}
						}
						break;
					case 6: /* blt2 */
						/* second loop for second belt */
						if (ipLoop == 1)
						{
							dbg(DEBUG,"SAI: BLT2=<%s>",pclTmpBuf);
							if (strlen(pclTmpBuf)>0)
							{
								MakeSingleEntry(pclTmpBuf,2,LEFT,' ',prlAfiExt.belt);
							}
							else
							{
								ilNoSend = TRUE;	
							}
						}
						break;
					case 7: /* org3 */
						dbg(DEBUG,"SAI: ORG3=<%s>",pclTmpBuf);
				 		MakeSingleEntry(pclTmpBuf,3,LEFT,' ',prlAfiExt.org3);
						break;
					case 8: /* psta */
						dbg(DEBUG,"SAI: PSTA=<%s>",pclTmpBuf);
				 		MakeSingleEntry(pclTmpBuf,3,LEFT,' ',prlAfiExt.psta);
						ilRc = RC_SUCCESS;
						break;
					default: /* unknown */
						dbg(TRACE,"SAI: don't know how to handle field Nr. <%d>",ilActField);
						ilRc = RC_FAIL;
						break;
				}/* end switch */
			}
		}/* for end */

		/* if we don't have a belt we don't need to send */
		if (ilNoSend == TRUE)
		{
			dbg(DEBUG,"SAI: No belt or flight-nr. assigned. Rejecting!");
			ilRc = RC_FAIL;
		}
		else
		{
			dbg(DEBUG,"SAI: Flightdata format OK. Proceeding!");
		}

		if (ilRc == RC_SUCCESS)
		{
			switch(ipLoop)
			{
				case 0:
					ilRc = SetDisplayValue(tlB1ba,tlB1ea,(char*)&prlAfiExt.show);
					break;
				case 1:
					ilRc = SetDisplayValue(tlB2ba,tlB2ea,(char*)&prlAfiExt.show);
					break;
			}
		}

		if (ipCalledBy == 1)
		{
			dbg(DEBUG,"SAI: Belt-reallocation! Flight will be deleted from display!");
			*prlAfiExt.show = '0';
			ilRc = RC_SUCCESS;
		}

		if (ilRc == RC_SUCCESS)
		{
			/* Now cleaning structure from unallowed bytes (0x00 bytes) */
			/* until one byte before the terminating NULL(0x00)-byte */
			CleanMessage((char*)&prlAfiExt,(sizeof(AFI_OUT)-1));
			/*snapit((char*)&prlAfiExt,sizeof(AFI_OUT),outp);*/
			*pcpResultBuf = 0x00;
			strncpy(pcpResultBuf,(char*)&prlAfiExt,sizeof(AFI_OUT));
		}
	}
	return ilRc;
}
/* ***********************************************************************************/
/* The SetDisplayValue() routine                                	                   */
/* Sets the display value 1 or 0 according to the first/last bag times of the flight */
/* ***********************************************************************************/
static int SetDisplayValue(time_t tpBeltOpen,time_t tpBeltClose,char *cpShow)
{
	int ilRc = RC_FAIL;
	time_t tlNow = time(NULL);
	time_t tlCompare = 0;
	char pclNow[XS_BUFF];

	*cpShow = ' ';
        
	TimeToStr(1,pclNow,tlNow,0);	
        dbg(DEBUG,"SDV: Now<%s,%ld> Belt open<%ld> Belt close<%ld>",pclNow,tlNow,tpBeltOpen,tpBeltClose);
	StrToTime(1,pclNow,&tlNow);

	dbg(DEBUG,"SDV: Now<%s,%ld> Belt open<%ld> Belt close<%ld>",pclNow,tlNow,tpBeltOpen,tpBeltClose);
	/* flight will be shown "time_beltopen" minutes after b1ba */
	tlCompare = (tpBeltOpen + (time_t)(atoi(prgCfg.time_beltopen)*SECONDS_PER_MINUTE));
	if (tpBeltOpen!=0 && tlNow>tlCompare)
	{
		*cpShow = '1';
		ilRc = RC_SUCCESS;
	}
	/* flight will be deleted "time_beltclose" minutes after b2ea */
	tlCompare = (tpBeltClose + (time_t)(atoi(prgCfg.time_beltclose)*SECONDS_PER_MINUTE));
	if (tpBeltClose!=0 && tlNow>tlCompare) 
	{
		*cpShow = '0';
		ilRc = RC_SUCCESS;
	}
	
	if (tgAutoDelete > 0)
	{
		/* flight will be deleted automatically from display */
		/* only if no close time is set */
		if ((tlNow > (tpBeltOpen+tgAutoDelete)) && tpBeltOpen!=0 && tpBeltClose==0)
		{
			dbg(DEBUG,"SDV: now using AUTO_DELETE!");
			*cpShow = '0';
			ilRc = RC_SUCCESS;
		}
	}
	switch(*cpShow)
	{
		case ' ':
			dbg(DEBUG,"SDV: First/Last-bag times out of bounds!");
			ilRc = RC_FAIL;
			break;
		case '1':
			dbg(DEBUG,"SDV: Flight will be shown on display!");
			ilRc = RC_SUCCESS;
			break;
		case '0':
			dbg(DEBUG,"SDV: Flight will be deleted from display!");
			ilRc = RC_SUCCESS;
			break;
	}
	return ilRc;
}
/* *****************************************************************/
/* The Convert_SSCI() routine                                */
/* Converts CEDA-data to the DYS-data telegram (data-file) format  */
/* *****************************************************************/
static int Convert_SSCI(char *pcpData)
{
	int ilRc = RC_FAIL;
	int ilCnt = 0;
	int ilFields = 0;
	int ilBytes = 0;
	int ilValidFields = 0;
	int ilActField = 0;
	CFI_OUT prlCfiExt;
	char *pclTmpPtr = NULL;
	char pclTmpBuf[L_BUFF];
	
	time_t tlStod = 0;
	time_t tlEtod = 0;
	
	ilFields = (int)GetNoOfElements(pcpData, ',');
	ilValidFields = (int)GetNoOfElements(pcgValid_SSCI_Fields, ',');

	if ((ilFields < 0) || (ilFields != ilValidFields))
	{
		dbg(TRACE,"SSCI: invalid Nr. of data-fields <%d>",ilFields);
	}
	else
	{
		pclTmpPtr = pcpData;
		memset((char*)&prlCfiExt,0x00,sizeof(CFI_OUT));

		/* over all elements */
		for (ilActField=0; ilActField<ilFields; ilActField++)
		{
			/* get next data */
			memset((void*)pclTmpBuf, 0x00, L_BUFF);
			if ((pclTmpPtr = CopyNextField(pclTmpPtr,',',pclTmpBuf)) == NULL)
			{
				dbg(TRACE,"SSCI: CopyNextField returns NULL!");
			}
			else
			{
				/* handle/convert field data */
				/* AFTTAB.FLNO,AFTTAB.STOD,AFTTAB.ETOD,AFTTAB.DES3,AFTTAB.PSTD */
				/* CHATAB.INDI,CHATAB.LNAM,CHATAB.FCLA,CHATAB.STOB,CHATAB.STOE */
				switch(ilActField)
				{
					case 0: /* flno */ 
						/*dbg(DEBUG,"SSCI: FLNO=<%s>",pclTmpBuf);*/
						MakeExtFlno(pclTmpBuf);
				 		MakeSingleEntry(pclTmpBuf,8,LEFT,' ',prlCfiExt.flno);
						break;
					case 1:  /* stod */
						/*dbg(DEBUG,"SSCI: STOD=<%s>",pclTmpBuf);*/
				 		MakeSingleEntry(pclTmpBuf,12,LEFT,' ',prlCfiExt.stod);
						break;
					case 2:  /* etod */
						/*dbg(DEBUG,"SSCI: ETOD=<%s>",pclTmpBuf);*/
				 		MakeSingleEntry(pclTmpBuf,12,LEFT,' ',prlCfiExt.etod);
						break;
					case 3:  /* des3 */
						/*dbg(DEBUG,"SSCI: DES3=<%s>",pclTmpBuf);*/
				 		MakeSingleEntry(pclTmpBuf,3,LEFT,' ',prlCfiExt.des3);
						break;
					case 4:  /* pstd */
						/*dbg(DEBUG,"SSCI: PSTD=<%s>",pclTmpBuf);*/
				 		MakeSingleEntry(pclTmpBuf,4,LEFT,' ',prlCfiExt.pstd);
						break;
					case 5:  /* indi */
						/*dbg(DEBUG,"SSCI: INDI=<%s>",pclTmpBuf);*/
				 		MakeSingleEntry(pclTmpBuf,1,LEFT,' ',prlCfiExt.indi);
						break;
					case 6:  /* lnam */
						/*dbg(DEBUG,"SSCI: LNAM=<%s>",pclTmpBuf);*/
				 		MakeSingleEntry(pclTmpBuf,3,LEFT,' ',prlCfiExt.lnam);
						break;
					case 7:  /* fcla */
						/*dbg(DEBUG,"SSCI: FCLA=<%s>",pclTmpBuf);*/
				 		MakeSingleEntry(pclTmpBuf,1,LEFT,' ',prlCfiExt.fcla);
						break;
					case 8:  /* stob */
						/*dbg(DEBUG,"SSCI: STOB=<%s>",pclTmpBuf);*/
				 		MakeSingleEntry(pclTmpBuf,12,LEFT,' ',prlCfiExt.o_tim);
						break;
					case 9:  /* stoe */
						/*dbg(DEBUG,"SSCI: STOE=<%s>",pclTmpBuf);*/
				 		MakeSingleEntry(pclTmpBuf,12,LEFT,' ',prlCfiExt.c_tim);
						ilRc = RC_SUCCESS;
						break;
					case 10:  /* tifb */
						/* dbg(DEBUG,"SSCI: TFIB=<%s>",pclTmpBuf);*/
						/* taking the best quality time for the telegram */
						TrimRight(pclTmpBuf);
						if (strlen(pclTmpBuf) > 0)
						{
				 			MakeSingleEntry(pclTmpBuf,12,LEFT,' ',prlCfiExt.o_tim);
						}
						break;
					case 11:  /* tife */
						/*dbg(DEBUG,"SSCI: TIFE=<%s>",pclTmpBuf);*/
						/* taking the best quality time for the telegram */
						TrimRight(pclTmpBuf);
						if (strlen(pclTmpBuf) > 0)
						{
				 			MakeSingleEntry(pclTmpBuf,12,LEFT,' ',prlCfiExt.c_tim);
						}
						ilRc = RC_SUCCESS;
						break;
					default: /* unknown */
						dbg(TRACE,"SSCI: don't know how to handle field Nr. <%d>",ilActField);
						ilRc = RC_FAIL;
						break;
				}/* end switch */
			}
		}/* for end */

		/* Now cleaning structure from unallowed bytes (0x00 bytes) */
		/* until one byte before the terminating NULL(0x00)-byte */
		CleanMessage((char*)&prlCfiExt,(sizeof(CFI_OUT)-1));
		
		if (ilRc == RC_SUCCESS)
		{
			snapit((char*)&prlCfiExt,sizeof(CFI_OUT),outp);
			*pcpData = 0x00;
			strncpy(pcpData,(char*)&prlCfiExt,sizeof(CFI_OUT));
		}
	}
	return ilRc;
}
/* *****************************************************************/
/* The Convert_SACI() routine                                */
/* Converts CEDA-data to the DYS-data telegram (data-file) format  */
/* *****************************************************************/
static int Convert_SACI(char *pcpData,int *ipType,int ipTelegramType)
{
	int ilRc = RC_FAIL;
	int ilOutSize = 0;
	int ilFields = 0;
	int ilBytes = 0;
	int ilValidFields = 0;
	int ilActField = 0;
	CFI_OUT prlCfiExt;
	CSI_OUT prlCsiExt;
	char pclOut[XS_BUFF];
	char *pclTmpPtr = NULL;
	char pclTmpBuf[L_BUFF];

	char pclIndi[XS_BUFF];
	char pclLnam[XS_BUFF];
	char pclFcla[XS_BUFF];
	char pclFlno[XS_BUFF];
	char pclPstd[XS_BUFF];
	char pclDes3[XS_BUFF];
	char pclStod[XS_BUFF];
	char pclEtod[XS_BUFF];
	char pclO_tim[XS_BUFF];
	char pclC_tim[XS_BUFF];
	char pclTisb[XS_BUFF];
	char pclTise[XS_BUFF];
	
	time_t tlStod = 0;
	time_t tlEtod = 0;

	memset((char*)&prlCfiExt,0x00,sizeof(CFI_OUT));
	memset((char*)&prlCsiExt,0x00,sizeof(CSI_OUT));
	memset((char*)&pclOut,0x00,XS_BUFF);
	memset(pclIndi,0x00,XS_BUFF);
	memset(pclLnam,0x00,XS_BUFF);
	memset(pclFcla,0x00,XS_BUFF);
	memset(pclFlno,0x00,XS_BUFF);
	memset(pclPstd,0x00,XS_BUFF);
	memset(pclDes3,0x00,XS_BUFF);
	memset(pclStod,0x00,XS_BUFF);
	memset(pclEtod,0x00,XS_BUFF);
	memset(pclO_tim,0x00,XS_BUFF);
	memset(pclC_tim,0x00,XS_BUFF);
	memset(pclTisb,0x00,XS_BUFF);
	memset(pclTise,0x00,XS_BUFF);
	
	ilFields = (int)GetNoOfElements(pcpData, ',');
	ilValidFields = (int)GetNoOfElements(pcgValid_SACI_Fields, ',');

	if ((ilFields < 0) || (ilFields != ilValidFields))
	{
		dbg(TRACE,"SACI: invalid Nr. of data-fields <%d>",ilFields);
	}
	else
	{
		dbg(DEBUG,"SACI: DB-DATA=<%s>",pcpData);
		GetDataItem(pclTisb,pcpData,13,',',"","");
		GetDataItem(pclTise,pcpData,14,',',"","");

		/*** converting following fields *******************************/
		/* AFTTAB.FLNO,AFTTAB.STOD,AFTTAB.ETOD,CHATAB.DES3,AFTTAB.PSTD */
		/* CHATAB.INDI,CHATAB.LNAM,CHATAB.FCLA,CHATAB.STOB,CHATAB.STOE */
		/* CHATAB.TIFB,CHATAB.TIFE,CHATAB.TISB,CHATAB.TISE */
		if (ipTelegramType==FTU || (ipTelegramType==SACI && (*pclTisb == 'S' && *pclTise == 'S')))
		{
			/***************************************/
			/* now building buffer for telegram 60 */
			/***************************************/
			dbg(DEBUG,"SACI: Building buffer for telegram Nr.60 !");
			GetDataItem(pclFlno,pcpData,1,',',"","");
			MakeExtFlno(pclFlno);
			MakeSingleEntry(pclFlno,8,LEFT,' ',prlCfiExt.flno);

			GetDataItem(pclStod,pcpData,2,',',"","");
			MakeSingleEntry(pclStod,12,LEFT,' ',prlCfiExt.stod);

			GetDataItem(pclEtod,pcpData,3,',',"","");
			MakeSingleEntry(pclEtod,12,LEFT,' ',prlCfiExt.etod);

			GetDataItem(pclDes3,pcpData,4,',',"","");
			MakeSingleEntry(pclDes3,3,LEFT,' ',prlCfiExt.des3);

			GetDataItem(pclPstd,pcpData,5,',',"","");
			MakeSingleEntry(pclPstd,4,LEFT,' ',prlCfiExt.pstd);

			GetDataItem(pclIndi,pcpData,6,',',"","");
			MakeSingleEntry(pclIndi,1,LEFT,' ',prlCfiExt.indi);

			GetDataItem(pclLnam,pcpData,7,',',"","");
			MakeSingleEntry(pclLnam,3,LEFT,' ',prlCfiExt.lnam);

			GetDataItem(pclFcla,pcpData,8,',',"","");
			MakeSingleEntry(pclFcla,1,LEFT,' ',prlCfiExt.fcla);

			GetDataItem(pclO_tim,pcpData,9,',',"","");
			MakeSingleEntry(pclO_tim,12,LEFT,' ',prlCfiExt.o_tim);

			GetDataItem(pclC_tim,pcpData,10,',',"","");
			MakeSingleEntry(pclC_tim,12,LEFT,' ',prlCfiExt.c_tim);

			*ipType = MSG_SSCI;
			ilOutSize = sizeof(CFI_OUT);
			memcpy(pclOut,(char*)&prlCfiExt,ilOutSize);
			ilRc = RC_SUCCESS;
		}
		else
		{
			if (*pclTisb=='A' || *pclTise=='A')
			{
				/***************************************/
				/* now building buffer for telegram 55 */
				/***************************************/
				dbg(DEBUG,"SACI: Building buffer for telegram Nr.55 !");
				GetDataItem(pclFlno,pcpData,1,',',"","");
				MakeExtFlno(pclFlno);
				MakeSingleEntry(pclFlno,8,LEFT,' ',prlCsiExt.flno);

				GetDataItem(pclStod,pcpData,2,',',"","");
				MakeSingleEntry(pclStod,12,LEFT,' ',prlCsiExt.stod);

				GetDataItem(pclEtod,pcpData,3,',',"","");
				MakeSingleEntry(pclEtod,12,LEFT,' ',prlCsiExt.etod);

				GetDataItem(pclDes3,pcpData,4,',',"","");
				MakeSingleEntry(pclDes3,3,LEFT,' ',prlCsiExt.des3);

				/* GetDataItem(pclIndi,pcpData,6,',',"","");*/
				/* because Mannesmann sends us only INDI=S for chute status (open/close)*/
				/* we're setting it fixed to S for Unitechnik dynamic-signage */ 
				strncpy(pclIndi,"S",1);
				MakeSingleEntry(pclIndi,1,LEFT,' ',prlCsiExt.indi);

				GetDataItem(pclLnam,pcpData,7,',',"","");
				MakeSingleEntry(pclLnam,3,LEFT,' ',prlCsiExt.lnam);

				GetDataItem(pclFcla,pcpData,8,',',"","");
				MakeSingleEntry(pclFcla,1,LEFT,' ',prlCsiExt.fcla);

				GetDataItem(pclO_tim,pcpData,11,',',"","");
				MakeSingleEntry(pclO_tim,12,LEFT,' ',prlCsiExt.o_tim);

				GetDataItem(pclC_tim,pcpData,12,',',"","");
				MakeSingleEntry(pclC_tim,12,LEFT,' ',prlCsiExt.c_tim);

				*ipType = MSG_SACI;
				ilOutSize = sizeof(CSI_OUT);
				memcpy(pclOut,(char*)&prlCsiExt,ilOutSize);
				ilRc = RC_SUCCESS;
			}
			else
			{
				dbg(TRACE,"SACI: unknown states=<%s><%s> for chute received!",pclTisb,pclTise);
				dbg(TRACE,"SACI: cancel processing!");
			}
		}
		if (ilRc == RC_SUCCESS)
		{
			/* Now cleaning structure from unallowed bytes (0x00 bytes) */
			/* until one byte before the terminating NULL(0x00)-byte */
			CleanMessage((char*)&pclOut,ilOutSize-1);
			/*snapit((char*)&pclOut,ilOutSize,outp);*/
			*pcpData = 0x00;
			strncpy(pcpData,(char*)&pclOut,ilOutSize);
		}
	}
	return ilRc;
}
/* *****************************************************************/
/* The Convert_CXX() routine                                */
/* Converts CEDA-data to the DYS-data telegram (data-file) format  */
/* *****************************************************************/
static int Convert_CXX(char *pcpData)
{
	int ilRc = RC_FAIL;
	CXX_EXT prlCxxExt;
	char pclDay[20];
	char pclDate[20];
	char pclToday[20];
	char pclOrg3[20];
	char pclAdid[20];
	char pclStod[20];
	char pclTifd[20];
	char pclFtyp[20];
	char pclFlno[20];
	
	time_t tlTifd = 0;
	time_t tlOffsDay = 0;
	time_t tlToday = 0;
	time_t tlStod = 0;
	time_t tlEtod = 0;
	
	memset(pclOrg3,0x00,20);
	memset(pclAdid,0x00,20);
	memset(pclStod,0x00,20);
	memset(pclTifd,0x00,20);
	memset(pclFtyp,0x00,20);
	memset(pclFlno,0x00,20);

	/* org3,adid,stod,tifd,ftyp,flno */
	GetDataItem(pclOrg3,pcpData,1,',',"","");
	TrimRight(pclOrg3);
	/*dbg(DEBUG,"CXX: ORG3=<%s>",pclOrg3);*/
	GetDataItem(pclAdid,pcpData,2,',',"","");
	TrimRight(pclAdid);
	/*dbg(DEBUG,"CXX: ADID=<%s>",pclAdid);*/
	GetDataItem(pclStod,pcpData,3,',',"","");
	TrimRight(pclStod);
	/*dbg(DEBUG,"CXX: STOD=<%s>",pclStod);*/
	GetDataItem(pclTifd,pcpData,4,',',"","");
	TrimRight(pclTifd);
	/*dbg(DEBUG,"CXX: TIFD=<%s>",pclTifd);*/
	GetDataItem(pclFtyp,pcpData,5,',',"","");
	TrimRight(pclFtyp);
	/*dbg(DEBUG,"CXX: FTYP=<%s>",pclFtyp);*/
	GetDataItem(pclFlno,pcpData,6,',',"","");
	MakeExtFlno(pclFlno);
	TrimRight(pclFlno);
	/*dbg(DEBUG,"CXX: FLNO=<%s>",pclFlno);*/
	
	if (strcmp(pclOrg3,pcgHomeAp)==0 && (*pclAdid=='D' || *pclAdid=='B'))
	{
		if (*pclFtyp!='O' && *pclFtyp!='R')
		{
			memset(pclDate,0x00,10);
			strncpy(pclDate,pclTifd,8);
			dbg(DEBUG,"Convert_CXX: check flight-date <%s>.",pclDate); 	
			if ((ilRc = CheckValue("SFU-TIFD",pclDate,CFG_NUM)) == RC_SUCCESS)
			{
				strcat(pclDate,"000010");
				StrToTime(0,pclDate,&tlTifd);

				memset(pclToday,0x00,sizeof(pclToday));
				TimeToStr(0,pclToday,0,0);
				pclToday[8]=0x00;
				strcat(pclToday,"000010");
				StrToTime(0,pclToday,&tlToday);
				
				tlOffsDay = tlToday + (SECONDS_PER_DAY * atol(prgCfg.fs_offset));
				if ((tlTifd >= tlToday) && (tlTifd <= tlOffsDay))
				{
					ilRc = RC_SUCCESS;
				}
				else
				{
					dbg(DEBUG,"Convert_CXX: Date outside range! Not processing!"); 	
					ilRc = RC_FAIL;
				}
			}
			else
			{
				dbg(TRACE,"Convert_CXX: invalid TIFD! Can't process!"); 	
			}
		}
	}
	else
	{
		dbg(DEBUG,"Convert_CXX: Arrival flight! Ignoring flight!");
	}

	if (ilRc == RC_SUCCESS)
	{
		memset((char*)&prlCxxExt,0x00,sizeof(CXX_EXT));
		/* flno */
		MakeExtFlno(pclFlno);
		MakeSingleEntry(pclFlno,8,LEFT,' ',prlCxxExt.flno);
		/* STOD */
		StrToTime(0,pclStod,&tlStod);
		MakeSingleEntry(pclStod,12,LEFT,' ',prlCxxExt.stod);

		/* Now cleaning structure from unallowed bytes (0x00  bytes) */
		/* until one byte before the terminating NULL(0x00)-byte */
		CleanMessage((char*)&prlCxxExt,(sizeof(CXX_EXT)-1));
			
		TimeToStr(0,pclToday,0,0);	
		TimeToStr(0,pclDay,tlStod,1);	
		if (strncmp(pclToday,pclDay,8) == 0)
		{
			MakeSingleEntry("8",1,LEFT,' ',prlCxxExt.day);
		}
		else
		{
			TimeToStr(0,pclDay,tlStod,3);	
			MakeSingleEntry(pclDay,1,LEFT,' ',prlCxxExt.day);
		}
		snapit((char*)&prlCxxExt,sizeof(CXX_EXT),outp);
		*pcpData = 0x00;
		strncpy(pcpData,(char*)&prlCxxExt,sizeof(CXX_EXT));
	}
	return ilRc;
}
/* ******************************************************************** */
/*                             												*/
/*                                                												*/
/* ******************************************************************** */
static void MakeSingleEntry(char *pcpSrc,int ipLen,int ipAdjust,char cpFill,
															char *pcpDest)
{
	int ilSrcLen = 0;
	int ilCnt = 0;
	int ilNrOfFillChars = 0;

	ilSrcLen = (int)strlen(pcpSrc);
	if (ilSrcLen > 0)
	{
		if (ilSrcLen >= ipLen)
		{
			strncpy(pcpDest,pcpSrc,ipLen);
		}
		else
		{
			ilNrOfFillChars = ipLen - ilSrcLen;
			switch(ipAdjust)
			{
				case RIGHT:
					while (ilCnt < ilNrOfFillChars)
					{
						memcpy(&pcpDest[ilCnt],&cpFill,1);
						ilCnt++;
					}
					strncat(&pcpDest[ilCnt],pcpSrc,ilSrcLen);
					break;
				case LEFT:
					strncpy(&pcpDest[ilCnt],pcpSrc,ilSrcLen);
					while (ilCnt < ilNrOfFillChars)
					{
						memcpy(&pcpDest[ilSrcLen+ilCnt],&cpFill,1);
						ilCnt++;
					}
					break;
			}
		}
	}
}
/* ******************************************************************** */
/* The StrToTime() routine																	*/
/* Format of Date has to be CEDA-Format - 14 Byte													*/
/* ******************************************************************** */
static int StrToTime(int ipUtcLocal,char *pcpTime,time_t *plpTime)
{
	struct tm *_tm;
	time_t now;
	char  _tmpc[6];
        char *tz;

	/*dbg(DEBUG,"StrToTime, <%s>",pcpTime);*/
	if (strlen(pcpTime) < 12 )
	{
		*plpTime = time(0L);
		return RC_FAIL;
	} 

	now = time(0L);
	switch(ipUtcLocal)
	{
		case 0:
			dbg(DEBUG,"StrToTime: LOC");
			_tm = (struct tm *)localtime(&now);
			break;
		case 1:
			dbg(DEBUG,"StrToTime: UTC");
		_tm = (struct tm *)gmtime(&now);
			break;
	}
	_tmpc[2] = '\0';
	_tm -> tm_sec = 0;
	strncpy(_tmpc,pcpTime+10,2);
	_tm -> tm_min = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+8,2);
	_tm -> tm_hour = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+6,2);
	_tm -> tm_mday = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+4,2);
	_tm -> tm_mon = atoi(_tmpc)-1;
	strncpy(_tmpc,pcpTime,4);
	_tmpc[4] = '\0';
	_tm -> tm_year = atoi(_tmpc)-1900;
	_tm -> tm_wday = 0;
	_tm -> tm_yday = 0;
        
	/* now = mktime(_tm); */
        if (ipUtcLocal == 0) {
            now = mktime(_tm);
        } else {
            tz = getenv("TZ");
            setenv("TZ", "UTC", 1);
            tzset();
            now = mktime(_tm);
            if (tz)
                setenv("TZ", tz, 1);
            else
                unsetenv("TZ");
            tzset();
        }
        
	dbg(DEBUG,"StrToTime GFO: %02d.%02d.%04d %02d:%02d Now: %ld",
	_tm->tm_mday,_tm->tm_mon+1,_tm->tm_year+1900,_tm->tm_hour,_tm->tm_min,now);
        
	if (now != (time_t) -1)
	{
		*plpTime = now;
		return RC_SUCCESS;
	}
	*plpTime = time(NULL);
	return RC_FAIL;
}
/******************************************************************************/
/* The GetViaByNumber routine                                                 */
/******************************************************************************/
static char *GetViaByNumber(char *pcpVial,int ipViaNo,int ipVia34)
{
	static char	pclVia[iMIN];

	if (pcpVial == NULL || !strlen(pcpVial) || ipViaNo < 1)
		return NULL;

	memset((void*)pclVia, 0x00, iMIN);
	if (ipVia34 == 3)
		strncpy(pclVia, pcpVial+(1+((ipViaNo-1)*120)),3);
	else
		strncpy(pclVia, pcpVial+(4+((ipViaNo-1)*120)),4);
	return pclVia;
}
/* ******************************************************************** */
/*                             												*/
/*                                                												*/
/* ******************************************************************** */
static int MakeSendBuffer(int ipGetNr,int ipCmd,char *pcpDataPointer,char *pcpSendBuffer)
{
	int ilRc = RC_FAIL;
	int ilDataLen = 0;
	int ilSendBufferSize = 0;
	HEAD pclHead;
	TAIL pclTail;
	
	/* HEADER */
	memset((char*)&pclHead,0x00,sizeof(HEAD));
	*(pclHead.start) = SOM;
	*(pclHead.cycle_no) = '0';
	sprintf(pclHead.t_type,"%02d",ipCmd);
	strncpy(pclHead.sender,prgCfg.my_sys_id,2);
	strncpy(pclHead.receiver,prgCfg.remote_sys_id,2);
	if (ipGetNr == FALSE)
	{
		strncpy(pclHead.order_no,"01",2);
	}
	else
	{
		GetMsgNumber(pclHead.order_no);
	}

	/* TAIL */
	memset((char*)&pclTail,0x00,sizeof(TAIL));
	GetMsgTimeStamp(pclTail.time);
	strncpy(pclTail.tail,prgEnd.tail,4);

	ilDataLen = (int)strlen(pcpDataPointer);

	ilSendBufferSize = igHeadLen+ilDataLen+igTailLen;

	/*dbg(DEBUG,"MSB: HEAD<%d> + DATA<%d> + TAIL<%d> = <%d> bytes."
		,igHeadLen,ilDataLen,igTailLen,ilSendBufferSize);*/

	if (ilSendBufferSize < MAX_TELEGRAM_LEN)
	{
		memcpy((char*)pcpSendBuffer,(char*)&pclHead,igHeadLen);
		memcpy((char*)&pcpSendBuffer[igHeadLen],pcpDataPointer,ilDataLen);
		memcpy((char*)&pcpSendBuffer[igHeadLen+ilDataLen],(char*)&pclTail,igTailLen);
		pcpSendBuffer[ilSendBufferSize] = 0x00;
		dbg(DEBUG,"MSB: <%s>",pcpSendBuffer);
		ilRc = RC_SUCCESS;
	}
	else
	{
		dbg(TRACE,"MSB: Buffer to BIG! Can't send!");
	}
	return ilRc;
}
/* ******************************************************************** */
/*                             												*/
/*                                                												*/
/* ******************************************************************** */
static int SendArrivalFlightInfo()
{
	int ilRc = RC_SUCCESS;

	if (( ilRc = PrepareData(SAI,"",0)) != RC_SUCCESS)
	{
		dbg(TRACE,"SendArrivalFlightInfo: PrepareData(SAI) failed!"); 	
	}
	if (ilRc == RC_FAIL)
	{
		dbg(TRACE,"SendArrivalFlightInfo: Error during request creation!");
	}
	ilRc = RC_SUCCESS;
	return ilRc;
}
/* ******************************************************************** */
/*                             												*/
/*                                                												*/
/* ******************************************************************** */
static int CreateChuteInfoFiles()
{
	int ilRc = RC_SUCCESS;
	int ilCnt = 0;
	time_t tlToday;
	time_t tlDayToSend;
	char pclToday[20];
	char pclSendDate[20];

	memset(pclToday,0x00,sizeof(pclToday));
	TimeToStr(1,pclToday,0,0);
	pclToday[8]=0x00;
	strcat(pclToday,"000010");
	StrToTime(1,pclToday,&tlToday);

	for (ilCnt=0;ilCnt<atoi(prgCfg.fs_offset) && ilRc==RC_SUCCESS;ilCnt++)
	{
		tlDayToSend = tlToday + (SECONDS_PER_DAY * ilCnt);
		memset(pclSendDate,0x00,sizeof(pclSendDate));
		TimeToStr(1,pclSendDate,tlDayToSend,1);
		if (( ilRc = PrepareData(SSCI,"",tlDayToSend)) != RC_SUCCESS)
		{
			dbg(TRACE,"CreateChuteInfoFiles: PrepareData(SSCI) failed!"); 	
		}
		if (ilRc == RC_FAIL)
		{
			dbg(TRACE,"CreateChuteInfoFiles: Error during request creation!");
		}
		ilRc = RC_SUCCESS;
	}
	return ilRc;
}
/* ******************************************************************** */
/*                             												*/
/*                                                												*/
/* ******************************************************************** */
static int SendFileViaFtp(char *pcpHost,char *pcpFile)
{
	int ilRc = RC_SUCCESS;
	char pclRemoteFile[S_BUFF];
	char pclRemoteStamp[S_BUFF];
	char pclLocalStamp[S_BUFF];
	time_t tlDayToSend = 0;
	
	memset(pclLocalStamp,0x00,S_BUFF);

	strncpy(pclLocalStamp,pcpFile,8);
	dbg(DEBUG,"SFVF: call FTPHDL for <%s>.",pcpFile);
	if ((ilRc = CheckValue("SFVF:",pclLocalStamp,CFG_NUM))==RC_SUCCESS)
	{
		strcat(pclLocalStamp,"000010");
		StrToTime(0,pclLocalStamp,&tlDayToSend);
		TimeToStr(0,pclRemoteStamp,tlDayToSend,2);
		memset(pclRemoteFile,0x00,S_BUFF);
		sprintf(pclRemoteFile,"%s.DTM",pclRemoteStamp);

		/*****************************************************/
		/* now filling the hyper-dynamic FTPConfig-structure */
		/*****************************************************/
		memset(&prgFtp,0x00,sizeof(FTPConfig));

		prgFtp.iFtpRC = FTP_SUCCESS; /* Returncode of FTP-transmission */
		strcpy(prgFtp.pcCmd,"FTP");
		strcpy(prgFtp.pcHostName,pcpHost);
		strcpy(prgFtp.pcUser,prgCfg.ftp_user);
		strcpy(prgFtp.pcPasswd,prgCfg.ftp_pass);
		prgFtp.cTransferType = CEDA_BINARY;
		prgFtp.iTimeout = 0; /* for tcp_waittimeout */
		prgFtp.lGetFileTimer = 100000; /* timeout (microseconds) for receiving data */
		prgFtp.lReceiveTimer = 100000; /* timeout (microseconds) for receiving data */
		prgFtp.iDebugLevel = DEBUG; /* Debugging level of FTPHDL during operation */
		if (strcmp(prgCfg.ftp_client_os,"WIN") == 0)
		{
			prgFtp.iClientOS = OS_WIN; /* Operating system of client */
		}
		if (strcmp(prgCfg.ftp_client_os,"UNIX") == 0)
		{
			prgFtp.iClientOS = OS_UNIX; /* Operating system of client */
		}
		prgFtp.iServerOS = OS_UNIX; /* Operating system of server */
		prgFtp.iRetryCounter = 0; /* number of retries */
		prgFtp.iDeleteRemoteSourceFile = 0; /* yes=1 no=0 */
		prgFtp.iDeleteLocalSourceFile = 0; /* yes=1 no=0 */
		prgFtp.iSectionType = iSEND; /* iSEND or iRECEIVE */
		prgFtp.iInvalidOffset = 0; /* time (min.) the section will be set invalid, after unsuccessfull retry */
		prgFtp.iTryAgainOffset = 0; /* time between retries */
		strcpy(prgFtp.pcHomeAirport,pcgHomeAp); /* HomeAirport */
		strcpy(prgFtp.pcTableExtension,pcgTabEnd); /* Table extension */

		/* local path for FTP */
		strcpy(prgFtp.pcLocalFilePath,prgCfg.fs_local_path);
		/* local filename for FTP */
		strcpy(prgFtp.pcLocalFileName,pcpFile);
		/* local filename after successfull FTP-transmission */
		if (strcmp(prgCfg.after_ftp,"R") == 0)
		{
			sprintf(prgFtp.pcRenameLocalFile,"%s_send",pcpFile);
		}

		/* remote path for FTP */
		strcpy(prgFtp.pcRemoteFilePath,prgCfg.fs_remote_path);
		/* remote filename during FTP-transmission */
		strcpy(prgFtp.pcRemoteFileName,pclRemoteFile);
		/* remote filename after successfull FTP-transmission */
		sprintf(prgFtp.pcRenameRemoteFile,"%s.TBL",pclRemoteStamp);

		if (strcmp(prgCfg.after_ftp,"D") == 0)
		{
			prgFtp.iDeleteLocalSourceFile = 1; /* files will be deleted */
		}
		if (strcmp(prgCfg.after_ftp,"K") == 0)
		{
			prgFtp.iDeleteLocalSourceFile = 0; /* files will not be deleted */
		}
		prgFtp.iSendAnswer = 1; /* yes=1 no=0 */ 
		prgFtp.cStructureCode = CEDA_FILE;
		prgFtp.cTransferMode = CEDA_STREAM;
		prgFtp.iStoreType = CEDA_CREATE;
		prgFtp.data[0] = 0x00;

		if ((ilRc = SendEvent("FTP"," "," ","DYN",(char*)&prgFtp,
												sizeof(FTPConfig),igModID_Ftphdl,mod_id,PRIORITY_3,"","",""))
								!= RC_SUCCESS)
		{
			dbg(TRACE,"SFVF: SendEvent() to <%d> returns <%d>!",igModID_Ftphdl,ilRc);
		}
	}
	else
	{
		dbg(TRACE,"SFVF: invalid filename-format! Can't send!");
	}
	return ilRc;
}
/* ******************************************************************** */
/* The MakeExtFlno routine					*/
/* ******************************************************************** */
static int MakeExtFlno(char *pcpFlno)
{
  int ilCnt = 0;
  int ilAlcCnt = 0;
  int ilFnrCnt = 0;
  int ilBreak = FALSE;
  int ilSufCnt = 0;
  int ilRC = RC_FAIL;
  int ilLen = 0;
  int ilPos = 0;
  char pclTmpFlno[16];
  char pclAlc[10];
  char pclFnr[10];
  char pclTmpFnr[10];
  char pclTmpBuf[10];
  char pclSuffix[10];

  memset(pclAlc,0x00,10);
  memset(pclFnr,0x00,10);
  memset(pclTmpFnr,0x00,10);
  memset(pclTmpBuf,0x00,10);
  memset(pclSuffix,0x00,10);

	str_trm_all(pcpFlno," ",TRUE);
  ilLen = strlen(pcpFlno);
	if (ilLen > 3 && ilLen < 10)
	{
		do
		{
			/* getting the airline */
			if (ilCnt<3 && (isalpha(pcpFlno[ilCnt])!=0 || isdigit(pcpFlno[ilCnt])!=0))
			{
				pclAlc[ilAlcCnt] = pcpFlno[ilCnt];	
				ilAlcCnt++;
			}
			/* getting the flight-nr */
			if (ilCnt>1 && isdigit(pcpFlno[ilCnt])!=0)
			{
				pclFnr[ilFnrCnt] = pcpFlno[ilCnt];	
				ilFnrCnt++;
			}
			/* getting the suffix */
			if (ilCnt>3 && isdigit(pcpFlno[ilCnt])==0)
			{
				pclSuffix[ilSufCnt] = pcpFlno[ilCnt];	
				ilSufCnt++;
			}
			ilCnt++;
		}while(ilCnt < ilLen);
		/*dbg(DEBUG,"MakeExtFlno: AIRLINE   = <%s>",pclAlc);*/
		/* stripping leading zeros */
		ilCnt=0;
		do
		{
			if (pclFnr[ilCnt]!='0')
			{
				strcpy(pclTmpBuf,&pclFnr[ilCnt]);
				ilBreak = TRUE;	
			}
			ilCnt++;	
		}while(ilBreak==FALSE&&ilCnt<strlen(pclFnr));
    strcpy(pclTmpFnr,"00000");
    ilPos = 3 - strlen(pclTmpBuf);
    if (ilPos < 0)
    {
      ilPos = 0;
    } /* end if */
    strcpy(&pclTmpFnr[ilPos],pclTmpBuf);
		/*dbg(DEBUG,"MakeExtFlno: FLIGHT-NR = <%s>",pclTmpFnr);*/
		str_trm_all(pclSuffix," ",TRUE);
		/*dbg(DEBUG,"MakeExtFlno: SUFFIX    = <%s>",pclSuffix);*/
		if (strlen(pclAlc) > 3)
		{
			dbg(TRACE,"MakeExtFlno: Flight-Airline-code to big!");
		}
		else
		{
			if (strlen(pclAlc) <= 2)
			{
				strcat(pclAlc," ");
			}
			memset(pclTmpFlno,0x00,16);
			strcpy(pclTmpFlno,pclAlc);
			if (strlen(pclTmpFnr) > 4)
			{
				dbg(TRACE,"MakeExtFlno: Flight-Nr to big! Larger than 4 digits!");
				dbg(TRACE,"MakeExtFlno: Rejecting flight!");
			}
			else
			{
				strcat(pclTmpFlno,pclTmpFnr);
				ilRC = RC_SUCCESS;
				if (strlen(pclSuffix) > 1)
				{
					dbg(TRACE,"MakeExtFlno: Flight-Suffix to big!");
				}
				else
				{
					/*memcpy((char*)&pclTmpFlno[7],pclSuffix,1);*/
					strcat(pclTmpFlno,pclSuffix);
				}
				str_trm_all(pclTmpFlno," ",TRUE);
				/*dbg(DEBUG,"MakeExtFlno: Reformatted FLNO = <%s>",pclTmpFlno);*/
				strcpy(pcpFlno,pclTmpFlno);
				ilRC = RC_SUCCESS;
			}
		}
	}
	else
	{
		dbg(TRACE,"MakeExtFlno: FLIGHT-NR to short/big! Ignoring!");
	}
  return ilRC;
} /* end MakeExtFlno */
/* ******************************************************************** */
/* The ProcessBeltUpdate routine					*/
/* ******************************************************************** */
static int ProcessBeltUpdate(char *pcpData,char *pcpFields,char *pcpSelection)
{
	int ilBlt1 = FALSE;
	int ilBlt2 = FALSE;
	int ilPosBlt1 = 0;
	int ilPosBlt2 = 0;
	int ilCol = 0;
	int ilFromStart = 0;
	int ilRc = RC_SUCCESS;
	int ilRc_SQL;
	int ilCnt = 0;
	int ilCnt2 = 0;
	int ilDataLen = 0;
	char pclOldData[XL_BUFF];
	char pclNewData[XL_BUFF];
	char pclOldBlt1[XS_BUFF];
	char pclOldBlt2[XS_BUFF];
	char pclNewBlt1[XS_BUFF];
	char pclNewBlt2[XS_BUFF];
  char pclSqlBuf[L_BUFF];
	char pclResultBuf[XL_BUFF];
	char pclSendBuffer[XL_BUFF];
  char pclTmpSqlAnswer[XL_BUFF];
  char pclData[DATABLK_SIZE];
	short slFkt = 0;
	short slCursor = 0;

	memset(pclOldData,0x00,XL_BUFF);
	memset(pclNewData,0x00,XL_BUFF);
	memset(pclOldBlt1,0x00,XS_BUFF);
	memset(pclOldBlt2,0x00,XS_BUFF);
	memset(pclNewBlt1,0x00,XS_BUFF);
	memset(pclNewBlt2,0x00,XS_BUFF);

	GetDataItem(pclNewData,pcpData,1,'\n',"","");
	TrimAll(pclNewData);
	GetDataItem(pclOldData,pcpData,2,'\n',"","");
	TrimAll(pclOldData);
	if ((ilRc=FindItemInList(pcpFields,"BLT1",',',&ilPosBlt1,&ilCol,&ilFromStart)==RC_SUCCESS))
	{
		GetDataItem(pclOldBlt1,pclOldData,ilPosBlt1,',',"","");
		TrimAll(pclOldBlt1);
		GetDataItem(pclNewBlt1,pclNewData,ilPosBlt1,',',"","");
		TrimAll(pclNewBlt1);
		if (strlen(pclNewBlt1) < 1)
		{
			strcpy(pclNewBlt1,pclOldBlt1);
		}
		ilBlt1 = TRUE;
	}
	if ((ilRc=FindItemInList(pcpFields,"BLT2",',',&ilPosBlt2,&ilCol,&ilFromStart)==RC_SUCCESS))
	{
		GetDataItem(pclOldBlt2,pclOldData,ilPosBlt2,',',"","");
		TrimAll(pclOldBlt2);
		GetDataItem(pclNewBlt2,pclNewData,ilPosBlt2,',',"","");
		TrimAll(pclNewBlt2);
		if (strlen(pclNewBlt2) < 1)
		{
			strcpy(pclNewBlt2,pclOldBlt2);
		}
		ilBlt2 = TRUE;
	}	

	if ((ilBlt1==TRUE&&strlen(pclOldBlt1)>0) || (ilBlt2==TRUE && strlen(pclOldBlt2)>0))
	{
		sprintf(pclSqlBuf,"SELECT %s FROM AFT%s %s",pcgValid_SAI_Fields,pcgTabEnd,pcpSelection);
		slFkt = START; 
		slCursor = 0;
		memset(pclTmpSqlAnswer,0x00,XL_BUFF);
		memset(pclData,0x00,DATABLK_SIZE);
		dbg(DEBUG,"PBU: (BLT)-SQL <%s>",pclSqlBuf);
		if ((ilRc_SQL = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))==RC_SUCCESS)
		{
			ilCnt++;
			BuildItemBuffer(pclTmpSqlAnswer,pcgValid_SAI_Fields,0,",");
			strcat(pclTmpSqlAnswer,"\n");
			strcat(pclData,pclTmpSqlAnswer);
		}
		close_my_cursor(&slCursor);
		/* delete the last "\n" for not confusing the GetNoOfElements()-function */
		pclData[strlen(pclData)-1] = 0x00;

		if (ilBlt1==TRUE && ilRc_SQL == RC_SUCCESS)
		{
			dbg(DEBUG,"PBU: Belt to delete from display <%s>",pclOldBlt1);
			memset(pclResultBuf,0x00,XL_BUFF);
			if ((ilRc = Convert_SAI(1,pclData,0,pclResultBuf)) == RC_SUCCESS)
			{
				MakeSingleEntry(pclOldBlt1,2,LEFT,' ',(char*)&pclResultBuf[8]);
				memset(pclSendBuffer,0x00,XL_BUFF);
				if ((ilRc = MakeSendBuffer(TRUE,MSG_SAI,pclResultBuf,
											pclSendBuffer)) == RC_SUCCESS)
				{
					ilDataLen = strlen(pclSendBuffer);
					if ((ilRc = Send_data(igSock,pclSendBuffer,ilDataLen))==RC_FAIL)
					{
						CloseTCP();
					}
					else
					{
						ilRc = WaitForAck(igSock,pclSendBuffer,ilDataLen);
					}
				}
				else
				{
					dbg(TRACE,"PBU: MakeSendBuffer() <SAI> failed!");
				}
			}
		}
		
		if (ilBlt2==TRUE && ilRc_SQL == RC_SUCCESS)
		{
			dbg(DEBUG,"PBU: Belt to delete from display <%s>",pclOldBlt2);
			memset(pclResultBuf,0x00,XL_BUFF);
			if ((ilRc = Convert_SAI(1,pclData,1,pclResultBuf)) == RC_SUCCESS)
			{
				MakeSingleEntry(pclOldBlt2,2,LEFT,' ',(char*)&pclResultBuf[8]);
				memset(pclSendBuffer,0x00,XL_BUFF);
				if ((ilRc = MakeSendBuffer(TRUE,MSG_SAI,pclResultBuf,
											pclSendBuffer)) == RC_SUCCESS)
				{
					ilDataLen = strlen(pclSendBuffer);
					if ((ilRc = Send_data(igSock,pclSendBuffer,ilDataLen))==RC_FAIL)
					{
						CloseTCP();
					}
					else
					{
						ilRc = WaitForAck(igSock,pclSendBuffer,ilDataLen);
					}
				}
				else
				{
					dbg(TRACE,"PBU: MakeSendBuffer() <SAI> failed!");
				}
			}
		}
	}
	else
	{
		ilRc = RC_FAIL;	
	}
	return ilRc;
}
/* ******************************************************************** */
/* The TrimAll() routine                                            */
/* ******************************************************************** */
static void TrimAll(char *pcpBuffer)
{
	const char *p = pcpBuffer;
	int i = 0;

	for (i = strlen(pcpBuffer); i > 0 && isspace(pcpBuffer[i-1]); i--);
			pcpBuffer[i] = '\0';
	for ( *p ; isspace(*p); p++);
	while ((*pcpBuffer++ = *p++) != '\0');
}
