#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Interface/cdi2.c 1.13 2010/07/01 17:54:06SGT heb Exp  $";
#endif /* _DEF_mks_version */

/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         : RBI                                                       */
/* Date           : May 1998                                                  */
/* Description    : CDI-Process handles TCP/IP-Connections to extenal systems */
/*                                                                            */
/* Update history : JWE 15.Feb. 2002 cleaned up some messy functions          */
/* 20050607 JIM: found event and no connection: check next event immediate    */
/* 20050609 JIM: PRF 7374: in WMQ a QUE_PUT normally closes the queue. This   */
/*               has to be avoided here, because we want to use the queue as  */
/*               buffer                                                       */
/* 20060627 JIM: Prf 8294: avoid multiple open of protocol files              */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
/* static char sccs_cdi2[]="@(#) UFIS 4.4 (c) ABB AAT/I cdi2.c 44.20 / 02/02/15 17:00:00 / JWE"; */
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* 
ACHTUNG !! ACHTUNG !! ACHTUNG !! ACHTUNG !! ACHTUNG !! ACHTUNG !! ACHTUNG !! 

Normalerweise sind mehrere Tabellendefinitionen fuer ein Kommando moeglich. Die Ausnahmen bilden die Free-Query-Kommandos sowie die Incoming-Data-Kommandos (DAT und DEL), welche jeweils nur mit einer Tabellendefinition arbeiten koennen..

ACHTUNG !! ACHTUNG !! ACHTUNG !! ACHTUNG !! ACHTUNG !! ACHTUNG !! ACHTUNG !! 
*/
/******************************************************************************/

/* This program is a MIKE main program */
#define 	U_MAIN
#define 	UGCCS_PRG
#define 	STH_USE

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "new_catch.h"
#include "hsbsub.h"
#include "db_if.h"
#include "debugrec.h"
#include "helpful.h"
#include "netin.h"
#include "tcputil.h"
#include "l_ccstime.h"
#include "send.h"
#include "cdi2.h"

#define MIN_SEQNO 1
#define MAX_SEQNO 99999

#undef UNIQUE

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/*FILE						*outp       = NULL;*/
int						debug_level = TRACE;

/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/
extern int 				SendRemoteShutdown(int);
extern void				snap(char *, int, FILE*);
extern long				nap(long);
extern int BuildItemBuffer(char *pcpData, char *pcpFieldList, int ipNoOfFields, char *pcpSepChr);
extern int get_real_item(char *, char *, int);

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  			*prgItem  = NULL;      /* The queue item pointer  */
static EVENT 			*prgEvent = NULL;      /* The event pointer       */

/******************************************************************************/
/* My Global variables                                                        */
/******************************************************************************/
static int igHoldQ2 = FALSE;
static int igSecondModID = 0;
static int igActiveQueue = 0;
static int igItemsOnQ2 = 0;
static int igModIdFtphdl = 0;
static int				igCCOCheck = 0;
static int				igDBLCheck = 0;
static int				igLastCCONo = 0;
static int				igNoENDCommand = 0;
static int				igUpdateService = iNOT_ACTIVE;
static int				igSignalNo = 0;
static int				igDBQueuesStopped = FALSE;
static int				igRestartTimeout = 0;
static time_t			time1;
static time_t			time2;
static char				pcgCfgFile[iMIN_BUF_SIZE];
static char				pcgProcessName[iMIN_BUF_SIZE];
static char				pcgTABEnd[iMIN];	 
static char				pcgHomeAP[iMIN];	 
static char				*pcgTCPReceiveMemory = NULL;
static char				*pcgTCPSendMemory = NULL;
static char				*pcgTCPSendCopy = NULL;
static char 			pcgUpdateStartTime[15];
static char 			pcgUpdateEndTime[15];
static int				igCDIEventNo = 0;
static int				igCDITcpPackageNo = 0;
static int				igDebugLevel = 0;
static CDIUnique		rgCDIUnique[4];
static CDIEvent		*prgCDIEvent = NULL;
static CDITcpPackage	*prgCDITcpPackage = NULL;
static CDIMain			rgTM;
static FTPConfig prgFtp; /* struct for hyper-dynamic programming of FTPHDL */

static int igSndInvCmd = FALSE;
static int igRcvInvCmd = FALSE;
static int igGotUpdCmd = FALSE;
static int igSndUpdCmd = FALSE;

static int igClientErr = FALSE;

static char pcgTwStart[64];
static int igMaxFd = 0;

static int igRouterID;

static int igTCPInCounter = 0;

static char pcgClientChars[100];
static char pcgServerChars[100];
static	char  	cgTdi1[24];
static	char  	cgTdi2[24];
static	char  	cgTich[24];
static char  cgHopo[8];
/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int 	InitCDI(char *, char *,int);
static int	Reset(void);              /* Reset program          */
void	Terminate(void);          /* Terminate program      */
static void	WaitForTerminate(int);          /* Terminate program      */
static void	HandleSignal(int);        /* Handles signals        */
static void	HandleQueErr(int);        /* Handles queuing errors */
static int	HandleData(void);         /* Handles event data     */
static void	HandleQueues(void);       /* Waiting for Sts.-switch*/
void ResetCCOInterval (void);

/******************************************************************************/
/* My Function prototypes	                                                   */
/******************************************************************************/
static int	CDI2Server(void);
static void SetIntValue(char *, int *, char *, char *, char *, int, int, int);
static char *GetViaByNumber(char *, int, int, int);
static void ToRProtFile(int);
static void ToSProtFile(int);
static void ToSCProtFile(int);
static int 	CreateTCPConnection(void);
static int 	ReceiveTcpIp(long, int, int);
static int 	CheckReceivedHeader(char **);
static int 	TcpWaitTimeout(int, long, int);
static int	HandleReceivedData(void);
static int	WriteERRToClient(char *);
static int	WriteEODToClient(void);
static int	CheckQueStatus(int);
static int 	CheckConnection(int);
static int  BuildEND(char *);
static int  BuildERR(char *);
static int  BuildCCO(char *);
static int SendClientCmd(void);
static int 	BuildDBL(void);
static int 	BuildEOD(void);
static void BuildHeader(CDI2Header *, char *, long);
static void ChangeHeader(CDI2Header *, int, char *);
static int 	CloseTCPConnection(void);
static int  CheckVersion(void);
static int 	SearchCommandTypeAndNumber(int, int *, int *);
static int	HandleCTLData(void);
static int	HandleDATData(void);
static int	HandleDELData(void);
static int 	HandleInventoryCmd(int);
static int 	HandleUpdateCmd(int);
static int 	HandleFreeQueryCmd(int);
static int 	HandleInternalCmd(int);
static int 	UtcToLocal(char *);
static int 	SendToQue(int, int, BC_HEAD *, CMDBLK *, char *, char *, char *,char *,int);
static void GetCDIInternalCmd(char*, char*, char*, char*, int*, int*, int*);
static void	PrepareTelegram(long, char *, CDITable *);
static int	BuildDATData(char *, char *, CDITable *);
static int	BuildDELData(char *, char *, CDITable *);
static int 	WriteTelegramToCLient(struct _CDICmd *);
static int      WriteSendFlag(char *, char *, CDITable *, char *);
static int	WriteToLogTab(char *, char *, char *, char *, char *, CDITable *, char *, char *, char *);
static int	ReadFromLogTab();
static int	ReplaceChar(char *, char , char );
static int	HandleAcknowledge(CDIIncomingData *);
static int	TimeToStr(char *pcpTime,time_t lpTime);
static int 	BuildFile(struct _CDICmd *);
static int 	SendFile(struct _CDICmd *, CDITable *);
static void PrintCDICmdStructure(struct _CDICmd *);
static int 	DeleteRowInFile(int, int);
static int 	HandleFileRecovery(int, int *, CDIRecover *, CDIRecover *);
static int	HandleDBStop(void);       
static int  PullEvent(void);
static int  PushEvent(void);
static void GetTime(char *);
static int  CheckTimeFrame(int, char *, char *, CDITable *);
static int  CheckTCPQuickly(void);
static int  HandleUniqueNumber(char *,char *,char *,int,long *,long *,long *);
static int	PushTCPPackage(int);
static int	PullTCPPackage(void);
static int	CombineData(CDITable *, char *, char *);
static int	SendFileToQue(struct _CDICmd *);
static char *CDIGetDataField(char *, UINT, char, int);
static char *CDICopyNextField(char *, char, char *, int);
static int CreateSecondQueue(void);
static int WriteToSecondQueue(int);
static int DeleteSecondQueue(void);
static int WriteFinaTelegram(int ,char *,char *);
static void CleanData(char *pcpMsg,int ipMsgLen,char cpChar, char cpRepl);
static int TriggerFtpHdl(struct _CDICmd *prpCDICmd,CDITable *prpCDITable);
static int ChangeCharFromTo(char *pcpData,char *pcpFrom,char *pcpTo);
static long lgUniqueDATNumber = 0;
static void TrimRight(char *s);
static int LocalToUtc(char *pcpTime);

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRC = RC_SUCCESS;
	int	ilCnt = 0;
	char	pclTmpBuf[iMIN_BUF_SIZE];
	char	pclTmpBuf1[iMIN_BUF_SIZE];

	
	/* General initialization	*/
	INITIALIZE;			
	igActiveQueue = mod_id;
	dbg(TRACE,"<MAIN VERSION> <SCM> <%s>",mks_version);
	/* 20060627 JIM: Prf 8294: avoid multiple open of protocol files */
	memset((void*) &rgTM,0,sizeof(rgTM));

	/* signal handling */
	(void)SetSignals(HandleSignal);
	(void)UnsetSignals();

	/* get config-file from sgs.tab */
	strcpy(pclTmpBuf, argv[0]);
	StringUPR((UCHAR*)pclTmpBuf);

	/* copy process-name to global buffer */
	strcpy(pcgProcessName, argv[0]);
	dbg(DEBUG,"<MAIN> Process-Name: <%s>", pcgProcessName);

	memset(pclTmpBuf1,0x00,iMIN_BUF_SIZE);
	/* get data from sgs.tab */
	if ((ilRC = tool_search_exco_data("CDI2", pclTmpBuf, pclTmpBuf1)) != RC_SUCCESS)
	{
		dbg(TRACE,"<MAIN> cannot find config-file-entry for <%s> in sgs.tab!",  pclTmpBuf);
		dbg(TRACE,"<MAIN> using my own processname to search for config-file name!");
		sprintf(pclTmpBuf1,"%s.cfg",pcgProcessName);
		/*WaitForTerminate(45);*/
	}
	sprintf(pcgCfgFile,"%s/%s", getenv("CFG_PATH"), pclTmpBuf1);
	dbg(DEBUG,"<MAIN> Cfg-File: <%s>",  pcgCfgFile);

	/* get data from sgs.tab */
	if ((ilRC = tool_search_exco_data("ALL", "TABEND", pcgTABEnd)) != RC_SUCCESS)
	{
		dbg(TRACE,"<MAIN> cannot find entry <TABEND> in sgs.tab");
		WaitForTerminate(45);
	}
	dbg(DEBUG,"<MAIN> found TABEND: <%s>",  pcgTABEnd);

	/* read HomeAirPort from SGS.TAB */
	if ((ilRC = tool_search_exco_data("SYS", "HOMEAP", pcgHomeAP)) != RC_SUCCESS)
	{
		dbg(TRACE,"<MAIN> cannot find entry <HOMEAP> in SGS.TAB");
		WaitForTerminate(45);
	}
	dbg(DEBUG,"<MAIN> found HOME-AIRPORT: <%s>",  pcgHomeAP); 

	/* Attach to the MIKE queues */
	do
	{
		ilRC = init_que();
		if (ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"<MAIN> init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}
	} while ((ilCnt < 10) && (ilRC != RC_SUCCESS));
	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"<MAIN> init_que() failed! waiting 60 sec ...");
		WaitForTerminate(45);
	}
	else
	{
		dbg(TRACE,"<MAIN> init_que() OK! mod_id <%d>",  mod_id);
	}

	ilCnt = 0;
	do
	{
		ilRC = init_db();
		if (ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRC != RC_SUCCESS));
	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}
	else
	{
		dbg(TRACE,"MAIN: init_db()  OK!");
	} /* end of if */

	/* necessary for HSB (binary) */
	sprintf(pclTmpBuf,"%s/cdi2",getenv("BIN_PATH"));
	ilRC = TransferFile(pclTmpBuf);
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"<MAIN> TransferFile(%s) failed!", pclTmpBuf);
	} 

	/* necessary for HSB (configuration-file) */
	ilRC = TransferFile(pcgCfgFile);
	if(ilRC != RC_SUCCESS)
	{ 
		dbg(TRACE,"<MAIN> TransferFile(%s) failed!", pcgCfgFile); 
	} 

	/* ask VBL for this */
	ilRC = SendRemoteShutdown(mod_id);
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"<MAIN> SendRemoteShutdown(%d) failed!", mod_id);
	} 

	if ((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"<MAIN> waiting for status switch ...");
		HandleQueues();
	}

	if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"<MAIN> initializing ...");
		rgTM.iTCPStartSocket = -1; /* init the listen socket to avoid doing this always in the init-loop*/
		                           /* because InitCDI is always invoked when a connection is lost */
		if ((ilRC = InitCDI(pcgCfgFile, pcgProcessName,0)) != RC_SUCCESS)
		{
			dbg(TRACE,"<MAIN> =============== END (Re)InitCDI() ===============");
			dbg(TRACE,"<MAIN> InitCDI: init failed!");
			WaitForTerminate(45);
		}
	} 
	else 
	{
		dbg(TRACE,"<MAIN> wrong ctrl_sta (after HandleQueues)");
		WaitForTerminate(45);
	}
	dbg(TRACE,"<MAIN> initializing OK");

	while (1)
	{
		switch (ctrl_sta)
		{
			case HSB_ACTIVE:
			case HSB_ACT_TO_SBY:
			case HSB_STANDALONE:
				switch ((ilRC = CDI2Server()))
				{
					case iCDI_RESET:
						if ((ilRC = Reset()) == iCDI_TERMINATE)
							WaitForTerminate(0);
						break;

					default:
						WaitForTerminate(0);
						break;
				}
				break;

			default:
				HandleQueues();
				break;
		}
	} 
} 

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int InitCDI(char *pcpCfgFile, char *pcpServerName,int ipCalledBy)
{
  int ilRC = RC_SUCCESS;		
  int ilDbgSave = debug_level; /* keep the level for reseting it after init */		
  int ilCmdType;
  int ilCurCmd;
  int ilCurTab;
  int ilCurKey;
  int ilAssignCnt;
  int ilCurAssign;
  int ilViaFlag;
  int *pilCmdCnt = NULL;
  int ilNeedFtpData = FALSE;
  int ilCurRule;
  int ilCurRuleField;
  UINT ilErrorFlag;
  char *pclS = NULL;
  char pclTmpBuf[iMIN_BUF_SIZE];
  char pclDbgLevel[iMIN_BUF_SIZE];
  char pclPosBuf[iMIN_BUF_SIZE];
  char pclKeyWord[iMIN_BUF_SIZE];
  char pclAllCommands[iMAX_BUF_SIZE];
  char pclCurCmd[iMIN];
  char pclCurTab[iMIN_BUF_SIZE];
  char pclAllTabSection[iMAX_BUF_SIZE];
  char pclInternalField[iMIN];
  char pclExternalField[iMIN];
  char pclCurAssign[iMIN_BUF_SIZE];
  char pclSearch[iMIN];
  char pclRules[iMAX_BUF_SIZE];
  char pclAllRuleSections[iMAX_BUF_SIZE];
  char pclCurRuleSection[iMIN_BUF_SIZE];
  char pclWrongVia[iMIN];
  char pclWrongLwrVia[iMIN];
  CDICmd **prlCmdPtr = NULL;
  FILE *pFh;
  int ilI;
  char *pclTmpPtr;
  char pclTmpNum[16];
  char pclDataArea[2256];
  char pclSqlBuf[256];
  short slCursor = 0;

  if (ipCalledBy == 0)
     debug_level = DEBUG;
  else
     debug_level = TRACE;

  dbg(TRACE,"<InitCDI> =============== START (Re)InitCDI() ==============");

  /* to inform the status-handler about startup time */
  if ((ilRC = SendStatus("TCPIP", pcgProcessName, "START", 1, "", "", "")) != RC_SUCCESS)
  {
     dbg(TRACE,"<InitCDI> SendStatus returns: <%d>",ilRC);
     return RC_FAIL;
  }

  dbg(DEBUG,"<InitCDI> --------------- CDI2 SETTINGS (MAIN) --------");
  if ((ilRC = iGetConfigRow(pcpCfgFile, "MAIN","debug", CFG_STRING, pclDbgLevel )) != RC_SUCCESS )
  {
     dbg(DEBUG,"<InitCDI> DEBUG_LEVEL IS DEFAULT <TRACE>");
     ilDbgSave = TRACE;
  }
  else
  {
     if (strcmp(pclDbgLevel,"DEBUG") == 0)
     {
        dbg(DEBUG,"<InitCDI> DEBUG_LEVEL IS <DEBUG>");
        ilDbgSave = DEBUG;
     }
     else if (strcmp(pclDbgLevel,"OFF") == 0)
     {
        dbg(DEBUG,"<InitCDI> DEBUG_LEVEL IS <OFF>");
        ilDbgSave = 0;
     }
     else
     {
        dbg(DEBUG,"<InitCDI> DEBUG_LEVEL IS DEFAULT <TRACE>");
        ilDbgSave = TRACE;
     }
  }

  /* mod-id of router... */
  if ((igRouterID = tool_get_q_id("router")) == RC_NOT_FOUND || igRouterID == RC_FAIL)
  {
     dbg(TRACE,"<InitCDI> tool_get_q_id(\"router\") returns: <%d>", igRouterID);
     return RC_FAIL;
  }

  /* timeout before restart */
  ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "restart_timeout", CFG_INT, (char*)&igRestartTimeout);
  if (ilRC != RC_SUCCESS)
  {
     dbg(DEBUG,"<InitCDI> missing restart_timeout, using 30 seconds");
     igRestartTimeout = 30;
  }
  if (igRestartTimeout < 0 || igRestartTimeout > 3600)
  {
     dbg(TRACE,"<InitCDI> invalid restart_timeout:%d, range between 0 and 3600 seconds, using 30 seconds",
         igRestartTimeout);
     igRestartTimeout = 30;
  }
  dbg(DEBUG,"<InitCDI> restart_timeout:%d", igRestartTimeout);

  /* next the global data separator */
  ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "data_separator", CFG_STRING, rgTM.pcSeparator);
  if (ilRC != RC_SUCCESS)
  {
     /* this is the default separator */
     strcpy(rgTM.pcSeparator, "#"); 
  }
  dbg(DEBUG,"<InitCDI> data separator: <%s>", rgTM.pcSeparator);

  /* read the shell... */
  rgTM.pcShell[0] = 0x00;
  if ((ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "shell", CFG_STRING, rgTM.pcShell)) != RC_SUCCESS)
  {
     strcpy(rgTM.pcShell, "ksh"); 
  }
  dbg(DEBUG,"<InitCDI> using shell: <%s>", rgTM.pcShell);

  /* which time unit */
  pclTmpBuf[0] = 0x00;
  (void)iGetConfigRow(pcpCfgFile, "MAIN", "time_unit", CFG_STRING, pclTmpBuf);

  /* convert to uppercase */
  StringUPR((UCHAR*)pclTmpBuf);

  /* set it in structure... */
  SetIntValue("TIME_UNIT", &rgTM.iTimeUnit, pclTmpBuf, "SECONDS", "MICRO_SECONDS", iSECONDS,
              iMICRO_SECONDS, iSECONDS);

  /* to inform other processes */
  ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "connect_mod_id", CFG_INT, (char*)&rgTM.iSendConnectModID);
  if (ilRC != RC_SUCCESS)
  {
     dbg(DEBUG,"<InitCDI> missing connect_mod_id!");
     rgTM.iSendConnectModID = -1;
  }
  else
  {
     /* get open command */
     ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "open_connect_command", CFG_STRING, rgTM.pcSendOpenConnectCmd);
     if (ilRC == RC_SUCCESS)
     {
        dbg(DEBUG,"<InitCDI> OpenConnectCommand: <%s>",rgTM.pcSendOpenConnectCmd);
     }

     /* get close command */
     ilRC = iGetConfigRow(pcpCfgFile,"MAIN", "close_connect_command", CFG_STRING, rgTM.pcSendCloseConnectCmd);
     if (ilRC == RC_SUCCESS)
     {
        dbg(DEBUG,"<InitCDI> CloseConnectCommand: <%s>", rgTM.pcSendCloseConnectCmd);
     }
  }

  /* keys for unique numbers... */
  if ((ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "cco_key", CFG_STRING, rgTM.pcCCOKey)) != RC_SUCCESS)
  {
     dbg(TRACE,"<InitCDI> missing CCO_KEY");
     return RC_FAIL; 
  }

  /* keys for unique numbers... */
  if ((ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "dat_key", CFG_STRING, rgTM.pcDATKey)) != RC_SUCCESS)
  {
     dbg(TRACE,"<InitCDI> missing DAT_KEY");
     return RC_FAIL; 
  }

  /* keys for unique numbers... */
  if ((ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "ctl_key", CFG_STRING, rgTM.pcCTLKey)) != RC_SUCCESS)
  {
     dbg(TRACE,"<InitCDI> missing CTL_KEY");
     return RC_FAIL; 
  }

  /* keys for unique numbers... */
  if ((ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "del_key", CFG_STRING, rgTM.pcDELKey)) != RC_SUCCESS)
  {
     dbg(TRACE,"<InitCDI> missing DEL_KEY");
     return RC_FAIL; 
  }

  /* read current version */
  rgTM.pcVersion[0] = 0x00;
  if ((ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "version", CFG_STRING, rgTM.pcVersion)) != RC_SUCCESS)
  {
     /* set to default (version 2.x) */
     rgTM.pcVersion[0] = 0x00;
  }

  if (!strlen(rgTM.pcVersion))
  {
     /* set to default (version 2.x) */
     rgTM.pcVersion[0] = 0x00;
     /*strcpy(rgTM.pcVersion, GetDataField(sccs_cdi2, 6, ' '));*/
     strcpy(rgTM.pcVersion, GetDataField(mks_version, 5, ' '));
     DeleteCharacterInString(rgTM.pcVersion, '.');

     /* plus 100 */
     rgTM.pcVersion[0] += 1; 

     dbg(DEBUG,"<InitCDI> version from SCM-version-string: <%s>", rgTM.pcVersion);
     pclS = rgTM.pcVersion;
     while (*pclS)
     {
        if (!isdigit(*pclS))
        {
           rgTM.pcVersion[0] = 0x00;
           break;
        }
        pclS++;
     }

     /* set default if necessary */
     if (!strlen(rgTM.pcVersion))
        strcpy(rgTM.pcVersion, "200");
  }

  rgTM.iVersion = atoi(rgTM.pcVersion);
  dbg(DEBUG,"<InitCDI> setting Version to: <%s> <%d>", rgTM.pcVersion, rgTM.iVersion);

  /* for NAP */
  if ((ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "nap", CFG_INT, (char*)&rgTM.lNapTime)) != RC_SUCCESS)
  {
     rgTM.lNapTime = 0;
  }
  dbg(DEBUG,"<InitCDI> Nap-Time: %ld", rgTM.lNapTime);

  /* read secure option */
  pclTmpBuf[0] = 0x00;
  (void)iGetConfigRow(pcpCfgFile, "MAIN", "security", CFG_STRING, pclTmpBuf);
	
  /* to uppercase */
  StringUPR((UCHAR*)pclTmpBuf);

  /* set it in structure... */
  SetIntValue("SECURE OPTION", &rgTM.iSecureOption, pclTmpBuf, "ON", "OFF", 1, 0, 1);

  ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "WriteFINATelegram", CFG_INT, (char*)&rgTM.iWriteFINATelegram);
  if (ilRC != RC_SUCCESS)
  {
     rgTM.iWriteFINATelegram = 0;
     dbg(DEBUG,"<InitCDI> WriteFINATelegram: <%d> => not sending FINA-telegram!",rgTM.iWriteFINATelegram);
  }
  else
  {
     dbg(DEBUG,"<InitCDI> WriteFINATelegram: <%d>", rgTM.iWriteFINATelegram);
     ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "FINA_DataType", CFG_STRING, (char*)&rgTM.cFINADataType);
     if (ilRC != RC_SUCCESS)
     {
        strncpy(rgTM.cFINADataType,"  ",2);
     }
     if (strlen(rgTM.cFINADataType)<2)
     {
        dbg(DEBUG,"<InitCDI> FINA_DataType length error! Set to default <  >!");
        strncpy(rgTM.cFINADataType,"  ",2);
     }
     if (strlen(rgTM.cFINADataType)>2)
     {
        dbg(DEBUG,"<InitCDI> FINA_DataType length error! Cutting after second byte!");
        rgTM.cFINADataType[2]=0x00;
     }
     dbg(DEBUG,"<InitCDI> FINA_DataType: <%s>", rgTM.cFINADataType);
  }
  /* By JWE 20.07.2001 because of Conrac requirements */
  ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "Key_adjustment", CFG_INT, (char*)&rgTM.iKey_adjustment);
  if (ilRC != RC_SUCCESS)
  {
     rgTM.iKey_adjustment = 0; /* 0=right adjusted 1=left adjusted */
     dbg(DEBUG,"<InitCDI> Key_adjustment: use default <%d> => key is adjusted to right side!",
         rgTM.iKey_adjustment);
  }
  else
  {
     if (rgTM.iKey_adjustment == 0)
     {
        dbg(DEBUG,"<InitCDI> Key_adjustment: <%d> => key is adjusted to right side!",rgTM.iKey_adjustment);
     }
     else
     {
        if (rgTM.iKey_adjustment == 1)
        {
           dbg(DEBUG,"<InitCDI> Key_adjustment: <%d> => key is adjusted to left side!",rgTM.iKey_adjustment);
        }
        else
        {
           dbg(DEBUG,"<InitCDI> Key_adjustment: <%d> => unknown value! use 0=right or 1=left!",
               rgTM.iKey_adjustment);
           rgTM.iKey_adjustment = 0; /* 0=right adjusted 1=left adjusted */
        }
     }
  }

  ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "Key_default", CFG_INT, (char*)&rgTM.iKey_default);
  if (ilRC != RC_SUCCESS)
  {
     rgTM.iKey_default = 0; /* 0=0x30 1=0x20 */
     dbg(DEBUG,"<InitCDI> Key_default: use default <%d> => key fill char is 0x30='0'!",rgTM.iKey_default);
  }
  else
  {
     if (rgTM.iKey_default == 0)
     {
        dbg(DEBUG,"<InitCDI> Key_default: <%d> => key fill char is 0x30='0'!",rgTM.iKey_default);
     }
     else
     {
        if (rgTM.iKey_default == 1)
        {
           dbg(DEBUG,"<InitCDI> Key_default: <%d> => key fill char is 0x20=' '!",rgTM.iKey_default);
        }
        else
        {
           dbg(DEBUG,"<InitCDI> Key_default: <%d> => unknown value! use 0='0' or 1=' '!",rgTM.iKey_default);
           rgTM.iKey_default = 0; 
        }
     }
  }


  /* server name for unique number */
  sprintf(rgTM.pcServerName, "CDI2TMP%s", &pcpServerName[5]);

  dbg(DEBUG,"<InitCDI> CCOName: <%s>", rgTM.pcCCOKey);
  dbg(DEBUG,"<InitCDI> DATName: <%s>", rgTM.pcDATKey);
  dbg(DEBUG,"<InitCDI> DELName: <%s>", rgTM.pcDELKey);
  dbg(DEBUG,"<InitCDI> CTLName: <%s>", rgTM.pcCTLKey);
  dbg(DEBUG,"<InitCDI> Srv.Name:<%s>", rgTM.pcServerName);

  /* period we check connection */
  ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "check_connection", CFG_INT, (char*)&rgTM.iCheckConnection);
  if (ilRC != RC_SUCCESS)
  {
     /* default-value is 100 seconds */
     rgTM.iCheckConnection = 100;
  }
  dbg(DEBUG,"<InitCDI> CheckConnection every <%d> seconds",rgTM.iCheckConnection);

  /* period between the cco answer has to be sent */
  ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "check_response", CFG_INT, (char*)&rgTM.iCheckResponse);
  if (ilRC != RC_SUCCESS)
  {
     /* default-value is 5 seconds */
     rgTM.iCheckResponse = 5;
  }
  dbg(DEBUG,"<InitCDI> CheckResponse period is <%d> seconds",rgTM.iCheckResponse);

  /* act as client -> connect to remote server */
  (void) iGetConfigRow(pcpCfgFile, "MAIN", "make_connect", CFG_STRING,pclTmpBuf);
    
  /* convert to uppercase */
  StringUPR((UCHAR*)pclTmpBuf);

  SetIntValue("MAKE_CONNECT", &(rgTM.iMake_Connect), pclTmpBuf, "YES", "NO", TRUE, FALSE, FALSE);

  if (rgTM.iMake_Connect == TRUE)
  {
     /* get remote machine */
     if ((ilRC = iGetConfigRow(pcpCfgFile,"MAIN", "rhost", CFG_STRING, rgTM.pcRemote_Host)) != RC_SUCCESS)
     {
        dbg(TRACE,"<InitCDI> error reading rhost");
        rgTM.iMake_Connect = FALSE;	
     } 
     dbg(DEBUG,"<InitCDI> remote-machine: <%s>", rgTM.pcRemote_Host);
  }

  /*add linefeed at the   */
  iGetConfigRow(pcpCfgFile, "MAIN", "add_linefeed", CFG_STRING,pclTmpBuf);

  /* convert to uppercase */
  StringUPR((UCHAR*)pclTmpBuf);
    
  SetIntValue("ADD_LINEFEED",&rgTM.iAddLFatEnd, pclTmpBuf, "YES", "NO",1,0,0);

  /* recover file, name and path */
  ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "recover_file", CFG_STRING, rgTM.pcRecoverFile);
  if (ilRC != RC_SUCCESS)
  {
     /* set default for it... */
     sprintf(rgTM.pcRecoverFile, "%s/recover%s.dat", getenv("TMP_PATH"), pcpServerName); 
  }
  dbg(DEBUG,"<InitCDI> recover file is: <%s>", rgTM.pcRecoverFile);

  /* counter for recovery */
  ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "recover_counter", CFG_INT, (char*)&rgTM.iRecoverCounter);
  if (ilRC != RC_SUCCESS)
  {
     rgTM.iRecoverCounter = 5000;
  }
  dbg(DEBUG,"<InitCDI> recover counter is: <%d>",rgTM.iRecoverCounter);

  /* ---------------------------------------------------------------------- */
  /* ---------------------------------------------------------------------- */
  /* ---------------------------------------------------------------------- */
  /* protocol (receive data) */
  dbg(DEBUG,"<InitCDI> --------------- CDI2 SETTINGS (PROT) --------");
  pclTmpBuf[0] = 0x00;
  (void)iGetConfigRow(pcpCfgFile, "PROT", "rprot", CFG_STRING, pclTmpBuf);

  /* convert to uppercase */
  StringUPR((UCHAR*)pclTmpBuf);

  /* set it in structure... */
  SetIntValue("RPROT", &rgTM.iUseRProt, pclTmpBuf, "ON", "OFF", 1, 0, 1);

  /* get protcol path and filename */
  if (rgTM.iUseRProt)
  {
     if ((ilRC = iGetConfigRow(pcpCfgFile, "PROT", "rprot_file", CFG_STRING, rgTM.pcRProtFile)) != RC_SUCCESS)
     {
        dbg(TRACE,"<InitCDI> error reading rprot_file, using default");
        sprintf(rgTM.pcRProtFile, "%s/rprot%05d.log", 
        getenv("TMP_PATH"), getpid());
     }
     else
     {
        sprintf(rgTM.pcRProtFile, "%s%05d.log", rgTM.pcRProtFile, getpid());	
     }
     dbg(DEBUG,"<InitCDI> R-PROT-FILE-NAME: <%s>",rgTM.pcRProtFile);
  }

  /* ---------------------------------------------------------------------- */
  /* ---------------------------------------------------------------------- */
  /* ---------------------------------------------------------------------- */
  /* protocol (send data) */
  pclTmpBuf[0] = 0x00;
  (void)iGetConfigRow(pcpCfgFile, "PROT", "sprot", CFG_STRING, pclTmpBuf);

  /* convert to uppercase */
  StringUPR((UCHAR*)pclTmpBuf);

  /* set it in structure... */
  SetIntValue("SPROT", &rgTM.iUseSProt, pclTmpBuf, "ON", "OFF", 1, 0, 1);

  /* get protcol path and filename */
  if (rgTM.iUseSProt)
  {
     if ((ilRC = iGetConfigRow(pcpCfgFile, "PROT", "sprot_file", CFG_STRING, rgTM.pcSProtFile)) != RC_SUCCESS)
     {
        dbg(TRACE,"<InitCDI> error reading rprot_file, using default");
        sprintf(rgTM.pcSProtFile, "%s/sprot%05d.log", 
        getenv("TMP_PATH"), getpid());
     }
     else
     {
        sprintf(rgTM.pcSProtFile, "%s%05d.log", rgTM.pcSProtFile, getpid());	
     }
     dbg(DEBUG,"<InitCDI> S-PROT-FILE-NAME: <%s>",rgTM.pcSProtFile);
  }

  /* ---------------------------------------------------------------------- */
  /* ---------------------------------------------------------------------- */
  /* ---------------------------------------------------------------------- */
  /* protocol (receive decoded data) */
  if (rgTM.iSecureOption)
  {
     pclTmpBuf[0] = 0x00;
     (void)iGetConfigRow(pcpCfgFile, "PROT", "rcprot", CFG_STRING, pclTmpBuf);

     /* convert to uppercase */
     StringUPR((UCHAR*)pclTmpBuf);

     /* set it in structure... */
     SetIntValue("RCPROT", &rgTM.iUseRCProt, pclTmpBuf, "ON", "OFF", 1, 0, 0);

     /* get protcol path and filename */
     if (rgTM.iUseRCProt)
     {
        ilRC = iGetConfigRow(pcpCfgFile, "PROT", "rcprot_file", CFG_STRING, rgTM.pcRCProtFile);
        if (ilRC != RC_SUCCESS)
        {
           dbg(TRACE,"<InitCDI> error reading rcprot_file, using default!");
           sprintf(rgTM.pcRCProtFile, "%s/rcprot%05d.log", 
           getenv("TMP_PATH"), getpid());
        }
        else
        {
           sprintf(rgTM.pcRCProtFile, "%s%05d.log", rgTM.pcRCProtFile, getpid());	
        }
        dbg(DEBUG,"<InitCDI> RC-PROT-FILE-NAME: <%s>", rgTM.pcRCProtFile);
     }

     /* ------------------------------------------------------------------- */
     /* ------------------------------------------------------------------- */
     /* ------------------------------------------------------------------- */
     /* protocol (send decoded data) */
     pclTmpBuf[0] = 0x00;
     (void)iGetConfigRow(pcpCfgFile, "PROT", "scprot", CFG_STRING, pclTmpBuf);

     /* convert to uppercase */
     StringUPR((UCHAR*)pclTmpBuf);

     /* set it in structure... */
     SetIntValue("SCPROT", &rgTM.iUseSCProt, pclTmpBuf, "ON", "OFF", 1, 0, 0);

     /* get protcol path and filename */
     if (rgTM.iUseSCProt)
     {
        ilRC = iGetConfigRow(pcpCfgFile, "PROT", "scprot_file", CFG_STRING, rgTM.pcSCProtFile);
        if (ilRC != RC_SUCCESS)
        {
           dbg(TRACE,"<InitCDI> error reading scprot_file, using default!");
           sprintf(rgTM.pcSCProtFile, "%s/scprot%05d.log", 
           getenv("TMP_PATH"), getpid());
        }
        else
        {
           sprintf(rgTM.pcSCProtFile, "%s%05d.log", rgTM.pcSCProtFile, getpid());	
        }
        dbg(DEBUG,"<InitCDI> SC-PROT-FILE-NAME: <%s>",rgTM.pcSCProtFile);
     }
  }
  else
  {
     rgTM.iUseRCProt = rgTM.iUseSCProt = 0;
  }

  dbg(DEBUG,"<InitCDI> --------------- CDI2 SETTINGS (TCP) ---------");
  /* TCPTimeout... */
  ilRC = iGetConfigRow(pcpCfgFile, "TCP", "tcp_timeout", CFG_INT, (char*)&rgTM.lTCPTimeout);
  if (ilRC != RC_SUCCESS)
  {
     if (rgTM.iTimeUnit == iSECONDS)
        rgTM.lTCPTimeout = 2;
     else	
        /* 2000000 micro-seconds = 2 seconds */
        rgTM.lTCPTimeout = 2000000;
  }
  else
  {
     if (rgTM.lTCPTimeout < 1)
     {
        if (rgTM.iTimeUnit == iSECONDS)
           rgTM.lTCPTimeout = 2;
        else	
           /* 2000000 micro-seconds = 2 seconds */
           rgTM.lTCPTimeout = 2000000;
     }
  }
  dbg(DEBUG,"<InitCDI> tcp-timeout is: <%d>",rgTM.lTCPTimeout);

  /* service_name ... */
  if ((ilRC = iGetConfigRow(pcpCfgFile, "TCP", "service_name", CFG_STRING, rgTM.pcServiceName)) != RC_SUCCESS)
  {
     dbg(TRACE,"<InitCDI> missing service_name, can't work ...");
     return RC_FAIL;
  }
  dbg(DEBUG,"<InitCDI> service_name <%s>",rgTM.pcServiceName);

  dbg(DEBUG,"<InitCDI> --------------- CDI2 SETTINGS (SERVER) ------");
  /* serversystem ... */
  ilRC = iGetConfigRow(pcpCfgFile, "SERVER", "serversystem", CFG_STRING, rgTM.pcServerSystem);
  if (ilRC != RC_SUCCESS)
  {
     dbg(TRACE,"<InitCDI> missing serversystem, can't work ...");
     return RC_FAIL;
  }
  dbg(DEBUG,"<InitCDI> serversystem <%s>",rgTM.pcServerSystem);

  /* sender ... */
  if ((ilRC = iGetConfigRow(pcpCfgFile, "SERVER", "sender", CFG_STRING, rgTM.pcSender)) != RC_SUCCESS)
  {
     dbg(TRACE,"<InitCDI> missing sender, can't work ...");
     return RC_FAIL;
  }
  dbg(DEBUG,"<InitCDI> sender <%s>",rgTM.pcSender);

  /* receiver ... */
  if ((ilRC = iGetConfigRow(pcpCfgFile, "SERVER", "receiver", CFG_STRING, rgTM.pcReceiver)) != RC_SUCCESS)
  {
     dbg(TRACE,"<InitCDI> missing receiver, can't work ...");
     return RC_FAIL;
  }
  dbg(DEBUG,"<InitCDI> receiver <%s>",rgTM.pcReceiver);

  /* get mod-id of ftphdl */
  if (( igModIdFtphdl = tool_get_q_id("ftphdl")) == RC_NOT_FOUND || igModIdFtphdl == RC_FAIL)
  {
     dbg(TRACE,"<InitCDI> tool_get_q_id(FTPHDL) returns: <%d>",igModIdFtphdl);
     dbg(TRACE,"<InitCDI> setting FTP-system-call mode!");
     igModIdFtphdl = 0;
     rgTM.iUseFtphdl = 0;
  }
  else
  {
  dbg(DEBUG,"<InitCDI> FTPHDL mod_id <%d>",igModIdFtphdl);
  }
  /* --------------------------------------------------------------------- */
  /* --------------------------------------------------------------------- */
  /* --------------------------------------------------------------------- */
  ilErrorFlag = 0x0000;
  for (ilCmdType=iINVENTORY_CMD; ilCmdType<=iINTERNAL_CMD; ilCmdType++)
  {
     switch (ilCmdType)
     {
        case iINVENTORY_CMD:
           strcpy(pclKeyWord, "inventory_commands");
           pilCmdCnt = &rgTM.iNoInventoryCmd;
           prlCmdPtr = &rgTM.prInventoryCmd;
           break;
        case iUPDATE_CMD:
           strcpy(pclKeyWord, "update_commands");
           pilCmdCnt = &rgTM.iNoUpdateCmd;
           prlCmdPtr = &rgTM.prUpdateCmd;
           break;
        case iFREE_QUERY_CMD:
           strcpy(pclKeyWord, "free_query_commands");
           pilCmdCnt = &rgTM.iNoFreeQueryCmd;
           prlCmdPtr = &rgTM.prFreeQueryCmd;
           break;
        case iINCOMING_DATA_CMD:
           strcpy(pclKeyWord, "incoming_data_commands");
           pilCmdCnt = &rgTM.iNoIncomingDataCmd;
           prlCmdPtr = &rgTM.prIncomingDataCmd;
           break;
        case iINTERNAL_CMD:
           strcpy(pclKeyWord, "internal_commands");
           pilCmdCnt = &rgTM.iNoInternalCmd;
           prlCmdPtr = &rgTM.prInternalCmd;
           break;
        default:
           dbg(TRACE,"<InitCDI> unknown command type!"); 
           return RC_FAIL;
     }

     /* set defaults */
     *pilCmdCnt = 0;
     *prlCmdPtr = NULL;

     dbg(DEBUG,"<InitCDI> --------------- CDI2 SETTINGS (COMMAND) -----");
     pclAllCommands[0] = 0x00;
     if ((ilRC = iGetConfigRow(pcpCfgFile, "COMMAND", pclKeyWord, CFG_STRING, pclAllCommands)) == RC_SUCCESS)
     {
        if (!strlen(pclAllCommands))
           ilRC = RC_FAIL;
     }

     /* set internal command, if necessary */
     if (ilCmdType == iINTERNAL_CMD && ilRC != RC_SUCCESS)
     {
        pclAllCommands[0] = 0x00;
        strcpy(pclAllCommands, "CCO,END,ERR,EOD,DBL");
        ilRC = RC_SUCCESS;
     }

     if (ilRC != RC_SUCCESS)
     {
        switch (ilCmdType)
        {
           case iINVENTORY_CMD:
              dbg(DEBUG,"<InitCDI> missing inventory_commands...");
              ilErrorFlag |= 0x0001;
              break;
           case iUPDATE_CMD:
              dbg(DEBUG,"<InitCDI> missing update_commands...");
              ilErrorFlag |= 0x0002;
              break;
           case iFREE_QUERY_CMD:
              dbg(DEBUG,"<InitCDI> missing free_query_commands...");
              ilErrorFlag |= 0x0004;
              break;
           case iINCOMING_DATA_CMD:
              dbg(DEBUG,"<InitCDI> missing incoming_data_commands...");
              ilErrorFlag |= 0x0008;
              break;
           default:
              dbg(TRACE,"<InitCDI> unknown command type"); 
              return RC_FAIL;
        }
        dbg(DEBUG,"<InitCDI> ErrorFlag: <%X>", ilErrorFlag);

        if (ilErrorFlag == 0x000F)
        {
           /* missing any command */
           dbg(TRACE,"<InitCDI> cannot find command");
           return RC_FAIL;
        }
     }
     else
     {
        dbg(DEBUG,"<InitCDI> initializing <%s>-command",CMD_TYPE(ilCmdType));
			
        /* convert to uppercase */
        StringUPR((UCHAR*)pclAllCommands);

        /* count commands */
        *pilCmdCnt = GetNoOfElements(pclAllCommands, cCOMMA);
        dbg(DEBUG,"<InitCDI> found <%d> <%s>", *pilCmdCnt, CMD_TYPE(ilCmdType));

        /* get memory for command structure... */
        if ((*prlCmdPtr = (CDICmd*)malloc((*pilCmdCnt)*sizeof(CDICmd))) == NULL)
        {
           dbg(TRACE,"<InitCDI> malloc-failure");
           return RC_FAIL;
        }
        memset((void*)(*prlCmdPtr), 0x00, (*pilCmdCnt)*sizeof(CDICmd));

        /* get memory for commands */
        for (ilCurCmd=0; ilCurCmd<*pilCmdCnt; ilCurCmd++)
        {
           pclCurCmd[0] = 0x00;
           strcpy(pclCurCmd, GetDataField(pclAllCommands, ilCurCmd, cCOMMA)); 
           if (strlen(pclCurCmd))
           {
              /* it looks good */
              dbg(TRACE,"<InitCDI> --------------- COMMAND <%s> ------",pclCurCmd);
              /* ----------------------------------------------------------- */
              /* ----------------------------------------------------------- */
              /* ----------------------------------------------------------- */
              /* the corresponding mod-id (see above) */ 
              (*prlCmdPtr)[ilCurCmd].iFileCreateModID = -1;
              (void)iGetConfigRow(pcpCfgFile, pclCurCmd, "file_create_mod_id", CFG_INT,
                                  (char*)&(*prlCmdPtr)[ilCurCmd].iFileCreateModID);
              if ((*prlCmdPtr)[ilCurCmd].iFileCreateModID == -1)
              {
                 /* this is a fatal error */
                 dbg(DEBUG,"<InitCDI> missing file_create_mod_id in section: <%s>",pclCurCmd);
                 if (igModIdFtphdl > 0)
                 {
                    dbg(DEBUG,"<InitCDI> setting default FTPHDL=<%d>!",igModIdFtphdl);
                    (*prlCmdPtr)[ilCurCmd].iFileCreateModID = igModIdFtphdl;
                 }
                 else 
                 {
                    rgTM.iUseFtphdl = 0;	
                    dbg(DEBUG,"<InitCDI> no FTPHDL available! Using FTP-system-call mode!");
                 }
              }

              /* ----------------------------------------------------------- */
              /* ----------------------------------------------------------- */
              /* ----------------------------------------------------------- */
              /* command see below */
              memset((void*)(*prlCmdPtr)[ilCurCmd].pcSendFileCmd, 0x00, iMIN);
              (void)iGetConfigRow(pcpCfgFile, pclCurCmd, "send_file_cmd", CFG_STRING,
                                  (*prlCmdPtr)[ilCurCmd].pcSendFileCmd);
              dbg(DEBUG,"<InitCDI> setting SEND_FILE_CMD to: <%s>", (*prlCmdPtr)[ilCurCmd].pcSendFileCmd);

              /* ----------------------------------------------------------- */
              /* ----------------------------------------------------------- */
              /* ----------------------------------------------------------- */
              /* should i send the file content to defined queue? */ 
              (*prlCmdPtr)[ilCurCmd].iSendFileToQue = -1;
              (void)iGetConfigRow(pcpCfgFile, pclCurCmd, "send_file_to_que", CFG_INT,
                                  (char*)&(*prlCmdPtr)[ilCurCmd].iSendFileToQue);
              dbg(DEBUG,"<InitCDI> setting SEND_FILE_TO_QUE to: <%d>",(*prlCmdPtr)[ilCurCmd].iSendFileToQue);
              if (strlen((*prlCmdPtr)[ilCurCmd].pcSendFileCmd) && (*prlCmdPtr)[ilCurCmd].iSendFileToQue == -1)
              {
                 /* this is a fatal error */
                 dbg(TRACE,"<InitCDI> missing SEND_FILE_TO_QUE in section: <%s>",pclCurCmd);
                 return RC_FAIL;
              }

              /* ----------------------------------------------------------- */
              /* ----------------------------------------------------------- */
              /* ----------------------------------------------------------- */
              /* sould i send EOD after update command? */
              pclTmpBuf[0] = 0x00;
              (void)iGetConfigRow(pcpCfgFile, pclCurCmd, "send_eod_after_update", CFG_STRING, pclTmpBuf);

              /* convert to uppercase */
              StringUPR((UCHAR*)pclTmpBuf);

              /* set it in structure... */
              SetIntValue("SEND_EOD_AFTER_UPDATE", &(*prlCmdPtr)[ilCurCmd].iSendEODAfterUpdate,
                          pclTmpBuf, "YES", "NO", 1, 0, 0);

              /* ----------------------------------------------------------- */
              /* ---------------- reading file info of command ------------- */
              /* ----------------------------------------------------------- */
              /* read local filepath for this command */
              (void)iGetConfigRow(pcpCfgFile, pclCurCmd, "ftp_localfilepath", CFG_STRING,
                                  (*prlCmdPtr)[ilCurCmd].pcLocalFilePath);
              if (!strlen((*prlCmdPtr)[ilCurCmd].pcLocalFilePath))
              {
              }
              dbg(DEBUG,"<InitCDI> local filepath: <%s>",(*prlCmdPtr)[ilCurCmd].pcLocalFilePath);

              (void)iGetConfigRow(pcpCfgFile, pclCurCmd, "ftp_localfilename", CFG_STRING,
                                  (*prlCmdPtr)[ilCurCmd].pcFileName);
              if (!strlen((*prlCmdPtr)[ilCurCmd].pcFileName))
              {
                 sprintf((*prlCmdPtr)[ilCurCmd].pcFileName, "%s.DAT", pclCurCmd);
              }
              dbg(DEBUG,"<InitCDI> local filename: <%s>",(*prlCmdPtr)[ilCurCmd].pcFileName);

              /* read remote filepath for this command */
              (void)iGetConfigRow(pcpCfgFile, pclCurCmd, "ftp_remotefilepath", CFG_STRING,
                                  (*prlCmdPtr)[ilCurCmd].pcRemoteFilePath);
              if (!strlen((*prlCmdPtr)[ilCurCmd].pcRemoteFilePath))
              {
              }
              dbg(DEBUG,"<InitCDI> remote filepath: <%s>",(*prlCmdPtr)[ilCurCmd].pcRemoteFilePath);

              /* read remote filename for this command */
              (void)iGetConfigRow(pcpCfgFile, pclCurCmd, "ftp_remotefilename", CFG_STRING,
                                  (*prlCmdPtr)[ilCurCmd].pcRFileName);
              if (!strlen((*prlCmdPtr)[ilCurCmd].pcRFileName))
              {
                 sprintf((*prlCmdPtr)[ilCurCmd].pcRFileName, "%s",(*prlCmdPtr)[ilCurCmd].pcFileName);
              }
              dbg(DEBUG,"<InitCDI> remote filename: <%s>",(*prlCmdPtr)[ilCurCmd].pcRFileName);

              /* ----------------------------------------------------------- */
              /* ----------------------------------------------------------- */
              /* ----------------------------------------------------------- */
              /* read homeairport for this command */
              (*prlCmdPtr)[ilCurCmd].pcHomeAirport[0] = 0x00;
              (void)iGetConfigRow(pcpCfgFile, pclCurCmd, "home_airport", CFG_STRING,
                                  (*prlCmdPtr)[ilCurCmd].pcHomeAirport);
              if (!strlen((*prlCmdPtr)[ilCurCmd].pcHomeAirport))
              {
                 strcpy((*prlCmdPtr)[ilCurCmd].pcHomeAirport, pcgHomeAP);
              }
              dbg(DEBUG,"<InitCDI> HomeAirport: <%s>",(*prlCmdPtr)[ilCurCmd].pcHomeAirport);

              /* ----------------------------------------------------------- */
              /* ----------------------------------------------------------- */
              /* ----------------------------------------------------------- */
              /* read table extension for this command */
              (*prlCmdPtr)[ilCurCmd].pcTableExtension[0] = 0x00;
              (void)iGetConfigRow(pcpCfgFile, pclCurCmd, "table_extension", CFG_STRING,
                                  (*prlCmdPtr)[ilCurCmd].pcTableExtension);
              if (!strlen((*prlCmdPtr)[ilCurCmd].pcTableExtension))
              {
                 strcpy((*prlCmdPtr)[ilCurCmd].pcTableExtension, pcgTABEnd);
              }
              dbg(DEBUG,"<InitCDI> TableExtension: <%s>",(*prlCmdPtr)[ilCurCmd].pcTableExtension);

              /* ----------------------------------------------------------- */
              /* ----------------------------------------------------------- */
              /* ----------------------------------------------------------- */
              /* should i use ftp */
              pclTmpBuf[0] = 0x00;
              (void)iGetConfigRow(pcpCfgFile, pclCurCmd, "use_ftp", CFG_STRING, pclTmpBuf);

              /* convert to uppercase */
              StringUPR((UCHAR*)pclTmpBuf);

              /* set it in structure... */
              SetIntValue("USE_FTP", &(*prlCmdPtr)[ilCurCmd].iUseFtp, pclTmpBuf, "YES", "NO", 1, 0, 0);

              if ((*prlCmdPtr)[ilCurCmd].iUseFtp)
              {
                 /* setting flag */
                 ilNeedFtpData = TRUE;
                 dbg(DEBUG,"<InitCDI> setting Fkt-Pointer to \"BuildFile\"");
                 (*prlCmdPtr)[ilCurCmd].pTelegramHandler = BuildFile;
              }
              else
              {
                 dbg(DEBUG,"<InitCDI> setting Fkt-Pointer to \"WriteTelegram\"");
                 (*prlCmdPtr)[ilCurCmd].pTelegramHandler = WriteTelegramToCLient;
              }

              /* set default to file pointer */
              (*prlCmdPtr)[ilCurCmd].pFh = NULL;

              /* store command */
              if (((*prlCmdPtr)[ilCurCmd].pcCmd = strdup(pclCurCmd)) == NULL)
              {
                 dbg(TRACE,"<InitCDI> strdup-failure!");
                 return RC_FAIL;
              }

              /* read all table-section for this command */
              pclAllTabSection[0] = 0x00;
              (void)iGetConfigRow(pcpCfgFile, pclCurCmd, "table_section", CFG_STRING, pclAllTabSection);
					
              /* set counter, clear buffer, etc... */
              (*prlCmdPtr)[ilCurCmd].iNextTableNo = 0;
              (*prlCmdPtr)[ilCurCmd].iNoReceivedTables = 0;
              memset((*prlCmdPtr)[ilCurCmd].rCDISave, 0x00, iMIN_BUF_SIZE*sizeof(CDISave));

              /* are there table sections defined? */ 
              if (!strlen(pclAllTabSection))
              {
                 /* defaults... */
                 (*prlCmdPtr)[ilCurCmd].iNoTable = 0;
                 (*prlCmdPtr)[ilCurCmd].prTabDef = NULL;
              }
              else
              {
                 /* convert to uppercase */
                 StringUPR((UCHAR*)pclAllTabSection);

                 /* count them */
                 (*prlCmdPtr)[ilCurCmd].iNoTable = GetNoOfElements(pclAllTabSection, cCOMMA);

                 /* get memory for table sections... */
                 if (((*prlCmdPtr)[ilCurCmd].prTabDef = (CDITable*)malloc((*prlCmdPtr)[ilCurCmd].iNoTable*sizeof(CDITable))) == NULL)
                 {
                    dbg(TRACE,"<InitCDI> malloc failure!");
                    return RC_FAIL;
                 }

                 /* read all table sections */
                 for (ilCurTab=0; ilCurTab<(*prlCmdPtr)[ilCurCmd].iNoTable; ilCurTab++)
                 {
                    dbg(DEBUG,"<InitCDI> clear Table-Section No: <%d>",ilCurTab);
                    memset((void*)&(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab], 0x00, sizeof(CDITable));

                    /* separate table-section list, store each element */
                    strcpy(pclCurTab, GetDataField(pclAllTabSection, ilCurTab, cCOMMA));
                    dbg(DEBUG, "<InitCDI> trying to read table_section: <%s>",pclCurTab);

                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* init ignore empty fields */
                    dbg(DEBUG,"<InitCDI> --------------- <%s>-subsection <%s> -----",pclCurCmd,pclCurTab);
                    pclTmpBuf[0] = 0x00;
                    (void)iGetConfigRow(pcpCfgFile, pclCurTab, "ignore_empty_fields", CFG_STRING, pclTmpBuf);

                    /* convert to uppercase */
                    StringUPR((UCHAR*)pclTmpBuf);

                    /* set int-value in structure... */
                    SetIntValue("IGNORE EMPTY FIELDS",
                                &(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iIgnoreEmptyFields,
                                pclTmpBuf, "YES", "NO", 1, 0, 1);

                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* init empty field handling*/
                    pclTmpBuf[0] = 0x00;
                    ilRC = iGetConfigRow(pcpCfgFile,pclCurTab,"empty_field_handling", CFG_STRING, pclTmpBuf);
                    if (ilRC != RC_SUCCESS)
                    {
                       dbg(DEBUG,"<InitCDI> missing key-word 'empty_field_handling' in table-section <%s>, use default value (DELETE_ALL_BLANKS)",pclCurTab);
                       strcpy(pclTmpBuf, "DELETE_ALL_BLANKS");
                    }

                    /* convert to uppercase */
                    StringUPR((UCHAR*)pclTmpBuf);

                    /* set int-value in structure... */
                    if (!strcmp(pclTmpBuf, "DELETE_ALL_BLANKS"))
                    {
                       dbg(DEBUG,"<InitCDI> setting empty_field_handling to <DELETE_ALL_BLANKS> in table-section <%s>", pclCurTab);
                       (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iEmptyFieldHandling = iDELETE_ALL_BLANKS;
                    }
                    else if (!strcmp(pclTmpBuf, "ONE_BLANK_REMAINING"))
                    {
                       dbg(DEBUG,"<InitCDI> setting empty_field_handling to <ONE_BLANK_REMAINING> in table-section <%s>", pclCurTab);
                       (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iEmptyFieldHandling = iONE_BLANK_REMAINING;
                    }
                    else if (!strcmp(pclTmpBuf, "KEEP_DATA_UNTOUCHED"))
                    {
                       dbg(DEBUG,"<InitCDI> setting empty_field_handling to <KEEP_DATA_UNTOUCHED> in table-section <%s>", pclCurTab);
                       (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iEmptyFieldHandling = iKEEP_DATA_UNTOUCHED;
                    }
                    else
                    {
                       /* unknown entry/value */
                       dbg(TRACE,"<InitCDI> found unknown value in table-section <%s>, key-word <empty_field_handling>", pclCurTab);
                       dbg(TRACE,"<InitCDI> now use default entry: DELETE_ALL_BLANKS...");
                       (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iEmptyFieldHandling = iDELETE_ALL_BLANKS;
                    }
	
                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* init telegramtype */
                    pclTmpBuf[0] = 0x00;
                    (void)iGetConfigRow(pcpCfgFile, pclCurTab, "telegram", CFG_STRING, pclTmpBuf);

                    /* convert to uppercase */
                    StringUPR((UCHAR*)pclTmpBuf);

                    /* set int-value in structure... */
                    /*SetIntValue("TELEGRAM-TYPE", &(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iTelegramType,
                                pclTmpBuf, "DAT", "DEL", iDAT_CLASS, iDEL_CLASS, iDAT_CLASS);*/
                     if (strcmp(pclTmpBuf,"DEL") == 0)
                     {
                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iTelegramType = iDEL_CLASS;
                        dbg(TRACE,"InitCDI: Set TELEGRAM-TYPE to <%d>",
                            (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iTelegramType);
                     }
                     else if (strcmp(pclTmpBuf,"INS") == 0)
                     {
                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iTelegramType = iINS_CLASS;
                        dbg(TRACE,"InitCDI: Set TELEGRAM-TYPE to <%d>",
                            (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iTelegramType);
                     }
                     else
                     {
                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iTelegramType = iDAT_CLASS;
                        dbg(TRACE,"InitCDI: Set TELEGRAM-TYPE to <%d>",
                            (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iTelegramType);
                     }

                    /* set key-value */	
                    if ((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iTelegramType == iDAT_CLASS ||
                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iTelegramType == iINS_CLASS)
                       strncpy((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTelegramKey, rgTM.pcDATKey, iMIN);
                    else
                       strncpy((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTelegramKey, rgTM.pcDELKey, iMIN);
                    dbg(DEBUG,"<InitCDI> TelegramKey: <%s>",
                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTelegramKey);

                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* init mod-id */
                    (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iModID = -1;

                    /* read mod-id we send data to */						
                    (void)iGetConfigRow(pcpCfgFile, pclCurTab, "mod_id", CFG_INT,
                                        (char*)&(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iModID);

                    /* check it */
                    if ((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iModID == -1)
                       (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iModID = igRouterID;
                    dbg(DEBUG,"<InitCDI> Mod-Id is: %d",(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iModID);

                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* init via-length */
                    (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iVia34 = -1;

                    /* read mod-id we send data to */						
                    (void)iGetConfigRow(pcpCfgFile, pclCurTab, "via", CFG_INT,
                                        (char*)&(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iVia34);

                    /* check it */
                    if ((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iVia34 != 3 &&
                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iVia34 != 4)
                       (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iVia34 = 3;
                    dbg(DEBUG,"<InitCDI> Via-length is: %d",(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iVia34);

                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* init DataType */
                    (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcDataType[0] = 0x00;

                    /* read mod-id we send data to */						
                    (void)iGetConfigRow(pcpCfgFile, pclCurTab, "data_type", CFG_STRING,
                                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcDataType);

                    /* check it */
                    if (!strlen((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcDataType))
                       strcpy((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcDataType, "--");
						
                    dbg(DEBUG,"<InitCDI> DataType is: <%s>",
                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcDataType);

                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* init TableName */
                    (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTableName[0] = 0x00;
 
                    /* read mod-id we send data to */						
                    (void)iGetConfigRow(pcpCfgFile, pclCurTab, "table_name", CFG_STRING,
                                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTableName);

                    /* check it */
                    if (!strlen((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTableName))
                    {
                       /* can't work without DB-Table name */
                       dbg(TRACE,"<InitCDI> missing table_name in section <%s>", pclCurTab);
                       return RC_FAIL;
                    }

                    /* need 6 character tablename-length */
                    if (strlen((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTableName) == 3)
                       strcat((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTableName,
                              (*prlCmdPtr)[ilCurCmd].pcTableExtension);

                    /* convert to uppercase */
                    StringUPR((UCHAR*)(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTableName);
                    dbg(DEBUG,"<InitCDI> TableName is: <%s>",
                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTableName);

                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* init Selection */
                    (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcSelection[0] = 0x00;

                    /* read mod-id we send data to */						
                    (void*)iGetConfigRow(pcpCfgFile, pclCurTab, "select", CFG_STRING,
                                         (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcSelection);

                    /* convert it to uppercase */
                    StringUPR((UCHAR*)(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcSelection);
                    dbg(DEBUG,"<InitCDI> Selection is: <%s>",
                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcSelection);

                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* use received selection */
                    pclTmpBuf[0] = 0x00;
                    (void)iGetConfigRow(pcpCfgFile,pclCurTab,"use_received_selection", CFG_STRING, pclTmpBuf);

                    /* convert to uppercase */
                    StringUPR((UCHAR*)pclTmpBuf);

                    /* set it in structure... */
                    SetIntValue("USE_RECEIVED_SELECTION",
                                &(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iUseReceivedSelection,
                                pclTmpBuf, "YES", "NO", 1, 0, 1);

                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* time_mapping */
                    pclTmpBuf[0] = 0x00;
                    (void)iGetConfigRow(pcpCfgFile, pclCurTab, "time_mapping", CFG_STRING, pclTmpBuf);

                    /* convert to uppercase */
                    StringUPR((UCHAR*)pclTmpBuf);

                    /* set it in structure... */
                    SetIntValue("TIME_MAPPING", &(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iTimeMapping,
                                pclTmpBuf, "YES", "NO", 1, 0, 0);

                    /* time_mapping_local_2_utc */
                    pclTmpBuf[0] = 0x00;
                    (void)iGetConfigRow(pcpCfgFile, pclCurTab, "time_mapping_local2utc", CFG_STRING, pclTmpBuf);

                    /* convert to uppercase */
                    StringUPR((UCHAR*)pclTmpBuf);

                    /* set it in structure... */
                    SetIntValue("TIME_MAPPING_LOCAL2UTC", &(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iTimeMappingLocal2Utc,
                                pclTmpBuf, "YES", "NO", 1, 0, 0);

                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* time_mapping field list */
                    ilRC = iGetConfigRow(pcpCfgFile, pclCurTab, "time_fields", CFG_STRING,
                                         (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTimeMappingFields);
                    if (ilRC == RC_SUCCESS)
                    {
                       /* convert to uppercase */
                       StringUPR((UCHAR*)(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTimeMappingFields);
                       dbg(DEBUG,"<InitCDI> TimeMappingFields: <%s>",
                           (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTimeMappingFields);
                    }
                    else
                    {
                       /* clear buffer */
                       memset((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTimeMappingFields,
                              0x00, iMAX_BUF_SIZE);
                    }

                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* get rules to combine the data, first count them */
                    pclAllRuleSections[0] = 0x00;
                    (void)iGetConfigRow(pcpCfgFile,pclCurTab,"combine_data", CFG_STRING, pclAllRuleSections);

                    /* count the rules */
                    (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iNoCombineRules = GetNoOfElements(pclAllRuleSections, cCOMMA);

                    if ((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iNoCombineRules > 32)
                    {
                       /* cannot handle more than 32 rules... */
                       dbg(TRACE,"<InitCDI> can't handle %d rules, maximum is 32, use first 32...",
                           (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iNoCombineRules);
                       (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iNoCombineRules = 32;
                    }

                    /* get the rules itself */
                    for (ilCurRule=0; ilCurRule<(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iNoCombineRules;
                         ilCurRule++)
                    {
                       /* get n.th section... */
                       pclCurRuleSection[0] = 0x00;
                       strcpy(pclCurRuleSection, GetDataField(pclAllRuleSections, ilCurRule, cCOMMA));
                       dbg(DEBUG,"<InitCDI> Current Rule is: <%s>",pclCurRuleSection);

                       pclRules[0] = 0x00;
                       ilRC = iGetConfigRow(pcpCfgFile, pclCurTab, pclCurRuleSection, CFG_STRING, pclRules);
                       if (ilRC != RC_SUCCESS)
                       {
                          dbg(DEBUG,"<InitCDI> ERROR: missing rules for section: <%s>", pclCurRuleSection);
                          return RC_FAIL;
                       }
                       else
                       {
                          /* convert to uppercase */
                          StringUPR((UCHAR*)pclRules);

                          /* separate result fields and rules */
                          strcpy((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcCombineResultFields[ilCurRule],
                                 GetDataField(pclRules, 0, ':'));

                          /* set pointer */
                          pclS = pclRules;
                          while (*pclS && *pclS != ':')
                              pclS++;
                          pclS++;

                          /* set rules */
                          strcpy(pclRules, pclS);

                          /* separate the necessary fields */
                          (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].piNoOfRuleFields[ilCurRule] = GetNoOfElements(pclRules, cCOMMA);
                          (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcCombineFields[ilCurRule][0] = 0x00;
                          for (ilCurRuleField=0;
                               ilCurRuleField<(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].piNoOfRuleFields[ilCurRule];
                               ilCurRuleField++)
                          {
                             /* get n.th field */
                             pclTmpBuf[0] = pclPosBuf[0] = 0x00;
                             strcpy(pclPosBuf, GetDataField(pclRules, ilCurRuleField, cCOMMA));
                             strcpy(pclTmpBuf, GetDataField(pclPosBuf,0,'['));
                             strcpy(pclPosBuf, GetDataField(pclPosBuf,1,'['));
                             strcat((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcCombineFields[ilCurRule],
                                    pclTmpBuf);
                             if (ilCurRuleField < (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].piNoOfRuleFields[ilCurRule]-1)
                                strcat((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcCombineFields[ilCurRule], ",");

                             /* calculate start position and length */
                             if (pclPosBuf[strlen(pclPosBuf)-1] == ']')
                                pclPosBuf[strlen(pclPosBuf)-1] = 0x00;
                             (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].piPositions[ilCurRule][ilCurRuleField][iRULE_START_POS] = atoi(GetDataField(pclPosBuf, 0, ':'));
                             if ((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].piPositions[ilCurRule][ilCurRuleField][iRULE_START_POS] < 0) 
                             {
                                dbg(TRACE,"<InitCDI> Startposition of rule <%s> is wrong: %d",
                                    pclCurRuleSection,
                                    (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].piPositions[ilCurRule][ilCurRuleField][iRULE_START_POS]);
                                return RC_FAIL;
                             }

                             (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].piPositions[ilCurRule][ilCurRuleField][iRULE_LENGTH] = atoi(GetDataField(pclPosBuf, 1, ':'));
                             if ((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].piPositions[ilCurRule][ilCurRuleField][iRULE_LENGTH] < 0)
                             {
                                dbg(TRACE,"<InitCDI> Length of rule <%s> is wrong: %d", pclCurRuleSection,
                                    (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].piPositions[ilCurRule][ilCurRuleField][iRULE_LENGTH]);
                                return RC_FAIL;

                             }

                             dbg(DEBUG,"<InitCDI> Start-Pos: <%d> and Length: <%d>",
                                 (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].piPositions[ilCurRule][ilCurRuleField][iRULE_START_POS],
                                 (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].piPositions[ilCurRule][ilCurRuleField][iRULE_LENGTH]);
                          }

                          dbg(DEBUG,"<InitCDI> result field: <%s>",
                              (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcCombineResultFields[ilCurRule]);
                          dbg(DEBUG,"<InitCDI> necessary fields: <%s>",
                              (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcCombineFields[ilCurRule]);
                       }
                    } /* */

                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* init timeframe field list */
                    (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTimeFrameFields[0] = 0x00;

                    /* read timeframe field list */						
                    (void)iGetConfigRow(pcpCfgFile, pclCurTab, "timeframe_fields", CFG_STRING,
                                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTimeFrameFields);
                
                    /* check length of field list */
                    if (strlen((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTimeFrameFields))
                    {
                       /* convert to uppercase */
                       StringUPR((UCHAR*)(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTimeFrameFields);

                       /* count them */
                       (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iNoTimeFrameFields = GetNoOfElements((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTimeFrameFields, cCOMMA);

                       dbg(DEBUG,"<InitCDI> found <%d> TimeFrameFields: <%s>",
                           (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iNoTimeFrameFields,
                           (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTimeFrameFields);
                    }

                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* init send commad */
                    (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcSndCmd[0] = 0x00;

                    /* read mod-id we send data to */						
                    (void)iGetConfigRow(pcpCfgFile, pclCurTab, "snd_cmd", CFG_STRING,
                                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcSndCmd);

                    /* convert to uppercase */
                    StringUPR((UCHAR*)(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcSndCmd);

                    /* check it */
                    if (!strlen((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcSndCmd))
                    {
                       /* can't work without DB-Table name */
                       dbg(TRACE,"<InitCDI> missing snd_cmd in section <%s>",pclCurTab);
                       return RC_FAIL;
                    }
								
                    dbg(DEBUG,"<InitCDI> SndCmd is: <%s>",(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcSndCmd);

                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* init CurExternalFieldList, specially for free query */
                    (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcCurExternalFieldList[0] = 0x00;
                    (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iNoCurExternalFieldList = 0;

                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* init internal fields */
                    (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcExternalFieldList[0] = 0x00;

                    /* read mod-id we send data to */						
                    (void)iGetConfigRow(pcpCfgFile, pclCurTab, "fields", CFG_STRING,
                                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcExternalFieldList);

                    /* convert to uppercase */
                    StringUPR((UCHAR*)(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcExternalFieldList);

                    /* check it */
                    if (!strlen((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcExternalFieldList))
                    {
                       /* can't work without DB-Table name */
                       dbg(TRACE,"<InitCDI> missing fields in section <%s>",pclCurTab);
                       return RC_FAIL;
                    }

                    /* copy User-Field-List to Internal-Field-List... */
                    strcpy((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList,
                           (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcExternalFieldList);
							
                    /* read assignment */
                    (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcFieldTableAssignment[0] = 0x00;
                    (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iFieldTableAssignment = 0;

                    (void)iGetConfigRow(pcpCfgFile, pclCurTab, "field_assignment", CFG_STRING,
                                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcFieldTableAssignment);
                    if (strlen((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcFieldTableAssignment))
                    {
                       /* convert to uppercase */
                       StringUPR((UCHAR*)(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcFieldTableAssignment);

                       /* count assignments */
                       ilAssignCnt = GetNoOfElements((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcFieldTableAssignment , cCOMMA);
                       (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iFieldTableAssignment = ilAssignCnt;
                       (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcExternAssign[0] = 0x00;
                       (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternAssign[0] = 0x00;

                       for (ilCurAssign=0; ilCurAssign<ilAssignCnt; ilCurAssign++)
                       {
                           strcpy(pclCurAssign,
                                  GetDataField((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcFieldTableAssignment , ilCurAssign, cCOMMA));

                           pclInternalField[0] = 0x00;
                           pclExternalField[0] = 0x00;
                           (void)SeparateIt(pclCurAssign, pclInternalField, pclExternalField, '=');
                           dbg(DEBUG,"<InitCDI> InternalField: <%s>", pclInternalField);
                           dbg(DEBUG,"<InitCDI> ExternalField: <%s>", pclExternalField);
                           strcat((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcExternAssign,pclExternalField);
                           strcat((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcExternAssign,",");
       
                           strcat((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternAssign,pclInternalField);
                           strcat((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternAssign,",");

                           /* change field-list */
                           (void)SearchStringAndReplace((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcExternalFieldList, pclInternalField, pclExternalField);
                       }
                       (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcExternAssign [strlen((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcExternAssign)-1] = 0x00;
                       (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternAssign [strlen((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternAssign)-1] = 0x00;
                       dbg(DEBUG,"<InitCDI> InternAssign <%s>",
                           (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternAssign);
                       dbg(DEBUG,"<InitCDI> ExternAssign <%s>",
                           (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcExternAssign);
                    }

                    /* look for via's (display) */
                    ilViaFlag = 0x0000;
                    while ((pclS = strstr((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList, "VI0")) != NULL || (pclS = strstr((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList, "VIAL")) != NULL || (pclS = strstr((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList, "VIAN")) != NULL)
                    {
                       if (*(pclS+3) >= '1' && *(pclS+3) <= '9')
                       {
                          /* set flag */
                          ilViaFlag |= 0x0001;
                          memset((void*)pclSearch, 0x00, iMIN);
                          if (pclS != (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList)
                             strncpy(pclSearch, pclS-1, 5);
                          else
                             strncpy(pclSearch, pclS, 5);
                          dbg(DEBUG,"<InitCDI> Search-Via: <%s>",pclSearch);

                          /* change field-list */
                          (void)SearchStringAndReplace((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList, pclSearch, "");
                       }
                       else if (*(pclS+3) == 'N')
                       {
                          ilViaFlag |= 0x0002;
                          dbg(DEBUG,"<InitCDI> found VIAN");
                          (void)SearchStringAndReplace((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList, "VIAN", "vian");
                       }
                       else if (*(pclS+3) == 'L')
                       {
                          ilViaFlag |= 0x0004;
                          dbg(DEBUG,"<InitCDI> found VIAL");
                          (void)SearchStringAndReplace((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList, "VIAL", "vial");
                       }
                       else
                       {
                          sprintf(pclWrongVia, "VIA%c", *(pclS+3));
                          dbg(DEBUG,"<InitCDI> found wrong via: <%s>",pclWrongVia);
                          strcpy(pclWrongLwrVia, pclWrongVia);
                          StringLWR((UCHAR*)pclWrongLwrVia);
                          (void)SearchStringAndReplace((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList, pclWrongVia, pclWrongLwrVia);
                       }
                    }

                    /* to uppercase */
                    StringUPR((UCHAR*)(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList);

                    /* check display via flag */
                    if (ilViaFlag & 0x0001)
                    {
                       /* found display via... */
                       if (!(ilViaFlag & 0x0002))
                       {
                          dbg(DEBUG,"<InitCDI> add VIAN");
                          /* VIAN isn't currently in field-list, so add it */
                          strcat((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList, ",VIAN");
                       }
                       if (!(ilViaFlag & 0x0004))
                       {
                          dbg(DEBUG,"<InitCDI> add VIAL");
                          /* VIAL isn't currently in field-list, so add it */
                          strcat((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList, ",VIAL");
                       }
                    }

                    /* is there a URNO in InternalFieldList? */
                    /* if not, add it */
                    if (!strstr((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList, "URNO"))
                    {
                       dbg(DEBUG,"<InitCDI> add URNO");
                       /* URNO isn't currently in field-list, so add it */
                       strcat((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList, ",URNO");
                    }

                    /* for rules: attach all necessary rule fields... */
                    for (ilCurRule=0; ilCurRule<(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iNoCombineRules;
                         ilCurRule++)
                    {
                       /* check out fieldname */
                       if (strstr((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcExternalFieldList,
                                  (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcCombineResultFields[ilCurRule])
                                  != NULL)
                       {
                          (void)SearchStringAndReplace((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList, (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcCombineResultFields[ilCurRule], (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcCombineFields[ilCurRule]);
                          {
                             char *pclStartPtr; 
                             char *pclEndPtr; 
                             /* look for Jokerfields */
                             pclStartPtr = strstr((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList,"\"" );
                             if (pclStartPtr != NULL)
                             {
                                if ((pclEndPtr = strstr(pclStartPtr,"," )) == NULL)
                                {
                                   /* item is last in list*/
                                   if (strlen(pclStartPtr) < strlen((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList))
                                   {
                                      /* list of items is > 1 */
                                      /* delete also the preceeding ","*/ 
                                      --pclStartPtr;
                                   } 
                                   *pclStartPtr = 0x00;
                                   /* end of if last element*/ 
                                }
                                else
                                { 
                                   /* item is in middle of list*/
                                   pclEndPtr++;
                                   strcpy(pclStartPtr,pclEndPtr);
                                }
                             }
                          }
                       }
                    } /* end of for */ 

                    /* count internal and external fields */
                    (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iNoExternalFields = GetNoOfElements((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcExternalFieldList, ',');
                    (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iNoInternalFields = GetNoOfElements((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList, ',');

                    dbg(DEBUG,"<InitCDI> found %d ExternalFields: <%s>",
                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iNoExternalFields, 
                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcExternalFieldList);
                    dbg(DEBUG,"<InitCDI> found %d InternalFields: <%s>",
                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].iNoInternalFields, 
                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList);

                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* ----------------------------------------------------- */
                    /* init table name for send flag */
                    (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTableNameForFlag[0] = 0x00;
                    /* read table name which contains the send flag */
                    (void)iGetConfigRow(pcpCfgFile, pclCurTab, "table_name_for_flag", CFG_STRING,
                                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTableNameForFlag);
                    /* convert to uppercase */
                    StringUPR((UCHAR*)(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTableNameForFlag);
                    dbg(DEBUG,"<InitCDI> TableNameForFlag is: <%s>",
                        (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTableNameForFlag);
                    if ((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcTableNameForFlag[0] != 0x00)
                    {
                       /* init field name for send flag */
                       (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcFieldNameForFlag[0] = 0x00;
                       (void)iGetConfigRow(pcpCfgFile, pclCurTab, "field_name_for_flag", CFG_STRING,
                                           (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcFieldNameForFlag);
                       if (!strlen((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcFieldNameForFlag))
                       {
                          dbg(TRACE,"<InitCDI> missing field_name_for_flag in section <%s>",pclCurTab);
                          return RC_FAIL;
                       }
                       StringUPR((UCHAR*)(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcFieldNameForFlag);
                       dbg(DEBUG,"<InitCDI> FieldNameForFlag is: <%s>",
                           (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcFieldNameForFlag);
                       /* init field contents for send flag */
                       (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcFieldContentsForFlag[0] = 0x00;
                       (void)iGetConfigRow(pcpCfgFile, pclCurTab, "field_contents_for_flag", CFG_STRING,
                                           (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcFieldContentsForFlag);
                       if (!strlen((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcFieldContentsForFlag))
                       {
                          dbg(TRACE,"<InitCDI> missing field_contents_for_flag in section <%s>",pclCurTab);
                          return RC_FAIL;
                       }
                       StringUPR((UCHAR*)(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcFieldContentsForFlag);
                       dbg(DEBUG,"<InitCDI> FieldContentsForFlag is: <%s>",
                           (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcFieldContentsForFlag);

                       /* init wait for ack flag */
                       (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcWaitForAckFlag[0] = 0x00;
                       (void)iGetConfigRow(pcpCfgFile, pclCurTab, "wait_for_ack_flag", CFG_STRING,
                                           (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcWaitForAckFlag);
                       if (!strlen((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcWaitForAckFlag))
                          strcpy((*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcWaitForAckFlag,"NO");
                       StringUPR((UCHAR*)(*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcWaitForAckFlag);
                       dbg(DEBUG,"<InitCDI> WaitForAckFlag is: <%s>",
                           (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcWaitForAckFlag);


                    }
                    else
                    {
                       (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcFieldNameForFlag[0] = 0x00;
                       (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcFieldContentsForFlag[0] = 0x00;
                       (*prlCmdPtr)[ilCurCmd].prTabDef[ilCurTab].pcWaitForAckFlag[0] = 0x00;
                    }

                 }
              }
           }
        }
     }
  }

  /* FTP-specific entries */
  if (ilNeedFtpData == TRUE)
  {
     dbg(DEBUG,"<InitCDI> --------------- CDI2 SETTINGS (FTP) --------");
     /* get user */
     if ((ilRC = iGetConfigRow(pcpCfgFile, "FTP", "ftp_user", CFG_STRING, rgTM.pcFTPUser)) != RC_SUCCESS)
     {
        dbg(TRACE,"<InitCDI> error reading ftp_user...");
        return RC_FAIL;
     }
     dbg(DEBUG,"<InitCDI> FTP-User is: <%s>",rgTM.pcFTPUser);

     /* get passwd using iGetConfigRow ensures that special chars are also read */
     /* and the passwd is not being cut at those chars */
     if ((ilRC = iGetConfigRow(pcpCfgFile, "FTP", "ftp_pass", CFG_STRING, rgTM.pcFTPPass)) != RC_SUCCESS)
     {
        dbg(TRACE,"<InitCDI> error reading ftp_pass...");
        return RC_FAIL;
     }
		
     dbg(DEBUG,"<InitCDI> FTP-Passwd: <%s>",rgTM.pcFTPPass);

     /* set path and name of ftp-ctrl file */
     sprintf(rgTM.pcFTPCtrlFile, "%s/%s.ftp", getenv("TMP_PATH"), pcpServerName);
     dbg(DEBUG,"<InitCDI> FTP-control-file: <%s>",rgTM.pcFTPCtrlFile);

     /* get remote machine */
     ilRC = iGetConfigRow(pcpCfgFile, "FTP", "ftp_remotehost", CFG_STRING, rgTM.pcMachine);
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"<InitCDI> error reading machine");
        dbg(TRACE,"<InitCDI> Setting default <SEARCH_DYNAMIC>!");
        strcpy(rgTM.pcMachine,"SEARCH_DYNAMIC");
     }
     dbg(DEBUG,"<InitCDI> remote-machine: <%s>",rgTM.pcMachine);

     /* get flag for using inetrnal (system-call) or external (ftphdl) transmission mode */
     ilRC = iGetConfigRow(pcpCfgFile,"FTP","use_ftphdl",CFG_INT,(char*)&rgTM.iUseFtphdl);
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"<InitCDI> error reading use_ftphdl. Set default 1=(yes)!");
        rgTM.iUseFtphdl = 1;
     }
     dbg(DEBUG,"<InitCDI> use_ftphdl set to: <%d>",rgTM.iUseFtphdl);

     /* get mod-id of ftphdl to use */
     ilRC = iGetConfigRow(pcpCfgFile, "FTP", "ftphdl_modid", CFG_INT, (char*)&rgTM.iftphdl_modid);
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"<InitCDI> error reading ftphdl_modid. Set to main FTPHDL=<%d>",igModIdFtphdl);
        rgTM.iftphdl_modid = igModIdFtphdl;
     }
     else
     {
        igModIdFtphdl = rgTM.iftphdl_modid;
     }
     if (rgTM.iftphdl_modid == 0)
     {
        dbg(TRACE,"<InitCDI> no FTPHDL available. Setting <use_ftphdl = 0>");
        rgTM.iUseFtphdl = 0;
     }
     dbg(DEBUG,"<InitCDI> ftphdl_modid set to: <%d>",rgTM.iftphdl_modid);

     /* get flag for inverting (use ntohl function) dynamic ip or not */
     ilRC = iGetConfigRow(pcpCfgFile, "FTP", "ftp_reverse_ip", CFG_INT, (char*)&rgTM.iReverseIp);
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"<InitCDI> error reading ftp_reverse_ip. Set default 0=(no inverting)!");
        rgTM.iReverseIp = 0;
     }
     dbg(DEBUG,"<InitCDI> Reverse IP-Adress set to: <%d>",rgTM.iReverseIp);

     /* get remote machine type */
     ilRC = iGetConfigRow(pcpCfgFile, "FTP", "ftp_remotehosttype", CFG_STRING, rgTM.pcMachineType);
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"<InitCDI> error reading remote machine type!");
        dbg(TRACE,"<InitCDI> Setting default <UNIX>!");
        strcpy(rgTM.pcMachineType,"UNIX");
     }
     dbg(DEBUG,"<InitCDI> remote machine-type: <%s>",rgTM.pcMachineType);
  }	

  /* memory for TCP/IP (receiving data) */
  if ((pcgTCPReceiveMemory = (char*)malloc((size_t)iMAX_DATA)) == NULL)
  {
     dbg(TRACE,"<InitCDI> malloc failure (TCPMemory)");
     return RC_FAIL;
  }

  /* memory for TCP/IP (sending data) */
  if ((pcgTCPSendMemory = (char*)malloc((size_t)iMAX_DATA)) == NULL)
  {
     dbg(TRACE,"<InitCDI> malloc failure (TCPMemory)");
     return RC_FAIL;
  }

  /* memory for TCP/IP (copy of sending data) */
  if ((pcgTCPSendCopy = (char*)malloc((size_t)iMAX_DATA)) == NULL)
  {
     dbg(TRACE,"<InitCDI> malloc failure (TCPMemoryCopy)");
     return RC_FAIL;
  }

  /* clear mempory */
  memset((void*)pcgTCPReceiveMemory, 0x00, iMAX_DATA);
  memset((void*)pcgTCPSendMemory, 0x00, iMAX_DATA);

  /* open protocol-files (send + receive-data) */
  if (rgTM.iUseRProt)
  {
     if (rgTM.iRProtFile == 0)
     { 	/* 20060627 JIM: Prf 8294: avoid multiple open of protocol files */
        if ((rgTM.iRProtFile = open(rgTM.pcRProtFile, O_RDWR | O_CREAT | O_APPEND, 0666)) == -1)
        {
           dbg(TRACE,"<InitCDI> cannot open <%s>",rgTM.pcRProtFile);
        }
     }
  }
  else
  {
     rgTM.iRProtFile = -1;
  }

  if (rgTM.iUseSProt)
  {
     if (rgTM.iSProtFile == 0)
     { 	/* 20060627 JIM: Prf 8294: avoid multiple open of protocol files */
        if ((rgTM.iSProtFile = open(rgTM.pcSProtFile, O_RDWR | O_CREAT | O_APPEND, 0666)) == -1)
        {
           dbg(TRACE,"<InitCDI> cannot open <%s>",rgTM.pcSProtFile);
        }
     }
  }
  else
  {
     rgTM.iSProtFile = -1;
  }

  /* open protocol-files (send + receive-decoded data) */
  if (rgTM.iUseRCProt)
  {
     if (rgTM.iRCProtFile == 0)
     { 	/* 20060627 JIM: Prf 8294: avoid multiple open of protocol files */
        if ((rgTM.iRCProtFile = open(rgTM.pcRCProtFile, O_RDWR | O_CREAT | O_APPEND, 0666)) == -1)
        {
           dbg(TRACE,"<InitCDI> cannot open <%s>", rgTM.pcRCProtFile);
        }
     }
  }
  else
  {
     rgTM.iRCProtFile = -1;
  }

  if (rgTM.iUseSCProt)
  {
     if (rgTM.iSCProtFile == 0)
     { 	/* 20060627 JIM: Prf 8294: avoid multiple open of protocol files */
        if ((rgTM.iSCProtFile = open(rgTM.pcSCProtFile, O_RDWR | O_CREAT | O_APPEND, 0666)) == -1)
        {
           dbg(TRACE,"<InitCDI> cannot open <%s>",rgTM.pcSCProtFile);
        }
     }
  }
  else
  {
     rgTM.iSCProtFile = -1;
  }

  /* try to read file: servername.uni (cdi2_1.uni) */
  /* normally it's available in $TMP_PATH (/ceda/tmp) */
  /* but first init CDIUnique-Structure... */
  for (ilCurKey=iCTL_CLASS; ilCurKey<=iCCO_CLASS; ilCurKey++)
  {
     rgCDIUnique[ilCurKey].lCurrentNo = -1;
     rgCDIUnique[ilCurKey].lLastNo = -1;
     rgCDIUnique[ilCurKey].lMin = -1;
     rgCDIUnique[ilCurKey].lMax = -1;
     rgCDIUnique[ilCurKey].lAvailableNo = 0;
  }

  pclTmpBuf[0] = 0x00;
  sprintf(pclTmpBuf, "%s/%s.uni", getenv("TMP_PATH"), pcgProcessName);
  if ((pFh = fopen(pclTmpBuf, "r")) != NULL)
  {
     for (ilCurKey=iCTL_CLASS; ilCurKey<=iCCO_CLASS; ilCurKey++)
     {
        fscanf(pFh, "%ld %ld %ld %ld %ld\n", &rgCDIUnique[ilCurKey].lCurrentNo, &rgCDIUnique[ilCurKey].lLastNo, &rgCDIUnique[ilCurKey].lAvailableNo, &rgCDIUnique[ilCurKey].lMax, &rgCDIUnique[ilCurKey].lMin);
     }
     fclose(pFh);		

     /* delete the .uni file... */
     remove(pclTmpBuf);
  }
  else
     dbg(DEBUG,"<InitCDI> cannot open file: <%s>", pclTmpBuf);

  for (ilCurKey=iCTL_CLASS; ilCurKey<=iCCO_CLASS; ilCurKey++)
  {
     switch (ilCurKey)
     {
        case iCTL_CLASS: 
           dbg(DEBUG,"<InitCDI> following CTL-Unique Number...");
           break;
        case iDAT_CLASS: 
        case iINS_CLASS: 
           dbg(DEBUG,"<InitCDI> following DAT-Unique Number...");
           break;
        case iDEL_CLASS: 
           dbg(DEBUG,"<InitCDI> following DEL-Unique Number...");
           break;
        case iCCO_CLASS: 
           dbg(DEBUG,"<InitCDI> following CCO-Unique Number...");
           break;
        default:
           dbg(TRACE,"<InitCDI> unknown current key!"); 
           return RC_FAIL;
     }

     dbg(DEBUG,"<InitCDI> CurrentUniqueNo: <%ld>, LastUniqueNo: <%ld>, AvailableNo: <%ld>, Max: <%ld>, Min: <%ld>", rgCDIUnique[ilCurKey].lCurrentNo, rgCDIUnique[ilCurKey].lLastNo, rgCDIUnique[ilCurKey].lAvailableNo, rgCDIUnique[ilCurKey].lMax, rgCDIUnique[ilCurKey].lMin);
  }

  /* set pointer to function */
  rgTM.pTCHandler[iCTL_CLASS] = HandleCTLData;
  rgTM.pTCHandler[iDAT_CLASS] = HandleDATData;
  rgTM.pTCHandler[iINS_CLASS] = HandleDATData;
  rgTM.pTCHandler[iDEL_CLASS] = HandleDELData;

  /* set pointer to function */
  rgTM.pCTLHandler[iINVENTORY_CMD] = HandleInventoryCmd;
  rgTM.pCTLHandler[iUPDATE_CMD] = HandleUpdateCmd;
  rgTM.pCTLHandler[iFREE_QUERY_CMD] = HandleFreeQueryCmd;
  rgTM.pCTLHandler[iINTERNAL_CMD] = HandleInternalCmd;

  /* set pointer to function */
  rgTM.pDataHandler[iDAT_CLASS][iNOT_ACTIVE] = BuildDATData;
  rgTM.pDataHandler[iINS_CLASS][iNOT_ACTIVE] = BuildDATData;
  rgTM.pDataHandler[iDEL_CLASS][iNOT_ACTIVE] = BuildDELData;
  rgTM.pDataHandler[iDAT_CLASS][iACTIVE] = BuildDATData;
  rgTM.pDataHandler[iINS_CLASS][iACTIVE] = BuildDATData;
  rgTM.pDataHandler[iDEL_CLASS][iACTIVE] = BuildDELData;

  /* reset flag */
  igCCOCheck = 0;
  igDBLCheck = 0;
  igCDIEventNo = 0;
  igNoENDCommand = 0;

  if (rgTM.iTCPStartSocket < 0)
  {
     /* create socket */
     if ((rgTM.iTCPStartSocket = tcp_create_socket(SOCK_STREAM, rgTM.pcServiceName)) == RC_FAIL)
     {
        dbg(TRACE,"<InitCDI> tcp_create_socket returns: %d", rgTM.iTCPStartSocket);
        return iCDI_TERMINATE;
     }

     dbg(TRACE,"<InitCDI> START SOCKET = %d (FOR LISTEN)",rgTM.iTCPStartSocket);

     /* now listen for connections on a socket */
     if ((ilRC = listen(rgTM.iTCPStartSocket, 5)) == -1)
     {
        dbg(TRACE,"<InitCDI> listen returns %d (%s)", rgTM.iTCPStartSocket, strerror(errno));
        return iCDI_TERMINATE;
     }
  }

  /* get client chars for mapping */
  ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "CLIENTCHARS", CFG_STRING, pclTmpBuf);
  if (ilRC == RC_SUCCESS)
  {
     ilI = 0;
     pclTmpPtr = pclTmpBuf;
     while (pclTmpPtr != NULL)
     {
        strncpy(pclTmpNum,pclTmpPtr,3);
        pclTmpNum[3] = '\0';
        pcgClientChars[ilI] = atoi(pclTmpNum);
        ilI++;
        pclTmpPtr = strstr(pclTmpPtr,",");
        if (pclTmpPtr != NULL)
           pclTmpPtr++;
     }
     pcgClientChars[ilI] = '\0';
  }
  else
     strcpy(pcgClientChars,"\042\047\054\012\015");
  /* get server chars for mapping */
  ilRC = iGetConfigRow(pcpCfgFile, "MAIN", "SERVERCHARS", CFG_STRING, pclTmpBuf);
  if (ilRC == RC_SUCCESS)
  {
     ilI = 0;
     pclTmpPtr = pclTmpBuf;
     while (pclTmpPtr != NULL)
     {
        strncpy(pclTmpNum,pclTmpPtr,3);
        pclTmpNum[3] = '\0';
        pcgServerChars[ilI] = atoi(pclTmpNum);
        ilI++;
        pclTmpPtr = strstr(pclTmpPtr,",");
        if (pclTmpPtr != NULL)
           pclTmpPtr++;
     }
     pcgServerChars[ilI] = '\0';
  }
  else
     strcpy(pcgServerChars,"\027\030\031\034\035");
  dbg(TRACE,"<InitCDI Client Chars:");
  snap(pcgClientChars,strlen(pcgClientChars),outp);
  dbg(TRACE,"<InitCDI Server Chars:");
  snap(pcgServerChars,strlen(pcgServerChars),outp);

    if(ilRC == RC_SUCCESS)
    {
        char clSection[128];
        char clKeyword[64];
            /* read HomeAirPort from SGS.TAB */
            memset((void*)&cgHopo[0], 0x00, 8);

            sprintf(&clSection[0],"SYS");
            sprintf(&clKeyword[0],"HOMEAP");

            ilRC  = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgHopo[0]);
            if (ilRC != RC_SUCCESS)
            {
                    dbg(TRACE, "<InitCDI> EXTAB,%s,%s not found in SGS.TAB",
                            &clSection[0], &clKeyword[0]);
            } else {
                    dbg(TRACE,"<InitCDI> home airport    <%s>",&cgHopo[0]);
            }/* end of if */
    }/* end of if */

/** read TICH (HEB)***********/

    sprintf(pclSqlBuf,
            "SELECT TICH,TDI1,TDI2 FROM APTTAB WHERE APC3 = '%s'",cgHopo);
    ilRC = sql_if(START,&slCursor,pclSqlBuf,pclDataArea);
    if (ilRC != DB_SUCCESS)
    {
            dbg(TRACE,"Home Airport <%s> not found in APTTAB %d",cgHopo,ilRC);
    } /* end while */
    else
    {
            strcpy(cgTdi1,"60");
            strcpy(cgTdi2,"60");
            get_fld(pclDataArea,0,STR,14,cgTich);
            get_fld(pclDataArea,1,STR,14,cgTdi1);
            get_fld(pclDataArea,2,STR,14,cgTdi2);
            TrimRight(cgTich);
            if (*cgTich != '\0')
            {
                    TrimRight(cgTdi1);
                    TrimRight(cgTdi2);
                    sprintf(cgTdi1,"%d",atoi(cgTdi1)*60);
                    sprintf(cgTdi2,"%d",atoi(cgTdi2)*60);
            }
            dbg(TRACE,"Tich: <%s> Tdi1: <%s> Tdi2 <%s>",
                    cgTich,cgTdi1,cgTdi2);
    }
    close_my_cursor(&slCursor);
    slCursor = 0;


  dbg(TRACE,"<InitCDI> =============== END (Re)InitCDI() ===============");
  /* bye bye */
  /*if (ipCalledBy==0)*/
  debug_level = ilDbgSave;
  return RC_SUCCESS;
} 

/******************************************************************************/
/******************************************************************************/
static int CreateSecondQueue(void)
{
  int ilRC = RC_SUCCESS;
  char pclTmpName[32];
  if (igSecondModID > 0)
  {
    dbg(TRACE,"SECOND QUEUE %d ALREADY EXISTS",igSecondModID);
  } /* end if */
  else
  {
    sprintf(pclTmpName,"%sQ2",mod_name);
    /* 20050609 JIM:
    if ((ilRC = GetDynamicQueue(&igSecondModID, pclTmpName)) != RC_SUCCESS)
    */
    if ((ilRC = GetDynamicQueueToMe(&igSecondModID, pclTmpName)) != RC_SUCCESS)
    {
      dbg(TRACE,"CANNOT CREATE SECOND QUEUE");
    } /* end if */
    else
    {
      dbg(TRACE,"SECOND QUEUE %d <%s> CREATED",igSecondModID,pclTmpName);
    } /* end else */
    igItemsOnQ2 = 0;
  } /* end else */


  return ilRC;
} /* end CreateSecondQueue */

/******************************************************************************/
/******************************************************************************/
static int DeleteSecondQueue(void)
{
  int ilRC = RC_SUCCESS;
  if (igSecondModID > 0)
  {
    if (que(QUE_DELETE, igSecondModID, igSecondModID, PRIORITY_3, 0, NULL)
	!= RC_SUCCESS)
    {
      dbg(TRACE,"CANNOT DELETE SECOND QUEUE %d",igSecondModID);
    } /* end if */
    else
    {
      dbg(TRACE,"SECOND QUEUE %d DELETED",igSecondModID);
    } /* end else */
  } /* end else */
  igSecondModID = 0;
  igItemsOnQ2 = 0;
  igActiveQueue = mod_id;
  return ilRC;
} /* end DeleteSecondQueue */

/******************************************************************************/
/******************************************************************************/
static int WriteToSecondQueue(int ipPrio)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilLen = 0;
  if (igSecondModID == 0)
  {
    ilRC = CreateSecondQueue();
  } /* end if */
  if (ilRC == RC_SUCCESS)
  {
    ilLen = sizeof(EVENT) + prgEvent->data_length;
    ilRC = que(QUE_PUT,igSecondModID,mod_id,ipPrio,ilLen,(char *)prgEvent);
    igItemsOnQ2++;
  } /* end if */
  return ilRC;
} /* end WriteToSecondQueue */


/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset(void)
{
	int			i;
	int			ilRC = RC_SUCCESS;				
	int			ilCurCmd;
	int			ilCmdType;
	int			*pilCmdCnt = NULL;
	CDICmd		*prlCmdPtr = NULL;

	dbg(DEBUG,"<Reset> ----- START -----");

	/* is there a version? */
	if (!strlen(rgTM.pcVersion))
	{
		strcpy(rgTM.pcVersion, "200");
		dbg(DEBUG,"<Reset> setting Version to: <%s>",  rgTM.pcVersion);
	}

	/* write current version to CFG-File... */
	(void)iWriteConfigEntry(pcgCfgFile, "MAIN", "version", CFG_STRING, rgTM.pcVersion);

	/* close socket */
	if ((ilRC = CloseTCPConnection()) == iCDI_TERMINATE)
		return iCDI_TERMINATE;

	/* delete memory here */
	for (ilCmdType=iINVENTORY_CMD; ilCmdType<=iINTERNAL_CMD; ilCmdType++)
	{
		switch (ilCmdType)
		{
			case iINVENTORY_CMD:
				dbg(DEBUG,"<Reset> next Inventory Commands...");
				pilCmdCnt = &rgTM.iNoInventoryCmd;
				prlCmdPtr = rgTM.prInventoryCmd;
				break;
			case iUPDATE_CMD:
				dbg(DEBUG,"<Reset> next Update Commands...");
				pilCmdCnt = &rgTM.iNoUpdateCmd;
				prlCmdPtr = rgTM.prUpdateCmd;
				break;
			case iFREE_QUERY_CMD:
				dbg(DEBUG,"<Reset> next Free Query Commands...");
				pilCmdCnt = &rgTM.iNoFreeQueryCmd;
				prlCmdPtr = rgTM.prFreeQueryCmd;
				break;
			case iINCOMING_DATA_CMD:
				dbg(DEBUG,"<Reset> next Incoming Data Commands...");
				pilCmdCnt = &rgTM.iNoIncomingDataCmd;
				prlCmdPtr = rgTM.prIncomingDataCmd;
				break;
			case iINTERNAL_CMD:
				dbg(DEBUG,"<Reset> next Internal Commands...");
				pilCmdCnt = &rgTM.iNoInternalCmd;
				prlCmdPtr = rgTM.prInternalCmd;
				break;
			default:
				dbg(TRACE,"<Reset> unkown command type"); 
				return iCDI_TERMINATE;
		}

		if (prlCmdPtr != NULL)
		{
			for (ilCurCmd=0; ilCurCmd<*pilCmdCnt; ilCurCmd++)
			{
				if (prlCmdPtr[ilCurCmd].pcCmd != NULL)
				{
					dbg(DEBUG,"<Reset> free prlCmdPtr[ilCurCmd].pcCmd");
					free ((void*)prlCmdPtr[ilCurCmd].pcCmd);
					prlCmdPtr[ilCurCmd].pcCmd = NULL;
				}

				if (prlCmdPtr[ilCurCmd].prTabDef != NULL)
				{
					dbg(DEBUG,"<Reset> free prlCmdPtr[ilCurCmd].prTabDef");
					free ((void*)prlCmdPtr[ilCurCmd].prTabDef);
					prlCmdPtr[ilCurCmd].prTabDef = NULL;
				}
			}
				
			dbg(DEBUG,"<Reset> free prlCmdPtr");
			free ((void*)prlCmdPtr);
			prlCmdPtr = NULL;
		}
	}

	/* free pushed events... */
	for (i=0; i<igCDIEventNo; i++)
	{
		/* delete memory here... */
		if (prgCDIEvent[i].pcEvent != NULL)
		{
			dbg(DEBUG,"<Reset> free event: %d", i);
			free((void*)prgCDIEvent[i].pcEvent);
			prgCDIEvent[i].pcEvent = NULL;
		}
	}

	/* delete remaining memory */
	if (prgCDIEvent != NULL)
	{
		dbg(DEBUG,"<Reset> free event pointer");
		free((void*)prgCDIEvent);
		prgCDIEvent = NULL;
	}

	/* free pushed packages... */
	for (i=0; i<igCDITcpPackageNo; i++)
	{
		/* delete memory here... */
		if (prgCDITcpPackage[i].pcPackage != NULL)
		{
			dbg(DEBUG,"<Reset> free package: %d", i);
			free((void*)prgCDITcpPackage[i].pcPackage);
			prgCDITcpPackage[i].pcPackage = NULL;
		}
	}

	/* delete remaining memory */
	if (prgCDITcpPackage != NULL)
	{
		dbg(DEBUG,"<Reset> free package pointer");
		free((void*)prgCDITcpPackage);
		prgCDITcpPackage = NULL;
	}

	/* reset counter */
	igCDIEventNo = 0;
	igCDITcpPackageNo = 0;

	/* delete memory (sending/receiving data) */
	if (pcgTCPReceiveMemory != NULL)
	{
		dbg(DEBUG,"<Reset> free pcgTCPReceiveMemory..."); 
		free ((void*)pcgTCPReceiveMemory);
		pcgTCPReceiveMemory = NULL;
	}

	if (pcgTCPSendMemory != NULL)
	{
		dbg(DEBUG,"<Reset> free pcgTCPSendMemory..."); 
		free ((void*)pcgTCPSendMemory);
		pcgTCPSendMemory = NULL;
	}

	/* to inform the status-handler about startup time */
	if ((ilRC = SendStatus("TCPIP", pcgProcessName, "STOP", 1, "", "", "")) != RC_SUCCESS)
	{
		dbg(TRACE,"<Reset> SendStatus returns: %d",  ilRC);
		dbg(DEBUG,"<Reset> ----- END -----");
		return iCDI_RESET;
	}

	/* new initialization */
	if ((ilRC = InitCDI(pcgCfgFile, pcgProcessName,1)) != RC_SUCCESS)
	{
		dbg(TRACE,"<Reset> =============== END (Re)InitCDI() ===============");
		dbg(TRACE,"<Reset> InitCDI returns: %d", ilRC);
		ilRC = iCDI_TERMINATE;
	}
	
	dbg(DEBUG,"<Reset> ----- END -----");
	return ilRC;
} 

/******************************************************************************/
/* The wait for termination routine                                          */
/******************************************************************************/
static void WaitForTerminate(int ipSleepTime)
{
	debug_level = TRACE;
	dbg(TRACE,"<Terminate> ========== START terminate() CDI2 ==========");
	if (ipSleepTime > 0)
	{
		dbg(TRACE,"<Terminate> waiting <%d> seconds till terminating",ipSleepTime);
		sleep(ipSleepTime);
	}
	Terminate();
	return;
} /* end WaitForTerminate */


/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
void Terminate(void)
{
	int		ilRC;

	/* ignore most signals */
	(void)UnsetSignals();

	/* is there a version? */
	if (!strlen(rgTM.pcVersion))
	{
		strcpy(rgTM.pcVersion, "200");
		dbg(DEBUG,"<Terminate> setting Version to: <%s>",  rgTM.pcVersion);
	}

	/* write current version to CFG-File... */
	(void)iWriteConfigEntry(pcgCfgFile, "MAIN", "version", CFG_STRING, rgTM.pcVersion);

	/* close socket */
	(void)CloseTCPConnection();
	/* shutdown (listen) socket */
	if ((ilRC = shutdown(rgTM.iTCPStartSocket, 2)) != 0)
	{
		dbg(DEBUG,"<Terminate> shutdown returns: %d, <%s>",  ilRC, strerror(errno));
	} 

	/* close (listen) socket */
	if ((ilRC = close(rgTM.iTCPStartSocket)) != 0)
	{
		dbg(DEBUG,"<Terminate> close returns: %d, <%s>",  ilRC, strerror(errno));
	}
	rgTM.iTCPStartSocket = -1;

	/* to inform the status-handler about startup time */
	if ((ilRC = SendStatus("TCPIP", pcgProcessName, "STOP", 1, "", "", "")) != RC_SUCCESS)
	{
		dbg(TRACE,"<Terminate> SendStatus returns: %d",  ilRC);
		dbg(DEBUG,"<Terminate> ----- END -----");
	}

	DeleteSecondQueue();

	dbg(DEBUG,"<Terminate> ----- END -----");
	if (igRestartTimeout > 0)
	{
		dbg(TRACE,"<Terminate> waiting %d seconds till terminating",  igRestartTimeout);
		sleep(igRestartTimeout);
	}
	dbg(TRACE,"<Terminate> now leaving ...");
	exit(0);
} 

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int ipSig)
{
	int	ilRC;

	if (ipSig != SIGALRM && ipSig != SIGCLD)
		dbg(TRACE,"<HandleSignal> signal <%d> received",  ipSig);

	switch (ipSig)
	{
		case SIGALRM:
		case SIGCLD:
			/* don't call SetSignals here */
			igSignalNo = ipSig;
			break;

		case SIGPIPE:
			/* to inform the status-handler about a broke pipe */
			if ((ilRC = SendStatus("TCPIP", pcgProcessName, "WARNING", 1, "got signal SIGPIPE (broken pipe)", "", "")) != RC_SUCCESS)
			{
				dbg(TRACE,"<HandleSignal> SendStatus returns: %d",  ilRC);
			}
			WaitForTerminate(igRestartTimeout);
			break;

		default:
			exit(0);
			break;
	} 

	/* bye bye */
	return;
} 

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr) 
	{
		case	QUE_E_FUNC:	/* Unknown function */
			dbg(TRACE,"<%d> : unknown function",pipErr);
			break;
		case	QUE_E_MEMORY:	/* Malloc reports no memory */
			dbg(TRACE,"<%d> : malloc failed",pipErr);
			break;
		case	QUE_E_SEND:	/* Error using msgsnd */
			dbg(TRACE,"<%d> : msgsnd failed",pipErr);
			break;
		case	QUE_E_GET:	/* Error using msgrcv */
			dbg(TRACE,"<%d> : msgrcv failed",pipErr);
			break;
		case	QUE_E_EXISTS:
			dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
			break;
		case	QUE_E_NOFIND:
			dbg(TRACE,"<%d> : route not found ",pipErr);
			break;
		case	QUE_E_ACKUNEX:
			dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
			break;
		case	QUE_E_STATUS:
			dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
			break;
		case	QUE_E_INACTIVE:
			dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
			break;
		case	QUE_E_MISACK:
			dbg(TRACE,"<%d> : missing ack ",pipErr);
			break;
		case	QUE_E_NOQUEUES:
			dbg(TRACE,"<%d> : queue does not exist",pipErr);
			break;
		case	QUE_E_RESP:	/* No response on CREATE */
			dbg(TRACE,"<%d> : no response on create",pipErr);
			break;
		case	QUE_E_FULL:
			dbg(TRACE,"<%d> : too many route destinations",pipErr);
			break;
		case	QUE_E_NOMSG:	/* No message on queue */
			/*
			i don't like this message, normaly i use QUE_GETBIGNW, so this message
			appeares every n seconds in the logfile, not necessary... 

			dbg(TRACE,"<%d> : no messages on queue",pipErr);
			*/
			break;
		case	QUE_E_INVORG:	/* Mod id by que call is 0 */
			dbg(TRACE,"<%d> : invalid originator=0",pipErr);
			break;
		case	QUE_E_NOINIT:	/* Queues is not initialized*/
			dbg(TRACE,"<%d> : queues are not initialized",pipErr);
			break;
		case	QUE_E_ITOBIG:
			dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
			break;
		case	QUE_E_BUFSIZ:
			dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
			break;
		case	QUE_E_PRIORITY:
			dbg(TRACE,"<%d> : unknown priority ",pipErr);
			break;
		default:	/* Unknown queue error */
			dbg(TRACE,"<%d> : unknown error",pipErr);
			break;
	} /* end switch */
         
	return;
} 

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues(void)
{
	int	ilRC = RC_SUCCESS;			
	int	ilBreakOut = FALSE;
	dbg(DEBUG,"<HandleQueues> ----- START -----");

	do
	{
		/* get item */
		ilRC = que(QUE_GETBIG, 0, mod_id, PRIORITY_3, 0, (char*)&prgItem);

		/* set event pointer */ 
		prgEvent = (EVENT*)prgItem->text;	

		if (ilRC == RC_SUCCESS)
		{
			ilRC = que(QUE_ACK, 0, mod_id, 0, 0, NULL);
			if (ilRC != RC_SUCCESS) 
			{
				HandleQueErr(ilRC);
			} 
		
			switch (prgEvent->command)
			{
				case	HSB_STANDBY:
					ctrl_sta = prgEvent->command;
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					break;	
		
				case	HSB_COMING_UP:
					ctrl_sta = prgEvent->command;
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					break;	
		
				case	HSB_ACTIVE:
					ctrl_sta = prgEvent->command;
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					ilBreakOut = TRUE;
					break;	

				case	HSB_ACT_TO_SBY:
					ctrl_sta = prgEvent->command;
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					break;	
		
				case	HSB_DOWN:
					ctrl_sta = prgEvent->command;
					WaitForTerminate(0);
					break;	
		
				case	HSB_STANDALONE:
					ctrl_sta = prgEvent->command;
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					ilBreakOut = TRUE;
					break;	

				case	SHUTDOWN:
					WaitForTerminate(0);
					break;
							
				case	RESET:
					if ((ilRC = Reset()) == iCDI_TERMINATE)
						WaitForTerminate(0);
					break;
							
				case	EVENT_DATA:
					dbg(TRACE,"HandleQueues: wrong hsb status <%d>", ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;
						
				case	TRACE_ON:
					dbg_handle_debug(prgEvent->command);
					break;

				case	TRACE_OFF:
					dbg_handle_debug(prgEvent->command);
					break;

				case 	MORE_DEBUG:
					if (igDebugLevel == OFF)
					{
						dbg(DEBUG,"<HandleQueues> setting MORE_DEBUG to DEBUG");
						igDebugLevel = DEBUG;
					}
					else if (igDebugLevel == DEBUG)
					{
						dbg(DEBUG,"<HandleQueues> setting MORE_DEBUG to TRACE");
						igDebugLevel = TRACE;
					}
					else if (igDebugLevel == TRACE)
					{
						dbg(DEBUG,"<HandleQueues> setting MORE_DEBUG to OFF");
						igDebugLevel = OFF;
					}
					break;

				default:
					dbg(TRACE,"HandleQueues: unknown event");
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;
			} 
		} 
		else 
		{
			HandleQueErr(ilRC);
		} 
	} while (ilBreakOut == FALSE);

	dbg(DEBUG,"<HandleQueues> ----- END -----");
	return;
} 
	
static int UNIQUE(int a, int lpMin, int lpMax )
{
   dbg(TRACE,"Unique Number before UNIQUE %ld",a);
   if (a >= 99999)
   {
      a = 1;
   }
   dbg(TRACE,"Unique Number after UNIQUE %ld",a);
   return a;
}


/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(void)
{
	int			i;
	int			ilRC 				= RC_SUCCESS;	
	int			ilFTP_RC;	
	int			ilCmdNo;
	int			ilTabNo;	
	int			ilCmdType;
	int			ilCurRecord;
	int			ilCurTable;
	int			ilAllRecords;
	int			ilNoInvalidRecords;
	long			llMin;
	long			llMax;
	long			llUniqueNumber;
        char pclUniqueNumber[32] = " ";
	time_t		CTCPQTime1;
	time_t		CTCPQTime2;
	BC_HEAD		*prlBCHead 		= NULL;
	CMDBLK		*prlCmdblk 		= NULL;
	char			*pclSelection 	= NULL;
	char			*pclFields		= NULL;
	char			*pclData			= NULL;
	CDICmd		*prlCmdPtr 		= NULL;
	CDITable	*prlTabPtr		= NULL;
	char			pclDataBuffer[iMAXIMUM];
	char			*pclTmpDataPtr	= NULL;
	char			pclLocalUrno[iMIN];
	char 			pclFileName[2*iMIN_BUF_SIZE];
	char 			pclDataType[iMIN];
	char		pclIlgUrno[16];
	char		pclNewFields[1024];
  char *pclErrorCode = NULL;
  int ilDataReceived;
  int ilI;
  int ilBytes;

	dbg(TRACE,"<HandleData> ------------------------- START -------------------------");
	dbg(TRACE,"<HandleData> CEDA EVENT FROM %d (USING QUEUE %d)",
			    prgEvent->originator,igActiveQueue);

	/* set BCHead pointer */
	prlBCHead = (BC_HEAD*)((char*)prgEvent+sizeof(EVENT));
	if (prlBCHead->rc == RC_FAIL)
	{
		/* this is an error */
		dbg(DEBUG,"<HandleData> BC_Head->rc: %d", prlBCHead->rc);
		dbg(TRACE,"<HandleData> ERROR-MESSAGE: <%s>", prlBCHead->data);

		/* set local pointer */
		prlCmdblk 		= (CMDBLK*)(prlBCHead->data + strlen(prlBCHead->data) + 1);
		pclSelection 	= prlCmdblk->data;
		pclFields 		= pclSelection + strlen(pclSelection) + 1;
		pclData 			= pclFields + strlen(pclFields) + 1;
		*pclData			= 0x00;
	}
	else
	{
		/* set local pointer */
		prlCmdblk 		= (CMDBLK*)prlBCHead->data;
		pclSelection 	= prlCmdblk->data;
		pclFields 		= pclSelection + strlen(pclSelection) + 1;
		pclData 			= pclFields + strlen(pclFields) + 1;
	}

  /*igDebugLevel*/
	dbg(TRACE,"<HandleData> SPECIAL INFO TWS <%s> TWE <%s>",
		   prlCmdblk->tw_start,prlCmdblk->tw_end);
	dbg(TRACE,"%05d:<HandleData> Command:   <%s>", __LINE__,prlCmdblk->command);
	dbg(TRACE,"%05d:<HandleData> Table:     <%s>", __LINE__,prlCmdblk->obj_name);
	dbg(TRACE,"%05d:<HandleData> Selection: <%s>", __LINE__,pclSelection);
	dbg(TRACE,"%05d:<HandleData> Fields:    <%s>", __LINE__,pclFields);
	dbg(DEBUG,"%05d:<HandleData> Data:      <%s>", __LINE__,pclData);
	
	/* search CDI-Internal Command, Command-Type and table number */
	GetCDIInternalCmd(prlCmdblk->command, prlCmdblk->obj_name, pclSelection, pclFields, &ilCmdType, &ilCmdNo, &ilTabNo);

	/* only a message */
	dbg(TRACE,"<HandleData> CMD_TYPE: <%s>, CMD_NO: %d, TAB_NO: %d", CMD_TYPE(ilCmdType), ilCmdNo, ilTabNo);

	/* is there a valid cmd-no, tab-no and cmd-type? */
	if (ilCmdType > -1 && ilCmdNo > -1 && ilTabNo > -1)
	{
		sprintf(pcgTwStart,"%d,%d,%d",ilCmdType,ilCmdNo,ilTabNo);
		switch (ilCmdType)
		{
			case iINVENTORY_CMD:
				dbg(TRACE,"<HandleData> GOT INVENTORY RESULT DATA FROM CEDA");
				igRcvInvCmd = TRUE;
				dbg(TRACE,"<HandleData> WAITING FOR START UPDATES");
				prlCmdPtr = &rgTM.prInventoryCmd[ilCmdNo];
				break;
			case iUPDATE_CMD:
				if ((igGotUpdCmd == FALSE) && (igSndUpdCmd == FALSE))
				{
					dbg(TRACE,"<HandleData> SEND UPDATES NOT YET ACTIVATED!");
					if ((igSndInvCmd == TRUE) || (igRcvInvCmd == TRUE))
					{
						dbg(TRACE,"<HandleData> SEND UPDATES TO Q2");
						WriteToSecondQueue(PRIORITY_4);
					} /* end if */
					else
					{
						dbg(TRACE,"<HandleData> STORE UPDATES IN ILGTAB");
						prlCmdPtr = &rgTM.prUpdateCmd[ilCmdNo];
						prlTabPtr = &prlCmdPtr->prTabDef[ilTabNo];
						WriteToLogTab(prlCmdblk->command,prlCmdblk->obj_name,
							      pclSelection,pclFields,pclData,prlTabPtr," ",
                                                              pclIlgUrno," ");
					}
	dbg(TRACE,"<HandleData> ------------------------- END -------------------------");
					return RC_SUCCESS;
				} /* end if */
				prlCmdPtr = &rgTM.prUpdateCmd[ilCmdNo];
				break;
			case iFREE_QUERY_CMD:
				prlCmdPtr = &rgTM.prFreeQueryCmd[ilCmdNo];
				break;
			case iINCOMING_DATA_CMD:
				prlCmdPtr = &rgTM.prIncomingDataCmd[ilCmdNo];
				break;
			default:
				dbg(TRACE,"<HandleData> unkown command type received! Ignoring!"); 
	dbg(TRACE,"<HandleData> ------------------------- END -------------------------");
				return iCDI_TERMINATE;
		}

		/* set pointer to current table definition */
		prlTabPtr = &prlCmdPtr->prTabDef[ilTabNo];

		/* increment internal counter */
		if (igUpdateService == iNOT_ACTIVE)
			prlCmdPtr->iNoReceivedTables++;

		/* should i save this item? */
		if (prlCmdPtr->iNextTableNo != ilTabNo && igUpdateService == iNOT_ACTIVE)
		{
			dbg(DEBUG,"<HandleData> STORE TABLE-ITEM[%d]", ilTabNo);

			/* must store this item temporary */
			prlCmdPtr->rCDISave[ilTabNo].iItemIsStored = TRUE;
			prlCmdPtr->rCDISave[ilTabNo].iCmdNo 		 = ilCmdNo;
			prlCmdPtr->rCDISave[ilTabNo].iTabNo 		 = ilTabNo;
			prlCmdPtr->rCDISave[ilTabNo].iCmdType 		 = ilCmdType;
			prlCmdPtr->rCDISave[ilTabNo].pcSelection 	 = strdup(pclSelection);
			prlCmdPtr->rCDISave[ilTabNo].pcFields 		 = strdup(pclFields);
			prlCmdPtr->rCDISave[ilTabNo].pcData 		 = strdup(pclData);
			prlCmdPtr->rCDISave[ilTabNo].prCDITable 	 = prlTabPtr;

			/* check pointer... */
			if (prlCmdPtr->rCDISave[ilTabNo].pcSelection	== NULL ||  
				 prlCmdPtr->rCDISave[ilTabNo].pcFields		== NULL || 
				 prlCmdPtr->rCDISave[ilTabNo].pcData 		== NULL)
			{
				dbg(TRACE,"<HandleData> MALLOC ERROR (STORE ITEM)...");
	dbg(TRACE,"<HandleData> ------------------------- END -------------------------");
				return iCDI_TERMINATE;
			}
		}
		else
		{
			/* send/write all possible tables */
			for (ilCurTable = (igUpdateService == iNOT_ACTIVE ? prlCmdPtr->iNextTableNo : ilTabNo); ilCurTable < (igUpdateService == iNOT_ACTIVE ? prlCmdPtr->iNoReceivedTables : ilTabNo+1); ilCurTable++)
			{
				if (prlCmdPtr->iNextTableNo != ilTabNo && prlCmdPtr->rCDISave[ilCurTable].iItemIsStored && igUpdateService == iNOT_ACTIVE)
				{
					/* set everything we need... */
					ilCmdNo 			= prlCmdPtr->rCDISave[ilCurTable].iCmdNo;
					ilTabNo 			= prlCmdPtr->rCDISave[ilCurTable].iTabNo;
					ilCmdType 		= prlCmdPtr->rCDISave[ilCurTable].iCmdType;
					pclSelection 	= prlCmdPtr->rCDISave[ilCurTable].pcSelection;
					pclFields 		= prlCmdPtr->rCDISave[ilCurTable].pcFields;
					pclData 			= prlCmdPtr->rCDISave[ilCurTable].pcData;
					prlTabPtr 		= prlCmdPtr->rCDISave[ilCurTable].prCDITable;
				}

				/* only for a valid index... */
				if (ilCurTable == ilTabNo)
				{
					/* count records */
					ilAllRecords = GetNoOfElements(pclData, 0x0A);
					dbg(DEBUG,"<HandleData> FOUND %d RECORDS...", ilAllRecords);

					/* check time frame for each record... */
					ilNoInvalidRecords = CheckTimeFrame(ilAllRecords, pclData, pclFields, prlTabPtr);
					dbg(DEBUG,"<HandleData> FOUND %d INVALID RECORDS...", ilNoInvalidRecords);

					/* are there records? */ 
					if ((ilAllRecords-ilNoInvalidRecords) > 0)
					{
						if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
						{
							/* get next unique numbers */
							if ((ilRC = HandleUniqueNumber(rgTM.pcServerName, prlTabPtr->pcTelegramKey, "MAXOW", ilAllRecords-ilNoInvalidRecords, &llUniqueNumber, &llMax, &llMin)) != RC_SUCCESS)
							{
								dbg(TRACE,"<HandleData> HandleUniqueNumber returns: %d", ilRC);
	dbg(TRACE,"<HandleData> ------------------------- END -------------------------");
								return iCDI_TERMINATE;
							}
						}
						else
						{
							/* wrong HSB-STATUS..., returning with reset here... */
							dbg(TRACE,"<HandleData> wrong CTRL-STA: <%s>", CTRL_STA(ctrl_sta));
	dbg(TRACE,"<HandleData> ------------------------- END -------------------------");
							return iCDI_RESET;
						}
					}

					/* clear send buffer */
					memset((void*)pcgTCPSendMemory, 0x00, iMAX_DATA);

					/* set tmp-pointer to data-area */
					pclTmpDataPtr = pclData;

					/* set new time... */
					CTCPQTime1 = time(NULL);

					/* for all records */
					for (ilCurRecord=0; ilCurRecord<ilAllRecords; ilCurRecord++, llUniqueNumber++)
					{
						/* test TCP/IP-connection, that's necessary if we received
							CCOx while handling the data 
						*/

						/* set new time... */
						CTCPQTime2 = time(NULL);

						/* look each 1/2 seconds at tcp-port... */
						if (difftime(CTCPQTime2, CTCPQTime1) > (double)0.5)
						{
							if ((ilRC = CheckTCPQuickly()) != RC_SUCCESS)
							{
								/* delete temporary memeory (storing items) */
								for (i=0; i<prlCmdPtr->iNoReceivedTables; i++)
								{
									if (prlCmdPtr->rCDISave[i].iItemIsStored)
									{
										dbg(DEBUG,"<HandleData> free stored item: %d", i);
										free(prlCmdPtr->rCDISave[i].pcSelection);
										free(prlCmdPtr->rCDISave[i].pcFields);
										free(prlCmdPtr->rCDISave[i].pcData);
										memset(&prlCmdPtr->rCDISave[i], 0x00, sizeof(CDISave));
									}
								}

								/* bye bye */
	dbg(TRACE,"<HandleData> ------------------------- END -------------------------");
								return ilRC;
							}

							/* set new time stamp */
							CTCPQTime1 = CTCPQTime2;
						}

						/* set next unique number */
						llUniqueNumber = UNIQUE(llUniqueNumber, llMax, llMin);
					
						/* get n.th record */
						pclDataBuffer[0] = 0x00;
						pclTmpDataPtr = CDICopyNextField(pclTmpDataPtr, 0x0A, pclDataBuffer, prlTabPtr->iEmptyFieldHandling);

						/* is this a valid record? */
						if (pclDataBuffer[0] != 0x01)
						{
							/* delete CR */
							if (pclDataBuffer[strlen(pclDataBuffer)-1] == 0x0D)
								pclDataBuffer[strlen(pclDataBuffer)-1] = 0x00;

							/* get Urno also */
							if ((ilRC = GetIndex(igUpdateService == iNOT_ACTIVE ? prlTabPtr->pcInternalFieldList : pclFields, "URNO", cCOMMA)) >= 0)
							{
								/* here we know position of VIAN */
								pclLocalUrno[0] = 0x00;
								strcpy(pclLocalUrno, GetDataField(pclDataBuffer, ilRC, cCOMMA));
							}

							/* acknowledge handling */
							ilRC = WriteSendFlag(pclFields,pclDataBuffer,
								 	     prlTabPtr,pclSelection);
                                                        sprintf(pclUniqueNumber,"%d",llUniqueNumber);
							WriteToLogTab(prlCmdblk->command,prlCmdblk->obj_name,
								      pclSelection,pclFields,pclDataBuffer,
							      	      prlTabPtr,"Y",pclIlgUrno,pclUniqueNumber);
							if (strcmp(pclIlgUrno,"NO") != 0)
							{
								sprintf(pclNewFields,"%s,LURN",pclFields);
								strcat(pclDataBuffer,",");
								strcat(pclDataBuffer,pclIlgUrno);
							}
							if (strcmp(pclIlgUrno,"NO") != 0 && igUpdateService == iNOT_ACTIVE)
							{
     ilDataReceived = FALSE;
     for (ilI = 0; ilI < 10 && ilDataReceived == FALSE; ilI++)
     {
dbg(TRACE,"AKL Wait (%d) 25 ms",ilI+1);
        nap(25);
        if ((ilBytes = ReceiveTcpIp(rgTM.lTCPTimeout, rgTM.iTimeUnit, iPULL_TCP_ALLOWED)) > 0)
        {
           /* write data to protocol file */
           ToRProtFile(ilBytes);
           ResetCCOInterval();
           if ((ilRC = CheckReceivedHeader(&pclErrorCode)) == RC_SUCCESS)
           {
              if ((ilRC = HandleReceivedData()) == RC_SUCCESS)
              {
                 ilDataReceived = TRUE;
              }
           }
        }
     }
                                                        }

							/* prepare the telegram */
							PrepareTelegram(llUniqueNumber, pclLocalUrno, prlTabPtr);
								
							/* produce the data we want to send... */
							/* FUNCTIONS: BuildDATData, BuildDELData, 
							*/
							if ((ilRC = (*rgTM.pDataHandler[prlTabPtr->iTelegramType][igUpdateService])(pclNewFields, pclDataBuffer, prlTabPtr)) != RC_SUCCESS)
							{
								dbg(TRACE,"<HandleData> DataHandler returns: <%s>",RETURN(ilRC));
	dbg(TRACE,"<HandleData> ------------------------- END -------------------------");
								return ilRC;
							}

							/* wait x milliseconds */
							nap(rgTM.lNapTime);

							/* write telegram or produce the file, 
								depending on command
								FUNCTIONS: WriteTelegramToClient, BuildFile 
							*/
							if (strcmp(pclIlgUrno,"0") != 0)
							{
								if ((ilRC = (*prlCmdPtr->pTelegramHandler)((struct _CDICmd*)prlCmdPtr)) != RC_SUCCESS)
								{
									dbg(TRACE,"<HandleData> TelegramHandler returns: <%s>",RETURN(ilRC));
	dbg(TRACE,"<HandleData> ------------------------- END -------------------------");
									return ilRC;
								}
							}
						}
						else
						{
							dbg(DEBUG,"<HandleData> found invalid records: <%s>", pclDataBuffer);
						}
					}

					/* reset global flag, necessary for FreeQueryCmds */
					prlTabPtr->iNoCurExternalFieldList = 0;

					/* this is unnecessary for updates */  
					if (igUpdateService == iNOT_ACTIVE)
					{
						/* is this last table? */
						if (ilCurTable == prlCmdPtr->iNoTable-1)
						{
							dbg(TRACE,"<HandleData> RECEIVED LAST EXPECTED RESULT FILE");
							/* - yes - so send file, write EOD or do nothing */ 
							if ((ilRC = SendFile((struct _CDICmd*)prlCmdPtr, prlTabPtr)) != RC_SUCCESS)
							{
								dbg(TRACE,"<HandleData> SendFile returns: <%s>",RETURN(ilRC));
	dbg(TRACE,"<HandleData> ------------------------- END -------------------------");
								return ilRC;
							}

							/* delete temporary memeory (storing items) */
							for (i=0; i<prlCmdPtr->iNoReceivedTables; i++)
							{
								if (prlCmdPtr->rCDISave[i].iItemIsStored)
								{
									dbg(DEBUG,"<HandleData> free stored item: %d", i);
									free(prlCmdPtr->rCDISave[i].pcSelection);
									free(prlCmdPtr->rCDISave[i].pcFields);
									free(prlCmdPtr->rCDISave[i].pcData);
									memset(&prlCmdPtr->rCDISave[i], 0x00, sizeof(CDISave));
								}
							}

							/* reset internal counter */
							prlCmdPtr->iNoReceivedTables = 0;
							prlCmdPtr->iNextTableNo = 0;
						}
						else
						{
							/* increment counter */
							prlCmdPtr->iNextTableNo++;
							dbg(DEBUG,"<HandleData> now NextTableNo: %d", prlCmdPtr->iNextTableNo);
						}
					}
					else
					{
						/* must call SendFile for an update! */
						if ((ilRC = SendFile((struct _CDICmd*)prlCmdPtr, prlTabPtr)) != RC_SUCCESS)
						{
							dbg(TRACE,"<HandleData> SendFile returns: <%s>",RETURN(ilRC));
	dbg(TRACE,"<HandleData> ------------------------- END -------------------------");
							return ilRC;
						}
					}
				}
			}
		}
	}
	else
	{
		if (strncmp(prlCmdblk->tw_start,"WEOD,",5) == 0)
		{
			if (strncmp(pclSelection,"FAIL,",5) == 0)
			{
				ilFTP_RC = RC_FAIL;
			} 
			else
			{
				ilFTP_RC = RC_SUCCESS;
			}
			GetDataItem(pclFileName,prlCmdblk->tw_start,2,',',"","");
			GetDataItem(pclDataType,prlCmdblk->tw_start,3,',',"","");
			dbg(TRACE,"<HandleData> GOT ANSWER WEOD=<%d> FOR FILE<%s> DATATYPE<%s>",ilFTP_RC, pclFileName,pclDataType);

			/* writing FINA telegram to client if configured */
			if (rgTM.iWriteFINATelegram == 1)
			{
				if ((ilRC = WriteFinaTelegram(ilFTP_RC,pclFileName,pclDataType))!=RC_SUCCESS)
				{
					dbg(TRACE,"<HandleData> WriteFinaTelegram returns: <%d>", ilRC);
	dbg(TRACE,"<HandleData> ------------------------- END -------------------------");
					return iCDI_RESET;
				}
				dbg(DEBUG,"<HandleData> WriteFinaTelegram returns: <%d>", ilRC);
			}

			/* write an EOD-Command to client */
			if ((ilRC = WriteEODToClient()) != RC_SUCCESS)
			{
				dbg(TRACE,"<HandleData> WriteEODToClient returns: <%s>", RETURN(ilRC));
	dbg(TRACE,"<HandleData> ------------------------- END -------------------------");
				return iCDI_RESET;
			}
			dbg(TRACE,"<HandleData> WriteEODToClient returns: %d", ilRC);
			dbg(TRACE,"<HandleData> WAITING FOR START UPDATES");
			igRcvInvCmd = TRUE;
		} 
		else
		{
			dbg(TRACE,"<HandleData> can't find CommandType or CommandNo or TableNo");
			/* return iCDI_RESET; */
		} /* end else */
	}
	
	dbg(TRACE,"<HandleData> ------------------------- END -------------------------");
	return RC_SUCCESS;
}

/******************************************************************************/
/* The GetCDIInternalCmd routine                                              */
/******************************************************************************/
static void GetCDIInternalCmd(char *pcpCommand, char *pcpTable, 
									   char *pcpSelection, char *pcpFields, 
										int *pipCmdType, int *pipCmdNo, int *pipTabNo)
{
	int			i;
	int			ilCnt;
	int			ilCurCmd;
	int			ilCurTab;
	int			ilCmdType;
	int			*pilCmdCnt = NULL;
	CDICmd		*prlCmdPtr = NULL;

	/* search it */
	for (i=0; i<4; i++)
	{
		/* initialize something... */
		*pipCmdType = *pipCmdNo = *pipTabNo = -1;
		ilCnt = 0;

		for (ilCmdType=iINVENTORY_CMD; ilCmdType<=iFREE_QUERY_CMD; ilCmdType++)
		{
			switch (ilCmdType)
			{
				case iINVENTORY_CMD:
					pilCmdCnt = &rgTM.iNoInventoryCmd;
					prlCmdPtr = rgTM.prInventoryCmd;
					break;
				case iUPDATE_CMD:
					pilCmdCnt = &rgTM.iNoUpdateCmd;
					prlCmdPtr = rgTM.prUpdateCmd;
					break;
				case iFREE_QUERY_CMD:
					pilCmdCnt = &rgTM.iNoFreeQueryCmd;
					prlCmdPtr = rgTM.prFreeQueryCmd;
					break;
				case iINCOMING_DATA_CMD:
					pilCmdCnt = &rgTM.iNoIncomingDataCmd;
					prlCmdPtr = rgTM.prIncomingDataCmd;
					break;
				default:
					dbg(TRACE,"<GCDIIC> unknown command type"); 
					return;
			}

			if (prlCmdPtr != NULL)
			{
				for (ilCurCmd=0; ilCurCmd<*pilCmdCnt; ilCurCmd++)
				{
					for (ilCurTab=0; ilCurTab<prlCmdPtr[ilCurCmd].iNoTable; ilCurTab++)
					{
						switch (i)
						{
							case 0:
								if (!strcmp(prlCmdPtr[ilCurCmd].prTabDef[ilCurTab].pcSndCmd, pcpCommand))
								{
									*pipCmdType = ilCmdType;
									*pipCmdNo = ilCurCmd;
									*pipTabNo = ilCurTab;
									ilCnt++;
								}
								break;

							case 1:
								if (!strcmp(prlCmdPtr[ilCurCmd].prTabDef[ilCurTab].pcSndCmd, pcpCommand) && !strcmp(prlCmdPtr[ilCurCmd].prTabDef[ilCurTab].pcTableName, pcpTable))
								{
									*pipCmdType = ilCmdType;
									*pipCmdNo = ilCurCmd;
									*pipTabNo = ilCurTab;
									ilCnt++;
								}
								break;

							case 2:
								if (!strcmp(prlCmdPtr[ilCurCmd].prTabDef[ilCurTab].pcSndCmd, pcpCommand) && !strcmp(prlCmdPtr[ilCurCmd].prTabDef[ilCurTab].pcTableName, pcpTable) && !strcmp(prlCmdPtr[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList, pcpFields))
								{
									*pipCmdType = ilCmdType;
									*pipCmdNo = ilCurCmd;
									*pipTabNo = ilCurTab;
									ilCnt++;
								}
								break;

							case 3:
								if (!strcmp(prlCmdPtr[ilCurCmd].prTabDef[ilCurTab].pcSndCmd, pcpCommand) && !strcmp(prlCmdPtr[ilCurCmd].prTabDef[ilCurTab].pcTableName, pcpTable) && !strcmp(prlCmdPtr[ilCurCmd].prTabDef[ilCurTab].pcCurrentSelection, pcpSelection) && !strcmp(prlCmdPtr[ilCurCmd].prTabDef[ilCurTab].pcInternalFieldList, pcpFields))
								{
									*pipCmdType = ilCmdType;
									*pipCmdNo = ilCurCmd;
									*pipTabNo = ilCurTab;
									ilCnt++;
								}
								break;
						}
						dbg(igDebugLevel,"<GCDIIC> cnt: %d", ilCnt);
					}
				}
			}
		}
		dbg(igDebugLevel,"<GCDIIC> CNT: %d", ilCnt);
		/* found exactly one command? */
		if (ilCnt == 1)
			return;
	}

	/* test counter */
	if (ilCnt != 1)
		*pipCmdType = *pipCmdNo = *pipTabNo = -1;

	/* that's all */
	return;
}

/******************************************************************************/
/* The CDI2Server routine                                                     */
/******************************************************************************/
static int CDI2Server(void)
{
  int ilRC;
  int ilBytes;
  char *pclErrorCode = NULL;

  /* now create the TCP/IP-Connection */
  if ((ilRC = CreateTCPConnection()) != RC_SUCCESS)
  {
     /* back to main */
     return ilRC;
  }

  if (rgTM.iMake_Connect == TRUE)
  {
     if ((ilRC = SendClientCmd()) != RC_SUCCESS)
     {
        return ilRC;
     }
  }

	/* initialize time */
	time1 = time(NULL);

	/* forever */
	dbg(DEBUG,"=================================================\n");
	while (1)
	{
		/*----------------------------------------------------------------
		look for TCP-data
		----------------------------------------------------------------*/
		if ((ilBytes = ReceiveTcpIp(rgTM.lTCPTimeout, rgTM.iTimeUnit, iPULL_TCP_ALLOWED)) > 0)
		{
			/* write data to protocol file */
			ToRProtFile(ilBytes);

			if ((ilRC = CheckReceivedHeader(&pclErrorCode)) != RC_SUCCESS)
			{
				/* back */

				/* in this case send an ERR-Command to client... */
				/* ...and reset or terminate the server */
				if ((ilRC = WriteERRToClient(pclErrorCode)) == iCDI_TERMINATE)
					return iCDI_TERMINATE;
				else
					return iCDI_RESET;
			}

	  ResetCCOInterval();

			/* only CheckReceivedHeader returns with success */
			if (ilRC == RC_SUCCESS)
			{
				/* handle received command */
				if ((ilRC = HandleReceivedData()) != RC_SUCCESS)
				{
					dbg(TRACE,"<CDI2Server> HRD returns: <%s>", RETURN(ilRC));

					/* back */
					return ilRC;
				}
			}
		}
		else if (ilBytes < 0)
		{
			/* back */
			return iCDI_RESET;
		}
		else
		{
			/* we haven't received data via TCP/IP */
			if (igCCOCheck)
			{
	      /* current time */
	      time2 = time(NULL);

	      /* is difftime to much as period between check and answer? */
	      if ((double)rgTM.iCheckResponse < difftime(time2, time1))
		{
				/* if we use CCO-Check -> this must be an timeout error */
				dbg(DEBUG,"<CDI2Server> timeout error");

				/* in this case send an ERR-Command to client... */
				strcpy(pcgTCPReceiveMemory,"GOT READ TIMEOUT");
				ToRProtFile(-1); 
				if ((ilRC = WriteERRToClient(sTIMEOUT)) == iCDI_TERMINATE)
					return iCDI_TERMINATE;
				else
					return iCDI_RESET;
			}
		}
	}

		/*----------------------------------------------------------------
		check que-status
		----------------------------------------------------------------*/
		if ((ilRC = CheckQueStatus(igActiveQueue)) != RC_SUCCESS)
		{
			return ilRC;
		}

		/*----------------------------------------------------------------
		check connection-status (watchdog) CCO-Command
		----------------------------------------------------------------*/
		if (rgTM.iCheckConnection)
		{
			if ((ilRC = CheckConnection(iCCO_CHECK)) < 0)
			{
				/* return from CDIServer */
				return iCDI_RESET;
	    } /* end of if*/
	} /* end of if*/
    } /* end of while */
}

/******************************************************************************/
/* The SetIntValue routine                                                    */
/******************************************************************************/
static void SetIntValue(char *pcpName, int *pipValue, char *pcpIn, 
			char *pcpCmp1, char *pcpCmp2, int ipW1, int ipW2, int ipDefault)
{
	if (!strcmp(pcpIn, pcpCmp1))
	{
		*pipValue = ipW1;
		dbg(DEBUG,"<SetIntValue> SetIntValue() setting <%s> to: <%s>", pcpName, pcpCmp1);
	}
	else if (!strcmp(pcpIn, pcpCmp2))
	{
		*pipValue = ipW2;
		dbg(DEBUG,"<SetIntValue> SetIntValue() setting <%s> to: <%s>",pcpName, pcpCmp2);
	}
	else
	{
		*pipValue = ipDefault;
		dbg(DEBUG,"<SetIntValue> SetIntValue() setting <%s> to (default): <%d>",pcpName, ipDefault);
	}
	return;
}

/******************************************************************************/
/* The GetViaByNumber routine                                                 */
/******************************************************************************/
static char *GetViaByNumber(char *pcpVial, int ipVian, int ipViaNo, int ipVia34)
{
	int			i;
	int			ilCnt;
	char			*pclVialPtr;
	static char	pclVia[iMIN];

	if (pcpVial == NULL || !strlen(pcpVial) || ipViaNo < 1)
		return NULL;

	/* missing VIAN */
	if (ipVian <= 0)
	{
		/* so calculate VIAN */
		ilCnt = 0;
		pclVialPtr = pcpVial;
		if (strlen(pclVialPtr))
			ilCnt++;
		while (strlen(pclVialPtr) > 120)
		{
			ilCnt++;
			pclVialPtr += 120;
		}
		ipVian = ilCnt;
		if (ipVian <= 0)	
			return NULL;
	}

	memset((void*)pclVia, 0x00, iMIN);
	for (i=0; i<ipVian; i++, pcpVial+=120)
		if (((*pcpVial)-0x30) == ipViaNo)
			if (ipVia34 == 3)
				strncpy(pclVia, pcpVial+1, 3);
			else
				strncpy(pclVia, pcpVial+4, 4);

	return strlen(pclVia) ? pclVia : NULL;
}

/******************************************************************************/
/* The ReceiveTcpIp routine                                                   */
/* the following funktion return the number of bytes received via				   */
/* TCP/IP - and sends an acknowledge if necessary 										*/
/******************************************************************************/
static int ReceiveTcpIp(long lpTimeout, int ipTimeUnit, int ipPullTCPPackage)
{
	int					ilI;
	int					ilRC;
	int					ilCnt;
	int					ilBytes;
	int					ilFirst;
	int					ilHaveToRead;
	int					ilLen;
	int					ilVersion;
	char					pclReceiveBuf[iMAX_DATA];
	char					pclReceiveCryptBuf[iMAX_DATA];
	char					pclTmpBuf[iMIN_BUF_SIZE];
	char					*pclS = NULL;
char *pclTmpPtr;

	/* bytecounter */
	ilI = 0;
	ilBytes = 0;

	/* clear memory */
	memset((void*)pcgTCPReceiveMemory, 0x00, iMAX_DATA);
	rgTM.iTCNumber = -1;

	errno = 0;
	/* look for data */
	if ((ilRC = TcpWaitTimeout(rgTM.iTCPWorkSocket, lpTimeout, ipTimeUnit)) == RC_SUCCESS)
	{
		/* for length */
		ilFirst = 1;

		/* for security only, read first two bytes... */
		if (rgTM.iSecureOption)
		{
			/* Return code */
			ilRC = 0;

			/* clear temporary buffer */
			memset((void*)pclReceiveBuf, 0x00, iMAX_DATA);
			memset((void*)pclReceiveCryptBuf, 0x00, iMAX_DATA);

			/* if we use secure option, we must read first two bytes... */
			ilCnt = 0;
	  	do {
				errno = 0;
				alarm(1);
	    /*   ilRC = read(rgTM.iTCPWorkSocket, pclReceiveBuf, 2); */
	    ilRC = recv(rgTM.iTCPWorkSocket, pclReceiveBuf, 2,0);
				alarm(0);

				if (++ilCnt >= 10)
				{
					dbg(igDebugLevel,"<RTCPIP> read returns: %d -> %s", ilRC, strerror(errno));
					/* don't send ENDCommand to WorkSocket, because pipe is broken */
					igNoENDCommand = 1;

					/* this shows function above the error */
					return iCDI_RESET;
				}
			} while (ilRC <= 0);

			/* only for debuging */
			memcpy(pclReceiveCryptBuf, pclReceiveBuf, 2);

			/* iOFFSET_IS_UNKNOWN = 0, this means that the function must calculate 
			the offset
			*/
			if (igDebugLevel == DEBUG)
				snap(pclReceiveBuf, 2, outp);
			pclS = SDecode(pclReceiveBuf, 2, iOFFSET_IS_UNKNOWN);
			if (igDebugLevel == DEBUG)
				dbg(DEBUG,"<RTCPIP> OFFSET: <%s>", pclS);
		}

		/* number of bytes we have to read */
		ilLen = ilHaveToRead = HEADER_LENGTH(rgTM.iVersion);
		dbg(igDebugLevel, "<RTCPIP> Have To Read: %d bytes", ilHaveToRead);

		/* data to receive */
		do
		{
			/* Return code */
			ilRC = 0;

			/* clear temporary buffer */
			memset((void*)pclReceiveBuf, 0x00, iMAX_DATA);

			/* read the data */
			ilCnt = 0;
			do
			{
				alarm(1);
				ilRC = read(rgTM.iTCPWorkSocket, pclReceiveBuf, ilLen);
				alarm(0);

				if (++ilCnt >= 10)
				{
					dbg(igDebugLevel,"<RTCPIP> read returns: %d -> %s", ilRC, strerror(errno));
					/* don't send ENDCommand to WorkSocket, because pipe is broken */
					igNoENDCommand = 1;

					/* this shows function above the error */
					return iCDI_RESET;
				}
			} while (ilRC <= 0);

			if (rgTM.iSecureOption)
			{
				/* only for debuging */
				memcpy(&pclReceiveCryptBuf[ilBytes+2], pclReceiveBuf, 2);

				/* encode received data... */
				if (igDebugLevel == DEBUG)
					snap(pclReceiveBuf, ilRC, outp);
				memcpy(pclReceiveBuf, SDecode(pclReceiveBuf, ilRC, iOFFSET_IS_KNOWN), ilRC); 
				if (igDebugLevel == DEBUG)
					snap(pclReceiveBuf, ilRC, outp);
			}
			else
			{
				if (igDebugLevel == DEBUG)
					snap(pclReceiveBuf, ilRC, outp);
			}

			/* set new temporary byte counter */
			ilLen -= ilRC;

			/* copy form temporary to static buffer */
			memmove(pcgTCPReceiveMemory+ilBytes, pclReceiveBuf, ilRC);

			/* increment Byte-Counter */
			ilBytes += ilRC;
			dbg(igDebugLevel, "<RTCPIP> read: %d bytes", ilBytes);
			
			/* could we read length? */
			if (ilBytes >= HEADER_LENGTH(rgTM.iVersion) && ilFirst)
			{
				/* never first */
				ilFirst = 0;

				/* check version... */
				if ((ilVersion = CheckVersion()) != iNOT_CHANGED)
				{
					switch (ilVersion)
					{
						case iCDI_RESET:
							return iCDI_RESET;

						case iTWO_2_ONE:
							/* header is 3 bytes less than version 2.xx */ 
							ilI = -3;
							break;

						case iONE_2_TWO:
							/* header is 3 bytes more than version 1.xx */ 
							ilI = 3;
							break;

						default:
							return iCDI_RESET;
					}
				}

				/* we know the position of the following number of bytes */
				/* get number of following bytes */
				memset(pclTmpBuf, 0x00, iMIN_BUF_SIZE);
				memmove(pclTmpBuf, pcgTCPReceiveMemory+HEADER_LENGTH(rgTM.iVersion)-4, 4);
				sscanf(pclTmpBuf, "%d", &ilRC);
				dbg(DEBUG,"<RTCPIP> found length in header: <%d>", ilRC);

				/* set new (read) counter */
				ilHaveToRead += (ilRC + ilI);
				dbg(igDebugLevel, "<RTCPIP> Have To Read: %d bytes", ilHaveToRead);

				/* set new len */
				ilLen = ilRC;

				/* check the memory */
				if (ilHaveToRead > iMAX_DATA)
				{
					/* try to allocate more memory */
					if ((pcgTCPReceiveMemory = (char*)realloc(pcgTCPReceiveMemory, ilHaveToRead*sizeof(char))) == NULL)
					{
						/* cannot get enough memory to run -> exit now */
						dbg(TRACE,"<RTCPIP> cannot realloc enough momory (need %d bytes)",  ilHaveToRead);
						return iCDI_TERMINATE;
					}
				}
			}
		} while (ilBytes < ilHaveToRead);
	}
	else if (ilRC == RC_NOT_FOUND)
	{
		/* is a stored tcp-package available?, yes so restore it */
		if (igCDITcpPackageNo && ipPullTCPPackage == iPULL_TCP_ALLOWED)
		{
			if ((ilBytes = PullTCPPackage()) < RC_SUCCESS)
			{
				/* ReceiveTcpIp returns ilBytes below!! */
				/* so, return here with an errorcode isn't necessary */
				dbg(TRACE,"<RTCPIP> PullTCPPackage returns: <%s>", RETURN(ilBytes));
			}
			dbg(DEBUG,"<RTCPIP> PullTCPPackage restores %d bytes",  ilBytes);
		}
		else
			ilBytes = 0;
	}
	else
	{
		/* normally ilRC = RC_FAIL... */
		/*dbg(DEBUG,"<RTCPIP> No RC_SUCCESS on TcpWaitTimeout! Error <%d>=<%s>",  errno,strerror(errno));
		if (errno==EINTR)
			ilBytes = 0;*/

			ilBytes = RC_FAIL;
	}
if (ilBytes > 0)
{
dbg(TRACE,"AKL TCP %d Received %d",++igTCPInCounter,ilBytes);
snap(pcgTCPReceiveMemory,ilBytes,outp);
pclTmpPtr = strstr(pclReceiveBuf,"LURN#");
if (pclTmpPtr != NULL)
dbg(TRACE,"AKL LURN = %d <%s>",igTCPInCounter,pclTmpPtr);
}

	/* only for debuging */
	if (rgTM.iSecureOption)
	{
		if (rgTM.iRCProtFile != -1 && ilBytes > 0)
		{
			if ((ilRC = write(rgTM.iRCProtFile, pclReceiveCryptBuf, ilBytes+2)) != ilBytes+2)
			{
				dbg(TRACE, "<RTCPIP> can't write received data to file (%d)", ilRC);
			}
			else
			{
				/* write \n */
				write (rgTM.iRCProtFile, "\n", 1);
			}
		}
	}

	/* und tschuess */
	return ilBytes;
}

/******************************************************************************/
/* The CheckQueStatus routine                                                 */
/******************************************************************************/
static int CheckQueStatus(int ipUseModID)
{
  int ilRC = RC_SUCCESS;
  int ilRCQ = RC_SUCCESS;
  int ilLoop = TRUE;
  char pclTestBuf[4000];
  CDIIncomingData *prlCDIIncomingData;

  while (ilLoop == TRUE)
  {
     /* without waiting... */
     ilRCQ = que(QUE_GETBIGNW, 0, ipUseModID, PRIORITY_3, 0, (char*)&prgItem);

     /* set global pointer */
     prgEvent = (EVENT*)prgItem->text;

     if (ilRCQ == RC_SUCCESS)
     {
        /* ack. the que */
        if ((ilRCQ = que(QUE_ACK, 0, ipUseModID, 0, 0, NULL)) != RC_SUCCESS)
        {
           HandleQueErr(ilRC);
        }

        switch (prgEvent->command)
        {
           case HSB_STANDBY:
           case HSB_COMMING_UP:
              ctrl_sta = prgEvent->command;
              send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
              ilRC = iCDI_RESET;
              break;

           case HSB_ACTIVE:
           case HSB_ACT_TO_SBY:
           case HSB_STANDALONE:
              ctrl_sta = prgEvent->command;
              send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
              break;

           case TRACE_ON:
           case TRACE_OFF:
              dbg_handle_debug(prgEvent->command);
              break;

           case HSB_DOWN:
              ctrl_sta = prgEvent->command;
              ilRC = iCDI_TERMINATE;
              break;	

           case SHUTDOWN:
              ilRC = iCDI_TERMINATE;
              break;

           case RESET:
              ilRC = iCDI_RESET;
              break;

           case EVENT_DATA:
              if (ipUseModID == igSecondModID)
              {
                 igItemsOnQ2--;
                 if (igItemsOnQ2 == 0)
                 {
                    DeleteSecondQueue();
                    igActiveQueue = mod_id;
                 }
              } /* end if */
              if (rgTM.iTCPWorkSocket >= 0)
              {
                 /* only for a valid socket... */
                 /* first, check sys_ID */
                 if (!strcmp(prgEvent->sys_ID, "STOP"))
                 {
                    /* in this case all db-queues (e.g. sqlhdl, flight...) 
                    are stopped, so we cannot read unique numbers from DB */
                    if (igDBQueuesStopped == FALSE)
                    {
                       dbg(DEBUG,"<CheckQueStatus> calling HandlDBStop...");
                       igDBQueuesStopped = TRUE;
                       if ((ilRC = HandleDBStop()) != RC_SUCCESS)
                       {
                          dbg(TRACE,"<CheckQueStatus> HandleDBStop returns with: <%s>",  RETURN(ilRC));
                          igDBQueuesStopped = FALSE;
                       }
                    }
                 }
                 else
                 {
                    if (igDBQueuesStopped == TRUE)
                    {
                       if (!strcmp(prgEvent->sys_ID, "START"))
                       {
                          dbg(DEBUG,"<CheckQueStatus> calling PullEvent...");
                          igDBQueuesStopped = FALSE;
                          ilRC = PullEvent();
                       }
                       else
                       {
                          dbg(DEBUG,"<CheckQueStatus> calling PushEvent...");
                          ilRC = PushEvent();
                       }
                    }
                    else
                    {
                       ilRC = HandleData();
                    }
                 }
              }
              else
              {
                 dbg(DEBUG,"EVENT DATA: SOCKET CONNECTION NOT ACTIVE");
              } /* end else */
              break;

           case MORE_DEBUG:
              if (igDebugLevel == OFF)
              {
                 dbg(DEBUG,"<HandleQueues> setting MORE_DEBUG to DEBUG");
                 igDebugLevel = DEBUG;
              }
              else if (igDebugLevel == DEBUG)
              {
                 dbg(DEBUG,"<HandleQueues> setting MORE_DEBUG to TRACE");
                 igDebugLevel = TRACE;
              }
              else if (igDebugLevel == TRACE)
              {
                 dbg(DEBUG,"<HandleQueues> setting MORE_DEBUG to OFF");
                 igDebugLevel = OFF;
              }
              break;

           case 776:
              dbg(TRACE,"TEST == TEST == TEST == TEST");
              dbg(TRACE,"SECOND QUEUE IS ON HOLD");
              igHoldQ2 = TRUE;
              break;

           case 777:
              ilRC = ReadFromLogTab();
              igGotUpdCmd = TRUE;
              if (igSecondModID > 0)
              {
                 dbg(TRACE,"SWITCHED TO SECOND QUEUE %d",igSecondModID);
                 dbg(TRACE,"(STORED %d UPDATE EVENTS)",igItemsOnQ2);
                 igActiveQueue = igSecondModID;
              } /* end if */
              igUpdateService = iACTIVE;
              dbg(TRACE,"START SENDING UPDATES");
              break;

           case 778:
              igSndInvCmd = TRUE;
              break;

           case 779:
              ilRC = HandleInventoryCmd(0);
              break;

           case 780:
              strcpy(pclTestBuf,"ABA");
              strcat(pclTestBuf,"20090730140700");
              strcat(pclTestBuf,"RGF");
              strcat(pclTestBuf,"FL");
              strcat(pclTestBuf,"0000000000000000000000000");
              strcat(pclTestBuf,"#LURN#1359#");
              prlCDIIncomingData = (CDIIncomingData *)pclTestBuf;
              HandleAcknowledge(prlCDIIncomingData);
              break;

           case 781:
              strcpy(pclTestBuf,"ABA");
              strcat(pclTestBuf,"20090730140700");
              strcat(pclTestBuf,"RGF");
              strcat(pclTestBuf,"FL");
              strcat(pclTestBuf,"0000000000000000000000000");
              strcat(pclTestBuf,"#LURN#1360#LURN#1361#LURN#1362#LURN#1363#LURN#1364#LURN#1365#LURN#1366#");
              prlCDIIncomingData = (CDIIncomingData *)pclTestBuf;
              HandleAcknowledge(prlCDIIncomingData);
              break;

           default:
              DebugPrintItem(TRACE, prgItem);
              DebugPrintEvent(TRACE, prgEvent);
              break;
        }

        ilLoop = FALSE;
        if ((rgTM.iTCPWorkSocket < 0) && (ilRC == RC_SUCCESS))
        { /* 20050607 JIM: found event and no connection: check next event immediate: */
           ilLoop = TRUE;
        }
     }
     else
     {
        /* error getting data from que */
        if (ilRCQ == QUE_E_NOMSG)
        {
#if 0
           if (ipUseModID == igSecondModID)
           {
              DeleteSecondQueue();
              igActiveQueue = mod_id;
           } /* end if */
           ilRCQ = RC_SUCCESS;
#endif
        } /* end if */
        else
           HandleQueErr(ilRC);
     ilLoop = FALSE;
     }

  } /* end while */

  /* return successfully */
  return ilRC;
}

/******************************************************************************/
/* The CheckVersion routine                                                   */
/******************************************************************************/
static int CheckVersion(void)
{
	CDI1Header	*prlCDI1Header 	= NULL;
	CDI2Header	*prlCDI2Header 	= NULL;
	int			ilVersionChanged 	= iNOT_CHANGED;
	char pclTmpBuf_jwe[100];

	/* set pointer */
	prlCDI2Header = (CDI2Header*)pcgTCPReceiveMemory;
	prlCDI1Header = (CDI1Header*)pcgTCPReceiveMemory;

        if (igDebugLevel == DEBUG)
        	snap(pcgTCPReceiveMemory,128,outp);
	memset(pclTmpBuf_jwe,0x00,100);
	strncat(pclTmpBuf_jwe,prlCDI2Header->pcTeleClass,3);
	strncat(pclTmpBuf_jwe,",",1);
	strncat(pclTmpBuf_jwe,prlCDI2Header->pcSender,10);
	strncat(pclTmpBuf_jwe,",",1);
	strncat(pclTmpBuf_jwe,prlCDI2Header->pcReceiver,10);
	strncat(pclTmpBuf_jwe,",",1);
	strncat(pclTmpBuf_jwe,prlCDI2Header->pcVersion,3);
	dbg(TRACE,"<CheckVersion> CLASS,SND,RCV,VER");
	dbg(TRACE,"<CheckVersion> Buffer = <%s>",pclTmpBuf_jwe);

	if (rgTM.iVersion >= 200)
	{
		/* this seems to be version 2.xx */
		/* first check TelegramClass */
		if (memcmp(prlCDI2Header->pcTeleClass, "CTL", 3) &&
			 memcmp(prlCDI2Header->pcTeleClass, "DAT", 3) &&
			 memcmp(prlCDI2Header->pcTeleClass, "DEL", 3))
		{
			/* error, try CDI1Header... */
			if (memcmp(prlCDI1Header->pcTeleClass, "CTL", 3) &&
				 memcmp(prlCDI1Header->pcTeleClass, "DAT", 3) &&
				 memcmp(prlCDI1Header->pcTeleClass, "DEL", 3))
			{
				/* unknown VERSION, massiv error */
				dbg(TRACE,"<CheckVersion> unknown version or unknown class...");

				/* send an error message to client system */
				if (WriteERRToClient(sUNKNOWN_CLASS) == iCDI_TERMINATE)
					return iCDI_TERMINATE;
				else
					return iCDI_RESET;
			}
			else
			{
				/* client is version type 1.xx */
				/* set Version-String and Int-Value */
				dbg(DEBUG,"<CheckVersion> changing version from %d to 100", rgTM.iVersion);
				strcpy(rgTM.pcVersion, "100");
				rgTM.iVersion = 100;
				ilVersionChanged = iTWO_2_ONE;

				if (!memcmp(prlCDI1Header->pcTeleClass, "CTL", 3))
					rgTM.iTCNumber = iCTL_CLASS;
				else if (!memcmp(prlCDI1Header->pcTeleClass, "DAT", 3))
					rgTM.iTCNumber = iDAT_CLASS;
				else if (!memcmp(prlCDI1Header->pcTeleClass, "INS", 3))
					rgTM.iTCNumber = iINS_CLASS;
				else if (!memcmp(prlCDI1Header->pcTeleClass, "DEL", 3))
					rgTM.iTCNumber = iDEL_CLASS;
			}
		}
		else
		{
			/* client is version 2.xx */
			/* set current telegram-class yet */
			if (!memcmp(prlCDI2Header->pcTeleClass, "CTL", 3))
				rgTM.iTCNumber = iCTL_CLASS;
			else if (!memcmp(prlCDI2Header->pcTeleClass, "DAT", 3))
				rgTM.iTCNumber = iDAT_CLASS;
			else if (!memcmp(prlCDI2Header->pcTeleClass, "INS", 3))
				rgTM.iTCNumber = iINS_CLASS;
			else if (!memcmp(prlCDI2Header->pcTeleClass, "DEL", 3))
				rgTM.iTCNumber = iDEL_CLASS;
		}
	}
	else
	{
		/* this seems to be version 1.xx */
		/* first check TelegramClass */
		if (memcmp(prlCDI1Header->pcTeleClass, "CTL", 3) &&
			 memcmp(prlCDI1Header->pcTeleClass, "DAT", 3) &&
			 memcmp(prlCDI1Header->pcTeleClass, "DEL", 3))
		{
			/* error, try CDI2Header... */
			if (memcmp(prlCDI2Header->pcTeleClass, "CTL", 3) &&
				 memcmp(prlCDI2Header->pcTeleClass, "DAT", 3) &&
				 memcmp(prlCDI2Header->pcTeleClass, "DEL", 3))
			{
				/* unknown VERSION, massiv error */
				dbg(TRACE,"<CheckVersion> unknown version or unknown class...");

				/* send an error message to client system */
				if (WriteERRToClient(sUNKNOWN_CLASS) == iCDI_TERMINATE)
					return iCDI_TERMINATE;
				else
					return iCDI_RESET;
			}
			else
			{
				/* client is version type 2.xx */
				/* set Version-String and Int-Value */
				dbg(DEBUG,"<CheckVersion> changing version from %d to 200", rgTM.iVersion);
				strcpy(rgTM.pcVersion, "200");
				rgTM.iVersion = 200;
				ilVersionChanged = iONE_2_TWO;

				if (!memcmp(prlCDI2Header->pcTeleClass, "CTL", 3))
					rgTM.iTCNumber = iCTL_CLASS;
				else if (!memcmp(prlCDI2Header->pcTeleClass, "DAT", 3))
					rgTM.iTCNumber = iDAT_CLASS;
				else if (!memcmp(prlCDI2Header->pcTeleClass, "INS", 3))
					rgTM.iTCNumber = iINS_CLASS;
				else if (!memcmp(prlCDI2Header->pcTeleClass, "DEL", 3))
					rgTM.iTCNumber = iDEL_CLASS;
			}
		}
		else
		{
			/* client is version 1.xx */
			if (!memcmp(prlCDI1Header->pcTeleClass, "CTL", 3))
				rgTM.iTCNumber = iCTL_CLASS;
			else if (!memcmp(prlCDI1Header->pcTeleClass, "DAT", 3))
				rgTM.iTCNumber = iDAT_CLASS;
			else if (!memcmp(prlCDI1Header->pcTeleClass, "INS", 3))
				rgTM.iTCNumber = iINS_CLASS;
			else if (!memcmp(prlCDI1Header->pcTeleClass, "DEL", 3))
				rgTM.iTCNumber = iDEL_CLASS;
		}
	}

	/* bye bye */
	return ilVersionChanged;
}

/******************************************************************************/
/* The CheckReceivedHeader routine                                            */
/******************************************************************************/
static int CheckReceivedHeader(char **pcpErrorCode)
{
	CDI1Header	*prlCDI1Header = NULL;
	CDI2Header	*prlCDI2Header = NULL;
	char			pclTimeStamp[15];
	char			pclTmpBuf1[iMIN];
	static char	pclTmpBuf[iMIN];
	char			pclServer[iMIN];
	char			pclSender[iMIN];
	char			pclReceiver[iMIN];

	/* initialize it */
	pclTmpBuf[0]  = 0x00;
	pclTmpBuf1[0] = 0x00;
	*pcpErrorCode = pclTmpBuf;

	memset(pclServer,0x00,iMIN);
	memset(pclSender,0x00,iMIN);
	memset(pclReceiver,0x00,iMIN);

	/* set pointer to memory */
	prlCDI2Header = (CDI2Header*)pcgTCPReceiveMemory;
	prlCDI1Header = (CDI1Header*)pcgTCPReceiveMemory;

	dbg(DEBUG,"<CheckReceivedHeader> Using Version: <%s>", rgTM.pcVersion);

	/* check serversystem... */
	if (rgTM.iVersion >= 200)
	{
		strncpy(pclServer,prlCDI2Header->pcServerSystem,3);
		str_trm_all(pclServer," ",TRUE);
		/*if (!memcmp(prlCDI2Header->pcServerSystem, rgTM.pcServerSystem, 3))*/
		if (strcmp(pclServer, rgTM.pcServerSystem)==0)
		{
			strncpy(pclSender,prlCDI2Header->pcSender,10);
			str_trm_all(pclSender," ",TRUE);
			/*if (!memcmp(prlCDI2Header->pcSender, rgTM.pcReceiver, 10))*/
			if (strcmp(pclSender, rgTM.pcReceiver)==0)
			{
				strncpy(pclReceiver,prlCDI2Header->pcReceiver,10);
				str_trm_all(pclReceiver," ",TRUE);
				/*if (!memcmp(prlCDI2Header->pcReceiver, rgTM.pcSender, 10))*/
				if (strcmp(pclReceiver, rgTM.pcSender)==0)
				{
					/* check timestamp */
					memset((void*)pclTimeStamp, 0x00, 15);
					memcpy(pclTimeStamp, prlCDI2Header->pcTimeStamp, 14);
					dbg(DEBUG,"<CheckReceivedHeader> Timestamp: <%s>", pclTimeStamp);
					if ((strcmp(pclTimeStamp, "19900101000000") < 0) ||
						 (strcmp(pclTimeStamp, "21001231595959") > 0))
					{
						/* wrong timestamp */	
						strcpy(pclTmpBuf, sWRONG_TIMESTAMP);
						dbg(TRACE,"<CheckReceivedHeader> wrong timestamp...");
						return iCDI_RESET;
					}
					else
					{
						/* check length */
						memset((void*)pclTmpBuf1, 0x00, iMIN);
						memcpy(pclTmpBuf1, prlCDI2Header->pcLength, 4);
						if (atoi(pclTmpBuf1) < 3)
						{
							/* length failure... */
							strcpy(pclTmpBuf, sWRONG_LENGTH);
							dbg(TRACE,"<CheckReceivedHeader> wrong length...");
							return iCDI_RESET;
						}
						else
						{
							/* check number of telegram */
							memset((void*)pclTmpBuf1, 0x00, iMIN);
							memcpy(pclTmpBuf1, prlCDI2Header->pcTeleNo, 4);
							if (atoi(pclTmpBuf1) < 0)
							{
								/* length failure... */
								strcpy(pclTmpBuf, sWRONG_NUMBER);
								dbg(TRACE,"<CheckReceivedHeader> wrong number...");
								return iCDI_RESET;
							}
						}
					}
				}
				else
				{
					/* wrong Receiver... */
					strcpy(pclTmpBuf, sUNKNOWN_RECEIVER);
					dbg(TRACE,"<CheckReceivedHeader> unknown receiver H=<%s> CFG=<%s>",pclReceiver,rgTM.pcSender);
					return iCDI_RESET;
				}
			}
			else
			{
				/* wrong Sender... */
				strcpy(pclTmpBuf, sUNKNOWN_SENDER);
				dbg(TRACE,"<CheckReceivedHeader> unknown sender H=<%s> CFG=<%s>",pclSender,rgTM.pcReceiver);
				return iCDI_RESET;
			}
		}
		else
		{
			/* wrong serversystem... */
			strcpy(pclTmpBuf, sWRONG_SERVERSYSTEM);
			dbg(TRACE,"<CheckReceivedHeader> wrong serversystem H=<%s> CFG=<%s>",pclServer,rgTM.pcServerSystem);
			return iCDI_RESET;
		}
	}
	else
	{
		if (!memcmp(prlCDI1Header->pcServerSystem, rgTM.pcServerSystem, 3))
		{
			if (!memcmp(prlCDI1Header->pcSender, rgTM.pcReceiver, 10))
			{
				if (!memcmp(prlCDI1Header->pcReceiver, rgTM.pcSender, 10))
				{
					/* check timestamp */
					memset((void*)pclTimeStamp, 0x00, 15);
					memcpy(pclTimeStamp, prlCDI1Header->pcTimeStamp, 14);
					dbg(DEBUG,"<CheckReceivedHeader> Timestamp: <%s>", pclTimeStamp);
					if ((strcmp(pclTimeStamp, "19900101000000") < 0) ||
						 (strcmp(pclTimeStamp, "21001231595959") > 0))
					{
						/* wrong timestamp */	
						strcpy(pclTmpBuf, sWRONG_TIMESTAMP);
						dbg(TRACE,"<CheckReceivedHeader> wrong timestamp...");
						return iCDI_RESET;
					}
					else
					{
						/* check length */
						memset((void*)pclTmpBuf1, 0x00, iMIN);
						memcpy(pclTmpBuf1, prlCDI1Header->pcLength, 4);
						if (atoi(pclTmpBuf1) < 3)
						{
							/* length failure... */
							strcpy(pclTmpBuf, sWRONG_LENGTH);
							dbg(TRACE,"<CheckReceivedHeader> wrong length...");
							return iCDI_RESET;
						}
						else
						{
							/* check number of telegram */
							memset((void*)pclTmpBuf1, 0x00, iMIN);
							memcpy(pclTmpBuf1, prlCDI1Header->pcTeleNo, 4);
							if (atoi(pclTmpBuf1) < 0)
							{
								/* length failure... */
								strcpy(pclTmpBuf, sWRONG_NUMBER);
								dbg(TRACE,"<CheckReceivedHeader> wrong number...");
								return iCDI_RESET;
							}
						}
					}
				}
				else
				{
					/* wrong Receiver... */
					strcpy(pclTmpBuf, sUNKNOWN_RECEIVER);
					dbg(TRACE,"<CheckReceivedHeader> unknown receiver...");
					return iCDI_RESET;
				}
			}
			else
			{
				/* wrong Sender... */
				strcpy(pclTmpBuf, sUNKNOWN_SENDER);
				dbg(TRACE,"<CheckReceivedHeader> unknown sender...");
				return iCDI_RESET;
			}
		}
		else
		{
			/* wrong serversystem... */
			strcpy(pclTmpBuf, sWRONG_SERVERSYSTEM);
			dbg(TRACE,"<CheckReceivedHeader> wrong serversystem...");
			return iCDI_RESET;
		}
	}

	/* bye bye */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The ToSCProtFile routine                                                   */
/******************************************************************************/
static void ToSCProtFile(int ipLen)
{
	int		ilRC;
	if (rgTM.iSCProtFile != -1)
	{
		if ((ilRC = write(rgTM.iSCProtFile, pcgTCPSendMemory, ipLen)) != ipLen)
		{
			dbg(TRACE, "<ToSCPF> can't write send data to file (%d)", ilRC);
		}
		else
		{
			/* write \n */
			write (rgTM.iSCProtFile, "\n", 1);
		}
	}
	return;
}

/******************************************************************************/
/* The ToRProtFile routine                                                    */
/******************************************************************************/
static void ToRProtFile(int ipLen)
{
	int		ilRC;
	char		pclTimeStamp[iMIN];

	if (rgTM.iRProtFile != -1)
	{
		if (ipLen < 0)
		{
			ipLen = strlen(pcgTCPReceiveMemory);
		} /* end if */
		pclTimeStamp[0] = 0x00;
		GetTime(pclTimeStamp);
		strcat(pclTimeStamp, " RCV - ");
		write(rgTM.iRProtFile, pclTimeStamp, strlen(pclTimeStamp));
		if ((ilRC = write(rgTM.iRProtFile, pcgTCPReceiveMemory, ipLen)) != ipLen)
		{
			dbg(TRACE, "<ToRPF> can't write received data to file (%d)", ilRC);
		}
		else
		{
			/* write \n */
			write (rgTM.iRProtFile, "\n", 1);
		}
	}
	return;
}

/******************************************************************************/
/* The ToSProtFile routine                                                    */
/******************************************************************************/
static void ToSProtFile(int ipLen)
{
	int		ilCnt=0;
	int		ilRC;
	char		pclTimeStamp[iMIN];

  if (rgTM.iSProtFile != -1) {
    if (ipLen < 0){
			ipLen = strlen(pcgTCPSendMemory);
    } 

    if(rgTM.iAddLFatEnd == 1){
      pcgTCPSendMemory[ipLen-1]= '\\';
      pcgTCPSendMemory[ipLen]= 'n';
      ipLen++;
    }

		pclTimeStamp[0] = 0x00;
		GetTime(pclTimeStamp);
		strcat(pclTimeStamp, " SND - ");
		write(rgTM.iSProtFile, pclTimeStamp, strlen(pclTimeStamp));
    if ((ilRC = write(rgTM.iSProtFile, pcgTCPSendMemory, ipLen)) != ipLen) {
			dbg(TRACE, "<ToSPF> can't write send data to file (%d)", ilRC);
    } else {
			/* write \n */
			write (rgTM.iSProtFile, "\n", 1);
		}
	}
	return;
}

/******************************************************************************/
/* The CreateTCPConnection routine                                            */
/******************************************************************************/
static int CreateTCPConnection(void)
{

#ifdef _SNI
	UINT								ilLen;
#else
	int								ilLen;
#endif

	int								ilRC;
	int								ilFlg;
	char								pclTmpBuf[iMIN_BUF_SIZE]; 
	struct in_addr					rlIn;
	static struct sockaddr_in	rlClient;

	DeleteSecondQueue();
	dbg(TRACE,"<CTCPIPC> CONNECTION IS CLOSED");
	igSndInvCmd = FALSE;
	igRcvInvCmd = FALSE;
	igGotUpdCmd = FALSE;
	igClientErr = FALSE;
	dbg(TRACE,"<CTCPIPC> STOPPED SENDING UPDATES");
	rgTM.iTCPWorkSocket = -1;
 
  /* if FALSE act as server and wait for connection */
  if(rgTM.iMake_Connect == FALSE){

	strcpy(pcgTCPSendMemory,"WAITING FOR CONNECTION");
	ToSProtFile(-1); 
	strcpy(pcgTCPReceiveMemory,"WAITING FOR CONNECTION");
	ToRProtFile(-1); 
	dbg(TRACE,"<CTCPIPC> START SOCKET = %d (FOR ACCEPT CONNECTION)",rgTM.iTCPStartSocket);

	do
	{
		/* set flag... */
		ilFlg = 0;

		ilLen = sizeof(rlClient);

		/*set alarm*/
		/*alarm(2);*/ /* now using select() inside TcpWaitTimeout function*/

		ilRC = TcpWaitTimeout(rgTM.iTCPStartSocket,rgTM.lTCPTimeout,rgTM.iTimeUnit);
		if (ilRC != RC_SUCCESS)
		{
			/*dbg(DEBUG,"<CTCPIPC> TcpWaitTimeout(select) failed! Nobody wants to connect :-((");*/
		}
		else
		{
			/* accept the TCP/IP-Connection */
			/* accept this */
			/*alarm(2);*/
			#ifdef _UNIXWARE
				rgTM.iTCPWorkSocket = accept(rgTM.iTCPStartSocket, (struct sockaddr*)&rlClient, (size_t*)&ilLen);
			#else
				rgTM.iTCPWorkSocket = accept(rgTM.iTCPStartSocket, (struct sockaddr*)&rlClient, &ilLen);
			#endif
			/* reset alarm */
			/*alarm(0);*/
		}

		/* is this a valid socket? */
		if (rgTM.iTCPWorkSocket >= 0)
		{
			/* ...and set flag */
			ilFlg = 1;

			/* should we use dynamic ftp-name?...*/
			strcpy(pclTmpBuf, rgTM.pcMachine);
			StringUPR((UCHAR*)pclTmpBuf);
			if (!strcmp(pclTmpBuf, "SEARCH_DYNAMIC"))
			{
				/*rlIn.s_addr = ntohl(rlClient.sin_addr.s_addr);*/
				if (rgTM.iReverseIp == 1)
					rlIn.s_addr = ntohl(rlClient.sin_addr.s_addr);
				else
					rlIn.s_addr = rlClient.sin_addr.s_addr;

				strcpy(rgTM.pcMachine, inet_ntoa(rlIn));
				dbg(DEBUG,"<CTCPIPC> machine is: <%s>", rgTM.pcMachine);
			}
		}
		else
		{
			/* Check Que now */
			if ((ilRC = CheckQueStatus(igActiveQueue)) != RC_SUCCESS)
			{
				return ilRC;
			}
		}
	} while (!ilFlg);
	dbg(TRACE,"<CTCPIPC> WORK  SOCKET = %d (ACCEPTED)",rgTM.iTCPWorkSocket);

  } else {
    /* act as "client" connect to remote host*/
  /* create socket */
  if ((rgTM.iTCPWorkSocket = tcp_create_socket(SOCK_STREAM,NULL)) == RC_FAIL)
    {
      dbg(TRACE,"<CTCPIPC> tcp_create_socket returns: %d", rgTM.iTCPWorkSocket);
      dbg(DEBUG,"<CTCPIPC> ----- END -----");
      return iCDI_TERMINATE;
    }

    dbg(TRACE,"<CTCPIPC> SOCKET = %d ",rgTM.iTCPWorkSocket);

    ilRC = tcp_open_connection(rgTM.iTCPWorkSocket, rgTM.pcServiceName, rgTM.pcRemote_Host);
    if (ilRC != RC_SUCCESS){
      dbg(TRACE,"<CTCPIPC> tcp_open_connection returns: %d", rgTM.iTCPWorkSocket);
      return iCDI_TERMINATE;
    }

  }


	/* to inform the status-handler about a established connection */
	if ((ilRC = SendStatus("TCPIP", pcgProcessName, "CON_UP", 1, rgTM.pcServiceName, "", "")) != RC_SUCCESS)
	{
		dbg(TRACE,"<CTCPIPC> SendStatus returns: %d",  ilRC);
		return iCDI_RESET;
	}

	/* send message to db, ceda.log... */
	if ((ilRC = send_message(IPRIO_DB, iCREATE_CONNECTION, 0, 0, rgTM.pcServiceName)) != RC_SUCCESS)
	{
		dbg(TRACE,"<CTCPIPC> send_message returns: %d", ilRC);
		return iCDI_RESET;
	}

	/* send OpenConnection to Andock-/Baggagehandler (someone else)... */
	if (rgTM.iSendConnectModID > 0)
	{
		/* here we send command */
		if ((ilRC = tools_send_sql(rgTM.iSendConnectModID, rgTM.pcSendOpenConnectCmd, "", "", "", "")) != RC_SUCCESS)
		{
			dbg(TRACE,"<CTCPIPC> tools_send_sql returns: %d",  ilRC);
			return iCDI_RESET;
		}
	}
	dbg(TRACE,"<CTCPIPC> CONNECTION IS OPENED");
	dbg(TRACE,"<CTCPIPC> START SOCKET = %d (ACTIVE)",rgTM.iTCPStartSocket);
	dbg(TRACE,"<CTCPIPC> WORK  SOCKET = %d (ACTIVE)",rgTM.iTCPWorkSocket);
	dbg(TRACE,"<CTCPIPC> SENDING UPDATES IS DISABLED");
	strcpy(pcgTCPSendMemory,"CONNECTION ESTABLISHED");
	ToSProtFile(-1); 
	strcpy(pcgTCPReceiveMemory,"CONNECTION ESTABLISHED");
	ToRProtFile(-1); 
	dbg(DEBUG,"<CTCPIPC> ----- END -----");
	/* everything is fine */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The CloseTCPConnection routine                                             */
/******************************************************************************/
static int CloseTCPConnection(void)
{
	int		ilRC;
	int		ilCurKey;
	int		ilOffset;
  int             ilTelegramLen;
	char		pclEndCode[iMIN];
	char		pclTmpBuf[iMIN_BUF_SIZE];
	FILE		*pFh = NULL;

	dbg(DEBUG,"<CloseTCPConnection> ----- START -----");

	/* write .uni file */
	pclTmpBuf[0] = 0x00;
	sprintf(pclTmpBuf, "%s/%s.uni", getenv("TMP_PATH"), pcgProcessName);
	dbg(DEBUG,"<CloseTCPConnection> open file: <%s>", pclTmpBuf);
	if ((pFh = fopen(pclTmpBuf, "w")) != NULL)
	{
		for (ilCurKey=iCTL_CLASS; ilCurKey<=iCCO_CLASS; ilCurKey++)
		{
			fprintf(pFh, "%ld %ld %ld %ld %ld\n", rgCDIUnique[ilCurKey].lCurrentNo, rgCDIUnique[ilCurKey].lLastNo, rgCDIUnique[ilCurKey].lAvailableNo, rgCDIUnique[ilCurKey].lMax, rgCDIUnique[ilCurKey].lMin);
		}
		fclose(pFh);		
	}
	else
		dbg(TRACE,"<CloseTCPConnection> ERROR: cannot open FILE <%s>", pclTmpBuf);

	if (rgTM.iTCPWorkSocket >= 0)
	{
		if (!igNoENDCommand)
		{
			/* get current system state... */
			ctrl_sta = get_system_state();

			/* check current HSB-Mode... */
			if ((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY) && (ctrl_sta != HSB_DOWN))
			{
				/* End-Code is System-State */
				strcpy(pclEndCode, "SS");
			}
			else
			{
				/* End-Code is Shutdown */
				strcpy(pclEndCode, "SD");
			}

			/* send an END to client, only if there is a connection */
			if ((ilRC = BuildEND(pclEndCode)) != RC_SUCCESS)
			{
				dbg(TRACE, "<CloseTCPConnection> BuildEND returns %d", ilRC);
				dbg(DEBUG,"<CloseTCPConnection> ----- END -----");
				ilRC = iCDI_TERMINATE;
			}
			else
			{
	      ilTelegramLen = END_LENGTH(rgTM.iVersion)+rgTM.iAddLFatEnd ;
				ilOffset = 0;
	      memcpy(pcgTCPSendCopy,pcgTCPSendMemory,ilTelegramLen);

				if (rgTM.iSecureOption)
				{
					ilOffset = 2;
					memcpy(pcgTCPSendMemory, SEncode(pcgTCPSendMemory,ilTelegramLen), ilTelegramLen+ilOffset);
					/*ToSCProtFile(ilTelegramLen+ilOffset); JWE */
				}

				/* write it to socket */
	      ilRC = write(rgTM.iTCPWorkSocket, pcgTCPSendMemory,ilTelegramLen+ilOffset);
	      if (ilRC != ilTelegramLen+ilOffset)
				{
					if (igClientErr != TRUE)
					{
						strcpy(pcgTCPSendMemory,"CLIENT CLOSED SOCKET. COULDN'T SEND:");
						ToSProtFile(-1); 
						igClientErr = TRUE;
					} /* end if */
					dbg(TRACE,"<CloseTCPConnection> can't write END to socket %d (RC=%d)",rgTM.iTCPWorkSocket, ilRC);
					ilRC = iCDI_RESET;
				}
				else
				{
					/* write it to send protocol file */
					memcpy(pcgTCPSendMemory,pcgTCPSendCopy,ilTelegramLen);
					ToSProtFile(ilTelegramLen); 

					/* write it to crypted-send protocol file */
					if (rgTM.iSecureOption)
					{
						ilOffset = 2;
						ToSCProtFile(ilTelegramLen+ilOffset);
					}
				}
			}
		}

		/* to inform the status-handler about a established connection */
		if ((ilRC = SendStatus("TCPIP", pcgProcessName, "CON_DOWN", 1, rgTM.pcServiceName, "", "")) != RC_SUCCESS)
		{
			dbg(TRACE,"<CloseTCPConnection> SendStatus returns: %d",  ilRC);
			dbg(DEBUG,"<CloseTCPConnection> ----- END -----");
			return iCDI_RESET;
		}
		
		/* send message to db, ceda.log... */
		if ((ilRC = send_message(IPRIO_DB, iDELETE_CONNECTION, 0, 0, rgTM.pcServiceName)) != RC_SUCCESS)
		{
			dbg(TRACE,"<CloseTCPConnection> send_message returns: %d", ilRC);
			dbg(DEBUG,"<CloseTCPConnection> ----- END -----");
			ilRC = iCDI_RESET;
		}

		/* send CloseConnection to Andock-/Baggagehandler (someone else)... */
		if (rgTM.iSendConnectModID > 0)
		{
			/* here we send command */
			if ((ilRC = tools_send_sql(rgTM.iSendConnectModID, rgTM.pcSendCloseConnectCmd, "", "", "", "")) != RC_SUCCESS)
			{
				dbg(TRACE,"<CloseTCPConnection> tools_send_sql returns: %d",  ilRC);
				dbg(DEBUG,"<CloseTCPConnection> ----- END -----");
				ilRC = iCDI_RESET;
			}
		}
	}

	/* shutdown (listen) socket */
	if (rgTM.iTCPStartSocket < 0)
	{
		if ((ilRC = shutdown(rgTM.iTCPStartSocket, 2)) != 0)
		{
			dbg(DEBUG,"<CloseTCPConnection> shutdown returns: %d, <%s>",  ilRC, strerror(errno));
		} 

		/* close (listen) socket */
		if ((ilRC = close(rgTM.iTCPStartSocket)) != 0)
		{
			dbg(DEBUG,"<CloseTCPConnection> close returns: %d, <%s>",  ilRC, strerror(errno));
		}
		rgTM.iTCPStartSocket = -1;
	}

	/* shutdown (work) socket */
	if ((ilRC = shutdown(rgTM.iTCPWorkSocket, 2)) != 0)
	{
		dbg(DEBUG,"<CloseTCPConnection> shutdown returns: %d, <%s>",  ilRC, strerror(errno));
	} 

	/* close (work) socket */
	if ((ilRC = close(rgTM.iTCPWorkSocket)) != 0)
	{
		dbg(DEBUG,"<CloseTCPConnection> close returns: %d, <%s>",  ilRC, strerror(errno));
	}
	rgTM.iTCPWorkSocket = -1;

	if (rgTM.iTCPWorkSocket >= 0)
	{
		strcpy(pcgTCPSendMemory,"CONNECTION CLOSED");
		ToSProtFile(-1); 
		strcpy(pcgTCPReceiveMemory,"CONNECTION CLOSED");
		ToRProtFile(-1); 
	}
	else
	{
		strcpy(pcgTCPSendMemory,"SOCKET CLOSED");
		ToSProtFile(-1); 
		strcpy(pcgTCPReceiveMemory,"SOCKET CLOSED");
		ToRProtFile(-1); 
	}
	dbg(DEBUG,"<CloseTCPConnection> ----- END -----");

	/* everything looks file */
	return ilRC;
}
/******************************************************************************/
/* The Build routine                                                       */
/******************************************************************************/
static int SendClientCmd(void)
{
  int ilRC;
  int ilLen;

  /* clear memory */
  memset((void*)pcgTCPSendMemory, 0x00, iMAX_DATA);

  /* build header for command */
  BuildHeader((CDI2Header*)pcgTCPSendMemory, "CTL", 0);

  /* set END specifics (Length) */
  sprintf(VERSION(rgTM.iVersion, ((CDI2Header*)pcgTCPSendMemory)->pcLength, ((CDI1Header*)pcgTCPSendMemory)->pcLength), "%-4.4d", sizeof(CDIError));

  /* Command END */
  memmove(((CDIError*)pcgTCPSendMemory)->pcCommand+HEADER_LENGTH(rgTM.iVersion), "SFS", 3);

  /* ErrorCode for END */
  memmove(((CDIError*)pcgTCPSendMemory)->pcData+HEADER_LENGTH(rgTM.iVersion), "0\n", 2);

  /* write it to socket */
 
  ilLen = ERR_LENGTH(rgTM.iVersion);

  ilRC = write(rgTM.iTCPWorkSocket, pcgTCPSendMemory,ilLen);
  if (ilRC != ilLen)
	{
    dbg(TRACE,"<SendClinetCmd> can't write to socket...");
    if (igClientErr != TRUE)
		{
      strcpy(pcgTCPSendMemory,"<SendClinetCmd> CLIENT CLOSED SOCKET. COULDN'T SEND:");
      ToSProtFile(-1); 
      igClientErr = TRUE;
    } /* end if */
    /* write it to send protocol file */
    /*ToSProtFile(ilLen); JWE */

    /* write it to the second temporary queue */
    WriteToSecondQueue(PRIORITY_3);
    return iCDI_RESET;
  }
	else
	{
		/* write it to send protocol file */
		ToSProtFile(ilLen);

		/* write it to crypted-send protocol file */
		if (rgTM.iSecureOption)
		{
			ToSCProtFile(ilLen+2);
		}
	}
  return RC_SUCCESS;
}


/******************************************************************************/
/* The BuildERR routine                                                       */
/******************************************************************************/
static int BuildERR(char *pcpErrorCode)
{
	int				ilRC;
	long				lMin;
	long				lMax;
	long				lUniqueNumber;
	
	/* clear memory */
	memset((void*)pcgTCPSendMemory, 0x00, iMAX_DATA);
	
	if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		/* get next uniquw number */
		if ((ilRC = HandleUniqueNumber(rgTM.pcServerName, rgTM.pcCTLKey, "MAXOW", 
										1, &lUniqueNumber, &lMax, &lMin)) != RC_SUCCESS)
		{
			/* fatal error */
			dbg(TRACE,"<BuildERR> HandleUniqueNumber returns: %d", ilRC);
				
			/* exit here */
			return iCDI_TERMINATE;
		}
	}
	else
	{
		/* wrong HSB-STATUS..., returning with reset here... */
		dbg(TRACE,"<BuildERR> wrong CTRL-STA: <%s>", CTRL_STA(ctrl_sta));
		return iCDI_RESET;
	}

	/* set unique number */
	lUniqueNumber = UNIQUE(lUniqueNumber, lMax, lMin);	

	/* build header for command */
	BuildHeader((CDI2Header*)pcgTCPSendMemory, "CTL", lUniqueNumber);

	/* set END specifics (Length) */
	sprintf(VERSION(rgTM.iVersion, ((CDI2Header*)pcgTCPSendMemory)->pcLength, ((CDI1Header*)pcgTCPSendMemory)->pcLength), "%-4.4d", sizeof(CDIError)+rgTM.iAddLFatEnd);

	/* Command END */
	memmove(((CDIError*)pcgTCPSendMemory)->pcCommand+HEADER_LENGTH(rgTM.iVersion), "ERR", 3);

	/* ErrorCode for END */
	memmove(((CDIError*)pcgTCPSendMemory)->pcData+HEADER_LENGTH(rgTM.iVersion), pcpErrorCode, 2);

	if(rgTM.iAddLFatEnd == 1){
	  memmove(((CDIError*)pcgTCPSendMemory)->pcData+HEADER_LENGTH(rgTM.iVersion)+2,"\n",1);
	}

	/* everything is fine */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The BuildEOD routine                                                       */
/******************************************************************************/
static int BuildEOD(void)
{
	int				ilRC;
	long				lMin;
	long				lMax;
	long				lUniqueNumber;
	
	/* clear memory */
	memset((void*)pcgTCPSendMemory, 0x00, iMAX_DATA);
	
	if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		/* get next unique number */
		if ((ilRC = HandleUniqueNumber(rgTM.pcServerName, rgTM.pcCTLKey, "MAXOW", 
										1, &lUniqueNumber, &lMax, &lMin)) != RC_SUCCESS)
		{
			/* fatal error */
			dbg(TRACE,"<BuildEOD> HandleUniqueNumber returns: %d", ilRC);
				
			/* exit here */
			return iCDI_TERMINATE;
		}
	}
	else
	{
		/* wrong HSB-STATUS..., returning with reset here... */
		dbg(TRACE,"<BuildEOD> wrong CTRL-STA: <%s>", CTRL_STA(ctrl_sta));
		return iCDI_RESET;
	}

	/* set unique number */
	lUniqueNumber = UNIQUE(lUniqueNumber, lMax, lMin);	

	/* build header for command */
	BuildHeader((CDI2Header*)pcgTCPSendMemory, "CTL", lUniqueNumber);

	/* set END specifics (Length) */
	sprintf(VERSION(rgTM.iVersion, ((CDI2Header*)pcgTCPSendMemory)->pcLength, ((CDI1Header*)pcgTCPSendMemory)->pcLength), "%-4.4d", sizeof(CDIEod)-2+rgTM.iAddLFatEnd);

	/* Command END */
	memmove(((CDIEod*)pcgTCPSendMemory)->pcCommand+HEADER_LENGTH(rgTM.iVersion), "EOD", 3);

	if(rgTM.iAddLFatEnd == 1){
	  memmove(((CDIEod*)pcgTCPSendMemory)->pcCommand+HEADER_LENGTH(rgTM.iVersion)+3,"\n",1);
	}


	/* everything is fine */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The BuildEND routine                                                       */
/******************************************************************************/
static int BuildEND(char *pcpEndCode)
{
	int				ilRC;
	long				lMin;
	long				lMax;
	long				lUniqueNumber;
	char				pclEndCode[iMIN];
	
	/* clear memory */
	memset((void*)pcgTCPSendMemory, 0x00, iMAX_DATA);
	pclEndCode[0] = 0x00;
	
	if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		/* get next unique number */
		if ((ilRC = HandleUniqueNumber(rgTM.pcServerName, rgTM.pcCTLKey, "MAXOW", 
										1, &lUniqueNumber, &lMax, &lMin)) != RC_SUCCESS)
		{
			/* fatal error */
			dbg(TRACE,"<BuildEND> HandleUniqueNumber returns: %d", ilRC);
			dbg(TRACE,"<BuildEND> using default number: 0");

			/* set default unique number... */
			lUniqueNumber = 0;

			/* set error code (DATABASE ERROR -> DB) */
			strcpy(pclEndCode, "DB");
		}
		else
		{
			/* set unique number */
			lUniqueNumber = UNIQUE(lUniqueNumber, lMax, lMin);	

			/* set error code */
			strcpy(pclEndCode, pcpEndCode);
		}
	}
	else
	{
		dbg(TRACE,"<BuildEND> CTRL-STA: <%s>", CTRL_STA(ctrl_sta));
		dbg(TRACE,"<BuildEND> using default number: 0");

		/* set default unique number... */
		lUniqueNumber = 0;

		/* set error code */
		strcpy(pclEndCode, pcpEndCode);
	}

	/* build header for command */
	BuildHeader((CDI2Header*)pcgTCPSendMemory, "CTL", lUniqueNumber);

	/* set END specifics (Length) */
	sprintf(VERSION(rgTM.iVersion, ((CDI2Header*)pcgTCPSendMemory)->pcLength, ((CDI1Header*)pcgTCPSendMemory)->pcLength), "%-4.4d", sizeof(CDIEnd)+rgTM.iAddLFatEnd);

	/* Command END */
	memmove(((CDIEnd*)pcgTCPSendMemory)->pcCommand+HEADER_LENGTH(rgTM.iVersion), "END", 3);

	/* ErrorCode for END */
	memmove(((CDIEnd*)pcgTCPSendMemory)->pcData+HEADER_LENGTH(rgTM.iVersion), pclEndCode, 2);

	if(rgTM.iAddLFatEnd == 1){
	  memmove(((CDIEnd*)pcgTCPSendMemory)->pcData+HEADER_LENGTH(rgTM.iVersion)+2,"\n",1);
	}

	/* everything is fine */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The BuildHeader routine                                                    */
/******************************************************************************/
static void BuildHeader(CDI2Header *prpTH, char *pcpClass, long lpTeleNo)
{
	/* fill structures (Server System) */
	memmove(prpTH->pcServerSystem, rgTM.pcServerSystem, 3);

	/* Sender */
	memmove(prpTH->pcSender, rgTM.pcSender, 10);

	/* Receiver */
	memmove(prpTH->pcReceiver, rgTM.pcReceiver, 10);

	if (rgTM.iVersion >= 200)
	{
		/* Version */
		memmove(prpTH->pcVersion, rgTM.pcVersion, 3);

		/* current TimeStamp */
		memmove(prpTH->pcTimeStamp, GetTimeStamp(), 14);

		/* write new number to header structure */
		sprintf(prpTH->pcTeleNo, "%-5.5ld", lpTeleNo);

		/* class types (CTL,DAT) */
		memmove(prpTH->pcTeleClass, pcpClass, 3);
	}
	else
	{
		/* current TimeStamp */
		memmove(prpTH->pcTimeStamp-3, GetTimeStamp(), 14);

		/* write new number to header structure */
		sprintf(prpTH->pcTeleNo-3, "%-5.5ld", lpTeleNo);

		/* class types (CTL,DAT) */
		memmove(prpTH->pcTeleClass-3, pcpClass, 3);
	}

	/* that's all */
	return;
}

/******************************************************************************/
/* The ChangeHeader routine                                                   */
/******************************************************************************/
static void ChangeHeader(CDI2Header *prpTH, int ipMember, char *pcpNewValue)
{
	if (ipMember == iSERVER_SYSTEM)
	{
		/* changed Server System) */
		memmove(prpTH->pcServerSystem, pcpNewValue, 3);
	}
	else if (ipMember == iSENDER)
	{
		/* Sender */
		memmove(prpTH->pcSender, pcpNewValue, 10);
	}
	else if (ipMember == iRECEIVER)
	{
		/* Receiver */
		memmove(prpTH->pcReceiver, pcpNewValue, 10);
	}
	else if (ipMember == iVERSION)
	{
		/* this is available at version 2.xx... */
		if (rgTM.iVersion >= 200)
		{
			/* Version */
			memmove(prpTH->pcVersion, pcpNewValue, 3);
		}
	}
	else if (ipMember == iTIME_STAMP)
	{
		if (rgTM.iVersion >= 200)
		{
			/* current TimeStamp */
			memmove(prpTH->pcTimeStamp, pcpNewValue, 14);
		}
		else
		{
			/* current TimeStamp */
			memmove(prpTH->pcTimeStamp-3, pcpNewValue, 14);
		}
	}
	else if (ipMember == iTELE_NUMBER)
	{
		if (rgTM.iVersion >= 200)
		{
			/* write new number to header structure */
			memmove(prpTH->pcTeleNo, pcpNewValue, 5);

		}
		else
		{
			/* write new number to header structure */
			memmove(prpTH->pcTeleNo-3, pcpNewValue, 5);
		}
	}
	else if (ipMember == iTELE_CLASS)
	{
		if (rgTM.iVersion >= 200)
		{
			/* class types (CTL,DAT) */
			memmove(prpTH->pcTeleClass, pcpNewValue, 3);
		}
		else
		{
			/* class types (CTL,DAT) */
			memmove(prpTH->pcTeleClass-3, pcpNewValue, 3);
		}
	}
	else if (ipMember == iLENGTH)
	{
		if (rgTM.iVersion >= 200)
		{
			/* set length */
			memmove(prpTH->pcLength, pcpNewValue, 3);
		}
		else
		{
			/* set length */
			memmove(prpTH->pcLength-3, pcpNewValue, 3);
		}
	}

	/* that's all */
	return;
}

/******************************************************************************/
/* Die interne TCP-Wait-Timeout-Routine, ist eigentlich eine LIB-Funktion     */
/* aber hier ist sie erweitert um dritten Parameter flg, kenner ob sekunden   */
/* oder microsekunden genommen werden sollen...     	                        */
/******************************************************************************/
static int TcpWaitTimeout(int sock, long t, int flg)
{
	int	rc = RC_FAIL;
	fd_set readfds;    
	struct timeval timeval;
	int cnt = 0;

	/*dbg(TRACE,"TcpWaitTimeout: sock<%d>,t<%ld>,flag<%d>",sock,t,flg);*/
	FD_ZERO(&readfds);
	FD_SET(sock,&readfds);
	if (sock > igMaxFd)
	{
		igMaxFd = sock;
	}
	if (flg)
	{
		timeval.tv_sec = t;
		timeval.tv_usec = 0;
	}
	else
	{
		timeval.tv_sec = 0;
		timeval.tv_usec = t;
	}

#ifndef _HPUX_SOURCE
		cnt = select (igMaxFd+1, &readfds, NULL, NULL, &timeval);
#else
		cnt = select (igMaxFd+1, (fd_set *) &readfds, NULL, NULL, &timeval);
#endif
	  
	if (cnt == -1)
	{
		dbg(TRACE, "TcpWaitTimeout: select err:sock=%d rc=%d : %s" ,sock,rc,strerror(errno));  	
		rc = RC_FAIL;
	}
 
	if (cnt == 0)
	{
		rc = RC_NOT_FOUND;
	}
	if (cnt > 0)
	{
		if (FD_ISSET(sock,&readfds))
			rc = RC_SUCCESS;
	}
	return rc ;
} 
/******************************************************************************/
/* The CheckConnection routine                                                */
/******************************************************************************/
static int CheckConnection(int ipFlag)
{
	int			ilRC;
	int			ilOffset;
	int			ilTelegramLen;
	char			pclNoBuf[10];

	/* current time */
	time2 = time(NULL);

	/* is difftime enough for new connection check? */
	if ((double)rgTM.iCheckConnection > difftime(time2, time1))
	{
		return RC_SUCCESS;
	}

	dbg(DEBUG,"<CheckConnection> ----- START -----");

	/* set new time1 */
	time1 = time2;

	if (ipFlag == iCCO_CHECK)
	{
		/* Build the CCO-Structure */
		if ((ilRC = BuildCCO("C")) != RC_SUCCESS)
		{
			dbg(TRACE,"<CheckConnection> BuildCCO returns %d", ilRC);
			dbg(DEBUG,"<CheckConnection> ----- END -----");
			return iCDI_TERMINATE;
		}

		/* set Flag, we need it for different timeouts */
		igCCOCheck = 1;

		/* set current CCO-Number */
		memmove(pclNoBuf, pcgTCPSendMemory+HEADER_LENGTH(rgTM.iVersion)-12, 5);
    pclNoBuf[5] = '\0';
		igLastCCONo = atoi(pclNoBuf);
		dbg(DEBUG,"<CheckConnection> LastCCONo: %d", igLastCCONo);

		/* set length */
		ilTelegramLen = CCO_LENGTH(rgTM.iVersion)+rgTM.iAddLFatEnd;
	}
	else
	{
		/* Build the CCO-Structure */
		if ((ilRC = BuildDBL()) != RC_SUCCESS)
		{
			dbg(TRACE, "<CheckConnection> BuildDBL returns %d", ilRC);
			dbg(DEBUG,"<CheckConnection> ----- END -----");
			return iCDI_TERMINATE;
		}

		/* set Flag, we need it for different timeouts */
		igDBLCheck = 1;

		/* set length */
		ilTelegramLen = DBL_LENGTH(rgTM.iVersion);
	}

	ilOffset = 0;
	memcpy(pcgTCPSendCopy,pcgTCPSendMemory,ilTelegramLen);

	if (rgTM.iSecureOption)
	{
		ilOffset = 2;
		memcpy(pcgTCPSendMemory, SEncode(pcgTCPSendMemory, ilTelegramLen), ilTelegramLen+ilOffset);
		/*ToSCProtFile(ilTelegramLen+ilOffset); JWE */
	}

	/* write it to socket */
	ilRC = write(rgTM.iTCPWorkSocket, pcgTCPSendMemory, ilTelegramLen+ilOffset);
	if (ilRC != ilTelegramLen+ilOffset)
	{
		if (igClientErr != TRUE)
		{
			strcpy(pcgTCPSendMemory,"CLIENT CLOSED SOCKET. COULDN'T SEND:");
			ToSProtFile(-1); 
			igClientErr = TRUE;
		} /* end if */
		memcpy(pcgTCPSendMemory,pcgTCPSendCopy,ilTelegramLen);
		ToSProtFile(ilTelegramLen); 
		dbg(TRACE,"<CheckConnection> can't write to socket %d (RC=%d)",rgTM.iTCPWorkSocket, ilRC);
		return iCDI_RESET;
	}
	else
	{
		/* write it to send protocol file */
		memcpy(pcgTCPSendMemory,pcgTCPSendCopy,ilTelegramLen);
		ToSProtFile(ilTelegramLen); 

		if (rgTM.iSecureOption)
		{
			ilOffset = 2;
			ToSCProtFile(ilTelegramLen+ilOffset);
		}
	}

	dbg(DEBUG,"<CheckConnection> ----- END -----");
	/* ciao */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The BuildCCO routine                                                       */
/******************************************************************************/
static int BuildCCO(char *pcpType)
{
	int				ilRC;
	long				lMin;
	long				lMax;
	long				lUniqueNumber;
	
	/* answer check connection call */
	memset((void*)pcgTCPSendMemory, 0x00, iMAX_DATA);

	/* get next number only if type is CHECK */
	if (!strcmp(pcpType, "C"))
	{
		if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
		{
			/* get next unique number */
			if ((ilRC = HandleUniqueNumber(rgTM.pcServerName, rgTM.pcCCOKey, "MAXOW", 1, &lUniqueNumber, &lMax, &lMin)) != RC_SUCCESS)
			{
				/* fatal error */
				dbg(TRACE,"<BuildCCO> HandleUniqueNumber returns: %d", ilRC);

				/* exit process */
				return iCDI_TERMINATE;
			}

			/* set unique number */
			lUniqueNumber = UNIQUE(lUniqueNumber, lMax, lMin);	
		}
		else
			lUniqueNumber = 0;
	}
	else
		lUniqueNumber = 0;

	/* build header for command */
	BuildHeader((CDI2Header*)pcgTCPSendMemory, "CTL", lUniqueNumber);

	/* set END specifics (Length) */
  sprintf(VERSION(rgTM.iVersion, ((CDI2Header*)pcgTCPSendMemory)->pcLength, ((CDI1Header*)pcgTCPSendMemory)->pcLength), "%-4.4d", sizeof(CDICheck)-1+rgTM.iAddLFatEnd);

	/* Command CCO */
	memmove(((CDICheck*)pcgTCPSendMemory)->pcCommand+HEADER_LENGTH(rgTM.iVersion), "CCO", 3);

	/* set answer / question */
	memmove(((CDICheck*)pcgTCPSendMemory)->pcData+HEADER_LENGTH(rgTM.iVersion), pcpType, 1);

  if(rgTM.iAddLFatEnd == 1){
    /* add linefeed */
    memset((((CDICheck*)pcgTCPSendMemory)->pcData+HEADER_LENGTH(rgTM.iVersion))+1,10,1);
  }

  dbg(DEBUG,"buildcco sendmem <%s> len <%d>",((CDICheck*)pcgTCPSendMemory)->pcData,strlen(pcgTCPSendMemory)); 
    
	/* everything is fine */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The BuildDBL routine                                                       */
/******************************************************************************/
static int BuildDBL(void)
{
	/* answer check connection call */
	memset((void*)pcgTCPSendMemory, 0x00, iMAX_DATA);

	/* build header for command */
	BuildHeader((CDI2Header*)pcgTCPSendMemory, "CTL", 0L);

	/* set END specifics (Length) */
	sprintf(VERSION(rgTM.iVersion, ((CDI2Header*)pcgTCPSendMemory)->pcLength, ((CDI1Header*)pcgTCPSendMemory)->pcLength), "%-4.4d", sizeof(CDIDBLocked));

	/* Command CCO */
	memmove(((CDICheck*)pcgTCPSendMemory)->pcCommand+HEADER_LENGTH(rgTM.iVersion), "DBL", 3);

	/* everything is fine */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The HandleReceivedData routine                                             */
/******************************************************************************/
static int HandleReceivedData(void)
{
	int		ilRC;

	dbg(DEBUG,"<HandleReceivedData> ----- START -----");

	/* reset CCO-Flag... */
	igCCOCheck = 0;
	igDBLCheck = 0;
	igNoENDCommand = 0;

	/* set new time... */
	time1 = time(NULL);

	/* FUNCTIONS: HandleCTLData, HandleDATData, HandleDELData */
	ilRC = (*rgTM.pTCHandler[rgTM.iTCNumber])();

	dbg(DEBUG,"<HandleReceivedData> ----- END -----");
	dbg(DEBUG,"=================================================\n");
	/* bye bye */
	return ilRC;
}

/******************************************************************************/
/* The WriteERRToClient routine                                               */
/******************************************************************************/
static int WriteERRToClient(char *pcpErrorCode)
{
	int		ilRC = RC_SUCCESS;
	int             iIncLF = 0;
	int		ilOffset;
	char		pclTmpBuf[iMIN];

	dbg(DEBUG,"<WERRToC> ----- START -----");

	iIncLF = rgTM.iAddLFatEnd;

	/* in this case send an ERR-Command to client... */
	if ((ilRC = BuildERR(pcpErrorCode)) != RC_SUCCESS)
	{
		dbg(TRACE,"<WERRToC> cannot create error command"); 
		dbg(DEBUG,"<WERRToC> ----- END -----");
		return iCDI_TERMINATE;
	}
	else
	{
		memset((void*)pclTmpBuf, 0x00, iMIN);
		if (!strcmp(pcpErrorCode, "WY"))
			strcpy(pclTmpBuf, "WRONG_SERVERSYSTEM");
		else if (!strcmp(pcpErrorCode, "US"))
			strcpy(pclTmpBuf, "UNKNOWN_SENDER");
		else if (!strcmp(pcpErrorCode, "UR"))
			strcpy(pclTmpBuf, "UNKNOWN_RECEIVER");
		else if (!strcmp(pcpErrorCode, "TO"))
			strcpy(pclTmpBuf, "TIMEOUT");
		else if (!strcmp(pcpErrorCode, "WC"))
			strcpy(pclTmpBuf, "WRONG_COMMAND");
		else if (!strcmp(pcpErrorCode, "UC"))
			strcpy(pclTmpBuf, "UNKNOWN_CLASS");
		else if (!strcmp(pcpErrorCode, "WT"))
			strcpy(pclTmpBuf, "WRONG_TIMESTAMP");
		else if (!strcmp(pcpErrorCode, "WL"))
			strcpy(pclTmpBuf, "WRONG_LENGTH");
		else if (!strcmp(pcpErrorCode, "WN"))
			strcpy(pclTmpBuf, "WRONG_NUMBER");
		else if (!strcmp(pcpErrorCode, "CN"))
			strcpy(pclTmpBuf, "CCO_TNUMBER");
		else if (!strcmp(pcpErrorCode, "AC"))
			strcpy(pclTmpBuf, "AC_ERROR");
		else if (!strcmp(pcpErrorCode, "ND"))
			strcpy(pclTmpBuf, "NO_DATA");
		else if (!strcmp(pcpErrorCode, "WS"))
			strcpy(pclTmpBuf, "WRONG_SUBCOMMAND");
		else if (!strcmp(pcpErrorCode, "SM"))
			strcpy(pclTmpBuf, "SEPARATOR_MISSING");
		else if (!strcmp(pcpErrorCode, "WD"))
			strcpy(pclTmpBuf, "WRONG_DATA");
		else if (!strcmp(pcpErrorCode, "UF"))
			strcpy(pclTmpBuf, "UNKNOWN_FIELDNAME");
		else if (!strcmp(pcpErrorCode, "WE"))
			strcpy(pclTmpBuf, "WRONG_ELEMENTS");
		else if (!strcmp(pcpErrorCode, "IC"))
			strcpy(pclTmpBuf, "INVALID_CMD");
		else if (!strcmp(pcpErrorCode, "OF"))
			strcpy(pclTmpBuf, "WRONG_NUMBER_OF_FIELDS");
		else if (!strcmp(pcpErrorCode, "IE"))
			strcpy(pclTmpBuf, "INTERNAL_ERROR");
		else
			strcpy(pclTmpBuf, "UNKNOWN_ERROR");
		
		/* to ceda-log file */
		if ((ilRC = send_message(IPRIO_DB, iWERR_TO_CLIENT, 0, 0, pclTmpBuf)) != RC_SUCCESS)
		{
			dbg(TRACE,"<WERRToC> send_message returns: %d", ilRC);
			dbg(DEBUG,"<WERRToC> ----- END -----");
			return iCDI_RESET;
		}

		/* write it to send protocol file */
		memcpy(pcgTCPSendCopy,pcgTCPSendMemory,ERR_LENGTH(rgTM.iVersion)+iIncLF);
		ilOffset = 0;

		if (rgTM.iSecureOption)
		{
			ilOffset = 2;
			memcpy(pcgTCPSendMemory, SEncode(pcgTCPSendMemory, ERR_LENGTH(rgTM.iVersion)+iIncLF), ERR_LENGTH(rgTM.iVersion)+ilOffset+iIncLF);
			/*ToSCProtFile(ERR_LENGTH(rgTM.iVersion)+ilOffset+iIncLF); JWE */
		}

		/* write it to socket */
		ilRC = write(rgTM.iTCPWorkSocket,pcgTCPSendMemory,ERR_LENGTH(rgTM.iVersion)+ilOffset+iIncLF);
		if (ilRC != ERR_LENGTH(rgTM.iVersion)+ilOffset+iIncLF)
		{
			if (igClientErr != TRUE)
			{
				strcpy(pcgTCPSendMemory,"CLIENT CLOSED SOCKET. COULDN'T SEND:");
				ToSProtFile(-1); 
				igClientErr = TRUE;
			} /* end if */
			memcpy(pcgTCPSendMemory,pcgTCPSendCopy,ERR_LENGTH(rgTM.iVersion)+iIncLF);
			ToSProtFile(ERR_LENGTH(rgTM.iVersion)+iIncLF); 
			dbg(TRACE,"<WERRToC> can't write ERR (%d)", ilRC);
			return iCDI_RESET;
		}
		else
		{
			memcpy(pcgTCPSendMemory,pcgTCPSendCopy,ERR_LENGTH(rgTM.iVersion)+iIncLF);
			ToSProtFile(ERR_LENGTH(rgTM.iVersion)+iIncLF); 

			if (rgTM.iSecureOption)
			{
				ilOffset = 2;
				ToSCProtFile(ERR_LENGTH(rgTM.iVersion)+ilOffset+iIncLF);
			}
		}
	}

	dbg(DEBUG,"<WERRToC> ----- END -----");
	/* bye bye */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The WriteEODToClient routine                                               */
/******************************************************************************/
static int WriteEODToClient(void)
{
	int		ilRC = RC_SUCCESS;
	int		ilOffset;

	dbg(TRACE,"<WEODToC> ----- START -----");

	/* in this case send an ERR-Command to client... */
	if ((ilRC = BuildEOD()) != RC_SUCCESS)
	{
		dbg(TRACE,"<WEODToC> cannot create EOD command"); 
		dbg(DEBUG,"<WEODToC> ----- END -----");
		return iCDI_TERMINATE;
	}
	else
	{
		/* write it to send protocol file */
		/*ToSProtFile(EOD_LENGTH(rgTM.iVersion)); JWE*/
		ilOffset = 0;

		if (rgTM.iSecureOption)
		{
			ilOffset = 2;
			memcpy(pcgTCPSendMemory, SEncode(pcgTCPSendMemory, EOD_LENGTH(rgTM.iVersion)), EOD_LENGTH(rgTM.iVersion)+ilOffset);
			/*ToSCProtFile(ERR_LENGTH(rgTM.iVersion)+ilOffset);  JWE */
		}

		/* write it to socket */
		ilRC = write(rgTM.iTCPWorkSocket, pcgTCPSendMemory, EOD_LENGTH(rgTM.iVersion)+ilOffset);
		if (ilRC != EOD_LENGTH(rgTM.iVersion)+ilOffset)
		{
			dbg(TRACE,"<WEODToC> can't write EOD-command (%d)", ilRC);
			dbg(DEBUG,"<WEODToC> ----- END -----");
			return iCDI_RESET;
		}
		else
		{
			/* write it to send protocol file */
			ToSProtFile(EOD_LENGTH(rgTM.iVersion));

			if (rgTM.iSecureOption)
			{
				ilOffset = 2;
				ToSCProtFile(ERR_LENGTH(rgTM.iVersion)+ilOffset); 
			}
		}
	}

	dbg(DEBUG,"<WEODToC> ----- END -----");
	/* bye bye */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The SearchCommandTypeAndNumber routine                                     */
/******************************************************************************/
static int SearchCommandTypeAndNumber(int ipClass, 
												  int *pipCmdType, int *pipCmdNo)
{
	int	i;

	if (ipClass == iCTL_CLASS)
	{
		/* search internal commands */
		dbg(DEBUG,"<SCTAN> looking for Internal Command...");
		for (i=0; i<rgTM.iNoInternalCmd; i++)
		{
			if (!memcmp(rgTM.prInternalCmd[i].pcCmd, ((CDIInternal*)pcgTCPReceiveMemory)->pcCommand+HEADER_LENGTH(rgTM.iVersion), 3))
			{
				dbg(DEBUG,"<SCTAN> found Internal Command <%s>...", rgTM.prInternalCmd[i].pcCmd);
				*pipCmdType = iINTERNAL_CMD;
				*pipCmdNo = i;
				return RC_SUCCESS;
			}
		}

		/* first search inventory commands */
		dbg(DEBUG,"<SCTAN> looking for Inventory Command...");
		for (i=0; i<rgTM.iNoInventoryCmd; i++)
		{
			if (!memcmp(rgTM.prInventoryCmd[i].pcCmd, ((CDIInventory*)pcgTCPReceiveMemory)->pcCommand+HEADER_LENGTH(rgTM.iVersion), 3))
			{
				dbg(DEBUG,"<SCTAN> found Inventory Command <%s>...", rgTM.prInventoryCmd[i].pcCmd);
				*pipCmdType = iINVENTORY_CMD;
				*pipCmdNo = i;
				return RC_SUCCESS;
			}
		}

		/* second search update commands */
		dbg(DEBUG,"<SCTAN> looking for Update Command...");
		for (i=0; i<rgTM.iNoUpdateCmd; i++)
		{
			if (!memcmp(rgTM.prUpdateCmd[i].pcCmd, ((CDIUpdate*)pcgTCPReceiveMemory)->pcCommand+HEADER_LENGTH(rgTM.iVersion), 3))
			{
				dbg(DEBUG,"<SCTAN> found Update Command <%s>...", rgTM.prUpdateCmd[i].pcCmd);
				*pipCmdType = iUPDATE_CMD;
				*pipCmdNo = i;
				return RC_SUCCESS;
			}
		}

		/* third search free query commands */
		dbg(DEBUG,"<SCTAN> looking for Free Query Command...");
		for (i=0; i<rgTM.iNoFreeQueryCmd; i++)
		{
			if (!memcmp(rgTM.prFreeQueryCmd[i].pcCmd, ((CDIFreeQuery*)pcgTCPReceiveMemory)->pcCommand+HEADER_LENGTH(rgTM.iVersion), 3))
			{
				dbg(DEBUG,"<SCTAN> found Free Query Command <%s>...", rgTM.prFreeQueryCmd[i].pcCmd);
				*pipCmdType = iFREE_QUERY_CMD;
				*pipCmdNo = i;
				return RC_SUCCESS;
			}
		}
      /* search incoming data commands */
      dbg(DEBUG,"<SCTAN> looking for Incoming Data Command...");
      for (i=0; i<rgTM.iNoIncomingDataCmd; i++)
	{
	  if (!memcmp(rgTM.prIncomingDataCmd[i].pcCmd, ((CDIIncomingData*)pcgTCPReceiveMemory)->pcCommand+HEADER_LENGTH(rgTM.iVersion), 3))
	    {
	      dbg(DEBUG,"<SCTAN> found Incoming Data Command <%s>...", rgTM.prIncomingDataCmd[i].pcCmd);
	      *pipCmdType = iINCOMING_DATA_CMD;
	      *pipCmdNo = i;
	      return RC_SUCCESS;
	    }
	}
	}
	else if (ipClass == iDAT_CLASS || ipClass == iINS_CLASS || ipClass == iDEL_CLASS)
	{
		/* search incoming data commands */
		dbg(DEBUG,"<SCTAN> looking for Incoming Data Command...");
		for (i=0; i<rgTM.iNoIncomingDataCmd; i++)
		{
			if (!memcmp(rgTM.prIncomingDataCmd[i].pcCmd, ((CDIIncomingData*)pcgTCPReceiveMemory)->pcCommand+HEADER_LENGTH(rgTM.iVersion), 3))
			{
				dbg(DEBUG,"<SCTAN> found Incoming Data Command <%s>...", rgTM.prIncomingDataCmd[i].pcCmd);
				*pipCmdType = iINCOMING_DATA_CMD;
				*pipCmdNo = i;
				return RC_SUCCESS;
			}
		}
	}

	/* return with command not found... */
	return RC_FAIL;
}

/******************************************************************************/
/* The HandleCTLData routine                                                  */
/******************************************************************************/
static int HandleCTLData(void)
{
	int		ilRC;
  int           i;
	int		ilCmdNo;
	int		ilCmdType;
  char         *pclPtr = NULL;

	dbg(DEBUG,"<HandleCTLData> ----- START -----");

	/* search command-type */
	if ((ilRC = SearchCommandTypeAndNumber(rgTM.iTCNumber, &ilCmdType, &ilCmdNo)) == RC_FAIL)
	{
		/* unknown command received */
		/* reset the server, because it's not allowed to send such a command */
		dbg(TRACE,"<HandleCTLData> SCTAN returns with RC_FAIL...");

		/* send an ERR-Command (Invalid Command received) to client */
		ilRC = WriteERRToClient(sINVALID_CMD);
		dbg(DEBUG,"<HandleCTLData> ----- END -----");
		return ilRC == iCDI_TERMINATE ? iCDI_TERMINATE : iCDI_RESET;
	}
	dbg(DEBUG,"<HandleCTLData> CMD-No: %d, CMD-Type: <%s>", ilCmdNo, CMD_TYPE(ilCmdType));

  if (ilCmdType == iINCOMING_DATA_CMD){
    dbg(DEBUG,"<HandleCTLData> %s are DAT not CTL",CMD_TYPE(ilCmdType)); 

      pclPtr = malloc(strlen( pcgTCPReceiveMemory + HEADER_LENGTH(rgTM.iVersion+3))+5);
      strcpy (pclPtr,pcgTCPReceiveMemory + HEADER_LENGTH(rgTM.iVersion)+3);
      memset(pcgTCPReceiveMemory + HEADER_LENGTH(rgTM.iVersion)+3,'0',44);
      strcpy(pcgTCPReceiveMemory + HEADER_LENGTH(rgTM.iVersion)+3+44,pclPtr);
      i = strlen(pcgTCPReceiveMemory);

      if (pcgTCPReceiveMemory[i-1] == '\n'){
         pcgTCPReceiveMemory[i-1] = 0x00;
      }
      dbg(DEBUG,"<HandleCTLData> %s",pcgTCPReceiveMemory); 
      
      free(pclPtr);



    ilRC = HandleDATData();
  } else {

	/* possible function calls */
	/* FUNCTIONS: HandleInventoryCmd, HandleUpdateCmd, 
					  HandleFreeQueryCmd, HandleInternalCmd
	*/
	/* call function depending on command type */
	ilRC = (*rgTM.pCTLHandler[ilCmdType])(ilCmdNo);

	dbg(DEBUG,"<HandleCTLData> ----- END -----");
	/* bye bye */
  }

	return ilRC;
}

/******************************************************************************/
/* The HandleDELData routine                                                  */
/******************************************************************************/
static int HandleDELData(void)
{
	int		ilRC;
	
	dbg(DEBUG,"<HandleDELData> ----- START -----");
	ilRC = HandleDATData();
	dbg(DEBUG,"<HandleDELData> ----- END -----");

	/* bye bye */
	return ilRC;
}

/******************************************************************************/
/* The HandleDATData routine                                                  */
/******************************************************************************/
static int HandleDATData(void)
{
	int					ilRC;
  int                                     i;
  int                                   ilIntIdx,ilExtIdx;
	int					ilCmdNo;
	int					ilCmdType;
	int					ilCurElement;
	int					ilNoOfElements;
	int					ilTimeMapping;
	int					ilOldDbgLvl;
	int					ilSelection;
	int					ilSelectItems;
	int					ilCurSelectItem;
  int                                   ilCombNo;
	char					clSeparator;
	char					*pclDP = NULL;
  char					*pclPtr = NULL;
	char					*pclDataPtr = NULL;
	char					pclDataBuffer[iMAX_BUF_SIZE];
  char					pclResDataBuffer[iMAX_BUF_SIZE];
  char					pclResFieldList[iMAX_BUF_SIZE];
	char					pclFieldList[iMAXIMUM];
	char					pclDataList[iMAXIMUM];
	char					pclSelection[2*iMAX_BUF_SIZE];
	char					pclTmpTime[15];
	char					pclTmpBuf[iMAX_BUF_SIZE];
	CDICmd				*prlCmdPtr = NULL;
	CDIIncomingData	*prlCDIIncomingData = NULL;
	BC_HEAD				rlBCHead;
	CMDBLK				rlCmdblk;
	/* CHANGES BY MOS */
	char 					pcldummy[iMAX_BUF_SIZE];
  /* END OF CHANGES */



	dbg(DEBUG,"<HandleDATData> ----- START -----");

	prlCDIIncomingData = (CDIIncomingData*)(pcgTCPReceiveMemory + HEADER_LENGTH(rgTM.iVersion));
        if (strncmp(prlCDIIncomingData->pcCommand,"ABA",3) == 0)
        {
           HandleAcknowledge(prlCDIIncomingData);
           return RC_SUCCESS;
        }

	/* search command-type */
	if ((ilRC = SearchCommandTypeAndNumber(rgTM.iTCNumber, &ilCmdType, &ilCmdNo)) == RC_FAIL)
	{
		/* unknown command received */
		/* reset the server, because it's not allowed to send such a command */
		dbg(TRACE,"<HandleDATData> SCTAN returns with RC_FAIL...");

		/* send an ERR-Command (Invalid Command received) to client */
		ilRC = WriteERRToClient(sINVALID_CMD);
		dbg(DEBUG,"<HandleDATData> ----- END -----");
		return ilRC == iCDI_TERMINATE ? iCDI_TERMINATE : iCDI_RESET;
	}
	dbg(TRACE,"<HandleDATData> CMD-No: %d, CMD-Type: <%s>", ilCmdNo, CMD_TYPE(ilCmdType));
	sprintf(pcgTwStart,"%d,%d,-1",ilCmdType,ilCmdNo);

	/* check command type, it must be incoming data command!! */
	if (ilCmdType != iINCOMING_DATA_CMD)
	{
		dbg(TRACE,"<HandleDATData> CMD-Type isn't INCOMING DATA CMD, can't handle");	
		/* send an ERR-Command (Invalid Command received) to client */
		ilRC = WriteERRToClient(sINVALID_CMD);
		dbg(DEBUG,"<HandleDATData> ----- END -----");
		return ilRC == iCDI_TERMINATE ? iCDI_TERMINATE : iCDI_RESET;
	}

	/* set pointer to stucture... */
	prlCmdPtr = &rgTM.prIncomingDataCmd[ilCmdNo];

	/* set pointer we use */
	prlCDIIncomingData = (CDIIncomingData*)(pcgTCPReceiveMemory + HEADER_LENGTH(rgTM.iVersion));

	/* get current separator... */
	clSeparator = prlCDIIncomingData->pcSeparator[0];
	dbg(DEBUG,"<HandleDATData> found Separator: <%c>",  clSeparator);

	/* set local pointer to data area... */
	pclDataPtr = prlCDIIncomingData->pcData;
	dbg(DEBUG,"<HandleDATData> found Data Area: <%s>",  pclDataPtr);

	/* check last character, must be separator... */
  if (pclDataPtr[strlen(pclDataPtr)-1] != clSeparator)   {
		dbg(DEBUG,"<HandleDATData> missing Separator <%c> at end of datablock...", clSeparator);
    if ((ilRC = WriteERRToClient(sSEPARATOR_MISSING)) != RC_SUCCESS)	{
			dbg(TRACE,"<HandleDATData> WriteERRToClient returns: <%s>", RETURN(ilRC));
			dbg(DEBUG,"<HandleDATData> ----- END -----");
			return ilRC;
    } else {
			dbg(DEBUG,"<HandleDATData> ----- END -----");
			return RC_SUCCESS;
		}
  } else {
		/* last separator is ok... */
		/* now format is: FIELD#DATA#FIELD#DATA#FIELD#.#SELECTION#DATA#DATA#...*/
		/* count elements */
		ilNoOfElements = GetNoOfElements(pclDataPtr, clSeparator);
		--ilNoOfElements;
		dbg(DEBUG,"<HandleDATData> found %d elements in record...",  ilNoOfElements);

		/* clear buffer */
		pclDataList[0] = 0x00;
		pclFieldList[0] = 0x00;

		/* set local selection string */
		strcpy(pclSelection, prlCmdPtr->prTabDef[0].pcSelection);
		dbg(DEBUG,"<HandleDATData> Selection: <%s>",  pclSelection);
		ilSelectItems = GetNoOfElements(pclSelection, ' ');
		dbg(DEBUG,"<HandleDATData> found %d select items",  ilSelectItems);

		/* for all elements */
    for (ilCurElement=0, ilSelection=FALSE, ilCurSelectItem=0; ilCurElement<ilNoOfElements; ilCurElement++) {
			/* get n.th element */
			pclDataBuffer[0] = 0x00;

			/*CHANGES BY MOS FOR REMAINING OF ONE BLANK*/
						/*pclDataPtr = CopyNextField(pclDataPtr, clSeparator, pclDataBuffer);*/
			pclDataPtr = CDICopyNextField(pclDataPtr, clSeparator, pclDataBuffer,iONE_BLANK_REMAINING);
			/* END OF CHANGES */


			/* following fields are the selection */
      if (ilSelection == TRUE) {
				/* this is selection, see configuration file */
				/* change selection */
	do {
					pclTmpBuf[0] = 0x00;
					strcpy(pclTmpBuf, GetDataField(pclSelection, ilCurSelectItem, ' '));

					/* alter counter because of blanks in data buffer */
					ilSelectItems = GetNoOfElements(pclSelection, ' ');
					dbg(DEBUG,"<HandleDATData> found %d select items",  ilSelectItems);

					/* this must be substituted */
					if (pclTmpBuf[0] == '%')
						SearchStringAndReplace(pclSelection, pclTmpBuf, pclDataBuffer);

					/* next try, next item */
					ilCurSelectItem++; 
				} while (pclTmpBuf[0] != '%' && ilCurSelectItem < ilSelectItems);
      } else {
				/* field or data? */
	if (!(ilCurElement%2)) {
					dbg(DEBUG, "<HandleDATData> found Field-Element <%s>",  pclDataBuffer);

					/* convert fieldname to uppercase */
					StringUPR((UCHAR*)pclDataBuffer);

					/* if next the select-criteria? */
	  if (!strcmp(pclDataBuffer, "SELECTION")) {
						/* yes, it is */
						ilSelection = TRUE;
	  } else  {

						/* is this field defined in configuration file? */

	    /* if (strstr(prlCmdPtr->prTabDef[0].pcExternalFieldList, pclDataBuffer)){ */
	    /*   if((i = GetIndex(prlCmdPtr->prTabDef[0].pcExternAssign,pclDataBuffer,',')) >= 0){ */
	    /*   strcpy(pclDataBuffer,GetDataField(prlCmdPtr->prTabDef[0].pcInternAssign,i,',')); */
	    /*   } */
	    
							dbg(DEBUG,"<HandleDATData> found this field in configuration");
							/* first,third,fifth,... is always fieldname */
							strcat(pclFieldList, pclDataBuffer);
							strcat(pclFieldList, ",");

							/* timefield-mapping from Localtime to UTC? */
	    if (prlCmdPtr->prTabDef[0].iTimeMapping && strstr(prlCmdPtr->prTabDef[0].pcTimeMappingFields, pclDataBuffer)){
								dbg(DEBUG,"<HandleDATData> Timemapping Localtime to UTC: <%s>", pclDataBuffer);	
								ilTimeMapping = TRUE;
	    } else {
								ilTimeMapping = FALSE;
						}
	    /*	    } else { */
							/* set pointer to next element, incr. counter */
	    /* dbg(DEBUG,"<HandleDATData> can't find this field in configuration"); */

	    /* 	    pclDataBuffer[0] = 0x00; */
	    /* 	    pclDataPtr = CopyNextField(pclDataPtr, clSeparator, pclDataBuffer); */
	    /* 	    ilCurElement++; */
	    /*  	  } */
				}
	} else  {
					dbg(DEBUG, "<HandleDATData> found Data-Element <%s>",  pclDataBuffer);
	  if (ilTimeMapping == TRUE)    {
						dbg(DEBUG,"<HandleDATData> calling LocaltimeToUtc...");
						ilOldDbgLvl = debug_level;
						/* debug_level = TRACE; */

						/* convert Time to UTC */
	    if (strlen(pclDataBuffer) == 14) {
							strcpy(pclTmpTime, s14_BLANKS);
	      if ((ilRC = LocaltimeToUtc(pclTmpTime, pclDataBuffer)) != RC_SUCCESS)  {
								/* error handling */
								dbg(TRACE,"<HandleDATData> LocaltimeToUtc returns: %d - using old timestamp",  ilRC);
	      } else {
								/* copy UTC-Timestamp to buffer */
								strcpy(pclDataBuffer, pclTmpTime);
							}
						}
						debug_level = ilOldDbgLvl;
					}

					/* second,fourth,sixth,... is always data */
					strcat(pclDataList, pclDataBuffer);
					strcat(pclDataList, ",");
				}
			}
		}

		/* check last character... */
		if (pclFieldList[strlen(pclFieldList)-1] == ',')
			pclFieldList[strlen(pclFieldList)-1] = 0x00;
		if (pclDataList[strlen(pclDataList)-1] == ',')
			pclDataList[strlen(pclDataList)-1] = 0x00;

		/* check buffers */
/********************
		if (strlen(pclFieldList) && strlen(pclDataList))
*********************/
		if (strlen(pclFieldList))
		{
	/** Begin**/
	pclResFieldList[0] = 0x00;
	pclResDataBuffer[0] = 0x00;

	CombineData(&(prlCmdPtr->prTabDef[0]),pclFieldList,pclDataList);
	for (ilCombNo=0; ilCombNo<prlCmdPtr->prTabDef[0].iNoCombineRules; ilCombNo++){
	  if (prlCmdPtr->prTabDef[0].pcCombineResultData[ilCombNo] != NULL) {
	    strcat(pclResDataBuffer,prlCmdPtr->prTabDef[0].pcCombineResultData[ilCombNo]);
	    strcat(pclResDataBuffer,",");
	    strcat(pclResFieldList,prlCmdPtr->prTabDef[0].pcCombineResultFields[ilCombNo]);
	    strcat(pclResFieldList,",");
	    free(prlCmdPtr->prTabDef[0].pcCombineResultData[ilCombNo]);
	    prlCmdPtr->prTabDef[0].pcCombineResultData[ilCombNo] = NULL;
	  }
	}

	i = GetNoOfElements(pclFieldList, cCOMMA);

	for(--i;i >= 0; i--){
	  pclPtr = GetDataField(pclFieldList,i, cCOMMA);
	  ilExtIdx = GetIndex(prlCmdPtr->prTabDef[0].pcExternalFieldList,pclPtr,cCOMMA);
	  ilIntIdx = GetIndex(prlCmdPtr->prTabDef[0].pcExternAssign,pclPtr,cCOMMA);

	  if(ilExtIdx >= 0){
	    if(ilIntIdx >= 0){
	      /*get internal assignment */
	      strcat(pclResFieldList,GetDataField(prlCmdPtr->prTabDef[0].pcInternAssign,ilIntIdx,cCOMMA));
	    } else {
	      /* use external field*/
	      strcat(pclResFieldList,pclPtr);
	    }
	    strcat(pclResFieldList,",");

			/* CHANGES BY MOS FOR REMAINING BLANK */
			sprintf(pcldummy,"%s",GetDataField(pclDataList,i, cCOMMA));
			if ((prlCmdPtr->prTabDef[0].iEmptyFieldHandling == iONE_BLANK_REMAINING) || (prlCmdPtr->prTabDef[0].iEmptyFieldHandling == iKEEP_DATA_UNTOUCHED)) {
					if (strcmp(pcldummy,"") == 0) {sprintf(pcldummy," ");}
			}
			 strcat(pclResDataBuffer,pcldummy);
			/* strcat(pclResDataBuffer,GetDataField(pclDataList,i, cCOMMA));*/
			/* END OF CHANGES */
	    strcat(pclResDataBuffer,",");    
	  } else {
	    if(ilIntIdx >= 0){
	      /*get internal assignment*/
	      strcat(pclResFieldList,GetDataField(prlCmdPtr->prTabDef[0].pcInternAssign,ilIntIdx,cCOMMA));
	      strcat(pclResFieldList,",");
	      strcat(pclResDataBuffer,GetDataField(pclDataList,i, cCOMMA));	  
	      strcat(pclResDataBuffer,",");    
	    } 
	  }
	} /* end of for */

	pclResFieldList[strlen(pclResFieldList)-1] = 0x00;
	pclResDataBuffer[strlen(pclResDataBuffer)-1] = 0x00;    

	/** End**/

			/* here we built a correct field and data list */
			dbg(DEBUG,"<HandleDATData> Selection: <%s>", pclSelection);
			dbg(DEBUG,"<HandleDATData> FieldList: <%s>", pclFieldList);
			dbg(DEBUG,"<HandleDATData> DataList : <%s>", pclDataList);
	dbg(DEBUG,"<HandleDATData> FieldList: <%s>", pclResFieldList);
	dbg(DEBUG,"<HandleDATData> DataList : <%s>", pclResDataBuffer);

			/* send this data to configuerd mod-id... */
			/* set BC-Head members */
			memset((void*)&rlBCHead, 0x00, sizeof(BC_HEAD));
			rlBCHead.rc = NETOUT_NO_ACK;
			strncpy(rlBCHead.dest_name, pcgProcessName, 10);
			strncpy(rlBCHead.recv_name, "EXCO", 10);

			/* set CMDBLK members */
			memset((void*)&rlCmdblk, 0x00, sizeof(CMDBLK));
			strncpy(rlCmdblk.command, prlCmdPtr->prTabDef[0].pcSndCmd, 6);
			strncpy(rlCmdblk.obj_name, prlCmdPtr->prTabDef[0].pcTableName, 10);
			sprintf(rlCmdblk.tw_start, "EOD1");
			sprintf(rlCmdblk.tw_end, "%s,%s,%s", prlCmdPtr->pcHomeAirport, prlCmdPtr->pcTableExtension, pcgProcessName);

			/* write request to QUE */
	if ((ilRC = SendToQue(prlCmdPtr->prTabDef[0].iModID, PRIORITY_4, &rlBCHead, &rlCmdblk, pclSelection, pclResFieldList, pclResDataBuffer,NULL,0)) == iCDI_TERMINATE)
			{
				dbg(TRACE,"<HandleInventoryCmd> SendToQue returns <%s>",  RETURN(ilRC));
				dbg(DEBUG,"<HandleInventoryCmd> ----- END -----");
				return ilRC;
			}
		}
		else
			dbg(DEBUG,"<HandleDATData> nothing to send...");
	}

	dbg(DEBUG,"<HandleDATData> ----- END -----");

	/* bye bye */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The HandleInventoryCmd routine                                             */
/******************************************************************************/
static int HandleInventoryCmd(int ipCmdNo)
{
	int					ilRC;
	int					ilCnt;
	int					ilCurTab;
	int					ilOldDbgLvl;
	int					ilCurUpdCmd;
	char					pclTmpKeyWord[iMIN_BUF_SIZE];
	char					pclSelection[2*iMAX_BUF_SIZE];
	char					pclStartTime[15];
	char					pclEndTime[15];
	char					pclTmpTime[15];
	CDIInventory		*prlCDIInventory = NULL;
	BC_HEAD				rlBCHead;
	CMDBLK				rlCmdblk;

	dbg(DEBUG,"<HandleInventoryCmd> ----- START -----");
	
	/* set update service invalid during inventory activity */
	if (igUpdateService == iACTIVE)
	{
		igUpdateService = iNOT_ACTIVE;
		memset((void*)pcgUpdateStartTime, 0x00, 15);
		memset((void*)pcgUpdateEndTime, 0x00, 15);
	}

	/* set pointer we use */
	prlCDIInventory = (CDIInventory*)(pcgTCPReceiveMemory + HEADER_LENGTH(rgTM.iVersion));

	/* copy to local buffer */
	memset((void*)pclStartTime, 0x00, 15);
	memcpy(pclStartTime, prlCDIInventory->pcFromTime, 14);
	memset((void*)pclEndTime, 0x00, 15);
	memcpy(pclEndTime, prlCDIInventory->pcToTime, 14);
	dbg(TRACE,"GOT INV COMMAND <%s> <%s>", pclStartTime,pclEndTime);
	igSndInvCmd = TRUE;
	igRcvInvCmd = FALSE;
	igGotUpdCmd = FALSE;
	dbg(TRACE,"WAITING FOR ANSWER OF CEDA");

	dbg(DEBUG,"<HandleInventoryCmd> received StartTime: <%s> and EndTime: <%s>", pclStartTime, pclEndTime);

	for (ilCurTab=0; ilCurTab<rgTM.prInventoryCmd[ipCmdNo].iNoTable; ilCurTab++)
	{
		/* should i convert timestamp? UTC/LOCAL*/
		if (rgTM.prInventoryCmd[ipCmdNo].prTabDef[ilCurTab].iTimeMapping)
		{
			ilOldDbgLvl = debug_level;
			/* debug_level = TRACE; */

			/* convert StartTime to UTC */
			if (strlen(pclStartTime) == 14)
			{
				strcpy(pclTmpTime, s14_BLANKS);
				if ((ilRC = LocaltimeToUtc(pclTmpTime, pclStartTime)) != RC_SUCCESS)
				{
					/* error handling */
					dbg(TRACE,"<HandleInventoryCmd> LocaltimeToUtc returns: %d - using old timestamp",  ilRC);
				}
				else
				{
					/* copy UTC-Timestamp to buffer */
					strcpy(pclStartTime, pclTmpTime);
				}
			}

			/* convert EndTime to UTC */
			if (strlen(pclEndTime) == 14)
			{
				strcpy(pclTmpTime, s14_BLANKS);
				if ((ilRC = LocaltimeToUtc(pclTmpTime, pclEndTime)) != RC_SUCCESS)
				{
					/* error handling */
					dbg(TRACE,"<HandleInventoryCmd> LocaltimeToUtc returns: %d - using old timestamp",  ilRC);
				}
				else
				{
					/* copy UTC-Timestamp to buffer */
					strcpy(pclEndTime, pclTmpTime);
				}
			}
			debug_level = ilOldDbgLvl;
		}

		dbg(DEBUG,"<HandleInventoryCmd> used StartTime: <%s> and EndTime: <%s>", pclStartTime, pclEndTime);

		/* clear buffer */
		memset((void*)pclSelection, 0x00, 2*iMAX_BUF_SIZE);

		/* copy selection to local buffer */
		strcpy(pclSelection, rgTM.prInventoryCmd[ipCmdNo].prTabDef[ilCurTab].pcSelection);

		/* try to replace keyword, if we use the received data... */
		if (rgTM.prInventoryCmd[ipCmdNo].prTabDef[ilCurTab].iUseReceivedSelection)
		{
			ilCnt = 0;
			memset((void*)pclTmpKeyWord, 0x00, iMIN_BUF_SIZE);
			while ((ilRC = GetAllBetween(rgTM.prInventoryCmd[ipCmdNo].prTabDef[ilCurTab].pcSelection, '\'', ilCnt, pclTmpKeyWord)) == 0)
			{
				if (strstr(pclTmpKeyWord, "START_TIME"))
				{
					(void)SearchStringAndReplace(pclSelection, pclTmpKeyWord, pclStartTime);
				}
				else if (strstr(pclTmpKeyWord, "END_TIME"))
				{
					(void)SearchStringAndReplace(pclSelection, pclTmpKeyWord, pclEndTime);
				}

				/* clear buffer for next read */
				memset((void*)pclTmpKeyWord, 0x00, iMIN_BUF_SIZE);
				
				/* incr counter to get all strings between... */
				ilCnt++;
			}
		}
		dbg(DEBUG,"<HandleInventoryCmd> Selection: <%s>",  pclSelection);

		/* store current selection */
		strcpy(rgTM.prInventoryCmd[ipCmdNo].prTabDef[ilCurTab].pcCurrentSelection, pclSelection);

		/* set BC-Head members */
		memset((void*)&rlBCHead, 0x00, sizeof(BC_HEAD));
		rlBCHead.rc = RC_SUCCESS;
		strncpy(rlBCHead.dest_name, pcgProcessName, 10);
		strncpy(rlBCHead.recv_name, "EXCO", 10);

		/* set CMDBLK members */
		memset((void*)&rlCmdblk, 0x00, sizeof(CMDBLK));
		strncpy(rlCmdblk.command, rgTM.prInventoryCmd[ipCmdNo].prTabDef[ilCurTab].pcSndCmd, 6);
		strncpy(rlCmdblk.obj_name, rgTM.prInventoryCmd[ipCmdNo].prTabDef[ilCurTab].pcTableName, 10);
		sprintf(rlCmdblk.tw_start, "EOD2");
		sprintf(rlCmdblk.tw_end, "%s,%s,%s", rgTM.prInventoryCmd[ipCmdNo].pcHomeAirport, rgTM.prInventoryCmd[ipCmdNo].pcTableExtension, pcgProcessName);

		/* write request to QUE */
		if ((ilRC = SendToQue(rgTM.prInventoryCmd[ipCmdNo].prTabDef[ilCurTab].iModID, PRIORITY_4, &rlBCHead, &rlCmdblk, pclSelection, rgTM.prInventoryCmd[ipCmdNo].prTabDef[ilCurTab].pcInternalFieldList, "",NULL,0)) == iCDI_TERMINATE)
		{
			dbg(TRACE,"<HandleInventoryCmd> SendToQue returns <%s>",  RETURN(ilRC));
			dbg(DEBUG,"<HandleInventoryCmd> ----- END -----");
			return ilRC;
		}
	}

	dbg(DEBUG,"<HandleInventoryCmd> ----- END -----");

	/* bye bye */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The HandleUpdateCmd routine                                                */
/******************************************************************************/
static int HandleUpdateCmd(int ipCmdNo)
{
	int				i;
	int				ilRC;
	int				ilCnt1 = 0;
	int				ilCnt2 = 0;
	int				ilOffset;
	char				*pclS = NULL;
	char				*pclAllValidNo = NULL;
	CDIRecover	rlCDIIn;
	CDIRecover	rlCDIOut;
	CDIUpdate		*prlCDIUpdate = NULL;

	dbg(DEBUG,"<HandleUpdateCmd> ----- START -----");

	/* set pointer we use */
	prlCDIUpdate = (CDIUpdate*)(pcgTCPReceiveMemory + HEADER_LENGTH(rgTM.iVersion));

	/* copy to local buffer */
	memset((void*)pcgUpdateStartTime, 0x00, 15);
	memset((void*)pcgUpdateEndTime, 0x00, 15);
	if (strlen(prlCDIUpdate->pcBegin))
		memcpy(pcgUpdateStartTime, prlCDIUpdate->pcBegin, 14);
	if (strlen(prlCDIUpdate->pcEnd))
		memcpy(pcgUpdateEndTime, prlCDIUpdate->pcEnd, 14);
	/* ---------------------------------------------------------------------*/
	/* check subcommand */
	if (!strncmp(prlCDIUpdate->pcSubCommand, "TIMESTAMP", 9))
	{
		dbg(TRACE,"<HandleUpdateCmd> GOT UPDATE COMMAND ST<%s> ET<%s>",
			   pcgUpdateStartTime,pcgUpdateEndTime);
		if (igHoldQ2 == TRUE)
		{
			dbg(TRACE,"<HandleUpdateCmd> TEST == TEST == TEST == TEST");
			dbg(TRACE,"<HandleUpdateCmd> SECOND QUEUE IS ON HOLD");
		} /* end if */
		else
		{
			ilRC = ReadFromLogTab();
			igGotUpdCmd = TRUE;
			if (igSecondModID > 0)
			{
				dbg(TRACE,"<HandleUpdateCmd> SWITCHED TO SECOND QUEUE %d",igSecondModID);
				dbg(TRACE,"<HandleUpdateCmd> (STORED %d UPDATE EVENTS)",igItemsOnQ2);
  			igActiveQueue = igSecondModID;
			} /* end if */
			dbg(TRACE,"<HandleUpdateCmd> START SENDING UPDATES");
		} /* end else */
		/* only for a valid timestamp */
		if (strlen(pcgUpdateStartTime) == 14)
		{
			memset(&rlCDIIn, 0x00, sizeof(CDIRecover));
			memset(&rlCDIOut, 0x00, sizeof(CDIRecover));
			strncpy(rlCDIIn.pcStartTimeStamp, pcgUpdateStartTime, 14);
			strncpy(rlCDIIn.pcEndTimeStamp, pcgUpdateEndTime, 14);

			/* get it all */
			if (rgTM.iRecoverCounter > 0)
			{
				if ((ilRC = HandleFileRecovery(iALL_TELENO_AT_TIME, &ilCnt1, &rlCDIIn, &rlCDIOut)) != RC_SUCCESS)
				{
					dbg(DEBUG,"<HandleUpdateCmd> HandleFileRecovery returns <%s>",  RETURN(ilRC));
					return ilRC;
				}
			}
		}
	}
	/* ---------------------------------------------------------------------*/
	else if (!strncmp(prlCDIUpdate->pcSubCommand, "SER_NUMBER", 10))
	{
		/* only for a valid telegramnumber */
		/* instead of time its a number */
		if (strlen(pcgUpdateStartTime) == 5)
		{
			memset(&rlCDIIn, 0x00, sizeof(CDIRecover));
			memset(&rlCDIOut, 0x00, sizeof(CDIRecover));
			strncpy(rlCDIIn.pcStartTeleNo, pcgUpdateStartTime, 5);
			strncpy(rlCDIIn.pcEndTeleNo, pcgUpdateEndTime, 5);

			/* get it all */
			if (rgTM.iRecoverCounter > 0)
			{
				if ((ilRC = HandleFileRecovery(iALL_TELENO_AT_NO, &ilCnt1, &rlCDIIn, &rlCDIOut)) != RC_SUCCESS)
				{
					dbg(DEBUG,"<HandleUpdateCmd> HandleFileRecovery returns <%s>",  RETURN(ilRC));
					return ilRC;
				}
			}
		}
	}
	/* ---------------------------------------------------------------------*/
	else
	{
		dbg(TRACE,"<HandleUpdateCmd> unknown SubCommand...");
		ilRC = WriteERRToClient(sWRONG_SUBCOMMAND);
		dbg(DEBUG,"<HandleUpdateCmd> ----- END -----");
		return ilRC;
	}

	/* for all records */
	if (ilCnt1 > 0)
	{
		/* set local pointer to memory with all valid telegram numbers */
		pclAllValidNo = rlCDIOut.pcAllValidNo;

		/* send all valid data telegrams */
		for (i=0; i<ilCnt1; i++)
		{
			/* next valid telegramnumber */
			pclS = GetDataField(pclAllValidNo, i, cCOMMA);

			/* clear structures */
			memset(&rlCDIIn, 0x00, sizeof(CDIRecover));
			memset(&rlCDIOut, 0x00, sizeof(CDIRecover));

			/* try to read this telegram */
			strcpy(rlCDIIn.pcStartTeleNo, pclS);

			/* get this telegram with current telegram number */
			if ((ilRC = HandleFileRecovery(iGET_TELENO_CMD, &ilCnt2, &rlCDIIn, &rlCDIOut)) != RC_SUCCESS)
			{
				dbg(DEBUG,"<HandleUpdateCmd> HandleFileRecovery returns <%s>",  RETURN(ilRC));
				return ilRC;
			}

			/* found item */
			if (ilCnt2 > 0)
			{
				/* copy it to send data memory */
				memcpy(pcgTCPSendMemory, rlCDIOut.pcData, rlCDIOut.iLength);			

				/* write it to protocol file */
				/* ToSProtFile(rlCDIOut.iLength); JWE */
				ilOffset = 0;

				if (rgTM.iSecureOption)
				{
					ilOffset = 2;
					memcpy(pcgTCPSendMemory, SEncode(pcgTCPSendMemory, rlCDIOut.iLength), rlCDIOut.iLength+ilOffset);
					/*ToSCProtFile(rlCDIOut.iLength+ilOffset); JWE */
				}

				/* wait x milliseconds */
				nap(rgTM.lNapTime);

				/* write data to client */
				ilRC = write(rgTM.iTCPWorkSocket,pcgTCPSendMemory,rlCDIOut.iLength+ilOffset);
				if (ilRC != rlCDIOut.iLength+ilOffset)
				{
					dbg(TRACE,"<HandleUpdateCmd> can't write data to client socket! Reseting CDI-Server!");
					dbg(DEBUG,"<HandleUpdateCmd> ----- END -----");

					/* delete memory */
					free (pclAllValidNo);

					/* stop working here */
					return iCDI_RESET;
				}
				else
				{
					/* write it to protocol file */
					ToSProtFile(rlCDIOut.iLength);

					if (rgTM.iSecureOption)
					{
						ilOffset = 2;
						ToSCProtFile(rlCDIOut.iLength+ilOffset);
					}
				}
			}
		}
		/* that's all */
		dbg(DEBUG,"<HandleUpdateCmd> end of recover procedure");
		free(pclAllValidNo);
	}
	igUpdateService = iACTIVE;
	dbg(DEBUG,"<HandleUpdateCmd> ----- END -----");

	/* bye bye */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The HandleFreeQueryCmd routine                                             */
/******************************************************************************/
static int HandleFreeQueryCmd(int ipCmdNo)
{
	int					ilRC;
	int					ilOldDbgLvl;
	int					ilNoTMFields;
	int					ilCurTMField;
	int					ilCurUpdCmd;
	int					ilFieldCnt;
	int					ilCurField;
	int					ilFieldError;
	char					clSeparator;
	char					pclTable[iMIN];
	char					pclFields[iMAXIMUM];
	char					pclTmpSel[2*iMAX_BUF_SIZE];
	char					pclSelection[2*iMAX_BUF_SIZE];
	char					pclSelectBuf[2*iMAX_BUF_SIZE];
	char					pclOrderBy[iMIN_BUF_SIZE];
	char					pclTmpTime[15];
	char					pclTimeStamp[15];
	char					pclOldTimeStamp[15];
	char					*pclS 				= NULL;
	char					*pclPtr				= NULL;
	char					*pclDataPtr			= NULL;
	CDIFreeQuery 		*prlCDIFreeQuery 	= NULL;
	BC_HEAD				rlBCHead;
	CMDBLK				rlCmdblk;

	dbg(DEBUG,"<HandleFreeQueryCmd> ----- START -----");

	/* diasble update service during Free-query command */
	if (igUpdateService == iACTIVE)
	{
		igUpdateService = iNOT_ACTIVE;
		memset((void*)pcgUpdateStartTime, 0x00, 15);
		memset((void*)pcgUpdateEndTime, 0x00, 15);
	}
	
	/* set pointer we use */
	prlCDIFreeQuery = (CDIFreeQuery*)(pcgTCPReceiveMemory + HEADER_LENGTH(rgTM.iVersion));

	/* get separator */
	clSeparator = prlCDIFreeQuery->pcSeparator[0];
	dbg(DEBUG,"<HandleFreeQueryCmd> found Separator: <%c>",  clSeparator);

	/* data-pointer */
	pclDataPtr = prlCDIFreeQuery->pcData;
	dbg(DEBUG,"<HandleFreeQueryCmd> DataPtr: <%s>",  pclDataPtr);

	/* first copy selection to temporary buffer */
	pclDataPtr = &pclDataPtr[strlen(pclDataPtr)-1] - 1;
	memset((void*)pclTmpSel, 0x00, 2*iMAX_BUF_SIZE);
	pclS = pclTmpSel;
	while (*pclDataPtr != clSeparator)
		*pclS++ = *pclDataPtr--;
	StringReverse(pclTmpSel);
	dbg(DEBUG,"<HandleFreeQueryCmd> Tmp-Selection: <%s>",  pclTmpSel);

	/* convert to uppercase */
	StringUPR((UCHAR*)prlCDIFreeQuery->pcData);

	/* copy unchanged data to buffer, if necessary */
	if (strlen(pclTmpSel))
	{
		pclDataPtr++;
		memcpy(pclDataPtr, pclTmpSel, strlen(pclTmpSel));
	}

	/* data-pointer */
	pclDataPtr = prlCDIFreeQuery->pcData;
	dbg(DEBUG,"<HandleFreeQueryCmd> DataPtr: <%s>",  pclDataPtr);

	/* check last character, must be separator... */
	if (pclDataPtr[strlen(pclDataPtr)-1] != clSeparator)
	{
		dbg(DEBUG,"<HandleFreeQuery> missing Separator <%c> at end of datablock...",  clSeparator);
		if ((ilRC = WriteERRToClient(sSEPARATOR_MISSING)) != RC_SUCCESS)
		{
			dbg(TRACE,"<HandleFreeQueryCmd> WriteERRToClient returns: <%s>",  RETURN(ilRC));
			dbg(DEBUG,"<HandleFreeQueryCmd> ----- END -----");
			return ilRC;
		}
		else
		{
			dbg(DEBUG,"<HandleFreeQueryCmd> ----- END -----");
			return RC_SUCCESS;
		}
	}
	else
	{
		/* last separator is ok... */
		/* delete it now, necessary for GetIndex, GetNoOfElements... */
		pclDataPtr[strlen(pclDataPtr)-1] = 0x00;

		/* get index of tablename... */
		if ((ilRC = GetIndex(pclDataPtr, "TABLE", clSeparator)) >= 0)
		{
			/* found table index */
			/* next item is table name... */
			++ilRC;

			/* get table name */
			pclTable[0] = 0x00;
			strcpy(pclTable, GetDataField(pclDataPtr, ilRC, clSeparator));

			/* check length, without TABEND it must be 3 characters */
			if (strlen(pclTable) == 3)
				strcat(pclTable, rgTM.prFreeQueryCmd[ipCmdNo].pcTableExtension);
			dbg(DEBUG,"<HandleFreeQueryCmd> TableName: <%s>",  pclTable);
			
			/* check length of tablename */
			if (strlen(pclTable) != 6)
			{
				dbg(DEBUG,"<HandleFreeQueryCmd> wrong TableName (length)...");
				if ((ilRC = WriteERRToClient(sWRONG_TABLE_LENGTH)) != RC_SUCCESS)
				{
					dbg(TRACE,"<HandleFreeQueryCmd> WriteERRToClient returns: <%s>",  RETURN(ilRC));
					dbg(DEBUG,"<HandleFreeQueryCmd> ----- END -----");
					return ilRC;
				}
				else
				{
					dbg(DEBUG,"<HandleFreeQueryCmd> ----- END -----");
					return RC_SUCCESS;
				}
			}
		}
		else
		{
			dbg(DEBUG,"<HandleFreeQueryCmd> missing TableName...");
			if ((ilRC = WriteERRToClient(sTABLE_MISSING)) != RC_SUCCESS)
			{
				dbg(TRACE,"<HandleFreeQueryCmd> WriteERRToClient returns: <%s>",  RETURN(ilRC));
				dbg(DEBUG,"<HandleFreeQueryCmd> ----- END -----");
				return ilRC;
			}
			else
			{
				dbg(DEBUG,"<HandleFreeQueryCmd> ----- END -----");
				return RC_SUCCESS;
			}
		}

		/* is this table defined in configuration file? */
		if (strcmp(rgTM.prFreeQueryCmd[ipCmdNo].prTabDef[0].pcTableName, pclTable))
		{
			dbg(DEBUG,"<HandleFreeQueryCmd> undefined TableName");
			if ((ilRC = WriteERRToClient(sUNDEFINED_TABLE)) != RC_SUCCESS)
			{
				dbg(TRACE,"<HandleFreeQueryCmd> WriteERRToClient returns: <%s>",  RETURN(ilRC));
				dbg(DEBUG,"<HandleFreeQueryCmd> ----- END -----");
				return ilRC;
			}
			else
			{
				dbg(DEBUG,"<HandleFreeQueryCmd> ----- END -----");
				return RC_SUCCESS;
			}
		}

		/* Fields... */
		if ((ilRC = GetIndex(pclDataPtr, "FIELD", clSeparator)) >= 0)
		{
			/* found field index */
			/* next item are fields ... */
			++ilRC;

			/* get table name */
			pclFields[0] = 0x00;
			strcpy(pclFields, GetDataField(pclDataPtr, ilRC, clSeparator));
			dbg(DEBUG,"<HandleFreeQueryCmd> Fields: <%s>",  pclFields);

			/* check length of fields */
			if (!strlen(pclFields))
			{
				dbg(DEBUG,"<HandleFreeQueryCmd> wrong Fields (length)...");
				if ((ilRC = WriteERRToClient(sWRONG_FIELD_LENGTH)) != RC_SUCCESS)
				{
					dbg(TRACE,"<HandleFreeQueryCmd> WriteERRToClient returns: <%s>",  RETURN(ilRC));
					dbg(DEBUG,"<HandleFreeQueryCmd> ----- END -----");
					return ilRC;
				}
				else
				{
					dbg(DEBUG,"<HandleFreeQueryCmd> ----- END -----");
					return RC_SUCCESS;
				}
			}
		}
		else
		{
			dbg(DEBUG,"<HandleFreeQueryCmd> missing Fields...");
			if ((ilRC = WriteERRToClient(sFIELDS_MISSING)) != RC_SUCCESS)
			{
				dbg(TRACE,"<HandleFreeQueryCmd> WriteERRToClient returns: <%s>",  RETURN(ilRC));
				dbg(DEBUG,"<HandleFreeQueryCmd> ----- END -----");
				return ilRC;
			}
			else
			{
				dbg(DEBUG,"<HandleFreeQueryCmd> ----- END -----");
				return RC_SUCCESS;
			}
		}

		ilFieldCnt = GetNoOfElements(pclFields, cCOMMA);
		for (ilCurField=0, ilFieldError=FALSE; ilCurField<ilFieldCnt && ilFieldError == FALSE; ilCurField++)
		{
			pclS = GetDataField(pclFields, ilCurField, cCOMMA);
			if (!strstr(rgTM.prFreeQueryCmd[ipCmdNo].prTabDef[0].pcExternalFieldList, pclS))
			{
				dbg(DEBUG,"<HandleFreeQueryCmd> cannot find field: <%s>",  pclS);
				ilFieldError = TRUE;
			}
		}

		/* all valid fields? */
		if (ilFieldError == TRUE)
		{
			dbg(DEBUG,"<HandleFreeQueryCmd> undefined Fields");
			if ((ilRC = WriteERRToClient(sUNDEFINED_FIELDS)) != RC_SUCCESS)
			{
				dbg(TRACE,"<HandleFreeQueryCmd> WriteERRToClient returns: <%s>",  RETURN(ilRC));
				dbg(DEBUG,"<HandleFreeQueryCmd> ----- END -----");
				return ilRC;
			}
			else
			{
				dbg(DEBUG,"<HandleFreeQueryCmd> ----- END -----");
				return RC_SUCCESS;
			}
		}

		/* selection */
		if ((ilRC = GetIndex(pclDataPtr, "FILTER", clSeparator)) >= 0)
		{
			/* found selection index */
			/* next item is select-statement... */
			++ilRC;

			/* get table name */
			pclSelectBuf[0] = 0x00;
			strcpy(pclSelectBuf, GetDataField(pclDataPtr, ilRC, clSeparator));

			/* check it */
			pclS = strstr(pclSelectBuf, "WHERE");
			if (pclS == NULL)
				 pclS = strstr(pclSelectBuf, "where");
			if (pclS == NULL)
			{
				/* missing WHERE */
				pclS = strstr(pclSelectBuf, "ORDER");
				if (pclS == NULL)
					pclS = strstr(pclSelectBuf, "order");
				if (pclS != NULL)
				{
					/* order by?, yes */	
					pclOrderBy[0] = 0x00;
					strcpy(pclOrderBy, pclS);
					*(pclS-1) = 0x00;
					sprintf(pclSelection, "WHERE (%s) %s", pclSelectBuf, pclOrderBy);
				}
				else
				{
					/* can't find order by */
					sprintf(pclSelection, "WHERE %s", pclSelectBuf);
				}
			}
			else
			{
				/* find where... */
				strcpy(pclSelection, pclSelectBuf);
			}

			dbg(DEBUG,"<HandleFreeQueryCmd> Selection before time-mapping: <%s>",  pclSelection);

			/* should i convert timestamp? UTC/LOCAL*/
			if (rgTM.prFreeQueryCmd[ipCmdNo].prTabDef[0].iTimeMapping)
			{
				/* count time mapping fields... */
				ilNoTMFields = GetNoOfElements(rgTM.prFreeQueryCmd[ipCmdNo].prTabDef[0].pcTimeMappingFields, cCOMMA);

				/* check all fields... */
				for (ilCurTMField=0; ilCurTMField<ilNoTMFields; ilCurTMField++)
				{
					/* separate next field */
					pclS = GetDataField(rgTM.prFreeQueryCmd[ipCmdNo].prTabDef[0].pcTimeMappingFields, ilCurTMField, cCOMMA);

					/* is this field in selection? */
					if ((pclPtr = strstr(pclSelection, pclS)) != NULL)
					{
						/* init timestamps */
						pclTmpTime[0] = 0x00;
						pclTimeStamp[0] = 0x00;
						pclOldTimeStamp[0] = 0x00;

						/* get next timestamp... */
						(void)GetAllBetween(pclPtr, '\'', 0, pclTimeStamp);

						/* convert StartTime to UTC */
						if (strlen(pclTimeStamp) == 14)
						{
							/* save current time stamp */
							strcpy(pclOldTimeStamp, pclTimeStamp);

							/* set debug level to TRACE and save old... */
							ilOldDbgLvl = debug_level;
							/* debug_level = TRACE; */

							strcpy(pclTmpTime, s14_BLANKS);
							if ((ilRC = LocaltimeToUtc(pclTmpTime, pclTimeStamp)) != RC_SUCCESS)
							{
								/* error handling */
								dbg(TRACE,"<HandleFreeQueryCmd> LocaltimeToUtc returns: %d - using old timestamp",  ilRC);
							}
							else
							{
								/* copy UTC-Timestamp to buffer */
								strcpy(pclTimeStamp, pclTmpTime);
							}

							/* set old debug level */
							debug_level = ilOldDbgLvl;

							/* set neu time stamp in selection */
							(void)SearchStringAndReplace(pclSelection, pclOldTimeStamp, pclTimeStamp);
						}
					}
				}
			}
			dbg(DEBUG,"<HandleFreeQueryCmd> Selection after time-mapping: <%s>",  pclSelection);
		}

		/* store current selection */
		strcpy(rgTM.prFreeQueryCmd[ipCmdNo].prTabDef[0].pcCurrentSelection, pclSelection);
		dbg(DEBUG,"<HandleFreeQueryCmd> Selection <%s>",  pclSelection);

		/* set BC-Head members */
		memset((void*)&rlBCHead, 0x00, sizeof(BC_HEAD));
		rlBCHead.rc = RC_SUCCESS;
		strncpy(rlBCHead.dest_name, pcgProcessName, 10);
		strncpy(rlBCHead.recv_name, "EXCO", 10);

		/* set CMDBLK members */
		memset((void*)&rlCmdblk, 0x00, sizeof(CMDBLK));
		strncpy(rlCmdblk.command, rgTM.prFreeQueryCmd[ipCmdNo].prTabDef[0].pcSndCmd, 6);
		strncpy(rlCmdblk.obj_name, rgTM.prFreeQueryCmd[ipCmdNo].prTabDef[0].pcTableName, 10);
		sprintf(rlCmdblk.tw_start, "EOD3");
		sprintf(rlCmdblk.tw_end, "%s,%s,%s", rgTM.prFreeQueryCmd[ipCmdNo].pcHomeAirport, rgTM.prFreeQueryCmd[ipCmdNo].pcTableExtension, pcgProcessName);

		/* write request to QUE */
		if ((ilRC = SendToQue(rgTM.prFreeQueryCmd[ipCmdNo].prTabDef[0].iModID, PRIORITY_4, &rlBCHead, &rlCmdblk, pclSelection, rgTM.prFreeQueryCmd[ipCmdNo].prTabDef[0].pcInternalFieldList, "",NULL,0)) == iCDI_TERMINATE)
		{
			dbg(TRACE,"<HandleFreeQueryCmd> SendToQue returns <%s>",  RETURN(ilRC));
			dbg(DEBUG,"<HandleFreeQueryCmd> ----- END -----");
			return ilRC;
		}
	}

	/* set the flags and current (actual) field list */
	rgTM.prFreeQueryCmd[ipCmdNo].prTabDef[0].iNoCurExternalFieldList = ilFieldCnt;
	strcpy(rgTM.prFreeQueryCmd[ipCmdNo].prTabDef[0].pcCurExternalFieldList, pclFields);
	dbg(DEBUG,"<HandleFreeQueryCmd> setting NoCurExteralFieldList: %d", rgTM.prFreeQueryCmd[ipCmdNo].prTabDef[0].iNoCurExternalFieldList);
	dbg(DEBUG,"<HandleFreeQueryCmd> setting CurExteralFieldList: <%s>", rgTM.prFreeQueryCmd[ipCmdNo].prTabDef[0].pcCurExternalFieldList);

	dbg(DEBUG,"<HandleFreeQueryCmd> ----- END -----");
	/* bye bye */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The HandleInternalCmd routine                                             */
/******************************************************************************/
static int HandleInternalCmd(int ipCmdNo)
{
	int			ilRC;
	int			ilOffset;
	char			clAC;
	char			pclTeleNo[6];
	char			pclErrorCode[3];
	CDICheck		*prlCDICheck	 = NULL;
	CDIError		*prlCDIError	 = NULL;
	CDIDBLocked	*prlCDIDBLocked = NULL;

	dbg(DEBUG,"<HandleInternalCmd> ----- START -----");

	/* --------------------------------------------------------------------*/
	/* --------------------------------------------------------------------*/
	/* --------------------------------------------------------------------*/
	if (!strncmp(rgTM.prInternalCmd[ipCmdNo].pcCmd, "CCO", 3))
	{
		dbg(DEBUG,"<HandleCCO> ----- START <CCO> -----");

		prlCDICheck = (CDICheck*)(pcgTCPReceiveMemory + HEADER_LENGTH(rgTM.iVersion));
		clAC = prlCDICheck->pcData[0];
		dbg(DEBUG,"<HandleCCO> found AC-Sign: <%c>", clAC);

		if (clAC == 'C')
		{
			/* this is a check */
			/* create the CCO-Structure */
			if ((ilRC = BuildCCO("A")) != RC_SUCCESS)
			{
				dbg(TRACE,"<CheckCCO> BuildCCO returns %d", ilRC);
				dbg(DEBUG,"<CheckCCO> ----- END -----");
				return iCDI_TERMINATE;
			}

			/* telegramnumber must be the same we received */
			ChangeHeader((CDI2Header*)pcgTCPSendMemory, iTELE_NUMBER, VERSION(rgTM.iVersion, ((CDI2Header*)pcgTCPReceiveMemory)->pcTeleNo, ((CDI1Header*)pcgTCPReceiveMemory)->pcTeleNo));

			ilOffset = 0;

			if (rgTM.iSecureOption)
			{
				ilOffset = 2;
	      memcpy(pcgTCPSendMemory, SEncode(pcgTCPSendMemory, CCO_LENGTH(rgTM.iVersion)+rgTM.iAddLFatEnd), CCO_LENGTH(rgTM.iVersion)+ilOffset+rgTM.iAddLFatEnd);
	      /*ToSCProtFile(CCO_LENGTH(rgTM.iVersion)+ilOffset+rgTM.iAddLFatEnd);  JWE */
			}

			/* ... and then to socket... */
			/* write it to socket */
	  	ilRC = write(rgTM.iTCPWorkSocket, pcgTCPSendMemory, CCO_LENGTH(rgTM.iVersion)+ilOffset+rgTM.iAddLFatEnd);
ilRC = CCO_LENGTH(rgTM.iVersion)+ilOffset+rgTM.iAddLFatEnd;
	  	if (ilRC != CCO_LENGTH(rgTM.iVersion)+ilOffset+rgTM.iAddLFatEnd)
			{
				dbg(TRACE,"<CheckCCO> can't write to client socket (%d)", ilRC);
				dbg(DEBUG,"<CheckCCO> ----- END -----");
				return iCDI_RESET;
			}
			else
			{
				/* Write it to logfile... */
				ToSProtFile(CCO_LENGTH(rgTM.iVersion)+rgTM.iAddLFatEnd); 

				if (rgTM.iSecureOption)
				{
					ilOffset = 2;
					ToSCProtFile(CCO_LENGTH(rgTM.iVersion)+ilOffset+rgTM.iAddLFatEnd);
				}
			}
		}
		else if (clAC == 'A')
		{
			/* this is a answer */
			/* check telegramnumber, it must be the same we wrote tp client */
			memcpy(pclTeleNo, VERSION(rgTM.iVersion, ((CDI2Header*)pcgTCPReceiveMemory)->pcTeleNo, ((CDI1Header*)pcgTCPReceiveMemory)->pcTeleNo), 5);
			pclTeleNo[5] = 0x00;

			dbg(DEBUG,"ERRCN: Version: %d LastCCONo %d TeleNo %d/<%s>",rgTM.iVersion,igLastCCONo,atoi(pclTeleNo),pclTeleNo);

			if (igLastCCONo != atoi(pclTeleNo))
			{
			dbg(DEBUG,"ERRCN: Version: %d LastCCONo %d TeleNo %d/<%s>",rgTM.iVersion,igLastCCONo,atoi(pclTeleNo),pclTeleNo);
				/* write an error to client... */
				if ((ilRC = WriteERRToClient(sCCO_TNUMBER)) != RC_SUCCESS)
					return ilRC;
			}
		}
		else
		{
			/* this is an error!!! */
			/* write an error to client... */
			if ((ilRC = WriteERRToClient(sAC_ERROR)) != RC_SUCCESS)
				return ilRC;
		}

		dbg(DEBUG,"<HandleCCO> ----- END <CCO> -----");
	}
	/* --------------------------------------------------------------------*/
	/* --------------------------------------------------------------------*/
	/* --------------------------------------------------------------------*/
	else if (!strncmp(rgTM.prInternalCmd[ipCmdNo].pcCmd, "ERR", 3))
	{
		dbg(DEBUG,"<HandleERR> ----- START <ERR> -----");

		prlCDIError = (CDIError*)(pcgTCPReceiveMemory + HEADER_LENGTH(rgTM.iVersion));
		memcpy(pclErrorCode, prlCDIError->pcData, 2);
		pclErrorCode[2] = 0x00;

		dbg(DEBUG,"<HandleERR> found ErrorCode: <%s>", pclErrorCode);
		if (!strcmp(pclErrorCode, sWRONG_SERVERSYSTEM) 	||
			 !strcmp(pclErrorCode, sUNKNOWN_SENDER) 		||
			 !strcmp(pclErrorCode, sUNKNOWN_RECEIVER) 	||
			 !strcmp(pclErrorCode, sWRONG_TIMESTAMP) 		||
			 !strcmp(pclErrorCode, sWRONG_LENGTH) 			||
			 !strcmp(pclErrorCode, sUNKNOWN_CLASS)			||
			 !strcmp(pclErrorCode, sWRONG_NUMBER))
		{
			dbg(DEBUG,"<HandleERR> ----- END <ERR> -----");
			return iCDI_RESET;	
		}

		dbg(DEBUG,"<HandleERR> ----- END <ERR> -----");
	}
	/* --------------------------------------------------------------------*/
	/* --------------------------------------------------------------------*/
	/* --------------------------------------------------------------------*/
	else if (!strncmp(rgTM.prInternalCmd[ipCmdNo].pcCmd, "END", 3))
	{
		dbg(DEBUG,"<HandleEND> ----- START <END> -----");
		dbg(DEBUG,"<HandleEND> ----- END <END> -----");
		igNoENDCommand = 1;
		return iCDI_RESET;
	}
	/* --------------------------------------------------------------------*/
	/* --------------------------------------------------------------------*/
	/* --------------------------------------------------------------------*/
	else if (!strncmp(rgTM.prInternalCmd[ipCmdNo].pcCmd, "EOD", 3))
	{
		dbg(DEBUG,"<HandleEOD> ----- START <EOD> -----");
		dbg(DEBUG,"<HandleEOD> ----- END <EOD> -----");
	}
	/* --------------------------------------------------------------------*/
	/* --------------------------------------------------------------------*/
	/* --------------------------------------------------------------------*/
	else if (!strncmp(rgTM.prInternalCmd[ipCmdNo].pcCmd, "DBL", 3))
	{
		dbg(DEBUG,"<HandleDBL> ----- START <DBL> -----");
		prlCDIDBLocked = (CDIDBLocked*)(pcgTCPReceiveMemory + HEADER_LENGTH(rgTM.iVersion));
		dbg(DEBUG,"<HandleDBL> found Command: <%s>", prlCDIDBLocked->pcCommand);
		dbg(DEBUG,"<HandleDBL> ----- END <DBL> -----");
	}

	dbg(DEBUG,"<HandleInternalCmd> ----- END -----");
	/* bye bye */
	return RC_SUCCESS;
}

/******************************************************************************/
/* the UtcToLocal routine                                                     */
/******************************************************************************/
static int UtcToLocal(char *pcpTime)
{
	struct tm *_tm;
	time_t    now;
	char	  _tmpc[6];
	int hour_gm,hour_local;
	int UtcDifference;

	now = time(NULL);
	_tm = (struct tm *)gmtime(&now);
	hour_gm = _tm->tm_hour;
	_tm = (struct tm *)localtime(&now);
	hour_local = _tm->tm_hour;
	if (hour_gm > hour_local)
	{
		UtcDifference = (hour_local+24-hour_gm)*3600;
	}
	else
	{
		UtcDifference = (hour_local-hour_gm)*3600;
	}

	if (strlen(pcpTime) < 12 )
	{
		return (time_t) 0;
	} /* end if */

	now = time(0L);
	_tm = (struct tm *)localtime(&now);

	_tmpc[2] = '\0';
	strncpy(_tmpc,pcpTime+12,2);
	_tm -> tm_sec = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+10,2);
	_tm -> tm_min = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+8,2);
	_tm -> tm_hour = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+6,2);
	_tm -> tm_mday = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+4,2);
	_tm -> tm_mon = atoi(_tmpc)-1;
	strncpy(_tmpc,pcpTime,4);
	_tmpc[4] = '\0';
	_tm -> tm_year = atoi(_tmpc)-1900;
	_tm -> tm_wday = 0;
	_tm -> tm_yday = 0;

	now = mktime(_tm) + UtcDifference;
	_tm = (struct tm *)localtime(&now);
	sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
		_tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
		_tm->tm_min,_tm->tm_sec);

	return RC_SUCCESS;
}

/******************************************************************************/
/* the SendToQue routine                                                      */
/******************************************************************************/
static int SendToQue(int ipModID, int ipPrio, BC_HEAD *prpBCHead, 
							CMDBLK *prpCmdblk, char *pcpSelection, 
							char *pcpFields, char *pcpData,char *pcpAddStruct,int ipAddLen )
{
	int			ilRC;
	int			ilLen;
	EVENT			*prlOutEvent 	= NULL;
	BC_HEAD		*prlOutBCHead 	= NULL;
	CMDBLK		*prlOutCmdblk	= NULL;

	dbg(DEBUG,"<SendToQue> ----- START -----");

	/* calculate size of memory we need */
	ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
			 strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + ipAddLen + 20; 

	/* get memory for out event */
	if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
	{
		dbg(TRACE,"<SendToQue> malloc failed, can't allocate %d bytes",  ilLen);
		dbg(DEBUG,"<SendToQue> ----- END -----");
		return iCDI_TERMINATE;
	}

	/* clear buffer */
	memset((void*)prlOutEvent, 0x00, ilLen);

	/* set structure members */
	prlOutEvent->type 		 = SYS_EVENT;
	prlOutEvent->command 	 = EVENT_DATA;
	prlOutEvent->originator  = mod_id;
	prlOutEvent->retry_count = 0;
	prlOutEvent->data_offset = sizeof(EVENT);
	prlOutEvent->data_length = ilLen - sizeof(EVENT);

	/* BCHead members */
	prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
	memcpy(prlOutBCHead, prpBCHead, sizeof(BC_HEAD));

	/* CMDBLK members */
	prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
	memcpy(prlOutCmdblk, prpCmdblk, sizeof(CMDBLK));

	/* Selection */
	strcpy(prlOutCmdblk->data, pcpSelection);

	/* fields */
	strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1, pcpFields);

	/* data */
	strcpy(prlOutCmdblk->data+strlen(pcpSelection)+strlen(pcpFields)+2, pcpData);

	/* additional,optional info (e.g. FTPHDL-info) */
	if (pcpAddStruct != NULL)
	{
		memcpy((prlOutCmdblk->data +
					(strlen(pcpSelection)+1) +
					(strlen(pcpFields)+1)) + 
					(strlen(pcpData)+1),pcpAddStruct,ipAddLen);
	}

	/* send this message */
	dbg(DEBUG,"<SendToQue> sending to Mod-ID: %d", ipModID);
	if ((ilRC = que(QUE_PUT, ipModID, mod_id, ipPrio, ilLen, (char*)prlOutEvent)) != RC_SUCCESS)
	{
		dbg(TRACE,"<SendToQue> QUE_PUT returns: %d",  ilRC);
		return iCDI_TERMINATE;
	}

	/* delete memory */
	free((void*)prlOutEvent);

	dbg(DEBUG,"<SendToQue> ----- END -----");

	/* bye bye */
	return RC_SUCCESS;
}

/******************************************************************************/
/* the BuildDATData routine                                                   */
/******************************************************************************/
static int BuildDATData(char *pcpFieldList, char *pcpData, CDITable *prpTabPtr)
{
	int	ilRC;
	int	ilFound;
	int	ilCombNo;
	int	ilExternalCnt;
	int	ilLocalViaN;
	int	ilAssignCnt;
	int	ilCurAssign;
	int	ilValidField;
	char	pclCurAssign[iMIN_BUF_SIZE];
	char	pclLocalViaL[iMAX_BUF_SIZE*2];
	char	pclLocalViaN[iMIN];
	char	pclFieldName[iMIN];
	char	pclInternalField[iMIN];
	char	pclExternalField[iMIN];
	char	pclLocalDataBuffer[iMAXIMUM];
	char	*pclHdrPtr = NULL;
	char	*pclTmpPtr = NULL;
        char    pclTmpTime[16];

	/*
	dbg(DEBUG,"<BDATD> ----- START -----");
	*/

	/* initialize something */
	pclLocalViaN[0] = 0x00;
	pclLocalViaL[0] = 0x00;
	ilLocalViaN = 0;
	pclHdrPtr = (char*)((CDIDataTelegram*)pcgTCPSendMemory)->pcData+HEADER_LENGTH(rgTM.iVersion);

	/* first of all, copy VIAL and VIAN to local buffers... */
	if ((ilRC = GetIndex(pcpFieldList, "VIAN", cCOMMA)) >= 0)
	{
		/* here we know position of VIAN */
		strcpy(pclLocalViaN, GetDataField(pcpData, ilRC, cCOMMA));

		/* convert to integer */
		ilLocalViaN = atoi(pclLocalViaN);			
	}

	/* this is VIAL */
	if ((ilRC = GetIndex(pcpFieldList, "VIAL", cCOMMA)) >= 0)
	{
		/* here we know position of VIAN */
		strcpy(pclLocalViaL, GetDataField(pcpData, ilRC, cCOMMA));
	}

	/* if we want to combine data from many fields */
	if ((ilRC = CombineData(prpTabPtr, pcpFieldList, pcpData)) != RC_SUCCESS)
	{
		dbg(TRACE,"<BDATD> CombineData returns: <%d>", ilRC);
		return RC_FAIL;
	}

	if (strstr(pcpFieldList,"LURN") != NULL)
	{
		strcat(prpTabPtr->pcExternalFieldList,",LURN");
		prpTabPtr->iNoExternalFields += 1;
		strcat(prpTabPtr->pcInternalFieldList,",LURN");
		prpTabPtr->iNoInternalFields += 1;
        }

	/* here we build telegram */
	for (ilExternalCnt=0; ilExternalCnt<(prpTabPtr->iNoCurExternalFieldList > 0 ? prpTabPtr->iNoCurExternalFieldList : prpTabPtr->iNoExternalFields); ilExternalCnt++)
	{
		/* init it */
		pclLocalDataBuffer[0] = 0x00;
		ilValidField = FALSE;

		/* get current fieldname */
		strcpy(pclFieldName, GetDataField((prpTabPtr->iNoCurExternalFieldList > 0 ? prpTabPtr->pcCurExternalFieldList : prpTabPtr->pcExternalFieldList), ilExternalCnt, cCOMMA));

      /* is the field a combined data field? */
      for (ilCombNo=0, ilFound=-1; ilCombNo<prpTabPtr->iNoCombineRules && ilFound == -1; ilCombNo++){
	if (!strcmp(pclFieldName, prpTabPtr->pcCombineResultFields[ilCombNo])) {
				ilFound = ilCombNo;
				ilValidField = TRUE;
			}
		}

		/* check via */
      if (strncmp(pclFieldName, "VI0", 3) || pclFieldName[3] < '1' || pclFieldName[3] > '9') {
			/* this is a 'normal' Field, this means it's not VIA1,VIA2,....VIA9 */
	if (ilFound == -1) {
				/* count assignments */
				ilAssignCnt = prpTabPtr->iFieldTableAssignment;
	  for (ilCurAssign=0; ilCurAssign<ilAssignCnt && ilFound == -1; ilCurAssign++){
					strcpy(pclCurAssign, GetDataField(prpTabPtr->pcFieldTableAssignment , ilCurAssign, cCOMMA));

					pclInternalField[0] = 0x00;
					pclExternalField[0] = 0x00;
					(void)SeparateIt(pclCurAssign, pclInternalField, pclExternalField, '=');
	    if (!strcmp(pclFieldName, pclExternalField)) {
	      if ((ilRC = GetIndex(pcpFieldList, pclInternalField, cCOMMA)) >= 0){
							strcpy(pclLocalDataBuffer, CDIGetDataField(pcpData, ilRC, cCOMMA, prpTabPtr->iEmptyFieldHandling));
							ilValidField = TRUE;
						}
						ilFound = 1;	
					}
				}

	  if (ilFound == -1){
					/* in this case there isn't a valid assignment */
	    if ((ilRC = GetIndex(pcpFieldList, pclFieldName, cCOMMA)) >= 0){
						strcpy(pclLocalDataBuffer, CDIGetDataField(pcpData, ilRC, cCOMMA, prpTabPtr->iEmptyFieldHandling));
						ilValidField = TRUE;
					}
				}
	} else {
	  if(prpTabPtr->pcCombineResultData[ilFound] == NULL){
	    ilValidField = FALSE;
	  } else {
				strcpy(pclLocalDataBuffer, prpTabPtr->pcCombineResultData[ilFound]);
	    free(prpTabPtr->pcCombineResultData[ilFound]);
	    prpTabPtr->pcCombineResultData[ilFound] = NULL;
	  }
	}
	if (ilValidField == TRUE ){
			/* is this a field for timemapping? */
	  if (prpTabPtr->iTimeMapping && strstr(prpTabPtr->pcTimeMappingFields, pclFieldName)) {

              if(prpTabPtr->iTimeMappingLocal2Utc){
                  /*HEB: must map Utc2Local*/
                  if (strlen(pclLocalDataBuffer) == 14){
                  /*strcpy(pclTmpTime, s14_BLANKS);*/
                  if ((ilRC = LocalToUtc(pclLocalDataBuffer)) != RC_SUCCESS){
					dbg(TRACE,"<BDATD> LocalToUtc returns: %d",  ilRC);
					return RC_FAIL;
				}
                  /*strcpy(pclLocalDataBuffer,pclTmpTime);*/
                  }
              } else {
				/* must map from UTC to LOCALTIME... */
				/* mapping function produced by MCU */
                    if ((ilRC = UtcToLocal(pclLocalDataBuffer)) != RC_SUCCESS){
					dbg(TRACE,"<BDATD> UtcToLocal returns: %d",  ilRC);
					return RC_FAIL;
                    }
              }
	    }
        }
      } else {
			/* this is VIA1 through VIA9 */
			strncpy(pclLocalDataBuffer, GetViaByNumber(pclLocalViaL, ilLocalViaN, atoi(&pclFieldName[3]), prpTabPtr->iVia34), prpTabPtr->iVia34);
			pclLocalDataBuffer[prpTabPtr->iVia34] = 0x00;
			ilValidField = TRUE;
		}

      /* if (pclLocalDataBuffer == -1)*/
		if (ilValidField == TRUE && (!prpTabPtr->iIgnoreEmptyFields || (prpTabPtr->iIgnoreEmptyFields && strlen(pclLocalDataBuffer))))
		{
			/* attach to header */
			strcat(pclHdrPtr, pclFieldName);
			strcat(pclHdrPtr, rgTM.pcSeparator);
			/* PRF 6260: cleaning data from unallowed chars like the separator itself */
			CleanData(pclLocalDataBuffer,0,rgTM.pcSeparator[0],' ');
			/* Convert Server Chars to Client Chars */
			ChangeCharFromTo(pclLocalDataBuffer,pcgServerChars,pcgClientChars);
			strcat(pclHdrPtr, pclLocalDataBuffer);
			strcat(pclHdrPtr, rgTM.pcSeparator);
		}
    }/* end of for*/

	pclTmpPtr = strstr(prpTabPtr->pcExternalFieldList,",LURN");
	if (pclTmpPtr != NULL)
	{
		*pclTmpPtr = '\0';
		prpTabPtr->iNoExternalFields -= 1;
	}
	pclTmpPtr = strstr(prpTabPtr->pcInternalFieldList,",LURN");
	if (pclTmpPtr != NULL)
	{
		*pclTmpPtr = '\0';
		prpTabPtr->iNoInternalFields -= 1;
	}

	/*
	dbg(DEBUG,"<BDATD> ----- END -----");
	*/
	return RC_SUCCESS;
}

/******************************************************************************/
/* the BuildDELData routine                                                   */
/******************************************************************************/
static int BuildDELData(char *pcpFieldList, char *pcpData, CDITable *prpTabPtr)
{
	/* that's enough */
	return BuildDATData(pcpFieldList, pcpData, prpTabPtr); 
}

/******************************************************************************/
/* the PrepareTelegram routine                                                */
/******************************************************************************/
static void PrepareTelegram(long lpUniqueNumber, 
									 char *pcpUrno, CDITable *prpCDITable)
{

	/* answer check connection call */
	memset((void*)pcgTCPSendMemory, 0x00, iMAX_DATA);

	/* build header for command */
	if (prpCDITable->iTelegramType == iDAT_CLASS || prpCDITable->iTelegramType == iINS_CLASS){
            dbg(TRACE,"old-UniqueNumber for DAT <%d>, new-UniqueNumber <%d>", lgUniqueDATNumber, lpUniqueNumber);
            if((lpUniqueNumber-lgUniqueDATNumber) > 1){
                dbg(TRACE,"!!!!!!!!!!!!!!! Number-Sequence for DAT is broken !!!!!!!!!!!");
            }
            BuildHeader((CDI2Header*)pcgTCPSendMemory, "DAT", lpUniqueNumber);
            lgUniqueDATNumber = lpUniqueNumber;
        }
	else
		BuildHeader((CDI2Header*)pcgTCPSendMemory, "DEL", lpUniqueNumber);

	/* current timestamp */
	memmove(((CDIDataTelegram*)pcgTCPSendMemory)->pcTimeStamp+HEADER_LENGTH(rgTM.iVersion), GetTimeStamp(), 14);

	/* source, is empty */
	memmove(((CDIDataTelegram*)pcgTCPSendMemory)->pcSource+HEADER_LENGTH(rgTM.iVersion), "   ", 3);

	/* data type */
	memmove(((CDIDataTelegram*)pcgTCPSendMemory)->pcDataType+HEADER_LENGTH(rgTM.iVersion), prpCDITable->pcDataType, 2);

	/* initialize key */
	if (rgTM.iKey_default == 0)
		memset(((CDIDataTelegram*)pcgTCPSendMemory)->pcKey+HEADER_LENGTH(rgTM.iVersion), '0', 25);
	if (rgTM.iKey_default == 1)
		memset(((CDIDataTelegram*)pcgTCPSendMemory)->pcKey+HEADER_LENGTH(rgTM.iVersion), ' ', 25);

	/* and set key (that's URNO) */
	/* copy the urno right justified */
	if (rgTM.iKey_adjustment == 0)
		memmove(((CDIDataTelegram*)pcgTCPSendMemory)->pcKey+HEADER_LENGTH(rgTM.iVersion)+25-strlen(pcpUrno), pcpUrno, strlen(pcpUrno));
	/* copy the urno left justified */
	if (rgTM.iKey_adjustment == 1)
		memmove(((CDIDataTelegram*)pcgTCPSendMemory)->pcKey+HEADER_LENGTH(rgTM.iVersion), pcpUrno, strlen(pcpUrno));

	/* separator */
	memmove(((CDIDataTelegram*)pcgTCPSendMemory)->pcSeparator+HEADER_LENGTH(rgTM.iVersion), rgTM.pcSeparator, 1);

	/* everything is fine */
	return;
}

/******************************************************************************/
/* the SendFile routine                                                       */
/******************************************************************************/
static int SendFile(struct _CDICmd *prpCDICmd, CDITable *prpCDITable)
{
	int					ilRC;
	int					ilRc;
	int					ilStatus;
	int					ilChildPid;	
	int					ilBreakOut;
	int	ilSendEOD = FALSE;
	time_t				tlTime1;
	time_t 				tlTime2;
	char					pclTmpBuf[2*iMIN_BUF_SIZE];
	struct itimerval	rlTimeVal;
	FILE					*pFh = NULL;
	char 					*pclS = NULL;

	dbg(TRACE,"<SendFile> ----- START -----");
	if (prpCDICmd->iUseFtp)	
	{
		dbg(TRACE,"<SendFile> ----- BUILD FILE -----");
		if (prpCDICmd->pFh == NULL)
		{
			if ((ilRC = BuildFile(prpCDICmd)) != RC_SUCCESS)
				return ilRC;
		}

		/* close the file here */
		sprintf(pclTmpBuf,"%s/%s", prpCDICmd->pcLocalFilePath,prpCDICmd->pcFileName);
		dbg(TRACE,"<SendFile> close file: <%s>", pclTmpBuf);
		fclose(prpCDICmd->pFh);
		prpCDICmd->pFh = NULL;

		/* use internal ftp mode, not the very secure one */
		if (rgTM.iUseFtphdl == 0)
		{ 
			dbg(TRACE,"<SendFile> ----- SEND FILE <internal-FTP-mode> -----");
			ilSendEOD = TRUE;
			/* produce ftp-control file */
			if ((pFh = fopen(rgTM.pcFTPCtrlFile, "w")) == NULL)
			{
				dbg(TRACE,"<SendFile> fopen <%s> return NULL", rgTM.pcFTPCtrlFile);
				return iCDI_RESET;
			}

			/* for login at remote machine... */
			/* decode password */
			pclS = rgTM.pcFTPPass;
			CDecode(&pclS);
			strcpy(rgTM.pcFTPPass, pclS);
			fprintf(pFh, "user %s %s\n", rgTM.pcFTPUser, rgTM.pcFTPPass);
			fprintf(pFh, "bin\n");
			if (strlen(prpCDICmd->pcRemoteFilePath) > 0)
				fprintf(pFh, "cd %s\n", prpCDICmd->pcRemoteFilePath);
			fprintf(pFh, "put %s/%s %s\n", 
					prpCDICmd->pcLocalFilePath, prpCDICmd->pcFileName, prpCDICmd->pcRFileName);
			fprintf(pFh, "bye");
			fclose(pFh);
			/* Encode password */
			pclS = rgTM.pcFTPPass;
			CEncode(&pclS);
			strcpy(rgTM.pcFTPPass, pclS);

			/* produce tmp buffer */
			pclTmpBuf[0] = 0x00;
			sprintf(pclTmpBuf, "ftp -n %s < %s 2>&1 > %s/%s_ftp.log", rgTM.pcMachine, rgTM.pcFTPCtrlFile, getenv("TMP_PATH"), pcgProcessName);
			dbg(DEBUG,"<SendFile> TmpBuf: <%s>", pclTmpBuf);

			/* create child process */
			dbg(DEBUG,"<SendFile> try to create child process...");

			/* check return code */
			switch (ilChildPid = fork())
			{
				case 0:
					/* if I'm child this is my return value from fork (0) */
					/* the new (child) process */
					errno = 0;
					ilRC = execlp(rgTM.pcShell, rgTM.pcShell, pclTmpBuf, NULL);
					dbg(TRACE,"<SendFile> execlp returns <%s>", strerror(errno));
					exit(0);
					break;

				case -1:
					/* this is an fork error */
					dbg(TRACE,"<SendFile> fork returns -1...");
					return iCDI_TERMINATE;

				default:
					/* the parent process */
					dbg(DEBUG,"<SendFile> child process PID: %d", ilChildPid);
					break;
			}
			dbg(DEBUG,"<SendFile> fork returns process-ID: %d", ilChildPid);

			/* set start time */
			tlTime1 = time(NULL);
			ilBreakOut = FALSE;

			do
			{
				/* wait for ftp-end... */
				igSignalNo = 0;

				/* set interrupt (SIGALRM) to 10 ms */
				rlTimeVal.it_interval.tv_sec = 0;
				rlTimeVal.it_interval.tv_usec = 0;
				rlTimeVal.it_value.tv_sec = 0;
				rlTimeVal.it_value.tv_usec = 10000;
				setitimer(ITIMER_REAL, &rlTimeVal, NULL);

				/* wait for death of the child */
				(void)wait(&ilStatus);

				/* reset alarm */
				alarm(0);

				dbg(DEBUG,"<SendFile> calling CheckTCPQuickly!");
				/* check TCP/IP-Port, necessary for CCO's... */
				if ((ilRC = CheckTCPQuickly()) != RC_SUCCESS)
					ilBreakOut = TRUE;
				
				/* check time difference */
				tlTime2 = time(NULL);
				if (difftime(tlTime2, tlTime1) > (double)90.0)
					ilBreakOut = TRUE;

			} while (igSignalNo == SIGALRM && ilBreakOut == FALSE);

			/* check child process */
			if (igSignalNo == SIGALRM)
			{
				dbg(DEBUG,"<SendFile> killing my child: %d", ilChildPid);
				errno = 0;
				if ((ilRC = kill(ilChildPid, SIGKILL)) != RC_SUCCESS)
				{
					/* remove control file */
					remove(rgTM.pcFTPCtrlFile);
					dbg(TRACE,"<SendFile> kill returns: Error <%d>=<%s>",errno,strerror(errno));
					dbg(TRACE,"<SendFile> check if child (%d) is still running!",ilChildPid);
					/*return iCDI_TERMINATE;*/
					/* set returncode for client, this shows client success... */
					/* only because kill doesn't work it does not mean that FTP*/
					/* hasn't worked out*/
					if (errno==ESRCH) /* No such process */
						ilRC = RC_SUCCESS;
					else
						ilRC = RC_FAIL;
				}
				else
				{
					dbg(DEBUG,"<SendFile> successfull killing, i'm a murderer...");
					/* set returncode for client, this shows client an error... */
					ilRC = RC_FAIL;
				}

			}
			else
			{
				/* set returncode for client, this shows client success... */
				ilRC = RC_SUCCESS;
			}

			/* remove control file */
			remove(rgTM.pcFTPCtrlFile);

			if (rgTM.iWriteFINATelegram == 1)
			{
				if ((ilRC = WriteFinaTelegram(ilRC,prpCDICmd->pcRFileName,""))!=RC_SUCCESS)
				{
					dbg(TRACE,"<SendFile> WriteFinaTelegram returns: <%d>", ilRC);
					return iCDI_RESET;
				}
				dbg(DEBUG,"<SendFile> WriteFinaTelegram returns: <%d>", ilRC);
			}

			/* write an EOD-Command to client */
			if ((ilRC = WriteEODToClient()) != RC_SUCCESS)
			{
				dbg(TRACE,"<SendFile> WriteEODToClient returns: <%s>", RETURN(ilRC));
				return ilRC;
			}
			dbg(TRACE,"<SendFile> WriteEODToClient returns: %d", ilRC);
			dbg(TRACE,"<SendFile> WAITING FOR START UPDATES");
		}
		else
		{
			dbg(TRACE,"<SendFile> ----- SEND FILE <FTPHDL-mode> -----");
			/* check ftp-command */
			/*if (strlen(prpCDICmd->pcFileCreateCmd))*/
			if (prpCDICmd->iUseFtp == 1)
			{
				if ((ilRC = TriggerFtpHdl(prpCDICmd,prpCDITable)) != RC_SUCCESS)
				{
					dbg(TRACE,"<SendFile> TriggerFtpHdl() returns: <%s>",  RETURN(ilRC));
					return ilRC;
				}
				dbg(TRACE,"<SendFile> TriggerFtpHdl() returns: <%s>",  RETURN(ilRC));
			}

			/* check send file to queue flag... */
			if (strlen(prpCDICmd->pcSendFileCmd))
			{
				if ((ilRC = SendFileToQue(prpCDICmd)) != RC_SUCCESS)
				{
					dbg(TRACE,"<SendFile> SendFileToQue returns: <%s>",  RETURN(ilRC));
					return ilRC;
				}
			}
		} /* endif for ftphdl-mode */
	}

	/* EOD isn't necessary for active update service */
	if ((igUpdateService==iNOT_ACTIVE && prpCDICmd->iSendEODAfterUpdate))
	{
		/* write an EOD-Command to client */
		if ((ilRC = WriteEODToClient()) != RC_SUCCESS)
		{
			dbg(TRACE,"<SendFile> WriteEODToClient returns: <%s>", RETURN(ilRC));
			return ilRC;
		}
		dbg(TRACE,"<SendFile> WriteEODToClient returns: %d", ilRC);
	}

	dbg(TRACE,"<SendFile> ----- END -----");
	return RC_SUCCESS;
}

/******************************************************************************/
/* the BuildFile routine                                                      */
/******************************************************************************/
static int BuildFile(struct _CDICmd *prpCDICmd)
{
	int			ilLen;
	char			pclFileNameAndPath[iMIN_BUF_SIZE];

	dbg(DEBUG,"<BuildFile> ----- START -----");

	/* calculate length of telegram... */
	if (strlen(((CDIDataTelegram*)pcgTCPSendMemory)->pcData+HEADER_LENGTH(rgTM.iVersion)))
	{
		/* necessary if we received 0 records from sqlhdl, flight... */ 
		/* in this case the filelength must be 0 */ 
		ilLen = strlen(((CDIDataTelegram*)pcgTCPSendMemory)->pcData+HEADER_LENGTH(rgTM.iVersion)) + sizeof(CDIDataTelegram) - 1;
	}
	else
	{
		/* set len to zero, like a flag... */
		ilLen = 0;
	}

	/* check file pointer... */
	if (prpCDICmd->pFh == NULL)
	{
		PrintCDICmdStructure(prpCDICmd);
		sprintf(pclFileNameAndPath,"%s/%s", prpCDICmd->pcLocalFilePath, prpCDICmd->pcFileName);
		/* file isn't open, open it now */
		dbg(DEBUG,"<BuildFile> try to open: <%s>", pclFileNameAndPath);
		if ((prpCDICmd->pFh = fopen(pclFileNameAndPath, "w")) == NULL)
		{
			dbg(TRACE,"<BuildFile> cannot open file");
			return iCDI_RESET;
		}

		dbg(DEBUG,"<BuildFile> Len=%d",ilLen);
		if (ilLen)
		{
			fwrite((((CDIDataTelegram*)pcgTCPSendMemory)->pcTimeStamp+HEADER_LENGTH(rgTM.iVersion)), ilLen, 1, prpCDICmd->pFh);
			fwrite("\n", 1, 1, prpCDICmd->pFh);
		}
	}
	else
	{
		if (ilLen)
		{
			fwrite((((CDIDataTelegram*)pcgTCPSendMemory)->pcTimeStamp+HEADER_LENGTH(rgTM.iVersion)), ilLen, 1, prpCDICmd->pFh);
			fwrite("\n", 1, 1, prpCDICmd->pFh);
		}
	}

	/*
	dbg(DEBUG,"<BuildFile> ----- END -----");
	*/
	return RC_SUCCESS;
}

/******************************************************************************/
/* the WriteTelegramToClient routine                                          */
/******************************************************************************/
static int WriteTelegramToCLient(struct _CDICmd *prpCDICmd)
{
	int			ilRC;
	int			ilLen;
	int			ilOffset;
	char			pclLen[5];
	char			pclLocalSendMemory[iMAX_DATA];
	CDIRecover	rlCDIRecover;


	if(rgTM.iAddLFatEnd == 1){
	  /* add Linefeed at end of telegram */
	strcat(((char*)((CDIDataTelegram*)pcgTCPSendMemory)->pcData+HEADER_LENGTH(rgTM.iVersion)),"\n");
	}

	/* calculate length of telegram... */
	ilLen = strlen(((CDIDataTelegram*)pcgTCPSendMemory)->pcData+HEADER_LENGTH(rgTM.iVersion));

	/* for header */
	memset((void*)pclLen, 0x00, 5);
	sprintf(pclLen, "%-4.4d", ilLen + sizeof(CDIDataTelegram) - 1);
	memcpy(VERSION(rgTM.iVersion, ((CDI2Header*)pcgTCPSendMemory)->pcLength, ((CDI1Header*)pcgTCPSendMemory)->pcLength), pclLen, 4);

	/* set length of data area in header */
	ilLen += sizeof(CDIDataTelegram) + HEADER_LENGTH(rgTM.iVersion);
	--ilLen;

	/* clear local buffer */
	memset((void*)pclLocalSendMemory, 0x00, iMAX_DATA);

	/* copy global to local buffer, to save current telegram */
	memcpy((void*)pclLocalSendMemory, pcgTCPSendMemory, iMAX_DATA); 

	ilOffset = 0;
	memcpy(pcgTCPSendCopy,pcgTCPSendMemory,ilLen);

	if (rgTM.iSecureOption)
	{
		ilOffset = 2;
		memcpy(pcgTCPSendMemory, SEncode(pcgTCPSendMemory, ilLen), ilLen+ilOffset);
		/*ToSCProtFile(ilLen+ilOffset); JWE */
	}

	/* write it to socket */
	ilRC = write(rgTM.iTCPWorkSocket, pcgTCPSendMemory, ilLen+ilOffset);
	if (ilRC != ilLen+ilOffset)
	{
		dbg(TRACE,"<WriteTelegramToClient> can't write to socket...");
		if (igClientErr != TRUE)
		{
			strcpy(pcgTCPSendMemory,"<WriteTelegramToClient> CLIENT CLOSED SOCKET. COULDN'T SEND:");
			ToSProtFile(-1); 
			igClientErr = TRUE;
		} /* end if */
		/* write it to send protocol file */
		/* memcpy(pcgTCPSendMemory,pcgTCPSendCopy,ilLen);*/
		/*ToSProtFile(ilLen);*/ 
		WriteToSecondQueue(PRIORITY_3);
		return iCDI_RESET;
	}
	else
	{
		/* write it to send protocol file */
		memcpy(pcgTCPSendMemory,pcgTCPSendCopy,ilLen);
		ToSProtFile(ilLen); 

		if (rgTM.iSecureOption)
		{
			ilOffset = 2;
			ToSCProtFile(ilLen+ilOffset);
		}
	}

	/* restore 'old' buffer */
	memcpy((void*)pcgTCPSendMemory, pclLocalSendMemory, iMAX_DATA); 

	/* set structure members */
	rlCDIRecover.iLength = ilLen;
	if (rgTM.iVersion >= 200)
	{
		memcpy(rlCDIRecover.pcStartTimeStamp, ((CDI2Header*)pcgTCPSendMemory)->pcTimeStamp, 14);
		memcpy(rlCDIRecover.pcStartTeleNo, ((CDI2Header*)pcgTCPSendMemory)->pcTeleNo, 5);
	}
	else
	{
		memcpy(rlCDIRecover.pcStartTimeStamp,	((CDI1Header*)pcgTCPSendMemory)->pcTimeStamp, 14);
		memcpy(rlCDIRecover.pcStartTeleNo, ((CDI1Header*)pcgTCPSendMemory)->pcTeleNo, 5);
	}
	memcpy(rlCDIRecover.pcData, pcgTCPSendMemory, ilLen);

	/* write it to file */
  if (rgTM.iRecoverCounter > 0)
  {
		if ((ilRC = HandleFileRecovery(iADD_CMD, NULL, &rlCDIRecover, NULL)) != RC_SUCCESS)
		{
			dbg(DEBUG,"<WriteTelegramToClient> HandleFileRecovery returns <%s>",  RETURN(ilRC));
			return ilRC;
		}
	}

	/* bye bye */
	return RC_SUCCESS;
}

/******************************************************************************/
/* the WriteSendFlag routine                                                  */
/******************************************************************************/
static int WriteSendFlag(char *pcpFields, char *pcpData, CDITable *prpCDITable, char *pcpSelection)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "WriteSendFlag:";
  char pclSelection[128];
  char pclUrno[128];
  char pclTwEnd[128];
  int ilItemNo;
  char *pclTmpPtr;

  if (prpCDITable->pcTableNameForFlag[0] == 0x00)
     return ilRC;  /* nothing to do */

  if (strcmp(prpCDITable->pcWaitForAckFlag,"YES") == 0)
     return ilRC;  /* nothing to do */

  ilItemNo = get_item_no(pcpFields,"URNO",5) + 1;
  if (ilItemNo <= 0)
  {
     pclTmpPtr = strstr(pcpSelection,"\n");
     if (pclTmpPtr != NULL)
     {
        pclTmpPtr++;
        strcpy(pclUrno,pclTmpPtr);
     }
     else
     {
        pclTmpPtr = strstr(pcpSelection,"=");
        if (pclTmpPtr != NULL)
        {
           pclTmpPtr++;
           strcpy(pclUrno,pclTmpPtr);
        }
        else
           strcpy(pclUrno,pcpSelection);
     }
  }
  else
     get_real_item(pclUrno,pcpData,ilItemNo);
  sprintf(pclTwEnd,"%s,%s,%s",pcgHomeAP,pcgTABEnd,mod_name);
  sprintf(pclSelection,"WHERE URNO = %s",pclUrno);
  dbg(TRACE,"%s Set Send Flag for <%s> of <%s>",pclFunc,pclUrno,prpCDITable->pcTableNameForFlag);
  dbg(DEBUG,"%s Send <URT> for <%s> to <%d>",pclFunc,prpCDITable->pcTableNameForFlag,igRouterID);
  dbg(DEBUG,"%s Selection = <%s>",pclFunc,pclSelection);
  dbg(DEBUG,"%s Fields = <%s>",pclFunc,prpCDITable->pcFieldNameForFlag);
  dbg(DEBUG,"%s Data = <%s>",pclFunc,prpCDITable->pcFieldContentsForFlag);
  ilRC = SendCedaEvent(igRouterID,0,mod_name,"CEDA",pcgTwStart,pclTwEnd,"URT",
                       prpCDITable->pcTableNameForFlag,pclSelection,
                       prpCDITable->pcFieldNameForFlag,prpCDITable->pcFieldContentsForFlag,
                       "",3,NETOUT_NO_ACK);

  return ilRC;
}

/******************************************************************************/
/* the WriteToLogTab routine                                                  */
/******************************************************************************/
static int WriteToLogTab(char *pcpExtCmd, char *pcpTabNam, char *pcpSelect, char *pcpFields, char *pcpData,
                         CDITable *prpCDITable, char *pcpSendFlag, char *pcpIlgUrno, char *pcpMsgi)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "WriteToLogTab:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[8000];
  char pclDataBuf[8000];
  char pclNextUrno[16];
  char pclCurrentTime[16];
  char pclComd[4];
  char *pclTmpPtr;
  int ilItemNo;
  char pclUrno[16];
  char pclSelect[256];
  char pclFields[1024];
  char pclData[4048];
  char pclFldLst[256];
  char pclDatLst[5000];
  char *pclTmpBgn;
  char *pclTmpEnd;

  strcpy(pcpIlgUrno,"NO");
  if (prpCDITable->pcTableNameForFlag[0] == 0x00)
     return ilRC;  /* nothing to do */

  if (strcmp(prpCDITable->pcWaitForAckFlag,"YES") != 0)
     return ilRC;  /* nothing to do */

  strcpy(pcpIlgUrno,"0");
  strcpy(pclFldLst,"URNO,HOPO,USEC,CDAT,PNAM,COMD,TABN,FLDN,FLAG,RURN,COMM,OBJN,SELE,FLDS,DATA,SEND,MSGI");
  TimeToStr(pclCurrentTime,time(NULL));
  strcpy(pclFields,pcpFields);
  ReplaceChar(pclFields,',','\027');
  pclTmpBgn = pcpData;
  pclTmpEnd = strstr(pclTmpBgn,"\n");
  if (pclTmpEnd != NULL || strstr(pcpSelect,"BETWEEN") != NULL || strstr(pcpSelect,"between") != NULL ||
      strstr(pcpSelect,"PRFL = ' '") != NULL || strstr(pcpSelect,"prfl = ' '") != NULL ||
      strstr(pcpSelect,"ACKF = ' '") != NULL || strstr(pcpSelect,"ackf = ' '") != NULL)
     strcpy(pclComd,"I");
  else if (prpCDITable->iTelegramType == iDAT_CLASS)
     strcpy(pclComd,"U");
  else if (prpCDITable->iTelegramType == iINS_CLASS)
     strcpy(pclComd,"I");
  else
     strcpy(pclComd,"D");
  while (pclTmpBgn != NULL)
  {
     if (pclTmpEnd == NULL)
        strcpy(pclData,pclTmpBgn);
     else
     {
        strncpy(pclData,pclTmpBgn,pclTmpEnd-pclTmpBgn);
        pclData[pclTmpEnd-pclTmpBgn] = '\0';
     }
     ilItemNo = get_item_no(pcpFields,"URNO",5) + 1;
     if (ilItemNo <= 0)
     {
        pclTmpPtr = strstr(pcpSelect,"\n");
        if (pclTmpPtr != NULL)
        {
           pclTmpPtr++;
           strcpy(pclUrno,pclTmpPtr);
        }
        else
        {
           pclTmpPtr = strstr(pcpSelect,"=");
           if (pclTmpPtr != NULL)
           {
              pclTmpPtr++;
              strcpy(pclUrno,pclTmpPtr);
           }
           else
              strcpy(pclUrno,pcpSelect);
        }
     }
     else
        get_real_item(pclUrno,pclData,ilItemNo);
     ReplaceChar(pclData,',','\027');
     ReplaceChar(pclData,'\047','\030');
     strcpy(pclSelect,pcpSelect);
     ReplaceChar(pclSelect,'\047','\030');
 /*    if (*pclComd != 'I')
     {
        sprintf(pclSqlBuf,"SELECT URNO FROM ILGTAB WHERE RURN = %s AND COMD = 'I' AND PNAM = '%s'",
                pclUrno,mod_name);
        slCursor = 0;
        slFkt = START;
        dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        close_my_cursor(&slCursor);
     }
     else
        if (*pcpSendFlag == ' ')
           ilRCdb = DB_ERROR;
        else
           ilRCdb = DB_SUCCESS;
     if (ilRCdb == DB_SUCCESS)
     {*/
        GetNextNumbers("ILGTAB",pclNextUrno,1,"GNV");
        strcpy(pcpIlgUrno,pclNextUrno);
        sprintf(pclDatLst,"%s,'%s','%s','%s','%s','%s','%s','%s','%s',%s,'%s','%s','%s','%s','%s','%s','%s'",
                pclNextUrno,pcgHomeAP,mod_name,pclCurrentTime,mod_name,pclComd,
                prpCDITable->pcTableNameForFlag,prpCDITable->pcFieldNameForFlag,
                prpCDITable->pcFieldContentsForFlag,pclUrno,pcpExtCmd,pcpTabNam,
                pclSelect,pclFields,pclData,pcpSendFlag,pcpMsgi);
        sprintf(pclSqlBuf,"INSERT INTO ILGTAB FIELDS(%s) VALUES(%s)",pclFldLst,pclDatLst);
        slCursor = 0;
        slFkt = START;
        if (debug_level == DEBUG)
           dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
        else
           dbg(TRACE,"%s Write to ILGTAB",pclFunc);
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        if (ilRCdb == DB_SUCCESS)
           commit_work();
        else
           dbg(TRACE,"%s Error inserting into ILGTAB",pclFunc);
        close_my_cursor(&slCursor);
 /*    }
     else
        dbg(TRACE,"%s Record <%s> was never sent",pclFunc,pclUrno);*/
     if (pclTmpEnd != NULL)
     {
        pclTmpBgn = pclTmpEnd + 1;
        pclTmpEnd = strstr(pclTmpBgn,"\n");
     }
     else
        pclTmpBgn = NULL;
  }

  return ilRC;
} /* End of WriteToLogTab */

/******************************************************************************/
/* the ReadFromLogTab routine                                                  */
/******************************************************************************/
static int ReadFromLogTab()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "ReadFromLogTab:";
  int ilRCdbRd = DB_SUCCESS;
  short slFktRd;
  short slCursorRd;
  char pclSqlBufRd[2048];
  char pclDataBufRd[8000];
  int ilRCdbDel = DB_SUCCESS;
  short slFktDel;
  short slCursorDel;
  char pclSqlBufDel[2048];
  char pclDataBufDel[8000];
  char pclFldLst[1024];
  char pclUrno[16];
  char pclIntCmd[16];
  char pclCommand[16];
  char pclTable[16];
  char pclSelect[512];
  char pclFields[1024];
  char pclData[4096];
  char pclTwEnd[128];
  int ilModId;

  sprintf(pclTwEnd,"%s,%s,%s",pcgHomeAP,pcgTABEnd,mod_name);
  if (igSecondModID > 0)
     ilModId = igSecondModID;
  else
     ilModId = mod_id;
  strcpy(pclFldLst,"URNO,COMD,COMM,OBJN,SELE,FLDS,DATA");
  sprintf(pclSqlBufRd,"SELECT %s FROM ILGTAB WHERE PNAM = '%s' AND SEND = ' ' AND ACKF = ' ' ORDER BY URNO",
          pclFldLst,mod_name);
  slCursorRd = 0;
  slFktRd = START;
  dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBufRd);
  ilRCdbRd = sql_if(slFktRd,&slCursorRd,pclSqlBufRd,pclDataBufRd);
  while (ilRCdbRd == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBufRd,"",7,",");
     get_real_item(pclUrno,pclDataBufRd,1);
     get_real_item(pclIntCmd,pclDataBufRd,2);
     get_real_item(pclCommand,pclDataBufRd,3);
     get_real_item(pclTable,pclDataBufRd,4);
     get_real_item(pclSelect,pclDataBufRd,5);
     ReplaceChar(pclSelect,'\030','\047');
     get_real_item(pclFields,pclDataBufRd,6);
     ReplaceChar(pclFields,'\027',',');
     get_real_item(pclData,pclDataBufRd,7);
     ReplaceChar(pclData,'\027',',');
     ReplaceChar(pclData,'\030','\047');
     sprintf(pclSqlBufDel,"DELETE FROM ILGTAB WHERE URNO = %s",pclUrno);
     slCursorDel = 0;
     slFktDel = START;
     dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBufDel);
     ilRCdbDel = sql_if(slFktDel,&slCursorDel,pclSqlBufDel,pclDataBufDel);
     commit_work();
     close_my_cursor(&slCursorDel);
     if (*pclIntCmd == 'U' || *pclIntCmd == 'D')
     {
        dbg(TRACE,"%s Send <%s> for <%s> to <%d>",pclFunc,pclCommand,pclTable,mod_id);
        dbg(TRACE,"%s Selection = <%s>",pclFunc,pclSelect);
        dbg(TRACE,"%s Fields = <%s>",pclFunc,pclFields);
        dbg(TRACE,"%s Data = <%s>",pclFunc,pclData);
        ilRC = SendCedaEvent(ilModId,0,mod_name,"CEDA",pcgTwStart,pclTwEnd,pclCommand,
                             pclTable,pclSelect,pclFields,pclData,"",2,NETOUT_NO_ACK);
        if (igSecondModID > 0)
           igItemsOnQ2++;
     }
     slFktRd = NEXT;
     ilRCdbRd = sql_if(slFktRd,&slCursorRd,pclSqlBufRd,pclDataBufRd);
  }
  close_my_cursor(&slCursorRd);

  return ilRC;
} /* End of ReadFromLogTab */

/******************************************************************************/
/* the ReplaceChar routine                                                  */
/******************************************************************************/
static int ReplaceChar(char *pcpText, char pcOrg, char pcNew)
{
  int ilRC = RC_SUCCESS;
  int ilI;

  for (ilI = 0; ilI < strlen(pcpText); ilI++)
  {
     if (pcpText[ilI] == pcOrg)
        pcpText[ilI] = pcNew;
  }

  return ilRC;
} /* End of ReplaceChar */

/******************************************************************************/
/* the HandleAcknowledge routine                                                  */
/******************************************************************************/
static int HandleAcknowledge(CDIIncomingData *prpInDat)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "HandleAcknowledge:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[2048];
  char pclDataBuf[8000];
  char pclSeparator[4];
  char pclSearch[8];
  char *pclData;
  char *pclBgn;
  char *pclEnd;
  char pclLurn[16];
  char pclComd[16];
  char pclTabn[16];
  char pclFldn[16];
  char pclFlag[16];
  char pclRurn[16];

  strncpy(pclSeparator,prpInDat->pcSeparator,1);
  pclSeparator[1] = '\0';
  pclData = prpInDat->pcData;
  sprintf(pclSearch,"LURN%s",pclSeparator);
  pclBgn = strstr(pclData,pclSearch);
  while (pclBgn != NULL)
  {
     pclBgn += strlen(pclSearch);
     pclEnd = strstr(pclBgn,pclSeparator);
     if (pclEnd != NULL)
     {
        memset(pclLurn,0x00,16);
        strncpy(pclLurn,pclBgn,pclEnd-pclBgn);
        sprintf(pclSqlBuf,"UPDATE ILGTAB SET ACKF = 'Y' WHERE URNO = %s",pclLurn);
        slCursor = 0;
        slFkt = START;
        if (debug_level == DEBUG)
           dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
        else
           dbg(TRACE,"%s Ack for Urno = <%s> of ILGTAB",pclFunc,pclLurn);
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        commit_work();
        close_my_cursor(&slCursor);
        sprintf(pclSqlBuf,"SELECT COMD,TABN,FLDN,FLAG,RURN FROM ILGTAB WHERE URNO = %s",pclLurn);
        slCursor = 0;
        slFkt = START;
        dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        close_my_cursor(&slCursor);
        if (ilRCdb == DB_SUCCESS)
        {
           BuildItemBuffer(pclDataBuf,"",5,",");
           get_real_item(pclComd,pclDataBuf,1);
           if (*pclComd == 'I')
           {
              get_real_item(pclTabn,pclDataBuf,2);
              get_real_item(pclFldn,pclDataBuf,3);
              get_real_item(pclFlag,pclDataBuf,4);
              get_real_item(pclRurn,pclDataBuf,5);
              sprintf(pclSqlBuf,"UPDATE %s SET %s = '%s' WHERE URNO = %s",pclTabn,pclFldn,pclFlag,pclRurn);
              slCursor = 0;
              slFkt = START;
              if (debug_level == DEBUG)
                 dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
              else
                 dbg(TRACE,"%s Set Flag for Urno = <%s> of <%s>",pclFunc,pclRurn,pclTabn);
              ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
              commit_work();
              close_my_cursor(&slCursor);
           }
        }
        pclBgn = strstr(pclEnd,pclSearch);
     }
     else
        pclBgn = NULL;
  }

  return ilRC;
} /* End of HandleAcknowledge */

/******************************************************************************/
/* The TimeToStr routine                                                      */
/******************************************************************************/
static int TimeToStr(char *pcpTime,time_t lpTime)
{
  struct tm *_tm;

  _tm = (struct tm *)gmtime(&lpTime);

  sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
          _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
          _tm->tm_min,_tm->tm_sec);

  return RC_SUCCESS;

} /* end of TimeToStr */

/******************************************************************************/
/* helpful for testing the server                                             */
/******************************************************************************/
static void PrintCDICmdStructure(struct _CDICmd *prpCDICmd)
{
	dbg(DEBUG,"<PrintCDICmdStructure> CMD: <%s>", prpCDICmd->pcCmd);
	dbg(DEBUG,"<PrintCDICmdStructure> UseFtp: <%s>", prpCDICmd->iUseFtp ? "YES" : "NO");
	dbg(DEBUG,"<PrintCDICmdStructure> NoTable: %d", prpCDICmd->iNoTable);
	return;
}
/******************************************************************************/
/* the WriteFINATelegram routine */
/******************************************************************************/
static int WriteFinaTelegram(int ipReturnCode,char *pcpFileName,char *pcpDataType)
{
	int			ilRC;
	int			ilLen;
	int			ilOffset;
	long			llMin;
	long			llMax;
	long			llUniqueNumber;
	char			*pclS = NULL;
	char			pclLen[5];
	char			pclTmpBuf[iMIN];

	dbg(DEBUG,"<WriteFinaTelegram> ----- START -----");

	if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		/* need exactly one number */
		if ((ilRC = HandleUniqueNumber(rgTM.pcServerName, rgTM.pcDATKey, "MAXOW", 1, &llUniqueNumber, &llMax, &llMin)) != RC_SUCCESS)
		{
			dbg(TRACE,"<WriteFinaTelegram> HandleUniqueNumber returns: %d", ilRC);
			return iCDI_TERMINATE;
		}
	}
	else
	{
		/* wrong HSB-STATUS..., returning with reset here... */
		dbg(TRACE,"<WriteFinaTelegram> wrong CTRL-STA: <%s>", CTRL_STA(ctrl_sta));
		return iCDI_RESET;
	}

	/* prepare the telegram */
	memset((void*)pcgTCPSendMemory, 0x00, iMAX_DATA);
	/* build header for command */
	BuildHeader((CDI2Header*)pcgTCPSendMemory, "DAT", llUniqueNumber);
	/* current timestamp */
	memmove(((CDIDataTelegram*)pcgTCPSendMemory)->pcTimeStamp+HEADER_LENGTH(rgTM.iVersion), GetTimeStamp(), 14);
	/* source, is empty */
	memmove(((CDIDataTelegram*)pcgTCPSendMemory)->pcSource+HEADER_LENGTH(rgTM.iVersion),"   ", 3);

	/* data type */
	if (strncmp(rgTM.cFINADataType,"  ",2) == 0)
		memmove(((CDIDataTelegram*)pcgTCPSendMemory)->pcDataType+HEADER_LENGTH(rgTM.iVersion),pcpDataType,2);
	else
		memmove(((CDIDataTelegram*)pcgTCPSendMemory)->pcDataType+HEADER_LENGTH(rgTM.iVersion),rgTM.cFINADataType,2);

	/* initialize key */
	memset(((CDIDataTelegram*)pcgTCPSendMemory)->pcKey+HEADER_LENGTH(rgTM.iVersion), '0', 25);
	/* and set key (that's URNO) */
	memmove(((CDIDataTelegram*)pcgTCPSendMemory)->pcKey+HEADER_LENGTH(rgTM.iVersion)+25-strlen("0"), "0", strlen("0"));
	/* separator */
	memmove(((CDIDataTelegram*)pcgTCPSendMemory)->pcSeparator+HEADER_LENGTH(rgTM.iVersion), rgTM.pcSeparator, 1);
	/* everything is fine */

	/* set data... */
	pclS = (char*)((CDIDataTelegram*)pcgTCPSendMemory)->pcData+HEADER_LENGTH(rgTM.iVersion);
	pclTmpBuf[0] = 0x00;
	sprintf(pclTmpBuf, "%d",ipReturnCode);
	sprintf(pclS,"FINA%s%s%sERRC%s%s%s", rgTM.pcSeparator, pcpFileName, rgTM.pcSeparator, rgTM.pcSeparator, pclTmpBuf, rgTM.pcSeparator);

	 /* calculate length of telegram... */
	ilLen = strlen(((CDIDataTelegram*)pcgTCPSendMemory)->pcData+HEADER_LENGTH(rgTM.iVersion));

	/* for header */
	memset((void*)pclLen, 0x00, 5);
	sprintf(pclLen, "%-4.4d", ilLen + sizeof(CDIDataTelegram) - 1);
	memcpy(VERSION(rgTM.iVersion, ((CDI2Header*)pcgTCPSendMemory)->pcLength, ((CDI1Header*)pcgTCPSendMemory)->pcLength), pclLen, 4);

	/* set length of data area in header */
	ilLen += sizeof(CDIDataTelegram) + HEADER_LENGTH(rgTM.iVersion);
	--ilLen;

	ilOffset = 0;

	if (rgTM.iSecureOption)
	{
		ilOffset = 2;
		memcpy(pcgTCPSendMemory, SEncode(pcgTCPSendMemory, ilLen), ilLen+ilOffset);
		/*ToSCProtFile(ilLen+ilOffset); */
	}

	/* write it to socket */
	ilRC = write(rgTM.iTCPWorkSocket, pcgTCPSendMemory, ilLen+ilOffset);
	if (ilRC != ilLen+ilOffset)
	{
		dbg(TRACE,"<WriteFinaTelegram> can't write to socket...");
		return iCDI_RESET;
	}
	else
	{
		/* write it to send protocol file */
		ToSProtFile(ilLen); 
		ilOffset = 0;

		if (rgTM.iSecureOption)
		{
			ilOffset = 2;
			ToSCProtFile(ilLen+ilOffset); 
		}
	}
	dbg(DEBUG,"<WriteFinaTelegram> ----- END -----");

	/* Bye bye */
	return RC_SUCCESS;
}
/******************************************************************************/
/* The HandleFileRecovery routine                                             */
/******************************************************************************/
static int HandleFileRecovery(int ipFunction, int *pipAnz, 
										CDIRecover *prpRecIn, CDIRecover *prpRecOut)
{
	int						i;
	int						ilRC = RC_SUCCESS;
	int						ilCmpStart;
	int						ilCmpEnd;
	int						ilPos;
	int						ilCnt = 1000;
	int						ilLevel;
	FILE					*fh;
	char					*pclAllValidNo = NULL;
	CDIRecover		rlRecTmp;
	 
	/* get current level... */
	if ((fh = fopen(rgTM.pcRecoverFile,"r")) == NULL)
	{
		dbg(TRACE,"<HFR> can't open file <%s> for reading.", rgTM.pcRecoverFile);
		ilLevel = 0;
	}
	else
	{
		/*dbg(DEBUG,"<HFR> opened file <%s> for reading.", rgTM.pcRecoverFile);*/
		/* get current entry-level */
		fseek(fh, 0L, SEEK_SET);
		fscanf(fh, "%d\n", &ilLevel);

		/* close file */
		/*dbg(DEBUG,"<HFR> closing file <%s> from reading.", rgTM.pcRecoverFile);*/
		fclose(fh);
	}

	switch(ipFunction)
	{
		case iCLEAR_ALL:
			/* open it again */
			if ((fh = fopen(rgTM.pcRecoverFile,"w")) == NULL)
			{
				dbg(TRACE,"<HFR> can't open file <%s> for writing.", rgTM.pcRecoverFile);
			}
			else
			{
				/*dbg(DEBUG,"<HFR> opened file <%s> for writing.", rgTM.pcRecoverFile);*/
				/* set current level */
				ilLevel = 0;

				/* write it to file */
				fseek(fh, 0L, SEEK_SET);
				fprintf(fh, "%08d\n", ilLevel);

				/* close file */
				/*dbg(DEBUG,"<HFR> closing file <%s> from writing.", rgTM.pcRecoverFile);*/
				fclose(fh);
			}
			break;
			
		case iADD_CMD:
			if (ilLevel == rgTM.iRecoverCounter*2)
			{
				/*dbg(DEBUG,"<HFR> overwrite first %d elements",rgTM.iRecoverCounter);*/

				/* delete first row in file */
				if ((ilRC = DeleteRowInFile(1, rgTM.iRecoverCounter)) != RC_SUCCESS)
				{
					dbg(TRACE,"<HFR> DeleteRowInFile returns <%s>", RETURN(ilRC));
					return ilRC;
				}
				else
				{
					ilLevel -= (rgTM.iRecoverCounter - 1);
				}
			}
			else
			{
				ilLevel++;
			}

			/* open file for appending */
			if ((fh = fopen(rgTM.pcRecoverFile,"a")) == NULL)
			{
				dbg(TRACE,"<HFR> can't open file <%s> for appending.", rgTM.pcRecoverFile);
			}
			else
			{
				/*dbg(DEBUG,"<HFR> opened file <%s> for appending.", rgTM.pcRecoverFile);*/
				/* seek to end of file */
				fseek(fh, 0L, SEEK_END);
				if (ftell(fh) == 0L)
				{
					fprintf(fh, "%08d\n", ilLevel);
				}

				/* write data to file */
				fprintf(fh, "%d %s %s ", prpRecIn->iLength, prpRecIn->pcStartTimeStamp, prpRecIn->pcStartTeleNo);
				fwrite(prpRecIn->pcData, prpRecIn->iLength, 1, fh);
				fprintf(fh, "\n");

				/* close recover file */
				/*dbg(DEBUG,"<HFR> closing file <%s> from appending.", rgTM.pcRecoverFile);*/
				fclose (fh);

				/* open it again for updating level */
				if ((fh = fopen(rgTM.pcRecoverFile,"r+")) == NULL)
				{
					dbg(TRACE,"<HFR> can't open file <%s> for reading+", rgTM.pcRecoverFile);
					return iCDI_RESET;
				}
				else
				{
					/*dbg(DEBUG,"<HFR> opened file <%s> for reading+.", rgTM.pcRecoverFile);*/
					/* seek to start of file */
					fseek(fh, 0L, SEEK_SET);

					/* write data to file */
					fseek(fh, 0L, SEEK_SET);
					fprintf(fh, "%08d\n", ilLevel);

					/* close recover file */
					/*dbg(DEBUG,"<HFR> closing file <%s> from reading+.", rgTM.pcRecoverFile);*/
					fclose (fh);
				}
			}
			break;
		case iALL_TELENO_AT_NO:
		case iALL_TELENO_AT_TIME:
			if ((fh = fopen(rgTM.pcRecoverFile,"r")) == NULL)
			{
				dbg(TRACE,"<HFR> can't open file <%s> for reading.", rgTM.pcRecoverFile);
			}
			else
			{
				/*dbg(DEBUG,"<HFR> opened file <%s> for reading.", rgTM.pcRecoverFile);*/
				/* get current entry-level */
				fseek(fh, 0L, SEEK_SET);
				fscanf(fh, "%d\n", &ilLevel);

				if (ilLevel)
				{
					/* seek to last command */
					for (i=0, *pipAnz=0, ilPos=0; i<ilLevel ; i++)
					{
						/* get this command */
						fscanf(fh,"%d %s %s ", &rlRecTmp.iLength, rlRecTmp.pcStartTimeStamp, rlRecTmp.pcStartTeleNo);
						fread(rlRecTmp.pcData, rlRecTmp.iLength, 1, fh);
						fscanf(fh,"\n");

						/* is this a valid telegramnumber or timestamp? */
						if (ipFunction == iALL_TELENO_AT_NO)
						{
							if (!strlen(prpRecIn->pcEndTeleNo))
							{
								/* whithout end telegram number */
								ilCmpStart = strcmp(rlRecTmp.pcStartTeleNo, prpRecIn->pcStartTeleNo);
								/* default entry */
								ilCmpEnd = -1;
							}
							else
							{
								/* with end telegram number */
								ilCmpStart = strcmp(rlRecTmp.pcStartTeleNo, prpRecIn->pcStartTeleNo);
								ilCmpEnd = strcmp(rlRecTmp.pcStartTeleNo, prpRecIn->pcEndTeleNo);
							}
						}
						else
						{
							if (!strlen(prpRecIn->pcEndTimeStamp))
							{
								ilCmpStart = strcmp(rlRecTmp.pcStartTimeStamp, prpRecIn->pcStartTimeStamp);
								ilCmpEnd = -1;
							}
							else
							{
								ilCmpStart = strcmp(rlRecTmp.pcStartTimeStamp, prpRecIn->pcStartTimeStamp);
								ilCmpStart = strcmp(rlRecTmp.pcStartTimeStamp, prpRecIn->pcEndTimeStamp);
							}
						}

						if (ilCmpStart >= 0 && ilCmpEnd <= 0)
						{
							/* yes, it's valid... */
							(*pipAnz)++;
							if (pclAllValidNo == NULL)
							{
								/* get memory for 1000 Telegram-No. + comma */
								if ((pclAllValidNo = (char*)malloc(7*ilCnt)) == NULL)
								{
									dbg(TRACE,"<HFR> malloc error..");	
									return iCDI_TERMINATE;
								}
							}
								
							/* is there enough memory? */
							if (*pipAnz == ilCnt)
							{
								/* get more memory... */
								ilCnt += 1000;
								if ((pclAllValidNo = (char*)realloc(pclAllValidNo, 7*ilCnt)) == NULL)
								{
									dbg(TRACE,"<HFR> realloc error..");
									return iCDI_TERMINATE;
								}
							}

							/* store it in buffer... */
							memcpy(&pclAllValidNo[ilPos], rlRecTmp.pcStartTeleNo, 5);
							ilPos += 5;
							memcpy(&pclAllValidNo[ilPos], ",", 1);
							ilPos += 1;
						}
					}

					/* set pointer... */
					if (*pipAnz)
						prpRecOut->pcAllValidNo = pclAllValidNo;
					else
						prpRecOut->pcAllValidNo = NULL;
				}
				/*dbg(DEBUG,"<HFR> closing file <%s> from reading.", rgTM.pcRecoverFile);*/
				fclose(fh);
			}	
			break;
		case iGET_TELENO_CMD:
			if ((fh = fopen(rgTM.pcRecoverFile,"r")) == NULL)
			{
				dbg(TRACE,"<HFR> can't open file <%s> for reading.", rgTM.pcRecoverFile);
			}
			else
			{
				/*dbg(DEBUG,"<HFR> opened file <%s> for reading.", rgTM.pcRecoverFile);*/
				/* get current entry-level */
				fseek(fh, 0L, SEEK_SET);
				fscanf(fh, "%d\n", &ilLevel);

				if (ilLevel)
				{
					/* seek to last command */
					for (i=0, *pipAnz=0; i<ilLevel && *pipAnz==0 ; i++)
					{
						/* get this command */
						fscanf(fh,"%d %s %s ", &prpRecOut->iLength, prpRecOut->pcStartTimeStamp, prpRecOut->pcStartTeleNo);
						fread(prpRecOut->pcData, prpRecOut->iLength, 1, fh);
						fscanf(fh,"\n");

						/* is this the number? */
						if (!strcmp(prpRecIn->pcStartTeleNo, prpRecOut->pcStartTeleNo))
						{
							*pipAnz = 1;
							/*fclose(fh);*/
						}
					}
				}
				/*else -- Note:must close in any case if level found or not 
				{*/
					/*dbg(DEBUG,"<HFR> Level = 0");*/
					/*dbg(DEBUG,"<HFR> closing file <%s> from reading.", rgTM.pcRecoverFile);*/
					fclose(fh);
				/*}*/
			}
			break;
	}

	/* bye bye */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The DeleteRowInFile routine                                                */
/******************************************************************************/
static int DeleteRowInFile(int ipRowVon, int ipRowBis)
{
	int				c;
	int				cnt;
	int				ilRC;
	FILE				*fp;
	FILE				*fh;
	char				pclTmpBuf[iMIN_BUF_SIZE];

	dbg(DEBUG,"<DRiF> ----- START -----");

	/* open recover file for reading */
	if ((fh = fopen(rgTM.pcRecoverFile,"r")) == NULL)
	{
		dbg(TRACE,"<DRiF> can't open file %s", rgTM.pcRecoverFile);
		return iCDI_RESET;
	}

	memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
	sprintf(pclTmpBuf, "%05d.tmp", getpid());
	if ((fp = fopen(pclTmpBuf, "w")) == NULL)
	{
		dbg(TRACE,"<DRiF> connot open temporary file %s", pclTmpBuf);
		return iCDI_RESET;
	}
	else
	{
		/* linecounter */
		cnt = 0;

		/* set position in files */
		fseek(fp, 0L, SEEK_SET);
		fseek(fh, 0L, SEEK_SET);

		/* do until end of file */
		while ((c = fgetc(fh)) != EOF)
		{
			if (c == '\n') cnt++;
			if (cnt >= ipRowVon  && cnt <= ipRowBis) 
			{
				do
				{
					if ((c = fgetc(fh)) == '\n')
						cnt++;
				} while (cnt <= ipRowBis && c != EOF);
				fputc('\n', fp);
			}
			else
				fputc(c, fp);
		}

		/* close file */
		fclose(fh);

		/* close tmp-file */
		fclose(fp);

		/* copy tmp-file to recover file */
		if ((ilRC = rename(pclTmpBuf, rgTM.pcRecoverFile)) != 0)
		{
			dbg(TRACE,"<DRiF> cannot rename file (%d)", ilRC);
			return iCDI_RESET;
		}
	}

	dbg(DEBUG,"<DRiF> ----- END -----");
	/* everything looks good */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The HandleDBStop routine                                                   */
/******************************************************************************/
static int HandleDBStop(void)       
{
	int			ilRC;
	int			ilBytes;
	char			*pclErrorCode = NULL;

	dbg(DEBUG,"<HandleDBStop> ----- START -----");

	do
	{
		/*----------------------------------------------------------------
		look for TCP-data
		----------------------------------------------------------------*/
		if ((ilBytes = ReceiveTcpIp(rgTM.lTCPTimeout, rgTM.iTimeUnit, iPULL_TCP_NOT_ALLOWED)) > 0)
		{
			/* write data to protocol file */
			ToRProtFile(ilBytes);

			if ((ilRC = CheckReceivedHeader(&pclErrorCode)) != RC_SUCCESS)
			{
				/* back */
				dbg(DEBUG,"<HandleDBStop> ----- END -----");

				/* in this case send an ERR-Command to client... */
				/* ...and reset or terminate the server */
				if ((ilRC = WriteERRToClient(pclErrorCode)) == iCDI_TERMINATE)
				{
					dbg(DEBUG,"<HandleDBStop> ----- END -----");
					return iCDI_TERMINATE;
				}	
				else
				{
					dbg(DEBUG,"<HandleDBStop> ----- END -----");
					return iCDI_RESET;
				}
			}

			/* only if CheckReceivedHeader returns with success */
			if (ilRC == RC_SUCCESS)
			{
				/* handle received command */
				if ((ilRC = HandleReceivedData()) != RC_SUCCESS)
				{
					dbg(TRACE,"<HandleDBStop> HRD returns: <%s>", RETURN(ilRC));
					dbg(DEBUG,"<HandleDBStop> ----- END -----");

					/* back */
					return ilRC;
				}
			}
		}
		else if (ilBytes < 0)
		{
			/* back */
			dbg(DEBUG,"<HandleDBStop> ----- END -----");
			return iCDI_RESET;
		}
		else
		{
			/* we haven't received data via TCP/IP */
			if (igDBLCheck)
			{
				/* if we use DBL-Check -> this must be an timeout error */
				dbg(TRACE,"<HandleDBStop> timeout error");
				dbg(DEBUG,"<HandleDBStop> ----- END -----");

				/* in this case send an ERR-Command to client... */
				if ((ilRC = WriteERRToClient(sTIMEOUT)) == iCDI_TERMINATE)
				{
					dbg(DEBUG,"<HandleDBStop> ----- END -----");
					return iCDI_TERMINATE;
				}
				else
				{
					dbg(DEBUG,"<HandleDBStop> ----- END -----");
					return iCDI_RESET;
				}
			}
		}

		/*----------------------------------------------------------------
		check que-status
		----------------------------------------------------------------*/
		if ((ilRC = CheckQueStatus(igActiveQueue)) != RC_SUCCESS)
		{
			dbg(DEBUG,"<HandleDBStop> ----- END -----");
			return ilRC;
		}

		/* ----------------------------------------------------------------
		send DBL (database locked) - command to client	
		---------------------------------------------------------------- */
		if ((ilRC = CheckConnection(iDBL_CHECK)) < 0)
		{
			/* return from CDIServer */
			dbg(DEBUG,"<HandleDBStop> ----- END -----");
			return iCDI_RESET;
		}

	} while (igDBQueuesStopped == TRUE);

	dbg(DEBUG,"<HandleDBStop> ----- END -----");
	return RC_SUCCESS;
}

/******************************************************************************/
/* The PullEvents routine                                                     */
/******************************************************************************/
static int PullEvent(void)
{
int	ilRC;
int	ilCurEvent;

  dbg(DEBUG,"<PullEvent> ----- START -----");

  for (ilCurEvent=0; ilCurEvent<igCDIEventNo; ilCurEvent++)
  {
     dbg(DEBUG,"<PullEvent> sending pushed event: %d",  ilCurEvent);
     if ((ilRC = que(QUE_PUT, mod_id, mod_id, PRIORITY_3, prgCDIEvent[ilCurEvent].iLength, prgCDIEvent[ilCurEvent].pcEvent)) != RC_SUCCESS)
     {
        dbg(TRACE,"<PullEvent> Error %d sending pushed event %d",  ilRC, ilCurEvent);
        return iCDI_TERMINATE;
     }

     /* delete memory here... */
     if (prgCDIEvent[ilCurEvent].pcEvent != NULL)
     {
        dbg(DEBUG,"<PullEvent> delete memory %d", ilCurEvent);
        free((void*)prgCDIEvent[ilCurEvent].pcEvent);
        prgCDIEvent[ilCurEvent].pcEvent = NULL;
     }
  }

  /* delete remaining memory */
  if (prgCDIEvent != NULL)
  {
     free((void*)prgCDIEvent);
     prgCDIEvent = NULL;
  }

  dbg(DEBUG,"<PullEvent> ----- END -----");

  /* reset global counter */
  igCDIEventNo = 0;

  return RC_SUCCESS;
}

/******************************************************************************/
/* The PushEvents routine                                                     */
/******************************************************************************/
static int PushEvent(void)
{
  dbg(DEBUG,"<PushEvent> ----- START -----");

  /* incr. counter */
  igCDIEventNo++;
  dbg(DEBUG,"<PushEvent> Number of CDIEvents: %d",  igCDIEventNo);

  /* get memory for event */
  if ((prgCDIEvent = (CDIEvent*)realloc(prgCDIEvent, igCDIEventNo*sizeof(CDIEvent))) == NULL)
  {
     dbg(TRACE,"<PushEvent> realloc failure...");
     return iCDI_TERMINATE;
  }

  /* calculate length */
  prgCDIEvent[igCDIEventNo-1].iLength = sizeof(EVENT) + prgEvent->data_length;
  dbg(DEBUG,"<PushEvent> Length: %d",  prgCDIEvent[igCDIEventNo-1].iLength);

  /* get memory for this event */
  if ((prgCDIEvent[igCDIEventNo-1].pcEvent = (char*)malloc(prgCDIEvent[igCDIEventNo-1].iLength)) == NULL)
  {
     dbg(TRACE,"<PushEvent> malloc (EVENT) failure...");
     return iCDI_TERMINATE;
  }

  /* store incoming event */
  memcpy(prgCDIEvent[igCDIEventNo-1].pcEvent, prgEvent, prgCDIEvent[igCDIEventNo-1].iLength);

  dbg(DEBUG,"<PushEvent> ----- END -----");

  return RC_SUCCESS;
}

/******************************************************************************/
/* The GetTime routine                                                        */
/******************************************************************************/
static void GetTime(char *s)
{
   struct 		timeb tp;
   time_t		_CurTime;
   struct tm	*CurTime;
   char			tmp[10];
      
   _CurTime = time(0L);
   CurTime = (struct tm *)localtime(&_CurTime);
#if defined(_WINNT)
   strftime(s,10,"%" "H:%" "M:%" "S",CurTime);
#else
   strftime(s,20,"%" "b%" "d %" "T",CurTime);
#endif

   ftime(&tp);
   sprintf(tmp,":%3.3d:",tp.millitm);
   strcat(s,tmp);
	return;
}

/******************************************************************************/
/* The CheckTimeFrame routine                                                 */
/******************************************************************************/
static int CheckTimeFrame(int ipRecCounter, char *pcpRecords, char *pcpFields, 
								  CDITable *prpTabDef)
{
	int			ilRC;
	int			ilStop;
	int			ilCurRec;
	int			ilCmpEnd;
	int			ilCmpStart;
	int			ilNoInvalidRecords;
	int			ilCurTFField;
	char			*pclTmpPtr = NULL;
	char			pclTFField[iMIN];
	char			pclTFData[iMIN];
	char			pclDataBuffer[iMAXIMUM];

	dbg(DEBUG,"<CheckTimeFrame> ----- START -----");

	/* init something */
	ilNoInvalidRecords = 0;

	/* is the service activated */
	if (igUpdateService == iACTIVE && strlen(prpTabDef->pcTimeFrameFields))
	{
		/* set temporary pointer to data area */
		pclTmpPtr = pcpRecords;

		/* for all records */
		for (ilCurRec=0; ilCurRec<ipRecCounter; ilCurRec++)
		{
			/* get n.th record */
			pclDataBuffer[0] = 0x00;
			pcpRecords = CopyNextField(pcpRecords, 0x0A, pclDataBuffer);

			/* for all timeframe fields */
			for (ilCurTFField=0, ilStop=0; ilCurTFField<prpTabDef->iNoTimeFrameFields && !ilStop; ilCurTFField++)
			{
				/* copy first timeframe fieldname to local buffer */
				strcpy(pclTFField, GetDataField(prpTabDef->pcTimeFrameFields, ilCurTFField, cCOMMA));

				/* then get the data */
				if ((ilRC = GetIndex(pcpFields, pclTFField, cCOMMA)) >= 0)
				{
					/* here we know position of current timeframe field */
					pclTFData[0] = 0x00;
					
					strcpy(pclTFData, GetDataField(pclDataBuffer, ilRC, cCOMMA));

					/* default values */
					ilCmpStart = ilCmpEnd = 0;
					
					/* compare start-timestamp */
					if (strlen(pcgUpdateStartTime) == 14)
					{
						ilCmpStart = strcmp(pclTFData, pcgUpdateStartTime);
					}

					/* compare end-timestamp */
					if (strlen(pcgUpdateEndTime) == 14)
					{
						ilCmpEnd = strcmp(pclTFData, pcgUpdateEndTime);
					}

					/* check timeframe now */
					if (ilCmpStart < 0 || ilCmpEnd > 0)
					{
						/* this is not a valid record ... */
						ilNoInvalidRecords++;

						/* break here */
						ilStop = 1;

						/* mark this record as not valid, this is realized 
							by setting first character to hex 0x01
						*/
						*pclTmpPtr = 0x01;

						/* set temporary pointer to next record... */
						while (*pclTmpPtr != 0x0A)
							pclTmpPtr++;
						pclTmpPtr++;
					}
				}
			}
		}
	}

	dbg(DEBUG,"<CheckTimeFrame> ----- END -----");

	/* bye bye */
	return ilNoInvalidRecords;
}

/******************************************************************************/
/* The CheckTCPQuickly routine                                                */
/******************************************************************************/
static int CheckTCPQuickly(void)
{
	int			ilRC;
	int			ilBytes;
	int			ilCmdNo;
	int			ilCmdType;
	int			ilOldDbgLvl;
	char			*pclErrorCode = NULL;

	/*
	dbg(DEBUG,"<CheckTCPQuickly> ----- START -----");
	*/

	/* save old debug level and set current */
	ilOldDbgLvl = debug_level;
	/*
	debug_level = TRACE;
	*/

	/* look for tcp-data */
	if ((ilBytes = ReceiveTcpIp(1, iMICRO_SECONDS, iPULL_TCP_NOT_ALLOWED)) > 0)
	{
		/* write data to protocol file */
		ToRProtFile(ilBytes);

		/* is this a valid header */
		if ((ilRC = CheckReceivedHeader(&pclErrorCode)) != RC_SUCCESS)
		{
			/* in this case send an ERR-Command to client... */
			/* ...and terminate the server */
			if ((ilRC = WriteERRToClient(pclErrorCode)) == iCDI_TERMINATE)
				return iCDI_TERMINATE;
			else
				return iCDI_RESET;
		}

		/* only CheckReceivedHeader returns with success */
		if (ilRC == RC_SUCCESS)
		{
			/* reset CCO- and some other flags... */
			igCCOCheck = 0;
			igDBLCheck = 0;
			igNoENDCommand = 0;

			/* set new time... */
			time1 = time(NULL);

			/* handle the telegram */
			switch (rgTM.iTCNumber)
			{
				case iCTL_CLASS:
					if ((ilRC = SearchCommandTypeAndNumber(rgTM.iTCNumber, &ilCmdType, &ilCmdNo)) == RC_FAIL)
					{
						dbg(TRACE,"<CheckTCPQuickly> SearchCommandTypeAndNumber returns <%s>",  RETURN(ilRC));
						return ilRC;
					}
					else
					{
						switch (ilCmdType)
						{
							case iINTERNAL_CMD:
								if ((ilRC = HandleInternalCmd(ilCmdNo)) != RC_SUCCESS)
								{
									dbg(TRACE,"<CheckTCPQuickly> HandleCTLData returns <%s>",  RETURN(ilRC));
									return ilRC;	
								}
								break;

							case iUPDATE_CMD:
							case iINVENTORY_CMD: 
							case iFREE_QUERY_CMD:
								if ((ilRC = PushTCPPackage(ilBytes)) != RC_SUCCESS)
								{
									dbg(TRACE,"<CheckTCPQuickly> PushTCPPackage returns <%s>",  RETURN(ilRC));
									return ilRC;	
								}
								break;
						}
					}
					break;

				case iDAT_CLASS:
				case iINS_CLASS:
				case iDEL_CLASS:
					/* store tcp-package here */
					if ((ilRC = PushTCPPackage(ilBytes)) != RC_SUCCESS)
					{
						dbg(TRACE,"<CheckTCPQuickly> PushTCPPackage returns <%s>",  RETURN(ilRC));
						return ilRC;	
					}
					break;
			}
		}
	}
	else if (ilBytes < 0)
	{
		dbg(TRACE,"<CheckTCPQuickly> ReceiveTcpIp returns: %d", ilBytes);
		return iCDI_RESET;	
	}

	/* set old debug level */
	debug_level = ilOldDbgLvl;

	/*
	dbg(DEBUG,"<CheckTCPQuickly> ----- END -----");
	*/

	/* bye bye */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The HandleUniqueNumber routine                                             */
/******************************************************************************/
static int HandleUniqueNumber(char *pcpProcessName, char *pcpKey, 
										char *pcpFlag,	int ipCnt, long *plpUniqueNumber, 
										long *plpMax, long *plpMin )
{
	int		ilRC;
	int		ilCurKey;
	long		llCurrentNo;
	long		llStepSize;
	char		pclTmpBuf[iMIN_BUF_SIZE];

	/* convert key to uppercase */
	pclTmpBuf[0] = 0x00;
	strcpy(pclTmpBuf, pcpKey);
	StringUPR((UCHAR*)pclTmpBuf);

	/* which key do we use */
	if (strstr(pclTmpBuf, "CTL"))
	{
		ilCurKey = iCTL_CLASS;
	}
	else if (strstr(pclTmpBuf, "DAT"))
	{
		ilCurKey = iDAT_CLASS;
	}		
	else if (strstr(pclTmpBuf, "INS"))
	{
		ilCurKey = iDAT_CLASS;
	}		
	else if (strstr(pclTmpBuf, "DEL"))
	{
		ilCurKey = iDEL_CLASS;
	}
	else if (strstr(pclTmpBuf, "CCO"))
	{
		ilCurKey = iCCO_CLASS;
	}
	else
	{
		dbg(TRACE,"<HUN> unknown key <%s> received...",  pcpKey);
		return iCDI_TERMINATE;
	}

	/* check available numbers */
	if (rgCDIUnique[ilCurKey].lAvailableNo < ipCnt)
	{
		if (ipCnt > iUNIQUE_STEP_SIZE)
			llStepSize = ipCnt;
		else
			llStepSize = iUNIQUE_STEP_SIZE;

		/* need new URNO's */
		if ((ilRC = GetUniqueNumber(pcpProcessName, pcpKey, pcpFlag, (int)llStepSize, &llCurrentNo, &rgCDIUnique[ilCurKey].lMax, &rgCDIUnique[ilCurKey].lMin)) != RC_SUCCESS)
		{
			dbg(TRACE,"<HandleUniqueNumber> GetUniqueNumber returns: %d", ilRC);
			return iCDI_TERMINATE;
		}

		/* set params */
		if (rgCDIUnique[ilCurKey].lCurrentNo == -1)
			*plpUniqueNumber = llCurrentNo;
		else
			*plpUniqueNumber = rgCDIUnique[ilCurKey].lCurrentNo;
		*plpMax = rgCDIUnique[ilCurKey].lMax; 
		*plpMin = rgCDIUnique[ilCurKey].lMin;

		/* set internal values */
		/* last */
		rgCDIUnique[ilCurKey].lLastNo = llCurrentNo + llStepSize;
		rgCDIUnique[ilCurKey].lLastNo = UNIQUE(rgCDIUnique[ilCurKey].lLastNo, rgCDIUnique[ilCurKey].lMax, rgCDIUnique[ilCurKey].lMin);

		/* current */
		if (rgCDIUnique[ilCurKey].lCurrentNo == -1)
			rgCDIUnique[ilCurKey].lCurrentNo = llCurrentNo + ipCnt;
		else
			rgCDIUnique[ilCurKey].lCurrentNo += ipCnt;
		rgCDIUnique[ilCurKey].lCurrentNo = UNIQUE(rgCDIUnique[ilCurKey].lCurrentNo, rgCDIUnique[ilCurKey].lMax, rgCDIUnique[ilCurKey].lMin);

		/* available */
		if (rgCDIUnique[ilCurKey].lLastNo >= rgCDIUnique[ilCurKey].lCurrentNo)
			rgCDIUnique[ilCurKey].lAvailableNo = rgCDIUnique[ilCurKey].lLastNo - rgCDIUnique[ilCurKey].lCurrentNo;
		else 
			rgCDIUnique[ilCurKey].lAvailableNo = rgCDIUnique[ilCurKey].lMax - rgCDIUnique[ilCurKey].lCurrentNo + rgCDIUnique[ilCurKey].lLastNo;
	}
	else
	{
		/* set params */
		*plpUniqueNumber = rgCDIUnique[ilCurKey].lCurrentNo;
		*plpMax = rgCDIUnique[ilCurKey].lMax; 
		*plpMin = rgCDIUnique[ilCurKey].lMin;

		/* set next (free) unique number */
		rgCDIUnique[ilCurKey].lCurrentNo += ipCnt;
		rgCDIUnique[ilCurKey].lCurrentNo = UNIQUE(rgCDIUnique[ilCurKey].lCurrentNo, rgCDIUnique[ilCurKey].lMax, rgCDIUnique[ilCurKey].lMin);

		/* set available numbers... */
		rgCDIUnique[ilCurKey].lAvailableNo -= ipCnt;
	}

	/* bye bye */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The PushTCPPackage routine                                                 */
/******************************************************************************/
static int PushTCPPackage(int ipLength)
{
  dbg(DEBUG,"<PushTCPPackage> ----- START -----");

  /* incr. counter */
  igCDITcpPackageNo++;
  dbg(DEBUG,"<PushTCPPackage> Number of CDIPackages: %d",  igCDITcpPackageNo);
dbg(TRACE,"AKL PUSH/PULL PUSH = %d",igCDITcpPackageNo);

  /* get memory for event */
  if ((prgCDITcpPackage = (CDITcpPackage*)realloc(prgCDITcpPackage, igCDITcpPackageNo*sizeof(CDITcpPackage))) == NULL)
  {
     dbg(TRACE,"<PushTCPPackage> realloc failure...");
     return iCDI_TERMINATE;
  }

  /* calculate length */
  prgCDITcpPackage[igCDITcpPackageNo-1].iLength = ipLength;
  dbg(DEBUG,"<PushTCPPackage> Length: %d",  prgCDITcpPackage[igCDITcpPackageNo-1].iLength);

  /* get memory for this event */
  if ((prgCDITcpPackage[igCDITcpPackageNo-1].pcPackage = (char*)malloc(prgCDITcpPackage[igCDITcpPackageNo-1].iLength)) == NULL)
  {
     dbg(TRACE,"<PushTCPPackage> malloc (PACKAGE) failure...");
     return iCDI_TERMINATE;
  }

  /* store incoming event */
  memcpy(prgCDITcpPackage[igCDITcpPackageNo-1].pcPackage, pcgTCPReceiveMemory, prgCDITcpPackage[igCDITcpPackageNo-1].iLength);

  dbg(DEBUG,"<PushTCPPackage> ----- END -----");

  /* bye bye */
  return RC_SUCCESS;
}

/******************************************************************************/
/* The PullTCPPackage routine                                                 */
/******************************************************************************/
static int PullTCPPackage(void)
{
  int ilBytes = 0;

  dbg(DEBUG,"<PullTCPPackage> ----- START -----");

  /* empty stack? */
  if (igCDITcpPackageNo)
  {
dbg(TRACE,"AKL PUSH/PULL PULL = %d",igCDITcpPackageNo);
     /* decrement global counter */
     --igCDITcpPackageNo;

     /* clear memory */
     memset((void*)pcgTCPReceiveMemory, 0x00, iMAX_DATA);
		
     /* get first stored package */
     memcpy(pcgTCPReceiveMemory, prgCDITcpPackage[0].pcPackage, prgCDITcpPackage[0].iLength);
dbg(TRACE,"AKL PullTCPPackage");
     snap(pcgTCPReceiveMemory, prgCDITcpPackage[0].iLength, outp);
     dbg(DEBUG,"<PullTCPPackage> restore package: 0, remaining: %d", igCDITcpPackageNo);

     /* set length of package... */
     ilBytes = prgCDITcpPackage[0].iLength;

     /* delete package memory */
     free ((void*)prgCDITcpPackage[0].pcPackage);
     prgCDITcpPackage[0].pcPackage = NULL;

     /* more packages? */
     if (igCDITcpPackageNo)
     {
        /* reorganize package stack */
        /* second becomes first, third becomes second, ... */
        dbg(DEBUG,"<PullTCPPackage> move %d packages for position 1 to 0", igCDITcpPackageNo);
        memmove(&prgCDITcpPackage[0], &prgCDITcpPackage[1], igCDITcpPackageNo*sizeof(CDITcpPackage));

        /* change memory */
        if ((prgCDITcpPackage = (CDITcpPackage*)realloc(prgCDITcpPackage, igCDITcpPackageNo*sizeof(CDITcpPackage))) == NULL)
        {
           dbg(TRACE,"<PullTCPPackage> realloc failure...");
           return iCDI_TERMINATE;
        }
     }
     else
     {
        free((void*)prgCDITcpPackage);
        prgCDITcpPackage = NULL;
     }

     /* check restore version and set some structure elements */
     /* very very very very important!!! */
     (void)CheckVersion();
  }

  dbg(DEBUG,"<PullTCPPackage> ----- END -----");

  /* bye bye */
  return ilBytes;
}

/******************************************************************************/
/* The CombineData routine                                                    */
/******************************************************************************/
static int CombineData(CDITable *prpTabPtr, char *pcpFieldList, char *pcpData)
{
  int   i;
  int	ilLen=0;
  int   ilCount=0;
	int		ilCurRule;
	int		ilCurField;
  int   ilExit = FALSE;
  int   ilIndex = 0;
  int   ilStartPos;
  int   ilPartLen;
  int   ilDataLen;
	char		pclCurRuleField[iMIN_BUF_SIZE];
  char	*pclTmpBuf=NULL;
  char  *pclPtr=NULL;

	/* for all rules */
  for (ilCurRule=0; ilCurRule<prpTabPtr->iNoCombineRules; ilCurRule++) {

    for(ilCurField=0; ilCurField < prpTabPtr->piNoOfRuleFields[ilCurRule]; ilCurField++){
      ilLen  += prpTabPtr->piPositions[ilCurRule][ilCurField][iRULE_LENGTH];
    }

    if (( pclPtr = (char*)malloc(ilLen+5)) == NULL){
      dbg(TRACE,"<CombineData> Error failed to malloc <%d>  ",ilLen+5); 
      return RC_FAIL;
    } else {
      memset(pclPtr,0x00,ilLen+5);
      prpTabPtr->pcCombineResultData[ilCurRule] = pclPtr;

    }
    pclTmpBuf = prpTabPtr->pcCombineResultData[ilCurRule];

    ilExit = FALSE;
		/* for all rule fields... */
    for (ilCurField=0, ilCount=0; ilExit == FALSE && ilCurField < prpTabPtr->piNoOfRuleFields[ilCurRule]; ilCurField++) {
      ilStartPos = prpTabPtr->piPositions[ilCurRule][ilCurField][iRULE_START_POS];
      ilPartLen = prpTabPtr->piPositions[ilCurRule][ilCurField][iRULE_LENGTH];
			/* copy current rule field to local buffer */
			strcpy(pclCurRuleField, GetDataField(prpTabPtr->pcCombineFields[ilCurRule], ilCurField, cCOMMA));
      /* check if field is jokerfield (with no data)*/
      if(pclCurRuleField[0] == '"'){
	strcpy(&pclTmpBuf[ilCount],&pclCurRuleField[1]);
	i = strlen(&pclTmpBuf[ilCount]);
	ilCount += i-1;	   
	pclTmpBuf[ilCount] = 0x00;
      } else {
			/* so get data for this field... */
	ilIndex = GetIndex(pcpFieldList, pclCurRuleField, cCOMMA);
	if ( ilIndex < 0){
	  /* Field not in list */
	  ilExit = TRUE;
	} else {
	  /* Get data for current field*/
	  pclPtr = CDIGetDataField(pcpData,ilIndex,cCOMMA, prpTabPtr->iEmptyFieldHandling);
	  ilDataLen = strlen(pclPtr);
	  if ( ilDataLen < ilStartPos){
	    /* current datalen to small for demand */
	    ilExit = TRUE;
	  } else if (ilDataLen < ilStartPos+ilPartLen) {
	    strcpy(&pclTmpBuf[ilCount], &pclPtr[ilStartPos]); 
	    /* number chars added ?*/
	    ilCount += ilDataLen-(ilStartPos); 
	    /* add blanks at end of string*/
	    for(i=0;i < ilStartPos+ilPartLen-ilDataLen;i++){
	      pclTmpBuf[ilCount] = ' ';
	      ilCount++;
			}
	  } else {
	    /* Datalen is long enough*/
	    strncpy(&pclTmpBuf[ilCount], &pclPtr[ilStartPos],ilPartLen); 
	    ilCount += ilPartLen;
		}
	} /* end of index < 0 */
	if (ilExit == TRUE){
	  free(prpTabPtr->pcCombineResultData[ilCurRule]);
	  prpTabPtr->pcCombineResultData[ilCurRule] = NULL;
	}
      } /* end of if jokerfield */
    } /* end of "for"  all rule fields*/
  } /* end of "for" all rules */
	/* bye bye */
	return RC_SUCCESS;
}
/******************************************************************************/
/* The TriggerFtpHdl routine                                                    */
/******************************************************************************/
static int TriggerFtpHdl(struct _CDICmd *prpCDICmd,CDITable *prpCDITable)
{
	int ilRc = RC_SUCCESS;
	BC_HEAD		rlBCHead;
	CMDBLK		rlCmdblk;
	char pclRemoteFile[iMIN];
	char clTmpData[sizeof(FTPConfig)+100];
	
	dbg(DEBUG,"<TriggerFtpHdl>: call FTPHDL for <%s> to host <%s>.",prpCDICmd->pcFileName,rgTM.pcMachine);

	/* set BC-Head members */
	memset((void*)&rlBCHead, 0x00, sizeof(BC_HEAD));
	rlBCHead.rc = RC_SUCCESS;
	strncpy(rlBCHead.dest_name, pcgProcessName, 10);
	strncpy(rlBCHead.recv_name, "EXCO", 10);

	/* set CMDBLK members */
	memset((void*)&rlCmdblk, 0x00, sizeof(CMDBLK));
	strncpy(rlCmdblk.command, "FTP", 3);
	/* we use the filename to generate the FINA-telegram at receiving the WEOD command from ftphdl */
	if (rgTM.iWriteFINATelegram == 1)
	{
		sprintf(rlCmdblk.tw_start, "WEOD,%s,%s",prpCDICmd->pcFileName,prpCDITable->pcDataType);
	}
	else
	{
	sprintf(rlCmdblk.tw_start, "WEOD,%s",pcgTwStart);
	}
	sprintf(rlCmdblk.tw_end, "%s,%s,%s", prpCDICmd->pcHomeAirport, prpCDICmd->pcTableExtension, pcgProcessName);

	memset(pclRemoteFile,0x00,iMIN);
	sprintf(pclRemoteFile,"%s.DTM",prpCDICmd->pcRFileName);

	/*****************************************************/
	/* now filling the hyper-dynamic FTPConfig-structure */
	/*****************************************************/
	memset(&prgFtp,0x00,sizeof(FTPConfig));

	prgFtp.iFtpRC = FTP_SUCCESS; /* Returncode of FTP-transmission */
	strcpy(prgFtp.pcCmd,"FTP");
	strcpy(prgFtp.pcHostName,rgTM.pcMachine);
	strcpy(prgFtp.pcUser,rgTM.pcFTPUser);
	strcpy(prgFtp.pcPasswd,rgTM.pcFTPPass);
	prgFtp.cTransferType = CEDA_BINARY;
	prgFtp.iTimeout = 1; /* for tcp_waittimeout */
	prgFtp.lGetFileTimer = 1; /* timeout (microseconds) for receiving data */
	prgFtp.lReceiveTimer = 1; /* timeout (microseconds) for receiving data */
	prgFtp.iDebugLevel = debug_level; /* Debugging level of FTPHDL during operation */
	if (strcmp(rgTM.pcMachineType,"WIN") == 0)
	{
		prgFtp.iClientOS = OS_WIN; /* Operating system of client */
	}
	if (strcmp(rgTM.pcMachineType,"UNIX") == 0)
	{
		prgFtp.iClientOS = OS_UNIX; /* Operating system of client */
	}
	prgFtp.iServerOS = OS_UNIX; /* Operating system of server */
	prgFtp.iRetryCounter = 0; /* number of retries */
	prgFtp.iDeleteRemoteSourceFile = 0; /* yes=1 no=0 */
	prgFtp.iDeleteLocalSourceFile = 0; /* yes=1 no=0 */
	prgFtp.iSectionType = iSEND; /* iSEND or iRECEIVE */
	prgFtp.iInvalidOffset = 0; /* time (min.) the section will be set invalid, after unsuccessfull retry */
	prgFtp.iTryAgainOffset = 0; /* time between retries */
	strcpy(prgFtp.pcHomeAirport,pcgHomeAP); /* HomeAirport */
	strcpy(prgFtp.pcTableExtension,pcgTABEnd); /* Table extension */

	/* local path for FTP */
	strcpy(prgFtp.pcLocalFilePath,prpCDICmd->pcLocalFilePath);
	/* local filename for FTP */
	strcpy(prgFtp.pcLocalFileName,prpCDICmd->pcFileName);

	/* remote path for FTP */
	strcpy(prgFtp.pcRemoteFilePath,prpCDICmd->pcRemoteFilePath);
	/* remote filename during FTP-transmission */
	strcpy(prgFtp.pcRemoteFileName,pclRemoteFile);
	/* remote filename after successfull FTP-transmission */
	sprintf(prgFtp.pcRenameRemoteFile,"%s",prpCDICmd->pcRFileName);

	prgFtp.iDeleteLocalSourceFile = 0; /* files will not be deleted */
	prgFtp.iSendAnswer = 1; /* yes=1 no=0 */ 
	prgFtp.cStructureCode = CEDA_FILE;
	prgFtp.cTransferMode = CEDA_STREAM;
	prgFtp.iStoreType = CEDA_CREATE;
	prgFtp.data[0] = 0x00;

	/*snap((char*)&prgFtp,sizeof(FTPConfig),outp);*/
	dbg(debug_level,"<TriggerFtpHdl>: --- FTPHDL-dynamic configuration START -->");
	DebugPrintFTPConfig(debug_level, &prgFtp);
	dbg(debug_level,"<TriggerFtpHdl>: --- FTPHDL-dynamic configuration END   -->");

#ifdef WMQ
  ConvertFTPConfigToDatalist(clTmpData,prgFtp);

	/* write request to QUE */
	if ((ilRc = SendToQue(prpCDICmd->iFileCreateModID, PRIORITY_4, &rlBCHead, &rlCmdblk, "", "", clTmpData,(char *) NULL, 0)) == iCDI_TERMINATE)
#else
	if ((ilRc = SendToQue(prpCDICmd->iFileCreateModID, PRIORITY_4, &rlBCHead, &rlCmdblk, "", "", "DYN",(char *) &prgFtp, sizeof(FTPConfig))) == iCDI_TERMINATE)
#endif
	{
		dbg(TRACE,"<SCAFC> SendToQue returns <%s>",  RETURN(ilRc));
		dbg(DEBUG,"<SCAFC> ----- END -----");
		return ilRc;
	}
	return ilRc;
}
/******************************************************************************/
/* The SendFileToQue routine                                                    */
/******************************************************************************/
static int SendFileToQue(struct _CDICmd *prpCDICmd)
{
	int			ilRC;
	long			llFileLen;
	char			*pclFileContents = NULL;
	char			pclTmpBuf[2*iMIN_BUF_SIZE];
	BC_HEAD		rlBCHead;
	CMDBLK		rlCmdblk;

	dbg(DEBUG,"<SendFileToQue> ----- START -----");

	sprintf(pclTmpBuf,"%s/%s", prpCDICmd->pcLocalFilePath, prpCDICmd->pcFileName);
	dbg(DEBUG,"<SendFileToQue> sending file <%s> to queue %d", pclTmpBuf, prpCDICmd->iSendFileToQue);

	if ((ilRC = tool_fn_to_buf(pclTmpBuf, &llFileLen, &pclFileContents)) != RC_SUCCESS)
	{
		dbg(TRACE,"<SendFileToQue> SendToQue returns <%s>",  RETURN(ilRC));
		dbg(DEBUG,"<SendFileToQue> ----- END -----");
		return ilRC;
	}

	/* set BC-Head members */
	memset((void*)&rlBCHead, 0x00, sizeof(BC_HEAD));
	rlBCHead.rc = RC_SUCCESS;
	strncpy(rlBCHead.dest_name, pcgProcessName, 10);
	strncpy(rlBCHead.recv_name, "EXCO", 10);

	/* set CMDBLK members */
	memset((void*)&rlCmdblk, 0x00, sizeof(CMDBLK));
	strncpy(rlCmdblk.command, prpCDICmd->pcSendFileCmd, 6);
	sprintf(rlCmdblk.tw_start, "EOD5");
	sprintf(rlCmdblk.tw_end, "%s,%s,%s", prpCDICmd->pcHomeAirport, prpCDICmd->pcTableExtension, pcgProcessName);

	/* write request to QUE */
	if ((ilRC = SendToQue(prpCDICmd->iSendFileToQue, PRIORITY_4, &rlBCHead, &rlCmdblk, "", "", pclFileContents,NULL,0)) == iCDI_TERMINATE)
	{
		/* delete memory */
		if (pclFileContents != NULL)
			free(pclFileContents);

		dbg(TRACE,"<SendFileToQue> SendToQue returns <%s>",  RETURN(ilRC));
		dbg(DEBUG,"<SendFileToQue> ----- END -----");
		return ilRC;
	}

	/* delete memory */
	if (pclFileContents != NULL)
		free(pclFileContents);

	dbg(DEBUG,"<SendFileToQue> ----- END -----");

	/* bye bye */
	return RC_SUCCESS;
}

/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */

/*
	Special Quick-Hack for Shanghai, all following function are originally 
	library-function. The originl versions can be found in the helpful.c 
	library-source-file. 
	As a sign for an empty field, it's necessary to send blanks to the 
	Client-systems in Shanghai. These client-systems are obviously not able
	to handle 'real' empty fields. F#*k this....
*/

/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// CDIGetDataField ->
returns a field spcified with field-number to the calling routine 
*/
static char *CDIGetDataField(char *s1, UINT No, char c, int ipFlag)
{
	UINT					i = 0;
	char					*pclS;
	static char			pclTmpBuf[iMAXIMUM];

	if (s1 == NULL)
		return NULL;

	/* clear buffer */
	memset ((void*)pclTmpBuf, 0x00, iMAXIMUM);
	pclS = pclTmpBuf;

	/* search the field */	
	while (*s1)
	{
		if (*s1 == c)
			i++; 
		else if (i == No)
		{
			while ((*s1 != c) && *s1)
				*pclS++ = *s1++;
			break;
		}
		s1++;
	}
	*pclS = 0x00;

	/* delete all blanks at end of string... */
	/* ...or keep the data untouched, it depends on the flag only */
	if (ipFlag == iDELETE_ALL_BLANKS || ipFlag == iONE_BLANK_REMAINING)
	{
		while (pclS > pclTmpBuf && *--pclS == cBLANK)
			*pclS = 0x00;
		if (ipFlag == iONE_BLANK_REMAINING)
		{
			if (!strlen(pclS))
			{
				/* 
					if there isn't any character left, we must attach 
					at least on blank 
				*/
				*pclS++ = cBLANK;
				*pclS = 0x00;
			}
		}
	}

	return pclTmpBuf;
}

/* Reset timer for CCO Interval, only send CCO if interval
is over without any reception inbetween */

void ResetCCOInterval ()
{
	/* current time */
	time1 = time(NULL);
}



/*/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////// CDICopyNextField, separator is char 
*/
static char *CDICopyNextField(char *s1, char c, char *s3, int ipFlag)
{
	char		*pclS;

	/* check params */
	if (s1 == NULL || s3 == NULL)
		return NULL;

	/* save start address */
	pclS = s3;

	/* copy to buffer */
	while (*s1 != c && *s1)
		*s3++ = *s1++;
	*s3 = 0x00;
	s1++;
	
	/* delete all blanks at end of string... */
	/* ...or keep the data untouched, it depends on the flag only */
	if (ipFlag == iDELETE_ALL_BLANKS || ipFlag == iONE_BLANK_REMAINING)
	{
		while (s3 > pclS && *--s3 == cBLANK)
			*s3 = 0x00;
		if (ipFlag == iONE_BLANK_REMAINING)
		{
			if (!strlen(s3))
			{
				/* 
					if there isn't any character left, we must attach 
					at least on blank 
				*/
				*s3++ = cBLANK;
				*s3 = 0x00;
			}
		}
	}
	/* bye bye */
	return s1;
}
/******************************************************************************
PRF 6260: 
Function   :CleanData()
						Returnvalue:none
Description:Replaces unallowed bytes from the original telegram (like data-sep.)
******************************************************************************/
static void CleanData(char *pcpMsg,int ipMsgLen,char cpChar, char cpRepl)
{
	int ilCnt = 0;
	if (ipMsgLen==0)
	{
		ipMsgLen=strlen(pcpMsg);
	}
	while(ilCnt < ipMsgLen)
	{
	 if (pcpMsg[ilCnt]==cpChar)
		 {
			 dbg(DEBUG,"<CleanData> Replace <%c> with <%c> in string <%s>",cpChar,cpRepl,pcpMsg);
			 pcpMsg[ilCnt]=cpRepl;
		 }
		 ilCnt++;
	}
}


static int ChangeCharFromTo(char *pcpData,char *pcpFrom,char *pcpTo)
{
  int ilRC = RC_SUCCESS;
  int i;
  char *pclData;

  if (pcpData == NULL)
     return -1;
  else if (pcpFrom == NULL)
     return -2;
  else if (pcpTo == NULL)
     return -3;
  else
  {
     pclData = pcpData;
     while (*pclData != 0x00)
     {
        for (i = 0; pcpFrom[i] != 0x00; i++)
        {
           if (pcpFrom[i] == *pclData)
           {
              *pclData = pcpTo[i];
           }
        }
        pclData++;
     }
  }

  return ilRC;
} /* End of ChangeCharFromTo */


static void TrimRight(char *s)
{    /* search for last non-space character */
    int i = 0;
    for (i = strlen(s) - 1; i >= 0 && isspace(s[i]); i--);
	s[++i] = '\0'; /* trim off right spaces */
}

static int LocalToUtc(char *pcpTime)
{
	int ilRc = RC_SUCCESS;
	char *pclTdi;
	char clLocal[32];

	strcpy(clLocal,pcpTime);
	pclTdi = (strcmp(pcpTime,cgTich) < 0) ? cgTdi1 : cgTdi2;

	AddSecondsToCEDATime(pcpTime,(time_t)0-atol(pclTdi),1);
	dbg(DEBUG,"2UTC  : <%s> ",pcpTime);

	return ilRc;
}

/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

