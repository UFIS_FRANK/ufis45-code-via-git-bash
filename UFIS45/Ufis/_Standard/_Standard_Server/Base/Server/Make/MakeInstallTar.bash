# $Id: Ufis/_Standard/_Standard_Server/Base/Server/Make/MakeInstallTar.bash 1.4 2006/09/28 20:21:26SGT jim Exp  $
#
# 20020605 JIM: allow members of GROUP 'ceda' to walk through directory tree
# 20020605 JIM: create basic links (flight, sqlhdl, rbshdl) in any case
# 20020617 JIM: check for links to create: do they exist?
# 20020617 JIM: /Bin/ replaced by /Bin`uname-s`/
# 20020617 JIM: /ceda/InstallDir replaced by ./TmpInstallDir
# 20020703 JIM: changing rights for ./TmpInstallDir/ceda/* before tar instead 
#               of changing rights for /ceda/* after installation
# 20020729 JIM: TAR-file with OS-Version and Date-String
# 20030219 JIM: Process-binaeries are now stored in $B only (not in $S/Bin_...)
# 20030220 JIM: repaired 20030219: "${S}/${B}*" ==> "${B}/*"
# 20030325 JIM: automatically add ". etc/UfisEnv" to .profile of ceda
# 20030624 JIM: cleanup: "rm -rf ./TmpInstallDir/"
#

case $SHELL in
   *bash)   ECHO="echo -e"
           ;;
   *ksh)    ECHO="echo "
           ;;
esac


case $1 in
   -?|--?|-h|--help)
   cat <<EOF

    MakeInstallTar: anlegen der Verzeichnis-Struktur in './TmpInstallDir/ceda/'
    fuer eine CEDA-Installation mit den Binaries aus \$B des aktuell gesetzten 
    Projects und der Kunden-Konfiguration aus \$CFC.
    Die vorhandenen Links in '/ceda/bin/' werden als Vorlage fuer Links in
    './TmpInstallDir/ceda/bin/' benutzt.

    Aufruf:
       "MakeInstallTar.bash"
    
EOF
   return 2>/dev/null # avoid exit if called with '.'
   exit
   ;;
esac

if [ ! -d "${S}" ] ; then
   echo
   echo "Sorry, aber anscheinend ist kein \$S (SandboxXXX/yyy) gesetzt"
   echo
   return 2>/dev/null # avoid exit if called with '.'
   exit
fi

MY_BUILD_DIR=`pwd`

DIRS="bin conf debug/old_prot debug/size etc runtime tmp \
      exco/FILES exco/sita/new exco/sita/arc "
TARFILE=CedaInstall.`uname -s`.`date +%Y%m%d_%H%M`.tar

function BuildInstallTree
{
# Create directories
rm -rf ./TmpInstallDir/*
for d in ${DIRS}
do
   if [ ! -d ./TmpInstallDir/ceda/$d ] ; then
      mkdir -p ./TmpInstallDir/ceda/$d
   else
      echo Directory \'./TmpInstallDir/ceda/$d\' existiert bereits!
   fi
done
} # end BuildInstallTree

function CopyCedaFiles
{
   # copy CEDA processes
   for f in ${B}/* 
   do
      if [ -f $f ] ; then
         cp $f ./TmpInstallDir/ceda/bin/.
      fi
   done

   # copy tools and scripts
   for f in ${TB}/* ${TS}/Scripts/*
   do
      if [ -f $f ] ; then
         cp $f ./TmpInstallDir/ceda/etc/.
      fi
   done
 
   # copy configuration
   for f in ${CFC}/*
   do
      if [ -f $f ] ; then
         cp $f ./TmpInstallDir/ceda/conf/.
      fi
   done
 
   # copy Heusenstamm configuration
   mkdir ./TmpInstallDir/ceda/conf/NotTested
   for f in ${CFD}/*
   do
      if [ -f $f ] ; then
         cp $f ./TmpInstallDir/ceda/conf/NotTested/.
      fi
   done
 
   # create links
   cd ${MY_BUILD_DIR}/TmpInstallDir/ceda/bin
   for f in `find /ceda/bin -type l`
   do
      # for all links in /ceda/bin get filename
      FILE=`basename $f`
      # list this filename with the link and get the link
      LINK=`ls -l $f | awk '{p=index(\$0,"->")+3 ; print substr(\$0,p)}'`
      # remove the executable '*' sign
      ### echo "LINK1="`echo ${LINK} `
      LINK1=`echo ${LINK} | awk '{p=index(\$0,"*")-1 ; if (p>0 ) {print substr(\$0,1,p)} else {print $0} }'`
      ### echo "f: <$f>, FILE: $FILE, LINK: ${LINK}, LINK1: ${LINK1}" >>${MY_BUILD_DIR}/LinkCheck.txt
      # this is what to do
      if [ -x `basename ${LINK1}` ] ; then
         ### echo "ln -s `basename ${LINK1}` ${FILE} " >>${MY_BUILD_DIR}/LinkCheck.txt
         ln -s `basename ${LINK1}` ${FILE}
      fi
   done
   # create some flight links in any case
   for f in fligro1 fligro2 flisea ; do
      if [ ! -h $f ] ; then
         if [ -x flight ] ; then
            ln -s flight $f
         fi
      fi
   done
   # create some sqlhdl links in any case
   for f in sqlhdl1 sqlhdl2 sqlhdl3 sqlhdl4 ; do
      if [ ! -h $f ] ; then
         if [ -x sqlhdl ] ; then
            ln -s sqlhdl $f
         fi
      fi
   done
   # create scbhdl link in any case
   if [ ! -h xbshdl ] ; then
      if [ -x scbhdl ] ; then
         ln -s scbhdl xbshdl
      fi
   fi
   
} # CopyCedaFiles

function ComplainMissingDirs
{
# complain missing directories
cd ${MY_BUILD_DIR}/TmpInstallDir/ceda/conf
CONF_CHECK=${MY_BUILD_DIR}/TmpInstallDir
for c in *.cfg ; do
#grep config file for all aktive lines containing /ceda/ and assume the 3. parameter as file name
for f in `grep "[^#].*/ceda/" $c | awk '{print \$3}'`
do
   case $f in
      *.*) f=`dirname $f`
      ;;
   esac
   if [ ! -d ${CONF_CHECK}$f ] ; then
      echo "Directory TmpInstallDir$f exisiert nicht (wird in $c benannt)"
   fi
done
done
} # ComplainMissingDirs

function NoteOnConfig
{
cat <<EOF

 please check in sgs.tab: 
    HOPO:       'EXTAB SYS,HOMEAP,...' 
    net mask:   'EXTAB NET,MASK,...'
    broadcast:  'EXTAB NET,BCADDR,...'

 in *.cfg:
    HOPO
    HOME

 in bchdl.cfg:
    all IP-Adresses, i.e with:
      echo ac1e0106 | awk '{a="0x"substr(\$1,1,2) ; 
                            b="0x"substr(\$1,3,2) ; 
		            c="0x"substr(\$1,5,2) ; 
		            d="0x"substr(\$1,7,2) ; 
		            printf "%d.%d.%d.%d\n",\
		            strtonum(a),strtonum(b),strtonum(c),strtonum(d)}'  
EOF
} # end NoteOnConfig

function MakeTar
{
# tar: create/verbose/follow links/tarfilename file-name directories
# (tested on SunOS)
  cd ${MY_BUILD_DIR}/TmpInstallDir
  tar cvlf ${TARFILE} ceda
  compress ${TARFILE}
} # end MakeTar

function ModifyAccessRights
{
      # allow members of GROUP 'ceda' to overwrite files of CEDA
      chmod -R ug+rw ceda
      # allow members of GROUP 'ceda' to execute files of CEDA/etc
      chmod -R g+x ceda/etc
      if [ -f "ceda/.[^.]?*" -o -d  "ceda/.[^.]?*" ] ; then
    	 # do not allow members of GROUP 'ceda' to overwrite DOT files
    	 # of CEDA (i.e. /ceda/.rhosts must be read-only for USER 'ceda')
    	 chmod -R og-rwx ceda/.[^.]?*
      fi
      # allow members of GROUP 'ceda' to walk through the directory tree:
      find ceda -type d -exec chmod g+x {} \;
}

function MakeInstallScript
{
 cd ${MY_BUILD_DIR}
 cat <<EOF

   FILE=${TARFILE}
   if [ ! -z "\$1" ] ; then
      FILE=\$1
   fi
   if [ -f \${FILE}.Z ] ; then
      uncompress \${FILE}
      # untar Ceda Tar File:
      tar xvlf \${FILE}
      # allow members of GROUP 'ceda' to overwrite files of CEDA
      chmod -R ug+rw ceda
      # allow members of GROUP 'ceda' to execute files of CEDA/etc
      chmod -R g+x ceda/etc
      if [ -f "ceda/.[^.]?*" -o -d  "ceda/.[^.]?*" ] ; then
    	 # do not allow members of GROUP 'ceda' to overwrite DOT files
    	 # of CEDA (i.e. /ceda/.rhosts must be read-only for USER 'ceda')
    	 chmod -R og-rwx ceda/.[^.]?*
      fi
      # allow members of GROUP 'ceda' to walk through the directory tree:
      find ceda -type d -exec chmod g+x {} \;
 
      if [ -f ceda/.profile ] ; then
    	 # modify ceda/.profile:
    	 cnt=\`grep -c UfisEnv ceda/.profile\`
    	 if [ \$cnt == 0 ] ; then
    	    echo "if [ -f etc/UfisEnv ] ; then " >>ceda/.profile
    	    echo "  . etc/UfisEnv" 	             >>ceda/.profile
    	    echo "fi"					                   >>ceda/.profile
    	 fi
    	 cnt=\`grep -c /sandbox/common/common_profile ceda/.profile\`
    	 if [ \$cnt == 0 ] ; then
    	    echo "if [ -f /sandbox/common/common_profile ] ; then " >>ceda/.profile
    	    echo "  . /sandbox/common/common_profile" 	      >>ceda/.profile
    	    echo "fi"					      >>ceda/.profile
    	 fi
      else
    	 echo Konnte 'ceda/.profile' nicht finden.
    	 echo Auf InHouse-Maschinen bitte im 'ceda/.profile' noch folgende 3 Zeilen eintragen:
    	 echo
    	 echo "if [ -f /sandbox/common/common_profile ] ; then "
    	 echo "  . /sandbox/common/common_profile"
    	 echo "fi"
    	 echo
      fi
   else
      echo "Datei \${FILE} nicht gefunden!"
      echo "Aufruf:   \$0 <datei>"
      echo "Als Default wird datei=${TARFILE} benutzt, wobei CedaInstall.tar compressed "
      echo "sein muss"
   fi

EOF

} # end MakeInstallScript

BuildInstallTree
cd ${MY_BUILD_DIR}
CopyCedaFiles
cd ${MY_BUILD_DIR}
ComplainMissingDirs 2>&1 | sort -u | tee ./TmpInstallDir/ceda/DirCheck.txt
cd ${MY_BUILD_DIR}
MakeInstallScript > ./TmpInstallDir/ceda/InstallScript.bash
cd ${MY_BUILD_DIR}/TmpInstallDir
ModifyAccessRights
cd ${MY_BUILD_DIR}
MakeTar
cd ${MY_BUILD_DIR}
cp ./TmpInstallDir/ceda/DirCheck.txt ./TmpInstallDir/ceda/InstallScript.bash ./TmpInstallDir/

cat <<EO_COMMENT

   Auszuliefern sind './TmpInstallDir/${TARFILE}.Z' und 
                     './TmpInstallDir/InstallScript.bash'
   In './TmpInstallDir/DirCheck.txt' ist die Liste der Directories, auf die in
   /ceda/conf/*.cfg verwiesen wird.

EO_COMMENT
