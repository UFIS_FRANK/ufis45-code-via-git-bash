#ifndef _DEF_mks_version_multiapt_c
  #define _DEF_mks_version_multiapt_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_multiapt_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Dblib/multiapt.c 1.2 2004/08/10 20:41:22SGT jim Exp  $";
#endif /* _DEF_mks_version */

#include <string.h>
#include "multiapt.h"
#include <glbdef.h>
#include <syslib.h>

extern char *pcgTwEnd; 


/* GSo 22/08/2000 see header file for description */
int CheckHopoUsage(char * pcpTableName)
{
  int  ilRc            = RC_SUCCESS;
  int  ilSearchResult  = 0;
  int  pilCount        = 1;
  char pclResult[5]    = ""; /* Should be big enough, actually i may get only 1 character */
  char pclTableName[8]= "";
	
	strncpy(pclTableName,pcpTableName,3);
	pclTableName[3] = 0x00;
  
	/* dbg(DEBUG,"<CheckHopoUsage> Start function ..."); */
  ilSearchResult = syslibSearchDbData("TABTAB", "LTNA", (char *)pclTableName, "IGHO", 
        (char *)pclResult, (int *)&pilCount, "");
/*
	dbg(DEBUG,"<CheckHopoUsage> Table Name %s ", pcpTableName);
	dbg(DEBUG,"<CheckHopoUsage> Result %s ", pclResult);
	dbg(DEBUG,"<CheckHopoUsage> Count %d ", pilCount);
	dbg(DEBUG,"<CheckHopoUsage> %d, %d",ilSearchResult, RC_FAIL);
*/ 
  if (ilSearchResult == RC_SUCCESS)
  {
    if (pilCount > 1) 
      return RC_NOT_FOUND; /* Something has gone wrong i have to get just one character */
		/* dbg(DEBUG,"<CheckHopoUsage> Data retreived %s",pclResult);	 */
  }
  else
    return RC_NOT_FOUND; /* Something has gone wrong internally the syslib function
                       got to drill-down the specific error ? */
    
	/* dbg(DEBUG,"<CheckHopoUsage> Got Value %s",pclResult); */
	/* dbg(DEBUG,"<CheckHopoUsage> End function ...");       */
  return (pclResult[0] != 'Y') ? RC_SUCCESS : RC_FAIL;
}

/* GSo 22/08/2000 see header file for description */
int CollectNonHopoTables(char * pcpListOfTables)
{
  int   ilRc            = RC_SUCCESS;
  int   ilSearchResult  = 0;
  int   pilCount        = 0;  /* Don't know actually how many records i might get */
	/* char  pclSrcFor[] = " "; */
  
	/* dbg(DEBUG,"<CollectNonHopoTables> Start function ..."); */
 
  ilSearchResult = syslibSearchDbData("TABTAB", "IGHO", (char *)" ", "LTNA", 
      (char *)pcpListOfTables, (int*)&pilCount, ",");

	/* dbg(DEBUG,"<CollectNonHopoTables> End function ..."); */
  return ilSearchResult;  
}


int GetHomePort(char *szBuffer)
{
	/*Extract the HOPO from pcgTwEnd field*/
	int ilRC;
	int iLenHopo;
	
	dbg(DEBUG,"<GetHomePort> Start Function");
	/*Take the first element in the variable*/
	iLenHopo = get_real_item(szBuffer,pcgTwEnd,1);

	if (iLenHopo == 3)
	{
		ilRC = RC_SUCCESS; /*The lenght of the string is the same of 3 LC*/
		dbg(DEBUG,"<GetHomePort> Home airport is --> %s",szBuffer);
	}
	else
	{
		ilRC = RC_FAIL; /*The lenght of the string is different of 3 LC*/
		dbg(DEBUG,"<GetHomePort> The wrong home airport is --> %s",szBuffer);
	}

	dbg(DEBUG,"<GetHomePort> End Function");
	return ilRC;
}


int CheckAPC3(char *szApc3)
{
	/*Check if the hopo is present in the database*/
	int		ilRC;
	int		ilCount;
	char	szlOutData[11];
	
	dbg(DEBUG,"<CheckAPC3> Start Function");
	ilCount = 1;
	memset(szlOutData,0x00,sizeof(szlOutData));
	ilRC = syslibSearchDbData("APTTAB","APC3", szApc3, "URNO",szlOutData,&ilCount,""); 

	if (ilRC == RC_NOT_FOUND) /* The were no data found. */
		ilRC = RC_FAIL;
	
	dbg(DEBUG,"<CheckAPC3> End Function");
	return ilRC;
}

