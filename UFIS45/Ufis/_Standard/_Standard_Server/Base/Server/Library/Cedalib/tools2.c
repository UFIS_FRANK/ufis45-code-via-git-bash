#ifndef _DEF_mks_version_tools2_c
  #define _DEF_mks_version_tools2_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_tools2_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/tools2.c 1.2 2004/08/10 20:29:55SGT jim Exp  $";
#endif /* _DEF_mks_version */
static   char  sccs_version_tools2_lib[] = "@(#) UFIS 4.4 (c) ABB AAT/I tools2.c  44.3 / 00/01/07 14:35:42 / JMU";

#define STH_USE
#define STH_IS_USED_IN_LIB

#include <sys/types.h>
#include <sys/stat.h>

#if defined(_SNI) || defined(_SOLARIS) || defined(_HPUX_SOURCE)
#define OWN_NAP
#endif

/* Define OWN_NAP, if the nap function is not supported by the unix */
#ifdef OWN_NAP
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>
#include <sys/param.h>
#endif


#include "tools.h"
#include "router.h"
#include "glbdef.h"
#include "initdef.h"
#include "netin.h"
/*	#include "netout.h"	*/
#include "namsrv.h"
/*	#include "rsaif.h"	*/

#ifndef	MAX_SELECTION_LEN
#define	MAX_SELECTION_LEN		1024
#endif

extern int iStrup(char *s);
extern char *sGet_word(char *s,short FldNum);
long		MakeTimeLong(char *dbtime,char *dbdate);
int		GetDate(int day, char *datebuf,char *timebuf);
int		HandleHSBItem(int,int);
extern int errno;
extern int mod_id;
extern FILE *outp;

/* ******************************************************************** */
/* The tool_get_q_own routine						*/
/* returns the QCP-ID for the given process name or RC_NOT_FOUND or	*/
/* RC_FAIL								*/
/* ******************************************************************** */
int tool_get_q_own (int route,char *Pnam)
{
	int	rc = RC_SUCCESS;	/* Return code 			*/
 	int	recno=0;
 	char	buf[30];
 	int	cnt=0;
 	int	ready = FALSE;
 	int	i,
		introute;

 	for (i=0;i<6;i++)
 	{
  		rc = sgs_get_record (PNTAB, recno, buf);
 	}

 	rc = sgs_get_no_of_record (PNTAB, buf);

 	if (rc > 0)
 	{
  		cnt = rc;
  		rc = RC_SUCCESS;
 	}
 	else
 	{
  		rc = RC_FAIL;
 	} /* fi */

 	for (recno=0; ! ready && rc == RC_SUCCESS && recno < cnt; recno++)
 	{	
  		rc = sgs_get_record (PNTAB, recno, buf);
  		if (rc == RC_SUCCESS)
  		{
#ifdef _SNI
        		introute = *((short *) (buf+8));
#else
			introute = *((int *) (buf+8));
#endif
			if (introute == route) {
				ready = TRUE;
				strncpy(Pnam,buf,8);
			}
	
     		 } /* fi */
 	 } /* for */
  
 	if (rc != RC_SUCCESS)
 	{
  		dbg (TRACE,"tool_get_q_id: returned %d Pnam=%s cnt=%d",rc,Pnam,cnt);
 	}

 	return rc;
} /* tool_get_q_id */

/*	***************************************************************	*/
/*      GetDate								*/
/*      Errechnet aus dem aktuelle Datum und dem Increment/Dekrement	*/
/*	des day und gibt in im CEDA-Format zur\374ck. 			*/
/*      Input:  int             Inkrement oder Dekrement		*/
/*                      char *  Resultat der Berechnung			*/
/*      Output: int             Fehler-Code				*/
/*      ***************************************************************	*/
#ifndef	SEC_PER_DAY
#define	SEC_PER_DAY	86400
#endif
int     GetDate(int day, char *datebuf,char *timebuf)
{
        int             rc = 0;
        long            t = 0;
        struct tm       *tbuf,tm_1;
        char            *t_buf,help[9];

        t_buf = help;
	memset(t_buf,0x00,5);
/*      Errechne den aktuellen Zeitwert und addiere 'day' hinzu		*/
        if (datebuf[0] != 0) {
dbg(DEBUG,"Databuf not emty  <%s>\n",datebuf);
                t = MakeTimeLong(timebuf,datebuf);
dbg(DEBUG,"MakeTimeLong returns %ld\n",t);
	}
	else
        	t = time(&t);
        t += ((long) day * SEC_PER_DAY);

dbg(DEBUG,"Zeit ist %ld, fuer TagInc %d\n",t,day);
        if (t < 0)
                rc = 1;
/*      Wandle den errechneten Wert in eine 'tm'-Struktur um und	*/
/*	fuelle den Puffer 'datebuf' mit den Werten in der Form		*/
/*	'YYYYMMDD'                         				*/

        else {
		memcpy(&tm_1,localtime(&t),sizeof(struct tm));
                /* tbuf = localtime(&t); */
		tbuf = &tm_1;
		strftime(help,9,"m%d",tbuf);
		memcpy(datebuf,help,8);
        }       /*      end else        */
        return rc;
}       /*	end of get_date()	*/
/*      ******************************************************************      */
/*      MakeTimeLong
                */
/*      Serviceroutine, rechnet einen Datums- und/oder Zeiteintrag im Ceda-     */
/*      format in einen Wert vom Type 'long' um.
     */
/*      Input:  char *  Zeiger auf CEDA-Zeitpuffer                                      */
/*                      char *  Zeiger auf CEDA-Datumspuffer
     */
/*      Output: long            Zeitwert oder invertierten Fehler-Code                  */
/*      ******************************************************************      */

long    MakeTimeLong(char *dbtime,char *dbdate)
{
        long                    rc = 0;
        int                     year;
        char                    ch_help[5];
        struct  tm      *tstr1 = NULL, t1, *tstr2 = NULL, t2;

	memset(&t1,0x00,sizeof(struct tm));
	memset(&t2,0x00,sizeof(struct tm));

        tstr1 = &t1;
        tstr2 = &t2;

        if (dbdate == NULL) {
/*      Errechne das aktuelle Datum					*/
                rc = time(&rc);
                memcpy(tstr1,localtime(&rc),sizeof(struct tm));
        }
        else {

/*      Rechne um in tm-Struktur					*/
		memset(ch_help,0x00,5);
                strncpy(ch_help,dbdate,4);
                year = atoi(ch_help);
                tstr1->tm_year = year - 1900;

		memset(ch_help,0x00,5);
                strncpy(ch_help,&dbdate[4],2);
                tstr1->tm_mon = atoi(ch_help) -1;

		memset(ch_help,0x00,5);
                strncpy(ch_help,&dbdate[6],2);
                tstr1->tm_mday = atoi(ch_help);
        	rc = mktime(tstr1);
        }
        if (dbtime != NULL) {

/*      Errechne die aktuelle Zeit					*/
		memset(ch_help,0x00,5);
                strncpy(ch_help,dbtime,2);
                tstr2->tm_hour = atoi(ch_help);

		memset(ch_help,0x00,5);
                strncpy(ch_help,&dbtime[3],2);
                tstr2->tm_min = atoi(ch_help);

		memset(ch_help,0x00,5);
                strncpy(ch_help,&dbtime[6],2);
                tstr2->tm_sec = atoi(ch_help);
        }
        else {
                rc = time((long *) 0);
                memcpy(tstr2,localtime(&rc),sizeof(struct tm));
/*      Rechne um in tm-Struktur					*/
        }
/*      Gib den Wert als 'long' zurueck					*/
        tstr1->tm_hour = tstr2->tm_hour;
        tstr1->tm_min = tstr2->tm_min;
        tstr1->tm_sec = tstr2->tm_sec;
        tstr1->tm_wday = 0;
        tstr1->tm_yday = 0;
        rc = mktime(tstr1);
	dbg(DEBUG,"Time is: '%s'",ctime(&rc));
        return(rc);
}       /*	end of MakeTimeLong()	*/

int     HandleHSBItem(int ipQueId,int ipHsbCom)
{
	int		ilRC = RC_SUCCESS,
			ili;
	char		pclHsbBuf[MAX_SELECTION_LEN];
	ITEM		*pclHsbItem;
	EVENT	*pclHsbEvent;

	pclHsbItem = (ITEM *) pclHsbBuf;
	pclHsbEvent = (EVENT *) pclHsbItem->text;
	for (ili = 0; ili < 5; ili++) {
		ilRC = que(QUE_GETNW,0,ipQueId,PRIORITY_3,MAX_SELECTION_LEN,(char *) pclHsbItem);
		if (ilRC == RC_SUCCESS) {
			ilRC = que(QUE_ACK,0,ipQueId,PRIORITY_3,0,NULL);
			if ( ilRC != RC_SUCCESS ) {	/*	handle que_ack error	*/
dbg(DEBUG,"QUE_ACK error %d for que pclHsbItem->originator ->%d\n",ilRC,pclHsbItem->originator);
			}	/*	end if	*/
			pclHsbEvent = (EVENT *) pclHsbItem->text;
			if (pclHsbEvent->command == ipHsbCom)
				break;
			else {
				ilRC = que(QUE_PUT_FRONT,mod_id,pclHsbItem->originator,pclHsbItem->priority,pclHsbItem->msg_length,(char *) pclHsbEvent);
				if (ilRC != RC_SUCCESS)
dbg(DEBUG,"QUE_PUT_FRONT error %d for que pclHsbItem->originator ->%d\n",ilRC,pclHsbItem->originator);
				else
					ilRC = -1;
				break;
			}
		}
		else if (ilRC == QUE_E_NOMSG)
			nap(250);
		else
			break;
	}
	return ilRC;
}

/* *******************************************************/
/* Space for Appending (Source-Code Control)		*/
/* ********************************************************/
