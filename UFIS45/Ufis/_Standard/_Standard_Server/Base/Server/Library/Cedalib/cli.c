#ifndef _DEF_mks_version_cli_c
  #define _DEF_mks_version_cli_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_cli_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/cli.c 1.2 2004/08/10 20:13:17SGT jim Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/


#define  UGCCS_FKT
#define  SPRED_FKT

#include "cli.h"
#include "tools.h"

static char *pmsk =
"$$$$$$$$:      ########  ########  ########  ########      ****************\n";

/**************************  Function prototype header **********************/
extern  int snap(char *pmem,long lim,FILE *outp);
static  int mskmov(char *dest,char *src,char *msk,char mskchr);
  int mtox(char *dest,char *src,long lim);
extern  int swap(char *p1,char *p2,long l);
extern  int strchk(char *s1,char c,int len);
extern  char *copy(char *dest,char *src,long lim);
extern  char *reverse(char *s,long l);
/****************************************************************************/
extern  char *mod_name;
extern  int debug_level;

extern int snap(char *pmem,long lim,FILE *outp)
{
	register int tsz;
	unsigned long l_adr;
	char outbuf[MAXLINE+1], workbuf[MAXLINE+1], help[sizeof(long)], *s;

	if ( pmem == (char *)0L ) {
		dbg(TRACE,"SNAP: Memory error buffer is NULL pointer\n");
		return -1;
	} /* end if */
	
	
	fprintf(outp,"\n");
	
	while (lim > 0)
              {
              strcpy(outbuf,pmsk);
	      l_adr = (long) pmem;
	      copy(help,(char *) &l_adr,sizeof(long));
	      reverse(help,sizeof(long));
	      mtox(workbuf,help,sizeof(long));
	      mskmov(outbuf,workbuf,pmsk,PCHR);
	      tsz = lim < LINE_CHR ? lim : LINE_CHR;
	      mtox(workbuf,pmem,tsz);
	      mskmov(outbuf,workbuf,pmsk,XCHR);
	      for (lim -= tsz,s = workbuf; tsz > 0; tsz--,pmem++,s++)
		   *s = isprint(*pmem) ? *pmem : '.';
	      *s = '\0';
	      mskmov(outbuf,workbuf,pmsk,CCHR);
	      fprintf(outp,"%s",outbuf);
              }
	      return 1;
}

static mskmov(register char *dest,register char *src,register char *msk,char mskchr)
{
	char *s;
	
	for (s = src; *msk; msk++,dest++)
	     if (*msk == mskchr)
		 *dest = *s ? *s++ : ' ';
	return(s - src);
}

extern char *copy(register char *dest,register char *src,register long lim)
{
	char *s = dest;
	
	if (dest < src)
	    while (lim-- > 0)
		   *dest++ = *src++;
	else
            for (dest += lim, src += lim; lim-- > 0; )
		 *--dest = *--src;
	return(s);
}
	   
int mtox(register char *dest,char *src,register long lim)
{
	register nib;
	int c;
	
	while (lim-- > 0)
              {
	      c = *src++ & 255;
	      *dest++ = ((nib = c >> 4) > 9) ? nib + 'A' - 10 : nib + '0';
	      *dest++ = ((nib = c & 15) > 9) ? nib + 'A' - 10 : nib + '0';
              }
        *dest = '\0';
	return 1;
}

extern char *reverse(register char *s,long l)
{
	register char *p;
	register long temp;
	char *sv = s;
	
	for (p = s+l-1; p > s; p--, s++)
	     temp = *s, *s = *p, *p = temp;
        return(sv);
}
	

extern int swap(register char *p1,register char *p2,long l)
{
	register char t;
	
	while(l-- > 0)
	      t = *p1, *p1++ = *p2, *p2++ = t;
        return 1;
}


extern int strchk(char *s1,char c,int len)
{
	register int	i,rc;
	
	if ( s1 == (char *)0L ) {
		return FATAL;
	} /* end if */
	
	for ( i=0; i<len ; i++ ) {
		if ( *(s1+i) != c) {
			return i+1;
		} /* fi */
	} /* rof */
	
	return 0;
} /* end of strchk *//****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
