#ifndef _DEF_mks_version_calutl_c
  #define _DEF_mks_version_calutl_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_calutl_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/calutl.c 1.2 2004/08/10 20:10:49SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* Tool Library for Calendar */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "glbdef.h"
#include "calutl.h"


struct _MonthTbl
{
  char cNamFul[10];/* Full Name of element        */
  char cNam3Lc[4]; /* 3 letter name of element    */
  int  iMaxDay;    /* Amount of Days in element   */
};/* end of struct _TblYears */
typedef struct _MonthTbl MOTB;
#define MOTB_SIZE sizeof(MOTB)

struct _DayTbl
{
  char cNamFul[10];/* Full Name of element        */
  char cNam2Lc[3]; /* 2 letter name of element    */
};/* end of struct _DayTbl */
typedef struct _DayTbl DYTB;
#define DYTB_SIZE sizeof(DYTB)

static struct _MonthTbl MonthTbl[] = 
{
	{ "INTERNAL",  "XXX",  0 },
	{ "January",   "JAN", 31 },
	{ "February",  "FEB", 28 },
	{ "March",     "MAR", 31 },
	{ "April",     "APR", 30 },
	{ "May",       "MAY", 31 },
	{ "June",      "JUN", 30 },
	{ "July",      "JUL", 31 },
	{ "August",    "AUG", 31 },
	{ "September", "SEP", 30 },
	{ "October",   "OCT", 31 },
	{ "November",  "NOV", 30 },
	{ "December",  "DEC", 31 }
};/* end of table TblMonth */

static struct _DayTbl DayTbl[] = 
{
	{ "INTERNAL", "XX" },
	{ "Monday",   "MO" },
	{ "Tuesday",  "TU" },
	{ "Wednesday","WE" },
	{ "Thursday", "TH" },
	{ "Friday",   "FR" },
	{ "Saturday", "SA" },
	{ "Sunday",   "SU" }
};/* end of table DayTbl */

YRMO *YrMo = NULL;

static void InitMonthTable(YRMO *);
static int CheckYear(char *, int, int, int *);
static int CheckMonth(char *, int, int *);
static int CheckDay(char *, int, int *, int, int);
static int CheckHour(char *, int, int *);
static int CheckMinute(char *, int, int *);

static void StrCpyByt(char *, int, char *, int, int);
static void CheckTableInit(void);

/* =========================================== */
/* =========================================== */
int InitYearTable(char *CalInit, int CalMaxLin)
{ 
  int ilRc = RC_SUCCESS;
  int ilIdx;
  int ilLen;
  char Year4Lc[] = "0000";
  char WeekNbr[] = "00";
  char DayOfWk[] = "0";
  YRMO *TblYer;
  ilLen = (CalMaxLin + 20) * YRMO_SIZE;
  YrMo = (YRMO *)malloc(ilLen);
  if (YrMo != NULL)
  {
    /* (pYrMo) = (char *)&YrMo[0]; */
    YrMo[0].iNbr4Lc = CalMaxLin;
    
    StrCpyByt(Year4Lc, 0, CalInit, 0, 4);
    StrCpyByt(WeekNbr, 0, CalInit, 4, 2);
    StrCpyByt(DayOfWk, 0, CalInit, 6, 1);

    InitMonthTable(YrMo);

    TblYer = YrMo + 15;
    
    TblYer[0].iNbr4Lc = atoi(Year4Lc);
    TblYer[0].iNbr2Lc = TblYer[0].iNbr4Lc % 100;
    TblYer[0].iWekDay = atoi(DayOfWk);
    TblYer[0].iNoWeek = atoi(WeekNbr);
    sprintf(TblYer[0].cNam2Lc,"%.2d",TblYer[0].iNbr2Lc);
    sprintf(TblYer[0].cNam4Lc,"%.4d",TblYer[0].iNbr4Lc);
    TblYer[0].iLpYear = LeapYear(TblYer[0].iNbr4Lc);
    TblYer[0].iMaxDay = 365 + TblYer[0].iLpYear;
    TblYer[0].iCntDay = 0;
    TblYer[0].iCntMin = 0;
  
    for (ilIdx = 1; ilIdx <= CalMaxLin; ilIdx++)
    {
      TblYer[ilIdx].iNbr4Lc = TblYer[ilIdx - 1].iNbr4Lc +1;
      TblYer[ilIdx].iNbr2Lc = TblYer[ilIdx].iNbr4Lc % 100;
      sprintf(TblYer[ilIdx].cNam2Lc,"%.2d",TblYer[ilIdx].iNbr2Lc);
      sprintf(TblYer[ilIdx].cNam4Lc,"%.4d",TblYer[ilIdx].iNbr4Lc);
      TblYer[ilIdx].iLpYear = LeapYear(TblYer[ilIdx].iNbr4Lc);
      TblYer[ilIdx].iMaxDay = 365 + TblYer[ilIdx].iLpYear;

      TblYer[ilIdx].iCntDay = TblYer[ilIdx - 1].iCntDay + TblYer[ilIdx - 1].iMaxDay;
      TblYer[ilIdx].iCntMin = TblYer[ilIdx].iCntDay * 1440;
    
      TblYer[ilIdx].iWekDay = ((TblYer[ilIdx - 1].iWekDay + TblYer[ilIdx - 1].iMaxDay - 1) % 7) + 1;  
      TblYer[ilIdx].iNoWeek = 0;  
      /*
      TblYer[ilIdx].iNoWeek = (TblYer[ilIdx].iCntDay / 7) + 1;  
      */
    } /* end for */
  } /* end if */
  else
  {
    ilRc = RC_FAIL;
  } /* end else */
  
  
  return ilRc;
} /* end of InitYearTable */


/* ============================================ */
/* The first 14 Entries of the Calendar are     */
/* reserved for Month Table. 			*/
/* ============================================ */
static void InitMonthTable(YRMO TblMon[])
{
  int ilIdx;
  
  TblMon[0].iCntDay = 0;  
  TblMon[0].iMaxDay = 0;  
  
  for (ilIdx = 1; ilIdx <= 13; ilIdx++)
  {
    strcpy(TblMon[ilIdx].cNamFul, MonthTbl[ilIdx].cNamFul);
    strcpy(TblMon[ilIdx].cNam3Lc, MonthTbl[ilIdx].cNam3Lc);
    sprintf(TblMon[ilIdx].cNam2Lc, "%.2d", ilIdx);
    TblMon[ilIdx].iNbr4Lc = ilIdx;
    TblMon[ilIdx].iNbr2Lc = ilIdx;
    TblMon[ilIdx].iMaxDay = MonthTbl[ilIdx].iMaxDay;

    TblMon[ilIdx].iCntDay = TblMon[ilIdx - 1].iCntDay + TblMon[ilIdx - 1].iMaxDay;
    TblMon[ilIdx].iCntMin = TblMon[ilIdx].iCntDay * 1440;
    TblMon[ilIdx].iWekDay = (TblMon[ilIdx].iCntDay % 7) + 1;  
    TblMon[ilIdx].iNoWeek = (TblMon[ilIdx].iCntDay / 7) + 1;  
  } /* end for */
  
  return;
} /* end of InitMonthTable */          


/* =========================================== */
/* =========================================== */
int LeapYear(int Year4Dc)
{
  int rc = 0;
  if ( (((Year4Dc % 4) == 0) && ((Year4Dc % 100) != 0)) || ((Year4Dc % 400) == 0) )
  {
    rc = 1;
  } /* end if */
  
  return rc;
} /* end of LeapYear */


/* =========================================== */
/* =========================================== */
int GetFullDay(char *DatTim, int *CntDay, int *CntMin, int *WkDy)
{
  int ilRc   = RC_SUCCESS;

  int ilYrId = 0;
  int ilMoVl = 0;
  int ilDyVl = 0;
  int ilHrVl = 0;
  int ilMiVl = 0;
  char *pclChr = NULL;

  YRMO *TblYer;
  YRMO *TblMon;
  CheckTableInit();
  TblMon = YrMo;
  TblYer = YrMo + 15;

  *CntDay = 0;
  *CntMin = 0;
  *WkDy = 0;

  pclChr = DatTim;
  ilRc = 0;
  while ((*pclChr != '\0') && (ilRc == 0))
  {
    if (!isdigit((int)*pclChr))
    {
      ilRc = -1;
    } /* end if */
    pclChr++;
  } /* end while */

  if (ilRc == 0)
  {
    ilRc = CheckDate(DatTim, &ilYrId, &ilMoVl, &ilDyVl, &ilHrVl, &ilMiVl);
  } /* end if */

  if (ilRc == 0)
  {
    /* Evaluate Day of Calendar */
    *CntDay = TblYer[ilYrId].iCntDay + TblMon[ilMoVl].iCntDay + ilDyVl;
    if ((TblYer[ilYrId].iLpYear == 1) && (ilMoVl > 2))
    {
      *CntDay += 1;
    } /* end if */

    /* Evaluate Minutes of Calendar */
    *CntMin  = TblYer[ilYrId].iCntMin + TblMon[ilMoVl].iCntMin;
    *CntMin += (ilDyVl -1) * 1440;
    *CntMin += (ilHrVl * 60) + ilMiVl;
    if ((TblYer[ilYrId].iLpYear == 1) && (ilMoVl > 2))
    {
      *CntMin += 1440;
    } /* end if */
    
    /* Evaluate Day of Week */
    *WkDy = (*CntDay - (8 - TblYer[0].iWekDay)) % 7;
    if (*WkDy == 0)
    {
      *WkDy = 7;
    } /* end if */
  } /* end if */
  else
  {
    *CntDay = ilYrId;
    ilRc = RC_FAIL;
  } /* end else */
  
  return ilRc;
} /* end of GetFullDay */

/* =========================================== */
/* =========================================== */
int CheckDate(char *DatTim, int *YrId, int *MoVl, int *DyVl, int *HrVl, int *MiVl)
{
  int ilRc = 0;
  int ilLen;
  
  int  ilYrPs = 0;
  int  ilMoPs = 2;
  int  ilDyPs = 4;
  int  ilHrPs = 6;
  int  ilMiPs = 8;

  int  ilYrLe = 2;
  int  ilHrLe = 2;
  int  ilMiLe = 2;

  /* Different Formats switched by length: */
  /* 951231         =  6 short date          */
  /* 19951231       =  8 full  date          */
  /* 9512312359     = 10 short date and time */
  /* 199512312359   = 12 full  date and time */
  /* 19951231235959 = 14 full  date,time,sec */

  
  ilLen = strlen(DatTim);
  switch (ilLen)
  {
    case  6:
      ilHrLe = 0;
      ilMiLe = 0;
      break;

    case  8:
      ilYrLe = 4;
      ilMoPs = 4;
      ilDyPs = 6;
      ilHrLe = 0;
      ilMiLe = 0;
      break;

    case 10:
      /* nothing do do */
      /* it is default */
      break;

    case 12:
      ilYrLe = 4;
      ilMoPs = 4;
      ilDyPs = 6;
      ilHrPs = 8;
      ilMiPs =10;
      break;

    case 14:
      ilYrLe = 4;
      ilMoPs = 4;
      ilDyPs = 6;
      ilHrPs = 8;
      ilMiPs =10;
      break;

    default:
      ilRc = 1;
      break;
  } /* end switch */

  if (ilRc == 0)
  {
    ilRc = CheckYear(DatTim, ilYrPs, ilYrLe, YrId);  
  } /* end if */
  
  if (ilRc == 0)
  {
    ilRc = CheckMonth(DatTim, ilMoPs, MoVl);  
  } /* end if */
  
  if (ilRc == 0)
  {
    ilRc = CheckDay(DatTim, ilDyPs, DyVl, *YrId, *MoVl);  
  } /* end if */
  

  if ((ilRc == 0) && (ilHrLe > 0))
  {
    ilRc = CheckHour(DatTim, ilHrPs, HrVl);  
  } /* end if */


  if ((ilRc == 0) && (ilMiLe > 0))
  {
    ilRc = CheckMinute(DatTim, ilMiPs, MiVl);  
  } /* end if */

  if (ilRc > 0)
  {
    *YrId = ilRc;
    ilRc = RC_FAIL;
  } /* end if */
  else
  {
    ilRc = RC_SUCCESS;
  } /* end else */

  
  return ilRc;
} /* end of CheckDate */

/* =========================================== */
/* =========================================== */
static void StrCpyByt(char *Dst, int DstPs, char *Src, int SrcPs, int CpyLe)
{
  while (CpyLe > 0)
  {
    Dst[DstPs] = Src[SrcPs];
    DstPs++;
    SrcPs++;
    CpyLe--;
  } /* end while */
} /* end of StrCpyByt */


/* =========================================== */
/* =========================================== */
static int CheckYear(char *DatTim, int YrPs, int YrLe, int *IdxYr)
{
  int ilRc = 0;
  int ilYrVl;
  char clYr[] = "0000";

  YRMO *TblYer;
  YRMO *TblMon;
  TblMon = YrMo;
  TblYer = YrMo + 15;

  clYr[YrLe] = 0x00;  
  StrCpyByt(clYr, 0, DatTim, YrPs, YrLe);
  ilYrVl = atoi(clYr);

  /* get Index of Year in Calendar */
  if (YrLe > 2)
  {
    /* four digits */
    *IdxYr = ilYrVl - TblYer[0].iNbr4Lc;
    if ((*IdxYr < 0) || (*IdxYr > YrMo[0].iNbr4Lc))
    {
      ilRc = 2;
    } /* end if */
  } /* end if */
  else
  {
    /* two digits */
    *IdxYr = ilYrVl - TblYer[0].iNbr2Lc;
    if (*IdxYr < 0)
    {
      /* check next century */
      *IdxYr += 100;
      if ( ((*IdxYr <= YrMo[0].iNbr4Lc) && (TblYer[*IdxYr].iNbr2Lc != ilYrVl)) ||
            (*IdxYr > YrMo[0].iNbr4Lc) )
      {
        ilRc = 3;
      } /* end if */
    } /* end if */
    else
    {
      if (*IdxYr > YrMo[0].iNbr4Lc)
      {
        ilRc = 3;
      } /* end if */
    } /* end else */
  } /* end else */

  return ilRc;
}/* end of ChkYear */  


/* =========================================== */
/* =========================================== */
static int CheckMonth(char *DatTim, int MoPs, int *MoVl)
{
  int ilRc = 0;
  char clMo[] = "00";

  StrCpyByt(clMo, 0, DatTim, MoPs, 2);
  *MoVl = atoi(clMo);
  if ((*MoVl < 1) || (*MoVl > 12))
  {
    ilRc = 4;
  } /* end if */
  return ilRc;
} /* end */

/* =========================================== */
/* =========================================== */
static int CheckDay(char *DatTim, int DyPs, int *DyVl, int YrId, int MoVl)
{
  int ilRc = 0;
  int ilTmpVl;
  char clDy[] = "00";
  YRMO *TblYer;
  YRMO *TblMon;
  TblMon = YrMo;
  TblYer = YrMo + 15;

  StrCpyByt(clDy, 0, DatTim, DyPs, 2);
  *DyVl = atoi(clDy);

  ilTmpVl = TblMon[MoVl].iMaxDay;
  if (MoVl == 2)
  {
    ilTmpVl += TblYer[YrId].iLpYear;
  } /* end if */
  if ((*DyVl < 1) || (*DyVl > ilTmpVl))
  {
    ilRc = 5;
  } /* end if */

  return ilRc;
} /* end */

/* =========================================== */
/* =========================================== */
static int CheckHour(char *DatTim, int HrPs, int *HrVl)
{
  int ilRc = 0;
  char clHr[] = "00";

  StrCpyByt(clHr, 0, DatTim, HrPs, 2);
  *HrVl = atoi(clHr);
  if ((*HrVl < 0) || (*HrVl > 23))
  {
    ilRc = 6;
  } /* end if */
  return ilRc;
} /* end */

/* =========================================== */
/* =========================================== */
static int CheckMinute(char *DatTim, int MiPs, int *MiVl)
{
  int ilRc = 0;
  char clMi[] = "00";

  StrCpyByt(clMi, 0, DatTim, MiPs, 2);
  *MiVl = atoi(clMi);
  if ((*MiVl < 0) || (*MiVl > 59))
  {
    ilRc = 7;
  } /* end if */
  return ilRc;
} /* end */


/* =========================================== */
/* =========================================== */
int DayDate(char *DatDay, int DayCnt)
{
  int ilRc = RC_FAIL;
  int ilYrId;
  int ilMoId;
  int ilLpYr = 0;
  
  char tmp[] = "00000000000000000000000000";

  YRMO *TblYer;
  YRMO *TblMon;
  CheckTableInit();
  TblMon = YrMo;
  TblYer = YrMo + 15;

  if ((DayCnt > 0) && (DayCnt <= TblYer[YrMo[0].iNbr4Lc].iCntDay))
  {

    ilYrId = (DayCnt - 1) / 365;

    if (DayCnt <= TblYer[ilYrId].iCntDay)
    {
      ilYrId--;
    } /* end if */
  
    DayCnt -= TblYer[ilYrId].iCntDay;

    ilMoId = (DayCnt + 29) / 30;
  
    if ((TblYer[ilYrId].iLpYear == 1) && (DayCnt > 59))
    {
      DayCnt--;
      if (DayCnt == 59)
      {
        ilLpYr = 1;
      } /* end if */
    } /* end if */

    if (ilMoId > 12)
    {
      ilMoId = 12;
    } /* end if */

    if (DayCnt <= TblMon[ilMoId].iCntDay)
    {
      ilMoId--;
    } /* end if */
  
    if (DayCnt > TblMon[ilMoId + 1].iCntDay)
    {
      ilMoId++;
    } /* end if */

    DayCnt -= TblMon[ilMoId].iCntDay;
    DayCnt += ilLpYr;

    sprintf(tmp,"%.2d",DayCnt);
    StrCpyByt(DatDay,0,TblYer[ilYrId].cNam2Lc, 0, 2);
    StrCpyByt(DatDay,2,TblMon[ilMoId].cNam2Lc, 0, 2);
    StrCpyByt(DatDay,4,tmp, 0, 2);
    DatDay[6] = 0x00;

    ilRc   = RC_SUCCESS;

  } /* end if */

  return ilRc;  
} /* end of DayDate */

/* =========================================== */
/* =========================================== */
int FullDayDate(char *DatDay, int DayCnt)
{
  int ilRc = RC_FAIL;
  int ilYrId;
  int ilMoId;
  int ilLpYr = 0;
  
  char tmp[] = "00000000000000000000000000";

  YRMO *TblYer;
  YRMO *TblMon;
  CheckTableInit();
  TblMon = YrMo;
  TblYer = YrMo + 15;

  if ((DayCnt > 0) && (DayCnt <= TblYer[YrMo[0].iNbr4Lc].iCntDay))
  {

    ilYrId = (DayCnt - 1) / 365;

    if (DayCnt <= TblYer[ilYrId].iCntDay)
    {
      ilYrId--;
    } /* end if */
  
    DayCnt -= TblYer[ilYrId].iCntDay;

    ilMoId = (DayCnt + 29) / 30;
  
    if ((TblYer[ilYrId].iLpYear == 1) && (DayCnt > 59))
    {
      DayCnt--;
      if (DayCnt == 59)
      {
        ilLpYr = 1;
      } /* end if */
    } /* end if */

    if (ilMoId > 12)
    {
      ilMoId = 12;
    } /* end if */

    if (DayCnt <= TblMon[ilMoId].iCntDay)
    {
      ilMoId--;
    } /* end if */
  
    if (DayCnt > TblMon[ilMoId + 1].iCntDay)
    {
      ilMoId++;
    } /* end if */

    DayCnt -= TblMon[ilMoId].iCntDay;
    DayCnt += ilLpYr;
    sprintf(tmp,"%.2d",DayCnt);
    StrCpyByt(DatDay,0,TblYer[ilYrId].cNam4Lc, 0, 4);
    StrCpyByt(DatDay,4,TblMon[ilMoId].cNam2Lc, 0, 2);
    StrCpyByt(DatDay,6,tmp, 0, 2);
    DatDay[8] = 0x00;

    ilRc   = RC_SUCCESS;

  } /* end if */

  return ilRc;  
} /* end of FullDayDate */

/* =========================================== */
/* =========================================== */
int TimDate(char *DatTim, int MinCnt)
{
  int ilRc   = RC_FAIL;
  int ilDyVl = 0;
  int ilHrVl = 0;
  int ilMiVl = 0;
  int ilYrCnt;
  /* int i; */
  
  char tmp[] = "0000000000000000";

  YRMO *TblYer;
  YRMO *TblMon;
  CheckTableInit();
  TblMon = (YRMO *)YrMo;
  TblYer = (YRMO *)YrMo + 15;
  ilYrCnt = YrMo[0].iNbr4Lc;

  if ((MinCnt >= 0) && (MinCnt <= TblYer[ilYrCnt].iCntMin))
  {
    ilDyVl = (MinCnt / 1440) + 1; 
    MinCnt = MinCnt % 1440;

    ilHrVl = MinCnt / 60;
    ilMiVl = MinCnt % 60;  

    ilRc = DayDate(DatTim, ilDyVl);

    sprintf(tmp,"%.2d", ilHrVl);
    StrCpyByt(DatTim,6,tmp, 0, 2);

    sprintf(tmp,"%.2d", ilMiVl);
    StrCpyByt(DatTim,8,tmp, 0, 2);
  
    DatTim[10] = 0x00;

  } /* end if */

  return ilRc;  

} /* end of TimDate */

/* =========================================== */
/* =========================================== */
int FullTimeString(char *DatTim, int MinCnt, char *pcpSec)
{
  int ilRc   = RC_FAIL;
  int ilDatLen;
  int ilSecLen;
  int ilSecChr;
  ilRc = FullTimDate(DatTim, MinCnt);
  ilSecLen = strlen(pcpSec);
  if (ilSecLen > 0)
  {
    ilDatLen = strlen(DatTim);
    for (ilSecChr = 0; ilSecChr <= ilSecLen; ilSecChr++)
    {
      DatTim[ilDatLen] = pcpSec[ilSecChr];
      ilDatLen++;
    } /* end for */
  } /* end if */
  return ilRc;  
} /* end FullTimeString */

/* =========================================== */
/* =========================================== */
int FullTimDate(char *DatTim, int MinCnt)
{
  int ilRc   = RC_FAIL;
  int ilDyVl = 0;
  int ilHrVl = 0;
  int ilMiVl = 0;
  int ilYrCnt;
  /* int i; */
  
  char tmp[] = "0000000000000000";

  YRMO *TblYer;
  YRMO *TblMon;
  CheckTableInit();
  TblMon = (YRMO *)YrMo;
  TblYer = (YRMO *)YrMo + 15;
  ilYrCnt = YrMo[0].iNbr4Lc;

  if ((MinCnt >= 0) && (MinCnt <= TblYer[ilYrCnt].iCntMin))
  {
    ilDyVl = (MinCnt / 1440) + 1; 
    MinCnt = MinCnt % 1440;

    ilHrVl = MinCnt / 60;
    ilMiVl = MinCnt % 60;  

    ilRc = FullDayDate(DatTim, ilDyVl);
    sprintf(tmp,"%.2d", ilHrVl);
    StrCpyByt(DatTim,8,tmp, 0, 2);

    sprintf(tmp,"%.2d", ilMiVl);
    StrCpyByt(DatTim,10,tmp, 0, 2);
  
    DatTim[12] = 0x00;

  } /* end if */

  return ilRc;  

} /* end of FullTimDate */

static void CheckTableInit()
{
  if (YrMo == NULL)
  {
    (void)InitYearTable("1995527",50);
  } /* end if */
  return;
}

int DateDiffToMin(char *pcpDate1, char *pcpDate2, int *pipDiffMin)
{
  int ilRetValue = RC_SUCCESS;
  int ilDate1Min = 0;
  int ilDate2Min = 0;
  int ilResultMin = 0;
  int ilDays = 0;
  int ilWkDay = 0;
  ilRetValue = GetFullDay(pcpDate1, &ilDays, &ilDate1Min, &ilWkDay);
  if (ilRetValue == RC_SUCCESS)
  {
    ilRetValue = GetFullDay(pcpDate2, &ilDays, &ilDate2Min, &ilWkDay);
  } /* end if */
  if (ilRetValue == RC_SUCCESS)
  {
    (*pipDiffMin) = ilDate1Min - ilDate2Min;
  } /* end if */
  else
  {
    (*pipDiffMin) = ilDays;
  } /* end else */
  return ilRetValue;  
} /* end DateDiffToMin */


int DateOffsToDate(char *pcpDate, char *pcpMinOffs, char *pcpNewDate)
{
  int ilRetValue = RC_SUCCESS;
  int ilDateMin = 0;
  int ilOffsMin = 0;
  int ilDays = 0;
  int ilWkDay = 0;
  ilRetValue = GetFullDay(pcpDate, &ilDays, &ilDateMin, &ilWkDay);
  if (ilRetValue == RC_SUCCESS)
  {
    ilOffsMin = atoi(pcpMinOffs);
  } /* end if */
  if (ilRetValue == RC_SUCCESS)
  {
    ilDateMin += ilOffsMin;
    ilRetValue = TimDate(pcpNewDate,ilDateMin);
  } /* end if */
  else
  {
    ; /* error trapping not yet defined */
    strcpy(pcpNewDate,pcpDate);
  } /* end else */
  return ilRetValue;  
} /* end DateOffToDate */

/* ******************************************************************** */
int ChgDateToDay(char *pcpDateTime, int *ipWkDay)
{
  int ilMin = 0;
  int ilDays = 0;
  int ilGetRc;
  ilGetRc = GetFullDay(pcpDateTime, &ilDays, &ilMin, ipWkDay);
  return ilDays;
} /* end ChgDateToDay */

/* ******************************************************************** */
int ChgDateToMin(char *pcpDateTime)
{
  int ilMin = 0;
  int ilDays = 0;
  int ilWkDay = 0;
  int ilGetRc;
  ilGetRc = GetFullDay(pcpDateTime, &ilDays, &ilMin, &ilWkDay);
  return ilMin;
} /* end ChgDateToMin */

/* ******************************************************************** */
int ChgDayToDate(char *pcpDateTime, int ipDayVal)
{
  int ilGetRc;
  ilGetRc = FullDayDate(pcpDateTime, ipDayVal);
  return ilGetRc;
} /* end ChgDayToDate */

/* ******************************************************************** */
int ChgMinToDate(char *pcpDateTime, int ipMinVal)
{
  int ilGetRc;
  ilGetRc = FullTimeString(pcpDateTime, ipMinVal, "00");
  return ilGetRc;
} /* end ChgMinToDate */

/* ******************************************************************** */
/* ******************************************************************** */
int ShowCalendar(void)
{
  int ilIdx;
  int days;
  int min;
  int wkDay;
  YRMO *CalTblYears;
  YRMO *CalTblMonth;

  /* to be sure if not yet initialized */
  (void)GetFullDay("9608290000", &days, &min, &wkDay);

  CalTblMonth = YrMo;
  CalTblYears = YrMo +15;

  (void)dbg(DEBUG," ");
  (void)dbg(DEBUG,"INITIALIZED TABLES OF INTERNAL CALENDAR\n");
  (void)dbg(DEBUG,"BASIC TABLE OF ONE YEAR (MONTHLY VALUES)");
  (void)dbg(DEBUG,"NR NAME       COD DAYS COUNT WD WK MINUTES");
  (void)dbg(DEBUG,"== ========== === ==== ===== == == =======");

  for (ilIdx = 1; ilIdx <= 12; ilIdx++)
  {
    (void)dbg(DEBUG,"%s %-10.10s %s   %2d   %3d  %1d %2d  %6u",
               CalTblMonth[ilIdx].cNam2Lc,
               CalTblMonth[ilIdx].cNamFul,
               CalTblMonth[ilIdx].cNam3Lc,
               CalTblMonth[ilIdx].iMaxDay,
               CalTblMonth[ilIdx].iCntDay,
               CalTblMonth[ilIdx].iWekDay,
               CalTblMonth[ilIdx].iNoWeek,
               CalTblMonth[ilIdx].iCntMin);            
  } /* end for */
  (void)dbg(DEBUG,"== ========== === ==== ===== == == =======");


  (void)dbg(DEBUG," ");
  (void)dbg(DEBUG,"BASIC TABLE OF CALENDAR PERIOD (YEARLY VALUES)");
  (void)dbg(DEBUG,"TX YEAR NR LY DAYS WD WK SUM DAYS SUM MINUTES");
  (void)dbg(DEBUG,"== ==== == == ==== == == ======== ===========");

  for (ilIdx = 0; ilIdx <= YrMo[0].iNbr4Lc; ilIdx++)
  {
    (void)dbg(DEBUG,"%s %d %.2d  %.1d  %d  %1d %2d %8u  %10u",
               CalTblYears[ilIdx].cNam2Lc,
               CalTblYears[ilIdx].iNbr4Lc,
               CalTblYears[ilIdx].iNbr2Lc,
               CalTblYears[ilIdx].iLpYear,
               CalTblYears[ilIdx].iMaxDay,
               CalTblYears[ilIdx].iWekDay,
               CalTblYears[ilIdx].iNoWeek,
               CalTblYears[ilIdx].iCntDay,
               CalTblYears[ilIdx].iCntMin);            
  } /* end for */
  (void)dbg(DEBUG,"== ==== == == ==== == == ======== ===========");

  return RC_SUCCESS;
} /* end ShowCalendar */
 

/* ******************************************************************** */
int ChgOraDateToDay(char *pcpOraDate, char *pcpCedaDate, int *ipWkDay)
{
  int ilMin = 0;
  int ilDays = 0;
  int ilGetRc;
  /* ORA DATE 03-NOV-11 */ 
  ilGetRc = GetFullDay(pcpCedaDate, &ilDays, &ilMin, ipWkDay);
  return ilDays;
} /* end ChgOraDateToDay */


/* ******************************************************************** */
int ChgUaeTimeToMin(char *pcpUaeTime, char *pcpCedaTime)
{
  int ilMin = 0;
  int ilGetRc = RC_SUCCESS;
  int ilLen = 0;
  char pclHH[4] = "00";
  char pclMM[4] = "00";
  char pclSS[4] = "00";
  /* UAE TIME: 10:25:00 */
  ilLen = strlen(pcpUaeTime);
  if (ilLen >= 2)
  {
    strncpy(pclHH,&pcpUaeTime[0],2);
  }
  if (ilLen >= 5)
  {
    strncpy(pclMM,&pcpUaeTime[3],2);
  }
  if (ilLen >= 8)
  {
    strncpy(pclSS,&pcpUaeTime[6],2);
  }
  sprintf(pcpCedaTime,"%s%s%s",pclHH,pclMM,pclSS);
  ilMin = atoi(pclHH) * 60;
  ilMin += atoi(pclMM);
  return ilMin;
} /* end ChgUaeTimeToMin */


