#ifndef _DEF_mks_version_unilib_c
  #define _DEF_mks_version_unilib_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_unilib_c[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/unilib.c 1.2 2004/07/29 17:49:55SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ************************************************************************** */
/*                                                                            */
/* Author         : Volker Bliss                                              */
/* Date           : 03.12.1998                                                */
/* Description    : unicenter interface mod_id=8550                           */
/*                                                                            */
/*                                                                            */
/* ************************************************************************** */
/* ************************************************************************** */
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* ************************************************************************** */

#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "tools.h"
#include "uniif.h"


/* ************************************************************************** */
/* ************************************************************************** */
/* ************************************************************************** */

extern int   mod_id; /* normaly declared in ugccsma.h for MAIN sources, a libsrc needs an external declaration */
extern char* mod_name;


/* ************************************************************************** */
/* ************************************************************************** */
/* ************************************************************************** */

static void HandleQueueError(int ipError);


/* ************************************************************************** */
/*                                                                            */
/* ************************************************************************** */
int SendMsgToUniIf(char* pcpMsg)
{
	int 	ilRC = RC_SUCCESS;
	int		ilLen = 0;
	UNIIF	rlUniIf;

	/***** Parameter check *****/

	if(ilRC == RC_SUCCESS)
	{
		ilLen = strlen(pcpMsg);
		if(ilLen == 0)
		{
			dbg(TRACE,"SendMsgToUniIf: invalid 1.st argument");
			dbg(TRACE,"SendMsgToUniIf: zero lenght not valid");
			ilRC = RC_FAIL;
		}else{
			if(ilLen > UNI_MSG_LEN)
			{
				dbg(TRACE,"SendMsgToUniIf: invalid 1.st argument");
				dbg(TRACE,"SendMsgToUniIf: pcpMsg length <%d> > max length <%d>",ilLen,UNI_MSG_LEN);
				ilRC = RC_FAIL;
			}/* end of if */
		}/* end of if */
	}/* end of if */

	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		strcpy((char *)(rlUniIf.cCause),mod_name);
		strcpy((char *)(rlUniIf.cMsg),pcpMsg);

		rlUniIf.sModId		= mod_id;
		rlUniIf.iState		= 1;
		rlUniIf.iFlags		= 42;

		rlUniIf.lTimeStamp	= time(0);

		ilRC = SendStructToUniIf(&rlUniIf);
		if(ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"SendMsgToUniIf: SendStructToUniIf failed <%d>",ilRC);
		}else{
			/* dbg(DEBUG,"SendMsgToUniIf: SendStructToUniIf replied <%s>",rlUniIf.cReply); */
		}/* end of if */
	}/* end of if */
	
	return(ilRC);

}/* end of SendMsgToUniIf */



/* ************************************************************************** */
/*                                                                            */
/* ************************************************************************** */
int SendToUniIf(char* pcpCause, short spModId, int ipState, int ipFlags, char* pcpMsg)
{
	int 	ilRC = RC_SUCCESS;
	int		ilLen = 0;
	UNIIF	rlUniIf;

	/***** Parameter check *****/

	if(ilRC == RC_SUCCESS)
	{
		if(pcpCause == NULL)
		{
			dbg(TRACE,"SendToUniIf: invalid 1.st argument");
			dbg(TRACE,"SendToUniIf: null-pointer not valid");
			ilRC = RC_FAIL;
		}else{
			ilLen = strlen(pcpCause);
			if(ilLen == 0)
			{
				dbg(TRACE,"SendToUniIf: invalid 1.st argument");
				dbg(TRACE,"SendToUniIf: zero lenght not valid");
				ilRC = RC_FAIL;
			}else{
				if(ilLen > UNI_CAUSE_LEN)
				{
					dbg(TRACE,"SendToUniIf: invalid 1.st argument");
					dbg(TRACE,"SendToUniIf: UNIIF Cause length <%d> > max length <%d>",ilLen,UNI_CAUSE_LEN);
					ilRC = RC_FAIL;
				}/* end of if */
			}/* end of if */
		}/* end of if */
	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		if((spModId < 1) || (spModId > 19999))
		{
			dbg(TRACE,"SendToUniIf: invalid 2.nd argument");
			dbg(TRACE,"SendToUniIf: 1 < ModId < 19999 : ModId <%d>",spModId);
			ilRC = RC_FAIL;
		}/* end of if */
	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		/* check ipState if needed */
	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		/* check ipFlags if needed */
	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		if(pcpMsg == NULL)
		{
			dbg(TRACE,"SendToUniIf: invalid 6.th argument");
			dbg(TRACE,"SendToUniIf: null-pointer not valid");
			ilRC = RC_FAIL;
		}else{
			ilLen = strlen(pcpMsg);
			if(ilLen == 0)
			{
				dbg(TRACE,"SendToUniIf: invalid 6.th argument");
				dbg(TRACE,"SendToUniIf: zero lenght not valid");
				ilRC = RC_FAIL;
			}else{
				if(ilLen > UNI_MSG_LEN)
				{
					dbg(TRACE,"SendToUniIf: invalid 6.th argument");
					dbg(TRACE,"SendToUniIf: UNIIF Msg length <%d> > max length <%d>",ilLen,UNI_MSG_LEN);
					ilRC = RC_FAIL;
				}/* end of if */
			}/* end of if */
		}/* end of if */
	}/* end of if */


	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		strcpy((char *)(rlUniIf.cCause),pcpCause);
		strcpy((char *)(rlUniIf.cMsg),pcpMsg);

		rlUniIf.sModId		= spModId;
		rlUniIf.iState		= ipState;
		rlUniIf.iFlags		= ipFlags;

		rlUniIf.lTimeStamp	= time(0);

		ilRC = SendStructToUniIf(&rlUniIf);
		if(ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"SendToUniIf: SendStructToUniIf failed <%d>",ilRC);
		}else{
			/* dbg(DEBUG,"SendToUniIf: SendStructToUniIf replied <%s>",rlUniIf.cReply); */
		}/* end of if */
	}/* end of if */
	
	return(ilRC);

}/* end of SendToUniIf */



/* ************************************************************************** */
/*                                                                            */
/* ************************************************************************** */
int SendStructToUniIf(UNIIF* prpUniIf)
{
	int 		ilRC = RC_SUCCESS;
	int			ilLen = 0;
	int			ilDest = 0;
	int			ilEventLen = 0;
	/* int		ilSecondQueue = 0; */
	/* ITEM*	prlItem = NULL */
	EVENT*		prlEvent = NULL;
	BC_HEAD*	prlBcHead = NULL;
	CMDBLK*		prlCmdBlk = NULL;
	UNIIF*		prlUniIf = NULL;

	/***** Parameter check *****/

	if(ilRC == RC_SUCCESS)
	{
		/* eventual route 19901 could be used instead */

		ilDest = tool_get_q_id(UNI_PROC_NAME);
		if(ilDest == 0)
		{
			dbg(DEBUG,"SendStructToUniIf: uniif not running : return RC_SUCCESS");
			return(RC_SUCCESS);
		}/* end of if */

		if((ilDest < 1) || (ilDest > 19999))
		{
			dbg(TRACE,"SendStructToUniIf: got invalid mod_id <%d> for <%s>",ilDest,UNI_PROC_NAME);
			ilRC = RC_FAIL;
		}/* end of if*/
	}/* end of if*/

	if(prpUniIf == NULL)
	{
		dbg(TRACE,"SendStructToUniIf: invalid 1.st argument");
		dbg(TRACE,"SendStructToUniIf: null-pointer not valid");
		ilRC = RC_FAIL;
	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		ilLen = strlen(prpUniIf->cCause);
		if(ilLen == 0)
		{
			dbg(TRACE,"SendStructToUniIf: invalid prpUniIf->cCause");
			dbg(TRACE,"SendStructToUniIf: zero lenght not valid");
			ilRC = RC_FAIL;
		}else{
			if(ilLen > UNI_CAUSE_LEN)
			{
				dbg(TRACE,"SendStructToUniIf: invalid prpUniIf->cCause");
				dbg(TRACE,"SendStructToUniIf: UNIIF Cause length <%d> > max length <%d>",ilLen,UNI_CAUSE_LEN);
				ilRC = RC_FAIL;
			}/* end of if */
		}/* end of if */
	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		if((prpUniIf->sModId < 1) || (prpUniIf->sModId > 19999))
		{
			dbg(TRACE,"SendStructToUniIf: invalid sModId");
			dbg(TRACE,"SendStructToUniIf: 1 < prpUniIf->sModId < 19999 : prpUniIf->sModId <%d>",prpUniIf->sModId);
			ilRC = RC_FAIL;
		}/* end of if */
	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		/* check ipState if needed */
	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		/* check ipFlags if needed */
	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		ilLen = strlen(prpUniIf->cMsg);
		if(ilLen == 0)
		{
			dbg(TRACE,"SendStructToUniIf: invalid prpUniIf->cMsg");
			dbg(TRACE,"SendStructToUniIf: zero lenght not valid");
			ilRC = RC_FAIL;
		}else{
			if(ilLen > UNI_MSG_LEN)
			{
				dbg(TRACE,"SendStructToUniIf: invalid iprpUniIf->cMsg");
				dbg(TRACE,"SendStructToUniIf: UNIIF Msg length <%d> > max length <%d>",ilLen,UNI_MSG_LEN);
				ilRC = RC_FAIL;
			}/* end of if */
		}/* end of if */
	}/* end of if */


	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		ilEventLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + sizeof(UNIIF);

		prlEvent = calloc(ilEventLen,1);
		if(prlEvent == NULL)
		{
			dbg(TRACE,"SendStructToUniIf: calloc(%d,1) failed",ilEventLen);
			ilRC = RC_FAIL;
		}else{
			prlBcHead = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT));
			prlCmdBlk = (CMDBLK  *) ((char *)prlBcHead->data);
			prlUniIf  = (UNIIF *) ((char *)prlCmdBlk->data);
		}/* end of if */

		if(ilRC == RC_SUCCESS)
		{
			/* eventual we could use a second queue as mod_id */
			/* ilRC = GetDynamicQueue(&ilSecondQueue,"answer"); */

			prlEvent->type = SYS_EVENT;
			prlEvent->command = EVENT_DATA;
			/* prlEvent->originator = ilSecondQueue; */
			prlEvent->originator = mod_id;
			prlEvent->retry_count = 0;
			prlEvent->data_offset = sizeof(EVENT);
			prlEvent->data_length = sizeof(BC_HEAD) + sizeof(CMDBLK) + sizeof(UNIIF);
			sprintf(prlEvent->sys_ID,"UFIS43");

			/* set prlBcHead if needed*/

			/* set prlCmdBlk if needed*/

			memcpy((char *)prlUniIf,(char *)prpUniIf,sizeof(UNIIF));
			
			ilRC = que(QUE_PUT,ilDest,mod_id,PRIORITY_3,ilEventLen,(char *)prlEvent);
			if(ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"SendStructToUniIf: que(QUE_PUT,%d,%d,%d,%d,%x) failed <%d>",ilDest,mod_id,PRIORITY_3,ilEventLen,prlEvent,ilRC);
				HandleQueueError(ilRC);
				ilRC = RC_FAIL;
			}else{
				/* may be we could wait for an answer on a second queue to get a reply from unicenter */
				/* set alarm(60) or use QUE_GETBIGNW loop */
				/* ilRC = que(QUE_GETBIG,0,ilSecondQueue,0,0,(char *)&prlItem); */
				/* sprintf(prpUniIf->cReply,"reply not implemented yet"); */
				/* and dont forget to delete the second queue afterwards */
				/* ilRC = que(QUE_DELETE,ilSecondQueue,ilSecondQueue,0,0,0); */
				/* free(prlItem); */
			}/* end of if */
		}/* end of if */

		if(prlEvent != NULL)
		{
			free(prlEvent);
		}/* end of if */
	}/* end of if */

	return(ilRC);

}/* end of SendStructToUniIf */



/* ************************************************************************** */
/*                                                                            */
/* ************************************************************************** */
static void HandleQueueError(int ipError)
{
	int ilRC = RC_SUCCESS;

	/***** Parameter check *****/

	if(ipError < 0)
	{
		dbg(TRACE,"HandleQueueError: invalid error code <%d>",ipError);
		ilRC = RC_FAIL;
	}/* end of if */

	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		switch(ipError)
		{
		case    QUE_E_FUNC      :       /* Unknown function */
			dbg(TRACE,"HandleQueueError: <%d> : unknown function",ipError);
			break;

		case    QUE_E_MEMORY    :       /* Malloc reports no memory */
			dbg(TRACE,"HandleQueueError: <%d> : malloc failed",ipError);
			break;

		case    QUE_E_SEND      :       /* Error using msgsnd */
			dbg(TRACE,"HandleQueueError: <%d> : msgsnd failed",ipError);
			break;

		case    QUE_E_GET       :       /* Error using msgrcv */
			dbg(TRACE,"HandleQueueError: <%d> : msgrcv failed",ipError);
			break;

		case    QUE_E_EXISTS    :
			dbg(TRACE,"HandleQueueError: <%d> : route/queue already exists ",ipError);
			break;

		case    QUE_E_NOFIND    :
			dbg(TRACE,"HandleQueueError: <%d> : route not found ",ipError);
			break;

		case    QUE_E_ACKUNEX   :
			dbg(TRACE,"HandleQueueError: <%d> : unexpected ack received ",ipError);
			break;

		case    QUE_E_STATUS    :
			dbg(TRACE,"HandleQueueError: <%d> :  unknown queue state ",ipError);
			break;

		case    QUE_E_INACTIVE  :
			dbg(TRACE,"HandleQueueError: <%d> : queue is inaktive ",ipError);
			break;

		case    QUE_E_MISACK    :
			dbg(TRACE,"HandleQueueError: <%d> : missing ack ",ipError);
			break;

		case    QUE_E_NOQUEUES  :
			dbg(TRACE,"HandleQueueError: <%d> : queue does not exist",ipError);
			break;

		case    QUE_E_RESP      :       /* No response on CREATE */
			dbg(TRACE,"HandleQueueError: <%d> : no response on create",ipError);
			break;

		case    QUE_E_FULL      :
			dbg(TRACE,"HandleQueueError: <%d> : too many route destinations",ipError);
			break;
			
		case    QUE_E_NOMSG     :       /* No message on queue */
			dbg(TRACE,"HandleQueueError: <%d> : no messages on queue",ipError);
			break;

		case    QUE_E_INVORG    :       /* Mod id by que call is 0 */
			dbg(TRACE,"HandleQueueError: <%d> : invalid originator=0",ipError);
			break;

		case    QUE_E_NOINIT    :       /* Queues is not initialized*/
			dbg(TRACE,"HandleQueueError: <%d> : queues are not initialized",ipError);
			break;

		case    QUE_E_ITOBIG    :
			dbg(TRACE,"HandleQueueError: <%d> : requestet itemsize to big ",ipError);
			break;

		case    QUE_E_BUFSIZ    :
			dbg(TRACE,"HandleQueueError: <%d> : receive buffer to small ",ipError);
			break;

		default                 :       /* Unknown queue error */
			dbg(TRACE,"HandleQueueError: <%d> : unknown error",ipError);
			break;
		} /* end switch */
	}/* end of if */

}/* end of SendStructToUniIf */



/* ************************************************************************** */
/*                                                                            */
/* ************************************************************************** */
int PrintUniIf(int ipDebugLevel, UNIIF* prpUniIf)
{
	int ilRC = RC_SUCCESS;

	/***** Parameter check *****/

	if(prpUniIf == NULL)
	{
		dbg(TRACE,"PrintUniIf: invalid 2.nd argument");
		dbg(TRACE,"PrintUniIf: null-pointer not valid");
		ilRC = RC_FAIL;
	}/* end of if */


	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		dbg(ipDebugLevel,"uniif->cause       (%s)",prpUniIf->cCause);
		dbg(ipDebugLevel,"uniif->modid       (%d)",prpUniIf->sModId);
		dbg(ipDebugLevel,"uniif->time        (%ld)",prpUniIf->lTimeStamp);
		dbg(ipDebugLevel,"uniif->state       (%d)",prpUniIf->iState);
		dbg(ipDebugLevel,"uniif->flags       (%d)",prpUniIf->iFlags);
		dbg(ipDebugLevel,"uniif->msg         (%s)",prpUniIf->cMsg);
	}/* end of if */

	return(ilRC);

}/* end of PrintUniIf */



/* ************************************************************************** */
/*                                                                            */
/* ************************************************************************** */
int PrintUniIfVersion(int ipDebugLevel)
{
	int ilRC = RC_SUCCESS;

	/***** Parameter check *****/


	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		dbg(ipDebugLevel,"PrintUniIfVersion: header  <%s>",mks_version_uniif_h);
		dbg(ipDebugLevel,"PrintUniIfVersion: libsrc  <%s>",mks_version_unilib_c);
	}/* end of if */

	return(ilRC);

}/* end of PrintUniIfVersion */



/* ************************************************************************** */
/* ************************************************************************** */
/* ************************************************************************** */
