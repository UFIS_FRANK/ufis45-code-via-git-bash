#ifndef _DEF_mks_version_fditools_c
  #define _DEF_mks_version_fditools_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_fditools_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/fditools.c 1.3 2009/02/17 20:49:48SGT akl Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* CCS Program Skeleton                                                       */
/*                                                                            */
/* Author         : SMI                                                       */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/

#include "fditools.h"



/************************************************************ 
 * Function:   GetNextDataItem
 * Parameter:  OUT     char *pcpResult
 *             IN/OUT  char **pcpInput
 *             IN      char *pcpDel    Delimiter
 *             IN      char *pcpDef    Default Result if token is empty
 *             IN      char *pcpTrim   " \0" = trim left with space
 *                                     "\0 " = trim right with space
 *                                     "::"  = trim left and right colon
 *                                     "\0\0" = no trim
 *             
 * Return Code: >=0          log. number of char. in result string
 *                           (blanks does not count as character )
 *                           pcpinput points behind hit 
 *              -1..-5       <number> argument is NULL
 *                           pcpinput not changed
 *                           empty pcpResult string
 *
 * Result:      *prpResult contains found string
 *           
 * Description: The function picks words from a list seperated
 *              by cpDel. The function manipulates *pcpInput to
 *              point to the next position behind cpDel. The 
 *              function interprets every delimiter as beginning
 *              of a new field. e.g.  ':' as delimiter and 
 *              ":Test:" as input string leads to
 *              ""       first call 
 *              "Test"   second call 
 *              ""       third call 
 *              The function can trim a result on the right or/and
 *              the left side by means of not returning those chars
 *              in th result string.
 *
 *************************************************************/
int GetNextDataItem(char *pcpResult,char **pcpInput,char *pcpDel,char *pcpDef,char *pcpTrim)
{
  int ilRC=RC_SUCCESS;
  int ilLastChar=0;
  int ilPos=0;
  if      ( pcpResult == NULL )
    ilRC = -1;
  else if ( *pcpInput == NULL )
    ilRC = -2;
  else if (pcpTrim == NULL)
    ilRC = -5;
  else
    {
      pcpResult[0] = '\0';
      
      if ( pcpTrim[0] != '\0')
	{
	  while (**pcpInput == pcpTrim[0])
	    (*pcpInput)++;
	}



      /* Search first Delimiter character */
      while(((**pcpInput) != pcpDel[0]) && ((**pcpInput) != 0) )
	{
	  pcpResult[ilPos] = **pcpInput;

	  if((pcpTrim != '\0' ) && (**pcpInput) != pcpTrim[1]){
	    ilLastChar = ilPos+1 ;
	  }
	  (*pcpInput)++;
	  ilPos++;
	}

      if (pcpTrim != '\0'){
	ilPos = ilLastChar;
      }
      pcpResult[ilPos] = '\0';
      ilRC = ilPos;

      if((ilPos == 0)  && (*pcpDef != '\0'))
	{
	  strcpy(pcpResult,pcpDef);  
	  ilRC = strlen(pcpDef)-1;
	  while((ilRC >= 0 ) && (pcpResult[ilRC] == ' '))
	    ilRC--;
	}

      if ((**pcpInput) == (pcpDel[0]))
	(*pcpInput)++;
    }
  return ilRC;
}/* GetNextDataItem */


/******************************************************
 * Function:   GetDataItem
 * Parameter:  OUT     char *pcpResult
 *             IN/OUT  char *pcpInput
 *             IN      int   ipNum     pos. of item in list
 *             IN      char  cpDel     Delimiter
 *             IN      char *pcpDef    Default Result if token is empty
 *             IN      char *pcpTrim   " \0" = trim left with space
 *                                     "\0 " = trim right with space
 *                                     "::"  = trim left and right colon
 *                                     "\0\0" = no trim
 *                                
 * Return Code: >=0          log. number of char. in result string
 *                           (blanks does not count as character )
 *                           pcpinput points behind hit 
 *              -1..-5       <number> argument is NULL
 *                           pcpinput not changed
 *                           empty pcpResult string
 *
 * Result:      *prpResult contains found string
 *           
 * Description: Get <ipNum>th item in <cpDel> seperated list
 *              and return the number of chars as return value
 *              and the item in <pcpResult>. 
 *              If the '\0' is used
 *              as delimiter the number of the requested item
 *              must not exceed the total number of items, to
 *              avoid a coredump. Every other delimiter is not 
 *              not critical, because end of string is check.
 *
 *******************************************************/

int GetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel, char *pcpDef, char *pcpTrim)
{
  int ilRC=RC_SUCCESS;
  int ilLastChar=0;
  int i,ilPos=0;

  if (pcpResult == NULL)
     ilRC = -1;
  else if (pcpInput == NULL)
     ilRC = -2;
  else if (ipNum == 0)
     ilRC = -3;
  else if (pcpTrim == NULL)
     ilRC = -4;
  else
  {
/* For test purposes */
if (*pcpTrim == '\0' && pcpTrim[1] != '\0' && pcpTrim[1] != ' ')
   dbg(TRACE,"AKL GetDataItem STRANGE <%c,%c,%c,%c> <%s>",
       pcpTrim[0],pcpTrim[1],pcpTrim[2],pcpTrim[3],pcpInput);
     while (ipNum > 1  && (*pcpInput != '\0' || cpDel == '\0'))
     {
        if (*pcpInput == cpDel)
        {
           if (cpDel != pcpTrim[0] || pcpInput[1] != cpDel)
           {
              ipNum--;
           }
        }
        pcpInput++;
     }/* end while*/
     pcpResult[0] = '\0';
     ilPos = ilLastChar=0;
     if (pcpTrim[0] != '\0')
     {
        while (*pcpInput == pcpTrim[0])
           pcpInput++;
     }
     /* Search first Delimiter character */
     while((*pcpInput != cpDel) && (*pcpInput != '\0') )
     {
        pcpResult[ilPos] = *pcpInput;
        if ((pcpTrim != '\0' ) && (*pcpInput != pcpTrim[1]))
           ilLastChar = ilPos+1 ;
        pcpInput++;
        ilPos++;
     }
     if (*pcpInput == cpDel)
        pcpInput++;
     if (pcpTrim[1] != '\0')
        ilPos = ilLastChar;
     pcpResult[ilPos]='\0';
     ilRC = ilPos;
     if ((ilPos == 0)  && (*pcpDef != '\0'))
     {
        strcpy(pcpResult,pcpDef);  
        ilRC = strlen(pcpDef)-1;
        while ((ilRC >= 0 ) && (pcpResult[ilRC] == ' '))
           ilRC--;
     }
  } /* else end */

  return ilRC;
} /* end GetDataItem */

/******************************************************
 * Function:   FindItemInList
 * Parameter:  
 *             IN      char *pcpInput  
 *             IN      char *pcpPattern Searchpattern  
 *             IN      char  cpDel      Delimiter
 *             OUT     int   ipLine     pos. of item in list
 *                                      (number of delims to hit)
 *             OUT     int   ipCol      number of chars from
 *                                      delimiter to item
 *             OUT     int   ipPos      number of chars from start
 *                                      of string to item
 *                                
 * Return Code: RC_SUCCESS
 *              -1..-5       <number> argument is NULL
 *                           pcpinput not changed
 *                           empty pcpResult string
 *
 * Result:     
 *           
 * Description: 
 *
 *******************************************************/

int FindItemInList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos) 
{
  int ilRC=RC_FAIL;
  int ilPattern=0;
  char *pclPoint;
  *ipLine = 1;
  *ipCol = 1;
  *ipPos = 1;
  pclPoint = pcpPattern;
  ilPattern=strlen(pcpPattern);
  if      ( pcpInput == NULL )
    ilRC = -1;
  else if ( pcpPattern == NULL )
    ilRC = -2;
  else if ( pcpPattern == NULL )
    ilRC = -3;
  else if ( pcpPattern == NULL )
    ilRC = -4;
  else if ( pcpPattern == NULL )
    ilRC = -5;
  else{
    for(;*pcpInput != '\0';)
      {
	while((*pcpInput != *pclPoint ) && (*pcpInput != '\0'))
	  {
	    if(*pcpInput == cpDel )
	      {
		(*ipLine)++;
		*ipCol = 0;
	      }  
	    (*ipCol)++;
	    (*ipPos)++;
	    pcpInput++;
	  }

	if ((*pcpInput != '\0') && (strncmp(pcpInput,pcpPattern,ilPattern)) == 0)
	  {
	    ilRC = RC_SUCCESS;	
	    break;
	  }
	else if(*pcpInput != '\0')
	  {
	    if(*pcpInput == cpDel)
	      {
		(*ipLine)++;
		*ipCol = 0;
	      }  
	    pcpInput++;
	    (*ipCol)++;
	    (*ipPos)++;
	  }
      }/* End for*/

    if (ilRC == RC_FAIL)
      *ipCol = *ipPos = *ipLine = 0;
  }
  return ilRC;    
} /* end of FindItemInList*/

/******************************************************
 * Function:   GetWord
 * Parameter:  
 *             OUT     char *pcpResult  
 *             IN      char *pcpInput  
 *             OUT     int   ipLine     pos. of item in list
 *                                
 * Return Code: >=0          log. number of char. in result string
 *                           (blanks does not count as character )
 *              -1..-3       <number> argument is NULL
 *                           empty pcpResult string
 *
 * Result:     
 *           
 * Description: 
 *
 *******************************************************/

static int GetWord(char *pcpResult, char *pcpInput,int ipNum)
{
    return GetDataItem(pcpResult,pcpInput,ipNum,' ',"","  ");
}

/******************************************************
 * Function:   MakeTokenList
 * Parameter:  
 *             OUT     char *pcpResult  
 *             IN      char *pcpInput  
 *             IN      char *pcpOldDelim  list of chars representing
 *                                        delimiters (e.g.",.:;")
 *             IN      char  cpDelim      new Delimiter
 *                                
 * Return Code: >=0          number of items in list
 *
 *              -1..-4       <number> argument is NULL
 *
 * Result:     *pcpResult contains new list
 *           
 * Description: 
 *
 *******************************************************/

int MakeTokenList(char *pcpResult, char *pcpInput, char *pcpOldDelim, char cpDelim )
{
  char *pclResult;
  int ilCount=0,ilRC=1;
  int hit,i,j,ilInput,ilOldDelim;
  ilInput = strlen(pcpInput);
  ilOldDelim = strlen(pcpOldDelim);
  
  if( pcpResult == NULL ) {
    return -1;
  } else if (pcpInput == NULL){
    return -2;
  } else if (pcpOldDelim == NULL){
    return -3;
  } else if (cpDelim == 0){
    return -4;
  } else {
    pclResult = pcpResult;
    for (i=0; i < ilInput; i++)
      {	
	hit = 0;	
	for (j=0;j < ilOldDelim; j++)
	  {
	    if (pcpInput[i] == pcpOldDelim[j])
	      {	
		hit = 1;
		if ((ilCount > 0) && (pcpResult[ilCount-1] != cpDelim) )
		  {
		    pcpResult[ilCount++] = cpDelim;
		    ilRC++;
		  }
		break;
	      }
	  }/* end for j */
	if(hit != 1)
	  {
	    pcpResult[ilCount++] = pcpInput[i];
	    if( pcpInput[i] == cpDelim){
	      ilRC++;
	    }
	  }
      }/* end for i */
    pcpResult[ilCount] = '\0';
    return ilRC;		
  }	
} /* end MakeTokenList() */



int CountToDelim(char *pcpInput,int ipNum,char cpDelim)
{
  int ilCount=0;
  int ilRC=0;
  
  while((pcpInput[ilRC] != '\0') && (ilCount < ipNum) )
    {
      if (pcpInput[ilRC] == cpDelim)
	ilCount++;
      ilRC++;
    }
  return ilRC;
} /* end of CountToDelim */





