/*****************************************************************************/
/*                                                                           */
/* ABB AAT/I dsplex.lex                                                      */
/*                                                                           */
/* Author         : Janos Heilig                                             */
/* Creation Date  : 23. March 2001                                           */
/* Description    : Page file lexer (see below for details)                  */
/*                                                                           */
/* Update history : Aug 16 01 JHI/JHE obj id + quoted parms                  */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

/*
 Detailed description
 --------------------

 The lexer expects lines containing 3 fields enclosed in square brackets
 followed by a string starting with "gt" (command) followed by the last field
 enclosed in parentheses (parameters):

   [field1] [field2] [field3] gt...(parameters)

 The 3rd field is expected to contain one or more sequences of two or three
 subfields separated by slashes. Starting with the (optional) second sequence,
 the sequences are separated by the string {ITEM}:

   subfield1/subfield2[/subfield3][{ITEM}subfield1...]

 White spaces before and after field separator patterns ("[", "]", "/" and
 "{ITEM}") are ignored. In case of the parameter field (see below), no
 separator pattern matches takes place inside of quotation marks (`"'). In
 other words, quotation marks must be properly closed.

 A semicolon terminating the gt... command is optional.

 In addition, lines beginning with "//" are recognized as comment lines and are not
 further processed.

 Lines containing only whitespace characters and carriage return (\r) characters
 at the end of line resulting from the DOS file format are ignored silently.

 With the exception of process_eol() which is called after 
 For all of the above defined fields (FIELD1, FIELD2, FIELD3N, FIELD3T,
 FIELD3S, COMMAND, PARM), dedicated functions are called by the lexer to process
 the matched pattern in yytext.

 */


%{

static char *sccsid="%Z% %P% %R%%S% %G% %U%";

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#ifndef STANDALONE_PARSER
#include "glbdef.h"
#include "ugccsma.h" 
#endif  /* STANDALONE_PARSER */

#include "lexdsp.h"


int dsplexwrap();
static int igErrState, igLineCount;
static UDBXPage *prgPage;
static UDBXCommand *prgCommand;
static DynStr *prgLineBuff;

static void open_f1(void), insert_f1(char *), close_f1(void);
static void open_f2(void), insert_f2(char *), close_f2(void);
static void open_f3n(void), insert_f3n(char *), close_f3n(void);
static void open_f3t(void), insert_f3t(char *), close_f3t(void);
static void open_f3s(void), insert_f3s(char *), close_f3s(void);
static void open_cmd(void), insert_cmd(char *), close_cmd(void);
static void open_parm(void), insert_parm(char *), close_parm(void);
static void unexpected(char *);

static void discard_command(void), process_eol(int);
static void insert_commands(UDBXCommand **, int);
static char **dup_strvec(char **, int );

/**
extern  int fileno(FILE  * stream);
extern void dbg(int,char *,...);
**/

extern int debug_level;

%}
%s FIELD1 FIELD2 FIELD2_L FIELD3_L FIELD3_N FIELD3_T FIELD3_S COMMAND PARM PARM_Q LEXERR
%%

<INITIAL>^\/\/.*$						{ ; }
<INITIAL>[ \t]*\[[ \t]*				{ BEGIN FIELD1; open_f1(); }

<FIELD1>[ \t]*\]						{ close_f1(); BEGIN FIELD2_L; }
<FIELD1>[^\n]							{ insert_f1(yytext); }

<FIELD2_L>[ \t]*\[[ \t]*			{ open_f2(); BEGIN FIELD2; }
<FIELD2>[ \t]*\]						{ close_f2(); BEGIN FIELD3_L; }
<FIELD2>[^\n]							{ insert_f2(yytext); }

<FIELD3_L>[ \t]*\[[ \t]*			{ open_f3n(); BEGIN FIELD3_N; }
<FIELD3_N>[ \t]*\/[ \t]*			{ close_f3n(); open_f3t(); BEGIN FIELD3_T; }
<FIELD3_N>[ \t]*\][ \t]*			{ close_f3n(); open_cmd(); BEGIN COMMAND; }
<FIELD3_N>[^\n]						{ insert_f3n(yytext); }

<FIELD3_T>[ \t]*\/[ \t]*			{ close_f3t(); open_f3s(); BEGIN FIELD3_S; }
<FIELD3_T>[ \t]*"{ITEM}"[ \t]*	{ close_f3t(); open_f3n(); BEGIN FIELD3_N; }
<FIELD3_T>[ \t]*\][ \t]*			{ close_f3t(); open_cmd(); BEGIN COMMAND; }
<FIELD3_T>[^\n]						{ insert_f3t(yytext); }

<FIELD3_S>[ \t]*"{ITEM}"[ \t]*	{ close_f3s(); open_f3n(); BEGIN FIELD3_N; }
<FIELD3_S>[ \t]*\][ \t]*			{ close_f3s(); open_cmd(); BEGIN COMMAND; }
<FIELD3_S>[^\n]						{ insert_f3s(yytext); }


<COMMAND>gt[A-Za-z0-9_]+[^(\n]	{insert_cmd(yytext); BEGIN PARM; close_cmd();}

<PARM_Q>\"								{ /*jhe may0901 insert_parm(yytext); */ BEGIN PARM; }
<PARM_Q>[^\n]							{ insert_parm(yytext); }
	
<PARM>\"									{ /*jhe may0901 insert_parm(yytext); */ BEGIN PARM_Q; }
<PARM>\([ \t]*							{ open_parm(); }
<PARM>[ \t]*,[ \t]*					{ close_parm(); open_parm(); }
<PARM>[ \t]*\);?						{ close_parm(); BEGIN INITIAL; }
<PARM>[^\n]								{ insert_parm(yytext); }

[ \t\r]*\n								{ process_eol(YY_START==INITIAL); ++igLineCount; BEGIN INITIAL;}
.											{ yyless(0); igErrState = YY_START; BEGIN LEXERR; }
<LEXERR>.*$								{ unexpected(yytext); BEGIN INITIAL; }

%%

/*
 * ===== API function =====
 */

void dsplex(FILE *fp, UDBXPage *p)
{
int rc;
/*
 * setup line buffer and dirty hooks for successive lex callbacks
 */
	prgLineBuff = DynStr_Create();
	prgPage = p;
	igLineCount = 1;
	igErrState = 0;
	yyrestart(fp); 
/*
 * lex it: structs created by lex callbacks
 */
	rc = yylex();
/*
 * check for incomplete command (premature EOF)
 */
	if (prgCommand)
	{
		dbg(WARNING,"%s.%d: discarding incomplete command (premature EOF)",
          prgPage->pcFileName, igLineCount);
		discard_command();
	}
/*
 * close buffs and src file
 */
	DynStr_Destroy(prgLineBuff);
	return;
}

/*
 * ===== local functions =====
 */

/*
 * === field functions called by the lexer
 */


/*
 * parsing of command fields finished: build the final command
 */

static void process_eol(int complete)
{
UDBXCommand *p2rCmd[2], *prRect, *prText;
int n, x, w, tx;
char *p, *pvec[10], buff[100];

	if(!complete)
	{
		dbg(ERROR,"%s.%d ignoring incomplete line\n", prgPage->pcFileName, igLineCount);
		discard_command();
	}
/*
 * could add more sophisticated error and consistency checking here
 */
	if(prgCommand == NULL)
		return;

	if(prgCommand->iNError)
	{
		dbg(ERROR,"%s.%d ignoring bad command line\n", prgPage->pcFileName, igLineCount);
		discard_command();
	}


	if (strcmp(prgCommand->pcDZCommand,"gtTextDefineEx") != 0)
	{
		p2rCmd[0] = prgCommand;
		n = 1;
	}
	else
/*
 * special processing for gtTextDefineEx (q&d creation and copy)
 *
 * DON'T ATTEMPT TO DO THE SAME UNLESS YOU COMPLETELY UNDERSTAND IT!!!
 */
	{
		prRect = UDBXCommand_Create();

/*
 * jhi 15.08.2001
 * 
 * old:  prRect->pcObjectId = strdup(prgCommand->pcObjectId);
 *
 * same oid used for two objects (wrong)
 */
    n=atoi(prgCommand->pcObjectId)+1;
    sprintf(buff,"%d",n);
    prRect->pcObjectId = strdup(buff);
 
		Assert(prRect->pcObjectId);
		prRect->iLanguage = prgCommand->iLanguage;
		prRect->iNFieldDesc = 1;
		prRect->prFieldDesc = malloc(sizeof(FieldDescription *));
		Assert(prRect->prFieldDesc);
		prRect->prFieldDesc[0] = FieldDescription_Create();
		p = "-";
		FieldReference_Set(&(prRect->prFieldDesc[0]->rFieldRef), &p);
		/* note FieldDisplayType set by _Create() */
		prRect->pcDZCommand = strdup("gtRectDefine");
		Assert(prRect->pcDZCommand);
		Assert(prgCommand->iNDZParms >= 11);

		for(n=0; n<8; ++n)
		{
			if(n < 4)
				pvec[n] = prgCommand->p2cDZParms[n];
			else if (n == 4)
				pvec[n] = prgCommand->p2cDZParms[6];
			else
				pvec[n] = "0";
		}
		
		prRect->iNDZParms = 8;
		prRect->p2cDZParms = dup_strvec(pvec, 8);

		prText = UDBXCommand_Create();
		prText->pcObjectId = strdup(prgCommand->pcObjectId);
		Assert(prText->pcObjectId);
		prText->iLanguage = prgCommand->iLanguage;

/* danger zone >>>> */
		prText->iNFieldDesc = prgCommand->iNFieldDesc;
		prText->prFieldDesc = prgCommand->prFieldDesc;
		prgCommand->iNFieldDesc = 0;
		prgCommand->prFieldDesc = NULL;
/* <<<< danger zone */

		prText->pcDZCommand = strdup("gtTextDefineWithAttributes");
		Assert(prText->pcDZCommand);
/* adjust for text alignment */
		Assert(sscanf(prgCommand->p2cDZParms[0],"%d",&x) == 1);
		Assert(sscanf(prgCommand->p2cDZParms[2],"%d",&w) == 1);
		if (strcmp(prgCommand->p2cDZParms[9],"gtRIGHT") == 0)
		      	x += w-1;
		else if(strcmp(prgCommand->p2cDZParms[9],"gtCENTER") == 0)
			x += w/2;

		sprintf(buff,"%d", x);	/* tbi */
		pvec[0] = buff;
		pvec[1] = prgCommand->p2cDZParms[1];
		for(n=2; n<9; ++n)
			pvec[n] = prgCommand->p2cDZParms[n+2]; /* start at 4 */
		
		prText->iNDZParms = 9;
		prText->p2cDZParms = dup_strvec(pvec, 9);


		discard_command();

		p2rCmd[0] = prRect;
		p2rCmd[1] = prText;
		n = 2;
	}

	insert_commands(p2rCmd, n);
	prgCommand = NULL;
}

static void open_f1()
{
	DynStr_Reset(prgLineBuff);

	Assert(prgCommand == NULL);
	prgCommand = UDBXCommand_Create();

}

static void insert_f1(char *pcToken)
{
	DynStr_AppendStr(prgLineBuff,pcToken);
}

static void close_f1()
{
	if(prgLineBuff->chars)
		prgCommand->pcObjectId = strdup(prgLineBuff->chars); /* (preset to NULL) */
}


static void open_f2()
{
	DynStr_Reset(prgLineBuff);
}

static void insert_f2(char *pcToken)
{
	DynStr_AppendStr(prgLineBuff,pcToken);
}

static void close_f2()
{
int lx;
	if(prgLineBuff->chars != NULL && sscanf(prgLineBuff->chars,"%d",&lx) == 1)
		prgCommand->iLanguage = lx;
}

static void open_f3n()
{
	unsigned int s;
	int n;

	DynStr_Reset(prgLineBuff);
	n = prgCommand->iNFieldDesc;
	s = (n+1) * sizeof(FieldDescription *);
	prgCommand->prFieldDesc = realloc(prgCommand->prFieldDesc, s);
	Assert(prgCommand->prFieldDesc);
	prgCommand->prFieldDesc[n] = FieldDescription_Create();
	++prgCommand->iNFieldDesc;
}

static void insert_f3n(char *pcToken)
{
	DynStr_AppendStr(prgLineBuff,pcToken);
}

static void close_f3n()
{
FieldReference *pFieldRef;
char *p;

	if (prgLineBuff->chars == NULL)
		return;
	pFieldRef = &(prgCommand->prFieldDesc[prgCommand->iNFieldDesc-1]->rFieldRef);
	p =  prgLineBuff->chars;
	FieldReference_Set(pFieldRef, &p);
}

static void open_f3t()
{
	DynStr_Reset(prgLineBuff);
}

static void insert_f3t(char *pcToken)
{
	DynStr_AppendStr(prgLineBuff,pcToken);
}

static void close_f3t()
{
FieldDisplayType *pFDT;
char *p;
	if (prgLineBuff->chars == NULL)
		return;
	pFDT = &(prgCommand->prFieldDesc[prgCommand->iNFieldDesc-1]->rDpyType);
	p =  prgLineBuff->chars;
	FieldDisplayType_Set(pFDT, &p);
}

static void open_f3s()
{
	DynStr_Reset(prgLineBuff);
}

static void insert_f3s(char *pcToken)
{
	DynStr_AppendStr(prgLineBuff,pcToken);
}

static void close_f3s()
{
int s;
	if(prgLineBuff->chars && sscanf(prgLineBuff->chars,"%d",&s) == 1)
	{
		prgCommand->prFieldDesc[prgCommand->iNFieldDesc-1]->iSize = s;
	}
}

static void open_cmd()
{
	DynStr_Reset(prgLineBuff);
}

static void insert_cmd(char *pcToken)
{
	DynStr_AppendStr(prgLineBuff,pcToken);
}

static void close_cmd()
{
	if(prgLineBuff->chars)
		prgCommand->pcDZCommand = strdup(prgLineBuff->chars);
}


static void open_parm()
{
	DynStr_Reset(prgLineBuff);
}

static void insert_parm(char *pcToken)
{
	DynStr_AppendStr(prgLineBuff,pcToken);
}

static void close_parm()
{
int n;
	if (prgLineBuff->chars)
	{
		n = prgCommand->iNDZParms;
		prgCommand->p2cDZParms = realloc(prgCommand->p2cDZParms, (n+1) * sizeof(char *) );
		Assert(prgCommand->p2cDZParms);
		prgCommand->p2cDZParms[n] = strdup(prgLineBuff->chars);
		Assert(prgCommand->p2cDZParms[n]);
		++prgCommand->iNDZParms;
	}
}


static void unexpected(char *pcToken)
{
	dbg(ERROR,"%s.%d: unexpected `%s' in state %d",
       prgPage->pcFileName, igLineCount, pcToken, igErrState);
	discard_command();
}

/*
 *===== helper functions
 */

static void discard_command()
{
	if (prgCommand)
	{
		UDBXCommand_Destroy(prgCommand);
		prgCommand = NULL;
	}
}

static char **dup_strvec(char **p2cIn, int n)
{
char **p3c;
int ix;

	if (n <= 0)
		return NULL;
	
	p3c = malloc(n * sizeof(char *) );
	Assert(p3c);
	for(ix=0; ix<n; ++ix)
	{
		p3c[ix] = strdup(p2cIn[ix]);
		Assert(p3c[ix]);
	}
	return p3c;
}

static void insert_commands(UDBXCommand **p2r, int ins)
{
int n, ix;
	n = prgPage->iNCommands;
	prgPage->prCommands = realloc(prgPage->prCommands, (n+ins) * sizeof(UDBXCommand *));
	Assert(prgPage->prCommands);
	for(ix=0; ix<ins; ++ix)
		prgPage->prCommands[n+ix] = p2r[ix];
	prgPage->iNCommands += ins;
}


int dsplexwrap()
      {
        /* got from source libyywrap.c */
        return 1;
       }   
