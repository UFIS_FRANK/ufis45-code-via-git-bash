#ifndef _DEF_mks_version_new_catch_c
  #define _DEF_mks_version_new_catch_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_new_catch_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/new_catch.c 1.4 2006/01/21 00:36:20SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*********************************************************************
  New Signal-Handler Funktions
20060117 JIM: added OS AIX

  **********************************************************************/

#include "signal.h"
#include "string.h"
#include "glbdef.h"
#include "tools.h"
#include "new_catch.h"

#undef MORE_DEBUG



/* ******************************************************************** */
/* catch signals														*/
/* ******************************************************************** */
int SetSignals(void (*HandleSignal)(int))
{
  int	ilRC	= RC_SUCCESS;
  int	size	= 0;
#if defined(_SNI) || defined(_SOLARIS) || defined(_HPUX_SOURCE) || defined(_UNIXWARE) || defined (_LINUX)
  sigset_t action_mask = {0,0,0,0};
#elif defined (_AIX)
  sigset_t action_mask;
  memset(&action_mask, 0, sizeof(action_mask) ); 
#else
  sigset_t    action_mask = 0;
#endif
  static struct	sigaction	*my_sig = NULL;
  
  
  size = sizeof(struct sigaction);
  
  errno = 0;
	if (my_sig == NULL)
	{
  	my_sig = (struct sigaction *)malloc(size);
	}
  if(my_sig == NULL)
  {
    dbg(TRACE,"SetSignals: my_sig malloc failed <%d><%s>",size,strerror(errno));
    return(RC_FAIL);
  }/* end of if */
  
  memset(my_sig,0x00,size);
  
  sigemptyset(&action_mask);
  
  sigaddset(&action_mask,SIGPIPE);
  sigaddset(&action_mask,SIGALRM);
  sigaddset(&action_mask,SIGTERM);
  sigaddset(&action_mask,SIGCHLD);
  
  my_sig->sa_handler = HandleSignal;/* signal handler routine */
  my_sig->sa_mask = action_mask;/* signals to be blocked while handling sig */
  
#ifdef MORE_DEBUG
  dbg(DEBUG,"SetSignals: action mask <%x>",action_mask);
  dbg(DEBUG,"SetSignals: set SIGPIPE");
#endif
  
  errno = 0;
  ilRC = sigaction(SIGPIPE,my_sig,NULL);
  if(ilRC != RC_SUCCESS)
  {
    dbg(TRACE,"SetSignals: sigaction SIGPIPE failed <%s>",strerror(errno));
  }/* end of if */
  
#ifdef MORE_DEBUG
  dbg(DEBUG,"SetSignals: set SIGALRM");
#endif
  
  errno = 0;
  ilRC = sigaction(SIGALRM,my_sig,NULL);
  if(ilRC != RC_SUCCESS)
  {
    dbg(TRACE,"SetSignals: sigaction SIGALRM failed <%s>",strerror(errno));
  }/* end of if */
  
#ifdef MORE_DEBUG
  dbg(DEBUG,"SetSignals: set SIGTERM");
#endif
  
  errno = 0;
  ilRC = sigaction(SIGTERM,my_sig,NULL);
  if(ilRC != RC_SUCCESS)
  {
    dbg(TRACE,"SetSignals: sigaction SIGTERM failed <%s>",strerror(errno));
  }/* end of if */
  
#ifdef MORE_DEBUG
  dbg(DEBUG,"SetSignals: set SIGCHLD");
#endif
  
  errno = 0;
  ilRC = sigaction(SIGCHLD,my_sig,NULL);
  if(ilRC != RC_SUCCESS)
  {
    dbg(TRACE,"SetSignals: sigaction SIGCHLD failed <%s>",strerror(errno));
  }/* end of if */
  
#ifdef MORE_DEBUG
  dbg(DEBUG,"SetSignals: ret %d",ilRC);
#endif
  return(ilRC);
  
}/* end of SetSignals */


/* ******************************************************************** */
/**/
/* ******************************************************************** */
int	UnsetSignals(void)
{
  int	ilRC	= RC_SUCCESS;
  int	size	= 0;
#if defined(_SNI) || defined(_SOLARIS) || defined(_HPUX_SOURCE) || defined(_UNIXWARE) || defined(_LINUX)

  sigset_t action_mask = {0,0,0,0};
#elif defined (_AIX)
  sigset_t action_mask;
  memset(&action_mask, 0, sizeof(action_mask) );
#else
  sigset_t    action_mask = 0;
#endif
  struct	sigaction	*my_sig;
  
  size = sizeof(struct sigaction);
  
  errno = 0;
  my_sig = (struct sigaction *)malloc(size);
  if(my_sig == NULL)
  {
    dbg(TRACE,"UnsetSignals: my_sig malloc failed <%d><%s>",size,strerror(errno));
    return(RC_FAIL);
  }/* end of if */
  
  memset(my_sig,0x00,size);
  
  sigemptyset(&action_mask);
  
  sigaddset(&action_mask,SIGPIPE);
  sigaddset(&action_mask,SIGCHLD);
  
  my_sig->sa_handler = SIG_IGN;/* signal handler routine */
  my_sig->sa_mask = action_mask;/* signals to be blocked while handling sig */
  
  dbg(DEBUG,"UnsetSignals: action mask <%x>",action_mask);
  dbg(DEBUG,"UnsetSignals: unset SIGPIPE");
  
  errno = 0;
  ilRC = sigaction(SIGPIPE,my_sig,NULL);
  if(ilRC != RC_SUCCESS)
  {
    dbg(TRACE,"UnsetSignals: sigaction SIGPIPE failed <%s>",strerror(errno));
  }/* end of if */
  
  dbg(DEBUG,"UnsetSignals: unset SIGCHLD");
  
  errno = 0;
  ilRC = sigaction(SIGCHLD,my_sig,NULL);
  if(ilRC != RC_SUCCESS)
  {
    dbg(TRACE,"UnsetSignals: sigaction SIGCHLD failed <%s>",strerror(errno));
  }/* end of if */
  
  return(ilRC);
  
}/* end of UnsetSignals */
