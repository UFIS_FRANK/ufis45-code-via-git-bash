#ifndef _DEF_mks_version_queprim_c
  #define _DEF_mks_version_queprim_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_queprim_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/queprim.c 1.11 2013/04/18 10:28:34SGT cst Exp  $";
#endif /* _DEF_mks_version */
/* ******************************************************************** */
/*									*/
/* This file contains the implementation of the UGCCS queuing primitive */
/* Revision 28.6.01 by GRM						*/
/* prepared for set up a diverter to send Events and Queues to 		*/
/* tsmhdl on operativserver rsp tmihdl on testserver			*/
/*									*/
/* ******************************************************************** */
#ident  "queprim.c Version 3.2.7.0"

/* second que put call (for tsmhdl) got another returnvalue GRM 18.7.01 */
/************************************************************************
   This primitive is the only means of communication between modules
   in the UGCCS complex. The following functions are supported by this
   primitive:
	 1. QUE_PUT		Queue a message ( end of queue )
	 2. QUE_PUT_FRONT	Queue a message to front of queue
	 3. QUE_GET		Get a message from queue
	 4. QUE_ACK		Acknowledge a message ( msg is removed)
	 5. QUE_CREATE		Create a new queue
	 6. QUE_DELETE		Delete an existing queue
	 7. QUE_EMPTY		Discard all items from a queue
	 8. QUE_GO		Set queue on go
	 9. QUE_HOLD		Set queue on hold
	10. QUE_WAKE_UP		Wake up signal from scheduler
	11. QUE_DUMP		Dump queue headers
	12. QUE_RESP		Get response on QUE_CREATE request
	21. QUE_GETNW		Get a message without wait
	26. QUE_GETBIG		Get a message of an unknown size
										que(...,char *msg) must point to a ichar pointer which
										will be used to allocate enough space for the message
										this pointer must be freed maually after usage

   In order to queue a message a route ID must be know. This ID is used
   by the Queue Control Program (QCP) to find the delivery adresses for
   the message. Up to 16 receiving modules can be associated with a
   route ID. In order to get a message from a queue an unique module ID
   must be used. Each module will have such a unique ID which associates
   the module with its own queue.
						          
   Syntax:                                     
  		#include "quedef.h"

  		int que(func,route,module,priority,len,msg)
  		int	func,route,module,priority,len
  		char	*msg    
*****************************************************************************/

/********************modification ****************** Romahn CSA *** GRM ***/
/* All modifications made by G.Romahn are signed with GRM   Version RIO   */
/***********************************************************end *** GRM ***/
/**************************************************************************/
/* 20030408 JIM: avoid check of TSMHDL / TMIHDL on any queue event        */
/* 20030410 JIM: reduce strcmp by using(!) the global flag                */
/* 20030410 JIM: checking global flag before calling access()             */
/* 20040728 JIM: for DEBUG only: SendNextForQueue sets one NEXTNW flag via*/ 
/*               CPUT in the the SYSQCP for a given queue                 */
/* 20040801 JIM: removed sccs_version                                     */ 
/* 20040820 JIM: dbg (len>I_SIZE) fuer Berni, BC-Attach testen            */ 
/* 20041203 MCU: Returncode of build_header changed. Now it is NULL in    */ 
/*               case of failure (no memory)                              */
/* 20041213 JIM: added flag to SendNextForQueue()                         */
/* 20041213 JIM: added dbg of shared memory id in case of error           */
/* 20070226 JIM: on AIX the pids are not continously increasing but       */
/*               random, so the modulo may create same tag at 2           */
/*               simultaniosly starting NETINs. Use complete PID          */
/* 2008SEP	 CST: Cleaning up code and correcting protocol errors	         */
/* 20130418 CST: Log and drop events on shmat() errors                    */
/**************************************************************************/



#define QUE_INTERNAL
#define UGCCS_FKT
/********************modification ****************** Romahn CSA *** GRM ***/

#undef  SEM_USED	/* use of system semaphore (done by semaphore files) ***/

/***********************************************************end *** GRM ***/

#include <malloc.h>
#include <memory.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include "glbdef.h"
#include "ugccsma.h"
#include "quedef.h"
/********************modification ****************** Romahn CSA *** GRM ***/



#include "tsmhdl.h"

typedef struct {
	unsigned long	counter ;
	int		len ;
	char		data[1] ;
} REMOTEQUE ;



/***********************************************************end *** GRM ***/


/* #define MORE_DEBUG */
#undef MORE_DEBUG
/* #define MORE_DEBUGGB */
#undef MORE_DEBUGGB

#ifdef MORE_DEBUG
#define MOREDB(debug) debug
#else
#define MOREDB(debug)
#endif

#ifdef MORE_DEBUGGB
#define MOREDBGB(debug) debug
#else
#define MOREDBGB(debug)
#endif

/********************** FUNCTION PROTOTYPES ********************************/
static struct	Item_Header *build_header(int fun,int rou,int org,int pri,int len);
static int	send_item(struct Item_Header *buff);
/* The following function performs the required action to terminate the main program */
/* in case of a fatal queing error (IPC releated, like removal of a queue) */
static void	terminate(void);
/********************modification ****************** Romahn CSA *** GRM ***/

static int 	receive_item(int id,int *length,ITEM *buff,int wait);
static int	recv_big_item(int id,int *length,ITEM **buff,int wait);	

/***********************************************************end *** GRM ***/

static void	free_SharedMem( char *pclShmData, SHMADR *prlShmAdr);
static int	get_response(int id,int length,ITEM *buff);
extern int	que(int func,int route,int module,int priority,int len,char *msg);
/********************modification ****************** Romahn CSA *** GRM ***/

extern int	queGB(int func,int route,int module,int priority,int *len,void *msg);	

/* big queue because of different call parameters

new established by GRM see description ***/

/***********************************************************end *** GRM ***/

extern int 	init_que(void );
static void	detach();
int		get_que_items(long req_que, int bits);
int		queue_overview(int own_id, int ipQueId, int bits);
/********************modification ****************** Romahn CSA *** GRM ***/


static void	ena_que();
static int	isTSMconnected(int cli) ;
static t_quemod	*quemod = NULL;
static int	ig_num_quemod = 0;
static ITEM	*item,*item1;   	/* from local to global */
static int	igQueActiv = 0;		/* to read only once the tsm.queue file */
static int	ilRC_que = FALSE;

/* Zusatz Event */

static void 	ena_evt();
static t_quemod	*evtmod = NULL;
static int	ig_num_evtmod = 0;
static int	igEvtActiv = 0;		/* to read only once the tsm.queue file */
static int	ilRC_evt = FALSE;
static int igDropEventOnError = FALSE;


/***********************************************************end *** GRM ***/

/***************************************************************************/

/* Following the global variables */
static int	igTsmTmiRoute = -1 ;	/* Route of TSM / TMI handler */
static int	qcp_stat;		/* Return status from qcp */
static int	t_length;		/* The total length of a queue item */
extern int	errno;			/* The global error no. */

static int	qcp_input;		/* The QCP input queue */
static int	qcp_attach;		/* The QCP attach queue */
static int	qcp_output;		/* The QCP output queue */

static int	initialized;		/* Initialization flag */
static int	termination_attempt = 0;	/* Flag for detecting ignored SIGTERM signal */

static int	last_pri;		/* Priority of last item */
extern int	mod_id;			/* Declared in ugccsma.h */
/********************modification ****************** Romahn CSA *** GRM ***/

/* 20030410 JIM: reduce strcmp by using(!) the global flag: */
#define NOT_CHECKED (-1)
#define NO_TSMorTMI (0)
#define IS_TSMorTMI (1)
int	igIsTSM	= NOT_CHECKED ;/* TSM sets it to IS_TSMorTMI */

					/* A value of NO_TSMorTMI sends received messages to TSM */

					/* Don't change at the moment later from system table!!!*/

					/* use mod_name to prove this */

					

/***********************************************************end *** GRM ***/

#ifdef SEM_USED

/********************new  lateron usable    ******** Romahn CSA *** GRM ***/

int	setsem(int num);		/* set semaphore (only flag) */
int	tstsem(int num);
int	clrsem(int num);

/***********************************************************end *** GRM ***/

#endif



/* ******************************************************************** */
/* Following the main subroutine					*/
/* ******************************************************************** */

extern int que(int func,int route,int module,int priority,int len,char *msg)
{
	int		return_value;
/********************modification ****************** Romahn CSA *** GRM ***/
	int		return_value_tsm;	/*GRM 18.07.01 */
	int		ilclient;
	int		ilnq;			/* loop counter */
	int		ilfound;		/* found in table */
	int		iladd;			/* add kind of diverter to func */	
	int		ilMsgflag = 0;		/* message flag for msgrcv and msgsnd */

/***********************************************************end *** GRM ***/

	/*	ITEM			*item; now global GRM*/

	/* used RIO version */
	ITEM		*next_item;
	int		next_func;	/* end of RIO */

	MOREDB(dbg(DEBUG,"que: qcp_input=(%d),qcp_output=(%d),qcp_attach=(%d)",qcp_input,qcp_output,qcp_attach));
	MOREDB(dbg(DEBUG,"que: func=(%d),route=(%d),module=(%d),pri=(%d),len=(%d),msg=(%x)",
		func,route,module,priority,len,msg));
	errno = 0;
/********************modification ****************** Romahn CSA *** GRM ***/

	/* TSM Signal via Monitorstart */

	/* 20030410 JIM: reduce strcmp by using(!) the global flag: */
	if (igIsTSM == NOT_CHECKED)
	{
		if (!strcmp (mod_name,"tsmhdl") || !strcmp (mod_name,"tmihdl"))
		{
			igIsTSM = IS_TSMorTMI;
		}
		else
		{
			igIsTSM = NO_TSMorTMI;
		}
	}
	if (igTsmTmiRoute == -1) /* 20030408 JIM: avoid check on any que ev. */
	{	/* Route of TSM / TMI handler not checked , check now */
		igTsmTmiRoute = tool_get_q_id("tsmhdl");
		if (igTsmTmiRoute == 0) igTsmTmiRoute = tool_get_q_id("tmihdl");
	}

/***********************************************************end *** GRM ***/
	
	if (initialized != 1) 
	{
		return QUE_E_NOINIT;	/* Queuing primitive not init. */
	} /* end if */

	if (module == NUL) 
	{
		func = INV_ORG;
	} /* end if */
	
/* ******************modification ****************** Romahn CSA *** GRM ***/

	ilclient = TRUE;

	if (igQueActiv == 0)
	{
		ena_que();
         	igQueActiv = 1;

         	MOREDB(dbg(TRACE,"que: hdl = %s id = %d  func=(%d),route=(%d),module=(%d),pri=(%d),len=(%d),msg=(%x)",
			mod_name,mod_id,func,route,module,priority,len,msg));

	}

	if (igEvtActiv == 0)
	{
		ena_evt();
         	igEvtActiv = 1;

         	MOREDB(dbg(TRACE,"evt: hdl = %s id = %d  func=(%d),route=(%d),module=(%d),pri=(%d),len=(%d),msg=(%x)",
			mod_name,mod_id,func,route,module,priority,len,msg));

	}

	if ( isTSMconnected(ilclient) )     	/* only if TESTCLIENT and connected */
	{
	    switch (func)
	    {
			case QUE_HSBCMD :
			case QUE_PUT:
			case QUE_PUT_FRONT:

/* check if we are testclient and avoid an entrance dependend of TSMQUE settings *****/

			/* if (!igIsTSM)   	/* not from the tsm task */

				if (igIsTSM == NO_TSMorTMI)
				{

					if (ig_num_quemod <= 0) break;	/* no queues are coming from production server so normal job*/
					else
					{
						for (ilnq = 0;ilnq < ig_num_quemod;ilnq++)
						{

							if ((quemod+ilnq)->source == module && (
							(quemod+ilnq)->destination == route || (quemod+ilnq)->destination == 0))
							{

								return RC_SUCCESS; /* should not be queued */
							} /* end if */

						} /* end for */
						break;   	/* should be queued*/
					} /* end else */
				} /* end if */
				else           		/* que request from tsm task */
				{
					if (ig_num_quemod == 0) return RC_SUCCESS;	/* no queues are coming from production server */
					else
					{
						ilfound = 0;
						for (ilnq = 0;ilnq < ig_num_quemod;ilnq++)
						{
							if ((quemod+ilnq)->source == module && (
							(quemod+ilnq)->destination == route || (quemod+ilnq)->destination == 0))
							{
								ilfound = 1;
								break; /* should be queued */
							} /* end if */

						} /* end for */

						if ( ilfound == 1) break;
						return RC_SUCCESS;   	/* should not be queued*/
					} /* end else */
				} /* end else */
			default:
			{
		 		break;
			}

		}   /* end switch */
	 } /* end if connected */		

/***********************************************************end *** GRM ***/

	/* RIO version */
	/* Here we check if we should send a QUE_NEXT event before we continue
	   A QUE_NEXT event is required before any GET commands are processed.
	   This is the solution to premature ACKs from CEDA processes.
	   A signal should not interrupt the send, hence the do loop.		*/
	   
	if ((func == QUE_GET) || 
	    (func == QUE_GETBIG) || 
	    (func == QUE_GETNW) || 
	    (func == QUE_GETBIGNW)
	    )
	{
		ilMsgflag = 0;
		
		if ((func == QUE_GET) || (func == QUE_GETBIG))
		{
			next_func = QUE_NEXT;
		} /* end if */
		
		else
		{
			next_func = QUE_NEXTNW;
			ilMsgflag |= IPC_NOWAIT;
		} /* end else */
		
		next_item = build_header(next_func,module,module,PRIORITY_5,0);
		errno = 0;
		do
		{
			return_value = msgsnd(qcp_input,next_item,sizeof(ITEM),ilMsgflag);
		} while (errno == EINTR);
		
		free((char *) next_item);
		
		
		if ((return_value != SUCCESS) && (errno != EAGAIN))
		{
			/* We only get here if it is a GET nowait request. No need to handle EAGAIN */
			/* as the msgrcv will return ENOMSG. Otherwise.... */
			/* Fatal error. Queues are gone or access not allowed*/
			return QUE_E_NOQUEUES;
		} /* end if */

	} /* end if (Next item processing) */	/* end RIO version */

	switch (func) 
	{
	case QUE_HSBCMD :
	case QUE_PUT:

		MOREDB(dbg(DEBUG,"que:------- QUE_PUT ---------"));

		if ((priority < PRIORITY_1) || (priority > PRIORITY_5))
		{
			return QUE_E_PRIORITY;	/* Queue priority invalid */
		} /* end if */
	
		item = build_header(func,route,module,priority,len);

		MOREDB(dbg(DEBUG,"que (QUE_PUT): item=(%x)",item));

		if ( item == NULL)
		{
			return_value = QUE_E_MEMORY;
		} 
		else 
		{
			if (len > NUL) 
			{
				memcpy(item->text,(char *) msg,len);
			} /* end if */
			return_value = send_item(item);

/********************modification ****************** Romahn CSA *** GRM ***/

			/*----------------------------------------------------------------------
			 *  Server Mode: sending received Msg to TSM server, if not TSM itself
			 * check if we are productionserver and not tsm and connection so we can
			 * send second queue entry to tsm
			 *----------------------------------------------------------------------*/

			MOREDB(dbg(TRACE,"now check if we shall send to tsm QUE_PUT module = %d route = %d TF = %d %d",module,route,TRUE,FALSE));

			/* check if data are in dataset to send to tsm */			

			ilRC_que = FALSE;

			for (ilnq = 0;ilnq < ig_num_quemod;ilnq++)

			{

				if ((quemod+ilnq)->source == module && (

					(quemod+ilnq)->destination == route || (quemod+ilnq)->destination == 0))

				{

					ilRC_que = TRUE;

					break;

				}

			}

			ilRC_evt = TRUE;
			if (ig_num_evtmod == 0) ilRC_evt = FALSE;
			for (ilnq = 0;ilnq < ig_num_evtmod;ilnq++)

			{

				if ((evtmod+ilnq)->source == module || module >= 10000 ||

						(evtmod+ilnq)->destination == route || route >= 10000)

				{

					ilRC_evt = FALSE;

					break;

				}

			}

			ilclient = FALSE;  /* not the testsystem */

			MOREDB(dbg(TRACE,"now send to tsm 1: cli=FALSE igIsTSM = %d ilRC_que = %d ilRC_evt = %d rv = %d",igIsTSM,ilRC_que,ilRC_evt,return_value));

      /* Route of TSM / TMI handler not checked , may be zero */
      route = igTsmTmiRoute;
			if( route > 0 && return_value == RC_SUCCESS && igIsTSM == NO_TSMorTMI  && 

				((ilRC_que == TRUE && isTSMconnected(ilclient)) ||

					(ilRC_evt == TRUE)))
			{


				/*send to TSM */

				module = item->route;	
        /* how to the hell shall route have been modified since last setting?
				 * route = tool_get_q_id("tsmhdl");
				 * if (route == 0) route = tool_get_q_id("tmihdl");
         */
				MOREDB(dbg(TRACE,"now send to tsm 1: module: %d route = %d len = %d",module,route,len+sizeof(ITEM)-sizeof(int)));
				
				priority = item->priority ;
				iladd = 0;

				if (ilRC_que == TRUE) iladd+=1;

				if (ilRC_evt == TRUE) iladd+=2;

				item1 = build_header(func,route,module,priority,len+sizeof(ITEM)-sizeof(int));
				item->function += iladd*TSMACTIONOFFSET;

				if ((len + sizeof(ITEM) -sizeof(int)) > NUL)

				{

					memcpy(item1->text,(char *) item,len+sizeof(ITEM)-sizeof(int));

				} /* end if */

				return_value_tsm = send_item(item1);

				MOREDB(dbg (TRACE, "send_TO_TSM: TO %d: <%d> + %d bytes func = %d if = %d", 
						route, len,sizeof(ITEM),func,item->function));

				free((char *) item1);

			}

			ilRC_que = FALSE;

			ilRC_evt = FALSE;

/*********************************************************** end *** GRM ***/


			MOREDB(dbg(DEBUG,"que: FREE (%x)",item));

			free((char *) item);
		} /* end else */
		break; /* end of QUE_PUT */

	case QUE_PUT_FRONT:

		MOREDB(dbg(DEBUG,"que:------- QUE_PUT_FRONT ---------"));

		if ((priority < PRIORITY_1) || (priority > PRIORITY_5))
		{
			return QUE_E_PRIORITY;	/* Queue priority invalid */
		} /* end if */
	
		item = build_header(func,route,module,priority,len);

		MOREDB(dbg(DEBUG,"que (QUE_PUT_FRONT): item=(%x)",item));

		if ( item == NULL) 
		{
			return_value = QUE_E_MEMORY;
		} 
		else 
		{
			if (len > NUL) 
			{
				memcpy(item->text,(char *) msg,len);
			} /* end if */

			return_value = send_item(item);

/********************modification ****************** Romahn CSA *** GRM ***/
			
			/*----------------------------------------------------------------------
			 *  Server Mode: sending received Msg to TSM server, if not TSM itself
			 * check if we are productionserver and not tsm and connection so we can
			 * send second queue entry to tsm
			 *----------------------------------------------------------------------*/

			MOREDB(dbg(TRACE,"now check if we shall send to tsm QUE_PUT_FRONT module = %d route = %d TS %d %d",module,route,TRUE,FALSE));

			ilRC_que = FALSE;			

			for (ilnq = 0;ilnq < ig_num_quemod;ilnq++)

			{

				if ((quemod+ilnq)->source == module && (

					(quemod+ilnq)->destination == route || (quemod+ilnq)->destination == 0))

				{

					ilRC_que = TRUE;
					break;

				}

			}

			ilRC_evt = TRUE;
			if (ig_num_evtmod == 0) ilRC_evt = FALSE;
			
			for (ilnq = 0;ilnq < ig_num_evtmod;ilnq++)

			{

				if ((evtmod+ilnq)->source == module || module >= 10000 ||

						(evtmod+ilnq)->destination == route || route >= 10000)

				{

					ilRC_evt = FALSE;

					break;

				}

			}

			ilclient = FALSE;  /* not the testsystem */

				MOREDB(dbg(TRACE,"now send to tsm 2: cli=FALSE igIsTSM = %d ilRC_que = %d ilRC_evt = %d rv = %d",igIsTSM,ilRC_que,ilRC_evt,return_value));

      /* Route of TSM / TMI handler, may be zero */
      route = igTsmTmiRoute;
			if( route > 0 && return_value == RC_SUCCESS && igIsTSM == NO_TSMorTMI  && 

				((ilRC_que == TRUE &&  isTSMconnected(ilclient)) ||

					(ilRC_evt == TRUE)))

			{

				/*send to TSM */

				module = item->route;	/* mod GRM */
    			
        /* how to the hell shall route have been modified since last setting?
				 * route = tool_get_q_id("tsmhdl");
				 * if (route == 0) route = tool_get_q_id("tmihdl");
         */
				MOREDB(dbg(TRACE,"now send to tsm 2: module: %d route = %d len = %d",module,route,len+sizeof(ITEM)-sizeof(int)));

				priority = item->priority ;

				iladd = 0;

				if (ilRC_que == TRUE) iladd+=1;

				if (ilRC_evt == TRUE) iladd+=2;				

				item1 = build_header(func,route,module,priority,len+sizeof(ITEM)-sizeof(int));

				item->function += iladd*TSMACTIONOFFSET;
				if ((len + sizeof(ITEM) -sizeof(int)) > NUL)

				{

					memcpy(item1->text,(char *) item,len+sizeof(ITEM)-sizeof(int));

				} /* end if */

				return_value_tsm = send_item(item1);

				MOREDB(dbg (TRACE, "send_TO_TSM: TO %d: <%d> + %d bytes func = %d if = %d",
					module, len,sizeof(ITEM),func,item->function));

				free((char *) item1);

			}

			ilRC_que = FALSE;

			ilRC_evt = FALSE;

/*********************************************************** end *** GRM ***/


			MOREDB(dbg(DEBUG,"que: FREE (%x)",item));

			free((char *) item);
		} /* end else */
		break; /* end of QUE_PUT_FRONT */
				
	case QUE_GET:

		MOREDB(dbg(DEBUG,"que:------- QUE_GET ---------"));

		return_value = receive_item(module,&len,(ITEM *)msg,1); /*GRM*/
		break; /* end of QUE_GET */

	case QUE_GETBIG:

		MOREDB(dbg(DEBUG,"que:------- QUE_GETBIG ---------"));

		return_value = recv_big_item(module,&len,(ITEM **)msg,1); /*GRM*/
		break; /* end of QUE_GET */

	case QUE_GETNW:

		MOREDB(dbg(DEBUG,"que:------- QUE_GETNW --------"));

	 	errno = 0;	
		return_value = receive_item(module,&len,(ITEM *)msg,0); /*GRM*/
		if ( return_value != RC_SUCCESS) 
		{
			if (errno == ENOMSG) 
			{
				return_value = QUE_E_NOMSG;
			} /* end if */
		} /* end if */
		break; /* end of QUE_GET */

	case QUE_GETBIGNW :

		MOREDB(dbg(DEBUG,"que:------- QUE_GETBIGNW ---------"));

	 	errno = 0;	
		return_value = recv_big_item(module,&len,(ITEM **)msg,0); /*GRM*/
		if ( return_value != RC_SUCCESS) 
		{
			if (errno == ENOMSG) 
			{
				return_value = QUE_E_NOMSG;
			} /* end if */
		} /* end if */
		break; /* end of QUE_GET */


	case QUE_RESP:

		MOREDB(dbg(DEBUG,"que:------- QUE_RESP ---------"));

		if (get_response(module,len,(ITEM *)msg) != RC_SUCCESS) 
		{
			return_value = QUE_E_RESP;
		} else {
			return_value = RC_SUCCESS;
		} /* end else */
		break; /* end of QUE_RESP */

	case INV_ORG:

		MOREDB(dbg(DEBUG,"que:------- INV_ORG ---------"));

		return_value = QUE_E_INVORG;
		break;

	default:

		MOREDB(dbg(DEBUG,"que:------- DEFAULT ---------"));

		if (func < LOW_FUN || func > HIG_FUN) 
		{
			return_value = QUE_E_FUNC;
		} else {
			item = build_header(func,route,module,priority,len);

			MOREDB(dbg(DEBUG,"que (default): item=(%x)",item));

			if ( item == NULL ) 
			{
				return_value = QUE_E_MEMORY;
			} else {
			    if (len > NUL) 
				{
					memcpy(item->text,(char *) msg,len);
			    } /* end if */
			    return_value = send_item(item);

					MOREDB(dbg(DEBUG,"que: FREE (%x)",item));

			    free((char *) item);
			} /* end else */
		} /* end else */
		break; /* end of default */
	} /* end switch */	

	if (return_value != RC_SUCCESS) 
	{
		if ((errno == EIDRM) || (errno == EINVAL) || (errno == EACCES) || (errno == EFAULT)) 
		{
			if (igDropEventOnError) {
				/* Remove (ACK) the bad event. */
				que(QUE_ACK,0,mod_id,0,0,NULL);
				/* Also terminate so we preserve the logfile with the origin of the event */
			}
				
			/* These are fatal errors, terminate the process */
			
			terminate();
		} /* end if */
	} /* end if */

	return return_value;

} /* end of main subroutine */

/* ******************************************************************** */

/* Following the main subroutine GB get big				GRM            */

/* ******************************************************************** */

int queGB(int func,int route,int module,int priority,int *len,void *msg)

{

	int			return_value;

/********************modification ****************** Romahn CSA *** GRM ***/

	int			illen;			   /* loop counter */	

/***********************************************************end *** GRM ***/

/********************modification ****************** Romahn CSA *** GRM ***/



	/* TSM Signal via Monitorstart */

  /* 20030410 JIM: reduce strcmp by using(!) the global flag: */
  if (igIsTSM	== NOT_CHECKED)
  {
    if (!strcmp (mod_name,"tsmhdl") || !strcmp (mod_name,"tmihdl"))
    {
       igIsTSM = IS_TSMorTMI;
    }
    else
    {
       igIsTSM = NO_TSMorTMI;
    }
  }

/***********************************************************end *** GRM ***/

	MOREDBGB(dbg(DEBUG,"que: qcp_input=(%d),qcp_output=(%d),qcp_attach=(%d)",qcp_input,qcp_output,qcp_attach));

	MOREDBGB(dbg(DEBUG,"que: func=(%d),route=(%d),module=(%d),pri=(%d),len=(%d),msg=(%x)",

		func,route,module,priority,len,msg));

	if (igQueActiv == 0)

	{

		ena_que();

         	igQueActiv = 1;

         	MOREDBGB(dbg(TRACE,"que: hdl = %s id = %d  func=(%d),route=(%d),module=(%d),pri=(%d),len=(%d),msg=(%x)",

				mod_name,mod_id,func,route,module,priority,len,msg));

	}

	errno = 0;
	
	if (initialized != 1)

	{

		return QUE_E_NOINIT;	/* Queuing primitive not init. */

	} /* end if */

	if (module == NUL)

	{

		func = INV_ORG;

	} /* end if */

	switch (func)

	{

	case QUE_GETBIG:

		MOREDBGB(dbg(DEBUG,"que:------- QUE_GETBIG ---------"));

		errno = 0;

		return_value = recv_big_item(module,&illen,(ITEM **)msg,1);

		*len = illen - sizeof(ITEM) + sizeof(int);

			MOREDBGB(dbg(DEBUG,"que:------- QUE_GETBIGNW --SUCC len = %d----",*len));

		break; /* end of QUE_GETBIG */

	case QUE_GETBIGNW :

		MOREDBGB(dbg(DEBUG,"que:------- QUE_GETBIGNW ---------"));

	 	errno = 0;	

		return_value = recv_big_item(module,&illen,(ITEM **)msg,0);

		if ( return_value != RC_SUCCESS)

		{

			if (errno == ENOMSG)

			{

				return_value = QUE_E_NOMSG;

			} /* end if */

		} /* end if */

		else 

		{

			*len = illen - sizeof(ITEM) + sizeof(int);
			
			MOREDB(dbg(DEBUG,"que:------- QUE_GETBIGNW --SUCC len = %d----",*len));
		}

		break; /* end of QUE_GETBIGNW */

	default:

		/* case not allowed */

		return_value = RC_FAIL;

		break; /* end of default */

	} /* end switch */	

	if (return_value != RC_SUCCESS)

	{

		if ((errno == EIDRM) || (errno == EINVAL) || (errno == EACCES) || (errno == EFAULT))

		{
			/* These are fatal errors, terminate the process */
			
			terminate();
		} /* end if */

	} /* end if */

	return return_value;

} /* end of main subroutine GB get big*/


/* ******************************************************************** */
/* Following a subroutine to build a proper queue item header		*/
/* The routine uses the following parameters:				*/
/*	1. fun - The function code					*/
/*	2. rou - The message route					*/
/*	3. pri - The message prioroty					*/
/*	4. len - The length of an optional text buffer			*/
/*	5. org - The ID of the requesting routine			*/
/*									*/
/* The routine also uses the global variable t_length.			*/
/*									*/
/* The routine delivers the the pointer to the buffer as result. A	*/
/* negative return value denotes an error condition.			*/
/* ******************************************************************** */

static ITEM *build_header(int fun,int rou,int org,int pri,int len)
{
	ITEM			*header;
	
	t_length = len + sizeof(ITEM);

	MOREDB(dbg(DEBUG,"build_header: t_length=(%d)",t_length));

	header = (ITEM *) malloc((unsigned) (t_length + 4));

	MOREDB(dbg(DEBUG,"build_header: header=(%x)",header));

	if (header != NULL) 
  {
		header->function = fun;
		header->route = rou;
		
		if (pri < 1 || pri > 5) {
			pri = 3;
		} /* endif */
		header->priority = pri;
		header->msg_length = len;
		header->originator = org;
		header->msg_header.mtype = (long) pri;
	} /* end if */

	MOREDB(dbg(DEBUG,"build_header: header(rc)=(%x)",header));

	return header;
}

/* ******************************************************************** */
/*									*/
/* Following a function to send an item via the UNIX msg facility.	*/
/* The function takes as parameter a pointer to a queue item.		*/
/* In order to prevent congestion of the UNIX msg facility the send is	*/
/* implemented so that the forwarded item must be acked before another	*/
/* can be sent.								*/
/*									*/
/* ******************************************************************** */

static int send_item(ITEM *buff)

{
	int	ilRC = RC_FAIL; 
	int	return_value = RC_FAIL; 
	int	ilCnt = 0;
	 int	ilMsgflag = 0;				/* message flag for msgrcv and msgsnd */
	int	ilBreak = FALSE;
	/*int	len = t_length - sizeof(long);*/	/* net. length of CEDA-package */
	int	ilSend_len = t_length - sizeof(long);	/* net. length of CEDA-package */
	ITEM	ack_buff;				/* The receive buffer for ack */
	int	id = buff->originator;			/* Originator ID */
	char	*pclTransferPointer = NULL;		/* used for msgsnd() */
	char	*pclShmData = NULL;			/* return-adress of shmat() */
	/*char	pclLocalBuffer[1024];*/			/* work-buffer */
	char	pclLocalBuffer[sizeof(ITEM)+sizeof(SHMADR)];	/* work-buffer */
	ITEM	*prlShmItem = (ITEM*)NULL;		/* item work ptr. (for queue or SHM) */
	SHMADR 	*prlShmAdr = (SHMADR*)NULL;		/* SHM Adr. work ptr. */
		
	if (buff->function == QUE_ACK) 
	{
		buff->priority = last_pri;	/* Use pri from last item */
	} /* end if */
	
	/* if total ITEM-size > I_SIZE then we need to send the item via SHM */
	if ( ilSend_len > I_SIZE ) 
	{
/* 20040820 JIM: fuer Berni, damit er die BC-Attach testen kann:
 * #ifdef MORE_DEBUG 
 */
		dbg(TRACE,"send_item: BIG package from (%d) -> (%d)",id,buff->route);
/*
 * #endif
 */
		prlShmItem = (ITEM *)pclLocalBuffer;	
		prlShmAdr = (SHMADR *)prlShmItem->text;
		pclTransferPointer = (char *)prlShmItem;
		memcpy(prlShmItem,buff,sizeof(ITEM));
		prlShmItem->function=QUE_SHM;
		/*???*/
		/*prlShmItem->msg_length=sizeof(pclLocalBuffer)-sizeof(long);*/
		prlShmItem->msg_length=(sizeof(ITEM)+sizeof(SHMADR))-sizeof(long);
		prlShmAdr->DataLength = ilSend_len;
		prlShmAdr->ShmId = shm_dyn(t_length,(long)(IPC_CREAT|0666));

		if (prlShmAdr->ShmId == -1 ) 
		{
			dbg(TRACE,"send_item: Shared Memory error (IPC_CREAT)!");
			dbg(TRACE,"send_item: Segment-ID=(%d)",prlShmAdr->ShmId);
			/* really necessary if ID = -1 ??? */
			errno = 0;
			if ((return_value=shmctl(prlShmAdr->ShmId,IPC_RMID,&prlShmAdr->ShmIdDs)) != RC_SUCCESS)
			{		
				dbg(TRACE,"send_item: OS-ERROR shmctl(RM:%d): (%d)=(%s)",prlShmAdr->ShmId,errno,strerror(errno));
			}
			return_value = QUE_E_SEND;
		} 
		else
		{

				MOREDB(dbg(DEBUG,"send_item: Shared Memory ID=(%d) created",prlShmAdr->ShmId));

				errno = 0;
				if ((pclShmData = (char *)shmat(prlShmAdr->ShmId,NULL,0)) == (void*)-1)
				{
					dbg(TRACE,"send_item: OS-ERROR shmat() on SHM-ID(%d): (%d)=(%s)"
						,prlShmAdr->ShmId,errno,strerror(errno));
					errno = 0;	
					if ((return_value=shmctl(prlShmAdr->ShmId,IPC_RMID,&prlShmAdr->ShmIdDs)) != RC_SUCCESS)
					{		
						dbg(TRACE,"send_item: OS-ERROR shmctl(RM:%d): (%d)=(%s)",prlShmAdr->ShmId,errno,strerror(errno));
					}
					return_value = QUE_E_SEND;
				}
				else
				{
					memcpy(pclShmData,buff,t_length);
					errno = 0;	
					if ((return_value=shmctl(prlShmAdr->ShmId,IPC_STAT,&prlShmAdr->ShmIdDs)) != RC_SUCCESS)
					{		
						dbg(TRACE,"send_item: OS-ERROR shmctl(IPC_STAT) SHM-ID(%d): (%d)=(%s)"
							,prlShmAdr->ShmId,errno,strerror(errno));

						/* not used because IPC_STAT only sets statistics on SHM-segments */
						/* would be nice, but isn't really neccessary */

						/*return_value = QUE_E_SEND;*/
					}

					errno = 0;
					if ((return_value=shmdt(pclShmData)) != RC_SUCCESS )
					{		
						dbg(TRACE,"send_item: OS-ERROR shmdt() SHM-ID(%d): (%d)=(%s)"
							,prlShmAdr->ShmId,errno,strerror(errno));
						return_value = QUE_E_SEND;
					}

#ifdef MORE_DEBUG
					dbg(DEBUG,"cuid = %d",prlShmAdr->ShmIdDs.shm_perm.cuid);
					dbg(DEBUG,"cgid = %d",prlShmAdr->ShmIdDs.shm_perm.cgid);
					dbg(DEBUG,"uid  = %d",prlShmAdr->ShmIdDs.shm_perm.uid);
					dbg(DEBUG,"gid  = %d",prlShmAdr->ShmIdDs.shm_perm.gid);
					dbg(DEBUG,"mode = %o",prlShmAdr->ShmIdDs.shm_perm.mode);
					dbg(DEBUG,"segsz= %x",prlShmAdr->ShmIdDs.shm_segsz);
					dbg(DEBUG,"cpid = %d",prlShmAdr->ShmIdDs.shm_cpid);
					dbg(DEBUG,"lpid = %d",prlShmAdr->ShmIdDs.shm_lpid);
					dbg(DEBUG,"natt = %d",prlShmAdr->ShmIdDs.shm_nattch);
					dbg(DEBUG,"atime= %d",prlShmAdr->ShmIdDs.shm_atime);
					dbg(DEBUG,"dtime= %d",prlShmAdr->ShmIdDs.shm_dtime);
					dbg(DEBUG,"ctime= %d",prlShmAdr->ShmIdDs.shm_ctime);
#endif

					/* len = sizeof(pclLocalBuffer)-sizeof(long);*/
					ilSend_len = (sizeof(ITEM)+sizeof(SHMADR))-sizeof(long);
				} /* end else */
		} /* end if */
	}
	else
	{

		MOREDB(dbg(DEBUG,"send_item: SMALL package from (%d) -> (%d)",id,buff->route));

		pclTransferPointer = (char *)buff;
		return_value = RC_SUCCESS;
	} /* end else */
	
	if (return_value == RC_SUCCESS)
	{
		MOREDB(dbg(DEBUG,"send_item: entering msgsnd() for (%d) bytes; qcp_input=(%d)!",ilSend_len,qcp_input));
		
		do
		{

			errno = 0;
			
			/* Blocking msgsnd required to ensure delivery and prevent hanging processes */
			
			ilRC = msgsnd(qcp_input,pclTransferPointer,ilSend_len,0);

			/* We stay in the loop if we are interrupted by a signal */

		}while ((ilRC == RC_FAIL) && (errno == EINTR));
		
		MOREDB(dbg(DEBUG,"send_item: leaving msgsnd() qcp_input=(%d)!",qcp_input));
					
		if (return_value == RC_SUCCESS ) 
		{
			/* Wait for the ack */

			int ilRecv_len = sizeof(ITEM);

			ilRC = RC_FAIL;
			do
			{
				errno = 0;

				MOREDB(dbg(DEBUG,"send_item: entering msgrcv() qcp_attach=(%d) for mod_id=(%d)!",qcp_attach,id));

				ilMsgflag = MSG_NOERROR;	/* Ignore message size error */
				ilRC = msgrcv(qcp_attach,&ack_buff,ilRecv_len,id,ilMsgflag);
				
				if ((ilRC != RC_FAIL) && (ack_buff.function == QUE_SHUTDOWN))
				{
					/* We received a shutdown from sysqcp, terminate */
					
					dbg(DEBUG,"Recieved a shutdown on the attach queue. Terminating");
					terminate();
				} /* end if */
					

				MOREDB(dbg(DEBUG,"send_item: leaving msgrcv() qcp_attach=(%d) for mod_id=(%d)!",qcp_attach,id));

				if ((ilRC == RC_FAIL) && (errno == EINTR)) 
				{

					MOREDB(dbg(DEBUG,"send_item: OS-WARNING  ACK-msgrcv(EINTR): (%d)=(%s)",errno,strerror(errno)));

				}
				else
				{
					if (ilRC == RC_FAIL) 
					{
						dbg(TRACE,"send_item: OS-ERROR  ACK-msgrcv(): (%d)=(%s)",errno,strerror(errno));
						return_value = QUE_E_MISACK;
						
						/* This error is fatal, terminate */
						
						terminate();
					} 
				}
				
				/* We stay in the loop if we are interrupted by a signal */
				
			}while((ilRC == RC_FAIL) && (errno==EINTR));

			if (return_value == RC_SUCCESS) 
			{

				MOREDB(dbg(DEBUG,"send_item: ack_buff.function = (%d)",ack_buff.function));

				qcp_stat = ack_buff.function;
				/* internal error code between sysqcp and sending process is stored in ack_buff.function */
				return_value = qcp_stat;
			} 
		} 
	}
	return return_value;
} /* end of send_item */

/* ******************************************************************** */
/*									*/
/* The following routine receives a queue item via the messages		*/
/* facility.								*/
/*									*/
/* ******************************************************************** */
static int receive_item(int id,int *plength /*GRM*/,ITEM *buff,int wait)
{
	int		ilCnt = 0;
	int		return_value = RC_FAIL;
	int		ilRC = RC_FAIL;
	int		flag;
	char		*pclShmData;
	ITEM		*prlLocalItem;
	SHMADR		*prlShmAdr;
	char		pclLocalBuffer[1024];
	int		length = *plength ;	/*GRM*/

	length -= sizeof(long);			/* Adjust for mtype */
		
	flag = 0;
	
	if ( wait == 0 ) {
		/* Non-blocking receive */
		flag |= IPC_NOWAIT;
	} /* end if */


	MOREDB(dbg(DEBUG,"recv_item: buff at<%x> length<%d>",buff,length));

	errno = 0;
	ilRC = msgrcv(qcp_output,(void *) buff,length,id,flag);

	/* If error was E2BIG, we need to remove the item from the queue */
			
	if (ilRC == RC_FAIL && errno == E2BIG)
	{
		/* This is a bad error, but we rely on the main program to perform data integrity check */
		/* At least the message is removed from the IPC message queue */
		/* An alternative solution would be to ACK the message and report an error, but it would */
		/* result in lost messages that are hard to trace */
		
		dbg(DEBUG,"recv_item: Message to big, loading truncated message");
		flag = MSG_NOERROR;
		flag |= IPC_NOWAIT;
		ilRC = msgrcv(qcp_output,(void *) buff,length,id,flag);
	} /* end if */				

	if (ilRC == RC_FAIL && errno != ENOMSG)
	{
		if (errno != EINTR)
		{
			dbg(DEBUG,"recv_item: OS-WARNING msgrcv(): <%d>=<%s>"
				,errno,strerror(errno));
				
			/* This is a fatal error, terminate */
			
			terminate();		
		}
		else
		{
			dbg(TRACE,"recv_item: msgrcv()interrupted by a signal: <%d>=<%s>"
				,errno,strerror(errno));
		} /* end else */
		return_value = QUE_E_GET;
	}
	else
	{
		/* means there was not a message of the desired type */
		/* errno = ENOMSG; not a failure */
		if (ilRC == RC_FAIL)
		{

			MOREDB(dbg(DEBUG,"recv_item: OS-WARNING msgrcv(): <%d>=<%s>",errno,strerror(errno)));

			return_value = QUE_E_NOMSG;
		}
		else
		{
			/* everything OK */

			MOREDB(dbg(DEBUG,"recv_item: msgrcv(): received <%d> bytes.",ilRC));

			return_value = RC_SUCCESS;
		}
	}

	if (return_value == RC_SUCCESS)
	{
		/* we received something, so we check if it is BIG or SMALL and get it */
		last_pri = buff->priority;
		if(buff->function == QUE_SHM ) 
		{

			MOREDB(dbg(DEBUG,"recv_item: Receiving BIG package!"));

			memcpy(pclLocalBuffer,((void *)buff),sizeof(ITEM)+sizeof(SHMADR));
			prlLocalItem = (ITEM *)pclLocalBuffer;
			prlShmAdr=(SHMADR *)prlLocalItem->text;

			MOREDB(dbg(DEBUG,"recv_item: SHM-ADR(%x) SHM-ID(%d) SHM-Length(%d)"
				,prlShmAdr,prlShmAdr->ShmId,prlShmAdr->DataLength));
	
			errno = 0;
			pclShmData = (char *)shmat(prlShmAdr->ShmId,NULL,0);
			if(pclShmData == (char *)-1 ) 
			{
				dbg(TRACE,"recv_item: OS-ERROR shmat(%ld):  <%d>=<%s>",prlShmAdr->ShmId,errno,strerror(errno));
				return_value = QUE_E_GET;
			} 
			else
			{

				MOREDB(dbg(DEBUG,"recv_item: SHM-DATA at (%x) attached SHM-ID(%d)"
					,pclShmData,prlShmAdr->ShmId));

			  if (prlShmAdr->DataLength > length )
			  {
			    dbg(TRACE,"receive_item: Recbuf too small (%d) for %d bytes",
						length, prlShmAdr->DataLength);
			    return_value = QUE_E_BUFSIZ;  /* buf too small */
			  }
			  else
			  {

					MOREDB(dbg(DEBUG,"recv_item: buff at<%x>-><%x>",buff,*buff));

					memcpy(((void *)buff),pclShmData,prlShmAdr->DataLength);
				}
			} /* end else */
			/* free Shared Mem in every case */
			free_SharedMem( pclShmData, prlShmAdr);
		} 
		else
		{
			/* wanted data is already in buffer SMALL item */

			MOREDB(dbg(DEBUG,"recv_item: Receiving SMALL package!"));

			return_value = RC_SUCCESS;
		}
	}
	return return_value;
} /* end of receive_item */

static void free_SharedMem( char *pclShmData, SHMADR *prlShmAdr)
{
int ilCnt = 0;
int ilRC = RC_FAIL;

	if (pclShmData != (char*)-1)
	{
		do
		{
			errno = 0;
			if ((ilRC=shmdt(pclShmData)) == RC_FAIL)
			{

				MOREDB(dbg(DEBUG,"free_SharedMem: OS-ERROR shmdt() (%d) try of 10.",ilCnt+1));

			}
			ilCnt++;
			nap(10);
		}while(ilRC == RC_FAIL && ilCnt < 10);
		if (ilRC == RC_FAIL)
		{
			dbg(TRACE,"free_SharedMem: OS-ERROR shmdt() SHM-DATA at (%x) SHM-ID(%d)  <%d>=<%s>"
				,pclShmData,prlShmAdr->ShmId,errno,strerror(errno));
		}
	}

#ifdef MORE_DEBUG
	dbg(DEBUG,"free_SharedMem: try to remove SHM-ID(%d)",prlShmAdr->ShmId);
	dbg(DEBUG,"cuid = %d",prlShmAdr->ShmIdDs.shm_perm.cuid);
	dbg(DEBUG,"cgid = %d",prlShmAdr->ShmIdDs.shm_perm.cgid);
	dbg(DEBUG,"uid  = %d",prlShmAdr->ShmIdDs.shm_perm.uid);
	dbg(DEBUG,"gid  = %d",prlShmAdr->ShmIdDs.shm_perm.gid);
	dbg(DEBUG,"mode = %o",prlShmAdr->ShmIdDs.shm_perm.mode);
	dbg(DEBUG,"segsz= %x",prlShmAdr->ShmIdDs.shm_segsz);
	dbg(DEBUG,"cpid = %d",prlShmAdr->ShmIdDs.shm_cpid);
	dbg(DEBUG,"lpid = %d",prlShmAdr->ShmIdDs.shm_lpid);
	dbg(DEBUG,"natt = %d",prlShmAdr->ShmIdDs.shm_nattch);
	dbg(DEBUG,"atime= %d",prlShmAdr->ShmIdDs.shm_atime);
	dbg(DEBUG,"dtime= %d",prlShmAdr->ShmIdDs.shm_dtime);
	dbg(DEBUG,"ctime= %d",prlShmAdr->ShmIdDs.shm_ctime);
#endif
			
	ilCnt = 0;
	do
	{
		int		ilShmId = 0;
		errno = 0;
		ilShmId = prlShmAdr->ShmId;
		if ((ilRC = shmctl(prlShmAdr->ShmId,IPC_RMID, &prlShmAdr->ShmIdDs)) == RC_FAIL)
		{

			MOREDB(dbg(DEBUG,"free_SharedMem: OS-ERROR shmctl(IPC_RMID) failed for (%d) try of 10.",ilCnt+1));

		} /* end if */
		else
		{

			MOREDB(dbg(DEBUG,"free_SharedMem: shmctl(IPC_RMID): SHM-ID(%d) removed!",ilShmId));

		}
		ilCnt++;
		nap(10);
	}while(ilRC == RC_FAIL && ilCnt < 10);
	if (ilRC == RC_FAIL)
	{
		dbg(TRACE,"free_SharedMem: OS-ERROR shmctl(RM:%d): (%d)=(%s)",prlShmAdr->ShmId,errno, strerror(errno));
		dbg(TRACE,"free_SharedMem: !!! SHM-SEGMENT STILL LEFT !!! TAKE CARE OF THE MEMORY !!!");
		dbg(TRACE,"free_SharedMem: no recovery from this error but function returns to keep system running");
	}
} /* end of free_SharedMem */
/* ******************************************************************** */
/*									*/
/* The following routine receives a (big) queue item via the messages	*/
/* facility.								*/
/*									*/
/* ******************************************************************** */

static int recv_big_item(int id,int *plength,ITEM **buff,int wait)

{

	int ilRC = RC_FAIL;

	int return_value=RC_FAIL;

	int	flag;

	int ilShmId = 0;

	char *pclShmData;

	ITEM *prlLocalItem;

	SHMADR *prlShmAdr;

	char pclLocalBuffer[1024];

	int	length = *plength ;

	MOREDBGB(dbg(DEBUG,"recv_big_item: ------------ START ---------------"));

	#ifdef _LINUX
		flag = IPC_NOWAIT;
	#else
		flag = ~0;
	#endif

	if ( wait != 0 ) 

	{

		#ifdef _LINUX
			flag = 0;
		#else
			flag ^= IPC_NOWAIT;
		#endif

	} /* end if */

	/* limit the package size to 16K-standard size of CEDA */

	length = I_SIZE;

	MOREDBGB(dbg(DEBUG,"recv_big_item: buff at<%x> length <%d>",(ITEM **)buff,length)); 

	errno = 0;

	(*(ITEM **)buff) = realloc((*(ITEM **)buff),(length + 4));

	if((*(ITEM **)buff) == NULL)

	{

		dbg(TRACE,"recv_big_item: OS-ERROR realloc(%d bytes): <%d>=<%s>"

			,length,errno,strerror(errno));

		return_value = QUE_E_MEMORY;

	}

	else

	{

		MOREDBGB(dbg(DEBUG,"recv_big_item: buff at<%x>-><%x>",(ITEM **)buff,(*(ITEM **)buff)));

		memset((*(ITEM **)buff),0x00,length);

		errno = 0;

		if ((ilRC = msgrcv(qcp_output,(*(ITEM **)buff),length,id,flag))==RC_FAIL && errno!=ENOMSG)

		{

			if (errno != EINTR)

			{

				MOREDBGB(dbg(DEBUG,"recv_item: OS-WARNING msgrcv(): <%d>=<%s>"

					,errno,strerror(errno)));
			}

			else

			{

				MOREDBGB(dbg(TRACE,"recv_big_item: OS-ERROR msgrcv(): <%d>=<%s>"

					,errno,strerror(errno)));

			}

			return_value = QUE_E_GET;

		}

		else

		{

			if (ilRC == RC_FAIL)

			{

				MOREDBGB(dbg(DEBUG,"recv_big_item: OS-WARNING msgrcv(): <%d>=<%s>",errno,strerror(errno)));

				return_value = QUE_E_GET;

			}

			else

			{

				MOREDB(dbg(DEBUG,"recv_big_item: msgrcv(): received <%d> bytes. func = %d",ilRC,(*(ITEM **)buff)->function));

				*plength = ilRC ;	/* receiving a big item will overwrite this value */

				return_value = RC_SUCCESS;

			}

		}

	} 

	/*****************************************end mod GRM */

	/* we received something, so we check if it is BIG or SMALL and get it */

	if (return_value == RC_SUCCESS) 

	{

		MOREDB(dbg(DEBUG,"recv_big_item: msgrcv():func = %d ",(*(ITEM **)buff)->function));

		last_pri = (*(ITEM **)buff)->priority;

		if((*(ITEM **)buff)->function == QUE_SHM ) 

		{

			MOREDB(dbg(DEBUG,"recv_big_item: Receiving BIG package!"));

			memcpy(pclLocalBuffer,(*(ITEM **)buff),sizeof(ITEM)+sizeof(SHMADR));

			prlLocalItem = (ITEM *)pclLocalBuffer;

			prlShmAdr=(SHMADR *)prlLocalItem->text;

			MOREDB(dbg(DEBUG,"recv_big_item: SHM-ADR(%x) SHM-ID(%d) SHM-Length(%d)"

				,prlShmAdr,prlShmAdr->ShmId,prlShmAdr->DataLength));

			errno = 0;

			pclShmData = (char *)shmat(prlShmAdr->ShmId,NULL,0);

			if(pclShmData == (char *)-1 ) 

			{

				dbg(TRACE,"recv_big_item: OS-ERROR shmat(%ld):  <%d>=<%s>",prlShmAdr->ShmId,errno,strerror(errno));
				dbg(TRACE,"recv_big_item: Event details      :  Route <%d> Originator <%d>",(*(ITEM **)buff)->route,(*(ITEM **)buff)->originator);
				
				if(errno == EINVAL) {
					/* EINVAL indicates that the SHM segment do not exist. Drop the event */
					igDropEventOnError = TRUE;
				}
				return_value = QUE_E_GET;

			} 

			else

			{

				MOREDB(dbg(DEBUG,"recv_big_item: SHM-DATA at (%x) attached SHM-ID(%d)"

					,pclShmData,prlShmAdr->ShmId));

				errno = 0;

				/* Add 16 bytes for win NT, dont know why ...  JBE+VBL*/

				(*(ITEM **)buff) = realloc((*(ITEM **)buff),prlShmAdr->DataLength+16);

				if((*(ITEM **)buff) == NULL)

				{

					dbg(TRACE,"recv_big_item: realloc(%d) failed <%d><%s>",prlShmAdr->DataLength,errno,strerror(errno));

					return_value = QUE_E_MEMORY;

				}

				else

				{

					MOREDB(dbg(DEBUG,"recv_big_item: buff at<%x> <%d> bytes received"

						,(ITEM **)buff,prlShmAdr->DataLength));


					memset((*(ITEM **)buff),0x00,prlShmAdr->DataLength+16);

					memcpy((*(ITEM **)buff),pclShmData,prlShmAdr->DataLength);

					*plength = prlShmAdr->DataLength ;  /* big Item overwrites byte cnt from msgrcv */

				}

			} /* end else */

			/* free Shared Mem in every case */

			free_SharedMem( pclShmData, prlShmAdr);

		}

		else

		{

			/* wanted data is already in buffer */

			MOREDB(dbg(DEBUG,"recv_big_item: Receiving SMALL package!"));

		}

	} 

	return return_value;

} /* end of recv_big_item */


/* ******************************************************************** */
/*									*/
/* The following routine receives a response on a QUE_CREATE request	*/
/* via the attach queue.						*/
/*									*/
/* ******************************************************************** */

static int get_response(int id,int length,ITEM *buff)

{

	int	return_value;
	
	length -= sizeof(long);			/* Adjust for mtype */
	

	MOREDB(dbg(DEBUG,"get_response: entering msgrcv() qcp_attach=(%d) for mod_id=(%d)!",qcp_attach,id));

	return_value = msgrcv(qcp_attach,buff,length,id, 0);

	MOREDB(dbg(DEBUG,"get_response: leaving msgrcv() qcp_attach=(%d) for mod_id=(%d)!",qcp_attach,id));

	if (return_value < RC_SUCCESS) {
		return_value = QUE_E_GET;
	} /* end if */
	else {
		if (buff->originator == NUL) {
			return_value = QUE_E_RESP;
		} /* end if */
		else {
			return_value = RC_SUCCESS;
		} /* end else */
	} /* end else */
	return return_value;
}

/* ******************************************************************** */
/*									*/
/* The following routine MUST be called before the queuing primitive	*/
/* can be used. The routine gets the msqids for the QCP input and out-	*/
/* put queues. The routine returns QUE_E_NOQUEUES if the queues are not	*/
/* yet created ( The QCP is not running ).				*/
/*									*/
/* ******************************************************************** */

int	init_que()
{
	qcp_input = msgget(QCP_INPUT,0);
	if (qcp_input < 0) {
		return QUE_E_NOQUEUES;
	} /* end if */
	
	qcp_attach = msgget(QCP_ATTACH,0);
	if (qcp_attach < 0) {
		return QUE_E_NOQUEUES;
	} /* end if */
	qcp_output = msgget(QCP_GENERAL,0);
	if (qcp_output < 0) {
		return QUE_E_NOQUEUES;
	} /* end if */
	
	initialized = 1;
	
	return RC_SUCCESS;
} /* end of init_que */

/* ******************************************************************** */
/*									*/
/* The following routine takes action to shutdown the calling program	*/
/* in case of a fatal IPC queue error					*/
/* The following errors are fatal:					*/
/* 									*/
/* EACCES	No access rights to the IPC message queue		*/
/* EFAULT	The message pointer is invalid				*/
/* EIDRM	The IPC message queue was removed			*/
/* EINVAL	Invalid parameters passed to msgsnd/msgrcv		*/
/*									*/
/* Fatal errors are handled by sending a TERMINATE signal (15) to	*/
/* the own PID. All UFIS processes are required to handle that signal	*/
/* A 5 second pause is done before sending the signal			*/
/*									*/
/* ******************************************************************** */

static void	terminate(void)
{
	dbg(TRACE,"queprim: Fatal IPC queue error: %d, %s. TERMINATING in 5 seconds",errno, strerror(errno));
	
	if (termination_attempt == 1)
	{
		/* SIGTERM was ignored by the process, perform an exit(-1) */
		
		dbg(TRACE,"queprim: Termination signal ignored by process, exiting");
		exit(-1);
	} /* end if */
		
	termination_attempt = 1;
	sleep(5);
	kill(getpid(),SIGTERM);
	
} /* end of terminate */


int get_que_items(long req_que, int bits)
{
  int rc=RC_SUCCESS;
  ITEM item;
  int tag;
  int que_count;
  int tmp_mod_id=0;
  
  /* 20070226 JIM: on AIX the pids are not continously increasing but random, so the
   * modulo may create same tag at 2 simultaniosly starting NETINs. Use complete PID
   * tag = ((getpid()%1000)+30000);
   */
  tag = getpid()+30000;
  rc = que(QUE_CREATE,0,tag,0,7,(char *)"quemon");

  MOREDB(dbg(DEBUG,"get_que_items 1 returned %d !", rc));

  if (rc != RC_SUCCESS)
  {
    rc = -2;
  } /* fi */
  
  if (rc == RC_SUCCESS)
  {
    alarm(10);
    rc = que(QUE_RESP,0,tag,0,sizeof(ITEM),(char *)&item);
    alarm(0);

    MOREDB(dbg(DEBUG,"get_que_items 2 returned %d !", rc));

    if (rc != RC_SUCCESS)
    {
      rc = -3;
    } /* fi */
  } /* end if */
  
  if (rc == RC_SUCCESS)
  {
    tmp_mod_id = item.originator;
    if ( tmp_mod_id == NUL ) 
    {
      rc = -4;
    } /* end if */
  } /* end if */
  
  
  if (rc == RC_SUCCESS)
  {
    que_count = queue_overview(tmp_mod_id, req_que, bits);
    rc = que(QUE_DELETE,tmp_mod_id,tmp_mod_id,0,0,0);

    MOREDB(dbg(DEBUG,"get_que_items 3 returned %d !", rc));
  }

  if (rc == RC_SUCCESS)
  {
    return que_count;
  }
  else
  {
    return rc;
  } /* fi */
}   /* end */


/* ******************************************************************** */
/*									*/
/* The following routine displays the queue overview.			*/
/* of one queue								*/
/*									*/
/* ******************************************************************** */
int queue_overview(int own_id, int ipQueId, int bits)
{
	ITEM		*item;
	QUE_INFO	*que_info;
	QINFO		*qinfo=NULL;
	int		line_count;
	int		que_count;
	int		len;
	int		return_value;

	/* Request the queue overview */
	errno = 10;
	return_value = que(QUE_O_QOVER,ipQueId,own_id,5,0,0);
	if ( return_value != RC_SUCCESS ) {
		return return_value;
	} /* end if */
	len = sizeof(ITEM) + sizeof(QUE_INFO) + (sizeof(QINFO) * 200);
	item = (ITEM *) malloc(len);
	if ( item == NULL ) {
	  dbg(TRACE,"QuePrim: Memory allocation error !"); fflush(stdout);
	  return -99;
	} /* end if */
	
	if ( (return_value= que(QUE_GET,0,own_id,5,len,(char *)item))!=RC_SUCCESS ) {
	  free((char *) item);
	  dbg(TRACE,"QuePrim: Queueing error!"); fflush(stdout);
	  return return_value;
	} /* end if */
	
	if ( item->msg_length == FATAL ) {
	  free((char *) item);
	  dbg(TRACE,"QuePrim: Memory allocation error !"); fflush(stdout);
	  return -99;
	} /* end if */
	
	que_info = (QUE_INFO *) &(item->text[0]);
	qinfo = que_info->data;
	if (qinfo->status != -1)
	{
	  que_count=0;
	  if (bits & GQI_PRIO1) que_count += qinfo->nbr_items[0];
	  if (bits & GQI_PRIO2) que_count += qinfo->nbr_items[1];
	  if (bits & GQI_PRIO3) que_count += qinfo->nbr_items[2];
	  if (bits & GQI_PRIO4) que_count += qinfo->nbr_items[3];
	  if (bits & GQI_PRIO5) que_count += qinfo->nbr_items[4];
	}
	else
	{
	  que_count=-1;
	}

	free((char *) item);
	return que_count;
} /* end of queue_overview */


/* Here is the detach subroutine */
static void detach()
{
	que(QUE_DELETE,mod_id,mod_id,0,0,0);
	return;
} /* end of the detach subroutine */

/******************** new *** until end ********** Romahn CSA *** GRM ***/

/* check if there is a TSM connection   */

	
static int isTSMconnected ( int ifclient)

{

  	static char *cldivtmp = NULL;

	static char *cldivnam = NULL ;

	int	ildivnamlen = 0 ,ilrc;

  /* Route of TSM / TMI handler, may be zero, init in que() */
  if (igTsmTmiRoute <= 0)
  { /* no further checks needed */
		return (FALSE);	/* TSM not active */
  }
  
	/* at  startup get TSM semaphore filename */

#ifdef SEM_USED

	ilrc = tstsem(2);

#else

	if( cldivtmp == NULL )

 	{

		cldivtmp = getenv( "TMP_PATH" );

    		if( cldivtmp == NULL )

      		{

     			dbg( TRACE, "TMP_PATH not set" );

        		return (FALSE);

        	}

        	ildivnamlen = strlen( cldivtmp ) + strlen( TSMACTFILENAME ) + 2 ;

        	cldivnam = malloc( ildivnamlen );

        	if( !cldivnam )

        	{

        		dbg( TRACE, "no more space available" );

        		return (FALSE);

        	}

        	sprintf( cldivnam, "%s/%s", cldivtmp, TSMACTFILENAME );

        }

	ilrc   = access( cldivnam, F_OK );

#endif	/* sem used */
	/* if diverter exists set flag TRUE */

	/* if(ifclient == TRUE && ilqmod == 0 && ilrc >= 0 ) 20030410 JIM: haeh? */
	if(ilrc != 0 )
	{
		return (FALSE);	/* TSM not active */
	}

  /* if we get here, one of tmihdl and tsmhdl is running and semaphore exists */
	if(ifclient == FALSE)
	{
		return (FALSE);	/* TSM not active */
	}

	return (TRUE);	/* TSM active */

}

static void ena_que()

{

	FILE	*fp;

	int	ilRC = RC_SUCCESS;				/* Return code */

	int	ilTmo ,ilCnt;	

	char	daten[20];

	char	*quep[2];

	static char *cldivtmp = NULL ;

	static char *cldivnam = NULL ;

	int	ildivnamlen = 0 ;

	ilCnt = 0;

	ig_num_quemod = ilCnt;		

 	MOREDB(dbg( TRACE, "ena_client queue set up data" ));

	cldivtmp = getenv( "CFG_PATH" );

  	if( cldivtmp == NULL )

   	{

    		dbg( TRACE, "CFG_PATH not set" );

     		return ;

    }

    ildivnamlen = strlen( cldivtmp ) + strlen( TSMQUEFILENAME ) + 2 ;

    if (!cldivnam) cldivnam = malloc( ildivnamlen );

    if( !cldivnam )

    {

        dbg( TRACE, "no more space available" );

        return ;

    }

    else

    {	

        sprintf( cldivnam, "%s/%s", cldivtmp, TSMQUEFILENAME );

	}	

	fp = fopen(cldivnam,"r");

	if (fp == 0) return;

	while (fgets(daten,20,fp) ) ilCnt++;

	rewind(fp);

	if (quemod) free(quemod);

	if (ilCnt == 0) ilCnt = 1;	

	quemod = malloc((ilCnt)*sizeof (t_quemod));

	(quemod + 0)->source = 0;	/* default Zero values*/	

	(quemod + 0)->destination = 0;	/* default Zero values*/	

	ilCnt = 0;		

	while (fgets(daten,20,fp) )

	{

		if (!isdigit (daten[0]) ) continue;

		if( quep[0] = strtok(daten,",")) 	/* please insert no blank previous blanks should be allowed */

		{

			if( quep[1] = strtok(NULL,";\n") ) 	

			{

			      (quemod+ilCnt)->source =  atoi(quep[0]);

			      (quemod+ilCnt)->destination =  atoi(quep[1]);

 				  if (ilCnt == 0 
					  && (quemod+ilCnt)->source == 0 
						&& (quemod+ilCnt)->destination == 0) break;

  				  dbg( TRACE, "QUE [%d]: source %d desti = %d",ilCnt,(quemod+ilCnt)->source,(quemod+ilCnt)->destination);

			      ilCnt++;

			}

		}

	}

	fclose(fp);	

	ig_num_quemod = ilCnt;				

	return;

}

static void ena_evt()

{

	FILE	*fp;

	int	ilRC = RC_SUCCESS;				/* Return code */

	int	ilTmo ,ilCnt;	

	char	daten[20];

	char	*evtp[2];

	static char *cldivtmp = NULL ;

	static char *cldivnam = NULL ;

	int	ildivnamlen = 0 ;

	ilCnt = 0;

	ig_num_evtmod = ilCnt;		

 	MOREDB(dbg( TRACE, "ena_client event set up data" ));

	cldivtmp = getenv( "CFG_PATH" );

  	if( cldivtmp == NULL )

   	{

    	dbg( TRACE, "CFG_PATH not set" );

    	return ;

    }

    ildivnamlen = strlen( cldivtmp ) + strlen( TSMEVTFILENAME ) + 2 ;

    if (!cldivnam) cldivnam = malloc( ildivnamlen );

    if( !cldivnam )

    {

        dbg( TRACE, "no more space available" );

        return ;

    }

    else

    {	

        	sprintf( cldivnam, "%s/%s", cldivtmp, TSMEVTFILENAME );

	}	

	fp = fopen(cldivnam,"r");

	if (fp == 0) return;

	while (fgets(daten,20,fp) ) ilCnt++;

	rewind(fp);

	if (evtmod) free(evtmod);

	if (ilCnt == 0)

	ilCnt = 1;	

	evtmod = malloc((ilCnt)*sizeof (t_quemod));

	(evtmod + 0)->source = 0;	/* default Zero values*/	

	(evtmod + 0)->destination = 0;	/* default Zero values*/	

	ilCnt = 0;		

	while (fgets(daten,20,fp) )

	{

		if (!isdigit (daten[0]) ) continue;

		if( evtp[0] = strtok(daten,",")) 	/* please insert no blank previous blanks should be allowed */

		{

			if( evtp[1] = strtok(NULL,";\n") ) 	

			{

			      (evtmod+ilCnt)->source =  atoi(evtp[0]);

			      (evtmod+ilCnt)->destination =  atoi(evtp[1]);

				  if (ilCnt == 0 
					  && (evtmod+ilCnt)->source == 0 
						&& (evtmod+ilCnt)->destination == 0) break;

   				  dbg( TRACE, "EVT [%d]: source %d desti = %d",ilCnt,
					  (evtmod+ilCnt)->source,(evtmod+ilCnt)->destination);

			      ilCnt++;

			}

		}

	}

	fclose(fp);	

	ig_num_evtmod = ilCnt;				

	return;

}

#ifdef SEM_USED

/********************new  lateron usable    ******** Romahn CSA *** GRM ***/

int	setsem(int num)			/* set semaphore (only flag) */

{

	int rc;

	rc = syslibSetSemaphore(ilSemId, num);

	return (rc);

}

int 	tstsem(int num,int *op)

{

	int rc;

	rc = syslibTestSemaphore(ilSemId, num);

	return(rc);

}

int	clrsem(int num)

{

	int rc;

	rc = syslibClearSemaphore(ilSemId, num);	

	return (rc);

}

/***********************************************************end *** GRM ***/

#endif


/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/* end of the queuing primitive implementation */
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/***** OLD queuing functions. Only for saving these here. DO NOT USE !!!! ***/
/****************************************************************************/
#if 0
static int receive_item(int id,int length,ITEM *buff,int wait)
{
	int		return_value=RC_SUCCESS;
	int		rc= RC_SUCCESS;
	int		flag;
	char		*pclShmData;
	ITEM		*prlLocalItem;
	SHMADR		*prlShmAdr;
	char		pclLocalBuffer[1024];
	
	length -= sizeof(long);			/* Adjust for mtype */
	
	
	#ifdef _LINUX
		flag = IPC_NOWAIT;
	#else
		flag = ~0;
	#endif
	if ( wait != 0 ) {
		#ifdef _LINUX
			flag = 0;
		#else
			flag ^= IPC_NOWAIT;
		#endif
	} /* end if */

	dbg(DEBUG,"receive_item: entering msgrcv() qcp_output=(%d) for mod_id=(%d)!",qcp_output,id);
	return_value = msgrcv(qcp_output,buff,length,id,flag);
	dbg(DEBUG,"receive_item: leaving msgrcv() qcp_output=(%d) for mod_id=(%d)!",qcp_output,id);

	
	if (return_value < RC_SUCCESS) {
		return_value = QUE_E_GET;
	} /* end if */
	else {
		last_pri = buff->priority;
		if ( buff->function == QUE_SHM ) {
			memcpy(pclLocalBuffer,buff,sizeof(ITEM)+sizeof(SHMADR));
			prlLocalItem = (ITEM *)pclLocalBuffer;
			prlShmAdr=(SHMADR *)prlLocalItem->text;
			pclShmData = (char *)shmat(prlShmAdr->ShmId,NULL,0);
			if ( pclShmData == (char *)-1 ) {
				dbg(TRACE,"receive_item: Shared Memory Access error(ATT)");
				return_value = QUE_E_SEND;
			} /* end if */
			else {
			  if ( prlShmAdr->DataLength > length )
			  {
			    dbg(TRACE,"receive_item: Recbuf too small (%d) for %d bytes",
				length, prlShmAdr->DataLength);
			    rc = QUE_E_BUFSIZ;  /* buf too small */
			  }
			  else
			  {
			    memcpy(buff,pclShmData,prlShmAdr->DataLength);
			  } /* fi */
			  return_value = shmdt(pclShmData);
			  errno =0;
dbg(DEBUG,"IPC_RMID is %d",IPC_RMID);
dbg(DEBUG,"cuid = %d",prlShmAdr->ShmIdDs.shm_perm.cuid);
dbg(DEBUG,"cgid = %d",prlShmAdr->ShmIdDs.shm_perm.cgid);
dbg(DEBUG,"uid  = %d",prlShmAdr->ShmIdDs.shm_perm.uid);
dbg(DEBUG,"gid  = %d",prlShmAdr->ShmIdDs.shm_perm.gid);
dbg(DEBUG,"mode = %o",prlShmAdr->ShmIdDs.shm_perm.mode);
dbg(DEBUG,"segsz= %x",prlShmAdr->ShmIdDs.shm_segsz);
dbg(DEBUG,"cpid = %d",prlShmAdr->ShmIdDs.shm_cpid);
dbg(DEBUG,"lpid = %d",prlShmAdr->ShmIdDs.shm_lpid);
dbg(DEBUG,"natt = %d",prlShmAdr->ShmIdDs.shm_nattch);
dbg(DEBUG,"atime= %d",prlShmAdr->ShmIdDs.shm_atime);
dbg(DEBUG,"dtime= %d",prlShmAdr->ShmIdDs.shm_dtime);
dbg(DEBUG,"ctime= %d",prlShmAdr->ShmIdDs.shm_ctime);

			  return_value=shmctl(prlShmAdr->ShmId,IPC_RMID,
					      &prlShmAdr->ShmIdDs);
			  if ( return_value == -1 ) {
			    dbg(TRACE,"receive_item: IPC_RMID failed");
			    dbg(TRACE,"receive_item: Error %d is %s",errno, strerror(errno));
			  } /* end if */
			} /* end else */
		} /* end if */

		return_value = RC_SUCCESS; 
	} /* end else */
	if (rc == RC_SUCCESS)
	{
	  return return_value;
	}
	else
	{
	  return rc;
	}
} /* end of receive_item */
#endif
#if 0
static int recv_big_item(int id,int length,ITEM **buff,int wait)
{
	int return_value=RC_SUCCESS;
	int	rc= RC_SUCCESS;
	int	flag;
	char *pclShmData;
	ITEM *prlLocalItem;
	SHMADR *prlShmAdr;
	char pclLocalBuffer[1024];
	
	#ifdef _LINUX
		flag = IPC_NOWAIT;
	#else
		flag = ~0;
	#endif
	if ( wait != 0 ) 
	{
		#ifdef _LINUX
			flag = 0;
		#else
			flag ^= IPC_NOWAIT;
		#endif
	} /* end if */

	dbg(DEBUG,"recv_big_item1: buff at<%x>-><%x> length <%d>",(ITEM **)buff,(*(ITEM **)buff),length); 
	length = I_SIZE;

	dbg(DEBUG,"recv_big_item2: buff at<%x>-><%x> length <%d>",(ITEM **)buff,(*(ITEM **)buff),length); 

	errno = 0;
	(*(ITEM **)buff) = realloc((*(ITEM **)buff),length);
	if((*(ITEM **)buff) == NULL)
	{
		dbg(TRACE,"recv_big_item: realloc(%d) failed <%d><%s>",length,errno,strerror(errno));
		rc = QUE_E_MEMORY;
	}else{
		dbg(DEBUG,"recv_big_item: buff at<%x>-><%x>",(ITEM **)buff,(*(ITEM **)buff));
		memset((*(ITEM **)buff),0x00,length);
		errno = 0;
		dbg(DEBUG,"recv_big_item: entering msgrcv() qcp_output=(%d) for mod_id=(%d)!",qcp_output,id);
		return_value = msgrcv(qcp_output,(*(ITEM **)buff),length,id,flag);
		if (errno != ENOMSG && errno != 0)
		{
			dbg(TRACE,"recv_big_item: return_value=(%d), errno=(%d),strerror(%s)"
				,return_value,errno,strerror(errno));;
		}
		dbg(DEBUG,"recv_big_item: leaving msgrcv() qcp_output=(%d) for mod_id=(%d)!",qcp_output,id);
	} /* fi */

	if((return_value < RC_SUCCESS) || (rc!=RC_SUCCESS))
	{
		return_value = QUE_E_GET;
	} else {
		last_pri = (*(ITEM **)buff)->priority;
		if((*(ITEM **)buff)->function == QUE_SHM ) 
		{
			memcpy(pclLocalBuffer,(*(ITEM **)buff),sizeof(ITEM)+sizeof(SHMADR));
			prlLocalItem = (ITEM *)pclLocalBuffer;
			prlShmAdr=(SHMADR *)prlLocalItem->text;
		
			errno = 0;
			pclShmData = (char *)shmat(prlShmAdr->ShmId,NULL,0);
			if(pclShmData == (char *)-1 ) 
			{
				dbg(TRACE,"recv_big_item: shmat failed <%d><%s>",errno,strerror(errno));
				/* should be changed to matching error code (recv) */
				return_value = QUE_E_SEND;
			} else {
				dbg(DEBUG,"SHM Attached(id=%d)(len=%d)",prlShmAdr->ShmId,prlShmAdr->DataLength);
				
				errno = 0;
				/* Add 16 bytes for win NT, dont know why ...  JBE+VBL*/
				(*(ITEM **)buff) = realloc((*(ITEM **)buff),prlShmAdr->DataLength+16);
				if((*(ITEM **)buff) == NULL)
				{
					dbg(TRACE,"recv_big_item: realloc(%d) failed <%d><%s>",prlShmAdr->DataLength,errno,strerror(errno));
					rc = QUE_E_MEMORY;
				}else{
					dbg(DEBUG,"recv_big_item: buff at<%x>-><%x>",(ITEM **)buff,(*(ITEM **)buff));
					/* memset should init "Datalength" bytes and not only "length" bytes */
					memset((*(ITEM **)buff),0x00,length);
					memcpy((*(ITEM **)buff),pclShmData,prlShmAdr->DataLength);
				}/* fi */

				errno = 0;
				return_value = shmdt(pclShmData);

				dbg(DEBUG,"IPC_RMID is %d",IPC_RMID);
				dbg(DEBUG,"cuid = %d",prlShmAdr->ShmIdDs.shm_perm.cuid);
				dbg(DEBUG,"cgid = %d",prlShmAdr->ShmIdDs.shm_perm.cgid);
				dbg(DEBUG,"uid  = %d",prlShmAdr->ShmIdDs.shm_perm.uid);
				dbg(DEBUG,"gid  = %d",prlShmAdr->ShmIdDs.shm_perm.gid);
				dbg(DEBUG,"mode = %o",prlShmAdr->ShmIdDs.shm_perm.mode);
				dbg(DEBUG,"segsz= %x",prlShmAdr->ShmIdDs.shm_segsz);
				dbg(DEBUG,"cpid = %d",prlShmAdr->ShmIdDs.shm_cpid);
				dbg(DEBUG,"lpid = %d",prlShmAdr->ShmIdDs.shm_lpid);
				dbg(DEBUG,"natt = %d",prlShmAdr->ShmIdDs.shm_nattch);
				dbg(DEBUG,"atime= %d",prlShmAdr->ShmIdDs.shm_atime);
				dbg(DEBUG,"dtime= %d",prlShmAdr->ShmIdDs.shm_dtime);
				dbg(DEBUG,"ctime= %d",prlShmAdr->ShmIdDs.shm_ctime);
				/*********************	
				*************************/
				
				errno = 0;
				return_value=shmctl(prlShmAdr->ShmId,IPC_RMID, &prlShmAdr->ShmIdDs);
				if ( return_value == -1 ) 
				{
					dbg(TRACE,"recv_big_item: IPC_RMID failed");
					dbg(TRACE,"recv_big_item: Error %d is %s",errno, strerror(errno));
				} /* end if */
			} /* end else */
		}else{
			/* wanted data is already in buffer */
		} /* end if */

		return_value = RC_SUCCESS; 
	} /* end if */

	if (rc == RC_SUCCESS)
	{
		dbg(DEBUG,"recv_big_item: return <%d>",return_value);
		return return_value;
	} else {
		dbg(TRACE,"recv_big_item: return <%d>",rc);
		return rc;
	}
} /* end of recv_big_item */
#endif

/* ******************************************************************** */
/* added 20040728 JIM: for DEBUG only !!!                               */
/* The 	SendNextForQueue sets one NEXTNW flag via CPUT in the the SYSQCP  */
/* ******************************************************************** */
/* 20040728 JIM: added flag                                             */
extern void SendNextForQueue(int module, int flag)
{

  ITEM		*next_item;
	int			next_func; 
  
		next_func = flag;
		
		next_item = build_header(next_func,module,module,PRIORITY_5,0);
		msgsnd(qcp_input,next_item,sizeof(ITEM),IPC_NOWAIT);
		free((char *) next_item);

}
