#ifndef _DEF_mks_version_tablog_c
  #define _DEF_mks_version_tablog_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_tablog_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/tablog.c 1.2 2004/08/10 20:29:54SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*	**********************************************************************	*/
/*																*/
/*	CCS libary routine WriteLogRecord()								*/
/*																*/
/*	Author:			JM�											*/
/*	Date:			18.09.1997									*/
/*	Description:		Libary-routine to send Log-Information to loghdl		*/
/*																*/
/*	Update history:	JM� 19.01.1998 Ver�nderung der Modusbehandlung, intern	*/
/*					gehaltener Modus, TRANS_BEGIN und TRANS_COMMIT ent-	*/
/*					halten keinen Nachrichtenblock.					*/
/*																*/
/*	**********************************************************************	*/

static   char  sccs_version_tablog_lib[] = "@(#) UFIS 4.4 (c) ABB AAT/I tablog.c  44.2 / 00/01/07 14:35:38 / JMU";

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>

/*	#include	<sys/types.h>	*/
#include	<time.h>
#include	<sys/timeb.h>

#include	"ugccsma.h"
#include	"msgno.h"
#include	"glbdef.h"
#include	"quedef.h"
#include	"uevent.h"
/*	#include	"sthdef.h"	*/
/*	#include	"db_if.h"	*/
#include	"loghdl.h"

#define	MAX_FUNC_NO	10
#ifndef	LOG_PRIMARY
#define	LOG_PRIMARY	7700
#endif

extern	int	SubstTokOfLine(char *,char,char);
extern	int	GetIWord(char *,char *,int,int);
extern	void dbg(int,char *, ...);

char	*pcGFuncList[MAX_FUNC_NO] = { "UFR", "ISF", "DFR", "IRT", "IBT", "URT", "DRT", "DBT", "GNU", "GMU" };
/*	*************************	INSERT	***************************************************************************
	******************************	INSERT + GNU	*****************************************************************
	****************************************	UPDATE	************************************************************
	*********************************************	DELETE	*******************************************************
	*******************************************************	DELETE + GNU	****************************************
	************************************************************	NEW URNO	****************************************
	*****************************************************************	SET OF NEW URNOs	*************************
	*******************************************************************************************************************	*/

struct	_logpark {
	char				*Buffer;
	int				Length;
	struct _logpark	*next;
};
typedef	struct	_logpark	LOGPARK;
#define	LOGPARK_SIZE	sizeof(LOGPARK)

typedef	struct {
	int		Count;
	LOGPARK	*next;
} PARKHEAD;

static	PARKHEAD	Head;
static	LOGPARK	*pSGActPtr;
static	int		iGTransMode;	/*	Neu 19.01.1998 JM�	*/
extern	int		mod_id;
extern	int		debug_level;

		int	WriteLogRecord(int,char *,int,char *,char *,char *,char *,char *,char *);
static	int	DeleteList();
static	int	SendEvent(char *,int);
static	int	SendTransAction();
static	void	PrintLogComSmall(EVENT *,LOGCOM *);


int	WriteLogRecord(int ipMode,char *pcpCom,int ipRou,char *pcpLead,char *pcpUser,char *pcpTab,char *pcpOrno,char *pcpFields,char *pcpValues)
{
	int		ilRC = RC_SUCCESS,
			ili,
			ilFound,
			ilLen,
			ilOffLen;
	EVENT	*pSlEvent;
	LOGCOM	*pSlLogCom, SlLogCom;
	char		*pclBufPtr,
			pclTable[MAX_TABNAME_SIZE];

dbg(DEBUG,"WriteLogRecord() received Command with mode = %d\n",ipMode);
	if ( (ipMode == INIT_TRANSACTION_HEAD) || (ipMode == TRANS_BEGIN) ) {
dbg(DEBUG,"WriteLogRecord() received Initialization Command\n");
		Head.Count	= 0;
		Head.next		= NULL;
		pSGActPtr		= NULL;
		iGTransMode	= TRANS_BEGIN;
		return RC_SUCCESS;
	}	/*	end if	*/

	if ( ipMode == TRANS_COMMIT ) {
dbg(DEBUG,"WriteLogRecord() received Commit Command\n");
		if ( iGTransMode == TRANS_MEMBER ) {
			ilRC = SendTransAction();
			iGTransMode = TRANS_BEGIN;
		}	/*	end if	*/
		return ilRC;
	}	/*	end if	*/
	if ( ipMode == TRANS_ROLLBACK ) {
dbg(DEBUG,"WriteLogRecord() received Rollback Command\n");
		if ( iGTransMode == TRANS_MEMBER ) {
			ilRC = DeleteList();
			if (ilRC != RC_SUCCESS) {
				dbg(DEBUG,"WriteLogRecord() returns with Error -> DeleteList() failed\n");
			}	/*	end if	*/
			iGTransMode = TRANS_BEGIN;
		}	/*	end if	*/
		return ilRC;
	}	/*	end if	*/

/*	Welcher Typ Nachricht?	*/

dbg(DEBUG,"WriteLogRecord() received Data Command\n");
	for (ili = 0, ilFound = FALSE; ili < MAX_FUNC_NO; ili++) {
		if (strncmp(pcpCom,pcGFuncList[ili],3) == 0) {
			strncpy(SlLogCom.Function,pcpCom,MAX_FUNCTION_SIZE);
dbg(DEBUG,"WriteLogRecord() Function '%s' is Valid (%s)\n",pcpCom,SlLogCom.Function);
			ilFound = TRUE;
			break;
		}	/*	end if strncmp()	*/
	}	/*	end for	*/
	if (ilFound == FALSE) {
		dbg(DEBUG,"WriteLogRecord() returns with Error -> %s is not a correct Command\n",pcpCom);
		iGTransMode = TRANS_BEGIN;
		return RC_FAIL;
	}	/*	end if ilFound	*/
	
/*	Sind alle Parameter in Ordnung?	*/

	if ( (pcpTab == NULL) || (pcpOrno == NULL) || (ipRou < 0) || (pcpUser == NULL) ) {
		dbg(DEBUG,"WriteLogRecord() returns with Error -> Parameter Error with Table or Original URNO or Route\n");
		iGTransMode = TRANS_BEGIN;
		return RC_FAIL;
	}	/*	end if	*/
	strncpy(SlLogCom.Table,pcpTab,MAX_TABNAME_SIZE);
	SlLogCom.UserRoute = ipRou;

	SubstTokOfLine(pcpOrno,44,32);
	for (ili = 0; GetIWord(pcpOrno,pclTable,MAX_URNO_SIZE,ili) > 0; ili++)
		;

/*	Bereite Meldung f�r den Versand vor!	*/

/*	1.	Calculate Buffer Size	*/

	ilLen = EVENT_H_LEN + LOGCOM_SIZE + 5;
	if (pcpLead == NULL)
		memcpy(SlLogCom.LeaderUrno," ",2);
	else	
		strncpy(SlLogCom.LeaderUrno,pcpLead,MAX_TABNAME_SIZE);
	if (pcpUser == NULL) {
		if (ipRou <= 20000) {
			SlLogCom.UserLen = 1;
			ilLen += 1;
		}	/*	end if pcpUser == NULL	*/
		else {
			dbg(DEBUG,"WriteLogRecord() User Error\n");
			iGTransMode = TRANS_BEGIN;
			return RC_FAIL;
		}	/*	end else	*/
	}	/*	end if	*/
	else {
		SlLogCom.UserLen = strlen(pcpUser);
		ilLen += SlLogCom.UserLen;
	}	/*	end if	*/
	SlLogCom.OrnoLen = strlen(pcpOrno);
	ilLen += SlLogCom.OrnoLen;
	if (pcpFields == NULL) {
		SlLogCom.FieldLen = 1;
		ilLen += 1;
	}	/*	end if	*/
	else {
		SlLogCom.FieldLen = strlen(pcpFields);
		ilLen += SlLogCom.FieldLen;
	}	/*	end if	*/
	if (pcpValues == NULL) {
		SlLogCom.ValueLen = 1;
		ilLen += 1;
	}	/*	end if	*/
	else {
		SlLogCom.ValueLen = strlen(pcpValues);
		ilLen += SlLogCom.ValueLen;
	}	/*	end if	*/

	pSlEvent = (EVENT *) calloc(1,ilLen);
	if (pSlEvent == NULL) {
		dbg(DEBUG,"WriteLogRecord() returns with Error -> Unable to allocate Event-Buffer\n");
		iGTransMode = TRANS_BEGIN;
		return RC_FAIL;
	}	/*	end if	*/
	pSlEvent->type = USR_EVENT;
	pSlEvent->command = EVENT_DATA;
	pSlEvent->retry_count = 0;
	pSlEvent->originator = mod_id;
	pSlEvent->data_offset = EVENT_H_LEN;
	ilOffLen = 0;
	pSlLogCom = (LOGCOM *) ((char *) pSlEvent + EVENT_H_LEN);
	memcpy(pSlLogCom,&SlLogCom,LOGCOM_SIZE);
	pclBufPtr = (char *) pSlEvent + LOGCOM_SIZE + EVENT_H_LEN;
	pSlLogCom->UserName = 0;
	if (pcpUser == NULL) {
		memcpy(pclBufPtr," ",1);
		pclBufPtr++;
		*pclBufPtr = 0;
		pclBufPtr++;
		ilOffLen = 2;
	}	/*	end if	*/
	else {
		strncpy(pclBufPtr,pcpUser,pSlLogCom->UserLen);
		pclBufPtr += pSlLogCom->UserLen;
		*pclBufPtr = 0;
		pclBufPtr++;
		ilOffLen += (1 + pSlLogCom->UserLen);
	}	/*	end else	*/
	pSlLogCom->OrnoList = ilOffLen;
	strncpy(pclBufPtr,pcpOrno,pSlLogCom->OrnoLen);
	pclBufPtr += pSlLogCom->OrnoLen;
	*pclBufPtr = 0;
	pclBufPtr++;
	ilOffLen += (1 + pSlLogCom->OrnoLen);
	pSlLogCom->FieldList = ilOffLen;
	if (pcpFields == NULL) {
		strncpy(pclBufPtr," ",1);
		pclBufPtr++;
		*pclBufPtr = 0;
		pclBufPtr++;
		ilOffLen = 2;
	}	/*	end if	*/
	else {
		strncpy(pclBufPtr,pcpFields,pSlLogCom->FieldLen);
		pclBufPtr += pSlLogCom->FieldLen;
		*pclBufPtr = 0;
		pclBufPtr++;
		ilOffLen += (1 + pSlLogCom->FieldLen);
	}	/*	end else	*/
	pSlLogCom->ValueList = ilOffLen;
	if (pcpValues == NULL) {
		strncpy(pclBufPtr," ",1);
		pclBufPtr++;
		*pclBufPtr = 0;
		pclBufPtr++;
		ilOffLen = 2;
	}	/*	end if	*/
	else {
		strncpy(pclBufPtr,pcpValues,pSlLogCom->ValueLen);
		pclBufPtr += pSlLogCom->ValueLen;
		*pclBufPtr = 0;
		pclBufPtr++;
		ilOffLen += (1 + pSlLogCom->ValueLen);
	}	/*	end else	*/
	pSlEvent->data_length = ilLen - EVENT_H_LEN;

/*	Versende die Nachricht oder speichere sie	*/
/*	PrintLogComSmall(pSlEvent,pSlLogCom);	*/
	if (ipMode == TRANS_MEMBER) {
		if ( (iGTransMode == TRANS_BEGIN) || (iGTransMode == TRANS_MEMBER) ) {
			if (Head.Count == 0) {
				Head.next = calloc(1,(LOGPARK_SIZE + ilLen));
				if (Head.next == NULL) {
					ilRC = RC_FAIL;
					dbg(DEBUG,"WriteLogRecord() returns with Error -> Unable to allocate LOGPARK-Buffer\n");
					iGTransMode = TRANS_BEGIN;
				}	/*	end if	*/
				else {
					pSGActPtr = Head.next;
					pSGActPtr->next = NULL;
					pSGActPtr->Length = ilLen;
					pSGActPtr->Buffer = (char *) pSlEvent;
					Head.Count = 1;
					if (iGTransMode == TRANS_BEGIN) {
						iGTransMode = TRANS_MEMBER;
					}	/*	end if	*/
				}	/*	end else	*/
			}	/*	end if	*/
			else {
				pSGActPtr->next = calloc(1,(LOGPARK_SIZE + ilLen));
				if (pSGActPtr->next == NULL) {
					ilRC = RC_FAIL;
					dbg(DEBUG,"WriteLogRecord() returns with Error -> Unable to allocate LOGPARK-Buffer\n");
					ilRC = DeleteList();
					if (ilRC != RC_SUCCESS) {
						dbg(DEBUG,"WriteLogRecord() returns with Error -> DeleteList() failed\n");
					}	/*	end if	*/
					iGTransMode = TRANS_BEGIN;
				}	/*	end if	*/
				else {
					pSGActPtr = pSGActPtr->next;
					pSGActPtr->next = NULL;
					pSGActPtr->Length = ilLen;
					pSGActPtr->Buffer = (char *) pSlEvent;
					Head.Count++;
					if (iGTransMode == TRANS_BEGIN) {
						iGTransMode = TRANS_MEMBER;
					}	/*	end if	*/
				}	/*	end else	*/
			}	/*	end if	*/
		}	/*	end else	*/
	}	/*	end if	*/
/*		case TRANS_COMMIT	:
			if ( iGTransMode == TRANS_MEMBER ) {
				ilRC = SendTransAction();
				iGTransMode = TRANS_BEGIN;
			}		end if	
			break;
		case TRANS_ROLLBACK	:
			if ( iGTransMode == TRANS_MEMBER ) {
				ilRC = DeleteList();
				if (ilRC != RC_SUCCESS) {
					dbg(DEBUG,"WriteLogRecord() returns with Error -> DeleteList() failed\n");
				}		end if	
				iGTransMode = TRANS_BEGIN;
			}		end if	
			break;
*/
	else {
		pSlLogCom->IntCom = NO_TRANS;
		ilRC = SendEvent((char *) pSlEvent,ilLen);
		if (ilRC != RC_SUCCESS) {
			dbg(DEBUG,"WriteLogRecord() returns with Error -> SendEvent() failed\n");
		}	/*	end if	*/
		iGTransMode = TRANS_BEGIN;
		free (pSlEvent);
	}	/*	end else	*/
	return ilRC;
}	/*	end of WriteLogRecord()	*/

static	int	DeleteList()
{
	LOGPARK	*pSlActPtr,
			*pSlWorkPtr;
	int		ili,
			ilRC = RC_SUCCESS;

	pSlActPtr = Head.next;
	for (ili = 0;(pSlActPtr != NULL) && (ili < Head.Count); ili++) {
		pSlWorkPtr = pSlActPtr;
		pSlActPtr = pSlActPtr->next;
		free(pSlWorkPtr);
	}	/*	end for	*/
	if (ili < Head.Count) {
		dbg(DEBUG,"DeleteList() returns with Error -> Count bigger than Number of records\n");
		ilRC = RC_FAIL;
	}	/*	end if	*/
	Head.Count = 0;
	return ilRC;
}	/*	end of DeleteList()	*/
	
static	int	SendEvent(char *pcpBuffer,int ipLen)
{
	int	ilRC = RC_SUCCESS;

	ilRC = que(QUE_PUT,LOG_PRIMARY,mod_id,PRIORITY_3,ipLen,pcpBuffer);
dbg(DEBUG,"SendEvent() returns with %d for QUE_PUT to %d\n",ilRC,LOG_PRIMARY);
	if (ilRC != RC_SUCCESS) {
		dbg(DEBUG,"SendEvent() returns with Error -> QUE_PUT to %d Failed\n",LOG_PRIMARY);
	} /* end if */
	return ilRC;
}	/*	end of SendEvent()	*/

static	int	SendTransAction()
{
	int		ilRC = RC_SUCCESS,
			ili;
	LOGPARK	*pSlActPtr,
			*pSlWorkPtr;
	EVENT	*pSlEvent;
	LOGCOM	*pSlLogCom;

dbg(DEBUG,"WriteLogRecord() Now in Send Transaction");
	for (ili = 0, pSlActPtr = Head.next; ili < Head.Count; ili++) {
dbg(DEBUG,"WriteLogRecord() Send Transaction Data No %d",ili + 1);
		if (pSlActPtr != NULL) {
			pSlEvent = (EVENT *) pSlActPtr->Buffer;
			pSlLogCom = (LOGCOM *) ((char *) pSlEvent + EVENT_H_LEN);
			PrintLogComSmall(pSlEvent,pSlLogCom);
			ilRC = SendEvent(pSlActPtr->Buffer,pSlActPtr->Length);
			if (ilRC != RC_SUCCESS) {
				dbg(DEBUG,"SendTransAction() returns with Error -> SendEvent Failed\n");
				break;
			} /* end if */
			else {
				pSlWorkPtr = pSlActPtr;
				pSlActPtr = pSlActPtr->next;
				free(pSlWorkPtr);
			}	/*	end else	*/
		}	/*	end if	*/
		else {
			ilRC = RC_SUCCESS;
			dbg(DEBUG,"SendTransAction() returns with Error -> Count bigger than Number of records\n");
			break;
		} /* end else */
	}	/*	end for	*/
	if (ilRC != RC_SUCCESS) {
		ili = DeleteList();
		if (ilRC != RC_SUCCESS) {
			dbg(DEBUG,"SendTransAction() returns with Error -> DeleteList() failed\n");
		}	/*	end if	*/
	}	/*	end if	*/
	return ilRC;
}	/*	end of SendTransAction	*/
		
static	void	PrintLogComSmall(EVENT *pSpEvent,LOGCOM *pSpCom)
{
	dbg(TRACE,"Print Event");
	dbg(TRACE,"Type:\t%d",pSpEvent->type);
	dbg(TRACE,"Type:\t%d",pSpEvent->command);
	dbg(TRACE,"Type:\t%d",pSpEvent->retry_count);
	dbg(TRACE,"Type:\t%d",pSpEvent->originator);
	dbg(TRACE,"Type:\t%d",pSpEvent->data_offset);
	dbg(TRACE,"Type:\t%d",pSpEvent->data_length);
	dbg(TRACE,"Print Buffer");
	dbg(TRACE,"Com:\t%d",pSpCom->IntCom);
	dbg(TRACE,"Rou:\t%d",pSpCom->UserRoute);
	dbg(TRACE,"Fkt:\t%s",pSpCom->Function);
	dbg(TRACE,"Tab:\t%s",pSpCom->Table);
	dbg(TRACE,"LUn:\t%s",pSpCom->LeaderUrno);
	return;
}
