#ifndef _DEF_mks_version_init_c
  #define _DEF_mks_version_init_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_init_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/init.c 1.2 2004/08/10 20:22:01SGT jim Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************/
/* 20021127 JIM: initialize_no_dbg: avoid (empty) putque log files      */
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/


#include "init.h"
#define		UGCCS_FKT
extern SPTREC *psptrec;			/* SPTAB structure */
extern GBLADR *pgbladr;			/* GBLADR structure */
extern STHBCB *pbcb;			/* STH bcb */

extern long shrd_mem_id;		/* shared memory id */
extern long shrd_mem_size;		/* shared memory size */

extern long glbrc;			/* global return code */

extern char *shm_acc();			/* Shared memory access */

/**************************  Function prototype header **********************/
extern  int init(void );
/****************************************************************************/

init()

{

	if ((shrd_mem_id =
	     shm_ass(shrd_mem_size,(long ) 0666)) == FATAL)
                     return(-1);	/* Assign failed */
	     
	if ((shm_acc(shrd_mem_id,NULL,shrd_mem_size,
		     pgbladr)) == FAILURE)
		     return(-2);	/* Access failed */
	     
	pbcb->function = (STTABLE|STLOCATE); /* locate SPTAB */
	pbcb->inputbuffer = NULL;
	pbcb->outputbuffer = (char *) &psptrec;
        pbcb->tabno = SPTAB;
	
	if ((glbrc = sth(pbcb,pgbladr)) !=0)
	    return(glbrc);		/* Locate failed */

	return(RC_SUCCESS);
}
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/* INITIALIZE macro as function */

extern char *mod_name;
extern char __CedaLogFile[128];
extern FILE *outp;
extern int mod_id;
extern int ctrl_sta;
 
void initialize(int argc,char *argv[])
{
	mod_name = argv[0];
  /* 20020822 JIM: if argv[0] contains path (i.e. started by ddd), skip path */
  if (strrchr(mod_name,'/')!= NULL)
  {
     mod_name= strrchr(mod_name,'/');
     mod_name++;
  }
        sprintf(__CedaLogFile,"%s/%s%.5ld.log",getenv("DBG_PATH"),mod_name,getpid());
        outp = fopen(__CedaLogFile,"w");
        if (argc > 1) 
        {
                mod_id = atoi(argv[1]);
                ctrl_sta = atoi(argv[2]);
        } else {
                mod_id = 0;
                ctrl_sta = 1;
        }
        glbrc = init();
        if(glbrc)
        {
                fprintf(outp,"\n<%s><%ld>STHINIT failed: ret = %d\n",mod_name,getpid(),glbrc);
                exit(1);
        }
}

void initialize_no_dbg(int argc,char *argv[])
{
	mod_name = argv[0];
  /* 20020822 JIM: if argv[0] contains path (i.e. started by ddd), skip path */
  if (strrchr(mod_name,'/')!= NULL)
  {
     mod_name= strrchr(mod_name,'/');
     mod_name++;
  }
        sprintf(__CedaLogFile,"%s/%s%.5ld.log",getenv("DBG_PATH"),mod_name,getpid());

/* avoid    outp = fopen(__CedaLogFile,"w"); for empty putque log files */
        if (argc > 1) 
        {
                mod_id = atoi(argv[1]);
                ctrl_sta = atoi(argv[2]);
        } else {
                mod_id = 0;
                ctrl_sta = 1;
        }
        glbrc = init();
        if(glbrc)
        {
                fprintf(outp,"\n<%s><%ld>STHINIT failed: ret = %d\n",mod_name,getpid(),glbrc);
                exit(1);
        }
}
