#ifndef _DEF_mks_version_dmslib_c
  #define _DEF_mks_version_dmslib_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_dmslib_c[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/dmslib.c 1.1 2011/08/09 16:27:05SGT bst Exp  $";
#endif /* _DEF_mks_version_dmslib_c */

#include <stdlib.h>
#include <string.h>
#include <stdio.h> 
#include <malloc.h>

#include "ct.h"
#include "db_if.h"
#include "glbdef.h"
#include "dmslib.h"


#define CT_TRUE -1
#define CT_FALSE 0 

#define SORT_ASC        0
#define SORT_DESC       1

#define FOR_INIT		0

#define URNO_SIZE 10

#define MAX_CTFLT_VALUE 4

static STR_DESC argCtFlValue[MAX_CTFLT_VALUE+1];
static STR_DESC rgCtDmsLine;
static STR_DESC rgCtDmsData;
static STR_DESC rgCtDmsList;
static STR_DESC rgCtDmsUrno;

static STR_DESC rgCtAftLine;
static STR_DESC rgCtAftData;
static STR_DESC rgCtAftList;
static STR_DESC rgCtAftUrno;
