#ifndef _DEF_mks_version_timest_h
  #define _DEF_mks_version_timest_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_timest_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/timest.h 1.2 2004/07/27 17:04:41SGT jim Exp  $";
#endif /* _DEF_mks_version */
#ifndef _TIMEST_H_
#define _TIMEST_H_

/*
 * useful time conversions
 */


#include <time.h>

/*
 * Convert 14 character time string (YYYYMMDDhhmmss) into unix time
 */
extern time_t TimeStamp2Unix(char *cpStamp);

/*
 * Convert unix time into 14 character time string (YYYYMMDDhhmmss)
 */
extern char *TimeUnix2Stamp(time_t ipTime);

/*
 * Convert 14 character time string (YYYYMMDDhhmmss) into unix time
 */
extern char *TimeStampNow(void);

/*
 * Convert date like "08APR1996" into unix time
 */

extern time_t TimeDDMMMYYYY2Unix(char *pcpDateIn);

#endif
