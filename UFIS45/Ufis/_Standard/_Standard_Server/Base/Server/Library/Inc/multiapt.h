#ifndef _DEF_mks_version_multiapt_h
  #define _DEF_mks_version_multiapt_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_multiapt_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/multiapt.h 1.2 2004/07/27 16:48:07SGT jim Exp  $";
#endif /* _DEF_mks_version */
  
#ifndef __MULTIAPT_H
#define __MULTIAPT_H
#endif

/* ============================================================
  WorkAround: due to limitations at using some internal 
  functions it's strongly suggested to use this define
  directive to determine the size of the out-buffer for the 
  CollectNonHopoTables function.
  
  Declaration example:
  
  char * pclMyBuffer[MAX_NO_HOPO*(TAB_NAME_LEN+SEP_FLD_LEN)];
*/

#define MAX_NO_HOPO  100

/* extern char pcgTwEnd[33]; */

/* =============================================================
  Retrieves the HOPO from the pcgTwEnd field and check it for 
  the right size. It returns the HOPO in the buffer   variable 
  and RC_SUCCESS if successful otherwise RC_FAIL.
*/
int GetHomePort(char * );

/* =============================================================
  Check with basic data if apc3 is a valid three letter code and
  returns RC_SUCCESS if succesful otherwise RC_FAIL.
*/

int CheckAPC3(char * );
/* =============================================================
  Checks if the specified table uses HOPO and returns RC_SUCCESS
  if affirmative otherwise RC_FAIL.
*/
int CheckHopoUsage(char * );

/* =============================================================
  Returns a list of tables which are not using HOPOs. It has to 
  be used during the process initialization to get the list.
  WARNING: since the internal syslib function used does not 
  allow the reallocation of the pointer passed, if there is not
  enough room in the destination buffer this function may core
  dump !!!!!!
  HINT: look at the MAX_NO_HOPO define directive
*/
int CollectNonHopoTables(char * );

