#ifndef _DEF_mks_version_fditools_h
  #define _DEF_mks_version_fditools_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_fditools_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/fditools.h 1.2 2004/07/27 16:47:42SGT jim Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* CCS Program Skeleton                                                       */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
#ifndef _FDITOOLS_H
#define _FDITOOLS_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "tools.h"
#include "glbdef.h"

extern int GetNextDataItem(char *pcpResult,char **pcpInput,char *pcpDel,char *pcpDef,char *pcpTrim);
extern int GetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel,char *pcpDef,char *pcpTrim);
extern int FindItemInList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos);
/*extern int GetWord(char *pcpResult, char *pcpInput,int ipNum);*/
extern int MakeTokenList(char *pcpResult, char *pcpInput, char *pcpOldDelim, char cpDelim );
extern int CountToDelim(char *pcpInput,int ipNum,char cpDelim);



#endif /* _FDITOOLS_H  */




