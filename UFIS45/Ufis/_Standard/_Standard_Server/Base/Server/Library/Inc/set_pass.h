#ifndef _DEF_mks_version_set_pass_h
  #define _DEF_mks_version_set_pass_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_set_pass_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/set_pass.h 1.2 2004/07/27 16:48:26SGT jim Exp  $";
#endif /* _DEF_mks_version */
#ifndef _SET_PASS_H
#define _SET_PASS_H


#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "helpful.h"

int main(int, char **);

#endif /* _SET_PASS_H */
