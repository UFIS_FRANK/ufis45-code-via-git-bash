#ifndef _DEF_mks_version_bchdl_h
  #define _DEF_mks_version_bchdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_bchdl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/bchdl.h 1.2 2004/07/27 16:46:45SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.						  */
/*									  */
/*  Program	  :     						  */
/*  Revision date :							  */
/*  Author    	  :							  */
/*  									  */
/*  NOTE : This should be the only include file for your program          */
/* ********************************************************************** */

#ifndef __BCHDL_INC
#define __BCHDL_INC


#define BC_REGISTER_CMD	"HRG"
#define BC_UNREGISTER_CMD	"HUR"

#endif
