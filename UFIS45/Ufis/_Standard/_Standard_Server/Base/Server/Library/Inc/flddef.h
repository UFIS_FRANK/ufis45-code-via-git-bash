#ifndef _DEF_mks_version_flddef_h
  #define _DEF_mks_version_flddef_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_flddef_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/flddef.h 1.2 2004/07/27 16:47:44SGT jim Exp  $";
#endif /* _DEF_mks_version */
#ifndef _FLDDEF_H
#define _FLDDEF_H


#define          FIELD_1            0
#define          FIELD_2            1
#define          FIELD_3            2
#define          FIELD_4            3
#define          FIELD_5            4
#define          FIELD_6            5
#define          FIELD_7            6
#define          FIELD_8            7
#define          FIELD_9            8
#define          FIELD_10           9
#define          FIELD_11           10
#define          FIELD_12           11
#define          FIELD_13           12
#define          FIELD_14           13
#define          FIELD_15           14
#define          FIELD_16           15
#define          FIELD_17           16
#define          FIELD_18           17
#define          FIELD_19           18
#define          FIELD_20           19
#define          FIELD_21           20
#define          FIELD_22           21
#define          FIELD_23           22
#define          FIELD_24           23
#define          FIELD_25           24
#define          FIELD_26           25
#define          FIELD_27           26
#define          FIELD_28           27
#define          FIELD_29           28
#define          FIELD_30           29
#define          FIELD_31           30
#define          FIELD_32           31
#define          FIELD_33           32
#define          FIELD_34           33
#define          FIELD_35           34
#define          FIELD_36           35
#define          FIELD_37           36
#define          FIELD_38           37
#define          FIELD_39           38
#define          FIELD_40           39
#define          FIELD_41           40
#define          FIELD_42           41
#define          FIELD_43           42
#define          FIELD_44           43
#define          FIELD_45           44
#define          FIELD_46           45
#define          FIELD_47           46
#define          FIELD_48           47
#define          FIELD_49           48
#define          FIELD_50           49
#define          FIELD_51           50
#define          FIELD_52           51
#define          FIELD_53           52
#define          FIELD_54           53
#define          FIELD_55           54
#define          FIELD_56           55
#define          FIELD_57           56
#define          FIELD_58           57
#define          FIELD_59           58
#define          FIELD_60           59
#define          FIELD_61           60
#define          FIELD_62           61
#define          FIELD_63           62
#define          FIELD_64           63
#define          FIELD_65           64
#define          FIELD_66           65
#define          FIELD_67           66
#define          FIELD_68           67
#define          FIELD_69           68
#define          FIELD_70           69
#define          FIELD_71           70
#define          FIELD_72           71
#define          FIELD_73           72
#define          FIELD_74           73
#define          FIELD_75           74
#define          FIELD_76           75
#define          FIELD_77           76
#define          FIELD_78           77
#define          FIELD_79           78
#define          FIELD_80           79
#define          FIELD_81           80
#define          FIELD_82           81
#define          FIELD_83           82
#define          FIELD_84           83
#define          FIELD_85           84
#define          FIELD_86           85
#define          FIELD_87           86
#define          FIELD_88           87
#define          FIELD_89           88
#define          FIELD_90           89
#define          FIELD_91           90
#define          FIELD_92           91
#define          FIELD_93           92
#define          FIELD_94           93
#define          FIELD_95           94
#define          FIELD_96           95
#define          FIELD_97           96
#define          FIELD_98           97
#define          FIELD_99           98
#define          FIELD_100          99

#endif
