#ifndef _DEF_mks_version_bhsif_h
  #define _DEF_mks_version_bhsif_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_bhsif_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/bhsif.h 1.9 2008/09/23 15:18:33SGT akl Exp  $";
#endif /* _DEF_mks_version */
/**************************************************************/
/* BAGGAGE-HANDLING Interface (BHSIF) header file        */
/* for ATHENS International Airport                      */
/**************************************************************/
#ifndef BOOL
#define BOOL int
#endif

/****************/
/* ALL INCLUDES */
/****************/
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <dirent.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "db_if.h"
#include "sthdef.h"
#include "helpful.h"
#include "hsbsub.h"
#include "debugrec.h"
#include "netin.h"
#include "tcputil.h"
#include "list.h"
#include "ftptools.h"
#include "ftphdl.h"
#include "libccstr.h"
#include "syslib.h"

/*********************/
/* common structures */
/*********************/
/* telegram header */
typedef struct
{
  char start[1];				/* start of message = '/' hex(0x02) */
  char cycle_no[1];			/* cycle nr. !NOT USED! JWE 18.11.1998 will be hex(0x30) */ 
  char t_type[2];				/* message identifier */
  char sender[2];				/* sending system ID. We're always 'FO' */
  char receiver[2];			/* receiving system ID */
  char order_no[2];			
}HEAD;

/* telegram end */
typedef struct
{
	char	time[9];				/* time of telegram creation and send */
	char	tail[5];				/* end of message sign='  (CR)(ETX)*/  
							/* CR=hex(0xD) */
							/* ETX=hex(0x03) */  
}TAIL;
/**********************/
/* different messages */
/**********************/
/* flight information's */
typedef struct
{
	char	tifd[15];
	char	adid[2];
	char	ftyp[2];
	char	flno[10];
	char	stod[15];
	#if 0
	char	etdi[15];
	char	alc3[4];
	char	act3[4];
	char	nose[4];
	char	vian[3];
	char	vial[1030];
	char	des3[4];
	char	baz1[4];
	char	baz2[4];
	char	baz3[4];
	char	baz4[4];
	char	baz5[4];
	#endif
}FI_INT;

/* internal Flight information message */
/* (update & insert) for sending to BHS */
typedef struct
{
	char	day[1];
	char	flno[8];
	char	status[1];
	char	handag[3];
	char	act[3];
	char	cap[4];
	char	stod[12];
	char	etod[12];
	char	dest1[3];
	char	chute1[3];
	char	o_tim1[12];
	char	c_tim1[12];
	char	dest2[3];
	char	chute2[3];
	char	o_tim2[12];
	char	c_tim2[12];
	char	dest3[3];
	char	chute3[3];
	char	o_tim3[12];
	char	c_tim3[12];
	char	dest4[3];
	char	chute4[3];
	char	o_tim4[12];
	char	c_tim4[12];
	char	dest5[3];
	char	chute5[3];
	char	o_tim5[12];
	char	c_tim5[12];
	char	dest6[3];
	char	chute6[3];
	char	o_tim6[12];
	char	c_tim6[12];
	char	dest7[3];
	char	chute7[3];
	char	o_tim7[12];
	char	c_tim7[12];
	char	dest8[3];
	char	chute8[3];
	char	o_tim8[12];
	char	c_tim8[12];
	char	dest9[3];
	char	chute9[3];
	char	o_tim9[12];
	char	c_tim9[12];
	char	dest10[3];
	char	chute10[4];
	char	o_tim10[12];
	char	c_tim10[12];
}CAI_OUT;

/* external chute allocation message */
/* for receiving from BHS */
typedef struct
{
	char	indi[1];
	char	chute[3];
	char	class[1];
	char	flno[8];
	char	des3[3];
	char	stod[12];
	char	etod[12];
	char	o_tim[12];
	char	c_tim[12];
}CAI_IN;
typedef struct
{
	char	indi[1];
	char	chute[3];
	char	class[1];
	char	transit[1];
	char	flno[8];
	char	des3[3];
	char	stod[12];
	char	etod[12];
	char	o_tim[12];
	char	c_tim[12];
}CAI_IN_T;

typedef struct 
{
	char cdat[15];
	char des3[4];
	char des4[5];
	char fcla[2];
	char lafx[2];
	char ftyp[2];
	char hopo[4];
	char indi[2];
	char lnam[6];
	char lstu[15];
	char ltyp[4];
	char rtab[4];
	char rurn[11];
	char stob[15];
	char stoe[15];
	char tifb[15];
	char tife[15];
	char tisb[2];
	char tise[2];
	char urno[11];
	char usec[33];
	char useu[33];
}CAI_DB;

/* chute status message */
typedef struct
{
	char	indi[1];
	char	chute[3];
	char	class[1];
	char	flno[8];
	char	des3[3];
	char	stod[12];
	char	etod[12];
	char	o_tim[12];
	char	c_tim[12];
}CST_IN;
typedef struct
{
	char	indi[1];
	char	chute[3];
	char	class[1];
	char	transit[1];
	char	flno[8];
	char	des3[3];
	char	stod[12];
	char	etod[12];
	char	o_tim[12];
	char	c_tim[12];
}CST_IN_T;

/* chute status message */
typedef struct
{
	char ftyp[2];
	char rurn[11];
	char chute[4];
	char lstu[15];
	char tifb[15];
	char tife[15];
	char tisb[2];
	char tise[2];
}CST_DB;

/* flight cancelled message */
typedef struct
{
	char	day[1];
	char	flno[8];
	char	stod[13];
}CXX_EXT;

/* telegram acknowledge */
typedef struct
{
	HEAD	head;						/* acknowledge header */  
	TAIL	tail;						/* acknowledge end */  						
}ACK_EXT;

typedef struct
{
	HEAD	head;						/* acknowledge header */  
	TAIL	tail;						/* acknowledge end */  						
}ALIVE;
/***************/
/***************/
/* ALL DEFINES */
/***************/
#define XS_BUFF  128
#define S_BUFF  512
#define M_BUFF  1024
#define L_BUFF  2048
#define XL_BUFF 4096

#ifndef MAX_BUFFER_LEN
	#define MAX_BUFFER_LEN 2048
#endif
#define DATABLK_SIZE  524288 /* Maximum space for sql_if data_area&sql-buffer*/
#define SOM  (char)0x02 /* start of message */
#define EOT  (char)0x03 /* end of message */
#define MAX_TELEGRAM_LEN 400
#define CONNECT_TIMEOUT 5
#define READ_TIMEOUT 2
#define WRITE_TIMEOUT 2

#define RIGHT 1100
#define LEFT 1101

#define SYS_ID_LEN 2 /* means 2 bytes for the system-ID */
#define CFG_ALPHA 1200
#define CFG_NUM		1201
#define CFG_ALPHANUM	1202
#define CFG_IGNORE	1203
#define CFG_PRINT	1204
#define CMD_LEN 2 			/* means 2 bytes for the length of a command */
#define ALIVE_LEN 4 		/* means 4 bytes for the length of alive-cmd */
#define ALIVE_ID "Uxxx"	/* string for alive telegram */
#define CEDA_DATE_LEN 14

#define MSG_STX 80 		/* ID for start of transmission of daily chute info */
#define MSG_ETX 81 		/* ID for end of transmission of daily chute info */
#define MSG_ALV 98 		/* ID for Alive Msg.*/
#define MSG_ACK 99 		/* ID for Acknowledge message */
#define MSG_CAI 56 		/* ID for Chute Allocation-Info message */
#define MSG_UFI 50 		/* ID for update flight info message */
#define MSG_CXX 51 		/* ID for cancelled flight info message */
#define MSG_FTF 53 		/* ID for flight table file information message */
#define MSG_FTT 54 		/* 20060803 JIM: PRF 8352: request for flight schedule table */
#define MSG_CST 55 		/* ID for Chute Status (open/close) message */
#define MSG_ALIVE 101 		/* ID for ALIVE-telegram form BHS */

#define SFS	102		/* ID for sending the flight schedule for 1.day to the BHS system */
#define SFU	103		/* ID for sending a flight update to the BHS system */
#define SFI	105		/* ID for sending a flight insert to the BHS system */
#define CXX	104		/* ID for sending a flight cancel to the BHS system */
#define DFR	106		/* ID for sending a flight deletion(cancel) to the BHS system */

/************************/
/* CFG-File - STRUCTURE */
/************************/
typedef struct 
{
	/* MAIN section in .cfg file*/    
	char *debug_level;		/* start debug-level*/
	char *mode;						/* type of running mode (REAL or TEST)*/
	char *db_write;				/* should recv. data be written to DB or not */
	char *seq_write_queue;/* dedicated queue for bhsif to guarantee correct order for inserts,updates into chatab */
	char *db_filter;			/* DB-filter condition */
	char *cxx_filter;			/* DB-cancel-filter condition */
	char *term_filter;			/* DB-filter condition for terminal check */
	char *term_list;			/* List of valid terminals */
	char *my_sys_id;			/* my own system ID 2 chars */
	char *bhs_host1;			/* IP-address or hostname of the 1. BHS-server */ 
	char *bhs_host2;			/* IP-address or hostname of the 2. BHS-server */ 
	char *service_port; 	/* service name for receiving data */
	char *wait_for_ack; 	/* wait-time in sec. for acknowledge */
	char *recv_timeout; 	/* wait-time in sec. for receiving a valid telegram */
	char *max_sends;			/* max. number of sends(resends) for a telegram */
	char *recv_log; 			/* log-file for receiving telegrams */
	char *send_log; 			/* log-file for sended telegrams */
	char *try_reconnect; 	/* try to connect every xy sec. */
	char *remote_sys_id; 	/* ID of the remote system */
	char *fs_offset; 			/* number of days the flight-schedule is sned in adv.*/
	char *fs_local_path; 	/* local path for flight-schedule files */
	char *fs_remote_path; /* remote path for flight-schedule files */
	char *ftp_client_os; 	/* operating system of the client */
	char *ftp_user; 			/* username for FTP-connection */
	char *ftp_pass; 			/* password for FTP-connection */
	char *ftp_timeout; 		/* */
	char *after_ftp; 			/* what to do after FTP-transmission */
	char *CAI_ShortLength;/* check for short external messages */
	char *ExtAlc2AddBlank;/* add Blank if ALC is 2 char long */
	char *EmptyChuteDeletes;/* delete chutes when blank received */
	char *BagAgentDefault;/* used when no Handling Agent for ALC3 exists */
	char *BagTaskCode;    /* Code and name of Baggage Task are not fix in DB, so */
	char BagTaskName[32]; /* they have to be configured for retrieving Handling Agent */
        int ilUseHTAForBagAgent; /* Use HTATAB to get Handling Agent, if TRUE */
        int ilReceiveTransitInfo; /* Receive "transit" info in telegram types 55 and 56, if TRUE */
}CFG;

typedef struct
{
	int ilMsgNr;
	char *msg;
}MSG_RECV;

extern void    DeleteOldQueues(char *pcpQueue);
