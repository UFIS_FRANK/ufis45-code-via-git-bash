#ifndef _DEF_mks_version_ntisch_h
  #define _DEF_mks_version_ntisch_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_ntisch_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/ntisch.h 1.5 2008/08/14 17:22:44SGT bst Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.                          */
/*                                    */
/*  Program   :                               */
/*  Revision date :                           */
/*  Author        :                           */
/*                                        */
/*  NOTE : This should be the only include file for your program          */
/* ********************************************************************** */
#ifndef _NTISCH_H
#define _NTISCH_H


#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <memory.h>

#include "ugccsma.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "msgno.h"
#include "helpful.h"
#include "timdef.h"
#include "tools.h"
#include "debugrec.h"
#include "new_catch.h"
#include "hsbsub.h"

extern char *strdup(const char*);

/* all possible days */
#define iSUN                    0x0001
#define iMON                    0x0002
#define iTUE                    0x0004
#define iWED                    0x0008
#define iTHU                    0x0010  
#define iFRI                    0x0020
#define iSAT                    0x0040

#define iMAX_DAYS           366
#define iMAX_HOURS          24
#define iMAX_MINUTES        60
#define iMAX_SECONDS        60

#define iREL                    0
#define iABS                    1
#define iDURATION           2
#define iDATE                   3
#define iFOREVER                4

#define  TYPE(a) ((a) == iREL ? "RELATIV" : ((a) == iABS ? "ABSOLUT" : ((a) == iDURATION ? "DURATION" : ((a) == iDATE ? "DATE" : "FOREVER"))))

#define iDELETE             0
#define iADD                    1
#define iUPDATE             2           
#define iUPD_BUF                3
#define iHSBADD             4
#define iDEL_MODID          5
#define iCHECK_TIMER        6

#define FUNCTION(a) ((a) == iDELETE ? "DELETE" : ((a) == iADD ? "ADD" : ((a) == iUPDATE ? "UPDATE" : ((a) == iUPD_BUF ? "UPDATE BUFFER" : ((a) == iHSBADD ? "HSBADD" : ((a) == iDEL_MODID ? "DEL-MODID" : "CHECK_TIMER"))))))

#define CTRL_STA(a) ((a) == HSB_STANDBY ? "STANDBY" : ((a) == HSB_COMING_UP ? "COMING UP" : ((a) == HSB_ACTIVE ? "ACTIVE" : ((a) == HSB_STANDALONE ? "STANDALONE" : ((a) == HSB_ACT_TO_SBY ? "ACTIVE TO STANDBY" : "OTHER STATUS")))))

#define iDEFAULT_STIME      1
#define sDEFAULT_FNAME      "ntisch.dat"
#define sDEFAULT_FTMP       "ntisch.tmp"

#define iHANDLE_DATA        0
#define iDELETE_TIMER       1
#define     iUPDATE_TIMER       2
#define iUPDBUF_TIMER       3
#define iDELMODID_TIMER 4
#define iDELETE_BIN_FILE    1000
#define iPRINT_NEXT_EVENT   2000
#define iDUMP_BIN_FILE      3000

#define iSECTION_OFFSET 100
#define iTERMINATE          -100

typedef struct _absdate
{
    int         iDayOfMonth;
    int         iMonthOfYear;
} AbsDate;

typedef struct _fileheader
{
    long            lLength;        /* of all records */
    int         iNumber;        /* of all records */
    int         iFId;           /* ID of next record */
} FileHeader;

typedef struct _recordheader
{
    int         iRecordID;
  int     iRecordEventID;
        int     lRecordLen;
} RecordHeader;

typedef struct _data_period
{
    PERIOD      rPeriod;
    long            lDataLen;
    char            data[1];
} DATA_PERIOD;

typedef struct _function
{
    int         iFunction;
    int         iEventID;
    int         iParentID;
    int         iDestinationID;
    struct tm   rStartTime;
    char            data[1];
} FUNCTION;

typedef struct _tntsection
{
  time_t tExecutedTime;       /* Mei */
  int   iSecInterval;		  /* Mei */
  char  pcHomeAirport[iMIN];
  char  pcTableExtension[iMIN];
  int   iFunction;
  int   iEventID;
  int   iFID;
  int   iValid;
  int   iParentID;
  int   iDestinationID;
  int   iEventCommand;          
  int	iQuePrio;	/* BST */
  UINT  iNoOfAbsDate;
  AbsDate   rAbsDate[iMAX_DAYS];
  UINT  iNoOfDOW;
  UINT  iDaysOfWeek;
  UINT  iNoOfHOD;
  UINT  piHoursOfDay[iMAX_HOURS];
  UINT  piHourIsSent[iMAX_HOURS];
  UINT  piHourCounter[iMAX_HOURS];
  UINT  iNoOfMOH;
  UINT  piMinOfHour[iMAX_MINUTES];
  UINT  piMinIsSent[iMAX_MINUTES];
  UINT  piMinCounter[iMAX_MINUTES];
  UINT  iNTime;
  UINT  iType;  
  int   iUseInternalEvent;  /* should i send event structure */
  UINT  iDataLen;
  char  *pcData;        /* specific user data */
  char	*pcMultiMsg;	/* BST */
  struct tm rNextHours[iMAX_HOURS];     /* only used if type = rel */
  struct tm rNextMinutes[iMAX_MINUTES]; /* only used if type = rel */
  struct tm rStartTime;         /* only used if type = rel */
} TNTSection;

typedef struct _tntmain
{
    UINT            iSleepTime;
    UINT            iNoOfSections;
    TNTSection  *prSection;
} TNTMain;

/* pointer to function */
typedef int (*TimerHandler)(FUNCTION *, int);

#endif  /* _NTISCH_H */

