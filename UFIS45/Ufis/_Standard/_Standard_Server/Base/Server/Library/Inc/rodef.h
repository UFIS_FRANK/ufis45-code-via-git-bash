#ifndef _DEF_mks_version_rodef_h
  #define _DEF_mks_version_rodef_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_rodef_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/rodef.h 1.2 2004/07/27 16:48:21SGT jim Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************
*                                                                           *
*	R E C O R D  S T R U C T U R E  F O R  R O T A B                    *
*                                                                           *
*****************************************************************************/

#ifndef __RODEF_INC
#define __RODEF_INC


struct _rorec
	{
	short route;		/* route id */
	short queues[15];	/* queue id */
        } ;
	
typedef struct _rorec ROREC;	

#endif

/****************************************************************************/
/****************************************************************************/
