#ifndef _DEF_mks_version_AATArray_h
  #define _DEF_mks_version_AATArray_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_AATArray_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/AATArray.h 1.10 2004/07/27 16:46:40SGT jim Exp  $";
#endif /* _DEF_mks_version */

/* 20040629 JIM: added CEDAArrayFillHint and CEDAArrayRefillHint to allow use of */
/*               Oracle DML Hints                                                */

#ifndef AATARRAY_H 
#define AATARRAY_H


#include <stdio.h>

#ifndef BOOL
#define BOOL short
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif


#ifndef HANDLE
#define HANDLE short
#endif

/* 
  Some parts in array structures and functions have fixed length.
  To beware of overflows, check if these values are enough.
  Both DATA Length are for full length => full VarChars!
  The numbers could be as large as needed, because variables in 
  array library are long! (Ausnahme: GetNextDataItem).
*/

#define ARR_ASC               1    /* Keys sorted from small (first) to large */
#define ARR_DESC             -1    /* Keys sorted from large (first) to small */

#define ARR_MAXSELECTLEN     (50*1024)   /* maximum for selectionstring  */
#define ARR_MAXFLDLSTLEN     4096  /* maximum of kommaseperated fieldlist.
                                      max 250 Fields * 5 => 2k */
#define ARR_MAXSQLFLDLSTLEN  8192  /* maximum of fieldlist for an sql call.
                                      e.g.: update: (4+V+4+3) * 250  => 4k */

/* for filtering */

#define ARR_NOOP             1000     
#define ARR_EQUAL            1001
#define ARR_NOTEQUAL         1002
#define ARR_SMALLER          1003
#define ARR_SMALLEREQUAL     1004
#define ARR_LARGER           1005
#define ARR_LARGEREQUAL      1006
#define ARR_OR               1007
#define ARR_AND              1008

#define ARR_MAXFILTERLEN     200

/* end for filtering */


/* Error Codings */

#define FIRSTERROR -21
#define UNKNOWN_ERROR 20

#ifndef RC_SUCCESS
#define RC_SUCCESS 0
#endif
#ifndef RC_NOTFOUND
#define RC_NOTFOUND 1
#endif
#ifndef RC_FAILURE
#define RC_FAILURE -1
#endif
#ifndef RC_OP_FALSE
#define RC_OP_FALSE -1
#endif

#ifndef RC_INVALID
#define RC_INVALID FIRSTERROR + -0
#endif
#ifndef RC_USED
#define RC_USED FIRSTERROR + -1
#endif
#ifndef RC_DELETED
#define RC_DELETED FIRSTERROR + -2
#endif
#ifndef RC_DUPLICATE
#define RC_DUPLICATE FIRSTERROR + -3
#endif
#ifndef RC_NOMEM
#define RC_NOMEM FIRSTERROR + -4
#endif
#ifndef RC_NOTINITIALIZED
#define RC_NOTINITIALIZED FIRSTERROR + -5
#endif
#ifndef RC_NODATA
#define RC_NODATA FIRSTERROR + -6
#endif
#ifndef RC_ROWDELETED
#define RC_ROWDELETED FIRSTERROR + -7
#endif
#ifndef RC_WRONGDATA 
#define RC_WRONGDATA  FIRSTERROR + -8
#endif

enum enIdxType
{
  ARR_IDX_BSEARCH,
  ARR_IDX_HASH,
  ARR_IDX_BTREE
};

enum enKeyType 
{
  ARR_LKEY_INT,
  ARR_LKEY_LONG,
  ARR_LKEY_DOUBLE,
  ARR_LKEY_DATE,
  ARR_LKEY_CHAR,
  ARR_LKEY_ITEM
};

enum enChangeFlag 
{
  ARR_DATA_EMPTY,
  ARR_DATA_UNCHANGED,
  ARR_DATA_CHANGED,
  ARR_DATA_NEW,
  ARR_DATA_DELETED,
  ARR_DATANEW_DELETED
};

enum enRowType 
{
  ARR_FIRST=-30,
  ARR_CURRENT,
  ARR_NEXT,
  ARR_LAST,
  ARR_PREV,
  ARR_FIND
};

enum enCommitType
{ 
  ARR_COMMIT_NONE_IF_ERR,       /* if error on one row nothing is commited   */
  ARR_COMMIT_NONE,              /* no commit at all (at end of all rollback) */
  ARR_COMMIT_ALL_OK,            /* commit all actions that were ok           */
  ARR_COMMIT_TILL_ERR,          /* commit all actions till first error       */
  ARR_COMMIT_USER               /* no commit and no rollback, so user 
                                   can commit after all his changes          */
};

struct _AATParamDescriptor
{
  EVT_INFO   OutEvent;   /* keeps all members of 'Event' SBC   */
  int        AppendData; /* true/false indicator               */
  int        ListAlloc;  /* amount of allocated lists in array */
  int        ListCount;  /* number of used lists (up from 0)   */
  LIST_DESC *DataList;   /* array of currently handled lists   */
};
typedef struct _AATParamDescriptor AAT_PARAM;


/*****************************
     Array Functions
******************************/
int AATArrayInitialize(long lpArrayCount,long lpIndexCount);
int AATArrayCreate(HANDLE *pspHandle, char *pcpArrayName,
                   BOOL bpFixLen,long lpInitialCount,
                   long lpFieldCount, long *plpFieldLen,
                   char *pcpFieldList, char *pcpFieldTypes);

int AATArrayDestroy(HANDLE *pspArray,char *pcpArrayName);

int AATArrayPutRow(HANDLE *pspArray,char *pcpArrayName,
                   long *plpRowNum,void *pvpDataNew);
int AATArrayAddRow(HANDLE *pspArray,char *pcpArrayName,
                   long *plpRowNum,void *pvpDataOrig);
int AATArrayAddRowPart(HANDLE *pspArray,char *pcpArrayName,
                       long *plpRowNum,char *pcpFldLst, void *pcpDataOrig);
int AATArrayGetRow(HANDLE *pspArray,char *pcpArrayName,
                   long lpRowNum, char cpDelimiter,
                   long lpMaxBytes,void *pvpDataBuf);
int AATArrayGetRowPointer(HANDLE *pspArray,char *pcpArrayName,
                          long lpRowNum,void **pvpDataBuf);
int AATArrayDeleteRow(HANDLE *pspArray,char *pcpArrayName,
                      long lpRowNum);

int AATArrayPutField(HANDLE *pspArray,char *pcpArrayName,
                     long *plpFieldNum,char *pcpFieldName,
                     long lpRowNum, BOOL spNewDat, void *pvpData);
int AATArrayGetField(HANDLE *pspArray,char *pcpArrayName,
                     long *plpFieldNum,char *pcpFieldName,
                     long lpMaxBytes, long lpRowNum,void *pvpDataBuf);
int AATArrayGetFields(HANDLE *pspArray,char *pcpArrayName,
                      long *plpFieldNumbers,char *pcpFieldList,
		                  char cpDelimiter,long lpMaxBytes, long lpRowNum,
                      void *pvpDataBuf, BOOL bpAllFields);
int AATArrayGetFieldPointer(HANDLE *pspArray,char *pcpArrayName,
                            long *plpFieldNum,char *pcpFieldName,
                            long lpRowNum,void **pvpDataBuf);

int AATArrayCreateSimpleIndex(HANDLE *pspArray,char *pcpArrayName,
		                          HANDLE *pspIndexHandle,char *pcpIndexName,
                              char *pcpKeyFldList, 
                              long lpDirection,BOOL bpMultiFlag);
int AATArrayDestroyIndex(HANDLE *pspArray,char *pcpArrayName,
                         HANDLE *pspIndex,char *pcpIndexName);
int AATArrayActivateIndex(HANDLE *pspArray,char *pcpArrayName,
                          HANDLE *pspIndex,char *pcpIndexName,
                          BOOL bpActive);
int AATArrayFindKey(HANDLE *pspArray,char *pcpArrayName,
                    HANDLE *pspIndex,char *pcpIndexName,
                    long *plpIndex,long lpMaxBytes,void *pvpKey);
int AATArrayFindRowPointer(HANDLE *pspArray,char *pcpArrayName,
                           HANDLE *pspIndex, char *pcpIndexName,
                           char *pcpPattern,long *plpIndex, void **pvpDataBuf);

int AATArrayDeleteRowsByIndexValue(HANDLE *pspArray,char *pcpArrayName,
                           HANDLE *pspIndex, char *pcpIndexName,
                           char *pcpPattern, BOOL bpReorg);

int AATArrayReorgIndexes(HANDLE *pspArray,char *pcpArrayName, BOOL bpReorg);

char *AATArrayGetErrorMsg(short spErrno);


/***************************
      CEDA Array Functions
****************************/

/* ****************************************************************
 * Function:    CEDAArrayInitialize
 * Parameter:   IN    : lpArrayCount    Number of arrays to store
 *                      lpIndexCount    Number of index for one array
 * Description: This function allocates memory for lpArrayCount
 *              Arrays and lpIndexCount Indexes per Array.
 *              If more needed realloc is done with these values
 *              as stepsize.
 ***************************************************************** */
int CEDAArrayInitialize (long lpArrayCount,long lpIndexCount);


/* ****************************************************************
 * Function:    CEDAArrayCreateInitCount
 * Parameter:   OUT   : pspHandle       Handle for array   
 *              IN    : pcpArrayName    name for the array
 *                      pcpTabNam       name of table
 *                      pcpSelect       selection string
 *                      pcpAddFldLst    user defined field list
 *                      plpAddFldLens   length of user defined fields
 *              IN/OUT: pcpFldLst       field list sep by komma
 *                      plpFldLens      length of fields in list
 *                      plpFldOffsets   offsets to each field
 *              IN    : lpInitialNumRecords   first alloc of records
 *                                            (if more realloc is done)
 * Description: This function creates an array for a list of fields.
 *              The funtion returns an arraystructure and a handle
 *              for handling the array.
 *              If fieldlist is "", the function looks into SYSTAB
 *              and gets all fields and length for defined table.
 *              The length of field is always get from SYSTAB.
 *              For User defined fields use AddFieldslist for fields
 *              and AddFlieldsLen for field length.
 *              In lpInitialNumRecords the number of rows to allocate
 *              is set. If more needed realloc is done with this value
 *              as stepsize.
 *              WARNING: In case of variable pcpFldLst is "" the fieldlist from 
 *                       SYSTAB will be written to it, so make it big enough,
 *                       or take NULL.
 ***************************************************************** */
int CEDAArrayCreateInitCount (HANDLE    *pspHandle,
                              char     *pcpArrayName,
                              char     *pcpTabNam,
                              char     *pcpSelect,
                              char     *pcpAddFldLst,
                              long     *plpAddFldLens,
                              char     *pcpFldLst,
                              long     *plpFldLens,
                              long     *plpFldOffsets,
                              long     lpInitialNumRecords);

/* ****************************************************************
 * Function:    CEDAArrayCreate
 * Parameter:   OUT   : pspHandle       Handle for array
 *              IN    : pcpArrayName    name for the array
 *                      pcpTabNam       name of table
 *                      pcpSelect       selection string
 *                      pcpAddFldLst    user defined field list
 *                      plpAddFldLens   length of user defined fields
 *              IN/OUT: pcpFldLst       field list sep by komma
 *                      plpFldLens      length of fields in list
 *                      plpFldOffsets   offsets to each field 
 * Description: This function creates an array for a list of fields.
 *              The funtion returns an arraystructure and a handle
 *              for handling the array.
 *              If fieldlist is "", the function looks into SYSTAB
 *              and gets all fields and length for defined table.
 *              The length of field is always get from SYSTAB.
 *              For User defined fields use AddFieldslist for fields
 *              and AddFlieldsLen for field length.
 *              The default size of array is 1000 rows.
 *              If more needed realloc is done with value 1000
 *              as stepsize.
 *              WARNING: In case of variable pcpFldLst is "" the fieldlist from 
 *                       SYSTAB will be written to it, so make it big enough 
 *                       or take NULL.
 ***************************************************************** */
int CEDAArrayCreate (HANDLE    *pspHandle,
                      char      *pcpArrayName,
                      char      *pcpTabNam,
                      char      *pcpSelect,
                      char      *pcpAddFldLst,
                      long      *plpAddFldLens,
                      char      *pcpFldLst,
                      long      *plpFldLens,
                      long      *plpFldOffsets);

/* ****************************************************************
 * Function:    CEDAArrayFillHint
 * Parameter:   IN    : pspHandle       Handle for array 
 *                      pcpArrayName    Name for the array
 *                      pcpAddFldData   user defined data list sep by komma
 *                      pcpHint         Oracle HINT, i.e 
 *                                      /#+ NOINDEX(JOBTAB_UJTY) #/   <== *)
 * Description: This function fills an array with data of one
 *              database table. Selection is possible.
 *              Fields and Selection are read from CEDAARRAY created
 *              by CEDAArrayCreate Call. The Example 
 *                /#+ NOINDEX(oracle index name) #/                   <== *)
 *              tells Oracle, not to use index <oracle index name>
 *              To use a special index, use 
 *                /#+ INDEX(oracle index name) #/                     <== *)
 *   <== *) replace all '#' by '*', can not write '/<star>' in c-code
 ***************************************************************** */
int CEDAArrayFillHint (HANDLE * pspHandle, 
                       char *pcpArrayName, 
                       char *pcpAddFldData, 
                       char *pcpHint);

/* ****************************************************************
 * Function:    CEDAArrayFill
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pcpAddFldData   user defined data list sep by komma
 * Description: This function fills an array with data of one
 *              database table. Selection is possible.
 *              Fields and Selection are readed from CEDAARRAY created
 *              by CEDAArrayCreate Call.
 * !!! This function is used for compatibility. Use CEDAArrayRefillHint !!!
 ***************************************************************** */
int CEDAArrayFill (HANDLE  *pspHandle,
                    char    *pcpArrayName,
                    char    *pcpAddFldData);

/* ****************************************************************
 * Function:    CEDAArrayRefillHint
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pcpSelect       New selection string
 *                      pcpAddFldData   user defined data list sep by komma, NOT USED!
 *                      lpRow           ARR_FIRST/ARR_NEXT => Delete/NoDelete
 *                      pcpHint         Oracle HINT, i.e 
 *                                      /#+ NOINDEX(JOBTAB_UJTY) #/   <== *)
 * Description: This function refills an array with data of one
 *              database table. Only a new Selection is possible.
 *              Fields and Table must be the same as in CEDAArrayCreate Call.
 *              The fieldlist and the length of fields didn't change.        
 *              Two possibilities: Function first deletes array and then
 *                                 fills array. (ARR_FIRST)
 *                                 Function adds new rows to array(ARR_NEXT)
 *              The Example 
 *                /#+ NOINDEX(oracle index name) #/                   <== *)
 *              tells Oracle, not to use index <oracle index name>
 *              To use a special index, use 
 *                /#+ INDEX(oracle index name) #/                     <== *)
 *   <== *) replace all '#' by '*', can not write '/<star>' in c-code
 ***************************************************************** */
int CEDAArrayRefillHint (HANDLE * pspHandle, 
                         char *pcpArrayName, 
                         char *pcpSelect, 
                         char *pcpAddFldData, 
                         long lpRow, 
                         char *pcpHint);

/* ****************************************************************
 * Function:    CEDAArrayRefill
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pcpSelect       New selection string
 *                      pcpAddFldData   user defined data list sep by komma
 *                      lpRow           ARR_START/ARR_NEXT => Delete/NoDelete
 * Description: This function refills an array with data of one
 *              database table. Only a new Selection is possible.
 *              Fields and Table must be the same as in CEDAArrayCreate Call.
 *              The fieldlist and the length of fields didn't change.
 *              Two possibilities: Function first deletes array and then
 *                                 fills array. (ARR_START)
 *                                 Function adds new rows to array(ARR_NEXT)
 * !!! This function is used for compatibility. Use CEDAArrayRefillHint !!!
 ***************************************************************** */
int CEDAArrayRefill (HANDLE  *pspHandle,
                     char   *pcpArrayName,
                     char   *pcpSelect,
                     char   *pcpAddFldData,
                     long   lpRow);

/* ****************************************************************
 * Function:    CEDAArrayDelete
 * Parameter:   IN    : pspHandle    Handle of the array
 *                      pcpArrayName Name of the array
 * Description: This function deletes one array without destroy.
 ***************************************************************** */
int CEDAArrayDelete (HANDLE *pspHandle, char *pcpArrayName);

/* ****************************************************************
 * Function:    CEDAArrayDestroy
 * Parameter:   IN    : pspHandle    Handle of the array
 *                      pcpArrayName Name of the array
 * Description: This function frees memory for one array.
 ***************************************************************** */
int CEDAArrayDestroy (HANDLE *pspHandle, char *pcpArrayName);


/* ****************************************************************
 * Function:    CEDAArrayCreateSimpleIndexUp
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pcpIndexNam     Name of Index
 *                      pcpKeyFldList   List of Keys
 *              OUT   : pspIndexHandle  Handle for Index
 * Description: This function creates an index for an array
 *              sorted from small (first) to large in all items
 *              of keyfields.
 ***************************************************************** */
int CEDAArrayCreateSimpleIndexUp (HANDLE *pspHandle,
                                  char   *pcpArrayName,
                                  HANDLE *pspIndexHandle,
                                  char   *pcpIndexNam,
                                  char   *pcpKeyFldList);

/* ****************************************************************
 * Function:    CEDAArrayCreateSimpleIndexDown
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pcpIndexNam     Name of Index
 *                      pcpKeyFldList   List of Keys
 *              OUT   : pspIndexHandle  Handle for Index
 * Description: This function creates an index for an array
 *              sorted from large (first) to small in all items
 *              of keyfields.
 ***************************************************************** */
int CEDAArrayCreateSimpleIndexDown (HANDLE *pspHandle,
                                    char   *pcpArrayName,
                                    HANDLE *pspIndexHandle,
                                    char   *pcpIndexNam,
                                    char   *pcpKeyFldList);

/* ****************************************************************
 * Function:    CEDAArrayCreateMultiIndex
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pcpIndexNam     Name of Index
 *                      pcpKeyFldList   List of Keys
 *                      plpDirection    Array of directions (1 Up, -1 Down)
 *                                      (Must be one direction for each field)
 *              OUT   : pspIndexHandle  Handle for Index
 * Description: This function creates an index for an array.
 *              For each item of keyfields the sort can be selected
 *              in plpDirection (ARR_ASC, ARR_DESC).
 ***************************************************************** */   
int CEDAArrayCreateMultiIndex (HANDLE *pspHandle,
                               char   *pcpArrayName,
                               HANDLE *pspIndexHandle,
                               char   *pcpIndexNam,
                               char   *pcpKeyFldList,
                               long   *plpDirection);

/* ****************************************************************
 * Function:    CEDAArrayActivateIndex
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pspIndexHdl     Handle for Index
 *                      pcpIndexNam     Name of Index
 * Description: This function activates an index for
 *              defined an array. So on every delete, update, or add row
 *              the index will be refreshed. For multiple actions
 *              first disactivate and than activate index. 
 ***************************************************************** */
int CEDAArrayActivateIndex(HANDLE *pspHandle,
                           char   *pcpArrayName,
                           HANDLE *pspIndexHdl,
                           char   *pcpIndexNam);

/* ****************************************************************
 * Function:    CEDAArrayDisactivateIndex
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pspIndexHdl     Handle for Index
 *                      pcpIndexNam     Name of Index
 * Description: This function disactivates an index for
 *              defined an array. So no refresh on index on every delete,
 *              update, or add row till activate is called.
 *              For multiple actions first disactivate and
 *              and than activate index.
 ***************************************************************** */
int CEDAArrayDisactivateIndex (HANDLE *pspHandle,
                               char   *pcpArrayName,
                               HANDLE *pspIndexHdl,
                               char   *pcpIndexNam);

/* ****************************************************************
 * Function:    CEDAArrayDestroyIndex
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pspIndex        Handle for Index
 *                      pcpIndexNam     Name of Index
 * Description: This function destroys an index for defined array and
 *              frees its memory.
 ***************************************************************** */
int CEDAArrayDestroyIndex(HANDLE *pspArray,
                          char *pcpArrayName,
                          HANDLE *pspIndex,
                          char *pcpIndexName);

/* ****************************************************************
 * Function:    CEDAArrayGetRow
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      lpRowNum        Row number (ARR_FIRST, ARR_CURRENT,..)
 *                      cpDelimiter     Delimiter for data returned
 *                                      (NULL = none)
 *                      lpMaxBytes      maximum size of result data
 *                                      (incl. Terminator and Null Delimiters)  
 *              OUT   : pvpData         Result Data
 * Description: This function gets data of one row in defined
 *              array. In cpDelimiter the delimiter for rows data
 *              could be named. 
 ***************************************************************** */
int CEDAArrayGetRow (HANDLE *pspHandle, char *pcpArrayName,
                     long lpRowNum, char cpDelimiter,
                     long lpMaxBytes, void *pvpData);

/* ****************************************************************
 * Function:    CEDAArrayGetRowPointer
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      lpRowNum        Row number (ARR_FIRST, ARR_CURRENT,..)
 *              OUT   : pvpData         Result Data
 * Description: This function gets pointer to data of one row in defined
 *              array.
 *              WARNING: No changes to pointerdata allowed!
 ***************************************************************** */
int CEDAArrayGetRowPointer (HANDLE *pspHandle, char *pcpArrayName,
                            long lpRowNum, void **pvpData);

/* ****************************************************************
 * Function:    CEDAArrayPutRow
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      plpRowNum       Row number (ARR_FIRST, ARR_CURRENT,..)
 *                      pvpDataNew      Input Data (komma seperated)
 * Description: This function changes data of one row in defined
 *              array (all fields of array must exist in pvpDataNew).
 *              Row must exist.
 ***************************************************************** */
int CEDAArrayPutRow (HANDLE *pspHandle, char *pcpArrayName,
                     long *plpRowNum,
                     void *pvpDataNew);

/* ****************************************************************
 * Function:    CEDAArrayAddRow
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      plpRowNum       Row number (ARR_FIRST, ARR_CURRENT,..)
 *                      pvpDataOrig     Input Data ('\0' seperated)
 * Description: This function adds one new row with data to array.
 *              Row must be new.
 *              The data in pvpDataOrig must be '\0' seperated.
 ***************************************************************** */
int CEDAArrayAddRow (HANDLE *pspHandle, char *pcpArrayName,
                     long *plpRowNum, void *pvpDataOrig);

/* ****************************************************************
 * Function:    CEDAArrayAddRowPart
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      plpRowNum       Row number (ARR_FIRST, ARR_CURRENT,..)
 *                      pcpFldLst       Fieldnamelist that are added (,)
 *                      pcpDataOrig     Input Data (komma seperated!)
 * Description: This function adds one new row with data to array.
 *              The difference to CEDAArrayAddRow is that the data
 *              mustn't contain all fields of array. Not send fields
 *              will be filled with one blanc as default.
 *              (Later type dependent).
 ***************************************************************** */
int CEDAArrayAddRowPart (HANDLE *pspHandle, char *pcpArrayName,
                         long *plpRowNum, char *pcpFldLst, char *pcpDataOrig);

/* ****************************************************************
 * Function:    CEDAArrayDeleteRow
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      lpRowNum        Row number (ARR_FIRST, ARR_CURRENT,..)
 * Description: This function deletes one row from array.
***************************************************************** */
int CEDAArrayDeleteRow (HANDLE *pspHandle, char *pcpArrayName,
                        long lpRowNum);

/* ****************************************************************
 * Function:    CEDAArrayGetRowCount
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *              OUT   : plpRowCount     Number of valid rows in Array
 * Description: This function returns in last Parameter the number
 *              of valid rows in array described by Handle or name.
 ***************************************************************** */
int CEDAArrayGetRowCount (HANDLE *pspHandle, char *pcpArrayName,
                          long *plpRowCount);


/* ****************************************************************
 * Function:    CEDAArrayGetFields
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      plpFields       List of fieldlens
 *                      pcpFldLst       List of fields
 *                      cpDelimiter     Delimiter for result data (NULL = none)
 *                      lpMaxBytes      maximum size of result data
 *                                      (incl. Terminator and delimiters)  
 *                      lpRowNum        Row number (ARR_FIRST, ARR_CURRENT,..)
 *              OUT   : pvpData         Result Data
 * Description: This function gets a list of field data for defined
 *              row in array.
 ***************************************************************** */
int CEDAArrayGetFields (HANDLE *pspHandle, char *pcpArrayName,
                         long *plpFields, char *pcpFldLst,
                         char cpDelimiter, long lpMaxBytes, long lpRowNum,
                         void *pvpData);

/* ****************************************************************
 * Function:    CEDAArrayGetField
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      plpField        Field Pos in array
 *                      pcpFieldNam     Name of field
 *                      lpMaxBytes      maximum size of result data
 *                                      (incl. Terminator)  
 *                      lpRowNum        Row number (ARR_FIRST, ARR_CURRENT,..)
 *              OUT   : pvpData         Result Data
 * Description: This function gets data of one field in defined
 *              row in array.
 ***************************************************************** */
int CEDAArrayGetField (HANDLE *pspHandle, char *pcpArrayName,
                        long *plpFields, char *pcpFieldNam,
                        long lpMaxBytes, long lpRowNum,
                        void *pvpDataBuf);

/* ****************************************************************
 * Function:    CEDAArrayGetFieldPointer
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      plpFieldNum     Field Pos in array 
 *                      pcpFieldNam     Name of field
 *                      lpRowNum        Row number (ARR_FIRST, ARR_CURRENT,..)
 *              OUT   : pvpData         Result Data Pointer
 * Description: This function gets pointer to data of one field in defined
 *              row in array.
 ***************************************************************** */
int CEDAArrayGetFieldPointer (HANDLE *pspHandle, char *pcpArrayName,
                              long *plpFieldNum, char *pcpFieldNam,
                              long lpRowNum, void **pvpDataBuf);

/* ****************************************************************
 * Function:    CEDAArrayPutField
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      plpFieldNum     Field Pos in array
 *                      pcpFieldNam     Name of field
 *                      lpRowNum        Row number (ARR_FIRST, ARR_CURRENT,..)
 *              OUT   : pvpDataNew      Input Data
 * Description: This function puts data to one field in defined
 *              row in array.
 ***************************************************************** */
int CEDAArrayPutField (HANDLE *pspHandle, char *pcpArrayName,
                        long *plpFieldNum, char *pcpFieldNam,
                        long lpRowNum, void *pvpDataNew);

/* ****************************************************************
 * Function:    CEDAArrayPutFields
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      plpFieldNum     Fields Positions in array
 *                      plpRowNum       Row number (ARR_FIRST, ARR_CURRENT,..)
 *                      pcpFieldLst     List of Fields (komma seperated)
 *                      pvpDataNewLst   List of new Data (komma seperated)
 * Description: This function puts data to all fields in one row
 *              seperated by komma in pcpFieldLst.
 *              Data is from pvpDataNewLst.
 ***************************************************************** */
int CEDAArrayPutFields (HANDLE *pspHandle, char *pcpArrayName,
                        long *plpFieldNum, long *plpRowNum,
                        char *pcpFieldLst, void *pvpDataNewLst);
 
/* ****************************************************************
 * Function:    CEDAArrayFindKey
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pspIndex        Handle of Index
 *                      pcpIndexName    Name of index
 *              IN/OUT: plpIndex        RowNum Index (ARR_FIRST, ARR_LAST ...)
 *                                      or ARR_FIND for search
 *                                      Back: RowNum of array
 *                      lpMaxBytes      Max size of result data
 *                                      (incl. Terminator)  
 *              OUT/IN: pvpKey          Result Data (Key)
 * Description: This function returns data in Key of row.
 *              Also it returns Number of array Row to get record.
 *              With function ARR_FIND Key content could be searched.
 *              This content should be komma seperated data in key
 *              (Always from first till n keyitems!)
 ***************************************************************** */
int CEDAArrayFindKey (HANDLE *pspHandle, char *pcpArrayName,
                      HANDLE *pspIndex, char *pcpIndexName,
                      long *plpIndex, long lpMaxBytes,
                      void *pvpKey);

/* ****************************************************************
 * Function:   CEDAArrayFindRowPointer
 * Parameter:  IN     : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      pspIndex        Handle of Index
 *                      pcpIndexName    Name of index
 *                      pcpPattern      Search Pattern in Keys
 *             IN & OUT:plpIndex        (ARR_FIRST,..) back: Row in Array
 *             OUT     :pvpKey          Result Data (Key)
 * Description: This function returns data stored in array in one
 *              row. The row is found with index. The Row could be
 *              defined by an Pattern and ARR_FIRST or ARR_LAST,
 *              or just by ARR_FIRST, ARR_LAST, ARR_NEXT, ARR_PREV ...
 *              With pattern and FIRST/NEXT LAST/PREV all found records
 *              become returned. FIRST and Pattern is like FindKey
 *              with an Key content.
 ***************************************************************** */ 
int CEDAArrayFindRowPointer(HANDLE *pspArray,char *pcpArrayName,
                            HANDLE *pspIndex, char *pcpIndexName,
                            char *pcpPattern, long *plpIndex,void **pvpDataBuf);


/* ****************************************************************
 * Function:    CEDAArrayReadAllHeader
 * Parameter:   outp       Pointer to output (file, stdout)
 * Description: This function prints out header information
 *              from all existing arrays to stdout.
 *              (Name, Handle, Rows)
 ***************************************************************** */
int CEDAArrayReadAllHeader(FILE *outp);


/* ****************************************************************
 * Function:    CEDAArrayInfoOneArray
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *                      outp            Pointer to output (file, stdout)
 * Description: This function prints out header information
 *              from one existing arrays to stdout.
 *              (Name, Handle, NumberRows, Table, Selection, Fieldlist)
 ***************************************************************** */
int CEDAArrayInfoOneArray(HANDLE *pspHandle, char *pcpArrayName, FILE *outp);

/* ****************************************************************
 * Function:    CEDAArrayEventUpdate
 * Parameter:   IN    : prpEvent     Eventstructure  (pcgEvent from skeleton)
 * Return:      RC_SUCCESS if all ok
 * Description: This function updates all arrays with events data send 
 *              from ACTION for example. The commands "IRT,URT,DRT"  
 *              are valid and will update all arrays that are filled
 *              with table of event.
 *              Selections must content "URNO =" or URNO IN (...)
 *              to get right record in array.
 *              Multiple (newline) events are not possible yet.
 ***************************************************************** */
int CEDAArrayEventUpdate (EVENT *prpEvent);

/* ****************************************************************
 * Function:    CEDAArrayEventUpdate2
 * Parameter:   IN    : prpEvent     Eventstructure  (pcgEvent from skeleton)
 *              OUT   : pcpUrnoLst   The Urnos that are in Eventselection
 *                      pclTable     The Tablename from Event
 * Return:      RC_SUCCESS if all ok
 * Description: This function updates all arrays with events data send
 *              from ACTION for example. The commands "IRT,URT,DRT"
 *              are valid and will update all arrays that are filled
 *              with table of event.
 *              Selections must content "URNO =" or URNO IN (...)
 *              to get right record in array.
 *              Multiple (newline) events are not possible yet.
 ***************************************************************** */
int CEDAArrayEventUpdate2 (EVENT *prpEvent, char *pcpUrnoLst, char *pcpTable);

/* ****************************************************************
 * Function:    CEDAArrayWriteDB
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *              OUT:    pcpUpdSelLst    Komma and newline seperated list
 *                                      of selections for records
 *                      pcpUpdFldLst    Komma and newline seperated list
 *                                      of updated fields of records
 *                      pcpUpdDatLst    Komma and newline seperated list
 *                                      of fields update data
 *                      plpNumUrnos     Number of changed Urnos (no insert)
 * Description: This function calls the function that fills database 
 *              with data of defined array.
 *              For LOGHDL:
 *              When an update or delete is done, the URNO of  
 *              the record is written in one line
 *              of pcpUpdSelLst, the updated fields are written to
 *              the same line of pcpUpdFldLst and the new or deleted 
 *              data for these fields is written to same line in
 *              pcpUpdDatLst. 
 *              This is for LOGHDL messages and history of DB operations! 
 ***************************************************************** */
int CEDAArrayWriteDB (HANDLE *pspHandle, char *pcpArrayName,
                      char **pcpUpdSelLst, char **pcpUpdFldLst,
                      char **pcpUpdDatLst, long *plpNumUrnos,
                      long lpCommitFlag);

/* ****************************************************************
 * Function:    CEDAArrayWriteDBGetChanges
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array
 *              OUT:    pcpCmdLst       newline seperated list 
 *                                      of commands done to DBwritten records
 *                                      (IRT,URT OR DRT)
 *                      pcpUpdSelLst    Komma and newline seperated list
 *                                      of selections for records
 *                      pcpUpdFldLst    Komma and newline seperated list
 *                                      of updated fields of records
 *                      pcpUpdDatLst    Komma and newline seperated list
 *                                      of fields update data
 *                      plpNumUrnos     Number of changed Urnos (no insert)
 * Description: This function calls the function that fills database 
 *              with data of defined array.
 *              For BROADCASTS:
 *              The function has one more parameter than CEDAArrayWriteDB.
 *              For broadcasts the command must be send also.
 *              When an insert, update or delete is done, the command
 *              IRT, DRT or URT is written to one line in pcpCmdLst.
 *              The URNO of the new or deleted or changed record
 *              is written to the same line of pcpUpdSelLst,
 *              the updated fields are written to the same line of 
 *              pcpUpdFldLst and the new or deleted data for these fields
 *              is written to same line in pcpUpdDatLst.
 *              This is for BCHDL messages and history of DB operations! 
 ***************************************************************** */
int CEDAArrayWriteDBGetChanges (HANDLE *pspHandle, char *pcpArrayName,
                                char **pcpCmdLst,
                                char **pcpUpdSelLst, char **pcpUpdFldLst,
                                char **pcpUpdDatLst, long *plpNumUrnos,
                                long lpCommitFlag);

/* ****************************************************************
 * Function:    CEDAArrayDBUpdateArray
 * Parameter:   IN    : pspHandle       Handle for array
 *                      pcpArrayName    Name for the array 
 * Return:      RC_SUCCESS, RC_FAIL
 * Description: This function must be called after CEDAArrayWriteDB
 *              if the commit_flag was ARR_COMMIT_USER. Otherwise
 *              the array is already updated to current DB contents.
 *              If return != RC_SUCCESS destroy and fill or refill array.
 ***************************************************************** */
int CEDAArrayDBUpdateArray(HANDLE *pspHandle, char *pcpArrayName);


/* ****************************************************************
 * Function:    SwitchToWholeRowUpdate
 * Parameter:   IN    : bpSwitch    TRUE => Update on all fields called
 *                      bpSwitch    FALSE=> Update on changed fields called
 * Description: This function fills database with data of defined row.
 *              In some cases the update on all fields in table is faster
 *              than checking wether field has changed and updating only
 *              a part of table fields. The Updatecursor could be left open
 *              when update is always on whole row.
 ***************************************************************** */
void SwitchToWholeRowUpdate (int bpSwitch);

/* ****************************************************************
 * Function:    CEDAArraySetFilter
 * Parameter:   IN    : pspHandle     Handle of Array
 *                      pcpArrayName  Name of Array
 *                      pcpFilterStr  Filter string
 * Description: This function is to set a filter to the defined array.
 *              The filter is like the sql-statement in FillArray,
 *              but in a more primitive way for parsing. 
 *
 *              The pcpFilterStr can contain operands like:
 *              "==", "!=", "&&", "||", "<", "<=", ">", ">="
 *
 *              Each statement should be written in paranthesis.
 *              The whole pcpFilterStr should be also written in
 *              paranthesis. 
 *              Examples: "(TIFA > 19991214000000)"
 *                        "((TIFA > 19991214000000) && (TIFA < 19991214235900))"
 *                       "(((TIFA > 19991214000000) && (TIFA < 19991214235900))
 *                        ||
 *                        ((TIFD > 19991214000000) && (TIFD < 19991214235900)))"
 *
 *              Till now only first example is realized!!!!!!!!!!!!!
 *
 *                        
 ***************************************************************** */
int CEDAArraySetFilter(HANDLE *pspHandle, char *pcpArrayName,
                       char *pcpFilterStr);

/* ****************************************************************
 * Function:    CEDAArraySendChanges2BCHDL
 * Parameter:   IN    : pspHandle    Handle of the array
 *                      pcpArrayName Name of the array
 *                      bpFlag       Flag (TRUE/FALSE)
 * Description: This function is for setting Arrays sendstatus to
 *              process bchdl in CEDA. If bpFlag == TRUE all changes
 *              written to Database will be sended to bchdl.
 *              (FALSE means no send)
 ***************************************************************** */
int CEDAArraySendChanges2BCHDL(HANDLE *pspHandle, char *pcpArrayName,
                               BOOL bpFlag);

/* ****************************************************************
 * Function:    CEDAArraySendChanges2ACTION
 * Parameter:   IN    : pspHandle    Handle of the array
 *                      pcpArrayName Name of the array
 *                      bpFlag       Flag (TRUE/FALSE)
 * Description: This function is for setting Arrays sendstatus to
 *              process action in CEDA. If bpFlag == TRUE all changes
 *              written to Database will be sended to action.
 *              (FALSE means no send)
 ***************************************************************** */
int CEDAArraySendChanges2ACTION(HANDLE *pspHandle, char *pcpArrayName,
                                BOOL bpFlag);

/*********************************************************************/
/* ParseFilterStr                                                    */
/*********************************************************************/

int ParseFilterStr(HANDLE *pspHandle, char *pcpArrayName, char *pcpFields, char *pcpData);

/*********************************************************************/
/*                                                                   */
/* GetNextUrnoFromSelection:                                         */
/*         This function gets the next URNO from the WHERE Clause    */
/* IN:     pcpSel: WHERE Clause                                      */
/* IN/OUT: pcpNxt: pointer behind the end of next URNO (for next run)*/
/* RETURN:         pointer to a STATIC copy of the next URNO         */
/*                 The next call to GetNextUrnoFromSelection         */
/*                 uses the same STATIC buffer!                      */
/*                                                                   */
/* !!! pcpNxt must be a pointer initialized to NULL at first call !!!*/
/* !!! and should not be used by user                             !!!*/
/*                                                                   */
/* The Function GetNextUrnoFromSelection scans WHERE clauses like    */
/*  "WHERE URNO = 123"                                               */
/*  "WHERE URNO = '123'"                                             */
/*  "WHERE URNO IN (567,678,789)"                                    */
/*  "WHERE URNO IN ( '567' , '678','789' )"                          */
/*  "123"                                                            */
/*  "'234'"                                                          */
/* The Parameter pcpNxt is set to the end of the URNO just copied.   */
/* In case of "WHERE URNO IN ..." this is the place, where the next  */
/* URNO may start. When in the next call the Variable pcpNxt already */
/* is set, the Function searches from here. In case of "WHERE URNO ="*/
/* the the second search will result to NULL                         */
/*                                                                   */
/* Example:                                                          */
/* void TestWhereClause(char *pcpSel)                                */
/* {                                                                 */
/*   void *pcvNxt = NULL;                                            */
/*   char *pclU = NULL;                                              */
/*                                                                   */
/*   pcvNxt = NULL;    <-- here doubled, but don't forget!           */
/*   printf("TestWhereClause: START with Selection: <%s>\n",pcpSel); */
/*   while ((pclU=GetNextUrnoFromSelection(pcpSel,&pcvNxt))!= NULL)  */
/*   {                                                               */
/*     printf("Selection: <%s>, Urno: <%s>\n",pcpSel, pclUrno);      */
/*   }                                                               */
/* }                                                                 */
/* TestWhereClause("WHERE URNO IN ( '567' , '678','789' )");         */
/*********************************************************************/

void *GetNextUrnoFromSelection (char *pcpSel, void **pcpNxt);
/* Functions to handle events from action in array */
int InsertEventData (HANDLE * pspHandle, char *pcpArrayName, char *pcpFldLst, char *pcpDataLst, char *pcpUrno);
int UpdateEventData (HANDLE * pspHandle, char *pcpArrayName, char *pcpSelect, char *pcpFldLst, char *pcpDataLst,
                            char *pcpUrnoLst);
int DeleteEventData (HANDLE * pspHandle, char *pcpArrayName, char *pcpSelect, char *pcpUrnoLst);


/*********************************************************************/
/*********************************************************************/
int AATArraySaveChanges(HANDLE *pspHandle, char *pcpArrayName, AAT_PARAM **prpArgDesc, long lpCommitFlag);
int AATArrayCreateArgDesc(char *pcpArrayName, AAT_PARAM **prpArgDesc);
int AATArrayDestroyArgDesc(char *pcpArrayName, AAT_PARAM **prpArgDesc);
int AATArrayInitArgDesc(char *pcpArrayName, AAT_PARAM *prpArgDesc, int ipListCount);

int AATArraySetSbcInfo(char *pcpArrayName, AAT_PARAM *prpArgDesc,
                       char *pcpQue, char *pcpPri, char *pcpCmd, char *pcpTbl,
                       char *pcpWks, char *pcpUsr, char *pcpTws, char *pcpTwe,
                       char *pcpSel, char *pcpFld, char *pcpDat);
int AATArrayCreateSbc(char *pcpArrayName, AAT_PARAM *prpArgDesc,
                      int ipFormat, char *pcpLists, int ipSpoolEvt);



#endif
