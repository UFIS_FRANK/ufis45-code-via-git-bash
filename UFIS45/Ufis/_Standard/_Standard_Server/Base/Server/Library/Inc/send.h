#ifndef _DEF_mks_version_send_h
  #define _DEF_mks_version_send_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_send_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/send.h 1.2 2004/07/27 16:48:25SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*****************************************************************************/
/* Includefile  send.c                                                       */
/*****************************************************************************/

#ifndef _SEND_H
#define _SEND_H


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "glbdef.h"
#include "tools.h"
#include "buffer.h"

#ifndef _SEND_C
#define EXTERN extern
#else
#define EXTERN
#endif

/*  Prototypes */
EXTERN int SendCedaEvent   (int, int, char*, char*, char*, char *, char*, 
                            char*, char*, char*, char*, char*, int, int); 
EXTERN int GetCedaTimestamp(char*,int,long);
EXTERN int SendStatus      (char*,char*,char*,int,char*,char*,char*);

#endif 

