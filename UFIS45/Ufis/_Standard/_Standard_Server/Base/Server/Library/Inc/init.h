#ifndef _DEF_mks_version_init_h
  #define _DEF_mks_version_init_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_init_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/init.h 1.2 2004/07/27 16:47:54SGT jim Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************
 T H E   M A S T E R   I N C L U D E   F I L E 
 Program        : init     
 Creation date  : Thu Nov 07 15:45:31 GMT 1991 
 Revision date  :        
 -----------------------------------------------------------------------------
 (c) Computer Communication GmbH 
***************************************************************************/ 

#ifndef __INIT_INC
#define __INIT_INC


#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>

#include <sys/shm.h>
#include <glbdef.h>
#include <sptdef.h>
#include <sthdef.h>

#define FAILURE (char *) -1

#endif

/****************************************************************************/
/****************************************************************************/
