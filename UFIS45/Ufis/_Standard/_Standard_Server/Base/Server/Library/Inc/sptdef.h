#ifndef _DEF_mks_version_sptdef_h
  #define _DEF_mks_version_sptdef_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_sptdef_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/sptdef.h 1.2 2004/07/27 16:48:27SGT jim Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************/
/****************************************************************************/
#ifndef _SPTDEF_H
#define _SPTDEF_H


/*	Defines for SPTAB                                       */

/*	System components                                       */

#define CFICC  (1 << BIT00)       /* hot standby config         */
#define CFLOGA (1 << BIT01)       /* logging (actual)           */
#define CFILOG (1 << BIT02)       /* input logging              */
#define CFDDC  (1 << BIT03)       /* staff handler              */
#define CFMVC  (1 << BIT04)       /* macrofont                  */
#define CFPVC  (1 << BIT05)       /* public video               */
#define CFVID  (1 << BIT06)       /* video                      */
#define CFSFC  (1 << BIT07)       /* split-flap                 */

#define MAXTB  76           /* maximum table #                  */

struct _sptrec              /* system parameter table           */
     {
     struct _sptfsh
          {
          unsigned long rt1sz; /* rec size of FS-memory 1          */
          unsigned long rt2sz; /* rec size of FS-memory 2          */
          unsigned long rt1p;  /* percentage of FS-memory 1        */
          unsigned long memsz; /* total FS-memory                  */
          } sptfsh;
     char pmsgdev[20];         /* primary mess. device name        */
     char smsgdev[20];         /* secondary mess. dev. name        */
     long devindic;            /* message device indicator         */
     unsigned long debug;      /* debug switches                   */
     unsigned long config;     /* system components                */
     unsigned long active;     /* active system components         */
     int tabsiz[MAXTB];        /* table sizes                      */
     int mrecno[MAXTB];        /* max. record # of a record table  */
     int recsiz[MAXTB];        /* record size of a record table    */
     int maxtab;               /* max. table #                     */
     long size;                /* size of 'systab' shared memory   */
     };

typedef struct _sptrec SPTREC;  

/* _SPTDEF_H */
#endif

/****************************************************************************/
/****************************************************************************/
