#ifndef _DEF_mks_version_tsmhdl_h
  #define _DEF_mks_version_tsmhdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_tsmhdl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/tsmhdl.h 1.2 2004/07/27 17:04:44SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.                                              */
/*                                                                        */
/*  Program       : tsm.c                                                 */	
/*                                                                        */
/*  Revision date : 28.6.01                                               */
/*                                                                        */
/*  Author    	  : GRM                                                   */
/*  some information setttings used from queprim.c	(library, src)		  */
/*                                       tsmhdl.c	(serverprog, src)     */
/*                                       tmihdl.c	(serverprog, src)	  */
/*                                  and  sendmsg.c  (tools, src)		  */
/*                                                                        */
/*                                                                        */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */

# ifndef _TSM_H_
# define _TSM_H_

#define STH_USE
#define U_MAIN

#define UGCCS_PRG

#include <sys/types.h>
#include <sys/signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

#include <ugccsma.h>
#include <glbdef.h>
#include <quedef.h>
#include <uevent.h>
#include <msgno.h>
/* #include <hsb.h> */
#include <chkdef.h>
#include <hsbsub.h>
#include <tools.h>
#include <tcpudp.h>
#include <debugrec.h>
#include <cli.h>

#include <ntisch.h>
#include <syslib.h>

# define TSMACTFILENAME "tsm.active"    /* TSM semaphore filename */
# define TSMQUEFILENAME "tsm.queue"     /* TSM active test queues filename */
# define TSMEVTFILENAME "tsm.event"     /* TSM active event to file filename */

# define TSMACTIONOFFSET 100000

/*
 * structure defines config param name-value pairs 
 */
typedef struct {
	char		*section ;
	char		*name ;
	short		type ;
	union {
		char	*ptr ;
		int	num ;
	} value ;
} t_cfgparam ;

typedef struct {
	int	source;
	int	destination;
} t_quemod;

/* from old evthdl.h roro */
struct t_config{

	FILE		*prFile;
	char		*pcFilename;/*read this from config and check if %d is in string!*/
	int			iCount;
	int			iRecords;
	int			iCloseCount;
};

typedef struct t_config CONF;

#define FIELDDELIMITER	'\x01'		/* SOH */
#define RECORDSTART		'\x02'		/* STX */
#define RECORDEND		'\x03'		/* ETX */

# endif /* _TSM_H_ */
