#ifndef _DEF_mks_version_debugrec_h
  #define _DEF_mks_version_debugrec_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_debugrec_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/debugrec.h 1.6 2006/02/21 21:57:23SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ******************************************************************** *
 * 20041103 JIM: added DebugPrintEventLine and DebugPrintItemLine for compressed
 *               debug of item and event 
 * 20041117 JIM: added cpCaller: string of calling routine in
 *                DebugPrintEventLine and DebugPrintItemLine
 * 20050313 JIM: added DebugPrintItemEvent
 * 20060221 JIM: moved SwitchDebugFile and SwitchDebugFileExt from debugrec.c to 
 *               dbg.c, ignoring any parameters to avoids log file mess in i.e SQLHDL
 * ******************************************************************** 
*/

#ifndef __DEBUGREC_H
#define __DEBUGREC_H


#include "glbdef.h"                                        
#include "quedef.h"  /* wegen ITEM                       */
#include "uevent.h"  /* wegen EVENT                      */
#include "router.h"  /* wegen BCHEAD und CMDBLK          */
#include "ftphdl.h"  /* wegen FTPConfig                  */
#include "action.h"  /* wegen ACTIONConfig               */

extern int	DebugPrintItem( int, ITEM *);
extern int	DebugPrintEvent( int, EVENT *);
extern int	DebugPrintBchead( int, BC_HEAD *);
extern int	DebugPrintCmdblk( int, CMDBLK *);
extern int	DebugPrintFTPConfig( int, FTPConfig *);
extern int	DebugPrintACTIONConfig( int, ACTIONConfig *);
extern int	DebugPrintEventLine( int ipLevel, char *cpCaller, EVENT *prpEvnt );
extern int	DebugPrintItemLine( int ipLevel, char *cpCaller, ITEM *prpitmItem );
extern int	DebugPrintItemEvent( int ipLevel, char *cpCaller, ITEM *prpitmItem);

#endif /* __DEBUGREC_H */

/* ******************************************************************** */
/* ******************************************************************** */
/*                      E N D   O F   F I L E                           */
/* ******************************************************************** */
/* ******************************************************************** */
