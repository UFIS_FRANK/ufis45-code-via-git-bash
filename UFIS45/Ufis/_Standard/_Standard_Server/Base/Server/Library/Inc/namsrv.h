#ifndef _DEF_mks_version_namsrv_h
  #define _DEF_mks_version_namsrv_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_namsrv_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/namsrv.h 1.2 2004/07/27 16:48:08SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.						  */
/*									  */
/*  Program	  :     						  */
/*  Revision date :							  */
/*  Author    	  :							  */
/*  									  */
/*  NOTE : This should be the only include file for your program          */
/* ********************************************************************** */

#ifndef __NAMSRV_INC
#define __NAMSRV_INC


#define NAMSRV_CEDA 	1
#define NAMSRV_EXCO 	2
#define NAMSRV_ARCHIV	3

#define NAMSRV_OWN_TYPE_STR	"OWN_TYPE"
#define NAMSRV_CEDA_STR		"CEDA"
#define NAMSRV_EXCO_STR		"EXCO"
#define NAMSRV_ARCHIV_STR	"ARCHIV"

#define NAMSRV_REQ_CMD		"NREQ"
#define NAMSRV_INFO_CMD		"NINF"
#define NAMSRV_SHUT_CMD		"SHUT"

#define NAMSRV_EMPTY		"0"

typedef struct {
  char serv_type[50];
  char serv_host[50];
} NAMSRV_INFO;

#endif

