#ifndef _DEF_mks_version_bhsif_h
  #define _DEF_mks_version_bhsif_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_bhsif_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/dgshdl.h 1.1 2007/05/03 19:42:40ICT akl Exp  $";
#endif /* _DEF_mks_version */
/**************************************************************/
/* DOCKING GUIDANCE HANDLING Interface header file (GOS)      */
/**************************************************************/
#ifndef BOOL
#define BOOL int
#endif

#define CONNECT_TIMEOUT 5
#define READ_TIMEOUT 2
#define WRITE_TIMEOUT 2

#define CFG_ALPHA       1200
#define CFG_NUM         1201
#define CFG_ALPHANUM    1202
#define CFG_IGNORE      1203
#define CFG_PRINT       1204


/************************/
/* CFG-File - STRUCTURE */
/************************/
typedef struct 
{
	/* MAIN section in .cfg file*/    
	char *debug_level;		/* start debug-level*/
	char *mode;						/* type of running mode (REAL or TEST)*/
	char *type;						/* SERVER or CLIENT (Default is SERVER */
	char *host1;					/* IP-address or hostname of the 1. server */ 
	char *host2;					/* IP-address or hostname of the 2. server */ 
	char *service_port; 	/* service name for the interface */
	char *wait_for_ack; 	/* wait-time in sec. for acknowledge */
	char *recv_timeout; 	/* wait-time in sec. for receiving a valid message */
	char *max_sends;			/* max. number of sends(resends) for a message */
	char *recv_log; 			/* log-file for received messages */
	char *send_log; 			/* log-file for sent messages */
	char *try_reconnect; 	/* try to connect every xy sec. */
	char *keep_alive; 		/* keep alive interval */
	char *keep_alive_format;/* the keep alive packet format (default tcpkeepalive.txt) */
	char *header_prefix;	/* prefix of the header (Default #) */
	char *header_size;		/* size of the header (including prefix. Default 6) */
	char *send_ack;				/* should we ack incoming messages? (default 1 = YES) */
	char *ack_format;			/* the ack packet format (default tcpack.txt in conf) */
	char *msgno_keyword;	/* The keyword preceeding the msg number, e.g, <msgno=> */
	char *ftyp_keyword;		/* Keywork to locate the FTYP flag */
	char *remove_suffix;	/* Remove suffix from stand names */
	char *keep_suffix;		/* Comma seperated list of stands where suffix should be kept */
	char *remove_zero;		/* Flag for remove leading zeros from DXB stand numbers */
	char *stand_tag;			/* The stand/position tag to search for */
}CFG;

/* CST Last Version */
