package jcedalib;

public class Bchead
{
      private static String mks_version = "@(#) UFIS_VERSION $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/jcedalib/Bchead.java 1.1 2005/09/05 15:20:05SGT heb Exp  $";
      public static String dest = "";
      public static String bcoriginator = "";
      public static String wks = "";
      public static String bcnum = "";
      public static String seqid = "";
      public static String totalbuf = "";
      public static String actbuf = "";
      public static String rseqid = "";
      public static String rc = "";
      public static String totalsize = "";
      public static String cmdsize = "";
      public static String datasize = "";
      public static String cmd = "";
      public static String object = "";
      public static String order = "";
      public static String twstart = "";
      public static String twend = "";
      public static String data = "";
      public static String fields = "";
      public static String selection = "";
      public static String route = "";
      public static String msglength = "";
      public static String originator = "";
      public static String blocknr = "";
      public static String event_command = "1"; //1=EVENT_DATA
      public static String event_type ="0"; //0=USR_EVENT


      public String getContentInfo()
      {
            return 
            "<DEST>" + this.dest + "</DEST>\n"+
            "<BCORIGINATOR>" + this.bcoriginator + "</BCORIGINATOR>\n"+
            "<WKS>" + this.wks + "</WKS>\n"+
            "<BCNUM>" + this.bcnum + "</BCNUM>\n"+
            "<SEQID>" + this.seqid + "</SEQID>\n"+
            "<TOTALBUF>" + this.totalbuf + "</TOTALBUF>\n"+
            "<ACTBUF>" + this.actbuf + "</ACTBUF>\n"+
            "<RSEQID>" + this.rseqid + "</RSEQID>\n"+
            "<RC>" + this.rc + "</RC>\n"+
            "<TOTALSIZE>" + this.totalsize + "</TOTALSIZE>\n"+
            "<CMDSIZE>" + this.cmdsize + "</CMDSIZE>\n"+
            "<DATASIZE>" + this.datasize + "</DATASIZE>\n"+
            "<CMD>" + this.cmd + "</CMD>\n"+
            "<OBJECT>" + this.object + "</OBJECT>\n"+
            "<ORDER>" + this.order + "</ORDER>\n"+
            "<TWSTART>" + this.twstart + "</TWSTART>\n"+
            "<TWEND>" + this.twend + "</TWEND>\n"+
            "<DATA>" + this.data + "</DATA>\n"+
            "<FIELDS>" + this.fields + "</FIELDS>\n"+
            "<SELECTION>" + this.selection + "</SELECTION>\n"+
            "<ROUTE>" + this.route + "</ROUTE>\n"+
            "<MSGLENGTH>" + this.msglength + "</MSGLENGTH>\n"+
            "<ORIGINATOR>" + this.originator + "</ORIGINATOR>\n"+
            "<BLOCKNR>" + this.blocknr + "</BLOCKNR>\n"+
            "<EVENT_COMMAND>" + this.event_command + "</EVENT_COMMAND>\n"+
            "<EVENT_TYPE>" + this.event_type + "</EVENT_TYPE>";
      }
}
