#ifndef _DEF_mks_version
#define _DEF_mks_version
#include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Stomp/bristomp.c 1.2 2013/04/17 21:27:36SGT gfo Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         : George Fourlanos GFO                                      */
/* Date           : 2011.01.03                                                */
/* Description    : Bridge Interface for ActiveMQ (with stomp library)        */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#define MAX_TMPBUF1_SIZE 8092
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>
/* includes for Stomp   */
#include "stomp.h"


#ifdef _HPUX_SOURCE
extern int daylight;
extern long timezone;
extern char *tzname[2];
#else
extern int _daylight;
extern long _timezone;
extern char *_tzname[2];
#endif

int debug_level = TRACE;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM *prgItem = NULL; /* The queue item pointer  */
static EVENT *prgEvent = NULL; /* The event pointer       */
static int igItemLen = 0; /* length of incoming item */
static EVENT *prgOutEvent = NULL;

static char cgProcessName[20] = "bristomp";
static char cgConfigFile[512] = "\0";
static char cgHopo[8] = "\0"; /* default home airport    */
static char cgTabEnd[8] = "\0"; /* default table extension */

static long lgEvtCnt = 0;
static int igLastCtrlSta;

static char cgMQServer[100] = "\0";
static char cgMQPort[10] = "\0";
static char cgQMgrReceive[50] = "\0";
static char cgQMgrSend[50] = "\0";
static char cgMqReceive[50] = "\0";
static char cgMqSend[50] = "\0";
static char cgWaitSend[100] = "0";
static int igUseInfobJ = TRUE;
static int igReconnectMq = 6;
static int igRcnctMax = 10;
static int igFwdModId = 7920;
static char cgFwdCmd[10] = "\0";
static char pcgTmpBuf1[MAX_TMPBUF1_SIZE];

pid_t gChildPid = 0;
/*Handles for MQ*/
apr_pool_t *pool;
stomp_connection *connection;

/*Handles for Receiving */

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int Init_Process();
static int ReadConfigEntry(char *pcpSection, char *pcpKeyword,
        char *pcpCfgBuffer);


static int Reset(void); /* Reset program          */
static void Terminate(int ipSleep); /* Terminate program      */
static void HandleSignal(int); /* Handles signals        */
static void HandleErr(int); /* Handles general errors */
static void HandleQueErr(int); /* Handles queuing errors */
static void HandleQueues(void); /* Waiting for Sts.-switch*/

static int HandleData(ITEM *prgItem); /* Handles event data     */
static int GetCommand(char *pcpCommand, int *pipCmd);
static int TimeToStr(char *pcpTime, time_t lpTime);
static int CreateLogFile();
void myinitialize(int argc, char *argv[]);

static int SendToCeda(char *pcpBuffer);

static int ConnectMQ();
static int ReceiveFromMQ();

static int SendToMq(char *pcpData);

static int CheckQueStatus(void);
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN{
    int ilRC = RC_SUCCESS; /* Return code			*/
    int ilCnt = 0;
    int ilOldDebugLevel = 0;

    //gChildPid = fork();

    /* added for SIGPIPE handle */
    struct sigaction sa;
    sa.sa_handler = SIG_IGN;

    /*    INITIALIZE;	*/
    myinitialize(argc, argv);

    dbg(TRACE, "MAIN: version mod_id <%d>", mod_id);

    /* CASE 1: */
    signal(SIGPIPE, SIG_IGN);

    /* CASE 2: */
    //    sigaction(SIGPIPE, &sa, 0);

    (void) SetSignals(HandleSignal);
    debug_level = TRACE;

    if (gChildPid == -1) {
        dbg(DEBUG, "%05d: Fork failed!", __LINE__);
    }

    strcpy(cgProcessName, argv[0]);

    dbg(TRACE, "MAIN: version <%s>", mks_version);
    while (gChildPid == -1) {
        dbg(TRACE, "********* fork failed, waiting 60 Seconds **********");
    }

    /* Attach to the MIKE queues */
    do {
        ilRC = init_que();
        if (ilRC != RC_SUCCESS) {
            dbg(TRACE, "MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    } while ((ilCnt < 10) && (ilRC != RC_SUCCESS));

    if (ilRC != RC_SUCCESS) {
        dbg(TRACE, "MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    } else {
        dbg(TRACE, "MAIN: init_que() OK! mod_id <%d>", mod_id);
    }/* end of if */


    sprintf(cgConfigFile, "%s/%s", getenv("BIN_PATH"), mod_name);
    dbg(TRACE, "Binary_File <%s>", cgConfigFile);
    ilRC = TransferFile(cgConfigFile);
    if (ilRC != RC_SUCCESS) {
        dbg(TRACE, "MAIN: TransferFile(%s) failed!", cgConfigFile);
    } /* end of if */

    sprintf(cgConfigFile, "%s/%s.cfg", getenv("CFG_PATH"), cgProcessName);
    /* sprintf(cgConfigFile, "%s/%s.cfg", getenv("CFG_PATH"), mod_name);*/
    dbg(TRACE, "ConfigFile <%s>", cgConfigFile);
    ilRC = TransferFile(cgConfigFile);
    if (ilRC != RC_SUCCESS) {
        dbg(TRACE, "MAIN: TransferFile(%s) failed!", cgConfigFile);
    } /* end of if */

    ilRC = SendRemoteShutdown(mod_id);
    if (ilRC != RC_SUCCESS) {
        dbg(TRACE, "MAIN: SendRemoteShutdown(%d) failed!", mod_id);
    } /* end of if */

    ilRC = Init_Process();
    if (ilRC != RC_SUCCESS) {
        dbg(TRACE, "MAIN: init_Process() failed! waiting 60 sec to exit ...");
        sleep(60);
        exit(2);
    }
    igLastCtrlSta = ctrl_sta;

    if (gChildPid == 0) {
        dbg(TRACE, "******* Read From MQ process*******");
    } else {
        dbg(TRACE, "******* Send To MQ process *******");
    }

    dbg(TRACE, "=====================");
    dbg(TRACE, "MAIN: initializing OK");
    dbg(TRACE, "=====================");

    if (gChildPid == 0) {
        ReceiveFromMQ();
    } else {
        for (;;) {
            /* the child should not listen to this CEDA-queue*/
            dbg(TRACE, "Now receive from CEDA-queue");
            ilRC = que(QUE_GETBIG, 0, mod_id, PRIORITY_3, igItemLen, (char *) &prgItem);
            /* depending on the size of the received item  */
            /* a realloc could be made by the que function */
            /* so do never forget to set event pointer !!! */
            prgEvent = (EVENT *) prgItem->text;
            dbg(TRACE, "CEDA-queue ilRC %d", ilRC);
            if (ilRC == RC_SUCCESS) {
                /* Acknowledge the item */
                ilRC = que(QUE_ACK, 0, mod_id, 0, 0, NULL);
                if (ilRC != RC_SUCCESS) {
                    /* handle que_ack error */
                    dbg(TRACE, "CEDA-queue HandleQueErr ilRC %d", ilRC);
                    HandleQueErr(ilRC);
                } /* fi */

                lgEvtCnt++;

                switch (prgEvent->command) {

                    case REMOTE_DB:
                        /* ctrl_sta is checked inside */
                        HandleRemoteDB(prgEvent);
                        break;
                    case SHUTDOWN:
                        /* process shutdown - maybe from uutil */
                        dbg(TRACE, "CEDA-queue SHUTDOWN ilRC %d", ilRC);
                        Terminate(1);
                        break;

                    case RESET:
                        ilRC = Reset();
                        break;

                    case EVENT_DATA:
                        ilRC = HandleData(prgItem);
                        if (ilRC != RC_SUCCESS) {
                            HandleErr(ilRC);
                        }/* end of if */
                        break;

                    case TRACE_ON:
                        dbg_handle_debug(prgEvent->command);
                        break;
                    case TRACE_OFF:
                        dbg_handle_debug(prgEvent->command);
                        break;
                    default:
                        dbg(TRACE, "MAIN: unknown event");
                        DebugPrintItem(TRACE, prgItem);
                        DebugPrintEvent(TRACE, prgEvent);
                        dbg(TRACE, "after DebugPrintEvent");
                        break;
                } /* end switch */
            } else {
                /* Handle queuing errors */
                dbg(TRACE, "CEDA-queue HandleQueErr ilRC %d", ilRC);
                HandleQueErr(ilRC);
            } /* end else */

        } /* end for */
    }

} /* end of MAIN */

static int ReadConfigEntry(char *pcpSection, char *pcpKeyword, char *pcpCfgBuffer) {
    int ilRC = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection, pcpSection);
    strcpy(clKeyword, pcpKeyword);

    ilRC = iGetConfigEntry(cgConfigFile, clSection, clKeyword,
            CFG_STRING, pcpCfgBuffer);
    if (ilRC != RC_SUCCESS) {
        dbg(TRACE, "Not found in %s: [%s] <%s>", cgConfigFile, clSection, clKeyword);
    } else {
        dbg(DEBUG, "Config Entry [%s],<%s>:<%s> found in %s",
                clSection, clKeyword, pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRC;
}


/******************************************************************************/
/* The initialization routine                                                 */

/******************************************************************************/

static int Init_Process() {
    int ilRC = RC_SUCCESS; /* Return code */
    int ilRc = RC_SUCCESS; /* Return code */
    char pclBuf[32] = "\0";
    char pclActMod[30] = "\0";
    int ilCnt = 0;

    if (ilRC == RC_SUCCESS) {
        ilRc = ReadConfigEntry("MAIN", "RUNTIME_MODE", pclBuf);
        if (ilRc == RC_SUCCESS) {
            if (strcmp(pclBuf, "DEBUG") == 0)
                debug_level = DEBUG;
            else if (strcmp(pclBuf, "OFF") == 0)
                debug_level = 0;
            else if (strcmp(pclBuf, "TRACE") == 0)
                debug_level = TRACE;
            else
                debug_level = TRACE;
        }
    }

    if (ilRC == RC_SUCCESS) {
        ilRC = ReadConfigEntry("MAIN", "ACT_MODE", pclActMod);
        if (strcmp(pclActMod, "SEND") == 0)
            gChildPid = 1;
        else if (strcmp(pclActMod, "READ") == 0)
            gChildPid = 0;
        else
            gChildPid = 1;
    }

    if (ilRC == RC_SUCCESS) {
        /* read HomeAirPort from SGS.TAB */
        ilRC = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
        if (ilRC != RC_SUCCESS) {
            dbg(TRACE, "<Init_Process> EXTAB,SYS,HOMEAP not found in SGS.TAB");
            Terminate(30);
        } else {
            dbg(TRACE, "<Init_Process> home airport <%s>", cgHopo);
        }
    }

    if (ilRC == RC_SUCCESS) {

        ilRC = tool_search_exco_data("ALL", "TABEND", cgTabEnd);
        if (ilRC != RC_SUCCESS) {
            dbg(TRACE, "<Init_Process> EXTAB,ALL,TABEND not found in SGS.TAB");
            Terminate(30);
        } else {
            dbg(TRACE, "<Init_Process> table extension <%s>", cgTabEnd);
        }
    }

    if (ilRC == RC_SUCCESS) {
        ilRC = ReadConfigEntry("QUEUE", "MQSERVER", cgMQServer);
        ilRC = ReadConfigEntry("QUEUE", "MQPORT", cgMQPort);
        ilRC = ReadConfigEntry("QUEUE", "MQ_RECEIVE", cgMqReceive);
        ilRC = ReadConfigEntry("QUEUE", "MQ_SEND", cgMqSend);
        ilRC = ReadConfigEntry("QUEUE", "WAIT_SEND_TO_CEDA", cgWaitSend);
        ilRC = ReadConfigEntry("QUEUE", "USE_INFOBJ", pcgTmpBuf1);
        if ((ilRC == RC_SUCCESS) && (strcmp(pcgTmpBuf1, "NO") == 0)) {
            dbg(TRACE, "CedaEvent will be used for SEND / REC ");
            igUseInfobJ = FALSE;
        }
    }

    if (ilRC == RC_SUCCESS) {
        ilRC = ReadConfigEntry("QUEUE", "MQ_FWD_MOD_ID", pclBuf);
        if (ilRC == RC_SUCCESS)
            igFwdModId = atoi(pclBuf);
        else
            igFwdModId = 7902;

        ilRC = ReadConfigEntry("QUEUE", "MQ_FWD_CMD", cgFwdCmd);
        if (ilRC != RC_SUCCESS)
            strcpy(cgFwdCmd, "XMLI");

        ilRC = ReadConfigEntry("RECONNECT", "RECONNECT_MQ_INTERVAL", pclBuf);
        if (ilRC == RC_SUCCESS)
            igReconnectMq = atoi(pclBuf);
        else
            igReconnectMq = 6;
        ilRC = ReadConfigEntry("RECONNECT", "RECNCT_MQ_MAX", pclBuf);

        if (ilRC == RC_SUCCESS)
            igRcnctMax = atoi(pclBuf);
        else
            igRcnctMax = 10;


        ilRC = RC_SUCCESS;
    }

    if (ilRC == RC_SUCCESS) {
        dbg(TRACE, " PID: <%d>", getpid());

        if (strlen(cgMQServer) > 0) {
            dbg(TRACE, "%05d: connect for sending to MQ", __LINE__);
            ilCnt = 0;
            do {
                ilRC = ConnectMQ();
                if (ilRC != RC_SUCCESS) {
                    dbg(TRACE, "MAIN: Connect MQ(%s) failed! waiting %d sec ...", cgMQServer, igReconnectMq);
                    sleep(igReconnectMq);
                    ilCnt++;
                }/* end of if */
                ilCnt++;
            } while ((ilCnt < igRcnctMax) && (ilRC != RC_SUCCESS));
        }
    }

    if (ilRC != RC_SUCCESS) {
        dbg(TRACE, "Init_Process failed");
    } else {
        dbg(DEBUG, "Init_Process successful");
    }

    return (ilRC);
} /* end of Init_Process */

/******************************************************************************/
/* The Reset routine                                                          */

/******************************************************************************/
static int Reset() {
    int ilRC = RC_SUCCESS; /* Return code */

    dbg(TRACE, "Reset: now resetting");

    return ilRC;

} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */

/******************************************************************************/
static void Terminate(int ipSleep) {
    int ilRC = RC_SUCCESS;
    apr_status_t rc;

    dbg(TRACE, "Sending Disconnect.");
    {
        stomp_frame frame;
        frame.command = "DISCONNECT";
        frame.headers = NULL;
        frame.body = NULL;
        /*frame.body_length = -1;*/
        /*
         * rc = stomp_write(connection, &frame,pool);
         * */
        rc = stomp_write(connection, &frame);

        if (ilRC != APR_SUCCESS) {
            dbg(TRACE, "Could not send frame");
        }

    }
    dbg(TRACE, "Disconnecting...");
    ilRC = stomp_disconnect(&connection);
    if (ilRC != APR_SUCCESS) {
        dbg(TRACE, "Could not disconnect");
    }

    apr_pool_destroy(pool);

    /*
    if (gChildPid == 0) {
    } else {
        kill(gChildPid, SIGTERM);
    }
     */

    /* unset SIGCHLD ! DB-Child will terminate ! */

    dbg(TRACE, "Terminate: now DB logoff ...");

    logoff();

    dbg(TRACE, "Terminate: now sleep(%d) ...", ipSleep);


    dbg(TRACE, "Terminate: now leaving ...");

    fclose(outp);

    sleep(ipSleep < 1 ? 1 : ipSleep);

    exit(0);

} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */

/******************************************************************************/

static void HandleSignal(int pipSig) {
    dbg(TRACE, "HandleSignal: signal <%d> received", pipSig);

    switch (pipSig) {
        case SIGCHLD:
            dbg(TRACE, "HandleSignal:SIGHCLD received, Terminate now");
            Terminate(1);
            break;
        case SIGTERM:
            dbg(TRACE, "HandleSignal:SIGTERM received !");
            if (gChildPid != 0) /* then its a parent */ {
                //    dbg(TRACE, "Due to SIGTERM Kill Child now");
                //    kill(gChildPid, SIGTERM);
            }
            dbg(TRACE, "Due to SIGTERM Terminate now");
            Terminate(1);
            break;

        case SIGPIPE:
            break;
        default:
            dbg(TRACE, "HandleSignal:SIGHCLD received default");
            Terminate(1);
            break;
    } /* end of switch */

    //    exit(1);

} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */

/******************************************************************************/
static void HandleErr(int pipErr) {
    dbg(TRACE, "HandleErr: <%d> : unknown ERROR", pipErr);

    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */

/******************************************************************************/
static void HandleQueErr(int pipErr) {
    switch (pipErr) {
        case QUE_E_FUNC: /* Unknown function */
            dbg(TRACE, "<%d> : unknown function", pipErr);
            break;
        case QUE_E_MEMORY: /* Malloc reports no memory */
            dbg(TRACE, "<%d> : malloc failed", pipErr);
            break;
        case QUE_E_SEND: /* Error using msgsnd */
            dbg(TRACE, "<%d> : msgsnd failed", pipErr);
            break;
        case QUE_E_GET: /* Error using msgrcv */
            dbg(TRACE, "%05d: <%d> : msgrcv failed", __LINE__, pipErr);
            Terminate(1);
            break;
        case QUE_E_EXISTS:
            dbg(TRACE, "<%d> : route/queue already exists ", pipErr);
            break;
        case QUE_E_NOFIND:
            dbg(TRACE, "<%d> : route not found ", pipErr);
            break;
        case QUE_E_ACKUNEX:
            dbg(TRACE, "<%d> : unexpected ack received ", pipErr);
            break;
        case QUE_E_STATUS:
            dbg(TRACE, "<%d> :  unknown queue status ", pipErr);
            break;
        case QUE_E_INACTIVE:
            dbg(TRACE, "<%d> : queue is inaktive ", pipErr);
            break;
        case QUE_E_MISACK:
            dbg(TRACE, "<%d> : missing ack ", pipErr);
            break;
        case QUE_E_NOQUEUES:
            dbg(TRACE, "<%d> : queue does not exist", pipErr);
            Terminate(1);
            break;
        case QUE_E_RESP: /* No response on CREATE */
            dbg(TRACE, "<%d> : no response on create", pipErr);
            break;
        case QUE_E_FULL:
            dbg(TRACE, "<%d> : too many route destinations", pipErr);
            break;
        case QUE_E_NOMSG: /* No message on queue */
            //            dbg(TRACE, "<%d> : no messages on queue", pipErr);
            break;
        case QUE_E_INVORG: /* Mod id by que call is 0 */
            dbg(TRACE, "<%d> : invalid originator=0", pipErr);
            break;
        case QUE_E_NOINIT: /* Queues is not initialized*/
            dbg(TRACE, "<%d> : queues are not initialized", pipErr);
            break;
        case QUE_E_ITOBIG:
            dbg(TRACE, "<%d> : requestet itemsize to big ", pipErr);
            break;
        case QUE_E_BUFSIZ:
            dbg(TRACE, "<%d> : receive buffer to small ", pipErr);
            break;
        default: /* Unknown queue error */
            dbg(TRACE, "<%d> : unknown error", pipErr);
            break;
    } /* end switch */
    return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle data routine                                                    */

/******************************************************************************/
static int HandleData(ITEM *prpItem) {

    char *pclBuffold;
    char pclBuff[40000];
    int ilRC = RC_SUCCESS;
    EVENT *prlEvent = NULL;
    BC_HEAD *prlBchead = NULL;
    CMDBLK *prlCmdblk = NULL;
    char *pclSelection = NULL;
    char *pclFields = NULL;
    char *pclData = NULL;
    char *pcpSrc = NULL;

    dbg(TRACE, "========== START SEND REMOTE EVENT  ==========");
    if (igUseInfobJ == TRUE) {

        prlEvent = (EVENT *) prpItem->text;
        prlBchead = (BC_HEAD *) ((char *) prlEvent + sizeof (EVENT));
        prlCmdblk = (CMDBLK *) ((char *) prlBchead->data);
        pclSelection = prlCmdblk->data;
        pclFields = pclSelection + strlen(pclSelection) + 1;
        pclData = pclFields + strlen(pclFields) + 1;

        //  pclBuff = malloc(sizeof(ITEM));
        strcpy(pclBuff, pclData);
        ilRC = SendToMq(pclBuff);
    } else {
        dbg(DEBUG, "CedaToMq:");

        pclBuffold = malloc(sizeof (ITEM));
        CedaToMq(prpItem, &pclBuffold);
        ilRC = SendToMq(pclBuffold);
    }

    dbg(TRACE, "CedaToMq done");

    //  free(pclBuff);

    dbg(TRACE, "========== END SEND REMOTE EVENT ==========");

    return (RC_SUCCESS);

} /* end of HandleData */

static int ConnectMQ() {
    static int ilRC = RC_SUCCESS;

    static char clMyMQServer[100] = "\0";
    static char clNowTime[20] = "\0";
    static char clAlert[256] = "\0";
    apr_status_t rc;

    setbuf(stdout, NULL);

    rc = apr_initialize();
    if (rc != APR_SUCCESS)
        dbg(TRACE, "Could not initialize");

    rc = apr_pool_create(&pool, NULL);
    if (rc != APR_SUCCESS)
        dbg(TRACE, "Could not allocate pool");

    dbg(TRACE, "Connecting %s %s......", cgMQServer, cgMQPort);
    rc = stomp_connect(&connection, cgMQServer, atoi(cgMQPort), pool);
    if (rc != APR_SUCCESS)
        dbg(TRACE, "Could not connect %d", rc);

    dbg(TRACE, "Connected %s %s......", cgMQServer, cgMQPort);
    if (rc == APR_SUCCESS) {
        dbg(TRACE, "Sending connect message.");
        stomp_frame frame;
        frame.command = "CONNECT";
        frame.headers = apr_hash_make(pool);
        apr_hash_set(frame.headers, "login", APR_HASH_KEY_STRING, NULL);
        apr_hash_set(frame.headers, "passcode", APR_HASH_KEY_STRING, NULL);
        frame.body = NULL;
        /*frame.body_length = -1;*/
        /*
         * rc = stomp_write(connection, &frame,pool);
         * */
        rc = stomp_write(connection, &frame);
        if (rc != APR_SUCCESS)
            dbg(TRACE, "Could not send frame");
        if (rc == APR_SUCCESS) {
            dbg(TRACE, "Reading Response.");
            stomp_frame *frame;
            rc = stomp_read(connection, &frame, pool);
            if (rc != APR_SUCCESS)
                dbg(TRACE, "Could not read frame");
            dbg(TRACE, "Response: %s, %s\n", frame->command, frame->body);
        }
    }
    if (rc != APR_SUCCESS)
        ilRC = RC_FAIL;
    return ilRC;
}

static int ReceiveFromMQ() {
    int ilRC = RC_SUCCESS;
    int ilLoop = TRUE;
    int ilBufLen = 0;
    int ilRealLen = 0;
    int ilOriginator;
    short slFkt = 0;
    char *pclBuf = NULL;
    char *pclSqlBuf = NULL;
    EVENT *prlEvent = NULL;
    ITEM *prlItem = NULL;
    BC_HEAD *prlBchead = NULL;
    CMDBLK *prlCmdblk = NULL;
    EVENT *prlRemQueEvent = NULL;
    static char cgSkipRootElement[50] = "";
    char *pclStart;
    char *pclEnd;

    apr_status_t rc;

    dbg(TRACE, "ReceiveFromMQ");
    /* In case we are the child process we will receive*/

    dbg(TRACE, "Sending Subscribe.");
    {
        stomp_frame frame;
        frame.command = "SUB";
        frame.headers = apr_hash_make(pool);
        apr_hash_set(frame.headers, "destination", APR_HASH_KEY_STRING, cgMqReceive);
        frame.body = NULL;
        /*frame.body_length = -1;*/
        /*
         * rc = stomp_write(connection, &frame,pool);
         * */
        rc = stomp_write(connection, &frame);
        if (rc != APR_SUCCESS) {
            dbg(TRACE, "Could not send frame");
        }

    }
    dbg(TRACE, "Subscribed.");
    fprintf(stdout, "Reading Response.");
    while (1) {
        char *messid;
        /*dbg(TRACE, "Reading Response.");    */
        {
            stomp_frame *frame;
            //      rc = stomp_new_read(connection, &frame, pool);
            rc = stomp_read(connection, &frame, pool);
            if (frame != NULL) {
                if (rc != APR_SUCCESS) {
                    dbg(TRACE, "Could not read frame");
                    Terminate(6);
                } else {
                    prlItem = malloc(sizeof (ITEM));
                    messid = (char *) apr_hash_get(frame->headers, "message-id", APR_HASH_KEY_STRING);
                    dbg(TRACE, "Response: command:%s, message:%s,  message-id:%s\n", frame->command, frame->body, messid);
                    SendToCeda(frame->body);
                }
            }
        }

        /* !!!! This Produces a SegFault
         * dbg(TRACE, "Sending Ack.");
        {
            stomp_frame *frame;
            frame.command = "ACK";
            frame.headers = apr_hash_make(pool);
            apr_hash_set(frame.headers, "destination", APR_HASH_KEY_STRING, cgMqReceive);
            apr_hash_set(frame.headers, "message-id", APR_HASH_KEY_STRING, messid);
            frame.body = NULL;

            rc = stomp_write(connection, &frame);
            if (rc != APR_SUCCESS)
                dbg(TRACE, "Could not send frame");
        }
         * */
        /*dbg(TRACE, "Ack was send.");*/
        (void) CheckQueStatus();
    }
    dbg(TRACE, "ReceiveFromMQ Exit.");
    return ilRC;
}

static int SendToCeda(char *pcpBuffer) {
    int ilRC = RC_SUCCESS;
    short slFkt = 0;
    short slCursor = 0;
    char clData[100] = "\0";
    char clUrno[12] = "\0";
    char clRecUrno[12] = "\0";
    int ilCount = 0;
    int ilAckRC = RC_FAIL;
    int ilLen = 0;
    char clNowTime[20] = "\0";
    char clSqlBuf[4500] = "\0";
    char clTwStart[30] = "\0";
    char clTwEnd[30] = "\0";
    int i = 0;
    BC_HEAD *prlBchead = NULL;
    CMDBLK *prlCmdblk = NULL;
    char *pclSelection = NULL;
    char *pclFields = NULL;
    char *pclData = NULL;
    char *pclRow = NULL;
    static ITEM *prlItem;
    EVENT *prlEvent;

    dbg(TRACE, "%05d: -----SendToCeda start -----", __LINE__);
    dbg(TRACE, "%05d: pcpBuffer: <%s>", __LINE__, pcpBuffer);

    if (igUseInfobJ == TRUE) {

        dbg(TRACE, "%05d: Now do MqToCeda", __LINE__);

        /****************************************/
        /* Check if prlItem->route equals mod_id and avoid sending the message
         otherwise this will create a loop
         */
        ilRC = SendCedaEvent(igFwdModId, /* adress to send the answer   */
                0, /* Set to mod_id if < 1        */
                clData, /* BC_HEAD.dest_name           */
                "", /* BC_HEAD.recv_name           */
                "", /* CMDBLK.tw_start             */
                "", /* CMDBLK.tw_end               */
                cgFwdCmd, /* CMDBLK.command              */
                "", /* CMDBLK.obj_name             */
                "", /* Selection Block             */
                "", /* Field Block                 */
                pcpBuffer, /* Data Block                  */
                "", /* Error description           */
                PRIORITY_3, /* 0 or Priority       */
                RC_SUCCESS); /* BC_HEAD.rc: RC_SUCCESS or   */
    } else {

        MqToCeda(&prlItem, pcpBuffer, NULL, NULL);
        dbg(TRACE, "%05d: MqToCeda done", __LINE__);
        prlEvent = (EVENT *) prlItem->text;

        prlBchead = (BC_HEAD *) ((char *) prlEvent + sizeof (EVENT));
        prlCmdblk = (CMDBLK *) ((char *) prlBchead->data);
        pclSelection = prlCmdblk->data;
        pclFields = pclSelection + strlen(pclSelection) + 1;
        pclData = pclFields + strlen(pclFields) + 1;


        /****************************************/
        dbg(TRACE, "Command:   <%s>", prlCmdblk->command);
        dbg(TRACE, "tw_start:  <%s>", prlCmdblk->tw_start);
        dbg(TRACE, "tw_end:    <%s>", prlCmdblk->tw_end);
        dbg(TRACE, "originator <%d>", prlEvent->originator);
        dbg(TRACE, "selection  <%s>", pclSelection);
        dbg(TRACE, "fields     <%s>", pclFields);
        dbg(TRACE, "data       <%s>", pclData);
        dbg(TRACE, "route      <%d>", prlItem->route);
        /****************************************/

        /* Check if prlItem->route equals mod_id and avoid sending the message
         otherwise this will create a loop
         */



        ilLen = sizeof (EVENT) + sizeof (BC_HEAD) + sizeof (CMDBLK) + strlen(pclSelection) + strlen(pclFields) + strlen(pclData);
        ilRC = que(QUE_PUT, prlItem->route, mod_id, PRIORITY_3, ilLen, (char *) prlEvent);
    }
    dbg(TRACE, "%05d: Now wait <%s> milliseconds", __LINE__, cgWaitSend);
    nap(atoi(cgWaitSend));
    dbg(TRACE, "%05d: -----SendToCeda end -----", __LINE__);
    return ilRC;
}

static int TimeToStr(char *pcpTime, time_t lpTime) {
    struct tm *_tm;
    char _tmpc[6];

    /*_tm = (struct tm *)localtime(&lpTime); */
    _tm = (struct tm *) gmtime(&lpTime);
    /*      strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",_tm); */


    sprintf(pcpTime, "%4d%02d%02d%02d%02d%02d",
            _tm->tm_year + 1900, _tm->tm_mon + 1, _tm->tm_mday, _tm->tm_hour,
            _tm->tm_min, _tm->tm_sec);

    return RC_SUCCESS;

} /* end of TimeToStr */

void myinitialize(int argc, char *argv[]) {
    mod_name = argv[0];
    /* 20020822 JIM: if argv[0] contains path (i.e. started by ddd), skip path */
    if (strrchr(mod_name, '/') != NULL) {
        mod_name = strrchr(mod_name, '/');
        mod_name++;
    }
    if (gChildPid == 0) {
        sprintf(__CedaLogFile, "%s/%sc%.5ld.log", getenv("DBG_PATH"), mod_name, getpid());
    } else {
        sprintf(__CedaLogFile, "%s/%sp%.5ld.log", getenv("DBG_PATH"), mod_name, getpid());
    }
    outp = fopen(__CedaLogFile, "w");
    if (argc > 1) {
        mod_id = atoi(argv[1]);
        ctrl_sta = atoi(argv[2]);
    } else {
        mod_id = 0;
        ctrl_sta = 1;
    }
    glbrc = init();
    if (glbrc) {
        fprintf(outp, "\n<%s><%ld>STHINIT failed: ret = %d\n", mod_name, getpid(), glbrc);
        exit(1);
    }
}

static int SendToMq(char *pcpData) {
    int ilRC = RC_SUCCESS;
    char clMyTime[30] = "\0";
    char clNowTime[30] = "\0";
    short slFkt = 0;
    short slCursor = 0;
    char clUrno[20] = "\0";
    char clSqlBuf[4500] = "\0";
    char clData[4500] = "\0";
    char clAlert[256] = "\0";
    apr_status_t rc;

    dbg(TRACE, "-----SendToMq start -----");

    stomp_frame frame;
    frame.command = "SEND";
    frame.headers = apr_hash_make(pool);
    apr_hash_set(frame.headers, "destination", APR_HASH_KEY_STRING, cgMqSend);
    apr_hash_set(frame.headers, "persistent", APR_HASH_KEY_STRING, "true");
    /*frame.body_length = -1;*/
    frame.body = pcpData;

    /*
     * rc = stomp_write(connection, &frame,pool);
     * */
    dbg(TRACE, "Response: %s, \n%s\n", frame.command, frame.body);

    // Write the headers
    if (frame.headers != NULL) {
        apr_hash_index_t *i;
        const void *key;
        void *value;
        char mybuff[32];
        for (i = apr_hash_first(NULL, frame.headers); i; i = apr_hash_next(i)) {
            apr_hash_this(i, &key, NULL, &value);
            strncpy(mybuff, (char *) key, strlen((char*) key));
            mybuff[strlen((char*) key)] = '\0';
            dbg(TRACE, "frame headers->key: %s", mybuff);
            strncpy(mybuff, (char *) value, strlen((char*) value));
            mybuff[strlen((char*) value)] = '\0';
            dbg(TRACE, "frame headers->value: %s", mybuff);
        }
    }

    rc = stomp_write(connection, &frame);
    if (rc != APR_SUCCESS)
        dbg(TRACE, "-----Could not send frame -----");


    dbg(TRACE, "-----SendToMq end -----");

    return ilRC;
}

static void terminate() {
    apr_terminate();
}

static int CheckQueStatus(void) {
    int ilRC;
    int len;
    /*memset(prgItem, 0x00, MAX_EVENT_SIZE);*/
    len = 0;
    /* 20020628 JIM: test for memory leak: allocation should be done in que */
    /* prgItem=NULL; */
    if ((ilRC = que(QUE_GETBIGNW, 0, mod_id, PRIORITY_3, len, (char*) &prgItem)) == RC_SUCCESS) {
        /* Acknowledge the item */
        ilRC = que(QUE_ACK, 0, mod_id, 0, 0, NULL);
        if (ilRC != RC_SUCCESS) {
            HandleQueErr(ilRC);
        }
        prgEvent = (EVENT *) prgItem->text;
        switch (prgEvent->command) {
            case HSB_COMING_UP:
                ctrl_sta = prgEvent->command;
                igLastCtrlSta = ctrl_sta;
                return -1;

            case HSB_STANDBY:
                ctrl_sta = prgEvent->command;
                igLastCtrlSta = ctrl_sta;
                return 0;

            case HSB_ACTIVE:
                ;
            case HSB_STANDALONE:
            case HSB_ACT_TO_SBY:
                ctrl_sta = prgEvent->command;
                if (igLastCtrlSta == HSB_STANDBY) {
                    igLastCtrlSta = ctrl_sta;
                    Reset();
                }
                return 0;
            case HSB_DOWN:
                ctrl_sta = prgEvent->command;
                igLastCtrlSta = ctrl_sta;
                Terminate(0);
                break;

            case SHUTDOWN:
                Terminate(0);
                break;

            case RESET:
                Reset();
                return -2;

            case EVENT_DATA:
                //                HandleData();
                return 0;

            case TRACE_OFF:
            case TRACE_ON:
                dbg_handle_debug(prgEvent->command);
                return 0;

            default:
                dbg(DEBUG, "main unknown event");
                DebugPrintItem(TRACE, prgItem);
                DebugPrintEvent(TRACE, prgEvent);
                return 0;
        }
    } else
        HandleQueErr(ilRC);

    return 0;
}
