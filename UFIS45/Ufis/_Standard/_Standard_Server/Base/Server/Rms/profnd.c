#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Rms/profnd.c 1.9 2010/10/20 16:59:28SGT ble Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton: PROFND                                        */
/*                                                                            */
/* Author         : hag                                                       */
/* Date           :                                                           */
/* Description    : profile finder for common check-in                        */
/*                                                                            */
/* Update history :                                                           */
/*                 19.01.2001 GKA Bugfixed invalid comment statement          */ 
/* MEI 20101020 : Error in static group searching when same names are used in SGRTAB */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
/* static char sccs_version[] ="@(#) UFIS4.5 (c) ABB AAT/I profnd.c 4.5.1.2 / 03/09/17 / HAG"; */
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h"
#include "AATArray.h"
#include "libccstr.h"
#include "syslib.h"
#include "fditools.h"
#include "chkval.h"
#include "rmschkrul.h"


extern int get_no_of_items(char *s);


/******************************************************************************/
/* Constants																  */
/******************************************************************************/
#define ARR_NAME_LEN           (8)
#define FIELD_LEN			4	
#define ARR_FLDLST_LEN         (256)
/*					 0	  1		2	3	 4	  5		6	7	 8	  9		10	11	 12	  13   14	15	 16	  17   18	19	 20   21   22		 		*/	
#define PRO_FIELDS "CDAT,DCOP,DCUP,DGAP,DSEP,EVTD,FDCO,FDCU,FDGA,FDSE,FISU,HOPO,LSTU,NAME,PRIO,PRST,REMA,UARC,URNO,USEC,USEU,UTPL,EXCL"  
#define RUE_FIELDS "RUSN,RUTY,URNO"

/*#define RUD_FIELDS "ALOC,DBAR,DEAR,DRTY,FADD,FFPD,HOPO,REFT,UCRU,UGHS,ULNK,URNO,URUE,VREF"*/
#define RUD_FIELDS "ALOC,FFPD,HOPO,URNO,URUE"
#define TSR_FIELDS "BTAB,BFLD,RTAB,RFLD,URNO"
#define DGR_FIELDS "GNAM,HOPO,URNO,UTPL,REFT,EXPR"
#define SGR_FIELDS "HOPO,GRPN,URNO,TABN,FLDN,APPL"

#define EVT_TURNAROUND	(0)
#define EVT_INBOUND	(1)
#define EVT_OUTBOUND	(2)
#define EVT_UNKNOWN     (3)

#define IDX_INBOUND (0)
#define IDX_OUTBOUND (1)
#define OP_IN		"IN"
#define CDT_ASC_SIMPLE  '0'
#define CDT_ASC_DYNAMIC '1'
#define CDT_ASC_STATIC  '2'

#define	DISCAL_ID		5250											/*rna*/

/******************************************************************************/
/* Typedefs																	  */
/******************************************************************************/
struct _arrayinfo
{

  HANDLE   rrArrayHandle;
  char     crArrayName[ARR_NAME_LEN+1];
  char     crArrayFieldList[ARR_FLDLST_LEN+1];
  long     lrArrayFieldCnt;
  char    *pcrArrayRowBuf;
  long     lrArrayRowLen;
  long    *plrArrayFieldOfs;
  long    *plrArrayFieldLen;
  HANDLE   rrIdx01Handle;
  char     crIdx01Name[ARR_NAME_LEN+1];
  char     crIdx01FieldList[ARR_FLDLST_LEN+1];
  long     lrIdx01FieldCnt;
  char    *pcrIdx01RowBuf;
  long     lrIdx01RowLen;
/*  long    *plrIdx01FieldPos;
  long    *plrIdx01FieldOrd;*/
  HANDLE   rrIdxUrnoHandle;
  char     crIdxUrnoName[ARR_NAME_LEN+1];
};
typedef struct _arrayinfo ARRAYINFO;

struct _condinfo 
{
 char   crTable[8] ;
 char   crField[8] ;
 char   crType[2] ;
 char   crOp[8] ;
 char   crValue[801] ;
 char   crRefTab[11] ;
 char   crRefFld[11] ;
} ;
typedef struct _condinfo CONDINFO ;

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = TRACE;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static EVENT  *prgOutEvent      = NULL ;
static long    lgOutEventSize  = 0 ;

static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];
static char  cgHopo[8];                         /* default home airport    */
static char  cgTabEnd[64]="" ;
static char pcgRecvName[64]; /* BC-Head used as WKS-Name */
static char pcgDestName[64]; /* BC-Head used as USR-Name */
static char pcgTwStart[64];  /* BC-Head used as Stamp */
static char pcgTwEnd[64];  /* BC-Head used as Stamp */ 
static int   igQueOut;  
static	int		igStartUpMode = TRACE;
static	int		igRuntimeMode = 0;
ARRAYINFO rgTplArray;
ARRAYINFO rgProArray;
ARRAYINFO rgRudArray;
ARRAYINFO rgRueArray;
ARRAYINFO rgTsrArray;
ARRAYINFO rgDgrArray;
ARRAYINFO rgSgrArray;
ARRAYINFO rgSgmArray;
ARRAYINFO rgFeleEvalArr;	/* saves field lengths */

static char    cgAddFields[256]="ADID,FTYP,STOD,TIFD,URNO";
static char    cgFieldListDest[256]="7550,7560"; 
static int     igDest          = 5250;   
static char  *pcgInbound = NULL;
static char  *pcgOutbound = NULL;
static char   cgSoh[2];
static char   cgStx[2];
static char   cgEtx[2];
static char   *pcgFieldList = NULL ;
int igExtendedTracing=1;
static char cgEvalBuffer[201];				

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int Init_profnd();
static int	Reset(void);                       /* Reset program          */
static void	Terminate(int ipSleep);                   /* Terminate program      */
/*static void	HandleSignal(int);*/                /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void	HandleQueErr(int);                 /* Handles queuing errors */
static int	HandleData(void);                  /* Handles event data     */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/
static int GetRowLength(char *pcpTana, char *pcpFieldList, long *plpLen);
static int GetFieldLength(char *pcpTana, char *pcpFina, long *plpLen);
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, char *pcpArrayFieldList,char *pcpAddFields,long *pcpAddFieldLens,char *pcpSelection, ARRAYINFO *prpArrayInfo);
static int  SendFieldListToOne( int ipDest, char *pcpHopo );
static int  SendFieldListToAll(char *pcpModIds);      
static int MyCompare(const void* ppP1, const void* ppP2); 
static int CheckRuleUrno ( char *pcpRuleUrno );
static int GetFieldByUrno ( ARRAYINFO *prpArray, char *pcpUrno, char *pcpField, 
							  char *pcpResult, int ipMaxBytes );

static int CreateIdx1 ( ARRAYINFO *prpArrayInfo, char *pcpIdxName, 
                        char *pcpFields, char *pcpOrdering );       

static int GetEventType(long *plpEventType,char **pcpInbound, char **pcpOutbound,char *pcpData);
static int GetPartialUrues( char *pcpRueUrnos, char *pcpPartUrue, int *pipCount );
static int GetRefTabField ( char *pcpBtab, char *pcpBfld, 
						    char *pcpRtab, char *pcpRfld );
static int ProcessConditions ( char *pcpHopo, char *pcpEvtd, char *pcpUtpl, int *pipEval );
static void DebugPrintCondInfo(int ipDebugLevel, CONDINFO *prpCondInfo);
static int ProcessProfiles(char *pcpHopo, char *pcpUrno );
static int GetCEDAField ( HANDLE * pspHandle, char *pcpArrayName,
						  long *plpFields, char *pcpFieldNam,
						  long lpMaxBytes, long lpRowNum,
						  void *pvpDataBuf, int ipTrimRight );
void TrimRight(char *s);
static int GetFieldData(char *pcpTabNam, char *pcpFldNam, char *pcpFldLst, 
						char *pcpData, char **pcpFieldData);
static int  CheckCDT2 ( char *pcpHopo, CONDINFO *pspCondition, int *pipEval ) ;
static int  CheckCDT1 ( char *pcpHopo, CONDINFO *pspCondition, char *pcpUtpl, int *pipEval ) ;
static int  CheckCDT0 ( char *pcpHopo, CONDINFO *pspCondition, char *pcpTable, int *pipEval ) ;
static int AnalyseStr( char *pcpHopo, char *cpSrcStr, char *cpTabname,
						long *lpFldCnt, char *cp1FieldStr, long *lpUsedChars) ;
static int SetCmpStringCond (char *pcpHopo, char *pcpCondition, char *pcpCmpStr, char *pcpDataArea  );
static int CheckCondition(char *pcpCondition);
static int ForwardEvent( int ipDest, char *pcpProUrno, char *pcpRueTyp0Urnos );
void ReplaceChar ( char *pcpStr, char cpOld, char cpNew );
static int CheckProfileCalendar(char *pcpHopo, char *pcpUrno, int *pipIsValid);
static int IsFlightToCalc ( char *pcpHopo, char *pcpFlight, int *pipGoOn );
static int FindFeleEvaluation ( char *pcpTable, char *pcpField, char *pcpFele );
static int SaveFeleEvaluation ( char *pcpTable, char *pcpField, char *pcpFele );
static int  SetAATArrayInfo(char *pcpArrayName,char *pcpArrayFieldList, long *plpFieldLens, 
						    char *pcpIdx01Name, char *pcpIdx01FieldList, char *pcpIdx01Order, 
						    ARRAYINFO *prpArrayInfo);
static int GetLogicalRowLength(char *pcpTotalFieldList,char *pcpFieldList, long *plpFieldSizes,long *plpLen);
static int GetItemNo(char *pcpFieldName,char *pcpFields, int *pipItemNo);
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo, int ipMode, int ipLogFile );
static int	GetDebugLevel(char *, int *);

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRC = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;
	
	INITIALIZE;			/* General initialization	*/
	dbg(TRACE,"MAIN: version <%s>",mks_version);
	/* Attach to the MIKE queues */
	do
	{
		ilRC = init_que();
		if(ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRC != RC_SUCCESS));
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}else{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */
	do
	{
		ilRC = init_db();
		if (ilRC != RC_SUCCESS)
		{
			check_ret(ilRC);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
		ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRC != RC_SUCCESS));
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}else{
		dbg(TRACE,"MAIN: init_db() OK!");
	} /* end of if */
	/* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
	sprintf(cgConfigFile,"%s/profnd",getenv("BIN_PATH"));
	ilRC = TransferFile(cgConfigFile);
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */
	
	sprintf(cgConfigFile,"%s/profnd.cfg",getenv("CFG_PATH")); 
	ilRC = TransferFile(cgConfigFile); 
	if(ilRC != RC_SUCCESS) 
	{ 
	 	dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile); 
	}  /* end of if */

	ilRC = SendRemoteShutdown(mod_id);
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */
	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */
	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		if(igInitOK == FALSE)
		{
			ilRC = Init_profnd();
			if(ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"Init_profnd: init failed!");
			} /* end of if */
			else
                igInitOK = TRUE;
		}/* end of if */
	} 
	else 
		Terminate(0);
	dbg(TRACE,"MAIN: initializing OK");
	debug_level = igRuntimeMode;
	for(;;)
	{
		ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRC == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRC != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRC);
			} /* fi */
			
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(0);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
				Terminate(0);
				break;
					
			case	RESET		:
				ilRC = Reset();
				break;
					
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRC = HandleData();
					if(ilRC != RC_SUCCESS)
					{
						HandleErr(ilRC);
					}/* end of if */
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			case 88:
				ilRC = SendFieldListToAll(cgFieldListDest) ;                       
				if (ilRC != RC_SUCCESS)                                            
					dbg (TRACE,"Init_profnd: SendFieldListToAll failed") ;         
				else                                                               
					dbg(TRACE,"Init_profnd: SendFieldListToAll OK") ;              
				break;
			case 99:
				ilRC = SaveIndexInfo ( &rgRudArray, 2, 0 );
				break;
			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRC);
		} /* end else */
		
	} /* end for */
	
	/*exit(0);*/
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_profnd()
{
    int	 ilRC = RC_SUCCESS;			/* Return code */
	char clSection[64];
    char clKeyword[64];   
	long pllAddFieldLens[3], llFieldLength;

	cgSoh[0] = 0x01;
	cgSoh[1] = 0x00;

	cgStx[0] = 0x02;
	cgStx[1] = 0x00;

 	cgEtx[0] = 0x03 ;
 	cgEtx[1] = 0x00 ;

    /* now reading from configfile or from database */

	if(ilRC == RC_SUCCESS)
	{
		/* read HomeAirPort from SGS.TAB */
		memset((void*)&cgHopo[0], 0x00, 8);

		sprintf(&clSection[0],"SYS");
		sprintf(&clKeyword[0],"HOMEAP");

		ilRC = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgHopo[0]);
		if (ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"Init_profnd: EXTAB,%s,%s not found in SGS.TAB",
				&clSection[0], &clKeyword[0]);
			Terminate(0);
		} 
		else
			dbg(TRACE,"Init_profnd: home airport    <%s>",&cgHopo[0]);
	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		sprintf(&clSection[0],"ALL");
		sprintf(&clKeyword[0],"TABEND");

		ilRC = tool_search_exco_data(&clSection[0], &clKeyword[0],&cgTabEnd[0]);

 		if (ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"Init_profnd: EXTAB,%s,%s not found in SGS.TAB",
					  &clSection[0], &clKeyword[0]);
			Terminate(0);
		} 
		else 
			dbg(TRACE,"Init_profnd: table extension <%s>",&cgTabEnd[0]);
	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		 GetDebugLevel("STARTUP_MODE", &igStartUpMode);
		 GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
	}
	debug_level = igStartUpMode;

	/* Read Table TPL */
    ilRC = CEDAArrayInitialize(10,10);
	if(ilRC != RC_SUCCESS)		
		dbg(TRACE,"Init_profnd: CEDAArrayInitialize failed <%d>",ilRC);	

    if (ilRC == RC_SUCCESS)                                                            
    {   /* create a table where all evaluations of fieldlenghts are stored */          
        if ( GetFieldLength("SYS","TANA",&llFieldLength) == RC_SUCCESS )               
            pllAddFieldLens[0] = llFieldLength;                                        
        else                                                                           
        {                                                                              
            dbg(TRACE,"Init_profnd: GetFieldLength for SYS.TANA failed <%d>",ilRC);    
            pllAddFieldLens[0] = 32;                                                   
        }                                                                              
                                                                                       
        if ( GetFieldLength("SYS","FINA",&llFieldLength) == RC_SUCCESS )               
            pllAddFieldLens[1] = llFieldLength;                                        
        else                                                                           
        {                                                                              
            dbg(TRACE,"Init_profnd: GetFieldLength for SYS.FINA failed <%d>",ilRC);    
            pllAddFieldLens[1] = 16;                                                   
        }                                                                              
                                                                                       
        if ( GetFieldLength("SYS","FELE",&llFieldLength) == RC_SUCCESS )               
            pllAddFieldLens[2] = llFieldLength;                                        
        else                                                                           
        {                                                                              
            dbg(TRACE,"Init_profnd: GetFieldLength for SYS.FELE failed <%d>",ilRC);    
            pllAddFieldLens[2] = 8;                                                    
        }                                                                              
                                                                                       
        ilRC = SetAATArrayInfo( "LENEVAL", "TANA,FINA,FELE", pllAddFieldLens,          
                                "IDXLEN", "TANA,FINA", "A,A", &rgFeleEvalArr );        
        if (ilRC != RC_SUCCESS)                                                        
            dbg(TRACE,"Init_profnd: SetAATArrayInfo for LENEVAL failed <%d>",ilRC);    
    }                                                                                  


	if (ilRC == RC_SUCCESS)	
	{	
		ilRC = SetArrayInfo("TPLTAB", "TPL", "TPST,HOPO,URNO,TNAM,RELX,FLDD",
							0, 0, "WHERE APPL='PROFILE' AND TPST='1'",&rgTplArray);
		if(ilRC != RC_SUCCESS)                                             
    		dbg(TRACE,"Init_profnd: SetArrayInfo for TPL failed <%d>",ilRC);		
	}
	if(ilRC == RC_SUCCESS)		
	{
		ilRC = SetArrayInfo("RUDTAB", "RUD", RUD_FIELDS, 0, 0, "where FFPD='1'",
							&rgRudArray );
		

		if(ilRC != RC_SUCCESS)                                             
    		dbg(TRACE,"Init_profnd: SetArrayInfo for RUD failed <%d>",ilRC);		
	}
	if(ilRC == RC_SUCCESS)		
	{
		ilRC = CreateIdx1 ( &rgRudArray, "IDXRUD1", "ALOC,FFPD,URUE", 
							"A,A,A" );
	    if (ilRC!=RC_SUCCESS)
			dbg(TRACE,"Init_profnd: CreateIdx1 for RUD failed <%d>",ilRC);		
	}	
	if (ilRC == RC_SUCCESS)                                        
	{                                                              
		ilRC = SetArrayInfo("RUETAB", "RUE", RUE_FIELDS, 0, 0, "", &rgRueArray );
		if (ilRC != RC_SUCCESS)                                             
			dbg(TRACE,"Init_profnd: SetArrayInfo for RUE failed <%d>",ilRC);
	}
	
	if (ilRC == RC_SUCCESS)                                        
	{                                                              
		ilRC = SetArrayInfo("PROTAB", "PRO", PRO_FIELDS, 0, 0, "", &rgProArray );
		if (ilRC != RC_SUCCESS)                                             
			dbg(TRACE,"Init_profnd: SetArrayInfo for PRO failed <%d>",ilRC);
		else
		{
			ilRC = CreateIdx1 ( &rgProArray, "IDXPRO1", "PRST,HOPO,EXCL,PRIO", 
							    "D,A,D,D" );
			if (ilRC!=RC_SUCCESS)
				dbg(TRACE,"Init_profnd: CreateIdx1 for PRO failed <%d>",ilRC);		
		}

	}
	if (ilRC == RC_SUCCESS)                                        
	{                                                              
		ilRC = SetArrayInfo("TSRTAB", "TSR", TSR_FIELDS, 0, 0, "", &rgTsrArray );
		if (ilRC != RC_SUCCESS)                                             
			dbg(TRACE,"Init_profnd: SetArrayInfo for TSR failed <%d>",ilRC);
		else
		{
			ilRC = CreateIdx1 ( &rgTsrArray, "IDXTSR1", "BFLD,BTAB", "A,A" );
			if (ilRC!=RC_SUCCESS)
				dbg(TRACE,"Init_profnd: CreateIdx1 for TSR failed <%d>",ilRC);		
		}
	}
	if (ilRC == RC_SUCCESS)                                        
	{                                                              
		ilRC = SetArrayInfo("DGRTAB", "DGR", DGR_FIELDS, 0, 0, "", &rgDgrArray );
		if (ilRC != RC_SUCCESS)                                             
			dbg(TRACE,"Init_profnd: SetArrayInfo for DGR failed <%d>",ilRC);
		else
		{
			ilRC = CreateIdx1 ( &rgDgrArray, "IDXDGR1", "HOPO,UTPL,GNAM,URNO", 
								"D,D,D,D" );
			if (ilRC!=RC_SUCCESS)
				dbg(TRACE,"Init_profnd: CreateIdx1 for DGR failed <%d>",ilRC);		
		}
	}
	if (ilRC == RC_SUCCESS)                                        
	{                                                              
		ilRC = SetArrayInfo("SGRTAB", "SGR", SGR_FIELDS, 0, 0, "", &rgSgrArray );
		if (ilRC != RC_SUCCESS)                                             
			dbg(TRACE,"Init_profnd: SetArrayInfo for SGR failed <%d>",ilRC);
		else
		{
			ilRC = CreateIdx1 ( &rgSgrArray, "IDXSGR1", "HOPO,GRPN,TABN,URNO,FLDN", 
								"D,D,D,D,D" );
			if (ilRC!=RC_SUCCESS)
				dbg(TRACE,"Init_profnd: CreateIdx1 for SGR failed <%d>",ilRC);		
		}
	}
	if (ilRC == RC_SUCCESS)                                        
	{                                                              
		ilRC = SetArrayInfo("SGMTAB", "SGM", "HOPO,USGR,UVAL,URNO", 0, 0, "", &rgSgmArray );
		if (ilRC != RC_SUCCESS)                                             
			dbg(TRACE,"Init_profnd: SetArrayInfo for SGM failed <%d>",ilRC);
		else
		{
			ilRC = CreateIdx1 ( &rgSgmArray, "IDXSGM1", "HOPO,USGR,UVAL,URNO", 
								"D,D,D,D,D" );
			if (ilRC!=RC_SUCCESS)
				dbg(TRACE,"Init_profnd: CreateIdx1 for SGM failed <%d>",ilRC);		
		}
	}
   	if (ilRC == RC_SUCCESS)
 	{
  		ilRC = InitValidityModule (CHKVAL_ARRAY, "VALTAB") ;
  		if (ilRC != RC_SUCCESS)
  		{
   			dbg (TRACE,"Init_profnd: InitValidityModule failed <%d>",ilRC) ;
   			ilRC = RC_SUCCESS ;
  		}
  		else
  		{
   			dbg (DEBUG,"Init_profnd: InitValidityModule OK") ;  

    			ilRC = ConfigureAction ("VALTAB", 0, 0 ) ;
    			if (ilRC != RC_SUCCESS)
     				dbg(TRACE,"Init_profnd: ConfigureAction failed <%d>", ilRC) ;
 		} /* end of if */
 	} /* end of if */

	/*		
	if (ilRC == RC_SUCCESS)  
	{    create a table where all evaluations of fieldlenghts are stored 
		if ( GetFieldLength("SYS","TANA",&llFieldLength) == RC_SUCCESS )
			pllAddFieldLens[0] = llFieldLength;
		else
		{
			dbg(TRACE,"Init_profnd: GetFieldLength for SYS.TANA failed <%d>",ilRC);
			pllAddFieldLens[0] = 32;
		}

		if ( GetFieldLength("SYS","FINA",&llFieldLength) == RC_SUCCESS )
			pllAddFieldLens[1] = llFieldLength;
		else
		{
			dbg(TRACE,"Init_profnd: GetFieldLength for SYS.FINA failed <%d>",ilRC);
			pllAddFieldLens[1] = 16;
		}

		if ( GetFieldLength("SYS","FELE",&llFieldLength) == RC_SUCCESS )
			pllAddFieldLens[2] = llFieldLength;
		else
		{
			dbg(TRACE,"Init_profnd: GetFieldLength for SYS.FELE failed <%d>",ilRC);
			pllAddFieldLens[2] = 8;
		}

		ilRC = SetAATArrayInfo( "LENEVAL", "TANA,FINA,FELE", pllAddFieldLens, 
						        "IDXLEN", "TANA,FINA", "A,A", &rgFeleEvalArr );
		if (ilRC != RC_SUCCESS)                                             
			dbg(TRACE,"Init_profnd: SetAATArrayInfo for LENEVAL failed <%d>",ilRC);
	}*/
	 
	if (ilRC == RC_SUCCESS)                                        
	{                                                              
		ilRC = SendFieldListToAll(cgFieldListDest) ;              
		if (ilRC != RC_SUCCESS)                                       
			dbg (TRACE,"Init_profnd: SendFieldListToAll failed") ;       
		else                                                          
			dbg(TRACE,"Init_profnd: SendFieldListToAll OK") ;            
	} /* end of if */                                              

    return(ilRC);
	
} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRC = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");
	
	return ilRC;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	/* unset SIGCHLD ! DB-Child will terminate ! */
	logoff();
	dbg(TRACE,"Terminate: now leaving ...");
	fclose(outp);

	sleep (ipSleep);
	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
#if 0
static void HandleSignal(int pipSig)
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);
	switch(pipSig)
	{
	default	:
		Terminate(0);
		break;
	} /* end of switch */
	exit(0);
	
} /* end of HandleSignal */
#endif

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	
		if( ilRC == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRC != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRC);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(0);
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				Terminate(0);
				break;
						
			case	RESET		:
				ilRC = Reset();
				break;
						
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRC);
		} /* end else */
	} while (ilBreakOut == FALSE);
	if(igInitOK == FALSE)
	{
			ilRC = Init_profnd();
			if(ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"Init_profnd: init failed!");
			} /* end of if */
			else
			    igInitOK = TRUE;
	}/* end of if */
	/* OpenConnection(); */
} /* end of HandleQueues */
	

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData()
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	
	BC_HEAD *prlBchd      = NULL;
	CMDBLK  *prlCmdblk    = NULL;
	char    *pclSelKey    = NULL;
	char    *pclFields    = NULL;
	char    *pclData      = NULL;
	char    pclHost[16]  = "";
	char    pclHopo[16]  = "";
	char    pclTable[16]  = "";
	char    pclProUrno[16]  = "";
	char    pclActCmd[16] = "";
	char    *pclRueUrnos     = NULL;
	long    llEventType;
	char	*pclPartialUrues=0;
	int	ilRuleCount=0, ilGoOn=0;

	 dbg(DEBUG,"================= HandleData EVENT BEGIN ================");

	/* Queue-id of event sender */
	igQueOut = prgEvent->originator;

	/* get broadcast header */
	prlBchd = (BC_HEAD *)((char *)prgEvent + sizeof(EVENT));

	/* Save Global Elements of BC_HEAD */
	memset(pcgRecvName, '\0', (sizeof(prlBchd->recv_name)+1));
	strcpy(pclHost,prlBchd->orig_name);                      /* Host        */
	strcpy(pcgDestName,prlBchd->dest_name);                  /* UserLoginName */
	strncpy(pcgRecvName, prlBchd->recv_name,
		  sizeof(prlBchd->recv_name));                       /* Workstation   */

	/* get command block  */
	prlCmdblk = (CMDBLK *)((char *)prlBchd->data);

	/* Save Global Elements of CMDBLK */
	strcpy(pcgTwStart,prlCmdblk->tw_start);
	strcpy(pcgTwEnd,prlCmdblk->tw_end);

	/*  3 Strings in CMDBLK -> data  */   
	pclSelKey = (char *)prlCmdblk->data;

	pclFields = (char *)pclSelKey + strlen(pclSelKey) + 1;
	pclData = (char *)pclFields + strlen(pclFields) + 1;
	pclRueUrnos  = pclData + strlen(pclData) + 1;
	
	pcgFieldList = pclFields;


	strcpy (pclTable, prlCmdblk->obj_name);                     /*  table name  */
	strcpy (pclActCmd, prlCmdblk->command);                 /*  actual command  */
	
	if ( GetDataItem (pclHopo, pcgTwEnd, 1, ',', "", "\0") < 3 )
	{
		dbg( DEBUG, "Didn't find hopo in event, using global Hopo from SGS.TAB" );
		strcpy ( pclHopo, cgHopo );	
	}
	else
		dbg( DEBUG, "Found hopo <%s> in event", pclHopo );
	if ( igExtendedTracing )
	{
		dbg(DEBUG," HANDLE COMMAND");
		dbg(DEBUG,"TWEHDL (mod_id = %d) GET FOLLOWING EVENT:", mod_id);
		DebugPrintEvent (DEBUG, prgEvent);
		dbg(DEBUG,"FROM ID<%d> HOST<%s>", igQueOut, pclHost);
		dbg(DEBUG,"WKS<%s> USER<%s>", pcgRecvName, pcgDestName);
		dbg(DEBUG,"CMD <%s>  TBL <%s>", pclActCmd,pclTable);
		dbg(DEBUG,"SELECT <%s>", pclSelKey);
		dbg(DEBUG,"FIELDS <%s>", pclFields);
		dbg(DEBUG,"DATA   <%s>", pclData);
		dbg(DEBUG,"SPECIAL INFO <%s>",pcgTwEnd);
		dbg(DEBUG,"TWSTART <%s>",pcgTwStart);
	}
	/*  COMPARE COMAND AND GOTO FUNCTION */

	if (!strcmp(pclActCmd,"IFR" )||!strcmp(pclActCmd,"UFR") )			/*  IFR,UFR  */
	{
		/* command IFR or UFR found */
		dbg(TRACE,"command IFR or UFR found ");

		/*  free memory */
		if ( pcgInbound )
			free ( pcgInbound );
		pcgInbound = 0;
		if ( pcgOutbound )
			free ( pcgOutbound );
		pcgOutbound = 0;

		ilRC = GetEventType(&llEventType,&pcgInbound,&pcgOutbound,pclData);
		dbg(TRACE, "GetEventType RC <%d> EventType <%ld>", ilRC, llEventType );
		dbg(DEBUG, "IFR/UFR Rule-Urnos: <%s>",  pclRueUrnos );
		dbg(DEBUG, "Inbound-Record: <%s>", pcgInbound );
		dbg(DEBUG, "Outbound-Record: <%s>", pcgOutbound );
		if ( ( ilRC == RC_SUCCESS ) && 
			 ( (llEventType==EVT_OUTBOUND) || (llEventType==EVT_TURNAROUND) ) &&
			 pcgOutbound && pcgOutbound[0] )	/*  Event mit Outbound-Flug ?*/
		{
		 	ilRC = IsFlightToCalc ( cgHopo, pcgOutbound, &ilGoOn );
			if ( ( ilRC==RC_SUCCESS ) && ilGoOn )
			{ 
				pclPartialUrues = calloc(1,strlen(pclRueUrnos)+1 );
				ilRC = GetPartialUrues( pclRueUrnos, pclPartialUrues, &ilRuleCount );
				if ( ilRC == RC_SUCCESS )
					dbg(DEBUG, "GetPartialUrues: Count <%d> Urnos <%s>", 
								ilRuleCount, pclPartialUrues );
				else
					dbg(TRACE, "GetPartialUrues failed." );
				if ( ( ilRC == RC_SUCCESS ) && (ilRuleCount>=1) )
				{	/* There is at least one Single-Rule that contains partial duty */
					/* requirements for CheckIn. So we have to find a profile		*/
					ilRC = ProcessProfiles(pclHopo, pclProUrno);
					dbg(DEBUG, "ProcessProfiles with Hopo <%s> returned <%d> found Urno <%s>", pclHopo, ilRC, pclProUrno );
				}
			}
			ilRC = ForwardEvent ( igDest, pclProUrno, pclPartialUrues );
			if ( ilRC != RC_SUCCESS ) 
				dbg(TRACE, "ForwardEvent with ProUrno <%s> Urues <%s> to MOD-ID <%s> failed. RC <%d>", 
							 pclProUrno, pclPartialUrues, igDest, ilRC );
		}	
	}
	else 
		if  ( !strcmp(pclActCmd,"IRT") || !strcmp(pclActCmd,"URT") ||
		      !strcmp(pclActCmd,"DRT") )	/*  IRT,URT,DRT  */
		{
			ilRC = CEDAArrayEventUpdate ( prgEvent ); 
			dbg(TRACE,"command IRT, URT or DRT processed, table<%s> ilRC <%ld>", 
				pclTable, ilRC );
		}
		else if ( !strcmp(pclActCmd,"SFL") )							/*  SFL      */
		{
            dbg (DEBUG, "HandleData: SFL") ;                       
            ilRC = SendFieldListToOne(prgEvent->originator, pclHopo );        
            if (ilRC != RC_SUCCESS)                                 
            	dbg(TRACE,"HandleData: SendFieldListToOne Dest=%ld failed <%d>",
					prgEvent->originator, ilRC ) ;   
		}
		else if ( !strcmp(pclActCmd,"XXX") )							/*  XXX      */
		{
            dbg (TRACE, "HandleData: XXX") ;                       
			if ( pclData && ( strlen ( pclData ) > 0 ) )
			{
				ilRC = CheckRuleUrno ( pclData );
				if (ilRC != RC_SUCCESS)                                 
			    	dbg(TRACE,"HandleData: CheckRuleUrno Urno=%s failed <%d>", pclData, ilRC );
				
			}
		}
		else if ( !strcmp(pclActCmd,"DFR") )						/*  DFR   */
		{
            dbg (TRACE, "HandleData: DFR") ;                       
			/*  free memory */
			if ( pcgInbound )
				free ( pcgInbound );
			pcgInbound = 0;
			if ( pcgOutbound )
				free ( pcgOutbound );
			pcgOutbound = 0;

			/*ilRC = GetEventType(&llEventType,&pcgInbound,&pcgOutbound,pclData);
			dbg(TRACE, "GetEventType RC <%d> EventType <%ld>", ilRC, llEventType );
			dbg(DEBUG, "IFR/UFR Rule-Urnos: <%s>",  pclRueUrnos );
			dbg(DEBUG, "Inbound-Record: <%s>", pcgInbound );
			dbg(DEBUG, "Outbound-Record: <%s>", pcgOutbound );
			if ( ( ilRC == RC_SUCCESS ) && 
				 ( (llEventType==EVT_OUTBOUND) || (llEventType==EVT_TURNAROUND) ) &&
				 pcgOutbound && pcgOutbound[0] ) *  Event mit Outbound-Flug ?*
			{*/
				ilRC = ForwardEvent ( igDest, NULL, NULL );
				if ( ilRC != RC_SUCCESS ) 
					dbg(TRACE,
						"ForwardEvent DFR to MOD-ID <%s> failed. RC <%d>", 
						pclProUrno, igDest, ilRC );
			/*}*/
		}
		else if (!strcmp(pclActCmd,"CFL"))					/* CFL */	/*rna*/
		{																/*rna*/
            dbg (TRACE, "HandleData: CFL") ;							/*rna*/
			ilRC = ForwardEvent( DISCAL_ID, NULL, NULL );				/*rna*/
			if ( ilRC != RC_SUCCESS ) 									/*rna*/
				dbg(TRACE,"ForwardEvent to discal failed. RC <%d>",ilRC);/*rna*/
		}																/*rna*/
		else if (!strcmp(pclActCmd,"ANSWER"))			
		{
			dbg(TRACE, "ANSWER-event arrived");
		}
		else
			dbg(TRACE, "unknown command <%s> found", pclActCmd ); 

	/* free memory */
	if ( pclPartialUrues )
		free (pclPartialUrues);
	pclPartialUrues = 0;

	return ilRC;
	
} /* end of HandleData */

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, char *pcpArrayFieldList,char *pcpAddFields,long *pcpAddFieldLens,char *pcpSelection, ARRAYINFO *prpArrayInfo)
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	char clTable[21];

	if(prpArrayInfo == NULL)
	{
		dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
		ilRc = RC_FAIL;
	}
	else
	{
		memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

		prpArrayInfo->rrArrayHandle = -1;
		prpArrayInfo->rrIdx01Handle = -1;

		if(pcpArrayName != NULL)
		{
			if(strlen(pcpArrayName) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crArrayName,pcpArrayName);
			}/* end of if */
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);
		if (pcpAddFields != NULL)
		{
			prpArrayInfo->lrArrayFieldCnt += get_no_of_items(pcpAddFields);
		}

		prpArrayInfo->plrArrayFieldOfs = (long*) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldOfs == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",
				prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->plrArrayFieldLen = (long*) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldLen == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",
				prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}
		else
		{
			*(prpArrayInfo->plrArrayFieldLen) = -1;
		}/* end of if */
	}/* end of if */

	if(pcpTableName == NULL)
	{
		dbg(TRACE,"SetArrayInfo: invalid parameter TableName: null pointer not valid");
		ilRc = RC_FAIL;
	}
	else
	{
		strcpy ( clTable,pcpTableName );
		strcat ( clTable,cgTabEnd );
       	dbg(TRACE,"SetArrayInfo: clTable=<%s>",clTable);
    }
   	if(ilRc == RC_SUCCESS)
   	{
       	ilRc = GetRowLength(clTable,pcpArrayFieldList,&(prpArrayInfo->lrArrayRowLen));
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
      	}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->lrArrayRowLen+=100;
		prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+10));
   		if(prpArrayInfo->pcrArrayRowBuf == NULL)
   		{
      			ilRc = RC_FAIL;
      			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */


	if(ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"CEDAArrayCreate follows Array <%s> Table <%s>",pcpArrayName,clTable);
		ilRc = CEDAArrayCreate(&(prpArrayInfo->rrArrayHandle),
				pcpArrayName,clTable,pcpSelection,pcpAddFields,
				pcpAddFieldLens,pcpArrayFieldList,
				&(prpArrayInfo->plrArrayFieldLen[0]),
				&(prpArrayInfo->plrArrayFieldOfs[0]));

		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */

	if(pcpArrayFieldList != NULL)
	{
		if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
		{
			strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
		}/* end of if */
	}/* end of if */
	if(pcpAddFields != NULL)
	{
		if(strlen(pcpAddFields) + 
			strlen(prpArrayInfo->crArrayFieldList) <= 
					(size_t)ARR_FLDLST_LEN)
		{
			strcat(prpArrayInfo->crArrayFieldList,",");
			strcat(prpArrayInfo->crArrayFieldList,pcpAddFields);
			dbg(TRACE,"<%s>",prpArrayInfo->crArrayFieldList);
		}/* end of if */
	}/* end of if */
/*****************************/
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayFill(&(prpArrayInfo->rrArrayHandle),pcpArrayName,NULL);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayFill failed <%d>",ilRc);
		}
	}
/*******************/
	if ( (ilRc == RC_SUCCESS) && strstr(pcpArrayFieldList,"URNO") )
	{
		strcpy ( prpArrayInfo->crIdxUrnoName, "URNOIDX" );
		ilRc = CEDAArrayCreateSimpleIndexDown(&(prpArrayInfo->rrArrayHandle),
											  prpArrayInfo->crArrayName,
											  &(prpArrayInfo->rrIdxUrnoHandle), 
											  prpArrayInfo->crIdxUrnoName, "URNO" );
		if(ilRc != RC_SUCCESS)                                             
		{
			dbg( TRACE,"Init_profnd: CEDAArrayCreateSimpleIndexDown for %s failed <%d>",
				 clTable, ilRc);		
		}
	}
	else
		prpArrayInfo->rrIdxUrnoHandle = -1;
	if (ilRc == RC_SUCCESS)
	{                                          /* send update msg. for active changes */ 
		ilRc = ConfigureAction(clTable,0,0) ;
		if (ilRc != RC_SUCCESS)
			dbg (TRACE, "SetArrayInfo: ConfigureAction table<%s> failed <%d>", clTable, ilRc) ;
		else
			dbg (TRACE, "SetArrayInfo: ConfigureAction table<%s> succeeded.", clTable ) ; 
	}/* end of if */


	{
		int ilLc;
		for(ilLc = 0; ilLc < prpArrayInfo->lrArrayFieldCnt; ilLc++)
		{
			dbg(TRACE,"%02d %d",ilLc,prpArrayInfo->plrArrayFieldOfs[ilLc]);
		}
	}
	return(ilRc);
}


/******************************************************************************/
/******************************************************************************/
static int GetRowLength(char *pcpTana, char *pcpFieldList, long *plpLen)
{
	int	 ilRc = RC_SUCCESS;			/* Return code */
	int  ilNoOfItems = 0;
	int  ilLoop = 0;
	long llFldLen = 0;
	long llRowLen = 0;
	char clFina[8];
	
	ilNoOfItems = get_no_of_items(pcpFieldList);

	ilLoop = 1;
	do
	{
		ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
		if(ilRc > 0)
		{
			ilRc = GetFieldLength(pcpTana,clFina,&llFldLen);
			if(ilRc == RC_SUCCESS)
			{
				llRowLen++;
				llRowLen += llFldLen;
			}/* end of if */
		}/* end of if */
		ilLoop++;
	}while((ilRc == RC_SUCCESS) && (ilLoop <= ilNoOfItems));

	if(ilRc == RC_SUCCESS)
	{
		*plpLen = llRowLen;
	}/* end of if */

	return(ilRc);
	
} /* end of GetRowLength */



/******************************************************************************/
/******************************************************************************/

static int SendFieldListToOne( int ipDest, char *pcpHopo )
{
	int       ilRC        = RC_SUCCESS;            /* Return code */
	char      clTabName[8];
	char      clFldLst[64];
	char      clLastItem[8];
	int       ilNoOfItems = 0;
	long      llRowLen    = 0;
	long      llRowCnt    = 0;
	long      llResLen    = 0;
	char    *pclResult    = NULL;
	char    *pclTmpDst    = NULL;
	int       ilEventLen  = 0;
	EVENT   *prlEvent;
	BC_HEAD *prlBchead    = NULL;
	CMDBLK  *prlCmdblk    = NULL;
	char    *pclSelection = NULL;
	char    *pclFields    = NULL;
	char    *pclData      = NULL;
	long      llFunc      = ARR_FIRST;
	long      llFldCnt    = 0;
	long	*pllFldPos    = NULL;
	int		 ilPrefLen=5;
	char	clKey[21];
	char	*pclEvtd =0, *pclNext=0;


	dbg(DEBUG,"SendFieldListToOne: dest <%d>",ipDest);

	sprintf(clTabName,"TPL%s", cgTabEnd );
	sprintf(clFldLst,"FLDD");

    if(ilRC == RC_SUCCESS)
    {
		ilRC = CEDAArrayGetRowCount(&(rgTplArray.rrArrayHandle),&(rgTplArray.crArrayName[0]),
									&llRowCnt);
    	if(ilRC != RC_SUCCESS)
    	{
			dbg(TRACE,
				"SendFieldListToOne: CEDAArrayGetRowCount failed <%d> handle <%d> array <%s>",
			    ilRC,rgTplArray.rrArrayHandle, rgTplArray.crArrayName );
    	}/* end of if */
    }/* end of if */

    if(ilRC == RC_SUCCESS)
    {
        ilRC =	GetRowLength (clTabName, clFldLst, &llRowLen);
        if(ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"SendFieldListToOne: GetRowLength failed <%s> <%s>",
				clTabName, clFldLst);
        }/* end of if */
    }/* end of if */

    if(ilRC == RC_SUCCESS)
    {
    	llResLen = llRowCnt * llRowLen;
		ilEventLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
					 strlen(&cgAddFields[0]) + 1 + llResLen + 1;

		prlEvent = (EVENT *) calloc(1,ilEventLen);
   		if(prlEvent == NULL)
   		{
       		ilRC = RC_FAIL;
       		dbg(TRACE,"SendFieldListToOne: calloc failed");
		}
		else
		{
			prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT));
			prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
			pclSelection = prlCmdblk->data;
			pclFields    = pclSelection + strlen(pclSelection) + 1;
   		}/* end of if */
    }/* end of if */

    if(ilRC == RC_SUCCESS)
    {
		llFldCnt = get_no_of_items(clFldLst);
		pllFldPos = (long *) calloc(llFldCnt,sizeof(long));
    	if(pllFldPos == NULL)
    	{
       		ilRC = RC_FAIL;
       		dbg(TRACE,"SendFieldListToOne: pllFldPos calloc(%d,%d) failed",llFldCnt,sizeof(long));
		}
		else
			*pllFldPos = -1;
    }/* end of if */

    if(ilRC == RC_SUCCESS)
    {
		sprintf ( clKey, "1,%s", pcpHopo );
		llFunc = ARR_FIRST;
		pclTmpDst = pclFields;
	
		sprintf( pclTmpDst,"%s", cgAddFields );
		pclTmpDst += strlen ( pclTmpDst );
        ilPrefLen = strlen ( ".AFT." );   	
		do
		{
			ilRC = CEDAArrayFindRowPointer (&(rgProArray.rrArrayHandle),
											rgProArray.crArrayName, 
											&rgProArray.rrIdx01Handle, 
											"IDXPRO1", clKey, &llFunc, 
											(void*) &pclResult );
			if(ilRC == RC_SUCCESS)
        	{
				pclEvtd = pclResult + rgProArray.plrArrayFieldOfs[5];
            	dbg(DEBUG,"SendFieldListToOne: Found Evtd <%s>", pclEvtd );  
				pclNext = pclEvtd;
				while ( pclNext =  strstr ( pclNext, ".AFT.") )
				{
					pclNext += ilPrefLen;
					if ( strlen(pclNext)>= FIELD_LEN )
					{
						strncpy ( clLastItem, pclNext, FIELD_LEN);
						clLastItem[FIELD_LEN] = '\0';
						TrimRight ( clLastItem );
						if ( !strstr ( pclFields, clLastItem ) )
						{	/* add field, if not yet in pclFields */
							sprintf ( pclTmpDst, ",%s", clLastItem );
							pclTmpDst += strlen ( pclTmpDst );
						}
					}
				}
				llFunc = ARR_NEXT;
			}
			else
			{
				if(ilRC != RC_NOTFOUND)
				{
					dbg(TRACE,"SendFieldListToOne: CEDAArrayGetFields failed <%d> ",ilRC);
					dbg(TRACE,"    with fieldList=%s ",clFldLst );
    			}/* end of if */
    		}/* end of if */
		}while(ilRC == RC_SUCCESS);

		if(ilRC == RC_NOTFOUND)
			ilRC = RC_SUCCESS;		
		
		if(ilRC == RC_SUCCESS)
		{
			memset(pclTmpDst,0x00,FIELD_LEN+1);
	
			ilNoOfItems = get_no_of_items(pclFields);
           	dbg(DEBUG,"SendFieldListToOne: unsorted fields <%d> <%s>",ilNoOfItems,pclFields);
		
			qsort((void *)pclFields,ilNoOfItems-1,FIELD_LEN+1,MyCompare);
	
			ilNoOfItems = get_no_of_items(pclFields);
           	dbg(DEBUG,"SendFieldListToOne:   sorted fields <%d> <%s>",ilNoOfItems,pclFields);

			/*
           	pclTmpDst = pclFields;
           	pclTmpSrc = pclFields;
			strncpy(&clLastItem[0],pclTmpSrc,FIELD_LEN);
	
			pclTmpDst += FIELD_LEN+1;
			pclTmpSrc += FIELD_LEN+1;

			while(*pclTmpSrc != 0x00)
			{
				if(strncmp(clLastItem,pclTmpSrc,FIELD_LEN) != 0)
				{
					memcpy(pclTmpDst,pclTmpSrc,FIELD_LEN+1);
					strncpy(&clLastItem[0],pclTmpSrc,FIELD_LEN);
					pclTmpDst += FIELD_LEN+1;
				}
				pclTmpSrc += FIELD_LEN+1;
			}

			pclTmpDst--;
			memset(pclTmpDst,0x00,FIELD_LEN+1);

			ilNoOfItems = get_no_of_items(pclFields);
			*/
           	dbg(DEBUG,"SendFieldListToOne:    uniqe fields <%d> <%s>",ilNoOfItems,pclFields);

			pclData = pclFields + strlen(pclFields) + 1;
			*pclData = 0x00;
				
			prlEvent->type = SYS_EVENT;
			prlEvent->command = EVENT_DATA;
			prlEvent->originator = mod_id;
			prlEvent->retry_count = 0;
			prlEvent->data_offset = sizeof(EVENT);
			prlEvent->data_length = ilEventLen - sizeof(EVENT);
			sprintf(prlEvent->sys_ID,"%s",mod_name);

			sprintf(prlBchead->dest_name,"%s",mod_name);
			sprintf(prlBchead->recv_name,"flight");

			sprintf(prlCmdblk->command,"PFL");
			sprintf(prlCmdblk->obj_name,"AFT");
			sprintf(prlCmdblk->tw_start,"RMS");
			sprintf(prlCmdblk->tw_end,"%s,TAB,INIT", pcpHopo );

			DebugPrintEvent(DEBUG,prlEvent);
			DebugPrintBchead(DEBUG,prlBchead);
			DebugPrintCmdblk(DEBUG,prlCmdblk);
			dbg(DEBUG,"selection <%s>",pclSelection);
			dbg(DEBUG,"fields    <%s>",pclFields);
			dbg(DEBUG,"data      <%s>",pclData);

			ilRC = que(QUE_PUT,ipDest,mod_id,PRIORITY_3,ilEventLen,(char *)prlEvent);
			if(ilRC != RC_SUCCESS)
			{
				HandleQueErr(ilRC);
				DebugPrintEvent(TRACE,prlEvent);
				DebugPrintBchead(TRACE,prlBchead);
				DebugPrintCmdblk(TRACE,prlCmdblk);
				dbg(TRACE,"selection <%s>",pclSelection);
				dbg(TRACE,"fields    <%s>",pclFields);
				dbg(TRACE,"data      <%s>",pclData);
       		}/* end of if */
			else
			{
				DebugPrintEvent(DEBUG,prlEvent);
				DebugPrintBchead(DEBUG,prlBchead);
				DebugPrintCmdblk(DEBUG,prlCmdblk);
				dbg(DEBUG,"selection <%s>",pclSelection);
				dbg(DEBUG,"fields    <%s>",pclFields);
				dbg(DEBUG,"data      <%s>",pclData);
			}
       	}/* end of if */
    }/* end of if */

	if(pllFldPos != NULL)
	{
		free(pllFldPos);
		pllFldPos = NULL;
	}/* end of if */

	if(prlEvent != NULL)
	{
		free(prlEvent);
		prlEvent = NULL;
	}/* end of if */

	return(ilRC);
	
} /* end of SendFieldListToOne */


/******************************************************************************/
/******************************************************************************/
static int  SendFieldListToAll(char *pcpModIds )
{
	int	 ilRC1 = RC_SUCCESS;
	int	 ilRC = RC_SUCCESS;
	int  ilLoop      = 0;
	int  ilNoOfItems = 0;
	int  ilDest      = 0;
	int  ilOk      = 0;
	char clTmpBuf[256];

	dbg(DEBUG,"SendFieldListToAll: pcpModIds <%s>",pcpModIds);

	ilNoOfItems = get_no_of_items(pcpModIds);

	for(ilLoop = 1 ; ilLoop <= ilNoOfItems ; ilLoop++)
	{
		ilRC1 = RC_FAIL;	/*  RC for each ilLoop */
		ilOk = get_real_item(clTmpBuf,pcpModIds,ilLoop);
		if(ilOk > 0)
		{
			if ( sscanf( clTmpBuf, "%d", &ilDest ) < 1 )
				dbg(TRACE,"SendFieldListToAll: Failed to extract Mod-Id from <%s>",clTmpBuf );
			else
			{	/* cgHopo or all hopos in TPLTAB ? */
				ilRC1 = SendFieldListToOne(ilDest, cgHopo );	
				if(ilRC1 != RC_SUCCESS)
					dbg(TRACE,"SendFieldListToAll: SendFieldListToOne failed <%d>",ilRC);
			}/* end of if */
		}
		else
			dbg(TRACE,"SendFieldListToAll: get_real_item failed <%d>", ilOk );

		ilRC |= ilRC1;
	}/* end of for */

	return(ilRC);
	
} /* end of SendFieldListToAll */



/******************************************************************************/
/******************************************************************************/
static int MyCompare(const void* ppP1, const void* ppP2)                             
{                                                                                    
    return(strcmp((char *)ppP1,(char *)ppP2));                                       
}/* end of MyCompare */                                                              
                                                                                     

/******************************************************************************/
/*	CheckRuleUrno: Check, whether rule contains partial demands for checkin	  */
/******************************************************************************/
static int CheckRuleUrno ( char *pcpRuleUrno )
{
	long llRow = ARR_FIRST ;
	int  ilRC;
	char	clRuleType[2]="", pclKey[21];
	char    *pclResult    = NULL;

	sprintf ( pclKey, "CIC,1,%s", pcpRuleUrno );
	/* strcpy ( rgRudArray.pcrIdx01RowBuf, pclKey ); */
	dbg(DEBUG,"CheckRuleUrno vor CEDAArrayFindKey Key <%s> IdxRowLen <%ld>",
			   pclKey, rgRudArray.lrIdx01RowLen );	
	/*ilRC = CEDAArrayFindKey( &(rgRudArray.rrArrayHandle), rgRudArray.crArrayName,
							 &(rgRudArray.rrIdx01Handle), "IDXRUD1", &llRow, 
							 rgRudArray.lrIdx01RowLen+1, 
							 rgRudArray.pcrIdx01RowBuf); doesn't work correctly*/
	ilRC = CEDAArrayFindRowPointer( &(rgRudArray.rrArrayHandle),  
									rgRudArray.crArrayName,
									 &(rgRudArray.rrIdx01Handle), "IDXRUD1",
                					 pclKey, &llRow, (void**)&pclResult ); 
	if ( ilRC==RC_SUCCESS )
	{
		dbg ( TRACE, "CheckRuleUrno, Rule <%s> has partial demands, RUD-Row <%ld>", 
						pcpRuleUrno, llRow );
		ilRC = GetFieldByUrno ( &rgRueArray, pcpRuleUrno, "RUTY", clRuleType, 2 );
		if ( (ilRC==RC_SUCCESS ) && ( clRuleType[0]!='0' ) )	/* No Single Rule ? */
		{
			dbg ( TRACE, "CheckRuleUrno: Rule <%s> no single Rule RUTY <%s>", 
			      pcpRuleUrno, clRuleType );
			ilRC = RC_FAIL;
		}
	}
	else
		dbg ( TRACE, "CheckRuleUrno, Rule <%s> has no partial demands.", pcpRuleUrno );
	return ilRC;
}


static int GetFieldByUrno ( ARRAYINFO *prpArray, char *pcpUrno, char *pcpField, 
							  char *pcpResult, int ipMaxBytes )
{
	int		ilRC1 = RC_FAIL;	
	int     ilRC = RC_FAIL;			/* Return code			*/
	long llRow = ARR_FIRST ;
	long    llFieldNr = -1;
	char *pclResult = 0;

	if ( prpArray && pcpUrno )
	{
		ilRC1 = CEDAArrayFindRowPointer( &(prpArray->rrArrayHandle), 
										prpArray->crArrayName,
										&(prpArray->rrIdxUrnoHandle), 
										prpArray->crIdxUrnoName, pcpUrno, 
										&llRow, (void**)&pclResult ); 
		dbg ( DEBUG, "GetFieldByUrno Urno <%s> llRow <%ld> RC <%d>", 
				  pcpUrno, llRow, ilRC1 );  
		if ( ilRC1 == RC_SUCCESS )
			ilRC = GetCEDAField ( &(prpArray->rrArrayHandle), 
								  prpArray->crArrayName, &llFieldNr, pcpField,
								  ipMaxBytes, llRow, pcpResult, TRUE );
		if ( ilRC == RC_SUCCESS )
			dbg(TRACE, "GetFieldByUrno Urno <%s>  Field <%s>: data <%s>", 
						pcpField, pcpUrno, pcpResult );
		else
			dbg(TRACE, "GetFieldByUrno Urno <%s> Field <%s> : RC1,RC <%d  %d>",
						pcpField, pcpUrno, ilRC1, ilRC );
	}
	return ilRC;
}
 
static int CreateIdx1 ( ARRAYINFO *prpArrayInfo, char *pcpIdxName,
						char *pcpFields, char *pcpOrdering )
{
	int		ilRC = RC_FAIL, ilLoop;
	char clOrderTxt[4];
	long	*pllOrdering=0;

	if ( !pcpFields )
		return ilRC;		/*  0-Pointer in parameter pcpFields */

	strcpy ( prpArrayInfo->crIdx01Name, pcpIdxName );
	strcpy ( prpArrayInfo->crIdx01FieldList, pcpFields );
	
    ilRC = GetRowLength( prpArrayInfo->crArrayName, pcpFields, 
						 &(prpArrayInfo->lrIdx01RowLen));
    
	if(ilRC != RC_SUCCESS)
        dbg(TRACE,"CreateIdx1: GetRowLength failed <%s> <%s>",
			prpArrayInfo->crArrayName, pcpFields);
	else
    {
		prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,
									   prpArrayInfo->lrIdx01RowLen+1);
   		if( prpArrayInfo->pcrIdx01RowBuf == NULL )
   		{
      		ilRC = RC_FAIL;
      		dbg(TRACE,"CreateIdx1: calloc failed");
   		}/* end of if */
   	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		prpArrayInfo->lrIdx01FieldCnt = get_no_of_items(pcpFields);
		pllOrdering = (long *) calloc(prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
		if(!pllOrdering )
		{
			dbg(TRACE,"SetArrayInfo: pllOrdering calloc(%d,%d) failed",prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
			ilRC = RC_FAIL;
		}
		else
		{
			for(ilLoop=0; ilLoop < prpArrayInfo->lrIdx01FieldCnt; ilLoop++)
			{
				if ( !pcpOrdering ||
					( get_real_item(clOrderTxt,pcpOrdering,ilLoop+1)
																!= RC_SUCCESS ))
					clOrderTxt[0] = 'D';
				if ( clOrderTxt[0] == 'A' )
					pllOrdering[ilLoop] = ARR_ASC;
				else
					pllOrdering[ilLoop] = ARR_DESC;
			}/* end of for */
		}/* end of else */
	}/* end of if */

	ilRC = CEDAArrayCreateMultiIndex( &(prpArrayInfo->rrArrayHandle), 
									  prpArrayInfo->crArrayName,
									  &(prpArrayInfo->rrIdx01Handle), 
									  prpArrayInfo->crIdx01Name,
									  prpArrayInfo->crIdx01FieldList, 
									  pllOrdering );
	if ( pllOrdering )
		free (pllOrdering);
	pllOrdering = 0;
	return ilRC;									
}


static int GetRefTabField ( char *pcpBtab, char *pcpBfld, 
						    char *pcpRtab, char *pcpRfld )
{
	int		ilRC1 = RC_FAIL;	
	int     ilRC = RC_FAIL;			/* Return code			*/
	long	llRow = ARR_FIRST ;
	long    llFieldRtab = -1;
	long    llFieldRfld = -1;
	char	*pclResult = 0;
	char	pclKey[41];

	if ( pcpBtab && pcpBfld && pcpRtab && pcpRfld )
	{
		sprintf ( pclKey, "%s,%s", pcpBfld, pcpBtab );
		ilRC1 = CEDAArrayFindRowPointer( &(rgTsrArray.rrArrayHandle), 
										rgTsrArray.crArrayName,
										&(rgTsrArray.rrIdx01Handle), "IDXTSR1",
										pclKey, 
										&llRow, (void**)&pclResult ); 
		dbg ( DEBUG, "GetRefTabField Bfld <%s> Btab <%s> llRow <%ld> RC <%d>", 
			  pcpBfld, pcpBtab, llRow, ilRC1 );  
		if ( ilRC1 == RC_SUCCESS )
		{
			ilRC = GetCEDAField ( &(rgTsrArray.rrArrayHandle), 
								  rgTsrArray.crArrayName, &llFieldRtab, 
								  "RTAB", 11, llRow, pcpRtab, TRUE );
			ilRC |= GetCEDAField ( &(rgTsrArray.rrArrayHandle), 
								   rgTsrArray.crArrayName, &llFieldRfld, 
								   "RFLD", 11, llRow, pcpRfld, TRUE );
			if ( ilRC == RC_SUCCESS )
			{
				dbg(DEBUG, "GetRefTabField Rfld <%s> Rtab <%s>", pcpRfld, pcpRtab );
				if ( ( strlen(pcpRtab) == 0 ) || ( strlen(pcpRfld) == 0 ) )
					ilRC = RC_NOT_FOUND;  /* Fields not filled */
			}
		}
		else
			dbg(TRACE, "GetRefTabField: CEDAArrayGetField failed : RC <%d>",
					ilRC1 );
	}
	return ilRC;	
}
 

/*  GetEventType: Analyse pcpData, Split into pcpInbound, pcpOutbound and 	*/
/*		  deliver plpEventType						*/
static int GetEventType(long *plpEventType,char **pcpInbound, char **pcpOutbound,char *pcpData)
{
	int  ilRc = RC_SUCCESS;			/* Return code */
	char clDel = '\n';
	char *pclTmp1 = NULL;
	
	if(ilRc == RC_SUCCESS)
	{
		pclTmp1 = strchr(pcpData,clDel);
		if(pclTmp1!= NULL)
		{
			*pclTmp1 = 0x00;
	
			*pcpInbound = strdup(pcpData);
			if(*pcpInbound == NULL)
			{
				dbg(TRACE,"GetEventType: strdup - calloc failed");
				ilRc = RC_FAIL;
			}/* end of if*/

			*pclTmp1 = clDel;
		}
		else
		{
			dbg(TRACE,"GetEventType: strchr <0x%x> not found in <%s>",clDel,pcpData);
			ilRc = RC_FAIL;
		}/* end of if*/
	}/* end of if*/

	
	if(ilRc == RC_SUCCESS)
	{
		pclTmp1++;
		*pcpOutbound = strdup(pclTmp1);
		if(*pcpOutbound == NULL)
		{
			dbg(TRACE,"GetEventType: strdup - calloc failed");
			ilRc = RC_FAIL;
		}/* end of if*/
	}/* end of if*/

	/* dbg(TRACE,"GetEventType:  inbound <%x><%s>",pcpInbound,*pcpInbound);
	dbg(TRACE,"GetEventType: outbound <%x><%s>",pcpOutbound,*pcpOutbound); */

	*plpEventType = EVT_UNKNOWN;

	if(strlen(*pcpInbound) == 0)
	{
		if(strlen(*pcpOutbound) == 0)
		{
			dbg(TRACE,"GetEventType: inbound and outbound empty");
			*plpEventType = EVT_UNKNOWN;
			ilRc = RC_FAIL;
		}
		else
		{
			*plpEventType = EVT_OUTBOUND;
			ilRc = RC_SUCCESS;
		}/* end of if */
	}
	else
	{
		if(strlen(*pcpOutbound) == 0)
		{
			*plpEventType = EVT_INBOUND;
			ilRc = RC_SUCCESS;
		}
		else
		{
			*plpEventType = EVT_TURNAROUND;
			ilRc = RC_SUCCESS;
		}/* end of if */
	}/* end of if */

	return(ilRc);
	
} /* end of GetEventType */



/******************************************************************************
	GetPartialUrues
	IN:		pcpRueUrnos: String with Rule-Urnos from UFR/IFR command
	OUT:	pcpPartUrue: list (comma-sep.) of Rule-Urnos (out of pcpRueUrnos )
						 containing partial-demands
			pipCount:	 Pointer to No. of found Rule-Urnos
*******************************************************************************/

static int GetPartialUrues( char *pcpRueUrnos, char *pcpPartUrue, int *pipCount )
{

	int		ilRc = RC_SUCCESS, ilRC2 ;
	int     ilLC = 0; /* Loop Count for inner loop */
	int		ilRueLen, ilDL;
	char	*pclPos1, *pclPos2;
	char	pclBuffer[128], pclUrno[64], pclFisu[21];

	/* initialize RUE-Urnos */
	if ( !pcpPartUrue )
		return RC_FAIL;

	pcpPartUrue[0] = '\0';
	pclPos1 = pcpRueUrnos;
	*pipCount = 0;
	do
	{
		ilRueLen = GetNextDataItem(pclBuffer, &pclPos1,&cgStx[0],"","\0\0");
		dbg(TRACE,"ilRueLen = %d,Urnos: <%s> Remaining Text <%s>",
				ilRueLen,pclBuffer,pclPos1);
		if (ilRueLen  > 0)
		{	
			pclPos2 = pclBuffer;
			for (ilLC = IDX_INBOUND; ilLC <= IDX_OUTBOUND; ilLC++)
			{
				/* we've got RUE-Urnos for one template */
				ilDL = GetNextDataItem(pclUrno, &pclPos2, &cgSoh[0],""," \0\0");
				ilDL += GetNextDataItem(pclFisu, &pclPos2, &cgSoh[0],""," \0\0"); 
				if ( ( ilDL >= 1 ) && ( ilLC==IDX_OUTBOUND ) )
				{	/*  Common Checkin is only interested in outbound rules */
					ilRC2 = CheckRuleUrno ( pclUrno );
					if ( ilRC2 == RC_SUCCESS )	
					{	/* Rule pclUrno does contain partial duty requirements */
						strcat ( pcpPartUrue, pclUrno );
						TrimRight(pcpPartUrue );
						strcat ( pcpPartUrue, "," );
						(*pipCount) ++;
					}
				}
			}
		}
	} while (ilRueLen > 0);
	if ( pcpPartUrue[0] )  /* remove last comma in result */
		pcpPartUrue[strlen(pcpPartUrue)-1] = '\0'; 
	return ilRc;
}


/******************************************************************************/
/******************************************************************************/
static int ProcessProfiles(char *pcpHopo, char *pcpUrno )
{
	int     ilRC            = RC_SUCCESS ;  /* Return code */
	long    llRow          = ARR_FIRST;
	char	clKey[41];
	long    llRowCnt        = 0;
	char   *pclResult       = NULL; /* READ ONLY !!! */
	char   *pclEvtd         = NULL; /* READ ONLY !!! */
	char   *pclPrio = 0;
	char   *pclName = 0;
	char   *pclUtpl = 0;
	char   *pclUrno = 0;
	char   *pclLastPrio = 0;
	int		ilBreakOut      = FALSE;
	long    llFoundRow	= -1;
	int		ilEval = FALSE;
 	
	if ( !pcpHopo || !pcpUrno )
	{
		dbg ( TRACE, "ProcessProfiles: At least one parameter = NULL" ); 
		return RC_FAIL;
	}
	pcpUrno[0] = '\0';
  	sprintf ( clKey, "1,%s", pcpHopo );
	dbg ( DEBUG, "ProcessProfiles with Pattern <%s>", clKey );
	do
	{
		ilRC = CEDAArrayFindRowPointer (&(rgProArray.rrArrayHandle),
                    &(rgProArray.crArrayName[0]), &(rgProArray.rrIdx01Handle),
                    "IDXPRO1", clKey, &llRow, (void *) &pclResult) ;

	    if (ilRC != RC_SUCCESS)
			dbg ( DEBUG,"PP: PROTAB no >> ROW! ilRC = <%ld>", ilRC ) ; 
		if (ilRC == RC_SUCCESS)
	    {  
			llRowCnt++;
			/* llCheckRule = TRUE; */
			llRow = ARR_NEXT;
     
			pclEvtd = pclResult + rgProArray.plrArrayFieldOfs[5];
			pclPrio = pclResult + rgProArray.plrArrayFieldOfs[14]; 
			pclUtpl = pclResult + rgProArray.plrArrayFieldOfs[21]; 
			pclUrno = pclResult + rgProArray.plrArrayFieldOfs[18];
  			pclName = pclResult + rgProArray.plrArrayFieldOfs[13]; 


			if ( pclLastPrio && strcmp (pclLastPrio, pclPrio) )
			{
				dbg (DEBUG, "PP: breakout prio changed, prio <%s>", pclPrio) ;
				ilBreakOut = TRUE;	/* we already found a Profile, actual Profile has different PRIO -> finished */
			}
			else
			{
				ilRC = CheckProfileCalendar(pcpHopo, pclUrno, &ilEval) ;
				if  ( (ilRC==RC_SUCCESS) && ilEval )	/* Validity evaluated to true */
					ilRC = ProcessConditions ( pcpHopo, pclEvtd, pclUtpl, &ilEval );
				else
				{	/* Profile not valid, continue with next profile */
					ilRC = RC_SUCCESS;
					continue;
				}

				if  ( (ilRC==RC_SUCCESS) && ilEval )	/* Conditions evaluated to true */
				{
					dbg ( TRACE, "PP: Profile <%s> evaluated to true. ", pclName );
					if ( !pclLastPrio )
					{
						pclLastPrio = strdup ( pclPrio );
						llFoundRow = llRow;
						strncpy ( pcpUrno, pclUrno, 10 );
						pcpUrno[10]= '\0';
						TrimRight ( pcpUrno );
					}
					else
					{
						dbg ( TRACE, "PP: At least 2 Profiles with same PRIO evaluate to true." );
						ilRC = RC_FAIL;
					}
				}
			}
		} /*end of if*/
	} while((ilRC == RC_SUCCESS) && !ilBreakOut );

	dbg(DEBUG, "PP: ilRC <%d> ilBreakOut <%d> llFoundRow <%ld>", ilRC , 
				ilBreakOut, llFoundRow ) ;

	if (pclLastPrio )
		free(pclLastPrio) ;

  return (ilRC) ;
} /* end of ProcessProfiles */


/******************************************************************************/
/*	Output of CONDINFO-structure into log-file								  */
/******************************************************************************/
static void DebugPrintCondInfo(int ipDebugLevel, CONDINFO *prpCondInfo)
{
	if ( !prpCondInfo )
	{
		dbg (ipDebugLevel, "DebugPrintCondInfo: nothing to print") ;
		return ;
	} /* end of if */

	dbg (ipDebugLevel, "DebugPrintCondInfo: type <%s> table.field  <%s.%s>", 
			prpCondInfo->crType, prpCondInfo->crTable, prpCondInfo->crField );
	dbg (ipDebugLevel, "DebugPrintCondInfo: op <%s> value <%s>", 
									prpCondInfo->crOp, prpCondInfo->crValue );
	dbg (ipDebugLevel, "DebugPrintCondInfo: reftab.reffld <%s.%s>", 
								prpCondInfo->crRefTab, prpCondInfo->crRefFld );

} /* end of DebugPrintCondInfo */


static int ProcessConditions ( char *pcpHopo, char *pcpEvtd, char *pcpUtpl, 
							   int *pipEval )
{
  
#define CONDITION_SIZE  800 

	char		clCondition [CONDITION_SIZE+1] ;       /* sizeof(evtd) */
	char		clEvtd [CONDITION_SIZE+1] ;            /* sizeof(evtd) */
	int			ilRC  = RC_SUCCESS ;                   /* Return code */
	int			ilLen, ilBreakOut=FALSE;
	int			ilCondCount=0;
	CONDINFO	rlCondition ;
	char		*pclPos, *pclCond=0;

	if ( !pcpHopo || !pcpEvtd || !pipEval )
	{
		dbg(TRACE, "ProcessConditions failed: At least one Parameter = NULL" ) ; 
		return RC_FAIL;
	}
  
	strcpy ( clEvtd, pcpEvtd );
	TrimRight ( clEvtd );
	/*  probably CCSClassLib has replaced semicolon by char(235) */
	ReplaceChar ( clEvtd, '\235', ';' );
  	dbg(TRACE, "PC: evtd <%s>", clEvtd ) ; 
	pclCond = clEvtd;
	*pipEval = TRUE;	/* hag20011007: no condition -> profile is ok */
 	do 
	{               /* read the complete condition -> one STX condition */ 
		ilLen = GetNextDataItem ( clCondition, &pclCond, ";","","  ") ;
	    if(ilLen > 0)             /* > 0 --> char read counter */ 
		{
			ilCondCount++;
			dbg(DEBUG,"PC: %d. Condition: len=<%d> item=<%s>", ilCondCount, ilLen, clCondition ) ;
			memset ( &rlCondition, 0, sizeof(rlCondition) );
			pclPos = clCondition;
			ilLen = GetNextDataItem ( rlCondition.crType, &pclPos, ".","","  ") ;
			if (ilLen>0)
			{
				if ( rlCondition.crType[0] == CDT_ASC_SIMPLE )
					strcpy ( rlCondition.crOp, "=" );
				else
					strcpy ( rlCondition.crOp, "IN" );
				ilLen = GetNextDataItem ( rlCondition.crTable, &pclPos, ".","","  ") ;
			}
			if (ilLen>0)
				ilLen = GetNextDataItem ( rlCondition.crField, &pclPos, "=","","  ") ;
			if (ilLen>0)
				ilLen = GetNextDataItem ( rlCondition.crValue, &pclPos, ";","","  ") ;
			if (  rlCondition.crType[0] != CDT_ASC_SIMPLE )
				ilRC = GetRefTabField ( rlCondition.crTable, rlCondition.crField, 
								rlCondition.crRefTab, rlCondition.crRefFld );
			DebugPrintCondInfo( DEBUG, &rlCondition );
			if ( ilRC != RC_SUCCESS )
				dbg ( TRACE, "GetRefTabField failed RC <%d>", ilRC );
		}
		else
			ilBreakOut = TRUE;
		if ( (ilRC == RC_SUCCESS)  && (ilBreakOut==FALSE) )  /* hag20011007 */
		{
			switch ( rlCondition.crType[0] )
			{
				case CDT_ASC_SIMPLE:
					ilRC = CheckCDT0(pcpHopo, &rlCondition, "AFTTAB", pipEval) ;
					break;
				case CDT_ASC_DYNAMIC:
                    ilRC = CheckCDT1 ( pcpHopo, &rlCondition, pcpUtpl, pipEval) ;
					break;
				case CDT_ASC_STATIC:			
					ilRC = CheckCDT2 ( pcpHopo, &rlCondition, pipEval) ;
                   	break;
				default:
					dbg( TRACE, "PC: unknown condition type <%s>", rlCondition.crType ) ;
					ilRC = RC_FAIL ;
					*pipEval = FALSE;
			}
		}
	} while (!ilBreakOut && (*pipEval) );
 
	return (ilRC) ;
	
} /* end of ProcessConditions () */


static int GetCEDAField ( HANDLE * pspHandle, char *pcpArrayName,
						  long *plpFields, char *pcpFieldNam,
						  long lpMaxBytes, long lpRowNum,
						  void *pvpDataBuf, int ipTrimRight )
{
	int ilRC;
	ilRC = CEDAArrayGetField ( pspHandle, pcpArrayName, plpFields, pcpFieldNam,
							   lpMaxBytes, lpRowNum, pvpDataBuf );
	if ( ( ilRC == RC_SUCCESS ) && ipTrimRight )
		TrimRight ( pvpDataBuf );
	return ilRC;
}


void TrimRight(char *s)
{
    int i = 0;    /* search for last non-space character */
    for (i = strlen(s) - 1; i >= 0 && isspace(s[i]); i--); /* trim off right spaces */    
	s[++i] = '\0';
}


/******************************************************************************/
/******************************************************************************/
static int GetFieldData(char *pcpTabNam, char *pcpFldNam, char *pcpFldLst, char *pcpData, char **pcpFieldData)
{
	int   ilRC     = RC_SUCCESS ;       /* Return code */
	int   ilPos    = 0 ;
	int   ilCol    = 0 ;
	int   ilItemNo = 0 ;
	long  llLen    = 0 ;
	char  clDel    = ',' ;
	
	if(ilRC == RC_SUCCESS)
	{
		ilRC = FindItemInList (pcpFldLst, pcpFldNam, clDel, &ilItemNo, &ilCol, &ilPos) ;
		if (ilRC == RC_SUCCESS)
		{
			dbg (DEBUG,"GetFieldData: Field <%s> Item <%d>", pcpFldNam, ilItemNo) ; 
			ilRC = GetFieldLength (pcpTabNam, pcpFldNam, &llLen) ;
		}
		if (ilRC == RC_SUCCESS)
		{
			dbg (DEBUG, "GetFieldData: len <%d>", llLen) ; 
			llLen++;
			*pcpFieldData = calloc (1, llLen) ;
			if (*pcpFieldData == NULL)
			{
				dbg (TRACE, "GetFieldData: calloc(1,%d) failed", llLen) ;
				ilRC = RC_FAIL ;
			} /* end of if */
			else      /* alloc ok */ 
				ilRC = GetDataItem (*pcpFieldData, pcpData, ilItemNo, clDel, "", "\0\0") ;
		    if (ilRC > 0)
		    	 ilRC = RC_SUCCESS ;
		} /* end of if */
	} /* end of if */
	
	return (ilRC) ;
} /* end of GetFieldData */


/**************************************************************************/
/* check simple profile conditions                                        */
/**************************************************************************/
static int CheckCDT0 (char *pcpHopo, CONDINFO *pspCondition, char *pcpTable, int *pipEval )
{
	int   ilRC          = RC_SUCCESS ;        /* Return code */
	char  *pclFieldData = NULL ;

	if ( !pcpHopo || !pspCondition || !pcpTable || !pipEval )
	{
		dbg(TRACE, "CheckCDT0 failed: At least one Parameter = NULL" ) ; 
		return RC_FAIL;
	}
	if ( !pcgOutbound ) 
	{
		dbg(TRACE, "CheckCDT0 failed: pcgOutbound = NULL" ) ; 
		return RC_FAIL;
	}

	*pipEval = TRUE ;
	
	ilRC = GetFieldData ( pcpTable, pspCondition->crField, pcgFieldList,
						  pcgOutbound, &pclFieldData) ;
	if (ilRC == RC_SUCCESS)
	{
		TrimRight(pclFieldData); /*hag010529: analogous to rulfnd */
		if (   ( pclFieldData[0] != ' ' )    
			  && (strcmp(pclFieldData, pspCondition->crValue) == 0)
		   ) 
			*pipEval = TRUE ;
		else 
			*pipEval = FALSE ;
		dbg ( DEBUG, "CheckCDT0: outbound evt=<%s> cdt=<%s> Eval<%d>", 
			  pclFieldData, pspCondition->crValue, *pipEval ) ;
	} /* end of if RC_SUCCESS */
	else
		dbg ( DEBUG, "CheckCDT0: GetFieldData failed RC <%d>", ilRC );

	if (pclFieldData != NULL)
	{
		free (pclFieldData) ;
		pclFieldData = NULL ;
	} /* end of if */
	return (ilRC) ;

} /* end of CheckCDT0 */


static int CheckCDT1(char *pcpHopo, CONDINFO *pspCondition, char *pcpUtpl, 
					 int *pipEval ) 
{
	int     ilRC       = RC_SUCCESS ;
	long    llRow     = ARR_FIRST ;
	char   *pclResult  = NULL ;  /* READ ONLY !!! */
	char   *pclValue = NULL ;
	char	clKey[101];
	char   *pclExpr    = NULL ;  /* READ ONLY */  
	long    llFldCnt, llCondLen ; 
	char    pclTableName[64] ; 
	char    pclCmpStr[2048] ; 
	short   slCursor ;

	char    clSqlbuf[1024] ;
	char    clDataArea[2048] ;
	short   sgSqlFkt ; 
	char    pclTabFldVal[2048] ;  
	char    pclFieldsName[1024] ; 
	char   *pclCondition ;
 
	ilRC        = RC_SUCCESS ;
	*pipEval  = FALSE ; 
	slCursor    = 0 ;
	pclTabFldVal[0] = '\0';

	if ( !pcpHopo || !pspCondition || !pcpHopo || !pipEval )
	{
		dbg(TRACE, "CheckCDT1 failed: At least one Parameter = NULL" ) ; 
		return RC_FAIL;
	}
	if ( !pcgOutbound ) 
	{
		dbg(TRACE, "CheckCDT1 failed: pcgOutbound = NULL" ) ; 
		return RC_FAIL;
	}

	ilRC = GetFieldData( pspCondition->crTable, pspCondition->crField,
						 pcgFieldList, pcgOutbound, &pclValue ) ;      
	if (ilRC == RC_SUCCESS)
	{
	     dbg( DEBUG,"CheckCDT1: outbound field <%s.%s> <%s> found", 
				pspCondition->crTable, pspCondition->crField, pclValue) ;
		strcpy ( pclTabFldVal, pclValue );
	}
	else
	    dbg(TRACE,"CheckCDT1: outbound field <%s.%s> not found", 
				  pspCondition->crTable, pspCondition->crField) ;
 
	if(ilRC == RC_SUCCESS)
	{
		sprintf ( clKey, "%s,%s,%s", pcpHopo, pcpUtpl, pspCondition->crValue ) ;
		dbg(DEBUG,"CheckCDT1: pattern <%s>", clKey ) ; 

		llRow  = ARR_FIRST ;
		ilRC = CEDAArrayFindRowPointer( &(rgDgrArray.rrArrayHandle),
										&(rgDgrArray.crArrayName[0]),
										&(rgDgrArray.rrIdx01Handle),
										&(rgDgrArray.crIdx01Name[0]),
										clKey, &llRow, (void *) &pclResult ) ;

		if(ilRC != RC_SUCCESS)                     
		{   /* dynamic group not found */                                       
			ilRC = RC_NOTFOUND ;
			dbg( DEBUG, "CheckCDT1: Dynamic group %s not found", pspCondition->crValue ) ;
		}
   }

   if(ilRC == RC_SUCCESS)
   {
		pclExpr = pclResult + rgDgrArray.plrArrayFieldOfs[5] ; 
		dbg(DEBUG,"CheckCDT1: DRG EXPR= <%s>", pclExpr) ; 
		sprintf (pclCmpStr, "%s", pclExpr) ;
		/* check, if one more data record exist */
		ilRC = CEDAArrayFindRowPointer( &(rgDgrArray.rrArrayHandle),
										rgDgrArray.crArrayName,
										&(rgDgrArray.rrIdx01Handle),
										rgDgrArray.crIdx01Name,
										clKey, &llRow, (void *) &pclResult ) ;
		if(ilRC == RC_SUCCESS)
		{
			/* ------------------------------ */ 
			 /* ERR -> only one RULE is useful */
			 /* ------------------------------ */ 
			 ilRC = RC_FAIL ;
		     dbg(TRACE,"CheckCDT1: URNO2=%s", pclResult + rgDgrArray.plrArrayFieldOfs[2]) ;
		}
	    else 
			ilRC = RC_SUCCESS ;  /* for perfect situation */     
	} /* if RC_SUCCESS > array handling */ 

	if(ilRC == RC_SUCCESS)     /* create SQL string */ 
	{
    		/*  probably CCSClassLib has replaced '(' by char(233) and ')' by char(234) */
    		ReplaceChar ( pclCmpStr, '\233', '(' );
    		ReplaceChar ( pclCmpStr, '\234', ')' );
 
		ilRC = AnalyseStr (pcpHopo, pclCmpStr, pclTableName, &llFldCnt, pclFieldsName,&llCondLen ) ;
		if(ilRC == RC_SUCCESS)      /* create SQL string */  
		{
			sprintf ( clSqlbuf, "select %s from %s where %s = :Val1", pclFieldsName,
                      /* pclTableName, pspCondition->crField ) ;   */
					  pclTableName, pspCondition->crRefFld ) ; 

		    dbg (DEBUG,"AnalyseStr: Condlen <%ld> llFldCnt <%ld>",  llCondLen, llFldCnt );
		    dbg (DEBUG,"CheckCDT1: sqlbuf=<%s>", clSqlbuf) ;  
		    pclCondition = calloc (1, llCondLen ) ;
			if (pclCondition == NULL) 
			{
				dbg(DEBUG,"CheckCDT1: ALLOC ERR for Condition String" ) ;
				ilRC = RC_FAIL ;
			}
		}    
	}

	sgSqlFkt = START | REL_CURSOR ;
	if (ilRC == RC_SUCCESS) 
	{
		do
		{
			dbg(DEBUG,"CheckCDT1: pclTabFldVal <%s>", pclTabFldVal ); 
			strcpy (clDataArea, pclTabFldVal) ;  /* set reference value new */ 
       			dbg(DEBUG, "CheckCDT1: SqlBuf <%s> DataArea <%s> Cursor <%d>", 
				clSqlbuf, clDataArea, slCursor ) ;         

			ilRC = sql_if(sgSqlFkt, &slCursor, clSqlbuf, clDataArea) ;
			if (ilRC == DB_SUCCESS)
			{
				dbg(DEBUG,"CheckCDT1: sqlif OK  <%s>", clDataArea ) ;   
				ilRC = RC_SUCCESS ;
			}
			else
			{
				dbg (TRACE, "CheckCDT1: sqlif NOT FOUND" ) ;
				ilRC = RC_NOTFOUND ; 
			} /* end of if */

			if (ilRC == RC_SUCCESS)
			{
				dbg (DEBUG, "CheckCDT1 vor SetCmpStringCond: pclCmpStr <%s> pclCondition <%s>", 
				pclCmpStr, pclCondition );
				ilRC = SetCmpStringCond (pcpHopo, pclCondition, pclCmpStr, clDataArea  ) ;       
				if (ilRC == RC_SUCCESS)
					dbg (DEBUG, "CONDITION --> %s", pclCondition) ;
				/* test for lex yacc function */
				dbg (DEBUG, "CheckCDT1 nach SetCmpStringCond: pclCmpStr <%s> pclCondition <%s>", 
				pclCmpStr, pclCondition );
				ilRC = CheckCondition(pclCondition) ;
				if (ilRC == RC_SUCCESS) 
				{	/* Condition evaluated to true */
					*pipEval = TRUE ;
				}
				dbg(DEBUG, "CheckCDT1: Checkcondition for condition <%s> returned <%d>", pclCondition, ilRC ) ; 
		     }
		} while ((ilRC == RC_SUCCESS) && (*pipEval == FALSE)) ;
	} /* if RC_SUCCESS */ 

	if(*pipEval == TRUE)
		dbg (DEBUG, "CheckCDT1: dynamic group <%s> found!", rgDgrArray.pcrIdx01RowBuf) ;
	
	dbg (DEBUG, "CheckCDT1: closing cursor <%d>", slCursor) ;
	close_my_cursor (&slCursor) ;

	if (pclValue != NULL)
	{
		free (pclValue) ;
		pclValue = NULL ;
	} /* end of if */

	if (pclCondition != NULL)
	{
		free (pclCondition) ;
		pclCondition = NULL ;
	} /* end of if */

	if(ilRC != RC_SUCCESS)
	{
		ilRC = RC_SUCCESS ; 
		dbg (DEBUG, "CheckCDT1: all dynamic group members processed" ) ;
	} /* if (ilRC == RC_NOTFOUND) */

	 return (ilRC) ;		
} /* end of CheckCDT1 */

/*******************************************************************/
/* Analyse Tablename and the fieldcount from the expression record */
/*  input string (cpSrcStr) = "ACT3, (TAB<ETX>FLDN <= XY) && (..)."*/
/*  was parsed for the following OUTPUT information:               */ 
/*     << cpTabname  : DB table (prev for delimiter <ETX>)         */
/*     << lpFldCnt   : count of all detected fields                */
/*     << cp1FieldStr: detect.flds like "aaaa,bbbb,cccc"           */
/*     << lpusedChars: the count of max. chars for the scan string */    
/* getestet 13.12.99 GHe                                           */
/* sep. tested with function: TestAnalyseStr(void) [use ilTest = 1]*/
/*******************************************************************/
static int AnalyseStr( char *pcpHopo, char *cpSrcStr, char *cpTabname,
						 long *lpFldCnt, char *cp1FieldStr, long *lpUsedChars) 
{ 
	long  ilLength ;    
	long  ilFldCnt ;  
	long  ilSearchPos ;  
	long  llusedChars ; 
	long  llHelp ;   
	long  ilTest ; 
	int   ilRC ; 
	char  cDelimiter ; 
	char  cpFieldStr[1024] ;
	char  clpTabname[64] ;  

	ilLength = strlen ( cpSrcStr ) ;  /* len from ACT3,(ACT.ACWS >= 41)... str */ 
	llusedChars = ilLength ; 
	cDelimiter = cgEtx[0] ;        /* between tablename and fieldACT<ETX>ACWS */  
	ilFldCnt = 0 ;                    /* counter for found fields*/
	ilSearchPos = 0 ;                 /* act search string position */  
	clpTabname[0] = '\0' ;            /* found tablename without end */ 
	ilRC = RC_SUCCESS ; 

	ilTest = 1 ;    /* 1 --> test function */ 
	if (ilTest == 1) 
		dbg (DEBUG, "AnalyseStr : len cmp Str = %ld", ilLength) ;

	do 
	{

		/*----- only test --------------------------------------*/
		/* if (cpSrcStr[ilSearchPos] == cDelimiter)             */
		/*  {                                                   */
		/*   dbg (DEBUG, "CMP <ETX> " );                        */
		/*  }                                                   */
		/*  else                                                */
		/*  {                                                   */
		/*   dbg (DEBUG, "CMP %c ", cpSrcStr[ilSearchPos] );    */
		/*  }                                                   */
		/*------------------------------------------------------*/ 

		if ( cpSrcStr[ilSearchPos] == cDelimiter ) 
		{
			if (ilTest == 1) 
				dbg (DEBUG, "AnalyseStr : delimiter found on pos=%ld", ilSearchPos) ;
			if (clpTabname[0] == '\0')
			{
				if (ilSearchPos < 3)
				{
					dbg (TRACE, "AnalyseStr : ERROR <EXT> ON START POSITION") ; 
					ilRC = RC_FAIL ; 
				} 
				else 
				{
					strncpy (clpTabname, &cpSrcStr[ilSearchPos-3], 3) ;
					clpTabname[3] = '\0' ; 
					sprintf (cpTabname, "%sTAB", clpTabname ) ;
					if (ilTest == 1) 
					{
						dbg (DEBUG, "AnalyseStr : copy tablename=<%s>", clpTabname) ;
						dbg (DEBUG, "AnalyseStr : tablename(END)=<%s>", cpTabname) ;
					} 
				}  
			} /* if (clpTabname[0] == '\0')  */ 

			if (ilTest == 1) 
				dbg (DEBUG, "AnalyseStr : search following field from <%s>",&cpSrcStr[ilSearchPos]) ;  
			if (ilSearchPos+4 > ilLength) 
			{
				dbg (TRACE, "AnalyseStr : ERROR <EXT> FIELD_LENGTH?") ; 
				ilRC = RC_FAIL ; 
			}
			else  /* CP TAB STRING already exist */
			{ 
				if (ilFldCnt == 0)
				{
					strncpy (cpFieldStr, &cpSrcStr[ilSearchPos+1], 4) ;
					cpFieldStr[4] = '\0' ; 
					strcpy (cp1FieldStr, cpFieldStr) ; 
					clpTabname[3] = '\0' ; 
					cpFieldStr[4] = '\0' ; 
					ilRC = GetFieldLength (clpTabname, cpFieldStr, &llHelp);
					if (ilTest == 1) 
					{
						dbg (DEBUG, "AnalyseStr : Tablename = <%s>", clpTabname ) ; 
						dbg (DEBUG, "AnalyseStr : Fieldname = <%s>", cpFieldStr ) ;
						dbg (DEBUG, "AnalyseStr : Found Fieldlength = <%ld>", llHelp ) ;
					} 
					llusedChars += llHelp ;  
				}
				else  /* ilFldCnt != 0 */  
				{
					strncpy (cpFieldStr, &cpSrcStr[ilSearchPos+1], 4) ;
					cpFieldStr[4] = '\0' ; 
					if (ilTest == 1) 
						dbg (DEBUG, "AnalyseStr : copy Fieldname=<%s>", cpFieldStr) ; 
					strcat (cp1FieldStr, ", ") ;
					strcat (cp1FieldStr, cpFieldStr) ;
					ilRC = GetFieldLength (clpTabname, cpFieldStr, &llHelp) ;
				 	llusedChars += llHelp ;
				}
			}
			ilFldCnt++ ;
		}
		ilSearchPos++ ;
	} while (( ilSearchPos < ilLength ) && (ilRC == RC_SUCCESS)) ;

	*lpFldCnt = ilFldCnt ;
	*lpUsedChars = llusedChars ;

	return (ilRC) ; 

} /* end of function AnalyseStr() */ 


/*******************************************************************/
/* CREATE the compare string       CHANGED VERSION                 */
/*  input string (cpSrcStr) = "(TAB<ETX>FLDN <= XY) && (..)."      */
/*  was parsed and changed for the following OUTPUT information:   */ 
/*     << cpCmpStr   : "(12 <= XY) && (..)"                        */
/*  the TAB<ETX>FLDN information must be in correct sequenz in the */
/*  input dataString in the following form:                        */
/*     >> cpDataArea : "12\\0112\\0..."                            */
/* getestet 14.12.99 GHe                                           */ 
/* sep. tested with function: TestAnalyseStr(void) [use ilTest = 1]*/
/* Additional we can work with different Tables                    */ 
/*                                                                 */  
/*******************************************************************/
static int SetCmpStringCond (char *pcpHopo, 
							 char *pcpCondition,   /* cond. for lex/Yacc */
                             char *pcpCmpStr,      /* given expression   */
                             char *pcpDataArea  )  /* Fieldvals from DB  */
{
	int     ilRC  = RC_SUCCESS ;
	long    ilConditionPos ; 
	long    ilDataAeraPos ;
	long    ilCmpStrPos ; 
	char    clTabName[32] ; 
	char    ilNameFieldlength ; 
	long    llFieldLen ;
	long    ilLength ;  
	char    cDelimiter ; 
	long    ilScanAll ; 
	char    clActField [64] ;
	long    ilTest ;
	char    clHelpStr[2048] ;  

	/* init values */
	ilRC  = RC_SUCCESS ;
	ilLength = strlen ( pcpCmpStr ) ;
	ilCmpStrPos = 0 ;
	ilConditionPos = 0 ;
	clTabName[0] = '\0' ;
	ilNameFieldlength = 4 ;
	ilDataAeraPos = 0 ;
	cDelimiter = cgEtx[0] ; 

	ilTest = 0 ;        /* 1 --> test function */ 
	ilScanAll = 0 ; 

	while ((ilScanAll == 0) && (ilRC == RC_SUCCESS) )
	{
		if (ilRC == RC_SUCCESS)     /* get tablename */ 
		{
			while (    ( ilCmpStrPos < ilLength )  
					&& ( pcpCmpStr[ilCmpStrPos] != cDelimiter ))
			{
				pcpCondition[ilConditionPos] = pcpCmpStr[ilCmpStrPos] ; 
				ilCmpStrPos++ ;
				ilConditionPos++ ;
				pcpCondition[ilConditionPos] = '\0' ;  

				if (pcpCondition [ilConditionPos-1] == ' ')
				{
					while(pcpCondition [ilConditionPos-1] == ' ')  /* filter blanks */
																	/* after cond.   */      
					{
						ilConditionPos-- ;
						pcpCondition[ilConditionPos] = '\0' ;        
					}
					pcpCondition [ilConditionPos] = ' ' ; 
					ilConditionPos++ ;
					pcpCondition[ilConditionPos] = '\0' ;
				}

				if (ilTest == 1) 
					dbg (DEBUG, "SetCmpStringCond : pcpConditionStr= <%s>", pcpCondition ) ;
			}
			ilCmpStrPos++ ; /* pos behind delimiter -> ABC1,(TAB<ETX>FLD1 >=... */
					/*                                       ^          */ 
			if (ilCmpStrPos >= ilLength) 
				ilScanAll = 1 ;
			if (ilTest == 1) 
				dbg (DEBUG, "SetCmpStringCond : pcpConditionStr POS = <%ld>", ilCmpStrPos) ; 
			pcpCondition[ilConditionPos] = '\0' ;
		}

		if (ilScanAll != 1)    
		{
			if ( ilConditionPos >= 3 )     /* get Tablename */ 
			{
				if (clTabName[0] == '\0') 
				{ 
					strncpy (clTabName, &pcpCmpStr[ilCmpStrPos-4], 3 ) ;
					clTabName[3] = '\0' ; 
					sprintf (clTabName, "%sTAB", clTabName ) ;  
					if (ilTest == 1) 
						dbg (DEBUG, "SetCmpStringCond : TABNAME = <%s>", clTabName) ;
				}
				if (( pcpCmpStr[ilCmpStrPos-1] != cDelimiter ) && (ilScanAll != 1))
				{
					if (ilTest == 1) 
						dbg (DEBUG, "SetCmpStringCond : TABNAME for Condition?") ;
					ilRC = RC_FAIL ;  
				}
				ilConditionPos -= 3 ; 
				pcpCondition[ilConditionPos] = '\0' ;
				if (ilTest == 1) 
					dbg (DEBUG, "SetCmpStringCond : xConditionStr= <%s>",pcpCondition ) ;
			}
			else
			{
				if (ilTest == 1) 
					dbg (DEBUG, "SetCmpStringCond : condition pos < 3") ; 
				ilRC = RC_FAIL ; 
			}   
		}

		if (( ilRC == RC_SUCCESS ) && ( ilScanAll == 0 ))   
		{
			if ( ilCmpStrPos+ilNameFieldlength >= ilLength)
			{                                          /* error in scan fieldlength */ 
				if (ilTest == 1) 
					dbg (DEBUG, "SetCmpStringCond : Fieldlength not correct") ;
				ilRC = RC_FAIL ;
			}
			else
			{
				strncpy (clActField, &pcpCmpStr[ilCmpStrPos], ilNameFieldlength) ;
				clActField[ilNameFieldlength] = '\0' ;
				if (ilTest == 1) 
					dbg (DEBUG, "SetCmpStringCond : act field = <%s>", clActField) ; 

				ilCmpStrPos += ilNameFieldlength ;
				if (ilTest == 1)
					llFieldLen = 3 ;
				else
					ilRC = GetFieldLength(clTabName, clActField, &llFieldLen) ;
				if ((ilRC != RC_SUCCESS) || (llFieldLen == 0) )
				{
					if (ilTest == 1) 
						dbg (DEBUG, "SetCmpStringCond : GetFieldlength not correct") ;
					ilRC = RC_FAIL ;
				}
				else
				{
					strncpy (clHelpStr, &pcpDataArea[ilDataAeraPos], llFieldLen ) ;
					clHelpStr[llFieldLen] = ' ' ;
					clHelpStr[llFieldLen+1] = '\0' ;
					strcat (&pcpCondition[ilConditionPos], " " ) ;
					strcat (&pcpCondition[ilConditionPos], clHelpStr ) ;
					ilDataAeraPos += llFieldLen + 1 ;
					ilConditionPos += llFieldLen + 2 ;  /* for blanks */
				}
			}
		} /* if (RC_SUCCESS && ilScan == 0) */
	} /* while */

	/* set a newline at the end of the condition str */
	strcat (pcpCondition, "\n") ;
	if (ilTest == 1) 
		dbg (DEBUG, "SetCmpStringCond : pcpConditionStr= <%s>",pcpCondition ) ;
	return (RC_SUCCESS) ;

} /* function SetCmpStringCond () */ 

/******************************************************************************/
/* using Lex and Yacc                                                         */
/******************************************************************************/
static int CheckCondition(char *pcpCondition)
{
	int ilRC = RC_SUCCESS;            /* Return code */
	int ilRes = RC_SUCCESS ;
	
	dbg(DEBUG, "CC: <%s>", pcpCondition) ;
	ilRC = RmsCheckRule(pcpCondition, &ilRes) ;
	if (ilRC == RC_SUCCESS)
	{
		if(ilRes == RC_SUCCESS)
			dbg(DEBUG, "CC: <%s> is true", pcpCondition) ; 
		else 
		{
			dbg (DEBUG, "CC: <%s> is false", pcpCondition) ;
			ilRC = RC_FAIL ; 
		} /* end of if */
	 } 
	 else 
	 {
		dbg (TRACE, "CC: Syntax-Error in check rule <%s>", pcpCondition) ;
		ilRC = RC_FAIL ;
	 } /* end of if */	
	 return (ilRC) ;
} /* end of CheckCondition */

/******************************************************************************/
/* Additional Check version for static datarecords (only one sql-statement    */
/******************************************************************************/
static int CheckCDT2 ( char *pcpHopo, CONDINFO *pspCondition, int *pipEval ) 
{
	int     ilRC       = RC_SUCCESS ;       /* Return code VBL */
	long    llRow     = ARR_FIRST ;
	char   *pclResult  = NULL ;             /* READ ONLY !!! */
	char   *pclUrno    = NULL ;             /* READ ONLY !!! */
	char   *pclTabn    = NULL ;             /* READ ONLY !!! */
	char   *pclValue   = NULL ;    
	short   slCursor   = 0 ;
	char    clSqlbuf[2048] ;
	char    clDataArea[2048] ;
	short   sgSqlFkt ;
	char    pclTabName[48] ;
	char    pclKey[2048] ;

	*pipEval  = FALSE ;

	dbg(DEBUG, "CheckCDT2: table <%s> field <%s>", pspCondition->crTable, pspCondition->crField) ;

	ilRC = GetFieldData( pspCondition->crTable, pspCondition->crField,
						 pcgFieldList, pcgOutbound, &pclValue ) ;      
	if (ilRC == RC_SUCCESS)
		dbg(DEBUG,"CheckCDT2: outbound field <%s> <%s> found", pspCondition->crField, pclValue) ; 
	else
	   dbg(TRACE,"CheckCDT2: outbound field <%s> not found - outbound", pspCondition->crField) ;
  
	sprintf (pclKey, "%s,%s,%s", pcpHopo, pspCondition->crValue, pspCondition->crRefTab ) ;
	dbg(DEBUG,"CheckCDT2: pattern <%s>", pclKey ) ;
	
	if(ilRC == RC_SUCCESS)
	{
		llRow  = ARR_FIRST ;
		pclUrno = NULL ;

		ilRC = CEDAArrayFindRowPointer( &(rgSgrArray.rrArrayHandle),
									   rgSgrArray.crArrayName,
									   &(rgSgrArray.rrIdx01Handle),
									   rgSgrArray.crIdx01Name,
									   pclKey, &llRow, (void *)&pclResult );

		if( (ilRC == RC_SUCCESS) && pclValue )
		{
			pclUrno = pclResult + rgSgrArray.plrArrayFieldOfs[2] ;
			pclTabn = pclResult + rgSgrArray.plrArrayFieldOfs[3] ;

			pclTabn[3] = '\0' ;   /* end of tablename */

			dbg (DEBUG, "CheckCDT2: found cond1=tbl=<%s> URNO=<%s> ", pclTabn, pclUrno) ;
			sprintf (pclTabName, "%sTAB", pclTabn ) ;

			/* create sql statement */
			/*sprintf (clSqlbuf, "select %s from %s%s where URNO =:Val01",
							pspCondition->crRefFld, pspCondition->crRefTab, cgTabEnd ) ;*/
			sprintf ( clSqlbuf, "select %s from %s%s where URNO in (select UVAL from SGM%s where USGR='%s') and %s='%s'",
							  pspCondition->crRefFld, pspCondition->crRefTab, cgTabEnd, cgTabEnd, pclUrno, 
							  pspCondition->crRefFld, pclValue ) ;

			clDataArea[0] = '\0' ; 
			slCursor = 0 ;
			dbg (DEBUG, "CheckCDT2: sql statement=<%s>", clSqlbuf) ;  
			sgSqlFkt = START ;  

			ilRC = sql_if ( sgSqlFkt, &slCursor, clSqlbuf, clDataArea ) ;
					
			dbg(DEBUG,"CheckCDT2: sqlif return <%d> Dataarea <%s>", ilRC, clDataArea ) ;

			if ( (ilRC == DB_SUCCESS) && strstr ( clDataArea, pclValue ) )
			{
				dbg(DEBUG,"CheckCDT2: sqlif OK  <%s>", &clDataArea[0] ) ;
				ilRC = RC_SUCCESS ;
				*pipEval = TRUE ;  
			}
			else
			{
				dbg (TRACE, "CheckCDT2: sql cond. not found!" ) ;
			} /* end of if */
				   
			if (ilRC == RC_NOTFOUND)
			{
			   ilRC = RC_SUCCESS ; 
			}   
			dbg ( DEBUG, "CheckCDT2: closing cursor <%d> pcpIsTrue = %d", 
				  slCursor,*pipEval) ;
			close_my_cursor (&slCursor) ;
		}		
	} /* end of if */

	if (pclValue != NULL)
	{
		free (pclValue) ;
		pclValue = NULL ;
	} /* end of if */

	if(ilRC == RC_NOTFOUND)
	{
		ilRC = RC_SUCCESS ;
		dbg (DEBUG, "CheckCDT2: all static group members processed" ) ;
	} /* end of if (ilRC == RC_NOTFOUND) */
	return (ilRC) ;
} /* end of CheckCDT2 */


static int ForwardEvent( int ipDest, char *pcpProUrno, char *pcpRueTyp0Urnos )
{
	int       ilRC        = RC_SUCCESS;  /* Return code */
	BC_HEAD  *prlBchead    = NULL;
	CMDBLK   *prlCmdblk    = NULL;
	char     *pclSelection = NULL;
	char     *pclFields    = NULL;
	char     *pclData      = NULL;
	char     *pclUrnoList  = NULL;
	long      llActSize   = 0;
	int		 ilProStrLength=0;

	if (pcpProUrno && pcpRueTyp0Urnos)
		dbg(DEBUG,"ForwardEvent: dest <%d> ProUrno <%s>  RueUrnos <%s>", ipDest,
			pcpProUrno, pcpRueTyp0Urnos);
	else
		dbg(DEBUG,"ForwardEvent: dest <%d> ProUrno and/or RueUrnos are NULL",
			ipDest);

	if ( pcpProUrno )
		ilProStrLength = strlen(pcpProUrno);
	llActSize = prgEvent->data_length + sizeof(EVENT) + ilProStrLength+2 ;

	if (llActSize > lgOutEventSize)
	{
		lgOutEventSize = 2 * llActSize ;

		prgOutEvent = realloc(prgOutEvent, lgOutEventSize) ;
		if (prgOutEvent == NULL)
		{
			dbg (TRACE, "ForwardEvent: realloc out event <%ld> bytes failed",lgOutEventSize);
			ilRC = RC_FAIL ;
		} /* end of if */
		else
			dbg (DEBUG, "ForwardEvent: realloc out event <%ld> bytes ok",lgOutEventSize);
	} /* end of if */
	dbg (DEBUG, "ForwardEvent: llActSize <%ld> lgOutEventSize <%ld>", llActSize, lgOutEventSize );
	if (ilRC == RC_SUCCESS)
	{
		memcpy (prgOutEvent, prgEvent, prgEvent->data_length + sizeof(EVENT)) ;

		prlBchead    = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
		prlCmdblk    = (CMDBLK *)  ((char *) prlBchead->data) ;
		pclSelection = prlCmdblk->data ;
	
		pclFields    = pclSelection + strlen(pclSelection) + 1 ;
		pclData      = pclFields + strlen(pclFields) + 1 ;
		/*  Nur Outbound-Flug wird weitergeleitet*/
		if ( pcgOutbound )
			strcpy ( pclData, pcgOutbound );
		else
			strcpy ( pclData, " " );
		pclUrnoList  = pclData + strlen(pclData) ;
		*pclUrnoList = '\n' ;
		pclUrnoList++ ;
		if ( pcpRueTyp0Urnos )
		{
			strcpy (pclUrnoList, pcpRueTyp0Urnos) ;
			pclUrnoList += strlen(pclUrnoList);
		}
		*pclUrnoList = '\n' ;
		pclUrnoList++;
		*pclUrnoList = '\0';

		if ( pcpProUrno )
			strcat (pclUrnoList, pcpProUrno) ;

		DebugPrintBchead (DEBUG,prlBchead) ;
		DebugPrintCmdblk (DEBUG,prlCmdblk) ;
		dbg (DEBUG, "ForwardEvent: selection <%s>", pclSelection) ;         
		dbg (DEBUG, "ForwardEvent: fields    <%s>", pclFields) ;            
		dbg (DEBUG, "ForwardEvent: data      <%s>", pclData) ;              
		if ( pclUrnoList = strchr ( pclData, '\n' ) )
		{
			pclUrnoList++;
			dbg (DEBUG, "ForwardEvent: urnolist  <%s>", pclUrnoList) ;         
		}
		else
		{
			pclUrnoList = 0;
			dbg (DEBUG, "ForwardEvent: urnolist is empty") ;         
		}
		/*************************************************************************/
			
		ilRC = que (QUE_PUT, ipDest, mod_id, PRIORITY_4, llActSize, (char *)prgOutEvent) ;
		if (ilRC != RC_SUCCESS)
		{
			HandleQueErr (ilRC) ;
			DebugPrintEvent (TRACE, prgOutEvent) ;
			DebugPrintBchead (TRACE, prlBchead) ;
			DebugPrintCmdblk (TRACE, prlCmdblk) ;
			dbg (TRACE, "selection <%s>", pclSelection) ;
			dbg (TRACE, "fields    <%s>", pclFields) ;
			dbg (TRACE, "data      <%s>", pclData) ;
			dbg (TRACE, "urnolist  <%s>", pclUrnoList) ;
		} /* end of if */
	} /* end of if */

	return (ilRC) ;
	
} /* end of ForwardEvent */

void ReplaceChar ( char *pcpStr, char cpOld, char cpNew )
{
	char *pclPos, *pclAct;
	pclAct = pcpStr;
	while ( pclAct )
	{
		if ( pclPos = strchr ( pclAct, cpOld ) )
			*pclPos = cpNew;
		pclAct = pclPos;
	}
}
  	

/******************************************************************************/
/******************************************************************************/
static int CheckProfileCalendar(char *pcpHopo, char *pcpUrno, int *pipIsValid)
{
	int	ilRC = RC_FAIL;			/* Return code */
	char	*pclStod = 0;
 
	ilRC = RC_SUCCESS;
	*pipIsValid = FALSE ;

	if ( pcgOutbound && pcgOutbound[0] )
	{
		ilRC = GetFieldData ( "AFT", "STOD", pcgFieldList, pcgOutbound, &pclStod );
		if (ilRC != RC_SUCCESS)
			dbg (TRACE, "GetFieldData for STOD failed RC <%d>", ilRC ) ;
		else
			dbg(DEBUG, "GetFieldData for STOD <%s> RC <%d>", pclStod, ilRC ) ;
	}
	if ( ilRC != RC_SUCCESS )
		return RC_FAIL;

	ilRC = CheckValidity ("PROFILE", pcpUrno, pclStod, pipIsValid) ; 	

	if (ilRC != RC_SUCCESS)
		dbg (TRACE, "CheckProfileCalendar: CheckValidity for Profile <%s> failed RC <%d>", 
						pcpUrno, ilRC) ;
	else
		dbg(DEBUG, "CheckProfileCalendar: CheckValidity ok, Validity for Profile <%s>: <%d>", 
						pcpUrno, *pipIsValid) ;

	return (ilRC) ;
	
} /* end of CheckProfileCalendar */


static int IsFlightToCalc ( char *pcpHopo, char *pcpFlight, int *pipGoOn )
{
	int		ilRC = RC_SUCCESS;
	char	*pclField=0;
	
	if ( !pcpFlight || !pipGoOn )
		return RC_FAIL;

	ilRC = GetFieldData( "AFT", "FTYP", pcgFieldList, pcpFlight, &pclField );
	if ( ( ilRC == RC_SUCCESS ) && pclField )
	{
		switch (pclField[0] )
		{	/* in case of Canceled, Rerouted, Diverted, Noop don't process flight */
			case 'X':
			case 'D':
			case 'R':
			case 'N':
				*pipGoOn = 0;
				break;
			default:
				*pipGoOn = 1;
		}
	}

	if ( pclField )
		free ( pclField );
	return ilRC;
}


static int FindFeleEvaluation ( char *pcpTable, char *pcpField, char *pcpFele )
{
	long llRow= ARR_FIRST;	
	int ilRC ;
	char *pclResult=0;
	char clKey[51];	/*"TANA,FINA" */
	char *pclFele;
	sprintf ( clKey, "%s,%s", pcpTable, pcpField );
	
	ilRC = CEDAArrayFindRowPointer( &(rgFeleEvalArr.rrArrayHandle),  
									rgFeleEvalArr.crArrayName,
									&(rgFeleEvalArr.rrIdx01Handle), 
									rgFeleEvalArr.crIdx01Name, clKey, 
									&llRow, (void**)&pclResult ); 
	if ( ilRC ==RC_SUCCESS ) 
	{
		pclFele = pclResult + rgFeleEvalArr.plrArrayFieldOfs[2];
		if ( ( pclFele[0] == ' ' ) || ( pclFele[0] == '\0' ) )
		{
			ilRC = RC_FAIL;
			dbg ( TRACE, "FindFeleEvaluation: Found empty FELE, pattern <%s>", 
				  clKey );
		}
		else
			strcpy ( pcpFele, pclFele );
	}					
	else
		dbg ( DEBUG, "FindFeleEvaluation: Didn't find pattern <%s>", clKey );
	return ilRC;
}

static int SaveFeleEvaluation ( char *pcpTable, char *pcpField, char *pcpFele )
{
	long llRow= ARR_FIRST;	
	int ilRC ;
	
	memset ( cgEvalBuffer, 0, sizeof(cgEvalBuffer) );
	ilRC = AATArrayAddRow( &(rgFeleEvalArr.rrArrayHandle), 
						   rgFeleEvalArr.crArrayName, &llRow, (void*)cgEvalBuffer );
	if ( ilRC != RC_SUCCESS )
	{
		dbg ( TRACE, "SaveFeleEvaluation: AATArrayAddRow failed Tana <%s> Fina <%s> ilRC <%d>", 
					  pcpTable, pcpField, ilRC );
	}
	else
	{
		sprintf ( cgEvalBuffer, "%s,%s,%s", pcpTable, pcpField, pcpFele	); 
		ilRC = CEDAArrayPutFields( &rgFeleEvalArr.rrArrayHandle,
								   rgFeleEvalArr.crArrayName, 0, &llRow, 
								   "TANA,FINA,FELE", cgEvalBuffer );
		if ( ilRC != RC_SUCCESS )
			dbg ( TRACE, "SaveFeleEvaluation: CEDAArrayPutFields failed Buffer <%s> Row <%ld> ilRC <%d>", 
				  cgEvalBuffer, llRow, ilRC );
		else
		{
			if ( CEDAArrayGetRowCount(&rgFeleEvalArr.rrArrayHandle,
								       rgFeleEvalArr.crArrayName, &llRow ) !=RC_SUCCESS )
				dbg ( TRACE, "SaveFeleEvaluation: CEDAArrayGetRowCount failed" );
			else
				dbg ( DEBUG, "SaveFeleEvaluation: Added Row <%s> successfully, now %ld rows", 
					  cgEvalBuffer, llRow );
		}
	}
	return ilRC;
}

static int GetFieldLength(char *pcpTana, char *pcpFina, long *plpLen)
{
 int  ilRC  = RC_SUCCESS ;     /* Return code */
 int  ilCnt = 0 ;
 char clTaFi[64] ;
 char clFele[32] ;
 char clFldLst[32] ;

	*plpLen = 0L;

 ilRC = FindFeleEvaluation ( pcpTana, pcpFina, clFele );
 if ( ( ilRC == RC_SUCCESS ) && ( sscanf(clFele, "%ld", plpLen ) >= 1 ) )
 {
	dbg (DEBUG, "GetFieldLength: from Array <%s,%s> <%ld>", pcpTana, pcpFina, *plpLen) ;
	return ilRC;
 }
 ilCnt = 1 ;
 sprintf (&clTaFi[0], "%s,%s", pcpTana, pcpFina) ;
 sprintf (&clFldLst[0], "TANA,FINA") ; /* wird von syslib zerst�rt - konstante nicht m�glich */

 ilRC = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"") ;
 switch (ilRC)
 {
  case RC_SUCCESS :
		SaveFeleEvaluation ( pcpTana, pcpFina, clFele );
        *plpLen = atoi (&clFele[0]) ;
         dbg (DEBUG, "GetFieldLength: <%s,%s> <%d>", pcpTana, pcpFina, *plpLen) ;
      break ;

  case RC_NOT_FOUND :

        dbg (TRACE, "GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s>", ilRC, &clTaFi[0]) ;
      break ;

  case RC_FAIL :

        dbg (TRACE, "GetFieldLength: syslibSSD returned <%d> RC_FAIL", ilRC) ;
      break ;

  default :

        dbg (TRACE, "GetFieldLength: syslibSSD returned <%d> unexpected", ilRC) ;
      break ;

 } /* end of switch */

 return (ilRC) ;
	
} /* end of GetFieldLength */


static int  SetAATArrayInfo(char *pcpArrayName,char *pcpArrayFieldList, long *plpFieldLens, 
						    char *pcpIdx01Name, char *pcpIdx01FieldList, char *pcpIdx01Order, 
						    ARRAYINFO *prpArrayInfo)
{
	int	ilRc   = RC_SUCCESS;				/* Return code */
	int ilLoop = 0;
	char clOrderTxt[4];
	long	*pllOrdering=0;

	if(prpArrayInfo == NULL)
	{
		dbg(TRACE,"SetAATArrayInfo: invalid last parameter : null pointer not valid");
		return  RC_FAIL;
	}
	
	memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

	prpArrayInfo->rrArrayHandle = -1;
	prpArrayInfo->rrIdx01Handle = -1;
	prpArrayInfo->rrIdxUrnoHandle = -1;

	if( pcpArrayName && (strlen(pcpArrayName) <= ARR_NAME_LEN) )
		strcpy(prpArrayInfo->crArrayName,pcpArrayName);
	
	if(pcpArrayFieldList && (strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN) )
		strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);

		prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if( prpArrayInfo->plrArrayFieldOfs == NULL )
		{
			dbg(TRACE,"SetAATArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}/* end of if */
		else
		{
			prpArrayInfo->plrArrayFieldOfs[0] = 0;
			for(ilLoop = 1 ; ilLoop < prpArrayInfo->lrArrayFieldCnt ; ilLoop++)
			{
				prpArrayInfo->plrArrayFieldOfs[ilLoop] = prpArrayInfo->plrArrayFieldOfs[ilLoop -1] + (plpFieldLens[ilLoop-1] +1);
			}/* end of for */
		}
	}/* end of if */


	if(pcpIdx01Name && (strlen(pcpIdx01Name) <= ARR_NAME_LEN) )
		strcpy(prpArrayInfo->crIdx01Name,pcpIdx01Name);

	if( pcpIdx01FieldList && (strlen(pcpIdx01FieldList) <= ARR_FLDLST_LEN) )
		strcpy(prpArrayInfo->crIdx01FieldList,pcpIdx01FieldList);

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldLen == NULL)
		{
			dbg(TRACE,"SetAATArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}
		else
			*(prpArrayInfo->plrArrayFieldLen) = -1;
	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
       	ilRc = GetLogicalRowLength(pcpArrayFieldList,pcpArrayFieldList,plpFieldLens,&(prpArrayInfo->lrArrayRowLen));
       	if(ilRc != RC_SUCCESS)
           	dbg(TRACE,"SetAATArrayInfo: GetLogicalRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->lrArrayRowLen++;
		prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+1));
   		if(prpArrayInfo->pcrArrayRowBuf == NULL)
   		{
      		ilRc = RC_FAIL;
      		dbg(TRACE,"SetAATArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
       	ilRc = GetLogicalRowLength(pcpArrayFieldList,pcpIdx01FieldList,plpFieldLens,&(prpArrayInfo->lrIdx01RowLen));
       	if(ilRc != RC_SUCCESS)
           	dbg(TRACE,"SetAATArrayInfo: GetLogicalRowLength failed <%s> <%s>",pcpIdx01Name,pcpIdx01FieldList);
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->lrIdx01RowLen++;
		prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx01RowLen+1));
   		if(prpArrayInfo->pcrIdx01RowBuf == NULL)
   		{
			ilRc = RC_FAIL;
			dbg(TRACE,"SetAATArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */


	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->lrIdx01FieldCnt = get_no_of_items(pcpIdx01FieldList);
		pllOrdering = (long *) calloc(prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
		if(!pllOrdering )
		{
			dbg(TRACE,"SetAATArrayInfo: pllOrdering calloc(%d,%d) failed",prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}
		else
		{
			for(ilLoop=0; ilLoop < prpArrayInfo->lrIdx01FieldCnt; ilLoop++)
			{
				if ( !pcpIdx01Order ||
				     ( get_real_item(clOrderTxt,pcpIdx01Order,ilLoop+1)!=RC_SUCCESS ))				 
					clOrderTxt[0] = 'D';
				if ( clOrderTxt[0] == 'A' )
					pllOrdering[ilLoop] = ARR_ASC;
				else
					pllOrdering[ilLoop] = ARR_DESC;
			}/* end of for */
		}/* end of else */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		
		ilRc = AATArrayCreate( &(prpArrayInfo->rrArrayHandle), pcpArrayName,
							   TRUE, 1000, prpArrayInfo->lrArrayFieldCnt,
							   plpFieldLens, pcpArrayFieldList, NULL );
		if(ilRc != RC_SUCCESS)
			dbg(TRACE,"SetAAtArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
	}/* end of if */



	if( (ilRc == RC_SUCCESS) && pcpIdx01Name )
	{
		ilRc = CEDAArrayCreateMultiIndex( &(prpArrayInfo->rrArrayHandle),
										  pcpArrayName,
										  &(prpArrayInfo->rrIdx01Handle),
										  pcpIdx01Name,pcpIdx01FieldList,
										  pllOrdering  );

		if(ilRc != RC_SUCCESS)
			dbg(TRACE,"SetAAtArrayInfo: CEDAArrayCreateMultiIndexfailed <%d>",ilRc);
	}/* end of if */
 
	if ( (ilRc == RC_SUCCESS) && strstr(pcpArrayFieldList,"URNO") )
	{
		strcpy ( prpArrayInfo->crIdxUrnoName, "URNOIDX" );
		ilRc = CEDAArrayCreateSimpleIndexDown(&(prpArrayInfo->rrArrayHandle),
											  prpArrayInfo->crArrayName,
											  &(prpArrayInfo->rrIdxUrnoHandle), 
											  prpArrayInfo->crIdxUrnoName, "URNO" );
		if(ilRc != RC_SUCCESS)                                             
		{
			dbg( TRACE,"SetAAtArrayInfo: CEDAArrayCreateSimpleIndexDown for %s failed <%d>",
				 pcpArrayName, ilRc);		
		}
	}
	else
		prpArrayInfo->rrIdxUrnoHandle = -1;

	if ( pllOrdering )
		free (pllOrdering);
	pllOrdering = 0;

	return(ilRc);
	
} /* end of SetAAtArrayInfo */


static int GetLogicalRowLength(char *pcpTotalFieldList,char *pcpFieldList, long *plpFieldSizes,long *plpLen)
{
	int	 ilRc        = RC_SUCCESS;			/* Return code */
	int  ilNoOfItems = 0;
	int  ilItemNo = 0;
	int  ilLoop      = 0;
	long llRowLen    = 0;
	char clFina[8];
	
	if (pcpTotalFieldList != NULL && pcpFieldList != NULL)
	{
		ilNoOfItems = get_no_of_items(pcpFieldList);

		
		ilLoop = 1;
		do
		{
			get_real_item(clFina,pcpFieldList,ilLoop);
			/*ilItemNo = get_item_no(pcpTotalFieldList,clFina,strlen(clFina) ); 
			if ( ilItemNo >= 0 )	*/
			if( ( GetItemNo(clFina,pcpTotalFieldList,&ilItemNo) == RC_SUCCESS)  
				&& ( ilItemNo > 0 ) )
			{
				llRowLen++;
				llRowLen += plpFieldSizes[ilItemNo-1];
				dbg(TRACE,"GetLogicalRowLength:  clFina <%s> plpFieldSizes[ilItemNo-1] <%ld>",clFina,plpFieldSizes[ilItemNo-1]);
			}

			ilLoop++;
		}while(ilLoop <= ilNoOfItems+1);
	}
	if(ilRc == RC_SUCCESS)
	{
		*plpLen = llRowLen;
	}/* end of if */

	dbg(TRACE,"GetLogicalRowLength:  FieldList <%s> plpLen <%ld>",pcpFieldList,*plpLen);
	return(ilRc);
	
} /* end of GetLogicalRowLength */

static int GetItemNo(char *pcpFieldName,char *pcpFields, int *pipItemNo)
{
	int ilRc = RC_FAIL;
	int ilLc;
	int ilItemCount;
	char pclFieldName[1200];

	*pipItemNo = 0;

	ilItemCount = get_no_of_items(pcpFields);

	dbg(TRACE,"GetItemNo itemCount = %d",ilItemCount);	
	for(ilLc = 1; ilLc <= ilItemCount && ilRc != RC_SUCCESS; ilLc++)
	{
		get_real_item(pclFieldName,pcpFields,ilLc);
		if(!strcmp(pclFieldName,pcpFieldName))
		{
				*pipItemNo = ilLc;
				dbg(TRACE,"GetItemNo:%05d: %s",ilLc,pclFieldName);
				ilRc = RC_SUCCESS;
		}
	}

	return ilRc;
}

static int SaveIndexInfo (ARRAYINFO *prpArrayInfo, int ipMode, int ipLogFile )
{
	int    ilRC       = RC_SUCCESS ;
	long   llRow      = 0 ;
	FILE  *prlFile    = NULL ;
	char   clDel      = ',' ;
	char   clFile[512] ;
	char  *pclTrimBuf = NULL ;
	long llRowCount;
	int ilCount = 0;
	char  *pclTestBuf = NULL ;
	HANDLE	*pslIdxHdl;
	char	*pclIdxName;
	char	clIdxFieldList[ARR_FLDLST_LEN+1];
	char	*pclRowBuff=0;
	int     ilRowLen;

	if ( ipMode == 1 )
	{
		pslIdxHdl = &(prpArrayInfo->rrIdxUrnoHandle);
		pclIdxName = prpArrayInfo->crIdxUrnoName;
		strcpy ( clIdxFieldList, "URNO" );
		ilRowLen = 11;
	}
	else
	{
		pslIdxHdl = &(prpArrayInfo->rrIdx01Handle);
		pclIdxName = prpArrayInfo->crIdx01Name;
		strcpy ( clIdxFieldList, prpArrayInfo->crIdx01FieldList );
		ilRowLen = prpArrayInfo->lrIdx01RowLen;
	}
	pclRowBuff = (char *) calloc(1, ilRowLen+1);
	dbg (TRACE,"SaveIndexInfo: ArrayName <%s>",prpArrayInfo->crArrayName) ;

	if ( *pslIdxHdl==-1) 
	{
		ilRC = RC_FAIL;
		dbg ( TRACE, "SaveIndexInfo: IdxHandle == -1, Mode <%d>", ipMode );
	}
	if ( !pclRowBuff )
	{
		dbg ( TRACE, "SaveIndexInfo: Calloc failed" );
		ilRC = RC_FAIL;
	}
		
	if (ilRC == RC_SUCCESS)
	{
		sprintf (&clFile[0],"%s/%s_%s.info",getenv("DBG_PATH"),mod_name,prpArrayInfo->crArrayName) ;

		errno = 0 ;
		prlFile = fopen (&clFile[0], "w") ;
		if (prlFile == NULL)
		{
			dbg (TRACE, "SaveIndexInfo: fopen <%s> failed <%d-%s>", clFile, 
				  errno, strerror(errno)) ;
			ilRC = RC_FAIL ;
		} /* end of if */
	} /* end of if */

	if (ilRC == RC_SUCCESS)
	{
		if ( ipLogFile )
		{
			dbg (DEBUG, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ;      
			dbg (DEBUG, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ;  
			dbg (DEBUG, "Idx01FieldList <%s>\n", clIdxFieldList) ;  
		}
		fprintf (prlFile, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ; /*fflush(prlFile) ;*/
		fprintf (prlFile, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ; /*fflush(prlFile) ;*/
		fprintf (prlFile, "Idx01FieldList <%s>\n", clIdxFieldList) ; 
		fflush(prlFile) ;

		if ( ipLogFile )
			dbg(DEBUG,"IndexData") ; 
		fprintf(prlFile,"IndexData\n"); fflush(prlFile);

		pclRowBuff[0] = '\0';
		llRow = ARR_FIRST ;
		do
		{
			ilRC = CEDAArrayFindKey ( &(prpArrayInfo->rrArrayHandle), 
									  prpArrayInfo->crArrayName, pslIdxHdl, 
									  pclIdxName, &llRow, ilRowLen, pclRowBuff );
			if (ilRC == RC_SUCCESS)
			{
				if ( ipLogFile )
					dbg(DEBUG, "SaveIndexInfo: Key <%ld> <%s>",llRow,pclRowBuff) ;
	
				if (strlen (pclRowBuff) == 0)
				{
					ilRC = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
					if( ipLogFile && (ilRC == RC_SUCCESS) )
						dbg (DEBUG, "SaveIndexInfo: Row <%ld> <%s>", llRow,prpArrayInfo->pcrArrayRowBuf) ;
				} /* end of if */

				pclTrimBuf = strdup (pclRowBuff) ;
				if (pclTrimBuf != NULL)
				{
					ilRC = GetDataItem (pclTrimBuf,pclRowBuff, 1, clDel, "", "  ") ;
					if (ilRC > 0)
					{
					   ilRC = RC_SUCCESS ;
					   fprintf (prlFile,"Key            <%ld><%s>\n",llRow,pclTrimBuf) ; fflush(prlFile) ;
					} /* end of if */
					free (pclTrimBuf) ;
					pclTrimBuf = NULL ;
				} /* end of if */
				llRow = ARR_NEXT;
			}
			else
			{
				if ( ipLogFile )
					dbg (TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRC) ;
			}
		} while (ilRC == RC_SUCCESS) ;

		if ( ipLogFile )
			dbg (DEBUG,"ArrayData") ;
	
		fprintf (prlFile,"ArrayData\n"); fflush(prlFile);
		llRow = ARR_FIRST ;

		CEDAArrayGetRowCount(&(prpArrayInfo->rrArrayHandle),prpArrayInfo->crArrayName,&llRowCount);

		llRow = ARR_FIRST;
		if ( ipLogFile )
			dbg(DEBUG,"Array  <%s> data follows Rows %ld", prpArrayInfo->crArrayName,llRowCount);
		do
		{
			ilRC = CEDAArrayGetRowPointer(&(prpArrayInfo->rrArrayHandle),
			&(prpArrayInfo->crArrayName[0]),llRow,(void *)&pclTestBuf);
			if (ilRC == RC_SUCCESS)
			{
				for( ilCount = 0; ilCount < prpArrayInfo->lrArrayFieldCnt; ilCount++)
				{
				
					fprintf (prlFile,"<%s>",&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]) ; 
					fflush(prlFile) ;
					if ( ipLogFile )
						dbg (DEBUG, "SaveIndexInfo: ilCount <%ld> <%s>", ilCount,&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]) ;
				}
				fprintf (prlFile,"\n") ; 
				if ( ipLogFile )
					dbg (DEBUG, "\n") ;
				fflush(prlFile) ;
				llRow = ARR_NEXT;
			}
			else
			{
				if ( ipLogFile )
					dbg(DEBUG,"GetRowPointer failed RC=%d",ilRC);
			}
		} while (ilRC == RC_SUCCESS);

		if (ilRC == RC_NOTFOUND)
			ilRC = RC_SUCCESS ;
	} /* end of if */

	if (prlFile != NULL)
	{
		fclose(prlFile) ;
		prlFile = NULL ;
		if ( ipLogFile )
			dbg (TRACE, "SaveIndexInfo: event saved - file <%s>", clFile) ;
	} /* end of if */
	if ( pclRowBuff )
		free ( pclRowBuff );
	return (ilRC) ;

}/* end of SaveIndexInfo */


static int GetDebugLevel(char *pcpMode, int *pipMode)
{
	int		ilRC = RC_SUCCESS;
	char	clCfgValue[64];

	ilRC = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,
							clCfgValue);

	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
	}
	else
	{
		dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
		if (!strcmp(clCfgValue, "DEBUG"))
			*pipMode = DEBUG;
		else if (!strcmp(clCfgValue, "TRACE"))
			*pipMode = TRACE;
		else
			*pipMode = 0;
	}

	return ilRC;
}


/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
