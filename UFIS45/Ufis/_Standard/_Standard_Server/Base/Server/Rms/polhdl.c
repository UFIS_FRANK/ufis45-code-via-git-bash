#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Rms/polhdl.c 1.46 2012/05/11 15:26:44SGT pde Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :  Added Changes for AutoCoverage from FSC                  */
/* 20060123 JIM: added flag AIX                                               */
/* MEI 14-OCT-10 : Due to oracle upgrade, change the JOBTAB oracle hint to configurable */
/* MAX 01-Feb-12 : fix crash when check urno in drrtabarray                   */
/* MAX 07-MAR-12 : ufis-1511 change shift                                     */
/* MAX 11-MAR-12 : ufis-1511 ,fix bug : set absence in rostering,             */
/*                   opss-pm staff name still display                         */
/* PDE 11-MAR-13 : UFIS-1075(3107) ,fix bug : AZ - Temporary Absence                */
/*                   - Server did not sent out updated changes                */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

/* static char sccs_version[] ="@(#) UFIS4.5 (c) ABB AAT/I polhdl.c 4.5.1.9 / 03/08/15 HAG"; */

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "AATArray.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>
#include <tools.h>

#define BSDFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgBsdArray.plrArrayFieldOfs[ipFieldNo]])
#define JOBFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgJobArray.plrArrayFieldOfs[ipFieldNo]])
#define DRRFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgDrrArray.plrArrayFieldOfs[ipFieldNo]])
/*#define STFFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgStfArray.plrArrayFieldOfs[ipFieldNo]])*/
#define FIELDVAL(rrArray,pcpBuf,ipFieldNo) (&pcpBuf[rrArray.plrArrayFieldOfs[ipFieldNo]])
#define PFIELDVAL(prArray,pcpBuf,ipFieldNo) (&pcpBuf[prArray->plrArrayFieldOfs[ipFieldNo]])
extern int get_no_of_items(char *s);

#define MAXDEVIATION (12)
#define MAXMINUTES (2880)
#define MAXINSEL 500		/* maximum number of Staff-URNOs in one Broadcast */

#define CMD_IRT     (0)
#define CMD_URT     (1)
#define CMD_DRT     (2)
#define CMD_CAE     (3)
#define CMD_SBC     (4)
#define CMD_ANSWER  (5)

#define DRDDEL 1
#define DRDDRD 2

#define V_OPERATIVE 1
#define V_RELEASES	2
#define V_BOTH		3

static char *prgCmdTxt[] = { "IRT",   "URT",   "DRT",  "CAE",  "SBC",   "ANSWER", NULL };
static int    rgCmdDef[] = { CMD_IRT, CMD_URT,CMD_DRT, CMD_CAE, CMD_SBC,CMD_ANSWER, 0 };

static int igTestOutput = 9;
#define CMP_FALSE   (1)
#define CMP_SMALLER (2)
#define CMP_EQUAL   (3)
#define CMP_GREATER (4)

#define MAX_FORWARDS 10

#define RC_JOB_TO_CREATE	77
#define RC_JOB_DELETED		88

#define URNOS_TO_FETCH 500

/*  next two variables to undo character mapping performed by classlib when */
/*	writing to database														*/
/*						         "   '  , [LF] [CR] :  {   }   [   ]   |   ^   ?  $  (  )  ;  @	*/
static char cgClientChars[] = "\042\047\54\012\015\072\173\175\133\135\174\136\77\44\50\51\73\100"; 
static char cgServerChars[] = "\260\261\262\264\263\277\223\224\225\226\227\230\231\232\233\234\235\236";

#ifndef min
#define min(a,b) (a < b ? a : b)
#endif

BOOL IS_EMPTY (char *pcpBuf) 
{
	return	(!pcpBuf || *pcpBuf==' ' || *pcpBuf=='\0') ? TRUE : FALSE;
}

#define IDX_IRTD 4	/* Index of List "INSERT" in prgDemInf->DataList */
#define IDX_URTD 5  /* Index of List "UPDATE" in prgDemInf->DataList */
#define IDX_DRTU 6  /* Index of List "DELETE" in prgDemInf->DataList */
#define IDX_IRTU 7	/* Index of List "IRT_URNO" in prgDemInf->DataList */
#define IDX_URTU 8	/* Index of List "URT_URNO" in prgDemInf->DataList */
#define IDX_DRTD 9	/* Index of List "DRT_DATA" in prgDemInf->DataList */

static int rgDIdx[3] = { IDX_IRTD, IDX_URTD, IDX_DRTD };
static AAT_PARAM *prgJobInf=0;

/******************************************************************************/
/***   Fields                                                             *****/
/******************************************************************************/

/*#define BSD_FIELDS "RGSB,VAFR,FCTC,FDEL,DPT1,BSDN,BKD1,LSEN,CTRC,REMA,USEC,SSH1,BEWC,BKR1,BKF1,BSDC,BKT1,BSDS,BSDK,ESBG,CDAT,LSTU,HOPO,SDU1,PRFL,TYPE,SEX1,USEU,URNO,VATO"*/
#define BSD_FIELDS "VAFR,FCTC,DPT1,BSDC,HOPO,URNO,VATO"

#define ODA_FIELDS "HOPO,URNO,SDAC"

/* #define STF_FIELDS "LINO,CDAT,LANM,FINM,REMA,PENO,TOHR,TELD,ZUTN,USEU,MATR,GSMN,DODM,DOEM,KTOU,HOPO,LSTU,PERC,MAKR,PRFL,SHNM,TELH,SKEN,URNO,TELP,ZUTB,USEC,ZUTW,ZUTF"*/
#define STF_FIELDS "PENO,URNO"

#define SOR_FIELDS "CODE,URNO,HOPO,VPFR,SURN,VPTO"

#define SPF_FIELDS "CODE,URNO,HOPO,SURN,VPTO,VPFR,PRIO"

#define SPE_FIELDS "CODE,URNO,HOPO,VPFR,SURN,VPTO,PRIO"

#define SCO_FIELDS "CODE,HOPO,VPTO,VPFR,SURN,URNO"

#define SWG_FIELDS "CODE,VPTO,VPFR,URNO,SURN,HOPO"

#define SDA_FIELDS "UPOL,VPTO,VPFR,SURN,URNO,HOPO"

#define ORG_FIELDS "URNO,DPTN,DPT1,HOPO,DPT2"

#define PFC_FIELDS "URNO,FCTN,PRIO,DPTC,HOPO,FCTC"

#define PER_FIELDS "URNO,PRIO,HOPO,PRMC,PRMN"

#define COT_FIELDS "URNO,WRKD,HOPO,DPTC,CTRC"

/*#define WGP_FIELDS "URNO,WGPC,REMA,PGPU,WGPN,PRFL,HOPO"*/

#define POL_FIELDS "URNO,POOL,NAME,HOPO"

#define DEL_FIELDS "DELT,DELF,BSDU,FCTC,DPT1,URNO,HOPO"
#define DRA_FIELDS "STFU,SDAY,HOPO,SDAC,ABTO,DRRN,ABFR,URNO"

#define DRS_FIELDS "ATS1,DRRU,URNO,STAT,HOPO"

/*
#define DELDRA_FIELDS "DELT,DELF,BSDU,FCTC,DPT1,STFU,SDAY,SDAC,ABTO,DRRN,ABFR"

#define PGP_FIELDS "PGPM,URNO,MAXM,PGPC,TYPE,PGPN,MINM,HOPO"
*/
#define DRR_FIELDS "SBLU,SBLP,ROSS,ROSL,EXPF,DRRN,SCOD,SBTO,SBFR,AVTO,AVFR,HOPO,SDAY,STFU,URNO,BSDU,FCTC"

#define DRD_FIELDS "FCTC,DPT1,PRMC,STFU,SDAY,HOPO,DRRN,URNO,DRDF,DRDT"


#define JOB_DBFIELDS "UALO,UAFT,STAT,UJTY,UDSR,UDRD,UDEL,UAID,TEXT,ACTO,ACFR,PLTO,PLFR,JOUR,USTF,DETY,LSTU,CDAT,USEC,USEU,URNO,HOPO"

/*#define JOB_FIELDS   "UALO,UAFT,STAT,UJTY,UDSR,UDRD,UDEL,UAID,TEXT,ACTO,ACFR,PLTO,PLFR,JOUR,USTF,DETY,LSTU,CDAT,USEC,USEU,URNO,HOPO,XXXX,SCOD"*/


#define POA_FIELDS "URNO,HOPO,UPOL,REFT,UVAL"
#define DRG_FIELDS "DRRN,FCTC,STFU,SDAY,URNO,WGPC"

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = 0;

#if defined(_HPUX_SOURCE) || defined(_AIX)
/* #ifdef _HPUX_SOURCE */
	extern int daylight;
	extern long timezone;
	extern char *tzname[2];
#else
	extern int _daylight;
	extern long _timezone;
	extern char *_tzname[2];
#endif
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;
static int   igInitOK      = FALSE;
static long  lgEvtCnt      = 0;
static char  cgConfigFile[512];
static char  cgDrrReloadHint[81]="";
static char  cgJobReloadHint[80]="";
static char  cgHopo[8];                         /* default home airport    */
static char  cgTabEnd[8];                       /* default table extension */
static	int		igStartUpMode = TRACE;
static	int		igRuntimeMode = 0;
static char  cgProcessName[80];                  /* name of executable */
static char  cgOpStart[24]  = "-1";            /* start of operational period */
static char  cgOpEnd[24]    =  "3";            /* end of operational period */
static char  cgOpFirstDay[24];
static char  cgOpLastDay[24];
static char  cgUTCStart[24]  = "-1";            /* start of operational period in UTC */
static char  cgUTCEnd[24]    =  "3";            /* end of operational period in UTC */

static char  cgUalo[124] = "";
static char  cgUjty[124] = "";
static char  cgUjtyBrk[124] = "";
static char  cgUjtyAbs[124] = "";
static char  cgUjtyDet[33] = "";
static char  cgUjtyDel[33] = "";
static char  cgUjtyDfj[33] = "";
static char  cgUjtyRfj[33] = "";
static char  cgDefaultPool[124] = "";
static char  cgActualUalo[124] = "";

static char  cgSprOffsetFrom[24] = "";
static char  cgSprOffsetTo[24] = "";
static char  cgSprBreakOffsetFrom[24] = "";
static char  cgSprBreakOffsetTo[24] = "";

static char  cgPoaREFT[24] = "";
static char  cgPoaREFT_Field[24] = "";
static char  cgPoaREFT_Table[24] = "";

static char cgTmpBuf[8192];
static char cgNewJobBuf[1024];
static char	cgBrkJobBuf[1024];
static char	cgOrgUnitsForPfcUse[4048]="";

static char pcgCfgBuffer[1024];
	
static char   cgTdi1[24];
static char   cgTdi2[24];
static char   cgTich[24];

static char cgDrrFields[512];
static char cgDelFields[81];
static char cgJobFields[512];

static char myJobUrno[20]="\0";

static BOOL bgBcActiv = TRUE;
static BOOL bgBreak = FALSE;
static BOOL bgSetStartAtClockIn = TRUE;
static BOOL bgSetEndAtClockOut = TRUE;
static BOOL bgSwapShiftEvent = FALSE;
//change shift in opss-pm preplanning,not include resize shift
static BOOL bgChangeShiftEvent = FALSE;

/* static array informations */

#define ARR_NAME_LEN   (18)
#define ARR_FLDLST_LEN (256)

struct DelDrrStruct
{
	char clKey[92];
	char clUrno[24];
	int  ilType;
} rgDelDrr[MAXDEVIATION+1];

int igDelDraFirstFree = 0;
signed char cgJobs[MAXMINUTES+2];
char cgActDay[15];	/* start of shift (local) */
time_t	lgStartofDayUTC;

struct _arrayinfo {
	HANDLE   rrArrayHandle;
	char     crArrayName[ARR_NAME_LEN+1];
	char     crArrayFieldList[ARR_FLDLST_LEN+1];
	long     lrArrayFieldCnt;
	char   *pcrArrayRowBuf;
	long     lrArrayRowLen;
	long   *plrArrayFieldOfs;
	long   *plrArrayFieldLen;
	HANDLE   rrIdx01Handle;
	char     crIdx01Name[ARR_NAME_LEN+1];
	char     crIdx01FieldList[ARR_FLDLST_LEN+1];
	long     lrIdx01FieldCnt;
	char   *pcrIdx01RowBuf;
	long     lrIdx01RowLen;
	long   *plrIdx01FieldPos;
	long   *plrIdx01FieldOrd;

/*****************/
	HANDLE   rrIdx02Handle;
	char     crIdx02Name[ARR_NAME_LEN+1];
	HANDLE   rrIdx03Handle;
	char     crIdx03Name[ARR_NAME_LEN+1];
	HANDLE   rrIdx04Handle;
	char     crIdx04Name[ARR_NAME_LEN+1];
	/****************************/
};
typedef struct _arrayinfo ARRAYINFO;


static ARRAYINFO rgOdaArray;
static ARRAYINFO rgBsdArray;
static ARRAYINFO rgSpfArray;
/*static ARRAYINFO rgStfArray;*/
static ARRAYINFO rgSorArray;
static ARRAYINFO rgSpeArray;
static ARRAYINFO rgScoArray;
static ARRAYINFO rgSwgArray;
static ARRAYINFO rgSdaArray;
static ARRAYINFO rgOrgArray;
static ARRAYINFO rgPfcArray;
static ARRAYINFO rgPerArray;
static ARRAYINFO rgCotArray;
/*static ARRAYINFO rgWgpArray; */
static ARRAYINFO rgPolArray;
static ARRAYINFO rgDelArray;
/*static ARRAYINFO rgPgpArray;*/
static ARRAYINFO rgDrrArray;
static ARRAYINFO rgDrdArray;
static ARRAYINFO rgDraArray;
static ARRAYINFO rgDrsArray;
static ARRAYINFO rgJobArray;
static ARRAYINFO rgPoaArray;
static ARRAYINFO rgDetArray;  /* detached jobs type DET or DEL */
static ARRAYINFO rgRfjArray;  /* restricted flight jobs */

static ARRAYINFO rgBrkArray;
static ARRAYINFO rgNewJobsArray;
static ARRAYINFO rgNewDrrArray;
static ARRAYINFO rgDrgArray;

static ARRAYINFO *prgStfAlidArray = NULL;
static ARRAYINFO *prgAlidArray = NULL;
static int *pigDrdKey = NULL;
static int *pigDelKey = NULL;
static int *pigBsdKey = NULL;
static int *pigDrrKey = NULL;
static BOOL bgUseFunctionsOrgUnit = FALSE;

static int igVafrKey = -1;
static int igVatoKey = -1;
static int igPrioIdx=-1;
static BOOL bgRELJOBtoACTION = FALSE;
static BOOL bgRELJOBtoBCHDL = FALSE;

static int igOdaSDAC;
static int igOdaURNO;
static int igBsdVAFR;
static int igBsdFCTC;
static int igBsdDPT1;
static int igBsdBSDC;
static int igBsdHOPO;
static int igBsdURNO;
static int igBsdVATO;
static int igCotWRKD;
static int igCotHOPO;
static int igCotDPTC;
static int igCotURNO;
static int igDelDELT;
static int igDelDELF;
static int igDelBSDU;
static int igDelFCTC;
static int igDelDPT1;
static int igDelURNO;
static int igDelHOPO;
static int igDelSDAC=-1;
static int igDraSTFU;
static int igDraSDAY;
static int igDraHOPO;
static int igDraSDAC;
static int igDraABTO;
static int igDraDRRN;
static int igDraABFR;
static int igDraURNO;

static int igDrsDRRU;
static int igDrsATS1;
static int igDrsURNO;
static int igDrsHOPO;
static int igDrsSTAT;

static int igDrdFCTC;
static int igDrdDPT1;
static int igDrdPRMC;
static int igDrdSTFU;
static int igDrdSDAY;
static int igDrdHOPO;
static int igDrdDRRN;
static int igDrdURNO;
static int igDrdDRDF;
static int igDrdDRDT;
static int igDrrSBLU;
static int igDrrSBLP;
static int igDrrROSS;
static int igDrrROSL;
static int igDrrEXPF;
static int igDrrDRRN;
static int igDrrSCOD;
static int igDrrSBTO;
static int igDrrSBFR;
static int igDrrAVTO;
static int igDrrAVFR;
static int igDrrHOPO;
static int igDrrSDAY;
static int igDrrSTFU;
static int igDrrURNO;
static int igDrrBSDU;
static int igDrrFCTC;
static int igDrrFILT;
static int igDrrBROS=-1;

static int igJobUALO;
static int igJobUAFT;
static int igJobSTAT;
static int igJobUJTY;
static int igJobUDSR;
static int igJobUDEL;
static int igJobUDRD;
static int igJobUAID;
static int igJobTEXT;
static int igJobACTO;
static int igJobACFR;
static int igJobPLTO;
static int igJobPLFR;
static int igJobJOUR;
static int igJobUSTF;
static int igJobFILT;
static int igJobDETY;
static int igJobLSTU;
static int igJobCDAT;
static int igJobUSEU;
static int igJobUSEC;
static int igJobURNO;
static int igJobHOPO;
static int igJobXXXX;
static int igJobSCOD;
static int igJobFCCO;
static int igJobWGPC;
static int igJobALOC;
static int igJobALID;
static int igDrgDRRN;
static int igDrgSTFU;
static int igDrgSDAY;
static int igDrgWGPC;
static int igDrgFCTC;
static int igOrgDPTN;
static int igOrgDPT1;
static int igOrgHOPO;
static int igOrgDPT2;
static int igOrgURNO;
static int igPerPRIO;
static int igPerHOPO;
static int igPerURNO;
static int igPerPRMC;
static int igPerPRMN;
static int igPfcFCTN;
static int igPfcPRIO;
static int igPfcDPTC;
static int igPfcHOPO;
static int igPfcFCTC;
static int igPfcURNO;
static int igPgpPGPM;
static int igPgpURNO;
static int igPgpMAXM;
static int igPgpPGPC;
static int igPgpTYPE;
static int igPgpPGPN;
static int igPgpMINM;
static int igPgpHOPO;
/*static int igPolPOOL;*/
static int igPolURNO;
static int igPolNAME;
static int igPolHOPO;
static int igSdaVPTO;
static int igSdaVPFR;
static int igSdaSURN;
static int igSdaUPOL;
static int igSdaURNO;
static int igSdaHOPO;
static int igSorCODE;
static int igSorURNO;
static int igSorHOPO;
static int igSorVPFR;
static int igSorSURN;
static int igSorVPTO;
static int igSpfCODE;
static int igSpfURNO;
static int igSpfHOPO;
static int igSpfSURN;
static int igSpfVPTO;
static int igSpfVPFR;
static int igSpfPRIO;

static int igSwgVPTO;
static int igSwgVPFR;
static int igSwgCODE;
/*
static int igStfPENO;
static int igStfURNO;

static int igSwgURNO;
static int igSwgSURN;
static int igSwgHOPO;

static int igWgpWGPC;
static int igWgpURNO;
static int igWgpREMA;
static int igWgpPGPU;
static int igWgpWGPN;
static int igWgpPRFL;
static int igWgpHOPO;*/

static int igPoaURNO;
static int igPoaHOPO;
static int igPoaUPOL;
static int igPoaREFT;
static int igPoaUVAL;

/*
static int igDelDraDELT;
static int igDelDraDELF;
static int igDelDraBSDU;
static int igDelDraFCTC;
static int igDelDraDPT1;
static int igDelDraSTFU;
static int igDelDraSDAY;
static int igDelDraSDAC;
static int igDelDraABTO;
static int igDelDraDRRN;
static int igDelDraABFR;
*/

typedef struct 
{
	char table[33];
	int	 modid;
} FWDEVENT;

FWDEVENT rgForwards[10];

static int igVersion = V_BOTH;
static int igModIdRelVersion=-1;
static char cgRelVersion[20]="";

static long   lgActUrno =0 ;
static int   igReservedUrnoCnt = 0;
static BOOL bgOpssSetsPlanned = TRUE;
static BOOL bgUpdPlannedTimes = TRUE;
static BOOL bgUseFilt = TRUE;

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
int check_ret1(int ipRc,int ipLine);
static int  InitPolhdl();
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, 
		char *pcpArrayFieldList,char *pcpAddFields,long *pcpAddFieldLens,
			ARRAYINFO *prpArrayInfo,char *pcpSelection,BOOL bpFill);
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
	char *pcpCfgBuffer);


static int	Reset(void);                       /* Reset program          */
static void	Terminate(int ipSleep);            /* Terminate program      */
/*static void	HandleSignal(int); */                /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void	HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

static void TrimRight(char *pcpBuffer);
static int  GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina,
							long *pipLen);
static int  GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList,
							long *pipLen);


static int  GetCommand(char *pcpCommand, int *pipCmd);

static int	HandleData(EVENT *prpEvent);       /* Handles event data     */
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo);
static int MySaveIndexInfo (ARRAYINFO *prpArrayInfo);

static int TimeToStr(char *pcpTime,time_t lpTime); 

static int StrToTime(char *pcpTime,time_t *pclTime);
static void  StrAddTime(char *,char*,char *, short);
static int ReCalculateShifts();
static int CreateTmpAbsJob(char *pcpDrrRow,char *pcpPoolJob);
static int CreatePoolJobsForShift(char *pcpDrrRow);
static int UpdatePoolJob(char *pcpOldJobUrno,char *pcpSource);
static int PutJob(char *pcpFrom,char *pcpTo,char cpJobNo);
static int CreatePoolJob(char *pcpDrrRow,int ipJobNo);
static int CreateSinglePoolJob(char *pcpDrrRow,char *pcpOldJobUrno,
		char *pcpFrom,char *pcpTo,int ipJobNo);
static int CheckExistingJob(char *pcpDrrRow);
static int CheckShift(char *pcpDrrRow,char *pcpBsdu);
static int CheckAbsence(char *pcpDrrRow);
static char *GetAlidUrnoFromStf(char *pcpDrrRow);
static char *GetAlidUrnoFromKey(char *pcpKey);
static char *GetAlidUrnoFromBsd(char *pcpKey);
static char *GetAlidUrnoFromDrr(char *pcpDrrRow);
/*static int CreateBc(long lpNumUrnos,char *pcpUpdCmdLst,char *pcpUpdSelLst,
	char *pcpUpdFldLst,char *pcpUpdDatLst);*/
static int LocalToUtc(char *pcpTime);
/*static int MyNewToolsSendSql(int ipRouteID,
						BC_HEAD *prpBchd,
						CMDBLK *prpCmdblk,
						char *pcpSelection,
						char *pcpFields,
						char *pcpData);
static int  TriggerAction(char *pcpTableName, char *pcpCommands);
static int  WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem);
static int  CheckQueue(int ipModId, ITEM **prpItem);*/
static int SavePoolJobs( BOOL bpSendSBC, char *pcpStart, char *pcpEnd );
static int FindItemInArrayList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos);
static int GetUrnoFromSelection(char *pcpSelection,char *pcpUrno);
static int SetSprStatus(char *pcpFields,char *pcpData);

static int	CreateBreakJob( char *pcpNewJobRow,char *pcpDrrRow );
static int DeletePoolJob( char *pcpDrrRow );
static int DeletePoolJobByUdrr ( char *pcpDrrUrno );
static int PreparePoolJob(char *pcpJobUrno);
static int CheckShiftCode(char *pcpDrrUrno);
static int newCheckShiftCode(char *pcpDrrUrno);
static int CheckBreak(char *pcpDrrRow);
static int DoSingleSelect(char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpDataArea);

static int DeleteSplittedPoolJobs(char *pcpDrrUrno);
static int	GetDebugLevel(char *, int *);
/* static int GetRealTimeDiff ( char *pcpLocal1, char *pcpLocal2, time_t *plpDiff ); */
static int AddSecsToCEDATime ( char *pcpTime, time_t lpSecs, short spLocal );
static int UpdatePoolJobs ( char *pcpDrrRow );
static int CopyNewPoolJobs ( char *pcpDrrRow );
static int IsDrrChangeImportant ( char *pcpFields, char *pcpData );
static int SetPooljobStatus ( char cpFcol, char *pcpUdsr );
static int CheckEmployeePresence ();
static int UpdateDetachedJob ( char *pcpPoolJobRow, char * pcpField, char* pcpValue );
static void FillAbsJob ( char *pcpAbsJob, char *pcpFrom, char *pcpTo, char *pcpUdsr, 
					     char *pcpUdel, char *pcpParentJob );
static int AddOneDayLocal ( char *pcpActDay, char *pcpNextDay );
static int HandleRelDrr ( char *pcpBCData );
void UndoCharMapping ( char* pcpString );
static int ProcessDrrChange ( char *pcpDrrUrno, BOOL bpSaveJobs );
static int ProcessChangeShift ( char *pcpDrrUrno, BOOL bpSaveJobs );
static int IniFwdEventArray ();
static int ReloadDrrRecord ( char *pcpFields, char *pcpData );
static int MyHandleDrt ( ARRAYINFO *prpArr, char *pcpSel, char *pcpUrnolist );
static int HandleRelJob (char *pcpBCData, char *pcpBCSelection );
static int GetNextUrno(char *pcpUrno);
static int ReadOrgUnitsForPfcUse();
static void ReadConfigEntries ();
static BOOL SetPlannedTimes ( CMDBLK *prpCmdblk );
static int MyHandleDrdDelete ( char *pcpSel, char *pcpUrnolist );
static int MyHandleDrgDelete ( char *pcpSel, char *pcpUrnolist );
static int GetWgpcForShift ( char *pcpDrrRow, char *pcpWgpc );
static int GetFccoForPjb ( char *pcpDrrRow, char *pcpUdrd, char *pcpUdel, char *pcpFcco );
static int GetValidStaffEntry ( ARRAYINFO *prpArr, char *pcpDrrRow, int ipVafrIdx, 
							    int ipVatoIdx, int ipPrioIdx, char **pcpRowFound );
static int GetPoolName ( char *pcpUpol, char *pcpPoolName );
static int FindDfjForRfj ( char *pcpRfjRow, char *pcpDfjUrno, long *lpDfjRownum );
static int FindPjbForDetached ( char *pcpDetRow, char *pcpUpjb, long *lpPjbRownum );
static int ProcessRfjChange(char *pcpFields, char *pcpData, char *pcpOldData );
static int HandleRfjDelete (char *pcpSel );

//UFIS 1075
static int GetDrrUrnoFromDraFields(char *pcpFields,char *pcpData,char *pcpUrno);

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;

	/************************
	FILE *prlErrFile = NULL;
	char clErrFile[512];
	************************/

	INITIALIZE;			/* General initialization	*/

	/****************************************************************************
	sprintf(&clErrFile[0],"%s/%s%5.5d.err",getenv("DBG_PATH"),mod_name,getpid());
	prlErrFile = freopen(&clErrFile[0],"w",stderr);
	fprintf(stderr,"HoHoHo\n");fflush(stderr);
	dbg(TRACE,"MAIN: error file <%s>",&clErrFile[0]);
	****************************************************************************/

	debug_level = TRACE;

	strcpy(cgProcessName,argv[0]);

	dbg(TRACE,"MAIN: version <%s>",mks_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}else{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */

	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
			check_ret(ilRc);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}else{
		dbg(TRACE,"MAIN: init_db() OK!");
	} /* end of if */

	/* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

	sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	sprintf(cgConfigFile,"%s/polhdl.cfg",getenv("CFG_PATH"));
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */

	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */

	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		if(igInitOK == FALSE)
		{
			ilRc = InitPolhdl();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: init failed!");
			} /* end of if */
		}/* end of if */
	} else {
		Terminate(30);
	}/* end of if */

/***
	dbg(TRACE,"MAIN: initializing OK sleep(60) ...");
	sleep(60);
***/

	dbg(TRACE,"=====================");
	dbg(TRACE,"MAIN: initializing OK");
	dbg(TRACE,"=====================");
	
	debug_level = igRuntimeMode;

	for(;;)
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
			
			lgEvtCnt++;

			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
				Terminate(1);
				break;
					
			case	RESET		:
				ilRc = Reset();
				break;
					
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRc = HandleData(prgEvent);
					if(ilRc != RC_SUCCESS)
					{
						HandleErr(ilRc);
					}/* end of if */
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
				
			case 101 :
						/*DumpDraArray();*/
						/*SaveIndexInfo(&rgStfArray);*/
						SaveIndexInfo(&rgDrrArray);
						SaveIndexInfo(&rgDraArray);
						break;
			case 102 :
						SaveIndexInfo(&rgDrdArray);
						SaveIndexInfo(&rgDrgArray);
						break;
			case 667 :
						/*DumpDraArray();*/
						MySaveIndexInfo(&rgJobArray);
						MySaveIndexInfo(&rgBrkArray);
						MySaveIndexInfo(&rgDetArray);
						break;

			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
		
	} /* end for */
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int FindItemInArrayList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos)
{
	int ilRc = RC_SUCCESS;

	ilRc = FindItemInList(pcpInput,pcpPattern,cpDel,ipLine,ipCol,ipPos);
	*ipLine = (*ipLine) -1;
	
	return ilRc;
}



static int InitFieldIndex()
{
	int ilRc = RC_SUCCESS;
	int ilCol,ilPos;

	/*FindItemInArrayList(DEM_FIELDS,"ALID",',',&igDemALID,&ilCol,&ilPos);*/



	FindItemInArrayList(ODA_FIELDS,"URNO",',',&igOdaURNO,&ilCol,&ilPos);
	FindItemInArrayList(ODA_FIELDS,"SDAC",',',&igOdaSDAC,&ilCol,&ilPos);

	FindItemInArrayList(BSD_FIELDS,"VAFR",',',&igBsdVAFR,&ilCol,&ilPos);
	FindItemInArrayList(BSD_FIELDS,"FCTC",',',&igBsdFCTC,&ilCol,&ilPos);
	FindItemInArrayList(BSD_FIELDS,"DPT1",',',&igBsdDPT1,&ilCol,&ilPos);
	FindItemInArrayList(BSD_FIELDS,"BSDC",',',&igBsdBSDC,&ilCol,&ilPos);
	FindItemInArrayList(BSD_FIELDS,"HOPO",',',&igBsdHOPO,&ilCol,&ilPos);
	FindItemInArrayList(BSD_FIELDS,"URNO",',',&igBsdURNO,&ilCol,&ilPos);
	FindItemInArrayList(BSD_FIELDS,"VATO",',',&igBsdVATO,&ilCol,&ilPos);
	FindItemInArrayList(COT_FIELDS,"WRKD",',',&igCotWRKD,&ilCol,&ilPos);
	FindItemInArrayList(COT_FIELDS,"HOPO",',',&igCotHOPO,&ilCol,&ilPos);
	FindItemInArrayList(COT_FIELDS,"DPTC",',',&igCotDPTC,&ilCol,&ilPos);
	FindItemInArrayList(COT_FIELDS,"URNO",',',&igCotURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgDelFields,"DELT",',',&igDelDELT,&ilCol,&ilPos);
	FindItemInArrayList(cgDelFields,"DELF",',',&igDelDELF,&ilCol,&ilPos);
	FindItemInArrayList(cgDelFields,"BSDU",',',&igDelBSDU,&ilCol,&ilPos);
	FindItemInArrayList(cgDelFields,"FCTC",',',&igDelFCTC,&ilCol,&ilPos);
	FindItemInArrayList(cgDelFields,"DPT1",',',&igDelDPT1,&ilCol,&ilPos);
	FindItemInArrayList(cgDelFields,"URNO",',',&igDelURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgDelFields,"HOPO",',',&igDelHOPO,&ilCol,&ilPos);
	FindItemInArrayList(cgDelFields,"SDAC",',',&igDelSDAC,&ilCol,&ilPos);
	dbg ( TRACE, "Index of DELTAB-field SDAC <%d>", igDelSDAC );

	FindItemInArrayList(DRA_FIELDS,"STFU",',',&igDraSTFU,&ilCol,&ilPos);
	FindItemInArrayList(DRA_FIELDS,"SDAY",',',&igDraSDAY,&ilCol,&ilPos);
	FindItemInArrayList(DRA_FIELDS,"HOPO",',',&igDraHOPO,&ilCol,&ilPos);
	FindItemInArrayList(DRA_FIELDS,"SDAC",',',&igDraSDAC,&ilCol,&ilPos);
	FindItemInArrayList(DRA_FIELDS,"ABTO",',',&igDraABTO,&ilCol,&ilPos);
	FindItemInArrayList(DRA_FIELDS,"DRRN",',',&igDraDRRN,&ilCol,&ilPos);
	FindItemInArrayList(DRA_FIELDS,"ABFR",',',&igDraABFR,&ilCol,&ilPos);
	FindItemInArrayList(DRA_FIELDS,"URNO",',',&igDraURNO,&ilCol,&ilPos);

	FindItemInArrayList(DRS_FIELDS,"URNO",',',&igDrsURNO,&ilCol,&ilPos);
	FindItemInArrayList(DRS_FIELDS,"DRRU",',',&igDrsDRRU,&ilCol,&ilPos);
	FindItemInArrayList(DRS_FIELDS,"ATS1",',',&igDrsATS1,&ilCol,&ilPos);
	FindItemInArrayList(DRS_FIELDS,"HOPO",',',&igDrsHOPO,&ilCol,&ilPos);
	FindItemInArrayList(DRS_FIELDS,"STAT",',',&igDrsSTAT,&ilCol,&ilPos);
	
	FindItemInArrayList(DRD_FIELDS,"FCTC",',',&igDrdFCTC,&ilCol,&ilPos);
	FindItemInArrayList(DRD_FIELDS,"DPT1",',',&igDrdDPT1,&ilCol,&ilPos);
	FindItemInArrayList(DRD_FIELDS,"PRMC",',',&igDrdPRMC,&ilCol,&ilPos);
	FindItemInArrayList(DRD_FIELDS,"STFU",',',&igDrdSTFU,&ilCol,&ilPos);
	FindItemInArrayList(DRD_FIELDS,"SDAY",',',&igDrdSDAY,&ilCol,&ilPos);
	FindItemInArrayList(DRD_FIELDS,"HOPO",',',&igDrdHOPO,&ilCol,&ilPos);
	FindItemInArrayList(DRD_FIELDS,"DRRN",',',&igDrdDRRN,&ilCol,&ilPos);
	FindItemInArrayList(DRD_FIELDS,"URNO",',',&igDrdURNO,&ilCol,&ilPos);
	FindItemInArrayList(DRD_FIELDS,"DRDF",',',&igDrdDRDF,&ilCol,&ilPos);
	FindItemInArrayList(DRD_FIELDS,"DRDT",',',&igDrdDRDT,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SBLU",',',&igDrrSBLU,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SBLP",',',&igDrrSBLP,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"ROSS",',',&igDrrROSS,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"ROSL",',',&igDrrROSL,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"EXPF",',',&igDrrEXPF,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"DRRN",',',&igDrrDRRN,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SCOD",',',&igDrrSCOD,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SBTO",',',&igDrrSBTO,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SBFR",',',&igDrrSBFR,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"AVTO",',',&igDrrAVTO,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"AVFR",',',&igDrrAVFR,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"HOPO",',',&igDrrHOPO,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SDAY",',',&igDrrSDAY,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"STFU",',',&igDrrSTFU,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"URNO",',',&igDrrURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"BSDU",',',&igDrrBSDU,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"FCTC",',',&igDrrFCTC,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"BROS",',',&igDrrBROS,&ilCol,&ilPos);
	if (bgUseFilt)
	{
		FindItemInArrayList(cgDrrFields,"FILT",',',&igDrrFILT,&ilCol,&ilPos);
	}
	dbg ( TRACE, "Index of DRRTAB-fields BROS <%d>", igDrrBROS );


	FindItemInArrayList(cgJobFields,"UALO",',',&igJobUALO,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"UAFT",',',&igJobUAFT,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"STAT",',',&igJobSTAT,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"UJTY",',',&igJobUJTY,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"UDSR",',',&igJobUDSR,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"UDEL",',',&igJobUDEL,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"UDRD",',',&igJobUDRD,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"UAID",',',&igJobUAID,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"TEXT",',',&igJobTEXT,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"ACTO",',',&igJobACTO,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"ACFR",',',&igJobACFR,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"PLTO",',',&igJobPLTO,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"PLFR",',',&igJobPLFR,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"JOUR",',',&igJobJOUR,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"USTF",',',&igJobUSTF,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"DETY",',',&igJobDETY,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"LSTU",',',&igJobLSTU,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"CDAT",',',&igJobCDAT,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"USEC",',',&igJobUSEC,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"USEU",',',&igJobUSEU,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"URNO",',',&igJobURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"HOPO",',',&igJobHOPO,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"XXXX",',',&igJobXXXX,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"SCOD",',',&igJobSCOD,&ilCol,&ilPos);
	if (bgUseFilt)
	{
		FindItemInArrayList(cgJobFields,"FILT",',',&igJobFILT,&ilCol,&ilPos);
	}

	FindItemInArrayList(cgJobFields,"FCCO",',',&igJobFCCO,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"WGPC",',',&igJobWGPC,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"ALOC",',',&igJobALOC,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"ALID",',',&igJobALID,&ilCol,&ilPos);

	FindItemInArrayList(DRG_FIELDS,"WGPC",',',&igDrgWGPC,&ilCol,&ilPos);
	FindItemInArrayList(DRG_FIELDS,"FCTC",',',&igDrgFCTC,&ilCol,&ilPos);
	FindItemInArrayList(DRG_FIELDS,"DRRN",',',&igDrgDRRN,&ilCol,&ilPos);
	FindItemInArrayList(DRG_FIELDS,"STFU",',',&igDrgSTFU,&ilCol,&ilPos);
	FindItemInArrayList(DRG_FIELDS,"SDAY",',',&igDrgSDAY,&ilCol,&ilPos);
	
	FindItemInArrayList(ORG_FIELDS,"DPTN",',',&igOrgDPTN,&ilCol,&ilPos);
	FindItemInArrayList(ORG_FIELDS,"DPT1",',',&igOrgDPT1,&ilCol,&ilPos);
	FindItemInArrayList(ORG_FIELDS,"HOPO",',',&igOrgHOPO,&ilCol,&ilPos);
	FindItemInArrayList(ORG_FIELDS,"DPT2",',',&igOrgDPT2,&ilCol,&ilPos);
	FindItemInArrayList(ORG_FIELDS,"URNO",',',&igOrgURNO,&ilCol,&ilPos);
	FindItemInArrayList(PER_FIELDS,"PRIO",',',&igPerPRIO,&ilCol,&ilPos);
	FindItemInArrayList(PER_FIELDS,"HOPO",',',&igPerHOPO,&ilCol,&ilPos);
	FindItemInArrayList(PER_FIELDS,"URNO",',',&igPerURNO,&ilCol,&ilPos);
	FindItemInArrayList(PER_FIELDS,"PRMC",',',&igPerPRMC,&ilCol,&ilPos);
	FindItemInArrayList(PER_FIELDS,"PRMN",',',&igPerPRMN,&ilCol,&ilPos);
	FindItemInArrayList(PFC_FIELDS,"FCTN",',',&igPfcFCTN,&ilCol,&ilPos);
	FindItemInArrayList(PFC_FIELDS,"PRIO",',',&igPfcPRIO,&ilCol,&ilPos);
	FindItemInArrayList(PFC_FIELDS,"DPTC",',',&igPfcDPTC,&ilCol,&ilPos);
	FindItemInArrayList(PFC_FIELDS,"HOPO",',',&igPfcHOPO,&ilCol,&ilPos);
	FindItemInArrayList(PFC_FIELDS,"FCTC",',',&igPfcFCTC,&ilCol,&ilPos);
	FindItemInArrayList(PFC_FIELDS,"URNO",',',&igPfcURNO,&ilCol,&ilPos);
/*	FindItemInArrayList(PGP_FIELDS,"PGPM",',',&igPgpPGPM,&ilCol,&ilPos);
	FindItemInArrayList(PGP_FIELDS,"URNO",',',&igPgpURNO,&ilCol,&ilPos);
	FindItemInArrayList(PGP_FIELDS,"MAXM",',',&igPgpMAXM,&ilCol,&ilPos);
	FindItemInArrayList(PGP_FIELDS,"PGPC",',',&igPgpPGPC,&ilCol,&ilPos);
	FindItemInArrayList(PGP_FIELDS,"TYPE",',',&igPgpTYPE,&ilCol,&ilPos);
	FindItemInArrayList(PGP_FIELDS,"PGPN",',',&igPgpPGPN,&ilCol,&ilPos);
	FindItemInArrayList(PGP_FIELDS,"MINM",',',&igPgpMINM,&ilCol,&ilPos);
	FindItemInArrayList(PGP_FIELDS,"HOPO",',',&igPgpHOPO,&ilCol,&ilPos);*/
/*	FindItemInArrayList(POL_FIELDS,"POOL",',',&igPolPOOL,&ilCol,&ilPos);*/
	FindItemInArrayList(POL_FIELDS,"URNO",',',&igPolURNO,&ilCol,&ilPos);
	FindItemInArrayList(POL_FIELDS,"NAME",',',&igPolNAME,&ilCol,&ilPos);
	FindItemInArrayList(POL_FIELDS,"HOPO",',',&igPolHOPO,&ilCol,&ilPos);
	FindItemInArrayList(SDA_FIELDS,"VPTO",',',&igSdaVPTO,&ilCol,&ilPos);
	FindItemInArrayList(SDA_FIELDS,"VPFR",',',&igSdaVPFR,&ilCol,&ilPos);
	FindItemInArrayList(SDA_FIELDS,"SURN",',',&igSdaSURN,&ilCol,&ilPos);
	FindItemInArrayList(SDA_FIELDS,"UPOL",',',&igSdaUPOL,&ilCol,&ilPos);
	FindItemInArrayList(SDA_FIELDS,"URNO",',',&igSdaURNO,&ilCol,&ilPos);
	FindItemInArrayList(SDA_FIELDS,"HOPO",',',&igSdaHOPO,&ilCol,&ilPos);
	FindItemInArrayList(SOR_FIELDS,"CODE",',',&igSorCODE,&ilCol,&ilPos);
	FindItemInArrayList(SOR_FIELDS,"URNO",',',&igSorURNO,&ilCol,&ilPos);
	FindItemInArrayList(SOR_FIELDS,"HOPO",',',&igSorHOPO,&ilCol,&ilPos);
	FindItemInArrayList(SOR_FIELDS,"VPFR",',',&igSorVPFR,&ilCol,&ilPos);
	FindItemInArrayList(SOR_FIELDS,"SURN",',',&igSorSURN,&ilCol,&ilPos);
	FindItemInArrayList(SOR_FIELDS,"VPTO",',',&igSorVPTO,&ilCol,&ilPos);
	FindItemInArrayList(SPF_FIELDS,"CODE",',',&igSpfCODE,&ilCol,&ilPos);
	FindItemInArrayList(SPF_FIELDS,"URNO",',',&igSpfURNO,&ilCol,&ilPos);
	FindItemInArrayList(SPF_FIELDS,"HOPO",',',&igSpfHOPO,&ilCol,&ilPos);
	FindItemInArrayList(SPF_FIELDS,"SURN",',',&igSpfSURN,&ilCol,&ilPos);
	FindItemInArrayList(SPF_FIELDS,"VPTO",',',&igSpfVPTO,&ilCol,&ilPos);
	FindItemInArrayList(SPF_FIELDS,"VPFR",',',&igSpfVPFR,&ilCol,&ilPos);
	FindItemInArrayList(SPF_FIELDS,"PRIO",',',&igSpfPRIO,&ilCol,&ilPos);
	
	FindItemInArrayList(SWG_FIELDS,"CODE",',',&igSwgCODE,&ilCol,&ilPos);
	FindItemInArrayList(SWG_FIELDS,"VPTO",',',&igSwgVPTO,&ilCol,&ilPos);
	FindItemInArrayList(SWG_FIELDS,"VPFR",',',&igSwgVPFR,&ilCol,&ilPos);
/*	FindItemInArrayList(STF_FIELDS,"PENO",',',&igStfPENO,&ilCol,&ilPos);
	FindItemInArrayList(STF_FIELDS,"URNO",',',&igStfURNO,&ilCol,&ilPos);

	FindItemInArrayList(SWG_FIELDS,"URNO",',',&igSwgURNO,&ilCol,&ilPos);
	FindItemInArrayList(SWG_FIELDS,"SURN",',',&igSwgSURN,&ilCol,&ilPos);
	FindItemInArrayList(SWG_FIELDS,"HOPO",',',&igSwgHOPO,&ilCol,&ilPos);
	FindItemInArrayList(WGP_FIELDS,"WGPC",',',&igWgpWGPC,&ilCol,&ilPos);
	FindItemInArrayList(WGP_FIELDS,"URNO",',',&igWgpURNO,&ilCol,&ilPos);
	FindItemInArrayList(WGP_FIELDS,"REMA",',',&igWgpREMA,&ilCol,&ilPos);
	FindItemInArrayList(WGP_FIELDS,"PGPU",',',&igWgpPGPU,&ilCol,&ilPos);
	FindItemInArrayList(WGP_FIELDS,"WGPN",',',&igWgpWGPN,&ilCol,&ilPos);
	FindItemInArrayList(WGP_FIELDS,"PRFL",',',&igWgpPRFL,&ilCol,&ilPos);
	FindItemInArrayList(WGP_FIELDS,"HOPO",',',&igWgpHOPO,&ilCol,&ilPos);*/

	FindItemInArrayList(POA_FIELDS,"URNO",',',&igPoaURNO,&ilCol,&ilPos);
	FindItemInArrayList(POA_FIELDS,"HOPO",',',&igPoaHOPO,&ilCol,&ilPos);
	FindItemInArrayList(POA_FIELDS,"UPOL",',',&igPoaUPOL,&ilCol,&ilPos);
	FindItemInArrayList(POA_FIELDS,"REFT",',',&igPoaREFT,&ilCol,&ilPos);
	FindItemInArrayList(POA_FIELDS,"UVAL",',',&igPoaUVAL,&ilCol,&ilPos);

	/*  now checking validity of criterion for poolassignment */
	if ( prgStfAlidArray )
	{
		FindItemInArrayList(prgStfAlidArray->crArrayFieldList,"VPFR",',',&igVafrKey,&ilCol,&ilPos);
		FindItemInArrayList(prgStfAlidArray->crArrayFieldList,"VPTO",',',&igVatoKey,&ilCol,&ilPos);
		dbg ( TRACE, "Indieces in <%s>: VPFR <%d> <%d>", prgStfAlidArray->crArrayName, igVafrKey,
			  igVatoKey );
	}
	return ilRc;

}


static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
	char *pcpCfgBuffer)
{
	int ilRc = RC_SUCCESS;

	char clSection[124];
	char clKeyword[124];

		strcpy(clSection,pcpSection);
		strcpy(clKeyword,pcpKeyword);

		ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
				CFG_STRING,pcpCfgBuffer);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
		} 
		else
		{
			dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
					&clSection[0],&clKeyword[0],pcpCfgBuffer,cgConfigFile);
		}/* end of if */
		return ilRc;
}


static int InitPolhdl()
{
	int  ilRc = RC_SUCCESS;			/* Return code */
  char clSection[64];
  char clKeyword[64];
  char clNow[64];
	/*time_t tlNow;*/
	int ilRc1;
	long pclAddFieldLens[12];
	char pclAddFields[256];
	char pclSqlBuf[2560];
	char pclSelection[1024];
 	short slCursor;
	short slSqlFunc;

	/************************
	void *pvlHandleSignalDummyPointer = HandleSignal;
	void *pvlDebugPrintArrayInfoDummyPointer = DebugPrintArrayInfo;
	void *pvlDummyPointer = ;
	***********************/

	if(ilRc == RC_SUCCESS)
	{
		 GetDebugLevel("STARTUP_MODE", &igStartUpMode);
		 GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
	}
	debug_level = igStartUpMode;

	GetServerTimeStamp("LOC",1,0,clNow);

/*	tlNow = time(NULL);
	dbg(DEBUG,"Now = <%s>",clNow);
	tlNow -= tlNow % 86400;
	TimeToStr(clNow,tlNow); */

	dbg(DEBUG,"Now = <%s>",clNow);

	/* now reading from configfile or from database */


	if(ilRc == RC_SUCCESS)
	{
		/* read HomeAirPort from SGS.TAB */
		memset((void*)&cgHopo[0], 0x00, 8);

		sprintf(&clSection[0],"SYS");
		sprintf(&clKeyword[0],"HOMEAP");

		ilRc = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgHopo[0]);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<InitPolhdl> EXTAB,%s,%s not found in SGS.TAB",&clSection[0], &clKeyword[0]);
			Terminate(30);
		} else {
			dbg(TRACE,"<InitPolhdl> home airport    <%s>",&cgHopo[0]);
		}/* end of if */
	}/* end of if */

	
	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"ALL");
		sprintf(&clKeyword[0],"TABEND");

		ilRc = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgTabEnd[0]);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<InitPolhdl> EXTAB,%s,%s not found in SGS.TAB",&clSection[0], &clKeyword[0]);
			Terminate(30);
		} else {
			dbg(TRACE,"<InitPolhdl> table extension <%s>",&cgTabEnd[0]);
		}/* end of if */
	}/* end of if */

        #ifndef _LINUX
	#if defined(_HPUX_SOURCE) || defined(_AIX)
	dbg ( TRACE, "Initial values daylight <%d> timezone <%ld>, zone0 <%s> zone1 <%s>",
				 daylight, timezone, tzname[0], tzname[1] );
	daylight = 0;
	#else
	dbg ( TRACE, "Initial values _daylight <%d> _timezone <%ld>, zone0 <%s> zone1 <%s>",
				 _daylight, _timezone, _tzname[0], _tzname[1] );
	_daylight = 0;
	#endif
        #endif

	if(ilRc == RC_SUCCESS)
	{
		if( ReadConfigEntry("MAIN","2ndInstance",pcgCfgBuffer) == RC_SUCCESS)
		{
			if ( !strcmp ( pcgCfgBuffer, mod_name ) )
				igVersion = V_RELEASES;	/* this instance is the 2nd instance, i. e. handling RELDRR */
			else
			{
				if((igModIdRelVersion = tool_get_q_id(pcgCfgBuffer)) > 0 )
				{	/* another instance called pcpCfgBuffer is handling RELDRR */
					igVersion = V_OPERATIVE;
					strncpy ( cgRelVersion, pcgCfgBuffer, 19 );
					cgRelVersion[19]='\0';
				}	
			}
			dbg ( TRACE, "InitPolhdl: Entry <2ndInstance> read, Version <%d> RelVersion <%s> Id<%d>", 
				  igVersion, cgRelVersion, igModIdRelVersion );
		}
		else
			dbg ( TRACE, "InitPolhdl: Entry <2ndInstance> not found" );
	}

	if(ilRc == RC_SUCCESS)
	{
		ilRc = ReadConfigEntry("MAIN","TESTSTART",pcgCfgBuffer);
		if(ilRc == RC_SUCCESS)
		{
			strcpy ( cgOpStart, pcgCfgBuffer);
		}
		else
		{
			if ( igVersion == V_RELEASES )
				strcpy ( clSection, mod_name );
			else
				strcpy ( clSection, "MAIN" );
			ilRc = ReadConfigEntry(clSection,"OPSTART",pcgCfgBuffer);
			if(ilRc == RC_SUCCESS)
			{
				StrAddTime(clNow,pcgCfgBuffer,cgOpStart, 0);
			}
			ilRc = RC_SUCCESS;
		}
		/* PRF5780: round start of time frame to begin of day (local) */
		cgOpStart[8] = '\0';
		strcpy ( cgOpFirstDay, cgOpStart );
		strcat(cgOpStart,"000000" );	
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = ReadConfigEntry("MAIN","TESTEND",pcgCfgBuffer);
		if(ilRc == RC_SUCCESS)
		{
			strcpy(cgOpEnd,pcgCfgBuffer);
		}
		else
		{
			if ( igVersion == V_RELEASES )
				strcpy ( clSection, mod_name );
			else
				strcpy ( clSection, "MAIN" );
			ilRc = ReadConfigEntry(clSection,"OPEND",pcgCfgBuffer);
			if(ilRc == RC_SUCCESS)
			{
				StrAddTime(clNow,pcgCfgBuffer,cgOpEnd,0);
			}
			ilRc = RC_SUCCESS;
		}
		/* hag 09.10.2003: last SDAY must be day before, otherwise timeframe  */
		/* of JOBTAB is less than timeframe for DRRTAB -> duplicate pooljobs  */
		/* in ReCalculateShifts												  */
		/*
		AddSecsToCEDATime(cgOpLastDay, -(20*3600), 0);
		cgOpLastDay[8] = '\0';	
		if ( strcmp(cgOpLastDay,cgOpFirstDay) < 0 )
		{
			strcpy ( cgOpLastDay, cgOpFirstDay );
			dbg ( TRACE, "InitPolhdl: Set LastDay to FirstDay" );
		} */
		/* PRF5780: round start of time frame to begin of day (local) */
		cgOpEnd[8] = '\0';
		strcpy ( cgOpLastDay, cgOpEnd );
		strcat(cgOpEnd,"235959" );	
	}
	ReadConfigEntries ();

	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayInitialize(20,20);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: CEDAArrayInitialize failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */

	sprintf(pclSqlBuf,
		"SELECT URNO FROM ALOTAB WHERE ALOC = 'POOL' AND HOPO = '%s'",cgHopo);
	slSqlFunc = START;
	slCursor = 0;
	ilRc = sql_if(slSqlFunc,&slCursor,pclSqlBuf,cgUalo);
	dbg(TRACE,"%05d:cgUALO <%s>",__LINE__,cgUalo);
	if (ilRc != RC_SUCCESS)
	{
			dbg(TRACE,"Error reading UALO: %d",ilRc);
			check_ret1(ilRc,__LINE__);
	}
	close_my_cursor(&slCursor);

	sprintf(pclSqlBuf,
		"SELECT URNO FROM JTYTAB WHERE NAME = 'POL' AND HOPO = '%s'",cgHopo);
	slSqlFunc = START;
	slCursor = 0;
	ilRc = sql_if(slSqlFunc,&slCursor,pclSqlBuf,cgUjty);
	dbg(TRACE,"%05d:cgUJTY <%s>",__LINE__,cgUjty);
	if (ilRc != RC_SUCCESS)
	{
			dbg(TRACE,"Error reading UJTY: %d",ilRc);
			check_ret1(ilRc,__LINE__);
	}
	close_my_cursor(&slCursor);

	sprintf(pclSqlBuf,
		"SELECT URNO FROM JTYTAB WHERE NAME = 'BRK' AND HOPO = '%s'",cgHopo);
	slSqlFunc = START;
	slCursor = 0;
	ilRc = sql_if(slSqlFunc,&slCursor,pclSqlBuf,cgUjtyBrk);
	dbg(TRACE,"%05d:cgUJTYBrk <%s>",__LINE__,cgUjtyBrk);
	if (ilRc != RC_SUCCESS)
	{
			dbg(TRACE,"Error reading UJTYBrk: %d",ilRc);
			check_ret1(ilRc,__LINE__);
	}
	close_my_cursor(&slCursor);

	sprintf(pclSqlBuf,
		"SELECT URNO FROM JTYTAB WHERE NAME = 'TABS' AND HOPO = '%s'",cgHopo);
	slSqlFunc = START;
	slCursor = 0;
	ilRc = sql_if(slSqlFunc,&slCursor,pclSqlBuf,cgUjtyAbs);
	dbg(TRACE,"%05d:cgUJTYAbs <%s>",__LINE__,cgUjtyAbs);
	if (ilRc != RC_SUCCESS)
	{
			dbg(TRACE,"Error reading UJTYAbs: %d",ilRc);
			check_ret1(ilRc,__LINE__);
			ilRc = RC_SUCCESS;
	}
	close_my_cursor(&slCursor);

	sprintf(pclSqlBuf,
		"SELECT URNO FROM JTYTAB WHERE NAME ='DEL' AND HOPO = '%s'",cgHopo);
	slSqlFunc = START;
	slCursor = 0;
	ilRc = sql_if(slSqlFunc,&slCursor,pclSqlBuf,cgUjtyDel);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"Error reading UJTYDel: %d",ilRc);
		check_ret1(ilRc,__LINE__);
		ilRc = RC_SUCCESS;
		strcpy ( cgUjtyDel, "2016" );
	}
	else
	{
		TrimRight(cgUjtyDel);
		dbg(TRACE,"%05d:cgUJTYDel <%s>",__LINE__,cgUjtyDel);
	}
	close_my_cursor(&slCursor);

	sprintf(pclSqlBuf,
		"SELECT URNO FROM JTYTAB WHERE NAME ='DET' AND HOPO = '%s'",cgHopo);
	slSqlFunc = START;
	slCursor = 0;
	ilRc = sql_if(slSqlFunc,&slCursor,pclSqlBuf,cgUjtyDet);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"Error reading UJTYDet: %d",ilRc);
		check_ret1(ilRc,__LINE__);
		ilRc = RC_SUCCESS;
		strcpy ( cgUjtyDet, "2008" );
	}
	else
	{
		TrimRight(cgUjtyDet);
		dbg(TRACE,"%05d:cgUJTYDet <%s>",__LINE__,cgUjtyDet);
	}
	close_my_cursor(&slCursor);

	/* SATS-RFC Delegated flight jobs */
	sprintf(pclSqlBuf,
		"SELECT URNO FROM JTYTAB WHERE NAME ='DFJ' AND HOPO = '%s'",cgHopo);
	slSqlFunc = START;
	slCursor = 0;
	ilRc = sql_if(slSqlFunc,&slCursor,pclSqlBuf,cgUjtyDfj);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"Error reading UJTYDfj: %d",ilRc);
		check_ret1(ilRc,__LINE__);
		ilRc = RC_SUCCESS;
		strcpy ( cgUjtyDfj, "2021" );
	}
	else
	{
		TrimRight(cgUjtyDfj);
		dbg(TRACE,"InitPolhdl:  cgUjtyDfj <%s>", cgUjtyDfj);
	}
	close_my_cursor(&slCursor);

	/* SATS-RFC Restricted flight jobs */
	sprintf(pclSqlBuf,
		"SELECT URNO FROM JTYTAB WHERE NAME ='RFJ' AND HOPO = '%s'",cgHopo);
	slSqlFunc = START;
	slCursor = 0;
	ilRc = sql_if(slSqlFunc,&slCursor,pclSqlBuf,cgUjtyRfj);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"Error reading UJTYRfj: %d",ilRc);
		check_ret1(ilRc,__LINE__);
		ilRc = RC_SUCCESS;
		strcpy ( cgUjtyRfj, "2022" );
	}
	else
	{
		TrimRight(cgUjtyRfj);
		dbg(TRACE,"InitPolhdl:  cgUjtyRfj <%s>", cgUjtyRfj);
	}
	close_my_cursor(&slCursor);


	if(ilRc == RC_SUCCESS)
	{
	/** read TICH ***********/
		char pclDataArea[2256];
		short slCursor = 0;
		sprintf(pclSqlBuf,
			"SELECT TICH,TDI1,TDI2 FROM APTTAB WHERE APC3 = '%s'",cgHopo);
		ilRc = sql_if(START,&slCursor,pclSqlBuf,pclDataArea);
		if (ilRc != DB_SUCCESS)
		{
			dbg(TRACE,"Home Airport <%s> not found in APTTAB %d",cgHopo,ilRc);
			check_ret(ilRc);
		} /* end while */
		else
		{
			strcpy(cgTdi1,"60");
			strcpy(cgTdi2,"60");
			get_fld(pclDataArea,0,STR,14,cgTich);
			get_fld(pclDataArea,1,STR,14,cgTdi1);
			get_fld(pclDataArea,2,STR,14,cgTdi2);
			TrimRight(cgTich);
			if (*cgTich != '\0')
			{
				TrimRight(cgTdi1);
				TrimRight(cgTdi2);
				sprintf(cgTdi1,"%d",atoi(cgTdi1)*60);
				sprintf(cgTdi2,"%d",atoi(cgTdi2)*60);
			}
			dbg(TRACE,"Tich: <%s> Tdi1: <%s> Tdi2 <%s>",
				cgTich,cgTdi1,cgTdi2);
		}
		close_my_cursor(&slCursor);
		slCursor = 0;
	}

	if(ilRc == RC_SUCCESS)
	{
		strcpy ( cgUTCStart, cgOpStart );
		LocalToUtc(cgUTCStart);
		strcpy ( cgUTCEnd, cgOpEnd );
		LocalToUtc(cgUTCEnd);
		dbg ( TRACE, "InitPolhdl: Timeframe local <%s> - <%s>", cgOpStart, cgOpEnd );
		dbg ( TRACE, "InitPolhdl: Timeframe UTC   <%s> - <%s>", cgUTCStart, cgUTCEnd );
		dbg ( TRACE, "InitPolhdl: Timeframe SDAYs <%s> - <%s>", cgOpFirstDay, cgOpLastDay );
	}

	ilRc1 = IniFwdEventArray ();
	dbg ( TRACE, "InitPolhdl: IniFwdEventArray found <%d> entries", ilRc1 );
/*
	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);
		// hag 30.07.2003: additional field to save status "New pooljob created" 
		strcpy(pclAddFields,"NEWJ");
		pclAddFieldLens[0] = 1;
		pclAddFieldLens[1] = 0;

		dbg(TRACE,"InitPolhdl: SetArrayInfo STFTAB array");
		ilRc = SetArrayInfo("STFTAB","STFTAB",STF_FIELDS,
				pclAddFields,pclAddFieldLens,&rgStfArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo STFTAB failed <%d>",ilRc);
		}
		else
		{
			strcpy(rgStfArray.crIdx01Name,"NEWJOB");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgStfArray.rrArrayHandle,
												rgStfArray.crArrayName,
												&rgStfArray.rrIdx01Handle,
												rgStfArray.crIdx01Name,"NEWJ");
			if (ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Index <%s> not created RC=%d", rgStfArray.crIdx01Name,ilRc);
			}
			else
			{
				strcpy(rgStfArray.crIdx02Name,"URNO");
				ilRc = CEDAArrayCreateSimpleIndexUp(&rgStfArray.rrArrayHandle,
													rgStfArray.crArrayName,
													&rgStfArray.rrIdx02Handle,
													rgStfArray.crIdx02Name,"URNO");
				if (ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"Index <%s> not created RC=%d", rgStfArray.crIdx02Name,ilRc);
				}
			}
			if (ilRc == RC_SUCCESS)
			{
				dbg(DEBUG,"InitPolhdl: SetArrayInfo STFTAB OK");
				dbg(DEBUG,"<%s> %d",rgStfArray.crArrayName, rgStfArray.rrArrayHandle);
				ilRc = ConfigureAction("STFTAB", 0, "IRT,DRT" );
				if(ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"InitPolhdl: ConfigureAction failed <%d>",ilRc);
				}
			}
		}
	}
	ChangeDynActionConfig ( "STFTAB", 0, "IRT,DRT", FALSE, TRUE );
	**/
	/*  new SavePoolJobs doesn't need STFTAB any longer in memory */

	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitPolhdl: SetArrayInfo SORTAB array");
		ilRc = SetArrayInfo("SORTAB","SORTAB",SOR_FIELDS,
				NULL,NULL,&rgSorArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo SORTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo SORTAB OK");
			dbg(DEBUG,"<%s> %d",rgSorArray.crArrayName,
				rgSorArray.rrArrayHandle);
			ilRc = ConfigureAction("SORTAB", 0, 0);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: ConfigureAction failed <%d>",ilRc);
			}/* end of if */
		}/* end of if */
	}

	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitPolhdl: SetArrayInfo SPFTAB array");
		ilRc = SetArrayInfo("SPFTAB","SPFTAB",SPF_FIELDS,
				NULL,NULL,&rgSpfArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo SPFTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo SPFTAB OK");
			dbg(DEBUG,"<%s> %d",rgSpfArray.crArrayName,
				rgSpfArray.rrArrayHandle);
			ilRc = ConfigureAction("SPFTAB", 0, 0);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: ConfigureAction failed <%d>",ilRc);
			}/* end of if */
		}/* end of if */
	}

	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitPolhdl: SetArrayInfo SPETAB array");
		ilRc = SetArrayInfo("SPETAB","SPETAB",SPE_FIELDS,
				NULL,NULL,&rgSpeArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo SPETAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo SPETAB OK");
			dbg(DEBUG,"<%s> %d",rgSpeArray.crArrayName,
				rgSpeArray.rrArrayHandle);
			ilRc = ConfigureAction("SPETAB", 0, 0);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: ConfigureAction failed <%d>",ilRc);
			}/* end of if */
		}/* end of if */
	}

	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitPolhdl: SetArrayInfo SCOTAB array");
		ilRc = SetArrayInfo("SCOTAB","SCOTAB",SCO_FIELDS,
				NULL,NULL,&rgScoArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo SCOTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo SCOTAB OK");
			dbg(DEBUG,"<%s> %d",rgScoArray.crArrayName,
				rgScoArray.rrArrayHandle);
			ilRc = ConfigureAction("SCOTAB", 0, 0);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: ConfigureAction failed <%d>",ilRc);
			}/* end of if */
		}/* end of if */
	}

	strcpy ( cgJobFields, JOB_DBFIELDS );

	if (bgUseFilt)
	{
		strcat(cgJobFields,",FILT");
	}
	if ( GetFieldLength(cgHopo,"JOBTAB","FCCO",pclAddFieldLens ) == RC_SUCCESS )
	{
		strcat ( cgJobFields, ",FCCO" );
	}
	else
	{
		dbg ( TRACE,"InitPolhdl: field JOBTAB.FCCO not in DB configuration" );
	}

	if ( GetFieldLength(cgHopo,"JOBTAB","ALID",pclAddFieldLens ) == RC_SUCCESS )
	{
		strcat ( cgJobFields, ",ALID" );
	}
	else
	{
		dbg ( TRACE,"InitPolhdl: field JOBTAB.ALID not in DB configuration" );
	}
	if ( GetFieldLength(cgHopo,"JOBTAB","ALOC",pclAddFieldLens ) == RC_SUCCESS )
	{
		strcat ( cgJobFields, ",ALOC" );
	}
	else
	{
		dbg ( TRACE,"InitPolhdl: field JOBTAB.ALID not in DB configuration" );
	}

	rgSwgArray.rrArrayHandle = rgDrgArray.rrArrayHandle = -1;
	if( (ilRc == RC_SUCCESS) && 
		( GetFieldLength( cgHopo, "JOBTAB", "WGPC", pclAddFieldLens ) == RC_SUCCESS ) )
	{
		strcat ( cgJobFields, ",WGPC" );
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitPolhdl: SetArrayInfo SWGTAB array");
		ilRc = SetArrayInfo("SWGTAB","SWGTAB",SWG_FIELDS,
				NULL,NULL,&rgSwgArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo SWGTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo SWGTAB OK");
			dbg(DEBUG,"<%s> %d",rgSwgArray.crArrayName,
				rgSwgArray.rrArrayHandle);
			ilRc = ConfigureAction("SWGTAB", SWG_FIELDS, 0);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: ConfigureAction failed <%d>",ilRc);
			}
			strcpy(rgSwgArray.crIdx01Name,"SWG_SURN");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgSwgArray.rrArrayHandle,
												rgSwgArray.crArrayName,
												&rgSwgArray.rrIdx01Handle,
												rgSwgArray.crIdx01Name,"SURN");
			strcpy(rgSwgArray.crIdx01FieldList,"SURN");
			if ( ilRc != RC_SUCCESS )
				dbg(TRACE,"InitPolhdl: CEDAArrayCreateSimpleIndexUp SWGTAB failed <%d>",ilRc);
		}
		if(ilRc == RC_SUCCESS)
		{
			sprintf(pclSelection,"WHERE SDAY  <=  '%s' AND SDAY >= '%s' AND HOPO= '%s'",
					cgOpLastDay,cgOpFirstDay,cgHopo);
			dbg(TRACE,"InitPolhdl: SetArrayInfo DRGTAB array");
			ilRc = SetArrayInfo("DRGTAB","DRGTAB",DRG_FIELDS,
								NULL,NULL,&rgDrgArray,pclSelection,TRUE);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: SetArrayInfo DRGTAB failed <%d>",ilRc);
			}
			else
			{
				dbg(DEBUG,"InitPolhdl: SetArrayInfo DRGTAB OK");
				dbg(DEBUG,"<%s> %d",rgDrgArray.crArrayName, rgDrgArray.rrArrayHandle);

				if ( igVersion & V_OPERATIVE )
					ilRc = ConfigureAction("DRGTAB", DRG_FIELDS, 0 );
				else
					ilRc = ChangeDynActionConfig ( "DRGTAB", DRG_FIELDS, 0, FALSE, TRUE );

				if(ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"InitPolhdl: ChangeDynActionConfig <DRGTAB> failed <%d>",ilRc);
				}
				strcpy(rgDrgArray.crIdx01Name,"DRG_IDX");
				ilRc = CEDAArrayCreateSimpleIndexUp(&rgDrgArray.rrArrayHandle,
													rgDrgArray.crArrayName,
													&rgDrgArray.rrIdx01Handle,
													rgDrgArray.crIdx01Name,"DRRN,SDAY,STFU");
				strcpy(rgDrgArray.crIdx01FieldList,"DRRN,SDAY,STFU");
				if ( ilRc != RC_SUCCESS )
					dbg(TRACE,"InitPolhdl: CEDAArrayCreateSimpleIndexUp DRGTAB failed <%d>",ilRc);
			}
			if ( ilRc == RC_SUCCESS )
			{
				strcpy(rgDrgArray.crIdx02Name,"DRG_URNO");
				ilRc = CEDAArrayCreateSimpleIndexUp(&rgDrgArray.rrArrayHandle,
													rgDrgArray.crArrayName,
													&rgDrgArray.rrIdx02Handle,
													rgDrgArray.crIdx02Name,"URNO");
			}
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitPolhdl: SetArrayInfo SDATAB array");
		ilRc = SetArrayInfo("SDATAB","SDATAB",SDA_FIELDS,
				NULL,NULL,&rgSdaArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo SDATAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo SDATAB OK");
			dbg(DEBUG,"<%s> %d",rgSdaArray.crArrayName,
				rgSdaArray.rrArrayHandle,TRUE);
			ilRc = ConfigureAction("SDATAB", 0, 0);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: ConfigureAction failed <%d>",ilRc);
			}/* end of if */
		}/* end of if */
	}


	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitPolhdl: SetArrayInfo ORGTAB array");
		ilRc = SetArrayInfo("ORGTAB","ORGTAB",ORG_FIELDS,
				NULL,NULL,&rgOrgArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo ORGTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo ORGTAB OK");
			dbg(DEBUG,"<%s> %d",rgOrgArray.crArrayName,
				rgOrgArray.rrArrayHandle);
			ilRc = ConfigureAction("ORGTAB", 0, 0);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: ConfigureAction failed <%d>",ilRc);
			}/* end of if */
		}/* end of if */
	}

	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitPolhdl: SetArrayInfo PFCTAB array");
		ilRc = SetArrayInfo("PFCTAB","PFCTAB",PFC_FIELDS,
				NULL,NULL,&rgPfcArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo PFCTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo PFCTAB OK");
			dbg(DEBUG,"<%s> %d",rgPfcArray.crArrayName,
				rgPfcArray.rrArrayHandle);
			ilRc = ConfigureAction("PFCTAB", 0, 0);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: ConfigureAction failed <%d>",ilRc);
			}/* end of if */
		}/* end of if */
	}

	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitPolhdl: SetArrayInfo PERTAB array");
		ilRc = SetArrayInfo("PERTAB","PERTAB",PER_FIELDS,
				NULL,NULL,&rgPerArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo PERTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo PERTAB OK");
			dbg(DEBUG,"<%s> %d",rgPerArray.crArrayName,
				rgPerArray.rrArrayHandle);
			ilRc = ConfigureAction("PERTAB", 0, 0);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: ConfigureAction failed <%d>",ilRc);
			}/* end of if */
		}/* end of if */
	}
	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitPolhdl: SetArrayInfo COTTAB array");
		ilRc = SetArrayInfo("COTTAB","COTTAB",COT_FIELDS,
				NULL,NULL,&rgCotArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo COTTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo COTTAB OK");
			dbg(DEBUG,"<%s> %d",rgCotArray.crArrayName,
				rgCotArray.rrArrayHandle);
			ilRc = ConfigureAction("COTTAB", 0, 0);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: ConfigureAction failed <%d>",ilRc);
			}/* end of if */
		}/* end of if */
	}
	/*
	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitPolhdl: SetArrayInfo WGPTAB array");
		ilRc = SetArrayInfo("WGPTAB","WGPTAB",WGP_FIELDS,
				NULL,NULL,&rgWgpArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo WGPTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo WGPTAB OK");
			dbg(DEBUG,"<%s> %d",rgWgpArray.crArrayName,
				rgWgpArray.rrArrayHandle);
			ilRc = ConfigureAction("WGPTAB", WGP_FIELDS, 0);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: ConfigureAction failed <%d>",ilRc);
			}
		}
	}
	*/
	if(ilRc == RC_SUCCESS)
	{

		dbg(TRACE,"InitPolhdl: SetArrayInfo POLTAB array");
		ilRc = SetArrayInfo("POLTAB","POLTAB",POL_FIELDS,
				NULL,NULL,&rgPolArray,NULL,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo POLTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo POLTAB OK");
			dbg(DEBUG,"<%s> %d",rgPolArray.crArrayName,
				rgPolArray.rrArrayHandle);
			ilRc = ConfigureAction("POLTAB", 0, 0);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: ConfigureAction failed <%d>",ilRc);
			}/* end of if */
		}/* end of if */
		if ( ilRc == RC_SUCCESS )
		{
			strcpy(rgPolArray.crIdx02Name,"POL_URNO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgPolArray.rrArrayHandle,
												rgPolArray.crArrayName,
												&rgPolArray.rrIdx02Handle,
												rgPolArray.crIdx02Name,"URNO");
			if ( ilRc != RC_SUCCESS )
				dbg ( TRACE, "InitPolhdl: CEDAArrayCreateSimpleIndexUp <POL_URNO> failed RC <%d>", ilRc );
		}

	}
	if(ilRc == RC_SUCCESS)
	{

		dbg(TRACE,"InitPolhdl: SetArrayInfo BREAK array");
		sprintf(pclSelection,"WHERE ACFR < '%s' AND ACTO > '%s' AND UJTY = '%s' AND HOPO = '%s'",
			cgUTCEnd,cgUTCStart,cgUjtyBrk,cgHopo);

       	if(  GetRowLength(&cgHopo[0],"JOBTAB", "JOUR", &(rgBrkArray.lrIdx01RowLen) )
			 != RC_SUCCESS )
			 rgBrkArray.lrIdx01RowLen = 0;
		ilRc = SetArrayInfo("BREAK","JOBTAB",cgJobFields,
				NULL,NULL,&rgBrkArray,pclSelection,FALSE);
		if(ilRc != RC_SUCCESS)
			dbg(TRACE,"InitPolhdl: SetArrayInfo BREAK failed <%d>",ilRc);
		else
		{
			ilRc = CEDAArrayFillHint(&rgBrkArray.rrArrayHandle,rgBrkArray.crArrayName,
									 0, cgJobReloadHint  );
			/*  if bgRELJOBtoBCHDL==TRUE switch off CEDAArraySendChanges2BCHDL during initialization */
			CEDAArraySendChanges2BCHDL(&rgBrkArray.rrArrayHandle,
										rgBrkArray.crArrayName,!bgRELJOBtoBCHDL );
			/*  if bgRELJOBtoACTION==TRUE switch off CEDAArraySendChanges2ACTION during initialization */
            CEDAArraySendChanges2ACTION(&rgBrkArray.rrArrayHandle,rgBrkArray.crArrayName,
										!bgRELJOBtoACTION);

			dbg(DEBUG,"InitPolhdl: SetArrayInfo BREAK OK");
			dbg(DEBUG,"<%s> %d",rgBrkArray.crArrayName,
				rgBrkArray.rrArrayHandle);

			sprintf(pclSelection,
				"((UJTY ==  %s) && (HOPO == %s))",cgUjtyBrk,cgHopo);

			ilRc = CEDAArraySetFilter(&rgBrkArray.rrArrayHandle,
				rgBrkArray.crArrayName,pclSelection);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: SetFilter BREAK failed <%d>",ilRc);
			}
			else
			{

				strcpy(rgBrkArray.crIdx01Name,"BRK_JOUR");
				ilRc = CEDAArrayCreateSimpleIndexUp(&rgBrkArray.rrArrayHandle,
					rgBrkArray.crArrayName,
					&rgBrkArray.rrIdx01Handle,
					rgBrkArray.crIdx01Name,"JOUR");
				if (ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"Index <%s> not created RC=%d",
					rgBrkArray.crIdx01Name,ilRc);
				}
				strcpy(rgBrkArray.crIdx02Name,"BRK_URNO");
				ilRc = CEDAArrayCreateSimpleIndexUp(&rgBrkArray.rrArrayHandle,
								rgBrkArray.crArrayName, &rgBrkArray.rrIdx02Handle,
								rgBrkArray.crIdx02Name,"URNO");
				if (ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"Index <%s> not created RC=%d", rgBrkArray.crIdx02Name,ilRc);
				}
			}

		}
	}
	/* Array for detached JOBS temp. assignments to pools and groups */
	if(ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"InitPolhdl: SetArrayInfo DETACHED array");
		sprintf(pclSelection,"WHERE ACFR < '%s' AND ACTO > '%s' AND HOPO = '%s' AND UJTY IN ('%s','%s','%s')",
			cgUTCEnd,cgUTCStart,cgHopo,cgUjtyDet,cgUjtyDel,cgUjtyDfj);

		dbg ( TRACE, "InitPolhdl: Selection for Detached Jobs:" );
		dbg ( TRACE, "InitPolhdl: %s", pclSelection );

       	if(  GetRowLength(&cgHopo[0],"JOBTAB", "UDSR,UAID,PLFR,PLTO", &(rgDetArray.lrIdx01RowLen) )
			 != RC_SUCCESS )
			 rgDetArray.lrIdx01RowLen = 0;

		ilRc = SetArrayInfo("DETACHED","JOBTAB",cgJobFields,
				NULL,NULL,&rgDetArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
			dbg(TRACE,"InitPolhdl: SetArrayInfo DETACHED failed <%d>",ilRc);
		else
		{
			CEDAArraySendChanges2BCHDL(&rgDetArray.rrArrayHandle,
										rgDetArray.crArrayName,TRUE);
            CEDAArraySendChanges2ACTION(&rgDetArray.rrArrayHandle,rgDetArray.crArrayName,TRUE);
			dbg(DEBUG,"InitPolhdl: SetArrayInfo DETACHED OK");
			dbg(DEBUG,"<%s> %d",rgDetArray.crArrayName,
				rgDetArray.rrArrayHandle);

			if ( strlen ( pclSqlBuf ) > 0 )
				sprintf(pclSelection, 
					    "((ACFR < %s) && (ACTO > %s) && ( ((UJTY == %s) || (UJTY == %s) ) || (UJTY == %s)) && (HOPO == %s))",
						cgUTCEnd,cgUTCStart,cgUjtyDet,cgUjtyDel,cgUjtyDfj,cgHopo);
			dbg ( TRACE, "InitPolhdl: Selection for filter Detached Jobs:" );
			dbg ( TRACE, "InitPolhdl: %s", pclSelection );

			ilRc = CEDAArraySetFilter(&rgDetArray.rrArrayHandle,
									  rgDetArray.crArrayName,pclSelection);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: SetFilter DETACHED failed <%d>",ilRc);
			}
			else
			{

				strcpy(rgDetArray.crIdx01Name,"DETACHED");
				ilRc = CEDAArrayCreateSimpleIndexUp(&rgDetArray.rrArrayHandle,
													rgDetArray.crArrayName,
													&rgDetArray.rrIdx01Handle,
													rgDetArray.crIdx01Name,
													"UDSR,UAID,PLFR,PLTO");
				if (ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"Index <%s> not created RC=%d",
						rgDetArray.crIdx01Name,ilRc);
				}
				strcpy(rgDetArray.crIdx02Name,"DFJ");
				ilRc = CEDAArrayCreateSimpleIndexUp(&rgDetArray.rrArrayHandle,
													rgDetArray.crArrayName,
													&rgDetArray.rrIdx02Handle,
													rgDetArray.crIdx02Name,
													"UDSR,UAFT");
				if (ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"Index <%s> not created RC=%d",
						rgDetArray.crIdx02Name,ilRc);
				}
			}

		}
	}

	/* Array for restricted flight jobs */
	if(ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"InitPolhdl: SetArrayInfo RFJJOB array");
		sprintf(pclSelection,"WHERE ACFR < '%s' AND ACTO > '%s' AND HOPO = '%s' AND UJTY = '%s'",
				cgUTCEnd,cgUTCStart,cgHopo,cgUjtyRfj);

		dbg ( TRACE, "InitPolhdl: Selection for Restricted Flight Jobs:" );
		dbg ( TRACE, "InitPolhdl: %s", pclSelection );

		ilRc = SetArrayInfo("RFJJOB","JOBTAB",cgJobFields,NULL,NULL,&rgRfjArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
			dbg(TRACE,"InitPolhdl: SetArrayInfo RFJJOB failed <%d>",ilRc);
		else
		{
			CEDAArraySendChanges2BCHDL(&rgRfjArray.rrArrayHandle,rgRfjArray.crArrayName,TRUE);
            CEDAArraySendChanges2ACTION(&rgRfjArray.rrArrayHandle,rgRfjArray.crArrayName,TRUE);
			dbg(DEBUG,"InitPolhdl: SetArrayInfo RFJJOB OK");
			dbg(DEBUG,"<%s> %d",rgRfjArray.crArrayName,	rgRfjArray.rrArrayHandle);

			if ( strlen ( pclSqlBuf ) > 0 )
				sprintf(pclSelection, 
					    "((ACFR < %s) && (ACTO > %s) && (UJTY == %s) && (HOPO == %s))",
						cgUTCEnd,cgUTCStart,cgUjtyRfj,cgHopo);
			dbg ( TRACE, "InitPolhdl: Selection for filter Restricted Flight Jobs:" );
			dbg ( TRACE, "InitPolhdl: %s", pclSelection );

			ilRc = CEDAArraySetFilter(&rgRfjArray.rrArrayHandle,
									  rgRfjArray.crArrayName,pclSelection);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: SetFilter RFJJOB failed <%d>",ilRc);
			}
			else
			{

				strcpy(rgRfjArray.crIdx02Name,"RFJURNO");
				ilRc = CEDAArrayCreateSimpleIndexUp(&rgRfjArray.rrArrayHandle,
													rgRfjArray.crArrayName,
													&rgRfjArray.rrIdx02Handle,
													rgRfjArray.crIdx02Name, "URNO");
				if (ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"Index <%s> not created RC=%d",
						rgRfjArray.crIdx02Name,ilRc);
				}
			}

		}
	}

	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitPolhdl: SetArrayInfo BSDTAB array");
		ilRc = SetArrayInfo("BSDTAB","BSDTAB",BSD_FIELDS,
				NULL,NULL,&rgBsdArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo BSDTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo BSDTAB OK");
			dbg(DEBUG,"<%s> %d",rgBsdArray.crArrayName,
				rgBsdArray.rrArrayHandle);
			strcpy(rgBsdArray.crIdx01Name,"BSD_BSDC");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgBsdArray.rrArrayHandle,
					rgBsdArray.crArrayName,
					&rgBsdArray.rrIdx01Handle,
					rgBsdArray.crIdx01Name,"BSDC");
			if (ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Index <%s> not created RC=%d",
					rgBsdArray.crIdx01Name,ilRc);
			}
		}/* end of if */
		if (ilRc == RC_SUCCESS)
		{
			ilRc = ConfigureAction("BSDTAB", 0, 0);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: ConfigureAction failed <%d>",ilRc);
			}/* end of if */
		}

	}

	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitPolhdl: SetArrayInfo ODATAB array");
		ilRc = SetArrayInfo("ODATAB","ODATAB",ODA_FIELDS,
				NULL,NULL,&rgOdaArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo ODATAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo ODATAB OK");
			dbg(DEBUG,"<%s> %d",rgOdaArray.crArrayName,
				rgOdaArray.rrArrayHandle);
			strcpy(rgOdaArray.crIdx01Name,"ODA_SDAC");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgOdaArray.rrArrayHandle,
					rgOdaArray.crArrayName,
					&rgOdaArray.rrIdx01Handle,
					rgOdaArray.crIdx01Name,"SDAC");
			if (ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Index <%s> not created RC=%d",
					rgOdaArray.crIdx01Name,ilRc);
			}
		}/* end of if */
		if (ilRc == RC_SUCCESS)
		{
			ilRc = ConfigureAction("ODATAB", 0, 0);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: ConfigureAction failed <%d>",ilRc);
			}/* end of if */
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		strcpy ( cgDelFields, DEL_FIELDS );
		if ( GetFieldLength( &cgHopo[0], "DELTAB", "SDAC", pclAddFieldLens ) 
			 == RC_SUCCESS )
		{
			strcat ( cgDelFields, ",SDAC" );
		}
		else
		{
			dbg ( TRACE,"InitPolhdl: field DELTAB.SDAC not in DB configuration" );
		}
		dbg(TRACE,"InitPolhdl: SetArrayInfo DELTAB array");
		ilRc = SetArrayInfo("DELTAB","DELTAB",cgDelFields,
				NULL,NULL,&rgDelArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo DELTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo DELTAB OK");
			dbg(DEBUG,"<%s> %d",rgDelArray.crArrayName,
				rgDelArray.rrArrayHandle);
			strcpy(rgDelArray.crIdx01Name,"DEL_BSDU");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgDelArray.rrArrayHandle,
					rgDelArray.crArrayName,
					&rgDelArray.rrIdx01Handle,
					rgDelArray.crIdx01Name,"BSDU");
			if (ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Index <%s> not created RC=%d",
					rgDelArray.crIdx01Name,ilRc);
			}
			strcpy(rgDelArray.crIdx02Name,"DEL_URNO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgDelArray.rrArrayHandle,
												rgDelArray.crArrayName,
												&rgDelArray.rrIdx02Handle,
												rgDelArray.crIdx02Name,"URNO");
			if (ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Index <%s> not created RC=%d", rgDelArray.crIdx02Name,ilRc);
			}
			ilRc = ConfigureAction("DELTAB", 0, 0);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: ConfigureAction failed <%d>",ilRc);
			}/* end of if */
		}/* end of if */
	}
	/*
	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);
		dbg(TRACE,"InitPolhdl: SetArrayInfo PGPTAB array");
		ilRc = SetArrayInfo("PGPTAB","PGPTAB",PGP_FIELDS,
				NULL,NULL,&rgPgpArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo PGPTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo PGPTAB OK");
			dbg(DEBUG,"<%s> %d",rgPgpArray.crArrayName,
				rgPgpArray.rrArrayHandle);
			ilRc = ConfigureAction("PGPTAB", PGP_FIELDS, 0);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: ConfigureAction failed <%d>",ilRc);
			}
		}
	}*/
	if(ilRc == RC_SUCCESS)
	{
		/*
		sprintf(pclSelection,"WHERE AVFR < '%s' AND AVTO > '%s' AND HOPO = '%s'"
			" AND ROSL = '3' AND ROSS != 'L'",
			cgOpEnd,cgOpStart,cgHopo); */
        sprintf(pclSelection,"WHERE (SDAY BETWEEN '%s' AND '%s') AND HOPO = '%s' AND ROSL = '3' AND ROSS != 'L'",
                       cgOpFirstDay, cgOpLastDay,cgHopo);
 
		dbg(TRACE,"InitPolhdl: SetArrayInfo DRRTAB array \n<%s>",pclSelection);
		strcpy ( cgDrrFields, DRR_FIELDS );
		if (bgUseFilt)
		{
			strcat(cgDrrFields,",FILT");
		}
		if ( GetFieldLength( &cgHopo[0], "DRRTAB", "BROS", pclAddFieldLens ) 
			 == RC_SUCCESS )
		{
			strcat ( cgDrrFields, ",BROS" );
		}
		else
		{
			dbg ( TRACE,"InitPolhdl: field DRRTAB.BROS not in DB configuration" );
		}

       	if(  GetRowLength(&cgHopo[0],"DRRTAB", cgDrrFields, &(rgDrrArray.lrIdx01RowLen) )
			 != RC_SUCCESS )
			 rgDrrArray.lrIdx01RowLen = 0;
		ilRc = SetArrayInfo("DRRTAB","DRRTAB",cgDrrFields, NULL,NULL,
							&rgDrrArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo DRRTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo DRRTAB OK");
			dbg(DEBUG,"<%s> %d",rgDrrArray.crArrayName, rgDrrArray.rrArrayHandle);
			
			strcpy(rgDrrArray.crIdx01Name,"DRR_DRRN");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgDrrArray.rrArrayHandle,
												rgDrrArray.crArrayName,
												&rgDrrArray.rrIdx01Handle,
												rgDrrArray.crIdx01Name,"DRRN,SDAY,STFU");
			strcpy(rgDrrArray.crIdx01FieldList,"DRRN,SDAY,STFU");

			strcpy(rgDrrArray.crIdx02Name,"DRR_URNO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgDrrArray.rrArrayHandle,
												rgDrrArray.crArrayName,
												&rgDrrArray.rrIdx02Handle,
												rgDrrArray.crIdx02Name,"URNO");

			sprintf(pclSelection, "((AVFR < %s) && (AVTO > %s) && (HOPO == %s) "
								  " && (ROSL == 3) && (ROSS != L))", cgOpEnd,cgOpStart,cgHopo);
/***********************
			sprintf(pclSelection,
					"((AVFR < %s) && (AVTO > %s) && (HOPO == %s) "
			" && (ROSL == 3) && (ROSS != L))",
			cgOpEnd,cgOpStart,cgHopo);
***************************/
				ilRc = CEDAArraySetFilter(&rgDrrArray.rrArrayHandle,
										  rgDrrArray.crArrayName,pclSelection);
		}/* end of if */
	}
	if(ilRc == RC_SUCCESS)
	{

		sprintf(pclSelection,"WHERE SDAY  <=  '%s' AND SDAY >= '%s' AND HOPO= '%s'",
			cgOpLastDay,cgOpFirstDay,cgHopo);
		dbg(TRACE,"InitPolhdl: SetArrayInfo DRDTAB array <%s>",pclSelection);
		ilRc = SetArrayInfo("DRDTAB","DRDTAB",DRD_FIELDS,
				NULL,NULL,&rgDrdArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo DRDTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo DRDTAB OK");
			dbg(DEBUG,"<%s> %d",rgDrdArray.crArrayName,
				rgDrdArray.rrArrayHandle);
			strcpy(rgDrdArray.crIdx01Name,"DRD_IDX");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgDrdArray.rrArrayHandle,
												rgDrdArray.crArrayName,
												&rgDrdArray.rrIdx01Handle,
												rgDrdArray.crIdx01Name,"DRRN,SDAY,STFU");
			strcpy(rgDrdArray.crIdx01FieldList,"DRRN,SDAY,STFU");
			strcpy(rgDrdArray.crIdx02Name,"DRD_URNO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgDrdArray.rrArrayHandle,
												rgDrdArray.crArrayName,
												&rgDrdArray.rrIdx02Handle,
												rgDrdArray.crIdx02Name,"URNO");

			sprintf(pclSelection, "((SDAY <= %s) && (SDAY >= %s) && (HOPO == %s))",
					cgOpLastDay,cgOpFirstDay,cgHopo);
			ilRc = CEDAArraySetFilter(&rgDrdArray.rrArrayHandle,
									  rgDrdArray.crArrayName,pclSelection);

			/*  Version which handles RELDRR gets Updates via operative version */
			if ( ilRc == RC_SUCCESS )
			{
				if ( igVersion & V_OPERATIVE )
					ilRc = ConfigureAction("DRDTAB", 0, 0 );
				else
					ilRc = ChangeDynActionConfig ( "DRDTAB", 0, 0, FALSE, TRUE );
				if(ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"InitPolhdl: ChangeDynActionConfig <DRDTAB> failed <%d>",ilRc);
				}/* end of if */
			}
		}/* end of if */
	}
	if(ilRc == RC_SUCCESS)
	{
		/* sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo); */
		sprintf(pclSelection,"WHERE SDAY  <=  '%s' AND SDAY >= '%s' AND HOPO= '%s'",
			    cgOpLastDay,cgOpFirstDay,cgHopo);

		dbg(TRACE,"InitPolhdl: SetArrayInfo DRATAB array");
		rgDraArray.lrIdx01RowLen = 124;
		ilRc = SetArrayInfo("DRATAB","DRATAB",DRA_FIELDS,
				NULL,NULL,&rgDraArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo DRATAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo DRATAB OK");
			dbg(DEBUG,"<%s> %d",rgDraArray.crArrayName,
			rgDraArray.rrArrayHandle);
			strcpy(rgDraArray.crIdx01Name,"DRA_IDX");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgDraArray.rrArrayHandle,
												rgDraArray.crArrayName,
												&rgDraArray.rrIdx01Handle,
												rgDraArray.crIdx01Name,"DRRN,SDAY,STFU");
			strcpy(rgDraArray.crIdx02Name,"URNO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgDraArray.rrArrayHandle,
												rgDraArray.crArrayName,
												&rgDraArray.rrIdx02Handle,
												rgDraArray.crIdx02Name,"URNO");

			sprintf(pclSelection, "((SDAY <= %s) && (SDAY >= %s) && (HOPO == %s))",
					cgOpLastDay,cgOpFirstDay,cgHopo);
			ilRc = CEDAArraySetFilter(&rgDraArray.rrArrayHandle,
									  rgDraArray.crArrayName,pclSelection);

			if (ilRc == RC_SUCCESS)
			{
				if ( igVersion & V_OPERATIVE )
					ilRc = ConfigureAction("DRATAB", 0, 0 );
				else
					ilRc = ChangeDynActionConfig ( "DRATAB", 0, 0, FALSE, TRUE );
				if(ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"InitPolhdl: ChangeDynActionConfig <DRATAB> failed <%d>",ilRc);
				}/* end of if */
			}
		}/* end of if */
	}
	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);
		dbg(TRACE,"InitPolhdl: SetArrayInfo DRSTAB array");
		ilRc = SetArrayInfo("DRSTAB","DRSTAB",DRS_FIELDS,
							NULL,NULL,&rgDrsArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo DRSTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo DRSTAB OK");
			dbg(DEBUG,"<%s> %d",rgDrsArray.crArrayName, rgDrsArray.rrArrayHandle);
			strcpy(rgDrsArray.crIdx01Name,"DRS_DRRU");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgDrsArray.rrArrayHandle,
												rgDrsArray.crArrayName,
												&rgDrsArray.rrIdx01Handle,
												rgDrsArray.crIdx01Name,"DRRU");
			if (ilRc == RC_SUCCESS)
			{
				if ( igVersion & V_OPERATIVE )
					ilRc = ConfigureAction("DRSTAB", 0, 0 );
				else
					ilRc = ChangeDynActionConfig ( "DRSTAB", 0, 0, FALSE, TRUE );
				if(ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"InitPolhdl: ChangeDynActionConfig <DRSTAB> failed <%d>",ilRc);
				}/* end of if */
			}
		}/* end of if */
	}
	if(ilRc == RC_SUCCESS)
	{
		strcpy(pclAddFields,"XXXX,SCOD");
		pclAddFieldLens[0] = 2;
		pclAddFieldLens[1] = 8;
		pclAddFieldLens[2] = 0;

		sprintf(pclSelection,"WHERE ACFR < '%s' AND ACTO > '%s' AND (UJTY = '%s' OR UJTY = '%s') AND HOPO = '%s'",
			cgUTCEnd,cgUTCStart,cgUjty,cgUjtyAbs,cgHopo);
		dbg(TRACE,"InitPolhdl: SetArrayInfo JOBTAB array \n<%s>",pclSelection);

       	if(  GetRowLength(&cgHopo[0],"JOBTAB", "UDSR,PLFR", &(rgJobArray.lrIdx01RowLen) )
			 != RC_SUCCESS )
			 rgJobArray.lrIdx01RowLen = 0;

		ilRc = SetArrayInfo("JOBTAB","JOBTAB",cgJobFields,
				pclAddFields,pclAddFieldLens,&rgJobArray,pclSelection,FALSE);
		if(ilRc == RC_SUCCESS)
		{
			/* ilRc = CEDAArrayFill(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,"0, ");*/
			ilRc = CEDAArrayFillHint(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,
									 "0, ", cgJobReloadHint  );

		}
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo JOBTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo JOBTAB OK");
			dbg(DEBUG,"<%s> %d",rgJobArray.crArrayName, rgJobArray.rrArrayHandle);
			strcpy(rgJobArray.crIdx01Name,"JOB_UDSR");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgJobArray.rrArrayHandle,
												rgJobArray.crArrayName,
												&rgJobArray.rrIdx01Handle,
												rgJobArray.crIdx01Name,"UDSR,PLFR");
			/*  if bgRELJOBtoACTION==TRUE switch off CEDAArraySendChanges2ACTION during initialization */
			CEDAArraySendChanges2ACTION(&rgJobArray.rrArrayHandle,
										rgJobArray.crArrayName,!bgRELJOBtoACTION);
			/*  if bgRELJOBtoBCHDL==TRUE switch off CEDAArraySendChanges2BCHDL during initialization */
			CEDAArraySendChanges2BCHDL(&rgJobArray.rrArrayHandle,
										rgJobArray.crArrayName,!bgRELJOBtoBCHDL );
			/************/
			strcpy(rgJobArray.crIdx02Name,"JOB_URNO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgJobArray.rrArrayHandle,
												rgJobArray.crArrayName,
												&rgJobArray.rrIdx02Handle,
												rgJobArray.crIdx02Name,"URNO");
						/************/
			/************/
			strcpy(rgJobArray.crIdx03Name,"JOB_UJTYUSTFJOUR");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgJobArray.rrArrayHandle,
												rgJobArray.crArrayName,
												&rgJobArray.rrIdx03Handle,
												rgJobArray.crIdx03Name,"UJTY,USTF,JOUR");
			strcpy(rgJobArray.crIdx04Name,"JOB_UJTYPLFR");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgJobArray.rrArrayHandle,
												rgJobArray.crArrayName,
												&rgJobArray.rrIdx04Handle,
												rgJobArray.crIdx04Name,"UJTY,PLFR");
						/************/
			if (ilRc == RC_SUCCESS)
			{
				ilRc = ConfigureAction("JOBTAB", 0, 0);
				if(ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"InitPolhdl: ConfigureAction failed <%d>",ilRc);
				}/* end of if */
				else
				{
					sprintf(pclSelection,
							"((ACFR < %s) && (ACTO > %s) && ((UJTY == %s) || (UJTY == %s)) && (HOPO == %s))"
							,cgUTCEnd,cgUTCStart,cgUjty,cgUjtyAbs,cgHopo);
					ilRc = CEDAArraySetFilter(&rgJobArray.rrArrayHandle,
						rgJobArray.crArrayName,pclSelection);
				}
			}
		}/* end of if */
	}

	if(ilRc == RC_SUCCESS)
	{
		strcpy(pclAddFields,"XXXX,SCOD");
		pclAddFieldLens[0] = 2;
		pclAddFieldLens[1] = 8;
		pclAddFieldLens[2] = 0;

		if(ilRc != RC_SUCCESS)
		sprintf(pclSelection,"WHERE ACFR < '%s' AND ACTO > '%s' AND (UJTY = '%s' OR UJTY = '%s') AND HOPO = '%s'",
			cgUTCEnd,cgUTCStart,cgUjty,cgUjtyAbs,cgHopo);
		dbg(TRACE,"InitPolhdl: SetArrayInfo NEWJOBS array \n<%s>",pclSelection);
		ilRc = SetArrayInfo("NEWJOBS","JOBTAB",cgJobFields,
				pclAddFields,pclAddFieldLens,&rgNewJobsArray,pclSelection,FALSE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo NEWJOBS failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo NEWJOBS OK");
			dbg(DEBUG,"<%s> %d",rgNewJobsArray.crArrayName,
				rgNewJobsArray.rrArrayHandle);
			strcpy(rgNewJobsArray.crIdx01Name,"JOB_UDSR");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgNewJobsArray.rrArrayHandle,
						rgNewJobsArray.crArrayName,
						&rgNewJobsArray.rrIdx01Handle,
						rgNewJobsArray.crIdx01Name,"UDSR");
			/*  CEDAArraySendChanges2ACTION(&rgNewJobsArray.rrArrayHandle,
										rgNewJobsArray.crArrayName,TRUE);
			CEDAArraySendChanges2BCHDL(&rgNewJobsArray.rrArrayHandle,
									   rgNewJobsArray.crArrayName,TRUE);*/
			/************/
			strcpy(rgNewJobsArray.crIdx02Name,"REASON");
			ilRc |= CEDAArrayCreateSimpleIndexUp(&rgNewJobsArray.rrArrayHandle,
											    rgNewJobsArray.crArrayName,
												&rgNewJobsArray.rrIdx02Handle,
												rgNewJobsArray.crIdx02Name,
												"UDSR,UDEL,UDRD,PLFR");
			if ( ilRc != RC_SUCCESS )
				dbg(TRACE,"InitPolhdl: CEDAArrayCreateSimpleIndexUp NEWJOBS failed <%d>",ilRc);
						/************/
		}/* end of if */
	}

	strcat ( cgJobFields, "," );
	strcat ( cgJobFields, pclAddFields );
	
	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);
		dbg(TRACE,"InitPolhdl: SetArrayInfo POATAB array");
		ilRc = SetArrayInfo("POATAB","POATAB",POA_FIELDS,
				NULL,NULL,&rgPoaArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo POATAB failed <%d>",ilRc);
		}
		else
		{
						strcpy(rgPoaArray.crIdx01Name,"POA_UVAL");
					ilRc = CEDAArrayCreateSimpleIndexUp(&rgPoaArray.rrArrayHandle,
						rgPoaArray.crArrayName,
						&rgPoaArray.rrIdx01Handle,
						rgPoaArray.crIdx01Name,"UVAL");
		if (ilRc == RC_SUCCESS)
		{
				ilRc = ConfigureAction("POATAB", 0, 0);
				if(ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"InitPolhdl: ConfigureAction failed <%d>",ilRc);
				}/* end of if */
		}
			}
			if (ilRc == RC_SUCCESS)
			{
			int ilRc, ilCol, ilPos;
			long llRowNum = ARR_FIRST;
			long	*pllFldPos    = NULL;
			char *pclDot;
			char clPoaREFT[24];
			char clBsdKeyField[24];
			
			dbg(DEBUG,"InitPolhdl: SetArrayInfo POATAB OK");
			dbg(DEBUG,"<%s> %d",rgPoaArray.crArrayName,
				rgPoaArray.rrArrayHandle);
		
			strcpy(clBsdKeyField,"DPT1");
			ilRc = CEDAArrayGetField(&rgPoaArray.rrArrayHandle,
				rgPoaArray.crArrayName,pllFldPos,"REFT",20,llRowNum,clPoaREFT);
			dbg(TRACE,"PoaREFT RC= %d <%s>",ilRc,clPoaREFT);
			if (ilRc == RC_SUCCESS)
			{
				strcpy(cgPoaREFT,clPoaREFT);
				if ((pclDot = strchr(clPoaREFT,'.')) != NULL)
				{
					pclDot++;
					strcpy(cgPoaREFT_Field,pclDot);
					pclDot--;
					*pclDot = '\0';
					strcpy(cgPoaREFT_Table,clPoaREFT);
					dbg(TRACE,"PoaREFT_Table <%s>, PoaREFT_Field <%s>",
								cgPoaREFT_Table,cgPoaREFT_Field);
					if(!strcmp(cgPoaREFT_Table,"ORG"))
					{
						prgStfAlidArray = &rgSorArray;
						prgAlidArray = &rgOrgArray;
						pigDrdKey = &igDrdDPT1;
						pigDelKey = &igDelDPT1;
						pigBsdKey = &igBsdDPT1;
						strcpy(clBsdKeyField,"DPT1");
						/* PRF6246: For criterion "Org. Unit the link in PFCTAB shall be used??? */ 
						if ( ( igDrrFCTC >= 0 ) && ( igPfcDPTC >=0 ) &&
 							 ( ReadConfigEntry("MAIN","UseFunctionsOrgUnit",
							                   pcgCfgBuffer) == RC_SUCCESS ) &&
							 (pcgCfgBuffer[0]=='Y')  && 
							 (ReadOrgUnitsForPfcUse() == RC_SUCCESS ) )
						{
							bgUseFunctionsOrgUnit = TRUE;
							pigDrrKey = &igDrrFCTC;
							strcpy(rgPfcArray.crIdx02Name,"PFC_FCTC");
							ilRc = CEDAArrayCreateSimpleIndexUp(&rgPfcArray.rrArrayHandle,
																rgPfcArray.crArrayName,
																&rgPfcArray.rrIdx02Handle,
																rgPfcArray.crIdx02Name,"FCTC" );
							if (ilRc != RC_SUCCESS)
							{
								dbg(TRACE,"Index <%s> not created RC=%d",
									rgPfcArray.crIdx02Name,ilRc);
							}
						}

					}
					else if(!strcmp(cgPoaREFT_Table,"PFC"))
					{
						prgStfAlidArray = &rgSpfArray;
						prgAlidArray = &rgPfcArray;
						pigDrdKey = &igDrdFCTC;
						pigDelKey = &igDelFCTC;
						pigBsdKey = &igBsdFCTC;
						if ( igDrrFCTC >= 0 )
							pigDrrKey = &igDrrFCTC;
						FindItemInArrayList(SPF_FIELDS,"PRIO",',',&igPrioIdx,&ilCol,&ilPos);
						strcpy(clBsdKeyField,"FCTC");
					}
					else if(!strcmp(cgPoaREFT_Table,"PER"))
					{
						prgStfAlidArray = &rgSpeArray;
						prgAlidArray = &rgPerArray;
						pigDrdKey = &igDrdPRMC;
						FindItemInArrayList(SPE_FIELDS,"PRIO",',',&igPrioIdx,&ilCol,&ilPos);
					}
					else if(!strcmp(cgPoaREFT_Table,"COT"))
					{
						prgStfAlidArray = &rgScoArray;
						prgAlidArray = &rgCotArray;
					}
			/*************************
					else if(!strcmp(cgPoaREFT_Table,"WGP"))
					{
						
						prgStfAlidArray = &rgSwgArray;
						prgAlidArray = &rgWgpArray;
						pigDrdKey = &igDrdWGPC;
					}
			*******************************/
					else if(!strcmp(cgPoaREFT_Table,"POL"))
					{
						prgStfAlidArray = &rgSdaArray;
						prgAlidArray = &rgPolArray;
					}
					if (prgAlidArray == NULL)
					{
						dbg(TRACE,"REFT in POATAB not found, using default SDATAB");
					}
					strcpy(prgStfAlidArray->crIdx01Name,"ALID_SURN");
					ilRc = CEDAArrayCreateSimpleIndexUp(&prgStfAlidArray->rrArrayHandle,
						prgStfAlidArray->crArrayName,
						&prgStfAlidArray->rrIdx01Handle,
						prgStfAlidArray->crIdx01Name,"SURN");
			}
			if ( rgSpfArray.rrIdx01Handle < 0 )
			{
				strcpy(rgSpfArray.crIdx01Name,"ALID_SURN");
				ilRc = CEDAArrayCreateSimpleIndexUp(&(rgSpfArray.rrArrayHandle),
													rgSpfArray.crArrayName,
													&(rgSpfArray.rrIdx01Handle),
													rgSpfArray.crIdx01Name,"SURN");
			}
			if (ilRc != RC_SUCCESS)
			{
					strcpy(rgBsdArray.crIdx02Name,"BSD_KEY");
					ilRc = CEDAArrayCreateSimpleIndexUp(&rgBsdArray.rrArrayHandle,
							rgBsdArray.crArrayName,
							&rgBsdArray.rrIdx02Handle,
							rgBsdArray.crIdx02Name,clBsdKeyField);
					if (ilRc != RC_SUCCESS)
					{
						dbg(TRACE,"Index <%s> not created RC=%d",
							rgBsdArray.crIdx02Name,ilRc);
					}
			}
			if (ilRc == RC_SUCCESS)
			{
			int ilRc;
			
			dbg(DEBUG,"InitPolhdl: SetArrayInfo POATAB OK");
			dbg(DEBUG,"<%s> %d",rgPoaArray.crArrayName,
				rgPoaArray.rrArrayHandle);
		
			sprintf(prgAlidArray->crIdx01Name,"%s_%s",
					cgPoaREFT_Table,cgPoaREFT_Field);
					ilRc = CEDAArrayCreateSimpleIndexUp(&prgAlidArray->rrArrayHandle,
						prgAlidArray->crArrayName,
						&prgAlidArray->rrIdx01Handle,
						prgAlidArray->crIdx01Name,cgPoaREFT_Field);
				dbg(DEBUG,
			"POA.REFT=<%s> Field <%s> Table <%s> ilRc=%d\nArray %d %s,Index %d <%s>",
						cgPoaREFT,cgPoaREFT_Field,cgPoaREFT_Table,ilRc,
						prgAlidArray->rrArrayHandle,prgAlidArray->crArrayName,
						prgAlidArray->rrIdx01Handle,prgAlidArray->crIdx01Name);
				}
		}/* end of if */
		} /* cgPoaREFT set */
	}
/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
	/*  RELDRR must be handled also from operative polhdl */
	if ( (ilRc == RC_SUCCESS)/* && ( igVersion & V_RELEASES )*/ )
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);
		ilRc = SetArrayInfo("NEWDRR","DRRTAB",cgDrrFields,
							NULL,NULL,&rgNewDrrArray,pclSelection,FALSE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo NEWDRR failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo NEWDRR OK");
		}/* end of if */
	}
	
/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/

	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayReadAllHeader(outp);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: CEDAArrayReadAllHeader failed <%d>",ilRc);
		}else{
			dbg(TRACE,"InitPolhdl: CEDAArrayReadAllHeader OK");
		}/* end of if */
	}/* end of if */


	if(ilRc == RC_SUCCESS)
	{
		ilRc = InitFieldIndex();
	}
	if(ilRc == RC_SUCCESS)
	{
		igInitOK = TRUE;
		dbg(TRACE,"InitPolhdl: InitFieldIndex OK");
	}/* end of if */
	else
	{
		dbg(TRACE,"InitPolhdl: InitFieldIndex failed");
	}

	/*
	{
		long llRowNum = ARR_FIRST;
		char *pclRow;

		while(CEDAArrayGetRowPointer(&rgDrdArray.rrArrayHandle,
									 rgDrdArray.crArrayName,llRowNum,(void *)&pclRow) 
									 == RC_SUCCESS)
		{
			llRowNum = ARR_NEXT;
			dbg(TRACE,"DRRN=<%s>,SDAY=<%s>,STFU=<%s>",
				FIELDVAL(rgDrdArray,pclRow,igDrdDRRN),
				FIELDVAL(rgDrdArray,pclRow,igDrdSDAY),
				FIELDVAL(rgDrdArray,pclRow,igDrdSTFU));
		}
	}*/
	if ( igVersion & V_OPERATIVE )
		ilRc = ConfigureAction("DRRTAB", 0, 0 );
	else
		ilRc = ChangeDynActionConfig ( "DRRTAB", 0, 0, FALSE, TRUE );
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"InitPolhdl: ChangeDynActionConfig <DRRTAB> failed <%d>",ilRc);
	}/* end of if */

	/* Initialise AAT_PARAM structure for old and new demands */
	ilRc = AATArrayCreateArgDesc(rgJobArray.crArrayName, &prgJobInf );
	if ( ilRc == RC_SUCCESS )
	{
		prgJobInf->AppendData = TRUE;
		ToolsListSetInfo(&(prgJobInf->DataList[IDX_IRTU]), "IRT_URNO", "JOBTAB", "IRT", "URNO", "","",",",-1);
		ToolsListSetInfo(&(prgJobInf->DataList[IDX_URTU]), "URT_URNO", "JOBTAB", "URT", "URNO", "","",",",-1);
		ToolsListSetInfo(&(prgJobInf->DataList[10]),"CMD_LIST","","","","","","\n",-1);
		ToolsListSetInfo(&(prgJobInf->DataList[11]),"SEL_LIST","","","","","","\n",-1);
		ToolsListSetInfo(&(prgJobInf->DataList[12]),"FLD_LIST","","","","","","\n",-1);
		ToolsListSetInfo(&(prgJobInf->DataList[13]),"DAT_LIST","","","","","","\n",-1);
	}

	if ( igVersion & V_RELEASES )
		ReCalculateShifts();

	PreparePoolJob("");

	/*  switch on CEDAArraySendChanges2ACTION for single DRR-changes */
	CEDAArraySendChanges2ACTION(&rgJobArray.rrArrayHandle,
								rgJobArray.crArrayName,TRUE);
	CEDAArraySendChanges2ACTION(&rgBrkArray.rrArrayHandle,
								rgBrkArray.crArrayName,TRUE);
	CEDAArraySendChanges2BCHDL(&rgJobArray.rrArrayHandle,
								rgJobArray.crArrayName,TRUE);
	CEDAArraySendChanges2BCHDL(&rgBrkArray.rrArrayHandle,
								rgBrkArray.crArrayName,TRUE);

	if ( igVersion & V_OPERATIVE )
		ilRc = ConfigureAction("SPRTAB", "PKNO,USTF,ACTI,FCOL", 0);
	else
		ilRc = ChangeDynActionConfig ( "SPRTAB", "PKNO,USTF,ACTI,FCOL", 0, FALSE, TRUE );
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"InitPolhdl: ChangeDynActionConfig <SPRTAB> failed <%d>",ilRc);
	}

	ilRc = ConfigureAction("RELDRR", 0, "SBC" );
	/*  RELDRR must be handled also from operative polhdl 
	if ( igVersion & V_RELEASES )
		ilRc = ConfigureAction("RELDRR", 0, "SBC" );
	else
		ilRc = ChangeDynActionConfig ( "RELDRR", 0, "SBC", FALSE, TRUE );*/
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"InitPolhdl: ConfigureAction <RELDRR> failed <%d>",ilRc);
	}
	else
		dbg(DEBUG,"InitPolhdl: ConfigureAction <RELDRR> OK" );

	/* if there is a special instance for RELDRR the 1st instance must handle RELJOB */
	if ( igVersion == V_OPERATIVE )
		ilRc = ConfigureAction("RELJOB", 0, "SBC" );
	else
		ilRc = ChangeDynActionConfig ( "RELJOB", 0, "SBC", FALSE, TRUE );
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"InitPolhdl: ChangeDynActionConfig <RELDRR> failed <%d>",ilRc);
	}
	else
		dbg(DEBUG,"InitPolhdl: ChangeDynActionConfig <RELDRR> OK" );

	return(ilRc);
	
} /* end of initialize */




/******************************************************************************/
/*                                                                            */
/******************************************************************************/
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, char *pcpArrayFieldList,char *pcpAddFields,long *pcpAddFieldLens,ARRAYINFO *prpArrayInfo,
	char *pcpSelection,BOOL bpFill)
{
	int	ilRc = RC_SUCCESS;				/* Return code */

	if(prpArrayInfo == NULL)
	{
		dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
		ilRc = RC_FAIL;
	}else
	{
		memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

		prpArrayInfo->rrArrayHandle = -1;
		prpArrayInfo->rrIdx01Handle = -1;

		if(pcpArrayName != NULL)
		{
			if(strlen(pcpArrayName) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crArrayName,pcpArrayName);
			}/* end of if */
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);
		if (pcpAddFields != NULL)
		{
			prpArrayInfo->lrArrayFieldCnt += get_no_of_items(pcpAddFields);
		}

		prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldOfs == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldLen == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			*(prpArrayInfo->plrArrayFieldLen) = -1;
		}/* end of if */
	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
       	ilRc = GetRowLength(&cgHopo[0],pcpTableName,pcpArrayFieldList,
							&(prpArrayInfo->lrArrayRowLen));
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
      	}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->lrArrayRowLen+=100;
		prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+10));
   		if(prpArrayInfo->pcrArrayRowBuf == NULL)
   		{
      			ilRc = RC_FAIL;
      			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx01RowLen+1));
   		if(prpArrayInfo->pcrIdx01RowBuf == NULL)
   		{
      			ilRc = RC_FAIL;
      			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"CEDAArrayCreate follows Array <%s> Table <%s>",pcpArrayName,pcpTableName);
		ilRc = CEDAArrayCreate(&(prpArrayInfo->rrArrayHandle),
				pcpArrayName,pcpTableName,pcpSelection,pcpAddFields,
				pcpAddFieldLens,pcpArrayFieldList,
				&(prpArrayInfo->plrArrayFieldLen[0]),
				&(prpArrayInfo->plrArrayFieldOfs[0]));

		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */

		if(pcpArrayFieldList != NULL)
		{
			if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
			{
				strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
			}/* end of if */
		}/* end of if */
		if(pcpAddFields != NULL)
		{
			if(strlen(pcpAddFields) + 
				strlen(prpArrayInfo->crArrayFieldList) <= (size_t)ARR_FLDLST_LEN)
			{
				strcat(prpArrayInfo->crArrayFieldList,",");
				strcat(prpArrayInfo->crArrayFieldList,pcpAddFields);
				dbg(TRACE,"<%s>",prpArrayInfo->crArrayFieldList);
			}/* end of if */
		}/* end of if */


	if(ilRc == RC_SUCCESS && bpFill)
	{
		ilRc = CEDAArrayFill(&(prpArrayInfo->rrArrayHandle),pcpArrayName,NULL);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayFill failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */

	return(ilRc);
}



/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");
	ReadConfigEntries();
	if ( bgUseFunctionsOrgUnit )
		ReadOrgUnitsForPfcUse() ;
	IniFwdEventArray ();
	return ilRc;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	/* unset SIGCHLD ! DB-Child will terminate ! */

	dbg(TRACE,"Terminate: now DB logoff ...");

	signal(SIGCHLD,SIG_IGN);

	logoff();

	dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


	dbg(TRACE,"Terminate: now leaving ...");

	fclose(outp);

	sleep(ipSleep < 1 ? 1 : ipSleep);
	
	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
#if 0
static void HandleSignal(int pipSig)
{
	dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

	switch(pipSig)
	{
	default	:
		Terminate(1);
		break;
	} /* end of switch */

	exit(1);
	
} /* end of HandleSignal */
#endif
/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	

		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	

			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;	

			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;

			case	SHUTDOWN	:
				Terminate(1);
				break;
						
			case	RESET		:
				ilRc = Reset();
				break;
						
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;

			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;

			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	} while (ilBreakOut == FALSE);

	if(igInitOK == FALSE)
	{
			ilRc = InitPolhdl();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitPolhdl: init failed!");
			} /* end of if */
	}/* end of if */

} /* end of HandleQueues */
	

/******************************************************************************/
/******************************************************************************/
static int GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList,
						long *plpLen)
{
	int	 ilRc = RC_SUCCESS;			/* Return code */
	int  ilNoOfItems = 0;
	int  ilLoop = 0;
	long llFldLen = 0;
	long llRowLen = 0;
	char clFina[8];
	
	ilNoOfItems = get_no_of_items(pcpFieldList);

	ilLoop = 1;
	do
	{
		ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
		if(ilRc > 0)
		{
			ilRc = GetFieldLength(pcpHopo,pcpTana,&clFina[0],&llFldLen);
			if(ilRc == RC_SUCCESS)
			{
				llRowLen++;
				llRowLen += llFldLen;
			}/* end of if */
		}/* end of if */
		ilLoop++;
	}while((ilRc == RC_SUCCESS) && (ilLoop <= ilNoOfItems));

	if(ilRc == RC_SUCCESS)
	{
		*plpLen = llRowLen;
	}/* end of if */

	return(ilRc);
	
} /* end of GetRowLength */


/******************************************************************************/
/******************************************************************************/
static int GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina,
							long *plpLen)
{
	int  ilRc = RC_SUCCESS;			/* Return code */
	int  ilCnt = 0;
	char clTaFi[32];
	char clFele[16];
	char clFldLst[16];

	ilCnt = 1;
	sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
	sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerst�rt */

	/*dbg(TRACE,"GetFieldLength Tana <%s> Fina <%s>",pcpTana,pcpFina);*/

	ilRc = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
	switch (ilRc)
	{
	case RC_SUCCESS :
		*plpLen = atoi(&clFele[0]);
		/* dbg(DEBUG,"GetFieldLength: <%s,%s,%s> <%d>",pcpHopo,pcpTana,pcpFina,*plpLen);*/
		break;

	case RC_NOT_FOUND :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s>",ilRc,&clTaFi[0]);
		break;

	case RC_FAIL :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
		break;

	default :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
		break;
	}/* end of switch */

	return(ilRc);
	
} /* end of GetFieldLength */


/******************************************************************************/
/******************************************************************************/
static int GetCommand(char *pcpCommand, int *pipCmd)
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int ilLoop = 0;
	char clCommand[48];
	
	memset(&clCommand[0],0x00,8);

	ilRc = get_real_item(&clCommand[0],pcpCommand,1);
	if(ilRc > 0)
	{
		ilRc = RC_FAIL;
	}/* end of if */

	while((ilRc != 0) && (prgCmdTxt[ilLoop] != NULL))
	{
		ilRc = strcmp(&clCommand[0],prgCmdTxt[ilLoop]);
		ilLoop++;
	}/* end of while */

	if(ilRc == 0)
	{
		ilLoop--;
		dbg(TRACE,"GetCommand: <%s> <%d>",prgCmdTxt[ilLoop],rgCmdDef[ilLoop]);
		*pipCmd = rgCmdDef[ilLoop];
	}else{
		dbg(TRACE,"GetCommand: <%s> is not valid",&clCommand[0]);
		ilRc = RC_FAIL;
	}/* end of if */

	return(ilRc);
	
} /* end of GetCommand */

static int GetDrrUrnoFromDra(char *pcpFields,char *pcpData,char *pcpUrno)
{
	int	ilRc = RC_SUCCESS;
	long llFunc = ARR_FIRST;
	char      clDel          = ',';
	int 		ilItemNo;
	int    	ilCol;
	int 		ilPos;
	char 	 	clDrrn[24];
	char 	 	clSday[24];
	char 	 	clStfu[24];
	char 	 	clKey[124];
	char  	*pclRow = NULL;


	dbg(TRACE,"GetDrrUrnoFromDra: <%s> <%s>",pcpFields,pcpData);

	ilRc = FindItemInList(pcpFields,"DRRN",clDel,&ilItemNo,&ilCol,&ilPos);
	if (ilRc == RC_SUCCESS)
	{
		GetDataItem(clDrrn,pcpData,ilItemNo,clDel,"","\0\0");
		dbg(TRACE,"DRRN found: <%s>",clDrrn);
	}
	if (ilRc == RC_SUCCESS)
	{
		ilRc = FindItemInList(pcpFields,"SDAY",clDel,&ilItemNo,&ilCol,&ilPos);
		if (ilRc == RC_SUCCESS)
		{
			GetDataItem(clSday,pcpData,ilItemNo,clDel,"","\0\0");
			dbg(TRACE,"SDAY found: <%s>",clSday);
		}
	}
	if (ilRc == RC_SUCCESS)
	{
		ilRc = FindItemInList(pcpFields,"STFU",clDel,&ilItemNo,&ilCol,&ilPos);
		if (ilRc == RC_SUCCESS)
		{
			GetDataItem(clStfu,pcpData,ilItemNo,clDel,"","\0\0");
			dbg(TRACE,"STFU found: <%s>",clStfu);
		}
		sprintf(clKey,"%s,%s,%s",clDrrn,clSday,clStfu);

		if(CEDAArrayFindRowPointer(&(rgDrrArray.rrArrayHandle),
																	&(rgDrrArray.crArrayName[0]),
																	&(rgDrrArray.rrIdx01Handle),
																	&(rgDrrArray.crIdx01Name[0]),
																	clKey,&llFunc,
																	(void *) &pclRow ) == RC_SUCCESS)
		{
			dbg(TRACE,"DRR-URNO <%s> DRR-ROSL <%s> ",DRRFIELD(pclRow,igDrrURNO),DRRFIELD(pclRow,igDrrROSL));
			//strncpy(pcpUrno,DRRFIELD(pclRow,igDrrURNO),10);//Commented for UFIS 1075
			//pcpUrno[10] = '\0';//Added for UFIS 1075
			sprintf(pcpUrno,"%s",DRRFIELD(pclRow,igDrrURNO));//Added for UFIS 1075
		}
		else
		{
			dbg(TRACE,"DRR not found <%s>",clKey);
			ilRc = RC_NOTFOUND;
		}
	}
	return(ilRc);

}

static int GetDrrUrnoFromDrs(char *pcpFields,char *pcpData,char *pcpUrno)
{
	int	ilRc = RC_SUCCESS;
	char      clDel          = ',';
	int 		ilItemNo;
	int    	ilCol;
	int 		ilPos;
	char 	 	clDrru[24];



	dbg(TRACE,"GetDrrUrnoFromDrs: <%s> <%s>",pcpFields,pcpData);

	ilRc = FindItemInList(pcpFields,"DRRU",clDel,&ilItemNo,&ilCol,&ilPos);
	if (ilRc == RC_SUCCESS)
	{
		GetDataItem(clDrru,pcpData,ilItemNo,clDel,"","\0\0");
		dbg(TRACE,"DRRU found: <%s>",clDrru);
	}
	if (ilRc == RC_SUCCESS)
	{
		strncpy(pcpUrno,clDrru,10);
	}
	else
	{
		dbg(TRACE,"DRR not found ");
	}
	return(ilRc);

}

static int DeleteTmpAbsentJob(char *pcpUrno)
{
	int	ilRc = RC_SUCCESS;
	long llRowNum = ARR_FIRST;
	char 	 	clKey[124] = "";
	char *pclJobRow;
		char *pclUpdSelLst = NULL;
		char *pclUpdFldLst = NULL;
		char *pclUpdDatLst = NULL;
		char *pclUpdCmdLst = NULL;
		long llNumUrnos = 0L;

	/* check for existing job */
	strcpy(clKey,pcpUrno);
	TrimRight(clKey);
	ilRc = CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),
						&(rgJobArray.crArrayName[0]),
						&(rgJobArray.rrIdx01Handle),
						&(rgJobArray.crIdx01Name[0]),
						clKey,&llRowNum,
						(void *) &pclJobRow ) ;         

	if (ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayDeleteRow(&(rgJobArray.rrArrayHandle),
				&(rgJobArray.crArrayName[0]),llRowNum);
		if (ilRc == RC_SUCCESS)
		{
				ilRc=CEDAArrayWriteDBGetChanges(&rgJobArray.rrArrayHandle,
							rgJobArray.crArrayName,&pclUpdCmdLst,&pclUpdSelLst,&pclUpdFldLst,
							&pclUpdDatLst,&llNumUrnos,
							ARR_COMMIT_ALL_OK);
				/*
					if (llNumUrnos > 0)
					{
						CreateBc(llNumUrnos,pclUpdCmdLst,pclUpdSelLst,pclUpdFldLst,pclUpdDatLst);
					}
				*/
			commit_work();

		}
	}

	return(ilRc);
}



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
	int	   ilRc           = RC_SUCCESS;			/* Return code */
	int      i, ilCmd          = 0;

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;
    char    *pclOldData     = NULL;
	char 		clUrnoList[2400];
	char 		clTable[34];
	int			ilUpdPoolJob = TRUE;
	char	*pclNewline = NULL;
	BOOL	blFwdToRelVersion = FALSE;
	char clUrno[124];

	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;

	dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);
	bgUpdPlannedTimes = SetPlannedTimes ( prlCmdblk );

	strcpy(clTable,prlCmdblk->obj_name);

	/****************************************/
	DebugPrintBchead(DEBUG,prlBchead);
	DebugPrintCmdblk(TRACE,prlCmdblk);
	/* dbg(DEBUG,"originator follows event = %p ",prpEvent); */

	dbg(TRACE,"originator<%d>",prpEvent->originator);
	/* dbg(DEBUG,"selection follows Selection = %p ",pclSelection); */
	dbg(TRACE,"selection <%s>",pclSelection);
	dbg(TRACE,"fields    <%s>",pclFields);
	dbg(TRACE,"data      <%s>",pclData);
	if ( pclOldData )
		dbg(DEBUG,"Old data  <%s>",pclOldData);
	/****************************************/

	bgSwapShiftEvent = FALSE;
	bgChangeShiftEvent = FALSE;

	/* delete old data from pclData */
	if ((pclNewline = strchr(pclData,'\n')) != NULL)
	{
		pclOldData = pclNewline+1;
		*pclNewline = '\0';
	}
	
	ilRc = GetCommand(&prlCmdblk->command[0],&ilCmd);
	if(ilRc == RC_SUCCESS)
	{
		switch (ilCmd)
		{
		case CMD_IRT :
		case CMD_URT :
    		dbg(DEBUG,"HandleData: CMD <%s>  table <%s>",prgCmdTxt[ilCmd], clTable );
			if (strcmp(clTable,"SPRTAB") == 0)
			{
				if ( igVersion & V_OPERATIVE ) 
					SetSprStatus(pclFields,pclData);
			}
			else
			{
				ilUpdPoolJob = TRUE;
				if ( !strcmp(clTable,"DRRTAB") )
				{
					if (ilCmd==CMD_URT)
					{
						ilUpdPoolJob = IsDrrChangeImportant ( pclFields,pclData );
					    ilRc = UpdateEventData ( &(rgDrrArray.rrArrayHandle),
												rgDrrArray.crArrayName, pclSelection, 
												pclFields, pclData, clUrnoList);

					}
					else
						ilRc = InsertEventData (&(rgDrrArray.rrArrayHandle),
												rgDrrArray.crArrayName, pclFields, pclData, clUrnoList);
				}
				else
				{
					if ( !strcmp(clTable,"JOBTAB") && (ilCmd==CMD_URT) )
						ProcessRfjChange(pclFields,pclData,pclOldData);
					dbg(TRACE,"HandleData: CEDAArrayEventUpdate2 begin");
					ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
					dbg(TRACE,"HandleData: CEDAArrayEventUpdate2 end");
				}
				if (ilRc != RC_SUCCESS)
				{
      				dbg(TRACE,"HandleData: CEDAArrayEventUpdate failed <%d>",ilRc);
					/*  Maybe the 2nd version is interested in DRR-, DRS-, DRA-, DRD-event */
					if ( ( igModIdRelVersion > 0 ) && !strncmp(clTable, "DR", 2) )
						blFwdToRelVersion = TRUE;
				}
				else if( bgChangeShiftEvent == TRUE )
				{
					//change shift in opss-pm
					dbg ( TRACE, "HandleData: Change Shift Event" );
					ProcessChangeShift( clUrnoList, TRUE );
				}
				else
				{
					BOOL blUdrrFound = TRUE;

					if ( ilUpdPoolJob )	/* don't calculate new pooljobs if e.g. only REMA changed */
					{
						dbg(DEBUG,"%s: Urno=<%s>, Table=<%s>", prgCmdTxt[ilCmd],clUrnoList,clTable);
						
						if (strcmp(clTable,"DRATAB") == 0)
						{
							//blUdrrFound = (GetDrrUrnoFromDra(pclFields,pclData,clUrnoList) == RC_SUCCESS); //commented and added the below for UFIS 1075
							if(ilCmd==CMD_IRT)
							blUdrrFound = (GetDrrUrnoFromDra(pclFields,pclData,clUrnoList) == RC_SUCCESS);
							else
							blUdrrFound = (GetDrrUrnoFromDraFields(pclFields,pclData,clUrnoList) == RC_SUCCESS);
						} 
						else if (strcmp(clTable,"JOBTAB") == 0)
						{
							PreparePoolJob(clUrnoList);
							blUdrrFound = FALSE;
						}
						if (strcmp(clTable,"DRSTAB") == 0)
						{
							blUdrrFound = (GetDrrUrnoFromDrs(pclFields,pclData,clUrnoList) == RC_SUCCESS);
						}
						else if (strcmp(clTable,"DRDTAB") == 0)
						{
							blUdrrFound = (GetDrrUrnoFromDra(pclFields,pclData,clUrnoList) == RC_SUCCESS);
						}
						else if (strcmp(clTable,"DRGTAB") == 0)
						{
							blUdrrFound = (GetDrrUrnoFromDra(pclFields,pclData,clUrnoList) == RC_SUCCESS);
						}

						if(blUdrrFound)
						{
							ilRc = ProcessDrrChange ( clUrnoList, TRUE );
							if ( (ilRc== RC_NOTFOUND) && ( igModIdRelVersion > 0 ) && !strncmp(clTable, "DR", 2) )
								blFwdToRelVersion = TRUE;
						}
						else
						{
							dbg(TRACE,"DRR-URNO not found UrnoList=<%s>",clUrnoList);
							if ( ( igModIdRelVersion > 0 ) && !strncmp(clTable, "DR", 2) )
								blFwdToRelVersion = TRUE;
						}
					}
				}
			}
			break ;
		case CMD_DRT :
			/*dbg(TRACE,"HandleData <DRT>: Urnolist=<%s>",clUrnoList);*/
			if (strcmp(clTable,"DRATAB") == 0)
			{
				ilRc = MyHandleDrt ( &rgDraArray, pclSelection, clUrnoList);
				if ( ilRc == RC_SUCCESS )
				{
					GetUrnoFromSelection(pclSelection,clUrno);
					dbg(TRACE,"DRATAB DRT: Urno=<%s>",clUrno);
			  		DeleteTmpAbsentJob(clUrno);	
				}
			}
			else if (strcmp(clTable,"DRRTAB") == 0)
			{
				ilRc = MyHandleDrt ( &rgDrrArray, pclSelection, clUrnoList);
				if ( ilRc == RC_SUCCESS )
				{
					GetUrnoFromSelection(pclSelection,clUrno);
					ilRc = DeletePoolJobByUdrr ( clUrno );
					dbg(TRACE,"DRRTAB DRT: Urno=<%s> DeletePoolJobByUdrr RC <%d>",clUrno, ilRc );
					if ( igModIdRelVersion > 0 )
						blFwdToRelVersion = TRUE;
				}
			}
			else if (strcmp(clTable,"DRDTAB") == 0)
			{
				ilRc = MyHandleDrdDelete ( pclSelection, clUrnoList);
				dbg ( TRACE, "HandleData: MyHandleDrdDelete RC <%d>", ilRc ); 
			}	
			else if (strcmp(clTable,"DRGTAB") == 0)
			{
				ilRc = MyHandleDrgDelete ( pclSelection, clUrnoList);
				dbg ( TRACE, "HandleData: MyHandleDrgDelete RC <%d>", ilRc ); 
			}	
			else
			{
				HandleRfjDelete ( pclSelection );
				ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
			}
			if (ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"HandleData: CEDAArrayEventUpdate failed <%d>",ilRc);
				/*  Maybe the 2nd version is interested in DRR-, DRS-, DRA-, DRD-event */
				if ( ( igModIdRelVersion > 0 ) && !strncmp(clTable, "DR", 2) )
					blFwdToRelVersion = TRUE;
			}
			break ; 
		case CMD_CAE: 
			if ( igVersion & V_OPERATIVE ) 
			{
				dbg(DEBUG,"HandleData: CAE");
				/*  check absent employees, i. e. that have not clocked on at PLFR+SPROFFSETTO */
				ilRc = CheckEmployeePresence ();
			}
			break;	
		case CMD_SBC:
			/*  RELDRR must be handled also from operative polhdl */
			if ( /*( igVersion & V_RELEASES ) &&*/ !strcmp ( clTable,"RELDRR") )
			{	
				/*  if bgRELJOBtoACTION==TRUE switch off CEDAArraySendChanges2ACTION during */
				/*	processing of RELDRR-event  */
				CEDAArraySendChanges2ACTION(&rgJobArray.rrArrayHandle,
											rgJobArray.crArrayName,!bgRELJOBtoACTION);
				CEDAArraySendChanges2ACTION(&rgBrkArray.rrArrayHandle,
											rgBrkArray.crArrayName,!bgRELJOBtoACTION);
				/*  if bgRELJOBtoBCHDL==TRUE switch off CEDAArraySendChanges2ACTION during */
				/*	processing of RELDRR-event  */
				CEDAArraySendChanges2BCHDL(&rgJobArray.rrArrayHandle,
											rgJobArray.crArrayName,!bgRELJOBtoBCHDL);
				CEDAArraySendChanges2BCHDL(&rgBrkArray.rrArrayHandle,
											rgBrkArray.crArrayName,!bgRELJOBtoBCHDL);
				
				ilRc = HandleRelDrr ( pclData );
				dbg ( TRACE, "HandleData: HandleRelDrr RC <%d>", ilRc );

				/*  switch on CEDAArraySendChanges2ACTION again for single DRR-changes */
				CEDAArraySendChanges2ACTION(&rgJobArray.rrArrayHandle,
											rgJobArray.crArrayName,TRUE);
				CEDAArraySendChanges2ACTION(&rgBrkArray.rrArrayHandle,
											rgBrkArray.crArrayName,TRUE);
				CEDAArraySendChanges2BCHDL(&rgJobArray.rrArrayHandle,
											rgJobArray.crArrayName,TRUE);
				CEDAArraySendChanges2BCHDL(&rgBrkArray.rrArrayHandle,
											rgBrkArray.crArrayName,TRUE);
			}
			if ( ( igVersion == V_OPERATIVE ) && !strcmp ( clTable,"RELJOB") )
			{	
				dbg ( TRACE, "HandleData: Going to handle RELJOB" );
				ilRc = HandleRelJob (pclData, pclSelection );
			}
			break;
		default: ;
		}		/* end of switch */
		if ( pclNewline && (*pclNewline == '\0') )	
			*pclNewline = '\n';
		if ( blFwdToRelVersion && (igModIdRelVersion>0) )
		{
			dbg ( TRACE, "HandleData: Forwarding event to 2nd instance <%d>", igModIdRelVersion );
			ilRc = que(QUE_PUT, igModIdRelVersion, mod_id, prgItem->priority, 
				       prpEvent->data_length + sizeof(EVENT), (char *) prpEvent) ;  
			if ( ilRc != RC_SUCCESS )
				dbg ( TRACE, "HandleData: Forward of event to ModId <%d> failed RC <%d>",
					  igModIdRelVersion, ilRc );
		}
		else
		{
			for ( i=0; i<MAX_FORWARDS; i++ )
			{
				if ( !rgForwards[i].modid )
					break;
				if ( !strcmp ( rgForwards[i].table, clTable ) )
				{	/* Forward this event */
					ilRc = que(QUE_PUT, rgForwards[i].modid, mod_id, prgItem->priority, 
							   prpEvent->data_length + sizeof(EVENT), (char *) prpEvent) ;  
					if ( ilRc != RC_SUCCESS )
						dbg ( TRACE, "HandleData: Forward of event to ModId <%d> failed RC <%d>",
							  rgForwards[i].modid, ilRc );
					
				}
			}
		}
	}
	else
	{
		dbg(TRACE,"HandleData: GetCommand failed");
		DebugPrintBchead(TRACE,prlBchead);
		DebugPrintCmdblk(TRACE,prlCmdblk);
		dbg(TRACE,"selection <%s>",pclSelection);
		dbg(TRACE,"fields    <%s>",pclFields);
		dbg(TRACE,"data      <%s>",pclData);
	}/* end of if */

	dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

	return(RC_SUCCESS);
	
} /* end of HandleData */


static int InitializeNewPooljob(char *pcpNewJobBuf)
{
	int ilRc = RC_SUCCESS;
	char pclUrno[32];
	char pclNow[3200];

	/*GetNextValues(pclUrno,1);*/
	GetNextUrno(pclUrno);

	TimeToStr(pclNow,time(NULL));

	memset(pcpNewJobBuf,0,rgJobArray.lrArrayRowLen);

	strcpy(JOBFIELD(pcpNewJobBuf,igJobACFR)," ");
	strcpy(JOBFIELD(pcpNewJobBuf,igJobACTO)," ");
	strcpy(JOBFIELD(pcpNewJobBuf,igJobCDAT),pclNow);
	strcpy(JOBFIELD(pcpNewJobBuf,igJobDETY)," ");
	strcpy(JOBFIELD(pcpNewJobBuf,igJobHOPO)," ");
	strcpy(JOBFIELD(pcpNewJobBuf,igJobXXXX),"1");
	strcpy(JOBFIELD(pcpNewJobBuf,igJobSCOD)," ");
	strcpy(JOBFIELD(pcpNewJobBuf,igJobJOUR)," ");
	strcpy(JOBFIELD(pcpNewJobBuf,igJobLSTU)," ");
	strcpy(JOBFIELD(pcpNewJobBuf,igJobPLFR)," ");
	strcpy(JOBFIELD(pcpNewJobBuf,igJobPLTO)," ");
	strcpy(JOBFIELD(pcpNewJobBuf,igJobSTAT)," ");
	strcpy(JOBFIELD(pcpNewJobBuf,igJobTEXT)," ");
	strcpy(JOBFIELD(pcpNewJobBuf,igJobUAFT),"0");
	strcpy(JOBFIELD(pcpNewJobBuf,igJobUAID)," ");
	/*strcpy(JOBFIELD(pcpNewJobBuf,igJobUALO)," ");*/
	strcpy(JOBFIELD(pcpNewJobBuf,igJobUDSR)," ");
	strcpy(JOBFIELD(pcpNewJobBuf,igJobUJTY)," ");
	strcpy(JOBFIELD(pcpNewJobBuf,igJobURNO),pclUrno);
	strcpy(JOBFIELD(pcpNewJobBuf,igJobUSEC),"POLHDL");
	strcpy(JOBFIELD(pcpNewJobBuf,igJobUSEU)," ");
	strcpy(JOBFIELD(pcpNewJobBuf,igJobUSTF)," ");
	strcpy(JOBFIELD(pcpNewJobBuf,igJobUALO),cgUalo);
	if ( igJobALOC>=0 )
		strcpy(JOBFIELD(pcpNewJobBuf,igJobALOC),"POOL");

	dbg(TRACE,"Urno: <%s>",pclUrno);
	return ilRc;
}

static char *GetAlidUrnoFromBsd(char *pcpDrrRow)
{
	int ilRc = RC_SUCCESS;
	long llFunc = ARR_FIRST;
	char *pclRow;
	char clKey[124];

	if ( pigBsdKey )
	{
		*clKey = '\0';
		strcpy(clKey,DRRFIELD(pcpDrrRow,igDrrSCOD));

		/* check for BSDTAB entry*/
		ilRc = CEDAArrayFindRowPointer( &(rgBsdArray.rrArrayHandle),
																			&(rgBsdArray.crArrayName[0]),
																			&(rgBsdArray.rrIdx01Handle),
																			&(rgBsdArray.crIdx01Name[0]),
																			clKey,
																			&llFunc,
																			(void *) &pclRow ) ;         
		
		dbg(DEBUG,"GetAlidUrnoFromBsd: Key=<%s>, RC=%d",clKey,ilRc);
	}
	else
	{
		dbg ( DEBUG, "GetAlidUrnoFromBsd: No field in BSDTAB for this criterion" );
		ilRc = RC_INVALID;
	}
	if (ilRc == RC_SUCCESS)
	{
		strcpy(clKey,FIELDVAL(rgBsdArray,pclRow,*pigBsdKey));	
		TrimRight(clKey);
		if (! *clKey)
		{
			ilRc = RC_FAIL;
		}
	}
	
	if (ilRc == RC_SUCCESS)
	{
			strcpy(cgActualUalo,clKey);
			strncpy(cgActualUalo,GetAlidUrnoFromKey(clKey),10);
	}
	else
	{
		*cgActualUalo = '\0';
	}
 
	return cgActualUalo;
}


static char *GetAlidUrnoFromKey(char *pcpKey)
{
	int ilRc = RC_SUCCESS;
	long llFunc = ARR_FIRST;
	char *pclRow;
	char clKey[124];

	*clKey = '\0';

	strcpy(clKey,pcpKey);
	clKey[10] = '\0';
	TrimRight(clKey);
	dbg(DEBUG,"%05d:GetAlidUrnoFromKey:Index %d <%s> Key: <%s>",__LINE__,
				prgAlidArray->rrIdx01Handle,
					prgAlidArray->crIdx01Name,clKey);

	llFunc = ARR_FIRST;
  ilRc = CEDAArrayFindRowPointer( &(prgAlidArray->rrArrayHandle),
                                    &(prgAlidArray->crArrayName[0]),
                                    &(prgAlidArray->rrIdx01Handle),
                                    &(prgAlidArray->crIdx01Name[0]),
                                    clKey,&llFunc,
                                    (void *) &pclRow ) ;         

	if (ilRc == RC_SUCCESS)
	{
		strncpy(clKey,PFIELDVAL((prgAlidArray),pclRow,0),10);
		clKey[10] = '\0';
		dbg(TRACE,"Key = <%s> %d",clKey,ilRc);
		llFunc = ARR_FIRST;
		ilRc = CEDAArrayFindRowPointer( &(rgPoaArray.rrArrayHandle),
																			&(rgPoaArray.crArrayName[0]),
																			&(rgPoaArray.rrIdx01Handle),
																			&(rgPoaArray.crIdx01Name[0]),
																			clKey,&llFunc,
																			(void *) &pclRow ) ;         

		strcpy(clKey,FIELDVAL(rgPoaArray,pclRow,igPoaUPOL));
		dbg(TRACE,"%05d:Found: <%s> %d",__LINE__,clKey,ilRc);
	}
 else
 {
	 dbg(TRACE,"GetAlidUrnoFromKey: Code not found <%s> Rc=%d",clKey,ilRc);
	}

	if (ilRc == RC_SUCCESS)
	{
		strcpy(cgActualUalo,clKey);
	}
 else
 {
		*cgActualUalo = '\0';
	}
 
	return cgActualUalo;

}

static char *GetAlidUrnoFromStf(char *pcpDrrRow)
{
	int ilRc = RC_SUCCESS;
	/*char clUstf[24],clVato[15], clVafr[15], clPrio[5];*/
	long llFunc = ARR_FIRST;
	char *pclRow=0/*, *pclLast=0*/;
	/*int	 ilLastPrio=10000, ilActPrio, ilFound=0, ilValid;*/

	ilRc = GetValidStaffEntry ( prgStfAlidArray, pcpDrrRow, igVafrKey, 
							    igVatoKey, igPrioIdx, &pclRow );

/*
	strncpy(clUstf,DRRFIELD(pcpDrrRow,igDrrSTFU),10);
	clUstf[10] = '\0';
	TrimRight(clUstf);
	dbg(DEBUG,"Index %d <%s> Key: <%s>",prgStfAlidArray->rrIdx01Handle,
	prgStfAlidArray->crIdx01Name,clUstf);
	
	do 
	{
		ilRc = CEDAArrayFindRowPointer( &(prgStfAlidArray->rrArrayHandle),
										&(prgStfAlidArray->crArrayName[0]),
										&(prgStfAlidArray->rrIdx01Handle),
										&(prgStfAlidArray->crIdx01Name[0]),
										clUstf,&llFunc, (void *) &pclRow ) ;         
		if(ilRc == RC_SUCCESS)
		{	
			ilValid = 1;
			if ( (igVafrKey>=0) && (igVatoKey>=0) )
			{
				strncpy ( clVafr, PFIELDVAL((prgStfAlidArray),pclRow,igVafrKey), 14 );
				strncpy ( clVato, PFIELDVAL((prgStfAlidArray),pclRow,igVatoKey), 14 );
				clVafr[14] = clVato[14] = '\0';
				TrimRight ( clVafr );
				TrimRight ( clVato );
							
				if ( (strlen (clVafr)==14) && 
					 ( strcmp ( DRRFIELD(pcpDrrRow,igDrrAVFR), clVafr ) < 0 ) )
					ilValid = 0;
				else
					if ( (strlen (clVato)==14) && 
						 ( strcmp ( clVato, DRRFIELD(pcpDrrRow,igDrrAVTO) ) < 0 ) )
						ilValid = 0;
				dbg ( DEBUG, "Validity-Check: <%s> Valid <%s>-<%s> PoolJob <%s>-<%s> Valid <%d>",
					  prgStfAlidArray->crArrayName, clVafr, clVato, DRRFIELD(pcpDrrRow,igDrrAVFR), 
					  DRRFIELD(pcpDrrRow,igDrrAVTO), ilValid );   
			}		
			if ( ilValid )
			{
				if ( igPrioIdx <0 )		// this criterion has no Priority	
				{						
					ilFound = 1;		// first valid record is ok			
					pclLast = pclRow;
				}	 
				else
				{
					strncpy ( clPrio, PFIELDVAL((prgStfAlidArray),pclRow,igPrioIdx), 4 );
					clPrio[4] = '\0';
					TrimRight ( clPrio );
					ilActPrio = (strlen(clPrio)>0)  ? atoi(clPrio) : 9999;
					if ( ilActPrio < ilLastPrio )
					{
						dbg ( DEBUG, "Prio-Check: New item <%s> found Old Prio <%d> new Prio<%d>",
							  pclRow, ilLastPrio, ilActPrio );
						ilLastPrio = ilActPrio ;
						pclLast = pclRow;			// keep valid record, look for more 	
					}
					else
						dbg ( DEBUG, "Prio-Check: Item with lower prio <%d> found, Old Prio <%d>",
							  ilActPrio, ilLastPrio );
				}
			} // endif ilValid 
		} // endif ilRc==RC_SUCCESS 
		llFunc = ARR_NEXT;
	} while (!ilFound && (ilRc == RC_SUCCESS ) );

	if ( pclLast )
	{
		ilRc = RC_SUCCESS;
		pclRow = pclLast;
	}
	else
		dbg ( TRACE, "GetAlidUrnoFromStf: No valid item found in <%s> for SURN <%s>",
					  prgStfAlidArray->crArrayName, clUstf );
*/
	if(ilRc == RC_SUCCESS)
	{
		char clKey[124];
		
		strcpy(clKey,pclRow);
		clKey[10] = '\0';
		TrimRight(clKey);
		dbg(DEBUG,"%05d:GetAlidUrnoFromStf:Index %d <%s> Key: <%s>",__LINE__,
					prgAlidArray->rrIdx01Handle,
						prgAlidArray->crIdx01Name,clKey);
		
		llFunc = ARR_FIRST;
		ilRc = CEDAArrayFindRowPointer( &(prgAlidArray->rrArrayHandle),
										&(prgAlidArray->crArrayName[0]),
										&(prgAlidArray->rrIdx01Handle),
										&(prgAlidArray->crIdx01Name[0]),
										clKey,&llFunc,
										(void *) &pclRow ) ;         

		strncpy(clKey,PFIELDVAL((prgAlidArray),pclRow,0),10);
		clKey[10] = '\0';
		dbg(TRACE,"Key = <%s> %d",clKey,ilRc);
		llFunc = ARR_FIRST;
		ilRc = CEDAArrayFindRowPointer( &(rgPoaArray.rrArrayHandle),
		                                &(rgPoaArray.crArrayName[0]),
		                                &(rgPoaArray.rrIdx01Handle),
		                                &(rgPoaArray.crIdx01Name[0]),
		                                clKey,&llFunc,
		                                (void *) &pclRow ) ;         
		
		dbg(TRACE,"%05d:Found: <%s> %d",__LINE__,FIELDVAL(rgPoaArray,pclRow,igPoaUPOL),ilRc);
		if (ilRc == RC_SUCCESS)
		{
			strcpy(cgActualUalo,FIELDVAL(rgPoaArray,pclRow,igPoaUPOL));
		}
		else
			/*  if pool assignment criterion is default pool, it is not necessary to
				have a mapping default pool1 -> pool2								*/
			if ( prgStfAlidArray == &rgSdaArray )
			{
				dbg ( DEBUG, "GetAlidUrnoFromStf: Criterion Pool, no mapping for pool <%s> found.", clKey );
				strcpy(cgActualUalo,clKey);
				ilRc = RC_SUCCESS;
			}		
	}

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"GetAlidUrnoFromStf: Code not found <%s> Rc=%d",DRRFIELD(pcpDrrRow,igDrrSTFU),ilRc);
		*cgActualUalo = '\0';
	}
	return cgActualUalo;

}

static int CheckExistingJob(char *pcpDrrRow)
{
	int ilRc = RC_SUCCESS;
	long llFunc;
	char *pclRow;
	char clKey[32];


	ilRc = CheckAbsence(pcpDrrRow);

	if(ilRc != RC_SUCCESS)
	{

		/* check for existing pooljob */
		llFunc = ARR_FIRST;
		strcpy(clKey,DRRFIELD(pcpDrrRow,igDrrURNO));
		TrimRight(clKey);
		dbg(DEBUG,"searching for job with UDSR = <%s>",clKey);
		ilRc = CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),
																			&(rgJobArray.crArrayName[0]),
																			&(rgJobArray.rrIdx01Handle),
																			&(rgJobArray.crIdx01Name[0]),
																			clKey,&llFunc,
																			(void *) &pclRow ) ;         

		if (ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"Job for UDSR = <%s> exists already", clKey );
		}
		else 
			ilRc = RC_JOB_TO_CREATE;
	}
	else
	{
		ilRc = DeletePoolJob(pcpDrrRow);
		if ( ilRc == RC_SUCCESS )
			ilRc = RC_JOB_DELETED;
	}
	return ilRc;
}


static int CheckShift(char *pcpDrrRow,char *pcpBsdu)
{
	int ilRc = RC_SUCCESS;
	long llFunc;
	char *pclRow;
	char clRoss[32];
	char clStat[10];

	/* check for roster status */
	strcpy(clRoss,DRRFIELD(pcpDrrRow,igDrrROSS));
	if(*clRoss != 'A')
	{
		ilRc = RC_FAIL;
	}

    if(ilRc == RC_SUCCESS)
	{
		/* check for DRSTAB entry*/
		llFunc = ARR_FIRST;
		dbg(TRACE,"CheckShift Check for DRS entry ");
		dbg(TRACE,"CheckShift DRRU <%s>",DRRFIELD(pcpDrrRow,igDrrURNO));
		if(CEDAArrayFindRowPointer( &(rgDrsArray.rrArrayHandle),
																			&(rgDrsArray.crArrayName[0]),
																			&(rgDrsArray.rrIdx01Handle),
																			&(rgDrsArray.crIdx01Name[0]),
																			DRRFIELD(pcpDrrRow,igDrrURNO),
																			&llFunc,
																			(void *) &pclRow ) == RC_SUCCESS)
		{
			strcpy(clStat,FIELDVAL(rgDrsArray,pclRow,igDrsSTAT));
			dbg(TRACE,"CheckShift clStat <%s>",clStat);
			if(*clStat == 'S')
			{
				ilRc = RC_FAIL;
			}
		}
	}

    if(ilRc == RC_SUCCESS)
	{
		char clSday[32] = "";
		char clVafr[32] = "";
		char clVato[32] = "";

		strcpy(clSday,DRRFIELD(pcpDrrRow,igDrrSDAY));

		/* check for BSDTAB entry*/
		llFunc = ARR_FIRST;
		while((ilRc = CEDAArrayFindRowPointer( &(rgBsdArray.rrArrayHandle),
																			&(rgBsdArray.crArrayName[0]),
																			&(rgBsdArray.rrIdx01Handle),
																			&(rgBsdArray.crIdx01Name[0]),
																			DRRFIELD(pcpDrrRow,igDrrSCOD),
																			&llFunc,
																			(void *) &pclRow )) == RC_SUCCESS)
		{
			strcpy(clVafr,FIELDVAL(rgBsdArray,pclRow,igBsdVAFR));
			clVafr[8] = '\0';
			strcpy(clVato,FIELDVAL(rgBsdArray,pclRow,igBsdVATO));
			clVato[8] = '\0';
			TrimRight(clVato);
			if ( (strcmp(clSday,clVafr) >= 0) && 
				( (strcmp(clVato," ") == 0) || (strcmp(clSday,clVato) <= 0) ) )
			{
				strcpy(pcpBsdu,FIELDVAL(rgBsdArray,pclRow,igBsdURNO));	
				return ilRc;			
			}

			llFunc = ARR_NEXT;
		};

		dbg(TRACE,"No entry in BSDTAB found <%s> %s-%s",
						DRRFIELD(pcpDrrRow,igDrrSCOD),
						DRRFIELD(pcpDrrRow,igDrrAVFR),
						DRRFIELD(pcpDrrRow,igDrrAVTO));
		*pcpBsdu = '\0';
	}

	return ilRc;
}


static int CheckAbsence(char *pcpDrrRow)
{
	int ilRc = RC_SUCCESS;
	long llFunc;
	char *pclRow;
	char clRoss[32];
	char clStat[10];

	/* check for roster status */
	strcpy(clRoss,DRRFIELD(pcpDrrRow,igDrrROSS));
	if(*clRoss != 'A')
	{
		ilRc = RC_FAIL;
	}




	/* check for BSDTAB entry*/
	llFunc = ARR_FIRST;
	ilRc = CEDAArrayFindRowPointer( &(rgOdaArray.rrArrayHandle),
																		&(rgOdaArray.crArrayName[0]),
																		&(rgOdaArray.rrIdx01Handle),
																		&(rgOdaArray.crIdx01Name[0]),
																		DRRFIELD(pcpDrrRow,igDrrSCOD),
																		&llFunc,
																		(void *) &pclRow ) ;         
	
	if (ilRc != RC_SUCCESS)
	{
		dbg(DEBUG,"No entry in ODATAB found <%s> %s-%s",
						DRRFIELD(pcpDrrRow,igDrrSCOD),
						DRRFIELD(pcpDrrRow,igDrrAVFR),
						DRRFIELD(pcpDrrRow,igDrrAVTO));
	}

	if(ilRc != RC_SUCCESS)
	{
		/* check for DRSTAB entry*/
		ilRc = RC_FAIL;
		llFunc = ARR_FIRST;
		if(CEDAArrayFindRowPointer( &(rgDrsArray.rrArrayHandle),
																			&(rgDrsArray.crArrayName[0]),
																			&(rgDrsArray.rrIdx01Handle),
																			&(rgDrsArray.crIdx01Name[0]),
																			DRRFIELD(pcpDrrRow,igDrrURNO),
																			&llFunc,
																			(void *) &pclRow ) == RC_SUCCESS)
		{
			strcpy(clStat,FIELDVAL(rgDrsArray,pclRow,igDrsSTAT));
			if(*clStat == 'S')
			{
				ilRc = RC_SUCCESS;
			}
		}
	}
	return ilRc;
}

/*
static int CreatePoolJob(char *pcpDrrRow,int ipJobNo)
{
	int ilRc = RC_SUCCESS;
	long llFunc;
	char *pclRow;
	char pclOldJobUrno[24] = "";
	char clStartDay[32] = "";
	char clFrom[32] = "";
	char clTo[32] = "";
	char clStart[32] = ""; 
	char clDuration[32] = "";
	int ilMinute;
	int ilStart,ilDuration;
	BOOL blFound;
	
	
	dbg(TRACE,"CreatePooljob");
	do
	{
		blFound = FALSE;
		ilStart = -1;
		ilDuration = 0;
		for(ilMinute = 0; ilMinute < MAXMINUTES; ilMinute++)
		{
			if (cgJobs[ilMinute] == ipJobNo)
			{
				if (ilStart < 0)
				{
					ilStart = ilMinute;
				}
				ilDuration++;
				cgJobs[ilMinute] = -1;
			}
			else if (ilStart >= 0)
			{
				break;
			}
		}
		if (ilStart != -1)
		{
			blFound = TRUE;
			if (ilDuration > 1)
			{
				ilDuration--;
				strncpy(clStartDay,DRRFIELD(pcpDrrRow,igDrrAVFR),8);
				clStartDay[8] = '\0';
				strcat(clStartDay,"000000");
				sprintf(clStart,"%d",ilStart*60);

				StrAddTime(clStartDay,clStart,clFrom,1);
				dbg ( TRACE, "Nach StrAddTime clStartDay <%s> clStart <%s> clFrom <%s>", 
					  clStartDay, clStart, clFrom );

				sprintf(clDuration,"%d",ilDuration*60);
				StrAddTime(clFrom,clDuration,clTo,1); 
				dbg ( TRACE, "Nach StrAddTime clDuration <%s> clTo <%s>", clDuration, clTo );

				dbg(TRACE,"Create Job No %d DRR-URNO=(%s) from %d(%dMin.) %s to %s",
					ipJobNo,DRRFIELD(pcpDrrRow,igDrrURNO),
					ilStart,ilDuration,clFrom,clTo);
				if (ipJobNo == 0)
				{
						char clKey[24];

						strncpy(clKey,DRRFIELD(pcpDrrRow,igDrrURNO),10);
						clKey[10] = '\0';
						dbg(TRACE,"Search Old Pooljob Key = <%s> %d",clKey,ilRc);
						llFunc = ARR_FIRST;
 						 ilRc = CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),
										&(rgJobArray.crArrayName[0]),
										&(rgJobArray.rrIdx01Handle),
										&(rgJobArray.crIdx01Name[0]),
										clKey,&llFunc,
										(void *) &pclRow ) ;         
					if (ilRc == RC_SUCCESS)
					{
						char clStatus[10];

						strcpy(clStatus,JOBFIELD(pclRow,igJobXXXX));
					
						// clStatus == '1' is only TRUE for new created Jobs,
						//	which are not saved in Database 
						if (*clStatus != '1')
						{
							strcpy(pclOldJobUrno,JOBFIELD(pclRow,igJobURNO));
							dbg(TRACE,"Old Job Urno = <%s>",pclOldJobUrno);
						}
				dbg(TRACE,"%05d:Old job found Urno = <%s> Key=<%s>Status=<%s>pclRow = <%s>",__LINE__,
							pclOldJobUrno,clKey,clStatus,pclRow);
					}
					else
					{
						dbg(TRACE,"Old job not found RC=%d Key=<%s>",
							ilRc,clKey,pclRow);
						*pclOldJobUrno = '\0';
					}
					CreateSinglePoolJob(pcpDrrRow,pclOldJobUrno,clFrom,clTo,ipJobNo);
				}
				else
				{
					*pclOldJobUrno = '\0';
					CreateSinglePoolJob(pcpDrrRow,pclOldJobUrno,clFrom,clTo,ipJobNo);
				}
			}
		}
	} while (blFound);

	dbg(TRACE,"CreatePooljob finished");

	return ilRc;
}
*/
static int CreatePoolJob(char *pcpDrrRow,int ipJobNo)
{
	int ilRc = RC_SUCCESS;
	/*long llFunc;
	  char *pclRow;*/
	char pclOldJobUrno[24] = "";
	time_t llJobStart, llJobEnd;
	char clFrom[32] = "";
	char clTo[32] = "";
	int ilMinute;
	int ilStart,ilDuration;
	BOOL blFound;

	
	dbg(TRACE,"CreatePooljob");
	

	do
	{
		blFound = FALSE;
		ilStart = -1;
		ilDuration = 0;
		for(ilMinute = 0; ilMinute < MAXMINUTES; ilMinute++)
		{
			if (cgJobs[ilMinute] == ipJobNo)
			{
				if (ilStart < 0)
				{
					ilStart = ilMinute;
				}
				ilDuration++;
				cgJobs[ilMinute] = -1;
			}
			else if (ilStart >= 0)
			{
				break;
			}
		}
		if (ilStart != -1)
		{
			blFound = TRUE;
			if (ilDuration > 1)
			{
				ilDuration--; 
				llJobStart = lgStartofDayUTC + (time_t)ilStart*60;
				llJobEnd = llJobStart + (time_t)ilDuration*60;

				TimeToStr (clFrom,llJobStart );
				TimeToStr (clTo,llJobEnd );

				dbg(TRACE,"Create Job No %d DRR-URNO=(%s) from %d(%dMin.) %s to %s",
					ipJobNo,DRRFIELD(pcpDrrRow,igDrrURNO),
					ilStart,ilDuration,clFrom,clTo);
				/*
				if (ipJobNo == 0)
				{
					char clKey[24];
					strncpy(clKey,DRRFIELD(pcpDrrRow,igDrrURNO),10);
					clKey[10] = '\0';

					dbg(TRACE,"Search Old Pooljob Key = <%s> %d",clKey,ilRc);
					llFunc = ARR_FIRST;
 					 ilRc = CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),
									&(rgJobArray.crArrayName[0]),
									&(rgJobArray.rrIdx01Handle),
									&(rgJobArray.crIdx01Name[0]),
									clKey,&llFunc, (void *) &pclRow ) ;         
					if (ilRc == RC_SUCCESS)
					{
						char clStatus[10];

						strcpy(clStatus,JOBFIELD(pclRow,igJobXXXX));
					
						// clStatus == '1' is only TRUE for new created Jobs,
						//	which are not saved in Database  
						if (*clStatus != '1')
						{
							strcpy(pclOldJobUrno,JOBFIELD(pclRow,igJobURNO));
							dbg(TRACE,"Old Job Urno = <%s>",pclOldJobUrno);
						}
					dbg(TRACE,"%05d:Old job found Urno = <%s> Key=<%s>Status=<%s>pclRow = <%s>",__LINE__,
							pclOldJobUrno,clKey,clStatus,pclRow);
					}
					else
					{
						dbg(TRACE,"Old job not found RC=%d Key=<%s>",
							ilRc,clKey,pclRow);
						*pclOldJobUrno = '\0';
					}
					CreateSinglePoolJob(pcpDrrRow,pclOldJobUrno,clFrom,clTo,ipJobNo);
				}
				else*/
				{
					*pclOldJobUrno = '\0';
					CreateSinglePoolJob(pcpDrrRow,pclOldJobUrno,clFrom,clTo,ipJobNo);
				}
			}
		}
	} while (blFound);
	dbg(TRACE,"CreatePooljob finished");
	return ilRc;
}

static int CreateSinglePoolJob(char *pcpDrrRow,char *pcpOldJobUrno,
		char *pcpFrom,char *pcpTo,int ipJobNo)
{
	int ilRc = RC_SUCCESS;
	long llRowNum;
	char *pclTmpBuf, *pclUdrd=0, *pclUdel=0;
	char pclFieldName[32];
	char clAcfr[32],clActo[32];
	char clKey[32];
	char clUAlo[32], clPool[24];
	int ilLc;

	dbg(TRACE,"CreateSinglePooljob From <%s> To <%s>", pcpFrom, pcpTo );

	*clUAlo = '\0';
	*clKey = '\0';

	
	if (ilRc == RC_SUCCESS)
	{
		InitializeNewPooljob(cgNewJobBuf);
	
		dbg(DEBUG,"New Job Urno <%s>",JOBFIELD(cgNewJobBuf,igJobURNO));
		*clUAlo = '\0';
		if(ipJobNo > 0)
		{
			strcpy(clKey,rgDelDrr[ipJobNo].clKey);
			strcpy(clUAlo,rgDelDrr[ipJobNo].clKey);
			clUAlo[10] = 0;
			TrimRight(clUAlo);
			if(rgDelDrr[ipJobNo].ilType == DRDDEL)
			{
					strncpy(JOBFIELD(cgNewJobBuf,igJobUDEL),
						rgDelDrr[ipJobNo].clUrno,10);
					pclUdel = rgDelDrr[ipJobNo].clUrno;
					strncpy(JOBFIELD(cgNewJobBuf,igJobUDRD),"0",10);
			}	
			else 
			{
					strncpy(JOBFIELD(cgNewJobBuf,igJobUDRD),
						rgDelDrr[ipJobNo].clUrno,10);
					pclUdrd = rgDelDrr[ipJobNo].clUrno;
					strncpy(JOBFIELD(cgNewJobBuf,igJobUDEL),"0",10);
			}
		}
		else
		{
					strncpy(JOBFIELD(cgNewJobBuf,igJobUDRD),"0",10);
					strncpy(JOBFIELD(cgNewJobBuf,igJobUDEL),"0",10);
		}

		strcpy(JOBFIELD(cgNewJobBuf,igJobSCOD),DRRFIELD(pcpDrrRow,igDrrSCOD));
		dbg(TRACE,"clUAlo <%s>",clUAlo);
		if (*cgPoaREFT != '\0')
		{
			if ( (*clUAlo == '\0') && pigDrrKey )
			{
				strcpy(clUAlo,GetAlidUrnoFromDrr(pcpDrrRow));
				dbg(TRACE,"AlidUrnoFromDrr  clUAlo=<%s> ActualUalo=<%s> JobNo %d",
						clUAlo,cgActualUalo,ipJobNo);
			}
			
			if (*clUAlo == '\0')
			{
				dbg(TRACE,"AlidUrnoFromBsd  follows");
				strcpy(clUAlo,GetAlidUrnoFromBsd(pcpDrrRow));
				dbg(TRACE,"AlidUrnoFromBsd  clUAlo=<%s> ActualUalo=<%s> JobNo %d",
						clUAlo,cgActualUalo,ipJobNo);
			}

			if (*clUAlo == '\0')
			{
				dbg(TRACE,"GetAlidUrnoFromStf  follows");
				strncpy(clUAlo,GetAlidUrnoFromStf(pcpDrrRow),10);
				clUAlo[10] = 0;
				dbg(TRACE,"GetAlidUrnoFromStf  clUAlo=<%s> ActualUalo=<%s> JobNo %d",
						clUAlo,cgActualUalo,ipJobNo);
				TrimRight(clUAlo);
			}
		}

		if (*clUAlo == '\0')
		{
			dbg(TRACE,"Default Pool  follows");
			strcpy(cgActualUalo,cgDefaultPool);
			strcpy(clUAlo,cgDefaultPool);
			dbg(TRACE,"Default Pool  clUAlo=<%s> ActualUalo=<%s> JobNo %d",
					clUAlo,cgActualUalo,ipJobNo);
		}

		if (*clUAlo == '\0')
			dbg(TRACE,"No ALID found  clUAlo=<%s> ActualUalo=<%s> JobNo %d",
					   clUAlo,cgActualUalo,ipJobNo);
		strcpy(clAcfr,pcpFrom);
		/* LocalToUtc(clAcfr); */
		strcpy(clActo,pcpTo);
		/* LocalToUtc(clActo); */
		strncpy(JOBFIELD(cgNewJobBuf,igJobACFR),clAcfr,14);
		strncpy(JOBFIELD(cgNewJobBuf,igJobACTO),clActo,14);
		strncpy(JOBFIELD(cgNewJobBuf,igJobDETY)," ",1);
		strncpy(JOBFIELD(cgNewJobBuf,igJobHOPO),
					DRRFIELD(pcpDrrRow,igDrrHOPO),3);
		strncpy(JOBFIELD(cgNewJobBuf,igJobJOUR)," ",10);
		strncpy(JOBFIELD(cgNewJobBuf,igJobPLFR),clAcfr,14);
		strncpy(JOBFIELD(cgNewJobBuf,igJobPLTO),clActo,14);
		strncpy(JOBFIELD(cgNewJobBuf,igJobSTAT),"P        ",10);
		if(ipJobNo > 0)
		{
			strncpy(clUAlo,GetAlidUrnoFromKey(clKey),10);
			if(*clUAlo == '\0')
			{
				strcpy(cgActualUalo,cgDefaultPool);
				strcpy(clUAlo,cgDefaultPool);
			}
			strcpy(JOBFIELD(cgNewJobBuf,igJobUAID),clUAlo);
		}
		else
		{
			strncpy(JOBFIELD(cgNewJobBuf,igJobUAID),clUAlo,10);
		}
		if ( (igJobALID>=0) && (GetPoolName ( clUAlo, clPool) == RC_SUCCESS ) )
		{
			clPool[14]='\0';
			strcpy(JOBFIELD(cgNewJobBuf,igJobALID),clPool );	
		}

		/*strncpy(JOBFIELD(cgNewJobBuf,igJobUALO),cgUalo,10);*/
		strncpy(JOBFIELD(cgNewJobBuf,igJobUDSR),
					DRRFIELD(pcpDrrRow,igDrrURNO),10);
		strncpy(JOBFIELD(cgNewJobBuf,igJobUJTY),cgUjty,10);
		strncpy(JOBFIELD(cgNewJobBuf,igJobUSTF),
					DRRFIELD(pcpDrrRow,igDrrSTFU),10);
		if (bgUseFilt)
		{
			strncpy(JOBFIELD(cgNewJobBuf,igJobFILT),
					DRRFIELD(pcpDrrRow,igDrrFILT),10);
		}
		if ( igJobWGPC >= 0 )
		{
			ilLc = GetWgpcForShift ( pcpDrrRow, clKey );
			if ( ilLc == RC_SUCCESS )
				strncpy(JOBFIELD(cgNewJobBuf,igJobWGPC), clKey, 5 );
			dbg ( DEBUG, "CreateSinglePooljob: GetWgpcForShift RC <%d> Code <%s>", ilLc, clKey );
		}
		if ( igJobFCCO >= 0 )
		{
			ilLc = GetFccoForPjb ( pcpDrrRow, pclUdrd, pclUdel, clKey );
			if ( ilLc == RC_SUCCESS )
				strncpy(JOBFIELD(cgNewJobBuf,igJobFCCO), clKey, 10 );
			dbg ( DEBUG, "CreateSinglePooljob: GetFccoForPjb RC <%d> Code <%s>", ilLc, clKey );
		}

		*cgTmpBuf = '\0';
		pclTmpBuf = cgTmpBuf;
		for(ilLc = 0; ilLc < rgJobArray.lrArrayFieldCnt; ilLc++)
		{
				*pclFieldName = '\0';
				GetDataItem(pclFieldName,rgJobArray.crArrayFieldList,
								ilLc+1,',',"","\0\0");
				strcpy(pclTmpBuf,JOBFIELD(cgNewJobBuf,ilLc)); 
				dbg(DEBUG,"%05d:%s <%s> <%s>",__LINE__,
					pclFieldName,JOBFIELD(cgNewJobBuf,ilLc),pclTmpBuf);
				pclTmpBuf += strlen(pclTmpBuf)+1;
		}
		/*snap(cgNewJobBuf,312,outp);
		dbg(TRACE,"PoolJobsUrno: %s ",
					JOBFIELD(cgNewJobBuf,igJobURNO));*/
		llRowNum = ARR_FIRST;
		ilRc = CEDAArrayAddRow(&rgNewJobsArray.rrArrayHandle,
							   rgNewJobsArray.crArrayName,&llRowNum,
							   (void *)cgTmpBuf);
		/*  moved to UpdatePoolJob 
		if ( bgBreak )
			ilRc = CreateBreakJob(cgNewJobBuf, pcpDrrRow );  */
		

		/*
		if(*pcpOldJobUrno)
		{
			UpdatePoolJob(pcpOldJobUrno,cgTmpBuf);
			CreateTmpAbsJob(pcpDrrRow,pcpOldJobUrno);
		}
		else
		{
			strcpy(clPoolJobUrno,JOBFIELD(cgNewJobBuf,igJobURNO));
			llRowNum = ARR_FIRST;
			ilRc=	CEDAArrayAddRow(&rgJobArray.rrArrayHandle,
					rgJobArray.crArrayName,&llRowNum,
						(void *)cgTmpBuf);
			dbg(TRACE,"New PoolJob Urno=<%s>",
					JOBFIELD(cgTmpBuf,igJobURNO));
			if ( bgBreak )
				ilRc = CreateBreakJob(cgNewJobBuf, pcpDrrRow );
			CreateTmpAbsJob(pcpDrrRow,clPoolJobUrno);
		}*/
	}	
	dbg(TRACE,"CreateSinglePooljob finished");
	return ilRc;
}

static int UpdatePoolJob(char *pcpOldJobUrno,char *pcpSource)
{
		int ilRc = RC_SUCCESS;
		int ilLc;
		long llCurrent = ARR_FIRST;
		char *pclRow;
		char *pclTmpOld;
		char *pclTmpNew;
		char pclFieldName[244];
		char clDontChange[41] = "CDAT,USEC,URNO,TEXT,STAT";
		char clStat[21];
		BOOL blRelevantChanges = FALSE;

		dbg(TRACE,"UpdatePoolJob Old=<%s>",pcpOldJobUrno);

		ilRc = CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
																			&(rgJobArray.crArrayName[0]),
																			&(rgJobArray.rrIdx02Handle),
																			&(rgJobArray.crIdx02Name[0]),
																			pcpOldJobUrno,&llCurrent,
																			(void *) &pclRow );
		if(ilRc == RC_SUCCESS)
		{
			{
				char clURNO[40];
				long	*pllFldPos    = NULL;

				strcpy(clURNO,"unknwon");
				CEDAArrayGetField(&rgJobArray.rrArrayHandle,
				rgJobArray.crArrayName,pllFldPos,"URNO",20,llCurrent,clURNO);
				dbg(DEBUG,"URNO=<%s> llCurrent=%d",clURNO,llCurrent);
			}
			dbg(DEBUG,"UpdatePoolJob old job found Row %ld",llCurrent);
			dbg(DEBUG,"New Job: <%s>",pcpSource);
			pclTmpOld = pclRow;
			pclTmpNew = pcpSource;
			strcpy(clStat, FIELDVAL(rgJobArray,pclTmpOld,igJobSTAT));
			/*  dont change STAT,PLFR,PLTO,ACFR for confirmed pooljobs */
			if ( clStat[0] == 'C' )			
				strcat ( clDontChange, ",PLFR,PLTO,ACFR" );
			else if ( !bgUpdPlannedTimes )
				strcat ( clDontChange, ",PLFR,PLTO" );

			for(ilLc = 0; ilLc < rgJobArray.lrArrayFieldCnt; ilLc++)
			{
				*pclFieldName = '\0';
				GetDataItem(pclFieldName,rgJobArray.crArrayFieldList,
								ilLc+1,',',"","\0\0");
				dbg(DEBUG,"%05d:%s <%s> <%s>",__LINE__, pclFieldName,pclTmpOld,pclTmpNew);
				if( !strstr(clDontChange, pclFieldName) && 
					!strstr("LSTU,USEU,XXXX", pclFieldName) &&
					strcmp(pclTmpOld,pclTmpNew) )
				{	/* Copy only changes in relevant fields */
					long llFieldNo;
					dbg ( DEBUG, "UpdatePoolJob: field <%s> Old <%s> -> New <%s>", pclFieldName, pclTmpOld,pclTmpNew );
					llFieldNo = ilLc;
					CEDAArrayPutField(&rgJobArray.rrArrayHandle,
									  rgJobArray.crArrayName, &llFieldNo,
									  pclFieldName,llCurrent, pclTmpNew);
					blRelevantChanges = TRUE;
				}
				pclTmpOld += strlen(pclTmpOld)+1;
				pclTmpNew += strlen(pclTmpNew)+1;
			}
			if ( blRelevantChanges )
			{
				ilRc = CEDAArrayPutField(&rgJobArray.rrArrayHandle,
										 rgJobArray.crArrayName, 
										 (long*) &igJobUSEU, "USEU",llCurrent,
										 (void *)"POLHDL");
				dbg(DEBUG,"RC from PutField USEU=%d,llCurrent=%d",ilRc,llCurrent);
				dbg(DEBUG,"Job-Fields: <%s>",rgJobArray.crArrayFieldList);
			}
			else
				dbg(TRACE,"UpdatePoolJob: No relevant changes for old UNRO <%s> llCurrent=%d",
				    pcpOldJobUrno,llCurrent);
				
			for(ilLc = 0; ilLc < rgJobArray.lrArrayFieldCnt; ilLc++)
			{
				long llFld1 = -1;
				char clTmp[256];

				*pclFieldName = '\0';
				GetDataItem(pclFieldName,rgJobArray.crArrayFieldList,
								ilLc+1,',',"","\0\0");
				strcpy(clTmp,"unknwon");
				CEDAArrayGetField(&rgJobArray.rrArrayHandle,
				rgJobArray.crArrayName,&llFld1,pclFieldName,256,llCurrent,clTmp);

				dbg(DEBUG,"%05d:%s %d <%s> ",__LINE__,
					pclFieldName,llFld1,clTmp);
			}

		}
		return ilRc;
}
/*
static int PutJob(char *pcpFrom,char *pcpTo,char cpJobNo)
{
	int ilRc = RC_SUCCESS;
	char pclTmp[12];
	int ilFrom,ilTo,ilLc;
	int ilTimeStart = 8;

	if (strlen(pcpFrom) < 14)
	{
		ilTimeStart = 0;
	}

	strncpy(pclTmp,&pcpFrom[ilTimeStart],2);
	pclTmp[2] = '\0';
	ilFrom = atoi(pclTmp) * 60;
	strncpy(pclTmp,&pcpFrom[ilTimeStart+2],2);
	pclTmp[2] = '\0';
	ilFrom += atoi(pclTmp);


	strncpy(pclTmp,&pcpTo[ilTimeStart],2);
	pclTmp[2] = '\0';
	ilTo = atoi(pclTmp) * 60;
	strncpy(pclTmp,&pcpTo[ilTimeStart+2],2);
	pclTmp[2] = '\0';
	ilTo += atoi(pclTmp);

	if (ilTo < ilFrom)
	{
		ilTo += 1440;
	}

	if ((ilFrom >= 0 && ilFrom < MAXMINUTES) &&
		(ilFrom < ilTo) && (ilTo > 0 && ilTo < MAXMINUTES))
	{
		for(ilLc = ilFrom; ilLc <= ilTo; ilLc++)
		{
			cgJobs[ilLc] = cpJobNo;
		}
	}
	else
	{
		ilRc = RC_FAIL;
	}

	return ilRc;
}
*/

static int CreateTmpAbsJob(char *pcpDrrRow,char *pcpPoolJobUrno)
{
	int	ilRc = RC_SUCCESS;
	int	ilTmpRc = RC_SUCCESS;
	int ilNo = 0;
	long llFunc = ARR_FIRST;
	long llDraFunc = ARR_FIRST;
	char    *pclPoolJob = NULL;
	char 	 	clOldJobUrno[24];
	char 	 	clShiftStart[24];
	char 	 	clAbfr[24];
	char 	 	clAbto[24];
	char 	 	clDrrUrno[24];
	char 	 	clKey[124] = "";
	char 	 	clDraKey[124] = "";
	char  	*pclRow = NULL;
	char  	*pclOldJobRow = NULL;
	/* unsigned long tlDraFrom,tlDraTo,tlJobFrom,tlJobTo; */
	char	*pclJobAcfr, *pclJobActo;


	dbg(TRACE,"CreateTmpAbsJob: DRRN <%s> SDAY <%s> STFU <%s>",
		DRRFIELD(pcpDrrRow,igDrrDRRN),
		DRRFIELD(pcpDrrRow,igDrrSDAY),
		DRRFIELD(pcpDrrRow,igDrrSTFU));

	/* find pool job */
	llFunc = ARR_FIRST;
	ilRc = CEDAArrayFindRowPointer (&(rgJobArray.rrArrayHandle),
									&(rgJobArray.crArrayName[0]),
									&(rgJobArray.rrIdx02Handle), 
									&(rgJobArray.crIdx02Name[0]), 
									pcpPoolJobUrno, &llFunc, (void*)&pclPoolJob);
	if (ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"Found Pooljob for DRA from <%s> to <%s>", 
		JOBFIELD(pclPoolJob,igJobACFR),
		JOBFIELD(pclPoolJob,igJobACTO));

		sprintf(clDraKey,"%s,%s,%s",
		DRRFIELD(pcpDrrRow,igDrrDRRN),
		DRRFIELD(pcpDrrRow,igDrrSDAY),
		DRRFIELD(pcpDrrRow,igDrrSTFU));
		dbg(TRACE,"%05d:Key=<%s>",__LINE__,clDraKey);

		llFunc = ARR_FIRST;
		llDraFunc = ARR_FIRST;
		dbg(TRACE,"DraFunc=%d, No=%d",llDraFunc,ilNo);
		while((ilTmpRc = CEDAArrayFindRowPointer(&(rgDraArray.rrArrayHandle),
												 &(rgDraArray.crArrayName[0]),
												 &(rgDraArray.rrIdx01Handle),
												 &(rgDraArray.crIdx01Name[0]),
												 clDraKey,&llDraFunc,
												(void *) &pclRow )) == RC_SUCCESS)
		{
			/* check for overlapping pooljob */
			
			dbg(TRACE,"Result from FindRowPointer %d",ilTmpRc);
			llDraFunc = ARR_NEXT;

			strcpy(clAbfr,FIELDVAL(rgDraArray,pclRow,igDraABFR));
			LocalToUtc(clAbfr);
			strcpy(clAbto,FIELDVAL(rgDraArray,pclRow,igDraABTO));
			LocalToUtc(clAbto);

			pclJobAcfr = JOBFIELD(pclPoolJob,igJobACFR);
			pclJobActo = JOBFIELD(pclPoolJob,igJobACTO);
			/*
			tlDraFrom = atol(clAbfr);
			tlDraTo   = atol(clAbto);

			tlJobFrom = atol(JOBFIELD(pclPoolJob,igJobACFR));
			tlJobTo   = atol(JOBFIELD(pclPoolJob,igJobACTO));
			
			dbg(TRACE,"%05d:Found DRA from <%s> to <%s>",__LINE__,
				FIELDVAL(rgDraArray,pclRow,igDraABFR),
				FIELDVAL(rgDraArray,pclRow,igDraABTO));
			dbg(TRACE,"Found DRA <%s> from %ld to %ld DRRN=<%s>, SDAY=<%s>,STFU=<%s>",
						FIELDVAL(rgDraArray,pclRow,igDraURNO), tlDraFrom,tlDraTo,
						FIELDVAL(rgDraArray,pclRow,igDraDRRN),
						FIELDVAL(rgDraArray,pclRow,igDraSDAY),
						FIELDVAL(rgDraArray,pclRow,igDraSTFU));
			*/
			dbg(TRACE,"Found DRA <%s> from <%s> to <%s> DRRN=<%s>, SDAY=<%s>,STFU=<%s>",
						FIELDVAL(rgDraArray,pclRow,igDraURNO), clAbfr,clAbto,
						FIELDVAL(rgDraArray,pclRow,igDraDRRN),
						FIELDVAL(rgDraArray,pclRow,igDraSDAY),
						FIELDVAL(rgDraArray,pclRow,igDraSTFU));

			/*if (tlDraFrom <  tlJobTo && tlDraTo > tlJobFrom)*/
			if ( ( strcmp(clAbfr,pclJobActo) < 0 ) && ( strcmp(clAbto,pclJobAcfr) > 0 ) )
			{
				dbg(TRACE,"matching DRA found ");
				/* check for existing job */
				llFunc = ARR_FIRST;
				strcpy(clKey,FIELDVAL(rgDraArray,pclRow,igDraURNO));
				TrimRight(clKey);
				ilRc = CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),
												&(rgJobArray.crArrayName[0]),
												&(rgJobArray.rrIdx01Handle),
												&(rgJobArray.crIdx01Name[0]),
												clKey,&llFunc, (void *) &pclOldJobRow ) ;         

				if (ilRc == RC_SUCCESS)
				{
					dbg(TRACE,"Temp. absent Job (for DRR) exists already");
					strcpy(clOldJobUrno,JOBFIELD(pclOldJobRow,igJobURNO));
				}
				else
				{
					*clOldJobUrno = '\0';
					dbg(TRACE,"Temp. absent Job for DRA.URNO <%s> not found",clKey);
				}
				FillAbsJob ( cgTmpBuf, clAbfr, clAbto, FIELDVAL(rgDraArray,pclRow,igDraURNO),
							 "",  pclPoolJob );
				if(*clOldJobUrno)
				{
					UpdatePoolJob(clOldJobUrno,cgTmpBuf);
				}
				else
				{
					long llRowNum = ARR_FIRST;
					ilRc=	CEDAArrayAddRow(&rgJobArray.rrArrayHandle,
					rgJobArray.crArrayName,&llRowNum,
						(void *)cgTmpBuf);
					dbg(TRACE,"New temporary absent job (for DRR) Urno=<%s>",
						JOBFIELD(cgTmpBuf,igJobURNO));
				}
			}

			llDraFunc = ARR_NEXT;
			ilNo++;
			dbg(TRACE,"DraFunc=%d, No=%d",llDraFunc,ilNo);
		}
		dbg(TRACE,"Result from FindRowPointer after loop  %d",ilTmpRc);
	}
	if ( (ilRc == RC_SUCCESS) && (igDelSDAC > 0) )
	{
		llDraFunc = ARR_FIRST;
		strcpy ( clDraKey, DRRFIELD(pcpDrrRow,igDrrBSDU));
		dbg(TRACE,"Search for temp. Absence in BSD <%s>",clDraKey);

		strncpy ( clShiftStart, DRRFIELD(pcpDrrRow,igDrrAVFR), 14 );
		clShiftStart[14] = '\0';
		
		strcpy ( clDrrUrno, DRRFIELD(pcpDrrRow,igDrrURNO) );
		TrimRight ( clDrrUrno );

		while( CEDAArrayFindRowPointer( &(rgDelArray.rrArrayHandle),
										&(rgDelArray.crArrayName[0]),
										&(rgDelArray.rrIdx01Handle),
										&(rgDelArray.crIdx01Name[0]), clDraKey,
										&llDraFunc, (void*)&pclRow ) == RC_SUCCESS )
		{
			char clDelSdac[21];
			char clDelUrno[21];
			char clJobUdel[21];
			char clJobUdsr[21];

			llDraFunc = ARR_NEXT;
			memset ( clDelSdac, 0, sizeof(clDelSdac) );
			strcpy ( clDelSdac, FIELDVAL(rgDelArray,pclRow,igDelSDAC) );
			TrimRight ( clDelSdac );

			if ( ( strlen ( clDelSdac ) >0 ) && strcmp(clDelSdac, " " ) )
			{
				dbg ( TRACE,"CreateTmpAbsJob: Found Temp. Absence <%s> in basic shift <%s>",
					  clDelSdac, clDraKey );
				strcpy ( clDelUrno, FIELDVAL(rgDelArray,pclRow,igDelURNO) );
				TrimRight ( clDelUrno );
				strcpy ( clAbfr, cgActDay );
				strncpy ( &(clAbfr[8]), FIELDVAL(rgDelArray,pclRow,igDelDELF), 4 );
				strcpy ( &(clAbfr[12]), "00" );
				strcpy ( clAbto, cgActDay );
				strncpy ( &(clAbto[8]), FIELDVAL(rgDelArray,pclRow,igDelDELT), 4 );
				strcpy ( &(clAbto[12]), "00" );
				clAbfr[14] = clAbto[14] = '\0';
				if ( strcmp ( clAbfr, clAbto ) > 0 )
				{
					AddOneDayLocal ( clAbto, clAbto );
					dbg ( DEBUG, "CreateTmpAbsJob: Added one day to clTo" );
				}
				if ( strcmp ( clAbfr, clShiftStart ) < 0 )
				{
					dbg ( TRACE, "CreateTmpAbsJob: Absence outside shift, 1 day added" );
					AddOneDayLocal ( clAbfr, clAbfr );
					AddOneDayLocal ( clAbto, clAbto );
				}

				LocalToUtc ( clAbfr );
				LocalToUtc ( clAbto );
				/* tlDraFrom = atol(clAbfr);
				tlDraTo   = atol(clAbto);

				tlJobFrom = atol(JOBFILD(pclPoolJob,igJobACFR));
				tlJobTo   = atol(JOBFIELD(pclPoolJob,igJobACTO));

				if (tlDraFrom <  tlJobTo && tlDraTo > tlJobFrom)*/
				pclJobAcfr = JOBFIELD(pclPoolJob,igJobACFR);
				pclJobActo = JOBFIELD(pclPoolJob,igJobACTO);
				if ( ( strcmp(clAbfr,pclJobActo) < 0 ) && ( strcmp(clAbto,pclJobAcfr) > 0 ) )
				{
					dbg ( TRACE,"CreateTmpAbsJob: Temp. Absence from <%s> to <%s>",
						  clAbfr, clAbto );
					sprintf ( clKey, "%s,%s", cgUjtyAbs, DRRFIELD(pcpDrrRow,igDrrSTFU) );
					*clOldJobUrno = '\0';
					dbg ( TRACE,"CreateTmpAbsJob: Search for temp. Absence Job with key <%s>",
						  clKey );
					llFunc  = ARR_FIRST;
					/* looking for existing temp. absence job */
					while ( !clOldJobUrno[0] && 
							( CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),
													   &(rgJobArray.crArrayName[0]),
													   &(rgJobArray.rrIdx03Handle),
													   &(rgJobArray.crIdx03Name[0]),
													   clKey, &llFunc, 
													   (void*) &pclOldJobRow ) ==RC_SUCCESS ) )
					{
						dbg ( DEBUG, "CreateTmpAbsJob: Found Abscence Job URNO <%s> UDSR <%s> UDEL <%s>",
							  JOBFIELD(pclOldJobRow,igJobURNO), JOBFIELD(pclOldJobRow,igJobUDSR),
							  JOBFIELD(pclOldJobRow,igJobUDEL) );
						strcpy ( clJobUdel, JOBFIELD(pclOldJobRow,igJobUDEL));
						TrimRight ( clJobUdel );
						strcpy ( clJobUdsr, JOBFIELD(pclOldJobRow,igJobUDSR));
						TrimRight ( clJobUdsr );
						dbg ( DEBUG, "CreateTmpAbsJob: Comparing JobUdel <%s> DelUrno <%s>, JobUdsr<%s> DrrUrno<%s>",
							  clJobUdel, clDelUrno, clJobUdsr, clDrrUrno ) ;
						if ( !strcmp ( clJobUdel, clDelUrno ) && !strcmp ( clJobUdsr, clDrrUrno ) )
						{
							dbg(TRACE,"CreateTmpAbsJob: Temp. absent Job (for BSD) exists already");
							strcpy(clOldJobUrno,JOBFIELD(pclOldJobRow,igJobURNO) );
						}
						llFunc = ARR_NEXT;
					}

					FillAbsJob ( cgTmpBuf, clAbfr, clAbto, DRRFIELD(pcpDrrRow,igDrrURNO),
								 clDelUrno,  pclPoolJob );
					if(*clOldJobUrno)
					{
						UpdatePoolJob(clOldJobUrno,cgTmpBuf);
					}
					else
					{
						long llRowNum = ARR_FIRST;
						ilRc=	CEDAArrayAddRow(&rgJobArray.rrArrayHandle,
						rgJobArray.crArrayName,&llRowNum,
							(void *)cgTmpBuf);
						dbg(TRACE,"New temporary absent job (for BSD) Urno=<%s>",
							JOBFIELD(cgTmpBuf,igJobURNO));
					}
				}
				else
					dbg ( TRACE,"CreateTmpAbsJob: Temp. Absence from <%s> to <%s> doesn't overlap pooljob <%s>",
						  clAbfr, clAbto, JOBFIELD(pclPoolJob,igJobURNO) );
			}
		}
		dbg(TRACE,"CreateTmpAbsJob: after DEL search loop");
	}
	return(ilRc);
}



static int CreatePoolJobsForShift(char *pcpDrrRow)
{
	int ilRc = RC_SUCCESS;
	long llFunc = ARR_FIRST;
	char *pclRow;
	int ilJobNo, ilLen;
	char clKey[124];
	char clDay[15], clUrno[21];

	igDelDraFirstFree = 0;

  dbg ( TRACE, "CreatePoolJobsForShift: begin");
  
	ilRc = CEDAArrayDelete( &(rgNewJobsArray.rrArrayHandle), 
							rgNewJobsArray.crArrayName );
	if ( ilRc != RC_SUCCESS )
	{
		dbg ( TRACE, "CreatePoolJobsForShift: CEDAArrayDelete failed RC <%d>", ilRc ); 
		return ilRc;
	}

	ilRc = CheckShift(pcpDrrRow,clKey);

	if (ilRc == RC_SUCCESS)
	{
		/* initialize cgJobs Array */
		memset(cgJobs,-1,sizeof(cgJobs));
		strncpy ( cgActDay, DRRFIELD(pcpDrrRow,igDrrAVFR), 8 );
		cgActDay [8] = '\0';
		strcat ( cgActDay, "000000" );
		
		strcpy ( clDay, cgActDay );
		LocalToUtc ( clDay );
		StrToTime ( clDay, &lgStartofDayUTC );

		/* clear rgDelDrr Array */
		memset(rgDelDrr,0,sizeof(rgDelDrr));
		igDelDraFirstFree = 0;

		/* fill cgJobs with pooljob from DRRTAB */
		PutJob(DRRFIELD(pcpDrrRow,igDrrAVFR),DRRFIELD(pcpDrrRow,igDrrAVTO),(char)igDelDraFirstFree);
		igDelDraFirstFree++;
		/*sprintf(clKey,"%s",
			DRRFIELD(pcpDrrRow,igDrrBSDU)); Key is created in CheckShift*/
		dbg(TRACE,"CreatePoolJobsForShift: Search for DEL Key = <%s> pigDelKey = %p",clKey,pigDelKey);
		if (pigDelKey != NULL)
		{
			while((ilRc = CEDAArrayFindRowPointer(&(rgDelArray.rrArrayHandle),
																				&(rgDelArray.crArrayName[0]),
																				&(rgDelArray.rrIdx01Handle),
																				&(rgDelArray.crIdx01Name[0]),
																				clKey,&llFunc,
																				(void *) &pclRow )) == RC_SUCCESS)
			{
				char *pclFrom,*pclTo;
				char clDelSdac[21];

				memset ( clDelSdac, 0, sizeof(clDelSdac) );
				if ( igDelSDAC > 0 )
				{
					strcpy ( clDelSdac, FIELDVAL(rgDelArray,pclRow,igDelSDAC) );
					TrimRight ( clDelSdac );
				}

				if ( ( strlen ( clDelSdac ) >0 ) && strcmp(clDelSdac, " " ) )
				{
					dbg ( TRACE,"CreatePoolJobsForShift: Found Temp. Absence <%s> in basic shift <%s>",
						  clDelSdac, clKey );
				}
				else
				{
					dbg(TRACE,"CreatePoolJobsForShift: Del found: %d <%s>",igDelDraFirstFree,pclRow);

					rgDelDrr[igDelDraFirstFree].ilType = DRDDEL;
					strcpy(rgDelDrr[igDelDraFirstFree].clUrno,
						FIELDVAL(rgDelArray,pclRow,igDelURNO));	
					strcpy(rgDelDrr[igDelDraFirstFree].clKey,
						FIELDVAL(rgDelArray,pclRow,*pigDelKey));	
					pclFrom = FIELDVAL(rgDelArray,pclRow,igDelDELF);
					pclTo   = FIELDVAL(rgDelArray,pclRow,igDelDELT);
					if (strcmp(pclFrom,pclTo))
					{
						PutJob(pclFrom,pclTo,(char)igDelDraFirstFree);
						igDelDraFirstFree++;
					}
				}
				llFunc = ARR_NEXT;
			}
			dbg(TRACE,"CreatePoolJobsForShift: after DEL search loop");
		}
		if(pigDrdKey != NULL)
		{
			sprintf(clKey,"%s,%s,%s",
			DRRFIELD(pcpDrrRow,igDrrDRRN),
			DRRFIELD(pcpDrrRow,igDrrSDAY),
			DRRFIELD(pcpDrrRow,igDrrSTFU));
			dbg(TRACE,"%05d:Key=<%s>",__LINE__,clKey);
			llFunc = ARR_FIRST;
			while((ilRc = CEDAArrayFindRowPointer(&(rgDrdArray.rrArrayHandle),
																				&(rgDrdArray.crArrayName[0]),
																				&(rgDrdArray.rrIdx01Handle),
																				&(rgDrdArray.crIdx01Name[0]),
																				clKey,&llFunc,
																				(void *) &pclRow )) == RC_SUCCESS)
			{
				llFunc = ARR_NEXT;
				dbg(TRACE,"CreatePoolJobsForShift: Drd found: ilRc=%d <%s>",ilRc,pclRow);
				dbg(TRACE,"CreatePoolJobsForShift: DRRN=<%s>,SDAY=<%s>,STFU=<%s>",
					FIELDVAL(rgDrdArray,pclRow,igDrdDRRN),
					FIELDVAL(rgDrdArray,pclRow,igDrdSDAY),
					FIELDVAL(rgDrdArray,pclRow,igDrdSTFU));

				rgDelDrr[igDelDraFirstFree].ilType = DRDDRD;
				strcpy(rgDelDrr[igDelDraFirstFree].clUrno,
					FIELDVAL(rgDrdArray,pclRow,igDrdURNO));	
				strcpy(rgDelDrr[igDelDraFirstFree].clKey,FIELDVAL(rgDrdArray,pclRow,*pigDrdKey));	
				dbg(TRACE,"Drd %d found: <%s>,Key=<%s>",igDelDraFirstFree,pclRow,
					rgDelDrr[igDelDraFirstFree].clKey);
				PutJob(FIELDVAL(rgDrdArray,pclRow,igDrdDRDF),
							FIELDVAL(rgDrdArray,pclRow,igDrdDRDT),(char)igDelDraFirstFree);
				igDelDraFirstFree++;
			}

		}
		dbg(TRACE,"CreatePoolJobsForShift: %d Deviations found",igDelDraFirstFree-1);
	
		for (ilJobNo = 0; ilJobNo < igDelDraFirstFree; ilJobNo++)
		{
			CreatePoolJob(pcpDrrRow,ilJobNo);
		}

		strncpy(clKey,DRRFIELD(pcpDrrRow,igDrrURNO),10);
		clKey[10] = '\0';

		ilRc = UpdatePoolJobs ( pcpDrrRow );
		if ( ilRc != RC_SUCCESS )
			dbg(TRACE,"CreatePoolJobsForShift: UpdatePoolJobs for DRR-URNO <%s> failed RC=%d",
								clKey, ilRc );
		ilRc = CopyNewPoolJobs ( pcpDrrRow );
		if ( ilRc != RC_SUCCESS )
			dbg(TRACE,"CreatePoolJobsForShift: CopyNewPoolJobs for DRR-URNO <%s> failed RC=%d",
								clKey, ilRc );
		cgTmpBuf[0] = '\0';
		llFunc = ARR_FIRST;
 		while ( CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),
										&(rgJobArray.crArrayName[0]),
										&(rgJobArray.rrIdx01Handle),
										&(rgJobArray.crIdx01Name[0]),
										clKey, &llFunc, (void *) &pclRow ) == RC_SUCCESS )         
		{	/*  collect URNOs of all pooljobs */
			ilLen = strlen ( cgTmpBuf );
			sprintf( &(cgTmpBuf[ilLen]),"%s,", JOBFIELD(pclRow,igJobURNO));
			llFunc = ARR_NEXT;
		}
		ilLen = strlen ( cgTmpBuf );
		/*if ( cgTmpBuf[ilLen-1] == ',' )
			cgTmpBuf[ilLen-1] = '\0';*/	/* remove last comma */

		dbg ( TRACE, "CreatePoolJobsForShift: URNO-list for abscence-calculation <%s>",
			  cgTmpBuf );	
		ilLen = 1;
		while ( GetDataItem ( clUrno, cgTmpBuf, ilLen, ',', "", "  ") >0 )
		{	
			dbg ( DEBUG, "CreatePoolJobsForShift: call CreateTmpAbsJob for job-URNO <%s>",
				  clUrno );
			CreateTmpAbsJob(pcpDrrRow,clUrno);
			ilLen++;
		}
	}
	else
	{
		ilRc = CheckAbsence(pcpDrrRow);
		if(ilRc == RC_SUCCESS)
		{
			ilRc = DeletePoolJob(pcpDrrRow);
		}
	}
	dbg(TRACE,"CreatePoolJobsForShift finished");
	return ilRc;
}


static int SavePoolJobs( BOOL bpSendSBC, char *pcpStart, char *pcpEnd )
{
	int		ilRc = RC_SUCCESS;
	long	llRowNum = ARR_FIRST;
	char	*pclJobRow = NULL, *pclBuffer=0, *pclOut=0;
	int		ilTouched = 0, ilBytes, i, j;
	char	clDel[2]="\262" /* mapped comma */;
	char	clDel2[2]="|" /* separates MAXINSEL count of URNOs */;
	char	*pclNow=0, *pclNext=0;
	char	clTwEnd[33], clLists[11], clUstf[16], clSelection[31] = "\0";
	int 	ilItemNo, ilCol, ilPos, ilAdded=0;
	LIST_DESC *prlList;
	BOOL    blAttachmentSent = FALSE;

	if ( pcpStart && pcpEnd )
		dbg ( TRACE, "SavePoolJobs: SendSBC <%d> Start <%s> End <%s>", bpSendSBC, pcpStart, pcpEnd );
	else
	{
		dbg ( TRACE, "SavePoolJobs: SendSBC <%d> Start and/or End is null -> Set SendSBC to FALSE", bpSendSBC );
		bpSendSBC = FALSE;
	}
	while (CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),
					&(rgJobArray.crArrayName[0]),
					&(rgJobArray.rrIdx01Handle),
					&(rgJobArray.crIdx01Name[0]),
					"",&llRowNum,
					(void *) &pclJobRow )  == RC_SUCCESS)

	{
		CEDAArrayPutField(&rgJobArray.rrArrayHandle,
						  rgJobArray.crArrayName, NULL,"XXXX",llRowNum,"0");
		llRowNum = ARR_NEXT;
	}

	if ( prgJobInf )
		prgJobInf->AppendData = FALSE;
	ilRc = AATArraySaveChanges(&rgJobArray.rrArrayHandle, rgJobArray.crArrayName, 
								&prgJobInf, ARR_COMMIT_ALL_OK);
	dbg ( TRACE, "SavePoolJobs: AATArraySaveChanges <JOBTAB> RC <%d>", ilRc );
	if ( ( ilRc == RC_SUCCESS ) && bpSendSBC && prgJobInf )
	{	
		ilTouched = prgJobInf->DataList[IDX_IRTD].ValueCount + 
					prgJobInf->DataList[IDX_URTD].ValueCount +
					prgJobInf->DataList[IDX_DRTD].ValueCount; 
		dbg ( TRACE, "SavePoolJobs: %d modified pooljobs to save", ilTouched );

	}
	if ( ilTouched > 0 )		/* something has been changed and SBC shall be sent */
	{
		ilBytes = MAXINSEL * 11 + 60;
		pclOut = malloc ( ilBytes );
		ilBytes = ilTouched * 11 + 10;
		pclBuffer = malloc ( ilBytes );
		if ( !pclBuffer || !pclOut )
		{
			dbg ( TRACE, "SavePoolJobs: unable to allocate <%d> bytes", ilBytes );
			ilRc = RC_NOMEM;
		}
		else
		{	/* build old style SBC header */
			pclBuffer[0] = '\0';
			for ( i=0; i<3; i++)
			{
				ilAdded = 0;
				prlList = &(prgJobInf->DataList[rgDIdx[i]]);
				dbg(DEBUG,"-------------------------------------------------");
				if ( prlList->ValueCount>0 )
				{
					if ( FindItemInList( prlList->ListHead,"USTF", 
										 ',', &ilItemNo,&ilCol,&ilPos) == RC_SUCCESS )
					{
						dbg ( DEBUG, "SavePoolJobs: Index of USTF in <%s> is <%d>", prlList->ListHead, ilItemNo );
						for ( j=1; j<=prlList->ValueCount; j++ )
						{
							if( GetDataItem(cgNewJobBuf,prlList->ValueList,j,
											prlList->ValueSepa[0],"","\0\0 ") >= 0)
							{	
								dbg ( DEBUG, "Rec. No. %d <%s>", j, cgNewJobBuf );
								get_real_item(clUstf,cgNewJobBuf,ilItemNo);
								TrimRight(clUstf);
								dbg ( DEBUG, "SavePoolJobs: Found USTF <%s>", clUstf );
								if ( ( atol(clUstf) > 0) && !strstr(pclBuffer,clUstf ) ) 
								{
									if ( *pclBuffer )
									{
										if ( ilAdded % MAXINSEL == 0 )
											strcat ( pclBuffer, clDel2 );
										else
											strcat ( pclBuffer, clDel );
									}
									strcat ( pclBuffer, clUstf );
									ilAdded++;
								}
							}
						}
						dbg(DEBUG,"SavePoolJobs: Added <%d> USTF from LIST <%s> RECCOUNT=%d",ilAdded, 
								   prlList->ListName, prlList->ValueCount);
						if (prlList->UsedSize > 0)
						{
							dbg(DEBUG,"FIELDS <%s>",prlList->ListHead );
							dbg(DEBUG,"LIST CONTENT (%d BYTES):\n%s>",prlList->UsedSize, prlList->ValueList);
						}
					}
					else
						dbg ( TRACE, "SavePoolJobs: USTF not in list <%s> fields <%s>", 
							  prlList->ListName, prlList->ListHead ); 	
				}
			}
			dbg(DEBUG,"-------------------------------------------------");
			dbg ( TRACE, "SavePoolJobs: USTF-Buffer <%s>", pclBuffer );
		}
	}
   	if ( bgBreak )
	{
		if ( prgJobInf )
			prgJobInf->AppendData = TRUE;
		ilRc = AATArraySaveChanges( &rgBrkArray.rrArrayHandle, rgBrkArray.crArrayName, 
									&prgJobInf, ARR_COMMIT_ALL_OK);
		dbg ( TRACE, "SavePoolJobs: AATArraySaveChanges <BREAK> RC <%d>", ilRc );
	}
	
	if ( ilTouched > 0 )  /* something has been changed and SBC shall be sent */
	{
		ilRc = RC_SUCCESS;
		sprintf (clTwEnd, "%s,%s,%s", cgHopo,cgTabEnd, cgProcessName);
		sprintf ( clSelection, "%s,%s", cgUjty, cgUjtyBrk );
		sprintf ( clLists, "%d,%d,%d", IDX_IRTD, IDX_URTD, IDX_DRTU );
			
		if ( pclBuffer )
			pclNow=pclBuffer;
		do 
		{
			if ( pclNow )
				pclNext=strstr(pclNow,clDel2);
			if (pclNext)
			{
				*pclNext = '\0';
				pclNext ++;
			}
			if ( pclNow )
			{
				sprintf ( pclOut, "%s-%s-%s", pcpStart, pcpEnd, pclNow );
				if ( bgRELJOBtoACTION )
				{
    				tools_send_info_flag( 7400, mod_id, "ACTION", cgProcessName, "", "", "", 
										  "SBC,-1,-1", clTwEnd, "SBC","RELJOB", 
										   clSelection, "EOT,RM", pclOut, 0 );
					dbg ( DEBUG, "SavePoolJobs: Sent SBC to action" );
				}
				if ( blAttachmentSent ) /* attachment has already been sent ? */
					strcat ( pclOut, "\n{=NOTCP=}" );
				dbg ( TRACE, "SavePoolJobs: SBC-Data <%s> AttachmentSent <%d>", pclOut,
					  blAttachmentSent );
				if ( bgRELJOBtoBCHDL )
					ilRc = AATArraySetSbcInfo(rgJobArray.crArrayName, prgJobInf, "1900", 
											  "3", "SBC", "RELJOB", "CEDA", cgProcessName, "SBC,-1,-1", 
											  clTwEnd, clSelection, "EOT,RM", pclOut );
			}
			else	/* only possible when malloc went wrong */	
			{
				if ( bgRELJOBtoBCHDL )
					ilRc = AATArraySetSbcInfo(rgJobArray.crArrayName, prgJobInf, "1900", 
											  "3", "SBC", "RELJOB", "CEDA", cgProcessName, 
											  "SBC,-1,-1", clTwEnd, "", "", "{=NOUDP=}" );
			}
			if ( ilRc != RC_SUCCESS )
				dbg ( TRACE, "SavePoolJobs: AATArraySetSbcInfo failed RC <%d>", ilRc );
			else
			{
				if ( bgRELJOBtoBCHDL )
				{
					if ( blAttachmentSent )		/* sent attachment only once */
						ilRc = AATArrayCreateSbc(rgJobArray.crArrayName, prgJobInf, 0, "", FALSE);
					else
						ilRc = AATArrayCreateSbc(rgJobArray.crArrayName, prgJobInf, 0, clLists, FALSE);
					if ( ilRc != RC_SUCCESS )
						dbg ( TRACE, "SavePoolJobs: AATArrayCreateSbc failed RC <%d>", ilRc );
					else
						blAttachmentSent = TRUE;
				}

			}
			pclNow = pclNext;
		}while (pclNow);
	}

	if ( pclOut )
		free ( pclOut );
	if ( pclBuffer )
		free ( pclBuffer );

	dbg(TRACE,"SavePoolJobs RC=%d",ilRc);

	return ilRc;
}




static int StrToTime(char *pcpTime,time_t *plpTime)
{
	struct tm *_tm;
	time_t now;
	char   _tmpc[6];
	long llUtcDiff;
	BOOL blDebugInfo = FALSE;

	if ( blDebugInfo )
		dbg(DEBUG,"StrToTime, <%s>",pcpTime); 
	if (strlen(pcpTime) < 12 )
	{
		*plpTime = time(0L);
		return RC_FAIL;
	} /* end if */




	now = time(0L);
	_tm = (struct tm *)gmtime(&now);

	_tmpc[2] = '\0';
/*
	strncpy(_tmpc,pcpTime+12,2);
	_tm -> tm_sec = atoi(_tmpc);
*/
	_tm -> tm_sec = 0;
	
	strncpy(_tmpc,pcpTime+10,2);
	_tm -> tm_min = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+8,2);
	_tm -> tm_hour = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+6,2);
	_tm -> tm_mday = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+4,2);
	_tm -> tm_mon = atoi(_tmpc)-1;
	strncpy(_tmpc,pcpTime,4);
	_tmpc[4] = '\0';
	_tm ->tm_year = atoi(_tmpc)-1900;
	_tm ->tm_wday = 0;
	_tm ->tm_yday = 0;
	_tm->tm_isdst = 0;  /*-1; no adjustment of daylight saving time */
	now = mktime(_tm);
	if ( blDebugInfo )
		dbg(DEBUG,"StrToTime: %02d.%02d.%04d %02d:%02d Now: %ld",_tm->tm_mday,
				  _tm->tm_mon+1,_tm->tm_year+1900,_tm->tm_hour,_tm->tm_min,now);
	if (now != (time_t) -1)
	{
		{
			time_t utc,local;
		
			_tm = (struct tm *)gmtime(&now);
			utc = mktime(_tm);
			_tm = (struct tm *)localtime(&now);
			local = mktime(_tm);
			if ( blDebugInfo )
				dbg(DEBUG,"utc %ld, local %ld, utc-diff %ld", utc,local,local-utc);
			llUtcDiff = local-utc;
		}
		now +=  + llUtcDiff;
		*plpTime = now;
		return RC_SUCCESS;
	}
	*plpTime = time(NULL);
	return RC_FAIL;
}

static int TimeToStr(char *pcpTime,time_t lpTime) 
{                                                                               
	struct tm *_tm;
	
	_tm = (struct tm *)gmtime(&lpTime);
	/* strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",_tm); */

	/*************/
	sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
			_tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
			_tm->tm_min,_tm->tm_sec);
			/*************/
	return (_tm->tm_wday == 0 ? 7 : _tm->tm_wday) + '0';                          
}                                                                               

/**
dbg(TRACE,"%05d:",__LINE__);
***/



/*
 *     struct  tm {
 *             int     tm_sec;    seconds after the minute -- [0, 59]  
 *             int     tm_min;    minutes after the hour -- [0, 59] 
 *             int     tm_hour;   hour since midnight -- [0, 23] 
 *             int     tm_mday;   day of the month -- [1, 31] 
 *             int     tm_mon;    months since January -- [0, 11] 
 *             int     tm_year;   years since 1900 
 *             int     tm_wday;   days since Sunday -- [0, 6] 
 *             int     tm_yday;   days since January 1 -- [0, 365] 
 *             int     tm_isdst;  flag for daylight savings time 
 *             long    tm_tzadj;  seconds from GMT (east < 0) 
 *             char    tm_name[LTZNMAX];   name of timezone 
 *     };
 */




void	StrAddTime(char *pcpBase,char *pcpOffs,char *pcpDest, short spLocal )
{

	if (pcpBase != pcpDest)
	{
		strcpy(pcpDest,pcpBase);
	}
	AddSecsToCEDATime(pcpDest,(time_t)atol(pcpOffs), spLocal);

} /* end of StrAddTime */


static int LocalToUtc(char *pcpTime)
{
	int ilRc = RC_SUCCESS;
	char *pclTdi;
	char clLocal[32];

	strcpy(clLocal,pcpTime);
	pclTdi = (strcmp(pcpTime,cgTich) < 0) ? cgTdi1 : cgTdi2;
	AddSecsToCEDATime(pcpTime,(time_t)0-atol(pclTdi),0);
	dbg(TRACE,"2UTC  : Local <%s> UTC <%s> ", clLocal, pcpTime);

	return ilRc;
}


static int ReCalculateShifts()
{

	int	 ilFound, ilRc = 0;
	long llRowNum = ARR_FIRST;
	char *pclDrrRow = NULL;
	/*long llStfRowNum;
	char *pclStfRow;
	char clKey[20], clNEWJ[11];
	int	 ilStfuCount = 0, ilBytes; */
	char clMinAvfr[15]="";
	char clMaxAvto[15]="";
	/*char *pclBuffer=0;*/
	/*int  ilFinished = 0;
	int  ilNEWJIdx = igStfURNO + 1;
	char clTwEnd[21] = "\0";
	char clSelection[31] = "\0";
	char	clDel='\262' // mapped comma */;
	
	
	dbg(TRACE,"ReCalculateShifts");
	while(CEDAArrayGetRowPointer(&rgDrrArray.rrArrayHandle,
				rgDrrArray.crArrayName,llRowNum,(void *)&pclDrrRow) == RC_SUCCESS)
	{
		llRowNum = ARR_NEXT;
		ilRc = CheckExistingJob(pclDrrRow);
		if ( ( ilRc == RC_JOB_TO_CREATE ) || (ilRc ==RC_JOB_DELETED) )
		{
			if ( ilRc == RC_JOB_TO_CREATE ) 
			{
				dbg(TRACE,"Job not found");
				CreatePoolJobsForShift(pclDrrRow);
			}
			else
				dbg(TRACE,"Job deleted");
			/*
			strcpy ( clKey, DRRFIELD(pclDrrRow,igDrrSTFU) );
			TrimRight ( clKey );
			llStfRowNum = ARR_FIRST;
			ilFound = CEDAArrayFindRowPointer( &(rgStfArray.rrArrayHandle),
											   rgStfArray.crArrayName,
											   &(rgStfArray.rrIdx02Handle),
											   rgStfArray.crIdx02Name, clKey,
											   &llStfRowNum, (void *)&pclStfRow );
			if ( ilFound == RC_SUCCESS)
			{
				strcpy ( clNEWJ, STFFIELD(pclStfRow, ilNEWJIdx) );
				if ( clNEWJ[0] == 'X' )
				{
					dbg ( DEBUG, "ReCalculateShifts: STFU <%s> already marked", clKey );
				}
				else
				{
					CEDAArrayPutField( &(rgStfArray.rrArrayHandle), rgStfArray.crArrayName, 
									   NULL,"NEWJ",llStfRowNum,"X");
					ilStfuCount ++;
					dbg ( DEBUG, "ReCalculateShifts: %d. STFU <%s> marked for BC", ilStfuCount, clKey );
				}
			}
			else	
				dbg ( TRACE, "ReCalculateShifts: Unable to find STFU <%s> RC <%d>", clKey, ilFound );
			*/
			/*  better take SDAY because AVFR, AVTO can be empty, if empl. is absent  
			if ( !clMinAvfr[0] || 
				 ( strcmp( DRRFIELD(pclDrrRow,igDrrAVFR), clMinAvfr ) < 0 )
			   )
				strcpy ( clMinAvfr, DRRFIELD(pclDrrRow,igDrrAVFR) );
			if ( strcmp(clMaxAvto, DRRFIELD(pclDrrRow,igDrrAVTO) ) < 0 )
				strcpy ( clMaxAvto, DRRFIELD(pclDrrRow,igDrrAVTO) );*/

			if ( !clMinAvfr[0] || 
				 ( strcmp( DRRFIELD(pclDrrRow,igDrrSDAY), clMinAvfr ) < 0 )
			   )
				strcpy ( clMinAvfr, DRRFIELD(pclDrrRow,igDrrSDAY) );
			if ( strcmp(clMaxAvto, DRRFIELD(pclDrrRow,igDrrSDAY) ) < 0 )
				strcpy ( clMaxAvto, DRRFIELD(pclDrrRow,igDrrSDAY) );

		}
		/*CreateSinglePoolJob(pclDrrRow);*/
		/*if (ilRc++ > 1)*/
	  /* break;*/
	}

	/* better take SDAY because AVFR, AVTO can be empty, if empl. is absent -> add hours,min.,sec. */
	clMinAvfr[8] = '\0';
	clMaxAvto[8] = '\0';
	strcat ( clMinAvfr, "000000" );
	strcat ( clMaxAvto, "235959" );

	SavePoolJobs(bgRELJOBtoACTION || bgRELJOBtoBCHDL, clMinAvfr, clMaxAvto );
	return ilRc;
}



int check_ret1(int ipRc,int ipLine)
{
	int ilOldDebugLevel;

	ilOldDebugLevel = debug_level;

	debug_level = TRACE;

	dbg(TRACE,"DB-ERROR in Line: %d",ipLine);
	debug_level  = ilOldDebugLevel;
	return check_ret(ipRc);
}

static void TrimRight(char *pcpBuffer)
{
	char *pclBlank = &(pcpBuffer[strlen(pcpBuffer)-1]);
	while(isspace(*pclBlank) && pclBlank != pcpBuffer)
	{
		*pclBlank = '\0';
		pclBlank--;
	}
}

#if 0
static int SendBc(char *pcpComand,char *pcpTable,char *pcpSelection,
									char *pcpFields,char *pcpData,char *pcpWksName)
{
	int ilRc = RC_SUCCESS;
	int ilBcLen;
	int ilLength;
	BC_HEAD rlBcHead;
	CMDBLK rlCmdBlk;

	if (bgBcActiv == TRUE)
	{

		ilLength = strlen(pcpTable)+strlen(pcpSelection)+strlen(pcpFields);
		ilBcLen = ilLength + strlen(pcpData);
		{

		dbg(DEBUG,"%05d: SendBC: BcLen %d Table %d Selection %d Fields %d Data %d",
			__LINE__, ilBcLen,strlen(pcpTable),
			strlen(pcpSelection),strlen(pcpFields),strlen(pcpData));
		dbg(DEBUG,"%5d: SendBC: %s <%s>",__LINE__,
			pcpComand,pcpFields);
		dbg(DEBUG,"%5d: SendBC: %s <%s>",__LINE__,
			pcpComand,pcpData);
		/*************
		utBc(mod_id+1,pcpComand,pcpTable,pcpSelection,
				pcpFields,pcpData,pcpWksName); 
		**********/
				memset(&rlBcHead,0,sizeof(rlBcHead));
				memset(&rlCmdBlk,0,sizeof(rlCmdBlk));
				strcpy(rlBcHead.dest_name,"OPSSLSG");
				strcpy(rlBcHead.orig_name,"RESHDL");
				strcpy(rlBcHead.recv_name,"RECV");
				strcpy(rlBcHead.seq_id,pcpWksName);

				strcpy(rlCmdBlk.obj_name,pcpTable);
				strcpy(rlCmdBlk.command,pcpComand);
			

				MyNewToolsSendSql(mod_id+1,&rlBcHead,&rlCmdBlk,
				pcpSelection,pcpFields,pcpData);

		dbg(DEBUG,"%5d: SendBC: %d <%s>",__LINE__,
			mod_id+1,pcpData);
			}
	}
	return ilRc;
}

static int CreateBc(long lpNumUrnos,char *pcpUpdCmdLst,char *pcpUpdSelLst,
	char *pcpUpdFldLst,char *pcpUpdDatLst)
{
	int ilRc = RC_SUCCESS;
	char *pclCmd,*pclSel,*pclFld,*pclDat;

	dbg(TRACE,"%05d:CreateBC: Urnos %ld\nCmd: %s\nSel: %s\nFld: %s\nDat: %s",__LINE__,
			lpNumUrnos,pcpUpdCmdLst,pcpUpdSelLst,pcpUpdFldLst,pcpUpdDatLst);

		pclCmd = pcpUpdCmdLst;
		pclSel = pcpUpdSelLst;
		pclFld = pcpUpdFldLst;
		pclDat = pcpUpdDatLst;

		if (*pclCmd != '\0')
		{
			do
			{
				pclCmd = CopyNextField(pclCmd,'\n',cgCmdBuf);
				if (*cgCmdBuf != 0)
				{
				pclSel = CopyNextField(pclSel,'\n',cgSelBuf);
				if (*cgSelBuf != 0)
				{
				pclFld = CopyNextField(pclFld,'\n',cgFldBuf);
				if (*cgFldBuf != 0)
				{
				pclDat = CopyNextField(pclDat,'\n',cgDatBuf);
				if (*cgDatBuf != 0)
				{
						dbg(TRACE,"CreateBc: Cmd <%s> Sel <%s> Fld <%s> Dat <%s>",
							cgCmdBuf,cgSelBuf,cgFldBuf,cgDatBuf);
						SendBc(cgCmdBuf,"JOBTAB",cgSelBuf,cgFldBuf,cgDatBuf,"JOBHDL");
				}
				}
				}
				}
			} while (pclCmd != NULL && *cgCmdBuf != '\0');
		}

	return ilRc;
}

int MyNewToolsSendSql(int ipRouteID,
						BC_HEAD *prpBchd,
						CMDBLK *prpCmdblk,
						char *pcpSelection,
						char *pcpFields,
						char *pcpData)
{
  int 		ilRc = RC_SUCCESS;
  static int ilTotalLen=0;
	int		ilLength=0;
  BC_HEAD 	*prlBchd;		/* Broadcast header		*/
  CMDBLK  	*prlCmdBlk;		/* Command Block 		*/
  char 		*pclDataBlk;		/* Data Block			*/
  char		*pclSel;
  char		*pclField;
  static EVENT 	*prlOutEvent = NULL;

	dbg(DEBUG,"Entering new_tools_*. TotalLen<%d>  ilRc<%d> prlOutEvent<%ld>  DataLength<%i>",ilTotalLen,prpBchd->rc,prlOutEvent,strlen(pcpData));	
	if( prpBchd->rc!= DB_SUCCESS )
		/* if there was an error then the data area gets copied to
			the area BCDH->data */
  		ilLength = strlen(pcpFields) + strlen(pcpData) + strlen(pcpData) +
  		strlen(prpCmdblk->data) + sizeof(BC_HEAD)  + sizeof(CMDBLK) +
  		sizeof(EVENT) + 12; 
	else
  		ilLength = strlen(pcpFields) + strlen(pcpData) + strlen(pcpSelection) + 
    	sizeof(BC_HEAD)  + sizeof(CMDBLK) + sizeof(EVENT) + 12; 
	dbg(DEBUG,"new_tools_: ilLength<%d>",ilLength);
	if (prlOutEvent==NULL) {
		while(ilTotalLen<ilLength)
			ilTotalLen+=32*1024; /* Increase in steps of 32K */
		prlOutEvent = (EVENT *) malloc(ilTotalLen);
		if (prlOutEvent == NULL){
			ilRc= RC_FAIL;
			dbg(TRACE,"new_tools_send_sql: Malloc error %s",strerror(errno));
		} else {
			dbg(DEBUG,"new_tools_send_sql: Alloced prlOutEvent(size= %d)",ilTotalLen);
		}
	} else if (ilTotalLen<=ilLength){
		while(ilTotalLen<=ilLength)
			ilTotalLen+=32*1024; /* Increase in steps of 32K */
		prlOutEvent=(EVENT *) realloc(prlOutEvent,ilTotalLen);
		if(prlOutEvent==NULL){
			ilRc=RC_FAIL;
			dbg(TRACE,"new_tools_send_sql: Malloc: %s", strerror(errno)); 
		} else {
			dbg(DEBUG,"new_tools_send_sql: Realloced prlOutEvent<size %d>",ilTotalLen);    
		}
	} else {
		dbg(DEBUG,"new_tools_send_sql: Already alloced<size %d>",ilTotalLen);
	}
	if(ilRc!=RC_FAIL) {
	memset (((char *) prlOutEvent), 0, ilTotalLen);
	prlOutEvent->data_offset = sizeof(EVENT);
	prlOutEvent->originator = mod_id; /* should B global */
	prlOutEvent->data_length =  ilLength;
	prlOutEvent->command = EVENT_DATA;
 
	prlBchd = (BC_HEAD *) (((char *)prlOutEvent) + sizeof(EVENT));
	memcpy(prlBchd,prpBchd,sizeof(BC_HEAD));
	/* prlBchd->rc=prpBchd->rc; */
	/* DebugPrintBchead(TRACE,prlBchd);  */
		if( prpBchd->rc!= DB_SUCCESS ) {
			/* if there was an error, then the data which contains
				the error description, gets copied to the BCHD->data
				area. This is because the dll uses this when the RC is
				non zero ! The command block will then point to the
				address after the BCHD address plus the length of the data */
			strcpy(prlBchd->data,pcpData);
			dbg(DEBUG,"prlBchd->data <%s>|| pcpData<%s>",prlBchd->data,pcpData);
			prlCmdBlk = (CMDBLK *)((char *)prlBchd->data+strlen(pcpData)+1);
			dbg(DEBUG,"new_tool: prlCmblk<%s>",prlCmdBlk);
		}
		else
		{
			prlCmdBlk= (CMDBLK  *) ((char *)prlBchd->data);
		}
		/* strcpy(prlCmdBlk->command,prpCmdblk->command);
		strcpy(prlCmdBlk->obj_name,prpCmdblk->obj_name); */
		memcpy(prlCmdBlk,prpCmdblk,sizeof(CMDBLK));
		/* DebugPrintCmdblk(TRACE,prlCmdBlk); */

		pclSel = prlCmdBlk->data;
		strcpy (pclSel, pcpSelection);
		pclField = (char *) pclSel + strlen (pclSel) + 1;
		strcpy (pclField, pcpFields);
		pclDataBlk   = pclField + strlen(pclField) + 1;
		strcpy (pclDataBlk, pcpData);
		dbg(DEBUG,"new_tools: Sel<%s> Fields<%s> Data<%s>",pclSel,pclField,pclDataBlk);
		ilRc = que(QUE_PUT,ipRouteID,mod_id,PRIORITY_3,ilLength,
	    		 (char *)prlOutEvent); 
		dbg(DEBUG,"new_tools_send_sql: Returning rc=%d", ilRc); 	
	}
	return ilRc;
}

static int TriggerAction(char *pcpTableName, char *pcpCommands)
{
	int           ilRc          = RC_SUCCESS ;    /* Return code */
	EVENT        *prlEvent      = NULL ;
	BC_HEAD      *prlBchead     = NULL ;
	CMDBLK       *prlCmdblk     = NULL ;
	/* 20030730 HAG,JIM: avoid alignment error by writing int to pclData */
	/* ACTIONConfig *prlAction  = NULL ; pointer may result to enlignment error */
	ACTIONConfig rlAction;

	char         *pclSelection  = NULL ;
	char         *pclFields     = NULL ;
	char         *pclData       = NULL ;
	long          llActSize     = 0 ;
	int           ilAnswerQueue = 0 ;
	char          clQueueName[16] ;
	
	if (ilRc == RC_SUCCESS)
	{
		sprintf (&clQueueName[0],"%s2", mod_name) ;
		
		ilRc = GetDynamicQueue(&ilAnswerQueue,&clQueueName[0]);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"TriggerAction: GetDynamicQueue failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"TriggerAction: got <%d> for <%s>",ilAnswerQueue, &clQueueName[0]);
		}/* end of if */
	}/* end of if */
	
	if (ilRc == RC_SUCCESS)
	{
		llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + sizeof(ACTIONConfig) + 666;
		prgOutEvent = realloc(prgOutEvent,llActSize);
		if(prgOutEvent == NULL)
		{
			dbg(TRACE,"TriggerAction: realloc out event <%d> bytes failed",llActSize);
			ilRc = RC_FAIL ;
		} /* end of if */
	} /* end of if */

	if(ilRc == RC_SUCCESS)
	{
		prlBchead     = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
		prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data) ;
		pclSelection  = prlCmdblk->data ;
		pclFields     = pclSelection + strlen (pclSelection) + 1 ;
		pclData       = pclFields + strlen (pclFields) + 1 ;
		strcpy (pclData, "DYN") ;
	   /* 20030730 HAG, JIM: avoid alignment error by writing int to pclData */
		memset(&rlAction,0,sizeof(ACTIONConfig));
		/* prlAction     = (ACTIONConfig *) (pclData + strlen(pclData) + 1) ;*/
			
		prlBchead->rc = RC_SUCCESS;
			
		prgOutEvent->type        = USR_EVENT ;
		prgOutEvent->command     = EVENT_DATA ;
		prgOutEvent->originator  = ilAnswerQueue ;
		prgOutEvent->retry_count = 0 ;
		prgOutEvent->data_offset = sizeof(EVENT) ;
		prgOutEvent->data_length = llActSize-sizeof(EVENT) ;
		
		rlAction.iEmptyFieldHandling = iDELETE_ALL_BLANKS ;
		rlAction.iIgnoreEmptyFields = 0 ;
		strcpy(rlAction.pcSndCmd, "") ;
		sprintf(rlAction.pcSectionName,"%s_%s", mod_name,pcpTableName) ;
		strcpy(rlAction.pcTableName, pcpTableName) ;
		strcpy(rlAction.pcFields, "-") ;
			 /*********************** MCU TEST ***************/
		strcpy(rlAction.pcFields, "") ;
		/*********************** MCU TEST ***************/
		/************/
		if (!strcmp(pcpTableName,"SPRTAB"))
		{
			strcpy(rlAction.pcFields, "PKNO,USTF,ACTI,FCOL") ;
		}
		/************/
		if ( pcpCommands )
			strcpy(rlAction.pcSectionCommands, pcpCommands ) ;
		else
			strcpy(rlAction.pcSectionCommands, "IRT,URT,DRT") ;
		rlAction.iModID = mod_id ;
		
		/****************************************
		  DebugPrintEvent(DEBUG,prgOutEvent) ;
		  DebugPrintBchead(DEBUG,prlBchead) ;
		  DebugPrintCmdblk(DEBUG,prlCmdblk) ;
		  dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
		  dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
		  dbg (DEBUG,"TriggerAction: data      <%s>",pclData) ;
		  DebugPrintACTIONConfig(DEBUG,prlAction) ;
		****************************************/
		
		if(ilRc == RC_SUCCESS)
		{
			rlAction.iADFlag = iDELETE_SECTION ;
		    /* 20030730 HAG, JIM: avoid alignment error by writing int to pclData */
			memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));
			ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_2, llActSize, (char *) prgOutEvent) ; 
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
			}
			else
			{
				ilRc = WaitAndCheckQueue(10, ilAnswerQueue,&prgItem) ;
				if(ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
				}
				else
				{
					/* return value for delete does not matter !!! */
				} /* end of if */
			}/* end of if */
		}/* end of if */
		
		if(ilRc == RC_SUCCESS)
		{
			rlAction.iADFlag = iADD_SECTION ;
		    /* 20030730 HAG, JIM: avoid alignment error by writing int to pclData */
			memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));
			ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_2, llActSize,
			                                           (char *) prgOutEvent ) ;
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
			}
			else
			{
				ilRc = WaitAndCheckQueue(10,ilAnswerQueue,&prgItem) ;
				if(ilRc != RC_SUCCESS)
				{
				 dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
				}
				else
				{
					prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
					prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
					prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
					pclSelection = (char *)    prlCmdblk->data ;
					pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
					pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;
					
					if(strcmp(pclData,"SUCCESS") != 0)
					{
						dbg(TRACE,"TriggerAction: add dynamic action config failed") ;
						DebugPrintItem(DEBUG,prgItem) ;
						DebugPrintEvent(DEBUG,prlEvent) ;
						DebugPrintBchead(DEBUG,prlBchead) ;
						DebugPrintCmdblk(DEBUG,prlCmdblk) ;
						dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
						dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
						dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
						ilRc = RC_FAIL ;
					}
					else
					{
						dbg(DEBUG,"TriggerAction: add dynamic action config OK") ;
					}/* end of if */
				}/* end of if */
			}/* end of if */
		}/* end of if */
		
		ilRc = que(QUE_DELETE,ilAnswerQueue,ilAnswerQueue,0,0,0) ;
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"que QUE_DELETE <%d> failed <%d>",ilAnswerQueue, ilRc) ;
		}
		else
		{
		 dbg(DEBUG,"TriggerAction: queue <%d> <%s> deleted", ilAnswerQueue, &clQueueName[0]) ;
		}/* end of if */
	}/* end of if */

 return (ilRc) ;
	
}

static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem)
{
	int ilRc          = RC_SUCCESS;                      /* Return code */
	int ilWaitCounter = 0;

	do
	{
		sleep(1);
		ilWaitCounter += 1;

		ilRc = CheckQueue(ipModId,prpItem);
		if(ilRc != RC_SUCCESS)
		{
			if(ilRc != QUE_E_NOMSG)
			{
				dbg(TRACE,"WaitAndCheckQueue: CheckQueue <%d> failed <%d>",mod_id,ilRc);
			}/* end of if */
		}/* end of if */
	}while((ilRc == QUE_E_NOMSG) && (ilWaitCounter < ipTimeout));

	if(ilWaitCounter >= ipTimeout)
	{
		dbg(TRACE,"WaitAndCheckQueue: timeout reached <%d>",ipTimeout);
		ilRc = RC_FAIL;
	}/* end of if */

	return ilRc;
}


static int CheckQueue(int ipModId, ITEM **prpItem)
{
	int     ilRc       = RC_SUCCESS;                      /* Return code */
	int     ilItemSize = 0;
	EVENT *prlEvent    = NULL;
static	ITEM  *prlItem      = NULL;
	
	ilItemSize = I_SIZE;
	
	ilRc = que(QUE_GETBIGNW,ipModId,ipModId,PRIORITY_3,ilItemSize,(char *) &prlItem);
	if(ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"DebugPrintItem with prlItem follows");
		DebugPrintItem(TRACE,prlItem);
		*prpItem = prlItem;
		dbg(TRACE,"DebugPrintItem with prpItem follows");
		DebugPrintItem(TRACE,*prpItem);
		prlEvent = (EVENT*) ((prlItem)->text);

		switch( prlEvent->command )
		{
			case    HSB_STANDBY :
				dbg(TRACE,"CheckQueue: HSB_STANDBY");
				HandleQueues();
				ctrl_sta = prlEvent->command;
				break;
	
			case    HSB_COMING_UP   :
				dbg(TRACE,"CheckQueue: HSB_COMING_UP");
				ctrl_sta = prlEvent->command;
				break;
	
			case    HSB_ACTIVE  :
				dbg(TRACE,"CheckQueue: HSB_ACTIVE");
				ctrl_sta = prlEvent->command;
				break;
	
			case    HSB_ACT_TO_SBY  :
				dbg(TRACE,"CheckQueue: HSB_ACT_TO_SBY");
				HandleQueues();
				ctrl_sta = prlEvent->command;
				break;
	
			case    HSB_DOWN    :
				dbg(TRACE,"CheckQueue: HSB_DOWN");
				ctrl_sta = prlEvent->command;
				/* Acknowledge the item 
				ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
				if( ilRc != RC_SUCCESS )
				{
					HandleQueErr(ilRc);
				}  */
				Terminate(30);
				break;
		
		case    HSB_STANDALONE  :
				dbg(TRACE,"CheckQueue: HSB_STANDALONE");
				ctrl_sta = prlEvent->command;
				break;
	
			case    SHUTDOWN    :
				dbg(TRACE,"CheckQueue: SHUTDOWN");
				/* Acknowledge the item */
				ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
				if( ilRc != RC_SUCCESS )
				{
					/* handle que_ack error */
					HandleQueErr(ilRc);
				} /* fi */
				Terminate(30);
				break;
	
			case    RESET       :
				ilRc = Reset();
				break;
	
			case    EVENT_DATA  :
				/* DebugPrintItem(DEBUG,prgItem); */
				/* DebugPrintEvent(DEBUG,prlEvent); */
				break;
	
			case    TRACE_ON :
				dbg_handle_debug(prlEvent->command);
				break;
	
			case    TRACE_OFF :
				dbg_handle_debug(prlEvent->command);
				break;


			default         :
				dbg(TRACE,"CheckQueue: unknown event");
				DebugPrintItem(TRACE,prlItem);
				DebugPrintEvent(TRACE,prlEvent);
				break;
	
		} /* end switch */
	
		/* Acknowledge the item */
		ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
		if( ilRc != RC_SUCCESS )
		{
			/* handle que_ack error */
			HandleQueErr(ilRc);
		} /* fi */
	}else{
		if(ilRc != QUE_E_NOMSG)
		{
			dbg(TRACE,"CheckQueue: que(QUE_GETBIG,%d,%d,...) failed <%d>",ipModId,ipModId,ilRc);
			HandleQueErr(ilRc);
		}/* end of if */
	}/* end of if */
       
	return ilRc;
}/* end of CheckQueue */
#endif


/****************************************************************************
* Function: GetUrnoFromSelection
* Parameter:           
*			 
* Return:
* Description: gets the Urno from the SQL Statement "WHERE URNO = 'xxxxxxx'"
*			   
*****************************************************************************/
static int GetUrnoFromSelection(char *pcpSelection,char *pcpUrno)
{
	char *pclFirst;
	char *pclLast;
	char pclTmp[24];
	int ilLen;
	
	pclFirst = strchr(pcpSelection,'\'');
	if (pclFirst != NULL)
	{
		pclLast  = strrchr(pcpSelection,'\'');
		if (pclLast == NULL)
		{
			pclLast = pclFirst + strlen(pclFirst);
		}
		ilLen = pclLast-pclFirst < 22 ? pclLast - pclFirst : 22;
		strncpy(pclTmp,++pclFirst,ilLen);
		pclTmp[ilLen-1] = '\0';
		strcpy(pcpUrno,pclTmp);
	}
	else
		{
		 pclFirst = pcpSelection;
		 while (!isdigit(*pclFirst) && *pclFirst != '\0')
		 {
			pclFirst++;
			}
		strcpy(pcpUrno,pclFirst);
		}
	return RC_SUCCESS;
}


static int SaveIndexInfo (ARRAYINFO *prpArrayInfo)
{
  int    ilRC       = RC_SUCCESS ;
  long   llRow      = 0 ;
  FILE  *prlFile    = NULL ;
  char   clDel      = ',' ;
  char   clFile[512] ;
  char  *pclTrimBuf = NULL ;

  dbg (TRACE,"SaveIndexInfo: ArrayName <%s>",prpArrayInfo->crArrayName) ;

  if (ilRC == RC_SUCCESS)
  {
   sprintf (&clFile[0],"%s/%s_%s.info",getenv("DBG_PATH"),mod_name,prpArrayInfo->crArrayName) ;

   errno = 0 ;
   prlFile = fopen (&clFile[0], "w") ;
   if (prlFile == NULL)
   {
    dbg (TRACE, "SaveIndexInfo: fopen <%s> failed <%d-%s>", &clFile[0], errno, strerror(errno)) ;
    ilRC = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if (ilRC == RC_SUCCESS)
  {
   if (igTestOutput > 2) 
   {
    dbg (DEBUG, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ;      
    dbg (DEBUG, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ;  
    dbg (DEBUG, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ;  
   }

   fprintf (prlFile, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ; fflush(prlFile) ;
   fprintf (prlFile, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ; fflush(prlFile) ;
   fprintf (prlFile, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ; fflush(prlFile) ;

   if (igTestOutput > 1) 
   {
    dbg(DEBUG,"IndexData") ; 
   }
   fprintf(prlFile,"IndexData\n"); fflush(prlFile);

   llRow = ARR_FIRST ;
   do
   {
    ilRC = CEDAArrayFindKey (&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), &(prpArrayInfo->rrIdx01Handle), prpArrayInfo->crIdx01Name, &llRow, prpArrayInfo->lrIdx01RowLen, prpArrayInfo->pcrIdx01RowBuf);
    if (ilRC == RC_SUCCESS)
    {
     if (igTestOutput > 1) 
     {
      dbg(DEBUG, "SaveIndexInfo: Key <%ld> <%s>",llRow,prpArrayInfo->pcrIdx01RowBuf) ;
     }

     if (strlen (prpArrayInfo->pcrIdx01RowBuf) == 0)
     {
      ilRC = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
      if(ilRC == RC_SUCCESS)
      {
       if (igTestOutput > 1) 
       {
        dbg (DEBUG, "SaveIndexInfo: Row <%ld> <%s>", llRow,prpArrayInfo->pcrArrayRowBuf) ;
       }
      } /* end of if */
     } /* end of if */

     pclTrimBuf = strdup (prpArrayInfo->pcrIdx01RowBuf) ;
     if (pclTrimBuf != NULL)
     {
      ilRC = GetDataItem (pclTrimBuf,prpArrayInfo->pcrIdx01RowBuf, 1, clDel, "", "  ") ;
      if (ilRC > 0)
      {
       ilRC = RC_SUCCESS ;
       fprintf (prlFile,"Key            <%ld><%s>\n",llRow,pclTrimBuf) ; fflush(prlFile) ;
      } /* end of if */

      free (pclTrimBuf) ;
      pclTrimBuf = NULL ;
     } /* end of if */

     llRow = ARR_NEXT;
    }
    else
    {
     dbg (TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRC) ;
    } /* end of if */
   } while (ilRC == RC_SUCCESS) ;

   if (igTestOutput > 1) 
   {
    dbg (DEBUG,"ArrayData") ;
   }
   fprintf (prlFile,"ArrayData\n"); fflush(prlFile);

   llRow = ARR_FIRST ;
   do
   {
    ilRC = CEDAArrayFindKey (&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), &(prpArrayInfo->rrIdx01Handle), prpArrayInfo->crIdx01Name, &llRow, prpArrayInfo->lrIdx01RowLen, prpArrayInfo->pcrIdx01RowBuf);
    if (ilRC == RC_SUCCESS)
    {
     ilRC = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
     if (ilRC == RC_SUCCESS)
     {
      if (igTestOutput > 1) 
      {
       dbg(DEBUG,"SaveIndexInfo: Row <%ld> <%s>",llRow,prpArrayInfo->pcrArrayRowBuf) ;
      }
      fprintf (prlFile,"Row            <%ld><%s>\n",llRow,prpArrayInfo->pcrArrayRowBuf) ; fflush(prlFile) ;
     }
     else
     {
      dbg (TRACE, "SaveIndexInfo: CEDAArrayGetRow failed <%d>", ilRC) ;
     } /* end of if */

     llRow = ARR_NEXT ;
    } 
    else
    {
     dbg (TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRC) ;
    } /* end of if */
   } while (ilRC == RC_SUCCESS) ;

   if (ilRC == RC_NOTFOUND)
   {
    ilRC = RC_SUCCESS ;
   } /* end of if */
  } /* end of if */

  if (prlFile != NULL)
  {
   fclose(prlFile) ;
   prlFile = NULL ;
   if (igTestOutput > 1) 
   {
    dbg (TRACE, "SaveIndexInfo: event saved - file <%s>", &clFile[0]) ;
   }
  } /* end of if */

  return (ilRC) ;

}

static int SetSprStatus(char *pcpFields,char *pcpData)
{
	int ilRc = RC_SUCCESS;
	long llRowNum = ARR_FIRST;
	char *pclRow;
	int ilUstfLen,ilUjtyLen;
	char	clUSTF[21];
	char clACTI[21];
	char clFCOL[11];
	char clUDSR[21]="";
	char clKey[31];
	char  clDel = ',';
	int   ilItemNo = 0;
	int   ilPos = 0;
	int   ilCol = 0;
	BOOL blFound = FALSE;
	static long llFieldNoACFR = -1;
	static long llFieldNoACTO = -1;
	static long llFieldNoSTAT = -1; 

	ilRc = FindItemInList(pcpFields,"USTF",clDel,&ilItemNo,&ilCol,&ilPos);
	if(ilRc == RC_SUCCESS)
	{
		get_real_item(clUSTF,pcpData,ilItemNo);
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = FindItemInList(pcpFields,"FCOL",clDel,&ilItemNo,&ilCol,&ilPos);
		if(ilRc == RC_SUCCESS)
		{
			get_real_item(clFCOL,pcpData,ilItemNo);
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = FindItemInList(pcpFields,"ACTI",clDel,&ilItemNo,&ilCol,&ilPos);
		if(ilRc == RC_SUCCESS)
		{
			get_real_item(clACTI,pcpData,ilItemNo);
		}
	}

	dbg(DEBUG,"SetSPRStatus: USTF <%s> FCOL <%s> ACTI <%s>",
			clUSTF,clFCOL,clACTI);
	if (ilRc == RC_SUCCESS)
	{
		ilUstfLen = strlen(clUSTF);
		ilUjtyLen = strlen(cgUjty);
		/*Handle Pooljobs*/
		if( (atoi(clFCOL) < 2) ||  (atoi(clFCOL) >= 4)  )
		{
			sprintf ( clKey, "%s,%s", cgUjty, clUSTF );
			while ( CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
									 &(rgJobArray.crArrayName[0]),
									 &(rgJobArray.rrIdx03Handle),
									 &(rgJobArray.crIdx03Name[0]), clKey,
									 &llRowNum, (void *) &pclRow ) == RC_SUCCESS ) 
			{
				/* job for this employee found */
				char clFrom[24];
				char clTo[24];
				char clStat[24];
				BOOL blOverlap = FALSE;

				strcpy(clFrom,FIELDVAL(rgJobArray,pclRow,igJobACFR));
				strcpy(clTo,FIELDVAL(rgJobArray,pclRow,igJobACTO));

				dbg(DEBUG,"SetSprStatus: URNO <%s> ACTI %s, From %s+(%s), To %s+(%s)",
  						  FIELDVAL(rgJobArray,pclRow,igJobURNO), clACTI,clFrom,
						  cgSprOffsetFrom,clTo,cgSprOffsetTo);
				if ( atoi(clFCOL) < 2 )   /* for abscence no Offset */
				{
					StrAddTime(clFrom,cgSprOffsetFrom,clFrom,0);
					StrAddTime(clTo,cgSprOffsetTo,clTo,0);
				}
				blOverlap = ((strcmp(clFrom,clACTI) < 1) &&
							 (strcmp(clTo,clACTI) > -1));
				if (debug_level == DEBUG)
				{
					BOOL blStart = FALSE;
					BOOL blEnd = FALSE;

					blStart = (strcmp(clFrom,clACTI) < 1);
					blEnd = (strcmp(clTo,clACTI) > -1);
					dbg(DEBUG, "SetSprStatus: ACTI %s, From %s, To %s, Overlap %d, Start %d End %d",
							   clACTI, clFrom, clTo, blOverlap, blStart, blEnd );
				}
				if (blOverlap)
				{
					char clACFR[24];
					char clACTO[24];
					char clSTAT[24];
					/* we found the corresponding job */
					sprintf(clSTAT, "%-10s", JOBFIELD(pclRow,igJobSTAT));

					if(*clFCOL == '1' && *clSTAT != 'P')
					{
						dbg(TRACE,"SetSprStatus: Job is already confirmed/finished <%s>",clSTAT);
						ilRc = RC_FAIL;
					}
					if(*clFCOL == '0' && *clSTAT != 'C')
					{
						dbg(TRACE,"SetSprStatus: Job is not confirmed or already finished <%s>",clSTAT);
						ilRc = RC_FAIL;
					}
					if(*clFCOL == '4' && clSTAT[4] != 'A')
					{
						dbg(TRACE,"SetSprStatus: Employee isn't abscent <%s>",clSTAT);
						ilRc = RC_FAIL;
					}
					if(*clFCOL == '5' && clSTAT[4] == 'A')
					{
						dbg(TRACE,"SetSprStatus: Employee is already abscent <%s>",clSTAT);
						ilRc = RC_FAIL;
					}
					if (ilRc == RC_SUCCESS)
					{
						blFound = TRUE;

						strcpy(clACFR,JOBFIELD(pclRow,igJobACFR));
						strcpy(clACTO,JOBFIELD(pclRow,igJobACTO));

						if( (*clFCOL == '1') && bgSetStartAtClockIn )
						{
							CEDAArrayPutField(&rgJobArray.rrArrayHandle,
								rgJobArray.crArrayName,
									&llFieldNoACFR,"ACFR",llRowNum,clACTI);
							/* *clSTAT = 'C'; */
							UpdateDetachedJob ( pclRow, "ACFR", clACTI );

						}
						else
							if( (*clFCOL == '0') && bgSetEndAtClockOut )
							{
								CEDAArrayPutField(&rgJobArray.rrArrayHandle,
									rgJobArray.crArrayName,
										&llFieldNoACTO,"ACTO",llRowNum,clACTI);
								/* *clSTAT = 'F'; */
								UpdateDetachedJob ( pclRow, "ACTO", clACTI );
							}
						strcpy ( clUDSR, JOBFIELD(pclRow,igJobUDSR));
						if ( atoi(clFCOL) >= 4 )   /* for abscence no Offset */
							break;
						/* CEDAArrayPutField(&rgJobArray.rrArrayHandle,
							rgJobArray.crArrayName,
								&llFieldNoSTAT,"STAT",llCurrent,clSTAT); */
					}
					/*
					break;  we found the matching job and stop working for this SPR*/
				}
				llRowNum = ARR_NEXT;
			}
			if(blFound)
			{
				ilItemNo = SetPooljobStatus ( clFCOL[0], clUDSR );
				dbg ( DEBUG, "SetSprStatus: SetPooljobStatus changed <%d> Pooljobs", ilItemNo );
				SavePoolJobs(FALSE, 0 , 0);	
			}
		}
		else /*Handle breaks*/
		{
			ilUjtyLen = strlen(cgUjtyBrk);

			dbg(DEBUG,"HANDlE Breaks");
			while((CEDAArrayGetRowPointer(&rgBrkArray.rrArrayHandle,
				rgBrkArray.crArrayName,llRowNum,(void *)&pclRow) == RC_SUCCESS) &&
					(! blFound))
			{
				llRowNum = ARR_NEXT;
				dbg(DEBUG,"Ustf <%s>",FIELDVAL(rgBrkArray,pclRow,igJobUSTF));
				if((!strncmp(clUSTF,FIELDVAL(rgBrkArray,pclRow,
							igJobUSTF),ilUstfLen)) &&
					(!strncmp(cgUjtyBrk,FIELDVAL(rgBrkArray,pclRow,
								igJobUJTY),ilUjtyLen)))
				{
				
				
					/* break for this employee found */
						char clFrom[24];
						char clTo[24];
						char clStat[24];
						BOOL blOverlap = FALSE;

						strcpy(clFrom,FIELDVAL(rgBrkArray,pclRow,igJobACFR));
						strcpy(clTo,FIELDVAL(rgBrkArray,pclRow,igJobACTO));

						dbg(DEBUG,"ACTI %s, From %s+(%s), To %s+(%s)",
							clACTI,clFrom,cgSprBreakOffsetFrom,clTo,cgSprBreakOffsetTo);
						StrAddTime(clFrom,cgSprBreakOffsetFrom,clFrom,0);
						StrAddTime(clTo,cgSprBreakOffsetTo,clTo,0);
						blOverlap = ((strcmp(clFrom,clACTI) < 1) &&
										(strcmp(clTo,clACTI) > -1));
						if (debug_level == DEBUG)
						{
							BOOL blStart = FALSE;
							BOOL blEnd = FALSE;

						blStart = (strcmp(clFrom,clACTI) < 1);
						blEnd = (strcmp(clTo,clACTI) > -1);
						dbg(DEBUG,"ACTI %s, From %s, To %s, Overlap %s, Start %s End %s",
							clACTI,clFrom,clTo,
								blOverlap ?   "TRUE" : "FALSE",
								blStart ?  "TRUE" : "FALSE",
								blEnd ? "TRUE" : "FALSE");
						}
						if (blOverlap)
						{
							char clACFR[24];
							char clACTO[24];
							char clSTAT[24];
							/* we found the correspongind job */
							strcpy(clSTAT,JOBFIELD(pclRow,igJobSTAT));
							if(*clFCOL == '2' && *clSTAT != 'P')
							{
								dbg(DEBUG,"Job is already confirmed/finished <%s>",clSTAT);
								ilRc = RC_FAIL;
							}
							if(*clFCOL == '3' && *clSTAT != 'C')
							{
								dbg(DEBUG,"Job is not confirmed or already finished <%s>",clSTAT);
								ilRc = RC_FAIL;
							}
							if (ilRc == RC_SUCCESS)
							{
								long llCurrent = ARR_CURRENT;
								blFound = TRUE;
								
								strcpy(clACFR,JOBFIELD(pclRow,igJobACFR));
								strcpy(clACTO,JOBFIELD(pclRow,igJobACTO));


								if(*clFCOL == '2')
								{
									CEDAArrayPutField(&rgBrkArray.rrArrayHandle,
										rgBrkArray.crArrayName,
											&llFieldNoACFR,"ACFR",llCurrent,clACTI);
									*clSTAT = 'C';
								}
								else
								{
									CEDAArrayPutField(&rgBrkArray.rrArrayHandle,
										rgBrkArray.crArrayName,
											&llFieldNoACTO,"ACTO",llCurrent,clACTI);
									*clSTAT = 'F';
								}
								CEDAArrayPutField(&rgBrkArray.rrArrayHandle,
									rgBrkArray.crArrayName,
										&llFieldNoSTAT,"STAT",llCurrent,clSTAT);
							}
							break; /* we found the matching job and stop working for this SPR*/
						}

				}
				llRowNum = ARR_NEXT;
			}
			if(blFound)
			{
				if ( (ilRc = CEDAArrayWriteDB(&rgBrkArray.rrArrayHandle, rgBrkArray.crArrayName,
											  NULL, NULL, NULL, NULL, ARR_COMMIT_ALL_OK) )
					  != RC_SUCCESS)
				{
					dbg(TRACE, "SetSprStatus: CEDAArrayWriteDB of Break Jobs error %d", ilRc);
				}
			}

		}
	}
	return ilRc;

}

static int CreateBreakJob( char *pcpNewJobRow,char *pcpDrrRow )
{
	int		ilRc = RC_SUCCESS;
	int		ilSblu = 0;
	time_t	/*tlPoolStart, tlPoolEnd,*/ tlSbfr;
	char	pclPoolStart[20];
	char	pclPoolEnd[20];
	char	pclBreakStart[20];
	char	pclBreakEnd[20];
	char	clSblu[20];
	char	clPauseEnd[20];
	char	clNow[128];
	char	clUrno[20];
	long	llRow = ARR_FIRST;
	long	llRowNum = ARR_FIRST;
	long	llFieldNum = -1;
	char    *pclBrkRow = NULL;
/*FSC changes for AutoCoverage*/
	int ilBros = 0;
	char    clBros[10] = "";
/*FSC changes for AutoCoverage end*/


	while((CEDAArrayFindRowPointer(&(rgBrkArray.rrArrayHandle),
									&(rgBrkArray.crArrayName[0]),
									&(rgBrkArray.rrIdx01Handle),
									&(rgBrkArray.crIdx01Name[0]),
									JOBFIELD(pcpNewJobRow,igJobURNO),&llRowNum,
									(void *) &pclBrkRow ) == RC_SUCCESS) && (ilRc == RC_SUCCESS))
	{
		if(atol(JOBFIELD(pclBrkRow,igJobUJTY)) == atol(cgUjtyBrk))
		{
			ilRc = RC_FAIL;
			/*Pooljob has already a break*/
		}
		llRowNum = ARR_NEXT;
	}

	if(ilRc == RC_SUCCESS)
	{

			/*FSC changes start*/
	/*
		StrToTime( JOBFIELD(cgNewJobBuf,igJobACFR),&tlPoolStart);
		tlPoolStart = tlPoolStart - 120;
		TimeToStr(pclPoolStart,tlPoolStart);
	*/


		
		StrAddTime(JOBFIELD(pcpNewJobRow,igJobACFR), "-120", pclPoolStart, 0);
		/*FSC changes end*/
		dbg(DEBUG,"Adjusted Pool Job Begin (-2min) <%s>",pclPoolStart);

		/*FSC changes start*/
	/*
		StrToTime( JOBFIELD(pcpNewJobRow,igJobACTO),&tlPoolEnd);
		tlPoolEnd = tlPoolEnd + 120;
		TimeToStr(pclPoolEnd,tlPoolEnd);
	*/
		StrAddTime(JOBFIELD(pcpNewJobRow,igJobACTO), "120", pclPoolEnd,0);
		/*FSC changes end*/
		dbg(DEBUG,"Adjusted Pool Job End (+2min) <%s>",pclPoolEnd);

		ilSblu = atoi(DRRFIELD(pcpDrrRow,igDrrSBLU)) * 60;
		dbg(DEBUG,"DRR.SBLU (default pause length in seconds) <%d>",ilSblu);

		dbg(DEBUG,"DRR.SBFR (Pausenlage begin - local) <%s>",
			DRRFIELD(pcpDrrRow,igDrrSBFR));


		/*FSC,HAG changes for AutoCoverage*/
		if ( igDrrBROS > 0 )
		{
			ilBros = atoi(DRRFIELD(pcpDrrRow,igDrrBROS)) * 60;
			sprintf(clBros,"%d",ilBros);
			dbg(TRACE,"clBros  <%s>",clBros);
			StrAddTime(DRRFIELD(pcpDrrRow,igDrrSBFR), clBros, pclBreakStart,1);
		}
		else	/*FSC,HAG changes for AutoCoverage end*/
		{
			strcpy(pclBreakStart, DRRFIELD(pcpDrrRow,igDrrSBFR));
			LocalToUtc(pclBreakStart);
		}
		dbg(DEBUG,"DRR.SBFR (Pausenlage begin - UTC)   <%s>",pclBreakStart);

		dbg(DEBUG,"DRR.SBTO (Pausenlage end - local) <%s>",
			DRRFIELD(pcpDrrRow,igDrrSBTO));
		strcpy(pclBreakEnd, DRRFIELD(pcpDrrRow,igDrrSBTO));

		LocalToUtc(pclBreakEnd);
		dbg(DEBUG,"DRR.SBTO (Pausenlage end - UTC)   <%s>",pclBreakEnd);

		if (ilSblu <= 0 || StrToTime(pclBreakStart,&tlSbfr) != RC_SUCCESS)
		{
			dbg(DEBUG, "DRR.SBLU <%d> or DRR.SBFR <%d not valid", ilSblu,
				pclBreakStart);
			ilRc = RC_FAIL;
		}
		else
		{
			/*FSC changes start*/
			/*
			tlSbfr = tlSbfr + ilSblu;
			TimeToStr(clPauseEnd,tlSbfr);
			*/

			sprintf(clSblu, "%d", ilSblu);
			StrAddTime(pclBreakStart, clSblu, clPauseEnd,0);

			/*FSC changes end*/

			/* if the pause doesn't fit in the Pausenlage then truncate it */
			if(strcmp(clPauseEnd,pclBreakEnd) > 0)
			{
				strcpy(clPauseEnd,pclBreakEnd);
				dbg(DEBUG, "PauseEnd outside Pausenlage - truncate to <%s>",
					clPauseEnd);
			}

			dbg(DEBUG, "Calculated Pause End <%s>", clPauseEnd);
			/* allocate pauses only if the pause is completely within the pool job*/

    		dbg(DEBUG, "Pause <%s> - <%s>  PoolJob <%s> - <%s>", pclBreakStart,
				clPauseEnd, pclPoolStart, pclPoolEnd);
			if (strcmp(pclPoolStart,pclBreakStart) <= 0 &&
				 strcmp(pclPoolEnd,clPauseEnd) >= 0)
			{
			
				/*GetNextValues(clUrno,1);*/
				GetNextUrno(clUrno);
				TimeToStr(clNow,time(NULL));

				memset(cgBrkJobBuf,0x00,rgBrkArray.lrArrayRowLen);

				ilRc = CEDAArrayAddRow(&(rgBrkArray.rrArrayHandle),
								&(rgBrkArray.crArrayName[0]),&llRow,
								(void *)cgBrkJobBuf);
				if ( ilRc != RC_SUCCESS )
					dbg(TRACE, "CreateBreakJob:CEDAArray failed with %d", ilRc);
				llFieldNum = -1;
				ilRc = CEDAArrayPutField(&(rgBrkArray.rrArrayHandle),
											rgBrkArray.crArrayName,&llFieldNum,
											"URNO",llRow,clUrno);
				llFieldNum = -1;
				CEDAArrayPutField(&(rgBrkArray.rrArrayHandle),
								rgBrkArray.crArrayName,&llFieldNum,"CDAT",llRow,clNow);
				llFieldNum = -1;
				CEDAArrayPutField(&(rgBrkArray.rrArrayHandle),
								rgBrkArray.crArrayName,&llFieldNum,"HOPO",llRow,cgHopo);
				llFieldNum = -1;
				CEDAArrayPutField(&(rgBrkArray.rrArrayHandle),
								rgBrkArray.crArrayName,&llFieldNum,"STAT",llRow,"P 0");
				llFieldNum = -1;
				CEDAArrayPutField(&(rgBrkArray.rrArrayHandle),
								rgBrkArray.crArrayName,&llFieldNum,"USEC",llRow,"POLHDL");
				llFieldNum = -1;
				CEDAArrayPutField(&(rgBrkArray.rrArrayHandle),
								rgBrkArray.crArrayName,&llFieldNum,"JOUR",llRow,
								JOBFIELD(pcpNewJobRow,igJobURNO));
				llFieldNum = -1;
				CEDAArrayPutField(&(rgBrkArray.rrArrayHandle),
								rgBrkArray.crArrayName,&llFieldNum,"PLFR",llRow,
								pclBreakStart);
				llFieldNum = -1;
				CEDAArrayPutField(&(rgBrkArray.rrArrayHandle),
								rgBrkArray.crArrayName,&llFieldNum,"ACFR",llRow,
								pclBreakStart);
				llFieldNum = -1;
				CEDAArrayPutField(&(rgBrkArray.rrArrayHandle),
								rgBrkArray.crArrayName,&llFieldNum,"PLTO",llRow,
								clPauseEnd);
				llFieldNum = -1;
				CEDAArrayPutField(&(rgBrkArray.rrArrayHandle),
								rgBrkArray.crArrayName,&llFieldNum,"ACTO",llRow,
								clPauseEnd);
				llFieldNum = -1;
				CEDAArrayPutField(&(rgBrkArray.rrArrayHandle),
								rgBrkArray.crArrayName,&llFieldNum,"UDSR",llRow,
								DRRFIELD(pcpDrrRow,igDrrURNO));
				llFieldNum = -1;
				CEDAArrayPutField(&(rgBrkArray.rrArrayHandle),
								rgBrkArray.crArrayName,&llFieldNum,"UJTY",llRow,
								cgUjtyBrk);
				llFieldNum = -1;
				CEDAArrayPutField(&(rgBrkArray.rrArrayHandle),
								rgBrkArray.crArrayName,&llFieldNum,"USTF",llRow,
								DRRFIELD(pcpDrrRow,igDrrSTFU));
				llFieldNum = -1;
				CEDAArrayPutField(&(rgBrkArray.rrArrayHandle),
									rgBrkArray.crArrayName,&llFieldNum,"UAFT",llRow,								"0");
				if (bgUseFilt)
				{
						llFieldNum = -1;
						CEDAArrayPutField(&(rgBrkArray.rrArrayHandle),
											rgBrkArray.crArrayName,&llFieldNum,"FILT",
											llRow,DRRFIELD(pcpDrrRow,igDrrFILT));
				}
			}
			else
			{
				dbg(DEBUG,"Pause NOT within this pool job!");
			}
		}

		dbg(DEBUG,"***** CreateBreakForNewPoolJob() returns %s *****",(ilRc == RC_SUCCESS )? "RC_SUCCESS" : "RC_FAIL");
	}
	return ilRc;
}

static int DeletePoolJob( char *pcpDrrRow )
{
	int ilRc = DeletePoolJobByUdrr ( DRRFIELD(pcpDrrRow,igDrrURNO) );
	if ( ilRc == RC_NOTFOUND )
		dbg ( DEBUG, "DeletePoolJob: DeletePoolJobByUdrr URNO <%s> no Pooljob found", DRRFIELD(pcpDrrRow,igDrrURNO) );
	else
		dbg ( DEBUG, "DeletePoolJob: DeletePoolJobByUdrr URNO <%s> ilRc <%d>", DRRFIELD(pcpDrrRow,igDrrURNO), ilRc );
	return ilRc;
}



static int DeleteSplittedPoolJobs(char *pcpDrrUrno)
{
	int ilRc = RC_SUCCESS;
	long llRowNum = ARR_FIRST;
	char *pclPoolJobRow = NULL;
	int ilJobCount = 0;

	while (CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),
			&(rgJobArray.crArrayName[0]),
			&(rgJobArray.rrIdx01Handle),
			&(rgJobArray.crIdx01Name[0]),
			pcpDrrUrno,&llRowNum,
			(void *) &pclPoolJobRow )  == RC_SUCCESS)
	{
		ilJobCount++;
		llRowNum = ARR_NEXT;
	}
	dbg(TRACE, "DeleteSplittedPoolJobs: ilJobCount <%d>",ilJobCount);
	if(ilJobCount > 0)
	{
		llRowNum = ARR_FIRST;
		while (CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),
										&(rgJobArray.crArrayName[0]),
										&(rgJobArray.rrIdx01Handle),
										&(rgJobArray.crIdx01Name[0]),
										pcpDrrUrno,&llRowNum,
										(void *) &pclPoolJobRow )  == RC_SUCCESS)
		{
			ilRc |= CEDAArrayDeleteRow( &(rgJobArray.rrArrayHandle),
										&(rgJobArray.crArrayName[0]),llRowNum);
			llRowNum = ARR_FIRST;
			
			dbg(TRACE, "DeleteSplittedPoolJobs: delete job <%d>",JOBFIELD(pclPoolJobRow,igJobURNO));
		}
		ilRc |= CEDAArrayWriteDB(&rgJobArray.rrArrayHandle,
								 rgJobArray.crArrayName, NULL, NULL, NULL, NULL,
									ARR_COMMIT_ALL_OK);

	}

	return ilRc;
}
		
static int CheckShiftCode(char *pcpDrrUrno)
{
	int ilRc = RC_SUCCESS;
	long llRowNum = ARR_FIRST;
	long llRowNum2 = ARR_FIRST;
	long llRowNum3 = ARR_FIRST;
	char *pclPoolJobRow = NULL;
	char *pclDrrRow = NULL;
	char *pclBreakRow = NULL;
	char clDrrScod[10];
	char clJobScod[10];
	char myDrrUrno[20]="\0";
	BOOL blWriteToDB = FALSE;

  dbg(TRACE, "CheckShiftCode: pcpDrrUrno <%s>",pcpDrrUrno);
  sprintf(myDrrUrno,"%s",pcpDrrUrno);
  dbg(TRACE, "CheckShiftCode: myDrrUrno <%s>",myDrrUrno);
  TrimRight(myDrrUrno);
  dbg(TRACE, "CheckShiftCode: TrimRight myDrrUrno <%s>",myDrrUrno);
	if (CEDAArrayFindRowPointer( &(rgDrrArray.rrArrayHandle),
			&(rgDrrArray.crArrayName[0]),
			&(rgDrrArray.rrIdx02Handle),
			&(rgDrrArray.crIdx02Name[0]),
			pcpDrrUrno,&llRowNum,
			(void *) &pclDrrRow )  == RC_SUCCESS)
	{
		llRowNum2 = ARR_FIRST;
		strcpy(clDrrScod,DRRFIELD(pclDrrRow,igDrrSCOD));
		TrimRight(clDrrScod);
		dbg(TRACE, "CheckShiftCode SCOD <%s>",clDrrScod);
		while(CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),
			&(rgJobArray.crArrayName[0]),
			&(rgJobArray.rrIdx01Handle),
			&(rgJobArray.crIdx01Name[0]),
			//pcpDrrUrno,&llRowNum2,
			myDrrUrno,&llRowNum2,
			(void *) &pclPoolJobRow )  == RC_SUCCESS)
		{
			dbg(TRACE, "CheckShiftCode find job <%s>",JOBFIELD(pclPoolJobRow,igJobURNO));
			strcpy(clJobScod,JOBFIELD(pclPoolJobRow,igJobSCOD));
			TrimRight(clJobScod);
			dbg(TRACE, "CheckShiftCode JobSCOD <%s>",clJobScod);

			if(strlen(clJobScod) > 1)
			{
				if(strcmp(clJobScod,clDrrScod) != 0)
				{
					ilRc = RC_FAIL;
					llRowNum3 = ARR_FIRST;
					while(CEDAArrayFindRowPointer( &(rgBrkArray.rrArrayHandle),
						&(rgBrkArray.crArrayName[0]),
						&(rgBrkArray.rrIdx01Handle),
						&(rgBrkArray.crIdx01Name[0]),
						JOBFIELD(pclPoolJobRow,igJobURNO),&llRowNum3,
						(void *) &pclBreakRow )  == RC_SUCCESS)
					{
						dbg(TRACE, "CheckShiftCode: check break");
						if(atol(JOBFIELD(pclBreakRow,igJobUJTY)) == atol(cgUjtyBrk))
						{
							dbg(TRACE, "CheckShiftCode: delete break");
							CEDAArrayDeleteRow(&(rgBrkArray.rrArrayHandle),
							&(rgBrkArray.crArrayName[0]),llRowNum3);
							llRowNum3 = ARR_FIRST;
							blWriteToDB = TRUE;
						}
					}

				}
			}
			CEDAArrayPutField(&rgJobArray.rrArrayHandle,
							rgJobArray.crArrayName,
								NULL,"SCOD",llRowNum2,
								DRRFIELD(pclDrrRow,igDrrSCOD));				
			llRowNum2 = ARR_NEXT;
		}
	}
	if(blWriteToDB)
	{
		CEDAArrayWriteDB(&rgBrkArray.rrArrayHandle,
								rgBrkArray.crArrayName, NULL, NULL, NULL, NULL,
								ARR_COMMIT_ALL_OK);
	}
	return ilRc;
}

static int CheckBreak(char *pcpDrrRow)
{
	int ilRc = RC_SUCCESS;
	long llRowNum = ARR_FIRST;
	char *pclPoolJobRow;

	dbg(TRACE,"CheckBreak");


	while(CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),
			&(rgJobArray.crArrayName[0]),
			&(rgJobArray.rrIdx01Handle),
			&(rgJobArray.crIdx01Name[0]),
			DRRFIELD(pcpDrrRow,igDrrURNO)	,&llRowNum,
			(void *) &pclPoolJobRow )  == RC_SUCCESS)
	{
		CreateBreakJob( pclPoolJobRow,pcpDrrRow );
		llRowNum = ARR_NEXT;
	}
	return ilRc;		
}



static int PreparePoolJob(char *pcpJobUrno)
{
	int ilRc = RC_SUCCESS;
	long llRowNum = ARR_FIRST;
	long llRowNum2 = ARR_FIRST;
	char *pclPoolJobRow = NULL;
	char *pclDrrRow = NULL;
	char clScodDrr[11];
	char clScodJob[11];
	int  ilChanged = 0;

	dbg(TRACE, "PreparePoolJob: URNO-Key <%s>", pcpJobUrno );
	while (CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),
			&(rgJobArray.crArrayName[0]),
			&(rgJobArray.rrIdx02Handle),
			&(rgJobArray.crIdx02Name[0]),
			pcpJobUrno,&llRowNum,
			(void *) &pclPoolJobRow )  == RC_SUCCESS)
	{
		llRowNum2 = ARR_FIRST;
		dbg(DEBUG, "PreparePoolJob JobUdsr <%s>",JOBFIELD(pclPoolJobRow,igJobUDSR));
		if(CEDAArrayFindRowPointer( &(rgDrrArray.rrArrayHandle),
			&(rgDrrArray.crArrayName[0]),
			&(rgDrrArray.rrIdx02Handle),
			&(rgDrrArray.crIdx02Name[0]),
			JOBFIELD(pclPoolJobRow,igJobUDSR),&llRowNum2,
			(void *) &pclDrrRow )  == RC_SUCCESS)
		{
			strcpy ( clScodDrr, DRRFIELD(pclDrrRow,igDrrSCOD) );
			strcpy ( clScodJob, JOBFIELD(pclPoolJobRow,igJobSCOD) );
			TrimRight ( clScodDrr );
			TrimRight ( clScodJob );
			if ( strcmp ( clScodJob, clScodDrr) )
			{
				dbg(DEBUG, "PreparePoolJob Change JobDrrScod <%s> to <%s>",
					JOBFIELD(pclPoolJobRow,igJobSCOD), DRRFIELD(pclDrrRow,igDrrSCOD));
				CEDAArrayPutField(&rgJobArray.rrArrayHandle, rgJobArray.crArrayName,
								NULL,"SCOD",llRowNum, DRRFIELD(pclDrrRow,igDrrSCOD));		
				ilChanged ++;
			}
			else
				dbg(DEBUG, "PreparePoolJob SCOD of Job <%s> equal to Drr <%s>",
					JOBFIELD(pclPoolJobRow,igJobSCOD), DRRFIELD(pclDrrRow,igDrrSCOD));

		}
		llRowNum = ARR_NEXT;

	}
	if ( ilChanged )
		SavePoolJobs(FALSE,0,0);	
	return ilRc;
}



/******************************************************************************/
/******************************************************************************/
static int MySaveIndexInfo (ARRAYINFO *prpArrayInfo)
{
  int    ilRC       = RC_SUCCESS ;
  long   llRow      = 0 ;
  FILE  *prlFile    = NULL ;
  char   clDel      = ',' ;
  char   clFile[512] ;
  char  *pclTrimBuf = NULL ;
	long llRowCount;
	int ilCount = 0;
  char  *pclTestBuf = NULL ;


  dbg (DEBUG,"SaveIndexInfo: Arrayname <%s>",prpArrayInfo->crArrayName) ;

  if (ilRC == RC_SUCCESS)
  {
   sprintf (&clFile[0],"%s/%s_%s.info",getenv("DBG_PATH"),mod_name,prpArrayInfo->crArrayName) ;

   errno = 0 ;
   prlFile = fopen (&clFile[0], "w") ;
   if (prlFile == NULL)
   {
    dbg (DEBUG, "SaveIndexInfo: fopen <%s> failed <%d-%s>", &clFile[0], errno, strerror(errno)) ;
    ilRC = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if (ilRC == RC_SUCCESS)
  {
   {
    dbg (DEBUG, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ;      
    dbg (DEBUG, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ;  
    dbg (DEBUG, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ;  
   }

   fprintf (prlFile, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ; fflush(prlFile) ;
   fprintf (prlFile, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ; fflush(prlFile) ;
   fprintf (prlFile, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ; fflush(prlFile) ;

   {
    dbg(DEBUG,"IndexData") ; 
   }
   fprintf(prlFile,"IndexData\n"); fflush(prlFile);

	dbg(DEBUG,"FindKey <%s>",prpArrayInfo->pcrIdx01RowBuf);
	*prpArrayInfo->pcrIdx01RowBuf = '\0';
   llRow = ARR_FIRST ;
   do
   {
    ilRC = CEDAArrayFindKey (&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), &(prpArrayInfo->rrIdx01Handle), prpArrayInfo->crIdx01Name, &llRow, prpArrayInfo->lrIdx01RowLen, prpArrayInfo->pcrIdx01RowBuf);
    if (ilRC == RC_SUCCESS)
    {
    
      dbg(DEBUG, "SaveIndexInfo: Key <%ld> <%s>",llRow,prpArrayInfo->pcrIdx01RowBuf) ;
    

     if (strlen (prpArrayInfo->pcrIdx01RowBuf) == 0)
     {
      ilRC = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
      if(ilRC == RC_SUCCESS)
      {
        dbg (DEBUG, "SaveIndexInfo: Row <%ld> <%s>", llRow,prpArrayInfo->pcrArrayRowBuf) ;
      } /* end of if */
     } /* end of if */

     pclTrimBuf = strdup (prpArrayInfo->pcrIdx01RowBuf) ;
     if (pclTrimBuf != NULL)
     {
      ilRC = GetDataItem (pclTrimBuf,prpArrayInfo->pcrIdx01RowBuf, 1, clDel, "", "  ") ;
      if (ilRC > 0)
      {
       ilRC = RC_SUCCESS ;
       fprintf (prlFile,"Key            <%ld><%s>\n",llRow,pclTrimBuf) ; fflush(prlFile) ;
      } /* end of if */

      free (pclTrimBuf) ;
      pclTrimBuf = NULL ;
     } /* end of if */

     llRow = ARR_NEXT;
    }
    else
    {
     dbg (DEBUG, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRC) ;
    } /* end of if */
   } while (ilRC == RC_SUCCESS) ;

   {
    dbg (DEBUG,"ArrayData") ;
   }
   fprintf (prlFile,"ArrayData\n"); fflush(prlFile);

   llRow = ARR_FIRST ;

	 CEDAArrayGetRowCount(&(prpArrayInfo->rrArrayHandle),&(prpArrayInfo->crArrayName[0]),&llRowCount);

	llRow = ARR_FIRST;
	dbg(DEBUG,"Array  <%s> data follows Rows %ld",
			prpArrayInfo->crArrayName,llRowCount);
	do
	{
			ilRC = CEDAArrayGetRowPointer(&(prpArrayInfo->rrArrayHandle),
			&(prpArrayInfo->crArrayName[0]),llRow,(void *)&pclTestBuf);
			if (ilRC == RC_SUCCESS)
			{
				for( ilCount = 0; ilCount < prpArrayInfo->lrArrayFieldCnt; ilCount++)
				{
				
					fprintf (prlFile,"<%s>",&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]) ; 
					fflush(prlFile) ;
					dbg (DEBUG, "SaveIndexInfo: ilCount <%ld> <%s>", ilCount,&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]) ;
				}
				fprintf (prlFile,"\n") ; 
				dbg (DEBUG, "\n") ;
				fflush(prlFile) ;
				llRow = ARR_NEXT;
			}
			else
			{
				dbg(DEBUG,"GetRowPointer failed RC=%d",ilRC);
			}
	} while (ilRC == RC_SUCCESS);

   if (ilRC == RC_NOTFOUND)
   {
    ilRC = RC_SUCCESS ;
   } /* end of if */
  } /* end of if */

  if (prlFile != NULL)
  {
   fclose(prlFile) ;
   prlFile = NULL ;
  
    dbg (DEBUG, "SaveIndexInfo: event saved - file <%s>", &clFile[0]) ;
  
  } /* end of if */

  return (ilRC) ;

}/* end of SaveIndexInfo */


static int GetDebugLevel(char *pcpMode, int *pipMode)
{
	int		ilRC = RC_SUCCESS;
	char	clCfgValue[64];

	ilRC = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,
							clCfgValue);

	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
	}
	else
	{
		dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
		if (!strcmp(clCfgValue, "DEBUG"))
			*pipMode = DEBUG;
		else if (!strcmp(clCfgValue, "TRACE"))
			*pipMode = TRACE;
		else
			*pipMode = 0;
	}

	return ilRC;
}

/*
static int GetRealTimeDiff ( char *pcpLocal1, char *pcpLocal2, time_t *plpDiff )
{
	int ilRC = RC_SUCCESS;
	char clTime1[15], clTime2[15];
	struct tm	rlTimeStruct1, rlTimeStruct2;
	time_t llT1, llT2;
	
	*plpDiff = (time_t)0;
	if ( !pcpLocal1 || ( strlen ( pcpLocal1 ) != 14 ) )
		ilRC = RC_FAIL;
	if ( !pcpLocal2 || ( strlen ( pcpLocal2 ) != 14 ) )
		ilRC = RC_FAIL;
	if ( ilRC != RC_SUCCESS )
		dbg ( TRACE, "GetRealTimeDiff: At least one parameter invalid" );
	else
	{
		strcpy ( clTime1, pcpLocal1 );
		strcpy ( clTime2, pcpLocal2 );
		LocalToUtc(clTime1);
		LocalToUtc(clTime2);
		ilRC = CedaDateToTimeStruct(&rlTimeStruct1, clTime1 );
		ilRC |= CedaDateToTimeStruct(&rlTimeStruct2, clTime2 );
	}
	if ( ilRC != RC_SUCCESS )
		dbg ( TRACE, "GetRealTimeDiff: At least one call to CedaDateToTimeStruct failed" );
	else
	{
		llT1 = mktime ( &rlTimeStruct1 );
		llT2 = mktime ( &rlTimeStruct2 );
		*plpDiff = llT2 - llT1;
		dbg ( TRACE, "GetRealTimeDiff: <%s> - <%s> = %ld seconds", pcpLocal2, pcpLocal1, *plpDiff );
	}
	return ilRC;
}
*/
/*
static int PutJob(char *pcpFrom,char *pcpTo,char cpJobNo)
{
	int ilRc = RC_SUCCESS;
	char clFrom[15], clTo[15];
	int ilFrom,ilTo,ilLc;
	time_t  llStart, llEnd;
	int i;

	dbg ( TRACE, "PutJob From <%s> To <%s>  JobNo <%d>", pcpFrom, pcpTo, (short)cpJobNo );
	if (strlen(pcpFrom) < 14)
	{
		strcpy ( clFrom, cgActDay );
		for ( i=0; i<4; i++ )
			clFrom[8+i] = pcpFrom[i];
		strcpy ( clTo, cgActDay );
		for ( i=0; i<4; i++ )
			clTo[8+i] = pcpTo[i];
		clFrom[14] = clTo[14] = '\0';
	}
	else 
	{
		strcpy ( clFrom, pcpFrom );
		strcpy ( clTo, pcpTo );
	}
	ilRc = GetRealTimeDiff ( cgActDay, clFrom, &llStart );
	if ( ilRc != RC_SUCCESS )
		dbg ( TRACE, "PutJob: GetRealTimeDiff <%s> <%s> failed <%d>", cgActDay, clFrom, ilRc );
	else
	{
		ilRc = GetRealTimeDiff ( cgActDay, clTo, &llEnd );
		if ( ilRc != RC_SUCCESS )
			dbg ( TRACE, "PutJob: GetRealTimeDiff <%s> <%s> failed <%d>", cgActDay, clTo, ilRc );
	}
	if ( ilRc == RC_SUCCESS )
	{
		ilFrom = (int)(llStart/60);
		ilTo = (int)(llEnd/60);
		if (ilTo < ilFrom)
		{
			ilTo += 1440;
		}

		if ((ilFrom >= 0 && ilFrom < MAXMINUTES) &&
			(ilFrom < ilTo) && (ilTo > 0 && ilTo < MAXMINUTES))
		{
			for(ilLc = ilFrom; ilLc <= ilTo; ilLc++)
			{
				cgJobs[ilLc] = cpJobNo;
			}
		}
		else
		{
			ilRc = RC_FAIL;
		}
	}
	dbg ( TRACE, "PutJob From in Min. <%d> To in Min. <%d>", ilFrom, ilTo );
	return ilRc;
}
*/

static int PutJob(char *pcpFrom,char *pcpTo,char cpJobNo)
{
	int ilRc = RC_SUCCESS;
	char clFrom[15], clTo[15];
	int ilFrom,ilTo,ilLc;
	time_t  llStart, llEnd;
	int i;

	if (strlen(pcpFrom) < 14)
	{
		strcpy ( clFrom, cgActDay );
		for ( i=0; i<4; i++ )
			clFrom[8+i] = pcpFrom[i];
		strcpy ( clTo, cgActDay );
		for ( i=0; i<4; i++ )
			clTo[8+i] = pcpTo[i];
		clFrom[14] = clTo[14] = '\0';
	}
	else 
	{
		strcpy ( clFrom, pcpFrom );
		strcpy ( clTo, pcpTo );
	}
	dbg ( TRACE, "PutJob From (local) <%s> To (local) <%s>  JobNo <%d>", clFrom, clTo, (short)cpJobNo );
	LocalToUtc ( clFrom );
	LocalToUtc ( clTo );
	dbg ( TRACE, "PutJob From (UTC) <%s> To (UTC) <%s>  JobNo <%d>", clFrom, clTo, (short)cpJobNo );

	ilRc = StrToTime ( clFrom, &llStart );
	ilRc |= StrToTime ( clTo, &llEnd );
	
	if ( ilRc == RC_SUCCESS )
	{
		ilFrom = ( llStart - lgStartofDayUTC ) / 60;  
		ilTo = ( llEnd - lgStartofDayUTC ) / 60;  
		if (ilTo < ilFrom)
		{
			ilTo += 1440;
		}

		if ((ilFrom >= 0 && ilFrom < MAXMINUTES) &&
			(ilFrom < ilTo) && (ilTo > 0 && ilTo < MAXMINUTES))
		{
			for(ilLc = ilFrom; ilLc <= ilTo; ilLc++)
			{
				cgJobs[ilLc] = cpJobNo;
			}
		}
		else
		{
			ilRc = RC_FAIL;
		}
	}
	dbg ( TRACE, "PutJob From in Min. <%d> To in Min. <%d>", ilFrom, ilTo );
	return ilRc;
}


static int AddSecsToCEDATime ( char *pcpTime, time_t lpSecs, short spLocal )
{
	int ilRC = RC_SUCCESS;
	char clTime1[15];
	time_t llT1;
	
	if ( !pcpTime || ( strlen ( pcpTime ) != 14 ) )
		ilRC = RC_FAIL;
	if ( ilRC != RC_SUCCESS )
		dbg ( TRACE, "AddSecsToCEDATime: spLocal invalid" );
	else
	{
		strcpy ( clTime1, pcpTime );
		if ( spLocal )
			LocalToUtc(clTime1);
		ilRC = StrToTime(clTime1,&llT1);
		if ( ilRC != RC_SUCCESS )
			dbg ( TRACE, "AddSecsToCEDATime: StrToTime failed RC <%d>", ilRC );
	}
	if ( ilRC == RC_SUCCESS )
	{
		llT1 += lpSecs;
		TimeToStr(clTime1, llT1); 
		dbg ( DEBUG, "AddSecsToCEDATime: Local <%d>, <%s> + <%ld> -> <%s>", 
			  spLocal, pcpTime, lpSecs, clTime1 );
		strcpy ( pcpTime, clTime1 );
	}		
	
	return ilRC;
}

static int UpdatePoolJobs ( char *pcpDrrRow )
{
	char  	*pclOldRow = NULL;
	char  	*pclOldRow2 = NULL;
	char	*pclNewRow, *pclDetRow, *pclUstf;
	char	clKey[32], clKey2[64], clUdsr[21];
	long	llRowOld = ARR_FIRST, llRowNew, llRowCount=0, llDetRow, llRowOld2 = ARR_FIRST;
	int     ilFound, ilRC2, ilRC=RC_SUCCESS;
	int     ilRemaining=0, ilDeleted = 0, ilExamined = 0;
	int		ilJour, ilUdrd, ilUdel, ilIndDisact;

	CEDAArrayGetRowCount(&(rgNewJobsArray.rrArrayHandle),rgNewJobsArray.crArrayName,&llRowCount);
	dbg ( TRACE, "UpdatePoolJobs: rgNewJobsArray contains <%d> rows at the beginnig", llRowCount );

	ilIndDisact = CEDAArrayDisactivateIndex ( &(rgJobArray.rrArrayHandle),	
											  &(rgJobArray.crArrayName[0]),
											  &(rgJobArray.rrIdx01Handle),
											  &(rgJobArray.crIdx01Name[0]));
	dbg ( DEBUG, "UpdatePoolJobs: CEDAArrayDisactivateIndex returns <%d>", ilIndDisact );		  		  

	strcpy(clKey,DRRFIELD(pcpDrrRow,igDrrURNO));
	TrimRight(clKey);
	dbg(TRACE,"UpdatePoolJobs: searching for old jobs with UDSR = <%s>",clKey);
	/* find pool jobs */
	while ( CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),	
									 &(rgJobArray.crArrayName[0]),
									&(rgJobArray.rrIdx01Handle),
									&(rgJobArray.crIdx01Name[0]),
									clKey,&llRowOld, (void *) &pclOldRow ) 
			== RC_SUCCESS )
	{		
		/*  workaround since CEDAArrayFindRowPointer sometimes finds wrong rows */
		strcpy ( clUdsr, JOBFIELD(pclOldRow,igJobUDSR) );
		TrimRight(clUdsr);
		if ( strcmp ( clKey, clUdsr ) )
		{
			dbg ( DEBUG, "UpdatePoolJobs: found old job URNO <%s> USTF <%s> UDSR <%s> key <%s>",
						 JOBFIELD(pclOldRow,igJobURNO), JOBFIELD(pclOldRow,igJobUSTF), 
						 JOBFIELD(pclOldRow,igJobUDSR), clKey );
			llRowOld = ARR_NEXT;
			continue;
		}
		/* end of workaround */

		/* check whether it is a pooljob for a detached job type DET,DEL */
		ilJour = atoi ( JOBFIELD(pclOldRow,igJobJOUR) );
		ilUdrd = atoi ( JOBFIELD(pclOldRow,igJobUDRD) );
		ilUdel = atoi ( JOBFIELD(pclOldRow,igJobUDEL) );
		/*if ( (ilJour!=0 ) && (ilUdrd==0) && (ilUdel==0) && (!strcmp( JOBFIELD(pclOldRow,igJob)*/
		if ( (ilJour!=0 ) && (ilUdrd==0) && (ilUdel==0) )
		{	/* not the main pooljob, nor from daily roster deviations or delegations */
			llDetRow = ARR_FIRST;

			sprintf (clKey2, "%s,%s,%s,%s", JOBFIELD(pclOldRow,igJobUDSR),
											JOBFIELD(pclOldRow,igJobUAID),
											JOBFIELD(pclOldRow,igJobPLFR),
											JOBFIELD(pclOldRow,igJobPLTO) );
			dbg ( DEBUG, "UpdatePoolJobs: searching for detached job with <UDSR,UAID,PLFR,PLTO> <%s>", clKey2 );
			ilFound = CEDAArrayFindRowPointer( &(rgDetArray.rrArrayHandle),	
											   &(rgDetArray.crArrayName[0]),
											   &(rgDetArray.rrIdx01Handle),
											   &(rgDetArray.crIdx01Name[0]),
											   clKey2, &llDetRow, (void*)&pclDetRow );
			if ( ilFound == RC_SUCCESS )
			{
				dbg ( TRACE, "UpdatePoolJobs: Found detached job URNO <%s> for pool job URNO <%s>",
							JOBFIELD(pclDetRow,igJobURNO), JOBFIELD(pclOldRow,igJobURNO) );
				/*  don't delete this pooljob; it has been created by assignment to other */
				/*	pool/workgroup in OPSS-PM											 */
				if ( bgSwapShiftEvent )
				{	/*  PRF5459: USTF must be updated for detached pooljobs */
					/*  separately, all others are done in UpdatePooljob	*/
					pclUstf = DRRFIELD(pcpDrrRow,igDrrSTFU) ;
					ilRC2 = CEDAArrayPutField( &rgJobArray.rrArrayHandle,
											   rgJobArray.crArrayName, NULL,
											   "USTF",llRowOld, pclUstf );
					if ( ilRC2 != RC_SUCCESS )
					{
						/* mark for deletion */
						dbg ( TRACE, "UpdatePoolJobs: CEDAArrayPutField USTF <%s> llRow <%ld> failed, RC<%d>", 
							  pclUstf, llRowOld, ilRC2 );
					}
				}
				llRowOld = ARR_NEXT;
				ilExamined++;
				ilRemaining++;
				continue;
			}
		}

		sprintf ( clKey2, "%s,%s,%s", JOBFIELD(pclOldRow,igJobUDSR),
				  JOBFIELD(pclOldRow,igJobUDEL), JOBFIELD(pclOldRow,igJobUDRD) );
		dbg ( TRACE,"UpdatePoolJobs: searching for new jobs with <UDSR,UDEL,UDRD> <%s>",
			  clKey2 );
		llRowNew = ARR_FIRST;
		ilFound = RC_NOTFOUND;

		ilFound = CEDAArrayFindRowPointer( &(rgNewJobsArray.rrArrayHandle),	
											   &(rgNewJobsArray.crArrayName[0]),
											   &(rgNewJobsArray.rrIdx02Handle),
											   &(rgNewJobsArray.crIdx02Name[0]),
											   clKey2, &llRowNew, (void *) &pclNewRow ) ;

		if ( ilFound == RC_SUCCESS )
		{
			dbg ( TRACE, "UpdatePoolJobs: New PoolJob Row <%ld> URNO <%s> found for Old-URNO <%s>", 
						  llRowNew, JOBFIELD(pclNewRow,igJobURNO), JOBFIELD(pclOldRow,igJobURNO) );
		/* Check if this is not a Pooljob for a restricted flight job. If it is, don't use for update */
			if (atoi(JOBFIELD(pclOldRow,igJobJOUR)) > 0)
			{
				dbg(TRACE,"This is not a normal PoolJob <%s>, don't use for update", JOBFIELD(pclOldRow,igJobURNO) );
				llRowOld = ARR_NEXT;
				continue;
			}
			ilRC2 = UpdatePoolJob(JOBFIELD(pclOldRow,igJobURNO),pclNewRow );
			if ( ilRC2 != RC_SUCCESS )
			{
				dbg ( TRACE, "UpdatePoolJobs: UpdatePoolJob failed, RC<%d>", ilRC2 );
			}
			else
			{
				/*	hag20030814: don't create new break in case of update 
						lli20091028: not just update here, so still need to create break
				if ( bgBreak )
					CreateBreakJob(pclOldRow, pcpDrrRow );  */
				ilRC2 = CEDAArrayDeleteRow( &(rgNewJobsArray.rrArrayHandle),
										    rgNewJobsArray.crArrayName, llRowNew );	

				if ( ilRC2 != RC_SUCCESS )
				{
					dbg ( TRACE, "UpdatePoolJobs: CEDAArrayDeleteRow (New) failed, llRow <%ld> RC<%d>",
						  llRowNew, ilRC2 );
				}
				ilRemaining++;
			}
			llRowOld = ARR_NEXT;
		} 
		else 
		{
			char clSelection[124];
			static char cgData[512];

			dbg(DEBUG,"Search for RFJ for this Pooljob URNO <%s>", JOBFIELD(pclOldRow,igJobURNO) );
			sprintf(clSelection,"WHERE JOUR = '%s' AND UJTY = '%s'",JOBFIELD(pclOldRow,igJobURNO),cgUjtyRfj );
			ilFound = DoSingleSelect("JOBTAB",clSelection,"URNO",cgData);

		}
		if ( ilFound != RC_SUCCESS )
		{
			dbg ( TRACE, "UpdatePoolJobs: Old PoolJob URNO <%s> is not needed any longer",
						  JOBFIELD(pclOldRow,igJobURNO) );
			ilRC2 = CEDAArrayPutField(&rgJobArray.rrArrayHandle,
								rgJobArray.crArrayName, NULL,"XXXX",llRowOld,"D");
			/*
			ilRC2 = CEDAArrayDeleteRow( &(rgJobArray.rrArrayHandle),
										rgJobArray.crArrayName, llRowOld );*/
			if ( ilRC2 != RC_SUCCESS )
			{
				/* mark for deletion */
				dbg ( TRACE, "UpdatePoolJobs: CEDAArrayPutField (Old, XXXX to D) failed, RC<%d>", ilRC2 );
			}
			else
			{
				/*llRowOld = ARR_CURRENT; */
				ilDeleted++;
			}
			llRowOld = ARR_NEXT;
		}

		ilExamined++;
		if ( ilRC2 != RC_SUCCESS )
			ilRC = RC_FAIL;
		/* test 
		llRowCount = ARR_CURRENT;
		if ( CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),	
									  &(rgJobArray.crArrayName[0]),
									  &(rgJobArray.rrIdx01Handle),
									  &(rgJobArray.crIdx01Name[0]),
									  "",&llRowCount, (void *) &pclOldRow ) == RC_SUCCESS )
		{
			dbg ( TRACE, "UpdatePoolJobs: End of Loop Current Row <%ld> URNO <%s> llRowOld <%d>",
				  llRowCount, JOBFIELD(pclOldRow,igJobURNO), llRowOld );
		}
		else
			dbg ( TRACE, "UpdatePoolJobs: End of Loop Current CEDAArrayFindRowPointer failed" );
		end of test */

	}
	llRowOld = ARR_LAST;
	ilFound = 0;
	while ( CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),	
									 &(rgJobArray.crArrayName[0]),
									&(rgJobArray.rrIdx01Handle),
									&(rgJobArray.crIdx01Name[0]),
									clKey,&llRowOld, (void *) &pclOldRow ) 
			== RC_SUCCESS )
	{		
		if ( strchr ( JOBFIELD(pclOldRow,igJobXXXX), 'D' ) )
		{
			ilFound++;
			ilRC2 = CEDAArrayDeleteRow( &(rgJobArray.rrArrayHandle),
										rgJobArray.crArrayName, llRowOld );
			if ( ilRC2 != RC_SUCCESS )
			{
				dbg ( TRACE, "UpdatePoolJobs: CEDAArrayDeleteRow (Old) failed, RC<%d>", ilRC2 );
			}
			else 
			{
				llRowOld2 = ARR_FIRST;
				while (CEDAArrayFindRowPointer( &(rgBrkArray.rrArrayHandle),
					&(rgBrkArray.crArrayName[0]),
					&(rgBrkArray.rrIdx01Handle),
					&(rgBrkArray.crIdx01Name[0]),
					JOBFIELD(pclOldRow,igJobURNO),&llRowOld2,
					(void *) &pclOldRow2 )  == RC_SUCCESS)
				{
					ilRC2 = CEDAArrayDeleteRow(&(rgBrkArray.rrArrayHandle),
					&(rgBrkArray.crArrayName[0]),llRowOld2);
					llRowOld2 = ARR_FIRST;
				}

			}
		}
		llRowOld = ARR_PREV;
	}
	if ( ilFound != ilDeleted )
		dbg ( TRACE, "UpdatePoolJobs: Expected to delete <%d> old rows, but found <%d> rows", 
			  ilDeleted, ilFound );
	dbg ( TRACE, "UpdatePoolJobs: Examined <%d> old rows <%d> deleted, <%d> remaining", 
		  ilExamined, ilDeleted, ilRemaining );
	CEDAArrayGetRowCount(&(rgNewJobsArray.rrArrayHandle),rgNewJobsArray.crArrayName,&llRowCount);
	dbg ( TRACE, "UpdatePoolJobs: rgNewJobsArray contains <%d> rows at the end", llRowCount );

	ilIndDisact = CEDAArrayActivateIndex ( &(rgJobArray.rrArrayHandle),	
										   &(rgJobArray.crArrayName[0]),
										   &(rgJobArray.rrIdx01Handle),
										   &(rgJobArray.crIdx01Name[0]));
	dbg ( DEBUG, "UpdatePoolJobs: CEDAArrayActivateIndex returns <%d>", ilIndDisact );		  		  

	return ilRC;
}

static int CopyNewPoolJobs ( char *pcpDrrRow )
{
	long	llFunc = ARR_FIRST, llWriteRow;
	char	*pclRow = 0;
	int		ilRC = RC_SUCCESS, ilRC2;
	char	clKey[11];


	strncpy(clKey,DRRFIELD(pcpDrrRow,igDrrURNO), 10);
	clKey[10] = '\0';
	TrimRight(clKey);

 	while ( CEDAArrayFindRowPointer( &(rgNewJobsArray.rrArrayHandle),
									&(rgNewJobsArray.crArrayName[0]),
									&(rgNewJobsArray.rrIdx01Handle),
									&(rgNewJobsArray.crIdx01Name[0]),
									clKey, &llFunc, (void *) &pclRow ) == RC_SUCCESS )         
	{
		llWriteRow = ARR_FIRST;
		ilRC2 = CEDAArrayAddRow(&rgJobArray.rrArrayHandle, 
								rgJobArray.crArrayName, &llWriteRow, (void*)pclRow);
		if ( ilRC2 != RC_SUCCESS )
		{
			dbg ( TRACE, "CopyNewPoolJobs: CEDAArrayAddRow failed Job-URNO <%s>, RC<%d>", 
				  JOBFIELD(pclRow,igJobURNO), ilRC2 );
			ilRC = ilRC2;
		}	
		else
			if ( bgBreak )
				CreateBreakJob(pclRow, pcpDrrRow );  

		llFunc = ARR_NEXT;
	}
	return ilRC;
}

static int IsDrrChangeImportant ( char *pcpFields, char *pcpData )
{
	int		ilNoOfItems, ilIdx, ilLoop, ilCol,ilPos, ilRC, ilImportant = TRUE; 
	char	clDrrUrno[21], *pclRow=0;
	long	llFunc = ARR_FIRST;
	char	clFina[11], clNew[81], clOld[81];
	char    clUnimportant[]="REMA,USEC,USEU,LSTU,CDAT,EXPF,BUFU,DRSF,SBLP";
	
	char  pclSqlBuf[1024]="\0";
	char pclDataArea[1024]="\0";
	short slCursor = 0;
	int ilGetRc;
	short slFkt;

	dbg ( DEBUG, "IsDrrChangeImportant: Fields <%s> Data <%s>", pcpFields, pcpData );
	
	ilRC = FindItemInList(pcpFields,"URNO",',',&ilIdx,&ilCol,&ilPos);
	if ( ilRC != RC_SUCCESS )
		dbg ( TRACE, "IsDrrChangeImportant: URNO not in Fieldlist" );
	else
	{
		dbg ( DEBUG, "IsDrrChangeImportant: found Index <%d> for URNO", ilIdx );
		if ( get_real_item(clDrrUrno,pcpData,ilIdx)<=0 )
		{
			ilRC = RC_FAIL;
			dbg ( TRACE, "IsDrrChangeImportant: URNO not filled" );
		}
		else
			dbg ( TRACE, "IsDrrChangeImportant: found URNO <%s> with idx <%d>",
				  clDrrUrno, ilIdx );	
	}
	if ( ilRC == RC_SUCCESS )
	{
		//check the urno in db first
		//with codition ROSL = '3' AND ROSS != 'L'"
		sprintf(pclSqlBuf,"SELECT URNO FROM DRRTAB WHERE URNO='%s'AND ROSL = '3' AND ROSS != 'L'",clDrrUrno);
	  dbg(TRACE,"IsDrrChangeImportant:check urno <%s>",pclSqlBuf);
	  slFkt = START;
	  ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pclDataArea);
	  close_my_cursor (&slCursor);
	  if(ilGetRc == DB_SUCCESS)
	  	{
	  		dbg(TRACE,"IsDrrChangeImportant:find urno in db,check rgDrrArray");
		ilRC = CEDAArrayFindRowPointer (&(rgDrrArray.rrArrayHandle),
										&(rgDrrArray.crArrayName[0]),
										&(rgDrrArray.rrIdx02Handle),
										&(rgDrrArray.crIdx02Name[0]),
										clDrrUrno ,&llFunc, (void *) &pclRow );
			}
			else
				{
					dbg(TRACE,"IsDrrChangeImportant:not found urno in db,no need check rgDrrArray again");
					ilRC = RC_FAIL;
					}
									
		if ( ilRC != RC_SUCCESS )
		{
			dbg ( TRACE, "IsDrrChangeImportant: DRR-record URNO <%s> not found", clDrrUrno );
			if ( ReloadDrrRecord ( pcpFields, pcpData ) == RC_SUCCESS )
			{	/* if record had not been loaded before, pooljobs have to be investigated */
				return TRUE;
			}
			else 
				return FALSE;
		}
		else
			dbg ( TRACE, "IsDrrChangeImportant: DRR-record URNO <%s> found in Row <%ld>", clDrrUrno, llFunc );
  }
	if ( ilRC == RC_SUCCESS )
	{
		ilNoOfItems = get_no_of_items(pcpFields);
		ilLoop = 1;
		ilImportant = FALSE;
		while(  !bgSwapShiftEvent && (ilLoop <= ilNoOfItems) )
		{
			get_real_item(clFina,pcpFields,ilLoop);
			if ( !strstr ( clUnimportant, clFina ) )	/* interested in this field ? */
			{
				get_real_item(clNew,pcpData,ilLoop);
				if ( FindItemInArrayList(cgDrrFields,clFina,',',&ilIdx,&ilCol,&ilPos) == RC_SUCCESS )
				{
					strcpy ( clOld, FIELDVAL((rgDrrArray),pclRow,ilIdx) );
					TrimRight ( clOld );
					if ( !strcmp ( clOld," " ) )
						clOld[0] = '\0';
					TrimRight ( clNew );
					if ( !strcmp ( clNew," " ) )
						clNew[0] = '\0';
					if ( strcmp ( clNew, clOld ) )  /* significant change */
					{
						if ( strcmp(clFina,"STFU") == 0)
						{
							dbg ( TRACE, "IsDrrChangeImportant: Field <%s> Old <%s> New <%s> different => Swap Shifts",
								  clFina, clOld, clNew );
							bgSwapShiftEvent = TRUE;
						}
						//added by MAX
						//check if change shift operation in opss-pm
						else if ( strcmp(clFina,"SCOD") == 0 )
						{
							dbg ( TRACE, "IsDrrChangeImportant: Field <%s> Old <%s> New <%s> different => change Shifts",
								  clFina, clOld, clNew );
							bgChangeShiftEvent = TRUE;
						}
						else
							dbg ( TRACE, "IsDrrChangeImportant: Field <%s> Old <%s> New <%s> are different",
								  clFina, clOld, clNew );
						ilImportant = TRUE;
					}
					else
						dbg ( DEBUG, "IsDrrChangeImportant: Field <%s> Old <%s> New <%s> are equal",
							  clFina, clOld, clNew );
		
				}
			}
			ilLoop++;
		}
	}
	//swap shift has high priorty
	if( bgSwapShiftEvent == TRUE)
		bgChangeShiftEvent == FALSE;
		
	dbg ( DEBUG, "IsDrrChangeImportant: changes significant <%d> Swap Shift <%d> change shift <%d>", ilImportant, bgSwapShiftEvent ,bgChangeShiftEvent);
	return ilImportant;
}

static char *GetAlidUrnoFromDrr(char *pcpDrrRow)
{
	int ilRc = RC_SUCCESS, i, j, k;
	long llFunc = ARR_FIRST;
	char *pclRow;
	char clKey[124], clKey2[24];

	*cgActualUalo = '\0';
	if ( pigDrrKey )
	{
		*clKey = '\0';
		strcpy(clKey,DRRFIELD(pcpDrrRow,*pigDrrKey) );	
		TrimRight(clKey);
		if (*clKey)
		{
			if ( !bgUseFunctionsOrgUnit )
			{
				strcpy(cgActualUalo,clKey);
				strncpy(cgActualUalo,GetAlidUrnoFromKey(clKey),10);
			}
			else
			{
				ilRc = CEDAArrayFindRowPointer(&rgPfcArray.rrArrayHandle,
											   rgPfcArray.crArrayName,
											   &rgPfcArray.rrIdx02Handle,
											   rgPfcArray.crIdx02Name, clKey,
											   &llFunc, (void*)&pclRow );
				if( ilRc == RC_SUCCESS )
				{
					strcpy ( clKey2, PFIELDVAL((&rgPfcArray),pclRow,igPfcDPTC) );
					dbg ( TRACE, "GetAlidUrnoFromDrr: Found Org.Unit <%s> for Function <%s>", 
						  clKey2, clKey );	
					TrimRight(clKey2);
					if ( *clKey2 )
					{
						sprintf (clKey, "%s,", clKey2 ); 
						if ( FindItemInList(cgOrgUnitsForPfcUse,clKey,',',&i,&j,&k) == RC_SUCCESS )
						{	
							dbg ( DEBUG, "GetAlidUrnoFromDrr: Found OrgUnit at position <%d,%d,%d>", i,j,k );
							strncpy(cgActualUalo,GetAlidUrnoFromKey(clKey2),10);
						}
						else
							dbg ( DEBUG, "GetAlidUrnoFromDrr: Don't use PFC.DPT1 for Org.Unit <%s>", clKey2 );
					}
				}
			}
		}
	}
 
	return cgActualUalo;
}

static int DeletePoolJobByUdrr ( char *pcpDrrUrno )
{
	int ilRc = RC_SUCCESS;
	long llRowNum = ARR_FIRST;
	long llRowNum2 = ARR_FIRST;
	char *pclPoolJobRow = NULL;
	char *pclBreakRow = NULL;
	BOOL blPoolJobFound = FALSE;
	BOOL blBreakFound = FALSE;
	long llDrrUrno = atol(pcpDrrUrno);
	char clKey[21];

	if(llDrrUrno > 0)
	{
		sprintf ( clKey, "%ld", llDrrUrno );
		dbg (DEBUG, "DeletePoolJobByUdrr: Key <%s>", clKey );

		while (CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),
						&(rgJobArray.crArrayName[0]),
						&(rgJobArray.rrIdx01Handle),
						&(rgJobArray.crIdx01Name[0]), clKey,&llRowNum,
						(void *) &pclPoolJobRow )  == RC_SUCCESS)
		{
			blPoolJobFound = TRUE;
			llRowNum2 = ARR_FIRST;
			while (CEDAArrayFindRowPointer( &(rgBrkArray.rrArrayHandle),
				&(rgBrkArray.crArrayName[0]),
				&(rgBrkArray.rrIdx01Handle),
				&(rgBrkArray.crIdx01Name[0]),
				JOBFIELD(pclPoolJobRow,igJobURNO),&llRowNum2,
				(void *) &pclBreakRow )  == RC_SUCCESS)
			{
				blBreakFound = TRUE;
				ilRc = CEDAArrayDeleteRow(&(rgBrkArray.rrArrayHandle),
				&(rgBrkArray.crArrayName[0]),llRowNum2);
				dbg ( DEBUG, "DeletePoolJobByUdrr: Delete Break <%ld> Result <%d>", llRowNum2, ilRc );
				llRowNum2 = ARR_FIRST;
			}
			ilRc = CEDAArrayDeleteRow(&(rgJobArray.rrArrayHandle),
			&(rgJobArray.crArrayName[0]),llRowNum);
			dbg ( DEBUG, "DeletePoolJobByUdrr: Delete Pooljob <%ld> Result <%d>", llRowNum, ilRc );
			llRowNum = ARR_FIRST;	
		}	
		if(blBreakFound )
		{
			if (CEDAArrayWriteDB(&rgBrkArray.rrArrayHandle,
									rgBrkArray.crArrayName, NULL, NULL, NULL, NULL,
									ARR_COMMIT_ALL_OK)
																	 != RC_SUCCESS)
			{
				dbg(TRACE, "DeletePoolJobByUdrr: CEDAArrayWriteDB Break error");
			}
		}
		if(blPoolJobFound)
		{
			if (CEDAArrayWriteDB(&rgJobArray.rrArrayHandle,
								rgJobArray.crArrayName, NULL, NULL, NULL, NULL,
								ARR_COMMIT_ALL_OK)
																 != RC_SUCCESS)
			{
				dbg(TRACE, "DeletePoolJobByUdrr: CEDAArrayWriteDB Job error");
			}
		}
		if ( !blBreakFound && !blPoolJobFound ) 
			ilRc = RC_NOTFOUND;
	}
	else
	{
		dbg(TRACE,"DeletePoolJobByUdrr. Invalid DrrUrno");
		ilRc = RC_FAIL;
	}
	return ilRc;
}
	
static int SetPooljobStatus ( char cpFcol, char *pcpUdsr )
{
	char *pclRow = 0;
	long llRowNum = ARR_FIRST;
	char clSTAT[11];
	int ilChanged = 0;
	static long llFieldNoSTAT = -1;

	if ( !pcpUdsr || (atol(pcpUdsr)==0) )
	{
		dbg ( TRACE, "SetPooljobStatus: Invalid Pooljob-URNO" );
		return 0;
	}

	while ( CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
									 &(rgJobArray.crArrayName[0]),
									 &(rgJobArray.rrIdx01Handle),
									 &(rgJobArray.crIdx01Name[0]), pcpUdsr,
									 &llRowNum, (void *) &pclRow ) == RC_SUCCESS ) 
	{
		sprintf ( clSTAT, "%-10s", JOBFIELD(pclRow,igJobSTAT));
		switch ( cpFcol )
		{
			case '0':
				clSTAT[0] = 'F';
				break;
			case '1':
				clSTAT[0] = 'C';
				break;
			case '4':
				clSTAT[4] = '0';
				break;
			case '5':
				clSTAT[4] = 'A';
				break;
		}
		dbg ( DEBUG, "SetPooljobStatus: Change Pooljob-Status URNO <%s> old <%s> new <%s>", 
					  JOBFIELD(pclRow,igJobURNO), JOBFIELD(pclRow,igJobSTAT), clSTAT );
		CEDAArrayPutField(&rgJobArray.rrArrayHandle, rgJobArray.crArrayName,
						  &llFieldNoSTAT,"STAT",llRowNum,clSTAT);
		llRowNum = ARR_NEXT;
		ilChanged ++;
	}
	return ilChanged;
}


static int CheckEmployeePresence ()
{
	char clNow[21], clStat[21];
	char clLastStart[21], clTemp[24];
	long llRowNum = ARR_FIRST;
	char *pclRow=0;
	int  ilChanged = 0, ilRC = RC_SUCCESS;
	int  ilOffset = atoi (cgSprOffsetTo);

	/* TimeToStr(clNow,time(NULL)); */
	GetServerTimeStamp("UTC",1,0,clNow);

	strcpy ( clLastStart, clNow );
	ilOffset *= -1;
	sprintf ( clTemp, "%d", ilOffset );
	StrAddTime(clLastStart,clTemp,clLastStart,0);
	dbg ( DEBUG, "CheckEmployeePresence: Now <%s> Offset <%s>",clNow, clTemp );

	dbg ( TRACE, "CheckEmployeePresence: Start before <%s> End after <%s>",
		  clLastStart, clNow );

	while ( CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
									 &(rgJobArray.crArrayName[0]),
									 &(rgJobArray.rrIdx04Handle),
									 &(rgJobArray.crIdx04Name[0]), cgUjty,
									 &llRowNum, (void *) &pclRow ) == RC_SUCCESS ) 
	{
		dbg ( DEBUG, "CheckEmployeePresence: Row <%ld> URNO <%s> PLFR <%s> PLTO <%s> STAT <%s>",
			  llRowNum, JOBFIELD(pclRow,igJobURNO), JOBFIELD(pclRow,igJobPLFR),
			  JOBFIELD(pclRow,igJobPLTO), JOBFIELD(pclRow,igJobSTAT) );
		if ( strcmp ( clLastStart, JOBFIELD(pclRow,igJobPLFR) ) < 0 ) 
		{
			dbg ( DEBUG, "CheckEmployeePresence: PLFR too new, Break loop" );
			break;
		}
		if ( strcmp ( clNow, JOBFIELD(pclRow,igJobPLTO) ) < 0 )
		{
			sprintf ( clStat, "%-10s", JOBFIELD(pclRow,igJobSTAT) );
			if ( (clStat[0] == 'P') && (clStat[4] != 'A') )
			{
				dbg ( DEBUG, "CheckEmployeePresence: Set Pooljob URNO <%s> to absent",
					  JOBFIELD(pclRow,igJobURNO) );
				ilChanged += SetPooljobStatus ( '5', JOBFIELD(pclRow,igJobUDSR) );
			}
			else
			{
				dbg ( DEBUG, "CheckEmployeePresence: Pooljob URNO <%s> still planned or already absent",
					  JOBFIELD(pclRow,igJobURNO) );
			}
		}
		else
		{
			dbg ( DEBUG, "CheckEmployeePresence: PLTO too old" );
		}
		llRowNum = ARR_NEXT;
	}
	if ( ilChanged > 0 )
	{
		dbg ( TRACE, "CheckEmployeePresence: Set %d Pooljobs to absent", ilChanged );
		ilRC = SavePoolJobs(FALSE,0,0);	
	}
	return ilRC;

}

static int UpdateDetachedJob ( char *pcpPoolJobRow, char * pcpField, char* pcpValue )
{
	int		ilJour, ilUdrd, ilUdel, ilFound, ilRc = RC_SUCCESS;
	long	llDetRow = ARR_FIRST;
	long	llFieldNo = -1;
	char	clKey2[64];
	char	*pclDetRow = 0;

	if ( !pcpPoolJobRow || !pcpField || !pcpValue )
	{
		dbg ( TRACE, "UpdateDetachedJob: At least one parameter NULL!" );
		return RC_WRONGDATA;
	}

	/* check whether it is a pooljob for a detached job type DET,DEL */
	ilJour = atoi ( JOBFIELD(pcpPoolJobRow,igJobJOUR) );
	ilUdrd = atoi ( JOBFIELD(pcpPoolJobRow,igJobUDRD) );
	ilUdel = atoi ( JOBFIELD(pcpPoolJobRow,igJobUDEL) );

	if ( (ilJour!=0 ) && (ilUdrd==0) && (ilUdel==0) )
	{	/* not the main pooljob, nor from daily roster deviations or delegations */
		sprintf (clKey2, "%s,%s,%s,%s", JOBFIELD(pcpPoolJobRow,igJobUDSR),
										JOBFIELD(pcpPoolJobRow,igJobUAID),
										JOBFIELD(pcpPoolJobRow,igJobPLFR),
										JOBFIELD(pcpPoolJobRow,igJobPLTO) );
		dbg ( DEBUG, "UpdateDetachedJob: searching for detached job with <UDSR,UAID,PLFR,PLTO> <%s>", clKey2 );
		ilFound = CEDAArrayFindRowPointer( &(rgDetArray.rrArrayHandle),	
										   &(rgDetArray.crArrayName[0]),
										   &(rgDetArray.rrIdx01Handle),
										   &(rgDetArray.crIdx01Name[0]),
										   clKey2, &llDetRow, (void*)&pclDetRow );
		if ( ilFound == RC_SUCCESS )
		{
			dbg ( TRACE, "UpdateDetachedJob: Found detached job URNO <%s> for pool job URNO <%s>",
						JOBFIELD(pclDetRow,igJobURNO), JOBFIELD(pcpPoolJobRow,igJobURNO) );
			/*  set pcpField (ACFR or ACTO) to pcpValue, so that times for	*/
			/*  detached job and corresponding pooljob remain the same		*/
			ilRc = CEDAArrayPutField ( &rgDetArray.rrArrayHandle,
									   rgDetArray.crArrayName,
									   &llFieldNo,pcpField,llDetRow,pcpValue );
			if ( ilRc != RC_SUCCESS )
				dbg ( TRACE, "UpdateDetachedJob: CEDAArrayPutField failed <%d>", ilRc );
			else
			{

				ilRc = CEDAArrayWriteDB ( &rgDetArray.rrArrayHandle, 
										  rgDetArray.crArrayName, NULL, NULL, 
										  NULL, NULL, ARR_COMMIT_ALL_OK);
				if ( ilRc != RC_SUCCESS )
					dbg ( TRACE, "CEDAArrayWriteDB: CEDAArrayWriteDB failed <%d>", ilRc );
				else
				{
					commit_work();
					dbg ( TRACE, "UpdateDetachedJob: One job updated" );
				}
			}
		}
	}
	return ilRc;
}

static void FillAbsJob ( char *pcpAbsJob, char *pcpFrom, char *pcpTo, char *pcpUdsr, 
					     char *pcpUdel, char *pcpParentJob )
{
	char *pclTmpBuf;
	char clFieldName[24], clPool[24];
	int  ilLc;

	InitializeNewPooljob(cgNewJobBuf);
	
	dbg ( DEBUG,"New temporary absent Job Urno <%s>",
		  JOBFIELD(cgNewJobBuf,igJobURNO));

	strcpy(JOBFIELD(cgNewJobBuf,igJobUAID),JOBFIELD(pcpParentJob,igJobUAID));
	if ( (igJobALID>=0) && (GetPoolName ( JOBFIELD(cgNewJobBuf,igJobUAID), clPool) == RC_SUCCESS ) )
	{
		clPool[14]='\0';
		strcpy(JOBFIELD(cgNewJobBuf,igJobALID),clPool );	
	}

	strncpy(JOBFIELD(cgNewJobBuf,igJobACFR),pcpFrom,14);
	strncpy(JOBFIELD(cgNewJobBuf,igJobACTO),pcpTo,14);
	strncpy(JOBFIELD(cgNewJobBuf,igJobPLFR),pcpFrom,14);
	strncpy(JOBFIELD(cgNewJobBuf,igJobPLTO),pcpTo,14);
	strncpy(JOBFIELD(cgNewJobBuf,igJobDETY)," ",1);
	strncpy(JOBFIELD(cgNewJobBuf,igJobHOPO),JOBFIELD(pcpParentJob,igJobHOPO),3);
	strncpy(JOBFIELD(cgNewJobBuf,igJobJOUR),JOBFIELD(pcpParentJob,igJobURNO),10);
	strncpy(JOBFIELD(cgNewJobBuf,igJobSTAT),"P        ",10);
	strncpy(JOBFIELD(cgNewJobBuf,igJobUDSR), pcpUdsr,10);
	strncpy(JOBFIELD(cgNewJobBuf,igJobUDEL), pcpUdel,10);
	dbg(TRACE,"%05d:UDSR=<%s>",__LINE__,JOBFIELD(cgNewJobBuf,igJobUDSR));

	strncpy(JOBFIELD(cgNewJobBuf,igJobUJTY),cgUjtyAbs,10);
	strncpy(JOBFIELD(cgNewJobBuf,igJobUSTF),JOBFIELD(pcpParentJob,igJobUSTF),10);
	if (bgUseFilt)
	{
		strncpy(JOBFIELD(cgNewJobBuf,igJobFILT),JOBFIELD(pcpParentJob,igJobFILT),10);
	}

	if ( igJobFCCO >= 0 )
		strcpy(JOBFIELD(cgNewJobBuf,igJobFCCO),JOBFIELD(pcpParentJob,igJobFCCO) );
	if ( igJobWGPC >= 0 )
		strcpy(JOBFIELD(cgNewJobBuf,igJobWGPC),JOBFIELD(pcpParentJob,igJobWGPC) );

	*pcpAbsJob = '\0';
	pclTmpBuf = pcpAbsJob;
	for(ilLc = 0; ilLc < rgJobArray.lrArrayFieldCnt; ilLc++)
	{
		*clFieldName = '\0';
		GetDataItem(clFieldName,rgJobArray.crArrayFieldList,
					ilLc+1,',',"","\0\0");
		strcpy(pclTmpBuf,JOBFIELD(cgNewJobBuf,ilLc)); 
		dbg(TRACE,"%05d:%s <%s> <%s>",__LINE__,
			clFieldName,JOBFIELD(cgNewJobBuf,ilLc),pclTmpBuf);
		pclTmpBuf += strlen(pclTmpBuf)+1;
	}
}

static int AddOneDayLocal ( char *pcpActDay, char *pcpNextDay )
{
	char clTmp[15];

	if ( !pcpActDay || !pcpNextDay || ( strlen (pcpActDay) < 14)  )
		return RC_INVALID;
	strncpy ( clTmp, pcpActDay, 8 );
	clTmp[8] = '\0';
	strcat ( clTmp, "000000" );
	AddSecsToCEDATime(clTmp,(time_t)(30*3600),0 );
	clTmp[8] = '\0';
	strcat ( clTmp, &(pcpActDay[8]) );
	strcpy ( pcpNextDay, clTmp );
	dbg  ( DEBUG, "AddOneDayLocal: ActDay <%s> NextDay <%s>", pcpActDay, pcpNextDay );
	return RC_SUCCESS ;
}

void ReplaceChar ( char *pcpStr, char cpOld, char cpNew )
{
	char *pclPos, *pclAct;
	pclAct = pcpStr;
	while ( pclAct )
	{
		if ( pclPos = strchr ( pclAct, cpOld ) ) 
			*pclPos = cpNew;
		pclAct = pclPos;
	}
}

void UndoCharMapping ( char* pcpString )
{
	unsigned int ilLen, i;

	ilLen = min ( strlen(cgClientChars), strlen(cgServerChars) );
	for ( i=0; i<ilLen; i++ )
		ReplaceChar ( pcpString, cgServerChars[i], cgClientChars[i] ) ;
}


static int HandleRelDrr ( char *pcpBCData )
{
	int		ilRC, ilItemNo=1, ilLen;
	char	*pclBuff;
	char	clDel='-';
	char    clStart[15]="", clEnd[15]="", clRelLevels[11], *pclSelection;
	int     ilRC1, ilRC2, ilLevel1=0, ilLevel2=0;
	BOOL	blDone = FALSE, blOverlap = FALSE;
	char    clSday1[9]="", clSday2[9]="", clUstf[11], clUdrr[11];
	long	llRowCount=0, llRowNum = ARR_FIRST, llFunc;
	char	*pclRow=0, *pclOldRow=0;

	if ( !pcpBCData )
		return RC_FAIL;
	ilRC = RC_SUCCESS;
	dbg(TRACE,"HandleRelDrr: Data <%s>", pcpBCData );

	pclBuff = (char*)calloc ( 1, strlen(pcpBCData)+1 ) ;
	ilLen = strlen(pcpBCData) * 3 / 2 + 130;
	pclSelection = (char*)calloc ( 1, ilLen ) ;
	if ( !pclBuff || !pclSelection )
	{
		dbg(TRACE,"HandleRelDrr: calloc of <%d> bytes failed", ilLen );
		if ( pclBuff )
			free ( pclBuff );
		return RC_NOMEM;
	}
	else
		dbg (DEBUG, "HandleRelDrr: Data len <%d> now allocated <%d>", 
					strlen(pcpBCData), ilLen );

	/* Check release levels, maybe nothing to be done */
	if ( GetDataItem(pclBuff, pcpBCData, 4, clDel, "", "\0\0") > 0 )
	{
		if ( strlen (pclBuff) > 10 )
			pclBuff[10] = '\0';		/* to avoid buffer overflow */
		UndoCharMapping ( pclBuff );
		ilRC1 = GetDataItem(clRelLevels, pclBuff, 1, ',', "", "\0\0");
		if ( ilRC1 > 0 )
			ilLevel1 = atoi ( clRelLevels );
		ilRC2 = GetDataItem(clRelLevels, pclBuff, 2, ',', "", "\0\0");
		if ( ilRC2 > 0 )
			ilLevel2 = atoi ( clRelLevels );
		dbg(TRACE,"HandleRelDrr: Found Release <%d>--><%d>", ilLevel1, ilLevel2 );
		if ( !ilLevel1 || !ilLevel2 )	/* both release levels found ? */
			ilRC = RC_NOT_FOUND;
		else
		{	/* if not released to daily list, nothing to do */
			if ( ilLevel2 != 3 )
				blDone = TRUE;
		}
	}
	else
	{
		dbg(TRACE,"HandleRelDrr: wrong parameter count" );
		return RC_FAIL;
	}
	
	/* Check release dates, maybe nothing to be done */
	if ( !blDone && (ilRC == RC_SUCCESS) )
	{
		if ( GetDataItem(pclBuff, pcpBCData, 1, clDel, "", "\0\0") > 0)
		{
			strncpy ( clStart, pclBuff, 8 );
			clStart[8] = '\0';
			strcpy ( clSday1, clStart );
		}
		if ( GetDataItem(pclBuff, pcpBCData, 2, clDel, "", "\0\0") > 0)
		{
			strncpy ( clEnd, pclBuff, 8 );
			clEnd[8] = '\0';
			strcpy ( clSday2, clEnd );
		}
		if ( (strlen(clEnd)<8) || (strlen (clStart)<8) )
			ilRC = RC_INIT_FAIL;
	}
	if ( !blDone && (ilRC == RC_SUCCESS) )
	{
		strcat ( clEnd, "235959" );
		strcat ( clStart, "000000" );
		LocalToUtc ( clStart );
		LocalToUtc ( clEnd );
		blOverlap = ((strcmp(clStart,cgOpEnd) <= 0) && (strcmp(clEnd,cgOpStart) >= 0));
		dbg ( TRACE, "HandleRelDrr: Start <%s> End <%s> overlaps timeframe ? <%d>", 
			  clStart, clEnd, blOverlap );
		if ( !blOverlap )
			blDone = TRUE;
	}
	if ( !blDone && (ilRC == RC_SUCCESS) )
	{
		if ( GetDataItem(pclBuff, pcpBCData, 3, clDel, "", "\0\0") > 0)
		{
			UndoCharMapping ( pclBuff );

			/* don't load days not included in the loaded timeframe */
			if ( strcmp ( clSday1, cgOpFirstDay ) < 0 )
				strcpy ( clSday1, cgOpFirstDay );
			if ( strcmp ( cgOpLastDay, clSday2 ) < 0 )
				strcpy ( clSday2, cgOpLastDay );

			sprintf(pclSelection,
					"WHERE SDAY BETWEEN '%s' AND '%s' AND HOPO = '%s' AND ROSL = '3' AND ROSS != 'L' AND STFU IN (", clSday1, clSday2, cgHopo );
			while(GetDataItem(clUstf, pclBuff, ilItemNo, ',', "", "\0\0") > 0)
			{
				ilLen = strlen (pclSelection);
				if ( ilItemNo > 1 )
					sprintf ( &(pclSelection[ilLen]), ",'%s'", clUstf );
				else
					sprintf ( &(pclSelection[ilLen]), "'%s'", clUstf );
				ilItemNo++;
			}
			strcat ( pclSelection, ")" );
			dbg ( TRACE, "HandleRelDrr: Reloading DRRTAB <%s>", pclSelection );
			ilRC = CEDAArrayRefillHint(&rgNewDrrArray.rrArrayHandle,rgNewDrrArray.crArrayName,
								   pclSelection, NULL, ARR_FIRST, cgDrrReloadHint );
			dbg(TRACE,"HandleRelDrr: NEWDRR Refill RC=%d",ilRC);
		}
		else
			ilRC = RC_INIT_FAIL;
	}	
	if ( !blDone && (ilRC == RC_SUCCESS) )
	{
		CEDAArrayGetRowCount(&rgNewDrrArray.rrArrayHandle,
							 rgNewDrrArray.crArrayName,&llRowCount);
		dbg(TRACE,"HandleRelDrr: %ld records read in NEWDRR", llRowCount );
		/* delete all old DRR records in rgDrrArray with the same URNOs */
		while ( CEDAArrayGetRowPointer(&rgNewDrrArray.rrArrayHandle,
									   rgNewDrrArray.crArrayName,
									   llRowNum,(void *)&pclRow) == RC_SUCCESS )
		{	
			strcpy ( clUdrr, DRRFIELD(pclRow,igDrrURNO)	);
			TrimRight ( clUdrr );
			dbg ( TRACE, "HandleRelDrr: Checking for old DRR record with URNO <%s>", clUdrr ); 
			llFunc = ARR_FIRST;
			if ( CEDAArrayFindRowPointer( &(rgDrrArray.rrArrayHandle),
										  rgDrrArray.crArrayName,
										  &(rgDrrArray.rrIdx02Handle),
										  rgDrrArray.crIdx02Name,
										  clUdrr, &llFunc, (void *) &pclOldRow ) == RC_SUCCESS )
			{
				/* Old Record with same URNO found -> delete record insert new one */
				ilRC1 = AATArrayDeleteRow(&(rgDrrArray.rrArrayHandle),
										  rgDrrArray.crArrayName, llFunc );
				dbg ( TRACE, "HandleRelDrr: AATArrayDeleteRow on old DRR-record RC <%d>", ilRC1 ); 
			}		
			llRowNum = ARR_NEXT;
			/* copy record from rgNewDrrArray to rgDrrArray instead of 2nd loading of records in prior versions */
			llFunc = ARR_FIRST;
			ilRC1 = AATArrayAddRow( &rgDrrArray.rrArrayHandle, rgDrrArray.crArrayName,
								   &llFunc, (void *)pclRow );
			if ( ilRC != RC_SUCCESS )
				dbg ( TRACE, "HandleRelDrr: AATArrayAddRow failed RC <%d>", ilRC1 );
		}
	}
	/*  RELDRR must be handled also from operative polhdl, but only to load 
		DRR-records, pooljobs are created by release version				*/
	if ( !blDone && (ilRC == RC_SUCCESS) && (igVersion & V_RELEASES) )
	{
		llRowNum = ARR_FIRST;
		while ( CEDAArrayGetRowPointer(&rgNewDrrArray.rrArrayHandle,
									   rgNewDrrArray.crArrayName,
									   llRowNum,(void *)&pclRow) == RC_SUCCESS )
		{	
			strcpy ( clUdrr, DRRFIELD(pclRow,igDrrURNO)	);
			TrimRight ( clUdrr );
			ilRC1 = ProcessDrrChange ( clUdrr, FALSE );
			dbg ( TRACE, "HandleRelDrr: ProcessDrrChange URNO <%s> RC <%d>", clUdrr, ilRC1 ); 
			llRowNum = ARR_NEXT;
		}
		SavePoolJobs(bgRELJOBtoACTION || bgRELJOBtoBCHDL, clSday1, clSday2 );
	}
	if ( pclBuff )
		free ( pclBuff );
	if ( pclSelection )
		free ( pclSelection );
	return ilRC;
}
static int ProcessChangeShift ( char *pcpDrrUrno, BOOL bpSaveJobs )
{
	BOOL blCheckBreak = FALSE;
	int	 ilRC =	RC_SUCCESS;
	long llFunc = ARR_FIRST;
	char *pclRow          = NULL;
	
	//
	char  pclSqlBuf[1024]="\0";
	char pclDataArea[1024]="\0";
	short slCursor = 0;
	int ilGetRc;
	short slFkt;
	
	dbg ( TRACE, "ProcessChangeShift: drr urno <%s>", pcpDrrUrno );
	if (pcpDrrUrno != NULL)
	{
		//check the urno in db first
		//with codition ROSL = '3' AND ROSS != 'L'"
		sprintf(pclSqlBuf,"SELECT URNO FROM DRRTAB WHERE URNO='%s'AND ROSL = '3' AND ROSS != 'L'",pcpDrrUrno);
	  dbg(TRACE,"ProcessChangeShift:check urno <%s>",pclSqlBuf);
	  slFkt = START;
	  ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pclDataArea);
	  close_my_cursor (&slCursor);
	  dbg(TRACE,"ProcessChangeShift:check urno result <%d>",ilGetRc);
	}
	if(ilGetRc != DB_SUCCESS)
	{
		ilRC = RC_NOT_FOUND;
	  return ilRC;
	}
		
	if(CEDAArrayFindRowPointer( &(rgDrrArray.rrArrayHandle),
								&(rgDrrArray.crArrayName[0]),
								&(rgDrrArray.rrIdx02Handle),
								&(rgDrrArray.crIdx02Name[0]),
								pcpDrrUrno,&llFunc, (void *) &pclRow ) == RC_SUCCESS)
	{
		dbg(TRACE,"ProcessChangeShift: DRR-URNO <%s>",DRRFIELD(pclRow,igDrrURNO),10);
				
		if (newCheckShiftCode(DRRFIELD(pclRow,igDrrURNO)) == RC_FAIL)
		{
			dbg(TRACE,"ProcessChangeShift: newCheckShiftCode is RC_FAIL");
			blCheckBreak = TRUE;
			DeleteSplittedPoolJobs(DRRFIELD(pclRow,igDrrURNO));
		}
		CreatePoolJobsForShift(pclRow);
		if(blCheckBreak && bgBreak)
		{
			CheckBreak(pclRow);
		}
		if ( bpSaveJobs )
			SavePoolJobs(FALSE,0,0);
	}
	else
		ilRC = RC_NOT_FOUND;
	return ilRC;
}
static int newCheckShiftCode(char *pcpDrrUrno)
{
	int ilRc = RC_SUCCESS;
	long llRowNum = ARR_FIRST;
	long llRowNum2 = ARR_FIRST;
	long llRowNum3 = ARR_FIRST;
	char *pclPoolJobRow = NULL;
	char *pclDrrRow = NULL;
	char *pclBreakRow = NULL;
	char clDrrScod[10];
	char clJobScod[10];
	char myDrrUrno[20]="\0";
	BOOL blWriteToDB = FALSE;

  memset(myJobUrno,0x00,20);
  
  dbg(TRACE, "newCheckShiftCode: pcpDrrUrno <%s>",pcpDrrUrno);
  sprintf(myDrrUrno,"%s",pcpDrrUrno);
  dbg(TRACE, "newCheckShiftCode: myDrrUrno <%s>",myDrrUrno);
  TrimRight(myDrrUrno);
  dbg(TRACE, "newCheckShiftCode: TrimRight myDrrUrno <%s>",myDrrUrno);
	if (CEDAArrayFindRowPointer( &(rgDrrArray.rrArrayHandle),
			&(rgDrrArray.crArrayName[0]),
			&(rgDrrArray.rrIdx02Handle),
			&(rgDrrArray.crIdx02Name[0]),
			pcpDrrUrno,&llRowNum,
			(void *) &pclDrrRow )  == RC_SUCCESS)
	{
		llRowNum2 = ARR_FIRST;
		strcpy(clDrrScod,DRRFIELD(pclDrrRow,igDrrSCOD));
		TrimRight(clDrrScod);
		dbg(TRACE, "newCheckShiftCode SCOD <%s>",clDrrScod);
		while(CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),
			&(rgJobArray.crArrayName[0]),
			&(rgJobArray.rrIdx01Handle),
			&(rgJobArray.crIdx01Name[0]),
			//pcpDrrUrno,&llRowNum2,
			myDrrUrno,&llRowNum2,
			(void *) &pclPoolJobRow )  == RC_SUCCESS)
		{
			
			dbg(TRACE, "newCheckShiftCode find job <%s>",JOBFIELD(pclPoolJobRow,igJobURNO));
			sprintf(myJobUrno,"%s",JOBFIELD(pclPoolJobRow,igJobURNO));
			dbg(TRACE, "newCheckShiftCode: myJobUrno <%s>",myJobUrno);
			strcpy(clJobScod,JOBFIELD(pclPoolJobRow,igJobSCOD));
			TrimRight(clJobScod);
			dbg(TRACE, "newCheckShiftCode JobSCOD <%s>",clJobScod);

			if(strlen(clJobScod) > 1)
			{
				if(strcmp(clJobScod,clDrrScod) != 0)
				{
					ilRc = RC_FAIL;
					llRowNum3 = ARR_FIRST;
					while(CEDAArrayFindRowPointer( &(rgBrkArray.rrArrayHandle),
						&(rgBrkArray.crArrayName[0]),
						&(rgBrkArray.rrIdx01Handle),
						&(rgBrkArray.crIdx01Name[0]),
						JOBFIELD(pclPoolJobRow,igJobURNO),&llRowNum3,
						(void *) &pclBreakRow )  == RC_SUCCESS)
					{
						dbg(TRACE, "newCheckShiftCode: check break");
						if(atol(JOBFIELD(pclBreakRow,igJobUJTY)) == atol(cgUjtyBrk))
						{
							dbg(TRACE, "newCheckShiftCode: delete break");
							CEDAArrayDeleteRow(&(rgBrkArray.rrArrayHandle),
							&(rgBrkArray.crArrayName[0]),llRowNum3);
							llRowNum3 = ARR_FIRST;
							blWriteToDB = TRUE;
						}
					}

				}
			}
			CEDAArrayPutField(&rgJobArray.rrArrayHandle,
							rgJobArray.crArrayName,
								NULL,"SCOD",llRowNum2,
								DRRFIELD(pclDrrRow,igDrrSCOD));
		
		  //change poolsjob 's acfr and acto ,according to new shift
		  //I check it whether is pool or not first,but in fact no need, as in rgJobArray only 
		  //store poolsjob.
		  //you may ask when real job created in opss-pm , polhdl still can recive the IRT cmd
		  //but you can find new job never inserted to rgJobArray
		  if (strncmp(JOBFIELD(pclPoolJobRow,igJobALOC),"POOL",4) == 0)
		  {
		  	char myACFR[30]="\0";
		  	char myACTO[30]="\0";
		  	
		  	dbg(TRACE, "newCheckShiftCode: change poolsjob acfr,acto");
		  	
		  	
		  				
		  	strcpy(myACFR,DRRFIELD(pclDrrRow,igDrrAVFR));
		  	dbg(TRACE, "newCheckShiftCode: old acfr <%s>, new local avfr<%s>",JOBFIELD(pclPoolJobRow,igJobACFR),myACFR);
		  	LocalToUtc(myACFR);
		  	dbg(TRACE, "newCheckShiftCode: old acfr <%s>, new UTC avfr<%s>",JOBFIELD(pclPoolJobRow,igJobACFR),myACFR);
		  	
		  	strcpy(myACTO , DRRFIELD(pclDrrRow,igDrrAVTO));
		  	dbg(TRACE, "newCheckShiftCode: old acto <%s>, new local acto<%s>",JOBFIELD(pclPoolJobRow,igJobACTO),myACTO);
		  	LocalToUtc(myACTO);
		  	dbg(TRACE, "newCheckShiftCode: old acto <%s>, new UTC acto<%s>",JOBFIELD(pclPoolJobRow,igJobACTO),myACTO);
		  				CEDAArrayPutField(&rgJobArray.rrArrayHandle,
							rgJobArray.crArrayName,
								NULL,"ACFR",llRowNum2,
								myACFR);
								
							CEDAArrayPutField(&rgJobArray.rrArrayHandle,
							rgJobArray.crArrayName,
								NULL,"ACTO",llRowNum2,
								myACTO);
								
							if( CEDAArrayWriteDB(&rgJobArray.rrArrayHandle,
								rgJobArray.crArrayName, NULL, NULL, NULL, NULL,
								ARR_COMMIT_ALL_OK) != RC_SUCCESS)
								dbg(TRACE, "newCheckShiftCode: poolsjob writed to db");
				
				//check poolsjob's job ,if exceess the time scope of pool, send job urno to jobhdl to delete the job
//				char  pclSqlBuf[1024]="\0";
//				char pclDataArea[1024]="\0";
//				char mySelection[100]="\0";
//				short slCursor = 0;
//	      int ilGetRc;
//	      short slFkt;
//	      slFkt = START;
//				sprintf(pclSqlBuf,"SELECT URNO FROM JOBTAB WHERE (ACFR<'%s' AND UDSR ='%s' AND ALOC <>'POOL' AND UJTY <> '2011') OR (ACTO>'%s' AND UDSR ='%s' AND ALOC <>'POOL' AND UJTY <> '2011') ",
//				myACFR,DRRFIELD(pclDrrRow,igDrrURNO),myACTO,DRRFIELD(pclDrrRow,igDrrURNO));
//				dbg(TRACE, "newCheckShiftCode: pclSqlBuf <%s>",pclSqlBuf);
//				
//				while ((ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pclDataArea)) == DB_SUCCESS)
//				{
//					dbg(TRACE, "newCheckShiftCode: pclDataArea <%s>",pclDataArea);
					//sprintf(mySelection,"WHERE URNO = '%s'",pclDataArea);
					dbg(TRACE, "newCheckShiftCode: myJobUrno <%s>",myJobUrno);
					tools_send_info_flag( 9100,mod_id, "JOBHDL", "POLHDL", "", "", 
                                             "", "", "polhdl", "DRTP","JOBTAB",myJobUrno,
                                             "URNO",myJobUrno,0 );
          //memset(mySelection,0x00,100);
//          slFkt = NEXT;
//				}
//				close_my_cursor (&slCursor);
			}						
			llRowNum2 = ARR_NEXT;
		}

	}
	if(blWriteToDB)
	{
		CEDAArrayWriteDB(&rgBrkArray.rrArrayHandle,
								rgBrkArray.crArrayName, NULL, NULL, NULL, NULL,
								ARR_COMMIT_ALL_OK);
	}
	return ilRc;
}
static int ProcessDrrChange ( char *pcpDrrUrno, BOOL bpSaveJobs )
{
	BOOL blCheckBreak = FALSE;
	int	 ilRC =	RC_SUCCESS;
	long llFunc = ARR_FIRST;
	char *pclRow          = NULL;
	
	//
	char  pclSqlBuf[1024]="\0";
	char pclDataArea[1024]="\0";
	short slCursor = 0;
	int ilGetRc;
	short slFkt;
	
	dbg ( TRACE, "ProcessDrrChange: drr urno <%s>", pcpDrrUrno );
	if (pcpDrrUrno != NULL)
	{
		//check the urno in db first
		//with codition ROSL = '3' AND ROSS != 'L'"
		sprintf(pclSqlBuf,"SELECT URNO FROM DRRTAB WHERE URNO='%s'AND ROSL = '3' AND ROSS != 'L'",pcpDrrUrno);
	  dbg(TRACE,"ProcessDrrChange:check urno <%s>",pclSqlBuf);
	  slFkt = START;
	  ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pclDataArea);
	  close_my_cursor (&slCursor);
	  dbg(TRACE,"ProcessDrrChange:check urno result <%d>",ilGetRc);
	}
	if(ilGetRc != DB_SUCCESS)
	{
		ilRC = RC_NOT_FOUND;
	  return ilRC;
	}
		
	if(CEDAArrayFindRowPointer( &(rgDrrArray.rrArrayHandle),
								&(rgDrrArray.crArrayName[0]),
								&(rgDrrArray.rrIdx02Handle),
								&(rgDrrArray.crIdx02Name[0]),
								pcpDrrUrno,&llFunc, (void *) &pclRow ) == RC_SUCCESS)
	{
		dbg(TRACE,"ProcessDrrChange: DRR-URNO <%s>",DRRFIELD(pclRow,igDrrURNO),10);
				
		if (CheckShiftCode(DRRFIELD(pclRow,igDrrURNO)) == RC_FAIL)
		{
			dbg(TRACE,"ProcessDrrChange: CheckShiftCode is RC_FAIL");
			blCheckBreak = TRUE;
			DeleteSplittedPoolJobs(DRRFIELD(pclRow,igDrrURNO));
		}
		CreatePoolJobsForShift(pclRow);
		if(blCheckBreak && bgBreak)
		{
			CheckBreak(pclRow);
		}
		if ( bpSaveJobs )
			SavePoolJobs(FALSE,0,0);
	}
	else
		ilRC = RC_NOT_FOUND;
	return ilRC;
}

/*
static int CopyNewDrrRecord ( long lpRowNum )	
{
	int		ilRC = RC_SUCCESS;
	long	llRowWrite = ARR_FIRST;
	long	*pllFields=0;

	pllFields = (long*)calloc( rgNewDrrArray.lrArrayFieldCnt, sizeof(long) );
	if ( !pllFields )
	{
		dbg ( TRACE, "CopyNewDrrRecords: calloc failed" );
		return RC_NOMEM;
	}
	pllFields[0] = -1;

	memset ( rgDrrArray.pcrArrayRowBuf, 0, sizeof(rgDrrArray.pcrArrayRowBuf) );

	ilRC = CEDAArrayGetFields( &rgNewDrrArray.rrArrayHandle,
							   rgNewDrrArray.crArrayName, pllFields, 
				  			   rgNewDrrArray.crArrayFieldList, ',',
							   rgNewDrrArray.lrArrayRowLen, lpRowNum,
							   (void*)rgNewDrrArray.pcrArrayRowBuf);
	if ( ilRC == RC_SUCCESS )
	{
		ilRC = CEDAArrayAddRow( &rgDrrArray.rrArrayHandle, rgDrrArray.crArrayName,
								&llRowWrite, (void *)rgDrrArray.pcrArrayRowBuf);
		if ( ilRC != RC_SUCCESS )
			dbg ( TRACE, "CopyNewDrrRecord: CEDAArrayAddRow failed RC <%d>", ilRC );
		else
		{
			ilRC = CEDAArrayPutFields( &rgDrrArray.rrArrayHandle,
									   rgDrrArray.crArrayName, 0, &llRowWrite, 
									   rgDrrArray.crArrayFieldList,
									   rgNewDrrArray.pcrArrayRowBuf );	
			if ( ilRC != RC_SUCCESS )
				dbg ( TRACE, "CopyNewDrrRecord: CEDAArrayPutFields failed RC <%d>", ilRC );
		}
	}

	if ( pllFields )
		free ( pllFields  );
	return ilRC;
}
*/

static int IniFwdEventArray ()
{
	char clTmp[101], clTable[33], clModids[101], clOneId[21];
	int	 ilTables, ilModids, ilIdx=0, ilActId;

	memset ( &rgForwards, 0, sizeof (rgForwards) );

	if(ReadConfigEntry("MAIN", "FwdEventOnTable",pcgCfgBuffer) == RC_SUCCESS)
	{
		ilTables = 1;
		while ( (ilIdx<MAX_FORWARDS) && 
			    ( GetDataItem(clTmp,pcgCfgBuffer,ilTables,';',"","\0\0") > 0) )
		{
			dbg ( DEBUG, "IniFwdEventArray: %d. entry <%s>", ilTables, clTmp );
			if ( GetDataItem(clTable,clTmp,1,':',""," \0") >= 0 ) 
			{
				if (GetDataItem(clModids,clTmp,2,':',""," \0") >= 0 ) 
				{
					dbg ( DEBUG, "IniFwdEventArray: Table <%s> ModIds <%s>", clTable, clModids );
					ilModids = 1;
					while ( (ilIdx<MAX_FORWARDS) && 
						    ( GetDataItem(clOneId,clModids,ilModids,',',""," \0") > 0) )
					{
						if ( sscanf ( clOneId, "%d", &ilActId ) >= 1 )
						{	
							strcpy ( rgForwards[ilIdx].table, clTable );
							rgForwards[ilIdx].modid = ilActId ;
							dbg ( DEBUG, "IniFwdEventArray: Added Idx <%d> Table <%s> ModId <%d>", 
								  ilIdx, rgForwards[ilIdx].table, rgForwards[ilIdx].modid );
							ilIdx ++;
						}
						else 
						{
							dbg ( DEBUG, "IniFwdEventArray: Unable to scan ModId from <%s>", 
								  clOneId );
							break;
							
						}
						ilModids++; 
					}
				}
			}	
			ilTables++;
		}
	}
	return ilIdx;
}

static int ReloadDrrRecord ( char *pcpFields, char *pcpData )
{
	char clUrno[11]="";
	char clRosl[2]="";
	char clRoss[2]="";
	char clSday[9]="";
	int	 ilIdx, ilCol,ilPos, ilRC; 
	char	clSelection[41];
	
	ilRC = FindItemInList(pcpFields,"URNO",',',&ilIdx,&ilCol,&ilPos);
	if ( ilRC == RC_SUCCESS )
	{
		get_real_item(clUrno,pcpData,ilIdx);
		ilRC = FindItemInList(pcpFields,"ROSS",',',&ilIdx,&ilCol,&ilPos);
	}
	if ( ilRC == RC_SUCCESS )
	{
		get_real_item(clRoss,pcpData,ilIdx);
		ilRC = FindItemInList(pcpFields,"ROSL",',',&ilIdx,&ilCol,&ilPos);
	}
	if ( ilRC == RC_SUCCESS )
	{
		get_real_item(clRosl,pcpData,ilIdx);
		ilRC = FindItemInList(pcpFields,"SDAY",',',&ilIdx,&ilCol,&ilPos);
	}
	if ( ilRC == RC_SUCCESS )
	{
		get_real_item(clSday,pcpData,ilIdx);
	}
	dbg ( DEBUG, "ReloadDrrRecord: Actual values URNO <%s> ROSS <%s> ROSL <%s> SDAY <%s>", 
		  clUrno, clRoss, clRosl, clSday );
	if ( (ilRC != RC_SUCCESS) || !clUrno[0] || !clRoss[0] || !clRosl[0] || !clSday[0] )
	{
		dbg ( TRACE, "ReloadDrrRecord: At least one of the fields <URNO,ROSS,ROSL,SDAY> is missing or empty" );
		ilRC = RC_FAIL;
	}
	else
	{
		if ( (clRoss[0]!='L') && (clRosl[0]=='3') && 
			 ( strcmp ( cgOpFirstDay,clSday) <= 0 ) && 
			 ( strcmp ( clSday, cgOpLastDay) <= 0) )
		{
			TrimRight ( clUrno );
			sprintf ( clSelection, "WHERE URNO='%s'", clUrno );
			ilRC = CEDAArrayRefill(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,
								   clSelection, NULL, ARR_NEXT );
			dbg ( TRACE, "ReloadDrrRecord: Refill of Array ilRC <%d>", ilRC );
		}
		else
		{
			dbg ( TRACE, "ReloadDrrRecord: DRR record doesn't match filter" );
			ilRC = RC_INVALID;
		}
	}
	return ilRC;
}

static int MyHandleDrt ( ARRAYINFO *prpArr, char *pcpSel, char *pcpUrnolist )
{
	long llRow=ARR_FIRST;
	char clUrno[41], *pclRow;
	int ilRc=RC_SUCCESS;

	/* Kruecke, weil Rc von DeleteEventData nichts aussagt */
	/*  rrIdx02Handle must be index on URNO */
	GetUrnoFromSelection(pcpSel,clUrno);
	ilRc = CEDAArrayFindRowPointer (&(prpArr->rrArrayHandle), prpArr->crArrayName,
									&(prpArr->rrIdx02Handle), prpArr->crIdx02Name,
									clUrno,&llRow, (void *) &pclRow );
	if ( ilRc == RC_SUCCESS )
	{
		ilRc = DeleteEventData (&(prpArr->rrArrayHandle), prpArr->crArrayName,
								pcpSel, pcpUrnolist );
		dbg ( TRACE, "MyHandleDrt: DeleteEventData URNO <%s> in <%s> RC <%d>", 
			  clUrno, prpArr->crArrayName, ilRc );
	}
	else
		dbg ( TRACE, "MyHandleDrt: URNO <%s> not found in <%s>", clUrno, prpArr->crArrayName );
	return ilRc;
}


static int HandleRelJob (char *pcpBCData, char *pcpBCSelection )
{
	int		ilRc = RC_SUCCESS ;
	char	clDel='-';
	/*char	clDel2='\262'  mapped comma ;*/
	char    clStart[15]="", clEnd[15]="", clBuf[101];
	int     ilRc1;
	BOOL	blDone = FALSE, blOverlap = FALSE;
	
	if ( !pcpBCData  || !pcpBCSelection )
		return RC_FAIL;

	dbg(TRACE,"HandleRelJob: Data <%s>", pcpBCData );
	dbg(TRACE,"HandleRelJob: Selection <%s>", pcpBCSelection );
	
	/* Check release dates, maybe nothing to be done */
	if ( !blDone && (ilRc == RC_SUCCESS) )
	{
		if ( GetDataItem(clBuf, pcpBCData, 1, clDel, "", "\0\0") > 0)
		{
			strncpy ( clStart, clBuf, 8 );
			clStart[8] = '\0';
		}
		if ( GetDataItem(clBuf, pcpBCData, 2, clDel, "", "\0\0") > 0)
		{
			strncpy ( clEnd, clBuf, 8 );
			clEnd[8] = '\0';
		}
		if ( (strlen(clEnd)<8) || (strlen (clStart)<8) )
			ilRc = RC_INIT_FAIL;
	}
	if ( !blDone && (ilRc == RC_SUCCESS) )
	{
		blOverlap = ((strcmp(clStart,cgOpLastDay) <= 0) && (strcmp(clEnd,cgOpFirstDay) >= 0));
		dbg ( TRACE, "HandleRelJob: Start <%s> End <%s> overlaps timeframe ? <%d>", clStart, clEnd, blOverlap );
		if ( !blOverlap )
			blDone = TRUE;
	}
	if ( !blDone && (ilRc == RC_SUCCESS) )
	{	/* Reload jobs and breaks */
		if ( strstr ( pcpBCSelection, cgUjtyBrk ) )
		{
			CEDAArrayDelete(&rgBrkArray.rrArrayHandle,rgBrkArray.crArrayName );
			/* ilRc1 = CEDAArrayFill(&rgBrkArray.rrArrayHandle,rgBrkArray.crArrayName,NULL); */
			ilRc1 = CEDAArrayFillHint(&rgBrkArray.rrArrayHandle,rgBrkArray.crArrayName,
									 0, cgJobReloadHint );
			dbg ( TRACE, "HandleRelJob: Reloading Breaks RC <%d>", ilRc1 );
			if ( ilRc1 != RC_SUCCESS )
				ilRc = ilRc1;
		}
		else
			dbg (DEBUG, "HandleRelJob: Breaks not in selection" );

		if ( strstr(pcpBCSelection,cgUjty) || strstr(pcpBCSelection,cgUjtyAbs) )
		{
			CEDAArrayDelete(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName );
			/*ilRc1 = CEDAArrayFill(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,"0, ");*/
			ilRc1 = CEDAArrayFillHint(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,
									 "0, ", cgJobReloadHint );
			dbg ( TRACE, "HandleRelJob: Reloading Pooljobs RC <%d>", ilRc1 );
			if ( ilRc1 != RC_SUCCESS )
				ilRc = ilRc1;

			PreparePoolJob("");
		}
		else
			dbg (DEBUG, "HandleRelJob: Pooljobs not in selection" );
	}
	return ilRc;
}

static int GetNextUrno(char *pcpUrno)
{
	int ilRC = RC_SUCCESS ;
	char  pclDataArea[IDATA_AREA_SIZE] ;
	
	if (igReservedUrnoCnt <= 0)
	{                            /* hole Urno buffer -> get new Urnos */
		memset( pclDataArea, 0, IDATA_AREA_SIZE) ;
		if ((ilRC = GetNextValues(pclDataArea, URNOS_TO_FETCH)) != RC_SUCCESS)
		{
			dbg ( TRACE,"GetNextValues failed RC <%d>", ilRC );
		}
		else
		{
			strcpy ( pcpUrno, pclDataArea );
			igReservedUrnoCnt = URNOS_TO_FETCH - 1 ;
			lgActUrno = atol(pcpUrno);
			dbg ( DEBUG, "GetNextUrno: fetched %d Urnos. Next is <%s>", 
				  URNOS_TO_FETCH, pcpUrno ) ;
		}
	}
	else
	{
		igReservedUrnoCnt-- ;
		lgActUrno++ ;
		sprintf ( pcpUrno, "%ld", lgActUrno );      /* get next reserved Urno */
		dbg ( DEBUG, "GetNextUrno: Next Urno <%s>, %d Urnos remaining", 
			  pcpUrno, igReservedUrnoCnt ) ;
	}
	
	return (ilRC) ;
} /* getNextUrno () */


/*	Read Org. Units from config file, where the link PFC.DPT1 shall be used for pool assignment */
static int ReadOrgUnitsForPfcUse()
{
	char clSection[24]="USE_PFC_DPT1", clEntry[24];
	int ilRc, ilLines=0, i, ilLen1, ilLen2, ilSize = sizeof(cgOrgUnitsForPfcUse);
	
	cgOrgUnitsForPfcUse[0] = '\0';
	ilRc = ReadConfigEntry ( clSection, "Lines", pcgCfgBuffer );
	if ( ilRc == RC_SUCCESS ) 
		ilLines = atoi(pcgCfgBuffer);
	if ( ilLines < 1 )
	{
		dbg ( TRACE, "ReadOrgUnitsForPfcUse: No valid entry Lines found" );
		ilRc = RC_INVALID;
	}
	if ( ilRc == RC_SUCCESS ) 
	{
		for ( i=1; i<= ilLines; i++ )
		{
			sprintf ( clEntry, "DPT_Line%d", i );
			if ( ( ReadConfigEntry ( clSection, clEntry, pcgCfgBuffer) == RC_SUCCESS) &&
				 *pcgCfgBuffer )
			{
				ilLen1 = strlen (cgOrgUnitsForPfcUse); 
				ilLen2 = strlen (pcgCfgBuffer); 
				if ( ilLen1 + ilLen2 + 2 > ilSize )
				{
					dbg ( TRACE, "ReadOrgUnitsForPfcUse: Not enough memory (%d) for list of Org. Units", ilSize );
					dbg ( TRACE, "ReadOrgUnitsForPfcUse: Already read (%d) <%s>", ilLen1, cgOrgUnitsForPfcUse );
					dbg ( TRACE, "ReadOrgUnitsForPfcUse: To add (%d) <%s>", ilLen2, pcgCfgBuffer );
					return RC_NOMEM;
				}
				strcat ( cgOrgUnitsForPfcUse, pcgCfgBuffer );
				strcat ( cgOrgUnitsForPfcUse, "," );
			}
		}
		if ( cgOrgUnitsForPfcUse[0] == '\0' ) 
			ilRc = RC_NODATA;
		dbg ( TRACE, "ReadOrgUnitsForPfcUse: Org.Units <%s> RC <%d>", cgOrgUnitsForPfcUse, ilRc );
	}
	return ilRc;
}

static void ReadConfigEntries ()
{
	char  clEntry[24];
	int ilRc;
	
	ReadConfigEntry("MAIN","DEFPOOL",cgDefaultPool);
	ilRc = ReadConfigEntry("MAIN","SPROFFSETFROM",cgSprOffsetFrom);
	if (ilRc != RC_SUCCESS)
	{
		strcpy(cgSprOffsetFrom,"-900");
	}
	ilRc = ReadConfigEntry("MAIN","SPROFFSETTO",cgSprOffsetTo);
	if (ilRc != RC_SUCCESS)
	{
		strcpy(cgSprOffsetTo,"900");
	}
	ilRc = ReadConfigEntry("MAIN","SPRBREAKOFFSETFROM",cgSprBreakOffsetFrom);
	if (ilRc != RC_SUCCESS)
	{
		strcpy(cgSprBreakOffsetFrom,"-900");
	}
	ilRc = ReadConfigEntry("MAIN","SPRBREAKOFFSETTO",cgSprBreakOffsetTo);
	if (ilRc != RC_SUCCESS)
	{
		strcpy(cgSprBreakOffsetTo,"900");
	}
	ilRc = ReadConfigEntry("MAIN","BREAK",clEntry);
	if ((ilRc == RC_SUCCESS) && (strcmp(clEntry,"YES") == 0))
		bgBreak = TRUE;
	else
		bgBreak = FALSE;

	ilRc = ReadConfigEntry("MAIN","SETSTARTATCLOCKIN",clEntry);
	if ((ilRc == RC_SUCCESS) && (strcmp(clEntry,"NO") == 0))
	{
		bgSetStartAtClockIn = FALSE;
		dbg ( TRACE, "Found SETSTARTATCLOCKIN = NO" );
	}
	else
		bgSetStartAtClockIn = TRUE;
	
	ilRc = ReadConfigEntry("MAIN","SETENDATCLOCKOUT",clEntry);
	if ((ilRc == RC_SUCCESS) && (strcmp(clEntry,"NO") == 0))
	{
		bgSetEndAtClockOut = FALSE;
		dbg ( TRACE, "Found SETENDATCLOCKOUT = NO" );
	}
	else
		bgSetEndAtClockOut = TRUE;
		
	bgRELJOBtoACTION = FALSE;
	ilRc = ReadConfigEntry("MAIN","SendRELJOBtoACTION",clEntry);
	if(ilRc != RC_SUCCESS)
	{	/* parameter missing -> take default and go on */
		dbg(TRACE,"InitPolhdl: did not find: <SendRELJOBtoACTION> in section <MAIN>, take default" );
	}
	else
	{
		if ( strcmp(clEntry,"YES") == 0)
			bgRELJOBtoACTION = TRUE;
	}/* end of if */

	bgRELJOBtoBCHDL = FALSE;
	ilRc = ReadConfigEntry("MAIN","SendRELJOBtoBCHDL",clEntry);
	if(ilRc != RC_SUCCESS)
	{	/* parameter missing -> take default and go on */
		dbg(TRACE,"InitPolhdl: did not find: <SendRELJOBtoBCHDL> in section <MAIN>, take default" );
	}
	else
	{
		if ( strcmp(clEntry,"YES") == 0)
			bgRELJOBtoBCHDL = TRUE;
	}/* end of if */
	dbg(TRACE,"InitPolhdl: RELJOB2ACTION <%d> RELJOB2BCHDL <%d>", bgRELJOBtoACTION, bgRELJOBtoBCHDL );

	ilRc = iGetConfigRow(cgConfigFile,"MAIN","DrrReloadHint", CFG_STRING,cgDrrReloadHint);

	if ( ilRc != RC_SUCCESS )
		strcpy ( cgDrrReloadHint, "/*+ INDEX(DRRTAB DRRTAB_SDAY_ROSL) */" );
	dbg(TRACE,"InitPolhdl: Hint for reloading DRRTAB <%s>", cgDrrReloadHint );
	
    /* MEI 14-OCT-2010 */
    if ( iGetConfigRow(cgConfigFile,"MAIN","JobReloadHint", CFG_STRING,cgJobReloadHint) != RC_SUCCESS )
        strcpy ( cgJobReloadHint, "/*+ INDEX(JOBTAB JOBTAB_ACTO) */" );
    dbg(TRACE,"InitPolhdl: Hint for reloading JOBTAB <%s>", cgJobReloadHint );

	bgOpssSetsPlanned = TRUE;
	ilRc = iGetConfigRow(cgConfigFile,"MAIN","OpssSetsPlanned", CFG_STRING,clEntry);
	if( (ilRc == RC_SUCCESS) && (strcmp(clEntry,"NO") == 0) )
	{
		bgOpssSetsPlanned = FALSE;
	}/* end of if */
	bgUseFilt = FALSE;
	ilRc = iGetConfigRow(cgConfigFile,"MAIN","UseFilt", CFG_STRING,clEntry);
	if( (ilRc == RC_SUCCESS) && (strcmp(clEntry,"YES") == 0) )
	{
		bgUseFilt = TRUE;
	}/* end of if */
}

static BOOL SetPlannedTimes ( CMDBLK *prpCmdblk )
{
	BOOL blRet = TRUE;

	if ( bgOpssSetsPlanned || !prpCmdblk )
		return TRUE;
	if ( !strcmp ( prpCmdblk->command, "URT") && 
		 ( GetDataItem (pcgCfgBuffer, prpCmdblk->tw_end, 3, ',', "", "\0") > 0 ) )
	{
		iStrup(pcgCfgBuffer);
		if ( !strncmp( pcgCfgBuffer, "OPSS", 4 ) )
			blRet = FALSE;
		dbg ( DEBUG, "SetPlannedTimes: Application <%s> bgOpssSetsPlanned <%d> return <%d>",
			  pcgCfgBuffer, bgOpssSetsPlanned, blRet ); 
	}
	return blRet;
}

static int MyHandleDrdDelete ( char *pcpSel, char *pcpUrnolist )
{
	long llRow=ARR_FIRST, llFunc=ARR_FIRST;
	char clUrno[21], clUdrr[21], *pclRow, clKey[41];
	int ilRc=RC_SUCCESS, ilRc1;

	/* Before we can delete DRD-record from array, we must find out which DRR-record is concerned */
	GetUrnoFromSelection(pcpSel,clUrno);
	ilRc = CEDAArrayFindRowPointer (&(rgDrdArray.rrArrayHandle), rgDrdArray.crArrayName,
									&(rgDrdArray.rrIdx02Handle), rgDrdArray.crIdx02Name,
									clUrno, &llRow, (void *) &pclRow );
	if ( ilRc == RC_SUCCESS )
	{
		sprintf(clKey,"%s,%s,%s", FIELDVAL(rgDrdArray,pclRow,igDrdDRRN),
								  FIELDVAL(rgDrdArray,pclRow,igDrdSDAY),
								  FIELDVAL(rgDrdArray,pclRow,igDrdSTFU) );
		dbg(TRACE,"MyHandleDrdDelete: Key to find DRR-record <%s>",clKey );

		ilRc1 = DeleteEventData (&(rgDrdArray.rrArrayHandle), 
								rgDrdArray.crArrayName, pcpSel, pcpUrnolist );
		dbg ( TRACE, "MyHandleDrdDelete: DeleteEventData URNO <%s> RC <%d>", 
			  clUrno, ilRc1 );

		if( CEDAArrayFindRowPointer(&(rgDrrArray.rrArrayHandle),
									rgDrrArray.crArrayName,
									&(rgDrrArray.rrIdx01Handle),
									rgDrrArray.crIdx01Name,
									clKey,&llFunc, (void *) &pclRow ) == RC_SUCCESS)
		{
			strcpy ( clUdrr, DRRFIELD(pclRow,igDrrURNO) );
			ilRc = ProcessDrrChange ( clUdrr, TRUE );
			dbg ( TRACE, "MyHandleDrdDelete: ProcessDrrChange URNO <%s> RC <%d>", clUdrr, ilRc ); 
		}
		else
		{
			dbg(TRACE,"DRR not found <%s>",clKey);
			ilRc = RC_NOTFOUND;
		}
		ilRc |= ilRc1;
	}
	else
		dbg ( TRACE, "MyHandleDrdDelete: URNO <%s>", clUrno );
	return ilRc;
}

static int MyHandleDrgDelete ( char *pcpSel, char *pcpUrnolist )
{
	long llRow=ARR_FIRST, llFunc=ARR_FIRST;
	char clUrno[21], clUdrr[21], *pclRow, clKey[41];
	int ilRc=RC_SUCCESS, ilRc1;

	/* Before we can delete DRG-record from array, we must find out which DRR-record is concerned */
	GetUrnoFromSelection(pcpSel,clUrno);
	ilRc = CEDAArrayFindRowPointer (&(rgDrgArray.rrArrayHandle), rgDrgArray.crArrayName,
									&(rgDrgArray.rrIdx02Handle), rgDrgArray.crIdx02Name,
									clUrno, &llRow, (void *) &pclRow );
	if ( ilRc == RC_SUCCESS )
	{
		sprintf(clKey,"%s,%s,%s", FIELDVAL(rgDrgArray,pclRow,igDrgDRRN),
								  FIELDVAL(rgDrgArray,pclRow,igDrgSDAY),
								  FIELDVAL(rgDrgArray,pclRow,igDrgSTFU) );
		dbg(TRACE,"MyHandleDrgDelete: Key to find DRR-record <%s>",clKey );

		ilRc1 = DeleteEventData (&(rgDrgArray.rrArrayHandle), 
								rgDrgArray.crArrayName, pcpSel, pcpUrnolist );
		dbg ( TRACE, "MyHandleDrgDelete: DeleteEventData URNO <%s> RC <%d>", 
			  clUrno, ilRc1 );

		if( CEDAArrayFindRowPointer(&(rgDrrArray.rrArrayHandle),
									rgDrrArray.crArrayName,
									&(rgDrrArray.rrIdx01Handle),
									rgDrrArray.crIdx01Name,
									clKey,&llFunc, (void *) &pclRow ) == RC_SUCCESS)
		{
			strcpy ( clUdrr, DRRFIELD(pclRow,igDrrURNO) );
			ilRc = ProcessDrrChange ( clUdrr, TRUE );
			dbg ( TRACE, "MyHandleDrgDelete: ProcessDrrChange URNO <%s> RC <%d>", clUdrr, ilRc ); 
		}
		else
		{
			dbg(TRACE,"DRR not found <%s>",clKey);
			ilRc = RC_NOTFOUND;
		}
		ilRc |= ilRc1;
	}
	else
		dbg ( TRACE, "MyHandleDrgDelete: URNO <%s>", clUrno );
	return ilRc;
}


static int GetWgpcForShift ( char *pcpDrrRow, char *pcpWgpc )
{
	char clKey[24] /*, clVafr[15], clVato[15]*/;
	long llFunc=ARR_FIRST;
	char *pclRow;
	BOOL blFound=FALSE;

	if ( (rgDrgArray.rrArrayHandle<0) || (rgSwgArray.rrArrayHandle<0) )
		return RC_NOTINITIALIZED;

	if ( !pcpDrrRow || !pcpWgpc )
		return RC_INVALID;

	sprintf ( clKey, "%s,%s,%s", DRRFIELD(pcpDrrRow,igDrrDRRN), 
			  DRRFIELD(pcpDrrRow,igDrrSDAY), DRRFIELD(pcpDrrRow,igDrrSTFU));
	dbg ( TRACE, "GetWgpcForShift: UDRR <%s> key <%s>", DRRFIELD(pcpDrrRow,igDrrURNO), clKey );
	if( CEDAArrayFindRowPointer(&(rgDrgArray.rrArrayHandle), rgDrgArray.crArrayName,
								&(rgDrgArray.rrIdx01Handle),rgDrgArray.crIdx01Name,
								clKey,&llFunc, (void *) &pclRow ) == RC_SUCCESS)
	{
		strncpy(pcpWgpc,FIELDVAL(rgDrgArray,pclRow,igDrgWGPC),5);
		pcpWgpc[5] = '\0';
		dbg(TRACE,"GetWgpcForShift: Found DRG-record WGPC <%s>", pcpWgpc );
		blFound = TRUE;
	}
	else
	{
		if ( GetValidStaffEntry ( &rgSwgArray, pcpDrrRow, igSwgVPFR, 
							      igSwgVPTO, -1, &pclRow ) == RC_SUCCESS )
		{
			strncpy(pcpWgpc,FIELDVAL(rgSwgArray,pclRow,igSwgCODE),5);
			pcpWgpc[5] = '\0';
			dbg(TRACE,"GetWgpcForShift: Found SWG-record WGPC <%s>", pcpWgpc );
			blFound = TRUE;			
		}
	}
	return blFound ? RC_SUCCESS : RC_NOTFOUND;
}

static int GetFccoForPjb ( char *pcpDrrRow, char *pcpUdrd, char *pcpUdel, char *pcpFcco )
{
	char clKey[24], clVafr[15], clVato[15];
	long llFunc=ARR_FIRST;
	char *pclRow;
	BOOL blFound=FALSE;
	char *pclVal=0;
	int ilRc;

	if ( !pcpDrrRow || !pcpFcco )
		return RC_INVALID;

	sprintf ( clKey, "%s,%s,%s", DRRFIELD(pcpDrrRow,igDrrDRRN), 
			  DRRFIELD(pcpDrrRow,igDrrSDAY), DRRFIELD(pcpDrrRow,igDrrSTFU));
	dbg ( TRACE, "GetFccoForPjb: UDRR <%s> key <%s>", DRRFIELD(pcpDrrRow,igDrrURNO), clKey );
	/*  look for DRG-record */
	if( CEDAArrayFindRowPointer(&(rgDrgArray.rrArrayHandle), rgDrgArray.crArrayName,
								&(rgDrgArray.rrIdx01Handle),rgDrgArray.crIdx01Name,
								clKey,&llFunc, (void *) &pclRow ) == RC_SUCCESS)
	{
		if ( !IS_EMPTY (FIELDVAL(rgDrgArray,pclRow,igDrgFCTC) ) )
		{
			pclVal = FIELDVAL(rgDrgArray,pclRow,igDrgFCTC);
			dbg(TRACE,"GetFccoForPjb: Found DRG-record FCTC <%s>", pclVal );
		}
	}
	/*  if pcpUdrd look for DRD-record */
	if( !pclVal && pcpUdrd && !IS_EMPTY(pcpUdrd) )
	{
		llFunc = ARR_FIRST;
		ilRc = CEDAArrayFindRowPointer(&(rgDrdArray.rrArrayHandle), rgDrdArray.crArrayName,
									   &(rgDrdArray.rrIdx02Handle),rgDrdArray.crIdx02Name,
									   pcpUdrd, &llFunc, (void *) &pclRow ) ;
		if ( (ilRc==RC_SUCCESS) && !IS_EMPTY (FIELDVAL(rgDrdArray,pclRow,igDrdFCTC) ) )
		{
			pclVal = FIELDVAL(rgDrdArray,pclRow,igDrdFCTC);
			dbg(TRACE,"GetFccoForPjb: Found DRD-record FCTC <%s>", pclVal );
		}
	}

	/*  if pcpUdel look for DEL-record */
	if( !pclVal && pcpUdel && !IS_EMPTY(pcpUdel) )
	{
		llFunc = ARR_FIRST;
		ilRc = CEDAArrayFindRowPointer(&(rgDelArray.rrArrayHandle), rgDelArray.crArrayName,
									   &(rgDelArray.rrIdx02Handle),rgDelArray.crIdx02Name,
									   pcpUdel, &llFunc, (void *) &pclRow ) ;
		if ( (ilRc==RC_SUCCESS) && !IS_EMPTY (FIELDVAL(rgDelArray,pclRow,igDelFCTC) ) )
		{
			pclVal = FIELDVAL(rgDelArray,pclRow,igDelFCTC);
			dbg(TRACE,"GetFccoForPjb: Found DEL-record FCTC <%s>", pclVal );
		}
	}

	/*  if DRR.FCTC is filled use it */
	if( !pclVal  && !IS_EMPTY(DRRFIELD(pcpDrrRow,igDrrFCTC)) )
	{
		pclVal = DRRFIELD(pcpDrrRow,igDrrFCTC);
		dbg(TRACE,"GetFccoForPjb: Found DRR.FCTC <%s> is used", pclVal );
	}
	/*  look for function in BSD-record */
	if( !pclVal  && !IS_EMPTY(DRRFIELD(pcpDrrRow,igDrrSCOD)) )
	{
		llFunc = ARR_FIRST;
		ilRc = CEDAArrayFindRowPointer(&(rgBsdArray.rrArrayHandle), rgBsdArray.crArrayName,
									   &(rgBsdArray.rrIdx01Handle),rgBsdArray.crIdx01Name,
									   DRRFIELD(pcpDrrRow,igDrrSCOD), &llFunc, (void*) &pclRow ) ;
		if ( (ilRc==RC_SUCCESS) && !IS_EMPTY(BSDFIELD(pclRow,igBsdFCTC) ) )
		{
			pclVal = BSDFIELD(pclRow,igBsdFCTC);
			dbg(TRACE,"GetFccoForPjb: Found BSD-record FCTC <%s>", pclVal );
		}
	}

	/*  look active function in SPFTAB */
	if ( !pclVal  )
	{
		ilRc = GetValidStaffEntry ( &rgSpfArray, pcpDrrRow, igSpfVPFR, 
									igSpfVPTO, igSpfPRIO, &pclRow );
		if ( (ilRc==RC_SUCCESS) && !IS_EMPTY (FIELDVAL(rgSpfArray,pclRow,igSpfCODE) ) )
		{
			pclVal = FIELDVAL(rgSpfArray,pclRow,igSpfCODE);
			dbg(TRACE,"GetFccoForPjb: Found SPF-record FCTC <%s>", pclVal );
		}
	}
	if ( pclVal )
	{
		strncpy(pcpFcco,pclVal,5);
		pcpFcco[5] = '\0';
		return RC_SUCCESS;
	}
	else
		return RC_NOTFOUND;
}


static int GetValidStaffEntry ( ARRAYINFO *prpArr, char *pcpDrrRow, int ipVafrIdx, 
							    int ipVatoIdx, int ipPrioIdx, char **pcpRowFound )
{
	int ilRc = RC_SUCCESS;
	char clUstf[24],clVato[15], clVafr[15], clPrio[5];
	long llFunc = ARR_FIRST;
	char *pclRow, *pclLast=0;
	int	 ilLastPrio=10000, ilActPrio, ilFound=0, ilValid;

	*pcpRowFound = 0;
	strncpy(clUstf, DRRFIELD(pcpDrrRow,igDrrSTFU),10);
	clUstf[10] = '\0';
	TrimRight(clUstf);
	dbg(DEBUG,"Index %d <%s> Key: <%s>",prpArr->rrIdx01Handle, prpArr->crIdx01Name,clUstf);
	
	do 
	{
		ilRc = CEDAArrayFindRowPointer( &(prpArr->rrArrayHandle), prpArr->crArrayName,
										&(prpArr->rrIdx01Handle), prpArr->crIdx01Name,
										clUstf,&llFunc, (void*)&pclRow ) ;         
		if(ilRc == RC_SUCCESS)
		{	
			ilValid = 1;
			if ( (ipVafrIdx>=0) && (ipVatoIdx>=0) )
			{
				strncpy ( clVafr, PFIELDVAL((prpArr),pclRow,ipVafrIdx), 14 );
				strncpy ( clVato, PFIELDVAL((prpArr),pclRow,ipVatoIdx), 14 );
				clVafr[14] = clVato[14] = '\0';
				TrimRight ( clVafr );
				TrimRight ( clVato );
							
				if ( (strlen (clVafr)==14) && 
					 ( strcmp ( DRRFIELD(pcpDrrRow,igDrrAVFR), clVafr ) < 0 ) )
					ilValid = 0;
				else
					if ( (strlen (clVato)==14) && 
						 ( strcmp ( clVato, DRRFIELD(pcpDrrRow,igDrrAVTO) ) < 0 ) )
						ilValid = 0;
				dbg ( DEBUG, "Validity-Check: <%s> Valid <%s>-<%s> PoolJob <%s>-<%s> Valid <%d>",
					  prpArr->crArrayName, clVafr, clVato, DRRFIELD(pcpDrrRow,igDrrAVFR), 
					  DRRFIELD(pcpDrrRow,igDrrAVTO), ilValid );   
			}		
			if ( ilValid )
			{
				if ( ipPrioIdx <0 )		/* this criterion has no Priority	*/
				{						
					ilFound = 1;		/* first valid record is ok			*/
					pclLast = pclRow;
				}	 
				else
				{
					strncpy ( clPrio, PFIELDVAL(prpArr,pclRow,ipPrioIdx), 4 );
					clPrio[4] = '\0';
					TrimRight ( clPrio );
					ilActPrio = (strlen(clPrio)>0)  ? atoi(clPrio) : 9999;
					if ( ilActPrio < ilLastPrio )
					{
						dbg ( DEBUG, "Prio-Check: New item <%s> found Old Prio <%d> new Prio<%d>",
							  pclRow, ilLastPrio, ilActPrio );
						ilLastPrio = ilActPrio ;
						pclLast = pclRow;			/* keep valid record, look for more 	*/
					}
					else
						dbg ( DEBUG, "Prio-Check: Item with lower prio <%d> found, Old Prio <%d>",
							  ilActPrio, ilLastPrio );
				}
			} /* endif ilValid */
		} /* endif ilRc==RC_SUCCESS */
		llFunc = ARR_NEXT;
	} while (!ilFound && (ilRc == RC_SUCCESS ) );
	
	if ( pclLast )
	{
		*pcpRowFound = pclLast;
		ilRc = RC_SUCCESS;
	}
	else
		dbg ( TRACE, "GetValidStaffEntry: No valid item found in <%s> for SURN <%s>",
					  prpArr->crArrayName, clUstf );
	return ilRc;
}


static int GetPoolName ( char *pcpUpol, char *pcpPoolName )
{
	int ilRc;
	long llFunc=ARR_FIRST;
	char *pclRow, *pclName;

	if ( IS_EMPTY(pcpUpol) )
		return RC_INVALID;

	ilRc = CEDAArrayFindRowPointer( &(rgPolArray.rrArrayHandle),rgPolArray.crArrayName,
									&(rgPolArray.rrIdx02Handle),rgPolArray.crIdx02Name,
									pcpUpol, &llFunc, (void*) &pclRow ) ;
	if ( ilRc == RC_SUCCESS )
		strcpy ( pcpPoolName, FIELDVAL(rgPolArray,pclRow,igPolNAME) );
	return ilRc;
}

static int GetItemNo(char *pcpFieldName,char *pcpFields,
		     int *pipItemNo)
{
    int ilRc = RC_FAIL;
    int ilLc;
    int ilItemCount;
    char pclFieldName[1200];

    *pipItemNo = 0;

    ilItemCount = get_no_of_items(pcpFields);

    for(ilLc = 1; ilLc <= ilItemCount && ilRc != RC_SUCCESS; ilLc++)
    {
	get_real_item(pclFieldName,pcpFields,ilLc);
	if(!strcmp(pclFieldName,pcpFieldName))
	{
	    *pipItemNo = ilLc;
	    ilRc = RC_SUCCESS;
	}
    }

    return ilRc;
}


static int ProcessRfjChange(char *pcpFields, char *pcpData, char *pcpOldData )
{
	int ilRc, ilRc1, ilRc2;
    int ilItemNoUrno;
    int ilItemNoAcfr;
    int ilItemNoActo;
	long llFieldNo;
	long llRow=ARR_FIRST, llDfjRownum, llPjbRownum;
	char clUrno[41], *pclRow, *pclRow2;
	char clAcfrOld[15], clAcfrNew[15];
	char clActoOld[15], clActoNew[15];
	BOOL blSetAcfr = FALSE;
	BOOL blSetActo = FALSE;
	char clDfjUrno[21], clPjbUrno[21];


	ilRc = GetItemNo("URNO",pcpFields,&ilItemNoUrno);
    if(ilRc == RC_SUCCESS)
    {
		get_real_item(clUrno,pcpData,ilItemNoUrno);
		ilRc = CEDAArrayFindRowPointer (&(rgRfjArray.rrArrayHandle), rgRfjArray.crArrayName,
										&(rgRfjArray.rrIdx02Handle), rgRfjArray.crIdx02Name,
										clUrno, &llRow, (void *) &pclRow );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( DEBUG, "ProcessRfjChange: URNO <%s> is no RFJ", clUrno );
			return RC_IGNORE;
		}
		else
		{	/* check whether ACFR,ACTO not in event or unchanged */
			ilRc1 = GetItemNo("ACFR",pcpFields,&ilItemNoAcfr);
			if ( ilRc1 == RC_SUCCESS ) 
			{
				get_real_item(clAcfrNew,pcpData,ilItemNoAcfr);
				if ( pcpOldData && *pcpOldData )
				{
					get_real_item(clAcfrOld,pcpOldData,ilItemNoAcfr);
					if ( strcmp ( clAcfrNew, clAcfrOld ) == 0 ) 
					{		
						dbg(DEBUG,"ProcessRfjChange: ACFR old <%s> == new <%s>", clAcfrOld, clAcfrNew );
					}
					else
						blSetAcfr = TRUE;
				}
				else
					blSetAcfr = TRUE;
			}
			ilRc1 = GetItemNo("ACTO",pcpFields,&ilItemNoActo);
			if ( ilRc1 == RC_SUCCESS ) 
			{
				get_real_item(clActoNew,pcpData,ilItemNoActo);
				if ( pcpOldData && *pcpOldData )
				{
					get_real_item(clActoOld,pcpOldData,ilItemNoActo);
					if ( strcmp ( clActoNew, clActoOld ) == 0 ) 
					{		
						dbg(DEBUG,"ProcessRfjChange: ACTO old <%s> == new <%s>", clActoOld, clActoNew );
					}
					else
						blSetActo = TRUE;
				}
				else
					blSetActo = TRUE;
			}
			if ( !blSetAcfr && !blSetActo )
			{
				dbg(DEBUG,"ProcessRfjChange: ACFR and ACTO not changed -> END" );
				return RC_IGNORE;
			}
						
			dbg(DEBUG,"ProcessRfjChange: change DFJ from <%s-%s> to <%s-%s>", clAcfrOld, clActoOld, clAcfrNew, clActoNew );
			
			/* changed Job is an RFJ -> look for corresponding DFJ */
			ilRc = FindDfjForRfj ( pclRow, clDfjUrno, &llDfjRownum );
			if ( ilRc==RC_SUCCESS ) 
			{
				ilRc = CEDAArrayGetRowPointer ( &(rgDetArray.rrArrayHandle), rgDetArray.crArrayName,
								 				llDfjRownum, (void *) &pclRow2 );
				if ( ilRc != RC_SUCCESS ) 
					dbg ( TRACE, "ProcessRfjChange: unable to find DFJ row <%ld> in array", llDfjRownum );
				else
				{
					ilRc1 = FindPjbForDetached ( pclRow2, clPjbUrno, &llPjbRownum );
					if ( ilRc1 == RC_SUCCESS )
					{
						if ( blSetAcfr )
						{
							llFieldNo = -1;
							dbg(DEBUG,"ProcessRfjChange: PJB-Row <%ld> set ACFR to <%s>", llPjbRownum, clAcfrNew );
							ilRc1 |= CEDAArrayPutField( &rgJobArray.rrArrayHandle, rgJobArray.crArrayName, 
													   &llFieldNo, "ACFR", llPjbRownum, clAcfrNew);
						}
						if ( blSetActo )
						{
							llFieldNo = -1;
							dbg(DEBUG,"ProcessRfjChange: PJB-Row <%ld> set ACTO to <%s>", llPjbRownum, clActoNew );
							ilRc1 |= CEDAArrayPutField( &rgJobArray.rrArrayHandle, rgJobArray.crArrayName, 
													        &llFieldNo, "ACTO", llPjbRownum, clActoNew);
						}
						if ( ilRc1 != RC_SUCCESS )
							dbg ( TRACE, "ProcessRfjChange: Error updating pool job RC <%d>", ilRc1 );
					}
					else
						dbg ( TRACE, "ProcessRfjChange: unable to find pool job for DFJ <%s>", JOBFIELD(pclRow2,igJobURNO) );
					ilRc2 = RC_SUCCESS;
					if ( blSetAcfr )
					{
						llFieldNo = -1;
						dbg(DEBUG,"ProcessRfjChange: DFJ-Row <%ld> set ACFR to <%s>", llDfjRownum, clAcfrNew );
						ilRc2 |= CEDAArrayPutField( &rgDetArray.rrArrayHandle, rgDetArray.crArrayName, 
												   &llFieldNo, "ACFR", llDfjRownum, clAcfrNew);
					}
					if ( blSetActo )
					{
						llFieldNo = -1;
						dbg(DEBUG,"ProcessRfjChange: DFJ-Row <%ld> set ACTO to <%s>", llDfjRownum, clActoNew );
						ilRc2 |= CEDAArrayPutField( &rgDetArray.rrArrayHandle, rgDetArray.crArrayName, 
												   &llFieldNo, "ACTO", llDfjRownum, clActoNew);
					}
					if ( ilRc1 == RC_SUCCESS )
						ilRc |= CEDAArrayWriteDB( &rgJobArray.rrArrayHandle, rgJobArray.crArrayName,
												  NULL, NULL, NULL, NULL, ARR_COMMIT_ALL_OK ) ;
					if ( ilRc2 == RC_SUCCESS )
						ilRc |= CEDAArrayWriteDB( &rgDetArray.rrArrayHandle, rgDetArray.crArrayName,
												  NULL, NULL, NULL, NULL, ARR_COMMIT_ALL_OK ) ;
				}
			}
		}
	}

	return ilRc;
}


static int HandleRfjDelete (char *pcpSel )
{
	long llRow=ARR_FIRST, llDfjRownum, llPjbRownum;
	char clUrno[41], *pclRow, *pclRow2;
	int ilRc=RC_SUCCESS, ilRc1, ilRcDel1, ilRcDel2;
	char clDfjUrno[21], clPjbUrno[21];

	GetUrnoFromSelection(pcpSel,clUrno);
	ilRc = CEDAArrayFindRowPointer (&(rgRfjArray.rrArrayHandle), rgRfjArray.crArrayName,
									&(rgRfjArray.rrIdx02Handle), rgRfjArray.crIdx02Name,
									clUrno, &llRow, (void *) &pclRow );
	if ( ilRc != RC_SUCCESS )
	{
		dbg ( DEBUG, "HandleRfjDelete: URNO <%s> is no RFJ", clUrno );
		return RC_IGNORE;
	}
	else
	{	/* deleted Job is an RFJ -> look for corresponding DFJ */
		ilRc = FindDfjForRfj ( pclRow, clDfjUrno, &llDfjRownum );
		if ( ilRc==RC_SUCCESS ) 
		{
			ilRc = CEDAArrayGetRowPointer ( &(rgDetArray.rrArrayHandle), rgDetArray.crArrayName,
								 	        llDfjRownum, (void *) &pclRow2 );
			if ( ilRc != RC_SUCCESS ) 
				dbg ( TRACE, "HandleRfjDelete: unable to find DFJ row <%ld> in array", llDfjRownum );
			else
			{
				ilRcDel1 = ilRcDel2 = RC_IGNORE;
				ilRc1 = FindPjbForDetached ( pclRow2, clPjbUrno, &llPjbRownum );
				if ( ilRc1 == RC_SUCCESS )
					ilRcDel1 = CEDAArrayDeleteRow( &(rgJobArray.rrArrayHandle), rgJobArray.crArrayName,llPjbRownum );
				else
					dbg ( TRACE, "HandleRfjDelete: unable to find pool job for DFJ <%s>", JOBFIELD(pclRow2,igJobURNO) );
				ilRcDel2 = CEDAArrayDeleteRow( &(rgDetArray.rrArrayHandle), rgDetArray.crArrayName, llDfjRownum ); 
				if ( ilRcDel1 == RC_SUCCESS )
					ilRc |= CEDAArrayWriteDB( &rgJobArray.rrArrayHandle, rgJobArray.crArrayName,
											  NULL, NULL, NULL, NULL, ARR_COMMIT_ALL_OK ) ;
				if ( ilRcDel2 == RC_SUCCESS )
					ilRc |= CEDAArrayWriteDB( &rgDetArray.rrArrayHandle, rgDetArray.crArrayName,
											  NULL, NULL, NULL, NULL, ARR_COMMIT_ALL_OK ) ;
			}
		}
	}
	return ilRc;
}


static int FindDfjForRfj ( char *pcpRfjRow, char *pcpDfjUrno, long *lpDfjRownum )
{
	long llRow=ARR_FIRST;
	char clUrno[41], *pclRow;
	int ilRc=RC_NOTFOUND;
	char clUdsr[21], clUaft[21], clKey[41], clUaft2[21];
	char *pclPlfr1, *pclPlfr2, *pclPlto1, *pclPlto2;

	strcpy ( clUdsr, JOBFIELD(pcpRfjRow,igJobUDSR) ); 
	strcpy ( clUaft, JOBFIELD(pcpRfjRow,igJobUAFT) ); 
	TrimRight ( clUaft );
	sprintf ( clKey, "%s,%s", clUdsr, clUaft );
	dbg ( DEBUG, "FindDfjForRfj: Searching DFJ on UAFT <%s> with key <%s>",  clUaft, clKey );

	while ( ( ilRc != RC_SUCCESS ) &&
		    ( CEDAArrayFindRowPointer ( &(rgDetArray.rrArrayHandle), rgDetArray.crArrayName,
								 	    &(rgDetArray.rrIdx02Handle), rgDetArray.crIdx02Name,
										clKey, &llRow, (void *) &pclRow ) == RC_SUCCESS ) )
	{
		strcpy ( clUaft2, JOBFIELD(pclRow,igJobUAFT) );
		TrimRight ( clUaft2 );
		pclPlfr1 = JOBFIELD(pcpRfjRow,igJobPLFR); 
		pclPlfr2 = JOBFIELD(pclRow,igJobPLFR); 
		pclPlto1 = JOBFIELD(pcpRfjRow,igJobPLTO); 
		pclPlto2 = JOBFIELD(pclRow,igJobPLTO); 
		if ( ( strcmp ( clUaft, clUaft2) == 0 ) &&
			 ( strncmp ( pclPlfr1, pclPlfr2, 14 ) == 0 ) &&	
			 ( strncmp ( pclPlto1, pclPlto2, 14 ) == 0 ) )	
		{
			dbg ( TRACE, "FindDfjForRfj: found DFJ URNO <%s>", JOBFIELD(pclRow,igJobURNO)  );
			*lpDfjRownum = llRow;
			strcpy ( pcpDfjUrno, JOBFIELD(pclRow,igJobURNO) );
			ilRc = RC_SUCCESS;
		}
		else
		{
			dbg ( TRACE, "FindDfjForRfj: DFJ URNO <%s> does not fit:", JOBFIELD(pclRow,igJobURNO)  );
			dbg ( TRACE, "PLFR <%s>-<%s> PLTO <%s>-<%s> UAFT <%s>-<%s>", 
				  pclPlfr1, pclPlfr2,	pclPlto1, pclPlto2, clUaft, clUaft2 );
		}
		llRow=ARR_NEXT;
	}
	return ilRc;
}




static int FindPjbForDetached ( char *pcpDetRow, char *pcpUpjb, long *lpPjbRownum )
{
	long llRow=ARR_FIRST;
	char clUrno[41], *pclRow;
	int ilRc=RC_NOTFOUND;
	char clUstf[21], clJour[21], clUaft[21], clKey[41], clUaft2[21];
	char *pclPlfr1, *pclPlfr2, *pclPlto1, *pclPlto2;

	strcpy ( clUstf, JOBFIELD(pcpDetRow,igJobUSTF) ); 
	strcpy ( clJour, JOBFIELD(pcpDetRow,igJobJOUR) ); 
	strcpy ( clUaft, JOBFIELD(pcpDetRow,igJobUAFT) ); 
	TrimRight ( clUstf );
	TrimRight ( clJour );
	TrimRight ( clUaft );
	
	sprintf ( clKey, "%s,%s,%s", cgUjty, clUstf, clJour );
	dbg ( DEBUG, "FindPjbForDetached: Searching Pooljob on flight <%s> with key <%s>", clUaft, clKey );

	while ( ( ilRc != RC_SUCCESS ) &&
		    ( CEDAArrayFindRowPointer ( &(rgJobArray.rrArrayHandle), rgJobArray.crArrayName,
								 	    &(rgJobArray.rrIdx03Handle), rgJobArray.crIdx03Name,
										clKey, &llRow, (void *) &pclRow ) == RC_SUCCESS ) )
	{
		strcpy ( clUaft2, JOBFIELD(pclRow,igJobUAFT) );
		pclPlfr1 = JOBFIELD(pcpDetRow,igJobPLFR); 
		pclPlfr2 = JOBFIELD(pclRow,igJobPLFR); 
		pclPlto1 = JOBFIELD(pcpDetRow,igJobPLTO); 
		pclPlto2 = JOBFIELD(pclRow,igJobPLTO); 
		if ( ( strcmp ( clUaft, clUaft2) == 0 ) &&
			 ( strncmp ( pclPlfr1, pclPlfr2, 14 ) == 0 ) &&	
			 ( strncmp ( pclPlto1, pclPlto2, 14 ) == 0 ) )	
		{
			strcpy ( pcpUpjb, JOBFIELD(pclRow,igJobURNO) );
			dbg ( TRACE, "FindPjbForDetached: found pool job URNO <%s>", pcpUpjb  );
			*lpPjbRownum = llRow;
			ilRc = RC_SUCCESS;
		}
		else
		{
			dbg ( TRACE, "FindPjbForDetached: DFJ URNO <%s> does not fit:", JOBFIELD(pclRow,igJobURNO)  );
			dbg ( TRACE, "PLFR <%s>-<%s> PLTO <%s>-<%s> UAFT <%s>-<%s>", 
				  pclPlfr1, pclPlfr2,	pclPlto1, pclPlto2, clUaft, clUaft2 );
		}
		llRow=ARR_NEXT;
	}
	return ilRc;
}


static int DoSingleSelect(char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpDataArea)
{
	int ilRc = RC_SUCCESS;
	static char clSqlBuf[4096];
	static char cgErrMsg[1024];

	short slCursor = 0;
	short slFunction;
	char *pclDataArea;
	int ilLc,ilItemCount;

	sprintf(clSqlBuf,"SELECT %s FROM %s %s",pcpFields,pcpTable,pcpSelection);

  *pcpDataArea = '\0';
	ilRc = sql_if(START,&slCursor,clSqlBuf,pcpDataArea);
	if(ilRc != RC_SUCCESS && ilRc != NOTFOUND)
	{
		/* there must be a database error */
		get_ora_err(ilRc, cgErrMsg);
		dbg(TRACE,"DoSingleSelect failed RC=%d <%s>",ilRc,cgErrMsg);
		dbg(TRACE,"SQL: <%s>",clSqlBuf);
	}
	close_my_cursor(&slCursor);
	slCursor = 0;

  if (ilRc == RC_SUCCESS)
	{
		ilItemCount = get_no_of_items(pcpFields); 
		pclDataArea = pcpDataArea;
		for(ilLc =  1; ilLc < ilItemCount; ilLc++)
		{
			while(*pclDataArea != '\0')
			{
				pclDataArea++;
			}
			*pclDataArea = ',';
		}
		dbg(DEBUG,"DoSingleSelect: RC=%d <%s>",ilRc,pcpDataArea);
	}
	else
	{
		dbg(DEBUG,"DoSingleSelect: RC=%d",ilRc);
	}
	return(ilRc);
}
//Added for UFIS 1075
/****************************************************************************/
/**********************GetDrrUrnoFromDraFields*******************************/ 
/****************************************************************************/
static int GetDrrUrnoFromDraFields(char *pcpFields,char *pcpData,char *pcpUrno)
{
	int	ilRc = RC_SUCCESS;
	long llFunc = ARR_FIRST;
	char      clDel          = ',';
	int 		ilItemNo;
	int    	ilCol;
	int 		ilPos;
	char 	 	clDrrn[24];
	char 	 	clSday[24];
	char 	 	clStfu[24];
	char 	 	clKey[124];
	char  	*pclRow = NULL;
//TRIAL 1075
	char pclUrno[12];
	long llDraFunc = ARR_FIRST;

	
	dbg(TRACE,"GetDrrUrnoFromDraFields: <%s> <%s>",pcpFields,pcpData);

	
			ilRc = FindItemInList(pcpFields,"URNO",clDel,&ilItemNo,&ilCol,&ilPos);
			if (ilRc == RC_SUCCESS)
			{
				GetDataItem(pclUrno,pcpData,ilItemNo,clDel,"","\0\0");
				dbg(TRACE,"URNO found: <%s>",pclUrno);
			}
			if((CEDAArrayFindRowPointer(&(rgDraArray.rrArrayHandle),
												 &(rgDraArray.crArrayName[0]),
												 &(rgDraArray.rrIdx02Handle),
												 &(rgDraArray.crIdx02Name[0]),
												 pclUrno,&llDraFunc,
												(void *) &pclRow )) == DB_SUCCESS)
		{
			strcpy(clSday,FIELDVAL(rgDraArray,pclRow,igDraSDAY));
			strcpy(clStfu,FIELDVAL(rgDraArray,pclRow,igDraSTFU));
			strcpy(clDrrn,FIELDVAL(rgDraArray,pclRow,igDraDRRN));
		}
		
		sprintf(clKey,"%s,%s,%s",clDrrn,clSday,clStfu);

		if(CEDAArrayFindRowPointer(&(rgDrrArray.rrArrayHandle),
																	&(rgDrrArray.crArrayName[0]),
																	&(rgDrrArray.rrIdx01Handle),
																	&(rgDrrArray.crIdx01Name[0]),
																	clKey,&llFunc,
																	(void *) &pclRow ) == RC_SUCCESS)
		{
			dbg(TRACE,"DRR-URNO <%s> DRR-ROSL <%s> ",DRRFIELD(pclRow,igDrrURNO),DRRFIELD(pclRow,igDrrROSL));
			//strncpy(pcpUrno,DRRFIELD(pclRow,igDrrURNO),10);
			//pcpUrno[10] = '\0';
			sprintf(pcpUrno,"%s",DRRFIELD(pclRow,igDrrURNO));//Added for UFIS 1075
		}
		else
		{
			dbg(TRACE,"DRR not found <%s>",clKey);
			ilRc = RC_NOTFOUND;
			
		}
		return (ilRc);
	
}
/****************************************************************************/


												

/******************************************************************************/
/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

