/*********************************************************/
/* GetValue61: Calculating the compensation working time */
/*********************************************************/
static double GetValue61(const char *pcpCheckBsdu,const char *pcpSday,const char *pcpLocalDrrStartTime,const char *pcpLocalDrrEndTime, const char *pcpLocalDrrBreakTime, const char* pcpIsCompensation)
{
	double	dlResultValue = 0;
	double	dlDrrMin;
	double	dlBreakMin;

	char	clVereinbarteWochenstunden[20];

	if (strcmp(pcpIsCompensation,"1") != 0)
	{
		dbg(DEBUG,"GetValue61(): No compensation found! Return 0!");
		return 0;
	}

	/* length of shift */
	dlDrrMin = GetDailyDRRMinutesByStaff(pcpLocalDrrBreakTime,pcpLocalDrrStartTime,pcpLocalDrrEndTime);

	/* break duration */
	dlBreakMin = GetDailyBreakMinutesByStaff(pcpCheckBsdu,pcpLocalDrrBreakTime,pcpSday);

	dbg(DEBUG,"GetValue61(): Shift minutes <dlDrrMin>: %lf",dlDrrMin);
	dbg(DEBUG,"GetValue61(): Break minutes <dlBreakMin>: %lf",dlBreakMin);

	/* check shift length */
	if (dlDrrMin == 0)
	{
		dbg(DEBUG,"GetValue61(): Using agreed weekly working hours.");
		if (ReadTemporaryField("VereinbarteWochenstunden",clVereinbarteWochenstunden) > 0)
		{
			dlDrrMin = atof(clVereinbarteWochenstunden);
		}
	}

	dbg(DEBUG,"GetValue61(): Result minutes: %lf",dlDrrMin + dlBreakMin);

	dlResultValue = (dlDrrMin + dlBreakMin) / 60;

	return dlResultValue;
}

/***********************************************************/
/****** Account 61: compensation hours for temporary staff */
/***********************************************************/
void bas_61()
{
	char	clLastCommand[6];
	char	clIsAushilfe[2];
	char	clStaffUrno[12];
	char	clSday[16];
	char	clMonthIndex[4];
	char	clYearIndex[6];

	double	dlAccValue[12];
	char	clNewShiftUrno[12];
	char	clDrrBreakTime[16];
	char	clDrrStartTime[16];
	char	clDrrEndTime[16];
	char	clNewDrrStatus[2];
	char	clOldDrrBreakTime[16];
	char	clOldDrrStartTime[16];
	char	clOldDrrEndTime[16];
	char	clOldDrrStatus[2];
	char	clOldShiftUrno[12];

	char	clIsCompensation[2];
	char	clCl12Acc60[12];
	double	dlOldValue;
	double	dlNewValue;
	double	dlResultValue;
	int	i;

	dbg(DEBUG, "============================= ACCOUNT 61 START ===========================");

	/* checking temporary staff */
	if (ReadTemporaryField("IsAushilfe",clIsAushilfe) == 0 && clIsAushilfe[0] == '0')
	{
		dbg(TRACE,"Account 61: Stopped, because no temporary staff");
		return;
	}

	/* get last command (IRT,URT,DRT) */
	strncpy(clLastCommand,GetLastCommand(),3);
	clLastCommand[3] = '\0';
	dbg(DEBUG,"Account 61: Command (%s)",clLastCommand);

	/* checking the URNO of the employee */
	if (ReadTemporaryField("StfUrno",clStaffUrno) == 0)
	{
		dbg(TRACE,"Account 61: Stopped, because <clStaffUrno> = undef!");
		return;
	}

	/* checking the SDAY */
	if (ReadTemporaryField("SDAY",clSday) == 0)
	{
		dbg(TRACE,"Account 61: Stopped, because <clSday> = undef!");
		return;
	}

	/* BSDU of new DRR-record */
	if (ReadTemporaryField("NewBsdu",clNewShiftUrno) == 0)
	{
		dbg(TRACE,"Account 61: Stopped, because <clNewShiftUrno> = undef!");
		return;
	}

	/* Status of the new recoed */
	if (ReadTemporaryField("NewRoss",clNewDrrStatus) == 0)
	{
		dbg(TRACE,"Account 61: <clNewDrrStatus> = undef!");
		clOldShiftUrno[0] = '\0';
	}
	/* start time */
	if (ReadTemporaryField("NewAvfr",clDrrStartTime) <= 0)
	{
		dbg(TRACE, "Account 61: <clDrrStartTime> = undef!");
		strcpy(clDrrStartTime,"0");
	}

	/* end time */
	if (ReadTemporaryField("NewAvto",clDrrEndTime) <= 0)
	{
		dbg(TRACE, "Account 61: <clDrrEndTime> = undef!");
		strcpy(clDrrEndTime,"0");
	}

	/* break duration */
	if (ReadTemporaryField("NewSblu",clDrrBreakTime) <= 0)
	{
		dbg(TRACE, "Account 61: <clDrrBreakTime> = undef!");
		strcpy(clDrrBreakTime,"0");
	}

	if (strcmp(clLastCommand,"URT") == 0) 
	{
		/* old start time */
		if (ReadTemporaryField("OldAvfr",clOldDrrStartTime) <= 0)
		{
			strcpy(clOldDrrStartTime,"0");
		}

		/* old end time */
		if (ReadTemporaryField("OldAvto",clOldDrrEndTime) <= 0)
		{
			strcpy(clOldDrrEndTime,"0");
		}

		/* old status */
		if (ReadTemporaryField("OldRoss",clOldDrrStatus) < 0)
		{
			clOldDrrStatus[0] = '\0';
		}

		/* old break duration */
		if (ReadTemporaryField("OldSblu",clOldDrrBreakTime) <= 0)
		{
			strcpy(clOldDrrBreakTime,"0");
		}

		/* BSDU of old DRR-record */
		if (ReadTemporaryField("OldBsdu",clOldShiftUrno) == 0)
		{
			strcpy(clOldShiftUrno,"0");
		}
	}

	/* calculate old value */
	dlOldValue = 0;
	if (strcmp(clLastCommand,"URT") == 0)
	{
		/* take only active records */
		if (clOldDrrStatus[0] == 'A')
		{
			ReadTemporaryField("OldIsCompensationNight",clIsCompensation);
			dlOldValue = GetValue61(clOldShiftUrno,clSday,clOldDrrStartTime,clOldDrrEndTime,clOldDrrBreakTime,clIsCompensation);
		}
		else
		{
			dbg(TRACE, "Account 61: Wrong status of old DRR-record: <clOldDrrStatus> = <%s>" ,clOldDrrStatus);
		}

		dbg(DEBUG,"Account 61: Old Value <dlOldValue> = <%ld>" , dlOldValue);
	}

	/* calculate new value */
	dlNewValue = 0;
	if (clNewDrrStatus[0] == 'A')
	{
		ReadTemporaryField("NewIsCompensationNight",clIsCompensation);
		dlNewValue = GetValue61(clNewShiftUrno,clSday,clDrrStartTime,clDrrEndTime,clDrrBreakTime,clIsCompensation);
	}
	else
	{
		dbg(TRACE, "Account 61: Wrong status of new DRR-record: <clNewDrrStatus> = <%s>" ,clNewDrrStatus);
	}
	dbg(DEBUG,"Account 61: New Value <dlNewValue> = <%lf>" ,dlNewValue);

	/* calculate difference */
	if (strcmp(clLastCommand,"DRT") != 0)
	{
		dlResultValue = dlNewValue - dlOldValue;
	}
	else
	{
		dlResultValue = - dlNewValue;
	}
	dbg(DEBUG,"Account 61: Result Value <dlResultValue> = <%lf>",dlResultValue);

	if (dlResultValue == 0)
	{
		dbg(DEBUG, "Account 61: Value not changed.");
		return;
	}

	/* get ACC record */
	strncpy(clYearIndex,&clSday[0],4);
	clYearIndex[4] = '\0';
	GetMultiAccCloseValueHandle(clStaffUrno,"61",clYearIndex,dlAccValue);
	strncpy(clMonthIndex,&clSday[4],2);
	clMonthIndex[2] = '\0';

	dbg(DEBUG,"Account 61: Month index <clMonthIndex>: <%s>",clMonthIndex);
	dbg(DEBUG,"Account 61: Old ACC value / month [%s] = <%lf>",clMonthIndex,dlAccValue[atoi(clMonthIndex)-1]);
	i = atoi(clMonthIndex);

	while  (i <= 12)
	{
		dlAccValue[i-1] = dlAccValue[i-1] + dlResultValue;
		dbg(DEBUG,"Account 61: New ACC value / month [%d] = <%lf>",i,dlAccValue[i-1]);
		i = i + 1;
	}

	/* save results */
	UpdateMultiAccountHandle("61",clStaffUrno,clYearIndex,"SCBHDL",dlAccValue);
	if (GetAccValueHandle("60","12",clYearIndex,clStaffUrno,"CL",clCl12Acc60))
	{
		dbg(DEBUG,"Account 61: CL-value account 60 = <%lf>",atof(clCl12Acc60));
		dbg(DEBUG,"Account 61: CL-value account 61 = <%lf>",dlAccValue[11]);
		dbg(DEBUG,"Account 61: Calling UpdateNextYearsValuesHandleAdjust() ...");
		UpdateNextYearsValuesHandleAdjust(clYearIndex,clStaffUrno,atof(clCl12Acc60),dlAccValue[11],"60","61");
	} 
}
