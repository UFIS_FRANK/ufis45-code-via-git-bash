/***************************************************************************/
/* GetHNormBasis(): delivers the norm working hours value from the NWHTAB  */
/***************************************************************************/
static BOOL	GetHNormBasis(const char *pcpContractCode,int ipYear,int ipMonth,double *dpHNormBasis)
{
	char	clHRxy[6];
	char	clNwhValue[8];
	char	clKeyValue_ResultBuffer[250];

	int	ilNwhHandle;

	/* get the handle of the MFMTAB (Minimum Free of Month) */
	ilNwhHandle = GetArrayIndex("NWHTAB");
	if (ilNwhHandle < 0)
	{
		dbg(TRACE,"GetHNormBasis(): ERROR - Array for NWHTAB not found!");
		return FALSE;
	}

	/* building field name */
	sprintf(clHRxy,"HR%02d",ipMonth);

	/* building key value to search */
	sprintf(clKeyValue_ResultBuffer,"%s,%02d",pcpContractCode,ipYear);

	/* searching fitting record */
	dbg(DEBUG,"GetHNormBasis(): Searching field value <> of key value <%s>",clHRxy,clKeyValue_ResultBuffer);
	if (FindFirstRecordByIndex(ilNwhHandle, clKeyValue_ResultBuffer) > 0)
	{
		if (GetFieldValueFromMonoBlockedDataString(clHRxy,sgRecordsets[ilNwhHandle].cFieldList,clKeyValue_ResultBuffer,clNwhValue) > 0)
		{
			*dpHNormBasis = atof(clNwhValue);
			dbg(DEBUG,"GetHNormBasis(): Value of <%s> = <%lf>",clHRxy,*clNwhValue);
			return TRUE;
		}
		else
		{
			dbg(TRACE,"GetHNormBasis(): NWH-field value of field <%s> not found!",clHRxy);
			return FALSE;
		}
	}
	else
	{
		dbg(TRACE,"GetHNormBasis(): NWH-record for contract <%s> and year <%d> not found!",pcpContractCode,ipYear);
		return FALSE;
	}
}

/***********************************************************/
/* Adaption of account 51 because of changes of account 23 */
/***********************************************************/
static BOOL	RecalcAcc51(int ilYear,const char *pcpStaffUrno)
{
	int	ilCounter;
	char	clMonth[4];
	char	clYear[6];
	double	dl26Array[12];
	double	dl23Array[12];
	double	dlResult[12];
	double	dl51OpenValue;

	dbg(DEBUG,"RecalcAcc51(): Start adaption of account 511...");

	sprintf(clYear,"%04d",ilYear);

	GetMultiAccCloseValueHandle(pcpStaffUrno,"26",clYear,dl26Array);
	GetMultiAccCorValueHandle(pcpStaffUrno,"23",clYear,dl23Array);

	dl51OpenValue = 0.0e0;

	for (ilCounter = 0; ilCounter < 12;ilCounter++)
	{
		dlResult[ilCounter] = dl26Array[ilCounter] - dl23Array[ilCounter] + dl51OpenValue;

		sprintf(clMonth,"%02d",ilCounter+1);

		dbg(DEBUG,"RecalcAcc51(): Open51Value <%lf>",dl51OpenValue);
		dbg(DEBUG,"RecalcAcc51(): Month <%s>",clMonth);
		dbg(DEBUG,"RecalcAcc51(): Year <%s>",clYear);
		dbg(DEBUG,"RecalcAcc51(): Empl <%s>",pcpStaffUrno);

		dl51OpenValue = dlResult[ilCounter];
		UpdateAccountHandle(dlResult[ilCounter],"51",clMonth,clYear,pcpStaffUrno,"XBSHDL");

		/*** �ber die Quartale werden keine Werte �bergeben, also OP Wert wird nicht gesetzt ***/
		/*switch(ilCounter)
		{
		case 3:
		case 6:
		case 9:
		case 12:
			dbg(DEBUG, "RecalcAcc51(): Update WITHOUT setting next month's open value (because last month of quarter).");
			UpdateAccountWithoutOpenHandle(dlResult[ilCounter-1],"51",clMonth,clYear,pcpStaffUrno,"XBSHDL");
			dl51OpenValue = 0.0e0;
		break;
		default:
			dbg(DEBUG, "RecalcAcc51(): Update WITH setting next month's open value.");
			dl51OpenValue = dlResult[ilCounter-1];
			UpdateAccountHandle(dlResult[ilCounter-1],"51",clMonth,clYear,pcpStaffUrno,"XBSHDL");
		}*/
		dbg(DEBUG,"RecalcAcc51(): New value of account 51 <%lf>", dlResult[ilCounter]);
	}

	return TRUE;
}

/****************************************************************************/
/* init_23: Initial-Account 23 - Norm Working Hours of month				*/
/* example: 40hBasis = 176.0hNorm � 37hvertraglich = 162,8hNorm				*/
/****************************************************************************/
static void bas_init_23()
{
	char	clFrom[16];
	char	clYearFrom[6];
	char	clMonFrom[4];
	char	clDayFrom[4];

	char	clTo[16];
	char	clYearTo[6];
	char	clMonTo[4];
	char	clDayTo[4];

	char	clCurrentEmpl[12];

	SCODATA		olScoData;
	STAFFDATA	olStfData;
	COTDATA		olCotData;

	double	dlHBasis = 40;
	double	dlHNorm = 0;
	double	dlHNormBasis = 0;
	char	clHBasis[128];
	char	clHNorm[128];
	char	clCalcDate[16];

	int	ilCountEmpl;
	int	ilCounter;
	int	ilYearCount;
	int	ilMonthCount;

	dbg(DEBUG,"============================= INIT ACCOUNT INIT_23 START =============================");

	/* checking the data */
	if (strlen(pcgData) == NULL)
	{
		dbg(TRACE,"Account Init 23: Stopped, because <pcgData> = undef!");
		return;
	}
	dbg(DEBUG,"Account Init 23: <pcgData> = <%s>",pcgData);

	/* checking the fields */
	if (strlen(pcgFields) == 0)
	{
		dbg(TRACE,"Account Init 23: Stopped, because <pcgFields> = undef!");
		return;
	}
	dbg(DEBUG, "Account Init 23: <pcgFields> = <%s>",pcgFields);

	/* checking the selection */
	if (strlen(pcgSelKey) == 0)
	{
		dbg(TRACE,"Account Init 23: Stopped, because <pcgSelKey> = undef!");
		return;
	}
	dbg(DEBUG, "Account Init 23: <pcgSelKey> = <%s>",pcgSelKey);

	/* get start date */
	if (GetFieldValueFromMonoBlockedDataString("FROM",pcgFields,pcgData,clFrom) <= 0)
	{
		dbg(TRACE,"Account Init 23: Stopped, because <clFrom> = undef!");
		return;
	}
	strncpy(clYearFrom,&clFrom[0],4);
	clYearFrom[4] = '\0';
	strncpy(clMonFrom,&clFrom[4],2);
	clMonFrom[2] = '\0';
	strncpy(clDayFrom,&clFrom[6],2);
	clDayFrom[2] = '\0';

	/* get end date */
	if (GetFieldValueFromMonoBlockedDataString("TO",pcgFields,pcgData,clTo) <= 0)
	{
		dbg(TRACE,"Account Init 23: Stopped, because <clTo> = undef!");
		return;
	}
	strncpy(clYearTo,&clTo[0],4);
	clYearTo[4] = '\0';
	strncpy(clMonTo,&clTo[4],2);
	clMonTo[2] = '\0';
	strncpy(clDayTo,&clTo[6],2);
	clDayTo[2] = '\0';

	dbg(DEBUG, "Account Init 23: Initializing from %s to %s.",clFrom,clTo);

	/* count number of employees */
	ilCountEmpl = CountElements(pcgSelKey);
	if (ilCountEmpl < 0)
	{
		dbg(TRACE,"Account Init 23: Stopped, because no employees to initialize!");
		return;
	}
	dbg(DEBUG,"Account Init 23: Number of employees = <%d>", ilCountEmpl);

	/* get Norm basis from PARTAB */
	if (GetPartabValue("ID_NORM_H_BASIS",clHBasis))
	{
		dlHBasis = atof(clHBasis);
		dbg(DEBUG, "Account Init 23: 'ID_NORM_H_BASIS' from PARTAB = <%lf>",dlHBasis);
	}
	else
	{
		dbg(TRACE, "Account Init 23: Couldn't get parameter 'ID_NORM_H_BASIS' from PARTAB! Setting to default <%lf>",dlHBasis);
	}

	/* loop per employee */
	for (ilCounter = 0; ilCounter < ilCountEmpl; ilCounter++)
	{
		if (GetElement(pcgSelKey,ilCounter,clCurrentEmpl) <= 0)
		{
			dbg(TRACE,"Account Init 23: Stopped, because <clCurrentEmpl> = undef!");
			return;
		}

		/* get staff data containing e.g. PENO */
		GetStaffData(clCurrentEmpl,&olStfData);

		/* Used in UpdateAccountXY! */
		WriteTemporaryField("Personalnummer",olStfData.cmPersonalnummer);

		dbg(DEBUG,"Account Init 23: Start calculation for employee <%s>",clCurrentEmpl);

		/* loop through the years */
		for (ilYearCount = atoi(clYearFrom); ilYearCount <= atoi(clYearTo); ilYearCount++)
		{
			/* loop through the months */
			for (ilMonthCount = atoi(clMonFrom); ilMonthCount <= atoi(clMonTo); ilMonthCount++)
			{
				/* reset values */
				dlHNorm 	 = 0.0e0;
				dlHNormBasis = 0.0e0;

				/* build time-variable for calculation */
				sprintf(clCalcDate,"%4d%02d%02d",ilYearCount,ilMonthCount,1);
				dbg(DEBUG,"Account Init 23: Start calculation for date <%s>",clCalcDate);

				/* get SCO-data */
				if (GetScoData(clCurrentEmpl,clCalcDate,&olScoData) == 0)
				{
					dbg(TRACE,"CalculateInit23(): ERROR - Couldn't get contract data of employee <%s>!",clCurrentEmpl);
					continue;
				}

				/* get COT-data */
				if (GetCotDataByCtrc(olScoData.cmScoCode,&olCotData) == 0)
				{
					dbg(TRACE,"CalculateInit23(): ERROR - Didn't find contract <%s>!",olScoData.cmScoCode);
					continue;
				}

				/* calculate not for all employees */
				if (olCotData.cmINBU[0] != 'x')
				{
					dbg(TRACE,"CalculateInit23(): No calculation for employee with PENO <%s>!",olStfData.cmPersonalnummer);
					dbg(TRACE,"CalculateInit23(): Contract-flag INBU (in budget/account calculation) not set!");
					continue;
				}

				/* check where to take the agreed hours from */
				if (olScoData.dmVereinbarteWochenstunden == 0)
				{
					olScoData.dmVereinbarteWochenstunden = olCotData.dmVereinbarteWochenstunden;
				}

				/* count back CWEH to h per week */
				olScoData.dmVereinbarteWochenstunden = (olScoData.dmVereinbarteWochenstunden * 5) / 60;

				/* get norm hours defined in NWHTAB for contract, year and month */
				if (GetHNormBasis(olScoData.cmScoCode,ilYearCount,ilMonthCount,&dlHNormBasis) == 0)
				{
					dbg(TRACE,"Account Init 23: ERROR in 'GetHNormBasis'! Calculating next employee!");
					continue;
				}

				/* calculate result */
				dlHNorm = dlHNormBasis * olScoData.dmVereinbarteWochenstunden / dlHBasis;

				dbg(DEBUG,"Account Init 23(): Contract code                = %s",olScoData.cmScoCode);
				dbg(DEBUG,"Account Init 23(): Agreed weekly working hours  = %lf",olScoData.dmVereinbarteWochenstunden);
				dbg(DEBUG,"Account Init 23(): Basis norm hours of contract = %lf",dlHNormBasis);
				dbg(DEBUG,"Account Init 23(): Global norm hours            = %lf",dlHBasis);
				dbg(DEBUG,"Account Init 23(): --------------------------------------------------");
				dbg(DEBUG,"Account Init 23(): Result value                 = %lf" ,dlHNorm);

				/* round and cut */
				sprintf(clHNorm,"%-8.2lf",dlHNorm);
				dlHNorm = atof(clHNorm);

				/* write to array to save */
				UpdateAccountXYHandle(clCurrentEmpl,dlHNorm,"23",clCalcDate,"XBSHDL","CO");
			}

			/* adapt values of account 51 */
			RecalcAcc51(ilYearCount,clCurrentEmpl);
		}
	}
}
