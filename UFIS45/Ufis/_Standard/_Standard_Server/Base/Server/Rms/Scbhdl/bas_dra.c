/***********************************************************************/
/****** Account DRA: compensation hours for temporary staff 	********/
/******	Regarding daily roster absences				********/	
/***********************************************************************/

void bas_dra()
{
	char	clLastCommand[6];
	char	clStaffUrno[12];
	char	clSday[16];
	char	clYear[6];
	char	clMonth[4];

	char	clAbfr[16];
	char	clAbto[16];
	char	clCode[10];
	char	clBsdu[12];

	BOOL 	blIsRegFree;
	char 	clIsLikeWork[2];
	BOOL 	blIsShiftChangeoverDay;
	BOOL 	blIsTraining;
	BOOL 	blIsCompensation;
	BOOL 	blIsCompensationNight;
	BOOL 	blIsHoliday;
	BOOL 	blIsIllness;

	char	clOldAbfr[16];
	char	clOldAbto[16];
	char	clOldCode[10];
	char	clOldBsdu[12];

	BOOL	blOldIsRegFree;
	char 	clOldIsLikeWork[2];
	BOOL 	blOldIsShiftChangeoverDay;
	BOOL 	blOldIsTraining;
	BOOL 	blOldIsCompensation;
	BOOL 	blOldIsCompensationNight;
	BOOL 	blOldIsHoliday;
	BOOL 	blOldIsIllness;

	double	dlNewAccValue30[12];
	double	dlNewAccValue33[12];
	double	dlNewAccValue60[12];
	double	dlNewAccValue61[12];
	int	i;

	double	dlNewValue30;
	double	dlNewValue33;
	double	dlNewValue60;
	double	dlNewValue61;

	double	dlOldValue30;
	double	dlOldValue33;
	double	dlOldValue60;
	double	dlOldValue61;

	double	dlResultValue30;
	double	dlResultValue33;
	double	dlResultValue60;
	double	dlResultValue61;

	STAFFDATA	olStfData;

	char	clLocalYear[6];
	char	clIsRecalc[2];

	int	ilAccHandle = -1;

	dbg(DEBUG, "============================= ACCOUNT 33 DRA BEGIN ===========================");

	blIsRegFree = FALSE;
	strcpy(clIsLikeWork,"0");
	blIsShiftChangeoverDay = FALSE;
	blIsTraining = FALSE;
	blIsCompensation = FALSE;
	blIsCompensationNight = FALSE;
	blIsHoliday = FALSE;
	blIsIllness = FALSE;

	blOldIsRegFree = FALSE;
	strcpy(clOldIsLikeWork,"0");
	blOldIsShiftChangeoverDay = FALSE;
	blOldIsTraining = FALSE;
	blOldIsCompensation = FALSE;
	blOldIsCompensationNight = FALSE;
	blOldIsHoliday = FALSE;
	blOldIsIllness = FALSE;

	/****** Ermittlung des Kommandos (IRT,URT,DRT) ****/
	strncpy(clLastCommand,GetLastCommand(),3);
	clLastCommand[3] = '\0';
	dbg(DEBUG,"Account 33DRA: Command (%s)",clLastCommand);


	/* checking the data */
	if (strlen(pcgData) == NULL)
	{
		dbg(TRACE,"Account 33DRA: Stopped, because <pcgData> = undef!");
		return;
	}
	dbg(DEBUG,"Account 33DRA: pcgData = <%s>",pcgData);

	/* checking the fields */
	if (strlen(pcgFields) == 0)
	{
		dbg(TRACE,"Account 33DRA: Stopped, because <pcgFields> = undef!");
		return;
	}
	dbg(DEBUG, "Account 33DRA: pcgFields = <%s>",pcgFields);

	/* checking the URNO of the employee */
	if (GetFieldValueFromMonoBlockedDataString("STFU",pcgFields,pcgData,clStaffUrno) == 0)
	{
		dbg(TRACE,"Account 33DRA: Stopped, because <clStaffUrno> = undef!");
		return;
	}

	/* checking the SDAY */
	if (GetFieldValueFromMonoBlockedDataString("SDAY",pcgFields,pcgData,clSday) == 0)
	{
		dbg(TRACE,"Account 33DRA: Stopped, because <clSday> = undef!");
		return;
	}
	strncpy(clYear,clSday,4);
	clYear[4] = '\0';
	strncpy(clMonth,&clSday[4],2);
	clMonth[2] = '\0';

	/* checking the BSDU */
	if (GetFieldValueFromMonoBlockedDataString("BSDU",pcgFields,pcgData,clBsdu) == 0)
	{
		dbg(TRACE,"Account 33DRA: Stopped, because <clBsdu> = undef!");
		return;
	}

	/****** Startzeit ******/
	if (GetFieldValueFromMonoBlockedDataString("ABFR",pcgFields,pcgData,clAbfr) == 0)
	{
		dbg(TRACE,"Account 33DRA: <clAbfr> = undef!");
	}

	/****** Endzeit ********/
	if (GetFieldValueFromMonoBlockedDataString("ABTO",pcgFields,pcgData,clAbto) == 0)
	{
		dbg(TRACE,"Account 33DRA: <clAbto> = undef!");
	}

	/****** Schicht Code ********/
	if (GetFieldValueFromMonoBlockedDataString("SDAC",pcgFields,pcgData,clCode) == 0)
	{
		dbg(TRACE,"Account 33DRA: <clCode> = undef!");
	}

	dbg(DEBUG,"Account 33DRA: clCode <%s>!" ,clCode);

	/*** Which absence type is it ? ***/
	GetODATypes(clBsdu,&blIsRegFree,&blIsShiftChangeoverDay,&blIsTraining,&blIsCompensation,&blIsCompensationNight,&blIsHoliday,&blIsIllness);

	if (blIsRegFree || blIsShiftChangeoverDay)
	{
		dbg(TRACE,"Account 33DRA: IsRegFree || IsShiftChangeoverDay aren't valid codes for DRA");
		return;
	}

	/*** Wie Schicht zu behandeln ? ***/
	GetOdaWorkData(clBsdu,clIsLikeWork);
	if (strcmp(clIsLikeWork,"1") == 0)
	{
		clIsLikeWork[0] = '0';
	}
	else
	{
		clIsLikeWork[0] = '1';
	}

	/************************* im Falle eines Updates alte Daten auslesen *****************************/
	if (strcmp(clLastCommand,"URT") == 0)
	{
		/****** Im Falle eines Updates wird mit dieser Funktion der alte Datensatz ausgelesen	**/
		if (strlen(pcgOldData) == NULL)
		{
			dbg(TRACE,"Account 33DRA: Stopped, because <pcgOldData> = undef!");
			return;
		}
		dbg(DEBUG,"Account 33DRA: pcgOldData = <%s>",pcgOldData);

		/* checking the BSDU */
		if (GetFieldValueFromMonoBlockedDataString("BSDU",pcgFields,pcgOldData,clOldBsdu) == 0)
		{
			dbg(TRACE,"Account 33DRA: Stopped, because <clOldBsdu> = undef!");
			return;
		}

		/****** Startzeit ******/
		if (GetFieldValueFromMonoBlockedDataString("ABFR",pcgFields,pcgOldData,clOldAbfr) == 0)
		{
			dbg(TRACE,"Account 33DRA: <clOldAbfr> = undef!");
		}

		/****** Endzeit ********/
		if (GetFieldValueFromMonoBlockedDataString("ABTO",pcgFields,pcgOldData,clOldAbto) == 0)
		{
			dbg(TRACE,"Account 33DRA: <clOldAbto> = undef!");
		}

		/****** Schicht Code ********/
		if (GetFieldValueFromMonoBlockedDataString("SDAC",pcgFields,pcgOldData,clOldCode) == 0)
		{
			dbg(TRACE,"Account 33DRA: <clOldCode> = undef!");
		}


		dbg(DEBUG,"Account 33DRA: clOldCode <%s>!" ,clOldCode);

		/*** Which absence type is it ? ***/
		GetODATypes(clOldBsdu,&blOldIsRegFree,&blOldIsShiftChangeoverDay,&blOldIsTraining,&blOldIsCompensation,&blOldIsCompensationNight,&blOldIsHoliday,&blOldIsIllness);

		if (blOldIsRegFree || blOldIsShiftChangeoverDay)
		{
			dbg(TRACE,"Account 33DRA: IsOldRegFree && IsOldShiftChangeoverDay aren't valid codes for DRA");
			return;
		}

		/*** Wie Schicht zu behandeln ? ***/
		GetOdaWorkData(clOldBsdu,clOldIsLikeWork);
		if (strcmp(clOldIsLikeWork,"1") == 0)
		{
			clOldIsLikeWork[0] = '0';
		}
		else
		{
			clOldIsLikeWork[0] = '1';
		}
	}

	/*** Mitarbeiter Daten raussuchen ***/
	GetStaffData(clStaffUrno,&olStfData);

	/*** Ausgabe ***/
	dbg(DEBUG, "Account 33DRA: Employee name = %s",olStfData.cmMaName);
	dbg(DEBUG, "Account 33DRA: PENO          = %s",olStfData.cmPersonalnummer);

	WriteTemporaryField("Personalnummer",olStfData.cmPersonalnummer);

	/* increment reference counter to staff accounts */
	ReadTemporaryField("IsRecalc",clIsRecalc);
	if (strcmp(clIsRecalc,"0") == 0)
	{
		StfToAccPrefetch(clStaffUrno,clYear,TRUE);
		sprintf(clLocalYear,"%d",atoi(clYear)+1);
		StfToAccPrefetch(clStaffUrno,clLocalYear,TRUE);
	}

	/* Werte mit altem DRA berechnen */
	dlOldValue30 = 0;
	dlOldValue33 = 0;
	dlOldValue60 = 0;
	dlOldValue61 = 0;

	/* Calculate old value for update and delete */
	if (strcmp(clLastCommand,"URT") == 0)
	{
		/**** Night working hours Account 60 ****/
		dlOldValue60 = - CalcNightWorkTime(clOldAbfr,clOldAbto,"0",clOldIsLikeWork);

		/**** Overtime compensation Account 33 ****/
		if (blOldIsCompensation)
		{
			dlOldValue33 = GetValue33(clOldBsdu,clSday,clOldAbfr,clOldAbto,"0","1");
		}

		/**** Night working hours Compensation Account 61 ****/
		if (blOldIsCompensationNight)
		{
			dlOldValue61 = GetValue61(clOldBsdu,clSday,clOldAbfr,clOldAbto,"0","1");
		}

		/**** Holiday hours Account 30	****/
		if (blOldIsHoliday)
		{
			dlOldValue30 = GetValue30("1",clSday,clOldAbfr,clOldAbto,"0");
		}

		dbg(DEBUG,"Account 33DRA : (oldValue30) <%lf>" ,dlOldValue30);
		dbg(DEBUG,"Account 33DRA : (oldValue33) <%lf>" ,dlOldValue33);
		dbg(DEBUG,"Account 33DRA : (oldValue60) <%lf>" ,dlOldValue60);
		dbg(DEBUG,"Account 33DRA : (oldValue61) <%lf>" ,dlOldValue61);
	}

	/* Werte mit neuem DRR berechnen */
	dlNewValue30 = 0;
	dlNewValue33 = 0;
	dlNewValue60 = 0;
	dlNewValue61 = 0;

	/**** Night working hours Account 60 ****/
	dlNewValue60 = - CalcNightWorkTime(clAbfr,clAbto,"0",clIsLikeWork);

	/**** Overtime compensation Account 33 ****/
	if (blIsCompensation)
	{
		dlNewValue33 = GetValue33(clBsdu,clSday,clAbfr,clAbto,"0","1");
	}

	/**** Night working hours Compensation Account 61 ****/
	if (blIsCompensationNight)
	{
		dlNewValue61 = GetValue61(clBsdu,clSday,clAbfr,clAbto,"0","1");
	}

	/**** Holiday hours Account 30	****/
	if (blIsHoliday)
	{
		dlNewValue30 = GetValue30("1",clSday,clAbfr,clAbto,"0");
	}

	dbg(DEBUG,"Account 33DRA : (newValue30) <%lf>" ,dlNewValue30);
	dbg(DEBUG,"Account 33DRA : (newValue33) <%lf>" ,dlNewValue33);
	dbg(DEBUG,"Account 33DRA : (newValue60) <%lf>" ,dlNewValue60);
	dbg(DEBUG,"Account 33DRA : (newValue61) <%lf>" ,dlNewValue61);


	/**********************************************************************************************************/
	/***************************************** Differenz berechnen*********************************************/
	/**********************************************************************************************************/
	/***** ACHTUNG: Im Falle eines Deletes wird der geloeschte DRA im neuen Datensatz uebertragen, deshalb hier abziehen ***/
	if (strcmp(clLastCommand,"DRT") != 0)
	{
		dlResultValue30 = dlNewValue30 - dlOldValue30;
		dlResultValue33 = dlNewValue33 - dlOldValue33;
		dlResultValue60 = dlNewValue60 - dlOldValue60;
		dlResultValue61 = dlNewValue61 - dlOldValue61;
	}
	else
	{
		dlResultValue30 = - dlNewValue30;
		dlResultValue33 = - dlNewValue33;
		dlResultValue60 = - dlNewValue60;
		dlResultValue61 = - dlNewValue61;
	}

	/***************************************** ACC Datensatz veraendern und sichern ****************************/
	/***********************************************************************************************************/
	/**** Pr�fen ob Ver�nderung vorliegt ****/

	dbg(DEBUG,"Account 33DRA : resultValue30:<%lf>",dlResultValue30);
	dbg(DEBUG,"Account 33DRA : resultValue33:<%lf>",dlResultValue33);
	dbg(DEBUG,"Account 33DRA : resultValue60:<%lf>",dlResultValue60);
	dbg(DEBUG,"Account 33DRA : resultValue61:<%lf>",dlResultValue61);

	GetMultiAccCloseValueHandle(clStaffUrno,"30",clYear,dlNewAccValue30);
	GetMultiAccCloseValueHandle(clStaffUrno,"33",clYear,dlNewAccValue33);
	GetMultiAccCloseValueHandle(clStaffUrno,"60",clYear,dlNewAccValue60);
	GetMultiAccCloseValueHandle(clStaffUrno,"61",clYear,dlNewAccValue61);

	dbg(DEBUG,"Account 33DRA: Old ACC 30 value / month [%s] = <%lf>",clMonth,dlNewAccValue30[atoi(clMonth)-1]);
	dbg(DEBUG,"Account 33DRA: Old ACC 33 value / month [%s] = <%lf>",clMonth,dlNewAccValue33[atoi(clMonth)-1]);
	dbg(DEBUG,"Account 33DRA: Old ACC 60 value / month [%s] = <%lf>",clMonth,dlNewAccValue60[atoi(clMonth)-1]);
	dbg(DEBUG,"Account 33DRA: Old ACC 61 value / month [%s] = <%lf>",clMonth,dlNewAccValue61[atoi(clMonth)-1]);

	i = atoi(clMonth);

	dbg(DEBUG,"Account 33DRA: Month index: <%s>",clMonth);

	while(i <= 12)
	{
		dlNewAccValue30[i-1] = dlNewAccValue30[i-1] - dlResultValue30;
		dbg(DEBUG,"Account 33DRA: New ACC 30 value / month [%d] = <%lf>",i,dlNewAccValue30[i-1]);
		dlNewAccValue33[i-1] = dlNewAccValue33[i-1] + dlResultValue33;
		dbg(DEBUG,"Account 33DRA: New ACC 33 value / month [%d] = <%lf>",i,dlNewAccValue33[i-1]);
		dlNewAccValue60[i-1] = dlNewAccValue60[i-1] + dlResultValue60;
		dbg(DEBUG,"Account 33DRA: New ACC 60 value / month [%d] = <%lf>",i,dlNewAccValue60[i-1]);
		dlNewAccValue61[i-1] = dlNewAccValue61[i-1] + dlResultValue61;
		dbg(DEBUG,"Account 33DRA: New ACC 61 value / month [%d] = <%lf>",i,dlNewAccValue61[i-1]);
		i = i + 1;
	}

	UpdateMultiAccountHandle("30",clStaffUrno,clYear,"SCBHDL",dlNewAccValue30);
	UpdateMultiAccountHandle("33",clStaffUrno,clYear,"SCBHDL",dlNewAccValue33);
	UpdateMultiAccountHandle("60",clStaffUrno,clYear,"SCBHDL",dlNewAccValue60);
	UpdateMultiAccountHandle("61",clStaffUrno,clYear,"SCBHDL",dlNewAccValue61);

	if (strcmp(clIsRecalc,"0") == 0)
	{
		/* write account changes to DB	*/
		ilAccHandle = GetArrayIndex("ACCHDL");
		if (ilAccHandle >= 0)
		{
			CloseRecordset(ilAccHandle,1);
		}

		/* decrement reference counter to staff accounts */
		StfToAccPrefetch(clStaffUrno,clYear,FALSE);
		sprintf(clLocalYear,"%d",atoi(clYear)+1);
		StfToAccPrefetch(clStaffUrno,clLocalYear,FALSE);
	}
}
