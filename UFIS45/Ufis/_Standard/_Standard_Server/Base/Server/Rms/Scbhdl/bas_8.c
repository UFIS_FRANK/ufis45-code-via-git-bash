/*****************************/
/* 11: free days in the quarter */
/*****************************/
void bas_8()
{

	char	clIsAushilfe[2];
	char	clStaffUrno[12];
	char	clSday[16];
	char	clSourceMonth[4];
	char	clYear[6];
	double	dlAccValue[12];
	double	dlNewAccValue[12];
	double	dlResult;
	char	clDate[16];

	dbg(DEBUG, "============================= ACCOUNT 8 START ===========================");

	/* checking temporary staff */
	if (ReadTemporaryField("IsAushilfe",clIsAushilfe) == 1 && clIsAushilfe[0] == '1')
	{
		dbg(TRACE,"Account 8: Stopped, because of temporary staff");
		return;
	}

	/* checking the data */
	if (strlen(pcgData) == NULL)
	{
		dbg(TRACE,"Account 8: Stopped, because <pcgData> = undef!");
		return;
	}
	dbg(DEBUG,"Account 8: pcgData = <%s>",pcgData);

	/* checking the fields */
	if (strlen(pcgFields) == 0)
	{
		dbg(TRACE,"Account 8: Stopped, because <pcgFields> = undef!");
		return;
	}
	dbg(DEBUG, "Account 8: pcgFields = <%s>",pcgFields);

	/* checking the URNO of the employee */
	if (ReadTemporaryField("StfUrno",clStaffUrno) == 0)
	{
		dbg(TRACE,"Account 8: Stopped, because <clStaffUrno> = undef!");
		return;
	}

	/* checking the SDAY */
	if (ReadTemporaryField("SDAY",clSday) == 0)
	{
		dbg(TRACE,"Account 8: Stopped, because <clSday> = undef!");
		return;
	}

	/* get ACC record */
	clSourceMonth[0] = clSday[4];
	clSourceMonth[1] = clSday[5];
	clSourceMonth[2] = '\0';
	
	strncpy(clYear,clSday,4);
	clYear[4] = '\0';

	dlResult = 0;
	dbg(DEBUG, "Account 8: Year  <%s>",clYear);
	dbg(DEBUG, "Account 8: Month <%s>",clSourceMonth);

	/* get values */
	GetMultiAccCloseValueHandle(clStaffUrno,"5",clYear,dlAccValue);
	memcpy(dlNewAccValue,dlAccValue,sizeof(dlAccValue));

	strcpy(clDate,clYear);
	/* first quarter */
	if (strcmp(clSourceMonth,"01") == 0 || strcmp(clSourceMonth,"02") == 0 || strcmp(clSourceMonth,"03") == 0)
	{
		dlResult = dlAccValue[0] + dlAccValue[1] + dlAccValue[2];
		dbg(DEBUG, "Account 8: Sum of 1st quarter: <%lf>", dlResult);

		dlNewAccValue[0] = dlResult;
		dlNewAccValue[1] = dlResult;
		dlNewAccValue[2] = dlResult;

		/* Save */
		strcpy(&clDate[4],"0101");
		UpdateAccountXYHandle(clStaffUrno,dlResult,"8",clDate,"SCBHDL","CL");
		strcpy(&clDate[4],"0201");
		UpdateAccountXYHandle(clStaffUrno,dlResult,"8",clDate,"SCBHDL","CL");
		strcpy(&clDate[4],"0301");
		UpdateAccountXYHandle(clStaffUrno,dlResult,"8",clDate,"SCBHDL","CL");
	}

	/* second quarter */
	else if (strcmp(clSourceMonth,"04") == 0 || strcmp(clSourceMonth,"05") == 0 || strcmp(clSourceMonth,"06") == 0)
	{
		dlResult = dlAccValue[3] + dlAccValue[4] + dlAccValue[5];

		dbg(DEBUG, "Account 8: Sum of 2nd quarter: <%lf>", dlResult);

		dlNewAccValue[3] = dlResult;
		dlNewAccValue[4] = dlResult;
		dlNewAccValue[5] = dlResult;

		/* Save */
		strcpy(&clDate[4],"0401");
		UpdateAccountXYHandle(clStaffUrno,dlResult,"8",clDate,"SCBHDL","CL");
		strcpy(&clDate[4],"0501");
		UpdateAccountXYHandle(clStaffUrno,dlResult,"8",clDate,"SCBHDL","CL");
		strcpy(&clDate[4],"0601");
		UpdateAccountXYHandle(clStaffUrno,dlResult,"8",clDate,"SCBHDL","CL");
	}

	/* third quarter */
	else if (strcmp(clSourceMonth,"07") == 0 || strcmp(clSourceMonth,"08") == 0 || strcmp(clSourceMonth,"09") == 0)
	{
		dlResult = dlAccValue[6] + dlAccValue[7] + dlAccValue[8];

		dbg(DEBUG, "Account 8: Sum of 3rd quarter: <%lf>", dlResult);

		dlNewAccValue[6] = dlResult;
		dlNewAccValue[7] = dlResult;
		dlNewAccValue[8] = dlResult;

		/* Save */
		strcpy(&clDate[4],"0701");
		UpdateAccountXYHandle(clStaffUrno,dlResult,"8",clDate,"SCBHDL","CL");
		strcpy(&clDate[4],"0801");
		UpdateAccountXYHandle(clStaffUrno,dlResult,"8",clDate,"SCBHDL","CL");
		strcpy(&clDate[4],"0901");
		UpdateAccountXYHandle(clStaffUrno,dlResult,"8",clDate,"SCBHDL","CL");
	}

	/* fourth quarter */
	else if (strcmp(clSourceMonth,"10") == 0 || strcmp(clSourceMonth,"11") == 0 || strcmp(clSourceMonth,"12") == 0)
	{
		dlResult = dlAccValue[9] + dlAccValue[10] + dlAccValue[11];

		dbg(DEBUG, "Account 8: Sum of 4th quarter: <%lf>", dlResult);

		dlNewAccValue[9]  = dlResult;
		dlNewAccValue[10] = dlResult;
		dlNewAccValue[11] = dlResult;


		/* Save */
		strcpy(&clDate[4],"1001");
		UpdateAccountXYHandle(clStaffUrno,dlResult,"8",clDate,"SCBHDL","CL");
		strcpy(&clDate[4],"1101");
		UpdateAccountXYHandle(clStaffUrno,dlResult,"8",clDate,"SCBHDL","CL");
		strcpy(&clDate[4],"1201");
		UpdateAccountXYHandle(clStaffUrno,dlResult,"8",clDate,"SCBHDL","CL");
	}


	SetNextScript("/ceda/bas/11.bbf");
	dbg(DEBUG, "Account 8: next account to be processed: account 11");
}
