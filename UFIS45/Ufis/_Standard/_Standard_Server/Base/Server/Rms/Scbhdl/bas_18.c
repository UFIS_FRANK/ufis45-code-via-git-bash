/*********************************************/
/* ACCOUNT 18: number of illnesses per month */
/* *******************************************/
static void bas_18()
{
	char 	clLastCommand[4];
	char	clStaffUrno[12];
	char	clSday[16];
	char	clNewDrrRoss[2];
	char	clOldShiftUrno[12];
	char	clOldDrrRoss[2];
	char	clYear[6];
	char	clMonth[4];
	char	clOldAccValue[128];
	char	clNewShiftUrno[12];
	int	ilOldValue;
	int	ilNewValue;
	int	ilResultValue;
	double	dlNewAccValue;

	dbg(DEBUG, "============================= ACCOUNT 18 START ===========================");

	/* get last command (IRT,URT,DRT) */
	strncpy(clLastCommand,GetLastCommand(),3);
	clLastCommand[3] = '\0';
	dbg(DEBUG,"Account 18: Command: (%s)",clLastCommand);

	/* checking the data */
	if (strlen(pcgData) == NULL)
	{
		dbg(TRACE,"Account 18: Stopped, because <pcgData> = undef!");
		return;
	}
	dbg(DEBUG,"Account 18: pcgData = <%s>",pcgData);

	/* checking the fields */
	if (strlen(pcgFields) == 0)
	{
		dbg(TRACE,"Account 18: Stopped, because <pcgFields> = undef!");
		return;
	}
	dbg(DEBUG, "Account 18: pcgFields = <%s>",pcgFields);

	/* checking the URNO of the employee */
	if (ReadTemporaryField("StfUrno",clStaffUrno) == 0)
	{
		dbg(TRACE,"Account 18: Stopped, because <clStaffUrno> = undef!");
		return;
	}

	/* checking the SDAY */
	if (ReadTemporaryField("SDAY",clSday) == 0)
	{
		dbg(TRACE,"Account 18: Stopped, because <clSday> = undef!");
		return;
	}

	/* BSDU of new DRR-record */
	if (ReadTemporaryField("NewBsdu",clNewShiftUrno) == 0)
	{
		dbg(TRACE,"Account 18: Stopped, because <clNewShiftUrno> = undef!");
		return;
	}

	/* Status des neuen Datensatzes	*/
	if (ReadTemporaryField("NewRoss",clNewDrrRoss) == 0)
	{
		dbg(TRACE,"Account 18: <clNewDrrRoss> = undef!");
		clOldShiftUrno[0] = '\0';
	}

	/* if URT get old account relevant values */
	if (strcmp(clLastCommand,"URT") == 0)
	{
		/* checking the old data */
		if (strlen(pcgOldData) == 0)
		{
			dbg(TRACE,"Account 18: <pcgOldData> = undef!");
		}
		else
		{
			dbg(DEBUG,"Account 18: <pcgOldData> = <%s>",pcgOldData);
		}

		/* BSDU of old DRR-record */
		if (ReadTemporaryField("OldBsdu",clOldShiftUrno) == 0)
		{
			dbg(TRACE, "Account 18: <clOldShiftUrno> = undef!");
			clOldShiftUrno[0] = '\0';
		}

		/* status of old record */
		if (ReadTemporaryField("OldRoss",clOldDrrRoss) == 0)
		{
			dbg(TRACE, "Account 18: <clOldDrrRoss> = undef!");
			clOldDrrRoss[0] = '\0';
		}
	}

	/* starting calculation */
	/* getting old value */
	ilOldValue = 0;
	if (strcmp(clLastCommand,"URT") == 0)
	{
		if (strcmp(clOldShiftUrno,"0") != 0 &&  (clOldShiftUrno[0] != '\0'))
		{
			/* use only active DRR-records */
			if (strcmp(clOldDrrRoss,"A") == 0)
			{
				char clOldValue[2];
				if (ReadTemporaryField("OldIsIllness",clOldValue) == 1 && clOldValue[0] == '1')
				{
					ilOldValue = 1;
				}
				else
				{
					ilOldValue = 0;
				}
			}
			else
			{
				dbg(TRACE, "Account 18: Wrong status of old DRR-record: <clOldDrrRoss> = <%s>" ,clOldDrrRoss);
			}
		}
	}

	/* getting new value */
	ilNewValue = 0;

	/* use only active DRR-records */
	if (strcmp(clNewDrrRoss,"A") == 0)
	{
		char clNewValue[2];
		if (ReadTemporaryField("NewIsIllness",clNewValue) == 1 && clNewValue[0] == '1')
		{
			ilNewValue = 1;
		}
		else
		{
			ilNewValue = 0;
		}
		dbg(DEBUG, "Account 18: <ilNewValue> = <%d>",ilNewValue);
	}
	else
	{
		dbg(TRACE, "Account 18: Wrong status of new DRR-record: <clNewDrrRoss> = <%s>" ,clNewDrrRoss);
	}

	/* calculate difference */
	if (strcmp(clLastCommand,"DRT") != 0)
	{
		ilResultValue = ilNewValue - ilOldValue;
	}
	else
	{
		ilResultValue = - ilNewValue; 
	}

	clMonth[0] = clSday[4];
	clMonth[1] = clSday[5];
	clMonth[2] = '\0';

	strncpy(clYear,clSday,4);
	clYear[4] = '\0';

	/* get ACC record */
	GetAccValueHandle("18",clMonth,clYear,clStaffUrno,"CL",clOldAccValue);

	dbg(DEBUG, "Account 18: Old ACC value: <%s>", clOldAccValue);

	dlNewAccValue = atof(clOldAccValue) + ilResultValue;

	dbg(DEBUG, "Account 18: New ACC value: <%lf>",dlNewAccValue);

	if (ilResultValue != 0)
	{
		UpdateAccountHandle(dlNewAccValue,"18",clMonth,clYear,clStaffUrno,"SCBHDL");
	}
	else
	{
		dbg(DEBUG, "Account 18: Value not changed: <%s>", clOldAccValue);
	}
}
