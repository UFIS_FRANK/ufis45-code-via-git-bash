
static int bas_CreateDRRSection(const char *pcpTableCommand,const char *pcpScriptToExec)
{
	static const char *pcsDrrFields = "URNO!,AVFR!,AVTO!,BSDU!,DRRN!,ROSL!,ROSS!,SBFR!,SBTO!,SDAY!,STFU!,SCOD!,SBLU!,DRS1!,DRS2!,DRS3!,DRS4!,BROS!";
	char clDrrSectionName[512];
	int ilRc;

	if (pcpTableCommand == NULL || pcpScriptToExec == NULL)
		return -1;

	strcpy(clDrrSectionName,pcpTableCommand);
	strcat(clDrrSectionName,"DRRTAB");
	ilRc = CreateActionSection(clDrrSectionName,pcpScriptToExec,"DRRTAB",pcsDrrFields,pcpTableCommand,0);
	if (ilRc < 0)
	{
		dbg (DEBUG,"bas_init_scbhdl(): bas_CreateDRRSection: CreateActionSection for <%s> failed!",clDrrSectionName);
	}

	return ilRc;		
}

static int bas_CreateDRSSection(const char *pcpTableCommand,const char *pcpScriptToExec)
{
	static const char *pcsDrsFields = "STFU!,SDAY!,DRS1!,DRS3!,DRS4!";
	char clDrsSectionName[512];
	int ilRc;

	if (pcpTableCommand == NULL || pcpScriptToExec == NULL)
		return -1;

	strcpy(clDrsSectionName,pcpTableCommand);
	strcat(clDrsSectionName,"DRSTAB");
	ilRc = CreateActionSection(clDrsSectionName,pcpScriptToExec,"DRSTAB",pcsDrsFields,pcpTableCommand,0);
	if (ilRc < 0)
	{
		dbg (DEBUG,"bas_init_scbhdl(): bas_CreateDRSSection: CreateActionSection for <%s> failed",clDrsSectionName);
	}

	return ilRc;		
}

static int bas_CreateDRWSection(const char *pcpTableCommand,const char *pcpScriptToExec)
{
	static const char *pcsDrwFields = "STFU!,SDAY!,WISC!,USEU!";
	char clDrwSectionName[512];
	int ilRc;

	if (pcpTableCommand == NULL || pcpScriptToExec == NULL)
		return -1;

	strcpy(clDrwSectionName,pcpTableCommand);
	strcat(clDrwSectionName,"DRWTAB");
	ilRc = CreateActionSection(clDrwSectionName,pcpScriptToExec,"DRWTAB",pcsDrwFields,pcpTableCommand,0);
	if (ilRc < 0)
	{
		dbg (DEBUG,"bas_init_scbhdl(): bas_CreateDRWSection: CreateActionSection for <%s> failed",clDrwSectionName);
	}

	return ilRc;		
}

static int bas_CreateDRASection(const char *pcpTableCommand,const char *pcpScriptToExec)
{
	static const char *pcsDraFields = "ABFR!,ABTO!,DRRN!,SDAC!,SDAY!,STFU!,DRS1!,DRS3!,DRS4!,BSDU!";
	char clDraSectionName[512];
	int ilRc;

	if (pcpTableCommand == NULL || pcpScriptToExec == NULL)
		return -1;

	strcpy(clDraSectionName,pcpTableCommand);
	strcat(clDraSectionName,"DRATAB");
	ilRc = CreateActionSection(clDraSectionName,pcpScriptToExec,"DRATAB",pcsDraFields,pcpTableCommand,0);
	if (ilRc < 0)
	{
		dbg (DEBUG,"bas_init_scbhdl(): bas_CreateDRASection: CreateActionSection for <%s> failed",clDraSectionName);
	}

	return ilRc;		
}

static int bas_CreateACCSection(const char *pcpTableCommand,const char *pcpScriptToExec)
{
	static const char *pcsAccFields = "USEC!,USEU!,TYPE!,STFU!,YEAR!,CO01!,CO02!,CO03!,CO04!,CO05!,CO06!,CO07!,CO08!,CO09!,CO10!,CO11!,CO12!,URNO!,OP01,OP02,OP03,OP04,OP05,OP06,OP07,OP08,OP09,OP10,OP11,OP12,CL01,CL02,CL03,CL04,CL05,CL06,CL07,CL08,CL09,CL10,CL11,CL12,YELA,YECU";
	char clAccSectionName[512];
	int ilRc;

	if (pcpTableCommand == NULL || pcpScriptToExec == NULL)
		return -1;

	strcpy(clAccSectionName,pcpTableCommand);
	strcat(clAccSectionName,"ACCTAB");
	ilRc = CreateActionSection(clAccSectionName,pcpScriptToExec,"ACCTAB",pcsAccFields,pcpTableCommand,0);
	if (ilRc < 0)
	{
		dbg (DEBUG,"bas_init_scbhdl(): bas_CreateACCSection: CreateActionSection for <%s> failed",clAccSectionName);
	}

	return ilRc;		
}

static void bas_init_scbhdl()
{
	static const char* pclBasPath = "/ceda/bas/";

	char clScripts[MAX_DATA_SIZE];
	int ilHandle;

	dbg(DEBUG,"bas_init_scbhdl(): Starting initialization");

	strcpy(clScripts,pclBasPath);
	strcat(clScripts,"init_drr.bbf,");

	/* Freitage im Monat zugewiesen */
	strcat(clScripts,pclBasPath);
	strcat(clScripts,"5.bbf,");

	/* Schulungen im Monat */
	strcat(clScripts,pclBasPath);
	strcat(clScripts,"16.bbf,");

	/* Abwesenheiten im Monat */
	strcat(clScripts,pclBasPath);
	strcat(clScripts,"17.bbf,");

	/* Krankheiten im Monat */
	strcat(clScripts,pclBasPath);
	strcat(clScripts,"18.bbf,");

	/* Arbeitsstunden im Monat aktuell */
	strcat(clScripts,pclBasPath);
	strcat(clScripts,"26.bbf,");

	/* HOLBAL Holiday balance */
	strcat(clScripts,pclBasPath);
	strcat(clScripts,"30.bbf,");

	/* �berzeit */
	strcat(clScripts,pclBasPath);
	strcat(clScripts,"32.bbf,");
	
	/* MINBAL Kompensation */
	strcat(clScripts,pclBasPath);
	strcat(clScripts,"33.bbf,");

	/* Freie Samstage im Monat */
	strcat(clScripts,pclBasPath);
	strcat(clScripts,"37.bbf,");

	/* Freie Sonntage im Monat */
	strcat(clScripts,pclBasPath);
	strcat(clScripts,"38.bbf,");

	/* Freie Samstage und Sonntage im Monat */
	strcat(clScripts,pclBasPath);
	strcat(clScripts,"40.bbf,");

	/* Nachtarbeitszeit+ */
	strcat(clScripts,pclBasPath);
	strcat(clScripts,"60.bbf,");

	/* Nachtarbeitszeit- */
	strcat(clScripts,pclBasPath);
	strcat(clScripts,"61.bbf,");

	strcat(clScripts,pclBasPath);
	strcat(clScripts,"exit_drr.bbf,");

	bas_CreateDRRSection("",clScripts);

	strcpy(clScripts,pclBasPath);
	strcat(clScripts,"dra.bbf,"); 
	bas_CreateDRASection("",clScripts);

	strcat(clScripts,pclBasPath);
	strcat(clScripts,"acccor.bbf,");
	strcat(clScripts,pclBasPath);
	strcat(clScripts,"cor_51.bbf,");
	bas_CreateACCSection("",clScripts);

	ilHandle = OpenACTIONRecordset("BSDTAB","BSDTAB","","URNO,CDAT,BSDC,ESBG,LSEN,SDU1,TYPE,BKD1","URNO",1,"IRT,URT,DRT","","",1,0,1);
	if (ilHandle <= -1)
	{
		dbg(TRACE,"bas_init_scbhdl(): ERROR when opening action array <%s>: <%d>!!!","BSDTAB",ilHandle);
	}

	ilHandle = OpenACTIONRecordset("ODATAB","ODATAB","","URNO,FREE,TBSD,SDAC,WORK,SDAS,TYPE,UPLN","URNO",1,"IRT,URT,DRT","","",1,0,1);
	if (ilHandle <= -1)
	{
		dbg(TRACE,"bas_init_scbhdl(): ERROR when opening action array  <%s>: <%d>!!!","ODATAB",ilHandle);
	}

	ilHandle = OpenACTIONRecordset("SCOTAB","SCOTAB","","URNO,SURN,VPFR,VPTO,CODE,CWEH,KOST","SURN,VPFR",0,"IRT,URT,DRT","","",1,0,1);
	if (ilHandle <= -1)
	{
		dbg(TRACE,"bas_init_scbhdl(): ERROR when opening action array <%s>: <%d>!!!","SCOTAB",ilHandle);
	}

	ilHandle = OpenACTIONRecordset("COTTAB","COTTAB","","CTRC,WHPW,INBU,URNO","CTRC",0,"IRT,URT,DRT","","",1,0,1);
	if (ilHandle <= -1)
	{
		dbg(TRACE,"bas_init_scbhdl(): ERROR when opening action array <%s>: <%d>!!!","COTTAB",ilHandle);
	}

	ilHandle = OpenACTIONRecordset("HOLTAB","HOLTAB","","URNO,HDAY,TYPE","URNO",1,"IRT,URT,DRT","","",1,0,1);
	if (ilHandle <= -1)
	{
		dbg(TRACE,"bas_init_scbhdl(): ERROR when opening action array <%s>: <%d>!!!","HOLTAB",ilHandle);
	}

	ilHandle = OpenACTIONRecordset("STFTAB","STFTAB","","URNO,PDGL,PDKL,PMAK,PMAG,LANM,GEBU,PENO,DODM,DOEM","URNO",1,"IRT,URT,DRT","","",1,0,1);
	if (ilHandle <= -1)
	{
		dbg(TRACE,"bas_init_scbhdl(): ERROR when opening action array <%s>: <%d>!!!","STFTAB",ilHandle);
	}

	ilHandle = OpenACTIONRecordset("ACITAB","ACITAB","","CTRC,TYPE,VPFR,VPTO,URNO","CTRC,TYPE",0,"IRT,URT,DRT","","",1,0,1);
	if (ilHandle <= -1)
	{
		dbg(TRACE,"bas_init_scbhdl(): ERROR when opening action array <%s>: <%d>!!!","ACITAB",ilHandle);
	}

	ilHandle = OpenACTIONRecordset("PARTAB","PARTAB","","PAID,VALU,URNO","PAID",0,"IRT,URT,DRT","","",1,0,1);
	if (ilHandle <= -1)
	{
		dbg(TRACE,"bas_init_scbhdl(): ERROR when opening action array <%s>: <%d>!!!","PARTAB",ilHandle);
	}

	AddXBSCommand("RECALC","init_recalc.bbf",pclBasPath);
	AddXBSCommand("ADDSTF","init_addstf.bbf",pclBasPath);
	AddXBSCommand("DELSTF","init_delstf.bbf",pclBasPath);
}

