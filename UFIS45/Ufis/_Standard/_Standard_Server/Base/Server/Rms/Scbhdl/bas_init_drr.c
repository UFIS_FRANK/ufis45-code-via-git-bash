/************************************************************************/
/* Startpunkt f�r die DRR Bearbeitung. Sammelt alle relevanten Infos,	*/
/* die dann global zur Verf�gung stehen					*/
/************************************************************************/
void bas_init_drr()
{
	char	clLastCommand[8];
	char	clStaffUrno[12];
	char	clSday[16];

	char	clShiftCode[10];
	char	clOldShiftCode[10];

	char	clDrrBsduUrno[12];
	char	clOldDrrBsduUrno[12];

	char	clDrrRosl[2];
	char	clOldDrrRosl[2];

	char	clDrrRoss[2];
	char	clOldDrrRoss[2];

	char	clAvfr[16];
	char	clOldAvfr[16];

	char	clAvto[16];
	char	clOldAvto[16];

	char	clSbfr[16];
	char	clOldSbfr[16];

	char	clSblu[6];
	char	clOldSblu[6];

	char	clNewBros[6];
	char	clOldBros[6];

	char	clTmpField[50];
	char	clLocalYear[6];
	char	clIsRecalc[2];

	STAFFDATA	olStfData;
	SCODATA		olScoData;
	COTDATA		olCotData;

	BOOL blNewIsBsd;
	BOOL blNewIsRegFree;
	char clNewIsLikeWork[2];
	BOOL blIsAushilfe;
	BOOL blNewIsShiftChangeoverDay;
	BOOL blNewIsTraining;
	BOOL blNewIsCompensation;
	BOOL blNewIsCompensationNight;
	BOOL blNewIsHoliday;
	BOOL blNewIsIllness;
	
	BOOL blOldIsBsd;
	BOOL blOldIsRegFree;
	char clOldIsLikeWork[2];
	BOOL blOldIsShiftChangeoverDay;
	BOOL blOldIsTraining;
	BOOL blOldIsCompensation;
	BOOL blOldIsCompensationNight;
	BOOL blOldIsHoliday;
	BOOL blOldIsIllness;

	dbg(DEBUG,"\n============================= INIT DRR (START) =============================");

	/****** Ermittlung des Kommandos (IRT,URT,DRT)	****/
	strncpy(clLastCommand,GetLastCommand(),3);
	clLastCommand[3] = '\0';
	dbg(DEBUG,"Calling command (%s)!",clLastCommand);

	/* init variables */
	strcpy(clOldDrrBsduUrno,"0");
	blOldIsBsd = FALSE;

	/****** erster Schritt: den DRR-Datensatz, der die Ausfuehrung	*****/
	/****** dieses Scripts veranlasst hat, analysieren		*****/
	if (strlen(pcgData) == NULL)
	{
		dbg(TRACE,"<pcgData> = undef!!! Calculation of event aborted!!!");
		return;
	}

	dbg(DEBUG,"<pcgData> = <%s>",pcgData);

	if (strlen(pcgFields) == 0)
	{
		dbg(TRACE,"<pcgFields> = undef!!! Calculation of event aborted!!!");
		return;
	}

	dbg(DEBUG, "<pcgFields> = <%s>",pcgFields);

	/****** Schichtcode	*****/
	GetFieldValueFromMonoBlockedDataString("SCOD",pcgFields,pcgData,clShiftCode);

	/****** die Urno des Mitarbeiters	******/
	if (GetFieldValueFromMonoBlockedDataString("STFU",pcgFields,pcgData,clStaffUrno) <= 0)
	{
		dbg(TRACE,"<clStaffUrno> = undef!!! Calculation of event aborted!!!");
		return;
	}

	/****** das Datum, fuer das das Konto ermittelt wird	*/
	if (GetFieldValueFromMonoBlockedDataString("SDAY",pcgFields,pcgData,clSday) <= 0)
	{
		dbg(TRACE,"<clSday> = undef!!! Calculation of event aborted!!!");
		return;
	}
	/****** Schichturno des DRR Datensatzen	******/
	if (GetFieldValueFromMonoBlockedDataString("BSDU",pcgFields,pcgData,clDrrBsduUrno) <= 0)
	{
		dbg(TRACE,"<clDrrBsduUrno> = undef!!!");
		clDrrBsduUrno[0] = '\0';
	}
	clOldShiftCode[0] = '\0';

	/*** Nur wenn ein Update kommt den alten Wert berechnen	*****/
	if (strcmp(clLastCommand,"URT") == 0)
	{
		/****** Im Falle eines Updates wird mit dieser Funktion der alte Datensatz ausgelesen	****/
		if (strlen(pcgOldData) == 0)
		{
			dbg(DEBUG,"<pcgOldData> = LEER!");
		}
		else
		{
			dbg(DEBUG,"<pcgOldData> = <%s>",pcgOldData);
		}

		/* alter SCOD */
		GetFieldValueFromMonoBlockedDataString("SCOD",pcgFields,pcgOldData,clOldShiftCode);
		if (strlen(clOldShiftCode) == 0)
		{
			dbg(DEBUG,"<clOldShiftCode> = undef!!! ");
		}
		else
		{
			dbg(DEBUG,"<clOldShiftCode> = <%s>",clOldShiftCode);
		}

		/* alte BSDU */
		GetFieldValueFromMonoBlockedDataString("BSDU",pcgFields,pcgOldData,clOldDrrBsduUrno);
		if (strlen(clOldDrrBsduUrno) == 0)
		{
			dbg(DEBUG,"<clOldDrrBsduUrno> = undef!!! ");
		}
		else
		{
			dbg(DEBUG,"<clOldDrrBsduUrno> = <%s>",clOldDrrBsduUrno);
		}
	}

	/* level and status information of DRR-record */
	GetFieldValueFromMonoBlockedDataString("ROSL",pcgFields,pcgData,clDrrRosl);
	GetFieldValueFromMonoBlockedDataString("ROSS",pcgFields,pcgData,clDrrRoss);
	WriteTemporaryField("NewRosl",clDrrRosl);
	WriteTemporaryField("NewRoss",clDrrRoss);
	dbg(DEBUG,"New DRR level  <%s>",clDrrRosl);
	dbg(DEBUG,"New DRR status <%s>",clDrrRoss);

	GetFieldValueFromMonoBlockedDataString("ROSL",pcgFields,pcgOldData,clOldDrrRosl);
	GetFieldValueFromMonoBlockedDataString("ROSS",pcgFields,pcgOldData,clOldDrrRoss);
	WriteTemporaryField("OldRosl",clOldDrrRosl);
	WriteTemporaryField("OldRoss",clOldDrrRoss);
	dbg(DEBUG,"Old DRR level  <%s>",clOldDrrRosl);
	dbg(DEBUG,"Old DRR status <%s>",clOldDrrRoss);
	
	/* start and end time of DRR-record */
	GetFieldValueFromMonoBlockedDataString("AVFR",pcgFields,pcgData,clAvfr);
	GetFieldValueFromMonoBlockedDataString("AVTO",pcgFields,pcgData,clAvto);
	WriteTemporaryField("NewAvfr",clAvfr);
	WriteTemporaryField("NewAvto",clAvto);
	dbg(DEBUG,"New DRR start <%s>",clAvfr);
	dbg(DEBUG,"New DRR end   <%s>",clAvto);

	GetFieldValueFromMonoBlockedDataString("AVFR",pcgFields,pcgOldData,clOldAvfr);
	GetFieldValueFromMonoBlockedDataString("AVTO",pcgFields,pcgOldData,clOldAvto);
	WriteTemporaryField("OldAvfr",clOldAvfr);
	WriteTemporaryField("OldAvto",clOldAvto);
	dbg(DEBUG,"Old DRR start <%s>",clOldAvfr);
	dbg(DEBUG,"Old DRR end   <%s>",clOldAvto);

	GetFieldValueFromMonoBlockedDataString("SBFR",pcgFields,pcgData,clSbfr);
	GetFieldValueFromMonoBlockedDataString("SBFR",pcgFields,pcgOldData,clOldSbfr);
	WriteTemporaryField("NewSbfr",clSbfr);
	WriteTemporaryField("OldSbfr",clOldSbfr);
	dbg(DEBUG,"New DRR break from <%s>",clSbfr);
	dbg(DEBUG,"Old DRR break from <%s>",clOldSbfr);

	GetFieldValueFromMonoBlockedDataString("SBLU",pcgFields,pcgData,clSblu);
	GetFieldValueFromMonoBlockedDataString("SBLU",pcgFields,pcgOldData,clOldSblu);
	WriteTemporaryField("NewSblu",clSblu);
	WriteTemporaryField("OldSblu",clOldSblu);
	dbg(DEBUG,"New DRR break duration <%s>",clSblu);
	dbg(DEBUG,"Old DRR break duration <%s>",clOldSblu);

	if (GetFieldValueFromMonoBlockedDataString("BROS",pcgFields,pcgData,clNewBros) == 0)
	{
		strcpy(clNewBros,"0");
	}
	if (GetFieldValueFromMonoBlockedDataString("BROS",pcgFields,pcgOldData,clOldBros) == 0)
	{
		strcpy(clOldBros,"0");
	}
	WriteTemporaryField("NewBros",clNewBros);
	WriteTemporaryField("OldBros",clOldBros);
	dbg(DEBUG,"New DRR break offset <%s>",clNewBros);
	dbg(DEBUG,"Old DRR break offset<%s>",clOldBros);

	/************************************/
	/*** Mitarbeiter Daten raussuchen ***/
	/************************************/

	GetStaffData(clStaffUrno,&olStfData);

	GetScoData(clStaffUrno,clSday,&olScoData);
	
	GetCotDataByCtrc(olScoData.cmScoCode,&olCotData);
	
	if (fabs(olScoData.dmVereinbarteWochenstunden) < 1.0e-5)
	{
		olScoData.dmVereinbarteWochenstunden = olCotData.dmVereinbarteWochenstunden;
	}

	/*************************************/
	/*** Schichtdaten Daten raussuchen ***/
	/*************************************/
	blNewIsBsd = FALSE;
	blNewIsRegFree = FALSE;
	strcpy(clNewIsLikeWork,"0");
	blNewIsShiftChangeoverDay = FALSE;
	blNewIsTraining = FALSE;
	blNewIsCompensation = FALSE;
	blNewIsCompensationNight = FALSE;
	blNewIsHoliday = FALSE;
	blNewIsIllness = FALSE;

	blOldIsRegFree = FALSE;
	strcpy(clOldIsLikeWork,"0");
	blOldIsShiftChangeoverDay = FALSE;
	blOldIsTraining = FALSE;
	blOldIsCompensation = FALSE;
	blOldIsCompensationNight = FALSE;
	blOldIsHoliday = FALSE;
	blOldIsIllness = FALSE;

	/*** neuer DRR ***/
	dbg(DEBUG,"New shift-code is <%s> ",clShiftCode);

	/* Schicht oder Abwesenheit? */
	blNewIsBsd = IsBsd(clDrrBsduUrno);

	if (!blNewIsBsd)
	{
		/*** Which absence type is it ? ***/
		GetODATypes(clDrrBsduUrno,&blNewIsRegFree,&blNewIsShiftChangeoverDay,&blNewIsTraining,&blNewIsCompensation,&blNewIsCompensationNight,&blNewIsHoliday,&blNewIsIllness);
		/*** Wie Schicht zu behandeln ? ***/
		GetOdaWorkData(clDrrBsduUrno,clNewIsLikeWork);
	}

	/*** alter DRR ***/
	if (strcmp(clLastCommand,"URT") == 0)
	{
		/* Schicht oder Abwesenheit ? */
		blOldIsBsd = IsBsd(clOldDrrBsduUrno);
		if (!blOldIsBsd)
		{
			/*** Which absence type is it ? ***/
			GetODATypes(clOldDrrBsduUrno,&blOldIsRegFree,&blOldIsShiftChangeoverDay,&blOldIsTraining,&blOldIsCompensation,&blOldIsCompensationNight,&blOldIsHoliday,&blOldIsIllness);
			/*** Wie Schicht zu behandeln ? ***/
			GetOdaWorkData(clOldDrrBsduUrno,clOldIsLikeWork);
		}
	}

	/*** Mitarbeiterdaten pr�fen ***/

	if (olCotData.cmINBU[0] == 'x')
	{
		blIsAushilfe = FALSE;		
	}
	else
	{
		blIsAushilfe = TRUE;
	}
	
	/**********************************************************************************************************/
	/*** Ausgabe ***/
	/**********************************************************************************************************/
	dbg(DEBUG, "<Employee name>               = %s",olStfData.cmMaName);
	dbg(DEBUG, "<Employee personnel number>   = %s",olStfData.cmPersonalnummer);
	dbg(DEBUG, "<Employee URNO>               = %s",clStaffUrno);
	dbg(DEBUG, "<New shift code>              = %s",clShiftCode);
	dbg(DEBUG, "<New shift URNO>              = %s",clDrrBsduUrno);
	dbg(DEBUG, "<New shift is basic shift>    = %d",blNewIsBsd);
	dbg(DEBUG, "<New shift is regular free>   = %d",blNewIsRegFree);
	dbg(DEBUG, "<New shift is Training        = %d",blNewIsTraining);
	dbg(DEBUG, "<New shift is Changeover day  = %d",blNewIsShiftChangeoverDay);
	dbg(DEBUG, "<New shift is Compensation    = %d",blNewIsCompensation);
	dbg(DEBUG, "<New shift is CompensationNight = %d",blNewIsCompensationNight);
	dbg(DEBUG, "<New shift is Holiday         = %d",blNewIsHoliday);
	dbg(DEBUG, "<New shift is Illness         = %d",blNewIsIllness);
	dbg(DEBUG, "<New shift is like work>      = %s",clNewIsLikeWork);
	
	dbg(DEBUG, "<Old shift code>              = %s",clOldShiftCode);
	dbg(DEBUG, "<Old shift URNO>              = %s",clOldDrrBsduUrno);
	dbg(DEBUG, "<Old shift is basic shift>    = %d",blOldIsBsd);
	dbg(DEBUG, "<Old shift is regular free>   = %d",blOldIsRegFree);
	dbg(DEBUG, "<Old shift is Training        = %d",blOldIsTraining);
	dbg(DEBUG, "<Old shift is Changeover day  = %d",blOldIsShiftChangeoverDay);
	dbg(DEBUG, "<Old shift is Compensation    = %d",blOldIsCompensation);
	dbg(DEBUG, "<Old shift is CompensationNight = %d",blOldIsCompensationNight);
	dbg(DEBUG, "<Old shift is Holiday         = %d",blOldIsHoliday);
	dbg(DEBUG, "<Old shift is Illness         = %d",blOldIsIllness);
	dbg(DEBUG, "<Old shift is like work>      = %s",clOldIsLikeWork);
	dbg(DEBUG, "<Contract-code (SCOTAB)>      = %s",olScoData.cmScoCode);
	dbg(DEBUG, "<'Vereinbarte Wochenstunden'> = %lf",olScoData.dmVereinbarteWochenstunden);

	/**********************************************************************************************************/
	/*** Speichern in Datenarray ***/
	/**********************************************************************************************************/
	WriteTemporaryField("MaName",olStfData.cmMaName);
	WriteTemporaryField("StfUrno",clStaffUrno);
	WriteTemporaryField("SDAY",clSday);
	WriteTemporaryField("OldBsdu",clOldDrrBsduUrno);
	WriteTemporaryField("NewBsdu",clDrrBsduUrno);

	sprintf(clTmpField,"%lf",olScoData.dmVereinbarteWochenstunden);
	WriteTemporaryField("VereinbarteWochenstunden",clTmpField);

	WriteTemporaryField("ScoCode",olScoData.cmScoCode);

	if (blNewIsBsd)
		WriteTemporaryField("NewIsBsd","1");
	else
		WriteTemporaryField("NewIsBsd","0");

	if (blNewIsRegFree)
		WriteTemporaryField("NewIsRegFree","1");
	else
		WriteTemporaryField("NewIsRegFree","0");
		
	if (blNewIsTraining)
		WriteTemporaryField("NewIsTraining","1");
	else
		WriteTemporaryField("NewIsTraining","0");

	if (blNewIsShiftChangeoverDay)
		WriteTemporaryField("NewIsShiftChangeoverDay","1");
	else
		WriteTemporaryField("NewIsShiftChangeoverDay","0");

	if (blNewIsCompensation)
		WriteTemporaryField("NewIsCompensation","1");
	else
		WriteTemporaryField("NewIsCompensation","0");
		
	if (blNewIsCompensationNight)
		WriteTemporaryField("NewIsCompensationNight","1");
	else
		WriteTemporaryField("NewIsCompensationNight","0");

	if (blNewIsHoliday)
		WriteTemporaryField("NewIsHoliday","1");
	else
		WriteTemporaryField("NewIsHoliday","0");

	if (blNewIsIllness)
		WriteTemporaryField("NewIsIllness","1");
	else
		WriteTemporaryField("NewIsIllness","0");


	if (blOldIsBsd)
		WriteTemporaryField("OldIsBsd","1");
	else
		WriteTemporaryField("OldIsBsd","0");

	if (blOldIsRegFree)
		WriteTemporaryField("OldIsRegFree","1");
	else
		WriteTemporaryField("OldIsRegFree","0");

	if (blOldIsTraining)
		WriteTemporaryField("OldIsTraining","1");
	else
		WriteTemporaryField("OldIsTraining","0");

	if (blOldIsShiftChangeoverDay)
		WriteTemporaryField("OldIsShiftChangeoverDay","1");
	else
		WriteTemporaryField("OldIsShiftChangeoverDay","0");

	if (blOldIsCompensation)
		WriteTemporaryField("OldIsCompensation","1");
	else
		WriteTemporaryField("OldIsCompensation","0");

	if (blOldIsCompensationNight)
		WriteTemporaryField("OldIsCompensationNight","1");
	else
		WriteTemporaryField("OldIsCompensationNight","0");

	if (blOldIsHoliday)
		WriteTemporaryField("OldIsHoliday","1");
	else
		WriteTemporaryField("OldIsHoliday","0");

	if (blOldIsIllness)
		WriteTemporaryField("OldIsIllness","1");
	else
		WriteTemporaryField("OldIsIllness","0");


	if (blIsAushilfe)
		WriteTemporaryField("IsAushilfe","1");
	else
		WriteTemporaryField("IsAushilfe","0");

	WriteTemporaryField("IsNewLikeWork",clNewIsLikeWork);
	WriteTemporaryField("IsOldLikeWork",clOldIsLikeWork);

	WriteTemporaryField("Personalnummer",olStfData.cmPersonalnummer);

	WriteTemporaryField("NewScod",clShiftCode);
	WriteTemporaryField("OldScod",clOldShiftCode);
	
	/* increment reference counter to staff accounts */
	ReadTemporaryField("IsRecalc",clIsRecalc);
	if (strcmp(clIsRecalc,"0") == 0)
	{
		strncpy(clLocalYear,clSday,4);
		clLocalYear[4] = '\0';
		StfToAccPrefetch(clStaffUrno,clLocalYear,TRUE);
		sprintf(clLocalYear,"%d",atoi(clLocalYear)+1);
		StfToAccPrefetch(clStaffUrno,clLocalYear,TRUE);
	}
}
