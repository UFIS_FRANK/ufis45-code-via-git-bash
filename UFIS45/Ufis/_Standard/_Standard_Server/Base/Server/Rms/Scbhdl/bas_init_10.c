/************************************************************/
/* init_10: initializing account 10 - free days in the year */
/************************************************************/
static void bas_init_10()
{
	char	clFrom[16];
	char	clYearFrom[6];
	char	clMonFrom[4];
	char	clDayFrom[4];

	char	clTo[16];
	char	clYearTo[6];
	char	clMonTo[4];
	char	clDayTo[4];

	char	clCurrentEmpl[12];

	STAFFDATA	olStfData;

	double	dlFreitageYearMin = 0;
	double	dlFreitageMonPlan;
	char	clFreitageYearMin[128];
	char	clCalcDate[16];

	int	ilCountEmpl;
	int	ilCounter;
	int	ilYearCount;
	int	ilMonthCount;
	int	ilQuartalCount;

	dbg(DEBUG,"============================= INIT ACCOUNT INIT_10 START =============================");

	/* checking the data */
	if (strlen(pcgData) == NULL)
	{
		dbg(TRACE,"Account Init 10: Stopped, because <pcgData> = undef!");
		return;
	}
	dbg(DEBUG,"Account Init 10: <pcgData> = <%s>",pcgData);

	/* checking the fields */
	if (strlen(pcgFields) == 0)
	{
		dbg(TRACE,"Account Init 10: Stopped, because <pcgFields> = undef!");
		return;
	}
	dbg(DEBUG, "Account Init 10: <pcgFields> = <%s>",pcgFields);

	/* checking the selection */
	if (strlen(pcgSelKey) == 0)
	{
		dbg(TRACE,"Account Init 10: Stopped, because <pcgSelKey> = undef!");
		return;
	}
	dbg(DEBUG, "Account Init 10: <pcgSelKey> = <%s>",pcgSelKey);

	/* get start date */
	if (GetFieldValueFromMonoBlockedDataString("FROM",pcgFields,pcgData,clFrom) <= 0)
	{
		dbg(TRACE,"Account Init 10: Stopped, because <clFrom> = undef!");
		return;
	}
	strncpy(clYearFrom,&clFrom[0],4);
	clYearFrom[4] = '\0';
	strncpy(clMonFrom,&clFrom[4],2);
	clMonFrom[2] = '\0';
	strncpy(clDayFrom,&clFrom[6],2);
	clDayFrom[2] = '\0';

	/* get end date */
	if (GetFieldValueFromMonoBlockedDataString("TO",pcgFields,pcgData,clTo) <= 0)
	{
		dbg(TRACE,"Account Init 10: Stopped, because <clTo> = undef!");
		return;
	}
	strncpy(clYearTo,&clTo[0],4);
	clYearTo[4] = '\0';
	strncpy(clMonTo,&clTo[4],2);
	clMonTo[2] = '\0';
	strncpy(clDayTo,&clTo[6],2);
	clDayTo[2] = '\0';

	dbg(DEBUG, "Account Init 10: Initializing from %s to %s.",clFrom,clTo);

	/* count number of employees */
	ilCountEmpl = CountElements(pcgSelKey);
	if (ilCountEmpl < 0)
	{
		dbg(TRACE,"Account Init 10: Stopped, because no employees to initialize!");
		return;
	}
	dbg(DEBUG,"Account Init 10: Number of employees = <%d>", ilCountEmpl);

	/* loop per employee */
	for (ilCounter = 0; ilCounter < ilCountEmpl; ilCounter++)
	{
		if (GetElement(pcgSelKey,ilCounter,clCurrentEmpl) <= 0)
		{
			dbg(TRACE,"Account Init 10: Stopped, because <clCurrentEmpl> = undef!");
			return;
		}

		GetStaffData(clCurrentEmpl,&olStfData);

		/* Used in UpdateAccountXY! */
		WriteTemporaryField("Personalnummer",olStfData.cmPersonalnummer);

		dbg(DEBUG,"Account Init 10: Start calculation for employee <%s>",clCurrentEmpl);

		/* loop through the years */
		for (ilYearCount = atoi(clYearFrom); ilYearCount <= atoi(clYearTo); ilYearCount++)
		{
			ilQuartalCount = 0;
			dlFreitageYearMin = 0;

			/* loop per month */
			for (ilMonthCount = atoi(clMonFrom); ilMonthCount <= atoi(clMonTo); ilMonthCount++)
			{
				dlFreitageMonPlan = 0.0e0;

				/* build time-variable for calculation */
				sprintf(clCalcDate,"%4d%02d%02d",ilYearCount,ilMonthCount,1);
				dbg(TRACE,"Account Init 10: Start calculation for date <%s>",clCalcDate);

				/* we take the same function of initialize account 4 - so we are independant if acc 4 is used or not */
				if (!CalculateInit4(clCurrentEmpl,clCalcDate,&dlFreitageMonPlan))
				{
					dbg(TRACE,"Account Init 10: ERROR in 'CalculateInit4'! Try to calculate next month!");
					continue;
				}
				else
				{
					dbg(DEBUG,"Account Init 10: <dlFreitageMonPlan> = <%lf>",dlFreitageMonPlan);
				}

				if (dlFreitageMonPlan > -1)
				{
					dlFreitageYearMin += dlFreitageMonPlan;
				}
			}

			dbg(DEBUG,"Account Init 10: <dlFreitageYearMin> = <%lf>",dlFreitageYearMin);

			/* put year value only in December-field */
			sprintf(clCalcDate,"%04d%02d%02d",ilYearCount,12,1);

			/* round and cut */
			sprintf(clFreitageYearMin,"%-8.2lf",dlFreitageYearMin);
			dlFreitageYearMin = atof(clFreitageYearMin);

			UpdateAccountXYHandle(clCurrentEmpl,dlFreitageYearMin,"10",clCalcDate,"XBSHDL","CO");
		}
	}
}
