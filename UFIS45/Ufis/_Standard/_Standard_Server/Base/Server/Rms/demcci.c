#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Rms/demcci.c 1.17 2011/10/30 04:17:35SGT ble Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         : R. Nassiri                                                */
/* Date           : 26.09.00                                                  */
/* Description    : calculates counter demands for common check in            */
/*                                                                            */
/* Update history :                                                           */
/*      18.01.2001 : GKA Bugfixed invalid comment                             */
/* 20101123 MEI: print baggage time used, default or in profile editor - v1.15 */
/* 20121012 MEI: Add in new restrict fr/to when creating collective dem v1.16 and 1.17 UFIS-1083 */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
/* static char sccs_version[] ="@(#) UFIS4.5 (c) ABB AAT/I demcci.c 4.5.1.2 / 03/09/17 / HAG"; */
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h"
#include "AATArray.h"
#include "libccstr.h"
#include "syslib.h"
#include "fditools.h"
#include "cedatime.h"


#define ARR_NAME_LEN    (8)
#define ARR_FLDLST_LEN  (256)

/* defines for fieldlens */
#define HOPOLEN     3
#define DATELEN     14
#define URNOLEN     10
#define STEPLEN     3
#define CICCLEN     3
#define LIMITLEN    4
#define MAXLEN      3
#define MINLEN      3
#define DATALEN     1152

#define MSGBUFLEN URNOLEN+1 + DATELEN+1 + DATELEN+1 + STEPLEN+1 + DATALEN

/* defines for defaults */
#define DEMANDMINDEFAULT        15
#define QUTYPEDEFAULT           "SINGLE"
#define RESTRATEDEFAULT         50
#define TAWITHBAGDEFAULT        110
#define TAWITHOUTBAGDEFAULT     80
#define QUEUEQUALITYDEFAULT     7

#define TIMETODATA(j) (tlSdiStart + j*slSdiStep)        /*position in sdi.data*/

/* flags for SetArrayInfo */
#define CEDAARRAY_ALL       0xFF
#define CEDAARRAY_FILL      0x01
#define CEDAARRAY_TRIGGER   0x04
#define CEDAARRAY_URNOIDX   0x02

#define SDIWRKFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgSdiWrkArray.plrArrayFieldOfs[ipFieldNo-1]])
#define CCCFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgCccArray.plrArrayFieldOfs[ipFieldNo-1]])
#define RUDFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgRudArray.plrArrayFieldOfs[ipFieldNo-1]])
#define DEMFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgDemArray.plrArrayFieldOfs[ipFieldNo-1]])
#define UCDFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgUcdArray.plrArrayFieldOfs[ipFieldNo-1]])



/*                   0    1     2   3    4    5     6   7    8    9     10  11   12   13   14   15   16   17   18   19   20   21   22               */  
#define RUD_FIELDS "HOPO,LIFR,LITO,MAXC,MINC,URNO,VREF,FADD,URUE"
#define SDI_FIELDS "DATA,DBAG,HOPO,PBEA,PENA,STEP,UAFT,UCCC,UPRO,URNO"
#define UCD_FIELDS "HOPO,UAFT,URNO,URUD"
#define CCC_FIELDS "CICC,HOPO,URNO"
#define PXC_FIELDS "UPRO,UCCC,TTBG,TTWO,HOPO,URNO"
#define RUE_FIELDS "RUSN,URNO,RUST"
#define DEM_FIELDS "URNO,URUD,DEBE,DEEN"


struct _arrayinfo
{
    HANDLE  rrArrayHandle;
    char    crArrayName[ARR_NAME_LEN+1];
    char    crTableName[ARR_NAME_LEN+1];
    char    crArrayFieldList[ARR_FLDLST_LEN+1];
    long    lrArrayFieldCnt;
    char    *pcrArrayRowBuf;
    long    lrArrayRowLen;
    long    *plrArrayFieldOfs;
    long    *plrArrayFieldLen;
    HANDLE  rrIdx01Handle;
    char    crIdx01Name[ARR_NAME_LEN+1];
    char    crIdx01FieldList[ARR_FLDLST_LEN+1];
    long    lrIdx01FieldCnt;
    char    *pcrIdx01RowBuf;
    long    lrIdx01RowLen;
    HANDLE  rrIdxUrnoHandle;
    char    crIdxUrnoName[ARR_NAME_LEN+1];
};
typedef struct  _arrayinfo ARRAYINFO;

struct _rgPAXToFlight
{
    float   frNoOfPAX;
};
typedef struct  _rgPAXToFlight PAXARRAY;

struct _rgWrkArray
{
    long        lrStartInSeconds;
    PAXARRAY    *prrPAXToFlight;
    float       frSumOfPAX;
};
typedef struct  _rgWrkArray WRKARRAY;

struct _rgConfArray                                 /*data from config-file*/
{
    char    crUccc[URNOLEN+1];
    int     irQuality;
    int     irTTWithBaggage;
    int     irTTWithoutBaggage;
};
typedef struct  _rgConfArray    CNFARRAY;

/******************************************************************************/
/* External variables and functions                                           */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = 0;

extern int get_no_of_items(char *s);

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static  ITEM    *prgItem = NULL;              /* The queue item pointer  */
static  EVENT   *prgEvent = NULL;                /* The event pointer       */
static  EVENT   *prgOutEvent      = NULL ;
static  int     igItemLen = 0;                   /* length of incoming item */
static  int     igInitOK = FALSE;
static  char    cgConfigFile[512];
static  char    cgHopo[HOPOLEN+1];                 /* default home airport    */
static  char    cgTabEnd[64]="" ;
static  char    pcgRecvName[64]; /* BC-Head used as WKS-Name */
static  char    pcgDestName[64]; /* BC-Head used as USR-Name */
static  char    pcgTwStart[64];  /* BC-Head used as Stamp */
static  char    pcgTwEnd[64];  /* BC-Head used as Stamp */ 
static  long    lgOutEventSize  = 0 ;
static  long    lgCccRow = 0;
static  int     igDemand;
static  char    cgQueueType[64];
static  int     igRestRate;
static  int     igDest = 9000;                          /* mod-ID of DEMHDL */
static  char    cgMsgData[MSGBUFLEN];
static  int     igPosInMsgData;
static  BOOL    bgEndOfIvReached = FALSE;
static  int     igLifr;
static  int     igLito;
static  int     igMaxc;
static  int     igMinc;
/** MEI v1.16  **/
static  int     igRestrictOn;
static  int     igRestrictFrom;
static  int     igRestrictTo;
static  char    pcgRefl[4];

static  char    cgTdi1[24];
static  char    cgTdi2[24];
static  char    cgTich[24];
static  int     igStartUpMode = TRACE;
static  int     igRuntimeMode = 0;

static  int     igUcdUaft;
static  int     igUcdUrud;
static  int     igUcdBegi;
static  int     igUcdEnde;
static  int     igSdiData;
static  int     igSdiDbag;
static  int     igSdiPbea;
static  int     igSdiPena;
static  int     igSdiStep;
static  int     igSdiUccc;
static  int     igSdiUaft;
static  int     igSdiUpro;
static  int     igCccCicc;
static  int     igCccUrno;
static  int     igPxcTtwo;
static  int     igPxcTtbg;
static  int     igRudFadd;
static  int     igRudVref;
static  int     igRudUrue;
static  int     igDemUrno;
static  int     igDemDebe;
static  int     igDemDeen;
static  int     igDemUrud;
static  int     igDemUaft;

ARRAYINFO   rgRudArray;
ARRAYINFO   rgRueArray;
ARRAYINFO   rgSdiArray;
ARRAYINFO   rgSdiWrkArray;
ARRAYINFO   rgUcdArray;
ARRAYINFO   rgUcdWrkArray;
ARRAYINFO   rgCccArray;
ARRAYINFO   rgPxcArray;
ARRAYINFO   rgDemArray;

CNFARRAY    *prgCnf = 0;
WRKARRAY    *prgWrk = 0;
PAXARRAY    *prgPAX = 0;

static char cgLoadStart[DATELEN+1] = "20040201000000";
static  int     igDaysInPast = 10;                          

/******************************************************************************/
/* Function prototypes                                                        */
/******************************************************************************/
static int  Init_demcci();
static int  Reset(void);                       /* Reset program          */
static void Terminate(void);                   /* Terminate program      */
/*static void   HandleSignal(int);*/                /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void HandleQueErr(int);                 /* Handles queuing errors */
static int  HandleData(void);                  /* Handles event data     */
static int  HandleCCD(char *, char *, char *);  /* Handles CCD */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/
static int  SetArrayInfo(char *, char *, char *, char *, long *, char *,
                         ARRAYINFO *, short);
static int  CreateIdx1(ARRAYINFO *, char *, char *, char *);       
static int  GetRowLength(char *, char *, long *);
static int  GetFieldLength(char *, char *, long *);
static int  GetFieldByUrno( ARRAYINFO *, char *, char *, char *, int);
void TrimRight(char *);
static int  SaveIndexInfo (ARRAYINFO *, int, int);
static int  CalcNewData(char *, char *);     /* Calculate Data from Sditab */
static int  TimeToStr(char *, time_t); 
static int  StrToTime(char *, time_t *);
static int  InitFieldIndex(void);
static int  FindCccPosition(char *, int *);/*find cfg-data belonging to CCCTAB*/
static int  SendEventUCD(void);
static int  SendEventCDF(EVENT *, char *);
static int  myget_no_of_items(char *);
static int  HandleCDF(EVENT *, char *, char *);             /* Handles CDF */
static int  GetItemData(char *,char *,char *,char *);
static int  SearchingForSDI(char *,char *,char *);
static int  ForwardEvent(int);
static int  LocalToUtc(char *);
static int  GetDebugLevel(char *, int *);
static int InitializeCNFARRAY ();
static int GetTransactionTime ( char *pcpUpro, char *pcpUccc, int ipFieldIdx,
                                int *pipValue );
static int GetCCIFlightUrnos ( char *pcpUrud, char *pcpDebe, char *pcpDeen, char **pcpUaft, char cpDel );
static int GetCCIFlightUrnos2 ( char *pcpUrud, char *pcpDebe, char *pcpDeen, char *pcpUaft, char cpDel );
static int HandleFLC(EVENT *pcpEvent, char *pcpData);
static int IniUcdWorkArray ( char *pcpFrom, char *pcpTo );
static int SendEventFLC(EVENT *prpEvent, long lpBuffLen );
static int GetNextDate ( char *pcpStartUtc, int ipTimeLocal, char *pcpNextUtc );
static int UtcToLocal(char *pcpTime);


/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int ilRC = RC_SUCCESS;
    int ilCnt = 0;
    
    INITIALIZE;         /* General initialization   */
    dbg(TRACE,"MAIN: version <%s>",mks_version);
    /* Attach to the MIKE queues */
    do
    {
        ilRC = init_que();
        if(ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }
    }while((ilCnt < 10) && (ilRC != RC_SUCCESS));

    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }
    else
    {
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }
    do
    {
        ilRC = init_db();
        if (ilRC != RC_SUCCESS)
        {
            check_ret(ilRC);
            dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
            sleep(6);
        ilCnt++;
        }
    } while((ilCnt < 10) && (ilRC != RC_SUCCESS));
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
        sleep(60);
        exit(2);
    }
    else
    {
        dbg(TRACE,"MAIN: init_db() OK!");
    }
    /*logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!!*/

    sprintf(cgConfigFile,"%s/demcci",getenv("BIN_PATH"));
    ilRC = TransferFile(cgConfigFile);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    }

    sprintf(cgConfigFile,"%s/demcci.cfg",getenv("CFG_PATH"));
    ilRC = TransferFile(cgConfigFile);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    }
    ilRC = SendRemoteShutdown(mod_id);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    }
    if ((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) &&
        (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }
    if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE)
        || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        if(igInitOK == FALSE)
        {
            ilRC = Init_demcci();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"Init_demcci: init failed!");
            }
        }
    }
    else
    {
        Terminate();
    }
    dbg(TRACE,"MAIN: initializing OK");
    debug_level = igRuntimeMode; 
    for(;;)
    {
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            }
            
            switch( prgEvent->command )
            {
                case HSB_STANDBY:
                    ctrl_sta = prgEvent->command;
                    HandleQueues();
                    break;  
                case HSB_COMING_UP:
                    ctrl_sta = prgEvent->command;
                    HandleQueues();
                    break;  
                case HSB_ACTIVE:
                    ctrl_sta = prgEvent->command;
                    break;  
                case HSB_ACT_TO_SBY:
                    ctrl_sta = prgEvent->command;
                    /* CloseConnection(); */
                    HandleQueues();
                    break;  
                case HSB_DOWN:
/*whole system shutdown - don't further use que(), send_message() or timsch()!*/
                    ctrl_sta = prgEvent->command;
                    Terminate();
                    break;  
                case HSB_STANDALONE:
                    ctrl_sta = prgEvent->command;
                    ResetDBCounter();
                    break;  
                case REMOTE_DB:
                    /* ctrl_sta is checked inside */
                    HandleRemoteDB(prgEvent);
                    break;
                case SHUTDOWN:
                    /* process shutdown - maybe from uutil */
                    Terminate();
                    break;
                        
                case RESET:
                    ilRC = Reset();
                    break;
                case 100:
                    SaveIndexInfo ( &rgDemArray, 0, 0 );
                    break;
                case 101:
                    SaveIndexInfo ( &rgUcdArray, 2, 0 );
                    SaveIndexInfo ( &rgSdiArray, 2, 0 );
                    break;
                case 102:
                    SaveIndexInfo ( &rgUcdWrkArray, 2, 0 );
                    break;  
                case EVENT_DATA:
                    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE)
                        || (ctrl_sta == HSB_ACT_TO_SBY))
                    {
                        ilRC = HandleData();
                        if(ilRC != RC_SUCCESS)
                        {
                            HandleErr(ilRC);
                        }
                    }
                    else
                    {
                        dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                        DebugPrintItem(TRACE,prgItem);
                        DebugPrintEvent(TRACE,prgEvent);
                    }
                    break;
                        
                case TRACE_ON:
                    dbg_handle_debug(prgEvent->command);
                    break;
                case TRACE_OFF:
                    dbg_handle_debug(prgEvent->command);
                    break;
                default:
                    dbg(TRACE,"MAIN: unknown event");
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                    break;
                } /* end switch */
            }
            else
            {
            /* Handle queuing errors */
                HandleQueErr(ilRC);
        }
        
    }
    
    /*exit(0);*/
    
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_demcci()
{
    int     ilRC = RC_SUCCESS;
    char    clSelection[101], clCurrTime[DATELEN+1];
    char    pclAddFields[1024] = "\0";
    int     ilDummy;
    long    llDummy;
    time_t  tlCurrTime;
    long    pllAddFieldLens[3];

    InitFieldIndex();

    /* now reading from configfile or from database */
    
    if ( debug_level < TRACE )
        debug_level = TRACE;

    /* read HomeAirPort from SGS.TAB */
    memset((void*)&cgHopo[0], 0x00, HOPOLEN+1);
    ilRC = tool_search_exco_data("SYS", "HOMEAP", &cgHopo[0]);

    if (ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"Init_demcci: EXTAB,SYS,HOMEAP not found in SGS.TAB" );
        Terminate();
    } 
    else
        dbg(TRACE,"Init_demcci: home airport    <%s>",&cgHopo[0]);
    
    ilRC = tool_search_exco_data("ALL", "TABEND", cgTabEnd);

    if (ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"Init_demcci: EXTAB, not found in SGS.TAB" );
        Terminate();
    } 
    else 
        dbg(TRACE,"Init_demcci: table extension <%s>",cgTabEnd);
    
    if(ilRC == RC_SUCCESS)
    {
         GetDebugLevel("STARTUP_MODE", &igStartUpMode);
         GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
    }
    debug_level = igStartUpMode;

    if (ilRC == RC_SUCCESS)
    {
        ilRC = iGetConfigEntry(cgConfigFile,"MAIN","SEND_RESULT_TO",CFG_INT,(char *)&ilDummy);
        if (ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"Init_demcci: SEND_RESULT_TO not in configuration - using default : <%d>",igDest);
            ilRC = RC_SUCCESS ;
        }
        else
        {
            igDest = ilDummy ;
            dbg (TRACE, "Init_demcci: <SEND_RESULT_TO> <%d>", igDest); 
        } /* end of if */
    }   

    if (ilRC == RC_SUCCESS)
    {
        ilRC = iGetConfigEntry(cgConfigFile,"MAIN","LOAD_DAYS_IN_PAST",CFG_INT,(char *)&ilDummy);
        if (ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"Init_demcci: LOAD_DAYS_IN_PAST not in configuration - using default : <%d>",igDaysInPast);
            ilRC = RC_SUCCESS ;
        }
        else
        {
            igDaysInPast = ilDummy ;
            dbg (TRACE, "Init_demcci: <LOAD_DAYS_IN_PAST> <%d>", igDaysInPast ); 
        } /* end of if */
        GetServerTimeStamp("UTC", 1, 0, clCurrTime);
        StrToTime(clCurrTime,&tlCurrTime);
        tlCurrTime -= igDaysInPast*24*3600;
        TimeToStr (cgLoadStart,tlCurrTime);
    }   

    ilRC = CEDAArrayInitialize(10,10);

    if(ilRC != RC_SUCCESS)      
        dbg(TRACE,"Init_demcci: CEDAArrayInitialize failed <%d>",ilRC); 
    else
    {   
        /** MEI v1.16 **/
        ilRC = GetFieldLength( "RUDTAB", "REFR", &llDummy );
        strcpy( pclAddFields, RUD_FIELDS );
        igRestrictOn = FALSE;
        if( ilRC == RC_SUCCESS )
        {
            igRestrictOn = TRUE;
            sprintf( pclAddFields, "%s,REFR,RETO,REFL", RUD_FIELDS );
        }

        /* Read Table RUD, only read RUDs of collective rules */
        strcpy ( clSelection, "WHERE URUE in (select URNO from RUETAB where RUTY='1')" );
        ilRC = SetArrayInfo("RUDTAB", "RUD", pclAddFields, 0, 0, clSelection, &rgRudArray,
                            CEDAARRAY_ALL );
        if(ilRC != RC_SUCCESS)                                             
            dbg(TRACE,"Init_demcci: SetArrayInfo for RUD failed <%d>",ilRC);        
    }   

    if (ilRC == RC_SUCCESS)                                        
    {                                                              
        strcpy ( clSelection, "WHERE RUTY='1'" );
        ilRC = SetArrayInfo("RUETAB", "RUE", RUE_FIELDS, 0, 0, clSelection, &rgRueArray,
                            CEDAARRAY_ALL );
        if (ilRC != RC_SUCCESS)                                             
            dbg(TRACE,"Init_demcci: SetArrayInfo for RUE failed <%d>",ilRC);
    }
    
    if (ilRC == RC_SUCCESS)                                        
    {                       
        sprintf ( clSelection, "WHERE PBEA>'%s'", cgLoadStart );
        ilRC = SetArrayInfo("SDITAB", "SDI", SDI_FIELDS, 0, 0, clSelection, &rgSdiArray,
                            CEDAARRAY_FILL|CEDAARRAY_TRIGGER );
        if (ilRC != RC_SUCCESS)                                             
            dbg(TRACE,"Init_demcci: SetArrayInfo for SDI failed <%d>",ilRC);
        else
        {
            ilRC = CreateIdx1 ( &rgSdiArray, "IDXSDI1", "HOPO,UAFT,UCCC", 
                                "A,A,A" );
            if (ilRC!=RC_SUCCESS)
                dbg(TRACE,"Init_demcci: CreateIdx1 for SDI failed <%d>",ilRC);      
        }
    }
    
    if (ilRC == RC_SUCCESS)                                        
    {                                                              
        ilRC = SetArrayInfo("SDIWRK", "SDI", SDI_FIELDS, 0, 0, "",
                             &rgSdiWrkArray, 0);
        if (ilRC != RC_SUCCESS)                                             
            dbg(TRACE,"Init_demcci: SetArrayInfo for SDIWRK failed <%d>",ilRC);
        else
        {
            ilRC = CreateIdx1( &rgSdiWrkArray, "IDXSDI1", "HOPO,UAFT,UCCC",
                                "A,A,A" );
            if (ilRC!=RC_SUCCESS)
                dbg(TRACE,"Init_demcci: CreateIdx1 for SDIWRK failed <%d>",
                    ilRC);      
        }
    }
    
    if (ilRC == RC_SUCCESS)                                        
    {          
        pllAddFieldLens[0] = DATELEN;
        pllAddFieldLens[1] = DATELEN;
        pllAddFieldLens[2] = 0;
        sprintf ( clSelection, "WHERE UAFT IN (SELECT URNO FROM AFTTAB WHERE STOD>'%s' AND ADID='D')", cgLoadStart );
        ilRC = SetArrayInfo("UCDTAB", "UCD", UCD_FIELDS, "BEGI,ENDE", 
                            pllAddFieldLens,  clSelection, &rgUcdArray,
                            CEDAARRAY_FILL|CEDAARRAY_TRIGGER );
        if (ilRC != RC_SUCCESS)                                             
            dbg(TRACE,"Init_demcci: SetArrayInfo for UCD failed <%d>",ilRC);
        else
        {
            ilRC = CreateIdx1 ( &rgUcdArray, "IDXUCD1", "HOPO,URUD,URNO", 
                                "A,A,A" );
            if (ilRC!=RC_SUCCESS)
                dbg(TRACE,"Init_demcci: CreateIdx1 for UCD failed <%d>",ilRC);      
        }
    }
    
    if (ilRC == RC_SUCCESS)                                        
    {          
        pllAddFieldLens[0] = DATELEN;
        pllAddFieldLens[1] = DATELEN;
        pllAddFieldLens[2] = 0;
        ilRC = SetArrayInfo("UCDWRK", "UCD", UCD_FIELDS, "BEGI,ENDE", pllAddFieldLens, 0, &rgUcdWrkArray, 0 );
        if (ilRC != RC_SUCCESS)                                             
            dbg(TRACE,"Init_demcci: SetArrayInfo for UCD failed <%d>",ilRC);
        else
        {
            ilRC = CreateIdx1 ( &rgUcdWrkArray, "IDXUCD1", "URUD",  "A" );
            if (ilRC!=RC_SUCCESS)
                dbg(TRACE,"Init_demcci: CreateIdx1 for UCDWRK failed <%d>",ilRC);       
        }
    }
    if(ilRC == RC_SUCCESS)      
    {   
        ilRC = SetArrayInfo("CCCTAB", "CCC", CCC_FIELDS, 0, 0, "", &rgCccArray,
                        CEDAARRAY_FILL|CEDAARRAY_TRIGGER );
        if (ilRC != RC_SUCCESS)                                             
            dbg(TRACE,"Init_demcci: SetArrayInfo for CCC failed <%d>",ilRC);
        else
        {
            ilRC = CreateIdx1 ( &rgCccArray, "IDXUCD1", "URNO,HOPO", "A,A" );

            if (ilRC!=RC_SUCCESS)
                dbg(TRACE,"Init_demcci: CreateIdx1 for CCC failed <%d>",ilRC);      
        }
    }
    if(ilRC == RC_SUCCESS)      
    {   
        ilRC = SetArrayInfo("PXCTAB", "PXC", PXC_FIELDS, 0, 0, "", &rgPxcArray,
                        CEDAARRAY_FILL|CEDAARRAY_TRIGGER );
        if ( ilRC == RC_SUCCESS)  
            ilRC = CreateIdx1 ( &rgPxcArray, "IDXPXC1", "UPRO,UCCC", "A,A" );
        if (ilRC != RC_SUCCESS)  
        {   
            dbg(TRACE,"Init_demcci: SetArrayInfo for PXC failed <%d>, possibly old PXCTAB", ilRC);
            dbg(TRACE,"Init_demcci: Transaction times from config-file will be used");
            ilRC= RC_SUCCESS;
            rgPxcArray.rrArrayHandle = -1;
        }
    }
    if(ilRC == RC_SUCCESS)      
    {   
        pllAddFieldLens[0] = 50 * (URNOLEN+1);
        pllAddFieldLens[1] = 0;
        ilRC = SetArrayInfo("DEMTAB", "DEM", DEM_FIELDS, "UAFT", pllAddFieldLens, 
                            "", &rgDemArray, 0 );
        if (ilRC != RC_SUCCESS)  
        {   
            dbg(TRACE,"Init_demcci: SetArrayInfo for DEM failed <%d>", ilRC);
            ilRC= RC_SUCCESS;
            rgDemArray.rrArrayHandle = -1;
        }
    }

    if (debug_level > TRACE)
        ilRC = SaveIndexInfo ( &rgCccArray, 2, 0 );
    
    if(ilRC == RC_SUCCESS)
    { /** read TICH ***********/
        char pclDataArea[2256];
        char pclSqlBuf[256];
        short slCursor = 0;
        sprintf(pclSqlBuf,
                "SELECT TICH,TDI1,TDI2 FROM APTTAB WHERE APC3 = '%s'", cgHopo);
        ilRC = sql_if(START, &slCursor, pclSqlBuf, pclDataArea);
        if (ilRC != DB_SUCCESS)
        {
            dbg(TRACE, "Home Airport <%s> not found in APTTAB %d", cgHopo,
                ilRC);
            check_ret(ilRC);
        }
        else
        {
            strcpy(cgTdi1, "60");
            strcpy(cgTdi2, "60");
            get_fld(pclDataArea, 0, STR, 14, cgTich);
            get_fld(pclDataArea, 1, STR, 14, cgTdi1);
            get_fld(pclDataArea, 2, STR, 14, cgTdi2);
            TrimRight(cgTich);
            if (*cgTich != '\0')
            {
                TrimRight(cgTdi1);
                TrimRight(cgTdi2);
                sprintf(cgTdi1, "%d", atoi(cgTdi1)*60);
                sprintf(cgTdi2, "%d", atoi(cgTdi2)*60);
            }
            dbg(TRACE,"Tich: <%s> Tdi1: <%s> Tdi2 <%s>", cgTich, cgTdi1,
                cgTdi2);
        }
        close_my_cursor(&slCursor);
        slCursor = 0;
    }

    if (ilRC == RC_SUCCESS)                                        
    {                                                              
        ilRC = iGetConfigEntry( cgConfigFile, "MINIMUM_OF_DEMAND_IN_MINITS",
                             "DEMANDMIN", CFG_INT, (char *)&igDemand );

        dbg(TRACE,"Minimum of Demand: <%d>",igDemand);

        if (igDemand == 0)
        {
            igDemand = DEMANDMINDEFAULT;
            dbg(TRACE,"No valid value for DEMANDMIN in Config-File, Default will be used for calculating");
        }
    }
    
    if (ilRC == RC_SUCCESS)                                        
    {                                                              
        ilRC = iGetConfigEntry( cgConfigFile, "QUEUETYPE",
                         "QUTYPE", CFG_STRING, cgQueueType );

        dbg(TRACE,"Typ of Queue: <%s>",cgQueueType);

        if (&cgQueueType[0] == 0)
        {
            sprintf( cgQueueType, QUTYPEDEFAULT);
            dbg(TRACE,"No valid value for QUTYPE in Config-File, Default will be used for calculating");
        }
    }
    
    if (ilRC == RC_SUCCESS)                                        
    {                                                              
        ilRC = iGetConfigEntry( cgConfigFile, "REST_PASSENGERS",
                         "RESTRATE", CFG_INT, (char *)&igRestRate );

        dbg(TRACE,"Restrate : <%d>",igRestRate);

        if (igRestRate == 0)
        {
            igRestRate = RESTRATEDEFAULT;
            dbg(TRACE,"No valid value for RESTRATE in Config-File, Default will be used for calculating");
        }
    }
    /*  
    if (ilRC == RC_SUCCESS)
    {
        llRow = ARR_FIRST;

        CEDAArrayGetRowCount(&(rgCccArray.rrArrayHandle),
                                rgCccArray.crArrayName,&lgCccRow);
        if (lgCccRow > 0)
        {
            prgCnf = (CNFARRAY*)malloc( lgCccRow * sizeof(CNFARRAY) ) ; 
        }

        if ( !prgCnf )
        {
            dbg( TRACE,"Init_demcci: not enough memory for internal CCCTAB");
            return RC_FAIL;
        }
        else
        {
            memset( prgCnf, 0, lgCccRow * sizeof(CNFARRAY) );
        }
    
        while(CEDAArrayFindRowPointer(&(rgCccArray.rrArrayHandle),
                                        rgCccArray.crArrayName,
                                        &(rgCccArray.rrIdx01Handle), 
                                        rgCccArray.crIdx01Name, "", &llRow,
                                        (void**)&pclResult) == RC_SUCCESS)
        {
            strcpy(prgCnf[i].crUccc, CCCFIELD(pclResult, igCccUrno));
            strcpy(clCicc, CCCFIELD(pclResult, igCccCicc));
            TrimRight(clCicc);
            dbg(TRACE, "CICC: <%s>", clCicc);

            ilRC = iGetConfigEntry(cgConfigFile, "QUEUEQUALITY", clCicc,
                                    CFG_INT, (char *)&prgCnf[i].irQuality);
            if (prgCnf[i].irQuality == 0)
            {
                dbg(TRACE,
                    "No valid Quality-Value for Class <%s>, default will be used for calculating",
                    clCicc);
                prgCnf[i].irQuality = QUEUEQUALITYDEFAULT;
            }
            dbg(TRACE,"Quality of Queue: <%d>",prgCnf[i].irQuality);

            ilRC = iGetConfigEntry(cgConfigFile, "TATIME_WITH_LUGGAGE", clCicc,
                                    CFG_INT,
                                    (char *)&prgCnf[i].irTTWithBaggage);

            if (prgCnf[i].irTTWithBaggage == 0)
            {
                dbg(TRACE,
                    "No valid TA time for PAX with Baggage for Class <%s>, default will be used for calculating",
                    clCicc);
                prgCnf[i].irTTWithBaggage = TAWITHBAGDEFAULT;
            }
            dbg(TRACE,"TT with Baggage : <%d>", prgCnf[i].irTTWithBaggage);

            ilRC = iGetConfigEntry(cgConfigFile,"TATIME_WITHOUT_LUGGAGE",clCicc,
                                    CFG_INT,
                                    (char *)&prgCnf[i].irTTWithoutBaggage);

            if (prgCnf[i].irTTWithoutBaggage == 0)
            {
                dbg(TRACE,
                    "No valid TA time for PAX without Baggage for Class <%s>, default will be used for calculating",
                    clCicc);
                prgCnf[i].irTTWithoutBaggage = TAWITHOUTBAGDEFAULT;
            }
            dbg(TRACE, "TT without Baggage : <%d>",
                prgCnf[i].irTTWithoutBaggage);

            llRow = ARR_NEXT;
            i++;
        }
    }       
    hag20011007: put into function "InitializeCNFARRAY", because we need it also from CCCTAB-updates */

    InitializeCNFARRAY ();

    igInitOK = TRUE;
    return ilRC;
}

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int ilRC = RC_SUCCESS;
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRC;
    
}

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate()
{
    /* unset SIGCHLD ! DB-Child will terminate ! */
    logoff();
    dbg(TRACE,"Terminate: now leaving ...");
    
    exit(0);
    
}

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
#if 0
static void HandleSignal(int pipSig)
{
    int ilRC = RC_SUCCESS;
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);
    switch(pipSig)
    {
        default :
            Terminate();
            break;
    }
    exit(0);
}
#endif

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    return;
}

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    switch(pipErr)
    {
        case QUE_E_FUNC :   /* Unknown function */
            dbg(TRACE,"<%d> : unknown function",pipErr);
            break;
        case QUE_E_MEMORY:  /* Malloc reports no memory */
            dbg(TRACE,"<%d> : malloc failed",pipErr);
            break;
        case QUE_E_SEND:    /* Error using msgsnd */
            dbg(TRACE,"<%d> : msgsnd failed",pipErr);
            break;
        case QUE_E_GET: /* Error using msgrcv */
            dbg(TRACE,"<%d> : msgrcv failed",pipErr);
            break;
        case QUE_E_EXISTS:
            dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
            break;
        case QUE_E_NOFIND:
            dbg(TRACE,"<%d> : route not found ",pipErr);
            break;
        case QUE_E_ACKUNEX:
            dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
            break;
        case QUE_E_STATUS:
            dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
            break;
        case QUE_E_INACTIVE:
            dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
            break;
        case QUE_E_MISACK:
            dbg(TRACE,"<%d> : missing ack ",pipErr);
            break;
        case QUE_E_NOQUEUES:
            dbg(TRACE,"<%d> : queue does not exist",pipErr);
            break;
        case QUE_E_RESP:    /* No response on CREATE */
            dbg(TRACE,"<%d> : no response on create",pipErr);
            break;
        case QUE_E_FULL:
            dbg(TRACE,"<%d> : too many route destinations",pipErr);
            break;
        case QUE_E_NOMSG:   /* No message on queue */
            dbg(TRACE,"<%d> : no messages on queue",pipErr);
            break;
        case QUE_E_INVORG:  /* Mod id by que call is 0 */
            dbg(TRACE,"<%d> : invalid originator=0",pipErr);
            break;
        case QUE_E_NOINIT:  /* Queues is not initialized*/
            dbg(TRACE,"<%d> : queues are not initialized",pipErr);
            break;
        case QUE_E_ITOBIG:
            dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
            break;
        case QUE_E_BUFSIZ:
            dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
            break;
        default:    /* Unknown queue error */
            dbg(TRACE,"<%d> : unknown error",pipErr);
            break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int ilRC = RC_SUCCESS;          /* Return code */
    int ilBreakOut = FALSE;
    
    do
    {
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text; 
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            }
        
            switch( prgEvent->command )
            {
                case HSB_STANDBY:
                    ctrl_sta = prgEvent->command;
                    break;  
    
                case HSB_COMING_UP:
                    ctrl_sta = prgEvent->command;
                    break;  
    
                case HSB_ACTIVE:
                    ctrl_sta = prgEvent->command;
                    ilBreakOut = TRUE;
                    break;  
                case HSB_ACT_TO_SBY:
                    ctrl_sta = prgEvent->command;
                    break;  
    
                case HSB_DOWN:
/*whole system shutdown - do not further use que(), send_message() or timsch()!*/
                    ctrl_sta = prgEvent->command;
                    Terminate();
                    break;  
    
                case HSB_STANDALONE:
                    ctrl_sta = prgEvent->command;
                    ResetDBCounter();
                    ilBreakOut = TRUE;
                    break;  
                case REMOTE_DB:
                    /* ctrl_sta is checked inside */
                    HandleRemoteDB(prgEvent);
                    break;
                case SHUTDOWN:
                    Terminate();
                    break;
                            
                case RESET:
                    ilRC = Reset();
                    break;
                            
                case EVENT_DATA:
                    dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                    break;
                        
                case TRACE_ON:
                    dbg_handle_debug(prgEvent->command);
                    break;
                case TRACE_OFF:
                    dbg_handle_debug(prgEvent->command);
                    break;
                default:
                    dbg(TRACE,"HandleQueues: unknown event");
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                    break;
            }
        }
        else
        {
            /* Handle queuing errors */
            HandleQueErr(ilRC);
        }
    } while (ilBreakOut == FALSE);

    if(igInitOK == FALSE)
    {
        ilRC = Init_demcci();
        if(ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"Init_demcci: init failed!");
        }
    }
}
    

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData()
{
    int ilRC = RC_SUCCESS;          /* Return code */
    
    BC_HEAD *prlBchd = NULL;
    CMDBLK  *prlCmdblk    = NULL;
    char    *pclSelKey    = NULL;
    char    *pclFields    = NULL;
    char    *pclData      = NULL;
    char    *pclIvStart     = NULL;
    char    *pclIvEnd     = NULL;
    char    *pclRudUrnos     = NULL;
    char    pclHost[16]  = "";
    char    pclTable[16]  = "";
    char    pclActCmd[16] = "";

    dbg(TRACE,"================= EVENT BEGIN ================");
    /* Queue-id of event sender */

    /* get broadcast header */
    prlBchd = (BC_HEAD *)((char *)prgEvent + sizeof(EVENT));

    /* Save Global Elements of BC_HEAD */
    memset(pcgRecvName, '\0', (sizeof(prlBchd->recv_name)+1));
    strcpy(pclHost,prlBchd->orig_name);                      /* Host        */
    strcpy(pcgDestName,prlBchd->dest_name);                  /* UserLoginName */
    strncpy(pcgRecvName, prlBchd->recv_name, sizeof(prlBchd->recv_name));  /* Workstation   */

    /* get command block  */
    prlCmdblk = (CMDBLK *)((char *)prlBchd->data);

    /* Save Global Elements of CMDBLK */
    strcpy(pcgTwStart,prlCmdblk->tw_start);
    strcpy(pcgTwEnd,prlCmdblk->tw_end);

    /*  3 Strings in CMDBLK -> data  */   
    pclSelKey = (char *)prlCmdblk->data;

    pclFields = (char *)pclSelKey + strlen(pclSelKey) + 1;
    pclData = (char *)pclFields + strlen(pclFields) + 1;
    strcpy (pclTable, prlCmdblk->obj_name);                   /*  table name  */
    strcpy (pclActCmd, prlCmdblk->command);               /*  actual command  */
    
    dbg(TRACE,"===================== START =====================" );
    dbg(TRACE,"FROM ID<%d> HOST<%s>", prgEvent->originator, pclHost);
    dbg(TRACE,"WKS<%s> USER<%s>", pcgRecvName, pcgDestName);
    dbg(TRACE,"CMD <%s>  TBL <%s>", pclActCmd,pclTable);
    dbg(TRACE,"SELECT <%s>", pclSelKey);
    dbg(TRACE,"FIELDS <%s>", pclFields);
    dbg(TRACE,"DATA   <%s>", pclData);
    dbg(TRACE,"TWSTART <%s> TWEND <%s>",pcgTwStart,pcgTwEnd);

    /*  COMPARE COMAND AND GOTO FUNCTION */
    
    if (!strcmp(pclActCmd,"CCD" ) ) /*  CCD  */
    {
        dbg(TRACE,"command CCD arrived");

/* parse Data-String -> [Startperiod<\n>Endperiod<\n>RudUrno1,RudUrno2,...] */

        pclIvStart = pclData;
        pclIvEnd  = strchr( pclIvStart, '\n' );
        if ( pclIvEnd && pclIvEnd[0] )  /* found End of interval */
        {
            *pclIvEnd ='\0';
            pclIvEnd++;

            pclRudUrnos = strchr( pclIvEnd, '\n' );
            if ( pclRudUrnos && pclRudUrnos[0] )    /* found RuleUrnos */
            {
                *pclRudUrnos = '\0';
                pclRudUrnos++;
                dbg(TRACE, "Intervall von <%s> bis <%s>", pclIvStart, pclIvEnd);
                dbg(TRACE, "RUDURNOS <%s>", pclRudUrnos);
                ilRC = HandleCCD(pclIvStart, pclIvEnd, pclRudUrnos);
            }
            else
                dbg(TRACE, "RUDURNOS are empty"); 
        }
        else
            dbg(TRACE, "End of interval is empty"); 
        
    }
    else if (!strcmp(pclActCmd, "CDF")) /*  CDF  */
    {
        dbg(TRACE, "command CDF arrived");
        ilRC = HandleCDF(prgEvent, pclFields, pclData);
    }
    else if (!strcmp(pclActCmd, "FLC")) /*  CDF  */
    {
        dbg(TRACE, "command FLC arrived");
        ilRC = HandleFLC(prgEvent, pclData);
    }
    else if ( !strcmp(pclActCmd,"IRT") || !strcmp(pclActCmd,"URT") ||
              !strcmp(pclActCmd,"DRT") )    /*  IRT,URT,DRT  */
    {
        ilRC = CEDAArrayEventUpdate( prgEvent ); 
        dbg(TRACE,"command IRT, URT or DRT processed, table<%s> ilRC <%ld>", 
            pclTable, ilRC);
        if ( strncmp ( pclTable, "CCC", 3 ) == 0 )
            InitializeCNFARRAY ();
    }
    else if (!strcmp(pclActCmd, "CFL")) /*  CFL  */
    {
        dbg(TRACE, "command CFL arrived");
        ilRC = ForwardEvent(igDest);
        if (ilRC != RC_SUCCESS)
        {
            dbg(TRACE, "HandleData: ForwardEvent failed <%d>", ilRC);
        }
    }
    else
        if ( !strcmp ( pclActCmd, "ANSWER" ) )
            dbg(TRACE, "ANSWER-event arrived");
        else
            dbg(TRACE, "unknown command <%s> found", pclActCmd);
    
    dbg(TRACE,"===================== END =====================" );
    return ilRC;
}

/******************************************************************************/
/* The Handle CCD-command                                                     */
/******************************************************************************/
static int HandleCCD(char *pcpIvStart, char *pcpIvEnd, char *pcpRudUrnos)
{
    int     ilRC = RC_SUCCESS;              /* Return code */
    char    clRudUrno[URNOLEN+1];
    char    clTmpStart[DATELEN+1];
    char    clTmpEnd[DATELEN+1];
    char    clMaxc[MAXLEN+1];
    char    clMinc[MINLEN+1];
    char    clLifr[LIMITLEN+1];
    char    clLito[LIMITLEN+1];
    char    clRefr[LIMITLEN+1];
    char    clReto[LIMITLEN+1];
    int     ilLoop = 1;
    int     ilNoOfItems = 0;
    time_t  tlIvStart = 0, tlIvEnd = 0, tlTmpStart = 0, tlTmpEnd = 0;
    int     ilRest;
    char    pclTimeStr[16] = "\0";

    if ( !pcpIvStart || !pcpIvEnd || !pcpRudUrnos)
    {
        dbg ( TRACE, "HandleCCD: At least one parameter = NULL" ); 
        return ilRC;
    }

    if ( !pcpIvStart[0] || !pcpIvEnd[0] || (strcmp(pcpIvEnd,pcpIvStart) <= 0) )
    {
        dbg ( TRACE, "HandleCCD: Invalid start <%s> or end <%s> of period found", pcpIvStart, pcpIvEnd ); 
        return ilRC;
    }
    ilNoOfItems = get_no_of_items(pcpRudUrnos);

    do
    {
        memset(clLifr, 0x00, LIMITLEN);
        memset(clLito, 0x00, LIMITLEN);
        memset(clMaxc, 0x00, MAXLEN);
        memset(clMinc, 0x00, MINLEN);
        memset(clRefr, 0x00, LIMITLEN);
        memset(clReto, 0x00, LIMITLEN);
        memset(pcgRefl, 0x00, LIMITLEN);

        ilRC = get_real_item(&clRudUrno[0],pcpRudUrnos,ilLoop);
        dbg(TRACE,"RUDURNONR:%d <%s>",ilLoop,&clRudUrno[0]);
        ilRC = GetFieldByUrno(&rgRudArray,clRudUrno,"LIFR",clLifr,LIMITLEN+1);
        ilRC = GetFieldByUrno(&rgRudArray,clRudUrno,"LITO",clLito,LIMITLEN+1);
        ilRC = GetFieldByUrno(&rgRudArray,clRudUrno,"MAXC",clMaxc,MAXLEN+1);
        ilRC = GetFieldByUrno(&rgRudArray,clRudUrno,"MINC",clMinc,MINLEN+1);
        dbg(TRACE, "LIFR: <%s>\tLITO: <%s>\tMAXC: <%s>\tMINC: <%s>", clLifr, clLito,
            clMaxc, clMinc);

        if( igRestrictOn == TRUE )
        {
            ilRC = GetFieldByUrno(&rgRudArray,clRudUrno,"REFR",clRefr,LIMITLEN+1);
            ilRC = GetFieldByUrno(&rgRudArray,clRudUrno,"RETO",clReto,LIMITLEN+1);
            ilRC = GetFieldByUrno(&rgRudArray,clRudUrno,"REFL",pcgRefl,LIMITLEN+1);
            dbg(TRACE, "REFR: <%s>\tRETO: <%s>\tREFL: <%s>", clRefr, clReto, pcgRefl );
        }
        igRestrictFrom = igRestrictTo = 0;
        igLifr = igLito = 0;
        if( igRestrictOn == TRUE && pcgRefl[0] == 0x31 )
        {
            igRestrictFrom = atoi(clRefr);
            igRestrictTo = atoi(clReto);
       }
        else
        {
            igLifr = atoi(clLifr);
            igLito = atoi(clLito);
        }
        igMaxc = atoi(clMaxc);
        igMinc = atoi(clMinc);

        StrToTime(pcpIvStart, &tlIvStart);
        StrToTime(pcpIvEnd, &tlIvEnd);
        tlTmpStart = tlIvStart;
        ilRest = tlTmpStart % (igDemand*60);
        if ( ilRest > 0 )
        {
            tlTmpStart -= ilRest;
            dbg ( TRACE, "HandleCCD: rounded start <%ld> -> <%ld>", tlIvStart, tlTmpStart );

        }
        StrToTime(pcpIvEnd, &tlTmpEnd);

        bgEndOfIvReached = FALSE;

        while(!bgEndOfIvReached)
        {
            memset(cgMsgData, 0x00, MSGBUFLEN);
            igPosInMsgData = 0;

            strcpy(&cgMsgData[igPosInMsgData], clRudUrno);
            igPosInMsgData += strlen(clRudUrno);
            if (igPosInMsgData >= MSGBUFLEN)
            {
                dbg(TRACE,"Not enough place for all data in message");
                return RC_FAILURE;
            }

            cgMsgData[igPosInMsgData] = 0x0A;
            igPosInMsgData += 1;
            if (igPosInMsgData >= MSGBUFLEN)
            {
                dbg(TRACE,"Not enough place for all data in message");
                return RC_FAILURE;
            }

            tlTmpEnd = tlTmpStart + 86400;
            if (tlTmpEnd >= tlIvEnd)
            {
                tlTmpEnd = tlIvEnd;
                bgEndOfIvReached = TRUE;
            }

            ilRest = tlTmpEnd % (igDemand*60);
            if ( ilRest > 0 )
            {
                tlIvStart = tlTmpEnd;
                tlTmpEnd -= ilRest;
                dbg ( TRACE, "HandleCCD: rounded end <%ld> -> <%ld>", tlIvStart, tlTmpEnd );
            }

            TimeToStr(clTmpStart,tlTmpStart);
            TimeToStr(clTmpEnd,tlTmpEnd);

            dbg(TRACE,"Temp. Intervall von <%s> bis <%s>",clTmpStart,clTmpEnd);
            /*  Collect SDI's for corresponding CCC-URNO of RUD  */
            ilRC = SearchingForSDI(clRudUrno,clTmpStart,clTmpEnd);
            if (ilRC == RC_SUCCESS)
                ilRC = CalcNewData(clTmpStart, clTmpEnd);
            tlTmpStart = tlTmpEnd;
        }
        ilLoop++;
    }while (ilLoop <= ilNoOfItems);

    return ilRC;
}

static int CreateIdx1( ARRAYINFO *prpArrayInfo, char *pcpIdxName,
                        char *pcpFields, char *pcpOrdering )
{
    int     ilRC = RC_FAIL, ilLoop;
    char    clOrderTxt[4];
    long    *pllOrdering=0;

    if ( !pcpFields )
        return ilRC;        /*  0-Pointer in parameter pcpFields */

    strcpy( prpArrayInfo->crIdx01Name, pcpIdxName );
    strcpy ( prpArrayInfo->crIdx01FieldList, pcpFields );
    
    ilRC = GetRowLength( prpArrayInfo->crTableName, pcpFields, 
                         &(prpArrayInfo->lrIdx01RowLen));
    
    if(ilRC != RC_SUCCESS)
        dbg(TRACE,"CreateIdx1: GetRowLength failed <%s> <%s>",
            prpArrayInfo->crTableName, pcpFields);
    else
    {
        prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,
                                                prpArrayInfo->lrIdx01RowLen+1);
        if( prpArrayInfo->pcrIdx01RowBuf == NULL )
        {
            ilRC = RC_FAIL;
            dbg(TRACE,"CreateIdx1: calloc failed");
        }
    }

    if(ilRC == RC_SUCCESS)
    {
        prpArrayInfo->lrIdx01FieldCnt = get_no_of_items(pcpFields);
        pllOrdering = (long *)calloc(prpArrayInfo->lrIdx01FieldCnt,
                                        sizeof(long));
        if(!pllOrdering )
        {
            dbg(TRACE,"SetArrayInfo: pllOrdering calloc(%d,%d) failed",
                        prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
            ilRC = RC_FAIL;
        }
        else
        {
            for(ilLoop=0; ilLoop < prpArrayInfo->lrIdx01FieldCnt; ilLoop++)
            {
                if ( !pcpOrdering ||
                    (get_real_item(clOrderTxt,pcpOrdering,ilLoop+1)
                                                    != RC_SUCCESS))
                    clOrderTxt[0] = 'D';
                if ( clOrderTxt[0] == 'A' )
                    pllOrdering[ilLoop] = ARR_ASC;
                else
                    pllOrdering[ilLoop] = ARR_DESC;
            }
        }
    }

    ilRC = CEDAArrayCreateMultiIndex( &(prpArrayInfo->rrArrayHandle), 
                                      prpArrayInfo->crArrayName,
                                      &(prpArrayInfo->rrIdx01Handle), 
                                      prpArrayInfo->crIdx01Name,
                                      prpArrayInfo->crIdx01FieldList, 
                                      pllOrdering );
    if ( pllOrdering )
        free (pllOrdering);
    pllOrdering = 0;
    return ilRC;                                    
}

static int GetRowLength(char *pcpTana, char *pcpFieldList, long *plpLen)
{
    int     ilRc = RC_SUCCESS;          /* Return code */
    int     ilNoOfItems = 0;
    int     ilLoop = 0;
    long    llFldLen = 0;
    long    llRowLen = 0;
    char    clFina[8];
    
    ilNoOfItems = get_no_of_items(pcpFieldList);

    ilLoop = 1;
    do
    {
        ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
        if(ilRc > 0)
        {
            ilRc = GetFieldLength(pcpTana,clFina,&llFldLen);
            if(ilRc == RC_SUCCESS)
            {
                llRowLen++;
                llRowLen += llFldLen;
            }
        }
        ilLoop++;
    }while((ilRc == RC_SUCCESS) && (ilLoop <= ilNoOfItems));

    if(ilRc == RC_SUCCESS)
    {
        *plpLen = llRowLen;
    }

    return ilRc;
}

static int GetFieldLength(char *pcpTana, char *pcpFina, long *plpLen)
{
    int     ilRc = RC_SUCCESS;          /* Return code */
    int     ilCnt = 0;
    char    clTaFi[32];
    char    clFele[16];
    char    clFldLst[16];

    ilCnt = 1;
    sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
    sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerst�rt */

    dbg(DEBUG,"GetFieldLength Tana <%s> Fina <%s>",pcpTana,pcpFina);

    ilRc = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",
                                    &clFele[0],&ilCnt,"");
    switch (ilRc)
    {
        case RC_SUCCESS:
            *plpLen = atoi(&clFele[0]);
            break;

        case RC_NOT_FOUND:
            dbg(TRACE,
                "GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s,%s>",
                ilRc,pcpTana,pcpFina);
            break;

        case RC_FAIL:
            dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
            break;

        default:
            dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",
                ilRc);
            break;
    }

    return ilRc;
    
}

/******************************************************************************/
/*  SetArrayInfo:                                                             */
/*  following flags activate/disactivate features to improve performance      */
/*  CEDAARRAY_FILL:  fill array from DB; rgNewSDIArr doesn't need to be filled*/
/*  CEDAARRAY_URNOIDX: create an index for URNO; if not used, we have one     */
/*                     Index less, which has to be sorted !                   */
/*  CEDAARRAY_TRIGGER: UCDTAB, that is used only by DISCAL doesn't need to be */
/*                     triggered                                              */
/******************************************************************************/

static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, 
                        char *pcpArrayFieldList,char *pcpAddFields,
                        long *pcpAddFieldLens,char *pcpSelection,
                        ARRAYINFO *prpArrayInfo, short spFlags )
{
    int ilRc = RC_SUCCESS, i, ilStart;

    if(prpArrayInfo == NULL)
    {
        dbg(TRACE,
            "SetArrayInfo: invalid last parameter : null pointer not valid");
        ilRc = RC_FAIL;
    }
    else
    {
        memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

        prpArrayInfo->rrArrayHandle = -1;
        prpArrayInfo->rrIdx01Handle = -1;

        if(pcpArrayName != NULL)
        {
            if(strlen(pcpArrayName) <= ARR_NAME_LEN)
            {
                strcpy(prpArrayInfo->crArrayName,pcpArrayName);
            }
        }
    }

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);
        if (pcpAddFields != NULL)
        {
            prpArrayInfo->lrArrayFieldCnt += get_no_of_items(pcpAddFields);
        }

        prpArrayInfo->plrArrayFieldOfs = 
                    (long*)calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
        if(prpArrayInfo->plrArrayFieldOfs == NULL)
        {
            dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",
                prpArrayInfo->lrArrayFieldCnt,sizeof(long));
            ilRc = RC_FAIL;
        }
    }

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->plrArrayFieldLen = 
                    (long*)calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
        if(prpArrayInfo->plrArrayFieldLen == NULL)
        {
            dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",
                prpArrayInfo->lrArrayFieldCnt,sizeof(long));
            ilRc = RC_FAIL;
        }
        else
        {
            *(prpArrayInfo->plrArrayFieldLen) = -1;
        }
    }

    if(pcpTableName == NULL)
    {
        dbg(TRACE,
            "SetArrayInfo:invalid parameter TableName: null pointer not valid");
        ilRc = RC_FAIL;
    }
    else
    {
        strcpy ( prpArrayInfo->crTableName,pcpTableName );
        strcat ( prpArrayInfo->crTableName,cgTabEnd );
        dbg(TRACE,"SetArrayInfo: crTableName=<%s>",prpArrayInfo->crTableName );
    }
    if(ilRc == RC_SUCCESS)
    {
        ilRc = GetRowLength(prpArrayInfo->crTableName, pcpArrayFieldList,
                            &(prpArrayInfo->lrArrayRowLen));
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",
                pcpArrayName,pcpArrayFieldList);
        }
    }

    if(ilRc == RC_SUCCESS)
    {
        dbg(DEBUG,"CEDAArrayCreate follows Array <%s> Table <%s>",pcpArrayName,
                                                    prpArrayInfo->crTableName);
        ilRc = CEDAArrayCreate( &(prpArrayInfo->rrArrayHandle),
                                pcpArrayName,prpArrayInfo->crTableName,
                                pcpSelection,pcpAddFields, pcpAddFieldLens, 
                                pcpArrayFieldList,
                                &(prpArrayInfo->plrArrayFieldLen[0]),
                                &(prpArrayInfo->plrArrayFieldOfs[0]));

        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"SetArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
        }
        else
        {
            if (pcpAddFields != NULL)
            {
                ilStart = get_no_of_items(pcpArrayFieldList);
                for ( i=ilStart; i<prpArrayInfo->lrArrayFieldCnt; i++ )
                {
                    prpArrayInfo->plrArrayFieldLen[i] = pcpAddFieldLens[i-ilStart];
                    prpArrayInfo->lrArrayRowLen += prpArrayInfo->lrArrayRowLen;
                }
            }

        }
    }

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->lrArrayRowLen+=100;
        prpArrayInfo->pcrArrayRowBuf = 
                            (char *)calloc(1,(prpArrayInfo->lrArrayRowLen+10));
        if(prpArrayInfo->pcrArrayRowBuf == NULL)
        {
            ilRc = RC_FAIL;
            dbg(TRACE,"SetArrayInfo: calloc failed");
        }
    }
    if(pcpArrayFieldList != NULL)
    {
        if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
        {
            strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
        }
    }
    if(pcpAddFields != NULL)
    {
        if(strlen(pcpAddFields) + strlen(prpArrayInfo->crArrayFieldList) <= 
                    (size_t)ARR_FLDLST_LEN)
        {
            strcat(prpArrayInfo->crArrayFieldList,",");
            strcat(prpArrayInfo->crArrayFieldList,pcpAddFields);
            dbg(DEBUG,"<%s>",prpArrayInfo->crArrayFieldList);
        }
    }
    if ( (ilRc == RC_SUCCESS) && (spFlags&CEDAARRAY_FILL) )
    {
        ilRc = CEDAArrayFill(&(prpArrayInfo->rrArrayHandle),pcpArrayName,NULL);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"SetArrayInfo: CEDAArrayFill failed <%d>",ilRc);
        }
    }
    if ( (ilRc == RC_SUCCESS) && (spFlags&CEDAARRAY_URNOIDX) && 
          strstr(pcpArrayFieldList,"URNO") )
    {
        strcpy ( prpArrayInfo->crIdxUrnoName, "URNOIDX" );
        ilRc = CEDAArrayCreateSimpleIndexDown(&(prpArrayInfo->rrArrayHandle),
                                              prpArrayInfo->crArrayName,
                                              &(prpArrayInfo->rrIdxUrnoHandle), 
                                              prpArrayInfo->crIdxUrnoName,
                                             "URNO" );
        if(ilRc != RC_SUCCESS)                                             
        {
            dbg(TRACE,
                "SetArrayInfo:CEDAArrayCreateSimpleIndexDown for %s failed %d",
                 prpArrayInfo->crTableName, ilRc);      
        }
    }
    else
        prpArrayInfo->rrIdxUrnoHandle = -1;
    if ( (ilRc == RC_SUCCESS) && (spFlags&CEDAARRAY_TRIGGER) )
    {                                 /* send update msg. for active changes */ 
        ilRc = ConfigureAction(prpArrayInfo->crTableName,pcpArrayFieldList, NULL ) ;
        if (ilRc != RC_SUCCESS)
            dbg (TRACE, "SetArrayInfo: triggerAction table<%s> failed <%d>", 
                  prpArrayInfo->crTableName, ilRc) ;
        else
            dbg (DEBUG, "SetArrayInfo: triggerAction table<%s> succeeded.", 
                  prpArrayInfo->crTableName ) ; 
    }

    return ilRc;
}



static int GetFieldByUrno ( ARRAYINFO *prpArray, char *pcpUrno, char *pcpField, 
                              char *pcpResult, int ipMaxBytes )
{
    int     ilRC1 = RC_FAIL;    
    int     ilRC = RC_FAIL;
    long    llRow = ARR_FIRST;
    long    llFieldNr = -1;
    char    *pclResult = 0;

    if ( prpArray && pcpUrno )
    {
        ilRC1 = CEDAArrayFindRowPointer( &(prpArray->rrArrayHandle), 
                                        prpArray->crArrayName,
                                        &(prpArray->rrIdxUrnoHandle), 
                                        prpArray->crIdxUrnoName, pcpUrno, 
                                        &llRow, (void**)&pclResult ); 
        dbg ( DEBUG, "GetFieldByUrno Urno <%s> llRow <%ld> RC <%d>", 
                  pcpUrno, llRow, ilRC1 );  
        if ( ilRC1 == RC_SUCCESS )
            ilRC = CEDAArrayGetField ( &(prpArray->rrArrayHandle), 
                                  prpArray->crArrayName, &llFieldNr, pcpField,
                                  ipMaxBytes, llRow, pcpResult);
        if ( ilRC == RC_SUCCESS )
            dbg(DEBUG, "GetFieldByUrno Urno <%s>  Field <%s>: data <%s>", 
                        pcpField, pcpUrno, pcpResult );
        else
            dbg(TRACE, "GetFieldByUrno Urno <%s> Field <%s> : RC1,RC <%d  %d>",
                        pcpField, pcpUrno, ilRC1, ilRC );
    }
    return ilRC;
}

void TrimRight(char *s)
{    /* search for last non-space character */
    int i = 0;

    for (i = strlen(s) - 1; i >= 0 && isspace(s[i]); i--); 
                                                /* trim off right spaces */
    s[++i] = '\0';
}

static int SaveIndexInfo (ARRAYINFO *prpArrayInfo, int ipMode, int ipLogFile )
{
    int     ilRC = RC_SUCCESS;
    long    llRow = 0 ;
    FILE    *prlFile = NULL;
    char    clDel = ',' ;
    char   clFile[512] ;
    char  *pclTrimBuf = NULL ;
    long    llRowCount;
    int     ilCount = 0;
    char    *pclTestBuf = NULL ;
    HANDLE  *pslIdxHdl;
    char    *pclIdxName;
    char    clIdxFieldList[ARR_FLDLST_LEN+1];
    char    *pclRowBuff=0;
    int     ilRowLen;

    switch ( ipMode )
    {
        case 1:
            pslIdxHdl = &(prpArrayInfo->rrIdxUrnoHandle);
            pclIdxName = prpArrayInfo->crIdxUrnoName;
            strcpy( clIdxFieldList, "URNO" );
            ilRowLen = 11;
            break;
        case 2:
            pslIdxHdl = &(prpArrayInfo->rrIdx01Handle);
            pclIdxName = prpArrayInfo->crIdx01Name;
            strcpy ( clIdxFieldList, prpArrayInfo->crIdx01FieldList );
            ilRowLen = prpArrayInfo->lrIdx01RowLen;
            break;
        default:
            pslIdxHdl = 0;
    }
    if ( pslIdxHdl )
    {
        pclRowBuff = (char *) calloc(1, ilRowLen+1);
        dbg(TRACE,"SaveIndexInfo: SetAATArrayInfo <%s>",prpArrayInfo->crArrayName) ;

        if ( *pslIdxHdl==-1) 
        {
            ilRC = RC_FAIL;
            dbg ( TRACE, "SaveIndexInfo: IdxHandle == -1, Mode <%d>", ipMode );
        }

        if ( !pclRowBuff )
        {
            dbg ( TRACE, "SaveIndexInfo: Calloc failed" );
            ilRC = RC_FAIL;
        }
    }       
    if (ilRC == RC_SUCCESS)
    {
        sprintf(&clFile[0],"%s/%s_%s.info",getenv("DBG_PATH"),mod_name,
                prpArrayInfo->crArrayName);

        errno = 0 ;
        prlFile = fopen(&clFile[0], "w");
        if (prlFile == NULL)
        {
            dbg (TRACE, "SaveIndexInfo: fopen <%s> failed <%d-%s>", clFile, 
                  errno, strerror(errno));
            ilRC = RC_FAIL;
        }
    }

    if (ilRC == RC_SUCCESS)
    {
        if ( ipLogFile )
        {
            dbg(DEBUG, "ArrayName      <%s>\n", prpArrayInfo->crArrayName);
            dbg(DEBUG, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList);
            if ( pslIdxHdl )
                dbg(DEBUG, "IdxFieldList <%s>\n", clIdxFieldList);  
        }
        fprintf(prlFile,"ArrayFieldList <%s>\n",prpArrayInfo->crArrayFieldList);
        if ( pslIdxHdl )
            fprintf (prlFile, "Idx01FieldList <%s>\n", clIdxFieldList); 
        fflush(prlFile);
        if ( pslIdxHdl )
        {
            if ( ipLogFile )
                dbg(DEBUG,"IndexData"); 
            fprintf(prlFile,"IndexData\n"); fflush(prlFile);

            pclRowBuff[0] = '\0';
            llRow = ARR_FIRST;
            do
            {
                ilRC = CEDAArrayFindKey( &(prpArrayInfo->rrArrayHandle), 
                                          prpArrayInfo->crArrayName, pslIdxHdl, 
                                          pclIdxName, &llRow, ilRowLen, pclRowBuff);
                if (ilRC == RC_SUCCESS)
                {
                    if ( ipLogFile )
                        dbg(DEBUG,"SaveIndexInfo: Key <%ld> <%s>",llRow,pclRowBuff);
        
                    if (strlen (pclRowBuff) == 0)
                    {
                        ilRC = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle),
                                                &(prpArrayInfo->crArrayName[0]),
                                                llRow, clDel,
                                                prpArrayInfo->lrArrayRowLen,
                                                prpArrayInfo->pcrArrayRowBuf);
                        if( ipLogFile && (ilRC == RC_SUCCESS) )
                            dbg(DEBUG,"SaveIndexInfo: Row <%ld> <%s>",llRow,
                                prpArrayInfo->pcrArrayRowBuf);
                    }

                    pclTrimBuf = strdup(pclRowBuff);
                    if (pclTrimBuf != NULL)
                    {
                        ilRC = GetDataItem(pclTrimBuf,pclRowBuff,1,clDel,"","  ");
                        if (ilRC > 0)
                        {
                            ilRC = RC_SUCCESS;
                            fprintf(prlFile,"Key            <%ld><%s>\n",llRow,
                                    pclTrimBuf);
                            fflush(prlFile) ;
                        }
                        free (pclTrimBuf);
                        pclTrimBuf = NULL;
                    }
                    llRow = ARR_NEXT;
                }
                else
                {
                    if ( ipLogFile )
                        dbg(TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>",
                             ilRC);
                }
            } while (ilRC == RC_SUCCESS);
        }
        if ( ipLogFile )
            dbg (DEBUG,"ArrayData");
    
        fprintf (prlFile,"ArrayData\n"); fflush(prlFile);
        llRow = ARR_FIRST;

        CEDAArrayGetRowCount(&(prpArrayInfo->rrArrayHandle),
                                prpArrayInfo->crArrayName,&llRowCount);

        llRow = ARR_FIRST;
        if ( ipLogFile )
            dbg(DEBUG,"Array  <%s> data follows Rows %ld",
                 prpArrayInfo->crArrayName,llRowCount);
        do
        {
            ilRC = CEDAArrayGetRowPointer(&(prpArrayInfo->rrArrayHandle),
            &(prpArrayInfo->crArrayName[0]),llRow,(void *)&pclTestBuf);
            if (ilRC == RC_SUCCESS)
            {
                for( ilCount = 0; ilCount < prpArrayInfo->lrArrayFieldCnt;
                     ilCount++)
                {
                    fprintf(prlFile,"<%s>",
                        &pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]);
                    fflush(prlFile);
                    if ( ipLogFile )
                        dbg(DEBUG,"SaveIndexInfo: ilCount <%ld> <%s>",ilCount,
                          &pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]);
                }
                fprintf (prlFile,"\n"); 
                if ( ipLogFile )
                    dbg (DEBUG, "\n");
                fflush(prlFile);
                llRow = ARR_NEXT;
            }
            else
            {
                if ( ipLogFile )
                    dbg(DEBUG,"GetRowPointer failed RC=%d",ilRC);
            }
        } while (ilRC == RC_SUCCESS);

        if (ilRC == RC_NOTFOUND)
            ilRC = RC_SUCCESS ;
    }

    if (prlFile != NULL)
    {
        fclose(prlFile);
        prlFile = NULL;
        if ( ipLogFile )
            dbg (DEBUG, "SaveIndexInfo: event saved - file <%s>", clFile);
    }
    if ( pclRowBuff )
        free( pclRowBuff );
    return ilRC;

}/* end of SaveIndexInfo */

static int CalcNewData(char *pcpIvStart, char *pcpIvEnd)
{
    int     ilRC = RC_SUCCESS;
    int     ilNoOfRec;
    int     ilRecno = 0;
    long    llRow = ARR_FIRST;
    char    *pclResult = 0;
    time_t  tlIvStart, tlIvEnd;
    time_t  tlSdiStart, tlSdiEnd;
    time_t  tlLiStart = 0, tlLiEnd = 0;
    time_t  tlRestrictFrom = 0, tlRestrictTo = 0;
    short   slStepInSec = igDemand*60;
    short   slSdiStep;
    float   flDbag = 0.0;
    int     i,j, ilValue;
    float   flNoOfPAX = 0.0, flPartOfPAX = 0.0;
    char    clData[DATALEN+1];
    char    clNoOfPAXStr[3+1];
    char    clTimeStr[DATELEN+1];
    float   flStartRest, flRest;
    long    llNoOfFlights;
    float   flSumOfTT = 0.0;
    int     ilCcc = 0;
    float   flServedRate = 0.0;
    int     ilNoOfEmpl = 0;
    int     ilNoOfItems = 0;
    int     *pilTTBaggage=0;
    int     *pilTTWoBaggage=0;
    char    clUpro[11], clUccc[11];
    char    pclTmpStr[128];
	time_t  tlTmpTime;
    char    *pclFunc = "CalcNewData";

    /*start- and end-time of working interval in UNIX-time*/
    StrToTime(pcpIvStart, &tlIvStart);
    StrToTime(pcpIvEnd, &tlIvEnd);

    /* this part will work because we calculate at most 24 hours */
    GetNextDate ( pcpIvStart, igLifr, clTimeStr );
    /*
    sprintf(clTimeStr, "%8.8s%4.4d00", pcpIvStart, igLifr);
    if (strcmp(clTimeStr, pcpIvStart) < 0)
        AddSecondsToCEDATime(clTimeStr, 86400, 1);
    LocalToUtc(clTimeStr);*/
    StrToTime(clTimeStr, &tlLiStart);

    dbg(TRACE, "Opening of counters is <%s>", clTimeStr);

    GetNextDate ( pcpIvStart, igLito, clTimeStr );
    /*
    sprintf(clTimeStr, "%8.8s%4.4d00", pcpIvEnd, igLito);
    if (strcmp(clTimeStr, pcpIvEnd) > 0)
        AddSecondsToCEDATime(clTimeStr, -86400, 1);
    LocalToUtc(clTimeStr);*/
    StrToTime(clTimeStr, &tlLiEnd);

    dbg(TRACE, "Closing of counters is <%s>", clTimeStr);

    /* MEI v1.16 */
    if( igRestrictOn == TRUE && pcgRefl[0] == 0x31 )
    {
        if( igRestrictFrom > 0 )
        {
            GetNextDate ( pcpIvStart, igRestrictFrom, clTimeStr );
            StrToTime(clTimeStr, &tlRestrictFrom);
            dbg(TRACE, "Restrict From <%s>", clTimeStr);
        }   
        if( igRestrictTo > 0 )
        {
            GetNextDate ( pcpIvStart, igRestrictTo, clTimeStr );
            StrToTime(clTimeStr, &tlRestrictTo);
            dbg(TRACE, "Restrict To <%s>", clTimeStr);
        }
    }
    /************/

    strcpy(&cgMsgData[igPosInMsgData],pcpIvStart);
    igPosInMsgData += strlen(pcpIvStart);
    if (igPosInMsgData > MSGBUFLEN)
    {
        dbg(TRACE,"Not enough place for all data in message");
        return RC_FAILURE;
    }

    cgMsgData[igPosInMsgData]=0x0A;
    igPosInMsgData += 1;
    if (igPosInMsgData > MSGBUFLEN)
    {
        dbg(TRACE,"Not enough place for all data in message");
        return RC_FAILURE;
    }

    strcpy(&cgMsgData[igPosInMsgData],pcpIvEnd);
    igPosInMsgData += strlen(pcpIvEnd);
    if (igPosInMsgData > MSGBUFLEN)
    {
        dbg(TRACE,"Not enough place for all data in message");
        return RC_FAILURE;
    }

    cgMsgData[igPosInMsgData]=0x0A;
    igPosInMsgData += 1;
    if (igPosInMsgData > MSGBUFLEN)
    {
        dbg(TRACE,"Not enough place for all data in message");
        return RC_FAILURE;
    }

    sprintf(&cgMsgData[igPosInMsgData],"%3d",igDemand);
    igPosInMsgData += 3;
    if (igPosInMsgData > MSGBUFLEN)
    {
        dbg(TRACE,"Not enough place for all data in message");
        return RC_FAILURE;
    }

    cgMsgData[igPosInMsgData]=0x0A;
    igPosInMsgData += 1;
    if (igPosInMsgData > MSGBUFLEN)
    {
        dbg(TRACE,"Not enough place for all data in message");
        return RC_FAILURE;
    }

    ilRC = CEDAArrayGetRowCount(&(rgSdiWrkArray.rrArrayHandle),
                                rgSdiWrkArray.crArrayName, &llNoOfFlights);
    if (ilRC != RC_SUCCESS)
    {
        dbg( TRACE,"CalcNewData: cannot read internal sditab");
        return ilRC;
    }
    dbg(DEBUG,"Number of flightrecords <%d>",llNoOfFlights);

    if (llNoOfFlights == 0)
    {
        llNoOfFlights = 1;
    }

    ilNoOfRec = (tlIvEnd-tlIvStart)/slStepInSec;
    dbg(DEBUG,"Number of records to calculate <%d>",ilNoOfRec);
    
    if (ilNoOfRec > 0)
    {
        prgWrk = (WRKARRAY*)malloc( ilNoOfRec * sizeof(WRKARRAY) ) ;    
    }

    if ( !prgWrk )
    {
        dbg( TRACE,"CalcNewData: not enough memory for calculating");
        return RC_FAIL;
    }
    else
    {
        memset( prgWrk, 0, ilNoOfRec * sizeof(WRKARRAY) );
    }
    
    if (llNoOfFlights > 0)
    {
        prgPAX = (PAXARRAY*)malloc(llNoOfFlights*ilNoOfRec * sizeof(PAXARRAY)); 
    }
    if ( !prgPAX )
    {
        dbg( TRACE,"CalcNewData: not enough memory for calculating");
        return RC_FAIL;
    }
    else
    {
        memset( prgPAX, 0, llNoOfFlights * ilNoOfRec * sizeof(PAXARRAY) );
    }

    if ( llNoOfFlights > 0 )
    {
        pilTTBaggage = (int*)malloc(llNoOfFlights* sizeof(int));    
        pilTTWoBaggage = (int*)malloc(llNoOfFlights* sizeof(int));  
        if ( !pilTTBaggage || !pilTTWoBaggage )
        {
            dbg( TRACE,"CalcNewData: not enough memory for array of transacion rates");
            return RC_NOMEM;
        }
        memset( pilTTBaggage, 0, llNoOfFlights* sizeof(int) );
        memset( pilTTWoBaggage, 0, llNoOfFlights* sizeof(int) );
    }

    for (i = 0; i < ilNoOfRec; i++)
    {
        prgWrk[i].lrStartInSeconds = tlIvStart + i*slStepInSec;
        prgWrk[i].prrPAXToFlight = prgPAX + i*(llNoOfFlights*sizeof(PAXARRAY));
    }

    while (CEDAArrayGetRowPointer( &(rgSdiWrkArray.rrArrayHandle),
                                    rgSdiWrkArray.crArrayName, llRow,
                                    (void**)&pclResult) == RC_SUCCESS)
    {
        ilRC = FindCccPosition(SDIWRKFIELD(pclResult, igSdiUccc), &ilCcc);
        if ( ilRC != RC_SUCCESS)
        {
            dbg( TRACE,"CalcNewData: no valid UCCC in SDI");
            return ilRC;            
        }
        StrToTime(SDIWRKFIELD(pclResult, igSdiPbea), &tlSdiStart);
        StrToTime(SDIWRKFIELD(pclResult, igSdiPena), &tlSdiEnd);
        slSdiStep = atoi(SDIWRKFIELD(pclResult, igSdiStep))*60;

        flDbag = atof(SDIWRKFIELD(pclResult, igSdiDbag));
        dbg(TRACE,"Dbag <%f> ",flDbag );
        strncpy(clData, SDIWRKFIELD(pclResult, igSdiData), DATALEN);
        clData[DATALEN] = '\0';
        dbg(TRACE,"Data <%s> ",clData );
        dbg(DEBUG,"Interval of SDI <%d - %d> Step <%d>",tlSdiStart,tlSdiEnd,
            slSdiStep);
        dbg(DEBUG,"Interval <%d - %d> Step <%d>",tlIvStart,tlIvEnd,slStepInSec);
        
        i = 0;
        j = 0;
        ilNoOfItems = myget_no_of_items(clData);
        dbg(DEBUG,"No of Items is %d",ilNoOfItems);
        while(TIMETODATA((j+1)) <= prgWrk[i].lrStartInSeconds)
        { /*while there are no relevant data in SDI-data*/
                j++;
        }

        while((TIMETODATA(j) < tlIvEnd) && (TIMETODATA(j) < tlSdiEnd))
        { /*while there are relevant data in SDI-data*/
            while (TIMETODATA(j) < (prgWrk[i].lrStartInSeconds + slStepInSec))
            { /*while we are in this working interval*/
                ilRC = GetDataItem(clNoOfPAXStr,clData,ilNoOfItems-j,'|',"",
                                    "\0 ");
                flNoOfPAX = atof(clNoOfPAXStr);

                if (TIMETODATA((j+1)) >
                                 (prgWrk[i].lrStartInSeconds + slStepInSec))
                { /*part of passengers belong to the next interval*/
                    flPartOfPAX = ((float)(prgWrk[i].lrStartInSeconds
                                     + slStepInSec
                                     - TIMETODATA(j)) / slSdiStep)
                                     * flNoOfPAX;
                    prgWrk[i].frSumOfPAX += flPartOfPAX;
                    prgPAX[ilRecno+i*llNoOfFlights].frNoOfPAX += flPartOfPAX;
                    if ( i < (ilNoOfRec-1))
                    {
                        prgWrk[i+1].frSumOfPAX += flNoOfPAX - flPartOfPAX;
                        prgPAX[ilRecno+i*llNoOfFlights+1].frNoOfPAX += flNoOfPAX
                                                                 - flPartOfPAX;
                    }
                }
                else
                { /*all passengers belong to this interval*/
                    prgPAX[llNoOfFlights*i+ilRecno].frNoOfPAX += flNoOfPAX;
                    prgWrk[i].frSumOfPAX += flNoOfPAX;
                }
                dbg(DEBUG,"NoOfPAX <%f>  PartOfPAX <%f>",flNoOfPAX,flPartOfPAX);
                j++;
            }
            i++;
        }
        pilTTBaggage[ilRecno] = prgCnf[ilCcc].irTTWithBaggage;
        pilTTWoBaggage[ilRecno] = prgCnf[ilCcc].irTTWithoutBaggage;
        
        if ( rgPxcArray.rrArrayHandle>=0 )
        {
            strncpy ( clUpro, SDIWRKFIELD(pclResult, igSdiUpro), 10 );
            strncpy ( clUccc, SDIWRKFIELD(pclResult, igSdiUccc), 10 );
            clUpro[10] = '\0';
            clUccc[10] = '\0';
            if ( GetTransactionTime ( clUpro, clUccc, igPxcTtwo, &ilValue) == RC_SUCCESS )
            {
                pilTTWoBaggage[ilRecno] = ilValue;
                /*dbg ( TRACE, "CalcNewData: Setting TT w/o baggage: UAFT <%s> UCCC <%s> value <%d>",
                      SDIWRKFIELD(pclResult, igSdiUaft), clUccc, pilTTWoBaggage[ilRecno] );*/
            }
            if ( GetTransactionTime ( clUpro, clUccc, igPxcTtbg, &ilValue) == RC_SUCCESS )
            {
                pilTTBaggage[ilRecno] = ilValue;
                /*dbg ( TRACE, "CalcNewData: Setting TT with baggage: UAFT <%s> UCCC <%s> value <%d>",
                      SDIWRKFIELD(pclResult, igSdiUaft), clUccc, pilTTBaggage[ilRecno] );*/
            }
            dbg ( TRACE, "CalcNewData: TT timing- UAFT <%s> UCCC <%s> TTwbag <%d> TTwobag <%d>",
                      SDIWRKFIELD(pclResult, igSdiUaft), clUccc, pilTTBaggage[ilRecno], pilTTWoBaggage[ilRecno] );
        }
        ilRecno++;
        llRow = ARR_NEXT;
    }

    /*start value for rest of passengers*/
    flStartRest = prgCnf[ilCcc].irQuality * igRestRate / 100;
    dbg(TRACE,"StartRest <%f>",flStartRest);

    for (i = 0; i < ilNoOfRec; i++)
    {
        for (j = 0; j < llNoOfFlights;j++)
        {
            dbg(DEBUG,"PAX <%f>, i = %d, j = %d",
                prgPAX[llNoOfFlights*i+j].frNoOfPAX,i,j);

            /*medium transaction time for all relevant passengers*/
            /*
            flSumOfTT += (prgCnf[ilCcc].irTTWithBaggage * flDbag/100 
                        + prgCnf[ilCcc].irTTWithoutBaggage * (100 - flDbag)/100)
                        * prgPAX[i*llNoOfFlights+j].frNoOfPAX;*/
            dbg(DEBUG,"pilTTBaggage <%d>  frNoOfPAX <%f> flDbag <%f>", pilTTBaggage[j], 
                prgPAX[i*llNoOfFlights+j].frNoOfPAX, flDbag );

            flSumOfTT += (pilTTBaggage[j] * flDbag/100 
                        + pilTTWoBaggage[j] * (100 - flDbag)/100)
                        * prgPAX[i*llNoOfFlights+j].frNoOfPAX;
            dbg(DEBUG,"Sum of TT<%f>",flSumOfTT);
        }

        dbg(DEBUG,"Sum of PAX <%f>",prgWrk[i].frSumOfPAX);
        dbg(DEBUG,"Sum of TT<%f>",flSumOfTT);
    
        if (prgWrk[i].frSumOfPAX != 0.0)
        {
            flServedRate = slStepInSec / (flSumOfTT / prgWrk[i].frSumOfPAX);
            dbg(DEBUG,"ServedRate <%f>",flServedRate);
            dbg(DEBUG,"Quality <%d>",prgCnf[ilCcc].irQuality);
            if (strcmp(cgQueueType,"COMMON") == 0)
            { /*all data are calculated together*/
                ilNoOfEmpl = floor(((prgWrk[i].frSumOfPAX + flStartRest)
                             - (float)prgCnf[ilCcc].irQuality)
                            / flServedRate) + 1;
                if ( ilNoOfEmpl < 0 )
                    ilNoOfEmpl = 0; 
            }
            else
            { /*"SINGLE",data are calculated for every counter*/
                ilNoOfEmpl = floor((prgWrk[i].frSumOfPAX + flStartRest) /
                             (flServedRate + (float)prgCnf[ilCcc].irQuality)) + 1;
            }
        }
        else
            ilNoOfEmpl = 0;
        dbg(TRACE,"NoOfEmpl <%d>",ilNoOfEmpl);
        dbg(TRACE,"tlLiStart <%d> tlLiEnd <%d>",tlLiStart,tlLiEnd);
        dbg(TRACE,"Start of Step <%d>",prgWrk[i].lrStartInSeconds);

/*igMinc is only active between Lifr and Lito if they are at the same day or
  not active between Lito and Lifr if they are at different days*/
        /*
        if ((((tlLiStart < tlLiEnd) && (prgWrk[i].lrStartInSeconds >= tlLiStart)
                                 && (prgWrk[i].lrStartInSeconds < tlLiEnd))
            || (!(tlLiStart < tlLiEnd) &&
                (prgWrk[i].lrStartInSeconds >= tlLiStart)
                || (prgWrk[i].lrStartInSeconds < tlLiEnd)))
            && (ilNoOfEmpl < igMinc))
        {
            ilNoOfEmpl = igMinc;
            dbg(TRACE, "ilNoOfEmpl is set to igMinc");
        }
            */
        if ( ilNoOfEmpl < igMinc )
        {
            if( (igRestrictOn == TRUE && pcgRefl[0] != 0x31) || (igRestrictOn == FALSE) )
            {
                if ( (tlLiStart < tlLiEnd) && (prgWrk[i].lrStartInSeconds >= tlLiStart)
                      && (prgWrk[i].lrStartInSeconds < tlLiEnd) )
                {
                    ilNoOfEmpl = igMinc;
                    dbg(TRACE, "Inside pos. Interval -> ilNoOfEmpl is set to igMinc");
                }
                if ( (tlLiStart > tlLiEnd) && 
                     ( (prgWrk[i].lrStartInSeconds >= tlLiStart)
                    || (prgWrk[i].lrStartInSeconds < tlLiEnd) ) )
                {
                    ilNoOfEmpl = igMinc;
                    dbg(TRACE, "Outside neg. Interval -> ilNoOfEmpl is set to igMinc");
                }
                if ( tlLiStart == tlLiEnd ) 
                {
                    ilNoOfEmpl = igMinc;
                    dbg(TRACE, "empty Interval -> ilNoOfEmpl is set to igMinc");
                }
            }
        }

        if ((igMaxc > 0) && (ilNoOfEmpl > igMaxc))
        {
            ilNoOfEmpl = igMaxc;
            dbg(TRACE, "ilNoOfEmpl is set to igMaxc");
        }


        /** MEI v1.16 ***/
        dbg( TRACE, "RESTRICT FLAG <%s><0x%x> igRestrictOn <%d> pcgRefl[0] = <%c>", pcgRefl,pcgRefl[0],igRestrictOn, pcgRefl[0] );
        if( igRestrictOn == TRUE && pcgRefl[0] == 0x31 )
        {
            memset( pclTmpStr, 0, sizeof(pclTmpStr) );
			tlTmpTime = prgWrk[i].lrStartInSeconds;
            TimeToStr (pclTmpStr,tlTmpTime);
            dbg( DEBUG, "%s: RESTRICTED CHECK Step Time = <%s>", pclFunc, pclTmpStr );
            if( tlRestrictFrom > 0 && prgWrk[i].lrStartInSeconds < tlRestrictFrom )
            {
                ilNoOfEmpl = 0;
                dbg(TRACE, "%s: Out of Restricted zone (from) <%ld> < <%d> ->  ilNoOfEmpl is set to 0",
                           pclFunc, prgWrk[i].lrStartInSeconds, tlRestrictFrom );
            }
            if( tlRestrictTo > 0 && prgWrk[i].lrStartInSeconds >= tlRestrictTo )
            {
                ilNoOfEmpl = 0;
                dbg(TRACE, "%s: Out of Restricted zone (to) <%ld> >= <%d> ->  ilNoOfEmpl is set to 0",
                           pclFunc, prgWrk[i].lrStartInSeconds, tlRestrictTo );
            }
        }
        /**************/

        sprintf(&cgMsgData[igPosInMsgData],"%3d",ilNoOfEmpl);
        igPosInMsgData += 3;
        if (igPosInMsgData > MSGBUFLEN)
        {
            dbg(TRACE,"Not enough place for all data in message");
            return RC_FAILURE;
        }

        cgMsgData[igPosInMsgData]='|';
        igPosInMsgData += 1;
        if (igPosInMsgData > MSGBUFLEN)
        {
            dbg(TRACE,"Not enough place for all data in message");
            return RC_FAILURE;
        }

        /*passengers who are not served in this interval*/
        flRest = prgWrk[i].frSumOfPAX + flStartRest - ilNoOfEmpl*flServedRate;
        if ( flRest < 0)
        {
            flRest = 0.0;
            flSumOfTT = 0.0;
        }
        else
        {
            if (prgWrk[i].frSumOfPAX != 0.0)
            {
                if (i < (ilNoOfRec-1))
                    prgWrk[i+1].frSumOfPAX += flRest;
                flSumOfTT = flRest * flSumOfTT / prgWrk[i].frSumOfPAX;
            }
            else
                flSumOfTT = 0.0;
        }
        dbg(DEBUG,"Sum of TT<%f>",flSumOfTT);
        if (flSumOfTT != 0.0)
            flStartRest = 0.0;
        dbg(TRACE,"Rest <%f>",flRest);
    }

    cgMsgData[igPosInMsgData-1] = '\0';
    dbg(DEBUG,"Message %s",cgMsgData);

    SendEventUCD();

    if ( pilTTBaggage )
        free (pilTTBaggage);
    if ( pilTTWoBaggage )
        free (pilTTWoBaggage);
    if (prgPAX)
        free(prgPAX);

    if (prgWrk)
        free(prgWrk);

    return ilRC;
}

static int StrToTime(char *pcpTime,time_t *plpTime)
{
    struct tm *_tm;
    time_t  now;
    char    _tmpc[6];

    dbg(0,"StrToTime, <%s>",pcpTime);
    if (strlen(pcpTime) < 12 )
    {
        /**plpTime = time(0L);*/
        return RC_FAIL;
    }

    now = time(0L);
    _tm = (struct tm *)gmtime(&now);

    _tmpc[2] = '\0';
    _tm -> tm_sec = 0;
    
    strncpy(_tmpc,pcpTime+10,2);
    _tm -> tm_min = atoi(_tmpc);
    strncpy(_tmpc,pcpTime+8,2);
    _tm -> tm_hour = atoi(_tmpc);
    strncpy(_tmpc,pcpTime+6,2);
    _tm -> tm_mday = atoi(_tmpc);
    strncpy(_tmpc,pcpTime+4,2);
    _tm -> tm_mon = atoi(_tmpc)-1;
    strncpy(_tmpc,pcpTime,4);
    _tmpc[4] = '\0';
    _tm ->tm_year = atoi(_tmpc)-1900;
    _tm ->tm_wday = 0;
    _tm ->tm_yday = 0;
    _tm->tm_isdst = -1; /* no adjustment of daylight saving time */
    now = mktime(_tm);
    dbg(0,"StrToTime: %02d.%02d.%04d %02d:%02d Now: %ld",_tm->tm_mday,
        _tm->tm_mon+1,_tm->tm_year+1900,_tm->tm_hour,_tm->tm_min,now);
    
    if (now != (time_t) -1)
    {
        /*{
            time_t utc,local;

            _tm = (struct tm *)gmtime(&now);
            utc = mktime(_tm);
            _tm = (struct tm *)localtime(&now);
            local = mktime(_tm);
            dbg(DEBUG,"utc %ld, local %ld, utc-diff %ld",
            utc,local,local-utc);
            llUtcDiff = local-utc;
        }
        now +=  + llUtcDiff;*/
        *plpTime = now;
        return RC_SUCCESS;
    }
    *plpTime = time(NULL);
    return RC_FAIL;
}

static int TimeToStr(char *pcpTime,time_t lpTime) 
{                                                                               
    struct tm *_tm;
    
    _tm = (struct tm *)localtime(&lpTime);
    strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",_tm);

    return (_tm->tm_wday == 0 ? 7 : _tm->tm_wday) + '0';
}

/*
 *     struct  tm {
 *             int     tm_sec;    seconds after the minute -- [0, 59]  
 *             int     tm_min;    minutes after the hour -- [0, 59] 
 *             int     tm_hour;   hour since midnight -- [0, 23] 
 *             int     tm_mday;   day of the month -- [1, 31] 
 *             int     tm_mon;    months since January -- [0, 11] 
 *             int     tm_year;   years since 1900 
 *             int     tm_wday;   days since Sunday -- [0, 6] 
 *             int     tm_yday;   days since January 1 -- [0, 365] 
 *             int     tm_isdst;  flag for daylight savings time 
 *             long    tm_tzadj;  seconds from GMT (east < 0) 
 *             char    tm_name[LTZNMAX];   name of timezone 
 *     };
 */




static int InitFieldIndex()
{
    int     ilRC = RC_SUCCESS;
    int     ilCol, ilPos, ilDBFields ;

    FindItemInList(UCD_FIELDS,"UAFT",',',&igUcdUaft,&ilCol,&ilPos);
    FindItemInList(UCD_FIELDS,"URUD",',',&igUcdUrud,&ilCol,&ilPos);
    ilDBFields = get_no_of_items(UCD_FIELDS);
    igUcdBegi = ilDBFields + 1;
    igUcdEnde = ilDBFields + 2;


    FindItemInList(SDI_FIELDS,"DATA",',',&igSdiData,&ilCol,&ilPos);
    FindItemInList(SDI_FIELDS,"DBAG",',',&igSdiDbag,&ilCol,&ilPos);
    FindItemInList(SDI_FIELDS,"PBEA",',',&igSdiPbea,&ilCol,&ilPos);
    FindItemInList(SDI_FIELDS,"PENA",',',&igSdiPena,&ilCol,&ilPos);
    FindItemInList(SDI_FIELDS,"STEP",',',&igSdiStep,&ilCol,&ilPos);
    FindItemInList(SDI_FIELDS,"UCCC",',',&igSdiUccc,&ilCol,&ilPos);
    FindItemInList(SDI_FIELDS,"UAFT",',',&igSdiUaft,&ilCol,&ilPos);
    FindItemInList(SDI_FIELDS,"UPRO",',',&igSdiUpro,&ilCol,&ilPos);

    FindItemInList(CCC_FIELDS,"CICC",',',&igCccCicc,&ilCol,&ilPos);
    FindItemInList(CCC_FIELDS,"URNO",',',&igCccUrno,&ilCol,&ilPos);

    FindItemInList(PXC_FIELDS,"TTWO",',',&igPxcTtwo,&ilCol,&ilPos);
    FindItemInList(PXC_FIELDS,"TTBG",',',&igPxcTtbg,&ilCol,&ilPos);

    FindItemInList(RUD_FIELDS,"FADD",',',&igRudFadd,&ilCol,&ilPos);
    FindItemInList(RUD_FIELDS,"VREF",',',&igRudVref,&ilCol,&ilPos);
    FindItemInList(RUD_FIELDS,"URUE",',',&igRudUrue,&ilCol,&ilPos);

    FindItemInList(DEM_FIELDS,"URNO",',',&igDemUrno,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS,"DEBE",',',&igDemDebe,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS,"DEEN",',',&igDemDeen,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS,"URUD",',',&igDemUrud,&ilCol,&ilPos);
    ilDBFields = get_no_of_items(DEM_FIELDS);
    igDemUaft = ilDBFields + 1;
    return ilRC;
}

int FindCccPosition( char *pcpUccc, int *pipPos )
{
    int i;

    dbg(DEBUG,"UCCC <%s>",pcpUccc);

    for ( i = 0; i < lgCccRow; i++)
    {
        if (strcmp(pcpUccc, prgCnf[i].crUccc) == 0)
        {
            *pipPos = i;
            return RC_SUCCESS;
        }
    }
    return RC_FAILURE;
}

static int SendEventUCD( )
{
    int     ilRC = RC_SUCCESS;
    BC_HEAD *prlBchead = NULL;
    CMDBLK  *prlCmdblk    = NULL;
    char    *pclData      = NULL;
    char    *pclSelection = NULL;
    char    *pclFields    = NULL;
    long    llActSize   = 0;

    llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 2
                + igPosInMsgData;

    if ( !prgOutEvent || ( llActSize > lgOutEventSize ) )
    {
        prgOutEvent = realloc(prgOutEvent,llActSize);

        if (prgOutEvent == NULL)
        {
            dbg(TRACE, "SendEventUCD: realloc out event <%d> bytes failed",
                llActSize);
            ilRC = RC_FAIL;
            lgOutEventSize = 0;
        } 
        else
        {
            dbg(DEBUG, "SendEventUCD: out event now <%d> bytes long", llActSize);
            lgOutEventSize = llActSize;
        } 
    } 

    if (ilRC == RC_SUCCESS)
    {
        memset( prgOutEvent, 0, lgOutEventSize );
        memcpy (prgOutEvent, prgEvent, sizeof(EVENT)) ;
        prgOutEvent->originator  = mod_id;
        prgOutEvent->retry_count = 0;
        prgOutEvent->data_length = llActSize - sizeof(EVENT);
        
        prlBchead = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
        memset( prlBchead, 0, sizeof(BC_HEAD) );
        prlBchead->rc = RC_SUCCESS;
        strcpy(prlBchead->dest_name, "DEMCCI" );
        strcpy(prlBchead->recv_name, "DEMHDL" );

        prlCmdblk    = (CMDBLK *)  ((char *) prlBchead->data) ;
        /* set CMDBLK members */
        strcpy(prlCmdblk->command, "UCD" );
        strcpy(prlCmdblk->obj_name, "RUD" );
        /* Selection is empty */    
        prlCmdblk->data[0] = '\0';
        /* fields are empty */  
        *(prlCmdblk->data+1) = '\0';
        /* data */  
        strcpy( prlCmdblk->data+2, cgMsgData);
        if ( strlen (prlCmdblk->tw_end) < 3 )   /* HOPO not yet in tw_end*/
            strcpy ( prlCmdblk->tw_end, cgHopo );
        sprintf( &(prlCmdblk->tw_end[3]), ",%s,DEMCCI", cgTabEnd );

        DebugPrintBchead (DEBUG, prlBchead) ;
        DebugPrintCmdblk (DEBUG, prlCmdblk) ;

        pclSelection = (char *)prlCmdblk->data;
        pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
        pclData = (char *)pclFields + strlen(pclFields) + 1;
        dbg (TRACE, "SendEventUCD: data      <%s>", pclData) ;              

        ilRC = que(QUE_PUT, igDest, mod_id, PRIORITY_4, llActSize,
                     (char *)prgOutEvent);

        if (ilRC != RC_SUCCESS)
        {
            HandleQueErr (ilRC) ;
            DebugPrintEvent (TRACE, prgOutEvent) ;
            DebugPrintBchead (TRACE, prlBchead) ;
            DebugPrintCmdblk (TRACE, prlCmdblk) ;
            dbg (TRACE, "selection <%s>", pclSelection) ;
            dbg (TRACE, "fields    <%s>", pclFields) ;
            dbg (TRACE, "data      <%s>", pclData) ;
        }
    }
    return ilRC;
}

static int myget_no_of_items(char *s)
{
    int rc = 0;

    if (strlen(s) && s)
    {
        /*rc = 1; comment, because sdi.data ends with a pipe*/
        while (*s)
        {
            if (*s++ == '|')
            {
                rc++;
            }
        }                       /* end while */
    }
    return rc;
}

static int HandleCDF(EVENT *pcpEvent, char *pcpFields, char *pcpData)
{
    int     ilRC = RC_SUCCESS;
    char    clDemUrud[URNOLEN+1];
    char    clDemDebe[DATELEN+1];
    char    clDemDeen[DATELEN+1];
    char    *pclAftData = 0;

    GetItemData("URUD",pcpFields,pcpData,clDemUrud);
    GetItemData("DEBE",pcpFields,pcpData,clDemDebe);
    GetItemData("DEEN",pcpFields,pcpData,clDemDeen);

    dbg(TRACE,"URUD = %s\tDEBE = %s\tDEEN = %s",clDemUrud,clDemDebe,clDemDeen);

    ilRC = GetCCIFlightUrnos ( clDemUrud,clDemDebe,clDemDeen, &pclAftData, '|' );
    if ( ilRC == RC_SUCCESS )
        ilRC = SendEventCDF(prgEvent, pclAftData);
    if ( pclAftData )
        free ( pclAftData );
    return ilRC;
}

static int GetItemData(char *pcpFieldName,char *pcpFields,char *pcpData,
                        char *pcpDest)
{
    int ilRc = RC_FAIL;
    int ilLc;
    int ilItemCount;
    char    pclFieldName[1200];

    ilItemCount = get_no_of_items(pcpFields);

    dbg(DEBUG,"GetItemData itemCount = %d",ilItemCount);    
    for(ilLc = 1; ilLc <= ilItemCount && ilRc != RC_SUCCESS; ilLc++)
    {
        get_real_item(pclFieldName,pcpFields,ilLc);
        if(!strcmp(pclFieldName,pcpFieldName))
        {
            get_real_item(pcpDest,pcpData,ilLc);
            dbg(DEBUG,"%05d: %s <%s>",ilLc,pclFieldName,pcpDest);
            ilRc = RC_SUCCESS;
        }
    }

    return ilRc;
}

static int SearchingForSDI(char *pcpDemUrud,char *pcpIvStart,char *pcpIvEnd)
{
    int     ilRC = RC_SUCCESS;
    char    *pclResult = 0;
    char    *pclResult2 = 0;
    char    clVref[URNOLEN+1];
    char    clUaft[URNOLEN+1];
    char    clPbea[DATELEN+1];
    char    clPena[DATELEN+1];
    char    clKey[HOPOLEN+1+URNOLEN+1];
    char    clKey2[HOPOLEN+1+URNOLEN+1+URNOLEN+1];
    long    llRow;
    long    llRow2;
    long    llSdiWrkRow = ARR_FIRST;
    long    llFieldNr = -1;
    char    clFadd[4], clRust[4];
    char    clUrue[URNOLEN+1];
    BOOL    blFinished = FALSE;

    ilRC = CEDAArrayDelete( &(rgSdiWrkArray.rrArrayHandle),
                             rgSdiWrkArray.crArrayName);

    if ( ilRC == RC_SUCCESS )
    {
        llRow = ARR_FIRST;

        if ( CEDAArrayFindRowPointer( &(rgRudArray.rrArrayHandle), 
                                        rgRudArray.crArrayName,
                                        &(rgRudArray.rrIdxUrnoHandle), 
                                        rgRudArray.crIdxUrnoName, pcpDemUrud, 
                                        &llRow, (void**)&pclResult ) != RC_SUCCESS )
        {
            /*  RudUrno no longer valid -> empty SdiWrkArray is ok */
            dbg ( TRACE, "SearchingForSDI: RUD <%s> no longer in DB or inactive -> calculating with 0 PAX", pcpDemUrud );
            blFinished = TRUE;
        }
        else
        {   
            strcpy ( clFadd, RUDFIELD(pclResult,igRudFadd) );
            strcpy ( clUrue, RUDFIELD(pclResult,igRudUrue) );
            dbg ( DEBUG, "SearchingForSDI: Found RUD <%s> URUE <%s> FADD <%s>", 
                  pcpDemUrud, clUrue, clFadd );
            if ( clFadd[0]=='0' )
            {   /* hag20031023:  PRF4827 */
                dbg ( TRACE, "SearchingForSDI: 'Disable Demand Creation' is selected for RUD <%s>", pcpDemUrud );
                blFinished = TRUE;
            }
            else
            {
                if ( ( GetFieldByUrno(&rgRueArray,clUrue,"RUST",clRust,2 ) == RC_SUCCESS ) 
                     && (clRust[0]!='1') )
                {   /* hag20031023:  PRF5052 */
                    dbg ( TRACE, "SearchingForSDI: Coll. Rule is not active URNO <%s> RUST <%s>", 
                          clUrue, clRust );
                    blFinished = TRUE;
                }
            }
        }
        if (  blFinished )
        {
            igMinc = 0;     /*  to prevent minimum demand creation for inactive RUDs */
            return RC_SUCCESS ;
        }
    }
    else 
    {
        dbg ( TRACE, "SearchingForSDI: CEDAArrayDelete for rgSdiWrkArray failed, RC <%d>", ilRC );
        return RC_FAIL ;
    }

    if ( pclResult )
        strcpy ( clVref, RUDFIELD( pclResult, igRudVref) );
    else
        ilRC = GetFieldByUrno(&rgRudArray,pcpDemUrud,"VREF",clVref,URNOLEN+1);

    dbg(DEBUG,"VREF: <%s>",&clVref[0]);
    if ( ilRC != RC_SUCCESS )
        dbg(TRACE,"SearchingForSDI: GetFieldByUrno failed Urno <%s> ilRC <%d>",
                      pcpDemUrud,ilRC);
    else
    {
        sprintf( clKey, "%s,%s", cgHopo, pcpDemUrud );
        dbg(DEBUG,"Key for UCD: <%s>",&clKey[0]);
        /*
        if (debug_level > TRACE)
        {
            ilRC = SaveIndexInfo ( &rgUcdArray, 2, 0 );
            ilRC = SaveIndexInfo ( &rgSdiArray, 2, 0 );
        }*/
        llRow = ARR_FIRST;

        /*looking for the corresponding UCD*/
        while(CEDAArrayFindRowPointer( &(rgUcdArray.rrArrayHandle),
                                        rgUcdArray.crArrayName,
                                        &(rgUcdArray.rrIdx01Handle), 
                                        rgUcdArray.crIdx01Name,
                                        clKey, &llRow,
                                        (void**)&pclResult) == RC_SUCCESS)
        {
            /*dbg(DEBUG,"UCD: <%s>",pclResult);*/

            ilRC = CEDAArrayGetField( &(rgUcdArray.rrArrayHandle), 
                                  rgUcdArray.crArrayName, &llFieldNr, "UAFT",
                                  URNOLEN+1, llRow, clUaft);
            dbg(DEBUG,"UAFT: <%s>",clUaft);
            if ( ilRC != RC_SUCCESS )
                dbg(TRACE,"SearchingForSDI: UAFT not filled, ilRC <%d>",ilRC);
            else
            {
                llRow2 = ARR_FIRST;
                sprintf(clKey2, "%s,%s,%s", cgHopo, clUaft, clVref);
                dbg(DEBUG,"Key for SDI: <%s>",clKey2);

            /*looking for SDI with CCC-URNO from RUD and AFT-URNO from UCD*/
                ilRC = CEDAArrayFindRowPointer( &(rgSdiArray.rrArrayHandle),
                                                rgSdiArray.crArrayName,
                                                &(rgSdiArray.rrIdx01Handle),
                                                rgSdiArray.crIdx01Name,
                                                clKey2, &llRow2,
                                                (void**)&pclResult2 ); 
                if ( ilRC != RC_SUCCESS )
                {
                    dbg(TRACE,
                        "SearchingForSDI: No Sdi with UAFT <%s> and UCCC <%s> found, ilRC <%d>",
                              clUaft,clVref,ilRC);
                    ilRC = RC_SUCCESS;
                }
                else 
                {
                    strncpy(clPbea,SDIWRKFIELD(pclResult2,igSdiPbea),DATELEN);
                    clPbea[DATELEN]='\0';
                    strncpy(clPena,SDIWRKFIELD(pclResult2,igSdiPena),DATELEN);
                    clPena[DATELEN]='\0';

                    dbg(DEBUG,"Period Begin: <%s> Period End: <%s>",&clPbea[0],&clPena[0]);
                    if (((strcmp( clPbea, (char *)pcpIvStart) >= 0) 
                            && (strcmp( (char *)pcpIvEnd, clPbea) > 0))
                        || ((strcmp( clPena, (char *)pcpIvStart) > 0) 
                            && (strcmp( (char *)pcpIvEnd, clPena) >= 0))
                        || ((strcmp(clPbea,(char *)pcpIvStart) < 0)
                            && (strcmp(clPena,(char *)pcpIvEnd) > 0)))
                    { 
                        ilRC = CEDAArrayAddRow(&rgSdiWrkArray.rrArrayHandle,
                                            rgSdiWrkArray.crArrayName,
                                            &llSdiWrkRow, (void *)pclResult2);
                        if ( ilRC != RC_SUCCESS )
                            dbg(TRACE,
                                "SearchingForSDI: Saving Sdi in Wrk-Array failed with ilRC <%d>",
                                ilRC );
                        else
                        {
                            llSdiWrkRow = ARR_NEXT;
                        }
                    }
                }
            }
            llRow = ARR_NEXT;
        }
        /* if (debug_level > TRACE)
            ilRC = SaveIndexInfo(&rgSdiWrkArray, 2, 0); */
    }
    return ilRC;
}

static int SendEventCDF(EVENT *prgEvent, char *pcpAftData )
{
    int     ilRC = RC_SUCCESS;
    BC_HEAD *prlBchead = NULL;
    CMDBLK  *prlCmdblk    = NULL;
    char    *pclData      = NULL;
    char    *pclSelection = NULL;
    char    *pclFields    = NULL;
    long    llActSize   = 0;

    llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 2
                + strlen(pcpAftData);

    if ( !prgOutEvent || ( llActSize > lgOutEventSize ) )
    {
        prgOutEvent = realloc(prgOutEvent,llActSize);

        if (prgOutEvent == NULL)
        {
            dbg(TRACE, "SendEventCDF: realloc out event <%d> bytes failed",
                llActSize);
            ilRC = RC_FAIL;
            lgOutEventSize = 0;
        } 
        else
        {
            dbg(DEBUG, "SendEventUCD: out event now <%d> bytes long", llActSize);
            lgOutEventSize = llActSize;
        } 
    } 

    if (ilRC == RC_SUCCESS)
    {
        memset( prgOutEvent, 0, lgOutEventSize );
        memcpy (prgOutEvent, prgEvent, sizeof(EVENT)) ;
        prgOutEvent->originator  = mod_id;
        prgOutEvent->retry_count = 0;
        prgOutEvent->data_length = llActSize - sizeof(EVENT);
        
        prlBchead = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
        memset( prlBchead, 0, sizeof(BC_HEAD) );
        prlBchead->rc = RC_SUCCESS;
        /*strcpy(prlBchead->dest_name, "DEMCCI" );
        strcpy(prlBchead->recv_name, "YYYHDL" );*/

        prlCmdblk    = (CMDBLK *)  ((char *) prlBchead->data) ;
        /* set CMDBLK members */
        strcpy(prlCmdblk->command, "CDF" );
        strcpy(prlCmdblk->obj_name, "AFT" );
        /* Selection is empty */    
        prlCmdblk->data[0] = '\0';
        /* fields are empty */  
        *(prlCmdblk->data+1) = '\0';
        /* data */  
        strcpy( prlCmdblk->data+2, pcpAftData);
        if ( strlen (prlCmdblk->tw_end) < 3 )   /* HOPO not yet in tw_end*/
            strcpy ( prlCmdblk->tw_end, cgHopo );
        sprintf( &(prlCmdblk->tw_end[3]), ",%s,DEMCCI", cgTabEnd );

        DebugPrintBchead (TRACE, prlBchead) ;
        DebugPrintCmdblk (TRACE, prlCmdblk) ;

        pclSelection = (char *)prlCmdblk->data;
        pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
        pclData = (char *)pclFields + strlen(pclFields) + 1;
        dbg (TRACE, "SendEventCDF: data      <%s>", pclData) ;              

        ilRC = que(QUE_PUT, prgEvent->originator, mod_id, PRIORITY_4, llActSize,
                     (char *)prgOutEvent);

        if (ilRC != RC_SUCCESS)
        {
            HandleQueErr (ilRC) ;
            DebugPrintEvent (TRACE, prgOutEvent) ;
            DebugPrintBchead (TRACE, prlBchead) ;
            DebugPrintCmdblk (TRACE, prlCmdblk) ;
            dbg (TRACE, "selection <%s>", pclSelection) ;
            dbg (TRACE, "fields    <%s>", pclFields) ;
            dbg (TRACE, "data      <%s>", pclData) ;
        }
    }
    return ilRC;
}

static int ForwardEvent( int ipDest )
{
    int         ilRC        = RC_SUCCESS;  /* Return code */
    BC_HEAD     *prlBchead    = NULL;
    CMDBLK      *prlCmdblk    = NULL;
    char        *pclSelection = NULL;
    char        *pclFields    = NULL;
    char        *pclData      = NULL;
    long        llActSize   = 0;

    dbg(DEBUG, "ForwardEvent: dest <%d>", ipDest);

    llActSize = prgEvent->data_length + sizeof(EVENT) + 2;

    if (llActSize > lgOutEventSize)
    {
        lgOutEventSize = 2 * llActSize;

        prgOutEvent = realloc(prgOutEvent, lgOutEventSize);
        if (prgOutEvent == NULL)
        {
            dbg(TRACE, "ForwardEvent: realloc out event <%d> bytes failed",
                lgOutEventSize);
            ilRC = RC_FAIL;
        }
    }

    if (ilRC == RC_SUCCESS)
    {
        memcpy(prgOutEvent, prgEvent, prgEvent->data_length + sizeof(EVENT));

        prlBchead = (BC_HEAD *)((char *)prgOutEvent + sizeof(EVENT));
        prlCmdblk = (CMDBLK *)((char *)prlBchead->data);
        pclSelection = prlCmdblk->data;
    
        pclFields = pclSelection + strlen(pclSelection) + 1;
        pclData  = pclFields + strlen(pclFields) + 1;

        DebugPrintBchead(DEBUG,prlBchead);
        DebugPrintCmdblk(DEBUG,prlCmdblk);
        dbg(DEBUG, "ForwardEvent: selection <%s>", pclSelection);         
        dbg(DEBUG, "ForwardEvent: fields    <%s>", pclFields);            
        dbg(DEBUG, "ForwardEvent: data      <%s>", pclData);              
            
        ilRC = que(QUE_PUT, ipDest, mod_id, PRIORITY_4, llActSize,
                    (char *)prgOutEvent);
        if (ilRC != RC_SUCCESS)
        {
            HandleQueErr (ilRC) ;
            DebugPrintEvent(TRACE, prgOutEvent);
            DebugPrintBchead(TRACE, prlBchead);
            DebugPrintCmdblk(TRACE, prlCmdblk);
            dbg(TRACE, "selection <%s>", pclSelection);
            dbg(TRACE, "fields    <%s>", pclFields);
            dbg(TRACE, "data      <%s>", pclData);
        }
    }
    return ilRC;
}

static int LocalToUtc(char *pcpTime)
{
    int ilRc = RC_SUCCESS;
    char *pclTdi;
    char clLocal[32];

    strcpy(clLocal,pcpTime);
    pclTdi = (strcmp(pcpTime,cgTich) < 0) ? cgTdi1 : cgTdi2;

    AddSecondsToCEDATime(pcpTime,(time_t)0-atol(pclTdi),1);
    dbg(TRACE,"2UTC  : <%s> -> <%s> ", clLocal, pcpTime);

    return ilRc;
}

static int GetDebugLevel(char *pcpMode, int *pipMode)
{
    int     ilRC = RC_SUCCESS;
    char    clCfgValue[64];

    ilRC = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,
                            clCfgValue);

    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
    }
    else
    {
        dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
        if (!strcmp(clCfgValue, "DEBUG"))
            *pipMode = DEBUG;
        else if (!strcmp(clCfgValue, "TRACE"))
            *pipMode = TRACE;
        else
            *pipMode = 0;
    }

    return ilRC;
}

static int InitializeCNFARRAY ()
{
    int     i = 0;
    long    llRow;
    char    *pclResult = 0;
    int     ilRC = RC_SUCCESS;
    char    clCicc[CICCLEN+1];
    
    llRow = ARR_FIRST;

    if ( prgCnf )
        free ( prgCnf );
    prgCnf = 0;

    CEDAArrayGetRowCount(&(rgCccArray.rrArrayHandle),
                            rgCccArray.crArrayName,&lgCccRow);
    
    if (lgCccRow > 0)
    {
        prgCnf = (CNFARRAY*)malloc( lgCccRow * sizeof(CNFARRAY) ) ; 
    }

    if ( !prgCnf )
    {
        dbg( TRACE,"InitializeCNFARRAY: not enough memory for internal CCCTAB");
        return RC_FAIL;
    }
    else
    {
        memset( prgCnf, 0, lgCccRow * sizeof(CNFARRAY) );
    }

    while(CEDAArrayFindRowPointer(&(rgCccArray.rrArrayHandle),
                                    rgCccArray.crArrayName,
                                    &(rgCccArray.rrIdx01Handle), 
                                    rgCccArray.crIdx01Name, "", &llRow,
                                    (void**)&pclResult) == RC_SUCCESS)
    {
        strcpy(prgCnf[i].crUccc, CCCFIELD(pclResult, igCccUrno));
        strcpy(clCicc, CCCFIELD(pclResult, igCccCicc));
        TrimRight(clCicc);
        dbg(TRACE, "CICC: <%s>", clCicc);

        ilRC = iGetConfigEntry(cgConfigFile, "QUEUEQUALITY", clCicc,
                                CFG_INT, (char *)&prgCnf[i].irQuality);
        if (prgCnf[i].irQuality == 0)
        {
            dbg(TRACE,
                "No valid Quality-Value for Class <%s>, default will be used for calculating",
                clCicc);
            prgCnf[i].irQuality = QUEUEQUALITYDEFAULT;
        }
        dbg(TRACE,"Quality of Queue: <%d>",prgCnf[i].irQuality);

        ilRC = iGetConfigEntry(cgConfigFile, "TATIME_WITH_LUGGAGE", clCicc,
                                CFG_INT,
                                (char *)&prgCnf[i].irTTWithBaggage);

        if (prgCnf[i].irTTWithBaggage == 0)
        {
            dbg(TRACE,
                "No valid TA time for PAX with Baggage for Class <%s>, default will be used for calculating",
                clCicc);
            prgCnf[i].irTTWithBaggage = TAWITHBAGDEFAULT;
        }
        dbg(TRACE,"TT with Baggage : <%d>", prgCnf[i].irTTWithBaggage);

        ilRC = iGetConfigEntry(cgConfigFile,"TATIME_WITHOUT_LUGGAGE",clCicc,
                                CFG_INT,
                                (char *)&prgCnf[i].irTTWithoutBaggage);

        if (prgCnf[i].irTTWithoutBaggage == 0)
        {
            dbg(TRACE,
                "No valid TA time for PAX without Baggage for Class <%s>, default will be used for calculating",
                clCicc);
            prgCnf[i].irTTWithoutBaggage = TAWITHOUTBAGDEFAULT;
        }
        dbg(TRACE, "TT without Baggage : <%d>",
            prgCnf[i].irTTWithoutBaggage);

        llRow = ARR_NEXT;
        i++;
    }
    return RC_SUCCESS;
}

/*  Get Transaction time for given Profile (pcpUpro) and counter class (pcpUccc)
    Kind of transaction time is given by ipFieldIdx (igPxcTtwo or igPxcTtbg)
    return RC_SUCCESS, if record found and value is filled with an integer  */
static int GetTransactionTime ( char *pcpUpro, char *pcpUccc, int ipFieldIdx,
                                int *pipValue )
{
    char clKey[31];
    char clValue[5];
    int  ilRC;
    long llRow=ARR_FIRST;
    char *pclResult;


    if (rgPxcArray.rrArrayHandle < 0 )
        return RC_FAIL;     /* array is not initialized */
    if ( !pcpUpro || !pcpUccc || !pipValue )
    {
        dbg ( TRACE, "GetTransactionTime: At least one parameter is null" );
        return RC_FAIL; 
    }
    sprintf ( clKey, "%s,%s", pcpUpro, pcpUccc );
    ilRC = CEDAArrayFindRowPointer(&(rgPxcArray.rrArrayHandle),
                                    rgPxcArray.crArrayName,
                                    &(rgPxcArray.rrIdx01Handle), 
                                    rgPxcArray.crIdx01Name, clKey, &llRow,
                                    (void**)&pclResult );
    dbg ( DEBUG, "GetTransactionTime: CEDAArrayFindRowPointer Key <%s> llRow <%ld> RC <%d>", 
                  clKey, llRow, ilRC );  
    if ( ilRC == RC_SUCCESS )
    {
        strncpy(clValue, pclResult + rgPxcArray.plrArrayFieldOfs[ipFieldIdx-1], 4);
        clValue[4] = '\0';
        if ( ( sscanf ( clValue, "%d", pipValue ) > 0 ) &&
             (*pipValue>=0) )
        {   /*  value ok */
            dbg(DEBUG,"GetTransactionTime: Found Transaction time Idx <%d> Value <%d> ",
                ipFieldIdx, *pipValue );
        }
        else
        {
            dbg(TRACE, "GetTransactionTime: Invalid Transaction time Idx <%d> Value <%s> ",
                ipFieldIdx, clValue );
            ilRC = RC_FAIL;
        }
    }
    return ilRC;
}

static int GetCCIFlightUrnos ( char *pcpUrud, char *pcpDebe, char *pcpDeen, char **pcpUaft, char cpDel )
{
    int ilRC;
    char    clDelStr[2];
    long    llNoOfFlights = 0;
    char    *pclResult = 0;
    long    llRow = ARR_FIRST;
    BOOL    blBegin = TRUE;

    clDelStr[0] = cpDel;
    clDelStr[1] = '\0';

    ilRC = SearchingForSDI(pcpUrud, pcpDebe, pcpDeen);

    ilRC = CEDAArrayGetRowCount(&(rgSdiWrkArray.rrArrayHandle),
                                rgSdiWrkArray.crArrayName, &llNoOfFlights);
    if (ilRC != RC_SUCCESS)
    {
        dbg( TRACE,"GetCCIFlightUrnos: cannot read internal sditab");
        return ilRC;
    }
    dbg(TRACE,"GetCCIFlightUrnos: Number of flightrecords <%ld>",llNoOfFlights);

    if (llNoOfFlights > 0)
    {
        *pcpUaft = malloc(llNoOfFlights * (URNOLEN+1)); 

        if ( !(*pcpUaft) )
        {
            dbg( TRACE,"GetCCIFlightUrnos: not enough memory for afts");
            return RC_FAIL;
        }
        else
        {
            memset( *pcpUaft, 0, llNoOfFlights * (URNOLEN+1) );
        }
    }
    else
    {
        dbg(TRACE,"GetCCIFlightUrnos: No corresponding data found");
        *pcpUaft = malloc(URNOLEN+1);   
        if ( *pcpUaft )
            memset( *pcpUaft, 0, URNOLEN+1 );
    }

    if ( *pcpUaft )
    {
        while (CEDAArrayGetRowPointer( &(rgSdiWrkArray.rrArrayHandle),
                                        rgSdiWrkArray.crArrayName, llRow,
                                        (void**)&pclResult) == RC_SUCCESS) 
        {
            if (blBegin == TRUE)
                blBegin = FALSE;
            else
                strcat(*pcpUaft,clDelStr);
            strcat(*pcpUaft,SDIWRKFIELD(pclResult, igSdiUaft));
            llRow = ARR_NEXT;
        }
        dbg(TRACE,"GetCCIFlightUrnos: AftData %s RC <%d>", *pcpUaft, ilRC );
    }
    return ilRC;
}

static int HandleFLC(EVENT *pcpEvent, char *pcpData)
{
    int     i, ilLen, ilRC1, ilUaftLen, ilRC = RC_SUCCESS, ilBufLen=0;
    char    *pclUrud, *pclDebe, *pclDeen, *pclUrno, *pclDemRow;
    char    clUdem[21], clTmp[31];
    char    *pclAftData = 0;
    char    *pclSelection = 0;
    int     ilDemUrnoCnt = get_no_of_items(pcpData);
    long    llDemRow = ARR_FIRST, llNoOfFlights;
    long    llFieldIdx = -1, llBuffLen = 2;
    char    clMinDebe[DATELEN+1]="9";
    char    clMaxDeen[DATELEN+1]="";

    dbg ( DEBUG, "HandleFLC: Data <%s>", pcpData );
    ilLen = ilDemUrnoCnt*(URNOLEN+3) + 21;
    dbg ( DEBUG, "HandleFLC: Count of Demands <%d> Bufferlen <%d>", ilDemUrnoCnt, ilLen );
    
    if ( ilDemUrnoCnt > 0 )
    {
        pclSelection = malloc ( ilLen );
        if ( pclSelection )
        {
            strcpy ( pclSelection, "WHERE URNO IN (" );
            for ( i=1; i<=ilDemUrnoCnt; i++ )
            {
                if ( get_real_item ( clUdem, pcpData, i ) > 0 )
                {
                    if ( i>1 )
                        sprintf ( clTmp, ",'%s'", clUdem ); 
                    else
                        sprintf ( clTmp, "'%s'", clUdem ); 
                    strcat ( pclSelection, clTmp );
                }
            }
            strcat ( pclSelection, ")" );
            ilRC = CEDAArrayRefill(&rgDemArray.rrArrayHandle,rgDemArray.crArrayName,
                                    pclSelection, 0, ARR_FIRST );  
            dbg ( TRACE, "HandleFLC: CEDAArrayRefill RC<%d>", ilRC );
        }
        else
        {
            dbg ( TRACE, "HandleFLC: malloc failed " );
            ilRC = RC_NOMEM;
        }
        if ( ilRC == RC_SUCCESS )
        {
            llDemRow = ARR_FIRST;
            while ( CEDAArrayGetRowPointer(&(rgDemArray.rrArrayHandle),
                                           rgDemArray.crArrayName,llDemRow,
                                           (void *)&pclDemRow) == RC_SUCCESS )
            {
                llDemRow = ARR_NEXT;
                pclDebe = DEMFIELD(pclDemRow,igDemDebe);
                pclDeen = DEMFIELD(pclDemRow,igDemDeen);
                if ( strcmp ( pclDebe, clMinDebe ) < 0 )
                    strcpy ( clMinDebe, pclDebe );
                if ( strcmp ( clMaxDeen, pclDeen ) < 0 )
                    strcpy ( clMaxDeen, pclDeen );
            }
            dbg ( TRACE, "HandleFLC: Demand range <%s> to <%s>", clMinDebe, clMaxDeen );
            ilRC1 = IniUcdWorkArray ( clMinDebe, clMaxDeen );
            dbg ( DEBUG, "HandleFLC: IniUcdWorkArray  RC <%d>", ilRC1 );
        }
        if ( ilRC == RC_SUCCESS )
        {
            ilRC = CEDAArrayGetRowCount(&(rgUcdWrkArray.rrArrayHandle),
                                          rgUcdWrkArray.crArrayName, &llNoOfFlights);
            if ( ilRC == RC_SUCCESS )
            {
                if ( llNoOfFlights < 1 )
                    llNoOfFlights = 1;
                ilBufLen = llNoOfFlights * (URNOLEN+1);
                pclAftData = malloc(ilBufLen);  
                if ( !pclAftData )
                {
                    dbg ( TRACE, "HandleFLC: Allocation of <%ld> bytes failed", ilBufLen );
                    ilRC = RC_NOMEM;
                }
            }
        }
        if ( ilRC == RC_SUCCESS )
        {
            llDemRow = ARR_FIRST;
            ilUaftLen = rgDemArray.plrArrayFieldLen[igDemUaft-1];
            dbg ( DEBUG, "HandleFLC:  Length of UAFT <%d>", ilUaftLen );
            while ( CEDAArrayGetRowPointer(&(rgDemArray.rrArrayHandle),
                                           rgDemArray.crArrayName,llDemRow,
                                           (void *)&pclDemRow) == RC_SUCCESS )
            {
                llDemRow = ARR_NEXT;

                pclUrud = DEMFIELD(pclDemRow,igDemUrud);
                pclDebe = DEMFIELD(pclDemRow,igDemDebe);
                pclDeen = DEMFIELD(pclDemRow,igDemDeen);
                pclUrno = DEMFIELD(pclDemRow,igDemUrno);

                memset( pclAftData, 0, ilBufLen );
                ilRC1 = GetCCIFlightUrnos2 ( pclUrud, pclDebe, pclDeen, pclAftData, ',' );
                if ( ilRC1 != RC_SUCCESS )
                    dbg ( TRACE, "HandleFLC: GetCCIFlightUrnos failed RC <%d>", ilRC1 );
                else
                {
                    ilLen = strlen ( pclAftData ) ;
                    if ( ilLen > ilUaftLen )
                    {
                        dbg ( TRACE, "HandleFLC: List of flights for UDEM <%s> too long <%d>",
                              pclUrno, ilLen );
                        ilLen = ilUaftLen;
                        pclAftData[ilLen] = '\0';
                    }         
                    llBuffLen += URNOLEN + 3 + ilLen;
                    dbg ( DEBUG, "HandleFLC: UAFT-List <%s> for UDEM <%s>", pclAftData, pclUrno );
                    ilRC1 = CEDAArrayPutField ( &(rgDemArray.rrArrayHandle),
                                                rgDemArray.crArrayName,&llFieldIdx,
                                                "UAFT", ARR_CURRENT, pclAftData) ;
                    if ( ilRC1 != RC_SUCCESS )
                    {
                        dbg ( TRACE, "HandleFLC: CEDAArrayPutField FldIdx <%ld> failed RC <%d>",
                              llFieldIdx, ilRC );
                        ilRC = ilRC1;
                    }
                }
            }
            if ( ilRC == RC_SUCCESS )
            {
                if ( debug_level > TRACE )
                    SaveIndexInfo ( &rgDemArray, 0, 0 );
                ilRC = SendEventFLC ( pcpEvent, llBuffLen );
                if ( ilRC != RC_SUCCESS )
                    dbg ( TRACE, "HandleFLC: SendEventFLC failed RC <%d>", ilRC );
            }
        }
    }
    if ( pclAftData )
        free ( pclAftData );
    if ( pclSelection )
        free ( pclSelection );
    dbg ( TRACE, "HandleFLC: RC <%d>", ilRC );
    return ilRC;
}

/*  Fill rgUcdWrkArray with all rows from rgUcdArray and initialise BEGI and ENDE */
static int IniUcdWorkArray ( char *pcpFrom, char *pcpTo )
{
    long llRowUcd = ARR_FIRST, llRowRud, llRowSdi, llUcdWrkRow = ARR_FIRST ;
    char *pclUcdRow, *pclRudRow, *pclSdiRow;
    char *pclUaft, *pclFadd ,*pclUrue, *pclUrud, *pclVref, *pclPbea, *pclPena;
    char clKey2[HOPOLEN+1+URNOLEN+1+URNOLEN+1], clRust[4];
    long llBegiIdx=-1, llEndeIdx=-1;
    int  ilRC, ilRC1;

    ilRC = CEDAArrayDelete( &(rgUcdWrkArray.rrArrayHandle),
                             rgUcdWrkArray.crArrayName );
 
    while ( CEDAArrayGetRowPointer( &(rgUcdArray.rrArrayHandle),
                                    rgUcdArray.crArrayName,
                                    llRowUcd, (void**)&pclUcdRow ) == RC_SUCCESS)
    {
        llRowUcd = ARR_NEXT;

        pclUaft = UCDFIELD(pclUcdRow,igUcdUaft);
        pclUrud = UCDFIELD(pclUcdRow,igUcdUrud);

        llRowRud = ARR_FIRST;
        ilRC1 = CEDAArrayFindRowPointer( &(rgRudArray.rrArrayHandle), 
                                         rgRudArray.crArrayName,
                                         &(rgRudArray.rrIdxUrnoHandle), 
                                         rgRudArray.crIdxUrnoName, pclUrud, 
                                         &llRowRud, (void**)&pclRudRow );
        if ( ilRC1 != RC_SUCCESS )
        {
            /*  RudUrno no longer valid -> empty SdiWrkArray is ok */
            dbg ( TRACE, "IniUcdWorkArray: URNO <%s> not found in rgRudArray", pclUrud );
        }
        else
        {   
            pclFadd = RUDFIELD(pclRudRow,igRudFadd) ;
            pclUrue = RUDFIELD(pclRudRow,igRudUrue) ;
            pclVref = RUDFIELD( pclRudRow, igRudVref);
            dbg ( DEBUG, "IniUcdWorkArray: Found RUD <%s> URUE <%s> FADD <%s>", 
                  pclUrud, pclUrue, pclFadd );
            if ( *pclFadd == '0' )
            {   
                dbg ( TRACE, "IniUcdWorkArray: 'Disable Demand Creation' is selected for RUD <%s>", pclUrud );
                ilRC1 = RC_IGNORE;
            }
        }
        if ( ilRC1 == RC_SUCCESS )
        {
            if ( ( GetFieldByUrno(&rgRueArray,pclUrue,"RUST",clRust,2 ) == RC_SUCCESS ) 
                     && (clRust[0]!='1') )
            {   
                dbg ( TRACE, "IniUcdWorkArray: Coll. Rule is not active URNO <%s> RUST <%s>", 
                      pclUrue, clRust );
                ilRC1 = RC_IGNORE;
            }
        }
        if ( ilRC1 == RC_SUCCESS )
        {
            llRowSdi = ARR_FIRST;
            sprintf(clKey2, "%s,%s,%s", cgHopo, pclUaft, pclVref);
            dbg(DEBUG,"Key for SDI: <%s>",clKey2);
            /*looking for SDI with CCC-URNO from RUD and AFT-URNO from UCD*/
            ilRC1 = CEDAArrayFindRowPointer( &(rgSdiArray.rrArrayHandle),
                                             rgSdiArray.crArrayName,
                                             &(rgSdiArray.rrIdx01Handle),
                                             rgSdiArray.crIdx01Name,
                                             clKey2, &llRowSdi,
                                             (void**)&pclSdiRow ); 
            if ( ilRC1 != RC_SUCCESS )
            {
                dbg(TRACE, "IniUcdWorkArray: No Sdi with UAFT <%s> and UCCC <%s> found, ilRC <%d>",
                           pclUaft, pclVref, ilRC1);
            }
        }
        if ( ilRC1 == RC_SUCCESS )
        {
            pclPbea = SDIWRKFIELD(pclSdiRow,igSdiPbea);
            pclPena = SDIWRKFIELD(pclSdiRow,igSdiPena);

            dbg(DEBUG,"IniUcdWorkArray: Period Begin: <%s> End <%s>", pclPbea, pclPena );
            if ( ( strcmp(pclPbea,pcpTo) < 0 ) && 
                 ( strcmp(pclPena,pcpFrom) > 0 ) )
            { 
                ilRC1 = CEDAArrayAddRow(&rgUcdWrkArray.rrArrayHandle,
                                        rgUcdWrkArray.crArrayName,
                                        &llUcdWrkRow, (void *)pclUcdRow );
                if ( ilRC1 != RC_SUCCESS )
                {
                    dbg(TRACE, "IniUcdWorkArray: Saving Ucd in Wrk-Array failed RC <%d>", ilRC1 );
                    ilRC = ilRC1;
                }
                else
                {
                    ilRC1 = CEDAArrayPutField ( &(rgUcdWrkArray.rrArrayHandle),
                                                rgUcdWrkArray.crArrayName,&llBegiIdx,
                                                "BEGI", llUcdWrkRow, pclPbea ) ;
                    ilRC1 |= CEDAArrayPutField ( &(rgUcdWrkArray.rrArrayHandle),
                                                 rgUcdWrkArray.crArrayName,&llEndeIdx,
                                                 "ENDE", llUcdWrkRow, pclPena ) ;
                    if ( ilRC1 != RC_SUCCESS )
                    {
                        dbg(TRACE, "IniUcdWorkArray: CEDAArrayPutField Row <%ld> failed RC <%d>", 
                            llUcdWrkRow, ilRC1 );
                        ilRC = ilRC1;
                    }
                }
                llUcdWrkRow = ARR_NEXT;
            }
        }
    }
    return ilRC;
}


static int GetCCIFlightUrnos2 ( char *pcpUrud, char *pcpDebe, char *pcpDeen, char *pcpUaft, char cpDel )
{
    int ilRC = RC_SUCCESS;
    char    clDelStr[2], *pclBegi, *pclEnde;
    char    *pclResult = 0;
    long    llRow = ARR_FIRST;
    BOOL    blBegin = TRUE;

    clDelStr[0] = cpDel;
    clDelStr[1] = '\0';

    while (CEDAArrayFindRowPointer( &(rgUcdWrkArray.rrArrayHandle),
                                    rgUcdWrkArray.crArrayName, 
                                    &(rgUcdWrkArray.rrIdx01Handle), 
                                    rgUcdWrkArray.crIdx01Name, pcpUrud, 
                                    &llRow, (void**)&pclResult) == RC_SUCCESS) 
    {
        llRow = ARR_NEXT;
        pclBegi = UCDFIELD(pclResult,igUcdBegi);
        pclEnde = UCDFIELD(pclResult,igUcdEnde);
        if ( ( strcmp ( pclBegi,pcpDeen) < 0 ) &&
             ( strcmp ( pcpDebe,pclEnde) < 0 ) )
        {
            if (blBegin == TRUE)
                blBegin = FALSE;
            else
                strcat(pcpUaft,clDelStr);
            strcat(pcpUaft,UCDFIELD(pclResult, igUcdUaft));
        }
    }
    dbg(TRACE,"GetCCIFlightUrnos2: AftData <%s>",  pcpUaft );
    return ilRC;
}


static int SendEventFLC(EVENT *prpEvent, long lpBuffLen )
{
    int     ilRC = RC_SUCCESS;
    BC_HEAD *prlBchead = NULL;
    CMDBLK  *prlCmdblk    = NULL;
    char    *pclData      = NULL;
    char    *pclTmp      = NULL;
    char    *pclSelection = NULL;
    char    *pclFields    = NULL;
    long    llActSize   = 0, llRow;
    BOOL    blFirst = TRUE;
    char    *pclDemRow, *pclUaft, *pclUrno;

    /*  length of one field DEMTAB.UAFT must be added, because this field is not trimmed */
    llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 2 + lpBuffLen + rgDemArray.plrArrayFieldLen[igDemUaft-1];

    if ( !prgOutEvent || ( llActSize > lgOutEventSize ) )
    {
        prgOutEvent = realloc(prgOutEvent,llActSize);

        if (prgOutEvent == NULL)
        {
            dbg(TRACE, "SendEventFLC: realloc out event <%d> bytes failed",
                llActSize);
            ilRC = RC_FAIL;
            lgOutEventSize = 0;
        } 
        else
        {
            dbg(DEBUG, "SendEventUCD: out event now <%d> bytes long", llActSize);
            lgOutEventSize = llActSize;
        } 
    } 

    if (ilRC == RC_SUCCESS)
    {
        memset( prgOutEvent, 0, lgOutEventSize );
        memcpy (prgOutEvent, prpEvent, sizeof(EVENT)) ;
        prgOutEvent->originator  = mod_id;
        prgOutEvent->retry_count = 0;
        prgOutEvent->data_length = llActSize - sizeof(EVENT);
        
        prlBchead = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
        memset( prlBchead, 0, sizeof(BC_HEAD) );
        prlBchead->rc = RC_SUCCESS;
        /*strcpy(prlBchead->dest_name, "DEMCCI" );
        strcpy(prlBchead->recv_name, "YYYHDL" );*/

        prlCmdblk    = (CMDBLK *)  ((char *) prlBchead->data) ;
        /* set CMDBLK members */
        strcpy(prlCmdblk->command, "FLC" );
        strcpy(prlCmdblk->obj_name, "DEMTAB" );
        /* Selection is empty */    
        prlCmdblk->data[0] = '\0';
        /* fields are empty */  
        *(prlCmdblk->data+1) = '\0';
        /* data */  
        if ( strlen (prlCmdblk->tw_end) < 3 )   /* HOPO not yet in tw_end*/
            strcpy ( prlCmdblk->tw_end, cgHopo );
        sprintf( &(prlCmdblk->tw_end[3]), ",%s,DEMCCI", cgTabEnd );

        DebugPrintBchead (TRACE, prlBchead) ;
        DebugPrintCmdblk (TRACE, prlCmdblk) ;

        pclSelection = (char *)prlCmdblk->data;
        pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
        pclData = (char *)pclFields + strlen(pclFields) + 1;
        pclTmp = pclData;

        llRow = ARR_FIRST;
        while ( CEDAArrayGetRowPointer(&(rgDemArray.rrArrayHandle),
                                       rgDemArray.crArrayName,llRow,
                                       (void *)&pclDemRow) == RC_SUCCESS )
        {
            pclUaft = DEMFIELD(pclDemRow,igDemUaft);
            pclUrno = DEMFIELD(pclDemRow,igDemUrno);

            dbg ( DEBUG, "SendEventFLC: UDEM <%s> UAFTs <%s>", pclUrno, pclUaft );
            if ( ( atol(pclUaft) > 0 ) && ( atol(pclUrno) > 0 ) )
            {
                if ( blFirst )
                {
                    sprintf ( pclTmp, "%s,%s", pclUrno, pclUaft );
                    blFirst = FALSE;
                }
                else
                    sprintf ( pclTmp, ";%s,%s", pclUrno, pclUaft );
                TrimRight ( pclTmp );
                pclTmp += strlen(pclTmp);
            }
            llRow = ARR_NEXT;
        }   
        dbg (DEBUG, "SendEventFLC: Selection <%s>", pclSelection) ;              
        dbg (DEBUG, "SendEventFLC: Fields    <%s>", pclFields) ;              
        dbg (DEBUG, "SendEventFLC: Data      <%s>", pclData) ;              
        dbg (DEBUG, "SendEventFLC: llActSize <%ld> lpBuffLen <%ld> length of Data <%d>" , 
                     llActSize, lpBuffLen, strlen(pclData) );
        dbg (DEBUG, "SendEventFLC: Data-Offset <%d> End-Offset <%d>", 
                     pclData-(char*)prgOutEvent, pclTmp-(char*)prgOutEvent );

        ilRC = que(QUE_PUT, prpEvent->originator, mod_id, PRIORITY_4, llActSize,
                     (char *)prgOutEvent);

        if (ilRC != RC_SUCCESS)
        {
            HandleQueErr (ilRC) ;
            DebugPrintEvent (TRACE, prgOutEvent) ;
            DebugPrintBchead (TRACE, prlBchead) ;
            DebugPrintCmdblk (TRACE, prlCmdblk) ;
            dbg (TRACE, "selection <%s>", pclSelection) ;
            dbg (TRACE, "fields    <%s>", pclFields) ;
            dbg (TRACE, "data      <%s>", pclData) ;
        }
    }
    return ilRC;
}

static int GetNextDate ( char *pcpStartUtc, int ipTimeLocal, char *pcpNextUtc )
{
    if ( !pcpStartUtc || !pcpNextUtc )
        return RC_INVALID;
    strcpy ( pcpNextUtc, pcpStartUtc );
    UtcToLocal ( pcpNextUtc );
    sprintf( &(pcpNextUtc[8]), "%04d00", ipTimeLocal );

    LocalToUtc(pcpNextUtc);
    if (strcmp(pcpNextUtc, pcpStartUtc) < 0)
        AddSecondsToCEDATime(pcpNextUtc, 86400, 1);
    dbg ( TRACE, "GetNextDate:  Date <%s> Time <%4d> -> Date <%s>",
          pcpStartUtc, ipTimeLocal, pcpNextUtc );
    return RC_SUCCESS;
}

static int UtcToLocal(char *pcpTime)
{
    int ilRc = RC_SUCCESS;
    char *pclTdi;
    char clUtc[32];

    strcpy(clUtc,pcpTime);
    pclTdi = (strcmp(pcpTime,cgTich) < 0) ? cgTdi1 : cgTdi2;

    AddSecondsToCEDATime(pcpTime,(time_t)0+atol(pclTdi),1);
    dbg(DEBUG,"2LOCAL  : <%s> -> <%s> ",clUtc, pcpTime);

    return ilRc;
}

/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
