#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Rms/discal.c 1.14 2011/02/17 10:45:35SGT ble Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         : hag                                                       */
/* Date           : 22.08.2000                                                */
/* Description    : Process to calculate single distribution of PAX flow to   */
/*                  CCI-counter for one flight                                */
/* Update history :                                                           */
/* MEI 20101022 : Problem in FindItemInList when comparing single char with whole string */
/* MEI 20101123 : Grouping/Ungrouping of rules are not informed to DISCAL when changes made in RegelWerk */
/* MEI 20110217 : CCI demand of 1 template when generating from Coverage flush off the other template */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
/*  static char sccs_version[] = "@(#) UFIS4.5 (c) ABB AAT/I discal.c 4.5.1.3 / 03/09/18 / HAG"; */
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <math.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h"
#include "AATArray.h"
#include "libccstr.h"
#include "fditools.h"
#include "syslib.h"
#include "cedatime.h"
#include "fileif.h"

extern int get_no_of_items(char *s);
int get_item_no(char *s,char *f,short elem_len) ;

/******************************************************************************/
/* Constants                                                                  */
/******************************************************************************/
#define ARR_NAME_LEN    (8)
#define FIELD_LEN       4   
#define ARR_FLDLST_LEN  (256)
#define SDI_DATA_LEN    1152
#define DATELEN         14                                              /*rna*/
#define URNOLEN         10                                              /*rna*/
#define HOPOLEN         3                                               /*rna*/
#define URNO_LIST_LEN   1000
#define EPS             0.0001
#define FORM_LEN        40
#define MAX_CALC_METH   5   /* max. no. supported methods to calc. PAX-counts */

/* flags for SetArrayInfo */
#define CEDAARRAY_ALL       0xFF
#define CEDAARRAY_FILL      0x01
#define CEDAARRAY_URNOIDX   0x02
#define CEDAARRAY_TRIGGER   0x04


/*                   0    1     2   3    4    5     6   7    8    9     10  11   12   13   14   15   16   17   18   19   20   21   22               */  
#define PRO_FIELDS "DCOP,FDCO,FISU,HOPO,NAME,PRIO,PRST,UARC,URNO,UTPL,EXCL"  
/*#define RUE_FIELDS "APPL,EVTY,RUSN,RUST,RUTY,URNO"*/
/*#define RUD_FIELDS "ALOC,DBAR,DEAR,DRTY,FADD,FFPD,HOPO,REFT,UCRU,UGHS,ULNK,URNO,URUE,VREF"*/
#define RUD_FIELDS "ALOC,FFPD,HOPO,UCRU,URNO,URUE,VREF,ULNK,UTPL"
#define VTP_FIELDS "HOPO,PBER,PENR,TABN,URNO,UVAL,VALU"
#define PXC_FIELDS "DBAG,DPAX,HOPO,UCCC,UPRO,UPXA,URNO"
#define SDI_FIELDS "CDAT,DATA,DBAG,HOPO,LSTU,PBEA,PENA,STEP,UAFT,UCCC,UPRO,URNO,USEC,USEU"
#define UCD_FIELDS "CDAT,HOPO,LSTU,UAFT,URNO,URUD,USEC,USEU"
#define CCC_FIELDS "CICC,HOPO,URNO"
/*#define PAX_FIELDS "AFTU,FDCS,FIPD,FKEY,HOPO,NOSC,NOSF,NOSY,PAXT,PBLC,PBLF,PBLY,PBTC,PBTY,PCLC,PCLF,PCLY,PCTC,PCTF,PCTY,PRDC,PRDF,PRDY,URNO"*/


/******************************************************************************/
/* Typedefs                                                                   */
/******************************************************************************/
typedef char CLASSCODES[64];    

struct _arrayinfo
{
  HANDLE   rrArrayHandle;
  char     crArrayName[ARR_NAME_LEN+1];
  char     crTableName[ARR_NAME_LEN+1];
  char     crArrayFieldList[ARR_FLDLST_LEN+1];
  long     lrArrayFieldCnt;
  char    *pcrArrayRowBuf;
  long     lrArrayRowLen;
  long    *plrArrayFieldOfs;
  long    *plrArrayFieldLen;
  HANDLE   rrIdx01Handle;
  char     crIdx01Name[ARR_NAME_LEN+1];
  char     crIdx01FieldList[ARR_FLDLST_LEN+1];
  long     lrIdx01FieldCnt;
  char    *pcrIdx01RowBuf;
  long     lrIdx01RowLen;
  HANDLE   rrIdxUrnoHandle;
  char     crIdxUrnoName[ARR_NAME_LEN+1];
};
typedef struct _arrayinfo ARRAYINFO;

struct _sdirec
{
    char    crDATA[SDI_DATA_LEN+1];
    char    crBAG[6];
    int     irStart;
    char    crSTEP[4];
    char    crPBEA[15];
};
typedef struct _sdirec SDIREC;

struct _paxcalcmethod
{
    char    crName[FORM_LEN+1];
    char    crTable[7];
    char    crFormT[FORM_LEN+1];    /* formula for PAX total calculation */
    char    **pcrClForms;   /* formulas for PAX calculation of all supported classes */
    char    crLinkField[5];     /* field of crTable that is linked to flight */
    char    crBaseTable[7];         /* base table, normally AFTTAB */
    char    crBaseField[5];         /* base field, e.g. REGN, ACT3, URNO  */
    short   srSeperate;
    short   srUseDCOP;
    long    lrValidity;     /* validity of method in minutes, back from stod */
    short   srEmptyIs0;             /* empty field means 0 PAX */
};
typedef struct _paxcalcmethod PAXCALCM;

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = TRACE;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];
static char  cgHopo[8];                         /* default home airport    */
static char     cgNewRecBuf[8192];
static long     lgUrnolen;                                              /*rna*/
static long     lgNoOfCC;                                               /*rna*/
static time_t   tgActDate;                                              /*rna*/

static char pcgRecvName[64]; /* BC-Head used as WKS-Name */
static char pcgDestName[64]; /* BC-Head used as USR-Name */
static char pcgTwStart[64];  /* BC-Head used as Stamp */
static char pcgTwEnd[64];  /* BC-Head used as Stamp */ 
static int      igStartUpMode = TRACE;
static int      igRuntimeMode = 0;

static char     cgTabEnd[64] = "";

static char     cgUCDChanges[URNO_LIST_LEN+1] = "";
static char     cgIvStart[DATELEN+1] = "";
static char     cgIvEnd[DATELEN+1] = "";

ARRAYINFO rgProArray;
ARRAYINFO rgRudArray;
/* ARRAYINFO rgRueArray; */
ARRAYINFO rgVtpArray;
ARRAYINFO rgPxcArray;
ARRAYINFO rgUcdArray;
ARRAYINFO rgSdiArray;
ARRAYINFO rgCccArray;
ARRAYINFO rgNewSdiArr;  /* array for newly calculated SDI records */

static CLASSCODES*  prgClassCodes;  /* Codes mapped to different classes e.g.:
                                    1st:'F','FC' 2nd:'C','CC' 3rd:'Y' 4th:'PCL'   */
static int igClassCount = 3;

static BOOL bgOpenUFRFLICOL = FALSE;                                    /*rna*/

int igExtendedTracing=1;
static EVENT  *prgOutEvent      = NULL ;
static long    lgOutEventSize  = 0 ;

ARRAYINFO rgFullDemArray;       /*  logical array for full demands (RUD'S of coll. rules */
ARRAYINFO rgFeleEvalArr;        /*  logical array for already evaluated field lengths */

static int igStepWidth = 5;     /* interval width in minutes */
static int igDest = 5270;       /* mod-ID of DEMCCI */

/* static char cgCICF[64] = ""; * Code for First class *
static char cgCICC[64] = "";    * Code for Business class *
static char cgCICY[64] = "";    * Code for Economy class */

static char *cgDefaultClasses[3] = {"F","C","Y"};

PAXCALCM *prgPaxCalc = 0;   
int igPaxCalcCnt = 0;
static int   igReservedUrnoCnt = 0;
static long   lgActUrno =0 ;
static char cgEvalBuffer[201];              

static int igRudUcruIdx = -1;
static int igRudUtplIdx = -1;
static int igUcdUrudIdx = -1;
static int igUcdCdatIdx = -1;
static int igUcdHopoIdx = -1;
static int igUcdLstuIdx = -1;
static int igUcdUaftIdx = -1;
static int igUcdUrnoIdx = -1;
static int igUcdUsecIdx = -1;
static int igUcdUseuIdx = -1;
static int igPxcUpxaIdx = -1;
static int igPxcDpaxIdx = -1;
static int igPxcDbagIdx = -1;
static int igVtpValuIdx = -1;
static int igVtpPenrIdx = -1;
static int igVtpPberIdx = -1;
static int igSdiUrnoIdx = -1;
static int igSdiPbeaIdx = -1;
static int igSdiPenaIdx = -1;
static int igSdiUcccIdx = -1;
static int igSdiUproIdx = -1;
static int igFullDemUrnoIdx = 1;
static int igFullDemVrefIdx = 2;


#define URNOS_TO_FETCH 100


#define RUDFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgRudArray.plrArrayFieldOfs[ipFieldNo-1]])
#define UCDFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgUcdArray.plrArrayFieldOfs[ipFieldNo-1]])
#define SDIFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgSdiArray.plrArrayFieldOfs[ipFieldNo-1]])
#define NEWSDIFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgNewSdiArr.plrArrayFieldOfs[ipFieldNo-1]])
#define FULLDEMFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgFullDemArray.plrArrayFieldOfs[ipFieldNo-1]])

/******************************************************************************/
/* Function prototypes                                                        */
/******************************************************************************/
static int Init_discal();
static int  Reset(void);                       /* Reset program          */
static void Terminate(void);            /* Terminate program      */
/*static void   HandleSignal(int);*/                /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void HandleQueErr(int);                 /* Handles queuing errors */
static int  HandleData(void);                  /* Handles event data     */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/


static int GetRowLength(char *pcpTana, char *pcpFieldList, long *plpLen);
static int GetFieldLength(char *pcpTana, char *pcpFina, long *plpLen);
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, 
                        char *pcpArrayFieldList,char *pcpAddFields,
                        long *pcpAddFieldLens,char *pcpSelection, 
                        ARRAYINFO *prpArrayInfo, short spFlags );
#if 0
static int TriggerAction(char *pcpTableName, char *pcpFields);
static int CheckQueue(int ipModId, ITEM **prpItem);
static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem);
#endif
/*static int CheckRuleUrno ( char *pcpRuleUrno );*/
static int GetFieldByUrno ( ARRAYINFO *prpArray, char *pcpUrno, char *pcpField, 
                              char *pcpResult, int ipMaxBytes );

static int CreateIdx1 ( ARRAYINFO *prpArrayInfo, char *pcpIdxName, 
                        char *pcpFields, char *pcpOrdering );       

static int GetCEDAField ( HANDLE * pspHandle, char *pcpArrayName,
                          long *plpFields, char *pcpFieldNam,
                          long lpMaxBytes, long lpRowNum,
                          void *pvpDataBuf, int ipTrimRight );
void TrimRight(char *s);
int TimeToStr(char *pcpTime,time_t lpTime) ;
static int StrToTime(char *,time_t *);                                  /*rna*/
static int GetFieldData(char *pcpTabNam, char *pcpFldNam, char *pcpFldLst, 
                        char *pcpData, char **pcpFieldData);
void ReplaceChar ( char *pcpStr, char cpOld, char cpNew );
static int HandleUFR ( char *pcpFlight, char *pcpFields, char *pcpRueUrnos, char *pcpProUrno );
static int HandleDFR ( char *pcpData, char *pcpFields, char *pcpSelection, char *pcpCmd, char *pcpRecvName, char *pcpDestName );
static int GetLogicalRowLength(char *pcpTotalFieldList,char *pcpFieldList, long *plpFieldSizes,long *plpLen);
static int  SetAATArrayInfo(char *pcpArrayName,char *pcpArrayFieldList, long *plpFieldLens, 
                            char *pcpIdx01Name, char *pcpIdx01FieldList, char *pcpIdx01Order, 
                            ARRAYINFO *prpArrayInfo);
static int SelFullDemands ( char *pcpRueUrnos );
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo, int ipMode, int ipLogFile );
static int UpdateUCDTab ( char *pcpAftUrno );
static int FillUCDRecord ( char *pcpUAFT, char *pcpURUD, char *pcpHopo,
               char* pcpRecBuf, int ipBuflen );
void MyStrnCpy ( char *pcpDest, const char *pcpSource, size_t ipCount, int ipTrimRight );
static int GetPAXCount ( char *pcpAFTUrno, char *pcpPROUrno, char *pcpCCCUrno, 
                         char *pcpHopo, char *pcpStod, int *pipPAXCnt ); 
static int CalcSDIData ( char *pcpPROUrno, char *pcpCCCUrno, char *pcpHopo, 
                         char *pcpAFTUrno, char *pcpStod, SDIREC *prpRec );
static int UpdateSDITab ( char *pcpAftUrno, char *pcpProUrno, char *pcpStod );
static int SaveSDIChanges ( char *pcpHopo, char *pcpAftUrno);
static int CompareSDIFields( char *pclNewData, char *pclOldData, int *iplEqual );
static int ResetGlobalTempVars ();
static int KeepChangedUCD ( char *pcpRudUrno, char *pcpUccc, char *pcpUaft,
                            char * pcpHopo );
static int SendEvent2DEMCCI( int ipDest );
static int ReadPaxCalcMethods ();
static void DebugPrintPaxCalcMethods ( int ipLevel );
int RemoveChar ( char *pcpStr, char cpRemove );
static int OnFlightUpdate ( char *pcpAftUrno );
static int SendToQue(int ipModID, int ipPrio, BC_HEAD *prpBCHead, 
                            CMDBLK *prpCmdblk, char *pcpSelection, 
                            char *pcpFields, char *pcpData);
static int GetFullDemands ( char *pcpAftUrno, char *pcpHopo );
static int CopyNewSdiRecords () ;

static int  OpenUFRFLICOL( void );                                      /*rna*/
static int  CloseUFRFLICOL( void );                                     /*rna*/
static int  CheckChanges( char *, char *, BOOL * );                     /*rna*/
static int  CheckValidity( int, char *, BOOL *);                        /*rna*/
static int  ForwardEvent( int );
static int  GetDebugLevel(char *, int *);
static int  AddUCDChanges(char *);
static int  CheckInterval(char *, char *);
static int InitClassCodes ();
static int GetClassIndex ( char * pcpClassCode, int * pipIdx );
static int GetNextUrno(char *pcpUrno);
static int FindFeleEvaluation ( char *pcpTable, char *pcpField, char *pcpFele );
static int SaveFeleEvaluation ( char *pcpTable, char *pcpField, char *pcpFele );
static int InitFieldIndices();
static int EnlargeTimeFrame ( char * pcpSelection );

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int ilRC = RC_SUCCESS;          /* Return code          */
    int ilCnt = 0;
    
    INITIALIZE;         /* General initialization   */
    dbg(TRACE,"MAIN: version <%s>",mks_version);
    /* Attach to the MIKE queues */
    do
    {
        ilRC = init_que();
        if(ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRC != RC_SUCCESS));
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */
    do
    {
        ilRC = init_db();
        if (ilRC != RC_SUCCESS)
        {
            check_ret(ilRC);
            dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
            sleep(6);
        ilCnt++;
        } /* end of if */
    } while((ilCnt < 10) && (ilRC != RC_SUCCESS));
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
        sleep(60);
        exit(2);
    }else{
        dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */
    /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
    sprintf(cgConfigFile,"%s/discal",getenv("BIN_PATH"));
    ilRC = TransferFile(cgConfigFile);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */
    
    sprintf(cgConfigFile,"%s/discal.cfg",getenv("CFG_PATH")); 
    ilRC = TransferFile(cgConfigFile); 
    if(ilRC != RC_SUCCESS) 
    { 
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile); 
    }  /* end of if */

    ilRC = SendRemoteShutdown(mod_id);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */
    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */
    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        if(igInitOK == FALSE)
        {
            ilRC = Init_discal();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"Init_discal: init failed!");
            } /* end of if */
        }/* end of if */
    } else {
        Terminate();
    }/* end of if */
    dbg(TRACE,"MAIN: initializing OK");
    debug_level = igRuntimeMode;
    for(;;)
    {
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
            
            switch( prgEvent->command )
            {
            case    HSB_STANDBY :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;  
            case    HSB_COMING_UP   :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;  
            case    HSB_ACTIVE  :
                ctrl_sta = prgEvent->command;
                break;  
            case    HSB_ACT_TO_SBY  :
                ctrl_sta = prgEvent->command;
                /* CloseConnection(); */
                HandleQueues();
                break;  
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;  
            case    HSB_STANDALONE  :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                break;  
            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;
            case    SHUTDOWN    :
                /* process shutdown - maybe from uutil */
                Terminate();
                break;
                    
            case    RESET       :
                ilRC = Reset();
                break;
                    
            case    EVENT_DATA  :
                if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                {
                    ilRC = HandleData();
                    if(ilRC != RC_SUCCESS)
                    {
                        HandleErr(ilRC);
                    }/* end of if */
                }else{
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            case 111:
                ilRC = SaveIndexInfo ( &rgFullDemArray, 1, 0 );
                break;
            case 112:
                ilRC = SaveIndexInfo ( &rgFullDemArray, 2, 0 );
                break;
            case 113:
                ilRC = SaveIndexInfo ( &rgUcdArray, 2, 0 );
                break;
            case 114:
                ilRC = SaveIndexInfo ( &rgNewSdiArr, 2, 0 );
                break;
            case 115:
                ilRC = SaveIndexInfo ( &rgSdiArray, 2, 0 );
                break;
            case 116:
                ilRC = SaveIndexInfo(&rgFeleEvalArr, 2, 0) ;
                break;
            default         :
                dbg(TRACE,"MAIN: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRC);
        } /* end else */
        
    } /* end for */
    
    /* exit(0); */
    
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_discal()
{
    int     ilRC = RC_SUCCESS;          /* Return code */
    long    pllAddFieldLens[4], llFieldLength;
    char    clTabName[11];
    char    clSelection[81];

    /* now reading from configfile or from database */

    if ( debug_level < TRACE )
        debug_level = TRACE;

    /* read HomeAirPort from SGS.TAB */
    memset((void*)&cgHopo[0], 0x00, 8);
    ilRC = tool_search_exco_data("SYS", "HOMEAP", &cgHopo[0]);

    if (ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"Init_discal: EXTAB,SYS,HOMEAP not found in SGS.TAB" );
        Terminate();
    } 
    else
        dbg(TRACE,"Init_discal: home airport    <%s>",&cgHopo[0]);
    
    ilRC = tool_search_exco_data("ALL", "TABEND", cgTabEnd);

    if (ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"Init_discal: EXTAB, not found in SGS.TAB" );
        Terminate();
    } 
    else 
        dbg(TRACE,"Init_discal: table extension <%s>",cgTabEnd);
    
    if(ilRC == RC_SUCCESS)
    {
         GetDebugLevel("STARTUP_MODE", &igStartUpMode);
         GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
    }
    debug_level = igStartUpMode;

    InitFieldIndices();
    rgFeleEvalArr.rrArrayHandle = -1;

    ilRC = CEDAArrayInitialize(10,10);
    if(ilRC != RC_SUCCESS)      
        dbg(TRACE,"Init_discal: CEDAArrayInitialize failed <%d>",ilRC); 
    else
    {   
        if (ilRC == RC_SUCCESS)  
        {   /* create a table where all evaluations of sgrtab are stored */                                    
            pllAddFieldLens[0] = 32;
            pllAddFieldLens[1] = 16;
            pllAddFieldLens[2] = 8;
            pllAddFieldLens[3] = 0;
            ilRC = SetAATArrayInfo( "LENEVAL", "TANA,FINA,FELE", pllAddFieldLens, 
                                    "IDXLEN", "TANA,FINA", "A,A", &rgFeleEvalArr );
            if (ilRC != RC_SUCCESS)                                             
                dbg(TRACE,"Init_discal: SetAATArrayInfo for LENEVAL failed <%d>",ilRC);
        }

        /* Read Table RUD */
        /* hag 17.09.2003: Load only RUDs from collective rules and partial demands */
        strcpy ( clSelection, "where urue in (select urno from ruetab where ruty='1') or ffpd='1'" );
        ilRC = SetArrayInfo("RUDTAB", "RUD", RUD_FIELDS, 0, 0, clSelection, &rgRudArray,
                            CEDAARRAY_ALL );
        if(ilRC != RC_SUCCESS)                                             
            dbg(TRACE,"Init_discal: SetArrayInfo for RUD failed <%d>",ilRC);        
        else
        {
            ilRC = CreateIdx1 ( &rgRudArray, "IDXRUD1", "ALOC,FFPD,URUE,UCRU", 
                                "A,A,A,A" );
            if (ilRC!=RC_SUCCESS)
                dbg(TRACE,"Init_discal: CreateIdx1 for RUD failed <%d>",ilRC);      
        }
    }   

    /*  Initialize Array for Profiles */
    if (ilRC == RC_SUCCESS)                                        
    {                                                              
        ilRC = SetArrayInfo("PROTAB", "PRO", PRO_FIELDS, 0, 0, "", &rgProArray,
                            CEDAARRAY_ALL );
        if (ilRC != RC_SUCCESS)                                             
            dbg(TRACE,"Init_discal: SetArrayInfo for PRO failed <%d>",ilRC);
        else
        {
            ilRC = CreateIdx1 ( &rgProArray, "IDXPRO1", "URNO,HOPO,PRST,EXCL,FDCO,DCOP", 
                                "A,A,D,D,D,A" );
            if (ilRC!=RC_SUCCESS)
                dbg(TRACE,"Init_discal: CreateIdx1 for PRO failed <%d>",ilRC);      
        }
    }

    /*  Initialize Array for PXCTAB */
    if (ilRC == RC_SUCCESS)                                        
    {                                                              
        ilRC = SetArrayInfo("PXCTAB", "PXC", PXC_FIELDS, 0, 0, "", &rgPxcArray,
                            CEDAARRAY_ALL );
        if (ilRC != RC_SUCCESS)                                             
            dbg(TRACE,"Init_discal: SetArrayInfo for PXC failed <%d>",ilRC);
        else
        {
            ilRC = CreateIdx1 ( &rgPxcArray, "IDXPXC1", "UPRO,HOPO,UCCC", 
                                "A,A,A" );
            if (ilRC!=RC_SUCCESS)
                dbg(TRACE,"Init_discal: CreateIdx1 for PRO failed <%d>",ilRC);      
        }
    }

    /*  Initialize Array for Values */
    if (ilRC == RC_SUCCESS)                                        
    {                                                              
        ilRC = SetArrayInfo("VTPTAB", "VTP", VTP_FIELDS, 0, 0, "", &rgVtpArray,
                            CEDAARRAY_ALL );
        if (ilRC != RC_SUCCESS)                                             
            dbg(TRACE,"Init_discal: SetArrayInfo for VTP failed <%d>",ilRC);
        else
        {
            ilRC = CreateIdx1 ( &rgVtpArray, "IDXVTP1", "TABN,UVAL,HOPO", 
                                "A,A,A" );
            if (ilRC!=RC_SUCCESS)
                dbg(TRACE,"Init_discal: CreateIdx1 for VTP failed <%d>",ilRC);      
        }
    }

    /*  Initialize Array for Single Distributions */
    if (ilRC == RC_SUCCESS)                                        
    {                                     
        /*  Since we have used CopyNewSdiRecords, we don't need TriggerAction 
            for this table any longer,
            EXCEPTION:   DISCAL is running more than once                   */
        ilRC = SetArrayInfo("SDITAB", "SDI", SDI_FIELDS, 0, 0, "", &rgSdiArray,
                            CEDAARRAY_FILL | CEDAARRAY_URNOIDX );
        if (ilRC != RC_SUCCESS)                                             
            dbg(TRACE,"Init_discal: SetArrayInfo for SDI failed <%d>",ilRC);
        else
        {
            ilRC = CreateIdx1 ( &rgSdiArray, "IDXSDI1", "HOPO,UAFT,UCCC,UPRO", 
                                "A,A,A,A" );
            if (ilRC!=RC_SUCCESS)
                dbg(TRACE,"Init_discal: CreateIdx1 for SDI failed <%d>",ilRC);      
        }
        if ( ilRC == RC_SUCCESS )
        {
            ilRC = CEDAArraySendChanges2ACTION(&rgSdiArray.rrArrayHandle,
                                               rgSdiArray.crArrayName,TRUE);
            if (ilRC!=RC_SUCCESS)                                                
                dbg(TRACE,"Init_discal: CEDAArraySendChanges2ACTION for SDI failed <%d>",
                    ilRC);
        }
    }

    /*  Initialize Array for Single Distributions */
    if (ilRC == RC_SUCCESS)                                        
    {                                                              
        ilRC = SetArrayInfo("NEWSDI", "SDI", SDI_FIELDS, 0, 0, "", 
                            &rgNewSdiArr, 0 );
        if (ilRC != RC_SUCCESS)                                             
            dbg(TRACE,"Init_discal: SetArrayInfo for NEWSDI failed <%d>",ilRC);
        else
        {
            ilRC = CreateIdx1 ( &rgNewSdiArr, "IDXSDI1", "UCCC,UAFT", "A,A" );

            if (ilRC!=RC_SUCCESS)
                dbg(TRACE,"Init_discal: CreateIdx1 for NEWSDI failed <%d>",ilRC);       
        }

        if (ilRC = GetFieldLength("SDI","URNO",&lgUrnolen) != RC_SUCCESS)/*rna*/
        {                                                               /*rna*/
            dbg(TRACE,"Init_discal: GetFieldLength for SDI.URNO failed <%d>",
                ilRC);                                                  /*rna*/
            lgUrnolen = 10;                                             /*rna*/
        }                                                               /*rna*/
        /* Since we have used CopyNewSdiRecords, next statement isn't needed 
          any longer */
        /*
        if ( ilRC == RC_SUCCESS )
        {
            ilRC = CEDAArraySendChanges2ACTION(&rgNewSdiArr.rrArrayHandle,
                                               rgNewSdiArr.crArrayName,TRUE);
            if (ilRC!=RC_SUCCESS)                                                
                dbg(TRACE,"Init_discal: CEDAArraySendChanges2ACTION for NEWSDI failed <%d>",
                    ilRC);
        }*/
    }

    /*  Initialize Array for UCDTAB */
    if (ilRC == RC_SUCCESS)                                        
    {                                                              
        ilRC = SetArrayInfo("UCDTAB", "UCD", UCD_FIELDS, 0, 0, "", &rgUcdArray,
                            CEDAARRAY_FILL );
        if (ilRC != RC_SUCCESS)                                             
            dbg(TRACE,"Init_discal: SetArrayInfo for UCD failed <%d>",ilRC);
        else
        {
            ilRC = CreateIdx1 ( &rgUcdArray, "IDXUCD1", "HOPO,UAFT,URUD", 
                                "A,A,A" );
            if (ilRC!=RC_SUCCESS)
                dbg(TRACE,"Init_discal: CreateIdx1 for UCD failed <%d>",ilRC);      
        }
        if ( ilRC == RC_SUCCESS )
        {
            ilRC = CEDAArraySendChanges2ACTION(&rgUcdArray.rrArrayHandle,
                                               rgUcdArray.crArrayName,TRUE);
            if (ilRC!=RC_SUCCESS)                                                
                dbg(TRACE,"Init_discal: CEDAArraySendChanges2ACTION for UCD:failed <%d>",
                    ilRC);
        }
    }

    if (ilRC == RC_SUCCESS)  
    {                                       
        if ( GetFieldLength("RUD","URNO",&llFieldLength) == RC_SUCCESS )
            pllAddFieldLens[0] = llFieldLength;
        else
        {
            dbg(TRACE,"Init_discal: GetFieldLength for RUD.URNO failed <%d>",ilRC);
            pllAddFieldLens[0] = 10;
        }
        if ( GetFieldLength("RUD","VREF",&llFieldLength) == RC_SUCCESS )
            pllAddFieldLens[1] = llFieldLength;
        else
        {
            dbg(TRACE,"Init_discal: GetFieldLength for RUD.VREF failed <%d>",ilRC);
            pllAddFieldLens[1] = 10;
        }
        pllAddFieldLens[2] = 0;
        ilRC = SetAATArrayInfo( "RUDFFPD0", "URNO,VREF", pllAddFieldLens, 
                             "IDXFFPD0", "VREF,URNO", "A,A", &rgFullDemArray );
        if (ilRC != RC_SUCCESS)                                             
            dbg(TRACE,"Init_discal: SetAATArrayInfo for RUDFFPD0 failed <%d>",ilRC);
    }
    
    /*  Initialize Array for CCCTAB */
    if (ilRC == RC_SUCCESS)                                        
    {                                                              
        ilRC = SetArrayInfo("CCCTAB", "CCC", CCC_FIELDS, 0, 0, "", &rgCccArray, 
                             CEDAARRAY_ALL );
        if (ilRC != RC_SUCCESS)                                             
            dbg(TRACE,"Init_discal: SetArrayInfo for CCC failed <%d>",ilRC);
    }
    if (ilRC == RC_SUCCESS)                                        
    {                                                              
        ilRC = CEDAArrayGetRowCount( &(rgCccArray.rrArrayHandle), 
                                     rgCccArray.crArrayName, &lgNoOfCC );

    }

    if (ilRC == RC_SUCCESS)                                        
    {                                                              
        ilRC = InitClassCodes (); 
        if ( ilRC != RC_SUCCESS )
            dbg(TRACE,"Init_discal: InitClassCodes failed <%d>",ilRC);
    }
        
    if (ilRC == RC_SUCCESS)                                        
    {                                                              
        ilRC = ReadPaxCalcMethods () ;
        if ( ilRC == RC_SUCCESS )
            DebugPrintPaxCalcMethods ( TRACE );
        else
            dbg(TRACE,"Init_discal: ReadPaxCalcMethods failed <%d>",ilRC);
    }

    /*  not necessary as long as processing of PAXTAB is inactive 
    if (ilRC == RC_SUCCESS)                                        
    {
        sprintf ( clTabName, "PAX%s", cgTabEnd );
        ilRC = TriggerAction(clTabName,PAX_FIELDS) ; 
        if (ilRC != RC_SUCCESS)
            dbg (TRACE, "Init: TriggerAction table<%s> failed <%d>", 
                 clTabName, ilRC) ;
    }
    */
    return(ilRC);
    
} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int ilRC = RC_SUCCESS;              /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRC;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(void)
{
    logoff();
    dbg(TRACE,"Terminate: now leaving ...");
    fclose(outp);

    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
#if 0
static void HandleSignal(int pipSig)
{
    int ilRC = RC_SUCCESS;          /* Return code */
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);
    switch(pipSig)
    {
    default :
        Terminate(0);
        break;
    } /* end of switch */
    exit(0);
    
} /* end of HandleSignal */
#endif

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    switch(pipErr) {
    case    QUE_E_FUNC  :   /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :   /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND  :   /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET   :   /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX   :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE  :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES  :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP  :   /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL  :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG :   /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :   /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :   /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default         :   /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int ilRC = RC_SUCCESS;          /* Return code */
    int ilBreakOut = FALSE;
    
    do
    {
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text; 
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_COMING_UP   :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_ACTIVE  :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;  
            case    HSB_ACT_TO_SBY  :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;  
    
            case    HSB_STANDALONE  :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                ilBreakOut = TRUE;
                break;  
            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;
            case    SHUTDOWN    :
                Terminate();
                break;
                        
            case    RESET       :
                ilRC = Reset();
                break;
                        
            case    EVENT_DATA  :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default         :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRC);
        } /* end else */
    } while (ilBreakOut == FALSE);
    if(igInitOK == FALSE)
    {
            ilRC = Init_discal();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"Init_discal: init failed!");
            } /* end of if */
    }/* end of if */
    /* OpenConnection(); */
} /* end of HandleQueues */
    

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData()
{
    int ilRC = RC_SUCCESS;          /* Return code */
    
    BC_HEAD *prlBchd      = NULL;
    CMDBLK  *prlCmdblk    = NULL;
    char    *pclSelKey    = NULL;
    char    *pclFields    = NULL;
    char    *pclData      = NULL;
    char    pclHost[16]  = "";
    char    pclTable[16]  = "";
    char    pclUrnoList[1001]  = "";
    char    pclActCmd[16] = "";
    char    *pclRueUrnos     = NULL;
    char    *pclProUrno     = NULL;
    int     ilQueOut = -1;

    dbg(TRACE,"================= TWEHDL EVENT BEGIN ================");
    /* Queue-id of event sender */
    ilQueOut = prgEvent->originator;

    /* get broadcast header */
    prlBchd = (BC_HEAD *)((char *)prgEvent + sizeof(EVENT));

    /* Save Global Elements of BC_HEAD */
    memset(pcgRecvName, '\0', (sizeof(prlBchd->recv_name)+1));
    strcpy(pclHost,prlBchd->orig_name);                      /* Host        */
    strcpy(pcgDestName,prlBchd->dest_name);                  /* UserLoginName */
    strncpy(pcgRecvName, prlBchd->recv_name,
          sizeof(prlBchd->recv_name));                       /* Workstation   */

    /* get command block  */
    prlCmdblk = (CMDBLK *)((char *)prlBchd->data);

    /* Save Global Elements of CMDBLK */
    strcpy(pcgTwStart,prlCmdblk->tw_start);
    strcpy(pcgTwEnd,prlCmdblk->tw_end);

    /*  3 Strings in CMDBLK -> data  */   
    pclSelKey = (char *)prlCmdblk->data;

    pclFields = (char *)pclSelKey + strlen(pclSelKey) + 1;
    pclData = (char *)pclFields + strlen(pclFields) + 1;
    
    strcpy (pclTable, prlCmdblk->obj_name);                     /*  table name  */
    strcpy (pclActCmd, prlCmdblk->command);                 /*  actual command  */

    if ( igExtendedTracing )
    {
        dbg(TRACE," HANDLE COMMAND");
        dbg(DEBUG,"TWEHDL (mod_id = %d) GET FOLLOWING EVENT:", mod_id);
        dbg(TRACE,"FROM ID<%d> HOST<%s>", ilQueOut, pclHost);
        dbg(TRACE,"WKS<%s> USER<%s>", pcgRecvName, pcgDestName);
        dbg(TRACE,"CMD <%s>  TBL <%s>", pclActCmd,pclTable);
        dbg(TRACE,"SELECT <%s>", pclSelKey);
        dbg(TRACE,"FIELDS <%s>", pclFields);
        dbg(TRACE,"DATA   <%s>", pclData);
        dbg(TRACE,"SPECIAL INFO <%s>",pcgTwEnd);
        dbg(DEBUG,"TWSTART <%s>",pcgTwStart);
    }
    else
        dbg(TRACE,"Received command <%s>", pclActCmd );  

    /*  COMPARE COMAND AND GOTO FUNCTION */
    
    if (!strcmp(pclActCmd,"IFR" )||!strcmp(pclActCmd,"UFR") )           /*  IFR,UFR  */
    {
        if (!strcmp(pclActCmd,"UFR") && !strcmp(pcgDestName,"FLICOL"))  /*rna*/
        {                                                               /*rna*/
            if (!bgOpenUFRFLICOL)                                       /*rna*/
                OpenUFRFLICOL();                                        /*rna*/
        }                                                               /*rna*/
        dbg(TRACE,"command IFR or UFR found ");
        /* parse Data-String -> [Flightdata<\n>RuleUrnos<\n>ProfileUrno] */
        pclRueUrnos  = strchr ( pclData, '\n' );
        if ( pclRueUrnos && pclRueUrnos[0] )    /* found RuleUrnos */
        {
            *pclRueUrnos = '\0';
            pclRueUrnos++;
            pclProUrno = strchr ( pclRueUrnos, '\n' );
            if ( pclProUrno )
            {
                *pclProUrno = '\0';
                pclProUrno++;
            }
            dbg(TRACE,"RULEURNOS <%s>", pclRueUrnos );
            if ( pclProUrno )
                dbg(TRACE,"PROFILEURNO <%s>", pclProUrno );
            else
                dbg(TRACE,"PROFILEURNO is empty" );
        }
        else
            dbg(TRACE,"RULEURNOS are empty" ); 
        if ( pclProUrno && pclProUrno[0] )   /* found ProfileUrno */
            ilRC = HandleUFR ( pclData, pclFields, pclRueUrnos, pclProUrno );
        else
            ilRC = HandleDFR(pclData, pclFields, pclSelKey, pclActCmd, pcgRecvName, pcgDestName);         
    }
    else if (!strcmp(pclActCmd,"DFR" ) )            /*  DFR  */
    {
        if (bgOpenUFRFLICOL)                                            /*rna*/
            CloseUFRFLICOL();                                           /*rna*/
        ilRC = HandleDFR ( pclData, pclFields, pclSelKey, pclActCmd, pcgRecvName, pcgDestName );
    }
    else if ( !strcmp(pclActCmd,"IRT") || !strcmp(pclActCmd,"URT") ||
              !strcmp(pclActCmd,"DRT") )    /*  IRT,URT,DRT  */
    {
        if (bgOpenUFRFLICOL)                                            /*rna*/
            CloseUFRFLICOL();                                           /*rna*/
        if ( !strncmp( pclTable, "PAX", 3 ) )
            /*ilRC = HandlePaxTabChange ( pclData, pclFields )*/;
        else
        {
            ilRC = CEDAArrayEventUpdate ( prgEvent );  
            dbg(TRACE,"command IRT, URT or DRT processed, table<%s> ilRC <%ld>", 
                pclTable, ilRC );
        }
    }
    else if ( !strcmp(pclActCmd,"TFC") )    /*  TFC = Test Flight Change */     
    {
        if (bgOpenUFRFLICOL)                                            /*rna*/
            CloseUFRFLICOL();                                           /*rna*/
        if ( strlen ( pclSelKey ) > 0 )
        {
            MyStrnCpy ( pclUrnoList, pclSelKey, 10, 1 );
            ReplaceChar ( pclUrnoList, '\n', '\0' );
            ReplaceChar ( pclUrnoList, '\r', '\0' );
            ilRC = OnFlightUpdate ( pclUrnoList );
            dbg ( TRACE, "OnFlightUpdate return <%d> for UAFT <%s>", ilRC, 
                  pclUrnoList );
        }
    }
    else if ( !strcmp(pclActCmd,"CFL") )                                /*rna*/
    {                                                                   
        EnlargeTimeFrame ( pclSelKey );
        if (bgOpenUFRFLICOL)                                            
            CloseUFRFLICOL();                                           

        ilRC = ForwardEvent(igDest);
        if (ilRC != RC_SUCCESS)
        {
            dbg(TRACE, "HandleData: ForwardEvent failed <%d>", ilRC);
        }
    }                                                                   /*rna*/
    else
        if ( strcmp ( pclActCmd, "ANSWER" ) )
            dbg(TRACE, "unknown command <%s> found", pclActCmd ); 
    return ilRC;
    
} /* end of HandleData */

/******************************************************************************/
/*  SetArrayInfo:                                                             */
/*  following flags activate/disactivate features to improve performance      */
/*  CEDAARRAY_FILL:    fill array from DB; rgNewSDIArr doesn't need to be filled  */
/*  CEDAARRAY_URNOIDX: create an index for URNO; if not used, we have one     */
/*                     Index less, which has to be sorted !                   */
/*  CEDAARRAY_TRIGGER: UCDTAB, that is used only by DISCAL doesn't need to be */
/*                     triggered                                              */
/******************************************************************************/

static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, 
                        char *pcpArrayFieldList,char *pcpAddFields,
                        long *pcpAddFieldLens,char *pcpSelection,
                        ARRAYINFO *prpArrayInfo, short spFlags )
{
    int ilRc = RC_SUCCESS;              /* Return code */

    if(prpArrayInfo == NULL)
    {
        dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
        ilRc = RC_FAIL;
    }
    else
    {
        memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

        prpArrayInfo->rrArrayHandle = -1;
        prpArrayInfo->rrIdx01Handle = -1;

        if(pcpArrayName != NULL)
        {
            if(strlen(pcpArrayName) <= ARR_NAME_LEN)
            {
                strcpy(prpArrayInfo->crArrayName,pcpArrayName);
            }/* end of if */
        }/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);
        if (pcpAddFields != NULL)
        {
            prpArrayInfo->lrArrayFieldCnt += get_no_of_items(pcpAddFields);
        }

        prpArrayInfo->plrArrayFieldOfs = (long*) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
        if(prpArrayInfo->plrArrayFieldOfs == NULL)
        {
            dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",
                prpArrayInfo->lrArrayFieldCnt,sizeof(long));
            ilRc = RC_FAIL;
        }/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->plrArrayFieldLen = (long*) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
        if(prpArrayInfo->plrArrayFieldLen == NULL)
        {
            dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",
                prpArrayInfo->lrArrayFieldCnt,sizeof(long));
            ilRc = RC_FAIL;
        }
        else
        {
            *(prpArrayInfo->plrArrayFieldLen) = -1;
        }/* end of if */
    }/* end of if */

    if(pcpTableName == NULL)
    {
        dbg(TRACE,"SetArrayInfo: invalid parameter TableName: null pointer not valid");
        ilRc = RC_FAIL;
    }
    else
    {
        strcpy ( prpArrayInfo->crTableName,pcpTableName );
        strcat ( prpArrayInfo->crTableName,cgTabEnd );
        dbg(TRACE,"SetArrayInfo: crTableName=<%s>",prpArrayInfo->crTableName );
    }
    if(ilRc == RC_SUCCESS)
    {
        ilRc = GetRowLength(prpArrayInfo->crTableName, pcpArrayFieldList,
                            &(prpArrayInfo->lrArrayRowLen));
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
        }/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->lrArrayRowLen+=100;
        prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+10));
        if(prpArrayInfo->pcrArrayRowBuf == NULL)
        {
                ilRc = RC_FAIL;
                dbg(TRACE,"SetArrayInfo: calloc failed");
        }/* end of if */
    }/* end of if */


    if(ilRc == RC_SUCCESS)
    {
        dbg(TRACE,"CEDAArrayCreate follows Array <%s> Table <%s>",pcpArrayName,
                                                    prpArrayInfo->crTableName);
        ilRc = CEDAArrayCreate( &(prpArrayInfo->rrArrayHandle),
                                pcpArrayName,prpArrayInfo->crTableName,
                                pcpSelection,pcpAddFields, pcpAddFieldLens, 
                                pcpArrayFieldList,
                                &(prpArrayInfo->plrArrayFieldLen[0]),
                                &(prpArrayInfo->plrArrayFieldOfs[0]));

        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"SetArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
        }/* end of if */
    }/* end of if */

    if(pcpArrayFieldList != NULL)
    {
        if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
        {
            strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
        }/* end of if */
    }/* end of if */
    if(pcpAddFields != NULL)
    {
        if(strlen(pcpAddFields) + 
            strlen(prpArrayInfo->crArrayFieldList) <= 
                    (size_t)ARR_FLDLST_LEN)
        {
            strcat(prpArrayInfo->crArrayFieldList,",");
            strcat(prpArrayInfo->crArrayFieldList,pcpAddFields);
            dbg(TRACE,"<%s>",prpArrayInfo->crArrayFieldList);
        }/* end of if */
    }/* end of if */
    if ( (ilRc == RC_SUCCESS) && (spFlags&CEDAARRAY_FILL) )
    {
        ilRc = CEDAArrayFill(&(prpArrayInfo->rrArrayHandle),pcpArrayName,NULL);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"SetArrayInfo: CEDAArrayFill failed <%d>",ilRc);
        }
    }
    if ( (ilRc == RC_SUCCESS) && (spFlags&CEDAARRAY_URNOIDX) && 
          strstr(pcpArrayFieldList,"URNO") )
    {
        strcpy ( prpArrayInfo->crIdxUrnoName, "URNOIDX" );
        ilRc = CEDAArrayCreateSimpleIndexDown(&(prpArrayInfo->rrArrayHandle),
                                              prpArrayInfo->crArrayName,
                                              &(prpArrayInfo->rrIdxUrnoHandle), 
                                              prpArrayInfo->crIdxUrnoName, "URNO" );
        if(ilRc != RC_SUCCESS)                                             
        {
            dbg( TRACE,"SetArrayInfo: CEDAArrayCreateSimpleIndexDown for %s failed <%d>",
                 prpArrayInfo->crTableName, ilRc);      
        }
    }
    else
        prpArrayInfo->rrIdxUrnoHandle = -1;
    if ( (ilRc == RC_SUCCESS) && (spFlags&CEDAARRAY_TRIGGER) )
    {                                          /* send update msg. for active changes */ 
        /*ilRc = TriggerAction(prpArrayInfo->crTableName,pcpArrayFieldList) ;*/
        ilRc = ConfigureAction (prpArrayInfo->crTableName,pcpArrayFieldList, NULL );

        if (ilRc != RC_SUCCESS)
            dbg (TRACE, "SetArrayInfo: ConfigureAction table<%s> failed <%d>", 
                  prpArrayInfo->crTableName, ilRc) ;
        else
            dbg (TRACE, "SetArrayInfo: ConfigureAction table<%s> succeeded.", 
                  prpArrayInfo->crTableName ) ; 
    }/* end of if */


    {
        int ilLc;
        for(ilLc = 0; ilLc < prpArrayInfo->lrArrayFieldCnt; ilLc++)
        {
            dbg(TRACE,"%02d %d",ilLc,prpArrayInfo->plrArrayFieldOfs[ilLc]);
        }
    }
    return(ilRc);
}



/******************************************************************************/
/******************************************************************************/
static int GetRowLength(char *pcpTana, char *pcpFieldList, long *plpLen)
{
    int  ilRc = RC_SUCCESS;         /* Return code */
    int  ilNoOfItems = 0;
    int  ilLoop = 0;
    long llFldLen = 0;
    long llRowLen = 0;
    char clFina[8];
    
    ilNoOfItems = get_no_of_items(pcpFieldList);

    ilLoop = 1;
    do
    {
        ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
        if(ilRc > 0)
        {
            ilRc = GetFieldLength(pcpTana,clFina,&llFldLen);
            if(ilRc == RC_SUCCESS)
            {
                llRowLen++;
                llRowLen += llFldLen;
            }/* end of if */
        }/* end of if */
        ilLoop++;
    }while((ilRc == RC_SUCCESS) && (ilLoop <= ilNoOfItems));

    if(ilRc == RC_SUCCESS)
    {
        *plpLen = llRowLen;
    }/* end of if */

    return(ilRc);
    
} /* end of GetRowLength */



/******************************************************************************/
/******************************************************************************/
#if 0
static int GetFieldLength(char *pcpTana, char *pcpFina, long *plpLen)
{
    int  ilRc = RC_SUCCESS;         /* Return code */
    int  ilCnt = 0;
    char clTaFi[32];
    char clFele[16];
    char clFldLst[16];

    ilCnt = 1;
    sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
    sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerst�rt */

    dbg(TRACE,"GetFieldLength Tana <%s> Fina <%s>",pcpTana,pcpFina);

    ilRc = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
    switch (ilRc)
    {
    case RC_SUCCESS :
        *plpLen = atoi(&clFele[0]);
        break;

    case RC_NOT_FOUND :
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s,%s>",
                        ilRc,pcpTana,pcpFina);
        break;

    case RC_FAIL :
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
        break;

    default :
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
        break;
    }/* end of switch */

    return(ilRc);
    
} /* end of GetFieldLength */
#endif

/******************************************************************************/
/******************************************************************************/


#if 0
static int TriggerAction(char *pcpTableName, char *pcpFields)
{
  int           ilRc          = RC_SUCCESS ;    /* Return code */
  EVENT        *prlEvent      = NULL ;
  BC_HEAD      *prlBchead     = NULL ;
  CMDBLK       *prlCmdblk     = NULL ;
  ACTIONConfig *prlAction     = NULL ;
  char         *pclSelection  = NULL ;
  char         *pclFields     = NULL ;
  char         *pclData       = NULL ;
  long          llActSize     = 0 ;
  int           ilAnswerQueue = 0 ;
  char          clQueueName[16] ;
    
  if(ilRc == RC_SUCCESS)
  {

  }/* end of if */
    

  if (ilRc == RC_SUCCESS)
  {
   sprintf (&clQueueName[0],"%s2", mod_name) ;

   ilRc = GetDynamicQueue(&ilAnswerQueue,&clQueueName[0]);
   if (ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: GetDynamicQueue failed <%d>",ilRc);
   }
   else
   {
    dbg(DEBUG,"TriggerAction: got <%d> for <%s>",ilAnswerQueue, &clQueueName[0]);
   }/* end of if */
  }/* end of if */
    
  if (ilRc == RC_SUCCESS)
  {
    llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + sizeof(ACTIONConfig) + 666;
    if ( !prgOutEvent || ( llActSize > lgOutEventSize ) )
    {
        prgOutEvent = realloc(prgOutEvent,llActSize);
        if(prgOutEvent == NULL)
        {
            dbg(TRACE,"TriggerAction: realloc out event <%d> bytes failed",llActSize);
            ilRc = RC_FAIL ;
            lgOutEventSize = 0;
        } /* end of if */
        else
            lgOutEventSize = llActSize;
    }
  } /* end of if */

  if(ilRc == RC_SUCCESS)
  {
   prlBchead     = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;

    memset( prlBchead, 0, sizeof(BC_HEAD) );    /*hag20011002*/
    prlBchead->rc = RC_SUCCESS;                 /*hag20011002*/
    strcpy(prlBchead->dest_name, "DISCAL" );    /*hag20011002*/ 
    strcpy(prlBchead->recv_name, "ACTION" );    /*hag20011002*/

   prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data) ;
   pclSelection  = prlCmdblk->data ;
   pclFields     = pclSelection + strlen (pclSelection) + 1 ;
   pclData       = pclFields + strlen (pclFields) + 1 ;
   strcpy (pclData, "DYN") ;
   prlAction     = (ACTIONConfig *) (pclData + strlen(pclData) + 1) ;
        
   prgOutEvent->type        = USR_EVENT ;
   prgOutEvent->command     = EVENT_DATA ;
   prgOutEvent->originator  = ilAnswerQueue ;
   prgOutEvent->retry_count = 0 ;
   prgOutEvent->data_offset = sizeof(EVENT) ;
   prgOutEvent->data_length = lgOutEventSize-sizeof(EVENT) ;

   prlAction->iEmptyFieldHandling = iDELETE_ALL_BLANKS ;
   prlAction->iIgnoreEmptyFields = 0 ;
   strcpy(prlAction->pcSndCmd, "") ;
   sprintf(prlAction->pcSectionName,"%s_%s", mod_name,pcpTableName) ;
   strcpy(prlAction->pcTableName, pcpTableName) ;
   strcpy(prlAction->pcFields, "") ;
   if ( pcpFields )
        strcpy ( prlAction->pcFields, pcpFields );
   strcpy(prlAction->pcSectionCommands, "IRT,URT,DRT") ;
   prlAction->iModID = mod_id ;

   /****************************************
     DebugPrintEvent(DEBUG,prgOutEvent) ;
     DebugPrintBchead(DEBUG,prlBchead) ;
     DebugPrintCmdblk(DEBUG,prlCmdblk) ;
     dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
     dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
     dbg (DEBUG,"TriggerAction: data      <%s>",pclData) ;
     DebugPrintACTIONConfig(DEBUG,prlAction) ;
   ****************************************/

   if(ilRc == RC_SUCCESS)
   {
    prlAction->iADFlag = iDELETE_SECTION ;

    ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_2, llActSize, (char *) prgOutEvent) ; 
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
    }
    else
    {
     ilRc = WaitAndCheckQueue(10, ilAnswerQueue,&prgItem) ;
     if(ilRc != RC_SUCCESS)
     {
      dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
     }
     else
     {
      /* return value for delete does not matter !!! */
        
      /****************************************
      prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
      prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
      prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
      pclSelection = (char *)    prlCmdblk->data ;
      pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1;
      pclData      = (char *)    pclFields + strlen(pclFields) + 1;
      DebugPrintItem(DEBUG, prgItem) ;
      DebugPrintEvent(DEBUG, prlEvent) ;
      DebugPrintBchead(DEBUG, prlBchead) ;
      DebugPrintCmdblk(DEBUG, prlCmdblk) ;
      dbg(DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg(DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ****************************************/
    } /* end of if */
   }/* end of if */
  }/* end of if */

  if(ilRc == RC_SUCCESS)
  {
   prlAction->iADFlag = iADD_SECTION ;

   ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_2, llActSize,
                                              (char *) prgOutEvent ) ;
   if(ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
   }
   else
   {
    ilRc = WaitAndCheckQueue(10,ilAnswerQueue,&prgItem) ;
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
    }
    else
    {
     prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
     prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
     prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
     pclSelection = (char *)    prlCmdblk->data ;
     pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
     pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;

     if(strcmp(pclData,"SUCCESS") != 0)
     {
      dbg(TRACE,"TriggerAction: add dynamic action config failed, pclData=%s", pclData ) ;
      DebugPrintItem(DEBUG,prgItem) ;
      DebugPrintEvent(DEBUG,prlEvent) ;
      DebugPrintBchead(DEBUG,prlBchead) ;
      DebugPrintCmdblk(DEBUG,prlCmdblk) ;
      dbg (TRACE,"TriggerAction: selection <%s>",pclSelection) ;
      dbg (TRACE,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(TRACE,"TriggerAction: data      <%s>",pclData) ;
      ilRc = RC_FAIL ;
     }
     else
     {
      dbg(TRACE,"TriggerAction: add dynamic action config OK") ;
     }/* end of if */
    }/* end of if */
   }/* end of if */
  }/* end of if */

  ilRc = que(QUE_DELETE,ilAnswerQueue,ilAnswerQueue,0,0,0) ;
  if(ilRc != RC_SUCCESS)
  {
   dbg(TRACE,"que QUE_DELETE <%d> failed <%d>",ilAnswerQueue, ilRc) ;
  }
  else
  {
   dbg(DEBUG,"TriggerAction: queue <%d> <%s> deleted", ilAnswerQueue, &clQueueName[0]) ;
  }/* end of if */
 }/* end of if */

 return (ilRc) ;
    
}



/******************************************************************************/
/******************************************************************************/
static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem)
{
    int ilRC          = RC_SUCCESS;                      /* Return code */
    int ilWaitCounter = 0;

    do
    {
        sleep(1);
        ilWaitCounter += 1;

        ilRC = CheckQueue(ipModId,prpItem);
        if(ilRC != RC_SUCCESS)
        {
            if(ilRC != QUE_E_NOMSG)
            {
                dbg(TRACE,"WaitAndCheckQueue: CheckQueue <%d> failed <%d>",mod_id,ilRC);
            }/* end of if */
        }/* end of if */
    }while((ilRC == QUE_E_NOMSG) && (ilWaitCounter < ipTimeout));

    if(ilWaitCounter >= ipTimeout)
    {
        dbg(TRACE,"WaitAndCheckQueue: timeout reached <%d>",ipTimeout);
        ilRC = RC_FAIL;
    }/* end of if */

    return ilRC;

}/* end of WaitAndCheckQueue */

/******************************************************************************/
static int CheckQueue(int ipModId, ITEM **prpItem)
{
    int     ilRc       = RC_SUCCESS;                      /* Return code */
    int     ilItemSize = 0;
    EVENT *prlEvent    = NULL;
static  ITEM  *prlItem      = NULL;
    
    ilItemSize = I_SIZE;
    
    ilRc = que(QUE_GETBIGNW,ipModId,ipModId,PRIORITY_3,ilItemSize,(char *) &prlItem);
    if(ilRc == RC_SUCCESS)
    {
        dbg(TRACE,"DebugPrintItem with prlItem follows");
        DebugPrintItem(TRACE,prlItem);
        *prpItem = prlItem;
        dbg(TRACE,"DebugPrintItem with prpItem follows");
        DebugPrintItem(TRACE,*prpItem);
        prlEvent = (EVENT*) ((prlItem)->text);

        switch( prlEvent->command )
        {
            case    HSB_STANDBY :
                dbg(TRACE,"CheckQueue: HSB_STANDBY");
                HandleQueues();
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_COMING_UP   :
                dbg(TRACE,"CheckQueue: HSB_COMING_UP");
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_ACTIVE  :
                dbg(TRACE,"CheckQueue: HSB_ACTIVE");
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_ACT_TO_SBY  :
                dbg(TRACE,"CheckQueue: HSB_ACT_TO_SBY");
                HandleQueues();
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_DOWN    :
                dbg(TRACE,"CheckQueue: HSB_DOWN");
                ctrl_sta = prlEvent->command;
                /* Acknowledge the item 
                ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
                if( ilRc != RC_SUCCESS )
                {
                    HandleQueErr(ilRc);
                }  */
                Terminate();
                break;
        
        case    HSB_STANDALONE  :
                dbg(TRACE,"CheckQueue: HSB_STANDALONE");
                ctrl_sta = prlEvent->command;
                break;
    
            case    SHUTDOWN    :
                dbg(TRACE,"CheckQueue: SHUTDOWN");
                /* Acknowledge the item */
                ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
                if( ilRc != RC_SUCCESS )
                {
                    /* handle que_ack error */
                    HandleQueErr(ilRc);
                } /* fi */
                Terminate();
                break;
    
            case    RESET       :
                ilRc = Reset();
                break;
    
            case    EVENT_DATA  :
                /* DebugPrintItem(DEBUG,prgItem); */
                /* DebugPrintEvent(DEBUG,prlEvent); */
                break;
    
            case    TRACE_ON :
                dbg_handle_debug(prlEvent->command);
                break;
    
            case    TRACE_OFF :
                dbg_handle_debug(prlEvent->command);
                break;


            default         :
                dbg(TRACE,"CheckQueue: unknown event");
                DebugPrintItem(TRACE,prlItem);
                DebugPrintEvent(TRACE,prlEvent);
                break;
    
        } /* end switch */
    
        /* Acknowledge the item */
        ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
        if( ilRc != RC_SUCCESS )
        {
            /* handle que_ack error */
            HandleQueErr(ilRc);
        } /* fi */
    }else{
        if(ilRc != QUE_E_NOMSG)
        {
            dbg(TRACE,"CheckQueue: que(QUE_GETBIG,%d,%d,...) failed <%d>",ipModId,ipModId,ilRc);
            HandleQueErr(ilRc);
        }/* end of if */
    }/* end of if */
       
    return ilRc;
}/* end of CheckQueue */

#endif

/******************************************************************************/
/*  CheckRuleUrno: Check, whether rule contains partial demands for checkin   */
/******************************************************************************/
/*
static int CheckRuleUrno ( char *pcpRuleUrno )
{
    long llRow = ARR_FIRST ;
    int  ilRC;
    char    clRuleType[2]="", pclKey[21];
    char    *pclResult    = NULL;

    sprintf ( pclKey, "CIC,1,%s", pcpRuleUrno );
    dbg(DEBUG,"CheckRuleUrno vor CEDAArrayFindKey Key <%s> IdxRowLen <%ld>",
               pclKey, rgRudArray.lrIdx01RowLen );  
    ilRC = CEDAArrayFindRowPointer( &(rgRudArray.rrArrayHandle),  
                                    rgRudArray.crArrayName,
                                     &(rgRudArray.rrIdx01Handle), "IDXRUD1",
                                     pclKey, &llRow, (void**)&pclResult ); 
    if ( ilRC==RC_SUCCESS )
    {
        dbg ( TRACE, "CheckRuleUrno, Rule <%s> has partial demands, RUD-Row <%ld>", 
                        pcpRuleUrno, llRow );
        ilRC = GetFieldByUrno ( &rgRueArray, pcpRuleUrno, "RUTY", clRuleType, 2 );
        if ( (ilRC==RC_SUCCESS ) && ( clRuleType[0]!='0' ) )    * No Single Rule ? *
        {
            dbg ( TRACE, "CheckRuleUrno: Rule <%s> no single Rule RUTY <%s>", 
                  pcpRuleUrno, clRuleType );
            ilRC = RC_FAIL;
        }
    }
    else
        dbg ( TRACE, "CheckRuleUrno, Rule <%s> has no partial demands.", pcpRuleUrno );
    return ilRC;
}
*/

static int GetFieldByUrno ( ARRAYINFO *prpArray, char *pcpUrno, char *pcpField, 
                              char *pcpResult, int ipMaxBytes )
{
    int     ilRC1 = RC_FAIL;    
    int     ilRC = RC_FAIL;         /* Return code          */
    long llRow = ARR_FIRST ;
    long    llFieldNr = -1;
    char *pclResult = 0;
    int  ilFieldIdx, ilCol, ilPos; 

    if ( prpArray && pcpUrno )
    {
        ilRC1 = CEDAArrayFindRowPointer( &(prpArray->rrArrayHandle), 
                                        prpArray->crArrayName,
                                        &(prpArray->rrIdxUrnoHandle), 
                                        prpArray->crIdxUrnoName, pcpUrno, 
                                        &llRow, (void**)&pclResult ); 
        dbg ( DEBUG, "GetFieldByUrno Field <%s> Urno <%s> llRow <%ld> RC <%d>", 
                 pcpField, pcpUrno, llRow, ilRC1 );  
        if ( ilRC1 == RC_SUCCESS )
        {
            if ( (FindItemInList (prpArray->crArrayFieldList, pcpField, ',', &ilFieldIdx, &ilCol, &ilPos) == RC_SUCCESS)
                 && (ilFieldIdx>0) 
                 && ( prpArray->plrArrayFieldOfs[ilFieldIdx-1] >= 0 ) )
            {
                dbg ( DEBUG, "GetFieldByUrno: ilFieldIdx <%d> Offset <%ld>", ilFieldIdx, 
                      prpArray->plrArrayFieldOfs[ilFieldIdx-1] );
                strncpy ( pcpResult, pclResult+prpArray->plrArrayFieldOfs[ilFieldIdx-1], ipMaxBytes-1 );
                pcpResult[ipMaxBytes-1] = '\0';
                TrimRight(pcpResult);
                ilRC = RC_SUCCESS;
            }
            else
                ilRC = GetCEDAField(&(prpArray->rrArrayHandle), 
                                    prpArray->crArrayName, &llFieldNr, pcpField,
                                    ipMaxBytes, llRow, pcpResult, TRUE );
        }   
        if ( ilRC == RC_SUCCESS )
            dbg(TRACE, "GetFieldByUrno Urno <%s>  Field <%s>: data <%s>", 
                        pcpUrno, pcpField, pcpResult );
        else
            dbg(TRACE, "GetFieldByUrno Urno <%s> Field <%s> : RC1,RC <%d  %d>",
                        pcpUrno, pcpField, ilRC1, ilRC );
    }
    return ilRC;
}
 
static int CreateIdx1 ( ARRAYINFO *prpArrayInfo, char *pcpIdxName,
                        char *pcpFields, char *pcpOrdering )
{
    int     ilRC = RC_FAIL, ilLoop;
    char clOrderTxt[4];
    long    *pllOrdering=0;

    if ( !pcpFields )
        return ilRC;        /*  0-Pointer in parameter pcpFields */

    strcpy ( prpArrayInfo->crIdx01Name, pcpIdxName );
    strcpy ( prpArrayInfo->crIdx01FieldList, pcpFields );
    
    ilRC = GetRowLength( prpArrayInfo->crTableName, pcpFields, 
                         &(prpArrayInfo->lrIdx01RowLen));
    
    if(ilRC != RC_SUCCESS)
        dbg(TRACE,"CreateIdx1: GetRowLength failed <%s> <%s>",
            prpArrayInfo->crTableName, pcpFields);
    else
    {
        prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,
                                       prpArrayInfo->lrIdx01RowLen+1);
        if( prpArrayInfo->pcrIdx01RowBuf == NULL )
        {
            ilRC = RC_FAIL;
            dbg(TRACE,"CreateIdx1: calloc failed");
        }/* end of if */
    }/* end of if */

    if(ilRC == RC_SUCCESS)
    {
        prpArrayInfo->lrIdx01FieldCnt = get_no_of_items(pcpFields);
        pllOrdering = (long *) calloc(prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
        if(!pllOrdering )
        {
            dbg(TRACE,"SetArrayInfo: pllOrdering calloc(%d,%d) failed",prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
            ilRC = RC_FAIL;
        }
        else
        {
            for(ilLoop=0; ilLoop < prpArrayInfo->lrIdx01FieldCnt; ilLoop++)
            {
                if ( !pcpOrdering ||
                     ( get_real_item(clOrderTxt,pcpOrdering,ilLoop+1)!=RC_SUCCESS )
                    )                
                    clOrderTxt[0] = 'D';
                if ( clOrderTxt[0] == 'A' )
                    pllOrdering[ilLoop] = ARR_ASC;
                else
                    pllOrdering[ilLoop] = ARR_DESC;
            }/* end of for */
        }/* end of else */
    }/* end of if */

    ilRC = CEDAArrayCreateMultiIndex( &(prpArrayInfo->rrArrayHandle), 
                                      prpArrayInfo->crArrayName,
                                      &(prpArrayInfo->rrIdx01Handle), 
                                      prpArrayInfo->crIdx01Name,
                                      prpArrayInfo->crIdx01FieldList, 
                                      pllOrdering );
    if ( pllOrdering )
        free (pllOrdering);
    pllOrdering = 0;
    return ilRC;                                    
}


static int GetCEDAField ( HANDLE * pspHandle, char *pcpArrayName,
                          long *plpFields, char *pcpFieldNam,
                          long lpMaxBytes, long lpRowNum,
                          void *pvpDataBuf, int ipTrimRight )
{
    int ilRC;
    ilRC = CEDAArrayGetField ( pspHandle, pcpArrayName, plpFields, pcpFieldNam,
                               lpMaxBytes, lpRowNum, pvpDataBuf );
    if ( ( ilRC == RC_SUCCESS ) && ipTrimRight )
        TrimRight ( pvpDataBuf );
    return ilRC;
}


void TrimRight(char *s)
{
    int i = 0;    /* search for last non-space character */
    for (i = strlen(s) - 1; i >= 0 && isspace(s[i]); i--); /* trim off right spaces */    
    s[++i] = '\0';
}


/******************************************************************************/
/******************************************************************************/
static int GetFieldData(char *pcpTabNam, char *pcpFldNam, char *pcpFldLst, char *pcpData, char **pcpFieldData)
{
    int   ilRC     = RC_SUCCESS ;       /* Return code */
    int   ilPos    = 0 ;
    int   ilCol    = 0 ;
    int   ilItemNo = 0 ;
    long  llLen    = 0 ;
    char  clDel    = ',' ;
    
    if(ilRC == RC_SUCCESS)
    {
        ilRC = FindItemInList (pcpFldLst, pcpFldNam, clDel, &ilItemNo, &ilCol, &ilPos) ;
        if (ilRC == RC_SUCCESS)
            dbg (DEBUG,"GetFieldData: <%s> <%d>", pcpFldNam, ilItemNo) ; 
        ilRC = GetFieldLength (pcpTabNam, pcpFldNam, &llLen) ;
        if (ilRC == RC_SUCCESS)
        {
            dbg (DEBUG, "GetFieldData: len <%d>", llLen) ; 
            llLen++;
            *pcpFieldData = calloc (1, llLen) ;
            if (*pcpFieldData == NULL)
            {
                dbg (TRACE, "GetFieldData: calloc(1,%d) failed", llLen) ;
                ilRC = RC_FAIL ;
            } /* end of if */
            else      /* alloc ok */ 
                ilRC = GetDataItem (*pcpFieldData, pcpData, ilItemNo, clDel, "", "\0\0") ;
            if (ilRC > 0)
                 ilRC = RC_SUCCESS ;
        } /* end of if */
    } /* end of if */
    
    return (ilRC) ;
} /* end of GetFieldData */


void ReplaceChar ( char *pcpStr, char cpOld, char cpNew )
{
    char /* *pclPos,*/ *pclAct;
    pclAct = pcpStr;
    while ( pclAct )
    {
        if ( pclAct = strchr ( pclAct, cpOld ) )
        {
            *pclAct = cpNew;
            pclAct ++;
        }
    }
}
    
static int HandleUFR ( char *pcpFlight, char *pcpFields, char *pcpRueUrnos, 
                       char *pcpProUrno )
{
    int ilRC = RC_FAIL;
    BOOL blNoChanges = FALSE;                                        /*rna*/
    char pclAftUrno[20], pclStod[20];
    char *pclTmpStr;
    char *pclFunc = "HandleUFR";

    ilRC = ResetGlobalTempVars ();
    if ( ilRC != RC_SUCCESS )
    {
        dbg( TRACE, "HandleUFR: ResetGlobalTempVars failed RC <%d>", ilRC );
        return ilRC;
    }

    if ( !pcpFlight || !pcpFields || !pcpRueUrnos || !pcpProUrno )
    {
        dbg ( TRACE, "HandleUFR: At least one parameter = NULL" ); 
        return ilRC;
    }
 
    ilRC = GetFieldData("AFT", "URNO", pcpFields, pcpFlight, &pclTmpStr );
    if ( ilRC == RC_FAIL )
    {
        dbg ( TRACE, "HandleUFR: GetFieldData for AFT.URNO failed, RC: <%d>", ilRC );
        if( pclTmpStr )
            free(pclTmpStr);
        return RC_FAIL;
    }
    strcpy( pclAftUrno, pclTmpStr );
    dbg ( TRACE, "HandleUFR: GetFieldData for AFT.URNO succeeded: <%s>", pclAftUrno );
    if( pclTmpStr )
        free(pclTmpStr);


    /* update entries in SDI and UCD for pclAftUrno */
    ilRC = SelFullDemands ( pcpRueUrnos );
    if ( ilRC != RC_SUCCESS )
    {
        dbg ( TRACE, "HandleUFR: SelFullDemands failed, RueUrnos<%s> RC <%d>", pcpRueUrnos, ilRC );
        return RC_FAIL;
    }

    /*ilRC = CheckChanges( pclAftUrno, pcpProUrno, &blNoChanges );*rna*
    if ( ilRC != RC_SUCCESS )                                    *rna*
        dbg( TRACE,                                              *rna*
            "HandleUFR: CheckChanges failed, RueUrnos<%s> RC <%d>", 
            pcpRueUrnos, ilRC );                                 *rna*
    if ( blNoChanges )                                           *rna*
        return ilRC;                                             *rna*
    else                                                         *rna*
    {                                                            *rna*
        if (bgOpenUFRFLICOL)                                     *rna*
            CloseUFRFLICOL();                                    *rna*
    }*/                                                         /*rna*/


    ilRC = GetFieldData("AFT", "STOD", pcpFields, pcpFlight, &pclTmpStr ); 
    if ( ilRC != RC_SUCCESS )
    {
        dbg ( TRACE, "HandleUFR: GetFieldData for AFT.STOD failed, RC: <%d>", ilRC );
        if( pclTmpStr )
            free(pclTmpStr);
        return RC_FAIL;
    }
    strcpy( pclStod, pclTmpStr );
    if( pclTmpStr )
        free(pclTmpStr);
    dbg ( TRACE, "HandleUFR: GetFieldData for AFT.STOD succeeded: <%s>", pclStod );

    ilRC = UpdateSDITab ( pclAftUrno, pcpProUrno, pclStod );
    if ( ilRC != RC_SUCCESS )
    {
        dbg ( TRACE, "HandleUFR: UpdateSDITab failed for flight <%s> RC <%d>", pclAftUrno, ilRC );
        return RC_FAIL;
    }
    
    ilRC = UpdateUCDTab ( pclAftUrno );
    if ( ilRC != RC_SUCCESS )
        dbg ( TRACE, "HandleUFR: UpdateUCDTab failed for flight <%s> RC <%d>", pclAftUrno, ilRC );
    else
    {                                                               /*rna*/
        if (!bgOpenUFRFLICOL)                                       /*rna*/
            ilRC = SendEvent2DEMCCI( igDest );
    }                                                               /*rna*/
    return ilRC;
}

static int HandleDFR(char *pcpData, char *pcpFields, char *pcpSelection, char *pcpCmd, char *pcpRecvName, char *pcpDestName)
{
    long llUcdRow = ARR_LAST;
    BOOL blIsDFR;
    int ilRC = RC_FAIL;
    int ilTotalUCD;
    int ilMatchUCD;
    char pclOneTplUrno[20];
    char pclRcvTplUrnos[512];
    char pclRudUrno[20];
    char pclKey[128];
    char pclAftUrno[20];
    char pclTmpStr2[128];
    char *pclTmpStr;
    char *pclResult;
    char *pclUrno;
    char *pclFunc = "HandleDFR";

    ilRC = ResetGlobalTempVars();
    if ( ilRC != RC_SUCCESS )
    {
        dbg ( TRACE, "HandleDFR: ResetGlobalTempVars failed RC <%d>", ilRC ); 
        return ilRC;
    }

    blIsDFR = FALSE;
    if( strcmp(pcpRecvName,"FLICOL") || strcmp(pcpDestName,"FLICOL") )
    {
        if( !strcmp(pcpCmd,"DFR") )
            strcpy( pclAftUrno, pcpSelection );
        else
        {
            ilRC = GetFieldData("AFT", "URNO", pcpFields, pcpData, &pclTmpStr);
            strcpy( pclAftUrno, pclTmpStr );
            if( pclTmpStr )
                free(pclTmpStr);
        }
        dbg( TRACE, "%s: This is a Flight <%s> deletion", pclFunc, pclAftUrno );
        if( strlen(pclAftUrno) <= 0 )
        {
            dbg( TRACE, "%s: UAFT was not received. Abort!", pclFunc );
            return RC_FAIL;
        }
        blIsDFR = TRUE;
    }

    /* This is from Coverage (generate demand request), where this flight does not match the required template only */
    if( blIsDFR == FALSE )
    {
        sprintf( pclRcvTplUrnos, ",%s,", pcpSelection );
        ilRC = GetFieldData("AFT", "URNO", pcpFields, pcpData, &pclTmpStr);
        strcpy( pclAftUrno, pclTmpStr );
        if( pclTmpStr )
            free(pclTmpStr);
        if( ilRC == RC_FAIL || (ilRC == RC_SUCCESS && atoi(pclAftUrno) <= 0) )
        {
             dbg( TRACE, "%s: ilRC = %d Fail to get Uaft", pclFunc, ilRC );
             return RC_FAIL;
        }
        dbg( TRACE, "%s: Flight <%s> of template <%s> is recv with no matching rule/profile", 
                     pclFunc, pclAftUrno, pclRcvTplUrnos );

        sprintf ( pclKey, "%s,%s", cgHopo, pclAftUrno );
        dbg( TRACE, "%s: Key to search UCDArray <Hopo,Uaft> <%s>", pclFunc, pclKey );

        ilTotalUCD = 0;
        ilMatchUCD = 0;
        ilRC = RC_SUCCESS;
        llUcdRow = ARR_LAST;

        while ( ilRC==RC_SUCCESS )
        {
            ilRC = CEDAArrayFindRowPointer( &(rgUcdArray.rrArrayHandle),  
                                              rgUcdArray.crArrayName,
                                            &(rgUcdArray.rrIdx01Handle), 
                                              rgUcdArray.crIdx01Name, pclKey, 
                                             &llUcdRow, (void**)&pclResult ); 
            if( ilRC != RC_SUCCESS )
                break;

            ilTotalUCD++;

            pclUrno = pclResult + rgUcdArray.plrArrayFieldOfs[igUcdUrudIdx-1];
            MyStrnCpy ( pclRudUrno, pclUrno, 10, 1 );      
            ilRC = GetFieldByUrno ( &rgRudArray, pclRudUrno, "UTPL", pclOneTplUrno, 21 );
            if( ilRC != RC_SUCCESS )
                continue;

            sprintf( pclTmpStr2, ",%s,", pclOneTplUrno );
            dbg( TRACE, "%s: One UTPL <%s>", pclFunc, pclOneTplUrno );
            if( strstr( pclRcvTplUrnos, pclTmpStr2 ) != NULL )
                ilMatchUCD++;
            llUcdRow = ARR_PREV;
        } /* while loop */

        dbg( TRACE, "%s: TotalUCD = %d MatchUCD = %d", pclFunc, ilTotalUCD, ilMatchUCD );
        if( ilTotalUCD > 0 && ilMatchUCD == 0 )
        {
            dbg( TRACE, "%s: Flight <%s> has counter rules of different template. Do not do anything", pclFunc, pclAftUrno );
            return RC_SUCCESS;
        }
        if( ilMatchUCD == ilTotalUCD )
            blIsDFR = TRUE;
        else
        {
            /* Mixture of template rules */
            /* dun think this will happen,jus delete */
            blIsDFR = TRUE;
        }
    } /* DFR = FALSE */

    if( blIsDFR == FALSE )
        return RC_SUCCESS;
    

    /* Delete entries in SDI and UCD for pclAftUrno */
    /* Just delete, dun need to chk any other thing */
    ilRC = UpdateSDITab ( pclAftUrno, 0, 0 );
    if ( ilRC != RC_SUCCESS )
    {
        dbg ( TRACE, "%s: UpdateSDITab failed for flight <%s> RC <%d>", pclFunc, pclAftUrno, ilRC );
        return RC_FAIL;
    }

    ilRC = UpdateUCDTab ( pclAftUrno );
    if ( ilRC != RC_SUCCESS )
    {
        dbg ( TRACE, "%s: UpdateUCDTab failed for flight <%s> RC <%d>", pclFunc, pclAftUrno, ilRC );
        return RC_FAIL;
    }
    if (!bgOpenUFRFLICOL)                                       /*rna*/
        ilRC = SendEvent2DEMCCI( igDest );

    dbg( TRACE, "%s Completed DFR for UAFT <%s>", pclFunc, pclAftUrno );

    return RC_SUCCESS;
} /* HandleDFR */

/******************************************************************************/
/*                                                                            */
/******************************************************************************/

static int  SetAATArrayInfo(char *pcpArrayName,char *pcpArrayFieldList, long *plpFieldLens, 
                            char *pcpIdx01Name, char *pcpIdx01FieldList, char *pcpIdx01Order, 
                            ARRAYINFO *prpArrayInfo)
{
    int ilRc   = RC_SUCCESS;                /* Return code */
    int ilLoop = 0;
    char clOrderTxt[4];
    long    *pllOrdering=0;

    if(prpArrayInfo == NULL)
    {
        dbg(TRACE,"SetAATArrayInfo: invalid last parameter : null pointer not valid");
        return  RC_FAIL;
    }
    
    memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

    prpArrayInfo->rrArrayHandle = -1;
    prpArrayInfo->rrIdx01Handle = -1;
    prpArrayInfo->rrIdxUrnoHandle = -1;

    if( pcpArrayName && (strlen(pcpArrayName) <= ARR_NAME_LEN) )
        strcpy(prpArrayInfo->crArrayName,pcpArrayName);
    
    if(pcpArrayFieldList && (strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN) )
        strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);

        prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
        if( prpArrayInfo->plrArrayFieldOfs == NULL )
        {
            dbg(TRACE,"SetAATArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
            ilRc = RC_FAIL;
        }/* end of if */
        else
        {
            prpArrayInfo->plrArrayFieldOfs[0] = 0;
            for(ilLoop = 1 ; ilLoop < prpArrayInfo->lrArrayFieldCnt ; ilLoop++)
            {
                prpArrayInfo->plrArrayFieldOfs[ilLoop] = prpArrayInfo->plrArrayFieldOfs[ilLoop -1] + (plpFieldLens[ilLoop-1] +1);
            }/* end of for */
        }
    }/* end of if */


    if(pcpIdx01Name && (strlen(pcpIdx01Name) <= ARR_NAME_LEN) )
        strcpy(prpArrayInfo->crIdx01Name,pcpIdx01Name);

    if( pcpIdx01FieldList && (strlen(pcpIdx01FieldList) <= ARR_FLDLST_LEN) )
        strcpy(prpArrayInfo->crIdx01FieldList,pcpIdx01FieldList);

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
        if(prpArrayInfo->plrArrayFieldLen == NULL)
        {
            dbg(TRACE,"SetAATArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
            ilRc = RC_FAIL;
        }
        else
            *(prpArrayInfo->plrArrayFieldLen) = -1;
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        ilRc = GetLogicalRowLength(pcpArrayFieldList,pcpArrayFieldList,plpFieldLens,&(prpArrayInfo->lrArrayRowLen));
        if(ilRc != RC_SUCCESS)
            dbg(TRACE,"SetAATArrayInfo: GetLogicalRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->lrArrayRowLen++;
        prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+1));
        if(prpArrayInfo->pcrArrayRowBuf == NULL)
        {
            ilRc = RC_FAIL;
            dbg(TRACE,"SetAATArrayInfo: calloc failed");
        }/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        ilRc = GetLogicalRowLength(pcpArrayFieldList,pcpIdx01FieldList,plpFieldLens,&(prpArrayInfo->lrIdx01RowLen));
        if(ilRc != RC_SUCCESS)
            dbg(TRACE,"SetAATArrayInfo: GetLogicalRowLength failed <%s> <%s>",pcpIdx01Name,pcpIdx01FieldList);
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->lrIdx01RowLen++;
        prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx01RowLen+1));
        if(prpArrayInfo->pcrIdx01RowBuf == NULL)
        {
            ilRc = RC_FAIL;
            dbg(TRACE,"SetAATArrayInfo: calloc failed");
        }/* end of if */
    }/* end of if */


    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->lrIdx01FieldCnt = get_no_of_items(pcpIdx01FieldList);
        pllOrdering = (long *) calloc(prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
        if(!pllOrdering )
        {
            dbg(TRACE,"SetAATArrayInfo: pllOrdering calloc(%d,%d) failed",prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
            ilRc = RC_FAIL;
        }
        else
        {
            for(ilLoop=0; ilLoop < prpArrayInfo->lrIdx01FieldCnt; ilLoop++)
            {
                if ( !pcpIdx01Order ||
                     ( get_real_item(clOrderTxt,pcpIdx01Order,ilLoop+1)!=RC_SUCCESS )
                    )                
                    clOrderTxt[0] = 'D';
                if ( clOrderTxt[0] == 'A' )
                    pllOrdering[ilLoop] = ARR_ASC;
                else
                    pllOrdering[ilLoop] = ARR_DESC;
            }/* end of for */
        }/* end of else */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        
        ilRc = AATArrayCreate( &(prpArrayInfo->rrArrayHandle), pcpArrayName,
                               TRUE, 1000, prpArrayInfo->lrArrayFieldCnt,
                               plpFieldLens, pcpArrayFieldList, NULL );
        if(ilRc != RC_SUCCESS)
            dbg(TRACE,"SetAAtArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
    }/* end of if */



    if( (ilRc == RC_SUCCESS) && pcpIdx01Name )
    {
        ilRc = CEDAArrayCreateMultiIndex( &(prpArrayInfo->rrArrayHandle),
                                          pcpArrayName,
                                          &(prpArrayInfo->rrIdx01Handle),
                                          pcpIdx01Name,pcpIdx01FieldList,
                                          pllOrdering  );

        if(ilRc != RC_SUCCESS)
            dbg(TRACE,"SetAAtArrayInfo: CEDAArrayCreateMultiIndexfailed <%d>",ilRc);
    }/* end of if */
 
    if ( (ilRc == RC_SUCCESS) && strstr(pcpArrayFieldList,"URNO") )
    {
        strcpy ( prpArrayInfo->crIdxUrnoName, "URNOIDX" );
        ilRc = CEDAArrayCreateSimpleIndexDown(&(prpArrayInfo->rrArrayHandle),
                                              prpArrayInfo->crArrayName,
                                              &(prpArrayInfo->rrIdxUrnoHandle), 
                                              prpArrayInfo->crIdxUrnoName, "URNO" );
        if(ilRc != RC_SUCCESS)                                             
        {
            dbg( TRACE,"SetAAtArrayInfo: CEDAArrayCreateSimpleIndexDown for %s failed <%d>",
                 pcpArrayName, ilRc);       
        }
    }
    else
        prpArrayInfo->rrIdxUrnoHandle = -1;

    if ( pllOrdering )
        free (pllOrdering);
    pllOrdering = 0;

    return(ilRc);
    
} /* end of SetAAtArrayInfo */


static int GetLogicalRowLength(char *pcpTotalFieldList,char *pcpFieldList, long *plpFieldSizes,long *plpLen)
{
    int  ilRc        = RC_SUCCESS;          /* Return code */
    int  ilNoOfItems = 0;
    int  ilItemNo = 0;
    int  ilLoop      = 0;
    long llRowLen    = 0;
    char clFina[8];
    
    if (pcpTotalFieldList != NULL && pcpFieldList != NULL)
    {
        ilNoOfItems = get_no_of_items(pcpFieldList);

        
        ilLoop = 1;
        do
        {
            get_real_item(clFina,pcpFieldList,ilLoop);
            ilItemNo = get_item_no(pcpTotalFieldList,clFina,strlen(clFina) ); 
            /* if(GetItemNo(clFina,pcpTotalFieldList,&ilItemNo) == RC_SUCCESS) */
            if ( ilItemNo >= 0 )
            {
                llRowLen++;
                llRowLen += plpFieldSizes[ilItemNo];
                dbg(TRACE,"GetLogicalRowLength:  clFina <%s> plpFieldSizes[ilItemNo] <%ld>",clFina,plpFieldSizes[ilItemNo]);
            }

            ilLoop++;
        }while(ilLoop <= ilNoOfItems+1);
    }
    if(ilRc == RC_SUCCESS)
    {
        *plpLen = llRowLen;
    }/* end of if */

    dbg(TRACE,"GetLogicalRowLength:  FieldList <%s> plpLen <%ld>",pcpFieldList,*plpLen);
    return(ilRc);
    
} /* end of GetLogicalRowLength */


/******************************************************************************/
/*  SelFullDemands:                                                           */
/*  Select Rud's of Collective rule for full demands (FFPD=0)                 */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
static int SelFullDemands ( char *pcpRueUrnos )
{
    int     ilRC = RC_FAIL,  ilFound ;
    char    clOneRueUrno[21], clKey[41], clUrnoKey[21]; 
    char    *pclResult=0;
    char    *pclResult2=0;
    long    llRow, llRow2, llRowCnt, llRowsProcessed=0;
    long    llUrnoIdx=-1, llVrefIdx=-1;             /* Indices in AATArray */
    char    *pclUcru = NULL;                        /* READ ONLY !!! */
    int     ilCol, ilPos, ilRC2, ilItem ;

    if ( !pcpRueUrnos )
    {
        dbg ( TRACE, "SelFullDemands: parameter pcpRueUrnos = NULL" ); 
        return ilRC;
    }

    ilRC = RC_SUCCESS;
    ilItem = 1;
    while ( get_real_item(clOneRueUrno,pcpRueUrnos,ilItem ) > 0 )
    {   /*  for all Urnos in List 'pcpRueUrnos' */
        if ( clOneRueUrno[0] == '\0' )
        {
            dbg ( TRACE, "SelFullDemands: %d.Item of Urnolist <%s>", ilItem, clOneRueUrno );
            continue;   
        }
        sprintf ( clKey, "CIC,1,%s", clOneRueUrno );
        dbg ( DEBUG, "SelFullDemands: processing RULE <%s>", clOneRueUrno );
        llRow = ARR_FIRST ;
        do
        {
            ilFound = CEDAArrayFindRowPointer( &(rgRudArray.rrArrayHandle),  
                                               rgRudArray.crArrayName,
                                               &(rgRudArray.rrIdx01Handle), 
                                               rgRudArray.crIdx01Name,
                                               clKey, &llRow, 
                                               (void**)&pclResult ); 
            if ( ilFound ==RC_SUCCESS )
            {
                pclUcru = pclResult + rgRudArray.plrArrayFieldOfs[igRudUcruIdx-1];
                dbg ( TRACE, "SelFullDemands:  found RUD-Urno <%s> for RUE-Urno <%s>",
                      pclUcru, clOneRueUrno );
                if ( pclUcru && pclUcru[0] && (pclUcru[0]!='0' ) )
                {
                    /*strncpy ( clUrnoKey, pclUcru, 10 );
                    TrimRight ( clUrnoKey );*/
                    MyStrnCpy ( clUrnoKey, pclUcru, 10, 1 );
                    /* check whether found UCRU is a valid URNO in RUDTAB */
                    llRow2 = ARR_FIRST;
                    ilRC2 = CEDAArrayFindRowPointer( &(rgRudArray.rrArrayHandle),  
                                                     rgRudArray.crArrayName,
                                                     &(rgRudArray.rrIdxUrnoHandle), 
                                                     rgRudArray.crIdxUrnoName,
                                                     clUrnoKey, &llRow2, 
                                                     (void**)&pclResult2 );
                    if ( ilRC2 != RC_SUCCESS )
                    {
                        dbg ( TRACE, "SelFullDemands: Skip invalid RUD <%s> in UCRU", clUrnoKey );
                    }
                    else 
                    {
                        llRow2 = ARR_FIRST; 
                        ilRC2 =  AATArrayFindRowPointer( &(rgFullDemArray.rrArrayHandle), 
                            rgFullDemArray.crArrayName,
                            &(rgFullDemArray.rrIdxUrnoHandle), 
                            rgFullDemArray.crIdxUrnoName,
                            clUrnoKey, &llRow2, (void**)&pclResult2 );
                        /*  if Urno of full demand not yet in rgFullDemArray */
                        if ( ilRC2 != RC_SUCCESS )  
                        {
                            ilRC = AATArrayAddRowPart( &(rgFullDemArray.rrArrayHandle), 
                                                       rgFullDemArray.crArrayName,
                                                       &llRow2, "URNO", clUrnoKey );
                            if ( ilRC == RC_SUCCESS )
                            {
                                dbg(TRACE,
                                    "SelFullDemands: AATArrayAddRowPart Urno <%s> Row <%ld>",
                                     clUrnoKey, llRow2);
                                AddUCDChanges(clUrnoKey);
                            }
                            else
                                dbg ( TRACE, "SelFullDemands: AATArrayAddRowPart failed: Urno <%s> RC <%d>",
                                      clUrnoKey, ilRC ); 
                        }
                    }
                }
            }
            else
                dbg ( TRACE, "CedaArrayFindRowPointer failed ilItem <%d> RC <%d>",
                ilItem, ilFound );

            llRow = ARR_NEXT;
        }while( (ilFound == RC_SUCCESS) && (ilRC == RC_SUCCESS) );
        ilItem++;
    }
    dbg ( TRACE, "SelFullDemands: %d items of Urnolist processed.", ilItem-1 );

    if ( ilRC ==RC_SUCCESS )
    {   /* fill VREF fields in rgFullDemArray from rgRudArray */
        ilRC = CEDAArrayGetRowCount( &(rgFullDemArray.rrArrayHandle), 
                                     rgFullDemArray.crArrayName, &llRowCnt );
        if ( ilRC ==RC_SUCCESS )
        {
            /*for ( llRow = 1; (llRow<=llRowCnt)&&(ilRC==RC_SUCCESS); llRow++ )*/
            llRow = ARR_FIRST;
            while ( ilRC ==RC_SUCCESS )
            {
                clUrnoKey[0] = '\0';
                ilRC = AATArrayFindKey( &(rgFullDemArray.rrArrayHandle),
                                        rgFullDemArray.crArrayName,
                                        &(rgFullDemArray.rrIdxUrnoHandle), 
                                        rgFullDemArray.crIdxUrnoName,
                                        &llRow, 20, clUrnoKey );
                dbg ( DEBUG, "SelFullDemands: AATArrayFindKey Key <%s> Row<%ld> RC <%d>", 
                      clUrnoKey, llRow, ilRC );
                if ( ilRC == RC_SUCCESS )
                {
                    ilRC = AATArrayGetField(&(rgFullDemArray.rrArrayHandle),
                                            rgFullDemArray.crArrayName, &llUrnoIdx,
                                            "URNO", 11, llRow, clUrnoKey ); 
                    if ( ilRC != RC_SUCCESS )
                        dbg ( TRACE, "SelFullDemands: AATArrayGetField failed llRow <%ld> ilRC <%d>", 
                              llRow, ilRC );
                }
                if ( ilRC == RC_SUCCESS )
                {
                    ilRC = GetFieldByUrno ( &rgRudArray, clUrnoKey, "VREF", 
                                            clKey, 21 );
                    if ( ilRC != RC_SUCCESS )
                    {
                        dbg ( TRACE, "SelFullDemands: GetFieldByUrno failed Urno <%s> ilRC <%d>", 
                              clUrnoKey, ilRC );
                        /* actual RUD found as UCRU in single rule is not valid */
                        /* nevertheless go on processing other RUD's */
                        ilRC = RC_SUCCESS;
                    }
                }
                if ( ilRC == RC_SUCCESS )
                {
                    llVrefIdx = -1; 
                    ilRC = AATArrayPutField( &(rgFullDemArray.rrArrayHandle),
                                             rgFullDemArray.crArrayName, 
                                             &llVrefIdx, "VREF",llRow, TRUE,
                                             clKey );
                    if ( ilRC != RC_SUCCESS )
                        dbg ( TRACE, "SelFullDemands: AATArrayPutField failed llRow <%ld> clKey<%s> ilRC <%d>", 
                              llRow, clKey, ilRC );
                    else
                    {
                        llRowsProcessed++;
                        dbg ( TRACE, "SelFullDemands: %ld.Row URNO<%s> VREF<%s>", 
                              llRowsProcessed, clUrnoKey, clKey );
                    }
                }
                llRow = ARR_NEXT;
            }
            if ( llRowsProcessed >= llRowCnt )
                ilRC = RC_SUCCESS;
        }
        else
            dbg ( TRACE, "SelFullDemands: CEDAArrayGetRowCount failed: RC <%d>", ilRC ); 
    }
    return ilRC;
}

/******************************************************************************/
/******************************************************************************/
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo, int ipMode, int ipLogFile )
{
    int    ilRC       = RC_SUCCESS ;
    long   llRow      = 0 ;
    FILE  *prlFile    = NULL ;
    char   clDel      = ',' ;
    char   clFile[512] ;
    char  *pclTrimBuf = NULL ;
    long llRowCount;
    int ilCount = 0;
    char  *pclTestBuf = NULL ;
    HANDLE  *pslIdxHdl;
    char    *pclIdxName;
    char    clIdxFieldList[ARR_FLDLST_LEN+1];
    char    *pclRowBuff=0;
    int     ilRowLen;

    if ( ipMode == 1 )
    {
        pslIdxHdl = &(prpArrayInfo->rrIdxUrnoHandle);
        pclIdxName = prpArrayInfo->crIdxUrnoName;
        strcpy ( clIdxFieldList, "URNO" );
        ilRowLen = 11;
    }
    else
    {
        pslIdxHdl = &(prpArrayInfo->rrIdx01Handle);
        pclIdxName = prpArrayInfo->crIdx01Name;
        strcpy ( clIdxFieldList, prpArrayInfo->crIdx01FieldList );
        ilRowLen = prpArrayInfo->lrIdx01RowLen;
    }
    pclRowBuff = (char *) calloc(1, ilRowLen+1);
    dbg (TRACE,"SaveIndexInfo: ArrayName <%s>",prpArrayInfo->crArrayName) ;

    if ( *pslIdxHdl==-1) 
    {
        ilRC = RC_FAIL;
        dbg ( TRACE, "SaveIndexInfo: IdxHandle == -1, Mode <%d>", ipMode );
    }
    if ( !pclRowBuff )
    {
        dbg ( TRACE, "SaveIndexInfo: Calloc failed" );
        ilRC = RC_FAIL;
    }
        
    if (ilRC == RC_SUCCESS)
    {
        sprintf (&clFile[0],"%s/%s_%s.info",getenv("DBG_PATH"),mod_name,prpArrayInfo->crArrayName) ;

        errno = 0 ;
        prlFile = fopen (&clFile[0], "w") ;
        if (prlFile == NULL)
        {
            dbg (TRACE, "SaveIndexInfo: fopen <%s> failed <%d-%s>", clFile, 
                  errno, strerror(errno)) ;
            ilRC = RC_FAIL ;
        } /* end of if */
    } /* end of if */

    if (ilRC == RC_SUCCESS)
    {
        if ( ipLogFile )
        {
            dbg (DEBUG, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ;      
            dbg (DEBUG, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ;  
            dbg (DEBUG, "Idx01FieldList <%s>\n", clIdxFieldList) ;  
        }
        fprintf (prlFile, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ; /*fflush(prlFile) ;*/
        fprintf (prlFile, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ; /*fflush(prlFile) ;*/
        fprintf (prlFile, "Idx01FieldList <%s>\n", clIdxFieldList) ; 
        fflush(prlFile) ;

        if ( ipLogFile )
            dbg(DEBUG,"IndexData") ; 
        fprintf(prlFile,"IndexData\n"); fflush(prlFile);

        pclRowBuff[0] = '\0';
        llRow = ARR_FIRST ;
        do
        {
            ilRC = CEDAArrayFindKey ( &(prpArrayInfo->rrArrayHandle), 
                                      prpArrayInfo->crArrayName, pslIdxHdl, 
                                      pclIdxName, &llRow, ilRowLen, pclRowBuff );
            if (ilRC == RC_SUCCESS)
            {
                if ( ipLogFile )
                    dbg(DEBUG, "SaveIndexInfo: Key <%ld> <%s>",llRow,pclRowBuff) ;
    
                if (strlen (pclRowBuff) == 0)
                {
                    ilRC = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
                    if( ipLogFile && (ilRC == RC_SUCCESS) )
                        dbg (DEBUG, "SaveIndexInfo: Row <%ld> <%s>", llRow,prpArrayInfo->pcrArrayRowBuf) ;
                } /* end of if */

                pclTrimBuf = strdup (pclRowBuff) ;
                if (pclTrimBuf != NULL)
                {
                    ilRC = GetDataItem (pclTrimBuf,pclRowBuff, 1, clDel, "", "  ") ;
                    if (ilRC > 0)
                    {
                       ilRC = RC_SUCCESS ;
                       fprintf (prlFile,"Key            <%ld><%s>\n",llRow,pclTrimBuf) ; fflush(prlFile) ;
                    } /* end of if */
                    free (pclTrimBuf) ;
                    pclTrimBuf = NULL ;
                } /* end of if */
                llRow = ARR_NEXT;
            }
            else
            {
                if ( ipLogFile )
                    dbg (TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRC) ;
            }
        } while (ilRC == RC_SUCCESS) ;

        if ( ipLogFile )
            dbg (DEBUG,"ArrayData") ;
    
        fprintf (prlFile,"ArrayData\n"); fflush(prlFile);
        llRow = ARR_FIRST ;

        CEDAArrayGetRowCount(&(prpArrayInfo->rrArrayHandle),prpArrayInfo->crArrayName,&llRowCount);

        llRow = ARR_FIRST;
        if ( ipLogFile )
            dbg(DEBUG,"Array  <%s> data follows Rows %ld", prpArrayInfo->crArrayName,llRowCount);
        do
        {
            ilRC = CEDAArrayGetRowPointer(&(prpArrayInfo->rrArrayHandle),
            &(prpArrayInfo->crArrayName[0]),llRow,(void *)&pclTestBuf);
            if (ilRC == RC_SUCCESS)
            {
                for( ilCount = 0; ilCount < prpArrayInfo->lrArrayFieldCnt; ilCount++)
                {
                
                    fprintf (prlFile,"<%s>",&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]) ; 
                    fflush(prlFile) ;
                    if ( ipLogFile )
                        dbg (DEBUG, "SaveIndexInfo: ilCount <%ld> <%s>", ilCount,&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]) ;
                }
                fprintf (prlFile,"\n") ; 
                if ( ipLogFile )
                    dbg (DEBUG, "\n") ;
                fflush(prlFile) ;
                llRow = ARR_NEXT;
            }
            else
            {
                if ( ipLogFile )
                    dbg(DEBUG,"GetRowPointer failed RC=%d",ilRC);
            }
        } while (ilRC == RC_SUCCESS);

        if (ilRC == RC_NOTFOUND)
            ilRC = RC_SUCCESS ;
    } /* end of if */

    if (prlFile != NULL)
    {
        fclose(prlFile) ;
        prlFile = NULL ;
        if ( ipLogFile )
            dbg (TRACE, "SaveIndexInfo: event saved - file <%s>", clFile) ;
    } /* end of if */
    if ( pclRowBuff )
        free ( pclRowBuff );
    return (ilRC) ;

}/* end of SaveIndexInfo */


static int UpdateUCDTab ( char *pcpAftUrno )
{
    int         ilRC = RC_SUCCESS;
    int         ilFound = RC_SUCCESS;
    int         ilUsed = RC_SUCCESS;
    char        clKey[41], clVref[21] ;
    char        clHopo[8];  
    char        clRudUrno[21];
    char        *pclUrno=0, *pclTmp;
    long        llUcdRow = ARR_LAST;
    long        llRowNew = ARR_LAST;
    int         ilRemaining=0;
    int         ilNew=0;
    int         ilDeleted=0;
    int         ilRC2;
    char *pclUpdSelLst = NULL;
    char *pclUpdFldLst = NULL;
    char *pclUpdDatLst = NULL;
    char *pclUpdCmdLst = NULL;
    long llNumUrnos = 0;
    char  *pclResult=0;

    if ( !pcpAftUrno || !pcpAftUrno[0] )
    {
        dbg (TRACE, "UpdateUCDTab: Invalid parameter AftUrno" );
        return RC_FAIL;
    }
    if ( GetDataItem (clHopo, pcgTwEnd, 1, ',', "", "\0") <= 0 )
        strcpy ( clHopo, cgHopo );  

    sprintf ( clKey, "%s,%s", clHopo, pcpAftUrno );

    while ( (ilRC==RC_SUCCESS) && (ilFound==RC_SUCCESS) )
    {
        ilFound = CEDAArrayFindRowPointer( &(rgUcdArray.rrArrayHandle),  
                                            rgUcdArray.crArrayName,
                                            &(rgUcdArray.rrIdx01Handle), 
                                            rgUcdArray.crIdx01Name, clKey, 
                                            &llUcdRow, (void**)&pclResult ); 
        if ( ilFound == RC_SUCCESS )
        {   /* found record in UCD, check whether record is valid any longer  */
            llRowNew = ARR_LAST;
            pclUrno = pclResult + rgUcdArray.plrArrayFieldOfs[igUcdUrudIdx-1];
            MyStrnCpy ( clRudUrno, pclUrno, 10, 1 );            

            ilUsed = CEDAArrayFindRowPointer( &(rgFullDemArray.rrArrayHandle),  
                                              rgFullDemArray.crArrayName,       
                                              &(rgFullDemArray.rrIdxUrnoHandle),
                                              rgFullDemArray.crIdxUrnoName,     
                                              clRudUrno, &llRowNew,   
                                              (void**)&pclResult ); 

            if ( ilUsed == RC_SUCCESS )
            {   /* this UCD-record is used further on */
                dbg ( TRACE, "UpdateUCDTab: RudUrno <%s> is used further on", pclUrno ); 
                ilRC = AATArrayDeleteRow( &(rgFullDemArray.rrArrayHandle),
                                        rgFullDemArray.crArrayName, llRowNew );
                if ( ilRC != RC_SUCCESS )
                    dbg ( TRACE, "UpdateUCDTab: AATArrayDeleteRow Row<%ld> failed, RC<%d>",
                          llRowNew, ilRC );
                ilRemaining ++;
            }
            else
            {   /* this UCD-record is NOT used any longer */
                dbg ( TRACE, "UpdateUCDTab: RudUrno <%s> is not used any longer", pclUrno );
                ilRC = CEDAArrayDeleteRow ( &(rgUcdArray.rrArrayHandle),  
                                            rgUcdArray.crArrayName, llUcdRow );
                if ( ilRC != RC_SUCCESS )
                    dbg ( TRACE, "UpdateUCDTab: CEDAArrayDeleteRow Row<%ld> failed, RC<%d>",
                          llUcdRow, ilRC );
                ilDeleted ++;
                if ( ilRC == RC_SUCCESS )
                {
                    ilRC2 = AddUCDChanges(clRudUrno);
                    if ( ilRC2 != RC_SUCCESS )
                        dbg ( TRACE, "UpdateUCDTab: AddUCDChanges for RUD <%s> failed, RC<%d>", 
                              clRudUrno, ilRC2 );
                }
            }
            llUcdRow = ARR_PREV;
        }
    }
    /*  for all rows remaining in rgFullDemArray create an entry in UCDTAB */
    ilFound = RC_SUCCESS;
    llRowNew = ARR_FIRST;           
    clKey[0] = '\0';
    while ( (ilRC==RC_SUCCESS) && (ilFound==RC_SUCCESS) )
    {
        ilFound = CEDAArrayFindRowPointer( &(rgFullDemArray.rrArrayHandle),  
                                            rgFullDemArray.crArrayName,
                                            &(rgFullDemArray.rrIdxUrnoHandle), 
                                            rgFullDemArray.crIdxUrnoName, clKey,
                                            &llRowNew, (void**)&pclResult );
        if ( ilFound == RC_SUCCESS )
        {
            MyStrnCpy ( clRudUrno, pclResult, 10, 1 );          
            ilRC = FillUCDRecord ( pcpAftUrno, clRudUrno, clHopo, cgNewRecBuf, 
                                   sizeof(cgNewRecBuf) );
            if ( ilRC == RC_SUCCESS )
            {
                dbg(TRACE, "UpdateUCDTab: FillUCDRecord successful RC<%d>",
                    ilRC);
                llUcdRow = ARR_FIRST;
                ilRC = CEDAArrayAddRow( &rgUcdArray.rrArrayHandle,
                                        rgUcdArray.crArrayName, &llUcdRow,
                                        (void *)cgNewRecBuf);
                if ( ilRC == RC_SUCCESS )
                {
                    ilNew ++;
                    pclTmp = pclResult + rgFullDemArray.plrArrayFieldOfs[1];
                    MyStrnCpy ( clVref, pclTmp, 10, 1 );            
                    ilRC2 = KeepChangedUCD ( clRudUrno, clVref, pcpAftUrno, 
                                             clHopo );
                    if ( ilRC2 != RC_SUCCESS )
                        dbg ( TRACE, "UpdateUCDTab: KeepChangedUCD failed, RC<%d>",
                              ilRC2 );
                }

                else
                    dbg ( TRACE, "UpdateUCDTab: CEDAArrayAddRow failed for RUD <%s> AFT<%s>, RC <%d>",
                          clRudUrno, pcpAftUrno, ilRC );
            }
        }
        llRowNew = ARR_NEXT;
    }
    if ( ilRC != RC_SUCCESS )
        dbg ( TRACE, "UpdateUCDTab: Error occured, nothing changed in UCDTAB" );
    else
    {
        dbg ( TRACE, "UpdateUCDTab added %d, deleted %d, didn't change %d records",
            ilNew, ilDeleted, ilRemaining );

        ilRC = CEDAArrayWriteDBGetChanges( &rgUcdArray.rrArrayHandle,
                                          rgUcdArray.crArrayName, &pclUpdCmdLst,
                                          &pclUpdSelLst, &pclUpdFldLst, 
                                          &pclUpdDatLst,&llNumUrnos,
                                          ARR_COMMIT_NONE_IF_ERR);
        if ( ilRC != RC_SUCCESS )
            dbg ( TRACE, "UpdateUCDTab: CEDAArrayWriteDBGetChanges failed RC <%d>", ilRC );
        else
        {
            dbg(TRACE,"CEDAArrayWriteDBGetChanges succeeded: Urnos: %ld Sel: <%s>, Fld: <%s>,Dat: <%s>",
                llNumUrnos,pclUpdSelLst,pclUpdFldLst,pclUpdDatLst);
            commit_work();
        }
    }
    return ilRC;
}

static int StrToTime(char *pcpTime,time_t *plpTime)
{
    struct tm *_tm;
    time_t  now;
    char    _tmpc[6];

    dbg(0,"StrToTime, <%s>",pcpTime);
    if (strlen(pcpTime) < 12 )
    {
        /**plpTime = time(0L);*/
        return RC_FAIL;
    }

    now = time(0L);
    _tm = (struct tm *)gmtime(&now);

    _tmpc[2] = '\0';
    _tm -> tm_sec = 0;
    
    strncpy(_tmpc,pcpTime+10,2);
    _tm -> tm_min = atoi(_tmpc);
    strncpy(_tmpc,pcpTime+8,2);
    _tm -> tm_hour = atoi(_tmpc);
    strncpy(_tmpc,pcpTime+6,2);
    _tm -> tm_mday = atoi(_tmpc);
    strncpy(_tmpc,pcpTime+4,2);
    _tm -> tm_mon = atoi(_tmpc)-1;
    strncpy(_tmpc,pcpTime,4);
    _tmpc[4] = '\0';
    _tm ->tm_year = atoi(_tmpc)-1900;
    _tm ->tm_wday = 0;
    _tm ->tm_yday = 0;
    _tm->tm_isdst = -1; /* no adjustment of daylight saving time */
    now = mktime(_tm);
    dbg(0,"StrToTime: %02d.%02d.%04d %02d:%02d Now: %ld",_tm->tm_mday,
        _tm->tm_mon+1,_tm->tm_year+1900,_tm->tm_hour,_tm->tm_min,now);
    
    if (now != (time_t) -1)
    {
        /*{
            time_t utc,local;

            _tm = (struct tm *)gmtime(&now);
            utc = mktime(_tm);
            _tm = (struct tm *)localtime(&now);
            local = mktime(_tm);
            dbg(DEBUG,"utc %ld, local %ld, utc-diff %ld",
            utc,local,local-utc);
            llUtcDiff = local-utc;
        }
        now +=  + llUtcDiff;*/
        *plpTime = now;
        return RC_SUCCESS;
    }
    *plpTime = time(NULL);
    return RC_FAIL;
}

int TimeToStr(char *pcpTime,time_t lpTime) 
{                                                                               
    struct tm *_tm;
    /*time_t now;
    char   _tmpc[6];*/
    
    _tm = (struct tm *)localtime(&lpTime);
    sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
            _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
            _tm->tm_min,_tm->tm_sec);
    return (_tm->tm_wday == 0 ? 7 : _tm->tm_wday) + '0';                          
}                                                                               

static int FillUCDRecord ( char *pcpUAFT, char *pcpURUD, char *pcpHopo,
               char *pcpRecBuf, int ipBuflen )
{
    char    clUrno[21], clNow[21];
    char    *pclTmpBuf=0, *pclPtr2Rec;
    int     ilIdx,ilRC=RC_FAIL;

    dbg ( TRACE, "FillUCDRecord: UAFT<%s> URUD<%s> HOPO<%s>", pcpUAFT, pcpURUD, pcpHopo );
    if ( !pcpUAFT || !pcpURUD || !pcpRecBuf || !pcpHopo )
    {
        dbg ( TRACE, "FillUCDRecord: At least one parameter is NULL" );
        return RC_FAIL ;
    }
    pclTmpBuf = calloc ( 1, ipBuflen );
    if ( !pclTmpBuf )
    {
        dbg ( TRACE, "FillUCDRecord: calloc failed !" );
    }
    else
    {
        memset ( pclTmpBuf, 0, ipBuflen );
        memset ( pcpRecBuf, 0, ipBuflen );
        ilRC = GetNextUrno(clUrno);
        if ( ilRC != RC_SUCCESS )
            dbg ( TRACE, "FillUCDRecord: GetNextUrno failed RC<%d>", ilRC );
    }
    if ( ilRC==RC_SUCCESS ) 
    {
        GetServerTimeStamp("UTC", 1, 0, clNow);
        
        strcpy ( UCDFIELD(pclTmpBuf,igUcdCdatIdx), clNow ) ;
        strcpy ( UCDFIELD(pclTmpBuf,igUcdHopoIdx), pcpHopo ) ;
        strcpy ( UCDFIELD(pclTmpBuf,igUcdLstuIdx), " " );
        strcpy ( UCDFIELD(pclTmpBuf,igUcdUaftIdx), pcpUAFT );
        strcpy ( UCDFIELD(pclTmpBuf,igUcdUrnoIdx), clUrno ) ;
        strcpy ( UCDFIELD(pclTmpBuf,igUcdUrudIdx), pcpURUD ) ;
        strcpy ( UCDFIELD(pclTmpBuf,igUcdUsecIdx), "DISCAL" );
        strcpy ( UCDFIELD(pclTmpBuf,igUcdUseuIdx), " " );

        pclPtr2Rec = pcpRecBuf; 
        for ( ilIdx=1; ilIdx<= rgUcdArray.lrArrayFieldCnt; ilIdx++) 
        {   
            strcpy( pclPtr2Rec, UCDFIELD(pclTmpBuf,ilIdx) );    
            pclPtr2Rec += strlen(pclPtr2Rec);
            *pclPtr2Rec = '|';
            pclPtr2Rec ++;
        }
        dbg ( TRACE, "FillUCDRecord: Buffer Size<%d> Content<%s>", ipBuflen, pcpRecBuf ); 
        ReplaceChar(pcpRecBuf,'|', '\0'); 
        /* DebugPrintRowInfo ( DEBUG, pcpRecBuf, &rgUcdArray ); */
    }
    if ( pclTmpBuf )
        free ( pclTmpBuf );
    return ilRC;
}

static int CalcSDIData ( char *pcpPROUrno, char *pcpCCCUrno, char *pcpHopo, 
                         char *pcpAFTUrno, char *pcpStod, SDIREC *prpRec )
{
    char    clKey[41];
    long    llRow;
    int     ilRC, ilPos, ilConv, ilProcessed=0;
    char    clUPXA[21]="", *pclResult=0;
    char    *pclTmp;
    int     ilPAXInClass, ilPENR, ilPBER, ilTimes, ilStart, ilLoop, ilFound;
    float   flValue, flAbsolut, flRest=0.0, *pflData=0 ;
    int     ilValues, ilIdx, ilLastIdx=-1;

    if ( !pcpPROUrno || !pcpCCCUrno || !pcpHopo || !prpRec || !pcpAFTUrno )
    {
        dbg ( TRACE, "CalcSDIData: At least one parameter = NULL" );
        return RC_FAIL ;
    }
    dbg ( TRACE, "CalcSDIData: Input Par. UPRO <%s> UCCC <%s> HOPO <%s> UAFT <%s>",
          pcpPROUrno, pcpCCCUrno, pcpHopo, pcpAFTUrno );

    /*  how many integer values fit into prpRec->crDATA ? 
        We need 3 char for value plus separator (comma)     */
    ilValues = SDI_DATA_LEN/4;
    if  ( ilValues > 0 )
        pflData = (float*) calloc(ilValues,sizeof(float)); 
    if ( !pflData )
    {
        dbg ( TRACE, "CalcSDIData: calloc failed" );
        return RC_FAIL ;
    }
    else
        memset ( pflData, 0, ilValues*sizeof(float) );

    sprintf ( clKey, "%s,%s,%s", pcpPROUrno, pcpHopo, pcpCCCUrno );
    llRow = ARR_FIRST;
    ilRC = CEDAArrayFindRowPointer( &(rgPxcArray.rrArrayHandle),  
                                    rgPxcArray.crArrayName,
                                     &(rgPxcArray.rrIdx01Handle), 
                                     rgPxcArray.crIdx01Name,
                                     clKey, &llRow, (void**)&pclResult ); 
    if ( ilRC != RC_SUCCESS )
        dbg ( TRACE, "CalcSDIData: Didn't find PXC-Record for key <%s>", clKey );
    else
    {
        ilRC = GetPAXCount ( pcpAFTUrno, pcpPROUrno, pcpCCCUrno, pcpHopo,
                             pcpStod, &ilPAXInClass );                  /*rna*/
        if ( ilRC != RC_SUCCESS )
            dbg ( TRACE, "CalcSDIData: GetPAXCount failed RC <%d> UAFT <%s> UPRO <%s> UCCC <%s>", 
                ilRC, pcpAFTUrno, pcpPROUrno, pcpCCCUrno );
    }
    
    if ( ilRC == RC_SUCCESS )
    {
        pclTmp = pclResult + rgPxcArray.plrArrayFieldOfs[igPxcUpxaIdx-1];
        if ( pclTmp )
            MyStrnCpy ( clUPXA, pclTmp, 10, 1 );            
        
        pclTmp = pclResult + rgPxcArray.plrArrayFieldOfs[igPxcDbagIdx-1];
        if ( pclTmp )
            MyStrnCpy ( prpRec->crBAG, pclTmp, 5, 1 );          
        dbg ( TRACE, "CalcSDIData: Found UPXA <%s>, DBAG <%s> for key <%s>", 
              clUPXA, prpRec->crBAG, clKey );
    }
    

    if ( ilRC == RC_SUCCESS )
    {   /* process all VTP-records with VTP.UVAL=clUPXA */
        llRow = ARR_FIRST ;
        sprintf ( clKey, "PXA,%s,%s", clUPXA, pcpHopo ); /* "TABN,UVAL,HOPO" */
        do
        {
            ilFound = CEDAArrayFindRowPointer( &(rgVtpArray.rrArrayHandle),  
                                               rgVtpArray.crArrayName,
                                               &(rgVtpArray.rrIdx01Handle), 
                                               rgVtpArray.crIdx01Name,
                                               clKey, &llRow, 
                                               (void**)&pclResult ); 
            if ( ilFound == RC_SUCCESS ) 
            {
                /* get value */
                pclTmp = pclResult + rgVtpArray.plrArrayFieldOfs[igVtpValuIdx-1];
                ilConv = sscanf(pclTmp, "%f", &flValue );
                /* get relative period begin */
                pclTmp = pclResult + rgVtpArray.plrArrayFieldOfs[igVtpPberIdx-1];
                ilConv += sscanf(pclTmp, "%d", &ilPBER );
                /* get relative period end */
                pclTmp = pclResult + rgVtpArray.plrArrayFieldOfs[igVtpPenrIdx-1];
                ilConv += sscanf(pclTmp, "%d", &ilPENR );
                if ( ilConv < 3 )   /* not all values could be extracted */
                    dbg ( TRACE, "CalcSDIData: Not all Values <VALU,PBER,PENR> ok in VTP-Row <%ld>", llRow );
                else
                {
                    if ( igExtendedTracing ) 
                        dbg ( DEBUG, "CalcSDIData: Split Record PBER <%d> PENR<%d> VALU <%.3lf> into:",
                              ilPBER, ilPENR, flValue );
                    ilPBER /= 60;
                    ilPENR /= 60;
                    ilTimes = ( ilPENR - ilPBER ) / igStepWidth;
                    flValue /= (float)ilTimes;
                    flAbsolut = (float)ilPAXInClass * flValue / 100.0;
                    for ( ilLoop=0; ilLoop<ilTimes; ilLoop++ )
                    {
                        ilStart = ilPBER + ilLoop*igStepWidth;
                        if ( igExtendedTracing ) 
                            dbg ( DEBUG, "CalcSDIData: %2d.Entry: Start %5d PAX %.2lf", 
                                  ilLoop+1, ilStart, flAbsolut );
                        /* insert into array */
                        ilIdx = abs(ilStart) / igStepWidth  - 1;
                        if ( (ilIdx < 0) || (ilIdx>=ilValues) )
                            dbg ( TRACE, "CalcSDIData: Calculated invalid index <%d> for ilStart <%d>",
                                  ilIdx, ilStart );
                        else
                        {
                            pflData[ilIdx] = flAbsolut;
                            ilLastIdx = FMAX ( ilLastIdx, ilIdx );
                        }       
                    }      
                }
                ilProcessed++;
            }
            llRow = ARR_NEXT;
        }while( ilFound == RC_SUCCESS );

        ilLoop = 0;     /*  used to check sum of PAX */
        for ( ilIdx = 0; ilIdx <= ilLastIdx; ilIdx++ )
        {
            ilPos = ilIdx*4;
            if ( ilPos > SDI_DATA_LEN-4 )
                dbg ( TRACE, "CalcSDIData: Calculated invalid string position <%d> Value Nr <%d>",
                      ilPos, ilIdx );
            else
            {
                ilConv = (int)(pflData[ilIdx] + flRest + EPS ); /* to avoid rounding error */
                flRest += pflData[ilIdx] - (float)ilConv;
                sprintf ( &(prpRec->crDATA[ilPos]), "%3d|", ilConv );
                if ( igExtendedTracing )
                    dbg ( DEBUG, "CalcSDIData: %d.Line Val: %5.2lf Ganzz. %d Rest %4.2lf",
                          ilIdx+1, pflData[ilIdx], ilConv,  flRest );
                ilLoop += ilConv;   
            }
        }
        if ( ilLastIdx > -1 )
            prpRec->irStart = - (ilLastIdx+1) * igStepWidth ;
        else
            prpRec->irStart = 0;
        sprintf ( prpRec->crSTEP, "%d", igStepWidth );

        dbg ( TRACE, "CalcSDIData: Processed %d lines of VTP, Sum of PAX <%d>, PAX in class <%d>", 
                     ilProcessed, ilLoop, ilPAXInClass );
        dbg ( TRACE, "CalcSDIData: Result String <%s>", prpRec->crDATA );
    }
    /*  free memory */
    if ( pflData )
        free ( pflData );
    return ilRC;
}

void MyStrnCpy ( char *pcpDest, const char *pcpSource, size_t ipCount, int ipTrimRight )
{
    strncpy ( pcpDest, pcpSource, ipCount );
    pcpDest[ipCount]='\0';
    if ( ipTrimRight )
        TrimRight ( pcpDest );
}

static int UpdateSDITab ( char *pcpAftUrno, char *pcpProUrno, char *pcpStod )
{
    int     ilRC=RC_SUCCESS, ilFound = RC_SUCCESS, ilProcessed=0;
    char    clKey[41], clVref[21], *pclTmp;
    long    llRow, llNewSDIRow;
    char    clHopo[8];  
    char    *pclResult=0, *pclNewSDIRes=0;
    int     ilAlreadyCalc, ilRC2, ilSDIOk = 1;
    SDIREC  rlSDIData;
            
    if ( !pcpAftUrno || !pcpAftUrno[0] )
    {
        dbg (TRACE, "UpdateSDITab: Invalid parameter AftUrno" );
        return RC_FAIL;
    }

    memset ( &rlSDIData, 0, sizeof(SDIREC) );

    if ( GetDataItem (clHopo, pcgTwEnd, 1, ',', "", "\0") <= 0 )
        strcpy ( clHopo, cgHopo );  

    dbg(TRACE, "UpdateSDITab: before CEDAArrayDelete "); 
    ilRC = CEDAArrayDelete( &(rgNewSdiArr.rrArrayHandle), 
                            rgNewSdiArr.crArrayName );
    dbg(TRACE, "UpdateSDITab: after CEDAArrayDelete "); 
    if ( ilRC != RC_SUCCESS )
    {
        dbg ( TRACE, "UpdateSDITab: CEDAArrayDelete failed RC <%d>", ilRC ); 
        return ilRC;
    }

    llRow = ARR_FIRST;          
    clKey[0] = '\0';
        
    /* if we didn't receive a Profile-Urno, we don't have to calculate SDI's */
    while ( pcpProUrno && pcpStod && (ilRC==RC_SUCCESS) && (ilFound==RC_SUCCESS) )
    {   /*  for all rows in rgFullDemArray calculate Single Distribution */

        ilFound = CEDAArrayFindRowPointer( &(rgFullDemArray.rrArrayHandle),  
                                            rgFullDemArray.crArrayName,
                                            &(rgFullDemArray.rrIdx01Handle), 
                                            rgFullDemArray.crIdx01Name, clKey, 
                                            &llRow, (void**)&pclResult );
        ilSDIOk = 1;
        if ( ilFound == RC_SUCCESS )
        {
            pclTmp = pclResult + rgFullDemArray.plrArrayFieldOfs[1];
            MyStrnCpy ( clVref, pclTmp, 10, 1 );            
            dbg ( TRACE, "UpdateSDITab: found row VREF <%s> Result <%s>", clVref, pclResult );
        
            /*  check whether SDI for this counter class has been already
                calculated during actual event */
            llNewSDIRow = ARR_FIRST;
            ilAlreadyCalc = CEDAArrayFindRowPointer( &(rgNewSdiArr.rrArrayHandle),  
                                            rgNewSdiArr.crArrayName,
                                            &(rgNewSdiArr.rrIdx01Handle), 
                                            rgNewSdiArr.crIdx01Name, clVref, 
                                            &llNewSDIRow, (void**)&pclNewSDIRes ); 
            if ( ilAlreadyCalc == RC_SUCCESS )
                dbg ( TRACE, "UpdateSDITab: Skipped counter class <%s>", clVref );
            else
            {
                ilRC = CalcSDIData( pcpProUrno, clVref, clHopo, pcpAftUrno, 
                                    pcpStod, &rlSDIData );
                if ( ilRC != RC_SUCCESS )
                {
                    dbg ( TRACE, "UpdateSDITab: CalcSDIData failed RC <%d>", ilRC );
                    ilSDIOk = 0;
                }
                else
                {   /* insert record into cgNewRecBuf */
                    memset ( cgNewRecBuf, 0, sizeof(cgNewRecBuf) );
                    llNewSDIRow = ARR_FIRST;
                    ilRC = CEDAArrayAddRow( &rgNewSdiArr.rrArrayHandle,
                                            rgNewSdiArr.crArrayName, &llNewSDIRow,
                                            (void *)cgNewRecBuf);
                    if ( ilRC != RC_SUCCESS )
                        dbg ( TRACE, "UpdateSDITab: CEDAArrayAddRow failed RC <%d>", ilRC );
                }
                if ( ilRC == RC_SUCCESS )
                {
                    strcpy ( rlSDIData.crPBEA, pcpStod );
                    ilRC2 = AddSecondsToCEDATime(rlSDIData.crPBEA, rlSDIData.irStart*60, 1 );

                    dbg ( DEBUG, "UpdateSDITab: STOD <%s> Offset <%d> min -> PBEA <%s> with RC<%d>",
                          pcpStod, rlSDIData.irStart, rlSDIData.crPBEA, ilRC2 );
                
                    sprintf ( cgNewRecBuf, "%s,%s,%s,%s,%s,%s,%s,%s", clVref, 
                              rlSDIData.crDATA, rlSDIData.crSTEP, 
                              rlSDIData.crBAG, pcpStod, rlSDIData.crPBEA, 
                              pcpAftUrno, pcpProUrno );
                    ilRC = CEDAArrayPutFields( &rgNewSdiArr.rrArrayHandle,
                                rgNewSdiArr.crArrayName, 0, &llNewSDIRow, 
                                "UCCC,DATA,STEP,DBAG,PENA,PBEA,UAFT,UPRO", 
                                cgNewRecBuf );  
                    if ( ilRC != RC_SUCCESS )
                        dbg ( TRACE, "UpdateSDITab: CEDAArrayPutFields failed RC <%d>", ilRC );
                }
                /*  Although PAX-calculation for one Counter class Failed, 
                    we shall go on                                          */
                ilRC = RC_SUCCESS;

            }
            ilProcessed++;
        }
        if ( ilSDIOk )
            llRow = ARR_NEXT; 
        else
        {
            ilRC2 = AATArrayDeleteRow( &(rgFullDemArray.rrArrayHandle),
                                        rgFullDemArray.crArrayName, llRow );
            if ( ilRC2 != RC_SUCCESS )
            {
                dbg ( TRACE, "UpdateUCDTab: AATArrayDeleteRow Row<%ld> failed, RC<%d>",
                          llRow, ilRC2 );
                llRow = ARR_NEXT;
            }
            else
            {
                llRow = ARR_CURRENT;
                dbg ( DEBUG, "UpdateUCDTab: Deleted RUD-Urno <%s> successfully from rgFullDemArray",
                      pclResult );
            }
        }
    }
    dbg ( TRACE, "UpdateSDITab: Processed %d lines of rgFullDemArray", ilProcessed );

    if ( ilRC == RC_SUCCESS )
    {   
        ilRC = SaveSDIChanges(clHopo, pcpAftUrno);
        if ( ilRC != RC_SUCCESS )
            dbg ( TRACE, "UpdateSDITab: SaveSDIChanges failed RC <%d>", ilRC );
    }
    return ilRC;
}

static int SaveSDIChanges(char *pcpHopo, char *pcpAftUrno)
{
    int     ilRC = RC_SUCCESS;
    int     ilRC2;
    int     ilEqual;
    long    llSdiArrayRow = ARR_LAST;
    long    llNewSdiArrRow = ARR_LAST;
    long    llNumUrnos = 0;
    long    llRowsToCopy = 0;
    char    *pclSdiArrayResult = NULL;
    char    *pclNewSdiArrResult = NULL;
    char    *pclUpdCmdLst = NULL;
    char    *pclUpdSelLst = NULL;
    char    *pclUpdFldLst = NULL;
    char    *pclUpdDatLst = NULL;
    char    clSdiArrayKey[HOPOLEN+1+URNOLEN+1];
    char    clNewSdiArrKey[URNOLEN+1+URNOLEN+1];
    char    clEmpty[URNOLEN+1] = "";
    char    clUccc[URNOLEN+1];
    char    clUrno[URNOLEN+1];
    char    clNow[DATELEN+1];
    char    clPbea[DATELEN+1];
    char    clPena[DATELEN+1];

    sprintf(clSdiArrayKey, "%s,%s", pcpHopo, pcpAftUrno);
    dbg(TRACE, "SaveSDIChanges: Key for SDI <%s>", clSdiArrayKey);

    while(CEDAArrayFindRowPointer(&(rgSdiArray.rrArrayHandle),  
                                    rgSdiArray.crArrayName,
                                    &(rgSdiArray.rrIdx01Handle), 
                                    rgSdiArray.crIdx01Name, clSdiArrayKey,
                                    &llSdiArrayRow,
                                    (void**)&pclSdiArrayResult) == RC_SUCCESS)
    {
        /* remember begin and end of interval*/
        strcpy ( clPbea, SDIFIELD(pclSdiArrayResult, igSdiPbeaIdx) );
        strcpy ( clPena, SDIFIELD(pclSdiArrayResult, igSdiPenaIdx) );
        CheckInterval(clPbea, clPena);

        strcpy ( clUccc, SDIFIELD(pclSdiArrayResult, igSdiUcccIdx) );
        sprintf(clNewSdiArrKey, "%s,%s", clUccc, pcpAftUrno);
        llNewSdiArrRow = ARR_LAST;

        ilEqual = 0;
        while((CEDAArrayFindRowPointer(&(rgNewSdiArr.rrArrayHandle),  
                                        rgNewSdiArr.crArrayName,
                                        &(rgNewSdiArr.rrIdx01Handle), 
                                        rgNewSdiArr.crIdx01Name, clNewSdiArrKey,
                                        &llNewSdiArrRow,
                                        (void**)&pclNewSdiArrResult)
                                            == RC_SUCCESS) && !ilEqual)
        {
            ilRC = CompareSDIFields(pclSdiArrayResult, pclNewSdiArrResult,
                                    &ilEqual);
            if ((ilRC == RC_SUCCESS) && ilEqual)
            {/*delete in new array because data are already in old array*/

                CEDAArrayDeleteRow(&(rgNewSdiArr.rrArrayHandle),
                                       rgNewSdiArr.crArrayName, llNewSdiArrRow);
            }
            llNewSdiArrRow = ARR_PREV;
        }
        if (!ilEqual)
        {/*delete in old array because data are no longer needed*/

            CEDAArrayDeleteRow(&(rgSdiArray.rrArrayHandle),
                               rgSdiArray.crArrayName, llSdiArrayRow);
        }

        llSdiArrayRow = ARR_PREV;
    }

    CEDAArrayGetRowCount(&(rgNewSdiArr.rrArrayHandle), 
                          rgNewSdiArr.crArrayName, &llRowsToCopy);
    if (llRowsToCopy > 0)
    {
        GetServerTimeStamp("UTC", 1, 0, clNow);
        llNewSdiArrRow = ARR_FIRST;
        while(CEDAArrayFindRowPointer(&(rgNewSdiArr.rrArrayHandle),  
                                        rgNewSdiArr.crArrayName,
                                        &(rgNewSdiArr.rrIdx01Handle), 
                                        rgNewSdiArr.crIdx01Name, clEmpty,
                                        &llNewSdiArrRow,
                                        (void**)&pclNewSdiArrResult)
                                            == RC_SUCCESS)
        {
            ilRC = GetNextUrno(clUrno);
            sprintf(cgNewRecBuf, "%s,%s,%s,DISCAL", clNow, pcpHopo, clUrno);
            ilRC = CEDAArrayPutFields(&rgNewSdiArr.rrArrayHandle,
                                        rgNewSdiArr.crArrayName, 0, 
                                        &llNewSdiArrRow, "CDAT,HOPO,URNO,USEC",
                                        cgNewRecBuf);   
            strcpy ( clPbea, NEWSDIFIELD(pclNewSdiArrResult, igSdiPbeaIdx) );
            strcpy ( clPena, NEWSDIFIELD(pclNewSdiArrResult, igSdiPenaIdx) );
            CheckInterval(clPbea, clPena);
            llNewSdiArrRow = ARR_NEXT;
        }
        ilRC = CopyNewSdiRecords();
        if ( ilRC != RC_SUCCESS ) 
        {
            dbg(TRACE, "SaveSDIChanges: CopyNewSdiRecords failed RC <%d>", ilRC);
        }   
    }
    else
        ilRC = RC_SUCCESS;

    ilRC2 = CEDAArrayWriteDBGetChanges(&rgSdiArray.rrArrayHandle,
                                        rgSdiArray.crArrayName, &pclUpdCmdLst,
                                        &pclUpdSelLst, &pclUpdFldLst,
                                        &pclUpdDatLst, &llNumUrnos,
                                        ARR_COMMIT_NONE_IF_ERR);
    if ((ilRC2 != RC_SUCCESS) && (ilRC2 != RC_NOTFOUND))
    {
        dbg(TRACE, "SaveSDIChanges: CEDAArrayWriteDBGetChanges for rgSdiArray RC <%d>",
            ilRC2);
    }
    commit_work();

    return ilRC;
}

static int CompareSDIFields( char *pclNewData, char *pclOldData, int *iplEqual )
{
    char clCmpFields[] = "DATA,DBAG,PBEA,PENA,STEP,UPRO" ;
    char clFina[8];
    char *pclNewVal=0, *pclOldVal=0;
    int  ilRC=RC_SUCCESS, ilLoop, ilNoOfItems, ilLen, ilDiff=0 ;
    int  ilFieldIdx, ilCol, ilPos; 
    char clOldOut[101], clNewOut[101];

    if ( !pclNewData || !pclOldData || !iplEqual )
    {
        dbg ( TRACE, "CompareSDIFields: At least one parameter = NULL" ); 
        return RC_FAIL;
    }
    *iplEqual = 0; /* initialize */

    ilNoOfItems = get_no_of_items(clCmpFields);
    ilLoop = 1;
    do
    {
        ilLen = get_real_item( clFina,clCmpFields,ilLoop);
        if(ilLen > 0)
        {
            ilRC = FindItemInList( SDI_FIELDS, clFina, ',', &ilFieldIdx, 
                                   &ilCol,&ilPos);
            if ( ilRC!=RC_SUCCESS )
                dbg ( TRACE, "CompareSDIFields: Unable to find field <%s>  in Fieldlist <%s>",
                      clFina, SDI_FIELDS );
            pclOldVal = pclOldData + rgSdiArray.plrArrayFieldOfs[ilFieldIdx-1];         
            pclNewVal = pclNewData + rgNewSdiArr.plrArrayFieldOfs[ilFieldIdx-1];        
            if ( (ilRC==RC_SUCCESS) && pclNewVal && pclOldVal )
            {   /* everything ok ! */
                ilDiff = strcmp( pclNewVal, pclOldVal );

                /*  perhaps strings are too long for debug-output */  
                MyStrnCpy ( clOldOut, pclOldVal, 100, 1 ); 
                MyStrnCpy ( clNewOut, pclNewVal, 100, 1 );  
                
                dbg ( DEBUG, "CompareSDIFields Field <%s> Old <%s> New <%s> ilDiff <%d>",    
                      clFina, clOldOut, clNewOut, ilDiff );
            }
            else    /* not everything ok, so perhabs records are different */
            {
                dbg ( TRACE, "CompareSDIFields: GetFieldData failed for Field <%s> RC <%d>",    
                      clCmpFields[ilLoop-1], ilRC );
                ilDiff = 1;
            }
            pclNewVal = pclOldVal = 0;
            ilLoop++;
        }/* end of if */ 
        else
            ilRC = RC_FAIL;
    }while((ilRC == RC_SUCCESS) && (ilLoop <= ilNoOfItems) && !ilDiff );
    
    dbg ( TRACE, "CompareSDIFields processed <%d> fields, ilDiff <%d> RC <%d>", 
                      ilLoop, ilDiff, ilRC );
    if ( (ilRC == RC_SUCCESS) && !ilDiff )
        *iplEqual = 1;
    return ilRC;
}

static int ResetGlobalTempVars ()
{
    int ilRC;

    if (!bgOpenUFRFLICOL)                                               /*rna*/
    {                                                                   /*rna*/
        cgUCDChanges[0]='\0';
        cgIvStart[0]='\0';
        cgIvEnd[0]='\0';
    }                                                                   /*rna*/

    dbg (TRACE, "ResetGlobalTempVars: Vor CEDAArrayDelete rgFullDemArray" );
    ilRC = CEDAArrayDelete( &(rgFullDemArray.rrArrayHandle), 
                            rgFullDemArray.crArrayName );
    dbg (TRACE, "ResetGlobalTempVars: Nach CEDAArrayDelete rgFullDemArray" );
    if ( ilRC != RC_SUCCESS )
    {
        dbg ( TRACE, "ResetGlobalTempVars: CEDAArrayDelete failed RC <%d>", ilRC ); 
    }

    return ilRC;
}


/*  KeepChangedUCD:                                                          */
/*  keep RUD-Urno of full demand in global string cgUCDChanges               */ 
/*  look for SDI-records for the current flight and affected counter class   */
/*  pcpUccc (SDI.UCCC == pcpUccc ),                                          */
/*  collect the SDI-urnos in global string cgSDIChanges                      */ 
static int KeepChangedUCD ( char *pcpRudUrno, char *pcpUccc, char *pcpUaft,
                            char * pcpHopo )
{
    int     ilRC = RC_SUCCESS;
    int     ilFound=RC_SUCCESS ;
    char    clKey[41], clUrno[21], *pclResult=0, *pclTmp;
    long    llRow;

    if ( !pcpUccc || !pcpRudUrno || !pcpUaft || !pcpHopo )
    {
        dbg ( TRACE, "KeepChangedUCD: At least one parameter=NULL" );
        return RC_FAIL;
    }

    MyStrnCpy ( clUrno, pcpUccc, 10, 1 );
    /*remember key for SDIArray: "HOPO,UAFT,UCCC,UPRO"*/
    sprintf ( clKey, "%s,%s,%s", pcpHopo, pcpUaft, clUrno );
    llRow = ARR_FIRST;      
    
    ilFound = CEDAArrayFindRowPointer( &(rgSdiArray.rrArrayHandle),  
                                       rgSdiArray.crArrayName,       
                                       &(rgSdiArray.rrIdx01Handle),
                                       rgSdiArray.crIdx01Name, clKey, 
                                       &llRow, (void**)&pclResult ); 
    if ( ilFound == RC_SUCCESS )
    {
        pclTmp = pclResult + rgSdiArray.plrArrayFieldOfs[igSdiUrnoIdx-1];
        dbg ( TRACE, "KeepChangedUCD: Found old SDI <%s> for full demand's counter class <%s>", 
              pclTmp, pcpUccc ); 
    }
    else
    {
        sprintf ( clKey, "%s,%s", clUrno, pcpUaft );
        llRow = ARR_FIRST;
        ilFound = CEDAArrayFindRowPointer( &(rgNewSdiArr.rrArrayHandle),  
                                           rgNewSdiArr.crArrayName,       
                                           &(rgNewSdiArr.rrIdx01Handle),
                                           rgNewSdiArr.crIdx01Name, clKey, 
                                           &llRow, (void**)&pclResult ); 
        if ( ilFound == RC_SUCCESS )
        {
            pclTmp = pclResult + rgNewSdiArr.plrArrayFieldOfs[igSdiUrnoIdx-1];
            dbg ( TRACE, "KeepChangedUCD: Found new SDI <%s> for full demand's counter class <%s>", 
                  pclTmp, pcpUccc ); 
        }
    
    }
    if ( ilFound != RC_SUCCESS )
    {
        dbg ( TRACE, "KeepChangedUCD: Error: No SDI record for RUD <%s> Flight <%s> UCCC <%s> inDB!", 
                     pcpRudUrno, pcpUaft, pcpUccc );
        ilRC = RC_NOT_FOUND;
    }

    AddUCDChanges(pcpRudUrno);

    /* if ( igExtendedTracing ) */
    {
        dbg ( DEBUG, "Now cgUCDChanges <%s>", cgUCDChanges );
    }
    return ilRC;
}


static int SendEvent2DEMCCI( int ipDest )
{
    int     ilRC = RC_SUCCESS, ilRC2;  /* Return code */
    BC_HEAD *prlBchead    = NULL;
    CMDBLK  *prlCmdblk    = NULL;
    char    *pclData      = NULL;
    char    *pclSelection = NULL;
    char    *pclFields    = NULL;
    long    llActSize   = 0;
    char    clStart[21], clEnd[21], clStr[21];
    char    clSqlbuf[2048] ;
    char    clDataArea1[2048]="", clDataArea2[2048]="";
    int     ilItem,ilLen, ilNoOfRuds;
    short   slCursor=0, slSqlFunc; 

    if (!cgUCDChanges[0]) 
    {   /*  nothing has been updated, therefore nothing has */
        /*  to be recalculated by DEMCCI */
        dbg(TRACE, "SendEvent2DEMCCI: cgUCDChanges are empty, no event will be sent to DEMCCI");
        return RC_SUCCESS;
    }

    ilNoOfRuds = get_no_of_items(cgUCDChanges);
    ilLen = 2*15 + ilNoOfRuds *11 + 1;

        /* 2*Time Stamps (je 14 char) + length of rudlist + delimiters */
    ilLen = 30 + strlen(cgUCDChanges) + 10;
    llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + ilLen;
    if ( !prgOutEvent || ( llActSize > lgOutEventSize ) )
    {
        prgOutEvent = realloc(prgOutEvent,llActSize);
        if (prgOutEvent == NULL)
        {
            dbg(TRACE, "SendEvent2DEMCCI: realloc out event <%d> bytes failed",
                llActSize);
            ilRC = RC_FAIL ;
            lgOutEventSize = 0;
        } /* end of if */
        else
            lgOutEventSize = llActSize;
    } /* end of if */
    /*------------------------------------------------------------------------*/

    if ( ilRC == RC_SUCCESS )
    {
        memset( prgOutEvent, 0, lgOutEventSize );
        memcpy (prgOutEvent, prgEvent, /*prgEvent->data_length +*/ sizeof(EVENT)) ;

        prgOutEvent->originator  = mod_id;
        prgOutEvent->data_offset = sizeof(EVENT);
        prgOutEvent->data_length = lgOutEventSize - sizeof(EVENT);
        
        prlBchead = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
        memset( prlBchead, 0, sizeof(BC_HEAD) );
        prlBchead->rc = RC_SUCCESS;
        strcpy(prlBchead->dest_name, "DISCAL" );
        strcpy(prlBchead->recv_name, "DEMCCI" );

        prlCmdblk    = (CMDBLK *)  ((char *) prlBchead->data) ;
        /* set CMDBLK members */
        strcpy(prlCmdblk->command, "CCD" );
        strcpy(prlCmdblk->obj_name, "RUD" );
        /* Selection is empty */    
        prlCmdblk->data[0] = '\0';
        /* fields are empty */  
        *(prlCmdblk->data+1) = '\0';
        /* data */  
        sprintf(prlCmdblk->data+2, "%s\n%s\n%s", cgIvStart, cgIvEnd,
                cgUCDChanges);
        if ( strlen (prlCmdblk->tw_end) < 3 )   /* HOPO not yet in tw_end */
            strcpy ( prlCmdblk->tw_end, cgHopo );
        sprintf( &(prlCmdblk->tw_end[3]), ",%s,DISCAL", cgTabEnd );

        DebugPrintBchead (DEBUG,prlBchead) ;
        DebugPrintCmdblk (DEBUG,prlCmdblk) ;

        pclSelection = (char *)prlCmdblk->data;
        pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
        pclData = (char *)pclFields + strlen(pclFields) + 1;
        dbg (TRACE, "SendEvent2DEMCCI: selection <%s>", pclSelection) ;         
        dbg (TRACE, "SendEvent2DEMCCI: fields    <%s>", pclFields) ;            
        dbg (TRACE, "SendEvent2DEMCCI: data      <%s>", pclData) ;              

        /*************************************************************************/
            
        ilRC = que (QUE_PUT, ipDest, mod_id, PRIORITY_4, llActSize, (char *)prgOutEvent) ;
        if (ilRC != RC_SUCCESS)
        {
            HandleQueErr (ilRC) ;
            DebugPrintEvent (TRACE, prgOutEvent) ;
            DebugPrintBchead (TRACE, prlBchead) ;
            DebugPrintCmdblk (TRACE, prlCmdblk) ;
            dbg (TRACE, "selection <%s>", pclSelection) ;
            dbg (TRACE, "fields    <%s>", pclFields) ;
            dbg (TRACE, "data      <%s>", pclData) ;
        } /* end of if */
    } /* end of if */

    return ilRC ;
    
} /* end of SendEvent2DEMCCI */

static int ReadPaxCalcMethods ()
{
    char clSection[FORM_LEN+1], clEntry[11];
    char clBuffer[101], *pclSep1=0, *pclSep2=0;
    int ilRC, ilMethods =0, ilRC2, ilIdx=0, i, ilLen, ilSize, j;
    short slDcopEntryOk, slEmptyIs0EntryOk;

    ilRC = iGetConfigRow ( cgConfigFile, "PAX_CALCULATION", "ACTUAL_DATE", 
                           CFG_STRING, cgNewRecBuf );                   /*rna*/
    dbg ( TRACE, "Init: iGetConfigRow Entry <ACTUAL_DATE> Value <%s> RC <%d>", 
          cgNewRecBuf, ilRC );
    if ( (ilRC == RC_SUCCESS) && (strlen(cgNewRecBuf) == DATELEN))  /*rna*/
        StrToTime( cgNewRecBuf, &tgActDate);                            /*rna*/
    else                                                                /*rna*/
        tgActDate = 0;                                                  /*rna*/

    /*
    ilRC = iGetConfigRow(cgConfigFile, "PAX_CALCULATION", "CLASSF", CFG_STRING,
                            cgNewRecBuf);
    dbg(TRACE, "Init: iGetConfigRow returned <%d> Entry <%s>", ilRC, 
        cgNewRecBuf);
    if (ilRC == RC_SUCCESS)
        strcpy(cgCICF, cgNewRecBuf);
    else
        strcpy(cgCICF, "F");
        
    ilRC = iGetConfigRow(cgConfigFile, "PAX_CALCULATION", "CLASSC", CFG_STRING,
                            cgNewRecBuf);
    dbg(TRACE, "Init: iGetConfigRow returned <%d> Entry <%s>", ilRC, 
        cgNewRecBuf);
    if (ilRC == RC_SUCCESS)
        strcpy(cgCICC, cgNewRecBuf);
    else
        strcpy(cgCICC, "C");

    ilRC = iGetConfigRow(cgConfigFile, "PAX_CALCULATION", "CLASSY", CFG_STRING,
                            cgNewRecBuf);
    dbg(TRACE, "Init: iGetConfigRow returned <%d> Entry <%s>", ilRC, 
        cgNewRecBuf);
    if (ilRC == RC_SUCCESS)
        strcpy(cgCICY, cgNewRecBuf);
    else
        strcpy(cgCICY, "Y");
*/
    ilRC = iGetConfigRow ( cgConfigFile, "PAX_CALCULATION", "METHODS", 
                           CFG_STRING, cgNewRecBuf );
    dbg ( DEBUG, "Init: iGetConfigRow returned <%d> Entry <%s>", ilRC, 
          cgNewRecBuf );
    if ( ilRC != RC_SUCCESS )
        dbg(TRACE,"Init: didn't find: Pax-Calc.-Methods in <%s>", cgConfigFile);
    else
    {
        ilMethods = get_no_of_items (cgNewRecBuf); 
        /* don't use more ways to calc PAX-count than MAX_CALC_METH */
        ilMethods = FMIN ( ilMethods, MAX_CALC_METH );  
        dbg(TRACE,"Init: <%d> Pax-Calc.-Methods in <%s>", ilMethods, cgNewRecBuf );
    }
    if ( ilMethods < 1 )
    {
        dbg(0,"Init: No method for PAX-calculation defined in <%s>", 
                cgNewRecBuf );
        ilRC = RC_FAIL;
    }
    else
    {
        prgPaxCalc = (PAXCALCM*)malloc( ilMethods *sizeof(PAXCALCM) ) ; 
        if ( !prgPaxCalc )
        {
            dbg ( TRACE,"Init: prgPaxCalc malloc(%d) failed",
                 ilMethods *sizeof(PAXCALCM)  );
            ilRC = RC_INIT_FAIL;
        }
        else
        {
            /* *igPaxCalcCnt = ilMethods; */
            memset ( prgPaxCalc, 0, ilMethods *sizeof(PAXCALCM) );
        }
    }
    ilIdx = 0;
    if (ilRC == RC_SUCCESS)
    {   
        ilSize = igClassCount * sizeof(char*);
        /*  read all methods into global buffer prgPaxCalc */
        for ( i=1; (i<=ilMethods)&&(ilRC==RC_SUCCESS); i++ )
        {
            ilLen = get_real_item ( clBuffer,cgNewRecBuf, i );
            dbg ( DEBUG, "Init: extracted %d chars name <%s> from entry <%s>", 
                  ilLen, clBuffer, cgNewRecBuf );
            if ( (ilLen<=0) || (ilLen> FORM_LEN) )
            {
                dbg ( TRACE, "Found invalid PAX-Calc-Method <%s>. Method will be ignored",
                      clBuffer );
                continue;
            }
            strcpy ( prgPaxCalc[ilIdx].crName, clBuffer );
            strcpy ( clSection, prgPaxCalc[ilIdx].crName );

            ilRC2 = iGetConfigRow ( cgConfigFile, clSection, "TABLE", CFG_STRING,
                                    clBuffer );
            if ( (ilRC2!=RC_SUCCESS) || (strlen(clBuffer)<3) )
            {
                dbg ( TRACE, "Didn't find valid entry TABLE for PAX-Calc-Method <%s>. Method will be ignored",
                      clSection );
                continue;
            }
            else
            {
                clBuffer[3]='\0';
                sprintf ( prgPaxCalc[ilIdx].crTable, "%s%s", 
                          clBuffer, cgTabEnd );
            }
            
            ilRC2 = iGetConfigRow ( cgConfigFile, clSection, "SEPERATE", CFG_STRING,
                                    clBuffer );
            if ( (ilRC2 == RC_SUCCESS ) && (strlen(clBuffer)>=1) )
            {
                switch (clBuffer[0] )
                {
                    case 'Y':
                    case 'y':
                        prgPaxCalc[ilIdx].srSeperate = 1;
                        break;
                    case 'N':
                    case 'n':
                        prgPaxCalc[ilIdx].srSeperate = 0;
                        break;
                    default:
                        ilRC2 = RC_INIT_FAIL;
                }
            }
            else
                ilRC2 = RC_INIT_FAIL;

            if ( ilRC2!=RC_SUCCESS)
            {
                dbg ( TRACE, "Didn't find valid entry SEPERATE for PAX-Calc-Method <%s>. Method will be ignored",
                      clSection );
                continue;
            }
            if (prgPaxCalc[ilIdx].srSeperate)
            {
                ilSize = igClassCount * sizeof(char**);
                prgPaxCalc[ilIdx].pcrClForms = (char**)malloc ( ilSize );
                if ( !prgPaxCalc[ilIdx].pcrClForms )
                {
                    dbg ( TRACE,"InitClassCodes: prgPaxCalc[%d].pcrClForms malloc(%d) failed", 
                          ilIdx, ilSize );
                    ilRC = RC_INIT_FAIL;    
                }
                else
                {
                    for ( j=1; (j<=igClassCount)&&(ilRC == RC_SUCCESS); j++ )
                    {
                        sprintf ( clEntry, "CLASS%d", j );
                        ilRC2 = iGetConfigRow ( cgConfigFile, clSection, clEntry, 
                                                CFG_STRING, clBuffer );
                        if ( (ilRC2==RC_SUCCESS) && (strlen(clBuffer)>0) && 
                             (strlen(clBuffer)<=FORM_LEN) )
                        {
                            prgPaxCalc[ilIdx].pcrClForms[j-1] = (char*)malloc ( FORM_LEN+1 );
                            if ( !prgPaxCalc[ilIdx].pcrClForms[j-1] )
                            {
                                dbg ( TRACE,"InitClassCodes: prgPaxCalc[%d].pcrClForms[%d] malloc(%d) failed", 
                                      ilIdx, j-1, FORM_LEN+1 );
                                ilRC = RC_INIT_FAIL;    
                            }
                            else
                                strcpy ( prgPaxCalc[ilIdx].pcrClForms[j-1], clBuffer );
                        }
                        else
                        {
                            prgPaxCalc[ilIdx].pcrClForms[j-1] = 0;
                            dbg ( TRACE, "Didn't find valid entry <%s> for PAX-Calc-Method <%s>. Class will be ignored for this method",
                                  clEntry, clSection );
                        }
                    }
                }
            }  /* end of if (prgPaxCalc[ilIdx].srSeperate)  */
            else
            {
                ilRC2 = iGetConfigRow ( cgConfigFile, clSection, "TOTAL", CFG_STRING,
                                        clBuffer );
                if ( (ilRC2==RC_SUCCESS) && (strlen(clBuffer)>0) && 
                     (strlen(clBuffer)<=FORM_LEN) )
                    strcpy ( prgPaxCalc[ilIdx].crFormT, clBuffer );
                else
                {
                    ilRC2 = RC_INIT_FAIL;
                    dbg ( TRACE, "Didn't find valid entry TOTAL for PAX-Calc-Method <%s>. Method will be ignored",
                          clSection );
                }
            } /* end of else */

            ilRC2 = iGetConfigRow ( cgConfigFile, clSection, "LINK", CFG_STRING,
                                    clBuffer );
            if ( ilRC2 == RC_SUCCESS )
            {
                if ( pclSep1 = strchr ( clBuffer, ',' ) )
                    pclSep2 =  strchr ( pclSep1, '.' );
            }
            if ( (ilRC2!=RC_SUCCESS) || !pclSep1 || !pclSep2 || 
                 (strlen(clBuffer)<13) )
            {
                dbg ( TRACE, "Didn't find valid entry LINK for PAX-Calc-Method <%s>. Method will be ignored",
                      clSection );
                continue;
            }
            else
            {
                *pclSep1 = '\0';
                *pclSep2 = '\0';
                MyStrnCpy ( prgPaxCalc[ilIdx].crLinkField, clBuffer, 4, 1 );
                MyStrnCpy ( prgPaxCalc[ilIdx].crBaseTable, pclSep1+1, 3, 1 );
                strcat ( prgPaxCalc[ilIdx].crBaseTable, cgTabEnd );
                MyStrnCpy ( prgPaxCalc[ilIdx].crBaseField, pclSep2+1, 4, 1 );
            }

            ilRC2 = iGetConfigRow( cgConfigFile, clSection, "VALIDITY", /*rna*/
                                    CFG_STRING, clBuffer );             /*rna*/
            if ( ilRC2 == RC_SUCCESS )                                  /*rna*/
                prgPaxCalc[ilIdx].lrValidity = atoi(clBuffer);          /*rna*/
            else                                                        /*rna*/
            {                                                           /*rna*/
                dbg(DEBUG, "No entry for validity for method %s",clSection);
                prgPaxCalc[ilIdx].lrValidity = 0;                       /*rna*/
                ilRC2 = RC_SUCCESS;                                     /*rna*/
            }                                                           /*rna*/

            if ( ilRC2==RC_SUCCESS ) 
            {
                slDcopEntryOk = 0;
                if ( ( iGetConfigRow ( cgConfigFile, clSection, "USE_DCOP", 
                                     CFG_STRING, clBuffer ) == RC_SUCCESS ) &&
                     ( strlen(clBuffer)>0 )
                   )
                {
                    switch (clBuffer[0] )
                    {
                        case 'Y':
                        case 'y':
                            slDcopEntryOk = 1;
                            prgPaxCalc[ilIdx].srUseDCOP = 1;
                            break;
                        case 'N':
                        case 'n':
                            slDcopEntryOk = 1;
                            prgPaxCalc[ilIdx].srUseDCOP = 0;
                            break;
                    }
                }
                if ( !slDcopEntryOk )
                {
                    dbg ( TRACE, "Didn't find valid entry USE_DCOP for PAX-Calc-Method <%s>. Parameter will be set to TRUE.",
                                  clSection );
                    prgPaxCalc[ilIdx].srUseDCOP = 1;
                }

                slEmptyIs0EntryOk = 0;
                if ( ( iGetConfigRow ( cgConfigFile, clSection, "EMPTY_EQUALS_TO_0", 
                                     CFG_STRING, clBuffer ) == RC_SUCCESS ) &&
                     ( strlen(clBuffer)>0 )
                   )
                {
                    switch (clBuffer[0] )
                    {
                        case 'Y':
                        case 'y':
                            slEmptyIs0EntryOk = 1;
                            prgPaxCalc[ilIdx].srEmptyIs0 = 1;
                            break;
                        case 'N':
                        case 'n':
                            slEmptyIs0EntryOk = 1;
                            prgPaxCalc[ilIdx].srEmptyIs0 = 0;
                            break;
                    }
                }
                if ( !slEmptyIs0EntryOk )
                {
                    dbg ( TRACE, "Didn't find valid entry EMPTY_EQUALS_TO_0 for PAX-Calc-Method <%s>. Parameter will be set to TRUE.",
                                  clSection );
                    prgPaxCalc[ilIdx].srEmptyIs0 = 1;
                }

                /* everything ok, so increment count of PaxCalcMethods */
                ilIdx++;
            }   
        }   /*  end of for-loop */

        /*  if not at least one Methods could be read correctly,
            initialization failed                                   */
        if ( ilIdx < 1 )
            ilRC = RC_INIT_FAIL;
    }
    igPaxCalcCnt = ilIdx;
    return ilRC;
}

/* ******************************************************************** */
/* print event members                                                  */
/* ******************************************************************** */
static void DebugPrintPaxCalcMethods ( int ipLevel )
{
    int i, j;
    
    if ( (igPaxCalcCnt>0) && !prgPaxCalc )
        dbg (TRACE, "DebugPrintPaxCalcMethods: Error! igPaxCalcCnt <%d> and prgPaxCalc <null>", 
             igPaxCalcCnt );
    else
    {
        dbg (ipLevel, "%d PaxCalcMethods in Array", igPaxCalcCnt );

        for ( i = 0; i<igPaxCalcCnt; i++ )
        {
            dbg(ipLevel, "%d. method: Name <%s> Table <%s>", i+1, 
                prgPaxCalc[i].crName, prgPaxCalc[i].crTable );
            dbg(ipLevel, "            Seperate <%d>  UseDCOP <%d> EMPTY_EQUALS_TO_0 <%d>", 
                prgPaxCalc[i].srSeperate, prgPaxCalc[i].srUseDCOP, prgPaxCalc[i].srEmptyIs0 ); 
            if ( prgPaxCalc[i].srSeperate && prgPaxCalc[i].pcrClForms )
            {
                for ( j=0; j<igClassCount; j++ )
                {
                    if ( prgPaxCalc[i].pcrClForms[j] )
                        dbg (ipLevel, "            Form Class%d: <%s>", j+1, prgPaxCalc[i].pcrClForms[j] );
                    else
                        dbg (ipLevel, "            Form Class%d: <null>", j+1 );
                }
            }
            else
                dbg (ipLevel, "            FormT <%s>", prgPaxCalc[i].crFormT );

            dbg ( ipLevel, "            LINK Field <%s> Base <%s.%s>", 
                  prgPaxCalc[i].crLinkField, prgPaxCalc[i].crBaseTable, 
                  prgPaxCalc[i].crBaseField );
        }   /* end of for-loop */
    }                           
}                               


/*  not yet finished, still under construction   !!!!!!  */
/*  current version only for testing  */   
static int GetPAXCount ( char *pcpAFTUrno, char *pcpPROUrno, char *pcpCCCUrno,
                         char *pcpHopo, char *pcpStod, int *pipPAXCnt )
{
    int     ilRC, ilRCSql1, ilOk ;
    char    clCICC[11], clField[11];
    char    clSqlbuf[2048], *pclForm, *pclPtr;
    char    clDataArea[2048]="", clBuffer[FORM_LEN+1]; 
    int     ilClassIdx; /* index for pcrClForms, i.e. position of Class Code in prgClassCodes */
    float   flFaktor, flDcopFaktor=1.0;
    char    clRefValue[81], *pclResult=0;
    short   slCursor=0, slSqlFunc; 
    int     ilSign, ilSum, ilVal, ilFound=0, ilIdx ;
    long    llRow;
    BOOL    blIsValid = TRUE;                                           /*rna*/

    if ( !pcpAFTUrno || !pcpPROUrno || !pcpCCCUrno || !pcpHopo || !pipPAXCnt )
    {
        dbg ( TRACE, "GetPAXCount: At least one parameter = NULL" );
        return RC_FAIL ;
    }
    dbg ( TRACE, "GetPAXCount: Parameters AFTUrno <%s> PROUrno <%s>, CCCUrno <%s> Hopo <%s>",
          pcpAFTUrno, pcpPROUrno, pcpCCCUrno, pcpHopo );

    ilRC = GetFieldByUrno( &rgCccArray, pcpCCCUrno, "CICC", clCICC, URNOLEN+1 );
    if ( ilRC == RC_SUCCESS )
    {
        ilRC = GetClassIndex ( clCICC, &ilClassIdx );
        if ( ilRC != RC_SUCCESS )
        {
            dbg ( TRACE, "GetPAXCount: found unknown counter class CICC <%s> URNO <%s>",
                  clCICC, pcpCCCUrno );
            ilRC = RC_NOT_FOUND;
        }
    }
    else
        dbg ( TRACE, "GetPAXCount: Invalid Counter class URNO <%s>", pcpCCCUrno );
    if ( ilRC == RC_SUCCESS )
    {
        for ( ilIdx=0; !ilFound && (ilIdx<igPaxCalcCnt); ilIdx++ )
        {   /*  loop through all methods of PAX-calculation  */

            ilRC = CheckValidity( ilIdx, pcpStod, &blIsValid );         /*rna*/
            if ( ilRC != RC_SUCCESS || !blIsValid )
                continue;

            if ( prgPaxCalc[ilIdx].srSeperate )
            {
                if ( prgPaxCalc[ilIdx].pcrClForms && (ilClassIdx<igClassCount) &&
                     prgPaxCalc[ilIdx].pcrClForms[ilClassIdx] )
                    pclForm = prgPaxCalc[ilIdx].pcrClForms[ilClassIdx];
                else
                {
                    dbg ( TRACE, "GetPAXCount: %d.Method doesn't contain a formula for Class <%s>", clCICC );
                    continue;
                }
                flFaktor = 1.0;
                
            }
            else
            {
                pclForm = prgPaxCalc[ilIdx].crFormT;
                /* take flFaktor from PXC-record */
                /* Idx01 for PXCTAB:   "UPRO,HOPO,UCCC"  */
                sprintf ( clBuffer, "%s,%s,%s", pcpPROUrno, pcpHopo, pcpCCCUrno );
                llRow = ARR_FIRST;
                ilOk = CEDAArrayFindRowPointer( &(rgPxcArray.rrArrayHandle),  
                                                rgPxcArray.crArrayName,
                                                &(rgPxcArray.rrIdx01Handle), 
                                                rgPxcArray.crIdx01Name,
                                                clBuffer, &llRow, 
                                                (void**)&pclResult ); 
                if ( ilOk != RC_SUCCESS )
                    dbg ( TRACE, "GetPAXCount: Didn't find PXC-Record for key <%s>", 
                         clBuffer );
                else
                {
                    pclPtr = pclResult + rgPxcArray.plrArrayFieldOfs[igPxcDpaxIdx-1];
                    clBuffer[0] = '\0';
                    if ( pclPtr )
                        MyStrnCpy ( clBuffer, pclPtr , 5, 1 );          
                    if ( sscanf ( clBuffer, "%f", &flFaktor ) < 1 )
                        ilOk = RC_FAIL;
                    else
                        flFaktor /= 100.0;
                }
                if ( ilOk != RC_SUCCESS )
                    continue ;  /* with next metod */
            }
            clRefValue[0] = '\0';
            if ( !strncmp ( prgPaxCalc[ilIdx].crBaseTable, "AFT", 3 ) &&
                 !strcmp ( prgPaxCalc[ilIdx].crBaseField, "URNO" )  )
            {   /* link is done via AFT.URNO */
                strcpy ( clRefValue, pcpAFTUrno );
                ilRCSql1 = RC_SUCCESS;
            }       
            else
            {
                sprintf ( clSqlbuf, "select %s from %s where URNO='%s'", 
                          prgPaxCalc[ilIdx].crBaseField, 
                          prgPaxCalc[ilIdx].crBaseTable, pcpAFTUrno );
                slSqlFunc = START | REL_CURSOR ;
                clDataArea[0] = '\0';
                dbg ( TRACE, "GetPAXCount: Before 1st sql_if in %d. method SqlBuffer <%s>", 
                      ilIdx, clSqlbuf );
                
                ilRCSql1 = sql_if ( slSqlFunc, &slCursor, clSqlbuf, clDataArea );
                    
                dbg ( TRACE, "GetPAXCount: 1st sql_if in %d. method returned <%d> DataArea <%s>", 
                      ilIdx, ilRCSql1, clDataArea );
                if ( ilRCSql1==RC_SUCCESS)
                    MyStrnCpy ( clRefValue, clDataArea, 80, 1 );
            }
            dbg ( TRACE, "GetPaxCount: Method %d used with RefValue <%s>", ilIdx, clRefValue );

            if ( pclForm && (ilRCSql1==RC_SUCCESS) &&  clRefValue[0] )
            {
                strcpy ( clBuffer, pclForm );
                RemoveChar ( clBuffer, ' ' );
                pclPtr = clBuffer ;
                ilSum = 0;
                do 
                {
                    if ( *pclPtr == '-' ) 
                        ilSign = -1;
                    else 
                        ilSign = 1;
                    if ( ( *pclPtr == '-' ) || ( *pclPtr == '+' ) )
                        pclPtr ++;
                    MyStrnCpy ( clField, pclPtr, 4, 0 );
                    sprintf ( clSqlbuf, "select %s from %s where %s='%s'", 
                              clField, prgPaxCalc[ilIdx].crTable, 
                              prgPaxCalc[ilIdx].crLinkField, clRefValue );
                    dbg ( TRACE, "GetPAXCount: Before 2nd sql_if in %d. method SqlBuffer <%s>", 
                          ilIdx, clSqlbuf );
                    
                    slSqlFunc = START | REL_CURSOR ;
                    clDataArea[0] = '\0';
                    ilOk = sql_if ( slSqlFunc, &slCursor, clSqlbuf, clDataArea );
                    dbg ( TRACE, "GetPAXCount: 2nd sql_if in %d. method returned <%d> DataArea <%s>",
                          ilIdx, ilOk, clDataArea );
                           
                    if ( ilOk==RC_SUCCESS)
                    {
                        if ( sscanf ( clDataArea, "%d", &ilVal ) < 1 )
                        {   
                            ilVal = 0;
                            if ( !prgPaxCalc[ilIdx].srEmptyIs0 )
                                ilOk = RC_FAIL;
                        }
                    }
                    if ( ilOk == RC_SUCCESS )
                    {
                        dbg ( TRACE, "GetPAXCount: Before incrementation pclPtr <%s> strlen <%d>",
                              pclPtr, strlen(pclPtr) );
                        pclPtr += 4;
                        dbg ( TRACE, "GetPAXCount: After incrementation pclPtr <%s> strlen <%d>",
                              pclPtr, strlen(pclPtr) );
                        ilSum += (ilSign*ilVal );
                    }
                }while ( *pclPtr && ( ilOk==RC_SUCCESS) );
                dbg ( TRACE, "GetPAXCount: Method <%d> Form <%s> Ok <%d> Sum <%d>",  ilIdx, pclForm, ilOk, ilSum );

                flDcopFaktor = 1.0;
                if ( ilOk ==RC_SUCCESS) 
                {
                    if ( prgPaxCalc[ilIdx].srUseDCOP )
                    {   /* use percentage of people going to CCI from PROTAB */
                        ilOk = GetFieldByUrno ( &rgProArray, pcpPROUrno, "FDCO",
                                                clBuffer, 2 ) ;
                        if ( ilOk == RC_SUCCESS)
                        {
                            if ( strcmp(clBuffer,"1") == 0 )
                            {
                                ilOk = GetFieldByUrno ( &rgProArray, pcpPROUrno,
                                                        "DCOP", clBuffer, 6 ) ;
                                if ( ilOk==RC_SUCCESS ) 
                                {
                                    if ( sscanf( clBuffer, "%f", &flDcopFaktor ) < 1 )
                                        ilOk = RC_FAIL;
                                    else
                                        flDcopFaktor /= 100.0;
                                }
                                else
                                    dbg ( TRACE, "GetPAXCount: %d. method: Failed to read DCOP for PRO-Urno <%s>",
                                  ilIdx, pcpPROUrno );
                            }
                            else
                                flDcopFaktor = 1.0;
                        }
                        else
                            dbg ( TRACE, "GetPAXCount: %d. method: Failed to read FDCO for PRO-Urno <%s>",
                                  ilIdx, pcpPROUrno );
                    }
                }
                if ( ilOk ==RC_SUCCESS) 
                    ilFound = 1;
                
            }
        }
    }
    if ( ilFound )
    {
        *pipPAXCnt = (int)( flFaktor * (float)ilSum * flDcopFaktor ); 
        dbg ( TRACE, "GetPAXCount: Method <%d> successful, flFaktor <%.2f> flDcopFaktor <%.2f> ilSum <%d> PaxCnt <%d>",
                     ilIdx-1, flFaktor, flDcopFaktor, ilSum, *pipPAXCnt );
    }
    else
        ilRC = RC_NOT_FOUND;

    return ilRC;
}

int RemoveChar ( char *pcpStr, char cpRemove )
{
    char *pclOld, *pclNew, *pclBuffer;
    pclBuffer = malloc ( strlen(pcpStr) +1 );
    if ( !pclBuffer )
    {
        dbg ( TRACE, "RemoveChar: Allocation failure" );
        return RC_FAIL;
    }
    pclOld = pcpStr;
    pclNew = pclBuffer;
    while ( *pclOld )
    {
        if ( *pclOld != cpRemove )
        {
            *pclNew = *pclOld;
            pclNew++;
        }
        pclOld++;
    }
    *pclNew = '\0';
    strcpy ( pcpStr, pclBuffer );
    free ( pclBuffer );
    return RC_SUCCESS;
}

static int OnFlightUpdate ( char *pcpAftUrno )
{
    int     ilRC =RC_SUCCESS, ilFound=RC_FAIL ;
    char    clKey[41], pclWhere[51], clHopo[11], *pclTmp;
    char    pclUpro[11], pclStod[15], clSDIUrnos[201]="", clUrno[11];
    long    llRow=ARR_FIRST;
    char    *pclResult=0;
    BC_HEAD rlBCHead;
    CMDBLK  rlCmdblk;
    int     ilUrnoCnt, ilLoop ;
    int     ilPos;
    
    if ( !pcpAftUrno )
    {
        dbg ( TRACE, "OnFlightUpdate: Parameter is NULL");
        return RC_FAIL;
    }   
    ResetGlobalTempVars ();
    if ( GetDataItem (clHopo, pcgTwEnd, 1, ',', "", "\0") <= 0 )
        strcpy ( clHopo, cgHopo );  

    /*  check whether there is already a SDI-record for this flight */
    sprintf ( clKey, "%s,%s", clHopo, pcpAftUrno );
    ilFound = CEDAArrayFindRowPointer( &(rgSdiArray.rrArrayHandle), 
                                   rgSdiArray.crArrayName,
                                   &(rgSdiArray.rrIdx01Handle), 
                                   rgSdiArray.crIdx01Name, clKey, 
                                   &llRow, (void**)&pclResult ); 
    dbg ( TRACE, "OnFlightUpdate: CEDAArrayFindRowPointer in SDIArray llRow <%ld> ilRC <%d>",
          llRow ,ilFound );

    if ( ( ilFound == RC_SUCCESS ) && pclResult )
        ilRC = GetFullDemands ( pcpAftUrno, clHopo );

    if ( ( ilFound == RC_SUCCESS ) && (ilRC == RC_SUCCESS) )
    {
        do 
        {   /* collect all SDI-Urnos that belong to to flight */
            pclTmp = pclResult+rgSdiArray.plrArrayFieldOfs[igSdiUrnoIdx-1];
            MyStrnCpy ( clUrno, pclTmp, 10, 1 );            
            if ( strlen ( clSDIUrnos ) >= 190 )
            {
                dbg ( TRACE, "OnFlightUpdate: Not sufficent memory for SDI-Urnos" );
                ilRC = RC_OVERFLOW;
            }
            else
            {
                strcat ( clSDIUrnos, clUrno );
                strcat ( clSDIUrnos, "," );
            }
            if ( ilRC == RC_SUCCESS )
            {
                llRow = ARR_NEXT;
                ilFound = CEDAArrayFindRowPointer ( &(rgSdiArray.rrArrayHandle), 
                                                    rgSdiArray.crArrayName,
                                                    &(rgSdiArray.rrIdx01Handle), 
                                                    rgSdiArray.crIdx01Name, clKey, 
                                                    &llRow, (void**)&pclResult ); 
            }
        }while ( ( ilFound == RC_SUCCESS ) && ( ilRC == RC_SUCCESS ) );
    }
    
    if ( ilRC == RC_SUCCESS )
    {
        ilPos = strlen ( clSDIUrnos );
        if ( (ilPos>0) && (clSDIUrnos[ilPos-1]==',') )
            clSDIUrnos[ilPos-1]='\0';       /* remove last comma */
        ilUrnoCnt = get_no_of_items(clSDIUrnos);
        dbg ( TRACE, "OnFlightUpdate: %d SDI-Urnos <%s> for flight <%s>", 
              ilUrnoCnt, clSDIUrnos, pcpAftUrno );

        for  ( ilLoop = 1; (ilLoop<=ilUrnoCnt)&&(ilRC==RC_SUCCESS); ilLoop++ )
        {
            ilPos = get_real_item(clUrno,clSDIUrnos,ilLoop);
            if ( ilPos > 0 )
            {
                ilRC = GetFieldByUrno(&rgSdiArray, clUrno, "UPRO", pclUpro,
                                        URNOLEN+1);
                ilRC |= GetFieldByUrno(&rgSdiArray, clUrno, "PENA", pclStod,
                                        DATELEN+1);
                if ( ilRC != RC_SUCCESS )
                    dbg ( TRACE, "OnFlightUpdate: GetFieldByUrno failed for UPRO or PENA SDI-Urno <%s>",
                          clUrno );
                else
                {
                    dbg ( DEBUG, "OnFlightUpdate: SDI-Record found UPRO <%s> PENA <%s>",
                          pclUpro, pclStod );
                    if ( pclStod[0] && pclUpro[0] )
                    {
                        ilRC = UpdateSDITab ( pcpAftUrno, pclUpro, pclStod );
                        if ( ilRC != RC_SUCCESS )
                            dbg ( TRACE, "OnFlightUpdate: UpdateSDITab failed for flight <%s> RC <%d>",
                                  pcpAftUrno, ilRC );
                    }
                    else
                    {
                        dbg ( TRACE, "OnFlightUpdate: UPRO or STOD empty for SDI-record <%s>",
                              clUrno );
                        ilRC = RC_FAIL;
                    }
                }
            }
            else
            {
                ilRC = RC_FAIL;
                dbg ( TRACE, "OnFlightUpdate: failed to read %d. Urno from <%s>",
                              ilLoop, clSDIUrnos );
            }
        }
    
        if ( ( ilUrnoCnt > 0 ) && ( ilRC ==  RC_SUCCESS ) )
        {   /* loop ended correctly, all records in SDI successfully processed */
            ilFound = SendEvent2DEMCCI( igDest );
            if ( ilFound != RC_SUCCESS )            
                dbg ( TRACE, "OnFlightUpdate: SendEvent2DEMCCI failed RC <%d>", ilRC );
        }
        else
            ilFound = RC_FAIL;
    }
    if ( ilFound != RC_SUCCESS )    
    {   /* no SDI record was found for flight, or an error occured */
        /* set BC-Head members */
        memset((void*)&rlBCHead, 0x00, sizeof(BC_HEAD));
        rlBCHead.rc = RC_SUCCESS;
        strcpy(rlBCHead.dest_name, "DISCAL");
        strcpy(rlBCHead.recv_name, "FLICOL");

        /* set CMDBLK members */
        memset((void*)&rlCmdblk, 0x00, sizeof(CMDBLK));
        strcpy(rlCmdblk.command, "CFL");
        rlCmdblk.obj_name[0] = '\0';
        sprintf(rlCmdblk.tw_end, "%s,%s,DISCAL", clHopo,cgTabEnd );
        
        sprintf ( pclWhere, "WHERE URNO='%s'     [ROTATIONS]", pcpAftUrno );
        ilRC = SendToQue( 1200, PRIORITY_4, &rlBCHead, &rlCmdblk, pclWhere, 
                          "", "" );
        if ( ilRC != RC_SUCCESS )
            dbg ( TRACE, "SendToQue: SendEvent2DEMCCI failed RC <%d>", ilRC );
    }
    return ilRC;
}


/******************************************************************************/
/* the SendToQue routine                                                      */
/******************************************************************************/
static int SendToQue(int ipModID, int ipPrio, BC_HEAD *prpBCHead, 
                     CMDBLK *prpCmdblk, char *pcpSelection, 
                     char *pcpFields, char *pcpData)
{
    int         ilRC;
    int         ilLen;
    EVENT           *prlOutEvent    = NULL;
    BC_HEAD     *prlOutBCHead   = NULL;
    CMDBLK      *prlOutCmdblk   = NULL;

    dbg(DEBUG,"<SendToQue> ----- START -----");

    /* calculate size of memory we need */
    ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
             strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 20; 

    /* get memory for out event */
    if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
    {
        dbg(TRACE,"<SendToQue> %05d malloc failed, can't allocate %d bytes", __LINE__, ilLen);
        dbg(DEBUG,"<SendToQue> ----- END -----");
        return RC_FAIL;
    }

    /* clear buffer */
    memset((void*)prlOutEvent, 0x00, ilLen);

    /* set structure members */
    prlOutEvent->type        = SYS_EVENT;
    prlOutEvent->command     = EVENT_DATA;
    prlOutEvent->originator  = mod_id;
    prlOutEvent->retry_count = 0;
    prlOutEvent->data_offset = sizeof(EVENT);
    prlOutEvent->data_length = ilLen - sizeof(EVENT);

    /* BCHead members */
    prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
    memcpy(prlOutBCHead, prpBCHead, sizeof(BC_HEAD));

    /* CMDBLK members */
    prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
    memcpy(prlOutCmdblk, prpCmdblk, sizeof(CMDBLK));

    /* Selection */
    strcpy(prlOutCmdblk->data, pcpSelection);

    /* fields */
    strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1, pcpFields);

    /* data */
    strcpy(prlOutCmdblk->data+strlen(pcpSelection)+strlen(pcpFields)+2, pcpData);

    /* send this message */
    dbg(DEBUG,"<SendToQue> sending to Mod-ID: %d", ipModID);
    if ((ilRC = que(QUE_PUT, ipModID, mod_id, ipPrio, ilLen, (char*)prlOutEvent)) != RC_SUCCESS)
    {
        dbg(TRACE,"<SendToQue> %05d QUE_PUT returns: %d", __LINE__, ilRC);
        return RC_FAIL;
    }

    /* delete memory */
    free((void*)prlOutEvent);

    dbg(DEBUG,"<SendToQue> ----- END -----");

    /* bye bye */
    return RC_SUCCESS;
}

/*  GetFullDemands:  Get RUD-Urnos of full demands for an already calculated */
/*                   flight and fill rgFullDemArray                          */
/*                   looks for UCD-Records with UAFT=pcpAftUrno and inserts  */
/*                   rows in rgFullDemArray accordingly                      */
static int GetFullDemands ( char *pcpAftUrno, char *pcpHopo )
{
    char    clKey[21], clUaft[11], clUrud[11], clVref[21];
    long    llRow=ARR_FIRST;
    char    *pclResult=0, *pclTmp;
    int     ilFound, ilRC=RC_SUCCESS;

    if ( !pcpAftUrno || !pcpHopo )
    {
        dbg ( TRACE, "GetFullDemands: At least one parameter = NULL" ); 
        return RC_FAIL;
    }
    sprintf ( clKey, "%s,%s", pcpHopo, pcpAftUrno );
    
    do 
    {
        ilFound = CEDAArrayFindRowPointer( &(rgUcdArray.rrArrayHandle), 
                                           rgUcdArray.crArrayName,
                                           &(rgUcdArray.rrIdx01Handle), 
                                           rgUcdArray.crIdx01Name, clKey, 
                                           &llRow, (void**)&pclResult ); 
        if ( ilFound == RC_SUCCESS ) 
        {
            pclTmp = pclResult+rgUcdArray.plrArrayFieldOfs[igUcdUaftIdx-1];
            MyStrnCpy ( clUaft, pclTmp, 10, 1 );            
            pclTmp = pclResult+rgUcdArray.plrArrayFieldOfs[igUcdUrudIdx-1];
            MyStrnCpy ( clUrud, pclTmp, 10, 1 );
            
            ilRC = GetFieldByUrno ( &rgRudArray, clUrud, "VREF", clVref, 21 );
            if ( ilRC != RC_SUCCESS )
            {
                dbg ( TRACE, "GetFullDemands: GetFieldByUrno failed Urno <%s> ilRC <%d>", 
                      clUrud, ilRC );
            }
            else
            {   
                /*  FullDemands fields: "URNO,VREF" */
                memset ( cgNewRecBuf, 0, sizeof(cgNewRecBuf) );
                sprintf ( cgNewRecBuf, "%s%c%s", clUrud, '\0', clVref ); 
                llRow = ARR_FIRST;
                ilRC = AATArrayAddRow( &(rgFullDemArray.rrArrayHandle), 
                                       rgFullDemArray.crArrayName, &llRow,
                                       (void *)cgNewRecBuf);
                if ( ilRC != RC_SUCCESS )
                {
                    dbg ( TRACE, "GetFullDemands: AATArrayAddRow failed Buffer <%s> ilRC <%d>", 
                          cgNewRecBuf, ilRC );
                }
            }
        }
        llRow = ARR_NEXT;
    }while ( ( ilFound == RC_SUCCESS ) && ( ilRC == RC_SUCCESS ) );
    
    /*if (debug_level > 0)
            SaveIndexInfo ( &rgFullDemArray, 2, 0 );*/
    return ilRC;
}


/*  CopyNewSdiRecords:                                                      */
/*  Copies all Records from rgNewSdiArr to rgSdiArray, that only one Array  */
/*  has to be saved and ACTION doesn't have to send IRT-messages to         */  
/*  DISCAL for SDI-Array                                                    */
static int CopyNewSdiRecords () 
{
    int     ilRC = RC_SUCCESS;
    int     ilFound = RC_SUCCESS;
    long    llRowRead = ARR_FIRST;  /*  row in rgNewSdiArr */
    long    llRowWrite = ARR_FIRST; /*  row in rgSdiArray */
    long    llRowsToCopy = 0;
    int     ilDone = 0;
    long    *pllFields=0;
    char    clKey[2] ="", *pclResult=0;

    pllFields = (long*)calloc( rgNewSdiArr.lrArrayFieldCnt, sizeof(long) );
    if ( !pllFields )
    {
        dbg ( TRACE, "CopyNewSdiRecords: calloc failed" );
        return RC_NOMEM;
    }
    pllFields[0] = -1;

    CEDAArrayGetRowCount( &(rgNewSdiArr.rrArrayHandle), 
                          rgNewSdiArr.crArrayName, &llRowsToCopy );
    dbg ( DEBUG, "CopyNewSdiRecords: rgNewSdiArr has %ld records", llRowsToCopy );
    memset ( cgNewRecBuf, 0, sizeof(cgNewRecBuf) );

    do
    {
        ilFound = CEDAArrayFindRowPointer( &(rgNewSdiArr.rrArrayHandle),  
                                            rgNewSdiArr.crArrayName,
                                            &(rgNewSdiArr.rrIdx01Handle), 
                                            rgNewSdiArr.crIdx01Name, clKey, 
                                            &llRowRead, (void**)&pclResult );
        if ( ilFound == RC_SUCCESS )
            ilFound = CEDAArrayGetFields( &rgNewSdiArr.rrArrayHandle,
                                      rgNewSdiArr.crArrayName, pllFields, 
                                      rgNewSdiArr.crArrayFieldList, ',',
                                      rgNewSdiArr.lrArrayRowLen, llRowRead,
                                      (void*)rgNewSdiArr.pcrArrayRowBuf);
        switch ( ilFound )
        {
            case RC_NOTFOUND:
                break;          /* finished */
            case RC_SUCCESS:
                llRowWrite = ARR_FIRST;
                ilRC = CEDAArrayAddRow( &rgSdiArray.rrArrayHandle,
                                        rgSdiArray.crArrayName, &llRowWrite,
                                        (void *)cgNewRecBuf);
                if ( ilRC != RC_SUCCESS )
                    dbg ( TRACE, "CopyNewSdiRecords: CEDAArrayAddRow failed to add %d. row", 
                          ilDone+1 );
                else
                {
                    ilRC = CEDAArrayPutFields( &rgSdiArray.rrArrayHandle,
                                               rgSdiArray.crArrayName, 0,
                                               &llRowWrite, 
                                               rgSdiArray.crArrayFieldList,
                                               rgNewSdiArr.pcrArrayRowBuf );    
                    if ( ilRC != RC_SUCCESS )
                        dbg ( TRACE, "CopyNewSdiRecords: CEDAArrayPutFields failed to set %d. row\n<%s>", 
                              ilDone+1, rgNewSdiArr.pcrArrayRowBuf );
                    else
                        ilDone ++;
                }
                llRowRead = ARR_NEXT;
                break;
            default:
                ilRC = ilFound;     /* set return value to error */
        }   /* end of switch */
    }while ( ilFound == RC_SUCCESS );

    dbg ( DEBUG, "CopyNewSdiRecords: Copied %d rows successfully RC <%d>", 
          ilDone, ilRC );
    if ( pllFields )
        free ( pllFields  );
    return ilRC;
}

       
static int  OpenUFRFLICOL(void)                                         /*rna*/
{
    int     ilRC = RC_SUCCESS;

    dbg(TRACE,"Begin of coverage data, %d",bgOpenUFRFLICOL);
    ResetGlobalTempVars ();
    bgOpenUFRFLICOL = TRUE;

    return ilRC;
}
static int  CloseUFRFLICOL(void)                                        /*rna*/
{
    int     ilRC = RC_SUCCESS;

    bgOpenUFRFLICOL = FALSE;
    dbg(TRACE,"End of coverage data, %d",bgOpenUFRFLICOL);
    ilRC = SendEvent2DEMCCI( igDest );

    return ilRC;
}

static int CheckChanges(char *pcpAftUrno, char *pcpProUrno, BOOL *pbpNoChanges)
{                                                                       /*rna*/
    int     ilRC = RC_SUCCESS;
    char    clHopo[HOPOLEN+1];
    char    clUrud[URNOLEN+1];
    char    clUpro[URNOLEN+1];
    char    clVref[URNOLEN+1];
    char    clFullDemUrud[URNOLEN+1];
    char    clKey[HOPOLEN+1 + URNOLEN+1];
    char    clKey2[HOPOLEN+1 + URNOLEN+1 + URNOLEN+1];
    long    llUcdRow = ARR_FIRST;
    long    llSdiRow, llFullDemRow;
    long    llFullDemCnt = 0;
    long    llUcdCnt = 0;
    char    *pclResult = 0;
    char    *pclResult2 = 0;
    char    *pclResult3 = 0;
    BOOL    blUrudFound = FALSE;

    *pbpNoChanges = TRUE;

        /*ilRC = SaveIndexInfo ( &rgFullDemArray, 1, 0 );*/
    if ( GetDataItem (clHopo, pcgTwEnd, 1, ',', "", "\0") <= 0 )
        strcpy ( clHopo, cgHopo );  
    sprintf( clKey, "%s,%s", clHopo, pcpAftUrno );

    ilRC = CEDAArrayGetRowCount( &(rgFullDemArray.rrArrayHandle), 
                                 rgFullDemArray.crArrayName, &llFullDemCnt );

    while ( ilRC = CEDAArrayFindRowPointer( &(rgUcdArray.rrArrayHandle),  
                                            rgUcdArray.crArrayName,
                                            &(rgUcdArray.rrIdx01Handle), 
                                            rgUcdArray.crIdx01Name, clKey, 
                                            &llUcdRow, (void**)&pclResult )
                                                                == RC_SUCCESS 
            && (*pbpNoChanges == TRUE) )
    {
        llUcdCnt++;

        strcpy ( clUrud, UCDFIELD(pclResult, igUcdUrudIdx) );

        llFullDemRow = ARR_FIRST;
        while (CEDAArrayGetRowPointer( &(rgFullDemArray.rrArrayHandle),
                                    rgFullDemArray.crArrayName, llFullDemRow,
                                    (void*)&pclResult2) == RC_SUCCESS) 
        {
            llFullDemRow = ARR_CURRENT;
            strcpy ( clFullDemUrud, FULLDEMFIELD(pclResult2, igFullDemUrnoIdx) );

            if ( strcmp(clFullDemUrud, clUrud) == 0)
            {
                blUrudFound = TRUE;
                break;
            }
            llFullDemRow = ARR_NEXT;
        }

        if ( blUrudFound)
        {
            strcpy ( clVref, FULLDEMFIELD(pclResult2, igFullDemVrefIdx) );
            sprintf( clKey2, "%s,%s", clKey, clVref );
            llSdiRow = ARR_FIRST;
            ilRC = CEDAArrayFindRowPointer( &(rgSdiArray.rrArrayHandle),  
                                            rgSdiArray.crArrayName,
                                            &(rgSdiArray.rrIdx01Handle), 
                                            rgSdiArray.crIdx01Name, clKey2,
                                            &llSdiRow, (void**)&pclResult3);
            if ( ilRC != RC_SUCCESS)
            {
                *pbpNoChanges = FALSE;
            }
            else
            {
                strcpy ( clUpro, SDIFIELD(pclResult3, igSdiUproIdx) );
                TrimRight ( clUpro );
                if ( strcmp(clUpro, pcpProUrno) != 0 )
                {
                    *pbpNoChanges = FALSE;
                }
            }
        }

        llUcdRow = ARR_NEXT; 

    }

    if ( !blUrudFound || (llFullDemCnt != llUcdCnt) )
        *pbpNoChanges = FALSE;

    return RC_SUCCESS;
}

static int CheckValidity( int ipIdx, char *pcpStod, BOOL *pbpIsValid )
{
    int     ilRC = RC_SUCCESS;
    time_t  tlNow, tlStod;

    if ( tgActDate == 0)
        tlNow = time( 0L );
    else
        tlNow = tgActDate;

    StrToTime( pcpStod, &tlStod );

    if ( prgPaxCalc[ipIdx].lrValidity == 0 )
        *pbpIsValid = TRUE;
    else
    {
        if ( (tlStod - prgPaxCalc[ipIdx].lrValidity*60) < tlNow )
            *pbpIsValid = TRUE;
        else
            *pbpIsValid = FALSE;
    }
    if ( *pbpIsValid )
        dbg( TRACE, "CheckValidity is TRUE");
    else
        dbg( TRACE, "CheckValidity is FALSE");

    return ilRC;
}

static int ForwardEvent( int ipDest )
{
    int         ilRC        = RC_SUCCESS;  /* Return code */
    BC_HEAD     *prlBchead    = NULL;
    CMDBLK      *prlCmdblk    = NULL;
    char        *pclSelection = NULL;
    char        *pclFields    = NULL;
    char        *pclData      = NULL;
    long        llActSize   = 0;

    dbg(DEBUG, "ForwardEvent: dest <%d>", ipDest);

    llActSize = prgEvent->data_length + sizeof(EVENT) + 2;

    if (llActSize > lgOutEventSize)
    {
        lgOutEventSize = 2 * llActSize;

        prgOutEvent = realloc(prgOutEvent, lgOutEventSize);
        if (prgOutEvent == NULL)
        {
            dbg(TRACE, "ForwardEvent: realloc out event <%d> bytes failed",
                lgOutEventSize);
            ilRC = RC_FAIL;
        }
    }

    if (ilRC == RC_SUCCESS)
    {
        memcpy(prgOutEvent, prgEvent, prgEvent->data_length + sizeof(EVENT));

        prlBchead = (BC_HEAD *)((char *)prgOutEvent + sizeof(EVENT));
        prlCmdblk = (CMDBLK *)((char *)prlBchead->data);
        pclSelection = prlCmdblk->data;
    
        pclFields = pclSelection + strlen(pclSelection) + 1;
        pclData  = pclFields + strlen(pclFields) + 1;

        DebugPrintBchead(DEBUG,prlBchead);
        DebugPrintCmdblk(DEBUG,prlCmdblk);
        dbg(DEBUG, "ForwardEvent: selection <%s>", pclSelection);         
        dbg(DEBUG, "ForwardEvent: fields    <%s>", pclFields);            
        dbg(DEBUG, "ForwardEvent: data      <%s>", pclData);              
            
        ilRC = que(QUE_PUT, ipDest, mod_id, PRIORITY_4, llActSize,
                    (char *)prgOutEvent);
        if (ilRC != RC_SUCCESS)
        {
            HandleQueErr (ilRC) ;
            DebugPrintEvent(TRACE, prgOutEvent);
            DebugPrintBchead(TRACE, prlBchead);
            DebugPrintCmdblk(TRACE, prlCmdblk);
            dbg(TRACE, "selection <%s>", pclSelection);
            dbg(TRACE, "fields    <%s>", pclFields);
            dbg(TRACE, "data      <%s>", pclData);
        }
    }

    return ilRC;
    
}

static int GetDebugLevel(char *pcpMode, int *pipMode)
{
    int     ilRC = RC_SUCCESS;
    char    clCfgValue[64];

    ilRC = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,
                            clCfgValue);

    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
    }
    else
    {
        dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
        if (!strcmp(clCfgValue, "DEBUG"))
            *pipMode = DEBUG;
        else if (!strcmp(clCfgValue, "TRACE"))
            *pipMode = TRACE;
        else
            *pipMode = 0;
    }

    return ilRC;
}

static int  AddUCDChanges(char *pcpRudUrno)
{
    int     ilRC = RC_SUCCESS;
    int     ilInArray;
    int     ilPos = 0, ilCol = 0, ilItemNo = 0;
    char    clUrno[URNOLEN+1];

    MyStrnCpy(clUrno, pcpRudUrno, 10, 1);
    ilInArray = FindItemInList(cgUCDChanges, clUrno, ',', &ilItemNo, &ilCol,
                                &ilPos);
    if (ilInArray == RC_SUCCESS)
        dbg(TRACE, "KeepChangedUCD: RUD.URNO <%s> already in list", clUrno);
    else
    {
        if ((strlen(cgUCDChanges) + strlen(clUrno) + 1) <= 
                                                    (unsigned int)URNO_LIST_LEN)
        {   /* there is still enough space in string */
            if (strlen(cgUCDChanges) > 0)
                strcat(cgUCDChanges, ",");
            strcat(cgUCDChanges, clUrno);
            dbg ( TRACE, "KeepChangedUCD: Added RUD.URNO <%s> to list",
                  clUrno );
        }
        else
        {
            dbg ( TRACE, "KeepChangedUCD: Couldn't add RUD.URNO <%s> to list, list is full", 
                  clUrno );
            ilRC = RC_OVERFLOW;
        }
    }
    return ilRC;
}
static int  CheckInterval(char *pcpPbea, char *pcpPena)
{
    int     ilRC = RC_SUCCESS;

    if ((cgIvStart[0] == '\0') || (strcmp(cgIvStart, pcpPbea) > 0))
        strcpy(cgIvStart, pcpPbea);
    if ((cgIvEnd[0] == '\0') || (strcmp(cgIvEnd, pcpPena) < 0))
        strcpy(cgIvEnd, pcpPena);

    return ilRC;
}

static int InitClassCodes ()
{
    int         ilRC, ilClasses, i, ilSize;
    BOOL        blCountOk = FALSE;
    char        clEntry[11];

    dbg ( DEBUG, "Entering InitClassCodes" );
    
    ilRC = iGetConfigRow ( cgConfigFile, "PAX_CALCULATION", "CLASS_COUNT", 
                           CFG_STRING, cgNewRecBuf );                   
    if ( (ilRC == RC_SUCCESS) && (sscanf(cgNewRecBuf, "%d", &ilClasses) >= 1) )
    {
        igClassCount = ilClasses;
        blCountOk = TRUE;
    }
    ilSize = igClassCount *sizeof(CLASSCODES);
    prgClassCodes = (CLASSCODES*)malloc( ilSize ) ; 
    if ( !prgClassCodes )
    {
        dbg ( TRACE,"InitClassCodes: prgClassCodes malloc(%d) failed", ilSize );
        prgClassCodes = 0;
        ilRC = RC_INIT_FAIL;
    }
    else
        ilRC = RC_SUCCESS;

    for ( i=1; (i<=igClassCount) && (ilRC == RC_SUCCESS); i++ )
    {
        sprintf ( clEntry, "CLASS%d", i );
        ilRC = iGetConfigRow ( cgConfigFile, "PAX_CALCULATION", clEntry, 
                             CFG_STRING, cgNewRecBuf );
        if ( (ilRC != RC_SUCCESS) && ( i<=3 ) )
        {
            strcpy ( cgNewRecBuf, cgDefaultClasses[i-1] );
            ilRC = RC_SUCCESS;
        }
        cgNewRecBuf[63] = '\0'; /* to prevent buffer overflow */
        if (ilRC == RC_SUCCESS)
            sprintf(prgClassCodes[i-1], ",%s,", cgNewRecBuf);
    }
    if ( i < igClassCount )
        igClassCount = i;

    for ( i=0; i<igClassCount; i++ )
        dbg ( DEBUG, "InitClassCodes: Codes for %d.Class <%s>", i+1, prgClassCodes[i] );
    return ilRC;
}

/*  look for Code "pcpClassCode" in comma-separated list "prgClassCodes".
    If found, return RC_SUCCESS and pipIdx returns the index within the list */
static int GetClassIndex ( char *pcpClassCode, int * pipIdx )
{
    int ilRC = RC_NOT_FOUND;
    int i;
    char pclTmpStr[256];
        
    TrimRight(pcpClassCode);
    sprintf( pclTmpStr, ",%s,", pcpClassCode );
    for ( i=0; i<igClassCount; i++ )
    {
        if( strstr( prgClassCodes[i], pclTmpStr ) != NULL )
        {
            *pipIdx = i;
            dbg ( DEBUG, "GetClassIndex: found index <%d> for counter class CICC <%s> in <%s>", *pipIdx, pclTmpStr, prgClassCodes[i] );
            return RC_SUCCESS;
        }
    }
    if ( ilRC!= RC_SUCCESS )
        dbg ( TRACE, "GetClassIndex: found unknown counter class CICC <%s>",  pcpClassCode );
    return ilRC;
}

/********************************************************************/
static int GetNextUrno(char *pcpUrno)
{
    int ilRC = RC_SUCCESS ;
    char  pclDataArea[IDATA_AREA_SIZE] ;
    
    if (igReservedUrnoCnt <= 0)
    {                            /* hole Urno buffer -> get new Urnos */
        memset( pclDataArea, 0, IDATA_AREA_SIZE) ;
        if ((ilRC = GetNextValues(pclDataArea, URNOS_TO_FETCH)) != RC_SUCCESS)
        {
            dbg ( TRACE,"GetNextValues failed RC <%d>", ilRC );
        }
        else
        {
            strcpy ( pcpUrno, pclDataArea );
            igReservedUrnoCnt = URNOS_TO_FETCH - 1 ;
            lgActUrno = atol(pcpUrno);
            dbg ( DEBUG, "GetNextUrno: fetched %d Urnos. Next is <%s>", 
                  URNOS_TO_FETCH, pcpUrno ) ;
        }
    }
    else
    {
        igReservedUrnoCnt-- ;
        lgActUrno++ ;
        sprintf ( pcpUrno, "%ld", lgActUrno );      /* get next reserved Urno */
        dbg ( DEBUG, "GetNextUrno: Next Urno <%s>, %d Urnos remaining", 
              pcpUrno, igReservedUrnoCnt ) ;
    }
    
    return (ilRC) ;
} /* getNextUrno () */

static int FindFeleEvaluation ( char *pcpTable, char *pcpField, char *pcpFele )
{
    long llRow= ARR_FIRST;  
    int ilRC = RC_FAIL ;
    char *pclResult=0;
    char clKey[51]; /*"TANA,FINA" */
    char *pclFele;
    sprintf ( clKey, "%s,%s", pcpTable, pcpField );
    
    if ( rgFeleEvalArr.rrArrayHandle >= 0 ) 
        ilRC = CEDAArrayFindRowPointer( &(rgFeleEvalArr.rrArrayHandle),  
                                        rgFeleEvalArr.crArrayName,
                                        &(rgFeleEvalArr.rrIdx01Handle), 
                                        rgFeleEvalArr.crIdx01Name, clKey, 
                                        &llRow, (void**)&pclResult ); 
    if ( ilRC ==RC_SUCCESS ) 
    {
        pclFele = pclResult + rgFeleEvalArr.plrArrayFieldOfs[2];
        if ( ( pclFele[0] == ' ' ) || ( pclFele[0] == '\0' ) )
        {
            ilRC = RC_FAIL;
            dbg ( TRACE, "FindFeleEvaluation: Found empty FELE, pattern <%s>", 
                  clKey );
        }
        else
        {
            strncpy ( pcpFele, pclFele, 8 );
            dbg ( DEBUG, "FindFeleEvaluation: Found fele <%s>", pclFele );
            pcpFele[8]= '\0';
        }
    }                   
    else
    {
        dbg( DEBUG, "FindFeleEvaluation: Didn't find pattern <%s>", clKey );
    }
    return ilRC;
}

static int SaveFeleEvaluation ( char *pcpTable, char *pcpField, char *pcpFele )
{
    long llRow= ARR_FIRST;  
    int ilRC =RC_FAIL ;
    
    memset ( cgEvalBuffer, ' ', sizeof(cgEvalBuffer) );
    sprintf ( cgEvalBuffer, "%s,%s,%s", pcpTable, pcpField, pcpFele ); 
    delton ( cgEvalBuffer );
    if ( rgFeleEvalArr.rrArrayHandle >= 0 ) 
        ilRC = AATArrayAddRow( &(rgFeleEvalArr.rrArrayHandle), 
                               rgFeleEvalArr.crArrayName, &llRow,
                               (void*)cgEvalBuffer );
    if ( ilRC != RC_SUCCESS )
    {
        dbg ( TRACE, "SaveFeleEvaluation: AATArrayAddRow failed Tana <%s> Fina <%s> ilRC <%d>", 
                      pcpTable, pcpField, ilRC );
    }
    return ilRC;
}

static int GetFieldLength(char *pcpTana, char *pcpFina, long *plpLen)
{
    int  ilRC  = RC_FAIL;     /* Return code */
    int  ilCnt = 0 ;
    char clTaFi[64] ;
    char clFele[32] ;
    char clFldLst[32] ;

    ilRC = FindFeleEvaluation ( pcpTana, pcpFina, clFele );
    if ( ( ilRC == RC_SUCCESS ) && ( sscanf(clFele, "%ld", plpLen ) >= 1 ) )
    {
        dbg (DEBUG, "GetFieldLength: from Array <%s,%s> <%ld>", pcpTana, pcpFina, *plpLen) ;
        return ilRC;
    }
    ilCnt = 1 ;
    sprintf (&clTaFi[0], "%s,%s", pcpTana, pcpFina) ;
    strcpy ( clFldLst, "TANA,FINA") ; /* wird von syslib zerstoert - konstante nicht moeglich */

    ilRC = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"") ;
    switch (ilRC)
    {
        case RC_SUCCESS :
            SaveFeleEvaluation ( pcpTana, pcpFina, clFele );
            *plpLen = atoi (&clFele[0]) ;
            dbg (DEBUG, "GetFieldLength: <%s,%s> <%d>", pcpTana, pcpFina, *plpLen) ;
            break ;
        
        case RC_NOT_FOUND :
            dbg (TRACE, "GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s>", ilRC, &clTaFi[0]) ;
            break ;
        
        case RC_FAIL :
            dbg (TRACE, "GetFieldLength: syslibSSD returned <%d> RC_FAIL", ilRC) ;
            break ;
        
        default :
            dbg(TRACE, "GetFieldLength: syslibSSD returned <%d> unexpected", ilRC) ;
            break ;
    } /* end of switch */
    return (ilRC) ;
} /* end of GetFieldLength */

static int InitFieldIndices()
{
    int ilRc = RC_SUCCESS;
    int ilCol,ilPos;

    ilRc = FindItemInList( RUD_FIELDS, "UCRU", ',', &igRudUcruIdx, &ilCol,&ilPos);
    dbg ( DEBUG, "InitFieldIndices: RUD: UCRU <%d>", igRudUcruIdx );
    ilRc = FindItemInList( RUD_FIELDS, "UTPL", ',', &igRudUtplIdx, &ilCol,&ilPos);
    dbg ( DEBUG, "InitFieldIndices: RUD: UTPL <%d>", igRudUtplIdx );

    ilRc |= FindItemInList( UCD_FIELDS, "URUD", ',', &igUcdUrudIdx,&ilCol,&ilPos);
    ilRc |= FindItemInList( UCD_FIELDS, "CDAT", ',', &igUcdCdatIdx,&ilCol,&ilPos);
    ilRc |= FindItemInList( UCD_FIELDS, "HOPO", ',', &igUcdHopoIdx,&ilCol,&ilPos);
    ilRc |= FindItemInList( UCD_FIELDS, "LSTU", ',', &igUcdLstuIdx,&ilCol,&ilPos);
    dbg ( DEBUG, "InitFieldIndices: UCD: URUD <%d> CDAT <%d> HOPO <%d> LSTU <%d>", 
        igUcdUrudIdx, igUcdCdatIdx, igUcdHopoIdx, igUcdLstuIdx );

    ilRc |= FindItemInList( UCD_FIELDS, "UAFT", ',', &igUcdUaftIdx,&ilCol,&ilPos);
    ilRc |= FindItemInList( UCD_FIELDS, "URNO", ',', &igUcdUrnoIdx,&ilCol,&ilPos);
    ilRc |= FindItemInList( UCD_FIELDS, "USEC", ',', &igUcdUsecIdx,&ilCol,&ilPos);
    ilRc |= FindItemInList( UCD_FIELDS, "USEU", ',', &igUcdUseuIdx,&ilCol,&ilPos);
    dbg ( DEBUG, "InitFieldIndices: UCD: UAFT <%d> URNO <%d> USEC <%d> USEU <%d>", 
        igUcdUaftIdx, igUcdUrnoIdx, igUcdUsecIdx, igUcdUseuIdx );

    ilRc |= FindItemInList( PXC_FIELDS, "UPXA", ',', &igPxcUpxaIdx,&ilCol,&ilPos);
    ilRc |= FindItemInList( PXC_FIELDS, "DPAX", ',', &igPxcDpaxIdx,&ilCol,&ilPos);
    ilRc |= FindItemInList( PXC_FIELDS, "DBAG", ',', &igPxcDbagIdx,&ilCol,&ilPos);
    dbg ( DEBUG, "InitFieldIndices: PXC: UPXA <%d> DPAX <%d> DBAG <%d>", 
        igPxcUpxaIdx, igPxcDpaxIdx, igPxcDbagIdx );

    ilRc |= FindItemInList( VTP_FIELDS, "VALU", ',', &igVtpValuIdx,&ilCol,&ilPos);
    ilRc |= FindItemInList( VTP_FIELDS, "PENR", ',', &igVtpPenrIdx,&ilCol,&ilPos);
    ilRc |= FindItemInList( VTP_FIELDS, "PBER", ',', &igVtpPberIdx,&ilCol,&ilPos);
    dbg ( DEBUG, "InitFieldIndices: VTP: VALU <%d> PENR <%d> PBER <%d>", 
        igVtpValuIdx, igVtpPenrIdx, igVtpPberIdx );

    ilRc |= FindItemInList( SDI_FIELDS, "URNO", ',', &igSdiUrnoIdx,&ilCol,&ilPos);
    ilRc |= FindItemInList( SDI_FIELDS, "PBEA", ',', &igSdiPbeaIdx,&ilCol,&ilPos);
    ilRc |= FindItemInList( SDI_FIELDS, "PENA", ',', &igSdiPenaIdx,&ilCol,&ilPos);
    ilRc |= FindItemInList( SDI_FIELDS, "UCCC", ',', &igSdiUcccIdx,&ilCol,&ilPos);
    ilRc |= FindItemInList( SDI_FIELDS, "UPRO", ',', &igSdiUproIdx,&ilCol,&ilPos);
    dbg ( DEBUG, "InitFieldIndices: SDI: UCCC <%d> URNO <%d> UPRO <%d> PBEA <%d> PENA <%d>", 
        igSdiUcccIdx, igSdiUrnoIdx, igSdiUproIdx, igSdiPbeaIdx, igSdiPenaIdx );
    
    if ( ilRc != RC_SUCCESS )
    {
        ilPos = debug_level;
        debug_level = TRACE;
        dbg (TRACE, "InitFieldIndices: ####################") ;
        dbg (TRACE, "InitFieldIndices: At least one field index could not be initialized !!! " ) ;
        dbg (TRACE, "InitFieldIndices: ####################") ;
        debug_level  = ilPos;
    }
    return ilRc;
}

/* If necessary enlarge Timeframe using TIFA-values from selection */ 
static int EnlargeTimeFrame ( char * pcpSelection )
{
    int ilRC = RC_SUCCESS;
    char *pos1=0, *pos2=0, *pos3=0;
    char clDate1[DATELEN+1]="";
    char clDate2[DATELEN+1]="";
    time_t tlTest;

    pos1 = strstr ( pcpSelection, "TIFA" );
    if ( pos1 )
        pos2 = strchr ( pos1, '\'' );
    if ( pos2 )
    {
        dbg ( DEBUG, "EnlargeTimeFrame: pos for 1st Date <%s>", pos2 );
        strncpy ( clDate1, pos2+1, DATELEN );
        clDate1[DATELEN] = '\0';
        if ( pos2 = strchr ( pos2+1, '\'' ) )
            pos3 = strchr ( pos2+1, '\'' );
    }
    if ( pos3 )
    {
        dbg ( DEBUG, "EnlargeTimeFrame: pos for 2nd Date <%s>", pos3 );
        strncpy ( clDate2, pos3+1, DATELEN );
        clDate2[DATELEN] = '\0';
    }
    ilRC = StrToTime( clDate1, &tlTest );
    ilRC |= StrToTime( clDate2, &tlTest );
    if ( ilRC != RC_SUCCESS )
        dbg ( TRACE, "EnlargeTimeFrame: no valids date in selection <%s> <%s>",
              clDate1, clDate2 );
    else
    {
        if ( strcmp ( clDate1, clDate2 ) < 0 ) 
        { /* begin is in clDate1 */
            pos1 = clDate1;
            pos2 = clDate2;
        }
        else
        {
            pos1 = clDate2;
            pos2 = clDate1;
        }
        dbg ( DEBUG, "EnlargeTimeFrame: pos1 <%s> pos2 <%s> cgIvStart <%s> cgIvEnd <%s>", 
              pos1, pos2, cgIvStart, cgIvEnd );
        if ( (cgIvStart[0] == '\0') || (strcmp ( pos1, cgIvStart ) < 0) )
        {
            dbg ( TRACE, "EnlargeTimeFrame: moving Start of interval <%s> -> <%s>",
                  cgIvStart, pos1 );
            strcpy ( cgIvStart, pos1 );
        }
        if ( strcmp ( cgIvEnd, pos2 ) < 0 )
        {
            dbg ( TRACE, "EnlargeTimeFrame: moving End of interval <%s> -> <%s>",
                  cgIvEnd, pos2 );
            strcpy ( cgIvEnd, pos2 );
        }
    }
    return ilRC;
}



/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
