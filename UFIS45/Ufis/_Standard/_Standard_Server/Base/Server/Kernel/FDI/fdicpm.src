
#ifndef _DEF_mks_version_fdicpm
  #define _DEF_mks_version_fdicpm
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_fdicpm[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/FDI/fdicpm.src 1.22 2013/07/19 15:39:44SGT dka Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  SMI                                                      */
/* Date           :                                                           */
/* Description    : INCLUDE PART FROM FDIHDL                                  */
/*                                                                            */
/* Update history :                                                           */
/* MEI 20101018: Container code exceed 20, extend to 40  (v.1.21)             */
/* 20130717 v.1.22 DKA	Trunate ULDT to LOATAB.ULDT width to avoid an array   */
/*			insert failing all rows because of just a few rows.   */
/*			This is a rare occurence, can happen if SI section has*/
/*			the keyword "ULD NBRS" and the normal ULDT is being   */
/*			appended with SI information. UFIS-3717               */
/*			Convert array sizes to multiples of 512 (e.g. 1000 to */
/*			1024), to avoid problems on some architectures.       */
/*			LOATAB.ULDT has width 16, CPM_VALUES.pclUldNumber[16] */
/*			is used to save parsed telex data. Need to keep this  */
/*			in mind if LOATAB is ever adjusted.		      */
/*			Existing code has no check if the main CPM section's  */
/*			ULD no. is > 16char, which could already then over-   */
/*			write structure members of CPM_VALUES.                */
/******************************************************************************/
/*                                                                            */
/* FDICPM handle functions                                                    */
/* -----------------------                                                    */
/* static int HandleCPM(char *pcpData)                                        */
/* static int SendCPMLOARes()                                                 */

/*
   The limiting size should be the variable used to store the data, which in  
   turn must be equal to or less than the DB field width.
*/
#define	SIZE_STRUCT_CPM_VALUES_PCL_ULD_NUM 16
#define CPM_ULD_NUMBER_CONTINUATION_CHAR '*'

static int HandleCPM(char *pcpData)
{
  int ilRC=RC_SUCCESS;  
  int ilI;
  int ilJ;
  int ilK;
  char pclFunc[]="HandleCPM:";
  char *pclData;
  char pclResult[1024];
  char pclResult2[1024];
  char pclInfo[1024];
  char *pclTmpPtr;
  char *pclTmpPtr2;
  T_TLXRESULT rlTlxResult;
  char pclHomeAirport[8];
  char pclOrig[8] = "";
  char pclDest[8] = "";
  int ilDestFound = FALSE;
  char pclTmpBuf[1024];
  int ilCount;
  int ilCtrExp;
  int ilCtrCur;
  char pclItem[4096];
  char pclItem2[4096];
  char pclWeight[4096];
  int ilWeight;
  int ilTotalWeight = 0;
  char pclAddi[128];
  char *pclTmpPtrAddi;
  int ilSIPosition;
  char clSIPosition;
  char *pclBeginSI;
  char pclPos[128];
  char pclULDNo[128];
  char pclVal[32][128];
  int ilVal;
  char pclPosition[128];
  char pclType[128];
  char pclNumber[128];
  char pclDestination[128];
  char pclTable[8];
  char pclRes[8];
  int ilCnt;
  int ilWeightIdx;
  int ilTypeIdx;
  int ilL;
  int ilDigit;
  char *pclTmpDot;
  char pclFlightLine[1024];

  InitRecordDescriptor(&rgRecDesc,1024,0,FALSE);
  memset(pcgLoaBuf,0x00,1000*512);
  igArrayPtr = 0;

  igCurCpmValues = 0;
  ilRC = GetDataItem(pclHomeAirport,pcgTwEnd,1,',',"","  ");
  if (ilRC != 3)
  {
     strcpy(pclHomeAirport,pcgHomeAP);
  }
  
  pclData = pcpData;
  memset(&rlTlxResult,0x00,TLXRESULT);

  dbg(DEBUG,"%s Extract Flight Line",pclFunc); 
  ilI = 1;
  ilRC = 0;
  while (ilRC  < 2 && ilI < 4)
  {
     /* Search for Flight line  */
     ilRC = GetDataItem(pclResult,&pclData[rgTlxInfo.TxtStart],ilI,'\n',"","  ");
     ilI++;
  }
  /* Extract information from flight line*/
  if (ilRC > 0)
  {
     if (strcmp(pclResult,"=TEXT") == 0)
     {
        ilRC = GetDataItem(pclResult,&pclData[rgTlxInfo.TxtStart],ilI,'\n',"","  ");
        ilI++;
        if (strcmp(pclResult,"COR") == 0)
        {
           ilRC = GetDataItem(pclResult,&pclData[rgTlxInfo.TxtStart],ilI,'\n',"","  ");
           ilI++;
        }
        if (strcmp(pclResult,"QUOTE") == 0)
        {
           ilRC = GetDataItem(pclResult,&pclData[rgTlxInfo.TxtStart],ilI,'\n',"","  ");
           ilI++;
        }
        if (strcmp(pclResult,"CPM") == 0)
        {
           ilRC = GetDataItem(pclResult,&pclData[rgTlxInfo.TxtStart],ilI,'\n',"","  ");
           ilI++;
        }
     }
     dbg(DEBUG,"%s Found Flight Line <%s>",pclFunc,pclResult);
     if (strstr(pcgUseRegnForFlightSearch,"CPM") != NULL)
     {
        MakeTokenList(pclFlightLine,pclResult,".",',');
        GetDataItem(pclTmpBuf,pclFlightLine,2,',',"","\0\0");
        if (strlen(pclTmpBuf) < 13 && strstr(pclTmpBuf," ") == NULL)
        {
           strcpy(pcgRegnForFlightSearch,pclTmpBuf);
           dbg(DEBUG,"%s Use Regn <%s> for Flight Search",pclFunc,pcgRegnForFlightSearch);
        }
        else
        {
           strcpy(pcgRegnForFlightSearch,"");
           dbg(DEBUG,"%s <%s> cannot be a valid REGN",pclFunc,pclTmpBuf);
        }
     }
     ConvertFlightLine(pclResult);
     GetFlightDate(pclResult,1,2);
     MakeTokenList(pclResult2,pclResult," ",',');
     MakeTokenList(pclResult,pclResult2,"/",',');
     MakeTokenList(pclResult2,pclResult,".",',');
     pclTmpPtr = pclResult2;
     /* Get Airline Code */
     ilRC = GetAlc(&pclTmpPtr,prgMasterTlx);
     /* Get Flight Number */
     ilRC = GetFltn(&pclTmpPtr,prgMasterTlx);
     /* Step behind Flight line  */
     ilCount = CountToDelim(&pclData[rgTlxInfo.TxtStart],ilI-1,'\n');
     /* Manipulate the TxtStart to start next search without flightline */
     rgTlxInfo.TxtStart += ilCount;
     /* Save SI Position */
     pclTmpPtr = strstr(&pclData[rgTlxInfo.TxtStart],"\nSI");
     if (pclTmpPtr != NULL)
     {
        ilSIPosition = pclTmpPtr - pclData;
        clSIPosition = *pclTmpPtr;
        *pclTmpPtr = '\0';
     }
     else
     {
        ilSIPosition = 0;
     }
     ilI = 1;
     pclTmpPtr = GetLine(&pclData[rgTlxInfo.TxtStart],ilI);
     while (pclTmpPtr != NULL)
     {
        CopyLine(pclResult,pclTmpPtr);
        if (*pclResult == '-')
        {
           pclTmpPtr2 = GetLine(&pclData[rgTlxInfo.TxtStart],ilI+1);
           if (pclTmpPtr2 != NULL)
           {
              if (*pclTmpPtr2 == '/')
              {
                 CopyLine(pclTmpBuf,pclTmpPtr2);
                 strcat(pclResult,pclTmpBuf);
                 ilI++;
              }
           }
           MakeTokenList(pclInfo,pclResult,"-",',');
           ilCount = GetNoOfElements(pclInfo,',');
           for (ilJ = 1; ilJ <= ilCount; ilJ++)
           {
              GetDataItem(pclItem,pclInfo,ilJ,',',"","\0\0");
              MakeTokenList(pclItem2,pclItem,"/",',');
              ilVal = GetNoOfElements(pclItem2,',');
dbg(TRACE,"Line = <%s> , %d",pclItem2,ilVal);
              if (ilVal <= 3)
              {
                 pclTmpPtr2 = GetLine(&pclData[rgTlxInfo.TxtStart],ilI+1);
                 if (pclTmpPtr2 != NULL)
                 {
                    if (*pclTmpPtr2 != '-')
                    {
                       CopyLine(pclTmpBuf,pclTmpPtr2);
                       strcat(pclItem,pclTmpBuf);
                       MakeTokenList(pclItem2,pclItem,"/",',');
                       ilVal = GetNoOfElements(pclItem2,',');
dbg(TRACE,"Corrected Line = <%s> , %d",pclItem2,ilVal);
                    }
                 }
              }
              if (ilVal > 1)
              {
                 for (ilK = 0; ilK < ilVal && ilK < 20; ilK++)
                 {
                     GetDataItem(&pclVal[ilK][0],pclItem2,ilK+1,',',"","\0\0");
                 }
                 strcpy(pclPosition,&pclVal[0][0]);
                 strcpy(pclType," ");
                 strcpy(pclNumber," ");
                 strcpy(pclDestination," ");
                 strcpy(pclWeight,"0");
                 if (ilVal == 2)
                 {
                    strcpy(pclType,&pclVal[1][0]);
                    ilTypeIdx = 1;
                 }
                 else
                 {
                    ilWeightIdx = -1;
                    for (ilK = 1; ilK < ilVal-1 && ilK < 5 && ilWeightIdx == -1; ilK++)
                    {
                       ilDigit = TRUE;
                       ilL = 0;
                       while (ilL < strlen(&pclVal[ilK][0]) && ilDigit == TRUE)
                       {
                          if (!isdigit(pclVal[ilK][ilL]))
                          {
                          ilDigit = FALSE;
                          }
                       ilL++;
                       }
                       if (ilDigit == TRUE)
                       { /* ilK value is the ULD weight */
                          ilWeightIdx = ilK;
                       }
                    }
                    if (ilWeightIdx != -1)
                    {
                       ilTypeIdx = ilWeightIdx + 1;
                    }
                    else
                    {
                       ilTypeIdx = -1;
                       for (ilK = 1; ilK < ilVal && ilK < 5 && ilTypeIdx == -1; ilK++)
                       {
                          pclTmpDot = strstr(&pclVal[ilK][0],".");
                          if (pclTmpDot != NULL)
                          {
                             *pclTmpDot = '\0';
                          }
                          if (strlen(&pclVal[ilK][0]) < 3)
                          { /* ilK value is the ULD load category code */
                             ilTypeIdx = ilK;
                          }
                          if (pclTmpDot != NULL)
                          {
                             *pclTmpDot = '.';
                          }
                       }
                    }
dbg(TRACE,"ilWeightIdx = %d , ilTypeIdx = %d",ilWeightIdx,ilTypeIdx);
                    if (ilTypeIdx != -1)
                    {
                       strcpy(pclType,&pclVal[ilTypeIdx][0]);
                       if (ilWeightIdx != -1)
                       { /* Weight value exists */
                          strcpy(pclWeight,&pclVal[ilWeightIdx][0]);
                          if (ilWeightIdx == 3)
                          { /* full information */
                             strcpy(pclNumber,&pclVal[1][0]);
                             strcpy(pclDestination,&pclVal[2][0]);
                          }
                          else
                          {
                             if (ilWeightIdx == 2)
                             {
                                if (strlen(&pclVal[1][0]) == 3)
                                {
                                   sprintf(pclTable,"APT%s",pcgTABEnd);
                                   ilCnt = 1;
                                   ilRC = syslibSearchDbData(pclTable,"APC3",&pclVal[1][0],
                                                             "APC4",pclRes,&ilCnt,"\n");
                                   if (ilRC == RC_SUCCESS)
                                   { /* 2nd value is the destination */
                                      strcpy(pclDestination,&pclVal[1][0]);
                                   }
                                   else
                                   { /* 2nd value is the ULD type code */
                                      strcpy(pclNumber,&pclVal[1][0]);
                                   }
                                }
                                else
                                { /* 2nd value is the ULD type code */
                                   strcpy(pclNumber,&pclVal[1][0]);
                                }
                             }
                          }
                       }
                       else
                       { /* no Weight value */
                          if (ilTypeIdx == 3)
                          { /* full information */
                             strcpy(pclNumber,&pclVal[1][0]);
                             strcpy(pclDestination,&pclVal[2][0]);
                          }
                          else
                          {
                             if (ilTypeIdx == 2)
                             {
                                if (strlen(&pclVal[1][0]) == 3)
                                {
                                   sprintf(pclTable,"APT%s",pcgTABEnd);
                                   ilCnt = 1;
                                   ilRC = syslibSearchDbData(pclTable,"APC3",&pclVal[1][0],
                                                             "APC4",pclRes,&ilCnt,"\n");
                                   if (ilRC == RC_SUCCESS)
                                   { /* 2nd value is the destination */
                                      strcpy(pclDestination,&pclVal[1][0]);
                                   }
                                   else
                                   { /* 2nd value is the ULD type code */
                                      strcpy(pclNumber,&pclVal[1][0]);
                                   }
                                }
                                else
                                { /* 2nd value is the ULD type code */
                                   strcpy(pclNumber,&pclVal[1][0]);
                                }
                             }
                          }
                       }
                    }
                 }
                 strcpy(prgCpmValues[igCurCpmValues].pclUldPosition,pclPosition);
                 strcpy(prgCpmValues[igCurCpmValues].pclUldNumber,pclNumber);
                 strcpy(prgCpmValues[igCurCpmValues].pclUldDestination,pclDestination);
                 if (strcmp(pclDestination,pclHomeAirport) == 0)
                 {
                    strcpy(pclDest,pclDestination);
                 }
                 if (strlen(pclDestination) > 1)
                 {
                    ilDestFound = TRUE;
                 }
                 strcpy(prgCpmValues[igCurCpmValues].pclUldWeight,pclWeight);
                 ilTotalWeight += atoi(pclWeight);
                 strcpy(pclTmpBuf,pclType);
                 pclTmpPtrAddi = strstr(pclTmpBuf,".");
                 if (pclTmpPtrAddi == NULL)
                 {
                    strncpy(prgCpmValues[igCurCpmValues].pclUldType,pclTmpBuf,3);
                    prgCpmValues[igCurCpmValues].pclUldType[3] = '\0';
                    TrimRight(prgCpmValues[igCurCpmValues].pclUldType);
                    if (ilTypeIdx < ilVal-1)
                    {
                       strcpy(prgCpmValues[igCurCpmValues].pclUldAddi,"");
                       for (ilL = ilTypeIdx+1; ilL < ilVal; ilL++)
                       {
                          strcat(prgCpmValues[igCurCpmValues].pclUldAddi,&pclVal[ilL][0]);
                          strcat(prgCpmValues[igCurCpmValues].pclUldAddi,"/");
                       }
                       prgCpmValues[igCurCpmValues].pclUldAddi[strlen(prgCpmValues[igCurCpmValues].pclUldAddi)-1] = '\0';
                    }
                    else
                    {
                       strcpy(prgCpmValues[igCurCpmValues].pclUldAddi," ");
                    }
                 }
                 else
                 {
                    *pclTmpPtrAddi = '\0';
                    strncpy(prgCpmValues[igCurCpmValues].pclUldType,pclTmpBuf,3);
                    prgCpmValues[igCurCpmValues].pclUldType[3] = '\0';
                    TrimRight(prgCpmValues[igCurCpmValues].pclUldType);
                    pclTmpPtrAddi = strstr(pclItem,".");
                    strcpy(prgCpmValues[igCurCpmValues].pclUldAddi,pclTmpPtrAddi);
                 }
                 strcpy(pclTmpBuf,prgCpmValues[igCurCpmValues].pclUldType);
                 if (strlen(prgCpmValues[igCurCpmValues].pclUldAddi) > 1 &&
                     prgCpmValues[igCurCpmValues].pclUldAddi[0] != '.')
                 {
                    strcat(pclTmpBuf,"/");
                 }
                 strcat(pclTmpBuf,prgCpmValues[igCurCpmValues].pclUldAddi);
                 strcpy(prgCpmValues[igCurCpmValues].pclUldAddi,pclTmpBuf);
dbg(TRACE,"Pos: <%s>",prgCpmValues[igCurCpmValues].pclUldPosition);
dbg(TRACE,"Num: <%s>",prgCpmValues[igCurCpmValues].pclUldNumber);
dbg(TRACE,"Des: <%s>",prgCpmValues[igCurCpmValues].pclUldDestination);
dbg(TRACE,"Wei: <%s>",prgCpmValues[igCurCpmValues].pclUldWeight);
dbg(TRACE,"Typ: <%s>",prgCpmValues[igCurCpmValues].pclUldType);
dbg(TRACE,"Add: <%s>",prgCpmValues[igCurCpmValues].pclUldAddi);
dbg(TRACE,"Tot: <%d>",ilTotalWeight);
                 if (strlen(prgCpmValues[igCurCpmValues].pclUldDestination) > 3)
                    dbg(DEBUG,"%s Invalid Destination ==> Discard Line.",pclFunc);
                 else
                    igCurCpmValues++;
              }
              else
              {
                 if (GetNoOfElements(pclItem2,',') == 1 && igCurCpmValues > 0 &&
                     strstr(pclItem2,".NIL") == NULL)
                 {
                    strcat(prgCpmValues[igCurCpmValues-1].pclUldAddi,"-");
                    strcat(prgCpmValues[igCurCpmValues-1].pclUldAddi,pclItem2);
                 }
              }
           }
        }
        ilI++;
        pclTmpPtr = GetLine(&pclData[rgTlxInfo.TxtStart],ilI);
     }
     if (strlen(pclDest) > 0)
     {
        memset(&rlTlxResult,0x00,TLXRESULT);
        strcpy(rlTlxResult.DValue,pclDest);
        strcpy(rlTlxResult.FValue,pclDest);
        strcpy(rlTlxResult.DName,"Dest");
        CCSArrayAddUnsort(prgMasterTlx,&rlTlxResult,TLXRESULT);
     }
     else
     {
        if (ilDestFound == TRUE)
        {
           memset(&rlTlxResult,0x00,TLXRESULT);
           strcpy(rlTlxResult.DValue,pclHomeAirport);
           strcpy(rlTlxResult.FValue,pclHomeAirport);
           strcpy(rlTlxResult.DName,"Orig");
           CCSArrayAddUnsort(prgMasterTlx,&rlTlxResult,TLXRESULT);
        }
     }
     if (ilTotalWeight > 0)
     {
        sprintf(pclTmpBuf,"%d",ilTotalWeight);
        memset(&rlTlxResult,0x00,TLXRESULT);
        strcpy(rlTlxResult.DValue,pclTmpBuf);
        strcpy(rlTlxResult.FValue,pclTmpBuf);
        strcpy(rlTlxResult.DName,"Tweight");
        CCSArrayAddUnsort(prgMasterTlx,&rlTlxResult,TLXRESULT);
     }
  }

  /* Restore Character at SI Position */
  if (ilSIPosition > 0)
  {
     pclData[ilSIPosition] = clSIPosition;
     if (igGetULDNumbersFromSISectionForCPM == TRUE)
     {
        pclTmpPtr = NULL;
        pclBeginSI = pclData + ilSIPosition + 1;
        pclBeginSI = strstr(pclBeginSI,"\n");
        if (pclBeginSI != NULL)
        {
           pclBeginSI++;
           pclTmpPtr = strstr(pclBeginSI,"ULD NBRS");
           if (pclTmpPtr != NULL)
              pclBeginSI = pclTmpPtr;
           ilI = 1;
           pclTmpPtr = GetLine(pclBeginSI,ilI);
        }
        while (pclTmpPtr != NULL)
        {
           CopyLine(pclResult,pclTmpPtr);
dbg(TRACE,"Check Line: <%s>",pclResult);
           if (*pclResult == '-')
           {
              MakeTokenList(pclInfo,pclResult,"-",',');
              ilCount = GetNoOfElements(pclInfo,',');
              for (ilJ = 1; ilJ <= ilCount; ilJ++)
              {
                 GetDataItem(pclItem,pclInfo,ilJ,',',"","\0\0");
                 MakeTokenList(pclItem2,pclItem,"/",',');
                 if (GetNoOfElements(pclItem2,',') == 2)
                 {
                    GetDataItem(pclPos,pclItem2,1,',',"","\0\0");
                    GetDataItem(pclULDNo,pclItem2,2,',',"","\0\0");
                    if (isdigit(pclULDNo[0]))
                    {
                       for (ilK = 0; ilK < igCurCpmValues; ilK++)
                       {
                          if (strcmp(prgCpmValues[ilK].pclUldPosition,pclPos) == 0)
                          {
                             sprintf(pclTmpBuf,"%s%s",prgCpmValues[ilK].pclUldNumber,pclULDNo);
                             if (strlen(pclTmpBuf) >= SIZE_STRUCT_CPM_VALUES_PCL_ULD_NUM)
                             {
                               /*
                                 v.1.22 - truncate and set a marker to
                                 indicate continuation
                               */

                               pclTmpBuf [SIZE_STRUCT_CPM_VALUES_PCL_ULD_NUM] = '\0';
                               pclTmpBuf [SIZE_STRUCT_CPM_VALUES_PCL_ULD_NUM -1] = CPM_ULD_NUMBER_CONTINUATION_CHAR; 
                               dbg(TRACE,"%s ULD num SI portion truncated to <%s>",
                                 pclFunc,pclTmpBuf);
                             }
                             strcpy(prgCpmValues[ilK].pclUldNumber,pclTmpBuf);
                             
                             /* strcat(prgCpmValues[ilK].pclUldNumber,pclULDNo); */
                          }
                       }
                    }
                 }
              }
           }
           ilI++;
           pclTmpPtr = GetLine(pclBeginSI,ilI);
        }
     }
  }

  ilRC = SendTlxRes(FDI_CPM);

  return RC_SUCCESS ;
} /* end HandleCPM*/



static int SendCPMLOARes()
{
  int ilRC = RC_SUCCESS;
  int ilI;
  int ilJ;
  int ilDestCount = 0;
  int ilDestFound;
  int ilTypeCount = 0;
  int ilTypeFound;
  int ilDestTypeCount = 0;
  int ilDestTypeFound;
  int ilTotalWeight = 0;
  int ilCount;
  int ilInsert;
  int ilCnt;
  int ilMaxRec = 40;
  char pclWeight[16];
  char pclUldType[8];
  char pclUldAddi[256];
  char pclTmpBuf[256];
  char pclTable[8];
  char pclRes[8];
  char pclIdent[32];
  char pclFieldList[1024];
  char pclValueList[2048];
  char pclSelection[128];
  char pclFlnuRurn[128];
  char pclSqlBuf[1024];
  typedef struct
  {
     char pclDest[8];
     int ilWeight;
  } DEST_TOTAL;
  DEST_TOTAL rlDestTotal[40];
  typedef struct
  {
     char pclType[8];
     int ilWeight;
  } TYPE_TOTAL;
  TYPE_TOTAL rlTypeTotal[40];
  typedef struct
  {
     char pclDest[8];
     char pclType[8];
     int ilWeight;
  } DEST_TYPE_TOTAL;
  DEST_TYPE_TOTAL rlDestTypeTotal[40];

  T_TLXRESULT *prlTlxPtr = NULL;
  char *pclTmpPtr;
  char pclFunc[]="SendCPMLOARes:";
 
  memset(pclFieldList,0x00,sizeof(pclFieldList));
  memset(pclValueList,0x00,sizeof(pclValueList));
  memset(pclSelection,0x00,sizeof(pclSelection));

  /*    First delete "old" entries from table */
  prlTlxPtr = CCSArrayGetData(prgMasterTlx,"FltKey");
  if (prlTlxPtr  != NULL )
  {
     if (strlen(pcgAftUrno) > 0)
     {
        sprintf(pclSelection,"WHERE FLNU = %s AND DSSN = 'CPM'",pcgAftUrno);
     }
     else
     {
        sprintf(pclIdent,"%sCPM",prlTlxPtr->FValue);		
        sprintf(pclSelection,"WHERE IDNT = '%s'",pclIdent);
     }
     strcpy(pclSqlBuf,"DELETE FROM LOATAB ");
     strcat(pclSqlBuf,pclSelection);
     ilRC = FdiSendSql(pclSqlBuf);

     strcpy(pclFlnuRurn,",");
     if (strlen(pcgAftUrno) > 0)
     {
        strcat(pclFlnuRurn,pcgAftUrno);
     }
     else
     {
        strcat(pclFlnuRurn,"0");
     }
     strcat(pclFlnuRurn,",");
     if (strlen(pcgNextTlxUrno) > 0)
     {
        strcat(pclFlnuRurn,pcgNextTlxUrno);
     }
     else
     {
        strcat(pclFlnuRurn,"0");
     }

     strcpy(pclFieldList,"TIME,IDNT,TYPE,STYP,SSTP,APC3,ULDP,ULDT,ADDI,VALU,DSSN,FLNU,RURN");
     if (igNewLoatab == TRUE)
     {
        strcat(pclFieldList,",KEYC,LEVL,SORT,REMA");
        if (igNewLoatabUkey == TRUE)
           strcat(pclFieldList,",UKEY");
     }
     for (ilI = 0; ilI < igCurCpmValues; ilI++)
     {
        sprintf(pclValueList,"%s,%s,",pcgFlightSchedTime,pclIdent);
        strcat(pclValueList,"ULD,DTL,");
        strcat(pclValueList,prgCpmValues[ilI].pclUldType);
        strcat(pclValueList,",");
        strcat(pclValueList,prgCpmValues[ilI].pclUldDestination);
        strcat(pclValueList,",");
        strcat(pclValueList,prgCpmValues[ilI].pclUldPosition);
        strcat(pclValueList,",");
        strcat(pclValueList,prgCpmValues[ilI].pclUldNumber);
        strcat(pclValueList,",");
        strcat(pclValueList,prgCpmValues[ilI].pclUldAddi);
        strcat(pclValueList,",");
        strcat(pclValueList,prgCpmValues[ilI].pclUldWeight);
        strcat(pclValueList,",CPM");
        strcat(pclValueList,pclFlnuRurn);
        if (igNewLoatab == TRUE)
        {
           strcat(pclValueList,",");
           strcat(pclValueList,pcgCpmDtl);
        }
        if (igStoreCPMDetails == TRUE)
        {
           if (igArrayInsert == TRUE)
           {
              ilRC = FillArray(pclValueList);
           }
           else
           {
              ilRC = FdiHandleSql("IBT","LOATAB","",pclFieldList,pclValueList,
                                  pcgTwStart,pcgTwEndNew,FALSE,FALSE);
           }
/* split of ULD data with several types no longer required
           strcpy(pclUldType,prgCpmValues[ilI].pclUldType);
           strcpy(pclUldAddi,prgCpmValues[ilI].pclUldAddi);
           if (strlen(pclUldAddi) > 1 && *pclUldAddi != '.')
           {
              pclTmpPtr = strstr(pclUldAddi,".");
              if (pclTmpPtr != NULL)
              {
                 *pclTmpPtr = '\0';
              }
              ilCount = GetNoOfElements(pclUldAddi,'/');
              for (ilJ = 2; ilJ <= ilCount; ilJ++)
              {
                 GetDataItem(pclTmpBuf,pclUldAddi,ilJ,'/',"","\0\0");
                 ilInsert = TRUE;
                 if (strlen(pclTmpBuf) == 3)
                 {
                    sprintf(pclTable,"APT%s",pcgTABEnd);
                    ilCnt = 1;
                    ilRC = syslibSearchDbData(pclTable,"APC3",pclTmpBuf,
                                              "APC4",pclRes,&ilCnt,"\n");
                    if (ilRC == RC_SUCCESS || isdigit(*pclTmpBuf))
                    {
                       ilInsert = FALSE;
                    }
                 }
                 else
                 {
                    if (isdigit(*pclTmpBuf))
                    {
                       ilInsert = FALSE;
                    }
                 }
                 if (ilInsert == TRUE)
                 {
                    sprintf(pclValueList,"%s,%s,",pcgFlightSchedTime,pclIdent);
                    strcat(pclValueList,"ULD,DTL,");
                    strcat(pclValueList,pclTmpBuf);
                    strcat(pclValueList,",");
                    strcat(pclValueList,prgCpmValues[ilI].pclUldDestination);
                    strcat(pclValueList,",");
                    strcat(pclValueList,prgCpmValues[ilI].pclUldPosition);
                    strcat(pclValueList,",");
                    strcat(pclValueList,prgCpmValues[ilI].pclUldNumber);
                    strcat(pclValueList,",");
                    strcat(pclValueList,prgCpmValues[ilI].pclUldAddi);
                    strcat(pclValueList,",");
                    strcat(pclValueList,"0");
                    strcat(pclValueList,",CPM");
                    strcat(pclValueList,pclFlnuRurn);
                    if (igNewLoatab == TRUE)
                    {
                       strcat(pclValueList,",");
                       strcat(pclValueList,pcgCpmDtl);
                    }
                    if (igArrayInsert == TRUE)
                    {
                       ilRC = FillArray(pclValueList);
                    }
                    else
                    {
                       ilRC = FdiHandleSql("IBT","LOATAB","",pclFieldList,pclValueList,
                                           pcgTwStart,pcgTwEndNew,FALSE,FALSE);
                    }
                 }
                 else
                 {
                    ilJ = ilCount;
                 }
              }
           }
*/
        }
        ilDestFound = FALSE;
        for (ilJ = 0; ilJ < ilDestCount; ilJ++)
        {
           if (strcmp(rlDestTotal[ilJ].pclDest,prgCpmValues[ilI].pclUldDestination) == 0)
           {
              rlDestTotal[ilJ].ilWeight += atoi(prgCpmValues[ilI].pclUldWeight);
              ilTotalWeight += atoi(prgCpmValues[ilI].pclUldWeight);
              ilDestFound = TRUE;
           }
        }
        if (ilDestFound == FALSE)
        {
           strcpy(rlDestTotal[ilJ].pclDest,prgCpmValues[ilI].pclUldDestination);
           rlDestTotal[ilJ].ilWeight = atoi(prgCpmValues[ilI].pclUldWeight);
           ilTotalWeight += atoi(prgCpmValues[ilI].pclUldWeight);
	   if( ilDestCount < ilMaxRec ) 
               ilDestCount++;
        }
        ilTypeFound = FALSE;
        for (ilJ = 0; ilJ < ilTypeCount; ilJ++)
        {
           if (strcmp(rlTypeTotal[ilJ].pclType,prgCpmValues[ilI].pclUldType) == 0)
           {
              rlTypeTotal[ilJ].ilWeight += atoi(prgCpmValues[ilI].pclUldWeight);
              ilTypeFound = TRUE;
           }
        }
        if (ilTypeFound == FALSE)
        {
           strcpy(rlTypeTotal[ilJ].pclType,prgCpmValues[ilI].pclUldType);
           rlTypeTotal[ilJ].ilWeight = atoi(prgCpmValues[ilI].pclUldWeight);
	   if( ilTypeCount < ilMaxRec ) 
		ilTypeCount++;
        }
        ilDestTypeFound = FALSE;
        for (ilJ = 0; ilJ < ilDestTypeCount; ilJ++)
        {
           if (strcmp(rlDestTypeTotal[ilJ].pclDest,prgCpmValues[ilI].pclUldDestination) == 0 &&
               strcmp(rlDestTypeTotal[ilJ].pclType,prgCpmValues[ilI].pclUldType) == 0)
           {
              rlDestTypeTotal[ilJ].ilWeight += atoi(prgCpmValues[ilI].pclUldWeight);
              ilDestTypeFound = TRUE;
           }
        }
        if (ilDestTypeFound == FALSE)
        {
           strcpy(rlDestTypeTotal[ilJ].pclDest,prgCpmValues[ilI].pclUldDestination);
           strcpy(rlDestTypeTotal[ilJ].pclType,prgCpmValues[ilI].pclUldType);
           rlDestTypeTotal[ilJ].ilWeight = atoi(prgCpmValues[ilI].pclUldWeight);
	   if( ilDestTypeCount < ilMaxRec ) 
		ilDestTypeCount++;
        }
     }
     if (igStoreCPMDetails == TRUE)
     {
        strcpy(pclFieldList,"TIME,IDNT,TYPE,STYP,SSTP,APC3,ULDP,ULDT,ADDI,VALU,DSSN,FLNU,RURN");
        if (igNewLoatab == TRUE)
        {
           strcat(pclFieldList,",KEYC,LEVL,SORT,REMA");
           if (igNewLoatabUkey == TRUE)
              strcat(pclFieldList,",UKEY");
        }
        for (ilI = 0; ilI < ilDestCount; ilI++)
        {
           sprintf(pclValueList,"%s,%s,",pcgFlightSchedTime,pclIdent);
           strcat(pclValueList,"ULD,SD, ,");
           strcat(pclValueList,rlDestTotal[ilI].pclDest);
           strcat(pclValueList,", , , ,");
           sprintf(pclWeight,"%d",rlDestTotal[ilI].ilWeight);
           strcat(pclValueList,pclWeight);
           strcat(pclValueList,",CPM");
           strcat(pclValueList,pclFlnuRurn);
           if (igNewLoatab == TRUE)
           {
              strcat(pclValueList,",");
              strcat(pclValueList,pcgCpmApt);
           }
           if (igArrayInsert == TRUE)
           {
              ilRC = FillArray(pclValueList);
           }
           else
           {
              ilRC = FdiHandleSql("IBT","LOATAB","",pclFieldList,pclValueList,
                                  pcgTwStart,pcgTwEndNew,FALSE,FALSE);
           }
        }
        strcpy(pclFieldList,"TIME,IDNT,TYPE,STYP,SSTP,APC3,ULDP,ULDT,ADDI,VALU,DSSN,FLNU,RURN");
        if (igNewLoatab == TRUE)
        {
           strcat(pclFieldList,",KEYC,LEVL,SORT,REMA");
           if (igNewLoatabUkey == TRUE)
              strcat(pclFieldList,",UKEY");
        }
        for (ilI = 0; ilI < ilTypeCount; ilI++)
        {
           sprintf(pclValueList,"%s,%s,",pcgFlightSchedTime,pclIdent);
           strcat(pclValueList,"ULD,ST, , , , ,");
           strcat(pclValueList,rlTypeTotal[ilI].pclType);
           strcat(pclValueList,",");
           sprintf(pclWeight,"%d",rlTypeTotal[ilI].ilWeight);
           strcat(pclValueList,pclWeight);
           strcat(pclValueList,",CPM");
           strcat(pclValueList,pclFlnuRurn);
           if (igNewLoatab == TRUE)
           {
              strcat(pclValueList,",");
              strcat(pclValueList,pcgCpmTyp);
           }
           if (igArrayInsert == TRUE)
           {
              ilRC = FillArray(pclValueList);
           }
           else
           {
              ilRC = FdiHandleSql("IBT","LOATAB","",pclFieldList,pclValueList,
                                  pcgTwStart,pcgTwEndNew,FALSE,FALSE);
           }
        }
        strcpy(pclFieldList,"TIME,IDNT,TYPE,STYP,SSTP,APC3,ULDP,ULDT,ADDI,VALU,DSSN,FLNU,RURN");
        if (igNewLoatab == TRUE)
        {
           strcat(pclFieldList,",KEYC,LEVL,SORT,REMA");
           if (igNewLoatabUkey == TRUE)
              strcat(pclFieldList,",UKEY");
        }
        for (ilI = 0; ilI < ilDestTypeCount; ilI++)
        {
           sprintf(pclValueList,"%s,%s,",pcgFlightSchedTime,pclIdent);
           strcat(pclValueList,"ULD,STD,");
           strcat(pclValueList,rlDestTypeTotal[ilI].pclType);
           strcat(pclValueList,",");
           strcat(pclValueList,rlDestTypeTotal[ilI].pclDest);
           strcat(pclValueList,", , , ,");
           sprintf(pclWeight,"%d",rlDestTypeTotal[ilI].ilWeight);
           strcat(pclValueList,pclWeight);
           strcat(pclValueList,",CPM");
           strcat(pclValueList,pclFlnuRurn);
           if (igNewLoatab == TRUE)
           {
              strcat(pclValueList,",");
              strcat(pclValueList,pcgCpmTypApt);
           }
           if (igArrayInsert == TRUE)
           {
              ilRC = FillArray(pclValueList);
           }
           else
           {
              ilRC = FdiHandleSql("IBT","LOATAB","",pclFieldList,pclValueList,
                                  pcgTwStart,pcgTwEndNew,FALSE,FALSE);
           }
        }
     }
     strcpy(pclFieldList,"TIME,IDNT,TYPE,STYP,SSTP,APC3,ULDP,ULDT,ADDI,VALU,DSSN,FLNU,RURN");
     if (igNewLoatab == TRUE)
     {
        strcat(pclFieldList,",KEYC,LEVL,SORT,REMA");
        if (igNewLoatabUkey == TRUE)
           strcat(pclFieldList,",UKEY");
     }
     sprintf(pclValueList,"%s,%s,",pcgFlightSchedTime,pclIdent);
     if (igStoreCPMDetails == TRUE)
     {
        strcat(pclValueList,"ULD,T, , , , , ,");
     }
     else
     {
        strcat(pclValueList,"LOA,C, , , , , ,");
     }
     sprintf(pclWeight,"%d",ilTotalWeight);
     strcat(pclValueList,pclWeight);
     strcat(pclValueList,",CPM");
     strcat(pclValueList,pclFlnuRurn);
     if (igNewLoatab == TRUE)
     {
        strcat(pclValueList,",");
        strcat(pclValueList,pcgCpmTot);
     }
     if (igArrayInsert == TRUE)
     {
        ilRC = FillArray(pclValueList);
     }
     else
     {
        ilRC = FdiHandleSql("IBT","LOATAB","",pclFieldList,pclValueList,
                            pcgTwStart,pcgTwEndNew,FALSE,FALSE);
     }
     if (strlen(pcgLoaBuf) > 0 && igArrayInsert == TRUE)
     {
        igArrayPtr--;
        pcgLoaBuf[igArrayPtr] = '\0';
        strcpy(pclFieldList,"URNO,TIME,IDNT,TYPE,STYP,SSTP,APC3,ULDP,ULDT,ADDI,VALU,DSSN,FLNU,RURN");
        strcpy(pclValueList,":VURNO,:VTIME,:VIDNT,:VTYPE,:VSTYP,:VSSTP,:VAPC3,:VULDP,:VULDT,:VADDI,:VVALU,:VDSSN,:VFLNU,:VRURN");
        if (igNewLoatab == TRUE)
        {
           strcat(pclFieldList,",KEYC,LEVL,SORT,REMA");
           strcat(pclValueList,",:VKEYC,:VLEVL,:VSORT,:VREMA");
           if (igNewLoatabUkey == TRUE)
           {
              strcat(pclFieldList,",UKEY");
              strcat(pclValueList,",:VUKEY");
           }
        }
        ilRC = SqlArrayInsert(pclFieldList,pclValueList,pcgLoaBuf);
     }
  }
  else
  {
     dbg(DEBUG,"%s No FltKey avail.",pclFunc); 
  }    
  return RC_SUCCESS;
} /* End of SendCPMLOARes */

