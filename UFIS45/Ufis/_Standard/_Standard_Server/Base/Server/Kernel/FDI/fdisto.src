#ifndef _DEF_mks_version_fdisto
  #define _DEF_mks_version_fdisto
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_fdisto[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/FDI/fdisto.src 1.9 2009/11/20 17:04:57SGT akl Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  AKL                                                      */
/* Date           :                                                           */
/* Description    :  INCLUDE PART FROM FDIHDL                                 */
/*                   Miscellaneous Messages                                   */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* FDISTO handle functions                                                    */
/* -----------------------                                                    */
/* static int HandleSCR (char *pcpData)                                       */
/* static int HandleBTM (char *pcpData)                                       */
/* static int HandleBAY (char *pcpData)                                       */


static int HandleSCR(char *pcpData)
{
  int ilRC;
  int ilLine;
  int ilStart;
  int ilEnd;
  int ilLen;
  char pclFunc[]="HandleSCR:";
  char *pclData;
  char pclResult[100];
  char *pclTmpResult;
  char pclTmpDate[100];
  char pclMonth[8] = "";
  char pclDay[8] = "";
  T_TLXINFOCMD *prlInfoCmd;
  T_TLXRESULT *prlResult;
  T_TLXRESULT prlTlxMonth;
  T_TLXRESULT prlTlxDay;

  memset(&prlTlxMonth,0x00,TLXRESULT);
  memset(&prlTlxDay,0x00,TLXRESULT);

  prlInfoCmd = &prgInfoCmd[0][0];

  pclData = pcpData;

  dbg(DEBUG,"%s Extract Flight Line",pclFunc); 

  ilLine = 1;
  ilRC = 0;
  /* i < 10 => flight line has to be within the following 10 lines   */
  /* and char. length > 1 */
  while (ilRC  < 2 && ilLine< 10)
  {
     /* Search for Home Airport  */
     ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
     if (strcmp(pclResult,pcgHomeAP) != 0)
     {
        ilRC = 0;
     }
     ilLine++;
  }
  /* Read Flight Line */
  ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");

  if (ilRC > 0)
  {
     dbg(DEBUG,"%s Found Flight Line <%s>",pclFunc,pclResult); 
     ConvertFlightLine(&pclResult[1]);
     pclTmpResult = pclResult + 1;
     if (*pclTmpResult == ' ')
     {
        pclTmpResult += 1;
     }
     ilRC = GetAlc(&pclTmpResult,prgMasterTlx);
     ilRC = GetFltn(&pclTmpResult,prgMasterTlx);
     ilLen = strlen(pclTmpResult);
     ilStart = 0;
     while (pclTmpResult[ilStart] == ' ' && ilStart <= ilLen)
     {
        ilStart++;
     }
     ilLen = strlen(&pclTmpResult[ilStart]);
     ilEnd = ilStart + 1;
     while (pclTmpResult[ilEnd] != ' ' && ilEnd <= ilLen)
     {
        ilEnd++;
     }
     strncpy(pclTmpDate,&pclTmpResult[ilStart],ilEnd-ilStart);
     pclTmpDate[ilEnd-ilStart] = '\0';
     if (strlen(pclTmpDate) < 10)
     {   /* 2nd search */
        ilLen = strlen(&pclTmpResult[ilEnd]);
        ilStart = ilEnd + 1;
        while (pclTmpResult[ilStart] == ' ' && ilStart <= ilLen)
        {
           ilStart++;
        }
        ilLen = strlen(&pclTmpResult[ilStart]);
        ilEnd = ilStart + 1;
        while (pclTmpResult[ilEnd] != ' ' && ilEnd <= ilLen)
        {
           ilEnd++;
        }
        strncpy(pclTmpDate,&pclTmpResult[ilStart],ilEnd-ilStart);
        pclTmpDate[ilEnd-ilStart] = '\0';
     }
     if (strlen(pclTmpDate) == 10)
     {
        strncpy(pclDay,pclTmpDate,2);
        pclDay[2] = '\0';
        strncpy(pclMonth,&pclTmpDate[2],3);
        pclMonth[3] = '\0';
        if (strcmp(pclMonth,"JAN") == 0)
           strcpy(pclMonth,"01");
        else
        if (strcmp(pclMonth,"FEB") == 0)
           strcpy(pclMonth,"02");
        else
        if (strcmp(pclMonth,"MAR") == 0)
           strcpy(pclMonth,"03");
        else
        if (strcmp(pclMonth,"APR") == 0)
           strcpy(pclMonth,"04");
        else
        if (strcmp(pclMonth,"MAY") == 0)
           strcpy(pclMonth,"05");
        else
        if (strcmp(pclMonth,"JUN") == 0)
           strcpy(pclMonth,"06");
        else
        if (strcmp(pclMonth,"JUL") == 0)
           strcpy(pclMonth,"07");
        else
        if (strcmp(pclMonth,"AUG") == 0)
           strcpy(pclMonth,"08");
        else
        if (strcmp(pclMonth,"SEP") == 0)
           strcpy(pclMonth,"09");
        else
        if (strcmp(pclMonth,"OCT") == 0)
           strcpy(pclMonth,"10");
        else
        if (strcmp(pclMonth,"NOV") == 0)
           strcpy(pclMonth,"11");
        else
        if (strcmp(pclMonth,"DEC") == 0)
           strcpy(pclMonth,"12");
        strcpy(prlTlxDay.DName,"FDate");
        strcpy(prlTlxDay.DValue,pclDay);
        strcpy(prlTlxDay.FValue,pclDay);
        CCSArrayAddUnsort(prgMasterTlx,&prlTlxDay,TLXRESULT);
        strcpy(prlTlxMonth.DName,"FMonth");
        strcpy(prlTlxMonth.DValue,pclMonth);
        strcpy(prlTlxMonth.FValue,pclMonth);
        CCSArrayAddUnsort(prgMasterTlx,&prlTlxMonth,TLXRESULT);
     }
  }

/*
  if (ilRC == RC_SUCCESS)
  {
     ilRC = SendTlxRes(FDI_SCR);
  }
*/
  cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;

  return ilRC;
} /* end of HandleSCR  */


static int HandleBTM(char *pcpData)
{
  int ilRC = RC_SUCCESS;

  cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;

  return ilRC;
} /* end of HandleBTM  */


static int HandleBAY(char *pcpData)
{
  int ilRC = RC_SUCCESS;

  cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;

  return ilRC;
} /* end of HandleBAY  */



