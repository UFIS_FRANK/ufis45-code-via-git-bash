
#ifndef _DEF_mks_version_fdipin
  #define _DEF_mks_version_fdipin
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_fdipin[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/FDI/fdipin.src 1.03 2013/06/03 15:45:24SGT dka Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  SMI                                                      */
/* Date           :                                                           */
/* Description    : INCLUDE PART FROM FDIHDL                                  */
/*                                                                            */
/* Update history :                                                           */
/* 20121002 DKA v.1.00 UFIS-2052 - from an AUH requirement.                   */
/*                     Similar to Kriscom ATPIN message. For first release    */
/*                     summation of arrival flights not performed, instead    */
/*                     only extract the summary totals per class from the     */
/*                     Flight Line.					      */
/*                     Named "fdipin" because the keyword in the message is   */
/*                     "PAX WITH INBOUND CONNECTION".                         */
/*		It appears that a clash should not occur with Kriscom's ATPIN */
/*		despite at least one airline also sending the key phrase      */
/*		above. This is because KRISCOM messages are found in 	      */
/*		FindTlxCmd with the synonym "SOURCE=>KRISCOM". 		      */
/* 20121206 DKA 1.01	Bug fix reqd after release to production.	      */
/*		"=TEXT" was missed, and also the "PART" check was missed.     */
/*		The synonym for dotIA cannot be "//MSG" as that is used for   */
/*		other telexes. But cannot use "PAX WITH INBOUND CONNECTION"   */
/*		without quite a bit of tweak of fdihdl.c because the command  */
/*		is searched only from the start of each line.		      */
/*		So try to look for this string in the body of the telex.      */
/*		Due to a quirk in that configured commands are added to the   */
/*		master array in the same (not sorted) order as they appear in */
/*		[TELEX].commands, the synonym "//MSG" will be searched last   */
/*		because PIN appears last in that list. 			      */
/* 20130214 DKA 1.02	Convert C++ style comments to C-style for HPUX        */
/*              compilation.						      */
/* 20130603 DKA 1.03	Cater for change in fdihdl.1.171 in passing of flight */
/*		line to this sub-module. A line-by-line search for the flight */
/* 		will not be performed as it will be extracted and saved in    */
/*		fdihdl:FindTlxCmd. Set a flag for if the "Part" is found.     */
/*		Left the previous code commented in case the parsing is re-   */
/*		to go through complete pax list as done in PTM.		      */
/*		Decided to skip checking the "PART" number as that would req  */
/*		fdihdl.c to save the line behind flight line as well, and, in */
/*		samples from AUH, the same line as the synonym-phrase has the */
/*		complete pax figures as well as the dep flight.		      */
/*									      */
/**                                                                           */
/******************************************************************************/
/*                                                                            */
/* FDIPIN handle functions                                                    */
/* -----------------------                                                    */
/* static int  HandlePIN(char *pcpData)                                       */
/* static int ExtractPINvalues (char *pcpLine)                                */
/* static int SendPINLOARes(void)                                                 */
/* static int StorePINData(char *pcpIdent,char *pcpFlnuRurn,char *pcpType,    */
/*                         char *pcpValue,char *pcpFieldList,char *pcpRecCont)*/ 
/* void       InitPinValues (ATPIN_VALUES *s_dpi_val); 			      */

static int FindPinFlight (char *pcpFlno, char *pcpFltDate, char *pcpAftUrno);

static char pcgPinStod[16];

static int HandlePIN(char *pcpData)
{
  int ilRC = RC_SUCCESS;
  int ilI;
  int ilCount;
  char pclFunc[]="HandlePIN:";
  char *pclData;
  char pclResult[512], pclResult2 [512];
  char pclFlno [16], pclFltDayMonth [16], *pclPart, pclPartNo [16];
  char pclAftUrno[4];
  T_TLXRESULT prlTlxResult;
  char *pclTmpPtr;
  char pclLine[1024];
  char pclTmpLine[512];
  char pclFlightLine [128];
  char pclF [8], pclJ [8], pclY [8];
  char *pclBegin;
  char *pclEnd;
  char pclInfo[128];
  int ilMemNo;
  char pclItem[128];
  char pclTable[8];
  char pclTmpBuf[128];
  int ilCnt, jj,kk,ilNoPaxClassTotals, ilFltLineNo, ilFoundFlightLine;
  char pclPaxClassTotals[64], pclPaxTotVal [8];
  char pclDummyFlightLine [32], pclTlxDate [32];
  int  ilFoundPartSpec = 0;

  InitRecordDescriptor(&rgRecDesc,1024,0,FALSE);
  memset(pcgLoaBuf,0x00,1000*512);
  igArrayPtr = 0;
  InitPinValues ();
  ilFoundFlightLine = FALSE;


  /*
    We have day and month which we treat as UTC.
    Flight number needs to be normalized to UFIS format. 
  */
  
  igTrustTlxDate = TRUE;

  pclData = pcpData;
  dbg(DEBUG,"%s Extract Flight Line",pclFunc);
  ilI = 1;
  ilRC = 0;
  ilFoundPartSpec = 0;
  /* while (ilRC  < 2 && ilI < 14) */
  /* while (ilI < 14) */
  /* { */
     /* Search for Flight line  */
     /* ilRC = GetDataItem(pclResult,&pclData[rgTlxInfo.TxtStart],ilI,'\n',"","  ");
     ilI++; */
  
     /* dbg(DEBUG,"%s ilRC <%d> ilI <%d> Read <%s>",pclFunc,ilRC,ilI,pclResult); */
/*     if (strstr(pclResult,"=TEXT") != '\0') */
/*     {  */
/*      dbg(TRACE,"%s Found =TEXT",pclFunc);  */
/*     ilRC = GetDataItem(pclResult,&pclData[rgTlxInfo.TxtStart],ilI,'\n',"","  ");  */
/*     ilI++;   */
       /* after "=TEXT" should be e.g. "//MSG 231 PART 1" */
       /* have to do extra work to prevent treating 'PART 12' as 'PART 1' */
       /* dbg(TRACE,"%s Line After TEXT <%s>",pclFunc,pclResult); */


/*       strcpy (pclResult,pcgPinFlightLine);
       pclPart = strstr (pclResult,"PART");
       if (pclPart != '\0')
       { 
         ilFoundPartSpec = 1;
         strcpy (pclPartNo, pclPart); 
         kk = 4; */  /* just after the 'T' of 'PART' */
/*         dbg(TRACE,"%s About to enter while() to check PART",pclFunc);
         while (pclPartNo [kk] != '\0')
         {
           if (pclPartNo [kk] != ' ')
             break;

           kk++; 
         }
         if (pclPartNo [kk] == '1')
         {
           if ((pclPartNo [kk + 1] == '\0')
                || (pclPartNo [kk + 1] == ' ')
                || (pclPartNo [kk + 1] == '\n')) 
           {
             dbg(TRACE,"%s Telex is 1st part <%s> - continue processing",
              pclFunc, pclResult); 
             ilRC = GetDataItem(pclResult,&pclData[rgTlxInfo.TxtStart],ilI,'\n',"","  ");
             ilI++;  */
             /* break;  */ /* from while loop */
/*           }
           else
           {
             dbg(TRACE,"%s Telex is not 1st part <%s> so stop processing",
               pclFunc,pclResult); 
             cgTlxStatus = FDI_STAT_ERROR;
             return RC_SUCCESS;
           }
         }
         else
         {
           dbg(TRACE,"%s Telex is not 1st part <%s> so stop processing",
             pclFunc,pclResult); 
           cgTlxStatus = FDI_STAT_ERROR;
           return RC_SUCCESS; 
         }
       }  */
   /* } */
/*} */

/*if (ilFoundPartSpec == 0)
  {
    dbg(TRACE,"%s Could not find PART no.,stop processing",pclFunc); 
    cgTlxStatus = FDI_STAT_ERROR;
    return RC_SUCCESS; 
  }
*/
  /* Allow up to 4 empty lines before Flight Line */
  /* v.1.03 - flight line was extracted in fdihdl:FindTlxCmd */
/*for (kk = 0; kk < 4; kk++)
  {
    if (!isalnum (pclResult[0]))
    {
      ilRC = GetDataItem(pclResult,&pclData[rgTlxInfo.TxtStart],ilI,'\n',"","  ");
      ilI++; 
    }
  }

  if (!isalnum (pclResult[0]))
  {
    dbg(TRACE,"%s Could not find flight line so stop processing",
      pclFunc,pclResult); 
    cgTlxStatus = FDI_STAT_ERROR;
    return RC_SUCCESS;
  } */

  strcpy (pclResult,pcgPinFlightLine); /* v.1.03 */
  strcpy(pclFlightLine,pclResult); 

  dbg(TRACE,"%s Flight Line <%s>",pclFunc,pclResult); 
  ilFltLineNo = MakeTokenList (pclResult2,pclFlightLine," ",',');
  ilRC = GetDataItem(pclFlno,pclResult2,1,',',"","  ");
  dbg(TRACE,"%s Flight Number from telex <%s>",pclFunc,pclFlno);  
  strcpy (pclDummyFlightLine,pclFlno); 
  CheckFlightNo (pclFlno); /* in fdiadf, normalizes given flight no to UFIS format */
				
  dbg(TRACE,"%s Flight Number normalised <%s>",pclFunc,pclFlno);  

  ilRC = GetDataItem(pclTlxDate,pclResult2,2,',',"","  ");
  strcat(pclDummyFlightLine,"/");
  strcat(pclDummyFlightLine,pclTlxDate);
  strcat(pclDummyFlightLine,".XXXXXX.YYY");
  /* Must call PruneFlight or equiv so that MakeFltKey will succeed */
  PruneFlight(pclResult,prgMasterTlx,0); /* '0' because no REGN */

  GetFlightDate (pclResult2,2,1); 
  dbg(TRACE,"%s Flight Date <%s>",pclFunc,pcgFltDate); 

  if (FindPinFlight (pclFlno, pcgFltDate, pclAftUrno) != RC_SUCCESS)
  {
    dbg(TRACE,"%s No flight found, stop processing this telex",pclFunc);
    return RC_FAIL; 
  } 

  /* set "Board" so MakeFltKey will add adid to the key */
  memset(&prlTlxResult,0x00,TLXRESULT);
  strcpy(prlTlxResult.DValue,pcgHomeAP);
  strcpy(prlTlxResult.FValue,prlTlxResult.DValue);
  sprintf(prlTlxResult.DName,"Board");
  CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);

  MakeFltKey();

  memset(&prlTlxResult,0x00,TLXRESULT);
  strcpy(prlTlxResult.DValue,pcgHomeAP);
  strcpy(prlTlxResult.FValue,prlTlxResult.DValue);
  sprintf(prlTlxResult.DName,"Orig");
  CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);
  dbg(TRACE,"%s Orig <%s>",pclFunc,prlTlxResult.DValue);
  dbg(TRACE,"%s TlxOriginator <%s>",pclFunc,pcgTlxOriginator);
  
  /* first release just take the summary in the flight line */
  pclTmpPtr = strstr (pclFlightLine,"CONNECTION");
  strcpy (pclResult2,pclTmpPtr);

  ilNoPaxClassTotals = MakeTokenList (pclPaxClassTotals,&pclResult2[10]," ",',');
  dbg(TRACE,"%s Pax Classes <%s> count <%d>",pclFunc,pclPaxClassTotals,ilNoPaxClassTotals);

  for (kk = 1; kk <= ilNoPaxClassTotals; kk++)
  {
    ilRC = GetDataItem(pclPaxTotVal,pclPaxClassTotals,kk,',',"","  ");   
    if (strstr (pclPaxTotVal,"F") != '\0')
    { 
      sprintf (prgPinValues [0].pclTotFirst,"%d",(atoi(pclPaxTotVal)));
      dbg(TRACE,"%s Total First <%s>",pclFunc,prgPinValues [0].pclTotFirst); 
    }

    if (strstr (pclPaxTotVal,"J") != '\0')
    { 
      sprintf (prgPinValues [0].pclTotBusiness,"%d",(atoi(pclPaxTotVal)));
      dbg(TRACE,"%s Total Business <%s>",pclFunc,prgPinValues [0].pclTotBusiness); 
    }

    if (strstr (pclPaxTotVal,"Y") != '\0')
    { 
      sprintf (prgPinValues [0].pclTotEconomy,"%d",(atoi(pclPaxTotVal)));
      dbg(TRACE,"%s Total Economy <%s>",pclFunc,prgPinValues [0].pclTotEconomy); 
    }
  }

  sprintf (prgPinValues [0].pclTotTotal,"%d",
    ((atoi(prgPinValues [0].pclTotFirst)) + 
    (atoi(prgPinValues [0].pclTotBusiness)) +
    (atoi(prgPinValues [0].pclTotEconomy))));

  igCurPinValues = 1;

  ilRC = SendTlxRes(rgTlxInfo.TlxCmd); 

  return ilRC;
} /* End of HandlePIN */

/****************************************************************************
 *
 * SendPINLOARes 
 *
 *
 ****************************************************************************
 */

static int SendPINLOARes(void)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[]="SendPINLOARes:";
  char pclFieldList[1000];
  char pclValueList[1000];
  char pclSelection[100];
  char pclIdent[32];
  char pclFlnuRurn[100];
  char pclSqlBuf[1024];
  T_TLXRESULT *prlTlxPtr = NULL;
  int ilI;
  int ilTotal;
  char pclTotal[16];
  char pclTmpBuf[1024];
  int ilFound;
  char pclDiff[8];
  int ilCrewTech;
  
  if (cgTlxStatus != FDI_STAT_FLT_UPDATED)
  {
    dbg(TRACE,"%s Status is <%c> so nothing to do",pclFunc,cgTlxStatus);
    return RC_SUCCESS;
  }
  
  memset(pclFieldList,0x00,sizeof(pclFieldList));
  memset(pclSelection,0x00,sizeof(pclSelection));

  /*    First delete "old" entries from table */
  prlTlxPtr = CCSArrayGetData(prgMasterTlx,"FltKey");
  sprintf(pclIdent,"%sPIN",prlTlxPtr->FValue);

  if (prlTlxPtr  != NULL )
  { 
     if (strlen(pcgAftUrno) > 0)
     {
        sprintf(pclSelection,"WHERE FLNU = %s AND DSSN = 'PIN'",pcgAftUrno);

     }
     else
     {
       sprintf(pclSelection,"WHERE IDNT = '%s'",pclIdent);
     }

     strcpy(pclSqlBuf,"DELETE FROM LOATAB ");
     strcat(pclSqlBuf,pclSelection);
     ilRC = FdiSendSql(pclSqlBuf);

     strcpy(pclFlnuRurn,",");
     if (strlen(pcgAftUrno) > 0)
     {
        strcat(pclFlnuRurn,pcgAftUrno);
     }
     else
     {
        strcat(pclFlnuRurn,"0");
     }
     strcat(pclFlnuRurn,",");
     if (strlen(pcgNextTlxUrno) > 0)
     {
        strcat(pclFlnuRurn,pcgNextTlxUrno);
     }
     else
     {
        strcat(pclFlnuRurn,"0");
     }

    strcpy(pclFieldList,"TIME,IDNT,TYPE,STYP,SSTP,SSST,APC3,VALU,DSSN,FLNU,RURN");
     if (igNewLoatab == TRUE) /* Is this nexcessary ? */
     {
       strcat(pclFieldList,",KEYC,LEVL,SORT,REMA");
       if (igNewLoatabUkey == TRUE)
         strcat(pclFieldList,",UKEY");
     }

     /*
        This is not exactly identical to Kriscom, there is a possibility that
        Kriscom total pax is saved incorrectly? Reason is the KRIS saves total
        as PAX,F, , , even though ATPIN is only transfer pax.
     */
     if (igCurPinValues >= 0)
     {
       /* Transfer pax first class */
       ilRC = StorePINData(pclIdent,pclFlnuRurn,"PAX,F,T, , ,", 
         prgPinValues[0].pclTotFirst,pclFieldList,pcgPtmTrfFclPax);
  
       /* Transfer pax business class */
       ilRC = StorePINData(pclIdent,pclFlnuRurn,"PAX,B,T, , ,", 
         prgPinValues[0].pclTotBusiness,pclFieldList,pcgPtmTrfBclPax);
      
       /* Transfer pax economy class */
       ilRC = StorePINData(pclIdent,pclFlnuRurn,"PAX,E,T, , ,", 
         prgPinValues[0].pclTotEconomy,pclFieldList,pcgPtmTrfEclPax);

       /* Total transfer (per PTM)  */ 
       ilRC = StorePINData(pclIdent,pclFlnuRurn,"PAX,T,T, , ,",
         prgPinValues[0].pclTotTotal,pclFieldList,pcgPtmTrfPax);
     }

     if ((strlen(pcgLoaBuf) > 0 && igArrayInsert == TRUE))
     {
        igArrayPtr--;
        pcgLoaBuf[igArrayPtr] = '\0';
        strcpy(pclFieldList,"URNO,TIME,IDNT,TYPE,STYP,SSTP,SSST,APC3,VALU,DSSN,FLNU,RURN");
        strcpy(pclValueList,":VURNO,:VTIME,:VIDNT,:VTYPE,:VSTYP,:VSSTP,:VSSST,:VAPC3,:VVALU,:VDSSN,:VFLNU,:VRURN");
        if (igNewLoatab == TRUE)
        {
           strcat(pclFieldList,",KEYC,LEVL,SORT,REMA");
           strcat(pclValueList,",:VKEYC,:VLEVL,:VSORT,:VREMA");
           if (igNewLoatabUkey == TRUE)
           {
              strcat(pclFieldList,",UKEY");
              strcat(pclValueList,",:VUKEY");
           }
        }
        ilRC = SqlArrayInsert(pclFieldList,pclValueList,pcgLoaBuf);
     } 
  }
  else
  {
     dbg(DEBUG,"%s No FltKey avail.",pclFunc);
  }
  return RC_SUCCESS;
} /* End of SendPINLOARes */

/*****************************************************************************
 *
 * StorePINData - based on StoreLDMDataTotal by AKL.
 * Re-use this because no APC3 needed.
 *
 ****************************************************************************/

static int StorePINData(char *pcpIdent,char *pcpFlnuRurn,char *pcpType,
                           char *pcpValue,char *pcpFieldList,char *pcpRecCont)
{
  char pclFunc [] = "StorePINData: ";
  int ilRC = RC_SUCCESS;
  char pclValueList[2000];
  /* char pclValue[16]; */
  char pclTypeLdmTab [32];
  int  jj,iCommaCount;
  
  memset(pclValueList,0x00,sizeof(pclValueList));
  
  if (strlen (pcpValue) > 0)
  { 
     sprintf(pclValueList,"%s,%s,",pcgFlightSchedTime,pcpIdent);
     strcat(pclValueList,pcpType);

     strcat(pclValueList,pcpValue);
     strcat(pclValueList,",PIN");
     strcat(pclValueList,pcpFlnuRurn);
     if (igNewLoatab == TRUE)
     {
        strcat(pclValueList,",");
        strcat(pclValueList,pcpRecCont);
     }

     if (igArrayInsert == TRUE)
     {
          ilRC = FillArray(pclValueList);
     }
     else
     {
          ilRC = FdiHandleSql("IBT","LOATAB","",pcpFieldList,pclValueList,
                              pcgTwStart,pcgTwEndNew,FALSE,FALSE);
     }

  }
  return RC_SUCCESS;
} /* End of StorePINData */


/****************************************************************************
 *
 * InitPinValues  
 *
 ****************************************************************************
 */
void InitPinValues (void)
{
  char	pclFunc[] = "InitPinValues: ";

/*  strcpy (prgPinValues[0].pclTotFirst,"");
    strcpy (prgPinValues[0].pclTotBusiness,"");
    strcpy (prgPinValues[0].pclTotEconomy,"");
    strcpy (prgPinValues[0].pclTotTotal,""); */

  memset(&prgPinValues,0x00,sizeof(prgPinValues));
   
  igCurPinValues = -1;
  
}

/****************************************************************************
 *
 * FindPinFlight - returns RC_SUCCESS if found else RC_FAIL
 * We look only for departures. Assume the date in the dot IA is UTC.
 *
 * IMPORTANT:  copied some code from fdipsc to cater for test of old telexes
 * and frozen states. This is a mess. IMO, FDIHDL is a prime candidate to
 * be replaced with C++, and split into a number of discrete programs.
 *
 * Also, just noticed that need to allocate/assign the next TLXTAB urno here..
 *
 ****************************************************************************
 */

static int FindPinFlight (char *pcpFlno, char *pcpFltDate, char *pcpAftUrno)
{
  int ilRC = RC_FAIL, ilDbRc;
  char pclFunc [] = "FindPinFlight: ";
  char pclSqlQuery [512];
  char pclDataBuf [64];
  char pclAftDqcs [16];
  int  ilDebugLevel;

  if (igIgnoreOldTelexes == TRUE && igTlxTooOld == TRUE)
  {
    cgTlxStatus = FDI_STAT_TLX_TOO_OLD;
    dbg(DEBUG,"%s Telex is too old",pclFunc);
  }
  else
  {
    strcpy (pclDataBuf,"");
    if (igGcftabDcqEnabled == FALSE)
    {
      sprintf (pclSqlQuery,
        "SELECT URNO,STOD FROM AFTTAB WHERE FLNO ='%s' AND ADID='D' AND STOD LIKE '%s%%'",
        pcpFlno,pcpFltDate);
    }
    else /* add data quality check field */
    {
      sprintf (pclSqlQuery,
        "SELECT URNO,STOD,DQCS FROM AFTTAB WHERE FLNO ='%s' AND ADID='D' AND STOD LIKE '%s%%'",
        pcpFlno,pcpFltDate);
    }
             

    dbg(TRACE,"%s SQL <%s>",pclFunc,pclSqlQuery);
    ilDbRc = RunSQL (pclSqlQuery,pclDataBuf);

    if (ilDbRc == DB_SUCCESS)
    {
      if (igGcftabDcqEnabled == TRUE)
        BuildItemBuffer(pclDataBuf,"",3,",");
      else
        BuildItemBuffer(pclDataBuf,"",2,",");
      (void) get_real_item (pcgAftUrno,pclDataBuf,1);
      (void) get_real_item (pcgPinStod,pclDataBuf,2);
      strcpy(pcgFlightSchedTime,pcgPinStod); /* do it here for MakeFltKey */
      dbg(TRACE,"%s Found Flight in AFTTAB <%s><D><%s>",pclFunc,pcgAftUrno,pcgPinStod);
      ilRC = RC_SUCCESS; 

      /* copied from fdipsc */
      strcpy(pcgAftAdid,"D");
      strcpy(pcgFlightNumber,pcpFlno);
      cgTlxStatus = FDI_STAT_FLT_UPDATED;
      if (igGcftabDcqEnabled == TRUE)
      {
        (void) get_real_item(pclAftDqcs,pclDataBuf,3);
        (void) CheckDcqLocked (pclAftDqcs);
        if ((cgTlxStatus == FDI_STAT_FLT_UPDATED)
          && (cgAftDcqsStatus == FDI_STAT_FLIGHT_FROZEN))
        {
          dbg(TRACE,
            "%s === TELEX STAT CHANGED FM [UPDATED => FROZEN] ===",pclFunc);
          cgTlxStatus = FDI_STAT_FLIGHT_FROZEN;
        }
      } 

      /* also copied from fdipsc */
      if (igCurTlxUrnoIdx < igMaxTlxUrnoIdx)
      {
        strcpy(pcgNextTlxUrno,&pcgTlxUrnoList[igCurTlxUrnoIdx][0]);
      }
      else
      {
        ilDebugLevel = debug_level;
        debug_level = 0;
#ifdef DB_LOAD_URNO_LIST
        /* UFIS-1485 */
        DB_LoadUrnoList(1,pcgNextTlxUrno,pcgTableKey);
#else
        GetNextValues(pcgNextTlxUrno,1);
#endif
        debug_level = ilDebugLevel;
      }
    }
    else
    {
      cgTlxStatus = FDI_STAT_FLT_NOT_FOUND;
      dbg(TRACE,"%s No Flight Found In AFTTAB",pclFunc);
      ilRC = RC_FAIL;
    }
  }
  return ilRC; 
}


/****************************************************************************
 *
 *
 *
 *
 ****************************************************************************
 */

