#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/ntshdl.c 1.08 2011/11/25 15:00:00SGT dka Exp fei(2011/04/19 08:50:03SGT) $";
#endif /* _DEF_mks_version */
/********************************************************************************
 *          
 * Night Schedule Handler
 *                      
 * Author         : dka 
 * Date           : 20110419                                                    
 * Description    : UFIS-540 - Auto update basic data           
 *                                                                     
 * Update history :                                                   
 *  20110419 DKA 1.01   First release.
 *  20110427 DKA 1.02   To avoid BC storm and mass updates to shared memory tables
 *          during normal BDPSUIF updates, by default, defer formula
 *          updates when received from
 *          BDPSUIF (i.e. with NTSTAB.URNO). They will then be 
 *          performed when triggered by NTISCH at off peak hours.
 *  20110824 DKA 1.03   Formula selection & deletion based on status (C/F) 
 *              corrected.
 *  20110831 DKA 1.04   Configurable target ID for SQLHDL, defaults to Load
 *              sharing queue (506).
 *  20110914 DKA/BLE 1.05   Accept NTS command from BDPSUIF. Act as a filter to remove
 *          configured fieldnames/values, and then relay on to SQLHDL.
 *          This is a workaround because SQLHDL cannot filter off fields.
 *          RemoveItemFromList is copied from fdihdl.c.
 *  20111020 DKA 1.06	Urgent hard-coded change - create own LSTU in Local time
 *	    and add this to fields sent to SQLHDL. This is to fix an existing
 *          issue in BDPSUIF. A subsequent release is needed to make this 
 *          configurable (in NTSHDL) such that it can be used at other sites
 *          where LSTU could be in UTC.
 *  20111027 DKA 1.07  Handle IRT to auto-update BDPSUIF tables with formula when
 *          IRT is received. This is for single row updates of target tables. 
 *          Requires additional section in action.cfg to send the IRT to NTSHDL.
 *  20111125 DKA 1.08  For IRT, send blanked USEU and LSTU to sqlhdl so that the
 *          formula update is not known to the user seeing BDPSUIF.
 *
 *
 *
 ********************************************************************************/

/**********************************************************************************/
/*                                                                                */
/* be carefule with strftime or similar functions !!!                             */
/*                                                                                */
/**********************************************************************************/
/* This program is a MIKE main program */

#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <math.h>   /* for 'ceil' and 'floor' calls */
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h"

/******************************************************************************/
/* Macros */
/******************************************************************************/

#define     N_CMDS      8   /* no. of cmds in mapped list from cfg */
#define     RC_DATA_ERR 2222
#define     BASIC_DATA_FIELD_VALUE_WIDTH    256
#define     CEIL        2
#define     FLOOR       4
#define     ADDITION    8
#define     SUBTRACT    16
#define     MULTIPLY    32
#define     DIVIDE      64
#define	    UTC_TIME  5601
#define	    LOCAL_TIME  5602

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/

int  debug_level = DEBUG;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];
static char  cgCedaSend[10] = "\0";
static char  cgCedaSendPrio[10] = "\0";
static char  cgCedaSendCommand[10] = "\0";
static char  cgMqrInterfaceName[10] = "fcs";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */
static int   igModIdSqlhdl = 506;       /* target ID for SQLHDL */
static char  pcgUserName [64];		/* last sender's username if any */
static char  pcgCedaCmd [64];		/* last command */

typedef struct {
  char cmd_msg [16];
  char cmd_map [16]; 
} t_struct_cmd_map;

static t_struct_cmd_map CmdMap [N_CMDS];

static int   igFlagFormulaDefer = 1;

typedef struct {
  char pclTableName [16];
  char pclRemoveFields [128]; 
} t_struct_remove_fields;

/******************************************************************************/
/* Function prototypes                                                          */
/******************************************************************************/
static int  InitPgm();
static int  Reset(void); 
static void Terminate(void);
static void HandleSignal(int);
static void HandleErr(int);         /* General errors */
static void HandleQueErr(int);
static int  HandleData(void);       /* CEDA  event data     */
static void HandleQueues(void);     /* Waiting for Sts.-switch*/

static int  TimeToStr (char *pcpTime,time_t lpTime, int ipFlag); /* handy */
static int  SendToCeda (char *pcpBuffer,char *pcpMyTime,long plpMyReason,
          char *pcpUrno);
static void HandleNTSCmd (char *clUrnoNts);
static void HandleSQLCmd (int ipQueOut, int ipPriority, char *pcpTable, char *pcpFields, char *pcpValues,
                          char *pcpTwStart, char *pcpTwEnd, char *pcpSel, BC_HEAD *prpBchd ); 
static void HandleIRT (char *pclTable,char *pclFld,char *pclData,char *pclUrnoTana);

static int  ExecuteNTS_Utan_Non_Zero (char *clDateTime);
static int  ExecuteNTS_Utan_Zero (char *clDateTime);
static int  HandleFormulaRows (char *clDateTime, char *clFnam);
static int  GetBdUpdInfo (char *clDateTime, char *clUrnoNts);

static int  UpdBasicData (char *clUrno, char *clUtan,
                  char *clTana, char *clFlst, char *clFval);
                  
static int  DoBdFormula (char *clUrno,
                  char *clTana, char *clFlst, char *clFval,
                  char *clFnam, char *clStat, char *pcpTanaUrno);

static int  GetFieldType (char *clTana,char *clFina,
                  char *clType, char *clFieldLen);

/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/

extern int get_real_item (char *, char *, int);
extern int get_item_no (char *, char *, int);

/******************************************************************************/
/* Internal variables                                                         */
/******************************************************************************/

#define DATABLK_SIZE (1024)

static char pcgDataArea[DATABLK_SIZE];
static char pcgSqlBuf[DATABLK_SIZE];

int  igDbgSave = DEBUG;
int  igSendToModid = 9560;
int  igSendPrio = 3;

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/

MAIN
{
    int    ilRC = RC_SUCCESS;            /* Return code            */
    int    ilCnt = 0;
    
    INITIALIZE;            /* General initialization    */

    dbg(TRACE,"MAIN: version <%s>",mks_version);
    /* handles signal       */
    (void)SetSignals(HandleSignal);
    (void)UnsetSignals();

    /* Attach to the MIKE queues */
    do
    {
        ilRC = init_que();
        if(ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRC != RC_SUCCESS));
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    ilRC = TransferFile(cgConfigFile);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */
    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    /* uncomment if necessary */
    /* ilRC = TransferFile(cgConfigFile); */
    /* if(ilRC != RC_SUCCESS) */
    /* { */
    /*     dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile); */
    /* } */ /* end of if */
    ilRC = SendRemoteShutdown(mod_id);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */
    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */
    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        if(igInitOK == FALSE)
        {
            ilRC = InitPgm();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"MAIN: init failed!");
            } /* end of if */
        }/* end of if */
    } else {
        Terminate();
    }/* end of if */
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"==================== Entering main pgm loop  =================");
    for(;;)
    {
        dbg(TRACE,"==================== START/END =================");
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
            
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                /* CloseConnection(); */
                HandleQueues();
                break;    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                break;    

            case    SHUTDOWN    :
                /* process shutdown - maybe from uutil */
                Terminate();
                break;
                    
            case    RESET        :
                ilRC = Reset();
                break;
                    
            case    EVENT_DATA    :
                if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                {
                    ilRC = HandleData();
                    if(ilRC != RC_SUCCESS)
                    {
                        HandleErr(ilRC);
                    }/* end of if */
                }else{
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"MAIN: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */ 
        }
   
    } /* end for */
    
    exit(0);
    
} /* end of MAIN */

/******************************************************************************
 * ReadConfigEntry
 ******************************************************************************
 */

static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
     char *pcpCfgBuffer, char *pcpDefault)
{
    int ilRC = RC_SUCCESS;

    char clSection[256] = "\0";
    char clKeyword[256] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRC = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
               CFG_STRING,pcpCfgBuffer);
    if(ilRC != RC_SUCCESS)
    {
    dbg(TRACE,"ReadConfigEntry: Not found in %s: [%s] <%s>",cgConfigFile,clSection,clKeyword);
    dbg(TRACE,"ReadConfigEntry: use default-value: <%s>",pcpDefault);
    strcpy(pcpCfgBuffer,pcpDefault);
    } 
    else
    {
    dbg(TRACE,"ReadConfigEntry: Config Entry [%s],<%s>:<%s> found in %s",
        clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRC;
}

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int InitPgm()
{
 int    ilRC = RC_SUCCESS;            /* Return code */
 char     pclDbgLevel [iMIN_BUF_SIZE];
 char     pclDummy [iMIN_BUF_SIZE];
 char     pclDummy2 [iMIN_BUF_SIZE],pclTmpBuf [64];
 int ilCnt = 0 ;
 int ilItem = 0 ;
 int ilLoop = 0 ;
 int jj, kk;

  /* now reading from configfile or from database */
  SetSignals(HandleSignal);

  do
  {
    ilRC = init_db();
    if (ilRC != RC_SUCCESS)
    {
      check_ret(ilRC);
      dbg(TRACE,"InitPgm: init_db() failed! waiting 6 sec ...");
      sleep(6);
      ilCnt++;
    } /* end of if */
  } while((ilCnt < 10) && (ilRC != RC_SUCCESS));

  if(ilRC != RC_SUCCESS)
  {
    dbg(TRACE,"InitPgm: init_db() failed! waiting 60 sec ...");
    sleep(60);
    exit(2);
  }else{
    dbg(TRACE,"InitPgm: init_db() OK!");
  } /* end of if */

  (void) ReadConfigEntry("MAIN","DEBUG_LEVEL",pclDbgLevel,"TRACE");
  if (strcmp(pclDbgLevel,"DEBUG") == 0)
  {
    dbg(DEBUG,"InitPgm: DEBUG_LEVEL IS <DEBUG>");
    igDbgSave = DEBUG;
  } 
  else if (strcmp(pclDbgLevel,"OFF") == 0) 
  {
    dbg(DEBUG,"InitPgm: DEBUG_LEVEL IS <OFF>");
    igDbgSave = 0;
  } 
  else 
  {
    dbg(DEBUG,"InitPgm: DEBUG_LEVEL IS <TRACE>");
    igDbgSave = TRACE;
  }

  /*
    Build mapping of commands from external source(s) to
    our internal command names.
  */
  (void) ReadConfigEntry("MAIN","CMD_LIST", pclDummy, "NTS"); /* from external */
  (void) ReadConfigEntry("MAIN","CMD_MAP", pclDummy2, "NTS"); /* internal name */

  for (jj = 1; jj < (N_CMDS - 1); jj++)
  {
    if (((get_real_item (CmdMap [jj].cmd_msg, pclDummy, jj)) > 0)
      &&
      ((get_real_item (CmdMap [jj].cmd_map, pclDummy2, jj)) > 0))
    {
    dbg (TRACE, "InitPgm: External Cmd [%s] mapped to internal [%s]",
      CmdMap [jj].cmd_msg, CmdMap [jj].cmd_map);
    }
    else
    {
      CmdMap [jj].cmd_msg [0] = '\0';
      CmdMap [jj].cmd_map [0] = '\0';
      break;
    }
  }

  /*
    Are formulae immediately executed when triggered by BDPFUIS?
  */

  (void) ReadConfigEntry("OPTIONS","FORMULA_DEFER", pclDummy, "YES");
  if (!strcmp (pclDummy, "NO")) 
    igFlagFormulaDefer = 0;
  else
    igFlagFormulaDefer = 1;

  dbg (TRACE, "InitPgm: Defer Formula Execution : <%s>", pclDummy);

  /*
    Target module ID for SQLHDL. Defaults to load sharing queue.  
  */

  (void) ReadConfigEntry("OPTIONS","MODID_SQLHDL", pclDummy, "506");
  if (strlen (pclDummy) > 0)
    igModIdSqlhdl = atoi (pclDummy);
  dbg (TRACE, "InitPgm: MOD ID SQLHDL : <%d>", igModIdSqlhdl);
    

  if(ilRC == RC_SUCCESS)
  {
    /* read HomeAirPort from SGS.TAB */
    ilRC = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
    if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"InitPgm: EXTAB,SYS,HOMEAP not found in SGS.TAB");
      Terminate();
    }
    else
    {
      dbg(TRACE,"InitPgm: home airport <%s>",cgHopo);
    }
  }

  if(ilRC == RC_SUCCESS)
  { 
    ilRC = tool_search_exco_data("ALL","TABEND", cgTabEnd);
    if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"InitPgm: EXTAB,ALL,TABEND not found in SGS.TAB");
      Terminate();
    }
    else
    {
      dbg(TRACE,"InitPgm: table extension <%s>",cgTabEnd);
    }
  }

  igInitOK = TRUE;
  debug_level = igDbgSave;
  return(ilRC);

} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int    ilRC = RC_SUCCESS;                /* Return code */
    
    return ilRC;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate()
{ 
    dbg(TRACE,"Terminate: now leaving ...");
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int ipSig)
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    switch(ipSig)
    {
        case SIGTERM:
            dbg(TRACE,"HandleSignal: Received Signal <%d> (SIGTERM). Terminating now...",ipSig);
            break;
        case SIGALRM:
            dbg(TRACE,"HandleSignal: Received Signal<%d>(SIGALRM)",ipSig);
            return;
            break;
        default:
            dbg(TRACE,"HandleSignal: Received Signal<%d>",ipSig);
            return;
            break;
    } /* end of switch */
    exit(0);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"HandleQueErr: <%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"HandleQueErr: <%d> malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
        dbg(TRACE,"HandleQueErr: <%d> msgsnd failed",pipErr);
        break;
    case    QUE_E_GET    :    /* Error using msgrcv */
        dbg(TRACE,"HandleQueErr: <%d> msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"HandleQueErr: <%d> route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"HandleQueErr: <%d> route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    :
        dbg(TRACE,"HandleQueErr: <%d> unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"HandleQueErr: <%d>  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"HandleQueErr: <%d> queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"HandleQueErr: <%d> missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"HandleQueErr: <%d> queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"HandleQueErr: <%d> no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"HandleQueErr: <%d> too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        dbg(TRACE,"HandleQueErr: <%d> no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"HandleQueErr: <%d> invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"HandleQueErr: <%d> queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"HandleQueErr: <%d> requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"HandleQueErr: <%d> receive buffer to small ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"HandleQueErr: <%d> unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    int    ilBreakOut = FALSE;
    
    do
    {
        ilRC = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;    
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;    
    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                ilBreakOut = TRUE;
                break;    

            case    SHUTDOWN    :
                Terminate();
                break;
                        
            case    RESET        :
                ilRC = Reset();
                break;
                        
            case    EVENT_DATA    :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        }
        
    } while (ilBreakOut == FALSE);
    if(igInitOK == FALSE)
    {
            ilRC = InitPgm();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"HandleQueues: : init failed!");
            } /* end of if */
    }/* end of if */
} /* end of HandleQueues */
    

/******************************************************************************
 * HandleData
 ******************************************************************************
 */

static int HandleData()
{

  int ilRC = RC_SUCCESS; 
  int que_out;          /* Sender que id */
  int ilItemNo,jj;
  BC_HEAD *prlBchd = NULL;  /* Broadcast heade */
  CMDBLK  *prlCmdblk = NULL;    /* Command Block */
  char *pclSel = '\0';
  char *pclFld = '\0';
  char *pclData = '\0';
  char pclFields [4096], pclValues [4096];
  char *pclOldData = '\0';  
  char pclUrnoNts [32], pclUrnoTana [32];
  char pclTable [32];
  char pclDataInsert [64];
  
  /* dbg (TRACE, " --------------START/END------------------"); */
  DebugPrintItem(DEBUG,prgItem);
  DebugPrintEvent(DEBUG,prgEvent);

  que_out = prgEvent->originator;   /* Queue-id des Absenders */
  prlBchd = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  prlCmdblk = (CMDBLK  *) ((char *)prlBchd->data);

  pclSel = prlCmdblk->data;
  pclFld = pclSel + strlen(pclSel)+1;
  pclData = pclFld + strlen(pclFld)+1;
  strncpy (pclFields, pclFld, 4096);
  strncpy (pclValues, pclData, 4096);

  pclOldData= strtok(pclData,"\n");
  pclOldData= strtok(NULL,"\n");
  dbg(TRACE,"FROM <%d> TBL <%s>",que_out,prlCmdblk->obj_name);
  strcpy (pclTable,prlCmdblk->obj_name);
  dbg(TRACE,"Cmd <%s> Que (%d) WKS <%s> Usr <%s>",
    prlCmdblk->command, prgEvent->originator, prlBchd->recv_name, prlBchd->dest_name);
  dbg(TRACE,"Prio (%d) TWS <%s> TWE <%s>", prgItem->priority, prlCmdblk->tw_start, prlCmdblk->tw_end);
  dbg(TRACE,"Sel <%s> ", pclSel);
  dbg(TRACE,"Fld <%s> ", pclFld);
  dbg(TRACE,"Dat <%s> ", pclData);
  if (pclOldData != '\0')
    dbg(TRACE,"Old <%s> ", pclOldData);

  strcpy (pclUrnoNts, "");

  /* Try both the message header user as well as data payload USEC */
  ilItemNo = get_item_no (pclFld, "USEC", 5) + 1;
  if (ilItemNo > 0)   /* get_item_no returns -1 if not found */
  {
    (void) get_real_item (pcgUserName, pclData, ilItemNo); 
  }
  else if (strlen (prlBchd->dest_name) > 0)
  {
    strncpy (pcgUserName,prlBchd->dest_name,16);
    pcgUserName [16] = '\0';

  }
  else
  {
    strcpy (pcgUserName, mod_name); 
  }

  strcpy (pcgCedaCmd,prlCmdblk->command);

  if (!(strcmp (prlCmdblk->command, "IRT"))) 
  { 
    ilItemNo = get_item_no (pclFld, "URNO", 5) + 1;
    if (ilItemNo > 0)   /* get_item_no returns -1 if not found */
    {
      (void) get_real_item (pclUrnoTana, pclData, ilItemNo);
      HandleIRT (pclTable,pclFld,pclData,pclUrnoTana);
    }
    else
    {
      dbg (TRACE,"HandleData: IRT without URNO - Doing nothing"); 
    } 
    return ilRC;
  }


  if ((strcmp (pclTable,"") != 0) && (strcmp (pclTable,"NTSTAB") != 0))
  { 
      HandleSQLCmd (que_out, prgItem->priority, pclTable, pclFields, pclValues,
                    prlCmdblk->tw_start,  prlCmdblk->tw_end, pclSel,prlBchd);
    /*
         We do not want to also execute a 2nd time just in case we also
         received NTS command together with the non-NTSTAB tablename so
     return now.
    */
      return ilRC;
  }

  if (!(strcmp (prlCmdblk->command, "NTS"))) 
  {
    ilItemNo = get_item_no (pclFld, "URNO", 5) + 1;
    if (ilItemNo > 0)   /* get_item_no returns -1 if not found */
    {
       (void) get_real_item (pclUrnoNts, pclData, ilItemNo);
    }
    HandleNTSCmd (pclUrnoNts);
    
    return ilRC;
  }

  return ilRC;
    
} /* end of HandleData */

/************************************************************************
 *      
 *  HandleNTSCmd 
 *
 *  The handling of field update (UTAN non zero) and formula
 *  execution is sufficiently different to justify two
 *  functions.
 *
 *  v.1.05 - add table name to arg's
 *
 ************************************************************************
 */

void HandleNTSCmd (char *clUrnoNts)
{ 
  char clNow [32] = "\0";
  int jj,kk;
  char pclTmpBuf [64];
 
  (void) TimeToStr (clNow, time(NULL), UTC_TIME);

  if (clUrnoNts [0] == '\0')
  {
    dbg (TRACE, "HandleNTSCmd: NTSTAB.URNO not received - Execute from NTSTAB");
    ExecuteNTS_Utan_Non_Zero (clNow);
    ExecuteNTS_Utan_Zero (clNow);
  }
  else
  { 
    dbg (TRACE, "HandleNTSCmd: NTSTAB.URNO received <%s>", clUrnoNts);
    GetBdUpdInfo (clNow, clUrnoNts);
  }

  return;
}

/********************************************************************************/

/******************************************************************************
 * ExecuteNTS_Utan_Non_Zero 
 *
 * Select records from NTSTAB where VAFR/VATO is eligible, then call the BD updater
 * function. This function is for records where UTAN is non zero, i.e. for
 * plain update of fields with values from FVAL. No formula execution.
 * Records are deleted after each update of the referred to BD table.
 *
 * Usually triggered via NTISCH. (without URNO)
 *
 ******************************************************************************
 */

static int ExecuteNTS_Utan_Non_Zero (char *clDateTime)
{ 
  int ilRC = RC_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf [2048] = "\0";
  char clDataArea [4096] = "\0";
  char clFieldList [2048] = "\0";
  char clWhereClause [2048] = "\0";
  char clWhereClauseCedaCmd [512] = "\0";
  int ilNoOfRec = 0;
  int ilGetRc = DB_SUCCESS;

  char pclUrno [64] = "\0";
  char pclUtan [64] = "\0";
  char pclTana [128] = "\0"; 
  char pclFlst [512] = "\0";
  char pclFval [512] = "\0"; 
  char pclFnam [128] = "\0";
  char pclVafr [32] = "\0"; 
  char pclVato [32] = "\0";
  char pclStat [32] = "\0"; 

  char clTwStart [64];
  char clTwEnd [64];
  char clCedaCmd [32];

  char clSqlBuf2 [2048] = "\0";
  char clDataArea2 [4096] = "\0";
  char clFieldList2 [2048] = "\0";
  char clWhereClause2 [2048] = "\0";
  int  iflag_formula = 0;
  char pclUserName [64];

  /* if (strlen (pcgUserName) == 0)
    strcpy (pclUserName, "NTS_AUTO");
  else */
    strcpy (pclUserName, pcgUserName);
  
  dbg (TRACE, "ExecuteNTS: Execute on NTSTAB for Now = <%s>", clDateTime);

  sprintf (clTwEnd,"%s,%s,%s",cgHopo,cgTabEnd,mod_name);

  strcpy (clFieldList,
    "URNO,UTAN,TANA,FLST,FVAL,FNAM,VAFR,VATO,STAT");
  
  /*
    Rather non-standard, but VATO could be null rather than blank.
  */

  sprintf (clWhereClause,
    " WHERE UTAN <> '0' AND VAFR <= '%s' AND (VATO > '%s' OR TRIM(VATO) IS NULL) ",
     clDateTime, clDateTime);
 
  sprintf (clSqlBuf,
    "SELECT %s FROM NTSTAB %s ORDER BY UTAN,FNAM,VAFR DESC",
    clFieldList, clWhereClause);
  dbg (TRACE, "ExecuteNTS_UtanNonZero: <%s>", clSqlBuf);

  ilNoOfRec = 0;
  slFkt = START;
  slCursor = 0;
  ilGetRc = DB_SUCCESS;
  while (ilGetRc == DB_SUCCESS)
  {
    ilGetRc = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea);
    slFkt = NEXT;
    if (ilGetRc == DB_SUCCESS)
    {
      strcpy (clFieldList,
       "URNO,UTAN,TANA,FLST,FVAL,FNAM,VAFR,VATO,STAT");
      ilNoOfRec++;
      BuildItemBuffer (clDataArea, clFieldList, 9, ",");

      (void) get_real_item (pclUrno, clDataArea, 1);
      (void) get_real_item (pclUtan, clDataArea, 2);
      (void) get_real_item (pclTana, clDataArea, 3);
      (void) get_real_item (pclFlst, clDataArea, 4);
      (void) get_real_item (pclFval, clDataArea, 5);
      (void) get_real_item (pclFnam, clDataArea, 6);
      (void) get_real_item (pclVafr, clDataArea, 7);
      (void) get_real_item (pclVato, clDataArea, 8);
      (void) get_real_item (pclStat, clDataArea, 9);
      
      dbg (DEBUG, 
        "ExecuteNTS: Fields <%s>", clFieldList); 
      dbg (DEBUG, 
        "ExecuteNTS: Values <%s>", clDataArea);

      if (pclUtan [0] == '\0')
      {
        dbg (TRACE, "ExecuteNTS_Utan_Non_Zero: Warning: Empty UTAN, skip this record");
        slFkt = NEXT;
        continue;
      } 

      if (!strcmp (pclUtan, "0"))   /* An 'execute formula in BD table' record */
      {
        /* Should not get here because of WHERE clause */
        slFkt = NEXT;
        continue;
      }
      else              /* An 'update fields in BD table' record */
      {
        ilRC = UpdBasicData (pclUrno, pclUtan, pclTana, pclFlst, pclFval);
    strcpy (clCedaCmd, "DRT");
        /* clFieldList and clDataArea were filled in during the sql_if earlier */ 
      }
       
      /*
    Send NTSTAB update/delete via load sharing queue for SQLHDL.
    SQLHDL will perform logging as well.  
      */

      sprintf (clWhereClauseCedaCmd, " WHERE URNO = '%s' ", pclUrno);
      sprintf (clTwStart, "%s,%s", "nts", pclUrno);
      dbg(TRACE, "ExecuteNTS: Send %s for NTSTAB URNO <%s> to %d",
        clCedaCmd,pclUrno,igModIdSqlhdl);

      ilRC = SendCedaEvent (igModIdSqlhdl,  /* Route ID */
        9802,               /* Origin ID */
        pclUserName,        /* Dest_name */
        "TOOL",             /* Recv_name */
        clTwStart,          /* CMDBLK TwStart */
        clTwEnd,            /* CMDBLK TwEnd */
        clCedaCmd,          /* CMDBLK command */
        "NTSTAB",           /* CMDBLK Obj name */
        clWhereClauseCedaCmd,       /* Selection block */
        clFieldList,            /* Field block */
        clDataArea,         /* Data block */
        "",             /* Error description */
        4,              /* priority */
        NETOUT_NO_ACK);         /* return code */

       if (ilRC != RC_SUCCESS)
       {
         dbg (TRACE, "ExecuteNTS: SendCedaEvent error <%ld>", ilRC);
       }
    
    } /* end if */
    else
    {
      if (ilGetRc == RC_FAIL)
      {
        dbg (TRACE, "ExecuteNTS  : ORACLE ERROR !!");
        ilRC = RC_FAIL;
      }                     /* end if */
/*      else
      {
        dbg (TRACE, "ExecuteNTS : no record(s) found");
      } */
    }                       /* end else */
    slFkt = NEXT;
  }                         /* while */

  close_my_cursor (&slCursor); 
  dbg (TRACE, "ExecuteNTS_UtanNonZero: worked on <%d> records", ilNoOfRec);
  return ilRC;
}

/******************************************************************************
 * ExecuteNTS_Utan_Zero 
 *
 * Select formula records from NTSTAB where VAFR is eligible. VATO is still 
 * retained in WHERE clause in case it is ever used in the future.
 *
 * Usually triggered via NTISCH. (without URNO). That poses the problem that
 * we have to scan through the NTSTAB to find first, all distinct Formula
 * names.
 * Flwg clarification from IGU on 20110426:
 * For each formula, if there is only one current 'C' record, execute that.
 * If there are more than one record, take the one which is most recent as far as
 * VAFR is concerned, execute it, set it to 'C', and delete all the older ones.
 * There must always be at least one record for each formula. If that one record's
 * VAFR is in the past, its stat should be 'C'. If the one record is in the future 
 * and its stat is 'C', that is a mistake in the table entry.
 *
 * Above is slightly incorrect (20110824) - if there is only one 'C' current
 * record, that should not be executed. It is left for user reference. Should
 * execute the current 'F' record, and then delete pre-existing 'C, then set
 * the executed 'F' to 'C'.
 *
 ******************************************************************************
 */

static int ExecuteNTS_Utan_Zero (char *clDateTime)
{ 
  int ilRC = RC_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf [2048] = "\0";
  char clDataArea [4096] = "\0";
  char clFieldList [2048] = "\0";
  char clWhereClause [2048] = "\0";
  char clWhereClauseCedaCmd [512] = "\0";
  int ilNoOfRec = 0;
  int ilGetRc = DB_SUCCESS;

  char pclUrno [64] = "\0";
  char pclUtan [64] = "\0";
  char pclTana [128] = "\0"; 
  char pclFlst [512] = "\0";
  char pclFval [512] = "\0"; 
  char pclFnam [128] = "\0";
  char pclVafr [32] = "\0"; 
  char pclVato [32] = "\0";
  char pclStat [32] = "\0"; 

  char clTwStart [64];
  char clTwEnd [64];
  char clCedaCmd [32];

  char clSqlBuf2 [2048] = "\0";
  char clDataArea2 [4096] = "\0";
  char clFieldList2 [2048] = "\0";
  char clWhereClause2 [2048] = "\0";

  dbg (TRACE, "ExecuteNTS_UtanZero: Execute on NTSTAB for Now = <%s>", clDateTime);

  sprintf (clTwEnd,"%s,%s,%s",cgHopo,cgTabEnd,mod_name);

  /* strcpy (clFieldList,
    "URNO,UTAN,TANA,FLST,FVAL,FNAM,VAFR,VATO,STAT"); */

  strcpy (clFieldList, "FNAM");

  /*
    BDPSUIF could set VATO to null rather than blank. So cannot test for 
    blank only.
  */
  sprintf (clWhereClause,
    " WHERE UTAN = '0' AND VAFR <= '%s' AND (VATO > '%s' OR TRIM(VATO) IS NULL) ",
    clDateTime, clDateTime);
 
  sprintf (clSqlBuf,
    "SELECT DISTINCT %s FROM NTSTAB %s",
    clFieldList, clWhereClause);
  dbg (TRACE, "ExecuteNTS_Utan_Zero: <%s>", clSqlBuf);

  ilNoOfRec = 0;
  slFkt = START;
  slCursor = 0;
  ilGetRc = DB_SUCCESS;
  while (ilGetRc == DB_SUCCESS)
  {
    ilGetRc = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea);
    slFkt = NEXT;
    if (ilGetRc == DB_SUCCESS)
    { 
      ilNoOfRec++; 
      strcpy (pclFnam, clDataArea);
      dbg (TRACE, "ExecuteNTS_Utan_Zero: handle FNAM <%s>", pclFnam);
      ilRC = HandleFormulaRows (clDateTime, pclFnam);
   
    } /* end if */
    else
    {
      if (ilGetRc == RC_FAIL)
      {
        dbg (TRACE, "ExecuteNTS  : ORACLE ERROR !!");
        ilRC = RC_FAIL;
      }                     /* end if */
/*      else
      {
        dbg (TRACE, "ExecuteNTS : no record(s) found");
      } */
    }                       /* end else */
    slFkt = NEXT;
  }                         /* while */


  dbg (TRACE, "ExecuteNTS_UtanZero: worked on <%d> records", ilNoOfRec);
  close_my_cursor (&slCursor);
  return ilRC;
}

/******************************************************************************
 *
 *  HandleFormulaRows. 
 *  Given a forumla and the current date, select the eligible formula to
 *  execute, execute it, update its stat to 'C' (current) if not already
 *  so, and delete all those who are outdated with the same formula name.
 *
 *  Important: there must always be at least one current formula remaining
 *  afterward.
 *
 *  V.1.03 - from UFIS-540 20110824:
 * 1. Group the records by Formula Name (FNAM)
 * 2. Find out are there any 'F' records with VAFR and VATO fulfilling the current time.
 * 3. If none, do not need to do anything
 * 4. If record is found,
 * 4a. execute the formula setting based on the 'F' records
 * 4b. delete the record with 'C' (under same group of formula)
 * 4c. update the 'F' record found with 'C'
 * Once a record is set to be 'C', nothing should be calculated by NTSHDL based on this record.
 * This is only used for reference by the Client on the current formula used.
 * 
 * With this, it can never have a 'C' record with VAFR to be in the future.
 * There should not be more than one 'F' for a formula name, unless e.g. the system
 * was shutdown for a day.  
 * 
 ******************************************************************************
 */

static int  HandleFormulaRows (char *clDateTime, char *clFnam)
{ 
  int ilRC = RC_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf [2048] = "\0";
  char clDataArea [4096] = "\0";
  char clFieldList [2048] = "\0";
  char clWhereClause [2048] = "\0";
  char clWhereClauseCedaCmd [512] = "\0";
  int ilNoOfRec = 0;
  int ilGetRc = DB_SUCCESS;

  char pclUrno [64] = "\0";
  char pclUrnoExecutedFormula [64] = "\0";
  char pclUtan [64] = "\0";
  char pclTana [128] = "\0"; 
  char pclFlst [512] = "\0";
  char pclFval [512] = "\0"; 
  char pclVafr [32] = "\0"; 
  char pclVato [32] = "\0";
  char pclStat [32] = "\0"; 

  char clTwStart [64];
  char clTwEnd [64];
  char clCedaCmd [32];

  char clSqlBuf2 [2048] = "\0";
  char clDataArea2 [4096] = "\0";
  char clFieldList2 [2048] = "\0";
  char clWhereClause2 [2048] = "\0";
 
  char pclUserName [64];

  /* if (strlen (pcgUserName) == 0)
    strcpy (pclUserName, "NTS_AUTO");
  else */
    strcpy (pclUserName, pcgUserName); /* from IRT sender */
  
  dbg (TRACE, "HandleFormulaRows: Now <%s> Formula <%s>",
    clDateTime, clFnam);

  sprintf (clTwEnd,"%s,%s,%s",cgHopo,cgTabEnd,mod_name);

  strcpy (clFieldList,
    "URNO,UTAN,TANA,FLST,FVAL,VAFR,VATO,STAT"); 

  /*
    BDPSUIF could set VATO to null rather than blank. So cannot test for 
    blank only.
  */

  /*    First check if there is at least one 'F' record. Get the one with the 
   *    most recent VAFR, and save it's URNO for later execution and setting
   *    STAT to 'C'.
   *    
   */

  /*
    order by clause is VAFR descending, so we get what is considered the most 
    eligible one to execute. The others which are older, might also be valid,
    but we always treat only one as the current one, and delete the rest, if
    any.  
  */

  sprintf (clWhereClause,
    " WHERE FNAM = '%s' AND VAFR <= '%s' AND STAT = 'F' AND (VATO > '%s' OR TRIM(VATO) IS NULL) ",
    clFnam, clDateTime, clDateTime);

  sprintf (clSqlBuf,
    "SELECT URNO FROM NTSTAB %s ORDER BY VAFR DESC", clWhereClause);
  dbg (TRACE, "HandleFormulaRows: <%s>", clSqlBuf);

  ilNoOfRec = 0;
  slFkt = START;
  slCursor = 0;
  ilGetRc = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea); 
  close_my_cursor (&slCursor);
  if (ilGetRc == DB_SUCCESS)
  {
    strcpy (pclUrnoExecutedFormula, clDataArea);
    dbg(TRACE,"HandleFormulaRows: Found NTSTAB.STAT=F record URNO <%s>", pclUrnoExecutedFormula); 
  }
  else
  {
    dbg(TRACE,"HandleFormulaRows: No NTSTAB.STAT=F eligible record for <%s>", clFnam);
    return ilRC;
  }

  sprintf (clWhereClause,
    " WHERE FNAM = '%s' AND VAFR <= '%s' AND (VATO > '%s' OR TRIM(VATO) IS NULL) ",
    clFnam, clDateTime, clDateTime);
 

  sprintf (clSqlBuf,
    "SELECT %s FROM NTSTAB %s ORDER BY VAFR DESC",
    clFieldList, clWhereClause);
  dbg (TRACE, "HandleFormulaRows: <%s>", clSqlBuf);

  ilNoOfRec = 0;
  slFkt = START;
  slCursor = 0;
  ilGetRc = DB_SUCCESS;
  while (ilGetRc == DB_SUCCESS)
  {
    ilGetRc = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea);
    slFkt = NEXT;
    if (ilGetRc == DB_SUCCESS)
    {
      ilNoOfRec++;
      BuildItemBuffer (clDataArea, clFieldList, 8, ",");

      (void) get_real_item (pclUrno, clDataArea, 1);
      (void) get_real_item (pclUtan, clDataArea, 2);
      (void) get_real_item (pclTana, clDataArea, 3);
      (void) get_real_item (pclFlst, clDataArea, 4);
      (void) get_real_item (pclFval, clDataArea, 5);
      (void) get_real_item (pclVafr, clDataArea, 6);
      (void) get_real_item (pclVato, clDataArea, 7);
      (void) get_real_item (pclStat, clDataArea, 8);
      
      dbg (DEBUG, 
        "HandleFormulaRows: Fields <%s>", clFieldList); 
      dbg (DEBUG, 
        "HandleFormulaRows: Values <%s>", clDataArea);

      if (pclUtan [0] == '\0')
      {
        dbg (TRACE, "HandleForumaRows: Warning: Empty UTAN, skip this record");
        slFkt = NEXT;
        continue;
      } 

      if (!strcmp (pclUtan, "0"))   /* An 'execute formula in BD table' record */
      { 
        /*
        Execute the record only if it was the eligible one found earlier.
        This is the only record that we execute.  
        */
        if (strcmp (pclUrno, pclUrnoExecutedFormula) == 0)
        {
          ilRC = DoBdFormula (pclUrno, pclTana, pclFlst,
            pclFval, clFnam, pclStat, ""); 

          strcpy (clFieldList2, "STAT");
          strcpy (clDataArea2, "C");
      strcpy (clCedaCmd, "URT");
        }
        else 
        {
          /* Delete the rest, do not execute */
      strcpy (clCedaCmd, "DRT"); 
          strcpy (clFieldList2, clFieldList);
          strcpy (clDataArea2, clDataArea);
        }
      }
      else              /* An 'update fields in BD table' record */
      {                                 /* should never get to this block */
        dbg (TRACE,"HandleFormulaRows: Error - UTAN non zero when we expected zero");
        slFkt = NEXT;
        continue;
      }
       
      /*
    Send NTSTAB update/delete via load sharing queue for SQLHDL.
    SQLHDL will perform logging as well.  
      */

      sprintf (clWhereClauseCedaCmd, " WHERE URNO = '%s' ", pclUrno);
      sprintf (clTwStart, "%s,%s", "nts", pclUrno);
      dbg(TRACE, "HandleFormulaRows: Send %s for NTSTAB URNO <%s> to %d",
        clCedaCmd,pclUrno,igModIdSqlhdl);

      ilRC = SendCedaEvent (igModIdSqlhdl,  /* Route ID */
        9802,               /* Origin ID */
        pclUserName,        /* Dest_name */
        "TOOL",             /* Recv_name */
        clTwStart,          /* CMDBLK TwStart */
        clTwEnd,            /* CMDBLK TwEnd */
        clCedaCmd,          /* CMDBLK command */
        "NTSTAB",           /* CMDBLK Obj name */
        clWhereClauseCedaCmd,       /* Selection block */
        clFieldList2,           /* Field block */
        clDataArea2,            /* Data block */
        "",             /* Error description */
        4,              /* priority */
        NETOUT_NO_ACK);         /* return code */

       if (ilRC != RC_SUCCESS)
       {
         dbg (TRACE, "ExecuteNTS: SendCedaEvent error <%ld>", ilRC);
       }
    
    } /* end if */
    else
    {
      if (ilGetRc == RC_FAIL)
      {
        dbg (TRACE, "ExecuteNTS  : ORACLE ERROR !!");
        ilRC = RC_FAIL;
      }                     /* end if */
/*      else
      {
        dbg (TRACE, "ExecuteNTS : no record(s) found");
      } */
    }                       /* end else */
    slFkt = NEXT;
  }                         /* while */

  close_my_cursor (&slCursor); 
  return ilRC;
}

/******************************************************************************
 * Handy 
 ******************************************************************************
 */

static int TimeToStr(char *pcpTime,time_t lpTime, int ipFlag)
{
    struct tm *_tm;
    char   _tmpc[6];

    if (ipFlag == UTC_TIME)
      _tm = (struct tm *)gmtime(&lpTime);
    else if (ipFlag == LOCAL_TIME)
      _tm = (struct tm *)localtime(&lpTime);
    else
    {
      dbg(TRACE,"TimeToStr: ERROR: called with unknown flag - defaulting to UTC");
      _tm = (struct tm *)gmtime(&lpTime);
    }
    
    /*      strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",_tm); */

    sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
            _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
            _tm->tm_min,_tm->tm_sec);

    return RC_SUCCESS;

}     /* end of TimeToStr */

/******************************************************************************
 *
 * GetBdUpdInfo 
 * Called when NTS command is received with a URNO. We expect at most one
 * record from NTSTAB.
 *
 * v.1.02 - defer formula execution is configured as such.
 * 
 ******************************************************************************
 */

int GetBdUpdInfo (char *clDateTime, char *clUrnoNts)
{
  int ilRC = RC_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf [2048] = "\0";
  char clDataArea [2048] = "\0";
  char clFieldList [2048] = "\0";
  char clWhereClause [2048] = "\0";
  int ilNoOfRec = 0;
  int ilGetRc = DB_SUCCESS;

  char pclUrno [64] = "\0";
  char pclUtan [64] = "\0";
  char pclTana [128] = "\0"; 
  char pclFlst [512] = "\0";
  char pclFval [512] = "\0"; 
  char pclFnam [128] = "\0";
  char pclVafr [32] = "\0"; 
  char pclVato [32] = "\0";
  char pclStat [32] = "\0"; 

  char clTwStart [64];
  char clTwEnd [64];

  dbg (TRACE, "GetBdUpdInfo: NOW <%s>  NST.URNO <%s>",
    clDateTime, clUrnoNts);

  sprintf (clTwEnd,"%s,%s,%s",cgHopo,cgTabEnd,mod_name);

  strcpy (clFieldList, "URNO,UTAN,TANA,FLST,FVAL,FNAM,VAFR,VATO,STAT"); 
  sprintf (clWhereClause, " WHERE URNO = '%s' ", clUrnoNts);
  sprintf (clSqlBuf,
    "SELECT %s FROM NTSTAB %s", clFieldList, clWhereClause);
  dbg (TRACE, "GetBdUpdInfo: <%s>", clSqlBuf);

  ilNoOfRec = 0;
  slFkt = START;
  slCursor = 0;
  ilGetRc = DB_SUCCESS;
  
  ilGetRc = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea);
  if (ilGetRc == DB_SUCCESS)
  { 
      BuildItemBuffer (clDataArea, clFieldList, 9, ",");

      (void) get_real_item (pclUrno, clDataArea, 1);
      (void) get_real_item (pclUtan, clDataArea, 2);
      (void) get_real_item (pclTana, clDataArea, 3);
      (void) get_real_item (pclFlst, clDataArea, 4);
      (void) get_real_item (pclFval, clDataArea, 5);
      (void) get_real_item (pclFnam, clDataArea, 6);
      (void) get_real_item (pclVafr, clDataArea, 7);
      (void) get_real_item (pclVato, clDataArea, 8);
      (void) get_real_item (pclStat, clDataArea, 9);

      dbg (TRACE, 
        "GetBdUpdInfo: Fields <%s>", clFieldList);
      dbg (TRACE, 
        "GetBdUpdInfo: Values <%s>", clDataArea);

      sprintf (clTwStart, "%s,%s", "nts", pclUrno);

      if (pclUtan [0] == '\0')
      {
        dbg (TRACE, "GetBdUpdInfo: empty UTAN, doing nothing");
        close_my_cursor (&slCursor); 
        return ilRC;
      } 

      if (pclVato [0] == '\0')  /* get_real_item nullifies blanks */
        strcpy (pclVato, "99999999999999");
      
      /* Assume pclVafr never blank */
      if ((strcmp (clDateTime, pclVafr) < 0)
        || (strcmp (clDateTime, pclVato) > 0))
      {
        dbg (TRACE,
          "GetBdUpdInfo: Either VAFR <%s> in the future or VATO <%s> in the past, doing nothing",
            pclVafr, pclVato); 
        close_my_cursor (&slCursor);
        return ilRC;
      }

      if (!strcmp (pclUtan, "0"))   /* An 'execute formula in BD table' record */
      {
        /* Perform immediately only if configured to do so */
        if (igFlagFormulaDefer == 1)
        {
          dbg (TRACE, "GetBdUpdInfo: Defer formula execution to off peak / manual");
        }
        else
          ilRC = HandleFormulaRows (clDateTime, pclFnam); 

      }
      else              /* An 'update fields in BD table' record */
      {
        ilRC = UpdBasicData (pclUrno, pclUtan, pclTana, pclFlst, pclFval);

        /*
        Send delete via load sharing queue for SQLHDL. SQLHDL will perform
        logging as well.  
        */

        dbg(TRACE, "GetBdUpdInfo: Send <DRT> for NTSTAB.URNO <%s> to <%d>",
          pclUrno,igModIdSqlhdl);

        ilRC = SendCedaEvent (igModIdSqlhdl,    /* Route ID */
          9802,             /* Origin ID */
          "ntshdl",         /* Dest_name */
          "TOOL",           /* Recv_name */
          clTwStart,            /* CMDBLK TwStart */
          clTwEnd,          /* CMDBLK TwEnd */
          "DRT",            /* CMDBLK command */
          "NTSTAB",         /* CMDBLK Obj name */
          clWhereClause,        /* Selection block */
          clFieldList,          /* Field block */
          clDataArea,           /* Data block */
          "",               /* Error description */
          4,                /* priority */
          NETOUT_NO_ACK);       /* return code */

        if (ilRC != RC_SUCCESS)
        {
          dbg (TRACE, "GetBdUpdInfo: SendCedaEvent error <%ld>", ilRC);
        }

      } /* else */
  } /* end if */
  else
  {
    if (ilGetRc == RC_FAIL)
    {
      dbg (TRACE, "GetBdUpdInfo  : ORACLE ERROR !!");
      ilRC = RC_FAIL;
    }                     /* end if */
    else
    {
      dbg (TRACE, "GetBdUpdInfo: No NTSTAB record found");
    }
  }                       /* end else */
 
  close_my_cursor (&slCursor); 
  return ilRC; 

}

/******************************************************************************
 *
 *  UpdBasicData
 *
 *  Update the basic data table identified by NTSTAB.URNO and where
 *  NTSTAB.UTAN is not zero
 *  The FVAL values could include blanks, but not nulls.
 *  v.1.06 - special handling for LSTU 
 * 
 ******************************************************************************
 */

int UpdBasicData (char *clUrno, char *clUtan,
                  char *clTana, char *clFlst, char *clFval)
{ 
  int ilRC = RC_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf [2048] = "\0";
  char clDataArea [2048] = "\0";
  char clFieldList [2048] = "\0";
  char clWhereClause [2048] = "\0";
  int ilNoOfRec = 0;
  int ilGetRc = DB_SUCCESS;

  char pclTableName [32];

  char pclFlstCopy [2048];
  char pclFvalCopy [2048];
  char pclFieldName [32];
  char pclFieldValue [256]; 
  char pclFieldNameList [2048] = "\0";
  char pclFieldValueList [2048] = "\0";
  char pclLstu [64];

  char clTwStart [64];
  char clTwEnd [64];

  int  jj, kk, mm, nn, res_fld, res_val, fcount;

  char pclUserName [64];

  /* if (strlen (pcgUserName) == 0)
    strcpy (pclUserName, "NTS_AUTO");
  else */
    strcpy (pclUserName, pcgUserName); /* from IRT sender */
  
  /* v.1.06 - hard code it to LOCAL time for BDPSUIF */
  (void) TimeToStr (pclLstu, time(NULL), LOCAL_TIME);

  sprintf (pclTableName, "%s%s", clTana, cgTabEnd);
  sprintf (clWhereClause, " WHERE URNO = '%s' ", clUtan);
  sprintf (clTwEnd,"%s,%s,%s",cgHopo,cgTabEnd,mod_name);

  strcpy (pclFlstCopy, clFlst);
  strcpy (pclFvalCopy, clFval);
  kk = strlen (pclFlstCopy);
  for (jj = 0; jj < kk; jj++)
  {
    if (pclFlstCopy [jj] == ';')
      pclFlstCopy [jj] = ',';
  } 
  kk = strlen (pclFvalCopy);
  for (jj = 0; jj < kk; jj++)
  {
    if (pclFvalCopy [jj] == ';')
      pclFvalCopy [jj] = ',';
  }

  fcount = field_count (pclFlstCopy); 
  mm = field_count (pclFvalCopy); 

  if (fcount != mm)
  {
    dbg (TRACE,
      "UpdBasicData: field count FLST <%s> <%d> FVAL <%s><%d> different; doing nothing",
      pclFlstCopy, fcount, pclFvalCopy,  mm);
    return RC_FAIL;
  }
  
  for (mm = 0; mm < fcount; mm++)
  { 
    res_fld = get_real_item (pclFieldName, pclFlstCopy, (mm+1));
    res_val = get_real_item (pclFieldValue, pclFvalCopy, (mm+1));

    if (pclFieldName [0] == '\0')
    {
      dbg (TRACE,"UpdBasicData: empty field name in FLST; doing nothing");
      return RC_FAIL;
    }
    strcat (pclFieldNameList, pclFieldName);
    strcat (pclFieldNameList, ","); 

    if (pclFieldValue [0] == '\0')  /* blanked value possible */
      strcpy (pclFieldValue, " ");
    strcat (pclFieldValueList, pclFieldValue);
    strcat (pclFieldValueList, ",");
  }

  if (pclFieldNameList [0] == '\0')
  {
    dbg(TRACE,
      "UpdBasicData: Failed to get FLST items for NSTTAB.URNO <%s>, Doing Nothing",
      clUrno); 
    return RC_FAIL;
  }

  /*
	v.1.06 - append local time LSTU  -- assume that BDPSUIF did not save LSTU
	to NTSTAB so that this is not a duplicate
  */
  strcat (pclFieldNameList, "LSTU");
  strcat (pclFieldNameList, ","); 
  strcat (pclFieldValueList, pclLstu);
  strcat (pclFieldValueList, ",");


  /* terminate at the trailing comma */
  jj = strlen (pclFieldNameList);
  pclFieldNameList [jj - 1] = '\0'; 
  jj = strlen (pclFieldValueList);
  pclFieldValueList [jj - 1] = '\0';


  /*
    Update via SQLHDL load sharing queue.
  */

  sprintf (clTwStart, "%s,%s", "nts", clUtan);
  dbg(TRACE, "UpdBasicData: Send <URT> for <%s> URNO <%s> to <%d>",
    pclTableName, clWhereClause,igModIdSqlhdl);
  dbg(TRACE, "Fields: <%s>", pclFieldNameList);
  dbg(TRACE, "Values: <%s>", pclFieldValueList);

  ilRC = SendCedaEvent (igModIdSqlhdl,  /* Route ID */
        9802,               /* Origin ID */
        pclUserName,        /* Dest_name */
        "TOOL",             /* Recv_name */
        clTwStart,          /* CMDBLK TwStart */
        clTwEnd,            /* CMDBLK TwEnd */
        "URT",              /* CMDBLK command */
        pclTableName,           /* CMDBLK Obj name */
        clWhereClause,          /* Selection block */
        pclFieldNameList,       /* Field block */
        pclFieldValueList,      /* Data block */
        "",             /* Error description */
        4,              /* priority */
        NETOUT_NO_ACK);         /* return code */

 if (ilRC != RC_SUCCESS)
 {
   dbg (TRACE, "UpdBasicData: SendCedaEvent error <%ld>", ilRC);
 } 

}

/******************************************************************************
 *
 *  DoBdFormula 
 *
 *  Perform the formula on the basic data table for the row identified by
 *  NTSTAB.URNO and where NTSTAB.UTAN is zero.
 *
 *  First release allow only a simple formula format:
 *    [rounding method]({<field_name>}<operand><constant>)
 *    e.g. ceil({MTOW}/1000)
 *
 *  A possible simpler alternative is to store an sql update with math 
 *  operation into FVAL.
 *  Or, store it in pseudo RPN, e.g. the above would be 
 *    {MTOW},1000,/,ceil
 *
 *    ver.1.07 - accept one URNO only for the TANA for applying formula to
 *    that one row in TANA only.
 *    ver.1.08 - IRT - send blanked LSTU and USEU
 *
 * 
 ******************************************************************************
 */

int DoBdFormula (char *clUrno,
                  char *clTana, char *clFlst, char *clFval,
                  char *clFnam, char *clStat, char *pcpTanaUrno)
{ 
  int ilRC = RC_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf [2048] = "\0";
  char clDataArea [2048] = "\0";
  char clFieldList [2048] = "\0";
  char clWhereClause [2048] = "\0";
  int ilNoOfRec = 0;
  int ilGetRc = DB_SUCCESS;

  char pclTableName [32];

  char pclFlstCopy [2048];
  char pclFvalCopy [2048];
  char pclFieldName [32];
  char pclFieldValue [256]; 
  char pclCurrValue [256];
  char pclTanaUrno [64];
  char pclTanaTable [64];
  char pclFieldNameList [2048] = "\0";
  char pclFieldValueList [2048] = "\0";
  char pclOperandFieldType [16] = "\0";
  char pclOperandFieldLen [16] = "\0";
  char pclDummy [256];
  char pclLstu [64];
  char pclUserName [64];

  char clTwStart [64];
  char clTwEnd [64];

  int  jj, kk, mm, nn, res_fld, res_val;
  int  iFlagRounding, brace_o, brace_c, bracket_o, bracket_c, operator;
  long lTemp, lTemp2, lCount;
  float fFloat, fConstant;
  char pclOperand [64], *pc1, *pc2, *pcOperator, pclConstant [64];
  int  ilModIdSqlhdl;

  /* v.1.06 - hard code it to LOCAL time for BDPSUIF */
  (void) TimeToStr (pclLstu, time(NULL), LOCAL_TIME);

  /* if (strlen (pcgUserName) == 0)
    strcpy (pclUserName, "NTS_AUTO");
  else */
    strcpy (pclUserName, pcgUserName); /* from IRT sender */
  
  if ((!strcmp (clFval, " ")) || (clFval [0] == '\0'))
  {
    dbg (TRACE,
      "DoBdFormula: Empty or null FVAL for NTSTAB.URNO <%s> - Doing nothing", clUrno);
    return RC_SUCCESS;
  }
  else if ((!strcmp (clFlst, " ")) || (clFlst [0] == '\0'))
  {
    dbg (TRACE,
      "DoBdFormula: Empty or null FLST for NTSTAB.URNO <%s> - Doing nothing", clUrno);
    return RC_SUCCESS;
  } 
  else
    dbg (TRACE,"DoBdFormula: Formula: <%s> <%s> to update <%s>",
      clFnam, clFval, clFlst);

  strcpy (pclDummy, clFval);
  for (jj = 0; jj < strlen (pclDummy); jj++)
    pclDummy [jj] = (char) toupper (pclDummy [jj]);

  /* Determine rounding method if any */
  if (strstr (pclDummy, "CEIL"))
  {
    iFlagRounding = CEIL;
    dbg (TRACE, "DoBdFormula: ceil detected");
  }
  else if (strstr (pclDummy, "FLOOR"))
  {
    iFlagRounding = FLOOR;
    dbg (TRACE, "DoBdFormula: floor detected");
  }
  else
  {
    iFlagRounding = 0;
    dbg (TRACE, "DoBdFormula: No rounding method detected");
  }

  /* The operator */
  if (pcOperator = (strstr (pclDummy, "+")))
  {
    operator = ADDITION;
    dbg (TRACE, "DoBdFormula: Addition");
  }
  else if (pcOperator = (strstr (pclDummy, "-")))
  {
    operator =  SUBTRACT;
    dbg (TRACE, "DoBdFormula: Subtraction");
  }
  else if (pcOperator = (strstr (pclDummy, "*")))
  {
    operator =  MULTIPLY;
    dbg (TRACE, "DoBdFormula: Multiplication");
  }
  else if (pcOperator = (strstr (pclDummy, "/")))
  {
    operator = DIVIDE;
    dbg (TRACE, "DoBdFormula: Division");
  }
  else 
  {
    dbg (TRACE,"DoBdFormula: None or unsupported operator - Doing nothing");
    return RC_SUCCESS;
  }

  /* Look for the operand fieldname */
  pc1 = strstr (pclDummy, "{");
  pc2 = strstr (pclDummy, "}");

  if ((pc1 == '\0') || (pc2 == '\0'))
  {
    dbg (TRACE,"DoBdFormula: At least one brace missing - Doing nothing");
    return RC_SUCCESS;
  }
  else
  {
    jj = ((int) (pc2 - pc1)) - 1;
    strncpy (pclOperand,  (pc1 + 1), jj);
    pclOperand [jj] = '\0'; 
    dbg (TRACE,"DoBdFormula: operand <%s>", pclOperand);
  }
  
  (void) GetFieldType (clTana, pclOperand, pclOperandFieldType,
    pclOperandFieldLen);

  /*
    Look for the constant. Allow a decimal point. 
  */

  pc1 = pcOperator + 1;
  while ((!isdigit (*pc1)) && ((*pc1) != '\0'))
    pc1++;

  if ((*pc1) == '\0')
  {
    dbg (TRACE, "Could not find constant - Doing nothing");
    return RC_SUCCESS;
  }

  pc2 = pc1 + 1;
  while  ((isdigit (*pc2) || ((*pc2) == '.')) && (((*pc2) != '\0') || ((*pc2) != ')')))
    pc2++;
  jj = ((int) (pc2 - pc1));
  strncpy (pclConstant, (pc1), jj);
  pclConstant [jj] = '\0';
  fConstant = atof (pclConstant);
  dbg (TRACE, "DoBdFormula: constant <%12.3f>", fConstant);

  if ((fConstant == 0) && (operator == DIVIDE))
  {
    dbg (TRACE, "Not going to divide by zero...");
    return RC_SUCCESS; 
  }

  /*
    Select all rows from TANA and perform the operation on the field in 
    the formula.  
  */ 

  sprintf (pclTanaTable, "%s%s", clTana, cgTabEnd);
  if (strlen (pcpTanaUrno) == 0)
  {
    sprintf (clSqlBuf,
      "SELECT URNO, %s from %s",
      pclOperand, pclTanaTable);
  }
  else
  {
    /* v.1.07 - allow formula update to only one row in TANA */
    sprintf (clSqlBuf,
      "SELECT URNO, %s from %s where URNO = '%s'",
      pclOperand, pclTanaTable, pcpTanaUrno); 
  }

  dbg (TRACE, "DoBdFormula: <%s>", clSqlBuf);

  ilNoOfRec = 0;
  slFkt = START;
  slCursor = 0;
  sprintf (clTwEnd,"%s,%s,%s",cgHopo,cgTabEnd,mod_name);   

  ilGetRc = DB_SUCCESS;
  
  lCount = 0;
  while (ilGetRc == DB_SUCCESS)
  { 
    ilGetRc = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea);
    if (ilGetRc == DB_SUCCESS)
    {
      BuildItemBuffer (clDataArea, clFieldList, 2, ",");

      (void) get_real_item (pclTanaUrno, clDataArea, 1);
      (void) get_real_item (pclCurrValue, clDataArea, 2); 

      /* 
    Char type operands will be skipped if blank.
    Num type operands will be treated as zero if blank/empty.  
    There are apparantly more than one char type in SYSTAB, but only one
    number type.
      */

      if (pclOperandFieldType [0] != 'N')
      {
        if ((!strcmp (pclCurrValue, " ")) || (pclCurrValue [0] == '\0'))
        {
          dbg (TRACE,
            "DoBdFormula: URNO <%s> <%s> is char type but blank/null - skip it",
             pclTanaUrno, pclOperand);
          slFkt = NEXT;
          continue;
        }
      }
      else
      { 
        if ((!strcmp (pclCurrValue, " ")) || (pclCurrValue [0] == '\0'))
          strcpy (pclCurrValue, "0");

        dbg (TRACE,
          "DoBdFormula: URNO <%s> <%s> is num type but blank/null - treat is as zero",
          pclTanaUrno, pclOperand); 
      }
      
      /*
    Beware of using switch{}, because ceil() and floor() would be
    in a separate switch.
      */

      if (operator == ADDITION)
        fFloat = atof (pclCurrValue) + fConstant;

      if (operator == SUBTRACT)
        fFloat = atof (pclCurrValue) - fConstant;

      if (operator == MULTIPLY)
        fFloat = atof (pclCurrValue) * fConstant;

      if (operator == DIVIDE)
        fFloat = atof (pclCurrValue) / fConstant;

      if (iFlagRounding == CEIL)
        fFloat = ceil (fFloat);
       
      if (iFlagRounding == FLOOR)
        fFloat = floor (fFloat);
  
      sprintf (clWhereClause, " WHERE URNO = '%s' ", pclTanaUrno);
      sprintf (pclFieldNameList, "%s", clFlst);

      /*

    Many CEDA tables store numbers as char, so need to print the
    float such that space padding will not cause the string
    width to exceed such fields. If the fields are NUMBER, then
    this is not a problem. An aribitary width is chosen, such
    that can express e.g. 0.23.
    
    Also, very important: SQLHDL is not SQLPLUS! So single quotes
    must not be used even though we might be updating a char field.

    There is no way to make a good guess on the format needed. Maybe should
    be in NTSTAB.

      */

      mm = atoi (pclOperandFieldLen);

      if ((mm > 0) && (mm < 5)) 
        sprintf (pclFieldValueList, "%d", (long) fFloat); /* drop decimals */
      else if ((mm >= 5) && (mm < 9)) 
        sprintf (pclFieldValueList, "%4.1f", fFloat); 
      else
        sprintf (pclFieldValueList, "%4.2f", fFloat);

      sprintf (clTwStart, "%s,%s", "nts", pclTanaUrno);

    /*
	v.1.06 - append local time LSTU  -- assume that BDPSUIF did not save LSTU
	to NTSTAB so that this is not a duplicate. Note that updating LSTU for
	formula updates is new to NTSHDL - past versions left it blank.
    */
    strcat (pclFieldNameList, ",LSTU");
    strcat (pclFieldValueList, ",");
    if (strcmp (pcgCedaCmd,"IRT") == 0)
      strcat (pclFieldValueList, " "); 
    else
      strcat (pclFieldValueList, pclLstu); 

    /*
	v.1.08 - append blank USEU at IRT. See HandleData for how pcgUserName is evaluated.  
    */

    if (strcmp (pcgCedaCmd,"IRT") == 0)
    {
      strcat (pclFieldNameList, ",USEU");
      strcat (pclFieldValueList, ",");
      strcat (pclFieldValueList, " "); 
      strcpy (pclUserName, " ");
    }


      dbg(DEBUG, "DoBdFormula: URT to %d: <%s> <%s> <%s> <%s>",
        igModIdSqlhdl,pclTanaTable, clWhereClause, pclFieldNameList, pclFieldValueList);

      if (strlen (pcpTanaUrno) == 0)
        ilModIdSqlhdl = igModIdSqlhdl;
      else
	ilModIdSqlhdl = 506;	/* Load sharing queue - ver.1.07 */
      
      ilRC = SendCedaEvent (ilModIdSqlhdl,  /* Route ID */
        9802,               /* Origin ID */
        pclUserName,        /* Dest_name */
        "TOOL",             /* Recv_name */
        clTwStart,          /* CMDBLK TwStart */
        clTwEnd,            /* CMDBLK TwEnd */
        "URT",              /* CMDBLK command */
        pclTanaTable,           /* CMDBLK Obj name */
        clWhereClause,          /* Selection block */
        pclFieldNameList,       /* Field block */
        pclFieldValueList,      /* Data block */
        "",             /* Error description */
        4,              /* priority */
        NETOUT_NO_ACK);         /* return code */

      if (ilRC != RC_SUCCESS)
      {
         dbg (TRACE, "DoBdFormula: SendCedaEvent error <%ld>", ilRC);
      } 
        else
          lCount++;
    }
    else
    {
      if (ilGetRc == RC_FAIL)
      {
        dbg (TRACE, "DoBdFormula  : ORACLE ERROR !!");
        ilRC = RC_FAIL;
      }                     /* end if */ 
    } 
     slFkt = NEXT;
  }

  dbg (TRACE, "DoBdFormula: Updated <%ld> rows of <%s><%s>",
    lCount, pclTanaTable,pclFieldNameList); 
  close_my_cursor (&slCursor); 
  return RC_SUCCESS;
}

/******************************************************************************
 *
 *  GetFieldType - get the fieldtype from SYSTAB. What we are really
 *  interested in is when a field is of type NUMBER. Also get the 
 *  field length.
 * 
 ******************************************************************************
 */

int GetFieldType (char *clTana,char *clFina,char *clType,
  char *clFieldLen)
{
  int ilRC = RC_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf [2048] = "\0";
  char clDataArea [2048] = "\0";
  char clFieldList [2048] = "\0";
  char clWhereClause [2048] = "\0";
  int ilNoOfRec = 0;
  int ilGetRc = DB_SUCCESS;

  char pclTableName [32];

  sprintf (clSqlBuf,
    "SELECT FITY,FELE FROM SYSTAB WHERE TANA = '%s' and FINA = '%s'",
    clTana, clFina); 

  ilNoOfRec = 0;
  slFkt = START;
  slCursor = 0;
  ilGetRc = DB_SUCCESS;
  
  ilGetRc = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea);
  if (ilGetRc == DB_SUCCESS)
  { 
    BuildItemBuffer (clDataArea, clFieldList, 2, ","); 
    (void) get_real_item (clType, clDataArea, 1); 

    /* Sanity clause */
    if ((!strcmp (clType, " ")) || (clType [0] == '\0'))
      strcpy (clType, "C");

    (void) get_real_item (clFieldLen, clDataArea, 2); 

    if ((!strcmp (clFieldLen, " ")) || (clFieldLen [0] == '\0'))
      strcpy (clFieldLen, "6"); /* as arbitary as it gets */

  }
  else
  {
    /*
    Default type is character in case undefined in SYSTAB.  
    Maybe it should be configurable?
    */

    strcpy (clType, "C");
    strcpy (clFieldLen, "6");
  }

  close_my_cursor (&slCursor);
  return ilRC;
}

/******************************************************************************
 *
 *  HandleSQLCmd - ver.1.05 - from BDPSUIF - remove fields and values per
 *  configuration and then convey message to SQLHDL.
 *
 ******************************************************************************
 */
void HandleSQLCmd (int ipQueOut, int ipPriority, char *pcpTable, char *pcpFields, char *pcpValues,
                   char *pcpTwStart, char *pcpTwEnd, char *pcpSel, BC_HEAD *prpBchd )
{ 
  int ili, ilRC;
  int ilNumItems;
  char pclTmp[1024];
  char pclOneField[32];
  char pclOneValue[1024];
  char pclFields[1024];
  char pclValues[4096];
  char pclRemoveFields[256];
  char pclConfigLabel[64];
  char *clConfigTag = "_REMOVE_FIELDS";
  char *pclFunc = "HandleSQLCmd: ";
  
  memset( pclFields, 0, sizeof(pclFields) );
  memset( pclValues, 0, sizeof(pclValues) );

  /* Check for any fields to be removed */
  /* Still need to forward to SQLHDL even if no field need to be removed */
  sprintf (pclConfigLabel, "%s%s", pcpTable,clConfigTag);
  memset( pclRemoveFields, 0, sizeof(pclRemoveFields) );
  ilRC = ReadConfigEntry("BDPSUIF_URT",pclConfigLabel,pclRemoveFields, "");
  if( ilRC == RC_SUCCESS && strlen(pclRemoveFields) > 0 )
  {
      sprintf( pclTmp, ",%s,", pclRemoveFields );
      strcpy( pclRemoveFields, pclTmp );

      dbg( TRACE,"%s [%s] Fields to be removed <%s>", pclFunc, pclConfigLabel, pclRemoveFields );
      ilNumItems = get_no_of_items(pcpFields);
      dbg( TRACE,"%s BEFORE REMOVE! NUMBER OF FIELDS <%d>", pclFunc, ilNumItems );

      for( ili = 1; ili <= ilNumItems; ili++ )
      {
          get_real_item( pclOneField, pcpFields, ili );
          sprintf( pclTmp, ",%s,", pclOneField );
          if( strstr( pclRemoveFields, pclTmp ) != NULL )
          {
              dbg( DEBUG, "%s Found field <%s> to be removed..", pclFunc, pclOneField );
              continue;
          }
          get_real_item( pclOneValue, pcpValues, ili );
          strcat( pclFields, pclOneField );
          strcat( pclValues, pclOneValue );
          strcat( pclFields, "," );
          strcat( pclValues, "," );
      }
      ili = strlen(pclFields);
      if( ili > 0 && pclFields[ili-1] == ',' )
          pclFields[ili-1] = '\0';
      ili = strlen(pclValues);
      if( ili > 0 && pclValues[ili-1] == ',' )
          pclValues[ili-1] = '\0';
      dbg( DEBUG, "%s AFTER REMOVAL +++\n FIELDS <%s> \n VALUES <%s>", pclFunc, pclFields, pclValues );
      ilNumItems = get_no_of_items(pclFields);
      dbg( TRACE,"%s AFTER REMOVE! NUMBER OF FIELDS <%d>", pclFunc, ilNumItems );
  }
  if( ilRC != RC_SUCCESS )
  {
      strcpy( pclFields, pcpFields );
      strcpy( pclValues, pcpValues );
  }


  /*
    Update via SQLHDL load sharing queue.
  */
  dbg(TRACE, "%s UpdBasicData: Send <URT> for <%s> URNO <%s> to <%d>", pclFunc,pcpTable,pcpSel,506);

  ilRC = SendCedaEvent (506,                     /* Route ID */
                        ipQueOut,                /* Origin ID */
                        prpBchd->dest_name,       /* Dest_name */
                        prpBchd->recv_name,       /* Recv_name */
                        pcpTwStart,              /* CMDBLK TwStart */
                        pcpTwEnd,                /* CMDBLK TwEnd */
                        "URT",                   /* CMDBLK command */
                        pcpTable,                /* CMDBLK Obj name */
                        pcpSel,                  /* Selection block */
                        pclFields,               /* Field block */
                        pclValues,               /* Data block */
                        "",                      /* Error description */
                        ipPriority,              /* priority */
                        RC_SUCCESS);                /* return code */

  if (ilRC != RC_SUCCESS)
  {
      dbg (TRACE, "%s SendCedaEvent error <%ld>", pclFunc,ilRC);
  } 

}

/******************************************************************************
 *
 *  HandleIRT - ver.1.07 - intercept IRT to update BDPSUIF tables if
 *  formula are available in NTSTAB. Algorithm per JIRA:
By right, these 2 fields should not allowed for insertion for newly created
aircraft records. And user has to be 'force' to use the 'Add-on' module for
weight management.  

To cater for this:
1. configure action.cfg to:

    * receive any IRT command of ACRTAB (URT and DRT are NOT required)
    * forward command to NTSHDL
      2. amend NTSHDL to:
    * acknowledge IRT command only, for now NTSHDL should only recognised IRT and NTS commands
    * search in NTSTAB for table names that match what is published in the IRT command
    * apply any formula (current formula only) to this record if the table name is found
    * send URT command to load balancing queue (506) to the same table with the processed formula

NOTE: if multiple formulas are found, apply all formula and send all updates to
SQLHDLs for update. Formula applied should only be current applicable formula,
ignored PAST/FUTURE formula. configuration and then convey message to SQLHDL.
 *
 * DKA - due to the way formula have been implemented, each formula can accept
 * only one input field in the formula and one result field.

 * v.1.08 - Send blanked USEU and LSTU to sqlhdl.
 ******************************************************************************
 */

static void HandleIRT (char *pclTable, char *pcpFld, char *pclData, char *pcpTanaUrno)
{

  char pclFunc[] = "HandleIRT: ";
  char pclIrtField [64];
  char pclIrtData [64];


  int ilRC = RC_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf [2048] = "\0";
  char clDataArea [2048] = "\0";
  char clFieldList [2048] = "\0";
  char clWhereClause [2048] = "\0";
  int ilNoOfRec = 0;
  int ilGetRc = DB_SUCCESS;

  char pclUrno [64] = "\0";
  char pclFlst [512] = "\0";
  char pclFval [512] = "\0"; 
  char pclFnam [128] = "\0";
  char pclVafr [32] = "\0"; 
  char pclVato [32] = "\0";
  char clDateTime [32] = "\0"; 
  int  ilNumItems, ili; 
  char pclOneField[32];
  char pclOneValue[1024];
  char clTwStart [64];
  char clTwEnd [64];
  char pclTable3 [32];

  strncpy (pclTable3, pclTable, 3);
  pclTable3 [3] = '\0';
  dbg(TRACE,"%s Search in NTSTAB for formula for table <%s> <%s> fields <%s>",
    pclFunc,pclTable,pclTable3,pcpFld);

  (void) TimeToStr (clDateTime, time(NULL), UTC_TIME);
  strcpy (clFieldList, "URNO,FLST,FVAL,FNAM,VAFR,VATO");
  sprintf (clWhereClause,
    " WHERE TANA = '%s' AND UTAN = '0' AND STAT = 'C' AND VAFR <= '%s' AND (VATO > '%s' OR TRIM(VATO) IS NULL) ",
    pclTable3, clDateTime, clDateTime);
 
  sprintf (clSqlBuf,
    "SELECT %s FROM NTSTAB %s ORDER BY FNAM,VAFR DESC",
    clFieldList, clWhereClause);
  dbg (TRACE, "%s <%s>", pclFunc, clSqlBuf);

  ilNoOfRec = 0;
  slFkt = START;
  slCursor = 0;


  ilGetRc = DB_SUCCESS; 
  while (ilGetRc == DB_SUCCESS)
  { 
    ilGetRc = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea);
    if (ilGetRc == DB_SUCCESS)
    {
      BuildItemBuffer (clDataArea, clFieldList, 6, ",");

      (void) get_real_item (pclUrno, clDataArea, 1);
      (void) get_real_item (pclFlst, clDataArea, 2); 

      /*
        Important: we depend on the assumption that FVAL for UTAN 0
	has a formula applying to only one field name. And it better
	be in uppercase.
      */ 

      (void) get_real_item (pclFval, clDataArea, 3);
      (void) get_real_item (pclFnam, clDataArea, 4);
      (void) get_real_item (pclVafr, clDataArea, 5);
      (void) get_real_item (pclVato, clDataArea, 6);
      
      dbg (TRACE, 
        "%s: Found formula <%s> NTSTAB.URNO <%s> <%s> to update -> <%s>",
	pclFunc,pclFnam,pclUrno,pclFval,pclFlst);
      
      ilNumItems = get_no_of_items(pcpFld);
      
      for( ili = 1; ili <= ilNumItems; ili++ )
      {
        get_real_item (pclOneField, pcpFld, ili);
	/*
          Is the inserted field within the formula?  
	*/
        if(strstr(pclFval, pclOneField) != NULL )
        {
          dbg(TRACE, "%s Field <%s> in IRT list is in formula <%s> <%s> NTSTAB.URNO <%s>",
            pclFunc, pclOneField,pclFval,pclFnam,pclUrno); 

          /* Actually the STAT is unused in DoBdFormula... */ 
          (void) DoBdFormula (pclUrno, pclTable3, pclFlst, pclFval,
                  pclFnam, "C", pcpTanaUrno);
        }
	else
	{
          dbg(TRACE, "%s Field <%s> in IRT list not in formula <%s> <%s> NTSTAB.URNO <%s>",
            pclFunc, pclOneField,pclFval,pclFnam,pclUrno); 
	  continue;
	}
      } /* end for */
      
    } /* end if */
    else
    {
      if (ilGetRc == RC_FAIL)
      {
        dbg (TRACE, "%s ORACLE ERROR !! <%d>", pclFunc, ilGetRc);
        ilRC = RC_FAIL;
      } 
    }
    slFkt = NEXT; 
  }  /* end while */

  close_my_cursor (&slCursor); 


}

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
