#ifndef _DEF_mks_version
#define _DEF_mks_version
#include "ufisvers.h"           /* sets UFIS_VERSION, must be done before mks_version */
static char mks_version[] = "@(#) " UFIS_VERSION " $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/ssihdl.c 3.4 2012/11/07 14:19:50SGT fya Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB AAT/I SSIHDL.C                                                         */
/*                                                                            */
/* Author         : Andreas Kloidt                                            */
/* Date           : April 2002                                                */
/* Description    : Process to maintain Seasonal Table (SSITAB)               */
/*                                                                            */
/* Update history :                                                           */
/* 01-AUG-08   BLE: Amend SSITAB and SSRTAB to run on its own URNO            */
/* 01-AUG-08   BLE: New cmd to truncate and pour new data into table          */
/* 17-FEB-09   BLE: Amend GenerateSSIM-GetSSIMDataLine for errorneous         */
/*                  STOD/STOA tabulation                                      */
/* 23-NOV-2011 ZWE: Added cmd SSIF                                            */
// 08-DEC-2011 ZWE: insert data to stya,styd from afttab's styp               */ 
// 04-APR-2012 ZWE: extract SSITAB.STYP info from AFTTAB                      */
//                  EXPORT file result can have TTYP or STYP                  */
// 14-APR-2012 ZWE: receive cmd from ntisch                                   */
//                  generate ssi data file for CargoSpot                      */
// 29-MAY-2012 ZWE: change vafi and vato value when generate CargoSpot file   */
// 20121102    FYA->UFIS-1756:1>Delete the file name at 1st row of the file
//                            2>Modify the time offset instead of copy it from the previous item
// 20121108		 FYA->UFIS-1756:1>Add month range config option for searching the season in ssitab and ssrtab
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_ssihdl[]="%Z% UFIS 4.4 (c) ABB AAT/I %M% %I% / %E% %U% / AKL";
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <netdb.h>
#include <time.h>
#include <dirent.h>
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h" 
#include "tools.h"
#include "helpful.h"
#include "timdef.h"
#include "urno_fn.h"
//#include "urno_fn.inc"
 

#define XS_BUFF  128
#define S_BUFF   512
#define M_BUFF   1024
#define L_BUFF   2048
#define XL_BUFF  4096
#define XXL_BUFF 8192 
#define XXXL_BUFF 32000 

#define MAX_TEXT_FILE_SIZE 5000000
#define MAX_NO_OF_AIRPORTS 1000
#define MAX_NO_TTB_FLIGHTS 25000
#define MAX_NO_OF_ARR_FLIGHTS_SSI 500
#define MAX_NO_OF_DEP_FLIGHTS_SSI 500
#define MAX_NO_OF_ARR_FLIGHTS_SSR 500
#define MAX_NO_OF_DEP_FLIGHTS_SSR 500
#define MAX_NO_OF_ROT_FLIGHTS_SSR 500
#define MAX_VIAS 10  /* the maximum number of legs VIAL can hold (1024/120) */
enum DirectionType {LEFT,RIGHT};

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
FILE *outp       = NULL;
int  debug_level = TRACE;
/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/
extern int SetSignals(void (*HandleSignal)(int));
extern int DebugPrintItem(int,ITEM *);
extern int DebugPrintEvent(int,EVENT *);
extern int init_db(void);
extern int  ResetDBCounter(void);
extern void HandleRemoteDB(EVENT*);
extern int  sql_if(short ,short* ,char* ,char* ); 
extern int close_my_cursor(short *cursor); 
extern void snap(char*,int,FILE*);
extern int GetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel,char *pcpDef,char *pcpTrim);
extern int BuildItemBuffer(char *pcpData, char *pcpFieldList, int ipNoOfFields, char *pcpSepChr);
extern int get_item_no(char *s, char *f, short elem_len);
extern void  GetServerTimeStamp(char*,int,long,char*);
extern int AddSecondsToCEDATime(char *,long,int);
extern int get_real_item(char *, char *, int);
extern int GetNextValues(char *, int);
extern int GetFullDay(char *, int *, int *, int *);
extern char *getItem(char *,char *,char *);
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;        /* The queue item pointer  */
static EVENT *prgEvent     = NULL;        /* The event pointer       */
static int   que_out;                     /* originator */
static int   igItemLen     = 0;           /* length of incoming item */
static int   igInitOK      = FALSE;      /* Flag for init */

/*   static CFG   *prgCfg; */                     /* structure of the cfg-file */
static char  pcgHomeAp[XS_BUFF];      /* buffer for home airport */
static char  pcgTabEnd[XS_BUFF];      /* buffer for TABEND */
static char  pcgTwStart[XS_BUFF] = "";
static char  pcgTwEnd[XS_BUFF];
static char  pcgConfFile[S_BUFF];      /* buffer for config-file name */
static int   igUseHopo = FALSE;          /* flag for use of HOPO-field */
static int   hasStyad = FALSE;
static int   hasStyp = FALSE;
static int   igModID_Router  = 1200;      /* MOD-ID of Router  */

static char      pcgHostName[XS_BUFF];
static char      pcgHostIp[XS_BUFF];

static int    igQueCounter=0;

/*entry's from configfile*/
static char pcgConfigFile[512];
static char pcgCfgBuffer[512];
static char pcgInitSsi[32] = "NO";
static char pcgInitSsr[32] = "NO";
static int  igHandleSSI = TRUE;
static int  myEXPORT_WITH_TTYP_OR_STYP = FALSE;
static int  igHandleSSR = TRUE;
static char pcgSeasonsForInit[1024] = "";
static char pcgSeasonsNotForUpdate[1024] = "";
static char pcgValidFtyp[512] = "";
static char pcgSSIMFilePath[128] = "/ceda/export/ssim";
static char pcgSSIMFileName[128] = "SSIM";
static char myCargoSSIMFileName[128] = "SSIM";
static char myCargoSSIMFilePath[128] = "/ceda/exco/FILES/CARGOSPOT";
static char myCargoSSIMFilePathBak[128] = "/ceda/exco/FILES/CARGOSPOT/arc";
static int  myCargoSSIMTimePeriod = 5;
static char myCargoSSIMFileExt[32] = "TXT";
static char pcgSSIMFileExt[32] = "TXT";
static char pcgSSIMVersion[8] = "0001";
static char pcgSSIMReference[8] = "SSIHDL";
static int  igSSIMLocalTimes = FALSE;
static char pcgTTBFilePath[128] = "/ceda/export/ttb";
static char pcgTTBFileName[128] = "TTB";
static char pcgTTBFileExt[32] = "TXT";
static char pcgFLUKOFilePath[128] = "/ceda/export/fluko";
static char pcgFLUKOFileName[128] = "FLUKO";
static char pcgFLUKOFileExt[32] = "TXT";

//Frank 20121107 v3.4
static int igMonthRangeForSeason = 12;

static char pcgSLOTCOFilePath[128] = "/ceda/export/slotco";
static char pcgSLOTCOFileName[128] = "SLOTCO";
static char pcgSLOTCOFileExt[32] = "TXT";
static char pcgSLOTCOHeader[512] = "";
static char pcgTriggerAction[32] = "NO";
static char pcgTriggerBchdl[32] = "NO";
static char pcgDebugLevel[32] = "TRACE";
static char pcgDebugLevelInit[32] = "TRACE";
static char pcgErrorDate[16] = "??????????????";
static char cgHopo[8] = "\0";                 /* default home airport    */

/* Structures */
typedef struct
{
  char pclTtyp[8];
  char pclAct3[8];
  char pclAct5[8];
  char pclAlc2[8];
  char pclAlc3[8];
  char pclFltn[8];
  char pclFlns[8];
  char pclJcnt[8];
  char pclJfno[128];
  char pclStoa[16];
  char pclStod[16];
  char pclOrg3[8];
  char pclOrg4[8];
  char pclDes3[8];
  char pclDes4[8];
  char pclVia3[8];
  char pclVia4[8];
  char pclVian[8];
  char pclVial[1030];
  char pclAdid[8];
  char pclUrno[16];
  char pclStyp[8];
} FLIGHT_RECORD_SSI;
#define FLIGHT_RECORD_SIZE_SSI sizeof(FLIGHT_RECORD_SSI)
FLIGHT_RECORD_SSI *pcgArrFlightSSI;
FLIGHT_RECORD_SSI *pcgDepFlightSSI;
typedef struct
{
  char pclNstp[8];
  char pclAct3[8];
  char pclAct5[8];
  char pclFtyp[8];
  char pclFlta[16];
  char pclAl2a[8];
  char pclAl3a[8];
  char pclFlna[8];
  char pclFlsa[8];
  char pclJcna[8];
  char pclJfna[128];
  char pclStoa[16];
  char pclStda[16];
  char pclOrg3[8];
  char pclOrg4[8];
  char pclVi3a[8];
  char pclVi4a[8];
  char pclVina[8];
  char pclVila[1030];
  char pclTtpa[8];
  char pclUrna[16];
  char pclFltd[16];
  char pclAl2d[8];
  char pclAl3d[8];
  char pclFlnd[8];
  char pclFlsd[8];
  char pclJcnd[8];
  char pclJfnd[128];
  char pclStod[16];
  char pclStad[16];
  char pclDes3[8];
  char pclDes4[8];
  char pclVi3d[8];
  char pclVi4d[8];
  char pclVind[8];
  char pclVild[1030];
  char pclTtpd[8];
  char pclUrnd[16];
  char pclStya[8];
  char pclStyd[8];
} FLIGHT_RECORD_SSR;
#define FLIGHT_RECORD_SIZE_SSR sizeof(FLIGHT_RECORD_SSR)
FLIGHT_RECORD_SSR *pcgArrFlightSSR;
FLIGHT_RECORD_SSR *pcgDepFlightSSR;
FLIGHT_RECORD_SSR *pcgRotFlightSSR;

typedef struct
{
  char pclVafr[16];
  char pclVato[16];
  char pclFreq[8];
  char pclDoop[8];
  char pclOrg3[8];
  char pclStod[8];
  char pclStoa[8];
  char pclDes3[8];
  char pclAlc3[8];
  char pclFltn[8];
  char pclFlns[8];
  char pclAct3[8];
  char pclVian[8];
  char pclVial[256];
  char pclFlno[16];
} FLIGHT_RECORD_TTB;
#define FLIGHT_RECORD_SIZE_TTB sizeof(FLIGHT_RECORD_TTB)
FLIGHT_RECORD_TTB *pcgFlightRecordTTB;

/* Layout of AFT VIAL */
struct VialTypeAFT {
  char display[1];
  char apc3[3];
  char apc4[4];
  char stoaDate[8];
  char stoaTime[4];
  char stoaSec[2];
  char etoa[14];
  char land[14];
  char onbl[14];
  char stodDate[8];
  char stodTime[4];
  char stodSec[2];
  char etod[14];
  char ofbl[14];
  char airb[14];
};

typedef struct VialTypeAFT *VialTypePtrAFT;

/* Layout of SSI VIAL */
struct VialTypeSSI {
  char apc3[3];
  char apc4[4];
  char stoaTime[4];
  char stodTime[4];
};

typedef struct VialTypeSSI *VialTypePtrSSI;

/* Global Variables */
static char pcgCurrentTime[32];
static char pcgUrnoArray[52][8][25];
static char pcgDayArray1[52][8][9];
static char pcgDayArray2[52][9];
static int igMinWeek;
static int igMaxWeek;
static int igMinDay;
static int igMaxDay;
static int igNoDoop;
static char pcgDoop[52][8];
static int igFreq[52];
static char pcgVpfr[52][9];
static char pcgVpto[52][9];
static char pcgUrnoList[52][5500];
static int igNoAirports;
static int igNoFlights;
static char pcgTimeDiffField[8];
static int igTimeDiff;
static char pcgTiDiHopo[8];
static char pcgCreatedFileName[256];
static char pcgCreatedFileNameArr[256];
static char pcgCreatedFileNameDep[256];
static int igFirstDay;
typedef struct
{
  char pclApc3[8];
  char pclTiDi[8];
  int ilTiDi;
  char pclApfn[64];
  char pclAptt[4];
} TIME_DIFF;
#define TIME_DIFF_SIZE sizeof(TIME_DIFF)
TIME_DIFF pcgTimeDiff[MAX_NO_OF_AIRPORTS];
static int igTimeDiffCount;
static char pcgNextUrno[256];
static int igNextUrno;

typedef struct {
  int  offset;
  char vafr[9];
  char vato[9];
  char doop[8];
  char org3[4];
  char des3[4];
  char stoa[16];
  char stod[16];
} LEG_TYPE;

/* Begin Array Insert */
REC_DESC rgRecDesc;
static int igUseArrayInsert = FALSE;
static char pcgNewUrnos[1010*16];
static int igLastUrno = 0;
static int igLastUrnoIdx = -1;
static int igArrayPtr = 0;
static char pcgSsiFldLst[] = "URNO,HOPO,SEAS,VAFR,VATO,DOOP,FREQ,TTYP,ACT3,ACT5,FLNO,ALC2,ALC3,FLTN,FLNS,JCNT,JFNO,STOA,STOD,ORG3,ORG4,DES3,DES4,VIA3,VIA4,VIAN,VIAL,ADID,URL1,URL2";
static char pcgSsiValLst[] = ":VURNO,:VHOPO,:VSEAS,:VVAFR,:VVATO,:VDOOP,:VFREQ,:VTTYP,:VACT3,:VACT5,:VFLNO,:VALC2,:VALC3,:VFLTN,:VFLNS,:VJCNT,:VJFNO,:VSTOA,:VSTOD,:VORG3,:VORG4,:VDES3,:VDES4,:VVIA3,:VVIA4,:VVIAN,:VVIAL,:VADID,:VURL1,:VURL2";
static char pcgSsrFldLst[] = "URNO,HOPO,SEAS,VAFR,VATO,DOOP,FREQ,NSTP,ACT3,ACT5,FTYP,FLTA,AL2A,AL3A,FLNA,FLSA,JCNA,JFNA,STOA,STDA,ORG3,ORG4,VI3A,VI4A,VINA,VILA,TTPA,FLTD,AL2D,AL3D,FLND,FLSD,JCND,JFND,STOD,STAD,DES3,DES4,VI3D,VI4D,VIND,VILD,TTPD,URL1,URL2,URL3";
static char pcgSsrValLst[] = ":VURNO,:VHOPO,:VSEAS,:VVAFR,:VVATO,:VDOOP,:VFREQ,:VNSTP,:VACT3,:VACT5,:VFTYP,:VFLTA,:VAL2A,:VAL3A,:VFLNA,:VFLSA,:VJCNA,:VJFNA,:VSTOA,:VSTDA,:VORG3,:VORG4,:VVI3A,:VVI4A,:VVINA,:VVILA,:VTTPA,:VFLTD,:VAL2D,:VAL3D,:VFLND,:VFLSD,:VJCND,:VJFND,:VSTOD,:VSTAD,:VDES3,:VDES4,:VVI3D,:VVI4D,:VVIND,:VVILD,:VTTPD,:VURL1,:VURL2,:VURL3";
static char pcgSsiBuf[200*7500];
/* End Array Insert */

/* for trigger action */
static EVENT *prgOutEvent = NULL;

static int    Init_ssihdl();
static int    Reset(void);                        /* Reset program          */
static void   Terminate(int);                     /* Terminate program      */
static void   HandleSignal(int);                  /* Handles signals        */
static void   HandleErr(int);                     /* Handles general errors */
static void   HandleQueErr(int);                  /* Handles queuing errors */
static int    HandleInternalData(void);           /* Handles event data     */
static void   HandleQueues(void);                 /* Waiting for Sts.-switch*/
/******************************************************************************/
/* Function prototypes by AKL                                                 */
/******************************************************************************/
/* Init-functions  */

static int GetQueues();
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
                     char *pcpTwStart,char* pcpTwEnd,
             char *pcpSelection,char *pcpFields,char *pcpData,
                     char *pcpAddStruct,int ipAddstructSize); 
static int GetConfig();
static int TimeToStr(char *pcpTime,time_t lpTime);
static int InitSsi();
static int InitSsi2();
static int InitSsr();
static int InitSsr2();
static int TriggerBchdlAction(char *pcpCmd, char *pcpTable, char *pcpSelection,
                              char *pcpFields, char *pcpData, int ipBchdl, int ipAction);
static int GetMyFullDay(char *pclDateTime, int *ipDays, int *ipMins, int *ipWeekDay,
                        int *ipWeek);
static void TrimRight(char *pcpBuffer);
static int GenerateSSI(char *pcpCurrentSeason, int ipTruncateTable);
static int GenerateSSR(char *pcpCurrentSeason, int ipTruncateTable);
static int GenerateFlightSSI(char *pcpCurrentSeason, char *pcpCurFlno, char *pcpFirstDay,
                             char *pcpLastDay, int ipCommit);
static int GenerateFlightSSR(char *pcpCurrentSeason, char *pcpCurFlno, char *pcpFirstDay,
                             char *pcpLastDay, char *pcpMode,
                             char *pcpRotFlno, int ipCommit);
static int GetFlightDataSSI(FLIGHT_RECORD_SSI *pcpFlight, char *pcpRec);
static int GetFlightDataSSR(FLIGHT_RECORD_SSR *pcpFlight, char *pcpRecA, char *pcpRecD);
static int GetFlightDataSSR_Rotation(FLIGHT_RECORD_SSR *pcpFlight, char *pcpRecA, char *pcpRecD);
static int GenerateDayArraysSSI(FLIGHT_RECORD_SSI *pcpFlight, char *pcpType, int ipNo);
static int GenerateDayArraysSSR(FLIGHT_RECORD_SSR *pcpFlight, char *pcpType, int ipNo);
static int GenerateDoop();
static int InsSSITAB(char *pcpCurrentSeason, char *pcpCurFlno, FLIGHT_RECORD_SSI *pcpFlight,
                     int ipCommit);
static int InsSSITAB_STYP(char *pcpCurrentSeason, char *pcpCurFlno, FLIGHT_RECORD_SSI *pcpFlight,
                     int ipCommit);
static int InsSSRTAB(char *pcpCurrentSeason, FLIGHT_RECORD_SSR *pcpFlight, int ipCommit);
static int InsSSRTAB_STYAD(char *pcpCurrentSeason, FLIGHT_RECORD_SSR *pcpFlight, int ipCommit);
static int SortRot(FLIGHT_RECORD_SSR *pcpRotFlight, int ipRotCount, char *pcpMode);
static int SortRotField(FLIGHT_RECORD_SSR *pcpRotFlight, int ipRotCount,
                        char *pcpField1, char *pcpField2);
static int UpdateSSI(char *pcpUrno, char *pcpFields, char *pcpData);
static int UpdateSSR(char *pcpUrno, char *pcpFields, char *pcpData);
static int GenerateSSIM(char *pcpSeas);
static int GenerateSSIMCargoSpot();
static int DeleteFlightSSR(char *pcpSeas, char *pcpArrFlt, char *pcpDepFlt);
static int GetSSIMHeader(FILE *pfpSSIMFilePtr, char *pcpSeas, char *pcpVpfr,
                         char *pcpVpto, char *pcpCurrentTime);
static int GetSSIMFooter(FILE *pfpSSIMFilePtr, char *pcpCurrentTime, int ipCurLine);
static int GetCorrectedFormatBlank(char *pcpResult, char *pcpString, int ipLen);
static int GetCorrectedFormatZero(char *pcpResult, char *pcpString, int ipLen);
static int GetMonthAlpha(char *pcpResult, char *pcpDate);
static int GetMonthNumeric(char *pcpResult, char *pcpDate);
static int GetSSIMDataLine(FILE *pfpSSIMFilePtr, char *pcpField, char *pcpRec, int *ipCurLine, int ipSeq);
static int GetSSIMDataLine2(FILE *pfpSSIMFilePtr, char *pcpField, char *pcpRec, int *ipCurLine, int ipSeq,char *vafr,char *vato);
static int ConvertSSIMToLocal(char *pcpLine);
static int AdjustDate(char *pcpNewDate, char *pcpOrgDate, char cpDirection);
static int GenerateTTB(char *pcpSeas);
static int StoreTTBFlightInfo(FLIGHT_RECORD_TTB *pcpFlight, char *pcpFlightRec,
                              char *pcpType, int *ipNoFlights);
static int GenerateTTBFile(FILE *pfpFilePtr, FLIGHT_RECORD_TTB *pcpFlight,
                           int ipNoFlights, char *pcpType);
static int SortTTB(FLIGHT_RECORD_TTB *pcpFlight, int ipCount, char *pcpType);
static int SortTTBField(FLIGHT_RECORD_TTB *pcpFlight, int ipCount,
                        char *pcpField1, char *pcpField2, char *pcpField3);
static int UtcToLocalTTB(FLIGHT_RECORD_TTB *pcpFlight,int ipNoFlights, char *pcpType);
static int GenerateFLUKO(char *pcpSeas);
static int GetFLUKODataLine(FILE *pfpFLUKOFilePtr, char *pcpDataBuf, char *pcpType);
static int GenerateSLOTCO(char *pcpSeas);
static int GetSLOTCODataLine(FILE *pfpSLOTCOFilePtr, char *pcpDataBuf);
static int HandleData(char *pcpCmd, char *pcpTable, char *pcpSelection, char *pcpFields,
                      char *pcpData);
static int GetFileNames(char *pcpCmd, char *pcpTable, char *pcpSelection);
static int ReadFile(char *pcpCmd, char *pcpTable, char *pcpSelection);
static int GetDirectory(char *pcpDirName, char *pcpFileName, char *pcpTable);
static int CreateFile(char *pcpCmd, char *pcpTable, char *pcpSelection);
static int DeleteFile(char *pcpCmd, char *pcpTable, char *pcpSelection);
static int ConvertTimeField(char *pcpTime);
static int GetTimeDiff(char *pcpTiDi, int *ipTiDi, char *pcpApt);
static int SqlArrayInsert(char *pcpTable, char *pcpFldList, char *pcpValList);
static void CorrectDateTime(char *,char *,char *,int *,int);
static void shiftData (LEG_TYPE *,int);
static int CedaDateToSSIM(char *, char *);
static int checkCedaTime(char *pcpData);
static int ResetSSI();
static int LoadSeas( char *pcpSeason );
static int  GetFieldLength(char *pcpTana, char *pcpFina, long *pipLen);
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
  int    ilRc = RC_SUCCESS;            /* Return code            */
  int    ilCnt = 0;
  int   ilItemFlag=TRUE; 
  time_t now = 0;
  INITIALIZE;            /* General initialization    */

  /* signal handling of SIGPIPE,SIGCHLD,SIGALRM,SIGTERM */
  SetSignals(HandleSignal);

  dbg(TRACE,"------------------------------------------");
  dbg(TRACE,"MAIN: version <%s>",sccs_ssihdl);

  /* Attach to the MIKE queues */
  do{
    ilRc = init_que();
    if(ilRc != RC_SUCCESS)
      {
    dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
    sleep(6);
    ilCnt++;
      }/* end of if */
  }while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
      sleep(60);
      exit(1);
    }else{
      dbg(TRACE,"MAIN: init_que() OK!");
      dbg(TRACE,"MAIN: mod_id   <%d>",mod_id);
      dbg(TRACE,"MAIN: mod_name <%s>",mod_name);
    }/* end of if */
  do
    {
      ilRc = init_db();
      if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
      sleep(6);
      ilCnt++;
    } /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
      sleep(60);
      exit(2);
    }else{
      dbg(TRACE,"MAIN: init_db()  OK!");
    } /* end of if */

  /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
  *pcgConfFile = 0x00;
  sprintf(pcgConfFile,"%s/%s",getenv("BIN_PATH"),mod_name);
  ilRc = TransferFile(pcgConfFile);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
    } /* end of if */
  dbg(TRACE,"MAIN: Binary-file = <%s>",pcgConfFile);
  ilRc = SendRemoteShutdown(mod_id);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    }
  if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: waiting for status switch ...");
      HandleQueues();
      dbg(TRACE,"MAIN: now running ...");
    }/* end of if */
  if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: initializing ...");
      dbg(TRACE,"------------------------------------------");
      if(igInitOK == FALSE)
    {
      ilRc = Init_ssihdl();
      if(ilRc == RC_SUCCESS)
        {
          dbg(TRACE,"");
          dbg(TRACE,"------------------------------------------");
          dbg(TRACE,"MAIN: initializing OK");
          igInitOK = TRUE;
        } 
    }
    }else{
      Terminate(1);
    }
  dbg(TRACE,"------------------------------------------");
    
  if (igInitOK == TRUE)
    {
      now = time(NULL);
      while(TRUE)
    {
      memset(prgItem,0x00,igItemLen);
      ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
      dbg(DEBUG,"QUE Counter %d",++igQueCounter);
      /* depending on the size of the received item  */
      /* a realloc could be made by the que function */
      /* so do never forget to set event pointer !!! */
      prgEvent = (EVENT *) prgItem->text;
      if( ilRc == RC_SUCCESS )
        {
          /* Acknowledge the item */
          ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
          if( ilRc != RC_SUCCESS ) 
        {
          /* handle que_ack error */
          HandleQueErr(ilRc);
        } /* fi */
          switch( prgEvent->command )
        {
        case    HSB_STANDBY    :
          ctrl_sta = prgEvent->command;
          HandleQueues();
          break;    
        case    HSB_COMING_UP    :
          ctrl_sta = prgEvent->command;
          HandleQueues();
          break;    
        case    HSB_ACTIVE    :
          ctrl_sta = prgEvent->command;
          break;    
        case    HSB_ACT_TO_SBY    :
          ctrl_sta = prgEvent->command;
          /* CloseConnection(); */
          HandleQueues();
          break;    
        case    HSB_DOWN    :
          /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
          ctrl_sta = prgEvent->command;
          Terminate(1);
          break;    
        case    HSB_STANDALONE    :
          ctrl_sta = prgEvent->command;
          ResetDBCounter();
          break;    
        case    REMOTE_DB :
          /* ctrl_sta is checked inside */
          HandleRemoteDB(prgEvent);
          break;
        case    SHUTDOWN    :
          /* process shutdown - maybe from uutil */
          Terminate(10);
          break;
        case    RESET        :
          ilRc = Reset();
          break;
        case    EVENT_DATA    :
          if((ctrl_sta == HSB_STANDALONE) ||
             (ctrl_sta == HSB_ACTIVE) ||
             (ctrl_sta == HSB_ACT_TO_SBY))
            {
              ilItemFlag=TRUE;
              ilRc = HandleInternalData();
              if(ilRc != RC_SUCCESS)
            {
              HandleErr(ilRc);
            }/* end of if */
            }
          else
            {
              dbg(TRACE,"MAIN: wrong HSB-status <%d>",ctrl_sta);
              DebugPrintItem(TRACE,prgItem);
              DebugPrintEvent(TRACE,prgEvent);
            }/* end of if */
          break; 
        case    TRACE_ON :
          dbg_handle_debug(prgEvent->command);
          break;
        case    TRACE_OFF :
          dbg_handle_debug(prgEvent->command);
          break;
        default            :
          dbg(TRACE,"MAIN: unknown event");
          DebugPrintItem(TRACE,prgItem);
          DebugPrintEvent(TRACE,prgEvent);
          break;
        } /* end switch */
        }else{
          /* Handle queuing errors */
          HandleQueErr(ilRc);
        } /* end else */



      /**************************************************************/
      /* time parameter for cyclic actions                          */
      /**************************************************************/
    
      now = time(NULL);

    } /* end while */
    }else{
      dbg(TRACE,"MAIN: Init_ssihdl() failed with <%d> Sleeping 30 sec.! Then terminating ...",ilRc);
      sleep(30);
    }
  exit(0);
  return 0;
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_ssihdl()
{
  int    ilRc = RC_SUCCESS;            /* Return code */
  char pclSeasons[1024];
  char pclCurrentSeason[32];
  char *pclTmpPtr;
  char *pclTmpPtrN;
  int ilSavDebugLevel;
  int ilLen;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  int ilNextUrno = 0;
  
  long llFieldLength;

  GetQueues();
  /* reading default home-airport from sgs.tab */
  memset(pcgHomeAp,0x00,sizeof(pcgHomeAp));
  ilRc = tool_search_exco_data("SYS","HOMEAP",pcgHomeAp);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_ssihdl : No HOMEAP entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_ssihdl : HOMEAP = <%s>",pcgHomeAp);
      strcpy( cgHopo, pcgHomeAp );      /* Mei */
    }
  /* reading default table-extension from sgs.tab */
  memset(pcgTabEnd,0x00,sizeof(pcgTabEnd));
  ilRc = tool_search_exco_data("ALL","TABEND",pcgTabEnd);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_ssihdl : No TABEND entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_ssihdl : TABEND = <%s>",pcgTabEnd);
      memset(pcgTwEnd,0x00,XS_BUFF);
      sprintf(pcgTwEnd,"%s,%s,%s",pcgHomeAp,pcgTabEnd,mod_name);
      dbg(TRACE,"Init_ssihdl : TW_END = <%s>",pcgTwEnd);
      
      if (strcmp(pcgTabEnd,"TAB") == 0)
    {
      igUseHopo = TRUE;
      dbg(TRACE,"Init_ssihdl: use HOPO-field!");
    }
    }

  ilLen = FLIGHT_RECORD_SIZE_TTB * MAX_NO_TTB_FLIGHTS;
  if ((pcgFlightRecordTTB = (FLIGHT_RECORD_TTB*)malloc((size_t)ilLen)) == NULL)
  {
     dbg(TRACE,"Allocation of pcgFlightRecordTTB failed !!!");
  }
  ilLen = FLIGHT_RECORD_SIZE_SSI * MAX_NO_OF_ARR_FLIGHTS_SSI;
  if ((pcgArrFlightSSI = (FLIGHT_RECORD_SSI*)malloc((size_t)ilLen)) == NULL)
  {
     dbg(TRACE,"Allocation of pcgArrFlightSSI failed !!!");
  }
  ilLen = FLIGHT_RECORD_SIZE_SSI * MAX_NO_OF_DEP_FLIGHTS_SSI;
  if ((pcgDepFlightSSI = (FLIGHT_RECORD_SSI*)malloc((size_t)ilLen)) == NULL)
  {
     dbg(TRACE,"Allocation of pcgDepFlightSSI failed !!!");
  }
  ilLen = FLIGHT_RECORD_SIZE_SSR * MAX_NO_OF_ARR_FLIGHTS_SSR;
  if ((pcgArrFlightSSR = (FLIGHT_RECORD_SSR*)malloc((size_t)ilLen)) == NULL)
  {
     dbg(TRACE,"Allocation of pcgArrFlightSSR failed !!!");
  }
  ilLen = FLIGHT_RECORD_SIZE_SSR * MAX_NO_OF_DEP_FLIGHTS_SSR;
  if ((pcgDepFlightSSR = (FLIGHT_RECORD_SSR*)malloc((size_t)ilLen)) == NULL)
  {
     dbg(TRACE,"Allocation of pcgDepFlightSSR failed !!!"); 
  }
  ilLen = FLIGHT_RECORD_SIZE_SSR * MAX_NO_OF_ROT_FLIGHTS_SSR;
  if ((pcgRotFlightSSR = (FLIGHT_RECORD_SSR*)malloc((size_t)ilLen)) == NULL)
  {
     dbg(TRACE,"Allocation of pcgRotFlightSSR failed !!!");
  }

  //added by MAX, check stya,styp of ssrtab for auh
  if ( (GetFieldLength( "SSR", "STYA", &llFieldLength ) == RC_SUCCESS) &&  (GetFieldLength( "SSR", "STYD", &llFieldLength ) == RC_SUCCESS))
      hasStyad = TRUE;
  else
  {
      dbg ( TRACE,"Init_ssihdl: not find stya,styd in systab" );
      hasStyad = FALSE;
  }
  
  if ( GetFieldLength( "SSI", "STYP", &llFieldLength ) == RC_SUCCESS )
  {
  	dbg ( TRACE,"Init_ssihdl: find styp in systab ,llFieldLength <%d>",llFieldLength );
  	hasStyp = TRUE;
  }
  else
  {
      dbg ( TRACE,"Init_ssihdl: not find styp in systab" );
      hasStyp = FALSE;
  }

  ilRc = GetConfig();

  ilRc = TimeToStr(pcgCurrentTime,time(NULL));

  InitRecordDescriptor(&rgRecDesc,1024,0,FALSE);

  ilRc = InitSsi();

  ilRc = InitSsr();

  return(ilRc);
} /* end of initialize */
/*********************************************************************
Function : GetQueues()
Paramter :
Return Code: RC_SUCCESS,RC_FAIL
Result:
Description: Gets all necessary queue-ID's for CEDA-internal
             communication!
*********************************************************************/
static int GetQueues()
{
  int ilRc = RC_FAIL;

  /* get mod-id of router */
  if ((igModID_Router = tool_get_q_id("router")) == RC_NOT_FOUND ||
      igModID_Router == RC_FAIL || igModID_Router == 0)
    {
      dbg(TRACE,"GetQueues   : tool_get_q_id(router) returns: <%d>",igModID_Router);
      ilRc = RC_FAIL;
    }else{
      dbg(TRACE,"GetQueues   : <router> mod_id <%d>",igModID_Router);
      ilRc = RC_SUCCESS;
    } 
  return ilRc;
}

static int CheckCfg(void)
{
  int ilRc = RC_SUCCESS;
  return ilRc;
}
/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{

  int    ilRc = RC_SUCCESS;    /* Return code */
    
  dbg(TRACE,"Reset: now reseting ...");

  return ilRc;
    
} /* end of Reset */
/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
  dbg(TRACE,"Terminate: now leaving ...");
 
  sleep(ipSleep);
  
  exit(0);
    
} /* end of Terminate */
/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
  switch(pipSig)
    {
    case SIGALRM:
      break;
    case SIGPIPE:
      break;
    case SIGCHLD:
      break;
    case SIGTERM:
      Terminate(1);
      break;
    default    :
      Terminate(10);
      break;
    } /* end of switch */
} /* end of HandleSignal */
/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
  /*    int    ilRc = RC_SUCCESS; */
    return;
} /* end of HandleErr */
/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRc = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
            dbg(TRACE,"<%d> : msgsnd failed",pipErr);
            break;
    case    QUE_E_GET    :    /* Error using msgrcv */
            if(pipErr != 4)        
             dbg(DEBUG,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    : 
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :   unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        /*dbg(TRACE,"<%d> : no messages on queue",pipErr);*/
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    case    QUE_E_PRIORITY    :
        dbg(TRACE,"<%d> : wrong priority was send ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */
/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
  int    ilRc = RC_SUCCESS;            /* Return code */
  int    ilBreakOut = FALSE;
    
  do{
    memset(prgItem,0x00,igItemLen);
    ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
    /* depending on the size of the received item  */
    /* a realloc could be made by the que function */
    /* so do never forget to set event pointer !!! */
    prgEvent = (EVENT *) prgItem->text;    
    if( ilRc == RC_SUCCESS )
      {
    /* Acknowledge the item */
    ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
    if( ilRc != RC_SUCCESS ) 
      {
        /* handle que_ack error */
        HandleQueErr(ilRc);
      } /* fi */
        
    switch( prgEvent->command )
      {
      case    HSB_STANDBY    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_COMING_UP    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_ACTIVE    :
        ctrl_sta = prgEvent->command;
        ilBreakOut = TRUE;
        break;    
      case    HSB_ACT_TO_SBY    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_DOWN    :
        /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
        ctrl_sta = prgEvent->command;
        Terminate(10);
        break;    
      case    HSB_STANDALONE    :
        ctrl_sta = prgEvent->command;
        ResetDBCounter();
        ilBreakOut = TRUE;
        break;    
      case    REMOTE_DB :
        /* ctrl_sta is checked inside */
        HandleRemoteDB(prgEvent);
        break;
      case    SHUTDOWN    :
        Terminate(1);
        break;
      case    RESET        :
        ilRc = Reset();
        break;
      case    EVENT_DATA    :
        dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
        DebugPrintItem(TRACE,prgItem);
        DebugPrintEvent(TRACE,prgEvent);
        break;
      case    TRACE_ON :
        dbg_handle_debug(prgEvent->command);
        break;
      case    TRACE_OFF :
        dbg_handle_debug(prgEvent->command);
        break;
      default            :
        dbg(TRACE,"HandleQueues: unknown event");
        DebugPrintItem(TRACE,prgItem);

        DebugPrintEvent(TRACE,prgEvent);

        break;
      } /* end switch */
      }else{
    /* Handle queuing errors */
    HandleQueErr(ilRc);
      } /* end else */
  } while (ilBreakOut == FALSE);
  if(igInitOK == FALSE)
    {
      ilRc = Init_ssihdl();
      if(ilRc == RC_SUCCESS)
    {
      dbg(TRACE,"HandleQueues: Init_ssihdl() OK!");
      igInitOK = TRUE;
    }else{ /* end of if */
      dbg(TRACE,"HandleQueues: Init_ssihdl() failed!");
      igInitOK = FALSE;
    } /* end of if */
    }/* end of if */
  /* OpenConnection(); */
} /* end of HandleQueues */
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleInternalData()
{
  int  ilRC = RC_SUCCESS;      /* Return code */
  char *pclSelection = NULL;
  char *pclFields = NULL;
  char *pclData = NULL;
  char pclNewData[L_BUFF];
  char pclOldData[L_BUFF];
  char pclUrno[50];
  char *pclTmpPtr=NULL;
  BC_HEAD *bchd = NULL;          /* Broadcast header*/
  CMDBLK  *cmdblk = NULL; 
  char pclTmpInitSsi[32];
  char pclTmpInitSsr[32];
  int ilCount;
  int ilI;
  int ilLen;

  bchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  cmdblk= (CMDBLK  *) ((char *)bchd->data);

  strcpy(pcgTwStart,cmdblk->tw_start);
  strcpy(pcgTwEnd,cmdblk->tw_end);

  /***********************************/
  /*    DebugPrintItem(DEBUG,prgItem);   */
  /*    DebugPrintEvent(DEBUG,prgEvent); */
  /***********************************/
  memset(pclOldData,0x00,L_BUFF);
  pclSelection = cmdblk->data;
  pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
  pclData = (char *)pclFields + strlen(pclFields) + 1;

  strcpy(pclNewData,pclData);
  pclTmpPtr = strstr(pclNewData,"\n");
  if (pclTmpPtr != NULL)
  {
     *pclTmpPtr = '\0';
     pclTmpPtr++;
     strcpy(pclOldData,pclTmpPtr);
  }
  *pclUrno = '\0';
  pclTmpPtr = strstr(pclSelection,"\n");
  if (pclTmpPtr != NULL)
  {
     *pclTmpPtr = '\0';
     pclTmpPtr++;
     strcpy(pclUrno,pclTmpPtr);
  }
  
  dbg(DEBUG,"Command:   <%s>",cmdblk->command);
  dbg(DEBUG,"Table:     <%s>",cmdblk->obj_name);
  dbg(DEBUG,"Selection: <%s><%s>",pclSelection,pclUrno);
  dbg(DEBUG,"Fields:    <%s>",pclFields);
  dbg(DEBUG,"New Data:  <%s>",pclNewData);
  dbg(DEBUG,"Old Data:  <%s>",pclOldData);

  ilRC = TimeToStr(pcgCurrentTime,time(NULL));

  if (strcmp(cmdblk->command,"SSII") == 0)
  {
     strcpy(pclTmpInitSsi,pcgInitSsi);
     strcpy(pcgInitSsi,"YES");
     ilRC = InitSsi();
     strcpy(pcgInitSsi,pclTmpInitSsi);
  }
  
  //MAX 2011/11/23
  if (strcmp(cmdblk->command,"SSIF") == 0)
  {
  	//SSII
     strcpy(pclTmpInitSsi,pcgInitSsi);
     strcpy(pcgInitSsi,"YES");
     ilRC = InitSsi2();
     strcpy(pcgInitSsi,pclTmpInitSsi);
  }

  if (strcmp(cmdblk->command,"SSRI") == 0)
  {
     strcpy(pclTmpInitSsr,pcgInitSsr);
     strcpy(pcgInitSsr,"YES");
     ilRC = InitSsr();
     strcpy(pcgInitSsr,pclTmpInitSsr);
  }
  
  //MAX 2011/11/08
  if (strcmp(cmdblk->command,"SSRF") == 0)
  {
     strcpy(pclTmpInitSsr,pcgInitSsr);
     strcpy(pcgInitSsr,"YES");
     ilRC = InitSsr2();
     strcpy(pcgInitSsr,pclTmpInitSsr);
  }
  
  //added by MAX 2012/13/04
  if (strcmp (cmdblk->command,"SSCG") == 0)
  {
	//Frank 20121102 UFIS-1756
	/*
	strcpy(pclTmpInitSsi,pcgInitSsi);     
	strcpy(pcgInitSsi,"YES");     
	ilRC = InitSsi();     
	strcpy(pcgInitSsi,pclTmpInitSsi);
  	*/
	GenerateSSIMCargoSpot();
  }
  
  

  if (strcmp(cmdblk->command,"SSIU") == 0)
  {
     ilRC = UpdateSSI(pclUrno,pclFields,pclNewData);
     ilRC = UpdateSSR(pclUrno,pclFields,pclNewData);
  }

  if (strcmp(cmdblk->command,"ACCE") == 0)
  {
     ilCount = GetNoOfElements(pclSelection,',');
     for (ilI = 1; ilI <= ilCount; ilI++)
     {
        ilLen = get_real_item(pclUrno,pclSelection,ilI);
        if (ilLen > 0)
        {
           dbg(DEBUG,"Update Flight %d of %d",ilI,ilCount);
           ilRC = UpdateSSI(pclUrno,"","");
           ilRC = UpdateSSR(pclUrno,"","");
        }
     }
  }

  /* Mei */
  if (strcmp(cmdblk->command,"RSTI") == 0)
      ResetSSI();

  if (strcmp(cmdblk->command,"SSIR") == 0)
  {
     que_out  = prgEvent->originator;
     ilRC = HandleData(cmdblk->command,cmdblk->obj_name,pclSelection,pclFields,pclNewData);
  }

  dbg(DEBUG,"========================= START / END =========================");
  return ilRC;
} /* end of HandleInternalData */
/*********************************************************************
Function : SendEvent()
Paramter : IN: pcpCmd = command for cmdblk->command
           IN: ipModID = process-ID where the event is send to
       IN: ipPriority = priority for sending ( 1- 5, usuallay 3)
       IN: pcpTable = Name (3 letters) of cmdblk->obj_name (if
           necessary), will be expanded with "pcgTabEnd".
       IN: pcpTwStart = cmdblk->twstart
       IN: pcpTwEnd = cmdblk->twend (always HOMEAP,TABEND,processname)
       IN: pcpSelection = selection for event (cmdblk->data)
       IN: pcpFields = fieldlist (corresponding to pcpdata)
       IN: pcpData = datalist (comma separated, corresponding to 
                                   pcpFields)
       IN: pcpAddStruct = additional structure to be transmitted
       IN: ipAddStructSize = size of the additional structure
Return Code: RC_SUCCESS, RC_FAIL
Result:
Description: Sends an event to another CEDA-process using que(QUE_PUT).
             Sends the event in standard CEDA-format (BCHEAD,CMDBLK,
         selection,fieldlist,datalist) or sends a different
         data structure (special) at CMDBLK->data. !! Sends always
         only one type, standard OR special, with one event !!
*********************************************************************/
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
             char *pcpTwStart, char *pcpTwEnd,
             char *pcpSelection,char *pcpFields,char *pcpData,
             char *pcpAddStruct,int ipAddStructSize)
{
  int     ilRc             = RC_FAIL;
  int     ilLen            = 0;
  EVENT   *prlOutEvent  = NULL;
  BC_HEAD *prlOutBCHead = NULL;
  CMDBLK  *prlOutCmdblk = NULL;

  if (pcpAddStruct == NULL)
    ipAddStructSize = 0;
  if (ipAddStructSize == 0)
    pcpAddStruct = NULL;

  /* size-calculation for prlOutEvent */
  ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
    strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 
    ipAddStructSize + 128;

  /* memory for prlOutEvent */
  if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
    {
      dbg(TRACE,"SendEvent: cannot malloc <%d>-bytes for outgoing event!",ilLen);
      prlOutEvent = NULL;
    }else{
      /* clear whole outgoing event */
      memset((void*)prlOutEvent, 0x00, ilLen);

      /* set event structure... */
      prlOutEvent->type         = SYS_EVENT;
      prlOutEvent->command    = EVENT_DATA;
      prlOutEvent->originator   = (short)mod_id;
      prlOutEvent->retry_count  = 0;
      prlOutEvent->data_offset  = sizeof(EVENT);
      prlOutEvent->data_length  = ilLen - sizeof(EVENT); 

      /* BC_HEAD-Structure... */
      prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
      /* prlOutBCHead->rc = (short)RC_SUCCESS;*/
      prlOutBCHead->rc = (short)NETOUT_NO_ACK;/*spaeter nur bei disconnect*/
      strncpy(prlOutBCHead->dest_name,mod_name,10);
      strncpy(prlOutBCHead->recv_name, "EXCO",10);
 
      /* Cmdblk-Structure... */
      prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
      strcpy(prlOutCmdblk->command,pcpCmd);
      if (pcpTable != NULL)
    {
      strcpy(prlOutCmdblk->obj_name,pcpTable);
      strcat(prlOutCmdblk->obj_name,pcgTabEnd);
    }
        
      /* setting tw_x entries */
      strncpy(prlOutCmdblk->tw_start,pcpTwStart,32);
      strncpy(prlOutCmdblk->tw_end,pcpTwEnd,32);
        
      /* means that no additional structure is used */
      /* STANDARD CEDA-ipcs between CEDA-processes */
      if (pcpAddStruct == NULL)
    {
      /* setting selection inside event */
      strcpy(prlOutCmdblk->data,pcpSelection);
      /* setting field-list inside event */
      strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1,pcpFields);
      /* setting data-list inside event */
      strcpy((prlOutCmdblk->data + (strlen(pcpSelection)+1) + (strlen(pcpFields)+1)),pcpData);
    }else{
      /*an additional structure is used and will be copied to */
      /*cmdblk + sizeof(CMDBLK).!!! No STANDARD CEDA-ipcs is used !!!! */
      memcpy(prlOutCmdblk->data,(char*)pcpAddStruct,ipAddStructSize);    
    }

      /*DebugPrintEvent(DEBUG,prlOutEvent);*/ 
      /*snapit((char*)prlOutEvent,ilLen,outp);*/
      dbg(DEBUG,"SendEvent: sending event to mod_id <%d>",ipModID);

      if (ipModID != 0)
    {
      if ((ilRc = que(QUE_PUT,ipModID,mod_id,ipPriority,ilLen,(char*)prlOutEvent))
          != RC_SUCCESS)
        {
          dbg(TRACE,"SendEvent: QUE_PUT returns: <%d>", ilRc);
          Terminate(1);
        }
    }else{
      dbg(TRACE,"SendEvent: mod_id = <%d>! Can't send!",ipModID);
    }
      /* free memory */
      free((void*)prlOutEvent); 
    }
  return ilRc;
}

/*
    Get Config Entries
*/
static int GetConfig()
{
  int ilRC = RC_SUCCESS;
  int ilI;

  sprintf(pcgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
  dbg(TRACE,"Config File is <%s>",pcgConfigFile);

  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","INIT_SSI",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgInitSsi,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","INIT_SSR",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgInitSsr,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","HANDLE_SSI",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pcgCfgBuffer,"NO") == 0)
     {
        igHandleSSI = FALSE;
     }
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","EXPORT_WITH_TTYP_OR_STYP",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pcgCfgBuffer,"STYP") == 0)
     {
        myEXPORT_WITH_TTYP_OR_STYP = TRUE;
        dbg(TRACE,"GetConfig: EXPORT_WITH_TTYP_OR_STYP <%s> export with STYP",pcgCfgBuffer);
     }
     else if (strcmp(pcgCfgBuffer,"TTYP") == 0)
     {
  	    myEXPORT_WITH_TTYP_OR_STYP = FALSE;
        dbg(TRACE,"GetConfig: EXPORT_WITH_TTYP_OR_STYP <%s> export with TTYP",pcgCfgBuffer);
     }
     else
     	{
     		myEXPORT_WITH_TTYP_OR_STYP = FALSE;
     		dbg(TRACE,"GetConfig: EXPORT_WITH_TTYP_OR_STYP <%s> unknown entry value ,use default export with TTYP",pcgCfgBuffer);
     	}
  }
  else
  {
  	myEXPORT_WITH_TTYP_OR_STYP = FALSE;
    dbg(TRACE,"GetConfig: EXPORT_WITH_TTYP_OR_STYP not found, export with default TTYP ");
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","HANDLE_SSR",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pcgCfgBuffer,"NO") == 0)
     {
        igHandleSSR = FALSE;
     }
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","SEASONS_FOR_INIT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgSeasonsForInit,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","SEASONS_NOT_FOR_UPDATE",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgSeasonsNotForUpdate,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","VALID_FTYP",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     memset(pcgValidFtyp,0x00,512);
     for (ilI = 0; ilI < strlen(pcgCfgBuffer); ilI += 2)
     {
        strcat(pcgValidFtyp,"'");
        pcgValidFtyp[strlen(pcgValidFtyp)] = pcgCfgBuffer[ilI];
        strcat(pcgValidFtyp,"',");
     }
     pcgValidFtyp[strlen(pcgValidFtyp)-1] = '\0';
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","SSIM_FILE_PATH",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgSSIMFilePath,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","SSIM_FILE_NAME",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgSSIMFileName,pcgCfgBuffer);
  }
  
  //added by MAX
  ilRC = iGetConfigEntry(pcgConfigFile,"CARGOSPOR","CARGOSPOR_SSIM_FILE_NAME",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
  	 memset(myCargoSSIMFileName,0x00,128);
     strcpy(myCargoSSIMFileName,pcgCfgBuffer);
     dbg(TRACE,"GetConfig: CARGOSPOR_SSIM_FILE_NAME <%s>",myCargoSSIMFileName);
  }
  else
  	 dbg(TRACE,"GetConfig: CARGOSPOR_SSIM_FILE_NAME not found ,use default <%s>",myCargoSSIMFileName);
  ilRC = iGetConfigEntry(pcgConfigFile,"CARGOSPOR","CARGOSPOR_SSIM_FILE_PATH",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
  	 memset(myCargoSSIMFilePath,0x00,128);
     strcpy(myCargoSSIMFilePath,pcgCfgBuffer);
     dbg(TRACE,"GetConfig: CARGOSPOR_SSIM_FILE_PATH <%s>",myCargoSSIMFilePath);
  }
  else
  	 dbg(TRACE,"GetConfig: CARGOSPOR_SSIM_FILE_PATH not found ,use default <%s>",myCargoSSIMFilePath);
  ilRC = iGetConfigEntry(pcgConfigFile,"CARGOSPOR","CARGOSPOR_SSIM_FILE_PATH_BAK",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
  	 memset(myCargoSSIMFilePathBak,0x00,128);
     strcpy(myCargoSSIMFilePathBak,pcgCfgBuffer);
     dbg(TRACE,"GetConfig: CARGOSPOR_SSIM_FILE_PATH_BAK <%s>",myCargoSSIMFilePathBak);
  }
  else
  	 dbg(TRACE,"GetConfig: CARGOSPOR_SSIM_FILE_PATH_BAK not found ,use default <%s>",myCargoSSIMFilePathBak);
  ilRC = iGetConfigEntry(pcgConfigFile,"CARGOSPOR","CARGOSPOR_SSIM_FILE_EXT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
  	 memset(myCargoSSIMFileExt,0x00,32);
     strcpy(myCargoSSIMFileExt,pcgCfgBuffer);
     dbg(TRACE,"GetConfig: CARGOSPOR_SSIM_FILE_EXT <%s>",myCargoSSIMFileExt);
  }
  else
  	dbg(TRACE,"GetConfig: CARGOSPOR_SSIM_FILE_EXT not found ,use default <%s>",myCargoSSIMFileExt);
  ilRC = iGetConfigEntry(pcgConfigFile,"CARGOSPOR","CARGOSPOR_SSIM_FILE_TIME_PERIOD",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     myCargoSSIMTimePeriod = atoi(pcgCfgBuffer);
     dbg(TRACE,"GetConfig: CARGOSPOR_SSIM_FILE_TIME_PERIOD <%d> months",myCargoSSIMTimePeriod);
  }
  else
  	dbg(TRACE,"GetConfig: CARGOSPOR_SSIM_FILE_TIME_PERIOD not found ,use default <%d> months",myCargoSSIMTimePeriod);
  
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","SSIM_FILE_EXT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgSSIMFileExt,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","SSIM_VERSION",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgSSIMVersion,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","SSIM_REFERENCE",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgSSIMReference,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","SSIM_LOCAL_TIMES",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pcgCfgBuffer,"YES") == 0)
     {
        igSSIMLocalTimes = TRUE;
     }
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","TTB_FILE_PATH",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgTTBFilePath,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","TTB_FILE_NAME",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  { 
     strcpy(pcgTTBFileName,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","TTB_FILE_EXT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgTTBFileExt,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","FLUKO_FILE_PATH",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgFLUKOFilePath,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","FLUKO_FILE_NAME",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgFLUKOFileName,pcgCfgBuffer);
  }
  
  //Frank 20121107 v3.4
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","MONTH_RANGE_FOR_SEASON",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igMonthRangeForSeason = atoi(pcgCfgBuffer);
     dbg(TRACE,"GetConfig: igMonthRangeForSeason<%d>",igMonthRangeForSeason);
  }
  else
  {
  	dbg(TRACE,"GetConfig: MONTH_RANGE_FOR_SEASON not found ,use default <%d>",igMonthRangeForSeason);
  }

  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","FLUKO_FILE_EXT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgFLUKOFileExt,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","SLOTCO_FILE_PATH",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgSLOTCOFilePath,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","SLOTCO_FILE_NAME",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgSLOTCOFileName,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","SLOTCO_FILE_EXT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgSLOTCOFileExt,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","SLOTCO_HEADER",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgSLOTCOHeader,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","TRIGGER_ACTION",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgTriggerAction,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","TRIGGER_BCHDL",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgTriggerBchdl,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","DEBUG_LEVEL",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgDebugLevel,pcgCfgBuffer);
  }
  if (strcmp(pcgDebugLevel,"DEBUG") == 0)
  {
     debug_level = DEBUG;
  }
  else
  {
     if (strcmp(pcgDebugLevel,"TRACE") == 0)
     {
        debug_level = TRACE;
     }
     else
     {
        if (strcmp(pcgDebugLevel,"NULL") == 0)
        {
           debug_level = 0;
        }
        else
        {
           debug_level = TRACE;
        }
     }
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","DEBUG_LEVEL_INIT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgDebugLevelInit,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"SSIHDL","USE_ARRAY_INSERT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pcgCfgBuffer,"YES") == 0)
        igUseArrayInsert = TRUE;
  }

  dbg(TRACE,"INIT_SSI = %s",pcgInitSsi);
  dbg(TRACE,"INIT_SSR = %s",pcgInitSsr);
  dbg(TRACE,"HANDLE_SSI = %d",igHandleSSI);
  dbg(TRACE,"HANDLE_SSR = %d",igHandleSSR);
  dbg(TRACE,"SEASONS_FOR_INIT = %s",pcgSeasonsForInit);
  dbg(TRACE,"SEASONS_NOT_FOR_UPDATE = %s",pcgSeasonsNotForUpdate);
  dbg(TRACE,"VALID_FTYP = %s",pcgValidFtyp);
  dbg(TRACE,"SSIM_FILE_PATH = %s",pcgSSIMFilePath);
  dbg(TRACE,"SSIM_FILE_NAME = %s",pcgSSIMFileName);
  dbg(TRACE,"SSIM_FILE_EXT = %s",pcgSSIMFileExt);
  dbg(TRACE,"SSIM_VERSION = %s",pcgSSIMVersion);
  dbg(TRACE,"SSIM_REFERENCE = %s",pcgSSIMReference);
  dbg(TRACE,"SSIM_LOCAL_TIMES = %d",igSSIMLocalTimes);
  dbg(TRACE,"TTB_FILE_PATH = %s",pcgTTBFilePath);
  dbg(TRACE,"TTB_FILE_NAME = %s",pcgTTBFileName);
  dbg(TRACE,"TTB_FILE_EXT = %s",pcgTTBFileExt);
  dbg(TRACE,"FLUKO_FILE_PATH = %s",pcgFLUKOFilePath);
  dbg(TRACE,"FLUKO_FILE_NAME = %s",pcgFLUKOFileName);
  dbg(TRACE,"FLUKO_FILE_EXT = %s",pcgFLUKOFileExt);
  dbg(TRACE,"SLOTCO_FILE_PATH = %s",pcgSLOTCOFilePath);
  dbg(TRACE,"SLOTCO_FILE_NAME = %s",pcgSLOTCOFileName);
  dbg(TRACE,"SLOTCO_FILE_EXT = %s",pcgSLOTCOFileExt);
  dbg(TRACE,"TRIGGER_ACTION = %s",pcgTriggerAction);
  dbg(TRACE,"TRIGGER_BCHDL = %s",pcgTriggerBchdl);
  dbg(TRACE,"DEBUG_LEVEL = %s",pcgDebugLevel);
  dbg(TRACE,"DEBUG_LEVEL_INIT = %s",pcgDebugLevelInit);
  dbg(TRACE,"USE_ARRAY_INSERT = %d",igUseArrayInsert);

  return RC_SUCCESS;
}

/******************************************************************************/
/* The TimeToStr routine                                                      */
/******************************************************************************/
static int TimeToStr(char *pcpTime,time_t lpTime) 
{                                                                              
  struct tm *_tm;
    
  _tm = (struct tm *)gmtime(&lpTime);

  sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
          _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
          _tm->tm_min,_tm->tm_sec);

  return RC_SUCCESS;  
                        
}     /* end of TimeToStr */

static int InitSsi2()
{
  int ilRC = RC_SUCCESS;
  int ilSavDebugLevel;
  int ilTruncateTable = TRUE;   /* Mei */
  char pclSeasons[1024];
  char pclCurrentSeason[32];
  char *pclTmpPtr;
  char *pclTmpPtrN;
  char pclCurrentTimeEnd[32];
  
  char mySeasons[100]="\0";
  char myCurrentTimeAddTenMonths[100]="\0";
  char myCurrentTimeMinTenMonths[100]="\0";
  char mySqlBuf[L_BUFF];
  char myDataBuf[L_BUFF];
  short slFkt;
  short slCursor;
  int ilRCdb = DB_SUCCESS;

  dbg(TRACE,"================ Now Start Initializing SSITAB ================");
  if (strcmp(pcgInitSsi,"YES") == 0)
  {
     ilSavDebugLevel = debug_level;
     if (strcmp(pcgDebugLevelInit,"DEBUG") == 0)
     {
        debug_level = DEBUG;
     }
     else
     {
        debug_level = TRACE;
     }
     ilRC = TimeToStr(pcgCurrentTime,time(NULL));
     dbg(DEBUG,"Current Time = <%s>",pcgCurrentTime);

     //get current season,before season, next season
     //TimeToStr(myCurrentTimeAddTenMonths,time(NULL) + 10*30*24*60*60);
     //TimeToStr(myCurrentTimeMinTenMonths,time(NULL) - 10*30*24*60*60);
     //Frank 20121107 v3.4
     TimeToStr(myCurrentTimeAddTenMonths,time(NULL) + igMonthRangeForSeason*30*24*60*60);
     TimeToStr(myCurrentTimeMinTenMonths,time(NULL) - igMonthRangeForSeason*30*24*60*60);
     
     //dbg(DEBUG,"InitSsi2: Ten months before = <%s>",myCurrentTimeMinTenMonths);
     //dbg(DEBUG,"InitSsi2: Ten months after = <%s>",myCurrentTimeAddTenMonths);
     //Frank 20121107 v3.4
     dbg(DEBUG,"InitSsi2: %d months before = <%s>",igMonthRangeForSeason,myCurrentTimeMinTenMonths);
     dbg(DEBUG,"InitSsi2: %d months after = <%s>",igMonthRangeForSeason,myCurrentTimeAddTenMonths);
     
     sprintf(mySqlBuf,"SELECT SEAS FROM SEATAB WHERE (VPFR > %s AND VPTO < %s)",
     myCurrentTimeMinTenMonths,myCurrentTimeAddTenMonths);
     
     dbg(DEBUG,"mySqlBuf<%s>",mySqlBuf);
     
     slCursor = 0;
     slFkt = START;
     /* dbg(TRACE,"<%s>",pclSqlBuf); */
     ilRCdb = sql_if(slFkt,&slCursor,mySqlBuf,myDataBuf);
     ilTruncateTable = TRUE;   /* Mei */
     while (ilRCdb == DB_SUCCESS)
     {
        //BuildItemBuffer(myDataBuf,"",1,",");
        dbg(DEBUG,"InitSsi2: Season <%s>",myDataBuf);
        ilRC = GenerateSSI(myDataBuf, ilTruncateTable);
        ilTruncateTable = FALSE;
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,mySqlBuf,myDataBuf);
     }
     close_my_cursor(&slCursor);

     dbg(TRACE," ");
     dbg(TRACE,"Start: %s",pcgCurrentTime);
     ilRC = TimeToStr(pclCurrentTimeEnd,time(NULL));
     dbg(TRACE,"End:   %s",pclCurrentTimeEnd);
     debug_level = ilSavDebugLevel;
  }

  dbg(TRACE,"================ Initializing SSITAB Finished =================");

  return RC_SUCCESS;
}

static int InitSsi()
{
  int ilRC = RC_SUCCESS;
  int ilSavDebugLevel;
  int ilTruncateTable = TRUE;   /* Mei */
  char pclSeasons[1024];
  char pclCurrentSeason[32];
  char *pclTmpPtr;
  char *pclTmpPtrN;
  char pclCurrentTimeEnd[32];

  dbg(TRACE,"================ Now Start Initializing SSITAB ================");
  if (strcmp(pcgInitSsi,"YES") == 0)
  {
     ilSavDebugLevel = debug_level;
     if (strcmp(pcgDebugLevelInit,"DEBUG") == 0)
     {
        debug_level = DEBUG;
     }
     else
     {
        debug_level = TRACE;
     }
     ilRC = TimeToStr(pcgCurrentTime,time(NULL));
     dbg(DEBUG,"Current Time = <%s>",pcgCurrentTime);

     strcpy(pclSeasons,pcgSeasonsForInit);
     pclTmpPtr = pclSeasons;
     ilTruncateTable = TRUE;
     while (strlen(pclTmpPtr) > 0)
     {
        pclTmpPtrN = strstr(pclTmpPtr,",");
        if (pclTmpPtrN == NULL)
        {
           strcpy(pclCurrentSeason,pclTmpPtr);
           pclTmpPtr += strlen(pclTmpPtr);
        }
        else
        {
           *pclTmpPtrN = '\0';
           strcpy(pclCurrentSeason,pclTmpPtr);
           pclTmpPtr = pclTmpPtrN + 1;
        }
        ilRC = GenerateSSI(pclCurrentSeason, ilTruncateTable);
        ilTruncateTable = FALSE;
     }

     dbg(TRACE," ");
     dbg(TRACE,"Start: %s",pcgCurrentTime);
     ilRC = TimeToStr(pclCurrentTimeEnd,time(NULL));
     dbg(TRACE,"End:   %s",pclCurrentTimeEnd);
     debug_level = ilSavDebugLevel;
  }

  dbg(TRACE,"================ Initializing SSITAB Finished =================");

  return RC_SUCCESS;
}

//added by MAX
static int InitSsr2()
{
  int ilRC = RC_SUCCESS;
  int ilSavDebugLevel;
  int ilTruncateTable = TRUE;   /* Mei */
  char pclSeasons[1024];
  char pclCurrentSeason[32];
  char *pclTmpPtr;
  char *pclTmpPtrN;
  char pclCurrentTimeEnd[32];
  
  char mySeasons[100]="\0";
  char myCurrentTimeAddTenMonths[100]="\0";
  char myCurrentTimeMinTenMonths[100]="\0";
  char mySqlBuf[L_BUFF];
  char myDataBuf[L_BUFF];
  short slFkt;
  short slCursor;
  int ilRCdb = DB_SUCCESS;


  dbg(TRACE,"================ Now Start InitSsr2:Initializing SSRTAB ================");
     ilSavDebugLevel = debug_level;
     if (strcmp(pcgDebugLevelInit,"DEBUG") == 0)
     {
        debug_level = DEBUG;
     }
     else
     {
        debug_level = TRACE;
     }
     ilRC = TimeToStr(pcgCurrentTime,time(NULL));
     dbg(DEBUG,"InitSsr2: Current Time = <%s>",pcgCurrentTime);
     
     //get current season,before season, next season
     //Frank 20121107 v3.4
     //TimeToStr(myCurrentTimeAddTenMonths,time(NULL) + 10*30*24*60*60);
     //TimeToStr(myCurrentTimeMinTenMonths,time(NULL) - 10*30*24*60*60);
     //dbg(DEBUG,"InitSsr2: Ten months before = <%s>",myCurrentTimeMinTenMonths);
     //dbg(DEBUG,"InitSsr2: Ten months after = <%s>",myCurrentTimeAddTenMonths);
     
     //Frank 20121107 v3.4
     TimeToStr(myCurrentTimeAddTenMonths,time(NULL) + igMonthRangeForSeason*30*24*60*60);
     TimeToStr(myCurrentTimeMinTenMonths,time(NULL) - igMonthRangeForSeason*30*24*60*60);
     dbg(DEBUG,"InitSsi2: %d months before = <%s>",igMonthRangeForSeason,myCurrentTimeMinTenMonths);
     dbg(DEBUG,"InitSsi2: %d months after = <%s>",igMonthRangeForSeason,myCurrentTimeAddTenMonths);
     
     sprintf(mySqlBuf,"SELECT SEAS FROM SEATAB WHERE (VPFR > %s AND VPTO < %s)",
     myCurrentTimeMinTenMonths,myCurrentTimeAddTenMonths);
     slCursor = 0;
     slFkt = START;
     /* dbg(TRACE,"<%s>",pclSqlBuf); */
     ilRCdb = sql_if(slFkt,&slCursor,mySqlBuf,myDataBuf);
     ilTruncateTable = TRUE;   /* Mei */
     while (ilRCdb == DB_SUCCESS)
     {
        //BuildItemBuffer(myDataBuf,"",1,",");
        dbg(DEBUG,"InitSsr2: Season <%s>",myDataBuf);
        ilRC = GenerateSSR(myDataBuf, ilTruncateTable);
        ilTruncateTable = FALSE;
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,mySqlBuf,myDataBuf);
     }
     close_my_cursor(&slCursor);

     dbg(TRACE," ");
     dbg(TRACE,"Start: %s",pcgCurrentTime);
     ilRC = TimeToStr(pclCurrentTimeEnd,time(NULL));
     dbg(TRACE,"End:   %s",pclCurrentTimeEnd);
     debug_level = ilSavDebugLevel;

  dbg(TRACE,"================ Initializing InitSsr2:SSRTAB Finished =================");

  return RC_SUCCESS;
}

static int InitSsr()
{
  int ilRC = RC_SUCCESS;
  int ilSavDebugLevel;
  int ilTruncateTable = TRUE;   /* Mei */
  char pclSeasons[1024];
  char pclCurrentSeason[32];
  char *pclTmpPtr;
  char *pclTmpPtrN;
  char pclCurrentTimeEnd[32];

  dbg(TRACE,"================ Now Start Initializing SSRTAB ================");
  if (strcmp(pcgInitSsr,"YES") == 0)
  {
     ilSavDebugLevel = debug_level;
     if (strcmp(pcgDebugLevelInit,"DEBUG") == 0)
     {
        debug_level = DEBUG;
     }
     else
     {
        debug_level = TRACE;
     }
     ilRC = TimeToStr(pcgCurrentTime,time(NULL));
     dbg(DEBUG,"Current Time = <%s>",pcgCurrentTime);

     strcpy(pclSeasons,pcgSeasonsForInit);
     pclTmpPtr = pclSeasons;
     ilTruncateTable = TRUE;   /* Mei */
     while (strlen(pclTmpPtr) > 0)
     {
        pclTmpPtrN = strstr(pclTmpPtr,",");
        if (pclTmpPtrN == NULL)
        {
           strcpy(pclCurrentSeason,pclTmpPtr);
           pclTmpPtr += strlen(pclTmpPtr);
        }
        else
        {
           *pclTmpPtrN = '\0';
           strcpy(pclCurrentSeason,pclTmpPtr);
           pclTmpPtr = pclTmpPtrN + 1;
        }
        ilRC = GenerateSSR(pclCurrentSeason, ilTruncateTable);
        ilTruncateTable = FALSE;

     }

     dbg(TRACE," ");
     dbg(TRACE,"Start: %s",pcgCurrentTime);
     ilRC = TimeToStr(pclCurrentTimeEnd,time(NULL));
     dbg(TRACE,"End:   %s",pclCurrentTimeEnd);
     debug_level = ilSavDebugLevel;
  }

  dbg(TRACE,"================ Initializing SSRTAB Finished =================");

  return RC_SUCCESS;
}


static int TriggerBchdlAction(char *pcpCmd, char *pcpTable, char *pcpSelection,
                              char *pcpFields, char *pcpData, int ipBchdl, int ipAction)
{
  int ilRC = RC_SUCCESS;

  if (ipBchdl == TRUE && strcmp(pcgTriggerBchdl,"YES") == 0)
  {
     (void) tools_send_info_flag(1900,0,"ssihdl","","CEDA","","",pcgTwStart,pcgTwEnd,
                                 pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData,0);
     dbg(DEBUG,"Send Broadcast");
  }
  if (ipBchdl == TRUE && strcmp(pcgTriggerAction,"YES") == 0)
  {
     (void) tools_send_info_flag(7400,0,"ssihdl","","CEDA","","",pcgTwStart,pcgTwEnd,
                                 pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData,0);
     dbg(DEBUG,"Send to ACTION");
  }

  return ilRC;
}


static int GetMyFullDay(char *pclDateTime, int *ipDays, int *ipMins, int *ipWeekDay,
                        int *ipWeek)
{
  int ilRC = RC_SUCCESS;

  ilRC = GetFullDay(pclDateTime,ipDays,ipMins,ipWeekDay);
  *ipWeek = (*ipDays - igFirstDay) / 7;

  return ilRC;
} /* End of GetMyFullDay */


static void TrimRight(char *pcpBuffer)
{
  char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];

  if (strlen(pcpBuffer) == 0)
  {
     strcpy(pcpBuffer," ");
  }
  else
  {
     while (isspace(*pclBlank) && pclBlank != pcpBuffer)
     {
        *pclBlank = '\0';
        pclBlank--;
     }
  }
} /* End of TrimRight */


static int GenerateSSI(char *pcpCurrentSeason, int ipTruncateTable)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  int ilLen;
  char pclFirstDay[32];
  char pclLastDay[32];
  char pclCurFlno[32];
  int ilDays;
  int ilMins;
  int ilWeekDay;

  dbg(TRACE," ");
  dbg(TRACE,"Generate SSITAB for season %s",pcpCurrentSeason);

  if( ipTruncateTable == TRUE )
  {
      sprintf(pclSqlBuf,"TRUNCATE TABLE SSITAB");
/*      igNextUrno = 1; */
  }
  else
      sprintf(pclSqlBuf,"DELETE FROM SSITAB WHERE SEAS = '%s'", pcpCurrentSeason );
  dbg(TRACE,"<%s>",pclSqlBuf);

  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  commit_work();
  close_my_cursor(&slCursor);

  if (igHandleSSI == FALSE)
  {
     dbg(TRACE,"SSITAB is not supported!!!");
     return ilRC;
  }
  sprintf(pclSqlBuf,"SELECT VPFR,VPTO FROM SEATAB WHERE SEAS = '%s'",pcpCurrentSeason);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);
  if (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",2,",");
     ilLen = get_real_item(pclFirstDay,pclDataBuf,1);
     ilLen = get_real_item(pclLastDay,pclDataBuf,2);
     ilRC = GetFullDay(pclFirstDay,&ilDays,&ilMins,&ilWeekDay);
     igFirstDay = ilDays - ilWeekDay + 1;
#if 1
     pclLastDay[8] = '\0';
     strcat(pclLastDay,"235959");
     /* Lin */
     /* Just select all FLNO first, it will be much faster */
     strcpy(pclSqlBuf,"SELECT DISTINCT FLNO FROM AFTTAB ");
     sprintf(pclSelectBuf,"WHERE FLNO IS NOT NULL ");
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     /* dbg(TRACE,"<%s>",pclSqlBuf); */
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     dbg(TRACE,"All FLNOs read");
     while (ilRCdb == DB_SUCCESS)
     {
        strcpy(pclCurFlno,pclDataBuf);
        TrimRight(pclCurFlno);
        if (strlen(pclCurFlno) > 3)
        {
           ilRC = GenerateFlightSSI(pcpCurrentSeason,pclCurFlno,pclFirstDay,pclLastDay,
                                    TRUE);
        }
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     }
     close_my_cursor(&slCursor);
#else
ilRC = GenerateFlightSSI(pcpCurrentSeason,"LH 600",pclFirstDay,pclLastDay,
TRUE);
#endif
  }

  return ilRC;
} /* End of GenerateSSI */


static int GenerateSSR(char *pcpCurrentSeason, int ipTruncateTable)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  int ilLen;
  char pclFirstDay[32];
  char pclLastDay[32];
  char pclCurFlno[32];
  int ilDays;
  int ilMins;
  int ilWeekDay;

  dbg(TRACE," ");
  dbg(TRACE,"Generate SSRTAB for season %s",pcpCurrentSeason);

  if( ipTruncateTable == TRUE )
  {
      sprintf(pclSqlBuf,"TRUNCATE TABLE SSRTAB");
/*      igNextUrno = 1; */
  }
  else
      sprintf(pclSqlBuf,"DELETE FROM SSRTAB WHERE SEAS = '%s'", pcpCurrentSeason );

  dbg(TRACE,"<%s>",pclSqlBuf);
  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  commit_work();
  close_my_cursor(&slCursor);
  
  if (igHandleSSR == FALSE)
  {
     dbg(TRACE,"SSRTAB is not supported!!!");
     return ilRC;
  }
  
  sprintf(pclSqlBuf,"SELECT VPFR,VPTO FROM SEATAB WHERE SEAS = '%s'",pcpCurrentSeason);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);
  if (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",2,",");
     ilLen = get_real_item(pclFirstDay,pclDataBuf,1);
     ilLen = get_real_item(pclLastDay,pclDataBuf,2);
     ilRC = GetFullDay(pclFirstDay,&ilDays,&ilMins,&ilWeekDay);
     igFirstDay = ilDays - ilWeekDay + 1;
#if 1
     pclLastDay[8] = '\0';
     strcat(pclLastDay,"235959");
     /* Lin */
     /* Just select all FLNO first, it will be much faster */
     strcpy(pclSqlBuf,"SELECT DISTINCT FLNO FROM AFTTAB ");
     sprintf(pclSelectBuf,"WHERE FLNO IS NOT NULL ");
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     /* dbg(TRACE,"<%s>",pclSqlBuf); */
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     dbg(TRACE,"All FLNOs read");
     while (ilRCdb == DB_SUCCESS)
     {
        strcpy(pclCurFlno,pclDataBuf);
        TrimRight(pclCurFlno);
        if (strlen(pclCurFlno) > 3)
        {
           ilRC = GenerateFlightSSR(pcpCurrentSeason,pclCurFlno,pclFirstDay,
                                    pclLastDay,"I",NULL,TRUE);
        }
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     }
     close_my_cursor(&slCursor);
#else
ilRC = GenerateFlightSSR(pcpCurrentSeason,"LH 600",pclFirstDay,pclLastDay,
                         "I",NULL,TRUE);
#endif
  }

  return ilRC;
} /* End of GenerateSSR */


static int GenerateFlightSSI(char *pcpCurrentSeason, char *pcpCurFlno, char *pcpFirstDay,
                             char *pcpLastDay, int ipCommit)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  int ilArrCount = 0;
  int ilDepCount = 0;
  char pclArrRec[MAX_NO_OF_ARR_FLIGHTS_SSI][1500];
  char pclDepRec[MAX_NO_OF_DEP_FLIGHTS_SSI][1500];
  int ilI;
  FLIGHT_RECORD_SSI *pclFlightBegin;
  FLIGHT_RECORD_SSI *pclFlightEnd;
  int ilCount;
  char pclSchedTimeBegin[32];
  char pclSchedTimeEnd[32];
  char pclSchedTimeBegin2[32];
  char pclSchedTimeEnd2[32];

  dbg(DEBUG,"Generate SSI for flight <%s>",pcpCurFlno);

  igArrayPtr = 0;
  strcpy(pcgSsiBuf,"");

  /* Lin */
  /* Force to use correct index  */
  strcpy(pclSqlBuf,"SELECT /*+ index(afttab AFTTAB_FLNO) */ TTYP,ACT3,ACT5,ALC2,ALC3,FLTN,FLNS,JCNT,JFNO,STOA,STOD,ORG3,ORG4,DES3,DES4,VIA3,VIA4,VIAN,VIAL,ADID,URNO,STYP FROM AFTTAB ");
  sprintf(pclSelectBuf,"WHERE STOA BETWEEN '%s' AND '%s' AND FLNO = '%s' AND DES3 = '%s' AND ADID = 'A' AND FTYP IN (%s) ORDER BY TTYP,ACT3,JFNO,ORG3,VIA3,SUBSTR(STOD,9,6),SUBSTR(STOA,9,6),STOA",pcpFirstDay,pcpLastDay,pcpCurFlno,pcgHomeAp,pcgValidFtyp);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  dbg(TRACE,"<%s>",pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",22,",");
     strcpy(&pclArrRec[ilArrCount][0],pclDataBuf);
     ilArrCount++;
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);

  /* Lin */
  /* Force to use correct index  */
  strcpy(pclSqlBuf,"SELECT /*+ index(afttab AFTTAB_FLNO) */ TTYP,ACT3,ACT5,ALC2,ALC3,FLTN,FLNS,JCNT,JFNO,STOA,STOD,ORG3,ORG4,DES3,DES4,VIA3,VIA4,VIAN,VIAL,ADID,URNO,STYP FROM AFTTAB ");
  sprintf(pclSelectBuf,"WHERE STOD BETWEEN '%s' AND '%s' AND FLNO = '%s' AND ORG3 = '%s' AND ADID = 'D' AND FTYP IN (%s) ORDER BY TTYP,ACT3,JFNO,DES3,VIA3,SUBSTR(STOA,9,6),SUBSTR(STOD,9,6),STOD",pcpFirstDay,pcpLastDay,pcpCurFlno,pcgHomeAp,pcgValidFtyp);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  dbg(TRACE,"<%s>",pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",22,",");
     strcpy(&pclDepRec[ilDepCount][0],pclDataBuf);
     ilDepCount++;
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);

  if (ilArrCount > 0)
  {
     for (ilI=0; ilI<ilArrCount; ilI++)
     {
        /*dbg(TRACE,"<%s>",&pclArrRec[ilI][0]);*/
        ilRC = GetFlightDataSSI(&pcgArrFlightSSI[ilI],&pclArrRec[ilI][0]);
     }
     pclFlightBegin = pcgArrFlightSSI;
     pclFlightEnd = pcgArrFlightSSI;
     pclFlightEnd++;
     ilCount = ilArrCount;
     ilI = 1;
     while (ilCount > 0)
     {
        strcpy(pclSchedTimeBegin,&pclFlightBegin->pclStoa[8]);
        strcpy(pclSchedTimeEnd,&pclFlightEnd->pclStoa[8]);
        strcpy(pclSchedTimeBegin2,&pclFlightBegin->pclStod[8]);
        strcpy(pclSchedTimeEnd2,&pclFlightEnd->pclStod[8]);
        while (ilCount > 1 &&
               strcmp(pclFlightBegin->pclTtyp,pclFlightEnd->pclTtyp) == 0 &&
               strcmp(pclFlightBegin->pclAct3,pclFlightEnd->pclAct3) == 0 &&
               strcmp(pclFlightBegin->pclJfno,pclFlightEnd->pclJfno) == 0 &&
               strcmp(pclFlightBegin->pclOrg3,pclFlightEnd->pclOrg3) == 0 &&
               strcmp(pclFlightBegin->pclVia3,pclFlightEnd->pclVia3) == 0 &&
               strcmp(pclFlightBegin->pclVial,pclFlightEnd->pclVial) == 0 &&
               strcmp(pclSchedTimeBegin2,pclSchedTimeEnd2) == 0 &&
               strcmp(pclSchedTimeBegin,pclSchedTimeEnd) == 0)
        {
           ilCount--;
           ilI++;
           pclFlightEnd++;
           strcpy(pclSchedTimeEnd,&pclFlightEnd->pclStoa[8]);
           strcpy(pclSchedTimeEnd2,&pclFlightEnd->pclStod[8]);
        }
        ilRC = GenerateDayArraysSSI(pclFlightBegin,"A",ilI);
        ilRC = GenerateDoop();
        if(hasStyp == TRUE)
        	ilRC = InsSSITAB_STYP(pcpCurrentSeason,pcpCurFlno,pclFlightBegin,ipCommit);
        else
          ilRC = InsSSITAB(pcpCurrentSeason,pcpCurFlno,pclFlightBegin,ipCommit);
        pclFlightBegin = pclFlightEnd;
        pclFlightEnd++;
        ilCount--;
        ilI = 1;
     }
  }
  if (ilDepCount > 0)
  {
     for (ilI=0; ilI<ilDepCount; ilI++)
     {
        /*dbg(TRACE,"<%s>",&pclDepRec[ilI][0]);*/
        ilRC = GetFlightDataSSI(&pcgDepFlightSSI[ilI],&pclDepRec[ilI][0]);
     }
     pclFlightBegin = pcgDepFlightSSI;
     pclFlightEnd = pcgDepFlightSSI;
     pclFlightEnd++;
     ilCount = ilDepCount;
     ilI = 1;
     while (ilCount > 0)
     {
        strcpy(pclSchedTimeBegin,&pclFlightBegin->pclStod[8]);
        strcpy(pclSchedTimeEnd,&pclFlightEnd->pclStod[8]);
        strcpy(pclSchedTimeBegin2,&pclFlightBegin->pclStoa[8]);
        strcpy(pclSchedTimeEnd2,&pclFlightEnd->pclStoa[8]);
        while (ilCount > 1 &&
               strcmp(pclFlightBegin->pclTtyp,pclFlightEnd->pclTtyp) == 0 &&
               strcmp(pclFlightBegin->pclAct3,pclFlightEnd->pclAct3) == 0 &&
               strcmp(pclFlightBegin->pclJfno,pclFlightEnd->pclJfno) == 0 &&
               strcmp(pclFlightBegin->pclDes3,pclFlightEnd->pclDes3) == 0 &&
               strcmp(pclFlightBegin->pclVia3,pclFlightEnd->pclVia3) == 0 &&
               strcmp(pclFlightBegin->pclVial,pclFlightEnd->pclVial) == 0 &&
               strcmp(pclSchedTimeBegin2,pclSchedTimeEnd2) == 0 &&
               strcmp(pclSchedTimeBegin,pclSchedTimeEnd) == 0)
        {
           ilCount--;
           ilI++;
           pclFlightEnd++;
           strcpy(pclSchedTimeEnd,&pclFlightEnd->pclStod[8]);
           strcpy(pclSchedTimeEnd2,&pclFlightEnd->pclStoa[8]);
        }
        ilRC = GenerateDayArraysSSI(pclFlightBegin,"D",ilI);
        ilRC = GenerateDoop();
        if(hasStyp == TRUE)
        	ilRC = InsSSITAB_STYP(pcpCurrentSeason,pcpCurFlno,pclFlightBegin,ipCommit);
        else
          ilRC = InsSSITAB(pcpCurrentSeason,pcpCurFlno,pclFlightBegin,ipCommit);
        pclFlightBegin = pclFlightEnd;
        pclFlightEnd++;
        ilCount--;
        ilI = 1;
     }
  }
  if (igUseArrayInsert == TRUE && igArrayPtr > 0)
  {
     igArrayPtr--;
     pcgSsiBuf[igArrayPtr] = '\0';
     ilRC = SqlArrayInsert("SSITAB",pcgSsiFldLst,pcgSsiValLst);
  }

  return ilRC;
} /* End of GenerateFlightSSI */


static int GenerateFlightSSR(char *pcpCurrentSeason, char *pcpCurFlno, char *pcpFirstDay,
                             char *pcpLastDay, char *pcpMode,
                             char *pcpRotFlno, int ipCommit)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  short slFktR;
  short slCursorR;
  char pclSqlBufR[L_BUFF];
  char pclSelectBufR[L_BUFF];
  char pclDataBufR[XXL_BUFF];
  int ilArrCount = 0;
  int ilDepCount = 0;
  int ilRotCount = 0;
  char pclArrRec[MAX_NO_OF_ARR_FLIGHTS_SSR][1500];
  char pclDepRec[MAX_NO_OF_DEP_FLIGHTS_SSR][1500];
  char pclRotRecA[MAX_NO_OF_ROT_FLIGHTS_SSR][1500];
  char pclRotRecD[MAX_NO_OF_ROT_FLIGHTS_SSR][1500];
  int ilI;
  FLIGHT_RECORD_SSR *pclFlightBegin;
  FLIGHT_RECORD_SSR *pclFlightEnd;
  int ilCount;
  char pclSchedTimeBegin[32];
  char pclSchedTimeEnd[32];
  char pclSchedTimeBegin2[32];
  char pclSchedTimeEnd2[32];
  char pclSchedTimeBegin3[32];
  char pclSchedTimeEnd3[32];
  char pclSchedTimeBegin4[32];
  char pclSchedTimeEnd4[32];
  char pclRtyp[32];
  char pclRkey[32];
  int ilLen;
  char pclStoa[32];
  char pclStod[32];
  int ilStoa;
  int ilStod;
  int ilDiff;
  char pclRotFlno[32];
  int ilMins;
  int ilWeekDay;

  if (pcpRotFlno == NULL)
  {
     dbg(DEBUG,"Generate SSR for flight <%s> <%s> <NULL>",pcpCurFlno,pcpMode);
  }
  else
  {
     dbg(DEBUG,"Generate SSR for flight <%s> <%s> <%s>",pcpCurFlno,pcpMode,pcpRotFlno);
  }

  igArrayPtr = 0;
  strcpy(pcgSsiBuf,"");

  /* Lin */
  /* Force to use correct index  */
  strcpy(pclSqlBuf,"SELECT /*+ index(afttab AFTTAB_FLNO) */ ACT3,ACT5,ALC2,ALC3,FLTN,FLNS,JCNT,JFNO,STOA,STOD,ORG3,ORG4,VIA3,VIA4,VIAN,VIAL,TTYP,URNO,FLNO,RTYP,RKEY,STYP FROM AFTTAB ");
  sprintf(pclSelectBuf,"WHERE STOA BETWEEN '%s' AND '%s' AND FLNO = '%s' AND DES3 = '%s' AND ADID = 'A' AND FTYP IN (%s) ORDER BY TTYP,ACT3,JFNO,ORG3,VIA3,SUBSTR(STOD,9,6),SUBSTR(STOA,9,6),STOA",pcpFirstDay,pcpLastDay,pcpCurFlno,pcgHomeAp,pcgValidFtyp);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",22,",");
     ilLen = get_real_item(pclRtyp,pclDataBuf,20);
     TrimRight(pclRtyp);
     ilLen = get_real_item(pclRkey,pclDataBuf,21);
     TrimRight(pclRkey);
     if (strcmp(pclRtyp,"S") == 0 || strcmp(pclRtyp," ") == 0)
     {
        if (strcmp(pcpMode,"I") == 0 || strcmp(pcpMode,"A") == 0)
        {
           strcpy(&pclArrRec[ilArrCount][0],pclDataBuf);
           ilArrCount++;
        }
     }
     else
     {
        /* Lin */
        /* Force to use correct index  */
        strcpy(pclSqlBufR,"SELECT /*+ index(afttab AFTTAB_FLNO) */ ACT3,ACT5,ALC2,ALC3,FLTN,FLNS,JCNT,JFNO,STOD,STOA,DES3,DES4,VIA3,VIA4,VIAN,VIAL,TTYP,URNO,FLNO,STYP FROM AFTTAB ");
        sprintf(pclSelectBufR,"WHERE RKEY = %s AND ADID = 'D'",pclRkey);
        strcat(pclSqlBufR,pclSelectBufR);
        slCursorR = 0;
        slFktR = START;
        /* dbg(TRACE,"<%s>",pclSqlBufR); */
        ilRCdb = sql_if(slFktR,&slCursorR,pclSqlBufR,pclDataBufR);
        if (ilRCdb == DB_SUCCESS)
        {
           BuildItemBuffer(pclDataBufR,"",20,",");
           ilLen = get_real_item(pclRotFlno,pclDataBufR,19);
           TrimRight(pclRotFlno);
           if (strcmp(pcpMode,"I") == 0)
           {
              strcpy(&pclRotRecA[ilRotCount][0],pclDataBuf);
              strcpy(&pclRotRecD[ilRotCount][0],pclDataBufR);
              ilRotCount++;
           }
           else
           {
              if (strcmp(pcpMode,"AD") == 0)
              {
                 if (strcmp(pcpRotFlno,pclRotFlno) == 0)
                 {
                    strcpy(&pclRotRecA[ilRotCount][0],pclDataBuf);
                    strcpy(&pclRotRecD[ilRotCount][0],pclDataBufR);
                    ilRotCount++;
                 }
              }
           }
        }
        else
        {
           if (strcmp(pcpMode,"I") == 0 || strcmp(pcpMode,"A") == 0)
           {
              strcpy(&pclArrRec[ilArrCount][0],pclDataBuf);
              ilArrCount++;
           }
        }
        close_my_cursor(&slCursorR);
     }
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);

  /* Lin */
  /* Force to use correct index  */
  strcpy(pclSqlBuf,"SELECT /*+ index(afttab AFTTAB_FLNO) */ ACT3,ACT5,ALC2,ALC3,FLTN,FLNS,JCNT,JFNO,STOD,STOA,DES3,DES4,VIA3,VIA4,VIAN,VIAL,TTYP,URNO,FLNO,RTYP,RKEY,STYP FROM AFTTAB ");
  sprintf(pclSelectBuf,"WHERE STOD BETWEEN '%s' AND '%s' AND FLNO = '%s' AND ORG3 = '%s' AND ADID = 'D' AND FTYP IN (%s) ORDER BY TTYP,ACT3,JFNO,DES3,VIA3,SUBSTR(STOA,9,6),SUBSTR(STOD,9,6),STOD",pcpFirstDay,pcpLastDay,pcpCurFlno,pcgHomeAp,pcgValidFtyp);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",22,",");
     ilLen = get_real_item(pclRtyp,pclDataBuf,20);
     TrimRight(pclRtyp);
     ilLen = get_real_item(pclRkey,pclDataBuf,21);
     TrimRight(pclRkey);
     if (strcmp(pclRtyp,"S") == 0 || strcmp(pclRtyp," ") == 0)
     {
        if (strcmp(pcpMode,"I") == 0 || strcmp(pcpMode,"D") == 0)
        {
           strcpy(&pclDepRec[ilDepCount][0],pclDataBuf);
           ilDepCount++;
        }
     }
     else
     {
        /* Lin */
        /* Force to use correct index  */
        strcpy(pclSqlBufR,"SELECT /*+ index(afttab AFTTAB_FLNO) */ ACT3,ACT5,ALC2,ALC3,FLTN,FLNS,JCNT,JFNO,STOA,STOD,ORG3,ORG4,VIA3,VIA4,VIAN,VIAL,TTYP,URNO,FLNO,STYP FROM AFTTAB ");
        sprintf(pclSelectBufR,"WHERE RKEY = %s AND ADID = 'A'",pclRkey);
        strcat(pclSqlBufR,pclSelectBufR);
        slCursorR = 0;
        slFktR = START;
        /* dbg(TRACE,"<%s>",pclSqlBufR); */
        ilRCdb = sql_if(slFktR,&slCursorR,pclSqlBufR,pclDataBufR);
        if (ilRCdb == DB_SUCCESS)
        {
           BuildItemBuffer(pclDataBufR,"",20,",");
           ilLen = get_real_item(pclRotFlno,pclDataBufR,19);
           TrimRight(pclRotFlno);
           if (strcmp(pcpMode,"DA") == 0)
           {
              if (strcmp(pcpRotFlno,pclRotFlno) == 0)
              {
                 strcpy(&pclRotRecD[ilRotCount][0],pclDataBuf);
                 strcpy(&pclRotRecA[ilRotCount][0],pclDataBufR);
                 ilRotCount++;
              }
           }
        }
        else
        {
           if (strcmp(pcpMode,"I") == 0 || strcmp(pcpMode,"D") == 0)
           {
              strcpy(&pclDepRec[ilDepCount][0],pclDataBuf);
              ilDepCount++;
           }
        }
        close_my_cursor(&slCursorR);
     }
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);

  if (ilArrCount > 0)
  {
     for (ilI=0; ilI<ilArrCount; ilI++)
     {
        /* dbg(TRACE,"<%s>",&pclArrRec[ilI][0]); */
        ilRC = GetFlightDataSSR(&pcgArrFlightSSR[ilI],&pclArrRec[ilI][0],NULL);
        strcpy(&pcgArrFlightSSR[ilI].pclFtyp[0],"A");
        strcpy(&pcgArrFlightSSR[ilI].pclNstp[0],"0");
        strcpy(&pcgArrFlightSSR[ilI].pclFlta[0],pcpCurFlno);
        strcpy(&pcgArrFlightSSR[ilI].pclFltd[0]," ");
     }
     pclFlightBegin = pcgArrFlightSSR;
     pclFlightEnd = pcgArrFlightSSR;
     pclFlightEnd++;
     ilCount = ilArrCount;
     ilI = 1;
     while (ilCount > 0)
     {
        strcpy(pclSchedTimeBegin,&pclFlightBegin->pclStoa[8]);
        strcpy(pclSchedTimeEnd,&pclFlightEnd->pclStoa[8]);
        strcpy(pclSchedTimeBegin2,&pclFlightBegin->pclStda[8]);
        strcpy(pclSchedTimeEnd2,&pclFlightEnd->pclStda[8]);
        while (ilCount > 1 &&
               strcmp(pclFlightBegin->pclTtpa,pclFlightEnd->pclTtpa) == 0 &&
               strcmp(pclFlightBegin->pclAct3,pclFlightEnd->pclAct3) == 0 &&
               strcmp(pclFlightBegin->pclJfna,pclFlightEnd->pclJfna) == 0 &&
               strcmp(pclFlightBegin->pclOrg3,pclFlightEnd->pclOrg3) == 0 &&
               strcmp(pclFlightBegin->pclVi3a,pclFlightEnd->pclVi3a) == 0 &&
               strcmp(pclFlightBegin->pclVila,pclFlightEnd->pclVila) == 0 &&
               /* strcmp(pclSchedTimeBegin2,pclSchedTimeEnd2) == 0 && */
               strcmp(pclSchedTimeBegin,pclSchedTimeEnd) == 0)
        {
           ilCount--;
           ilI++;
           pclFlightEnd++;
           strcpy(pclSchedTimeEnd,&pclFlightEnd->pclStoa[8]);
           strcpy(pclSchedTimeEnd2,&pclFlightEnd->pclStda[8]);
        }
        ilRC = GenerateDayArraysSSR(pclFlightBegin,"A",ilI);
        ilRC = GenerateDoop();
        //ilRC = InsSSRTAB(pcpCurrentSeason,pclFlightBegin,ipCommit);
        if(hasStyad == TRUE)
           ilRC = InsSSRTAB_STYAD(pcpCurrentSeason,pclFlightBegin,ipCommit);
        else
        	ilRC = InsSSRTAB(pcpCurrentSeason,pclFlightBegin,ipCommit);
        pclFlightBegin = pclFlightEnd;
        pclFlightEnd++;
        ilCount--;
        ilI = 1;
     }
  }
  if (ilDepCount > 0)
  {
     for (ilI=0; ilI<ilDepCount; ilI++)
     {
        /* dbg(TRACE,"<%s>",&pclDepRec[ilI][0]); */
        ilRC = GetFlightDataSSR(&pcgDepFlightSSR[ilI],NULL,&pclDepRec[ilI][0]);
        strcpy(&pcgDepFlightSSR[ilI].pclFtyp[0],"D");
        strcpy(&pcgDepFlightSSR[ilI].pclNstp[0],"0");
        strcpy(&pcgDepFlightSSR[ilI].pclFltd[0],pcpCurFlno);
        strcpy(&pcgDepFlightSSR[ilI].pclFlta[0]," ");
     }
     pclFlightBegin = pcgDepFlightSSR;
     pclFlightEnd = pcgDepFlightSSR;
     pclFlightEnd++;
     ilCount = ilDepCount;
     ilI = 1;
     while (ilCount > 0)
     {
        strcpy(pclSchedTimeBegin,&pclFlightBegin->pclStod[8]);
        strcpy(pclSchedTimeEnd,&pclFlightEnd->pclStod[8]);
        strcpy(pclSchedTimeBegin2,&pclFlightBegin->pclStad[8]);
        strcpy(pclSchedTimeEnd2,&pclFlightEnd->pclStad[8]);
        while (ilCount > 1 &&
               strcmp(pclFlightBegin->pclTtpd,pclFlightEnd->pclTtpd) == 0 &&
               strcmp(pclFlightBegin->pclAct3,pclFlightEnd->pclAct3) == 0 &&
               strcmp(pclFlightBegin->pclJfnd,pclFlightEnd->pclJfnd) == 0 &&
               strcmp(pclFlightBegin->pclDes3,pclFlightEnd->pclDes3) == 0 &&
               strcmp(pclFlightBegin->pclVi3d,pclFlightEnd->pclVi3d) == 0 &&
               strcmp(pclFlightBegin->pclVild,pclFlightEnd->pclVild) == 0 &&
               /* strcmp(pclSchedTimeBegin2,pclSchedTimeEnd2) == 0 && */
               strcmp(pclSchedTimeBegin,pclSchedTimeEnd) == 0)
        {
           ilCount--;
           ilI++;
           pclFlightEnd++;
           strcpy(pclSchedTimeEnd,&pclFlightEnd->pclStod[8]);
           strcpy(pclSchedTimeEnd2,&pclFlightEnd->pclStad[8]);
        }
        ilRC = GenerateDayArraysSSR(pclFlightBegin,"D",ilI);
        ilRC = GenerateDoop();
        if(hasStyad == TRUE)
           ilRC = InsSSRTAB_STYAD(pcpCurrentSeason,pclFlightBegin,ipCommit);
        else
        	ilRC = InsSSRTAB(pcpCurrentSeason,pclFlightBegin,ipCommit);
        pclFlightBegin = pclFlightEnd;
        pclFlightEnd++;
        ilCount--;
        ilI = 1;
     }
  }
  if (ilRotCount > 0)
  {
     for (ilI=0; ilI<ilRotCount; ilI++)
     {
        /* dbg(TRACE,"<%s>",&pclRotRecA[ilI][0]); */
         //dbg(TRACE,"nostyp <%s>",&pclRotRecD[ilI][0]); 
        ilRC = GetFlightDataSSR_Rotation(&pcgRotFlightSSR[ilI],&pclRotRecA[ilI][0],&pclRotRecD[ilI][0]);
        ilLen = get_real_item(&pcgRotFlightSSR[ilI].pclFlta[0],
                              &pclRotRecA[ilI][0],19);
        ilLen = get_real_item(&pcgRotFlightSSR[ilI].pclFltd[0],
                              &pclRotRecD[ilI][0],19);
        strcpy(&pcgRotFlightSSR[ilI].pclFtyp[0],"R");
        if (strncmp(&pcgRotFlightSSR[ilI].pclStoa[0],&pcgRotFlightSSR[ilI].pclStod[0],8) == 0)
        {
           strcpy(&pcgRotFlightSSR[ilI].pclNstp[0],"0");
        }
        else
        {
           strncpy(pclStoa,&pcgRotFlightSSR[ilI].pclStoa[0],8);
           strncpy(pclStod,&pcgRotFlightSSR[ilI].pclStod[0],8);
           ilRC = GetFullDay(pclStoa,&ilStoa,&ilMins,&ilWeekDay);
           ilRC = GetFullDay(pclStod,&ilStod,&ilMins,&ilWeekDay);
           ilDiff = ilStod - ilStoa;
           if (ilDiff >= 0)
           {
              sprintf(&pcgRotFlightSSR[ilI].pclNstp[0],"%d",ilDiff);
           }
           else
           {
              dbg(TRACE,"Negative Night Stop: <%s> <%s>",
                  &pcgRotFlightSSR[ilI].pclStoa[0],&pcgRotFlightSSR[ilI].pclStod[0]);
              strcpy(&pcgRotFlightSSR[ilI].pclNstp[0],"-");
           }
        }
     }
     ilRC = SortRot(pcgRotFlightSSR,ilRotCount,pcpMode);
     pclFlightBegin = pcgRotFlightSSR;
     pclFlightEnd = pcgRotFlightSSR;
     pclFlightEnd++;
     ilCount = ilRotCount;
     ilI = 1;
     while (ilCount > 0)
     {
        strcpy(pclSchedTimeBegin,&pclFlightBegin->pclStoa[8]);
        strcpy(pclSchedTimeEnd,&pclFlightEnd->pclStoa[8]);
        strcpy(pclSchedTimeBegin2,&pclFlightBegin->pclStda[8]);
        strcpy(pclSchedTimeEnd2,&pclFlightEnd->pclStda[8]);
        strcpy(pclSchedTimeBegin3,&pclFlightBegin->pclStod[8]);
        strcpy(pclSchedTimeEnd3,&pclFlightEnd->pclStod[8]);
        strcpy(pclSchedTimeBegin4,&pclFlightBegin->pclStad[8]);
        strcpy(pclSchedTimeEnd4,&pclFlightEnd->pclStad[8]);
        while (ilCount > 1 &&
               strcmp(pclFlightBegin->pclTtpa,pclFlightEnd->pclTtpa) == 0 &&
               strcmp(pclFlightBegin->pclAct3,pclFlightEnd->pclAct3) == 0 &&
               strcmp(pclFlightBegin->pclJfna,pclFlightEnd->pclJfna) == 0 &&
               strcmp(pclFlightBegin->pclOrg3,pclFlightEnd->pclOrg3) == 0 &&
               strcmp(pclFlightBegin->pclVi3a,pclFlightEnd->pclVi3a) == 0 &&
               strcmp(pclFlightBegin->pclVila,pclFlightEnd->pclVila) == 0 &&
               /* strcmp(pclSchedTimeBegin2,pclSchedTimeEnd2) == 0 && */
               strcmp(pclSchedTimeBegin,pclSchedTimeEnd) == 0 &&
               strcmp(pclFlightBegin->pclFltd,pclFlightEnd->pclFltd) == 0 &&
               strcmp(pclFlightBegin->pclTtpd,pclFlightEnd->pclTtpd) == 0 &&
               strcmp(pclFlightBegin->pclJfnd,pclFlightEnd->pclJfnd) == 0 &&
               strcmp(pclFlightBegin->pclDes3,pclFlightEnd->pclDes3) == 0 &&
               strcmp(pclFlightBegin->pclVi3d,pclFlightEnd->pclVi3d) == 0 &&
               strcmp(pclFlightBegin->pclVild,pclFlightEnd->pclVild) == 0 &&
               /* strcmp(pclSchedTimeBegin4,pclSchedTimeEnd4) == 0 && */
               strcmp(pclSchedTimeBegin3,pclSchedTimeEnd3) == 0 &&
               strcmp(pclFlightBegin->pclNstp,pclFlightEnd->pclNstp) == 0)
        {
           ilCount--;
           ilI++;
           pclFlightEnd++;
           strcpy(pclSchedTimeEnd,&pclFlightEnd->pclStoa[8]);
           strcpy(pclSchedTimeEnd2,&pclFlightEnd->pclStda[8]);
           strcpy(pclSchedTimeEnd3,&pclFlightEnd->pclStod[8]);
           strcpy(pclSchedTimeEnd4,&pclFlightEnd->pclStad[8]);
        }
        dbg(TRACE,"check styd <%s>",
                  pclFlightBegin->pclStyd);
        ilRC = GenerateDayArraysSSR(pclFlightBegin,"A",ilI);
        ilRC = GenerateDoop();
        //ilRC = InsSSRTAB(pcpCurrentSeason,pclFlightBegin,ipCommit);
        if(hasStyad == TRUE)
           ilRC = InsSSRTAB_STYAD(pcpCurrentSeason,pclFlightBegin,ipCommit);
        else
        	ilRC = InsSSRTAB(pcpCurrentSeason,pclFlightBegin,ipCommit);
        	
        pclFlightBegin = pclFlightEnd;
        pclFlightEnd++;
        ilCount--;
        ilI = 1;
     }
  }

  if (igUseArrayInsert == TRUE && igArrayPtr > 0)
  {
     igArrayPtr--;
     pcgSsiBuf[igArrayPtr] = '\0';
     ilRC = SqlArrayInsert("SSRTAB",pcgSsrFldLst,pcgSsrValLst);
  }

  return ilRC;
} /* End of GenerateFlightSSR */


static int GetFlightDataSSI(FLIGHT_RECORD_SSI *pcpFlight, char *pcpRec)
{
  int ilRC = RC_SUCCESS;
  int ilLen;
  char pclVial[1024];
  int ilNo;
  int ilI;
  VialTypePtrSSI pclSSIVial;
  VialTypePtrAFT pclAFTVial;

  ilLen = get_real_item(pcpFlight->pclTtyp,pcpRec,1);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclTtyp," ");
  }
  ilLen = get_real_item(pcpFlight->pclAct3,pcpRec,2);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclAct3," ");
  }
  ilLen = get_real_item(pcpFlight->pclAct5,pcpRec,3);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclAct5," ");
  }
  ilLen = get_real_item(pcpFlight->pclAlc2,pcpRec,4);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclAlc2," ");
  }
  ilLen = get_real_item(pcpFlight->pclAlc3,pcpRec,5);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclAlc3," ");
  }
  ilLen = get_real_item(pcpFlight->pclFltn,pcpRec,6);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclFltn," ");
  }
  ilLen = get_real_item(pcpFlight->pclFlns,pcpRec,7);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclFlns," ");
  }
  ilLen = get_real_item(pcpFlight->pclJcnt,pcpRec,8);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclJcnt," ");
  }
  ilLen = get_real_item(pcpFlight->pclJfno,pcpRec,9);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclJfno," ");
  }
  ilLen = get_real_item(pcpFlight->pclStoa,pcpRec,10);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclStoa,"              ");
  }
  ilLen = get_real_item(pcpFlight->pclStod,pcpRec,11);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclStod,"              ");
  } 
  ilLen = get_real_item(pcpFlight->pclOrg3,pcpRec,12);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclOrg3," ");
  }
  ilLen = get_real_item(pcpFlight->pclOrg4,pcpRec,13);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclOrg4," ");
  }
  ilLen = get_real_item(pcpFlight->pclDes3,pcpRec,14);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclDes3," ");
  }
  ilLen = get_real_item(pcpFlight->pclDes4,pcpRec,15);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclDes4," ");
  }
  ilLen = get_real_item(pcpFlight->pclVia3,pcpRec,16);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclVia3," ");
  }
  ilLen = get_real_item(pcpFlight->pclVia4,pcpRec,17);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclVia4," ");
  }
  ilLen = get_real_item(pcpFlight->pclVian,pcpRec,18);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclVian," ");
  }
  ilLen = get_real_item(pclVial,pcpRec,19);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclVial," ");
  }
  else /* Handle VIAL update */
  {
    ilNo = atoi(pcpFlight->pclVian);
    pclSSIVial = (VialTypePtrSSI)&(pcpFlight->pclVial[0]);
    pclAFTVial = (VialTypePtrAFT)&(pclVial[0]);
    for (ilI = 0; ilI < ilNo; ilI++){
      if (ilLen > ilI*120 + 1){/* APC3 provided ? */
    strncpy(pclSSIVial->apc3,pclAFTVial->apc3,3);
    pclSSIVial->apc3[3] = '\0';
      }
      if (ilLen > ilI*120 + 4){/* APC4 provided ? */
    strncpy(pclSSIVial->apc4,pclAFTVial->apc4,4);
    pclSSIVial->apc4[4] = '\0';
      }
      if (ilLen > ilI*120 + 8){/* STOA provided? */
    strncpy(pclSSIVial->stoaTime,pclAFTVial->stoaTime,4);
    pclSSIVial->stoaTime[4] = '\0';
      }
      if (ilLen > ilI*120 + 64){/* STOD provided? */
    strncpy(pclSSIVial->stodTime,pclAFTVial->stodTime,4);
    pclSSIVial->stodTime[4] = '\0';
      }
      pclSSIVial++;
      pclAFTVial++;
    }
  }
  ilLen = get_real_item(pcpFlight->pclAdid,pcpRec,20);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclAdid," ");
  }
  ilLen = get_real_item(pcpFlight->pclUrno,pcpRec,21);
  
  ilLen = get_real_item(pcpFlight->pclStyp,pcpRec,22);
  if (ilLen == 0)
  {
     strcpy(pcpFlight->pclStyp," ");
  }
  return ilRC;
} /* End of GetFlightDataSSI */


static int GetFlightDataSSR(FLIGHT_RECORD_SSR *pcpFlight, char *pcpRecA, char *pcpRecD)
{
  int ilRC = RC_SUCCESS;
  int ilLen;
  char pclVial[1024];
  int ilNo;
  int ilI;

  if (pcpRecA != NULL)
  {
     ilLen = get_real_item(pcpFlight->pclAct3,pcpRecA,1);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclAct3," ");
     }
     ilLen = get_real_item(pcpFlight->pclAct5,pcpRecA,2);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclAct5," ");
     }
  }
  else
  {
     ilLen = get_real_item(pcpFlight->pclAct3,pcpRecD,1);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclAct3," ");
     }
     ilLen = get_real_item(pcpFlight->pclAct5,pcpRecD,2);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclAct5," ");
     }
  }
  if (pcpRecA != NULL)
  {
     ilLen = get_real_item(pcpFlight->pclAl2a,pcpRecA,3);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclAl2a," ");
     }
     ilLen = get_real_item(pcpFlight->pclAl3a,pcpRecA,4);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclAl3a," ");
     }
     ilLen = get_real_item(pcpFlight->pclFlna,pcpRecA,5);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclFlna," ");
     }
     ilLen = get_real_item(pcpFlight->pclFlsa,pcpRecA,6);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclFlsa," ");
     }
     ilLen = get_real_item(pcpFlight->pclJcna,pcpRecA,7);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclJcna," ");
     }
     ilLen = get_real_item(pcpFlight->pclJfna,pcpRecA,8);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclJfna," ");
     }
     ilLen = get_real_item(pcpFlight->pclStoa,pcpRecA,9);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclStoa,"              ");
     }
     ilLen = get_real_item(pcpFlight->pclStda,pcpRecA,10);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclStda,"              ");
     }
     ilLen = get_real_item(pcpFlight->pclOrg3,pcpRecA,11);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclOrg3," ");
     }
     ilLen = get_real_item(pcpFlight->pclOrg4,pcpRecA,12);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclOrg4," ");
     }
     ilLen = get_real_item(pcpFlight->pclVi3a,pcpRecA,13);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclVi3a," ");
     }
     ilLen = get_real_item(pcpFlight->pclVi4a,pcpRecA,14);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclVi4a," ");
     }
     ilLen = get_real_item(pcpFlight->pclVina,pcpRecA,15);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclVina," ");
     }
     ilLen = get_real_item(pclVial,pcpRecA,16);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclVila," ");
     }
     else
     {
        ilNo = atoi(pcpFlight->pclVina);
        for (ilI = 0; ilI < ilNo; ilI++)
        {
           if (ilLen > ilI*120 + 1)
           {
              strncpy(&pcpFlight->pclVila[ilI*15],&pclVial[ilI*120+1],3);
              pcpFlight->pclVila[ilI*15+3] = '\0';
           }
           if (ilLen > ilI*120 + 4)
           {
              strncpy(&pcpFlight->pclVila[ilI*15+3],&pclVial[ilI*120+4],4);
              pcpFlight->pclVila[ilI*15+7] = '\0';
           }
           if (ilLen > ilI*120 + 8)
           {
              strncpy(&pcpFlight->pclVila[ilI*15+7],&pclVial[ilI*120+16],4);
              pcpFlight->pclVila[ilI*15+11] = '\0';
           }
           if (ilLen > ilI*120 + 64)
           {
              strncpy(&pcpFlight->pclVila[ilI*15+11],&pclVial[ilI*120+72],4);
              pcpFlight->pclVila[ilI*15+15] = '\0';
           }
        }
     }
     ilLen = get_real_item(pcpFlight->pclTtpa,pcpRecA,17);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclTtpa," ");
     }
     ilLen = get_real_item(pcpFlight->pclUrna,pcpRecA,18);
     
     //added by MAX
     ilLen = get_real_item(pcpFlight->pclStya,pcpRecA,20);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclStya," ");
        dbg(TRACE,"<GetFlightDataSSR> no styp in arrival flight data");
     }
  }
  else
  {
     strcpy(pcpFlight->pclAl2a," ");
     strcpy(pcpFlight->pclAl3a," ");
     strcpy(pcpFlight->pclFlna," ");
     strcpy(pcpFlight->pclFlsa," ");
     strcpy(pcpFlight->pclJcna," ");
     strcpy(pcpFlight->pclJfna," ");
     strcpy(pcpFlight->pclStoa,"              ");
     strcpy(pcpFlight->pclStda,"              ");
     strcpy(pcpFlight->pclOrg3," ");
     strcpy(pcpFlight->pclOrg4," ");
     strcpy(pcpFlight->pclVi3a," ");
     strcpy(pcpFlight->pclVi4a," ");
     strcpy(pcpFlight->pclVina," ");
     strcpy(pcpFlight->pclVila," ");
     strcpy(pcpFlight->pclTtpa," ");
     strcpy(pcpFlight->pclUrna," ");
     
     strcpy(pcpFlight->pclStya," ");
  }
  if (pcpRecD != NULL)
  {
     ilLen = get_real_item(pcpFlight->pclAl2d,pcpRecD,3);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclAl2d," ");
     }
     ilLen = get_real_item(pcpFlight->pclAl3d,pcpRecD,4);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclAl3d," ");
     }
     ilLen = get_real_item(pcpFlight->pclFlnd,pcpRecD,5);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclFlnd," ");
     }
     ilLen = get_real_item(pcpFlight->pclFlsd,pcpRecD,6);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclFlsd," ");
     }
     ilLen = get_real_item(pcpFlight->pclJcnd,pcpRecD,7);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclJcnd," ");
     }
     ilLen = get_real_item(pcpFlight->pclJfnd,pcpRecD,8);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclJfnd," ");
     }
     ilLen = get_real_item(pcpFlight->pclStod,pcpRecD,9);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclStod,"              ");
     }
     ilLen = get_real_item(pcpFlight->pclStad,pcpRecD,10);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclStad,"              ");
     }
     ilLen = get_real_item(pcpFlight->pclDes3,pcpRecD,11);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclDes3," ");
     }
     ilLen = get_real_item(pcpFlight->pclDes4,pcpRecD,12);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclDes4," ");
     }
     ilLen = get_real_item(pcpFlight->pclVi3d,pcpRecD,13);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclVi3d," ");
     }
     ilLen = get_real_item(pcpFlight->pclVi4d,pcpRecD,14);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclVi4d," ");
     }
     ilLen = get_real_item(pcpFlight->pclVind,pcpRecD,15);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclVind," ");
     }
     ilLen = get_real_item(pclVial,pcpRecD,16);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclVild," ");
     }
     else
     {
        ilNo = atoi(pcpFlight->pclVind);
        for (ilI = 0; ilI < ilNo; ilI++)
        {
           if (ilLen > ilI*120 + 1)
           {
              strncpy(&pcpFlight->pclVild[ilI*15],&pclVial[ilI*120+1],3);
              pcpFlight->pclVild[ilI*15+3] = '\0';
           }
           if (ilLen > ilI*120 + 4)
           {
              strncpy(&pcpFlight->pclVild[ilI*15+3],&pclVial[ilI*120+4],4);
              pcpFlight->pclVild[ilI*15+7] = '\0';
           }
           if (ilLen > ilI*120 + 8)
           {
              strncpy(&pcpFlight->pclVild[ilI*15+7],&pclVial[ilI*120+16],4);
              pcpFlight->pclVild[ilI*15+11] = '\0';
           }
           if (ilLen > ilI*120 + 64)
           {
              strncpy(&pcpFlight->pclVild[ilI*15+11],&pclVial[ilI*120+72],4);
              pcpFlight->pclVild[ilI*15+15] = '\0';
           }
        }
     }
     ilLen = get_real_item(pcpFlight->pclTtpd,pcpRecD,17);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclTtpd," ");
     }
     ilLen = get_real_item(pcpFlight->pclUrnd,pcpRecD,18);
     
     ilLen = get_real_item(pcpFlight->pclStyd,pcpRecD,22);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclStyd," ");
        dbg(TRACE,"<GetFlightDataSSR> no styp in depature flight data");
     }
  }
  else
  {
     strcpy(pcpFlight->pclAl2d," ");
     strcpy(pcpFlight->pclAl3d," ");
     strcpy(pcpFlight->pclFlnd," ");
     strcpy(pcpFlight->pclFlsd," ");
     strcpy(pcpFlight->pclJcnd," ");
     strcpy(pcpFlight->pclJfnd," ");
     strcpy(pcpFlight->pclStod,"              ");
     strcpy(pcpFlight->pclStad,"              ");
     strcpy(pcpFlight->pclDes3," ");
     strcpy(pcpFlight->pclDes4," ");
     strcpy(pcpFlight->pclVi3d," ");
     strcpy(pcpFlight->pclVi4d," ");
     strcpy(pcpFlight->pclVind," ");
     strcpy(pcpFlight->pclVild," ");
     strcpy(pcpFlight->pclTtpd," ");
     strcpy(pcpFlight->pclUrnd," ");
     
     strcpy(pcpFlight->pclStyd," ");
  }

  return ilRC;
} /* End of GetFlightDataSSR */
static int GetFlightDataSSR_Rotation(FLIGHT_RECORD_SSR *pcpFlight, char *pcpRecA, char *pcpRecD)
{
  int ilRC = RC_SUCCESS;
  int ilLen;
  char pclVial[1024];
  int ilNo;
  int ilI;

  if (pcpRecA != NULL)
  {
     ilLen = get_real_item(pcpFlight->pclAct3,pcpRecA,1);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclAct3," ");
     }
     ilLen = get_real_item(pcpFlight->pclAct5,pcpRecA,2);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclAct5," ");
     }
  }
  else
  {
     ilLen = get_real_item(pcpFlight->pclAct3,pcpRecD,1);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclAct3," ");
     }
     ilLen = get_real_item(pcpFlight->pclAct5,pcpRecD,2);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclAct5," ");
     }
  }
  if (pcpRecA != NULL)
  {
     ilLen = get_real_item(pcpFlight->pclAl2a,pcpRecA,3);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclAl2a," ");
     }
     ilLen = get_real_item(pcpFlight->pclAl3a,pcpRecA,4);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclAl3a," ");
     }
     ilLen = get_real_item(pcpFlight->pclFlna,pcpRecA,5);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclFlna," ");
     }
     ilLen = get_real_item(pcpFlight->pclFlsa,pcpRecA,6);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclFlsa," ");
     }
     ilLen = get_real_item(pcpFlight->pclJcna,pcpRecA,7);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclJcna," ");
     }
     ilLen = get_real_item(pcpFlight->pclJfna,pcpRecA,8);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclJfna," ");
     }
     ilLen = get_real_item(pcpFlight->pclStoa,pcpRecA,9);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclStoa,"              ");
     }
     ilLen = get_real_item(pcpFlight->pclStda,pcpRecA,10);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclStda,"              ");
     }
     ilLen = get_real_item(pcpFlight->pclOrg3,pcpRecA,11);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclOrg3," ");
     }
     ilLen = get_real_item(pcpFlight->pclOrg4,pcpRecA,12);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclOrg4," ");
     }
     ilLen = get_real_item(pcpFlight->pclVi3a,pcpRecA,13);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclVi3a," ");
     }
     ilLen = get_real_item(pcpFlight->pclVi4a,pcpRecA,14);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclVi4a," ");
     }
     ilLen = get_real_item(pcpFlight->pclVina,pcpRecA,15);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclVina," ");
     }
     ilLen = get_real_item(pclVial,pcpRecA,16);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclVila," ");
     }
     else
     {
        ilNo = atoi(pcpFlight->pclVina);
        for (ilI = 0; ilI < ilNo; ilI++)
        {
           if (ilLen > ilI*120 + 1)
           {
              strncpy(&pcpFlight->pclVila[ilI*15],&pclVial[ilI*120+1],3);
              pcpFlight->pclVila[ilI*15+3] = '\0';
           }
           if (ilLen > ilI*120 + 4)
           {
              strncpy(&pcpFlight->pclVila[ilI*15+3],&pclVial[ilI*120+4],4);
              pcpFlight->pclVila[ilI*15+7] = '\0';
           }
           if (ilLen > ilI*120 + 8)
           {
              strncpy(&pcpFlight->pclVila[ilI*15+7],&pclVial[ilI*120+16],4);
              pcpFlight->pclVila[ilI*15+11] = '\0';
           }
           if (ilLen > ilI*120 + 64)
           {
              strncpy(&pcpFlight->pclVila[ilI*15+11],&pclVial[ilI*120+72],4);
              pcpFlight->pclVila[ilI*15+15] = '\0';
           }
        }
     }
     ilLen = get_real_item(pcpFlight->pclTtpa,pcpRecA,17);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclTtpa," ");
     }
     ilLen = get_real_item(pcpFlight->pclUrna,pcpRecA,18);
     
     //added by MAX
     ilLen = get_real_item(pcpFlight->pclStya,pcpRecA,20);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclStya," ");
        dbg(TRACE,"<GetFlightDataSSR> no styp in arrival flight data");
     }
  }
  else
  {
     strcpy(pcpFlight->pclAl2a," ");
     strcpy(pcpFlight->pclAl3a," ");
     strcpy(pcpFlight->pclFlna," ");
     strcpy(pcpFlight->pclFlsa," ");
     strcpy(pcpFlight->pclJcna," ");
     strcpy(pcpFlight->pclJfna," ");
     strcpy(pcpFlight->pclStoa,"              ");
     strcpy(pcpFlight->pclStda,"              ");
     strcpy(pcpFlight->pclOrg3," ");
     strcpy(pcpFlight->pclOrg4," ");
     strcpy(pcpFlight->pclVi3a," ");
     strcpy(pcpFlight->pclVi4a," ");
     strcpy(pcpFlight->pclVina," ");
     strcpy(pcpFlight->pclVila," ");
     strcpy(pcpFlight->pclTtpa," ");
     strcpy(pcpFlight->pclUrna," ");
     
     strcpy(pcpFlight->pclStya," ");
  }
  if (pcpRecD != NULL)
  {
     ilLen = get_real_item(pcpFlight->pclAl2d,pcpRecD,3);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclAl2d," ");
     }
     ilLen = get_real_item(pcpFlight->pclAl3d,pcpRecD,4);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclAl3d," ");
     }
     ilLen = get_real_item(pcpFlight->pclFlnd,pcpRecD,5);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclFlnd," ");
     }
     ilLen = get_real_item(pcpFlight->pclFlsd,pcpRecD,6);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclFlsd," ");
     }
     ilLen = get_real_item(pcpFlight->pclJcnd,pcpRecD,7);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclJcnd," ");
     }
     ilLen = get_real_item(pcpFlight->pclJfnd,pcpRecD,8);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclJfnd," ");
     }
     ilLen = get_real_item(pcpFlight->pclStod,pcpRecD,9);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclStod,"              ");
     }
     ilLen = get_real_item(pcpFlight->pclStad,pcpRecD,10);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclStad,"              ");
     }
     ilLen = get_real_item(pcpFlight->pclDes3,pcpRecD,11);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclDes3," ");
     }
     ilLen = get_real_item(pcpFlight->pclDes4,pcpRecD,12);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclDes4," ");
     }
     ilLen = get_real_item(pcpFlight->pclVi3d,pcpRecD,13);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclVi3d," ");
     }
     ilLen = get_real_item(pcpFlight->pclVi4d,pcpRecD,14);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclVi4d," ");
     }
     ilLen = get_real_item(pcpFlight->pclVind,pcpRecD,15);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclVind," ");
     }
     ilLen = get_real_item(pclVial,pcpRecD,16);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclVild," ");
     }
     else
     {
        ilNo = atoi(pcpFlight->pclVind);
        for (ilI = 0; ilI < ilNo; ilI++)
        {
           if (ilLen > ilI*120 + 1)
           {
              strncpy(&pcpFlight->pclVild[ilI*15],&pclVial[ilI*120+1],3);
              pcpFlight->pclVild[ilI*15+3] = '\0';
           }
           if (ilLen > ilI*120 + 4)
           {
              strncpy(&pcpFlight->pclVild[ilI*15+3],&pclVial[ilI*120+4],4);
              pcpFlight->pclVild[ilI*15+7] = '\0';
           }
           if (ilLen > ilI*120 + 8)
           {
              strncpy(&pcpFlight->pclVild[ilI*15+7],&pclVial[ilI*120+16],4);
              pcpFlight->pclVild[ilI*15+11] = '\0';
           }
           if (ilLen > ilI*120 + 64)
           {
              strncpy(&pcpFlight->pclVild[ilI*15+11],&pclVial[ilI*120+72],4);
              pcpFlight->pclVild[ilI*15+15] = '\0';
           }
        }
     }
     ilLen = get_real_item(pcpFlight->pclTtpd,pcpRecD,17);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclTtpd," ");
     }
     ilLen = get_real_item(pcpFlight->pclUrnd,pcpRecD,18);
     
     ilLen = get_real_item(pcpFlight->pclStyd,pcpRecD,20);
     if (ilLen == 0)
     {
        strcpy(pcpFlight->pclStyd," ");
        dbg(TRACE,"<GetFlightDataSSR> no styp in depature flight data");
     }
  }
  else
  {
     strcpy(pcpFlight->pclAl2d," ");
     strcpy(pcpFlight->pclAl3d," ");
     strcpy(pcpFlight->pclFlnd," ");
     strcpy(pcpFlight->pclFlsd," ");
     strcpy(pcpFlight->pclJcnd," ");
     strcpy(pcpFlight->pclJfnd," ");
     strcpy(pcpFlight->pclStod,"              ");
     strcpy(pcpFlight->pclStad,"              ");
     strcpy(pcpFlight->pclDes3," ");
     strcpy(pcpFlight->pclDes4," ");
     strcpy(pcpFlight->pclVi3d," ");
     strcpy(pcpFlight->pclVi4d," ");
     strcpy(pcpFlight->pclVind," ");
     strcpy(pcpFlight->pclVild," ");
     strcpy(pcpFlight->pclTtpd," ");
     strcpy(pcpFlight->pclUrnd," ");
     
     strcpy(pcpFlight->pclStyd," ");
  }

  return ilRC;
} /* End of GetFlightDataSSR_R */

static int GenerateDayArraysSSI(FLIGHT_RECORD_SSI *pcpFlight, char *pcpType, int ipNo)
{
  int ilRC = RC_SUCCESS;
  int ilI;
  int ilJ;
  char pclTime[32];
  int ilTime;
  int ilWeek;
  int ilDay;
  int ilMinWeek;
  int ilMaxWeek;
  int ilMinDay;
  int ilMaxDay;
  int ilDays;

  for (ilI = 0; ilI < 52; ilI++)
  {
     for (ilJ = 1; ilJ <= 7; ilJ++)
     {
        strcpy(&pcgDayArray1[ilI][ilJ][0],"        ");
        strcpy(&pcgUrnoArray[ilI][ilJ][0]," ");
     }
     strcpy(&pcgDayArray2[ilI][0]," 0000000");
  }

  igMinWeek = 51;
  igMaxWeek = 0;
  for (ilI = 0; ilI < ipNo; ilI++)
  {
     if (strcmp(pcpType,"A") == 0)
     {
        strcpy(pclTime,pcpFlight->pclStoa);
     }
     else
     {
        strcpy(pclTime,pcpFlight->pclStod);
     }
     ilRC = GetMyFullDay(pclTime,&ilDays,&ilTime,&ilDay,&ilWeek);
     if (ilWeek >= 0)
     {
        if (ilWeek <= igMinWeek)
        {
           igMinWeek = ilWeek;
           if (ilDay < igMinDay)
           {
              igMinDay = ilDay;
           }
        }
        if (ilWeek >= igMaxWeek)
        {
           igMaxWeek = ilWeek;
           if (ilDay > igMaxDay)
           {
              igMaxDay = ilDay;
           }
        }
     }
     strncpy(&pcgDayArray1[ilWeek][ilDay][0],pclTime,8);
     pcgDayArray1[ilWeek][ilDay][8] = '\0';
     strcpy(&pcgUrnoArray[ilWeek][ilDay][0],"#");
     strcat(&pcgUrnoArray[ilWeek][ilDay][0],pcpFlight->pclUrno);
     pcpFlight++;
  }
  igMinDay = 7;
  igMaxDay = 1;
  for (ilI = 7; ilI >= 1; ilI--)
  {
     if (strcmp(&pcgDayArray1[igMinWeek][ilI][0],"        ") != 0)
     {
        igMinDay = ilI;
     }
  }
  for (ilI = 1; ilI <= 7; ilI++)
  {
     if (strcmp(&pcgDayArray1[igMaxWeek][ilI][0],"        ") != 0)
     {
        igMaxDay = ilI;
     }
  }

  for (ilI = igMinWeek; ilI <= igMaxWeek; ilI++)
  {
     for (ilJ = 1; ilJ <= 7; ilJ++)
     {
        if (strcmp(&pcgDayArray1[ilI][ilJ][0],"        ") != 0)
        {
           pcgDayArray2[ilI][ilJ] = 0x31;
        }
     }
  }
  if (igMaxWeek > igMinWeek)
  {
     ilMinWeek = igMinWeek;
     ilMaxWeek = ilMinWeek + 1;
     while (ilMaxWeek <= igMaxWeek + 1)
     {
        if (strcmp(&pcgDayArray2[ilMaxWeek][0]," 0000000") == 0)
        {
           ilMaxWeek--;
           ilMinDay = 7;
           for (ilI = 7; ilI >= 1; ilI--)
           {
              if (strcmp(&pcgDayArray1[ilMinWeek][ilI][0],"        ") != 0)
              {
                 ilMinDay = ilI;
              }
           }
           ilMaxDay = 1;
           for (ilI = 1; ilI <= 7; ilI++)
           {
              if (strcmp(&pcgDayArray1[ilMaxWeek][ilI][0],"        ") != 0)
              {
                 ilMaxDay = ilI;
              }
           }
           for (ilI = ilMinDay-1; ilI >= 1; ilI--)
           {
              pcgDayArray2[ilMinWeek][ilI] = pcgDayArray2[ilMinWeek+1][ilI];
           }
           for (ilI = ilMaxDay+1; ilI <= 7; ilI++)
           {
              pcgDayArray2[ilMaxWeek][ilI] = pcgDayArray2[ilMaxWeek-1][ilI];
           }
           ilMinWeek = ilMaxWeek + 2;
           while (ilMinWeek < igMaxWeek &&
                  strcmp(&pcgDayArray2[ilMinWeek][0]," 0000000") == 0)
           {
              ilMinWeek++;
           }
           ilMaxWeek = ilMinWeek + 1;
        }
        else
        {
           ilMaxWeek++;
        }
     }
  }
/*
  dbg(TRACE,"Week: %d - %d , Day: %d %d",igMinWeek,igMaxWeek,igMinDay,igMaxDay);
  for (ilI = igMinWeek; ilI <= igMaxWeek; ilI++)
  {
     dbg(TRACE,"%s %s %s %s %s %s %s",
         &pcgUrnoArray[ilI][1][0],
         &pcgUrnoArray[ilI][2][0],
         &pcgUrnoArray[ilI][3][0],
         &pcgUrnoArray[ilI][4][0],
         &pcgUrnoArray[ilI][5][0],
         &pcgUrnoArray[ilI][6][0],
         &pcgUrnoArray[ilI][7][0]);
  }
  for (ilI = igMinWeek; ilI <= igMaxWeek; ilI++)
  {
     dbg(TRACE,"%s %s %s %s %s %s %s",
         &pcgDayArray1[ilI][1][0],
         &pcgDayArray1[ilI][2][0],
         &pcgDayArray1[ilI][3][0],
         &pcgDayArray1[ilI][4][0],
         &pcgDayArray1[ilI][5][0],
         &pcgDayArray1[ilI][6][0],
         &pcgDayArray1[ilI][7][0]);
  }
  for (ilI = igMinWeek; ilI <= igMaxWeek; ilI++)
  {
     dbg(TRACE,"%s",&pcgDayArray2[ilI][0]);
  }
*/

  return ilRC;
} /* End of GenerateDayArraysSSI */


static int GenerateDayArraysSSR(FLIGHT_RECORD_SSR *pcpFlight, char *pcpType, int ipNo)
{
  int ilRC = RC_SUCCESS;
  int ilI;
  int ilJ;
  char pclTime[32];
  int ilTime;
  int ilWeek;
  int ilDay;
  int ilMinWeek;
  int ilMaxWeek;
  int ilMinDay;
  int ilMaxDay;
  int ilDays;

  for (ilI = 0; ilI < 52; ilI++)
  {
     for (ilJ = 1; ilJ <= 7; ilJ++)
     {
        strcpy(&pcgDayArray1[ilI][ilJ][0],"        ");
        strcpy(&pcgUrnoArray[ilI][ilJ][0]," ");
     }
     strcpy(&pcgDayArray2[ilI][0]," 0000000");
  }

  igMinWeek = 51;
  igMaxWeek = 0;
  for (ilI = 0; ilI < ipNo; ilI++)
  {
     if (strcmp(pcpType,"A") == 0 || strcmp(pcpType,"R") == 0)
     {
        strcpy(pclTime,pcpFlight->pclStoa);
     }
     else
     {
        strcpy(pclTime,pcpFlight->pclStod);
     }
     ilRC = GetMyFullDay(pclTime,&ilDays,&ilTime,&ilDay,&ilWeek);
     if (ilWeek >= 0)
     {
        if (ilWeek <= igMinWeek)
        {
           igMinWeek = ilWeek;
           if (ilDay < igMinDay)
           {
              igMinDay = ilDay;
           }
        }
        if (ilWeek >= igMaxWeek)
        {
           igMaxWeek = ilWeek;
           if (ilDay > igMaxDay)
           {
              igMaxDay = ilDay;
           }
        }
     }
     strncpy(&pcgDayArray1[ilWeek][ilDay][0],pclTime,8);
     pcgDayArray1[ilWeek][ilDay][8] = '\0';
     strcpy(&pcgUrnoArray[ilWeek][ilDay][0],"#");
     strcat(&pcgUrnoArray[ilWeek][ilDay][0],pcpFlight->pclUrna);
     strcat(&pcgUrnoArray[ilWeek][ilDay][0],"#");
     strcat(&pcgUrnoArray[ilWeek][ilDay][0],pcpFlight->pclUrnd);
     pcpFlight++;
  }
  igMinDay = 7;
  igMaxDay = 1;
  for (ilI = 7; ilI >= 1; ilI--)
  {
     if (strcmp(&pcgDayArray1[igMinWeek][ilI][0],"        ") != 0)
     {
        igMinDay = ilI;
     }
  }
  for (ilI = 1; ilI <= 7; ilI++)
  {
     if (strcmp(&pcgDayArray1[igMaxWeek][ilI][0],"        ") != 0)
     {
        igMaxDay = ilI;
     }
  }

  for (ilI = igMinWeek; ilI <= igMaxWeek; ilI++)
  {
     for (ilJ = 1; ilJ <= 7; ilJ++)
     {
        if (strcmp(&pcgDayArray1[ilI][ilJ][0],"        ") != 0)
        {
           pcgDayArray2[ilI][ilJ] = 0x31;
        }
     }
  }
  if (igMaxWeek > igMinWeek)
  {
     ilMinWeek = igMinWeek;
     ilMaxWeek = ilMinWeek + 1;
     while (ilMaxWeek <= igMaxWeek + 1)
     {
        if (strcmp(&pcgDayArray2[ilMaxWeek][0]," 0000000") == 0)
        {
           ilMaxWeek--;
           ilMinDay = 7;
           for (ilI = 7; ilI >= 1; ilI--)
           {
              if (strcmp(&pcgDayArray1[ilMinWeek][ilI][0],"        ") != 0)
              {
                 ilMinDay = ilI;
              }
           }
           ilMaxDay = 1;
           for (ilI = 1; ilI <= 7; ilI++)
           {
              if (strcmp(&pcgDayArray1[ilMaxWeek][ilI][0],"        ") != 0)
              {
                 ilMaxDay = ilI;
              }
           }
           for (ilI = ilMinDay-1; ilI >= 1; ilI--)
           {
              pcgDayArray2[ilMinWeek][ilI] = pcgDayArray2[ilMinWeek+1][ilI];
           }
           for (ilI = ilMaxDay+1; ilI <= 7; ilI++)
           {
              pcgDayArray2[ilMaxWeek][ilI] = pcgDayArray2[ilMaxWeek-1][ilI];
           }
           ilMinWeek = ilMaxWeek + 2;
           while (ilMinWeek < igMaxWeek &&
                  strcmp(&pcgDayArray2[ilMinWeek][0]," 0000000") == 0)
           {
              ilMinWeek++;
           }
           ilMaxWeek = ilMinWeek + 1;
        }
        else
        {
           ilMaxWeek++;
        }
     }
  }
/*
  dbg(TRACE,"Week: %d - %d , Day: %d %d",igMinWeek,igMaxWeek,igMinDay,igMaxDay);
  for (ilI = igMinWeek; ilI <= igMaxWeek; ilI++)
  {
     dbg(TRACE,"%s %s %s %s %s %s %s",
         &pcgUrnoArray[ilI][1][0],
         &pcgUrnoArray[ilI][2][0],
         &pcgUrnoArray[ilI][3][0],
         &pcgUrnoArray[ilI][4][0],
         &pcgUrnoArray[ilI][5][0],
         &pcgUrnoArray[ilI][6][0],
         &pcgUrnoArray[ilI][7][0]);
  }
  for (ilI = igMinWeek; ilI <= igMaxWeek; ilI++)
  {
     dbg(TRACE,"%s %s %s %s %s %s %s",
         &pcgDayArray1[ilI][1][0],
         &pcgDayArray1[ilI][2][0],
         &pcgDayArray1[ilI][3][0],
         &pcgDayArray1[ilI][4][0],
         &pcgDayArray1[ilI][5][0],
         &pcgDayArray1[ilI][6][0],
         &pcgDayArray1[ilI][7][0]);
  }
  for (ilI = igMinWeek; ilI <= igMaxWeek; ilI++)
  {
     dbg(TRACE,"%s",&pcgDayArray2[ilI][0]);
  }
*/

  return ilRC;
} /* End of GenerateDayArraysSSR */


static int GenerateDoop()
{
  int ilRC = RC_SUCCESS;
  int ilI;
  int ilJ;
  int ilFirstWeek;
  int ilLastWeek;
  int ilEqualWeek;
  int ilMinDay;

  for (ilI = 0; ilI < 52; ilI++)
  {
     strcpy(&pcgUrnoList[ilI][0],"");
  }
  igNoDoop = 0;
  if (igMaxWeek >= igMinWeek)
  {
     if (igMaxWeek == igMinWeek)
     {
        strcpy(&pcgVpfr[igNoDoop][0],&pcgDayArray1[igMinWeek][igMinDay][0]);
        strcpy(&pcgVpto[igNoDoop][0],&pcgDayArray1[igMaxWeek][igMaxDay][0]);
        strcpy(&pcgDoop[igNoDoop][0],"0000000");
        igFreq[igNoDoop] = 1;
        for (ilI = igMinDay; ilI <= igMaxDay; ilI++)
        {
           if (strcmp(&pcgDayArray1[igMinWeek][ilI][0],"        ") != 0)
           {
              pcgDoop[igNoDoop][ilI-1] = ilI + 0x30;
              strcat(&pcgUrnoList[igNoDoop][0],&pcgUrnoArray[igMinWeek][ilI][0]);
           }
        }
        if (strcmp(&pcgDoop[igNoDoop][0],"0000000") != 0)
        {
           igNoDoop++;
        }
     }
     else
     {
        strcpy(&pcgVpfr[igNoDoop][0],&pcgDayArray1[igMinWeek][igMinDay][0]);
        strcpy(&pcgVpto[igNoDoop][0],&pcgDayArray1[igMaxWeek][igMaxDay][0]);
        ilFirstWeek = igMinWeek;
        ilLastWeek = igMinWeek;
        ilEqualWeek = TRUE;
        while (ilFirstWeek <= igMaxWeek)
        {
           igFreq[igNoDoop] = 1;
           ilI = ilFirstWeek+1;
           while (ilI <= igMaxWeek && strcmp(&pcgDayArray2[ilI][0]," 0000000") == 0)
           {
              ilI++;
           }
           if (strcmp(&pcgDayArray2[ilFirstWeek][0],&pcgDayArray2[ilI][0]) == 0)
           {
              igFreq[igNoDoop] = ilI - ilFirstWeek;
           }
           if (igFreq[igNoDoop] > 9)
           {
              igFreq[igNoDoop] = 1;
           }
           while (ilEqualWeek == TRUE && ilLastWeek <= igMaxWeek - igFreq[igNoDoop])
           {
              if (strcmp(&pcgDayArray2[ilFirstWeek][0],
                         &pcgDayArray2[ilLastWeek+igFreq[igNoDoop]][0]) != 0)
              {
                 ilEqualWeek = FALSE;
              }
              if (ilLastWeek + igFreq[igNoDoop] <= igMaxWeek)
              {
                 if (ilEqualWeek == TRUE)
                 {
                    for (ilI = ilLastWeek+igFreq[igNoDoop]+1;
                         ilI < ilLastWeek+(2*igFreq[igNoDoop]);
                         ilI++)
                    {
                       if (strcmp(&pcgDayArray2[ilI][0]," 0000000") != 0)
                       {
                          ilEqualWeek = FALSE;
                       }
                    } 
                 }
              }
              if (ilEqualWeek == TRUE)
              {
                 ilLastWeek += igFreq[igNoDoop];
              }
           } /* End of WHILE */
           if (igFreq[igNoDoop] > 1 && ilFirstWeek == ilLastWeek)
           {
              igFreq[igNoDoop] = 1;
           }
           strcpy(&pcgDoop[igNoDoop][0],"0000000");
           for (ilI = ilFirstWeek; ilI <= ilLastWeek; ilI++)
           {
              for (ilJ = 1; ilJ <= 7; ilJ++)
              {
                 if (strcmp(&pcgDayArray1[ilI][ilJ][0],"        ") != 0)
                 {
                    pcgDoop[igNoDoop][ilJ-1] = ilJ + 0x30;
                    strcpy(&pcgVpto[igNoDoop][0],&pcgDayArray1[ilI][ilJ][0]);
                    strcat(&pcgUrnoList[igNoDoop][0],&pcgUrnoArray[ilI][ilJ][0]);
                    strcpy(&pcgDayArray1[ilI][ilJ][0],"        ");
                    pcgDayArray2[ilI][ilJ] = 0x30;
                 }
              }
           }
           if (strcmp(&pcgDoop[igNoDoop][0],"0000000") != 0)
           {
              igNoDoop++;
           }
           ilFirstWeek = ilLastWeek + 1;
           while (ilFirstWeek < igMaxWeek &&
                  strcmp(&pcgDayArray2[ilFirstWeek][0],"        ") == 0)
           {
              ilFirstWeek++;
           }
           ilLastWeek = ilFirstWeek;
           ilEqualWeek = TRUE;
           if (ilFirstWeek <= igMaxWeek)
           {
              if (ilFirstWeek < igMaxWeek)
              {
                 ilMinDay = 0;
                 for (ilI = 7; ilI >= 1; ilI--)
                 {
                    if (pcgDayArray2[ilFirstWeek][ilI] == 0x31)
                    {
                       ilMinDay = ilI;
                    }
                 }
                 for (ilI = ilMinDay-1; ilI >= 1; ilI--)
                 {
                    pcgDayArray2[ilFirstWeek][ilI] = pcgDayArray2[ilFirstWeek+1][ilI];
                 }
              }
              for (ilI = 7; ilI >= 1; ilI--)
              {
                 if (strcmp(&pcgDayArray1[ilFirstWeek][ilI][0],"        ") != 0)
                 {
                    strcpy(&pcgVpfr[igNoDoop][0],&pcgDayArray1[ilFirstWeek][ilI][0]);
                 }
              }
           }
        } /* End of WHILE */
     } /* End of ELSE */
  }

/*
  for (ilI = 0; ilI < igNoDoop; ilI++)
  {
    dbg(TRACE,"%s %s %s %d %s",
        &pcgVpfr[ilI][0],&pcgVpto[ilI][0],&pcgDoop[ilI][0],igFreq[ilI],&pcgUrnoList[ilI][0]);
  }
*/

  return ilRC;
}


static int InsSSITAB(char *pcpCurrentSeason, char *pcpCurFlno, FLIGHT_RECORD_SSI *pcpFlight,
                     int ipCommit)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[XXL_BUFF];
  char pclDataBuf[XXL_BUFF];
  char pclSqlBufFields[L_BUFF];
  char pclSqlBufValues[XXL_BUFF];
  char pclDataList[XXL_BUFF];
  char pclNewUrno[32];
  char pclNewStoa[32];
  char pclNewStod[32];
  int ilI;
  int ilJ;
  char pclUrn1[2048];
  char pclUrn2[2048];

  strcpy(pclSqlBufFields,"URNO,HOPO,SEAS,VAFR,VATO,DOOP,FREQ,TTYP,ACT3,ACT5,FLNO,ALC2,ALC3,FLTN,FLNS,JCNT,JFNO,STOA,STOD,ORG3,ORG4,DES3,DES4,VIA3,VIA4,VIAN,VIAL,ADID,URL1,URL2");

  /* Mei */
  igNextUrno = NewUrnos( "SSITAB", igNoDoop );
  if( igNextUrno == RC_FAIL || igNextUrno == DB_ERROR )
      return;

  for (ilI = 0; ilI < igNoDoop; ilI++)
  {
     sprintf(pclNewUrno,"%d",igNextUrno);
     igNextUrno++;
     strcpy(pclNewStoa,&pcpFlight->pclStoa[8]);
     strcpy(pclNewStod,&pcpFlight->pclStod[8]);
     pclNewStoa[4] = '\0';
     pclNewStod[4] = '\0';
     if (strlen(&pcgUrnoList[ilI][0]) > 2000)
     {
        memset(pclUrn1,0x00,2048);
        memset(pclUrn2,0x00,2048);
        for (ilJ = 1999; ilJ > 0; ilJ--)
        {
           if (pcgUrnoList[ilI][ilJ] == '#')
           {
              strncpy(pclUrn1,&pcgUrnoList[ilI][0],ilJ);
              strcpy(pclUrn2,&pcgUrnoList[ilI][ilJ]);
              ilJ = -1;
           }
        }
     }
     else
     {
        strcpy(pclUrn1,&pcgUrnoList[ilI][0]);
        strcpy(pclUrn2," ");
     }
     sprintf(pclSqlBufValues,"%s,'%s','%s','%s','%s','%s','%d','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'",
             pclNewUrno,pcgHomeAp,pcpCurrentSeason,&pcgVpfr[ilI][0],&pcgVpto[ilI][0],
             &pcgDoop[ilI],igFreq[ilI],
             pcpFlight->pclTtyp,
             pcpFlight->pclAct3,
             pcpFlight->pclAct5,
             pcpCurFlno,
             pcpFlight->pclAlc2,
             pcpFlight->pclAlc3,
             pcpFlight->pclFltn,
             pcpFlight->pclFlns,
             pcpFlight->pclJcnt,
             pcpFlight->pclJfno,
             pclNewStoa,
             pclNewStod,
             pcpFlight->pclOrg3,
             pcpFlight->pclOrg4,
             pcpFlight->pclDes3,
             pcpFlight->pclDes4,
             pcpFlight->pclVia3,
             pcpFlight->pclVia4,
             pcpFlight->pclVian,
             pcpFlight->pclVial,
             pcpFlight->pclAdid,
             pclUrn1,
             pclUrn2);
     sprintf(pclSqlBuf,"INSERT INTO SSITAB FIELDS(%s) VALUES(%s)",
             pclSqlBufFields,pclSqlBufValues);
     sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
             pclNewUrno,pcgHomeAp,pcpCurrentSeason,&pcgVpfr[ilI][0],&pcgVpto[ilI][0],
             &pcgDoop[ilI],igFreq[ilI],
             pcpFlight->pclTtyp,
             pcpFlight->pclAct3,
             pcpFlight->pclAct5,
             pcpCurFlno,
             pcpFlight->pclAlc2,
             pcpFlight->pclAlc3,
             pcpFlight->pclFltn,
             pcpFlight->pclFlns,
             pcpFlight->pclJcnt,
             pcpFlight->pclJfno,
             pclNewStoa,
             pclNewStod,
             pcpFlight->pclOrg3,
             pcpFlight->pclOrg4,
             pcpFlight->pclDes3,
             pcpFlight->pclDes4,
             pcpFlight->pclVia3,
             pcpFlight->pclVia4,
             pcpFlight->pclVian,
             pcpFlight->pclVial,
             pcpFlight->pclAdid,
             pclUrn1,
             pclUrn2);
     if (igUseArrayInsert == TRUE)
     {
        StrgPutStrg(pcgSsiBuf,&igArrayPtr,pclDataList,0,-1,"\n");
     }
     else
     {
        slCursor = 0;
        slFkt = START;
        dbg(DEBUG,"InsSSITAB: <%s>",pclSqlBuf);
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        if (ilRCdb == DB_SUCCESS)
        {
           if (ipCommit == TRUE)
           {
              commit_work();
           }
           ilRC = TriggerBchdlAction("IRT","SSITAB",pclNewUrno,pclSqlBufFields,
                                     pclDataList,TRUE,TRUE);
        }
        close_my_cursor(&slCursor);
     }
  }

  return ilRC;
} /* End of InsSSITAB */
static int InsSSITAB_STYP(char *pcpCurrentSeason, char *pcpCurFlno, FLIGHT_RECORD_SSI *pcpFlight,
                     int ipCommit)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[XXL_BUFF];
  char pclDataBuf[XXL_BUFF];
  char pclSqlBufFields[L_BUFF];
  char pclSqlBufValues[XXL_BUFF];
  char pclDataList[XXL_BUFF];
  char pclNewUrno[32];
  char pclNewStoa[32];
  char pclNewStod[32];
  int ilI;
  int ilJ;
  char pclUrn1[2048];
  char pclUrn2[2048];

  strcpy(pclSqlBufFields,"URNO,HOPO,SEAS,VAFR,VATO,DOOP,FREQ,TTYP,ACT3,ACT5,FLNO,ALC2,ALC3,FLTN,FLNS,JCNT,JFNO,STOA,STOD,ORG3,ORG4,DES3,DES4,VIA3,VIA4,VIAN,VIAL,ADID,URL1,URL2,STYP");

  /* Mei */
  igNextUrno = NewUrnos( "SSITAB", igNoDoop );
  if( igNextUrno == RC_FAIL || igNextUrno == DB_ERROR )
      return;

  for (ilI = 0; ilI < igNoDoop; ilI++)
  {
     sprintf(pclNewUrno,"%d",igNextUrno);
     igNextUrno++;
     strcpy(pclNewStoa,&pcpFlight->pclStoa[8]);
     strcpy(pclNewStod,&pcpFlight->pclStod[8]);
     pclNewStoa[4] = '\0';
     pclNewStod[4] = '\0';
     if (strlen(&pcgUrnoList[ilI][0]) > 2000)
     {
        memset(pclUrn1,0x00,2048);
        memset(pclUrn2,0x00,2048);
        for (ilJ = 1999; ilJ > 0; ilJ--)
        {
           if (pcgUrnoList[ilI][ilJ] == '#')
           {
              strncpy(pclUrn1,&pcgUrnoList[ilI][0],ilJ);
              strcpy(pclUrn2,&pcgUrnoList[ilI][ilJ]);
              ilJ = -1;
           }
        }
     }
     else
     {
        strcpy(pclUrn1,&pcgUrnoList[ilI][0]);
        strcpy(pclUrn2," ");
     }
     sprintf(pclSqlBufValues,"%s,'%s','%s','%s','%s','%s','%d','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'",
             pclNewUrno,pcgHomeAp,pcpCurrentSeason,&pcgVpfr[ilI][0],&pcgVpto[ilI][0],
             &pcgDoop[ilI],igFreq[ilI],
             pcpFlight->pclTtyp,
             pcpFlight->pclAct3,
             pcpFlight->pclAct5,
             pcpCurFlno,
             pcpFlight->pclAlc2,
             pcpFlight->pclAlc3,
             pcpFlight->pclFltn,
             pcpFlight->pclFlns,
             pcpFlight->pclJcnt,
             pcpFlight->pclJfno,
             pclNewStoa,
             pclNewStod,
             pcpFlight->pclOrg3,
             pcpFlight->pclOrg4,
             pcpFlight->pclDes3,
             pcpFlight->pclDes4,
             pcpFlight->pclVia3,
             pcpFlight->pclVia4,
             pcpFlight->pclVian,
             pcpFlight->pclVial,
             pcpFlight->pclAdid,
             pclUrn1,
             pclUrn2,
             pcpFlight->pclStyp);
     sprintf(pclSqlBuf,"INSERT INTO SSITAB FIELDS(%s) VALUES(%s)",
             pclSqlBufFields,pclSqlBufValues);
     sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
             pclNewUrno,pcgHomeAp,pcpCurrentSeason,&pcgVpfr[ilI][0],&pcgVpto[ilI][0],
             &pcgDoop[ilI],igFreq[ilI],
             pcpFlight->pclTtyp,
             pcpFlight->pclAct3,
             pcpFlight->pclAct5,
             pcpCurFlno,
             pcpFlight->pclAlc2,
             pcpFlight->pclAlc3,
             pcpFlight->pclFltn,
             pcpFlight->pclFlns,
             pcpFlight->pclJcnt,
             pcpFlight->pclJfno,
             pclNewStoa,
             pclNewStod,
             pcpFlight->pclOrg3,
             pcpFlight->pclOrg4,
             pcpFlight->pclDes3,
             pcpFlight->pclDes4,
             pcpFlight->pclVia3,
             pcpFlight->pclVia4,
             pcpFlight->pclVian,
             pcpFlight->pclVial,
             pcpFlight->pclAdid,
             pclUrn1,
             pclUrn2,
             pcpFlight->pclStyp);
     if (igUseArrayInsert == TRUE)
     {
        StrgPutStrg(pcgSsiBuf,&igArrayPtr,pclDataList,0,-1,"\n");
     }
     else
     {
        slCursor = 0;
        slFkt = START;
        dbg(DEBUG,"InsSSITAB: <%s>",pclSqlBuf);
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        if (ilRCdb == DB_SUCCESS)
        {
           if (ipCommit == TRUE)
           {
              commit_work();
           }
           ilRC = TriggerBchdlAction("IRT","SSITAB",pclNewUrno,pclSqlBufFields,
                                     pclDataList,TRUE,TRUE);
        }
        close_my_cursor(&slCursor);
     }
  }

  return ilRC;
} /* End of InsSSITAB_STYP */
static int InsSSRTAB(char *pcpCurrentSeason, FLIGHT_RECORD_SSR *pcpFlight, int ipCommit)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[XXXL_BUFF];
  char pclDataBuf[XXXL_BUFF];
  char pclSqlBufFields[XL_BUFF];
  char pclSqlBufValues[XXXL_BUFF];
  char pclDataList[XXXL_BUFF];
  char pclNewUrno[32];
  char pclNewStoa[32];
  char pclNewStod[32];
  char pclNewStda[32];
  char pclNewStad[32];
  int ilI;
  int ilJ;
  int ilK;
  char pclUrn1[2048];
  char pclUrn2[2048];
  char pclUrn3[2048];

  strcpy(pclSqlBufFields,"URNO,HOPO,SEAS,VAFR,VATO,DOOP,FREQ,NSTP,ACT3,ACT5,FTYP,FLTA,AL2A,AL3A,FLNA,FLSA,JCNA,JFNA,STOA,STDA,ORG3,ORG4,VI3A,VI4A,VINA,VILA,TTPA,FLTD,AL2D,AL3D,FLND,FLSD,JCND,JFND,STOD,STAD,DES3,DES4,VI3D,VI4D,VIND,VILD,TTPD,URL1,URL2,URL3");

  /* Mei */
  igNextUrno = NewUrnos( "SSRTAB", igNoDoop );
  if( igNextUrno == RC_FAIL || igNextUrno == DB_ERROR )
      return;

  for (ilI = 0; ilI < igNoDoop; ilI++)
  {
     sprintf(pclNewUrno,"%d",igNextUrno);
     igNextUrno++;
     strcpy(pclNewStoa,&pcpFlight->pclStoa[8]);
     strcpy(pclNewStod,&pcpFlight->pclStod[8]);
     strcpy(pclNewStda,&pcpFlight->pclStda[8]);
     strcpy(pclNewStad,&pcpFlight->pclStad[8]);
     pclNewStoa[4] = '\0';
     pclNewStod[4] = '\0';
     pclNewStda[4] = '\0';
     pclNewStad[4] = '\0';
     if (strlen(&pcgUrnoList[ilI][0]) > 2000)
     {
        memset(pclUrn1,0x00,2048);
        memset(pclUrn2,0x00,2048);
        memset(pclUrn3,0x00,2048);
        for (ilJ = 1999; ilJ > 0; ilJ--)
        {
           if (pcgUrnoList[ilI][ilJ] == '#')
           {
              strncpy(pclUrn1,&pcgUrnoList[ilI][0],ilJ);
              if (strlen(&pcgUrnoList[ilI][0]) > 3900)
              {
                 for (ilK = 3900; ilK > 0; ilK--)
                 {
                    if (pcgUrnoList[ilI][ilK] == '#')
                    {
                       strncpy(pclUrn2,&pcgUrnoList[ilI][ilJ],ilK-ilJ);
                       strcpy(pclUrn3,&pcgUrnoList[ilI][ilK]);
                       ilK = -1;
                    }
                 }
              }
              else
              {
                 strcpy(pclUrn2,&pcgUrnoList[ilI][ilJ]);
                 strcpy(pclUrn3," ");
              }
              ilJ = -1;
           }
        }
     }
     else
     {
        strcpy(pclUrn1,&pcgUrnoList[ilI][0]);
        strcpy(pclUrn2," ");
        strcpy(pclUrn3," ");
     }
     sprintf(pclSqlBufValues,"%s,'%s','%s','%s','%s','%s','%d','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'",
             pclNewUrno,pcgHomeAp,pcpCurrentSeason,&pcgVpfr[ilI][0],&pcgVpto[ilI][0],
             &pcgDoop[ilI],igFreq[ilI],
             pcpFlight->pclNstp,
             pcpFlight->pclAct3,
             pcpFlight->pclAct5,
             pcpFlight->pclFtyp,
             pcpFlight->pclFlta,
             pcpFlight->pclAl2a,
             pcpFlight->pclAl3a,
             pcpFlight->pclFlna,
             pcpFlight->pclFlsa,
             pcpFlight->pclJcna,
             pcpFlight->pclJfna,
             pclNewStoa,
             pclNewStda,
             pcpFlight->pclOrg3,
             pcpFlight->pclOrg4,
             pcpFlight->pclVi3a,
             pcpFlight->pclVi4a,
             pcpFlight->pclVina,
             pcpFlight->pclVila,
             pcpFlight->pclTtpa,
             pcpFlight->pclFltd,
             pcpFlight->pclAl2d,
             pcpFlight->pclAl3d,
             pcpFlight->pclFlnd,
             pcpFlight->pclFlsd,
             pcpFlight->pclJcnd,
             pcpFlight->pclJfnd,
             pclNewStod,
             pclNewStad,
             pcpFlight->pclDes3,
             pcpFlight->pclDes4,
             pcpFlight->pclVi3d,
             pcpFlight->pclVi4d,
             pcpFlight->pclVind,
             pcpFlight->pclVild,
             pcpFlight->pclTtpd,
             pclUrn1,
             pclUrn2,
             pclUrn3);
     sprintf(pclSqlBuf,"INSERT INTO SSRTAB FIELDS(%s) VALUES(%s)",
             pclSqlBufFields,pclSqlBufValues);
     sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
             pclNewUrno,pcgHomeAp,pcpCurrentSeason,&pcgVpfr[ilI][0],&pcgVpto[ilI][0],
             &pcgDoop[ilI],igFreq[ilI],
             pcpFlight->pclNstp,
             pcpFlight->pclAct3,
             pcpFlight->pclAct5,
             pcpFlight->pclFtyp,
             pcpFlight->pclFlta,
             pcpFlight->pclAl2a,
             pcpFlight->pclAl3a,
             pcpFlight->pclFlna,
             pcpFlight->pclFlsa,
             pcpFlight->pclJcna,
             pcpFlight->pclJfna,
             pclNewStoa,
             pclNewStda,
             pcpFlight->pclOrg3,
             pcpFlight->pclOrg4,
             pcpFlight->pclVi3a,
             pcpFlight->pclVi4a,
             pcpFlight->pclVina,
             pcpFlight->pclVila,
             pcpFlight->pclTtpa,
             pcpFlight->pclFltd,
             pcpFlight->pclAl2d,
             pcpFlight->pclAl3d,
             pcpFlight->pclFlnd,
             pcpFlight->pclFlsd,
             pcpFlight->pclJcnd,
             pcpFlight->pclJfnd,
             pclNewStod,
             pclNewStad,
             pcpFlight->pclDes3,
             pcpFlight->pclDes4,
             pcpFlight->pclVi3d,
             pcpFlight->pclVi4d,
             pcpFlight->pclVind,
             pcpFlight->pclVild,
             pcpFlight->pclTtpd,
             pclUrn1,
             pclUrn2,
             pclUrn3);
     if (igUseArrayInsert == TRUE)
     {
        StrgPutStrg(pcgSsiBuf,&igArrayPtr,pclDataList,0,-1,"\n");
     }
     else
     {
        slCursor = 0;
        slFkt = START;
        dbg(DEBUG,"InsSSRTAB: <%s>",pclSqlBuf);
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        if (ilRCdb == DB_SUCCESS)
        {
           if (ipCommit == TRUE)
           {
              commit_work();
           }
           ilRC = TriggerBchdlAction("IRT","SSRTAB",pclNewUrno,pclSqlBufFields,
                                     pclDataList,TRUE,TRUE);
        }
        close_my_cursor(&slCursor);
     }
  }

  return ilRC;
} /* End of InsSSRTAB */
static int InsSSRTAB_STYAD(char *pcpCurrentSeason, FLIGHT_RECORD_SSR *pcpFlight, int ipCommit)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[XXXL_BUFF];
  char pclDataBuf[XXXL_BUFF];
  char pclSqlBufFields[XL_BUFF];
  char pclSqlBufValues[XXXL_BUFF];
  char pclDataList[XXXL_BUFF];
  char pclNewUrno[32];
  char pclNewStoa[32];
  char pclNewStod[32];
  char pclNewStda[32];
  char pclNewStad[32];
  int ilI;
  int ilJ;
  int ilK;
  char pclUrn1[2048];
  char pclUrn2[2048];
  char pclUrn3[2048];

  strcpy(pclSqlBufFields,"URNO,HOPO,SEAS,VAFR,VATO,DOOP,FREQ,NSTP,ACT3,ACT5,FTYP,FLTA,AL2A,AL3A,FLNA,FLSA,JCNA,JFNA,STOA,STDA,ORG3,ORG4,VI3A,VI4A,VINA,VILA,TTPA,FLTD,AL2D,AL3D,FLND,FLSD,JCND,JFND,STOD,STAD,DES3,DES4,VI3D,VI4D,VIND,VILD,TTPD,URL1,URL2,URL3,STYA,STYD");

  /* Mei */
  igNextUrno = NewUrnos( "SSRTAB", igNoDoop );
  if( igNextUrno == RC_FAIL || igNextUrno == DB_ERROR )
      return;

  for (ilI = 0; ilI < igNoDoop; ilI++)
  {
     sprintf(pclNewUrno,"%d",igNextUrno);
     igNextUrno++;
     strcpy(pclNewStoa,&pcpFlight->pclStoa[8]);
     strcpy(pclNewStod,&pcpFlight->pclStod[8]);
     strcpy(pclNewStda,&pcpFlight->pclStda[8]);
     strcpy(pclNewStad,&pcpFlight->pclStad[8]);
     pclNewStoa[4] = '\0';
     pclNewStod[4] = '\0';
     pclNewStda[4] = '\0';
     pclNewStad[4] = '\0';
     if (strlen(&pcgUrnoList[ilI][0]) > 2000)
     {
        memset(pclUrn1,0x00,2048);
        memset(pclUrn2,0x00,2048);
        memset(pclUrn3,0x00,2048);
        for (ilJ = 1999; ilJ > 0; ilJ--)
        {
           if (pcgUrnoList[ilI][ilJ] == '#')
           {
              strncpy(pclUrn1,&pcgUrnoList[ilI][0],ilJ);
              if (strlen(&pcgUrnoList[ilI][0]) > 3900)
              {
                 for (ilK = 3900; ilK > 0; ilK--)
                 {
                    if (pcgUrnoList[ilI][ilK] == '#')
                    {
                       strncpy(pclUrn2,&pcgUrnoList[ilI][ilJ],ilK-ilJ);
                       strcpy(pclUrn3,&pcgUrnoList[ilI][ilK]);
                       ilK = -1;
                    }
                 }
              }
              else
              {
                 strcpy(pclUrn2,&pcgUrnoList[ilI][ilJ]);
                 strcpy(pclUrn3," ");
              }
              ilJ = -1;
           }
        }
     }
     else
     {
        strcpy(pclUrn1,&pcgUrnoList[ilI][0]);
        strcpy(pclUrn2," ");
        strcpy(pclUrn3," ");
     }
     sprintf(pclSqlBufValues,"%s,'%s','%s','%s','%s','%s','%d','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'",
             pclNewUrno,pcgHomeAp,pcpCurrentSeason,&pcgVpfr[ilI][0],&pcgVpto[ilI][0],
             &pcgDoop[ilI],igFreq[ilI],
             pcpFlight->pclNstp,
             pcpFlight->pclAct3,
             pcpFlight->pclAct5,
             pcpFlight->pclFtyp,
             pcpFlight->pclFlta,
             pcpFlight->pclAl2a,
             pcpFlight->pclAl3a,
             pcpFlight->pclFlna,
             pcpFlight->pclFlsa,
             pcpFlight->pclJcna,
             pcpFlight->pclJfna,
             pclNewStoa,
             pclNewStda,
             pcpFlight->pclOrg3,
             pcpFlight->pclOrg4,
             pcpFlight->pclVi3a,
             pcpFlight->pclVi4a,
             pcpFlight->pclVina,
             pcpFlight->pclVila,
             pcpFlight->pclTtpa,
             pcpFlight->pclFltd,
             pcpFlight->pclAl2d,
             pcpFlight->pclAl3d,
             pcpFlight->pclFlnd,
             pcpFlight->pclFlsd,
             pcpFlight->pclJcnd,
             pcpFlight->pclJfnd,
             pclNewStod,
             pclNewStad,
             pcpFlight->pclDes3,
             pcpFlight->pclDes4,
             pcpFlight->pclVi3d,
             pcpFlight->pclVi4d,
             pcpFlight->pclVind,
             pcpFlight->pclVild,
             pcpFlight->pclTtpd,
             pclUrn1,
             pclUrn2,
             pclUrn3,
             pcpFlight->pclStya,
             pcpFlight->pclStyd);
     sprintf(pclSqlBuf,"INSERT INTO SSRTAB FIELDS(%s) VALUES(%s)",
             pclSqlBufFields,pclSqlBufValues);
     //sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
     sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",        
             pclNewUrno,pcgHomeAp,pcpCurrentSeason,&pcgVpfr[ilI][0],&pcgVpto[ilI][0],
             &pcgDoop[ilI],igFreq[ilI],
             pcpFlight->pclNstp,
             pcpFlight->pclAct3,
             pcpFlight->pclAct5,
             pcpFlight->pclFtyp,
             pcpFlight->pclFlta,
             pcpFlight->pclAl2a,
             pcpFlight->pclAl3a,
             pcpFlight->pclFlna,
             pcpFlight->pclFlsa,
             pcpFlight->pclJcna,
             pcpFlight->pclJfna,
             pclNewStoa,
             pclNewStda,
             pcpFlight->pclOrg3,
             pcpFlight->pclOrg4,
             pcpFlight->pclVi3a,
             pcpFlight->pclVi4a,
             pcpFlight->pclVina,
             pcpFlight->pclVila,
             pcpFlight->pclTtpa,
             pcpFlight->pclFltd,
             pcpFlight->pclAl2d,
             pcpFlight->pclAl3d,
             pcpFlight->pclFlnd,
             pcpFlight->pclFlsd,
             pcpFlight->pclJcnd,
             pcpFlight->pclJfnd,
             pclNewStod,
             pclNewStad,
             pcpFlight->pclDes3,
             pcpFlight->pclDes4,
             pcpFlight->pclVi3d,
             pcpFlight->pclVi4d,
             pcpFlight->pclVind,
             pcpFlight->pclVild,
             pcpFlight->pclTtpd,
             pclUrn1,
             pclUrn2,
             pclUrn3,
             pcpFlight->pclStya,
             pcpFlight->pclStyd);
     if (igUseArrayInsert == TRUE)
     {
        StrgPutStrg(pcgSsiBuf,&igArrayPtr,pclDataList,0,-1,"\n");
     }
     else
     {
        slCursor = 0;
        slFkt = START;
        dbg(DEBUG,"InsSSRTAB: <%s>",pclSqlBuf);
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        if (ilRCdb == DB_SUCCESS)
        {
           if (ipCommit == TRUE)
           {
              commit_work();
           }
           ilRC = TriggerBchdlAction("IRT","SSRTAB",pclNewUrno,pclSqlBufFields,
                                     pclDataList,TRUE,TRUE);
        }
        close_my_cursor(&slCursor);
     }
  }

  return ilRC;
} /* End of InsSSRTAB */


static int SortRot(FLIGHT_RECORD_SSR *pcpRotFlight, int ipRotCount, char *pcpMode)
{
  int ilRC = RC_SUCCESS;
  int ilI;

/*
dbg(TRACE,"Before Sort:");
for (ilI = 0; ilI < ipRotCount; ilI++)
{
dbg(TRACE,"<%s><%s><%s><%s><%s><%s><%s><%s><%s><%s>",
&pcpRotFlight[ilI].pclFlta[0],
&pcpRotFlight[ilI].pclTtpa[0],
&pcpRotFlight[ilI].pclJfna[0],
&pcpRotFlight[ilI].pclOrg3[0],
&pcpRotFlight[ilI].pclStoa[8],
&pcpRotFlight[ilI].pclFltd[0],
&pcpRotFlight[ilI].pclTtpd[0],
&pcpRotFlight[ilI].pclJfnd[0],
&pcpRotFlight[ilI].pclDes3[0],
&pcpRotFlight[ilI].pclStod[8]);
}
*/

  if (strcmp(pcpMode,"DA") == 0)
  {
     ilRC = SortRotField(pcpRotFlight,ipRotCount,
                         pcpRotFlight->pclFtyp,pcpRotFlight->pclFlta);
     ilRC = SortRotField(pcpRotFlight,ipRotCount,
                         pcpRotFlight->pclFlta,&pcpRotFlight->pclStoa[8]);
     ilRC = SortRotField(pcpRotFlight,ipRotCount,
                         &pcpRotFlight->pclStoa[8],pcpRotFlight->pclOrg3);
     ilRC = SortRotField(pcpRotFlight,ipRotCount,
                         pcpRotFlight->pclOrg3,pcpRotFlight->pclJfna);
     ilRC = SortRotField(pcpRotFlight,ipRotCount,
                         pcpRotFlight->pclJfna,pcpRotFlight->pclTtpa);
     ilRC = SortRotField(pcpRotFlight,ipRotCount,
                         pcpRotFlight->pclTtpa,pcpRotFlight->pclVi3a);
/*
     ilRC = SortRotField(pcpRotFlight,ipRotCount,
                         pcpRotFlight->pclVi3a,&pcpRotFlight->pclStda[8]);
*/
  }

  ilRC = SortRotField(pcpRotFlight,ipRotCount,
                      &pcpRotFlight->pclStoa[8],pcpRotFlight->pclFltd);
  ilRC = SortRotField(pcpRotFlight,ipRotCount,
                      pcpRotFlight->pclFltd,&pcpRotFlight->pclStod[8]);
  ilRC = SortRotField(pcpRotFlight,ipRotCount,
                      &pcpRotFlight->pclStod[8],pcpRotFlight->pclDes3);
  ilRC = SortRotField(pcpRotFlight,ipRotCount,
                      pcpRotFlight->pclDes3,pcpRotFlight->pclJfnd);
  ilRC = SortRotField(pcpRotFlight,ipRotCount,
                      pcpRotFlight->pclJfnd,pcpRotFlight->pclTtpd);
  ilRC = SortRotField(pcpRotFlight,ipRotCount,
                      pcpRotFlight->pclTtpd,pcpRotFlight->pclVi3d);
/*
  ilRC = SortRotField(pcpRotFlight,ipRotCount,
                      pcpRotFlight->pclVi3d,&pcpRotFlight->pclStad[8]);
  ilRC = SortRotField(pcpRotFlight,ipRotCount,
                      &pcpRotFlight->pclStad[8],pcpRotFlight->pclNstp);
*/
  ilRC = SortRotField(pcpRotFlight,ipRotCount,
                      pcpRotFlight->pclVi3d,pcpRotFlight->pclNstp);

/*
dbg(TRACE,"After Sort:");
for (ilI = 0; ilI < ipRotCount; ilI++)
{
dbg(TRACE,"<%s><%s><%s><%s><%s><%s><%s><%s><%s><%s>",
&pcpRotFlight[ilI].pclFlta[0],
&pcpRotFlight[ilI].pclTtpa[0],
&pcpRotFlight[ilI].pclJfna[0],
&pcpRotFlight[ilI].pclOrg3[0],
&pcpRotFlight[ilI].pclStoa[8],
&pcpRotFlight[ilI].pclFltd[0],
&pcpRotFlight[ilI].pclTtpd[0],
&pcpRotFlight[ilI].pclJfnd[0],
&pcpRotFlight[ilI].pclDes3[0],
&pcpRotFlight[ilI].pclStod[8]);
}
*/

  return ilRC;
} /* End of SortRot */


static int SortRotField(FLIGHT_RECORD_SSR *pcpRotFlight, int ipRotCount,
                        char *pcpField1, char *pcpField2)
{
  int ilRC = RC_SUCCESS;
  int ilI;
  int ilJ;
  int ilK;
  FLIGHT_RECORD_SSR pclSaveFlight;
  FLIGHT_RECORD_SSR *pclField1;
  FLIGHT_RECORD_SSR *pclField2;

  pclField1 = (FLIGHT_RECORD_SSR *)pcpField1;
  pclField2 = (FLIGHT_RECORD_SSR *)pcpField2;

  for (ilI = 1; ilI < ipRotCount; ilI++)
  {
     ilJ = ilI -1;
     while (ilJ >= 0 &&
            strcmp(&pclField1[ilI].pclNstp[0],&pclField1[ilJ].pclNstp[0]) == 0 &&
            strcmp(&pclField2[ilI].pclNstp[0],&pclField2[ilJ].pclNstp[0]) < 0)
     {
        ilJ--;
     }
     if (ilJ < ilI - 1)
     {
        memcpy((char *)&pclSaveFlight.pclNstp[0],
               (char *)&pcpRotFlight[ilI].pclNstp[0],FLIGHT_RECORD_SIZE_SSR);
        for (ilK = ilI - 1; ilK >= ilJ + 1; ilK--)
        {
           memcpy((char *)&pcpRotFlight[ilK+1].pclNstp[0],
                  (char *)&pcpRotFlight[ilK].pclNstp[0],FLIGHT_RECORD_SIZE_SSR);
        }
        memcpy((char *)&pcpRotFlight[ilJ+1].pclNstp[0],
               (char *)&pclSaveFlight.pclNstp[0],FLIGHT_RECORD_SIZE_SSR);
     }
  }

  return ilRC;
} /* End of SortRotField */


static int UpdateSSI(char *pcpUrno, char *pcpFields, char *pcpData)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  int ilRCdbRd = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  short slFktRd;
  short slCursorRd;
  char pclSqlBufRd[L_BUFF];
  char pclSelectBufRd[L_BUFF];
  char pclDataBufRd[XXL_BUFF];
  int ilLen;
  char pclFirstDay[32];
  char pclLastDay[32];
  char pclCurFlno[32];
  char pclFlno[32];
  char pclSeas[32];
  int ilItemNo;
  char pclStoa[32];
  char pclStod[32];
  char pclAdid[32];
  char pclTime[32];
  int ilDelRec = FALSE;
  int ilRecExists = FALSE;
  int ilTime;
  int ilDay;
  char pclDay[8];
  char *pclStr;
  char pclSsiDoop[8] = "";
  char pclSsiTtyp[8] = "";
  char pclSsiAct3[8] = "";
  char pclSsiJfno[128] = "";
  char pclSsiStoa[8] = "";
  char pclSsiStod[8] = "";
  char pclSsiOrg3[8] = "";
  char pclSsiDes3[8] = "";
  char pclSsiVia3[8] = "";
  char pclSsiVial[260] = "";
  char pclSsiFlno[16] = "";
  FLIGHT_RECORD_SSI pclFlight;
  char pclTmpBuf[16];
  int ilRecFound;
  int ilRecFoundTotal;
  int ilDays;
  int ilMins;
  int ilWeekDay;

  if (igHandleSSI == FALSE)
  {
     dbg(TRACE,"SSITAB is not supported!!!");
     return ilRC;
  }
  sprintf(pclSqlBuf,"SELECT FLNO,SEAS,STOA,STOD,ADID FROM AFTTAB WHERE URNO = %s",pcpUrno);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);
  if (ilRCdb != DB_SUCCESS)
  {
     ilDelRec = TRUE;
     ilItemNo = get_item_no(pcpFields,"FLNO",5) + 1;
     if (ilItemNo > 0)
     {
        ilLen = get_real_item(pclFlno,pcpData,ilItemNo);
     }
     else
     {
        dbg(TRACE,"Configuration Error: FLNO missing!!!!");
        return RC_FAIL;
     } 
     ilItemNo = get_item_no(pcpFields,"STOA",5) + 1;
     if (ilItemNo > 0)
     {
        ilLen = get_real_item(pclStoa,pcpData,ilItemNo);
     }
     else
     {
        dbg(TRACE,"Configuration Error: STOA missing!!!!");
        return RC_FAIL;
     } 
     ilItemNo = get_item_no(pcpFields,"STOD",5) + 1;
     if (ilItemNo > 0)
     {
        ilLen = get_real_item(pclStod,pcpData,ilItemNo);
     }
     else
     {
        dbg(TRACE,"Configuration Error: STOD missing!!!!");
        return RC_FAIL;
     } 
     ilItemNo = get_item_no(pcpFields,"ADID",5) + 1;
     if (ilItemNo > 0)
     {
        ilLen = get_real_item(pclAdid,pcpData,ilItemNo);
     }
     else
     {
        dbg(TRACE,"Configuration Error: ADID missing!!!!");
        return RC_FAIL;
     } 
     if (strcmp(pclAdid,"A") == 0)
     {
        strcpy(pclTime,pclStoa);
     }
     else
     {
        if (strcmp(pclAdid,"D") == 0)
        {
           strcpy(pclTime,pclStod);
        }
        else
        {
           dbg(DEBUG,"UpdateSSI: Wrong ADID <%s>",pclAdid);
           return ilRC;
        }
     }
     pclTime[8] = '\0';
     sprintf(pclSqlBuf,
             "SELECT SEAS FROM SSITAB WHERE FLNO = '%s' AND VAFR <= '%s' AND VATO >= '%s'",
             pclFlno,pclTime,pclTime);
     slCursor = 0;
     slFkt = START;
     /* dbg(TRACE,"<%s>",pclSqlBuf); */
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     close_my_cursor(&slCursor);
     if (ilRCdb == DB_SUCCESS)
     {
        strcpy(pclSeas,pclDataBuf);
        TrimRight(pclSeas);
     }
     else
     {
        dbg(DEBUG,"UpdateSSI: SEAS not found in SSITAB for flight <%s>",pclFlno);
        return ilRC;
     }
  }
  else
  {
     BuildItemBuffer(pclDataBuf,"",5,",");
     ilLen = get_real_item(pclFlno,pclDataBuf,1);
     ilLen = get_real_item(pclSeas,pclDataBuf,2);
     ilLen = get_real_item(pclStoa,pclDataBuf,3);
     ilLen = get_real_item(pclStod,pclDataBuf,4);
     ilLen = get_real_item(pclAdid,pclDataBuf,5);
     if (strcmp(pclAdid,"A") == 0)
     {
        strcpy(pclTime,pclStoa);
     }
     else
     {
        if (strcmp(pclAdid,"D") == 0)
        {
           strcpy(pclTime,pclStod);
        }
        else
        {
           dbg(DEBUG,"UpdateSSI: Wrong ADID <%s>",pclAdid);
           return ilRC;
        }
     }
     pclTime[8] = '\0';
  }

  dbg(TRACE,"Update SSI for flight <%s>",pclFlno);

  ilRecFound = 0;
  ilRecFoundTotal = 0;
  strcpy(pclSqlBuf,"SELECT DOOP,TTYP,ACT3,JFNO,STOA,STOD,ORG3,DES3,VIA3,VIAL,FLNO FROM SSITAB ");
  sprintf(pclSelectBuf,
          "WHERE URL1 LIKE '%%%s%%' OR URL2 LIKE '%%%s%%'",
          pcpUrno,pcpUrno);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  if (ilRCdb != DB_SUCCESS)
  {
     if (ilDelRec == TRUE)
     {
        dbg(DEBUG,"Deleted flight does not exist in SSITAB");
        close_my_cursor(&slCursor);
        return ilRC;
     }
  }
  else
  {
     strcat(pclTime,"120000");
     ilRC = GetFullDay(pclTime,&ilDays,&ilTime,&ilDay);
     sprintf(pclDay,"%d",ilDay);
     if (ilDelRec == FALSE)
     {   /* Read AFT record */
        strcpy(pclSqlBufRd,"SELECT TTYP,ACT3,ACT5,ALC2,ALC3,FLTN,FLNS,JCNT,JFNO,STOA,STOD,ORG3,ORG4,DES3,DES4,VIA3,VIA4,VIAN,VIAL,ADID,URNO FROM AFTTAB ");
        sprintf(pclSelectBufRd,"WHERE URNO = %s",pcpUrno);
        strcat(pclSqlBufRd,pclSelectBufRd);
        slCursorRd = 0;
        slFktRd = START;
        /* dbg(TRACE,"<%s>",pclSqlBufRd); */
        ilRCdbRd = sql_if(slFktRd,&slCursorRd,pclSqlBufRd,pclDataBufRd);
        if (ilRCdbRd == DB_SUCCESS)
        {
           BuildItemBuffer(pclDataBufRd,"",21,",");
           ilRC = GetFlightDataSSI(&pclFlight,pclDataBufRd);
           if (pclFlight.pclStoa[0] == ' ')
           {
              strcpy(pclFlight.pclStoa," ");
           }
           else
           {
              strncpy(pclTmpBuf,&pclFlight.pclStoa[8],4);
              pclTmpBuf[4] = '\0';
              strcpy(pclFlight.pclStoa,pclTmpBuf);
           }
           if (pclFlight.pclStod[0] == ' ')
           {
              strcpy(pclFlight.pclStod," ");
           }
           else
           {
              strncpy(pclTmpBuf,&pclFlight.pclStod[8],4);
              pclTmpBuf[4] = '\0';
              strcpy(pclFlight.pclStod,pclTmpBuf);
           }
        }
        close_my_cursor(&slCursorRd);
     }
     if (ilRCdb == DB_SUCCESS)
     {
        BuildItemBuffer(pclDataBuf,"",11,",");
        ilLen = get_real_item(pclSsiDoop,pclDataBuf,1);
        pclStr = strstr(pclSsiDoop,pclDay);
        if (pclStr != NULL)
        {
          ilRecExists = TRUE;
          ilRecFoundTotal++;
          ilLen = get_real_item(pclSsiTtyp,pclDataBuf,2);
          TrimRight(pclSsiTtyp); 
          ilLen = get_real_item(pclSsiAct3,pclDataBuf,3);
          TrimRight(pclSsiAct3); 
          ilLen = get_real_item(pclSsiJfno,pclDataBuf,4);
          TrimRight(pclSsiJfno); 
          ilLen = get_real_item(pclSsiStoa,pclDataBuf,5);
          TrimRight(pclSsiStoa); 
          ilLen = get_real_item(pclSsiStod,pclDataBuf,6);
          TrimRight(pclSsiStod); 
          ilLen = get_real_item(pclSsiOrg3,pclDataBuf,7);
          TrimRight(pclSsiOrg3); 
          ilLen = get_real_item(pclSsiDes3,pclDataBuf,8);
          TrimRight(pclSsiDes3); 
          ilLen = get_real_item(pclSsiVia3,pclDataBuf,9);
          TrimRight(pclSsiVia3); 
          ilLen = get_real_item(pclSsiVial,pclDataBuf,10);
          TrimRight(pclSsiVial); 
          ilLen = get_real_item(pclSsiFlno,pclDataBuf,11);
          TrimRight(pclSsiFlno); 
          if (ilDelRec == FALSE)
          {   /* Compare SSI with AFT record */
             if (strcmp(pclFlight.pclTtyp,pclSsiTtyp) == 0 &&
                 strcmp(pclFlight.pclAct3,pclSsiAct3) == 0 &&
                 strcmp(pclFlight.pclJfno,pclSsiJfno) == 0 &&
                 strcmp(pclFlight.pclStoa,pclSsiStoa) == 0 &&
                 strcmp(pclFlight.pclStod,pclSsiStod) == 0 &&
                 strcmp(pclFlight.pclOrg3,pclSsiOrg3) == 0 &&
                 strcmp(pclFlight.pclDes3,pclSsiDes3) == 0 &&
                 strcmp(pclFlight.pclVia3,pclSsiVia3) == 0 &&
                 strcmp(pclFlight.pclVial,pclSsiVial) == 0 &&
                 strcmp(pclFlno,pclSsiFlno) == 0)
             {
                ilRecFound++;
             }
          }
        }
     }
  }
  close_my_cursor(&slCursor);
  if (ilRecExists == FALSE && ilDelRec == TRUE)
  {
     dbg(DEBUG,"Deleted flight does not exist in SSITAB");
     return ilRC;
  }
  if (ilRecExists == TRUE && ilRecFound == 1 && ilRecFoundTotal == 1)
  {
     dbg(DEBUG,"Flight is already updated in SSITAB");
     return ilRC;
  }

  if (strstr(pcgSeasonsNotForUpdate,pclSeas) != NULL)
  {
     dbg(DEBUG,"Flight is in season, for which no updates are done");
     return ilRC;
  }

  if (strlen(pclSsiFlno) > 0 && strcmp(pclFlno,pclSsiFlno) != 0)
  {
     sprintf(pclSqlBuf,"DELETE FROM SSITAB WHERE SEAS = '%s' AND FLNO = '%s'",
             pclSeas,pclSsiFlno);
     slCursor = 0;
     slFkt = START;
     dbg(TRACE,"UpdateSSI: <%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     close_my_cursor(&slCursor);
  }
  sprintf(pclSqlBuf,"DELETE FROM SSITAB WHERE SEAS = '%s' AND FLNO = '%s'",
          pclSeas,pclFlno);
  slCursor = 0;
  slFkt = START;
  dbg(TRACE,"UpdateSSI: <%s>",pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);
  sprintf(pclSqlBuf,"SELECT VPFR,VPTO FROM SEATAB WHERE SEAS = '%s'",pclSeas);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);
  if (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",2,",");
     ilLen = get_real_item(pclFirstDay,pclDataBuf,1);
     ilLen = get_real_item(pclLastDay,pclDataBuf,2);
     ilRC = GetFullDay(pclFirstDay,&ilDays,&ilMins,&ilWeekDay);
     igFirstDay = ilDays - ilWeekDay + 1;
     if (strlen(pclSsiFlno) > 0 && strcmp(pclFlno,pclSsiFlno) != 0)
     {
        ilRC = GenerateFlightSSI(pclSeas,pclSsiFlno,pclFirstDay,pclLastDay,FALSE);
     }
     ilRC = GenerateFlightSSI(pclSeas,pclFlno,pclFirstDay,pclLastDay,FALSE);
  }
  commit_work();

  return ilRC;
} /* End of UpdateSSI */


static int UpdateSSR(char *pcpUrno, char *pcpFields, char *pcpData)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  int ilRCdbRd = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  short slFktRd;
  short slCursorRd;
  char pclSqlBufRd[L_BUFF];
  char pclSelectBufRd[L_BUFF];
  char pclDataBufRd[XXL_BUFF];
  short slFktRdR;
  short slCursorRdR;
  char pclSqlBufRdR[L_BUFF];
  char pclSelectBufRdR[L_BUFF];
  char pclDataBufRdR[XXL_BUFF];
  int ilLen;
  char pclFirstDay[32];
  char pclLastDay[32];
  char pclCurFlno[32];
  char pclFlno[32];
  char pclSeas[32];
  int ilItemNo;
  char pclStoa[32];
  char pclStod[32];
  char pclAdid[32];
  char pclTime[32];
  char pclFltListArr[100][32];
  char pclFltListDep[100][32];
  int ilI;
  int ilDelRec = FALSE;
  int ilRecExists = FALSE;
  char pclRtyp[32];
  char pclRkey[32];
  int ilFound;
  int  ilTime;
  int ilDay;
  char pclDay[8];
  char *pclStr;
  char pclSsrDoop[8] = " ";
  char pclSsrAct3[8] = " ";
  char pclSsrFlta[16] = " ";
  char pclSsrJfna[128] = " ";
  char pclSsrStoa[8] = " ";
  char pclSsrStda[8] = " ";
  char pclSsrOrg3[8] = " ";
  char pclSsrVi3a[8] = " ";
  char pclSsrVila[260] = " ";
  char pclSsrTtpa[8] = " ";
  char pclSsrFltd[16] = " ";
  char pclSsrJfnd[128] = " ";
  char pclSsrStod[8] = " ";
  char pclSsrStad[8] = " ";
  char pclSsrDes3[8] = " ";
  char pclSsrVi3d[8] = " ";
  char pclSsrVild[260] = " ";
  char pclSsrTtpd[8] = " ";
  FLIGHT_RECORD_SSR pclFlight;
  char pclRotFlno[32];
  char pclTmpBuf[16];
  int ilRecFound;
  int ilRecFoundTotal;
  int ilDays;
  int ilMins;
  int ilWeekDay;

  if (igHandleSSR == FALSE)
  {
     dbg(TRACE,"SSRTAB is not supported!!!");
     return ilRC;
  }
  sprintf(pclSqlBuf,"SELECT FLNO,SEAS,STOA,STOD,ADID,RTYP,RKEY FROM AFTTAB WHERE URNO = %s",pcpUrno);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);
  if (ilRCdb != DB_SUCCESS)
  {
     ilDelRec = TRUE;
     ilItemNo = get_item_no(pcpFields,"FLNO",5) + 1;
     if (ilItemNo > 0)
     {
        ilLen = get_real_item(pclFlno,pcpData,ilItemNo);
     }
     else
     {
        dbg(TRACE,"Configuration Error: FLNO missing!!!!");
        return RC_FAIL;
     } 
     ilItemNo = get_item_no(pcpFields,"STOA",5) + 1;
     if (ilItemNo > 0)
     {
        ilLen = get_real_item(pclStoa,pcpData,ilItemNo);
     }
     else
     {
        dbg(TRACE,"Configuration Error: STOA missing!!!!");
        return RC_FAIL;
     } 
     ilItemNo = get_item_no(pcpFields,"STOD",5) + 1;
     if (ilItemNo > 0)
     {
        ilLen = get_real_item(pclStod,pcpData,ilItemNo);
     }
     else
     {
        dbg(TRACE,"Configuration Error: STOD missing!!!!");
        return RC_FAIL;
     } 
     ilItemNo = get_item_no(pcpFields,"ADID",5) + 1;
     if (ilItemNo > 0)
     {
        ilLen = get_real_item(pclAdid,pcpData,ilItemNo);
     }
     else
     {
        dbg(TRACE,"Configuration Error: ADID missing!!!!");
        return RC_FAIL;
     } 
     if (strcmp(pclAdid,"A") == 0)
     {
        strcpy(pclTime,pclStoa);
        pclTime[8] = '\0';
        sprintf(pclSqlBuf,
                "SELECT SEAS FROM SSRTAB WHERE FLTA = '%s' AND VAFR <= '%s' AND VATO >= '%s'",
                pclFlno,pclTime,pclTime);
     }
     else
     {
        if (strcmp(pclAdid,"D") == 0)
        {
           strcpy(pclTime,pclStod);
           pclTime[8] = '\0';
           sprintf(pclSqlBuf,
                "SELECT SEAS FROM SSRTAB WHERE FLTD = '%s' AND VAFR <= '%s' AND VATO >= '%s'",
                pclFlno,pclTime,pclTime);
        }
        else
        {
           dbg(DEBUG,"UpdateSSR: Wrong ADID <%s>",pclAdid);
           return ilRC;
        }
     }
     slCursor = 0;
     slFkt = START;
     /* dbg(TRACE,"<%s>",pclSqlBuf); */
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     close_my_cursor(&slCursor);
     if (ilRCdb == DB_SUCCESS)
     {
        strcpy(pclSeas,pclDataBuf);
        TrimRight(pclSeas);
     }
     else
     {
        dbg(DEBUG,"UpdateSSR: SEAS not found in SSRTAB for flight <%s>",pclFlno);
        return ilRC;
     }
  }
  else
  {
     BuildItemBuffer(pclDataBuf,"",7,",");
     ilLen = get_real_item(pclFlno,pclDataBuf,1);
     TrimRight(pclFlno);
     ilLen = get_real_item(pclSeas,pclDataBuf,2);
     TrimRight(pclSeas);
     ilLen = get_real_item(pclStoa,pclDataBuf,3);
     TrimRight(pclStoa);
     ilLen = get_real_item(pclStod,pclDataBuf,4);
     TrimRight(pclStod);
     ilLen = get_real_item(pclAdid,pclDataBuf,5);
     TrimRight(pclAdid);
     ilLen = get_real_item(pclRtyp,pclDataBuf,6);
     TrimRight(pclRtyp);
     ilLen = get_real_item(pclRkey,pclDataBuf,7);
     TrimRight(pclRkey);
     if (strcmp(pclAdid,"A") == 0)
     {
        strcpy(pclTime,pclStoa);
     }
     else
     {
        if (strcmp(pclAdid,"D") == 0)
        {
           strcpy(pclTime,pclStod);
        }
        else
        {
           dbg(DEBUG,"UpdateSSR: Wrong ADID <%s>",pclAdid);
           return ilRC;
        }
     }
     pclTime[8] = '\0';
  }

  dbg(TRACE,"Update SSR for flight <%s>",pclFlno);

  /* Begin of checking */
  ilRecFound = 0;
  ilRecFoundTotal = 0;
  strcpy(pclSqlBuf,"SELECT DOOP,ACT3,FLTA,JFNA,STOA,STDA,ORG3,VI3A,VILA,TTPA,FLTD,JFND,STOD,STAD,DES3,VI3D,VILD,TTPD FROM SSRTAB ");
  sprintf(pclSelectBuf,
          "WHERE URL1 LIKE '%%%s%%' OR URL2 LIKE '%%%s%%' OR URL3 LIKE '%%%s%%'",
          pcpUrno,pcpUrno,pcpUrno);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  if (ilRCdb != DB_SUCCESS)
  {
     if (ilDelRec == TRUE)
     {
        dbg(DEBUG,"Deleted flight does not exist in SSRTAB");
        close_my_cursor(&slCursor);
        return ilRC;
     }
  }
  strcpy(pclFlight.pclFlta," ");
  strcpy(pclFlight.pclFltd," ");
   
  strcat(pclTime,"120000");
  ilRC = GetFullDay(pclTime,&ilDays,&ilTime,&ilDay);
  sprintf(pclDay,"%d",ilDay);
  if (ilDelRec == FALSE)
  {   /* Read AFT record */
     if (strcmp(pclAdid,"A") == 0)
     {
        strcpy(pclSqlBufRd,"SELECT ACT3,ACT5,ALC2,ALC3,FLTN,FLNS,JCNT,JFNO,STOA,STOD,ORG3,ORG4,VIA3,VIA4,VIAN,VIAL,TTYP,URNO,FLNO,RTYP,RKEY FROM AFTTAB ");
     }
     else
     {
        strcpy(pclSqlBufRd,"SELECT ACT3,ACT5,ALC2,ALC3,FLTN,FLNS,JCNT,JFNO,STOD,STOA,DES3,DES4,VIA3,VIA4,VIAN,VIAL,TTYP,URNO,FLNO,RTYP,RKEY FROM AFTTAB ");
     }
     sprintf(pclSelectBufRd,"WHERE URNO = %s",pcpUrno);
     strcat(pclSqlBufRd,pclSelectBufRd);
     slCursorRd = 0;
     slFktRd = START;
     /* dbg(TRACE,"<%s>",pclSqlBufRd); */
     ilRCdbRd = sql_if(slFktRd,&slCursorRd,pclSqlBufRd,pclDataBufRd);
     close_my_cursor(&slCursorRd);
     if (ilRCdbRd == DB_SUCCESS)
     {
        BuildItemBuffer(pclDataBufRd,"",21,",");
        ilLen = get_real_item(pclRkey,pclDataBufRd,21);
        if (strcmp(pclAdid,"A") == 0)
        {
           strcpy(pclSqlBufRdR,"SELECT ACT3,ACT5,ALC2,ALC3,FLTN,FLNS,JCNT,JFNO,STOD,STOA,DES3,DES4,VIA3,VIA4,VIAN,VIAL,TTYP,URNO,FLNO FROM AFTTAB ");
           sprintf(pclSelectBufRdR,"WHERE RKEY = %s AND ADID = 'D'",pclRkey);
        }
        else
        {
           strcpy(pclSqlBufRdR,"SELECT ACT3,ACT5,ALC2,ALC3,FLTN,FLNS,JCNT,JFNO,STOA,STOD,ORG3,ORG4,VIA3,VIA4,VIAN,VIAL,TTYP,URNO,FLNO FROM AFTTAB ");
           sprintf(pclSelectBufRdR,"WHERE RKEY = %s AND ADID = 'A'",pclRkey);
        }
        strcat(pclSqlBufRdR,pclSelectBufRdR);
        slCursorRdR = 0;
        slFktRdR = START;
        /* dbg(TRACE,"<%s>",pclSqlBufRdR); */
        ilRCdbRd = sql_if(slFktRdR,&slCursorRdR,pclSqlBufRdR,pclDataBufRdR);
        close_my_cursor(&slCursorRdR);
        if (ilRCdbRd == DB_SUCCESS)
        {
           BuildItemBuffer(pclDataBufRdR,"",19,",");
           ilLen = get_real_item(pclRotFlno,pclDataBufRdR,19);
           if (ilLen == 0)
           {
              strcpy(pclRotFlno," ");
           }
           if (strcmp(pclAdid,"A") == 0)
           {
              ilRC = GetFlightDataSSR(&pclFlight,pclDataBufRd,pclDataBufRdR);
              strcpy(pclFlight.pclFlta,pclFlno);
              strcpy(pclFlight.pclFltd,pclRotFlno);
           }
           else
           {
              ilRC = GetFlightDataSSR(&pclFlight,pclDataBufRdR,pclDataBufRd);
              strcpy(pclFlight.pclFlta,pclRotFlno);
              strcpy(pclFlight.pclFltd,pclFlno);
           }
        }
        else
        {
           if (strcmp(pclAdid,"A") == 0)
           {
              ilRC = GetFlightDataSSR(&pclFlight,pclDataBufRd,NULL);
              strcpy(pclFlight.pclFlta,pclFlno);
              strcpy(pclFlight.pclFltd," ");
           }
           else
           {
              ilRC = GetFlightDataSSR(&pclFlight,NULL,pclDataBufRd);
              strcpy(pclFlight.pclFlta," ");
              strcpy(pclFlight.pclFltd,pclFlno);
           }
        }
        if (pclFlight.pclStoa[0] == ' ')
        {
           strcpy(pclFlight.pclStoa," ");
        }
        else
        {
           strncpy(pclTmpBuf,&pclFlight.pclStoa[8],4);
           pclTmpBuf[4] = '\0';
           strcpy(pclFlight.pclStoa,pclTmpBuf);
        }
        if (pclFlight.pclStod[0] == ' ')
        {
           strcpy(pclFlight.pclStod," ");
        }
        else
        {
           strncpy(pclTmpBuf,&pclFlight.pclStod[8],4);
           pclTmpBuf[4] = '\0';
           strcpy(pclFlight.pclStod,pclTmpBuf);
        }
        if (pclFlight.pclStda[0] == ' ')
        {
           strcpy(pclFlight.pclStda," ");
        }
        else
        {
           strncpy(pclTmpBuf,&pclFlight.pclStda[8],4);
           pclTmpBuf[4] = '\0';
           strcpy(pclFlight.pclStda,pclTmpBuf);
        }
        if (pclFlight.pclStad[0] == ' ')
        {
        strcpy(pclFlight.pclStad," ");
        }
        else
        {
           strncpy(pclTmpBuf,&pclFlight.pclStad[8],4);
           pclTmpBuf[4] = '\0';
           strcpy(pclFlight.pclStad,pclTmpBuf);
        }
     }
  }
  if (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",18,",");
     ilLen = get_real_item(pclSsrDoop,pclDataBuf,1);
     pclStr = strstr(pclSsrDoop,pclDay);
     if (pclStr != NULL)
     {
        ilRecExists = TRUE;
        ilRecFoundTotal++;
        ilLen = get_real_item(pclSsrAct3,pclDataBuf,2);
        TrimRight(pclSsrAct3); 
        ilLen = get_real_item(pclSsrFlta,pclDataBuf,3);
        TrimRight(pclSsrFlta); 
        ilLen = get_real_item(pclSsrJfna,pclDataBuf,4);
        TrimRight(pclSsrJfna); 
        ilLen = get_real_item(pclSsrStoa,pclDataBuf,5);
        TrimRight(pclSsrStoa); 
        ilLen = get_real_item(pclSsrStda,pclDataBuf,6);
        TrimRight(pclSsrStda); 
        ilLen = get_real_item(pclSsrOrg3,pclDataBuf,7);
        TrimRight(pclSsrOrg3); 
        ilLen = get_real_item(pclSsrVi3a,pclDataBuf,8);
        TrimRight(pclSsrVi3a); 
        ilLen = get_real_item(pclSsrVila,pclDataBuf,9);
        TrimRight(pclSsrVila); 
        ilLen = get_real_item(pclSsrTtpa,pclDataBuf,10);
        TrimRight(pclSsrTtpa); 
        ilLen = get_real_item(pclSsrFltd,pclDataBuf,11);
        TrimRight(pclSsrFltd); 
        ilLen = get_real_item(pclSsrJfnd,pclDataBuf,12);
        TrimRight(pclSsrJfnd); 
        ilLen = get_real_item(pclSsrStod,pclDataBuf,13);
        TrimRight(pclSsrStod); 
        ilLen = get_real_item(pclSsrStad,pclDataBuf,14);
        TrimRight(pclSsrStad); 
        ilLen = get_real_item(pclSsrDes3,pclDataBuf,15);
        TrimRight(pclSsrDes3); 
        ilLen = get_real_item(pclSsrVi3d,pclDataBuf,16);
        TrimRight(pclSsrVi3d); 
        ilLen = get_real_item(pclSsrVild,pclDataBuf,17);
        TrimRight(pclSsrVild); 
        ilLen = get_real_item(pclSsrTtpd,pclDataBuf,18);
        TrimRight(pclSsrTtpd); 
        if (ilDelRec == FALSE)
        {   /* Compare SSI with AFT record */
           if (strcmp(pclFlight.pclAct3,pclSsrAct3) == 0 &&
               strcmp(pclFlight.pclFlta,pclSsrFlta) == 0 &&
               strcmp(pclFlight.pclJfna,pclSsrJfna) == 0 &&
               strcmp(pclFlight.pclStoa,pclSsrStoa) == 0 &&
               strcmp(pclFlight.pclStda,pclSsrStda) == 0 &&
               strcmp(pclFlight.pclOrg3,pclSsrOrg3) == 0 &&
               strcmp(pclFlight.pclVi3a,pclSsrVi3a) == 0 &&
               strcmp(pclFlight.pclVila,pclSsrVila) == 0 &&
               strcmp(pclFlight.pclTtpa,pclSsrTtpa) == 0 &&
               strcmp(pclFlight.pclFltd,pclSsrFltd) == 0 &&
               strcmp(pclFlight.pclJfnd,pclSsrJfnd) == 0 &&
               strcmp(pclFlight.pclStod,pclSsrStod) == 0 &&
               strcmp(pclFlight.pclStad,pclSsrStad) == 0 &&
               strcmp(pclFlight.pclDes3,pclSsrDes3) == 0 &&
               strcmp(pclFlight.pclVi3d,pclSsrVi3d) == 0 &&
               strcmp(pclFlight.pclVild,pclSsrVild) == 0 &&
               strcmp(pclFlight.pclTtpd,pclSsrTtpd) == 0)
           {
              ilRecFound++;
           }
        }
     }
  }
  close_my_cursor(&slCursor);
  if (ilRecExists == FALSE && ilDelRec == TRUE)
  {
     dbg(DEBUG,"Deleted flight does not exist in SSRTAB");
     return ilRC;
  }
  if (ilRecExists == TRUE && ilRecFound == 1 && ilRecFoundTotal == 1)
  {
     dbg(DEBUG,"Flight is already updated in SSRTAB");
     return ilRC;
  }
  /* End of checking */

  sprintf(pclSqlBuf,"SELECT VPFR,VPTO FROM SEATAB WHERE SEAS = '%s'",pclSeas);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);
  if (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",2,",");
     ilLen = get_real_item(pclFirstDay,pclDataBuf,1);
     ilLen = get_real_item(pclLastDay,pclDataBuf,2);
     ilRC = GetFullDay(pclFirstDay,&ilDays,&ilMins,&ilWeekDay);
     igFirstDay = ilDays - ilWeekDay + 1;
  }
  if (strcmp(pclAdid,"A") != 0 && strcmp(pclAdid,"D") != 0)
  {
     return ilRC;
  }

  if (strstr(pcgSeasonsNotForUpdate,pclSeas) != NULL)
  {
     dbg(DEBUG,"Flight is in season, for which no updates are done");
     return ilRC;
  }

  dbg(TRACE,"SSRFlta = <%s> , SSRFltd = <%s>",pclSsrFlta,pclSsrFltd);
  dbg(TRACE,"AFTFlta = <%s> , AFTFltd = <%s>",pclFlight.pclFlta,pclFlight.pclFltd);

  if (strlen(pclSsrFlta) > 1 || strlen(pclSsrFltd) > 1)
  {
     if (strlen(pclSsrFlta) > 1 && strlen(pclSsrFltd) > 1)
     {
        ilRC = DeleteFlightSSR(pclSeas,pclSsrFlta,pclSsrFltd);
        ilRC = DeleteFlightSSR(pclSeas,pclSsrFlta," ");
        ilRC = DeleteFlightSSR(pclSeas," ",pclSsrFltd);
        ilRC = GenerateFlightSSR(pclSeas,pclSsrFlta,pclFirstDay,
                                 pclLastDay,"AD",pclSsrFltd,FALSE);
        ilRC = GenerateFlightSSR(pclSeas,pclSsrFlta,pclFirstDay,
                                 pclLastDay,"A",NULL,FALSE);
        ilRC = GenerateFlightSSR(pclSeas,pclSsrFltd,pclFirstDay,
                                 pclLastDay,"D",NULL,FALSE);
     }
     else
     {
        if (strlen(pclSsrFlta) > 1)
        {
           ilRC = DeleteFlightSSR(pclSeas,pclSsrFlta," ");
           ilRC = GenerateFlightSSR(pclSeas,pclSsrFlta,pclFirstDay,
                                    pclLastDay,"A",NULL,FALSE);
        }
        else
        {
           if (strlen(pclSsrFltd) > 1)
           {
              ilRC = DeleteFlightSSR(pclSeas," ",pclSsrFltd);
              ilRC = GenerateFlightSSR(pclSeas,pclSsrFltd,pclFirstDay,
                                       pclLastDay,"D",NULL,FALSE);
           }
        }
     }
  }
  if (strcmp(pclFlight.pclFlta,pclSsrFlta) != 0 || strcmp(pclFlight.pclFltd,pclSsrFltd) != 0)
  {
     if (strlen(pclFlight.pclFlta) > 1 && strlen(pclFlight.pclFltd) > 1)
     {
        ilRC = DeleteFlightSSR(pclSeas,pclFlight.pclFlta,pclFlight.pclFltd);
        ilRC = DeleteFlightSSR(pclSeas,pclFlight.pclFlta," ");
        ilRC = DeleteFlightSSR(pclSeas," ",pclFlight.pclFltd);
        ilRC = GenerateFlightSSR(pclSeas,pclFlight.pclFlta,pclFirstDay,
                                 pclLastDay,"AD",pclFlight.pclFltd,FALSE);
        ilRC = GenerateFlightSSR(pclSeas,pclFlight.pclFlta,pclFirstDay,
                                 pclLastDay,"A",NULL,FALSE);
        ilRC = GenerateFlightSSR(pclSeas,pclFlight.pclFltd,pclFirstDay,
                                 pclLastDay,"D",NULL,FALSE);
     }
     else
     {
        if (strlen(pclFlight.pclFlta) > 1)
        {
           ilRC = DeleteFlightSSR(pclSeas,pclFlight.pclFlta," ");
           ilRC = GenerateFlightSSR(pclSeas,pclFlight.pclFlta,pclFirstDay,
                                    pclLastDay,"A",NULL,FALSE);
        }
        else
        {
           if (strlen(pclFlight.pclFltd) > 1)
           {
              ilRC = DeleteFlightSSR(pclSeas," ",pclFlight.pclFltd);
              ilRC = GenerateFlightSSR(pclSeas,pclFlight.pclFltd,pclFirstDay,
                                       pclLastDay,"D",NULL,FALSE);
           }
        }
     }
  }

  commit_work();

  return ilRC;
} /* End of UpdateSSR */


static int DeleteFlightSSR(char *pcpSeas, char *pcpArrFlt, char *pcpDepFlt)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclDataBuf[L_BUFF];

  sprintf(pclSqlBuf,
          "DELETE FROM SSRTAB WHERE SEAS = '%s' AND FLTA = '%s' AND FLTD = '%s'",
          pcpSeas,pcpArrFlt,pcpDepFlt);
  slCursor = 0;
  slFkt = START;
  dbg(TRACE,"UpdateSSR: <%s>",pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);

  return ilRC;
} /* End of DeleteFlightSSR */


static int GenerateSSIM(char *pcpSeas)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  char pclFieldBuf[S_BUFF];
  char myFieldBuf[S_BUFF];
  FILE *pflSSIMFilePtr = NULL;
  char pclSSIMFileName[256];
  char pclCurrentTime[32];
  char pclVpfr[32];
  char pclVpto[32];
  int ilLen;
  char pclFlno[16];
  char pclFlnoPrev[16];
  int ilSeq;
  int ilCurLine;
  char pclTmpBuf[128];
  char *pclFunc = "GenerateSSIM";

  dbg(TRACE,"================== Generate SSIM Export File ==================");
  ilRC = TimeToStr(pclCurrentTime,time(NULL));
  if (strlen(pcgSSIMFileName) > 0)
  {
     strcpy(pcgCreatedFileName,pcgSSIMFileName);
     strcat(pcgCreatedFileName,"-");
     strcat(pcgCreatedFileName,pcpSeas);
     strcat(pcgCreatedFileName,"-");
     strcat(pcgCreatedFileName,pclCurrentTime);
  }
  else
  {
     if (*pcpSeas == 'W')
        strcpy(pcgCreatedFileName,"WS");
     else
        strcpy(pcgCreatedFileName,"SS");
     strcpy(pclTmpBuf,&pcpSeas[strlen(pcpSeas)-2]);
     strcat(pcgCreatedFileName,pclTmpBuf);
     if (*pcpSeas == 'W')
     {
        strcat(pcgCreatedFileName,"_");
        sprintf(pclTmpBuf,"%d",atoi(pclTmpBuf)+1);
        if (strlen(pclTmpBuf) == 1)
           strcat(pcgCreatedFileName,"0");
        strcat(pcgCreatedFileName,pclTmpBuf);
     }
  }
  strcat(pcgCreatedFileName,".");
  strcat(pcgCreatedFileName,pcgSSIMFileExt);
  strcpy(pclSSIMFileName,pcgSSIMFilePath);
  strcat(pclSSIMFileName,"/");
  strcat(pclSSIMFileName,pcgCreatedFileName);
  pflSSIMFilePtr = fopen(pclSSIMFileName,"w");
  if (pflSSIMFilePtr == NULL)
  {
     dbg(TRACE,"Could not open file <%s>",pclSSIMFileName);
     return RC_FAIL;
  }
  else
  {
     dbg(TRACE,"FileName: <%s>",pclSSIMFileName);
  }

  fprintf(pflSSIMFilePtr,"%s\n",pcgCreatedFileName);
  /*
  ** SEAS: WInn or SUnn nn: last 2 digits of year
  ** This is the range of a season for the specified year
  */
  sprintf(pclSqlBuf,"SELECT VPFR,VPTO FROM SEATAB WHERE SEAS = '%s'",pcpSeas);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);
  if (ilRCdb != DB_SUCCESS)
  {
     dbg(TRACE,"Season <%s> not found in SEATAB",pcpSeas);
     return RC_FAIL;
  }

  BuildItemBuffer(pclDataBuf,"",2,",");
  ilLen = get_real_item(pclVpfr,pclDataBuf,1);
  ilLen = get_real_item(pclVpto,pclDataBuf,2);

  /* Summer or Winter ? */
  if (strncmp(pclVpfr,pclVpto,4) == 0)
  {
     strcpy(pcgTimeDiffField,"TDIS");
  }
  else
  {
     strcpy(pcgTimeDiffField,"TDIW");
  }

  pcgTiDiHopo[0] = '\0';
  /* Get timediff for selected season(pcgTimeDiffField) from apttab */
  ilRC = GetTimeDiff(pcgTiDiHopo,&igTimeDiff,pcgHomeAp);
  igTimeDiffCount = 0;

  ilRC = GetSSIMHeader(pflSSIMFilePtr,pcpSeas,pclVpfr,pclVpto,pclCurrentTime);

  strcpy(pclFlnoPrev," ");
  ilCurLine = 3;
  sprintf(pclFieldBuf,"FLNO,TTYP,VAFR,VATO,DOOP,FREQ,ORG3,DES3,STOA,STOD,ACT3,VIAN,VIAL,ADID");
  sprintf(myFieldBuf,"FLNO,STYP,VAFR,VATO,DOOP,FREQ,ORG3,DES3,STOA,STOD,ACT3,VIAN,VIAL,ADID");
  if (myEXPORT_WITH_TTYP_OR_STYP == FALSE)
    sprintf(pclSqlBuf,"SELECT %s FROM SSITAB ",pclFieldBuf);
  else
  	sprintf(pclSqlBuf,"SELECT %s FROM SSITAB ",myFieldBuf);
  sprintf(pclSelectBuf,"WHERE SEAS = '%s' ORDER BY FLNO,VAFR,VATO",pcpSeas);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",14,",");
     ilLen = get_real_item(pclFlno,pclDataBuf,1);
     if (strcmp(pclFlno,pclFlnoPrev) == 0)
     {
        ilSeq++;
     }
     else
     {
        ilSeq = 1;
     }
	 dbg( DEBUG, "<%s> <%s>", pclFunc, pclDataBuf );
     ilRC = GetSSIMDataLine(pflSSIMFilePtr,pclFieldBuf,pclDataBuf,&ilCurLine,ilSeq);
     strcpy(pclFlnoPrev,pclFlno);
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);

  ilRC = GetSSIMFooter(pflSSIMFilePtr,pclCurrentTime,ilCurLine);

  fclose(pflSSIMFilePtr);
  dbg(TRACE,"================== SSIM Export File generated =================");

  return ilRC;
} /* End of GenerateSSIM */

static int GenerateSSIMCargoSpot()
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  char pclFieldBuf[S_BUFF];
  char myFieldBuf[S_BUFF];
  FILE *pflSSIMFilePtr = NULL;
  char pclSSIMFileName[256] = "\0";
  char pclSSIMFileNameBak[256] = "\0";
  char pclCurrentTime[32];
  char myct[32] = "\0";
  char pclVpfr[32];
  char pclVpto[32];
  char pcpSeas[32];
  int ilLen;
  char pclFlno[16];
  char pclFlnoPrev[16];
  int ilSeq;
  int ilCurLine;
  char pclTmpBuf[128];
  char myTmpBuf[256] = "\0";
  char *pclFunc = "GenerateSSIMCargoSpot";
  char myCreatedFileName[256] = "\0";
  char myCurrentTimeAddFiveMonths[128]="\0";
  char myCurrentTimeAddFiveMonths_[16]="\0";
  char myCurrentTime_[16]="\0";

  dbg(TRACE,"================== Generate CargoSpot SSIM File ==================");
  dbg(DEBUG,"GenerateSSIMCargoSpot: myCargoSSIMFileName <%s> ",myCargoSSIMFileName);
  dbg(DEBUG,"GenerateSSIMCargoSpot: myCargoSSIMFilePath <%s> ",myCargoSSIMFilePath);
  dbg(DEBUG,"GenerateSSIMCargoSpot: myCargoSSIMFilePathBak <%s> ",myCargoSSIMFilePathBak);
  dbg(DEBUG,"GenerateSSIMCargoSpot: myCargoSSIMFileExt <%s> ",myCargoSSIMFileExt);
  dbg(DEBUG,"GenerateSSIMCargoSpot: myCargoSSIMTimePeriod <%d> months",myCargoSSIMTimePeriod);
  ilRC = TimeToStr(pclCurrentTime,time(NULL));
  if (strlen(myCargoSSIMFilePath) > 0)
  {
     strcpy(myCreatedFileName,myCargoSSIMFileName);
     TimeToStr(myct,time(NULL));
     UtcToLocalTimeFixTZ(myct); // convert to local 
     strcat(myCreatedFileName,myct);
  }
  else
  {
     dbg(TRACE,"GenerateSSIMCargoSpot: myCargoSSIMFilePath <%s> empty",myCargoSSIMFilePath);
     return RC_FAIL;
  }
  strcat(myCreatedFileName,".");
  strcat(myCreatedFileName,myCargoSSIMFileExt);
  strcpy(pclSSIMFileName,myCargoSSIMFilePath);
  strcat(pclSSIMFileName,"/");
  strcat(pclSSIMFileName,myCreatedFileName);
  //for backup folder
  strcpy(pclSSIMFileNameBak,myCargoSSIMFilePathBak);
  strcat(pclSSIMFileNameBak,"/");
  strcat(pclSSIMFileNameBak,myCreatedFileName);
  
  pflSSIMFilePtr = fopen(pclSSIMFileName,"w");
  if (pflSSIMFilePtr == NULL)
  {
     dbg(TRACE,"GenerateSSIMCargoSpot: Could not open file <%s>",pclSSIMFileName);
     return RC_FAIL;
  }
  else
  {
     dbg(TRACE,"GenerateSSIMCargoSpot: FileName: <%s>",pclSSIMFileName);
  }

  //fprintf(pflSSIMFilePtr,"%s\n",myCreatedFileName);
  /*
  ** SEAS: WInn or SUnn nn: last 2 digits of year
  ** This is the range of a season for the specified year
  */
  sprintf(pclSqlBuf,"SELECT VPFR,VPTO,SEAS FROM SEATAB WHERE VPFR <= '%s' AND VPTO >= '%s'",pclCurrentTime,pclCurrentTime);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);
  if (ilRCdb != DB_SUCCESS)
  {
     dbg(TRACE,"Season  not found in SEATAB");
     return RC_FAIL;
  }

  BuildItemBuffer(pclDataBuf,"",3,",");
  ilLen = get_real_item(pclVpfr,pclDataBuf,1);
  ilLen = get_real_item(pclVpto,pclDataBuf,2);
  ilLen = get_real_item(pcpSeas,pclDataBuf,3);

  /* Summer or Winter ? */
  if (strncmp(pclVpfr,pclVpto,4) == 0)
  {
     strcpy(pcgTimeDiffField,"TDIS");
  }
  else
  {
     strcpy(pcgTimeDiffField,"TDIW");
  }

  pcgTiDiHopo[0] = '\0';
  /* Get timediff for selected season(pcgTimeDiffField) from apttab */
  ilRC = GetTimeDiff(pcgTiDiHopo,&igTimeDiff,pcgHomeAp);
  igTimeDiffCount = 0;

  ilRC = GetSSIMHeader(pflSSIMFilePtr,pcpSeas,pclVpfr,pclVpto,pclCurrentTime);

  strcpy(pclFlnoPrev," ");
  ilCurLine = 3;
  /*
  ** select max 5 months flights in ssitab ,from current time
  ** 
  */
  TimeToStr(myCurrentTimeAddFiveMonths,time(NULL) + myCargoSSIMTimePeriod*31*24*60*60);
  dbg(DEBUG,"GenerateSSIMCargoSpot: current time <%s> months before <%s>",pclCurrentTime,myCurrentTimeAddFiveMonths);
  strncpy(myCurrentTime_,pclCurrentTime,8);
  strncpy(myCurrentTimeAddFiveMonths_,myCurrentTimeAddFiveMonths,8);
  dbg(DEBUG,"GenerateSSIMCargoSpot: real current time <%s> real months before <%s>",myCurrentTime_,myCurrentTimeAddFiveMonths_);
  
  sprintf(pclFieldBuf,"FLNO,TTYP,VAFR,VATO,DOOP,FREQ,ORG3,DES3,STOA,STOD,ACT3,VIAN,VIAL,ADID");
  sprintf(myFieldBuf,"FLNO,STYP,VAFR,VATO,DOOP,FREQ,ORG3,DES3,STOA,STOD,ACT3,VIAN,VIAL,ADID");
  if (myEXPORT_WITH_TTYP_OR_STYP == FALSE)
    sprintf(pclSqlBuf,"SELECT %s FROM SSITAB ",pclFieldBuf);
  else
  	sprintf(pclSqlBuf,"SELECT %s FROM SSITAB ",myFieldBuf);
  sprintf(pclSelectBuf,"WHERE (VAFR >= '%s' AND VAFR <= '%s') OR (VATO >= '%s' AND VATO <= '%s') ORDER BY FLNO,VAFR,VATO",
            myCurrentTime_,myCurrentTimeAddFiveMonths_,myCurrentTime_,myCurrentTimeAddFiveMonths_);
  strcat(pclSqlBuf,pclSelectBuf);
  
  dbg(TRACE,"pclSqlBuf<%s>",pclSqlBuf);
  
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",14,",");
     ilLen = get_real_item(pclFlno,pclDataBuf,1);
     if (strcmp(pclFlno,pclFlnoPrev) == 0)
     {
        ilSeq++;
     }
     else
     {
        ilSeq = 1;
     }
	 dbg( DEBUG, "<%s> <%s>", pclFunc, pclDataBuf );
     ilRC = GetSSIMDataLine2(pflSSIMFilePtr,pclFieldBuf,pclDataBuf,&ilCurLine,ilSeq,myCurrentTime_,myCurrentTimeAddFiveMonths_);
     strcpy(pclFlnoPrev,pclFlno);
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);

  ilRC = GetSSIMFooter(pflSSIMFilePtr,pclCurrentTime,ilCurLine);

  fclose(pflSSIMFilePtr);
  
  /* built command... */
  dbg(TRACE,"GenerateSSIMCargoSpot: backup FileName: <%s>",pclSSIMFileNameBak);
	sprintf(myTmpBuf,"cp %s %s", 
					pclSSIMFileName, pclSSIMFileNameBak);
	dbg(DEBUG,"GenerateSSIMCargoSpot: system-string is <%s>", myTmpBuf);
	alarm(90);
	ilRC = system(myTmpBuf);
	alarm(0);
	dbg(DEBUG,"GenerateSSIMCargoSpot: system returns: %d", ilRC);
  dbg(TRACE,"================== CargoSpot SSIM Export File generated =================");

  return ilRC;
} /* End of GenerateSSIMCargoSpot */

static int GetSSIMHeader(FILE *pfpSSIMFilePtr, char *pcpSeas, char *pcpVpfr,
                         char *pcpVpto, char *pcpCurrentTime)
{
  int ilRC = RC_SUCCESS;
  int ilI;
  char pclOutputLine[256];
  char pclBlankLine[256];
  char pclZeroLine[256];
  char pclSeason[8];
  char pclVpfr[8];
  char pclVpto[8];
  char pclDate[8];
  char pclTime[8];
  char pclResult[256];

  strncpy(pclSeason,pcpSeas,3);
  pclSeason[3] = '\0';
  strncpy(pclVpfr,&pcpVpfr[6],2);
  pclVpfr[2] = '\0';
  ilRC = GetMonthAlpha(pclResult,pcpVpfr);
  strcat(pclVpfr,pclResult);
  strncat(pclVpfr,&pcpVpfr[2],2);
  pclVpfr[7] = '\0';
  strncpy(pclVpto,&pcpVpto[6],2);
  pclVpto[2] = '\0';
  ilRC = GetMonthAlpha(pclResult,pcpVpto);
  strcat(pclVpto,pclResult);
  strncat(pclVpto,&pcpVpto[2],2);
  pclVpto[7] = '\0';
  strncpy(pclDate,&pcpCurrentTime[6],2);
  pclDate[2] = '\0';
  ilRC = GetMonthAlpha(pclResult,pcpCurrentTime);
  strcat(pclDate,pclResult);
  strncat(pclDate,&pcpCurrentTime[2],2);
  pclDate[7] = '\0';
  strncpy(pclTime,&pcpCurrentTime[8],4);
  pclTime[4] = '\0';

  strcpy(pclOutputLine,"1AIRLINE STANDARD SCHEDULE DATA SET");
  ilRC = GetCorrectedFormatBlank(pclBlankLine," ",156);
  strcat(pclOutputLine,pclBlankLine);
  strcat(pclOutputLine,"001000001");
  pclOutputLine[40] = '1';
  fprintf(pfpSSIMFilePtr,"%s\n",pclOutputLine);
  ilRC = GetCorrectedFormatZero(pclZeroLine,"0",200);
  for (ilI = 2; ilI <= 5; ilI++)
  {
     fprintf(pfpSSIMFilePtr,"%s\n",pclZeroLine);
  }
  strcpy(pclOutputLine,"2G");
  strcat(pclOutputLine,pcgHomeAp);
  strcat(pclOutputLine," ");
  strcat(pclOutputLine,pcgSSIMVersion);
  strcat(pclOutputLine,pclSeason);
  strcat(pclOutputLine," ");
  strcat(pclOutputLine,pclVpfr);
  strcat(pclOutputLine,pclVpto);
  strcat(pclOutputLine,pclDate);
  strcat(pclOutputLine,"SSIM FILE");
  ilRC = GetCorrectedFormatBlank(pclBlankLine," ",20);
  strcat(pclOutputLine,pclBlankLine);
  strcat(pclOutputLine,pclDate);
  strcat(pclOutputLine,"P");
  strcat(pclOutputLine,pcgSSIMReference);
  ilRC = GetCorrectedFormatBlank(pclBlankLine," ",112);
  strcat(pclOutputLine,pclBlankLine);
  strcat(pclOutputLine,pclTime);
  strcat(pclOutputLine,"000002");
  fprintf(pfpSSIMFilePtr,"%s\n",pclOutputLine);
  for (ilI = 2; ilI <= 5; ilI++)
  {
     fprintf(pfpSSIMFilePtr,"%s\n",pclZeroLine);
  }

  return ilRC;
} /* End of GetSSIMHeader */


static int GetSSIMFooter(FILE *pfpSSIMFilePtr, char *pcpCurrentTime, int ipCurLine)
{
  int ilRC = RC_SUCCESS;
  int ilI;
  char pclOutputLine[256];
  char pclBlankLine[256];
  char pclZeroLine[256];
  char pclDate[8];
  char pclResult[256];
  char pclCurLine[8];
  int ilFillCount;

  strncpy(pclDate,&pcpCurrentTime[6],2);
  pclDate[2] = '\0';
  ilRC = GetMonthAlpha(pclResult,pcpCurrentTime);
  strcat(pclDate,pclResult);
  strncat(pclDate,&pcpCurrentTime[2],2);
  pclDate[7] = '\0';

  ilRC = GetCorrectedFormatZero(pclZeroLine,"0",200);
  ilFillCount = ipCurLine - 3;
  ilI = ilFillCount / 5;
  ilFillCount = ilFillCount - (ilI * 5);
  ilFillCount = 5 - ilFillCount;
  if (ilFillCount < 5)
  {
     for (ilI = 1; ilI <= ilFillCount; ilI++)
     {
        fprintf(pfpSSIMFilePtr,"%s\n",pclZeroLine);
     }
  }
  strcpy(pclOutputLine,"5 ");
  strcat(pclOutputLine,pcgHomeAp);
  strcat(pclOutputLine,pclDate);
  ilRC = GetCorrectedFormatBlank(pclBlankLine," ",175);
  strcat(pclOutputLine,pclBlankLine);
  sprintf(pclCurLine,"%d",ipCurLine);
  ilRC = GetCorrectedFormatZero(pclResult,pclCurLine,6);
  strcat(pclOutputLine,pclResult);
  strcat(pclOutputLine,"E");
  sprintf(pclCurLine,"%d",ipCurLine+1);
  ilRC = GetCorrectedFormatZero(pclResult,pclCurLine,6);
  strcat(pclOutputLine,pclResult);
  fprintf(pfpSSIMFilePtr,"%s\n",pclOutputLine);
  for (ilI = 2; ilI <= 15; ilI++)
  {
     fprintf(pfpSSIMFilePtr,"%s\n",pclZeroLine);
  }

  return ilRC;
} /* End of GetSSIMFooter */


static int GetCorrectedFormatBlank(char *pcpResult, char *pcpString, int ipLen)
{
  int ilRC = RC_SUCCESS;
  int ilI;

  if (strlen(pcpString) >= ipLen)
  {
     strncpy(pcpResult,pcpString,ipLen);
     pcpResult[ipLen] = '\0';
  }
  else
  {
     strcpy(pcpResult,pcpString);
     for (ilI = 0; ilI < ipLen - strlen(pcpString); ilI++)
     {
        strcat(pcpResult," ");
     }
  }

  return ilRC;
} /* End of GetCorrectedFormatBlank */


static int GetCorrectedFormatZero(char *pcpResult, char *pcpString, int ipLen)
{
  int ilRC = RC_SUCCESS;
  int ilI;

  if (strlen(pcpString) >= ipLen)
  {
     strncpy(pcpResult,pcpString,ipLen);
     pcpResult[ipLen] = '\0';
  }
  else
  {
     pcpResult[0] = '\0';
     for (ilI = 0; ilI < ipLen - strlen(pcpString); ilI++)
     {
        strcat(pcpResult,"0");
     }
     strcat(pcpResult,pcpString);
  }

  return ilRC;
} /* End of GetCorrectedFormatZero */

static char *MonthTab [] = { "JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC",NULL };

/*
** CedaDate: YYYYMMDD
** SSIMDate: DDMMMYY where MMM is alpha representation of month
*/
static int CedaDateToSSIM(char *pcpResult, char *pcpDate)
{
  char clYear[8];
  char clMonth[8];
  char clDay[8];
  int  ilMonth;
  char clDate[16];

  strncpy(clYear,pcpDate,4);
  clYear[4] = '\0';
  strncpy(clMonth,pcpDate+4,2);
  clMonth[2]= '\0';
  ilMonth = atoi (clMonth);
  strncpy(clDay,pcpDate+6,2);
  clDay[2] = '\0';
  strcpy(clDate,clDay);
  strcat(clDate,MonthTab[ilMonth-1]);
  strcat(clDate,&clYear[2]);
  strcpy(pcpResult,clDate);
}

static int GetMonthAlpha(char *pcpResult, char *pcpDate)
{
  int ilRC = RC_SUCCESS;
  char pclMonth[8];

  strncpy(pclMonth,&pcpDate[4],2);
  pclMonth[2] = '\0';
  if (strcmp(pclMonth,"01") == 0)
  {
     strcpy(pcpResult,"JAN");
     return ilRC;
  }
  if (strcmp(pclMonth,"02") == 0)
  {
     strcpy(pcpResult,"FEB");
     return ilRC;
  }
  if (strcmp(pclMonth,"03") == 0)
  {
     strcpy(pcpResult,"MAR");
     return ilRC;
  }
  if (strcmp(pclMonth,"04") == 0)
  {
     strcpy(pcpResult,"APR");
     return ilRC;
  }
  if (strcmp(pclMonth,"05") == 0)
  {
     strcpy(pcpResult,"MAY");
     return ilRC;
  }
  if (strcmp(pclMonth,"06") == 0)
  {
     strcpy(pcpResult,"JUN");
     return ilRC;
  }
  if (strcmp(pclMonth,"07") == 0)
  {
     strcpy(pcpResult,"JUL");
     return ilRC;
  }
  if (strcmp(pclMonth,"08") == 0)
  {
     strcpy(pcpResult,"AUG");
     return ilRC;
  }
  if (strcmp(pclMonth,"09") == 0)
  {
     strcpy(pcpResult,"SEP");
     return ilRC;
  }
  if (strcmp(pclMonth,"10") == 0)
  {
     strcpy(pcpResult,"OCT");
     return ilRC;
  }
  if (strcmp(pclMonth,"11") == 0)
  {
     strcpy(pcpResult,"NOV");
     return ilRC;
  }
  if (strcmp(pclMonth,"12") == 0)
  {
     strcpy(pcpResult,"DEC");
     return ilRC;
  }
  strcpy(pcpResult,"   ");

  return ilRC;
} /* End of GetMonthAlpha */


static int GetMonthNumeric(char *pcpResult, char *pcpDate)
{
  int ilRC = RC_SUCCESS;
  char pclMonth[8];

  strncpy(pclMonth,&pcpDate[2],3);
  pclMonth[3] = '\0';
  if (strcmp(pclMonth,"JAN") == 0)
  {
     strcpy(pcpResult,"01");
     return ilRC;
  }
  if (strcmp(pclMonth,"FEB") == 0)
  {
     strcpy(pcpResult,"02");
     return ilRC;
  }
  if (strcmp(pclMonth,"MAR") == 0)
  {
     strcpy(pcpResult,"03");
     return ilRC;
  }
  if (strcmp(pclMonth,"APR") == 0)
  {
     strcpy(pcpResult,"04");
     return ilRC;
  }
  if (strcmp(pclMonth,"MAY") == 0)
  {
     strcpy(pcpResult,"05");
     return ilRC;
  }
  if (strcmp(pclMonth,"JUN") == 0)
  {
     strcpy(pcpResult,"06");
     return ilRC;
  }
  if (strcmp(pclMonth,"JUL") == 0)
  {
     strcpy(pcpResult,"07");
     return ilRC;
  }
  if (strcmp(pclMonth,"AUG") == 0)
  {
     strcpy(pcpResult,"08");
     return ilRC;
  }
  if (strcmp(pclMonth,"SEP") == 0)
  {
     strcpy(pcpResult,"09");
     return ilRC;
  }
  if (strcmp(pclMonth,"OCT") == 0)
  {
     strcpy(pcpResult,"10");
     return ilRC;
  }
  if (strcmp(pclMonth,"NOV") == 0)
  {
     strcpy(pcpResult,"11");
     return ilRC;
  }
  if (strcmp(pclMonth,"DEC") == 0)
  {
     strcpy(pcpResult,"12");
     return ilRC;
  }
  strcpy(pcpResult,"  ");

  return ilRC;
} /* End of GetMonthNumeric */

static int GetSSIMDataLine2(FILE *pfpSSIMFilePtr, char *pcpFld, char *pcpRec, int *ipCurLine, int ipSeq,char *vafr,char *vato)
{
  int ilRC = RC_SUCCESS;
  char pclOutputLine[256];
  char pclBlankLine[256];
  char pclFlno[16];
  char *pclFlnoRec;
  char pclTtyp[8];
  char *pclTtypRec;
  char pclVafr[16];
  char *pclVafrRec;
  char pclVato[16];
  char *pclVatoRec;
  char *pclData;
  char pclDoop[8];
  char pclFreq[8];
  char pclOrg3[8];
  char *pclOrg3Rec;
  char pclDes3[8];
  char *pclDes3Rec;
  char pclStoa[8];
  char *pclStoaRec;
  char pclStod[8];
  char *pclStodRec;
  char pclAct3[8];
  char *pclAct3Rec;
  char pclVian[8];
  char pclVial[256]="\0";
  char pclAdid[8];
  char pclAirline[8];
  char pclFlightNo[8];
  char pclSuffix[8];
  char pclResult[256];
  char pclSeq[8];
  int ilSeqV;
  char pclSeqV[8];
  char pclCurLine[8];
  int ilLen;
  int ilI;
  int ilVian;
  char pclTiDi[8];
  int ilTimeDiff;
  int ilActLeg;
  int ilDirection;
  int ilOffset;
  int ilVialPos;
  int ilTimeOk;
  LEG_TYPE slLeg[MAX_VIAS];
  VialTypePtrSSI pclVialPtr[MAX_VIAS]; 
  char clRefDate[16]="\0";
  char clTestDate[16]="\0";
  char clHopoDate[16];
  char pclVafrRaw[16];
  char pclVatoRaw[16];
  
  int  ilCntMin;
  int  ilCntDay;
  int  ilWkDy;
  int  ilCntMin2;
  int  ilCntDay2;
  int  ilWkDy2;
  /*
  ** Get FLNO
  */ 
  //

  pclFlnoRec = getItem(pcpFld,pcpRec,"FLNO");
  if (pclFlnoRec != NULL) 
  {
    ilRC = GetCorrectedFormatBlank(pclFlno,pclFlnoRec,9);
    strncpy(pclAirline,pclFlno,3);
    pclAirline[3] = '\0';
    if (pclFlno[6] == ' ')
    {
      strcpy(pclFlightNo," ");
      strncat(pclFlightNo,&pclFlno[3],3);
    }
    else 
    {
      strncpy(pclFlightNo,&pclFlno[3],4);
    }
    pclFlightNo[4] = '\0';
    strncpy(pclSuffix,&pclFlno[8],1);
    pclSuffix[1] = '\0';
  }
  else 
  {
    dbg(TRACE,"<GetSSIMDataLine2> Fatal -- missing FLNO");
    return RC_FAIL;
  }

  /* TTYP */
  pclTtypRec = getItem(pcpFld,pcpRec,"TTYP");
  if (pclTtypRec != NULL) 
  {
    ilRC = GetCorrectedFormatBlank(pclTtyp,pclTtypRec,1);
  }

  /* VAFR */
  pclVafrRec = getItem(pcpFld,pcpRec,"VAFR");
  if (pclVafrRec != NULL) 
  {  	
    GetFullDay(pclVafrRec, &ilCntDay, &ilCntMin, &ilWkDy);
    dbg(DEBUG,"<GetSSIMDataLine2>pclVafrRec<%s> ilCntDay <%i> ilCntMin <%i> ilWkDy<%i>",pclVafrRec,ilCntDay,ilCntMin,ilWkDy);
    GetFullDay(vafr, &ilCntDay2, &ilCntMin2, &ilWkDy2);
    dbg(DEBUG,"<GetSSIMDataLine2> vafr<%s> ilCntDay2 <%i> ilCntMin2 <%i> ilWkDy2<%i>",vafr,ilCntDay2,ilCntMin2,ilWkDy2);
    if(ilCntDay2>=ilCntDay)
    	pclVafrRec = vafr;
    strncpy(pclVafrRaw,pclVafrRec,8);
    pclVafrRaw[8] = '\0';
    strncpy(pclVafr,&pclVafrRec[6],2);
    pclVafr[2] = '\0';
    ilRC = GetMonthAlpha(pclResult,pclVafrRec);
    strcat(pclVafr,pclResult);
    strncat(pclVafr,&pclVafrRec[2],2);
    pclVafr[7] = '\0';
    dbg(DEBUG,"<GetSSIMDataLine2> pclVafr <%s>",pclVafr);
  }

  /* VATO */
  pclVatoRec = getItem(pcpFld,pcpRec,"VATO");
  if (pclVatoRec != NULL) 
  {
  	GetFullDay(pclVatoRec, &ilCntDay, &ilCntMin, &ilWkDy);
    dbg(DEBUG,"<GetSSIMDataLine2>pclVatoRec<%s> ilCntDay <%i> ilCntMin <%i> ilWkDy<%i>",pclVatoRec,ilCntDay,ilCntMin,ilWkDy);
    GetFullDay(vato, &ilCntDay2, &ilCntMin2, &ilWkDy2);
    dbg(DEBUG,"<GetSSIMDataLine2> vato<%s> ilCntDay2 <%i> ilCntMin2 <%i> ilWkDy2<%i>",vato,ilCntDay2,ilCntMin2,ilWkDy2);
    if(ilCntDay2<=ilCntDay)
    	pclVatoRec = vato;
    strncpy(pclVatoRaw,pclVatoRec,8);
    pclVatoRaw[8] = '\0';
    strncpy(pclVato,&pclVatoRec[6],2);
    pclVato[2] = '\0';
    ilRC = GetMonthAlpha(pclResult,pclVatoRec);
    strcat(pclVato,pclResult);
    strncat(pclVato,&pclVatoRec[2],2);
    pclVato[7] = '\0';
    dbg(DEBUG,"<GetSSIMDataLine2> pclVato <%s> ",pclVato);
  }

  /* DOOP */
  pclData  = getItem(pcpFld,pcpRec,"DOOP");
  if (pclData != NULL) 
  {
    strcpy(pclDoop,pclData);
    for (ilI = 0; ilI < 7; ilI++)
    {
      if (pclDoop[ilI] == '0')
      {
        pclDoop[ilI] = ' ';
      }
    }
  }

  /* FREQ */
  pclData = getItem(pcpFld,pcpRec,"FREQ");
  if (pclData != NULL) 
  {
    strcpy(pclFreq,pclData);
  }

  /* ORG3 */
  pclOrg3Rec = getItem(pcpFld,pcpRec,"ORG3");
  if (pclOrg3Rec != NULL) 
  {
    ilRC = GetCorrectedFormatBlank(pclOrg3,pclOrg3Rec,3);
  }

  /* DES3 */
  pclDes3Rec = getItem(pcpFld,pcpRec,"DES3");
  if (pclDes3Rec != NULL) 
  {
    ilRC = GetCorrectedFormatBlank(pclDes3,pclDes3Rec,3);
  }

  /* STOA */
  pclStoaRec = getItem(pcpFld,pcpRec,"STOA");
  if (pclStoaRec != NULL) 
  {
    ilRC = GetCorrectedFormatBlank(pclStoa,pclStoaRec,4);
  }

  /* STOD */
  pclStodRec = getItem(pcpFld,pcpRec,"STOD");
  if (pclStodRec != NULL) 
  {
    ilRC = GetCorrectedFormatBlank(pclStod,pclStodRec,4);
  }

  /* ACT3 */
  pclAct3Rec = getItem(pcpFld,pcpRec,"ACT3");
  if (pclAct3Rec != NULL) 
  {
    ilRC = GetCorrectedFormatBlank(pclAct3,pclAct3Rec,3);
  }

  /* VIAN */
  pclData = getItem(pcpFld,pcpRec,"VIAN");
  if (pclData != NULL) 
  {
    strcpy(pclVian,pclData);
  }

  /* VIAL */
  pclData = getItem(pcpFld,pcpRec,"VIAL");
  if (pclData != NULL) 
  {
    strcpy(pclVial,pclData);

	 //Frank 20121102 UFIS-1756    
	dbg(TRACE,"pclVial<%s>-------",pclVial);
  }

  /* ADID */
  pclData = getItem(pcpFld,pcpRec,"ADID");
  if (pclData != NULL) 
  {
    strcpy(pclAdid,pclData);
  }
  
  ilVian = atoi(pclVian);
  dbg(DEBUG,"<GetSSIMDataLine2> ilVian <%d> ",ilVian);
  ilSeqV = 1;

  /*
  ** Handle different FLIGHT without VIA
  */
  if (ilVian == 0) 
  {
    ilActLeg = 0;
    strcpy(slLeg[ilActLeg].vafr,pclVafrRaw);
    strcpy(slLeg[ilActLeg].vato,pclVatoRaw);
    strcpy(slLeg[ilActLeg].doop,pclDoop);
    strcpy(slLeg[ilActLeg].stod,pclVafrRaw); /* preliminary date part */
    slLeg[ilActLeg].stod[8]  = '\0';
    strcpy(slLeg[ilActLeg].stoa,pclVafrRaw); /* preliminary date part */
    slLeg[ilActLeg].stoa[8]  = '\0';
    strcpy(slLeg[ilActLeg].org3,pclOrg3);
    strncat(slLeg[ilActLeg].stod,pclStod,4);
    strncpy(slLeg[ilActLeg].des3,pclDes3,3);
    slLeg[ilActLeg].des3[3] = '\0';
    strncat(slLeg[ilActLeg].stoa,pclStoa,4);

    if (checkCedaTime(slLeg[ilActLeg].stod) == RC_FAIL) 
      strcpy(slLeg[ilActLeg].stod,pcgErrorDate);

    if (checkCedaTime(slLeg[ilActLeg].stoa) == RC_FAIL) 
      strcpy(slLeg[ilActLeg].stoa,pcgErrorDate);
  }
  else 
  {
    if (!strcmp(pclAdid,"A")) 
    {
      ilVialPos = 0;
      for (ilActLeg=0;ilActLeg<ilVian;ilActLeg++)
      {
      	dbg(DEBUG,"<GetSSIMDataLine2> pclVial <%s> ",pclVial);
        pclVialPtr[ilActLeg] = (VialTypePtrSSI) &pclVial[ilVialPos];
        ilVialPos = ilVialPos + sizeof(struct VialTypeSSI);
        dbg(DEBUG,"<GetSSIMDataLine2> pclVialPtr[ilActLeg] <%s> ",pclVialPtr[ilActLeg]);
      }

      /* first fill preliminary VIA list */
      for (ilActLeg=0;ilActLeg<=ilVian;ilActLeg++)
      {
        strcpy(slLeg[ilActLeg].vafr,pclVafrRaw);
        strcpy(slLeg[ilActLeg].vato,pclVatoRaw);
        //dbg(DEBUG,"<GetSSIMDataLine2> A ilActLeg <%i> slLeg[ilActLeg].vato <%s>",ilActLeg,slLeg[ilActLeg].vato);
        strcpy(slLeg[ilActLeg].doop,pclDoop);
        strcpy(slLeg[ilActLeg].stod,pclVafrRaw); /* preliminary date part */
        dbg(DEBUG,"<GetSSIMDataLine2> stod 1 <%s> ",slLeg[ilActLeg].stod);
        slLeg[ilActLeg].stod[8]  = '\0';
        strcpy(slLeg[ilActLeg].stoa,pclVafrRaw); /* preliminary date part */
        slLeg[ilActLeg].stoa[8]  = '\0';
dbg(DEBUG,"<GetSSIMDataLine2> stoa <%s> ",slLeg[ilActLeg].stoa);
        if (ilActLeg == 0) /* first leg origin + STOD */
        {
          strcpy(slLeg[ilActLeg].org3,pclOrg3);
          strncat(slLeg[ilActLeg].stod,pclStod,4);
        }
        else 
        {
          strncpy(slLeg[ilActLeg].org3,pclVialPtr[ilActLeg-1]->apc3,3);
          slLeg[ilActLeg].org3[3] = '\0';
          dbg(DEBUG,"<GetSSIMDataLine2> stodTime <%s> ",pclVialPtr[ilActLeg-1]->stodTime);
          strncat(slLeg[ilActLeg].stod,pclVialPtr[ilActLeg-1]->stodTime,4);
          //char tmp[5] = "    ";
          //strncat(slLeg[ilActLeg].stod,tmp,4);
          dbg(DEBUG,"<GetSSIMDataLine2> stod 2 <%s> ",slLeg[ilActLeg].stod);
        }
        slLeg[ilActLeg].stod[12]='\0';
        strcat(slLeg[ilActLeg].stod,"00");
    
        if (ilActLeg == ilVian) /* last leg */
        {
          strncpy(slLeg[ilActLeg].des3,pclDes3,3);
          slLeg[ilActLeg].des3[3] = '\0';
          strncat(slLeg[ilActLeg].stoa,pclStoa,4);
dbg(DEBUG,"<GetSSIMDataLine2> stoa 2 <%s> ",slLeg[ilActLeg].stoa);
        }
        else 
        {
          strncpy(slLeg[ilActLeg].des3,pclVialPtr[ilActLeg]->apc3,3);
          slLeg[ilActLeg].des3[3] = '\0';
          strncat(slLeg[ilActLeg].stoa,pclVialPtr[ilActLeg]->stoaTime,4);
        }
        slLeg[ilActLeg].stoa[12]='\0';
        strcat(slLeg[ilActLeg].stoa,"00");
dbg(DEBUG,"<GetSSIMDataLine2> stoa 3 <%s> ",slLeg[ilActLeg].stoa);
      } /* for loop */
      /* 
      ** now we have filled an array of legs 
      ** let's start to compute the corrected time chain
      ** beginning with the last leg proceeding to first leg
      */
dbg(DEBUG,"<GetSSIMDataLine2> stoa 4 <%s> ",slLeg[ilVian].stoa);
      strcpy(clRefDate,slLeg[ilVian].stoa); /* all times are related to HOPO time */
      strcpy(clHopoDate,clRefDate);
      ilTimeOk = TRUE;
      for (ilActLeg=ilVian;ilActLeg>=0;ilActLeg--) /* Start correction with first leg apart HOPO */
      {
        if (ilTimeOk) 
        {
          if (ilActLeg!=ilVian) /* STOA */
          {
            strcpy(clTestDate,clRefDate);
            clTestDate[8] = '\0';
            strcat(clTestDate,&slLeg[ilActLeg].stoa[8]);  
            CorrectDateTime(clRefDate,clTestDate,clHopoDate,&ilOffset,LEFT);

            if (ilOffset == 999999) ilTimeOk = FALSE;
            if (ilTimeOk) 
            {
              strcpy(slLeg[ilActLeg].stoa,clTestDate);
              strcpy(clRefDate,clTestDate);
            }
            else 
              strcpy(slLeg[ilActLeg].stoa,pcgErrorDate);
          }
        }
        else 
          strcpy(slLeg[ilActLeg].stoa,pcgErrorDate);

    if (ilTimeOk) 
    {
      strcpy(clTestDate,clRefDate);
      clTestDate[8] = '\0';
dbg(DEBUG,"<GetSSIMDataLine2> stod 4 <%s> ",slLeg[ilActLeg].stod);
      strcat(clTestDate,&slLeg[ilActLeg].stod[8]);
if(ilVian==1)
dbg(DEBUG,"<GetSSIMDataLine2> clRefDate <%s> clTestDate<%s>clHopoDate<%s>",clRefDate,clTestDate,clHopoDate);
      CorrectDateTime(clRefDate,clTestDate,clHopoDate,&ilOffset,LEFT);
dbg(DEBUG,"<GetSSIMDataLine2> ilOffset <%d> ",ilOffset);
      if (ilOffset == 999999) ilTimeOk = FALSE;
      if (ilTimeOk) 
      {
        slLeg[ilActLeg].offset = ilOffset;

        if (ilOffset!=0) 
          shiftData(&slLeg[ilActLeg],ilOffset);
if(ilVian==1)
dbg(DEBUG,"<GetSSIMDataLine2> stod <%s> ",slLeg[1].stod);
        strcpy(slLeg[ilActLeg].stod,clTestDate);
        strcpy(clRefDate,clTestDate);
if(ilVian==1)
dbg(DEBUG,"<GetSSIMDataLine2> stod <%s> ",slLeg[1].stod);
      }
      else 
        strcpy(slLeg[ilActLeg].stod,pcgErrorDate);
    }
    else 
      strcpy(slLeg[ilActLeg].stod,pcgErrorDate);
      }
    } /* arrival */

    /* Handle mulit-leg departure flight */
    else if (!strcmp(pclAdid,"D")) 
    {
      ilVialPos = 0;
      for (ilActLeg=0;ilActLeg<ilVian;ilActLeg++)
      {
    pclVialPtr[ilActLeg] = (VialTypePtrSSI) &pclVial[ilVialPos];
    ilVialPos = ilVialPos + sizeof(struct VialTypeSSI);
      }
      /* first fill preliminary VIA list */
      for (ilActLeg=0;ilActLeg<=ilVian;ilActLeg++)
      {
    strcpy(slLeg[ilActLeg].vafr,pclVafrRaw);
    strcpy(slLeg[ilActLeg].vato,pclVatoRaw);
    //dbg(DEBUG,"<GetSSIMDataLine2> ilActLeg <%i> slLeg[ilActLeg].vato <%s>",ilActLeg,slLeg[ilActLeg].vato);
    strcpy(slLeg[ilActLeg].doop,pclDoop);
    strcpy(slLeg[ilActLeg].stod,pclVafrRaw); /* preliminary date part */
    slLeg[ilActLeg].stod[8]  = '\0';
    strcpy(slLeg[ilActLeg].stoa,pclVafrRaw); /* preliminary date part */
    slLeg[ilActLeg].stoa[8]  = '\0';
      
    if (ilActLeg == 0) /* first leg origin + STOD */
    {
      strcpy(slLeg[ilActLeg].org3,pclOrg3);
      strncat(slLeg[ilActLeg].stod,pclStod,4);
    }
    else 
    {
      strncpy(slLeg[ilActLeg].org3,pclVialPtr[ilActLeg-1]->apc3,3);
      slLeg[ilActLeg].org3[3] = '\0';
      strncat(slLeg[ilActLeg].stod,pclVialPtr[ilActLeg-1]->stodTime,4);
    }
    slLeg[ilActLeg].stod[12]='\0';
    strcat(slLeg[ilActLeg].stod,"00");
      
    if (ilActLeg == ilVian) /* last leg */
    {
      strncpy(slLeg[ilActLeg].des3,pclDes3,3);
      slLeg[ilActLeg].des3[3] = '\0';
      strncat(slLeg[ilActLeg].stoa,pclStoa,4);
    }
    else 
    {
      strncpy(slLeg[ilActLeg].des3,pclVialPtr[ilActLeg]->apc3,3);
      slLeg[ilActLeg].des3[3] = '\0';
      strncat(slLeg[ilActLeg].stoa,pclVialPtr[ilActLeg]->stoaTime,4);
    }
    slLeg[ilActLeg].stoa[12]='\0';
    strcat(slLeg[ilActLeg].stoa,"00");
      }

      /* 
       ** now we have filled an array of legs 
       ** let's start to compute the corrected time chain
       ** beginning with the first leg proceeding to last leg
       */
      strcpy(clRefDate,slLeg[0].stod); /* all times are related to HOPO time */
      strcpy(clHopoDate,clRefDate);
      ilTimeOk = TRUE;
      for (ilActLeg=0;ilActLeg<=ilVian;ilActLeg++) /* Start correction with first leg apart HOPO */
      {
    if (ilTimeOk) 
    {
      if (ilActLeg != 0)
      {
        clTestDate[8] = '\0';
if(ilVian==1)
dbg(DEBUG,"<GetSSIMDataLine2> stod <%s> ",slLeg[1].stod);
        strcat(clTestDate,&slLeg[ilActLeg].stod[8]);
        CorrectDateTime(clRefDate,clTestDate,clHopoDate,&ilOffset,RIGHT);
dbg(DEBUG,"<GetSSIMDataLine2> ilOffset <%d> ",ilOffset);
        if (ilOffset == 999999) ilTimeOk = FALSE;
        if (ilTimeOk) 
        {
          slLeg[ilActLeg].offset = ilOffset;
          if (ilOffset!=0) 
        shiftData(&slLeg[ilActLeg],ilOffset);
if(ilVian==1)
dbg(DEBUG,"<GetSSIMDataLine2> stod <%s> ",slLeg[1].stod);
          strcpy(slLeg[ilActLeg].stod,clTestDate);
          strcpy(clRefDate,clTestDate);
if(ilVian==1)
dbg(DEBUG,"<GetSSIMDataLine2> stod <%s> ",slLeg[1].stod);
        }
        else 
          strcpy(slLeg[ilActLeg].stod,pcgErrorDate);
      }
    }
    else 
      strcpy(slLeg[ilActLeg].stod,pcgErrorDate);

    if (ilTimeOk) 
    {
      strcpy(clTestDate,clRefDate);
      clTestDate[8] = '\0';
      strcat(clTestDate,&slLeg[ilActLeg].stoa[8]);  
      CorrectDateTime(clRefDate,clTestDate,clHopoDate,&ilOffset,RIGHT);
      if (ilOffset == 999999) ilTimeOk = FALSE;
      if (ilTimeOk) 
      {
        strcpy(slLeg[ilActLeg].stoa,clTestDate);
        strcpy(clRefDate,clTestDate);
      }
      else 
        strcpy(slLeg[ilActLeg].stoa,pcgErrorDate);
    }
    else 
      strcpy(slLeg[ilActLeg].stoa,pcgErrorDate);
      }
    } /* departure */
  } /* ilVian != 0 */

  for (ilActLeg=0;ilActLeg<=ilVian;ilActLeg++)
  {
    strcpy(pclOutputLine,"3");
    strcat(pclOutputLine,pclSuffix);
    strcat(pclOutputLine,pclAirline);
    strcat(pclOutputLine,pclFlightNo);
    sprintf(pclSeq,"%d",ipSeq);
    ilRC = GetCorrectedFormatZero(pclResult,pclSeq,2);
    strcat(pclOutputLine,pclResult);
    sprintf(pclSeqV,"%d",ilSeqV);
    ilRC = GetCorrectedFormatZero(pclResult,pclSeqV,2);
    strcat(pclOutputLine,pclResult);
    strcat(pclOutputLine,pclTtyp);
    ilRC = CedaDateToSSIM(pclResult,slLeg[ilActLeg].vafr);
    strcat(pclOutputLine,pclResult);
    //dbg(DEBUG,"<GetSSIMDataLine2> ilActLeg <%i> Vato <%s> ",ilActLeg,slLeg[ilActLeg].vato);
    ilRC = CedaDateToSSIM(pclResult,slLeg[ilActLeg].vato);
    //dbg(DEBUG,"<GetSSIMDataLine2> Vato pclResult <%s> ",pclResult);
    strcat(pclOutputLine,pclResult);
    strcat(pclOutputLine,slLeg[ilActLeg].doop);
    strcat(pclOutputLine,pclFreq);
    strcat(pclOutputLine,slLeg[ilActLeg].org3);
    strncat(pclOutputLine,&slLeg[ilActLeg].stod[8],4);
    strncat(pclOutputLine,&slLeg[ilActLeg].stod[8],4);
    ilRC = GetTimeDiff(pclTiDi,&ilTimeDiff,slLeg[ilActLeg].org3);
    strcat(pclOutputLine,pclTiDi);
    strcat(pclOutputLine,"  ");
    strcat(pclOutputLine,slLeg[ilActLeg].des3);
    strncat(pclOutputLine,&slLeg[ilActLeg].stoa[8],4);
    strncat(pclOutputLine,&slLeg[ilActLeg].stoa[8],4);

    //Frank 20121102 UFIS-1756
	//ilRC = GetTimeDiff(pclTiDi,&ilTimeDiff,slLeg[ilActLeg].org3);
    	ilRC = GetTimeDiff(pclTiDi,&ilTimeDiff,slLeg[ilActLeg].des3);
	strcat(pclOutputLine,pclTiDi);
    strcat(pclOutputLine,"  ");
    
    strcat(pclOutputLine,pclAct3);
    strcat(pclOutputLine,"C00Y000");
    ilRC = GetCorrectedFormatBlank(pclBlankLine," ",112);
    strcat(pclOutputLine,pclBlankLine);
    sprintf(pclCurLine,"%d",*ipCurLine);
    ilRC = GetCorrectedFormatZero(pclResult,pclCurLine,6);
    strcat(pclOutputLine,pclResult);
    fprintf(pfpSSIMFilePtr,"%s\n",pclOutputLine);
    ilSeqV++;
    *ipCurLine = *ipCurLine + 1;
  }
  return ilRC;
} /* End of GetSSIMDataLine2 */
static int GetSSIMDataLine(FILE *pfpSSIMFilePtr, char *pcpFld, char *pcpRec, int *ipCurLine, int ipSeq)
{
  int ilRC = RC_SUCCESS;
  char pclOutputLine[256];
  char pclBlankLine[256];
  char pclFlno[16];
  char *pclFlnoRec;
  char pclTtyp[8];
  char *pclTtypRec;
  char pclVafr[16];
  char *pclVafrRec;
  char pclVato[16];
  char *pclVatoRec;
  char *pclData;
  char pclDoop[8];
  char pclFreq[8];
  char pclOrg3[8];
  char *pclOrg3Rec;
  char pclDes3[8];
  char *pclDes3Rec;
  char pclStoa[8];
  char *pclStoaRec;
  char pclStod[8];
  char *pclStodRec;
  char pclAct3[8];
  char *pclAct3Rec;
  char pclVian[8];
  char pclVial[256];
  char pclAdid[8];
  char pclAirline[8];
  char pclFlightNo[8];
  char pclSuffix[8];
  char pclResult[256];
  char pclSeq[8];
  int ilSeqV;
  char pclSeqV[8];
  char pclCurLine[8];
  int ilLen;
  int ilI;
  int ilVian;
  char pclTiDi[8];
  int ilTimeDiff;
  int ilActLeg;
  int ilDirection;
  int ilOffset;
  int ilVialPos;
  int ilTimeOk;
  LEG_TYPE slLeg[MAX_VIAS];
  VialTypePtrSSI pclVialPtr[MAX_VIAS]; 
  char clRefDate[16];
  char clTestDate[16];
  char clHopoDate[16];
  char pclVafrRaw[16];
  char pclVatoRaw[16];
  /*
  ** Get FLNO
  */ 
  pclFlnoRec = getItem(pcpFld,pcpRec,"FLNO");
  if (pclFlnoRec != NULL) 
  {
    ilRC = GetCorrectedFormatBlank(pclFlno,pclFlnoRec,9);
    strncpy(pclAirline,pclFlno,3);
    pclAirline[3] = '\0';
    if (pclFlno[6] == ' ')
    {
      strcpy(pclFlightNo," ");
      strncat(pclFlightNo,&pclFlno[3],3);
    }
    else 
    {
      strncpy(pclFlightNo,&pclFlno[3],4);
    }
    pclFlightNo[4] = '\0';
    strncpy(pclSuffix,&pclFlno[8],1);
    pclSuffix[1] = '\0';
  }
  else 
  {
    dbg(TRACE,"<GetSSIMDataLine> Fatal -- missing FLNO");
    return RC_FAIL;
  }

  /* TTYP */
  pclTtypRec = getItem(pcpFld,pcpRec,"TTYP");
  if (pclTtypRec != NULL) 
  {
    ilRC = GetCorrectedFormatBlank(pclTtyp,pclTtypRec,1);
  }

  /* VAFR */
  pclVafrRec = getItem(pcpFld,pcpRec,"VAFR");
  if (pclVafrRec != NULL) 
  {
    strncpy(pclVafrRaw,pclVafrRec,8);
    pclVafrRaw[8] = '\0';
    strncpy(pclVafr,&pclVafrRec[6],2);
    pclVafr[2] = '\0';
    ilRC = GetMonthAlpha(pclResult,pclVafrRec);
    strcat(pclVafr,pclResult);
    strncat(pclVafr,&pclVafrRec[2],2);
    pclVafr[7] = '\0';
  }

  /* VATO */
  pclVatoRec = getItem(pcpFld,pcpRec,"VATO");
  if (pclVatoRec != NULL) 
  {
    strncpy(pclVatoRaw,pclVatoRec,8);
    pclVatoRaw[8] = '\0';
    strncpy(pclVato,&pclVatoRec[6],2);
    pclVato[2] = '\0';
    ilRC = GetMonthAlpha(pclResult,pclVatoRec);
    strcat(pclVato,pclResult);
    strncat(pclVato,&pclVatoRec[2],2);
    pclVato[7] = '\0';
  }

  /* DOOP */
  pclData  = getItem(pcpFld,pcpRec,"DOOP");
  if (pclData != NULL) 
  {
    strcpy(pclDoop,pclData);
    for (ilI = 0; ilI < 7; ilI++)
    {
      if (pclDoop[ilI] == '0')
      {
        pclDoop[ilI] = ' ';
      }
    }
  }

  /* FREQ */
  pclData = getItem(pcpFld,pcpRec,"FREQ");
  if (pclData != NULL) 
  {
    strcpy(pclFreq,pclData);
  }

  /* ORG3 */
  pclOrg3Rec = getItem(pcpFld,pcpRec,"ORG3");
  if (pclOrg3Rec != NULL) 
  {
    ilRC = GetCorrectedFormatBlank(pclOrg3,pclOrg3Rec,3);
  }

  /* DES3 */
  pclDes3Rec = getItem(pcpFld,pcpRec,"DES3");
  if (pclDes3Rec != NULL) 
  {
    ilRC = GetCorrectedFormatBlank(pclDes3,pclDes3Rec,3);
  }

  /* STOA */
  pclStoaRec = getItem(pcpFld,pcpRec,"STOA");
  if (pclStoaRec != NULL) 
  {
    ilRC = GetCorrectedFormatBlank(pclStoa,pclStoaRec,4);
  }

  /* STOD */
  pclStodRec = getItem(pcpFld,pcpRec,"STOD");
  if (pclStodRec != NULL) 
  {
    ilRC = GetCorrectedFormatBlank(pclStod,pclStodRec,4);
  }

  /* ACT3 */
  pclAct3Rec = getItem(pcpFld,pcpRec,"ACT3");
  if (pclAct3Rec != NULL) 
  {
    ilRC = GetCorrectedFormatBlank(pclAct3,pclAct3Rec,3);
  }

  /* VIAN */
  pclData = getItem(pcpFld,pcpRec,"VIAN");
  if (pclData != NULL) 
  {
    strcpy(pclVian,pclData);
  }

  /* VIAL */
  pclData = getItem(pcpFld,pcpRec,"VIAL");
  if (pclData != NULL) 
  {
    strcpy(pclVial,pclData);
  }

  /* ADID */
  pclData = getItem(pcpFld,pcpRec,"ADID");
  if (pclData != NULL) 
  {
    strcpy(pclAdid,pclData);
  }
  
  ilVian = atoi(pclVian);
  dbg(DEBUG,"<GetSSIMDataLine2> ilVian <%d> ",ilVian);
  ilSeqV = 1;

  /*
  ** Handle different FLIGHT without VIA
  */
  if (ilVian == 0) 
  {
    ilActLeg = 0;
    strcpy(slLeg[ilActLeg].vafr,pclVafrRaw);
    strcpy(slLeg[ilActLeg].vato,pclVatoRaw);
    strcpy(slLeg[ilActLeg].doop,pclDoop);
    strcpy(slLeg[ilActLeg].stod,pclVafrRaw); /* preliminary date part */
    slLeg[ilActLeg].stod[8]  = '\0';
    strcpy(slLeg[ilActLeg].stoa,pclVafrRaw); /* preliminary date part */
    slLeg[ilActLeg].stoa[8]  = '\0';
    strcpy(slLeg[ilActLeg].org3,pclOrg3);
    strncat(slLeg[ilActLeg].stod,pclStod,4);
    strncpy(slLeg[ilActLeg].des3,pclDes3,3);
    slLeg[ilActLeg].des3[3] = '\0';
    strncat(slLeg[ilActLeg].stoa,pclStoa,4);

    if (checkCedaTime(slLeg[ilActLeg].stod) == RC_FAIL) 
      strcpy(slLeg[ilActLeg].stod,pcgErrorDate);

    if (checkCedaTime(slLeg[ilActLeg].stoa) == RC_FAIL) 
      strcpy(slLeg[ilActLeg].stoa,pcgErrorDate);
  }
  else 
  {
    if (!strcmp(pclAdid,"A")) 
    {
      ilVialPos = 0;
      for (ilActLeg=0;ilActLeg<ilVian;ilActLeg++)
      {
        pclVialPtr[ilActLeg] = (VialTypePtrSSI) &pclVial[ilVialPos];
        ilVialPos = ilVialPos + sizeof(struct VialTypeSSI);
      }

      /* first fill preliminary VIA list */
      for (ilActLeg=0;ilActLeg<=ilVian;ilActLeg++)
      {
        strcpy(slLeg[ilActLeg].vafr,pclVafrRaw);
        strcpy(slLeg[ilActLeg].vato,pclVatoRaw);
        strcpy(slLeg[ilActLeg].doop,pclDoop);
        strcpy(slLeg[ilActLeg].stod,pclVafrRaw); /* preliminary date part */
        slLeg[ilActLeg].stod[8]  = '\0';
        strcpy(slLeg[ilActLeg].stoa,pclVafrRaw); /* preliminary date part */
        slLeg[ilActLeg].stoa[8]  = '\0';
    
        if (ilActLeg == 0) /* first leg origin + STOD */
        {
          strcpy(slLeg[ilActLeg].org3,pclOrg3);
          strncat(slLeg[ilActLeg].stod,pclStod,4);
        }
        else 
        {
          strncpy(slLeg[ilActLeg].org3,pclVialPtr[ilActLeg-1]->apc3,3);
          slLeg[ilActLeg].org3[3] = '\0';
          strncat(slLeg[ilActLeg].stod,pclVialPtr[ilActLeg-1]->stodTime,4);
        }
        slLeg[ilActLeg].stod[12]='\0';
        strcat(slLeg[ilActLeg].stod,"00");
    
        if (ilActLeg == ilVian) /* last leg */
        {
          strncpy(slLeg[ilActLeg].des3,pclDes3,3);
          slLeg[ilActLeg].des3[3] = '\0';
          strncat(slLeg[ilActLeg].stoa,pclStoa,4);
        }
        else 
        {
          strncpy(slLeg[ilActLeg].des3,pclVialPtr[ilActLeg]->apc3,3);
          slLeg[ilActLeg].des3[3] = '\0';
          strncat(slLeg[ilActLeg].stoa,pclVialPtr[ilActLeg]->stoaTime,4);
        }
        slLeg[ilActLeg].stoa[12]='\0';
        strcat(slLeg[ilActLeg].stoa,"00");
      } /* for loop */
      /* 
      ** now we have filled an array of legs 
      ** let's start to compute the corrected time chain
      ** beginning with the last leg proceeding to first leg
      */
      strcpy(clRefDate,slLeg[ilVian].stoa); /* all times are related to HOPO time */
      strcpy(clHopoDate,clRefDate);
      ilTimeOk = TRUE;
      for (ilActLeg=ilVian;ilActLeg>=0;ilActLeg--) /* Start correction with first leg apart HOPO */
      {
        if (ilTimeOk) 
        {
          if (ilActLeg!=ilVian) /* STOA */
          {
            strcpy(clTestDate,clRefDate);
            clTestDate[8] = '\0';
            strcat(clTestDate,&slLeg[ilActLeg].stoa[8]);  
            CorrectDateTime(clRefDate,clTestDate,clHopoDate,&ilOffset,LEFT);
if(ilVian==1)
dbg(DEBUG,"<GetSSIMDataLine2> stod <%s> ",slLeg[1].stod);
            if (ilOffset == 999999) ilTimeOk = FALSE;
            if (ilTimeOk) 
            {
              strcpy(slLeg[ilActLeg].stoa,clTestDate);
              strcpy(clRefDate,clTestDate);
            }
            else 
              strcpy(slLeg[ilActLeg].stoa,pcgErrorDate);
          }
        }
        else 
          strcpy(slLeg[ilActLeg].stoa,pcgErrorDate);

    if (ilTimeOk) 
    {
      strcpy(clTestDate,clRefDate);
      clTestDate[8] = '\0';
      strcat(clTestDate,&slLeg[ilActLeg].stod[8]);
if(ilVian==1)
dbg(DEBUG,"<GetSSIMDataLine2> clRefDate <%s> clTestDate<%s>clHopoDate<%s>",clRefDate,clTestDate,clHopoDate);
      CorrectDateTime(clRefDate,clTestDate,clHopoDate,&ilOffset,LEFT);
if(ilVian==1)
dbg(DEBUG,"<GetSSIMDataLine2> ilOffset <%d> ",ilOffset);
      if (ilOffset == 999999) ilTimeOk = FALSE;
      if (ilTimeOk) 
      {
        slLeg[ilActLeg].offset = ilOffset;

        if (ilOffset!=0) 
          shiftData(&slLeg[ilActLeg],ilOffset);
        strcpy(slLeg[ilActLeg].stod,clTestDate);
        strcpy(clRefDate,clTestDate);
      }
      else 
        strcpy(slLeg[ilActLeg].stod,pcgErrorDate);
    }
    else 
      strcpy(slLeg[ilActLeg].stod,pcgErrorDate);
      }
    } /* arrival */

    /* Handle mulit-leg departure flight */
    else if (!strcmp(pclAdid,"D")) 
    {
      ilVialPos = 0;
      for (ilActLeg=0;ilActLeg<ilVian;ilActLeg++)
      {
    pclVialPtr[ilActLeg] = (VialTypePtrSSI) &pclVial[ilVialPos];
    ilVialPos = ilVialPos + sizeof(struct VialTypeSSI);
      }
      /* first fill preliminary VIA list */
      for (ilActLeg=0;ilActLeg<=ilVian;ilActLeg++)
      {
    strcpy(slLeg[ilActLeg].vafr,pclVafrRaw);
    strcpy(slLeg[ilActLeg].vato,pclVatoRaw);
    strcpy(slLeg[ilActLeg].doop,pclDoop);
    strcpy(slLeg[ilActLeg].stod,pclVafrRaw); /* preliminary date part */
    slLeg[ilActLeg].stod[8]  = '\0';
    strcpy(slLeg[ilActLeg].stoa,pclVafrRaw); /* preliminary date part */
    slLeg[ilActLeg].stoa[8]  = '\0';
      
    if (ilActLeg == 0) /* first leg origin + STOD */
    {
      strcpy(slLeg[ilActLeg].org3,pclOrg3);
      strncat(slLeg[ilActLeg].stod,pclStod,4);
    }
    else 
    {
      strncpy(slLeg[ilActLeg].org3,pclVialPtr[ilActLeg-1]->apc3,3);
      slLeg[ilActLeg].org3[3] = '\0';
      strncat(slLeg[ilActLeg].stod,pclVialPtr[ilActLeg-1]->stodTime,4);
    }
    slLeg[ilActLeg].stod[12]='\0';
    strcat(slLeg[ilActLeg].stod,"00");
      
    if (ilActLeg == ilVian) /* last leg */
    {
      strncpy(slLeg[ilActLeg].des3,pclDes3,3);
      slLeg[ilActLeg].des3[3] = '\0';
      strncat(slLeg[ilActLeg].stoa,pclStoa,4);
    }
    else 
    {
      strncpy(slLeg[ilActLeg].des3,pclVialPtr[ilActLeg]->apc3,3);
      slLeg[ilActLeg].des3[3] = '\0';
      strncat(slLeg[ilActLeg].stoa,pclVialPtr[ilActLeg]->stoaTime,4);
    }
    slLeg[ilActLeg].stoa[12]='\0';
    strcat(slLeg[ilActLeg].stoa,"00");
      }

      /* 
       ** now we have filled an array of legs 
       ** let's start to compute the corrected time chain
       ** beginning with the first leg proceeding to last leg
       */
      strcpy(clRefDate,slLeg[0].stod); /* all times are related to HOPO time */
      strcpy(clHopoDate,clRefDate);
      ilTimeOk = TRUE;
      for (ilActLeg=0;ilActLeg<=ilVian;ilActLeg++) /* Start correction with first leg apart HOPO */
      {
    if (ilTimeOk) 
    {
      if (ilActLeg != 0)
      {
        clTestDate[8] = '\0';
        strcat(clTestDate,&slLeg[ilActLeg].stod[8]);
        CorrectDateTime(clRefDate,clTestDate,clHopoDate,&ilOffset,RIGHT);

        if (ilOffset == 999999) ilTimeOk = FALSE;
        if (ilTimeOk) 
        {
          slLeg[ilActLeg].offset = ilOffset;
          if (ilOffset!=0) 
        shiftData(&slLeg[ilActLeg],ilOffset);

          strcpy(slLeg[ilActLeg].stod,clTestDate);
          strcpy(clRefDate,clTestDate);
        }
        else 
          strcpy(slLeg[ilActLeg].stod,pcgErrorDate);
      }
    }
    else 
      strcpy(slLeg[ilActLeg].stod,pcgErrorDate);

    if (ilTimeOk) 
    {
      strcpy(clTestDate,clRefDate);
      clTestDate[8] = '\0';
      strcat(clTestDate,&slLeg[ilActLeg].stoa[8]);  
      CorrectDateTime(clRefDate,clTestDate,clHopoDate,&ilOffset,RIGHT);
      if (ilOffset == 999999) ilTimeOk = FALSE;
      if (ilTimeOk) 
      {
        strcpy(slLeg[ilActLeg].stoa,clTestDate);
        strcpy(clRefDate,clTestDate);
      }
      else 
        strcpy(slLeg[ilActLeg].stoa,pcgErrorDate);
    }
    else 
      strcpy(slLeg[ilActLeg].stoa,pcgErrorDate);
      }
    } /* departure */
  } /* ilVian != 0 */

  for (ilActLeg=0;ilActLeg<=ilVian;ilActLeg++)
  {
    strcpy(pclOutputLine,"3");
    strcat(pclOutputLine,pclSuffix);
    strcat(pclOutputLine,pclAirline);
    strcat(pclOutputLine,pclFlightNo);
    sprintf(pclSeq,"%d",ipSeq);
    ilRC = GetCorrectedFormatZero(pclResult,pclSeq,2);
    strcat(pclOutputLine,pclResult);
    sprintf(pclSeqV,"%d",ilSeqV);
    ilRC = GetCorrectedFormatZero(pclResult,pclSeqV,2);
    strcat(pclOutputLine,pclResult);
    strcat(pclOutputLine,pclTtyp);
    ilRC = CedaDateToSSIM(pclResult,slLeg[ilActLeg].vafr);
    strcat(pclOutputLine,pclResult);
    ilRC = CedaDateToSSIM(pclResult,slLeg[ilActLeg].vato);
    strcat(pclOutputLine,pclResult);
    strcat(pclOutputLine,slLeg[ilActLeg].doop);
    strcat(pclOutputLine,pclFreq);
    strcat(pclOutputLine,slLeg[ilActLeg].org3);
    strncat(pclOutputLine,&slLeg[ilActLeg].stod[8],4);
    strncat(pclOutputLine,&slLeg[ilActLeg].stod[8],4);
    ilRC = GetTimeDiff(pclTiDi,&ilTimeDiff,slLeg[ilActLeg].org3);
    strcat(pclOutputLine,pclTiDi);
    strcat(pclOutputLine,"  ");
    strcat(pclOutputLine,slLeg[ilActLeg].des3);
    strncat(pclOutputLine,&slLeg[ilActLeg].stoa[8],4);
    strncat(pclOutputLine,&slLeg[ilActLeg].stoa[8],4);
    ilRC = GetTimeDiff(pclTiDi,&ilTimeDiff,slLeg[ilActLeg].org3);
    strcat(pclOutputLine,pclTiDi);
    strcat(pclOutputLine,"  ");
    
    strcat(pclOutputLine,pclAct3);
    strcat(pclOutputLine,"C00Y000");
    ilRC = GetCorrectedFormatBlank(pclBlankLine," ",112);
    strcat(pclOutputLine,pclBlankLine);
    sprintf(pclCurLine,"%d",*ipCurLine);
    ilRC = GetCorrectedFormatZero(pclResult,pclCurLine,6);
    strcat(pclOutputLine,pclResult);
    fprintf(pfpSSIMFilePtr,"%s\n",pclOutputLine);
    ilSeqV++;
    *ipCurLine = *ipCurLine + 1;
  }
  return ilRC;
} /* End of GetSSIMDataLine */

static void shiftData (LEG_TYPE *pcpLeg,int ipOffset)
{
  static char DoopList [] = {'0','1','2','3','4','5','6','7'};
  char clDoop[16];
  char clDate[16];
  int  i,ilPos;
  int  ilCntMin;
  int  ilCntDay;
  int  ilWkDy;
  int  ilRC;
  
  /* shift day of operation per leg */
  strcpy(clDoop,"       ");
  for (i=0;i<7;i++){
    if (pcpLeg->doop[i] != ' ') {
      ilPos = i +  ipOffset;
      if (ilPos < 0) ilPos = 7+i+ipOffset;
      else if (ilPos >= 7) ilPos = ilPos-7;
      clDoop[ilPos]= DoopList[ilPos+1];
    }
  }
  strcpy(pcpLeg->doop,clDoop);
  /* shift vato/vafr */
  strncpy(clDate,pcpLeg->vato,8);
  clDate[8] = '\0';
  strcat(clDate,"000000");
  ilRC = GetFullDay(clDate, &ilCntDay, &ilCntMin, &ilWkDy);
  ilCntDay = ilCntDay + ipOffset;
  FullDayDate(pcpLeg->vato,ilCntDay);

  strncpy(clDate,pcpLeg->vafr,8);
  clDate[8] = '\0';
  strcat(clDate,"000000");
  ilRC = GetFullDay(clDate, &ilCntDay, &ilCntMin, &ilWkDy);
  ilCntDay = ilCntDay + ipOffset;
  FullDayDate(pcpLeg->vafr,ilCntDay);
}

static void CorrectDateTime(char *pcpRefDate,char *pcpTestDate,char *pcpHopoDate,int *ipOffset,int ipDir)
{
  int  ilCntMinHopo;
  int  ilCntDayHopo;
  int  ilCntMinRef;
  int  ilCntDayRef;
  int  ilCntMinTest;
  int  ilCntDayTest;
  int  ilWkDy;
  int  ilRC;
  int  ilMinDiff;
  char clTestTime[7];

  ilRC = checkCedaTime(pcpTestDate);
  if (ilRC == RC_FAIL){
    strcpy(pcpTestDate,pcgErrorDate);
    *ipOffset = 999999;
    return;
  }
  strncpy(clTestTime, &pcpTestDate[8], 6);
  clTestTime[6] = '\0';

  ilRC = GetFullDay(pcpHopoDate, &ilCntDayHopo, &ilCntMinHopo, &ilWkDy);
  ilRC = GetFullDay(pcpRefDate, &ilCntDayRef, &ilCntMinRef, &ilWkDy);
  ilRC = GetFullDay(pcpTestDate, &ilCntDayTest, &ilCntMinTest, &ilWkDy);
  if (ipDir == LEFT) {
    if (ilCntMinTest>ilCntMinRef){
      ilCntDayTest = ilCntDayTest - 1;
      FullDayDate(pcpTestDate, ilCntDayTest);
      strcat(pcpTestDate, clTestTime);
    }
    *ipOffset = ilCntDayTest - ilCntDayHopo;
  }
  else { /* ipDir == RIGHT */
    if (ilCntMinTest<ilCntMinRef){
      ilCntDayTest = ilCntDayTest + 1;
      FullDayDate(pcpTestDate, ilCntDayTest);
      strcat(pcpTestDate, clTestTime);
    }
    *ipOffset = ilCntDayTest - ilCntDayHopo;
  }
}

static int GenerateTTB(char *pcpSeas)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  FILE *pflTTBFilePtrArr = NULL;
  char pclTTBFileNameArr[256];
  FILE *pflTTBFilePtrDep = NULL;
  char pclTTBFileNameDep[256];
  char pclCurrentTime[32];
  char pclVpfr[32];
  char pclVpto[32];
  int ilLen;
  int ilNoFlights;
  char pclTimeDiff[8];

  dbg(TRACE,"================ Generate TIMETABLE Export File ===============");
  ilRC = TimeToStr(pclCurrentTime,time(NULL));

  sprintf(pclSqlBuf,"SELECT VPFR,VPTO FROM SEATAB WHERE SEAS = '%s'",pcpSeas);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);
  if (ilRCdb != DB_SUCCESS)
  {
     dbg(TRACE,"Season <%s> not found in SEATAB",pcpSeas);
     return RC_FAIL;
  }
  BuildItemBuffer(pclDataBuf,"",2,",");
  ilLen = get_real_item(pclVpfr,pclDataBuf,1);
  ilLen = get_real_item(pclVpto,pclDataBuf,2);

  if (strncmp(pclVpfr,pclVpto,4) == 0)
  {
     strcpy(pcgTimeDiffField,"TDIS");
  }
  else
  {
     strcpy(pcgTimeDiffField,"TDIW");
  }

  pcgTiDiHopo[0] = '\0';
  ilRC = GetTimeDiff(pcgTiDiHopo,&igTimeDiff,pcgHomeAp);
  igTimeDiffCount = 0;

  strcpy(pcgCreatedFileName,pcgTTBFileName);
  strcat(pcgCreatedFileName,"-ARR-");
  strcat(pcgCreatedFileName,pcpSeas);
  strcat(pcgCreatedFileName,"-");
  strcat(pcgCreatedFileName,pclCurrentTime);
  strcat(pcgCreatedFileName,".");
  strcat(pcgCreatedFileName,pcgTTBFileExt);
  strcpy(pclTTBFileNameArr,pcgTTBFilePath);
  strcat(pclTTBFileNameArr,"/");
  strcat(pclTTBFileNameArr,pcgCreatedFileName);
  strcpy(pcgCreatedFileNameArr,pcgCreatedFileName);
  pflTTBFilePtrArr = fopen(pclTTBFileNameArr,"w");
  if (pflTTBFilePtrArr == NULL)
  {
     dbg(TRACE,"Could not open file <%s>",pclTTBFileNameArr);
     return RC_FAIL;
  }
  else
  {
     dbg(TRACE,"FileName: <%s>",pclTTBFileNameArr);
  }

  strcpy(pcgCreatedFileName,pcgTTBFileName);
  strcat(pcgCreatedFileName,"-DEP-");
  strcat(pcgCreatedFileName,pcpSeas);
  strcat(pcgCreatedFileName,"-");
  strcat(pcgCreatedFileName,pclCurrentTime);
  strcat(pcgCreatedFileName,".");
  strcat(pcgCreatedFileName,pcgTTBFileExt);
  strcpy(pclTTBFileNameDep,pcgTTBFilePath);
  strcat(pclTTBFileNameDep,"/");
  strcat(pclTTBFileNameDep,pcgCreatedFileName);
  strcpy(pcgCreatedFileNameDep,pcgCreatedFileName);
  pflTTBFilePtrDep = fopen(pclTTBFileNameDep,"w");
  if (pflTTBFilePtrDep == NULL)
  {
     dbg(TRACE,"Could not open file <%s>",pclTTBFileNameDep);
     return RC_FAIL;
  }
  else
  {
     dbg(TRACE,"FileName: <%s>",pclTTBFileNameDep);
  }

  ilNoFlights = 0;
  sprintf(pclSqlBuf,"SELECT VAFR,VATO,FREQ,DOOP,ORG3,STOD,STOA,DES3,ALC3,FLTN,FLNS,ACT3,VIAN,VIAL FROM SSITAB ");
  sprintf(pclSelectBuf,"WHERE SEAS = '%s' AND ADID = 'A' ORDER BY ORG3,STOA,ALC3,FLTN,VAFR,VATO",pcpSeas);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",14,",");
     ilRC = StoreTTBFlightInfo(pcgFlightRecordTTB,pclDataBuf,"A",&ilNoFlights);
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);

  ilRC = UtcToLocalTTB(pcgFlightRecordTTB,ilNoFlights,"A");

  ilRC = SortTTB(pcgFlightRecordTTB,ilNoFlights,"A");

  fprintf(pflTTBFilePtrArr,"%s,%s\n",pcgCreatedFileNameArr,pcgCreatedFileNameDep);
  fprintf(pflTTBFilePtrArr,"Timetable booklet /Arrival/\n");
  ilRC = GenerateTTBFile(pflTTBFilePtrArr,pcgFlightRecordTTB,ilNoFlights,"A");
  dbg(TRACE,"No Airports: %d , No Flights: %d",igNoAirports,igNoFlights);

  fclose(pflTTBFilePtrArr);

  ilNoFlights = 0;
  sprintf(pclSqlBuf,"SELECT VAFR,VATO,FREQ,DOOP,ORG3,STOD,STOA,DES3,ALC3,FLTN,FLNS,ACT3,VIAN,VIAL FROM SSITAB ");
  sprintf(pclSelectBuf,"WHERE SEAS = '%s' AND ADID = 'D' ORDER BY DES3,STOD,ALC3,FLTN,VAFR,VATO",pcpSeas);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",14,",");
     ilRC = StoreTTBFlightInfo(pcgFlightRecordTTB,pclDataBuf,"D",&ilNoFlights);
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);

  ilRC = UtcToLocalTTB(pcgFlightRecordTTB,ilNoFlights,"D");

  ilRC = SortTTB(pcgFlightRecordTTB,ilNoFlights,"D");

  fprintf(pflTTBFilePtrDep,"%s,%s\n",pcgCreatedFileNameDep,pcgCreatedFileNameArr);
  fprintf(pflTTBFilePtrDep,"Timetable booklet /Departure/\n");
  ilRC = GenerateTTBFile(pflTTBFilePtrDep,pcgFlightRecordTTB,ilNoFlights,"D");
  dbg(TRACE,"No Airports: %d , No Flights: %d",igNoAirports,igNoFlights);

  fclose(pflTTBFilePtrDep);
  dbg(TRACE,"================ TIMETABLE Export File generated ==============");

  return ilRC;
} /* End of GenerateTTB */


static int StoreTTBFlightInfo(FLIGHT_RECORD_TTB *pcpFlight, char *pcpFlightRec,
                              char *pcpType, int *ipNoFlights)
{
  int ilRC = RC_SUCCESS;
  int ilLen;
  int ilI;
  int ilIdx;
  int ilSavIdx;
  int ilVian;
  char pclStoa[8];
  char pclStod[8];
  char pclFltn[8];
  char pclFlns[8];
  char pclAct3[8];
  char pclFlno[16];
  char pclOrg3[8];
  char pclDes3[8];
  char pclAlc3[8];

  ilIdx = *ipNoFlights;
  ilLen = get_real_item(&pcpFlight[ilIdx].pclVafr[0],pcpFlightRec,1);
  ilLen = get_real_item(&pcpFlight[ilIdx].pclVato[0],pcpFlightRec,2);
  ilLen = get_real_item(&pcpFlight[ilIdx].pclFreq[0],pcpFlightRec,3);
  if (pcpFlight[ilIdx].pclFreq[0] == '1')
  {
     strcpy(&pcpFlight[ilIdx].pclFreq[0],"weekly");
  }
  else
  {
     strcat(&pcpFlight[ilIdx].pclFreq[0],"weeks");
  }
  ilLen = get_real_item(&pcpFlight[ilIdx].pclDoop[0],pcpFlightRec,4);
  for (ilI = 0; ilI < 7; ilI++)
  {
     if (pcpFlight[ilIdx].pclDoop[ilI] == '0')
     {
        pcpFlight[ilIdx].pclDoop[ilI] = '.';
     }
  }
  ilLen = get_real_item(pclOrg3,pcpFlightRec,5);
  ilRC = GetCorrectedFormatBlank(&pcpFlight[ilIdx].pclOrg3[0],pclOrg3,3);
  ilLen = get_real_item(pclStod,pcpFlightRec,6);
  ilRC = GetCorrectedFormatBlank(&pcpFlight[ilIdx].pclStod[0],pclStod,4);
  ilLen = get_real_item(pclStoa,pcpFlightRec,7);
  ilRC = GetCorrectedFormatBlank(&pcpFlight[ilIdx].pclStoa[0],pclStoa,4);
  ilLen = get_real_item(pclDes3,pcpFlightRec,8);
  ilRC = GetCorrectedFormatBlank(&pcpFlight[ilIdx].pclDes3[0],pclDes3,3);
  ilLen = get_real_item(pclAlc3,pcpFlightRec,9);
  ilRC = GetCorrectedFormatBlank(&pcpFlight[ilIdx].pclAlc3[0],pclAlc3,3);
  ilLen = get_real_item(pclFltn,pcpFlightRec,10);
  ilRC = GetCorrectedFormatBlank(&pcpFlight[ilIdx].pclFltn[0],pclFltn,5);
  ilLen = get_real_item(pclFlns,pcpFlightRec,11);
  ilRC = GetCorrectedFormatBlank(&pcpFlight[ilIdx].pclFlns[0],pclFlns,1);
  ilLen = get_real_item(pclAct3,pcpFlightRec,12);
  ilRC = GetCorrectedFormatBlank(&pcpFlight[ilIdx].pclAct3[0],pclAct3,3);
  ilLen = get_real_item(&pcpFlight[ilIdx].pclVian[0],pcpFlightRec,13);
  ilLen = get_real_item(&pcpFlight[ilIdx].pclVial[0],pcpFlightRec,14);
  strcpy(pclFlno,&pcpFlight[ilIdx].pclAlc3[0]);
  strcat(pclFlno,&pcpFlight[ilIdx].pclFltn[0]);
  strcat(pclFlno,&pcpFlight[ilIdx].pclFlns[0]);
  strcpy(&pcpFlight[ilIdx].pclFlno[0],pclFlno);
  ilIdx++;

  if (pcpFlight[ilIdx-1].pclVian[0] != '0')
  {
     ilSavIdx = ilIdx - 1;
     ilVian = atoi(&pcpFlight[ilSavIdx].pclVian[0]);
     for (ilI = 0; ilI < ilVian; ilI++)
     {
        strcpy(&pcpFlight[ilIdx].pclVafr[0],&pcpFlight[ilSavIdx].pclVafr[0]);
        strcpy(&pcpFlight[ilIdx].pclVato[0],&pcpFlight[ilSavIdx].pclVato[0]);
        strcpy(&pcpFlight[ilIdx].pclFreq[0],&pcpFlight[ilSavIdx].pclFreq[0]);
        strcpy(&pcpFlight[ilIdx].pclDoop[0],&pcpFlight[ilSavIdx].pclDoop[0]);
        strcpy(&pcpFlight[ilIdx].pclAlc3[0],&pcpFlight[ilSavIdx].pclAlc3[0]);
        strcpy(&pcpFlight[ilIdx].pclFltn[0],&pcpFlight[ilSavIdx].pclFltn[0]);
        strcpy(&pcpFlight[ilIdx].pclFlns[0],&pcpFlight[ilSavIdx].pclFlns[0]);
        strcpy(&pcpFlight[ilIdx].pclAct3[0],&pcpFlight[ilSavIdx].pclAct3[0]);
        strcpy(&pcpFlight[ilIdx].pclFlno[0],&pcpFlight[ilSavIdx].pclFlno[0]);
        if (strcmp(pcpType,"A") == 0)
        {
           strncpy(pclOrg3,&pcpFlight[ilSavIdx].pclVial[ilI*15],3);
           pclOrg3[3] = '\0';
           ilRC = GetCorrectedFormatBlank(&pcpFlight[ilIdx].pclOrg3[0],pclOrg3,3);
           strcpy(pclDes3,&pcpFlight[ilSavIdx].pclDes3[0]);
           ilRC = GetCorrectedFormatBlank(&pcpFlight[ilIdx].pclDes3[0],pclDes3,3);
        }
        else
        {
           strcpy(pclOrg3,&pcpFlight[ilSavIdx].pclOrg3[0]);
           ilRC = GetCorrectedFormatBlank(&pcpFlight[ilIdx].pclOrg3[0],pclOrg3,3);
           strncpy(pclDes3,&pcpFlight[ilSavIdx].pclVial[ilI*15],3);
           pclDes3[3] = '\0';
           ilRC = GetCorrectedFormatBlank(&pcpFlight[ilIdx].pclDes3[0],pclDes3,3);
        }
        if (strlen(&pcpFlight[ilSavIdx].pclVial[0]) >= ilI * 15 + 14)
        {
           strncpy(&pcpFlight[ilIdx].pclStod[0],&pcpFlight[ilSavIdx].pclVial[ilI*15+11],4);
           pcpFlight[ilIdx].pclStod[4] = '\0';
        }
        else
        {
           strcpy(&pcpFlight[ilIdx].pclStod[0],"    ");
        }
        if (strlen(&pcpFlight[ilSavIdx].pclVial[0]) >= ilI * 15 + 10)
        {
           strncpy(&pcpFlight[ilIdx].pclStoa[0],&pcpFlight[ilSavIdx].pclVial[ilI*15+7],4);
           pcpFlight[ilIdx].pclStod[4] = '\0';
        }
        else
        {
           strcpy(&pcpFlight[ilIdx].pclStoa[0],"    ");
        }
        sprintf(&pcpFlight[ilIdx].pclVian[0],"%d",ilVian-ilI-1);
        ilIdx++;
     }
  }

  *ipNoFlights = ilIdx;
  return ilRC;
} /* End of StoreTTBFlightInfo */


static int GenerateTTBFile(FILE *pfpFilePtr, FLIGHT_RECORD_TTB *pcpFlight,
                           int ipNoFlights, char *pcpType)
{
  int ilRC = RC_SUCCESS;
  int ilI;
  char pclAirport[8];
  char pclOutputLine[256];
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  int ilNewAirport = FALSE;
  char pclAirportName[64];
  char pclAirportType[8];
  char pclTimeDiff[8];
  char pclField1[64];
  char pclField2[8];
  int ilTimeDiff;
  int ilLen;
  int ilJ;

  strcpy(pclAirport,"");
  igNoAirports = 0;
  igNoFlights = 0;

  for (ilI = 0; ilI < ipNoFlights; ilI++)
  {
     ilNewAirport = FALSE;
     if (strcmp(pcpType,"A") == 0)
     {
        if (strcmp(&pcpFlight[ilI].pclOrg3[0],pclAirport) != 0)
        {
           strcpy(pclAirport,&pcpFlight[ilI].pclOrg3[0]);
           ilNewAirport = TRUE;
        }
     }
     else
     {
        if (strcmp(&pcpFlight[ilI].pclDes3[0],pclAirport) != 0)
        {
           strcpy(pclAirport,&pcpFlight[ilI].pclDes3[0]);
           ilNewAirport = TRUE;
        }
     }
     if (ilNewAirport == TRUE)
     {
        strcpy(pclField1," ");
        strcpy(pclField2," ");
        ilTimeDiff = igTimeDiff;
        for (ilJ = 0; ilJ < igTimeDiffCount; ilJ++)
        {
           if (strcmp(&pcgTimeDiff[ilJ].pclApc3[0],pclAirport) == 0)
           {
              strcpy(pclField1,&pcgTimeDiff[ilJ].pclApfn[0]);
              strcpy(pclField2,&pcgTimeDiff[ilJ].pclAptt[0]);
              strcpy(pclTimeDiff,&pcgTimeDiff[ilJ].pclTiDi[0]);
              ilJ = igTimeDiffCount;
           }
        }
        ilRC = GetCorrectedFormatBlank(pclAirportName,pclField1,32);
        ilRC = GetCorrectedFormatBlank(pclAirportType,pclField2,1);
        fprintf(pfpFilePtr,"--------------------------------------------------------------------\n");
        fprintf(pfpFilePtr,"%s,%s,%s,%s\n",
                pclAirport,pclAirportName,pclAirportType,pclTimeDiff);
        fprintf(pfpFilePtr,"--------------------------------------------------------------------\n");
        igNoAirports++;
     }
     strcpy(pclOutputLine,&pcpFlight[ilI].pclVafr[0]);
     strcat(pclOutputLine,",");
     strcat(pclOutputLine,&pcpFlight[ilI].pclVato[0]);
     strcat(pclOutputLine,",");
     strcat(pclOutputLine,&pcpFlight[ilI].pclFreq[0]);
     strcat(pclOutputLine,",");
     strcat(pclOutputLine,&pcpFlight[ilI].pclDoop[0]);
     strcat(pclOutputLine,",");
     strcat(pclOutputLine,&pcpFlight[ilI].pclOrg3[0]);
     strcat(pclOutputLine,",");
     strcat(pclOutputLine,&pcpFlight[ilI].pclStod[0]);
     strcat(pclOutputLine,",");
     strcat(pclOutputLine,&pcpFlight[ilI].pclStoa[0]);
     strcat(pclOutputLine,",");
     strcat(pclOutputLine,&pcpFlight[ilI].pclDes3[0]);
     strcat(pclOutputLine,",");
     strcat(pclOutputLine,&pcpFlight[ilI].pclAlc3[0]);
     strcat(pclOutputLine,",");
     strcat(pclOutputLine,&pcpFlight[ilI].pclFltn[0]);
     strcat(pclOutputLine,",");
     strcat(pclOutputLine,&pcpFlight[ilI].pclFlns[0]);
     strcat(pclOutputLine,",");
     strcat(pclOutputLine,&pcpFlight[ilI].pclAct3[0]);
     strcat(pclOutputLine,",");
     strcat(pclOutputLine,&pcpFlight[ilI].pclVian[0]);
     fprintf(pfpFilePtr,"%s\n",pclOutputLine);
     igNoFlights++;
  }

  fprintf(pfpFilePtr,"--------------------------------------------------------------------\n");

  return ilRC;
} /* End of GenerateTTBFile */


static int SortTTB(FLIGHT_RECORD_TTB *pcpFlight, int ipCount, char *pcpType)
{
  int ilRC = RC_SUCCESS;
  int ilI;

  if (strcmp(pcpType,"A") == 0)
  {
     ilRC = SortTTBField(pcpFlight,ipCount,NULL,
                         NULL,pcpFlight->pclOrg3);
     ilRC = SortTTBField(pcpFlight,ipCount,NULL,
                         pcpFlight->pclOrg3,pcpFlight->pclStoa);
     ilRC = SortTTBField(pcpFlight,ipCount,pcpFlight->pclOrg3,
                         pcpFlight->pclStoa,pcpFlight->pclFlno);
  }
  else
  {
     ilRC = SortTTBField(pcpFlight,ipCount,NULL,
                         NULL,pcpFlight->pclDes3);
     ilRC = SortTTBField(pcpFlight,ipCount,NULL,
                         pcpFlight->pclDes3,pcpFlight->pclStod);
     ilRC = SortTTBField(pcpFlight,ipCount,pcpFlight->pclDes3,
                         pcpFlight->pclStod,pcpFlight->pclFlno);
  }

  return ilRC;
} /* End of SortTTB */


static int SortTTBField(FLIGHT_RECORD_TTB *pcpFlight, int ipCount,
                        char *pcpField1, char *pcpField2, char *pcpField3)
{
  int ilRC = RC_SUCCESS;
  int ilI;
  int ilJ;
  int ilK;
  FLIGHT_RECORD_TTB pclSaveFlight;
  FLIGHT_RECORD_TTB *pclField1;
  FLIGHT_RECORD_TTB *pclField2;
  FLIGHT_RECORD_TTB *pclField3;

  pclField1 = (FLIGHT_RECORD_TTB *)pcpField1;
  pclField2 = (FLIGHT_RECORD_TTB *)pcpField2;
  pclField3 = (FLIGHT_RECORD_TTB *)pcpField3;

  for (ilI = 1; ilI < ipCount; ilI++)
  {
     ilJ = ilI -1;
     if (pcpField1 == NULL && pcpField2 == NULL)
     {
        while (ilJ >= 0 &&
               strcmp(&pclField3[ilI].pclVafr[0],&pclField3[ilJ].pclVafr[0]) < 0)
        {
           ilJ--;
        }
     }
     else
     {
        if (pcpField1 == NULL)
        {
           while (ilJ >= 0 &&
                  strcmp(&pclField2[ilI].pclVafr[0],&pclField2[ilJ].pclVafr[0]) == 0 &&
                  strcmp(&pclField3[ilI].pclVafr[0],&pclField3[ilJ].pclVafr[0]) < 0)
           {
              ilJ--;
           }
        }
        else
        {
           while (ilJ >= 0 &&
                  strcmp(&pclField1[ilI].pclVafr[0],&pclField1[ilJ].pclVafr[0]) == 0 &&
                  strcmp(&pclField2[ilI].pclVafr[0],&pclField2[ilJ].pclVafr[0]) == 0 &&
                  strcmp(&pclField3[ilI].pclVafr[0],&pclField3[ilJ].pclVafr[0]) < 0)
           {
              ilJ--;
           }
        }
     }
     if (ilJ < ilI - 1)
     {
        memcpy((char *)&pclSaveFlight.pclVafr[0],
               (char *)&pcpFlight[ilI].pclVafr[0],FLIGHT_RECORD_SIZE_TTB);
        for (ilK = ilI - 1; ilK >= ilJ + 1; ilK--)
        {
           memcpy((char *)&pcpFlight[ilK+1].pclVafr[0],
                  (char *)&pcpFlight[ilK].pclVafr[0],FLIGHT_RECORD_SIZE_TTB);
        }
        memcpy((char *)&pcpFlight[ilJ+1].pclVafr[0],
               (char *)&pclSaveFlight.pclVafr[0],FLIGHT_RECORD_SIZE_TTB);
     }
  }

  return ilRC;
} /* End of SortTTBField */


static int UtcToLocalTTB(FLIGHT_RECORD_TTB *pcpFlight,int ipNoFlights, char *pcpType)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  char pclDate[16];
  int ilI;
  int ilJ;
  int ilK;
  int ilNextDayStoa;
  int ilNextDayStod;
  int ilDoop;
  char pclDoopOld[8];
  char pclDoopNew[8];
  int ilLen;
  char pclTimeDiff[8];
  char pclLastAirport[8];
  char pclCurAirport[8];
  int ilTimeDiff;

  strcpy(pclLastAirport,"   ");
  for (ilI = 0; ilI < ipNoFlights; ilI++)
  {
     if (strcmp(pcpType,"A") == 0)
     {
        strcpy(pclCurAirport,&pcpFlight[ilI].pclOrg3[0]);
     }
     else
     {
        strcpy(pclCurAirport,&pcpFlight[ilI].pclDes3[0]);
     }
     if (strcmp(pclLastAirport,pclCurAirport) != 0)
     {
        strcpy(pclLastAirport,pclCurAirport);
        ilRC = GetTimeDiff(pclTimeDiff,&ilTimeDiff,pclCurAirport);
     }
     ilNextDayStoa = 0;
     ilNextDayStod = 0;
     strcpy(pclDate,&pcpFlight[ilI].pclVafr[0]);
     if (pcpFlight[ilI].pclStoa[0] != ' ')
     {
        strcat(pclDate,&pcpFlight[ilI].pclStoa[0]);
        strcat(pclDate,"00");
        if (strcmp(pcpType,"A") == 0)
        {
           ilRC = AddSecondsToCEDATime(pclDate,igTimeDiff*60,1);
        }
        else
        {
           ilRC = AddSecondsToCEDATime(pclDate,ilTimeDiff*60,1);
        }
        strncpy(&pcpFlight[ilI].pclStoa[0],&pclDate[8],4);
        pcpFlight[ilI].pclStoa[4] = '\0';
        pclDate[8] = '\0';
        if (strcmp(pclDate,&pcpFlight[ilI].pclVafr[0]) < 0)
        {
           ilNextDayStoa = -1;
        }
        else
        {
           if (strcmp(pclDate,&pcpFlight[ilI].pclVafr[0]) > 0)
           {
              ilNextDayStoa = 1;
           }
        }
     }
     strcpy(pclDate,&pcpFlight[ilI].pclVafr[0]);
     if (pcpFlight[ilI].pclStod[0] != ' ')
     {
        strcat(pclDate,&pcpFlight[ilI].pclStod[0]);
        strcat(pclDate,"00");
        if (strcmp(pcpType,"D") == 0)
        {
           ilRC = AddSecondsToCEDATime(pclDate,igTimeDiff*60,1);
        }
        else
        {
           ilRC = AddSecondsToCEDATime(pclDate,ilTimeDiff*60,1);
        }
        strncpy(&pcpFlight[ilI].pclStod[0],&pclDate[8],4);
        pcpFlight[ilI].pclStod[4] = '\0';
        pclDate[8] = '\0';
        if (strcmp(pclDate,&pcpFlight[ilI].pclVafr[0]) < 0)
        {
           ilNextDayStod = -1;
        }
        else
        {
           if (strcmp(pclDate,&pcpFlight[ilI].pclVafr[0]) > 0)
           {
              ilNextDayStod = 1;
           }
        }
     }
     ilDoop = 0;
     if (ilNextDayStoa != 0 && strcmp(pcpType,"A") == 0)
     {
        ilDoop = ilNextDayStoa;
     }
     if (ilNextDayStod != 0 && strcmp(pcpType,"D") == 0)
     {
        ilDoop = ilNextDayStod;
     }
     if (ilDoop != 0)
     {
        strcpy(pclDoopOld,&pcpFlight[ilI].pclDoop[0]);
        strcpy(pclDoopNew,".......");
        for (ilJ = 1; ilJ <= 7; ilJ++)
        {
           if (pclDoopOld[ilJ-1] != '.')
           {
              ilK = ilJ + ilDoop;
              if (ilK == 0)
              {
                 ilK = 7;
              }
              if (ilK == 8)
              {
                 ilK = 1;
              }
           pclDoopNew[ilK-1] = ilK + 0x30;
           }
        }
        strcpy(&pcpFlight[ilI].pclDoop[0],pclDoopNew);
        strcpy(pclDate,&pcpFlight[ilI].pclVafr[0]);
        strcat(pclDate,"120000");
        ilRC = AddSecondsToCEDATime(pclDate,24*60*60*ilDoop,1);
        pclDate[8] = '\0';
        strcpy(&pcpFlight[ilI].pclVafr[0],pclDate);
        strcpy(pclDate,&pcpFlight[ilI].pclVato[0]);
        strcat(pclDate,"120000");
        ilRC = AddSecondsToCEDATime(pclDate,24*60*60*ilDoop,1);
        pclDate[8] = '\0';
        strcpy(&pcpFlight[ilI].pclVato[0],pclDate);
     }
  }

  return ilRC;
} /* End of UtcToLocalTTB */


static int GenerateFLUKO(char *pcpSeas)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  FILE *pflFLUKOFilePtr = NULL;
  char pclFLUKOFileName[256];
  char pclCurrentTime[32];
  char pclVpfr[32];
  char pclVpto[32];
  int ilLen;
  int ilCurLine;
  char pclSeas[8];
  char pclTmpStr[8];
  char pclCurLine[8];

  dbg(TRACE,"================== Generate FLUKO Export File ==================");
  ilRC = TimeToStr(pclCurrentTime,time(NULL));
  strcpy(pcgCreatedFileName,pcgFLUKOFileName);
  strcat(pcgCreatedFileName,"-");
  strcat(pcgCreatedFileName,pcpSeas);
  strcat(pcgCreatedFileName,"-");
  strcat(pcgCreatedFileName,pclCurrentTime);
  strcat(pcgCreatedFileName,".");
  strcat(pcgCreatedFileName,pcgFLUKOFileExt);
  strcpy(pclFLUKOFileName,pcgFLUKOFilePath);
  strcat(pclFLUKOFileName,"/");
  strcat(pclFLUKOFileName,pcgCreatedFileName);
  pflFLUKOFilePtr = fopen(pclFLUKOFileName,"w");
  if (pflFLUKOFilePtr == NULL)
  {
     dbg(TRACE,"Could not open file <%s>",pclFLUKOFileName);
     return RC_FAIL;
  }
  else
  {
     dbg(TRACE,"FileName: <%s>",pclFLUKOFileName);
  }

  sprintf(pclSqlBuf,"SELECT VPFR,VPTO FROM SEATAB WHERE SEAS = '%s'",pcpSeas);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);
  if (ilRCdb != DB_SUCCESS)
  {
     dbg(TRACE,"Season <%s> not found in SEATAB",pcpSeas);
     return RC_FAIL;
  }

  BuildItemBuffer(pclDataBuf,"",2,",");
  ilLen = get_real_item(pclVpfr,pclDataBuf,1);
  ilLen = get_real_item(pclVpto,pclDataBuf,2);

  fprintf(pflFLUKOFilePtr,"%s\n",pcgCreatedFileName);
  strcpy(pclSeas,pcpSeas);
  pclSeas[3] = '\0';
  fprintf(pflFLUKOFilePtr,"SN;FHK;%s;;S;%s;%s;%s;UTC;\n",
          pcgHomeAp,pclSeas,pclVpfr,pclVpto);

  ilCurLine = 0;
  sprintf(pclSqlBuf,"SELECT FLTA,STOA,VAFR,VATO,DOOP,ORG3,VI3A,ACT3,TTPA,FREQ,FLTD,STOD,NSTP FROM SSRTAB ");
  sprintf(pclSelectBuf,"WHERE SEAS = '%s' AND FLTA <> ' ' ORDER BY STOA,FLTA,VAFR,VATO",pcpSeas);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",13,",");
     ilRC = GetFLUKODataLine(pflFLUKOFilePtr,pclDataBuf,"A");
     ilCurLine++;
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);
  sprintf(pclSqlBuf,"SELECT FLTD,STOD,VAFR,VATO,DOOP,DES3,VI3D,ACT3,TTPD,FREQ,FLTA,STOA,NSTP FROM SSRTAB ");
  sprintf(pclSelectBuf,"WHERE SEAS = '%s' AND FLTD <> ' ' ORDER BY STOD,FLTD,VAFR,VATO",pcpSeas);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",13,",");
     ilRC = GetFLUKODataLine(pflFLUKOFilePtr,pclDataBuf,"D");
     ilCurLine++;
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);

  sprintf(pclTmpStr,"%d",ilCurLine);
  ilRC = GetCorrectedFormatZero(pclCurLine,pclTmpStr,6);
  fprintf(pflFLUKOFilePtr,"E;%s\n",pclCurLine);

  fclose(pflFLUKOFilePtr);
  dbg(TRACE,"================== FLUKO Export File generated =================");

  return ilRC;
} /* End of GenerateFLUKO */


static int GetFLUKODataLine(FILE *pfpFLUKOFilePtr, char *pcpDataBuf, char *pcpType)
{
  int ilRC = RC_SUCCESS;
  int ilLen;
  char pclFlno[16];
  char pclScht[8];
  char pclVafr[16];
  char pclVato[16];
  char pclDoop[8];
  char pclApc3[8];
  char pclVia3[8];
  char pclAct3[8];
  char pclTtyp[8];
  char pclFreq[8];
  char pclRotf[16];
  char pclRott[8];
  char pclNstp[8];
  char pclTmpStr[16];
  char pclOutputLine[256];

  ilLen = get_real_item(pclFlno,pcpDataBuf,1);
  ilLen = get_real_item(pclScht,pcpDataBuf,2);
  ilLen = get_real_item(pclVafr,pcpDataBuf,3);
  ilLen = get_real_item(pclVato,pcpDataBuf,4);
  ilLen = get_real_item(pclDoop,pcpDataBuf,5);
  ilLen = get_real_item(pclApc3,pcpDataBuf,6);
  ilLen = get_real_item(pclVia3,pcpDataBuf,7);
  ilLen = get_real_item(pclAct3,pcpDataBuf,8);
  ilLen = get_real_item(pclTtyp,pcpDataBuf,9);
  ilLen = get_real_item(pclFreq,pcpDataBuf,10);
  ilLen = get_real_item(pclRotf,pcpDataBuf,11);
  ilLen = get_real_item(pclRott,pcpDataBuf,12);
  ilLen = get_real_item(pclNstp,pcpDataBuf,13);
  pclTtyp[1] = '\0';
  ilRC = GetCorrectedFormatBlank(pclTmpStr,pclFlno,9);
  strncpy(pclFlno,pclTmpStr,3);
  pclFlno[3] = '\0';
  if (pclTmpStr[6] == ' ')
  {
     strcat(pclFlno," ");
     strncat(pclFlno,&pclTmpStr[3],3);
     pclFlno[7] = '\0';
  }
  else
  {
     strncat(pclFlno,&pclTmpStr[3],4);
     pclFlno[7] = '\0';
  }
  if (pclTmpStr[8] != ' ')
  {
     pclFlno[7] = pclTmpStr[8];
     pclFlno[8] = '\0';
  }
  if (strlen(pclRotf) > 0)
  {
     ilRC = GetCorrectedFormatBlank(pclTmpStr,pclRotf,9);
     strncpy(pclRotf,pclTmpStr,3);
     pclRotf[3] = '\0';
     if (pclTmpStr[6] == ' ')
     {
        strcat(pclRotf," ");
        strncat(pclRotf,&pclTmpStr[3],3);
        pclRotf[7] = '\0';
     }
     else
     {
        strncat(pclRotf,&pclTmpStr[3],4);
        pclRotf[7] = '\0';
     }
     if (pclTmpStr[8] != ' ')
     {
        pclRotf[7] = pclTmpStr[8];
        pclRotf[8] = '\0';
     }
  }

  strcpy(pclOutputLine,"N;");
  strcat(pclOutputLine,pcgHomeAp);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclFlno);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pcpType);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclScht);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclVafr);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclVato);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclDoop);
  strcat(pclOutputLine,";");
  if (strlen(pclVia3) > 0)
  {
     strcat(pclOutputLine,pclVia3);
     strcat(pclOutputLine,";");
     strcat(pclOutputLine,pclApc3);
     strcat(pclOutputLine,";");
  }
  else
  {
     strcat(pclOutputLine,pclApc3);
     strcat(pclOutputLine,";;");
  }
  strcat(pclOutputLine,pclAct3);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclTtyp);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclFreq);
  strcat(pclOutputLine,";");
  if (strlen(pclRotf) > 0)
  {
     strcat(pclOutputLine,pclRotf);
     strcat(pclOutputLine,";");
     strcat(pclOutputLine,pclRott);
     strcat(pclOutputLine,";");
     strcat(pclOutputLine,pclNstp);
     strcat(pclOutputLine,";");
  }
  else
  {
     strcat(pclOutputLine,";;;");
  }

  fprintf(pfpFLUKOFilePtr,"%s\n",pclOutputLine);

  return ilRC;
} /* End of GetFLUKODataLine */


static int GenerateSLOTCO(char *pcpSeas)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  FILE *pflSLOTCOFilePtr = NULL;
  char pclSLOTCOFileName[256];
  char pclCurrentTime[32];
  char pclVpfr[32];
  char pclVpto[32];
  int ilLen;

  dbg(TRACE,"================== Generate SLOTCO Export File ==================");
  ilRC = TimeToStr(pclCurrentTime,time(NULL));
  strcpy(pcgCreatedFileName,pcgSLOTCOFileName);
  strcat(pcgCreatedFileName,"-");
  strcat(pcgCreatedFileName,pcpSeas);
  strcat(pcgCreatedFileName,"-");
  strcat(pcgCreatedFileName,pclCurrentTime);
  strcat(pcgCreatedFileName,".");
  strcat(pcgCreatedFileName,pcgSLOTCOFileExt);
  strcpy(pclSLOTCOFileName,pcgSLOTCOFilePath);
  strcat(pclSLOTCOFileName,"/");
  strcat(pclSLOTCOFileName,pcgCreatedFileName);
  pflSLOTCOFilePtr = fopen(pclSLOTCOFileName,"w");
  if (pflSLOTCOFilePtr == NULL)
  {
     dbg(TRACE,"Could not open file <%s>",pclSLOTCOFileName);
     return RC_FAIL;
  }
  else
  {
     dbg(TRACE,"FileName: <%s>",pclSLOTCOFileName);
  }

  sprintf(pclSqlBuf,"SELECT VPFR,VPTO FROM SEATAB WHERE SEAS = '%s'",pcpSeas);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);
  if (ilRCdb != DB_SUCCESS)
  {
     dbg(TRACE,"Season <%s> not found in SEATAB",pcpSeas);
     return RC_FAIL;
  }

  BuildItemBuffer(pclDataBuf,"",2,",");
  ilLen = get_real_item(pclVpfr,pclDataBuf,1);
  ilLen = get_real_item(pclVpto,pclDataBuf,2);

  fprintf(pflSLOTCOFilePtr,"%s\n",pcgCreatedFileName);
  fprintf(pflSLOTCOFilePtr,"%s\n",pcgSLOTCOHeader);

  sprintf(pclSqlBuf,"SELECT FLTA,FLTD,VAFR,VATO,DOOP,ACT3,ORG3,VI3A,STOA,STOD,NSTP,VI3D,DES3,TTPA,TTPD,FREQ FROM SSRTAB ");
  sprintf(pclSelectBuf,"WHERE SEAS = '%s' ORDER BY FTYP,FLTA,STOA,FLTD,STOD,VAFR,VATO",pcpSeas);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",16,",");
     ilRC = GetSLOTCODataLine(pflSLOTCOFilePtr,pclDataBuf);
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);

  fclose(pflSLOTCOFilePtr);
  dbg(TRACE,"================== SLOTCO Export File generated =================");

  return ilRC;
} /* End of GenerateSLOTCO */


static int GetSLOTCODataLine(FILE *pfpSLOTCOFilePtr, char *pcpDataBuf)
{
  int ilRC = RC_SUCCESS;
  int ilLen;
  int ilI;
  char pclFlta[16];
  char pclFltd[16];
  char pclVafr[16];
  char pclVato[16];
  char pclDoop[8];
  char pclAct3[8];
  char pclOrg3[8];
  char pclVi3a[8];
  char pclStoa[8];
  char pclStod[8];
  char pclNstp[8];
  char pclVi3d[8];
  char pclDes3[8];
  char pclTtpa[8];
  char pclTtpd[8];
  char pclFreq[8];
  char pclTmpStr[16];
  char pclFltaA[8];
  char pclFltaN[8];
  char pclFltdA[8];
  char pclFltdN[8];
  char pclVafrD[8];
  char pclVafrM[8];
  char pclVafrDMY[16];
  char pclVatoD[8];
  char pclVatoM[8];
  char pclVatoDMY[16];
  char pclTtyp[8];
  char pclStoaH[4];
  char pclStoaM[4];
  char pclStodH[4];
  char pclStodM[4];
  char pclDoopF[32];
  char pclOutputLine[256];

  ilLen = get_real_item(pclFlta,pcpDataBuf,1);
  ilLen = get_real_item(pclFltd,pcpDataBuf,2);
  ilLen = get_real_item(pclVafr,pcpDataBuf,3);
  ilLen = get_real_item(pclVato,pcpDataBuf,4);
  ilLen = get_real_item(pclDoop,pcpDataBuf,5);
  ilLen = get_real_item(pclAct3,pcpDataBuf,6);
  ilLen = get_real_item(pclOrg3,pcpDataBuf,7);
  ilLen = get_real_item(pclVi3a,pcpDataBuf,8);
  ilLen = get_real_item(pclStoa,pcpDataBuf,9);
  ilLen = get_real_item(pclStod,pcpDataBuf,10);
  ilLen = get_real_item(pclNstp,pcpDataBuf,11);
  ilLen = get_real_item(pclVi3d,pcpDataBuf,12);
  ilLen = get_real_item(pclDes3,pcpDataBuf,13);
  ilLen = get_real_item(pclTtpa,pcpDataBuf,14);
  ilLen = get_real_item(pclTtpd,pcpDataBuf,15);
  ilLen = get_real_item(pclFreq,pcpDataBuf,16);

  pclTtpa[1] = '\0';
  if (strlen(pclTtpa) == 0)
  {
     strcpy(pclTtpa," ");
  }
  pclTtpd[1] = '\0';
  if (strlen(pclTtpd) == 0)
  {
     strcpy(pclTtpd," ");
  }
  strcpy(pclTtyp,pclTtpa);
  strcat(pclTtyp,pclTtpd);

  if (strlen(pclFlta) > 0)
  {
     ilRC = GetCorrectedFormatBlank(pclTmpStr,pclFlta,9);
     strncpy(pclFltaA,pclTmpStr,3);
     pclFltaA[3] = '\0';
     if (pclTmpStr[6] == ' ')
     {
        strcpy(pclFltaN," ");
        strncat(pclFltaN,&pclTmpStr[3],3);
        pclFltaN[4] = '\0';
     }
     else
     {
        strncpy(pclFltaN,&pclTmpStr[3],4);
        pclFltaN[4] = '\0';
     }
     if (pclTmpStr[8] != ' ')
     {
        pclFltaN[4] = pclTmpStr[8];
        pclFltaN[5] = '\0';
     }
  }
  else
  {
     pclFltaA[0] = '\0';
     pclFltaN[0] = '\0';
  }
  if (strlen(pclFltd) > 0)
  {
     ilRC = GetCorrectedFormatBlank(pclTmpStr,pclFltd,9);
     strncpy(pclFltdA,pclTmpStr,3);
     pclFltdA[3] = '\0';
     if (pclTmpStr[6] == ' ')
     {
        strcpy(pclFltdN," ");
        strncat(pclFltdN,&pclTmpStr[3],3);
        pclFltdN[4] = '\0';
     }
     else
     {
        strncpy(pclFltdN,&pclTmpStr[3],4);
        pclFltdN[4] = '\0';
     }
     if (pclTmpStr[8] != ' ')
     {
        pclFltdN[4] = pclTmpStr[8];
        pclFltdN[5] = '\0';
     }
  }
  else
  {
     pclFltdA[0] = '\0';
     pclFltdN[0] = '\0';
  }

  strcpy(pclVafrD,&pclVafr[6]);
  ilRC = GetMonthAlpha(pclVafrM,pclVafr);
  strcpy(pclVafrDMY,&pclVafr[6]);
  strcat(pclVafrDMY,"/");
  strncpy(&pclVafrDMY[3],&pclVafr[4],2);
  pclVafrDMY[5] = '\0';
  strcat(pclVafrDMY,"/");
  strncpy(&pclVafrDMY[6],&pclVafr[2],2);
  pclVafrDMY[8] = '\0';
  strcpy(pclVatoD,&pclVato[6]);
  ilRC = GetMonthAlpha(pclVatoM,pclVato);
  strcpy(pclVatoDMY,&pclVato[6]);
  strcat(pclVatoDMY,"/");
  strncpy(&pclVatoDMY[3],&pclVato[4],2);
  pclVatoDMY[5] = '\0';
  strcat(pclVatoDMY,"/");
  strncpy(&pclVatoDMY[6],&pclVato[2],2);
  pclVatoDMY[8] = '\0';

  memset(pclDoopF,0x00,32);
  for (ilI = 0; ilI < 7; ilI++)
  {
     if (pclDoop[ilI] != '0')
     {
        pclDoopF[strlen(pclDoopF)] = pclDoop[ilI];
     }
     if (ilI < 6)
     {
        strcat(pclDoopF,";");
     }
  }

  if (strlen(pclVi3a) == 0)
  {
     strcpy(pclVi3a,pclOrg3);
  }
  if (strlen(pclVi3d) == 0)
  {
     strcpy(pclVi3d,pclDes3);
  }

  if (strlen(pclStoa) > 0)
  {
     strncpy(pclStoaH,pclStoa,2);
     pclStoaH[2] = '\0';
     strcpy(pclStoaM,&pclStoa[2]);
  }
  else
  {
     pclStoaH[0] = '\0';
     pclStoaM[0] = '\0';
  }
  if (strlen(pclStod) > 0)
  {
     strncpy(pclStodH,pclStod,2);
     pclStodH[2] = '\0';
     strcpy(pclStodM,&pclStod[2]);
  }
  else
  {
     pclStodH[0] = '\0';
     pclStodM[0] = '\0';
  }

  if (pclNstp[0] == '0')
  {
     pclNstp[0] = '\0';
  }

  strcpy(pclOutputLine,pclFltaA);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclFltaN);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclFltdA);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclFltdN);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclVafrD);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclVafrM);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclVatoD);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclVatoM);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclDoopF);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclAct3);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclOrg3);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclVi3a);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclStoaH);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclStoaM);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclStodH);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclStodM);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclNstp);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclVi3d);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclDes3);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclTtyp);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclFreq);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclVafrDMY);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,pclVatoDMY);
  strcat(pclOutputLine,";");
  strcat(pclOutputLine,";;;;;;;;;;;;");

  fprintf(pfpSLOTCOFilePtr,"%s\n",pclOutputLine);

  return ilRC;
} /* End of GetSLOTCODataLine */


static int HandleData(char *pcpCmd, char *pcpTable, char *pcpSelection, char *pcpFields,
                      char *pcpData)
{
  int ilRC = RC_SUCCESS;

  if (strcmp(pcpFields,"FILENAMES") == 0)
  {
     ilRC = GetFileNames(pcpCmd,pcpTable,pcpSelection);
  }
  if (strcmp(pcpFields,"READ") == 0)
  {
     ilRC = ReadFile(pcpCmd,pcpTable,pcpSelection);
  }
  if (strcmp(pcpFields,"CREATE") == 0)
  {
     ilRC = CreateFile(pcpCmd,pcpTable,pcpSelection);
  }
  if (strcmp(pcpFields,"DELETE") == 0)
  {
     ilRC = DeleteFile(pcpCmd,pcpTable,pcpSelection);
  }

  return ilRC;
} /* End of HandleData */


static int GetFileNames(char *pcpCmd, char *pcpTable, char *pcpSelection)
{
  int ilRC = RC_SUCCESS;
  char pclData[64000];
  DIR *prlDirP;
  struct dirent *prlDirEntP;
  char pclDirName[256];
  char pclFileName[256];

  ilRC = GetDirectory(pclDirName,pclFileName,pcpTable);
  pclData[0] = '\0';
  if ((prlDirP = opendir(pclDirName)) != NULL)
  {
     while ((prlDirEntP = readdir(prlDirP)) != NULL)
     {
        if (strstr(prlDirEntP->d_name,pclFileName) != NULL)
        {
           if (strlen(pcpSelection) == 0 || strlen(pcgSSIMFileName) == 0)
           {
              if (prlDirEntP->d_name[0] != '.')
              {
                 strcat(pclData,prlDirEntP->d_name);
                 strcat(pclData,"\n");
              }
           }
           else
           {
              if (strstr(prlDirEntP->d_name,pcpSelection) != NULL)
              {
                 strcat(pclData,prlDirEntP->d_name);
                 strcat(pclData,"\n");
              }
           }
        }
     }
     closedir(prlDirP);
  }
  pclData[strlen(pclData)-1] = '\0';

  (void) tools_send_info_flag(que_out,0,"ssihdl","","CEDA","","",pcgTwStart,pcgTwEnd,
                              pcpCmd,pcpTable,pcpSelection,"",pclData,0);

  return ilRC;
} /* End of GetFileNames */


static int ReadFile(char *pcpCmd, char *pcpTable, char *pcpSelection)
{
  int ilRC = RC_SUCCESS;
  char pclData[MAX_TEXT_FILE_SIZE];
  FILE *pflFilePtr = NULL;
  char pclFileName[256];
  char pclDummy[256];

  ilRC = GetDirectory(pclFileName,pclDummy,pcpTable);
  strcat(pclFileName,"/");
  strcat(pclFileName,pcpSelection);
  pclData[0] = '\0';
  pflFilePtr = fopen(pclFileName,"r");
  if (pflFilePtr == NULL)
  {
     dbg(TRACE,"Could not open file <%s>",pclFileName);
  }
  else
  {
     ilRC = fread(pclData,1,MAX_TEXT_FILE_SIZE,pflFilePtr);
     pclData[ilRC] = '\0';
     dbg(DEBUG,"ReadFile: %d chars read from file <%s>",ilRC,pclFileName);
     fclose(pflFilePtr);
  }

  (void) tools_send_info_flag(que_out,0,"ssihdl","","CEDA","","",pcgTwStart,pcgTwEnd,
                              pcpCmd,pcpTable,pcpSelection,"",pclData,0);

  return ilRC;
} /* End of ReadFile */


static int GetDirectory(char *pcpDirName, char *pcpFileName, char *pcpTable)
{
  int ilRC = RC_SUCCESS;

  if (strcmp(pcpTable,"SSIM") == 0)
  {
     strcpy(pcpDirName,pcgSSIMFilePath);
     strcpy(pcpFileName,pcgSSIMFileName);
  }
  else
  {
     if (strcmp(pcpTable,"TTB") == 0)
     {
        strcpy(pcpDirName,pcgTTBFilePath);
        strcpy(pcpFileName,pcgTTBFileName);
     }
     else
     {
        if (strcmp(pcpTable,"FLUKO") == 0)
        {
           strcpy(pcpDirName,pcgFLUKOFilePath);
           strcpy(pcpFileName,pcgFLUKOFileName);
        }
        else
        {
           if (strcmp(pcpTable,"SLOTCO") == 0)
           {
              strcpy(pcpDirName,pcgSLOTCOFilePath);
              strcpy(pcpFileName,pcgSLOTCOFileName);
           }
           else
           {
              strcpy(pcpDirName," ");
              strcpy(pcpFileName," ");
           }
        }
     }
  }

  return ilRC;
} /* End of GetDirectory */


static int CreateFile(char *pcpCmd, char *pcpTable, char *pcpSelection)
{
  int ilRC = RC_SUCCESS;
  FILE *pflFilePtr = NULL;
  char pclFileName[256];

  if (strcmp(pcpTable,"SSIM") == 0)
  {
     ilRC = GenerateSSIM(pcpSelection);
  }
  else
  {
     if (strcmp(pcpTable,"TTB") == 0)
     {
        ilRC = GenerateTTB(pcpSelection);
     }
     else
     {
        if (strcmp(pcpTable,"FLUKO") == 0)
        {
           ilRC = GenerateFLUKO(pcpSelection);
        }
        else
        {
           if (strcmp(pcpTable,"SLOTCO") == 0)
           {
              ilRC = GenerateSLOTCO(pcpSelection);
           }
           else
           {
              ilRC = RC_FAIL;
           }
        }
     }
  }

  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pcpTable,"TTB") == 0)
     {
        ilRC = ReadFile(pcpCmd,pcpTable,pcgCreatedFileNameArr);
     }
     else
     {
        ilRC = ReadFile(pcpCmd,pcpTable,pcgCreatedFileName);
     }
  }

  return ilRC;
} /* End of CreateFile */


static int DeleteFile(char *pcpCmd, char *pcpTable, char *pcpSelection)
{
  int ilRC = RC_SUCCESS;
  char pclData[256];
  char pclFileName[256];
  char pclDummy[256];

  ilRC = GetDirectory(pclFileName,pclDummy,pcpTable);
  strcat(pclFileName,"/");
  strcat(pclFileName,pcpSelection);
  strcpy(pclData,"rm ");
  strcat(pclData,pclFileName);
  ilRC = system(pclData);
  dbg(DEBUG,"DeleteFile: File <%s> was deleted",pclData);

  (void) tools_send_info_flag(que_out,0,"ssihdl","","CEDA","","",pcgTwStart,pcgTwEnd,
                              pcpCmd,pcpTable,pcpSelection,"",pclData,0);

  return ilRC;
} /* End of DeleteFile */


static int ConvertTimeField(char *pcpTime)
{
  int ilRC = RC_SUCCESS;
  int ilTime;
  int ilTimeHour;
  int ilTimeMin;

  if (strlen(pcpTime) == 0 || pcpTime[0] == ' ')
  {
     strcpy(pcpTime,"+????");
     return ilRC;
  }

  ilTime = atoi(pcpTime);
  ilTimeHour = ilTime / 60;
  ilTimeMin = abs(ilTime - (ilTimeHour * 60));
  if (ilTimeHour < 0)
  {
     strcpy(pcpTime,"-");
     ilTimeHour = ilTimeHour * (-1);
  }
  else
  {
     strcpy(pcpTime,"+");
  }
  if (ilTimeHour < 10)
  {
     sprintf(&pcpTime[1],"0%d",ilTimeHour);
  }
  else
  {
     sprintf(&pcpTime[1],"%d",ilTimeHour);
  }
  if (ilTimeMin < 10)
  {
     sprintf(&pcpTime[3],"0%d",ilTimeMin);
  }
  else
  {
     sprintf(&pcpTime[3],"%d",ilTimeMin);
  }

  return ilRC;
} /* End of ConvertTimeField */


static int GetTimeDiff(char *pcpTiDi, int *ipTiDi, char *pcpApt)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb;
  short slCursor;
  short slFkt;
  char pclSqlBuf[128];
  char pclDataBuf[128];
  int ilI;
  int ilLen;

  for (ilI = 0; ilI < igTimeDiffCount; ilI++)
  {
     if (strcmp(&pcgTimeDiff[ilI].pclApc3[0],pcpApt) == 0)
     {
        strcpy(pcpTiDi,&pcgTimeDiff[ilI].pclTiDi[0]);
        *ipTiDi = pcgTimeDiff[ilI].ilTiDi;
        return ilRC;
     }
  }

  sprintf(pclSqlBuf,"SELECT APFN,APTT,%s FROM APTTAB WHERE APC3 = '%s'",
          pcgTimeDiffField,pcpApt);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);
  if (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",3,",");
     ilLen = get_real_item(&pcgTimeDiff[igTimeDiffCount].pclApfn[0],pclDataBuf,1);
     ilLen = get_real_item(&pcgTimeDiff[igTimeDiffCount].pclAptt[0],pclDataBuf,2);
     ilLen = get_real_item(pcpTiDi,pclDataBuf,3);
     if (strlen(pcpTiDi) > 0 && pcpTiDi[0] != ' ')
     {
        pcgTimeDiff[igTimeDiffCount].ilTiDi = atoi(pcpTiDi);
     }
     else
     {
        pcgTimeDiff[igTimeDiffCount].ilTiDi = igTimeDiff;
     }
     ilRC = ConvertTimeField(pcpTiDi);
  }
  else
  {
     strcpy(pcpTiDi,"+????");
     pcgTimeDiff[igTimeDiffCount].ilTiDi = igTimeDiff;
     strcpy(&pcgTimeDiff[igTimeDiffCount].pclApfn[0]," ");
     strcpy(&pcgTimeDiff[igTimeDiffCount].pclAptt[0]," ");
  }
  *ipTiDi = pcgTimeDiff[igTimeDiffCount].ilTiDi;

  strcpy(&pcgTimeDiff[igTimeDiffCount].pclApc3[0],pcpApt);
  strcpy(&pcgTimeDiff[igTimeDiffCount].pclTiDi[0],pcpTiDi);
  igTimeDiffCount++;

  return ilRC;
} /* End of GetTimeDiff */


static int ConvertSSIMToLocal(char *pcpLine)
{
  int ilRC = RC_SUCCESS;
  char pclOrgLine[256];
  char pclOrgVafr[8];
  char pclOrgVato[8];
  char pclOrgDoop[8];
  char pclOrgStod[8];
  char pclOrgStoa[8];
  char pclOrigDiff[8];
  char pclDestDiff[8];
  char pclNewVafr[8];
  char pclNewVato[8];
  char pclNewDoop[8];
  char pclNewStod[8];
  char pclNewStoa[8];
  char pclDate[16];
  char pclHour[8];
  char pclMin[8];
  int ilTimeDiff;

  memset(pclOrgVafr,0x00,8);
  memset(pclOrgVato,0x00,8);
  memset(pclOrgDoop,0x00,8);
  memset(pclOrgStod,0x00,8);
  memset(pclOrgStoa,0x00,8);
  memset(pclOrigDiff,0x00,8);
  memset(pclDestDiff,0x00,8);
  strcpy(pclOrgLine,pcpLine);

  strncpy(pclOrgVafr,&pcpLine[14],7);
  strncpy(pclOrgVato,&pcpLine[21],7);
  strncpy(pclOrgDoop,&pcpLine[28],7);
  strncpy(pclOrgStod,&pcpLine[39],4);
  strncpy(pclOrgStoa,&pcpLine[57],4);
  strncpy(pclOrigDiff,&pcpLine[47],5);
  strncpy(pclDestDiff,&pcpLine[65],5);

  if (strcmp(pclOrigDiff,"+????") != 0 && strcmp(pclOrigDiff,"+0000") != 0 &&
      strcmp(pclOrgStod,"    ") != 0)
  {
     strcpy(pclDate,"20040108");
     strcat(pclDate,pclOrgStod);
     strcat(pclDate,"00");
     strcpy(pclHour,&pclOrigDiff[1]);
     pclHour[2] = '\0';
     strcpy(pclMin,&pclOrigDiff[3]);
     ilTimeDiff = atoi(pclHour) * 60 + atoi(pclMin);
     if (*pclOrigDiff == '-')
        ilTimeDiff *= -1;
     ilRC = AddSecondsToCEDATime(pclDate,ilTimeDiff*60,1);
     strcpy(pclNewStod,&pclDate[8]);
     pclNewStod[4] = '\0';
     if ((strcmp(pclNewStod,pclOrgStod) < 0 && *pclOrigDiff == '+') ||
         (strcmp(pclNewStod,pclOrgStod) > 0 && *pclOrigDiff == '-'))
     {
        strcpy(pclNewDoop,"       ");
        if (*pclOrigDiff == '+')
        {
           if (pclOrgDoop[0] == '1')
              pclNewDoop[1] = '2';
           if (pclOrgDoop[1] == '2')
              pclNewDoop[2] = '3';
           if (pclOrgDoop[2] == '3')
              pclNewDoop[3] = '4';
           if (pclOrgDoop[3] == '4')
              pclNewDoop[4] = '5';
           if (pclOrgDoop[4] == '5')
              pclNewDoop[5] = '6';
           if (pclOrgDoop[5] == '6')
              pclNewDoop[6] = '7';
           if (pclOrgDoop[6] == '7')
              pclNewDoop[0] = '1';
           ilRC = AdjustDate(pclNewVafr,pclOrgVafr,'+');
           ilRC = AdjustDate(pclNewVato,pclOrgVato,'+');
        }
        else
        {
           if (pclOrgDoop[0] == '1')
              pclNewDoop[6] = '7';
           if (pclOrgDoop[1] == '2')
              pclNewDoop[0] = '1';
           if (pclOrgDoop[2] == '3')
              pclNewDoop[1] = '2';
           if (pclOrgDoop[3] == '4')
              pclNewDoop[2] = '3';
           if (pclOrgDoop[4] == '5')
              pclNewDoop[3] = '4';
           if (pclOrgDoop[5] == '6')
              pclNewDoop[4] = '5';
           if (pclOrgDoop[6] == '7')
              pclNewDoop[5] = '6';
           ilRC = AdjustDate(pclNewVafr,pclOrgVafr,'-');
           ilRC = AdjustDate(pclNewVato,pclOrgVato,'-');
        }
     }
     else
     {
        strcpy(pclNewVafr,pclOrgVafr);
        strcpy(pclNewVato,pclOrgVato);
        strcpy(pclNewDoop,pclOrgDoop);
     }
  }
  else
  {
     strcpy(pclNewVafr,pclOrgVafr);
     strcpy(pclNewVato,pclOrgVato);
     strcpy(pclNewDoop,pclOrgDoop);
     strcpy(pclNewStod,pclOrgStod);
  }
  if (strcmp(pclDestDiff,"+????") != 0 && strcmp(pclDestDiff,"+0000") != 0 &&
      strcmp(pclOrgStoa,"    ") != 0)
  {
     strcpy(pclDate,"20040108");
     strcat(pclDate,pclOrgStoa);
     strcat(pclDate,"00");
     strcpy(pclHour,&pclDestDiff[1]);
     pclHour[2] = '\0';
     strcpy(pclMin,&pclDestDiff[3]);
     ilTimeDiff = atoi(pclHour) * 60 + atoi(pclMin);
     if (*pclDestDiff == '-')
        ilTimeDiff *= -1;
     ilRC = AddSecondsToCEDATime(pclDate,ilTimeDiff*60,1);
     strcpy(pclNewStoa,&pclDate[8]);
     pclNewStoa[4] = '\0';
  }
  else
  {
     strcpy(pclNewStoa,pclOrgStoa);
  }

  pcpLine[14] = '\0';
  strcat(pcpLine,pclNewVafr);
  strcat(pcpLine,pclNewVato);
  strcat(pcpLine,pclNewDoop);
  strcat(pcpLine,&pclOrgLine[35]);
  pcpLine[39] = '\0';
  strcat(pcpLine,pclNewStod);
  strcat(pcpLine,&pclOrgLine[43]);
  pcpLine[57] = '\0';
  strcat(pcpLine,pclNewStoa);
  strcat(pcpLine,&pclOrgLine[61]);

  return ilRC;
} /* End of ConvertSSIMToLocal */



static int AdjustDate(char *pcpNewDate, char *pcpOrgDate, char cpDirection)
{
  int ilRC = RC_SUCCESS;
  char pclTmpDateIn[32];
  char pclTmpDateOut[32];
  char pclTmpBuf[32];

  ilRC = GetMonthNumeric(pclTmpBuf,pcpOrgDate);
  strcpy(pclTmpDateIn,"20");
  strcat(pclTmpDateIn,&pcpOrgDate[5]);
  strcat(pclTmpDateIn,pclTmpBuf);
  strcat(pclTmpDateIn,pcpOrgDate);
  pclTmpDateIn[8] = '\0';
  strcat(pclTmpDateIn,"120000");
  if (cpDirection == '+')
  {
     ilRC = AddSecondsToCEDATime(pclTmpDateIn,86400,1);
  }
  else
  {
     ilRC = AddSecondsToCEDATime(pclTmpDateIn,-86400,1);
  }

  pclTmpDateIn[8] = '\0';
  ilRC = GetMonthAlpha(pclTmpBuf,pclTmpDateIn);
  strcpy(pclTmpDateOut,&pclTmpDateIn[6]);
  strcat(pclTmpDateOut,pclTmpBuf);
  strcat(pclTmpDateOut,&pclTmpDateIn[2]);
  pclTmpDateOut[7] = '\0';
  strcpy(pcpNewDate,pclTmpDateOut);

  return ilRC;
} /* End of AdjustDate */



static int SqlArrayInsert(char *pcpTable, char *pcpFldList, char *pcpValList)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SqlArrayInsert:";
  int ilRCdb = DB_SUCCESS;
  char pclSqlBuf[1024];
  short slFkt = 0;
  short slCursor = 0;

  sprintf(pclSqlBuf,"INSERT INTO %s (%s) VALUES (%s)",pcpTable,pcpFldList,pcpValList);
  slCursor = 0;
  slFkt = 0;
  dbg(DEBUG,"%s <%s>\n%s",pclFunc,pclSqlBuf,pcgSsiBuf);
  ilRCdb = SqlIfArray(slFkt,&slCursor,pclSqlBuf,pcgSsiBuf,0,&rgRecDesc);
  if (ilRCdb == DB_SUCCESS)
  {
     commit_work();
  }
  else
  {
     rollback();
     dbg(TRACE,"%s Error = %d",pclFunc,ilRCdb);
  }
  close_my_cursor(&slCursor);

  return ilRC;
} /* End of SqlArrayInsert */


/******************************************************************************/
/* checkCedaTime                                                          */
/*                                                                            */
/* Check if the provided time pattern is valid. It has to look like:          */
/* YYYYMMDDHHMM, DDHH.                                                        */
/*                                                                            */
/* Input:                                                                     */
/*        pcpData -- pointer to data we have to check                         */
/*                                                                            */
/* Returns:                                                                   */
/*        RC_SUCCESS if pattern is valid                                      */
/*        RC_FAIL else                                                        */
/*                                                                            */
/******************************************************************************/
static unsigned int igDayTab [] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
static int checkCedaTime(char *pcpData) 
{
  int ilHour, ilMin, ilSec;
  int ilYear, ilMonth, ilDay;
  int ili, ilTimeStrLen, ilLen;
  char pclTmpStr[20];
  char pclData[20];
  char *pclFunc = "checkCedaTime";

  memset( pclData, 0, sizeof(pclData) );
  memcpy( pclData, pcpData, 12 );
  dbg( DEBUG, "<%s> Org <%s> Date <%s>", pclFunc, pcpData, pclData );
  ilLen = ilTimeStrLen = strlen(pclData);
  for( ili = 0; ili < ilTimeStrLen; ili++ )
    if( isspace(pclData[ili]) )
      ilLen--;
  
  if( ilLen != 12 )
      return RC_FAIL;

  sprintf( pclTmpStr, "%4.4s", pclData );
  ilYear = atoi(pclTmpStr);
  sprintf( pclTmpStr, "%2.2s", &pclData[4] );
  ilMonth = atoi(pclTmpStr);
  sprintf( pclTmpStr, "%2.2s", &pclData[6] );
  ilDay = atoi(pclTmpStr);
  
  dbg( DEBUG, "<%s> ilYear=%d ilMonth=%d ilDay=%d", pclFunc, ilYear, ilMonth, ilDay );
  if( ilYear < 1970 || ilYear > 2100) 
  {
    dbg(TRACE,"<%s> Illegal year <%d>", pclFunc, ilYear);
    return RC_FAIL;
  }
  if (ilMonth < 1 || ilMonth > 12) 
  {
    dbg(TRACE,"<%s> Illegal month <%d>", pclFunc, ilMonth);
    return RC_FAIL;
  }
  /* check special case of 29. FEB in leap years! */
  if (LeapYear(ilYear) == 1 && ilMonth == 2) 
  {
    if (ilDay < 1 || ilDay > 29) 
    {
      dbg(TRACE,"<%s> Illegal day <%d> in month <%d>", pclFunc, ilDay, ilMonth);
      return RC_FAIL;
    }
  }
  else if (ilDay < 1 || ilDay > igDayTab[ilMonth]) 
  {
    dbg(TRACE,"<%s> Illegal day <%d> in month <%d>", pclFunc, ilDay, ilMonth);
    return RC_FAIL;
  } 
  /* proceed to date entry */

  sprintf( pclTmpStr, "%2.2s", &pclData[8] );
  ilHour = atoi(pclTmpStr);
  sprintf( pclTmpStr, "%2.2s", &pclData[10] );
  ilMin = atoi(pclTmpStr);
  dbg( DEBUG, "<%s> ilHour=%d ilMin=%d", pclFunc, ilHour, ilMin );

  if( ilHour < 0 || ilHour > 23) 
  {
      dbg(TRACE,"<%s> Illegal Hour <%d>", pclFunc, ilHour);
      return RC_FAIL;
  }
  if (ilMin < 0 || ilMin > 59) 
  {
      dbg(TRACE,"<%s> Illegal Minute <%d>", pclFunc, ilMin);
      return RC_FAIL;  
  }
  return RC_SUCCESS;
}

/* Mei */
static int ResetSSI()
{
  int ilRc = RC_SUCCESS;
  int index = 0;
  short slSqlCursor;
  short slSqlFunc;
  char pclSqlBuf[1024];
  char pclSqlData[1024];
  char *pclFunc = "ResetSSI";

  dbg( TRACE,"<%s> ================ Start ================", pclFunc );

  sprintf( pclSqlBuf, "%s", "TRUNCATE TABLE SSITAB" );
  dbg( TRACE, "<%s> SQL: <%s>", pclFunc, pclSqlBuf );
  ilRc = RunSQL( pclSqlBuf, pclSqlData );
  if( ilRc != DB_SUCCESS )
  {
      dbg( TRACE, "<%s> Fail to truncate SSITAB", pclFunc );
      return;
  }

  /* Lin */
  /* Make use of STOA and STOD to make the statement faster */
  index += sprintf( pclSqlBuf + index, "%s", " SELECT SEAS FROM SEATAB" );
  index += sprintf( pclSqlBuf + index, "%s", " WHERE" );
  index += sprintf( pclSqlBuf + index, "%s", " VPFR BETWEEN (SELECT MIN(STOD) FROM AFTTAB" );
  index += sprintf( pclSqlBuf + index, "%s", " WHERE STOD != '              ') AND (SELECT MAX(STOD) FROM AFTTAB" );
  index += sprintf( pclSqlBuf + index, "%s", " WHERE STOD != '              ')" );
  index += sprintf( pclSqlBuf + index, "%s", " OR" );
  index += sprintf( pclSqlBuf + index, "%s", " VPFR BETWEEN (SELECT MIN(STOA) FROM AFTTAB" );
  index += sprintf( pclSqlBuf + index, "%s", " WHERE STOA != '              ') AND (SELECT MAX(STOA) FROM AFTTAB" );
  index += sprintf( pclSqlBuf + index, "%s", " WHERE STOA != '              ')" );
  index += sprintf( pclSqlBuf + index, "%s", " OR" );
  index += sprintf( pclSqlBuf + index, "%s", " VPTO BETWEEN (SELECT MIN(STOD) FROM AFTTAB" );
  index += sprintf( pclSqlBuf + index, "%s", " WHERE STOD != '              ') AND (SELECT MAX(STOD) FROM AFTTAB" );
  index += sprintf( pclSqlBuf + index, "%s", " WHERE STOD != '              ')" );
  index += sprintf( pclSqlBuf + index, "%s", " OR" );
  index += sprintf( pclSqlBuf + index, "%s", " VPTO BETWEEN (SELECT MIN(STOA) FROM AFTTAB" );
  index += sprintf( pclSqlBuf + index, "%s", " WHERE STOA != '              ') AND (SELECT MAX(STOA) FROM AFTTAB" );
  index += sprintf( pclSqlBuf + index, "%s", " WHERE STOA != '              ')" );
  dbg( TRACE, "<%s> SQL: <%s>", pclFunc, pclSqlBuf );

  slSqlFunc = START;
  slSqlCursor = 0;
  while((ilRc = sql_if(slSqlFunc,&slSqlCursor,pclSqlBuf,pclSqlData)) == DB_SUCCESS)
  {
      slSqlFunc = NEXT;
      dbg( TRACE, "<%s> Loading Season <%s>...", pclFunc, pclSqlData );
      LoadSeas( pclSqlData );
      dbg( TRACE, "<%s> Loaded Season <%s>", pclFunc, pclSqlData );
  }
  close_my_cursor(&slSqlCursor);

  dbg( TRACE,"<%s> ================ End =================", pclFunc );
  return RC_SUCCESS;
}

static int LoadSeas( char *pcpSeason )
{
  int ilRc = RC_SUCCESS;
  int ilDays;
  int ilMins;
  int ilWeekDay;
  short slSqlCursor;
  short slSqlFunc;
  char pclSqlBuf[1024];
  char pclSqlData[1024];
  char pclVpfr[20];
  char pclVpto[20];
  char *pclFunc = "LoadSeas";


  sprintf( pclSqlBuf,"SELECT VPFR,VPTO FROM SEATAB WHERE SEAS = '%s'", pcpSeason );
  ilRc = RunSQL( pclSqlBuf, pclSqlData );
  if (ilRc != DB_SUCCESS)
  {
      dbg( TRACE, "<%s> Fail to get VPFR and VPTO from SEATAB", pclFunc );
      return;
  }
  get_fld(pclSqlData,FIELD_1,STR,20,pclVpfr);
  get_fld(pclSqlData,FIELD_2,STR,20,pclVpto);
  dbg( TRACE, "<%s> VPFR <%s> VPTO <%s>", pclFunc, pclVpfr, pclVpto );

  ilRc = GetFullDay(pclVpfr,&ilDays,&ilMins,&ilWeekDay);
  igFirstDay = ilDays - ilWeekDay + 1;
  pclVpto[8] = '\0';
  strcat(pclVpto,"235959");

  /* Lin */
  /* Just select all FLNO first. It will be much faster */
  sprintf( pclSqlBuf,"SELECT DISTINCT FLNO FROM AFTTAB WHERE FLNO IS NOT NULL ");
  slSqlCursor = 0;
  slSqlFunc = START;
  while((ilRc = sql_if(slSqlFunc,&slSqlCursor,pclSqlBuf,pclSqlData)) == DB_SUCCESS)
  {
      slSqlFunc = NEXT;
      TrimRight(pclSqlData);
      dbg( DEBUG, "<%s> FLNO <%s>", pclFunc, pclSqlData );

      if (strlen(pclSqlData) > 3)
          ilRc = GenerateFlightSSI(pcpSeason,pclSqlData,pclVpfr,pclVpto,TRUE);
  }
  close_my_cursor(&slSqlCursor);

}
/******************************************************************************/
static int GetFieldLength(char *pcpTana, char *pcpFina, long *plpLen)
{
    int  ilRc  = RC_SUCCESS;            /* Return code */
    int  ilCnt = 0;
    char clTaFi[32];
    char clFele[16];
    char clFldLst[16];

    ilCnt = 1;
    sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
    sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerst�rt - konstante nicht m�glich */

 
    ilRc = syslibSearchSystabData(&pcgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
    switch (ilRc)
    {
    case RC_SUCCESS :
        *plpLen = atoi(&clFele[0]);
        /* dbg(DEBUG,"GetFieldLength: <%s,%s,%s> <%d>",pcpHopo,pcpTana,pcpFina,*plpLen);*/
        break;

    case RC_NOT_FOUND :

        dbg(TRACE,"GetFieldLength: pcpTana <%s> pcpFina<%s>",pcpTana,pcpFina);
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s>",ilRc,&clTaFi[0]);
        break;

    case RC_FAIL :
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
        break;

    default :
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
        break;
    }/* end of switch */

    return(ilRc);
    
} /* end of GetFieldLength */
