#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/tlxgen.c 1.7 2012/10/18 17:50:30SGT dka Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/* 20040810 JIM removed sccs_ version string                                  */
/* 20091130 BST? 1.6	First checked in version to SVN.		*/
/* 20121018 DKA  1.7	Convert standard SITA telex to SITA7 for AUH.	*/
/*			UFIS-1973. Add command "SITA7" for this.	*/
/*			Copy over GetLine(),CopyLine(), from fdihxx	*/
/******************************************************************************/
/*                                                                            */


/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE

#define DATABLK_SIZE     (256*1024)
#define C_MAX_SP_FIELDS        (10)
#define C_MAXFIELDS           (100)
#define C_MAXFIELDNAMELEN       (4)
#define C_MAXFIELDVALLEN     (2048)

/* selection keyword inps */
#define C_IMPORT           (1)
#define C_PREVIEW          (2)

/* for field struct definition */
#define C_AKTIV            (1)
#define C_DEAKTIV          (0)
#define C_NOTSET           (-1)
#define C_SET              (1)
#define C_DISABLE          (0)
#define C_ENABLE           (1)
#define C_FOUND            (0)
#define C_NOTFOUND         (1)
#define C_SEARCH           (1)
#define C_INSERT           (2)
#define C_UPDATE           (3)
#define C_SELECT           (4)
#define C_FLDCMP           (5)

#define C_FIRST            (1)
#define C_NEXT             (2)
#define C_LAST             (3)
#define C_PREV             (4)

/* FUNCTION ERROR CODES */
#define C_NO_ERROR           (0)
#define C_UNKNOWN_COMMAND    (1)
#define C_NO_ACTIVE_FIELDS   (2)
#define C_SET_ON_FIRST_FIELD (3)
#define C_SET_ON_LAST_FIELD  (4)


/* DEFINITION FOR LOADING URNOS */
#define C_RESERVED_X_URNOS   (100)



#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <string.h> 	/* for strerror() */
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h"
#include "tlxgen.h"
#include "send.h"
#include "ct.h"
#include "fditools.h"	/* e.g. for GetLine */

#define SUCCESS RC_SUCCESS
#define FAIL    RC_FAIL


/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = DEBUG;
int MyDebugSwitch = FALSE; 

/*************************************************************************/
/* STRUCTS                                                               */
/*************************************************************************/
typedef struct
{
 int  Status      [C_MAXFIELDS];
 char Fieldname   [C_MAXFIELDS][C_MAXFIELDNAMELEN+1];
 char FieldVal    [C_MAXFIELDS][C_MAXFIELDVALLEN];
 int  StrItem     [C_MAXFIELDS];
 long StrBeginPos [C_MAXFIELDS];
 long ValLength   [C_MAXFIELDS];
 long Item        [C_MAXFIELDS];
 int  ReadFromEv  [C_MAXFIELDS];
 int  LastAttach  [C_MAXFIELDS];
 int  DoFldCmp    [C_MAXFIELDS];
 int  DoUpdate    [C_MAXFIELDS];
 int  DoSelect    [C_MAXFIELDS];
 int  DoInsert    [C_MAXFIELDS];
 int  DoSearch    [C_MAXFIELDS];

} ST_EVT_DATA_HDL;


typedef struct 
{
 char    cDateStampStr [32] ;
 int     iYear ;
 int     iMonth ;
 int     iDate ;
 int     iHour ;
 int     iMins ;
 int     iSecs ;
 int iStDateInterpret ;
 int iStTimeInterpret ;
} DATEINFO ;





/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];
static EVENT * prgOutEvent = NULL ;
static ST_EVT_DATA_HDL    sBRS_ARR ;
static ST_EVT_DATA_HDL    sMVT_ARR ;
static ST_EVT_DATA_HDL    sMV1_ARR ;
static ST_EVT_DATA_HDL    sTLX_ARR ;
static ST_EVT_DATA_HDL    sBRS_PRN ;
static ST_EVT_DATA_HDL    sMVT_PRN ;

static int   igMVT_Flag ; 
static int   igBRS_Flag ; 
static int   que_out = 0;
/* static int   igToTlxQue = 7298;  */
static int   igToTlxQue = 7208; 
static int   igToInfQue = 7206; 
static char  cgConfigFile[512] ;
static char  pcgTblNam[12] ;
static char  pcgTblExt[12] ;
static char  pcgChkWks[128] ;
static char  pcgChkUsr[128] ;
static char  pcgSqlBuf[12*1024] ;
static char  pcgDataArea[4096] ;
static char  pcgTmpDataArea[4096] ;
static char  pcgHomeHopo[64] ; 
static char  cgDateFrom[32] ; 
static char  pcgTimeInfo[64] ; 
static char  pcgDateOffset[64] ; 
static char  cur_time[16];  /* buffer for timestamp */

static int   igRealImportFlag ;  /* send 2 flight if TRUE */
static int   igActUrno ;
static int   igReservedUrnoCnt ;

static char  pcgRecvName[64] ;  /* BC-Head used as WKS-Name */
static char  pcgDestName[64] ;  /* BC-Head used as USR-Name */
static char  pcgTwStart[64] ;   /* BC-Head used as Stamp    */
static char  pcgTwEnd[64] ;     /* BC-Head used as Stamp    */

/* v.1.07 */
/* static char pcgSita7Prio[32]; */
static char pcgSita7TlxTypes [512]; 
static char pcgSita7FileNameExt [16]; 
static char pcgSita7FileDir [256]; 
static char igSita7CreateFile = 0;
static int  igToTlxQueSita7 = -1;

/***************************************************************************/
/* Flags for testing                                                       */
/***************************************************************************/
/* if one of the parameters is set to = 1 --> test is active ELSE no test  */

static int gl_updateTest ;
static int gl_noChangeTest ;
static int igTestFktFlag ;     /* if set to TRUE >> additional fct. output */

/***************************************************************************/
/* External functions                                                      */
/***************************************************************************/

extern int get_item (int, char *, char *, int, char *, char *, char *) ;
extern int get_real_item (char *, char *, int) ;
extern int get_item_no (char *, char *, int) ;
extern int FullDayDate (char * clDateStr, int ilDay ) ;
extern int BuildItemBuffer(char *, char *, int, char *) ;

/*****************************************************************************/
/* Function prototypes                                                       */
/*****************************************************************************/
static int      Init_cedadb() ;
static int      Reset(void) ;                    /* Reset program            */
static void     Terminate(void) ;                /* Terminate program        */
static void     HandleSignal(int) ;              /* Handles signals          */
static void     HandleErr(int) ;                 /* Handles general errors   */
static void     HandleQueErr(int) ;              /* Handles queuing errors   */
static int      HandleData(void) ;               /* Handles event data       */
static void     HandleQueues(void) ;             /* Waiting for Sts.-switch  */
static int      handleBRS_Call(char*, char *) ;  /* load pricipial data      */
static int      getNextBRSFilename() ;           /* chk., if something to do */
static int      getItemCnt  (char *cpFldStr, int *ipCnt) ;
static int      prepSqlValueStr (int iItems, char *cpValStr) ;
static int      GetNextUrno (int *pipUrno) ;
static int      GetNextUrnoItem (int *pipUrno) ;
static int      GetLocalTimeStamp (char *cpTimeStamp) ;
static int      readDataItemFromStr( char *cpFldData, char *cpFldName,
                                     char *cpFldLst, char *cpDataArea) ;
static int      getDataFromDbStrs (int *iItemNo, char *cFld,
                                   char *pcgSearchFldLst, /* fld list select */
                                   char *pcgDataArea,    /* data list select */
                                   char *pcgFoundData
                                  ) ;
static int      setTlxTableDat  (ST_EVT_DATA_HDL *sTLX_ARR, 
                                 ST_EVT_DATA_HDL *sSND_ARR ) ; 
static int      getTextField (ST_EVT_DATA_HDL sSND_ARR, char *ipTxtStr ) ;
static int      createValStr4Tlx (char *cpValStr, ST_EVT_DATA_HDL sTLX_ARR ) ;  
static int      SQLInsertTlxTab (ST_EVT_DATA_HDL sTlxTab) ; 
static int      getVialShortTxt (char *cpVialStr, char *cpVialShortStr) ; 
static int      getBRSHeaderInfo (ST_EVT_DATA_HDL sBRS_ARR, 
                                  ST_EVT_DATA_HDL *sBRS_PRN ) ;  
static int      getBRSTlxTextData (void) ; 
static int      createMVTTelex (void) ;  
static int      TriggerAction(char *pcpTableName, char *pcpCmdRec, 
							      					char *pcpFieldName) ; 
static int      WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem);
static int      CheckQueue(int ipModId, ITEM **prpItem);
static int      handleMVT_Call (char *cpUrno, char *cpAction ) ; 
static int      createBRSTelex (void) ;  


/* field struct handle functions */
static int    ResetFldStruct (ST_EVT_DATA_HDL *sFldArr) ;
static int    fillFldStruct (char *cpFldsStr, ST_EVT_DATA_HDL *sFldArr) ;
static int    fillDataInFldStruct (char *cpDataStr, ST_EVT_DATA_HDL *sFldArr) ;
static int    AddFld2Struct (ST_EVT_DATA_HDL *sFdlArr, char *cpFldName,
                             char *cpValStr ) ;
static int    InsOrReplFld2Struct (ST_EVT_DATA_HDL *sFldArr, char *cpFldName,
                                   char *cpValStr ) ;
static int    ReplaceFldDataInStruct (char *cpDatStr, char *pclFld,
                                      ST_EVT_DATA_HDL *sFldArr) ;
static int    getFldDataFromStruct (char *cpRetStr, char *cpFld,
                                    ST_EVT_DATA_HDL sFldArr) ;
static int    DoesFldExistInStruct (char *cpFld, ST_EVT_DATA_HDL sFldArr) ;
static int    getFldStruct (char *pclFldsStr, char *pclDataStr, int iAction,
                            ST_EVT_DATA_HDL *sFldArr) ;
static int    setNextFldPosInStruct (int *ipActionResult, int iAction,
                                     ST_EVT_DATA_HDL *sFldArr) ;
static int    setChkFldinStruct4Fld (char *pclFld, int iDoWhat,
                                     ST_EVT_DATA_HDL *sFldArr) ;
static int    setStatusInStruct (char *pclFld, int iDoWhat, int iKindOfFlg,
                                 ST_EVT_DATA_HDL *sFldArr) ;
static int    createFldAndDataLst (char *cpFldLst, char *cpDataLst,
                                int iKindOf, ST_EVT_DATA_HDL sFldArr) ;
static int    ifRecordChanged (int *iChgFlg, char *pcgSearchFldLst,
                               char *pcgDataArea, ST_EVT_DATA_HDL sFldArr) ;

static int    GetItemsFromCfgFile(char *cpConfigFile) ; 
static int    getDateDiff (DATEINFO *st_beginDate, DATEINFO *st_lastDate,
                           int *retDateCnt ) ;
static int    interpretDateInfo (char *charDate, DATEINFO *st_Date ) ;
static int    incDateAtOne (char *retStrDate, DATEINFO *st_Date,
                            int *ipStatus ) ;

static int    startWhileTimeReached (ST_EVT_DATA_HDL *stAct, char *cpDateFrom, char *cpDateTo) ; 
static int    readMVTTab (void) ;  

/* v.1.07 */
/* ReadTelexAndSend renamed to ReadTelexAndSendSMV */
static int    ReadTelexAndSendSMV (char *cpUrno ) ; 
static int    ReadTelexAndSendSITA7 (char *pcpFields, char *pcpData ) ;
static int    CreateTelexFile (char *pcpTlxText, int ipTxtLen,
    char *pcpFileNamePrepend, char *pcpFileNameExt, char *pcpFileDir);

static char  *GetLine(char *pcpLine,int ipLineNo);	/* from fdihxx - v.1.07 */
static void   CopyLine(char *pcpDest,char *pcpSource);	/* from fdihxx - v.1.07 */
static void   TrimRight(char *pcpBuffer);		/* from fdihxx */
static int    generateMTVTelex (char *clFldLst, char *clData) ; 

static void PatchTextServerToClient(char *pcpText, int ipUseOld);

static void ForwardTelexToFdihdl(char *pcpSelKey, char *pcpData);

static int CreateInftabXML(char *pcpSelKey, char *pcpFields, char *pcpData);


static void TestReplace(void);
static void SetStringValue(STR_DESC* prpStringBuffer, char *pcpNewString);
static void ReplacePatternInString(STR_DESC* prpString, char *pcpSearch, char *pcpReplace);

/*************************************************************/
/* Copied over from mvtgen */
/** BST 17-DEC-2009 **/
static int HandleSqlSelectRows(char *pcpTable, char *pcpSqlKey, char *pcpSqlFld, char *pcpSqlDat, int ipLines);
static int get_any_item(char *dest, char *src, int ItemNbr, char *sep);
static int GetFieldValue(char *pcpFldVal, char *pcpFldLst, char *pcpFldNam, char *pcpDatLst);
static int GetFieldValues(char *pcpFldVal, char *pcpFldLst, char *pcpFldNam, char *pcpDatLst);
static int GetCfg (char *, char *, char *, short, char *, char *);

static int CleanDoubleBytes(char *pcpText, char *pcpFind, char *pcpSet); 

/* From mvtgen for compilation only for v.1.07 */
char pcgSenderTelexAddr[128];
char pcgSenderEmailAddr[128];
char pcgSenderEmailSmtp[128];
char pcgSenderEmailPath[128];
int igEmailIsReadyToUse = FALSE; 
static int ReplaceStringInString(char *pcpString, char *pcpSearch, char *pcpReplace);
static STR_DESC rgMyDataString;

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
 int     ilRC = RC_SUCCESS ;            /* Return code            */
 int     ilCnt = 0 ;

	char dateTo[64] ; 
	char dateFrom_i[64] ; 
	char dateFrom[64] ;
	char helpStr[64] ; 
	int  i ; 


 /*--------------------------------------------------------------------*/
 /* the normal sequenz of this process is the simulation mode. This    */
 /* means, that the flag igRealImportFlag is set to FALSE. This        */
 /* incident can be solved by the event 998. The 998 calling causes a  */
 /* toggle to the igRealImportFlag.                                    */
 /*--------------------------------------------------------------------*/

 debug_level = DEBUG ;     /* also debug level is on */

 INITIALIZE ;              /* General initialization       */
 dbg (TRACE, "MAIN: version <%s>", mks_version) ;
 igRealImportFlag = TRUE ;

 /* setting Testfalgs */
 /* all telex modes are set to disable here. For use a telex */
 /* format, you should turn on with the telex.cfg inputs     */
 /* the CONFIG names, which intressting for this, can you    */
 /* find after the disable statements in comment             */ 

 igMVT_Flag = C_DISABLE ;       /* [MVT]  MVT_TELEXE_ON */ 
 igBRS_Flag = C_DISABLE ;       /* [BRS]  BRS_TELEXE_ON */ 


 igTestFktFlag = TRUE ;
 gl_noChangeTest = 0 ;
 igActUrno = 0 ;          /* URNO VALUE - not set */
 igReservedUrnoCnt = 0 ;  /* Urno Buffer - no reserved Urnos */
 ilRC = GetLocalTimeStamp(cgDateFrom) ;

 /* Attach to the MIKE queues */
 do
 {
  ilRC = init_que() ;
  if (ilRC != RC_SUCCESS)
  {
   dbg (TRACE, "MAIN: init_que() failed! waiting 6 sec ...") ;
   sleep(6) ;
   ilCnt++ ;
  } /* if */
 } while ((ilCnt < 10) && (ilRC != RC_SUCCESS)) ;

 if (ilRC != RC_SUCCESS)
 {
  dbg (TRACE,"MAIN: init_que() failed! waiting 60 sec ...") ;
  sleep(60) ;
  exit(1) ;
 }
 else
 {
  dbg (TRACE, "MAIN: init_que() OK! mod_id <%d>",mod_id) ;
 } /* if */

 do
 {
  ilRC = init_db();
  if (ilRC != RC_SUCCESS)
  {
   check_ret (ilRC) ;
   dbg (TRACE, "MAIN: init_db() failed! waiting 6 sec ...") ;
   sleep(6) ;
   ilCnt++ ;
  } /* if */
 } while ((ilCnt < 10) && (ilRC != RC_SUCCESS)) ;


 if (ilRC != RC_SUCCESS)
 {
  dbg (TRACE,"MAIN: init_db() failed! waiting 60 sec ...") ;
  sleep(60) ;
  exit(2) ;
 }
 else
 {
  dbg (TRACE, "MAIN: init_db() OK!") ;
 } /* if */

 /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

 sprintf(cgConfigFile,"%s/tlxgen",getenv("BIN_PATH")) ;
 ilRC = TransferFile(cgConfigFile) ;
 if (ilRC != RC_SUCCESS)
 {
  dbg(TRACE, "MAIN: TransferFile(%s) failed!", cgConfigFile) ;
 } /* if */

 sprintf (cgConfigFile, "%s/tlxgen.cfg", getenv ("CFG_PATH")) ;
 ilRC = TransferFile(cgConfigFile) ;
 ilRC = GetItemsFromCfgFile (cgConfigFile) ; 
 if(ilRC != RC_SUCCESS)
 {
  dbg(TRACE,"MAIN:CONFIG FILE READING ERROR -> CANT WORK (%s)!", cgConfigFile) ;
 } /* if */


 ilRC = SendRemoteShutdown (mod_id) ;
 if (ilRC != RC_SUCCESS)
 {
  dbg (TRACE,"MAIN: SendRemoteShutdown(%d) failed!", mod_id) ;
 } /* if */

 if ((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE)
    && (ctrl_sta != HSB_ACT_TO_SBY))
 {
  dbg (DEBUG,"MAIN: waiting for status switch ...") ;
  HandleQueues() ;
 } /* if */

 if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
 {
  dbg (TRACE, "MAIN: initializing ...") ;
  if (igInitOK == FALSE)
  {
   ilRC = Init_cedadb() ;
   if (ilRC != RC_SUCCESS)
   {
    dbg (TRACE, "Init_cedadb: init failed!") ;
   } /* if */
  } /* if */
 }
 else
 {
  Terminate() ;
 } /* if */

/*  if (igMVT_Flag != C_ENABLE)
 {
  ilRC = TriggerAction("AFTTAB", "MV1", "NXTI") ; 
  ilRC = TriggerAction("AFTTAB", "MV2", "AIRB") ; 
  ilRC = TriggerAction("AFTTAB", "MV3", "EDTI") ; 
  ilRC = TriggerAction("AFTTAB", "MV4", "FLDA") ; 
  ilRC = TriggerAction("AFTTAB", "MV5", "VIAL") ; 
  ilRC = TriggerAction("AFTTAB", "MV6", "ONBL") ; 
 }
*/
 dbg (TRACE,"MAIN: initializing OK");
 for (;;)
 {
  dbg(TRACE,"================= START/END =========================");
  ilRC = que (QUE_GETBIG, 0, mod_id, PRIORITY_3, igItemLen,
             (char *) &prgItem) ;
  /* depending on the size of the received item  */
  /* a realloc could be made by the que function */
  /* so do never forget to set event pointer !!! */
  prgEvent = (EVENT *) prgItem->text ;

  if( ilRC == RC_SUCCESS )
  {                            /* Acknowledge the item */

   ilRC = que (QUE_ACK, 0, mod_id, 0, 0, NULL) ;
   if ( ilRC != RC_SUCCESS )
   {                         /* handle que_ack error */
    HandleQueErr(ilRC);
  } /* fi */

  switch( prgEvent->command )
  {
   case    HSB_STANDBY     :
           ctrl_sta = prgEvent->command ;
           HandleQueues() ;
          break ;

   case    HSB_COMING_UP   :
           ctrl_sta = prgEvent->command ;
           HandleQueues() ;
          break ;

   case    HSB_ACTIVE      :
           ctrl_sta = prgEvent->command ;
          break ;

   case    HSB_ACT_TO_SBY  :
           ctrl_sta = prgEvent->command ;
           /* CloseConnection() ; */
           HandleQueues() ;
          break ;

   case    HSB_DOWN        :
           /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
           ctrl_sta = prgEvent->command ;
           Terminate() ;
          break ;

   case    HSB_STANDALONE  :
           ctrl_sta = prgEvent->command ;
           ResetDBCounter() ;
          break ;

   case    REMOTE_DB :
           /* ctrl_sta is checked inside */
           HandleRemoteDB(prgEvent) ;
          break ;

   case    SHUTDOWN        :
           /* process shutdown - maybe from uutil */
           Terminate();
          break ;

   case    RESET           :
           ilRC = Reset() ;
          break ;

   case    EVENT_DATA      :
           if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
           {
            ilRC = HandleData() ;
            if (ilRC != RC_SUCCESS)
            {
             HandleErr(ilRC) ;
            } /* if */
           }
           else
           {
            dbg (TRACE, "MAIN: wrong hsb status <%d>", ctrl_sta) ;
            DebugPrintItem (TRACE, prgItem) ;
            DebugPrintEvent (TRACE, prgEvent) ;
           } /* if */
          break ;

   case    TRACE_ON :
           dbg_handle_debug (prgEvent->command) ;
          break ;

   case    TRACE_OFF :
           dbg_handle_debug (prgEvent->command) ;
          break ;


   case 100:     /* data testmode */

           dbg (DEBUG, "------------------------------------------------") ;
           dbg (DEBUG, "I  Using BRS test datas                        I") ;
           dbg (DEBUG, "------------------------------------------------") ;
					 if (igBRS_Flag == C_ENABLE)
					 {
            handleBRS_Call ("20010317000000", "20010317201000" ) ;
           } 
					 else 
					 {
						dbg (DEBUG, "BRS handle switch is off >> change config!") ; 
					 }
          /*  SendCedaEvent (9200,0,"ufis","Y","0","ATH,TAB,BDPS-UIF","BRS",
                           "","BBRS","FLD","DAT","X",4, RC_SUCCESS) ;    */
          break ;


   case 101:     /* data testmode */

           dbg (DEBUG, "------------------------------------------------") ;
           dbg (DEBUG, "I  Using MVT test datas                        I") ;
           dbg (DEBUG, "------------------------------------------------") ;

					 if (igMVT_Flag == C_ENABLE)
					 {
		        handleMVT_Call ("0030883440", "AB") ;
           }
					 else 
					 {
						dbg (DEBUG, "MVT handle switch is off >> change config!") ; 
					 }

				/*  SendCedaEvent (9200,0,"ufis","Y","0","ATH,TAB,BDPS-UIF","BRS",
                           "","BBRS","FLD","DAT","X",4, RC_SUCCESS) ;    */
          break ;

       case 102:     /* data testmode */

           dbg (DEBUG, "------------------------------------------------") ;
           dbg (DEBUG, "I  Test the date / time differenz function     I") ;
           dbg (DEBUG, "------------------------------------------------") ;

					 strcpy(dateFrom_i, "200103xx000000") ; 
					 for (i=1; i<25; i++)  
					 {
						sprintf(helpStr, "%#02d", i) ;
						dateFrom_i[6] = helpStr[0] ; 
						dateFrom_i[7] = helpStr[1] ; 
		        ReplaceFldDataInStruct (dateFrom_i, "#SDT", &sBRS_ARR) ; 
					  ilRC = startWhileTimeReached (&sBRS_ARR, dateFrom, dateTo) ; 

           }
    break;						


  case 777:
    dbg (TRACE, "------------------------------------------------") ;
    TestReplace();
    dbg (TRACE, "------------------------------------------------") ;
    break;						


       case 997:

            if (igTestFktFlag == FALSE)
            {
             igTestFktFlag = TRUE ;
             debug_level = DEBUG ;     /* also debug level is on */
            }
            else
            {
             igTestFktFlag = FALSE ;
            }
          break ;


       case 998:

        /*--------------------------------------------------------------------*/
        /* the normal sequenz of this process is the simulation mode. This    */
        /* means, that the flag igRealImportFlag is set to FALSE. This        */
        /* incident can be solved by the event 998. The 998 calling causes a  */
        /* toggle to the igRealImportFlag.                                    */
        /*--------------------------------------------------------------------*/

         dbg (DEBUG, "---------------------------------") ;
         if (igRealImportFlag == FALSE)
         {
          dbg (DEBUG, "MAIN: real import is switched on!") ;
          debug_level = DEBUG ;     /* also debug level is on */
          igRealImportFlag = TRUE ;
         }
         else
         {
          dbg (DEBUG, "MAIN: real import is switched off!") ;
          igRealImportFlag = FALSE ;
         }
         dbg (DEBUG, "---------------------------------") ;
              break ;

   default   :
           dbg (TRACE, "MAIN: unknown event") ;
           DebugPrintItem (TRACE, prgItem) ;
           DebugPrintEvent (TRACE, prgEvent) ;
           /* break ;  statement not reached */
   } /* end switch */
  }
  else
  {                       /* Handle queuing errors */
   HandleQueErr (ilRC) ;
  } /* end else */

 } /* end for */

 /* exit(0) ;      statement not reached */

} /* MAIN */




/*****************************************************************************/
/* read all important Items from the config file !                           */
/* this function was written for a dynamicly read between the runtime!       */
/*****************************************************************************/
static int GetItemsFromCfgFile(char *cpConfigFile)
{
 int   ilRC = RC_SUCCESS ; 
 int   ilZ ; 
 int   ilStat ; 
 int   il ;
 int   il4Chr ; 
 int   ilLen ;
 long int ilFldLen ;
 char  clSelection[64] ; 
 char  cpDestStr[1024] ; 
 char  cpFoundStr[1024] ;
 char  clCfgItem[100] ; 
 char  clStrItem[100] ;
 char  cpHlpStr[100] ; 
 char pclTmpBuf[128];

 ilStat = RC_SUCCESS ; 
 /* test, if this function is used between the runtime or at first time */
 if (DoesFldExistInStruct ("IERR", sBRS_ARR) == RC_SUCCESS)
 {
	/* Reset here all other telex structs */
  ilRC = ResetFldStruct (&sBRS_ARR) ;
  ilRC = ResetFldStruct (&sTLX_ARR) ;
  ilRC = ResetFldStruct (&sMVT_ARR) ;
  ilRC = ResetFldStruct (&sMV1_ARR) ;
  ilRC = ResetFldStruct (&sMVT_PRN) ;
  ilRC = ResetFldStruct (&sBRS_PRN) ;

	dbg (DEBUG, "------------------------------") ; 
	dbg (DEBUG, ">>> Interpret CONFIG FILE new!") ; 
	dbg (DEBUG, "------------------------------") ; 
 }
 else 
 {
	dbg (DEBUG, "----------------------------------------") ; 
	dbg (DEBUG, ">>> Interpret CONFIG FILE at first time!") ; 
	dbg (DEBUG, "----------------------------------------") ; 
 }

  /* set PRINTER FIELDS FOR BRS TELEXE */
  ilRC = AddFld2Struct (&sBRS_PRN, "H001", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H002", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H003", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H004", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H005", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H006", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H007", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H008", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H009", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H010", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H011", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H012", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H013", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H014", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H015", "" ) ; 
  sprintf (cpHlpStr, ".AAD%c%c",0x0d, 0x0a) ;  
	ilRC = AddFld2Struct (&sBRS_PRN, "BEGI", cpHlpStr ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "FLNO", ".F/" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "STOD", ".S/" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "FTYP", ".C/" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "TIFD", ".D/" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "TIFT", ".T/" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "ADID", ".H/" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "VIAL", ".R/" ) ; 

	/* fields for read the MVTTAB */
	ilRC = AddFld2Struct (&sMV1_ARR, "URNO", "" ) ; 
  ilRC = setStatusInStruct ("URNO", C_ENABLE, C_SELECT, &sMV1_ARR) ;  
	ilRC = AddFld2Struct (&sMV1_ARR, "ALC3", "" ) ; 
  ilRC = setStatusInStruct ("ALC3", C_ENABLE, C_SELECT, &sMV1_ARR) ;  
	ilRC = AddFld2Struct (&sMV1_ARR, "APC3", "" ) ; 
  ilRC = setStatusInStruct ("APC3", C_ENABLE, C_SELECT, &sMV1_ARR) ;  
	ilRC = AddFld2Struct (&sMV1_ARR, "TEAD", "" ) ; 
  ilRC = setStatusInStruct ("TEAD", C_ENABLE, C_SELECT, &sMV1_ARR) ;  
	ilRC = AddFld2Struct (&sMV1_ARR, "TEMA", "" ) ; 
  ilRC = setStatusInStruct ("TEMA", C_ENABLE, C_SELECT, &sMV1_ARR) ;  
	ilRC = AddFld2Struct (&sMV1_ARR, "SMVT", "" ) ; 
  ilRC = setStatusInStruct ("SMVT", C_ENABLE, C_SELECT, &sMV1_ARR) ;  
	ilRC = AddFld2Struct (&sMV1_ARR, "VAFR", "" ) ; 
  ilRC = setStatusInStruct ("VAFR", C_ENABLE, C_SELECT, &sMV1_ARR) ;  
	ilRC = AddFld2Struct (&sMV1_ARR, "BEME", "" ) ; 
  ilRC = setStatusInStruct ("BEME", C_ENABLE, C_SELECT, &sMV1_ARR) ;  
	ilRC = AddFld2Struct (&sMV1_ARR, "PAYE", "" ) ; 
  ilRC = setStatusInStruct ("PAYE", C_ENABLE, C_SELECT, &sMV1_ARR) ;  

 ilRC = AddFld2Struct (&sBRS_ARR, "IERR", "" ) ; /* Test Fld. BRS */
 ilRC = setStatusInStruct ("IERR", C_DISABLE, 0, &sBRS_ARR) ;

 /* read all globals into globals fields    - GLOBAL -  */
 sprintf (clSelection, "GLOBAL") ; 

 /* read home airport */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "HOMEAIRPORT", CFG_STRING,pcgHomeHopo);
 if (ilRC != RC_SUCCESS)
 {
  dbg (DEBUG, "ERR! - cant read <HOMEAIRPORT> from <%s>", cpConfigFile) ;
  pcgHomeHopo[0] = 0x00 ;
	ilStat = RC_FAIL ;     /* input needed */
 }
 
 /* GET TLX Destination */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "TELEX_MOD_ID", CFG_STRING, pclTmpBuf) ; 
 if (ilRC == RC_SUCCESS)
 {
   igToTlxQue = atoi(pclTmpBuf);
 }
 dbg(TRACE,"SENDING TELEXES TO TELEX_MOD_ID %d",igToTlxQue);

 /* SITA7 telex commands v.1.07 */
 ilRC = iGetConfigEntry(cpConfigFile, "SITA7", "TLX_TYPES", CFG_STRING, pcgSita7TlxTypes) ;
 if (ilRC == RC_SUCCESS)
 {
   dbg(TRACE,"[SITA7].[TLX_TYPES] = <%s>",pcgSita7TlxTypes);
 }
 else
 {
  strcpy (pcgSita7TlxTypes,"MVT,LDM,PTM,CPM");
  dbg(TRACE,"*** [SITA7].[TLX_TYPES] not defined - default to <%s> ***",pcgSita7TlxTypes);
 } 

 /* SITA7 Telex Queue ID. If not to send to a queue, configure it to -1 */
 ilRC = iGetConfigEntry(cpConfigFile, "SITA7", "TELEX_MOD_ID", CFG_STRING, pclTmpBuf) ; 
 if (ilRC == RC_SUCCESS)
 {
   igToTlxQueSita7 = atoi(pclTmpBuf);
 }
 dbg(TRACE,"[SITA7].[TELEX_MOD_ID] <%d> : Send Telexes to this module",igToTlxQueSita7);


 /* SITA7 create file? 1.07 */
 igSita7CreateFile = 0;
 ilRC = iGetConfigEntry(cpConfigFile, "SITA7", "CREATE_FILE", CFG_STRING, pclTmpBuf) ;
 if (ilRC == RC_SUCCESS)
 {
   if ((strcmp (pclTmpBuf,"YES") == 0) || (strcmp (pclTmpBuf,"yes") == 0))
   {
     igSita7CreateFile = 1;
   }
 }
 dbg(TRACE,"*** [SITA7].[CREATE_FILE] <%d> ***",igSita7CreateFile);
  
 /* SITA7 Filename Extension v.1.07 */
 ilRC = iGetConfigEntry(cpConfigFile, "SITA7", "FILENAME_EXT", CFG_STRING, pcgSita7FileNameExt) ;
 if (ilRC == RC_SUCCESS)
 {
   dbg(TRACE,"[SITA7].[FILENAME_EXT] = <%s>",pcgSita7FileNameExt);
 }
 else
 {
   strcpy (pcgSita7FileNameExt,".TXT");
   dbg(TRACE,"*** [SITA7].[FILENAME_EXT] not defined - default to <%s> ***",pcgSita7FileNameExt);
 } 

 /* SITA7 File Directory v.1.07 */
 ilRC = iGetConfigEntry(cpConfigFile, "SITA7", "FILE_DIR", CFG_STRING, pcgSita7FileDir) ;
 if (ilRC == RC_SUCCESS)
 {
   dbg(TRACE,"[SITA7].[FILE_DIR] = <%s>",pcgSita7FileDir);
 }
 else
 {
   strcpy (pcgSita7FileDir,"/ceda/tmp");
   dbg(TRACE,"*** [SITA7].[FILE_DIR] not defined - default to <%s> ***",pcgSita7FileDir);
 } 


 
 /* read next date offset */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "DATE_OFFSET", CFG_STRING,
												pcgDateOffset) ; 
 if (ilRC != RC_SUCCESS)
 {
  dbg (DEBUG, "ERR! - cant read <DATE_OFFSET> from <%s>", cpConfigFile) ;
  pcgDateOffset[0] = 0x00 ;
	ilStat = RC_FAIL ;     /* input needed */
 }
 else
 {
  dbg(DEBUG,
			"read <DATE_OFFSET> from <%s> with <%s>",cpConfigFile,pcgDateOffset); 
 }

 /* read send time info */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "INFO_TIME", CFG_STRING,
												pcgTimeInfo) ; 
 if (ilRC != RC_SUCCESS)
 {
  dbg (DEBUG, "ERR! - cant read <INFO_TIME> from <%s>", cpConfigFile) ;
  pcgTimeInfo[0] = 0x00 ;
	ilStat = RC_FAIL ;     /* input needed */
 } 
 else
 {
  dbg(DEBUG, "read <INFO_TIME> from <%s> with <%s>",cpConfigFile,pcgTimeInfo) ;
 }

 /* -------- read TLXTAB fields -------------- */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "SQL_TLXTAB_FLDS", 
												CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "ERR! - cant read GLOBAL/SQL_TLXTAB_FLDS from <%s>",cpConfigFile) ;
  ilStat = RC_FAIL ; 
 }
 else    /* bring fields into the struct */  
 { 
	/*  store the comlete field list in field #TLX */
	ilRC = AddFld2Struct (&sTLX_ARR, "#TLX", cpDestStr) ; /* get SQL Fld. list */
  ilRC = setStatusInStruct ("#TLX", C_DISABLE, 0, &sTLX_ARR) ;
  il = 1 ; 
	do 
	{
   ilLen = get_real_item (cpFoundStr, cpDestStr, il) ;
	 if (ilLen > 0)
	 {
		ilRC = AddFld2Struct (&sTLX_ARR, cpFoundStr, "") ; /* get SQL Fld */
    ilRC = setStatusInStruct (cpFoundStr, C_DISABLE, 0, &sTLX_ARR) ;
    ilRC = setStatusInStruct (cpFoundStr, C_ENABLE, C_INSERT, &sTLX_ARR) ;  
    ilRC = setStatusInStruct (cpFoundStr, C_ENABLE, C_SELECT, &sTLX_ARR) ;  
	 }
	 else il = -1 ; 
	 il++ ; 
	} while (il > 0) ; 
 }

 
 /* -------- read all information for BRS fields  - BRS TELEXE -  */
 sprintf (clSelection, "BRS") ; 
 ilRC = AddFld2Struct (&sBRS_ARR, "#_ID", "BRS" ) ; /* get own id */
 ilRC = setStatusInStruct ("#_ID", C_DISABLE, 0, &sBRS_ARR) ;

 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "SQL_SELECT_FLDS", 
												CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "ERR! - cant read BRS/SQL_SELECT_FLDS from <%s>", cpConfigFile) ;
  ReplaceFldDataInStruct
	        			("BRS CANT FIND SQL_SELECT_FLDS in CFG", "IERR", &sBRS_ARR) ; 
  ilStat = RC_FAIL ; 
 }
 else    /* bring fields into the struct */  
 {
	/*  store the comlete field list in field #BRS */
	ilRC = AddFld2Struct (&sBRS_ARR, "#TLX", cpDestStr) ; /* get SQL Fld. list */
  ilRC = setStatusInStruct ("#TLX", C_DISABLE, 0, &sBRS_ARR) ;
  il = 1 ; 
	do 
	{
   ilLen = get_real_item (cpFoundStr, cpDestStr, il) ;
	 if (ilLen > 0)     
	 {
		ilRC = AddFld2Struct (&sBRS_ARR, cpFoundStr, "") ; /* get SQL Fld */
    ilRC = setStatusInStruct (cpFoundStr, C_DISABLE, 0, &sBRS_ARR) ;
    ilRC = setStatusInStruct (cpFoundStr, C_ENABLE, C_SELECT, &sBRS_ARR) ;  
	 }
	 else il = -1 ; 
	 il++ ; 
	} while (il > 0) ; 
 }

 /* read default text fields */
 /* The default text must start with the string DEFAULT_TXT_001 and */
 /* counts up like DEFAULT_TXT_002,....003 and so on                */
 il = 1 ;      /* start with DEFAULT_TXT_001 */
 while (il > 0) 
 {
	sprintf ( clCfgItem, "BRSDEF_TXT_%#03d", il) ; 
	sprintf ( clStrItem, "@%#03d", il) ;       /* in struct like @001... */ 
  dbg(DEBUG,"Generate Item <%s> for struct item <%s>", clCfgItem, clStrItem) ;
  ilRC = iGetConfigEntry(cpConfigFile, clSelection, clCfgItem, 
	  						         CFG_STRING, cpDestStr) ; 
  if (ilRC == RC_SUCCESS)
	{
   /* change the @ char to blanks ! */
   ilZ = strlen (cpDestStr) ;
	 il4Chr = 0 ;  
   while (il4Chr < ilZ) 
	 {
    if (cpDestStr[il4Chr] == '@')
		{ 
		 cpDestStr[il4Chr] = ' ' ;
    } 
		il4Chr++ ; 
	 }
   /* insert the text item into the struct */
   ilRC = AddFld2Struct (&sBRS_ARR, clStrItem, cpDestStr ) ; 
   ilRC = setStatusInStruct (clStrItem, C_DISABLE, 0, &sBRS_ARR) ;
   ilRC = setStatusInStruct (clStrItem, C_ENABLE, C_SEARCH, &sBRS_ARR) ;  
	 il++ ;                /* read next item */
	}
	else il = 0 ;          /* mark the end */
 } /* while not all DEFAULT_TXT_ITEMS read */
 getBRSHeaderInfo (sBRS_ARR, &sBRS_PRN ) ;  


 /* ------------- read BRS MAX FLIGHTS ----------- */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "BRS_MAX_FLIGHTS", 
												CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  strcpy (cpDestStr, "1") ; 
 }
 ilRC = AddFld2Struct (&sBRS_ARR, "#DAT", cpDestStr ) ; /* get date */
 ilRC = setStatusInStruct ("#DAT", C_DISABLE, 0, &sBRS_ARR) ;


 /* ------------- read DATE INTERPRET -------- */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "DATE_INTERPRET", 
												CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  sprintf (cpDestStr, "JAN,FEB,MAR,APR,MAI,JUN,JUL,AUG,SEP,OCT,NOV,DEC") ; 
 }
 AddFld2Struct (&sBRS_ARR, "#MON", cpDestStr ) ; /* get month short names  */
 setStatusInStruct ("#MON", C_DISABLE, 0, &sBRS_ARR) ;

 /* ------------- read BRS DATE OFFSET ----------- */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "BRS_DATE_OFFSET", 
												CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "ERR! - cant read BRS/BRS_DATE_OFFSET from <%s>", cpConfigFile) ;
  /* ilStat = RC_FAIL ;       can work without this field */  
 }
 else    /* bring fields into the struct */  
 {
  ilRC = AddFld2Struct (&sBRS_ARR, "#DAT", cpDestStr ) ; /* get date */
  ilRC = setStatusInStruct ("#DAT", C_DISABLE, 0, &sBRS_ARR) ;
 }

 /* ------------- read BRS TIME OFFSET ------------ */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "BRS_INFO_TIME", 
												CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "ERR! - cant read BRS/BRS_INFO_TIME from <%s>", cpConfigFile) ;
  ilStat = RC_FAIL /* can work without this field */ ;  
 }
 else    /* bring fields into the struct */  
 {
  ilRC = AddFld2Struct (&sBRS_ARR, "#TIM", cpDestStr ) ; /* get date */
  ilRC = setStatusInStruct ("#TIM", C_DISABLE, 0, &sBRS_ARR) ;
 }

 /* ------------- read BRS START DATE TIME ------------ */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "BRS_START_DATI", 
												CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
	if (cgDateFrom[0] != 0x00) 
	{
   ilRC = AddFld2Struct (&sBRS_ARR, "#SDT", cgDateFrom ) ; /* get date */
  } 
	else 
	{
   ilRC = AddFld2Struct (&sBRS_ARR, "#SDT", "" ) ; /* get no date */
	}
  ilRC = setStatusInStruct ("#SDT", C_DISABLE, 0, &sBRS_ARR) ;
 }
 else    /* bring fields into the struct */  
 {
  ilRC = AddFld2Struct (&sBRS_ARR, "#SDT", cpDestStr ) ; /* get date */
  ilRC = setStatusInStruct ("#SDT", C_DISABLE, 0, &sBRS_ARR) ;
 }


 /* ------------- read BRS PRIORITY ------------ */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "BRS_PRIORITY", 
												CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "ERR! - cant read BRS/BRS_TIME_OFFSET from <%s>", cpConfigFile) ;
  /* ilStat = RC_FAIL ;    can work without this field */  
 }
 else    /* bring fields into the struct */  
 {
  ilRC = AddFld2Struct (&sBRS_ARR, "#PRI", cpDestStr ) ; /* get date */
  ilRC = setStatusInStruct ("#PRI", C_DISABLE, 0, &sBRS_ARR) ;
 }

 /* ------------- read BRS TELEX SWITCH ON ------------ */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "BRS_TELEXE_ON", 
												CFG_STRING, cpDestStr) ; 
 igBRS_Flag = C_DISABLE ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "WARNING! - cant read BRS/BRS_TELEXE_ON from <%s>", cpConfigFile) ;
  dbg(DEBUG, "SET BRS TELEX SENDING TO DISABLED") ; 
 }
 else    /* bring fields into the struct */  
 {
  ilRC = AddFld2Struct (&sBRS_ARR, "#SON", cpDestStr ) ; /* get date */
  ilRC = setStatusInStruct ("#SON", C_DISABLE, 0, &sBRS_ARR) ;
  if (cpDestStr[0] == '1')
	{
	 igBRS_Flag = C_ENABLE ;  
   dbg(DEBUG, "SET BRS TELEX SENDING TO ENABLED") ; 
  }
	else 
	{
   dbg(DEBUG, "SET BRS TELEX SENDING TO DISABLED") ; 
	}
 }

 /* ------------- read BRS SEND FILEPATH --------- */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "BRS_SEND_TLX_PATH", 
												CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "ERR! - cant read BRS/BRS_SEND_TLX_PATH from <%s>", cpConfigFile) ;
  ReplaceFldDataInStruct
	        			("BRS CANT FIND BRS_SEND_TLX_PATH in CFG", "IERR", &sBRS_ARR) ; 
  ilStat = RC_FAIL ;    /* need this field */ 
 }
 else    /* bring fields into the struct */  
 {
  ilRC = AddFld2Struct (&sBRS_ARR, "#FIL", cpDestStr ) ; /* get date */
  ilRC = setStatusInStruct ("#FIL", C_DISABLE, 0, &sBRS_ARR) ;
 }


 /* ------------- read BRS ADRESS LIST --------- */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "BRS_ADRESS_LIST", 
												CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "ERR! - cant read BRS/BRS_ADRESS_LIST from <%s>", cpConfigFile) ;
  ReplaceFldDataInStruct
	        			("BRS CANT FIND BRS_ADRESS_LIST in CFG", "IERR", &sBRS_ARR) ; 
  ilStat = RC_FAIL ;    /* need this field */ 
 }
 else    /* bring fields into the struct */  
 {
  ilRC = AddFld2Struct (&sBRS_ARR, "#ADR", cpDestStr ) ; /* get date */
  ilRC = setStatusInStruct ("#FIL", C_DISABLE, 0, &sBRS_ARR) ;
 }



 /* MVT Information */
 /* -------- read all information for MVT fields  - MVT TELEXE -  */
 sprintf (clSelection, "MVT") ; 

 /* ------------- read MVT TELEX SWITCH ON ------------ */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "MVT_TELEXE_ON", 
												CFG_STRING, cpDestStr) ; 
 igMVT_Flag = C_DISABLE ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "WARNING! - cant read MVT/MVT_TELEXE_ON from <%s>", cpConfigFile) ;
  dbg(DEBUG, "SET MVT TELEX SENDING TO DISABLED") ;   
 }
 else    /* bring fields into the struct */  
 {
  ilRC = AddFld2Struct (&sMVT_ARR, "#SON", cpDestStr ) ; /* get date */
  ilRC = setStatusInStruct ("#SON", C_DISABLE, 0, &sMVT_ARR) ;
	if (cpDestStr[0] == '1')
	{
   dbg(DEBUG, "SET MVT TELEX SENDING TO ENABLED") ;  
   igMVT_Flag = C_ENABLE ; 
	}
	else 
	{
   dbg(DEBUG, "SET MVT TELEX SENDING TO DISABLED") ;   
	}
 }

 /* ------------ get my own id ------------------------ */
 ilRC = AddFld2Struct (&sMVT_ARR, "#_ID", "MVT" ) ; /* get own id */
 ilRC = setStatusInStruct ("#_ID", C_DISABLE, 0, &sMVT_ARR) ;

 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "SQL_SELECT_FLDS", 
												CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "ERR! - cant read MVT/SQL_SELECT_FLDS from <%s>", cpConfigFile) ;
  ReplaceFldDataInStruct
	        			("MVT CANT FIND SQL_SELECT_FLDS in CFG", "IERR", &sMVT_ARR) ; 
  ilStat = RC_FAIL ; 
 }
 else    /* bring fields into the struct */  
 {
	/*  store the comlete field list in field #MVT */
	ilRC = AddFld2Struct (&sMVT_ARR, "#TLX", cpDestStr) ; /* get SQL Fld. list */
  ilRC = setStatusInStruct ("#TLX", C_DISABLE, 0, &sMVT_ARR) ;
  il = 1 ; 
	do 
	{
   ilLen = get_real_item (cpFoundStr, cpDestStr, il) ;
	 if (ilLen > 0)     
	 {
		ilRC = AddFld2Struct (&sMVT_ARR, cpFoundStr, "") ; /* get SQL Fld */
    ilRC = setStatusInStruct (cpFoundStr, C_DISABLE, 0, &sMVT_ARR) ;
    ilRC = setStatusInStruct (cpFoundStr, C_ENABLE, C_SELECT, &sMVT_ARR) ;  
	 }
	 else il = -1 ; 
	 il++ ; 
	} while (il > 0) ; 
 }


 /* ------------- read MVT SEND FILEPATH --------- */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "MVT_SEND_TLX_PATH", 
												CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "ERR! - cant read MVT/MVT_SEND_TLX_PATH from <%s>", cpConfigFile) ;
  ReplaceFldDataInStruct
	        			("MVT CANT FIND MVT_SEND_TLX_PATH in CFG", "IERR", &sMVT_ARR) ; 
  ilStat = RC_FAIL ;    /* need this field */ 
 }
 else    /* bring fields into the struct */  
 {
  ilRC = AddFld2Struct (&sMVT_ARR, "#FIL", cpDestStr ) ; /* get date */
  ilRC = setStatusInStruct ("#FIL", C_DISABLE, 0, &sMVT_ARR) ;
 }


 /* The default text must start with the string CL2SRV_CHRxx  */
 /* where x a raising sequenz from 01 TO 99 be should         */
 il = 1 ;      /* start with CL2SRV_CHR01 */
 while (il > 0) 
 {
	sprintf ( clCfgItem, "CL2SRV_CHR%#02d", il) ; 
	sprintf ( clStrItem, "@C%#02d", il) ;       /* in struct like @001... */ 
  dbg(DEBUG,"Generate Item <%s> for struct item <%s>", clCfgItem, clStrItem) ;
  ilRC = iGetConfigEntry(cpConfigFile, clSelection, clCfgItem, 
	  						         CFG_STRING, cpDestStr) ; 
  if (ilRC == RC_SUCCESS)
	{
   /* insert the text item into the struct */
   ilRC = AddFld2Struct (&sMVT_ARR, clStrItem, cpDestStr ) ; 
   ilRC = setStatusInStruct (clStrItem, C_DISABLE, 0, &sMVT_ARR) ;
	 il++ ;                /* read next item */
	}
	else il = 0 ;          /* mark the end */
 } /* while not all CL2SRV_SRV ITEMS read */
 ilRC = RC_SUCCESS ;  

 return (ilRC) ; 

} /* GetItemsFromCfgFile() */


 /* read default text fields */





/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_cedadb()
{
  int     ilRC = RC_SUCCESS ;                      /* Return code */

  /* now reading from configfile or from database */
  igInitOK = TRUE ;
  return (ilRC) ;

} /* end of initialize */





/*****************************************************************************/
/* The Reset routine                                                         */
/*****************************************************************************/
static int Reset()
{
 int     ilRC = RC_SUCCESS ;           /* Return code */

 dbg(TRACE,"Reset: now resetting") ;
 return ilRC ;

} /* end of Reset */






/*****************************************************************************/
/* The termination routine                                                   */
/*****************************************************************************/
static void Terminate()
{
 /* unset SIGCHLD ! DB-Child will terminate ! */
 logoff() ;
 dbg (TRACE, "Terminate: now leaving ...") ;

 exit(0) ;

} /* end of Terminate */






/*****************************************************************************/
/* The handle signals routine                                                */
/*****************************************************************************/
static void HandleSignal(int pipSig)
{
 int  ilRC = RC_SUCCESS ;                      /* Return code */

 dbg(TRACE, "HandleSignal: signal <%d> received", pipSig) ;
 switch(pipSig)
 {
  default :
           Terminate();
          break;
 } /* switch */
 exit(0) ;

} /* end of HandleSignal */







/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr (int pipErr)
{
 int     ilRC = RC_SUCCESS ;
 return ;
} /* end of HandleErr */







/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr (int pipErr)
{
 int     ilRC = RC_SUCCESS;

 switch (pipErr)
 {
  case    QUE_E_FUNC      :       /* Unknown function */
          dbg (TRACE, "<%d> : unknown function", pipErr) ;
         break ;
  case    QUE_E_MEMORY    :       /* Malloc reports no memory */
          dbg (TRACE, "<%d> : malloc failed", pipErr) ;
         break ;
  case    QUE_E_SEND      :       /* Error using msgsnd */
          dbg (TRACE, "<%d> : msgsnd failed", pipErr) ;
         break ;
  case    QUE_E_GET       :       /* Error using msgrcv */
          dbg (TRACE, "<%d> : msgrcv failed", pipErr) ;
         break ;
  case    QUE_E_EXISTS    :
          dbg (TRACE, "<%d> : route/queue already exists ", pipErr) ;
         break ;
  case    QUE_E_NOFIND    :
          dbg (TRACE, "<%d> : route not found ", pipErr) ;
         break ;
  case    QUE_E_ACKUNEX   :
          dbg (TRACE, "<%d> : unexpected ack received ", pipErr) ;
         break;
  case    QUE_E_STATUS    :
          dbg (TRACE, "<%d> :  unknown queue status ", pipErr) ;
         break;
  case    QUE_E_INACTIVE  :
          dbg (TRACE, "<%d> : queue is inaktive ", pipErr) ;
         break ;
  case    QUE_E_MISACK    :
          dbg (TRACE, "<%d> : missing ack ", pipErr) ;
         break ;
  case    QUE_E_NOQUEUES  :
          dbg (TRACE, "<%d> : queue does not exist", pipErr) ;
         break ;
  case    QUE_E_RESP      :       /* No response on CREATE */
          dbg(TRACE, "<%d> : no response on create", pipErr) ;
         break ;
  case    QUE_E_FULL      :
          dbg (TRACE, "<%d> : too many route destinations", pipErr) ;
         break ;
  case    QUE_E_NOMSG     :       /* No message on queue */
          dbg (TRACE, "<%d> : no messages on queue", pipErr) ;
         break ;
  case    QUE_E_INVORG    :       /* Mod id by que call is 0 */
          dbg (TRACE, "<%d> : invalid originator=0",pipErr);
         break ;
  case    QUE_E_NOINIT    :       /* Queues is not initialized*/
          dbg (TRACE, "<%d> : queues are not initialized", pipErr) ;
         break ;
  case    QUE_E_ITOBIG    :
          dbg (TRACE, "<%d> : requestet itemsize to big ", pipErr) ;
         break ;
  case    QUE_E_BUFSIZ    :
          dbg (TRACE, "<%d> : receive buffer to small ", pipErr) ;
         break ;
 default                 :       /* Unknown queue error */
         dbg (TRACE,"<%d> : unknown error", pipErr) ;
         break;
 } /* end switch */

 return ;
} /* end of HandleQueErr */


/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
 int  ilRC = RC_SUCCESS ;                      /* Return code */
 int  ilBreakOut = FALSE ;

 do
 {
  ilRC = que (QUE_GETBIG, 0, mod_id, PRIORITY_3, igItemLen, (char *)&prgItem) ;
  /* depending on the size of the received item  */
  /* a realloc could be made by the que function */
  /* so do never forget to set event pointer !!! */
  prgEvent = (EVENT *) prgItem->text ;
  if( ilRC == RC_SUCCESS )
  {
   /* Acknowledge the item */
   ilRC = que(QUE_ACK,0,mod_id,0,0,NULL) ;
   if( ilRC != RC_SUCCESS )
   {
    /* handle que_ack error */
    HandleQueErr(ilRC);
   } /* fi */

   switch ( prgEvent->command )
   {
    case    HSB_STANDBY     :
            ctrl_sta = prgEvent->command ;
           break ;

    case    HSB_COMING_UP   :
            ctrl_sta = prgEvent->command ;
           break ;

    case    HSB_ACTIVE      :
            ctrl_sta = prgEvent->command ;
            ilBreakOut = TRUE;
           break ;

    case    HSB_ACT_TO_SBY  :
            ctrl_sta = prgEvent->command ;
           break ;

    case    HSB_DOWN        :
            /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
            ctrl_sta = prgEvent->command ;
            Terminate() ;
           break ;

    case    HSB_STANDALONE  :
            ctrl_sta = prgEvent->command;
            ResetDBCounter() ;
            ilBreakOut = TRUE ;
           break ;

    case    REMOTE_DB :
            /* ctrl_sta is checked inside */
            HandleRemoteDB(prgEvent) ;
           break ;

    case    SHUTDOWN        :
            Terminate() ;
           break ;

    case    RESET           :
            ilRC = Reset() ;
           break ;

    case    EVENT_DATA      :
            dbg(TRACE, "HandleQueues: wrong hsb status <%d>", ctrl_sta) ;
            DebugPrintItem (TRACE, prgItem) ;
            DebugPrintEvent (TRACE, prgEvent) ;
           break ;

    case    TRACE_ON :
            dbg_handle_debug(prgEvent->command) ;
           break ;

    case    TRACE_OFF :
            dbg_handle_debug(prgEvent->command) ;
           break ;
    default                 :
            dbg(TRACE, "HandleQueues: unknown event") ;
            DebugPrintItem (TRACE, prgItem) ;
            DebugPrintEvent (TRACE, prgEvent) ;
           break ;
   } /* end switch */
  }
  else
  {                           /* Handle queuing errors */
   HandleQueErr (ilRC) ;
  } /* else */
 } while (ilBreakOut == FALSE) ;

 if(igInitOK == FALSE)
 {
  ilRC = Init_cedadb() ;
  if(ilRC != RC_SUCCESS)
  {
   dbg (TRACE,"Init_cedadb: init failed!") ;
  } /* if */
 }/* if */
                              /* OpenConnection() ; */
} /* end of HandleQueues */



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData()
{
  int       ilRC = RC_SUCCESS ;          /* Return code */
  BC_HEAD   *prlBchd ;
  CMDBLK    *prlCmdblk ;
  char      pclActCmd[16] ;
  char      *pclSelKey ;
  char      *pclFields ;
  char      *pclData ;
  char      cpDateFrom [128] ; 
	char      cpDateTo[128] ; 
  char      clActDate[128] ; 
	char      cpUrno[100];
	char      cpAction[100] ;
	char      clFldItem1[1024] ; 
	char      clFldItem2[1024] ;
  int       ilLen ; 
	int       ilStat ; 


  que_out = prgEvent->originator;
  prlBchd    = (BC_HEAD *) ((char *) prgEvent + sizeof(EVENT)) ;
  prlCmdblk  = (CMDBLK *) ((char *)prlBchd->data) ;
  pclSelKey  = (char *) prlCmdblk->data ;
  pclFields  = (char *) pclSelKey + strlen(pclSelKey) + 1 ;
  pclData    = (char *) pclFields + strlen(pclFields) + 1 ;

  /* Save global elements of BC_HEAD */
  /* workstation */
  memset (pcgRecvName, '\0', (sizeof(prlBchd->recv_name) + 1)) ;
  strncpy (pcgRecvName, prlBchd->recv_name, sizeof(prlBchd->recv_name)) ;

  /* User  */
  memset (pcgDestName, '\0', sizeof(prlBchd->recv_name)) ;
  strncpy (pcgDestName, prlBchd->dest_name, sizeof(prlBchd->recv_name)) ;
  sprintf (pcgChkWks, "%s,", pcgRecvName) ;
  sprintf (pcgChkUsr, "%s,", pcgDestName) ;

  /* Save Global info of CMDBLK */
  strcpy (pcgTwStart, prlCmdblk->tw_start) ;
  strcpy (pcgTwEnd, prlCmdblk->tw_end) ;
  strcpy (pclActCmd, prlCmdblk->command) ;

  dbg (TRACE, "CMD <%s>    WKS <%s>     USR <%s> ",
             pclActCmd, pcgRecvName, pcgDestName) ;

  dbg (TRACE, "SPECIAL INFO TWS <%s>    TWE <%s> ",
             pcgTwStart, pcgTwEnd ) ;

  dbg (TRACE, "...FIELDS <%s>", pclFields) ;
  dbg (TRACE, "...DATA   <%s>", pclData) ;
  dbg (TRACE, "...SELECT <%s>", pclSelKey) ;

  if (prlBchd->rc == RC_FAIL)
  {
   return (RC_SUCCESS) ;
  }

  ilRC = RC_SUCCESS ; 
  if (strcmp(pclActCmd, "BRS") == 0)
  {
	 dbg (DEBUG, "-------------------------------") ;
   dbg (DEBUG, "HandeData: get <BRS> Command   ") ;
   dbg (DEBUG, "-------------------------------") ;
   if (igBRS_Flag == C_ENABLE) 
	 {
    dbg (DEBUG, "BRS TELEX MODE IS SWITCHED OFF !!!") ; 
	 }
	 else 
	 {
	  if (startWhileTimeReached (&sBRS_ARR, cpDateFrom, cpDateTo) == RC_SUCCESS) 
    {
		 ilRC = handleBRS_Call (cpDateFrom, cpDateTo) ;
     if (ilRC == RC_SUCCESS)
		 {                     /* set new locate time into BRS_ARR -> Fld. #SDT */
      ilRC = GetLocalTimeStamp(clActDate) ;
		  ReplaceFldDataInStruct (clActDate, "#SDT", &sBRS_ARR) ; 
     }
	  }
	 } /* BRS TELEXE WORK... */
  }

  else    /* other commands */ 
	{
   if (strcmp(pclActCmd, "SMV") == 0)
   {
    dbg (DEBUG, "-------------------------------") ;
    dbg (DEBUG, "HandeData: get <MVT> Command   ") ;
    dbg (DEBUG, "-------------------------------") ;
    get_real_item (cpUrno, pclData, 1) ;
    ReadTelexAndSendSMV (cpUrno ) ; 
   }

   /* v.1.07 - convert std telex from user to SITA7 - UFIS-1973 */
   if (strcmp(pclActCmd, "SITA7") == 0)
   {
    dbg (DEBUG, "-------------------------------") ;
    dbg (DEBUG, "HandeData: get <SITA7> Command - conv TLXTAB to SITA7 telex") ;
    dbg (DEBUG, "-------------------------------") ;
    sprintf(pcgTwEnd,"%s,TAB,TLXGEN",pcgHomeHopo);
    get_real_item (cpUrno, pclData, 1) ;
    ReadTelexAndSendSITA7(pclFields, pclData);
   }



   else if (strcmp(pclActCmd, "TLX") == 0)
   {
     ForwardTelexToFdihdl(pclSelKey, pclData);
   }
   else if (strcmp(pclActCmd, "INF") == 0)
   {
     ilRC = CreateInftabXML(pclSelKey, pclFields, pclData);
   }
   else
   {
    if ((pclActCmd[0] == 'M') && (pclActCmd[1] == 'V') && (pclActCmd[2] == 'T'))
    {
     dbg (DEBUG, "---------------------------------") ;
     dbg (DEBUG, "HandeData: get <MVTxx> Command   ") ;
     dbg (DEBUG, "---------------------------------") ;
     /* read the data from the data string */
     ilStat = 0 ; 
		 ilLen = get_real_item (clFldItem1, pclFields, 1) ;  
     if (strlen (clFldItem1) < strlen (pclFields)) 
		 {
			if (strcmp(clFldItem1, "URNO") == 0) ilStat = 1 ;  
		  ilLen = get_real_item (clFldItem2, pclFields, 2) ;
			if (strcmp(clFldItem2, "URNO") == 0) ilStat = 2 ;  
     }
		 cpAction[0] = 0x00 ; 
		 switch (ilStat)   /* have URNO Field found */ 
		 {
		  case 1 : ilLen = get_real_item (cpUrno, pclData, 1) ;
		           ilLen = get_real_item (clFldItem1, pclData, 2) ;
             break ; 

			case 2 : ilLen = get_real_item (cpUrno, pclData, 2) ;
		           ilLen = get_real_item (clFldItem1, pclData, 1) ;
             break ; 

      default:  dbg (DEBUG, "URNO NOT FOUND") ; 
             break ; 
     } 

		 strcpy (cpAction, &pclActCmd[3]) ; 
		 if (cpAction[0] != 0x00) 
		 {
		  ilRC = handleMVT_Call (cpUrno, cpAction) ;
     }
     else
		 {
			dbg (DEBUG, "Action gets unknown ") ;
      dbg (TRACE, "CANT INTERPRET CMD");
      dbg (TRACE, "=== DATA ELEMENTS FROM ORIGINATOR =======");
      dbg (TRACE, "... CMD    <%s>", pclActCmd) ;
      dbg (TRACE, "... FIELDS <%s>", pclFields) ;
      dbg (TRACE, "... DATA   <%s>", pclData) ;
      dbg (TRACE, "... SELECT <%s>", pclSelKey) ;
      dbg (TRACE, "=========================================");
		 } ; 
		} 
    else 
    {          /* unknown command */
     dbg (TRACE, "UNKNOWN COMMAND RECEIVED");
     dbg (TRACE, "=== DATA ELEMENTS FROM ORIGINATOR =======");
     dbg (TRACE, "... CMD    <%s>", pclActCmd) ;
     dbg (TRACE, "... FIELDS <%s>", pclFields) ;
     dbg (TRACE, "... DATA   <%s>", pclData) ;
     dbg (TRACE, "... SELECT <%s>", pclSelKey) ;
     dbg (TRACE, "=========================================");
    } 
   }
	}

 return ilRC ;


} /* end of HandleData */



static int CreateInftabXML(char *pcpSelKey, char *pcpFields, char *pcpData)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  char pclGocTbl[32];
  char pclGocCmd[32];
  char pclGocKey[32];
  char pclGocFld[32];
  char pclGocDat[32];
  char pclInfTabFields[128];
  char pclOutMsg[4096*2];
  char pclSqlBuf[128];
  char pclSqlData[4096];
  char pclTmpData[4096];
  char pclTmpItem[4096];
  int ilOutPos = 0;
  int ilLen = 0;
  short sql_cursor = 0;
  short sql_fkt = 0;
  dbg(TRACE,"SELKEY <%s>",pcpSelKey);
  dbg(TRACE,"FIELDS <%s>",pcpFields);
  dbg(TRACE,"DATA   <%s>",pcpData);
  strcpy(pclInfTabFields,"APC3,APPL,FUNK,AREA,LSTU,USEU,TEXT");
  sprintf(pclSqlBuf,"SELECT %s FROM INFTAB WHERE URNO='%s'",pclInfTabFields,pcpData);
  pclSqlData[0] = 0x00;
  sql_cursor = 0 ;
  sql_fkt = START ;
  ilGetRc = sql_if(sql_fkt, &sql_cursor, pclSqlBuf, pclSqlData);
  close_my_cursor (&sql_cursor) ;
  if (ilGetRc == DB_SUCCESS)
  {
    ilRC = BuildItemBuffer(pclSqlData,pclInfTabFields,0,",");
    dbg(TRACE,"%s",pclSqlData);
    ilOutPos = 0;
    ilLen = get_real_item(pclTmpItem, pclSqlData, 1) ;
    sprintf(pclTmpData,"<INFO>\n  <PATH>\n    <ROOT>%s</ROOT>\n",pclTmpItem);
    StrgPutStrg(pclOutMsg, &ilOutPos, pclTmpData, 0, -1, "\0");
    ilLen = get_real_item(pclTmpItem, pclSqlData, 2) ;
    sprintf(pclTmpData,"    <CATEGORY>%s</CATEGORY>\n",pclTmpItem);
    StrgPutStrg(pclOutMsg, &ilOutPos, pclTmpData, 0, -1, "\0");
    ilLen = get_real_item(pclTmpItem, pclSqlData, 3) ;
    sprintf(pclTmpData,"    <PARTITION>%s</PARTITION>\n",pclTmpItem);
    StrgPutStrg(pclOutMsg, &ilOutPos, pclTmpData, 0, -1, "\0");
    ilLen = get_real_item(pclTmpItem, pclSqlData, 4) ;
    sprintf(pclTmpData,"    <SECTION>%s</SECTION>\n",pclTmpItem);
    StrgPutStrg(pclOutMsg, &ilOutPos, pclTmpData, 0, -1, "\0");
    sprintf(pclTmpData,"  </PATH>\n  <MESSAGE>\n");
    StrgPutStrg(pclOutMsg, &ilOutPos, pclTmpData, 0, -1, "\0");
    ilLen = get_real_item(pclTmpItem, pclSqlData, 5) ;
    sprintf(pclTmpData,"    <PUBLISHED>%s</PUBLISHED>\n",pclTmpItem);
    StrgPutStrg(pclOutMsg, &ilOutPos, pclTmpData, 0, -1, "\0");
    ilLen = get_real_item(pclTmpItem, pclSqlData, 6) ;
    sprintf(pclTmpData,"    <PUBLISHER>%s</PUBLISHER>\n",pclTmpItem);
    StrgPutStrg(pclOutMsg, &ilOutPos, pclTmpData, 0, -1, "\0");
    ilLen = get_real_item(pclTmpItem, pclSqlData, 7) ;
    PatchTextServerToClient(pclTmpItem,TRUE);
    if ((strstr(pclTmpItem,"<") != NULL) || (strstr(pclTmpItem,"<") != NULL) ||
        (strstr(pclTmpItem,"&") != NULL))
    {
      sprintf(pclTmpData,"    <FULLTEXT><![CDATA[%s]]></FULLTEXT>\n",pclTmpItem);
    }
    else
    {
      sprintf(pclTmpData,"    <FULLTEXT>%s</FULLTEXT>\n",pclTmpItem);
    }
    StrgPutStrg(pclOutMsg, &ilOutPos, pclTmpData, 0, -1, "\0");
    sprintf(pclTmpData,"  </MESSAGE>\n</INFO>\n");
    StrgPutStrg(pclOutMsg, &ilOutPos, pclTmpData, 0, -1, "\0");
    ilOutPos--;
    pclOutMsg[ilOutPos] = 0x00;
    dbg(TRACE,"\n%s",pclOutMsg);

    strcpy(pclGocTbl,"TBL");
    strcpy(pclGocCmd,"XMLO");
    strcpy(pclGocFld,"FLD");
    strcpy(pclGocFld,"DAT");
    strcpy(pclGocKey,"KEY");
    (void) tools_send_info_flag (igToInfQue, 0, pcgDestName, "", pcgRecvName, "", "",
                                 pcgTwStart, pcgTwEnd, pclGocCmd,
                                 pclGocTbl, pclGocKey, pclGocFld, pclOutMsg, 3);



  }
  return ilRC ;
}


static int handleMVT_Call (char *cpUrno, char *cpAction )
{
 
 int   ilRC = RC_SUCCESS ;
 int   il ;
 int   ilLen ; 
 int   ilGetRc ;
 int   ilMoreRecs ; 
 short sql_cursor ;
 short sql_fkt ;
 char  clSqlStatement[2048] ;
 char  clCondFld[2048] ; 
 char  clCondData[2048] ; 
 char  clSelFlds[2048] ; 
 char  clData [4096] ; 
 char  clDataItem [1024] ; 
 char  clFldItem[1024] ; 
 char  pclFldLst[1024] ; 
 char  pclDatLst[4096] ; 

 /* test if the date and time correct, for sending telexs */
 {
  clSelFlds[0]  = 0x00 ;    /* create sql field list */ 
	clCondData[0] = 0x00 ; 
  ilRC=createFldAndDataLst(clSelFlds, clCondData, C_SELECT, sMVT_ARR) ;
  sprintf (clSqlStatement,
  				"SELECT %s FROM AFTTAB WHERE URNO = '%s'", clSelFlds, cpUrno) ;  
  if (igTestFktFlag)
  {
   dbg (DEBUG, "SQLSelectRecord: SQL SELECT STATEMENT = <%s>",
							 clSqlStatement) ;
  }
  sprintf (clData, clCondData) ;        /* prepare data pool */
  sql_cursor = 0 ;
  sql_fkt = START ;
  ilGetRc = sql_if (sql_fkt, &sql_cursor, clSqlStatement, clData) ;
  close_my_cursor (&sql_cursor) ;
  if (ilGetRc == DB_SUCCESS)
  {
   ilRC = BuildItemBuffer(clData,clSelFlds,0,",") ;
	 ilMoreRecs++ ; 
   ilRC = RC_SUCCESS ;
   /* telex record */ 
   dbg (DEBUG, "SQLSelectRecord: find rec=<%ld> data = <%s>",
	  					 ilMoreRecs, clData) ;
   il = 1 ; 
   do 
	 {
    ilLen = get_real_item (clFldItem, clSelFlds, il) ;
	  if (ilLen > 0)     
	  {
     ilLen = get_real_item (clDataItem, clData, il) ;
	   ilRC = ReplaceFldDataInStruct (clDataItem, clFldItem, &sMVT_ARR) ;
	  }
	  else il = -1 ; 
	  il++ ; 
   } while (il > 0) ; 
  
	 /* write this information into the tlxtab */
   ilRC = setTlxTableDat (&sTLX_ARR, &sMVT_ARR ) ;  
   ilRC = readMVTTab () ;
	 if (ilRC == RC_SUCCESS) /* records read */
	 {
	  ilRC = getFldDataFromStruct (clDataItem, "TEAD", sMV1_ARR) ; 
    if (strlen(clDataItem) > 0) 
	  {
	   ilRC = ReplaceFldDataInStruct (clDataItem, "TXT2", &sTLX_ARR) ;
     /* change all other important files ...... */
		}

    /* write here all other used changes from MV1_ARR (Data from MVT TABLE */
		/* AFT_ARR and the styp from action */ 
    strcpy (clDataItem, "MVT") ;      /* set TTPY */
		/* if (strcmp(cpAction, "AD") == 0) strcpy (clDataItem, "MVT") ; */ 
		/* if (strcmp(cpAction, "ED") == 0) strcpy (clDataItem, "MVT") ; */ 
		ilRC = ReplaceFldDataInStruct (clDataItem, "TTYP", &sTLX_ARR) ;

		/* set STYP */
		strcpy (clDataItem, cpAction) ; 
	  ilRC = ReplaceFldDataInStruct (cpAction, "STYP", &sTLX_ARR) ;

	  /* create Telex                  */
    ilRC = SQLInsertTlxTab (sTLX_ARR) ;

		/* send the info to the braodcast handler 1900 */
    /* first set field and data list for Tlx */ 
    getFldDataFromStruct (pclFldLst, "#TLX", sTLX_ARR) ; 
    createValStr4Tlx (pclDatLst, sTLX_ARR) ;

		ilRC = SendCedaEvent (1900,0,"ufis","","0","BKK,TAB,TLXGEN",
       				 "MVT", "TLXTAB", "TELEXE",pclFldLst,pclDatLst,
			       	 "", 4, RC_SUCCESS) ; 
    dbg (DEBUG, "send to BCH FldLst = <%s>", pclFldLst) ;
    dbg (DEBUG, "send to BCH DataLst = <%s>", pclDatLst) ;
    if (ilRC == RC_SUCCESS)
		{
     dbg (DEBUG, "SendCedaEvent successfully send to BCH") ; 
		}
		else 
    {
     dbg (DEBUG, "SendCedaEvent NOT successfully send to BCH") ; 
		} 
	 }
	 /* call generate telex */
	 sql_fkt = NEXT ; 
  }
  else
  {
   dbg (DEBUG, "SQLSelectRecord: All records for the time range read") ; 
  }
 } /* if must send telex */

 return (ilRC) ;

} /* handleMVT_Call () */



static int ReadTelexAndSendSMV (char *cpUrno )
{
 int   ilRC = RC_SUCCESS ;
 int   ilLen ; 
 int   ilGetRc ;
 short sql_cursor ;
 short sql_fkt ;
 char  clSqlStatement[2048] ;
 char  clData [8096] ; 
 char  clTlxText [8096] ; 
 char  clDataItem [4096] ; 

  sprintf (clSqlStatement, "SELECT TXT1,TXT2 FROM TLXTAB WHERE URNO=%s", cpUrno) ;  
  sql_cursor = 0 ;
  sql_fkt = START ;
  clData[0] = 0x00;
  ilGetRc = sql_if (sql_fkt, &sql_cursor, clSqlStatement, clData) ;
  close_my_cursor (&sql_cursor) ;
  if (ilGetRc == DB_SUCCESS)
  {
   ilRC = BuildItemBuffer(clData,"",2,",") ;
   /* dbg(TRACE,"TELEX FIELDS <%s>", clData); */
   ilLen = get_real_item (clDataItem, clData, 1);
   strcpy(clTlxText, clDataItem);
   ilLen = get_real_item (clDataItem, clData, 2);
   strcat(clTlxText, clDataItem);
   /* dbg(TRACE,"TELEX TEXT PATCH <%s>", clTlxText); */
   PatchTextServerToClient(clTlxText,FALSE);
   dbg(TRACE,"TELEX TEXT CLEAN <%s>", clTlxText);
   if (igToTlxQue > 0)
   {
     ilRC = SendCedaEvent(igToTlxQue,0,"ufis","",cpUrno,"BKK,TAB,TLXGEN",
                         "TLX", "TLXTAB", cpUrno,"",clTlxText,
                          "", 4, RC_SUCCESS); 
   }
   else
   {
   }
  } /* if */

 return (ilRC) ;

} /* ReadTelexAndSendSMV ( ) */

/****************************************************************************
 *
 * ReadTelexAndSendSITA7
 * Read a normal "old-style" telex (what is the correct name?) and convert
 * it to SITA7 before sending it out.  Need to update TLXTAB TXT1/TXT2 but
 * that might have side effects, what if the user tries to read their
 * sent telexes and wants to resend? So make it configurable.
 *
 * This is a modified copy of ReadTelexAndSend from mvtgen (v.1.5 of 20110114).
 *
 * If the telex appears to be already in SITA7 format, then do not alter, just
 * send it.
 *
 ****************************************************************************
 */

static int ReadTelexAndSendSITA7(char *pcpFields, char *pcpData)
{
    int   ilRC = RC_SUCCESS;
    char pclFunc[] = {"ReadTelexAndSendSITA7"};
    int   ilLen; 
    int   ilGetRc;
    int   ilToPid;
    int   ilChkMVT;
    long  llLen = 0;
    short slSqlCursor;
    short slSqlFunc;
    char  pclUrno[16];
    char  pclSere[16];
    char  pclStat[16];
    char  pclTtyp[16];
    char  pclStartTime[20];
    char  pclEndTime[20];
    char  pclSqlFld[2048];
    char  pclSqlBuf[2048];
    char  pclSqlData[8096]; 
    char  pclTlxText[8096]; 
    char  pclDataItem[4096]; 
    char  pclSelection[512];
    char  pclTlxMail[8096]; 
    char pclCleanLook[4] = "";
    char pclCleanPaste[4] = ""; 
    char  pclNowTime[20] = "";
    char  pclTlxSubject[128] = "";
    char  pclMailFile[128] = "";
    char  pclFullName[128] = "";
    char  pclSmtpScript[1024] = "";
    char  pclSystemCall[1024] = "";
    char  pclQuote[4] = "X";
    char *pclPtr = NULL;
    FILE  *tmpFile ;
    pclQuote[0] = 34;
    
    char  *pclLinePtr, pclLine [1024],pclTlxTextSITA7[8096];
    char  pclTimeStampUtc [32],pclTmp1[256],pclTmp2[256];
    char  pclDestAddrList [1024],pclTlxTtyp[32],pclDestTmp[1024];
    int   ilLine,ilFirstDot,ilFoundPrio,jj,kk,mm;
    int   ilCol,ilPos,ilDoneAddrList,ilAddrEnd;
    int   ilMsgid,ilOrigin,ilText,ilTtypLen;
    
    ilTtypLen = 0;
    /* ilToPid = igTELEX; */ /* never used in mvtgen ... ? */
    ilChkMVT = FALSE;
    dbg(TRACE,"%s FIELDS <%s>",pclFunc,pcpFields);
    dbg(TRACE,"%s DATA   <%s>",pclFunc,pcpData);
    ilGetRc = GetFieldValue(pclUrno, pcpFields, "URNO", pcpData);
    ilGetRc = GetFieldValue(pclSere, pcpFields, "SERE", pcpData);
    ilGetRc = GetFieldValue(pclStat, pcpFields, "STAT", pcpData);
    ilGetRc = GetFieldValue(pclTtyp, pcpFields, "TTYP", pcpData);  

    if (ilGetRc <= 0)
    {
      dbg(TRACE,"%s No TTYP in SITA7 message - will look within telex msg",pclFunc);
      strcpy(pclTtyp,"");
    }
    else
    { 
      dbg(TRACE,"%s TTYP from CEDA msg <%s> will be the SMI",pclFunc,pclTtyp); 
      ilTtypLen = strlen (pclTtyp);
    }

    if ((strcmp(pclSere,"S") == 0) && (strcmp(pclStat,"S") == 0))
    { 
      strcpy(pclSqlFld,"TXT1,TXT2,URNO");  
      sprintf(pclSqlBuf,"WHERE URNO=%s", pclUrno);  
      ilGetRc = HandleSqlSelectRows("TLXTAB", pclSqlBuf, pclSqlFld, pclSqlData, 1);
      dbg(TRACE,"SQL FIELDS <%s>", pclSqlFld);
      dbg(TRACE,"SQL DATA   <%s>", pclSqlData);
      /* TXT1 */
      ilLen = get_real_item (pclDataItem, pclSqlData, 1);
      strcpy(pclTlxText, pclDataItem);
      /* TXT2 */
      ilLen = get_real_item (pclDataItem, pclSqlData, 2);
      strcat(pclTlxText, pclDataItem);

      PatchTextServerToClient(pclTlxText,FALSE);
      sprintf(pclCleanLook, "%c%c", 0x0D, 0x0D);
      sprintf(pclCleanPaste, "%c", 0x0D);
      (void) CleanDoubleBytes(pclTlxText, pclCleanLook, pclCleanPaste);
      dbg(TRACE,"********** TELEX TEXT CLEAN ***********\n<%s>", pclTlxText);

      /* Look for telex command if TTYP was not sent from TelexPool */

      if (strcmp(pclTtyp,"") == 0)
      {
        dbg(TRACE,"%s No TTYP in SITA7 message - will look within telex",pclFunc);
        jj = get_no_of_items(pcgSita7TlxTypes);
        mm = RC_FAIL;
        for (kk = 1; kk <= jj; kk++)
        {
          get_real_item (pclTmp1,pcgSita7TlxTypes,kk);
          mm = FindItemInList (pclTlxText,pclTmp1,'\n',&ilLen,&ilCol,&ilPos);
          if (mm == RC_SUCCESS)
          {
            dbg(TRACE,"%s Found Telex Command <%s> Pos: <%d>",pclFunc,pclTmp1,ilPos);
            strcpy (pclTtyp,pclTmp1);
            ilTtypLen = strlen (pclTtyp);
            break; 
          } 
        }
      }

      if (strcmp(pclTtyp,"") == 0)
      {
        dbg(TRACE,"%s No configured telex commands found in text",pclFunc);
      }

      /* Look for SITA7 tags */
      if ((strstr (pclTlxText,"=HEADER") != '\0') ||
          (strstr (pclTlxText,"=ORIGIN") != '\0') ||
          (strstr (pclTlxText,"=TEXT") != '\0'))
      {
        dbg(TRACE,"%s **** Telex has SITA7 tags - do not convert ***",pclFunc); 
        dbg(TRACE,"%s Either =HEADER, =ORIGIN, or =TEXT found",pclFunc); 
      }
      else
      { 
        memset(pclTlxTextSITA7,'\0',sizeof (pclTlxTextSITA7));
        memset(pclDestAddrList,'\0',sizeof (pclDestAddrList));

        /* Timestamp should be automatically prepended by SITA  at receiver end */
        /* strcpy (pclTlxTextSITA7,"=HEADER\n");
        GetTimeStamp (pclTimeStampUtc,"%Y/%m/%d %H:%M");
        strcat (pclTlxTextSITA7,pclTimeStampUtc); */

        strcat (pclTlxTextSITA7,"=PRIORITY\n");
        /* strcat (pclTlxTextSITA7,pcgSita7Prio); */
        /* strcat (pclTlxTextSITA7,"\nDESTINATION TYPE B\n"); */
        ilFirstDot = 0;	/* flag for when we have already processed originator line */
        ilFoundPrio = 0; /* flag when found priority code */ 
        ilDoneAddrList = 0;
        ilMsgid = 0; ilOrigin = 0; ilText = 0;

        ilLine = 1;
        pclLinePtr = (char *) GetLine (pclTlxText, ilLine);
        while (pclLinePtr != '\0')
        {
          CopyLine(pclLine,pclLinePtr); 
          /* dbg(TRACE,"%s <%d><%.80s>",pclFunc,ilLine,pclLine); */

          ilLine++;
          pclLinePtr = GetLine (pclTlxText, ilLine); 

          /*
            Look for priority code which would be the first occurence of "Q%" 
            followed by an alpha code. Priority is followed by destination
            addresses, possibly on more than one line, until the first line
            that starts with a dot, which could be sender's addr and timestamp.
            TelexPool sends SOH (start of header) ^A just before the priority
            code. Cleaning the telex does not remove it.
          */
          if ((pclLine[0] == '\x01') && (pclLine[1] == 'Q') && (ilFoundPrio == 0))
          {
            if (pclLine[2] != '\0')
            {
              if(isalpha(pclLine[1]))
              {
                ilFoundPrio = 1;
                pclTmp1[0] = 'Q';
                pclTmp1[1] = pclLine[2];
                pclTmp1[2] = '\0';
                strcat (pclTlxTextSITA7,pclTmp1);
                dbg(TRACE,"%s **** Priority <%s>",pclFunc,pclTmp1);
                strcat (pclTlxTextSITA7,"\n=DESTINATION TYPE B\n");
                /*
                  Step backwards to be able to re-read the same line 
                  to get the destinations.
                */
                ilLine--;
                pclLinePtr = GetLine (pclTlxText, ilLine); 
                continue;
              }
            }
          }

          if ((ilDoneAddrList == 0) && (ilFoundPrio != 0) && (ilFirstDot == 0))
          {
             dbg(TRACE,"%s Create Destinations",pclFunc,pclTmp1);
             strcpy (pclDestTmp,&pclLine[4]); /* Same line with the priority code */
             
             while (1)
             { 
               if (pclLinePtr != '\0')
               {
                 CopyLine(pclLine,pclLinePtr);
                 if (pclLine[0] != '.')
                   strcat (pclDestTmp,pclLine); 
                 else
                   break; 
               }
               else
               {
                 dbg(TRACE,"%s ***** UNEXPECTED - no dot+origin found after destinations",pclFunc);
                 dbg(TRACE,"%s Cannot proceed with this telex",pclFunc); 
                 return RC_FAIL; 
               }

               ilLine++;
               pclLinePtr = GetLine (pclTlxText, ilLine); 
             }
             /* Set this flag unconditionally to avoid repeating this block */
             /* Current line is the one starting with dot */
             ilFirstDot = 1;
             
             dbg(TRACE,"%s Dest List Raw <%s>",pclFunc,pclDestTmp);
             /* Replace all spaces between destinations with one CR */
             jj = strlen (pclDestTmp);
             for (kk = 0,mm = 0,ilAddrEnd = 0; kk < jj; kk++)
             {
               if (isalnum(pclDestTmp[kk])) /* tlx addr can be alpha/num */
               {
                 ilAddrEnd = 1;
                 pclDestAddrList[mm] = pclDestTmp[kk];
                 mm++;
               }
               else
               {
                 if (ilAddrEnd == 1)
                 {
                   pclDestAddrList[mm] = '\n';
                   mm++;
                   ilAddrEnd = 0;
                   continue;
                 }
               }
             }
             dbg(TRACE,"%s Dest List Formatted <%s>",pclFunc,pclDestAddrList);
             strcat (pclTlxTextSITA7,pclDestAddrList);
             strcat (pclTlxTextSITA7,"\n=ORIGIN\n");
             ilDoneAddrList = 1;
             continue;
          }

          if ((ilDoneAddrList != 0) && (ilFoundPrio != 0) && (ilOrigin == 0))
          {
            /* pclLine should be at origin+msgid line */
            if (ilFirstDot == 0)
            {
              dbg(TRACE,"%s ***** UNEXPECTED - we are not at dot+origin+msgid ***",pclFunc);
              dbg(TRACE,"%s Cannot proceed with this telex",pclFunc); 
              return RC_FAIL;
            }

            for (jj = 1,kk=0;((pclLine[jj] != '\0') && (pclLine[jj] != ' '));jj++,kk++)
            {
              pclTmp1[kk] = pclLine[jj];
            }
            pclTmp1[kk] = '\0';
            dbg(TRACE,"%s **** Origin <%s>",pclFunc,pclTmp1);
            strcat (pclTlxTextSITA7,pclTmp1);
            strcat (pclTlxTextSITA7,"\n=MSGID\n");
            ilOrigin = 1;

            for (kk=0; (pclLine[jj] != '\0') ;jj++)
            {
              if (isdigit(pclLine[jj]))
              { 
                pclTmp1 [kk] = pclLine[jj];
                kk++;
              }
            }
            pclTmp1[kk] = '\0';

            dbg(TRACE,"%s **** Msgid <%s>",pclFunc,pclTmp1);
            strcat (pclTlxTextSITA7,pclTmp1);
            ilMsgid = 1;


            if ((strcmp (pclTtyp,"") != 0))
            {
              strcat (pclTlxTextSITA7,"\n=SMI\n");
              strcat (pclTlxTextSITA7,pclTtyp);
            }
            strcat (pclTlxTextSITA7,"\n=TEXT\n");
            ilText = 1; 

            continue;
          }


          if ((ilDoneAddrList != 0) && (ilFoundPrio != 0) && (ilOrigin != 0) 
            && (ilText != 0))
          {
            /* Do not write telex command into =TEXT section */
            if (ilTtypLen > 0)
            { 
              if (strncmp(pclLine,pclTtyp,ilTtypLen) == 0)
              {
                dbg(TRACE,"%s strncmp matched, skip this line",pclFunc);
                continue; 
              }
            } 

            strcat (pclTlxTextSITA7,pclLine);
            strcat (pclTlxTextSITA7,"\n");
          } 
        }

        dbg(TRACE,"%s Converted to SITA7 telex:\n<%s>",pclFunc,pclTlxTextSITA7);
      }

      /* Here now we'll strip off the eMail addresses */ 
      llLen = -1;

      /* Disable it for now, it was in mvtgen - DKA - v.1.07 */
      /* pclPtr = strstr(pclTlxText,"<EMAIL_ADDR>"); */ /* original from mvtgen */
      pclPtr = '\0';  /* so email addr extraction not performed */

      if (pclPtr != NULL)
      {
        dbg(TRACE,"TELEX CONTAINS ATTACHED EMAIL ADDRESSES");
        (void) CedaGetKeyItem(pclTlxMail, &llLen, pclTlxText, "<EMAIL_ADDR>", "</EMAIL_ADDR>", TRUE);
        if (pclTlxText[2] == 0x01)
        {
          /* SATS TPF NODE FORMAT */
          /*  TelexText = Chr(13) & Chr(10) & Chr(1) & Trim(txtPreview(idx).Text) 
              & Chr(13) & Chr(10) & Chr(3)
          */
          pclPtr[0] = 0x03;
          pclPtr[1] = 0x00;
        }
        else
        {
          pclPtr[0] = 0x00;
        }
        dbg(TRACE,"TELEX TEXT:\n<%s>",pclTlxText);
        dbg(TRACE,"EMAIL ADDR:\n<%s>",pclTlxMail);
      }
   
      if (igToTlxQueSita7 > 1000)
      { 
        sprintf(pcgTwEnd,"%s,TAB,TLXGEN",pcgHomeHopo);
        ilRC = SendCedaEvent(igToTlxQueSita7,0,mod_name,"",pclUrno,pcgTwEnd,
                      "TLX", "TLXTAB", pclUrno,"",pclTlxTextSITA7, "", 4, RC_SUCCESS); 
        dbg(TRACE,"TELEX PUT ONTO  QUEUE %d",igToTlxQueSita7);
        if (ilRC != RC_SUCCESS)
        {
          dbg(TRACE,"TELEX NOT PUT ONTO FAILING QUEUE %d",igToTlxQueSita7);
          igToTlxQueSita7 = -igToTlxQueSita7;
        }
      }
      else
      {
        dbg(TRACE,"TELEX NOT PUT ONTO INVALID QUEUE <%d>",igToTlxQueSita7);
      }

      if (igSita7CreateFile != 0)
      {
        ilRC = CreateTelexFile (pclTlxTextSITA7, strlen(pclTlxTextSITA7),"SITA7",
          pcgSita7FileNameExt,pcgSita7FileDir);
        if (ilRC != RC_SUCCESS)
          dbg(TRACE,"%s *** FAILED TO CREATE SITA7 FILE",pclFunc);
      }

      sprintf( pclSelection, "WHERE URNO=%s", pclUrno );
      ilRC = SendCedaEvent(1200,0,mod_name,"",pclUrno,pcgTwEnd,
                               "URT", "TLXTAB", pclSelection, "STAT", "Q", "", 3, NETOUT_NO_ACK); 
      dbg(TRACE,"TELEX STATUS UPDATED TO <Q>");

      if (pclPtr != NULL)
      {
        /* Here now we can invoke the Telex Email function           */
        /* Addresses in: pclTlxMail as list separated by CRLF (\r\n) */
        /* Telex Text in: pclTlxText as plain ASCII text             */
        /* Originator in: pcgSenderEmailAddr                         */
        dbg(TRACE,"GONNA SEND THE EMAIL NOW ...");
        if (igEmailIsReadyToUse == TRUE)
        {
          /* ... call the SEND_EMAIL function ... */
          if (pclTlxText[2] == 0x01)
          {
            /* SATS TPF NODE FORMAT */
            /*  TelexText = Chr(13) & Chr(10) & Chr(1) & Trim(txtPreview(idx).Text) & Chr(13) & Chr(10) & Chr(3) */
            pclPtr[0] = 0x00;
            pclPtr = &pclTlxText[2];
            pclPtr++;
          }
          else
          {
            pclPtr = &pclTlxText[2];
          }
          /* JIRA.ISSUE.UFIS-95:
          AutoMVT: Sending Telex using Email
          There is a shell script named: send_telex_email.sh for sending emails.
          Pre-requirement: SMTP setup working correctly on the server system.
          Script requires minimum of 4 parameters as described below
          (does not care about more than 4):
          send_telex_email.sh <reply address> <subject> <addressees> <file with mail body>
          Reply Address: Reply address is in the form of xxxxxx@xxxxxxxx.com.
          Subject: A free text as decided by the user.
          (NOTE: No special characters have been tested in the subject)
          Addressees: List of addresses in the comma separated format.
          File with mail body: The body of the message have to be saved 
          in a plain text file first before sending 
          (Yet to be decided what to do with the file once the message have been sent,
          could be deleted if requested). It is also assumed that the file can be found 
          in the directory from which the script execution takes place; 
          however full path of the file have to be specified if the location of the file 
          is not as specified before.
          */

          /* 1.) Create comma separated address list */
          /* Replace CRLF by Comma */
          ReplaceStringInString(pclTlxMail, "\r\n", ",");
          /* Remove all spaces */
          ReplaceStringInString(pclTlxMail, " ", "");
          ReplaceStringInString(pclTlxMail, ",,", ",");
          while (pclTlxMail[0] == ',')
          {
            pclTlxMail[0] = ' ';
            ReplaceStringInString(pclTlxMail, " ", "");
          }
          ilLen = strlen(pclTlxMail);
          while (ilLen > 0) 
          {
            ilLen--;
            if (pclTlxMail[ilLen] == ',')
            {
              pclTlxMail[ilLen] = '\0';
            }
            else
            {
              ilLen = -1;
            }
          }

          strcpy(pclTlxSubject,"TELEX");

          dbg(TRACE,"EMAIL ORIG: <%s>",pcgSenderEmailAddr);
          dbg(TRACE,"EMAIL ADDR:\n<%s>",pclTlxMail);
          ReplaceStringInString(pclPtr, "\r", "");
          dbg(TRACE,"EMAIL TEXT:\n<%s>",pclPtr);

          /* mksingh82@gmail.com */

          /* 2.) Create eMail Text File */
          /* We'll use a TimeStamp plus Tlxtab.Urno as name */
          GetServerTimeStamp("UTC", 1, 0, pclNowTime);
          sprintf(pclMailFile,"MAIL%sTLX%s",pclNowTime,pclUrno);
          sprintf(pclFullName,"%s/%s",pcgSenderEmailPath,pclMailFile);
          tmpFile = fopen (pclFullName, "w"); 
          if (tmpFile != NULL) 
          {
            fprintf(tmpFile, pclPtr); 
            fflush(tmpFile); 
            fclose(tmpFile);

            /* Note: Subject must be one word (no spaces) */
            sprintf(pclTlxSubject,"MAIL:%s-TLX:%s",pclNowTime,pclUrno);
            /* send_telex_email.sh <reply address> <subject> <addressees> <file with mail body> */
            strcpy(pclSmtpScript,"UfisEmail");

            sprintf(pclSystemCall, "%s %s %s %s %s",
                                    pclSmtpScript,
                                    pcgSenderEmailAddr,
                                    pclTlxSubject,
                                    pclTlxMail,
                                    pclFullName); 

            dbg(TRACE, "SYSTEM CALL <%s>", pclSystemCall); 
            system(pclSystemCall);
          }
          else 
          {
            dbg(TRACE, "CREATE EMAIL FILE ERROR !!!") ; 
            dbg(TRACE, "CANNOT CREATE <%s>", pclFullName) ; 
          }
        }
        else
        {
          dbg(TRACE,"EMAIL FUNCTION NOT ACTIVATED");
        }
      }
      else
      {
        dbg(TRACE,"OOOPS: THE POINTER IS SUDDENLY NULL");
      }
    }
    else
    {
      dbg(TRACE,"COMBINATION OF SERE(%s) AND STAT(%s) NOT TO BE SENT",pclSere,pclStat);
      dbg(TRACE,"Should be SERE='S' and STAT='S' to be sent",pclSere,pclStat);
    }
  return ilRC;
} /* ReadTelexAndSendSITA7 ( ) */




/****************************************************************************
 *
 *
 *
 *
 ****************************************************************************
 */


static void PatchTextServerToClient(char *pcpText, int ipUseOld)
{
  char pclOldChr[] = "-80,-79,178,-77,180,155,156,149,150,-65";
  char pclSvrChr[] = "23,24,25,28,29,40,41";
  char pclCliChr[] = "34,39,44,10,13,40,41,91,93,58";
  char pclVal[4];
  int ili = 0;
  int ilLen = 0;
  int ilItm = 0;
  int ilMax = 0;
  unsigned int ilSvr = 0;
  int ilCli = 0;
  ilLen = strlen(pcpText);
  if (ipUseOld == FALSE)
  {
    ilMax = field_count(pclSvrChr);
  }
  else
  {
    ilMax = field_count(pclOldChr);
  }  
  for (ilItm=1; ilItm<=ilMax; ilItm++)
  {
    if (ipUseOld == FALSE)
    {
      get_real_item (pclVal, pclSvrChr, ilItm);
    }
    else
    {
      get_real_item (pclVal, pclOldChr, ilItm);
    }
    ilSvr = atoi(pclVal);
    get_real_item (pclVal, pclCliChr, ilItm);
    ilCli = atoi(pclVal);
    for (ili=0; ili<ilLen;ili++)
    {
      /* dbg(TRACE,"CHAR<%c> ASC(%d) SVR(%d)",pcpText[ili],pcpText[ili],ilSvr); */
      if (pcpText[ili] == ilSvr)
      {
        pcpText[ili] = ilCli;
      }
    }
  }
  return;
}

/****************************************************************************
 *
 *
 *
 *
 ****************************************************************************
 */


static void PatchTextClientToServer(char *pcpText)
{
  char pclSvrChr[] = "23,24,25,28,29";
  char pclCliChr[] = "34,39,44,10,13";
  char pclVal[4];
  int ili = 0;
  int ilLen = 0;
  int ilItm = 0;
  int ilMax = 0;
  int ilSvr = 0;
  int ilCli = 0;
  ilLen = strlen(pcpText);
  ilMax = field_count(pclSvrChr);
  for (ilItm=1; ilItm<=ilMax; ilItm++)
  {
    get_real_item (pclVal, pclSvrChr, ilItm);
    ilSvr = atoi(pclVal);
    get_real_item (pclVal, pclCliChr, ilItm);
    ilCli = atoi(pclVal);
    for (ili=0; ili<ilLen;ili++)
    {
      if (pcpText[ili] == ilCli)
      {
        pcpText[ili] = ilSvr;
      }
    }
  }
  return;
}
static int generateMTVTelex (char *clFldLst, char *clData)
{
  int   ilRC = RC_SUCCESS ; 
  char  cpWorkFilename[256] ; 
  char  cpFilename[128] ; 
  char  cpPath[128] ; 
  char  cpTmpFilename[128] ;
	FILE  *tmpFile ;
  char  clDataStr[2096] ;
  char  clMonthStr[256] ; 
  char  clHlpStr[256] ;
	char  clFoundField[64] ; 
  int   ilPtr ; 
  char  cpField[64] ; 
  int   isLen ; 
  int   iChgChrPtr ;
  char  clFldStr[20] ;
	char  clChrStr[50] ; 
  char  cClientChr ;  
  char  cSrvChr ;  
  int   ili ; 
  int   ilLen ; 
  char  cpSystemCall[1024] ; 

	dbg (DEBUG, "generateMTVTelex >>") ; 
	ilRC = getNextBRSFilename (cpFilename ) ; /* also used for MVT !!! */ 
  ilRC = getFldDataFromStruct (cpPath, "#FIL", sMVT_ARR) ;  
  sprintf (cpWorkFilename, "%s%s.snd", cpPath, cpFilename) ; 
	sprintf (cpTmpFilename, "%s%s.tmp", cpPath, cpFilename) ; 
  dbg (DEBUG, "createMVTTelex: filename new is (%s)", cpWorkFilename ) ; 

  tmpFile = fopen (cpTmpFilename, "w") ; 
  isLen = strlen (clData) ; 
  iChgChrPtr = 1 ;  /* change all defined chars in created list */

  do 
	{
   cClientChr = 0 ;  
   cSrvChr = 0 ;  
   sprintf (clFldStr, "@C%#02d", iChgChrPtr) ;  
   ilRC = getFldDataFromStruct (clChrStr, clFldStr, sMVT_ARR) ;  
   if (ilRC == RC_SUCCESS)
	 {
    /* get the new chars */
    ilLen = get_real_item (clDataStr, clChrStr, 1) ;
    cClientChr = atoi(clDataStr) ;  
    ilLen = get_real_item (clDataStr, clChrStr, 2) ;
    cSrvChr = atoi(clDataStr) ;  
    if ((cSrvChr > 0) && (cSrvChr < 255) && (cSrvChr != 26) &&
				(cClientChr > 0) && (cClientChr < 256) && (cClientChr != 26))
		{
      ili = 0 ; 
			while (ili < isLen) 
      { 
			 if (clData[ili] == cClientChr) clData[ili] = cSrvChr ; 
       ili++ ; 
      }
		}
	 }
   iChgChrPtr++ ; 
	} while ((cClientChr == 0x00) && (cSrvChr == 0x00)) ; 
  ilRC = RC_SUCCESS ; 

  /* write this to the file and close file */

  if (tmpFile != NULL) 
	{
   dbg (DEBUG, "createMVTTelex FILE writing <%s>",cpTmpFilename) ; 
   /* HEADER INFO for the BRS TELEX */
   fprintf (tmpFile, clData) ; 
	 fflush (tmpFile) ; 
	 fclose (tmpFile) ;
	 sprintf (cpSystemCall, "mv %s %s", cpTmpFilename, cpWorkFilename ) ; 
	 system (cpSystemCall) ;
	 dbg (DEBUG, "system call = <%s>", cpSystemCall) ; 
  }
  else 
	{
   dbg (DEBUG, "createMVTTelex FILE writing ERROR !!!") ; 
   dbg (DEBUG, "Cant create MVT filename <%s>", cpTmpFilename) ; 
	 ilRC = RC_FAIL ; 
  } 

  return ilRC ; } /* generateMTVTelex () */




/*-----------------------------------------------------------------------*/
/* function :  readMVTTab ()                                             */
/*             read all importamnt information (send adress,....)        */
/*             from the MVT tab                                          */
/* pars.: none (gets information over MVTARR (act AFT record) and MV1TAB */
/*                                                                  GHe  */
/*-----------------------------------------------------------------------*/
static int readMVTTab (void) 
{
 int   ilRC = RC_SUCCESS ;
 int   il ;
 int   ilLen ; 
 int   ilGetRc ;
 int   ilMoreRecs ; 
 short sql_cursor ;
 short sql_fkt ;
 char  clSqlStatement[2048] ;
 char  clCondFld[2048] ; 
 char  clCondData[2048] ; 
 char  clSelFlds[2048] ; 
 char  clData [4096] ; 
 char  clDataItem [1024] ; 
 char  clFldItem[1024] ;
 char  clVialStr[2096] ; 
 char  clAlc3Str[1024] ; 
 char  clOrg3Str[1024] ;
 char  clDes3Str[1024] ; 
 char  clAdressPayd[2096] ; 
 char  clDelimiter[2] ; 

 clDelimiter[0] = 0x019 ;    /* delimiter for clAdressPayd field */ 
 clDelimiter[1] = 0x000 ; 

 /* test if the date and time correct, for sending telexs */
 clAdressPayd[0] = 0x00 ; 
 clSelFlds[0]  = 0x00 ;    /* create sql field list */ 
 clCondData[0] = 0x00 ;
 ilRC=createFldAndDataLst(clSelFlds, clCondData, C_SELECT, sMV1_ARR) ;

 /* from AFTTAB we need the information for Dest, OR3, VIAL ACT3 */
 ilRC = getFldDataFromStruct (clAlc3Str, "ALC3", sMVT_ARR) ; 
 ilRC = getFldDataFromStruct (clVialStr, "VIAL", sMVT_ARR) ; 
 ilRC = getFldDataFromStruct (clOrg3Str, "ORG3", sMVT_ARR) ; 
 ilRC = getFldDataFromStruct (clDes3Str, "DES3", sMVT_ARR) ; 

 if (clAlc3Str[0] == '0x00') strcpy (clAlc3Str, "=--") ; /* this never exist */ 
 if (clVialStr[0] == '0x00') strcpy (clVialStr, "===") ; /* this never exist */ 
 if (clOrg3Str[0] == '0x00') strcpy (clOrg3Str, "==-") ; /* -.- */ 
 if (clDes3Str[0] == '0x00') strcpy (clDes3Str, "-=-") ; /* -.- */ 
 sprintf (clSqlStatement,
 "SELECT %s FROM MVTTAB WHERE ALC3 = '%s' AND (APC3 = '%s' OR APC3 = '%s' OR APC3 = '%s')" ,
				clSelFlds, clAlc3Str, clVialStr, clOrg3Str, clDes3Str) ; 
 dbg (DEBUG, "MVT TAB: SQL SELECT STATEMENT = <%s>", clSqlStatement) ;
 sprintf (clData, clCondData) ;        /* prepare data pool */
 sql_cursor = 0 ;
 sql_fkt = START ;
 do 
 {
	ilGetRc = sql_if (sql_fkt, &sql_cursor, clSqlStatement, clData) ;
  if (ilGetRc == DB_SUCCESS)
  {
   ilRC = BuildItemBuffer(clData,clSelFlds,0,",") ;
	 ilMoreRecs++ ; 
   /* telex record */ 
   dbg (DEBUG, "SQLSelectRecord: find rec=<%ld> data = <%s>",
	 						ilMoreRecs, clData) ;
   il = 1 ; 
   do                               /* add to the adress list */ 
	 {
    ilLen = get_real_item (clFldItem, clSelFlds, il) ;
	  if (ilLen > 0)     
	  {
     ilLen = get_real_item (clDataItem, clData, il) ;
	   ilRC = ReplaceFldDataInStruct (clDataItem, clFldItem, &sMV1_ARR) ;
	  }
	  else il = -1 ; 
	  il++ ; 
   } while (il > 0) ; 
   /* add the adress and paid field in the list  */ 
   /* like in form of ...,TEAD,PAYE,TEAD,PAYE,.. */
   if(clAdressPayd[0] != 0x00) strcat (clAdressPayd, clDelimiter) ; 
   ilRC = getFldDataFromStruct (clDataItem, "TEAD", sMV1_ARR) ; 
   strcat (clAdressPayd, clDataItem) ; 
   if(clAdressPayd[0] != 0x00) strcat (clAdressPayd, clDelimiter) ; 
   ilRC = getFldDataFromStruct (clDataItem, "PAYE", sMV1_ARR) ; 
   strcat (clAdressPayd, clDataItem) ; 
  }
  else
  {
   dbg (DEBUG, "SQLSelectRecord: All records for the time range read") ; 
	 ilRC = ReplaceFldDataInStruct (clAdressPayd, "TEAD", &sMV1_ARR) ;
   ilRC = RC_FAIL ; 
	}
  sql_fkt = NEXT ;
 } while (ilGetRc == DB_SUCCESS) ; 

 close_my_cursor (&sql_cursor) ;
 if (clAdressPayd[0] != '0x00') ilRC = RC_SUCCESS ; 

 return (ilRC) ;

} /* readMVTTab () */







/*-----------------------------------------------------------------------*/
/* function :  setTlxTableDat ()                                         */
/*             use the AFT Fields to get the information for the TLXTAB  */
/*             fields, which must be set                                 */
/*  pars.:                                                               */
/*          ST_EVT_DATA_HDL sTLX_ARR : the array with telex datas        */
/*          ST_EVT_DATA_HDL sSND_ARR : the array with act telex generated*/
/*                                     datas from AFTTAB                 */
/*                                                                  GHe  */
/*-----------------------------------------------------------------------*/
static int setTlxTableDat (ST_EVT_DATA_HDL *sTLX_ARR, 
                           ST_EVT_DATA_HDL *sSND_ARR ) 
{
  int      ilRC = RC_SUCCESS ;
  char     clFoundData [4096] ; 
  int      ilNextUrno ; 

	/* check if the field ALC3 exists in TLX */
  if (DoesFldExistInStruct ("ALC3", *sTLX_ARR) == RC_SUCCESS) 
	{
	 clFoundData[0] = 0x00 ; 
   getFldDataFromStruct (clFoundData, "ALC3", *sSND_ARR) ; /* read from AFT */ 
   ReplaceFldDataInStruct (clFoundData, "ALC3", sTLX_ARR) ;  
	}

	/* BEME Bemerkungen (in the moment no information) */
	 clFoundData[0] = 0x00 ; 

	/* check if the field CDAT exists in TLX */
  if (DoesFldExistInStruct ("CDAT", *sTLX_ARR) == RC_SUCCESS) 
	{
   ilRC = GetLocalTimeStamp(clFoundData) ;
   ReplaceFldDataInStruct (clFoundData, "CDAT", sTLX_ARR) ;  
	}

	/* check if the field FKEY exists in TLX */
  if (DoesFldExistInStruct ("FKEY", *sTLX_ARR) == RC_SUCCESS) 
	{
	 clFoundData[0] = 0x00 ; 
   getFldDataFromStruct (clFoundData, "FKEY", *sSND_ARR) ; /* read from AFT */ 
   ReplaceFldDataInStruct (clFoundData, "FKEY", sTLX_ARR) ;  
	}


	/* check if the field FLNS exists in TLX */
  if (DoesFldExistInStruct ("FLNS", *sTLX_ARR) == RC_SUCCESS) 
	{
	 clFoundData[0] = 0x00 ; 
   getFldDataFromStruct (clFoundData, "FLNS", *sSND_ARR) ; /* read from AFT */ 
   ReplaceFldDataInStruct (clFoundData, "FLNS", sTLX_ARR) ;  
	}

	/* check if the field FLNU exists in TLX */
  if (DoesFldExistInStruct ("FLNU", *sTLX_ARR) == RC_SUCCESS) 
	{
	 /* this field gets the URNO from AFT TAB */
	 clFoundData[0] = 0x00 ; 
   getFldDataFromStruct (clFoundData, "URNO", *sSND_ARR) ; /* read from AFT */ 
   ReplaceFldDataInStruct (clFoundData, "FLNU", sTLX_ARR) ;  
	}

	/* check if the field FLTN exists in TLX */
  if (DoesFldExistInStruct ("FLTN", *sTLX_ARR) == RC_SUCCESS) 
	{
	 clFoundData[0] = 0x00 ; 
   getFldDataFromStruct (clFoundData, "FLTN", *sSND_ARR) ; /* read from AFT */ 
   ReplaceFldDataInStruct (clFoundData, "FLTN", sTLX_ARR) ;  
	}

  if (DoesFldExistInStruct ("HOPO", *sTLX_ARR) == RC_SUCCESS) 
	{
   /* read from SYS */ 
   ReplaceFldDataInStruct (pcgHomeHopo, "HOPO", sTLX_ARR) ;  
	}

	/* the fields LSTU, MORE, MSNO, MSTX, PRFL are stored on other pos. */

	/* check if the field SERE exists in TLX */
	/* send (S) and receive (R) and create (C) marker */
  if (DoesFldExistInStruct ("SERE", *sTLX_ARR) == RC_SUCCESS) 
	{
	 strcpy (clFoundData, "C") ;    
   ReplaceFldDataInStruct (clFoundData, "SERE", sTLX_ARR) ;  
	}

	/* check if the field STAT exists in TLX */
	/* send (S) and receive (R) and create (C) marker */
  if (DoesFldExistInStruct ("STAT", *sTLX_ARR) == RC_SUCCESS) 
	{
	 strcpy (clFoundData, "C") ;    
   ReplaceFldDataInStruct (clFoundData, "STAT", sTLX_ARR) ;  
	}


	/* check if the field TIME exists in TLX */
  if (DoesFldExistInStruct ("TIME", *sTLX_ARR) == RC_SUCCESS) 
	{         /* copy from CDAT */
	 clFoundData[0] = 0x00 ; 
   getFldDataFromStruct (clFoundData, "CDAT", *sTLX_ARR) ; /* read from AFT */ 
   ReplaceFldDataInStruct (clFoundData, "TIME", sTLX_ARR) ;  
	}

  /* TKEY ???   */

	/* TTYP  >> AAD/MVT status      */


	/* TXNO ???  */


	/* TXT1 is the complete information of the AFT tab in the */
	/* field sequenz from BRS,MVT,...                         */
  if (DoesFldExistInStruct ("TXT1", *sTLX_ARR) == RC_SUCCESS) 
	{      
	 clFoundData[0] = 0x00 ; 
   getTextField (*sSND_ARR, clFoundData) ; 
   ReplaceFldDataInStruct (clFoundData, "TXT1", sTLX_ARR) ;  
	}
 
	/* check if the field TXT2 exists in TLX */
  if (DoesFldExistInStruct ("TXT2", *sTLX_ARR) == RC_SUCCESS) 
	{
	 clFoundData[0] = 0x00 ; 
   getFldDataFromStruct (clFoundData, "#ADR", *sSND_ARR) ;  
   ReplaceFldDataInStruct (clFoundData, "TXT2", sTLX_ARR) ;  
	}

  /* get URNO */
  ilRC = GetNextUrnoItem (&ilNextUrno) ;     /* get next Urno */
  if (ilRC == RC_SUCCESS)
  {
	 sprintf (clFoundData, "%#010ld", ilNextUrno) ; 
   ReplaceFldDataInStruct (clFoundData, "URNO", sTLX_ARR) ;  
	 ilRC = RC_SUCCESS ;
  }
  else
	{  
	 dbg (DEBUG, "cant read a URNO for TLXTAB") ; 
	 ilRC = RC_FAIL ;
  }

  /* write in USER = USEC and USEU  = user (last change) */
	/* check if the field USEC exists in TLX */
  if (DoesFldExistInStruct ("USEC", *sTLX_ARR) == RC_SUCCESS) 
	{
   ReplaceFldDataInStruct (pcgRecvName, "USEC", sTLX_ARR) ;  
	}
	/* check if the field USEU exists in TLX */
  if (DoesFldExistInStruct ("USEU", *sTLX_ARR) == RC_SUCCESS) 
	{
   ReplaceFldDataInStruct (pcgRecvName, "USEU", sTLX_ARR) ;  
	}

  return (RC_SUCCESS) ; 

} /* setTlxTableDat () */ 




/*-----------------------------------------------------------------------*/
/* function :  getVIALShortTxt ()                                        */
/*             Returns a string with all VIAL HOPOs like xxxYYYsssMMM... */
/*  pars.:                                                               */
/*    char * cpVialStr : coma sep value list                             */
/*    char * cpVialShortStr : the new short string from vial             */
/*                                                                  GHe  */
/*-----------------------------------------------------------------------*/
static int getVialShortTxt (char *cpVialStr, char *cpVialShortStr) 
{
 int  ilRC = RC_SUCCESS ; 
 char ilPtr ; 
 char clVial [10] ; 

 cpVialShortStr[0] = 0x00 ;
 ilPtr = 0 ; 
 while (strlen(&cpVialStr[ilPtr]) > 3)
 {
  memcpy (clVial, &cpVialStr[ilPtr+1], 3) ;
  clVial[3] = 0x00 ;
	strcat (cpVialShortStr, clVial) ; 
	ilPtr += 120 ; 
 }
 return ilRC ; 

} /* getVialShortTxt () */ 






/*-----------------------------------------------------------------------*/
/* function :  createValStr4Tlx()                                        */
/*             creates the data string for Telex                         */
/*  pars.:                                                               */
/*    char         * cpValStr : coma sep value list                      */
/*    ST_EVT_DATA_HDL sSNDARR : includes all information about the AFTTAB*/
/*                                                                  GHe  */
/*-----------------------------------------------------------------------*/
static int createValStr4Tlx (char *cpValStr, ST_EVT_DATA_HDL sTLX_ARR ) 
{
 int  ilRC = RC_SUCCESS ; 
 char clCompleteFldList[1024] ; 
 int  actItemInFldLst ;
 int  ilFldLen ; 
 char clFoundData[4096] ;
 char clFoundField[256] ;

 ilRC = getFldDataFromStruct (clCompleteFldList, "#TLX", sTLX_ARR) ;  

 cpValStr[0] = 0x00 ; 
 actItemInFldLst = 1 ; 
 if (ilRC == RC_SUCCESS) 
 {
  do 
	{
   ilFldLen = get_real_item (clFoundField, clCompleteFldList, actItemInFldLst) ;
   if (ilFldLen > 0)
	 {
    getFldDataFromStruct (clFoundData, clFoundField, sTLX_ARR) ; 
		if (strlen(cpValStr) > 0) strcat (cpValStr, ",") ; 
    strcat (cpValStr, clFoundData) ; 
	 } /* if field exist */
   actItemInFldLst++ ; 
	} while (ilFldLen > 0) ; 
 }
 return ilRC ;

} /* createValStr4Tlx () */ 


/*****************************************************************************/
static int SQLInsertTlxTab (ST_EVT_DATA_HDL sTLX_ARR)
{
 int   ilRC = RC_SUCCESS ;
 int   ilDBRc ;
 short sql_cursor ;
 short sql_fkt ;
 char  cpUsedFlds[1024] ;
 char  pcSqlStatement[1024] ;
 char  cplValStr[1024] ;
 int   ilItems ;

 getFldDataFromStruct (cpUsedFlds, "#TLX", sTLX_ARR) ; 
 ilRC = getItemCnt (cpUsedFlds, &ilItems) ;
 ilRC = prepSqlValueStr (ilItems, cplValStr) ;

 /* put in information for SQL statement */
 sprintf (pcSqlStatement, "INSERT INTO TLXTAB (%s) %s", cpUsedFlds, cplValStr) ;
 sql_cursor = 0 ;
 sql_fkt = START ;

 createValStr4Tlx (cplValStr, sTLX_ARR) ;

 dbg (DEBUG, "INSERT INTO TLXTAB USED FLDS = <%s>", cpUsedFlds) ; 
 dbg (DEBUG, "INSERT INTO TLXTAB VAL STR   = <%s>", cplValStr) ; 
 delton (cplValStr) ;
 ilDBRc = sql_if (sql_fkt, &sql_cursor, pcSqlStatement, cplValStr) ;
 if (ilDBRc != DB_SUCCESS)
 {
  dbg (TRACE, "-------------------------------------------------") ;
  dbg (TRACE, "TlxTab : CANT INSERT RECORD stat.: <%s>", pcSqlStatement) ;
  dbg (TRACE, "-------------------------------------------------") ;
  ilRC = RC_FAIL ;
 }
 else
 { 
  ilRC = RC_SUCCESS ; 
  commit_work() ;
 }
 close_my_cursor (&sql_cursor) ;

 return ilRC ; 
} /* SQLInsertTlxTab() */ 








/*-----------------------------------------------------------------------*/
/* function :  interpretDateInfo ()                                      */
/*             interprets a date/Time-string and writes the information  */
/*             to the DATEINFO struct                                    */
/*             On success the return is RC_SUCCESS else RC_FAIL          */
/*  pars.:                                                               */
/*          charDate       : the interpret date/time as string           */
/*          st_Date        : act. date in DATEINFO struct format         */
/*                                                                  GHe  */
/*-----------------------------------------------------------------------*/
static int interpretDateInfo (char *charDate, DATEINFO *st_Date )
{
 char cHelpDate[64] ;
 int  ilRC = RC_SUCCESS ;
 int  iLen = 0 ;

 dbg (DEBUG, "interpretDateInfo : START") ;

 iLen = strlen (charDate) ;
 if (iLen > 32)
 {
  memcpy (st_Date->cDateStampStr, charDate, 14) ;
  st_Date->cDateStampStr[14] = '\0' ;
 }
 else
 {
  strcpy (st_Date->cDateStampStr, charDate) ;
 }

 dbg (DEBUG, "interpretDateInfo : input str = <%s>", charDate) ;

 st_Date->iStDateInterpret = 0 ;     /* status for date */
 st_Date->iStTimeInterpret = 0 ;     /* status for time */
 st_Date->iYear  = 0 ;               /* reset all dates */
 st_Date->iMonth = 0 ;
 st_Date->iDate  = 0 ;
 st_Date->iHour  = 0 ;
 st_Date->iMins  = 0 ;
 st_Date->iSecs  = 0 ;

 if (iLen >= 8)
 {                                       /* get Date info */
  dbg (DEBUG, "interpretDateInfo : get date info") ;

  strcpy (cHelpDate, charDate) ;
  cHelpDate[4] = '\0' ;
  st_Date->iYear = atoi (cHelpDate) ;    /* year */

  strcpy (cHelpDate, &charDate[4]) ;
  cHelpDate[2] = '\0' ;
  st_Date->iMonth = atoi (cHelpDate) ;   /* month */

  strcpy (cHelpDate, &charDate[6]) ;
  cHelpDate[2] = '\0' ;
  st_Date->iDate = atoi (cHelpDate) ;    /* day */
  st_Date->iStDateInterpret = 1 ;        /* status : 1 -> read a date seq. */

 }
 else
 {                                       /* cant interpret date */
  dbg (DEBUG,"<interpretDateInfo> cant interpret date (len) => <%s>", charDate) ;
  ilRC = RC_FAIL ;
 }

 if (ilRC == RC_SUCCESS)                 /* interpret time string */
 {
  dbg (DEBUG, "interpretDateInfo : get time info") ;

  strcpy (cHelpDate, &charDate[8]) ;     /* start of time */
  if (strlen(cHelpDate) < 6)
  {
   dbg (DEBUG,"<interpretDateInfo> time not interpreted (len) !") ;
   ilRC = RC_FAIL ;
  }
  else
  {
   strcpy (cHelpDate, charDate) ;
   cHelpDate[2] = '\0' ;
   st_Date->iHour = atoi (cHelpDate) ;    /* hours  */

   strcpy (cHelpDate, &charDate[10]) ;
   cHelpDate[2] = '\0' ;
   st_Date->iMins = atoi (cHelpDate) ;    /* minutes */

   strcpy (cHelpDate, &charDate[12]) ;
   cHelpDate[2] = '\0' ;
   st_Date->iSecs = atoi (cHelpDate) ;    /* seconds */
   st_Date->iStTimeInterpret = 1 ;        /* status : 1 -> read a time seq. */
  }
 }

 dbg (DEBUG, "interpretDateInfo : check sequenz") ;

 if ((ilRC == RC_SUCCESS) && (st_Date->iStDateInterpret == 1) ) /*SYNCHK 4DATE*/
 {
  if ((st_Date->iMonth > 12) || (st_Date->iMonth < 1))
  {
   st_Date->iStDateInterpret = 11 ;  /* status:11 -> wrong month in date seq. */
   return (RC_FAIL) ;
  }

  if ((st_Date->iDate < 1) || (st_Date->iDate > 31))
  {
   st_Date->iStDateInterpret = 12 ;  /* status : 12 -> wrong day in date seq. */
   return (RC_FAIL) ;
  }

  switch (st_Date->iMonth)
  {
   case 2 : if ((((st_Date->iYear - 1900) % 4 ) != 0) && (st_Date->iDate > 28) )
            {
             st_Date->iStDateInterpret = 12 ;  /*st:12-> wrong day date seq. */
             return (RC_FAIL) ;
            }
            else
            {
             if (( ((st_Date->iYear - 1900) % 4 ) == 0) && (st_Date->iDate > 29) )
             {
              st_Date->iStDateInterpret = 12 ; /*st: 12->wrong day date seq. */
              return (RC_FAIL) ;
             }
            }
           break ;

   case  4 : /* months with 30 days */
   case  6 :
   case  9 :
   case 11 : if (st_Date->iDate > 30)
             {
              st_Date->iStDateInterpret = 12 ; /*st:12 -> wrong daydate seq. */
              return (RC_FAIL) ;
             }
           break ;

   default : /* months with 31 days ! check is done above */

           break ;

  }  /* switch */

  st_Date->iStTimeInterpret = 2 ;        /* status : 2 -> date seq. ok */
 }    /* if rc == RC_SUCCESS */


 if ((ilRC == RC_SUCCESS)  && (st_Date->iStTimeInterpret == 1) )  /* SYN CHK FOR TIME */
 {
  if ((st_Date->iHour > 23) || (st_Date->iHour <= 0))
  {
   st_Date->iStTimeInterpret = 12 ; /* st: 11 -> wrong hour range date seq. */
   return (RC_FAIL) ;
  }

  if ((st_Date->iMins > 60) || (st_Date->iMins <= 0))
  {
   st_Date->iStTimeInterpret = 13 ; /* st:11 -> wrong minute range date seq. */
   return (RC_FAIL) ;
  }

  if ((st_Date->iSecs > 60) || (st_Date->iSecs <= 0))
  {
   st_Date->iStTimeInterpret = 14 ; /* st:11 -> wrong secs range date seq. */
   return (RC_FAIL) ;
  }

  st_Date->iStTimeInterpret = 1 ;    /* status : 2 -> correct time seq. */

 }

 return ( ilRC ) ;

} /* end of interpretDateInfo () */





/*-----------------------------------------------------------------------*/
/* function :  incDateAtOne()                                            */
/*             adds 1 day, if the date in st_Date less than  st_maxDate  */
/*             if end date reached (no day added!) ipStatus returns 1    */
/*             and function return status is RC_FAIL (retStrDate is "",  */
/*             else the ipStatus is 0, function status is RC_SUCCESS     */
/*             and the string retStrDate contains the act. date in form  */
/*             yyyymmddhhmmss. The time part (hhmmss) is always zero, if */
/*             the act. date not the max date                            */
/*  pars.:                                                               */
/*          retStrDate     : returns the date in str. form after INC     */
/*          st_Date        : act. date previous the INC                  */
/*          st_MaxDate     : the max. useful whised date                 */
/*          ipStatus       : if inc done -> = 0, else  = 1 (no inc)      */
/*          incDateAtOne() : if inc done -> = RC_SUCCESS, else = RC_FAIL */
/*                           (no inc)                               GHe  */
/*-----------------------------------------------------------------------*/
static int incDateAtOne (char *retStrDate, DATEINFO *st_Date, int *ipStatus )
{
 char helpStr[64] ;
 int  ilRC ;
 int  iDateSet ;

 *ipStatus = 0 ;            /* init */
 retStrDate[0] = '\0' ;     /* reset a date */
 ilRC = RC_SUCCESS ;

 dbg (DEBUG, "incDateAtOne: START") ;

 switch (st_Date->iMonth)  /* in the switch, are all critical incs computed */
 {
  case 2 : if ((((st_Date->iYear - 1900) % 4 ) != 0) && (st_Date->iDate == 28) )
           {
            st_Date->iDate = 1 ;               /* NEXT MONTH */
            st_Date->iMonth = 3 ;
            iDateSet = 1 ;
           }
           else
           {
            if((((st_Date->iYear - 1900) % 4 ) == 0) && (st_Date->iDate == 29) )
            {
             st_Date->iDate = 1 ;               /* NEXT MONTH */
             st_Date->iMonth = 3 ;
             iDateSet = 1 ;
            }
           }
          break ;

  case  4 : /* months with 30 days */
  case  6 :
  case  9 :
  case 11 : if (st_Date->iDate == 30)
            {
             st_Date->iDate = 1 ;               /* NEXT MONTH */
             st_Date->iMonth = st_Date->iMonth + 1 ;
             iDateSet = 1 ;
            }
           break ;

  default : /* months with 31 days ! check is done above */
            if (st_Date->iDate == 31)
            {
             st_Date->iDate = 1 ;               /* NEXT MONTH */
             st_Date->iMonth = st_Date->iMonth + 1 ;
             if (st_Date->iMonth > 12)
             {
              st_Date->iMonth = 1 ;
              st_Date->iYear = st_Date->iYear + 1 ;
             }
             iDateSet = 1 ;
            }
           break ;

  }  /* switch */

  if (iDateSet == 0)
  {
   st_Date->iDate = st_Date->iDate + 1 ;        /* next Day  */
  }

 if (ilRC == RC_SUCCESS)
 {
  /* bring struct to string form = yyyymmddhhmmss*/
  sprintf (retStrDate, "%4d%#02d%#02d", st_Date->iYear,
					 st_Date->iMonth, st_Date->iDate) ;

  strcpy (helpStr, "000000") ;    /* no time */
  /* if the last date reached, add the time */
  strcat (retStrDate, helpStr) ;
 } /* else get datestr */

 return ( ilRC ) ;

} /* end of interpretDateInfo () */








/*-----------------------------------------------------------------------*/
/* function :  getDateDiff()                            SHORTNAME : gDD  */
/*             gets the day differenz between a StartDate and a Enddate. */
/*             The function works with the DAEINFO struct                */
/*  pars.:                                                               */
/*          st_beginDate   : act. date previous the INC                  */
/*          st_lastDate    : the max. useful whised date                 */
/*          retDateCnt     : the date differenz (on ERR -> -1)           */
/*          getDateDiff()  : RC_SUCCESS OR RC_FAIL                       */
/*                                                                  GHe  */
/*-----------------------------------------------------------------------*/
static int getDateDiff (DATEINFO *st_beginDate, DATEINFO *st_lastDate,
												int *retDateCnt )
{
 int  ilRC ;
 int  internDayCounter ;
 int  startYear ;
 int  endYear ;
 int  startMonth ;
 int  endMonth ;
 int  startDay ;
 int  endDay ;

 dbg (DEBUG, "gDD: -->") ;

 /*--- init ---*/
 internDayCounter = 0 ;
 *retDateCnt = 0 ;                    /* on error return here -1 */
 ilRC = RC_SUCCESS ;
 startYear  = st_beginDate->iYear ;
 endYear    = st_lastDate->iYear ;
 startMonth = st_beginDate->iMonth ;
 endMonth   = st_lastDate->iMonth ;
 startDay   = st_beginDate->iDate ;
 endDay     = st_lastDate->iDate ;


 /*--- check for right pars ---*/

 /* if date correct ? */
 if (startYear < endYear)
 {
  *retDateCnt = -1 ;
  dbg (DEBUG, "gDD: !!! START_YEAR is smaller as END_YEAR") ;
  return (RC_FAIL) ;
 }
 if ((startMonth > endMonth) && (startYear == endYear ))
 {
  dbg (DEBUG, "gDD: !!! START_YEAR/MONTH is bigger as END_YEAR/MONTH") ;
  *retDateCnt = -1 ;
  return (RC_FAIL) ;
 }
 if ((startDay > endDay) && (startMonth == endMonth) && (startYear == endYear))
 {
  dbg (DEBUG, "gDD: !!! START_YEAR/MONTH/DAY is bigger as END_YEAR/MONTH/DAY") ;
  *retDateCnt = -1 ;
  return (RC_FAIL) ;
 }

 /* interpret the daydiff 2 next month, if startmonth not equal endmonth */
 if (startMonth != endMonth)
 {
  switch (startMonth)
  {
   case 2 : if ( ( (startYear - 1900) % 4 ) != 0)
            {
             internDayCounter = 28 - startDay ;
            }
            else
            {
             internDayCounter = 29 - startDay ;
            }
           break ;

   case  4 : /* months with 30 days */
   case  6 :
   case  9 :
   case 11 : internDayCounter = 30 - startDay ;
           break ;

   default :              /* months with 31 days ! check is done above */
             internDayCounter = 31 - startDay ;
           break ;

  }  /* switch */

  startDay = 1 ;
  startMonth += 1 ;
  if (startMonth == 13)     /* jump into next year */
  {
   startMonth = 1 ;
   startYear += 1 ;
  }
 } /* if add diff. days from 1.st month */


 while (    (startYear < endYear)
        || ((startYear == endYear) && (startMonth < endMonth)) )
 {
  switch (startMonth)  /* in switch, are all critical incs computed */
  {
   case 2 : if ( ( (startYear - 1900) % 4 ) != 0)
            {
             internDayCounter += 28 ;
            }
            else
            {
             internDayCounter += 29 ;
            }
           break ;

   case  4 : /* months with 30 days */
   case  6 :
   case  9 :
   case 11 : internDayCounter += 30 ;
           break ;

   default :              /* months with 31 days ! check is done above */
             internDayCounter += 31 ;
           break ;

  }  /* switch */

  startMonth += 1 ;
  if (startMonth == 13)     /* jump into next year */
  {
   startMonth = 1 ;
   startYear += 1 ;
  }
 } /* while startyear... */

 /* add last diff days */
 internDayCounter += (endDay - startDay) ;

 dbg (DEBUG, "gDD: Daydiffernz between <%#02d.%#02d.%4d> and <%#02d.%#02d.%4d> is : <%d> days",
             st_beginDate->iDate, st_beginDate->iMonth,
						 st_beginDate->iYear, st_lastDate->iDate,
						 st_lastDate->iMonth, st_lastDate->iYear, internDayCounter
     ) ;

 *retDateCnt = internDayCounter ;

 dbg (DEBUG, "gDD: <--") ;
 return ( ilRC ) ;

} /* end of getDateDiff () */




/*==============================================*/
/* FUNCTION get_data_item                       */
/*==============================================*/
static int get_data_item(char *dest, char *src, int nbr)
{
 int rc ; /* logical Length of Item */
 rc = get_real_item(dest, src, nbr) ;
 dbg(DEBUG,"------------------------------------------------------") ;
 dbg(DEBUG,"ITEM (%d) RC=(%d)", nbr, rc) ;
 dbg(DEBUG,"DATA <%s>", src) ;
 dbg(DEBUG,"TEXT <%s>", dest) ;
 dbg(DEBUG,"------------------------------------------------------") ;
 if (rc < 1)
 {
  dest[0] = 0x20 ;
  dest[1] = 0x00 ;
 } /* end if */

 return rc ;
} /* end of get_data_item */

/*==============================================*/
/* FUNCTION timestamp                           */
/*                                              */
/*                                              */
/*==============================================*/
static int timestamp(void)
{
  time_t        _CurTime;
  struct tm     *CurTime;

  cur_time[0] = '\0';
  _CurTime = time(0L);
  CurTime = (struct tm *) localtime(&_CurTime);
  strftime (cur_time, 11, "%" "y%" "m%" "d%" "H%" "M",CurTime);
  return (0) ; 
} /* end of timestamp */



/**********************************************************************/
static int readDataItemFromStr( char *cpFldData, char *cpFldName,
                                char *cpFldLst, char *cpDataArea)
{
 int ilRC ;
 int ilItem ;

 ilRC = RC_SUCCESS ;
 ilItem = 0 ;
 ilRC = getDataFromDbStrs(&ilItem, cpFldName, cpFldLst, cpDataArea, cpFldData) ;
 return (ilRC) ;

} /* function readDataItemFromStr () */



/******************************************************************************/
static int getDataFromDbStrs (int *iItemNo, char *cFld,
                              char *pcgSearchFldLst, /* field list select */
                              char *pcgDataArea,     /* data list select */
                              char *pcgFoundData
                             )
{
 /* if ilItemNo (Input != 0) : search the item (1..n) and return the  */
 /* fieldname and data. If not found return inItemNo = 0              */
 /* if cFld (Input != "") : search the fieldname and return the       */
 /* ilItemNo (1..n) and data. If not found return inItemNo = 0        */

 int  ilRC = RC_SUCCESS ;
 int  ilLen ;
 int  ilFoundItem ;
 int  ilStatus ;

 ilStatus = C_NOTFOUND ;
 strcpy (pcgFoundData, "") ;
 if(*iItemNo == 0)
 {                                   /* search with field */
  ilFoundItem = get_item_no (pcgSearchFldLst, cFld, 5) + 1 ;
  if (ilFoundItem > 0)
  {                     /* found the field -> read data  */
   ilLen = get_real_item ( pcgFoundData, pcgDataArea, ilFoundItem ) ;
   if (ilLen > 0)
   {
    ilStatus = C_FOUND ;
    dbg (DEBUG, "getDataFromDbStrs: found fielddata <%s> from field <%s>",
                pcgFoundData, cFld) ;
   }
   else
   {
    ilStatus = C_FOUND ;
    dbg (DEBUG, "getDataFromDbStrs: fielddata length == 0 for field <%s>", cFld);
   }
  }
  else      /* fld name not found */
  {
   dbg(DEBUG,"getDataFromDbStrs: field <%s> not found", cFld);
  }
 }
 else   /* search with item */
 {                                                        /* get Field */
  ilFoundItem = *iItemNo ;
  ilLen = get_real_item ( cFld, pcgSearchFldLst, ilFoundItem ) ;
  if (ilLen > 0)
  {
   dbg (DEBUG, "getDataFromDbStrs: found field <%s> for item <%d>",
                cFld, ilFoundItem) ;
  }
  else
  {
   dbg(DEBUG,"getDataFromDbStrs: field length == 0 for field <%s>", cFld);
  }

  /* get Data */
  ilLen = get_real_item ( pcgFoundData, pcgDataArea, ilFoundItem ) ;
  if (ilLen > 0)
  {
   ilStatus = C_FOUND ;
   dbg (DEBUG, "getDataFromDbStrs: found fielddata <%s> from field <%s>",
               pcgFoundData, cFld) ;
  }
  else
  {
   ilStatus = C_FOUND ;
   dbg (DEBUG, "getDataFromDbStrs: fielddata length == 0 for field <%s>", cFld);
  }

 } /* search with item */

 if (ilStatus == C_NOTFOUND)
 {
  ilRC = RC_FAIL ;
 }
 else
 {
  ilRC = RC_SUCCESS ;
  *iItemNo = ilFoundItem ;
 }
 return (ilRC) ;
} /* getDataFromDbStrs () */




/******************************************************************************/

/******************************************************************************/
static int getItemCnt (char *cpFldStr, int *ipCnt)
{
 int   ilRC = RC_SUCCESS ;
 int   ilZ ;
 int   ilCnt ;
 int   ilReady ;
 char  clSubStr1[1024] ;
 char  clSubStr[1024] ;
 char  *cpChrPos ;

 ilZ = 0 ;
 ilCnt = 1 ;
 ilReady = RC_SUCCESS ;
 strcpy (clSubStr, cpFldStr) ;
 strcpy (clSubStr1, cpFldStr) ;
 if (strlen (clSubStr) < 3) ilCnt = 0 ;
 do
 {
  strcpy (clSubStr1, clSubStr) ;
  ilZ = 0 ;
  cpChrPos = strstr (clSubStr, ",") ;
  if (cpChrPos != NULL)                 /* more selections */
  {
   ilCnt++ ;
   *cpChrPos = 0x00 ;
   ilZ = strlen(clSubStr)+1 ;
   strcpy (clSubStr, &clSubStr1[ilZ]) ;
  }
  else
  {
   ilReady = RC_FAIL ;
  }
 } while (ilReady != RC_FAIL ) ;
 *ipCnt = ilCnt ;
 return (ilRC) ;

} /* getItemCnt () */





/*****************************************************************************/
/*******************************************************************/
static int prepSqlValueStr (int iItems, char *cpValStr)
{
 int   ilRC = RC_SUCCESS ;
 int   ilGetRc ;
 int   ilZ ;
 int   ilFirst ;
 char  cpDivStr[2] ;
 char  cpVal[20] ;

 strcpy (cpDivStr, ",") ;
 ilFirst = TRUE ;
 strcpy (cpValStr, "VALUES (") ;
 if (iItems == 0 ) return (RC_FAIL) ;
 for (ilZ = 0; ilZ < iItems; ilZ++)
 {
  if (ilFirst == FALSE) strcat (cpValStr, cpDivStr) ;
  ilFirst = FALSE ;
  sprintf (cpVal, ":val%d",ilZ);
  strcat (cpValStr, cpVal) ;
 }
 strcat (cpValStr, ")") ;
 if (igTestFktFlag)
 {
  dbg (DEBUG, "prepSqlValueStr: CREATE PREPARE SQL VAL STR = <%s>", cpValStr) ;
 }
 return (ilRC) ;
} /* prepSqlValueStr() */




/********************************************************************/
static int GetNextUrno(int *pipUrno)
{
  int   ilRC ;
  int   ilOldDbgLvl ;
  char  pclDataArea[IDATA_AREA_SIZE] ;

  if (igTestFktFlag)
  {
   dbg (DEBUG, "GetNextUrno >>") ;
  }
  /* clear buffer */
  memset((void*)pclDataArea, 0x00, IDATA_AREA_SIZE) ;

  /* get the next... */
  ilOldDbgLvl = debug_level ;
  debug_level = TRACE;
  if ((ilRC = GetNextValues(pclDataArea, C_RESERVED_X_URNOS)) != RC_SUCCESS)
  {
   dbg (TRACE,"GetNextValues returns: %d", ilRC) ;
   debug_level = ilOldDbgLvl ;
   ilRC = RC_FAIL ;
  }
  else
  {
   debug_level = ilOldDbgLvl ;
  }

  /* convert to integer */
  *pipUrno = atoi(pclDataArea) ;
  if (igTestFktFlag)
  {
   dbg (DEBUG, "GetNextUrno <<") ;
  }
  return RC_SUCCESS ;

} /* getNextUrno () */




/********************************************************************/
static int GetNextUrnoItem(int *pipUrno)
{
 int  ilRC ;

 if (igTestFktFlag)
 {
  dbg (DEBUG, "GetNextUrnoItem >>") ;
 }
 igReservedUrnoCnt-- ;
 ilRC = RC_SUCCESS ;
 if (igReservedUrnoCnt <= 0)
 {                            /* hole Urno buffer -> get new Urnos */
  ilRC = GetNextUrno (pipUrno) ;
  igReservedUrnoCnt = C_RESERVED_X_URNOS - 1 ;
  igActUrno = *pipUrno ;
 }
 else
 {
  igActUrno++ ;
  *pipUrno = igActUrno ;      /* get next reserved Urno */
 }

 if (igTestFktFlag)
 {
  dbg (DEBUG, "GetNextUrnoItem: Next Urno is : <%#010d>", *pipUrno) ;
  dbg (DEBUG, "GetNextUrnoItem <<") ;
 }
 return (ilRC) ;

} /* getNextUrnoItem () */



/********************************************************************/
static int GetLocalTimeStamp(char *cpTimeStamp)
{
 int  ilRC ;
 static char  pclTime[iTIME_SIZE] ;
 time_t       _llCurTime ;
 struct tm   *_tm ;
 struct tm    prlCurTime ;

 if (igTestFktFlag)
 {
  dbg (DEBUG, "GetLocalTimeStamp >>") ;
 }
 ilRC = RC_SUCCESS ;
 memset((void*)pclTime, 0x00, iTIME_SIZE) ;
 _llCurTime = time(0L) ;
 _tm = (struct tm *) localtime(&_llCurTime) ;
 if (_tm != NULL)
 {
  prlCurTime = *_tm ;
  if (_llCurTime > 0)
  {
   strftime(pclTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S", &prlCurTime) ;
  }
  else
  {
   dbg(TRACE,"GetLocalTimeStamp: _llCurTime is %ld", _llCurTime) ;
  }
 }
 else
 {
  dbg(TRACE,"GetLocalTimeStamp: localtime %ld failed", _llCurTime) ;
  ilRC = RC_FAIL ;
 }
 strcpy (cpTimeStamp, pclTime) ;
 if (igTestFktFlag)
 {
  dbg (DEBUG, "GetLocalTimeStamp <<") ;
 }
 return (ilRC) ;
} /* GetLocalTimeStamp() */


/*-------------------------------------------------------------------*/
/*              FUNTIONS for FIELD STRUCT HANDLING                   */
/*-------------------------------------------------------------------*/

/* RESET_FLD_STRUCT */
/*********************************************************************/
static int ResetFldStruct (ST_EVT_DATA_HDL *sFldArr)
{
 int ilRC ;
 int ilZ ;

 ilRC = RC_SUCCESS ;
 for (ilZ = 0; ilZ < C_MAXFIELDS; ilZ++)
 {
  sFldArr->Status[ilZ] = C_DEAKTIV ;    /* reset field */
 }
 return (ilRC) ;
} /* ResetFldStruct () */


/* INSERT_FLD_STRUCT */
/*******************************************************************/
/* ATTENTION: USE THIS FUNCTION ONLY FOR HANDLE EVENT DATA         */
/*******************************************************************/
static int fillFldStruct (char *cpFldsStr, ST_EVT_DATA_HDL *sFldArr)
{
 char delim[2] ;
 int  fldLen ;
 int  fldPos ;
 long ilZ ;
 int  ilRC ;

 if (igTestFktFlag)
 {
  dbg (DEBUG, "fillFldStruct: work with string <%s>", cpFldsStr) ;
 }

 ilRC = RC_SUCCESS ;
 fldPos = 1 ;
 fldLen = 4 ;
 ilZ = 0 ;
 strcpy (delim, ",") ;                        /* used delimiter */
 while(strlen (&cpFldsStr[fldPos-1]) >= fldLen )
 {
  if (fldPos < fldLen) fldPos-- ;
  strncpy (sFldArr->Fieldname[ilZ], &cpFldsStr[fldPos], fldLen) ;
  sFldArr->Fieldname[ilZ][fldLen] = 0x00 ;
  sFldArr->Status[ilZ] = C_AKTIV ;           /* set field active */
  sFldArr->FieldVal[ilZ][0] = 0x000 ;

  sFldArr->Item[ilZ] = get_item_no(cpFldsStr,sFldArr->Fieldname[ilZ],5)+1;
  sFldArr->StrItem[ilZ] = C_NOTSET ;
  sFldArr->StrBeginPos[ilZ] = (ilZ + (ilZ * fldLen)) - 1 ;
  sFldArr->ValLength[ilZ] = 0 ;
  sFldArr->ReadFromEv[ilZ] = C_ENABLE ;
  sFldArr->DoFldCmp[ilZ] = C_ENABLE ;
  sFldArr->DoUpdate[ilZ] = C_DISABLE ;
  sFldArr->DoSelect[ilZ] = C_DISABLE ;
  sFldArr->DoInsert[ilZ] = C_DISABLE ;
  sFldArr->DoSearch[ilZ] = C_DISABLE ;
  if (igTestFktFlag)
  {
   dbg(DEBUG,"fillFldStruct: interpret Fld=<%s>",sFldArr->Fieldname[ilZ]);
  }
  ilZ++ ;
  fldPos = ilZ + (ilZ * fldLen) ;
 }
 return (ilRC) ;
} /* end of fillFldStruct */





/*******************************************************************/
/* ATTENTION: USE THIS FUNCTION ONLY FOR READ EVENT DATA           */
/*******************************************************************/
static int fillDataInFldStruct (char *cpDataStr, ST_EVT_DATA_HDL *sFldArr)
{
 int ilRC = RC_SUCCESS ;
 int ilZ ;
 int ilLen ;

 ilZ = 0 ;
 while (sFldArr->Status[ilZ] == C_AKTIV)
 {
  if (sFldArr->Item[ilZ] > C_NOTSET)
  {
   if (sFldArr->ReadFromEv[ilZ] == C_ENABLE)
   {
    ilLen=get_real_item(sFldArr->FieldVal[ilZ], cpDataStr, sFldArr->Item[ilZ]);
    /* str_trm_all(sFldArr->FieldVal[ilZ], " ", TRUE) ; */
    if (igTestFktFlag)
    {
     dbg (DEBUG, "fillDataInFieldStruct: Fld=<%s> Data=<%s>",
                 sFldArr->Fieldname[ilZ], sFldArr->FieldVal[ilZ]) ;
    }
    sFldArr->ValLength[ilZ] = strlen (sFldArr->FieldVal[ilZ]) ;
   }
  }
  else
  {
   ilRC = RC_FAIL ;
   dbg (DEBUG, "fillDataInFieldStruct: ERR: sFldArr ITEM > 0 !!! ") ;
  }

  ilZ++ ;
 }
 return (ilRC) ;

} /* fillDataInFldStruct () */





/*******************************************************************/
static int AddFld2Struct (ST_EVT_DATA_HDL *sFldArr, char *cpFldName,
                          char *cpValStr )
{
 long ilZ ;
 int  ilRC = RC_SUCCESS ;

 ilZ = 0 ;

 while (sFldArr->Status[ilZ] == C_AKTIV) ilZ++ ;
 if (ilZ > C_MAXFIELDS)
 {
  dbg (TRACE, "AddFldStruct: BUFFER OVERFLOW") ;
  ilRC = RC_FAIL ;
 }
 if (ilRC == RC_SUCCESS)
 {
  strcpy (sFldArr->Fieldname[ilZ], cpFldName) ;
  sFldArr->Status[ilZ] = C_AKTIV ;           /* set field active */
  strcpy (sFldArr->FieldVal[ilZ], cpValStr) ;
  sFldArr->StrItem[ilZ] = C_NOTSET ;
  sFldArr->StrBeginPos[ilZ] = 0 ;
  sFldArr->Item[ilZ] = 0 ;
  sFldArr->ValLength[ilZ] = strlen (cpValStr) ;
  sFldArr->LastAttach[ilZ] = C_NOTSET ;
  sFldArr->ReadFromEv[ilZ] = C_DISABLE ;
  sFldArr->DoFldCmp[ilZ] = C_DISABLE ;
  sFldArr->DoUpdate[ilZ] = C_DISABLE ;
  sFldArr->DoSelect[ilZ] = C_DISABLE ;
  sFldArr->DoInsert[ilZ] = C_DISABLE ;
  sFldArr->DoSearch[ilZ] = C_DISABLE ;
  if (igTestFktFlag)
  {
   dbg (DEBUG, "AddFld2Struct: INSERT REC=FLD<%s>,STAT=AKTIV,DATA=<%s>",
               sFldArr->Fieldname[ilZ],
               sFldArr->FieldVal[ilZ]) ;
  }
 }
 return (ilRC) ;
} /* end of AddFld2Struct */






/*******************************************************************/
static int InsOrReplFld2Struct (ST_EVT_DATA_HDL *sFldArr, char *cpFldName,
                                char *cpValStr )
{
 int  ilRC = RC_SUCCESS ;

 ilRC = DoesFldExistInStruct (cpFldName, *sFldArr) ;
 if (ilRC == RC_FAIL)
 {
  /* field does not exist */
  ilRC = AddFld2Struct (sFldArr, cpFldName, cpValStr ) ;
 }
 else
 {
  /* field exist -> update the field */
  dbg(DEBUG, "InsOrReplFld2Struct: REPLACE NOT ACTIVE!!!") ;
  /* ilRC = ReplaceFldDataInStruct (cpValStr, cpFldName, sFldArr) ; */
 }
 return (ilRC) ;
} /* end of InsOrReplFld2Struct */






/*******************************************************************/
static int ReplaceFldDataInStruct (char *cpDatStr, char *pclFld,
                                   ST_EVT_DATA_HDL *sFldArr)
{
 int ilRC = RC_SUCCESS ;
 int ilZ ;
 int ilStatus ;

 ilZ = 0 ;
 ilStatus = C_NOTFOUND ;
 while ((sFldArr->Status[ilZ] == C_AKTIV) && (ilStatus == C_NOTFOUND))
 {
  if (strcmp (pclFld, sFldArr->Fieldname[ilZ]) == 0)
  {
   /* found the field */
   strcpy (sFldArr->FieldVal[ilZ], cpDatStr) ;
   ilStatus = C_FOUND ;
   if (igTestFktFlag)
   {
    dbg (DEBUG, "ReplaceFldDataInStruct: Fld=<%s> Data=<%s>", pclFld,
                cpDatStr) ;
   }
  }
  ilZ++ ;
 }
 if (ilStatus == C_NOTFOUND)
 {
  ilRC = RC_FAIL ;
  dbg (TRACE, "ReplaceFldDataInStruct: ERR: Field=<%s> NOT found", pclFld) ;
 }
 return (ilRC) ;

} /* ReplaceDataInStruct () */




/*******************************************************************/
static int getFldDataFromStruct (char *cpRetStr, char *cpFld,
                                 ST_EVT_DATA_HDL sFldArr)
{
 int ilRC = RC_SUCCESS ;
 int ilZ ;
 int ilStatus ;

 ilZ = 0 ;
 ilStatus = C_NOTFOUND ;
 while ((sFldArr.Status[ilZ] == C_AKTIV) && (ilStatus == C_NOTFOUND))
 {
  if (strcmp (cpFld, sFldArr.Fieldname[ilZ]) == 0)
  {
   /* found the field */
   strcpy (cpRetStr, sFldArr.FieldVal[ilZ]);
   ilStatus = C_FOUND ;
   if (igTestFktFlag)
   {
    dbg (DEBUG, "getFldDataFromStruct: DATA=<%s> FLD=<%s>",
                cpRetStr, cpFld) ;
   }
  }
  ilZ++ ;
 }
 if (ilStatus == C_NOTFOUND)
 {
  ilRC = RC_FAIL ;
	cpRetStr[0] = 0x00 ; 
  dbg (TRACE, "getFldDataFromStruct: CANT FIND FIELD: <%s>", cpFld) ;
 }
 return (ilRC) ;

} /* getFldDataFromStruct () */





/*******************************************************************/
static int DoesFldExistInStruct (char *cpFld, ST_EVT_DATA_HDL sFldArr)
{
 int ilRC = RC_SUCCESS ;
 int ilZ ;
 int ilStatus ;

 ilZ = 0 ;
 ilStatus = C_NOTFOUND ;
 while ((sFldArr.Status[ilZ] == C_AKTIV) && (ilStatus == C_NOTFOUND))
 {
  if (strcmp (cpFld, sFldArr.Fieldname[ilZ]) == 0)
  {
   /* found the field */
   if (igTestFktFlag)
   {
    dbg (DEBUG, "DoesFldExistInStruct: Fld=<%s> exist", cpFld) ;
   }
   ilStatus = C_FOUND ;
  }
  ilZ++ ;
 }
 if (ilStatus == C_NOTFOUND)
 {
  ilRC = RC_FAIL ;
  if (igTestFktFlag)
  {
   dbg (DEBUG, "DoesFldExistInStruct: Fld=<%s> does not exist", cpFld) ;
  }
 }
 return (ilRC) ;

} /* DoesFldExistInStruct () */







/*******************************************************************/
static int getFldStruct (char *pclFldsStr, char *pclDataStr, int iAction,
                         ST_EVT_DATA_HDL *sFldArr)
{
 int  ilZ ;
 int  ilRC = RC_SUCCESS ;
 int  ilCanPosAttach ;
 int  ilDone ;

 pclFldsStr[0] = 0x00 ;
 pclDataStr[0] = 0x00 ;
 ilRC = setNextFldPosInStruct (&ilCanPosAttach, iAction, sFldArr) ;
 if (ilRC == RC_SUCCESS)    /* found another field */
 {
  /* get the active attach field */
  ilDone = FALSE ;
  ilZ = 0 ;
  do
  {
   if (sFldArr->LastAttach[ilZ] == C_AKTIV)
   {
    strcpy (pclFldsStr, sFldArr->Fieldname[ilZ]) ;
    strcpy (pclDataStr, sFldArr->FieldVal[ilZ]) ;
    ilDone = TRUE ;
    if (igTestFktFlag)
    {
     dbg (DEBUG, "getFldStruct: READ DATA=<%s>, FLD=<%s>", pclDataStr,
                  pclFldsStr) ;
    }
   } /* if found field & data */
   else
   {
    if (ilZ >= C_MAXFIELDS-1)
    {
     ilDone = TRUE ;
     ilRC = RC_FAIL ;
     if (igTestFktFlag)
     {
      dbg (DEBUG, "getFldStruct: NO FIELDPOS FOUND!") ;
     }
    }
   }
   ilZ++ ;
  } while (ilDone == FALSE) ;
 }

 return (ilRC) ;

} /* getFldStruct() */





/*******************************************************************/
static int setNextFldPosInStruct (int *ipActionResult, int iAction,
                                  ST_EVT_DATA_HDL *sFldArr)
{
 long ilZ ;
 int  ilRC ;
 int  ilDone ;
 int  ilActionCode ;

 ilRC = RC_SUCCESS ;
 ilActionCode = C_NO_ERROR ;
 ilDone = FALSE ;
 switch (iAction)
 {
  case  C_FIRST:
                 if (igTestFktFlag)
                 {
                   dbg (DEBUG, "setNextFldPosInStruct: POS ON BEGINING!") ;
                 }
                 for (ilZ = 0; ilZ < C_MAXFIELDS; ilZ++)
                 {                    /* reset all attach fields */
                  sFldArr->LastAttach[ilZ] = C_DEAKTIV ;
                 }
                 if (sFldArr->Status[0] == C_AKTIV)
                 {
                  sFldArr->LastAttach[0] = C_AKTIV ;
                 }
                 else
                 {
                  ilActionCode = C_NO_ACTIVE_FIELDS ;
                 }
                break ;

  case C_NEXT:
                 if (igTestFktFlag)
                 {
                  dbg (DEBUG, "setNextFldPosInStruct: POS ON NEXT FIELD!") ;
                 }
                 ilZ = 0 ;
                 do
                 {
                  if (sFldArr->Status[ilZ] == C_AKTIV)
                  {
                   if (sFldArr->LastAttach[ilZ] == 1)
                   {
                    if (ilZ >= C_MAXFIELDS)
                    {
                     ilDone = TRUE ;
                     ilActionCode = C_SET_ON_LAST_FIELD ;
                    }
                    else
                    {
                     if (sFldArr->Status[ilZ+1] == C_AKTIV)
                     {
                      sFldArr->LastAttach[ilZ] = C_DEAKTIV ;   /* reset old */
                      sFldArr->LastAttach[ilZ+1] = C_AKTIV ;   /* set new */
                      ilDone = TRUE ;
                     }
                     else
                     {
                      ilDone = TRUE ;
                      ilActionCode = C_SET_ON_LAST_FIELD ;
                     } /* if next fldStr == AKTIV */
                    } /* if not all fields */
                   } /* if last attach == AKTIV */
                  } /* if status == AKTIV */
                  else
                  {
                   ilDone = TRUE ;
                   ilActionCode = C_NO_ACTIVE_FIELDS ;
                  }
                  ilZ++ ;
                 } while (ilDone == FALSE) ;
                break ;

  case C_PREV:
                 if (igTestFktFlag)
                 {
                  dbg (DEBUG, "setNextFldPosInStruct: POS ON PREVIOUS FIELD!") ;
                 }
                 ilZ = 0 ;
                 do
                 {
                  if (sFldArr->Status[ilZ] == C_AKTIV)
                  {
                   if (sFldArr->LastAttach[ilZ] == C_AKTIV)
                   {
                    if (ilZ <= 0)      /* set on the first position */
                    {
                     ilDone = TRUE ;
                     ilActionCode = C_SET_ON_FIRST_FIELD ;
                    }
                    else
                    {
                     sFldArr->LastAttach[ilZ] = C_DEAKTIV ; /* reset old */
                     sFldArr->LastAttach[ilZ-1] = C_AKTIV ; /* set new */
                     ilDone = TRUE ;
                    } /* if not all fields */
                   } /* if last attach == AKTIV */
                  } /* if status == AKTIV */
                  else
                  {
                   ilDone = TRUE ;
                   ilActionCode = C_NO_ACTIVE_FIELDS ;
                  }
                  ilZ++ ;
                 } while (ilDone == FALSE) ;
                break ;

  case C_LAST:
                 if (igTestFktFlag)
                 {
                  dbg (DEBUG, "setNextFldPosInStruct: POS ON LAST FIELD!") ;
                 }
                 for (ilZ = 0; ilZ < C_MAXFIELDS; ilZ++)
                 {                         /* reset all attach fields */
                  sFldArr->LastAttach[ilZ] = C_DEAKTIV ;
                 }
                 ilZ = 0 ;      /* set on start */
                 while (sFldArr->Status[ilZ] == C_AKTIV)
                 {
                  ilZ++ ;
                 }  /* search the last field */
                 if (ilZ == 0)
                 {
                  ilActionCode = C_NO_ACTIVE_FIELDS ;
                 }
                 else
                 {
                  sFldArr->LastAttach[ilZ-1] = C_AKTIV ;
                 }
                break ;

  default :

                 dbg (DEBUG, "setNextFldPosInStruct: UNKNOWN Cmd = <%d>", iAction) ;
                 ilActionCode = C_UNKNOWN_COMMAND ;
                break ;

  } /* switch */

  if (ilActionCode != C_NO_ERROR)
  {
   if (igTestFktFlag)
   {
    dbg (DEBUG, "setNextFldPosInStruct: CONDITION REACHED -> CANT POS!") ;
   }
   ilRC = RC_FAIL ;
  }
  *ipActionResult = ilActionCode ;

  return ( ilRC) ;

} /* function setNextFldPosInStruct() */






/*******************************************************************/
static int setChkFldinStruct4Fld (char *pclFld, int iDoWhat,
                                  ST_EVT_DATA_HDL *sFldArr)
{
 int ilRC = RC_SUCCESS ;
 int ilZ ;
 int ilStatus ;

 ilZ = 0 ;
 ilStatus = C_NOTFOUND ;
 while ((sFldArr->Status[ilZ] == C_AKTIV) && (ilStatus == C_NOTFOUND))
 {
  if (strcmp (pclFld, sFldArr->Fieldname[ilZ]) == 0)
  {
   /* found the field */
   sFldArr->DoFldCmp[ilZ] = iDoWhat ;
   ilStatus = C_FOUND ;
   if (igTestFktFlag)
   {
    dbg (DEBUG, "setChkFldinStruct4Fld: Set fldCmp to=<%d> in FLD=<%s>",
                iDoWhat, pclFld) ;
   }
  }
  ilZ++ ;
 }
 if (ilStatus == C_NOTFOUND)
 {
  ilRC = RC_FAIL ;
  dbg (TRACE, "setChkFldinStruct4Fld: CANT FIND FIELD: <%s>", pclFld) ;
 }

 return (ilRC) ;
} /* setChkFldinStruct4Fld () */







/*******************************************************************/
/* if pclFld == "" -> do set Flag iDoWhat for all fields           */
/* if iKindOfFlg == 0 -> Update all Falgs with iDoWhat             */
/*******************************************************************/
static int setStatusInStruct (char *pclFld, int iDoWhat, int iKindOfFlg,
                              ST_EVT_DATA_HDL *sFldArr)
{
 int  ilRC = RC_SUCCESS ;
 int  ilZ ;
 int  ilStatus ;
 char cActionKind[30] ;

 ilZ = 0 ;
 ilStatus = C_NOTFOUND ;
 if (pclFld[0] == 0x00)
 {
  while (sFldArr->Status[ilZ] == C_AKTIV)
  {
   switch (iKindOfFlg)
   {
    case 0        : sFldArr->DoFldCmp[ilZ] = iDoWhat ;
                    sFldArr->DoUpdate[ilZ] = iDoWhat ;
                    sFldArr->DoSelect[ilZ] = iDoWhat ;
                    sFldArr->DoInsert[ilZ] = iDoWhat ;
                    sFldArr->DoSearch[ilZ] = iDoWhat ;
                    strcpy (cActionKind, "ALL") ;
                  break ;

    case C_SEARCH : sFldArr->DoSearch[ilZ] = iDoWhat ;
                    strcpy (cActionKind, "SEARCH") ;
                  break ;

    case C_INSERT : sFldArr->DoInsert[ilZ] = iDoWhat ;
                    strcpy (cActionKind, "INSERT") ;
                  break ;

    case C_UPDATE : sFldArr->DoUpdate[ilZ] = iDoWhat ;
                    strcpy (cActionKind, "UPDATE") ;
                  break ;

    case C_SELECT : sFldArr->DoSelect[ilZ] = iDoWhat ;
                    strcpy (cActionKind, "SELECT") ;
                  break ;

    case C_FLDCMP : sFldArr->DoFldCmp[ilZ] = iDoWhat ;
                    strcpy (cActionKind, "FLDCMP") ;
                  break ;

    default       : dbg (TRACE, "SetStatusInStruct: UNKNOWN KIND OF FIELD") ;
                    strcpy (cActionKind, "UNKNOWN") ;
                  break ;

   } /* switch() */
   ilZ++ ;
   ilStatus = C_FOUND ;
  } /* while */

  if (igTestFktFlag)
  {
   switch (iDoWhat)
   {
    case C_ENABLE :

          dbg(DEBUG, "setStatusInStruct: ENAB <%s> STAT FOR ALL FLDS",
                     cActionKind   ) ;
         break ;

    case C_DISABLE :

          dbg(DEBUG,"setStatusInStruct: DISAB <%s> STAT FOR ALL FLDS",
                     cActionKind   ) ;
         break ;

    default :

          dbg(DEBUG,"setStatusInStruct: SET <%s> STAT = <%d> FOR ALL FLDS",
                     cActionKind, iDoWhat) ;
         break ;
   } /* switch */
  } /* if TestFlag */
 } /* if all */
 else
 {           /* search for the field */
  while ((sFldArr->Status[ilZ] == C_AKTIV) && (ilStatus == C_NOTFOUND))
  {
   if (strcmp (pclFld, sFldArr->Fieldname[ilZ]) == 0)
   {
    /* found the field */
    switch (iKindOfFlg)
    {
     case 0        : sFldArr->DoFldCmp[ilZ] = iDoWhat ;
                     sFldArr->DoUpdate[ilZ] = iDoWhat ;
                     sFldArr->DoSelect[ilZ] = iDoWhat ;
                     sFldArr->DoInsert[ilZ] = iDoWhat ;
                     sFldArr->DoSearch[ilZ] = iDoWhat ;
                     strcpy (cActionKind, "ALL") ;
                   break ;

     case C_SEARCH : sFldArr->DoSearch[ilZ] = iDoWhat ;
                     strcpy (cActionKind, "SEARCH") ;
                   break ;

     case C_INSERT : sFldArr->DoInsert[ilZ] = iDoWhat ;
                     strcpy (cActionKind, "INSERT") ;
                   break ;

     case C_UPDATE : sFldArr->DoUpdate[ilZ] = iDoWhat ;
                     strcpy (cActionKind, "UPDATE") ;
                   break ;

     case C_SELECT : sFldArr->DoSelect[ilZ] = iDoWhat ;
                     strcpy (cActionKind, "SELECT") ;
                   break ;

     case C_FLDCMP : sFldArr->DoFldCmp[ilZ] = iDoWhat ;
                     strcpy (cActionKind, "FLDCMP") ;
                   break ;

     default       : dbg (TRACE, "SetStatusInStruct: KIND OF FIELD") ;
                     strcpy (cActionKind, "UNKNOWN") ;
                   break ;

    } /* switch() */
    ilStatus = C_FOUND ;

    if (igTestFktFlag)
    {
     switch (iDoWhat)
     {
      case C_ENABLE :

           dbg (DEBUG, "setStatusInStruct: ENAB <%s> STAT > FLD = <%s>",
                       cActionKind, pclFld);
          break ;

      case C_DISABLE :

           dbg (DEBUG, "setStatusInStruct: DISAB <%s> STAT > FLD = <%s>",
                       cActionKind, pclFld);
          break ;

       default :

           dbg (DEBUG, "setStatusInStruct: SET <%s> STAT=<%d> > FLD = <%s>",
                       cActionKind, iDoWhat, pclFld) ;
          break ;
     } /* switch */
    } /* if testFlag */
   } /* if strcmp == fld */
   ilZ++ ;
  } /* while field not found */
 } /* else */

 if (ilStatus == C_NOTFOUND)
 {
  ilRC = RC_FAIL ;
  dbg (TRACE, "setStatusInStruct: CANT FIND FIELD: <%s>", pclFld) ;
 }
 return (ilRC) ;
} /* setStatusInStruct () */




/*******************************************************************/
static int createFldAndDataLst (char *cpFldLst, char *cpDataLst,
                                int iKindOf, ST_EVT_DATA_HDL sFldArr)
{
 int ilRC = RC_SUCCESS ;
 int ilZ ;
 int ilFirstItem ;
 int iAddFldData ;
 char cpDivStr[2] ;

 strcpy (cpDivStr, ",") ;
 ilZ = 0 ;
 ilFirstItem = TRUE ;
 while (sFldArr.Status[ilZ] == C_AKTIV)
 {
  iAddFldData = FALSE ;
  switch (iKindOf)
  {
   case C_SEARCH : if (sFldArr.DoSearch[ilZ] == C_ENABLE)
                   {
                    iAddFldData = TRUE ;
                   }
                  break ;

   case C_INSERT : if (sFldArr.DoInsert[ilZ] == C_ENABLE)
                   {
                    iAddFldData = TRUE ;
                   }
                  break ;

   case C_SELECT : if (sFldArr.DoSelect[ilZ] == C_ENABLE)
                   {
                    iAddFldData = TRUE ;
                   }
                  break ;

   case C_UPDATE : if (sFldArr.DoUpdate[ilZ] == C_ENABLE)
                   {
                    iAddFldData = TRUE ;
                   }
                  break ;

   case C_FLDCMP : if (sFldArr.DoFldCmp[ilZ] == C_ENABLE)
                   {
                    iAddFldData = TRUE ;
                   }
                  break ;

    } /* switch */

    if (iAddFldData == TRUE)
    {
     /* add item to field and data */
     if (ilFirstItem == FALSE)
     {
      strcat (cpFldLst, cpDivStr) ;
      strcat (cpDataLst, cpDivStr) ;
     }
     ilFirstItem = FALSE ;
     strcat (cpFldLst, sFldArr.Fieldname[ilZ]) ;
     strcat (cpDataLst, sFldArr.FieldVal[ilZ]) ;
    }
    ilZ++ ;
   }  /* while not all flds */

   if (igTestFktFlag)
   {
    dbg (DEBUG, "createFldAndDataLst: generated Fieldlist = <%s>", cpFldLst) ;
    dbg (DEBUG, "createFldAndDataLst: generated Datalist = <%s>", cpDataLst) ;
   }
   return (ilRC) ;

} /* createFldAndDataLst () */







/*****************************************************************************/
/* send a event message to other processes, that this process gets an        */
/* update of active database changes                                         */
/*****************************************************************************/
static int TriggerAction(char *pcpTableName, char *pcpCmdRec, 
												 char *pcpFieldName)
{
 int           ilRC          = RC_SUCCESS ;    /* Return code */
 EVENT        *prlEvent      = NULL ;
 BC_HEAD      *prlBchead     = NULL ;
 CMDBLK       *prlCmdblk     = NULL ;
 ACTIONConfig *prlAction     = NULL ;
 char         *pclSelection  = NULL ;
 char         *pclFields     = NULL ;
 char         *pclData       = NULL ;
 long          llSize        = 0 ;
 int           ilAnswerQueue = 0 ;
 char          clQueueName[16] ;
 int           ili = 0 ; 

 /* get memory for event */
 llSize= sizeof(EVENT)+sizeof(BC_HEAD)+sizeof(CMDBLK)+sizeof(ACTIONConfig)
											+strlen(pcpTableName)+strlen(pcpFieldName)+
											+strlen(pcpCmdRec) + 666; 
 prgOutEvent = (EVENT *)malloc((size_t)llSize) ; 
 if(prgOutEvent == NULL)
 {
  dbg(TRACE,"TriggerAction: malloc out event <%d> bytes failed", llSize) ;
  return (RC_FAIL) ;
 } /* end of if */
 memset ((void*)prgOutEvent, 0x00, llSize) ;

 /* create trigger queue */
 sprintf (&clQueueName[0],"%s2", mod_name) ;
 dbg (DEBUG, "Used Trigger queue name is <%s>", clQueueName) ; 
 ilRC = GetDynamicQueue(&ilAnswerQueue, &clQueueName[0]) ;
 if (ilRC != RC_SUCCESS)
 {
  dbg (TRACE, "TriggerAction: GetDynamicQueue failed <%d>", ilRC) ;
  return (ilRC) ; 
 }
 else
 {
  dbg(DEBUG,"TriggerAction: got <%d> for <%s>",ilAnswerQueue, &clQueueName[0]) ;
 } /* end of if */

 prlBchead     = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ; 
 prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data) ;
 pclSelection  = prlCmdblk->data ;
 pclFields     = pclSelection + strlen (pclSelection) + 1 ;
 pclData       = pclFields + strlen (pclFields) + 1 ;
 strcpy (pclData, "DYN") ;   /* STA */
 prlAction     = (ACTIONConfig *) (pclData + strlen(pclData) + 1) ;

 prgOutEvent->type        = USR_EVENT ;
 prgOutEvent->command     = EVENT_DATA ;
 prgOutEvent->originator  = ilAnswerQueue ;
 prgOutEvent->retry_count = 0 ;
 prgOutEvent->data_offset = sizeof(EVENT) ;
 prgOutEvent->data_length = llSize-sizeof(EVENT) ;

 prlAction->iEmptyFieldHandling = iDELETE_ALL_BLANKS ;
 prlAction->iIgnoreEmptyFields = 0 ;
 /* from Action return defs. */
 strcpy(prlAction->pcSndCmd, "") ;       /* pcpCmdRec) ; */
 sprintf(prlAction->pcSectionName,"%s_%s", mod_name, pcpTableName) ;
 strcpy(prlAction->pcTableName, pcpTableName) ;
 sprintf(prlAction->pcFields, "%s%s", pcpFieldName, ",URNO") ; /* VIAL,FLDA */
 strcpy(prlAction->pcSectionCommands, "URT,IRT") ;
 prlAction->iModID = mod_id ;

 /* send to action handler send first a delete, for action knows this already */
 if(ilRC == RC_SUCCESS)
 {
  prlAction->iADFlag = iDELETE_SECTION ;
  ilRC=
	 que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llSize, (char *)prgOutEvent);
  if (ilRC != RC_SUCCESS)
  {
   dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRC) ;
  }
  else
  {
   ilRC = WaitAndCheckQueue(10, ilAnswerQueue,&prgItem) ;
   if(ilRC != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRC) ;
   }
  } /* end of if */
 } /* end of if */


 if(ilRC == RC_SUCCESS)
 {
  prlAction->iADFlag = iADD_SECTION ;

  ilRC = que(QUE_PUT,7400,ilAnswerQueue,PRIORITY_3,llSize,(char *)prgOutEvent);
  if(ilRC != RC_SUCCESS)
  {
   dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRC) ;
  }
  else
  {
   ilRC = WaitAndCheckQueue(10,ilAnswerQueue,&prgItem) ;
   if(ilRC != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRC) ;
   }
   else
   {
    prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
    prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
    pclSelection = (char *)    prlCmdblk->data ;
    pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
    pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;

    if (strcmp (pclData, "SUCCESS") != 0)
    {
     ilRC = RC_FAIL ;
    }
    else
    {
     dbg(DEBUG,"TriggerAction: add dynamic action config OK") ;
    }/* end of if */
   }/* end of if */
  }/* end of if */
 }/* end of if */

 /* delete the queue */
 ilRC = que(QUE_DELETE,ilAnswerQueue,ilAnswerQueue,0,0,0) ;
 if(ilRC != RC_SUCCESS)
 {
  dbg(TRACE,"que QUE_DELETE <%d> failed <%d>", ilAnswerQueue, ilRC) ;
 }
 else
 {
  dbg(DEBUG,"TriAct: queue <%d> <%s> deleted",ilAnswerQueue,&clQueueName[0]) ;
 }/* end of if */

 return (ilRC) ;
} /* end of TriggerAction */





/*****************************************************************************/
/*****************************************************************************/
static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem)
{
 int ilRC          = RC_SUCCESS ;      /* Return code */
 int ilWaitCounter = 0 ;

 do
 {
  sleep(1) ;
  ilWaitCounter++ ;    

  ilRC = CheckQueue (ipModId, prpItem) ;
  if (ilRC != RC_SUCCESS)
  {
   if (ilRC != QUE_E_NOMSG)
   {
    dbg(TRACE, "WaitAndCheckQueue: CheckQueue <%d> failed <%d>", mod_id, ilRC) ;
   } /* end of if */
  } /* end of if */
 } while ((ilRC == QUE_E_NOMSG) && (ilWaitCounter < ipTimeout)) ;

 if (ilWaitCounter >= ipTimeout)
 {
  dbg (TRACE, "WaitAndCheckQueue: timeout reached <%d>", ipTimeout) ;
  ilRC = RC_FAIL ;
 } /* end of if */

 return (ilRC) ;

} /* end of WaitAndCheckQueue */




/*****************************************************************************/
static int CheckQueue(int ipModId, ITEM **prpItem)
{
 int     ilRC       = RC_SUCCESS;                      /* Return code */
 int     ilItemSize = 0 ;
 EVENT  *prlEvent   = NULL ;

 ilItemSize = I_SIZE ;

 ilRC =que(QUE_GETBIGNW,ipModId,ipModId,PRIORITY_3,ilItemSize,(char *) prpItem);
 if (ilRC == RC_SUCCESS)
 {
  prlEvent = (EVENT*) ((*prpItem)->text) ;

  switch ( prlEvent->command )
  {
   case    HSB_STANDBY     : dbg(TRACE, "CheckQueue: HSB_STANDBY") ;
                             HandleQueues() ;
                             ctrl_sta = prlEvent->command ;
                            break ;

   case    HSB_COMING_UP   : dbg(TRACE, "CheckQueue: HSB_COMING_UP") ;
                             ctrl_sta = prlEvent->command ;
                            break ;

   case    HSB_ACTIVE      : dbg(TRACE, "CheckQueue: HSB_ACTIVE") ;
                             ctrl_sta = prlEvent->command ;
                            break ;

   case    HSB_ACT_TO_SBY  : dbg(TRACE, "CheckQueue: HSB_ACT_TO_SBY") ;
                             HandleQueues() ;
                             ctrl_sta = prlEvent->command ;
                            break ;

   case    HSB_DOWN        : dbg (TRACE, "CheckQueue: HSB_DOWN") ;
                             ctrl_sta = prlEvent->command ;
                             ilRC = que ( QUE_ACK, 0, ipModId, 0, 0, NULL) ;
                             if ( ilRC != RC_SUCCESS )
                             {        /* handle que_ack error */
                              HandleQueErr(ilRC) ;
                             } /* fi */
                             Terminate() ;
                            break ;

   case    HSB_STANDALONE  : dbg (TRACE, "CheckQueue: HSB_STANDALONE") ;
                             ctrl_sta = prlEvent->command ;
                            break ;

   case    SHUTDOWN        : dbg (TRACE, "CheckQueue: SHUTDOWN") ;   /* Ack */
                             ilRC = que (QUE_ACK, 0, ipModId, 0, 0, NULL) ;
                             if ( ilRC != RC_SUCCESS )
                             {   /* handle que_ack error */
                              HandleQueErr(ilRC) ;
                             } /* fi */
                             Terminate() ;
                            break ;

   case    RESET           : ilRC = Reset() ;
                            break ;

   case    EVENT_DATA      : break ;

   case    TRACE_ON        : dbg_handle_debug(prlEvent->command) ;
                            break ;

   case    TRACE_OFF       : dbg_handle_debug(prlEvent->command) ;
                            break ;

   default                 : dbg (TRACE, "CheckQueue: unknown event") ;
                            break ;

  } /* end switch */

  /* Acknowledge the item */
  ilRC = que (QUE_ACK, 0, ipModId, 0, 0, NULL) ;
  if ( ilRC != RC_SUCCESS )
  {                                    /* handle que_ack error */
   HandleQueErr (ilRC) ;
  } /* fi */
 }
 else
 {
  if (ilRC != QUE_E_NOMSG)
  {
   dbg (TRACE, "CheckQueue: que (QUE_GETBIG,%d,%d,...) failed <%d>", ipModId, ipModId, ilRC) ;
   HandleQueErr (ilRC) ;
  } /* end of if */
 } /* end of if */

 return (ilRC) ;

} /* end of CheckQueue */




/*-----------------------------------------------------------------------*/
/* function :  getTextField ()                                           */
/*             creates a complete information string of the AFT Tab      */
/*             The field VIAL is compressed only AIRPORT shortnames      */
/*             in sequenz                                                */
/*  pars.:                                                               */
/*    ST_EVT_DATA_HDL sSNDARR : includes all information about the AFTTAB*/
/*                                                                  GHe  */
/*-----------------------------------------------------------------------*/
static int getTextField (ST_EVT_DATA_HDL sSND_ARR, char *ipTxtStr ) 
{
 int  ilRC = RC_SUCCESS ; 
 char clCompleteFldList[2096] ; 
 int  actItemInFldLst ;
 int  ilFldLen ; 
 char clFoundData[2096] ;
 char clFoundField[256] ;
 int  ilPtr ; 
 char clHelpStr[255] ; 
 char clVial [10] ; 
 char cpDeli[2] ;
 char cpDeliFldDiv[2] ;

 cpDeli[0] = 0x019 ;            /* char 25 */
 cpDeli[1] = 0x000 ;   
 cpDeliFldDiv[0] = 0x01E ;      /* char 30 */
 cpDeliFldDiv[1] = 0x000 ;   
 ilRC = getFldDataFromStruct (clCompleteFldList, "#TLX", sSND_ARR) ;  

 strcpy (ipTxtStr, clCompleteFldList) ; 
 ilFldLen = strlen (ipTxtStr) ; 
 ilPtr = 0 ; 
 while (ilPtr < ilFldLen) 
 {
	if (ipTxtStr[ilPtr] == ',') ipTxtStr[ilPtr] = cpDeli[0] ; 
	ilPtr++ ;
 }
 ipTxtStr[ilPtr] = cpDeliFldDiv[0] ; /* divide fields from data */ 
 ipTxtStr[ilPtr+1] = 0x00 ; 

 actItemInFldLst = 1 ; 
 dbg (DEBUG, "Fieldlist for TXT field is = <%s>", ipTxtStr) ; 
 if (ilRC == RC_SUCCESS) 
 {
  do 
	{
   ilFldLen = get_real_item (clFoundField, clCompleteFldList, actItemInFldLst) ;
   dbg (DEBUG, "TXT1 CREAETE : ACT ITEM IS <%ld>", actItemInFldLst) ; 
   dbg (DEBUG, "TXT1 CREATE  : ACT FLD IS <%s>", clFoundField) ; 
	 if (ilFldLen > 0)
	 {
    getFldDataFromStruct (clFoundData, clFoundField, sSND_ARR) ; 
    dbg (DEBUG, "TXT1 CREAETE : FOUND DATA <%s>", clFoundData) ; 
		if (actItemInFldLst > 1) strcat (ipTxtStr, cpDeli) ; 
    if (strcmp(clFoundField, "VIAL") == 0) 
		{
     getVialShortTxt (clFoundData, clHelpStr) ;  
		 strcpy (clFoundData, clHelpStr) ;
		 dbg (DEBUG, "VIAL SHORT TEXT IS <%s>", clFoundData) ; 
    } 
		ilRC = RC_SUCCESS ; 
    strcat (ipTxtStr, clFoundData) ; 
	 } /* if field exist */
   actItemInFldLst++ ; 
	} while (ilFldLen > 0) ; 
 }
 if (strlen (ipTxtStr) > 2048) 
 {
  /* string is to long for database field */
  dbg (DEBUG, "STRING TO LONG : <%s>", ipTxtStr) ;  
  dbg (DEBUG, "LOST THE FOLLOWING INFORMATION: : <%s>", &ipTxtStr[1999]) ; 
	ipTxtStr[1999] = 0x00 ; 
 } 

 return ilRC ;

} /* getTextField () */



/*----------------------------------------------------------------------*/
/* Functions to generate BRS telexe                                     */
/*----------------------------------------------------------------------*/


static int getBRSTlxTextData (void) 
{
  int   ilRC = RC_SUCCESS ; 
	char  cpCRLF[3] ; 
	char  cpErrStr[255] ; 
  char  clDataStr[2096] ;
  char  clDayStr[256] ; 
  char  clMonthStr[256] ; 
  char  clHlpStr[256] ;
	char  clMonth[64] ;
	char  clFoundField[64] ;
	char  clCompleteStr[64] ; 
  char  clHelpStr[1024] ; 

	cpCRLF[0] = 0x0D ;
	cpCRLF[1] = 0x0A ;
	cpCRLF[2] = 0x0 ;
  cpErrStr[0] = 0x00 ; 

	 /* set flight no  like <OA 944> FLD AFT = FLNO */
   clDataStr[0] = 0x00 ; 
	 ilRC = getFldDataFromStruct (clDataStr, "FLNO", sBRS_ARR) ; 
   if (ilRC == RC_SUCCESS) 
	 {
		sprintf (clCompleteStr, ".F/%s%s", clDataStr, cpCRLF) ; 
    ReplaceFldDataInStruct (clCompleteStr, "FLNO", &sBRS_PRN) ;  
	 }
   else 
	 {
		if (strlen (cpErrStr) > 0) strcat (cpErrStr, ",") ; 
		strcat (cpErrStr, "FLNO") ; 
   }

   /* set STOD/TIFD date/time */
   /* we need the day and the first 3 letters from the month */
	 ilRC = getFldDataFromStruct (clMonthStr, "#MON", sBRS_ARR) ; 
	 ilRC = getFldDataFromStruct (clDataStr, "STOD", sBRS_ARR) ;
   strcpy (clDayStr, &clDataStr[6]) ; 	 
   clDayStr[2] = 0x00 ; 
   /* get month for this (use this as item for the month field) */
   strcpy ( clMonth, &clDataStr[4] ) ; 
   clMonth[2] = 0x00 ; 
   get_real_item (clFoundField, clMonthStr, atoi(clMonth)) ;
   sprintf (clHelpStr, "%s%s", clDayStr, clFoundField) ;
	 dbg (DEBUG, "Created STOD is <%s>", clHelpStr) ; 
	 if (strlen (clHelpStr) == 5) 
   {
  	sprintf (clCompleteStr, ".S/%s%s", clHelpStr, cpCRLF) ; 
    ReplaceFldDataInStruct (clCompleteStr, "STOD", &sBRS_PRN) ;  
   }
	 else 
	 {
		if (strlen (cpErrStr) > 0) strcat (cpErrStr, ",") ; 
		strcat (cpErrStr, "STOD") ; 
   }
   
	 /* set FTYP or TISD ./C */
   clDataStr[0] = 0x00 ; 
	 ilRC = getFldDataFromStruct (clDataStr, "FTYP", sBRS_ARR) ; 
   if (clDataStr[0] != 'X') 
	 {
    clDataStr[0] = 0x00 ; 
	  ilRC = getFldDataFromStruct (clDataStr, "TISD", sBRS_ARR) ; 
   }
	 if (ilRC == RC_SUCCESS)  
   {
  	sprintf (clCompleteStr, ".C/%s%s", clDataStr, cpCRLF) ; 
    ReplaceFldDataInStruct (clCompleteStr, "FTYP", &sBRS_PRN) ;  
   }
   else 
	 {
	  if (strlen (cpErrStr) > 0) strcat (cpErrStr, ",") ; 
	  strcat (cpErrStr, "FTYP//TISD") ; 
   }

   /* get TIFD additional time is used use moth str from STOA  */
	 ilRC = getFldDataFromStruct (clDataStr, "TIFD", sBRS_ARR) ;
   strcpy ( clDayStr, &clDataStr[6]) ; 	 
   clDayStr[2] = 0x00 ; 
   /* get month for this (use this as item for the month field) */
   strcpy ( clMonth, &clDataStr[4] ) ; 
   clMonth[2] = 0x00 ; 
   get_real_item (clFoundField, clMonthStr, atoi(clMonth)) ;
   sprintf (clHelpStr, "%s%s", clDayStr, clFoundField) ; 
	 if (strlen (clHelpStr) == 5)  
   {
  	sprintf (clCompleteStr, ".D/%s%s", clHelpStr, cpCRLF) ; 
    ReplaceFldDataInStruct (clCompleteStr, "TIFD", &sBRS_PRN) ;  
   }
   else 
	 {
		if (strlen (cpErrStr) > 0) strcat (cpErrStr, ",") ; 
		strcat (cpErrStr, "TIFD") ; 
   }
   
   strcpy ( clHelpStr, &clDataStr[8]) ; 	 
   clHelpStr[4] = 0x00 ; 
	 if (strlen (clHelpStr) == 4)  
   {
  	sprintf (clCompleteStr, ".T/%s%s", clHelpStr, cpCRLF) ; 
    ReplaceFldDataInStruct (clCompleteStr, "TIFT", &sBRS_PRN) ;  
   }
   else 
	 {
		if (strlen (cpErrStr) > 0) strcat (cpErrStr, ",") ; 
		strcat (cpErrStr, "TIFT") ; 
   }

	 /* set help -> field ADID .H/  */
   clDataStr[0] = 0x00 ; 
	 ilRC = getFldDataFromStruct (clDataStr, "ADID", sBRS_ARR) ; 
   if (ilRC == RC_SUCCESS)  
   {
  	sprintf (clCompleteStr, ".F/%s%s", clDataStr, cpCRLF) ; 
    ReplaceFldDataInStruct (clCompleteStr, "ADID", &sBRS_PRN) ;  
   }
   else 
	 {
		if (strlen (cpErrStr) > 0) strcat (cpErrStr, ",") ; 
		strcat (cpErrStr, "ADID") ; 
   }

	 /* VIAL in short list */
   clDataStr[0] = 0x00 ; 
	 ilRC = getFldDataFromStruct (clDataStr, "VIAL", sBRS_ARR) ; 
   if (ilRC == RC_SUCCESS)
	 { 
    getVialShortTxt (clDataStr, clHelpStr) ;
  	sprintf (clCompleteStr, ".R/%s%s", clHelpStr, cpCRLF) ; 
    ReplaceFldDataInStruct (clCompleteStr, "VIAL", &sBRS_PRN) ;  
   } 
   else 
	 {
		if (strlen (cpErrStr) > 0) strcat (cpErrStr, ",") ; 
		strcat (cpErrStr, "ADID") ; 
   }

   ilRC = RC_SUCCESS ; 
	 if (strlen (cpErrStr) > 0)
	 {
		ilRC = RC_FAIL ;
		dbg (DEBUG, "------------------------------------------------") ; 
		dbg (DEBUG, "TELEX CREATOR --- BRS --------------------------") ;  
		dbg (DEBUG, "ERRS DETECTED in FIELDS: -----------------------") ; 
		dbg (DEBUG, "FIELDS = <%s> ", cpErrStr ) ;  
		dbg (DEBUG, "------------------------------------------------") ; 
   }

  return ilRC ; 
} /* end getBRSTlxTextData () */ 









/*-----------------------------------------------------------------------*/
/* function :  getBRSHeaderInfo ()                                       */
/*             interprets from the config file the header information    */
/*             into the struct sBRS_PRN                                  */
/*  pars.:                                                               */
/*          ST_EVT_DATA_HDL sBRS_ARR : the struct with telex datas       */
/*          ST_EVT_DATA_HDL sBRS_PRN : the struct with header info       */
/*                                                                  GHe  */
/*-----------------------------------------------------------------------*/
static int getBRSHeaderInfo (ST_EVT_DATA_HDL sBRS_ARR, 
                             ST_EVT_DATA_HDL *sBRS_PRN ) 
{
  int      ilRC = RC_SUCCESS ;
  char     clFoundData [2096] ; 
  char     cpCRLF[3];
  char     cpFieldBRS[64] ;
  char     cpFieldPrnNo[64] ;
	char     cpHelpStr[128] ; 
	int      ilEnd ; 
	int      ilActPrnPos ;  
	int      ilActFieldPos ;  

	cpCRLF[0] = 0x0D ;
	cpCRLF[1] = 0x0A ;
	cpCRLF[2] = 0x0 ;
  ilActPrnPos = 1 ; 
	ilActFieldPos = 1 ;
	ilEnd = 0 ; 
	do 
	{
   sprintf (cpFieldBRS, "@%#03d", ilActFieldPos) ;  
   ilRC = getFldDataFromStruct (clFoundData, cpFieldBRS, sBRS_ARR) ;  
   ilActFieldPos++ ;
   if ((strcmp(clFoundData, "BLOCK_BEGIN") == 0) || 
			 (strcmp (clFoundData, "BLOCK_END") == 0 ) || (ilRC != RC_SUCCESS) ) 
	 {
    if (strcmp(clFoundData, "BLOCK_END") == 0) ilEnd = 1 ;
    if (ilRC != RC_SUCCESS) ilEnd = 1 ;
   }
	 else 
   {       /* write this line to the header */
    sprintf (cpFieldPrnNo, "H%#03d", ilActPrnPos) ;
		sprintf (cpHelpStr, "%s%s", clFoundData, cpCRLF) ; 
    ReplaceFldDataInStruct (cpHelpStr, cpFieldPrnNo, sBRS_PRN) ;  
    ilActPrnPos++ ;
	 } 
   if (ilActFieldPos == 15) ilEnd = 1 ; 
	} while (ilEnd == 0) ;

	ilRC = RC_SUCCESS ; 
  return (ilRC) ;

}  /* getBRSHeaderInfo() */






static int getNextBRSFilename (char *cpNxtFilename )
{
 int      ilRC = RC_SUCCESS ; 
 short    sql_cursor ;
 short    sql_fkt ;
 char     clSqlStatement[2048] ;
 char     clData [4096] ; 
 long     int ilActNo ; 
 long     int ilMaxNo ;
 long     int ilMinNo ; 
 char     clDataItem[128] ; 
 int      ilItems ; 
 char     clUsedFlds[1024] ;
 char     clValStr[4096] ;
 int      ilDBRc ; 

 /* select from the database the last number */
 /* NUMTAB : ACNU = actualy number */
 /*          FLAG = ???            */
 /*          HOP  = Flughafen      */
 /*          KEYS = Actionsname    */
 /*          MAXN = max number     */
 /*          MINN = min number     */

 dbg (DEBUG, "getNextBRSFilename >>") ; 
 if (ilRC == RC_SUCCESS)    /* then sending telex */
 {
  dbg (DEBUG, "getNextBRSFilename: SQL SELECT PART NUMTAB (TELEXE)") ; 
  sprintf (clSqlStatement,
					 "SELECT ACNU,MAXN,MINN FROM NUMTAB WHERE KEYS = 'TELEXE'" ) ; 
  sql_cursor = 0 ;
  sql_fkt = START ;
  ilDBRc = sql_if (sql_fkt, &sql_cursor, clSqlStatement, clData) ;
  close_my_cursor (&sql_cursor) ;     /* close select cursor */
  if (ilDBRc != DB_SUCCESS)
  {
   /* SPECIAL CASE USE TELEXE AT 1.ST TIME */
   /* -> insert telexe into the database   */
   dbg(DEBUG, "getNextBRSFilename: SQL INSERT PART NUMTAB (TELEXE)") ; 
	 dbg(DEBUG, "getNextBRSFileName: create new record KEYS=TELEXE for NumTab");
   strcpy (clUsedFlds, "ACNU,FLAG,HOPO,KEYS,MAXN,MINN") ;
   ilRC = getItemCnt (clUsedFlds, &ilItems) ;
   ilRC = prepSqlValueStr (ilItems, clValStr) ;

   /* put in information for SQL statement */
   sprintf (clSqlStatement, "INSERT INTO NUMTAB (%s) %s",clUsedFlds, clValStr);
   sql_cursor = 0 ;
   sql_fkt = START ;

   /* flds : ACNU,FLAG,HOPO,KEYS,MAXN,MINN   */
   /* len  :  10   20    3   10   10   10    */
   sprintf (clValStr, "%#010ld,%s,%s,%s,%#010ld,%#010ld",
                      1,                       /* start number is 1 */
  										"",                      /* 20 Blanks */
                      pcgHomeHopo,             /*  3 chars like ATH */ 
                      "TELEXE",                /* 10 chars Home Airport */ 
                      999999999,               /* 10 chars max number */
 										  1                        /* 10 chars min number */
				   ) ;
   dbg (DEBUG, "SELECT SQL VALUE STRING IS: <%s>", clValStr) ;
   delton (clValStr) ;
   ilDBRc = sql_if (sql_fkt, &sql_cursor, clSqlStatement, clValStr) ;
   if (ilDBRc = DB_SUCCESS) 
   {
    dbg (TRACE, "-------------------------------------------------") ;
    dbg (TRACE, "SQLInsertRecord : CANT INSERT RECORD stat.: <%s>",
                                                     clSqlStatement) ;
    dbg (TRACE, "-------------------------------------------------") ;
    ilRC = RC_FAIL ;
    if (gl_updateTest == 1) ilRC = RC_SUCCESS ;
   }
   else
   {
    commit_work() ;
    ilRC = RC_SUCCESS ;
		strcpy (cpNxtFilename, "0000000001") ; 
   }
   close_my_cursor (&sql_cursor) ; /* close insert cursor */
  } /* special case set data at first time */
  else      /* found a telex number */
  {
   dbg(DEBUG, "getNextBRSFilename: SQL UPDATE PART NUMTAB (TELEXE)") ; 
   ilRC = BuildItemBuffer(clData, "ACNU,MAXN,MINN",0,",") ;
   get_real_item (clDataItem, clData, 1) ;
   ilActNo = atol (clDataItem) ; 
   get_real_item (clDataItem, clData, 2) ;
   ilMaxNo = atol (clDataItem) ; 
   get_real_item (clDataItem, clData, 3) ;
	 ilMinNo = atol (clDataItem) ; 
   ilActNo++ ; 
	 if (ilActNo > ilMaxNo) 
	 {
	  ilActNo = ilMinNo ; 
   } ;
   sprintf (cpNxtFilename, "%#010ld", ilActNo) ;  
   dbg(DEBUG,"read act=<%ld>, max=<%ld>, min=<%ld>",ilActNo,ilMaxNo,ilMinNo) ; 
	 /* update the new actNo into the database */
   strcpy (clUsedFlds, "ACNU") ;

   /* put in information for SQL statement */
   sprintf (clSqlStatement,
					 "UPDATE NUMTAB SET ACNU =:VFLD1 WHERE KEYS='TELEXE'" ) ;
   sql_cursor = 0 ;
   sql_fkt = START ;

   /* flds : ACNU,FLAG,HOPO,KEYS,MAXN,MINN   */
   /* len  :  10   20    3   10   10   10    */
   sprintf (clValStr, "%#010ld", ilActNo) ; 
   dbg (DEBUG, "UPDATE SQL VALUE STRING IS: <%s>", clValStr) ;
   delton (clValStr) ;
   ilDBRc = sql_if (sql_fkt, &sql_cursor, clSqlStatement, clValStr) ;
   if (ilDBRc != DB_SUCCESS) 
   {
    dbg (TRACE, "-------------------------------------------------") ;
    dbg (TRACE, "SQL UPDATE Record : CANT UPDATE RECORD stat.: <%s>",
                                                    clSqlStatement) ;
    dbg (TRACE, "-------------------------------------------------") ;
    ilRC = RC_FAIL ;
   }
   else
   {
    commit_work() ;
    ilRC = RC_SUCCESS ;
	 } 
   close_my_cursor (&sql_cursor) ;    /* close update cursor */
  } /* else if must send telex */
 } /* time check done */
 return (ilRC) ;

} /* end getNextBRSFilename()  */





static int createBRSTelex (void) 
{
  int   ilRC = RC_SUCCESS ; 
  char  cpWorkFilename[256] ; 
  char  cpFilename[128] ; 
  char  cpPath[128] ; 
	char  cpTmpFilename[128] ; 
	FILE  *tmpFile ;
  char  clDataStr[2096] ;
  char  clMonthStr[256] ; 
  char  clHlpStr[256] ;
	char  clFoundField[64] ; 
  int   ilPtr ; 
  char  cpField[64] ; 
  char  cpSystemCall[1024] ; 

  dbg (DEBUG, "createBRSTelex >>") ; 
	
	ilRC = getNextBRSFilename (cpFilename ) ; 
  ilRC = getFldDataFromStruct (cpPath, "#FIL", sBRS_ARR) ;  
  sprintf (cpWorkFilename, "%s%s.snd", cpPath, cpFilename) ; 
	sprintf (cpTmpFilename, "%s%s.tmp", cpPath, cpFilename) ; 
  dbg (DEBUG, "createBRSTelex: filename new is (%s)", cpWorkFilename ) ; 

  ilRC = getBRSTlxTextData () ;   /* now the BRS info in sBRS_PRN */ 
  if (ilRC == RC_SUCCESS) 
	{
   tmpFile = fopen (cpTmpFilename, "w") ; 
	 if (tmpFile != NULL) 
	 {
    dbg (DEBUG, "createBRSTelex FILE writing <%s>",cpTmpFilename) ; 
    /* HEADER INFO for the BRS TELEX */
    ilPtr = 1 ;
	  do 
	  {
	   sprintf (cpField, "H%#03d", ilPtr) ;  
	   ilRC = getFldDataFromStruct (clDataStr, cpField, sBRS_PRN) ; 
     if (clDataStr[0] != 0x00) fprintf (tmpFile, clDataStr) ; 
     ilPtr++ ; 
    } while ((ilPtr < 15) && (clDataStr[0] != 0x00)) ; 

		ilRC = getFldDataFromStruct (clDataStr, "BEGI", sBRS_PRN) ; 
    fprintf (tmpFile, clDataStr) ; 
	  ilRC = getFldDataFromStruct (clDataStr, "FLNO", sBRS_PRN) ; 
    fprintf (tmpFile, clDataStr) ; 
	  ilRC = getFldDataFromStruct (clDataStr, "STOD", sBRS_PRN) ; 
    fprintf (tmpFile, clDataStr) ; 
	  ilRC = getFldDataFromStruct (clDataStr, "FTYP", sBRS_PRN) ; 
    fprintf (tmpFile, clDataStr) ; 
	  ilRC = getFldDataFromStruct (clDataStr, "TIFD", sBRS_PRN) ; 
    fprintf (tmpFile, clDataStr) ; 
	  ilRC = getFldDataFromStruct (clDataStr, "TIFT", sBRS_PRN) ; 
    fprintf (tmpFile, clDataStr) ; 
	  ilRC = getFldDataFromStruct (clDataStr, "ADID", sBRS_PRN) ; 
    fprintf (tmpFile, clDataStr) ; 
	  ilRC = getFldDataFromStruct (clDataStr, "VIAL", sBRS_PRN) ; 
    fprintf (tmpFile, clDataStr) ; 

	  fflush (tmpFile) ; 
	  fclose (tmpFile) ;
    setTlxTableDat (&sTLX_ARR, &sBRS_ARR ) ; 
	  /* TODO set telex status to S */
	  SQLInsertTlxTab (sTLX_ARR) ; 
	  sprintf (cpSystemCall, "mv %s %s", cpTmpFilename, cpWorkFilename) ; 
	  system (cpSystemCall) ; 
    dbg (DEBUG, "system call = <%s>", cpSystemCall) ; 
   }
   else 
	 {
    dbg (DEBUG, "createBRSTelex FILE writing ERROR !!!") ; 
    dbg (DEBUG, "Cant create BRS filename <%s>", cpTmpFilename) ; 
	  ilRC = RC_FAIL ; 
   } 
  } /* if RC_SUCCESS */ 

  return ilRC ; 
} /* end createBRSTelex */



static int handleBRS_Call (char *cpDateFrom, char *cpDateTo )
{
 
 int   ilRC = RC_SUCCESS ;
 int   il ;
 int   ilLen ; 
 int   ilGetRc ;
 int   ilMoreRecs ; 
 short sql_cursor ;
 short sql_fkt ;
 char  clSqlStatement[2048] ;
 char  clCondFld[2048] ; 
 char  clCondData[2048] ; 
 char  clSelFlds[2048] ; 
 char  clData [4096] ; 
 char  clDataItem [1024] ; 
 char  clFldItem[1024] ; 

 /* test if the date and time correct, for sending telexs */
 clSelFlds[0]  = 0x00 ;    /* create sql field list */ 
 clCondData[0] = 0x00 ; 
 ilRC=createFldAndDataLst(clSelFlds, clCondData, C_SELECT, sBRS_ARR) ;
 sprintf (clSqlStatement,
 				"SELECT %s FROM AFTTAB WHERE TIFD BETWEEN '%s' AND '%s'",
				clSelFlds, cpDateFrom, cpDateTo) ;  
 dbg (DEBUG, "handlBRSCall: SQL SELECT STATEMENT = <%s>", clSqlStatement) ;
 sprintf (clData, clCondData) ;        /* prepare data pool */
 sql_cursor = 0 ;
 sql_fkt = START ;
 ilMoreRecs = 0 ;  
 do 
 {
  ilGetRc = sql_if (sql_fkt, &sql_cursor, clSqlStatement, clData) ;
  if (ilGetRc == DB_SUCCESS)
  {
	 ilMoreRecs++ ; 
   ilRC = BuildItemBuffer(clData,clSelFlds,0,",") ;
   ilRC = RC_SUCCESS ;
   /* telex record */ 
   dbg (DEBUG, "SQLSelectRecord: find rec=<%ld> data = <%s>",
								ilMoreRecs, clData) ;
   il = 1 ; 
   do 
	 {
    ilLen = get_real_item (clFldItem, clSelFlds, il) ;
	  if (ilLen > 0)     
	  {
     ilLen = get_real_item (clDataItem, clData, il) ;
	   ilRC = ReplaceFldDataInStruct (clDataItem, clFldItem, &sBRS_ARR) ;
	  }
	  else il = -1 ; 
	  il++ ; 
   } while (il > 0) ; 
   
   ilRC = createBRSTelex () ; 
	 /* call generate telex */
  }  /* if found next record DB_SUCCESS */
  else
  {
   dbg (DEBUG, "handleBRSCall: All records for the time range read") ; 
   ilMoreRecs = 0 ;   /* all telex records read */
  }
  sql_fkt = NEXT ; 
 } while (ilMoreRecs != 0) ; 

 close_my_cursor (&sql_cursor) ;
 return (ilRC) ;

} /* handleBRSCall () */








static int startWhileTimeReached (ST_EVT_DATA_HDL *stAct, char *cpDateFrom,
					  											char *cpDateTo)
{
 int       ilRC = RC_SUCCESS ; 
 char      clDateFrom [64] ; 
 char      clDateTo   [64] ;
 char      clActionDate[64] ; 
 DATEINFO  stlDateFrom ; 
 DATEINFO  stlDateTo; 
 int       ilDayOffset ; 
 char      cpHStr[64] ;
 long int  ilActionDay ; 
 long int  ilActionTime ; 
 long int  ilToDay ; 
 long int  ilToTime ; 
 int       ilTimeInt ;  
 int       ilStatus ;  


 ilRC = getFldDataFromStruct (cpHStr, "#DAT", *stAct) ; 
 ilDayOffset = atoi(cpHStr) ; 
 ilRC = getFldDataFromStruct (cpHStr, "#TIM", *stAct) ; 
 ilRC = getFldDataFromStruct (clDateFrom, "#SDT", *stAct) ;  
 ilRC = interpretDateInfo (clDateFrom, &stlDateFrom) ;
 ilRC = GetLocalTimeStamp(clDateTo) ;
 ilRC = interpretDateInfo (clDateTo, &stlDateTo) ;
 while (ilDayOffset > 0) 
 {
  ilRC = incDateAtOne (clDateFrom, &stlDateFrom, &ilStatus ) ;
  ilDayOffset-- ; 
 }
 if (cpHStr[0] != 0x00) clDateFrom[8] = cpHStr[0] ; 
 if (cpHStr[1] != 0x00) clDateFrom[9] = cpHStr[1] ;
 if (cpHStr[2] != 0x00) 
 {
  if (cpHStr[3] != 0x00) clDateFrom[10] = cpHStr[3] ; 
  if (cpHStr[4] != 0x00) clDateFrom[11] = cpHStr[4] ; 
 } 
 
 /* get new offset date */
 /* compute the dates as long */ 
 ilActionTime = atol (&clDateFrom[8]) ; 
 clDateFrom[8] = 0x00 ; 
 ilActionDay = atol (clDateFrom)  ; 
 ilToTime = atol (&clDateTo[8]) ; 
 clDateTo[8] = 0x00 ; 
 ilToDay = atol (clDateTo)  ; 

 ilTimeInt = 0 ; 
 if (ilActionDay > ilToDay) ilTimeInt = 1 ; 
 if ((ilActionDay == ilToDay) && (ilActionTime >= ilToTime)) ilTimeInt = 1 ;  
 
 
 dbg (DEBUG, "The actualy time with offset is : <%8ld> <%6ld>", ilActionDay,
																												  			ilActionTime) ;
 dbg (DEBUG, "The server time is <%8ld> <%6ld>", ilToDay, ilToTime) ;
 if (ilTimeInt == 1)
 {
	dbg (DEBUG, "The Telex sending is started!") ;
 }
 else
 {
	dbg (DEBUG, "No Telex sending is started!") ;
 }

 if (ilTimeInt == 1) return RC_SUCCESS ;  /* start telex sending */
 return RC_FAIL ;  /* time not reached */
} /* startWhileTimeReached () */



static void ForwardTelexToFdihdl(char *pcpSelKey, char *pcpData)
{
  SendCedaEvent (8450,0,pcgDestName,pcgRecvName,pcgTwStart,pcgTwEnd,
                 "FDI", "TLXTAB", pcpSelKey,"",pcpData, "", 4, RC_SUCCESS) ; 
  SendCedaEvent (que_out,0,"","","0","BKK,TAB,TLXGEN",
                 "TLX", "TLXTAB", "TELEXE","","", "", 4, RC_SUCCESS) ; 
  return;
} 


/******************************************************************************/
/******************************************************************************/
static void TestReplace(void)
{
  STR_DESC rlMyString;
  char *pclText = "XXXX[OOOO]YYYY";
  char *pclPatt = "[OOOO]";
  char *pclValu1 = "";
  char *pclValu2 = "123";
  char *pclValu3 = "123456";
  char *pclValu4 = "1234567890";
  CT_InitStringDescriptor(&rlMyString);
  dbg(TRACE,"=================");
  dbg(TRACE,"TEST: REPLACE (1)");
  dbg(TRACE,"=================");
  SetStringValue(&rlMyString, pclText);
  dbg(TRACE,"TEST: CURRENT STRING = <%s>",rlMyString.Value);
  dbg(TRACE,"TEST: ALLOCATED SIZE = %d",rlMyString.AllocatedSize);
  dbg(TRACE,"TEST: USED STRG SIZE = %d",rlMyString.UsedLen);
  dbg(TRACE,"TEST: SEARCH  STRING = <%s>",pclPatt);
  dbg(TRACE,"TEST: REPLACE STRING = <%s>",pclValu1);
  ReplacePatternInString(&rlMyString, pclPatt, pclValu1);
  dbg(TRACE,"TEST: RESULT  STRING = <%s>",rlMyString.Value);
  dbg(TRACE,"TEST: ALLOCATED SIZE = %d",rlMyString.AllocatedSize);
  dbg(TRACE,"TEST: USED STRG SIZE = %d",rlMyString.UsedLen);
  dbg(TRACE,"=================");
  dbg(TRACE,"TEST: REPLACE (2)");
  dbg(TRACE,"=================");
  SetStringValue(&rlMyString, pclText);
  dbg(TRACE,"TEST: CURRENT STRING = <%s>",rlMyString.Value);
  dbg(TRACE,"TEST: ALLOCATED SIZE = %d",rlMyString.AllocatedSize);
  dbg(TRACE,"TEST: USED STRG SIZE = %d",rlMyString.UsedLen);
  dbg(TRACE,"TEST: SEARCH  STRING = <%s>",pclPatt);
  dbg(TRACE,"TEST: REPLACE STRING = <%s>",pclValu2);
  ReplacePatternInString(&rlMyString, pclPatt, pclValu2);
  dbg(TRACE,"TEST: RESULT  STRING = <%s>",rlMyString.Value);
  dbg(TRACE,"TEST: ALLOCATED SIZE = %d",rlMyString.AllocatedSize);
  dbg(TRACE,"TEST: USED STRG SIZE = %d",rlMyString.UsedLen);
  dbg(TRACE,"=================");
  dbg(TRACE,"TEST: REPLACE (3)");
  dbg(TRACE,"=================");
  SetStringValue(&rlMyString, pclText);
  dbg(TRACE,"TEST: CURRENT STRING = <%s>",rlMyString.Value);
  dbg(TRACE,"TEST: ALLOCATED SIZE = %d",rlMyString.AllocatedSize);
  dbg(TRACE,"TEST: USED STRG SIZE = %d",rlMyString.UsedLen);
  dbg(TRACE,"TEST: SEARCH  STRING = <%s>",pclPatt);
  dbg(TRACE,"TEST: REPLACE STRING = <%s>",pclValu3);
  ReplacePatternInString(&rlMyString, pclPatt, pclValu3);
  dbg(TRACE,"TEST: RESULT  STRING = <%s>",rlMyString.Value);
  dbg(TRACE,"TEST: ALLOCATED SIZE = %d",rlMyString.AllocatedSize);
  dbg(TRACE,"TEST: USED STRG SIZE = %d",rlMyString.UsedLen);
  dbg(TRACE,"=================");
  dbg(TRACE,"TEST: REPLACE (4)");
  dbg(TRACE,"=================");
  SetStringValue(&rlMyString, pclText);
  dbg(TRACE,"TEST: CURRENT STRING = <%s>",rlMyString.Value);
  dbg(TRACE,"TEST: ALLOCATED SIZE = %d",rlMyString.AllocatedSize);
  dbg(TRACE,"TEST: USED STRG SIZE = %d",rlMyString.UsedLen);
  dbg(TRACE,"TEST: SEARCH  STRING = <%s>",pclPatt);
  dbg(TRACE,"TEST: REPLACE STRING = <%s>",pclValu4);
  ReplacePatternInString(&rlMyString, pclPatt, pclValu4);
  dbg(TRACE,"TEST: RESULT  STRING = <%s>",rlMyString.Value);
  dbg(TRACE,"TEST: ALLOCATED SIZE = %d",rlMyString.AllocatedSize);
  dbg(TRACE,"TEST: USED STRG SIZE = %d",rlMyString.UsedLen);
  free(rlMyString.Value);
  return;
}

static void SetStringValue(STR_DESC* prpStringBuffer, char *pcpNewString)
{
  int ilLen = 0;
  ilLen = strlen(pcpNewString);

  if (prpStringBuffer->AllocatedSize <= ilLen)
  {
    prpStringBuffer->Value = realloc(prpStringBuffer->Value, ilLen+1);
    prpStringBuffer->AllocatedSize = ilLen+1;
  }

  strcpy(prpStringBuffer->Value, pcpNewString);
  prpStringBuffer->UsedLen = ilLen;
  
  return;
}

static void ReplacePatternInString(STR_DESC* prpString, char *pcpSearch, char *pcpReplace)
{
  int ilSearchLen = 0;
  int ilReplaceLen = 0;
  int ilNewLen = 0;
  int ilLeftPos = 0;
  int ilRightPos = 0;
  ilSearchLen = strlen(pcpSearch);
  ilReplaceLen = strlen(pcpReplace);
  char *pclPos = NULL;

  pclPos = strstr(prpString->Value,pcpSearch);
  if (pclPos != NULL)
  {
    if (ilReplaceLen == ilSearchLen)
    {
      strncpy(pclPos,pcpReplace,ilReplaceLen);
    }
    else if (ilReplaceLen < ilSearchLen)
    {
      strncpy(pclPos,pcpReplace,ilReplaceLen);
      ilLeftPos = pclPos - prpString->Value + ilReplaceLen;
      ilRightPos = pclPos - prpString->Value + ilSearchLen;
      StrgShiftLeft(ilLeftPos,ilRightPos,prpString->Value);
      prpString->UsedLen = prpString->UsedLen - ilSearchLen + ilReplaceLen;
    }
    else
    {
      ilNewLen = prpString->UsedLen - ilSearchLen + ilReplaceLen;
      if (ilNewLen >= prpString->AllocatedSize)
      {
        prpString->Value = realloc(prpString->Value, ilNewLen+1);
        prpString->AllocatedSize = ilNewLen+1;
      }
      ilLeftPos = pclPos - prpString->Value + ilSearchLen;
      ilRightPos = pclPos - prpString->Value + ilReplaceLen;
      StrgShiftRight(ilLeftPos,ilRightPos,prpString->Value);
      strncpy(pclPos,pcpReplace,ilReplaceLen);
      prpString->UsedLen = prpString->UsedLen - ilSearchLen + ilReplaceLen;
    }
  }
  return;
}

/**************************************************************************
 * These functions are copied from mvtgen. The functions were first copied
 * to mvtgen from gochdl by BST in Y2009.
 * 
 * HandleSqlSelectRows
 * get_any_item
 * GetFieldValue
 * GetFieldValues
 * GetCfg
 *
 **************************************************************************
 */

static int HandleSqlSelectRows(char *pcpTable, char *pcpSqlKey, char *pcpSqlFld, char *pcpSqlDat, int ipLines)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS; 
  char pclSqlBuf[4096*2];
  char pclDatBuf[4096*2];
  int ilPos = 0;
  int ilCnt = 0;
  sprintf(pclSqlBuf,"SELECT %s FROM %s %s", pcpSqlFld, pcpTable, pcpSqlKey);
  dbg(TRACE,"<%s>",pclSqlBuf);
  ilPos = 0;
  ilCnt = 0;
  short slCursor = 0;
  short slFkt = START;
  ilGetRc = DB_SUCCESS; 
  while (ilGetRc == DB_SUCCESS)
  {
    pclDatBuf[0] = 0x00;
    ilGetRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclDatBuf);
    if (ilGetRc == DB_SUCCESS)
    {
      ilCnt++;
      /* dbg(TRACE,"DATA <%s>",pclDatBuf); */
      BuildItemBuffer(pclDatBuf,pcpSqlFld,-1,",");
      StrgPutStrg(pcpSqlDat, &ilPos, pclDatBuf, 0, -1, "\n");
      slFkt = NEXT;
    }
  }
  close_my_cursor(&slCursor);
  if (ilPos > 0)
  {
    ilPos--;
  }
  pcpSqlDat[ilPos] = 0x00;
  dbg(TRACE,"SELECT ROWS: GOT %d RECORDS",ilCnt);
  ilRc = ilGetRc;
  if ((ilGetRc == ORA_NOT_FOUND) && (ilPos > 0))
  {
    ilRc = DB_SUCCESS;
  }
  else
  {
    dbg(TRACE,"SELECT ROWS: NO DATA FOUND");
  }
  dbg(TRACE,"--------------------------");
  return ilCnt;
}

/* ******************************************************************** */
/* ******************************************************************** */
static int  get_any_item(char *dest, char *src, int ItemNbr, char *sep)
{
        int rc = -1;
        int gt_rc;
        int b_len = 0;
        int trim = TRUE;
        if (ItemNbr < 0){
                trim = FALSE;
                ItemNbr = - ItemNbr;
        } /* end if */
        gt_rc = get_item(ItemNbr, src, dest, b_len, sep, "\0", "\0");
        if (gt_rc == TRUE){
                if (trim == TRUE){
                        str_trm_rgt(dest, " ", TRUE);
                } /* end if */
                rc = strlen(dest);
        }/* end if */
        return rc;
}/* end of get_any_item */

/* ******************************************************************** */
/* ******************************************************************** */
static int GetFieldValues(char *pcpFldVal, char *pcpFldLst, char *pcpGetFldLst, char *pcpDatLst)
{
  int ilGetRc = 0;
  char pclFldName[8] = "";
  char pclFldData[4096] = "";
  int ilPos = 0;
  int ilFld = 0;
  int ilMax = 0;

  ilPos = 0;
  ilMax = field_count(pcpGetFldLst);
  for (ilFld=1;ilFld<=ilMax;ilFld++)
  {
    ilGetRc = get_real_item(pclFldName,pcpGetFldLst, ilFld);
    ilGetRc = GetFieldValue(pclFldData, pcpFldLst, pclFldName, pcpDatLst);
    StrgPutStrg (pcpFldVal, &ilPos, pclFldData, 0, -1, ",");
  }
  ilPos--;
  pcpFldVal[ilPos] = 0x00;
  return ilPos;
}

/* ******************************************************************** */
/* ******************************************************************** */
static int GetFieldValue(char *pcpFldVal, char *pcpFldLst, char *pcpFldNam, char *pcpDatLst)
{
  int ilLen = -1;
  int ilFldItm = 0;
  ilFldItm = get_item_no (pcpFldLst, pcpFldNam, 5) + 1;
  if (ilFldItm > 0)
  {
    ilLen = get_real_item (pcpFldVal, pcpDatLst, ilFldItm);
  }
  if (ilLen <= 0)
  {
    strcpy (pcpFldVal, " ");
  }
  return ilLen;
}

/* FROM FLIGHT (Modified) */
/*******************************************************/
/*******************************************************/
static int GetCfgLine(char *pcpFile, char *pcpGroup, char *pcpLine, short spNoOfLines, char *pcpDestBuff, char *pcpDefault)
{
  int ilRC = RC_SUCCESS;

  pcpDestBuff[0] = 0x00;
  ilRC = iGetConfigRow (pcpFile, pcpGroup, pcpLine, spNoOfLines, pcpDestBuff);
  if (ilRC != RC_SUCCESS)
  {
    /* use default entry */
    strcpy (pcpDestBuff, pcpDefault);
  }                             /* end if */
  return ilRC;
}

/**************************************************************************
 * 
 * Handy from mvtgen - CleanDoubleBytes
 * 
 **************************************************************************
 */

static int CleanDoubleBytes(char *pcpText, char *pcpFind, char *pcpSet)
{
  int ilLen1 = 0;
  int ilLen2 = 0;
  ilLen1 = strlen(pcpText);
  ReplaceStringInString(pcpText, pcpFind, pcpSet);
  ilLen2 = strlen(pcpText);
  if (ilLen1 != ilLen2)
  {
    dbg(TRACE,"CLEAN DOUBLE 0D0D: LEN1=%d LEN2=%d",ilLen1,ilLen2);
  }
  return ilLen2;
}

/**************************************************************************
 * 
 * Handy from mvtgen - ReplaceStringInString
 * 
 **************************************************************************
 */
static int ReplaceStringInString(char *pcpString, char *pcpSearch, char *pcpReplace)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  if (MyDebugSwitch == TRUE)
  {
    dbg(TRACE,"TEST: PATCH   STRING = <%s>",pcpString);
    dbg(TRACE,"TEST: SEARCH  STRING = <%s>",pcpSearch);
    dbg(TRACE,"TEST: REPLACE STRING = <%s>",pcpReplace);
  }
  if (MyDebugSwitch == TRUE)
  {
    dbg(TRACE,"BEFORE SET STRING");
  }
  SetStringValue(&rgMyDataString, pcpString);
  if (strcmp(pcpSearch,pcpReplace) != 0)
  {
    if (MyDebugSwitch == TRUE)
    {
      dbg(TRACE,"CALLING REPLACE PATTERN");
    }
    ReplacePatternInString(&rgMyDataString, pcpSearch, pcpReplace);
    strcpy(pcpString,rgMyDataString.Value);
    if (MyDebugSwitch == TRUE)
    {
      dbg(TRACE,"BACK FROM REPLACE PATTERN");
    }
  }
  if (MyDebugSwitch == TRUE)
  {
    dbg(TRACE,"TEST: RESULT  STRING = <%s>",pcpString);
  }
  return ilRc;
}

/* FROM GOCHDL */

/**************************************************************************
 * 
 * Handy from fdihxx - GetLine
 * 
 **************************************************************************
 */

static char *GetLine(char *pcpLine,int ipLineNo)
{
   char *pclResult = NULL;
   char *pclP = pcpLine;
   int ilCurLine;

   ilCurLine = 1;
   while (*pclP != '\0' && ilCurLine < ipLineNo)
   {
      if (*pclP == '\n')
      {
         ilCurLine++;
      }
      pclP++;
   }
   if (*pclP != '\0')
   {
      pclResult = pclP;
   }

   return pclResult;
}

/**************************************************************************
 * 
 * Handy from fdihxx - CopyLine
 * 
 **************************************************************************
 */

static void CopyLine(char *pcpDest,char *pcpSource)
{
   char *pclP1 = pcpDest;
   char *pclP2 = pcpSource;

   while (*pclP2 != '\n' && *pclP2 != '\0')
   {
      *pclP1 = *pclP2;
      pclP1++;
      pclP2++;
   }
   *pclP1 = '\0';
   TrimRight(pcpDest);
} /* End of CopyLine */


/**************************************************************************
 * 
 * Handy from fdihxx - TrimRight
 * 
 **************************************************************************
 */

static void TrimRight(char *pcpBuffer)
{
   char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];

   if (strlen(pcpBuffer) == 0)
   {
      strcpy(pcpBuffer," ");
   }
   else
   {
      while (isspace(*pclBlank) && pclBlank != pcpBuffer)
      {
         *pclBlank = '\0';
         pclBlank--;
      }
   }

} /* End of TrimRight */

/**************************************************************************
 * 
 * CreateTelexFile 
 * v.1.07 - create a file named per
 *  <file_dir>/<prepend>.<YYYYMMDDhhmmss><extension>
 * The <extension> must contain the dot separator if any required.
 * If the prepend is empty, then filename begins with the system date.
 * We always pause for a second so that a unique sustem time is
 * generated (to seconds precision), and thus a unique filename.
 * 
 **************************************************************************
 */

static int    CreateTelexFile (char *pcpTlxText, int ipTxtLen,
    char *pcpFileNamePrepend, char *pcpFileNameExt, char *pcpFileDir)
{
  int   ilRC = RC_SUCCESS;
  char pclFunc[] = "CreateTelexFile:";

  FILE  *fp;
  char  pclFileNameFull [512];
  int   jj;
  
  /*
    Force a sleep of 1s so that the server timestamp which is formatted to
    a second's precision, will be unique for each new telex file.
  */
  nap(1000); 
  if ((strlen(pcpFileNamePrepend)) > 0)
    sprintf (pclFileNameFull,"%s/%s.%s%s",pcpFileDir,pcpFileNamePrepend,
      GetTimeStamp(),pcpFileNameExt);
  else
    sprintf (pclFileNameFull,"%s/%s%s",pcpFileDir,
      GetTimeStamp(),pcpFileNameExt);


  fp = fopen (pclFileNameFull,"w");
  if (fp == '\0')
  {
   dbg(TRACE,"%s **** Error [%s] opening [%s] for writing",
     pclFunc,strerror(errno),pclFileNameFull);
   return RC_FAIL;
  } 

  jj = fwrite (pcpTlxText,sizeof(char),ipTxtLen,fp);
  if (jj < ipTxtLen) 
  {
   dbg(TRACE,"%s **** Error Writing to [%s], wrote <%d> bytes of <%d>",
     pclFunc,pclFileNameFull,jj,ipTxtLen);
   ilRC = RC_FAIL;
  } 
  else 
   dbg(TRACE,"%s **** OK wrote to [%s]",pclFunc,pclFileNameFull);

  fflush(fp);
  fclose(fp);

  return ilRC;
}

/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
