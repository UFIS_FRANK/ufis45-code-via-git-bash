#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/foghdl.c 1.29 2013/01/09 14:48:00SGT gfo Exp $";
#endif /* _DEF_mks_version */

/*                                                                            */
/* ABB AAT/I FOGHDL.C                                                         */
/*                                                                            */
/* Author         : Andreas Kloidt                                            */
/* Date           : March 2002                                                */
/* Description    : Process to maintain Flight On Ground Table (FOGTAB)       */
/*                                                                            */
/* Update history : BST - MAR2010 - Implementation of AutoTowing              */
/*                  BST - JUN2010 - Reading BLKTAB for AutoTowing             */
/*                  GFO - MAR2011 - Fixing the Return Flight JOIN UFIS-425    */
/*                  GFO - APR2011 - Fixing wrong Inventory report UFIS-513    */
/*                                  Added DeleteOFBL_FOGTAB                   */
/*                  GFO - MAY2011   Rolling back from DeleteOFBL_FOGTAB       */
/*                  BST - AUG2011   FltLib inserted, Other Modifications      */
/*                  GFO - SEP2011   UFIS-1015                                 */
/*                  GFO - OCT2011 - Fixing wrong Inventory report UFIS-513    */
/*                  GFO - 20120119  UFIS-1265 RECEIVES FOGU from Flight for   */
/*                                  triggering CalculateGroundTimes           */
/* 20120214 GFO: Added DB_LoadUrnoList UFIS-1484                              */
/* 20120321 GFO: UFIS-1265 Send FOGU to FOGDHD for triggering                 */
/*               CalculateGroundTimes (from JoinAftRecs,SplitAftRecs)         */
/* 20120405 GFO: UFIS-1265 Calculate PAES with PaesArr                        */
/* 20120418 GFO: UFIS-513 Delete records that are Arrival and CXX or NOOP     */
/* 20130109 GFO: UFIS-2032 Change Query , Introduce new Variable ROG_TIMEEND  */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_foghdl[]="%Z% UFIS 4.4 (c) ABB AAT/I %M% %I% / %E% %U% / AKL";
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <netdb.h>
#include <time.h>
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h"
#include "tools.h"
#include "helpful.h"
#include "timdef.h"
#include "ct.h"
#include "fltlib.h"

#define XS_BUFF  128
#define S_BUFF   512
#define M_BUFF   1024
#define L_BUFF   2048
#define XL_BUFF  4096
#define XXL_BUFF 8192

#define MAX_CT_VALUE 10
#define SORT_ASC        0
#define SORT_DESC       1

static int igMyOwnModId = 7700;

static int igUsingGortabRelation = TRUE;
static int igGorTabInitId = 0;
static int igGorTabTriggerQue = 0;
static int igUseGorTabData = FALSE;



/* DEFAULT IN FIPS CEDA.INI "TOWING_IF_GROUNDTIME_MORE_THAN", "1440" */
/* OVERNIGHT TURNAROUNDS SHOULD NOT BE TOWED */

static char pcgTmpBuf1[XL_BUFF];
static char pcgTmpBuf2[XL_BUFF];
static char pcgTmpBuf3[XL_BUFF];

static int igTriggerIsFullRkeyCheck = FALSE;
static int igTriggerIsArrPstaUpdate = FALSE;
static int igTriggerIsDepPstdUpdate = FALSE;
static int igTriggerIsArrTifaUpdate = FALSE;
static int igTriggerIsDepTifdUpdate = FALSE;

static long lgFirstUserTowLine = 0;
static long lgLastUserTowLine = 0;
static long lgFirstOwnTowOutLine = 0;
static long lgFirstUserTowOutLine = 0;
static long lgLastOwnTowInLine = 0;
static long lgLastUserTowInLine = 0;

static int igCheckUserTowArr = TRUE;
static int igCheckUserTowDep = TRUE;

static int igAutoCreateTowArr = FALSE;
static int igAutoCreateTowDep = FALSE;
static int igRecreateTowings = FALSE;

static int igToTowHdl = 0;
static int igIAmTowHdl = FALSE;
static int igHideDelTow = FALSE;
static int igSkipOfblFlights = TRUE;
static int igSkipSkedFlights = TRUE;
static int igSkipPabaFlights = TRUE;
static int igTowEmptyStands = FALSE;
static int igAutoRepairAurn = TRUE;
static int igMustRepairAurn = FALSE;
static int igRepairAurnOnly = FALSE;
static int igTowRecDelFlag = FALSE;
static int igTowingTime = 10;
static int igMinPosTime = 30;
static int igTestTowOutTime = 0;

static int igAutoGlueTowOut = FALSE;
static int igAutoGlueTowIn = FALSE;
static int igAutoGlueTowing = FALSE;
static int igRuleTowInTime = 0;
static int igSkipARuleTime = 0;
static int igSkipDRuleTime = 0;
static int igSkipTRuleTime = 0;


static int igMaxOvnTime = 1440;
static int igOpnArrGrdTime = 5200000;
static char pcgTowOvnHour[16] = "";
static char pcgTowArrTime[16] = "";
static char pcgTowDepTime[16] = "";
static char pcgTowOutPos[16] = "";
static char pcgRmtOutPos[16] = "";

static char pcgTowOldRem2[1024] = "";
static char pcgTowNewRem2[1024] = "";
static char pcgTowNewUTWO[16] = "";
static char pcgTowOldUTWO[16] = "";
static char pcgTowNewUTWI[16] = "";
static char pcgTowOldUTWI[16] = "";

static char pcgBlockedStands[4096] = "";
static char pcgBridgeStands[4096] = "";
static char pcgTowRkeyList[4096] = "";
static char pcgTowRacRegn[16] = "";
static char pcgTowRacTifa[16] = "";
static char pcgTowRacTifd[16] = "";
static char pcgCurRacTifa[16] = "";
static char pcgCurRacTifd[16] = "";
static char pcgCurTowRkey[16] = "";
static char pcgCurRkeyMsg[4096] = "";

static char pcgSendAlwaysFldList[1024] = "FTYP,ADID,FLNO,ALC2,ALC3,REGN,ACT3,ACT5,ORG3,ORG4,DES3,DES4,STOD,STOA,TIFD,TISD,TIFA,TISA,PSTD,PSTA,OFBL,ONBL,RKEY,AURN,URNO";

static char pcgCtAft[] = "AFTTAB";
static char pcgCtGhp[] = "GHPTAB";
static char pcgCtPst[] = "PSTTAB";
static char pcgCtTow[] = "PSTTOW";

static char pcgCtAftFldLst[] = "FTYP,ADID,FLNO,ALC2,ALC3,FLTN,FLNS,FLTI,CSGN,TTYP,REGN,ACT3,ACT5,ORG3,ORG4,DES3,DES4,VIA3,VIA4,STOD,STOA,TIFD,TISD,TIFA,TISA,PSTD,PSTA,OFBL,ONBL,PABS,PAES,PDBS,PDES,PABA,PAEA,PDBA,PDEA,STAT,REM1,REM2,USEC,USEU,CDAT,LSTU,RKEY,AURN,URNO";
static char pcgCtAftFldLen[] = "1,1,9,2,3,5,1,1,8,5,12,3,5,3,4,3,4,3,4,14,14,14,1,14,1,5,5,14,14,14,14,14,14,14,14,14,14,10,10,10,32,32,14,14,10,10,10";

static char pcgCtGhpFldLst[] = "PRSN,PRFL,PRIO,FLCD,FLND,FLSD,FLID,TTPD,REGN,ACT5,TTGD,DEST,VI1D,SCCA,SCCD,MAXT,HTPA,HTPD,REMA,URNO";
static char pcgCtGhpFldLen[] = "20,1,5,3,5,1,5,10,10,5,10,4,4,1,1,6,6,6,20,10";

static char pcgCtPstFldLst[] = "PNAM,RESN,PRFL,URNO";
static char pcgCtPstFldLen[] = "5,10,10,10";

static char pcgCtTowFldLst[] = "FTYP,ADID,PSTA,PABS,PAES,TIFA,PRFL,RKEY,AURN,URNO";
static char pcgCtTowFldLen[] = "1,1,5,14,14,14,3,10,10,10";

static char pcgSorter[] = "SORTER";
static char pcgSorterFldLst[] = "DATA,SORT";
static char pcgSorterFldLen[] = "10,10";
static STR_DESC rgCurData;
static STR_DESC rgCurSort;

static STR_DESC argCtValue[MAX_CT_VALUE+1];
static STR_DESC rgCtLine;
static STR_DESC rgCtData;
static STR_DESC rgCtList;
static STR_DESC rgGorPosList;

static STR_DESC rgTowStndArr;
static STR_DESC rgTowPrioArr;
static STR_DESC rgTowStndDep;
static STR_DESC rgTowPrioDep;

static STR_DESC rgAnyStrg;
static STR_DESC rgGridVal;

static STR_DESC rgCurPrfl;
static STR_DESC rgCurPrio;
static STR_DESC rgCurAdid;
static STR_DESC rgCurFtyp;
static STR_DESC rgCurStnd;
static STR_DESC rgCurPstd;
static STR_DESC rgCurPsta;
static STR_DESC rgCurTifd;
static STR_DESC rgCurTifa;
static STR_DESC rgCurStod;
static STR_DESC rgCurOfbl;
static STR_DESC rgCurOnbl;
static STR_DESC rgCurStoa;
static STR_DESC rgCurUrno;
static STR_DESC rgCurAurn;
static STR_DESC rgCurRkey;
static STR_DESC rgCurRegn;
static STR_DESC rgCurAct3;
static STR_DESC rgCurRem1;
static STR_DESC rgCurRem2;
static STR_DESC rgCurUsec;

static STR_DESC rgPrvPrio;
static STR_DESC rgPrvAdid;
static STR_DESC rgPrvFtyp;
static STR_DESC rgPrvStnd;
static STR_DESC rgPrvPstd;
static STR_DESC rgPrvPsta;
static STR_DESC rgPrvTifd;
static STR_DESC rgPrvTifa;
static STR_DESC rgPrvStod;
static STR_DESC rgPrvStoa;
static STR_DESC rgPrvUrno;
static STR_DESC rgPrvAurn;
static STR_DESC rgPrvRkey;
static STR_DESC rgPrvRegn;
static STR_DESC rgPrvAct3;
static STR_DESC rgPrvRem1;

static char pcgGhpArrRuleFldLst[1024] = "FLCD,FLND,FLSD,FLID,TTPD,REGN,ACT5,TTGD,DEST,VI1D";
static char pcgAftArrRuleFldLst[1024] = "ALC2;ALC3,FLTN,FLNS,FLTI,TTYP,REGN,ACT3;ACT5,----,ORG3;DES3;ORG4;DES4,VIA3;VIA4";

static char pcgGhpDepRuleFldLst[1024] = "FLCD,FLND,FLSD,FLID,TTPD,REGN,ACT5,TTGD,DEST,VI1D";
static char pcgAftDepRuleFldLst[1024] = "ALC2;ALC3,FLTN,FLNS,FLTI,TTYP,REGN,ACT3;ACT5,----,ORG3;DES3;ORG4;DES4,VIA3;VIA4";

static char pcgGhpRotRuleFldLst[1024] = "FLCD,FLND,FLSD,FLID,TTPD,REGN,ACT5,TTGD,DEST,VI1D";
static char pcgAftRotRuleFldLst[1024] = "ALC2;ALC3,FLTN,FLNS,FLTI,TTYP,REGN,ACT3;ACT5,----,ORG3;DES3;ORG4;DES4,VIA3;VIA4";

static char pcgPrintTitle[256] = "";
static char pcgPrintHeader[256] = "";
static char pcgPrintFields[256] = "";
static int igPrintReports = FALSE;


/* UFIS-1485 */
static int igHandleUrnoRanges = FALSE;
static char pcgTableFullName[10] = "FOGTAB";
static char pcgTableKey[10] = "FOGTAB";

/* UFIS-1265 */
static int igToFogHdl = 7770;

/* Structure for flight rotation evaluation */
/* Index 1=Arrival / 2=Departure */
typedef struct
{
  int  FltExists;      /* Avoid new insertion */
  int  FltIsOALeg;     /* Open Arrival Leg */
  int  FltIsNoop;      /* CXX/NOP Flight Flag */
  int  FltIsValid;     /* Result of analysis Flag */
  int  RotIsValid;     /* Result of analysis Flag */
  int  TowIsValid;     /* Result of analysis Flag */
  int  DelAllTows;     /* Deletion Flag */
  int  DelArrTowOut;   /* Deletion Flag */
  int  DelDepTowIn;    /* Deletion Flag */
  int  DelAllOther;    /* Deletion Flag */
  char StrFltUrno[16]; /* Flight STOA/STOD */
  char StrFltRkey[16]; /* Flight STOA/STOD */
  char StrFltSked[16]; /* Flight STOA/STOD */
  char StrFltTime[16]; /* Flight TIFA/TIFD */
  char StrFltStnd[16]; /* Flight PSTA/PSTD */
  char StrLastPos[16]; /* Flight PSTA/PSTD Last Pos of ARR Chain */
  char StrNewStnd[16]; /* Flight NEW PSTA/PSTD Updated by FltHdl */
  char StrNewTime[16]; /* Flight NEW TIFA/TIFD Updated by FltHdl */
  char StrOldTime[16]; /* Flight OLD TIFA/TIFD Updated by FltHdl */
  char StrRulType[16];
  int  ValFltSked;     /* Flight STOA/STOD as minutes */
  int  ValFltTime;     /* Flight TIFA/TIFD as minutes */
  int  ValNewTime;     /* Flight NEW TIFA/TIFD as minutes updated */
  int  ValOldTime;     /* Flight OLD TIFA/TIFD as minutes updated */
  int  ValMinDiff;     /* Difference TIME (NEW-OLD) in minutes */
  int  MaxGrdTime;     /* Turnaround ground time based on BEST */
  int  SkdGrdTime;     /* Turnaround ground time based on BEST */
  int  RotGrdTime;     /* Turnaround ground time based on BEST */
  int  FltGrdTime;     /* Flight ground time */
  int  TowBgnOffs;     /* Tow-Out minutes after arrival (RULE) */
  int  TowEndOffs;     /* Tow-In minutes before departure (RULE) */
  int  CurBgnOffs;     /* Tow-Out minutes after arrival (CURRENT) */
  int  CurEndOffs;     /* Tow-In minutes before departure (CURRENT) */
  long FltLineNo;      /* LineNo of related flight in data grid */
  long FirstTowLineNo; /* LineNo of last towing record in data grid */
  long LastTowLineNo;  /* LineNo of last towing record in data grid */
  long GlueGtoLineNo;
  long GlueGtiLineNo;
  int MaxTowCount;     /* Amount of towings */
  int UsrTowCount;     /* Amount of towings created by users */
  int ActTowCount;     /* Amount of active (not hidden) towings */
  int DelTowCount;     /* Amount of deleted (hidden) towings */
  int ProTowCount;     /* Amount of protected (pre-release) towings */
  int MaxErrCount;     /* Amount of identified errors in the data set */
} STR_FLT;
static STR_FLT rgFltDat[4];


/* Structure for towing data evaluation */
/* FIXED Index 1=ArrTow (TOW-OUT) / 2=DepTow (TOW-IN) */
typedef struct
{
  int  TowExists;      /* Avoid new insertion */
  int  TowIsValid;     /* Result of analysis Flag */
  int  TowIsHidden;    /* Flag for deleted towing */
  int  TowIsTowOut;    /* Flag for arrival towing */
  int  TowIsTowIn;     /* Flag for departure towing */
  int  TowOutAndIn;    /* Flag for double meaning */
  int  TowOutIsOfbl;   /* Flag for skip updates */
  int  TowInIsOfbl;    /* Flag for skip updates */
  int  MyOwnTowRec;    /* Flag of record ownership */
  char StrTowUrno[16]; /* URNO of towing record */
  char StrFltSked[16]; /* Flight STOA/STOD */
  char StrFltTime[16]; /* Flight TIFA/TIFD */
  char StrFltStnd[8];  /* Flight PSTA/PSTD */
  int  ValFltSked;     /* Flight STOA/STOD as minutes */
  int  ValFltTime;     /* Flight TIFA/TIFD as minutes */
  int  TowBgnOffs;     /* Tow-Out minutes after arrival (RULE) */
  int  TowEndOffs;     /* Tow-In minutes before departure (RULE) */
  int  CurBgnOffs;     /* Tow-Out minutes after arrival (CURRENT) */
  int  CurEndOffs;     /* Tow-In minutes before departure (CURRENT) */
  char StrTowStod[16]; /* Tow start time */
  char StrTowStoa[16]; /* Tow end time */
  char StrNewStod[16]; /* Tow start time */
  char StrNewStoa[16]; /* Tow end time */
  char StrTowPstd[8];  /* Tow start position */
  char StrTowPsta[8];  /* Tow end position */
  int  ValTowStod;     /* Tow start time in minutes */
  int  ValTowStoa;     /* Tow end time in minutes */
  int  ValNewStod;     /* Tow start time in minutes */
  int  ValNewStoa;     /* Tow end time in minutes */
  int  ValTowTime;     /* Tow duration in minutes */
  int  CfgTowTime;     /* Tow duration in minutes */
  int  ValTowPark;     /* Stand occupation time on PSTA */
  long ArrLineNo;      /* LineNo of arrival flight in data grid */
  long FltLineNo;      /* LineNo of related flight in data grid */
  long TowLineNo;      /* LineNo of related towing in data grid */
  char StrTowRem1[264]; /* REM1 of towing record */
  char StrRem2Old[264]; /* REM2 of towing record */
  char StrRem2New[264]; /* REM2 of towing record */
  char StrTowUsec[64];  /* User Name of towing record */
  char StrTowTRule[64];  /* Turn Rule Name of towing record */
  char StrTowARule[64];  /* Arr Rule Name of towing record */
  char StrTowDRule[64];  /* Dep Rule Name of towing record */
} STR_TOW;
static STR_TOW rgTowDat[6];


/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
FILE *outp       = NULL;
int  debug_level = TRACE;
/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/
extern int SetSignals(void (*HandleSignal)(int));
extern int DebugPrintItem(int,ITEM *);
extern int DebugPrintEvent(int,EVENT *);
extern int init_db(void);
extern int  ResetDBCounter(void);
extern void HandleRemoteDB(EVENT*);
extern int  sql_if(short ,short* ,char* ,char* );
extern int close_my_cursor(short *cursor);
extern void snap(char*,int,FILE*);
extern int GetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel,char *pcpDef,char *pcpTrim);
extern int BuildItemBuffer(char *pcpData, char *pcpFieldList, int ipNoOfFields, char *pcpSepChr);
extern int get_item_no(char *s, char *f, short elem_len);
extern void  GetServerTimeStamp(char*,int,long,char*);
extern int AddSecondsToCEDATime(char *,long,int);
extern int get_real_item(char *, char *, int);
extern long nap(long);
extern int StrgPutStrg (char *, int *, char *, int, int, char *);



/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;        /* The queue item pointer  */
static EVENT *prgEvent     = NULL;        /* The event pointer       */
static int   igItemLen     = 0;           /* length of incoming item */
static int   igInitOK      = FALSE;      /* Flag for init */

/*   static CFG   *prgCfg; */                     /* structure of the cfg-file */
static char  pcgHomeAp[XS_BUFF] = "";      /* buffer for home airport */
static char  pcgTabEnd[XS_BUFF] = "";      /* buffer for TABEND */

static char pcgRecvName[64] = ""; /* BC-Head used as WKS-Name */
static char pcgDestName[64] = ""; /* BC-Head used as USR-Name */
static char pcgTwStart[64]  = ""; /* BC-Head used as Stamp */
static char pcgTwEnd[64] = "";    /* BC-Head used as Stamp */

static char pcgSaveMyHopo[16] = "SIN";

static char  pcgConfFile[S_BUFF];      /* buffer for config-file name */
static int   igUseHopo = FALSE;          /* flag for use of HOPO-field */
static int   igModID_Router  = 1200;      /* MOD-ID of Router  */

static char      pcgHostName[XS_BUFF];
static char      pcgHostIp[XS_BUFF];

static int    igQueCounter=0;

/*entry's from configfile*/
static char pcgConfigFile[512];
static char pcgUfisToUfisConfigFile[512];
static char pcgCfgBuffer[512];
static int igPabsArr = 10;
static int igPaesArr = 10;
static char pcgPaesArr[32] = "20201231235959";
static int igPabsRot = 10;
static int igPaesRot = 10;
static int igPdbsDep = 10;
static int igPdesDep = 10;
static int igPdbsRot = 10;
static int igPdesRot = 10;
static int igRotTimeArrPerc = 50;
static char pcgInitHisStart[32] = "20020101";
static char pcgLastDayForInit[32] = "20021231";
static int igOnGroundTimeLimit = 60;
static int igTolerance = 0;
static int igRogTimeEnd = 7;
static int ilSelectVers = 1;
static char pcgInitFog[32] = "NO";
static char pcgExclFtyp[512] = "";
static char pcgRogBegin[32] = "";
static char pcgRogEnd[32] = "";
static char pcgInitRog[32] = "NO";
static char pcgTriggerAction[32] = "NO";
static char pcgTriggerBchdl[32] = "NO";
static char pcgTriggerAFTBchdl[32] = "YES";
static int igBcOutMode = 0;
static char pcgDebugLevel[32] = "TRACE";
static char pcgDebugLevelInit[32] = "TRACE";
static int igSSIHDL = 7760;
static int igCedaEventInterface = -1;
static int igRemoteTarget = -1;
static int igValidate = FALSE;
static int igValInsTot = 0;
static int igValDelTot = 0;
static int igValUpdTot = 0;
static int igValOKTot = 0;
static int igValIns = 0;
static int igValDel = 0;
static int igValUpd = 0;
static int igValOK = 0;

static int igSendRACBroadcast = FALSE;
static char pcgRACCommand[16];
static char pcgRACDataList[32000];
static char pcgRACBegin[32];
static char pcgRACEnd[32];
static char pcgRACTargets[128] = "";
static int igTotCountRead;
static int igTotCountIns;
static int igRegnCountRead;
static int igRegnCountIns;
static int igDiffRegn;
static int igAutoJoin = TRUE;
static char pcgUrnoList[32000] = "";

static int igWaitBeforeROGU = 0;  /* Wait im ms before ROGU command is processed */

static char pcgCurrentTime[32] = "";

static int igDiffUtcToLocal;   /* Time diff between Utc and Local Time */
static int igUtcToLocalDiff;   /* Time diff between Utc and Local Time */

/* Begin Array Insert */
#define S_FOR  1
#define S_BACK 2
REC_DESC rgRecDesc;
static int igUseArrayInsert = FALSE;
static char pcgNewUrnos[1000*16];
static int igLastUrno = 0;
static int igLastUrnoIdx = -1;

static char pcgCheckOnGroundTimeLimit[32] = "YES";

typedef struct
{
  char pclUrno[16];
  char pclHopo[8];
  char pclRegn[16];
  char pclAurn[16];
  char pclDurn[16];
  char pclRkey[16];
  char pclLand[16];
  char pclAirb[16];
  char pclFtyp[4];
  char pclStat[4];
  char pclFtpa[4];
  char pclFtpd[4];
  char pclSeqn[16];
  char pclSeqs[4];
  char pclFlag[4];
} STR_ROG;
static STR_ROG rgRogDat[10000];
static int igCurRogDat = -1;
static char pcgRogBuf[10000*150];
static char pcgRogFld[1024] = "URNO,HOPO,REGN,AURN,DURN,RKEY,LAND,AIRB,FTYP,STAT,FTPA,FTPD,SEQN,SEQS";
static char pcgRogVal[1024] = ":VURNO,:VHOPO,:VREGN,:VAURN,:VDURN,:VRKEY,:VLAND,:VAIRB,:VFTYP,:VSTAT,:VFTPA,:VFTPD,:VSEQN,:VSEQS";
/* End Array Insert */

/* for trigger action */
static EVENT *prgOutEvent = NULL;

static int    Init_foghdl();
static int    Reset(void);                        /* Reset program          */
static void   Terminate(int);                     /* Terminate program      */
static void   HandleSignal(int);                  /* Handles signals        */
static void   HandleErr(int);                     /* Handles general errors */
static void   HandleQueErr(int);                  /* Handles queuing errors */
static int    HandleInternalData(void);           /* Handles event data     */
static void   HandleQueues(void);                 /* Waiting for Sts.-switch*/
/******************************************************************************/
/* Function prototypes by AKL                                                 */
/******************************************************************************/
/* Init-functions  */

static int GetQueues();
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
                     char *pcpTwStart,char* pcpTwEnd,
		     char *pcpSelection,char *pcpFields,char *pcpData,
                     char *pcpAddStruct,int ipAddstructSize);
static int GetConfig();
static int TimeToStr(char *pcpTime,time_t lpTime);
static int InitFog();
static int BuildGroundTimes(char *pclWorkDay);
static int BuildGroundTimesArrivals(char *pclWorkDay);
static int BuildGroundTimesDepartures(char *pclWorkDay);
static int BuildGroundTimesRotations(char *pclWorkDay);
static int CalculateGroundTimes(char *pcpTifa, char *pcpTifaPrev,
                                char *pcpTifd, char *pcpTifdNext,
                                char *pcpOnbl, char *pcpOnblPrev,
                                char *pcpOfbl, char *pcpOfblNext,
                                char *pcpType, int ipCur, int ipMax,
                                char *pcpPabs, char *pcpPaes, char *pcpPdbs, char *pcpPdes);
static int InsFOGTAB(char *pcpUrno, char *pcpType, char *pcpStat,
                     char *pcpFirstDay, char *pcpLastDay);
static int CheckFOGTAB();
static int UpdateFOGTAB(char *pcpUrno);
static int UpdateArrival(char *pcpUrno, char *pcpTifa, char *pcpTifd, char *pcpPabs,
                         char *pcpPaes, char *pcpPdbs, char *pcpPdes, char *pcpOnbl,
                         char *pcpRkey, char *pcpFtyp, char *pcpFlno);
static int UpdateDeparture(char *pcpUrno, char *pcpTifa, char *pcpTifd, char *pcpPabs,
                           char *pcpPaes, char *pcpPdbs, char *pcpPdes, char *pcpRkey,
                           char *pcpFtyp, char *pcpFlno);
static int UpdateRotation(char *pcpUrno, char *pcpRkey);
static int UpdFOGTAB(char *pcpUrno, char *pcpOgbs, char *pcpOges, char *pcpType,
                     char *pcpOnbl, char *pcpOfbl, char *pcpRkey, char *pcpAdid);
static int CheckTolerance(char *pcpNewPabs, char *pcpOldPabs, char *pcpNewPaes, char *pcpOldPaes,
                          char *pcpNewPdbs, char *pcpOldPdbs, char *pcpNewPdes, char *pcpOldPdes);
static int CompareTime(char *pcpTime1, char *pcpTime2, int ipFac);
static int ValidateFOGTAB();
static int TriggerBchdlAction(char *pcpCmd, char *pcpTable, char *pcpSelection,
                              char *pcpFields, char *pcpData, int ipBchdl, int ipAction);
static long GetSecondsFromCEDATime(char *pcpDateTime);
static int InitRog();
static int BuildROGTAB(char *pcpRegn, char *pcpStartTime);
static int InsROGTAB(char *pcpRegn, char *pcpAurn, char *pcpDurn, char *pcpRkey,
                     char *pcpLand, char *pcpAirb, char *pcpFtyp, char *pcpStat,
                     char *pcpFtpa, char *pcpFtpd);
static int UpdROGTAB(char *pcpAdid, char *pcpUrno,  char *pcpRurn, char *pcpTime,
                     char *pcpFtyp, char *pcpStat, char *pcpFtpad);
static int SortCheckRegn(char *pcpRegn, char *pcpTime, char *pcpSeqn);
static int UpdSEQN(char *pcpUrno, int ipSeqn, char *pcpSeqn);
static int GetCorrectedFormatZero(char *pcpResult, char *pcpString, int ipLen);
static int UpdSEQS(char *pcpUrno, char *pcpSeqs, char *pcpPrevSeqs);
static int UpdateROGTAB(char *pcpUrno);
static int DelROG(char *pcpUrno);
static void TrimRight(char *pcpBuffer);
static int JoinAftRecs(char *pcpArrUrno, char *pcpDepUrno, char *pcpRkey);
static int SplitAftRecs(char *pcpArrUrno, char *pcpDepUrno);
static int CheckROGvsAFT(char *pcpRegn, char *pcpSeqn);
static int CompressDataList(char *pcpData);
static int AppendUrnoList();
static int CalculateMaxCEDATime(char *pcpResult, char *pcpVal1, char *pcpVal2, char *pcpVal3,
                                char *pcpVal4);
static int MyAddSecondsToCEDATime(char *pcpTime, long lpValue, int ipP);
static int SqlArrayInsert();
static int SqlArrayFill();
static int SearchByKey(char *pcpKey, char * pcpVal, int ilDirection, int *pipIndex);
static int SearchByAIRB(char *pcpAirb,int *pipIndex);
static int SearchByRKEYDep(char *pcpRkey,int *pipIndex);
static int SearchByLAND(char *pcpLand,int *pipIndex);
static int SearchByRKEYArr(char *pcpRkey,int *pipIndex);
static int SearchByLANDSeqs(char *pcpLand,int *pipIndex);
static int SearchByLANDSeqn(char *pcpSeqn,int *pipIndex);
static int SearchByAIRBSeqn(char *pcpSeqn,int *pipIndex);
static int SearchBySEQN(char *pcpSeqn,int *pipIndex);
static int CheckBcMode(char *pcpNPabs, char *pcpOPabs, char *pcpNPaes, char *pcpOPaes,
                       char *pcpNPdbs, char *pcpOPdbs, char *pcpNPdes, char *pcpOPdes,
                       char *pcpFieldList, char *pcpDataList);
static int HandleSimulation();

/* Functions for AutoTowing */
/* ======================== */
static int InitMyTab(char *pcpCtTabName, char *pcpCtFldList, char *pcpCtFldLength);
static int LoadTowRules(void);
static int LoadFlightRotation(char *pcpAftRkey);
static int CheckFlightRotation(void);
static int EvaluateTowingRules(void);
static long CheckOverNightRule(char *pcpArrTifa, char *pcpDepTifd);
static long SearchTowingRule(char *pcpRuleTypes, long lpFlightLine, int ipTellMeWhy);
static int CreateOrUpdateTowings(void);
static int CreateTowingRecord(int ipTowIdx);
static int InsertTowingRecord2DB(int ipTowIdx);
static void BuildSqlInsValStrg(char *pcpResult, char *pcpFldLst);
static void ResetFlightArray(void);
static void ResetTowingArray(int ipIdx);
static void FillTowingArray(int ipIdx, long lpLine);
static void PrintTowingArray(int ipIdx);
static int get_any_item(char *dest, char *src, int ItemNbr, char *sep);
static void CheckGhpReload(char *pcpCmd, char *pcpTbl, char *pcpSel, char *pcpFld, char *pcpDat);
static void CheckPstReload(char *pcpCmd, char *pcpTbl, char *pcpSel, char *pcpFld, char *pcpDat);
static void CheckAutoTowTrigger(char *pcpCmd, char *pcpTbl, char *pcpSel, char *pcpFld, char *pcpDat);
static void CheckTowings(char *pcpCmd, char *pcpTbl, char *pcpSel, char *pcpFld, char *pcpDat);
static void CheckTowingChain(void);
static void CheckTowingTimeUpdates(int ipForWhat, int ipIdx, char *pcpOldTime, char *pcpNewTime);
static void CheckUserTowingUpdates(int ipForWhat, long lpLineNo, char *pcpData, int ipValue);
static void ShiftTowingTimeField(int ipForWhat, long lpLineNo, char *pcpFldName, int ipMinDiff);
static void UpdateStandAllocationTimes(long lpPrvTowLine, long lpCurTowLine);
static int CountTowings(int ipForWhat, int ipStatus);
static int AutoRepairAurn(int ipReset);
static long SearchNextTowing(char *pcpPsta, long ipBgnLine, long ipStatus, int ipChkPos);
static long SearchPrevTowing(char *pcpPstd, long ipStatus);
static void InitBridgeStands(void);
static int StandWithBridge(char *pcpStand);
static int UpdateGridData(char *pcpGrid, long lpLineNo, char *pcpName, char *pcpData);
static int CheckGridUpdates(char *pcpGrid, int ipForWhat);
static int CreateOutMessage(int ipFowWhat, char *pcpCmd, char *pcpFldName, char *pcpNewData, char *pcpOldData, char *pcpUrno);
static int CreateSendAlways(char *pcpGrid, char *pcpFldList, char *pcpNewData, char *pcpOldData);
static int UpdateDbRecord(char *pcpTab, char *pcpSel, char *pcpFld, char *pcpDat);
static int DeleteDbRecord(char *pcpTab, char *pcpSel, char *pcpFld, char *pcpDat);
static void PrintRotationChain(char *pcpTitle, char *pcpHead, char *pcpFields);
static void PrintReports(char *pcpForWhat);

/******************************************************************************/

static int ReleaseActionInfo(char *pcpRoute, char *pcpPrios, char *pcpTbl, char *pcpCmd, char *pcpUrnoList,
                              char *pcpSel, char *pcpFld, char *pcpDat, char *pcpOldDat);
static void GetTowHdlConfig(char *pcpInfo);
static void CreateUfisTags(char *pcpName, char *pcpBgn, char * pcpEnd);
static int InitAutoTowing(void);
static void PublishTowhdlConfig(void);
static int GetCfg (char *pcpGroup, char *pcpLine, short spNoOfLines, char *pcpDestBuff, char *pcpDefault);
static int LoadRemoteStands(void);
static void GetBestRemoteStand(char *pcpArrStand, char *pcpDepStand, char *pcpBgnTime, char *pcpEndTime, char *pcpRmtStand);
static int PrepareStandAllocation(char *pcpBgnTime, char *pcpEndTime, char *pcpUsedStands, char *pcpUnusedStands);
static int FindBestRemoteStand(char *pcpBgnTime, char *pcpEndTime, char *pcpRmtStand);
static int LoadOccupiedStands(char *pcpBgn, char *pcpEnd, char *pcpPstArr, int ipForWhat);
static int GetStandsFromGortab(char *pcpBgnTime, char *pcpEndTime, char *pcpArrPst, char *pcpUnusedStands, int ipForWhat);
static int LoadBlockedStands(char *pcpBgnTime, char *pcpEndTime, char *pcpArrPst, int ipForWhat);
static int CombineTwoLists(char *pcpResult, char *pcpList1, char *pcpList2, int ilForWhat);
static int SortItemsByPrioList(char *pcpOutList, char *pcpOutPrio, char *pcpGetItems, char *pcpItmList, char *pcpItmPrio, int ilForWhat);
static void ReplaceChars(char *pcpItems, char *pcpGet, char *pcpSet);
static void CreateRem2DataList(int ipIdx, int ipUpdateGrid, int ipForInsert, long lpLineNo, int ipForWhat);
static void GetAnyValue(int ipForWhat, char *pcpGridName, long lpGridLine, char *pcpFldName, char *pcpResult);
static int CheckRotationUpdates(int ipForWhat, long lpLineNo);

static int DeleteOFBL_FOGTAB(char *pcpRegn);


/* FOR GORTAB USAGE */
static int CheckGorTabInit(void);
static int GetGorTabInitTrigger(char *pcpSqlKey, char *pcpFldName, char *pcpFldData);
static int HandleGorTabTrigger(char *pcpSelKey, char *pcpFields, char *pcpData);



/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
  int    ilRc = RC_SUCCESS;            /* Return code            */
  int    ilCnt = 0;
  int   ilItemFlag=TRUE;
  char pclTest1[4096];
  char pclTest2[4096];
  time_t now = 0;
  INITIALIZE;            /* General initialization    */

  /* signal handling of SIGPIPE,SIGCHLD,SIGALRM,SIGTERM */
  SetSignals(HandleSignal);

  dbg(TRACE,"------------------------------------------");
  dbg(TRACE,"MAIN: version <%s>",sccs_foghdl);
  igMyOwnModId = mod_id;

  /* Attach to the MIKE queues */
  do{
    ilRc = init_que();
    if(ilRc != RC_SUCCESS)
      {
	dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
	sleep(6);
	ilCnt++;
      }/* end of if */
  }while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
      sleep(60);
      exit(1);
    }else{
      dbg(TRACE,"MAIN: init_que() OK!");
      dbg(TRACE,"MAIN: mod_id   <%d>",mod_id);
      dbg(TRACE,"MAIN: mod_name <%s>",mod_name);
    }/* end of if */
  do
    {
      ilRc = init_db();
      if (ilRc != RC_SUCCESS)
	{
	  dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
	  sleep(6);
	  ilCnt++;
	} /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
      sleep(60);
      exit(2);
    }else{
      dbg(TRACE,"MAIN: init_db()  OK!");
    } /* end of if */

  /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
  *pcgConfFile = 0x00;
  sprintf(pcgConfFile,"%s/%s",getenv("BIN_PATH"),mod_name);
  ilRc = TransferFile(pcgConfFile);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
    } /* end of if */
  dbg(TRACE,"MAIN: Binary-file = <%s>",pcgConfFile);
  ilRc = SendRemoteShutdown(mod_id);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    }
  if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: waiting for status switch ...");
      HandleQueues();
      dbg(TRACE,"MAIN: now running ...");
    }/* end of if */
  if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: initializing ...");
      dbg(TRACE,"------------------------------------------");
      if(igInitOK == FALSE)
	{
	  ilRc = Init_foghdl();
	  if(ilRc == RC_SUCCESS)
	    {
	      dbg(TRACE,"");
	      dbg(TRACE,"------------------------------------------");
	      dbg(TRACE,"MAIN: initializing OK");
	      igInitOK = TRUE;
	    }
	}
    }else{
      Terminate(1);
    }
  dbg(TRACE,"------------------------------------------");
  igHandleUrnoRanges = DB_GetUrnoKeyCode(pcgTableFullName,pcgTableKey);
  dbg(TRACE, "===============================================================");
  if (igHandleUrnoRanges != TRUE)
  {
    dbg(TRACE,"URNO POOL: GET URNO VALUES BY <GetNextValues> SNOTAB.ACNU");
  }
  else
  {
    dbg(TRACE,"URNO POOL: GET URNO NUMBERS BY <GetNextNumbers> NUMTAB.KEYS");
  }
  dbg(TRACE, "===============================================================");

  if (igInitOK == TRUE)
    {
      CheckGorTabInit();
      now = time(NULL);
      while(TRUE)
	{
	  memset(prgItem,0x00,igItemLen);
	  ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
	  dbg(DEBUG,"QUE Counter %d",++igQueCounter);
	  /* depending on the size of the received item  */
	  /* a realloc could be made by the que function */
	  /* so do never forget to set event pointer !!! */
	  prgEvent = (EVENT *) prgItem->text;
	  if( ilRc == RC_SUCCESS )
	    {
	      /* Acknowledge the item */
	      ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
	      if( ilRc != RC_SUCCESS )
		{
		  /* handle que_ack error */
		  HandleQueErr(ilRc);
		} /* fi */
	      switch( prgEvent->command )
		{
		case    HSB_STANDBY    :
		  ctrl_sta = prgEvent->command;
		  HandleQueues();
		  break;
		case    HSB_COMING_UP    :
		  ctrl_sta = prgEvent->command;
		  HandleQueues();
		  break;
		case    HSB_ACTIVE    :
		  ctrl_sta = prgEvent->command;
		  break;
		case    HSB_ACT_TO_SBY    :
		  ctrl_sta = prgEvent->command;
		  /* CloseConnection(); */
		  HandleQueues();
		  break;
		case    HSB_DOWN    :
		  /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
		  ctrl_sta = prgEvent->command;
		  Terminate(1);
		  break;
		case    HSB_STANDALONE    :
		  ctrl_sta = prgEvent->command;
		  ResetDBCounter();
		  break;
		case    REMOTE_DB :
		  /* ctrl_sta is checked inside */
		  HandleRemoteDB(prgEvent);
		  break;
		case    SHUTDOWN    :
		  /* process shutdown - maybe from uutil */
		  Terminate(0);
		  break;
		case    RESET        :
		  ilRc = Reset();
		  break;
		case    EVENT_DATA    :
		  if((ctrl_sta == HSB_STANDALONE) ||
		     (ctrl_sta == HSB_ACTIVE) ||
		     (ctrl_sta == HSB_ACT_TO_SBY))
		    {
		      ilItemFlag=TRUE;
		      ilRc = HandleInternalData();
		      if(ilRc != RC_SUCCESS)
			{
			  HandleErr(ilRc);
			}/* end of if */
		    }
		  else
		    {
		      dbg(TRACE,"MAIN: wrong HSB-status <%d>",ctrl_sta);
		      DebugPrintItem(TRACE,prgItem);
		      DebugPrintEvent(TRACE,prgEvent);
		    }/* end of if */
		  break;
		case    TRACE_ON :
		  dbg_handle_debug(prgEvent->command);
		  break;
		case    TRACE_OFF :
		  dbg_handle_debug(prgEvent->command);
		  break;
                case  111 :
                  ilRc = iGetConfigEntry(pcgConfigFile,"FOGHDL","REPAIR_REGN",
                                         CFG_STRING,pcgCfgBuffer);
                  if (ilRc == RC_SUCCESS)
                  {
                     igSendRACBroadcast = TRUE;
                     strcpy(pcgRACCommand,"RAC");
                     strcpy(pcgRACBegin,"");
                     strcpy(pcgRACEnd,"");
                     strcpy(pcgRACDataList,"");
                     ilRc = BuildROGTAB(pcgCfgBuffer,NULL);
                     strcpy(pcgRACDataList,"");
                     igSendRACBroadcast = FALSE;
                  }
                  break;
                case 776:
                  dbg(TRACE,"+++++++++++++++TEST+++++++++++++++++++++");
                  strcpy(pclTest1,"0,1,2,3,4,5,6,7,8,9");
                  dbg(TRACE,"TEST STRING <%s>",pclTest1);
                  CT_GetItemsFromTo(pclTest2, pclTest1, 0, 0, ",");
                  dbg(TRACE,"ITEMS 0-0 <%s>",pclTest2);
                  CT_GetItemsFromTo(pclTest2, pclTest1, 1, 1, ",");
                  dbg(TRACE,"ITEMS 1-1 <%s>",pclTest2);
                  CT_GetItemsFromTo(pclTest2, pclTest1, 9, 9, ",");
                  dbg(TRACE,"ITEMS 9-9 <%s>",pclTest2);
                  CT_GetItemsFromTo(pclTest2, pclTest1, 0, 9, ",");
                  dbg(TRACE,"ITEMS 0-9 <%s>",pclTest2);
                  CT_GetItemsFromTo(pclTest2, pclTest1, 1, 9, ",");
                  dbg(TRACE,"ITEMS 1-9 <%s>",pclTest2);
                  CT_GetItemsFromTo(pclTest2, pclTest1, 0, 8, ",");
                  dbg(TRACE,"ITEMS 0-8 <%s>",pclTest2);
                  dbg(TRACE,"+++++++++++++++TEST+++++++++++++++++++++");
                  strcpy(pclTest1,"0xxx1xxx2xxx3xxx4xxx5xxx6xxx7xxx8xxx9");
                  dbg(TRACE,"TEST STRING <%s>",pclTest1);
                  CT_GetItemsFromTo(pclTest2, pclTest1, 0, 0, "xxx");
                  dbg(TRACE,"ITEMS 0-0 <%s>",pclTest2);
                  CT_GetItemsFromTo(pclTest2, pclTest1, 1, 1, "xxx");
                  dbg(TRACE,"ITEMS 1-1 <%s>",pclTest2);
                  CT_GetItemsFromTo(pclTest2, pclTest1, 9, 9, "xxx");
                  dbg(TRACE,"ITEMS 9-9 <%s>",pclTest2);
                  CT_GetItemsFromTo(pclTest2, pclTest1, 0, 9, "xxx");
                  dbg(TRACE,"ITEMS 0-9 <%s>",pclTest2);
                  CT_GetItemsFromTo(pclTest2, pclTest1, 1, 9, "xxx");
                  dbg(TRACE,"ITEMS 1-9 <%s>",pclTest2);
                  CT_GetItemsFromTo(pclTest2, pclTest1, 0, 8, "xxx");
                  dbg(TRACE,"ITEMS 0-8 <%s>",pclTest2);
                  dbg(TRACE,"+++++++++++++++TEST+++++++++++++++++++++");
                  break;
		case 777:
                  dbg(TRACE,"+++++++++TEST SINGLE ARRIVAL++++++++++++");
                  ToolsSetSpoolerInfo("TESTRECV", "TESTDEST", "TESTTWS", pcgTwEnd);
                  CheckTowings("CTOW", "AFTTAB", "", "RKEY", "1053060382");
                  ToolsReleaseEventSpooler(TRUE, PRIORITY_3);
                  dbg(TRACE,"+++++++++++++++TEST+++++++++++++++++++++");
		  break;
		case 778:
                  dbg(TRACE,"++++++++++++TEST ROTATION+++++++++++++++");
                  ToolsSetSpoolerInfo("TESTRECV", "TESTDEST", "TESTTWS", pcgTwEnd);
                  CheckTowings("CTOW", "AFTTAB", "", "RKEY", "1053106328");
                  ToolsReleaseEventSpooler(TRUE, PRIORITY_3);
                  dbg(TRACE,"+++++++++++++++TEST+++++++++++++++++++++");
		  break;
		case 779:
                  dbg(TRACE,"+++++++++TEST SINGLE DEPARTURE++++++++++");
                  ToolsSetSpoolerInfo("TESTRECV", "TESTDEST", "TESTTWS", pcgTwEnd);
                  CheckTowings("CTOW", "AFTTAB", "", "RKEY", "1058175465");
                  ToolsReleaseEventSpooler(TRUE, PRIORITY_3);
                  dbg(TRACE,"+++++++++++++++TEST+++++++++++++++++++++");
		  break;
		case 780:
                  dbg(TRACE,"++++++++TEST MULTIPLE RKEY LIST+++++++++");
                  ToolsSetSpoolerInfo("TESTRECV", "TESTDEST", "TESTTWS", pcgTwEnd);
                  CheckTowings("CTOW", "AFTTAB", "", "RKEY", "1053060382,1053106328,1058175465");
                  ToolsReleaseEventSpooler(TRUE, PRIORITY_3);
                  dbg(TRACE,"+++++++++++++++TEST+++++++++++++++++++++");
		  break;
		case 888:
                  dbg(TRACE,"++++++++TEST GORTAB LIST+++++++++");
                  GetStandsFromGortab("20110715223000", "20110811020000", "'400','504','506','606','BST','DKA'", argCtValue[4].Value, 0);
                  dbg(TRACE,"+++++++++++++TEST++++++++++++++++");
		  break;
		case 889:
                  dbg(TRACE,"++++++++TEST GORTAB LIST+++++++++");
                  GetStandsFromGortab("20110810220000", "20110811020000", "'400','504','506','606','BST','DKA'", argCtValue[4].Value, 0);
                  dbg(TRACE,"+++++++++++++TEST++++++++++++++++");
		  break;
		default            :
		  dbg(TRACE,"MAIN: unknown event");
		  DebugPrintItem(TRACE,prgItem);
		  DebugPrintEvent(TRACE,prgEvent);
		  break;
		} /* end switch */
	    }else{
	      /* Handle queuing errors */
	      HandleQueErr(ilRc);
	    } /* end else */



	  /**************************************************************/
	  /* time parameter for cyclic actions                          */
	  /**************************************************************/

	  now = time(NULL);

	} /* end while */
    }else{
      dbg(TRACE,"MAIN: Init_foghdl() failed with <%d> Sleeping 30 sec.! Then terminating ...",ilRc);
      sleep(30);
    }
  exit(0);
  return 0;
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_foghdl()
{
  int    ilRc = RC_SUCCESS;            /* Return code */

  GetQueues();

  strcpy(pcgHomeAp,"");
  strcpy(pcgTabEnd,"");
  strcpy(pcgRecvName,"");
  strcpy(pcgDestName,"");
  strcpy(pcgTwStart,"");
  strcpy(pcgTwEnd,"");

  dbg(TRACE,"=================================");
  dbg(TRACE,"CHECK INITIAL GLOBAL VARIABLES");
  dbg(TRACE,"pcgHomeAp <%s>",pcgHomeAp);
  dbg(TRACE,"pcgTabEnd <%s>",pcgTabEnd);
  dbg(TRACE,"pcgRecvName <%s>",pcgRecvName);
  dbg(TRACE,"pcgDestName <%s>",pcgDestName);
  dbg(TRACE,"pcgTwStart <%s>",pcgTwStart);
  dbg(TRACE,"pcgTwEnd <%s>",pcgTwEnd);
  dbg(TRACE,"=================================");

  /* reading default home-airport from sgs.tab */
  ilRc = tool_search_exco_data("SYS","HOMEAP",pcgHomeAp);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_foghdl : No HOMEAP entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_foghdl : HOMEAP = <%s>",pcgHomeAp);
    }
  strcpy(pcgSaveMyHopo,pcgHomeAp);

  /* reading default table-extension from sgs.tab */
  ilRc = tool_search_exco_data("ALL","TABEND",pcgTabEnd);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_foghdl : No TABEND entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      sprintf(pcgTwEnd,"%s,%s,%s",pcgHomeAp,pcgTabEnd,mod_name);
      dbg(TRACE,"Init_foghdl : TW_END = <%s>",pcgTwEnd);

      if (strcmp(pcgTabEnd,"TAB") == 0)
	{
	  igUseHopo = TRUE;
	  dbg(TRACE,"Init_foghdl: use HOPO-field!");
	}
    }

  ilRc = GetConfig();

  ilRc = TimeToStr(pcgCurrentTime,time(NULL));

  InitRecordDescriptor(&rgRecDesc,1024,0,FALSE);

  ilRc = InitFog();
  ilRc = InitRog();

  if (igIAmTowHdl == TRUE)
  {
    dbg(TRACE,"NOW PREPARING THE TOWHDL ENVIRONMENT");
    dbg(TRACE,"====================================");
    InitMyTab("STR_DESC","","");
    InitMyTab(pcgCtAft,pcgCtAftFldLst,pcgCtAftFldLen);
    InitMyTab(pcgCtGhp,pcgCtGhpFldLst,pcgCtGhpFldLen);
    InitBridgeStands();
    InitMyTab(pcgCtPst,pcgCtPstFldLst,pcgCtPstFldLen);
    LoadRemoteStands();
    InitMyTab(pcgCtTow,pcgCtTowFldLst,pcgCtTowFldLen);
    LoadTowRules();
    InitMyTab(pcgSorter,pcgSorterFldLst,pcgSorterFldLen);
    dbg(TRACE,"TOWHDL ENVIRONMENT IS READY FOR USE");
    dbg(TRACE,"===================================");
  }

  dbg(TRACE,"=================================");
  dbg(TRACE,"CHECK FINAL GLOBAL VARIABLES");
  dbg(TRACE,"pcgHomeAp <%s>",pcgHomeAp);
  dbg(TRACE,"pcgTabEnd <%s>",pcgTabEnd);
  dbg(TRACE,"pcgRecvName <%s>",pcgRecvName);
  dbg(TRACE,"pcgDestName <%s>",pcgDestName);
  dbg(TRACE,"pcgTwStart <%s>",pcgTwStart);
  dbg(TRACE,"pcgTwEnd <%s>",pcgTwEnd);
  dbg(TRACE,"=================================");


  return(ilRc);
} /* end of initialize */

/*********************************************************************
Function : GetQueues()
Paramter :
Return Code: RC_SUCCESS,RC_FAIL
Result:
Description: Gets all necessary queue-ID's for CEDA-internal
             communication!
*********************************************************************/
static int GetQueues()
{
  int ilRc = RC_FAIL;

  /* get mod-id of router */
  if ((igModID_Router = tool_get_q_id("router")) == RC_NOT_FOUND ||
      igModID_Router == RC_FAIL || igModID_Router == 0)
    {
      dbg(TRACE,"GetQueues   : tool_get_q_id(router) returns: <%d>",igModID_Router);
      ilRc = RC_FAIL;
    }else{
      dbg(TRACE,"GetQueues   : <router> mod_id <%d>",igModID_Router);
      ilRc = RC_SUCCESS;
    }
  return ilRc;
}

static int CheckCfg(void)
{
  int ilRc = RC_SUCCESS;
  return ilRc;
}
/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{

  int    ilRc = RC_SUCCESS;    /* Return code */

  dbg(TRACE,"Reset: now reseting ...");

  return ilRc;

} /* end of Reset */
/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
  dbg(TRACE,"Terminate: now leaving ...");

  sleep(ipSleep);

  exit(0);

} /* end of Terminate */
/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
  switch(pipSig)
    {
    case SIGALRM:
      break;
    case SIGPIPE:
      break;
    case SIGCHLD:
      break;
    case SIGTERM:
      Terminate(1);
      break;
    default    :
      Terminate(10);
      break;
    } /* end of switch */
} /* end of HandleSignal */
/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
  /*    int    ilRc = RC_SUCCESS; */
    return;
} /* end of HandleErr */
/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRc = RC_SUCCESS;

    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
            dbg(TRACE,"<%d> : msgsnd failed",pipErr);
            break;
    case    QUE_E_GET    :    /* Error using msgrcv */
            if(pipErr != 4)
             dbg(DEBUG,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :   unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        /*dbg(TRACE,"<%d> : no messages on queue",pipErr);*/
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    case    QUE_E_PRIORITY    :
        dbg(TRACE,"<%d> : wrong priority was send ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */

    return;
} /* end of HandleQueErr */
/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
  int    ilRc = RC_SUCCESS;            /* Return code */
  int    ilBreakOut = FALSE;

  do{
    memset(prgItem,0x00,igItemLen);
    ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
    /* depending on the size of the received item  */
    /* a realloc could be made by the que function */
    /* so do never forget to set event pointer !!! */
    prgEvent = (EVENT *) prgItem->text;
    if( ilRc == RC_SUCCESS )
      {
    /* Acknowledge the item */
    ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
    if( ilRc != RC_SUCCESS )
      {
        /* handle que_ack error */
        HandleQueErr(ilRc);
      } /* fi */

    switch( prgEvent->command )
      {
      case    HSB_STANDBY    :
        ctrl_sta = prgEvent->command;
        break;
      case    HSB_COMING_UP    :
        ctrl_sta = prgEvent->command;
        break;
      case    HSB_ACTIVE    :
        ctrl_sta = prgEvent->command;
        ilBreakOut = TRUE;
        break;
      case    HSB_ACT_TO_SBY    :
        ctrl_sta = prgEvent->command;
        break;
      case    HSB_DOWN    :
        /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
        ctrl_sta = prgEvent->command;
        Terminate(10);
        break;
      case    HSB_STANDALONE    :
        ctrl_sta = prgEvent->command;
        ResetDBCounter();
        ilBreakOut = TRUE;
        break;
      case    REMOTE_DB :
        /* ctrl_sta is checked inside */
        HandleRemoteDB(prgEvent);
        break;
      case    SHUTDOWN    :
        Terminate(1);
        break;
      case    RESET        :
        ilRc = Reset();
        break;
      case    EVENT_DATA    :
        dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
        DebugPrintItem(TRACE,prgItem);
        DebugPrintEvent(TRACE,prgEvent);
        break;
      case    TRACE_ON :
        dbg_handle_debug(prgEvent->command);
        break;
      case    TRACE_OFF :
        dbg_handle_debug(prgEvent->command);
        break;
      default            :
        dbg(TRACE,"HandleQueues: unknown event");
        DebugPrintItem(TRACE,prgItem);

        DebugPrintEvent(TRACE,prgEvent);

        break;
      } /* end switch */
      }else{
    /* Handle queuing errors */
    HandleQueErr(ilRc);
      } /* end else */
  } while (ilBreakOut == FALSE);
  if(igInitOK == FALSE)
    {
      ilRc = Init_foghdl();
      if(ilRc == RC_SUCCESS)
    {
      dbg(TRACE,"HandleQueues: Init_foghdl() OK!");
      igInitOK = TRUE;
    }else{ /* end of if */
      dbg(TRACE,"HandleQueues: Init_foghdl() failed!");
      igInitOK = FALSE;
    } /* end of if */
    }/* end of if */
  /* OpenConnection(); */
} /* end of HandleQueues */
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleInternalData()
{
  int  ilRC = RC_SUCCESS;      /* Return code */
  char *pclSelection = NULL;
  char *pclFields = NULL;
  char *pclData = NULL;
  char pclNewData[L_BUFF];
  char pclOldData[L_BUFF];
  char pclUrno[50];
  char *pclTmpPtr=NULL;
  BC_HEAD *bchd = NULL;          /* Broadcast header*/
  CMDBLK  *cmdblk = NULL;
  char pclTmpInitFog[32];
  char pclRegn[32];
  char pclStartTime[32];
  int ilLen;
  short que_out = 0;
  short que_prio = 0;
  short que_orig = 0;

  dbg(TRACE,"========================= HANDLE DATA START =========================");

  que_out = prgEvent->originator;
  que_prio = prgItem->priority;
  que_orig = prgItem->originator;

  dbg(TRACE,"MESSAGE FROM QUEUE %d ON PRIO %d (ORIGIN %d)",que_orig,que_prio,que_out);
  dbg(TRACE,"-----------------------------------------------");

  bchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  cmdblk= (CMDBLK  *) ((char *)bchd->data);

  strcpy(pcgTwStart,cmdblk->tw_start);
  strcpy(pcgTwEnd,cmdblk->tw_end);

  /***********************************/
  /*    DebugPrintItem(DEBUG,prgItem);   */
  /*    DebugPrintEvent(DEBUG,prgEvent); */
  /***********************************/


  strcpy(pclOldData,"");
  pclSelection = cmdblk->data;
  pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
  pclData = (char *)pclFields + strlen(pclFields) + 1;

  strcpy(pclNewData,pclData);
  pclTmpPtr = strstr(pclNewData,"\n");
  if (pclTmpPtr != NULL)
  {
     *pclTmpPtr = '\0';
     pclTmpPtr++;
     strcpy(pclOldData,pclTmpPtr);
  }
  *pclUrno = '\0';
  pclTmpPtr = strstr(pclSelection,"\n");
  if (pclTmpPtr != NULL)
  {
     *pclTmpPtr = '\0';
     pclTmpPtr++;
     strcpy(pclUrno,pclTmpPtr);
  }

  /* WorkStation */
  memset (pcgRecvName, '\0', (sizeof (bchd->recv_name) + 1));
  strncpy (pcgRecvName, bchd->recv_name, sizeof (bchd->recv_name));

  /* User */
  memset (pcgDestName, '\0', (sizeof (bchd->dest_name) + 1));
  strncpy (pcgDestName, bchd->dest_name, sizeof (bchd->dest_name));

  dbg(TRACE,"Command:   <%s>",cmdblk->command);
  dbg(TRACE,"Selection: <%s><%s>",pclSelection,pclUrno);
  dbg(TRACE,"Fields:    <%s>",pclFields);
  dbg(TRACE,"New Data:  <%s>",pclNewData);
  dbg(TRACE,"Old Data:  <%s>",pclOldData);
  dbg(TRACE,"TwStart:   <%s>",pcgTwStart);
  dbg(TRACE,"TwEnd:     <%s>",pcgTwEnd);
  dbg(TRACE,"RecvName:  <%s>",pcgRecvName);
  dbg(TRACE,"DestName:  <%s>",pcgDestName);

  ToolsSetSpoolerInfo(pcgDestName, pcgRecvName, pcgTwStart, pcgTwEnd);

  ilRC = TimeToStr(pcgCurrentTime,time(NULL));

  igAutoJoin = TRUE;
  igValidate = FALSE;
  igPrintReports = FALSE;

  if (strcmp(cmdblk->command,"FOGU") == 0)
  {
     ilRC = UpdateFOGTAB(pclUrno);
  }

  if (strcmp(cmdblk->command,"ROGU") == 0)
  {
     if (igWaitBeforeROGU > 0)
     {
        nap(igWaitBeforeROGU); /* Wait a little bit */
     }
     igSendRACBroadcast = TRUE;
     strcpy(pcgRACCommand,"RAC");
     strcpy(pcgRACBegin,"");
     strcpy(pcgRACEnd,"");
     strcpy(pcgRACDataList,"");
     ilRC = UpdateROGTAB(pclUrno);
     igSendRACBroadcast = FALSE;
  }

  if (strcmp(cmdblk->command,"FOGC") == 0)
  {
     ilRC = CheckFOGTAB();
  }

  if (strcmp(cmdblk->command,"FOGV") == 0)
  {
     ilRC = ValidateFOGTAB();
  }

  if (strcmp(cmdblk->command,"FOGI") == 0)
  {
     strcpy(pclTmpInitFog,pcgInitFog);
     strcpy(pcgInitFog,"YES");
     ilRC = InitFog();
     strcpy(pcgInitFog,pclTmpInitFog);
  }

  if (strcmp(cmdblk->command,"ROGI") == 0)
  {
     strcpy(pclTmpInitFog,pcgInitRog);
     strcpy(pcgInitRog,"YES");
     ilRC = InitRog();
     strcpy(pcgInitRog,pclTmpInitFog);
  }

  if (strcmp(cmdblk->command,"ROGX") == 0)
  {
     igAutoJoin = FALSE;
  }
  if (strcmp(cmdblk->command,"ROGR") == 0 || strcmp(cmdblk->command,"ROGX") == 0)
  {
     *pclRegn = '\0';
     *pclStartTime = '\0';
     BuildItemBuffer(pclSelection,"",2,",");
     ilLen = get_real_item(pclRegn,pclSelection,1);
     ilLen = get_real_item(pclStartTime,pclSelection,2);
     strcpy(pcgRACBegin,pclStartTime);
     ilLen = get_real_item(pcgRACEnd,pclSelection,3);
     TrimRight(pclRegn);
     TrimRight(pclStartTime);
     dbg(DEBUG,"Rebuild ROGTAB for Registration <%s> starting at <%s>",pclRegn,pclStartTime);
     if (*pclRegn != ' ' && *pclStartTime != ' ')
     {
        strcpy(pcgRACDataList,pclNewData);
        if (strlen(pcgRACDataList) > 0)
        {
           strcat(pcgRACDataList,",");
        }
        igSendRACBroadcast = TRUE;
        if (strcmp(cmdblk->command,"ROGR") == 0)
        {
           strcpy(pcgRACCommand,"RAC");
        }
        else
        {
           strcpy(pcgRACCommand,"RAX");
        }
        ilRC = BuildROGTAB(pclRegn,pclStartTime);
        ilRC = AppendUrnoList();
        strcpy(pcgRACDataList,"");
        igSendRACBroadcast = FALSE;
     }
     else
     {
        dbg(TRACE,"Invalid Registration <%s> or StartTime <%s>",pclRegn,pclStartTime);
     }
  }

  if (strcmp(cmdblk->command,"EXCO") == 0)
  {
     ilRC = TriggerBchdlAction(cmdblk->command,cmdblk->obj_name,pclSelection,pclFields,
                               pclNewData,TRUE,FALSE);
  }

  if (strcmp(cmdblk->command,"ACCE") == 0)
  {
     if (strlen(pcgUrnoList) > 0)
     {
        dbg(TRACE,"Send ACCE to %d , Urno List = \n<%s>",igSSIHDL,pcgUrnoList);
        (void) tools_send_info_flag(igSSIHDL,0,"roghdl","","CEDA","","",pcgTwStart,pcgTwEnd,
                                    "ACCE","",pcgUrnoList,"","",0);
        strcpy(pcgUrnoList,"");
     }
  }

  if (strcmp(cmdblk->command,"SIMU") == 0)
  {
     ilRC = HandleSimulation();
  }

  igAutoJoin = TRUE;

  if (strcmp(cmdblk->command,"CPST") == 0)
  {
    /* Command sent from Action */
    CheckPstReload(cmdblk->command, "PSTTAB", pclSelection, pclFields, pclData);
  }

  if (strcmp(cmdblk->command,"CGHP") == 0)
  {
    /* Command sent from Action */
    CheckGhpReload(cmdblk->command, "GHPTAB", pclSelection, pclFields, pclData);
  }

  if (strstr("CTOW,UTOW,DTOW",cmdblk->command) != NULL)
  {
    /* Commands sent from Flight (FltHdl) */
    CheckAutoTowTrigger(cmdblk->command, "AFTTAB", pclSelection, pclFields, pclData);
  }

  if (strcmp(cmdblk->command,"GOR") == 0)
  {
    ilRC = HandleGorTabTrigger(pclSelection, pclFields, pclData);
  }


  dbg(TRACE,"RELEASING SPOOLED EVENTS TO DESTINATION QUEUES");
  ToolsReleaseEventSpooler(TRUE, PRIORITY_3);

  if (igPrintReports == TRUE)
  {
    PrintReports("");
  }

  dbg(TRACE,"========================= HANDLE DATA END ===========================");
  return ilRC;
} /* end of HandleInternalData */

static void CheckAutoTowTrigger(char *pcpCmd, char *pcpTbl, char *pcpSel, char *pcpFld, char *pcpDat)
{
  int ilRc = RC_SUCCESS;
  long llLen = 0;
  char *pclMsgPtr = NULL;
  char pclMsgType[64] = "";
  char pclAreaBgn[64] = "";
  char pclAreaEnd[64] = "";
  char pclAreaMsg[4096] = "";
  char pclMsgData[4096] = "";
  char pclMsgKeys[1024] = "";
  char pclFldData[1024] = "";
  char pcgTrgMsgRkey[16] = "";
  char pcgTrgMsgUrno[16] = "";
  char pcgTrgTifaOld[16] = "";
  char pcgTrgPstaOld[16] = "";
  char pcgTrgTifdOld[16] = "";
  char pcgTrgPstdOld[16] = "";
  char pcgTrgTifaNew[16] = "";
  char pcgTrgPstaNew[16] = "";
  char pcgTrgTifdNew[16] = "";
  char pcgTrgPstdNew[16] = "";

  ToolsSetSpoolerInfo("AutoTowing", "CEDA", pcgTwStart, pcgTwEnd);

  igTriggerIsFullRkeyCheck = FALSE;

  igTriggerIsArrPstaUpdate = FALSE;
  igTriggerIsDepPstdUpdate = FALSE;
  igTriggerIsArrTifaUpdate = FALSE;
  igTriggerIsDepTifdUpdate = FALSE;

  pclMsgPtr = pcpFld;
  strcpy(pclAreaMsg,"");
  sprintf(pclAreaBgn,"{INFO:TYPE[");
  sprintf(pclAreaEnd,"]}",pclMsgType);
  CedaGetKeyItem(pclMsgType,&llLen,pclMsgPtr,pclAreaBgn,pclAreaEnd, TRUE);
  dbg(TRACE,"FLIGHT MESSAGE TYPE IS <%s>",pclMsgType);
  sprintf(pclAreaBgn,"{INFO:TYPE[%s]}",pclMsgType);
  sprintf(pclAreaEnd,"{TYPE[%s]INFO}",pclMsgType);
  CedaGetKeyItem(pclAreaMsg,&llLen,pclMsgPtr,pclAreaBgn,pclAreaEnd, TRUE);
  dbg(TRACE,"MESSAGE CONTENT <%s>",pclAreaMsg);

  if (strcmp(pcpCmd,"CTOW") == 0)
  {
    igTriggerIsFullRkeyCheck = TRUE;
    dbg(TRACE,"COMMAND <%s> (CHECK TOWINGS) RECEIVED FROM FLIGHT HANDLER",pcpCmd);
    CheckTowings(pcpCmd, pcpTbl, pcpSel, pclAreaMsg, pcpDat);
    igPrintReports = TRUE;
  }

  if (strcmp(pcpCmd,"UTOW") == 0)
  {
    dbg(TRACE,"TRIGGER <%s> (UPDATED FLIGHTS) RECEIVED FROM FLIGHT HANDLER",pcpCmd);
    pclMsgPtr = pcpFld;
    while (strstr(pclMsgPtr,"RKEY|URNO<") != NULL)
    {
      igTriggerIsArrPstaUpdate = FALSE;
      igTriggerIsDepPstdUpdate = FALSE;
      igTriggerIsArrTifaUpdate = FALSE;
      igTriggerIsDepTifdUpdate = FALSE;
      CedaGetKeyItem(pclMsgKeys,&llLen,pclMsgPtr,"RKEY|URNO<",">RKEY|URNO", TRUE);
      dbg(TRACE,"FLIGHT TRIGGER KEYS <%s>",pclMsgKeys);
      get_any_item(pcgTrgMsgRkey, pclMsgKeys, 1, "|");
      get_any_item(pcgTrgMsgUrno, pclMsgKeys, 2, "|");
      CedaGetKeyItem(pclMsgData,&llLen,pclMsgPtr,"RKEY|URNO:",":RKEY|URNO", TRUE);
      dbg(TRACE,"FLIGHT TRIGGER DATA <%s>",pclMsgData);
      if (strstr(pclMsgData,"TIFA") != NULL)
      {
        igTriggerIsArrTifaUpdate = TRUE;
        CedaGetKeyItem(pclFldData,&llLen,pclMsgPtr,"TIFA<",">TIFA", TRUE);
        dbg(TRACE,"FLIGHT TRIGGER TIFA <%s>",pclFldData);
        get_any_item(pcgTrgTifaOld, pclFldData, 1, "|");
        get_any_item(pcgTrgTifaNew, pclFldData, 2, "|");
      }
      dbg(TRACE,">> TRIGGER MESSAGES ARE NOT YET HANDLED");
      /*
      CheckTowings(pcpCmd, pcpTbl, pcpSel, pcpFld, pcgTrgMsgRkey);
      igPrintReports = TRUE;
      */
      pclMsgPtr++;
    }
  }

  if (strcmp(pcpCmd,"DTOW") == 0)
  {
    dbg(TRACE,"TRIGGER <%s> (DELETION OF TOWINGS) RECEIVED FROM FLIGHT HANDLER",pcpCmd);
    /*
    CheckTowings(pcpCmd, pcpTbl, pcpSel, pcpFld, pcpDat);
    igPrintReports = TRUE;
    */
  }


  return;
}


/*********************************************************************
Function : SendEvent()
Paramter : IN: pcpCmd = command for cmdblk->command
           IN: ipModID = process-ID where the event is send to
       IN: ipPriority = priority for sending ( 1- 5, usuallay 3)
       IN: pcpTable = Name (3 letters) of cmdblk->obj_name (if
           necessary), will be expanded with "pcgTabEnd".
       IN: pcpTwStart = cmdblk->twstart
       IN: pcpTwEnd = cmdblk->twend (always HOMEAP,TABEND,processname)
       IN: pcpSelection = selection for event (cmdblk->data)
       IN: pcpFields = fieldlist (corresponding to pcpdata)
       IN: pcpData = datalist (comma separated, corresponding to
                                   pcpFields)
       IN: pcpAddStruct = additional structure to be transmitted
       IN: ipAddStructSize = size of the additional structure
Return Code: RC_SUCCESS, RC_FAIL
Result:
Description: Sends an event to another CEDA-process using que(QUE_PUT).
             Sends the event in standard CEDA-format (BCHEAD,CMDBLK,
         selection,fieldlist,datalist) or sends a different
         data structure (special) at CMDBLK->data. !! Sends always
         only one type, standard OR special, with one event !!
*********************************************************************/
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
             char *pcpTwStart, char *pcpTwEnd,
             char *pcpSelection,char *pcpFields,char *pcpData,
             char *pcpAddStruct,int ipAddStructSize)
{
  int     ilRc             = RC_FAIL;
  int     ilLen            = 0;
  EVENT   *prlOutEvent  = NULL;
  BC_HEAD *prlOutBCHead = NULL;
  CMDBLK  *prlOutCmdblk = NULL;

  if (pcpAddStruct == NULL)
    ipAddStructSize = 0;
  if (ipAddStructSize == 0)
    pcpAddStruct = NULL;

  /* size-calculation for prlOutEvent */
  ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) +
    strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) +
    ipAddStructSize + 128;

  /* memory for prlOutEvent */
  if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
    {
      dbg(TRACE,"SendEvent: cannot malloc <%d>-bytes for outgoing event!",ilLen);
      prlOutEvent = NULL;
    }else{
      /* clear whole outgoing event */
      memset((void*)prlOutEvent, 0x00, ilLen);

      /* set event structure... */
      prlOutEvent->type         = SYS_EVENT;
      prlOutEvent->command    = EVENT_DATA;
      prlOutEvent->originator   = (short)mod_id;
      prlOutEvent->retry_count  = 0;
      prlOutEvent->data_offset  = sizeof(EVENT);
      prlOutEvent->data_length  = ilLen - sizeof(EVENT);

      /* BC_HEAD-Structure... */
      prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
      /* prlOutBCHead->rc = (short)RC_SUCCESS;*/
      prlOutBCHead->rc = (short)NETOUT_NO_ACK;/*spaeter nur bei disconnect*/
      strncpy(prlOutBCHead->dest_name,mod_name,10);
      strncpy(prlOutBCHead->recv_name, "EXCO",10);

      /* Cmdblk-Structure... */
      prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
      strcpy(prlOutCmdblk->command,pcpCmd);
      if (pcpTable != NULL)
    {
      strcpy(prlOutCmdblk->obj_name,pcpTable);
      strcat(prlOutCmdblk->obj_name,pcgTabEnd);
    }

      /* setting tw_x entries */
      strncpy(prlOutCmdblk->tw_start,pcpTwStart,32);
      strncpy(prlOutCmdblk->tw_end,pcpTwEnd,32);

      /* means that no additional structure is used */
      /* STANDARD CEDA-ipcs between CEDA-processes */
      if (pcpAddStruct == NULL)
    {
      /* setting selection inside event */
      strcpy(prlOutCmdblk->data,pcpSelection);
      /* setting field-list inside event */
      strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1,pcpFields);
      /* setting data-list inside event */
      strcpy((prlOutCmdblk->data + (strlen(pcpSelection)+1) + (strlen(pcpFields)+1)),pcpData);
    }else{
      /*an additional structure is used and will be copied to */
      /*cmdblk + sizeof(CMDBLK).!!! No STANDARD CEDA-ipcs is used !!!! */
      memcpy(prlOutCmdblk->data,(char*)pcpAddStruct,ipAddStructSize);
    }

      /*DebugPrintEvent(DEBUG,prlOutEvent);*/
      /*snapit((char*)prlOutEvent,ilLen,outp);*/
      dbg(DEBUG,"SendEvent: sending event to mod_id <%d>",ipModID);

      if (ipModID != 0)
    {
      if ((ilRc = que(QUE_PUT,ipModID,mod_id,ipPriority,ilLen,(char*)prlOutEvent))
          != RC_SUCCESS)
        {
          dbg(TRACE,"SendEvent: QUE_PUT returns: <%d>", ilRc);
          Terminate(1);
        }
    }else{
      dbg(TRACE,"SendEvent: mod_id = <%d>! Can't send!",ipModID);
    }
      /* free memory */
      free((void*)prlOutEvent);
    }
  return ilRc;
}

/*
	Get Config Entries
*/
static int GetConfig()
{
  int ilRC = RC_SUCCESS;
  int ilI;
  char pclTmpBuf[128];
  int ilLen;

  sprintf(pcgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
  dbg(TRACE,"Config File is <%s>",pcgConfigFile);
  sprintf(pcgUfisToUfisConfigFile,"%s/UfisToUfis.cfg",getenv("CFG_PATH"));
  dbg(TRACE,"UfisToUfisConfig File is <%s>",pcgUfisToUfisConfigFile);

  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","PABS_ARR",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igPabsArr = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","PAES_ARR_S",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igPaesArr = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","PAES_ARR_L",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgPaesArr,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","PABS_ROT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igPabsRot = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","PAES_ROT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igPaesRot = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","PDBS_DEP",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igPdbsDep = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","PDES_DEP",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igPdesDep = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","PDBS_ROT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igPdbsRot = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","PDES_ROT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igPdesRot = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","ROT_TIME_ARR_PERC",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igRotTimeArrPerc = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","INIT_HIS_START",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgInitHisStart,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","LAST_DAY_FOR_INIT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgLastDayForInit,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","ON_GROUND_TIME_LIMIT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igOnGroundTimeLimit = atoi(pcgCfgBuffer);
     igOnGroundTimeLimit *= 60;
  }


  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","CHECK_ON_GROUND_TIME_LIMIT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgCheckOnGroundTimeLimit,pcgCfgBuffer);
  }

  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","TOLERANCE",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igTolerance = atoi(pcgCfgBuffer);
     igTolerance *= 60;
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","INIT_FOG",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgInitFog,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","EXCL_FTYP",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     memset(pcgExclFtyp,0x00,512);
     for (ilI = 0; ilI < strlen(pcgCfgBuffer); ilI += 2)
     {
        strcat(pcgExclFtyp,"'");
        pcgExclFtyp[strlen(pcgExclFtyp)] = pcgCfgBuffer[ilI];
        strcat(pcgExclFtyp,"',");
     }
     pcgExclFtyp[strlen(pcgExclFtyp)-1] = '\0';
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","ROG_BEGIN",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgRogBegin,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","ROG_END",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgRogEnd,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","INIT_ROG",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgInitRog,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","TRIGGER_ACTION",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgTriggerAction,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","TRIGGER_BCHDL",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgTriggerBchdl,pcgCfgBuffer);
  }

  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","TRIGGER_AFTBCHDL",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgTriggerAFTBchdl,pcgCfgBuffer);
  }


  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","BC_OUT_MODE",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igBcOutMode = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","DEBUG_LEVEL",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgDebugLevel,pcgCfgBuffer);
  }
  if (strcmp(pcgDebugLevel,"DEBUG") == 0)
  {
     debug_level = DEBUG;
  }
  else
  {
     if (strcmp(pcgDebugLevel,"TRACE") == 0)
     {
        debug_level = TRACE;
     }
     else
     {
        if (strcmp(pcgDebugLevel,"NULL") == 0)
        {
           debug_level = 0;
        }
        else
        {
           debug_level = TRACE;
        }
     }
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","DEBUG_LEVEL_INIT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgDebugLevelInit,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","SSIHDL",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igSSIHDL = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","WAIT_BEFORE_ROGU",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igWaitBeforeROGU = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgUfisToUfisConfigFile,"CEI",mod_name,CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     ilLen = get_real_item(pclTmpBuf,pcgCfgBuffer,2);
     igCedaEventInterface = atoi(pclTmpBuf);
     ilLen = get_real_item(pclTmpBuf,pcgCfgBuffer,3);
     igRemoteTarget = atoi(pclTmpBuf);
     strcpy(pcgRACTargets,pcgCfgBuffer);
  }
  else
  {
     igCedaEventInterface = -1;
     ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","REMOTE_TARGET_FOR_RAC",CFG_STRING,pcgCfgBuffer);
     if (ilRC == RC_SUCCESS)
     {
        igRemoteTarget = atoi(pcgCfgBuffer);
        strcpy(pcgRACTargets,pcgCfgBuffer);
     }
     else
     {
        igRemoteTarget = -1;
        strcpy(pcgRACTargets,"-");
     }
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","USE_ARRAY_INSERT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pcgCfgBuffer,"YES") == 0)
     {
        igUseArrayInsert = TRUE;
     }
  }

  /* UFIS-1265 */
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","QUE_TO_FOGDHDL",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igToFogHdl = atoi(pcgCfgBuffer);
  }

  /*UFIS-2032*/
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","ROG_TIMEEND",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igRogTimeEnd = atoi(pcgCfgBuffer);
  }


  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","SELECT_VERS",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS) {
     ilSelectVers = atoi(pcgCfgBuffer);
  }

  dbg(TRACE,"PABS_ARR = %d",igPabsArr);
  dbg(TRACE,"PAES_ARR_S = %d",igPaesArr);
  dbg(TRACE,"PAES_ARR_L = %s",pcgPaesArr);
  dbg(TRACE,"PABS_ROT = %d",igPabsRot);
  dbg(TRACE,"PAES_ROT = %d",igPaesRot);
  dbg(TRACE,"PDBS_DEP = %d",igPdbsDep);
  dbg(TRACE,"PDES_DEP = %d",igPdesDep);
  dbg(TRACE,"PDBS_ROT = %d",igPdbsRot);
  dbg(TRACE,"PDES_ROT = %d",igPdesRot);
  dbg(TRACE,"ROT_TIME_ARR_PERC = %d",igRotTimeArrPerc);
  dbg(TRACE,"INIT_HIS_START = %s",pcgInitHisStart);
  dbg(TRACE,"LAST_DAY_FOR_INIT = %s",pcgLastDayForInit);
  dbg(TRACE,"ON_GROUND_TIME_LIMIT = %d",igOnGroundTimeLimit/60);
  dbg(TRACE,"CHECK_ON_GROUND_TIME_LIMIT = %s",pcgCheckOnGroundTimeLimit);
  dbg(TRACE,"TOLERANCE = %d",igTolerance/60);
  dbg(TRACE,"INIT_FOG = %s",pcgInitFog);
  dbg(TRACE,"EXCL_FTYP = %s",pcgExclFtyp);
  dbg(TRACE,"ROG_BEGIN = %s",pcgRogBegin);
  dbg(TRACE,"ROG_END = %s",pcgRogEnd);
  dbg(TRACE,"INIT_ROG = %s",pcgInitRog);
  dbg(TRACE,"SSIHDL = %d",igSSIHDL);
  /* UFIS-1265 */
  dbg(TRACE,"FOGDHDL = %d",igToFogHdl);
  dbg(TRACE,"ID Ceda Event Interface = %d",igCedaEventInterface);
  dbg(TRACE,"ID Remote Target for RAC = %d",igRemoteTarget);
  dbg(TRACE,"ID List: Targets for RAC = %s",pcgRACTargets);
  dbg(TRACE,"Wait before ROGU = %d",igWaitBeforeROGU);
  dbg(TRACE,"Use Array Insert = %d",igUseArrayInsert);
  dbg(TRACE,"TRIGGER_ACTION = %s",pcgTriggerAction);
  dbg(TRACE,"TRIGGER_BCHDL = %s",pcgTriggerBchdl);
  dbg(TRACE,"TRIGGER_AFTBCHDL = %s",pcgTriggerAFTBchdl);
  dbg(TRACE,"BROADCAST OUTPUT MODE = %d",igBcOutMode);
  dbg(TRACE,"DEBUG_LEVEL = %s",pcgDebugLevel);
  dbg(TRACE,"DEBUG_LEVEL_INIT = %s",pcgDebugLevelInit);
  /*UFIS-2032*/
  dbg(TRACE,"ROG_TIMEEND = %d",igRogTimeEnd);
  dbg(TRACE,"SELECT_VERS = %d",ilSelectVers);

  InitAutoTowing();

  return RC_SUCCESS;
} /* Enf of GetConfig */

/*******************************************************/
/*******************************************************/
static int InitAutoTowing(void)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  dbg(TRACE,"================================================");
  dbg(TRACE,"CHECK THE CONFIGURATION OF AUTO_TOWING FUNCTIONS");
  ilGetRc = GetCfg ("AUTO_TOWING", "TOWHDL_QUEUE_NBR", CFG_STRING, pcgTmpBuf1, "-1");
  igToTowHdl = atoi(pcgTmpBuf1);
  if (igToTowHdl<1000)
  {
    igToTowHdl = 0;
  }
  if ((igToTowHdl > 0)&&(igToTowHdl == mod_id))
  {
    dbg(TRACE,"AUTO_TOWING ACTIVATED ON QUEUE %d: THAT'S ME !",igToTowHdl);
    dbg(TRACE,"================================================");
    igIAmTowHdl = TRUE;

    ilGetRc = GetCfg ("AUTO_TOWING", "USING_GORTAB_POS", CFG_STRING, pcgTmpBuf1, "YES");
    if (strcmp (pcgTmpBuf1, "YES") == 0)
    {
      igUseGorTabData = TRUE;
      dbg(TRACE,"USING GORTAB FOR STAND ALLOCATION (RTYP=POS)");
      dbg(TRACE,"--------------------------------------------");
    }
    else
    {
      igUseGorTabData = FALSE;
      dbg(TRACE,"GORTAB IS NOT YET READY FOR USE");
      dbg(TRACE,"-------------------------------");
    }


    ilGetRc = GetCfg ("AUTO_TOWING", "HIDE_DELETED_TOW", CFG_STRING, pcgTmpBuf1, "YES");
    if (strcmp (pcgTmpBuf1, "YES") == 0)
    {
      igHideDelTow = TRUE;
    }
    else
    {
      igHideDelTow = FALSE;
    }

    ilGetRc = GetCfg ("AUTO_TOWING", "SKIP_OFBL_FLIGHT", CFG_STRING, pcgTmpBuf1, "YES");
    if (strcmp (pcgTmpBuf1, "YES") == 0)
    {
      igSkipOfblFlights = TRUE;
    }
    else
    {
      igSkipOfblFlights = FALSE;
    }

    ilGetRc = GetCfg ("AUTO_TOWING", "SKIP_SKED_FLIGHT", CFG_STRING, pcgTmpBuf1, "YES");
    if (strcmp (pcgTmpBuf1, "YES") == 0)
    {
      igSkipSkedFlights = TRUE;
    }
    else
    {
      igSkipSkedFlights = FALSE;
    }

    ilGetRc = GetCfg ("AUTO_TOWING", "SKIP_PABA_FLIGHT", CFG_STRING, pcgTmpBuf1, "YES");
    if (strcmp (pcgTmpBuf1, "YES") == 0)
    {
      igSkipPabaFlights = TRUE;
    }
    else
    {
      igSkipPabaFlights = FALSE;
    }

    ilGetRc = GetCfg ("AUTO_TOWING", "TOW_EMPTY_STANDS", CFG_STRING, pcgTmpBuf1, "YES");
    if (strcmp (pcgTmpBuf1, "YES") == 0)
    {
      igTowEmptyStands = TRUE;
    }
    else
    {
      igTowEmptyStands = FALSE;
    }

    ilGetRc = GetCfg ("AUTO_TOWING", "AUTO_REPAIR_AURN", CFG_STRING, pcgTmpBuf1, "YES");
    if (strcmp (pcgTmpBuf1, "YES") == 0)
    {
      igAutoRepairAurn = TRUE;
    }
    else
    {
      igAutoRepairAurn = FALSE;
    }

    ilGetRc = GetCfg ("AUTO_TOWING", "MAX_OVN_GRD_TIME", CFG_STRING, pcgTmpBuf1, "1440");
    igMaxOvnTime = atoi(pcgTmpBuf1);
    ilGetRc = GetCfg ("AUTO_TOWING", "LOC_OVN_GRD_HOUR", CFG_STRING, pcgTowOvnHour, "0300");
    ilGetRc = GetCfg ("AUTO_TOWING", "LOC_OVN_ARR_TIME", CFG_STRING, pcgTowArrTime, "2100");
    ilGetRc = GetCfg ("AUTO_TOWING", "LOC_OVN_DEP_TIME", CFG_STRING, pcgTowDepTime, "0800");
    ilGetRc = GetCfg ("AUTO_TOWING", "DEF_REMOTE_STAND", CFG_STRING, pcgTowOutPos, " ");
    if (strlen(pcgTowOutPos) == 0)
    {
      strcpy(pcgTowOutPos," ");
    }

    ilGetRc = GetCfg ("AUTO_TOWING", "TIME_FOR_TOWINGS", CFG_STRING, pcgTmpBuf1, "10");
    igTowingTime = atoi(pcgTmpBuf1);
    if (igTowingTime < 0)
    {
      igTowingTime = 10;
    }

    ilGetRc = GetCfg ("AUTO_TOWING", "MIN_PARKING_TIME", CFG_STRING, pcgTmpBuf1, "10");
    igMinPosTime = atoi(pcgTmpBuf1);
    if (igMinPosTime < 0)
    {
      igMinPosTime = 10;
    }

    ilGetRc = GetCfg ("AUTO_TOWING", "TEST_TOWOUT_TIME", CFG_STRING, pcgTmpBuf1, "0");
    igTestTowOutTime = atoi(pcgTmpBuf1);
    if (igTestTowOutTime < 0)
    {
      igTestTowOutTime = 0;
    }

    ilGetRc = GetCfg ("AUTO_TOWING", "AUTO_GLUE_TOWOUT", CFG_STRING, pcgTmpBuf1, "NO");
    if (strcmp (pcgTmpBuf1, "YES") == 0)
    {
      igAutoGlueTowOut = TRUE;
    }
    else
    {
      igAutoGlueTowOut = FALSE;
    }

    ilGetRc = GetCfg ("AUTO_TOWING", "AUTO_GLUE_TOW_IN", CFG_STRING, pcgTmpBuf1, "NO");
    if (strcmp (pcgTmpBuf1, "YES") == 0)
    {
      igAutoGlueTowIn = TRUE;
    }
    else
    {
      igAutoGlueTowIn = FALSE;
    }

    ilGetRc = GetCfg ("AUTO_TOWING", "AUTO_GLUE_TOWING", CFG_STRING, pcgTmpBuf1, "NO");
    if (strcmp (pcgTmpBuf1, "YES") == 0)
    {
      igAutoGlueTowing = TRUE;
    }
    else
    {
      igAutoGlueTowing = FALSE;
    }

    ilGetRc = GetCfg ("AUTO_TOWING", "RULE_TOW_IN_TIME", CFG_STRING, pcgTmpBuf1, "0");
    igRuleTowInTime = atoi(pcgTmpBuf1);
    if (igRuleTowInTime < 0)
    {
      igRuleTowInTime = 0;
    }

    ilGetRc = GetCfg ("AUTO_TOWING", "SKIP_A_RULE_TIME", CFG_STRING, pcgTmpBuf1, "0");
    igSkipARuleTime = atoi(pcgTmpBuf1);
    if (igSkipARuleTime < 0)
    {
      igSkipARuleTime = 0;
    }

    ilGetRc = GetCfg ("AUTO_TOWING", "SKIP_D_RULE_TIME", CFG_STRING, pcgTmpBuf1, "0");
    igSkipDRuleTime = atoi(pcgTmpBuf1);
    if (igSkipDRuleTime < 0)
    {
      igSkipDRuleTime = 0;
    }

    ilGetRc = GetCfg ("AUTO_TOWING", "SKIP_T_RULE_TIME", CFG_STRING, pcgTmpBuf1, "0");
    igSkipTRuleTime = atoi(pcgTmpBuf1);
    if (igSkipTRuleTime < 0)
    {
      igSkipTRuleTime = 0;
    }

    PublishTowhdlConfig();

  }
  else if (igToTowHdl > 0)
  {
    dbg(TRACE,"AUTO_TOWING ACTIVATED ON QUEUE %d: THAT'S NOT ME!",igToTowHdl);
    dbg(TRACE,"====================================================");
    igIAmTowHdl = FALSE;
  }
  else
  {
    dbg(TRACE,"AUTO_TOWING NOT ACTIVATED (TOWHDL_QUEUE_NBR = %s)",pcgTmpBuf1);
    dbg(TRACE,"============= ===================================");
    igIAmTowHdl = FALSE;
  }


  return ilRC;
}

/*******************************************************/
/*******************************************************/
static void PublishTowhdlConfig(void)
{
  if (igSkipOfblFlights == TRUE)
  {
    dbg(TRACE,"[AUTO_TOWING] SKIP_OFBL_FLIGHT = YES");
  }
  else
  {
    dbg(TRACE,"[AUTO_TOWING] SKIP_OFBL_FLIGHT = NO");
  }
  if (igSkipSkedFlights == TRUE)
  {
    dbg(TRACE,"[AUTO_TOWING] SKIP_SKED_FLIGHT = YES");
  }
  else
  {
    dbg(TRACE,"[AUTO_TOWING] SKIP_SKED_FLIGHT = NO");
  }
  if (igSkipPabaFlights == TRUE)
  {
    dbg(TRACE,"[AUTO_TOWING] SKIP_PABA_FLIGHT = YES");
  }
  else
  {
    dbg(TRACE,"[AUTO_TOWING] SKIP_PABA_FLIGHT = NO");
  }
  if (igTowEmptyStands == TRUE)
  {
    dbg(TRACE,"[AUTO_TOWING] TOW_EMPTY_STANDS = YES");
  }
  else
  {
    dbg(TRACE,"[AUTO_TOWING] TOW_EMPTY_STANDS = NO");
  }
  if (igAutoRepairAurn == TRUE)
  {
    dbg(TRACE,"[AUTO_TOWING] AUTO_REPAIR_AURN = YES");
  }
  else
  {
    dbg(TRACE,"[AUTO_TOWING] AUTO_REPAIR_AURN = NO");
  }
  if (igHideDelTow == TRUE)
  {
    dbg(TRACE,"[AUTO_TOWING] HIDE_DELETED_TOW = YES");
  }
  else
  {
    dbg(TRACE,"[AUTO_TOWING] HIDE_DELETED_TOW = NO");
  }



  dbg(TRACE,"[AUTO_TOWING] MAX_OVN_GRD_TIME = %d",igMaxOvnTime);
  dbg(TRACE,"[AUTO_TOWING] LOC_OVN_ARR_TIME = %s",pcgTowArrTime);
  dbg(TRACE,"[AUTO_TOWING] LOC_OVN_GRD_HOUR = %s",pcgTowOvnHour);
  dbg(TRACE,"[AUTO_TOWING] LOC_OVN_DEP_TIME = %s",pcgTowDepTime);
  dbg(TRACE,"[AUTO_TOWING] TIME_FOR_TOWINGS = %d",igTowingTime);
  dbg(TRACE,"[AUTO_TOWING] MIN_PARKING_TIME = %d",igMinPosTime);
  dbg(TRACE,"[AUTO_TOWING] DEF_REMOTE_STAND = %s",pcgTowOutPos);


  if (igAutoGlueTowOut == TRUE)
  {
    dbg(TRACE,"[AUTO_TOWING] AUTO_GLUE_TOWOUT = YES");
  }
  else
  {
    dbg(TRACE,"[AUTO_TOWING] AUTO_GLUE_TOWOUT = NO");
  }
  if (igAutoGlueTowIn == TRUE)
  {
    dbg(TRACE,"[AUTO_TOWING] AUTO_GLUE_TOW_IN = YES");
  }
  else
  {
    dbg(TRACE,"[AUTO_TOWING] AUTO_GLUE_TOW_IN = NO");
  }
  if (igAutoGlueTowing == TRUE)
  {
    dbg(TRACE,"[AUTO_TOWING] AUTO_GLUE_TOWING = YES");
  }
  else
  {
    dbg(TRACE,"[AUTO_TOWING] AUTO_GLUE_TOWING = NO");
  }

  dbg(TRACE,"[AUTO_TOWING] TEST_TOWOUT_TIME = %d",igTestTowOutTime);
  dbg(TRACE,"[AUTO_TOWING] RULE_TOW_IN_TIME = %d",igRuleTowInTime);
  dbg(TRACE,"[AUTO_TOWING] SKIP_T_RULE_TIME = %d",igSkipTRuleTime);
  dbg(TRACE,"[AUTO_TOWING] SKIP_A_RULE_TIME = %d",igSkipARuleTime);
  dbg(TRACE,"[AUTO_TOWING] SKIP_D_RULE_TIME = %d",igSkipDRuleTime);

  return;
}

/*******************************************************/
/*******************************************************/
static int GetCfg (char *pcpGroup, char *pcpLine, short spNoOfLines, char *pcpDestBuff, char *pcpDefault)
{
  int ilRC = RC_SUCCESS;

  pcpDestBuff[0] = 0x00;
  ilRC = iGetConfigRow (pcgConfigFile, pcpGroup, pcpLine, spNoOfLines, pcpDestBuff);
  if (ilRC != RC_SUCCESS)
  {
    /* use default entry */
    strcpy (pcpDestBuff, pcpDefault);
  }                             /* end if */
  return ilRC;
}                               /* end GetCfg */

/******************************************************************************/
/* The TimeToStr routine                                                      */
/******************************************************************************/
static int TimeToStr(char *pcpTime,time_t lpTime)
{
  struct tm *_tm;

  _tm = (struct tm *)gmtime(&lpTime);

  sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
          _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
          _tm->tm_min,_tm->tm_sec);

  return RC_SUCCESS;

}     /* end of TimeToStr */


static int InitFog()
{
  int ilRC = RC_SUCCESS;
  int ilI;
  char pclCurrentTimeEnd[32];
  char pclWorkDay[32];
  char pclLastDay[32];
  int ilSavDebugLevel;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclDataBuf[L_BUFF];

  dbg(TRACE,"================ Now Start Initializing FOGTAB ================");
  if (strcmp(pcgInitFog,"YES") == 0)
  {
     ilSavDebugLevel = debug_level;
     if (strcmp(pcgDebugLevelInit,"DEBUG") == 0)
     {
        debug_level = DEBUG;
     }
     else
     {
        debug_level = TRACE;
     }
     dbg(DEBUG,"Current Time = <%s>",pcgCurrentTime);

     sprintf(pclSqlBuf,"DELETE FROM FOGTAB");
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"<%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     commit_work();
     close_my_cursor(&slCursor);

     strcpy(pclLastDay,pcgLastDayForInit);
     strcpy(pclWorkDay,pcgInitHisStart);
     while (strcmp(pclWorkDay,pclLastDay) <= 0)
     {
        ilRC = BuildGroundTimes(pclWorkDay);
        strcat(pclWorkDay,"120000");
        ilRC = MyAddSecondsToCEDATime(pclWorkDay,24 * 60 * 60,1);
        pclWorkDay[8] = '\0';
     }

     dbg(TRACE," ");
     dbg(TRACE,"Start: %s",pcgCurrentTime);
     ilRC = TimeToStr(pclCurrentTimeEnd,time(NULL));
     dbg(TRACE,"End:   %s",pclCurrentTimeEnd);
     debug_level = ilSavDebugLevel;
  }

  dbg(TRACE,"================ Initializing FOGTAB Finished =================");

  return RC_SUCCESS;
} /* End of InitFog */


static int BuildGroundTimes(char *pclWorkDay)
{
  int ilRC = RC_SUCCESS;

  dbg(TRACE," ");
  dbg(TRACE,"Build Ground Times for Day <%s>",pclWorkDay);

  ilRC = BuildGroundTimesArrivals(pclWorkDay);
  ilRC = BuildGroundTimesDepartures(pclWorkDay);
  ilRC = BuildGroundTimesRotations(pclWorkDay);

  return ilRC;
} /* End of BuildGroundTimes */


static int BuildGroundTimesArrivals(char *pclWorkDay)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFktRd1;
  short slCursorRd1;
  short slFktWr;
  short slCursorWr;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  char pclUpdSqlBuf[L_BUFF];
  char pclUpdSelectBuf[L_BUFF];
  char pclUpdTmpBuf[L_BUFF];
  char pclTmpBuf[XS_BUFF];
  char pclUrno[32];
  char pclTifa[32];
  char pclTifd[32];
  char pclOnbl[32];
  char pclRkey[32];
  char pclFtyp[32];
  char pclNewPabs[32];
  char pclNewPaes[32];
  char pclNewPdbs[32];
  char pclNewPdes[32];
  int ilLen;
  int ilTotRecs = 0;
  int ilInsRecs = 0;
  char pclFieldList[L_BUFF];
  char pclDataList[L_BUFF];

  strcpy(pclSqlBuf,"SELECT URNO,TIFA,TIFD,ONBL,RKEY,FTYP FROM AFTTAB ");
  sprintf(pclSelectBuf,"WHERE TIFA LIKE '%s%%' AND DES3 = '%s' AND (RTYP = 'S' OR RTYP = ' ') AND ADID = 'A'",
                       pclWorkDay,pcgHomeAp);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursorRd1 = 0;
  slFktRd1 = START;
  ilRCdb = sql_if(slFktRd1,&slCursorRd1,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     ilTotRecs++;
     BuildItemBuffer(pclDataBuf,"",6,",");
     ilLen = get_real_item(pclUrno,pclDataBuf,1);
     ilLen = get_real_item(pclTifa,pclDataBuf,2);
     ilLen = get_real_item(pclTifd,pclDataBuf,3);
     ilLen = get_real_item(pclOnbl,pclDataBuf,4);
     ilLen = get_real_item(pclRkey,pclDataBuf,5);
     ilLen = get_real_item(pclFtyp,pclDataBuf,6);
     ilRC = CalculateGroundTimes(pclTifa,NULL,NULL,NULL,pclOnbl,NULL,NULL,NULL,"A",1,1,
                                 pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
     sprintf(pclUpdTmpBuf,"PABS='%s',PAES='%s',PDBS='%s',PDES='%s'",
             pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
     strcpy(pclFieldList,"PABS,PAES,PDBS,PDES,TIFA,TIFD,ADID,RKEY,FTYP,URNO");
     sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes,
             pclTifa,pclTifd,"A",pclRkey,pclFtyp,pclUrno);
     sprintf(pclUpdSqlBuf,"UPDATE AFTTAB SET %s ",pclUpdTmpBuf);
     sprintf(pclUpdSelectBuf,"WHERE URNO = %s",pclUrno);
     strcat(pclUpdSqlBuf,pclUpdSelectBuf);
     slCursorWr = 0;
     slFktWr = START;
     dbg(DEBUG,"<%s>",pclUpdSqlBuf);
     ilRCdb = sql_if(slFktWr,&slCursorWr,pclUpdSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
        ilRC = TriggerBchdlAction("UFR","AFTTAB",pclUrno,pclFieldList,
                                  pclDataList,FALSE,FALSE);
     }
     close_my_cursor(&slCursorWr);
     if (strlen(pclOnbl) > 0)
     {
        ilRC = InsFOGTAB(pclUrno,"A","O",pclNewPabs,pclNewPaes);
        ilInsRecs++;
     }
     slFktRd1 = NEXT;
     ilRCdb = sql_if(slFktRd1,&slCursorRd1,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursorRd1);
  dbg(TRACE,"There were %d arrival records of AFT updated",ilTotRecs);
  dbg(TRACE,"There were %d arrival records into FOG inserted",ilInsRecs);
  return ilRC;
} /* End of BuildGroundTimesArrivals */


static int BuildGroundTimesDepartures(char *pclWorkDay)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFktRd1;
  short slCursorRd1;
  short slFktWr;
  short slCursorWr;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  char pclUpdSqlBuf[L_BUFF];
  char pclUpdSelectBuf[L_BUFF];
  char pclUpdTmpBuf[L_BUFF];
  char pclTmpBuf[XS_BUFF];
  char pclUrno[32];
  char pclTifa[32];
  char pclTifd[32];
  char pclRkey[32];
  char pclFtyp[32];
  char pclNewPabs[32];
  char pclNewPaes[32];
  char pclNewPdbs[32];
  char pclNewPdes[32];
  int ilLen;
  int ilTotRecs = 0;
  char pclFieldList[L_BUFF];
  char pclDataList[L_BUFF];

  strcpy(pclSqlBuf,"SELECT URNO,TIFA,TIFD,RKEY,FTYP FROM AFTTAB ");
  sprintf(pclSelectBuf,"WHERE TIFD LIKE '%s%%' AND ORG3 = '%s' AND (RTYP = 'S' OR RTYP = ' ') AND ADID = 'D'",
                       pclWorkDay,pcgHomeAp);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursorRd1 = 0;
  slFktRd1 = START;
  ilRCdb = sql_if(slFktRd1,&slCursorRd1,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     ilTotRecs++;
     BuildItemBuffer(pclDataBuf,"",5,",");
     ilLen = get_real_item(pclUrno,pclDataBuf,1);
     ilLen = get_real_item(pclTifa,pclDataBuf,2);
     ilLen = get_real_item(pclTifd,pclDataBuf,3);
     ilLen = get_real_item(pclRkey,pclDataBuf,4);
     ilLen = get_real_item(pclFtyp,pclDataBuf,5);
     ilRC = CalculateGroundTimes(NULL,NULL,pclTifd,NULL,NULL,NULL,NULL,NULL,"D",1,1,
                                 pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
     sprintf(pclUpdTmpBuf,"PDBS='%s',PDES='%s',PABS='%s',PAES='%s'",
             pclNewPdbs,pclNewPdes,pclNewPabs,pclNewPaes);
     strcpy(pclFieldList,"PDBS,PDES,PABS,PAES,TIFA,TIFD,ADID,RKEY,FTYP,URNO");
     sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",pclNewPdbs,pclNewPdes,pclNewPabs,pclNewPaes,
             pclTifa,pclTifd,"D",pclRkey,pclFtyp,pclUrno);
     sprintf(pclUpdSqlBuf,"UPDATE AFTTAB SET %s ",pclUpdTmpBuf);
     sprintf(pclUpdSelectBuf,"WHERE URNO = %s",pclUrno);
     strcat(pclUpdSqlBuf,pclUpdSelectBuf);
     slCursorWr = 0;
     slFktWr = START;
     dbg(DEBUG,"<%s>",pclUpdSqlBuf);
     ilRCdb = sql_if(slFktWr,&slCursorWr,pclUpdSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
        ilRC = TriggerBchdlAction("UFR","AFTTAB",pclUrno,pclFieldList,
                                  pclDataList,FALSE,FALSE);
     }
     close_my_cursor(&slCursorWr);
     slFktRd1 = NEXT;
     ilRCdb = sql_if(slFktRd1,&slCursorRd1,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursorRd1);
  dbg(TRACE,"There were %d departure records of AFT updated",ilTotRecs);
  return ilRC;
} /* End of BuildGroundTimesDepartures */


static int BuildGroundTimesRotations(char *pclWorkDay)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFktRd1;
  short slCursorRd1;
  short slFktRd2;
  short slCursorRd2;
  short slFktWr;
  short slCursorWr;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  char pclSqlBuf2[L_BUFF];
  char pclSelectBuf2[L_BUFF];
  char pclDataBuf2[XXL_BUFF];
  char pclUpdSqlBuf[L_BUFF];
  char pclUpdSelectBuf[L_BUFF];
  char pclUpdTmpBuf[L_BUFF];
  char pclTmpBuf[XS_BUFF];
  char pclRkey[32];
  char pclNewPabs[32];
  char pclNewPaes[32];
  char pclNewPdbs[32];
  char pclNewPdes[32];
  int ilLen;
  int ilTotRecs = 0;
  int ilJoinedRecs = 0;
  char pclUrnoList[100][32];
  char pclTifaList[100][32];
  char pclTifdList[100][32];
  char pclOnblList[100][32];
  char pclOfblList[100][32];
  char pclAdidList[100][32];
  char pclFtypList[100][32];
  char pclUrnoDep[32];
  char pclTifaDep[32];
  char pclTifdDep[32];
  char pclOnblDep[32];
  char pclOfblDep[32];
  char pclAdidDep[32];
  char pclFtypDep[32];
  int ilI;
  int ilInsRecs = 0;
  char pclFieldList[L_BUFF];
  char pclDataList[L_BUFF];
  char pclFirstDay[32];
  char pclLastDay[32];
  long llTime1;
  long llTime2;
  long llTime;
  char pclStat[8];
  char pclNewLastDay[32];
  int ilDepRecExists;

  strcpy(pclSqlBuf,"SELECT DISTINCT RKEY FROM AFTTAB ");
  sprintf(pclSelectBuf,"WHERE TIFD LIKE '%s%%' AND ORG3 = '%s' AND ADID = 'D' AND RTYP = 'J' and OFBL= ' '",
                       pclWorkDay,pcgHomeAp,pclWorkDay,pcgHomeAp);

  /*

  sprintf(pclSelectBuf,"WHERE TIFA LIKE '%s%%' AND DES3 = '%s' AND ADID = 'A' AND RTYP = 'J'",
                       pclWorkDay,pcgHomeAp,pclWorkDay,pcgHomeAp);
  */

  strcat(pclSqlBuf,pclSelectBuf);
  slCursorRd1 = 0;
  slFktRd1 = START;
  ilRCdb = sql_if(slFktRd1,&slCursorRd1,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     ilJoinedRecs = 0;
     strcpy(pclRkey,pclDataBuf);

     /* Read Arrival Record */
     strcpy(pclSqlBuf2,"SELECT URNO,TIFA,TIFD,ONBL,OFBL,ADID,FTYP FROM AFTTAB ");
     sprintf(pclSelectBuf2,"WHERE RKEY = %s AND ADID = 'A'",pclRkey);
     strcat(pclSqlBuf2,pclSelectBuf2);
     slCursorRd2 = 0;
     slFktRd2 = START;
     ilRCdb = sql_if(slFktRd2,&slCursorRd2,pclSqlBuf2,pclDataBuf2);
     if (ilRCdb == DB_SUCCESS)
     {
        ilTotRecs++;
        BuildItemBuffer(pclDataBuf2,"",7,",");
        ilLen = get_real_item(&pclUrnoList[ilJoinedRecs][0],pclDataBuf2,1);
        ilLen = get_real_item(&pclTifaList[ilJoinedRecs][0],pclDataBuf2,2);
        ilLen = get_real_item(&pclTifdList[ilJoinedRecs][0],pclDataBuf2,3);
        ilLen = get_real_item(&pclOnblList[ilJoinedRecs][0],pclDataBuf2,4);
        ilLen = get_real_item(&pclOfblList[ilJoinedRecs][0],pclDataBuf2,5);
        ilLen = get_real_item(&pclAdidList[ilJoinedRecs][0],pclDataBuf2,6);
        strcat(&pclAdidList[ilJoinedRecs][0],"R");
        ilLen = get_real_item(&pclFtypList[ilJoinedRecs][0],pclDataBuf2,7);
        ilJoinedRecs++;
     }
     close_my_cursor(&slCursorRd2);
     /* Read Departure Record */
     strcpy(pclSqlBuf2,"SELECT URNO,TIFA,TIFD,ONBL,OFBL,ADID,FTYP FROM AFTTAB ");
     sprintf(pclSelectBuf2,"WHERE RKEY = %s AND ADID = 'D'",pclRkey);
     strcat(pclSqlBuf2,pclSelectBuf2);
     slCursorRd2 = 0;
     slFktRd2 = START;
     ilRCdb = sql_if(slFktRd2,&slCursorRd2,pclSqlBuf2,pclDataBuf2);
     if (ilRCdb == DB_SUCCESS)
     {
        ilTotRecs++;
        BuildItemBuffer(pclDataBuf2,"",7,",");
        ilLen = get_real_item(&pclUrnoDep[0],pclDataBuf2,1);
        ilLen = get_real_item(&pclTifaDep[0],pclDataBuf2,2);
        ilLen = get_real_item(&pclTifdDep[0],pclDataBuf2,3);
        ilLen = get_real_item(&pclOnblDep[0],pclDataBuf2,4);
        ilLen = get_real_item(&pclOfblDep[0],pclDataBuf2,5);
        ilLen = get_real_item(&pclAdidDep[0],pclDataBuf2,6);
        strcat(&pclAdidDep[0],"R");
        ilLen = get_real_item(&pclFtypDep[0],pclDataBuf2,7);
        ilDepRecExists = TRUE;
     }
     else
     {
        ilDepRecExists = FALSE;
     }
     close_my_cursor(&slCursorRd2);
     /* Read Towing Records , etc which are OnBlock */
     strcpy(pclSqlBuf2,"SELECT URNO,TIFA,TIFD,ONBL,OFBL,ADID,FTYP FROM AFTTAB ");
     /*
     sprintf(pclSelectBuf2,"WHERE RKEY = %s AND ADID = 'B' AND ONBL <> ' ' ORDER BY ADID,TIFA",pclRkey);
      * */
     sprintf(pclSelectBuf2,"WHERE RKEY = %s AND ADID = 'B' and FTYP in ('T','G') AND ONBL <> ' ' ORDER BY ADID,TIFA",pclRkey);
     strcat(pclSqlBuf2,pclSelectBuf2);
     slCursorRd2 = 0;
     slFktRd2 = START;
     ilRCdb = sql_if(slFktRd2,&slCursorRd2,pclSqlBuf2,pclDataBuf2);
     while (ilRCdb == DB_SUCCESS)
     {
        ilTotRecs++;
        BuildItemBuffer(pclDataBuf2,"",7,",");
        ilLen = get_real_item(&pclUrnoList[ilJoinedRecs][0],pclDataBuf2,1);
        ilLen = get_real_item(&pclTifaList[ilJoinedRecs][0],pclDataBuf2,2);
        ilLen = get_real_item(&pclTifdList[ilJoinedRecs][0],pclDataBuf2,3);
        ilLen = get_real_item(&pclOnblList[ilJoinedRecs][0],pclDataBuf2,4);
        ilLen = get_real_item(&pclOfblList[ilJoinedRecs][0],pclDataBuf2,5);
        ilLen = get_real_item(&pclAdidList[ilJoinedRecs][0],pclDataBuf2,6);
        strcat(&pclAdidList[ilJoinedRecs][0],"R");
        ilLen = get_real_item(&pclFtypList[ilJoinedRecs][0],pclDataBuf2,7);
        ilJoinedRecs++;
        slFktRd2 = NEXT;
        ilRCdb = sql_if(slFktRd2,&slCursorRd2,pclSqlBuf2,pclDataBuf2);
     }
     close_my_cursor(&slCursorRd2);
     /* Insert Departure Record here, if it is OffBlock */
     if (ilDepRecExists == TRUE && strlen(&pclOfblDep[0]) > 0)
     {
        strcpy(&pclUrnoList[ilJoinedRecs][0],pclUrnoDep);
        strcpy(&pclTifaList[ilJoinedRecs][0],pclTifaDep);
        strcpy(&pclTifdList[ilJoinedRecs][0],pclTifdDep);
        strcpy(&pclOnblList[ilJoinedRecs][0],pclOnblDep);
        strcpy(&pclOfblList[ilJoinedRecs][0],pclOfblDep);
        strcpy(&pclAdidList[ilJoinedRecs][0],pclAdidDep);
        strcpy(&pclFtypList[ilJoinedRecs][0],pclFtypDep);
        ilJoinedRecs++;
     }
     else
     {
        /* Read Towing Records , etc which are not yet OnBlock */
        strcpy(pclSqlBuf2,"SELECT URNO,TIFA,TIFD,ONBL,OFBL,ADID,FTYP FROM AFTTAB ");
        sprintf(pclSelectBuf2,"WHERE RKEY = %s AND ADID = 'B' AND ONBL = ' ' ORDER BY ADID,TIFA",pclRkey);
        strcat(pclSqlBuf2,pclSelectBuf2);
        slCursorRd2 = 0;
        slFktRd2 = START;
        ilRCdb = sql_if(slFktRd2,&slCursorRd2,pclSqlBuf2,pclDataBuf2);
        while (ilRCdb == DB_SUCCESS)
        {
           ilTotRecs++;
           BuildItemBuffer(pclDataBuf2,"",7,",");
           ilLen = get_real_item(&pclUrnoList[ilJoinedRecs][0],pclDataBuf2,1);
           ilLen = get_real_item(&pclTifaList[ilJoinedRecs][0],pclDataBuf2,2);
           ilLen = get_real_item(&pclTifdList[ilJoinedRecs][0],pclDataBuf2,3);
           ilLen = get_real_item(&pclOnblList[ilJoinedRecs][0],pclDataBuf2,4);
           ilLen = get_real_item(&pclOfblList[ilJoinedRecs][0],pclDataBuf2,5);
           ilLen = get_real_item(&pclAdidList[ilJoinedRecs][0],pclDataBuf2,6);
           strcat(&pclAdidList[ilJoinedRecs][0],"R");
           ilLen = get_real_item(&pclFtypList[ilJoinedRecs][0],pclDataBuf2,7);
           ilJoinedRecs++;
           slFktRd2 = NEXT;
           ilRCdb = sql_if(slFktRd2,&slCursorRd2,pclSqlBuf2,pclDataBuf2);
        }
        close_my_cursor(&slCursorRd2);
     }
     /* Insert Departure Record here, if it is not OffBlock */
     if (ilDepRecExists == TRUE && strlen(&pclOfblDep[0]) == 0)
     {
        strcpy(&pclUrnoList[ilJoinedRecs][0],pclUrnoDep);
        strcpy(&pclTifaList[ilJoinedRecs][0],pclTifaDep);
        strcpy(&pclTifdList[ilJoinedRecs][0],pclTifdDep);
        strcpy(&pclOnblList[ilJoinedRecs][0],pclOnblDep);
        strcpy(&pclOfblList[ilJoinedRecs][0],pclOfblDep);
        strcpy(&pclAdidList[ilJoinedRecs][0],pclAdidDep);
        strcpy(&pclFtypList[ilJoinedRecs][0],pclFtypDep);
        ilJoinedRecs++;
     }

     if (ilJoinedRecs == 1)
     {
        pclAdidList[0][1] = '\0';
        strcpy(pclLastDay,pcgPaesArr);
     }

     for (ilI = 0; ilI < ilJoinedRecs; ilI++)
     {
        if (ilI == 0)
        {
           ilRC = CalculateGroundTimes(&pclTifaList[ilI][0],NULL,
                                       &pclTifdList[ilI][0],&pclTifdList[ilI+1][0],
                                       &pclOnblList[ilI][0],NULL,
                                       &pclOfblList[ilI][0],&pclOfblList[ilI+1][0],
                                       &pclAdidList[ilI][0],ilI,ilJoinedRecs,
                                       pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
           strcpy(pclFirstDay,pclNewPabs);
        }
        else
        {
           if (ilI == ilJoinedRecs-1)
           {
              ilRC = CalculateGroundTimes(&pclTifaList[ilI][0],&pclTifaList[ilI-1][0],
                                          &pclTifdList[ilI][0],NULL,
                                          &pclOnblList[ilI][0],&pclOnblList[ilI-1][0],
                                          &pclOfblList[ilI][0],NULL,
                                          &pclAdidList[ilI][0],ilI,ilJoinedRecs,
                                          pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
              strcpy(pclLastDay,pclNewPdes);
           }
           else
           {
              ilRC = CalculateGroundTimes(&pclTifaList[ilI][0],&pclTifaList[ilI-1][0],
                                          &pclTifdList[ilI][0],&pclTifdList[ilI+1][0],
                                          &pclOnblList[ilI][0],&pclOnblList[ilI-1][0],
                                          &pclOfblList[ilI][0],&pclOfblList[ilI+1][0],
                                          &pclAdidList[ilI][0],ilI,ilJoinedRecs,
                                          pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
           }
        }
        sprintf(pclUpdTmpBuf,"PABS='%s',PAES='%s',PDBS='%s',PDES='%s'",
                pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
        strcpy(pclFieldList,"PABS,PAES,PDBS,PDES,TIFA,TIFD,ADID,RKEY,FTYP,URNO");
        sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%c,%s,%s,%s",pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes,
                &pclTifaList[ilI][0],&pclTifdList[ilI][0],
                pclAdidList[ilI][0],pclRkey,&pclFtypList[ilI][0],&pclUrnoList[ilI][0]);
        sprintf(pclUpdSqlBuf,"UPDATE AFTTAB SET %s ",pclUpdTmpBuf);
        sprintf(pclUpdSelectBuf,"WHERE URNO = %s",&pclUrnoList[ilI][0]);
        strcat(pclUpdSqlBuf,pclUpdSelectBuf);
        slCursorWr = 0;
        slFktWr = START;
        dbg(DEBUG,"<%s>",pclUpdSqlBuf);
        ilRCdb = sql_if(slFktWr,&slCursorWr,pclUpdSqlBuf,pclDataBuf);
        if (ilRCdb == DB_SUCCESS)
        {
           commit_work();
           ilRC = TriggerBchdlAction("UFR","AFTTAB",&pclUrnoList[ilI][0],pclFieldList,
                                     pclDataList,FALSE,FALSE);
        }
        close_my_cursor(&slCursorWr);
     }
     if (strcmp(pclLastDay,pcgPaesArr) != 0)
     {
        llTime1 = GetSecondsFromCEDATime(pclFirstDay);
        llTime2 = GetSecondsFromCEDATime(pclLastDay);
        llTime = llTime2 - llTime1;
        if (llTime > igOnGroundTimeLimit && strcmp(pcgCheckOnGroundTimeLimit,"YES") == 0)
        {
           if (strlen(&pclOnblList[0][0]) == 0)
           {
              strcpy(pclStat,"S");
           }
           else
           {
              if (strlen(&pclOfblList[ilJoinedRecs-1][0]) == 0)
              {
                 strcpy(pclStat,"O");
                 ilRC = TimeToStr(pclNewLastDay,time(NULL));
                 if (strcmp(pclNewLastDay,pclLastDay) > 0)
                 {
                    strcpy(pclLastDay,pclNewLastDay);
                 }
              }
              else
              {
                 strcpy(pclStat,"C");
              }
           }
           ilRC = InsFOGTAB(&pclUrnoList[0][0],"R",pclStat,pclFirstDay,pclLastDay);
           ilInsRecs++;
        }
        else
        {
           if (strlen(&pclOnblList[0][0]) > 0 &&
               strlen(&pclOfblList[ilJoinedRecs-1][0]) == 0)
           {
              ilRC = TimeToStr(pclNewLastDay,time(NULL));
              if (strcmp(pclNewLastDay,pclLastDay) > 0)
              {
                 strcpy(pclLastDay,pclNewLastDay);
              }
              ilRC = InsFOGTAB(&pclUrnoList[0][0],"P","O",pclFirstDay,pclLastDay);
              ilInsRecs++;
           }
        }
     }
     else
     {
        if (strlen(&pclOnblList[0][0]) > 0)
        {
           ilRC = InsFOGTAB(&pclUrnoList[0][0],"R","O",pclFirstDay,pclLastDay);
           ilInsRecs++;
        }
     }
     slFktRd1 = NEXT;
     ilRCdb = sql_if(slFktRd1,&slCursorRd1,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursorRd1);
  dbg(TRACE,"There were %d rotation records of AFT updated",ilTotRecs);
  dbg(TRACE,"There were %d rotation records into FOG inserted",ilInsRecs);
  return ilRC;
} /* End of BuildGroundTimesRotations */


static int CalculateGroundTimes(char *pcpTifa, char *pcpTifaPrev,
                                char *pcpTifd, char *pcpTifdNext,
                                char *pcpOnbl, char *pcpOnblPrev,
                                char *pcpOfbl, char *pcpOfblNext,
                                char *pcpType, int ipCur, int ipMax,
                                char *pcpPabs, char *pcpPaes, char *pcpPdbs, char *pcpPdes)
{
  int ilRC = RC_SUCCESS;
  long llTime1;
  long llTime2;
  long llTime;
  char pclCurTime[32];

  if (strcmp(pcpType,"A") == 0)
  {
/********** Single Arrival Record **********/
     strcpy(pcpPabs,pcpTifa);
     ilRC = MyAddSecondsToCEDATime(pcpPabs,igPabsArr*60,1);

     /* UFIS-1265
      * The PAES should be calculated even the flight is ONBL
      * So we have put in comments the old solution
      */
     strcpy(pcpPaes,pcpTifa);
     ilRC = MyAddSecondsToCEDATime(pcpPaes,igPaesArr*60,1);


/*
     if (strlen(pcpOnbl) == 0)
     {
        strcpy(pcpPaes,pcpTifa);
        ilRC = MyAddSecondsToCEDATime(pcpPaes,igPaesArr*60,1);
     }
     else
     {
        strcpy(pcpPaes,pcgPaesArr);
     }
*/
     dbg(TRACE,"CalculateGroundTimes Single Arrival Record");
     strcpy(pcpPdbs," ");
     strcpy(pcpPdes," ");
  }
  else
  {
/********** Single Departure Record **********/
     if (strcmp(pcpType,"D") == 0)
     {
        strcpy(pcpPdbs,pcpTifd);
        ilRC = MyAddSecondsToCEDATime(pcpPdbs,igPdbsDep*(-60),1);
        strcpy(pcpPdes,pcpTifd);
        ilRC = MyAddSecondsToCEDATime(pcpPdes,igPdesDep*(-60),1);
        strcpy(pcpPabs," ");
        strcpy(pcpPaes," ");
        dbg(TRACE,"CalculateGroundTimes Single Departure Record");
     }
     else
     {
        if (ipCur == 0)
        {
/********** First Record of a Rotation Chain **********/
           strcpy(pcpPabs,pcpTifa);
           ilRC = MyAddSecondsToCEDATime(pcpPabs,igPabsArr*60,1);
           llTime1 = GetSecondsFromCEDATime(pcpTifdNext);
           llTime = time(NULL);
           if (strlen(pcpOnbl) > 0 && strlen(pcpOfblNext) == 0 && llTime1 < llTime)
           {  /* current time is after scheduled departure time */
              llTime1 = llTime;
           }
           llTime2 = GetSecondsFromCEDATime(pcpTifa);
           llTime = (llTime1 - llTime2) * igRotTimeArrPerc / 100 + igPaesRot * 60;
           strcpy(pcpPaes,pcpTifa);
           if ((llTime1 - llTime2) > 0)
           {
              ilRC = MyAddSecondsToCEDATime(pcpPaes,llTime,1);
           }
           strcpy(pcpPdbs," ");
           strcpy(pcpPdes," ");
           dbg(TRACE,"CalculateGroundTimes First Record of a Rotation Chain");
        }
        else
        {
           if (ipCur == ipMax-1)
/********** Last Record of a Rotation Chain **********/
           {
              if (strcmp(pcpType,"DR") == 0)
              {
/********** Last Record is a Departure Record **********/
                 llTime1 = GetSecondsFromCEDATime(pcpTifd);
                 llTime = time(NULL);
                 if (strlen(pcpOnblPrev) > 0 && strlen(pcpOfbl) == 0 && llTime1 < llTime)
                 {  /* current time is after scheduled departure time */
                    llTime1 = llTime;
                 }
                 llTime2 = GetSecondsFromCEDATime(pcpTifaPrev);
                 llTime = (llTime1 - llTime2) * igRotTimeArrPerc / 100 + igPdbsRot * 60;
                 strcpy(pcpPdbs,pcpTifaPrev);
                 if ((llTime1 - llTime2) > 0)
                 {
                    ilRC = MyAddSecondsToCEDATime(pcpPdbs,llTime,1);
                 }
                 ilRC = TimeToStr(pclCurTime,time(NULL));
                 strcpy(pcpPdes,pcpTifd);
                 if (strlen(pcpOnblPrev) > 0 && strlen(pcpOfbl) == 0 &&
                     strcmp(pcpTifd,pclCurTime) < 0)
                 {  /* current time is after scheduled departure time */
                    strcpy(pcpPdes,pclCurTime);
                 }
                 ilRC = MyAddSecondsToCEDATime(pcpPdes,igPdesDep*60*(-1),1);
                 strcpy(pcpPabs," ");
                 strcpy(pcpPaes," ");
                 dbg(TRACE,"CalculateGroundTimes Last Record of a Rotation Chain , DR");
              }
              else
              {
/********** Last Record is a Return Flight or Towing Record , etc **********/
                 llTime1 = GetSecondsFromCEDATime(pcpTifd);
                 llTime = time(NULL);
                 if (strlen(pcpOnblPrev) > 0 && strlen(pcpOfbl) == 0 && llTime1 < llTime)
                 {  /* current time is after scheduled departure time */
                    llTime1 = llTime;
                 }
                 llTime2 = GetSecondsFromCEDATime(pcpTifaPrev);
                 llTime = (llTime1 - llTime2) * igRotTimeArrPerc / 100 + igPdbsRot * 60;
                 strcpy(pcpPdbs,pcpTifaPrev);
                 ilRC = MyAddSecondsToCEDATime(pcpPdbs,llTime,1);
                 ilRC = TimeToStr(pclCurTime,time(NULL));
                 strcpy(pcpPdes,pcpTifd);
                 if (strlen(pcpOnblPrev) > 0 && strlen(pcpOfbl) == 0 &&
                     strcmp(pcpTifd,pclCurTime) < 0)
                 {  /* current time is after scheduled departure time */
                    strcpy(pcpPdes,pclCurTime);
                 }
                 ilRC = MyAddSecondsToCEDATime(pcpPdes,igPdesRot*60*(-1),1);
                 strcpy(pcpPabs,pcpTifa);
                 ilRC = MyAddSecondsToCEDATime(pcpPabs,igPabsArr*60,1);

                 /* UFIS-1265
                  * The PAES should be calculated even the flight is ONBL
                  * So we have put in comments the old solution
                */
                 strcpy(pcpPaes,pcpTifa);
                 ilRC = MyAddSecondsToCEDATime(pcpPaes,igPaesArr*60,1);

/*
                 if (strlen(pcpOnbl) == 0)
                 {
                    strcpy(pcpPaes,pcpTifa);
                    ilRC = MyAddSecondsToCEDATime(pcpPaes,igPaesArr*60,1);
                 }
                 else
                 {
                    strcpy(pcpPaes,pcgPaesArr);
                 }
*/

                 dbg(TRACE,"CalculateGroundTimes Last Record of a Rotation Chain ");
              }
           }
           else
           {
/********** Return Flights or Towing Records ,etc in middle of a Rotation Chain **********/

/********** Calculate PABS **********/
              if (strcmp(pcpType,"DR") == 0)
              {
                 strcpy(pcpPabs," ");
              }
              else
              {
                 strcpy(pcpPabs,pcpTifa);
                 ilRC = MyAddSecondsToCEDATime(pcpPabs,igPabsRot*60,1);
              }
/********** Calculate PAES **********/
              if (strcmp(pcpType,"DR") == 0)
              {
                 strcpy(pcpPaes," ");
              }
              else
              {
                 llTime1 = GetSecondsFromCEDATime(pcpTifdNext);
                 llTime = time(NULL);
                 if (strlen(pcpOnbl) > 0 && strlen(pcpOfblNext) == 0 && llTime1 < llTime)
                 {  /* current time is after scheduled departure time */
                    llTime1 = llTime;
                 }
                 llTime2 = GetSecondsFromCEDATime(pcpTifa);
                 llTime = (llTime1 - llTime2) * igRotTimeArrPerc / 100 + igPaesRot * 60;
                 strcpy(pcpPaes,pcpTifa);
                 if ((llTime1 - llTime2) > 0)
                 {
                    ilRC = MyAddSecondsToCEDATime(pcpPaes,llTime,1);
                 }
                 dbg(TRACE,"CalculateGroundTimes (pcpPaes) Return Flights or Towing Records pcpType <%s> ",pcpType);
              }
/********** Calculate PDBS **********/
              llTime1 = GetSecondsFromCEDATime(pcpTifd);
              llTime = time(NULL);
              if (strlen(pcpOnblPrev) > 0 && strlen(pcpOfbl) == 0 && llTime1 < llTime)
              {  /* current time is after scheduled departure time */
                 llTime1 = llTime;
              }
              llTime2 = GetSecondsFromCEDATime(pcpTifaPrev);
              llTime = (llTime1 - llTime2) * igRotTimeArrPerc / 100 + igPdbsRot * 60;
              strcpy(pcpPdbs,pcpTifaPrev);
              if ((llTime1 - llTime2) > 0)
              {
                 ilRC = MyAddSecondsToCEDATime(pcpPdbs,llTime,1);
              }
/********** Calculate PDES **********/
              ilRC = TimeToStr(pclCurTime,time(NULL));
              strcpy(pcpPdes,pcpTifd);
              if (strlen(pcpOnblPrev) > 0 && strlen(pcpOfbl) == 0 &&
                  strcmp(pcpTifd,pclCurTime) < 0)
              {  /* current time is after scheduled departure time */
                 strcpy(pcpPdes,pclCurTime);
              }
              ilRC = MyAddSecondsToCEDATime(pcpPdes,igPdesRot*60*(-1),1);
              dbg(TRACE,"CalculateGroundTimes  Return Flights or Towing Records pcpType <%s> ",pcpType);
           }
        }
     }
  }

  return ilRC;
} /* End of Calculate GroundTimes */


static int InsFOGTAB(char *pcpUrno, char *pcpType, char *pcpStat,
                     char *pcpFirstDay, char *pcpLastDay)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBufIns[L_BUFF];
  char pclSqlBufFields[L_BUFF];
  char pclSqlBufValues[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  char pclNewUrno[32];
  int ilDebugLevel;
  char pclDataList[L_BUFF];

  ilDebugLevel = debug_level;
  debug_level = 0;
  /*
  GetNextValues(pclNewUrno,1);
  */

  DB_LoadUrnoList(1,pclNewUrno,pcgTableKey);

  debug_level = ilDebugLevel;
  strcpy(pclSqlBufFields,"URNO,HOPO,FLNU,OGBS,OGES,FTYP,STAT");
  sprintf(pclSqlBufValues,"%s,'%s',%s,'%s','%s','%s','%s'",
          pclNewUrno,pcgHomeAp,pcpUrno,pcpFirstDay,pcpLastDay,pcpType,pcpStat);
  sprintf(pclSqlBufIns,"INSERT INTO FOGTAB FIELDS(%s) VALUES(%s)",
          pclSqlBufFields,pclSqlBufValues);
  sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%s",
          pclNewUrno,pcgHomeAp,pcpUrno,pcpFirstDay,pcpLastDay,pcpType,pcpStat);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"InsFOGTAB: <%s>",pclSqlBufIns);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBufIns,pclDataBuf);
  if (ilRCdb == DB_SUCCESS)
  {
     commit_work();
     ilRC = TriggerBchdlAction("IRT","FOGTAB",pclNewUrno,pclSqlBufFields,
                               pclDataList,FALSE,TRUE);
  }
  close_my_cursor(&slCursor);

  return ilRC;
} /* End of InsFOGTAB */


static int CheckFOGTAB()
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  short slFktWr;
  short slCursorWr;
  char pclSqlBufWr[L_BUFF];
  int ilNoRecs = 0;
  int ilUpdRecs = 0;
  char pclUrno[32];
  char pclFlnu[32];
  char pclOgbs[32];
  char pclOges[32];
  char pclFtyp[32];
  char pclLastDay[32];
  char pclNewFtyp[32];
  int ilLen;
  long llTime1;
  long llTime2;
  long llTime;
  char pclFieldList[256];
  char pclDataList[256];
  char pclUpdBuf[256];

  strcpy(pclSqlBuf,"SELECT URNO,FLNU,OGBS,OGES,FTYP FROM FOGTAB ");
  sprintf(pclSelectBuf,"WHERE STAT = 'O' AND (FTYP = 'R' OR FTYP = 'P') AND OGES <> '%s'",pcgPaesArr);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  dbg(TRACE,"<%s>",pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     ilNoRecs++;
     BuildItemBuffer(pclDataBuf,"",5,",");
     ilLen = get_real_item(pclUrno,pclDataBuf,1);
     ilLen = get_real_item(pclFlnu,pclDataBuf,2);
     ilLen = get_real_item(pclOgbs,pclDataBuf,3);
     ilLen = get_real_item(pclOges,pclDataBuf,4);
     ilLen = get_real_item(pclFtyp,pclDataBuf,5);
     ilRC = TimeToStr(pclLastDay,time(NULL));
     if (strcmp(pclLastDay,pclOges) > 0)
     {
        ilUpdRecs++;
        if (strcmp(pclFtyp,"P") == 0)
        {
           llTime1 = GetSecondsFromCEDATime(pclOgbs);
           llTime2 = GetSecondsFromCEDATime(pclLastDay);
           llTime = llTime2 - llTime1;
           if (llTime > igOnGroundTimeLimit && strcmp(pcgCheckOnGroundTimeLimit,"YES") == 0)
           {
              strcpy(pclNewFtyp,"R");
           }
           else
           {
              strcpy(pclNewFtyp,"P");
           }
        }
        else
        {
           strcpy(pclNewFtyp,"R");
        }
        sprintf(pclUpdBuf,"OGES='%s',FTYP='%s'",pclLastDay,pclNewFtyp);
        strcpy(pclFieldList,"OGES,FTYP");
        sprintf(pclDataList,"%s,%s",pclLastDay,pclNewFtyp);
        sprintf(pclSqlBufWr,"UPDATE FOGTAB SET %s WHERE URNO = %s",
                pclUpdBuf,pclUrno);
        slCursorWr = 0;
        slFktWr = START;
        dbg(DEBUG,"CheckFOGTAB: <%s>",pclSqlBufWr);
        ilRCdb = sql_if(slFktWr,&slCursorWr,pclSqlBufWr,pclDataBuf);
        if (ilRCdb == DB_SUCCESS)
        {
           commit_work();
           ilRC = TriggerBchdlAction("URT","FOGTAB",pclUrno,pclFieldList,
                                     pclDataList,FALSE,TRUE);
        }
        close_my_cursor(&slCursorWr);
     }
     ilRC = UpdateFOGTAB(pclFlnu);   /* Now recalculate Position Times */
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);

  dbg(TRACE,"CheckFOGTAB: There were %d recs found and %d recs updated",ilNoRecs,ilUpdRecs);

  return ilRC;
} /* End of CheckFOGTAB */


static int UpdateFOGTAB(char *pcpUrno)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  char pclDataBuf2[XXL_BUFF];
  char pclTifa[32];
  char pclTifd[32];
  char pclPabs[32];
  char pclPaes[32];
  char pclPdbs[32];
  char pclPdes[32];
  char pclRtyp[32];
  char pclRkey[32];
  char pclAdid[32];
  char pclOnbl[32];
  char pclFtyp[32];
  char pclFlno[32];
  char pclRegn[32];
  char pclOfbl[32];
  char pclAirb[32];
  int ilLen;
  int ilJoinedArrivals = FALSE;

  strcpy(pclSqlBuf,"SELECT TIFA,TIFD,PABS,PAES,PDBS,PDES,RTYP,RKEY,ADID,ONBL,FTYP,FLNO,REGN,OFBL,AIRB FROM AFTTAB ");
  sprintf(pclSelectBuf,"WHERE URNO = %s",pcpUrno);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  if (ilRCdb != DB_SUCCESS)
  {
     close_my_cursor(&slCursor);
     strcpy(pclSqlBuf,"DELETE FROM FOGTAB ");
     sprintf(pclSelectBuf,"WHERE FLNU = %s",pcpUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"UpdateFOGTAB: <%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     commit_work();
     close_my_cursor(&slCursor);
     return ilRC;
  }
  close_my_cursor(&slCursor);
  BuildItemBuffer(pclDataBuf,"",14,",");
  ilLen = get_real_item(pclTifa,pclDataBuf,1);
  ilLen = get_real_item(pclTifd,pclDataBuf,2);
  ilLen = get_real_item(pclPabs,pclDataBuf,3);

  if (ilLen == 0)
  {
     strcpy(pclPabs," ");
  }
  ilLen = get_real_item(pclPaes,pclDataBuf,4);
  if (ilLen == 0)
  {
     strcpy(pclPaes," ");
  }
  ilLen = get_real_item(pclPdbs,pclDataBuf,5);
  if (ilLen == 0)
  {
     strcpy(pclPdbs," ");
  }
  ilLen = get_real_item(pclPdes,pclDataBuf,6);
  if (ilLen == 0)
  {
     strcpy(pclPdes," ");
  }
  ilLen = get_real_item(pclRtyp,pclDataBuf,7);
  if (ilLen == 0)
  {
     strcpy(pclRtyp,"S");
  }
  ilLen = get_real_item(pclRkey,pclDataBuf,8);
  ilLen = get_real_item(pclAdid,pclDataBuf,9);
  ilLen = get_real_item(pclOnbl,pclDataBuf,10);
  ilLen = get_real_item(pclFtyp,pclDataBuf,11);
  ilLen = get_real_item(pclFlno,pclDataBuf,12);

  ilLen = get_real_item(pclRegn,pclDataBuf,13);
  ilLen = get_real_item(pclOfbl,pclDataBuf,14);
  ilLen = get_real_item(pclAirb,pclDataBuf,14);

  /* UFIS-513
   * Check if the Flight is Departure 'D' or Return Flight B
   * or Arrival which are CXX or NOOP (FTYP = X,N)
   */
  if  ( (strcmp(pclAdid,"D") == 0 && strlen(pclAirb) > 4) ||
        (strcmp(pclAdid,"A") == 0 && (strcmp(pclFtyp,"X") == 0 || strcmp(pclFtyp,"N") == 0) )
      )
  {
       dbg(TRACE,"UpdateFOGTAB: FLIGHT DEPARTURE URNO,RKEY=%s,%s With AIRB=%s should be removed ",pcpUrno,pclRkey,pclAirb);
       close_my_cursor(&slCursor);
       strcpy(pclSqlBuf,"DELETE FROM FOGTAB ");
       sprintf(pclSelectBuf,"WHERE FLNU in ( %s , %s)",pclRkey,pcpUrno);
       strcat(pclSqlBuf,pclSelectBuf);
       slCursor = 0;
       slFkt = START;
       dbg(DEBUG,"UpdateFOGTAB: <%s>",pclSqlBuf);
       ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
       commit_work();
       close_my_cursor(&slCursor);
       return ilRC;
  } else if ( strcmp(pclAdid,"B") == 0 && strcmp(pclFtyp,"Z") == 0  && strlen(pclOnbl) < 4 ) {
      dbg(TRACE,"UpdateFOGTAB: FLIGHT RETURN URNO=%s With AIRB=%s should be removed ",pcpUrno,pclOnbl);
      (void) DeleteOFBL_FOGTAB(pclRegn);
      return ilRC;

  }

  /*
  if (ilLen > 0)
  {
    dbg(TRACE,"UpdateFOGTAB: FLIGHT URNO=%s ,REGN=%s",pcpUrno,pclRegn);
    (void) DeleteOFBL_FOGTAB(pclRegn);
  }
  */


/* Modification for UFIS-1265 In case of Single Arrival with Tow the RTYP is S
   if (strcmp(pclAdid,"A") == 0 && strcmp(pclRtyp,"S") == 0)
*/
  if (strcmp(pclAdid,"A") == 0 )
  {
     strcpy(pclSqlBuf,"SELECT URNO FROM AFTTAB ");
     sprintf(pclSelectBuf,"WHERE RKEY = %s AND URNO <> %s ORDER BY TIFA",pclRkey,pclRkey);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf2);
/*
     && strcmp(pclRtyp,"J") == 0
*/

     if (ilRCdb == DB_SUCCESS )
     {
         /* Modification for UFIS-1265 In case of Single Arrival with Tow the RTYP is S
          * So we will hae more than one Records
          */
          dbg(TRACE,"UpdateFOGTAB: ilJoinedArrivals=TRUE , Selection %s",pclSqlBuf);
          ilJoinedArrivals = TRUE;
/*
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf2);
        if (ilRCdb == DB_SUCCESS)
        {
           ilJoinedArrivals = TRUE;
        }
*/
     }
     close_my_cursor(&slCursor);


     if (ilJoinedArrivals == TRUE)
     {
        ilRC = UpdateRotation(pcpUrno,pclRkey);
     }
     else
     {
        ilRC = UpdateArrival(pcpUrno,pclTifa,pclTifd,pclPabs,pclPaes,pclPdbs,pclPdes,pclOnbl,
                             pclRkey,pclFtyp,pclFlno);
     }
  }
  else
  {
     if (strcmp(pclAdid,"D") == 0 && strcmp(pclRtyp,"S") == 0)
     {
        ilRC = UpdateDeparture(pcpUrno,pclTifa,pclTifd,pclPabs,pclPaes,pclPdbs,pclPdes,pclRkey,
                               pclFtyp,pclFlno);
     }
     else
     {
        if (strcmp(pclRtyp,"J") == 0)
        {
           ilRC = UpdateRotation(pcpUrno,pclRkey);
        }
     }
  }

  return ilRC;
} /* End of UpdateFOGTAB */

/* UFIS-513 . UFIS-98
 * Delete Entries from FOGTAB when we have OFBL for a specific Aircraft */
static int DeleteOFBL_FOGTAB(char *pcpRegn)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  int ilLen;
  dbg(TRACE,"INSIDE DeleteOFB_FOGTAB: REGN <%s>",pcpRegn);

  /*
  This checks the records from FOGTAB where STAT=O and finds records where
  The OFBL != ' '
  The Delets the records from fogtab
  */
  strcpy(pclSqlBuf,"DELETE fogtab WHERE flnu IN ( ");
  strcat(pclSqlBuf," SELECT a.urno ");
  strcat(pclSqlBuf," FROM afttab a, afttab d, fogtab f ");
  strcat(pclSqlBuf," WHERE f.stat = 'O' ");
  strcat(pclSqlBuf," AND a.urno = f.flnu ");
  strcat(pclSqlBuf," AND a.rkey = d.rkey ");
  strcat(pclSqlBuf," AND a.adid IN ('A','B') ");
  strcat(pclSqlBuf," AND d.adid = 'D' ");
  strcat(pclSqlBuf," AND d.airb != ' ' ");
  sprintf(pclSelectBuf,"AND a.regn = '%s' ",pcpRegn);
  strcat(pclSqlBuf,pclSelectBuf);
  strcat(pclSqlBuf," ) ");

  dbg(TRACE,"DeleteOFB_FOGTAB: <%s>",pclSqlBuf);
  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  commit_work();
  close_my_cursor(&slCursor);
  return ilRCdb;

}/* End of DeleteOFBL_FOGTAB */



static int UpdateArrival(char *pcpUrno, char *pcpTifa, char *pcpTifd, char *pcpPabs,
                         char *pcpPaes, char *pcpPdbs, char *pcpPdes, char *pcpOnbl,
                         char *pcpRkey, char *pcpFtyp, char *pcpFlno)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slCursor;
  short slFkt;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  char pclNewPabs[32];
  char pclNewPaes[32];
  char pclNewPdbs[32];
  char pclNewPdes[32];
  char pclUpdBuf[L_BUFF];
  char pclFieldList[L_BUFF];
  char pclDataList[L_BUFF];

  dbg(DEBUG,"UpdateArrival:");
  ilRC = CalculateGroundTimes(pcpTifa,NULL,NULL,NULL,pcpOnbl,NULL,NULL,NULL,"A",1,1,
                              pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
/*
  if (strcmp(pclNewPabs,pcpPabs) != 0 || strcmp(pclNewPaes,pcpPaes) != 0 ||
      strcmp(pclNewPdbs,pcpPdbs) != 0 || strcmp(pclNewPdes,pcpPdes) != 0)
*/
  if (CheckTolerance(pclNewPabs,pcpPabs,pclNewPaes,pcpPaes,
                     pclNewPdbs,pcpPdbs,pclNewPdes,pcpPdes) == 0)
  {
     sprintf(pclUpdBuf,"PABS='%s',PAES='%s',PDBS='%s',PDES='%s'",
             pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
     strcpy(pclFieldList,"PABS,PAES,PDBS,PDES,TIFA,TIFD,ADID,RKEY,FTYP,FLNO,URNO");
     sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
             pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes,pcpTifa,pcpTifd,"A",pcpRkey,
             pcpFtyp,pcpFlno,pcpUrno);
     ilRC = CheckBcMode(pclNewPabs,pcpPabs,pclNewPaes,pcpPaes,
                        pclNewPdbs,pcpPdbs,pclNewPdes,pcpPdes,
                        pclFieldList,pclDataList);
     sprintf(pclSqlBuf,"UPDATE AFTTAB SET %s ",pclUpdBuf);
     sprintf(pclSelectBuf,"WHERE URNO = %s",pcpUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     dbg(DEBUG,"UpdateArrival: <%s>",pclSqlBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
        ilRC = TriggerBchdlAction("UFR","AFTTAB",pcpUrno,pclFieldList,
                                  pclDataList,TRUE,TRUE);
     }
     close_my_cursor(&slCursor);
  }

  ilRC = UpdFOGTAB(pcpUrno,pclNewPabs,pclNewPaes,"A",pcpOnbl,"",pcpRkey,"A");

  return ilRC;
} /* End of UpdateArrival */


static int UpdateDeparture(char *pcpUrno, char *pcpTifa, char *pcpTifd, char *pcpPabs,
                           char *pcpPaes, char *pcpPdbs, char *pcpPdes, char *pcpRkey,
                           char *pcpFtyp, char *pcpFlno)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slCursor;
  short slFkt;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  char pclNewPabs[32];
  char pclNewPaes[32];
  char pclNewPdbs[32];
  char pclNewPdes[32];
  char pclUpdBuf[L_BUFF];
  char pclFieldList[L_BUFF];
  char pclDataList[L_BUFF];

  dbg(DEBUG,"UpdateDeparture:");
  ilRC = CalculateGroundTimes(NULL,NULL,pcpTifd,NULL,NULL,NULL,NULL,NULL,"D",1,1,
                              pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
/*
  if (strcmp(pclNewPabs,pcpPabs) != 0 || strcmp(pclNewPaes,pcpPaes) != 0 ||
      strcmp(pclNewPdbs,pcpPdbs) != 0 || strcmp(pclNewPdes,pcpPdes) != 0)
*/
  if (CheckTolerance(pclNewPabs,pcpPabs,pclNewPaes,pcpPaes,
                     pclNewPdbs,pcpPdbs,pclNewPdes,pcpPdes) == 0)
  {
     sprintf(pclUpdBuf,"PABS='%s',PAES='%s',PDBS='%s',PDES='%s'",
             pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
     strcpy(pclFieldList,"PABS,PAES,PDBS,PDES,TIFA,TIFD,ADID,RKEY,FTYP,FLNO,URNO");
     sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
             pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes,pcpTifa,pcpTifd,"D",pcpRkey,
             pcpFtyp,pcpFlno,pcpUrno);
     ilRC = CheckBcMode(pclNewPabs,pcpPabs,pclNewPaes,pcpPaes,
                        pclNewPdbs,pcpPdbs,pclNewPdes,pcpPdes,
                        pclFieldList,pclDataList);
     sprintf(pclSqlBuf,"UPDATE AFTTAB SET %s ",pclUpdBuf);
     sprintf(pclSelectBuf,"WHERE URNO = %s",pcpUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     dbg(DEBUG,"UpdateDeparture: <%s>",pclSqlBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
        ilRC = TriggerBchdlAction("UFR","AFTTAB",pcpUrno,pclFieldList,
                                  pclDataList,TRUE,TRUE);
     }
     close_my_cursor(&slCursor);
  }

  ilRC = UpdFOGTAB(pcpUrno,pclNewPdbs,pclNewPdes,"D","","",pcpRkey,"D");

  return ilRC;
} /* End of UpdateDeparture */


static int UpdateRotation(char *pcpUrno, char *pcpRkey)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  char pclUrnoList[100][32];
  char pclTifaList[100][32];
  char pclTifdList[100][32];
  char pclOnblList[100][32];
  char pclOfblList[100][32];
  char pclAdidList[100][32];
  char pclPabsList[100][32];
  char pclPaesList[100][32];
  char pclPdbsList[100][32];
  char pclPdesList[100][32];
  char pclFtypList[100][32];
  char pclFlnoList[100][32];
  char pclUrnoDep[32];
  char pclTifaDep[32];
  char pclTifdDep[32];
  char pclOnblDep[32];
  char pclOfblDep[32];
  char pclAdidDep[32];
  char pclPabsDep[32];
  char pclPaesDep[32];
  char pclPdbsDep[32];
  char pclPdesDep[32];
  char pclFtypDep[32];
  char pclFlnoDep[32];
  char pclNewPabs[32];
  char pclNewPaes[32];
  char pclNewPdbs[32];
  char pclNewPdes[32];

  char pclRtypArr[32];

  int ilLen;
  int ilJoinedRecs = 0;
  int ilI;
  char pclUpdBuf[L_BUFF];
  char pclFieldList[L_BUFF];
  char pclDataList[L_BUFF];
  char pclFirstDay[32];
  char pclLastDay[32];
  int ilDepRecExists;


  strcat(pclRtypArr,"");

  dbg(DEBUG,"UpdateRotation:");
  /* Read Arrival Record */
  strcpy(pclSqlBuf,"SELECT URNO,TIFA,TIFD,ONBL,OFBL,ADID,PABS,PAES,PDBS,PDES,FTYP,FLNO,RTYP FROM AFTTAB ");
  sprintf(pclSelectBuf,"WHERE RKEY = %s AND ADID = 'A'",pcpRkey);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  if (ilRCdb == DB_SUCCESS)
  {
     dbg(TRACE,"UpdateFOGTAB: (1), Selection %s",pclSelectBuf);
     BuildItemBuffer(pclDataBuf,"",13,",");
     ilLen = get_real_item(&pclUrnoList[ilJoinedRecs][0],pclDataBuf,1);
     ilLen = get_real_item(&pclTifaList[ilJoinedRecs][0],pclDataBuf,2);
     ilLen = get_real_item(&pclTifdList[ilJoinedRecs][0],pclDataBuf,3);
     ilLen = get_real_item(&pclOnblList[ilJoinedRecs][0],pclDataBuf,4);
     ilLen = get_real_item(&pclOfblList[ilJoinedRecs][0],pclDataBuf,5);
     ilLen = get_real_item(&pclAdidList[ilJoinedRecs][0],pclDataBuf,6);
     strcat(&pclAdidList[ilJoinedRecs][0],"R");
     ilLen = get_real_item(&pclPabsList[ilJoinedRecs][0],pclDataBuf,7);
     ilLen = get_real_item(&pclPaesList[ilJoinedRecs][0],pclDataBuf,8);
     ilLen = get_real_item(&pclPdbsList[ilJoinedRecs][0],pclDataBuf,9);
     ilLen = get_real_item(&pclPdesList[ilJoinedRecs][0],pclDataBuf,10);
     ilLen = get_real_item(&pclFtypList[ilJoinedRecs][0],pclDataBuf,11);
     ilLen = get_real_item(&pclFlnoList[ilJoinedRecs][0],pclDataBuf,12);

     ilLen = get_real_item(pclRtypArr,pclDataBuf,13);

     ilJoinedRecs++;
  }
  close_my_cursor(&slCursor);
  /* Read Departure Record */
  strcpy(pclSqlBuf,"SELECT URNO,TIFA,TIFD,ONBL,OFBL,ADID,PABS,PAES,PDBS,PDES,FTYP,FLNO FROM AFTTAB ");
  sprintf(pclSelectBuf,"WHERE RKEY = %s AND ADID = 'D'",pcpRkey);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);

  /* Modification for UFIS-1265 In case of Single Arrival with Tow the RTYP is S
   * But In case of Split the Departure Part is updated afterwards so the we can
   * only know that is Split from the RTYP value
   * So we will hae more than one Records
   */
  if (ilRCdb == DB_SUCCESS && strcmp(pclRtypArr,"S") != 0 )
  {
     dbg(TRACE,"UpdateFOGTAB: (2), Selection %s",pclSelectBuf);
     BuildItemBuffer(pclDataBuf,"",12,",");
     ilLen = get_real_item(&pclUrnoDep[0],pclDataBuf,1);
     ilLen = get_real_item(&pclTifaDep[0],pclDataBuf,2);
     ilLen = get_real_item(&pclTifdDep[0],pclDataBuf,3);
     ilLen = get_real_item(&pclOnblDep[0],pclDataBuf,4);
     ilLen = get_real_item(&pclOfblDep[0],pclDataBuf,5);
     ilLen = get_real_item(&pclAdidDep[0],pclDataBuf,6);
     strcat(&pclAdidDep[0],"R");
     ilLen = get_real_item(&pclPabsDep[0],pclDataBuf,7);
     ilLen = get_real_item(&pclPaesDep[0],pclDataBuf,8);
     ilLen = get_real_item(&pclPdbsDep[0],pclDataBuf,9);
     ilLen = get_real_item(&pclPdesDep[0],pclDataBuf,10);
     ilLen = get_real_item(&pclFtypDep[0],pclDataBuf,11);
     ilLen = get_real_item(&pclFlnoDep[0],pclDataBuf,12);
     ilDepRecExists = TRUE;
  }
  else
  {
     ilDepRecExists = FALSE;
  }
  close_my_cursor(&slCursor);
  /* Read Towing Records , etc which are OnBlock */
  strcpy(pclSqlBuf,"SELECT URNO,TIFA,TIFD,ONBL,OFBL,ADID,PABS,PAES,PDBS,PDES,FTYP,FLNO FROM AFTTAB ");
  sprintf(pclSelectBuf,"WHERE RKEY = %s AND ADID = 'B' AND ONBL <> ' ' ORDER BY ADID,TIFA",pcpRkey);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     dbg(TRACE,"UpdateFOGTAB: (3), Selection %s",pclSelectBuf);
     BuildItemBuffer(pclDataBuf,"",12,",");
     ilLen = get_real_item(&pclUrnoList[ilJoinedRecs][0],pclDataBuf,1);
     ilLen = get_real_item(&pclTifaList[ilJoinedRecs][0],pclDataBuf,2);
     ilLen = get_real_item(&pclTifdList[ilJoinedRecs][0],pclDataBuf,3);
     ilLen = get_real_item(&pclOnblList[ilJoinedRecs][0],pclDataBuf,4);
     ilLen = get_real_item(&pclOfblList[ilJoinedRecs][0],pclDataBuf,5);
     ilLen = get_real_item(&pclAdidList[ilJoinedRecs][0],pclDataBuf,6);
     strcat(&pclAdidList[ilJoinedRecs][0],"R");
     ilLen = get_real_item(&pclPabsList[ilJoinedRecs][0],pclDataBuf,7);
     ilLen = get_real_item(&pclPaesList[ilJoinedRecs][0],pclDataBuf,8);
     ilLen = get_real_item(&pclPdbsList[ilJoinedRecs][0],pclDataBuf,9);
     ilLen = get_real_item(&pclPdesList[ilJoinedRecs][0],pclDataBuf,10);
     ilLen = get_real_item(&pclFtypList[ilJoinedRecs][0],pclDataBuf,11);
     ilLen = get_real_item(&pclFlnoList[ilJoinedRecs][0],pclDataBuf,12);
     ilJoinedRecs++;
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);
  /* Insert Departure Record here, if it is OffBlock */
  if (ilDepRecExists == TRUE && strlen(&pclOfblDep[0]) > 0)
  {
     strcpy(&pclUrnoList[ilJoinedRecs][0],pclUrnoDep);
     strcpy(&pclTifaList[ilJoinedRecs][0],pclTifaDep);
     strcpy(&pclTifdList[ilJoinedRecs][0],pclTifdDep);
     strcpy(&pclOnblList[ilJoinedRecs][0],pclOnblDep);
     strcpy(&pclOfblList[ilJoinedRecs][0],pclOfblDep);
     strcpy(&pclAdidList[ilJoinedRecs][0],pclAdidDep);
     strcpy(&pclPabsList[ilJoinedRecs][0],pclPabsDep);
     strcpy(&pclPaesList[ilJoinedRecs][0],pclPaesDep);
     strcpy(&pclPdbsList[ilJoinedRecs][0],pclPdbsDep);
     strcpy(&pclPdesList[ilJoinedRecs][0],pclPdesDep);
     strcpy(&pclFtypList[ilJoinedRecs][0],pclFtypDep);
     strcpy(&pclFlnoList[ilJoinedRecs][0],pclFlnoDep);
     ilJoinedRecs++;
  }
  else
  {
     /* Read Towing Records , etc whic are not yet OnBlock */
     strcpy(pclSqlBuf,"SELECT URNO,TIFA,TIFD,ONBL,OFBL,ADID,PABS,PAES,PDBS,PDES,FTYP,FLNO FROM AFTTAB ");
     sprintf(pclSelectBuf,"WHERE RKEY = %s AND ADID = 'B' AND ONBL = ' ' ORDER BY ADID,TIFA",pcpRkey);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     while (ilRCdb == DB_SUCCESS)
     {
        dbg(TRACE,"UpdateFOGTAB: (4), Selection %s",pclSelectBuf);
        BuildItemBuffer(pclDataBuf,"",12,",");
        ilLen = get_real_item(&pclUrnoList[ilJoinedRecs][0],pclDataBuf,1);
        ilLen = get_real_item(&pclTifaList[ilJoinedRecs][0],pclDataBuf,2);
        ilLen = get_real_item(&pclTifdList[ilJoinedRecs][0],pclDataBuf,3);
        ilLen = get_real_item(&pclOnblList[ilJoinedRecs][0],pclDataBuf,4);
        ilLen = get_real_item(&pclOfblList[ilJoinedRecs][0],pclDataBuf,5);
        ilLen = get_real_item(&pclAdidList[ilJoinedRecs][0],pclDataBuf,6);
        strcat(&pclAdidList[ilJoinedRecs][0],"R");
        ilLen = get_real_item(&pclPabsList[ilJoinedRecs][0],pclDataBuf,7);
        ilLen = get_real_item(&pclPaesList[ilJoinedRecs][0],pclDataBuf,8);
        ilLen = get_real_item(&pclPdbsList[ilJoinedRecs][0],pclDataBuf,9);
        ilLen = get_real_item(&pclPdesList[ilJoinedRecs][0],pclDataBuf,10);
        ilLen = get_real_item(&pclFtypList[ilJoinedRecs][0],pclDataBuf,11);
        ilLen = get_real_item(&pclFlnoList[ilJoinedRecs][0],pclDataBuf,12);
        ilJoinedRecs++;
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     }
     close_my_cursor(&slCursor);
  }
  /* Insert Departure Record here, if it is not OffBlock */
  if (ilDepRecExists == TRUE && strlen(&pclOfblDep[0]) == 0)
  {
     strcpy(&pclUrnoList[ilJoinedRecs][0],pclUrnoDep);
     strcpy(&pclTifaList[ilJoinedRecs][0],pclTifaDep);
     strcpy(&pclTifdList[ilJoinedRecs][0],pclTifdDep);
     strcpy(&pclOnblList[ilJoinedRecs][0],pclOnblDep);
     strcpy(&pclOfblList[ilJoinedRecs][0],pclOfblDep);
     strcpy(&pclAdidList[ilJoinedRecs][0],pclAdidDep);
     strcpy(&pclPabsList[ilJoinedRecs][0],pclPabsDep);
     strcpy(&pclPaesList[ilJoinedRecs][0],pclPaesDep);
     strcpy(&pclPdbsList[ilJoinedRecs][0],pclPdbsDep);
     strcpy(&pclPdesList[ilJoinedRecs][0],pclPdesDep);
     strcpy(&pclFtypList[ilJoinedRecs][0],pclFtypDep);
     strcpy(&pclFlnoList[ilJoinedRecs][0],pclFlnoDep);
     ilJoinedRecs++;
  }

  for (ilI = 0; ilI < ilJoinedRecs; ilI++)
  {
     if (strlen(&pclPabsList[ilI][0]) == 0)
     {
        strcpy(&pclPabsList[ilI][0]," ");
     }
     if (strlen(&pclPaesList[ilI][0]) == 0)
     {
        strcpy(&pclPaesList[ilI][0]," ");
     }
     if (strlen(&pclPdbsList[ilI][0]) == 0)
     {
        strcpy(&pclPdbsList[ilI][0]," ");
     }
     if (strlen(&pclPdesList[ilI][0]) == 0)
     {
        strcpy(&pclPdesList[ilI][0]," ");
     }
  }

  *pclLastDay = '\0';
  if (ilJoinedRecs == 1)
  {
     pclAdidList[0][1] = '\0';
     strcpy(pclLastDay,pcgPaesArr);
  }
  for (ilI = 0; ilI < ilJoinedRecs; ilI++)
  {
     if (ilI == 0)
     {
        ilRC = CalculateGroundTimes(&pclTifaList[ilI][0],NULL,
                                    &pclTifdList[ilI][0],&pclTifdList[ilI+1][0],
                                    &pclOnblList[ilI][0],NULL,
                                    &pclOfblList[ilI][0],&pclOfblList[ilI+1][0],
                                    &pclAdidList[ilI][0],ilI,ilJoinedRecs,
                                    pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
        strcpy(pclFirstDay,pclNewPabs);
        dbg(TRACE,"UpdateFOGTAB: (1) CalculateGroundTimes");
     }
     else
     {
        if (ilI == ilJoinedRecs-1)
        {
           ilRC = CalculateGroundTimes(&pclTifaList[ilI][0],&pclTifaList[ilI-1][0],
                                       &pclTifdList[ilI][0],NULL,
                                       &pclOnblList[ilI][0],&pclOnblList[ilI-1][0],
                                       &pclOfblList[ilI][0],NULL,
                                       &pclAdidList[ilI][0],ilI,ilJoinedRecs,
                                       pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
           /* strcpy(pclLastDay,pclNewPdes); */
           ilRC = CalculateMaxCEDATime(pclLastDay,pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
           dbg(TRACE,"UpdateFOGTAB: (2) CalculateGroundTimes");
        }
        else
        {
           ilRC = CalculateGroundTimes(&pclTifaList[ilI][0],&pclTifaList[ilI-1][0],
                                       &pclTifdList[ilI][0],&pclTifdList[ilI+1][0],
                                       &pclOnblList[ilI][0],&pclOnblList[ilI-1][0],
                                       &pclOfblList[ilI][0],&pclOfblList[ilI+1][0],
                                       &pclAdidList[ilI][0],ilI,ilJoinedRecs,
                                       pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
           ilRC = CalculateMaxCEDATime(pclLastDay,pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
           dbg(TRACE,"UpdateFOGTAB: (3) CalculateGroundTimes");
        }
     }
/*
     if (strcmp(pclNewPabs,&pclPabsList[ilI][0]) != 0 ||
         strcmp(pclNewPaes,&pclPaesList[ilI][0]) != 0 ||
         strcmp(pclNewPdbs,&pclPdbsList[ilI][0]) != 0 ||
         strcmp(pclNewPdes,&pclPdesList[ilI][0]) != 0)
*/
     if (CheckTolerance(pclNewPabs,&pclPabsList[ilI][0],pclNewPaes,&pclPaesList[ilI][0],
                        pclNewPdbs,&pclPdbsList[ilI][0],pclNewPdes,&pclPdesList[ilI][0]) == 0)
     {
        sprintf(pclUpdBuf,"PABS='%s',PAES='%s',PDBS='%s',PDES='%s'",
                pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
        strcpy(pclFieldList,"PABS,PAES,PDBS,PDES,TIFA,TIFD,ADID,RKEY,FTYP,FLNO,URNO");
        sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%c,%s,%s,%s,%s",pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes,
                &pclTifaList[ilI][0],&pclTifdList[ilI][0],
                pclAdidList[ilI][0],pcpRkey,&pclFtypList[ilI][0],
                &pclFlnoList[ilI][0],&pclUrnoList[ilI][0]);
       ilRC = CheckBcMode(pclNewPabs,&pclPabsList[ilI][0],pclNewPaes,&pclPaesList[ilI][0],
                          pclNewPdbs,&pclPdbsList[ilI][0],pclNewPdes,&pclPdesList[ilI][0],
                          pclFieldList,pclDataList);
        sprintf(pclSqlBuf,"UPDATE AFTTAB SET %s ",pclUpdBuf);
        sprintf(pclSelectBuf,"WHERE URNO = %s",&pclUrnoList[ilI][0]);
        strcat(pclSqlBuf,pclSelectBuf);
        dbg(TRACE,"UpdateRotation: <%s>",pclSqlBuf);
        slCursor = 0;
        slFkt = START;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        if (ilRCdb == DB_SUCCESS)
        {
           commit_work();
           ilRC = TriggerBchdlAction("UFR","AFTTAB",&pclUrnoList[ilI][0],pclFieldList,
                                     pclDataList,TRUE,TRUE);
        }
        close_my_cursor(&slCursor);
     }
  }
  if (ilJoinedRecs > 1)
  {
     ilRC = UpdFOGTAB(&pclUrnoList[0][0],pclFirstDay,pclLastDay,"R",
                      &pclOnblList[0][0],&pclOfblList[ilJoinedRecs-1][0],pcpRkey,
                      &pclAdidList[ilJoinedRecs-1][0]);
  }
  else
  {
     ilRC = UpdFOGTAB(&pclUrnoList[0][0],pclFirstDay,pclLastDay,&pclAdidList[0][0],
                      &pclOnblList[0][0],"",pcpRkey,&pclAdidList[0][0]);
  }

  return ilRC;
} /* End of UpdateRotation */


static int UpdFOGTAB(char *pcpUrno, char *pcpOgbs, char *pcpOges, char *pcpType,
                     char *pcpOnbl, char *pcpOfbl, char *pcpRkey, char *pcpAdid)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  int ilRecInFOG;
  int ilLen;
  char pclFogUrno[32];
  char pclFogOgbs[32];
  char pclFogOges[32];
  char pclFogFtyp[32];
  char pclFogStat[32];
  char pclFieldList[L_BUFF];
  char pclDataList[L_BUFF];
  char pclFieldValues[L_BUFF];
  char pclNewUrno[32];
  char pclUpdBuf[L_BUFF];
  char pclType[8];
  char pclStat[8];
  int ilInsert;
  int ilDelete;
  long llTime1;
  long llTime2;
  long llTime;
  int ilDebugLevel;

  dbg(DEBUG,"UpdFOGTAB: <URNO> <OGBS> <OGES> <TYPE> <ONBL> <OFBL> <RKEY> <ADID>");
  dbg(DEBUG,"UpdFOGTAB: <%s> <%s> <%s> <%s> <%s> <%s> <%s> <%s>",
      pcpUrno,pcpOgbs,pcpOges,pcpType,pcpOnbl,pcpOfbl,pcpRkey,pcpAdid);
  strcpy(pclSqlBuf,"SELECT URNO,OGBS,OGES,FTYP,STAT FROM FOGTAB ");
  sprintf(pclSelectBuf,"WHERE FLNU = %s",pcpUrno);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);
  ilRecInFOG = FALSE;
  if (ilRCdb == DB_SUCCESS)
  {
     ilRecInFOG = TRUE;
     BuildItemBuffer(pclDataBuf,"",5,",");
     ilLen = get_real_item(pclFogUrno,pclDataBuf,1);
     ilLen = get_real_item(pclFogOgbs,pclDataBuf,2);
     ilLen = get_real_item(pclFogOges,pclDataBuf,3);
     ilLen = get_real_item(pclFogFtyp,pclDataBuf,4);
     ilLen = get_real_item(pclFogStat,pclDataBuf,5);
  }

  ilInsert = FALSE;
  ilDelete = FALSE;

  if (strcmp(pcpType,"D") == 0)
  {
     if (ilRecInFOG == TRUE)
     {
        ilDelete = TRUE;
     }
  }
  else
  {
     if (strcmp(pcpType,"A") == 0)
     {
        strcpy(pclType,"A");
        strcpy(pclStat,"O");
        if (ilRecInFOG == FALSE)
        {
           if (strlen(pcpOnbl) > 0)
           {
              ilInsert = TRUE;
           }
        }
        else
        {
           if (strlen(pcpOnbl) == 0)
           {
              ilDelete = TRUE;
           }
        }
     }
     else
     {
        llTime1 = GetSecondsFromCEDATime(pcpOgbs);
        llTime2 = GetSecondsFromCEDATime(pcpOges);
        llTime = llTime2 - llTime1;
        if (llTime > igOnGroundTimeLimit && strcmp(pcgCheckOnGroundTimeLimit,"YES") == 0)
        {
           strcpy(pclType,"R");
           if (strlen(pcpOnbl) == 0)
           {
              strcpy(pclStat,"S");
           }
           else
           {
              if (strlen(pcpOfbl) == 0 || strcmp(pcpOges,pcgPaesArr) == 0)
              {
                 strcpy(pclStat,"O");
              }
              else
              {
                 strcpy(pclStat,"C");
              }
           }
           if (ilRecInFOG == FALSE)
           {
              ilInsert = TRUE;
           }
        }
        else
        {
           strcpy(pclType,"P");
           strcpy(pclStat,"O");
           if (strlen(pcpOnbl) == 0)
           {
              if (ilRecInFOG == TRUE)
              {
                 ilDelete = TRUE;
              }
           }
           else
           {
              if (strlen(pcpOfbl) != 0)
              {
                 if (ilRecInFOG == TRUE && *pcpAdid != 'B')
                 {
                    ilDelete = TRUE;
                 }
              }
              else
              {
                 if (ilRecInFOG == FALSE)
                 {
                    ilInsert = TRUE;
                 }
              }
           }
        }
     }
  }

  if (ilDelete == TRUE)
  {
     strcpy(pclSqlBuf,"DELETE FROM FOGTAB ");
     sprintf(pclSelectBuf,"WHERE FLNU = %s",pcpUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"UpdFOGTAB: <%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     commit_work();
     close_my_cursor(&slCursor);
     if (igValidate == TRUE)
        igValDel++;
     return ilRC;
  }

  if (ilInsert == TRUE)
  {
     if (strcmp(pcpUrno,pcpRkey) != 0)
     {
        dbg(TRACE,"UpdFOGTAB: ATTENTION: URNO <%s> and RKEY <%s> is different",pcpUrno,pcpRkey);
     }
     else
     {
        ilDebugLevel = debug_level;
        debug_level = 0;
         /*
        GetNextValues(pclNewUrno,1);
        UFIS-1485
        */
        DB_LoadUrnoList(1,pclNewUrno,pcgTableKey);
        debug_level = ilDebugLevel;
        strcpy(pclFieldList,"URNO,HOPO,FLNU,OGBS,OGES,FTYP,STAT");
/*
        sprintf(pclFieldValues,"%s,'%s',%s,'%s','%s','%s','%s'",
                pclNewUrno,pcgHomeAp,pcpUrno,pcpOgbs,pcpOges,pclType,pclStat);
*/
        sprintf(pclFieldValues,"%s,'%s',%s,'%s','%s','%s','%s'",
                pclNewUrno,pcgHomeAp,pcpRkey,pcpOgbs,pcpOges,pclType,pclStat);
        sprintf(pclSqlBuf,"INSERT INTO FOGTAB FIELDS(%s) VALUES(%s)",
                pclFieldList,pclFieldValues);
/*
        sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%s",
                pclNewUrno,pcgHomeAp,pcpUrno,pcpOgbs,pcpOges,pclType,pclStat);
*/
        sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%s",
                pclNewUrno,pcgHomeAp,pcpRkey,pcpOgbs,pcpOges,pclType,pclStat);
        slCursor = 0;
        slFkt = START;
        dbg(DEBUG,"UpdFOGTAB: <%s>",pclSqlBuf);
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        if (ilRCdb == DB_SUCCESS)
        {
           commit_work();
           ilRC = TriggerBchdlAction("IRT","FOGTAB",pclNewUrno,pclFieldList,
                                     pclDataList,FALSE,TRUE);
        }
        close_my_cursor(&slCursor);
        if (igValidate == TRUE)
           igValIns++;
     }
  }
  else
  {
     if (ilRecInFOG == TRUE)
     {
        if (strcmp(pclFogOgbs,pcpOgbs) != 0 || strcmp(pclFogOges,pcpOges) != 0 ||
            strcmp(pclFogFtyp,pclType) != 0 || strcmp(pclFogStat,pclStat) != 0)
        {
           sprintf(pclUpdBuf,"OGBS='%s',OGES='%s',FTYP='%s',STAT='%s'",
                   pcpOgbs,pcpOges,pclType,pclStat);
           if (igValidate == TRUE)
           {
              strcat(pclUpdBuf,",CHCK=' '");
              igValUpd++;
           }
           sprintf(pclSqlBuf,"UPDATE FOGTAB SET %s ",pclUpdBuf);
           sprintf(pclSelectBuf,"WHERE URNO = %s",pclFogUrno);
           strcat(pclSqlBuf,pclSelectBuf);
           if (igValidate == TRUE)
           {
              strcpy(pclFieldList,"OGBS,OGES,FTYP,STAT,CHCK,URNO");
              sprintf(pclDataList,"%s,%s,%s,%s, ,%s",pcpOgbs,pcpOges,pclType,pclStat,pclFogUrno);
           }
           else
           {
              strcpy(pclFieldList,"OGBS,OGES,FTYP,STAT,URNO");
              sprintf(pclDataList,"%s,%s,%s,%s,%s",pcpOgbs,pcpOges,pclType,pclStat,pclFogUrno);
           }
           dbg(DEBUG,"UpdFOGTAB: <%s>",pclSqlBuf);
           slCursor = 0;
           slFkt = START;
           ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
           if (ilRCdb == DB_SUCCESS)
           {
              commit_work();
              ilRC = TriggerBchdlAction("URT","FOGTAB",pclFogUrno,pclFieldList,
                                        pclDataList,FALSE,TRUE);
           }
           close_my_cursor(&slCursor);
        }
        else
        {
           if (igValidate == TRUE)
           {
              sprintf(pclSqlBuf,"UPDATE FOGTAB SET CHCK = ' ' ",pclUpdBuf);
              sprintf(pclSelectBuf,"WHERE URNO = %s",pclFogUrno);
              strcat(pclSqlBuf,pclSelectBuf);
              slCursor = 0;
              slFkt = START;
              ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
              if (ilRCdb == DB_SUCCESS)
                 commit_work();
              close_my_cursor(&slCursor);
              igValOK++;
           }
        }
     }
  }

  return ilRC;
} /* End of UpdFOGTAB */


static int CheckTolerance(char *pcpNewPabs, char *pcpOldPabs, char *pcpNewPaes, char *pcpOldPaes,
                          char *pcpNewPdbs, char *pcpOldPdbs, char *pcpNewPdes, char *pcpOldPdes)
{
  int ilRC = 1;

  ilRC = CompareTime(pcpNewPabs,pcpOldPabs,1);
  if (ilRC == 1)
     ilRC = CompareTime(pcpNewPaes,pcpOldPaes,2);
  if (ilRC == 1)
     ilRC = CompareTime(pcpNewPdbs,pcpOldPdbs,2);
  if (ilRC == 1)
     ilRC = CompareTime(pcpNewPdes,pcpOldPdes,1);

  return ilRC;
} /* End of CheckTolerance */


static int CompareTime(char *pcpTime1, char *pcpTime2, int ipFac)
{
  int ilRC = RC_SUCCESS;
  int ilResult = 1;
  char pclTime1[16];
  char pclTime2[16];

  strcpy(pclTime1,pcpTime1);
  strcpy(pclTime2,pcpTime2);
  if (strcmp(pclTime1,pclTime2) < 0)
  {
     ilRC = MyAddSecondsToCEDATime(pclTime1,igTolerance/ipFac,1);
     if (strcmp(pclTime1,pclTime2) <= 0)
        ilResult = 0;
  }
  else if (strcmp(pclTime1,pclTime2) > 0)
  {
     ilRC = MyAddSecondsToCEDATime(pclTime2,igTolerance/ipFac,1);
     if (strcmp(pclTime1,pclTime2) >= 0)
        ilResult = 0;
  }

  return ilResult;
} /* End of CompareTime */


static int ValidateFOGTAB()
{
  int ilRC = RC_SUCCESS;
  int ilI;
  char pclCurrentTimeEnd[32];
  char pclWorkDay[32];
  char pclLastDay[32];
  int ilSavDebugLevel;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  int ilCount;

  dbg(TRACE,"================ Now Start Validate FOGTAB ================");
  dbg(DEBUG,"Current Time = <%s>",pcgCurrentTime);

  igValidate = TRUE;
  ilSavDebugLevel = debug_level;
  debug_level = TRACE;
  strcpy(pclLastDay,pcgCurrentTime);
  pclLastDay[8] = '\0';
  strcat(pclLastDay,"235959");
  sprintf(pclSqlBuf,"UPDATE FOGTAB SET CHCK = '1' WHERE OGBS <= '%s'",pclLastDay);
  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  commit_work();
  close_my_cursor(&slCursor);

  igValInsTot = 0;
  igValDelTot = 0;
  igValUpdTot = 0;
  igValOKTot = 0;
  strcpy(pclLastDay,pcgCurrentTime);
  pclLastDay[8] = '\0';
  strcpy(pclSqlBuf,"SELECT MIN(OGBS) FROM FOGTAB");
  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclWorkDay);
  close_my_cursor(&slCursor);
  pclWorkDay[8] = '\0';
  while (strcmp(pclWorkDay,pclLastDay) <= 0)
  {
     dbg(TRACE,"Validate Day <%s>",pclWorkDay);
     igValIns = 0;
     igValDel = 0;
     igValUpd = 0;
     igValOK = 0;
     ilCount = 0;
     strcpy(pclSqlBuf,"SELECT URNO FROM AFTTAB ");
     sprintf(pclSelectBuf,
             "WHERE TIFA LIKE '%s%%' AND DES3 = '%s' AND (RTYP = 'S' OR RTYP = ' ') AND ADID = 'A'",
             pclWorkDay,pcgHomeAp);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     while (ilRCdb == DB_SUCCESS)
     {
        ilCount++;
        ilRC = UpdateFOGTAB(pclDataBuf);
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     }
     close_my_cursor(&slCursor);
     dbg(TRACE,"There were %d arrival records of AFT validated",ilCount);
     ilCount = 0;
     strcpy(pclSqlBuf,"SELECT URNO FROM AFTTAB ");
     sprintf(pclSelectBuf,
             "WHERE TIFD LIKE '%s%%' AND ORG3 = '%s' AND (RTYP = 'S' OR RTYP = ' ') AND ADID = 'D'",
             pclWorkDay,pcgHomeAp);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     while (ilRCdb == DB_SUCCESS)
     {
        ilCount++;
        ilRC = UpdateFOGTAB(pclDataBuf);
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     }
     close_my_cursor(&slCursor);
     dbg(TRACE,"There were %d departure records of AFT validated",ilCount);
     ilCount = 0;
     strcpy(pclSqlBuf,"SELECT URNO FROM AFTTAB ");
     sprintf(pclSelectBuf,
             "WHERE TIFA LIKE '%s%%' AND DES3 = '%s' AND RTYP = 'J' AND ADID = 'A'",
             pclWorkDay,pcgHomeAp);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     while (ilRCdb == DB_SUCCESS)
     {
        ilCount++;
        ilRC = UpdateFOGTAB(pclDataBuf);
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     }
     close_my_cursor(&slCursor);
     dbg(TRACE,"There were %d rotation records of AFT validated",ilCount);
     dbg(TRACE,"There were %d FOG records deleted",igValDel);
     dbg(TRACE,"There were %d FOG records inserted",igValIns);
     dbg(TRACE,"There were %d FOG records updated",igValUpd);
     dbg(TRACE,"There were %d FOG records OK",igValOK);
     igValInsTot += igValIns;
     igValDelTot += igValDel;
     igValUpdTot += igValUpd;
     igValOKTot += igValOK;
     strcat(pclWorkDay,"120000");
     ilRC = MyAddSecondsToCEDATime(pclWorkDay,24 * 60 * 60,1);
     pclWorkDay[8] = '\0';
  }
  sprintf(pclSqlBuf,"SELECT COUNT(*) FROM FOGTAB WHERE CHCK = '1'");
  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);
  dbg(TRACE," ");
  dbg(TRACE,"Summary:");
  dbg(TRACE,"There were %d FOG records deleted",igValDelTot);
  dbg(TRACE,"There were %d FOG records inserted",igValInsTot);
  dbg(TRACE,"There were %d FOG records updated",igValUpdTot);
  dbg(TRACE,"There were %d FOG records OK",igValOKTot);
  dbg(TRACE,"There were %d FOG records remaining and deleted",atoi(pclDataBuf));
  if (atoi(pclDataBuf) > 0)
  {
     sprintf(pclSqlBuf,"DELETE FROM FOGTAB WHERE CHCK = '1'");
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     commit_work();
     close_my_cursor(&slCursor);
  }

  dbg(TRACE," ");
  dbg(TRACE,"Start: %s",pcgCurrentTime);
  ilRC = TimeToStr(pclCurrentTimeEnd,time(NULL));
  dbg(TRACE,"End:   %s",pclCurrentTimeEnd);
  dbg(TRACE,"================ Validate FOGTAB Finished =================");
  debug_level = ilSavDebugLevel;
  igValidate = FALSE;

  return RC_SUCCESS;
} /* End of ValidateFOGTAB */


static int TriggerBchdlAction(char *pcpCmd, char *pcpTable, char *pcpSelection,
                              char *pcpFields, char *pcpData, int ipBchdl, int ipAction)
{
  int ilRC = RC_SUCCESS;

  if (ipBchdl == TRUE && strcmp(pcgTriggerBchdl,"YES") == 0 &&
      strstr(pcgTwStart,".NBC.") == NULL)
  {
      if (strcmp(pcgTriggerAFTBchdl, "YES") == 0 && strcmp(pcpTable, "AFTAB") == 0) {
            (void) tools_send_info_flag(1900, 0, "foghdl", "", "CEDA", "", "", pcgTwStart, pcgTwEnd,
                    pcpCmd, pcpTable, pcpSelection, pcpFields, pcpData, 0);
            dbg(DEBUG, "Send Broadcast: <%s><%s><%s><%s><%s>",
                    pcpCmd, pcpTable, pcpSelection, pcpFields, pcpData);
        } else if (strcmp(pcgTriggerBchdl, "YES") == 0) {
            (void) tools_send_info_flag(1900, 0, "foghdl", "", "CEDA", "", "", pcgTwStart, pcgTwEnd,
                    pcpCmd, pcpTable, pcpSelection, pcpFields, pcpData, 0);
            dbg(DEBUG, "Send Broadcast: <%s><%s><%s><%s><%s>",
                    pcpCmd, pcpTable, pcpSelection, pcpFields, pcpData);
        }
  }



  if (ipAction == TRUE && strcmp(pcgTriggerAction,"YES") == 0)
  {
     (void) tools_send_info_flag(7400,0,"foghdl","","CEDA","","",pcgTwStart,pcgTwEnd,
                                 pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData,0);
     dbg(DEBUG,"Send to ACTION: <%s><%s><%s><%s><%s>",
         pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData);
  }

  return ilRC;
} /* End of TriggerBchdlAction */


/*******************************************++++++++++*************************/
/*   GetSecondsFromCEDATime                                                   */
/*                                                                            */
/*   Serviceroutine, rechnet einen Datumseintrag im Cedaformat in Seconds um  */
/*   Input:  char *  Zeiger auf CEDA-Zeitpuffer                               */
/*   Output: long    Zeitwert                                                 */
/******************************************************************************/

static long GetSecondsFromCEDATime(char *pcpDateTime)
{
  long rc = 0;
  int year;
  char ch_help[5];
  struct tm *tstr1 = NULL, t1;
  struct tm *CurTime, t2;
  time_t llTime;
  time_t llUtcTime;
  time_t llLocalTime;

  CurTime = &t2;
  llTime = time(NULL);
  CurTime = (struct tm *)gmtime(&llTime);
  CurTime->tm_isdst = 0;
  llUtcTime = mktime(CurTime);
  CurTime = (struct tm *)localtime(&llTime);
  CurTime->tm_isdst = 0;
  llLocalTime = mktime(CurTime);
  igDiffUtcToLocal = llLocalTime - llUtcTime;
  dbg(DEBUG,"UTC: <%ld> , Local: <%ld> , Diff: <%ld>",llUtcTime,llLocalTime,igDiffUtcToLocal);

  memset(&t1,0x00,sizeof(struct tm));

  tstr1 = &t1;

  memset(ch_help,0x00,5);
  strncpy(ch_help,pcpDateTime,4);
  year = atoi(ch_help);
  tstr1->tm_year = year - 1900;

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[4],2);
  tstr1->tm_mon = atoi(ch_help) -1;

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[6],2);
  tstr1->tm_mday = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[8],2);
  tstr1->tm_hour = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[10],2);
  tstr1->tm_min = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[12],2);
  tstr1->tm_sec = atoi(ch_help);

  tstr1->tm_wday = 0;
  tstr1->tm_yday = 0;
  tstr1->tm_isdst = 0;
  rc = mktime(tstr1);

  rc = rc + igDiffUtcToLocal; /* add difference between local and utc */

  return(rc);
} /* end of GetSecondsFromCEDATime() */


static int InitRog()
{
  int ilRC = RC_SUCCESS;
  int ilI;
  char pclCurrentTimeEnd[32];
  int ilSavDebugLevel;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  int ilLen;
  char pclRegn[32];

  dbg(TRACE,"================ Now Start Initializing ROGTAB ================");
  if (strcmp(pcgInitRog,"YES") == 0)
  {
     ilSavDebugLevel = debug_level;
     if (strcmp(pcgDebugLevelInit,"DEBUG") == 0)
     {
        debug_level = DEBUG;
     }
     else
     {
        debug_level = TRACE;
     }
     dbg(DEBUG,"Current Time = <%s>",pcgCurrentTime);

     sprintf(pclSqlBuf,"TRUNCATE TABLE ROGTAB");
     slCursor = 0;
     slFkt = START;
     dbg(TRACE,"<%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     commit_work();
     close_my_cursor(&slCursor);

     igTotCountRead = 0;
     igTotCountIns = 0;
     igRegnCountRead = 0;
     igRegnCountIns = 0;
     igDiffRegn = 0;
     strcpy(pclSqlBuf,"SELECT DISTINCT REGN FROM AFTTAB ");
     if (strlen(pcgRogBegin) == 0)
     {
        if (strlen(pcgRogEnd) == 0)
        {
           sprintf(pclSelectBuf,"");
        }
        else
        {
           sprintf(pclSelectBuf,"WHERE TIFA <= '%s'",pcgRogEnd);
        }
     }
     else
     {
        if (strlen(pcgRogEnd) == 0)
        {
           sprintf(pclSelectBuf,"WHERE TIFA >= '%s'",pcgRogBegin);
        }
        else
        {
           sprintf(pclSelectBuf,"WHERE TIFA BETWEEN '%s' AND '%s'",pcgRogBegin,pcgRogEnd);
        }
     }
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     dbg(TRACE,"<%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     while (ilRCdb == DB_SUCCESS)
     {
        BuildItemBuffer(pclDataBuf,"",1,",");
        ilLen = get_real_item(pclRegn,pclDataBuf,1);
        if (ilLen > 0)
        {
           if (strlen(pcgRogBegin) == 0)
           {
              ilRC = BuildROGTAB(pclRegn,NULL);
              strcpy(pcgRACDataList,"");
           }
           else
           {
              ilRC = BuildROGTAB(pclRegn,pcgRogBegin);
              strcpy(pcgRACDataList,"");
           }
           dbg(TRACE,"There were %d / %d records for REGN <%s> read / inserted",
               igRegnCountRead,igRegnCountIns,pclRegn);
           igRegnCountRead = 0;
           igRegnCountIns = 0;
           igDiffRegn++;
        }
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     }
     close_my_cursor(&slCursor);
     dbg(TRACE," ");
     dbg(TRACE,"There were %d / %d records for %d registrations read / inserted",
         igTotCountRead,igTotCountIns,igDiffRegn);

     dbg(TRACE," ");
     dbg(TRACE,"Start: %s",pcgCurrentTime);
     ilRC = TimeToStr(pclCurrentTimeEnd,time(NULL));
     dbg(TRACE,"End:   %s",pclCurrentTimeEnd);
     debug_level = ilSavDebugLevel;
  }

  dbg(TRACE,"================ Initializing ROGTAB Finished =================");

  return RC_SUCCESS;
} /* End of InitRog */


static int BuildROGTAB(char *pcpRegn, char *pcpStartTime)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSelectBuf[XL_BUFF];
  char pclSqlBuf[XL_BUFF];
  char pclDataBuf[512000];
  int ilRCdbRd = DB_SUCCESS;
  short slFktRd;
  short slCursorRd;
  char pclSelectBufRd[XL_BUFF];
  char pclSqlBufRd[XL_BUFF];
  char pclDataBufRd[512000];
  int ilRCdbFirst = DB_SUCCESS;
  short slFktFirst;
  short slCursorFirst;
  char pclSelectBufFirst[XL_BUFF];
  char pclSqlBufFirst[XL_BUFF];
  char pclDataBufFirst[512000];
  int ilLen;
  char pclUrno[32];
  char pclRegn[32];
  char pclLand[32];
  char pclAirb[32];
  char pclTifa[32];
  char pclTifd[32];
  char pclFtyp[32];
  char pclRkey[32];
  char pclRtyp[32];
  char pclAdid[32];
  char pclOrg3[32];
  char pclDes3[32];
  char pclRogUrno[32];
  char pclRogAurn[32];
  char pclRogDurn[32];
  char pclRogRkey[32];
  char pclRogLand[32];
  char pclRogAirb[32];
  char pclRogFtyp[32];
  char pclRogStat[32];
  char pclRogSeqn[32];
  char pclNewRogFtyp[32];
  char pclNewRogStat[32];
  char pclTime[32];
  char pclSeqn[32];
  char pclSelList[128];
  char pclFieldList[32];
  char pclStartTime[32];
  char pclEndTime[32];
  int ilDoOwnCheck;
  int ilTakeRecord;
  int ilFirstRecord;
  int ilMissingRecordFound;
  int ilContinue;
  int ilRogIdx;
  int ilRogIdxNew;

  int ilRogRecCnt = 0;
  char pclUrnoList[512000];
  char pclTmpBuf[2048];
  char pclRACDest[16] = "";
  int ilMaxItm = 0;
  int ilCurItm = 0;
  int ilRacQue = 0;

  dbg(TRACE,"BEGIN OF BUILD ROGTAB");
  igCurRogDat = -1;

  if (pcpStartTime == NULL)
  {
     dbg(TRACE,"PROCEDURE WITHOUT STARTTIME");
     strcpy(pclSqlBuf,"DELETE FROM ROGTAB ");
     sprintf(pclSelectBuf,"WHERE REGN = '%s'",
             pcpRegn);
  }
  else
  {
     strcpy(pclStartTime,pcpStartTime);
     dbg(TRACE,"PROCEDURE WITH STARTTIME <%s>",pcpStartTime);
     strcpy(pclSqlBuf,"SELECT LAND,AIRB FROM ROGTAB ");
     sprintf(pclSelectBuf,"WHERE REGN = '%s' AND (LAND >= '%s' OR AIRB >= '%s') ORDER BY SEQN",
             pcpRegn,pclStartTime,pclStartTime);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     dbg(TRACE,"BuildROGTAB: <%s>",pclSqlBuf);
     ilRogRecCnt = 0;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     while (ilRCdb == DB_SUCCESS)
     {
        ilRogRecCnt++;
        BuildItemBuffer(pclDataBuf,"",2,",");
        dbg(TRACE,"GOT <%s>",pclDataBuf);
        ilLen = get_real_item(pclRogLand,pclDataBuf,1);
        ilLen = get_real_item(pclRogAirb,pclDataBuf,2);
        if (strlen(pclRogLand) > 0)
        {
           if (strcmp(pclRogLand,pclStartTime) < 0)
           {
              strcpy(pclStartTime,pclRogLand);
           }
        }
        if (strlen(pclRogAirb) > 0)
        {
           if (strcmp(pclRogAirb,pclStartTime) < 0)
           {
              strcpy(pclStartTime,pclRogAirb);
           }
        }
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     }
     close_my_cursor(&slCursor);
     dbg(TRACE,"FOUND %d RECORDS OF <%s> IN ROGTAB",ilRogRecCnt,pcpRegn);
     strcpy(pclSqlBuf,"DELETE FROM ROGTAB ");
     sprintf(pclSelectBuf,"WHERE REGN = '%s' AND (LAND >= '%s' OR AIRB >= '%s')",
             pcpRegn,pclStartTime,pclStartTime);
  }
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  dbg(TRACE,"BuildROGTAB: <%s>",pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  commit_work();
  close_my_cursor(&slCursor);

  if (pcpStartTime != NULL && igUseArrayInsert == TRUE)
  {
     dbg(TRACE,"BUILD ROGTAB: GOT A TIME (%s) AND USING ORA ARRAY",pcpStartTime);
     strcpy(pclRogSeqn,"0000000000");
     strcpy(pclStartTime,pcgRogBegin);
     strcat(pclStartTime,"000000");
     strcpy(pclSqlBuf,"SELECT URNO,HOPO,REGN,AURN,DURN,RKEY,LAND,AIRB,FTYP,STAT,FTPA,FTPD,SEQN,SEQS FROM ROGTAB ");
     sprintf(pclSelectBuf,"WHERE REGN = '%s' ORDER BY SEQN DESC",
             pcpRegn);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     dbg(TRACE,"BuildROGTAB: <%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     while (ilRCdb == DB_SUCCESS)
     {
        BuildItemBuffer(pclDataBuf,"",14,",");
        ilLen = get_real_item(pclRogStat,pclDataBuf,10);
        ilLen = get_real_item(pclRogSeqn,pclDataBuf,13);
        if (*pclRogStat == 'B')
        {
           ilLen = get_real_item(&rgRogDat[0].pclUrno[0],pclDataBuf,1);
           ilLen = get_real_item(&rgRogDat[0].pclHopo[0],pclDataBuf,2);
           ilLen = get_real_item(&rgRogDat[0].pclRegn[0],pclDataBuf,3);
           ilLen = get_real_item(&rgRogDat[0].pclAurn[0],pclDataBuf,4);
           ilLen = get_real_item(&rgRogDat[0].pclDurn[0],pclDataBuf,5);
           ilLen = get_real_item(&rgRogDat[0].pclRkey[0],pclDataBuf,6);
           ilLen = get_real_item(&rgRogDat[0].pclLand[0],pclDataBuf,7);
           ilLen = get_real_item(&rgRogDat[0].pclAirb[0],pclDataBuf,8);
           ilLen = get_real_item(&rgRogDat[0].pclFtyp[0],pclDataBuf,9);
           ilLen = get_real_item(&rgRogDat[0].pclStat[0],pclDataBuf,10);
           ilLen = get_real_item(&rgRogDat[0].pclFtpa[0],pclDataBuf,11);
           ilLen = get_real_item(&rgRogDat[0].pclFtpd[0],pclDataBuf,12);
           strcpy(&rgRogDat[0].pclSeqn[0]," ");
           strcpy(&rgRogDat[0].pclSeqs[0]," ");
           strcpy(&rgRogDat[0].pclFlag[0]," ");
           igCurRogDat = 1;
           ilLen = get_real_item(pclStartTime,pclDataBuf,7);
           ilRCdb = DB_ERROR;
           dbg(TRACE,"STOP LOOP: FOUND RECORD WITH STATUS 'B' AT SEQN %s",pclRogSeqn);
        }
        else
        {
           slFkt = NEXT;
           ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        }
     }
     strcpy(pclSqlBuf,"DELETE FROM ROGTAB ");
     sprintf(pclSelectBuf,"WHERE REGN = '%s' AND SEQN >= '%s'",
             pcpRegn,pclRogSeqn);
     close_my_cursor(&slCursor);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     dbg(TRACE,"BuildROGTAB: <%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     commit_work();
     close_my_cursor(&slCursor);
  }
  /*/*SELECT URNO,REGN,LAND,AIRB,TIFA,TIFD,FTYP,RKEY,RTYP,ADID,ORG3,DES3 FROM AFTTAB WHERE REGN = 'SXOBB' AND ((TIFA >= '20110923084500') OR (TIFD >= '20110923084500')) AND FTYP NOT IN (
'T','G','X') DECODE(ADID,'A',TIFA,'D',TIFD)*/
  strcpy(pclSqlBuf,"SELECT URNO,REGN,LAND,AIRB,TIFA,TIFD,FTYP,RKEY,RTYP,ADID,ORG3,DES3 FROM AFTTAB ");
  if (pcpStartTime == NULL)
  {
     /* UFIS-1015
        GFO modified "ORDER BY TIFA" TO  "ORDER BY DECODE(ADID,'A',TIFA,'D',TIFD,'B',TIFA)"
      */
     sprintf(pclSelectBuf,"WHERE REGN = '%s' AND FTYP NOT IN (%s) ORDER BY DECODE(ADID,'A',TIFA,'D',TIFD,'B',TIFA) ",
             pcpRegn,pcgExclFtyp);
     ilDoOwnCheck = FALSE;
  }
  else
  {
     if (ilSelectVers == 2) {
         /* UFIS-1015
            GFO modified "ORDER BY TIFA" TO  "ORDER BY DECODE(ADID,'A',TIFA,'D',TIFD,'B',TIFA)"
         */
        sprintf(pclSelectBuf,"WHERE REGN = '%s' AND FTYP NOT IN (%s) ORDER BY DECODE(ADID,'A',TIFA,'D',TIFD,'B',TIFA)",
                pcpRegn,pcgExclFtyp);
     } else {
         /* UFIS-1015
            GFO modified "ORDER BY TIFA" TO  "ORDER BY DECODE(ADID,'A',TIFA,'D',TIFD,'B',TIFA)"
            UFIS-2032
            GFO modified query switch to a time range  add another Variable for the end range "
         */
        /*
        sprintf(pclSelectBuf,
                "WHERE REGN = '%s' AND ((TIFA >= '%s') OR (TIFD >= '%s')) AND FTYP NOT IN (%s) ORDER BY DECODE(ADID,'A',TIFA,'D',TIFD,'B',TIFA)",
                pcpRegn,pclStartTime,pclStartTime,pcgExclFtyp);
         */
        strcpy(pclEndTime,pclStartTime);
        ilRC = MyAddSecondsToCEDATime(pclEndTime,igRogTimeEnd * 24 * 60 * 60,1);
        sprintf(pclSelectBuf,
                "WHERE REGN = '%s' AND ((TIFA BETWEEN '%s' AND '%s' ) OR (TIFD BETWEEN '%s' AND '%s' )) AND FTYP NOT IN (%s) ORDER BY DECODE(ADID,'A',TIFA,'D',TIFD,'B',TIFA)",
                pcpRegn,pclStartTime,pclEndTime,pclStartTime,pclEndTime,pcgExclFtyp);

     }
     ilDoOwnCheck = TRUE;
  }
  if (strcmp(pcgHomeAp,pcgSaveMyHopo) != 0)
  {
    dbg(TRACE,"============================================");
    dbg(TRACE,"DISPLAY HOPO NOW (VERIFY AIRPORT)");
    dbg(TRACE,"START TIME <%s> / HOME PORT <%s>",pclStartTime,pcgHomeAp);
    dbg(TRACE,"RESTORE HOPO AS CURRENT AIRPORT");
    strcpy(pcgHomeAp,pcgSaveMyHopo);
    dbg(TRACE,"START TIME <%s> / HOME PORT <%s>",pclStartTime,pcgHomeAp);
    dbg(TRACE,"============================================");
  }
  strcat(pclSqlBuf,pclSelectBuf);
  strcpy(pclUrnoList,"");
  slCursor = 0;
  slFkt = START;
  dbg(TRACE,"<%s>",pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  dbg(TRACE,"End of Selection");
  ilFirstRecord = TRUE;
  while (ilRCdb == DB_SUCCESS)
  {
     ilMissingRecordFound = FALSE;
     BuildItemBuffer(pclDataBuf,"",12,",");
     dbg(TRACE,"<%s>",pclDataBuf);
     ilLen = get_real_item(pclUrno,pclDataBuf,1);
     ilLen = get_real_item(pclRegn,pclDataBuf,2);
     ilLen = get_real_item(pclLand,pclDataBuf,3);
     ilLen = get_real_item(pclAirb,pclDataBuf,4);
     ilLen = get_real_item(pclTifa,pclDataBuf,5);
     ilLen = get_real_item(pclTifd,pclDataBuf,6);
     ilLen = get_real_item(pclFtyp,pclDataBuf,7);
     ilLen = get_real_item(pclRkey,pclDataBuf,8);
     ilLen = get_real_item(pclRtyp,pclDataBuf,9);
     ilLen = get_real_item(pclAdid,pclDataBuf,10);
     ilLen = get_real_item(pclOrg3,pclDataBuf,11);
     ilLen = get_real_item(pclDes3,pclDataBuf,12);
     if (ilDoOwnCheck == TRUE)
     {
        if ((strcmp(pclTifa,pclStartTime) >= 0 && strcmp(pclDes3,pcgHomeAp) == 0) ||
            (strcmp(pclTifd,pclStartTime) >= 0 && strcmp(pclOrg3,pcgHomeAp) == 0))
        {
           ilTakeRecord = TRUE;
           dbg(DEBUG,"DO_OWN_CHECK TRUE : TAKE THIS RECORD");
        }
        else
        {
           ilTakeRecord = FALSE;
           dbg(DEBUG,"DO_OWN_CHECK FALSE: DO NOT TAKE THIS RECORD");
        }
     }
     else
     {
        ilTakeRecord = TRUE;
        dbg(DEBUG,"TAKE_THIS_RECORD IS ALWAYS TRUE");
     }
     if (ilTakeRecord == TRUE)
     {
        dbg(TRACE,"Got Record with URNO = <%s>, RKEY = <%s>, ADID = <%s>",pclUrno,pclRkey,pclAdid);
        if (ilFirstRecord == TRUE)
        {
           if (strcmp(pclAdid,"D") == 0 && strcmp(pclRtyp,"J") == 0)
           {
              dbg(TRACE,"ADID IS 'D' AND RTYP IS 'J'");
              strcpy(pclSqlBufFirst,"SELECT URNO,REGN,LAND,AIRB,TIFA,TIFD,FTYP,RKEY,RTYP,ADID,ORG3,DES3 FROM AFTTAB ");
              sprintf(pclSelectBufFirst,"WHERE REGN = '%s' AND FTYP NOT IN (%s) AND ADID = 'A' AND RKEY = %s",
                      pcpRegn,pcgExclFtyp,pclRkey);
              strcat(pclSqlBufFirst,pclSelectBufFirst);
              slCursorFirst = 0;
              slFktFirst = START;
              dbg(TRACE,"Get missing arrival record: <%s>",pclSqlBufFirst);
              ilRCdbFirst = sql_if(slFktFirst,&slCursorFirst,pclSqlBufFirst,pclDataBufFirst);
              if (ilRCdbFirst == DB_SUCCESS)
              {
                 BuildItemBuffer(pclDataBufFirst,"",12,",");
                 ilLen = get_real_item(pclUrno,pclDataBufFirst,1);
                 ilLen = get_real_item(pclRegn,pclDataBufFirst,2);
                 ilLen = get_real_item(pclLand,pclDataBufFirst,3);
                 ilLen = get_real_item(pclAirb,pclDataBufFirst,4);
                 ilLen = get_real_item(pclTifa,pclDataBufFirst,5);
                 ilLen = get_real_item(pclTifd,pclDataBufFirst,6);
                 ilLen = get_real_item(pclFtyp,pclDataBufFirst,7);
                 ilLen = get_real_item(pclRkey,pclDataBufFirst,8);
                 ilLen = get_real_item(pclRtyp,pclDataBufFirst,9);
                 ilLen = get_real_item(pclAdid,pclDataBufFirst,10);
                 ilLen = get_real_item(pclOrg3,pclDataBufFirst,11);
                 ilLen = get_real_item(pclDes3,pclDataBufFirst,12);
                 dbg(TRACE,"Missing arrival record found, URNO = <%s>, RKEY = <%s>",pclUrno,pclRkey);
                 ilMissingRecordFound = TRUE;
              }
              else
              {
                 dbg(TRACE,"Missing arrival record not found");
              }
              close_my_cursor(&slCursorFirst);
           }
           ilFirstRecord = FALSE;
        }
        sprintf(pclTmpBuf,"#%s#",pclUrno);
	/* UFIS-425
	   This is causing wrong results when we have to Join a  Return Flight
	*/

        /* if (strstr(pclUrnoList,pclTmpBuf) == NULL)
        {
           strcat(pclUrnoList,pclTmpBuf);
	 */
           igTotCountRead++;
           igRegnCountRead++;

           if (strcmp(pclAdid,"A") == 0)
           { /* arrival record */
              if (igUseArrayInsert == TRUE)
              {
                 ilRCdbRd = DB_ERROR;
                 ilRogIdx = -1;
                 if (igAutoJoin == TRUE)
                    ilRC = SearchByAIRB(pclTifa,&ilRogIdx);
                 else
                    ilRC = SearchByRKEYDep(pclRkey,&ilRogIdx);
                 if (ilRogIdx >= 0)
                    ilRCdbRd = DB_SUCCESS;
              }
              else
              {
                 strcpy(pclSqlBufRd,"SELECT URNO,AURN,DURN,RKEY,LAND,AIRB,FTYP,STAT,SEQN FROM ROGTAB ");
                 if (igAutoJoin == TRUE)
                 {
                    sprintf(pclSelectBufRd,
                            "WHERE REGN = '%s' AND LAND = ' ' AND STAT = 'D' AND AIRB >= '%s' ORDER BY AIRB",
                            pclRegn,pclTifa);
                 }
                 else
                 {
                    sprintf(pclSelectBufRd,
                            "WHERE REGN = '%s' AND RKEY = %s AND LAND = ' ' AND STAT = 'D'",
                            pclRegn,pclRkey);
                 }
                 strcat(pclSqlBufRd,pclSelectBufRd);
                 slCursorRd = 0;
                 slFktRd = START;
                 /* dbg(TRACE,"<%s>",pclSqlBufRd); */
                 ilRCdbRd = sql_if(slFktRd,&slCursorRd,pclSqlBufRd,pclDataBufRd);
                 close_my_cursor(&slCursorRd);
              }
              if (ilRCdbRd == DB_SUCCESS && strcmp(pclFtyp,"X") != 0 &&
                  strcmp(pclFtyp,"N") != 0 && strcmp(pclFtyp,"D") != 0)
              {
                 if (igUseArrayInsert == TRUE)
                 {
                    strcpy(pclRogUrno,&rgRogDat[ilRogIdx].pclUrno[0]);
                    strcpy(pclRogAurn,&rgRogDat[ilRogIdx].pclAurn[0]);
                    strcpy(pclRogDurn,&rgRogDat[ilRogIdx].pclDurn[0]);
                    strcpy(pclRogRkey,&rgRogDat[ilRogIdx].pclRkey[0]);
                    strcpy(pclRogLand,&rgRogDat[ilRogIdx].pclLand[0]);
                    strcpy(pclRogAirb,&rgRogDat[ilRogIdx].pclAirb[0]);
                    strcpy(pclRogFtyp,&rgRogDat[ilRogIdx].pclFtyp[0]);
                    strcpy(pclRogStat,&rgRogDat[ilRogIdx].pclStat[0]);
                    strcpy(pclRogSeqn,&rgRogDat[ilRogIdx].pclSeqn[0]);
                 }
                 else
                 {
                    BuildItemBuffer(pclDataBufRd,"",9,",");
                    ilLen = get_real_item(pclRogUrno,pclDataBufRd,1);
                    ilLen = get_real_item(pclRogAurn,pclDataBufRd,2);
                    ilLen = get_real_item(pclRogDurn,pclDataBufRd,3);
                    ilLen = get_real_item(pclRogRkey,pclDataBufRd,4);
                    ilLen = get_real_item(pclRogLand,pclDataBufRd,5);
                    ilLen = get_real_item(pclRogAirb,pclDataBufRd,6);
                    ilLen = get_real_item(pclRogFtyp,pclDataBufRd,7);
                    ilLen = get_real_item(pclRogStat,pclDataBufRd,8);
                    ilLen = get_real_item(pclRogSeqn,pclDataBufRd,9);
                 }
                 if (strcmp(pclRogFtyp,"S") == 0)
                 {
                    if (strlen(pclLand) > 0)
                    {
                       strcpy(pclNewRogFtyp,"O");
                       strcpy(pclTime,pclLand);
                    }
                    else
                    {
                       strcpy(pclNewRogFtyp,"S");
                       strcpy(pclTime,pclTifa);
                    }
                 }
                 else
                 {
                    if (strlen(pclLand) > 0)
                    {
                       strcpy(pclNewRogFtyp,"H");
                       strcpy(pclTime,pclLand);
                    }
                    else
                    {
                       strcpy(pclNewRogFtyp,"E");
                       strcpy(pclTime,pclTifa);
                    }
                 }
                 if (strcmp(pclRogAirb,pclTime) >= 0)
                 {
                    strcpy(pclNewRogStat,"B");
dbg(TRACE,"AKL UPD 1");
                    ilRC = UpdROGTAB("A",pclRogUrno,pclUrno,pclTime,pclNewRogFtyp,
                                     pclNewRogStat,pclFtyp);
                 }
                 else
                 {
                    if (igAutoJoin == TRUE)
                    {
                       if (strlen(pclLand) == 0)
                       {
                          strcpy(pclTime,pclTifa);
dbg(TRACE,"AKL INS 1");
                          ilRC = InsROGTAB(pclRegn,pclUrno,"0",pclRkey,pclTifa,
                                           " ","S","A",pclFtyp," ");
                       }
                       else
                       {
                          strcpy(pclTime,pclLand);
dbg(TRACE,"AKL INS 2");
                          ilRC = InsROGTAB(pclRegn,pclUrno,"0",pclRkey,pclLand,
                                           " ","O","A",pclFtyp," ");
                       }
                       igTotCountIns++;
                       igRegnCountIns++;
                    }
                    else
                    {
                       strcpy(pclNewRogStat,"E");
dbg(TRACE,"AKL UPD 2");
                       ilRC = UpdROGTAB("A",pclRogUrno,pclUrno,pclTime,pclNewRogFtyp,
                                        pclNewRogStat,pclFtyp);
                    }
                 }
              }
              else
              {
                 if (strlen(pclLand) == 0)
                 {
                    strcpy(pclTime,pclTifa);
dbg(TRACE,"AKL INS 3");
                    ilRC = InsROGTAB(pclRegn,pclUrno,"0",pclRkey,pclTifa,
                                     " ","S","A",pclFtyp," ");
                 }
                 else
                 {
                    strcpy(pclTime,pclLand);
dbg(TRACE,"AKL INS 4");
                    ilRC = InsROGTAB(pclRegn,pclUrno,"0",pclRkey,pclLand,
                                     " ","O","A",pclFtyp," ");
                 }
                 igTotCountIns++;
                 igRegnCountIns++;
              }
           }
           else
           {
              if (strcmp(pclAdid,"D") == 0)
              { /* departure record */
                 strcpy(pclSqlBufRd,"SELECT URNO,AURN,DURN,RKEY,LAND,AIRB,FTYP,STAT FROM ROGTAB ");
                 if (igAutoJoin == TRUE)
                 {
                    if (igUseArrayInsert == TRUE)
                    {
                       ilRCdbRd = DB_ERROR;
                       ilRogIdx = -1;
                       ilRC = SearchByLAND(pclTifd,&ilRogIdx);
                       if (ilRogIdx >= 0)
                          ilRCdbRd = DB_SUCCESS;
                    }
                    else
                    {
                       sprintf(pclSelectBufRd,
                               "WHERE REGN = '%s' AND LAND <= '%s' AND FTPA <> 'X' AND FTPA <> 'N' AND FTPA <> 'D' ORDER BY LAND DESC",
                               pclRegn,pclTifd);
                       strcat(pclSqlBufRd,pclSelectBufRd);
                       slCursorRd = 0;
                       slFktRd = START;
                       /* dbg(TRACE,"<%s>",pclSqlBufRd); */
                       ilRCdbRd = sql_if(slFktRd,&slCursorRd,pclSqlBufRd,pclDataBufRd);
                    }
                    ilContinue = TRUE;
                    while (ilRCdbRd == DB_SUCCESS && ilContinue == TRUE)
                    {
                       if (igUseArrayInsert == TRUE)
                       {
                          strcpy(pclRogUrno,&rgRogDat[ilRogIdx].pclUrno[0]);
                          strcpy(pclRogAurn,&rgRogDat[ilRogIdx].pclAurn[0]);
                          strcpy(pclRogDurn,&rgRogDat[ilRogIdx].pclDurn[0]);
                          strcpy(pclRogRkey,&rgRogDat[ilRogIdx].pclRkey[0]);
                          strcpy(pclRogLand,&rgRogDat[ilRogIdx].pclLand[0]);
                          strcpy(pclRogAirb,&rgRogDat[ilRogIdx].pclAirb[0]);
                          strcpy(pclRogFtyp,&rgRogDat[ilRogIdx].pclFtyp[0]);
                          strcpy(pclRogStat,&rgRogDat[ilRogIdx].pclStat[0]);
                       }
                       else
                       {
                          BuildItemBuffer(pclDataBufRd,"",8,",");
                          ilLen = get_real_item(pclRogUrno,pclDataBufRd,1);
                          ilLen = get_real_item(pclRogAurn,pclDataBufRd,2);
                          ilLen = get_real_item(pclRogDurn,pclDataBufRd,3);
                          ilLen = get_real_item(pclRogRkey,pclDataBufRd,4);
                          ilLen = get_real_item(pclRogLand,pclDataBufRd,5);
                          ilLen = get_real_item(pclRogAirb,pclDataBufRd,6);
                          ilLen = get_real_item(pclRogFtyp,pclDataBufRd,7);
                          ilLen = get_real_item(pclRogStat,pclDataBufRd,8);
                       }
                       if (strlen(pclRogAirb) > 1)
                       {
                          if (strcmp(pclRogAirb,pclTifd) < 0)
                          {
                             ilContinue = FALSE;
                             ilRCdbRd = DB_ERROR;
                          }
                       }
                       else
                       {
                          ilContinue = FALSE;
                       }
                       if (ilContinue == TRUE)
                       {
                          if (igUseArrayInsert == TRUE)
                          {
                             ilRCdbRd = DB_ERROR;
                             ilRogIdxNew = ilRogIdx;
                             ilRC = SearchByLAND(pclTifd,&ilRogIdxNew);
                             if (ilRogIdxNew != ilRogIdx)
                             {
                                ilRCdbRd = DB_SUCCESS;
                                ilRogIdx = ilRogIdxNew;
                             }
                          }
                          else
                          {
                             slFktRd = NEXT;
                             ilRCdbRd = sql_if(slFktRd,&slCursorRd,pclSqlBufRd,pclDataBufRd);
                          }
                       }
                    }
                 }
                 else
                 {
                    if (igUseArrayInsert == TRUE)
                    {
                       ilRCdbRd = DB_ERROR;
                       ilRogIdx = -1;
                       ilRC = SearchByRKEYArr(pclRkey,&ilRogIdx);
                       if (ilRogIdx >= 0)
                          ilRCdbRd = DB_SUCCESS;
                    }
                    else
                    {
                       sprintf(pclSelectBufRd,
                               "WHERE REGN = '%s' AND RKEY = %s AND AIRB = ' ' AND STAT = 'A'",
                               pclRegn,pclRkey);
                       strcat(pclSqlBufRd,pclSelectBufRd);
                       slCursorRd = 0;
                       slFktRd = START;
                       /* dbg(TRACE,"<%s>",pclSqlBufRd); */
                       ilRCdbRd = sql_if(slFktRd,&slCursorRd,pclSqlBufRd,pclDataBufRd);
                    }
                 }
                 if (igUseArrayInsert == FALSE)
                    close_my_cursor(&slCursorRd);
                 if (ilRCdbRd == DB_SUCCESS && strcmp(pclFtyp,"X") != 0 &&
                     strcmp(pclFtyp,"N") != 0 && strcmp(pclFtyp,"D") != 0)
                 {
                    if (igUseArrayInsert == TRUE)
                    {
                       strcpy(pclRogUrno,&rgRogDat[ilRogIdx].pclUrno[0]);
                       strcpy(pclRogAurn,&rgRogDat[ilRogIdx].pclAurn[0]);
                       strcpy(pclRogDurn,&rgRogDat[ilRogIdx].pclDurn[0]);
                       strcpy(pclRogRkey,&rgRogDat[ilRogIdx].pclRkey[0]);
                       strcpy(pclRogLand,&rgRogDat[ilRogIdx].pclLand[0]);
                       strcpy(pclRogAirb,&rgRogDat[ilRogIdx].pclAirb[0]);
                       strcpy(pclRogFtyp,&rgRogDat[ilRogIdx].pclFtyp[0]);
                       strcpy(pclRogStat,&rgRogDat[ilRogIdx].pclStat[0]);
                    }
                    else
                    {
                       BuildItemBuffer(pclDataBufRd,"",8,",");
                       ilLen = get_real_item(pclRogUrno,pclDataBufRd,1);
                       ilLen = get_real_item(pclRogAurn,pclDataBufRd,2);
                       ilLen = get_real_item(pclRogDurn,pclDataBufRd,3);
                       ilLen = get_real_item(pclRogRkey,pclDataBufRd,4);
                       ilLen = get_real_item(pclRogLand,pclDataBufRd,5);
                       ilLen = get_real_item(pclRogAirb,pclDataBufRd,6);
                       ilLen = get_real_item(pclRogFtyp,pclDataBufRd,7);
                       ilLen = get_real_item(pclRogStat,pclDataBufRd,8);
                    }
                    if (strcmp(pclRogFtyp,"S") == 0)
                    {
                       if (strlen(pclAirb) > 0)
                       {
                          strcpy(pclNewRogFtyp,"E");
                          strcpy(pclTime,pclAirb);
                       }
                       else
                       {
                          strcpy(pclNewRogFtyp,"S");
                          strcpy(pclTime,pclTifd);
                       }
                    }
                    else
                    {
                       if (strlen(pclAirb) > 0)
                       {
                          strcpy(pclNewRogFtyp,"H");
                          strcpy(pclTime,pclAirb);
                       }
                       else
                       {
                          strcpy(pclNewRogFtyp,"O");
                          strcpy(pclTime,pclTifd);
                       }
                    }
                    if (strcmp(pclTime,pclRogLand) >= 0)
                    {
                       strcpy(pclNewRogStat,"B");
dbg(TRACE,"AKL UPD 3");
                       ilRC = UpdROGTAB("D",pclRogUrno,pclUrno,pclTime,pclNewRogFtyp,
                                        pclNewRogStat,pclFtyp);
                    }
                    else
                    {
                       if (igAutoJoin == TRUE)
                       {
                          if (strlen(pclAirb) == 0)
                          {
                             strcpy(pclTime,pclTifd);
dbg(TRACE,"AKL INS 5");
                             ilRC = InsROGTAB(pclRegn,"0",pclUrno,pclRkey," ",
                                              pclTifd,"S","D"," ",pclFtyp);
                          }
                          else
                          {
                             strcpy(pclTime,pclAirb);
dbg(TRACE,"AKL INS 6");
                             ilRC = InsROGTAB(pclRegn,"0",pclUrno,pclRkey," ",
                                           pclAirb,"H","D"," ",pclFtyp);
                          }
                          igTotCountIns++;
                          igRegnCountIns++;
                       }
                       else
                       {
                          strcpy(pclNewRogStat,"E");
dbg(TRACE,"AKL UPD 4");
                          ilRC = UpdROGTAB("D",pclRogUrno,pclUrno,pclTime,pclNewRogFtyp,
                                           pclNewRogStat,pclFtyp);
                       }
                    }
                 }
                 else
                 {
                    if (strlen(pclAirb) == 0)
                    {
                       strcpy(pclTime,pclTifd);
dbg(TRACE,"AKL INS 7");
                       ilRC = InsROGTAB(pclRegn,"0",pclUrno,pclRkey," ",
                                        pclTifd,"S","D"," ",pclFtyp);
                    }
                    else
                    {
                       strcpy(pclTime,pclAirb);
dbg(TRACE,"AKL INS 8");
                       ilRC = InsROGTAB(pclRegn,"0",pclUrno,pclRkey," ",
                                        pclAirb,"H","D"," ",pclFtyp);
                    }
                    igTotCountIns++;
                    igRegnCountIns++;
                 }
              }
              else
              {
                 if (strcmp(pclAdid,"B") == 0)
                 { /* Return flight record , etc */
                    if (strlen(pclAirb) > 0)
                    {
                       if (strlen(pclLand) > 0)
                       {
                          strcpy(pclNewRogFtyp,"H");
                       }
                       else
                       {
                          strcpy(pclNewRogFtyp,"O");
                          strcpy(pclLand,pclTifa);
                       }
                    }
                    else
                    {
                       if (strlen(pclLand) > 0)
                       {
                          strcpy(pclNewRogFtyp,"E");
                       }
                       else
                       {
                          strcpy(pclNewRogFtyp,"S");
                          strcpy(pclLand,pclTifa);
                       }
                       strcpy(pclAirb,pclTifd);
                    }
                    if (strcmp(pclLand,pclAirb) >= 0)
                    {
                       strcpy(pclNewRogStat,"R");
                    }
                    else
                    {
                       strcpy(pclNewRogStat,"E");
                    }
                    strcpy(pclTime,pclAirb);
dbg(TRACE,"AKL INS 9");
                    ilRC = InsROGTAB(pclRegn,pclUrno,pclUrno,pclRkey,pclLand,
                                     pclAirb,pclNewRogFtyp,pclNewRogStat,pclFtyp,pclFtyp);
                    igTotCountIns++;
                    igRegnCountIns++;
                 }
                 else
                 {
                    dbg(TRACE,"Record <%s><%s><%s><%s><%s><%s><%s><%s> was not inserted / updated",
                        pclUrno,pclRegn,pclLand,pclAirb,pclFtyp,pclRkey,pclRtyp,pclAdid);
                 }
              }
           }

        /*}

        else
        {
           dbg(TRACE,"Record with URNO = <%s> discarded",pclUrno);
        }
        */
     }
     if (ilMissingRecordFound == TRUE)
     {
        slFkt = START;
     }
     else
     {
        slFkt = NEXT;
     }
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);

  if (pclStartTime == NULL)
  {
     ilRC = SortCheckRegn(pcpRegn,"",pclSeqn);
  }
  else
  {
     ilRC = SortCheckRegn(pcpRegn,pclStartTime,pclSeqn);
  }

  if (igUseArrayInsert == TRUE && igCurRogDat >= 0)
  {
     ilRC = SqlArrayFill();
     ilRC = SqlArrayInsert();
  }

  if (igAutoJoin == TRUE)
  {
     ilRC = CheckROGvsAFT(pcpRegn,pclSeqn);
  }

  if (igSendRACBroadcast == TRUE)
  {
     strcpy(pclSelList,pcpRegn);
     strcat(pclSelList,",");
     strcat(pclSelList,pcgRACBegin);
     strcat(pclSelList,",");
     strcat(pclSelList,pcgRACEnd);
     strcpy(pclFieldList,"RKEY");
     pcgRACDataList[strlen(pcgRACDataList)-1] = '\0';
     if (strlen(pcgRACDataList) > 0)
     {
        ilRC = CompressDataList(pcgRACDataList);
        ilRC = TriggerBchdlAction(pcgRACCommand,"AFTTAB",pclSelList,pclFieldList,
                                  pcgRACDataList,TRUE,FALSE);
        if (igCedaEventInterface > 0)
        {
           (void) tools_send_info_flag(igCedaEventInterface,igRemoteTarget,"roghdl",
                                       "","CEDA","","",pcgTwStart,pcgTwEnd,
                                       pcgRACCommand,"AFTTAB",pclSelList,pclFieldList,
                                       pcgRACDataList,0);
           dbg(DEBUG,"Send to CEDA Event Interface: <%s><%s><%s><%s><%s>",
               pcgRACCommand,"AFTTAB",pclSelList,pclFieldList,pcgRACDataList);
        }
        else
        {
           if (igRemoteTarget > 0)
           {
              dbg(TRACE,"SENDING RAC TO <%s>",pcgRACTargets);
              ilMaxItm = field_count(pcgRACTargets);
              for(ilCurItm=1;ilCurItm<=ilMaxItm;ilCurItm++)
              {
                ilLen = get_real_item(pclRACDest,pcgRACTargets,ilCurItm);
                if (ilLen > 0)
                {
                  ilRacQue = atoi(pclRACDest);
                  if (ilRacQue > 0)
                  {

                    (void) tools_send_info_flag(ilRacQue,0,"roghdl",
                                              "","CEDA","","",pcgTwStart,pcgTwEnd,
                                              pcgRACCommand,"AFTTAB",pclSelList,pclFieldList,
                                              pcgRACDataList,0);
                    dbg(TRACE,"Sent to (%d): <%s><%s><%s><%s><%s>",
                        ilRacQue,pcgRACCommand,"AFTTAB",
                        pclSelList,pclFieldList,pcgRACDataList);
                  }
                }
              }
           }
        }
     }
  }

  return RC_SUCCESS;
} /* End of BuildROGTAB */



static int InsROGTAB(char *pcpRegn, char *pcpAurn, char *pcpDurn, char *pcpRkey,
                     char *pcpLand, char *pcpAirb, char *pcpFtyp, char *pcpStat,
                     char *pcpFtpa, char *pcpFtpd)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBufIns[L_BUFF];
  char pclSqlBufFields[L_BUFF];
  char pclSqlBufValues[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  char pclNewUrno[32];
  int ilDebugLevel;
  char pclDataList[L_BUFF];
  char pclSqlBuf[L_BUFF];
  int ilRogIdx;

dbg(TRACE,"AKL INS <%s> <%s> <%s> <%s> <%s> <%s> <%s> <%s> <%s> <%s>",
pcpRegn,pcpAurn,pcpDurn,pcpRkey,pcpLand,pcpAirb,pcpFtyp,pcpStat,pcpFtpa,pcpFtpd);
  if (strcmp(pcpAurn,"0") == 0)
  {
     if (igUseArrayInsert == TRUE)
     {
        ilRCdb = DB_ERROR;
        ilRogIdx = -1;
        ilRC = SearchByKey("DURN",pcpDurn,S_FOR,&ilRogIdx);
        if (ilRogIdx >= 0)
           ilRCdb = DB_SUCCESS;
     }
     else
     {
        sprintf(pclSqlBuf,"SELECT URNO FROM ROGTAB WHERE REGN = '%s' AND DURN = %s",
                pcpRegn,pcpDurn);
        slCursor = 0;
        slFkt = START;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        close_my_cursor(&slCursor);
     }
     if (ilRCdb == DB_SUCCESS)
     {
        dbg(TRACE,"InsROGTAB: Dep Record exists already");
        return ilRC;
     }
  }
  else
  {
     if (strcmp(pcpDurn,"0") == 0)
     {
        if (igUseArrayInsert == TRUE)
        {
           ilRCdb = DB_ERROR;
           ilRogIdx = -1;
           ilRC = SearchByKey("AURN",pcpAurn,S_FOR,&ilRogIdx);
           if (ilRogIdx >= 0)
              ilRCdb = DB_SUCCESS;
        }
        else
        {
           sprintf(pclSqlBuf,"SELECT URNO FROM ROGTAB WHERE REGN = '%s' AND AURN = %s",
                   pcpRegn,pcpAurn);
           slCursor = 0;
           slFkt = START;
           ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
           close_my_cursor(&slCursor);
        }
        if (ilRCdb == DB_SUCCESS)
        {
           dbg(TRACE,"InsROGTAB: Arr Record exists already");
           return ilRC;
        }
     }
  }

  if (igUseArrayInsert == TRUE)
  {
     if (igLastUrnoIdx == 1000 || igLastUrnoIdx < 0)
     {
        ilDebugLevel = debug_level;
        debug_level = 0;

         /*
        GetNextValues(pclNewUrno,1010);
        UFIS-1485
        */
        DB_LoadUrnoList(1010,pcgNewUrnos,pcgTableKey);
         dbg(TRACE,"InsROGTAB: URNO  pcgNewUrnos <%s>",pcgNewUrnos);
        debug_level = ilDebugLevel;
        igLastUrnoIdx = 0;
        igLastUrno = atoi(pcgNewUrnos);
        dbg(TRACE,"InsROGTAB: URNO igLastUrno <%d>,<%s>",igLastUrno,pcgNewUrnos);
     }
     else
     {
        igLastUrnoIdx++;
        igLastUrno++;
        dbg(TRACE,"InsROGTAB: 2  URNO <%d>",igLastUrno);
     }
     if (igCurRogDat < 0)
        igCurRogDat = 0;
     sprintf(&rgRogDat[igCurRogDat].pclUrno[0],"%d",igLastUrno);
     strcpy(&rgRogDat[igCurRogDat].pclHopo[0],pcgHomeAp);
     strcpy(&rgRogDat[igCurRogDat].pclRegn[0],pcpRegn);
     if (*pcpAurn != '\0')
        strcpy(&rgRogDat[igCurRogDat].pclAurn[0],pcpAurn);
     else
        strcpy(&rgRogDat[igCurRogDat].pclAurn[0]," ");
     if (*pcpDurn != '\0')
        strcpy(&rgRogDat[igCurRogDat].pclDurn[0],pcpDurn);
     else
        strcpy(&rgRogDat[igCurRogDat].pclDurn[0]," ");
     if (*pcpRkey != '\0')
        strcpy(&rgRogDat[igCurRogDat].pclRkey[0],pcpRkey);
     else
        strcpy(&rgRogDat[igCurRogDat].pclRkey[0]," ");
     if (*pcpLand != '\0')
     {
        strcpy(&rgRogDat[igCurRogDat].pclLand[0],pcpLand);
        if (strlen(pcpLand) == 12)
           strcat(&rgRogDat[igCurRogDat].pclLand[0],"00");
     }
     else
        strcpy(&rgRogDat[igCurRogDat].pclLand[0]," ");
     if (*pcpAirb != '\0')
     {
        strcpy(&rgRogDat[igCurRogDat].pclAirb[0],pcpAirb);
        if (strlen(pcpAirb) == 12)
           strcat(&rgRogDat[igCurRogDat].pclAirb[0],"00");
     }
     else
        strcpy(&rgRogDat[igCurRogDat].pclAirb[0]," ");
     if (*pcpFtyp != '\0')
        strcpy(&rgRogDat[igCurRogDat].pclFtyp[0],pcpFtyp);
     else
        strcpy(&rgRogDat[igCurRogDat].pclFtyp[0]," ");
     if (*pcpStat != '\0')
        strcpy(&rgRogDat[igCurRogDat].pclStat[0],pcpStat);
     else
        strcpy(&rgRogDat[igCurRogDat].pclStat[0]," ");
     if (*pcpFtpa != '\0')
        strcpy(&rgRogDat[igCurRogDat].pclFtpa[0],pcpFtpa);
     else
        strcpy(&rgRogDat[igCurRogDat].pclFtpa[0]," ");
     if (*pcpFtpd != '\0')
        strcpy(&rgRogDat[igCurRogDat].pclFtpd[0],pcpFtpd);
     else
        strcpy(&rgRogDat[igCurRogDat].pclFtpd[0]," ");
     strcpy(&rgRogDat[igCurRogDat].pclSeqn[0]," ");
     strcpy(&rgRogDat[igCurRogDat].pclSeqs[0]," ");
     strcpy(&rgRogDat[igCurRogDat].pclFlag[0]," ");
     igCurRogDat++;
  }
  else
  {
     ilDebugLevel = debug_level;
     debug_level = 0;
     /*
        GetNextValues(pclNewUrno,1);
        UFIS-1485
     */
     DB_LoadUrnoList(1,pclNewUrno,pcgTableKey);

     debug_level = ilDebugLevel;
     strcpy(pclSqlBufFields,"URNO,HOPO,REGN,AURN,DURN,RKEY,LAND,AIRB,FTYP,STAT,FTPA,FTPD");
     sprintf(pclSqlBufValues,"%s,'%s','%s',%s,%s,%s,'%s','%s','%s','%s','%s','%s'",
             pclNewUrno,pcgHomeAp,pcpRegn,pcpAurn,pcpDurn,pcpRkey,pcpLand,pcpAirb,
             pcpFtyp,pcpStat,pcpFtpa,pcpFtpd);
     sprintf(pclSqlBufIns,"INSERT INTO ROGTAB FIELDS(%s) VALUES(%s)",
             pclSqlBufFields,pclSqlBufValues);
     sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
             pclNewUrno,pcgHomeAp,pcpRegn,pcpAurn,pcpDurn,pcpRkey,pcpLand,pcpAirb,
             pcpFtyp,pcpStat,pcpFtpa,pcpFtpd);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"InsROGTAB: <%s>",pclSqlBufIns);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBufIns,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
        ilRC = TriggerBchdlAction("IRT","ROGTAB",pclNewUrno,pclSqlBufFields,
                                  pclDataList,FALSE,FALSE);
     }
     close_my_cursor(&slCursor);
  }

  return ilRC;
} /* End of InsROGTAB */



static int UpdROGTAB(char *pcpAdid, char *pcpUrno, char *pcpRurn, char *pcpTime,
                     char *pcpFtyp, char *pcpStat, char *pcpFtpad)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  char pclUpdBuf[L_BUFF];
  char pclFieldList[L_BUFF];
  char pclDataList[L_BUFF];
  int ilRogIdx;
  char pclTime[16];

dbg(TRACE,"AKL UPD <%s> <%s> <%s> <%s> <%s> <%s> <%s>",
pcpAdid,pcpUrno,pcpRurn,pcpTime,pcpFtyp,pcpStat,pcpFtpad);
  strcpy(pclTime,pcpTime);
  if (strlen(pclTime) == 12)
     strcat(pclTime,"00");
  if (igUseArrayInsert == TRUE)
  {
     ilRogIdx = -1;
     ilRC = SearchByKey("URNO",pcpUrno,S_BACK,&ilRogIdx);
     if (ilRogIdx >= 0)
     {
        if (strcmp(pcpAdid,"A") == 0)
        {
           if (*pcpRurn != '\0')
              strcpy(&rgRogDat[ilRogIdx].pclAurn[0],pcpRurn);
           else
              strcpy(&rgRogDat[ilRogIdx].pclAurn[0]," ");
           if (*pcpTime != '\0')
              strcpy(&rgRogDat[ilRogIdx].pclLand[0],pclTime);
           else
              strcpy(&rgRogDat[ilRogIdx].pclLand[0]," ");
           if (*pcpFtyp != '\0')
              strcpy(&rgRogDat[ilRogIdx].pclFtyp[0],pcpFtyp);
           else
              strcpy(&rgRogDat[ilRogIdx].pclFtyp[0]," ");
           if (*pcpStat != '\0')
              strcpy(&rgRogDat[ilRogIdx].pclStat[0],pcpStat);
           else
              strcpy(&rgRogDat[ilRogIdx].pclStat[0]," ");
           if (*pcpFtpad != '\0')
              strcpy(&rgRogDat[ilRogIdx].pclFtpa[0],pcpFtpad);
           else
              strcpy(&rgRogDat[ilRogIdx].pclFtpa[0]," ");
        }
        else
        {
           if (*pcpRurn != '\0')
              strcpy(&rgRogDat[ilRogIdx].pclDurn[0],pcpRurn);
           else
              strcpy(&rgRogDat[ilRogIdx].pclDurn[0]," ");
           if (*pcpTime != '\0')
              strcpy(&rgRogDat[ilRogIdx].pclAirb[0],pclTime);
           else
              strcpy(&rgRogDat[ilRogIdx].pclAirb[0]," ");
           if (*pcpFtyp != '\0')
              strcpy(&rgRogDat[ilRogIdx].pclFtyp[0],pcpFtyp);
           else
              strcpy(&rgRogDat[ilRogIdx].pclFtyp[0]," ");
           if (*pcpStat != '\0')
              strcpy(&rgRogDat[ilRogIdx].pclStat[0],pcpStat);
           else
              strcpy(&rgRogDat[ilRogIdx].pclStat[0]," ");
           if (*pcpFtpad != '\0')
              strcpy(&rgRogDat[ilRogIdx].pclFtpd[0],pcpFtpad);
           else
              strcpy(&rgRogDat[ilRogIdx].pclFtpd[0]," ");
        }
     }
  }
  else
  {
     if (strcmp(pcpAdid,"A") == 0)
     {
        sprintf(pclUpdBuf,"AURN=%s,LAND='%s',FTYP='%s',STAT='%s',FTPA='%s'",
                pcpRurn,pcpTime,pcpFtyp,pcpStat,pcpFtpad);
        strcpy(pclFieldList,"AURN,LAND,FTYP,STAT,FTPA,URNO");
        sprintf(pclDataList,"%s,%s,%s,%s,%s,%s",
                pcpRurn,pcpTime,pcpFtyp,pcpStat,pcpFtpad,pcpUrno);
     }
     else
     {
        sprintf(pclUpdBuf,"DURN=%s,AIRB='%s',FTYP='%s',STAT='%s',FTPD='%s'",
                pcpRurn,pcpTime,pcpFtyp,pcpStat,pcpFtpad);
        strcpy(pclFieldList,"DURN,AIRB,FTYP,STAT,FTPD,URNO");
        sprintf(pclDataList,"%s,%s,%s,%s,%s,%s",
                pcpRurn,pcpTime,pcpFtyp,pcpStat,pcpFtpad,pcpUrno);
     }
     sprintf(pclSqlBuf,"UPDATE ROGTAB SET %s ",pclUpdBuf);
     sprintf(pclSelectBuf,"WHERE URNO = %s",pcpUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     dbg(DEBUG,"UpdROGTAB: <%s>",pclSqlBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
        ilRC = TriggerBchdlAction("URT","ROGTAB",pcpUrno,pclFieldList,
                                  pclDataList,FALSE,FALSE);
     }
     close_my_cursor(&slCursor);
  }

  return ilRC;
} /* End of UpdROGTAB */



static int SortCheckRegn(char *pcpRegn, char *pcpTime, char *pcpSeqn)
{
  int ilRC = RC_SUCCESS;
  int ilRCdbLand = DB_SUCCESS;
  short slFktLand;
  short slCursorLand;
  char pclSqlBufLand[L_BUFF];
  char pclSelectBufLand[L_BUFF];
  char pclDataBufLand[L_BUFF];
  int ilRCdbAirb = DB_SUCCESS;
  short slFktAirb;
  short slCursorAirb;
  char pclSqlBufAirb[L_BUFF];
  char pclSelectBufAirb[L_BUFF];
  char pclDataBufAirb[L_BUFF];
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  int ilLen;
  char pclUrnoLand[32];
  char pclLandLand[32];
  char pclAirbLand[32];
  char pclFtypLand[32];
  char pclStatLand[32];
  char pclFtpaLand[32];
  char pclSeqnLand[32];
  char pclUrnoAirb[32];
  char pclLandAirb[32];
  char pclAirbAirb[32];
  char pclFtypAirb[32];
  char pclStatAirb[32];
  char pclFtpdAirb[32];
  char pclSeqnAirb[32];
  char pclDurnAirb[32];
  int ilIdx;
  int ilReadLand;
  int ilReadAirb;
  char pclTimeLand[32];
  char pclTimeAirb[32];
  char pclUrno[32];
  char pclAurn[32];
  char pclRkey[32];
  char pclLand[32];
  char pclAirb[32];
  char pclFtyp[32];
  char pclStat[32];
  char pclFtpa[32];
  char pclFtpd[32];
  char pclSeqn[32];
  char pclSeqs[32];
  char pclLastLand[32];
  char pclLastAirb[32];
  int ilLand;
  int ilAirb;
  char pclStatus[32];
  char pclIdx[32];
  char pclUpdType[32];
  int ilJoin;
  int ilJoinDone;
  char pclLastArrUrno[2000][32];
  char pclLastArrTime[2000][32];
  int ilNoOpenArr;
  char pclDelListUrno[2000][32];
  int ilDelListNo;
  int ilI;
  char pclNewFtyp[8];
  int ilJ;
  int ilRogIdx;
  int ilRogIdxLand;
  int ilRogIdxAirb;
  int ilRogIdxNew;
  int ilRogIdxLandNew;
  int ilRogIdxAirbNew;

  strcpy(pclIdx," ");
  ilIdx = 0;
  if (strlen(pcpTime) > 0)
  {
     if (igUseArrayInsert == TRUE)
     {
        strcpy(pclSqlBufLand,"SELECT MAX(SEQN) FROM ROGTAB ");
        sprintf(pclSelectBufLand,
                "WHERE REGN = '%s'",pcpRegn);
     }
     else
     {
        strcpy(pclSqlBufLand,"SELECT AIRB,SEQN FROM ROGTAB ");
        sprintf(pclSelectBufLand,
                "WHERE REGN = '%s' AND LAND <> ' ' AND LAND < '%s' AND FTYP <> 'E' AND STAT = 'B' AND SEQS = 'O' ORDER BY LAND DESC",
                pcpRegn,pcpTime);
     }
     strcat(pclSqlBufLand,pclSelectBufLand);
     slCursorLand = 0;
     slFktLand = START;
     /* dbg(TRACE,"<%s>",pclSqlBufLand); */
     ilRCdbLand = sql_if(slFktLand,&slCursorLand,pclSqlBufLand,pclDataBufLand);
     if (ilRCdbLand == DB_SUCCESS)
     {
        if (igUseArrayInsert == TRUE)
        {
           strcpy(pclIdx,pclDataBufLand);
           ilIdx = atoi(pclIdx);
        }
        else
        {
           BuildItemBuffer(pclDataBufLand,"",2,",");
           ilLen = get_real_item(pclAirb,pclDataBufLand,1);
           ilLen = get_real_item(pclIdx,pclDataBufLand,2);
           ilIdx = atoi(pclIdx) - 1;
           if (strlen(pclAirb) == 0 || strcmp(pclAirb,pcpTime) >= 0)
           {
              slFktLand = NEXT;
              ilRCdbLand = sql_if(slFktLand,&slCursorLand,pclSqlBufLand,pclDataBufLand);
              if (ilRCdbLand == DB_SUCCESS)
              {
                 BuildItemBuffer(pclDataBufLand,"",2,",");
                 ilLen = get_real_item(pclAirb,pclDataBufLand,1);
                 ilLen = get_real_item(pclIdx,pclDataBufLand,2);
                 ilIdx = atoi(pclIdx) - 1;
              }
              else
              {
                 strcpy(pclIdx," ");
                 ilIdx = 0;
              }
           }
        }
     }
     close_my_cursor(&slCursorLand);
  }

dbg(TRACE,"Sequence Start = <%s><%d>",pclIdx,ilIdx);
  if (igUseArrayInsert == TRUE)
  {
     ilRCdbLand = DB_ERROR;
     ilRogIdxLand = -1;
     ilRC = SearchByLANDSeqn(pclIdx,&ilRogIdxLand);
     if (ilRogIdxLand >= 0)
        ilRCdbLand = DB_SUCCESS;
     ilRCdbAirb = DB_ERROR;
     ilRogIdxAirb = -1;
     ilRC = SearchByAIRBSeqn(pclIdx,&ilRogIdxAirb);
     if (ilRogIdxAirb >= 0)
        ilRCdbAirb = DB_SUCCESS;
  }
  else
  {
     strcpy(pclSqlBufLand,"SELECT URNO,LAND,AIRB,FTYP,STAT,FTPA,SEQN FROM ROGTAB ");
     sprintf(pclSelectBufLand,
             "WHERE REGN = '%s' AND STAT IN ('A','B','E') AND (SEQN = ' ' OR SEQN >= '%s') ORDER BY LAND",
             pcpRegn,pclIdx);
     strcat(pclSqlBufLand,pclSelectBufLand);
     slCursorLand = 0;
     slFktLand = START;
     /* dbg(TRACE,"<%s>",pclSqlBufLand); */

     strcpy(pclSqlBufAirb,"SELECT URNO,LAND,AIRB,FTYP,STAT,FTPD,SEQN,DURN FROM ROGTAB ");
     sprintf(pclSelectBufAirb,
             "WHERE REGN = '%s' AND STAT IN ('D','R') AND (SEQN = ' ' OR SEQN >= '%s') ORDER BY AIRB",
             pcpRegn,pclIdx);
     strcat(pclSqlBufAirb,pclSelectBufAirb);
     slCursorAirb = 0;
     slFktAirb = START;
     /* dbg(TRACE,"<%s>",pclSqlBufAirb); */

     ilRCdbLand = sql_if(slFktLand,&slCursorLand,pclSqlBufLand,pclDataBufLand);
     ilRCdbAirb = sql_if(slFktAirb,&slCursorAirb,pclSqlBufAirb,pclDataBufAirb);
  }
  ilNoOpenArr = 0;
  ilDelListNo = 0;
  ilReadLand = TRUE;
  ilReadAirb = TRUE;
  while (ilRCdbLand == DB_SUCCESS || ilRCdbAirb == DB_SUCCESS)
  {
     if (ilRCdbLand == DB_SUCCESS && ilReadLand == TRUE)
     {
        if (igUseArrayInsert == TRUE)
        {
           strcpy(pclUrnoLand,&rgRogDat[ilRogIdxLand].pclUrno[0]);
           strcpy(pclLandLand,&rgRogDat[ilRogIdxLand].pclLand[0]);
           strcpy(pclAirbLand,&rgRogDat[ilRogIdxLand].pclAirb[0]);
           strcpy(pclFtypLand,&rgRogDat[ilRogIdxLand].pclFtyp[0]);
           strcpy(pclStatLand,&rgRogDat[ilRogIdxLand].pclStat[0]);
           strcpy(pclFtpaLand,&rgRogDat[ilRogIdxLand].pclFtpa[0]);
           strcpy(pclSeqnLand,&rgRogDat[ilRogIdxLand].pclSeqn[0]);
        }
        else
        {
           BuildItemBuffer(pclDataBufLand,"",7,",");
           ilLen = get_real_item(pclUrnoLand,pclDataBufLand,1);
           ilLen = get_real_item(pclLandLand,pclDataBufLand,2);
           ilLen = get_real_item(pclAirbLand,pclDataBufLand,3);
           ilLen = get_real_item(pclFtypLand,pclDataBufLand,4);
           ilLen = get_real_item(pclStatLand,pclDataBufLand,5);
           ilLen = get_real_item(pclFtpaLand,pclDataBufLand,6);
           ilLen = get_real_item(pclSeqnLand,pclDataBufLand,7);
        }
     }
     if (ilRCdbAirb == DB_SUCCESS && ilReadAirb == TRUE)
     {
        if (igUseArrayInsert == TRUE)
        {
           strcpy(pclUrnoAirb,&rgRogDat[ilRogIdxAirb].pclUrno[0]);
           strcpy(pclLandAirb,&rgRogDat[ilRogIdxAirb].pclLand[0]);
           strcpy(pclAirbAirb,&rgRogDat[ilRogIdxAirb].pclAirb[0]);
           strcpy(pclFtypAirb,&rgRogDat[ilRogIdxAirb].pclFtyp[0]);
           strcpy(pclStatAirb,&rgRogDat[ilRogIdxAirb].pclStat[0]);
           strcpy(pclFtpdAirb,&rgRogDat[ilRogIdxAirb].pclFtpd[0]);
           strcpy(pclSeqnAirb,&rgRogDat[ilRogIdxAirb].pclSeqn[0]);
           strcpy(pclDurnAirb,&rgRogDat[ilRogIdxAirb].pclDurn[0]);
        }
        else
        {
           BuildItemBuffer(pclDataBufAirb,"",8,",");
           ilLen = get_real_item(pclUrnoAirb,pclDataBufAirb,1);
           ilLen = get_real_item(pclLandAirb,pclDataBufAirb,2);
           ilLen = get_real_item(pclAirbAirb,pclDataBufAirb,3);
           ilLen = get_real_item(pclFtypAirb,pclDataBufAirb,4);
           ilLen = get_real_item(pclStatAirb,pclDataBufAirb,5);
           ilLen = get_real_item(pclFtpdAirb,pclDataBufAirb,6);
           ilLen = get_real_item(pclSeqnAirb,pclDataBufAirb,7);
           ilLen = get_real_item(pclDurnAirb,pclDataBufAirb,8);
        }
     }
     if (ilRCdbLand == DB_SUCCESS && ilRCdbAirb != DB_SUCCESS)
     {
        strcpy(pclUpdType,"A");
        ilReadLand = TRUE;
        ilReadAirb = FALSE;
     }
     if (ilRCdbAirb == DB_SUCCESS && ilRCdbLand != DB_SUCCESS)
     {
        strcpy(pclUpdType,"D");
        ilReadAirb = TRUE;
        ilReadLand = FALSE;
     }
     if (ilRCdbLand == DB_SUCCESS && ilRCdbAirb == DB_SUCCESS)
     {
        if (strcmp(pclStatLand,"B") == 0)
        {
           strcpy(pclTimeLand,pclAirbLand);
        }
        else
        {
           strcpy(pclTimeLand,pclLandLand);
        }
        if (strcmp(pclStatAirb,"R") == 0)
        {
           strcpy(pclTimeAirb,pclLandAirb);
        }
        else
        {
           strcpy(pclTimeAirb,pclAirbAirb);
        }
        if (strcmp(pclTimeLand,pclTimeAirb) <= 0)
        {
           strcpy(pclUpdType,"A");
           ilReadLand = TRUE;
           ilReadAirb = FALSE;
        }
        else
        {
           strcpy(pclUpdType,"D");
           ilReadAirb = TRUE;
           ilReadLand = FALSE;
        }
     }
     ilJoin = FALSE;
     if (ilNoOpenArr > 0 && strcmp(pclUpdType,"D") == 0)
     {
        if (strcmp(pclStatAirb,"D") == 0 &&
           strcmp(pclFtypAirb,"E") != 0 &&
           strcmp(pclFtpdAirb,"X") != 0 &&
           strcmp(pclFtpdAirb,"N") != 0 &&
           strcmp(pclFtpdAirb,"D") != 0)
        {
           ilJoin = TRUE;
        }
     }
     if (ilJoin == TRUE && igAutoJoin == TRUE)
     {
        ilJoinDone = FALSE;
        for (ilI = ilNoOpenArr-1; ilI >= 0 && ilJoinDone == FALSE; ilI--)
        {
           if (strcmp(&pclLastArrTime[ilI][0],pclAirbAirb) < 0)
           {   /* do join */
              if (igUseArrayInsert == TRUE)
              {
                 ilRCdb = DB_ERROR;
                 ilRogIdx = -1;
                 ilRC = SearchByKey("URNO",&pclLastArrUrno[ilI][0],S_BACK,&ilRogIdx);
                 if (ilRogIdx >= 0)
                    ilRCdb = DB_SUCCESS;
              }
              else
              {
                 strcpy(pclSqlBuf,"SELECT AURN,RKEY,LAND,FTYP,STAT FROM ROGTAB ");
                 sprintf(pclSelectBuf,"WHERE URNO = %s",&pclLastArrUrno[ilI][0]);
                 strcat(pclSqlBuf,pclSelectBuf);
                 slCursor = 0;
                 slFkt = START;
                 ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
                 close_my_cursor(&slCursor);
              }
              if (ilRCdb == DB_SUCCESS)
              {
                 if (igUseArrayInsert == TRUE)
                 {
                    strcpy(pclAurn,&rgRogDat[ilRogIdx].pclAurn[0]);
                    strcpy(pclRkey,&rgRogDat[ilRogIdx].pclRkey[0]);
                    strcpy(pclLand,&rgRogDat[ilRogIdx].pclLand[0]);
                    strcpy(pclFtyp,&rgRogDat[ilRogIdx].pclFtyp[0]);
                    strcpy(pclStat,&rgRogDat[ilRogIdx].pclStat[0]);
                 }
                 else
                 {
                    BuildItemBuffer(pclDataBuf,"",5,",");
                    ilLen = get_real_item(pclAurn,pclDataBuf,1);
                    ilLen = get_real_item(pclRkey,pclDataBuf,2);
                    ilLen = get_real_item(pclLand,pclDataBuf,3);
                    ilLen = get_real_item(pclFtyp,pclDataBuf,4);
                    ilLen = get_real_item(pclStat,pclDataBuf,5);
                 }
                 strcpy(pclNewFtyp,"E");
                 if (strcmp(pclFtyp,"S") == 0)
                 {
                    if (strcmp(pclFtypAirb,"H") == 0)
                    {
                       strcpy(pclNewFtyp,"E");
                    }
                    else
                    {
                       strcpy(pclNewFtyp,"S");
                    }
                 }
                 else
                 {
                    if (strcmp(pclFtyp,"O") == 0)
                    {
                       if (strcmp(pclFtypAirb,"S") == 0)
                       {
                          strcpy(pclNewFtyp,"O");
                       }
                       else
                       {
                          strcpy(pclNewFtyp,"H");
                       }
                    }
                 }
                 if (strcmp(pclNewFtyp,"E") != 0)
                 {
dbg(TRACE,"AKL UPD 5");
                    ilRC = UpdROGTAB("D",&pclLastArrUrno[ilI][0],pclDurnAirb,
                                     pclAirbAirb,pclNewFtyp,"B",pclFtpdAirb);
                    for (ilJ = ilI; ilJ < ilNoOpenArr-1; ilJ++)
                    {
                       strcpy(&pclLastArrUrno[ilJ][0],&pclLastArrUrno[ilJ+1][0]);
                       strcpy(&pclLastArrTime[ilJ][0],&pclLastArrTime[ilJ+1][0]);
                    }
                    ilNoOpenArr--;
                    ilJoinDone = TRUE;
                    strcpy(&pclDelListUrno[ilDelListNo][0],pclUrnoAirb);
                    ilDelListNo++;
                 }
              }
           }
        }
        if (ilJoinDone == FALSE)
        {
           ilIdx++;
           ilRC = UpdSEQN(pclUrnoAirb,ilIdx,pclSeqnAirb);
        }
     }
     else
     {
        ilIdx++;
        if (strcmp(pclUpdType,"A") == 0)
        {
           ilRC = UpdSEQN(pclUrnoLand,ilIdx,pclSeqnLand);
        }
        else
        {
           ilRC = UpdSEQN(pclUrnoAirb,ilIdx,pclSeqnAirb);
        }
     }
     if (strcmp(pclUpdType,"A") == 0)
     {
        if (strcmp(pclStatLand,"B") == 0)
        {
           ilNoOpenArr = 0;
        }
        else
        {
           if (strcmp(pclStatLand,"A") == 0 &&
              strcmp(pclFtypLand,"E") != 0 &&
              strcmp(pclFtpaLand,"X") != 0 &&
              strcmp(pclFtpaLand,"N") != 0 &&
              strcmp(pclFtpaLand,"D") != 0)
           {
              strcpy(&pclLastArrUrno[ilNoOpenArr][0],pclUrnoLand);
              strcpy(&pclLastArrTime[ilNoOpenArr][0],pclLandLand);
              ilNoOpenArr++;
           }
        }
     }
     if (ilReadLand == TRUE)
     {
        if (igUseArrayInsert == TRUE)
        {
           ilRCdbLand = DB_ERROR;
           ilRogIdxLandNew = ilRogIdxLand;
           ilRC = SearchByLANDSeqn(pclIdx,&ilRogIdxLandNew);
           if (ilRogIdxLandNew != ilRogIdxLand)
           {
              ilRCdbLand = DB_SUCCESS;
              ilRogIdxLand = ilRogIdxLandNew;
           }
        }
        else
        {
           slFktLand = NEXT;
           ilRCdbLand = sql_if(slFktLand,&slCursorLand,pclSqlBufLand,pclDataBufLand);
        }
     }
     if (ilReadAirb == TRUE)
     {
        if (igUseArrayInsert == TRUE)
        {
           ilRCdbAirb = DB_ERROR;
           ilRogIdxAirbNew = ilRogIdxAirb;
           ilRC = SearchByAIRBSeqn(pclIdx,&ilRogIdxAirbNew);
           if (ilRogIdxAirbNew != ilRogIdxAirb)
           {
              ilRCdbAirb = DB_SUCCESS;
              ilRogIdxAirb = ilRogIdxAirbNew;
           }
        }
        else
        {
           slFktAirb = NEXT;
           ilRCdbAirb = sql_if(slFktAirb,&slCursorAirb,pclSqlBufAirb,pclDataBufAirb);
        }
     }
  }
  if (igUseArrayInsert == FALSE)
  {
     close_my_cursor(&slCursorLand);
     close_my_cursor(&slCursorAirb);
  }

  if (ilDelListNo > 0)
  {
     for (ilI = 0; ilI < ilDelListNo; ilI++)
     {
       ilRC = DelROG(&pclDelListUrno[ilI][0]);
     }
  }

  strcpy(pcpSeqn,pclIdx);
  strcpy(pclLastLand,"19000101000000");
  strcpy(pclLastAirb,"19000101000000");
  ilLand = TRUE;
  ilAirb = TRUE;
  strcpy(pclStatus,"");
  if (igUseArrayInsert == TRUE)
  {
     ilRCdb = DB_ERROR;
     ilRogIdx = -1;
     ilRC = SearchBySEQN(pclIdx,&ilRogIdx);
     if (ilRogIdx >= 0)
        ilRCdb = DB_SUCCESS;
  }
  else
  {
     strcpy(pclSqlBuf,"SELECT URNO,LAND,AIRB,FTYP,STAT,FTPA,FTPD,SEQN,SEQS FROM ROGTAB ");
     sprintf(pclSelectBuf,"WHERE REGN = '%s' AND SEQN >= '%s' ORDER BY SEQN",
             pcpRegn,pclIdx);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     /* dbg(TRACE,"<%s>",pclSqlBuf); */
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  while (ilRCdb == DB_SUCCESS)
  {
     if (igUseArrayInsert == TRUE)
     {
        strcpy(pclUrno,&rgRogDat[ilRogIdx].pclUrno[0]);
        strcpy(pclLand,&rgRogDat[ilRogIdx].pclLand[0]);
        strcpy(pclAirb,&rgRogDat[ilRogIdx].pclAirb[0]);
        strcpy(pclFtyp,&rgRogDat[ilRogIdx].pclFtyp[0]);
        strcpy(pclStat,&rgRogDat[ilRogIdx].pclStat[0]);
        strcpy(pclFtpa,&rgRogDat[ilRogIdx].pclFtpa[0]);
        strcpy(pclFtpd,&rgRogDat[ilRogIdx].pclFtpd[0]);
        strcpy(pclSeqn,&rgRogDat[ilRogIdx].pclSeqn[0]);
        strcpy(pclSeqs,&rgRogDat[ilRogIdx].pclSeqs[0]);
     }
     else
     {
        BuildItemBuffer(pclDataBuf,"",9,",");
        ilLen = get_real_item(pclUrno,pclDataBuf,1);
        ilLen = get_real_item(pclLand,pclDataBuf,2);
        ilLen = get_real_item(pclAirb,pclDataBuf,3);
        ilLen = get_real_item(pclFtyp,pclDataBuf,4);
        ilLen = get_real_item(pclStat,pclDataBuf,5);
        ilLen = get_real_item(pclFtpa,pclDataBuf,6);
        ilLen = get_real_item(pclFtpd,pclDataBuf,7);
        ilLen = get_real_item(pclSeqn,pclDataBuf,8);
        ilLen = get_real_item(pclSeqs,pclDataBuf,9);
     }
     if (strcmp(pclFtyp,"E") == 0 || strcmp(pclStat,"E") == 0)
     {
        ilRC = UpdSEQS(pclUrno,"E",pclSeqs);
     }
     else
     {
        if (strlen(pclAirb) == 0 || *pclAirb == ' ')
        {
           if (strcmp(pclFtpa,"X") == 0 || strcmp(pclFtpa,"N") == 0 ||
               strcmp(pclFtpa,"D") == 0)
           {
              ilRC = UpdSEQS(pclUrno,"O",pclSeqs);
           }
           else
           {
              if (strcmp(pclLand,pclLastAirb) >= 0 && ilAirb == TRUE)
              {
                 if (strcmp(pclFtyp,"O") == 0)
                 {
                    if (strlen(pclStatus) == 0 || strcmp(pclStatus,"AIR") == 0)
                    {
                       ilRC = UpdSEQS(pclUrno,"O",pclSeqs);
                    }
                    else
                    {
                       ilRC = UpdSEQS(pclUrno,"N",pclSeqs);
                    }
                 }
                 else
                 {
                    ilRC = UpdSEQS(pclUrno,"O",pclSeqs);
                 }
              }
              else
              {
                 ilRC = UpdSEQS(pclUrno,"N",pclSeqs);
              }
              ilLand = TRUE;
              ilAirb = FALSE;
              strcpy(pclLastLand,pclLand);
              if (strcmp(pclFtyp,"O") == 0)
              {
                 strcpy(pclStatus,"LAN");
              }
              else
              {
                 strcpy(pclStatus,"SCH");
              }
           }
        }
        else
        {
           if (strlen(pclLand) == 0 || *pclLand == ' ')
           {
              if (strcmp(pclFtpd,"X") == 0 || strcmp(pclFtpd,"N") == 0 ||
                  strcmp(pclFtpd,"D") == 0)
              {
                 ilRC = UpdSEQS(pclUrno,"O",pclSeqs);
              }
              else
              {
                 if (strcmp(pclAirb,pclLastLand) >= 0 && ilLand == TRUE)
                 {
                    if (strcmp(pclFtyp,"H") == 0)
                    {
                       if (strlen(pclStatus) == 0 || strcmp(pclStatus,"LAN") == 0)
                       {
                          ilRC = UpdSEQS(pclUrno,"O",pclSeqs);
                       }
                       else
                       {
                          ilRC = UpdSEQS(pclUrno,"N",pclSeqs);
                       }
                    }
                    else
                    {
                       ilRC = UpdSEQS(pclUrno,"O",pclSeqs);
                    }
                 }
                 else
                 {
                    ilRC = UpdSEQS(pclUrno,"N",pclSeqs);
                 }
                 ilLand = FALSE;
                 ilAirb = TRUE;
                 strcpy(pclLastAirb,pclAirb);
                 if (strcmp(pclFtyp,"H") == 0)
                 {
                    strcpy(pclStatus,"AIR");
                 }
                 else
                 {
                    strcpy(pclStatus,"SCH");
                 }
              }
           }
           else
           {
              if (strcmp(pclStat,"B") == 0)
              {
                 if (strcmp(pclLand,pclLastAirb) >= 0 && ilAirb == TRUE)
                 {
                    if (strcmp(pclFtyp,"H") == 0 || strcmp(pclFtyp,"O") == 0)
                    {
                       if (strlen(pclStatus) == 0 || strcmp(pclStatus,"AIR") == 0)
                       {
                          ilRC = UpdSEQS(pclUrno,"O",pclSeqs);
                       }
                       else
                       {
                          ilRC = UpdSEQS(pclUrno,"N",pclSeqs);
                       }
                    }
                    else
                    {
                       ilRC = UpdSEQS(pclUrno,"O",pclSeqs);
                    }
                 }
                 else
                 {
                    ilRC = UpdSEQS(pclUrno,"N",pclSeqs);
                 }
                 ilLand = FALSE;
                 ilAirb = TRUE;
                 strcpy(pclLastLand,pclLand);
                 strcpy(pclLastAirb,pclAirb);
                 if (strcmp(pclFtyp,"H") == 0)
                 {
                    strcpy(pclStatus,"AIR");
                 }
                 else
                 {
                    if (strcmp(pclFtyp,"O") == 0)
                    {
                       strcpy(pclStatus,"LAN");
                    }
                    else
                    {
                       strcpy(pclStatus,"SCH");
                    }
                 }
              }
              else
              {
                 if (strcmp(pclAirb,pclLastLand) >= 0)
                 {
                    ilRC = UpdSEQS(pclUrno,"O",pclSeqs);
                 }
                 else
                 {
                    ilRC = UpdSEQS(pclUrno,"N",pclSeqs);
                 }
              }
           }
        }
     }
     if (igUseArrayInsert == TRUE)
     {
        ilRCdb = DB_ERROR;
        ilRogIdxNew = ilRogIdx;
        ilRC = SearchBySEQN(pclIdx,&ilRogIdxNew);
        if (ilRogIdxNew != ilRogIdx)
        {
           ilRCdb = DB_SUCCESS;
           ilRogIdx = ilRogIdxNew;
        }
     }
     else
     {
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     }
  }
  if (igUseArrayInsert == FALSE)
     close_my_cursor(&slCursor);

  dbg(TRACE,"All records of REGN <%s> sorted",pcpRegn);

  return ilRC;
} /* End of SortCheckRegn */



static int UpdSEQN(char *pcpUrno, int ipSeqn, char *pcpSeqn)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  char pclUpdBuf[L_BUFF];
  char pclFieldList[L_BUFF];
  char pclDataList[L_BUFF];
  char pclSeqn[32];
  char pclTmpSeqn[32];
  int ilSeqn;
  int ilRogIdx;

  ilSeqn = atoi(pcpSeqn);
  if (ipSeqn == ilSeqn)
  {
     return ilRC;
  }

  sprintf(pclTmpSeqn,"%d",ipSeqn);
  ilRC = GetCorrectedFormatZero(pclSeqn,pclTmpSeqn,10);
  if (igUseArrayInsert == TRUE)
  {
     ilRogIdx = -1;
     ilRC = SearchByKey("URNO",pcpUrno,S_BACK,&ilRogIdx);
     if (ilRogIdx >= 0)
     {
        strcpy(&rgRogDat[ilRogIdx].pclSeqn[0],pclSeqn);
     }
  }
  else
  {
     sprintf(pclUpdBuf,"SEQN='%s'",pclSeqn);
     strcpy(pclFieldList,"SEQN,URNO");
     sprintf(pclDataList,"%s,%s",pclSeqn,pcpUrno);
     sprintf(pclSqlBuf,"UPDATE ROGTAB SET %s ",pclUpdBuf);
     sprintf(pclSelectBuf,"WHERE URNO = %s",pcpUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     dbg(DEBUG,"UpdSEQN: <%s>",pclSqlBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
        ilRC = TriggerBchdlAction("URT","ROGTAB",pcpUrno,pclFieldList,
                                  pclDataList,FALSE,FALSE);
     }
     close_my_cursor(&slCursor);
  }

  return ilRC;
} /* End of UpdSEQN */



static int GetCorrectedFormatZero(char *pcpResult, char *pcpString, int ipLen)
{
  int ilRC = RC_SUCCESS;
  int ilI;

  if (strlen(pcpString) >= ipLen)
  {
     strncpy(pcpResult,pcpString,ipLen);
     pcpResult[ipLen] = '\0';
  }
  else
  {
     pcpResult[0] = '\0';
     for (ilI = 0; ilI < ipLen - strlen(pcpString); ilI++)
     {
        strcat(pcpResult,"0");
     }
     strcat(pcpResult,pcpString);
  }

  return ilRC;
} /* End of GetCorrectedFormatZero */



static int UpdSEQS(char *pcpUrno, char *pcpSeqs, char *pcpPrevSeqs)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  char pclUpdBuf[L_BUFF];
  char pclFieldList[L_BUFF];
  char pclDataList[L_BUFF];
  int ilRogIdx;

  if (strcmp(pcpSeqs,pcpPrevSeqs) == 0)
  {
     return ilRC;
  }

  if (igUseArrayInsert == TRUE)
  {
     ilRogIdx = -1;
     ilRC = SearchByKey("URNO",pcpUrno,S_BACK,&ilRogIdx);
     if (ilRogIdx >= 0)
     {
        strcpy(&rgRogDat[ilRogIdx].pclSeqs[0],pcpSeqs);
     }
  }
  else
  {
     sprintf(pclUpdBuf,"SEQS='%s'",pcpSeqs);
     strcpy(pclFieldList,"SEQS,URNO");
     sprintf(pclDataList,"%s,%s",pcpSeqs,pcpUrno);
     sprintf(pclSqlBuf,"UPDATE ROGTAB SET %s ",pclUpdBuf);
     sprintf(pclSelectBuf,"WHERE URNO = %s",pcpUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     dbg(DEBUG,"UpdSEQS: <%s>",pclSqlBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
        ilRC = TriggerBchdlAction("URT","ROGTAB",pcpUrno,pclFieldList,
                                  pclDataList,FALSE,FALSE);
     }
     close_my_cursor(&slCursor);
  }

  return ilRC;
} /* End of UpdSEQS */



static int UpdateROGTAB(char *pcpUrno)
{
  int ilRC = RC_SUCCESS;
  int ilRCdbAft = DB_SUCCESS;
  int ilRCdbRog = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBufAft[XXL_BUFF];
  char pclDataBufRog[XXL_BUFF];
  int ilLen;
  char pclRogRegn[32] = "";
  char pclRogAurn[32] = "";
  char pclRogDurn[32] = "";
  char pclRogLand[32] = "";
  char pclRogAirb[32] = "";
  char pclRogRkey[32] = "";
  char pclAftRegn[32] = "";
  char pclAftTifa[32] = "";
  char pclAftTifd[32] = "";
  char pclAftAdid[32] = "";
  char pclAftFtyp[32] = "";
  char pclAftTime[32] = "";
  char pclRogTime[32] = "";
  char pclTime[32];
  char pclExclFtyp[512] = "";
  char pclTmpFtyp[32] = "";
  int ilCount;
  int ilI;

  dbg(TRACE,"BEGIN: UPDATE ROGTAB");
  strcpy(pclAftRegn,"");
  strcpy(pclSqlBuf,"SELECT REGN,TIFA,TIFD,ADID,FTYP FROM AFTTAB ");
  sprintf(pclSelectBuf,"WHERE URNO = %s",pcpUrno);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  dbg(TRACE,"<%s>",pclSqlBuf);
  ilRCdbAft = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBufAft);
  close_my_cursor(&slCursor);
  if (ilRCdbAft == DB_SUCCESS)
  {
     dbg(TRACE,"FOUND AFTREC <%s>",pclDataBufAft);
     BuildItemBuffer(pclDataBufAft,"",5,",");
     ilLen = get_real_item(pclAftRegn,pclDataBufAft,1);
     ilLen = get_real_item(pclAftTifa,pclDataBufAft,2);
     ilLen = get_real_item(pclAftTifd,pclDataBufAft,3);
     ilLen = get_real_item(pclAftAdid,pclDataBufAft,4);
     ilLen = get_real_item(pclAftFtyp,pclDataBufAft,5);
     strcpy(pclExclFtyp,pcgExclFtyp);
     ilCount = GetNoOfElements(pclExclFtyp,',');
     for (ilI = 1; ilI <= ilCount; ilI++)
     {
        ilLen = get_real_item(pclTmpFtyp,pclExclFtyp,ilI);
        if (ilLen > 0)
        {
           if (pclTmpFtyp[1] == pclAftFtyp[0])
           {
              dbg(TRACE,"UpdateROGTAB: Ftyp <%s> is excluded",pclAftFtyp);
              return ilRC;
           }
        }
     }
     if (strcmp(pclAftAdid,"D") == 0)
     {
        strcpy(pclAftTime,pclAftTifd);
     }
     else
     {
        strcpy(pclAftTime,pclAftTifa);
     }
  }

  strcpy(pclSqlBuf,"SELECT REGN,AURN,DURN,LAND,AIRB,RKEY FROM ROGTAB ");
  sprintf(pclSelectBuf,"WHERE AURN = %s OR DURN = %s",pcpUrno,pcpUrno);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  dbg(TRACE,"<%s>",pclSqlBuf);
  ilRCdbRog = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBufRog);
  while (ilRCdbRog == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBufRog,"",6,",");
     dbg(TRACE,"GOT ROGTAB REC <%s>",pclDataBufRog);
     ilLen = get_real_item(pclRogRegn,pclDataBufRog,1);
     ilLen = get_real_item(pclRogAurn,pclDataBufRog,2);
     ilLen = get_real_item(pclRogDurn,pclDataBufRog,3);
     ilLen = get_real_item(pclRogLand,pclDataBufRog,4);
     ilLen = get_real_item(pclRogAirb,pclDataBufRog,5);
     ilLen = get_real_item(pclRogRkey,pclDataBufRog,6);
     if (strlen(pclRogLand) > 0 && strlen(pclRogAirb) > 0)
     {
        if (strcmp(pclRogLand,pclRogAirb) < 0)
        {
           strcpy(pclRogTime,pclRogLand);
        }
        else
        {
           strcpy(pclRogTime,pclRogAirb);
        }
     }
     else
     {
        if (strlen(pclRogLand) > 0)
        {
           strcpy(pclRogTime,pclRogLand);
        }
        else
        {
           strcpy(pclRogTime,pclRogAirb);
        }
     }
     strcpy(pclTime,pclRogTime);
     if (strlen(pclAftRegn) > 0 && strcmp(pclRogRegn,pclAftRegn) == 0)
     {
        strcpy(pclAftRegn,"");
        if (strcmp(pclAftTime,pclTime) < 0)
        {
           strcpy(pclTime,pclAftTime);
        }
     }
/* Coding produced unnecessary broadcasts
     strcat(pcgRACDataList,pclRogRkey);
     strcat(pcgRACDataList,",");
     if (strlen(pcgRACBegin) == 0)
     {
        strcpy(pcgRACBegin,pclTime);
     }
     else
     {
        if (strcmp(pcgRACBegin,pclTime) > 0)
        {
           strcpy(pcgRACBegin,pclTime);
        }
     }
     if (strlen(pcgRACEnd) == 0)
     {
        strcpy(pcgRACEnd,pclTime);
     }
     else
     {
        if (strcmp(pcgRACEnd,pclTime) < 0)
        {
           strcpy(pcgRACEnd,pclTime);
        }
     }
*/
     ilRC = BuildROGTAB(pclRogRegn,pclTime);
     strcpy(pcgRACDataList,"");
     slFkt = NEXT;
     ilRCdbRog = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBufRog);
  }
  close_my_cursor(&slCursor);

  if (strlen(pclAftRegn) > 0)
  {
     ilRC = BuildROGTAB(pclAftRegn,pclAftTime);
     strcpy(pcgRACDataList,"");
  }

  dbg(TRACE,"END: UPDATE ROGTAB");
  return ilRC;
} /* End of UpdateROGTAB */


static int DelROG(char *pcpUrno)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  int ilRogIdx;



  if (igUseArrayInsert == TRUE)
  {
     ilRogIdx = -1;
     ilRC = SearchByKey("URNO",pcpUrno,S_BACK,&ilRogIdx);
     if (ilRogIdx >= 0)
     {
        strcpy(&rgRogDat[ilRogIdx].pclFlag[0],"D");
     }
  }
  else
  {
     strcpy(pclSqlBuf,"DELETE FROM ROGTAB ");
     sprintf(pclSelectBuf,"WHERE URNO = %s",pcpUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"DelROG: <%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     commit_work();
     close_my_cursor(&slCursor);
  }

  return ilRC;
} /* End of DelROG */


static void TrimRight(char *pcpBuffer)
{
  char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];

  if (strlen(pcpBuffer) == 0)
  {
     strcpy(pcpBuffer, " ");
  }
  else
  {
     while (isspace(*pclBlank) && pclBlank != pcpBuffer)
     {
        *pclBlank = '\0';
        pclBlank--;
     }
  }
} /* End of TrimRight */


static int JoinAftRecs(char *pcpArrUrno, char *pcpDepUrno, char *pcpRkey)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  char pclUpdBuf[L_BUFF];
  char pclSelListArr[L_BUFF];
  char pclFieldListArr[L_BUFF];
  char pclDataListArr[L_BUFF];
  char pclSelListDep[L_BUFF];
  char pclFieldListDep[L_BUFF];
  char pclDataListDep[L_BUFF];
  char pclUrnoWhere[64];

  strcpy(pclUpdBuf,"RTYP='J'");
  strcpy(pclFieldListArr,"RTYP,URNO");
  sprintf(pclDataListArr,"J,%s",pcpArrUrno);
  sprintf(pclSqlBuf,"UPDATE AFTTAB SET %s ",pclUpdBuf);
  sprintf(pclSelectBuf,"WHERE URNO = %s",pcpArrUrno);
  sprintf(pclSelListArr,"WHERE URNO=%s\n%s",pcpArrUrno,pcpArrUrno);
  strcat(pclSqlBuf,pclSelectBuf);
  dbg(TRACE,"JoinAftRecs: <%s>",pclSqlBuf);
  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  if (ilRCdb == DB_SUCCESS)
  {
     commit_work();
  }
  close_my_cursor(&slCursor);

  sprintf(pclUpdBuf,"RKEY=%s,RTYP='J',AURN=%s",pcpArrUrno,pcpArrUrno);
  strcpy(pclFieldListDep,"RKEY,RTYP,AURN,URNO");
  sprintf(pclDataListDep,"%s,J,%s,%s",pcpArrUrno,pcpArrUrno,pcpDepUrno);
  sprintf(pclSqlBuf,"UPDATE AFTTAB SET %s ",pclUpdBuf);
  sprintf(pclSelectBuf,"WHERE URNO = %s",pcpDepUrno);
  sprintf(pclSelListDep,"WHERE URNO=%s\n%s",pcpDepUrno,pcpDepUrno);
  strcat(pclSqlBuf,pclSelectBuf);
  dbg(TRACE,"JoinAftRecs: <%s>",pclSqlBuf);
  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  if (ilRCdb == DB_SUCCESS)
  {
     commit_work();
     /* UFIS-1265 */
     dbg(TRACE, "-----------------START SEND FOGU---------------------");
     sprintf(pclUrnoWhere, "WHERE URNO=%s\n%s", pcpArrUrno, pcpArrUrno);
     (void) tools_send_info_flag(igToFogHdl, 0, pcgDestName, "", pcgRecvName,
                "", "", pcgTwStart, pcgTwEnd, "FOGU", pcgCtAft,
                pclUrnoWhere, "URNO", pcpArrUrno, 0);
     dbg(TRACE, "SELECT :\n<%s> , URNO <%s> ", pclUrnoWhere, pcpArrUrno);
     dbg(TRACE, "-----------------END SEND FOGU---------------------");

  }
  close_my_cursor(&slCursor);

  ilRC = TriggerBchdlAction("UFR","AFTTAB",pclSelListArr,pclFieldListArr,
                            pclDataListArr,FALSE,TRUE);
  ilRC = TriggerBchdlAction("UFR","AFTTAB",pclSelListDep,pclFieldListDep,
                            pclDataListDep,FALSE,TRUE);

  return ilRC;
} /* End of JoinAftRecs */


static int SplitAftRecs(char *pcpArrUrno, char *pcpDepUrno)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  char pclUpdBuf[L_BUFF];
  char pclSelListArr[L_BUFF];
  char pclFieldListArr[L_BUFF];
  char pclDataListArr[L_BUFF];
  char pclSelListDep[L_BUFF];
  char pclFieldListDep[L_BUFF];
  char pclDataListDep[L_BUFF];
  char pclUrnoWhere[64];

  if (pcpArrUrno != NULL)
  {
     strcpy(pclUpdBuf,"RTYP='S'");

     strcpy(pclFieldListArr,"RTYP,URNO");
     sprintf(pclDataListArr,"S,%s",pcpArrUrno);
     sprintf(pclSqlBuf,"UPDATE AFTTAB SET %s ",pclUpdBuf);
     sprintf(pclSelectBuf,"WHERE URNO = %s",pcpArrUrno);
     sprintf(pclSelListArr,"WHERE URNO=%s\n%s",pcpArrUrno,pcpArrUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     dbg(TRACE,"SplitAftRecs: <%s>",pclSqlBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
        /* UFIS-1265 */
        dbg(TRACE, "-----------------START SEND FOGU---------------------");
        sprintf(pclUrnoWhere, "WHERE URNO=%s\n%s", pcpArrUrno, pcpArrUrno);
        (void) tools_send_info_flag(igToFogHdl, 0, pcgDestName, "", pcgRecvName,
                "", "", pcgTwStart, pcgTwEnd, "FOGU", pcgCtAft,
                pclUrnoWhere, "URNO", pcpArrUrno, 0);
        dbg(TRACE, "SELECT :\n<%s> , URNO <%s> ", pclUrnoWhere, pcpArrUrno);
        dbg(TRACE, "-----------------END SEND FOGU---------------------");
     }
     close_my_cursor(&slCursor);
  }

  if (pcpDepUrno != NULL)
  {
     sprintf(pclUpdBuf,"RKEY=%s,RTYP='S',AURN=' '",
             pcpDepUrno);
     strcpy(pclFieldListDep,"RKEY,RTYP,AURN,URNO");
     sprintf(pclDataListDep,"%s,S, ,%s",pcpDepUrno,pcpDepUrno);
     sprintf(pclSqlBuf,"UPDATE AFTTAB SET %s ",pclUpdBuf);
     sprintf(pclSelectBuf,"WHERE URNO = %s",pcpDepUrno);
     sprintf(pclSelListDep,"WHERE URNO=%s\n%s",pcpDepUrno,pcpDepUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     dbg(TRACE,"SplitAftRecs: <%s>",pclSqlBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
        /* UFIS-1265 */
        dbg(TRACE, "-----------------START SEND FOGU---------------------");
        sprintf(pclUrnoWhere, "WHERE URNO=%s\n%s", pcpDepUrno, pcpDepUrno);
        (void) tools_send_info_flag(igToFogHdl, 0, pcgDestName, "", pcgRecvName,
                "", "", pcgTwStart, pcgTwEnd, "FOGU", pcgCtAft,
                pclUrnoWhere, "URNO", pcpDepUrno, 0);
        dbg(TRACE, "SELECT :\n<%s> , URNO <%s> ", pclUrnoWhere, pcpDepUrno);
        dbg(TRACE, "-----------------END SEND FOGU---------------------");
     }
     close_my_cursor(&slCursor);
  }

  if (pcpArrUrno != NULL)
  {
     ilRC = TriggerBchdlAction("UFR","AFTTAB",pclSelListArr,pclFieldListArr,
                               pclDataListArr,FALSE,TRUE);
  }
  if (pcpDepUrno != NULL)
  {
     ilRC = TriggerBchdlAction("UFR","AFTTAB",pclSelListDep,pclFieldListDep,
                               pclDataListDep,FALSE,TRUE);
  }

  return ilRC;
} /* End of SplitAftRecs */


static int CheckROGvsAFT(char *pcpRegn, char *pcpSeqn)
{
  int ilRC = RC_SUCCESS;
  int ilRCdbROG = DB_SUCCESS;
  short slFktROG;
  short slCursorROG;
  char pclSqlBufROG[L_BUFF];
  char pclSelectBufROG[L_BUFF];
  char pclDataBuf[L_BUFF];
  int ilRCdbAFT = DB_SUCCESS;
  short slFktAFT;
  short slCursorAFT;
  char pclSqlBufAFT[L_BUFF];
  char pclSelectBufAFT[L_BUFF];
  int ilLen;
  char pclRogAurn[32];
  char pclRogDurn[32];
  char pclRogRkey[32];
  char pclAftaUrno[32];
  char pclAftaRegn[32];
  char pclAftaRkey[32];
  char pclAftaRtyp[32];
  char pclAftaTifa[32];
  char pclAftdAdid[2];
  char pclAftdFtyp[2];
  char pclAftdUrno[32];
  char pclAftdRegn[32];
  char pclAftdRkey[32];
  char pclAftdRtyp[32];
  char pclAftdTifd[32];
  char pclAftdAurn[32];
  int ilDoSplit;
  int ilDoJoin;
  char pclSeqn[32];

  dbg(TRACE,"Check ROG vs AFT for registration <%s> starting at no. <%s>",pcpRegn,pcpSeqn);

/*
  strcpy(pcgRACDataList,"");
  strcpy(pcgRACBegin,"");
  strcpy(pcgRACEnd,"");
*/
  strcpy(pclSeqn,pcpSeqn);
  if (strlen(pclSeqn) == 0)
     strcpy(pclSeqn," ");
  strcpy(pclSqlBufROG,"SELECT AURN,DURN,RKEY FROM ROGTAB ");
  sprintf(pclSelectBufROG,"WHERE REGN = '%s' AND SEQN >= '%s' ORDER BY SEQN",pcpRegn,pclSeqn);
  strcat(pclSqlBufROG,pclSelectBufROG);
  slCursorROG = 0;
  slFktROG = START;
  dbg(TRACE,"<%s>",pclSqlBufROG);
  ilRCdbROG = sql_if(slFktROG,&slCursorROG,pclSqlBufROG,pclDataBuf);
  while (ilRCdbROG == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",3,",");
     ilLen = get_real_item(pclRogAurn,pclDataBuf,1);
     ilLen = get_real_item(pclRogDurn,pclDataBuf,2);
     ilLen = get_real_item(pclRogRkey,pclDataBuf,3);
     strcpy(pclAftaRegn,"");
     strcpy(pclAftaRkey,"");
     strcpy(pclAftaRtyp,"");
     strcpy(pclAftaTifa,"");
     strcpy(pclAftdRegn,"");
     strcpy(pclAftdRkey,"");
     strcpy(pclAftdRtyp,"");
     strcpy(pclAftdTifd,"");
     strcpy(pclAftdAurn,"");

     if (strcmp(pclRogAurn,"0") != 0)
     {
        strcpy(pclSqlBufAFT,"SELECT REGN,RKEY,RTYP,TIFA FROM AFTTAB ");
        sprintf(pclSelectBufAFT,"WHERE URNO = %s",pclRogAurn);
        strcat(pclSqlBufAFT,pclSelectBufAFT);
        slCursorAFT = 0;
        slFktAFT = START;
        dbg(TRACE,"<%s>",pclSqlBufAFT);
        ilRCdbAFT = sql_if(slFktAFT,&slCursorAFT,pclSqlBufAFT,pclDataBuf);
        if (ilRCdbAFT == DB_SUCCESS)
        {
           BuildItemBuffer(pclDataBuf,"",4,",");
           ilLen = get_real_item(pclAftaRegn,pclDataBuf,1);
           ilLen = get_real_item(pclAftaRkey,pclDataBuf,2);
           ilLen = get_real_item(pclAftaRtyp,pclDataBuf,3);
           ilLen = get_real_item(pclAftaTifa,pclDataBuf,4);

        }
        close_my_cursor(&slCursorAFT);
     }

     if (strcmp(pclRogDurn,"0") != 0)
     {
        strcpy(pclSqlBufAFT,"SELECT REGN,RKEY,RTYP,TIFD,AURN FROM AFTTAB ");
        sprintf(pclSelectBufAFT,"WHERE URNO = %s",pclRogDurn);
        strcat(pclSqlBufAFT,pclSelectBufAFT);
        slCursorAFT = 0;
        slFktAFT = START;
        dbg(TRACE,"<%s>",pclSqlBufAFT);
        ilRCdbAFT = sql_if(slFktAFT,&slCursorAFT,pclSqlBufAFT,pclDataBuf);
        if (ilRCdbAFT == DB_SUCCESS)
        {
           BuildItemBuffer(pclDataBuf,"",5,",");
           ilLen = get_real_item(pclAftdRegn,pclDataBuf,1);
           ilLen = get_real_item(pclAftdRkey,pclDataBuf,2);
           ilLen = get_real_item(pclAftdRtyp,pclDataBuf,3);
           ilLen = get_real_item(pclAftdTifd,pclDataBuf,4);
           ilLen = get_real_item(pclAftdAurn,pclDataBuf,5);
        }
        close_my_cursor(&slCursorAFT);
     }

     if (strcmp(pclRogAurn,"0") != 0 && strcmp(pclRogDurn,"0") == 0)
     {  /* Arrival only */
        dbg(TRACE,"==>> HANDLE ARRIVAL ONLY");
        ilDoSplit = FALSE;
        strcpy(pclSqlBufAFT,"SELECT URNO,ADID,FTYP FROM AFTTAB ");

        /* UFIS-1678
         * GFO Added ( ADID = 'B' and FTYP = 'O')
        sprintf(pclSelectBufAFT,"WHERE RKEY = %s AND (ADID = 'D' OR (ADID ='B' AND FTYP='O'))",pclRogRkey);*/
        sprintf(pclSelectBufAFT,"WHERE RKEY = %s AND ADID = 'D' ",pclRogRkey);
        strcat(pclSqlBufAFT,pclSelectBufAFT);
        slCursorAFT = 0;
        slFktAFT = START;
        dbg(TRACE,"<%s>",pclSqlBufAFT);
        ilRCdbAFT = sql_if(slFktAFT,&slCursorAFT,pclSqlBufAFT,pclDataBuf);
        close_my_cursor(&slCursorAFT);
        if (ilRCdbAFT == DB_SUCCESS)
        {

           BuildItemBuffer(pclDataBuf,"",3,",");
           ilLen = get_real_item(pclAftdUrno,pclDataBuf,1);
           ilLen = get_real_item(pclAftdAdid,pclDataBuf,2);
           ilLen = get_real_item(pclAftdFtyp,pclDataBuf,3);

           dbg(TRACE, "==>> FLIGHT FOUND WILL BE  SPLIT ");
               ilRC = SplitAftRecs(pclRogAurn, pclAftdUrno);
               strcat(pcgRACDataList, pclRogAurn);
               strcat(pcgRACDataList, ",");
               strcat(pcgRACDataList, pclAftdUrno);
               strcat(pcgRACDataList, ",");
               ilDoSplit = TRUE;

           /* UFIS-1678
           if ( strcmp(pclAftdAdid,"B") == 0  && strcmp(pclAftdFtyp,"O") == 0) {
               dbg(TRACE,"==>> CIRCULAR FLIGHT FOUND DON'T SPLIT " );
               ilDoSplit = FALSE;
           } else {
               dbg(TRACE, "==>> FLIGHT FOUND WILL BE  SPLIT ");
               ilRC = SplitAftRecs(pclRogAurn, pclAftdUrno);
               strcat(pcgRACDataList, pclRogAurn);
               strcat(pcgRACDataList, ",");
               strcat(pcgRACDataList, pclAftdUrno);
               strcat(pcgRACDataList, ",");
               ilDoSplit = TRUE;
           }
            * */

        }
        else
        {
           if (strcmp(pclAftaRtyp,"S") != 0)
           {

                  dbg(TRACE,"==>> NO RECORD FOUND DEPARTURE " );
                  ilRC = SplitAftRecs(pclRogAurn,NULL);
                  strcat(pcgRACDataList,pclRogAurn);
                  strcat(pcgRACDataList,",");
                  ilDoSplit = TRUE;

           }
        }
        if (ilDoSplit == TRUE)
        {
           strcat(pcgRACDataList,pclRogRkey);
           strcat(pcgRACDataList,",");
           if (strlen(pcgRACBegin) == 0)
           {
              strcpy(pcgRACBegin,pclAftaTifa);
           }
           else
           {
              if (strcmp(pcgRACBegin,pclAftaTifa) > 0)
              {
                 strcpy(pcgRACBegin,pclAftaTifa);
              }
           }
           if (strlen(pcgRACEnd) == 0)
           {
              strcpy(pcgRACEnd,pclAftaTifa);
           }
           else
           {
              if (strcmp(pcgRACEnd,pclAftaTifa) < 0)
              {
                 strcpy(pcgRACEnd,pclAftaTifa);
              }
           }
        }
     }
     else
     {
        if (strcmp(pclRogAurn,"0") == 0 && strcmp(pclRogDurn,"0") != 0)
        {  /* Departure only */
           dbg(TRACE,"==>> HANDLE DEPARTUE ONLY");
           ilDoSplit = FALSE;
           strcpy(pclSqlBufAFT,"SELECT URNO FROM AFTTAB ");
           sprintf(pclSelectBufAFT,"WHERE RKEY = %s AND ADID = 'A'",pclRogRkey);
           strcat(pclSqlBufAFT,pclSelectBufAFT);
           slCursorAFT = 0;
           slFktAFT = START;
           dbg(TRACE,"<%s>",pclSqlBufAFT);
           ilRCdbAFT = sql_if(slFktAFT,&slCursorAFT,pclSqlBufAFT,pclDataBuf);
           close_my_cursor(&slCursorAFT);
           if (ilRCdbAFT == DB_SUCCESS)
           {
              BuildItemBuffer(pclDataBuf,"",1,",");
              ilLen = get_real_item(pclAftaUrno,pclDataBuf,1);
              ilRC = SplitAftRecs(pclAftaUrno,pclRogDurn);
              strcat(pcgRACDataList,pclAftaUrno);
              strcat(pcgRACDataList,",");
              strcat(pcgRACDataList,pclRogDurn);
              strcat(pcgRACDataList,",");
              ilDoSplit = TRUE;
           }
           else
           {
              if (strcmp(pclAftdRtyp,"S") != 0 || strlen(pclAftdAurn) > 0)
              {
                 ilRC = SplitAftRecs(NULL,pclRogDurn);
                 strcat(pcgRACDataList,pclRogDurn);
                 strcat(pcgRACDataList,",");
                 ilDoSplit = TRUE;
              }
           }
           if (ilDoSplit == TRUE)
           {
              strcat(pcgRACDataList,pclRogRkey);
              strcat(pcgRACDataList,",");
              if (strlen(pcgRACBegin) == 0)
              {
                 strcpy(pcgRACBegin,pclAftdTifd);
              }
              else
              {
                 if (strcmp(pcgRACBegin,pclAftdTifd) > 0)
                 {
                    strcpy(pcgRACBegin,pclAftdTifd);
                 }
              }
              if (strlen(pcgRACEnd) == 0)
              {
                 strcpy(pcgRACEnd,pclAftdTifd);
              }
              else
              {
                 if (strcmp(pcgRACEnd,pclAftdTifd) < 0)
                 {
                    strcpy(pcgRACEnd,pclAftdTifd);
                 }
              }
           }
        }
        else
        {  /* Rotation */
           dbg(TRACE,"==>> HANDLE ROTATION");
           ilDoJoin = FALSE;
           if (strcmp(pclAftaRkey,pclRogRkey) != 0 ||
               strcmp(pclAftdRkey,pclRogRkey) != 0)
           {
              ilDoJoin = TRUE;
              strcpy(pclSqlBufAFT,"SELECT URNO FROM AFTTAB ");
              sprintf(pclSelectBufAFT,"WHERE RKEY = %s AND ADID = 'D'",pclRogRkey);
              strcat(pclSqlBufAFT,pclSelectBufAFT);
              slCursorAFT = 0;
              slFktAFT = START;
              dbg(TRACE,"<%s>",pclSqlBufAFT);
              ilRCdbAFT = sql_if(slFktAFT,&slCursorAFT,pclSqlBufAFT,pclDataBuf);
              close_my_cursor(&slCursorAFT);
              if (ilRCdbAFT == DB_SUCCESS)
              {
                 BuildItemBuffer(pclDataBuf,"",1,",");
                 ilLen = get_real_item(pclAftdUrno,pclDataBuf,1);
                 ilRC = SplitAftRecs(pclRogAurn,pclAftdUrno);
              }
              ilRC = JoinAftRecs(pclRogAurn,pclRogDurn,pclRogRkey);
           }
           else
           {
              if (strcmp(pclAftaRtyp,"J") != 0 || strcmp(pclAftdRtyp,"J") != 0 ||
                  strlen(pclAftdAurn) == 0)
              {
                 ilDoJoin = TRUE;
                 ilRC = JoinAftRecs(pclRogAurn,pclRogDurn,pclRogRkey);
              }
           }
           if (ilDoJoin == TRUE)
           {
              strcat(pcgRACDataList,pclRogAurn);
              strcat(pcgRACDataList,",");
              strcat(pcgRACDataList,pclRogDurn);
              strcat(pcgRACDataList,",");
              strcat(pcgRACDataList,pclRogRkey);
              strcat(pcgRACDataList,",");
              if (strlen(pcgRACBegin) == 0)
              {
                 strcpy(pcgRACBegin,pclAftaTifa);
              }
              else
              {
                 if (strcmp(pcgRACBegin,pclAftaTifa) > 0)
                 {
                    strcpy(pcgRACBegin,pclAftaTifa);
                 }
              }
              if (strlen(pcgRACEnd) == 0)
              {
                 strcpy(pcgRACEnd,pclAftdTifd);
              }
              else
              {
                 if (strcmp(pcgRACEnd,pclAftdTifd) < 0)
                 {
                    strcpy(pcgRACEnd,pclAftdTifd);
                 }
              }
           }
        }
     }

     slFktROG = NEXT;
     ilRCdbROG = sql_if(slFktROG,&slCursorROG,pclSqlBufROG,pclDataBuf);
  }
  close_my_cursor(&slCursorROG);

  dbg(TRACE,"All records for registration <%s> checked",pcpRegn);

  return ilRC;
} /* End of CheckROGvsAFT */



static int CompressDataList(char *pcpData)
{
  int ilRC = RC_SUCCESS;
  char pclData[XXL_BUFF];
  char *pclPtrS;
  char *pclPtrE;

  strcpy(pclData,pcpData);
  strcpy(pcpData,"");
  pclPtrS = pclData;
  pclPtrE = strstr(pclPtrS,",");
  while (pclPtrE != NULL)
  {
     *pclPtrE = '\0';
     if (strstr(pcpData,pclPtrS) == NULL)
     {
        strcat(pcpData,pclPtrS);
        strcat(pcpData,",");
     }
     pclPtrS = pclPtrE + 1;
     pclPtrE = strstr(pclPtrS,",");
  }
  if (strstr(pcpData,pclPtrS) == NULL)
  {
     strcat(pcpData,pclPtrS);
     strcat(pcpData,",");
  }
  if (pcpData[strlen(pcpData)-1] == ',')
  {
     pcpData[strlen(pcpData)-1] = '\0';
  }

  return ilRC;
} /* End of CompressData */



static int AppendUrnoList()
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  char pclTmpRkey[16];
  int ilCount;
  int ilI;
  int ilLen;

  ilCount = GetNoOfElements(pcgRACDataList,',');
  for (ilI = 1; ilI <= ilCount; ilI++)
  {
     ilLen = get_real_item(pclTmpRkey,pcgRACDataList,ilI);
     if (ilLen > 0)
     {
        sprintf(pclSqlBuf,
                "SELECT URNO FROM AFTTAB WHERE RKEY = %s AND (ADID = 'A' OR ADID = 'D')",
                pclTmpRkey);
        slCursor = 0;
        slFkt = START;
        /* dbg(TRACE,"<%s>",pclSqlBuf); */
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        while (ilRCdb == DB_SUCCESS)
        {
           if (strstr(pcgUrnoList,pclDataBuf) == NULL)
           {
              if (strlen(pcgUrnoList) == 0)
              {
                 strcpy(pcgUrnoList,pclDataBuf);
              }
              else
              {
                 strcat(pcgUrnoList,",");
                 strcat(pcgUrnoList,pclDataBuf);
              }
           }
           slFkt = NEXT;
           ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        }
        close_my_cursor(&slCursor);
     }
  }

  return ilRC;
} /* End of AppendUrnoList */



static int CalculateMaxCEDATime(char *pcpResult, char *pcpVal1, char *pcpVal2, char *pcpVal3,
                                char *pcpVal4)
{
  int ilRC = RC_SUCCESS;
  char pclTmpResult[32];
  char pclTmpVal1[32];
  char pclTmpVal2[32];
  char pclTmpVal3[32];
  char pclTmpVal4[32];

  strcpy(pclTmpResult,"              ");
  strcpy(pclTmpVal1,"              ");
  strcpy(pclTmpVal2,"              ");
  strcpy(pclTmpVal3,"              ");
  strcpy(pclTmpVal4,"              ");
  if (strlen(pcpResult) == 14)
  {
     strcpy(pclTmpResult,pcpResult);
  }
  if (strlen(pcpVal1) == 14)
  {
     strcpy(pclTmpVal1,pcpVal1);
  }
  if (strlen(pcpVal2) == 14)
  {
     strcpy(pclTmpVal2,pcpVal2);
  }
  if (strlen(pcpVal3) == 14)
  {
     strcpy(pclTmpVal3,pcpVal3);
  }
  if (strlen(pcpVal4) == 14)
  {
     strcpy(pclTmpVal4,pcpVal4);
  }

  if (strcmp(pclTmpResult,pclTmpVal1) < 0)
  {
     strcpy(pclTmpResult,pclTmpVal1);
  }
  if (strcmp(pclTmpResult,pclTmpVal2) < 0)
  {
     strcpy(pclTmpResult,pclTmpVal2);
  }
  if (strcmp(pclTmpResult,pclTmpVal3) < 0)
  {
     strcpy(pclTmpResult,pclTmpVal3);
  }
  if (strcmp(pclTmpResult,pclTmpVal4) < 0)
  {
     strcpy(pclTmpResult,pclTmpVal4);
  }

  strcpy(pcpResult,pclTmpResult);

  return ilRC;
} /* End of CalculateMaxCEDATime */


static int MyAddSecondsToCEDATime(char *pcpTime, long lpValue, int ipP)
{
  int ilRC = RC_SUCCESS;

  if (strlen(pcpTime) == 12)
     strcat(pcpTime,"00");
  ilRC = AddSecondsToCEDATime(pcpTime,lpValue,ipP);
  return ilRC;
} /* End of MyAddSecondsToCEDATime */


static int SqlArrayInsert()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SqlArrayInsert:";
  int ilRCdb = DB_SUCCESS;
  char pclSqlBuf[1024];
  short slFkt = 0;
  short slCursor = 0;

  sprintf(pclSqlBuf,"INSERT INTO ROGTAB (%s) VALUES (%s)",pcgRogFld,pcgRogVal);
  slCursor = 0;
  slFkt = 0;
  if (strcmp(pcgInitRog,"NO") == 0)
     dbg(DEBUG,"%s <%s>\n%s",pclFunc,pclSqlBuf,pcgRogBuf);
  ilRCdb = SqlIfArray(slFkt,&slCursor,pclSqlBuf,pcgRogBuf,0,&rgRecDesc);
  if (ilRCdb == DB_SUCCESS)
  {
     commit_work();
  }
  else
  {
     rollback();
     dbg(TRACE,"%s Error = %d",pclFunc,ilRCdb);
  }
  close_my_cursor(&slCursor);

  return ilRC;
} /* End of SqlArrayInsert */


static int SqlArrayFill()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SqlArrayFill:";
  char pclRogRec[256];
  int ilI;
  int ilArrayPtr;

  ilArrayPtr = 0;
  for (ilI = 0; ilI < igCurRogDat; ilI++)
  {
     if (rgRogDat[ilI].pclFlag[0] == ' ')
     {
        sprintf(pclRogRec,"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
                &rgRogDat[ilI].pclUrno[0],
                &rgRogDat[ilI].pclHopo[0],
                &rgRogDat[ilI].pclRegn[0],
                &rgRogDat[ilI].pclAurn[0],
                &rgRogDat[ilI].pclDurn[0],
                &rgRogDat[ilI].pclRkey[0],
                &rgRogDat[ilI].pclLand[0],
                &rgRogDat[ilI].pclAirb[0],
                &rgRogDat[ilI].pclFtyp[0],
                &rgRogDat[ilI].pclStat[0],
                &rgRogDat[ilI].pclFtpa[0],
                &rgRogDat[ilI].pclFtpd[0],
                &rgRogDat[ilI].pclSeqn[0],
                &rgRogDat[ilI].pclSeqs[0]);
        StrgPutStrg(pcgRogBuf,&ilArrayPtr,pclRogRec,0,-1,"\n");
     }
  }
  ilArrayPtr--;
  pcgRogBuf[ilArrayPtr] = '\0';

  return ilRC;
} /* End of SqlArrayFill */


static int SearchByKey(char *pcpKey, char * pcpVal, int ilDirection, int *pipIndex)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchByKey:";
  int ilI;
  int ilFound;

  if (ilDirection == S_FOR)
     ilI = 0;
  else
     ilI = igCurRogDat - 1;
  ilFound = FALSE;
  while (ilFound == FALSE && igCurRogDat >= 0 && ilI < igCurRogDat && ilI >= 0)
  {
     if (strcmp(pcpKey,"URNO") == 0)
        ilRC = strcmp(&rgRogDat[ilI].pclUrno[0],pcpVal);
     else if (strcmp(pcpKey,"AURN") == 0)
             ilRC = strcmp(&rgRogDat[ilI].pclAurn[0],pcpVal);
     else if (strcmp(pcpKey,"DURN") == 0)
             ilRC = strcmp(&rgRogDat[ilI].pclDurn[0],pcpVal);
     if (ilRC == 0)
     {
        ilFound = TRUE;
        *pipIndex = ilI;
     }
     else
     {
        if (ilDirection == S_FOR)
           ilI++;
        else
           ilI--;
     }
  }

  return ilRC;
} /* End of SearchByKey */


static int SearchByAIRB(char *pcpAirb,int *pipIndex)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchByAIRB:";
  int ilI;
  int ilFound;
  int ilLastIdx;
  char pclAirb[16];
  char pclLastAirb[16];

  strcpy(pclAirb,pcpAirb);
  if (strlen(pclAirb) < 14)
     strcat(pclAirb,"00");
  ilLastIdx = -1;
  ilI = 0;
  ilFound = FALSE;
  while (ilFound == FALSE && igCurRogDat >= 0 && ilI < igCurRogDat)
  {
     if (rgRogDat[ilI].pclLand[0] == ' ' &&
         rgRogDat[ilI].pclStat[0] == 'D')
     {
        ilRC = strcmp(&rgRogDat[ilI].pclAirb[0],pclAirb);
        if (ilRC > 0)
        {
           if (ilLastIdx == -1)
           {
              ilLastIdx = ilI;
              strcpy(pclLastAirb,&rgRogDat[ilI].pclAirb[0]);
           }
           else
           {
              if (strcmp(&rgRogDat[ilI].pclAirb[0],pclLastAirb) < 0)
              {
                 ilLastIdx = ilI;
                 strcpy(pclLastAirb,&rgRogDat[ilI].pclAirb[0]);
              }
           }
        }
     }
     ilI++;
  }
  *pipIndex = ilLastIdx;

  return ilRC;
} /* End of SearchByAIRB */


static int SearchByRKEYDep(char *pcpRkey,int *pipIndex)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchByRKEYDep:";
  int ilI;
  int ilFound;

  ilI = igCurRogDat - 1;
  ilFound = FALSE;
  while (ilFound == FALSE && igCurRogDat >= 0 && ilI >= 0)
  {
     ilRC = strcmp(&rgRogDat[ilI].pclRkey[0],pcpRkey);
     if (ilRC == 0)
     {
        if (rgRogDat[ilI].pclLand[0] == ' ' &&
            rgRogDat[ilI].pclStat[0] == 'D')
        {
           ilFound = TRUE;
           *pipIndex = ilI;
        }
     }
     ilI--;
  }

  return ilRC;
} /* End of SearchByRKEYDep */


static int SearchByLAND(char *pcpLand,int *pipIndex)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchByLAND:";
  int ilI;
  int ilFound;
  int ilLastIdx;
  char pclLastLand[16];
  int ilLastIdxRead;
  char pclLastLandRead[16];

  ilLastIdx = -1;
  ilLastIdxRead = -1;
  if (*pipIndex >= 0)
  {
     ilLastIdx = *pipIndex;
     strcpy(pclLastLand,&rgRogDat[*pipIndex].pclLand[0]);
     ilLastIdxRead = *pipIndex;
     strcpy(pclLastLandRead,&rgRogDat[*pipIndex].pclLand[0]);
  }
  else
  {
     strcpy(pclLastLandRead,pcpLand);
  }
  if (strlen(pclLastLandRead) < 14)
     strcat(pclLastLandRead,"00");
  ilI = igCurRogDat - 1;
  ilFound = FALSE;
  while (ilFound == FALSE && igCurRogDat >= 0 && ilI >= 0)
  {
     if (rgRogDat[ilI].pclFtpa[0] != 'X' &&
         rgRogDat[ilI].pclFtpa[0] != 'N' &&
         rgRogDat[ilI].pclFtpa[0] != 'D')
     {
        ilRC = strcmp(&rgRogDat[ilI].pclLand[0],pclLastLandRead);
        if (ilRC < 0)
        {
           if (ilLastIdx == -1)
           {
              ilLastIdx = ilI;
              strcpy(pclLastLand,&rgRogDat[ilI].pclLand[0]);
           }
           else
           {
              if (strcmp(&rgRogDat[ilI].pclLand[0],pclLastLand) > 0)
              {
                 ilLastIdx = ilI;
                 strcpy(pclLastLand,&rgRogDat[ilI].pclLand[0]);
              }
           }
        }
     }
     ilI--;
  }
  if (ilLastIdx >= 0)
     *pipIndex = ilLastIdx;

  return ilRC;
} /* End of SearchByLAND */


static int SearchByRKEYArr(char *pcpRkey,int *pipIndex)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchByRKEYArr:";
  int ilI;
  int ilFound;

  ilI = igCurRogDat - 1;
  ilFound = FALSE;
  while (ilFound == FALSE && igCurRogDat >= 0 && ilI >= 0)
  {
     ilRC = strcmp(&rgRogDat[ilI].pclRkey[0],pcpRkey);
     if (ilRC == 0)
     {
        if (rgRogDat[ilI].pclAirb[0] == ' ' &&
            rgRogDat[ilI].pclStat[0] == 'A')
        {
           ilFound = TRUE;
           *pipIndex = ilI;
        }
     }
     ilI--;
  }

  return ilRC;
} /* End of SearchByRKEYArr */


static int SearchByLANDSeqs(char *pcpLand,int *pipIndex)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchByLANDSeqs:";
  int ilI;
  int ilFound;
  int ilLastIdx;
  char pclLastLand[16];
  int ilLastIdxRead;
  char pclLastLandRead[16];

  ilLastIdx = -1;
  ilLastIdxRead = -1;
  if (*pipIndex >= 0)
  {
     ilLastIdx = *pipIndex;
     strcpy(pclLastLand,&rgRogDat[*pipIndex].pclLand[0]);
     ilLastIdxRead = *pipIndex;
     strcpy(pclLastLandRead,&rgRogDat[*pipIndex].pclLand[0]);
  }
  else
  {
     strcpy(pclLastLandRead,pcpLand);
  }
  if (strlen(pclLastLandRead) < 14)
     strcat(pclLastLandRead,"00");
  ilI = igCurRogDat - 1;
  ilFound = FALSE;
  while (ilFound == FALSE && igCurRogDat >= 0 && ilI >= 0)
  {
     if (rgRogDat[ilI].pclFtyp[0] != 'E' &&
         rgRogDat[ilI].pclStat[0] == 'B' &&
         rgRogDat[ilI].pclSeqs[0] == 'O' &&
         rgRogDat[ilI].pclLand[0] != ' ')
     {
        ilRC = strcmp(&rgRogDat[ilI].pclLand[0],pclLastLandRead);
        if (ilRC < 0)
        {
           if (ilLastIdx == -1)
           {
              ilLastIdx = ilI;
              strcpy(pclLastLand,&rgRogDat[ilI].pclLand[0]);
           }
           else
           {
              if (strcmp(&rgRogDat[ilI].pclLand[0],pclLastLand) > 0)
              {
                 ilLastIdx = ilI;
                 strcpy(pclLastLand,&rgRogDat[ilI].pclLand[0]);
              }
           }
        }
     }
     ilI--;
  }
  if (ilLastIdx >= 0)
     *pipIndex = ilLastIdx;

  return ilRC;
} /* End of SearchByLANDSeqs */


static int SearchByLANDSeqn(char *pcpSeqn,int *pipIndex)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchByLANDSeqn:";
  int ilI;
  int ilFound;
  int ilLastIdx;
  char pclLastLand[16];
  int ilLastIdxRead;
  char pclLastLandRead[16];
  int ilSeqn;
  int ilRogSeqn;

  ilLastIdx = -1;
  ilLastIdxRead = -1;
  if (*pipIndex >= 0)
  {
     ilLastIdxRead = *pipIndex;
     strcpy(pclLastLandRead,&rgRogDat[*pipIndex].pclLand[0]);
  }
  else
  {
     strcpy(pclLastLandRead,"00000000000000");
  }
  if (strlen(pclLastLandRead) < 14)
     strcat(pclLastLandRead,"00");
  ilSeqn = atoi(pcpSeqn);
  ilI = 0;
  ilFound = FALSE;
  while (ilFound == FALSE && igCurRogDat >= 0 && ilI < igCurRogDat)
  {
     ilRogSeqn = atoi(&rgRogDat[ilI].pclSeqn[0]);
     if ((rgRogDat[ilI].pclStat[0] == 'A' ||
          rgRogDat[ilI].pclStat[0] == 'B' ||
          rgRogDat[ilI].pclStat[0] == 'E') &&
         (rgRogDat[ilI].pclSeqn[0] == ' ' ||
          ilRogSeqn >= ilSeqn))
     {
        ilRC = strcmp(&rgRogDat[ilI].pclLand[0],pclLastLandRead);
        if (ilRC > 0)
        {
           if (ilLastIdx == -1)
           {
              ilLastIdx = ilI;
              strcpy(pclLastLand,&rgRogDat[ilI].pclLand[0]);
           }
           else
           {
              if (strcmp(&rgRogDat[ilI].pclLand[0],pclLastLand) < 0)
              {
                 ilLastIdx = ilI;
                 strcpy(pclLastLand,&rgRogDat[ilI].pclLand[0]);
              }
           }
        }
     }
     ilI++;
  }
  if (ilLastIdx >= 0)
     *pipIndex = ilLastIdx;

  return ilRC;
} /* End of SearchByLANDSeqn */


static int SearchByAIRBSeqn(char *pcpSeqn,int *pipIndex)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchByAIRBSeqn:";
  int ilI;
  int ilFound;
  int ilLastIdx;
  char pclLastAirb[16];
  int ilLastIdxRead;
  char pclLastAirbRead[16];
  int ilSeqn;
  int ilRogSeqn;

  ilLastIdx = -1;
  ilLastIdxRead = -1;
  if (*pipIndex >= 0)
  {
     ilLastIdxRead = *pipIndex;
     strcpy(pclLastAirbRead,&rgRogDat[*pipIndex].pclAirb[0]);
  }
  else
  {
     strcpy(pclLastAirbRead,"00000000000000");
  }
  if (strlen(pclLastAirbRead) < 14)
     strcat(pclLastAirbRead,"00");
  ilSeqn = atoi(pcpSeqn);
  ilI = 0;
  ilFound = FALSE;
  while (ilFound == FALSE && igCurRogDat >= 0 && ilI < igCurRogDat)
  {
     ilRogSeqn = atoi(&rgRogDat[ilI].pclSeqn[0]);
     if ((rgRogDat[ilI].pclStat[0] == 'D' ||
          rgRogDat[ilI].pclStat[0] == 'R') &&
         (rgRogDat[ilI].pclSeqn[0] == ' ' ||
          ilRogSeqn >= ilSeqn))
     {
        ilRC = strcmp(&rgRogDat[ilI].pclAirb[0],pclLastAirbRead);
        if (ilRC > 0)
        {
           if (ilLastIdx == -1)
           {
              ilLastIdx = ilI;
              strcpy(pclLastAirb,&rgRogDat[ilI].pclAirb[0]);
           }
           else
           {
              if (strcmp(&rgRogDat[ilI].pclAirb[0],pclLastAirb) < 0)
              {
                 ilLastIdx = ilI;
                 strcpy(pclLastAirb,&rgRogDat[ilI].pclAirb[0]);
              }
           }
        }
     }
     ilI++;
  }
  if (ilLastIdx >= 0)
     *pipIndex = ilLastIdx;

  return ilRC;
} /* End of SearchByAIRBSeqn */


static int SearchBySEQN(char *pcpSeqn,int *pipIndex)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchBySEQN:";
  int ilI;
  int ilFound;
  int ilLastIdx;
  char pclLastSeqn[16];
  int ilLastIdxRead;
  char pclLastSeqnRead[16];
  int ilSeqn;
  int ilRogSeqn;

  ilLastIdx = -1;
  ilLastIdxRead = -1;
  if (*pipIndex >= 0)
  {
     ilLastIdxRead = *pipIndex;
     strcpy(pclLastSeqnRead,&rgRogDat[*pipIndex].pclSeqn[0]);
  }
  else
  {
     strcpy(pclLastSeqnRead,"0000000000");
  }
  ilSeqn = atoi(pcpSeqn);
  ilI = 0;
  ilFound = FALSE;
  while (ilFound == FALSE && igCurRogDat >= 0 && ilI < igCurRogDat)
  {
     ilRogSeqn = atoi(&rgRogDat[ilI].pclSeqn[0]);
     if (rgRogDat[ilI].pclFlag[0] == ' ')
     {
        ilRC = strcmp(&rgRogDat[ilI].pclSeqn[0],pclLastSeqnRead);
        if (ilRC > 0)
        {
           if (ilLastIdx == -1)
           {
              ilLastIdx = ilI;
              strcpy(pclLastSeqn,&rgRogDat[ilI].pclSeqn[0]);
           }
           else
           {
              if (strcmp(&rgRogDat[ilI].pclSeqn[0],pclLastSeqn) < 0)
              {
                 ilLastIdx = ilI;
                 strcpy(pclLastSeqn,&rgRogDat[ilI].pclSeqn[0]);
              }
           }
        }
     }
     ilI++;
  }
  if (ilLastIdx >= 0)
     *pipIndex = ilLastIdx;

  return ilRC;
} /* End of SearchBySEQN */


static int CheckBcMode(char *pcpNPabs, char *pcpOPabs, char *pcpNPaes, char *pcpOPaes,
                       char *pcpNPdbs, char *pcpOPdbs, char *pcpNPdes, char *pcpOPdes,
                       char *pcpFieldList, char *pcpDataList)
{
  int ilRC = RC_SUCCESS;

  if (igBcOutMode == 1)
  {
     if (strcmp(pcpNPabs,pcpOPabs) != 0)
     {
        strcat(pcpFieldList,",#PABS");
        strcat(pcpDataList,",");
        strcat(pcpDataList,pcpOPabs);
     }
     if (strcmp(pcpNPaes,pcpOPaes) != 0)
     {
        strcat(pcpFieldList,",#PAES");
        strcat(pcpDataList,",");
        strcat(pcpDataList,pcpOPaes);
     }
     if (strcmp(pcpNPdbs,pcpOPdbs) != 0)
     {
        strcat(pcpFieldList,",#PDBS");
        strcat(pcpDataList,",");
        strcat(pcpDataList,pcpOPdbs);
     }
     if (strcmp(pcpNPdes,pcpOPdes) != 0)
     {
        strcat(pcpFieldList,",#PDES");
        strcat(pcpDataList,",");
        strcat(pcpDataList,pcpOPdes);
     }
  }
  if (igBcOutMode == 2)
  {
     strcat(pcpFieldList,",[CF]");
     strcat(pcpDataList,",[CF]");
     if (strcmp(pcpNPabs,pcpOPabs) != 0)
     {
        strcat(pcpFieldList,",PABS");
        strcat(pcpDataList,",");
        strcat(pcpDataList,pcpNPabs);
     }
     if (strcmp(pcpNPaes,pcpOPaes) != 0)
     {
        strcat(pcpFieldList,",PAES");
        strcat(pcpDataList,",");
        strcat(pcpDataList,pcpNPaes);
     }
     if (strcmp(pcpNPdbs,pcpOPdbs) != 0)
     {
        strcat(pcpFieldList,",PDBS");
        strcat(pcpDataList,",");
        strcat(pcpDataList,pcpNPdbs);
     }
     if (strcmp(pcpNPdes,pcpOPdes) != 0)
     {
        strcat(pcpFieldList,",PDES");
        strcat(pcpDataList,",");
        strcat(pcpDataList,pcpNPdes);
     }
  }

  return ilRC;
} /* End of CheckBcMode */


static int HandleSimulation()
{
  int ilRC = RC_SUCCESS;
  char pclCommand[32];
  char pclTable[32];
  char pclFields[1024];
  char pclFieldsOld[1024];
  char pclData[1024];
  char pclDataOld[1024];
  char pclSelect[1024];

  ilRC = iGetConfigEntry(pcgConfigFile,"SEND_TO_FLIGHT","Command",CFG_STRING,pclCommand);
  ilRC = iGetConfigEntry(pcgConfigFile,"SEND_TO_FLIGHT","Table",CFG_STRING,pclTable);
  ilRC = iGetConfigEntry(pcgConfigFile,"SEND_TO_FLIGHT","Fields",CFG_STRING,pclFields);
  ilRC = iGetConfigEntry(pcgConfigFile,"SEND_TO_FLIGHT","FieldsOld",CFG_STRING,pclFieldsOld);
  ilRC = iGetConfigEntry(pcgConfigFile,"SEND_TO_FLIGHT","Data",CFG_STRING,pclData);
  ilRC = iGetConfigEntry(pcgConfigFile,"SEND_TO_FLIGHT","DataOld",CFG_STRING,pclDataOld);
  ilRC = iGetConfigEntry(pcgConfigFile,"SEND_TO_FLIGHT","Select",CFG_STRING,pclSelect);
dbg(TRACE," ");
dbg(TRACE,"Command:   <%s>",pclCommand);
dbg(TRACE,"Table:     <%s>",pclTable);
dbg(TRACE,"Fields:    <%s>",pclFields);
dbg(TRACE,"FieldsOld: <%s>",pclFieldsOld);
dbg(TRACE,"Data:      <%s>",pclData);
dbg(TRACE,"DataOld:   <%s>",pclDataOld);
dbg(TRACE,"Select:    <%s>",pclSelect);
  strcat(pclFields,"\n");
  strcat(pclFields,pclFieldsOld);
  strcat(pclData,"\n");
  strcat(pclData,pclDataOld);

  (void) tools_send_info_flag(7800,0,"CDI2ATC","","CEDA","","",pcgTwStart,pcgTwEnd,
                              pclCommand,pclTable,pclSelect,pclFields,pclData,0);

  return ilRC;
} /* End of HandleSimulation */

/* *************************************************** */
/* *************************************************** */
static int InitMyTab(char *pcpCtTabName, char *pcpCtFldList, char *pcpCtFldLength)
{
  int ilRC = RC_SUCCESS;
  int i = 0;

  if (strcmp(pcpCtTabName,"STR_DESC") != 0)
  {
    CT_CreateArray(pcpCtTabName);
    CT_SetFieldList(pcpCtTabName,pcpCtFldList);
    CT_SetLengthList(pcpCtTabName, pcpCtFldLength);
    CT_SetDataPoolAllocSize(pcpCtTabName, 1000);
    dbg(TRACE,"CT DATA GRID <%s> INITIALIZED",pcpCtTabName);
  }
  else
  {
    for (i=0;i<=MAX_CT_VALUE;i++)
	{
      CT_InitStringDescriptor(&argCtValue[i]);
      argCtValue[i].Value = malloc(4096);
      argCtValue[i].AllocatedSize = 4096;
      argCtValue[i].Value[0] = 0x00;
	}
    CT_InitStringDescriptor(&rgCtLine);
    rgCtLine.Value = malloc(4096);
    rgCtLine.AllocatedSize = 4096;
    rgCtLine.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCtData);
    rgCtData.Value = malloc(4096);
    rgCtData.AllocatedSize = 4096;
    rgCtData.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCtList);
    rgCtList.Value = malloc(9182);
    rgCtList.AllocatedSize = 9182;
    rgCtList.Value[0] = 0x00;

    /* ------------------------------ */
    CT_InitStringDescriptor(&rgGorPosList);
    rgGorPosList.Value = malloc(4096);
    rgGorPosList.AllocatedSize = 4096;
    rgGorPosList.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgTowStndArr);
    rgTowStndArr.Value = malloc(4096);
    rgTowStndArr.AllocatedSize = 4096;
    rgTowStndArr.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgTowStndDep);
    rgTowStndDep.Value = malloc(4096);
    rgTowStndDep.AllocatedSize = 4096;
    rgTowStndDep.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgTowPrioArr);
    rgTowPrioArr.Value = malloc(4096);
    rgTowPrioArr.AllocatedSize = 4096;
    rgTowPrioArr.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgTowPrioDep);
    rgTowPrioDep.Value = malloc(4096);
    rgTowPrioDep.AllocatedSize = 4096;
    rgTowPrioDep.Value[0] = 0x00;

    /* ------------------------------ */
    CT_InitStringDescriptor(&rgAnyStrg);
    rgAnyStrg.Value = malloc(512);
    rgAnyStrg.AllocatedSize = 512;
    rgAnyStrg.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgGridVal);
    rgGridVal.Value = malloc(512);
    rgGridVal.AllocatedSize = 512;
    rgGridVal.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurPrfl);
    rgCurPrfl.Value = malloc(16);
    rgCurPrfl.AllocatedSize = 16;
    rgCurPrfl.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurPrio);
    rgCurPrio.Value = malloc(16);
    rgCurPrio.AllocatedSize = 16;
    rgCurPrio.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurAdid);
    rgCurAdid.Value = malloc(16);
    rgCurAdid.AllocatedSize = 16;
    rgCurAdid.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurFtyp);
    rgCurFtyp.Value = malloc(16);
    rgCurFtyp.AllocatedSize = 16;
    rgCurFtyp.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurStnd);
    rgCurStnd.Value = malloc(16);
    rgCurStnd.AllocatedSize = 16;
    rgCurStnd.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurPstd);
    rgCurPstd.Value = malloc(16);
    rgCurPstd.AllocatedSize = 16;
    rgCurPstd.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurPsta);
    rgCurPsta.Value = malloc(16);
    rgCurPsta.AllocatedSize = 16;
    rgCurPsta.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurTifd);
    rgCurTifd.Value = malloc(16);
    rgCurTifd.AllocatedSize = 16;
    rgCurTifd.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurTifa);
    rgCurTifa.Value = malloc(16);
    rgCurTifa.AllocatedSize = 16;
    rgCurTifa.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurStod);
    rgCurStod.Value = malloc(16);
    rgCurStod.AllocatedSize = 16;
    rgCurStod.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurOfbl);
    rgCurOfbl.Value = malloc(16);
    rgCurOfbl.AllocatedSize = 16;
    rgCurOfbl.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurOnbl);
    rgCurOnbl.Value = malloc(16);
    rgCurOnbl.AllocatedSize = 16;
    rgCurOnbl.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurStoa);
    rgCurStoa.Value = malloc(16);
    rgCurStoa.AllocatedSize = 16;
    rgCurStoa.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurUrno);
    rgCurUrno.Value = malloc(16);
    rgCurUrno.AllocatedSize = 16;
    rgCurUrno.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurAurn);
    rgCurAurn.Value = malloc(16);
    rgCurAurn.AllocatedSize = 16;
    rgCurAurn.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurRkey);
    rgCurRkey.Value = malloc(16);
    rgCurRkey.AllocatedSize = 16;
    rgCurRkey.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurRegn);
    rgCurRegn.Value = malloc(16);
    rgCurRegn.AllocatedSize = 16;
    rgCurRegn.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurAct3);
    rgCurAct3.Value = malloc(16);
    rgCurAct3.AllocatedSize = 16;
    rgCurAct3.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurRem1);
    rgCurRem1.Value = malloc(512);
    rgCurRem1.AllocatedSize = 512;
    rgCurRem1.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurRem2);
    rgCurRem2.Value = malloc(512);
    rgCurRem2.AllocatedSize = 512;
    rgCurRem2.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurUsec);
    rgCurUsec.Value = malloc(64);
    rgCurUsec.AllocatedSize = 64;
    rgCurUsec.Value[0] = 0x00;

    /* ------------------------------ */
    CT_InitStringDescriptor(&rgPrvPrio);
    rgPrvPrio.Value = malloc(16);
    rgPrvPrio.AllocatedSize = 16;
    rgPrvPrio.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgPrvAdid);
    rgPrvAdid.Value = malloc(16);
    rgPrvAdid.AllocatedSize = 16;
    rgPrvAdid.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgPrvFtyp);
    rgPrvFtyp.Value = malloc(16);
    rgPrvFtyp.AllocatedSize = 16;
    rgPrvFtyp.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgPrvStnd);
    rgPrvStnd.Value = malloc(16);
    rgPrvStnd.AllocatedSize = 16;
    rgPrvStnd.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgPrvPstd);
    rgPrvPstd.Value = malloc(16);
    rgPrvPstd.AllocatedSize = 16;
    rgPrvPstd.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgPrvPsta);
    rgPrvPsta.Value = malloc(16);
    rgPrvPsta.AllocatedSize = 16;
    rgPrvPsta.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgPrvTifd);
    rgPrvTifd.Value = malloc(16);
    rgPrvTifd.AllocatedSize = 16;
    rgPrvTifd.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgPrvTifa);
    rgPrvTifa.Value = malloc(16);
    rgPrvTifa.AllocatedSize = 16;
    rgPrvTifa.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgPrvStod);
    rgPrvStod.Value = malloc(16);
    rgPrvStod.AllocatedSize = 16;
    rgPrvStod.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgPrvStoa);
    rgPrvStoa.Value = malloc(16);
    rgPrvStoa.AllocatedSize = 16;
    rgPrvStoa.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgPrvUrno);
    rgPrvUrno.Value = malloc(16);
    rgPrvUrno.AllocatedSize = 16;
    rgPrvUrno.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgPrvAurn);
    rgPrvAurn.Value = malloc(16);
    rgPrvAurn.AllocatedSize = 16;
    rgPrvAurn.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgPrvRkey);
    rgPrvRkey.Value = malloc(16);
    rgPrvRkey.AllocatedSize = 16;
    rgPrvRkey.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgPrvRegn);
    rgPrvRegn.Value = malloc(16);
    rgPrvRegn.AllocatedSize = 16;
    rgPrvRegn.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgPrvAct3);
    rgPrvAct3.Value = malloc(16);
    rgPrvAct3.AllocatedSize = 16;
    rgPrvAct3.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgPrvRem1);
    rgPrvRem1.Value = malloc(512);
    rgPrvRem1.AllocatedSize = 512;
    rgPrvRem1.Value[0] = 0x00;

    /* ------------------------------ */
    CT_InitStringDescriptor(&rgCurData);
    rgCurData.Value = malloc(16);
    rgCurData.AllocatedSize = 16;
    rgCurData.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCurSort);
    rgCurSort.Value = malloc(16);
    rgCurSort.AllocatedSize = 16;
    rgCurSort.Value[0] = 0x00;

    dbg(TRACE,"STRING BUFFERS <%s> INITIALIZED",pcpCtTabName);
  }
  return ilRC;
} /* end of InitMyTab */

/* *************************************************** */
/* *************************************************** */
static int LoadTowRules(void)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  char pclTist[16] = "";
  char pclTblName[16] = "";
  char pclFldList[128] = "";
  char pclSqlCond[256] = "";
  char pclSqlData[1024] = "";
  char pclSqlBuff[1024] = "";
  char pclGrpBuff[4096] = "";
  char pclActType[16] = "";
  char pclNewPrio[16] = "";
  int ilFldCnt = 0;
  int ilLen = 0;
  int ilFilled = 0;
  int ilRulePrio = 0;
  long llMaxGhpLine = 0;
  long llCurGhpLine = 0;
  long llMaxGhpCol = 0;
  long llCurGhpCol = 0;
  short slCursor = 0;
  short slFkt = 0;

  dbg(TRACE,"INIT TOWING RULES DATA GRID");
  dbg(TRACE,"---------------------------");
  CT_ResetContent(pcgCtGhp);
  GetServerTimeStamp("UTC",1,0,pclTist);

  strcpy(pclTblName,pcgCtGhp);
  strcpy(pclFldList,pcgCtGhpFldLst);
  sprintf(pclSqlCond,"APPL='TOW' AND (VAFR<='%s' AND (VATO=' ' OR VATO>='%s')) ORDER BY PRIO DESC,PRFL ASC",pclTist,pclTist);
  sprintf(pclSqlBuff,"SELECT %s FROM %s WHERE %s",pclFldList,pclTblName,pclSqlCond);
  dbg(TRACE,"<%s>",pclSqlBuff);

  ilFldCnt = field_count(pclFldList);
  slCursor = 0;
  slFkt = START;
  ilGetRc = RC_SUCCESS;
  while (ilGetRc == DB_SUCCESS)
  {
    ilGetRc = sql_if(slFkt,&slCursor,pclSqlBuff,pclSqlData);
	if (ilGetRc == DB_SUCCESS)
    {
      BuildItemBuffer(pclSqlData,"",ilFldCnt,",");
      dbg(DEBUG,"<%s>",pclSqlData);
      CT_InsertTextLine(pcgCtGhp,pclSqlData);
    }
    slFkt = NEXT;
  }
  close_my_cursor(&slCursor);

  dbg(TRACE,"CREATE PRIORITY AND AIRCRAFT TYPE GROUPS");
  dbg(TRACE,"----------------------------------------");
  llMaxGhpCol = ilFldCnt - 1;
  llMaxGhpLine = CT_GetLineCount(pcgCtGhp) - 1;
  for (llCurGhpLine=0;llCurGhpLine<=llMaxGhpLine;llCurGhpLine++)
  {
    CT_GetLineValues(pcgCtGhp, llCurGhpLine, &rgCtLine);
    dbg(DEBUG,"LINE %d: DATA <%s>", llCurGhpLine, rgCtLine.Value);

    CT_GetColumnValueByName(pcgCtGhp, llCurGhpLine, "PRIO", &rgCurPrio);
    str_trm_rgt(rgCurPrio.Value, " ", TRUE);
    ilRulePrio = atoi(rgCurPrio.Value);
    ilFilled = 0;
    for (llCurGhpCol=0;llCurGhpCol<=llMaxGhpCol;llCurGhpCol++)
    {
      CT_GetColumnValue(pcgCtGhp,llCurGhpLine,llCurGhpCol,&argCtValue[1]);
      str_trm_rgt(argCtValue[1].Value, " ", TRUE);
      if (strlen(argCtValue[1].Value) > 0)
      {
        ilFilled++;
      }
    }
    sprintf(pclNewPrio,"%02d%02d",ilRulePrio,ilFilled);
    CT_SetColumnValue(pcgCtGhp, llCurGhpLine, 2, pclNewPrio);

    CT_GetColumnValue(pcgCtGhp,llCurGhpLine,10,&argCtValue[1]);
    str_trm_rgt (argCtValue[1].Value, " ", TRUE);
    dbg(DEBUG,"GHPTAB.TTGD VALUE <%s>",argCtValue[1].Value);
    if (strlen(argCtValue[1].Value) > 0)
    {
      dbg(DEBUG,"INIT A/C TYPE GROUP WITH SGMTAB.USGR <%s>",argCtValue[1].Value);
      strcpy(pclTblName,"ACTTAB");
      strcpy(pclFldList,"ACT3,ACT5");
      sprintf(pclSqlCond,"URNO IN (SELECT UVAL FROM SGMTAB WHERE USGR='%s')",argCtValue[1].Value);
      sprintf(pclSqlBuff,"SELECT %s FROM %s WHERE %s",pclFldList,pclTblName,pclSqlCond);
      dbg(DEBUG,"<%s>",pclSqlBuff);
      strcpy(pclGrpBuff,";");

      ilFldCnt = field_count(pclFldList);
      slCursor = 0;
      slFkt = START;
      ilGetRc = RC_SUCCESS;
      while (ilGetRc == DB_SUCCESS)
      {
        ilGetRc = sql_if(slFkt,&slCursor,pclSqlBuff,pclSqlData);
	    if (ilGetRc == DB_SUCCESS)
        {
          BuildItemBuffer(pclSqlData,"",ilFldCnt,",");
          dbg(DEBUG,"<%s>",pclSqlData);
          ilLen = get_real_item(pclActType,pclSqlData,1);
          if (ilLen > 0)
          {
            strcat(pclGrpBuff,pclActType);
            strcat(pclGrpBuff,";");
          }
          ilLen = get_real_item(pclActType,pclSqlData,2);
          if (ilLen > 0)
          {
            strcat(pclGrpBuff,pclActType);
            strcat(pclGrpBuff,";");
          }
        }
        slFkt = NEXT;
      }
      close_my_cursor(&slCursor);
      dbg(DEBUG,"<%s>",pclGrpBuff);
      CT_SetColumnValue(pcgCtGhp, llCurGhpLine, 18, pclGrpBuff);
    }
  }

  dbg(TRACE,"SORT GRID LINES BY PRIO (DESC)");
  CT_SortByColumnNames(pcgCtGhp, "PRIO", SORT_DESC);



  dbg(TRACE,"------------------------------------");

  llMaxGhpLine = CT_GetLineCount(pcgCtGhp) - 1;
  for (llCurGhpLine=0;llCurGhpLine<=llMaxGhpLine;llCurGhpLine++)
  {
    CT_GetLineValues(pcgCtGhp, llCurGhpLine, &rgCtLine);
    dbg(TRACE,"LINE %d: \nDATA <%s>", llCurGhpLine, rgCtLine.Value);
  }
  dbg(TRACE,"------------------------------------");
  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static int LoadRemoteStands(void)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  int ilPstRc = DB_SUCCESS;
  int ilSgrRc = DB_SUCCESS;
  int ilSgmRc = DB_SUCCESS;
  char pclTist[16] = "";
  char pclTblName[16] = "";
  char pclFldList[128] = "";
  char pclSqlCond[256] = "";
  char pclSqlData[1024] = "";
  char pclSqlBuff[1024] = "";

  char pclSgrTblName[16] = "";
  char pclSgrFldList[128] = "";
  char pclSgrSqlCond[256] = "";
  char pclSgrSqlData[1024] = "";
  char pclSgrSqlBuff[1024] = "";

  char pclSgmTblName[16] = "";
  char pclSgmFldList[128] = "";
  char pclSgmSqlCond[256] = "";
  char pclSgmSqlData[1024] = "";
  char pclSgmSqlBuff[1024] = "";

  char pclPstTblName[16] = "";
  char pclPstFldList[128] = "";
  char pclPstSqlCond[256] = "";
  char pclPstSqlData[1024] = "";
  char pclPstSqlBuff[1024] = "";

  char pclPstBuff[4096] = "";
  char pclSrtBuff[4096] = "";
  char pclAloUrno[16] = "";
  char pclSgrUrno[16] = "";
  char pclPstUrno[16] = "";
  char pclPstSort[16] = "";
  char pclCurPnam[16] = "";
  char pclAnyData[16] = "";
  int ilFldCnt = 0;
  int ilPstFldCnt = 0;
  int ilSgmFldCnt = 0;
  int ilLen = 0;
  int ilCnt = 0;
  int ilFilled = 0;
  int ilRulePrio = 0;
  int ilSort = 0;
  long llMaxPstLine = 0;
  long llCurPstLine = 0;
  long llMaxPstCol = 0;
  long llCurPstCol = 0;
  short slCursor = 0;
  short slFkt = 0;
  short slSgrCursor = 0;
  short slSgrFkt = 0;
  short slSgmCursor = 0;
  short slSgmFkt = 0;
  short slPstCursor = 0;
  short slPstFkt = 0;

  dbg(TRACE,"INIT PREFERRED REMOTE STANDS DATA GRID");
  dbg(TRACE,"--------------------------------------");
  CT_ResetContent(pcgCtPst);
  GetServerTimeStamp("UTC",1,0,pclTist);

  strcpy(pclTblName,pcgCtPst);
  strcpy(pclFldList,pcgCtPstFldLst);

  strcpy(pclSqlCond,"BRGS='X'");
  sprintf(pclSqlBuff,"SELECT %s FROM %s WHERE %s ORDER BY PNAM",pclFldList,pclTblName,pclSqlCond);
  dbg(TRACE,"<%s>",pclSqlBuff);

  ilFldCnt = field_count(pclFldList);
  slCursor = 0;
  slFkt = START;
  ilGetRc = RC_SUCCESS;
  while (ilGetRc == DB_SUCCESS)
  {
    ilGetRc = sql_if(slFkt,&slCursor,pclSqlBuff,pclSqlData);
	if (ilGetRc == DB_SUCCESS)
    {
      BuildItemBuffer(pclSqlData,"",ilFldCnt,",");
      dbg(DEBUG,"<%s>",pclSqlData);
      CT_InsertTextLine(pcgCtPst,pclSqlData);
    }
    slFkt = NEXT;
  }
  close_my_cursor(&slCursor);

  dbg(TRACE,"GET ALLOCATION URNO OF POS-POS RELATIONS");
  dbg(TRACE,"----------------------------------------");

  strcpy(pclTblName,"ALOTAB");
  strcpy(pclFldList,"URNO");
  sprintf(pclSqlCond,"ALOC='POSPOS'");
  sprintf(pclSqlBuff,"SELECT %s FROM %s WHERE %s",pclFldList,pclTblName,pclSqlCond);
  dbg(TRACE,"<%s>",pclSqlBuff);

  slCursor = 0;
  slFkt = START;
  ilGetRc = sql_if(slFkt,&slCursor,pclSqlBuff,pclSqlData);
  close_my_cursor(&slCursor);
  if (ilGetRc == DB_SUCCESS)
  {
    strcpy(pclAloUrno,pclSqlData);
    str_trm_rgt(pclAloUrno, " ", TRUE);
    dbg(TRACE,"URNO OF MATCHING ALOTAB RECORD <%s>",pclAloUrno);

    strcpy(pclSgrTblName,"SGRTAB");
    strcpy(pclSgrFldList,"URNO");
    strcpy(pclSgrSqlCond,"UGTY=:VUGTY AND STYP=:VSTYP");    /* pclAloUrno,rgCurUrno.Value);*/
    sprintf(pclSgrSqlBuff,"SELECT %s FROM %s WHERE %s",pclSgrFldList,pclSgrTblName,pclSgrSqlCond);
    dbg(TRACE,"<%s>",pclSgrSqlBuff);

    strcpy(pclSgmTblName,"SGMTAB");
    strcpy(pclSgmFldList,"UVAL,SORT");
    strcpy(pclSgmSqlCond,"USGR=:VUSGR");    /*pclSgrUrno);*/
    sprintf(pclSgmSqlBuff,"SELECT %s FROM %s WHERE %s",pclSgmFldList,pclSgmTblName,pclSgmSqlCond);
    dbg(TRACE,"<%s>",pclSgmSqlBuff);

    strcpy(pclPstTblName,"PSTTAB");
    strcpy(pclPstFldList,"PNAM");
    strcpy(pclPstSqlCond,"URNO=:VURNO");    /*pclPstUrno);*/
    sprintf(pclPstSqlBuff,"SELECT %s FROM %s WHERE %s",pclPstFldList,pclPstTblName,pclPstSqlCond);
    dbg(TRACE,"<%s>",pclPstSqlBuff);

    ilSgmFldCnt= field_count(pclSgmFldList);
    ilPstFldCnt= field_count(pclPstFldList);

    slSgrCursor = 0;
    slSgmCursor = 0;
    slPstCursor = 0;

    dbg(TRACE,"-----------------------------------------------------");
    dbg(TRACE,"CREATE LIST OF PREFERRED REMOTE STANDS AND PRIORITIES");
    dbg(TRACE,"-----------------------------------------------------");
    llMaxPstCol = ilFldCnt - 1;
    llMaxPstLine = CT_GetLineCount(pcgCtPst) - 1;
    for (llCurPstLine=0;llCurPstLine<=llMaxPstLine;llCurPstLine++)
    {
      CT_GetLineValues(pcgCtPst, llCurPstLine, &rgCtLine);
      CT_GetColumnValueByName(pcgCtPst, llCurPstLine, "PNAM", &rgCurPsta);
      CT_GetColumnValueByName(pcgCtPst, llCurPstLine, "URNO", &rgCurUrno);
      str_trm_rgt(rgCurPsta.Value, " ", TRUE);
      str_trm_rgt(rgCurUrno.Value, " ", TRUE);
      dbg(DEBUG,"LINE %d: DATA <%s> URNO <%s>", llCurPstLine, rgCtLine.Value,rgCurUrno.Value);
      dbg(DEBUG,"==>>GET REMOTE STANDS FOR <%s> (URNO=%s)",rgCurPsta.Value,rgCurUrno.Value);

      slSgrFkt = START;
      sprintf(pclSgrSqlData,"%s,%s",pclAloUrno,rgCurUrno.Value);
      delton(pclSgrSqlData);
      ilSgrRc = sql_if(slSgrFkt,&slSgrCursor,pclSgrSqlBuff,pclSgrSqlData);
	  if (ilSgrRc == DB_SUCCESS)
      {
        strcpy(pclSgrUrno,pclSgrSqlData);
        str_trm_rgt(pclSgrUrno, " ", TRUE);
        dbg(DEBUG,"URNO OF MATCHING SGRTAB RECORD <%s>",pclSgrUrno);

        strcpy(pclPstBuff,"'");
        strcpy(pclSrtBuff,"");

        ilCnt = 0;
        slSgmFkt = START;
        strcpy(pclSgmSqlData,pclSgrUrno);
        ilSgmRc = RC_SUCCESS;
        while (ilSgmRc == DB_SUCCESS)
        {
          ilSgmRc = sql_if(slSgmFkt,&slSgmCursor,pclSgmSqlBuff,pclSgmSqlData);
	      if (ilSgmRc == DB_SUCCESS)
          {
            BuildItemBuffer(pclSgmSqlData,"",ilSgmFldCnt,",");
            dbg(DEBUG,"<%s>",pclSgmSqlData);
            ilLen = get_real_item(pclPstUrno,pclSgmSqlData,1);
            ilLen = get_real_item(pclPstSort,pclSgmSqlData,2);
            slPstFkt = START;
            strcpy(pclPstSqlData,pclPstUrno);
            ilPstRc = sql_if(slPstFkt,&slPstCursor,pclPstSqlBuff,pclPstSqlData);
            if (ilPstRc == DB_SUCCESS)
            {
              BuildItemBuffer(pclPstSqlData,"",ilPstFldCnt,",");
              dbg(DEBUG,"<%s>",pclPstSqlData);
              ilLen = get_real_item(pclCurPnam,pclPstSqlData,1);
              if (ilLen > 0)
              {
                strcat(pclPstBuff,pclCurPnam);
                strcat(pclPstBuff,"';'");
                ilSort = atoi(pclPstSort);
                sprintf(pclAnyData,"%0.3d",ilSort);
                strcat(pclSrtBuff,pclAnyData);
                strcat(pclSrtBuff,";");
                ilCnt++;
              }
            }
            else
            {
              dbg(TRACE,"WARNING: BASICDATA PSTTAB: STAND WITH URNO <%s> DOES NOT EXIST",pclPstUrno);
            }
          }
          slSgmFkt = NEXT;
        }
        if (ilCnt>0)
        {
          ilLen = strlen(pclPstBuff) - 2;
          pclPstBuff[ilLen] = 0x00;
          dbg(DEBUG,"<%s>",pclPstBuff);
          ilLen = strlen(pclSrtBuff) - 1;
          pclSrtBuff[ilLen] = 0x00;
          dbg(DEBUG,"<%s>",pclSrtBuff);
        }
        else
        {
          dbg(DEBUG,"NO REMOTE STAND GROUP DEFINED FOR <%s>",rgCurPsta.Value);
          strcpy(pclPstBuff,"");
          strcpy(pclSrtBuff,"");
        }
        CT_SetColumnValueByName(pcgCtPst, llCurPstLine, "RESN", pclPstBuff);
        CT_SetColumnValueByName(pcgCtPst, llCurPstLine, "PRFL", pclSrtBuff);
      }
      else
      {
        dbg(DEBUG,"NO REMOTE STAND RELATION DEFINED FOR <%s>",rgCurPsta.Value);
        strcpy(pclPstBuff,"");
        strcpy(pclSrtBuff,"");
        CT_SetColumnValueByName(pcgCtPst, llCurPstLine, "RESN", pclPstBuff);
        CT_SetColumnValueByName(pcgCtPst, llCurPstLine, "PRFL", pclSrtBuff);
      }
    }

    close_my_cursor(&slPstCursor);
    close_my_cursor(&slSgmCursor);
    close_my_cursor(&slSgrCursor);

    /*
    dbg(TRACE,"SORT GRID LINES BY PRIO (DESC)");
    CT_SortByColumnNames(pcgCtGhp, "PRIO", SORT_DESC);
    */


    dbg(DEBUG,"-----------------------------------------------------");

    llMaxPstLine = CT_GetLineCount(pcgCtPst) - 1;
    for (llCurPstLine=0;llCurPstLine<=llMaxPstLine;llCurPstLine++)
    {
      CT_GetColumnValueByName(pcgCtPst, llCurPstLine, "PNAM", &argCtValue[1]);
      str_trm_rgt(argCtValue[1].Value, " ", TRUE);
      CT_SetColumnValueByName(pcgCtPst, llCurPstLine, "PNAM", argCtValue[1].Value);
      CT_GetColumnValueByName(pcgCtPst, llCurPstLine, "TERM", &argCtValue[2]);
      str_trm_rgt(argCtValue[2].Value, " ", TRUE);
      CT_SetColumnValueByName(pcgCtPst, llCurPstLine, "TERM", argCtValue[2].Value);
      /*
      CT_GetLineValues(pcgCtPst, llCurPstLine, &rgCtLine);
      dbg(TRACE,"<%s>", rgCtLine.Value);
      */
      CT_GetColumnValueByName(pcgCtPst, llCurPstLine, "RESN", &argCtValue[2]);
      CT_GetColumnValueByName(pcgCtPst, llCurPstLine, "PRFL", &argCtValue[3]);
      if (strlen(argCtValue[2].Value) > 0)
      {
        dbg(DEBUG,"----------");
        dbg(DEBUG,"PSTA: <%s>",argCtValue[1].Value);
        dbg(DEBUG,"LIST: <%s>",argCtValue[2].Value);
        dbg(DEBUG,"PRIO: <%s>",argCtValue[3].Value);
      }
    }
  }
  else
  {
    dbg(TRACE,"THERE IS NOTHING DEFINED IN ALOTAB");
  }
  dbg(TRACE,"-----------------------------------------------------");
  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static int LoadFlightRotation(char *pcpAftRkey)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  char pclAftRkey[16] = "";
  char pclTblName[16] = "";
  char pclFldList[256] = "";
  char pclSqlCond[256] = "";
  char pclSqlData[1024] = "";
  char pclSqlBuff[1024] = "";
  long llMaxAftLine = 0;
  long llCurAftLine = 0;
  long llLineNo = 0;
  int ilFldCnt = 0;
  short slCursor = 0;
  short slFkt = 0;

  dbg(TRACE,"LOADING FLIGHT ROTATION RECORDS WITH TOWINGS");
  dbg(TRACE,"--------------------------------------------");
  CT_ResetContent(pcgCtAft);

  strcpy(pclTblName,pcgCtAft);
  strcpy(pclFldList,pcgCtAftFldLst);
  strcpy(pclAftRkey,pcpAftRkey);
  sprintf(pclSqlCond,"RKEY=%s ORDER BY ADID,TIFD",pclAftRkey);
  sprintf(pclSqlBuff,"SELECT %s FROM %s WHERE %s",pclFldList,pclTblName,pclSqlCond);
  dbg(TRACE,"<%s>",pclSqlBuff);

  ilFldCnt = field_count(pclFldList);
  slCursor = 0;
  slFkt = START;
  ilGetRc = RC_SUCCESS;
  while (ilGetRc == DB_SUCCESS)
  {
    ilGetRc = sql_if(slFkt,&slCursor,pclSqlBuff,pclSqlData);
	if (ilGetRc == DB_SUCCESS)
    {
      BuildItemBuffer(pclSqlData,"",ilFldCnt,",");
      dbg(DEBUG,"<%s>",pclSqlData);
      CT_InsertTextLine(pcgCtAft,pclSqlData);
      llLineNo = CT_GetLineCount(pcgCtAft) - 1;
      CT_SetAnyData(pcgCtAft, llLineNo, pclSqlData);
    }
    slFkt = NEXT;
  }
  close_my_cursor(&slCursor);

  dbg(TRACE,"-------------------------------------------");
  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static int CheckFlightRotation(void)
{
  int ilRc = RC_SUCCESS;
  int ilCase = 0;
  int ilIsHidden = FALSE;
  int ilSwapRotation = FALSE;
  long llMaxLine = 0;
  long llCurLine = 0;
  int ilDummy = 0;
  int i = 0;
  char pclCorrAurn[32] = "";
  dbg(TRACE,"ANALYSING FLIGHT ROTATION RECORDS WITH TOWINGS");
  dbg(TRACE,"----------------------------------------------");
  PrintReports("");

  ilSwapRotation = FALSE;
  ResetFlightArray();
  ResetTowingArray(-1);
  igMustRepairAurn = FALSE;
  igRepairAurnOnly = FALSE;

  lgFirstOwnTowOutLine = -1;
  lgLastOwnTowInLine = -1;
  lgFirstUserTowOutLine = -1;
  lgLastUserTowInLine = -1;
  rgFltDat[1].MaxTowCount = 0;
  rgFltDat[1].UsrTowCount = 0;

  strcpy(pcgTowOldRem2,"");
  strcpy(pcgTowNewUTWO,"");
  strcpy(pcgTowOldUTWO,"");
  strcpy(pcgTowNewUTWI,"");
  strcpy(pcgTowOldUTWI,"");

  dbg(TRACE,"----------------------------------------------");
  dbg(TRACE,"FIELDS:\n%s",pcgCtAftFldLst);
  dbg(TRACE,"----------------------------------------------");

  igCheckUserTowArr = TRUE;
  igCheckUserTowDep = FALSE;

  llMaxLine = CT_GetLineCount(pcgCtAft) - 1;
  for (llCurLine=0;llCurLine<=llMaxLine;llCurLine++)
  {
    dbg(DEBUG,"------------");
    CT_GetLineValues(pcgCtAft, llCurLine, &rgCtLine);
    dbg(TRACE,"LINE %d DATA\n<%s>", llCurLine,rgCtLine.Value);
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "ADID", &rgCurAdid);
    dbg(DEBUG,"ADID: <%s>",rgCurAdid.Value);
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "PRFL", &rgCurPrfl);
    dbg(DEBUG,"PRFL: <%s>",rgCurPrfl.Value);
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "FTYP", &rgCurFtyp);
    dbg(DEBUG,"FTYP: <%s>",rgCurFtyp.Value);
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "URNO", &rgCurUrno);
    str_trm_rgt(rgCurUrno.Value, " ", TRUE);
    dbg(DEBUG,"URNO: <%s>",rgCurUrno.Value);
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "AURN", &rgCurAurn);
    str_trm_rgt(rgCurAurn.Value, " ", TRUE);
    dbg(DEBUG,"AURN: <%s>",rgCurAurn.Value);
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "RKEY", &rgCurRkey);
    str_trm_rgt(rgCurRkey.Value, " ", TRUE);
    dbg(DEBUG,"RKEY: <%s>",rgCurRkey.Value);
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "REGN", &rgCurRegn);
    str_trm_rgt(rgCurRegn.Value, " ", TRUE);
    dbg(DEBUG,"REGN: <%s>",rgCurRegn.Value);
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "ACT3", &rgCurAct3);
    str_trm_rgt(rgCurAct3.Value, " ", TRUE);
    dbg(DEBUG,"ACT3: <%s>",rgCurAct3.Value);
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "PSTD", &rgCurPstd);
    str_trm_rgt(rgCurPstd.Value, " ", TRUE);
    dbg(DEBUG,"PSTD: <%s>",rgCurPstd.Value);
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "PSTA", &rgCurPsta);
    str_trm_rgt(rgCurPsta.Value, " ", TRUE);
    dbg(DEBUG,"PSTA: <%s>",rgCurPsta.Value);
    dbg(TRACE,"===>> LINE %d: FTYP=%s ADID=%s PSTD=%s PSTA=%s AURN=%s URNO=%s",
               llCurLine,rgCurFtyp.Value,rgCurAdid.Value,rgCurPstd.Value,
                         rgCurPsta.Value,rgCurAurn.Value,rgCurUrno.Value);

    CT_GetColumnValueByName(pcgCtAft, llCurLine, "REM1", &rgCurRem1);
    str_trm_rgt(rgCurRem1.Value, " ", TRUE);
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "REM2", &rgCurRem2);
    str_trm_rgt(rgCurRem2.Value, " ", TRUE);
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "USEC", &rgCurUsec);
    str_trm_rgt(rgCurUsec.Value, " ", TRUE);


    if (llCurLine > 0)
    {
      if (strcmp(rgCurRegn.Value,rgPrvRegn.Value) != 0)
      {
        dbg(TRACE,"ERROR: (REC CASE 1) INCONSISTENT REGN VALUES <%s> <%s>",rgCurRegn.Value,rgPrvRegn.Value);
        ilCase = 1;
        rgFltDat[1].MaxErrCount++;
      }
      if (strcmp(rgCurAct3.Value,rgPrvAct3.Value) != 0)
      {
        dbg(TRACE,"ERROR: (REC CASE 2) INCONSISTENT ACT3 VALUES <%s> <%s>",rgCurAct3.Value,rgPrvAct3.Value);
        ilCase = 2;
        rgFltDat[1].MaxErrCount++;
      }
      if (strcmp(rgCurRkey.Value,rgPrvRkey.Value) != 0)
      {
        /* In fact this cannot be because we are reading by RKEY :-) */
        dbg(TRACE,"ERROR: (REC CASE 3) INCONSISTENT RKEY VALUES <%s> <%s>",rgCurRkey.Value,rgPrvRkey.Value);
        ilCase = 3;
        rgFltDat[1].MaxErrCount++;
      }
      if (strcmp(rgCurAurn.Value,rgPrvAurn.Value) != 0)
      {
        dbg(TRACE,"ERROR: (REC CASE 4) INCONSISTENT AURN VALUES CUR.AURN <%s> PRV.AURN <%s>",rgCurAurn.Value,rgPrvAurn.Value);
        if (igAutoRepairAurn == FALSE)
        {
          ilCase = 4;
          rgFltDat[1].MaxErrCount++;
        }
        else
        {
          dbg(TRACE,">>>>> AUTO_REPAIR_AURN IS ACTIVATED");
          igMustRepairAurn = TRUE;
          sprintf(pclCorrAurn,"COR|%s|%s|",rgCurAurn.Value,rgFltDat[1].StrFltUrno);
          CT_SetColumnValueByName(pcgCtAft, llCurLine, "AURN", pclCorrAurn);
        }
      }
      if (rgFltDat[1].FltLineNo >= 0)
      {
        if (strcmp(rgCurAurn.Value,rgFltDat[1].StrFltUrno) != 0)
        {
          dbg(TRACE,"ERROR: (REC CASE 5) AURN DOES NOT MATCH ARRIVAL FLIGHT CUR.AURN <%s> ARR.URNO <%s>",rgCurAurn.Value,rgFltDat[1].StrFltUrno);
          if (igAutoRepairAurn == FALSE)
          {
            ilCase = 5;
            rgFltDat[1].MaxErrCount++;
        }
          else
          {
            dbg(TRACE,">>>>> AUTO_REPAIR_AURN IS ACTIVATED");
            igMustRepairAurn = TRUE;
            sprintf(pclCorrAurn,"COR|%s|%s|",rgCurAurn.Value,rgFltDat[1].StrFltUrno);
            CT_SetColumnValueByName(pcgCtAft, llCurLine, "AURN", pclCorrAurn);
          }
        }
      }
    }
    else if (llCurLine == 0)
    {
      switch(rgCurAdid.Value[0])
      {
        case 'A':
          dbg(DEBUG,"THE FIRST RECORD (LINE %d) IS AN ARRIVAL FLIGHT",llCurLine);
          break;
        case 'B':
          if (strstr("TG",rgCurFtyp.Value) != NULL)
          {
            dbg(TRACE,"THE FIRST RECORD (LINE %d) IS A TOWING OR GROUND MOVEMENT",llCurLine);
            ilCase = 6;
            rgFltDat[1].MaxErrCount++;
            dbg(TRACE,"(FLT CASE 6) RECORD SET NOT STARTING WITH ARRIVAL FLIGHT");
          }
          break;
        default:
        break;
      }
    }

    switch(rgCurAdid.Value[0])
    {
      case 'A':
        dbg(TRACE,"THIS IS AN ARRIVAL FLIGHT");
        if (llCurLine == 0)
        {
          dbg(TRACE,"RECORD SET STARTING WITH ARRIVAL FLIGHT");
        }
        else
        {
          ilCase = 6;
          rgFltDat[1].MaxErrCount++;
          dbg(TRACE,"(FLT CASE 6) RECORD SET NOT STARTING WITH ARRIVAL FLIGHT");
        }
        if (rgFltDat[1].FltLineNo < 0)
        {
          rgFltDat[1].FltExists = TRUE;
          rgFltDat[1].FltLineNo = llCurLine;
          if (strstr("XND",rgCurFtyp.Value) != NULL)
          {
            dbg(TRACE,"(FLT CASE 13) THIS IS A NON-OPERATIONAL FLIGHT (FTYP=<%s>)",rgCurFtyp.Value);
            rgFltDat[1].FltIsNoop = TRUE;
            rgFltDat[1].MaxErrCount++;
            ilCase = 13;
          }
          else
          {
            rgFltDat[1].FltIsNoop = FALSE;
          }
          strcpy(pcgTowRacRegn,rgCurRegn.Value);
          CT_GetColumnValueByName(pcgCtAft, llCurLine, "URNO", &argCtValue[1]);
          str_trm_rgt(argCtValue[1].Value, " ", TRUE);
          strcpy(rgFltDat[1].StrFltUrno,argCtValue[1].Value);
          dbg(DEBUG,"ARR URNO: <%s>",rgFltDat[1].StrFltUrno);
          strcpy(rgCurAurn.Value,rgFltDat[1].StrFltUrno);
          dbg(DEBUG,"SET AURN: <%s>",rgCurAurn.Value);
          if (strlen(rgCurPsta.Value) == 0)
          {
            dbg(TRACE,"EMPTY ARRIVAL STAND PATCHED AS <#PA#>");
            strcpy(rgCurPsta.Value,"#PA#");
          }
          strcpy(rgFltDat[1].StrFltStnd,rgCurPsta.Value);
          dbg(DEBUG,"ARR PSTA: <%s>",rgFltDat[1].StrFltStnd);
          CT_GetColumnValueByName(pcgCtAft, llCurLine, "STOA", &argCtValue[1]);
          str_trm_rgt(argCtValue[1].Value, " ", TRUE);
          strcpy(rgFltDat[1].StrFltSked,argCtValue[1].Value);
          (void) GetFullDay(rgFltDat[1].StrFltSked, &ilDummy, &rgFltDat[1].ValFltSked, &ilDummy);
          dbg(DEBUG,"ARR STOA: <%s> (MINUTES=%d)",rgFltDat[1].StrFltSked,rgFltDat[1].ValFltSked);
          CT_GetColumnValueByName(pcgCtAft, llCurLine, "TIFA", &argCtValue[1]);
          str_trm_rgt(argCtValue[1].Value, " ", TRUE);
          strcpy(rgFltDat[1].StrFltTime,argCtValue[1].Value);
          (void) GetFullDay(rgFltDat[1].StrFltTime, &ilDummy, &rgFltDat[1].ValFltTime, &ilDummy);
          dbg(DEBUG,"ARR TIFA: <%s> (MINUTES=%d)",rgFltDat[1].StrFltTime,rgFltDat[1].ValFltTime);
        }
        else
        {
          ilCase = 7;
          rgFltDat[1].MaxErrCount++;
          dbg(TRACE,"ERROR: (FLT CASE 7) FOUND MORE THAN ONE ARRIVAL FLIGHT");
        }
        break;
      case 'B':
        dbg(TRACE,"CHECK LOCAL FLIGHTS OR TOWINGS (ADID IS B)");
        if (strstr("TG",rgCurFtyp.Value) != NULL)
        {
          dbg(TRACE,"THIS IS A TOWING OR GROUND MOVEMENT");
          dbg(TRACE,"TOWING REMARK REM1 <%s>",rgCurRem1.Value);
          if (strlen(pcgTowOldRem2) == 0)
          {
            strcpy(pcgTowOldRem2,rgCurRem2.Value);
            dbg(TRACE,"TOWING REMARK REM2 <%s>",rgCurRem2.Value);
          }
          if (strcmp(rgPrvPsta.Value,rgCurPstd.Value) != 0)
          {
            dbg(TRACE,"WARNING! NOT MATCHING PARKING STANDS: LAST ARR AT <%s> CURR DEP FROM <%s>",rgPrvPsta.Value,rgCurPstd.Value);
          }
          rgFltDat[1].MaxTowCount++;
          CT_GetColumnValueByName(pcgCtAft, llCurLine, "STAT", &argCtValue[1]);
          str_trm_rgt(argCtValue[1].Value, " ", TRUE);
          if (strstr(argCtValue[1].Value,"DEL") != NULL)
          {
            dbg(TRACE,"THIS IS A HIDDEN (DELETED) TOWING");
            rgFltDat[1].DelTowCount++;
            ilIsHidden = TRUE;
          }
          else
          {
            rgFltDat[1].ActTowCount++;
            ilIsHidden = FALSE;
          }
          if (strcmp(rgFltDat[1].StrFltStnd,rgCurPstd.Value) != 0)
          {
            if (igCheckUserTowArr == TRUE)
            {
              dbg(TRACE,"TOWING RECORD FROM ANOTHER POSITION");
              dbg(TRACE,"WON'T LOOK FOR USER TOW-OUT ANYMORE");
              igCheckUserTowArr = FALSE;
            }
          }
          if (strstr(rgCurUsec.Value,"AutoTow") == NULL)
          {
            dbg(TRACE,"THIS TOWING RECORD HAS BEEN CREATED BY A USER");
            rgFltDat[1].UsrTowCount++;
            if (strcmp(rgFltDat[1].StrFltStnd,rgCurPstd.Value) == 0)
            {
              if (igCheckUserTowArr == TRUE)
              {
                dbg(TRACE,"THIS IS A USER CREATED TOW-OUT RECORD");
                if (lgFirstUserTowOutLine < 0)
                {
                  lgFirstUserTowOutLine = llCurLine;
                }
                else
                {
                  dbg(TRACE,"WARNING: FOUND MORE THAN ONE USER_TOW_OUT");
                }
              }
            }
          }
          if (strstr(rgCurUsec.Value,"AutoTowUsr") != NULL)
          {
            dbg(TRACE,"THIS AUTOMATED TOWING RECORD IS OWNED BY A USER");
            rgFltDat[1].UsrTowCount++;
            if (strcmp(rgFltDat[1].StrFltStnd,rgCurPstd.Value) == 0)
            {
              if (igCheckUserTowArr == TRUE)
              {
                dbg(TRACE,"THIS IS A USER OWNED TOW-OUT RECORD");
                if (lgFirstUserTowOutLine < 0)
                {
                  lgFirstUserTowOutLine = llCurLine;
                }
                else
                {
                  dbg(TRACE,"WARNING: FOUND MORE THAN ONE USER_TOW_OUT");
                }
              }
            }
          }
          if (strstr(rgCurUsec.Value,"AutoTowArr") != NULL)
          {
            dbg(TRACE,"THIS IS MY OWN CREATED TOW-OUT RECORD");
            if (lgFirstOwnTowOutLine < 0)
            {
              lgFirstOwnTowOutLine = llCurLine;
            }
            else
            {
              dbg(TRACE,"WARNING: FOUND MORE THAN ONE AUTO_TOW_OUT");
            }
          }
          if (strstr(rgCurUsec.Value,"AutoTowDep") != NULL)
          {
            dbg(TRACE,"THIS IS MY OWN CREATED TOW-IN RECORD");
            if (lgLastOwnTowInLine < 0)
            {
              lgLastOwnTowInLine = llCurLine;
            }
            else
            {
              dbg(TRACE,"WARNING: FOUND MORE THAN ONE AUTO_TOW_DEP");
            }
          }
          if (lgFirstOwnTowOutLine == llCurLine)
          {
            rgTowDat[1].TowExists = TRUE;
            rgTowDat[1].TowLineNo = llCurLine;
            if (strlen(rgCurPstd.Value) == 0)
            {
              dbg(TRACE,"EMPTY TOW-OUT STAND PATCHED AS <#PA#>");
              strcpy(rgCurPstd.Value,"#PA#");
            }
            CT_GetColumnValueByName(pcgCtAft, llCurLine, "STOD", &argCtValue[1]);
            str_trm_rgt(argCtValue[1].Value, " ", TRUE);
            strcpy(rgTowDat[1].StrTowStod,argCtValue[1].Value);
            (void) GetFullDay(rgTowDat[1].StrTowStod, &ilDummy, &rgTowDat[1].ValTowStod, &ilDummy);
            CT_GetColumnValueByName(pcgCtAft, llCurLine, "STOA", &argCtValue[1]);
            str_trm_rgt(argCtValue[1].Value, " ", TRUE);
            strcpy(rgTowDat[1].StrTowStoa,argCtValue[1].Value);
            (void) GetFullDay(rgTowDat[1].StrTowStoa, &ilDummy, &rgTowDat[1].ValTowStoa, &ilDummy);
            rgTowDat[1].ValTowTime = rgTowDat[1].ValTowStoa - rgTowDat[1].ValTowStod;
            strcpy(rgTowDat[1].StrTowPstd,rgCurPstd.Value);
            strcpy(rgTowDat[1].StrTowPsta,rgCurPsta.Value);
            strcpy(rgTowDat[1].StrTowRem1,rgCurRem1.Value);
            strcpy(rgTowDat[1].StrTowUsec,rgCurUsec.Value);
            dbg(TRACE,"TOW PSTD/STOD <%s> <%s> PSTA/STOA <%s> <%s> (TOW TIME=%d)",rgTowDat[1].StrTowPstd,rgTowDat[1].StrTowStod,rgTowDat[1].StrTowPsta,rgTowDat[1].StrTowStoa,rgTowDat[1].ValTowTime);
            rgTowDat[1].TowIsHidden = ilIsHidden;
            if ((strstr(rgCurUsec.Value,"AutoTowArr") != NULL)||(strstr(rgCurUsec.Value,"AutoTowDep") != NULL))
            {
              dbg(TRACE,"THIS IS ONE OF MY OWN TOWING RECORDS");
              rgTowDat[1].MyOwnTowRec = TRUE;
            }
            rgTowDat[1].TowIsValid = TRUE;
          }
          else if (lgLastOwnTowInLine == llCurLine)
          {
            rgTowDat[2].TowExists = TRUE;
            rgTowDat[2].TowLineNo = llCurLine;
            CT_GetColumnValueByName(pcgCtAft, llCurLine, "STOD", &argCtValue[1]);
            str_trm_rgt(argCtValue[1].Value, " ", TRUE);
            strcpy(rgTowDat[2].StrTowStod,argCtValue[1].Value);
            (void) GetFullDay(rgTowDat[2].StrTowStod, &ilDummy, &rgTowDat[2].ValTowStod, &ilDummy);
            CT_GetColumnValueByName(pcgCtAft, llCurLine, "STOA", &argCtValue[1]);
            str_trm_rgt(argCtValue[1].Value, " ", TRUE);
            strcpy(rgTowDat[2].StrTowStoa,argCtValue[1].Value);
            (void) GetFullDay(rgTowDat[2].StrTowStoa, &ilDummy, &rgTowDat[2].ValTowStoa, &ilDummy);
            rgTowDat[2].ValTowTime = rgTowDat[2].ValTowStoa - rgTowDat[2].ValTowStod;
            strcpy(rgTowDat[2].StrTowPstd,rgCurPstd.Value);
            if (strlen(rgCurPsta.Value) == 0)
            {
              dbg(TRACE,"EMPTY TOW-IN STAND PATCHED AS <#PD#>");
              strcpy(rgCurPsta.Value,"#PD#");
            }
            strcpy(rgTowDat[2].StrTowPsta,rgCurPsta.Value);
            strcpy(rgTowDat[2].StrTowRem1,rgCurRem1.Value);
            strcpy(rgTowDat[2].StrTowUsec,rgCurUsec.Value);
            dbg(TRACE,"TOW PSTD/STOD <%s> <%s> PSTA/STOA <%s> <%s> (TOW TIME=%d)",rgTowDat[2].StrTowPstd,rgTowDat[2].StrTowStod,rgTowDat[2].StrTowPsta,rgTowDat[2].StrTowStoa,rgTowDat[2].ValTowTime);
            rgTowDat[2].TowIsHidden = ilIsHidden;
            if ((strstr(rgCurUsec.Value,"AutoTowArr") != NULL)||(strstr(rgCurUsec.Value,"AutoTowDep") != NULL))
            {
              dbg(TRACE,"THIS IS ONE OF MY OWN TOWING RECORDS");
              rgTowDat[2].MyOwnTowRec = TRUE;
            }
            else
            {
              rgTowDat[2].MyOwnTowRec = FALSE;
            }
            rgTowDat[2].TowIsValid = TRUE;
          }
          else if (lgFirstUserTowOutLine == llCurLine)
          {
          }
          else if (lgLastUserTowInLine == llCurLine)
          {
          }
          strcpy(rgFltDat[1].StrLastPos,rgCurPsta.Value);
          rgFltDat[1].LastTowLineNo = llCurLine;
          if (strcmp(rgCurPrfl.Value,"I") == 0)
          {
            dbg(TRACE,"(TOW CASE 20) WARNING! THIS TOWING IS PROTECTED BY FIPS!");
            ilCase = 20;
            rgFltDat[1].MaxErrCount++;
            rgFltDat[1].ProTowCount++;
          }
        }
        else if (strstr("BZ",rgCurFtyp.Value) != NULL)
        {
          dbg(TRACE,"(FLT CASE 11) THIS IS A RETURN FLIGHT OR RETURN TO RAMP");
          ilCase = 11;
          rgFltDat[1].MaxErrCount++;
          ilSwapRotation = TRUE;
        }
        else
        {
          dbg(TRACE,"(FLT CASE 12) SEEMS TO BE A PLAIN LOCAL FLIGHT");
          ilCase = 12;
          rgFltDat[1].MaxErrCount++;
          ilSwapRotation = TRUE;
        }
        if (ilSwapRotation == TRUE)
        {
          dbg(TRACE,"=========================================");
          dbg(TRACE,"RESETTING INTERNAL FLIGHT ROTATION MEMORY");
          ResetFlightArray();
          ResetTowingArray(-1);
          if (igMustRepairAurn == TRUE)
          {
            dbg(TRACE,"RESETTING AUTO_REPAIR_AURN FUNCTION MEMORY");
            AutoRepairAurn(FALSE);
            igMustRepairAurn = FALSE;
            igRepairAurnOnly = FALSE;
          }
          ilCase = 0;
          ilSwapRotation = FALSE;
          if (rgFltDat[1].FltLineNo < 0)
          {
            rgFltDat[1].FltExists = TRUE;
            rgFltDat[1].FltLineNo = llCurLine;
            if (strstr("XND",rgCurFtyp.Value) != NULL)
            {
              dbg(TRACE,"(FLT CASE 13) THIS IS A NON-OPERATIONAL FLIGHT (FTYP=<%s>)",rgCurFtyp.Value);
              rgFltDat[1].FltIsNoop = TRUE;
              rgFltDat[1].MaxErrCount++;
              ilCase = 13;
            }
            else
            {
              rgFltDat[1].FltIsNoop = FALSE;
            }
            strcpy(pcgTowRacRegn,rgCurRegn.Value);
            CT_GetColumnValueByName(pcgCtAft, llCurLine, "URNO", &argCtValue[1]);
            str_trm_rgt(argCtValue[1].Value, " ", TRUE);
            strcpy(rgFltDat[1].StrFltUrno,argCtValue[1].Value);
            dbg(TRACE,"ARR URNO: <%s>",rgFltDat[1].StrFltUrno);
            strcpy(rgCurAurn.Value,rgFltDat[1].StrFltUrno);
            dbg(TRACE,"SET AURN: <%s>",rgCurAurn.Value);
            strcpy(rgFltDat[1].StrFltStnd,rgCurPsta.Value);
            dbg(TRACE,"ARR PSTA: <%s>",rgFltDat[1].StrFltStnd);
            CT_GetColumnValueByName(pcgCtAft, llCurLine, "STOA", &argCtValue[1]);
            str_trm_rgt(argCtValue[1].Value, " ", TRUE);
            strcpy(rgFltDat[1].StrFltSked,argCtValue[1].Value);
            (void) GetFullDay(rgFltDat[1].StrFltSked, &ilDummy, &rgFltDat[1].ValFltSked, &ilDummy);
            dbg(TRACE,"ARR STOA: <%s> (MINUTES=%d)",rgFltDat[1].StrFltSked,rgFltDat[1].ValFltSked);
            CT_GetColumnValueByName(pcgCtAft, llCurLine, "TIFA", &argCtValue[1]);
            str_trm_rgt(argCtValue[1].Value, " ", TRUE);
            strcpy(rgFltDat[1].StrFltTime,argCtValue[1].Value);
            (void) GetFullDay(rgFltDat[1].StrFltTime, &ilDummy, &rgFltDat[1].ValFltTime, &ilDummy);
            dbg(TRACE,"ARR TIFA: <%s> (MINUTES=%d)",rgFltDat[1].StrFltTime,rgFltDat[1].ValFltTime);
            dbg(TRACE,"NOW PROCEEDING WITH NEW ARRIVAL FLIGHT");
            dbg(TRACE,"======================================");
          }
        }
        break;
      case 'D':
        dbg(TRACE,"THIS IS A DEPARTURE FLIGHT");
        if (llCurLine == llMaxLine)
        {
          dbg(TRACE,"RECORD SET ENDING WITH DEPARTURE FLIGHT");
          dbg(TRACE,"---------------------------------------");
        }
        else
        {
          ilCase = 8;
          rgFltDat[1].MaxErrCount++;
          dbg(TRACE,"(FLT CASE 8) RECORD SET NOT ENDING WITH DEPARTURE FLIGHT");
        }
        if (rgFltDat[2].FltLineNo < 0)
        {
          rgFltDat[2].FltExists = TRUE;
          rgFltDat[2].FltLineNo = llCurLine;
          if (strstr("XND",rgCurFtyp.Value) != NULL)
          {
            dbg(TRACE,"(FLT CASE 13) THIS IS A NON-OPERATIONAL FLIGHT (FTYP=<%s>)",rgCurFtyp.Value);
            rgFltDat[2].FltIsNoop = TRUE;
            ilCase = 13;
            rgFltDat[1].MaxErrCount++;
          }
          else
          {
            rgFltDat[2].FltIsNoop = FALSE;
          }
          CT_GetColumnValueByName(pcgCtAft, llCurLine, "URNO", &argCtValue[1]);
          str_trm_rgt(argCtValue[1].Value, " ", TRUE);
          strcpy(rgFltDat[2].StrFltUrno,argCtValue[1].Value);
          dbg(DEBUG,"DEP URNO: <%s>",rgFltDat[2].StrFltUrno);
          if (strlen(rgCurPstd.Value) == 0)
          {
            dbg(TRACE,"EMPTY DEPARTURE STAND PATCHED AS <#PD#>");
            strcpy(rgCurPstd.Value,"#PD#");
          }
          strcpy(rgFltDat[2].StrFltStnd,rgCurPstd.Value);
          dbg(DEBUG,"DEP PSTD: <%s>",rgFltDat[2].StrFltStnd);
          CT_GetColumnValueByName(pcgCtAft, llCurLine, "STOD", &argCtValue[1]);
          str_trm_rgt(argCtValue[1].Value, " ", TRUE);
          strcpy(rgFltDat[2].StrFltSked,argCtValue[1].Value);
          (void) GetFullDay(rgFltDat[2].StrFltSked, &ilDummy, &rgFltDat[2].ValFltSked, &ilDummy);
          dbg(DEBUG,"DEP STOD: <%s> (MINUTES=%d)",rgFltDat[2].StrFltSked,rgFltDat[2].ValFltSked);
          CT_GetColumnValueByName(pcgCtAft, llCurLine, "TIFD", &argCtValue[1]);
          str_trm_rgt(argCtValue[1].Value, " ", TRUE);
          strcpy(rgFltDat[2].StrFltTime,argCtValue[1].Value);
          (void) GetFullDay(rgFltDat[2].StrFltTime, &ilDummy, &rgFltDat[2].ValFltTime, &ilDummy);
          dbg(DEBUG,"DEP TIFD: <%s> (MINUTES=%d)",rgFltDat[2].StrFltTime,rgFltDat[2].ValFltTime);
        }
        else
        {
          ilCase = 9;
          rgFltDat[1].MaxErrCount++;
          dbg(TRACE,"(FLT CASE 9) ERROR: FOUND MORE THAN ONE DEPARTURE FLIGHT");
        }
        break;
      default:
        break;
    }
    strcpy(rgPrvRegn.Value,rgCurRegn.Value);
    strcpy(rgPrvAct3.Value,rgCurAct3.Value);
    strcpy(rgPrvRkey.Value,rgCurRkey.Value);
    strcpy(rgPrvAurn.Value,rgCurAurn.Value);
    strcpy(rgPrvPstd.Value,rgCurPstd.Value);
    strcpy(rgPrvPsta.Value,rgCurPsta.Value);
    strcpy(rgPrvUrno.Value,rgCurUrno.Value);
  } /* for loop */

  dbg(TRACE,"=======================================");

  igCheckUserTowDep = FALSE;
  if (rgFltDat[2].FltExists == TRUE)
  {
    if (rgFltDat[1].UsrTowCount > 0)
    {
      igCheckUserTowDep = TRUE;
    }
  }

  /* Find Last User Tow-In */
  lgLastUserTowInLine = -1;
  llMaxLine = CT_GetLineCount(pcgCtAft) - 1;
  for (llCurLine=llMaxLine;(llCurLine>0)&&(igCheckUserTowDep==TRUE);llCurLine--)
  {
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "FTYP", &rgCurFtyp);
    if (strstr("TG",rgCurFtyp.Value) != NULL)
    {
      CT_GetColumnValueByName(pcgCtAft, llCurLine, "USEC", &rgCurUsec);
      str_trm_rgt(rgCurUsec.Value, " ", TRUE);
      if ((strstr(rgCurUsec.Value,"AutoTowArr") == NULL)&&(strstr(rgCurUsec.Value,"AutoTowDep") == NULL))
      {
        CT_GetColumnValueByName(pcgCtAft, llCurLine, "PSTA", &rgCurPsta);
        str_trm_rgt(rgCurPsta.Value, " ", TRUE);
        if (strcmp(rgFltDat[2].StrFltStnd,rgCurPsta.Value) == 0)
        {
          lgLastUserTowInLine = llCurLine;
        }
        igCheckUserTowDep = FALSE;
      }
    }
  } /* for loop */

  FillTowingArray (1,lgFirstOwnTowOutLine);
  FillTowingArray (2,lgLastOwnTowInLine);
  CreateRem2DataList(1, FALSE, FALSE, -1, -1);
  CreateRem2DataList(2, FALSE, FALSE, -1, -1);
  PrintTowingArray(1);
  PrintTowingArray(2);

  if ((rgFltDat[1].FltLineNo >= 0) && (rgFltDat[2].FltLineNo >= 0))
  {
    if (strcmp(rgFltDat[1].StrFltStnd,rgFltDat[2].StrFltStnd) == 0)
    {
      if (rgFltDat[1].MaxTowCount > 0)
      {
        dbg(TRACE,"ARR/DEP ON SAME STAND WITH %d TOWINGS",rgFltDat[1].MaxTowCount);
      }
      else
      {
        dbg(TRACE,"ARR/DEP ON SAME STAND WITHOUT TOWINGS: CHECK TIME CONDITION FOR TOWING");
      }
    }
    else
    {
      dbg(TRACE,"ARR/DEP ON DIFFERENT STANDS WITH %d TOWINGS",rgFltDat[1].MaxTowCount);
    }
    if (llCurLine >= 0)
    {
      if (rgFltDat[1].MaxTowCount > 0)
      {
        if (rgTowDat[2].TowExists == FALSE)
        {
          if (strcmp(rgTowDat[1].StrTowPsta,rgFltDat[2].StrFltStnd) == 0)
          {
            dbg(TRACE,"MERGE TOWING MEMORY: TOW-OUT INTO TOW-IN");
            rgTowDat[2].TowExists = TRUE;
            rgTowDat[2].TowLineNo = rgTowDat[1].TowLineNo;
            strcpy(rgTowDat[2].StrTowStod,rgTowDat[1].StrTowStod);
            rgTowDat[2].ValTowStod = rgTowDat[1].ValTowStod;
            strcpy(rgTowDat[2].StrTowStoa,rgTowDat[1].StrTowStoa);
            rgTowDat[2].ValTowStoa = rgTowDat[1].ValTowStoa;
            rgTowDat[2].ValTowTime = rgTowDat[1].ValTowTime;
            strcpy(rgTowDat[2].StrTowPstd,rgTowDat[1].StrTowPstd);
            strcpy(rgTowDat[2].StrTowPsta,rgTowDat[1].StrTowPsta);
            rgTowDat[2].TowBgnOffs = rgTowDat[1].TowBgnOffs;
            rgTowDat[2].CurBgnOffs = rgTowDat[1].CurBgnOffs;
            dbg(TRACE,"TOW PSTD/STOD <%s> <%s> PSTA/STOA <%s> <%s> (TOW TIME=%d)",rgTowDat[2].StrTowPstd,rgTowDat[2].StrTowStod,rgTowDat[2].StrTowPsta,rgTowDat[2].StrTowStoa,rgTowDat[2].ValTowTime);
            rgTowDat[2].TowIsValid = TRUE;
            rgTowDat[2].TowIsHidden = rgTowDat[1].TowIsHidden;
            ResetTowingArray(1);
            strcpy(rgTowDat[1].StrTowPstd,rgTowDat[2].StrTowPstd);
          }
        }
      }
      else
      {
        dbg(TRACE,"ARR/DEP ON DIFFERENT STANDS WITHOUT TOWINGS: NEEDS AT LEAST ONE TOWING");
      }
      if (rgTowDat[1].TowIsValid == TRUE)
      {
        if (strcmp(rgTowDat[1].StrTowPstd,rgFltDat[1].StrFltStnd) == 0)
        {
          dbg(TRACE,"TOWING IDX=1 IS A VALID TOW-OUT");
          rgTowDat[1].TowIsTowOut = TRUE;
          llCurLine = rgTowDat[1].TowLineNo;
          CT_GetColumnValueByName(pcgCtAft, llCurLine, "OFBL", &argCtValue[1]);
          str_trm_rgt(argCtValue[1].Value, " ", TRUE);
          if (strlen(argCtValue[1].Value) > 0)
          {
            dbg(TRACE,"THE TOW-OUT IS ALREADY OFF BLOCKS");
            rgTowDat[1].TowOutIsOfbl = TRUE;
            if (igSkipPabaFlights == TRUE)
            {
              /*
              ilCase = -1;
              rgFltDat[1].MaxErrCount++;
              */
            }
            else
            {
              dbg(DEBUG,"[AUTO_TOWING] SKIP_PABA_FLIGHT CONFIGURED TO 'NO'");
            }
          }
        }
        else
        {
          dbg(TRACE,"TOWING IDX=1 IS NOT MATCHING THE ARRIVAL STAND");
        }
        if (strcmp(rgTowDat[1].StrTowPsta,rgFltDat[2].StrFltStnd) == 0)
        {
          dbg(TRACE,"TOWING IDX=1 IS A VALID TOW-IN TOO");
          rgTowDat[1].TowIsTowIn = TRUE;
          rgTowDat[1].TowInIsOfbl = rgTowDat[1].TowOutIsOfbl;
          rgTowDat[2].TowInIsOfbl = rgTowDat[1].TowOutIsOfbl;
        }
        if ((rgTowDat[1].TowIsTowOut == TRUE)&&(rgTowDat[1].TowIsTowIn == TRUE))
        {
          dbg(TRACE,"TOWING IDX=1 HANDLES BOTH TOW-OUT AND TOW-IN TOO");
          rgTowDat[1].TowOutAndIn = TRUE;
          rgTowDat[1].TowInIsOfbl = rgTowDat[1].TowOutIsOfbl;
          rgTowDat[2].TowInIsOfbl = rgTowDat[1].TowOutIsOfbl;
        }
      }
      if (rgTowDat[2].TowIsValid == TRUE)
      {
        dbg(TRACE,"TOWING IDX=2 IS A VALID TOWING");
        if (strcmp(rgTowDat[2].StrTowPsta,rgFltDat[2].StrFltStnd) == 0)
        {
          dbg(TRACE,"TOWING IDX=2 IS A VALID TOW-IN");
          rgTowDat[2].TowIsTowIn = TRUE;
          llCurLine = rgTowDat[2].TowLineNo;
          CT_GetColumnValueByName(pcgCtAft, llCurLine, "OFBL", &argCtValue[1]);
          str_trm_rgt(argCtValue[1].Value, " ", TRUE);
          if (strlen(argCtValue[1].Value) > 0)
          {
            dbg(TRACE,"THE TOW-IN IS ALREADY OFF BLOCKS");
            rgTowDat[2].TowInIsOfbl = TRUE;
            if (igSkipPabaFlights == TRUE)
            {
              /*
              rgFltDat[1].MaxErrCount++;
              ilCase = -2;
              */
            }
            else
            {
              dbg(DEBUG,"[AUTO_TOWING] SKIP_PABA_FLIGHT CONFIGURED TO 'NO'");
            }
          }
        }
        else
        {
          dbg(TRACE,"TOWING IDX=2 IS NOT MATCHING THE DEPARTURE STAND");
        }
        if ((strcmp(rgTowDat[2].StrTowPstd,rgFltDat[1].StrFltStnd) == 0)&&
            ((rgFltDat[1].MaxTowCount == 1)||(rgTowDat[1].TowIsHidden == TRUE)))
        {
          dbg(TRACE,"TOWING IDX=2 IS A VALID TOW-OUT TOO");
          rgTowDat[2].TowIsTowOut = TRUE;
          rgTowDat[2].TowOutIsOfbl = rgTowDat[2].TowInIsOfbl;
          rgTowDat[1].TowOutIsOfbl = rgTowDat[2].TowInIsOfbl;
        }
        if ((rgTowDat[2].TowIsTowOut == TRUE)&&(rgTowDat[2].TowIsTowIn == TRUE))
        {
          dbg(TRACE,"TOWING IDX=2 HANDLES BOTH TOW-OUT AND TOW-IN TOO");
          rgTowDat[2].TowOutAndIn = TRUE;
        }
      }
      else
      {
        dbg(TRACE,"TOWING IDX=2 HAS NOT BEEN SET AS 'VALID'");
      }
    }
    llCurLine = rgFltDat[2].FltLineNo;
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "OFBL", &argCtValue[1]);
    str_trm_rgt(argCtValue[1].Value, " ", TRUE);
    if (strlen(argCtValue[1].Value) > 0)
    {
      if (igSkipOfblFlights == TRUE)
      {
        dbg(TRACE,"(FLT CASE -1) THE DEPARTURE FLIGHT IS ALREADY OFF BLOCKS");
        rgFltDat[1].MaxErrCount++;
        ilCase = -1;
      }
      else
      {
        dbg(TRACE,"THE DEPARTURE FLIGHT IS ALREADY OFF BLOCKS");
        dbg(TRACE,"[AUTO_TOWING] SKIP_OFBL_FLIGHT CONFIGURED TO 'NO'");
      }
    }
    llCurLine = rgFltDat[1].FltLineNo;
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "PSTA", &argCtValue[1]);
    str_trm_rgt(argCtValue[1].Value, " ", TRUE);
    if (strlen(argCtValue[1].Value) > 0)
    {
      if (StandWithBridge(argCtValue[1].Value) != TRUE)
      {
        dbg(TRACE,"(FLT CASE -2) ARRIVAL FLIGHT IS ALREADY ASSIGNED TO REMOTE STAND");
        rgFltDat[1].MaxErrCount++;
        ilCase = -2;
      }
    }
    else
    {
      dbg(TRACE,"(FLT CASE -3) STAND FOR ARRIVAL FLIGHT NOT YET ASSIGNED");
      ilCase = -3;
      rgFltDat[1].MaxErrCount++;
    }
    llCurLine = rgFltDat[1].FltLineNo;
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "ONBL", &argCtValue[1]);
    str_trm_rgt(argCtValue[1].Value, " ", TRUE);
    if (strlen(argCtValue[1].Value) == 0)
    {
      if (igSkipSkedFlights == TRUE)
      {
        dbg(TRACE,"(FLT CASE -4) THE ARRIVAL FLIGHT IS NOT YET ON BLOCKS");
        ilCase = -4;
        rgFltDat[1].MaxErrCount++;
      }
      else
      {
        dbg(TRACE,"[AUTO_TOWING] SKIP_SKED_FLIGHT CONFIGURED TO 'NO'");
      }
    }
  }
  else if (rgFltDat[1].FltLineNo >= 0)
  {
    if (rgFltDat[1].MaxTowCount == 0)
    {
      dbg(TRACE,"STANDALONE ARRIVAL FLIGHT WITHOUT TOWING");
    }
    else if (rgFltDat[1].MaxTowCount > 1)
    {
      dbg(TRACE,"STANDALONE ARRIVAL FLIGHT WITH %d TOWINGS",rgFltDat[1].MaxTowCount);
    }
    else
    {
      dbg(TRACE,"STANDALONE ARRIVAL FLIGHT WITH ONE TOWING");
    }
    llCurLine = rgFltDat[1].FltLineNo;
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "PSTA", &argCtValue[1]);
    str_trm_rgt(argCtValue[1].Value, " ", TRUE);
    if (strlen(argCtValue[1].Value) > 0)
    {
      if (StandWithBridge(argCtValue[1].Value) != TRUE)
      {
        dbg(TRACE,"(FLT CASE -2) ARRIVAL FLIGHT ALREADY ASSIGNED TO REMOTE STAND");
        rgFltDat[1].MaxErrCount++;
        ilCase = -2;
      }
    }
    else
    {
      dbg(TRACE,"(FLT CASE -3) STAND FOR ARRIVAL FLIGHT NOT YET ASSIGNED");
      rgFltDat[1].MaxErrCount++;
      ilCase = -3;
    }
    if (rgTowDat[1].TowIsValid == TRUE)
    {
      if (strcmp(rgTowDat[1].StrTowPstd,rgFltDat[1].StrFltStnd) == 0)
      {
        dbg(TRACE,"TOWING IDX=1 IS A VALID TOW-OUT");
        rgTowDat[1].TowIsTowOut = TRUE;
        llCurLine = rgTowDat[1].TowLineNo;
        CT_GetColumnValueByName(pcgCtAft, llCurLine, "OFBL", &argCtValue[1]);
        str_trm_rgt(argCtValue[1].Value, " ", TRUE);
        if (strlen(argCtValue[1].Value) > 0)
        {
          dbg(TRACE,"THE TOW-OUT IS ALREADY OFF BLOCKS");
          rgTowDat[1].TowOutIsOfbl = TRUE;
          if (igSkipPabaFlights == TRUE)
          {
            /*
            rgFltDat[1].MaxErrCount++;
            ilCase = -1;
            */
          }
          else
          {
            dbg(DEBUG,"[AUTO_TOWING] SKIP_PABA_FLIGHT CONFIGURED TO 'NO'");
          }
        }
      }
      else
      {
        dbg(TRACE,"(TOW CASE -1) TOWING IDX=1 IS NOT MATCHING THE ARRIVAL STAND");
        /*
        ilCase = -1;
        rgFltDat[1].MaxErrCount++;
        */
      }
    }
  }
  else if (rgFltDat[2].FltLineNo >= 0)
  {
    if (rgFltDat[1].MaxTowCount == 0)
    {
      dbg(TRACE,"STANDALONE DEPARTURE FLIGHT WITHOUT ANY TOWING");
    }
    else
    {
      ilCase = 10;
      rgFltDat[1].MaxErrCount++;
      dbg(TRACE,"ERROR: (TOW CASE 10) STANDALONE DEPARTURE FLIGHT WITH %d TOWINGS",rgFltDat[1].MaxTowCount);
    }
  }
  if (ilCase == 0)
  {
    dbg(TRACE,"CHECK RESULT: EVERYTHING SHOULD BE FINE");
    dbg(TRACE,"---------------------------------------");
  }
  else
  {
    dbg(TRACE,"###################################################");
    dbg(TRACE,"# CHECK RESULT: THIS ROTATION WILL NOT BE HANDLED #");
    dbg(TRACE,">> NOTE: THE LAST IDENTIFIED FLT/TOW CASE WAS %d <<",ilCase);
    dbg(TRACE,"###################################################");
  }
  dbg(TRACE,"=======================================");
  return ilCase;
}

/* *************************************************** */
/* *************************************************** */
static void ResetFlightArray(void)
{
  int i = 0;
  for (i=0;i<=3;i++)
  {
    rgFltDat[i].FltExists = FALSE;
    rgFltDat[i].FltIsOALeg = FALSE;
    rgFltDat[i].FltIsNoop = FALSE;
    rgFltDat[i].FltIsValid = FALSE;
    rgFltDat[i].RotIsValid = FALSE;
    rgFltDat[i].TowIsValid = FALSE;
    rgFltDat[i].DelAllTows = FALSE;
    rgFltDat[i].DelArrTowOut = FALSE;
    rgFltDat[i].DelDepTowIn = FALSE;
    rgFltDat[i].DelAllOther = FALSE;
    strcpy(rgFltDat[i].StrFltUrno,"");
    strcpy(rgFltDat[i].StrFltRkey,"");
    strcpy(rgFltDat[i].StrFltSked,"");
    strcpy(rgFltDat[i].StrFltTime,"");
    strcpy(rgFltDat[i].StrFltStnd,"");
    strcpy(rgFltDat[i].StrLastPos,"");
    strcpy(rgFltDat[i].StrNewStnd,"");
    strcpy(rgFltDat[i].StrNewTime,"");
    strcpy(rgFltDat[i].StrOldTime,"");
    strcpy(rgFltDat[i].StrRulType,"");
    rgFltDat[i].ValFltSked = 0;
    rgFltDat[i].ValFltTime = 0;
    rgFltDat[i].ValNewTime = 0;
    rgFltDat[i].ValOldTime = 0;
    rgFltDat[i].ValMinDiff = 0;
    rgFltDat[i].MaxGrdTime = 0;
    rgFltDat[i].SkdGrdTime = 0;
    rgFltDat[i].RotGrdTime = 0;
    rgFltDat[i].CurBgnOffs = 0;
    rgFltDat[i].CurEndOffs = 0;
    rgFltDat[i].TowBgnOffs = 0;
    rgFltDat[i].TowEndOffs = 0;
    rgFltDat[i].FltLineNo = -1;
    rgFltDat[i].LastTowLineNo = -1;
    rgFltDat[i].MaxTowCount = 0;
    rgFltDat[i].ActTowCount = 0;
    rgFltDat[i].DelTowCount = 0;
    rgFltDat[i].ProTowCount = 0;
    rgFltDat[i].MaxErrCount = 0;
  }
  return;
}

/* *************************************************** */
/* *************************************************** */
static void ResetTowingArray(int ipIdx)
{
  int i = 0;
  int i1 = 0;
  int i2 = 0;
  if (ipIdx >= 0)
  {
    i1 = ipIdx;
    i2 = ipIdx;
  }
  else
  {
    i1 = 0;
    i2 = 5;
  }
  for (i=i1;i<=i2;i++)
  {
    rgTowDat[i].TowExists = FALSE;
    rgTowDat[i].TowIsValid = FALSE;
    rgTowDat[i].TowIsHidden = FALSE;
    rgTowDat[i].TowOutAndIn = FALSE;
    rgTowDat[i].TowOutIsOfbl = FALSE;
    rgTowDat[i].TowInIsOfbl = FALSE;
    rgTowDat[i].TowIsTowOut = FALSE;
    rgTowDat[i].TowIsTowIn = FALSE;
    rgTowDat[i].MyOwnTowRec = FALSE;
    strcpy(rgTowDat[i].StrTowUrno,"");
    strcpy(rgTowDat[i].StrFltSked,"");
    strcpy(rgTowDat[i].StrFltTime,"");
    strcpy(rgTowDat[i].StrFltStnd,"");
    strcpy(rgTowDat[i].StrTowStod,"");
    strcpy(rgTowDat[i].StrTowStoa,"");
    strcpy(rgTowDat[i].StrNewStod,"");
    strcpy(rgTowDat[i].StrNewStoa,"");
    strcpy(rgTowDat[i].StrTowPstd,"");
    strcpy(rgTowDat[i].StrTowPsta,"");
    rgTowDat[i].ValFltSked = 0;
    rgTowDat[i].ValFltTime = 0;
    rgTowDat[i].ValTowStod = 0;
    rgTowDat[i].ValTowStoa = 0;
    rgTowDat[i].ValNewStod = 0;
    rgTowDat[i].ValNewStoa = 0;
    rgTowDat[i].ValTowTime = 0;
    rgTowDat[i].CfgTowTime = igTowingTime;
    rgTowDat[i].ValTowPark = 0;
    rgTowDat[i].CurBgnOffs = 0;
    rgTowDat[i].CurEndOffs = 0;
    rgTowDat[i].TowBgnOffs = 0;
    rgTowDat[i].TowEndOffs = 0;
    rgTowDat[i].ArrLineNo = -1;
    rgTowDat[i].FltLineNo = -1;
    rgTowDat[i].TowLineNo = -1;

    strcpy(rgTowDat[i].StrTowRem1,"");
    strcpy(rgTowDat[i].StrRem2Old,"");
    strcpy(rgTowDat[i].StrRem2New,"");
    strcpy(rgTowDat[i].StrTowUsec,"");
    strcpy(rgTowDat[i].StrTowTRule,"");
    strcpy(rgTowDat[i].StrTowARule,"");
    strcpy(rgTowDat[i].StrTowDRule,"");
  }
  return;
}

/* *************************************************** */
/* *************************************************** */
static void FillTowingArray(int ipIdx, long lpLine)
{
  int ilTowIdx = -1;
  long llTowLine = -1;
  long llFltLine = -1;
  int ilDummy = 0;

  ilTowIdx = ipIdx;
  llTowLine = lpLine;
  if (ilTowIdx == 1)
  {

    ResetTowingArray(ilTowIdx);

    dbg(TRACE,"INITIALIZING MEMORY STRUCTURE OF TOW-OUT RECORD");

    if (rgFltDat[1].FltExists == TRUE)
    {
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* rgTowDat[i].ArrLineNo = -1;        */
      /* rgTowDat[i].FltLineNo = -1;        */
      /* ---------------------------------- */
      rgTowDat[ilTowIdx].ArrLineNo = rgFltDat[1].FltLineNo;
      rgTowDat[ilTowIdx].FltLineNo = rgFltDat[1].FltLineNo;
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* strcpy(rgTowDat[i].StrFltSked,""); */
      /* strcpy(rgTowDat[i].StrFltTime,""); */
      /* strcpy(rgTowDat[i].StrFltStnd,""); */
      /* ---------------------------------- */
      strcpy(rgTowDat[ilTowIdx].StrFltSked,rgFltDat[1].StrFltSked);
      strcpy(rgTowDat[ilTowIdx].StrFltTime,rgFltDat[1].StrFltTime);
      strcpy(rgTowDat[ilTowIdx].StrFltStnd,rgFltDat[1].StrFltStnd);
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* rgTowDat[i].ValFltSked = 0;        */
      /* rgTowDat[i].ValFltTime = 0;        */
      /* ---------------------------------- */
      rgTowDat[ilTowIdx].ValFltSked = rgFltDat[1].ValFltSked;
      rgTowDat[ilTowIdx].ValFltTime = rgFltDat[1].ValFltTime;
    }
    if (llTowLine >= 0)
    {
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* rgTowDat[i].TowExists = FALSE;     */
      /* rgTowDat[i].TowIsValid = FALSE;    */
      /* rgTowDat[i].TowLineNo = -1;        */
      /* ---------------------------------- */
      rgTowDat[ilTowIdx].TowExists = TRUE;
      rgTowDat[ilTowIdx].TowIsValid = TRUE;
      rgTowDat[ilTowIdx].TowLineNo = llTowLine;
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* strcpy(rgTowDat[i].StrTowUsec,""); */
      /* rgTowDat[i].MyOwnTowRec = FALSE;   */
      /* rgTowDat[i].TowIsTowOut = FALSE;   */
      /* ---------------------------------- */
      CT_GetColumnValueByName(pcgCtAft, llTowLine, "USEC", &rgGridVal);
      str_trm_rgt(rgGridVal.Value, " ", TRUE);
      strcpy(rgTowDat[ilTowIdx].StrTowUsec,rgGridVal.Value);
      if ((strstr(rgGridVal.Value,"AutoTowArr") != NULL)||(strstr(rgGridVal.Value,"AutoTowDep") != NULL))
      {
        rgTowDat[ilTowIdx].MyOwnTowRec = TRUE;
      }
      if (strstr(rgGridVal.Value,"AutoTowArr") != NULL)
      {
        rgTowDat[ilTowIdx].TowIsTowOut = TRUE;
      }
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* rgTowDat[i].TowIsHidden = FALSE;   */
      /* ---------------------------------- */
      CT_GetColumnValueByName(pcgCtAft, llTowLine, "STAT", &rgGridVal);
      str_trm_rgt(rgGridVal.Value, " ", TRUE);
      if (strstr(rgGridVal.Value,"DEL") != NULL)
      {
        rgTowDat[ilTowIdx].TowIsHidden = TRUE;
      }
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* strcpy(rgTowDat[i].StrTowPstd,""); */
      /* strcpy(rgTowDat[i].StrTowPsta,""); */
      /* ---------------------------------- */
      CT_GetColumnValueByName(pcgCtAft, llTowLine, "PSTD", &rgGridVal);
      str_trm_rgt(rgGridVal.Value, " ", TRUE);
      strcpy(rgTowDat[ilTowIdx].StrTowPstd,rgGridVal.Value);
      CT_GetColumnValueByName(pcgCtAft, llTowLine, "PSTA", &rgGridVal);
      str_trm_rgt(rgGridVal.Value, " ", TRUE);
      strcpy(rgTowDat[ilTowIdx].StrTowPsta,rgGridVal.Value);
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* strcpy(rgTowDat[i].StrTowStod,""); */
      /* strcpy(rgTowDat[i].StrTowStoa,""); */
      /* ---------------------------------- */
      CT_GetColumnValueByName(pcgCtAft, llTowLine, "STOD", &rgGridVal);
      str_trm_rgt(rgGridVal.Value, " ", TRUE);
      strcpy(rgTowDat[ilTowIdx].StrTowStod,rgGridVal.Value);
      CT_GetColumnValueByName(pcgCtAft, llTowLine, "STOA", &rgGridVal);
      str_trm_rgt(rgGridVal.Value, " ", TRUE);
      strcpy(rgTowDat[ilTowIdx].StrTowStoa,rgGridVal.Value);
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* rgTowDat[i].ValTowStod = 0;        */
      /* rgTowDat[i].ValTowStoa = 0;        */
      /* rgTowDat[i].ValTowTime = 0;        */
      /* ---------------------------------- */
      (void) GetFullDay(rgTowDat[ilTowIdx].StrTowStod, &ilDummy, &rgTowDat[ilTowIdx].ValTowStod, &ilDummy);
      (void) GetFullDay(rgTowDat[ilTowIdx].StrTowStoa, &ilDummy, &rgTowDat[ilTowIdx].ValTowStoa, &ilDummy);
      rgTowDat[ilTowIdx].ValTowTime = rgTowDat[ilTowIdx].ValTowStoa - rgTowDat[ilTowIdx].ValTowStod;
      /* ---------------------------------- */
      /* strcpy(rgTowDat[i].StrNewStod,""); */
      /* strcpy(rgTowDat[i].StrNewStoa,""); */
      /* rgTowDat[i].ValNewStod = 0;        */
      /* rgTowDat[i].ValNewStoa = 0;        */
      /* ---------------------------------- */
      strcpy(rgTowDat[ilTowIdx].StrNewStod,rgTowDat[ilTowIdx].StrTowStod);
      strcpy(rgTowDat[ilTowIdx].StrNewStoa,rgTowDat[ilTowIdx].StrTowStoa);
      rgTowDat[ilTowIdx].ValNewStod = rgTowDat[ilTowIdx].ValTowStod;
      rgTowDat[ilTowIdx].ValNewStoa = rgTowDat[ilTowIdx].ValTowStoa;
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* strcpy(rgTowDat[i].StrTowRem1,""); */
      /* ---------------------------------- */
      CT_GetColumnValueByName(pcgCtAft, llTowLine, "REM1", &rgGridVal);
      str_trm_rgt(rgGridVal.Value, " ", TRUE);
      strcpy(rgTowDat[ilTowIdx].StrTowRem1,rgGridVal.Value);
      CT_GetColumnValueByName(pcgCtAft, llTowLine, "REM2", &rgGridVal);
      str_trm_rgt(rgGridVal.Value, " ", TRUE);
      strcpy(rgTowDat[ilTowIdx].StrRem2Old,rgGridVal.Value);
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* rgTowDat[i].TowOutIsOfbl = FALSE;  */
      /* ---------------------------------- */
      CT_GetColumnValueByName(pcgCtAft, llTowLine, "OFBL", &rgGridVal);
      str_trm_rgt(rgGridVal.Value, " ", TRUE);
      if (strlen(rgGridVal.Value) > 0)
      {
        rgTowDat[ilTowIdx].TowOutIsOfbl = TRUE;
      }
      if (rgFltDat[1].FltExists == TRUE)
      {
        /* ---------------------------------- */
        /* Default Values After ResetTowArray */
        /* rgTowDat[i].CurBgnOffs = 0;        */
        /* rgTowDat[i].TowBgnOffs = 0;        */
        /* ---------------------------------- */
        rgTowDat[ilTowIdx].CurBgnOffs = rgTowDat[ilTowIdx].ValTowStod - rgTowDat[ilTowIdx].ValFltTime;
        /* rgTowDat[ilTowIdx].TowBgnOffs = rgTowDat[ilTowIdx].ValTowStod - rgTowDat[ilTowIdx].ValFltTime; */
      }

      if (rgFltDat[2].FltExists == TRUE)
      {
        /* ---------------------------------- */
        /* Default Values After ResetTowArray */
        /* rgTowDat[i].TowOutAndIn = FALSE;   */
        /* ---------------------------------- */
        if (strcmp(rgTowDat[ilTowIdx].StrTowPsta,rgFltDat[2].StrFltStnd) == 0)
        {
          rgTowDat[ilTowIdx].TowOutAndIn = TRUE;
        }
      }


      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* (Related to DEP or Not Yet Known)  */
      /* ---------------------------------- */
      /* rgTowDat[i].TowInIsOfbl = FALSE;   */
      /* rgTowDat[i].TowIsTowIn = FALSE;    */
      /* rgTowDat[i].ValTowPark = 0;        */
      /* rgTowDat[i].CurEndOffs = 0;        */
      /* rgTowDat[i].TowEndOffs = 0;        */
      /* strcpy(rgTowDat[i].StrTowTRule,"");*/
      /* strcpy(rgTowDat[i].StrTowARule,"");*/
      /* strcpy(rgTowDat[i].StrTowDRule,"");*/
      /* ---------------------------------- */

    }
  }
  if (ilTowIdx == 2)
  {

    ResetTowingArray(ilTowIdx);

    dbg(TRACE,"INITIALIZING MEMORY STRUCTURE OF TOW-IN RECORD");

    if (rgFltDat[2].FltExists == TRUE)
    {
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* rgTowDat[i].ArrLineNo = -1;        */
      /* rgTowDat[i].FltLineNo = -1;        */
      /* ---------------------------------- */
      rgTowDat[ilTowIdx].ArrLineNo = rgFltDat[1].FltLineNo;
      rgTowDat[ilTowIdx].FltLineNo = rgFltDat[2].FltLineNo;
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* strcpy(rgTowDat[i].StrFltSked,""); */
      /* strcpy(rgTowDat[i].StrFltTime,""); */
      /* strcpy(rgTowDat[i].StrFltStnd,""); */
      /* ---------------------------------- */
      strcpy(rgTowDat[ilTowIdx].StrFltSked,rgFltDat[2].StrFltSked);
      strcpy(rgTowDat[ilTowIdx].StrFltTime,rgFltDat[2].StrFltTime);
      strcpy(rgTowDat[ilTowIdx].StrFltStnd,rgFltDat[2].StrFltStnd);
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* rgTowDat[i].ValFltSked = 0;        */
      /* rgTowDat[i].ValFltTime = 0;        */
      /* ---------------------------------- */
      rgTowDat[ilTowIdx].ValFltSked = rgFltDat[2].ValFltSked;
      rgTowDat[ilTowIdx].ValFltTime = rgFltDat[2].ValFltTime;
    }
    if (llTowLine >= 0)
    {
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* rgTowDat[i].TowExists = FALSE;     */
      /* rgTowDat[i].TowIsValid = FALSE;    */
      /* rgTowDat[i].TowLineNo = -1;        */
      /* ---------------------------------- */
      rgTowDat[ilTowIdx].TowExists = TRUE;
      rgTowDat[ilTowIdx].TowIsValid = TRUE;
      rgTowDat[ilTowIdx].TowLineNo = llTowLine;
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* strcpy(rgTowDat[i].StrTowUsec,""); */
      /* rgTowDat[i].MyOwnTowRec = FALSE;   */
      /* rgTowDat[i].TowIsTowIn = FALSE;    */
      /* ---------------------------------- */
      CT_GetColumnValueByName(pcgCtAft, llTowLine, "USEC", &rgGridVal);
      str_trm_rgt(rgGridVal.Value, " ", TRUE);
      strcpy(rgTowDat[ilTowIdx].StrTowUsec,rgGridVal.Value);
      if ((strstr(rgGridVal.Value,"AutoTowArr") != NULL)||(strstr(rgGridVal.Value,"AutoTowDep") != NULL))
      {
        rgTowDat[ilTowIdx].MyOwnTowRec = TRUE;
      }
      if (strstr(rgGridVal.Value,"AutoTowDep") != NULL)
      {
        rgTowDat[ilTowIdx].TowIsTowIn = TRUE;
      }
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* rgTowDat[i].TowIsHidden = FALSE;   */
      /* ---------------------------------- */
      CT_GetColumnValueByName(pcgCtAft, llTowLine, "STAT", &rgGridVal);
      str_trm_rgt(rgGridVal.Value, " ", TRUE);
      if (strstr(rgGridVal.Value,"DEL") != NULL)
      {
        rgTowDat[ilTowIdx].TowIsHidden = TRUE;
      }
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* strcpy(rgTowDat[i].StrTowPstd,""); */
      /* strcpy(rgTowDat[i].StrTowPsta,""); */
      /* ---------------------------------- */
      CT_GetColumnValueByName(pcgCtAft, llTowLine, "PSTD", &rgGridVal);
      str_trm_rgt(rgGridVal.Value, " ", TRUE);
      strcpy(rgTowDat[ilTowIdx].StrTowPstd,rgGridVal.Value);
      CT_GetColumnValueByName(pcgCtAft, llTowLine, "PSTA", &rgGridVal);
      str_trm_rgt(rgGridVal.Value, " ", TRUE);
      strcpy(rgTowDat[ilTowIdx].StrTowPsta,rgGridVal.Value);
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* strcpy(rgTowDat[i].StrTowStod,""); */
      /* strcpy(rgTowDat[i].StrTowStoa,""); */
      /* ---------------------------------- */
      CT_GetColumnValueByName(pcgCtAft, llTowLine, "STOD", &rgGridVal);
      str_trm_rgt(rgGridVal.Value, " ", TRUE);
      strcpy(rgTowDat[ilTowIdx].StrTowStod,rgGridVal.Value);
      CT_GetColumnValueByName(pcgCtAft, llTowLine, "STOA", &rgGridVal);
      str_trm_rgt(rgGridVal.Value, " ", TRUE);
      strcpy(rgTowDat[ilTowIdx].StrTowStoa,rgGridVal.Value);
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* rgTowDat[i].ValTowStod = 0;        */
      /* rgTowDat[i].ValTowStoa = 0;        */
      /* rgTowDat[i].ValTowTime = 0;        */
      /* ---------------------------------- */
      (void) GetFullDay(rgTowDat[ilTowIdx].StrTowStod, &ilDummy, &rgTowDat[ilTowIdx].ValTowStod, &ilDummy);
      (void) GetFullDay(rgTowDat[ilTowIdx].StrTowStoa, &ilDummy, &rgTowDat[ilTowIdx].ValTowStoa, &ilDummy);
      rgTowDat[ilTowIdx].ValTowTime = rgTowDat[ilTowIdx].ValTowStoa - rgTowDat[ilTowIdx].ValTowStod;
      /* ---------------------------------- */
      /* strcpy(rgTowDat[i].StrNewStod,""); */
      /* strcpy(rgTowDat[i].StrNewStoa,""); */
      /* rgTowDat[i].ValNewStod = 0;        */
      /* rgTowDat[i].ValNewStoa = 0;        */
      /* ---------------------------------- */
      strcpy(rgTowDat[ilTowIdx].StrNewStod,rgTowDat[ilTowIdx].StrTowStod);
      strcpy(rgTowDat[ilTowIdx].StrNewStoa,rgTowDat[ilTowIdx].StrTowStoa);
      rgTowDat[ilTowIdx].ValNewStod = rgTowDat[ilTowIdx].ValTowStod;
      rgTowDat[ilTowIdx].ValNewStoa = rgTowDat[ilTowIdx].ValTowStoa;
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* strcpy(rgTowDat[i].StrTowRem1,""); */
      /* ---------------------------------- */
      CT_GetColumnValueByName(pcgCtAft, llTowLine, "REM1", &rgGridVal);
      str_trm_rgt(rgGridVal.Value, " ", TRUE);
      strcpy(rgTowDat[ilTowIdx].StrTowRem1,rgGridVal.Value);
      CT_GetColumnValueByName(pcgCtAft, llTowLine, "REM2", &rgGridVal);
      dbg(TRACE,"REM2(%d) OLD <%s>",ilTowIdx,rgGridVal.Value);
      str_trm_rgt(rgGridVal.Value, " ", TRUE);
      strcpy(rgTowDat[ilTowIdx].StrRem2Old,rgGridVal.Value);
      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* rgTowDat[i].TowInIsOfbl = FALSE;   */
      /* ---------------------------------- */
      CT_GetColumnValueByName(pcgCtAft, llTowLine, "OFBL", &rgGridVal);
      str_trm_rgt(rgGridVal.Value, " ", TRUE);
      if (strlen(rgGridVal.Value) > 0)
      {
        rgTowDat[ilTowIdx].TowInIsOfbl = TRUE;
      }
      if (rgFltDat[1].FltExists == TRUE)
      {
        /* ---------------------------------- */
        /* Default Values After ResetTowArray */
        /* rgTowDat[i].CurEndOffs = 0;        */
        /* rgTowDat[i].TowEndOffs = 0;        */
        /* ---------------------------------- */
        rgTowDat[ilTowIdx].CurEndOffs = rgTowDat[ilTowIdx].ValFltTime - rgTowDat[ilTowIdx].ValTowStoa;
        /* rgTowDat[ilTowIdx].TowEndOffs = rgTowDat[ilTowIdx].ValFltTime - rgTowDat[ilTowIdx].ValTowStoa; */
      }

      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* rgTowDat[i].TowOutAndIn = FALSE;   */
      /* ---------------------------------- */
      if (strcmp(rgTowDat[ilTowIdx].StrTowPstd,rgFltDat[1].StrFltStnd) == 0)
      {
        rgTowDat[ilTowIdx].TowOutAndIn = TRUE;
      }


      /* ---------------------------------- */
      /* Default Values After ResetTowArray */
      /* (Related to ARR or Not Yet Known)  */
      /* ---------------------------------- */
      /* rgTowDat[i].TowIsTowOut = FALSE;   */
      /* rgTowDat[i].ValTowPark = 0;        */
      /* rgTowDat[i].TowOutIsOfbl = FALSE;  */
      /* rgTowDat[i].CurBgnOffs = 0;        */
      /* rgTowDat[i].TowBgnOffs = 0;        */
      /* strcpy(rgTowDat[i].StrTowTRule,"");*/
      /* strcpy(rgTowDat[i].StrTowARule,"");*/
      /* strcpy(rgTowDat[i].StrTowDRule,"");*/
      /* ---------------------------------- */

    }
  }
  return;
}

/* *************************************************** */
/* *************************************************** */
static void PrintTowingArray(int ipIdx)
{
  int i = -1;
  if (ipIdx == 1)
  {
    i = ipIdx;
    dbg(TRACE,"=> ----------------------------------------");
    dbg(TRACE,"=> MEMORY STRUCTURE OF TOW-OUT RECORD (IDX=%d)",i);
  }
  if (ipIdx == 2)
  {
    i = ipIdx;
    dbg(TRACE,"=> ----------------------------------------");
    dbg(TRACE,"=> MEMORY STRUCTURE OF TOW-IN  RECORD (IDX=%d)",i);
  }
  if (ipIdx == 0)
  {
    i = ipIdx;
    dbg(TRACE,"=> -------------------------------------------");
    dbg(TRACE,"=> MEMORY STRUCTURE OF ANY TOWING RECORD (IDX=%d)",i);
  }
  if (i < 0)
  {
    return;
  }

  dbg(TRACE,"=> rgTowDat[%d].TowExists .... %d", i,rgTowDat[i].TowExists);
  dbg(TRACE,"=> rgTowDat[%d].TowIsValid ... %d", i,rgTowDat[i].TowIsValid);
  dbg(TRACE,"=> rgTowDat[%d].TowIsHidden .. %d", i,rgTowDat[i].TowIsHidden);
  dbg(TRACE,"=> rgTowDat[%d].TowOutAndIn .. %d", i,rgTowDat[i].TowOutAndIn);
  dbg(TRACE,"=> rgTowDat[%d].TowOutIsOfbl . %d", i,rgTowDat[i].TowOutIsOfbl);
  dbg(TRACE,"=> rgTowDat[%d].TowInIsOfbl .. %d", i,rgTowDat[i].TowInIsOfbl);
  dbg(TRACE,"=> rgTowDat[%d].TowIsTowOut .. %d", i,rgTowDat[i].TowIsTowOut);
  dbg(TRACE,"=> rgTowDat[%d].TowIsTowIn ... %d", i,rgTowDat[i].TowIsTowIn);
  dbg(TRACE,"=> rgTowDat[%d].MyOwnTowRec .. %d", i,rgTowDat[i].MyOwnTowRec);
  dbg(TRACE,"=> rgTowDat[%d].StrTowUrno ...<%s>",i,rgTowDat[i].StrTowUrno);
  dbg(TRACE,"=> rgTowDat[%d].StrFltSked ...<%s>",i,rgTowDat[i].StrFltSked);
  dbg(TRACE,"=> rgTowDat[%d].StrFltTime ...<%s>",i,rgTowDat[i].StrFltTime);
  dbg(TRACE,"=> rgTowDat[%d].StrFltStnd ...<%s>",i,rgTowDat[i].StrFltStnd);
  dbg(TRACE,"=> rgTowDat[%d].StrTowStod ...<%s>",i,rgTowDat[i].StrTowStod);
  dbg(TRACE,"=> rgTowDat[%d].StrTowStoa ...<%s>",i,rgTowDat[i].StrTowStoa);
  dbg(TRACE,"=> rgTowDat[%d].StrNewStod ...<%s>",i,rgTowDat[i].StrNewStod);
  dbg(TRACE,"=> rgTowDat[%d].StrNewStoa ...<%s>",i,rgTowDat[i].StrNewStoa);
  dbg(TRACE,"=> rgTowDat[%d].StrTowPstd ...<%s>",i,rgTowDat[i].StrTowPstd);
  dbg(TRACE,"=> rgTowDat[%d].StrTowPsta ...<%s>",i,rgTowDat[i].StrTowPsta);
  dbg(TRACE,"=> rgTowDat[%d].ValFltSked ... %d", i,rgTowDat[i].ValFltSked);
  dbg(TRACE,"=> rgTowDat[%d].ValFltTime ... %d", i,rgTowDat[i].ValFltTime);
  dbg(TRACE,"=> rgTowDat[%d].ValTowStod ... %d", i,rgTowDat[i].ValTowStod);
  dbg(TRACE,"=> rgTowDat[%d].ValTowStoa ... %d", i,rgTowDat[i].ValTowStoa);
  dbg(TRACE,"=> rgTowDat[%d].ValNewStod ... %d", i,rgTowDat[i].ValNewStod);
  dbg(TRACE,"=> rgTowDat[%d].ValNewStoa ... %d", i,rgTowDat[i].ValNewStoa);
  dbg(TRACE,"=> rgTowDat[%d].ValTowTime ... %d", i,rgTowDat[i].ValTowTime);
  dbg(TRACE,"=> rgTowDat[%d].CfgTowTime ... %d", i,rgTowDat[i].CfgTowTime);
  dbg(TRACE,"=> rgTowDat[%d].ValTowPark ... %d", i,rgTowDat[i].ValTowPark);
  dbg(TRACE,"=> rgTowDat[%d].CurBgnOffs ... %d", i,rgTowDat[i].CurBgnOffs);
  dbg(TRACE,"=> rgTowDat[%d].CurEndOffs ... %d", i,rgTowDat[i].CurEndOffs);
  dbg(TRACE,"=> rgTowDat[%d].TowBgnOffs ... %d", i,rgTowDat[i].TowBgnOffs);
  dbg(TRACE,"=> rgTowDat[%d].TowEndOffs ... %d", i,rgTowDat[i].TowEndOffs);
  dbg(TRACE,"=> rgTowDat[%d].ArrLineNo .... %d", i,rgTowDat[i].ArrLineNo);
  dbg(TRACE,"=> rgTowDat[%d].FltLineNo .... %d", i,rgTowDat[i].FltLineNo);
  dbg(TRACE,"=> rgTowDat[%d].TowLineNo .... %d", i,rgTowDat[i].TowLineNo);
  dbg(TRACE,"=> rgTowDat[%d].StrTowRem1 ...<%s>",i,rgTowDat[i].StrTowRem1);
  dbg(TRACE,"=> rgTowDat[%d].StrTowUsec ...<%s>",i,rgTowDat[i].StrTowUsec);
  dbg(TRACE,"=> rgTowDat[%d].StrTowTRule ..<%s>",i,rgTowDat[i].StrTowTRule);
  dbg(TRACE,"=> rgTowDat[%d].StrTowARule ..<%s>",i,rgTowDat[i].StrTowARule);
  dbg(TRACE,"=> rgTowDat[%d].StrTowDRule ..<%s>",i,rgTowDat[i].StrTowDRule);
  dbg(TRACE,"=> rgTowDat[%d].StrRem2Old ...<%s>",i,rgTowDat[i].StrRem2Old);
  dbg(TRACE,"=> rgTowDat[%d].StrRem2New ...<%s>",i,rgTowDat[i].StrRem2New);
  dbg(TRACE,"=> ----------------------------------------");
  return;
}

/* *************************************************** */
/* *************************************************** */
static int EvaluateTowingRules(void)
{
  int ilRc = RC_SUCCESS;
  int ilIsFine = TRUE;
  char pclChkRuleType[8] = "";
  char pclCurRuleType[8] = "";
  char pclGhpFldLst[1024] = "";
  char pclAftFldLst[1024] = "";
  char pclGhpFldName[128] = "";
  char pclAftFldList[128] = "";
  char pclAftFldName[128] = "";
  char pclAftFldData[128] = "";
  long llMaxLine = 0;
  long llCurLine = 0;
  int ilGhpFld = 0;
  int ilGhpCnt = 0;
  int ilAftFld = 0;
  int ilAftCnt = 0;
  int ilRuleFound = FALSE;
  int ilRuleMatch = TRUE;
  int ilFieldMatch = TRUE;
  int ilFlightMatch = TRUE;
  int ilRuleCount = 0;
  int ilMax = 0;
  int ilCur = 0;
  int ilRetVal = 0;
  int ilMaxOvnTime = 0;
  long llAftRecLine = -1;
  long llChkAftArrLine = -1;
  long llChkAftDepLine = -1;
  long llArrRuleLineNo = -1;
  long llDepRuleLineNo = -1;
  long llRotRuleLineNo = -1;
  long llRuleLineNo = -1;

  dbg(TRACE,"EVALUATION OF TOWING RULES");
  dbg(TRACE,"--------------------------");
  dbg(TRACE,"CHECK CONDITION OF LOADED DATA SET");
  dbg(TRACE,"==================================");


  if ((rgFltDat[1].FltExists == TRUE)&&(rgTowDat[1].TowExists == TRUE))
  {
    rgTowDat[1].CurBgnOffs = rgTowDat[1].ValTowStod - rgFltDat[1].ValFltTime;
    rgFltDat[1].CurBgnOffs = rgTowDat[1].CurBgnOffs;
    dbg(TRACE,"ARR TOWING.OUT: STAND <%s>\tFLT.TIFA/TOW.STOD <%s> <%s> OFFSET=%d",rgTowDat[1].StrTowPstd,rgFltDat[1].StrFltTime,rgTowDat[1].StrTowStod,rgTowDat[1].CurBgnOffs);
  }
  if ((rgFltDat[2].FltExists == TRUE)&&(rgTowDat[2].TowExists == TRUE))
  {
    rgTowDat[2].CurEndOffs =  rgFltDat[2].ValFltTime - rgTowDat[2].ValTowStoa;
    rgFltDat[2].CurBgnOffs = rgTowDat[2].CurBgnOffs;
    dbg(TRACE,"DEP TOWING.IN : STAND <%s>\tFLT.TIFD/TOW.STOA <%s> <%s> OFFSET=%d",rgTowDat[2].StrTowPsta,rgFltDat[2].StrFltTime,rgTowDat[2].StrTowStoa,rgTowDat[2].CurEndOffs);
  }

  pclChkRuleType[0] = 0x00;
  ilMaxOvnTime = 0;
  igAutoCreateTowArr = FALSE;
  igAutoCreateTowDep = FALSE;

  if ((rgFltDat[1].FltLineNo >= 0) && (rgFltDat[2].FltLineNo >= 0))
  {
    dbg(TRACE,"CHECK CONDITION OF FLIGHT ROTATION");
    dbg(TRACE,"==================================");
    ilRetVal = DateDiffToMin(rgFltDat[2].StrFltTime,rgFltDat[1].StrFltTime,&rgFltDat[1].RotGrdTime);

    dbg(TRACE,"ACTUAL GROUND TIME: ARR.TIFA <%s> DEP.TIFD <%s> (%d MINUTES)",rgFltDat[1].StrFltTime,rgFltDat[2].StrFltTime,rgFltDat[1].RotGrdTime);
    ilMaxOvnTime = CheckOverNightRule(rgFltDat[1].StrFltTime, rgFltDat[2].StrFltTime);
    dbg(TRACE,"==================================");

    strcpy(pcgCurRacTifa,rgFltDat[1].StrFltTime);
    strcpy(pcgCurRacTifd,rgFltDat[2].StrFltTime);

    if (rgFltDat[1].MaxTowCount > 1)
    {
      dbg(TRACE,"ROTATION WITH %d EXISTING TOWINGS",rgFltDat[1].MaxTowCount);
      dbg(TRACE,"(CHECK RULES FOR TIME CONDITIONS)");
      dbg(TRACE,"---------------------------------");
      strcpy(pclChkRuleType,"TAD");
    }
    else if (rgFltDat[1].MaxTowCount == 1)
    {
      dbg(TRACE,"ROTATION WITH ONE SINGLE TOWING");
      dbg(TRACE,"-------------------------------");
      if (strcmp(rgFltDat[1].StrFltStnd,rgFltDat[2].StrFltStnd) == 0)
      {
        dbg(TRACE,"ARR/DEP FLIGHT ON SAME STAND");
        dbg(TRACE,"(WOULD NEED ONE MORE TOWING)");
        dbg(TRACE,"----------------------------");
        strcpy(pclChkRuleType,"TAD");
      }
      else
      {
        dbg(TRACE,"ARR/DEP FLIGHT ON DIFFERENT STANDS");
        dbg(TRACE,"(TURNAROUND NEEDS ONLY ONE TOWING)");
        if (rgTowDat[1].TowExists == TRUE)
        {
          dbg(TRACE,"(CHECK CONDITION FOR TOWING-IN)");
          dbg(TRACE,"-------------------------------");
          strcpy(pclChkRuleType,"TAD");
        }
        else
        {
          dbg(TRACE,"(CHECK CONDITION FOR TOWING-OUT)");
          dbg(TRACE,"--------------------------------");
          strcpy(pclChkRuleType,"TAD");
        }
      }
    }
    else
    {
      dbg(TRACE,"ROTATION WITHOUT TOWINGS");
      if (strcmp(rgFltDat[1].StrFltStnd,rgFltDat[2].StrFltStnd) == 0)
      {
        dbg(TRACE,"ARR/DEP FLIGHT ON SAME PARKING STAND");
        dbg(TRACE,"(SEARCH RULES FOR TOWING ARR.OUT AND DEP.IN)");
        dbg(TRACE,"--------------------------------------------");
        strcpy(pclChkRuleType,"TAD");
      }
      else
      {
        dbg(TRACE,"ARR/DEP FLIGHT ON DIFFERENT STANDS");
        dbg(TRACE,"(WOULD NEED AT LEAST ONE TOWING)");
        dbg(TRACE,"--------------------------------------------");
        strcpy(pclChkRuleType,"TAD");
      }

    }
  }
  else if (rgFltDat[1].FltLineNo >= 0)
  {
    dbg(TRACE,"CHECK CONDITION OF STANDALONE ARRIVAL FLIGHT");
    dbg(TRACE,"--------------------------------------------");
    strcpy(pcgCurRacTifa,rgFltDat[1].StrFltTime);
    rgFltDat[1].FltIsOALeg = TRUE;
    if (rgFltDat[1].MaxTowCount > 1)
    {
      dbg(TRACE,"STANDALONE ARRIVAL FLIGHT WITH %d EXISTING TOWINGS",rgFltDat[1].MaxTowCount);
      dbg(TRACE,"(CHECK TIME CONDITION FOR TOWING OUT)");
    }
    else if (rgFltDat[1].MaxTowCount == 1)
    {
      dbg(TRACE,"STANDALONE ARRIVAL FLIGHT WITH ONE SINGLE TOWING");
      dbg(TRACE,"(CHECK TIME CONDITION FOR TOWING OUT)");
    }
    else
    {
      dbg(TRACE,"STANDALONE ARRIVAL FLIGHT WITHOUT ANY TOWING");
      dbg(TRACE,"(SEARCH TIME CONDITION FOR TOWING OUT)");
    }
    ilMaxOvnTime = igMaxOvnTime;
    DateDiffToMin(pcgPaesArr,rgFltDat[1].StrFltTime,&igOpnArrGrdTime);
    if (ilMaxOvnTime > 0)
    {
      rgFltDat[1].RotGrdTime = igOpnArrGrdTime;
      dbg(TRACE,"FLIGHT WILL BE HANDLED AS OVERNIGHT (MAX TIME=%d)",ilMaxOvnTime);
      dbg(TRACE,"OPN_ARR_GRD_TIME SET TO %d ",rgFltDat[1].RotGrdTime);
    }
    else
    {
      ilMaxOvnTime = 1400;
      rgFltDat[1].RotGrdTime = igOpnArrGrdTime;
      dbg(TRACE,"OPN_ARR_GRD_TIME SET TO %d ",rgFltDat[1].RotGrdTime);
    }
    strcpy(pclChkRuleType,"A");
  }
  else if (rgFltDat[2].FltLineNo >= 0)
  {
    dbg(TRACE,"CHECK CONDITION OF STANDALONE DEPARTURE FLIGHT");
    dbg(TRACE,"----------------------------------------------");
    if (rgFltDat[1].MaxTowCount > 1)
    {
      ilIsFine = FALSE;
      dbg(TRACE,"ERROR: STANDALONE DEPARTURE FLIGHT WITH %d TOWINGS",rgFltDat[1].MaxTowCount);
      dbg(TRACE,"(NO TOWING RULE EVALUATION FOR DEPARTURE FLIGHTS)");
      dbg(TRACE,"-------------------------------------------------");
      rgFltDat[1].DelAllTows = TRUE;
    }
    else if (rgFltDat[1].MaxTowCount == 1)
    {
      ilIsFine = FALSE;
      dbg(TRACE,"ERROR: STANDALONE DEP FLIGHT WITH ONE SINGLE TOWING");
      dbg(TRACE,"(NO TOWING RULE EVALUATION FOR DEPARTURE FLIGHTS)");
      dbg(TRACE,"-------------------------------------------------");
      rgFltDat[1].DelAllTows = TRUE;
    }
    else
    {
      dbg(TRACE,"STANDALONE DEPARTURE FLIGHT WITHOUT TOWING");
      dbg(TRACE,"(NO TOWING RULE EVALUATION FOR DEP FLIGHTS)");
      dbg(TRACE,"-------------------------------------------");
    }
  }

  if (strlen(pclChkRuleType) > 0)
  {
    llRuleLineNo = -1;
    if (ilMaxOvnTime <= 0)
    {
      if (strstr(pclChkRuleType,"T") != NULL)
      {
        if (rgFltDat[2].FltLineNo >= 0)
        {
          dbg(TRACE,"SEARCH TURNAROUND RULE WITH DEPARTURE RECORD");
          llRuleLineNo = SearchTowingRule("T",rgFltDat[2].FltLineNo,FALSE);
          if (llRuleLineNo < 0)
          {
            dbg(TRACE,"NO RULE FOUND. TRY AGAIN WITH ARRIVAL RECORD");
          }
        }
        if (llRuleLineNo < 0)
        {
          if (rgFltDat[1].FltLineNo >= 0)
          {
            dbg(TRACE,"SEARCH TURNAROUND RULE WITH ARRIVAL RECORD");
            llRuleLineNo = SearchTowingRule("T",rgFltDat[1].FltLineNo,FALSE);
          }
        }
      }
    }
    else
    {
      dbg(TRACE,"SKIP TURNAROUND RULE (OVERNIGHT)");
      strcpy(rgTowDat[1].StrTowTRule,"OVN");
      strcpy(rgTowDat[2].StrTowTRule,"OVN");
      llRuleLineNo = -1;
      strcpy(rgFltDat[1].StrRulType,"O");
    }
    if ((llRuleLineNo >= 0)||(ilMaxOvnTime > 0))
    {
      if (ilMaxOvnTime <= 0)
      {
        strcpy(rgFltDat[1].StrRulType,"R");
        CT_GetColumnValueByName(pcgCtGhp, llRuleLineNo, "PRSN", &rgAnyStrg);
        str_trm_rgt(rgAnyStrg.Value, " ", TRUE);
        strcpy(rgTowDat[1].StrTowTRule,rgAnyStrg.Value);
        strcpy(rgTowDat[2].StrTowTRule,rgAnyStrg.Value);
        CT_GetColumnValueByName(pcgCtGhp, llRuleLineNo, "MAXT", &argCtValue[1]);
        str_trm_rgt(argCtValue[1].Value, " ", TRUE);
        dbg(TRACE,"RULE T <%s> MAX TURNAROUND TIME <%s> <%s> <%s>",rgAnyStrg.Value,pcgCtGhp,"MAXT",argCtValue[1].Value);
        rgFltDat[1].MaxGrdTime = atoi(argCtValue[1].Value);

        if (strstr(pclChkRuleType,"A") != NULL)
        {
          CT_GetColumnValueByName(pcgCtGhp, llRuleLineNo, "HTPA", &argCtValue[1]);
          str_trm_rgt(argCtValue[1].Value, " ", TRUE);
          rgTowDat[1].TowBgnOffs = atoi(argCtValue[1].Value);
          if (rgTowDat[1].TowBgnOffs > 0)
          {
            dbg(TRACE,"TURNAROUND RULE: TIME FOR TOW-OUT <%s> <%s> <%s>",pcgCtGhp,"HTPA",argCtValue[1].Value);
            dbg(TRACE,"---------------------------------");
            rgTowDat[1].TowIsValid = TRUE;
            igAutoCreateTowArr = TRUE;
          }
        }
        if (strstr(pclChkRuleType,"D") != NULL)
        {
          CT_GetColumnValueByName(pcgCtGhp, llRuleLineNo, "HTPD", &argCtValue[1]);
          str_trm_rgt(argCtValue[1].Value, " ", TRUE);
          rgTowDat[2].TowEndOffs = atoi(argCtValue[1].Value);
          if (rgTowDat[2].TowEndOffs > 0)
          {
            dbg(TRACE,"TURNAROUND RULE: TIME FOR TOW-IN <%s> <%s> <%s>",pcgCtGhp,"HTPD",argCtValue[1].Value);
            dbg(TRACE,"--------------------------------");
            rgTowDat[2].TowIsValid = TRUE;
            igAutoCreateTowDep = TRUE;
          }
        }
      }
      else
      {
        rgFltDat[1].MaxGrdTime = ilMaxOvnTime;
      }
      dbg(TRACE,"COMPARE GRD TIME: ROTATION=%d MAXIMUM=%d",rgFltDat[1].RotGrdTime,rgFltDat[1].MaxGrdTime);
      if (rgFltDat[1].RotGrdTime > rgFltDat[1].MaxGrdTime)
      {
        dbg(TRACE,"ACTUAL GROUND TIME (%d) EXCEEDS MAXIMUM (%d)",rgFltDat[1].RotGrdTime,rgFltDat[1].MaxGrdTime);

        if ((rgTowDat[1].TowBgnOffs <= 0)&&(strstr(pclChkRuleType,"A") != NULL))
        {
          dbg(TRACE,"NOW LOOKING FOR AN ARRIVAL TOW-OUT RULE");
          llRuleLineNo = SearchTowingRule("A",rgFltDat[1].FltLineNo,FALSE);
          if (llRuleLineNo >= 0)
          {
            CT_GetColumnValueByName(pcgCtGhp, llRuleLineNo, "PRSN", &rgAnyStrg);
            str_trm_rgt(rgAnyStrg.Value, " ", TRUE);
            strcpy(rgTowDat[1].StrTowARule,rgAnyStrg.Value);
            strcpy(rgTowDat[2].StrTowARule,rgAnyStrg.Value);
            CT_GetColumnValueByName(pcgCtGhp, llRuleLineNo, "HTPA", &argCtValue[1]);
            str_trm_rgt(argCtValue[1].Value, " ", TRUE);
            dbg(TRACE,"RULE A <%s> ARRIVAL RULE: TIME OFFSET FOR TOW-OUT <%s> <%s> <%s>",rgAnyStrg.Value,pcgCtGhp,"HTPA",argCtValue[1].Value);
            dbg(TRACE,"------------------------------");
            rgTowDat[1].TowBgnOffs = atoi(argCtValue[1].Value);
            if (rgTowDat[1].TowBgnOffs >= 0)
            {
              if (igTestTowOutTime > 0)
              {
                rgTowDat[1].TowBgnOffs = igTestTowOutTime;
                dbg(TRACE,"###########################################");
                dbg(TRACE,"# TEST ONLY: TowBgnOffs set to %d minutes #",rgTowDat[1].TowBgnOffs);
                dbg(TRACE,"###########################################");
              }
              rgTowDat[1].TowIsValid = TRUE;
              igAutoCreateTowArr = TRUE;
            }
            else
            {
              dbg(TRACE,"TIME OFFSET IS LESS THAN 0 MINUTES");
              dbg(TRACE,"WON'T CREATE TOWING BASED ON THIS RULE");
              igAutoCreateTowArr = FALSE;
            }
          }
          else
          {
            dbg(TRACE,"THERE WAS NO RULE MATCHING THIS ARRIVAL FLIGHT");
            dbg(TRACE,"-------REPEAT SEARCH LOOP: TELL ME WHY--------");
            llRuleLineNo = SearchTowingRule("A",rgFltDat[1].FltLineNo,TRUE);
            dbg(TRACE,"----------------------------------------------");
            igAutoCreateTowArr = FALSE;
          }
        }
        if ((rgTowDat[2].TowEndOffs <= 0)&&(strstr(pclChkRuleType,"D") != NULL))
        {
          dbg(TRACE,"NOW LOOKING FOR A DEPARTURE TOW-IN RULE");
          llRuleLineNo = SearchTowingRule("D",rgFltDat[2].FltLineNo,FALSE);
          if (llRuleLineNo >= 0)
          {
            CT_GetColumnValueByName(pcgCtGhp, llRuleLineNo, "PRSN", &rgAnyStrg);
            str_trm_rgt(rgAnyStrg.Value, " ", TRUE);
            strcpy(rgTowDat[1].StrTowDRule,rgAnyStrg.Value);
            strcpy(rgTowDat[2].StrTowDRule,rgAnyStrg.Value);
            CT_GetColumnValueByName(pcgCtGhp, llRuleLineNo, "HTPD", &argCtValue[1]);
            str_trm_rgt(argCtValue[1].Value, " ", TRUE);
            dbg(TRACE,"RULE D <%s> DEPARTURE RULE: TIME FOR TOW-IN <%s> <%s> <%s>",rgAnyStrg.Value,pcgCtGhp,"HTPD",argCtValue[1].Value);
            dbg(TRACE,"-------------------------------");
            rgTowDat[2].TowEndOffs = atoi(argCtValue[1].Value);
            rgTowDat[2].TowIsValid = TRUE;
            igAutoCreateTowDep = TRUE;
          }
          else
          {
            dbg(TRACE,"THERE WAS NO RULE MATCHING THIS DEPARTURE FLIGHT");
            dbg(TRACE,"-------REPEAT SEARCH LOOP: TELL ME WHY----------");
            llRuleLineNo = SearchTowingRule("D",rgFltDat[2].FltLineNo,TRUE);
            dbg(TRACE,"------------------------------------------------");
            igAutoCreateTowDep = FALSE;
          }
        }
      }
      else
      {
        dbg(TRACE,"ACTUAL GROUND TIME (%d) IS WITHIN MAXIMUM (%d)",rgFltDat[1].RotGrdTime,rgFltDat[1].MaxGrdTime);
      }
    }
    else
    {
      dbg(TRACE,"THERE IS NO RULE MATCHING THIS ROTATION");
      strcpy(rgFltDat[1].StrRulType,"-");
    }
  }
  else
  {
    /* Called in CreateOrUpdateTowings() */
    /* CheckTowingChain(); */
  }

  dbg(TRACE,"-----------------------------------");
  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static long CheckOverNightRule(char *pcpArrTifa, char *pcpDepTifd)
{
  int ilRc = RC_SUCCESS;
  int ilRetVal = 0;
  char pclLocTifa[16] = "";
  char pclLocTifd[16] = "";
  char pclOvnHour[16] = "";
  char pclBgnHour[16] = "";
  char pclEndHour[16] = "";
  char pclOvnTist[16] = "";
  char pclBgnTist[16] = "";
  char pclEndTist[16] = "";
  int ilArrTimeMin = 0;
  int ilDepTimeMin = 0;
  int ilOvnTimeMin = 0;
  int ilBgnTimeMin = 0;
  int ilEndTimeMin = 0;
  int ilMaxOvnTime = 0;
  int ilDummy = 0;

  /* The Rule is:               */
  /* The arrival flight and the */
  /* departure must be within a */
  /* configured time period.    */

  /* If the flight is on ground */
  /* at 03:00AM then it will be */
  /* be considered as OverNight */
  /* and usally not towed out.  */

  /* Meaning of MaxOvnGrdTime:  */
  /* The default value is 1440, */
  /* which avoids the creation  */
  /* of towings, while a short  */
  /* value of MaxOvnGrdTime can */
  /* be used to create towings  */
  /* for some short turnarounds */
  /* as well.                   */

  dbg(TRACE,"CHECK TURNAROUND OVERNIGHT CONDITION");
  dbg(TRACE,"------------------------------------");
  if (igMaxOvnTime > 0)
  {
    ilMaxOvnTime = igMaxOvnTime;
    dbg(TRACE,"CHECK OVERNIGHT PERIOD (LT) %s - %s AT %s",pcgTowArrTime,pcgTowDepTime,pcgTowOvnHour);
    igUtcToLocalDiff = CheckServerTime();
    (void) GetFullDay(pcpArrTifa, &ilDummy, &ilArrTimeMin, &ilDummy);
    (void) GetFullDay(pcpDepTifd, &ilDummy, &ilDepTimeMin, &ilDummy);
    ilArrTimeMin += igUtcToLocalDiff;
    (void) FullTimeString(pclLocTifa, ilArrTimeMin, "00");
    ilDepTimeMin += igUtcToLocalDiff;
    (void) FullTimeString(pclLocTifd, ilDepTimeMin, "00");
    dbg(DEBUG,"ARR TIFA LOC: <%s> MINUTES=%d (UTC: <%s>)",pclLocTifa,ilArrTimeMin,pcpArrTifa);
    dbg(DEBUG,"DEP TIFD LOC: <%s> MINUTES=%d (UTC: <%s>)",pclLocTifd,ilDepTimeMin,pcpDepTifd);

    strcpy(pclBgnHour,pcgTowArrTime);
    strcpy(pclEndHour,pcgTowDepTime);
    strcpy(pclOvnHour,pcgTowOvnHour);

    strcpy(pclBgnTist,pclLocTifa);
    strncpy(&pclBgnTist[8],pclBgnHour,4);

    strcpy(pclEndTist,pclLocTifa);
    strncpy(&pclEndTist[8],pclEndHour,4);

    strcpy(pclOvnTist,pclLocTifa);
    strncpy(&pclOvnTist[8],pclOvnHour,4);

    (void) GetFullDay(pclBgnTist, &ilDummy, &ilBgnTimeMin, &ilDummy);
    (void) GetFullDay(pclOvnTist, &ilDummy, &ilOvnTimeMin, &ilDummy);
    (void) GetFullDay(pclEndTist, &ilDummy, &ilEndTimeMin, &ilDummy);

    if (ilArrTimeMin >= ilBgnTimeMin)
    {
      if (ilOvnTimeMin < ilBgnTimeMin)
      {
        ilOvnTimeMin += 1440;
        (void) FullTimeString(pclOvnTist, ilOvnTimeMin, "00");
      }
      if (ilEndTimeMin < ilBgnTimeMin)
      {
        ilEndTimeMin += 1440;
        (void) FullTimeString(pclEndTist, ilEndTimeMin, "00");
      }
    }
    else
    {
      if (ilBgnTimeMin > ilEndTimeMin)
      {
        ilBgnTimeMin -= 1440;
        (void) FullTimeString(pclBgnTist, ilBgnTimeMin, "00");
      }
    }

    dbg(TRACE,"OVERNIGHT TIME RULE <%s>-(%s)-<%s>",pclBgnTist,pclOvnTist,pclEndTist);

    ilRetVal = -1;
    if ((ilArrTimeMin > ilBgnTimeMin) && (ilArrTimeMin < ilEndTimeMin))
    {
      /* Arrival within the OVN period */
      if ((ilDepTimeMin > ilBgnTimeMin) && (ilDepTimeMin < ilEndTimeMin))
      {
        /* Departure within the OVN period */
        if ((ilOvnTimeMin > ilArrTimeMin) && (ilOvnTimeMin<ilDepTimeMin))
        {
          /* Rotation on Ground at OVN hour */
          dbg(TRACE,"TURNAROUND TO BE HANDLED AS OVERNIGHT");
          ilRetVal = ilMaxOvnTime;
        }
      }
    }
    if (ilRetVal < 0)
    {
      dbg(TRACE,"TURNAROUND NOT TO BE HANDLED AS OVERNIGHT");
      ilRetVal = -1;
    }
  }
  else
  {
    dbg(TRACE,"OVERNIGHT FUNCTION HAS NOT BEEN CONFIGURED.");
    dbg(TRACE,"TURNAROUND TO BE HANDLED BASED ON RULES.");
    ilRetVal = -1;
  }
  return ilRetVal;
}

/* *************************************************** */
/* *************************************************** */
static long SearchTowingRule(char *pcpRuleType, long lpFlightLine, int ipTellMeWhy)
{
  int ilRc = RC_SUCCESS;
  int ilIsFine = TRUE;
  char pclChkRuleType[8] = "";
  char pclCurRuleType[8] = "";
  char pclGhpFldLst[1024] = "";
  char pclAftFldLst[1024] = "";
  char pclGhpFldName[128] = "";
  char pclAftFldList[128] = "";
  char pclAftFldName[128] = "";
  char pclAftFldData[128] = "";
  long llMaxLine = 0;
  long llCurLine = 0;
  int ilGhpFld = 0;
  int ilGhpCnt = 0;
  int ilAftFld = 0;
  int ilAftCnt = 0;
  int ilRuleFound = FALSE;
  int ilRuleMatch = TRUE;
  int ilFieldMatch = TRUE;
  int ilFlightMatch = TRUE;
  int ilRuleCount = 0;
  int ilMax = 0;
  int ilCur = 0;
  int ilRetVal = 0;
  long llAftRecLine = -1;
  long llChkAftArrLine = -1;
  long llChkAftDepLine = -1;
  long llArrRuleLineNo = -1;
  long llDepRuleLineNo = -1;
  long llRotRuleLineNo = -1;
  long llRuleLineNo = -1;

  /* USAGE OF GHPTAB FIELDS: */
  /* PRSN = Rule Short Name  */
  /* PRNA = Rule Long Name   */
  /* PRIO = Priority         */
  /* PRFL = TurnType T/A/D   */
  /* REGN = AFTTAB.REGN      */
  /* ACT5 = AFTTAB.ACT3/ACT5 */
  /* FLCD = AFTTAB.ALC2/ALC3 */
  /* FLND = AFTTAB.FLTN      */
  /* FLSD = AFTTAB.FLNS      */
  /* FLID = AFTTAB.FLTI      */
  /* DEST = AFTTAB.DES3/DES4 */
  /* VI1D = AFTTAB.VIA3/VIA4 */
  /* TTPD = AFTTAB.TTYP      */
  /* TTGD = ACT3/ACT5 GROUP  */
  /* MAXT = Max Turn Time    */
  /* HTPA = Time after ONBL  */
  /* HTPD = Time before OFBL */
  /* SCCA = Create Towing    */
  /* SCCD = Create Grd.Mvmt. */

  dbg(DEBUG,"EVALUATION OF TOWING RULES");
  dbg(DEBUG,"--------------------------");
  strcpy(pclChkRuleType,pcpRuleType);
  llAftRecLine = lpFlightLine;

  dbg(DEBUG,"-----------------------------------");
  dbg(DEBUG,"NOW RUNNING THE RULES LOOP FOR <%s>",pclChkRuleType);
  strcpy(pclCurRuleType,"-");
  ilMax = strlen(pclChkRuleType);
  for (ilCur=0;ilCur<ilMax;ilCur++)
  {
    dbg(TRACE,"============================================");
    pclCurRuleType[0] = pclChkRuleType[ilCur];
    switch(pclCurRuleType[0])
    {
      case 'A':
        dbg(DEBUG,"LOOP %d: CHECK ARRIVAL RULES (TYPE <%s>)",(ilCur+1),pclCurRuleType);
        strcpy(pclGhpFldLst,pcgGhpArrRuleFldLst);
        strcpy(pclAftFldLst,pcgAftArrRuleFldLst);
        break;
      case 'D':
        dbg(DEBUG,"LOOP %d: CHECK DEPARTURE RULES (TYPE <%s>)",(ilCur+1),pclCurRuleType);
        strcpy(pclGhpFldLst,pcgGhpDepRuleFldLst);
        strcpy(pclAftFldLst,pcgAftDepRuleFldLst);
        break;
      case 'T':
        dbg(DEBUG,"LOOP %d: CHECK TURNAROUND RULES (TYPE <%s>)",(ilCur+1),pclCurRuleType);
        strcpy(pclGhpFldLst,pcgGhpRotRuleFldLst);
        strcpy(pclAftFldLst,pcgAftRotRuleFldLst);
      default:
        break;
    }
    dbg(DEBUG,"--------------------------------------------");
    dbg(DEBUG,"GHP FIELDS <%s>",pclGhpFldLst);
    dbg(DEBUG,"AFT FIELDS <%s>",pclAftFldLst);
    dbg(DEBUG,"============================================");


    ilRuleFound = FALSE;
    ilGhpCnt = field_count(pclGhpFldLst);
    llMaxLine = CT_GetLineCount(pcgCtGhp);
    for (llCurLine=0;((llCurLine<llMaxLine)&&(ilRuleFound!=TRUE));llCurLine++)
    {
      CT_GetColumnValueByName(pcgCtGhp, llCurLine, "PRFL", &argCtValue[3]);
      str_trm_rgt(argCtValue[3].Value, " ", TRUE);
      if (strcmp(pclCurRuleType,argCtValue[3].Value) == 0)
      {
        dbg(DEBUG,"FOUND MATCHING RULE TYPE <%s>",argCtValue[3].Value);

        dbg(DEBUG,"----------------------------");
        CT_GetLineValues(pcgCtGhp, llCurLine, &rgCtLine);
        if (ipTellMeWhy == TRUE)
        {
          dbg(TRACE,"===> RULE LINE %2d TYPE <%s>: DATA <%s>", llCurLine, pclCurRuleType, rgCtLine.Value);
          dbg(TRACE,"---> ---------------------");
        }
        else
        {
          dbg(DEBUG,"RULE %d: DATA <%s>", llCurLine, rgCtLine.Value);
          dbg(DEBUG,"----------------------------");
        }
        /* -------------------------------------- */
        /* BOOL CONDITION OF RULE FIELDS IS 'AND' */
        /* -------------------------------------- */
        ilRuleMatch = TRUE;
        for (ilGhpFld=1;((ilGhpFld<=ilGhpCnt)&&(ilRuleMatch==TRUE));ilGhpFld++)
        {
          get_any_item(pclGhpFldName, pclGhpFldLst, ilGhpFld, ",");
          /* dbg(DEBUG,"CHECK FIELD (%d) <%s> OF RULE %d",ilGhpFld,pclGhpFldName,llCurLine); */
          CT_GetColumnValueByName(pcgCtGhp, llCurLine, pclGhpFldName, &argCtValue[1]);
          str_trm_rgt(argCtValue[1].Value, " ", TRUE);
          /* dbg(DEBUG,"<%s> <%s> <%s>",pcgCtGhp,pclGhpFldName,argCtValue[1].Value); */
          if (strlen(argCtValue[1].Value) > 0)
          {
            dbg(DEBUG,"ACTIVE RULE FIELD <%s> VALUE <%s>",pclGhpFldName,argCtValue[1].Value);
            get_any_item(pclAftFldList, pclAftFldLst, ilGhpFld, ",");
            dbg(DEBUG,"RELATED AFT FIELD GROUP <%s>",pclAftFldList);
            if (strcmp(pclAftFldList,"----") != 0)
            {
              /* --------------------------------------- */
              /* BOOL CONDITION OF FLIGHT FIELDS IS 'OR' */
              /* --------------------------------------- */
              ilFieldMatch = FALSE;
              ilAftCnt = CT_CountPattern(pclAftFldList, ";") + 1;
              for (ilAftFld=1;((ilAftFld<=ilAftCnt)&&(ilFieldMatch!=TRUE));ilAftFld++)
              {
                get_any_item(pclAftFldName, pclAftFldList, ilAftFld, ";");
                CT_GetColumnValueByName(pcgCtAft, llAftRecLine, pclAftFldName, &argCtValue[2]);
                str_trm_rgt(argCtValue[2].Value, " ", TRUE);
                if (strlen(argCtValue[2].Value) == 0)
                {
                  strcpy(argCtValue[2].Value,"#");
                }
                dbg(DEBUG,"COMPARE <%s> <%s> WITH <%s> <%s> <%s>",pclGhpFldName,argCtValue[1].Value,pcgCtAft,pclAftFldName,argCtValue[2].Value);
                if (strcmp(argCtValue[1].Value,argCtValue[2].Value) == 0)
                {
                  dbg(DEBUG,"FLIGHT FIELD MATCHES RULE CONDITION");
                  ilFieldMatch = TRUE;
                }
                else
                {
                  if (ipTellMeWhy == TRUE)
                  {
                    dbg(TRACE,"===> NO MATCH <%s> <%s> VERSUS <%s> <%s> <%s>",pclGhpFldName,argCtValue[1].Value,pcgCtAft,pclAftFldName,argCtValue[2].Value);
                  }
                  if ((strcmp(argCtValue[1].Value,"S") == 0)&&(strcmp(argCtValue[2].Value,"PAX") == 0))
                  {
                    dbg(TRACE,"===> QUICK HACK: PAX NATURE MATCHES RULE CONDITION");
                    ilFieldMatch = TRUE;
                  }
                }
              }
            }
            else
            {
              CT_GetColumnValueByName(pcgCtGhp, llCurLine, "REMA", &argCtValue[1]);
              dbg(DEBUG,"<%s> <%s> GROUP DATA <%s>",pcgCtGhp,pclGhpFldName,argCtValue[1].Value);
              /* BOOL CONDITION OF FLIGHT FIELDS IS 'OR' */
              ilFieldMatch = FALSE;
              strcpy(pclAftFldList,"ACT3;ACT5");
              ilAftCnt = CT_CountPattern(pclAftFldList, ";") + 1;
              for (ilAftFld=1;((ilAftFld<=ilAftCnt)&&(ilFieldMatch!=TRUE));ilAftFld++)
              {
                get_any_item(pclAftFldName, pclAftFldList, ilAftFld, ";");
                CT_GetColumnValueByName(pcgCtAft, llAftRecLine, pclAftFldName, &argCtValue[2]);
                str_trm_rgt(argCtValue[2].Value, " ", TRUE);
                if (strlen(argCtValue[2].Value) == 0)
                {
                  strcpy(argCtValue[2].Value,"#");
                }
                dbg(DEBUG,"LOOK UP <%s> <%s> WITH <%s> <%s> <%s>",pclGhpFldName,argCtValue[1].Value,pcgCtAft,pclAftFldName,argCtValue[2].Value);
                sprintf(pclAftFldData,";%s;",argCtValue[2].Value);
                if (strstr(argCtValue[1].Value,pclAftFldData) != NULL)
                {
                  dbg(DEBUG,"FLIGHT FIELD MATCHES RULE CONDITION");
                  ilFieldMatch = TRUE;
                }
                else
                {
                  if (ipTellMeWhy == TRUE)
                  {
                    dbg(DEBUG,"===> NO MATCH <%s> <%s> IN <%s> <%s> <%s>",pclGhpFldName,argCtValue[1].Value,pcgCtAft,pclAftFldName,argCtValue[2].Value);
                  }
                }
              }
            }
            if (ilFieldMatch != TRUE)
            {
              dbg(DEBUG,"FLIGHT FIELD DOES NOT MATCH RULE CONDITION");
              ilRuleMatch = FALSE;
            }
          }
        }
        if (ilRuleMatch == TRUE)
        {
          dbg(TRACE,"FOUND A MATCHING <%s> RULE FOR THIS FLIGHT",pclCurRuleType);
          dbg(TRACE,"=========================================");
          dbg(TRACE,"RULE %d: DATA <%s>", llCurLine, rgCtLine.Value);
          dbg(TRACE,"=========================================");
          ilRuleFound = TRUE;
          ilRuleCount++;
          if (ilRuleCount == 1)
          {
            llRuleLineNo = llCurLine;
          }
          dbg(DEBUG,"(STOP THIS LOOP NOW)");
        }
        else
        {
          dbg(DEBUG,"FLIGHT DOES NOT MATCH RULE CONDITION");
          dbg(DEBUG,"(CONTINUE THIS LOOP)");
        }
      }
    }
    dbg(DEBUG,"FOUND A TOTAL OF %d MATCHING RULES",ilRuleCount);
    dbg(DEBUG,"----------------------------------");
  }
  return llRuleLineNo;
}

/* *************************************************** */
/* *************************************************** */
static int CreateOrUpdateTowings(void)
{
  char pclPstData[16] = "";
  int ilRc = RC_SUCCESS;
  int ilGetRc = 0;
  int ilNewArrTow = FALSE;
  int ilNewDepTow = FALSE;
  int ilUpdTowSked = FALSE;
  int ilCheckAutoTow = FALSE;
  int ilIsDone = 0;
  int ilMinParkTime = 0;
  long llCurLine = 0;

  dbg(TRACE,"-------------------------------------------");
  dbg(TRACE,"STARTING CREATE_OR_UPDATE TOWING EVALUATION");
  dbg(TRACE,"CHECKING THE TOWING ENVIRONMENT CONDITIONS");
  dbg(TRACE,"-------------------------------------------");

  dbg(TRACE,"EXISTING TOWINGS: TOTAL=%d ACTIVE=%d HIDDEN=%d PROTECTED=%d",
             rgFltDat[1].MaxTowCount,rgFltDat[1].ActTowCount,rgFltDat[1].DelTowCount,rgFltDat[1].ProTowCount);

  if (rgTowDat[1].TowBgnOffs > rgFltDat[1].MaxGrdTime)
  {
    dbg(TRACE,"FUNNY RULES: TOW-OUT TIME (%d) IS GREATER THAN MAX GRD TIME (%d)",rgTowDat[1].TowBgnOffs,rgFltDat[1].MaxGrdTime);
  }
  if (rgTowDat[1].TowBgnOffs > rgFltDat[1].RotGrdTime)
  {
    dbg(TRACE,"TOWING RULE: TOW-OUT TIME (%d) IS GREATER THAN ACTUAL GRD TIME (%d)",rgTowDat[1].TowBgnOffs,rgFltDat[1].RotGrdTime);
  }

  if (rgFltDat[1].FltExists == TRUE)
  {
    dbg(TRACE,"FLIGHT ARRIVAL IS AT POS PSTA <%s>",rgFltDat[1].StrFltStnd);
    if (rgFltDat[1].MaxTowCount > 0)
    {
      if (lgFirstUserTowOutLine >= 0)
      {
        dbg(TRACE,"THE FIRST USER TOWING IS FROM PSTD <%s>",rgFltDat[1].StrFltStnd);
      }
      else
      {
        dbg(TRACE,"THE FIRST AUTO TOWING IS FROM PSTD <%s>",rgTowDat[1].StrTowPstd);
        if (rgTowDat[1].TowIsHidden == TRUE)
        {
          dbg(TRACE,"ATTENTION! AUTO TOW-OUT IS HIDDEN (DELETED BY USER)");
        }
      }
      dbg(TRACE,"THE FINAL TOWING GOES TO PSTA <%s>",rgFltDat[1].StrLastPos);
    }
  }
  if (rgFltDat[2].FltExists == TRUE)
  {
    dbg(TRACE,"FLIGHT DEPARTURE IS FROM PSTD <%s>",rgFltDat[2].StrFltStnd);
  }
  dbg(TRACE,"-------------------------------------------");
  dbg(TRACE,"PREPARING THE TOWING EVALUATION DATA VALUES");
  if (rgTowDat[1].TowExists == TRUE)
  {
    if (rgFltDat[1].FltIsOALeg == TRUE)
    {
      ilUpdTowSked = TRUE;
      dbg(TRACE,"OPEN ARRIVAL LEG: ALLOWING UPDATES ON TOWING SKED TIME");
    }
    if (rgTowDat[1].TowBgnOffs <= 0)
    {
      rgTowDat[1].TowBgnOffs = rgTowDat[1].CurBgnOffs;
      dbg(TRACE,"TOW_BGN_OFFS SET TO CUR_BGN_OFFS");
    }
    /*
    if (rgTowDat[1].TowBgnOffs <= 0)
    {
      rgTowDat[1].TowBgnOffs = 90;
      dbg(TRACE,"#############################################");
      dbg(TRACE,"# TOW_BGN_OFFS SET TO INTERNAL DEFAULT (90) #");
      dbg(TRACE,"#############################################");
    }
    */
  }

  dbg(TRACE,"-------------------------------------------");
  dbg(TRACE,"CHECK FOR FLIGHT UPDATES AND GLUE FUNCTION.");
  dbg(TRACE,"(BEFORE CREATING TOWINGS OR UPDATING THEM.)");
  CreateRem2DataList(1, FALSE, FALSE, -1, -1);
  CreateRem2DataList(2, FALSE, FALSE, -1, -1);
  PrintTowingArray(1);
  PrintTowingArray(2);
  ilGetRc = CheckRotationUpdates(0, -1);

  /* Create List of Flight Data */
  CreateRem2DataList(0, FALSE, FALSE, -1, 0);
  /* Check for Updates */
  ilGetRc = CheckRotationUpdates(1, lgFirstUserTowOutLine);
  /* Save List of Flight Data */
  CreateRem2DataList(0, FALSE, FALSE, lgFirstUserTowOutLine, 1);
  dbg(TRACE,"-------------------------------------------");

  if ((rgFltDat[1].FltExists == TRUE)&&(rgTowDat[1].TowExists == TRUE))
  {
    rgTowDat[1].CurBgnOffs = rgTowDat[1].ValTowStod - rgFltDat[1].ValFltTime;
    rgFltDat[1].CurBgnOffs = rgTowDat[1].CurBgnOffs;
    dbg(TRACE,"ARR TOWING.OUT: STAND <%s>\tFLT.TIFA/TOW.STOD <%s> <%s> OFFSET=%d",rgTowDat[1].StrTowPstd,rgFltDat[1].StrFltTime,rgTowDat[1].StrTowStod,rgTowDat[1].CurBgnOffs);
  }
  if ((rgFltDat[2].FltExists == TRUE)&&(rgTowDat[2].TowExists == TRUE))
  {
    rgTowDat[2].CurEndOffs =  rgFltDat[2].ValFltTime - rgTowDat[2].ValTowStoa;
    rgFltDat[2].CurBgnOffs = rgTowDat[2].CurBgnOffs;
    dbg(TRACE,"DEP TOWING.IN : STAND <%s>\tFLT.TIFD/TOW.STOA <%s> <%s> OFFSET=%d",rgTowDat[2].StrTowPsta,rgFltDat[2].StrFltTime,rgTowDat[2].StrTowStoa,rgTowDat[2].CurEndOffs);
  }


  if (rgFltDat[1].RotGrdTime > rgFltDat[1].MaxGrdTime)
  {
    dbg(TRACE,"CREATE OR UPDATE TOWINGS (GRD TIME ACT=%d MAX=%d)",rgFltDat[1].RotGrdTime,rgFltDat[1].MaxGrdTime);
    ilCheckAutoTow = TRUE;
  }
  else if (rgFltDat[1].FltIsOALeg == TRUE)
  {
    dbg(TRACE,"CREATE OR UPDATE TOW_OUT OF OAL (GRD TIME ACT=%d MAX=%d)",rgFltDat[1].RotGrdTime,rgFltDat[1].MaxGrdTime);
    ilCheckAutoTow = TRUE;
  }
  else
  {
    dbg(TRACE,"ACTUAL GROUND TIME (%d) IS WITHIN MAXIMUM (%d)",rgFltDat[1].RotGrdTime,rgFltDat[1].MaxGrdTime);
    dbg(TRACE,"USUALLY TOWINGS ARE NOT REQUIRED IN THIS CASE.");
    ilCheckAutoTow = FALSE;
    if (igAutoCreateTowArr == FALSE)
    {
      if (rgTowDat[1].TowExists == TRUE)
      {
        dbg(TRACE,"BUT THERE IS AN OBSOLETE AUTO_TOW_OUT.");
        if (rgTowDat[1].TowOutIsOfbl == FALSE)
        {
          dbg(TRACE,"THE AUTO_TOW_OUT IS NOT YET OFF BLOCKS.");
          dbg(TRACE,"THUS WE'LL DELETE IT NOW.");
          ilCheckAutoTow = TRUE;
        }
        else
        {
          dbg(TRACE,"THE AUTO_TOW_OUT IS ALREADY OFF BLOCKS.");
          dbg(TRACE,"THUS WE WON'T DELETE IT.");
        }
      }
    }
    if (igAutoCreateTowDep == FALSE)
    {
      if (rgTowDat[2].TowExists == TRUE)
      {
        dbg(TRACE,"BUT THERE IS AN OBSOLETE AUTO_TOW_IN.");
        if (rgTowDat[2].TowOutIsOfbl == FALSE)
        {
          dbg(TRACE,"THE AUTO_TOW_IN IS NOT YET OFF BLOCKS.");
          dbg(TRACE,"THUS WE'LL DELETE IT NOW.");
          ilCheckAutoTow = TRUE;
        }
        else
        {
          dbg(TRACE,"THE AUTO_TOW_IN IS ALREADY OFF BLOCKS.");
          dbg(TRACE,"THUS WE WON'T DELETE IT.");
        }
      }
    }
  }

  if (ilCheckAutoTow == TRUE)
  {
    if (rgFltDat[1].FltExists == TRUE)
    {
      if (rgTowDat[1].TowExists == FALSE)
      {
        dbg(TRACE,"AN AUTOMATED ARR TOW-OUT DOES NOT YET EXIST");
        if (lgFirstUserTowOutLine >= 0)
        {
          dbg(TRACE,"BUT THERE IS A USER DEFINED TOW-OUT!");
          dbg(TRACE,"THUS WE'LL NOT CREATE A NEW TOW-OUT!");
          rgTowDat[1].TowIsValid = FALSE;
        }
        if (igAutoCreateTowArr != TRUE)
        {
          dbg(TRACE,"THERE WAS NO MATCHING RULE CONDITION.");
          dbg(TRACE,"THUS WE'LL NOT CREATE A NEW TOW-OUT!");
          rgTowDat[1].TowIsValid = FALSE;
        }
        if (rgTowDat[1].TowIsValid == TRUE)
        {
          dbg(TRACE,"CREATING NEW ARR.TOW-OUT ACCORDING TO RULES");
          rgTowDat[1].ArrLineNo = rgFltDat[1].FltLineNo;
          rgTowDat[1].FltLineNo = rgFltDat[1].FltLineNo;
          strcpy(rgTowDat[1].StrTowPstd,rgFltDat[1].StrFltStnd);
          rgTowDat[1].ValTowStod = rgTowDat[1].ValFltTime + rgTowDat[1].TowBgnOffs;
          rgTowDat[1].ValTowStoa = rgTowDat[1].ValTowStod + rgTowDat[1].CfgTowTime;
          (void) FullTimeString(rgTowDat[1].StrTowStod, rgTowDat[1].ValTowStod, "00");
          (void) FullTimeString(rgTowDat[1].StrTowStoa, rgTowDat[1].ValTowStoa, "00");
          rgTowDat[1].ValNewStod = rgFltDat[1].ValFltTime + rgTowDat[1].TowBgnOffs;
          rgTowDat[1].ValNewStoa = rgTowDat[1].ValNewStod + rgTowDat[1].CfgTowTime;
          (void) FullTimeString(rgTowDat[1].StrNewStod, rgTowDat[1].ValNewStod, "00");
          (void) FullTimeString(rgTowDat[1].StrNewStoa, rgTowDat[1].ValNewStoa, "00");
          strcpy(rgTowDat[1].StrTowPsta,"???");
          dbg(TRACE,"NEW.ARR TOW-OUT FROM <%s>:\nFLT.ARR <%s> OFFS=%d TOW.DEP <%s> TIME=%d TOW.ARR <%s> AT <%s>",
                     rgTowDat[1].StrTowPstd,rgFltDat[1].StrFltTime,rgTowDat[1].TowBgnOffs,
                     rgTowDat[1].StrTowStod,rgTowDat[1].CfgTowTime,rgTowDat[1].StrTowStoa,rgTowDat[1].StrTowPsta);
          rgTowDat[1].TowIsTowOut = TRUE;
          rgTowDat[1].MyOwnTowRec = TRUE;
          ilNewArrTow = TRUE;
          PrintTowingArray(1);
        }
      }
      else
      {
        dbg(TRACE,"AN AUTOMATED ARR TOW-OUT RECORD EXISTS");
        if (lgFirstUserTowOutLine >= 0)
        {
          dbg(TRACE,"BUT THERE IS A USER DEFINED TOW-OUT TOO!");
          dbg(TRACE,"THUS WE'LL DELETE OUR OWN TOW-OUT NOW!");
          rgTowDat[1].TowIsValid = FALSE;
          rgFltDat[1].DelArrTowOut = TRUE;
        }
        if (igAutoCreateTowArr != TRUE)
        {
          dbg(TRACE,"THERE WAS NO MATCHING RULE CONDITION.");
          dbg(TRACE,"THUS WE'LL DELETE OUR OWN TOW-OUT NOW!");
          rgTowDat[1].TowIsValid = FALSE;
          rgFltDat[1].DelArrTowOut = TRUE;
        }
      }
    }

    if ((rgTowDat[1].TowExists == TRUE) && (rgTowDat[1].TowIsHidden == TRUE))
    {
      if (rgFltDat[2].FltExists == TRUE)
      {
        if (strcmp(rgFltDat[1].StrFltStnd,rgFltDat[2].StrFltStnd) == 0)
        {
          dbg(TRACE,"ARR/DEP ON SAME STAND WITH DELETED AUTO TOW-OUT");
          dbg(TRACE,"REMOTE STAND GLUE FUNCTION DISABLED");
          dbg(TRACE,"CREATION OF AUTO TOW-IN DISABLED");
          igAutoCreateTowDep = FALSE;
          rgTowDat[2].TowIsValid = FALSE;
        }
      }
    }

    if (rgFltDat[2].FltExists == TRUE)
    {
      if (rgTowDat[2].TowExists == FALSE)
      {
        dbg(TRACE,"AN AUTOMATED DEP TOW-IN DOES NOT YET EXIST");
        if (lgLastUserTowInLine >= 0)
        {
          dbg(TRACE,"BUT THERE IS A USER DEFINED TOW-IN!");
          dbg(TRACE,"THUS WE'LL NOT CREATE A NEW TOW-IN!");
          rgTowDat[2].TowIsValid = FALSE;
        }
        if (igAutoCreateTowDep != TRUE)
        {
          dbg(TRACE,"THERE WAS NO MATCHING RULE CONDITION.");
          dbg(TRACE,"THUS WE'LL NOT CREATE A NEW TOW-IN!");
          rgTowDat[2].TowIsValid = FALSE;
        }
        if (rgTowDat[2].TowIsValid == TRUE)
        {
          dbg(TRACE,"CREATING NEW DEP.TOW-IN ACCORDING TO RULES");
          rgTowDat[2].ArrLineNo = rgFltDat[1].FltLineNo;
          rgTowDat[2].FltLineNo = rgFltDat[2].FltLineNo;
          strcpy(rgTowDat[2].StrTowPsta,rgFltDat[2].StrFltStnd);
          rgTowDat[2].ValTowStoa = rgTowDat[2].ValFltTime - rgTowDat[2].TowEndOffs;
          rgTowDat[2].ValTowStod = rgTowDat[2].ValTowStoa - rgTowDat[2].CfgTowTime;
          (void) FullTimeString(rgTowDat[2].StrTowStod, rgTowDat[2].ValTowStod, "00");
          (void) FullTimeString(rgTowDat[2].StrTowStoa, rgTowDat[2].ValTowStoa, "00");
          rgTowDat[2].ValNewStoa = rgFltDat[2].ValFltTime - rgTowDat[2].TowEndOffs;
          rgTowDat[2].ValNewStod = rgTowDat[2].ValNewStoa - rgTowDat[2].CfgTowTime;
          (void) FullTimeString(rgTowDat[2].StrNewStod, rgTowDat[2].ValNewStod, "00");
          (void) FullTimeString(rgTowDat[2].StrNewStoa, rgTowDat[2].ValNewStoa, "00");
          strcpy(rgTowDat[2].StrTowPstd,"???");
          dbg(TRACE,"NEW DEP.TOW-IN  FROM <%s>:\nTOW.DEP <%s> TIME=%d TOW.ARR <%s> AT <%s> OFFS=%d FLT.DEP <%s>",
                     rgTowDat[2].StrTowPstd,rgTowDat[2].StrTowStod,rgTowDat[2].CfgTowTime,rgTowDat[2].StrTowStoa,
                     rgTowDat[2].StrTowPsta,rgTowDat[2].TowEndOffs,rgFltDat[2].StrFltTime);
          rgTowDat[2].TowIsTowIn = TRUE;
          rgTowDat[2].MyOwnTowRec = TRUE;
          ilNewDepTow = TRUE;
          PrintTowingArray(2);
        }
      }
      else
      {
        dbg(TRACE,"AN AUTOMATED DEP TOW-IN RECORD EXISTS");
        if (lgLastUserTowInLine >= 0)
        {
          dbg(TRACE,"BUT THERE IS A USER DEFINED TOW-IN TOO!");
          dbg(TRACE,"THUS WE'LL DELETE OUR OWN TOW-IN NOW!");
          rgTowDat[2].TowIsValid = FALSE;
          rgFltDat[1].DelDepTowIn = TRUE;
        }
        if (igAutoCreateTowDep != TRUE)
        {
          dbg(TRACE,"THERE WAS NO MATCHING RULE CONDITION.");
          dbg(TRACE,"THUS WE'LL DELETE OUR OWN TOW-IN NOW!");
          rgTowDat[2].TowIsValid = FALSE;
          rgFltDat[1].DelDepTowIn = TRUE;
        }
      }
    }

    dbg(TRACE,"------------------------------------------");
    dbg(TRACE,"----- SECTION OF PLAUSIBILITY CHECKS -----");
    dbg(TRACE,"------------------------------------------");

    if ((rgFltDat[1].FltExists == TRUE)&&(rgTowDat[1].TowExists == TRUE))
    {
      dbg(TRACE,"CHECK EXISTING ARR.TOW-OUT FOR UPDATES");
      if (rgTowDat[1].TowIsValid == TRUE)
      {
        dbg(TRACE,"CUR ARR TOW-OUT FROM <%s>:\nFLT.ARR <%s> OFFS=%d TOW.DEP <%s> TIME=%d TOW.ARR <%s> AT <%s>",
                   rgTowDat[1].StrTowPstd,rgFltDat[1].StrFltTime,rgTowDat[1].CurBgnOffs,
                   rgTowDat[1].StrTowStod,rgTowDat[1].ValTowTime,rgTowDat[1].StrTowStoa,rgTowDat[1].StrTowPsta);
        if (ilUpdTowSked == TRUE)
        {
          rgTowDat[1].ValNewStod = rgFltDat[1].ValFltTime + rgTowDat[1].TowBgnOffs;
          rgTowDat[1].ValNewStoa = rgTowDat[1].ValNewStod + rgTowDat[1].CfgTowTime;
          (void) FullTimeString(rgTowDat[1].StrNewStod, rgTowDat[1].ValNewStod, "00");
          (void) FullTimeString(rgTowDat[1].StrNewStoa, rgTowDat[1].ValNewStoa, "00");
          if ((rgTowDat[1].ValNewStod != rgTowDat[1].ValTowStod)||(rgTowDat[1].ValNewStoa != rgTowDat[1].ValTowStoa))
          {
            dbg(TRACE,"NEW ARR TOW-OUT FROM <%s>:\nFLT.ARR <%s> OFFS=%d TOW.DEP <%s> TIME=%d TOW.ARR <%s> AT <%s>",
                     rgTowDat[1].StrTowPstd,rgFltDat[1].StrFltTime,rgTowDat[1].TowBgnOffs,
                     rgTowDat[1].StrNewStod,rgTowDat[1].CfgTowTime,rgTowDat[1].StrNewStoa,rgTowDat[1].StrTowPsta);
          }
        }
        else
        {
          dbg(TRACE,"UPDATE SKED TIMES OF ARR.TOW-OUT: IS SWITCHED OFF");
          strcpy(rgTowDat[1].StrNewStod,rgTowDat[1].StrTowStod);
          strcpy(rgTowDat[1].StrNewStoa,rgTowDat[1].StrTowStoa);
          rgTowDat[1].ValNewStod = rgTowDat[1].ValTowStod;
          rgTowDat[1].ValNewStoa = rgTowDat[1].ValTowStoa;
        }
      }
    }

    if ((rgFltDat[2].FltExists == TRUE)&&(rgTowDat[2].TowExists == TRUE))
    {
      dbg(TRACE,"CHECK EXISTING DEP.TOW-IN FOR UPDATES");
      if (rgTowDat[2].TowIsValid == TRUE)
      {
        dbg(TRACE,"CUR DEP TOW-IN  FROM <%s>:\nTOW.DEP <%s> TIME=%d TOW.ARR <%s> AT <%s> OFFS=%d FLT.DEP <%s>",
                   rgTowDat[2].StrTowPstd,rgTowDat[2].StrTowStod,rgTowDat[2].ValTowTime,rgTowDat[2].StrTowStoa,
                   rgTowDat[2].StrTowPsta,rgTowDat[2].CurEndOffs,rgFltDat[2].StrFltTime);
        if (ilUpdTowSked == TRUE)
        {
          rgTowDat[2].ValNewStoa = rgFltDat[2].ValFltTime - rgTowDat[2].TowEndOffs;
          rgTowDat[2].ValNewStod = rgTowDat[2].ValNewStoa - rgTowDat[2].CfgTowTime;
          (void) FullTimeString(rgTowDat[2].StrNewStod, rgTowDat[2].ValNewStod, "00");
          (void) FullTimeString(rgTowDat[2].StrNewStoa, rgTowDat[2].ValNewStoa, "00");
          if ((rgTowDat[2].ValNewStod != rgTowDat[2].ValTowStod)||(rgTowDat[2].ValNewStoa != rgTowDat[2].ValTowStoa))
          {
            dbg(TRACE,"UPD DEP TOW-IN  FROM <%s>:\nTOW.DEP <%s> TIME=%d TOW.ARR <%s> AT <%s> OFFS=%d FLT.DEP <%s>",
                     rgTowDat[2].StrTowPstd,rgTowDat[2].StrNewStod,rgTowDat[2].CfgTowTime,rgTowDat[2].StrNewStoa,
                     rgTowDat[2].StrTowPsta,rgTowDat[2].TowEndOffs,rgFltDat[2].StrFltTime);
          }
        }
        else
        {
          dbg(TRACE,"UPDATE SKED TIMES OF TOW-IN: IS SWITCHED OFF");
          strcpy(rgTowDat[2].StrNewStod,rgTowDat[2].StrTowStod);
          strcpy(rgTowDat[2].StrNewStoa,rgTowDat[2].StrTowStoa);
          rgTowDat[2].ValNewStod = rgTowDat[2].ValTowStod;
          rgTowDat[2].ValNewStoa = rgTowDat[2].ValTowStoa;
        }
      }
    }

    if ((rgFltDat[2].FltExists == FALSE)&&(rgTowDat[2].TowExists == TRUE))
    {
      dbg(TRACE,"WE DON'T HAVE A DEPARTURE FLIGHT BUT AN EXISTING TOW-IN");
      dbg(TRACE,"(SOLUTION TO BE IMPLEMENTED)");
      dbg(TRACE,"SET DELETION FLAG FOR TOW-IN: TOW-IN WITHOUT DEP FLIGHT");
      rgFltDat[1].DelDepTowIn = TRUE;
      rgTowDat[2].TowIsValid = FALSE;
      ilNewDepTow = FALSE;
    }

    if ((rgTowDat[1].TowIsValid == TRUE)&&(rgTowDat[2].TowIsValid == TRUE))
    {
      dbg(TRACE,"CHECK CONFLICTS OF BOTH: VALID TOW-OUT VERSUS VALID TOW-IN");
      rgTowDat[1].ValTowPark = rgTowDat[2].ValTowStod - rgTowDat[1].ValTowStoa;
      dbg(TRACE,"TIME DIFFERENCE (STAND OCCUPATION TIME) WOULD BE %d MINUTES",rgTowDat[1].ValTowPark);
      ilMinParkTime = igMinPosTime;
      if (rgTowDat[1].ValTowPark < ilMinParkTime)
      {
        dbg(TRACE,"OCCUPATION TIME (%d) WOULD BE LESS THAN MINIMUM (%d)",rgTowDat[1].ValTowPark,ilMinParkTime);
      }
      if (rgTowDat[1].ValTowStoa > rgTowDat[2].ValTowStod)
      {
        dbg(TRACE,"SET DELETION FLAG FOR TOW-OUT: TOW-OUT HAPPENS AFTER TOW-IN");
        rgFltDat[1].DelArrTowOut = TRUE;
        rgTowDat[1].TowIsValid = FALSE;
        ilNewArrTow = FALSE;
        if (ilNewDepTow == TRUE)
        {
          strcpy(rgTowDat[2].StrTowPstd,rgTowDat[1].StrTowPstd);
          rgTowDat[2].TowIsTowOut = TRUE;
          rgTowDat[2].TowOutAndIn = TRUE;
        }
        if (rgTowDat[2].TowIsValid == TRUE)
        {
          rgFltDat[1].FltGrdTime = rgTowDat[2].ValTowStod - rgFltDat[1].ValFltTime;
          dbg(TRACE,"FLIGHT GRD TIME AFTER ARRIVAL IS %d MINUTES UNTIL TOW-IN",rgFltDat[1].FltGrdTime);
        }
      }

      if (rgTowDat[1].TowIsValid == TRUE)
      {
        dbg(TRACE,"CHECK CONFLICTS OF VALID TOW-OUT (STEP 1)");
        rgTowDat[1].ValTowPark = rgTowDat[2].ValNewStod - rgTowDat[1].ValNewStoa;
        dbg(TRACE,"STAND OCCUPATION TIME ON <%s> IS %d MINUTES",rgTowDat[1].StrTowPsta,rgTowDat[1].ValTowPark);
        ilMinParkTime = igMinPosTime;
        if (rgTowDat[1].ValTowPark < ilMinParkTime)
        {
          dbg(TRACE,"STAND OCCUPATION TIME (%d) IS LESS THAN MINIMUM (%d)",rgTowDat[1].ValTowPark,ilMinParkTime);
        }
        if (rgTowDat[1].ValNewStoa > rgTowDat[2].ValNewStod)
        {
          dbg(TRACE,"UPDATE ON FIRST TOWING (TOW-OUT) DISABLED: TOW-OUT HAPPENS AFTER TOW-IN");
          rgFltDat[1].DelArrTowOut = TRUE;
          rgTowDat[1].ValNewStod = rgTowDat[1].ValTowStod;
          rgTowDat[1].ValNewStoa = rgTowDat[1].ValTowStoa;
          strcpy(rgTowDat[1].StrNewStod,rgTowDat[1].StrTowStod);
          strcpy(rgTowDat[1].StrNewStoa,rgTowDat[1].StrTowStoa);
          if (rgTowDat[2].TowIsValid == TRUE)
          {
            rgFltDat[1].FltGrdTime = rgTowDat[2].ValNewStod - rgFltDat[1].ValFltTime;
            dbg(TRACE,"FLIGHT GRD TIME AFTER ARRIVAL IS %d MINUTES UNTIL TOW-IN",rgFltDat[1].FltGrdTime);
          }
        }
      }
    }

    if (rgTowDat[1].TowIsValid == TRUE)
    {
      dbg(TRACE,"CHECK CONFLICTS OF VALID TOW-OUT (STEP 2)");
      rgFltDat[1].FltGrdTime = rgTowDat[1].ValTowStod - rgFltDat[1].ValFltTime;
      dbg(TRACE,"CURRENT FLIGHT GRD TIME AFTER ARRIVAL(TIFA) IS %d MINUTES UNTIL TOW-OUT",rgFltDat[1].FltGrdTime);
      rgFltDat[1].FltGrdTime = rgTowDat[1].ValNewStod - rgFltDat[1].ValFltTime;
      dbg(TRACE,"UPDATED FLIGHT GRD TIME AFTER ARRIVAL WOULD BE %d MINUTES UNTIL TOW-OUT",rgFltDat[1].FltGrdTime);
      if (strcmp(rgFltDat[1].StrFltStnd,rgTowDat[1].StrTowPstd) != 0)
      {
        dbg(TRACE,"WARNING: FIRST TOWING.PSTD NOT MATCHING ARR FLIGHT.PSTA");
      }
      if (strcmp(rgTowDat[1].StrTowPsta,"???") == 0)
      {
        if (rgFltDat[1].FltIsOALeg == FALSE)
        {
          dbg(TRACE,"TOW-OUT IS VALID AND MUST BE ALLOCATED FOR ROTATION");
          GetBestRemoteStand(rgTowDat[1].StrTowPstd, rgTowDat[2].StrTowPsta, rgTowDat[1].StrTowStoa, rgTowDat[2].StrTowStod, pcgRmtOutPos);
        }
        else
        {
          dbg(TRACE,"TOW-OUT IS VALID AND MUST BE ALLOCATED FOR OPEN ARR LEG");
          dbg(TRACE,"FLIGHT IS OPEN ARR LEG (TOW-IN IS NOT VALID)");
          dbg(TRACE,"(SETTING A FEW VALUES AS FAKE TOW-IN FOR STAND LOOK-UP)");
          rgTowDat[2].ValTowStod = rgTowDat[1].ValTowStoa + rgFltDat[1].RotGrdTime;
          (void) FullTimeString(rgTowDat[2].StrTowStod, rgTowDat[2].ValTowStod, "00");
          (void) FullTimeString(rgTowDat[2].StrTowStoa, rgTowDat[2].ValTowStod, "00");
          strcpy(rgTowDat[2].StrTowPstd,rgTowDat[1].StrTowPsta);
          strcpy(rgTowDat[2].StrTowPsta,"---");
          strcpy(rgTowDat[2].StrNewStod,rgTowDat[2].StrTowStod);
          strcpy(rgTowDat[2].StrNewStoa,rgTowDat[2].StrTowStoa);
          dbg(TRACE,"SIM ARR TOW-OUT  FROM <%s>: SIM.DEP <%s> SIM.ARR <%s> ",
                     rgTowDat[2].StrTowPstd,rgTowDat[2].StrTowStod,rgTowDat[2].StrTowStoa);

          GetBestRemoteStand(rgTowDat[1].StrTowPstd, rgTowDat[2].StrTowPsta, rgTowDat[1].StrTowStoa, rgTowDat[2].StrTowStod, pcgRmtOutPos);

        }
        strcpy(rgTowDat[1].StrTowPsta,pcgRmtOutPos);
      }
    }

    if ((rgTowDat[2].TowIsValid == TRUE)&&(rgFltDat[2].FltExists == TRUE))
    {
      dbg(TRACE,"CHECK CONFLICTS OF VALID TOW-IN WITH DEPARTURE FLIGHT");
      if (strcmp(rgFltDat[2].StrFltStnd,rgTowDat[2].StrTowPsta) != 0)
      {
        dbg(TRACE,"WARNING: LAST TOWING.PSTA NOT MATCHING DEP FLIGHT.PSTD");
      }
      if (rgTowDat[2].ValTowStod < rgFltDat[1].ValFltTime)
      {
        dbg(TRACE,"LAST TOWING (TOW-IN) DISABLED: TOW-IN STARTS BEFORE FLIGHT.ARRIVAL");
        ilNewDepTow = FALSE;
        rgTowDat[2].TowIsValid = FALSE;
      }
      if (rgTowDat[2].ValNewStod < rgFltDat[1].ValFltTime)
      {
        dbg(TRACE,"UPDATE ON LAST TOWING (TOW-IN) DISABLED: TOW-IN STARTS BEFORE FLIGHT.ARRIVAL");
        rgTowDat[2].ValNewStod = rgTowDat[2].ValTowStod;
        rgTowDat[2].ValNewStoa = rgTowDat[2].ValTowStoa;
        strcpy(rgTowDat[2].StrNewStod,rgTowDat[2].StrTowStod);
        strcpy(rgTowDat[2].StrNewStoa,rgTowDat[2].StrTowStoa);
        rgTowDat[2].ValNewStod = rgTowDat[2].ValTowStod;
        rgTowDat[2].ValNewStoa = rgTowDat[2].ValTowStoa;
      }
      if ((strcmp(rgTowDat[2].StrTowPstd,"???") == 0)&&(rgTowDat[1].TowIsValid == TRUE))
      {
        strcpy(rgTowDat[2].StrTowPstd,rgTowDat[1].StrTowPsta);
      }
    }

    if ((rgTowDat[1].TowIsValid == TRUE)&&(rgTowDat[1].TowExists == TRUE))
    {
      dbg(TRACE,"ARRIVAL STAND GLUE FUNCTION FOR TOW-OUT");
      strcpy(rgTowDat[1].StrTowPstd,rgFltDat[1].StrFltStnd);
      llCurLine = rgTowDat[1].TowLineNo;
      UpdateGridData(pcgCtAft, llCurLine, "PSTD", rgTowDat[1].StrTowPstd);
    }

    if ((rgTowDat[1].TowIsValid == TRUE)&&(rgTowDat[2].TowIsValid == TRUE))
    {
      dbg(TRACE,"FLIGHT ROTATION REMOTE STAND GLUE FUNCTION FOR TOW-IN");
      llCurLine = rgTowDat[2].TowLineNo;
      if (rgFltDat[1].UsrTowCount == 0)
      {
        if (rgTowDat[1].TowIsHidden != TRUE)
        {
          strcpy(rgTowDat[2].StrTowPstd,rgTowDat[1].StrTowPsta);
        }
        else
        {
          dbg(TRACE,"USING ARR.FLIGHT.PSTA (AUTO TOW-OUT IS HIDDEN)");
          strcpy(rgTowDat[2].StrTowPstd,rgTowDat[1].StrTowPstd);
          /* strcpy(rgTowDat[2].StrTowPstd,rgFltDat[1].StrFltStnd); */
        }
        UpdateGridData(pcgCtAft, llCurLine, "PSTD", rgTowDat[2].StrTowPstd);
      }
    }

    if ((rgTowDat[2].TowIsValid == TRUE)&&(rgTowDat[2].TowExists == TRUE))
    {
      if (rgFltDat[2].FltExists == TRUE)
      {
        dbg(TRACE,"FLIGHT DEPARTURE STAND GLUE FUNCTION FOR TOW-IN");
        llCurLine = rgTowDat[2].TowLineNo;
        strcpy(rgTowDat[2].StrTowPsta,rgFltDat[2].StrFltStnd);
        UpdateGridData(pcgCtAft, llCurLine, "PSTA", rgTowDat[2].StrTowPsta);
        if (rgTowDat[2].TowOutAndIn == TRUE)
        {
          dbg(TRACE,"ARRIVAL STAND GLUE FUNCTION FOR TOW-IN");
          strcpy(rgTowDat[2].StrTowPstd,rgFltDat[1].StrFltStnd);
          UpdateGridData(pcgCtAft, llCurLine, "PSTD", rgTowDat[2].StrTowPstd);
        }
      }
    }


    ilIsDone = 0;
    if (ilNewArrTow == TRUE)
    {
      dbg(TRACE,"NOW CREATING ARR TOWING (TOW-OUT) RECORD");
      CreateTowingRecord(1);
      ilIsDone++;
    }
    if (ilNewDepTow == TRUE)
    {
      dbg(TRACE,"NOW CREATING DEP TOWING (TOW-IN) RECORD");
      CreateTowingRecord(2);
      ilIsDone++;
    }

    dbg(TRACE,"NOW CHECKING CONDITIONS FOR AUTO CLEARING (DELETION)");

    if (rgTowDat[1].TowIsValid == TRUE)
    {
      dbg(TRACE,"THE ARR TOW-OUT IS STILL VALID - CHECKING FOR ERRORS");
      dbg(TRACE,"COMPARE TOW-OUT STANDS");
      if (strcmp(rgFltDat[1].StrFltStnd,rgTowDat[1].StrTowPsta) == 0)
      {
        dbg(TRACE,"ERROR: TOW-OUT.PSTA MATCHES ARR-FLT.PSTA (TOW-OUT TO ARR STAND)");
        dbg(TRACE,"(AUTO_TOW WILL CLEAR THIS TOWING AUTOMATICALLY)");
        rgFltDat[1].DelArrTowOut = TRUE;
      }
      if (strcmp(rgTowDat[1].StrTowPstd,rgTowDat[1].StrTowPsta) == 0)
      {
        dbg(TRACE,"ERROR: TOW-OUT.PSTA MATCHES TOW-OUT.PSTD (TOW-OUT FROM/TO SAME STAND)");
        dbg(TRACE,"(AUTO_TOW WILL CLEAR THIS TOWING AUTOMATICALLY)");
        rgFltDat[1].DelArrTowOut = TRUE;
      }
      if (rgTowDat[1].ValTowPark < ilMinParkTime)
      {
        dbg(TRACE,"STAND OCCUPATION TIME (%d) IS LESS THAN MINIMUM (%d)",rgTowDat[1].ValTowPark,ilMinParkTime);
        dbg(TRACE,"(AUTO_TOW WILL CLEAR THIS TOWING AUTOMATICALLY)");
        rgFltDat[1].DelArrTowOut = TRUE;
      }
      if ((rgTowDat[2].TowIsValid == TRUE)&&(rgFltDat[2].FltExists == TRUE))
      {
        if (rgTowDat[1].ValTowStoa > rgTowDat[2].ValTowStod)
        {
          dbg(TRACE,"FIRST TOWING TOW-OUT HAPPENS AFTER TOW-IN");
          dbg(TRACE,"(AUTO_TOW WILL CLEAR THIS TOWING AUTOMATICALLY)");
          rgFltDat[1].DelArrTowOut = TRUE;
        }
      }
    }

    if ((rgTowDat[2].TowIsValid == TRUE)&&(rgFltDat[2].FltExists == TRUE))
    {
      dbg(TRACE,"THE DEP TOW-IN IS STILL VALID - CHECKING FOR ERRORS");
      dbg(TRACE,"COMPARE TOW-IN STANDS");
      if (strcmp(rgFltDat[2].StrFltStnd,rgTowDat[2].StrTowPstd) == 0)
      {
        dbg(TRACE,"ERROR: TOW-IN.PSTD MATCHES DEP-FLT.PSTD (TOW-IN FROM DEP STAND)");
        dbg(TRACE,"(AUTO_TOW WILL CLEAR THIS TOWING AUTOMATICALLY)");
        rgFltDat[1].DelDepTowIn = TRUE;
      }
      if (strcmp(rgTowDat[2].StrTowPstd,rgTowDat[2].StrTowPsta) == 0)
      {
        dbg(TRACE,"ERROR: TOW-IN.PSTA MATCHES TOW-IN.PSTD (TOW-IN FROM/TO SAME STAND)");
        dbg(TRACE,"(AUTO_TOW WILL CLEAR THIS TOWING AUTOMATICALLY)");
        rgFltDat[1].DelDepTowIn = TRUE;
      }
      if ((rgTowDat[2].TowExists == TRUE)&&(rgTowDat[2].CurEndOffs < 10))
      {
        dbg(TRACE,"FUNNY TOWING: LAST TOWING (TOW-OUT OR TOW-IN) TIMING DOES NOT MAKE SENSE");
        dbg(TRACE,"(AUTO_TOW WILL CLEAR THIS TOWING AUTOMATICALLY)");
        rgFltDat[1].DelDepTowIn = TRUE;
      }
    }

    if ((rgTowDat[1].TowIsValid == TRUE)&&(rgTowDat[2].TowIsValid == TRUE))
    {
      dbg(TRACE,"BOTH ARR TOW-OUT AND DEP TOW-IN ARE STILL VALID - CHECKING FOR ERRORS");
      dbg(TRACE,"CHECK TIMINGS");
      rgTowDat[1].ValTowPark = rgTowDat[2].ValTowStod - rgTowDat[1].ValTowStoa;
      dbg(TRACE,"ACTUAL STAND OCCUPATION TIME ON <%s> IS %d MINUTES",rgTowDat[1].StrTowPsta,rgTowDat[1].ValTowPark);
      ilMinParkTime = igMinPosTime;
      if (rgTowDat[1].ValTowPark < ilMinParkTime)
      {
        dbg(TRACE,"STAND OCCUPATION TIME (%d) IS LESS THAN MINIMUM (%d)",rgTowDat[1].ValTowPark,ilMinParkTime);
        dbg(TRACE,"(AUTO_TOW WILL CLEAR THIS TOWING AUTOMATICALLY)");
        rgFltDat[1].DelArrTowOut = TRUE;
      }
      if (rgTowDat[1].ValTowStoa > rgTowDat[2].ValTowStod)
      {
        dbg(TRACE,"FIRST TOWING (TOW-OUT) DISABLED: TOW-OUT HAPPENS AFTER TOW-IN");
        dbg(TRACE,"(AUTO_TOW WILL CLEAR THIS TOWING AUTOMATICALLY)");
        rgFltDat[1].DelArrTowOut = TRUE;
        rgFltDat[1].FltGrdTime = rgTowDat[2].ValTowStod - rgFltDat[1].ValFltTime;
        dbg(TRACE,"FLIGHT GRD TIME AFTER ARRIVAL IS %d MINUTES UNTIL TOW-IN",rgFltDat[1].FltGrdTime);
      }
      if (rgFltDat[1].DelArrTowOut == TRUE)
      {
        strcpy(rgTowDat[2].StrTowPstd,rgFltDat[1].StrFltStnd);
        rgTowDat[2].TowIsTowOut = TRUE;
        rgTowDat[2].TowOutAndIn = TRUE;
      }
    }

    dbg(TRACE,"NOW CHECKING THE COMPLETENESS OF MY TOWINGS");
    if ((rgTowDat[1].TowIsValid == FALSE)&&(rgTowDat[2].TowIsValid == FALSE))
    {
      dbg(TRACE,"OOOOPS: BOTH ARR TOW-OUT AND DEP TOW-IN ARE NO LONGER VALID");
      dbg(TRACE,"CHECK CONDITIONS HOW TO FIX IT AUTOMATICALLY");
      if ((rgTowDat[1].TowExists == TRUE)&&(rgTowDat[2].TowExists == TRUE))
      {
        dbg(TRACE,"BOTH ARR TOW-OUT AND DEP TOW-IN ARE VALID RECORDS IN AFTTAB");
        if ((rgFltDat[1].DelArrTowOut == TRUE)&&(rgFltDat[1].DelDepTowIn == TRUE))
        {
          dbg(TRACE,"BOTH ARR TOW-OUT AND DEP TOW-IN SHALL BE DELETED");
        }
        else if (rgFltDat[1].DelArrTowOut == TRUE)
        {
          dbg(TRACE,"THE ARR TOW-OUT SHALL BE DELETED");
        }
        else if (rgFltDat[1].DelDepTowIn == TRUE)
        {
          dbg(TRACE,"THE DEP TOW-IN SHALL BE DELETED");
          if (rgFltDat[2].FltExists == TRUE)
          {
            dbg(TRACE,"THE DEPARTURE FLIGHT EXISTS");
          }
          else
          {
            dbg(TRACE,"THERE IS NO DEPARTURE FLIGHT");
          }
        }
      }
      else if (rgTowDat[1].TowExists == TRUE)
      {
        dbg(TRACE,"WE HAVE ONLY AN EXISTING ARR TOW-OUT IN AFTTAB");
        if (rgFltDat[1].DelArrTowOut == TRUE)
        {
          dbg(TRACE,"THE ARR TOW-OUT SHALL BE DELETED");
        }
      }
      else if (rgTowDat[2].TowExists == TRUE)
      {
        dbg(TRACE,"WE HAVE ONLY AN EXISTING DEP TOW-IN IN AFTTAB");
        if (rgFltDat[1].DelDepTowIn == TRUE)
        {
          dbg(TRACE,"THE DEP TOW-IN SHALL BE DELETED");
          if (rgFltDat[2].FltExists == TRUE)
          {
            dbg(TRACE,"THE DEPARTURE FLIGHT EXISTS");
          }
          else
          {
            dbg(TRACE,"THERE IS NO DEPARTURE FLIGHT");
          }
        }
      }
      dbg(TRACE,"NOTE: RESULT OF COMPLETENESS CHECK TO BE IMPLEMENTED");
    }


    dbg(TRACE,"=> ----------------------------------------");
    dbg(TRACE,"=> DISPLAY OF CURRENT TOWING STRUCTURES");
    CreateRem2DataList(1, TRUE, TRUE, -1, -1);
    CreateRem2DataList(2, TRUE, TRUE, -1, -1);
    PrintTowingArray(1);
    PrintTowingArray(2);

    /* Create List of Flight Data (FOR FINAL UTWO/UTWI) */
    CreateRem2DataList(0, FALSE, FALSE, -1, 0);
    /* Save List of Flight Data (FOR FINAL UTWO/UTWI) */
    CreateRem2DataList(0, FALSE, FALSE, lgFirstUserTowOutLine, 1);
    CreateRem2DataList(0, FALSE, FALSE, lgLastUserTowInLine, 1);
    /* STRIP OUT FINAL UTWO/UTWI */
    CreateRem2DataList(0, FALSE, FALSE, -1, 2);

    dbg(TRACE,"NOW CHECKING THE CHAIN OF ALL TOWINGS");
    CheckTowingChain();

    if (ilIsDone > 0)
    {
      ilIsDone = 0;

      if (ilNewArrTow == TRUE)
      {
        if (rgFltDat[1].DelArrTowOut == FALSE)
        {
          dbg(TRACE,"NOW INSERTING ARR TOWING (TOW-OUT) TO DB");
          InsertTowingRecord2DB(1);
          ilIsDone++;
        }
      }
      if (ilNewDepTow == TRUE)
      {
        if (rgFltDat[1].DelDepTowIn == FALSE)
        {
          dbg(TRACE,"NOW INSERTING DEP TOWING (TOW-IN) TO DB");
          InsertTowingRecord2DB(2);
          ilIsDone++;
        }
      }
    }

    if (ilIsDone > 0)
    {
      if (strstr(pcgTowRkeyList,pcgCurTowRkey) == NULL)
      {
        strcat(pcgTowRkeyList,pcgCurTowRkey);
        strcat(pcgTowRkeyList,",");
        if (strcmp(pcgTowRacTifa,pcgCurRacTifa) > 0)
        {
          dbg(TRACE,"PRV.TIFA <%s> WAS GREATER THAN CUR.TIFA <%s>",pcgTowRacTifa,pcgCurRacTifa);
          strcpy(pcgTowRacTifa,pcgCurRacTifa);
        }
        if (strcmp(pcgTowRacTifd,pcgCurRacTifd) < 0)
        {
          dbg(TRACE,"PRV.TIFD <%s> WAS LESS THAN CUR.TIFD <%s>",pcgTowRacTifd,pcgCurRacTifd);
          strcpy(pcgTowRacTifd,pcgCurRacTifd);
        }
      }
    }
    else
    {
      dbg(TRACE,"CHECK INSERTS: THERE WAS NOTHING TO BE CREATED");
    }
  }
  else
  {

    CheckTowingChain();

  }
  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static int CreateTowingRecord(int ipTowIdx)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  char pclNewData[1024] = "";
  char pclNewUrno[16] = "";
  char pclSqlTab[16] = "";
  char pclSqlFld[1024] = "";
  char pclSqlVal[1024] = "";
  char pclSqlBuf[1024] = "";
  long llFltLineNo = 0;
  long llTowLineNo = 0;
  short slCursor = 0;
  short slFkt = 0;
  dbg(TRACE,"=====================================================");
  llFltLineNo = rgTowDat[ipTowIdx].ArrLineNo;
  CT_GetLineValues(pcgCtAft, llFltLineNo, &rgCtLine);
  dbg(TRACE,"COPY ARRIVAL FLIGHT AS TOWING\n<%s>",rgCtLine.Value);
  llTowLineNo = CT_InsertTextLine(pcgCtAft,rgCtLine.Value);
  rgTowDat[ipTowIdx].TowLineNo = llTowLineNo;
  dbg(DEBUG,"NEW TOW GRID LINENO. IS %d",llTowLineNo);
  /*pcgCtAftFldLst[] = "FTYP,ADID,FLNO,ALC2,ALC3,FLTN,FLNS,FLTI,CSGN,TTYP,
                        REGN,ACT3,ACT5,ORG3,ORG4,DES3,DES4,VIA3,VIA4,
                        STOD,STOA,TIFD,TISD,TIFA,TISA,PSTD,PSTA,
                        OFBL,ONBL,PABS,PAES,PDBS,PDES,PABA,PAEA,PDBA,PDEA,
                        RKEY,AURN,URNO";*/
   /*
  GetNextValues(pclNewUrno,1);
  UFIS-1485
  */
  DB_LoadUrnoList(1,pclNewUrno,pcgTableKey);

  dbg(TRACE,"NEW URNO IS <%s>",pclNewUrno);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "URNO", pclNewUrno);

  dbg(TRACE,"NOW PATCHING ARR FLIGHT RECORD AS TOWING");
  dbg(TRACE,"=====================================================");

  strcpy(pclNewData,"T");
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "FTYP", pclNewData);

  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "ADID", "B");

  CT_GetColumnValueByName(pcgCtAft, llFltLineNo, "URNO", &argCtValue[1]);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "AURN", argCtValue[1].Value);

  CT_GetColumnValueByName(pcgCtAft, llFltLineNo, "DES3", &argCtValue[1]);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "ORG3", argCtValue[1].Value);
  CT_GetColumnValueByName(pcgCtAft, llFltLineNo, "DES4", &argCtValue[1]);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "ORG4", argCtValue[1].Value);

  strcpy(pclNewData," ");
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "FLTI", pclNewData);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "VIA3", pclNewData);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "VIA4", pclNewData);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "OFBL", pclNewData);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "ONBL", pclNewData);

  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "PABS", pclNewData);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "PAES", pclNewData);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "PABA", pclNewData);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "PAEA", pclNewData);

  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "PDBS", pclNewData);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "PDES", pclNewData);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "PDBA", pclNewData);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "PDEA", pclNewData);

  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "STAT", pclNewData);


  if (strlen(rgTowDat[ipTowIdx].StrTowPstd) == 0)
  {
    strcpy(rgTowDat[ipTowIdx].StrTowPstd," ");
  }
  if (strlen(rgTowDat[ipTowIdx].StrTowPsta) == 0)
  {
    strcpy(rgTowDat[ipTowIdx].StrTowPsta," ");
  }
  if (strcmp(rgTowDat[ipTowIdx].StrTowPsta,"#PD#") == 0)
  {
    strcpy(rgTowDat[ipTowIdx].StrTowPsta," ");
  }
  if (strcmp(rgTowDat[ipTowIdx].StrTowPsta,"#PA#") == 0)
  {
    strcpy(rgTowDat[ipTowIdx].StrTowPsta," ");
  }
  dbg(TRACE,"TOW.PSTD <%s> TOW.STOD <%s> TOW.STOA <%s> TOW.PSTA <%s>",
             rgTowDat[ipTowIdx].StrTowPstd,rgTowDat[ipTowIdx].StrTowStod,
             rgTowDat[ipTowIdx].StrTowStoa,rgTowDat[ipTowIdx].StrTowPsta);

  strcpy(pclNewData,rgTowDat[ipTowIdx].StrTowPstd);
  if (strcmp(pclNewData,"#PD#") == 0)
  {
    strcpy(pclNewData," ");
  }
  if (strcmp(pclNewData,"#PA#") == 0)
  {
    strcpy(pclNewData," ");
  }
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "PSTD", pclNewData);
  strcpy(pclNewData,rgTowDat[ipTowIdx].StrTowPsta);
  if (strcmp(pclNewData,"#PD#") == 0)
  {
    strcpy(pclNewData," ");
  }
  if (strcmp(pclNewData,"#PA#") == 0)
  {
    strcpy(pclNewData," ");
  }
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "PSTA", pclNewData);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "STOD", rgTowDat[ipTowIdx].StrTowStod);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "TIFD", rgTowDat[ipTowIdx].StrTowStod);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "STOA", rgTowDat[ipTowIdx].StrTowStoa);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "TIFA", rgTowDat[ipTowIdx].StrTowStoa);

  strcpy(pclNewData,"S");
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "TISD", pclNewData);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "TISA", pclNewData);

/*
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "REM2", rgTowDat[ipTowIdx].StrRem2New);
*/
  if (ipTowIdx == 1)
  {
    CT_SetLineDbInsFlag(pcgCtAft, llTowLineNo, 1);
    strcpy(pclNewData,"AutoTowArr");
    CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "USEC", pclNewData);
    CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "USEU", pclNewData);
    strcpy(pclNewData,"TOW-OUT");
    CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "CSGN", pclNewData);
  }
  else if (ipTowIdx == 2)
  {
    CT_SetLineDbInsFlag(pcgCtAft, llTowLineNo, 1);
    strcpy(pclNewData,"AutoTowDep");
    CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "USEC", pclNewData);
    CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "USEU", pclNewData);
    strcpy(pclNewData,"TOW-IN");
    CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "CSGN", pclNewData);
  }
  else
  {
    /* For future use */
  }
  GetServerTimeStamp("UTC",1,0,pclNewData);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "CDAT", pclNewData);
  CT_SetColumnValueByName(pcgCtAft, llTowLineNo, "LSTU", pclNewData);

  CT_GetLineValues(pcgCtAft, llTowLineNo, &rgCtLine);
  dbg(TRACE,"NEW TOWING RECORD\n<%s>",rgCtLine.Value);
  CT_SetAnyData(pcgCtAft, llTowLineNo, rgCtLine.Value);

  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static int InsertTowingRecord2DB(int ipTowIdx)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  char pclSqlTab[16] = "";
  char pclSqlFld[1024] = "";
  char pclSqlVal[1024] = "";
  char pclSqlBuf[1024] = "";
  long llTowLineNo = 0;
  short slCursor = 0;
  short slFkt = 0;

/*
  dbg(TRACE,"======================================================");
  dbg(TRACE,"TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST");
  dbg(TRACE,"======================================================");
  dbg(TRACE,"     NOT PERFORMING THE INSERTION INTO AFTTAB         ");
  dbg(TRACE,"======================================================");
return ilRc;
*/

  llTowLineNo = rgTowDat[ipTowIdx].TowLineNo;
  CT_GetLineValues(pcgCtAft, llTowLineNo, &rgCtLine);
  dbg(TRACE,"NEW TOWING RECORD\n<%s>",rgCtLine.Value);

  strcpy(pclSqlTab,"AFTTAB");
  strcpy(pclSqlFld,pcgCtAftFldLst);
  BuildSqlInsValStrg(pclSqlVal,pclSqlFld);
  sprintf(pclSqlBuf,"INSERT INTO %s (%s) VALUES (%s)",pclSqlTab,pclSqlFld,pclSqlVal);

  dbg(TRACE,"<%s>",pclSqlBuf);

  slCursor = 0;
  slFkt = IGNORE;
  delton(rgCtLine.Value);
  ilGetRc = sql_if(slFkt, &slCursor, pclSqlBuf, rgCtLine.Value);
  if (ilGetRc == DB_SUCCESS)
  {
    commit_work ();
    dbg(TRACE,"TOWING INSERTED");
    dbg(TRACE,"===============");
  }
  else
  {
    dbg (TRACE, "ORA INSERT FAILED. ROLLBACK!");
    dbg (TRACE, "============================");
    rollback ();
  }
  close_my_cursor (&slCursor);

  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static void BuildSqlInsValStrg(char *pcpResult, char *pcpFldLst)
{
  char pclFldNam[16] = "";
  char pclFldVal[32] = "";
  long llFldCnt = 0;
  long i = 0;
  pcpResult[0] = 0x00;
  llFldCnt = CT_CountPattern(pcpFldLst, ",");
  for (i=0;i<=llFldCnt;i++)
  {
    CT_GetItemsFromTo(pclFldNam, pcpFldLst, i, i, ",");
    sprintf(pclFldVal,":V%s,",pclFldNam,pclFldNam);
    strcat(pcpResult,pclFldVal);
  }
  i = strlen(pcpResult) - 1;
  pcpResult[i] = 0x00;
  return;
}

/* *************************************************** */
/* *************************************************** */
static int get_any_item(char *dest, char *src, int ItemNbr, char *sep)
{
        int rc = -1;
        int gt_rc;
        int b_len = 0;
        int trim = TRUE;
        if (ItemNbr < 0){
                trim = FALSE;
                ItemNbr = - ItemNbr;
        } /* end if */
        gt_rc = get_item(ItemNbr, src, dest, b_len, sep, "\0", "\0");
        if (gt_rc == TRUE){
                if (trim == TRUE){
                        str_trm_rgt(dest, " ", TRUE);
                } /* end if */
                rc = strlen(dest);
        }/* end if */
        return rc;
}/* end of get_any_item */

/* *************************************************** */
/* *************************************************** */
static void CheckGhpReload(char *pcpCmd, char *pcpTbl, char *pcpSel, char *pcpFld, char *pcpDat)
{
  int ilReload = TRUE;
  if (ilReload == TRUE)
  {
    LoadTowRules();
  }
  return;
}

/* *************************************************** */
/* *************************************************** */
static void CheckTowings(char *pcpCmd, char *pcpTbl, char *pcpSel, char *pcpFld, char *pcpDat)
{
  long llLen = 0;
  char *pclMsgPtr = NULL;
  char pclGorRkey[64] = "";
  char pclMsgType[64] = "";
  char pclAreaBgn[64] = "";
  char pclAreaEnd[64] = "";
  char pclAreaMsg[4096] = "";
  char pclMsgData[4096] = "";
  char pclMsgKeys[1024] = "";
  char pclFldData[1024] = "";
  char pcgTrgMsgRkey[16] = "";
  char pcgTrgMsgUrno[16] = "";
  char pcgTrgTifaOld[16] = "";
  char pcgTrgPstaOld[16] = "";
  char pcgTrgTifdOld[16] = "";
  char pcgTrgPstdOld[16] = "";
  char pcgTrgTifaNew[16] = "";
  char pcgTrgPstaNew[16] = "";
  char pcgTrgTifdNew[16] = "";
  char pcgTrgPstdNew[16] = "";
  char pclRkey[16] = "";
  int ilSendRacAlways = FALSE;
  int ilCurItm = 0;
  int ilMaxItm = 0;
  int ilLen = 0;
  int ilCase = 0;
  int ilUpdCnt = 0;
  dbg(TRACE,"==============================");
  dbg(TRACE,"CHECK TOWINGS: CMD <%s>",pcpCmd);
  dbg(TRACE,"CHECK TOWINGS: TBL <%s>",pcpTbl);
  dbg(TRACE,"CHECK TOWINGS: SEL <%s>",pcpSel);
  dbg(TRACE,"CHECK TOWINGS: FLD <%s>",pcpFld);
  dbg(TRACE,"CHECK TOWINGS: DAT <%s>",pcpDat);

  GetTowHdlConfig(pcpSel);

  strcpy(pcgTowRkeyList,"");
  strcpy(pcgTowRacTifa,"99999999999999");
  strcpy(pcgTowRacTifd,"00000000000000");
  ilMaxItm = field_count(pcpDat);
  dbg(TRACE,"CHECK TOWINGS: %d RKEY VALUES IN DATA",ilMaxItm);
  for (ilCurItm=1;ilCurItm<=ilMaxItm;ilCurItm++)
  {
    ilLen = get_real_item(pclRkey,pcpDat,ilCurItm);
    if (ilLen > 0)
    {
      dbg(TRACE,"=======================================");
      dbg(TRACE,"CHECK TOWINGS: RKEY <%s> ITEM=%d",pclRkey,ilCurItm);
      dbg(TRACE,"=======================================");

      strcpy(pcgCurTowRkey,pclRkey);

      igTriggerIsArrPstaUpdate = FALSE;
      igTriggerIsDepPstdUpdate = FALSE;
      igTriggerIsArrTifaUpdate = FALSE;
      igTriggerIsDepTifdUpdate = FALSE;

      pclMsgPtr = pcpFld;
      strcpy(pclAreaMsg,"");
      sprintf(pclAreaBgn,"{AREA:RKEY[%s]}",pclRkey);
      sprintf(pclAreaEnd,"{RKEY[%s]AREA}",pclRkey);
      CedaGetKeyItem(pcgCurRkeyMsg,&llLen,pclMsgPtr,pclAreaBgn,pclAreaEnd, TRUE);
      dbg(TRACE,"RKEY MESSAGE AREA\n%s",pcgCurRkeyMsg);
      dbg(TRACE,"---------------------------------------------------------");
      strcpy(pcgTowRacRegn,"");
      strcpy(pcgCurRacTifa,"99999999999999");
      strcpy(pcgCurRacTifd,"00000000000000");

      if (igUsingGortabRelation == TRUE)
      {
        /* To be safe: SYNC GORTAB before using it */
        sprintf(pclGorRkey,"WHERE RKEY=%s",pclRkey);
        FLT_SyncGorRotation(pclGorRkey);
      }
      LoadFlightRotation(pclRkey);
      ilCase = CheckFlightRotation();

      if ((ilCase == 0)||(igMustRepairAurn == TRUE))
      {
        if (ilCase != 0)
        {
          dbg(TRACE,"===========================================");
          dbg(TRACE,">>>>> AUTO_REPAIR_AURN HAS BEEN ACTIVATED!!");
          dbg(TRACE,">>>>> SETTING WORK FLOW TO REPAIR_AURN_ONLY");
          dbg(TRACE,"===========================================");
          igRepairAurnOnly = TRUE;
        }

        EvaluateTowingRules();
        CreateOrUpdateTowings();

        if (strlen(pcgTowRkeyList) > 0)
        {
          /* At least one insert has been performed.            */
          /* Thus we'll finally issue a RAC command, but ...    */
          /* dbg(TRACE,"Still Must Perform Silent DB Updates"); */
          /* dbg(TRACE,"Still Must Trigger Action and Loghdl"); */
          ilUpdCnt = CheckGridUpdates(pcgCtAft,1);
        }
        else
        {
          ilUpdCnt = CheckGridUpdates(pcgCtAft,0);
        }
      }
      if (ilCase == 10)
      {
        /* STANDALONE DEPARTURE WITH TOWINGS */
        rgFltDat[1].DelAllTows = TRUE;
        ilUpdCnt = CheckGridUpdates(pcgCtAft,0);
      }
      if (igUsingGortabRelation == TRUE)
      {
        /* Always SYNC GORTAB after AutoTow evaluation */
        /*sprintf(pclGorRkey,"WHERE RKEY=%s",pclRkey);*/
        FLT_SyncGorRotation(pclGorRkey);
      }
    }
  }

  if ((strlen(pcgTowRkeyList) > 0)||(ilUpdCnt > 0)||
      (igTowRecDelFlag == TRUE)||(ilSendRacAlways == TRUE))
  {
    /* At least one insert has been performed    */
    /* or least one deletion has been performed. */
    /* Thus we'll finally issue a RAC command.   */
    /* This will convince Fips to understand it. */
    if (strlen(pcgTowRkeyList) == 0)
    {
      strcpy(pcgTowRkeyList,pcgCurTowRkey);

    }
    dbg(TRACE,"DEPLOY FINAL RAC COMMAND TO BCHDL");
    dbg(TRACE,"---------------------------------");
    CreateOutMessage(4, "RAC", pcgCtAft, "", "B", "");
  }

  return;
}

/* *************************************************** */
/* *************************************************** */
static void PrintReports(char *pcpForWhat)
{
  strcpy(pcgPrintTitle,"LIST OF TOWING CHAIN AND TIME FIELDS");
  strcpy(pcgPrintHeader,"F,A,  CSGN  ,PSTD ,     STOD     ,     OFBL     ,PSTA ,     STOA     ,     ONBL     ,URNO");
  strcpy(pcgPrintFields,"FTYP,ADID,CSGN,PSTD,STOD,OFBL,PSTA,STOA,ONBL,URNO");
  PrintRotationChain(pcgPrintTitle,pcgPrintHeader,pcgPrintFields);

  strcpy(pcgPrintTitle,"LIST OF SCHEDULED STAND ALLOCATION TIMES (BASED ON TIFD/TIFA)");
  strcpy(pcgPrintHeader,"F,A,  CSGN  ,PSTD ,     PDBS     ,     PDES     ,PSTA ,     PABS     ,     PAES     ,URNO");
  strcpy(pcgPrintFields,"FTYP,ADID,CSGN,PSTD,PDBS,PDES,PSTA,PABS,PAES,URNO");
  PrintRotationChain(pcgPrintTitle,pcgPrintHeader,pcgPrintFields);

/*
  strcpy(pcgPrintTitle,"LIST OF ACTUAL STAND ALLOCATION TIMES (BASED ON BEST TIMES)");
  strcpy(pcgPrintHeader,"F,A,  CSGN  ,PSTA ,     PABA     ,     PAEA     ,PSTD ,     PDBA     ,     PDEA     ,URNO");
  strcpy(pcgPrintFields,"FTYP,ADID,CSGN,PSTA,PABA,PAEA,PSTD,PDBA,PDEA,URNO");
  PrintRotationChain(pcgPrintTitle,pcgPrintHeader,pcgPrintFields);
*/

/*
  strcpy(pcgPrintTitle,"COMPARING PSTA ALLOCATION TIMES WITH BEST TIME VALUES");
  strcpy(pcgPrintHeader,"F,A,PSTD ,PSTA ,     TIFD     ,     TIFA     ,     PABS     ,     PAES     ,URNO");
  strcpy(pcgPrintFields,"FTYP,ADID,PSTD,PSTA,TIFD,TIFA,PABS,PAES,URNO");
  PrintRotationChain(pcgPrintTitle,pcgPrintHeader,pcgPrintFields);
*/

/*
  strcpy(pcgPrintTitle,"COMPARING PSTA ALLOCATION TIMES WITH SKED TIME VALUES");
  strcpy(pcgPrintHeader,"F,A,PSTD ,PSTA ,     STOD     ,     STOA     ,     PABS     ,     PAES     ,URNO");
  strcpy(pcgPrintFields,"FTYP,ADID,PSTD,PSTA,STOD,STOA,PABS,PAES,URNO");
  PrintRotationChain(pcgPrintTitle,pcgPrintHeader,pcgPrintFields);
*/

/*
  strcpy(pcgPrintTitle,"COMPARING PSTA ALLOCATION TIMES WITH ACTUAL TIME VALUES");
  strcpy(pcgPrintHeader,"F,A,PSTD ,PSTA ,     OFBL     ,     ONBL     ,     PABA     ,     PAEA     ,URNO");
  strcpy(pcgPrintFields,"FTYP,ADID,PSTD,PSTA,OFBL,ONBL,PABA,PAEA,URNO");
  PrintRotationChain(pcgPrintTitle,pcgPrintHeader,pcgPrintFields);
*/

/*
  strcpy(pcgPrintTitle,"COMPARING PSTD ALLOCATION TIMES WITH BEST TIME VALUES");
  strcpy(pcgPrintHeader,"F,A,PSTD ,PSTA ,     TIFD     ,     TIFA     ,     PDBS     ,     PDES     ,URNO");
  strcpy(pcgPrintFields,"FTYP,ADID,PSTD,PSTA,TIFD,TIFA,PDBS,PDES,URNO");
  PrintRotationChain(pcgPrintTitle,pcgPrintHeader,pcgPrintFields);
*/

/*
  strcpy(pcgPrintTitle,"COMPARING PSTD ALLOCATION TIMES WITH SKED TIME VALUES");
  strcpy(pcgPrintHeader,"F,A,PSTD ,PSTA ,     STOD     ,     STOA     ,     PDBS     ,     PDES     ,URNO");
  strcpy(pcgPrintFields,"FTYP,ADID,PSTD,PSTA,STOD,STOA,PDBS,PDES,URNO");
  PrintRotationChain(pcgPrintTitle,pcgPrintHeader,pcgPrintFields);
*/

/*
  strcpy(pcgPrintTitle,"COMPARING PSTD ALLOCATION TIMES WITH ACTUAL TIME VALUES");
  strcpy(pcgPrintHeader,"F,A,PSTD ,PSTA ,     OFBL     ,     ONBL     ,     PDBA     ,     PDEA     ,URNO");
  strcpy(pcgPrintFields,"FTYP,ADID,PSTD,PSTA,OFBL,ONBL,PDBA,PDEA,URNO");
  PrintRotationChain(pcgPrintTitle,pcgPrintHeader,pcgPrintFields);
*/

  return;
}

/* *************************************************** */
/* Function to set pabs etc and check for towing gaps  */
/* *************************************************** */
static void CheckTowingChain(void)
{
  int ilTowCnt = 0;
  int ilChkCnt = 0;
  int ilOnwCnt = 0;
  int ilRevCnt = 0;
  int ilGapCnt = 0;
  int ilCheckOnwards = FALSE;
  int ilCheckReverse = FALSE;
  int ilCheckTowGaps = FALSE;
  int ilTowingArrDep = FALSE;
  int ilTowDelFlag = FALSE;
  long llPrvLineNo = -1;
  long llCurLineNo = -1;
  long llGapLineNo = -1;
  char pclEmpty[16] = "";
  strcpy(pclEmpty,"              ");
  if (rgFltDat[1].FltExists == TRUE)
  {
    ilOnwCnt = 0;
    ilRevCnt = 0;
    ilGapCnt = 0;
    ilCheckOnwards = TRUE;
    ilTowCnt = CountTowings(0,0);
    ilChkCnt = ilTowCnt;
    dbg(TRACE,"WE HAVE %d TOWINGS IN THE DATA GRID",ilTowCnt);
    if (igMustRepairAurn == TRUE)
    {
      AutoRepairAurn(TRUE);
      if (igRepairAurnOnly == TRUE)
      {
        dbg(TRACE,"#####################################");
        dbg(TRACE,"# >>>>> MODE WAS REPAIR_AURN_ONLY!! #");
        dbg(TRACE,"# >>>>> NOW LEAVING DATA PROCESSING #");
        dbg(TRACE,"#####################################");
        return;
      }
    }
    igTowRecDelFlag = FALSE;
    if (rgFltDat[1].DelAllTows == TRUE)
    {
      dbg(TRACE,"NOTE: ALL TOWINGS SHALL BE DELETED");
      rgFltDat[1].DelArrTowOut = TRUE;
      rgFltDat[1].DelDepTowIn = TRUE;
    }
    if (rgFltDat[1].DelArrTowOut == TRUE)
    {
      llCurLineNo = rgTowDat[1].TowLineNo;
      if (llCurLineNo >= 0)
      {
        dbg(TRACE,"TOWING IDX=1 SHALL BE DELETED");
        CT_SetLineDbDelFlag(pcgCtAft, llCurLineNo, 1);
        igTowRecDelFlag = TRUE;
      }
    }
    if (rgFltDat[1].DelDepTowIn == TRUE)
    {
      llCurLineNo = rgTowDat[2].TowLineNo;
      if (llCurLineNo >= 0)
      {
        dbg(TRACE,"TOWING IDX=2 SHALL BE DELETED");
        CT_SetLineDbDelFlag(pcgCtAft, llCurLineNo, 1);
        igTowRecDelFlag = TRUE;
      }
    }

    if (rgTowDat[1].CurBgnOffs <= 0)
    {
      rgTowDat[1].CurBgnOffs = rgTowDat[1].TowBgnOffs;
    }
    if (rgTowDat[2].CurEndOffs <= 0)
    {
      rgTowDat[2].CurEndOffs = rgTowDat[2].TowEndOffs;
    }
    if (rgTowDat[1].TowIsValid == TRUE)
    {
      sprintf(rgTowDat[1].StrTowRem1,"ONB+%d (%s/%s=%d)",rgTowDat[1].CurBgnOffs,rgTowDat[1].StrTowTRule,rgTowDat[1].StrTowARule,rgTowDat[1].TowBgnOffs);
      llCurLineNo = rgTowDat[1].TowLineNo;
      UpdateGridData(pcgCtAft, llCurLineNo, "REM1", rgTowDat[1].StrTowRem1);
    }
    if (rgTowDat[2].TowIsValid == TRUE)
    {
      sprintf(rgTowDat[2].StrTowRem1,"ETD-%d (%s/%s=%d)",rgTowDat[2].CurEndOffs,rgTowDat[2].StrTowTRule,rgTowDat[2].StrTowDRule,rgTowDat[2].TowEndOffs);
      llCurLineNo = rgTowDat[2].TowLineNo;
      UpdateGridData(pcgCtAft, llCurLineNo, "REM1", rgTowDat[2].StrTowRem1);
    }
    if (ilTowCnt > 0)
    {
      if (ilCheckOnwards == TRUE)
      {
        if ((rgFltDat[1].FltExists == TRUE)||(rgFltDat[1].DelAllTows == TRUE)||(rgFltDat[1].DelAllOther == TRUE))
        {
          dbg(TRACE,"CHECK CHAIN OF TOWINGS (ONWARDS) AND SET ALLOCATION TIMES");
          dbg(TRACE,"=========================================================");
          strcpy(rgCurPsta.Value,rgFltDat[1].StrFltStnd);
          strcpy(rgCurStoa.Value,rgFltDat[1].StrFltTime);
          llCurLineNo = rgFltDat[1].FltLineNo;
          llPrvLineNo = llCurLineNo;
          llCurLineNo = SearchNextTowing(rgCurPsta.Value, rgFltDat[1].FltLineNo, 1, TRUE);
          while (llCurLineNo >= 0)
          {
            ilOnwCnt++;
            ilChkCnt--;
            dbg(TRACE,"FOUND %d. TOWING IN GRIDLINE %d",ilOnwCnt,llCurLineNo);
            ilTowingArrDep = FALSE;
            ilTowDelFlag = FALSE;
            if (rgTowDat[1].TowExists == TRUE)
            {
              if (llCurLineNo == rgTowDat[1].TowLineNo)
              {
                dbg(TRACE,"---------------------------------------");
                dbg(TRACE,"THIS IS THE EXISTING ARR FLIGHT TOW-OUT");
                dbg(TRACE,"---------------------------------------");
                ilTowingArrDep = TRUE;
                if (rgFltDat[1].DelArrTowOut == FALSE)
                {
                  /*
                  UpdateGridData(pcgCtAft, llCurLineNo, "REM1", rgTowDat[1].StrTowRem1);
                  if (rgTowDat[1].TowOutIsOfbl == FALSE)
                  {
                    if ((rgTowDat[1].ValNewStod != rgTowDat[1].ValTowStod)||
                        (rgTowDat[1].ValNewStoa != rgTowDat[1].ValTowStoa))
                    {
                      dbg(TRACE,"UPDATING TOW.STOD/TOW.STOA BASED ON ARR.TIFA");
                      UpdateGridData(pcgCtAft, llCurLineNo, "STOD", rgTowDat[1].StrNewStod);
                      UpdateGridData(pcgCtAft, llCurLineNo, "STOA", rgTowDat[1].StrNewStoa);
                    }
                  }
                  else
                  {
                    dbg(TRACE,"NOT UPDATING TOW.STOD/TOW.STOA (TOW-OUT IS OFF-BLOCK)");
                  }
                  */
                }
                else
                {
                  dbg(TRACE,"DELETE ARR TOW-OUT: THIS TOWING SHALL BE DELETED");
                  CT_SetLineDbDelFlag(pcgCtAft, llCurLineNo, 1);
                  ilTowDelFlag = TRUE;
                }
              }
            }
            else if (rgTowDat[1].TowIsValid == TRUE)
            {
              if (llCurLineNo == rgTowDat[1].TowLineNo)
              {
                dbg(TRACE,"----------------------------------");
                dbg(TRACE,"THIS IS THE NEW ARR FLIGHT TOW-OUT");
                dbg(TRACE,"----------------------------------");
                ilTowingArrDep = TRUE;
                if (rgFltDat[1].DelArrTowOut == FALSE)
                {
                  /*
                  UpdateGridData(pcgCtAft, llCurLineNo, "REM1", rgTowDat[1].StrTowRem1);
                  */
                }
                else
                {
                  dbg(TRACE,"DELETE NEW ARR TOW-OUT: THIS TOWING SHALL BE DELETED");
                  CT_SetLineDbDelFlag(pcgCtAft, llCurLineNo, 1);
                  ilTowDelFlag = TRUE;
                }
              }
            }
            /*
            if (rgFltDat[2].FltExists == TRUE)
            {
              dbg(TRACE,"DEBUGGING: FLT-EXISTS[2] = TRUE");
            }
            else
            {
              dbg(TRACE,"DEBUGGING: FLT-EXISTS[2] = FALSE");
            }
            if (rgTowDat[2].TowExists == TRUE)
            {
              dbg(TRACE,"DEBUGGING: TOW-EXISTS[2] = TRUE");
            }
            else
            {
              dbg(TRACE,"DEBUGGING: TOW-EXISTS[2] = FALSE");
            }
            if (rgTowDat[2].TowIsValid == TRUE)
            {
              dbg(TRACE,"DEBUGGING: TOW-IS-VALID[2] = TRUE");
            }
            else
            {
              dbg(TRACE,"DEBUGGING: TOW-IS-VALID[2] = FALSE");
            }

            if (rgTowDat[2].TowIsTowIn == TRUE)
            {
              dbg(TRACE,"DEBUGGING: TOW-IS-TOW-IN[2] = TRUE");
            }
            else
            {
              dbg(TRACE,"DEBUGGING: TOW-IS-TOW-IN[2] = FALSE");
            }
            */
            if ((rgTowDat[2].TowExists == TRUE)&&((rgFltDat[2].FltExists == TRUE)||(rgFltDat[1].DelDepTowIn == TRUE)))
            {
              if ((llCurLineNo == rgTowDat[2].TowLineNo)&&(rgTowDat[2].TowIsTowIn == TRUE))
              {
                dbg(TRACE,"--------------------------------------");
                dbg(TRACE,"THIS IS THE EXISTING DEP FLIGHT TOW-IN");
                if (rgTowDat[2].TowIsTowOut == TRUE)
                {
                  dbg(TRACE,"INCL THE ACTUAL ARRIVAL FLIGHT TOW-OUT");
                }
                dbg(TRACE,"--------------------------------------");
                ilTowingArrDep = TRUE;
                if (rgFltDat[1].DelDepTowIn == FALSE)
                {
                  /*
                  if (rgTowDat[2].TowInIsOfbl == FALSE)
                  {
                    UpdateGridData(pcgCtAft, llCurLineNo, "REM1", rgTowDat[2].StrTowRem1);
                    if ((rgTowDat[2].ValNewStod != rgTowDat[2].ValTowStod)||
                        (rgTowDat[2].ValNewStoa != rgTowDat[2].ValTowStoa))
                    {
                      dbg(TRACE,"UPDATING TOW.STOD/TOW.STOA BASED ON DEP.TIFD");
                      UpdateGridData(pcgCtAft, llCurLineNo, "STOD", rgTowDat[2].StrNewStod);
                      UpdateGridData(pcgCtAft, llCurLineNo, "STOA", rgTowDat[2].StrNewStoa);
                    }
                  }
                  else
                  {
                    dbg(TRACE,"NOT UPDATING TOW.STOD/TOW.STOA (TOW-IN IS OFF-BLOCK)");
                  }
                  */
                }
                else
                {
                  dbg(TRACE,"DELETE DEP TOW-IN: THIS TOWING SHALL BE DELETED");
                  CT_SetLineDbDelFlag(pcgCtAft, llCurLineNo, 1);
                  ilTowDelFlag = TRUE;
                }
              }
            }
            else if ((rgTowDat[2].TowIsValid == TRUE)&&(rgFltDat[2].FltExists == TRUE))
            {
              if (llCurLineNo == rgTowDat[2].TowLineNo)
              {
                dbg(TRACE,"---------------------------------");
                dbg(TRACE,"THIS IS THE NEW DEP FLIGHT TOW-IN");
                if (rgTowDat[2].TowIsTowOut == TRUE)
                {
                  dbg(TRACE,"(INCL THE NEW ARR FLIGHT TOW-OUT)");
                }
                dbg(TRACE,"---------------------------------");
                ilTowingArrDep = TRUE;
                if (rgFltDat[1].DelDepTowIn == FALSE)
                {
                  /*
                  UpdateGridData(pcgCtAft, llCurLineNo, "REM1", rgTowDat[2].StrTowRem1);
                  */
                }
                else
                {
                  dbg(TRACE,"DELETE DEP TOW-IN: THIS TOWING SHALL BE DELETED");
                  CT_SetLineDbDelFlag(pcgCtAft, llCurLineNo, 1);
                  ilTowDelFlag = TRUE;
                }
              }
            }
            if (ilTowingArrDep != TRUE)
            {
              dbg(TRACE,"--------------------------");
              dbg(TRACE,"THIS IS YET ANOTHER TOWING");
              dbg(TRACE,"--------------------------");
              dbg(TRACE,"EVALUTION FOR SUCH TOWINGS");
              dbg(TRACE,"DOES NOT INCLUDE STOD/STOA");
              dbg(TRACE,"--------------------------");
              if ((rgFltDat[1].DelAllTows == TRUE)||(rgFltDat[1].DelAllOther == TRUE))
              {
                if (rgFltDat[1].DelAllTows == TRUE)
                {
                  dbg(TRACE,"DELETE ALL TOWINGS: THIS TOWING SHALL BE DELETED");
                }
                if (rgFltDat[1].DelAllOther == TRUE)
                {
                  dbg(TRACE,"DELETE ALL OTHER TOWINGS: THIS TOWING SHALL BE DELETED");
                }
                CT_SetLineDbDelFlag(pcgCtAft, llCurLineNo, 1);
                ilTowDelFlag = TRUE;
              }
            }
            if (ilTowDelFlag == FALSE)
            {
              /*
              CT_GetColumnValueByName(pcgCtAft, llCurLineNo, "STOD", &rgCurStod);
              CT_GetColumnValueByName(pcgCtAft, llCurLineNo, "OFBL", &rgCurOfbl);
              dbg(TRACE,"SETTING <PAES> OF PREVIOUS RECORD BASED ON STOD <%s>",rgCurStod.Value);
              UpdateGridData(pcgCtAft, llPrvLineNo, "PAES", rgCurStod.Value);
              dbg(TRACE,"SETTING <PAEA> OF PREVIOUS RECORD BASED ON OFBL <%s>",rgCurOfbl.Value);
              UpdateGridData(pcgCtAft, llPrvLineNo, "PAEA", rgCurOfbl.Value);

              CT_GetColumnValueByName(pcgCtAft, llCurLineNo, "STOA", &rgCurStoa);
              CT_GetColumnValueByName(pcgCtAft, llCurLineNo, "ONBL", &rgCurOnbl);
              dbg(TRACE,"SETTING <PABS> OF CURRENT RECORD BASED ON STOA <%s>",rgCurStoa.Value);
              UpdateGridData(pcgCtAft, llCurLineNo, "PABS", rgCurStoa.Value);
              dbg(TRACE,"SETTING <PABA> OF CURRENT RECORD BASED ON ONBL <%s>",rgCurOnbl.Value);
              UpdateGridData(pcgCtAft, llCurLineNo, "PABA", rgCurOnbl.Value);
              */

              UpdateStandAllocationTimes(llPrvLineNo, llCurLineNo);


              llPrvLineNo = llCurLineNo;
            }
            else
            {
              igTowRecDelFlag = TRUE;
            }
            CT_GetColumnValueByName(pcgCtAft, llCurLineNo, "PSTA", &rgCurPsta);
            llCurLineNo = SearchNextTowing(rgCurPsta.Value, rgFltDat[1].FltLineNo, 1, TRUE);
          } /* end while */

          if (rgFltDat[2].FltExists == TRUE)
          {
            llCurLineNo = rgFltDat[2].FltLineNo;
            str_trm_rgt(rgCurPsta.Value, " ", TRUE);
            dbg(TRACE,"LAST TOW ARR STAND (PSTA) <%s>",rgCurPsta.Value);
            dbg(TRACE,"DEPARTURE ON STAND (PSTD) <%s>",rgFltDat[2].StrFltStnd);
            if (strcmp(rgCurPsta.Value,rgFltDat[2].StrFltStnd) == 0)
            {
              dbg(TRACE,"LAST IDENTIFIED TOWING MATCHES DEPARTURE POSITION");
              if (llPrvLineNo == rgTowDat[2].TowLineNo)
              {
                if (rgTowDat[2].TowOutAndIn == TRUE)
                {
                  dbg(TRACE,"TOWING IS TOW-OUT AND TOW-IN");
                }
                dbg(TRACE,"CREATE OCCUPATION TIMES ON PSTA FOR THE LAST TOW-IN");
                /*
                CT_GetColumnValueByName(pcgCtAft, llPrvLineNo, "STOA", &rgCurStoa);
                CT_GetColumnValueByName(pcgCtAft, llCurLineNo, "TIFD", &rgCurStod);
                dbg(TRACE,"SETTING <PABS> OF LAST TOW RECORD BASED ON STOA <%s> (TOW)",rgCurStoa.Value);
                UpdateGridData(pcgCtAft, llPrvLineNo, "PABS", rgCurStoa.Value);
                dbg(TRACE,"SETTING <PAES> OF LAST TOW RECORD BASED ON TIFD <%s> (FLT)",rgCurStod.Value);
                UpdateGridData(pcgCtAft, llPrvLineNo, "PAES", rgCurStod.Value);

                dbg(TRACE,"SETTING <PABS> OF DEP FLT RECORD BASED ON STOA <%s> (TOW)",rgCurStoa.Value);
                UpdateGridData(pcgCtAft, llCurLineNo, "PABS", rgCurStoa.Value);

                CT_GetColumnValueByName(pcgCtAft, llCurLineNo, "OFBL", &rgCurOfbl);
                dbg(TRACE,"SETTING <PAEA> OF LAST TOW RECORD BASED ON OFBL <%s> (FLT)",rgCurOfbl.Value);
                UpdateGridData(pcgCtAft, llPrvLineNo, "PAEA", rgCurOfbl.Value);
                */

                UpdateStandAllocationTimes(llPrvLineNo, llCurLineNo);

              }
              else
              {
                dbg(TRACE,"NOT UPDATING IDENTIFIED TOWING BECAUSE IT IS NOT THE LAST IN THE LIST");
              }
            }
            /*
            dbg(TRACE,"SETTING ALLOCATION TIMES OF DEP FLIGHT");
            CT_GetColumnValueByName(pcgCtAft, llCurLineNo, "TIFD", &rgCurStod);
            CT_GetColumnValueByName(pcgCtAft, llCurLineNo, "OFBL", &rgCurOfbl);

            dbg(TRACE,"SETTING <PAES> OF DEP FLT RECORD BASED ON TIFD <%s>",rgCurStod.Value);
            UpdateGridData(pcgCtAft, llCurLineNo, "PAES", rgCurStod.Value);
            dbg(TRACE,"SETTING <PAEA> OF CURRENT RECORD BASED ON OFBL <%s>",rgCurOfbl.Value);
            UpdateGridData(pcgCtAft, llCurLineNo, "PAEA", rgCurOfbl.Value);
            */

          }
        } /* Flight Exists */
      } /* Onwards */

      /* 'Reverse' does not yet work */
      ilCheckReverse = FALSE;
      if (ilCheckReverse == TRUE)
      {
        if (rgFltDat[2].FltExists == TRUE)
        {
          dbg(TRACE,"CHECK CHAIN OF TOWINGS (REVERSE) AND SET ALLOCATION TIMES");
          dbg(TRACE,"=========================================================");
          strcpy(rgCurPstd.Value,rgFltDat[2].StrFltStnd);
          strcpy(rgCurStod.Value,rgFltDat[2].StrFltTime);
          llCurLineNo = rgFltDat[2].FltLineNo;
          llPrvLineNo = llCurLineNo;
          llCurLineNo = SearchPrevTowing(rgCurPstd.Value, 2);
          while (llCurLineNo >= 0)
          {
            ilRevCnt++;
            ilChkCnt--;
            dbg(TRACE,"FOUND %d. TOWING IN GRIDLINE %d",ilRevCnt,llCurLineNo);
            /*
            CT_GetColumnValueByName(pcgCtAft, llCurLineNo, "STOA", &rgCurStoa);
            UpdateGridData(pcgCtAft, llPrvLineNo, "PABS", rgCurStod.Value);
            CT_GetColumnValueByName(pcgCtAft, llCurLineNo, "STOD", &rgCurStod);
            UpdateGridData(pcgCtAft, llCurLineNo, "PAES", rgCurStod.Value);
            */
            CT_GetColumnValueByName(pcgCtAft, llCurLineNo, "PSTD", &rgCurPstd);
            llPrvLineNo = llCurLineNo;
            llCurLineNo = SearchPrevTowing(rgCurPstd.Value, 2);
          }
        }
      }
      if (ilChkCnt > 0)
      {
        ilCheckTowGaps = TRUE;
      }
      if (ilCheckTowGaps == TRUE)
      {
        if ((rgFltDat[1].FltExists == TRUE)||(rgFltDat[1].DelAllTows == TRUE)||(rgFltDat[1].DelAllOther == TRUE))
        {
          dbg(TRACE,"CHECK CHAIN OF TOWINGS (FOR GAPS) AND SET ALLOCATION TIMES");
          dbg(TRACE,"==========================================================");
          while (ilChkCnt > 0)
          {
            strcpy(rgCurPsta.Value,"***");
            llGapLineNo = SearchNextTowing(rgCurPsta.Value, rgFltDat[1].FltLineNo, 3, FALSE);
            if (llGapLineNo >= 0)
            {
              ilChkCnt--;
              ilGapCnt++;
              dbg(TRACE,"FOUND %d. NOT YET HANDLED TOWING IN GRIDLINE %d",ilGapCnt,llGapLineNo);
              CT_GetColumnValueByName(pcgCtAft, llGapLineNo, "PSTA", &rgCurPsta);
              dbg(TRACE,"(CHECK CHAIN OF THIS TOWING ARRIVING AT <%s>)",rgCurPsta.Value);
              dbg(TRACE,"---------------------------------------------");
              if ((rgFltDat[1].DelAllTows == TRUE)||(rgFltDat[1].DelAllOther == TRUE))
              {
                if (rgFltDat[1].DelAllTows == TRUE)
                {
                  dbg(TRACE,"DELETE ALL TOWINGS: THIS TOWING SHALL BE DELETED");
                }
                if (rgFltDat[1].DelAllOther == TRUE)
                {
                  dbg(TRACE,"DELETE ALL OTHER TOWINGS: THIS TOWING SHALL BE DELETED");
                }
                CT_SetLineDbDelFlag(pcgCtAft, llGapLineNo, 1);
                ilTowDelFlag = TRUE;
              }
              llPrvLineNo = llGapLineNo;
              llCurLineNo = SearchNextTowing(rgCurPsta.Value, rgFltDat[1].FltLineNo, 3, TRUE);
              while (llCurLineNo >= 0)
              {
                ilGapCnt++;
                ilChkCnt--;
                dbg(TRACE,"FOUND %d. NOT YET HANDLED TOWING IN GRIDLINE %d",ilGapCnt,llCurLineNo);
                if ((rgFltDat[1].DelAllTows == TRUE)||(rgFltDat[1].DelAllOther == TRUE))
                {
                  if (rgFltDat[1].DelAllTows == TRUE)
                  {
                    dbg(TRACE,"DELETE ALL TOWINGS: THIS TOWING SHALL BE DELETED");
                  }
                  if (rgFltDat[1].DelAllOther == TRUE)
                  {
                    dbg(TRACE,"DELETE ALL OTHER TOWINGS: THIS TOWING SHALL BE DELETED");
                  }
                  CT_SetLineDbDelFlag(pcgCtAft, llCurLineNo, 1);
                  ilTowDelFlag = TRUE;
                }
                llPrvLineNo = llCurLineNo;
                llCurLineNo = SearchNextTowing(rgCurPsta.Value, rgFltDat[1].FltLineNo, 3, TRUE);
              }
            }
          }
        }
      }
    }
    else
    {
      dbg(TRACE,"THERE ARE NO TOWINGS IN THE DATA GRID");
      dbg(TRACE,"(MUST CALCULATE PABS/PAES - PDBS/PDES)");
      dbg(TRACE,"--------------------------------------");
      llPrvLineNo = rgFltDat[1].FltLineNo;
      llCurLineNo = rgFltDat[2].FltLineNo;
      UpdateStandAllocationTimes(llPrvLineNo, llCurLineNo);
    }
  }
  dbg(TRACE,"===============================================");
  return;
}

/* *************************************************** */
/* *************************************************** */
static void UpdateStandAllocationTimes(long lpPrvTowLine, long lpCurTowLine)
{
  char pclPrvPaes[16] = "";
  char pclCurPdbs[16] = "";
  int ilTifa = 0;
  int ilTifd = 0;
  int ilDiff = 0;
  int ilHalf = 0;
  int ilPaes = 0;
  int ilPdbs = 0;
  int ilDummy = 0;

  dbg(TRACE,"---------------------------------------------------");
  dbg(TRACE,"UPDATE STAND ALLOCATION TIMES (LINES: PRV=%d CUR=%d)",lpPrvTowLine,lpCurTowLine);
  dbg(TRACE,"---------------------------------------------------");
  if ((lpPrvTowLine >= 0)&&(lpCurTowLine >= 0))
  {
    CT_GetColumnValueByName(pcgCtAft, lpPrvTowLine, "PSTA", &rgPrvStnd);
    CT_GetColumnValueByName(pcgCtAft, lpCurTowLine, "PSTD", &rgCurStnd);
    dbg(DEBUG,"CURRENT PARKING STAND: PRV=%s CUR=%s",rgPrvStnd.Value,rgCurStnd.Value);

    CT_GetColumnValueByName(pcgCtAft, lpPrvTowLine, "TIFA", &rgPrvTifa);
    CT_GetColumnValueByName(pcgCtAft, lpCurTowLine, "TIFD", &rgCurTifd);
    dbg(DEBUG,"PRV TIFA=%s CUR TIFD=%s",rgPrvTifa.Value,rgCurTifd.Value);

    (void) GetFullDay(rgPrvTifa.Value, &ilDummy, &ilTifa, &ilDummy);
    (void) GetFullDay(rgCurTifd.Value, &ilDummy, &ilTifd, &ilDummy);

    ilDiff = ilTifd - ilTifa;
    ilHalf = (ilDiff * 50) / 100;
    if (ilHalf < 5) ilHalf = 5;
    ilPaes = ilTifa + ilHalf;
    ilHalf = ilDiff - ilHalf;
    if (ilHalf < 5) ilHalf = 5;
    ilPdbs = ilTifd - ilHalf;

    /* Because of bad calculation in old Foghdl */
    ilPdbs = ilPaes;

    (void) FullTimeString(pclPrvPaes, ilPaes, "00");
    (void) FullTimeString(pclCurPdbs, ilPdbs, "00");

    dbg(DEBUG,"PRV PABS=%s PRV PAES=%s",rgPrvTifa.Value,pclPrvPaes);
    dbg(DEBUG,"CUR PDBS=%s CUR PDES=%s",pclCurPdbs,rgCurTifd.Value);

    dbg(TRACE,"=====NOW SETTING THE SCHEDULED ALLOCATION TIMES====");
    dbg(TRACE,"THIS IS THE OLD METHOD TO FILL THE PABS-PDES FIELDS");
    UpdateGridData(pcgCtAft, lpPrvTowLine, "PABS", rgPrvTifa.Value);
    UpdateGridData(pcgCtAft, lpPrvTowLine, "PAES", pclPrvPaes);
    UpdateGridData(pcgCtAft, lpCurTowLine, "PDBS", pclCurPdbs);
    UpdateGridData(pcgCtAft, lpCurTowLine, "PDES", rgCurTifd.Value);
/*
    dbg(TRACE,"=====NOW SETTING THE ACTUAL ALLOCATION TIMES=====");
    dbg(TRACE,"THIS IS A NEW METHOD TO FILL THE PABA-PAEA FIELDS");
    UpdateGridData(pcgCtAft, lpPrvTowLine, "PABA", rgPrvTifa.Value);
    UpdateGridData(pcgCtAft, lpPrvTowLine, "PAEA", rgCurTifd.Value);
    dbg(TRACE,"-------------------------------------------------");
    dbg(TRACE,"THIS IS A NEW METHOD TO FILL THE PDBA-PDEA FIELDS");
    UpdateGridData(pcgCtAft, lpCurTowLine, "PDBA", rgPrvTifa.Value);
    UpdateGridData(pcgCtAft, lpCurTowLine, "PDEA", rgCurTifd.Value);
    dbg(TRACE,"-------------------------------------------------");
*/
/*
    dbg(TRACE,"=====NOW CLEARING THE ACTUAL ALLOCATION TIMES=====");
    UpdateGridData(pcgCtAft, lpPrvTowLine, "PABA", " ");
    UpdateGridData(pcgCtAft, lpPrvTowLine, "PAEA", " ");
    dbg(TRACE,"-------------------------------------------------");
    dbg(TRACE,"THIS IS A NEW METHOD TO FILL THE PDBA-PDEA FIELDS");
    UpdateGridData(pcgCtAft, lpCurTowLine, "PDBA", " ");
    UpdateGridData(pcgCtAft, lpCurTowLine, "PDEA", " ");
    dbg(TRACE,"-------------------------------------------------");
*/

  }
  else
  {
    dbg(TRACE,"CAN'T WORK BECAUSE OF MISSING LINE REFERENCE IN DATA GRID");
  }

  dbg(TRACE,"---------------------------------------------------");
  return;
}

/* *************************************************** */
/* *************************************************** */
static void PrintRotationChain(char *pcpTitle, char *pcpHeader, char *pcpFields)
{
  char pclPrintTitle[256] = "";
  char pclPrintHeader[256] = "";
  char pclPrintFields[256] = "";
  long llMaxLine = 0;
  long llCurLine = 0;
  strcpy(pclPrintTitle,pcpTitle);
  strcpy(pclPrintHeader,pcpHeader);
  strcpy(pclPrintFields,pcpFields);
  ReplaceChars(pclPrintHeader, ",", "|");

  dbg(TRACE,"===============================================================================================");
  dbg(TRACE,"PRINT REPORT: %s",pclPrintTitle);
  dbg(TRACE,"-----------------------------------------------------------------------------------------------");
  dbg(TRACE,"%s",pclPrintHeader);
  dbg(TRACE,"-----------------------------------------------------------------------------------------------");
  llMaxLine = CT_GetLineCount(pcgCtAft) - 1;
  for (llCurLine=0;llCurLine<=llMaxLine;llCurLine++)
  {
    CT_GetFieldValues(pcgCtAft, llCurLine, pclPrintFields, &rgCtLine);
    ReplaceChars(rgCtLine.Value, ",", "|");
    dbg(TRACE,"%s",rgCtLine.Value);
  }
  dbg(TRACE,"===============================================================================================");
  return;
}

/* *************************************************** */
/* *************************************************** */
static int AutoRepairAurn(int ipReset)
{
  int ilCnt = 0;
  long llMaxLine = 0;
  long llCurLine = 0;
  dbg(TRACE,">>>>> AUTO_REPAIR_AURN: STARTING REPAIR FUNCTION");
  if (ipReset == TRUE)
  {
    dbg(TRACE,">>>>> AUTO_REPAIR_AURN: REPAIR MODE = RESET ONLY");
  }
  llMaxLine = CT_GetLineCount(pcgCtAft) - 1;
  for (llCurLine=0;llCurLine<=llMaxLine;llCurLine++)
  {
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "AURN", &argCtValue[1]);
    if (strstr(argCtValue[1].Value,"COR") != NULL)
    {
      ilCnt++;
      dbg(TRACE,">>>>> MUST REPAIR AURN IN LINE %d (%s)",llCurLine,argCtValue[1].Value);
      get_any_item(rgPrvAurn.Value, argCtValue[1].Value, 2, "|");
      get_any_item(rgCurAurn.Value, argCtValue[1].Value, 3, "|");
      if (ipReset == TRUE)
      {
        dbg(TRACE,">>>>> RESTORE USED AURN <%s> ONLY",rgPrvAurn.Value);
        CT_SetColumnValueByName(pcgCtAft, llCurLine, "AURN", rgPrvAurn.Value);
      }
      else
      {
        dbg(TRACE,">>>>> RESTORE USED AURN <%s> AND SET CORRECTED <%s>",rgPrvAurn.Value,rgCurAurn.Value);
        CT_SetColumnValueByName(pcgCtAft, llCurLine, "AURN", rgPrvAurn.Value);
        UpdateGridData(pcgCtAft, llCurLine, "AURN", rgCurAurn.Value);
      }
    }
  }
  dbg(TRACE,">>>>> AUTO_REPAIR_AURN: REPAIR FUNCTION FINISHED");
  if (ipReset == TRUE)
  {
    dbg(TRACE,">>>>> AUTO_REPAIR_AURN: %d RECORDS RESTORED");
  }
  else
  {
    dbg(TRACE,">>>>> AUTO_REPAIR_AURN: %d RECORDS REPAIRED");
  }
  return ilCnt;
}

/* *************************************************** */
/* *************************************************** */
static int CountTowings(int ipForWhat, int ipStatus)
{
  int ilTowCnt = 0;
  long llMaxLine = 0;
  long llCurLine = 0;
  llMaxLine = CT_GetLineCount(pcgCtAft) - 1;
  for (llCurLine=0;llCurLine<=llMaxLine;llCurLine++)
  {
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "FTYP", &rgCurFtyp);
    if (strstr("TG",rgCurFtyp.Value) != NULL)
    {
      /* dbg(TRACE,"THIS IS A TOWING OR GROUND MOVEMENT"); */
      CT_SetLineStatusValue(pcgCtAft, llCurLine, ipStatus);
      ilTowCnt++;
    }
  }
  return ilTowCnt;
}

/* *************************************************** */
/* *************************************************** */
static long SearchNextTowing(char *pcpPsta, long ipBgnLine, long ipStatus, int ipChkPos)
{
  long llLineStatus = 0;
  long llMaxLine = 0;
  long llCurLine = 0;
  long llTowLineNo = -1;
  strcpy(rgPrvPsta.Value,pcpPsta);
  str_trm_rgt(rgPrvPsta.Value, " ", TRUE);
  strcpy(rgPrvStod.Value,"99999999999999");
  dbg(TRACE,"SEARCH THE EARLIEST NEXT TOWING FROM POS <%s> STOD <%s>",rgPrvPsta.Value,rgPrvStod.Value);
  llMaxLine = CT_GetLineCount(pcgCtAft) - 1;
  for (llCurLine=ipBgnLine;llCurLine<=llMaxLine;llCurLine++)
  {
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "FTYP", &rgCurFtyp);
    if (strstr("TG",rgCurFtyp.Value) != NULL)
    {
      /* dbg(TRACE,"THIS IS A TOWING OR GROUND MOVEMENT"); */
      llLineStatus = CT_GetLineStatusValue(pcgCtAft, llCurLine);
      if (llLineStatus == 0)
      {
        dbg(DEBUG,"TOWING RECORD IN LINE %d NOT YET HANDLED",llCurLine);
        CT_GetColumnValueByName(pcgCtAft, llCurLine, "PSTD", &rgCurPstd);
        str_trm_rgt(rgCurPstd.Value, " ", TRUE);
        dbg(DEBUG,"LOOK PRV PSTA <%s> CURRENT PSTD = <%s>",rgPrvPsta.Value,rgCurPstd.Value);
        if ((strcmp(rgPrvPsta.Value,rgCurPstd.Value) == 0)||(ipChkPos == FALSE))
        {
          if (ipChkPos == TRUE)
          {
            dbg(DEBUG,"MATCHING ARR.PSTA->TOW.PSTD <%s>",rgCurPstd.Value);
          }
          else
          {
            dbg(DEBUG,"CHECK ANY ARR.PSTA(***)->TOW.PSTD(%s)",rgCurPstd.Value);
          }
          CT_GetColumnValueByName(pcgCtAft, llCurLine, "STOD", &rgCurStod);
          if (strcmp(rgPrvStod.Value,rgCurStod.Value) > 0)
          {
            dbg(DEBUG,"PRV.STOD <%s> IS GREATER THAN CUR.STOD <%s>",rgPrvStod.Value,rgCurStod.Value);
            llTowLineNo = llCurLine;
            dbg(DEBUG,"CURRENTLY BEST MATCHING TOWING RECORD IN LINE %d",llTowLineNo);
            strcpy(rgPrvStod.Value,rgCurStod.Value);
          }
        }
      }
    }
  }
  if (llTowLineNo >= 0)
  {
    dbg(DEBUG,"THE BEST MATCHING TOWING RECORD IS IN LINE %d",llTowLineNo);
    CT_SetLineStatusValue(pcgCtAft, llTowLineNo, ipStatus);
  }
  else
  {
    dbg(DEBUG,"COULD NOT FIND A MATCHING TOWING RECORD FROM <%s>",rgPrvPsta.Value);
  }
  return llTowLineNo;
}

/* *************************************************** */
/* *************************************************** */
static long SearchPrevTowing(char *pcpPstd, long ipStatus)
{
  long llLineStatus = 0;
  long llMaxLine = 0;
  long llCurLine = 0;
  long llTowLineNo = -1;
  strcpy(rgPrvPstd.Value,pcpPstd);
  strcpy(rgPrvStoa.Value,"00000000000000");
  llMaxLine = CT_GetLineCount(pcgCtAft) - 1;
  for (llCurLine=llMaxLine;llCurLine>=0;llCurLine--)
  {
    CT_GetColumnValueByName(pcgCtAft, llCurLine, "FTYP", &rgCurFtyp);
    if (strstr("TG",rgCurFtyp.Value) != NULL)
    {
      llLineStatus = CT_GetLineStatusValue(pcgCtAft, llCurLine);
      if (llLineStatus == 0)
      {
        dbg(TRACE,"TOWING RECORD IN LINE %d NOT YET HANDLED",llCurLine);
        CT_GetColumnValueByName(pcgCtAft, llCurLine, "PSTA", &rgCurPsta);
        str_trm_rgt(rgCurPsta.Value, " ", TRUE);
        if (strcmp(rgPrvPstd.Value,rgCurPsta.Value) == 0)
        {
          dbg(TRACE,"MATCHING TOW.PSTA<-PRV.PSTD <%s>",rgCurPsta.Value);
          CT_GetColumnValueByName(pcgCtAft, llCurLine, "STOA", &rgCurStoa);
          if (strcmp(rgCurStoa.Value,rgPrvStoa.Value) > 0)
          {
            dbg(TRACE,"CUR.STOA <%s> IS GREATER THAN PRV.STOA <%s>",rgCurStoa.Value,rgPrvStoa.Value);
            llTowLineNo = llCurLine;
            strcpy(rgPrvStoa.Value,rgCurStoa.Value);
          }
        }
      }
    }
  }
  if (llTowLineNo >= 0)
  {
    CT_SetLineStatusValue(pcgCtAft, llTowLineNo, ipStatus);
  }
  return llTowLineNo;
}

/* *************************************************** */
/* *************************************************** */
static void CheckPstReload(char *pcpCmd, char *pcpTbl, char *pcpSel, char *pcpFld, char *pcpDat)
{
  int ilReload = TRUE;
  if (ilReload == TRUE)
  {
    InitBridgeStands();
  }
  return;
}

/* *************************************************** */
/* *************************************************** */
static void InitBridgeStands(void)
{
  int ilGetRc = DB_SUCCESS;
  char pclSqlTab[16] = "";
  char pclSqlFld[1024] = "";
  char pclSqlDat[1024] = "";
  char pclSqlKey[1024] = "";
  char pclSqlBuf[1024] = "";
  long llTowLineNo = 0;
  short slCursor = 0;
  short slFkt = 0;
  dbg(TRACE,"=====================================================");
  strcpy(pcgBridgeStands,",");
  strcpy(pclSqlTab,"PSTTAB");
  strcpy(pclSqlFld,"PNAM");
  strcpy(pclSqlKey,"BRGS='X'");
  sprintf(pclSqlBuf,"SELECT %s FROM %s WHERE %s ORDER BY PNAM",pclSqlFld,pclSqlTab,pclSqlKey);
  dbg(TRACE,"<%s>",pclSqlBuf);

  slCursor = 0;
  slFkt = START;
  ilGetRc = RC_SUCCESS;
  while (ilGetRc == DB_SUCCESS)
  {
    ilGetRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclSqlDat);
	if (ilGetRc == DB_SUCCESS)
    {
      str_trm_rgt(pclSqlDat, " ", TRUE);
      strcat(pcgBridgeStands,pclSqlDat);
      strcat(pcgBridgeStands,",");
    }
    slFkt = NEXT;
  }
  close_my_cursor(&slCursor);
  dbg(TRACE,"STANDS WITH PAX BRIDGE\n<%s>",pcgBridgeStands);
  return;
}

/* *************************************************** */
/* *************************************************** */
static int StandWithBridge(char *pcpStand)
{
  char pclChkStand[16] = "";
  int ilRetVal = TRUE;
  sprintf(pclChkStand,",%s,",pcpStand);
  if (strstr(pcgBridgeStands,pclChkStand) != NULL)
  {
    ilRetVal = TRUE;
  }
  else
  {
    ilRetVal = FALSE;
  }
  return ilRetVal;
}

/* *************************************************** */
/* *************************************************** */
static int UpdateGridData(char *pcpGrid, long lpLineNo, char *pcpName, char *pcpData)
{
  int ilRc = RC_SUCCESS;
  long llColNo = 0;
  char pclOldVal[1024] = "";
  char pclNewVal[1024] = "";
  char pclAllowEmpty[128] = "PABA,PAEA,PDBA,PDEA";
  if (lpLineNo < 0)
  {
    return ilRc;
  }
  dbg(DEBUG,"UPDATE GRID DATA <%s> (%d) <%s> <%s>",pcpGrid,lpLineNo,pcpName,pcpData);
  strcpy(pclNewVal,pcpData);
  if (strlen(pclNewVal) == 0)
  {
    strcpy(pclNewVal," ");
  }
  if (strstr(pclAllowEmpty,pcpName) == NULL)
  {
    str_trm_rgt(pclNewVal, " ", TRUE);
  }
  if (strlen(pclNewVal) > 0)
  {
    strcpy(pclNewVal,pcpData);
    str_trm_rgt(pclNewVal, " ", TRUE);
    if (strlen(pclNewVal) == 0)
    {
      strcpy(pclNewVal," ");
    }
    if (strstr("PSTD,PSTA",pcpName) != NULL)
    {
      if (strcmp(pclNewVal,"#PD#") == 0)
      {
        strcpy(pclNewVal," ");
      }
      if (strcmp(pclNewVal,"#PA#") == 0)
      {
        strcpy(pclNewVal," ");
      }
    }
    llColNo = CT_SetColumnValueByName(pcpGrid, lpLineNo, pcpName, pclNewVal);
    if (llColNo >= 0)
    {
      CT_GetAnyList(pcpGrid, lpLineNo, &rgCtList);
      CT_GetAnyData(pcpGrid, lpLineNo, &rgCtData);
      dbg(DEBUG,"LINE %d TAG\n<%s>",lpLineNo,rgCtData.Value);
      CT_GetItemsFromTo(pclOldVal, rgCtData.Value, llColNo, llColNo, ",");
      str_trm_rgt(pclNewVal, " ", TRUE);
      str_trm_rgt(pclOldVal, " ", TRUE);
      if (strcmp(pclNewVal,pclOldVal) != 0)
      {
        dbg(TRACE,"LINE %d UPDATING: <%s> OLD VALUE <%s> NEW VALUE <%s>",lpLineNo,pcpName,pclOldVal,pclNewVal);
        /* Create the list of updated field names */
        if (strstr(rgCtList.Value,pcpName) == NULL)
        {
          if (rgCtList.Value[0] != '\0')
          {
            strcat(rgCtList.Value,",");
          }
          strcat(rgCtList.Value,pcpName);
          CT_SetAnyList(pcpGrid,lpLineNo,rgCtList.Value);
          CT_SetLineDbUpdFlag(pcpGrid,lpLineNo,1);
        }
      }
      else
      {
        dbg(TRACE,"LINE %d IDENTICAL <%s> OLD VALUE <%s> NEW VALUE <%s>",lpLineNo,pcpName,pclOldVal,pclNewVal);
        /* Perhaps we must remove the field from the List tag and reset the flag */
        if (strstr(rgCtList.Value,pcpName) != NULL)
        {
          dbg(DEBUG,"(A FUNCTION TO RESET UPDATE FLAGS HAS YET TO BE IMPLEMENTED)");
        }
      }
    }
    else
    {
      dbg(TRACE,"THE FIELD <%s> DOES NOT EXIST IN THE GRID",pcpName);
    }
  }
  else
  {
    dbg(TRACE,"WON'T UPDATE EMPTY DATA <%s> (%d) <%s> <%s>",pcpGrid,lpLineNo,pcpName,pcpData);
  }
  /* dbg(TRACE,"END OF UPDATE FUNCTION"); */
  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static int CheckGridUpdates(char *pcpGrid, int ipForWhat)
{
  int ilRc = RC_SUCCESS;
  int ilUpdCnt = 0;
  char pclFldName[64] = "";
  char pclNewData[1024] = "";
  char pclOldData[1024] = "";
  long llDbUpdFlag = 0;
  long llDbInsFlag = 0;
  long llDbDelFlag = 0;
  long llMaxLine = 0;
  long llBgnLine = 0;
  long llCurLine = 0;
  long llFldCnt = 0;
  long llCurFld = 0;
  long llColNo = 0;
  int ilForWhat = -1;
  int ilDeployMsg = TRUE;
  ilForWhat = ipForWhat;
  ilUpdCnt = 0;
  llBgnLine = 0;
  llMaxLine = CT_GetLineCount(pcpGrid) - 1;
  for (llCurLine=llBgnLine;llCurLine<=llMaxLine;llCurLine++)
  {
    llDbInsFlag = CT_GetLineDbInsFlag(pcpGrid,llCurLine);
    llDbUpdFlag = CT_GetLineDbUpdFlag(pcpGrid,llCurLine);
    llDbDelFlag = CT_GetLineDbDelFlag(pcpGrid,llCurLine);
    if ((llDbUpdFlag > 0)||(llDbInsFlag > 0)||(llDbDelFlag > 0))
    {
      ilUpdCnt++;
      CT_GetColumnValueByName(pcgCtAft, llCurLine, "FTYP", &rgCurFtyp);
      CT_GetColumnValueByName(pcgCtAft, llCurLine, "ADID", &rgCurAdid);
      CT_GetLineValues(pcpGrid, llCurLine, &rgCtLine);
      if (llDbDelFlag > 0)
      {
        if (llDbInsFlag == 0)
        {
          ilForWhat = 2;
          CT_GetAnyData(pcpGrid, llCurLine, &rgCtData);
          CT_GetAnyList(pcpGrid, llCurLine, &rgCtList);
          dbg(TRACE,"LINE %d UPDATED FIELDS <%s>",llCurLine,rgCtList.Value);
          CreateOutMessage(0, "DFR", "", "", "", "");
          CreateSendAlways(pcpGrid, pcgSendAlwaysFldList, rgCtLine.Value, rgCtData.Value);
          llFldCnt = CT_CountPattern(rgCtList.Value, ",");
          for (llCurFld=0;(llCurFld<=llFldCnt);llCurFld++)
          {
            CT_GetItemsFromTo(pclFldName, rgCtList.Value, llCurFld, llCurFld, ",");
            llColNo = CT_GetColumnNameIndex(pcpGrid, pclFldName);
            CT_GetItemsFromTo(pclNewData, rgCtLine.Value, llColNo, llColNo, ",");
            str_trm_rgt(pclNewData, " ", TRUE);
            if (strlen(pclNewData) == 0)
            {
              strcpy(pclNewData," ");
            }
            CT_GetItemsFromTo(pclOldData, rgCtData.Value, llColNo, llColNo, ",");
            str_trm_rgt(pclOldData, " ", TRUE);
            if (strlen(pclOldData) == 0)
            {
              strcpy(pclOldData," ");
            }
            CreateOutMessage(1, "", pclFldName, pclNewData, pclOldData, "");
          }
          llColNo = CT_GetColumnNameIndex(pcpGrid, "URNO");
          CT_GetItemsFromTo(pclOldData, rgCtLine.Value, llColNo, llColNo, ",");
          str_trm_rgt(pclOldData, " ", TRUE);
          sprintf(pclNewData,"WHERE URNO=%s",pclOldData);
          CreateOutMessage(-2, "DFR", pcpGrid, pclNewData, "", pclOldData);
        }
        else
        {
          dbg(TRACE,"WON'T DELETE OR PUBLISH NOT YET INSERTED RECORD");
          ilForWhat = -1;
        }
      }
      else if (llDbInsFlag > 0)
      {
        if (llDbDelFlag == 0)
        {
          ilForWhat = 1;
          dbg(TRACE,"LINE %d WAS INSERTED INTO DB",llCurLine);
          llColNo = CT_GetColumnNameIndex(pcpGrid, "URNO");
          CT_GetItemsFromTo(pclOldData, rgCtLine.Value, llColNo, llColNo, ",");
          str_trm_rgt(pclOldData, " ", TRUE);
          CreateOutMessage(0, "IFR", pcgCtAftFldLst, rgCtLine.Value, "", pclOldData);
        }
        else
        {
          dbg(TRACE,"WON'T INSERT OR PUBLISH NOT YET DELETED RECORD");
          ilForWhat = -1;
        }
      }
      else if (llDbUpdFlag > 0)
      {
        ilForWhat = 0;
        CT_GetAnyData(pcpGrid, llCurLine, &rgCtData);
        CT_GetAnyList(pcpGrid, llCurLine, &rgCtList);
        dbg(TRACE,"LINE %d (FTYP=%s ADID=%s) UPDATED FIELDS <%s>",
                   llCurLine,rgCurFtyp.Value,rgCurAdid.Value,rgCtList.Value);
        CreateOutMessage(0, "", "", "", "", "");
        CreateSendAlways(pcpGrid, pcgSendAlwaysFldList, rgCtLine.Value, rgCtData.Value);
        llFldCnt = CT_CountPattern(rgCtList.Value, ",");
        for (llCurFld=0;(llCurFld<=llFldCnt);llCurFld++)
        {
          CT_GetItemsFromTo(pclFldName, rgCtList.Value, llCurFld, llCurFld, ",");
          llColNo = CT_GetColumnNameIndex(pcpGrid, pclFldName);
          CT_GetItemsFromTo(pclNewData, rgCtLine.Value, llColNo, llColNo, ",");
          str_trm_rgt(pclNewData, " ", TRUE);
          if (strlen(pclNewData) == 0)
          {
            strcpy(pclNewData," ");
          }
          CT_GetItemsFromTo(pclOldData, rgCtData.Value, llColNo, llColNo, ",");
          str_trm_rgt(pclOldData, " ", TRUE);
          if (strlen(pclOldData) == 0)
          {
            strcpy(pclOldData," ");
          }
          CreateOutMessage(1, "", pclFldName, pclNewData, pclOldData, "");
        }
        llColNo = CT_GetColumnNameIndex(pcpGrid, "URNO");
        CT_GetItemsFromTo(pclOldData, rgCtLine.Value, llColNo, llColNo, ",");
        str_trm_rgt(pclOldData, " ", TRUE);
        sprintf(pclNewData,"WHERE URNO=%s",pclOldData);
        CreateOutMessage(2, "", pcpGrid, pclNewData, "", pclOldData);
      }
      ilDeployMsg = TRUE;
      if ((llCurLine == rgTowDat[1].TowLineNo)&&(rgTowDat[1].TowIsHidden == TRUE))
      {
        dbg(TRACE,"TOW-OUT IS HIDDEN: SUSPEND DEPLOYMENT OF MESSAGE ");
        ilDeployMsg = FALSE;
      }
      if ((llCurLine == rgTowDat[2].TowLineNo)&&(rgTowDat[2].TowIsHidden == TRUE))
      {
        dbg(TRACE,"TOW-IN IS HIDDEN: SUSPEND DEPLOYMENT OF MESSAGE ");
        ilDeployMsg = FALSE;
      }
      if (ilDeployMsg == TRUE)
      {
        switch (ilForWhat)
        {
          case 0:
            dbg(TRACE,"DEPLOY UFR MESSAGE TO BCHDL, LOGHDL AND ACTION");
            dbg(TRACE,"------------------------------------------");
            CreateOutMessage(3, "UFR", pcpGrid, pclNewData, "BLA", pclOldData);
            break;
          case 1:
            dbg(TRACE,"DEPLOY IFR MESSAGE TO LOGHDL AND ACTION");
            dbg(TRACE,"-----------------------------------");
            CreateOutMessage(3, "IFR", pcpGrid, pclOldData, "LA", pclOldData);
            break;
          case 2:
            dbg(TRACE,"DEPLOY DFR MESSAGE TO BCHDL, LOGHDL AND ACTION");
            dbg(TRACE,"------------------------------------------");
            CreateOutMessage(3, "DFR", pcpGrid, pclNewData, "BLA", pclOldData);
            break;
          default:
            break;
        }
      }
    }
  }
  if (ilUpdCnt > 0)
  {
    dbg(TRACE,"CHECK UPDATES: %d GRID LINES HAVE BEEN MODIFIED",ilUpdCnt);
  }
  else
  {
    dbg(TRACE,"CHECK UPDATES: THERE WAS NOTHING TO BE UPDATED");
  }
  return ilUpdCnt;
}

/* *************************************************** */
/* *************************************************** */
static int CreateSendAlways(char *pcpGrid, char *pcpFldList, char *pcpNewData, char *pcpOldData)
{
  int ilRc = RC_SUCCESS;
  char pclFldName[1024] = "";
  char pclNewData[1024] = "";
  char pclOldData[1024] = "";
  long llFldCnt = 0;
  long llCurFld = 0;
  long llColNo = 0;
  if (pcpFldList[0] != '\0')
  {
    dbg(DEBUG,"CREATING SEND ALWAYS FIELDS");
    dbg(DEBUG,"---------------------------");
    llFldCnt = CT_CountPattern(pcpFldList, ",");
    for (llCurFld=0;(llCurFld<=llFldCnt);llCurFld++)
    {
      CT_GetItemsFromTo(pclFldName, pcpFldList, llCurFld, llCurFld, ",");
      llColNo = CT_GetColumnNameIndex(pcpGrid, pclFldName);
      if (llColNo >= 0)
      {
        CT_GetItemsFromTo(pclNewData, pcpNewData, llColNo, llColNo, ",");
        str_trm_rgt(pclNewData, " ", TRUE);
        if (strlen(pclNewData) == 0)
        {
          strcpy(pclNewData," ");
        }
        CT_GetItemsFromTo(pclOldData, pcpOldData, llColNo, llColNo, ",");
        str_trm_rgt(pclOldData, " ", TRUE);
        if (strlen(pclOldData) == 0)
        {
          strcpy(pclOldData," ");
        }
        /* Set the field values into the message format */
        CreateOutMessage(1, "", pclFldName, pclNewData, pclOldData, "");
      }
    }
    dbg(TRACE,"---------------------------");
  }
  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static int CreateOutMessage(int ipForWhat, char *pcpCmd, char *pcpFldName, char *pcpNewData, char *pcpOldData, char *pcpUrno)
{
  static char pclNewFldList[1024] = "";
  static char pclOldFldList[1024] = "";
  static char pclNewFldData[1024] = "";
  static char pclOldFldData[1024] = "";
  static char pclNewValData[1024] = "";
  static char pclOldValData[1024] = "";
  static char pclSqlFldList[1024] = "";
  int ilRc = RC_SUCCESS;
  int ilLen = 0;
  char pclOldName[64] = "";
  char pclSqlName[64] = "";
  char pclOutTab[64] = "";
  char pclOutSel[4096] = "";
  char pclOutFld[4096] = "";
  char pclOutDat[4096] = "";
  char pclNewOutDat[4096] = "";
  char pclOldOutDat[4096] = "";

  switch (ipForWhat)
  {
    case 0:
      /* INIT */
      pclNewFldList[0] = 0x00;
      pclNewFldData[0] = 0x00;
      pclOldFldList[0] = 0x00;
      pclOldFldData[0] = 0x00;
      pclOldValData[0] = 0x00;
      pclNewValData[0] = 0x00;
      pclSqlFldList[0] = 0x00;
      strcpy(pclNewFldList,pcpFldName);
      strcpy(pclNewFldData,pcpNewData);
      dbg(DEBUG,"INIT NEW CMD <%s>",pcpCmd);
      dbg(DEBUG,"INIT NEW FLD <%s>",pcpFldName);
      dbg(DEBUG,"INIT NEW DAT <%s>",pcpNewData);
      dbg(DEBUG,"INIT OLD DAT <%s>",pcpOldData);
      dbg(DEBUG,"INIT URNO    <%s>",pcpUrno);
      break;
    case 1:
      /* Set Updated Field Values */
      sprintf(pclOldName,"#%s",pcpFldName);
      sprintf(pclSqlName,"%s=:V%s",pcpFldName,pcpFldName);
      if (strstr(pclNewFldList,pcpFldName) == NULL)
      {
        if (strcmp(pcpOldData,pcpNewData) == 0)
        {
          dbg(DEBUG,"SEND ALWAYS FIELD: ACT <%s> <%s>",pcpFldName,pcpNewData);
        }
        else
        {
          dbg(TRACE,"SET UPDATED FIELD: NEW <%s> <%s> OLD <%s> <%s>",pcpFldName,pcpNewData,pclOldName,pcpOldData);
        }
        if (pclNewFldList[0] != '\0')
        {
          strcat(pclNewFldList,",");
          strcat(pclNewFldData,",");
          strcat(pclOldFldData,",");
        }
        strcat(pclNewFldList,pcpFldName);
        strcat(pclNewFldData,pcpNewData);
        strcat(pclOldFldData,pcpOldData);
        if (strcmp(pcpOldData,pcpNewData) != 0)
        {
          if (pclOldFldList[0] != '\0')
          {
            strcat(pclOldFldList,",");
            strcat(pclNewValData,",");
            strcat(pclOldValData,",");
            strcat(pclOldValData,",");
            strcat(pclSqlFldList,",");
          }
          strcat(pclOldFldList,pclOldName);
          strcat(pclNewValData,pcpNewData);
          strcat(pclOldValData,pcpOldData);
          strcat(pclSqlFldList,pclSqlName);
        }
      }
      break;
    case 2:
      /* Update the DB Table */
      strcpy(pclOutTab,pcpFldName);
      strcpy(pclOutSel,pcpNewData);
      UpdateDbRecord(pclOutTab, pclOutSel, pclSqlFldList, pclNewValData);
      break;
    case -2:
      /* Delete from DB Table */
      strcpy(pclOutTab,pcpFldName);
      strcpy(pclOutSel,pcpNewData);
      DeleteDbRecord(pclOutTab, pclOutSel, pclSqlFldList, pclNewValData);
      break;
    case 3:
      /* Finalize Message and Send */
      dbg(TRACE,"COMPOSING OUTGOING MESSAGE AS <%s>",pcpCmd);
      dbg(TRACE,"OUT FLAGS (IN OLDDATA) <%s>",pcpOldData);
      dbg(TRACE,"COMPOSE PARAM NEW CMD <%s>",pcpCmd);
      dbg(TRACE,"COMPOSE PARAM NEW FLD <%s>",pcpFldName);
      dbg(TRACE,"COMPOSE PARAM NEW DAT <%s>",pcpNewData);
      dbg(TRACE,"COMPOSE PARAM OLD DAT <%s>",pcpOldData);
      dbg(TRACE,"COMPOSE PARAM URNO    <%s>",pcpUrno);

      if (strlen(pcgTowNewUTWO) == 0)
      {
        strcpy(pcgTowNewUTWO," ");
      }
      if (strlen(pcgTowOldUTWO) == 0)
      {
        strcpy(pcgTowOldUTWO," ");
      }
      if (strlen(pcgTowNewUTWI) == 0)
      {
        strcpy(pcgTowNewUTWI," ");
      }
      if (strlen(pcgTowOldUTWI) == 0)
      {
        strcpy(pcgTowOldUTWI," ");
      }

      strcpy(pclOutFld,"UTWO,UTWI");
      strcat(pclOutFld,",");
      strcpy(pclNewOutDat,"");
      strcat(pclNewOutDat,pcgTowNewUTWO);
      strcat(pclNewOutDat,",");
      strcat(pclNewOutDat,pcgTowNewUTWI);
      strcat(pclNewOutDat,",");
      strcpy(pclOldOutDat,"");
      strcat(pclOldOutDat,pcgTowOldUTWO);
      strcat(pclOldOutDat,",");
      strcat(pclOldOutDat,pcgTowOldUTWI);
      strcat(pclOldOutDat,",");

      strcat(pclOutFld,pclNewFldList);
      if (strlen(pclOldFldList) > 0)
      {
        strcat(pclOutFld,",");
        strcat(pclOutFld,pclOldFldList);
      }

      strcat(pclNewOutDat,pclNewFldData);
      if (strlen(pclOldFldList) > 0)
      {
        strcat(pclNewOutDat,",");
        strcat(pclNewOutDat,pclOldValData);
      }
      strcat(pclOldOutDat,pclOldFldData);
      if (strlen(pclOldFldList) > 0)
      {
        strcat(pclOldOutDat,",");
        strcat(pclOldOutDat,pclNewValData);
      }
      dbg(TRACE,"OUT FIELDS <%s>",pclOutFld);
      dbg(TRACE,"OUT NEW DATA\n<%s>",pclNewOutDat);
      dbg(TRACE,"OUT OLD DATA\n<%s>",pclOldOutDat);
      strcpy(pclOutTab,pcpFldName);
      strcpy(pclOutSel,pcpNewData);
      dbg(TRACE,"OUT TAB <%s>",pclOutTab);
      dbg(TRACE,"OUT SEL <%s>",pclOutSel);
      if (strstr(pcpOldData,"B") != NULL)
      {
        if (strstr(pcgTwStart,".NBC.") == NULL)
        {
          (void) ReleaseActionInfo("1900", "3", pclOutTab, pcpCmd, "", pcpUrno,
                                   pclOutFld, pclNewOutDat, pclOldOutDat);
          dbg(TRACE,"MESSAGE TO BCHDL  (1900) CREATED");
        }
        else
        {
          dbg(TRACE,"MESSAGE TO BCHDL  (1900) NOT CREATED");
          dbg(TRACE,"THIS TRANSACTION WAS IN SILENT MODE (.NBC.)");
        }
      }
      if (strstr(pcpOldData,"A") != NULL)
      {
        (void) ReleaseActionInfo("7400", "3", pclOutTab, pcpCmd, pcpUrno, pclOutSel,
                                 pclOutFld, pclNewOutDat, pclOldOutDat);
        dbg(TRACE,"MESSAGE TO ACTION (7400) CREATED");
      }
      if (strstr(pcpOldData,"L") != NULL)
      {
        (void) ReleaseActionInfo("7700", "3", pclOutTab, pcpCmd, pcpUrno, pclOutSel,
                                 pclOutFld, pclNewOutDat, pclOldOutDat);
        dbg(TRACE,"MESSAGE TO LOGHDL (7700) CREATED");
      }
      break;
    case 4:
      /* Finalize RAC Message and Send */
      dbg(TRACE,"COMPOSING OUTGOING RAC COMMAND");
      if (rgFltDat[1].FltExists == TRUE)
      {
        strcpy(pcgTowRacTifa,rgFltDat[1].StrFltTime);
      }
      else
      {
        strcpy(pcgTowRacTifa,rgFltDat[2].StrFltTime);
      }
      if (rgFltDat[2].FltExists == TRUE)
      {
        strcpy(pcgTowRacTifd,rgFltDat[2].StrFltTime);
      }
      else if (rgTowDat[1].TowIsValid == TRUE)
      {
        strcpy(pcgTowRacTifd,rgTowDat[2].StrTowStoa);
      }
      else
      {
        strcpy(pcgTowRacTifd,pcgPaesArr);
      }
      /*
      if (strcmp(pcgTowRacTifd,"00000000000000") == 0)
      {
        strcpy(pcgTowRacTifd,pcgTowRacTifa);
      }
      if (strcmp(pcgTowRacTifa,"99999999999999") == 0)
      {
        strcpy(pcgTowRacTifa,pcgTowRacTifd);
      }
      */

      strcpy(pclOutTab,"AFTTAB");
      strcpy(pclOutFld,"RKEY");
      strcpy(pclOutSel,pcgTowRacRegn);
      strcat(pclOutSel,",");
      strcat(pclOutSel,pcgTowRacTifa);
      strcat(pclOutSel,",");
      strcat(pclOutSel,pcgTowRacTifd);
      dbg(TRACE,"RAC COMMAND KEY VALUES <%s>",pclOutSel);
      ilLen = strlen(pcgTowRkeyList) - 1;
      if (ilLen > 0)
      {
        if (pcgTowRkeyList[ilLen] == ',')
        {
          pcgTowRkeyList[ilLen] = '\0';
        }
      }
      if (strstr(pcgTwStart,".NBC.") == NULL)
      {
        (void) ReleaseActionInfo("1900", "3", pclOutTab, pcpCmd, "", pclOutSel,
                                 pclOutFld, pcgTowRkeyList, "");
        dbg(TRACE,"MESSAGE TO BCHDL  (1900) CREATED");
      }
      else
      {
        dbg(TRACE,"MESSAGE TO BCHDL  (1900) NOT CREATED");
        dbg(TRACE,"THIS TRANSACTION WAS IN SILENT MODE (.NBC.)");
      }
      break;
    default:
      break;
  }
  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static int UpdateDbRecord(char *pcpTab, char *pcpSel, char *pcpFld, char *pcpDat)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  char pclSqlTab[16] = "";
  char pclSqlFld[1024] = "";
  char pclSqlVal[1024] = "";
  char pclSqlBuf[1024] = "";
  char pclSqlDat[1024] = "";
  short slCursor = 0;
  short slFkt = 0;
  dbg(TRACE,"=====================================================");

  strcpy(pclSqlDat,pcpDat);
  strcpy(pclSqlTab, pcpTab);
  strcpy(pclSqlVal,pcpFld);
  sprintf(pclSqlBuf,"UPDATE %s SET %s %s",pclSqlTab,pclSqlVal,pcpSel);
  dbg(TRACE,"SQL DATA <%s>",pclSqlDat);
  dbg(TRACE,"<%s>",pclSqlBuf);

  slCursor = 0;
  slFkt = IGNORE;
  delton(pclSqlDat);
  ilGetRc = sql_if(slFkt, &slCursor, pclSqlBuf, pclSqlDat);
  if (ilGetRc == DB_SUCCESS)
  {
    commit_work ();
    dbg(TRACE,"RECORD UPDATED");
  }
  else
  {
    dbg (TRACE, "ORA UPDATE FAILED. ROLLBACK!");
    rollback ();
  }
  close_my_cursor (&slCursor);

  dbg(TRACE,"=====================================================");
  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static int DeleteDbRecord(char *pcpTab, char *pcpSel, char *pcpFld, char *pcpDat)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  char pclSqlTab[16] = "";
  char pclSqlFld[1024] = "";
  char pclSqlVal[1024] = "";
  char pclSqlBuf[1024] = "";
  char pclSqlDat[1024] = "";
  short slCursor = 0;
  short slFkt = 0;
  dbg(TRACE,"=====================================================");

  strcpy(pclSqlTab, pcpTab);
  strcpy(pclSqlDat,"");
  strcpy(pclSqlVal,"");

  if (igUsingGortabRelation == TRUE)
  {
    sprintf (pclSqlBuf, "DELETE FROM GORTAB WHERE FLNU IN (SELECT URNO FROM %s %s)", pcpTab, pcpSel);
    dbg(TRACE,"<%s>",pclSqlBuf);
    pclSqlDat[0] = 0x00;
    slCursor = 0;
    ilGetRc = sql_if (IGNORE, &slCursor, pclSqlBuf, pclSqlDat);
    if (ilGetRc == DB_SUCCESS)
    {
      dbg (TRACE, "RECORD(S) DELETED %s", pclSqlDat);
      /*ilRc= RC_SUCCESS;*/
    }                             /* end if */
    else
    {
      /*ilRc = RC_FAIL;*/
    }                             /* end else */
    commit_work ();
    close_my_cursor (&slCursor);
  }

  strcpy(pclSqlTab, pcpTab);
  strcpy(pclSqlDat,"");
  strcpy(pclSqlVal,"");
  sprintf(pclSqlBuf,"DELETE FROM %s %s",pclSqlTab,pcpSel);
  dbg(TRACE,"SQL DATA <%s>",pclSqlDat);
  dbg(TRACE,"<%s>",pclSqlBuf);

  slCursor = 0;
  slFkt = IGNORE;
  delton(pclSqlDat);
  ilGetRc = sql_if(slFkt, &slCursor, pclSqlBuf, pclSqlDat);
  if (ilGetRc == DB_SUCCESS)
  {
    commit_work ();
    dbg(TRACE,"RECORD DELETED");
  }
  else
  {
    dbg (TRACE, "ORA UPDATE FAILED. (NO ROLLBACK!)");
    /*rollback ();*/
  }
  close_my_cursor (&slCursor);

  dbg(TRACE,"=====================================================");
  return ilRc;
}

/********************************************************/
/********************************************************/
static int ReleaseActionInfo(char *pcpRoute, char *pcpPrios, char *pcpTbl, char *pcpCmd, char *pcpUrnoList,
                              char *pcpSel, char *pcpFld, char *pcpDat, char *pcpOldDat)
{
  int ilRC = RC_SUCCESS;
  char pclQuePrio[64];
  sprintf(pclQuePrio,"%s|%s",pcpRoute,pcpPrios);
  (void) ToolsSpoolEventData(pclQuePrio,pcpTbl,pcpCmd,pcpUrnoList,pcpSel,pcpFld,pcpDat,pcpOldDat);

  return ilRC;
}                               /* end ReleaseActionInfo() */

/********************************************************/
/********************************************************/
static void GetTowHdlConfig(char *pcpInfo)
{
  char pclBgnTag[16] = "";
  char pclEndTag[16] = "";
  char pclValue[4096] = "";
  long llLen = 0;
  if (strncmp(pcpInfo,"TOWCFG",6) == 0)
  {
    dbg(TRACE,"INIT TOWHDL CONFIG FROM FLTHDL");
    dbg(TRACE,"==============================");
    CreateUfisTags("HIDETOWG",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pclValue,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    igHideDelTow = atoi(pclValue);
    CreateUfisTags("SKIPOFBL",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pclValue,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    igSkipOfblFlights = atoi(pclValue);
    CreateUfisTags("SKIPSKED",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pclValue,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    igSkipSkedFlights = atoi(pclValue);
    CreateUfisTags("SKIPPABA",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pclValue,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    igSkipPabaFlights = atoi(pclValue);
    CreateUfisTags("TOWEMPTY",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pclValue,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    igTowEmptyStands = atoi(pclValue);
    CreateUfisTags("AUTOAURN",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pclValue,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    igAutoRepairAurn = atoi(pclValue);
    CreateUfisTags("MAXOVNGT",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pclValue,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    igMaxOvnTime = atoi(pclValue);
    CreateUfisTags("OVNGRDHR",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pcgTowOvnHour,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    CreateUfisTags("OVNGRDARR",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pcgTowArrTime,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    CreateUfisTags("OVNGRDDEP",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pcgTowDepTime,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    CreateUfisTags("DEFTOWPS",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pcgTowOutPos,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    CreateUfisTags("MINPARKT",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pclValue,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    igMinPosTime = atoi(pclValue);
    CreateUfisTags("DTOWTIME",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pclValue,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    igTowingTime = atoi(pclValue);


    CreateUfisTags("TESTTOW",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pclValue,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    igTestTowOutTime = atoi(pclValue);


    CreateUfisTags("RULETOWIN",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pclValue,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    igRuleTowInTime = atoi(pclValue);

    CreateUfisTags("GLUETOWOUT",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pclValue,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    igAutoGlueTowOut = atoi(pclValue);
    CreateUfisTags("GLUETOWIN",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pclValue,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    igAutoGlueTowIn = atoi(pclValue);
    CreateUfisTags("GLUETOWING",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pclValue,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    igAutoGlueTowing = atoi(pclValue);

    CreateUfisTags("SKIPTRULE",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pclValue,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    igSkipTRuleTime = atoi(pclValue);
    CreateUfisTags("SKIPARULE",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pclValue,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    igSkipARuleTime = atoi(pclValue);
    CreateUfisTags("SKIPDRULE",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pclValue,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    igSkipDRuleTime = atoi(pclValue);

    CreateUfisTags("SENDALWS",pclBgnTag,pclEndTag);
    CedaGetKeyItem(pcgSendAlwaysFldList,&llLen,pcpInfo,pclBgnTag,pclEndTag, TRUE);
    dbg(TRACE,"SEND ALWAYS FIELDS ARE:\n<%s>",pcgSendAlwaysFldList);
    dbg(TRACE,"==============================");
    PublishTowhdlConfig();
    dbg(TRACE,"==============================");
  }
  return;
}

/*******************************************************/
/*******************************************************/
static void CreateUfisTags(char *pcpName, char *pcpBgn, char * pcpEnd)
{
  sprintf(pcpBgn,"{=%s=}",pcpName);
  sprintf(pcpEnd,"{=/%s=}",pcpName);
  return;
}

/*******************************************************/
/*******************************************************/
static void GetBestRemoteStand(char *pcpArrStand, char *pcpDepStand, char *pcpBgnTime, char *pcpEndTime, char *pcpRmtStand)
{
  int ilGetRc = RC_SUCCESS;
  char pclBestMatch[16] = "";
  long llHitLines = 0;
  long llPstLineNo = 0;
  int ilPreferUnused = TRUE;
  int ilPreferCombi = TRUE;
  int ilLen = 0;
  int ilCnt = 0;
  dbg(TRACE,"-------------------------------------------------------");
  dbg(TRACE,"SEARCH BEST MATCHING REMOTE STAND FOR TOW-OUT FROM <%s>",pcpArrStand);
  dbg(TRACE,"SEARCH BEST MATCHING REMOTE STAND FOR TOW-IN  TO <%s>",pcpDepStand);
  dbg(TRACE,"DESIRED REMOTE PARKING TIME BGN <%s> END <%s>",pcpBgnTime,pcpEndTime);
  dbg(TRACE,"-------------------------------------------------------");

  strcpy(pclBestMatch,pcgTowOutPos);
  llHitLines = CT_GetLinesByColumnValue(pcgCtPst, "PNAM", pcpArrStand, &rgCtLine, 0);
  if (llHitLines > 0)
  {
    dbg(DEBUG,"FOUND %d LINES IN PSTTAB GRID: <%s> FOR ARR",llHitLines,rgCtLine.Value);
    llPstLineNo = atoi(rgCtLine.Value);
    CT_GetColumnValueByName(pcgCtPst, llPstLineNo, "RESN", &rgTowStndArr);
    CT_GetColumnValueByName(pcgCtPst, llPstLineNo, "PRFL", &rgTowPrioArr);
    ReplaceChars(rgTowStndArr.Value, ";", ",");
    ReplaceChars(rgTowPrioArr.Value, ";", ",");
    dbg(TRACE,"RELATED ARR STND <%s>",rgTowStndArr.Value);
    dbg(TRACE,"RELATED ARR SORT <%s>",rgTowPrioArr.Value);

    llHitLines = 0;
    if ((strcmp(pcpArrStand,pcpDepStand) != 0)&&(ilPreferCombi == TRUE))
    {
      llHitLines = CT_GetLinesByColumnValue(pcgCtPst, "PNAM", pcpDepStand, &rgCtLine, 0);
    }
    if (llHitLines > 0)
    {
      dbg(DEBUG,"FOUND %d LINES IN PSTTAB GRID: <%s> FOR DEP",llHitLines,rgCtLine.Value);
      llPstLineNo = atoi(rgCtLine.Value);
      CT_GetColumnValueByName(pcgCtPst, llPstLineNo, "RESN", &rgTowStndDep);
      CT_GetColumnValueByName(pcgCtPst, llPstLineNo, "PRFL", &rgTowPrioDep);
      ReplaceChars(rgTowStndDep.Value, ";", ",");
      ReplaceChars(rgTowPrioDep.Value, ";", ",");
      dbg(TRACE,"RELATED DEP STND <%s>",rgTowStndDep.Value);
      dbg(TRACE,"RELATED DEP SORT <%s>",rgTowPrioDep.Value);
    }
    else
    {
      strcpy(rgTowStndDep.Value,"");
    }

    ilCnt = LoadBlockedStands(pcpBgnTime, pcpEndTime, rgTowStndArr.Value, 0);
    ilCnt = 0;

    if (ilPreferCombi == TRUE)
    {
      ilCnt = CombineTwoLists(argCtValue[3].Value, rgTowStndArr.Value, rgTowStndDep.Value, 0);
      dbg(TRACE,"COMBINED LISTS: COUNT=%d LIST <%s>",ilCnt,argCtValue[3].Value);
    }

    if (ilPreferCombi == FALSE)
    {
      ilCnt = 0;
    }

    if (ilCnt > 0)
    {
      if (igUseGorTabData == TRUE)
      {
        dbg(TRACE,"LOADING OCCUPIED STANDS FROM GORTAB");
        ilCnt = GetStandsFromGortab(pcpBgnTime, pcpEndTime, argCtValue[3].Value, argCtValue[4].Value, 0);
      }
      else
      {
        dbg(TRACE,"LOADING OCCUPIED STANDS FROM AFTTAB AND FOGTAB");
        ilCnt = LoadOccupiedStands(pcpBgnTime, pcpEndTime, argCtValue[3].Value, 0);
        dbg(TRACE,"FOUND %d ALLOCATIONS ON MY COMBINED PREFERRED STANDS",ilCnt);
        ilCnt = PrepareStandAllocation(pcpBgnTime, pcpEndTime, argCtValue[3].Value, argCtValue[4].Value);
        if (ilPreferUnused == FALSE)
        {
          ilCnt = FindBestRemoteStand(pcpBgnTime, pcpEndTime, pclBestMatch);
        }
      }
    }
    if (ilCnt == 0)
    {
      dbg(TRACE,"MUST USE PREFERRED ARRIVAL STANDS");
      if (igUseGorTabData == TRUE)
      {
        dbg(TRACE,"LOADING OCCUPIED STANDS FROM GORTAB");
        ilCnt = GetStandsFromGortab(pcpBgnTime, pcpEndTime, argCtValue[3].Value, argCtValue[4].Value, 0);
      }
      else
      {
        dbg(TRACE,"LOADING OCCUPIED STANDS FROM AFTTAB AND FOGTAB");
        ilCnt = LoadOccupiedStands(pcpBgnTime, pcpEndTime, rgTowStndArr.Value, 0);
        dbg(TRACE,"FOUND %d ALLOCATIONS ON MY ARRIVAL PREFERRED STANDS",ilCnt);
        ilCnt = PrepareStandAllocation(pcpBgnTime, pcpEndTime, rgTowStndArr.Value, argCtValue[4].Value);
        if (ilPreferUnused == FALSE)
        {
          ilCnt = FindBestRemoteStand(pcpBgnTime, pcpEndTime, pclBestMatch);
        }
      }
    }
    if (ilCnt > 0)
    {
      ilCnt = SortItemsByPrioList(argCtValue[5].Value, argCtValue[6].Value, argCtValue[4].Value, rgTowStndArr.Value, rgTowPrioArr.Value, 0);
      dbg(TRACE,"AVAILABLE STANDS <%s> PRIO <%s>",argCtValue[5].Value, argCtValue[6].Value);
    }
    if ((ilCnt > 0)&&(ilPreferUnused == TRUE))
    {
      ilLen = get_real_item(pclBestMatch,argCtValue[5].Value,1);
    }
    else if ((ilCnt > 0)&&(ilPreferUnused == FALSE))
    {
      ilLen = get_real_item(pclBestMatch,argCtValue[5].Value,1);
    }
    else if (ilCnt == 0)
    {
      ilCnt = SortItemsByPrioList(argCtValue[5].Value, argCtValue[6].Value, rgTowStndArr.Value, rgTowStndArr.Value, rgTowPrioArr.Value, 0);
      dbg(TRACE,"USING ALL STANDS <%s> PRIO <%s>",argCtValue[5].Value, argCtValue[6].Value);
      ilLen = get_real_item(pclBestMatch,argCtValue[5].Value,1);
    }
  }
  else
  {
    dbg(TRACE,"THERE ARE NO PREFERRED STANDS DEFINED FOR TOW-OUT FROM <%s>",pcpArrStand);
  }

  str_trm_all(pclBestMatch, "'", TRUE);
  strcpy(pcpRmtStand,pclBestMatch);
  dbg(TRACE,"RESULT: BEST MATCHING REMOTE STAND FOR TOW-OUT FROM <%s> IS <%s>",pcpArrStand,pcpRmtStand);
  return;
}

/* *************************************************** */
/* *************************************************** */
static void ReplaceChars(char *pcpItems, char *pcpGet, char *pcpSet)
{
  char *pclPtr = NULL;
  pclPtr = pcpItems;
  while (*pclPtr != '\0')
  {
    if (*pclPtr == *pcpGet) *pclPtr = *pcpSet;
    pclPtr++;
  }
  return;
}

/* *************************************************** */
/* *************************************************** */
static int CombineTwoLists(char *pcpResult, char *pcpList1, char *pcpList2, int ilForWhat)
{
  int ilCnt1 = 0;
  int ilCnt2 = 0;
  int ilCnt = 0;
  int ilMax = 0;
  int ilItm = 0;
  int ilLen = 0;
  char pclCopy1[4096] = "";
  char pclCopy2[4096] = "";
  char *pclPtr1 = NULL;
  char *pclPtr2 = NULL;
  char pclValue[32] = "";
  char pclCheck[32] = "";
  pcpResult[0] = 0x00;
  ilCnt =0;
  ilCnt1 = field_count(pcpList1);
  ilCnt2 = field_count(pcpList2);
  switch (ilForWhat)
  {
    case 0:
      /* Result: Items existing in both lists */
      /* Result: SCHNITTMENGE = INTERSECTION */
      if (ilCnt1 >= ilCnt2)
      {
        if (ilCnt2 > 0)
        {
          ilMax = ilCnt2;
          pclPtr1 = pcpList1;
          pclPtr2 = pcpList2;
        }
      }
      else
      {
        if (ilCnt1 > 0)
        {
          ilMax = ilCnt1;
          pclPtr1 = pcpList2;
          pclPtr2 = pcpList1;
        }
      }
      if (pclPtr2 != NULL)
      {
        sprintf(pclCopy1,",%s,",pclPtr1);
        for(ilItm=1;ilItm<=ilMax;ilItm++)
        {
          ilLen = get_real_item(pclValue,pclPtr2,ilItm);
          sprintf(pclCheck,",%s,",pclValue);
          if (strstr(pclCopy1,pclCheck) != NULL)
          {
            strcat(pcpResult,pclValue);
            strcat(pcpResult,",");
            ilCnt++;
          }
        }
        ilLen = strlen(pcpResult) - 1;
        if (ilLen > 0)
        {
          pcpResult[ilLen] = 0x00;
        }
      }
      break;
    case 1:
      /* Result: Items existing in List2 but missing in List1 */
      /* Result: SUBSET OF ITEMS REMOVED FROM LIST2 */
      if (ilCnt1 == 0)
      {
        strcpy(pcpResult,pcpList2);
        ilCnt = ilCnt2;
      }
      else if (ilCnt2 > 0)
      {
        ilMax = ilCnt2;
        pclPtr1 = pcpList1;
        pclPtr2 = pcpList2;
      }
      if (pclPtr2 != NULL)
      {
        sprintf(pclCopy1,",%s,",pclPtr1);
        for(ilItm=1;ilItm<=ilMax;ilItm++)
        {
          ilLen = get_real_item(pclValue,pclPtr2,ilItm);
          sprintf(pclCheck,",%s,",pclValue);
          if (strstr(pclCopy1,pclCheck) == NULL)
          {
            strcat(pcpResult,pclValue);
            strcat(pcpResult,",");
            ilCnt++;
          }
        }
        ilLen = strlen(pcpResult) - 1;
        if (ilLen > 0)
        {
          pcpResult[ilLen] = 0x00;
        }
      }
      break;
    case 2:
      /* Result: Items existing in List1 but missing in List2 */
      /* Result: SUBSET OF ITEMS ADDED TO LIST1 */
      if (ilCnt2 == 0)
      {
        strcpy(pcpResult,pcpList1);
        ilCnt = ilCnt1;
      }
      else if (ilCnt1 > 0)
      {
        ilMax = ilCnt1;
        pclPtr1 = pcpList2;
        pclPtr2 = pcpList1;
      }
      if (pclPtr2 != NULL)
      {
        sprintf(pclCopy1,",%s,",pclPtr1);
        for(ilItm=1;ilItm<=ilMax;ilItm++)
        {
          ilLen = get_real_item(pclValue,pclPtr2,ilItm);
          sprintf(pclCheck,",%s,",pclValue);
          if (strstr(pclCopy1,pclCheck) == NULL)
          {
            strcat(pcpResult,pclValue);
            strcat(pcpResult,",");
            ilCnt++;
          }
        }
        ilLen = strlen(pcpResult) - 1;
        if (ilLen > 0)
        {
          pcpResult[ilLen] = 0x00;
        }
      }
      break;
    default:
    break;
  }


  return ilCnt;
}

/* *************************************************** */
/* *************************************************** */
static int SortItemsByPrioList(char *pcpOutList, char *pcpOutPrio, char *pcpGetList, char *pcpItmList, char *pcpItmPrio, int ilForWhat)
{
  int ilCnt = 0;
  int ilMax = 0;
  int ilItm = 0;
  int ilLen = 0;
  char pclLine[32] = "";
  char pclData[32] = "";
  char pclSort[32] = "";
  char pclChkValue[32] = "";
  long llCurLine = 0;
  long llMaxLine = 0;
  pcpOutList[0] = 0x00;
  pcpOutPrio[0] = 0x00;
  ilCnt =0;
  switch (ilForWhat)
  {
    case 0:
      /* Result:  List of 'GetItems' ordered by Prio ascendend */
      CT_ResetContent(pcgSorter);
      ilMax = field_count(pcpItmList);
      for(ilItm=1;ilItm<=ilMax;ilItm++)
      {
        ilLen = get_real_item(pclData,pcpItmList,ilItm);
        ilLen = get_real_item(pclSort,pcpItmPrio,ilItm);
        sprintf(pclLine,"%s,%s",pclData,pclSort);
        CT_InsertTextLine(pcgSorter,pclLine);
      }
      CT_SortByColumnNames(pcgSorter, "SORT", SORT_ASC);
      llMaxLine = CT_GetLineCount(pcgSorter) - 1;
      for (llCurLine=0;llCurLine<=llMaxLine;llCurLine++)
      {
        CT_GetColumnValueByName(pcgSorter, llCurLine, "DATA", &rgCurData);
        CT_GetColumnValueByName(pcgSorter, llCurLine, "SORT", &rgCurSort);
        if (strstr(pcpGetList,rgCurData.Value) != NULL)
        {
          strcat(pcpOutList,rgCurData.Value);
          strcat(pcpOutList,",");
          strcat(pcpOutPrio,rgCurSort.Value);
          strcat(pcpOutPrio,",");
          ilCnt++;
        }
      }
      if (ilCnt > 0)
      {
        ilLen = strlen(pcpOutList) - 1;
        pcpOutList[ilLen] = 0x00;
        ilLen = strlen(pcpOutPrio) - 1;
        pcpOutPrio[ilLen] = 0x00;
      }
      break;
    case 1:
      break;
    case 2:
      break;
    default:
    break;
  }
  dbg(DEBUG,"ALL ITEM LIST <%s>",pcpItmList);
  dbg(DEBUG,"ALL PRIO LIST <%s>",pcpItmPrio);
  dbg(DEBUG,"GET ITEM LIST <%s>",pcpGetList);
  dbg(DEBUG,"OUT ITEM LIST <%s>",pcpOutList);
  dbg(DEBUG,"OUT PRIO LIST <%s>",pcpOutPrio);
  return ilCnt;
}


/* *************************************************** */
/* *************************************************** */
static int GetStandsFromGortab(char *pcpBgnTime, char *pcpEndTime, char *pcpArrPst, char *pcpUnusedStands, int ipForWhat)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  char pclBgn[16] = "";
  char pclEnd[16] = "";
  char pclAftRkey[16] = "";
  char pclTblName[16] = "";
  char pclFldList[1024] = "";
  char pclSqlCond[4096] = "";
  char pclSqlData[4096] = "";
  char pclSqlBuff[4096] = "";
  long llMaxAftLine = 0;
  long llCurAftLine = 0;
  long llLineNo = 0;
  int ilGapOffBgn = 60;
  int ilGapOffEnd = 60;
  int ilValBgn = 0;
  int ilValEnd = 0;
  int ilValDif = 0;
  int ilValHours = 0;
  int ilDummy = 0;
  int ilFldCnt = 0;
  int ilCnt = 0;
  int ilCntAft = 0;

  short slCursor = 0;
  short slFkt = 0;

  dbg(TRACE,"LOADING OCCUPATION LIST OF SELECTED REMOTE STANDS");
  dbg(TRACE,"-------------------------------------------------");

  strcpy(pclBgn,pcpBgnTime);
  strcpy(pclEnd,pcpEndTime);

  /* Workaround to cover longterm parking a bit */
  (void) GetFullDay(pclBgn, &ilDummy, &ilValBgn, &ilDummy);
  (void) GetFullDay(pclEnd, &ilDummy, &ilValEnd, &ilDummy);
  ilValDif = ilValEnd - ilValBgn + 1;
  ilValHours = ((ilValDif-1) / 60) + 1;
  dbg(TRACE,"TIMEFRAME BGN <%s> END <%s> (%d MIN) (%d HOURS)",pclBgn,pclEnd,ilValDif,ilValHours);

  ilValBgn -= ilGapOffBgn;
  (void) FullTimeString(pclBgn, ilValBgn, "00");
  ilValEnd += ilGapOffEnd;
  (void) FullTimeString(pclEnd, ilValEnd, "00");

  ilValDif = ilValEnd - ilValBgn + 1;
  ilValHours = ((ilValDif-1) / 60) + 1;
  dbg(TRACE,"GAP FRAME BGN <%s> END <%s> (%d MIN) (%d HOURS)",pclBgn,pclEnd,ilValDif,ilValHours);

  strcpy(pclTblName,pcgCtAft);
  strcpy(pclFldList,pcgCtTowFldLst);

  ilCnt = FLT_GetGortabData(pclBgn, pclEnd, pcpArrPst, ipForWhat, &rgGorPosList);

  dbg(TRACE,"FOUND %d ALLOCATION RECORDS IN TOTAL",ilCnt);
  dbg(TRACE,"RESULT: <%s>",rgGorPosList.Value);
  if (strlen(pcgBlockedStands) > 0)
  {
    if (strlen(rgGorPosList.Value) > 0)
    {
      strcat(rgGorPosList.Value,",");
    }
    strcat(rgGorPosList.Value,pcgBlockedStands);
  }

  dbg(TRACE,"-----------------------------------------------");
  ilCnt = CombineTwoLists(pcpUnusedStands, rgGorPosList.Value, pcpArrPst, 1);
  dbg(TRACE,"LIST OF PREFERRED  STANDS: <%s>",pcpArrPst);
  dbg(TRACE,"OCCUPIED / BLOCKED STANDS: <%s>",rgGorPosList.Value);
  dbg(TRACE,"FREE AND AVAILABLE STANDS: <%s>",pcpUnusedStands);
  dbg(TRACE,"-----------------------------------------------");

  dbg(DEBUG,"RETURN COUNT VALUE: %d",ilCnt);
  return ilCnt;
}


/* *************************************************** */
/* *************************************************** */
static int LoadOccupiedStands(char *pcpBgnTime, char *pcpEndTime, char *pcpArrPst, int ipForWhat)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  char pclBgn[16] = "";
  char pclEnd[16] = "";
  char pclAftRkey[16] = "";
  char pclTblName[16] = "";
  char pclFldList[1024] = "";
  char pclSqlCond[4096] = "";
  char pclSqlData[4096] = "";
  char pclSqlBuff[4096] = "";
  long llMaxAftLine = 0;
  long llCurAftLine = 0;
  long llLineNo = 0;
  int ilGapOffBgn = 60;
  int ilGapOffEnd = 60;
  int ilValBgn = 0;
  int ilValEnd = 0;
  int ilValDif = 0;
  int ilValHours = 0;
  int ilDummy = 0;
  int ilFldCnt = 0;
  int ilCnt = 0;
  int ilCntAft = 0;

  short slCursor = 0;
  short slFkt = 0;

  dbg(TRACE,"LOADING PARKED FLIGHTS ON SELECTED REMOTE STANDS");
  dbg(TRACE,"------------------------------------------------");

  strcpy(pclBgn,pcpBgnTime);
  strcpy(pclEnd,pcpEndTime);

  /* Workaround to cover longterm parking a bit */
  (void) GetFullDay(pclBgn, &ilDummy, &ilValBgn, &ilDummy);
  (void) GetFullDay(pclEnd, &ilDummy, &ilValEnd, &ilDummy);
  ilValDif = ilValEnd - ilValBgn + 1;
  ilValHours = ((ilValDif-1) / 60) + 1;
  dbg(TRACE,"TIMEFRAME BGN <%s> END <%s> (%d MIN) (%d HOURS)",pclBgn,pclEnd,ilValDif,ilValHours);

  ilValBgn -= ilGapOffBgn;
  (void) FullTimeString(pclBgn, ilValBgn, "00");
  ilValEnd += ilGapOffEnd;
  (void) FullTimeString(pclEnd, ilValEnd, "00");

  ilValDif = ilValEnd - ilValBgn + 1;
  ilValHours = ((ilValDif-1) / 60) + 1;
  dbg(TRACE,"GAP FRAME BGN <%s> END <%s> (%d MIN) (%d HOURS)",pclBgn,pclEnd,ilValDif,ilValHours);

  strcpy(pclTblName,pcgCtAft);
  strcpy(pclFldList,pcgCtTowFldLst);

  if ((ipForWhat == 0)||(ipForWhat == 1))
  {
    /* Reset for initial load */
    CT_ResetContent(pcgCtTow);

    if (ipForWhat == 0)
    {
      dbg(TRACE,"READING FLIGHT RECORDS FROM AFTTAB ONLY");
      dbg(TRACE,"---------------------------------------");
      sprintf(pclSqlCond,"((TIFA BETWEEN '%s' AND '%s') AND PSTA IN (%s)) AND DES3='%s'",
                            pclBgn,pclEnd,pcpArrPst,pcgHomeAp);
    }
    else
    {
      dbg(TRACE,"READING FLIGHT RECORDS FROM AFTTAB AND FOGTAB");
      dbg(TRACE,"---------------------------------------------");
      sprintf(pclSqlCond,"((TIFA BETWEEN '%s' AND '%s' AND DES3='%s') "
                           "OR (RKEY IN (SELECT FLNU FROM FOGTAB WHERE OGES>='%s' AND OGBS<='%s'))) "
                           "AND PSTA IN (%s)",
                            pclBgn,pclEnd,pcgHomeAp,pclBgn,pclEnd,pcpArrPst);
    }

    strcat(pclSqlCond," AND FTYP IN ('O','S','T','G','B','Z') AND STAT<>'DEL'");
    /* NO SORT: Must sort the grid data anyway */
    /* strcat(pclSqlCond," ORDER BY PSTA,TIFA,PAES"); */
    sprintf(pclSqlBuff,"SELECT %s FROM %s WHERE %s",pclFldList,pclTblName,pclSqlCond);
    dbg(TRACE,"<%s>",pclSqlBuff);

    dbg(TRACE,"-----------------READ DB BGN-----------------");
    ilFldCnt = field_count(pclFldList);
    ilCnt = 0;
    slCursor = 0;
    slFkt = START;
    ilGetRc = RC_SUCCESS;
    while (ilGetRc == DB_SUCCESS)
    {
      ilGetRc = sql_if(slFkt,&slCursor,pclSqlBuff,pclSqlData);
	  if (ilGetRc == DB_SUCCESS)
      {
        BuildItemBuffer(pclSqlData,"",ilFldCnt,",");
        ilCnt++;
        dbg(DEBUG,"%d: <%s>",ilCnt,pclSqlData);
        CT_InsertTextLine(pcgCtTow,pclSqlData);
      }
      slFkt = NEXT;
    }
    close_my_cursor(&slCursor);
    dbg(TRACE,"-----------------READ DB END-----------------");
    dbg(TRACE,"FOUND %d ALLOCATION RECORDS FROM AFTTAB",ilCnt);
  }

  if ((ipForWhat == 0)||(ipForWhat == 2))
  {
    dbg(TRACE,"MERGING FLIGHT RECORDS SELECTED FROM FOGTAB");
    dbg(TRACE,"-------------------------------------------");

    sprintf(pclSqlCond,"RKEY IN (SELECT FLNU FROM FOGTAB WHERE OGES>='%s' AND OGBS<='%s') "
                       "AND PSTA IN (%s)",
                        pclBgn,pclEnd,pcpArrPst);
    strcat(pclSqlCond," AND FTYP IN ('O','S','T','G','B','Z') AND STAT<>'DEL'");
    /* NO SORT: Must sort the grid data anyway */
    /* strcat(pclSqlCond," ORDER BY PSTA,TIFA,PAES"); */
    sprintf(pclSqlBuff,"SELECT %s FROM %s WHERE %s",pclFldList,pclTblName,pclSqlCond);
    dbg(TRACE,"<%s>",pclSqlBuff);

    dbg(TRACE,"-----------------READ DB BGN---------------");
    ilFldCnt = field_count(pclFldList);
    ilCntAft = ilCnt;
    ilCnt = 0;
    slCursor = 0;
    slFkt = START;
    ilGetRc = RC_SUCCESS;
    while (ilGetRc == DB_SUCCESS)
    {
      ilGetRc = sql_if(slFkt,&slCursor,pclSqlBuff,pclSqlData);
	  if (ilGetRc == DB_SUCCESS)
      {
        BuildItemBuffer(pclSqlData,"",ilFldCnt,",");
        ilCnt++;
        dbg(DEBUG,"%d: <%s>",ilCnt,pclSqlData);
        CT_InsertTextLine(pcgCtTow,pclSqlData);
      }
      slFkt = NEXT;
    }
    close_my_cursor(&slCursor);
    dbg(TRACE,"-----------------READ DB END---------------");
    dbg(TRACE,"FOUND %d ALLOCATION RECORDS FROM FOGTAB",(ilCnt-ilCntAft));
  }
  dbg(TRACE,"FOUND %d ALLOCATION RECORDS IN TOTAL",ilCnt);

  return ilCnt;
}


/* *************************************************** */
/* *************************************************** */
static int LoadBlockedStands(char *pcpBgnTime, char *pcpEndTime, char *pcpArrPst, int ipForWhat)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  char pclBgn[16] = "";
  char pclEnd[16] = "";
  char pclTblName[16] = "";
  char pclFldList[1024] = "";
  char pclSqlCond[4096] = "";
  char pclSqlData[4096] = "";
  char pclSqlBuff[4096] = "";
  char pclTmpData[16] = "";
  char pclTmpItem[16] = "";
  int ilGapOffBgn = 60;
  int ilGapOffEnd = 60;
  int ilValBgn = 0;
  int ilValEnd = 0;
  int ilValDif = 0;
  int ilValHours = 0;
  int ilDummy = 0;
  int ilCnt = 0;
  int ilLen = 0;

  short slCursor = 0;
  short slFkt = 0;

  dbg(TRACE,"LOADING BLOCKED STANDS");
  dbg(TRACE,"----------------------");

  strcpy(pclBgn,pcpBgnTime);
  strcpy(pclEnd,pcpEndTime);

  /* Workaround to cover longterm parking a bit */
  (void) GetFullDay(pclBgn, &ilDummy, &ilValBgn, &ilDummy);
  (void) GetFullDay(pclEnd, &ilDummy, &ilValEnd, &ilDummy);
  ilValDif = ilValEnd - ilValBgn + 1;
  ilValHours = ((ilValDif-1) / 60) + 1;
  dbg(TRACE,"TIMEFRAME BGN <%s> END <%s> (%d MIN) (%d HOURS)",pclBgn,pclEnd,ilValDif,ilValHours);

  ilValBgn -= ilGapOffBgn;
  (void) FullTimeString(pclBgn, ilValBgn, "00");
  ilValEnd += ilGapOffEnd;
  (void) FullTimeString(pclEnd, ilValEnd, "00");

  ilValDif = ilValEnd - ilValBgn + 1;
  ilValHours = ((ilValDif-1) / 60) + 1;
  dbg(TRACE,"GAP FRAME BGN <%s> END <%s> (%d MIN) (%d HOURS)",pclBgn,pclEnd,ilValDif,ilValHours);

  strcpy(pcgBlockedStands,"");

  strcpy(pclTblName,"PSTTAB");
  strcpy(pclFldList,"PNAM");

    sprintf(pclSqlCond,"URNO IN "
                       "(SELECT DISTINCT(BURN) FROM BLKTAB WHERE NAFR<='%s' AND NATO>='%s' AND TABN='PST')",
                        pclEnd,pclBgn);
    sprintf(pclSqlBuff,"SELECT %s FROM %s WHERE %s",pclFldList,pclTblName,pclSqlCond);
    dbg(TRACE,"<%s>",pclSqlBuff);

    dbg(TRACE,"-----------------READ DB BGN---------------");
    ilCnt = 0;
    slCursor = 0;
    slFkt = START;
    ilGetRc = RC_SUCCESS;
    while (ilGetRc == DB_SUCCESS)
    {
      ilGetRc = sql_if(slFkt,&slCursor,pclSqlBuff,pclSqlData);
	  if (ilGetRc == DB_SUCCESS)
      {
        ilCnt++;
        ilLen = get_real_item(pclTmpItem,pclSqlData,1);
        sprintf(pclTmpData,"'%s',",pclTmpItem);
        strcat(pcgBlockedStands,pclTmpData);
      }
      slFkt = NEXT;
    }
    close_my_cursor(&slCursor);
    dbg(TRACE,"-----------------READ DB END---------------");
    ilLen = strlen(pcgBlockedStands);
    if (ilLen > 0)
    {
      ilLen--;
      pcgBlockedStands[ilLen] = 0x00;
    }
    dbg(TRACE,"FOUND %d BLOCKED STANDS IN BLKTAB",ilCnt);
    dbg(TRACE,"<%s>",pcgBlockedStands);

  return ilCnt;
}



/*******************************************************/
/*******************************************************/
static int PrepareStandAllocation(char *pcpBgnTime, char *pcpEndTime, char *pcpFetchedStands, char *pcpUnusedStands)
{
  int ilRc = RC_SUCCESS;
  long llMaxLine = 0;
  long llCurLine = 0;
  int ilLen = 0;
  int ilCnt = 0;
  int ilItm = 0;
  int ilMax = 0;
  char pclFreeStands[4096] = "";
  char pclAvalStands[4096] = "";
  char pclUsedStands[4096] = "";
  char pclGridStands[4096] = "";
  char pclAnyData[16] = "";
  char pclStand[16] = "";
  char pclMinStoa[16] = "";
  char pclMaxStod[16] = "";
  long llFirstLine = 0;
  int ilMerged = FALSE;
  int ilUpdBgn = FALSE;
  int ilUpdEnd = FALSE;

  dbg(TRACE,"PREPARE ALLOCATION GAPS FOR BGN <%s> END <%s>",pcpBgnTime,pcpEndTime);
  dbg(TRACE,"-----------------------------------------------");
  dbg(DEBUG,"STEP 1: REMOVE DOUBLE RECORDS");
  dbg(DEBUG,"SORT GRID LINES BY URNO (ASCENDEND)");
  dbg(DEBUG,"-----------------------------------------------");
  CT_SortByColumnNames(pcgCtTow, "URNO", SORT_ASC);
  llMaxLine = CT_GetLineCount(pcgCtTow) - 1;
  llCurLine = 0;
  strcpy(rgPrvUrno.Value,"----------");
  while (llCurLine<=llMaxLine)
  {
    CT_GetColumnValueByName(pcgCtTow, llCurLine, "URNO", &rgCurUrno);
    if (strcmp(rgPrvUrno.Value,rgCurUrno.Value) == 0)
    {
      CT_DeleteDataLine(pcgCtTow, llCurLine);
      llCurLine--;
      llMaxLine--;
      dbg(DEBUG,"DOUBLE URNO <%s> REMOVED",rgCurUrno.Value);
    }
    strcpy(rgPrvUrno.Value,rgCurUrno.Value);
    llCurLine++;
  }

  dbg(DEBUG,"-----------------------------------------------");
  dbg(TRACE,"STEP 2A: REMOVE RECORDS OUTSIDE MY TIMEFRAME");
  dbg(DEBUG,"-----------------------------------------------");
  llMaxLine = CT_GetLineCount(pcgCtTow) - 1;
  llCurLine = 0;
  while (llCurLine<=llMaxLine)
  {
    CT_GetColumnValueByName(pcgCtTow, llCurLine, "PSTA", &rgCurPsta);
    CT_GetColumnValueByName(pcgCtTow, llCurLine, "PABS", &rgCurStoa);
    CT_GetColumnValueByName(pcgCtTow, llCurLine, "PAES", &rgCurStod);
    if (strcmp(rgCurStod.Value,pcpBgnTime) < 0)
    {
      CT_DeleteDataLine(pcgCtTow, llCurLine);
      llCurLine--;
      llMaxLine--;
      dbg(DEBUG,"STAND <%s> PAES<BGN <%s> (PABS <%s> PAES <%s>) REMOVED",
                 rgCurPsta.Value,pcpBgnTime,rgCurStoa.Value,rgCurStod.Value);
    }
    else if (strcmp(rgCurStoa.Value,pcpEndTime) > 0)
    {
      CT_DeleteDataLine(pcgCtTow, llCurLine);
      llCurLine--;
      llMaxLine--;
      dbg(DEBUG,"STAND <%s> PABS>END <%s> (PABS <%s> PAES <%s>) REMOVED",
                 rgCurPsta.Value,pcpEndTime,rgCurStoa.Value,rgCurStod.Value);
    }
    llCurLine++;
  }

  dbg(DEBUG,"-----------------------------------------------");
  dbg(TRACE,"STEP 2B: CHECK TIME FRAMES OF LOADED STAND USAGE");
  dbg(DEBUG,"SORT GRID LINES BY STAND AND PABS (ASCENDEND)");
  dbg(DEBUG,"-----------------------------------------------");
  CT_SortByColumnNames(pcgCtTow, "PABS", SORT_ASC);
  CT_SortByColumnNames(pcgCtTow, "PSTA", SORT_ASC);
  ilCnt = 0;
  strcpy(pclGridStands,""); /* unique list of stands in the grid */
  llMaxLine = CT_GetLineCount(pcgCtTow) - 1;
  for (llCurLine=0;llCurLine<=llMaxLine;llCurLine++)
  {
    CT_GetColumnValueByName(pcgCtTow, llCurLine, "PSTA", &rgCurPsta);
    str_trm_rgt(rgCurPsta.Value, " ", TRUE);
    sprintf(pclAnyData,"'%s'",rgCurPsta.Value);
    /* dbg(TRACE,"LINE %d STAND <%s>",llCurLine,pclAnyData); */
    if (strstr(pclGridStands,pclAnyData) == NULL)
    {
      strcat(pclGridStands,pclAnyData);
      strcat(pclGridStands,",");
      ilCnt++;
    }
  }
  ilLen = strlen(pclGridStands) - 1;
  if (ilLen > 0)
  {
    pclGridStands[ilLen] = 0x00;
  }
  dbg(DEBUG,"LOOP THROUGH LOADED STANDS <%s>",pclGridStands);

  ilMax = ilCnt;
  for (ilItm=1;ilItm<=ilMax;ilItm++)
  {
    ilLen = get_real_item(pclStand,pclGridStands,ilItm);
    llFirstLine = -1;
    ilMerged = FALSE;
    strcpy(pclMinStoa,"99999999999999");
    strcpy(pclMaxStod,"00000000000000");
    ilUpdBgn = FALSE;
    ilUpdEnd = FALSE;
    llCurLine = 0;
    llMaxLine = CT_GetLineCount(pcgCtTow) - 1;
    dbg(DEBUG,"-----------------------------------------------");
    dbg(DEBUG,"CHECK STAND <%s> IN %d GRID LINES",pclStand,(llMaxLine+1));
    while (llCurLine<=llMaxLine)
    {
      CT_GetColumnValueByName(pcgCtTow, llCurLine, "PSTA", &rgCurPsta);
      str_trm_rgt(rgCurPsta.Value, " ", TRUE);
      sprintf(pclAnyData,"'%s'",rgCurPsta.Value);
      if (strcmp(pclStand,pclAnyData) == 0)
      {
        CT_GetColumnValueByName(pcgCtTow, llCurLine, "PABS", &rgCurStoa);
        CT_GetColumnValueByName(pcgCtTow, llCurLine, "PAES", &rgCurStod);
        if ((strcmp(pcpBgnTime,rgCurStod.Value) <= 0)&&(strcmp(pcpEndTime,rgCurStoa.Value) >= 0))
        {
          dbg(DEBUG,"OVERLAP <%s> (MIN <%s> MAX <%s>) ",pclAnyData,pclMinStoa,pclMaxStod);
          dbg(DEBUG,"OVERLAP <%s> (BGN <%s> END <%s>) ",pclAnyData,rgCurStoa.Value,rgCurStod.Value);
          if (strcmp(pclMinStoa,rgCurStoa.Value) >= 0)
          {
            strcpy(pclMinStoa,rgCurStoa.Value);
            ilMerged = TRUE;
            ilUpdBgn = TRUE;
          }
          if (strcmp(pclMaxStod,rgCurStod.Value) <= 0)
          {
            strcpy(pclMaxStod,rgCurStod.Value);
            ilMerged = TRUE;
            ilUpdEnd = TRUE;
          }
          if (ilMerged == TRUE)
          {
            if (llFirstLine >= 0)
            {
              CT_DeleteDataLine(pcgCtTow, llCurLine);
              llCurLine--;
              llMaxLine--;
            }
            else
            {
              llFirstLine = llCurLine;
            }
          }
        }
        else
        {
          dbg(DEBUG,"DESIRED <%s> (MIN <%s> MAX <%s>) ",pclAnyData,pcpBgnTime,pcpEndTime);
          dbg(DEBUG,"OUTSIDE <%s> (BGN <%s> END <%s>) ",pclAnyData,rgCurStoa.Value,rgCurStod.Value);
          CT_DeleteDataLine(pcgCtTow, llCurLine);
          llCurLine--;
          llMaxLine--;
          dbg(DEBUG,"DATA LINE <%s> (BGN <%s> END <%s>) DELETED",pclAnyData,rgCurStoa.Value,rgCurStod.Value);
        }
        dbg(DEBUG,"---------");
      }
      ilMerged = FALSE;
      llCurLine++;
    }
    if (llFirstLine >= 0)
    {
      if (ilUpdBgn == TRUE)
      {
        dbg(DEBUG,"SET MIN PABS <%s>",pclMinStoa);
        CT_SetColumnValueByName(pcgCtTow, llFirstLine, "PABS", pclMinStoa);
      }
      if (ilUpdEnd == TRUE)
      {
        dbg(DEBUG,"SET MAX PAES <%s>",pclMaxStod);
        CT_SetColumnValueByName(pcgCtTow, llFirstLine, "PAES", pclMaxStod);
      }
      CT_GetColumnValueByName(pcgCtTow, llFirstLine, "PABS", &rgCurStoa);
      CT_GetColumnValueByName(pcgCtTow, llFirstLine, "PAES", &rgCurStod);
      dbg(DEBUG,"FINAL STAND <%s> OCCUPATION PABS <%s> PAES <%s>",pclStand,rgCurStoa.Value,rgCurStod.Value);
    }
  }


  dbg(DEBUG,"-----------------------------------------------");
  dbg(TRACE,"STEP 3: CREATE LIST OF USED (NOT FREE) STANDS");
  dbg(DEBUG,"-----------------------------------------------");
  strcpy(pclGridStands,pcgBlockedStands);
  strcat(pclGridStands,",");
  llMaxLine = CT_GetLineCount(pcgCtTow) - 1;
  for (llCurLine=0;llCurLine<=llMaxLine;llCurLine++)
  {
    CT_GetColumnValueByName(pcgCtTow, llCurLine, "PSTA", &rgCurPsta);
    str_trm_rgt(rgCurPsta.Value, " ", TRUE);
    sprintf(pclAnyData,"'%s'",rgCurPsta.Value);
    CT_GetColumnValueByName(pcgCtTow, llCurLine, "PABS", &rgCurStoa);
    CT_GetColumnValueByName(pcgCtTow, llCurLine, "PAES", &rgCurStod);
    dbg(DEBUG,"LINE %d\tSTAND <%s> OCCUPATION PABS <%s> PAES <%s>",llCurLine,pclAnyData,rgCurStoa.Value,rgCurStod.Value);
    if (strstr(pclGridStands,pclAnyData) == NULL)
    {
      strcat(pclGridStands,pclAnyData);
      strcat(pclGridStands,",");
    }
    else
    {
      dbg(DEBUG,"OOOOOPS <%s> ???",pclAnyData);
    }
  }
  dbg(DEBUG,"-----------------------------------------------");
  ilLen = strlen(pclGridStands) - 1;
  if (ilLen > 0)
  {
    pclGridStands[ilLen] = 0x00;
  }
  dbg(DEBUG,"BLOCKED OR OCCUPIED STANDS ARE <%s> (FINAL LIST)",pclGridStands);


/*
  dbg(DEBUG,"-----------------------------------------------");
  dbg(DEBUG,"STEP 5: GET LIST OF STANDS WITH MATCHING GAPS");
  dbg(DEBUG,"-----------------------------------------------");
  strcpy(pclFreeStands,"");
  strcpy(pclAvalStands,"");
  strcpy(pclUsedStands,"");
  llMaxLine = CT_GetLineCount(pcgCtTow) - 1;
  for (llCurLine=0;llCurLine<=llMaxLine;llCurLine++)
  {
    CT_GetColumnValueByName(pcgCtTow, llCurLine, "PSTA", &rgCurPsta);
    str_trm_rgt(rgCurPsta.Value, " ", TRUE);
    sprintf(pclAnyData,"'%s'",rgCurPsta.Value);
    CT_GetColumnValueByName(pcgCtTow, llCurLine, "PABS", &rgCurStoa);
    CT_GetColumnValueByName(pcgCtTow, llCurLine, "PAES", &rgCurStod);
    if (strstr(pclGridStands,pclAnyData) == NULL)
    {
      strcat(pclGridStands,pclAnyData);
      strcat(pclGridStands,",");
    }
    if ((strcmp(pcpEndTime,rgCurStod.Value) > 0)&&(strcmp(pcpBgnTime,rgCurStoa.Value) < 0))
    {
      if (strstr(pclUsedStands,pclAnyData) == NULL)
      {
        strcat(pclUsedStands,pclAnyData);
        strcat(pclUsedStands,",");
      }
    }
    if ((strcmp(pcpEndTime,rgCurStod.Value) > 0)||(strcmp(pcpBgnTime,rgCurStoa.Value) < 0))
    {
      if (strstr(pclUsedStands,pclAnyData) == NULL)
      {
        strcat(pclUsedStands,pclAnyData);
        strcat(pclUsedStands,",");
      }
    }
    else
    {
      dbg(TRACE,"SHORT GAP STAND <%s> (BGN <%s> END <%s>) ",rgCurPsta.Value,rgCurStoa.Value,rgCurStod.Value);
    }
  }
*/

  dbg(TRACE,"-----------------------------------------------");
  dbg(TRACE,"BLOCKED OR OCCUPIED STANDS:\n<%s>",pclGridStands);
  ilCnt = CombineTwoLists(pcpUnusedStands, pclGridStands, pcpFetchedStands, 1);
  dbg(TRACE,"FREE OR AVAILABLE STANDS:\n<%s>",pcpUnusedStands);
  dbg(TRACE,"-----------------------------------------------");

  return ilCnt;
}

/*******************************************************/
/*******************************************************/
static int FindBestRemoteStand(char *pcpBgnTime, char *pcpEndTime, char *pcpRmtStand)
{
  int ilRc = RC_SUCCESS;
  long llMaxLine = 0;
  long llCurLine = 0;
  long llMapLine = -1;
  int ilCnt = 0;

  dbg(TRACE,"FIND ALLOCATION GAPS FOR BGN <%s> END <%s>",pcpBgnTime,pcpEndTime);
  dbg(TRACE,"-----------------------------------------------");
  llMaxLine = CT_GetLineCount(pcgCtTow) - 1;
  for (llCurLine=0;llCurLine<=llMaxLine;llCurLine++)
  {
    /*
    CT_GetColumnValueByName(pcgCtPst, llCurPstLine, "PNAM", &argCtValue[1]);
    str_trm_rgt(argCtValue[1].Value, " ", TRUE);
    CT_SetColumnValueByName(pcgCtPst, llCurPstLine, "PNAM", argCtValue[1].Value);
    CT_GetColumnValueByName(pcgCtPst, llCurPstLine, "TERM", &argCtValue[1]);
    str_trm_rgt(argCtValue[1].Value, " ", TRUE);
    CT_SetColumnValueByName(pcgCtPst, llCurPstLine, "TERM", argCtValue[1].Value);
    */
    CT_GetLineValues(pcgCtTow, llCurLine, &rgCtLine);
    dbg(TRACE,"<%s>", rgCtLine.Value);
  }
  dbg(TRACE,"-----------------------------------------------");

  return  ilCnt;
}

/*******************************************************/
/*******************************************************/
static void CreateRem2DataList(int ipIdx, int ipUpdateGrid, int ipForInsert, long lpLineNo, int ipForWhat)
{
  int ilPos = 0;
  char pclBgnTag[16] = "";
  char pclEndTag[16] = "";
  char pclValue[64] = "";
  char pclTmpDat[64] = "";
  char pclOldTowDat[264] = "";
  char pclOldValues[264] = "";
  char pclNewValues[264] = "";
  char *pclPtr = NULL;
  long llCurLine = 0;
  int ilGrp = 0;
  int ilFld = 0;

  if (ipForWhat <= 0)
  {
    strcpy(pclBgnTag,"");
    strcpy(pclEndTag,";");
    pclPtr = rgTowDat[ipIdx].StrRem2New;

    /* GROUP 1: Sytem Time Stamp */
    strcpy(pclTmpDat,""); /* System Time Stamp */
    GetAnyValue(1, "", -1, "", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, "|");

    /* GROUP 2: Basic Rotation Information */
    llCurLine = rgFltDat[1].FltLineNo;
    strcpy(pclTmpDat,""); /* ARR.FLT.URNO */;
    GetAnyValue(0, pcgCtAft, llCurLine, "URNO", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);
    strcpy(pclTmpDat,""); /* ARR.FLT.FTYP */
    GetAnyValue(0, pcgCtAft, llCurLine, "FTYP", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    llCurLine = rgFltDat[2].FltLineNo;
    strcpy(pclTmpDat,""); /* DEP.FLT.URNO */
    GetAnyValue(0, pcgCtAft, llCurLine, "URNO", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);
    strcpy(pclTmpDat,""); /* DEP.FLT.FTYP */
    GetAnyValue(0, pcgCtAft, llCurLine, "FTYP", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    strcpy(pclTmpDat,""); /* TOW-OUT.URNO */
    if (rgTowDat[1].TowIsValid == TRUE)
    {
      if (rgTowDat[1].TowIsHidden == FALSE)
      {
        llCurLine = rgTowDat[1].TowLineNo;
        GetAnyValue(0, pcgCtAft, llCurLine, "URNO", pclTmpDat);
      }
    }
    if (strlen(pclTmpDat) == 0)
    {
      if (lgFirstUserTowOutLine >= 0)
      {
        GetAnyValue(0, pcgCtAft, lgFirstUserTowOutLine, "URNO", pclTmpDat);
      }
    }
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    strcpy(pclTmpDat,""); /* TOW-IN.URNO */
    if (rgTowDat[2].TowIsValid == TRUE)
    {
      if (rgTowDat[2].TowIsHidden == FALSE)
      {
        llCurLine = rgTowDat[2].TowLineNo;
        GetAnyValue(0, pcgCtAft, llCurLine, "URNO", pclTmpDat);
      }
    }
    if (strlen(pclTmpDat) == 0)
    {
      if (lgLastUserTowInLine >= 0)
      {
        GetAnyValue(0, pcgCtAft, lgLastUserTowInLine, "URNO", pclTmpDat);
      }
    }
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, "|");

    /* GROUP 3: Towing Statistics */
    strcpy(pclTmpDat,""); /* TOW-OUT N/A/H */
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    strcpy(pclTmpDat,""); /* TOW-IN N/A/H */
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    strcpy(pclTmpDat,""); /* TOWINGS.COUNT */
    GetAnyValue(2, pcgCtAft, llCurLine, "FTYP", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    strcpy(pclTmpDat,""); /* TOWGAPS Y/N */
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, "|");

    /* GROUP 4: Towing Chain (Stands) */
    llCurLine = rgFltDat[1].FltLineNo;
    strcpy(pclTmpDat,""); /* ARR.FLT.PSTA */
    GetAnyValue(0, pcgCtAft, llCurLine, "PSTA", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    llCurLine = rgTowDat[1].TowLineNo;
    strcpy(pclTmpDat,""); /* TOW-OUT.PSTD */
    GetAnyValue(0, pcgCtAft, llCurLine, "PSTD", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    llCurLine = rgTowDat[1].TowLineNo;
    strcpy(pclTmpDat,""); /* TOW-OUT.PSTA */
    GetAnyValue(0, pcgCtAft, llCurLine, "PSTA", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    llCurLine = rgTowDat[2].TowLineNo;
    strcpy(pclTmpDat,""); /* TOW-IN.PSTD */
    GetAnyValue(0, pcgCtAft, llCurLine, "PSTD", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    llCurLine = rgTowDat[2].TowLineNo;
    strcpy(pclTmpDat,""); /* TOW-IN.PSTA */
    GetAnyValue(0, pcgCtAft, llCurLine, "PSTA", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    llCurLine = rgFltDat[2].FltLineNo;
    strcpy(pclTmpDat,""); /* DEP.FLT.PSTD */
    GetAnyValue(0, pcgCtAft, llCurLine, "PSTD", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, "|");

    /* GROUP 5: Towing Chain (Times) */
    llCurLine = rgFltDat[1].FltLineNo;
    strcpy(pclTmpDat,""); /* ARR.FLT.TIFA */
    GetAnyValue(0, pcgCtAft, llCurLine, "TIFA", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    llCurLine = rgTowDat[1].TowLineNo;
    strcpy(pclTmpDat,""); /* TOW-OUT.TIFD */
    GetAnyValue(0, pcgCtAft, llCurLine, "TIFD", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    llCurLine = rgTowDat[1].TowLineNo;
    strcpy(pclTmpDat,""); /* TOW-OUT.TIFA */
    GetAnyValue(0, pcgCtAft, llCurLine, "TIFA", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    llCurLine = rgTowDat[2].TowLineNo;
    strcpy(pclTmpDat,""); /* TOW-IN.TIFD */
    GetAnyValue(0, pcgCtAft, llCurLine, "TIFD", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    llCurLine = rgTowDat[2].TowLineNo;
    strcpy(pclTmpDat,""); /* TOW-IN.TIFA */
    GetAnyValue(0, pcgCtAft, llCurLine, "TIFA", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    llCurLine = rgFltDat[2].FltLineNo;
    strcpy(pclTmpDat,""); /* DEP.FLT.TIFD */
    GetAnyValue(0, pcgCtAft, llCurLine, "TIFD", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, "|");

    /* GROUP 6: ONBL/OFBL Information */
    llCurLine = rgFltDat[1].FltLineNo;
    strcpy(pclTmpDat,""); /* ARR.FLT.ONBL Y/N */
    GetAnyValue(3, pcgCtAft, llCurLine, "ONBL", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    llCurLine = rgTowDat[1].TowLineNo;
    strcpy(pclTmpDat,""); /* TOW-OUT.OFBL Y/N */
    GetAnyValue(3, pcgCtAft, llCurLine, "OFBL", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    llCurLine = rgTowDat[1].TowLineNo;
    strcpy(pclTmpDat,""); /* TOW-OUT.ONBL Y/N */
    GetAnyValue(3, pcgCtAft, llCurLine, "ONBL", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    llCurLine = rgTowDat[2].TowLineNo;
    strcpy(pclTmpDat,""); /* TOW-IN.OFBL Y/N */
    GetAnyValue(3, pcgCtAft, llCurLine, "OFBL", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    llCurLine = rgTowDat[2].TowLineNo;
    strcpy(pclTmpDat,""); /* TOW-IN.ONBL Y/N */
    GetAnyValue(3, pcgCtAft, llCurLine, "ONBL", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    llCurLine = rgFltDat[2].FltLineNo;
    strcpy(pclTmpDat,""); /* DEP.FLT.OFBL Y/N */
    GetAnyValue(3, pcgCtAft, llCurLine, "OFBL", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, "|");

    /* GROUP 7: Rules and Time Information */
    strcpy(pclTmpDat,""); /* TYPE (O=OVN, R=RULE) */
    strcpy(pclTmpDat,rgFltDat[1].StrRulType);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    strcpy(pclTmpDat,""); /* T-RULE TIME */
    sprintf(pclTmpDat,"%d",rgFltDat[1].MaxGrdTime);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    strcpy(pclTmpDat,""); /* A-RULE TIME */
    sprintf(pclTmpDat,"%d",rgTowDat[1].TowBgnOffs);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    strcpy(pclTmpDat,""); /* D-RULE TIME */
    sprintf(pclTmpDat,"%d",rgTowDat[2].TowEndOffs);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, "|");

    /* GROUP 8: Aircraft Registration and Type */
    llCurLine = rgFltDat[1].FltLineNo;
    strcpy(pclTmpDat,""); /* ARR.FLT.REGN */
    GetAnyValue(0, pcgCtAft, llCurLine, "REGN", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    llCurLine = rgFltDat[1].FltLineNo;
    strcpy(pclTmpDat,""); /* ARR.FLT.ACT3 */
    GetAnyValue(0, pcgCtAft, llCurLine, "ACT3", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    llCurLine = rgFltDat[1].FltLineNo;
    strcpy(pclTmpDat,""); /* ARR.FLT.ACT5 */
    GetAnyValue(0, pcgCtAft, llCurLine, "ACT5", pclTmpDat);
    sprintf(pclValue,"%s%s",pclBgnTag,pclTmpDat);
    StrgPutStrg(pclPtr, &ilPos, pclValue, 0, -1, pclEndTag);

    ilPos--;
    pclPtr[ilPos] = 0x00;

    strcpy(pcgTowNewRem2,pclPtr);

  }
  if ((ipIdx >= 1)&&(ipIdx <= 2))
  {
    /* AUTOMATED TOWING RECORDS (TOW-OUT AND TOW-IN) */
    if ((rgTowDat[ipIdx].TowExists == TRUE)||(ipForInsert == TRUE))
    {
      if ((ipUpdateGrid == TRUE)||(ipForInsert == TRUE))
      {
        strcpy(pclOldTowDat,rgTowDat[ipIdx].StrRem2Old);
        strncpy(pclOldTowDat,rgTowDat[ipIdx].StrRem2New,14);
        if (strcmp(pclOldTowDat,rgTowDat[ipIdx].StrRem2New) != 0)
        {
          dbg(TRACE,"UPDATING REM2 VALUE LIST OF TOW IDX=%d",ipIdx);
          llCurLine = rgTowDat[ipIdx].TowLineNo;
          UpdateGridData(pcgCtAft, llCurLine, "REM2", rgTowDat[ipIdx].StrRem2New);
        }
        else
        {
          dbg(TRACE,"NO UPDATE ON REM2 OF TOW IDX=%d REQUIRED",ipIdx);
        }
        if (strlen(rgTowDat[ipIdx].StrRem2Old) == 0)
        {
          dbg(TRACE,"REM2(IDX=%d) OLD VALUE IS EMPTY!!",ipIdx);
          strcpy(rgTowDat[ipIdx].StrRem2Old,rgTowDat[ipIdx].StrRem2New);
        }
      }
    }
  }
  else if ((ipIdx >= 3)&&(ipIdx <= 4))
  {
    /* USER DEFINED TOWING RECORDS (TOW-OUT AND TOW-IN) */
    /* (FUTURE USE) */
  }
  else if (ipIdx == 0)
  {
    if (ipForWhat == 1)
    {
      dbg(TRACE,"ANY RANDOM TOWING RECORD UPDATE ON REM2");
      llCurLine = lpLineNo;
      if (llCurLine >= 0)
      {
        GetAnyValue(0, pcgCtAft, llCurLine, "REM2", pclOldTowDat);
        strncpy(pclOldTowDat,rgTowDat[ipIdx].StrRem2New,14);
        if (strcmp(pclOldTowDat,rgTowDat[ipIdx].StrRem2New) != 0)
        {
          dbg(TRACE,"UPDATING REM2 VALUE LIST OF TOW IDX=%d",ipIdx);
          UpdateGridData(pcgCtAft, llCurLine, "REM2", rgTowDat[ipIdx].StrRem2New);
        }
        else
        {
          dbg(TRACE,"NO UPDATE ON REM2 OF TOW IDX=%d REQUIRED",ipIdx);
        }
        if (strlen(rgTowDat[ipIdx].StrRem2Old) == 0)
        {
          dbg(TRACE,"REM2(IDX=%d) OLD VALUE IS EMPTY!!",ipIdx);
          strcpy(rgTowDat[ipIdx].StrRem2Old,rgTowDat[ipIdx].StrRem2New);
        }
      }
    }
    else if (ipForWhat == 2)
    {
      dbg(TRACE,"GET LAST VALID DATA OF UTWO AND UTWI");
      ilGrp = 2;
      get_any_item(pclOldValues, pcgTowOldRem2, ilGrp, "|");
      get_any_item(pclNewValues, pcgTowNewRem2, ilGrp, "|");
      ilFld = 5;
      get_any_item(pcgTowOldUTWO, pclOldValues, ilFld, ";");
      get_any_item(pcgTowNewUTWO, pclNewValues, ilFld, ";");
      ilFld = 6;
      get_any_item(pcgTowOldUTWI, pclOldValues, ilFld, ";");
      get_any_item(pcgTowNewUTWI, pclNewValues, ilFld, ";");
    }
  }

  return;
}

/* *************************************************** */
/* *************************************************** */
static void GetAnyValue(int ipForWhat, char *pcpGridName, long lpGridLine, char *pcpFldName, char *pcpResult)
{
  int ilValue = 0;
  strcpy(rgGridVal.Value,"");
  switch (ipForWhat)
  {
    case 0:
      CT_GetColumnValueByName(pcpGridName, lpGridLine, pcpFldName, &rgGridVal);
      str_trm_rgt(rgGridVal.Value, " ", TRUE);
      strcpy(pcpResult,rgGridVal.Value);
      break;
    case 1:
      GetServerTimeStamp("UTC",1,0,pcpResult);
      break;
    case 2:
      ilValue = CountTowings(1,0);
      sprintf(pcpResult,"%d",ilValue);
      break;
    case 3:
      CT_GetColumnValueByName(pcpGridName, lpGridLine, pcpFldName, &rgGridVal);
      str_trm_rgt(rgGridVal.Value, " ", TRUE);
      if (strlen(rgGridVal.Value) > 0)
      {
        strcpy(pcpResult,"Y");
      }
      else
      {
        strcpy(pcpResult,"N");
      }
      break;
    default:
      strcpy(pcpResult,"");
      break;
  }
  return;
}


/* *************************************************** */
/* *************************************************** */
static int CheckRotationUpdates(int ipForWhat, long lpLineNo)
{
  int ilRetVal = 0;
  char *pclOldData = NULL;
  char *pclNewData = NULL;
  char pclGridRem2[1024];
  char pclOldValues[1024];
  char pclNewValues[1024];
  char pclOldFldDat[1024];
  char pclNewFldDat[1024];
  char pclAnyTime[16] = "";
  long llCurLine = 0;
  int ilDiff = 0;
  int ilGrp = 0;
  int ilFld = 0;
  if (ipForWhat <= 0)
  {
    dbg(TRACE,">> CHECK ROTATION UPDATES OF AUTO_TOWING");
    if (rgTowDat[1].TowExists == TRUE)
    {
      if (rgTowDat[1].TowOutIsOfbl == FALSE)
      {
        dbg(TRACE,"-- ARR TIME GLUE FUNCTION (TOW-OUT)");
        pclOldData = rgTowDat[1].StrRem2Old;
        pclNewData = rgTowDat[1].StrRem2New;
        ilGrp = 5;
        get_any_item(pclOldValues, pclOldData, ilGrp, "|");
        get_any_item(pclNewValues, pclNewData, ilGrp, "|");
        ilFld = 1;
        get_any_item(pclOldFldDat, pclOldValues, ilFld, ";");
        get_any_item(pclNewFldDat, pclNewValues, ilFld, ";");
        if (strcmp(pclOldFldDat, pclNewFldDat) != 0)
        {
          CheckTowingTimeUpdates(1, 1, pclOldFldDat, pclNewFldDat);
        }
      }
    }

    if (rgTowDat[2].TowExists == TRUE)
    {
      if (rgTowDat[2].TowInIsOfbl == FALSE)
      {
        dbg(TRACE,"-- DEP TIME GLUE FUNCTION (TOW-IN)");
        pclOldData = rgTowDat[2].StrRem2Old;
        pclNewData = rgTowDat[2].StrRem2New;
        ilGrp = 5;
        get_any_item(pclOldValues, pclOldData, ilGrp, "|");
        get_any_item(pclNewValues, pclNewData, ilGrp, "|");
        ilFld = 6;
        get_any_item(pclOldFldDat, pclOldValues, ilFld, ";");
        get_any_item(pclNewFldDat, pclNewValues, ilFld, ";");
        if (strcmp(pclOldFldDat, pclNewFldDat) != 0)
        {
          CheckTowingTimeUpdates(1, 2, pclOldFldDat, pclNewFldDat);
        }
      }
    }
    if (rgTowDat[1].TowExists == TRUE)
    {
      if (rgTowDat[1].TowOutIsOfbl == FALSE)
      {
        dbg(TRACE,"-- ARR RULE GLUE FUNCTION (TOW-OUT OFFSET)");
        pclOldData = rgTowDat[1].StrRem2Old;
        pclNewData = rgTowDat[1].StrRem2New;
        ilGrp = 7;
        get_any_item(pclOldValues, pclOldData, ilGrp, "|");
        get_any_item(pclNewValues, pclNewData, ilGrp, "|");
        ilFld = 3;
        get_any_item(pclOldFldDat, pclOldValues, ilFld, ";");
        get_any_item(pclNewFldDat, pclNewValues, ilFld, ";");
        if (strcmp(pclOldFldDat, pclNewFldDat) != 0)
        {
          CheckTowingTimeUpdates(2, 1, pclOldFldDat, pclNewFldDat);
        }
      }
    }
    if (rgTowDat[2].TowExists == TRUE)
    {
      if (rgTowDat[2].TowInIsOfbl == FALSE)
      {
        dbg(TRACE,"-- DEP RULE GLUE FUNCTION (TOW-IN OFFSET)");
        pclOldData = rgTowDat[2].StrRem2Old;
        pclNewData = rgTowDat[2].StrRem2New;
        ilGrp = 7;
        get_any_item(pclOldValues, pclOldData, ilGrp, "|");
        get_any_item(pclNewValues, pclNewData, ilGrp, "|");
        ilFld = 4;
        get_any_item(pclOldFldDat, pclOldValues, ilFld, ";");
        get_any_item(pclNewFldDat, pclNewValues, ilFld, ";");
        if (strcmp(pclOldFldDat, pclNewFldDat) != 0)
        {
          CheckTowingTimeUpdates(2, 2, pclOldFldDat, pclNewFldDat);
        }
      }
    }
  }
  else if (ipForWhat == 1)
  {
    dbg(TRACE,">> CHECK ROTATION UPDATES OF USER TOWINGS");
    if (igAutoGlueTowOut == TRUE)
    {
      if (rgFltDat[1].FltExists == TRUE)
      {
        llCurLine = lpLineNo;
        if (llCurLine >= 0)
        {
          GetAnyValue(0, pcgCtAft, llCurLine, "OFBL", pclAnyTime);
          if (strlen(pclAnyTime) < 14)
          {
            /* NOT YET OFF BLOCK */
            dbg(TRACE,"-- ARR TIME GLUE FUNCTION (TOW-OUT)");
            pclNewData = rgTowDat[0].StrRem2New;
            GetAnyValue(0, pcgCtAft, llCurLine, "REM2", pclGridRem2);
            if (strlen(pclGridRem2) == 0)
            {
              dbg(TRACE,"OLD REM2 OF USER TOWING IS EMPTY");
              strcpy(pclGridRem2,rgTowDat[0].StrRem2New);
            }
            pclOldData = pclGridRem2;
            dbg(TRACE,"REM2 OLD <%s>",pclOldData);
            dbg(TRACE,"REM2 NEW <%s>",pclNewData);
            ilGrp = 5;
            get_any_item(pclOldValues, pclOldData, ilGrp, "|");
            get_any_item(pclNewValues, pclNewData, ilGrp, "|");
            ilFld = 1;
            get_any_item(pclOldFldDat, pclOldValues, ilFld, ";");
            get_any_item(pclNewFldDat, pclNewValues, ilFld, ";");
            if (strcmp(pclOldFldDat, pclNewFldDat) != 0)
            {
              DateDiffToMin(pclNewFldDat,pclOldFldDat,&ilDiff);
              dbg(TRACE,"TIFA NEW <%s> OLD <%s> DIFF %d",pclNewFldDat,pclOldFldDat,ilDiff);
              CheckUserTowingUpdates(1, llCurLine, "", ilDiff);
            }
          }
        }
      }
    }
  }
  dbg(TRACE,"--------------------------------------------");
  return ilRetVal;
}


/* *************************************************** */
/* *************************************************** */
static void CheckTowingTimeUpdates(int ipForWhat, int ipIdx, char *pcpOldData, char *pcpNewData)
{
  char pclOldTime[16] = "";
  char pclNewTime[16] = "";
  int ilDiff = 0;
  int ilOldOffs = 0;
  int ilNewOffs = 0;
  long llCurLine = 0;
  switch (ipForWhat)
  {
    case 1:
      /* Flight Time Change (NEW TIFA OR TIFD) */
      switch (ipIdx)
      {
        case 1:
          /* (NEW TIFA FOR TOW-OUT) */
          if ((rgFltDat[1].FltExists == TRUE)&&(rgTowDat[1].TowExists == TRUE))
          {
            if (rgTowDat[1].TowOutIsOfbl == FALSE)
            {
              strcpy(pclOldTime,pcpOldData);
              strcpy(pclNewTime,pcpNewData);
              DateDiffToMin(pclNewTime,pclOldTime,&ilDiff);
              if (ilDiff != 0)
              {
                dbg(TRACE,"ARR.FLIGHT TIME CHANGE: TIFA OLD <%s> NEW <%s> DIFF=%d",pclOldTime,pclNewTime,ilDiff);
                rgTowDat[1].ValNewStod = rgTowDat[1].ValTowStod + ilDiff;
                (void) FullTimeString(rgTowDat[1].StrNewStod, rgTowDat[1].ValNewStod, "00");
                dbg(TRACE,"TOW-OUT OLD STOD <%s> NEW STOD <%s>",rgTowDat[1].StrTowStod,rgTowDat[1].StrNewStod);
                llCurLine = rgTowDat[1].TowLineNo;
                UpdateGridData(pcgCtAft, llCurLine, "STOD", rgTowDat[1].StrNewStod);
                UpdateGridData(pcgCtAft, llCurLine, "TIFD", rgTowDat[1].StrNewStod);
                strcpy(rgTowDat[1].StrTowStod,rgTowDat[1].StrNewStod);
                rgTowDat[1].ValTowStod = rgTowDat[1].ValNewStod;
                rgTowDat[1].ValNewStoa = rgTowDat[1].ValTowStoa + ilDiff;
                (void) FullTimeString(rgTowDat[1].StrNewStoa, rgTowDat[1].ValNewStoa, "00");
                dbg(TRACE,"TOW-OUT OLD STOA <%s> NEW STOA <%s>",rgTowDat[1].StrTowStoa,rgTowDat[1].StrNewStoa);
                llCurLine = rgTowDat[1].TowLineNo;
                UpdateGridData(pcgCtAft, llCurLine, "STOA", rgTowDat[1].StrNewStoa);
                UpdateGridData(pcgCtAft, llCurLine, "TIFA", rgTowDat[1].StrNewStoa);
                strcpy(rgTowDat[1].StrTowStoa,rgTowDat[1].StrNewStoa);
                rgTowDat[1].ValTowStoa = rgTowDat[1].ValNewStoa;
              }
            }
          }
          break;
        case 2:
          /* (NEW TIFD FOR TOW-IN) */
          if ((rgFltDat[2].FltExists == TRUE)&&(rgTowDat[2].TowExists == TRUE))
          {
            if (rgTowDat[2].TowInIsOfbl == FALSE)
            {
              strcpy(pclOldTime,pcpOldData);
              strcpy(pclNewTime,pcpNewData);
              DateDiffToMin(pclNewTime,pclOldTime,&ilDiff);
              if (ilDiff != 0)
              {
                dbg(TRACE,"DEP.FLIGHT TIME CHANGE: TIFD OLD <%s> NEW <%s> DIFF=%d",pclOldTime,pclNewTime,ilDiff);
                rgTowDat[2].ValNewStod = rgTowDat[2].ValTowStod + ilDiff;
                (void) FullTimeString(rgTowDat[2].StrNewStod, rgTowDat[2].ValNewStod, "00");
                dbg(TRACE,"TOW-IN OLD STOD <%s> NEW STOD <%s>",rgTowDat[2].StrTowStod,rgTowDat[2].StrNewStod);
                llCurLine = rgTowDat[2].TowLineNo;
                UpdateGridData(pcgCtAft, llCurLine, "STOD", rgTowDat[2].StrNewStod);
                UpdateGridData(pcgCtAft, llCurLine, "TIFD", rgTowDat[2].StrNewStod);
                strcpy(rgTowDat[2].StrTowStod,rgTowDat[2].StrNewStod);
                rgTowDat[2].ValTowStod = rgTowDat[2].ValNewStod;
                rgTowDat[2].ValNewStoa = rgTowDat[2].ValTowStoa + ilDiff;
                (void) FullTimeString(rgTowDat[2].StrNewStoa, rgTowDat[2].ValNewStoa, "00");
                dbg(TRACE,"TOW-IN OLD STOA <%s> NEW STOA <%s>",rgTowDat[2].StrTowStoa,rgTowDat[2].StrNewStoa);
                llCurLine = rgTowDat[2].TowLineNo;
                UpdateGridData(pcgCtAft, llCurLine, "STOA", rgTowDat[2].StrNewStoa);
                UpdateGridData(pcgCtAft, llCurLine, "TIFA", rgTowDat[2].StrNewStoa);
                strcpy(rgTowDat[2].StrTowStoa,rgTowDat[2].StrNewStoa);
                rgTowDat[2].ValTowStoa = rgTowDat[2].ValNewStoa;
              }
            }
          }
          break;
        default:
        break;
      } /* end switch ipIdx */
    case 2:
      /* RULE TIME OFFSET CHANGE */
      switch (ipIdx)
      {
        case 1:
          /* (NEW TOW-OUT OFFSET) */
          if ((rgFltDat[1].FltExists == TRUE)&&(rgTowDat[1].TowExists == TRUE))
          {
            if (rgTowDat[1].TowOutIsOfbl == FALSE)
            {
              ilOldOffs = atoi(pcpOldData);
              ilNewOffs = atoi(pcpNewData);
              if (ilNewOffs != ilOldOffs)
              {
                ilDiff = ilNewOffs - ilOldOffs;
                dbg(TRACE,"ARR RULE TIME OFFSET CHANGE: TIME OLD <%s> NEW <%s> DIFF=%d",pcpOldData,pcpNewData,ilDiff);
                rgTowDat[1].ValNewStod = rgTowDat[1].ValTowStod + ilDiff;
                (void) FullTimeString(rgTowDat[1].StrNewStod, rgTowDat[1].ValNewStod, "00");
                dbg(TRACE,"TOW-OUT OLD STOD <%s> NEW STOD <%s>",rgTowDat[1].StrTowStod,rgTowDat[1].StrNewStod);
                llCurLine = rgTowDat[1].TowLineNo;
                UpdateGridData(pcgCtAft, llCurLine, "STOD", rgTowDat[1].StrNewStod);
                UpdateGridData(pcgCtAft, llCurLine, "TIFD", rgTowDat[1].StrNewStod);
                strcpy(rgTowDat[1].StrTowStod,rgTowDat[1].StrNewStod);
                rgTowDat[1].ValTowStod = rgTowDat[1].ValNewStod;
                rgTowDat[1].ValNewStoa = rgTowDat[1].ValTowStoa + ilDiff;
                (void) FullTimeString(rgTowDat[1].StrNewStoa, rgTowDat[1].ValNewStoa, "00");
                dbg(TRACE,"TOW-OUT OLD STOA <%s> NEW STOA <%s>",rgTowDat[1].StrTowStoa,rgTowDat[1].StrNewStoa);
                llCurLine = rgTowDat[1].TowLineNo;
                UpdateGridData(pcgCtAft, llCurLine, "STOA", rgTowDat[1].StrNewStoa);
                UpdateGridData(pcgCtAft, llCurLine, "TIFA", rgTowDat[1].StrNewStoa);
                strcpy(rgTowDat[1].StrTowStoa,rgTowDat[1].StrNewStoa);
                rgTowDat[1].ValTowStoa = rgTowDat[1].ValNewStoa;
              }
            }
          }
          break;
        case 2:
          /* (NEW TOW-IN OFFSET) */
          if ((rgFltDat[2].FltExists == TRUE)&&(rgTowDat[2].TowExists == TRUE))
          {
            if (rgTowDat[2].TowInIsOfbl == FALSE)
            {
              ilOldOffs = atoi(pcpOldData);
              ilNewOffs = atoi(pcpNewData);
              if (ilNewOffs != ilOldOffs)
              {
                ilDiff = ilNewOffs - ilOldOffs;
                dbg(TRACE,"DEP RULE TIME OFFSET CHANGE: TIME OLD <%s> NEW <%s> DIFF=%d",pcpOldData,pcpNewData,ilDiff);
                ilDiff = -ilDiff;
                rgTowDat[2].ValNewStod = rgTowDat[2].ValTowStod + ilDiff;
                (void) FullTimeString(rgTowDat[2].StrNewStod, rgTowDat[2].ValNewStod, "00");
                dbg(TRACE,"TOW-IN OLD STOD <%s> NEW STOD <%s>",rgTowDat[2].StrTowStod,rgTowDat[2].StrNewStod);
                llCurLine = rgTowDat[2].TowLineNo;
                UpdateGridData(pcgCtAft, llCurLine, "STOD", rgTowDat[2].StrNewStod);
                UpdateGridData(pcgCtAft, llCurLine, "TIFD", rgTowDat[2].StrNewStod);
                strcpy(rgTowDat[2].StrTowStod,rgTowDat[2].StrNewStod);
                rgTowDat[2].ValTowStod = rgTowDat[2].ValNewStod;
                rgTowDat[2].ValNewStoa = rgTowDat[2].ValTowStoa + ilDiff;
                (void) FullTimeString(rgTowDat[2].StrNewStoa, rgTowDat[2].ValNewStoa, "00");
                dbg(TRACE,"TOW-IN OLD STOA <%s> NEW STOA <%s>",rgTowDat[2].StrTowStoa,rgTowDat[2].StrNewStoa);
                llCurLine = rgTowDat[2].TowLineNo;
                UpdateGridData(pcgCtAft, llCurLine, "STOA", rgTowDat[2].StrNewStoa);
                UpdateGridData(pcgCtAft, llCurLine, "TIFA", rgTowDat[2].StrNewStoa);
                strcpy(rgTowDat[2].StrTowStoa,rgTowDat[2].StrNewStoa);
                rgTowDat[2].ValTowStoa = rgTowDat[2].ValNewStoa;
              }
            }
          }
          break;
        default:
        break;
      } /* end switch ipIdx */
      break;
    default:
    break;
  } /* end switch ipForWhat */
  return;
}

/* *************************************************** */
/* *************************************************** */
static void CheckUserTowingUpdates(int ipForWhat, long lpLineNo, char *pcpData, int ipValue)
{
  if (lpLineNo >= 0)
  {
    switch (ipForWhat)
    {
      case 1:
        /* Flight Time Change (NEW TIFA OR TIFD) */
        /* Adapt STOD/STOA/TIFD/TIFA according to ilDiff */
        ShiftTowingTimeField(1, lpLineNo, "STOD", ipValue);
        ShiftTowingTimeField(1, lpLineNo, "TIFD", ipValue);
        ShiftTowingTimeField(1, lpLineNo, "STOA", ipValue);
        ShiftTowingTimeField(1, lpLineNo, "TIFA", ipValue);
        break;
      default:
      break;
    } /* end switch ipForWhat */
  }
  return;
}

/* *************************************************** */
/* *************************************************** */
static void ShiftTowingTimeField(int ipForWhat, long lpLineNo, char *pcpFldName, int ipMinDiff)
{
  char pclOldTime[16] = "";
  char pclNewTime[16] = "";
  int ilTimeVal = 0;
  int ilDummy = 0;
  if ((lpLineNo >= 0) && (ipMinDiff != 0))
  {
    GetAnyValue(0, pcgCtAft, lpLineNo, pcpFldName, pclOldTime);
    (void) GetFullDay(pclOldTime, &ilDummy, &ilTimeVal, &ilDummy);
    ilTimeVal += ipMinDiff;
    (void) FullTimeString(pclNewTime, ilTimeVal, "00");
    dbg(DEBUG,"TOWING OLD %s <%s> NEW <%s>",pcpFldName,pclOldTime,pclNewTime);
    UpdateGridData(pcgCtAft, lpLineNo, pcpFldName, pclNewTime);
  }
  return;
}


/*
Values kept in REM2 as semicolon separated list.
The groups are separated by a pipe symbol.
===============================================

1.) Last Time Accessed
----------------------
UTC TIME STAMP = 14       0 + 14 + 1 =  15

2.) Rotation Overview
---------------------
ARR.FLT: URNO  = 10      15 + 10 + 1 =  26
ARR.FLT: FTYP  =  1      26 +  1 + 1 =  28
DEP.FLT: URNO  = 10      28 + 10 + 1 =  39
DEP.FLT: FTYP  =  1      39 +  1 + 1 =  41

3.) Towing Statistic
--------------------
TOW-OUT: N/A/H =  1      41 +  1 + 1 =  43
TOW-IN : N/A/H =  1      43 +  1 + 1 =  45
TOWINGS: COUNT =  2      45 +  2 + 1 =  48
TOWGAPS: Y/N   =  1      48 +  1 + 1 =  50

4.) Towing Chain
----------------
ARR.FLT: PSTA  =  5      50 +  5 + 1 =  56
TOW-OUT: PSTD  =  5      56 +  5 + 1 =  62
TOW-OUT: PSTA  =  5      62 +  5 + 1 =  68
TOW-IN : PSTD  =  5      68 +  5 + 1 =  74
TOW_IN : PSTA  =  5      74 +  5 + 1 =  80
DEP.FLT: PSTD  =  5      80 +  5 + 1 =  86

5.) Time Chain
--------------
ARR.FLT: TIFA  = 14      86 + 14 + 1 = 101
TOW-OUT: TIFD  = 14     101 + 14 + 1 = 116
TOW-OUT: TIFA  = 14     116 + 14 + 1 = 131
TOW-IN : TIFD  = 14     131 + 14 + 1 = 146
TOW_IN : TIFA  = 14     146 + 14 + 1 = 161
DEP.FLT: TIFD  = 14     161 + 14 + 1 = 176

6.) Actual Events (Y/N)
-----------------------
ARR.FLT: ONBL  =  1     176 +  1 + 1 = 178
TOW-OUT: OFBL  =  1     178 +  1 + 1 = 180
TOW-OUT: ONBL  =  1     180 +  1 + 1 = 182
TOW-IN : OFBL  =  1     182 +  1 + 1 = 184
TOW-IN : ONBL  =  1     184 +  1 + 1 = 186
DEP.FLT: OFBL  =  1     186 +  1 + 1 = 188

7.) Rule Values
---------------
TYPE (OVN/RULE)=  1     188 +  1 + 1 = 190
T-RULE TIME    =  5     190 +  5 + 1 = 196
A-RULE TIME    =  5     196 +  5 + 1 = 202
D-RULE TIME    =  5     202 +  5 + 1 = 208

8.) Aircraft Data
-----------------
ARR.FLT: REGN  = 12     208 + 12 + 1 = 221
ARR.FLT: ACT3  =  3     221 +  3 + 1 = 225
ARR.FLT: ACT5  =  5     225 +  5 + 1 = 231
                                     -----
Total amount of used characters      = 231
                                     =====

The field REM2 is VARCHAR(256)

Free space = 256 - 231 = 25 characters.

These values will be saved in both towing records
which are maintained by Towhdl, Tow-out and Tow-in.

BST, 13.04.2010
*/

/*******************************************************/
/*******************************************************/
static int CheckGorTabInit(void)
{
  int ilRc = RC_SUCCESS;
  int ilCheckNext = TRUE;
  int ilGetRc = RC_SUCCESS;
  int ilGetRc1 = RC_SUCCESS;
  int ilGetRc2 = RC_SUCCESS;
  int ilCnt = 0;
  int ilItm = 0;
  int ilLen = 0;
  char pclCfgData[1024] = "";
  char pclCfgDat1[1024] = "";
  char pclCfgDat2[1024] = "";
  char pclCurItem[32] = "";
  char pclCurDat1[32] = "";
  char pclCurDat2[32] = "";
  char pclSqlKey[512] = "";
  char pclMsgFld[512] = "";
  char pclMsgDat[512] = "";

  ilGetRc = GetCfg ("GORTAB_PROCESSING", "USING_GORTAB_RELATION", CFG_STRING, pcgTmpBuf1, "YES");
  dbg(TRACE,"USING_GORTAB_RELATION = %s",pcgTmpBuf1);
  if (strcmp(pcgTmpBuf1,"YES") == 0)
  {
    igUsingGortabRelation = TRUE;
  }
  else
  {
    igUsingGortabRelation = FALSE;
  }
  if (igUsingGortabRelation == TRUE)
  {
    /* ============================== */
    (void)FLT_InitFltLibEnvironment();
    /* ============================== */
    ilGetRc = GetCfg ("GORTAB_PROCESSING", "INIT_GORTAB_FOGHDL_ID", CFG_STRING, pcgTmpBuf1, "-1");
    igGorTabInitId = atoi(pcgTmpBuf1);
    if (igGorTabInitId<1000)
    {
      igGorTabInitId = 0;
    }
    dbg(TRACE,"GORTAB INIT QUE ID = %d (MY ID = %d)",igGorTabInitId,igMyOwnModId);

    sprintf(pclCurItem,"%d",igGorTabInitId);
    ilGetRc = GetCfg ("GORTAB_PROCESSING", "GORTAB_TRIGGER_TO_QUE", CFG_STRING, pcgTmpBuf1, pclCurItem);
    igGorTabTriggerQue = atoi(pcgTmpBuf1);
    if (igGorTabTriggerQue<1000)
    {
      igGorTabTriggerQue = 0;
    }
    dbg(TRACE,"GORTAB TRIGGER QUE = %d (MY ID = %d)",igGorTabTriggerQue,igMyOwnModId);

    if (igGorTabInitId == igMyOwnModId)
    {
      dbg(TRACE,"NOW GOING TO INITIALIZE GORTAB RECORDS");
      dbg(TRACE,"======================================");
      ilCheckNext = TRUE;
      if (ilCheckNext == TRUE)
      {
        ilGetRc = GetCfg ("GORTAB_PROCESSING", "READ_AFTTAB_URNO_LIST", CFG_STRING, pclCfgData, "");
        if (ilGetRc == RC_SUCCESS)
        {
          dbg(TRACE,"GORTAB: INIT BY LIST OF AFTTAB URNOS");
          dbg(TRACE,"<%s>",pclCfgData);
          ilCnt = field_count(pclCfgData);
          for (ilItm = 1; ilItm <= ilCnt; ilItm++)
          {
            ilLen = get_real_item (pclCurItem, pclCfgData, ilItm);
            if (ilLen > 0)
            {
              sprintf(pclSqlKey,"URNO=%s",pclCurItem);
              dbg(TRACE,"SQL KEY <%s>",pclSqlKey);
              GetGorTabInitTrigger(pclSqlKey, "URNO", pclCurItem);
            }
          }
          ilCheckNext = FALSE;
        }
      }
      if (ilCheckNext == TRUE)
      {
        ilGetRc = GetCfg ("GORTAB_PROCESSING", "READ_AFTTAB_RKEY_LIST", CFG_STRING, pclCfgData, "");
        if (ilGetRc == RC_SUCCESS)
        {
          dbg(TRACE,"GORTAB: INIT BY LIST OF AFTTAB RKEYS");
          dbg(TRACE,"<%s>",pclCfgData);
          ilCnt = field_count(pclCfgData);
          for (ilItm = 1; ilItm <= ilCnt; ilItm++)
          {
            ilLen = get_real_item (pclCurItem, pclCfgData, ilItm);
            if (ilLen > 0)
            {
              sprintf(pclSqlKey,"RKEY=%s",pclCurItem);
              dbg(TRACE,"SQL KEY <%s>",pclSqlKey);
              GetGorTabInitTrigger(pclSqlKey, "RKEY", pclCurItem);
            }
          }
          ilCheckNext = FALSE;
        }
      }
      if (ilCheckNext == TRUE)
      {
        ilGetRc1 = GetCfg ("GORTAB_PROCESSING", "READ_AFTTAB_DATE_FROM", CFG_STRING, pclCfgDat1, "");
        ilGetRc2 = GetCfg ("GORTAB_PROCESSING", "READ_AFTTAB_DATE_TILL", CFG_STRING, pclCfgDat2, "");
        if ((ilGetRc1 == RC_SUCCESS) && (ilGetRc2 == RC_SUCCESS))
        {
          dbg(TRACE,"GORTAB: INIT BY TWO DATES (FROM-TILL)");
          ilGetRc = RC_SUCCESS;
        }
        else if (ilGetRc1 == RC_SUCCESS)
        {
          dbg(TRACE,"GORTAB: INIT BY TWO DATES (FROM-FROM)");
          strcpy(pclCfgDat2,pclCfgDat1);
          ilGetRc = RC_SUCCESS;
        }
        else if (ilGetRc2 == RC_SUCCESS)
        {
          dbg(TRACE,"GORTAB: INIT BY TWO DATES (TILL-TILL)");
          strcpy(pclCfgDat1,pclCfgDat2);
          ilGetRc = RC_SUCCESS;
        }
        else
        {
          ilGetRc = RC_FAIL;
        }
        if (ilGetRc == RC_SUCCESS)
        {
          dbg(TRACE,"GORTAB: READING AFTTAB BY LIST OF DATES");
          dbg(TRACE,"FROM <%s>",pclCfgDat1);
          dbg(TRACE,"TILL <%s>",pclCfgDat2);
          ilCnt = field_count(pclCfgDat1);
          for (ilItm = 1; ilItm <= ilCnt; ilItm++)
          {
            ilLen = get_real_item (pclCurDat1, pclCfgDat1, ilItm);
            ilLen = get_real_item (pclCurDat2, pclCfgDat2, ilItm);
            if (ilLen > 0)
            {
              sprintf(pclSqlKey,"(STOA BETWEEN '%s000000' AND '%s235959' AND DES3='%s')"
                                " OR "
                                "(STOD BETWEEN '%s000000' AND '%s235959' AND ORG3='%s')",
                                pclCurDat1,pclCurDat2,pcgHomeAp,pclCurDat1,pclCurDat2,pcgHomeAp);
              dbg(TRACE,"SQL KEY <%s>",pclSqlKey);
              strcpy(pclMsgFld,"FROM,TILL");
              sprintf(pclMsgDat,"%s,%s",pclCurDat1,pclCurDat2);
              GetGorTabInitTrigger(pclSqlKey, pclMsgFld, pclMsgDat);
            }
          }
          ilCheckNext = FALSE;
        }
      }
      dbg(TRACE,"======================================");
    }
    else
    {
      dbg(TRACE,"GORTAB 'INIT AND SYNC' IS NOT MY JOB");
      dbg(TRACE,"====================================");
    }
  }
  return ilRc;
}

/*******************************************************/
/*******************************************************/
static int GetGorTabInitTrigger(char *pcpSqlKey, char *pcpFldName, char *pcpFldData)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  long llCntRec = 0;
  char pclTblName[16] = "";
  char pclSqlCond[1024] = "";
  char pclSqlData[1024] = "";
  char pclSqlBuff[1024] = "";
  char pclTrigger[128] = "";
  char pclTWS[64] = "";
  char pclTWE[64] = "";
  short slCursor = 0;
  short slFkt = 0;
  strcpy(pclTWS,pcgTwStart);
  strcpy(pclTWE,pcgTwEnd);
  if (strlen(pclTWE) == 0)
  {
    sprintf(pclTWE,"%s,%s,GorIni",pcgHomeAp,pcgTabEnd);
  }
  strcpy(pclTblName,"AFTTAB");
  strcpy(pclSqlCond,pcpSqlKey);
  sprintf(pclSqlBuff,"SELECT DISTINCT(RKEY) FROM %s WHERE %s",pclTblName,pclSqlCond);
  dbg(TRACE,"<%s>",pclSqlBuff);
  slCursor = 0;
  slFkt = START;
  ilGetRc = DB_SUCCESS;
  while (ilGetRc == DB_SUCCESS)
  {
    ilGetRc = sql_if(slFkt,&slCursor,pclSqlBuff,pclSqlData);
	if (ilGetRc == DB_SUCCESS)
    {
      sprintf (pclTrigger, "WHERE RKEY=%s", pclSqlData);
      (void) tools_send_info_flag (igGorTabTriggerQue, 0, pcgDestName, "", pcgRecvName, "", "", pclTWS, pclTWE,
                                  "GOR", pclTblName, pclTrigger, pcpFldName, pcpFldData, 5);
      /* dbg(TRACE,"TRIGGER 'GOR' <%s> SENT TO MYSELF (%d)",pclTrigger,mod_id); */
      llCntRec++;
    }
    slFkt = NEXT;
  }
  close_my_cursor(&slCursor);
  dbg(TRACE,"TRIGGER 'GOR': SENT %d EVENTS TO %d",llCntRec,igGorTabTriggerQue);
  return ilRc;
}

/*******************************************************/
/*******************************************************/
static int HandleGorTabTrigger(char *pcpSelKey, char *pcpFields, char *pcpData)
{
  int ilRc = RC_SUCCESS;
  dbg(TRACE,"===== DATA ELEMENTS FROM TRIGGER =====");
  dbg(TRACE,"GOR FIELDS <%s>", pcpFields);
  dbg(TRACE,"GOR DATA   <%s>", pcpData);
  dbg(TRACE,"GOR SELECT <%s>", pcpSelKey);
  dbg(TRACE,"========================================");
  FLT_SyncGorRotation(pcpSelKey);
  return ilRc;
}



