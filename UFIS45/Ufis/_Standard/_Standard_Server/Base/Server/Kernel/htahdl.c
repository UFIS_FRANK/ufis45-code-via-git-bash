#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/htahdl.c 1.3 2011/12/16 21:58:45SGT ble Exp  $";
#endif /* _DEF_mks_version */

/*                                                                            */
/* UFIS AS HTAHDL.C                                                           */
/*                                                                            */
/* Author         : Andreas Kloidt                                            */
/* Date           : August 2008                                               */
/* Description    : Process to maintain automatically HTATAB                  */
/*                                                                            */
/* Update history :                                                           */
/*  20080908 1.1   AKL  Last checked in version in SVN.                       */
/*  20111129 1.2   DKA  Add BLD command to re-evaluate handlers for           */
/*                 selections from AFTTAB. This will replace                  */
/*                 rows in HTATAB, so selection must not include              */
/*                 flights which are already handled.                         */
/*  20111211 1.3   BLE                                                        */
/*  1. update agent fields in AFTTAB for rulfnd to create demands based on agent rules */
/*  2. remove self maintain HTATAB urno to use ready made lib NewUrno() to prevent int overflow */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_eqihdl[]="%Z% UFIS 4.4 (c) ABB AAT/I %M% %I% / %E% %U% / AKL";
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <netdb.h>
#include <time.h>
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h" 
#include "tools.h"
#include "helpful.h"
#include "timdef.h"
#include "urno_fn.inc"
 

#define XS_BUFF  128
#define S_BUFF   512
#define M_BUFF   1024
#define L_BUFF   2048
#define XL_BUFF  4096
#define XXL_BUFF 8192 

typedef struct 
{
    char pclAftField[5];
    char pclHtyp[23];
} FLTAGTNODE;


/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
FILE *outp       = NULL;
int  debug_level = TRACE;
/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/
extern int SetSignals(void (*HandleSignal)(int));
extern int DebugPrintItem(int,ITEM *);
extern int DebugPrintEvent(int,EVENT *);
extern int init_db(void);
extern int  ResetDBCounter(void);
extern void HandleRemoteDB(EVENT*);
extern int  sql_if(short ,short* ,char* ,char* ); 
extern int close_my_cursor(short *cursor); 
extern void snap(char*,int,FILE*);
extern int GetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel,char *pcpDef,char *pcpTrim);
extern int BuildItemBuffer(char *pcpData, char *pcpFieldList, int ipNoOfFields, char *pcpSepChr);
extern int get_item_no(char *s, char *f, short elem_len);
extern void  GetServerTimeStamp(char*,int,long,char*);
extern int AddSecondsToCEDATime(char *,long,int);
extern int get_real_item(char *, char *, int);
extern int GetNoOfElements(char *s, char c);
extern long nap(long);
extern int SendCedaEvent(int,int,char*,char*,char*,char*,char*,char*,char*,char*,char*,char*,int,int);
extern int GetFullDay(char *, int *, int *, int *);
static int UpdateAftAgent( char *pcpAftUrno, char *pcpHtyp, char *pcpHsna );

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;        /* The queue item pointer  */
static EVENT *prgEvent     = NULL;        /* The event pointer       */
static int   igItemLen     = 0;           /* length of incoming item */
static int   igInitOK      = FALSE;      /* Flag for init */

/*   static CFG   *prgCfg; */                     /* structure of the cfg-file */
static char  pcgHomeAp[XS_BUFF];      /* buffer for home airport */
static char  pcgTabEnd[XS_BUFF];      /* buffer for TABEND */
static char  pcgTwStart[XS_BUFF] = "";
static char  pcgTwEnd[XS_BUFF];
static char  pcgConfFile[S_BUFF];      /* buffer for config-file name */
static int   igUseHopo = FALSE;          /* flag for use of HOPO-field */
static int   igModID_Router  = 1200;      /* MOD-ID of Router  */

static char  pcgHostName[XS_BUFF];
static char  pcgHostIp[XS_BUFF];

static int   igQueCounter=0;
static int   que_out=0;

/*entry's from configfile*/
static char pcgConfigFile[512];
static char pcgCfgBuffer[512];
static EVENT *prgOutEvent = NULL;
static char pcgCurrentTime[32];
static int igSendResponse;
static int igDiffUtcToLocal;

/*** MEI 11-DEC-2011 ***/
static int igModID_Flight;
static int igNumAftAgentFields;
static FLTAGTNODE *rgAftAgt;
static char cgHopo[8];

static int    Init_htahdl();
static int    Reset(void);                        /* Reset program          */
static void   Terminate(int);                     /* Terminate program      */
static void   HandleSignal(int);                  /* Handles signals        */
static void   HandleErr(int);                     /* Handles general errors */
static void   HandleQueErr(int);                  /* Handles queuing errors */
static int    HandleInternalData(void);           /* Handles event data     */
static void   HandleQueues(void);                 /* Waiting for Sts.-switch*/
/******************************************************************************/
/* Function prototypes by AKL                                                 */
/******************************************************************************/
/* Init-functions  */

static int GetQueues();
static int TimeToStr(char *pcpTime,time_t lpTime);
static int GetConfig();
static void TrimRight(char *pcpBuffer);
static int HandleInsertOrUpdate(char *pcpFunc, char *pcpAftUrno, char *pcpFields, char *pcpData);
static int DeleteHandlingAgent(char *pcpAftUrno, char *pcpAftAdid, char *pcpHtaUrno);
static int InsertHandlingAgent(char *pcpAftUrno, char *pcpAltUrno, char *pcpAftAdid);
static int InsertIntoHTA(char *pcpAftAdid, char *pcpAftUrno, char *pcpHtyHtyp, char *pcpHaiHsna);
static int RebuildHandlingAgent(char *pcpSelection);
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
  int    ilRc = RC_SUCCESS;            /* Return code            */
  int    ilCnt = 0;
  int   ilItemFlag=TRUE; 
  time_t now = 0;
  INITIALIZE;            /* General initialization    */

  /* signal handling of SIGPIPE,SIGCHLD,SIGALRM,SIGTERM */
  SetSignals(HandleSignal);

  dbg(TRACE,"------------------------------------------");
  dbg(TRACE,"MAIN: version <%s>",sccs_eqihdl);

  /* Attach to the MIKE queues */
  do{
    ilRc = init_que();
    if(ilRc != RC_SUCCESS)
      {
    dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
    sleep(6);
    ilCnt++;
      }/* end of if */
  }while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
      sleep(60);
      exit(1);
    }else{
      dbg(TRACE,"MAIN: init_que() OK!");
      dbg(TRACE,"MAIN: mod_id   <%d>",mod_id);
      dbg(TRACE,"MAIN: mod_name <%s>",mod_name);
    }/* end of if */
  do
    {
      ilRc = init_db();
      if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
      sleep(6);
      ilCnt++;
    } /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
      sleep(60);
      exit(2);
    }else{
      dbg(TRACE,"MAIN: init_db()  OK!");
    } /* end of if */

  /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
  *pcgConfFile = 0x00;
  sprintf(pcgConfFile,"%s/%s",getenv("BIN_PATH"),mod_name);
  ilRc = TransferFile(pcgConfFile);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
    } /* end of if */
  dbg(TRACE,"MAIN: Binary-file = <%s>",pcgConfFile);
  ilRc = SendRemoteShutdown(mod_id);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    }
  if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: waiting for status switch ...");
      HandleQueues();
      dbg(TRACE,"MAIN: now running ...");
    }/* end of if */
  if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: initializing ...");
      dbg(TRACE,"------------------------------------------");
      if(igInitOK == FALSE)
    {
      ilRc = Init_htahdl();
      if(ilRc == RC_SUCCESS)
        {
          dbg(TRACE,"");
          dbg(TRACE,"------------------------------------------");
          dbg(TRACE,"MAIN: initializing OK");
          igInitOK = TRUE;
        } 
    }
    }else{
      Terminate(1);
    }
  dbg(TRACE,"------------------------------------------");
    
  if (igInitOK == TRUE)
    {
      now = time(NULL);
      while(TRUE)
    {
      memset(prgItem,0x00,igItemLen);
      ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
      dbg(DEBUG,"QUE Counter %d",++igQueCounter);
      /* depending on the size of the received item  */
      /* a realloc could be made by the que function */
      /* so do never forget to set event pointer !!! */
      prgEvent = (EVENT *) prgItem->text;
      if( ilRc == RC_SUCCESS )
        {
          /* Acknowledge the item */
          ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
          if( ilRc != RC_SUCCESS ) 
        {
          /* handle que_ack error */
          HandleQueErr(ilRc);
        } /* fi */
          switch( prgEvent->command )
        {
        case    HSB_STANDBY    :
          ctrl_sta = prgEvent->command;
          HandleQueues();
          break;    
        case    HSB_COMING_UP    :
          ctrl_sta = prgEvent->command;
          HandleQueues();
          break;    
        case    HSB_ACTIVE    :
          ctrl_sta = prgEvent->command;
          break;    
        case    HSB_ACT_TO_SBY    :
          ctrl_sta = prgEvent->command;
          /* CloseConnection(); */
          HandleQueues();
          break;    
        case    HSB_DOWN    :
          /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
          ctrl_sta = prgEvent->command;
          Terminate(1);
          break;    
        case    HSB_STANDALONE    :
          ctrl_sta = prgEvent->command;
          ResetDBCounter();
          break;    
        case    REMOTE_DB :
          /* ctrl_sta is checked inside */
          HandleRemoteDB(prgEvent);
          break;
        case    SHUTDOWN    :
          /* process shutdown - maybe from uutil */
          Terminate(1);
          break;
        case    RESET        :
          ilRc = Reset();
          break;
        case    EVENT_DATA    :
          if((ctrl_sta == HSB_STANDALONE) ||
             (ctrl_sta == HSB_ACTIVE) ||
             (ctrl_sta == HSB_ACT_TO_SBY))
            {
              ilItemFlag=TRUE;
              ilRc = HandleInternalData();
              if(ilRc != RC_SUCCESS)
            {
              HandleErr(ilRc);
            }/* end of if */
            }
          else
            {
              dbg(TRACE,"MAIN: wrong HSB-status <%d>",ctrl_sta);
              DebugPrintItem(TRACE,prgItem);
              DebugPrintEvent(TRACE,prgEvent);
            }/* end of if */
          break; 
        case    TRACE_ON :
          dbg_handle_debug(prgEvent->command);
          break;
        case    TRACE_OFF :
          dbg_handle_debug(prgEvent->command);
          break;
                case  111 :
                  break;
        default            :
          dbg(TRACE,"MAIN: unknown event");
          DebugPrintItem(TRACE,prgItem);
          DebugPrintEvent(TRACE,prgEvent);
          break;
        } /* end switch */
        }else{
          /* Handle queuing errors */
          HandleQueErr(ilRc);
        } /* end else */



      /**************************************************************/
      /* time parameter for cyclic actions                          */
      /**************************************************************/
    
      now = time(NULL);

    } /* end while */
    }else{
      dbg(TRACE,"MAIN: Init_htahdl() failed with <%d> Sleeping 30 sec.! Then terminating ...",ilRc);
      sleep(30);
    }
  exit(0);
  return 0;
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_htahdl()
{
  int    ilRc = RC_SUCCESS;            /* Return code */
  char pclFunc[] = "Init_htahdl:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[1024];
  char pclDataBuf[2048];
  char pclFields[512];
  char pclData[2048];

  GetQueues();
  /* reading default home-airport from sgs.tab */
  memset(pcgHomeAp,0x00,sizeof(pcgHomeAp));
  ilRc = tool_search_exco_data("SYS","HOMEAP",pcgHomeAp);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_htahdl : No HOMEAP entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_htahdl : HOMEAP = <%s>",pcgHomeAp);
      strcpy( cgHopo, pcgHomeAp );
    }
  /* reading default table-extension from sgs.tab */
  memset(pcgTabEnd,0x00,sizeof(pcgTabEnd));
  ilRc = tool_search_exco_data("ALL","TABEND",pcgTabEnd);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_htahdl : No TABEND entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_htahdl : TABEND = <%s>",pcgTabEnd);
      memset(pcgTwEnd,0x00,XS_BUFF);
      sprintf(pcgTwEnd,"%s,%s,%s",pcgHomeAp,pcgTabEnd,mod_name);
      dbg(TRACE,"Init_htahdl : TW_END = <%s>",pcgTwEnd);
      
      if (strcmp(pcgTabEnd,"TAB") == 0)
    {
      igUseHopo = TRUE;
      dbg(TRACE,"Init_htahdl: use HOPO-field!");
    }
    }

  ilRc = GetConfig();
  ilRc = TimeToStr(pcgCurrentTime,time(NULL));
  
  return(ilRc);
} /* end of initialize */
/*********************************************************************
Function : GetQueues()
Paramter :
Return Code: RC_SUCCESS,RC_FAIL
Result:
Description: Gets all necessary queue-ID's for CEDA-internal
             communication!
*********************************************************************/
static int GetQueues()
{
  int ilRc = RC_FAIL;

  /* get mod-id of router */
  if ((igModID_Router = tool_get_q_id("router")) == RC_NOT_FOUND ||
      igModID_Router == RC_FAIL || igModID_Router == 0)
    {
      dbg(TRACE,"GetQueues   : tool_get_q_id(router) returns: <%d>",igModID_Router);
      ilRc = RC_FAIL;
    }else{
      dbg(TRACE,"GetQueues   : <router> mod_id <%d>",igModID_Router);
      ilRc = RC_SUCCESS;
    } 
  igModID_Flight = tool_get_q_id("flight");
  dbg(TRACE,"GetQueues   : <flight> mod_id <%d>",igModID_Flight);
  return ilRc;
}

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{

  int    ilRc = RC_SUCCESS;    /* Return code */
    
  dbg(TRACE,"Reset: now reseting ...");

  return ilRc;
    
} /* end of Reset */
/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
  dbg(TRACE,"Terminate: now leaving ...");
 
  sleep(ipSleep);
  
  exit(0);
    
} /* end of Terminate */
/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
  switch(pipSig)
    {
    case SIGALRM:
      break;
    case SIGPIPE:
      break;
    case SIGCHLD:
      break;
    case SIGTERM:
      Terminate(1);
      break;
    default    :
      Terminate(10);
      break;
    } /* end of switch */
} /* end of HandleSignal */
/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
  /*    int    ilRc = RC_SUCCESS; */
    return;
} /* end of HandleErr */
/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRc = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
            dbg(TRACE,"<%d> : msgsnd failed",pipErr);
            break;
    case    QUE_E_GET    :    /* Error using msgrcv */
            if(pipErr != 4)        
             dbg(DEBUG,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    : 
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :   unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        /*dbg(TRACE,"<%d> : no messages on queue",pipErr);*/
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    case    QUE_E_PRIORITY    :
        dbg(TRACE,"<%d> : wrong priority was send ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */
/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
  int    ilRc = RC_SUCCESS;            /* Return code */
  int    ilBreakOut = FALSE;
    
  do{
    memset(prgItem,0x00,igItemLen);
    ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
    /* depending on the size of the received item  */
    /* a realloc could be made by the que function */
    /* so do never forget to set event pointer !!! */
    prgEvent = (EVENT *) prgItem->text;    
    if( ilRc == RC_SUCCESS )
      {
    /* Acknowledge the item */
    ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
    if( ilRc != RC_SUCCESS ) 
      {
        /* handle que_ack error */
        HandleQueErr(ilRc);
      } /* fi */
        
    switch( prgEvent->command )
      {
      case    HSB_STANDBY    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_COMING_UP    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_ACTIVE    :
        ctrl_sta = prgEvent->command;
        ilBreakOut = TRUE;
        break;    
      case    HSB_ACT_TO_SBY    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_DOWN    :
        /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
        ctrl_sta = prgEvent->command;
        Terminate(10);
        break;    
      case    HSB_STANDALONE    :
        ctrl_sta = prgEvent->command;
        ResetDBCounter();
        ilBreakOut = TRUE;
        break;    
      case    REMOTE_DB :
        /* ctrl_sta is checked inside */
        HandleRemoteDB(prgEvent);
        break;
      case    SHUTDOWN    :
        Terminate(1);
        break;
      case    RESET        :
        ilRc = Reset();
        break;
      case    EVENT_DATA    :
        dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
        DebugPrintItem(TRACE,prgItem);
        DebugPrintEvent(TRACE,prgEvent);
        break;
      case    TRACE_ON :
        dbg_handle_debug(prgEvent->command);
        break;
      case    TRACE_OFF :
        dbg_handle_debug(prgEvent->command);
        break;
      default            :
        dbg(TRACE,"HandleQueues: unknown event");
        DebugPrintItem(TRACE,prgItem);

        DebugPrintEvent(TRACE,prgEvent);

        break;
      } /* end switch */
      }else{
    /* Handle queuing errors */
    HandleQueErr(ilRc);
      } /* end else */
  } while (ilBreakOut == FALSE);
  if(igInitOK == FALSE)
    {
      ilRc = Init_htahdl();
      if(ilRc == RC_SUCCESS)
    {
      dbg(TRACE,"HandleQueues: Init_htahdl() OK!");
      igInitOK = TRUE;
    }else{ /* end of if */
      dbg(TRACE,"HandleQueues: Init_htahdl() failed!");
      igInitOK = FALSE;
    } /* end of if */
    }/* end of if */
  /* OpenConnection(); */
} /* end of HandleQueues */
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleInternalData()
{
  BC_HEAD *bchd = NULL;          /* Broadcast header*/
  CMDBLK  *cmdblk = NULL; 
  int  ilRC = RC_SUCCESS;      /* Return code */
  char pclUrno[16];
  char pclAftUrno[16];
  char pclHtyp[16];
  char pclHsna[40];
  char pclOldData[1024];
  char *pclSelection = NULL;
  char *pclFields = NULL;
  char *pclData = NULL;
  char *pclTmpPtr;

  que_out = prgEvent->originator;
  bchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  cmdblk= (CMDBLK  *) ((char *)bchd->data);
  if (bchd->rc == NETOUT_NO_ACK)
     igSendResponse = FALSE;
  else
     igSendResponse = TRUE;

  strcpy(pcgTwStart,cmdblk->tw_start);
  strcpy(pcgTwEnd,cmdblk->tw_end);

  /***********************************/
  /*    DebugPrintItem(DEBUG,prgItem);   */
  /*    DebugPrintEvent(DEBUG,prgEvent); */
  /***********************************/
  pclSelection = cmdblk->data;
  pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
  pclData = (char *)pclFields + strlen(pclFields) + 1;
  *pclUrno = '\0';
  pclTmpPtr = strstr(pclSelection,"\n");
  if (pclTmpPtr != NULL)
  {
     *pclTmpPtr = '\0';
     pclTmpPtr++;
     strcpy(pclUrno,pclTmpPtr);
  }
  pclTmpPtr = strstr(pclData,"\n");
  if (pclTmpPtr != NULL)
  {
     *pclTmpPtr = '\0';
     pclTmpPtr++;
     strcpy(pclOldData,pclTmpPtr);
  }

  ilRC = TimeToStr(pcgCurrentTime,time(NULL));
  dbg( TRACE, "========================= START =========================" );
  dbg( TRACE,"CMD <%s>  TBL <%s>", cmdblk->command, cmdblk->obj_name );
  dbg( TRACE,"FIELDS <%s>", pclFields );
  dbg( TRACE,"DATA <%s>", pclData );
  dbg( TRACE,"SEL/URNO: <%s> <%s>", pclSelection, pclUrno );
  dbg( TRACE,"TWS <%s> TWE <%s>", pcgTwStart, pcgTwEnd );

  if (strcmp(cmdblk->command,"INS") == 0 || strcmp(cmdblk->command,"UPD") == 0)
  {
     ilRC = HandleInsertOrUpdate(cmdblk->command,pclUrno,pclFields,pclData);
  }
  else if (strcmp(cmdblk->command,"DEL") == 0)
  {
     ilRC = DeleteHandlingAgent(pclUrno,"","");
  }
  else if (strcmp(cmdblk->command,"BLD") == 0)
  { 
     if (strlen (pclSelection) == 0)
     {
       dbg(TRACE,"BLD command must have Selection string with WHERE keyword and fields specified");
       dbg(TRACE,"E.g. WHERE ALC2='ZZ' AND FLDA = '20100814'");
       dbg(TRACE,"Doing nothing");
     }
     else
       ilRC = RebuildHandlingAgent(pclSelection);
  }
  else if (strcmp(cmdblk->obj_name,"HTATAB") == 0)
  {
      if ( strcmp(cmdblk->command,"URT") == 0 || strcmp(cmdblk->command,"DRT") == 0 )
      {
          /* Blanco the old one */
          get_real_item ( pclAftUrno, pclOldData, 1 );
          get_real_item ( pclHtyp, pclOldData, 2 );
          get_real_item ( pclHsna, pclOldData, 3 );
          dbg( TRACE, "CMD <%s> *OLD DATA* AftUrno <%s> Htyp <%s> Hsna <%s>", cmdblk->command, pclAftUrno, pclHtyp, pclHsna );
          UpdateAftAgent( pclAftUrno, pclHtyp, " " );
      }

      if ( strcmp(cmdblk->command,"IRT") == 0 || strcmp(cmdblk->command,"URT") == 0 )
      {
          /* Update the new one */
          get_real_item ( pclAftUrno, pclData, 1 );
          get_real_item ( pclHtyp, pclData, 2 );
          get_real_item ( pclHsna, pclData, 3 );
          dbg( TRACE, "CMD <%s> AftUrno <%s> Htyp <%s> Hsna <%s>", cmdblk->command, pclAftUrno, pclHtyp, pclHsna );
          UpdateAftAgent( pclAftUrno, pclHtyp, pclHsna );
      }
  }


  dbg( TRACE, "========================= END =========================" );
  return ilRC;
} /* end of HandleInternalData */
/*********************************************************************

/*
    Get Config Entries
*/
static int GetConfig()
{
  int ilRC = RC_SUCCESS;
  int ili, ilCount;
  char pclOneConfig[1024] = "\0";
  char pclTmpStr[1024] = "\0";
  char pclFunc[] = "GetConfig:";

  sprintf(pcgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
  dbg(TRACE,"%s Config File is <%s>",pclFunc,pcgConfigFile);

  ilRC = iGetConfigEntry(pcgConfigFile,"MAIN","DEBUG_LEVEL",CFG_STRING,pclOneConfig);
  debug_level = TRACE;
  if (strcmp(pclOneConfig,"DEBUG") == 0)
     debug_level = DEBUG;
  else if (strcmp(pclOneConfig,"OFF") == 0)
     debug_level = 0;
  dbg(TRACE,"%s DEBUG_LEVEL = %s",pclFunc,pclOneConfig);

  /** MEI 11-DEC-2011 **/
  /* Read agent config */
  ilRC = iGetConfigEntry( pcgConfigFile, "FLIGHT_AGENTS", "AFT_AGENT_FIELDS", CFG_STRING, pclOneConfig );
  if( ilRC != RC_SUCCESS )
  {
      dbg( TRACE, "%s NO AGENT CONFIG FOUND!", pclFunc );
      return RC_SUCCESS;
  }
  ilCount = get_no_of_items(pclOneConfig);
  if( ilCount <= 0 )
  {
      dbg( TRACE, "%s 0 AGENT CONFIG FOUND!", pclFunc );
      return RC_SUCCESS;
  }
  igNumAftAgentFields = ilCount;
  rgAftAgt = (FLTAGTNODE *)malloc( igNumAftAgentFields * sizeof(FLTAGTNODE) );
  memset( rgAftAgt, 0, sizeof(FLTAGTNODE)*igNumAftAgentFields );
  dbg( TRACE, "%s <%d> AGENT CONFIG FOUND - <%s>", pclFunc, igNumAftAgentFields, pclOneConfig );

  for( ili = 1; ili <= ilCount; ili++ )
  {
      get_real_item (rgAftAgt[ili-1].pclAftField, pclOneConfig, ili);
      sprintf( pclTmpStr, "%s_HTYP", rgAftAgt[ili-1].pclAftField );
      ilRC = iGetConfigEntry( pcgConfigFile, "FLIGHT_AGENTS", pclTmpStr, CFG_STRING, rgAftAgt[ili-1].pclHtyp );
      sprintf( pclTmpStr, ",%s,", rgAftAgt[ili-1].pclHtyp );
      strcpy( rgAftAgt[ili-1].pclHtyp, pclTmpStr );
      dbg( TRACE, "%s REC %d: FIELD <%s> HTYP <%s>", pclFunc, ili, rgAftAgt[ili-1].pclAftField, rgAftAgt[ili-1].pclHtyp );
  }
  /***************************/

  return RC_SUCCESS;
} /* Enf of GetConfig */

/******************************************************************************/
/* The TimeToStr routine                                                      */
/******************************************************************************/
static int TimeToStr(char *pcpTime,time_t lpTime) 
{                                                                              
  struct tm *_tm;
    
  _tm = (struct tm *)gmtime(&lpTime);

  sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
          _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
          _tm->tm_min,_tm->tm_sec);

  return RC_SUCCESS;  
                        
}     /* end of TimeToStr */


static void TrimRight(char *pcpBuffer)
{
  char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];

  if (strlen(pcpBuffer) == 0)
  {
     strcpy(pcpBuffer, " ");
  }
  else
  {
     while (isspace(*pclBlank) && pclBlank != pcpBuffer)
     {
        *pclBlank = '\0';
        pclBlank--;
     }
  }
} /* End of TrimRight */


static int HandleInsertOrUpdate (char *pcpFunc, char *pcpAftUrno, char *pcpFields, char *pcpData)
{
  int ilRC = RC_SUCCESS;
  int ilItemNo;
  int ilCount;
  char pclAftAlc2[8] = "";
  char pclAftAlc3[8] = "";
  char pclAftFtyp[8] = "";
  char pclAftAdid[8] = "";
  char pclAltUrno[16];
  char pclFunc[] = "HandleInsertOrUpdate:";

  ilItemNo = get_item_no(pcpFields,"ALC2",5) + 1;
  if (ilItemNo > 0)
     get_real_item(pclAftAlc2,pcpData,ilItemNo);
  ilItemNo = get_item_no(pcpFields,"ALC3",5) + 1;
  if (ilItemNo > 0)
     get_real_item(pclAftAlc3,pcpData,ilItemNo);
  TrimRight(pclAftAlc2);
  TrimRight(pclAftAlc3);
  if (*pclAftAlc2 == ' ' && *pclAftAlc3 == ' ')
  {
     dbg(DEBUG,"%s No Airline Code Specified ==> Nothing to do!",pclFunc);
     return ilRC;
  }

  ilCount = 1;
  if (*pclAftAlc3 != ' ')
     ilRC = syslibSearchDbData("ALTTAB","ALC3",pclAftAlc3,"URNO",pclAltUrno,&ilCount,"\n");
  else
     ilRC = syslibSearchDbData("ALTTAB","ALC2",pclAftAlc2,"URNO",pclAltUrno,&ilCount,"\n");
  if (ilRC != RC_SUCCESS)
  {
     dbg(DEBUG,"%s Airline Code <%s>/<%s> not specified ==> Nothing to do!",pclFunc,pclAftAlc2,pclAftAlc3);
     return ilRC;
  }
  TrimRight(pclAltUrno);

  ilItemNo = get_item_no(pcpFields,"FTYP",5) + 1;
  if (ilItemNo > 0)
     get_real_item(pclAftFtyp,pcpData,ilItemNo);
  ilItemNo = get_item_no(pcpFields,"ADID",5) + 1;
  if (ilItemNo > 0)
     get_real_item(pclAftAdid,pcpData,ilItemNo);
  TrimRight(pclAftFtyp);
  TrimRight(pclAftAdid);
  if (*pclAftFtyp == 'T' || *pclAftFtyp == 'G')
  {
     dbg(DEBUG,"%s Towing Record inserted ==> Nothing to do!",pclFunc);
     return ilRC;
  }

  if (*pclAftAdid == 'B')
  {
     ilRC = InsertHandlingAgent(pcpAftUrno,pclAltUrno,"D");
     ilRC = InsertHandlingAgent(pcpAftUrno,pclAltUrno,"A");
  }
  else if (*pclAftAdid == 'A')
  {
     ilRC = DeleteHandlingAgent(pcpAftUrno,"D","");
     ilRC = InsertHandlingAgent(pcpAftUrno,pclAltUrno,pclAftAdid);
  }
  else
  {
     ilRC = DeleteHandlingAgent(pcpAftUrno,"A","");
     ilRC = InsertHandlingAgent(pcpAftUrno,pclAltUrno,pclAftAdid);
  }

  return ilRC;
} /* End of HandleInsertOrUpdate */


static int DeleteHandlingAgent(char *pcpAftUrno, char *pcpAftAdid, char *pcpHtaUrno)
{
  int ilRC = RC_SUCCESS;
  int ilRCdbDel = DB_SUCCESS;
  int ilRCdbSel = DB_SUCCESS;
  short slSqlCursor;    
  short slSqlFunc;  
  char pclFLNU[12] = "\0";
  char pclHTYP[12] = "\0";
  char pclHSNA[12] = "\0";
  char pclSelectionDel[1024];
  char pclSqlBufDel[1024];
  char pclDataBufDel[2048];
  char pclFunc[] = "DeleteHandlingAgent:";

  if (*pcpHtaUrno == '\0')
  {
     if (*pcpAftAdid == '\0')
        sprintf(pclSelectionDel,"WHERE FLNU = %s",pcpAftUrno);
     else
        sprintf(pclSelectionDel,"WHERE FLNU = %s AND ADID = '%s'",pcpAftUrno,pcpAftAdid);
  }
  else
     sprintf(pclSelectionDel,"WHERE URNO = %s",pcpHtaUrno);

  /** MEI 11-DEC-2011 **/
  /* Update AFTTAB before delete **/
  sprintf( pclSqlBufDel, "SELECT FLNU,TRIM(HTYP),TRIM(HSNA) FROM HTATAB %s", pclSelectionDel );
  slSqlCursor = 0;
  slSqlFunc = START;
  ilRCdbSel = DB_SUCCESS;
  while( ilRCdbSel == DB_SUCCESS )
  {
      ilRCdbSel = sql_if(slSqlFunc,&slSqlCursor,pclSqlBufDel,pclDataBufDel);
      get_fld( pclDataBufDel,FIELD_1,STR,12,pclFLNU);
      get_fld( pclDataBufDel,FIELD_2,STR,12,pclHTYP);
      get_fld( pclDataBufDel,FIELD_3,STR,12,pclHSNA);
      dbg( DEBUG, "%s FLNU <%d> HTYP <%s> HSNA <%s>", pclFunc, pclFLNU, pclHTYP, pclHSNA );
      UpdateAftAgent( pclFLNU, pclHTYP, " " );
      slSqlFunc = NEXT;
  }
  close_my_cursor(&slSqlCursor);
  /*********/

  sprintf(pclSqlBufDel,"DELETE FROM HTATAB %s",pclSelectionDel);
  
  ilRCdbDel = RunSQL(pclSqlBufDel,pclDataBufDel);
  dbg(DEBUG,"%s ilRc <%d> SQL = <%s>",pclFunc,ilRCdbDel,pclSqlBufDel);

  return ilRC;
} /* End of DeleteHandlingAgent */


static int InsertHandlingAgent(char *pcpAftUrno, char *pcpAltUrno, char *pcpAftAdid)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "InsertHandlingAgent:";
  int ilRCdbHai = DB_SUCCESS;
  short slFktHai;
  short slCursorHai;
  char pclSelectionHai[1024];
  char pclSqlBufHai[1024];
  char pclDataBufHai[2048];
  int ilRCdbHty = DB_SUCCESS;
  short slFktHty;
  short slCursorHty;
  char pclSelectionHty[1024];
  char pclSqlBufHty[1024];
  char pclDataBufHty[2048];
  char pclHaiHsna[512];
  char pclHaiTask[512];
  char pclHtyHtyp[512];
  char pclHtyAdid[512];

  sprintf(pclSelectionHai,"WHERE ALTU = %s",pcpAltUrno);
  sprintf(pclSqlBufHai,"SELECT HSNA,TASK FROM HAITAB %s",pclSelectionHai);
  slCursorHai = 0;
  slFktHai = START;
  dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBufHai);
  ilRCdbHai = sql_if(slFktHai,&slCursorHai,pclSqlBufHai,pclDataBufHai);
  while (ilRCdbHai == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBufHai,"",2,",");
     get_real_item(pclHaiHsna,pclDataBufHai,1);
     TrimRight(pclHaiHsna);
     get_real_item(pclHaiTask,pclDataBufHai,2);
     TrimRight(pclHaiTask);
     dbg(DEBUG,"%s HSNA/TASK = <%s> <%s>",pclFunc,pclHaiHsna,pclHaiTask);
     sprintf(pclSelectionHty,"WHERE HNAM = '%s'",pclHaiTask);
     sprintf(pclSqlBufHty,"SELECT HTYP,ADID FROM HTYTAB %s",pclSelectionHty);
     slCursorHty = 0;
     slFktHty = START;
     dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBufHty);
     ilRCdbHty = sql_if(slFktHty,&slCursorHty,pclSqlBufHty,pclDataBufHty);
     close_my_cursor(&slCursorHty);
     if (ilRCdbHty == DB_SUCCESS)
     {
        BuildItemBuffer(pclDataBufHty,"",2,",");
        get_real_item(pclHtyHtyp,pclDataBufHty,1);
        TrimRight(pclHtyHtyp);
        get_real_item(pclHtyAdid,pclDataBufHty,2);
        TrimRight(pclHtyAdid);
        dbg(DEBUG,"%s HTYP/ADID = <%s> <%s>",pclFunc,pclHtyHtyp,pclHtyAdid);
        if (*pclHtyAdid == ' ' || *pclHtyAdid == 'B' || *pclHtyAdid == *pcpAftAdid)
        {
           if (*pcpAftAdid == 'B')
           {
              ilRC = InsertIntoHTA("A",pcpAftUrno,pclHtyHtyp,pclHaiHsna);
              ilRC = InsertIntoHTA("D",pcpAftUrno,pclHtyHtyp,pclHaiHsna);
           }
           else
              ilRC = InsertIntoHTA(pcpAftAdid,pcpAftUrno,pclHtyHtyp,pclHaiHsna);
        }
     }
     dbg(DEBUG,"---------------------------------------------------------------");
     slFktHai = NEXT;
     ilRCdbHai = sql_if(slFktHai,&slCursorHai,pclSqlBufHai,pclDataBufHai);
  }
  close_my_cursor(&slCursorHai);

  return ilRC;
} /* End of InsertHandlingAgent */


static int InsertIntoHTA(char *pcpAftAdid, char *pcpAftUrno, char *pcpHtyHtyp, char *pcpHaiHsna)
{
  int ilRC = RC_SUCCESS;
  int ilRCdbHta = DB_SUCCESS;
  int ilRCdbIns = DB_SUCCESS;
  int ilNextUrno;
  char pclSelectionHta[1024];
  char pclSqlBufHta[1024];
  char pclDataBufHta[2048];
  char pclSqlBufIns[1024];
  char pclDataBufIns[2048];
  char pclHtaUrno[512];
  char pclHtaHsna[512];
  char pclHtaFlag[512];
  char pclFields[512];
  char pclData[2048];
  char pclFunc[] = "InsertIntoHTA:";

  strcpy(pclFields,"URNO,HOPO,USEC,CDAT,FLNU,HSNA,HTYP,FLAG,ADID");
  sprintf(pclSelectionHta,"WHERE FLNU = %s AND HTYP = '%s' AND ADID = '%s'",
          pcpAftUrno,pcpHtyHtyp,pcpAftAdid);

  /* Look for existing record in HTATAB */
  sprintf(pclSqlBufHta,"SELECT URNO,HSNA,FLAG FROM HTATAB %s",pclSelectionHta);
  ilRCdbHta = RunSQL( pclSqlBufHta, pclDataBufHta );
  dbg(DEBUG,"%s SELSQL = <%s>",pclFunc,pclSqlBufHta);
  if (ilRCdbHta != DB_SUCCESS)
  {
     /*ilRC = GetNextNumbers("HTATAB",pclNextUrno,1,"GNV");*/
     ilNextUrno = NewUrnos( "HTATAB", 1 );
     sprintf(pclData,"'%d','%s','%s',%s,'%s','%s','%s','G','%s'",
             ilNextUrno,pcgHomeAp,mod_name,pcgCurrentTime,pcpAftUrno,pcpHaiHsna,pcpHtyHtyp,pcpAftAdid);
     sprintf(pclSqlBufIns,"INSERT INTO HTATAB FIELDS(%s) VALUES(%s)",pclFields,pclData);
     ilRCdbIns = RunSQL( pclSqlBufIns, pclDataBufIns );
     dbg(DEBUG,"%s ilRc = <%d> 1.INSSQL <%s>",pclFunc,ilRCdbIns,pclSqlBufIns);
  }
  else
  {
     BuildItemBuffer(pclDataBufHta,"",3,",");
     get_real_item(pclHtaUrno,pclDataBufHta,1);  TrimRight(pclHtaUrno);
     get_real_item(pclHtaHsna,pclDataBufHta,2);  TrimRight(pclHtaHsna);
     get_real_item(pclHtaFlag,pclDataBufHta,3);  TrimRight(pclHtaFlag);
     if (*pclHtaFlag == 'G' && strcmp(pclHtaHsna,pcpHaiHsna) != 0)
     {
        ilRC = DeleteHandlingAgent("","",pclHtaUrno);
        /*ilRC = GetNextNumbers("HTATAB",pclNextUrno,1,"GNV");*/
        ilNextUrno = NewUrnos( "HTATAB", 1 );
        sprintf(pclData,"'%d','%s','%s',%s,'%s','%s','%s','G','%s'",
                ilNextUrno,pcgHomeAp,mod_name,pcgCurrentTime,pcpAftUrno,pcpHaiHsna,pcpHtyHtyp,pcpAftAdid);
        sprintf(pclSqlBufIns,"INSERT INTO HTATAB FIELDS(%s) VALUES(%s)",pclFields,pclData);
        ilRCdbIns = RunSQL( pclSqlBufIns, pclDataBufIns );
        dbg(DEBUG,"%s ilRc = <%d> 2.INSSQL <%s>",pclFunc,ilRCdbIns,pclSqlBufIns);
     }
  }
  UpdateAftAgent( pcpAftUrno, pcpHtyHtyp, pcpHaiHsna );

  return ilRC;
} /* End of InsertIntoHTA */



int RebuildHandlingAgent(char *pcpSelection)
{
  int ilRC = RC_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf [2048] = "\0";
  char clDataArea [4096] = "\0";
  char clFieldList [2048] = "\0";
  char clWhereClause [2048] = "\0";
  char clWhereClauseCedaCmd [512] = "\0";
  int ilNoOfRec = 0;
  int ilGetRc = DB_SUCCESS;
  char pclFunc[] = "RebuildHandlingAgent:";
  char pclUrno [64];
  char pclAdid [64];
  char pclAlc2 [64];
  char pclAlc3 [64];
  char pclFtyp [64];
  char pclFlno [64];
  char pclFlda [64];

  strcpy (clWhereClause,pcpSelection);
  sprintf (clSqlBuf,
    "SELECT URNO,ADID,ALC2,ALC3,FTYP,FLNO,FLDA FROM AFTTAB %s ORDER BY FLDA", 
    clWhereClause);
  dbg (TRACE, "%s : <%s>", pclFunc,clSqlBuf);

  ilNoOfRec = 0;
  slFkt = START;
  slCursor = 0;
  ilGetRc = DB_SUCCESS;
  while (ilGetRc == DB_SUCCESS)
  { 
    ilGetRc = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea);
    slFkt = NEXT;
    if (ilGetRc == DB_SUCCESS)
    {
      strcpy (clFieldList,
       "URNO,ADID,ALC2,ALC3,FTYP,FLNO,FLDA");
      ilNoOfRec++;
      BuildItemBuffer (clDataArea, clFieldList, 7, ",");

      (void) get_real_item (pclUrno, clDataArea, 1);
      (void) get_real_item (pclAdid, clDataArea, 2);
      (void) get_real_item (pclAlc2, clDataArea, 3);
      (void) get_real_item (pclAlc3, clDataArea, 4);
      (void) get_real_item (pclFtyp, clDataArea, 5);
      (void) get_real_item (pclFlno, clDataArea, 6);
      (void) get_real_item (pclFlda, clDataArea, 7);


      dbg(TRACE,"%s = AFT: URNO <%s> ADID <%s> ALC2 <%s> ALC3 <%s> FTYP <%s> <%s/%s> =",
        pclFunc, pclUrno, pclAdid, pclAlc2, pclAlc3, pclFtyp, pclFlno, pclFlda);

      ilRC = HandleInsertOrUpdate("UPD",pclUrno,clFieldList,clDataArea); 
    } 
    else
    {
      if (ilGetRc == RC_FAIL)
      {
        dbg (TRACE, "%s ORACLE ERROR <%d> !!", pclFunc,ilGetRc);
        ilRC = RC_FAIL;
      }                     /* end if */
      else
      {
        dbg (TRACE, "%s : no record(s) found, ilRC <%d>",pclFunc,ilGetRc);
      } 
    }
    slFkt = NEXT;
    continue;
  }

  dbg(TRACE,"%s Worked on <%d> records <%s>",
    pclFunc,ilNoOfRec,pcpSelection);

  close_my_cursor (&slCursor);
  return ilRC; 

}

static int UpdateAftAgent( char *pcpAftUrno, char *pcpHtyp, char *pcpHsna )
{
    int ilRc;
    int ili;
    int ilFound;
    char pclAftSelect[512] = "\0";
    char pclTmpHtyp[12] = "\0";
    char *pclFunc = "UpdateAftAgent";

    dbg( TRACE, "%s: AftUrno <%s> Htyp <%s> Hsna <%s>", pclFunc, pcpAftUrno, pcpHtyp, pcpHsna );

    sprintf( pclTmpHtyp, ",%s,", pcpHtyp );
    ilFound = FALSE;
    for( ili = 0; ili < igNumAftAgentFields; ili++ )
    {
        if( strstr( rgAftAgt[ili].pclHtyp, pclTmpHtyp ) == NULL )
            continue;
        ilFound = TRUE;
        break;
    }
    if( ilFound == FALSE )
    {
        dbg( DEBUG, "%s: Nothing to be updated", pclFunc );
        return RC_SUCCESS;
    }
    sprintf( pclAftSelect, "WHERE URNO = %s", pcpAftUrno );

	dbg( DEBUG, "%s Found AFT FIELD <%d><%s> Updating HARA", pclFunc, ili, rgAftAgt[ili].pclAftField );
	dbg( DEBUG, "%s Select <%s>", pclFunc, pclAftSelect );
    SendCedaEvent( igModID_Flight, 0, mod_name, " ",  " ", " ", "UFR", "AFTTAB",
                   pclAftSelect, rgAftAgt[ili].pclAftField, pcpHsna, " ", 3, NETOUT_NO_ACK );
    return RC_SUCCESS;
}
