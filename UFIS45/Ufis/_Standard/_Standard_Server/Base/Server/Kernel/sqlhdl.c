#ifndef _DEF_mks_version
 #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/sqlhdl.c 1.29 2013/07/10 11:18:00SGT cst Exp  $";
#endif /*_DEF_mks_version*/

/* *******************************************************
 * Modulname:		sqlhdl
 * Library:  		sqlhdl.c
 * Author:			Gordon Sawyerr
 * Date:				05/06/97
 * Description:	handles the following commands from clients:
 *			RT    	- Request Table	(reqhdl).
 *			RTA 		- Request Table Array (Rqahdl).
 * 			IBT   	- Insert a record	(damhdl).
 *			GNU 		- Get URNO (ibthdl).
 *			UBT,URT - Update an existing record (ibthdl,damhdl).
 *			IRT 		- Insert record specifying URNO explicitly.
 *			DRT,DBT - Delete a record (ibthdl,damhdl).
 *      SYS			-	SYSTAB Command.
 *			SA23,SA32,SA2O3		- Special commands
 *	(Description added BCH 15/12/95)
 * Update history: Program combines functions of former CEDA
 *                 processes: reqhdl, rqahdl, ibthdl, damhdl.
 * BCH - 15/12/95,  removed a call to check_ret() because if
 * an Oracle error occurs we want to pass the Oracle error
 * back to the application
 * check_ret() effectively makes the error message unavaliable
 * rkl - 15/05/00   Changeing MAIN - Switch DebugFile after 2500 Messages
 * jim - 11/11/2001 Update of shared memory before broadcast
 *                  Use of already read URNO (..list) for Update
 *                  and Delete in database
 * jim - 08/02/2002 SqlHandleUpdate: no rc of new_tools_send_sql
 * jim - 08/02/2002 dam_sql_and_send: no rc of new_tools_send_sql
 * MCU - 05/06/2002 SqlHandleUpdate: don't use rc from dam_sql_and_send
 * AKL - 22/01/2003 New features described in CS for sqlhdl
 * JWE - 22/01/2003 Added syslibUpdate-function to WRD-command in WriteRecordData()-function
 * JWE - 05/02/2003 Added bugfixes from Std. to LIS
 * AKL - 15/05/2003 Delete only max 990 records in one sql-statement
 * MCU - 21/05/2003 Don't send answer if dam_sql_and_send is called in loop for DRT
 * 20030717 JIM: outp alredy is defined in ugccsma.h!
 * AKL - 03/09/2003 Don't send broadcast if dam_sql_and_send is called in loop for DRT,
 *                  also don't call action
 * 20031218 JIM:    PRF 5455: do NOT remove LSTU or USEU from Insert-List
 * 20031218 JIM:    removed sccs_sqlhdl_c
 * 20040507 JIM: first version for CEI-Handling
 * 20040712 JIM:    also check pcgTabsInSHM in WRD section
 * 20040820 JIM: beliebiges config �ndern: GSI CFGTAB sind Pflicht:
 *      cput 0 sqlhdl1 GSI CFGTAB "test_flag = wert" /ceda/conf/test.cfg MAIN
 * 20040823 JIM:    beliebiges per Event empfangenen Befehl ausf�hren
 *      cput 0 sqlhdl1 GSI KHSTAB "Kommando" "" ""
 * 20040823 JIM:    Performancesteigerung durch deaktivieren von CheckHopoUsage
 *                  da in SIN sowieso immer TRUE rauskommt
 * 20040824 JIM: catching von stderr und stdout von KSH-Befehlen korrigiert
 * 20040831 JIM: PRF 6444: Merge of AIA CEI interface into STD45 to avoid
 *               version split
 * 20040928 JIM: setting ilSaveDebugLevel properly
 * 20050308 JIM: PRF 7053 modified some sizes of targets for one field data
 *               from 2048 to 4096 Byte
 * 20050504 JIM: avoid mod_id+1 in WMQ  *
 * 20050523 JIM: PRF 7323: in WMQ send to BCHDL and ACTION, in SYSQCP to mod_id+1
 * 20050523 JIM: do not send double
 * 20051102 JIM: PRF 7875 / 7921: read URNO also when table is not in SHM for BC
 * 20051219 JIM: PRF 8043: avoid memory schmier in GetTabDescrIdx
 * 20110929 GFO: UFIS-881 , UFIS-882
 *               Changing (deleting) a handling agent does not result in sending an update message to BHS
 * 20121029 DKA: UFIS-2467 - v.1.28 Handle new command FPE to enter Flight Permit number
 *               into FPETAB. This command requires a cyclic sequence no. in
 *               NUMTAB which is maintained by SQLHDL.
 *		 The completely formatted PERN will be updated to FPETAB and sent back to
 *		 the workstation.
 * 20121203 DKA: UFIS-2467 - v.1.29 : also broadcast the result.
 * 20130710 CST: Cancelled queuing on mod_id + 1 due to SHM problems
 *
***********************************************************/

/* This program is a CEDA main program */
#define U_MAIN
#define CEDA_PRG


/*
#ifndef UFIS43
	#define UFIS43
#endif
*/

#define URNO_SIZE 10

#define BOOL int

/* The master header file */
#include  "sqlhdl.h"
#include  "db_if.h"
#include  "tools.h"
#include  "loghdl.h"
#include  "hsbsub.h"
#include  "new_catch.h"
#include <time.h>
#ifdef UFIS43
#include "syslib.h"
#endif
#include "multiapt.h"


#define FOR_NORMAL 0
#define FOR_UPDATE 1
#define FOR_INSERT 2
#define FOR_SEARCH 3
#define FOR_TABLES 4

/* 20040406 JIM: UfisToUfis.cfg: Ceda Event Interface */
#define CEI_UNDEF 0
#define CEI_CLIENT 1
#define CEI_SERVER 2

/* 20121102 v.1.28 */
#define SEQUENCE_INIT 1

/***************
internal structures
****************/

/* Mapping Field Lists */
typedef struct
{
  int  Count;
  char Name[33];
  char RcvFldLst[512];
  char UseFldLst[512];
} MAP_LIST;

/* Outsending Field Lists */
typedef struct
{
  int  Count;
  char Table[16];
  char Fields[512];
} OUT_FLD_LIST;


/***************
Global variables
****************/
ITEM 	*prgItem = NULL;
EVENT 	*prgEvent = NULL;

/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX and Linux immediatly !!!!              */
/* 20030717 JIM: FILE *outp; */

int que_out;   /* Sender que id */
int gl_no_ack = FALSE;
int debug_level= TRACE;
char    pcgCfgFile[128];

#define MAX_TMPBUF1_SIZE 8092
#define MAX_TMPBUF2_SIZE 8092
#define MAX_TMPBUF3_SIZE 8092

static char pcgTmpBuf1[MAX_TMPBUF1_SIZE];
static char pcgTmpBuf2[MAX_TMPBUF2_SIZE];
static char pcgTmpBuf3[MAX_TMPBUF3_SIZE];

static int igToBcHdl = 1900;
static int igToAction = 7400;
static int igToLogHdl = 0;
static char pcgOutRoute[64];
static char pcgActionRoute[64];

static int igAddHopo = FALSE;
static int igUseHopo = TRUE;

static int igStartUpMode = TRACE;
static int igRuntimeMode = TRACE;

static time_t tgBeginStamp = 0;
static time_t tgEndStamp = 0;
static time_t tgStampDiff = 0;
static int  igModifyAllowed = 1;  /* altes Verhalten */
static int  igMultiAirport = 0;   /* altes Verhalten */

char pcgTwEnd[64];
static char pcgTwStart[64];
static char pcgDestName[64];
static char pcgRecvName[64];
static char pcgClientAppl[64];
static char pcgDefTblExt[8];
static char pcgTblExt[8];
static char pcgDefH3LC[8];
static char pcgH3LC[8];
static char pcgRecvTablename[64];
static char pcgUfisTables[2048];
static char pcgHopoTables[2048];
static char pcgIgnoreHopo[512];
static char pcgTabsInSHM[2048];  /* List of Tables, which are in SHM */
static char pcgRcvFldLst[4*1024];
static char pcgSqlBuf[12*1024]; /* Sql Statement Buffer */
static char pcgSqlRead[12*1024]; /* Sql Statement Buffer */
static char pcgRcvSelKey[12*1024];
static char pcgActSelKey[12*1024];
static char *pcgResultData=NULL; /* Global result data buffer */
static int igActResultSize=0;
static char pcgDataArea[DATABLK_SIZE]; /* SQLHDL i/o Data Buffer */
static char pcgRcvDatBlk[DATABLK_SIZE];
static char pcgActDatBlk[DATABLK_SIZE];

static char pcgAllOutFld[4096];
static char pcgNewData[DATABLK_SIZE];
static char pcgOldData[DATABLK_SIZE];
static char pcgSveData[DATABLK_SIZE];
static char pcgNewAndOldData[(DATABLK_SIZE)*2];
static char pcgErrMsg[512];

static char pcgFldLst[4096];
static char pcgDatLst[DATABLK_SIZE];
static char pcgUser[128];

static OUT_FLD_LIST prgOutFldLst[150];

static int igTxBegin = 0;   /* Begin of transaction */
static int igTimeOut = 0;   /* Timeout value */
static int igCheckPerf = FALSE;
static long lgMyMaxSizeDiff = 10000;

static int igQueueWait = 0; /* Value for wait after QUE_PUT */

static int igMultiInsert = FALSE; /* Flag for multiple inserts */
static int igMultiInsQueueId = 0; /* QueueId for multiple inserts */
static int igMultiInsPrio = 3; /* Priority for multiple inserts */
static int igMultiUpdate = FALSE; /* Flag for multiple updates */
static int igMultiUpdQueueId = 0; /* QueueId for multiple updates */
static int igMultiUpdPrio = 3; /* Priority for multiple updates */
static int igMultiDelete = FALSE; /* Flag for multiple deletes */
static int igMultiDelQueueId = 0; /* QueueId for multiple deletes */
static int igMultiDelPrio = 3; /* Priority for multiple deletes */
static char pcgMultiSerialToken[256] = "#SER#"; /* token for multiple line processing in serial mode */
static int igMultiLineSerial = FALSE;

/* v.1.28 - for flight permits */
static char pcgFpeCoName [32];
static char pcgFpeAirport [32];

static char pcgSqlQueueIds[256] = "";

typedef struct
{
   /* 20051219 JIM: avoid memory schmier in GetTabDescrIdx
   *  char pcgTableName[8];
   */
  char pcgTableName[33]; /* This size also is defined in uevent.h, obj_name */
  char UrnoRangeKey[16];
  char cgUrnoType;
  char cgHopoType;
  char cgCdatType;
  char cgUsecType;
  char cgLstuType;
  char cgUseuType;
} TAB_DESCR;
#define TAB_DESCR_SIZE sizeof(TAB_DESCR)
TAB_DESCR prgTabDescr[1000];

static int igMaxTabIdx = 0;
static int igCurTabIdx = -1;

/* Keep Default Urno Range per APPL */
typedef struct
{
  char pcgApplName[33];
  char UrnoRangeKey[16];
} APP_DESCR;
#define APP_DESCR_SIZE sizeof(APP_DESCR)
APP_DESCR prgAppDescr[100];

static int igMaxAppIdx = 0;
static int igCurAppIdx = -1;

static char pcgNoSqlWriteCmds[] = ",SQL,RT,RTA,RTB,GSI,SHM,"; /* beware leading and trailing ',' */

/* 20040406 JIM: UfisToUfis.cfg: Ceda Event Interface */
static int igCEI_Role = CEI_UNDEF;
static int igCEI_ModId = 0;
static int igCEI_FinalDest = 0;

/******************
Function prototypes
*******************/
static void InitSqlhdl();    /* Perform general initialization */
static int 	SqlHandleData();	/* Handles event data	*/
static int 	SqlHandleReq();	 	/* DB request function */
static int 	SqlHandleInsert(void);	/* DB insert function */
static int 	SqlHandleUpdate(void);	/* DB update function */
static int 	SqlHandleDelete(void);	/* Db delete function */
static int 	SqlHandleSys(void);	/* Function to manipulate SSM */
static int 	HandleSA23(void);	/* Function to manipulate SSM */
static int 	HandleSA32(void);	/* Function to manipulate SSM */
static int 	HandleS2O3(void);	/* Function to manipulate SSM */
					/* UFIS-2467 */
static int	HandleFPE (void);	/* Generate Flight Permit No. in FPETAB.PERN */
static void 	PutKeySequence(char *pcpKey,long lpSequence,char *pcpFlag);
static long	GetKeySequence(char *pcpKey, char *pcpFlagType,
                  char *pcpFlagYear, char *pcpFlagMonth);
static void	StringTrimRight(char *pcpBuffer);

static int  GetNextUrno(); /* Get next urno */
static int  GetManyUrno(); /* Get next urno */
static int  GetManyNumbers(); /* Get several numbers */
static int reset();		/* Reset program		*/
static void sql_terminate();	/* Terminate program	*/
static void sql_handle_sig();	/* Handles signals	*/
static void sql_handle_err(int);	/* Handles general errors	*/
static void sql_handle_qerr(int);	/* Handles queuing errors	*/
static int build_sql_buf(char *buf, char *pcpFields,char *pcpTable,char *pcpFilter);
static int dam_sql_and_send(char *pcpSqlBuf,char *pcpDatablk,char *pcpFldLst,BC_HEAD *prpBchd,char *pcpUrnoList,char cpShmCmd, BOOL bpSendAnswer, BOOL bpSendBc);
static unsigned long ExtractUrno(char *, char *);
static int ParseSel(char *,char *,char *); /* Parses selection */
static void HandleQueues(void);

extern SendRemoteShutdown(int);
extern void str_chg_upc(char *);
extern int StrgPutStrg(char *, int *, char *, int, int, char *);
extern int get_real_item(char *, char *, int);
extern void str_trm_all(char *,char *, int);
extern int  get_item_no(char *,char *, short);
extern void BuildItemBuffer(char *,char *,int,char *);
extern int GetNoOfElements(char *, char);
extern void nap(int);
extern void GetServerTimeStamp(char *, int, long, char *);
static void CheckWhereClause(int, char *, int, int, char *);
static void CheckHomePort(void);
static int CheckTableExtension(char *);

static void CheckPerformance(int ipFlag);
static void RemoveHopoFromList(char *pcpFields, char *pcpData);
static void RemoveHopoInWhereClause(char *pcpSelKey);
static void CompareHopoInList(char *pcpFldBuf,char* FldData);
static void CompareHopoInWhereClause(char *pcpSelKey);
static void SetFieldValue(int, char *,char *,char *,char *);
static void CollectUfisTables(void);
static void CollectHopoTables(void);
static void CheckUrnoValue(char *, char *, long);
static int CheckUrnoType(char *pcpTable);
static void trim_quotes (char *pcpData,char cpQuote);
static int HandleFreeSql(char *,char *,char *,char *);
static int WriteRecordData(char *,char *,char *,char *);

static int ReadCfg(char *, char *, char *, char *, char *);
static int ReleaseActionInfo(char *,char *,char *,char *,
			     char *,char *,char *,char *);
static int GetOutModId(char *pcpProcName);
static void GetLogFileMode(int *ipModeLevel, char *pcpLine, char *pcpDefault);
static void InitOutFieldList(void);

static char *SkipChars(char *pcpStrg, char *pcpPattern);
static char *SkipWords(char *pcpStrg, char *pcpPattern);
static int BuildSqlFldStrg(char *pcpSqlStrg, char *pcpFldLst, int ipTyp);
static void ReadOldData(char *, char *, char *, char *);
static void BuildOutFieldList(char *pcpRcvFld, char *pcpOutFld);
static void BuildNewData(char *pcpRcvFld, char *pcpRcvDat);
static void OutFieldsToSelFields(char *pcpTbl, char *pcpFld);

static int new_tools_send_sql_perf(int ipRouteId, BC_HEAD *prpBchd, CMDBLK *prpCmdblk,
                                   char *pcpFields, char *pcpData);
static int tools_send_info_flag_perf(int ipRouteId, int ipOrigId, char *pcpDestName,
                                     char *pcpOrigName, char *pcpRecvName, char *pcpSeqId,
                                     char *pcpRefSeqId, char *pcpTwStart, char *pcpTwEnd,
                                     char *pcpCommand, char *pcpTabName, char *pcpSelection,
                                     char *pcpFields, char *pcpData, int ipFlag);
static int tools_send_sql_rc_perf(int ipRouteId, char *pcpCommand, char *pcpTabName,
                                  char *pcpSelection, char *pcpFields, char *pcpData,
                                  int ipRetCode);
static int SaveStartTime(int ipRouteId, char *pcpStartTime);
static int CheckTxTimeout(int ipRouteId);
static int GetOraErr(int ipErrCode, char *pcpResultBuffer, char *pcpSqlStatement,
                     char *pcpRoutine);
static void TrimRight(char *pcpBuffer);
static void GetTabDescrIdx(char *pcpCommand, char *pcpTabName);
static void GetAppDescrIdx(char *pcpCommand, char *pcpAppName);

static int CheckURNO(char cpFkt, char cpFieldType, char *pcpFieldList,
                     char *pcpVList, char *pcpDList);
static int CheckHOPO(char cpFkt, char cpFieldType, char *pcpFieldList,
                     char *pcpVList, char *pcpDList);
static int CheckCDAT(char cpFkt, char cpFieldType, char *pcpFieldList,
                     char *pcpVList, char *pcpDList);
static int CheckUSEC(char cpFkt, char cpFieldType, char *pcpFieldList,
                     char *pcpVList, char *pcpDList);
static int CheckLSTU(char cpFkt, char cpFieldType, char *pcpFieldList,
                     char *pcpVList, char *pcpDList);
static int CheckUSEU(char cpFkt, char cpFieldType, char *pcpFieldList,
                     char *pcpVList, char *pcpDList);
static int RemoveItemFromList(int ipFldNum, char *pcpList);
static int AddItemToList(char *pcpList, char *pcpVal);
static int AddListToItem(char *pcpList, char *pcpVal);
static void CheckQueueWait(int ipRouteId);
static void CheckMultiInput(char *pcpCommand, char *pcpTable, char **pcpSelection,
                            char **pcpFields, char **pcpData, char cpFkt);


static int HandleGetServerInfo(char *pcpTbl, char *pcpSel, char *pcpFld, char *pcpData);

/* 20040406 JIM: Ceda Event Interface */
void SetupCEI(void);


/***************
The MAIN program
****************/
MAIN
{
	int 		rc;
	int		ilRC;
	int		ilCnt;
	char		pclFileName[512];
char pclTstFields[128];
char pclTstData[128];

	int ilMsgCounter = 0;
    int ilDbgFileCount = 0;

	/* General initialization	*/
	INITIALIZE;
	dbg(TRACE,"<MAIN VERSION>\n%s",mks_version);

#ifdef UFIS43
	dbg(DEBUG,"<MAIN> USING UFIS 4.3 FEATURES");
#else
	dbg(TRACE,"<MAIN> NOT USING UFIS 4.3 FEATURES");
#endif

	/* handles signal		*/
	(void)SetSignals(sql_handle_sig);
	(void)UnsetSignals();

	/* Attach to the CCS queues */
	ilCnt = 0;
	do
	{
		if ((ilRC = init_que()) != RC_SUCCESS)
		{
			sleep(6);		/* Wait for QCP to create queues */
			ilCnt++;
		}
	} while ((ilCnt < 10) && (ilRC != RC_SUCCESS));

	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que failed!");
		sleep(30);
		sql_terminate();
	}

	/* logon to DB */
	ilCnt = 0;
	do
	{
		dbg(DEBUG,"<MAIN> calling init_db()");
		if ((ilRC = init_db()) != RC_SUCCESS)
		{
			sleep(6);		/* Wait for next try */
			ilCnt++;
		}
	}while ((ilCnt < 10) && (ilRC != RC_SUCCESS));

	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db failed!");
		sleep(30);
		sql_terminate();
	}

	/* transfer binary file to remote machine... */
	pclFileName[0] = 0x00;
	sprintf(pclFileName, "%s/sqlhdl", getenv("BIN_PATH"));
	dbg(DEBUG,"<MAIN> %05d TransferFile: <%s>", __LINE__, pclFileName);
	if ((ilRC = TransferFile(pclFileName)) != RC_SUCCESS)
	{
		dbg(TRACE,"<MAIN> TransferFile <%s> returns: %d", pclFileName, ilRC);
		set_system_state(HSB_STANDALONE);
	}

	/* transfer configuration file to remote machine... */
	pclFileName[0] = 0x00;
	sprintf(pclFileName, "%s/sqlhdl.cfg", getenv("CFG_PATH"));
	strcpy(pcgCfgFile,pclFileName);
	dbg(DEBUG,"<MAIN> %05d TransferFile: <%s>", __LINE__, pclFileName);
	if ((ilRC = TransferFile(pclFileName)) != RC_SUCCESS)
	{
		dbg(TRACE,"<MAIN> TransferFile <%s> returns: %d", pclFileName, ilRC);
		set_system_state(HSB_STANDALONE);
	}

GetLogFileMode(&igStartUpMode,"STARTUP_MODE","OFF");
debug_level = igStartUpMode;
GetLogFileMode(&igRuntimeMode,"RUNTIME_MODE","OFF");

	/* ask VBL about this!! */
	dbg(DEBUG,"<MAIN> %05d SendRemoteShutdown: %d", __LINE__, mod_id);
	if ((ilRC = SendRemoteShutdown(mod_id)) != RC_SUCCESS)
	{
		dbg(TRACE,"<MAIN> SendRemoteShutdown(%d) returns: %d", mod_id, ilRC);
		set_system_state(HSB_STANDALONE);
	}

	if ((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"<MAIN> waiting for status switch ...");
		HandleQueues();
	}

	if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"<MAIN> initializing ...");
		InitSqlhdl();
	}
	else
	{
		sleep(30);
		sql_terminate();
	}

	/* only a message */
        CheckMySizeDiff(-1);
	dbg(TRACE,"<MAIN> initializing OK");
        debug_level = igRuntimeMode;

	/* forever */
	while(1)
	{
                dbg(TRACE,"=================== START/END ==================");
		/* get item */
		rc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,0,(char*)&prgItem);

		/* set event pointer */
		prgEvent = (EVENT*)prgItem->text;

		/* check returncode */
		if (rc == RC_SUCCESS)
		{
			if ((rc = que(QUE_ACK,0,mod_id,0,0,NULL)) != RC_SUCCESS)
			{
				sql_handle_qerr(rc);
			}

			switch (prgEvent->command)
			{
			case REMOTE_DB:
				HandleRemoteDB(prgEvent);
				break;

			case SHUTDOWN:
				sql_terminate();
				break;

			case RESET:
				rc = reset();
				break;

			case EVENT_DATA:
				if(ctrl_sta==HSB_ACTIVE || ctrl_sta==HSB_STANDALONE || ctrl_sta==HSB_ACT_TO_SBY )
				{
				    /* Switch Debug File after ilMsgcount */
					ilMsgCounter++;
  					if((ilMsgCounter % 2500) == 0)
  					{
  						SwitchDebugFile(debug_level,&ilDbgFileCount);
						ilMsgCounter = 0;
  					}

					rc = SqlHandleData();
				}
				else
				{
					dbg(DEBUG,"MAIN %05d wrong hsb status <%d>", __LINE__, ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}
				break;

			case TRACE_ON:  /* 7 */
			case TRACE_OFF: /* 8 */
				dbg_handle_debug(prgEvent->command);
				break;

			case LOG_SWITCH: /* 2 */
				SwitchDebugFile(debug_level,&ilDbgFileCount);
				break;

			case HSB_DOWN:
				ctrl_sta = prgEvent->command;
				sql_terminate(RC_SHUTDOWN);
				break;

			case HSB_STANDALONE:
				ctrl_sta = prgEvent->command;
				send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
				ResetDBCounter();
				break;

			case HSB_STANDBY:
				ctrl_sta = prgEvent->command;
				send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
				HandleQueues();
				break;

			case HSB_ACTIVE:
				ctrl_sta = prgEvent->command;
				send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
				break;

			case HSB_COMMING_UP:
				ctrl_sta = prgEvent->command;
				send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
				HandleQueues();
				break;

			case HSB_ACT_TO_SBY:
				ctrl_sta = prgEvent->command;
				send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
				HandleQueues();
				break;

			case 100:
				rc=PrintAllCursorStatistics();
				break;

case 777:
dbg(TRACE,"---------- TEST START -------------");
dbg(TRACE,"---------- TEST END ---------------");
rc = RC_SUCCESS;
break;

			default:
				dbg(TRACE,"sqlhdl: unknown command =%d",
				prgEvent->command);
				break;
			}

			/* Handle error conditions */
			if (rc!=RC_SUCCESS)
			{
				sql_handle_err(rc);
			}
		}
		else
		{
			sql_handle_qerr(rc);
		}
	}
}

/* ******************************************
 * Function: 	InitSqlhdl
 * Parameter: void
 * Return: 		void
 * Decripion: General initialisation routine,
 * sets up queue and database connection.
 * v.1.28 - read Flight Permit co-name+airport if any, otherwise use
 * defaulted AUH values.
*********************************************/
static void InitSqlhdl()
{
  int rc=RC_SUCCESS;
  int ilRC;
  int i = 0;
  char *pclCfgPath;
  char pclActPath[128];
  char pclCfgFile[128];
  char pclTmpBuf[128];
  char pclCfgSec[128];
  char pclCfgDet[128];
  char pclTmpItem[128];

  SetupCEI();  /* 20040406 JIM: Ceda Event Interface */

  pcgDefTblExt[0] = 0x00;
  pcgDefTblExt[0] = 0x00;
  pcgDefH3LC[0] = 0x00;
  rc = tool_search_exco_data("ALL","TABEND",pcgDefTblExt);
  strcpy(pcgTblExt,pcgDefTblExt);
  rc = tool_search_exco_data("SYS","HOMEAP",pcgDefH3LC);
  strcpy(pcgH3LC,pcgDefH3LC);
  dbg(TRACE,"DEFAULTS: HOME <%s> EXT <%s>",pcgDefH3LC,pcgDefTblExt);
  rc = CheckTableExtension("SYS");

  CollectUfisTables();

  if (pcgResultData==NULL)
  {
     pcgResultData = (char *) calloc(1, I_RESULTBUFSTEP);
     dbg(TRACE,"INIT: ALLOCATED %d BYTES", I_RESULTBUFSTEP);
     igActResultSize= I_RESULTBUFSTEP;
     if (pcgResultData==NULL)
     {
        igActResultSize=0;
        dbg(TRACE,"InitSqlhdl: Malloc(pcgResultData) failed");
     } /* fi */
  } /* end if */


  /* igToLogHdl = tool_get_q_id("loghdl"); */
  igToLogHdl = GetOutModId("loghdl");
  igToBcHdl  = GetOutModId("bchdl");
  igToAction = GetOutModId("action");

  dbg(TRACE,"SPECIAL OUT QUEUES:");
  dbg(TRACE,"LOGHDL=%d, ACTION:%d BCHDL: %d",igToLogHdl,igToAction,igToBcHdl);
  sprintf(pcgOutRoute,"%d",igToLogHdl);
  sprintf(pcgActionRoute,"%d",igToAction);

  InitOutFieldList();

  pclTmpBuf[0] = 0x00;
  if ((ilRC = iGetConfigEntry(pcgCfgFile,
                              "MAIN", "MULTIAIRPORT", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
  {
     dbg(TRACE,"INIT: Setting MULTIAIRPORT to default NO" );
     igMultiAirport=0;
  }
  if (!strcmp(pclTmpBuf,"YES"))
  {
     igMultiAirport=1;
     dbg(TRACE,"INIT: MULTIAIRPORT = <YES>");
  }
  else if (!strcmp(pclTmpBuf,"NO"))
  {
     igMultiAirport=0;
     dbg(TRACE,"INIT: MULTIAIRPORT = <NO>");
  }
  else
  {
     dbg(TRACE,"INIT: Setting MULTIAIRPORT to default NO" );
     igMultiAirport = 0;
  }

  ilRC = iGetConfigEntry(pcgCfgFile,"QUEUE_PARAMETERS","QUEUE_WAIT",CFG_STRING,pclTmpBuf);
  if (ilRC == RC_SUCCESS)
  {
     igQueueWait = atoi(pclTmpBuf);
  }
  dbg(TRACE,"Wait after each QUE_PUT %d ms",igQueueWait);

  ilRC = iGetConfigEntry(pcgCfgFile,"QUEUE_PARAMETERS","MULTI_INS",CFG_STRING,pclTmpBuf);
  if (ilRC == RC_SUCCESS)
  {
     ilRC = get_real_item(pclTmpItem,pclTmpBuf,1);
     if (strcmp(pclTmpItem,"YES") == 0)
     {
        igMultiInsert = TRUE;
        ilRC = get_real_item(pclTmpItem,pclTmpBuf,2);
        igMultiInsQueueId = atoi(pclTmpItem);
        ilRC = get_real_item(pclTmpItem,pclTmpBuf,3);
        igMultiInsPrio = atoi(pclTmpItem);
     }
  }
  if (igMultiInsert == TRUE)
  {
     dbg(TRACE,"Multiple insert allowed for queue %d with priority %d",
         igMultiInsQueueId,igMultiInsPrio);
  }
  else
  {
     dbg(TRACE,"Multiple insert is not allowed");
  }
  ilRC = iGetConfigEntry(pcgCfgFile,"QUEUE_PARAMETERS","MULTI_UPD",CFG_STRING,pclTmpBuf);
  if (ilRC == RC_SUCCESS)
  {
     ilRC = get_real_item(pclTmpItem,pclTmpBuf,1);
     if (strcmp(pclTmpItem,"YES") == 0)
     {
        igMultiUpdate = TRUE;
        ilRC = get_real_item(pclTmpItem,pclTmpBuf,2);
        igMultiUpdQueueId = atoi(pclTmpItem);
        ilRC = get_real_item(pclTmpItem,pclTmpBuf,3);
        igMultiUpdPrio = atoi(pclTmpItem);
     }
  }
  if (igMultiUpdate == TRUE)
  {
     dbg(TRACE,"Multiple update allowed for queue %d with priority %d",
         igMultiUpdQueueId,igMultiUpdPrio);
  }
  else
  {
     dbg(TRACE,"Multiple update is not allowed");
  }
  ilRC = iGetConfigEntry(pcgCfgFile,"QUEUE_PARAMETERS","MULTI_DEL",CFG_STRING,pclTmpBuf);
  if (ilRC == RC_SUCCESS)
  {
     ilRC = get_real_item(pclTmpItem,pclTmpBuf,1);
     if (strcmp(pclTmpItem,"YES") == 0)
     {
        igMultiDelete = TRUE;
        ilRC = get_real_item(pclTmpItem,pclTmpBuf,2);
        igMultiDelQueueId = atoi(pclTmpItem);
        ilRC = get_real_item(pclTmpItem,pclTmpBuf,3);
        igMultiDelPrio = atoi(pclTmpItem);
     }
  }
  if (igMultiDelete == TRUE)
  {
     dbg(TRACE,"Multiple delete allowed for queue %d with priority %d",
         igMultiDelQueueId,igMultiDelPrio);
  }
  else
  {
     dbg(TRACE,"Multiple delete is not allowed");
  }

  ilRC = iGetConfigEntry(pcgCfgFile,"QUEUE_PARAMETERS","QUEUE_IDS",CFG_STRING,pcgSqlQueueIds);
  dbg(TRACE,"Queue Ids of SQLHDL are <%s>",pcgSqlQueueIds);

  ilRC = iGetConfigEntry(pcgCfgFile,"QUEUE_PARAMETERS","MULTI_SERIAL_TOKEN",
                         CFG_STRING,pclTmpBuf);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgMultiSerialToken,pclTmpBuf);
  }
  dbg(TRACE,"Token for processing multiple lines in serial mode (TWS) = <%s>",
      pcgMultiSerialToken);

  CollectHopoTables();

  ilRC = iGetConfigEntry(pcgCfgFile,"SYSTEM","MAX_SIZE_DIFF",CFG_STRING,pclTmpBuf);
  if (ilRC == RC_SUCCESS)
  {
     lgMyMaxSizeDiff = atol(pclTmpBuf);
  }
  dbg(TRACE,"MAX_SIZE_DIFF LIMIT=%d K",lgMyMaxSizeDiff);


  /* v.1.28 - flight permit UFIS-2467 */
  ilRC = iGetConfigEntry(pcgCfgFile,"FLIGHT_PERMIT","CO_NAME",CFG_STRING,pclTmpBuf);
  if (ilRC == RC_SUCCESS)
  {
    strcpy (pcgFpeCoName,pclTmpBuf);
  }
  else
  {
    dbg(TRACE,"InitSqlhdl: FLIGHT_PERMIT:CO_NAME not found, use default");
    strcpy (pcgFpeCoName,"ADAC");
  }
  dbg(TRACE,"InitSqlhdl: FLIGHT_PERMIT:CO_NAME <%s>",pcgFpeCoName);

  strcpy (pcgFpeAirport,pcgDefH3LC);
  dbg(TRACE,"InitSqlhdl: FLIGHT_PERMIT AIRPORT is set to HOPO <%s>",pcgFpeAirport);


  i= 0;
  ilRC = RC_SUCCESS;
  while (ilRC == RC_SUCCESS)
  {
    i++;
    sprintf(pclTmpItem,"INIT_%d",i);
    ilRC = DB_GetUfisCedaCfg("URNO_RANGE_KEYS", pclTmpItem, pclTmpBuf, "");
    if (ilRC == RC_SUCCESS)
    {
      GetTabDescrIdx("GMU",pclTmpBuf);
    }
  }



  return;
} /* end of initialize */

/* ***************************************************
 * Function: 		SqlHandleData
 * Parameter: 	void
 * Return: 			RC_SUCCESS, RC_FAIL, SQL_NOTFOUND
 * Description: The general handle data routine.
 * Commands recognized: RT,RTA,IBT,GNU,IRT,UBT
 * URT,DRT,DBT.
 * FPE - UFIS-2467 : Flight Permit number generation for FPETAB.
 *****************************************************/
static int SqlHandleData()
{
  int rc = 0;			/* Return code 			*/
  int ilRC;
#ifndef _WINNT
  struct mallinfo rlMallInf;
#endif
  char pclModifyAllowed[128];
  int ilTwEndFldCnt;
  BC_HEAD *prlBchd = NULL;		/* Broadcast header		*/
  CMDBLK  *prlCmdblk = NULL;	/* Command Block 		*/
  char *pclSel=NULL,*pclFld=NULL,*pclData=NULL;
	int ilCnt=0;
	int ilCount=0;
  int ilLen = 0;
  char pclWRDSingleDataLine[DATABLK_SIZE];
  char pclWRDSelection[DATABLK_SIZE];
  int ilSaveDebugLevel;

  que_out = prgEvent->originator; 	/* Queue-id des Absenders */
  prlBchd = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  prlCmdblk = (CMDBLK  *) ((char *)prlBchd->data);
  pclSel = prlCmdblk->data;
  pclFld = pclSel + strlen(pclSel)+1;
  pclData = pclFld + strlen(pclFld)+1;

  memset(pcgOldData,0x00,DATABLK_SIZE);
  pcgNewAndOldData[0] = 0x00;
  pcgAllOutFld[0] = 0x00;

  ilSaveDebugLevel= debug_level;  /* 20040928 JIM: setting properly */
  if ((strcmp(prlCmdblk->obj_name,"KSHTAB") == 0) ||
      (strcmp(prlCmdblk->obj_name,"CFGTAB") == 0))
  {  /* don't debug this! */
      debug_level = 0;
  }
  dbg(TRACE,"CMD <%s> TBL <%s>", prlCmdblk->command,prlCmdblk->obj_name);
  dbg(TRACE,"FROM <%d> WKS <%s> USR <%s>",
      que_out,prlBchd->recv_name,prlBchd->dest_name);
  strcpy(pcgUser,prlBchd->dest_name);
  ilRC = SaveStartTime(que_out,prlBchd->orig_name);
  /*
   * GFO Removed this already exists in line 893
   * GetTabDescrIdx(prlCmdblk->command,prlCmdblk->obj_name);*/

  strncpy(pcgRecvName, prlBchd->recv_name, sizeof(prlBchd->recv_name));
  pcgRecvName[sizeof(prlBchd->recv_name)] = 0x00;
  strncpy(pcgDestName, prlBchd->dest_name, sizeof(prlBchd->dest_name));
  pcgDestName[sizeof(prlBchd->dest_name)] = 0x00;
  strncpy(pcgTwStart, prlCmdblk->tw_start, sizeof(prlCmdblk->tw_start));
  pcgTwStart[sizeof(prlCmdblk->tw_start)-1] = 0x00;
  strncpy(pcgTwEnd, prlCmdblk->tw_end, sizeof(prlCmdblk->tw_end));
  pcgTwEnd[sizeof(prlCmdblk->tw_end)-1] = 0x00;

  /* Get the Application Name */
  ilLen = get_real_item(pcgClientAppl,pcgTwEnd,3);
  if (ilLen <=0)
  {
    strcpy(pcgClientAppl,"UNDEFINED");
  }

  GetTabDescrIdx(prlCmdblk->command,prlCmdblk->obj_name);

  if (strstr(pcgTwStart,pcgMultiSerialToken) != NULL)
  {
     igMultiLineSerial = TRUE;
  }
  else
  {
     igMultiLineSerial = FALSE;
  }

  if (igMultiAirport == 1)
  {
     ilTwEndFldCnt = field_count(pcgTwEnd);
     if (ilTwEndFldCnt == 4)
     {
        ilRC = get_real_item(pclModifyAllowed,pcgTwEnd,4);
        igModifyAllowed = atoi(pclModifyAllowed);
        dbg(DEBUG,"Modify allowed = <%d>",igModifyAllowed);
     }
     else
     {
        igModifyAllowed = 1;
        dbg(TRACE,"HOPO-FAILURE: Wrong Number of Items in TW_END - Using default <%d> for ModifyFlag",
            igModifyAllowed);
     }
  }
  else
  {
     igModifyAllowed = 1;
     dbg(DEBUG,"Modify allowed = <%d>",igModifyAllowed);
  }

  /* Trimmed TableName */
  strncpy(pcgRecvTablename, prlCmdblk->obj_name, sizeof(prlCmdblk->obj_name));
  str_trm_all(pcgRecvTablename," ",TRUE);

  dbg(TRACE,"SPECIAL INFO TWS <%s> TWE <%s>",pcgTwStart,pcgTwEnd);
  dbg(TRACE,"\nFLD <%s>\nDAT <%s>\nSEL <%s>",pclFld,pclData,pclSel);

/* pcgH3LC setzen
  if (igMultiAirport == 1)
  {
     ilRC = get_real_item(pcgH3LC,pcgTwEnd,1);
     dbg(TRACE," Using RECEIVED HOPO=<%s>",pcgH3LC);
  }
  else
  {
     strcpy(pcgH3LC,pcgDefH3LC);
     dbg(DEBUG,"USE DEFAULT HOPO <%s>",pcgH3LC);
  }
*/

  tgBeginStamp = 0;
  tgEndStamp = 0;
  tgStampDiff = 0;

  CheckHomePort();
  igAddHopo = FALSE;

  if (strcmp(pcgDefTblExt,"TAB") == 0)
  {
     if (strstr(pcgUfisTables,pcgRecvTablename) != NULL)
     {
        igAddHopo = TRUE;
        igUseHopo = TRUE;
     } /* end if */


/* Ganz_Neues_HOPO-Verhalten:
*     / * Neues HOPO-Verhalten * /
*     / * if ((igMultiAirport == 1) && (strstr(pcgHopoTables,pcgRecvTablename)== NULL))* /
*     ilRC=CheckHopoUsage(pcgRecvTablename);
*     dbg(DEBUG,"CheckHopoUsage for Table <%s> returns <%d>",pcgRecvTablename,ilRC);
*     if ((igMultiAirport == 1) && ( ilRC != RC_SUCCESS))
*     {
*        igUseHopo = FALSE;
*        dbg(TRACE,"USE HOPO = <FALSE>");
*     } / * end if * /
*     else
*     {
*        igUseHopo = TRUE;
*        dbg(TRACE,"USE HOPO = <TRUE>");
*     }
* Ganz_Neues_HOPO-Verhalten
*/
  } /* end if */

  if (prlBchd->rc == NETOUT_NO_ACK)
  {
     gl_no_ack = TRUE;
  }
  else
  {
     gl_no_ack = FALSE;
  } /* end if */

  if (strcmp(prlCmdblk->command, "IBT")==0 || strcmp(prlCmdblk->command,"IRT")==0)
  {
     rc=SqlHandleInsert();
  }
  else if(strcmp(prlCmdblk->command, "GNU")==0)
  {
     rc=GetNextUrno();
  }
  else if(strcmp(prlCmdblk->command, "GMN")==0)
  {
     rc=GetManyNumbers();
  }
  else if(strcmp(prlCmdblk->command, "GMU")==0)
  {
     rc=GetManyUrno();
  }
  else if(strcmp(prlCmdblk->command,"URT")==0 || strcmp(prlCmdblk->command,"UBT")==0)
  {
     strcpy(prlCmdblk->command, "URT");
     rc=SqlHandleUpdate();
  }
  else if(strcmp(prlCmdblk->command,"DRT")==0 || strcmp(prlCmdblk->command,"DBT")==0)
  {
     strcpy(prlCmdblk->command, "DRT");
     rc=SqlHandleDelete();
  }
  else if(strcmp(prlCmdblk->command,"RT")==0)
  {
     dbg(DEBUG,"Received <%s> command",prlCmdblk->command);
     rc=SqlHandleReq();
  }
  else if(strcmp(prlCmdblk->command,"RTA")==0 || strcmp(prlCmdblk->command,"RTB")==0)
  {
     rc=SqlHandleReq();
  }
  else if(strcmp(prlCmdblk->command,"SYS")==0 )
  {
     dbg(DEBUG,"HandleData: Received SYS command from <%d>",que_out);
     rc=SqlHandleSys();
  }
  else if(strcmp(prlCmdblk->command,"SQL")==0)
  {
     rc=HandleFreeSql(prlCmdblk->obj_name,pclSel,pclFld,pclData);
  }
  else if(strcmp(prlCmdblk->command,"GSI")==0)
  {
     rc=HandleGetServerInfo(prlCmdblk->obj_name,pclSel,pclFld,pclData);
  }
  else if(strcmp(prlCmdblk->command,"WRD")==0)
  {
		 /* expecting single line or LF-delimited package of lines for WRD command */
		 ilCount = (int)GetNoOfElements(pclData,0x0A);
		 if (ilCount > 0)
		 {
			 for (ilCnt=0;ilCnt<ilCount;ilCnt++)
			 {
		 		*pclWRDSelection=0x00;
		 		strcpy(pclWRDSelection,pclSel);
				memset(pclWRDSingleDataLine,0x00,DATABLK_SIZE);
				GetDataItem(pclWRDSingleDataLine,pclData,ilCnt+1,0x0A,"","");
				dbg(TRACE,"WRD: processing line <%d>/<%d>!",ilCnt+1,ilCount);
				rc=WriteRecordData(prlCmdblk->obj_name,pclWRDSelection,pclFld,pclWRDSingleDataLine);
				if (rc != RC_SUCCESS)
					dbg(TRACE,"WRD: line <%d>/<%d>=<%s> failed to be processed!",ilCnt+1,ilCount,pclWRDSingleDataLine);
			 }
		 }
		 else if (ilCount==0 && strlen(pclData)>0)
		 {
		 		*pclWRDSelection=0x00;
		 		strcpy(pclWRDSelection,pclSel);
     		rc=WriteRecordData(prlCmdblk->obj_name,pclWRDSelection,pclFld,pclData);
					if (rc != RC_SUCCESS)
						dbg(TRACE,"WRD: line <%s> failed to be processed!",pclData);
		 }
  }
  else if(strcmp(prlCmdblk->command,"SA32")==0)
  {
     rc=HandleSA32();
  }
  else if(strcmp(prlCmdblk->command,"SA23")==0)
  {
     rc=HandleSA23();
  }
  else if(strcmp(prlCmdblk->command,"S2O3")==0)
  {
     rc=HandleS2O3();
  }
  else if(strcmp(prlCmdblk->command,"SHM")==0)
  {
     rc=HandleSHM(pclSel,prlCmdblk->obj_name,pclFld,pclData);
  }
  else if (strcmp (prlCmdblk->command,"FPE") == 0)
  {
    rc = HandleFPE();
  }
  else
  {
     dbg(TRACE,"%.4s. This is not a valid command", prlCmdblk->command);
     rc = RC_FAIL;
  } /* end else ifs */

  if ((ilRC = CheckMySizeDiff(lgMyMaxSizeDiff)) != RC_SUCCESS)
  {
    sql_terminate();
  }
  debug_level = ilSaveDebugLevel;
  return rc;
} /* SqlHandleData */


/* ***************************************************
 * Function: 		SqlHandleReq
 * Parameter: 	void
 * Return: 			RC_SUCCESS, RC_FAIL, SQL_NOTFOUND
 * Description: Handles smaller DB requests.
 **************************************************** */
static int SqlHandleReq()
{
  int rc = RC_SUCCESS;	/* Return code */
  short ilFkt;		/* database function	*/
  BC_HEAD *prlBchd;	/* Broadcast header	*/
  CMDBLK *prlCmdblk;	/* Command Block 	*/
  char pclLine[MAX_ROW_LEN];
  char *pclSelection;
  char *pclFields;
  char pclRcvSelKey[12*1024];
  short ilFldcnt,i,ilLocalCursor;
  int ilLineLen=0;
  int ilTotalResultBufLen=0;
  int ilCnt = 0;

  que_out = prgEvent->originator; 	/* Queue-id des Absenders */
  prlBchd = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  prlCmdblk = (CMDBLK  *) ((char *)prlBchd->data);
  pclSelection = prlCmdblk->data;
  pclFields = pclSelection + strlen(pclSelection)+1;

  if (pcgResultData==NULL)
  {
     pcgResultData = (char *) calloc(1, RESULTBUFSTEP);
     dbg(DEBUG,"SqlHandleReq: Alloced pcgResultData");
     if (pcgResultData==NULL)
     {
        prlBchd->rc = RC_FAIL;
        igActResultSize=0;
        dbg(TRACE,"SqlHandleReq: Malloc(pcgResultData) failed");
        rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,"");
        return rc;
     } /* fi */
     else
     {
        igActResultSize=RESULTBUFSTEP;
        dbg(DEBUG,"SqlHandleReq: Initializing pcgResultData");
     }
  }
  pcgResultData[0]='\0';
  strcpy(pclRcvSelKey,pclSelection);
  CompareHopoInWhereClause(pclRcvSelKey);
  if (igUseHopo == 0)
  {
     RemoveHopoInWhereClause(pclRcvSelKey);
  }
  dbg(DEBUG,"SqlHandleReq: Field<%s>",pclFields);
  ilFldcnt = build_sql_buf(pcgSqlBuf,pclFields,prlCmdblk->obj_name,pclRcvSelKey);
  ilLocalCursor = 0;
#ifdef CCSDB_INFORMIX
  ilFkt = START|COMMIT;
#else
  ilFkt = START;
#endif
  rc = RC_SUCCESS;
  dbg(TRACE,"SqlHandleReq: sqlbuf\n<%s>",pcgSqlBuf);
  ilCnt = 0;
  CheckPerformance(TRUE);
  while (rc == RC_SUCCESS)
  {
     /* Get record from database */
     memset(pcgDataArea,'\0',5);
     dbg(DEBUG,"SqlHandleReq: Initiating read from database");
     rc = sql_if(ilFkt, &ilLocalCursor, pcgSqlBuf, pcgDataArea);
     dbg(DEBUG,"SqlHandleReq: Returned from sql_if<%d> Cursor<%hd>",rc,ilLocalCursor);
     if ( rc == RC_SUCCESS )
     {
        ilCnt++; /* Record count */
        memset(pclLine,0x00,MAX_ROW_LEN);
        for (i=0; i<ilFldcnt; i++)
        {
           get_fld(pcgDataArea,i,STR,MAX_FIELD_LEN,pclLine+strlen(pclLine));
           if ( i<(ilFldcnt -1))
           {
              strcat(pclLine,",");
           } /* end if */
        } /* end for */
        ilLineLen=strlen(pclLine);
        memcpy(pclLine+ilLineLen,"\r\n",3); /* For Toolbook */
        ilLineLen+=2;
        dbg(DEBUG,"HandleRequest: No. of lines<%d>  Got line<%s>",ilCnt,pclLine);
        if ((ilTotalResultBufLen+ilLineLen+sizeof(BC_HEAD)+sizeof(EVENT))>igActResultSize)
        {
           dbg(DEBUG,"SqlHandleReq: Oh,oh have to reallocate. This may take some time....ActSize<%d> Need at least<%d>",
               igActResultSize,(ilTotalResultBufLen+ilLineLen+sizeof(BC_HEAD)+sizeof(EVENT)));
           /* Result data buffer full! Reallocate space */
           while ((ilTotalResultBufLen+ilLineLen+sizeof(BC_HEAD)+sizeof(EVENT))>igActResultSize)
              igActResultSize += RESULTBUFSTEP;
           pcgResultData=(char *) realloc(pcgResultData,igActResultSize);
           if (pcgResultData == NULL)
           {
              rc = RC_FAIL;
              if (ilLocalCursor!=0)
                 close_my_cursor(&ilLocalCursor);
              dbg(TRACE,"SqlHandleReq: Realloc 3 failed");
              prlBchd->rc = RC_FAIL;
              igActResultSize=0;
              dbg(TRACE,"SqlHandleReq: Malloc(pcgResultData) failed");
              strcpy(pcgDataArea,"Insufficient memory!!");
              /* rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,"");
                 return rc; */
           } /* fi */
           else
           {
              dbg(TRACE,"REALLOCATED RESULT BUFFER TO %d BYTES",igActResultSize);
           }
        } /* end if */
        if (rc != RC_FAIL)
        {
           memcpy(pcgResultData+ilTotalResultBufLen,pclLine,ilLineLen+1);
           ilTotalResultBufLen+=ilLineLen;
        } /* fi */
     } /* end if */
     else if (rc==RC_FAIL)
     {
        GetOraErr(rc,pcgDataArea,pcgSqlBuf,"SqlHandleReq");
        /* dbg(TRACE,"SqlHandleReq: Oracle Error<%s>",pcgDataArea); */
        /*  check_ret(rc); Error text will be needed */
     }
     ilFkt = NEXT;
     if (rc == RC_SUCCESS)
     {
        rc = CheckTxTimeout(que_out); /* Check if timeout value already reached */
     }
  } /* while */
  CheckPerformance(FALSE);
  dbg(TRACE,"<%s> %d RECS FETCHED (%d BYTES) (%d SEC)",
      prlCmdblk->obj_name,ilCnt,ilTotalResultBufLen,tgStampDiff);
#ifdef CCSDB_INFORMIX
  commit_work();
#endif
  close_my_cursor(&ilLocalCursor);

  if (pcgResultData!=NULL)
  {
     if (rc==RC_FAIL)
     {
        strcpy(pcgResultData,pcgDataArea);
        prlBchd->rc = RC_FAIL; /* There should B enough space */
     }
     else if (strlen(pcgResultData)==0)
     { /* Error */
        strcpy(pcgResultData,"No Data Found");
        prlBchd->rc = RC_FAIL; /* There should B enough space */
     }
     else
     {
        pcgResultData [strlen(pcgResultData) - 2] = EOS;
        prlBchd->rc = RC_SUCCESS;
     }
     rc = new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,pcgResultData);
  }
  else
  {
     prlBchd->rc = RC_FAIL; /* There should B enough space */
     rc = new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,pcgDataArea);
  }

  return rc;
} /* end of SqlHandleReq */

/* ***************************************************
 * Function: 		SqlHandleInsert
 * Parameter: 	void
 * Return: 			RC_SUCCESS, RC_FAIL, SQL_NOTFOUND
 * Description: Inserts new records into DB. Generate on
 * request a new URNO (cmd=IBT).
 **************************************************** */
static int SqlHandleInsert(void)
{
  int rc = RC_SUCCESS; /* Return code  */
  BC_HEAD *prlBchd; /* Broadcast header */
  CMDBLK *prlCmdblk; /* Command Block */
  char *pclDatablk; /* Data Block */
  char *pclSelection;
  char *pclFields;
  char *pclTmpBuf;
  static char pclTmp[64];
  static char pclTmpUrno[16];
  static char pclRcvFields[4096];
  /*long ilNewUrno=0;*/ /* New Unique Record Number */
  unsigned long ullNewUrno = 0;
  short ilFldcnt, i;

  que_out = prgEvent->originator;
  dbg(DEBUG,"SqlHandleInsert:This Request is from (%d)",que_out);
  memset(pclTmp,'\0',64);
  /***************
   Get the structs
  ****************/
  prlBchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  prlCmdblk= (CMDBLK  *) ((char *)prlBchd->data);
  pclSelection = prlCmdblk->data;
  pclFields = (char *) pclSelection + strlen(pclSelection)+1;
  pclDatablk = (char *) pclFields + strlen(pclFields)+1;
  pcgDataArea[0]='\0';

  CheckMultiInput(prlCmdblk->command,prlCmdblk->obj_name,&pclSelection,
                  &pclFields,&pclDatablk,'I');

  if (strcmp(prlCmdblk->command,"IBT")==0)
  {
     dbg(DEBUG,"SqlHandleInsert: Reading the next URNO !!!");
#ifdef CCSDB_INFORMIX
     rc = GetUrno(&ilNewUrno);
     if (rc!=RC_SUCCESS)
     {
        dbg(TRACE,"SqlHandleInsert: GetUrno failed %d",rc);
        rc=RC_FAIL;
     }
     else
     {
        dbg(DEBUG,"HandleInsert: Urno(INFORMIX) %d",ilNewUrno);
     }
#else
     if (igCurTabIdx < 0)
     {
       GetTabDescrIdx("GMU",prlCmdblk->obj_name);
     }
     if (igCurTabIdx >= 0)
     {
       dbg(TRACE,"IBT REQUEST: URNO POOL <%s><%s> COUNT=%d",
                  prlCmdblk->obj_name,prgTabDescr[igCurTabIdx].UrnoRangeKey,1);
       rc = GetNextNumbers(prgTabDescr[igCurTabIdx].UrnoRangeKey,pcgDataArea,1,"GNV");
     }
     else
     {
       rc = GetNextValues(pcgDataArea,1); /* Comment this function */
     }
     if (rc!=RC_FAIL)
     {
        /*ilNewUrno=atoi(pcgDataArea);*/
        ullNewUrno = strtoul (pcgDataArea,NULL,0);
        memmove(pclSelection+strlen(pcgDataArea)+1,
                pclFields,strlen(pclFields)+strlen(pclDatablk)+2);
        strcpy(pclSelection,pcgDataArea);
        pclFields=(char *)pclSelection+strlen(pcgDataArea)+1;
        pclDatablk=(char *)pclFields+strlen(pclFields)+1;
        sprintf(pcgDataArea,"%lu,%s",ullNewUrno,pclDatablk);
        memmove(pclFields+strlen("URNO,"),pclFields,strlen(pclFields)+1);
        strncpy(pclFields,"URNO,",5);
        pclDatablk=(char *)pclFields+strlen(pclFields)+1;
        strcpy(pclDatablk,pcgDataArea);
        dbg(TRACE,"pclSelection<%s> pclFields<%s> pclDatablk<%s>",
            pclSelection,pclFields,pclDatablk);
        rc=RC_SUCCESS;
     }
#endif
     dbg(DEBUG,"Called using <IBT>");
  } /* end if */

  ilFldcnt = field_count(pclFields);
  dbg(TRACE,"SqlHandleInsert: Fieldcount is <%d>,pclFields <%s>",ilFldcnt,pclFields);
  pclTmpBuf = (char *) calloc (ilFldcnt+7, 10);
  if (pclTmpBuf==NULL)
  {
     dbg(TRACE,"SqlHandleInsert: Malloc error (pclTmpBuf)");
     rc=RC_FAIL;
  }
  else
  {
     dbg(TRACE,"SqlHandleInsert: Recieved CMD <%s> Fldcnt %d",prlCmdblk->command,ilFldcnt);
     if (rc!=RC_FAIL)
     {
        strcpy(pclRcvFields,pclFields);
        strcpy(pcgDataArea,pclDatablk);
        if (((igAddHopo == TRUE) && (igModifyAllowed==1)) || (igUseHopo == 0))
        {
           /* QuickHack */
           CompareHopoInList(pclRcvFields, pcgDataArea);
           RemoveHopoFromList(pclRcvFields, pcgDataArea);
        } /* end if */

        ilFldcnt = field_count(pclRcvFields);
        for (i=0; i<ilFldcnt ; i++)
        {
           sprintf(pclTmp,":Val%d",i);
           strcat(pclTmpBuf,pclTmp);
           if (i<(ilFldcnt-1))
           {
              strcat(pclTmpBuf,",");
           } /* end if */
        } /* end for */

        if ((igUseHopo == TRUE) && (igModifyAllowed==1) &&
            (strcmp(pcgDefTblExt,"TAB") == 0) &&
            (strstr(pclRcvFields,"HOPO") == NULL))
        {
           strcat(pclRcvFields,",HOPO");
           strcat(pclTmpBuf,",:VHOPO");
           strcat(pcgDataArea,",");
           strcat(pcgDataArea,pcgH3LC);
           dbg(TRACE,"SqlHandleInsert - :  Inside HOPO ");
        } /* end if */

/*
        if (strcmp(prlCmdblk->command,"IRT")==0)
        {
           ilNewUrno=ExtractUrno(pcgDataArea,pclFields);
        }
*/
        strcpy(pcgAllOutFld,pclRcvFields);
        if (igCurTabIdx >= 0)
        {
           rc = CheckURNO('I',prgTabDescr[igCurTabIdx].cgUrnoType,
                          pclRcvFields,pclTmpBuf,pcgDataArea);
           rc = CheckHOPO('I',prgTabDescr[igCurTabIdx].cgHopoType,
                          pclRcvFields,pclTmpBuf,pcgDataArea);
           rc = CheckCDAT('I',prgTabDescr[igCurTabIdx].cgCdatType,
                          pclRcvFields,pclTmpBuf,pcgDataArea);
           rc = CheckUSEC('I',prgTabDescr[igCurTabIdx].cgUsecType,
                          pclRcvFields,pclTmpBuf,pcgDataArea);
           rc = CheckLSTU('I',prgTabDescr[igCurTabIdx].cgLstuType,
                          pclRcvFields,pclTmpBuf,pcgDataArea);
           rc = CheckUSEU('I',prgTabDescr[igCurTabIdx].cgUseuType,
                          pclRcvFields,pclTmpBuf,pcgDataArea);
        }
        ullNewUrno=ExtractUrno(pcgDataArea,pclRcvFields);
        dbg(TRACE,"SqlHandleInsert 1 :  ullNewUrno\n<%lu>",ullNewUrno);
        strcpy(prlCmdblk->command,"IRT");
        sprintf(pclTmpUrno,"%lu",ullNewUrno);
        sprintf(pcgSqlBuf,"INSERT INTO %s (%s) VALUES (%s)",
                prlCmdblk->obj_name,pclRcvFields, pclTmpBuf);
        dbg(TRACE,"SqlHandleInsert 2 :  SQLBUF\n <%s>, <%s>,<%s>",pcgSqlBuf,pcgDataArea,pclTmpUrno);
        rc = dam_sql_and_send(pcgSqlBuf,pcgDataArea,pclRcvFields,prlBchd,pclTmpUrno,'I',TRUE,TRUE);

        if (pclTmpBuf!=NULL)
           free(pclTmpBuf);
     }
  } /* end if */
#ifdef UFIS43
  /* inform action */
  if (prlBchd->rc==RC_SUCCESS)
  {
     if (ullNewUrno>0)
     {
        sprintf(pclTmpUrno,"%lu",ullNewUrno);
        (void)ReleaseActionInfo(pcgOutRoute,prlCmdblk->obj_name,"IRT",
                                pclTmpUrno,pclTmpUrno,pclRcvFields,
                                pcgDataArea,"\0");
     }
  }
#endif /* end #ifdef UFIS43 */

  return rc;
} /* end of SqlHandleInsert */

/* ***************************************************
 * Function: 		GetNextUrno
 * Parameter: 	void
 * Return: 			RC_SUCCESS, RC_FAIL, SQL_NOTFOUND
 * Description: Fetches next URNO using Lib functions.
 **************************************************** */
static int GetNextUrno()
{
  int rc = RC_SUCCESS;	/* Return code  */
  BC_HEAD *prlBchd;	/* Broadcast header	*/
  CMDBLK *prlCmdblk;		/* Command Block 	*/
  char *pclSelection;
  char *pclFields;

  que_out = prgEvent->originator;
  dbg(DEBUG,"GetNextUrno: This Request is from (%d)",que_out);

  /****************
    Get the structs
  *****************/
  prlBchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  prlCmdblk= (CMDBLK  *) ((char *)prlBchd->data);
  pclSelection = prlCmdblk->data;
  pclFields = (char *)pclSelection + strlen(pclSelection)+1;

  rc = GetNextValues(pcgDataArea,1);
  if (rc==RC_FAIL)
  {
     strcpy(pcgDataArea,"GetNextUrno: Unable to generate URNO");
     dbg(TRACE,"GetNextUrno: Unable to generate URNO");
  }
  else
  {
    dbg(DEBUG,"GetNextUrno: Urno<%s>",pcgDataArea);
    rc=RC_SUCCESS;
  }
  prlBchd->rc=rc;
  rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,pcgDataArea);
  dbg(DEBUG,"GetNextUrno: returning code<%d>",rc);

  return rc;
} /* end of GetNextUrno */

/* ***************************************************
 * Function: 		GetManyUrno
 * Parameter: 	void
 * Return: 			RC_SUCCESS, RC_FAIL, SQL_NOTFOUND
 * Description: Fetches next URNO using Lib functions.
 **************************************************** */
static int GetManyUrno()
{
  int rc = RC_SUCCESS;	/* Return code  */
  int ilGetRc = RC_SUCCESS;
  char pclItem[32] = "";
  char pclData[32] = "";
  char pclKeys[32] = "";
  BC_HEAD *prlBchd=NULL;	/* Broadcast header	*/
  CMDBLK *prlCmdblk=NULL;	/* Command Block 	*/
  char *pclDatablk=NULL;	/* Data Block	*/
  char *pclSelection=NULL;
  char *pclFields=NULL;
  char *pclPtr=NULL;
  int i = 0;
  /*int ilNewUrno=0;*/
  unsigned long ullNewUrno = 0;
  int ilNoOfUrnos=0;
  int ilLen = 0; /* Length of Urno string */
  int ilOff = 0;
  int ilItemLen = 0;
  int ilDataLen = 0;
  char pclTmpSelection[10] = "";
  BOOL blitemFound = FALSE;



  que_out = prgEvent->originator;
  dbg(DEBUG,"GetManyUrno: This Request is from (%d)",que_out);
  prlBchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  prlCmdblk= (CMDBLK  *) ((char *)prlBchd->data);
  pclSelection = prlCmdblk->data;
  pclFields = (char *)pclSelection + strlen(pclSelection)+1;
  pclDatablk  = pclFields + strlen(pclFields)+1;

   /* Hack for the ATH URNO Issue for the CDID , LoaTabViewer and ATC Pallas */
   if (strcmp(pcgH3LC, "ATH")==0) {
       dbg(TRACE,"GetManyUrno :  Before pcgH3LC <%s>, pclSelection <%s>, pcgRecvTablename <%s> ,pcgClientAppl<%s>  ",pcgH3LC,pclSelection , pcgRecvTablename, pcgClientAppl );
      if (strcmp(pcgClientAppl, "CDID")==0)  {
         strcpy(pclTmpSelection,"TLXTAB");
        dbg(TRACE,"GetManyUrno : 1 pclSelection <%s> ",pclTmpSelection);
      }

      if (strcmp(pcgClientAppl, "LoadTabVie")==0 || strcmp(pcgRecvTablename,"TLXTAB")==0) {
        strcpy(pclTmpSelection,"TLXTAB");
        dbg(TRACE,"GetManyUrno : 2 pclSelection <%s> ",pclTmpSelection);
      }

      if (strcmp(pcgClientAppl, "CuteIF")==0 || strcmp(pcgRecvTablename,"FLGTAB")==0
         || strcmp(pcgRecvTablename,"FXTTAB")==0 || strcmp(pcgRecvTablename,"FLZTAB")==0
         || strcmp(pcgRecvTablename,"FLDTAB")==0
         )
      {

        strcpy(pclTmpSelection,"FLZTAB");
        dbg(TRACE,"GetManyUrno : 3 pclSelection <%s> ",pclTmpSelection);
      }
      dbg(TRACE,"GetManyUrno : final pcgH3LC <%s>, pclSelection <%s>, pcgRecvTablename <%s> ,pcgClientAppl<%s>  ",pcgH3LC,pclTmpSelection , pcgRecvTablename, pcgClientAppl );
  }



  ilLen = strlen(pclSelection);
  if (ilLen > 0 || strlen(pclTmpSelection)>0 )
  {
    /* New Style of multiple GMU Request */
    dbg(TRACE,"NEW STYLE OF GMU COMMAND");
    pcgDataArea[0]='\0';
    i = 0;
    ilLen = 99;
    dbg(TRACE,"GMU REQUEST: URNO POOL ilLen <%d> blitemFound=%d",ilLen,blitemFound);
    while (ilLen > 0 && blitemFound==FALSE)
    {
      i++;
      ilDataLen = get_real_item(pclData,pclDatablk,i);
      ilItemLen = get_real_item(pclItem,pclSelection,i);

      /* to avoid the Loop in case of more than one Urnos are requested*/
      if (ilItemLen <= 0 && strlen(pclTmpSelection) >0 ) {
          strcpy(pclItem,pclTmpSelection);
          blitemFound = TRUE;
          ilItemLen = strlen(pclTmpSelection);
          dbg(TRACE,"GMU REQUEST: URNO POOL ilItemLen <%d> blitemFound=%d",ilItemLen,blitemFound);
      }

      ilLen = ilItemLen;
      dbg(TRACE,"GMU REQUEST: URNO POOL <%s> COUNT=%d",pclItem,ilLen);
      if (ilLen > 0)
      {
        GetTabDescrIdx("GMU",pclItem);
        if (igCurTabIdx >= 0)
        {
          strcpy(pclKeys,prgTabDescr[igCurTabIdx].UrnoRangeKey);
          if (ilDataLen > 0)
          {
            ilNoOfUrnos = atoi(pclData);
          }
          dbg(TRACE,"GMU REQUEST: URNO POOL <%s><%s> COUNT=%d",pclItem,pclKeys,ilNoOfUrnos);


/*        ilGetRc = GetNextNumbers(pclKeys,pcgDataArea,ilNoOfUrnos,"GNV");
          strncpy(pcgTmpBuf2, "00000000000000000000", URNO_SIZE);
          ilLen = strlen(pcgTmpBuf1);
          ilOff = URNO_SIZE - ilLen;
          if (ilOff < 0)
          {
            ilOff = 0;
          }
          strcpy(&(pcgTmpBuf2[ilOff]),pcgTmpBuf1);
          sprintf(pcgTmpBuf3,"%s,%s,%s,%d\n",pclItem,pclKeys,pcgTmpBuf2,ilNoOfUrnos);
          dbg(TRACE,"GOT RANGE <%s>",pcgTmpBuf3);
          strcat(pcgDataArea,pcgTmpBuf3);
*/

          /* UFIS-1485 */
          ilGetRc = GetNextNumbers(pclKeys,pcgDataArea,ilNoOfUrnos,"GNV");
          /*ilNewUrno=atoi(pcgDataArea);*/
          ullNewUrno = strtoul (pcgDataArea,NULL,0);


          ilLen=0;
          pcgDataArea[0]='\0';
          for (i=0;i<ilNoOfUrnos;i++)
          {
             if (i<ilNoOfUrnos-1)
             {
                sprintf(pcgDataArea+ilLen,"%lu,",ullNewUrno++);
                ilLen+=strlen(pcgDataArea+ilLen);
             }
             else
                sprintf(pcgDataArea+ilLen,"%lu",ullNewUrno);
          }

        }
        else
        {
          dbg(TRACE,"SOMETHING WENT WRONG WITH KEY CODE <%s>",pclItem);
        }
      }
    }
  }
  else
  {
    /* Old Style of single GMU Request */
    dbg(TRACE,"OLD STYLE OF GMU COMMAND");

    ilNoOfUrnos=atoi(pclDatablk);

    if (ilNoOfUrnos<=0 || ilNoOfUrnos > 500)
    {
       dbg(TRACE,"GetManyUrno:Request for %d Urnos(<= 500) failed",ilNoOfUrnos);
       strcpy(pcgDataArea,"Too many URNOs requested.");
       rc=RC_FAIL;
    }
    else
    {
       /****************
         Get the structs
       *****************/
#ifdef CCSDB_INFORMIX
       pcgDataArea[0]='\0';
       ilLen=0;
       for (i=0;i<ilNoOfUrnos;i++)
       {
          rc=GetUrno(&ilNewUrno);
          if (rc==RC_SUCCESS)
          {
             if (i<ilNoOfUrnos-1)
                sprintf(pcgDataArea+ilLen,"%d,",ilNewUrno);
             else
                sprintf(pcgDataArea+ilLen,"%d",ilNewUrno);
             ilLen=strlen(pcgDataArea);
          }
          else
          {
             strcpy(pcgDataArea,"Unable to generate URNO");
             break;
          }
       }
       dbg(TRACE,"GetManyUrno: DB variable must be set to Oracle.");
#else
       dbg(DEBUG,"GetManyUrno: Processing request for %d Urnos",ilNoOfUrnos);
       rc = GetNextValues(pcgDataArea,ilNoOfUrnos);
       dbg(DEBUG,"GetManyUrno: rc<%i>, Datablk <%s>",rc,pcgDataArea);
       if (rc==RC_FAIL)
       {
          strcpy(pcgDataArea,"Unable to generate URNO");
          dbg(TRACE,"GetManyUrno: Unable to generate URNO");
       }
       else
       {
          /*ilNewUrno=atoi(pcgDataArea);*/
          ullNewUrno = strtoul (pcgDataArea,NULL,0);
          ilLen=0;
          pcgDataArea[0]='\0';
          for (i=0;i<ilNoOfUrnos;i++)
          {
             if (i<ilNoOfUrnos-1)
             {
                sprintf(pcgDataArea+ilLen,"%lu,",ullNewUrno++);
                ilLen+=strlen(pcgDataArea+ilLen);
             }
             else
                sprintf(pcgDataArea+ilLen,"%lu",ullNewUrno);
          }
       }
       rc=RC_SUCCESS;
     }
#endif
  } /* end if */
  prlBchd->rc=rc;
  dbg(TRACE,"GetManyUrno: Sending Result \n%s",pcgDataArea);
  rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,pcgDataArea);

  return rc;
} /* end of GetManyUrno */

/* ***************************************************
 * Function: 		GetManyNumbers
 * Parameter: 	void
 * Return: 			RC_SUCCESS, RC_FAIL, SQL_NOTFOUND
 * Description: Fetches next URNO using Lib functions.
 **************************************************** */
static int GetManyNumbers()
{
  int rc = RC_SUCCESS;	/* Return code  */
  BC_HEAD *prlBchd;	/* Broadcast header	*/
  CMDBLK *prlCmdblk;	/* Command Block 	*/
  char *pclDatablk;	/* Data Block	*/
  char *pclFlag,*pclSelection;
  char *pclFields,pclTmp[20];
  int ilNoOfUrnos=0,ilNoOfParams=0;

  que_out = prgEvent->originator;
  dbg(DEBUG,"GetManyNumber: This Request is from (%d)",que_out);
  prlBchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  prlCmdblk= (CMDBLK  *) ((char *)prlBchd->data);
  pclSelection = prlCmdblk->data;
  pclFields = (char *)pclSelection + strlen(pclSelection)+1;
  pclDatablk  = pclFields + strlen(pclFields)+1;

  dbg(DEBUG,"GetManyNumber: Data list<%s>",pclDatablk);
  strcpy(pcgDataArea,pclDatablk);
  ilNoOfParams=field_count(pcgDataArea);
  dbg(DEBUG,"GetManyNumber: No. of Params<%d>",ilNoOfParams);
  if (ilNoOfParams>3)
  {
     dbg(TRACE,"GetManyNumbers: Too many parameters defined!!");
     prlBchd->rc=RC_FAIL;
     rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,"");
     return rc;
  }
  delton(pcgDataArea);
  ilNoOfUrnos=atoi(pcgDataArea);
  sprintf(pclTmp,"%s",pcgDataArea+strlen(pcgDataArea)+1);
  dbg(DEBUG,"GetManyNumbers: Key<%s>",pclTmp);
  pclFlag=strrchr(pclDatablk,',');
  pclFlag++;
  dbg(DEBUG,"GetManyNumber: Flag<%s> Key<%s> No. of Urnos<%d>",pclFlag,pclTmp,ilNoOfUrnos);
  /****************
    Get the structs
  *****************/
#ifdef UFIS43
  dbg(DEBUG,"GetManyNumbers: Processing request for %d Urnos",ilNoOfUrnos);
  rc = GetNextNumbers(pclTmp,pcgDataArea,ilNoOfUrnos,pclFlag);
  dbg(DEBUG,"GetManyNumbers: rc<%i>",rc);
  if (rc!=RC_SUCCESS)
  {
     strcpy(pcgDataArea,"Unable to generate URNO");
     dbg(TRACE,"GetManyUrno: Unable to generate URNO. Request from<%d>",que_out);
     rc=RC_FAIL;
  }
  else
  {
     dbg(DEBUG,"GetManyNumbers: rc<%i>, Datablk <%s>",rc,pcgDataArea);
  }
#endif
  prlBchd->rc=rc;
  dbg(DEBUG,"GetManyNumbers: Sending result<%s> to sender",pcgDataArea);
  rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,pcgDataArea);
  dbg(DEBUG,"GetManyNumbers: returning code<%d>",rc);

  return rc;
} /* end of GetManyNumbers */

/* ***************************************************
 * Function: 		SqlHandleUpdate
 * Parameter: 	void
 * Return: 			RC_SUCCESS, RC_FAIL, SQL_NOTFOUND
 * Description: Edits records in DB.
 **************************************************** */
static int SqlHandleUpdate(void)
{
  int rc = RC_SUCCESS;	/* Return code 	*/
  int rc2 = RC_FAIL;	/* Return code 	*/
  BC_HEAD *prlBchd;		/* Broadcast header	*/
  CMDBLK *prlCmdblk;		/* Command Block 	*/
  char *pclDatablk;		/* Data Block	*/
  char *pclSelection;
  char *pclFields;
  char *pclTmpBuf;
  char pclTmp[64];
  char pclTmpUrno1[32];
  char pclTmpUrno2[32];
  int ilLineLen;
  int ilUrnoPos= 0;
  /*int ilNewUrno;*/
  unsigned long ullNewUrno = 0;
  short	ilFkt,ilFldcnt,i,ilLocalCursor;
  char clUrnoQuote= ' ';
  char pclSqlBuf2[sizeof(pcgSqlBuf)];
  int ilFieldCount;

  que_out = prgEvent->originator;
  dbg(DEBUG,"SqlHandleUpdate: This Request is from (%d)",que_out);
  /************
  Get the structs
  ************/
  prlBchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  prlCmdblk= (CMDBLK  *) ((char *)prlBchd->data);
  pclSelection = prlCmdblk->data;
  pclFields = (char *) pclSelection + strlen(pclSelection)+1;
  pclDatablk = pclFields + strlen(pclFields)+1;
  ilFldcnt = field_count(pclFields);
  pclDatablk = pclFields + strlen(pclFields)+1;

  CheckMultiInput(prlCmdblk->command,prlCmdblk->obj_name,&pclSelection,
                  &pclFields,&pclDatablk,'U');

  clUrnoQuote= ' '; /* default */
  if (CheckUrnoType(prlCmdblk->obj_name)==0)
  {
     clUrnoQuote= '\'';
  }

  if (((igAddHopo == TRUE) && (igModifyAllowed == 1)) || (igUseHopo == 0))
  {
    /* QuickHack */
    CompareHopoInList(pclFields, pclDatablk);
    RemoveHopoFromList(pclFields, pclDatablk);
  } /* end if */
  ilFldcnt = field_count(pclFields);
  CompareHopoInWhereClause(pclSelection);
  if (igUseHopo == 0)
  {
     RemoveHopoInWhereClause(pclSelection);
  }

  /* In order to send URNO info via WriteActionLog to the LOGHDL */
  /* affected URNOs must first be read out */
  ilLineLen=0;
  dbg(DEBUG,"SqlHandleUpdate: dest_name<%s>",prlBchd->dest_name);
  dbg(DEBUG,"SqlHandleUpdate: Fields<%s>",pclFields);

  OutFieldsToSelFields(prlCmdblk->obj_name, pclFields);
  ilUrnoPos= get_item_no(pcgAllOutFld,"URNO",5) + 1;
  ilFieldCount = field_count(pcgAllOutFld);

  /* Get field count per record */
  ilFldcnt = build_sql_buf(pclSqlBuf2,pcgAllOutFld,prlCmdblk->obj_name,pclSelection);
  dbg(DEBUG,"SqlHandleUpdate: pclSqlBuf2<%s>",pclSqlBuf2);
  ilLocalCursor = 0;
  ilFkt = START;
  rc = RC_SUCCESS;
  pclTmpUrno2[0]= 0;
  CheckPerformance(TRUE);
  while (rc == RC_SUCCESS)
  {
     memset(pcgOldData,0x00,DATABLK_SIZE);
     rc = sql_if(ilFkt,&ilLocalCursor,pclSqlBuf2,pcgOldData);
     if (rc==RC_SUCCESS)
     {
        (void) BuildItemBuffer(pcgOldData,"",ilFieldCount,",");
        if (strlen(pcgOldData)==0)
        { /* Error */
           prlBchd->rc=RC_FAIL;
           dbg(TRACE,"SqlHandleUpdate: No Data Found");
           rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,"No Data Found");
           dbg(DEBUG,"SqlHandleUpdate: returning from Update");
           rc=RC_FAIL;
        }
        else
        {
           /* 11/11/2001: ilLineLen = get_real_item(pclTmpUrno,pcgOldData,1); wg '...' */
           if (ilUrnoPos > 0)
           {
              ilLineLen = get_real_item(pclTmpUrno1,pcgOldData,ilUrnoPos) + 2;
              sprintf(pclTmpUrno2,"%c%s%c",clUrnoQuote,pclTmpUrno1,clUrnoQuote);
           }
           else
           {
              strcpy(pclTmpUrno2,"");
           }
           dbg(DEBUG,"SqlHandleUpdate:Total len of data=%d, rc<%d>",strlen(pcgOldData),rc);
           dbg(DEBUG,"URNO of record is/are <%s>",pclTmpUrno2);

           /* jim: 20011210: begin update record by record: */
           pcgDataArea[0]='\0';
           ilFldcnt=field_count(pclFields);
           dbg(DEBUG,"SqlHandleUpdate: ilFldcnt<%d> pclFields<%s>",ilFldcnt,pclFields);
           for (i=0; i <ilFldcnt ; i++)
           {
              sprintf(pclTmp,"%s=:Val%hi",sGet_item(pclFields,(short)(i+1)),i);
              strcat(pcgDataArea,pclTmp);
              if (i<(ilFldcnt-1))
              {
                 strcat(pcgDataArea,",");
              } /* end if */
           } /* end for */

           /* 11/11/2001: */
           if (strlen(pclTmpUrno2) == 0)
           { /* no urno found, use original selection */
              strcpy(pcgRcvSelKey,pclSelection);
              CheckWhereClause(TRUE, pcgRcvSelKey, FALSE, TRUE, "\0");
           }
           else
           { /* urno found, use only found urnos in selection */
              sprintf(pcgRcvSelKey,"WHERE URNO = %s",pclTmpUrno2);
              dbg(DEBUG,"urno found, selection modified to: <%s>,",pcgRcvSelKey);
           }

           strcpy(pcgFldLst,pclFields);
           strcpy(pcgDatLst,pclDatablk);
           if (igCurTabIdx >= 0)
           {
              rc = CheckURNO('U',prgTabDescr[igCurTabIdx].cgUrnoType,
                             pcgFldLst,pcgDataArea,pcgDatLst);
              rc = CheckHOPO('U',prgTabDescr[igCurTabIdx].cgHopoType,
                             pcgFldLst,pcgDataArea,pcgDatLst);
              rc = CheckCDAT('U',prgTabDescr[igCurTabIdx].cgCdatType,
                             pcgFldLst,pcgDataArea,pcgDatLst);
              rc = CheckUSEC('U',prgTabDescr[igCurTabIdx].cgUsecType,
                             pcgFldLst,pcgDataArea,pcgDatLst);
              rc = CheckLSTU('U',prgTabDescr[igCurTabIdx].cgLstuType,
                             pcgFldLst,pcgDataArea,pcgDatLst);
              rc = CheckUSEU('U',prgTabDescr[igCurTabIdx].cgUseuType,
                             pcgFldLst,pcgDataArea,pcgDatLst);
           }
           sprintf(pcgSqlBuf,"UPDATE %s SET %s %s ",prlCmdblk->obj_name,
                   pcgDataArea,pcgRcvSelKey);
           dbg(TRACE,"SqlHandleUpdate: SQLBUF\n<%s>",pcgSqlBuf);
           dbg(DEBUG,"SqlHandleUpdate: recv_name %s",prlBchd->recv_name);
           BuildNewData(pcgFldLst,pcgDatLst);
           sprintf(pcgNewAndOldData,"%s\n%s",pcgNewData,pcgOldData);
           trim_quotes(pclTmpUrno2,clUrnoQuote); /*fuer syslibUpdateDb und ReleaseActionInfo*/
           rc = dam_sql_and_send(pcgSqlBuf,pcgDatLst,pcgFldLst,prlBchd, pclTmpUrno2, 'U',TRUE,TRUE);
           rc2 = rc; /* MCU 20020506: using rc2 as return value, leaving rc unchanged because it is used in the loop condition "while(rc == RC_SUCESS) */
           dbg(DEBUG,"SqlHandleUpdate:  returned from dam_sql_and_send");
#ifdef UFIS43
           if (rc==RC_SUCCESS)
           {
              (void)ReleaseActionInfo(pcgOutRoute,prlCmdblk->obj_name,"URT",
                                      pclTmpUrno2,pclSelection,pcgAllOutFld,
                                      pcgNewData, pcgOldData);
           }
#endif /* end #ifdef UFIS43 */
           /* jim: 20011210: end   update record by record: */
        }
     }  /* end if rc==RC_SUCCESS */
     else if (rc==SQL_NOTFOUND)
     {
        if (pclTmpUrno2[0]== 0)
        { /* no data found in first run */
          prlBchd->rc=RC_FAIL;
          dbg(TRACE,"SqlHandleUpdate: No Data Found");
          (void) new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,"No Data Found");
          dbg(DEBUG,"SqlHandleUpdate: returning from Update");
          rc=RC_FAIL;
        }
        else
        {
           /**rc = RC_SUCCESS; MCU 20020308 **/ /* at least one record processed */
           /*rc = RC_SUCCESS;  JIM 20020422: at least one record processed (pclTmpUrno2 initialized) */
           /*** MCU 20020506: if rc is set to SUCCESS the while loop is not breaked */
        }
     } /* end if SQL_NOTFOUND */
     else if(rc==RC_FAIL)
     {
        strcpy(pcgOldData,"\0");
        /* check_ret(rc); Error text will be needed */
        GetOraErr(rc,pcgErrMsg,pclSqlBuf2,"SqlHandleUpdate");
        /* dbg(TRACE,"SqlHandleUpdate: Oracle Error<%s>",pcgErrMsg); */
     } /* end if sql_if */
     ilFkt = NEXT;
  } /* while */
  CheckPerformance(FALSE);
  close_my_cursor(&ilLocalCursor);

  return rc2;
} /* end of SqlHandleUpdate */

/* ***************************************************
 * Function: 		SqlHandleDelete
 * Parameter: 	void
 * Return: 			RC_SUCCESS, RC_FAIL, SQL_NOTFOUND
 * Description: Removes records from DB Table.
 **************************************************** */
static int SqlHandleDelete (void)
{
  int rc = RC_SUCCESS;	/* loop variables and return code 	*/
  BC_HEAD *prlBchd;		/* Broadcast header	*/
  CMDBLK *prlCmdblk;		/* Command Block 	*/
  char *pclDatablk;		/* Data Block	*/
  char *pclSelection;
  char *pclFields;
  char pclTmpUrno[16];
  int ilLineLen,ilTotalResultBufLen;
  short ilFkt,ilLocalCursor;
  char clUrnoQuote= ' ';
  int ilCount;
  char pclSqlBuf2[sizeof(pcgSqlBuf)];
	BOOL blLoopedOnce = FALSE;
	int ilTotalCount = 0;

  int ilItemNo;
  int ilFldCnt;

  que_out = prgEvent->originator;
  dbg(DEBUG,"SqlHandleDelete: This Request is from (%d)",que_out);
  /* Get the structs */
  prlBchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  prlCmdblk= (CMDBLK  *) ((char *)prlBchd->data);
  pclSelection = prlCmdblk->data;
  pclFields = (char *) pclSelection + strlen(pclSelection)+1;
  pclDatablk = pclFields + strlen(pclFields)+1;

  CheckMultiInput(prlCmdblk->command,prlCmdblk->obj_name,&pclSelection,
                  &pclFields,&pclDatablk,'D');

  clUrnoQuote= ' '; /* default */
  if (CheckUrnoType(prlCmdblk->obj_name)==0)
  {
     clUrnoQuote= '\'';
  }
  strcpy(pcgRcvSelKey,pclSelection);
  CompareHopoInWhereClause(pclSelection);
  CheckWhereClause(TRUE, pcgRcvSelKey, FALSE, TRUE, "\0");
  if (igUseHopo == 0)
  {
     RemoveHopoInWhereClause(pcgRcvSelKey);
  }
  pclFields = (char *)pclSelection + strlen(pclSelection)+1;
  pclDatablk = pclFields + strlen(pclFields)+1;

  if (prgTabDescr[igCurTabIdx].cgUrnoType == 'M')
  {  /* Table has no URNO */
     sprintf(pcgSqlBuf,"DELETE FROM %s %s",prlCmdblk->obj_name,pclSelection);
     dbg(TRACE,"SqlHandleDelete: pcgSqlBuf = <%s>",pcgSqlBuf);
     ilLocalCursor = 0;
     ilFkt = START;
     rc = sql_if(ilFkt,&ilLocalCursor,pcgSqlBuf,pcgDataArea);
     if (rc == DB_SUCCESS)
     {
        prlBchd->rc=RC_SUCCESS;
        rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,"");
     }
     else
     {
        GetOraErr(rc,pcgErrMsg,pcgSqlBuf,"SqlHandleDelete");
        prlBchd->rc=RC_FAIL;
        rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,pcgErrMsg);
     }
     commit_work();
     close_my_cursor(&ilLocalCursor);
     return RC_SUCCESS;
  }

  /* In order to send URNO info via WriteLog to the LOGHDL */
  /* affected URNOs must first be read out */
  ilTotalResultBufLen=0;
  ilLineLen=0;
  dbg(DEBUG,"SqlHandleDelete: dest_name<%s>",prlBchd->dest_name);
  dbg(DEBUG,"SqlHandleDelete: Fields<%s>",pclFields);
  /* Get field count per record */
  /* build_sql_buf(pcgSqlBuf,"URNO",prlCmdblk->obj_name,pclSelection);*/

  /* UFIS-881 / UFIS-882
     * Changes In order to send the current Values to Action  before the Deletion
     * Have not been tested with Multiple deletions
     */
   dbg(TRACE,"SqlHandleDelete: pcgRcvSelKey<%s>",pcgRcvSelKey);
    if (igMultiInsert == TRUE || strlen(pclFields) < 2)  {
        build_sql_buf(pcgSqlBuf, "URNO", prlCmdblk->obj_name, pcgRcvSelKey);
    } else {
        build_sql_buf(pcgSqlBuf, pclFields, prlCmdblk->obj_name, pcgRcvSelKey);
    }
   dbg(TRACE,"SqlHandleDelete 2 : pcgRcvSelKey<%s>",pcgRcvSelKey);
  if (pcgResultData!=NULL)
     memset(pcgResultData,'\0',5);
  dbg(DEBUG,"SqlHandleDelete: pcgSqlBuf<%s>",pcgSqlBuf);
  ilLocalCursor = 0;
  ilFkt = START;
  rc = RC_SUCCESS;
  dbg(DEBUG,"SqlHandleDelete: sqlbuf<%s>",pcgSqlBuf);
  ilCount = 0;
  CheckPerformance(TRUE);
  pcgDataArea[0]='\0';
  while (rc == RC_SUCCESS)
  {

     dbg(DEBUG,"SqlHandleDelete: Accessing database");
     rc = sql_if(ilFkt,&ilLocalCursor,pcgSqlBuf,pcgDataArea);

     if (rc==RC_SUCCESS)
     {
              /* UFIS-881 / UFIS-882
               * Changes In order to send the current Values to Action  before the Deletion
               * Have not been tested with Multiple deletions
              */
             if (igMultiInsert == FALSE && strlen(pclFields) > 4) {
                ilFldCnt = field_count(pclFields);
                BuildItemBuffer(pcgDataArea, pclFields, ilFldCnt, ",");

                ilItemNo = get_item_no(pclFields, "URNO", 5) + 1;
                /*ilLineLen = get_real_item(pclTmpUrno, pcgDataArea, ilItemNo);*/
                ilLineLen = get_real_item(pclTmpUrno, pcgDataArea, ilItemNo) + 2;
                dbg(DEBUG,"urno found, pclTmpUrno 1 : <%s>,",pclTmpUrno);

            } else {
                /* 11/11/2001: ilLineLen = get_real_item(pclTmpUrno,pcgDataArea,1); wg '...' */
                ilLineLen = get_real_item(pclTmpUrno, pcgDataArea, 1) + 2;
                dbg(DEBUG,"urno found, pclTmpUrno 2 : <%s>,",pclTmpUrno);
            }






        if (ilCount >= 990)
        {
           if ((pcgResultData!=NULL) && (strlen(pcgResultData) > 0))
           {
              sprintf(pcgRcvSelKey,"WHERE URNO IN (%s)",pcgResultData);
              dbg(DEBUG,"urno found, selection modified to: <%s>,",pcgRcvSelKey);
              sprintf(pclSqlBuf2,"DELETE FROM  %s %s",prlCmdblk->obj_name,pcgRcvSelKey);
              dbg(TRACE,"SqlHandleDelete 3 : SQLBUF\n<%s>",pclSqlBuf2);
              trim_quotes(pcgResultData,clUrnoQuote); /* fuer syslibUpdateDb und ReleaseActionInfo */
							blLoopedOnce = TRUE;

              /* UFIS-881 / UFIS-882
               * Changes In order to send the current Values to Action  before the Deletion
               * Have not been tested with Multiple deletions
               */
             if ( igMultiInsert == TRUE || strlen(pclFields) < 2) {
                rc = dam_sql_and_send(pclSqlBuf2,pclDatablk,pclFields,prlBchd, pcgResultData, 'D',
                                    FALSE,FALSE);
             } else {
                rc = dam_sql_and_send(pclSqlBuf2,pcgDataArea,pclFields,prlBchd, pcgResultData, 'D',
                                    FALSE,FALSE);
             }
/* 03.09.2003 AKL
              if (rc==RC_SUCCESS)
              {
                 (void)ReleaseActionInfo(pcgOutRoute,prlCmdblk->obj_name,"DRT",
                                         pcgResultData,pclSelection,"\0","\0","\0");
              }
*/
              rc = RC_SUCCESS;
           }
           ilCount = 0;
           memset(pcgResultData,0x00,5);
           ilTotalResultBufLen = 0;
        }
        ilCount++;
        ilTotalCount++;
        if ((sizeof(EVENT)+sizeof(BC_HEAD)+ilTotalResultBufLen+ilLineLen)>igActResultSize)
        {
           /* Result data buffer full! Reallocate space */
           while ((sizeof(EVENT)+sizeof(BC_HEAD)+ilTotalResultBufLen+ilLineLen)>igActResultSize)
           {
              igActResultSize+=RESULTBUFSTEP;
           }
           if (pcgResultData==NULL)
           {
              dbg(DEBUG,"SqlHandleDelete: Allocing pcgResultData");
              pcgResultData = (char *)calloc(1,igActResultSize);
           }
           else
           {
              dbg(DEBUG,"SqlHandleDelete: Reallocing pcgResultData");
              pcgResultData=(char *)realloc(pcgResultData,igActResultSize);
           }
           if (pcgResultData==NULL)
           {
              igActResultSize=0;
              rc = RC_FAIL;
              dbg(TRACE,"SqlHandleDelete: Realloc pcgResultData(3) failed");
              if (ilLocalCursor!=0)
                 close_my_cursor(&ilLocalCursor);
              prlBchd->rc = RC_FAIL;
              igActResultSize=0;
              dbg(TRACE,"SqlHandleDelete: Malloc(pcgResultData) failed");
              rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,"");
              return rc;
           }
           else
           {
              dbg(DEBUG,"SqlHandleDelete: (Re)alloced pcgResultData<size %i> <rc %i>",
                  igActResultSize, rc);
           }
        } /* end if */
        if (rc!=RC_FAIL)
        {
           if (ilTotalResultBufLen==0)
              sprintf(pcgResultData,"%c%s%c",clUrnoQuote,pclTmpUrno,clUrnoQuote);
           else
           {
              sprintf(pcgResultData+ilTotalResultBufLen,",%c%s%c",
                      clUrnoQuote,pclTmpUrno,clUrnoQuote);
              ilTotalResultBufLen++;
           }
           ilTotalResultBufLen+=ilLineLen;
           dbg(TRACE,"URNO of record is <%s>",pcgResultData);
        } /* end if */
     }  /* end if rc==RC_SUCCESS */
     else if(rc==RC_FAIL)
     {
        /* check_ret(rc); Error text will be needed */
        GetOraErr(rc,pcgErrMsg,pcgSqlBuf,"SqlHandleDelete");
        /* dbg(TRACE,"SqlHandleDelete: Oracle Error<%s>",pcgErrMsg); */
     }
     ilFkt = NEXT;
  } /* while */
  CheckPerformance(FALSE);
  close_my_cursor(&ilLocalCursor);
   dbg(TRACE,"SqlHandleDelete: %d Records found",ilTotalCount);
  if (pcgResultData!=NULL)
  {
     if (rc==RC_FAIL)
     { /* Error */
        prlBchd->rc=RC_FAIL;
        /* dbg(TRACE,"SqlHandleDelete: Oracle Error <%s>. SqlBuf<%s>",pcgResultData,pcgSqlBuf); */
        rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,pcgErrMsg);
        return rc;
     }
     else if(strlen(pcgResultData)==0)
     {
        prlBchd->rc=RC_FAIL;
				if (blLoopedOnce == TRUE)
				{
        	rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,"OK");
        	dbg(TRACE,"SqlHandleDelete: 991 Records found");
				}
				else
				{
              /* UFIS-881 / UFIS-882
               * Changes In order to send the current Values to Action  before the Deletion
               * Have not been tested with Multiple deletions
               */
        if (igMultiInsert == TRUE || strlen(pclFields) < 2 ) {
            rc = new_tools_send_sql_perf(que_out, prlBchd, prlCmdblk, pclFields, "No Data Found");
            dbg(DEBUG, "SqlHandleDelete: No Data Found");
        } else {
            rc = new_tools_send_sql_perf(que_out, prlBchd, prlCmdblk, pclFields, pcgDataArea);
            dbg(DEBUG, "SqlHandleDelete: %s", pcgDataArea);
        }
				}
        return rc;
     }
     else
     {
        ;
     } /* end if */

     if ((pcgResultData!=NULL) && (strlen(pcgResultData) > 0))
     {
        sprintf(pcgRcvSelKey,"WHERE URNO IN (%s)",pcgResultData);
        dbg(DEBUG,"urno found, selection modified to: <%s>,",pcgRcvSelKey);
     }
     /* sprintf(pcgSqlBuf,"DELETE FROM  %s %s",prlCmdblk->obj_name,pclSelection);*/
     sprintf(pcgSqlBuf,"DELETE FROM  %s %s",prlCmdblk->obj_name,pcgRcvSelKey);
     dbg(TRACE,"SqlHandleDelete 4 : SQLBUF\n<%s>",pcgSqlBuf);

     trim_quotes(pcgResultData,clUrnoQuote); /* fuer syslibUpdateDb und ReleaseActionInfo */

     /* UFIS-881 / UFIS-882
               * Changes In order to send the current Values to Action  before the Deletion
               * Have not been tested with Multiple deletions
               */
      if (igMultiInsert == TRUE || strlen(pclFields) < 2) {
        rc = dam_sql_and_send(pcgSqlBuf,pclDatablk,pclFields,prlBchd, pcgResultData, 'D',TRUE,TRUE);
      } else {
        rc = dam_sql_and_send(pcgSqlBuf,pcgDataArea,pclFields,prlBchd, pcgResultData, 'D',TRUE,TRUE);
      }

     if (rc==RC_SUCCESS)
     {
        (void)ReleaseActionInfo(pcgOutRoute,prlCmdblk->obj_name,"DRT",
                                pcgResultData,pclSelection,"\0","\0","\0");
     }
  }
  else
  {
     prlBchd->rc=RC_FAIL;
     dbg(TRACE,"SqlHandleDelete: Alloc failure(pcgResultData)");
     rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,"");
  }

  return rc;
} /* end of SqlHandleDelete */

/* ***************************************************
 * Function: 	SqlHandleSys
 * Parameter: 	void
 * Return: 	RC_SUCCESS, RC_FAIL, SQL_NOTFOUND
 * Description: Removes records from DB Table.
 **************************************************** */
static int SqlHandleSys (void)
{
  int ilLen=0, rc = RC_SUCCESS;	/* Return code 	*/
  int i,ilTotalBufLen;
  BC_HEAD *prlBchd;		/* Broadcast header	*/
  CMDBLK *prlCmdblk;		/* Command Block 	*/
  char *pclSelection,pclPrjName[4];
  char *pclInFldLst; /* Contain list of selection */
  char *pclInFldVal; /* fields, values (max. 2) */
  char *pclFields, *pclPtr;
  char *pclFldTmp; /* OutFieldList storage pointer */
  int ilBoolFkt,ilZero=0;

#ifdef UFIS43
  que_out = prgEvent->originator;
  dbg(DEBUG,"SqlHandleSys: This Request is from (%d)",que_out);
  /* Get the structs */
  prlBchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  prlCmdblk= (CMDBLK  *) ((char *)prlBchd->data);
  pclSelection = prlCmdblk->data;
  pclFields = (char *)pclSelection + strlen(pclSelection)+1;

  strcpy(pclPrjName,&prlCmdblk->obj_name[3]);
  /* Allocate FldLst and FldVal */
  pclInFldLst=(char *)calloc(1,3*MAX_FIELD_LEN+1);
  if (pclInFldLst==NULL)
  {
     dbg(TRACE,"Alloc error pclInFldLst");
     ilBoolFkt=-2;
  } /* end if */
  else
  {
     pclInFldVal=(char *)calloc(1,3*MAX_FIELD_LEN+1);
     if (pclInFldVal==NULL)
     {
        dbg(TRACE,"Alloc error pclInFldVal");
        ilBoolFkt=-2;
     } /* end if */
     else
     {
        ilBoolFkt=ParseSel(pclSelection,pclInFldLst,pclInFldVal);
     } /* end else */
  } /* end else */
  switch(ilBoolFkt)
  {
     case -1:
        sprintf(pcgDataArea,"SqlHandleSys: Cannot parse<%s>",pclSelection);
        rc=RC_FAIL;
        break;
     case -2:
        strcpy(pcgDataArea,"SqlHandleSys:4. Not enough memory");
        dbg(TRACE,"SqlHandleSys: 4. Not enough memory.");
        rc=RC_FAIL;
        break;
     default:
        break;
  } /*end switch */
  /* **************************** */
  /* Allocate space for pclFldTmp */
  ilLen=strlen(pclFields);
  dbg(DEBUG,"HandleSys: ilLen=%d",ilLen);
  pclFldTmp=(char *) calloc(1,ilLen+1);
  if (pclFldTmp==NULL)
  {
     dbg(TRACE,"SqlHandleSys: Alloc error pclFldTmp");
     rc=RC_FAIL;
     strcpy(pcgDataArea,"SqlHandleSys: 5. Not enough memory!");
  } /* end if */
  if (rc!=RC_FAIL)
  {
     /********************** */
     /* Initialise variables */
     pcgDataArea[0]='\0';
     memcpy(pclFldTmp,pclFields,ilLen);
     pclPtr=&pcgDataArea[0];
     dbg(DEBUG,"SqlHandleSys: ilBoolFkt <%d>",ilBoolFkt);
     if (strncmp(prlCmdblk->obj_name,"SYS",3)==0)
     { /* SYSTAB Request */
        if (ilBoolFkt==0)
        {
           dbg(DEBUG,"Selection Fld:%s Val:%s",pclInFldLst,pclInFldVal);
           rc=syslibSearchSystabData(pclPrjName,pclInFldLst,pclInFldVal,
                                     pclFldTmp,pclPtr,&ilZero,"\r\n");
           dbg(DEBUG,"Returned from syslib fx. rc<%d>,ilZero %d, pclPtr %s",rc,ilZero,pclPtr);
        }
        else if (ilBoolFkt>0)
        {
           ilTotalBufLen=0;
           dbg(DEBUG,"Found %i Conditions",ilBoolFkt);
           for (i=0; i<ilBoolFkt && rc==RC_SUCCESS; i++)
           {
              pcgDataArea[0]='\0';
              pclPtr=&pcgDataArea[0];
              strcpy(pclFldTmp,pclFields);
              dbg(DEBUG,"SqlHandleSys: Selection Fld:%s Val:%s",pclInFldLst,pclInFldVal);
              rc=syslibSearchSystabData(pclPrjName,pclInFldLst,pclInFldVal,
                                        pclFldTmp,&pcgDataArea[0],&ilZero,"\r\n");
              if (rc==RC_SUCCESS)
              {
                 ilLen=strlen(pcgDataArea);
                 if (pcgResultData==NULL)
                 {
                    while ((ilTotalBufLen+ilLen)>=igActResultSize)
                       igActResultSize+=RESULTBUFSTEP;
                    pcgResultData = (char *) calloc(1,igActResultSize);
                    if (pcgResultData==NULL)
                    {
                       rc = RC_FAIL;
                       igActResultSize=0;
                       dbg(TRACE,"SqlHandleSys: Malloc 2 failed");
                       prlBchd->rc = RC_FAIL;
                       rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,"");
                       return rc;
                    }
                    else
                    {
                       pcgResultData[0]='\0';
                       dbg(DEBUG,"SqlHandleSys: Initializing pcgResultData");
                    }
                 }
                 else if ((ilTotalBufLen+ilLen+12)>=igActResultSize)
                 {
                    while ((ilTotalBufLen+ilLen)>=igActResultSize)
                       igActResultSize+=RESULTBUFSTEP;
                    pcgResultData=(char *)realloc(pcgResultData,igActResultSize);
                    if (pcgResultData==NULL)
                    {
                       rc=RC_FAIL;
                       igActResultSize=0;
                       dbg(TRACE,"SqlHandleSys: Malloc 2 failed");
                       prlBchd->rc = RC_FAIL;
                       rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,"");
                       return rc;
                    }
                 }/* end if pcgResultData ==  NULL */
                 memcpy(pcgResultData+ilTotalBufLen,pcgDataArea,ilLen);
                 ilTotalBufLen+=ilLen;
                 if (i<(ilBoolFkt-1))
                 {
                    memcpy(pcgResultData+ilTotalBufLen,"\r\n",3);
                    ilTotalBufLen+=2;
                 }
                 else
                 {
                    pcgResultData[ilTotalBufLen]='\0';
                 }
                 if (rc==RC_SUCCESS)
                 {
                    pclInFldLst+=strlen(pclInFldLst)+1;
                    pclInFldVal+=strlen(pclInFldVal)+1;
                    ilZero=0;
                 }
                 else
                 {
                    dbg(TRACE,"SqlHandleSys: Error!!");
                    break;
                 }
              } /* end if */
           }
        } /* end if ilBoolFkt */
     } /* end if SYSTAB Request */
     else
     { /* DB Request */
        if (ilBoolFkt==0)
        {
           if ((int)strlen(pclInFldLst)>0)
              rc=syslibSearchDbData(prlCmdblk->obj_name,pclInFldLst,pclInFldVal,
                                    pclFldTmp,pclPtr,&ilZero,"\r\n");
           else
           {
              rc=syslibSearchDbData(prlCmdblk->obj_name,NULL,NULL,pclFldTmp,
                                    pclPtr,&ilZero,"\r\n");
              dbg(DEBUG,"SqlHandleSys: returned from syslibfx with NULL parameters. rc<%d>",
                  rc);
           }
        }
        else if (ilBoolFkt>0)
        { /* ORed list found. */
           ilTotalBufLen=0;
           dbg(DEBUG,"SqlHandleSys: Found %i ORed Conditions",ilBoolFkt);
           for (i=0; i<ilBoolFkt && rc==RC_SUCCESS; i++)
           {
              dbg(DEBUG,"SqlHandleSys: Selection Fld:%s Val:%s, Table: %s",
                  pclInFldLst,pclInFldVal,prlCmdblk->obj_name);
              pcgDataArea[0]='\0';
              pclPtr=&pcgDataArea[0];
              strcpy(pclFldTmp,pclFields);
              dbg(DEBUG,"SqlHandleSys: Working with FldLst<%s> Values<%s>",
                  pclInFldLst,pclInFldVal);
              rc=syslibSearchDbData(prlCmdblk->obj_name,pclInFldLst,pclInFldVal,
                                    pclFldTmp,&pcgDataArea[0],&ilZero,"\r\n");
              if (rc==RC_SUCCESS)
              {
                 ilLen=strlen(pcgDataArea);
                 if (pcgResultData==NULL)
                 {
                    while ((ilTotalBufLen+ilLen)>=igActResultSize)
                       igActResultSize+=RESULTBUFSTEP;
                    pcgResultData=(char *) calloc(1,igActResultSize);
                    if (pcgResultData==NULL)
                    {
                       rc = RC_FAIL;
                       igActResultSize=0;
                       dbg(TRACE,"SqlHandleSys: Malloc 2 failed");
                       prlBchd->rc = RC_FAIL;
                       rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,"");
                       return rc;
                    }
                    else
                    {
                       pcgResultData[0]='\0';
                       dbg(DEBUG,"SqlHandleSys: Initializing pcgResultData");
                    }
                 }
                 else if ((ilTotalBufLen+ilLen+12)>=igActResultSize)
                 {
                    while ((ilTotalBufLen+ilLen)>=igActResultSize)
                       igActResultSize+=RESULTBUFSTEP;
                    pcgResultData=(char *)realloc(pcgResultData,igActResultSize);
                    if (pcgResultData==NULL)
                    {
                       rc=RC_FAIL;
                       igActResultSize=0;
                       dbg(TRACE,"SqlHandleSys: Malloc 2 failed");
                       prlBchd->rc = RC_FAIL;
                       rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,"");
                       return rc;
                    }
                 }/* end if pcgResultData ==  NULL */
                 memcpy(pcgResultData+ilTotalBufLen,pcgDataArea,ilLen);
                 ilTotalBufLen+=ilLen;
                 if (i<(ilBoolFkt-1))
                 {
                    memcpy(pcgResultData+ilTotalBufLen,"\r\n",3);
                    ilTotalBufLen+=2;
                 }
                 else
                 {
                    pcgResultData[ilTotalBufLen]='\0';
                    dbg(DEBUG,"SqlHandleSys: Sending receipt<%s>",pcgResultData);
                    rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,
                                               pcgResultData);
                 }
                 if (rc==RC_SUCCESS)
                 {
                    pclInFldLst+=strlen(pclInFldLst)+1;
                    pclInFldVal+=strlen(pclInFldVal)+1;
                    ilZero=0;
                 }
                 else
                 {
                    dbg(TRACE,"SqlHandleSys: Error!!");
                    break;
                 }
              } /* end if */
           } /* end for */
        } /* end if ilBoolFkt */
     } /* end else Other Request */
     if (rc==RC_SUCCESS)
     {
        dbg(DEBUG,"SqlHandleSys: Got data<%s>",pcgResultData);
     }
     else if (rc==RC_FAIL)
     {
        dbg(TRACE,"SqlHandleSys: Error calling syslibSearch.. function");
        sprintf(pcgDataArea,"SqlHandleSys: Error. Table<%s> or field(s)<%s><%s> probably not found in shared memory",prlCmdblk->obj_name,pclFields,pclSelection);
     }
     else
     {
        rc=RC_FAIL;
        dbg(TRACE,"SqlHandleSys: Key(s)<%s> not found",pclSelection);
        sprintf(pcgDataArea,"SqlHandleSys: Key(s)<%s> not found in shared memory",
                pclSelection);
     } /* end if */
  } /* end if rc==RC_SUCCESS */
  if (pclInFldLst!=NULL)
  {
     free(pclInFldLst);
  }
  if (pclInFldVal!=NULL)
  {
     free(pclInFldVal);
  }
  if (pclFldTmp!=NULL)
  {
     free(pclFldTmp);
  }
  prlBchd->rc=rc;
  if (rc==RC_SUCCESS)
  {
     dbg(DEBUG,"SqlHandleSys: Prepare to send receipt. Result<%s> ilBoolFkt<%d>",
         pcgResultData,ilBoolFkt);
     if (ilBoolFkt>0)
        rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,pcgResultData);
     else
        rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,pcgDataArea);
  }
  else
     rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,pcgDataArea);
#else
  strcpy(pcgDataArea,"SqlHandleSys: System is not UFIS43. Please consult your System Administrator");
  prlBchd->rc=RC_FAIL;
  rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,pcgDataArea);
  dbg(TRACE,"SqlHandleSys: Error! %s",pcgDataArea);
#endif /* end ifdef UFIS43 */

  return rc;
} /* end of SqlHandleSys */

/* ***************************************************
 * Function: 		HandleSA32
 * Parameter: 	void
 * Return: 			RC_SUCCESS, RC_FAIL, SQL_NOTFOUND
 * Description:	Implicit Command. Returns ALC2 values
 * 							corresponding to specified ALC3 value.
 *							ALC3 value specified in Data.
 **************************************************** */
static int HandleSA32 (void)
{
  int rc = RC_SUCCESS;	/* Return code 	*/
  BC_HEAD *prlBchd;	/* Broadcast header	*/
  CMDBLK *prlCmdblk;	/* Command Block 	*/
  char *pclSelection,*pclFields,*pclData;
  char pclTmpBuf[7],pclTable[7], *pclPtr;
  int i,ilZero=0;

#ifdef UFIS43
  que_out = prgEvent->originator;
  memset(pclTable,'\0',7); /* Initialise pclTable */
  dbg(DEBUG,"HandleSA32: This Request is from (%d)",que_out);
  /* Get the structs */
  prlBchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  prlCmdblk= (CMDBLK  *) ((char *)prlBchd->data);
  pclSelection = (char *)prlCmdblk->data;
  pclFields= (char *)pclSelection+strlen(pclSelection)+1;
  pclData=(char *)pclFields +strlen(pclFields)+1;
  if (strlen(pclData)>3)
  {
     strcpy(pcgDataArea,"HandleSA32: Key > 3 characters long.");
     rc=RC_FAIL;
  } /* fi */

  if (strlen(prlCmdblk->obj_name)==3)
  {
     /* project name */
     strcpy(pclTable,"ALT");
     strcat(pclTable,prlCmdblk->obj_name);
  }
  else if(strncmp(prlCmdblk->obj_name,"ALT",3)==0)
  {
     strncpy(pclTable,prlCmdblk->obj_name,strlen(prlCmdblk->obj_name));
     dbg(DEBUG,"Just wrote pclTable %s",pclTable);
  }
  else
  {
     strcpy(pcgDataArea,"Error: Invalid object name");
     prlBchd->rc=RC_FAIL;
     rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,pcgDataArea);
     return rc;
  }
  if (rc!=RC_FAIL)
  {
     dbg(DEBUG,"Object %s. Table %s.",prlCmdblk->obj_name,pclTable);
     ilZero=0;
     pcgDataArea[0]='\0';
     pclPtr=&pcgDataArea[0];
     rc=syslibSearchDbData(pclTable,"ALC3",pclData,"ALC3,ALC2",pclPtr,&ilZero,"");
     if (rc==RC_SUCCESS)
     {
        for (i=0;i<ilZero;i++)
        {
           strcpy(pclTmpBuf,pclPtr);
           delton(pclTmpBuf);
           if (strcmp(pclTmpBuf,pclData)==0)
           {
              dbg(DEBUG,"Found %s in pcgDataArea<%s,%s>",
                  pclData,pclTmpBuf,pclTmpBuf+strlen(pclTmpBuf)+1);
              strcpy(pcgDataArea,pclTmpBuf+strlen(pclTmpBuf)+1);
              rc=RC_SUCCESS;
              break;
           }
           pclPtr+=strlen(pclPtr)+1;
           dbg(DEBUG,"Still searching. Next pclPtr<%s>",pclPtr);
           rc=RC_NOT_FOUND;
        }
     }
     if (rc==RC_NOT_FOUND)
     {
        pcgDataArea[0]='\0';
        rc=RC_SUCCESS;
     }
     else if (rc==RC_FAIL)
     {
        sprintf(pcgDataArea,"Table or Project <%s> does not exist",prlCmdblk->obj_name);
     }
  } /* end if */
  strcpy(prlCmdblk->obj_name,pclTable);
  prlBchd->rc=rc;
  rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,"ALC2",pcgDataArea);
#else
  strcpy(pcgDataArea,"HandleSA32: System is not UFIS43. Please consult your System Administrator");
  prlBchd->rc=RC_FAIL;
  rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,"",pcgDataArea);
#endif /* end ifdef UFIS43 */

  return rc;
} /* end of HandleSA32 */

/* ***************************************************
 * Function: 		HandleSA23
 * Parameter: 	void
 * Return: 			RC_SUCCESS, RC_FAIL, SQL_NOTFOUND
 * Description:	Implicit Command. Returns ALC2 values
 * 							corresponding to specified ALC3 value.
 *							ALC3 value specified in Data.
 **************************************************** */
static int HandleSA23 (void)
{
  int rc = RC_SUCCESS;	/* Return code 	*/
  int i;
  BC_HEAD *prlBchd;		/* Broadcast header	*/
  CMDBLK *prlCmdblk;		/* Command Block 	*/
  char *pclSelection,*pclFields,*pclData;
  char pclTable[7], pclTmpBuf[7],*pclPtr;
  int ilZero=0;

#ifdef UFIS43
  que_out = prgEvent->originator;
  memset(pclTable,'\0',7); /* Initialise pclTable */
  dbg(DEBUG,"HandleSA23: This Request is from (%d)",que_out);
  /* Get the structs */
  prlBchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  prlCmdblk= (CMDBLK  *) ((char *)prlBchd->data);
  pclSelection = prlCmdblk->data;
  pclFields= (char *)pclSelection+strlen(pclSelection)+1;
  pclData=(char *)pclFields +strlen(pclFields)+1;
  if (strlen(pclData)>2)
  {
     strcpy(pcgDataArea,"HandleSA23: Key > 2 characters long.");
     rc=RC_FAIL;
  } /* fi */

  if (strlen(prlCmdblk->obj_name)==3)
  {
     /* project name */
     strcpy(pclTable,"ALT");
     strcat(pclTable,prlCmdblk->obj_name);
  }
  else if (strncmp(prlCmdblk->obj_name,"ALT",3)==0)
  {
     strncpy(pclTable,prlCmdblk->obj_name,strlen(prlCmdblk->obj_name));
     dbg(DEBUG,"Just wrote pclTable %s",pclTable);
  }
  else
  {
     strcpy(pcgDataArea,"Error: Invalid object name");
     prlBchd->rc=RC_FAIL;
     rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,"",pcgDataArea);
     return rc;
  }
  if (rc!=RC_FAIL)
  {
     dbg(DEBUG,"HandleSA23: Object<%s>. Table<%s>. ",prlCmdblk->obj_name,pclTable);
     ilZero=0;
     pcgDataArea[0]='\0';
     pclPtr=&pcgDataArea[0];
     rc=syslibSearchDbData(pclTable,"ALC2",pclData,"ALC2,ALC3",pclPtr,&ilZero,"");
     if (rc==RC_SUCCESS)
     {
        for (i=0;i<ilZero;i++)
        {
           strcpy(pclTmpBuf,pclPtr);
           delton(pclTmpBuf);
           if (strcmp(pclTmpBuf,pclData)==0)
           {
              dbg(DEBUG,"HandleSA23: Found %s in pcgDataArea<%s,%s>",
                  pclData,pclTmpBuf,pclTmpBuf+strlen(pclTmpBuf)+1);
              strcpy(pcgDataArea,pclTmpBuf+strlen(pclTmpBuf)+1);
              rc=RC_SUCCESS;
              break;
           }
           pclPtr+=strlen(pclPtr)+1;
           rc=RC_NOT_FOUND;
           dbg(DEBUG,"HandleSA23: Still searching. Next pclPtr<%s>",pclPtr);
        }
     }
     if (rc==RC_NOT_FOUND)
     {
        rc=RC_SUCCESS;
        pcgDataArea[0]='\0';
     }
     else if (rc==RC_FAIL)
     {
        sprintf(pcgDataArea,"Table or Project <%s> does not exist",prlCmdblk->obj_name);
     }
  } /* end if */
  prlBchd->rc=rc;
  strcpy(prlCmdblk->obj_name,pclTable);
  rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,"ALC3",pcgDataArea);
#else
  strcpy(pcgDataArea,"HandleSA23: System is not UFIS43. Please consult your System Administrator");
  prlBchd->rc=RC_FAIL;
  rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,"",pcgDataArea);
#endif /* end ifdef UFIS43 */

  return rc;
} /* end of HandleSA23 */


/* *********************************************************
 * Function: 		HandleS2O3
 * Parameter: 	void
 * Return: 			RC_SUCCESS, RC_FAIL, SQL_NOTFOUND
 * Description:	Implicit Command. Returns ALC2, ALC3 or
 *	NULL to caller corresponding to specified
 *	Data value.
 ********************************************************* */
static int HandleS2O3 (void)
{
  int i,rc1,rc = RC_SUCCESS;	/* Return code 	*/
  BC_HEAD *prlBchd;		/* Broadcast header	*/
  CMDBLK  *prlCmdblk;		/* Command Block 	*/
  char *pclSelection,*pclFields,*pclData;
  char pclTable[7], *pclPtr;
  int ilZero=0;

#ifdef UFIS43
  que_out = prgEvent->originator;
  memset(pclTable,'\0',7); /* Initialise pclTable */
  dbg(DEBUG,"HandleS2O3: This Request is from (%d)",que_out);
  /* Get the structs */
  prlBchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  prlCmdblk= (CMDBLK  *) ((char *)prlBchd->data);
  pclSelection = prlCmdblk->data;
  pclFields= (char *)pclSelection+strlen(pclSelection)+1;
  pclData=(char *)pclFields +strlen(pclFields)+1;
  dbg(DEBUG,"pclData in 2O3 |%s|",pclData);
  if (strlen(pclData)>3)
  {
     sprintf(pcgDataArea,"HandleS2O3:Key(%s)>3 chars long.",pclData);
     rc=RC_FAIL;
  } /* fi */

  if (strlen(prlCmdblk->obj_name)==3)
  {
     /* project name */
     strcpy(pclTable,"ALT");
     strcat(pclTable,prlCmdblk->obj_name);
  }
  else if (strncmp(prlCmdblk->obj_name,"ALT",3)==0 && strlen(prlCmdblk->obj_name)==6)
  {
     strncpy(pclTable,prlCmdblk->obj_name,strlen(prlCmdblk->obj_name));
     dbg(DEBUG,"Just wrote pclTable<%s>",pclTable);
  }
  else
  {
     strcpy(pcgDataArea,"Error: Invalid object name");
     prlBchd->rc=RC_FAIL;
     dbg(TRACE,"S2O3: Failure. Object name<%s>",prlCmdblk->obj_name);
     rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,"",pcgDataArea);
     return rc;
  }
  if (rc!=RC_FAIL)
  {
     dbg(DEBUG,"ilZero<%d> Data<%s> Object<%s>. Table<%s>.",
         ilZero,pclData,prlCmdblk->obj_name,pclTable);
     pclPtr=&pcgDataArea[0];
     rc=syslibSearchDbData(pclTable,"ALC3",pclData,"ALC3",pclPtr,&ilZero,"");
     dbg(DEBUG,"203:rc-><%i> pclData<%s> pcgDataArea<%s>",rc,pclData,pcgDataArea);
     if (rc==RC_SUCCESS)
     {
        for (i=ilZero;i>0;i--)
        {
           if (strcmp(pclData,pclPtr)==0)
           {
              strcpy(pcgDataArea,"ALC3");
              rc=RC_SUCCESS;
              break;
           } /* end if */
           pclPtr+=strlen(pclPtr)+1; /* Point to next data */
           rc=RC_NOT_FOUND;
        } /* end for */
     }
     else
     {
        ilZero=0;
        pclPtr=&pcgDataArea[0];
        rc1=syslibSearchDbData(pclTable,"ALC2",pclData,"ALC2",pclPtr,&ilZero,"");
        dbg(DEBUG,"2O3:ALC2 rc<%d> rc1<%d> pclPtr<%s> ilZero<%d>",rc,rc1,pclPtr,ilZero);
        if (rc1==RC_SUCCESS)
        {
           for (i=ilZero;i>0;i--)
           {
              dbg(DEBUG,"2O3: pclData %s pclPtr %s",pclData,pclPtr);
              if (strcmp(pclData,pclPtr)==0)
              {
                 if (rc==RC_SUCCESS)
                    strcat(pcgDataArea,"\r\nALC2");
                 else
                    strcpy(pcgDataArea,"ALC2");
                 rc1=RC_SUCCESS;
                 break;
              } /* end if */
              pclPtr+=strlen(pclPtr)+1; /* Point to next data */
              rc1=RC_NOT_FOUND;
           } /* end for */
           if (rc1==RC_SUCCESS)
           {
              rc=RC_SUCCESS;
              dbg(DEBUG,"2O3:After 1. rc<%d> rc1<%d> pcgDataArea<%s>",rc,rc1,pcgDataArea);
           }
           dbg(DEBUG,"2O3:After rc<%d> rc1<%d> pcgDataArea<%s>",rc,rc1,pcgDataArea);
        } /* end if */
     }
     if (rc==RC_NOT_FOUND)
     {
        rc=RC_SUCCESS;
        strcpy(pcgDataArea,"NULL");
     }
     else if (rc==RC_FAIL)
     {
        rc=RC_FAIL;
        strcpy(pcgDataArea,"syslib Error. Table or field(s) probably not found");
     }/* end if */
  } /* end if rc!=RC_FAIL */
  prlBchd->rc=rc;
  strcpy(prlCmdblk->obj_name,pclTable);
  rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,"",pcgDataArea);
  dbg(DEBUG,"Handle2O3: Calling new_tools. pcgDataArea=<%s> rc<%d>",pcgDataArea,rc);
#else
  strcpy(pcgDataArea,"HandleSA2O3: System is not UFIS43. Please consult your System Administrator");
  prlBchd->rc=RC_FAIL;
  rc=new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,"",pcgDataArea);
  dbg(DEBUG,"Handle2O3:Calling new_tools.pcgDataArea=<%s> rc<%d>",pcgDataArea,rc);
#endif /* end ifdef UFIS43 */

  return rc;
} /* end of HandleSA2O3 */

/* ***************************************************************
 * Function:		ParseSel
 * Parameter: 	IN: 	char *pcpSel     	Selection
 *		OUT:	char *pcpInFldLst  	Field name list
 * 		OUT: 	char *pcpInFldVal	Field value list
 * Return: 	-2 -> Malloc Error, -1 -> Parsing error,
		0 -> ALL, 1 -> AND, 2 -> OR
 * Description: parses Selection appropriately
 * **************************************************************/
static int ParseSel(char *pcpSel,char *pcpFldLst, char *pcpFldVal)
{
  int ilBoolFkt = 0;	/* Boolean Return code */
  int ilLstSize=2*MAX_FIELD_LEN+1; /* pcpFldLst Size */
  int ilValSize=2*MAX_FIELD_LEN+1; /* pcpFldVal Size */
  int i,ilCnt=0,ilLstIndex=0,ilValIndex=0; /* Counters */
  char *pclPtr;

  pcpFldLst[0] = 0x00;
  pcpFldVal[0] = 0x00;

  pclPtr=pcpSel; /* Initialise pointer to selection */
  /* Detrmine number of fields using number of '=' */
  for (ilCnt=0;(pclPtr=strstr(pclPtr,"="))!=NULL; ilCnt++)
  {
     pclPtr++;
  } /* end for */
  /* Ascertain selection is valid */
  if (strstr(pcpSel," NOT ")!=NULL ||
      (strstr(pcpSel," AND ")!=NULL && strstr(pcpSel," OR ")!=NULL) ||
      strstr(pcpSel,"<")!=NULL || strstr(pcpSel,">")!=NULL)
  {
     ilBoolFkt=-1;
  } /* Valid. Parse 'AND'ed Selection? */
  else if (strstr(pcpSel,"AND")!=NULL)
  {
     ilBoolFkt=1;
  } /* No. Parse 'OR'ed Selection? */
  else if (strstr(pcpSel,"OR")!=NULL)
  {
     ilBoolFkt=2;
  } /* Then selection has a max. of one field */
  else
  {
     ilBoolFkt=0;
  }

  /* Extract field list and corresponding values */
  switch (ilBoolFkt)
  {
     case 0:
        if (strstr(pcpSel,"=")!=NULL)
        {
           pclPtr=strstr(pcpSel," ");
           while (*pclPtr==' ')
              pclPtr++;
           while (*pclPtr!= '=')
           {
              memcpy(pcpFldLst+ilLstIndex,pclPtr,1);
              pclPtr++;
              ilLstIndex++;
              if (ilLstIndex>=ilLstSize)
              {
                 pcpFldLst=(char *)realloc(pcpFldLst,2*ilLstSize);
                 if (pcpFldLst==NULL)
                 {
                    dbg(TRACE,"ParseSel: Alloc error pcpFldLst");
                    ilBoolFkt=-2;
                    return ilBoolFkt;
                 } /*end if alloc check */
                 ilLstSize*=2;
              } /* end if */
           } /* end while */
           memcpy(pcpFldLst+ilLstIndex,"",1);
           pclPtr++;
           while (*pclPtr==' ' || *pclPtr=='\'')
              pclPtr++; /* skip any blanks or inverted commas */
           while (*pclPtr!=' ' && *pclPtr!='\'' && *pclPtr!='\0')
           {
              memcpy(pcpFldVal+ilValIndex,pclPtr++,1);
              ilValIndex++;
              if (ilValIndex>=ilValSize-1)
              {
                 pcpFldVal=(char *)realloc(pcpFldVal,2*ilValSize);
                 if (pcpFldVal==NULL)
                 {
                    dbg(TRACE,"ParseSel: Alloc error pcpFldVal.");
                    ilBoolFkt=-2;
                    return ilBoolFkt;
                 } /*end if alloc check */
                 ilValSize*=2;
              } /* end if */
           } /* end while */
           memcpy(pcpFldVal+ilValIndex,"",1);
        } /* end if */
        break;
     case 1:
        pclPtr=strstr(pcpSel," ");
        while (*pclPtr==' ') /* skip blanks */
           pclPtr++;
        for (i=0;i<ilCnt;i++)
        {
           while (*pclPtr!= '=')
           {
              memcpy(pcpFldLst+ilLstIndex,pclPtr,1);
              pclPtr++;
              ilLstIndex++;
              if (ilLstIndex>=ilLstSize)
              {
                 pcpFldLst=(char *)realloc(pcpFldLst,2*ilLstSize);
                 if (pcpFldLst==NULL)
                 {
                    dbg(TRACE,"ParseSel: Alloc error pcpFldLst");
                    ilBoolFkt=-2;
                    return ilBoolFkt;
                 } /*end if alloc check */
                 ilLstSize*=2;
              } /* end if */
           } /* end while */
           if (i==(ilCnt-1))
              memcpy(pcpFldLst+ilLstIndex,"",1);
           else
              memcpy(pcpFldLst+ilLstIndex,",",1);
           ilLstIndex++;
           pclPtr++;
           while (*pclPtr==' ' || *pclPtr=='\'')
              pclPtr++; /* skip any blanks or inverted commas */
           while (*pclPtr!=' ' && *pclPtr!='\'' && *pclPtr!='\0')
           {
              memcpy(pcpFldVal+ilValIndex,pclPtr,1);
              ilValIndex++;
              pclPtr++;
              if (ilValIndex>=ilValSize-1)
              {
                 pcpFldVal=(char *)realloc(pcpFldVal,2*ilValSize);
                 if (pcpFldVal==NULL)
                 {
                    dbg(TRACE,"ParseSel: Alloc error pcpFldVal.");
                    ilBoolFkt=-2;
                    return ilBoolFkt;
                 } /*end if alloc check */
                 ilValSize*=2;
              } /* end if */
           } /* end while */
           if (i==(ilCnt-1))
              memcpy(pcpFldVal+ilValIndex,"",1);
           else
           {
              memcpy(pcpFldVal+ilValIndex,",",1);
              ilValIndex++;
              pclPtr=strchr(pclPtr,'D'); /* Go to end of  'AND' */
              while(*(++pclPtr)==' ');
              /* skip any blanks */
           }
        } /* end for */
        ilBoolFkt=0;
        break;
     case 2:
        pclPtr=strstr(pcpSel," ");
        while (*pclPtr==' ')
           pclPtr++;
        for (i=0;i<ilCnt;i++)
        {
           while (*pclPtr!= '=')
           {
              memcpy(pcpFldLst+ilLstIndex,pclPtr,1);
              pclPtr++;
              ilLstIndex++;
              if (ilLstIndex>=ilLstSize)
              {
                 pcpFldLst=(char *)realloc(pcpFldLst,2*ilLstSize);
                 if (pcpFldLst==NULL)
                 {
                    dbg(TRACE,"ParseSel: Alloc error pcpFldLst");
                    ilBoolFkt=-2;
                    return ilBoolFkt;
                 } /*end if alloc check */
                 ilLstSize*=2;
              } /* end if */
           } /* end while */
           memcpy(pcpFldLst+ilLstIndex,"",1);
           ilLstIndex++;
           pclPtr++;
           while (*pclPtr==' ' || *pclPtr=='\'')
              pclPtr++; /* skip any blanks or inverted commas */
           while (*pclPtr!=' ' && *pclPtr!='\'' && *pclPtr!='\0')
           {
              memcpy(pcpFldVal+ilValIndex,pclPtr++,1);
              ilValIndex++;
              if (ilValIndex>=ilValSize-1)
              {
                 pcpFldVal=(char *)realloc(pcpFldVal,2*ilValSize);
                 if (pcpFldVal==NULL)
                 {
                    dbg(TRACE,"ParseSel: Alloc error pcpFldVal");
                    ilBoolFkt=-2;
                    return ilBoolFkt;
                 } /*end if alloc check */
                 ilValSize*=2;
              } /* end if */
           } /* end while */
           memcpy(pcpFldVal+ilValIndex,"",1);
           ilValIndex++;
           if (i<(ilCnt-1))
           {
              pclPtr=strchr(pclPtr,'R'); /* Go to end of  'OR' */
              while (*(++pclPtr)==' ');
              /* skip any blanks */
           } /* end if */
        } /* end for */
        ilBoolFkt=ilCnt; /* No. of fields */
        break;
     default:
        dbg(TRACE,"ParseSel:Invalid Selection");
        ilBoolFkt=-1;
        break;
  } /* end switch */

  if ((igAddHopo == TRUE) &&
      (strcmp(pcgDefTblExt,"TAB") == 0) &&
      (strstr(pcpFldLst,"HOPO") == NULL))
  {
     if (strlen(pcpFldLst) > 0)
     {
        strcat(pcpFldLst,",");
        strcat(pcpFldVal,",");
     } /* end if */
     strcat(pcpFldLst,"HOPO");
     strcat(pcpFldVal,pcgH3LC);
  } /* end if */

  dbg(DEBUG,"ParseSel: FLD <%s> VAL <%s>",pcpFldLst,pcpFldVal);
  return ilBoolFkt;
} /* end ParseSel */

/* *********************************
 * Function:		reset
 * Parameter: 	void
 * Return: 			int
 * Description: Reset return code
 * ********************************/
static int reset() {
int	rc = 0;	/* Return code */
	if(pcgResultData!=NULL)
		free(pcgResultData);
	igActResultSize=RESULTBUFSTEP;
	return rc;
} /* end of reset */

/* ***************************************
 * Function: 		sql_terminate
 * Parameter:		void
 * Return: 			void
 * Description:	Terminates sqlhdl and
 * 							releases all its resources
* ****************************************/
static void sql_terminate() {
	(void)UnsetSignals();
	free(prgItem);
	prgItem = NULL;
	if(pcgResultData!=NULL)
		free(pcgResultData);
	igActResultSize=RESULTBUFSTEP;
	logoff();  /* disconnect from oracle */
	dbg(TRACE,"sql_terminate: Now Leaving.....");
	exit(0);
} /* end of sql_terminate */

/* ***************************************
 * Function: 		sql_handle_sig
 * Parameter:		IN: int ipSig Process signal
 * Return: 			void
 * Description:	Handles signals SIGTERM and
 *							SIGALRM.
 ******************************************/
static void sql_handle_sig(ipSig) {
	switch (ipSig) {
	case SIGTERM:
		dbg(TRACE,"sql_handle_sig: Received Signal <%d> (SIGTERM). Terminating now...",ipSig);
		break;
	case SIGALRM:
		dbg(TRACE,"sql_handle_sig: Received Signal<%d>(SIGALRM)",ipSig);
		break;
	default:
		dbg(TRACE,"sql_handle_sig: Received Signal<%d>",ipSig);
		break;
	} /* end switch; */
	/* All other signals are filtered through */
	sql_terminate();
} /* end of sql_handle_sig */

/* ***************************************
 * Function: 		sql_handle_err
 * Parameter:		IN: int ipErr  Error code
 * Return: 			void
 * Description:	Processes error codes.
 ******************************************* */
static void sql_handle_err(int ipErr) {
	return;
} /* end of sql_handle_err */

/* **************************************
 * Function: 		sql_handle_qerr
 * Parameter:		IN: int ipErr  Error code
 * Return: 			void
 * Description:	Handles queueing errors
 *************************************** */
static void sql_handle_qerr(int ipErr) {
	switch(ipErr) {
	case  QUE_E_FUNC:			/* Unknown function */
					break;
	case QUE_E_MEMORY:		/* Malloc reports no memory */
					break;
	case QUE_E_SEND:			/* Error using msgsnd */
					break;
	case QUE_E_GET:				/* Error using msgrcv */
					break;
	case QUE_E_EXISTS:		/* Route/Queue exists */
					break;
	case QUE_E_NOFIND:		/* Not found (ex. route ) */
					break;
	case QUE_E_ACKUNEX:		/* Unexpected ACK received */
					break;
	case QUE_E_STATUS:		/* Unknown queue status */
					break;
	case QUE_E_INACTIVE:	/* Queue is inactive */
					break;
	case QUE_E_MISACK:		/* Missing ACK */
					break;
	case QUE_E_NOQUEUES:	/* The queues don't exist */
					break;
	case QUE_E_RESP:			/* No response on CREATE */
					break;
	case QUE_E_FULL:			/* Table full */
					break;
	case QUE_E_NOMSG:			/* No message on queue */
					break;
	case QUE_E_INVORG:		/* Mod id by que call is 0 */
					break;
	case QUE_E_NOINIT:		/* Queues is not initialized*/
					break;
	default:							/* Unknown queue error */
					break;
	} /* end switch */
	return;
} /* end of sql_handle_qerr */


/* *************************************************************
 * Function: 	dam_sql_and_send_routine
 * Parameter:	IN: char *pcpSqlBuf 	SQL Statement storage
 *		IN: char *pcpDataBlk 	Data storage
 *              IN: char *pcpBchd  	Broadcast header
 * Return: 	RC_SUCCESS, RC_FAIL, SQL_NOTFOUND.
 * Description:	Executes SQL-Statement and sends result
 *		(error-) code to sender(que_out) and to
 *		the BCHDL via the tools function new_tools_send_sql
 * **************************************************************/
static int dam_sql_and_send(char *pcpSqlBuf,char *pcpDatablk,char *pcpFldLst,BC_HEAD *prpBchd,
                            char *pcpUrnoList, char clShmCmd,BOOL bpSendAnswer, BOOL bpSendBc)
{
  int rc = RC_SUCCESS;		/* Return code 	*/
  int ilFld= 0;
  int ilAnz= 0;
  short ilFkt;			/* database function	*/
  CMDBLK *prlCmdblkOut;		/* Command header	*/
  char *pclFieldsOut;		/* Field pointer	*/
  short i, ilLocalCursor;	/* SQL things 	*/
  short ilDbError = FALSE;
  char *pclPtr,*pclPdatabuf=NULL;
  char pclTmpUrno[32];
  int ilTmpUrno = 0;

  prlCmdblkOut=(CMDBLK *)prpBchd->data;
  pclFieldsOut=(char *)prlCmdblkOut->data+strlen(prlCmdblkOut->data)+1;
  ilLocalCursor = 0;
  ilFkt = START ; /* MCU 20020308 soll nur START sein, COMMIT folgt
                     weiter unten *************************/
  pclPdatabuf = calloc(1,(strlen(pcpDatablk)+12));
  dbg(DEBUG,"sqlhdl/dam_sql_and_send: Create pclPdatabuf");
  if (pclPdatabuf == NULL)
  {
     rc = RC_FAIL;
     dbg(TRACE,"sqlhdl/dam_sql_and_send: Malloc error");
     return rc;
  } /* fi */
  if (rc != RC_FAIL)
  {
     /* i.e pclPdatabuf allocated space successfully */
     strcpy (pclPdatabuf, pcpDatablk);
     delton(pclPdatabuf);  /* Delimiter to Null */
     dbg(DEBUG,"sqlhdl/dam_sql_and_send: Initiate database access. Sqlbuf<%s>",pcpSqlBuf);
     pclPtr=pclPdatabuf;
     dbg(DEBUG,"dam_sql: pclPdatabuf...");
     for (i=0;i<field_count(pcpDatablk);i++)
     {
        dbg(DEBUG,"[%d]-> <%s>",i,pclPtr);
        pclPtr+=strlen(pclPtr)+1;
     }
     CheckPerformance(TRUE);
     /* HIER SOLLEN IN ZUKUNFT DIE SOWIESO SCHON GELESENEN URNOS BENUTZT WERDEN */
     rc = sql_if(ilFkt,&ilLocalCursor,pcpSqlBuf,pclPdatabuf);
     CheckPerformance(FALSE);
     dbg(DEBUG,"dam_sql_and_send: Returned<%d> from sql_if,SQL Buffer<%s>",rc,pcpSqlBuf);
     dbg(DEBUG,"sqlhdl/dam_sql_and_send: returned from sql_if<%d>",rc);
     if (ilLocalCursor!=0)
        close_my_cursor(&ilLocalCursor);
     if (rc!=RC_SUCCESS)
     {
        ilDbError=TRUE;
        if (rc==RC_FAIL)
        {
           GetOraErr(rc,pcgDataArea,pcpSqlBuf,"dam_sql_and_send");
           /* dbg(TRACE,"dam_sql_and_send:Oracle Error <%s>",pcgDataArea); */
        }
     }
     else
     {
        commit_work(); /** MCU 20020308 commit, siehe oben bei ilFkt */
        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
#ifdef UFIS43
        /* Update shared memory */
        if (strstr(pcgTabsInSHM,prlCmdblkOut->obj_name) != NULL)
        {
           dbg(TRACE,"dam_sql_and_send: Updating (%c) shared memory for URNOs: <%s>",
               clShmCmd,pcpUrnoList);
           if (strlen(pcpUrnoList)>0)
           {
              ilAnz= field_count(pcpUrnoList);
              for (ilFld=1; ilFld <= ilAnz; ilFld++)
              {
                 (void)get_real_item(pclTmpUrno,pcpUrnoList,ilFld);
                 tool_filter_spaces(pclTmpUrno);
                 ilTmpUrno=atoi(pclTmpUrno);
                 CheckUrnoValue(prlCmdblkOut->obj_name,pclTmpUrno,ilTmpUrno);
                 dbg(TRACE,"Calling SYSLIB UPDATE: TAB <%s> FKT <%c> URNO <%s>",
                     prlCmdblkOut->obj_name,clShmCmd,pclTmpUrno);
                 rc=syslibUpdateDbData(clShmCmd,prlCmdblkOut->obj_name,"URNO",pclTmpUrno);
                 if (rc != RC_SUCCESS)
                 {
                    dbg(TRACE,"Error returned from syslibUpdateDbData with rc = <%d>",rc);
                    rc=syslibReloadDbData(prlCmdblkOut->obj_name);
                    dbg(TRACE,"dam_sql_and_send: Reloading Table %s rc = <%d>",
                        prlCmdblkOut->obj_name,rc);
                    rc = RC_SUCCESS; /* bad thing, but what shall we do? */
                 } /* end if */
              }   /* for urnos */
           }
           else /* ==> pcpUrnoList == "" */
           {
              dbg(DEBUG,"dam_sql_and_send: Reloading Table %s into SHM: No Urno? Wrong Type?",
                  prlCmdblkOut->obj_name);
              rc=syslibReloadDbData(prlCmdblkOut->obj_name);
              if (rc != RC_SUCCESS)
              {
                 dbg(TRACE,"dam_sql_and_send: syslibReloadDbData Error = %d",rc);
              }
              rc = RC_SUCCESS; /* bad thing, but what shall we do? */
           }
        }
        else
        {
           dbg(TRACE,"dam_sql_and_send: Table <%s> is not in shared memory",
               prlCmdblkOut->obj_name);
        }
#endif /* end #ifdef UFIS43 */
        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
     }
  }
  if (rc==RC_SUCCESS)
  {
     dbg(DEBUG,"dam_sql_and_send: Copying databuf<%s> to <%s>",pcpDatablk,pclPdatabuf);
     strcpy(pcgDataArea,pcpDatablk);
  }
  /* Back to sender */
  prpBchd->rc=rc;
  dbg(DEBUG,"dam_sql: Bchd->rc<%d>  Data buffer<%s>",prpBchd->rc,pcgDataArea);
  if (gl_no_ack == FALSE && bpSendAnswer == TRUE)
  {
     /* jim 20020208: rc=new_tools_send_sql(que_out,prpBchd,prlCmdblkOut,pcpFldLst,
                                            pcgDataArea); */
     (void) new_tools_send_sql_perf(que_out,prpBchd,prlCmdblkOut,pcpFldLst,pcgDataArea);
     dbg(DEBUG," sqlhdl/dam_sql_and_send: Sent answer to sender %d rc = %d", que_out,rc);
  }
  else
  {
     rc = RC_SUCCESS;
     dbg(DEBUG,"sqlhdl/dam_sql_and_send: Sent NO answer to sender %d! None wanted ",que_out);
  } /* fi */
  if (rc== RC_SUCCESS && ilDbError==FALSE && bpSendBc == TRUE)
  {
     /* Broadcast it if it's in the DB */
     /* Each event is going also to actsch */
     /* SEE ROTAB (in /ceda/conf/sgs.tab) */
     dbg(DEBUG," sqlhdl/dam_sql_and_send: Sending answer to broadcaster %d rc = %d Data<%s>",
         que_out,rc,pcgDataArea);
     if (strlen(pcgNewAndOldData) == 0)
     {
        sprintf(pcgNewAndOldData,"%s\n%s",pcgDataArea,pcgOldData);
     } /* end if */
     if (strlen(pcgAllOutFld) == 0)
     {
        strcpy(pcgAllOutFld,pcpFldLst);
     } /* end if */
/* 20130710 CST: Removed because does not work with messages in SHM. Always send separately
   A general fix is required in sysqcp to handle SHM messages to multi destinations
   
#ifdef WMQ */
     /* 20050504 JIM: avoid mod_id+1 in WMQ Sending to bchdl (not on mod_id+1) */
     /* 20050523 JIM: in WMQ send to BCHDL and ACTION, in SYSQCP to mod_id+1 */
     
     rc=new_tools_send_sql_perf(igToBcHdl,prpBchd,prlCmdblkOut,pcgAllOutFld,pcgNewAndOldData);
     rc=new_tools_send_sql_perf(igToAction,prpBchd,prlCmdblkOut,pcgAllOutFld,pcgNewAndOldData);
     
/* #else
     rc=new_tools_send_sql_perf(mod_id+1,prpBchd,prlCmdblkOut,pcgAllOutFld,pcgNewAndOldData);
#endif */

  } /* end if */
  if (pclPdatabuf != NULL)
     free(pclPdatabuf);

  return ilDbError==FALSE ? RC_SUCCESS : RC_FAIL;
} /* dam_sql_and_send*/


/* **************************************************
 * Function: 	build_sql_buf
 * Parameter:	OUT: 	char 	*pcpBuf  	SQL buffer.
 *		IN: 	char 	*pcpFields	Request fields.
 *		IN:	char 	*prpTable 	Object name.
 *		IN:	char 	*pcpFilter	Selections.
 * Return: 	RC_SUCCESS, RC_FAIL, SQL_NOTFOUND.
 * Description:	Composes SQL request statements.
 * ****************************************************/
static int build_sql_buf(char *pcpBuf,char *pcpFields,char *pcpTable,char *pcpFilter)
{
  int	ilFldcnt;
  char pclTblNam[8];

  ilFldcnt=field_count(pcpFields);

  strncpy(pclTblNam,pcpTable,3);
  pclTblNam[3] = 0x00;
  strcpy(pcgRcvSelKey,pcpFilter);

  if (strstr("AFT,ARC",pclTblNam) != NULL)
  {
     CheckWhereClause(TRUE, pcgRcvSelKey, TRUE, TRUE, "\0");
  } /* end if */
  else
  {
     CheckWhereClause(TRUE, pcgRcvSelKey, FALSE, TRUE, "\0");
  } /* end else */

  sprintf(pcpBuf, "SELECT %s FROM %s %s",pcpFields,pcpTable,pcgRcvSelKey);

  dbg(DEBUG,"sqlhdl/build_sql_buf:SQLBUF=\n<%s>",pcpBuf);

  return ilFldcnt;
} /* end of build_sql_buf */

/* **************************************************
 * Function: 	ExtractUrno
 * Parameter:   IN: 	char *pcpDatablk
 *		IN: 	char 	*pcpFields	Request fields.
 * Return: 	Urno, 0 (Urno not found), -1 (Malloc error).
 * Description:	Extracts Urno.
 * ****************************************************/
static unsigned long ExtractUrno(char *pcpDatablk,char *pcpFields)
{
  short ilFldcnt,i;
  char pclTmp[MAX_FIELD_LEN+1], *pclFields, *pclDatablk;

  ilFldcnt=field_count(pcpFields);
  pclFields=(char *)calloc(1,(ilFldcnt*(MAX_FIELD_LEN+1)+1));
  if (pclFields==NULL)
  {
     dbg(TRACE,"ExtractUrno: Cannot allocate");
     return RC_FAIL;
  }
  pclDatablk=(char *) calloc(1,MAX_ROW_LEN);
  if (pclDatablk==NULL)
  {
     free(pclFields);
     dbg(TRACE,"ExtractUrno: Cannot allocate");
     return RC_FAIL;
  }
  strcpy(pclDatablk,pcpDatablk);
  delton(pclDatablk);
  strcpy(pclFields,pcpFields);
  delton(pclFields);
  dbg(DEBUG, "ExtractUrno:Datablk %s, Field %s",pclDatablk,pclFields);
  for (i=0;i<ilFldcnt; i++)
  {
     get_fld(pclFields,i,STR,MAX_FIELD_LEN,pclTmp);
     if (strcmp(pclTmp,"URNO")==0)
     {
        get_fld(pclDatablk,i,STR,MAX_FIELD_LEN,pclTmp);
        free(pclFields);
        free(pclDatablk);
        /*return atoll(pclTmp);*/
        return strtoul (pclTmp,NULL,0);
     }
  }
  free(pclFields);
  free(pclDatablk);

  return 0;
} /* end of ExtractUrno */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;

	do
	{
		/* get next item */
		ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,0,(char*)&prgItem);

		/* set event pointer */
		prgEvent = (EVENT*)prgItem->text;

		/* check ret code */
		if( ilRC == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRC != RC_SUCCESS )
			{
				sql_handle_qerr(ilRC);
			}

			switch (prgEvent->command)
			{
				case HSB_STANDBY	:
					ctrl_sta = prgEvent->command;
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					break;

				case HSB_COMING_UP	:
					ctrl_sta = prgEvent->command;
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					break;

				case HSB_ACTIVE	:
					ctrl_sta = prgEvent->command;
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					ilBreakOut = TRUE;
					break;

				case HSB_STANDALONE	:
					ctrl_sta = prgEvent->command;
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					ResetDBCounter();
					ilBreakOut = TRUE;
					break;

				case HSB_ACT_TO_SBY	:
					ctrl_sta = prgEvent->command;
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					break;

				case HSB_DOWN	:
					ctrl_sta = prgEvent->command;
					sql_terminate();
					break;

				case SHUTDOWN	:
					sql_terminate();
					break;

				case RESET		:
					ilRC = reset();
					break;

				case EVENT_DATA	:
					dbg(DEBUG,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;

				case REMOTE_DB:
					HandleRemoteDB(prgEvent);
					break;

				case TRACE_ON :
					dbg_handle_debug(prgEvent->command);
					break;

				case TRACE_OFF :
					dbg_handle_debug(prgEvent->command);
					break;

				default			:
					dbg(DEBUG,"HandleQueues: unknown event");
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;
			} /* end switch */

			/* Handle error conditions */
			if(ilRC != RC_SUCCESS)
			{
				sql_handle_err(ilRC);
			} /* end if */
		}
		else
		{
			sql_handle_qerr(ilRC);
		}
	} while (ilBreakOut == FALSE);
}

/********************************************************/
/********************************************************/
static void CheckWhereClause(int ipUseOrder, char *pcpSelKey,
                             int ipChkStat, int ipChkHopo, char *pcpOrderKey)
{
  char pclTmpBuf[32];
  char pclAddKey[128];
  char pclRcvSqlBuf[12*1024];
  char *pclWhereBgn = NULL;
  char *pclWhereEnd = NULL;
  char *pclDummyWhere = "  ";
  char *pclOrderBgn = NULL;
  char *pclChrPtr = NULL;
  int ilKeyPos = 0;
  int blFldDat = FALSE;
  int blWhereFound = FALSE;
  pclAddKey[0] = 0x00;

  /* First Copy Selection Text To WorkBuffer */
  strcpy(pclRcvSqlBuf,pcpSelKey);
  /* Set All Chars Of WhereClause To Capitals */
  str_chg_upc(pclRcvSqlBuf);

  pclWhereBgn = strstr(pclRcvSqlBuf,"WHERE ");
  if (pclWhereBgn == NULL)
  {
     pclWhereBgn = pclDummyWhere;
     pclWhereEnd = pclDummyWhere;
  } /* end if */
  else
  {
     /* Skip Over KeyWord */
     pclWhereBgn += 6;
     blWhereFound = TRUE;

     /* Search First Non Blank Character of WhereClause */
     while ((*pclWhereBgn == ' ') && (*pclWhereBgn != '\0'))
     {
        pclWhereBgn++;
     } /* end while */
     pclWhereEnd = pclWhereBgn + strlen(pclWhereBgn);

     /* Set All Data To Blank, So Fields And 'Order By' Are Remaining */
     pclChrPtr = pclWhereBgn;
     blFldDat = FALSE;
     while (*pclChrPtr != '\0')
     {
        if (*pclChrPtr == '\'')
        {
           if (blFldDat == FALSE)
           {
              blFldDat = TRUE;
           } /* end if */
           else
           {
              blFldDat = FALSE;
           } /* end else */
        } /* end if */
        else
        {
           if (blFldDat == TRUE)
           {
             *pclChrPtr = ' ';
           } /* end if */
        } /* end else */
        pclChrPtr++;
     } /* end while */
  } /* end else */

  /* Search 'Order By' Statement */
  pclOrderBgn = strstr(pclRcvSqlBuf,"ORDER BY ");
  if (pclOrderBgn != NULL)
  {
     if (blWhereFound == TRUE)
     {
        pclWhereEnd = pclOrderBgn - 1;
     } /* end if */
  } /* end if */
  if (pclWhereEnd > pclWhereBgn)
  {
     /* Search Last Non Blank Character of WhereClause */
     pclWhereEnd--;
     while ((pclWhereEnd >= pclWhereBgn) && (*pclWhereEnd == ' '))
     {
        pclWhereEnd--;
     } /* end while */
     pclWhereEnd++;
     *pclWhereEnd = '\0';
  } /* end if */

  if (ipChkStat == TRUE)
  {
     if (strstr(pclWhereBgn,"STAT") == NULL)
     {
        strcpy(pclAddKey,"STAT<>'DEL'");
     } /* end if */
  } /* end if */
  ilKeyPos = strlen(pclAddKey);

  if ((igAddHopo == TRUE) && (igModifyAllowed == 1) && (igUseHopo==1) &&
      (ipChkHopo == TRUE) && (strcmp(pcgDefTblExt,"TAB") == 0))
  {
     if (strstr(pclWhereBgn,"HOPO") == NULL)
     {
        if (ilKeyPos > 0)
        {
           StrgPutStrg(pclAddKey,&ilKeyPos," AND ",0,-1,"\0");
        }
        sprintf(pclTmpBuf,"HOPO='%s'",pcgH3LC);
        StrgPutStrg(pclAddKey,&ilKeyPos,pclTmpBuf,0,-1,"\0");
     } /* end if */
  } /* end if */
  pclAddKey[ilKeyPos] = 0x00;

  if ((ilKeyPos > 0) || (ipUseOrder == FALSE))
  {
     /* Restore Original WhereClause */
     strcpy(pclRcvSqlBuf,pcpSelKey);
     if (blWhereFound == TRUE)
     {
        *pclWhereEnd = '\0';
     } /* end if */
     dbg(DEBUG,"MODIFY WHERECLAUSE:");
     dbg(DEBUG,"KEY: <%s>",pclWhereBgn);
     dbg(DEBUG,"ADD: <%s>",pclAddKey);
     if (ilKeyPos > 0)
     {
        if (pclWhereEnd > pclWhereBgn)
        {
           sprintf(pcpSelKey,"WHERE (%s)",pclWhereBgn);
           ilKeyPos = strlen(pcpSelKey);
           StrgPutStrg(pcpSelKey,&ilKeyPos," AND ",0,-1,"\0");
        } /* end if */
        else
        {
           sprintf(pcpSelKey,"WHERE ");
           ilKeyPos = strlen(pcpSelKey);
        } /* end else */
        StrgPutStrg(pcpSelKey,&ilKeyPos,pclAddKey,0,-1,"\0");
        pcpSelKey[ilKeyPos] = 0x00;
     } /* end if */
     else
     {
        if (pclWhereEnd > pclWhereBgn)
        {
           sprintf(pcpSelKey,"WHERE %s",pclWhereBgn);
        } /* end if */
     } /* end else */
     if (ipUseOrder == FALSE)
     {
        pcpOrderKey[0] = 0x00;
     } /* end if */
     if (pclOrderBgn != NULL)
     {
        if (ipUseOrder == TRUE)
        {
           ilKeyPos = strlen(pcpSelKey);
           StrgPutStrg(pcpSelKey,&ilKeyPos," ",0,-1,pclOrderBgn);
           pcpSelKey[ilKeyPos] = 0x00;
        } /* end if */
        else
        {
           strcpy(pcpOrderKey,pclOrderBgn);
        } /* end else */
     } /* end if */
     dbg(DEBUG,"KEY: <%s>",pcpSelKey);
     if (ipUseOrder == FALSE)
     {
        dbg(DEBUG,"ORD: <%s>",pcpOrderKey);
     } /* end if */
  } /* end if */

  return;
} /* end CheckWhereClause */


/********************************************************/
/********************************************************/
static void RemoveHopoInWhereClause(char *pcpSelBuf)
{
  int		ilDataStart = FALSE;

  char pclTmpBuf[32];
  char pclAddKey[128];
  char pclRcvSelBuf[12*1024];

  char *pclHopoBgn = NULL;
  char *pclHopoEnd = NULL;

  char *pclWhereBgn = NULL;
  char *pclWhereEnd = NULL;
  char *pclDummyWhere = "  ";
  char *pclOrderBgn = NULL;
  char *pclChrPtr = NULL;
  int ilKeyPos = 0;
  int blFldDat = FALSE;
  int blWhereFound = FALSE;
  pclAddKey[0] = 0x00;

  /* First Copy Selection Text To WorkBuffer */
  strcpy(pclRcvSelBuf,pcpSelBuf);
  /* Set All Chars Of WhereClause To Capitals */
  str_chg_upc(pclRcvSelBuf);
  pclWhereBgn = pclRcvSelBuf;

  if ((pclHopoBgn=strstr(pclRcvSelBuf,"HOPO")) == NULL)
  {
    dbg(DEBUG,"NO HOPO IN WHERE-CLAUSE");
    return;
  }
  else
  {
     pclWhereBgn = strstr(pclRcvSelBuf,"WHERE ");
     if (pclWhereBgn == NULL)
     {
        pclWhereBgn = pclRcvSelBuf;
     } /* end if */
     else
     {
        /* Skip Over KeyWord */
        pclWhereBgn += 6;
        blWhereFound = TRUE;
     }
     /* Search First Non Blank Character of WhereClause */
     while ((*pclWhereBgn == ' ') && (*pclWhereBgn != '\0'))
     {
        pclWhereBgn++;
     } /* end while */
     pclWhereEnd = pclWhereBgn + strlen(pclWhereBgn);
     if (pclHopoBgn != pclWhereBgn)
     {
        pclHopoBgn-=2;  /* Skip before Blank */
        while ((*pclHopoBgn != ' ') && (pclHopoBgn > pclWhereBgn))/* skip over keyword */
        {
           pclHopoBgn--;
        }
     }
     *pclHopoBgn = '\0';
     ilDataStart = FALSE;
     if ((pclHopoEnd=strstr(pcpSelBuf,"HOPO")) != NULL);
     {
        while (*pclHopoEnd != '\0')
        {
           if ((*pclHopoEnd == '\'') && (ilDataStart == TRUE))
              break; /* Data End */
           if ((*pclHopoEnd == '\'') && (ilDataStart == FALSE))
           {
              ilDataStart = TRUE; /* DataBegin */
           }
           pclHopoEnd++;
        }
        pclHopoEnd++;
     }
     if (pclHopoEnd < pclWhereEnd)
        strcat(pclRcvSelBuf,pclHopoEnd);
     dbg(TRACE,"RemoveHopoInWhereClause: ORIGINAL SELECTION: %s",pcpSelBuf);
     dbg(TRACE,"RemoveHopoInWhereClause: MODIFIED SELECTION: %s",pclRcvSelBuf);
     strcpy(pcpSelBuf,pclRcvSelBuf);
  }

  return;
} /* end RemoveHopoInWhereClause */

static void CompareHopoInList(char *pcpFldBuf,char *pcpFldData)
{
  int ilDataStart = FALSE;
  char pclTmpBuf[32];
  char pclRcvFldBuf[12*1024];
  char pclHopo[12];
  char *pclHopoBgn = NULL;
  int ilFld = 0;

  if ((pclHopoBgn=strstr(pcpFldBuf,"HOPO")) == NULL)
  {
     dbg(DEBUG,"NO HOPO in  FieldList");
     return;
  }
  else
  {
     ilFld = get_item_no(pcpFldBuf,"HOPO",5) + 1;
     if (ilFld > 0)
     {
        get_real_item(pclHopo,pcpFldData,ilFld);
     } /* end if */
     if ((strncmp(pcgH3LC,pclHopo,3)) != 0)
     {
        dbg(TRACE,"HOPO-FAILURE: HOPO IN TW_END<%3s> DIFFERENT FROM FIELDLIST<%3s>",
            pcgH3LC,pclHopo);
     }
  }

  return;
}

/************************************************************************/
static void CompareHopoInWhereClause(char *pcpSelBuf)
{
  int ilDataStart = FALSE;
  char pclTmpBuf[32];
  char pclAddKey[128];
  char pclHopo[12];
  char pclRcvSelBuf[12*1024];
  char *pclHopoBgn = NULL;
  char *pclHopoEnd = NULL;
  char *pclSelectBgn = NULL;
  char *pclWhereBgn = NULL;
  char *pclWhereEnd = NULL;
  char *pclDummyWhere = "  ";
  char *pclOrderBgn = NULL;
  char *pclChrPtr = NULL;
  int ilKeyPos = 0;
  int blFldDat = FALSE;
  int blWhereFound = FALSE;
  pclAddKey[0] = 0x00;

  /* First Copy Selection Text To WorkBuffer */
  strcpy(pclRcvSelBuf,pcpSelBuf);
  /* Set All Chars Of WhereClause To Capitals */
  str_chg_upc(pclRcvSelBuf);
  pclSelectBgn = pclRcvSelBuf;

  if ((pclHopoBgn=strstr(pclRcvSelBuf,"HOPO")) == NULL)
  {
     dbg(DEBUG,"NO HOPO in  Selection");
     return;
  }
  else
  {
     while ((*pclHopoBgn != '\'') && (*pclHopoBgn != '\0'))
     {
        pclHopoBgn++;
     }
     pclHopoBgn++;
     strncpy(pclHopo,pclHopoBgn,3);
     pclHopo[3]=0x00;
     if ((strncmp(pclHopo,pcgH3LC,3)) != 0)
     {
        dbg(TRACE,"HOPO-FAILURE: HOPO IN TW_END<%s> DIFFERENT FROM WHERECLAUSE<%s>",
            pcgH3LC,pclHopo);
     }
  }

  return;
}

/********************************************************/
/********************************************************/
static void CheckHomePort(void)
{
  int ilGetRc = RC_SUCCESS;
  int ilTblNbr = -1;
  int ilHomLen = 0;
  int ilExtLen = 0;
  char pclHome[8];
  char pclExts[8];
  char pclTmp[8];

  if (strcmp(pcgDefTblExt,"TAB") == 0)
  {
     /* First Check Table Extension */
     /* Because We Need It Later */
     ilExtLen = get_real_item(pclExts,pcgTwEnd,2);
     if (ilExtLen != 3)
     {
       strcpy(pclExts,pcgDefTblExt);
     } /* end if */
     /* QuickHack */
     strcpy(pclExts,pcgDefTblExt);
     if (strcmp(pclExts,pcgTblExt) != 0)
     {
        strcpy(pcgTblExt,pclExts);
        dbg(DEBUG,"EXTENSION SET TO <%s>",pcgTblExt);
     } /* end if */
     /* Now Check HomePort */
     ilHomLen = get_real_item(pclHome,pcgTwEnd,1);
     if (ilHomLen != 3)
     {
        strcpy(pclHome,pcgDefH3LC);
        dbg(TRACE,"HOMEPORT SET TO DEFAULT <%s>",pcgDefH3LC);
     } /* end if */
     if (strcmp(pclHome,pcgH3LC) != 0)
     {
        strcpy(pcgH3LC,pclHome);
        dbg(DEBUG,"HOMEPORT SWITCHED TO <%s>",pcgH3LC);
     } /* end if */
  } /* end if */

  return;
} /* end CheckHomePort */

/*******************************************************/
/*******************************************************/
static int CheckTableExtension(char *pcpTblNam)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  sprintf(pcgSqlBuf,"SELECT TABLE_NAME FROM USER_TABLES "
		    "WHERE TABLE_NAME='%s%s'",pcpTblNam,pcgTblExt);
  slFkt = START;
  slCursor = 0;
  ilGetRc = sql_if(slFkt, &slCursor, pcgSqlBuf,pcgDataArea);
  if (ilGetRc == RC_SUCCESS)
  {
     ilRC = RC_SUCCESS;
  } /* end if */
  else
  {
     if (ilGetRc != NOTFOUND)
     {
       dbg(TRACE,"=====================================================");
       dbg(TRACE,"ERROR: WRONG SGS CONFIGURATION !!");
       dbg(TRACE,"TABLE EXT CORRECTED TO: <%s>",pcgH3LC);
       strcpy(pcgDefTblExt,pcgH3LC);
       strcpy(pcgTblExt,pcgH3LC);
       dbg(TRACE,"=====================================================");
       ilRC = RC_FAIL;
     } /* end if */
  } /* end else */
  close_my_cursor(&slCursor);

  return ilRC;
} /* end CheckTableExtension */

/* *******************************************************/
/* *******************************************************/
static void CheckPerformance(int ipFlag)
{
  if (ipFlag == TRUE)
  {
     dbg(TRACE,"---- SQL START");
     tgBeginStamp = time(0L);
  } /* end if */
  else
  {
     tgEndStamp = time(0L);
     tgStampDiff = tgEndStamp - tgBeginStamp;
     dbg(TRACE,"---- SQL READY (%d SEC)",tgStampDiff);
     if (tgStampDiff > 10)
     {
        dbg(TRACE,"PLEASE CHECK PERFORMANCE (%d SEC)",tgStampDiff);
     } /* end if */
   } /* end else */

   return;
} /* end CheckPerformance */

/* *******************************************************/
/* There are Clients sending the HOPO Field with wrong or */
/* blank HOPO Value. The way to avoid irritations in the */
/* Data Tables is to erase the field and its data, so that */
/* the programmed evaluation of Hopo Data can work */
/* *******************************************************/
static void RemoveHopoFromList(char *pcpFields, char *pcpData)
{
  SetFieldValue(FALSE,pcpFields,pcpData,"HOPO",pcgH3LC);
  return;
} /* end RemoveHopoFromList */


/******************************************************************************/
/******************************************************************************/
static void SetFieldValue(int ipFlag, char *pcpFields, char *pcpData, char *pcpSetFld,
                          char *pcpSetDat)
{
  int ilFldNbr = 0;
  int ilFldCnt = 0;
  int ilFld = 0;
  int ilFldPos = 0;
  int ilDatPos = 0;
  int ilSetField = TRUE;
  char pclFldNam[16];
  char pclFldDat[4096];  /* 20050308 JIM: PRF 7053 modified from 2048 to 4096 */
  char pclOldFld[4096];
  char pclOldDat[DATABLK_SIZE];

  ilFldNbr = get_item_no(pcpFields,pcpSetFld,5) + 1;
  if (ilFldNbr > 0)
  {
     strcpy(pclOldFld,pcpFields);
     strcpy(pclOldDat,pcpData);
     ilFldCnt = field_count(pcpFields);
     for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
     {
        ilSetField = TRUE;
        get_real_item(pclFldNam,pclOldFld,ilFld);
        get_real_item(pclFldDat,pclOldDat,-ilFld);
        if (strcmp(pclFldNam,pcpSetFld) == 0)
        {
           strcpy(pclFldDat,pcpSetDat);
           ilSetField = ipFlag;
        } /* end if */
        if (ilSetField == TRUE)
        {
           StrgPutStrg(pcpFields,&ilFldPos,pclFldNam,0,-1,",");
           StrgPutStrg(pcpData,&ilDatPos,pclFldDat,0,-1,",");
        } /* end if */
     } /* end for */
     if (ilFldPos > 0)
     {
        ilFldPos--;
        ilDatPos--;
     } /* end if */
     pcpFields[ilFldPos] = 0x00;
     pcpData[ilDatPos] = 0x00;
  } /* end if */
  else
  {
     if (ipFlag == TRUE)
     {
        strcat(pcpFields,",");
        strcat(pcpFields,pcpSetFld);
        strcat(pcpData,",");
        strcat(pcpData,pcpSetDat);
     } /* end if */
  } /* end else */

  return;
} /* end SetFieldValue */


/* *******************************************************/
/* *******************************************************/
static void CollectUfisTables(void)
{
  int ilRC = RC_SUCCESS;
  int ilChrPos = 0;
  short slCursor = 0;
  short slFkt = 0;

  strcpy(pcgIgnoreHopo,"SYSTAB,NUMTAB,MSGTAB,TABTAB,LOGTAB,IDXTAB");
  sprintf(pcgSqlBuf,"SELECT TABLE_NAME FROM USER_TAB_COLUMNS "
                    "WHERE COLUMN_NAME='HOPO' "
                    "AND TABLE_NAME IN "
                    "(SELECT TABLE_NAME FROM USER_TAB_COLUMNS "
                    "WHERE TABLE_NAME LIKE '___%s' "
                    "AND COLUMN_NAME='URNO') "
                    "ORDER BY TABLE_NAME",pcgDefTblExt);
  dbg(DEBUG,"<%s>",pcgSqlBuf);
  pcgUfisTables[0] = 0x00;
  ilChrPos = 0;
  slCursor = 0;
  slFkt=START;
  while ((ilRC=sql_if(slFkt,&slCursor,pcgSqlBuf,pcgDataArea)) == RC_SUCCESS)
  {
     str_trm_all(pcgDataArea," ",TRUE);
     if (strstr(pcgIgnoreHopo,pcgDataArea) == NULL)
     {
        StrgPutStrg(pcgUfisTables, &ilChrPos, pcgDataArea, 0, -1, ",");
     } /* end if */
     slFkt = NEXT;
  } /* end while */
  close_my_cursor(&slCursor);
  if (ilChrPos > 0)
  {
     ilChrPos--;
  } /* end if */
  pcgUfisTables[ilChrPos] = 0x00;
  dbg(TRACE,"UFIS TABLES CONTAINING 'URNO' AND 'HOPO'\n<%s>",pcgUfisTables);

  sprintf(pcgSqlBuf,"SELECT DISTINCT TANA FROM SYSTAB WHERE SYST = 'Y'");
  dbg(DEBUG,"<%s>",pcgSqlBuf);
  pcgTabsInSHM[0] = 0x00;
  ilChrPos = 0;
  slCursor = 0;
  slFkt=START;
  while ((ilRC=sql_if(slFkt,&slCursor,pcgSqlBuf,pcgDataArea)) == RC_SUCCESS)
  {
     str_trm_all(pcgDataArea," ",TRUE);
     StrgPutStrg(pcgTabsInSHM,&ilChrPos,pcgDataArea,0,-1,",");
     pcgTabsInSHM[ilChrPos-1] = 0x00;
     strcat(pcgTabsInSHM,pcgTblExt);
     strcat(pcgTabsInSHM,",");
     ilChrPos += strlen(pcgTblExt);
     slFkt = NEXT;
  } /* end while */
  close_my_cursor(&slCursor);
  if (ilChrPos > 0)
  {
     ilChrPos--;
  } /* end if */
  pcgTabsInSHM[ilChrPos] = 0x00;
  dbg(TRACE,"UFIS TABLES WHICH ARE IN SHM:\n<%s>",pcgTabsInSHM);

  return;
} /* end CollectUfisTables */

/* *******************************************************/
/* *******************************************************/
static void CollectHopoTables(void)
{
  int ilRC = RC_SUCCESS;
  int ilChrPos = 0;
  short slCursor = 0;
  short slFkt = 0;
  int   ilLen = 0;
  char pclTmpStrg[8];

  sprintf(pclTmpStrg,"%s,",pcgDefTblExt);
  /* Short Tablenames in TABTAB */
  strcpy(pcgIgnoreHopo,"SYS,NUM,MSG,TAB,LOG,IDX");
  sprintf(pcgSqlBuf,"SELECT LTNA FROM TABTAB WHERE IGHO<>'Y'");
  dbg(DEBUG,"<%s>",pcgSqlBuf);
  pcgHopoTables[0] = 0x00;
  ilChrPos = 0;
/*
  slCursor = 0;
  slFkt=START;
  while ((ilRC=sql_if(slFkt,&slCursor,pcgSqlBuf,pcgDataArea)) == RC_SUCCESS)
  {
     str_trm_all(pcgDataArea," ",TRUE);
     if (strstr(pcgIgnoreHopo,pcgDataArea) == NULL)
     {
       StrgPutStrg(pcgHopoTables, &ilChrPos, pcgDataArea, 0, -1, pclTmpStrg);
     }
     slFkt = NEXT;
  }
  close_my_cursor(&slCursor);
  if (ilChrPos > 0)
  {
     ilChrPos--;
  }
*/
  pcgHopoTables[ilChrPos] = 0x00;
  dbg(TRACE,"NOTE: HOPO TABLES <%s> IN TABTAB ",pcgHopoTables);
  dbg(TRACE,"NOTE: THIS FUNCTION IS NO LONGER SUPPORTED");

  return;
} /* end CollectHopoTables */

/* *******************************************************/
/* *******************************************************/
static void CheckUrnoValue(char *pcpTable, char *pcpResult, long lpValue)
{
  int ilRC = RC_SUCCESS;
  int ilZero = 0;
  char pclTable[16];
  char pclFields[16];
  char pclSelKey[16];
  char pclFity[16];

  strncpy(pclTable,pcpTable,3);
  pclTable[3] = 0x00;
  strcpy(pclFields,"TANA,FINA");
  sprintf(pclSelKey,"%s,URNO",pclTable);
  ilRC = syslibSearchSystabData(pcgDefTblExt,pclFields,pclSelKey,
                                "FITY",pclFity,&ilZero,",");
  if ((ilRC == RC_SUCCESS) && (pclFity[0] != 'N'))
  {
     sprintf(pcpResult,"%-10d",lpValue);
     dbg(DEBUG,"CheckUrnoValue: TABLE <%s> URNO <%s> IS CHAR",pclTable,pcpResult);
  } /* end if */
  else
  {
     sprintf(pcpResult,"%d",lpValue);
     dbg(DEBUG,"CheckUrnoValue: TABLE <%s> URNO <%s> IS NUMBER",pclTable,pcpResult);
  } /* end else */

  return;
} /* end CheckUrnoValue */

/* *******************************************************/
/* *******************************************************/
static int CheckUrnoType(char *pcpTable)
{
  int ilRC = RC_SUCCESS;
  int ilZero = 0;
  char pclTable[16];
  char pclFields[16];
  char pclSelKey[16];
  char pclFity[16];

  strncpy(pclTable,pcpTable,3);
  pclTable[3] = 0x00;
  strcpy(pclFields,"TANA,FINA");
  sprintf(pclSelKey,"%s,URNO",pclTable);
  ilRC = syslibSearchSystabData(pcgDefTblExt,pclFields,pclSelKey,
                                "FITY",pclFity,&ilZero,",");
  if (ilRC == RC_SUCCESS)
  {
     if (pclFity[0] == 'C')
     {
        dbg(TRACE,"CHECK: URNO of TABLE <%s> IS CHAR",pclTable);
        return 0;
     } /* end if */
     else if(pclFity[0] == 'N')
     {
        dbg(TRACE,"CHECK: URNO of TABLE <%s> IS NUMBER",pclTable);
        return 1;
     } /* end if */
  } /* end if */
  else
  {
     dbg(TRACE,"CHECK: URNO of TABLE <%s> NOT SET IN SHM! USED AS NUMBER!!",pclTable);
     return 1;
  } /* end else */

  return -1;
} /* end CheckUrnoType */

/* *******************************************************/
/* *******************************************************/
static int HandleFreeSql(char *pcpTblCmd,
                         char *pcpSqlCmd,char *pcpFldInf,char *pcpSqlDat)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilLoop = FALSE;
  int ilFldCnt = 1;
  int ilTotLen = 0;
  int ilLen = 0;
  int ilChrPos = 0;
  short slFkt = 0;
  short slCursor = 0;
  char pclAnsTyp[8];
  char pclBrcTyp[8];
  char pclCmdKey[8];
  char pclTmpBuf[8];

  dbg(TRACE,"RECEIVED FREE SQL COMMAND");
  dbg(TRACE,"TblCmd <%s>",pcpTblCmd);
  dbg(TRACE,"SqlCmd <%s>",pcpSqlCmd);
  dbg(TRACE,"SqlDat <%s>",pcpSqlDat);
  dbg(TRACE,"Info:  <%s>",pcpFldInf);
  ilLoop = FALSE;
  get_real_item(pclAnsTyp,pcpTblCmd,1);
  get_real_item(pclBrcTyp,pcpTblCmd,2);
  get_real_item(pclCmdKey,pcpTblCmd,3);
  if (strcmp(pclCmdKey,"READ") == 0)
  {
     ilLoop = TRUE;
     get_real_item(pclTmpBuf,pcpTblCmd,4);
     ilFldCnt = atoi(pclTmpBuf);
     if (ilFldCnt < 1)
     {
       ilFldCnt = 1;
     } /* end if */
     dbg(TRACE,"READING DATA OF %d FIELDS",ilFldCnt);
  } /* end if */
  if (pclAnsTyp[0] == 'Q')
  {
     dbg(TRACE,"QUICK ANSWER SENT TO %d",que_out);
     (void) tools_send_info_flag_perf(que_out,0, pcgDestName, "", pcgRecvName,
                                      "", "", pcgTwStart, pcgTwEnd,
                                      "SQL",pcpTblCmd,pcpSqlCmd,pcpFldInf,pcpSqlDat,0);
  } /* end if */
  strcpy(pcgDataArea,pcpSqlDat);
  strcpy(pcgSqlBuf,pcpSqlCmd);
  delton(pcgDataArea);
  slCursor = 0;
  slFkt=START;
  do
  {
     ilGetRc = sql_if(slFkt,&slCursor,pcgSqlBuf,pcgDataArea);
     if (ilGetRc == RC_SUCCESS)
     {
        BuildItemBuffer(pcgDataArea,"",ilFldCnt,",");
        dbg(TRACE,"SQL ANSWER <%s>",pcgDataArea);
        ilTotLen += strlen(pcgDataArea);
        if (ilTotLen > igActResultSize)
        {
           igActResultSize += RESULTBUFSTEP;
           dbg(TRACE,"FREE SQL: MALLOC %d",igActResultSize);
           pcgResultData=(char *) realloc(pcgResultData,igActResultSize);
           if (pcgResultData == NULL)
           {
              debug_level = TRACE;
              dbg(TRACE,"FREE SQL: MALLOC ERROR");
              exit(0);
           } /* end if */
        } /* end if */
        StrgPutStrg(pcgResultData, &ilChrPos, pcgDataArea, 0, -1, "\n");
     } /* end if */
     else
     {
        if (ilGetRc != SQL_NOTFOUND)
        {
           dbg(TRACE,"SQL ERROR (%d)",ilGetRc);
           strcpy(pcpTblCmd,"ERROR");
           GetOraErr(ilGetRc,pcgDataArea,pcgSqlBuf,"HandleFreeSql");
	   ilRC = RC_FAIL;
        } /* end if */
        else
        {
           if (ilChrPos == 0)
           {
              dbg(TRACE,"SQL NOT FOUND");
              strcpy(pcpTblCmd,"NOTFOUND");
              strcpy(pcgDataArea,pcpSqlDat);
           } /* end if */
        } /* end else */
     } /* end else */
     slFkt = NEXT;
  } while ((ilGetRc == RC_SUCCESS) && (ilLoop == TRUE));
  commit_work();
  close_my_cursor(&slCursor);
  if (ilChrPos > 0)
  {
     ilChrPos--;
  } /* end if */
  pcgResultData[ilChrPos] = 0x00;
  if (ilRC != RC_SUCCESS)
  {
     strcpy(pcgResultData,pcgDataArea);
  } /* end if */
  dbg(TRACE,"FINAL RESULT\n<%s>",pcgResultData);
  if (pclAnsTyp[0] != 'Q')
  {
     dbg(TRACE,"LATE ANSWER SENT TO %d",que_out);
     (void) tools_send_info_flag_perf(que_out,0, pcgDestName, "", pcgRecvName,
                                      "", "", pcgTwStart, pcgTwEnd,
                                      "SQL",pcpTblCmd,pcpSqlCmd,pcpFldInf,pcgResultData,0);
  } /* end if */
  if ((ilRC == RC_SUCCESS) && (pclBrcTyp[0] == 'Y'))
  {
     dbg(TRACE,"SEND BROADCAST");
     (void) tools_send_info_flag_perf(igToBcHdl,0, pcgDestName, "", pcgRecvName,
                                      "", "", pcgTwStart, pcgTwEnd,
                                      "SQL",pcpTblCmd,pcpSqlCmd,pcpFldInf,pcpSqlDat,0);
  } /* end if */

  return RC_SUCCESS;
} /* end HandleFreeSql */


/* *******************************************************/
/* *******************************************************/
static int WriteRecordData(char *pcpTbl, char *pcpSel,char *pcpFld,char *pcpDat)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilGetRcShm = RC_SUCCESS;
  int ilError = TRUE;
  int ilItmCnt = 0;
  int ilItm = 0;
  int ilFld = 0;
  int ilSelPos = 0;
  int ilDatPos = 0;
  short slCursor = 0;
  short slFkt = START;
  char pclActCmd[8];
  char pclFldNam[8];
  char pclFldVal[8];
  char pclFldDat[4096];  /* 20050308 JIM: PRF 7053 modified from 2048 to 4096 */
  char pclFldLst[4096];
  char pclNewUrno[64];
  char *pclPtr = NULL;
  char *pclSelPtr = NULL;

	/* JWE: 21.01.2003: added so that also WRD commands update SHM */
	char clCmdId = 0x00;
	char pclTmpUrno[15];
	int  ilTmpUrno=0;
	char pclShmUpdateSel[iMAX_BUF_SIZE];
	int ilLen = 0;
	int rc = RC_SUCCESS;
	/* JWE: 21.01.2003: END */

	/* JWE: 19.02.2003: added function to process SQL-like selections */
  int ilCount = 0;
  int ilTokenCount = 0;
  int ilCnt = 0;
  int ilCnt2 = 0;
  int ilRC_Replace = 0;
  int ilUseRealSqlSelect = FALSE;
	char pclSubs[12];
	char *pclSubsPointer;
	char *pclSubsCountPointer;
	char pclData[iMAX_BUF_SIZE];
	char pclField[iMIN];
	/* JWE: 19.02.2003: END */

	memset(pclShmUpdateSel,0x00,iMAX_BUF_SIZE);

/* JIM 20051102: if table not in SHM or if more *_OUT fields as received,
                 reread data also :
 */
	*pclTmpUrno=0x00;

	if (strlen(pcpDat)<=0)
		return RC_FAIL;

	/* JWE: 05.02.2003: now on WRD command also LSTU;CDAT;USEC,USEU*/
	/*                  are checked and inserted/removed */
	/* first we assume an update to happen and check if we need to add */
	/* some generic table fields like LSTU,CDAT,etc.                   */
  dbg(TRACE,"WRD: ---------------- <START> ---------------");
  dbg(TRACE,"WRD: --- NOW TRYING TO UPDATE GIVEN DATA! ---");
	pcgDataArea[0] = 0x00;
	pcgFldLst[0] = 0x00;
	pcgDatLst[0] = 0x00;
	strcpy(pcgFldLst,pcpFld);
	strcpy(pcgDatLst,pcpDat);
	if (igCurTabIdx >= 0)
	{
		rc = CheckCDAT('U',prgTabDescr[igCurTabIdx].cgCdatType,
									 pcgFldLst,pcgDataArea,pcgDatLst);
		rc = CheckUSEC('U',prgTabDescr[igCurTabIdx].cgUsecType,
									 pcgFldLst,pcgDataArea,pcgDatLst);
		rc = CheckLSTU('U',prgTabDescr[igCurTabIdx].cgLstuType,
									 pcgFldLst,pcgDataArea,pcgDatLst);
		rc = CheckUSEU('U',prgTabDescr[igCurTabIdx].cgUseuType,
									 pcgFldLst,pcgDataArea,pcgDatLst);
	}

	/**********************************************/
	/* Now prepare selection from new-style input */
	/**********************************************/
	if (strstr(pcpSel,"#FC:") != NULL)
	{
		dbg(DEBUG,"WRD: SUBST.-SEL=<%s>",pcpSel);
		ilUseRealSqlSelect = TRUE;
  	ilCount = GetNoOfElements(pcpFld,',');
		for (ilCnt=0; ilCnt<ilCount;ilCnt++)
		{
			ilError=FALSE;
			*pclSubs=0x00;
			memset(pclField,0x00,iMIN);
			get_real_item(pclField,pcpFld,ilCnt+1);
			memset(pclData,0x00,iMAX_BUF_SIZE);
			get_real_item(pclData,pcpDat,ilCnt+1);
			sprintf(pclSubs,"#FC:%s#",pclField);
			/*dbg(DEBUG,"WRD: check <%s> token.",pclSubs);*/
			pclSubsPointer=NULL;
			while ((pclSubsPointer=strstr(pcpSel,pclSubs))!=NULL)
			{
				if ((ilRC_Replace = SearchStringAndReplace(pcpSel,pclSubs,pclData)) != RC_SUCCESS)
				{
					dbg(TRACE,"WRD: SearchStringAndReplace() failed for <%s>!",pclSubs);
					ilError=TRUE;
				}
				else
				{
					ilError=FALSE;
					dbg(DEBUG,"WRD: Replaced <%s> with <%s> for <%s>",pclSubs,pclData,pclField);
				}
			}
		}
		if (ilError == TRUE)
		{
			dbg(TRACE,"WRD: failed to replace at least one substitute! Can't continue!");
  		strcpy(pcgErrMsg,"ERROR: substitute of pattern inside selection failed");
		}
		else
		{
			dbg(DEBUG,"WRD: SELECTION :<%s>",pcpSel);
			pcgDataArea[0] = 0x00;
			sprintf(pcgDataArea,"%s",pcgDatLst);
			dbg(DEBUG,"WRD: DATA-AREA :<%s>",pcgDataArea);
		}
	}

	/**********************************************/
	/* Now prepare selection from old-style input */
	/**********************************************/
	if (ilUseRealSqlSelect == FALSE)
	{
		pclSelPtr = SkipChars(pcpSel," ");
		pclSelPtr = SkipWords(pclSelPtr,"WHERE ");
		pclPtr = pclSelPtr;
		pclSelPtr = SkipWords(pclSelPtr,"[SQL]");
		if (pclPtr != pclSelPtr)
		{
			 pclSelPtr = SkipChars(pclSelPtr," ");
			 if (strlen(pclSelPtr) > 0)
			 {
					dbg(DEBUG,"WRD: DEFINED WHERECLAUSE <%s>",pclSelPtr);
					sprintf(pcgRcvSelKey,"WHERE %s",pclSelPtr);
					/*strcpy(pcgDataArea,pcpDat);*/
					strcpy(pcgDataArea,pcgDatLst);
					ilError = FALSE;
			 } /* end if */
		} /* end if */
		else
		{
			 pclSelPtr = SkipChars(pclSelPtr," ");
			 if (strlen(pclSelPtr) > 0)
			 {
					dbg(DEBUG,"WRD: BUILD WHERECLAUSE <%s>",pclSelPtr);
					StrgPutStrg(pcgRcvSelKey,&ilSelPos,"WHERE",0,-1," ");
					strcat(pclShmUpdateSel,"WHERE ");
					ilItmCnt = field_count(pclSelPtr);
					for (ilItm = 1; ilItm <= ilItmCnt; ilItm++)
					{
						 get_real_item(pclFldNam,pclSelPtr,ilItm);
						 /*ilFld = get_item_no(pcpFld,pclFldNam,5) + 1;*/
						 ilFld = get_item_no(pcgFldLst,pclFldNam,5) + 1;
						 if (ilFld > 0)
						 {
								/*get_real_item(pclFldDat,pcpDat,-ilFld);*/
								get_real_item(pclFldDat,pcgDatLst,-ilFld);
								StrgPutStrg(pcgRcvSelKey,&ilSelPos,pclFldNam,0,-1,"=:V");
								StrgPutStrg(pcgRcvSelKey,&ilSelPos,pclFldNam,0,-1," ");
								strcat(pclShmUpdateSel,pclFldNam);
								strcat(pclShmUpdateSel,"='");
								strcat(pclShmUpdateSel,pclFldDat);
								strcat(pclShmUpdateSel,"' ");
								if (ilItm < ilItmCnt)
								{
									 StrgPutStrg(pcgRcvSelKey,&ilSelPos,"AND",0,-1," ");
									strcat(pclShmUpdateSel,"AND ");
								} /* end if */
								StrgPutStrg(pcgRcvDatBlk,&ilDatPos,pclFldDat,0,-1,",");
						 } /* end if */
						 else
						 {
								dbg(TRACE,"WRD: KEY FIELD <%s> NOT IN LIST",pclFldNam);
								sprintf(pcgErrMsg,"COMMAND SYNTAX ERROR <%s>",pclFldNam);
								ilError = TRUE;
						 } /* end else */
					} /* end for */
					if (ilDatPos > 0)
					{
						 ilDatPos--;
					} /* end if */
					pcgRcvSelKey[ilSelPos] = 0x00;
					pcgRcvDatBlk[ilDatPos] = 0x00;
					dbg(DEBUG,"WRD: RESULT SHM-KEY <%s>",pclShmUpdateSel);
					dbg(DEBUG,"WRD: RESULT DB -KEY <%s>",pcgRcvSelKey);
					dbg(DEBUG,"WRD: RESULT DB -DAT <%s>",pcgRcvDatBlk);
					strcpy(pcgActSelKey,pcgRcvSelKey);
					strcpy(pcgActDatBlk,pcgRcvDatBlk);

					/* cleaning data-area once again , because it's only a dummy buffer for */
					pcgDataArea[0] = 0x00;
					sprintf(pcgDataArea,"%s,%s",pcgDatLst,pcgRcvDatBlk);
		 			dbg(DEBUG,"WRD: DATA-AREA :<%s>",pcgDataArea);
					ilError = FALSE;
			 } /* end if */
		} /* end else */
	}

	/**************************/
	/* Now try to update data */
	/**************************/
  if (ilError == FALSE)
  {
     strcpy(pclActCmd,"URT");
		 dbg(DEBUG,"WRD: FIELDLIST :<%s>",pcgFldLst);
		 if (ilUseRealSqlSelect == TRUE)
		 {
     	 BuildSqlFldStrg(pcgRcvFldLst,pcgFldLst,FOR_UPDATE);
			 CheckWhereClause(TRUE, pcpSel, FALSE, TRUE, "\0");
    	 sprintf(pcgSqlBuf,"UPDATE %s SET %s %s ",pcpTbl,pcgRcvFldLst,pcpSel);
		 }
		 else
		 {
     	 strcpy(pclFldLst,pcgFldLst);
     	 BuildSqlFldStrg(pcgRcvFldLst,pcgFldLst,FOR_UPDATE);
			 CheckWhereClause(TRUE, pcgRcvSelKey, FALSE, TRUE, "\0");
			 CheckWhereClause(TRUE, pclShmUpdateSel, FALSE, TRUE, "\0");
			sprintf(pcgSqlBuf,"UPDATE %s SET %s %s ",pcpTbl,pcgRcvFldLst,pcgRcvSelKey);
		 }
		 dbg(DEBUG,"WRD: SQL :<%s>",pcgSqlBuf);
     dbg(DEBUG,"WRD: DATA:<%s>",pcgDataArea);
     strcpy(pcgRcvDatBlk,pcgDatLst);
     delton(pcgDataArea);
     slCursor = 0;
     slFkt=START;
     ilGetRc = sql_if(slFkt,&slCursor,pcgSqlBuf,pcgDataArea);
     if (ilGetRc != RC_SUCCESS)
     {
        GetOraErr(ilGetRc,pcgErrMsg,pcgSqlBuf,"WriteRecodData");
     } /* end if */
		 else
		 {
			  commit_work();
			  close_my_cursor(&slCursor);
        /* JIM 20051102: PRF 7875 / 7921: read URNO also when table is not in SHM for BC */

				dbg(DEBUG,"WRD: Update via WRD-command successfull! Now rereading URNO for BC!");
				/* gettting the corresponding URNO for BC */
				*pcgSqlBuf=0x00;
				*pcgDataArea=0x00;
				if (ilUseRealSqlSelect == TRUE)
				{
     			sprintf(pcgSqlBuf,"SELECT URNO FROM %s %s ",pcpTbl,pcpSel);
				}
		 		else
		 		{
     			sprintf(pcgSqlBuf,"SELECT URNO FROM %s %s ",pcpTbl,pclShmUpdateSel);
				}
				dbg(DEBUG,"WRD: SQL <%s>",pcgSqlBuf);
				ilGetRcShm = sql_if(slFkt,&slCursor,pcgSqlBuf,pcgDataArea);
				if (ilGetRcShm != RC_SUCCESS)
				{
					GetOraErr(ilGetRcShm,pcgErrMsg,pcgSqlBuf,"WriteRecodData");
					dbg(TRACE,"WRD: unable to update SHM via WRD-command! Inconsistent SHM for table <%s>!",pcpTbl);
				} /* end if */
				else
				{
        	(void) BuildItemBuffer(pcgDataArea,"",1,",");
          /* JIM 20051102: 	*pclTmpUrno=0x00; ... done at entry */
        	ilLen = get_real_item(pclTmpUrno,pcgDataArea,1);
          tool_filter_spaces(pclTmpUrno);
          if (strstr(pcgTabsInSHM,pcpTbl) != NULL)
          {
  		  		dbg(DEBUG,"WRD: Now updating SHM!");
  		  		clCmdId='U';
  		  		dbg(DEBUG,"WRD: Calling SYSLIB UPDATE: TAB <%s> FKT <%c> URNO <%s>",pcpTbl,clCmdId,pclTmpUrno);
  		  		tool_filter_spaces(pclTmpUrno);
  		  		ilTmpUrno=atoi(pclTmpUrno);
  		  		CheckUrnoValue(pcpTbl,pclTmpUrno,ilTmpUrno);
  		  		rc=syslibUpdateDbData(clCmdId,pcpTbl,"URNO",pclTmpUrno);
  		  		if (rc != RC_SUCCESS)
  		  		{
  		  		  dbg(TRACE,"WRD: Error returned from syslibUpdateDbData with rc = <%d>",rc);
  		  		  rc=syslibReloadDbData(pcpTbl);
  		  		  dbg(TRACE,"WRD: Reloading Table %s rc = <%d>", pcpTbl,rc);
  		  		  rc = RC_SUCCESS; /* bad thing, but what shall we do? */
  		  		} /* end if */
	  	    }
	  	    else
	  	    {
			      dbg(DEBUG,"WRD: Update via WRD-command successfull! (No SHM update)");
	  	    }
  			}
		 }
     commit_work();
     close_my_cursor(&slCursor);
		 /************************************************/
		 /* Now inserting data because update has failed */
		 /************************************************/
     if (ilGetRc != RC_SUCCESS)
     {
        if (ilGetRc == SQL_NOTFOUND)
        {
          dbg(TRACE,"WRD: ---- DATA FOR SQL-UPDATE NOT FOUND. SO INSERT DATA! ----");
					if (igCurTabIdx >= 0)
					{
						rc = CheckCDAT('I',prgTabDescr[igCurTabIdx].cgCdatType,
													 pcgFldLst,pcgDataArea,pcgDatLst);
						rc = CheckUSEC('I',prgTabDescr[igCurTabIdx].cgUsecType,
													 pcgFldLst,pcgDataArea,pcgDatLst);
						rc = CheckLSTU('I',prgTabDescr[igCurTabIdx].cgLstuType,
													 pcgFldLst,pcgDataArea,pcgDatLst);
						rc = CheckUSEU('I',prgTabDescr[igCurTabIdx].cgUseuType,
													 pcgFldLst,pcgDataArea,pcgDatLst);
					 }
           strcpy(pcgDataArea,pcgDatLst);
           if (igAddHopo == TRUE)
           {
              if (strstr(pcgFldLst,"HOPO") == NULL)
              {
                 strcat(pcgFldLst,",HOPO");
                 strcat(pcgDataArea,",");
                 strcat(pcgDataArea,pcgH3LC);
              } /* end if */
           } /* end if */
           if (strstr(pcgFldLst,"URNO") == NULL)
           {
              strcpy(pclActCmd,"IRT");

               /* UFIS-1485 */
              if (strcmp(pcgH3LC, "ATH")==0 ) {
                 if (igCurTabIdx < 0)
                 {
                        GetTabDescrIdx("GMU",pcpTbl);
                 }
                 if (igCurTabIdx >= 0)
                 {
                     ilGetRc = GetNextNumbers(prgTabDescr[igCurTabIdx].UrnoRangeKey,pclNewUrno,1,"GNV");
                 } else {
                     ilGetRc = GetNextValues(pclNewUrno, 1);
                 }
              } else {
                  ilGetRc = GetNextValues(pclNewUrno, 1);
              }

              strcat(pcgFldLst,",URNO");
              strcat(pcgDataArea,",");
              strcat(pcgDataArea,pclNewUrno);
           } /* end if */
           else
           {
              strcpy(pclActCmd,"IRT");
           } /* end else */

           BuildSqlFldStrg(pcgRcvFldLst,pcgFldLst,FOR_INSERT);
           sprintf(pcgSqlBuf,"INSERT INTO %s %s", pcpTbl,pcgRcvFldLst);
           dbg(DEBUG,"WRD: SQL :<%s>",pcgSqlBuf);
           dbg(DEBUG,"WRD: DATA:<%s>",pcgDataArea);
           strcpy(pcgRcvDatBlk,pcgDataArea);
           delton(pcgDataArea);
           slCursor = 0;
           slFkt=START;
           ilGetRc = sql_if(slFkt,&slCursor,pcgSqlBuf,pcgDataArea);
           if (ilGetRc != RC_SUCCESS)
           {
              GetOraErr(ilGetRc,pcgDataArea,pcgSqlBuf,"WriteRecordData");
           } /* end if */
					else
					{
            if (strstr(pcgTabsInSHM,pcpTbl) != NULL)
             {
            /* JIM 20051102: 	*pclTmpUrno=0x00; ... done at entry */
						strcpy(pclTmpUrno,pclNewUrno);
						tool_filter_spaces(pclTmpUrno);
						clCmdId='I';
						dbg(DEBUG,"WRD: Calling SYSLIB UPDATE: TAB <%s> FKT <%c> URNO <%s>",pcpTbl,clCmdId,pclTmpUrno);
						tool_filter_spaces(pclTmpUrno);
						ilTmpUrno=atoi(pclTmpUrno);
						CheckUrnoValue(pcpTbl,pclTmpUrno,ilTmpUrno);
						rc=syslibUpdateDbData(clCmdId,pcpTbl,"URNO",pclTmpUrno);
						if (rc != RC_SUCCESS)
						{
							dbg(TRACE,"WRD: Error returned from syslibUpdateDbData with rc = <%d>",rc);
							rc=syslibReloadDbData(pcpTbl);
							dbg(TRACE,"WRD: Reloading Table %s rc = <%d>",
									pcpTbl,rc);
							rc = RC_SUCCESS; /* bad thing, but what shall we do? */
						} /* end if */
					}
					}
           commit_work();
           close_my_cursor(&slCursor);
        } /* end if */
     } /* end if */
		 /******************************************/
		 /* Now broadcasting, send to action , etc.*/
		 /******************************************/
     if (ilGetRc == RC_SUCCESS)
     {
			 if (ilUseRealSqlSelect == TRUE)
			 {
				 dbg(TRACE,"WRD: RESULT SEL. <%s>",pcpSel);
			 }
			 else
			 {
				 dbg(TRACE,"WRD: RESULT SEL. <%s>",pcgRcvSelKey);
			 }
			 dbg(TRACE,"WRD: RESULT FLD. <%s>",pcgFldLst);
			 dbg(TRACE,"WRD: RESULT DAT. <%s>",pcgRcvDatBlk);
			 dbg(TRACE,"WRD: RESULT URNO <%s>",pclTmpUrno);
			 dbg(DEBUG,"WRD: sending info to <BCHDL,ACTION,LOGHDL,ORIGINATOR>");
			 dbg(DEBUG,"WRD: OLD-DATA forward to <ACTION> IS NOT SUPPORTED!");
       strcpy(pcgActDatBlk,pclTmpUrno);
       if (strcmp(pclActCmd,"URT") == 0)
       {
         sprintf(pcgRcvSelKey,"WHERE URNO=%s",pclTmpUrno);
       } /* end if */
       else
       {
          strcpy(pcgRcvSelKey,pclTmpUrno);
       } /* end else */
       delton(pcgRcvDatBlk);
       BuildItemBuffer(pcgRcvDatBlk,pcgFldLst,0,",");
       if (gl_no_ack != TRUE)
       {
           (void) tools_send_info_flag_perf(que_out,0, pcgDestName, "", pcgRecvName,
                                            "", "", pcgTwStart, pcgTwEnd,
                                            pclActCmd,pcpTbl,pcgRcvSelKey,pcgFldLst,
                                            pcgRcvDatBlk,0);
        } /* end if */
        (void) tools_send_info_flag_perf(igToBcHdl,0, pcgDestName, "", pcgRecvName,
                                         "", "", pcgTwStart, pcgTwEnd,
                                         pclActCmd,pcpTbl,pcgRcvSelKey,pcgFldLst,
                                         pcgRcvDatBlk,0);
				pcgOldData[0]=0x00;
        /* Sending to Action (not on mod_id+1) */
        (void) ReleaseActionInfo(pcgActionRoute,pcpTbl,pclActCmd,
									 pcgActDatBlk,pcgRcvSelKey,pcgFldLst,
                                 pcgRcvDatBlk,pcgOldData);

        /* Sending to LOGHDL,if it is existing */
				if (atoi(pcgOutRoute) > 0)
				{
        	(void) ReleaseActionInfo(pcgOutRoute,pcpTbl,pclActCmd,
									 pcgActDatBlk,pcgRcvSelKey,pcgFldLst,
									 pcgRcvDatBlk,pcgOldData);
			  }
				ilError=FALSE;
     } /* end if */
     else
     {
        dbg(TRACE,"WRD: ORACLE ERROR !!");
        ilError = TRUE;
     } /* end else */
  } /* end if */

  if (ilError == TRUE)
  {
     dbg(TRACE,"WRD: ERR MSG <%s>",pcgErrMsg);
     if (gl_no_ack != TRUE)
     {
        tools_send_sql_rc_perf(que_out,"WRD","", "","",pcgErrMsg,RC_FAIL);
     } /* end if */
		 ilRC = RC_FAIL;
  } /* end if */
  dbg(TRACE,"WRD: ---------------- <END> RC=<%d> ---------",ilRC);

  return ilRC;
} /* end WriteRecordData */

/* *******************************************************/
/* *******************************************************/
static char *SkipChars(char *pcpStrg, char *pcpPattern)
{
  char *pclPtr = NULL;
  pclPtr = pcpStrg;
  while (*pclPtr == pcpPattern[0])
  {
     pclPtr++;
  } /* end while */

  return pclPtr;
} /* end SkipChars */

/* *******************************************************/
/* *******************************************************/
static char *SkipWords(char *pcpStrg, char *pcpPattern)
{
  char *pclPtr = NULL;
  pclPtr = strstr(pcpStrg,pcpPattern);
  if (pclPtr != NULL)
  {
     pclPtr += strlen(pcpPattern);
  } /* end if */
  else
  {
     pclPtr = pcpStrg;
  } /* end else */

  return pclPtr;
} /* end SkipChars */


/********************************************************/
/********************************************************/
static int ReleaseActionInfo(char *pcpRoute, char *pcpTbl, char *pcpCmd,
                             char *pcpUrnoList, char *pcpSel, char *pcpFld,
                             char *pcpDat,char *pcpOldDat)
{
  int ilRC = RC_SUCCESS;
  int ilSelLen = 0;
  int ilSelPos = 0;
  int ilDatLen = 0;
  int ilDatPos = 0;
  int ilRouteItm = 0;
  int ilItmCnt = 0;
  int ilActRoute = 0;
  char pclRouteNbr[8];
  char *pclOutSel = NULL;
  char *pclOutDat = NULL;

  dbg(DEBUG,"ACTION-INFORMATION");
  dbg(DEBUG," TBL <%s>",pcpTbl);
  dbg(DEBUG," CMD <%s>",pcpCmd);
  dbg(DEBUG," URN <%s>",pcpUrnoList);
  dbg(DEBUG," USR <%s>",pcgDestName);
  dbg(DEBUG," SEL <%s>",pcpSel);
  dbg(DEBUG," FLD <%s>",pcpFld);
  dbg(DEBUG," DAT <%s>",pcpDat);
  dbg(DEBUG," OLD <%s>",pcpOldDat);
  dbg(DEBUG," To  <%s>",pcpRoute);

  ilSelLen = strlen(pcpUrnoList) + strlen(pcpSel) + 32;
  pclOutSel = (char *) malloc(ilSelLen);
  ilDatLen = strlen(pcpDat) + strlen(pcpOldDat) + 32;
  pclOutDat = (char *) malloc(ilDatLen);
  if ((pclOutSel != NULL) && (pclOutDat != NULL))
  {
     StrgPutStrg(pclOutSel,&ilSelPos,pcpSel,0,-1," \n");
     StrgPutStrg(pclOutSel,&ilSelPos,pcpUrnoList,0,-1,"\n");
     if (ilSelPos > 0)
     {
        ilSelPos--;
     } /* end if */
     pclOutSel[ilSelPos] = 0x00;
     StrgPutStrg(pclOutDat,&ilDatPos,pcpDat,0,-1," \n");
     StrgPutStrg(pclOutDat,&ilDatPos,pcpOldDat,0,-1,"\n");
     if (ilDatPos > 0)
     {
        ilDatPos--;
     } /* end if */
     pclOutDat[ilDatPos] = 0x00;
     ilItmCnt = field_count(pcpRoute);
     for (ilRouteItm = 1; ilRouteItm <= ilItmCnt; ilRouteItm++)
     {
        (void) get_real_item(pclRouteNbr,pcpRoute,ilRouteItm);
        ilActRoute = atoi(pclRouteNbr);
        if (ilActRoute > 0)
        {
           (void) tools_send_info_flag_perf(ilActRoute,0, pcgDestName, "", pcgRecvName,
                                            "", "", pcgTwStart, pcgTwEnd,
                                            pcpCmd,pcpTbl,pclOutSel,pcpFld,pclOutDat,0);
        } /* end if */
     } /* end for */
     free(pclOutSel);
     free(pclOutDat);
  } /* end if */
  else
  {
     debug_level = TRACE;
     dbg(TRACE,"ERROR: ALLOC %d/%d BYTES FOR ACTION INFO",ilSelLen,ilDatLen);
     exit(0);
  } /* end else */
  pclOutSel = NULL;
  pclOutDat = NULL;

  return ilRC;
} /* end ReleaseActionInfo() */


/**************************************************************/
/**************************************************************/
static int GetOutModId(char *pcpProcName)
{
  int ilQueId = -1;
  char pclCfgCode[32];
  char pclCfgValu[32];

  sprintf(pclCfgCode,"QUE_TO_%s",pcpProcName);
  ReadCfg(pcgCfgFile,pclCfgValu,"QUEUE_DEFINES",pclCfgCode,"-1");
  ilQueId = atoi(pclCfgValu);
  if (ilQueId < 0)
  {
     ilQueId = tool_get_q_id(pcpProcName);
  } /* end if */
  if (ilQueId < 0)
  {
     ilQueId = 0;
  } /* end if */

  return ilQueId;
} /* end of GetOutModId */


/* *******************************************************/
/* *******************************************************/
static int ReadCfg(char *pcpFile, char *pcpVar, char *pcpSection,
                   char *pcpEntry, char *pcpDefVar)
{
  int ilRC = RC_SUCCESS;

  ilRC = iGetConfigRow(pcpFile,pcpSection,pcpEntry,CFG_STRING,pcpVar);
  if (ilRC != RC_SUCCESS || strlen(pcpVar) <= 0)
  {
     strcpy(pcpVar,pcpDefVar);
     ilRC = RC_FAIL;
  } /* end if */

  return ilRC;
} /* end ReadCfg */


/********************************************************/
/********************************************************/
static int BuildSqlFldStrg(char *pcpSqlStrg, char *pcpFldLst, int ipTyp)
{
  int ilRC = RC_SUCCESS;
  int ilFldCnt = 0;
  int ilFld = 0;
  int ilStrLen = 0;
  char pclFldNam[8];
  char pclFldVal[16];
  char pclTmpBuf3[4096];

  pcpSqlStrg[0] = 0x00;
  pclTmpBuf3[0] = 0x00;
  ilFldCnt = field_count(pcpFldLst);
  switch (ipTyp)
  {
     case FOR_UPDATE:
        ilStrLen = 0;
        for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
        {
           (void)get_real_item(pclFldNam, pcpFldLst, ilFld);
           sprintf(pclFldVal,"%s=:V%s",pclFldNam,pclFldNam);
           StrgPutStrg(pcpSqlStrg, &ilStrLen, pclFldVal, 0, -1, ",");
        } /* end for */
        if (ilStrLen > 0)
        {
           ilStrLen--;
           pcpSqlStrg[ilStrLen] = 0x00;
        } /* end if */
        break;
     case FOR_INSERT:
        ilStrLen = 0;
        for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
        {
           (void)get_real_item(pclFldNam, pcpFldLst, ilFld);
           sprintf(pclFldVal,":V%s",pclFldNam);
           StrgPutStrg(pclTmpBuf3, &ilStrLen, pclFldVal, 0, -1, ",");
        } /* end for */
        if (ilStrLen > 0)
        {
           ilStrLen--;
           pclTmpBuf3[ilStrLen] = 0x00;
           sprintf(pcpSqlStrg,"(%s) VALUES (%s)",pcpFldLst,pclTmpBuf3);
        } /* end if */
        break;
     default:
        break;
  } /* end switch */
  /* dbg(TRACE,"SQL STRING <%s>",pcpSqlStrg); */

  return ilRC;
} /* end BuildSqlFldStrg */

/*******************************************************/
static void OutFieldsToSelFields(char *pcpTbl, char *pcpFld)
{
  int ilGetRc = RC_SUCCESS;
  int ilUrnoPos = 0;
  int ilTbl = 0;
  int ilFound = FALSE;

  for (ilTbl = 1; ((ilTbl <= prgOutFldLst[0].Count) && (ilFound == FALSE));
       ilTbl++)
  {
     if (strcmp(prgOutFldLst[ilTbl].Table,pcpTbl) == 0)
     {
        ilFound = TRUE;
        dbg(TRACE,"TABLE <%s> OUT <%s>",prgOutFldLst[ilTbl].Table,
            prgOutFldLst[ilTbl].Fields);
        BuildOutFieldList(pcpFld,prgOutFldLst[ilTbl].Fields);
     } /* end if */
  } /* end for */
  if (ilFound == FALSE)
  {
     strcpy(pcgAllOutFld,pcpFld);
  } /* end if */
  ilUrnoPos= get_item_no(pcgAllOutFld,"URNO",5) + 1;
  if (ilUrnoPos < 1 && prgTabDescr[igCurTabIdx].cgUrnoType != 'M')
  {
     strcat(pcgAllOutFld,",URNO");
  }
}

/*******************************************************/
static void ReadOldDataPerRecord(char *pcpTbl, char *pcpFld, char *pcpDat, char *pcpSel)
{
  int ilGetRc = RC_SUCCESS;
  int ilTbl = 0;
  int ilFound = FALSE;
  short slCursor = 0;

  sprintf(pcgSqlRead,"SELECT %s FROM %s %s", pcgAllOutFld,pcpTbl,pcpSel);
  dbg(TRACE,"<%s>",pcgSqlRead);
  ilGetRc = sql_if(START, &slCursor, pcgSqlRead, pcgOldData);
  if (ilGetRc == RC_SUCCESS)
  {
     (void) BuildItemBuffer(pcgOldData,pcgAllOutFld,0,",");
  } /* end if */
  else
  {
     strcpy(pcgOldData,"\0");
  } /* end else */
  close_my_cursor(&slCursor);
  if (ilFound == TRUE)
  {
     BuildNewData(pcpFld,pcpDat);
  } /* end if */
  else
  {
     strcpy(pcgNewData,pcpDat);
  } /* end else */
  sprintf(pcgNewAndOldData,"%s\n%s",pcgNewData,pcgOldData);

  return;
} /* end ReadOldDataPerRecord */

/*******************************************************/
static void ReadOldData(char *pcpTbl, char *pcpFld, char *pcpDat, char *pcpSel)
{
  int ilGetRc = RC_SUCCESS;
  int ilTbl = 0;
  int ilFound = FALSE;
  short slCursor = 0;

  for (ilTbl = 1; ((ilTbl <= prgOutFldLst[0].Count) && (ilFound == FALSE));
       ilTbl++)
  {
     if (strcmp(prgOutFldLst[ilTbl].Table,pcpTbl) == 0)
     {
        ilFound = TRUE;
        dbg(TRACE,"TABLE <%s> OUT <%s>",prgOutFldLst[ilTbl].Table,
            prgOutFldLst[ilTbl].Fields);
        BuildOutFieldList(pcpFld,prgOutFldLst[ilTbl].Fields);
     } /* end if */
  } /* end for */
  if (ilFound == FALSE)
  {
     strcpy(pcgAllOutFld,pcpFld);
  } /* end if */
  sprintf(pcgSqlRead,"SELECT %s FROM %s %s", pcgAllOutFld,pcpTbl,pcpSel);
  dbg(TRACE,"<%s>",pcgSqlRead);
  ilGetRc = sql_if(START, &slCursor, pcgSqlRead, pcgOldData);
  if (ilGetRc == RC_SUCCESS)
  {
     (void) BuildItemBuffer(pcgOldData,pcgAllOutFld,0,",");
  } /* end if */
  else
  {
     strcpy(pcgOldData,"\0");
  } /* end else */
  close_my_cursor(&slCursor);
  if (ilFound == TRUE)
  {
     BuildNewData(pcpFld,pcpDat);
  } /* end if */
  else
  {
     strcpy(pcgNewData,pcpDat);
  } /* end else */
  sprintf(pcgNewAndOldData,"%s\n%s",pcgNewData,pcgOldData);

  return;
} /* end ReadOldData */

/*******************************************************/
/*******************************************************/
static void BuildOutFieldList(char *pcpRcvFld, char *pcpOutFld)
{
  int ilFldCnt = 0;
  int ilFld = 0;
  char pclFldNam[8];

  strcpy(pcgAllOutFld,pcpOutFld);
  ilFldCnt = field_count(pcpRcvFld);
  for (ilFld=1; ilFld<=ilFldCnt; ilFld++)
  {
     get_real_item(pclFldNam,pcpRcvFld,ilFld);
     if (strstr(pcgAllOutFld,pclFldNam) == NULL)
     {
        strcat(pcgAllOutFld,",");
        strcat(pcgAllOutFld,pclFldNam);
     } /* end if */
  } /* end for */

  return;
} /* end BuildOutFieldList */

/*******************************************************/
/*******************************************************/
static void BuildNewData(char *pcpRcvFld, char *pcpRcvDat)
{
  int ilFldCnt = 0;
  int ilRcvFld = 0;
  int ilOutFld = 0;
  int ilOldPos = 0;
  int ilNewPos = 0;
  int ilLen = 0;
  char pclFldNam[8];
  char pclNewVal[4096];  /* 20050308 JIM: PRF 7053 modified from 2048 to 4096 */
  char pclOldVal[4096];  /* 20050308 JIM: PRF 7053 modified from 2048 to 4096 */

  strcpy(pcgSveData,pcgOldData);
  ilFldCnt = field_count(pcgAllOutFld);
  for (ilOutFld=1; ilOutFld<=ilFldCnt; ilOutFld++)
  {
     get_real_item(pclFldNam,pcgAllOutFld,ilOutFld);
     ilLen = get_real_item(pclOldVal,pcgSveData,ilOutFld);
     if (ilLen < 1)
     {
        strcpy(pclOldVal," ");
     } /* end if */
     StrgPutStrg(pcgOldData,&ilOldPos,pclOldVal,0,-1,",");
     ilRcvFld = get_item_no(pcpRcvFld,pclFldNam,5) + 1;
     if (ilRcvFld > 0)
     {
        ilLen = get_real_item(pclNewVal,pcpRcvDat,ilRcvFld);
     } /* end if */
     else
     {
        ilLen = get_real_item(pclNewVal,pcgSveData,ilOutFld);
     } /* end else */
     if (ilLen < 1)
     {
        strcpy(pclNewVal," ");
     } /* end if */
     StrgPutStrg(pcgNewData,&ilNewPos,pclNewVal,0,-1,",");
  } /* end for */
  if (ilNewPos > 0)
  {
     ilNewPos--;
  } /* end if */
  if (ilOldPos > 0)
  {
     ilOldPos--;
  } /* end if */
  pcgNewData[ilNewPos] = 0x00;
  pcgOldData[ilOldPos] = 0x00;

  return;
} /* end BuildNewData */


/*******************************************************/
/*******************************************************/
static void GetLogFileMode(int *ipModeLevel, char *pcpLine, char *pcpDefault)
{
  int ilRC = RC_SUCCESS;
  char pclTmp[32];

  ReadCfg(pcgCfgFile,pclTmp,"SYSTEM",pcpLine,pcpDefault);
  if (strcmp(pclTmp,"TRACE")==0)
  {
     *ipModeLevel = TRACE;
  }
  else if(strcmp(pclTmp,"DEBUG")==0)
  {
     *ipModeLevel = DEBUG;
  } /* end else if */
  else
  {
     *ipModeLevel = 0;
  } /* end else */

  return;
} /* end GetLogFileMode */

/* *******************************************************/
/* *******************************************************/
static void InitOutFieldList(void)
{
  int ilCnt = 0;
  int ilItmCnt = 0;
  int ilItm = 0;
  int ilLen = 0;
  char pclTmpName[64];
  char pclKeyName[64];
  char pclTmpLine[512];
  char pclKeyLine[512];

  ilCnt = 0;
  ReadCfg(pcgCfgFile,pclTmpLine,"ACTION_INFO","TABLE_CFG","\0");
  ilItmCnt = field_count(pclTmpLine);
  for (ilItm = 1; ilItm <= ilItmCnt; ilItm++)
  {
     ilLen = get_real_item(pclTmpName,pclTmpLine,ilItm);
     if (ilLen > 0)
     {
        ilCnt++;
        sprintf(prgOutFldLst[ilCnt].Table, "%s%s",pclTmpName,pcgTblExt);
        sprintf(pclKeyName,"%s_OUT",pclTmpName);
        ReadCfg(pcgCfgFile,prgOutFldLst[ilCnt].Fields, "ACTION_INFO",
                pclKeyName,"\0");
        dbg(TRACE,"<%s> OUT <%s>",prgOutFldLst[ilCnt].Table,
            prgOutFldLst[ilCnt].Fields);
     } /* end if */
  } /* end for */
  prgOutFldLst[0].Count = ilCnt;
  dbg(TRACE,"OUT TABLE CFG'S = %d",ilCnt);

  return;
} /* end InitOutFieldList */

/* ******************************************************************** */
/* trim_quotes: remove \' or ' ' from data area                         */
/* ******************************************************************** */
static void trim_quotes (char *pcpData,char cpQuote)
{
  int i=0, j=0;

  if (pcpData != NULL)
  {
     for (i=0, j=0; pcpData[i] != 0; i++)
     {
        if (pcpData[i]!=cpQuote)
        {
           pcpData[j] = pcpData[i];
           j++;
        } /* fi */
     } /* for */
     pcpData[j] = 0;
  } /* pcpData != NULL */
} /* trim_quotes */


static int new_tools_send_sql_perf(int ipRouteId, BC_HEAD *prpBchd, CMDBLK *prpCmdblk,
                                   char *pcpFields, char *pcpData)
{
  int ilRC = RC_SUCCESS;
  char pclRouteId[16];

  ilRC = CheckTxTimeout(ipRouteId);
  if (ilRC == RC_SUCCESS)
  {
     sprintf(pclRouteId,"%d",ipRouteId);
     if (strstr(pcgSqlQueueIds,pclRouteId) == NULL)
     {
        ilRC = new_tools_send_sql(ipRouteId,prpBchd,prpCmdblk,pcpFields,pcpData);
        CheckQueueWait(ipRouteId);
     }
  }

  return ilRC;
} /* End of new_tools_send_sql_perf */

static int tools_send_info_flag_perf(int ipRouteId, int ipOrigId, char *pcpDestName,
                                     char *pcpOrigName, char *pcpRecvName, char *pcpSeqId,
                                     char *pcpRefSeqId, char *pcpTwStart, char *pcpTwEnd,
                                     char *pcpCommand, char *pcpTabName, char *pcpSelection,
                                     char *pcpFields, char *pcpData, int ipFlag)
{
  int ilRC = RC_SUCCESS;
  char pclRouteId[16];

  ilRC = CheckTxTimeout(ipRouteId);
  if (ilRC == RC_SUCCESS)
  {
     sprintf(pclRouteId,"%d",ipRouteId);
     if (strstr(pcgSqlQueueIds,pclRouteId) == NULL)
     {
        ilRC = tools_send_info_flag(ipRouteId,ipOrigId,pcpDestName,pcpOrigName,pcpRecvName,
                                    pcpSeqId,pcpRefSeqId,pcpTwStart,pcpTwEnd,pcpCommand,
                                    pcpTabName,pcpSelection,pcpFields,pcpData,ipFlag);
        CheckQueueWait(ipRouteId);
     }
  }

  return ilRC;
} /* End of tools_send_info_flag_perf */

static int tools_send_sql_rc_perf(int ipRouteId, char *pcpCommand, char *pcpTabName,
                                  char *pcpSelection, char *pcpFields, char *pcpData,
                                  int ipRetCode)
{
  int ilRC = RC_SUCCESS;
  char pclRouteId[16];

  ilRC = CheckTxTimeout(ipRouteId);
  if (ilRC == RC_SUCCESS)
  {
     sprintf(pclRouteId,"%d",ipRouteId);
     if (strstr(pcgSqlQueueIds,pclRouteId) == NULL)
     {
        ilRC = tools_send_sql_rc(ipRouteId,pcpCommand,pcpTabName,pcpSelection,pcpFields,
                                 pcpData,ipRetCode);
        CheckQueueWait(ipRouteId);
     }
  }

  return ilRC;
} /* End of tools_send_sql_rc_perf */

static int SaveStartTime(int ipRouteId, char *pcpStartTime)
{
  int ilRC = RC_SUCCESS;
  char pclTxBegin[128];
  char pclTimeOut[128];

  igCheckPerf = FALSE;
  if (ipRouteId >= 20000)
  {
     if (strstr(pcpStartTime,",") != NULL)
     {
        (void) get_real_item(pclTxBegin,pcpStartTime,1);
        (void) get_real_item(pclTimeOut,pcpStartTime,2);
        if ((pclTxBegin[0] >= '0' && pclTxBegin[0] <= '9') &&
            (pclTimeOut[0] >= '0' && pclTimeOut[0] <= '9'))
        {
           igTxBegin = atoi(pclTxBegin);
           igTimeOut = atoi(pclTimeOut);
           igCheckPerf = TRUE;
           dbg(TRACE,"Check Tx Timeout initialized with <%d> <%d>",igTxBegin,igTimeOut);
        }
     }
  }

  return ilRC;
} /* End of SaveStartTime */

static int CheckTxTimeout(int ipRouteId)
{
  int ilRC = RC_SUCCESS;
  time_t tlActSeconds;

  if (igCheckPerf == TRUE)
  {
     if (ipRouteId >= 20000 && igTimeOut > 0)
     {
        tlActSeconds = time(0L);
        tlActSeconds = tlActSeconds % 10000;
        if (tlActSeconds < igTxBegin)
        {
           tlActSeconds += 10000;
        }
        if (igTxBegin + igTimeOut <= tlActSeconds)
        {
           dbg(TRACE,"Transaction Time is > Timeout Value: <%d> > <%d>",
               tlActSeconds-igTxBegin,igTimeOut);
           dbg(TRACE,"Data are not sent to client");
           ilRC = RC_FAIL;
        }
     }
  }

  return ilRC;
} /* End of CheckTxTimeout */

static int GetOraErr(int ipErrCode, char *pcpResultBuffer, char *pcpSqlStatement,
                     char *pcpRoutine)
{
  int ilRC = RC_SUCCESS;
  char *pclTmpPtr;
  int ilDebugLevel;

  strcpy(pcpResultBuffer,"");
  ilRC = get_ora_err(ipErrCode,pcpResultBuffer);
  pclTmpPtr = strstr(pcpResultBuffer,"\r");
  if (pclTmpPtr == NULL)
  {
     pclTmpPtr = strstr(pcpResultBuffer,"\n");
  }
  if (pclTmpPtr != NULL)
  {
     *pclTmpPtr = '\0';
  }
  TrimRight(pcpResultBuffer);
  ilDebugLevel = debug_level;
  debug_level = TRACE;
  dbg(TRACE,"%s: \nOracle Error: <%s>\nSql Statement:\n<%s>",
      pcpRoutine,pcpResultBuffer,pcpSqlStatement);
  debug_level = ilDebugLevel;

  return ilRC;
} /* End of GetOraErr */

static void TrimRight(char *pcpBuffer)
{
  char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];

  if (strlen(pcpBuffer) == 0)
  {
     strcpy(pcpBuffer," ");
  }
  else
  {
     while (isspace(*pclBlank) && pclBlank != pcpBuffer)
     {
        *pclBlank = '\0';
        pclBlank--;
     }
  }

} /* End of TrimRight */


/* ========================================================== */
/* ========================================================== */
static void GetTabDescrIdx(char *pcpCommand, char *pcpTabName)
{
  int ilRCdb = DB_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  int ilI;
  int ilFound = FALSE;
  int ilLen;
  char pclFieldName[8];
  char pclFieldValue[256];

  if (strcmp(pcgH3LC, "ATH")==0 && strcmp(pcpCommand, "GMU")==0 ) {
      dbg(TRACE,"GetTabDescrIdx : Before pcgH3LC <%s>, pclSelection <%s> ",pcgH3LC,pcpTabName);
      if (strcmp(pcgClientAppl, "CDID")==0)  {
         strcpy(pcpTabName,"DCFTAB");
         dbg(TRACE,"GetTabDescrIdx :1  pclSelection <%s> ",pcpTabName);
      }

      if (strcmp(pcgClientAppl, "LoadTabVie")==0) {
        strcpy(pcpTabName,"LOATAB");
        dbg(TRACE,"GetTabDescrIdx :2  pclSelection <%s> ",pcpTabName);
      }


        if (strcmp(pcgClientAppl, "CuteIF")==0 || strcmp(pcpTabName,"FLGTAB")==0
         || strcmp(pcpTabName,"FXTTAB")==0 || strcmp(pcpTabName,"FLZTAB")==0
         || strcmp(pcpTabName,"FLDTAB")==0
         )
      {

        strcpy(pcpTabName,"FLZTAB");
        dbg(TRACE,"GetTabDescrIdx :3  pclSelection <%s> ",pcpTabName);
      }
      dbg(TRACE,"GetTabDescrIdx : final pcgH3LC <%s>, pclSelection <%s> ",pcgH3LC,pcpTabName);
  }

  if (strlen(pcpTabName) == 0)
  {
     igCurTabIdx = -1;
     dbg(TRACE,"GetTabDescrIdx: Table name is empty, index = %d",igCurTabIdx);
     return;
  }

  /* 20051219 JIM: avoid memory schmier in GetTabDescrIdx
  * misuse of pclFieldName for finding ",command,"
  */
  memset(pclFieldName,0,8);
  pclFieldName[0]=',';
  strncat(pclFieldName,pcpCommand,6);
  strcat(pclFieldName,",");
  if (strstr(pcgNoSqlWriteCmds,pclFieldName)!=NULL)
  {
     igCurTabIdx = -1;
     dbg(TRACE,"WRITE CMDS <%s>",pcgNoSqlWriteCmds);
     dbg(TRACE,"GetTabDescrIdx: no write cmd");
     return;
  }
  memset(pclFieldName,0,8); /* end misuse */

  for (ilI = 0; ilI < igMaxTabIdx && ilFound == FALSE; ilI++)
  {
     /* 20051219 JIM: avoid memory schmier in GetTabDescrIdx
    * if (strcmp(&prgTabDescr[ilI].pcgTableName[0],pcpTabName) == 0)
    */
     if (strncmp(&prgTabDescr[ilI].pcgTableName[0],pcpTabName,32) == 0)
     {
        ilFound = TRUE;
        igCurTabIdx = ilI;
     }
  }
  if (ilFound == TRUE)
  {
     dbg(TRACE,"GetTabDescrIdx: Found index %d for table <%s>",igCurTabIdx,pcpTabName);
  }
  else
  {
     igCurTabIdx = igMaxTabIdx;
     if (igMaxTabIdx > 999)
     { /* 20051219 JIM: avoid memory schmier */
        dbg(TRACE,"GetTabDescrIdx: should add Table <%s> to list, but list size exceeded (%d)!",pcpTabName,igCurTabIdx);
        igCurTabIdx = -1;
        return;
     }
     igMaxTabIdx++;
     /* 20051219 JIM: avoid memory schmier in GetTabDescrIdx
    * strcpy(&prgTabDescr[igCurTabIdx].pcgTableName[0],pcpTabName);
    */
     strncpy(&prgTabDescr[igCurTabIdx].pcgTableName[0],pcpTabName,32);
     prgTabDescr[igCurTabIdx].pcgTableName[32]=0;
     if (strlen(pcpTabName)>32)
     {
       dbg(TRACE,"GetTabDescrIdx: Table name truncated to <%s>, may cause error when first 32 chars are duplicate!",
                 prgTabDescr[igCurTabIdx].pcgTableName);
     }
     strcpy(prgTabDescr[igCurTabIdx].UrnoRangeKey,"SNOTAB");

     prgTabDescr[igCurTabIdx].cgUrnoType = 'M';
     prgTabDescr[igCurTabIdx].cgHopoType = 'M';
     prgTabDescr[igCurTabIdx].cgCdatType = 'M';
     prgTabDescr[igCurTabIdx].cgUsecType = 'M';
     prgTabDescr[igCurTabIdx].cgLstuType = 'M';
     prgTabDescr[igCurTabIdx].cgUseuType = 'M';
     sprintf(pcgSqlBuf,"SELECT COLUMN_NAME,DATA_TYPE FROM USER_TAB_COLUMNS "
             "WHERE TABLE_NAME='%s'",pcpTabName);
     slFkt = START;
     slCursor = 0;
     ilRCdb = sql_if(slFkt,&slCursor,pcgSqlBuf,pcgDataArea);
     while (ilRCdb == DB_SUCCESS)
     {
        (void) BuildItemBuffer(pcgDataArea,"",2,",");
        ilLen = get_real_item(pclFieldName,pcgDataArea,1);
        ilLen = get_real_item(pclFieldValue,pcgDataArea,2);
        if (strcmp(pclFieldName,"URNO") == 0)
        {
           prgTabDescr[igCurTabIdx].cgUrnoType = pclFieldValue[0];
        }
        else if (strcmp(pclFieldName,"HOPO") == 0)
        {
           prgTabDescr[igCurTabIdx].cgHopoType = pclFieldValue[0];
        }
        else if (strcmp(pclFieldName,"CDAT") == 0)
        {
           prgTabDescr[igCurTabIdx].cgCdatType = pclFieldValue[0];
        }
        else if (strcmp(pclFieldName,"USEC") == 0)
        {
           prgTabDescr[igCurTabIdx].cgUsecType = pclFieldValue[0];
        }
        else if (strcmp(pclFieldName,"LSTU") == 0)
        {
           prgTabDescr[igCurTabIdx].cgLstuType = pclFieldValue[0];
        }
        else if (strcmp(pclFieldName,"USEU") == 0)
        {
           prgTabDescr[igCurTabIdx].cgUseuType = pclFieldValue[0];
        }
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,pcgSqlBuf,pcgDataArea);
     }
     if (ilRCdb != ORA_NOT_FOUND)
     {
        GetOraErr(ilRCdb,pcgDataArea,pcgSqlBuf,"GetTabDescrIdx");
     }
     close_my_cursor(&slCursor);
     dbg(TRACE,"=================================================");
     dbg(TRACE,"GetTabDescrIdx:");
     dbg(TRACE,"NEW INDEX %d FOR TABLE <%s> WITH %c %c %c %c %c %c",
         igCurTabIdx,pcpTabName,prgTabDescr[igCurTabIdx].cgUrnoType,
         prgTabDescr[igCurTabIdx].cgHopoType,prgTabDescr[igCurTabIdx].cgCdatType,
         prgTabDescr[igCurTabIdx].cgUsecType,prgTabDescr[igCurTabIdx].cgLstuType,
         prgTabDescr[igCurTabIdx].cgUseuType);
     dbg(TRACE,"=================================================");
     DB_GetUrnoKeyCode(prgTabDescr[igCurTabIdx].pcgTableName,prgTabDescr[igCurTabIdx].UrnoRangeKey);
     dbg(TRACE,"URNO POOL <%s> USING NUMTAB.KEYS <%s>",
                prgTabDescr[igCurTabIdx].pcgTableName,
                prgTabDescr[igCurTabIdx].UrnoRangeKey);
     dbg(TRACE,"=================================================");
  }

  return;

} /* End of GetTabDescrIdx */

static int CheckURNO(char cpFkt, char cpFieldType, char *pcpFieldList,
                     char *pcpVList, char *pcpDList)
{
  int ilRC = RC_SUCCESS;
  int ilFldNum;
  char pclFldVal[16];
  char pclTmpNextVal[16];
  char pclTmpUrno[16];

  ilFldNum = get_item_no(pcpFieldList,"URNO",5) + 1;
  if (ilFldNum > 0)
  {
     get_real_item(pclFldVal,pcpDList,ilFldNum);
     if (cpFieldType == 'M')
     {
        dbg(TRACE,"CheckURNO: Remove URNO from List");
        ilRC = RemoveItemFromList(ilFldNum,pcpFieldList);
        ilRC = RemoveItemFromList(ilFldNum,pcpVList);
        ilRC = RemoveItemFromList(ilFldNum,pcpDList);
        ilFldNum = get_item_no(pcgAllOutFld,"URNO",5) + 1;
        if (ilFldNum > 0)
        {
           ilRC = RemoveItemFromList(ilFldNum,pcgAllOutFld);
           ilRC = RemoveItemFromList(ilFldNum,pcgOldData);
        }
     }
  }
  else
  {
     if (cpFieldType != 'M' && cpFkt == 'I')
     {
        dbg(TRACE,"CheckURNO: Add URNO to List");
        ilRC = GetNextValues(pclTmpNextVal,1);
        if (cpFieldType == 'N')
        {
           strcpy(pclTmpUrno,pclTmpNextVal);
        }
        else
        {
           sprintf(pclTmpUrno,"'%s'",pclTmpNextVal);
        }
        if (cpFkt == 'I')
        {
           ilRC = AddListToItem(pcpFieldList,"URNO");
           ilRC = AddListToItem(pcpVList,":VURNO");
           ilRC = AddListToItem(pcpDList,pclTmpUrno);
        }
        if (strstr(pcgAllOutFld,"URNO") == NULL)
        {
           ilRC = AddListToItem(pcgAllOutFld,"URNO");
           ilRC = AddListToItem(pcgOldData,"0");
        }
     }
  }

  return ilRC;
} /* End of CheckURNO */

static int CheckHOPO(char cpFkt, char cpFieldType, char *pcpFieldList,
                     char *pcpVList, char *pcpDList)
{
  int ilRC = RC_SUCCESS;
  int ilFldNum;
  char pclFldVal[16];

  ilFldNum = get_item_no(pcpFieldList,"HOPO",5) + 1;
  if (ilFldNum > 0)
  {
     get_real_item(pclFldVal,pcpDList,ilFldNum);
     if (cpFieldType == 'M')
     {
        dbg(TRACE,"CheckHOPO: Remove HOPO from List");
        ilRC = RemoveItemFromList(ilFldNum,pcpFieldList);
        ilRC = RemoveItemFromList(ilFldNum,pcpVList);
        ilRC = RemoveItemFromList(ilFldNum,pcpDList);
        ilFldNum = get_item_no(pcgAllOutFld,"HOPO",5) + 1;
        if (ilFldNum > 0)
        {
           ilRC = RemoveItemFromList(ilFldNum,pcgAllOutFld);
           ilRC = RemoveItemFromList(ilFldNum,pcgOldData);
        }
     }
  }
  else
  {
     if (cpFieldType != 'M' && cpFkt == 'I')
     {
        dbg(TRACE,"CheckHOPO: Add HOPO to List");
        if (cpFkt == 'I')
        {
           ilRC = AddItemToList(pcpFieldList,"HOPO");
           ilRC = AddItemToList(pcpVList,":VHOPO");
           ilRC = AddItemToList(pcpDList,pcgH3LC);
        }
        else
        {
           ilRC = AddItemToList(pcpFieldList,"HOPO");
           ilRC = AddItemToList(pcpVList,"HOPO=:VHOPO");
           ilRC = AddItemToList(pcpDList,pcgH3LC);
        }
        if (strstr(pcgAllOutFld,"HOPO") == NULL)
        {
           ilRC = AddItemToList(pcgAllOutFld,"HOPO");
           ilRC = AddItemToList(pcgOldData," ");
        }
     }
  }

  return ilRC;
} /* End of CheckHOPO */

static int CheckCDAT(char cpFkt, char cpFieldType, char *pcpFieldList,
                     char *pcpVList, char *pcpDList)
{
  int ilRC = RC_SUCCESS;
  int ilFldNum;
  char pclFldVal[16];
  char pclCurTime[32];

  GetServerTimeStamp("UTC",1,0,pclCurTime);
  ilFldNum = get_item_no(pcpFieldList,"CDAT",5) + 1;
  if (ilFldNum > 0)
  {
     get_real_item(pclFldVal,pcpDList,ilFldNum);
     if (cpFieldType == 'M' || cpFkt == 'U')
     {
        dbg(TRACE,"CheckCDAT: Remove CDAT from List");
        ilRC = RemoveItemFromList(ilFldNum,pcpFieldList);
        ilRC = RemoveItemFromList(ilFldNum,pcpVList);
        ilRC = RemoveItemFromList(ilFldNum,pcpDList);
        ilFldNum = get_item_no(pcgAllOutFld,"CDAT",5) + 1;
        if (ilFldNum > 0)
        {
           ilRC = RemoveItemFromList(ilFldNum,pcgAllOutFld);
           ilRC = RemoveItemFromList(ilFldNum,pcgOldData);
        }
     }
  }
  else
  {
     if (cpFieldType != 'M' && cpFkt == 'I')
     {
        dbg(TRACE,"CheckCDAT: Add CDAT to List");
        ilRC = AddItemToList(pcpFieldList,"CDAT");
        ilRC = AddItemToList(pcpVList,":VCDAT");
        ilRC = AddItemToList(pcpDList,pclCurTime);
        if (strstr(pcgAllOutFld,"CDAT") == NULL)
        {
           ilRC = AddItemToList(pcgAllOutFld,"CDAT");
           ilRC = AddItemToList(pcgOldData," ");
        }
     }
  }

  return ilRC;
} /* End of CheckCDAT */

static int CheckUSEC(char cpFkt, char cpFieldType, char *pcpFieldList,
                     char *pcpVList, char *pcpDList)
{
  int ilRC = RC_SUCCESS;
  int ilFldNum;
  char pclFldVal[128];

  ilFldNum = get_item_no(pcpFieldList,"USEC",5) + 1;
  if (ilFldNum > 0)
  {
     get_real_item(pclFldVal,pcpDList,ilFldNum);
     if (cpFieldType == 'M' || cpFkt == 'U')
     {
        dbg(TRACE,"CheckUSEC: Remove USEC from List");
        ilRC = RemoveItemFromList(ilFldNum,pcpFieldList);
        ilRC = RemoveItemFromList(ilFldNum,pcpVList);
        ilRC = RemoveItemFromList(ilFldNum,pcpDList);
        ilFldNum = get_item_no(pcgAllOutFld,"USEC",5) + 1;
        if (ilFldNum > 0)
        {
           ilRC = RemoveItemFromList(ilFldNum,pcgAllOutFld);
           ilRC = RemoveItemFromList(ilFldNum,pcgOldData);
        }
     }
  }
  else
  {
     if (cpFieldType != 'M' && cpFkt == 'I')
     {
        dbg(TRACE,"CheckUSEC: Add USEC to List");
        ilRC = AddItemToList(pcpFieldList,"USEC");
        ilRC = AddItemToList(pcpVList,":VUSEC");
        ilRC = AddItemToList(pcpDList,pcgUser);
        if (strstr(pcgAllOutFld,"USEC") == NULL)
        {
           ilRC = AddItemToList(pcgAllOutFld,"USEC");
           ilRC = AddItemToList(pcgOldData," ");
        }
     }
  }

  return ilRC;
} /* End of CheckUSEC */

static int CheckLSTU(char cpFkt, char cpFieldType, char *pcpFieldList,
                     char *pcpVList, char *pcpDList)
{
  int ilRC = RC_SUCCESS;
  int ilFldNum;
  char pclFldVal[16];
  char pclCurTime[32];

  GetServerTimeStamp("UTC",1,0,pclCurTime);
  ilFldNum = get_item_no(pcpFieldList,"LSTU",5) + 1;
  if (ilFldNum > 0)
  {
     get_real_item(pclFldVal,pcpDList,ilFldNum);
     /* 20031218 JIM: PRF 5455: do NOT remove LSTU or USEU from Insert-List
     if (cpFieldType == 'M' || cpFkt == 'I')
     */
     if (cpFieldType == 'M')
     {
        dbg(TRACE,"CheckLSTU: Remove LSTU from List");
        ilRC = RemoveItemFromList(ilFldNum,pcpFieldList);
        ilRC = RemoveItemFromList(ilFldNum,pcpVList);
        ilRC = RemoveItemFromList(ilFldNum,pcpDList);
        ilFldNum = get_item_no(pcgAllOutFld,"LSTU",5) + 1;
        if (ilFldNum > 0)
        {
           ilRC = RemoveItemFromList(ilFldNum,pcgAllOutFld);
           ilRC = RemoveItemFromList(ilFldNum,pcgOldData);
        }
     }
  }
  else
  {
     if (cpFieldType != 'M' && cpFkt == 'U')
     {
        dbg(TRACE,"CheckLSTU: Add LSTU to List");
        ilRC = AddItemToList(pcpFieldList,"LSTU");
        ilRC = AddItemToList(pcpVList,"LSTU=:VLSTU");
        ilRC = AddItemToList(pcpDList,pclCurTime);
        if (strstr(pcgAllOutFld,"LSTU") == NULL)
        {
           ilRC = AddItemToList(pcgAllOutFld,"LSTU");
           ilRC = AddItemToList(pcgOldData," ");
        }
     }
  }

  return ilRC;
} /* End of CheckLSTU */

static int CheckUSEU(char cpFkt, char cpFieldType, char *pcpFieldList,
                     char *pcpVList, char *pcpDList)
{
  int ilRC = RC_SUCCESS;
  int ilFldNum;
  char pclFldVal[128];

  ilFldNum = get_item_no(pcpFieldList,"USEU",5) + 1;
  if (ilFldNum > 0)
  {
     get_real_item(pclFldVal,pcpDList,ilFldNum);
     /* 20031218 JIM: PRF 5455: do NOT remove LSTU or USEU from Insert-List
     if (cpFieldType == 'M' || cpFkt == 'I')
     */
     if (cpFieldType == 'M')
     {
        dbg(TRACE,"CheckUSEU: Remove USEU from List");
        ilRC = RemoveItemFromList(ilFldNum,pcpFieldList);
        ilRC = RemoveItemFromList(ilFldNum,pcpVList);
        ilRC = RemoveItemFromList(ilFldNum,pcpDList);
        ilFldNum = get_item_no(pcgAllOutFld,"USEU",5) + 1;
        if (ilFldNum > 0)
        {
           ilRC = RemoveItemFromList(ilFldNum,pcgAllOutFld);
           ilRC = RemoveItemFromList(ilFldNum,pcgOldData);
        }
     }
  }
  else
  {
     if (cpFieldType != 'M' && cpFkt == 'U')
     {
        dbg(TRACE,"CheckUSEU: Add USEU to List");
        ilRC = AddItemToList(pcpFieldList,"USEU");
        ilRC = AddItemToList(pcpVList,"USEU=:VUSEU");
        ilRC = AddItemToList(pcpDList,pcgUser);
        if (strstr(pcgAllOutFld,"USEU") == NULL)
        {
           ilRC = AddItemToList(pcgAllOutFld,"USEU");
           ilRC = AddItemToList(pcgOldData," ");
        }
     }
  }

  return ilRC;
} /* End of CheckUSEU */


static int RemoveItemFromList(int ipFldNum, char *pcpList)
{
  int ilRC = RC_SUCCESS;
  int ilI;
  char *pclTmpPtr1;
  char *pclTmpPtr2;

  if (field_count(pcpList) < ipFldNum)
  {
     return ilRC;
  }

  ilI = 0;
  pclTmpPtr1 = pcpList;
  while (ilI < ipFldNum - 1)
  {
     pclTmpPtr1 = strstr(pclTmpPtr1,",");
     pclTmpPtr1++;
     ilI++;
  }
  pclTmpPtr2 = strstr(pclTmpPtr1,",");
  if (pclTmpPtr2 == NULL)
  {
     if (pclTmpPtr1 > pcpList)
     {
        pclTmpPtr1--;
     }
     *pclTmpPtr1 = '\0';
  }
  else
  {
     pclTmpPtr2++;
     strcpy(pclTmpPtr1,pclTmpPtr2);
  }

  return ilRC;
} /* End of RemoveItemFromList */


static int AddItemToList(char *pcpList, char *pcpVal)
{
  int ilRC = RC_SUCCESS;

  if (strlen(pcpList) > 0)
  {
     strcat(pcpList,",");
  }
  strcat(pcpList,pcpVal);

  return ilRC;
} /* End of AddItemToList */


static int AddListToItem(char *pcpList, char *pcpVal)
{
  int ilRC = RC_SUCCESS;

  if (strlen(pcpList) == 0)
  {
     strcpy(pcpList,pcpVal);
  }
  else
  {
     memmove(pcpList+strlen(pcpVal)+1,pcpList,strlen(pcpList)+1);
     strncpy(pcpList,pcpVal,strlen(pcpVal));
     strncpy(pcpList+strlen(pcpVal),",",1);
  }

  return ilRC;
} /* End of AddListToItem */

static void CheckQueueWait(int ipRouteId)
{
  int ilRC = RC_SUCCESS;

  /*if (ipRouteId >= 20000 && igQueueWait > 0)*/
  if (igQueueWait > 0)
  {
     nap(igQueueWait);
     dbg(TRACE,"Waited %d ms",igQueueWait);
  }

} /* End of CheckQueueWait */

static void CheckMultiInput(char *pcpCommand, char *pcpTable, char **pcpSelection,
                            char **pcpFields, char **pcpData, char cpFkt)
{
  int ilRC = RC_SUCCESS;
  int ilNoSel = 0;
  int ilNoFld = 0;
  int ilNoDat = 0;
  char *pclTmpPtr;
  char *pclTmpSel;
  char *pclTmpFld;
  char *pclTmpDat;
  char *pclTmpNextSel;
  char *pclTmpNextFld;
  char *pclTmpNextDat;
  int ilI;
  int ilDiffNoOfLines;
  int ilMulti;
  int ilMultiQueueId;
  int ilMultiPrio;
  int ilNoLines;

  if (cpFkt == 'I')
  {
     ilMulti = igMultiInsert;
     ilMultiQueueId = igMultiInsQueueId;
     ilMultiPrio = igMultiInsPrio;
  }
  else
  {
     if (cpFkt == 'U')
     {
        ilMulti = igMultiUpdate;
        ilMultiQueueId = igMultiUpdQueueId;
        ilMultiPrio = igMultiUpdPrio;
     }
     else
     {
        ilMulti = igMultiDelete;
        ilMultiQueueId = igMultiDelQueueId;
        ilMultiPrio = igMultiDelPrio;
     }
  }
  if (igMultiLineSerial == TRUE)
  {
     ilMultiQueueId = mod_id;
  }

  ilNoSel = GetNoOfElements(*pcpSelection,'\n');
  ilNoFld = GetNoOfElements(*pcpFields,'\n');
  ilNoDat = GetNoOfElements(*pcpData,'\n');
  if (ilNoSel > 1 || ilNoFld > 1 || ilNoDat > 1)
  {
     dbg(TRACE,"No of Selections Lines = %d, Field Lines = %d, Data Lines = %d",
         ilNoSel,ilNoFld,ilNoDat);
     ilDiffNoOfLines = FALSE;
     if (cpFkt == 'I')
     {
        if (ilNoFld != ilNoDat)
        {
           ilDiffNoOfLines = TRUE;
        }
     }
     else
     {
        if (cpFkt == 'U')
        {
           if (ilNoSel != ilNoFld || ilNoSel != ilNoDat || ilNoFld != ilNoDat)
           {
              ilDiffNoOfLines = TRUE;
           }
        }
     }
     if (ilDiffNoOfLines == TRUE || ilMulti == FALSE)
     {
        if (ilMulti == FALSE)
        {
           dbg(TRACE,"Multiple Insert are not allowed !!!");
        }
        else
        {
           dbg(TRACE,"No of Selection Lines (%d) or Field Lines (%d) or Data Lines (%d) are different",ilNoSel,ilNoFld,ilNoDat);
        }
        if (ilNoSel > 1)
        {
           pclTmpPtr = strstr(*pcpSelection,"\n");
           if (pclTmpPtr != NULL)
           {
              *pclTmpPtr = '\0';
              if (*(pclTmpPtr-1) == 0xd)
              {
                 *(pclTmpPtr-1) = '\0';
              }
           }
        }
        if (ilNoFld > 1)
        {
           pclTmpPtr = strstr(*pcpFields,"\n");
           if (pclTmpPtr != NULL)
           {
              *pclTmpPtr = '\0';
              if (*(pclTmpPtr-1) == 0xd)
              {
                 *(pclTmpPtr-1) = '\0';
              }
           }
        }
        if (ilNoDat > 1)
        {
           pclTmpPtr = strstr(*pcpData,"\n");
           if (pclTmpPtr != NULL)
           {
              *pclTmpPtr = '\0';
              if (*(pclTmpPtr-1) == 0xd)
              {
                 *(pclTmpPtr-1) = '\0';
              }
           }
        }
     }
     else
     {
        if (cpFkt == 'I' || cpFkt == 'U')
        {
           ilNoLines = ilNoFld;
        }
        else
        {
           ilNoLines = ilNoSel;
        }
        pclTmpSel = *pcpSelection;
        pclTmpFld = *pcpFields;
        pclTmpDat = *pcpData;
        for (ilI = 0; ilI < ilNoLines; ilI++)
        {
           if (ilNoSel > 1)
           {
              pclTmpNextSel = strstr(pclTmpSel,"\n");
              if (pclTmpNextSel != NULL)
              {
                 *pclTmpNextSel = '\0';
                 if (*(pclTmpNextSel-1) == 0xd)
                 {
                    *(pclTmpNextSel-1) = '\0';
                 }
                 pclTmpNextSel++;
              }
           }
           else
           {
              pclTmpNextSel = pclTmpSel;
           }
           if (ilNoFld > 1)
           {
              pclTmpNextFld = strstr(pclTmpFld,"\n");
              if (pclTmpNextFld != NULL)
              {
                 *pclTmpNextFld = '\0';
                 if (*(pclTmpNextFld-1) == 0xd)
                 {
                    *(pclTmpNextFld-1) = '\0';
                 }
                 pclTmpNextFld++;
              }
           }
           else
           {
              pclTmpNextFld = pclTmpFld;
           }
           if (ilNoDat > 1)
           {
              pclTmpNextDat = strstr(pclTmpDat,"\n");
              if (pclTmpNextDat != NULL)
              {
                 *pclTmpNextDat = '\0';
                 if (*(pclTmpNextDat-1) == 0xd)
                 {
                    *(pclTmpNextDat-1) = '\0';
                 }
                 pclTmpNextDat++;
              }
           }
           else
           {
              pclTmpNextDat = pclTmpDat;
           }
           if (ilI > 0)
           {
              dbg(TRACE,"Put <%s> <%s> <%s> <%s> <%s> on queue <%d> with prio <%d>",
                  pcpCommand,pcpTable,pclTmpSel,pclTmpFld,pclTmpDat,
                  ilMultiQueueId,ilMultiPrio);
              (void) tools_send_info_flag(ilMultiQueueId,0,pcgDestName,"",
                                          pcgRecvName,"","",pcgTwStart,pcgTwEnd,
                                          pcpCommand,pcpTable,
                                          pclTmpSel,pclTmpFld,pclTmpDat,ilMultiPrio);
              CheckQueueWait(ilMultiQueueId);
           }
           pclTmpSel = pclTmpNextSel;
           pclTmpFld = pclTmpNextFld;
           pclTmpDat = pclTmpNextDat;
        }
     }
     pclTmpPtr = *pcpSelection + strlen(*pcpSelection) + 1;
     strcpy(pclTmpPtr,*pcpFields);
     *pcpFields = pclTmpPtr;
     pclTmpPtr = *pcpFields + strlen(*pcpFields) + 1;
     strcpy(pclTmpPtr,*pcpData);
     *pcpData = pclTmpPtr;
     dbg(TRACE,"Command   = <%s>",pcpCommand);
     dbg(TRACE,"Table     = <%s>",pcpTable);
     dbg(TRACE,"Selection = <%s>",*pcpSelection);
     dbg(TRACE,"Fields    = <%s>",*pcpFields);
     dbg(TRACE,"Data      = <%s>",*pcpData);
  }

} /* End of CheckMultiInput */


static int HandleGetServerInfo(char *pcpTbl, char *pcpSel, char *pcpFld, char *pcpData)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  char *pcNewVal;
  char pclCmdStr [1024] = "";

  dbg(TRACE,"GET SERVER INFO");
  pcgResultData[0] = 0x00;
  if (strcmp(pcpTbl,"CONFIG") == 0)
  {
    ilGetRc = iGetConfigRow (pcpData, pcpSel, pcpFld, CFG_STRING, pcgResultData);
    dbg(TRACE,"<%s>",pcgResultData);
  }
  else if (strcmp(pcpTbl,"CFGTAB") == 0)
  {
    pcpFld=strtok(pcpFld,"=");
    TrimRight(pcpFld);
    pcNewVal=strtok(NULL,"=");
    if (pcNewVal!=NULL)
    {
      ilGetRc = iWriteConfigEntry(pcpData, pcpSel, pcpFld, CFG_STRING, pcNewVal);
    }
    return ilRc;
  }
  else if (strcmp(pcpTbl,"KSHTAB") == 0)
  { /* 20040823 JIM: execute some command send via event: */
      sprintf(pclCmdStr, "( %s ) >/dev/null 2>/dev/null", pcpFld);
      errno = 0;
      system(pclCmdStr);
      errno = 0;
    return ilRc;
  }
  else if (strcmp(pcpTbl,"FILE") == 0)
  {
  }
  (void) tools_send_info_flag_perf(que_out,0, pcgDestName, "", pcgRecvName,
                                   "", "", pcgTwStart, pcgTwEnd,
                                   "GSI",pcpTbl,pcpSel,pcpFld,
                                   pcgResultData,0);
  return ilRc;
}

/******************************************************************************/
/* HandleSHM: handle remote FIDS request for SHM */
/******************************************************************************/
int HandleSHM(char* pcpCommand, char* pcpTable, char* pcpFields, char* pcpData)
{
  int rc = RC_SUCCESS;

     if (igCEI_Role == CEI_CLIENT)
     {
        dbg(DEBUG,"HandleSHM: CMD <%s>, Table <%s>, Fields <%s>, Data  <%s>",
               pcpCommand,pcpTable,pcpFields,pcpData);
        rc= syslibUpdateDbData(pcpCommand[0],pcpTable,pcpFields,pcpData);
        if (rc != RC_SUCCESS)
        {
           dbg(TRACE,"HandleSHM: Error returned from syslibUpdateDbData with rc = <%d>",rc);
           rc=syslibReloadDbData(pcpTable);
           dbg(TRACE,"HandleSHM: Reloading Table %s rc = <%d>",pcpTable,rc);
           rc = RC_SUCCESS; /* bad thing, but what shall we do? */
        } /* end if */
     }
#if 0
* Wenn ich es recht sehe, dann brauche ich f�r ReleaseActionInfo eine Selection,
* die nicht mit der Selection f�r SHM identisch sein braucht. Also brauche ich
* ein noch HandleActionFromRemoteFIDS, das von FIDS-Seite bei jedem
* ReleaseActionInfo getriggert wird.
* Telefonat mit Bernie 20040507: Der Action-Teil wird entgegen der Spezifikation
* im RQ0013 bereits im Action selber abgewickelt, somit gibt es auch keine
* Probleme mit Old-Data. Allerdings muss (z.B.) die CCA_OUT-Config der SQLHDL
* auf beiden Seiten �bereinstimmen!
*
#endif
}

/******************************************************************************/
/* read UfisToUfis config: */
/******************************************************************************/
void SetupCEI(void)
{
  int ilRC = RC_SUCCESS;
  int ilLen;
  char pclDest[256];
  char pclTmp[256];

  igCEI_Role = CEI_UNDEF;
  igCEI_ModId = 0;
  igCEI_FinalDest = mod_id;
  pclDest[0]=0;

  if (ilRC = Get_CEI_Cfg(mod_name,pclDest,"") == RC_SUCCESS)
  {
     if (strncmp(pclDest,"CLIENT",6) == 0)
     {
        igCEI_Role= CEI_CLIENT;
     }
     else if (strncmp(pclDest,"SERVER",6) == 0)
     {
        igCEI_Role= CEI_SERVER;
     }
     if (igCEI_Role != CEI_UNDEF)
     {
        if ((ilLen = get_real_item (pclTmp, pclDest, 2)) > 0)
        {
            igCEI_ModId= atoi(pclTmp);
        }
        if ((ilLen = get_real_item (pclTmp, pclDest, 3)) > 0)
        {
            igCEI_FinalDest = atoi(pclTmp);
        }
     }
  }
  else
  {
     dbg (TRACE, "UfisToUfis configuration: reading CFG failed : <%d>", ilRC);
  }
  dbg (TRACE, "UfisToUfis configuration: Role         <%d>", igCEI_Role);
  dbg (TRACE, "UfisToUfis configuration: CEI mod-id   <%d>", igCEI_ModId);
  dbg (TRACE, "UfisToUfis configuration: final mod-id <%d>", igCEI_FinalDest);
}

/*****************************************************************************
 *
 * HandleFPE - UFIS-2467 - generate flight permit number
 * Return the next sequence number from NUMTAB where KEYS=FPE_PERN.
 * The current year is to be in the FLAG column. If current year changes, then
 * the ACNU is to reset to 1, and the FLAG updated.
 * Return the complete flight permit no, which consists of 
 * co.name, airport, running no. plus the year,
 * e.g. <11234566,ADAC/AUH/1234/12> where 
 * 11234566 is FPETAB urno, 1234 is new permit number, 12 is year two-digit.
 * ADAC and AUH are default values but each can be changed via sqlhdl.cfg.
 * Update FPETAB with the new PERN.
 * v.1.29 - also broadcast. 
 *****************************************************************************
 */

static int HandleFPE(void)
{
  int rc = RC_SUCCESS;	/* Return code 	*/
  int rc2,ilRc;
  char	pclFunc[] = "HandleFPE: ";
  BC_HEAD *prlBchd;		/* Broadcast header	*/
  CMDBLK *prlCmdblk;		/* Command Block 	*/
  char *pclDatablk;		/* Data Block	*/
  char *pclSelection;
  char *pclFields;
  char pclTable [16];
  char pclFlagForClientYear [16],pclFlagForClientMonth [16];
  
  char pclNextPermitNo [32],pclDataList [64];
  char *pclFpeTmp,pclFpeUrno [32];
  int  jj,nn;
  int ilNextPermitNo = -1;

  /* for update of FPETAB */ 
  short slCursorUpd = 0;
  char clSqlBuf[256];
  char pclDataBuf [256];
  char pclErrMsg [1024];


  que_out = prgEvent->originator;
  dbg(DEBUG,"%s: This Request is from (%d)",pclFunc,que_out);
  /************
  Get the structs
  ************/
  prlBchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  prlCmdblk= (CMDBLK  *) ((char *)prlBchd->data);
  pclSelection = prlCmdblk->data;
  pclFields = (char *) pclSelection + strlen(pclSelection)+1;
  pclDatablk = pclFields + strlen(pclFields)+1;
  strcpy (pclTable,prlCmdblk->obj_name);

  dbg(TRACE,"%s TBL <%s> FLD <%s> SEL <%s> DAT <%s>",
    pclFunc,pclTable,pclFields,pclSelection,pclDatablk);

  rc2 = RC_FAIL;
  strcpy (pclDataList,",");

  if ((strcmp (pclTable,"FPETAB") != 0) ||
      (strcmp (pclFields,"URNO,PERN") != 0) ||
      (strlen (pclSelection) == 0))
  {
    prlBchd->rc=RC_FAIL;
    dbg(TRACE,"%s *** TAB or FIELDS or SELECTION invalid",pclFunc);
  }
  else
  {
    strcpy (pclFlagForClientYear,"");
    strcpy (pclFlagForClientMonth,"");
    ilNextPermitNo = GetKeySequence("FPE_PERN","YEAR",
      pclFlagForClientYear,pclFlagForClientMonth);

    if (ilNextPermitNo < 1)
    {
      /* GetKeySequence() returns -1 for failed generation though */
      prlBchd->rc=RC_FAIL;
    }
    else
    { 
      /* UFIS-2467 requires 4-digit */
      sprintf (pclNextPermitNo,"%s/%s/%s/%s/%.4d",
        pcgFpeCoName,pcgFpeAirport,
        pclFlagForClientYear,pclFlagForClientMonth,ilNextPermitNo);
      pclFpeTmp = strstr (pclSelection,"="); /* eg. WHERE URNO = 123, or '123' */
      if (pclFpeTmp != '\0')
      {
        pclFpeTmp++;
        strcpy(pclFpeUrno,pclFpeTmp);
        /* remove quote marks if any */
        for (nn=0;nn < strlen(pclFpeUrno);nn++)
        {
          if (pclFpeUrno[nn] == '\'')
            pclFpeUrno [nn] = ' ';
        }
        nn = atol (pclFpeUrno);	/* remove spaces before/after */
        sprintf (pclFpeUrno,"%d",nn);
        sprintf (pclDataList,"%s,%s",pclFpeUrno,pclNextPermitNo);
        rc2 = RC_SUCCESS;
      }
      else
      {
        prlBchd->rc=RC_FAIL;
        dbg(TRACE,"%s *** SELECTION IN UNEXPECTED FORMAT",pclFunc);
      }
    }
  }

  /* Update FPETAB */

  sprintf(clSqlBuf,
    "UPDATE FPETAB SET PERN='%s' WHERE URNO=%s",
    pclNextPermitNo,pclFpeUrno);

  slCursorUpd = 0;
  dbg(TRACE,"%s <%s>",pclFunc,clSqlBuf);
  ilRc = sql_if(START,&slCursorUpd,clSqlBuf,pclDataBuf);
  if(ilRc == SQL_NOTFOUND)
  {
    dbg(TRACE,"%s *** FPETAB URNO <%s> Not Found For Update",pclFunc,pclFpeUrno);
  }
  else if (ilRc != DB_SUCCESS)
  { 
    /* there must be a database error */
    get_ora_err(ilRc, pclErrMsg);
    dbg(TRACE,"%s *** Update FPETAB URNO <%s> failed RC=%d <%s>",
      pclFunc,pclFpeUrno,ilRc,pclErrMsg);
  }

  commit_work();
  close_my_cursor(&slCursorUpd);


  /* Then send to client */
  /* Cannot use tools_send_info_flag() as that sends an empty data list */
  rc2 = new_tools_send_sql_perf(que_out,prlBchd,prlCmdblk,pclFields,pclDataList);
  dbg (TRACE,"%s Sent <FPE><%s><%s><%s><%s> to <%d>",
     pclFunc,pclTable,pclSelection,pclFields,pclDataList,que_out); 

  /* And broadcast */
  
  rc2 = SendCedaEvent (igToBcHdl, /* Route ID */
        mod_id,                   /* Origin ID */
        pcgDestName,
        "SQL",                    /* Recv_name */
        pcgTwStart,
        pcgTwEnd,
        "URT",                    /* CMDBLK command */
        "FPETAB",                 /* CMDBLK Obj name */
        pclSelection,             /* Selection block */
        pclFields,                /* Field block */
        pclDataList,              /* Data block */
        "",                       /* Error description */
        4,                        /* priority */
        NETOUT_NO_ACK);           /* return code */ 

    return rc2;
} /* end of HandleFPE */

/*****************************************************************************
 *
 * Flwg three functions taken from saphdl for use by HandleFPE. 
 * GetSequence will return the next running number from NUMTAB based on a
 * Key. If the row is not found, it will be inserted.
 * The sequence number will be reset at the start of either a new year or
 * month, based on a flag passed to the function. 
 * The year or month is the "Flag", either YYYY or MM.
 * FlagType will be either "YEAR" or "MONTH"
 * GetSequence will update that row in NUMTAB with an incremented sequence.
 * The sequence is initialized at 1, not 0.
 *
 *****************************************************************************
 */

/*****************************************************************************
 *
 * PutKeySequence - will update or insert a sequence number given a flag and
 * key into NUMTAB. 
 *
 *****************************************************************************
 */

static void PutKeySequence(char *pcpKey,long lpSequence,char *pcpFlag)
{
  int ilRc;
  char pclFunc[] = "PutKeySequence: ";
  short slCursorUpd = 0;
  short slCursorIns = 0;
  char clSqlBuf[256];
  char pclDataBuf [256];
  char pclErrMsg [1024];

  sprintf(clSqlBuf,
    "UPDATE NUMTAB SET ACNU=%d,FLAG='%s' WHERE KEYS='%s' AND HOPO='%s'",
    lpSequence,pcpFlag,pcpKey,pcgDefH3LC);

  dbg(TRACE,"%s <%s>",pclFunc,clSqlBuf);
  ilRc = sql_if(START,&slCursorUpd,clSqlBuf,pclDataBuf);
  if(ilRc == SQL_NOTFOUND)
  {
    /* sequence not yet written, create new one */
    sprintf(clSqlBuf,
      "INSERT INTO NUMTAB (ACNU,FLAG,HOPO,KEYS,MAXN,MINN) "
      "VALUES(%d,'%s','%s','%s',9999999999,1)",
      lpSequence,pcpFlag,pcgDefH3LC,pcpKey);
    
    dbg(TRACE,"%s <%s>",pclFunc,clSqlBuf);

    slCursorIns = 0;
    ilRc = sql_if(START,&slCursorIns,clSqlBuf,pclDataBuf);
    if (ilRc != RC_SUCCESS)
    {
      /* there must be a database error */
      get_ora_err (ilRc, pclErrMsg);
      dbg(TRACE,"%s *** Inserting Sequence <%s> failed RC=%d <%s>",
        pclFunc,pcpKey,ilRc,pclErrMsg);
    }
    close_my_cursor(&slCursorIns);
  }
  else if (ilRc != DB_SUCCESS)
  { 
    /* there must be a database error */
    get_ora_err(ilRc, pclErrMsg);
    dbg(TRACE,"%s *** Update NUMTAB <%s> failed RC=%d <%s>",
      pclFunc,pcpKey,ilRc,pclErrMsg);
  }

  commit_work();
  close_my_cursor(&slCursorUpd);
}

/*****************************************************************************
 *
 * GetKeySequence - will get the current seq number for a particulat key 
 * and flag type (YEAR/MONTH). If the db flag is different from current month
 * or year (depending on flag type), it will update the flag and reset the
 * counter. If no row found in NUMTAB, insert a new row and reset the counter.
 * key into NUMTAB. 
 * NUMTAB saves YYYY, we return YY to the caller. Perhaps this could be simpler.
 * UFIS-2467 requires 4-digit format. Do not save in that format in NUMTAB,
 * to keep this function slightly generic. Formatting is in HandleFPE.
 *
 * return the month MM and year YY for the flight permit.
 *
 *****************************************************************************
 */

static long GetKeySequence(char *pcpKey, char *pcpFlagType,
  char *pcpFlagForClientYear,char *pcpFlagForClientMonth)
{
  int ilRc;
  char pclFunc[] = "GetKeySequence: ";
  long llSequence = -1;
  char clSelection[512]; 
  char clSequence[24];
  char clTimeStamp[24];
  char clDbFlag[32];
  char clCurFlag[16]; 

  short slCursor = 0;
  short slFunction;
  char  pclDataArea [256];
  char  pclErrMsg [1024];


  GetServerTimeStamp("LOC", 1,0,clTimeStamp);
  if (strcmp(pcpFlagType,"MONTH") == 0)
  {
    strncpy (clCurFlag,&clTimeStamp[4],2);
    clCurFlag [2] = '\0';
  }
  else if (strcmp(pcpFlagType,"YEAR") == 0)
  {
    strncpy (clCurFlag,&clTimeStamp[0],4);
    clCurFlag [4] = '\0';
  }
  else
  {
    dbg(TRACE,"%s ERROR - called with unexpected FlagType <%s>",
      pclFunc,pcpFlagType);
    strcpy (clCurFlag,"UNKNOWN");
  }

  /*
	The current month will always be correct, because it is not
	used as the flag in numtab. 
  */
  strncpy (pcpFlagForClientMonth,&(clTimeStamp[4]),2);
  pcpFlagForClientMonth [2] = '\0';

 
  sprintf(clSelection,
    "SELECT ACNU,FLAG FROM NUMTAB WHERE KEYS='%s' AND HOPO='%s'",pcpKey,pcgDefH3LC);
  dbg(TRACE,"%s <%s>",pclFunc,clSelection);
  strcpy (pclDataArea,"");
  slCursor = 0;
  ilRc = sql_if(START,&slCursor,clSelection,pclDataArea); 
  if(ilRc == RC_SUCCESS)
  {
    BuildItemBuffer(pclDataArea, "ACNU,FLAG", 2, ",");
    GetDataItem(clSequence,pclDataArea,1,',',"","\0\0");
    GetDataItem(clDbFlag,pclDataArea,2,',',"","\0\0");
    dbg(TRACE,"%s: Sequence <%s> %s <%s>",pclFunc,clSequence,pcpFlagType,clDbFlag);

    if (((strncmp(clCurFlag,clDbFlag,2) == 0) && (strcmp (pcpFlagType,"MONTH") == 0))
      ||
        ((strncmp(clCurFlag,clDbFlag,4) == 0) && (strcmp (pcpFlagType,"YEAR") == 0)))
    {
      llSequence = atol(clSequence);
      
      /* Save incremented sequence, note the flag */
      PutKeySequence(pcpKey,(llSequence + 1),clDbFlag);
      strncpy (pcpFlagForClientYear,&(clDbFlag[2]),2);
      pcpFlagForClientYear [2] = '\0';
    }
    else
    {
      llSequence = SEQUENCE_INIT;
      StringTrimRight(clDbFlag);
      dbg(TRACE,"%s <%s> changed from <%s> to <%s> use new Sequence <%d>",
        pclFunc,pcpFlagType,clDbFlag,clCurFlag,llSequence);
      /* Save incremented sequence, note the flag */
      PutKeySequence(pcpKey,(llSequence + 1),clCurFlag);
      strncpy (pcpFlagForClientYear,&(clCurFlag[2]),2);
      pcpFlagForClientYear [2] = '\0';
    }

  }
  else if(ilRc == SQL_NOTFOUND)
  {
    dbg(TRACE,"%s *** KEYS <%s> not found in NUMTAB. Create flag <%s> Seq <%d>",
        pclFunc,pcpKey,clCurFlag,llSequence);
    llSequence = SEQUENCE_INIT;
    /* give the init to the client, but save the incremented number */
    PutKeySequence(pcpKey,(llSequence + 1),clCurFlag);
    strncpy (pcpFlagForClientYear,&(clCurFlag[2]),2);
    pcpFlagForClientYear [2] = '\0';
  } 
  else
  { 
    get_ora_err(ilRc, pclErrMsg);
    dbg(TRACE,"%s failed RC=%d <%s>",pclFunc,ilRc,pclErrMsg);
    llSequence = -1;
  }

  close_my_cursor(&slCursor);
  return llSequence;
}

static void StringTrimRight(char *pcpBuffer)
{
  if (*pcpBuffer != '\0')
  {
    char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];
    while(isspace(*pclBlank) && pclBlank != pcpBuffer)
    {
      *pclBlank = '\0';
      pclBlank--;
    }
  }
}

