
prompt prerequisite: a link in /ora/cedaurno must point to free space
prompt create link: ln -s /u04 /ora/cedaurno

prompt prerequisite: a link in /ora/cedalogging must point to free space
prompt create link: ln -s /u04 /ora/cedalogging
 
CREATE TABLESPACE CEDAURNO DATAFILE
        '/ora/cedaurno/oradata/UFIS/cedaurno.dbf' SIZE 2000M 
        autoextend on
default storage (
        initial         1M
        next            1M
        pctincrease     0
        minextents      1
        maxextents      unlimited
        );

CREATE TABLESPACE CEDALOGGING DATAFILE
        '/ora/cedalogging/oradata/UFIS/cedalogging.dbf' SIZE 2000M 
        autoextend on
default storage (
        initial         1M
        next            1M
        pctincrease     0
        minextents      1
        maxextents      unlimited
        );
