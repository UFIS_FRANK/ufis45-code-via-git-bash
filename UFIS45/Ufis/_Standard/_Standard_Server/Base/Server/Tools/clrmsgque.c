#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/clrmsgque.c 1.1 2003/03/19 17:03:27SGT fei Exp  $";
#endif /* _DEF_mks_version */
/* ******************************************************************** */
/*                                                                      */
/* Author         : Volker Bliss                                        */
/* Date           : 08.11.1996                                          */
/* Description    :                                                     */
/*                                                                      */
/* 20020924 JIM: added global variables mod_name, __CedaLogFile and outp    */
/*               to avoid core dump by null pointer in dbg                  */
/* ******************************************************************** */

static char sccs_clrmsgque[] ="@(#) UFIS 4.4 (c) ABB AAT/I clrmsgque.c 44.1 / 00/01/07 09:57:30 / VBL / MIKE";

#define U_MAIN
#define UGCCS_PRG	
#define QUE_INTERNAL

#ifdef _ODT5
#include <prototypes.h>
#endif

#include "glbdef.h"
#include "ugccsma.h"
#include "quedef.h"
#include "uevent.h"
#include "cli.h"

int debug_level = DEBUG;

static void usage(void);
char	*mod_name;
char	__CedaLogFile[128];
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp = NULL;*/

/* ******************************************************************** */
/* The MAIN program                                                     */
/* ******************************************************************** */
MAIN
{
	int	rc = 0;
	long dest = 0;
	size_t len = sizeof(ITEM)+sizeof(EVENT);
	int	flag;
	int que;
	int	msgque;
	ITEM *item = NULL;
	EVENT *event = NULL;
	
	if(argc != 4)
	{
		usage();
		exit(1);
	}
  mod_name = argv[0];
  /* if argv[0] contains path (i.e. started by ddd), skip path */
  if (strrchr(mod_name,'/')!= NULL)
  {
     mod_name= strrchr(mod_name,'/');
     mod_name++;
  }
	sprintf(__CedaLogFile,"%s/%s%.5ld.log",getenv("DBG_PATH"),mod_name,getpid());

	que=atoi(argv[1]);
	switch(que)
	{
	case 1 :
		msgque = msgget(QCP_INPUT,0);
		break;
	case 3 :
		msgque = msgget(QCP_GENERAL,0);
		break;
	default :
	case 2 :
		msgque = msgget(QCP_ATTACH,0);
		break;
	}

	if (msgque < 0) {
		printf("ERROR: unable to init que!\n");fflush(stdout);
		exit(1);
	} /* end if */

	printf("clear msgque <%d>\n",msgque);fflush(stdout);

	len = atoi(argv[2]);
	printf("item_len <%d>\n",len);fflush(stdout);

	dest = atoi(argv[3]);
	printf("checking dest <%d>\n",dest);fflush(stdout);

	item = (ITEM *) malloc(len);
	event = (EVENT *) item->text;

	flag = IPC_NOWAIT;

	rc = 0;
	errno=0;
	rc = msgrcv(msgque,item,len,dest,flag);
	if (rc > 0) 
	{
		printf("this item was for <%ld>\n",item->msg_header.mtype);
       	printf("item.msg_header.mtype (%ld)\n",item->msg_header.mtype);
       	printf("item.function         (%d)\n",item->function);
       	printf("item.route            (%d)\n",item->route);
       	printf("item.priority         (%d)\n",item->priority);
       	printf("item.msg_length       (%d)\n",item->msg_length);
       	printf("item.originator       (%d)\n",item->originator);
       	printf("item.block_nr         (%d)\n",item->block_nr);
       	printf("event.type            (%d)\n",event->type);
       	printf("event.command         (%d)\n",event->command);
       	printf("event.originator      (%d)\n",event->originator);
       	printf("event.sys_ID          (%s)\n",event->sys_ID);
       	printf("event.retry_count     (%d)\n",event->retry_count);
       	printf("event.data_offset     (%d)\n",event->data_offset);
       	printf("event.data_length     (%ld)\n",event->data_length);

		if(rc < len)
		{
			snap((char *)item,rc,stdout);
		}else{
			snap((char *)item,len,stdout);
		}/* end of if */

		printf("rc <%d> bytes received\n",rc);
		fflush(stdout);
	}else{
		printf("msgrcv returned <%d> errno<%d><%s>\n",rc,errno,strerror(errno));
		printf("EINVAL <%d> ",EINVAL);
		printf("EACCES <%d> ",EACCES);
		printf("E2BIG  <%d> ",E2BIG);
		printf("ENOMSG <%d> ",ENOMSG);
		printf("EFAULT <%d>\n",EFAULT);
		fflush(stdout);
	} /* end if */
			
	exit(0);
	
} /* end of MAIN */

static void usage(void)
{
	printf("Usage: clrmsgque msgq bytes dest\n");
	printf("msgq:  1 QCP_INPUT    key<%x>\n",QCP_INPUT);
	printf("       2 QCP_ATTACH   key<%x>\n",QCP_ATTACH);
	printf("       3 QCP_GENERAL  key<%x>\n",QCP_GENERAL);
	printf("bytes: allocated space for the requested item\n");
	printf("dest:  expected queueid - originator\n");
	printf("       0  requests any item\n");
}

/* ******************************************************************** */
/* Space for Appendings (Source-Code Control)				*/
/* ******************************************************************** */
/* ******************************************************************** */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
