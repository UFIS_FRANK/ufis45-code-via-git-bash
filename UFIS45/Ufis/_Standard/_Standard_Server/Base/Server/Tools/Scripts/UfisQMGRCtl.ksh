#!/bin/ksh
THIS=`basename $0`
case "${LOGNAME}" in
   "ceda"|"mqm") ;;
   *)            echo "${THIS}: You must be 'ceda' or 'mqm' to run this...."
                 exit 1
                 ;;
esac


MQMANAGER=`awk '{if ($1 == "MQMANAGER") {print $3 ; exit}}' ${CFG_PATH}/wmqlib.cfg`
#MQMANAGER=TESTMQ
MQSTAT=`/opt/mqm/bin/dspmq -m ${MQMANAGER} 2>&1`
#echo "MQMANAGER is ${MQMANAGER}"
echo "${THIS}: Status of ${MQMANAGER} is ${MQSTAT}"

GetModIdsForShutdown( )
{
# This Script lists the Queue-ID's from ${CFG_PATH}/wmqlib.cfg, where the
# Queue-ID's pointing to load sharing queues are replaced by the Queue-ID of the
# loadsharing queue. For i.e load sharing queue "7160 = 506" the 506 is listed.
awk '
   BEGIN {LQFLAG=0 ; LSN=0; NQN=0 ; NQFLAG=0} 
   {
     if ($1=="[LSQUEUES]") 
     {
        LQFLAG=1
     } 
     else if ($1=="[LSQUEUES_END]") 
     {
        LQFLAG=0
     } 
     else if ($1=="[QUEUES]") 
     {
        NQFLAG=1
     } 
     else if ($1=="[QUEUES_END]") 
     {
        NQFLAG=0
     } 
     else if (NQFLAG==1) 
     { 
        if (($2 = "=") && (substr($1,1,1) != "#"))
        {
           NQ[NQN]=$3
           NQN++
        }
     }
     else if (LQFLAG==1) 
     { 
        if (($2 = "=") && (substr($1,1,1) != "#"))
        {
           LSQ0[LSN]=$1
           LSQ1[LSN]=$3
           # print $0
           LSN++
        }
     }
    }
    END {
       for (j=0; j<NQN; j++)
       {
         found=0
         for (i=0; i<LSN; i++)
         {
            if (NQ[j]==LSQ0[i])
            {
              found=LSQ1[i]
              i=LSN # leave loop i
            }
         }
         if (found==0)
         {
              print NQ[j]
         }
         else
         {
              print found
         }
       }
    }
    ' ${CFG_PATH}/wmqlib.cfg
}
SendCedaShutdown()
{
# This Script sends SHUTDOWN to processes listed by GetModIdsForShutdown
  for q in `GetModIdsForShutdown` ; do
     echo "${THIS}: queue $q: "`${ETC_PATH}/qput que=$q cmd=-1 debug=0 wait=-1 | grep -v "DEBUG SET to 0" | grep -v "DEBUG SWITCHED OFF" `
  done
}

case $1 in
   start)
      case ${MQSTAT} in
    	 QMNAME\(${MQMANAGER}\)*Running*)
	     echo "${THIS}: Error: MQ Manager ${MQMANAGER} still is running!"
	     echo "${THIS}: Error: MQ system may be corrupted!"
	     echo "${THIS}: please check system state!"
	     exit 1
	     ;;
      esac
      echo "${THIS}: Clearing shared memory, semaphores and queues ..."
      /ceda/etc/DelIpcMqm
      echo "${THIS}: starting MQ Manager ${MQMANAGER} ..."
      /opt/mqm/bin/strmqm ${MQMANAGER}
      MQSTAT=`/opt/mqm/bin/dspmq -m ${MQMANAGER} 2>&1`
      echo "${THIS}: New Status of ${MQMANAGER} is ${MQSTAT}"
      case ${MQSTAT} in
    	 QMNAME\(${MQMANAGER}\)*Running*)
             runmqlsr -t tcp -m ${MQMANAGER} -p 1414 2>&1 >/dev/null &
	     runmqlsr -t tcp -m ${MQMANAGER} -p 1515 2>&1 >/dev/null &
	     strmqcsv ${MQMANAGER} 2>&1 >/dev/null &
	     echo
	     exit 0
             ;;
         *)
             exit 2
             ;;
      esac
      ;;
   ceda_stop)
      case ${MQSTAT} in
       QMNAME\(${MQMANAGER}\)*Running*)
         echo "${THIS}: Sending SHUTDOWN to CEDA queues ..."
         # send SHUTDOWN to Queues listed in ${CFG_PATH}/wmqlib.cfg
         SendCedaShutdown
         sleep 2
       ;;
      esac
      exit 0
      ;;
   wmq_stop)
      echo "${THIS}: stopping MQ Manager ${MQMANAGER} ..."
      /opt/mqm/bin/endmqm -i ${MQMANAGER}
      MQSTAT=`/opt/mqm/bin/dspmq -m ${MQMANAGER} 2>&1`
      echo "${THIS}: New Status of ${MQMANAGER} is ${MQSTAT}"
      case ${MQSTAT} in
    	 QMNAME\(${MQMANAGER}\)*STATUS\(Ended\ *)
             echo "${THIS}: stopping MQ Listener ${MQMANAGER} ..."
    	     endmqlsr -m ${MQMANAGER} 
    	     endmqcsv ${MQMANAGER}
	     echo
             exit 0
             ;;
         *)
             exit 3
             ;;
      esac
      ;;
   *)
      echo "${THIS}: no command (start/stop) given..."
      exit 4
      ;;
esac
#      case ${MQSTAT} in
#	  QMNAME\(${MQMANAGER}\)*Running*)
#	    if [ "$1" = "start" ] ; then
#	       echo "${THIS}: MQ Manager ${MQMANAGER} is already running, nothing to do..."
#	       exit 0
#	    else
#	       echo "${THIS}: stopping MQ Manager ${MQMANAGER} ..."
#	       /opt/mqm/bin/endmqm -i ${MQMANAGER}
#	       exit 0
#	    fi
#	    ;;
#	  QMNAME\(${MQMANAGER}\)*STATUS\(Ended\ *)
#	    if [ "$1" = "stop" ] ; then
#	       echo "${THIS}: MQ Manager ${MQMANAGER} is not running, nothing to do..."
#	       exit 0
#	    else
#	       echo "${THIS}: starting MQ Manager ${MQMANAGER} ..."
#	       /opt/mqm/bin/strmqm ${MQMANAGER}
#	    fi
#	   ;;
#	  AMQ7048*)
#	   echo "${THIS}: ERROR checking status of MQ Manager ${MQMANAGER}: "
#	   echo "${THIS}: ${MQSTAT}"
#	   exit 1
#	   ;;
#	  *)
#	   echo "${THIS}: ERROR checking status of MQ Manager ${MQMANAGER}: "
#	   echo "${THIS}: ${MQSTAT}"
#	   exit 1
#	   ;;
#      esac
##	case $1 in
##	   start)
##	      echo "${THIS}: starting MQ listener..."
##	      MQLSN_PIDS=$(ps -ef | grep "runmqlsr .* ${MQMANAGER}" | grep -v "grep")
##	      ;;
##	  stop)
##	      echo "${THIS}: stopping MQ listener..."
##	      ;;
##	esac
#   ;;
#
#
#
#
