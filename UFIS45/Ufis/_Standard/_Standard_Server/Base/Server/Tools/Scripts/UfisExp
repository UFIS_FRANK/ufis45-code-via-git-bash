#!/usr/bin/ksh
# $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/Scripts/UfisExp 1.3 2005/11/29 16:55:40SGT jim Exp  $
#
# 20031208 JIM: using ${CEDADBUSER}/{$CEDADBPW} instead of fix settings
# 20051129 JIM: adding ${MKNOD} and awk exression for remote DB
#
function display_help
{
   cat <<EOF

 This shell script $0 will make an export of the ORACLE DB.
 The export dump file will be compressed at instance by a
 named pipe, so there is no uncompressed file at any time
 this script is running.
 If no parameters are given, it uses the command

   exp ${CEDADBUSER}/${CEDADBPW} file=expdat.dmp buffer=131072 consistent=yes

 but any parameter of 'exp' can be modified.

EOF
}

unset EXPUSER
unset EXPOWNER
unset EXPTABLES
unset EXPFILE
unset EXPBUFFER
unset EXPCONSISTENT
unset EXPIGNORE
unset EXPCOMMIT
unset EXPOTHER
unset HELP_NEEDED

# 20051129 JIM: adding ${MKNOD}:
MKNOD=/usr/sbin/mknod
if [ ! -f ${MKNOD} ] ; then
   MKNOD=/sbin/mknod
fi
if [ ! -f ${MKNOD} ] ; then
   MKNOD=mknod
fi

# At first evaluate the parameters the user has given:
case "$1" in
   *=*) ;;           # if first parameter has '=', no user given
   */*) EXPUSER=$1   # else a user/passwd should be given and we shift
        shift;;
   *)
esac

while [ ! -z "$1" ] ; do
   case "$1" in
      file=*)        EXPFILE=$1;;
      buffer=*)      EXPBUFFER=$1;;
      consistent=*)  EXPCONSISTENT=$1;;
      ignore=*)      EXPIGNORE=$1;;
      commit=*)      EXPCOMMIT=$1;;
      owner=*)       EXPOWNER=$1;;
      tables=*)      EXPTABLES=$1;;
      ?|-?|-h|--help) HELP_NEEDED=yes ;;
      *)             EXPOTHER="${EXPOTHER} $1";;
   esac
   shift
done

if [ -z "${HELP_NEEDED}" ] ; then

   # now setting some defaults, if the user did not set them
   # 20051129 JIM: adding awk exression for remote DB (sqlplus
   # allows i.e. user@host/password, but 'exp' and 'imp' only
   # allow  i.e. user/password@host:)
   if [ -z "${EXPUSER}" ] ; then
      EXPUSER=`echo ${CEDADBUSER} ${CEDADBPW} | awk '
         {
           nn=split($1,usr,"@")
           if (nn>1)
           {
              print usr[1]"/"$2"@"usr[2]
           }
           else
           {
              print $1"/"$2
           }
         }'`
   fi
   if [ -z "${EXPOWNER}" -a -z "${EXPTABLES}" ] ; then
      EXPOWNER="${CEDADBUSER}"
   fi
   if [ -z "${EXPBUFFER}" ] ; then
      EXPBUFFER="buffer=131072"
   fi
   if [ -z "${EXPFILE}" ] ; then
      EXPFILE="file=expdat.dmp"
   fi
   if [ -z "${EXPCONSISTENT}" ] ; then
      EXPCONSISTENT="consistent=yes"
   fi

   # we need the filename without 'file='
   EXPFNAME=`echo "${EXPFILE}" | awk '{print substr($1,6)}'`

   # create a named pipe:
   # 20051129 JIM: adding ${MKNOD} 
   echo ${MKNOD} "${EXPFNAME}" p
   ${MKNOD} "${EXPFNAME}" p
   if [ $? != 0 ] ; then
       echo $0: unable to create pipe "${EXPFNAME}"
   else
      # start a compress in background, which reads from the named pipe:
      # COMPRESS WITH PIPE DOES NOT WORK ON UNIXWARE 7.1!!!
      #echo compress -c "${EXPFNAME}" \>"${EXPFNAME}.Z" \&
      #compress -c "${EXPFNAME}" >"${EXPFNAME}.Z" &
      echo cat "${EXPFNAME}" \| compress \-c \>"${EXPFNAME}".Z \&
      cat "${EXPFNAME}" | compress -c >"${EXPFNAME}".Z &

      STARTDATE=`date +"%Y%m%d %H:%M:%S"`
      STARTSECS=`date +"%y %j %H %M" | awk '{print ((($1*365)+$2*24)+$3*60)+$4}'`
      # start a export writing to the named pipe, the compress will write
      # the data to the '.Z' file
      echo exp "${EXPUSER}" "${EXPFILE}" "${EXPBUFFER}" "${EXPCONSISTENT}" \
          "${EXPIGNORE}" "${EXPCOMMIT}" "${EXPTABLES}" "${EXPOTHER}"
      exp "${EXPUSER}" "${EXPFILE}" "${EXPBUFFER}" "${EXPCONSISTENT}" \
          "${EXPIGNORE}" "${EXPCOMMIT}" "${EXPTABLES}" "${EXPOTHER}"
      ENDDATE=`date +"%Y%m%d %H:%M:%S"`
      ENDSECS=`date +"%y %j %H %M" | awk '{print ((($1*365)+$2*24)+$3*60)+$4}'`

      # if export failed, nothing may be written to the pipe and compress still
      # waits:
      sleep 3
      cnt=`ps -ef | grep "cat ${EXPFNAME} \| compress " | grep -v grep | grep -c "^"`
      if [ ${cnt} \> 0 ] ; then
         echo .> "${EXPFNAME}"
      fi
      # remove the named pipe
      rm "${EXPFNAME}"

      echo Export has run from "${STARTDATE}" to "${ENDDATE}"
      echo Export needed `expr "${ENDSECS}" - "${STARTSECS}"` minutes
   fi
else
   display_help
fi

unset EXPUSER
unset EXPOWNER
unset EXPTABLES
unset EXPFILE
unset EXPBUFFER
unset EXPCONSISTENT
unset EXPIGNORE
unset EXPCOMMIT
unset EXPOTHER
unset HELP_NEEDED     

