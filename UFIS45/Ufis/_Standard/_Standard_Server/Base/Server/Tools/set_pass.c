#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/set_pass.c 1.1 2003/03/19 17:05:15SGT fei Exp  $";
#endif /* _DEF_mks_version */
#include "set_pass.h"
#ifdef iMAX_BUF_SIZE
#undef iMAX_BUF_SIZE
#define iMAX_BUF_SIZE 1024000
#endif
int main(int argc, char **argv)
{
	int		i;
	int		ilSOS;
	int		ilEOS;
	int		ilFound;
	int		ilLineNo;
	char		*pclPtr;
	char		*pclPasswd;
	char		pclTmpBuf[iMIN_BUF_SIZE];
	char		pclMaxBuf[iMAX_BUF_SIZE];
	char		pclSection[iMIN_BUF_SIZE];
	char		pclEndSection[iMIN_BUF_SIZE];
	char		pclKeyWord[iMIN_BUF_SIZE];
	char		*pclFile[iMAX_BUF_SIZE];
	FILE		*fh;

	if (argc != 5)
	{
		fprintf(stderr,"Usage: %s passwd filename section keyword\n", *argv);
		exit(1);
	}
	
	/* clear buffer... */
	memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
	
	/* copy passwd to buffer... */
	strcpy((char*)pclTmpBuf, argv[1]);

	/* check for vaild characters... */
	for (i=0; i<(int)strlen((char*)pclTmpBuf); i++)
	{
		if (pclTmpBuf[i] < 32 || pclTmpBuf[i] > 127)
		{
			fprintf(stderr,"This is not a valid password\n");
			exit(2);
		}
	}

	/* set pointer... */
	pclPasswd = pclTmpBuf;

	/* Encode passwd... */
	CEncode(&pclPasswd);
	if (strncmp(argv[2],"debug_only",10) == 0)
	{
		fprintf(stderr,"Password encrypted=<%s>\n",pclPasswd);
		CDecode(&pclPasswd);
		fprintf(stderr,"Password decrypted=<%s>\n",pclPasswd);
		CEncode(&pclPasswd);
		fprintf(stderr,"Password encrypted=<%s>\n",pclPasswd);
		exit(0);
	}

	/* open file */
	if ((fh = fopen(argv[2],"r+")) == NULL)
	{
		fprintf(stderr,"can't open file \"%s\"\n", argv[2]);
		exit(3);
	}

	/* built section */
	memset((void*)pclSection, 0x00, iMIN_BUF_SIZE);
	memset((void*)pclEndSection, 0x00, iMIN_BUF_SIZE);
	memset((void*)pclKeyWord, 0x00, iMIN_BUF_SIZE);
	sprintf((char*)pclSection,"%c%s%c",'[',argv[3],']');
	sprintf((char*)pclEndSection,"%c%s%s",'[',argv[3],"_END]");

	/* convert to uppercase */
	StringUPR((UCHAR*)pclSection);
	StringUPR((UCHAR*)pclEndSection);

	/* keyword */
	memset((void*)pclKeyWord, 0x00, iMIN_BUF_SIZE);
	strcpy((char*)pclKeyWord, argv[4]);

	/* read file into memory */
	ilLineNo = 0;
	while (!feof(fh))
	{
		/* clear buffer */
		memset((void*)pclMaxBuf, 0x00, iMAX_BUF_SIZE);

		/* read line */
		if (fscanf(fh,"%[^\n]%*c", pclMaxBuf) != 1)
		{
			pclFile[ilLineNo++] = (char*)strdup(" ");		
			fscanf(fh,"%*c");
		}
		else
		{
			pclFile[ilLineNo++] = (char*)strdup((char*)pclMaxBuf);		
		}
	}

	/* close file */
	fclose (fh);

	/* search section */
	for (i=0, ilSOS=0, ilEOS=0, ilFound=0, pclPtr=NULL; i<ilLineNo; i++)
	{
		/* duplicate string */
		if ((pclPtr = (char*)strdup((char*)pclFile[i])) == NULL)
		{
			/*fprintf(stderr,"cannot allocate memory...\n");*/
			exit(1);
		}

		/* convert to uppercase */
		StringUPR((UCHAR*)pclPtr);
		
		/* is this start of section or end of section? */
		if (!strncmp((char*)pclPtr, (char*)pclSection, strlen((char*)pclSection)))
		{
			/* found start of section */
			ilSOS = 1;
		}
		else if (!strncmp((char*)pclPtr, (char*)pclEndSection, strlen((char*)pclEndSection)))
		{
			/* found start of section */
			ilEOS = 1;
		}
		else 
		{
			if (ilSOS && !ilEOS)
			{
				/* clear buffer */
				memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
				memset((void*)pclMaxBuf, 0x00, iMAX_BUF_SIZE);
				
				/* separate the line */
				SeparateIt(pclFile[i], pclTmpBuf, pclMaxBuf, '=');
				if (strstr((char*)pclTmpBuf, (char*)pclKeyWord) != NULL)
				{
					/* found entry */
					free ((void*)pclFile[i]);
					memset((void*)pclMaxBuf, 0x00, iMAX_BUF_SIZE);

					/* delete all blanks at end of string... */
					while (pclTmpBuf[strlen((char*)pclTmpBuf)-1] == ' ')
						pclTmpBuf[strlen((char*)pclTmpBuf)-1] = '\0';

					/* copy to buffer */
					sprintf((char*)pclMaxBuf,"%s = %s", pclTmpBuf, pclPasswd);
					pclFile[i] = (char*)strdup((char*)pclMaxBuf);
					ilFound = 1;
					break;
				}
			}
		}

		/* free duplicated string */
		if (pclPtr != NULL)	
			free((void*)pclPtr);
		pclPtr = NULL;
	}
	
	if (ilFound)
	{
		/* remove old file */
		if (remove(argv[2]))
		{
			/* remove fails */
			fprintf(stderr, "cannot remove old file %s\n", argv[2]);
			exit(4); 
		}
		else
		{
			/* open new file */
			if ((fh = fopen(argv[2], "w")) == NULL)
			{
				fprintf(stderr, "cannot open file %s\n", argv[2]);
				exit(5);
			}
			else
			{
				for (i=0; i<ilLineNo-1; i++)
					fprintf(fh,"%s\n", pclFile[i]);
				fclose(fh);
				printf("...OK\n");
			}
		}
	}
	else
	{
		fprintf(stderr, "can't find entry in file\n");
	}

	/* free memory */
	for (i=0; i<ilLineNo; i++)
		free((void*)pclFile[i]);

	/* everything is fine */
	exit(0);
}
