#ifndef _DEF_mks_version
#define _DEF_mks_version
#include "ufisvers.h"           /* sets UFIS_VERSION, must be done before mks_version */
static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/qput.c 1.19 2012/08/26 13:57:43SGT cst Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************/
/* UGCCS Program Skeleton					      */
/*								      */
/* Author		: Jochen Hiller	fuer cput, Jibbo Mueller fuer qput  			      */
/* Date			: 30.04.2001				      */
/* Description		:					      */
/*								      */
/* Update history	:					      */
/* 20020924 JIM: added global variables mod_name, __CedaLogFile and outp    */
/*               to avoid core dump by null pointer in dbg                  */
/* 20030219 JIM: additional arguments WKS, USER, TW_START and TW_END        */
/* 20040706 JIM: -102: delete all queues of CPUT                            */
/* 20040706 JIM: -103: delete all queues of *UTIL*                          */
/* 20040706 JIM: -104: delete one dedicated queue of *UTIL*  and *CPUT*     */
/* 20040706 JIM: QUE_GETBIG instead of QUE_GET, allocates memory in que()   */
/* 20040707 JIM: SYSQCP only uses I_SIZE Bytes (Unix-Queue-Size) for        */
/*               que_overview, so printf has to stop before this limit      */
/* 20040715 JIM: toggle the debug_level of SYSQCP with 'qput 0 7 -105'      */
/*               Switch off debug_level of SYSQCP with 'qput 0 8 -105'      */
/* 20040728 JIM: for DEBUG only: use SendNextForQueue to set one NEXTNW flag*/
/*               in the the SYSQCP, i.e. 'qput 0 7560 -106' for rulfndp     */
/* 20040804 JIM: for cc: declare SendNextForQueue before usage              */
/* 20040804 JIM: handled nearly all gcc warnings                            */
/* 20040813 MCU: alarm replaced by wait in loop (for MQ)                    */
/* 20040813 JIM: prototype que() removed                                    */
/* 20040813 JIM: display hint on SYSQCP_stat.log                            */
/* 20040813 JHE: using exit codes usage/error/timeout/ok (see XC_ consts)   */
/* 20041018 JIM: renamed to qput to redesign parameters                     */
/* 20041026 JIM: removed 'qput 0 7 -105' and 'qput 0 8 -105', TRACE / DEBUG */
/*               to SYSQCP or '1' mapped internal                           */
/* 20041028 JIM: displaying help with -h instead of -?, avoid bash expansion*/
/* 20041104 JIM: queue_overview uses SYSQCP_stat.log.<mod> to complete list */
/* 20041108 JIM: default first 2 params as <process> and <cmd>, "=" obsolet */
/* 20041109 JIM: if timeout given, use also for db-connect                  */
/* 20041109 JIM: replaced .<pid> by .<mod>                                  */
/* 20041117 JIM: added parameter "nap=", also debug count of GETNW          */
/* 20041117 JIM: added loop function to handle_data_input()                 */
/* 20041117 JIM: avoid further work if malloc fails in handle_data_input()  */
/* 20041123 JIM: move nap() to avoid compile error...                       */
/* 20041124 JIM: use DBG_PATH everywhere instead of /ceda/debug and /ceda/tmp */
/* 20041124 JIM: added QUE_ACK after QUE_GETBIGxx                           */
/* 20041124 JIM: for none WMQ use alarm() to wait                           */
/* 20041124 JIM: added commands "go,hold,empty,next,ack"                    */
/* 20041124 JIM: forgot some * test *                                       */
/* 20041213 JIM: added flag to SendNextForQueue()                           */
/* 20041213 JIM: added ack and que_get, renamed next to next_flag           */
/* 20050417 JIM: ensured debug_level off until outp is assigned  for libs   */
/* 20050425 JIM: disable pure SYSQCP functions in WMQ environment           */
/* 20050425 JIM: allow full paramater names (tab/table, fld/fields, ... )   */
/* 20050425 JIM: allow beside -1 and -2 also usage of 'shutdown' and 'reset'*/
/* 20050502 JIM: correction on "disable pure SYSQCP functions in WMQ ..."   */
/* 20050913 JIM: Only if queue created, delete it                           */
/* 20070226 JIM: on AIX the pids are not continously increasing but random, */
/*               so the modulo may create same tag at 2 simultaniosly       */
/*               starting NETINs. Use complete PID                          */
/* 20120826 CST: Fixed modifying quoted strings (TWS/TWE)						  */
/* ********************************************************************/
/* This program is a UGCCS main program */

#define U_MAIN
#define CEDA_PRG
#define QUE_INTERNAL
#include <stdio.h>
#include <unistd.h>
#include "glbdef.h"
#include "ugccsma.h"
#include "quedef.h"
#include "uevent.h"
#include "tools.h"
#include "sthdef.h"
#include "quedef.h"
#include "queutil.h"
#include <stdarg.h>
#include <time.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include <sys/stat.h>

#ifndef max
#define max(a,b)    (((a) > (b)) ? (a) : (b))
#endif
#ifndef min
#define min(a,b)    (((a) < (b)) ? (a) : (b))
#endif

extern int debug_level = 0; /* 20050417 JIM: default is OFF to avoid libs using outp */
extern int run_debug_level = TRACE;
extern int nap(unsigned long);
/* ******************************************************************** */
/* External variables							*/
/* ******************************************************************** */

/* ******************************************************************** */
/* Global variables							*/
/* ******************************************************************** */
char *mod_name;
static char pcgCedaLogFile[128];
static FILE *pfgDbgLog = NULL;
static char pcgMyName[32];

/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp = NULL;*/
static EVENT event;          /* The event            */
typedef struct CommandLineParams{
   char *cQueue;
   char *cPriority;
   char *cCommand;
   char *cTable;
   char *cFields;
   char *cData;
   char *cSelection;
   char *cTimeWait;
   char *cRcv;
   char *cDst;
   char *cTws; 
   char *cTwe;
   char *cLoop;
   char *cDelay;
   char *cNap;
   
} PARAMETERS;
                          /* q, pri, cmd, tb, fl, dt, sel, wait, rcv,    dst,     TWS,         TWE:hop,tab,qput loop delay */
                          /* be carefull mofifying strings and length, <mod> and <HOPO> are patched into this */
PARAMETERS sgParameters = { "1", "1", "", "", "", "", "", "120", "EXCO", "QPUT", "QPUT.<mod>  ", "   ,TAB,QPUT", "1", "0", "1000" } ;

/* Quoted strings are  in read-only memory so we need variables for the cTws and cTwe parameter because they are modified */
char cDyncTws[16];
char cDyncTwe[16];
char *pcgCurrParName;
char *pcgCurrParVal;
char constgDbgPath[] = "/ceda/debug";
char *pcgDbgPath;
char pcgQputTmpFile[128] = "";


/* ******************************************************************** */
/* External functions							*/
/* ******************************************************************** */
extern int init_que ();      /* Attach to UGCCS queues */

/* extern	int	que(int,int,int,int,int,ITEM **); /* UGCCS queuing routine*/
extern void catch_all ();    /* Catches all signals    */
extern int send_message (int, int, int, int, char *);
extern int atoi (const char *);

/* ******************************************************************** */
/* Function prototypes							*/
/* ******************************************************************** */
/* 20041109 JIM: ipTimeWait: if timeout given, use also for db-connect: */
static int init_cput (int ipTimeWait);
static void handle_sig ();   /* Handles signals    */

                                    /* static void	handle_err(int);*//* Handles general errors */
static void handle_qerr (int);  /* Handles queuing errors */
static int get_q_id (char *);
static void one_queue_overview (int pmod_id);
static void queue_overview (int ipWhat, int ipDest);
static void TrimRight (char *pcpBuffer);
static int delete_queue (int que_ID, int mod_id);
static int handle_data_input (int ipDestination, int ipPriority, char *pcpCommand, char *pcpTable, char *pcpFields,
                              char *pcpData, char *pcpSelection, int ipTimeWait, char *pcpDst, char *pcpRcv,
                              char *pcpTws, char *pcpTwe, int ipLoop, int ipNap);
static void get_time (char *s);
static void dbglog (int level, char *fmt, ...);
static void cput45_terminate (int ipExVal);

static void set_sysqcp_dbg (int level);
static void set_sysqcp_nextnw (int module);
static void send_que_cmd (char *cmd, int que_id);
static void Usage(int ipMode);

/*
 * 04 oct 04 jhe added exit codes consts
 * original nonzero exit codes are left unchanged
 */
#define XC_OK 0
#define XC_USAGE 1
#define XC_ERROR 2
#define XC_TIMEOUT 3

/* ******************************************************************** */
/*									*/
/* The MAIN program							*/
/*									*/
/* ******************************************************************** */
static int data;

MAIN
{
  int rc = 0;                /* Return code      */
  int dest, i;
  int loop;
  int delay;
  short cmd;
  char pid[200];             /* command      */
  char cmd1[100];
  char shellcommand[200];
  char filename[100];
  char pclTimeStamp[32];
  char dummyfile[100] = "/ceda/bin/dummy";
  EVENT *prlOutEvent = NULL;
  BC_HEAD *prlOutBCHead = NULL;
  CMDBLK *prlOutCmdblk = NULL;
  FILE *fp;
  int ilMaxFileSize = (1024 * 1024 * 2);
  struct stat rlLfBuff;
  int iLen = 0;
  int iFound = 0;
  char pclSgsStr[16];
	int ilPrio = 1;

	if (argc < 1)
		Usage(0);

	/* Reset the cDst and cTwe parameters due to read-only locations of the strings */
	
	strcpy(cDyncTws, sgParameters.cTws);
	sgParameters.cTws = cDyncTws;
	strcpy(cDyncTwe, sgParameters.cTwe);
	sgParameters.cTwe = cDyncTwe;
	
  mod_name = "qput";
  /* 20041124 JIM: evrywhere use DBG_PATH if available, so init at start */
  pcgDbgPath= getenv("DBG_PATH");
  if (pcgDbgPath == NULL)
  {
     pcgDbgPath= constgDbgPath;
  }
  sprintf(pcgQputTmpFile,"%s/qput.tmp",pcgDbgPath);
  sprintf (pcgMyName, "%s_%05d", mod_name, getpid ());
  sprintf (pcgCedaLogFile, "%s/%s_all.log", pcgDbgPath, mod_name);

  for (i = 1; i < argc; i++)
  {
    if (argv[i] != NULL)
    {
      if (strncmp(argv[i],"debug=",6)==0)
      {
         printf("DEBUG SET to %c!\n",argv[i][6]);
         if (argv[i][6]=='0')
         {
            printf("DEBUG SWITCHED OFF\n");
            run_debug_level = 0;
         }
         else if (argv[i][6]=='1')
         {
            printf("DEBUG SWITCHED TO TRACE\n");
            run_debug_level = TRACE;
         }
         else if (argv[i][6]=='2')
         {
            printf("DEBUG SWITCHED TO DEBUG\n");
            run_debug_level = DEBUG;
         }
         else
         {
            printf("DEBUG SWITCHED TO DEFAULT TRACE\n");
            run_debug_level = TRACE;
         }
      }
    }
  }
  dbglog (TRACE, "============ NEW QPUT SESSION ===========");
  if (pfgDbgLog != NULL)
  {
    if (fstat (fileno (pfgDbgLog), &rlLfBuff) != RC_FAIL)
    {
      if (rlLfBuff.st_size >= ilMaxFileSize)
      {
        dbglog (TRACE, "SWITCHING LOGFILE (SIZE NOW = %d)", rlLfBuff.st_size);
        get_time (pclTimeStamp);
        pclTimeStamp[14] = 0x00;
        sprintf (filename, "%s/%s_till_%s.bak.log", pcgDbgPath, mod_name, pclTimeStamp);
        dbglog (TRACE, "RENAMING TO <%s>", filename);
        fclose (pfgDbgLog);
        pfgDbgLog = NULL;
        rename (pcgCedaLogFile, filename);
        dbglog (TRACE, "============ NEW QPUT SESSION ===========");
      }
    }
  }

  init ();
  i = tool_search_exco_data("SYS","HOMEAP",pclSgsStr);
  if (i==RC_SUCCESS)
  {
     strncpy(sgParameters.cTwe,pclSgsStr,3);
  }
  i = tool_search_exco_data ("ALL", "TABEND", pclSgsStr);
  if (i==RC_SUCCESS)
  {
     strncpy(&sgParameters.cTwe[4],pclSgsStr,3);
  }

  for (i = 1; i < argc; i++)
  {
    if (argv[i] != NULL)
    {
      dbglog (TRACE, "ARG(%d): <%s>", i, argv[i]);
    }
    else
    {
      dbglog (TRACE, "ARG(%d): <NULL>", i);
    }
  }
  for (i = 1; i < argc; i++)
  {
    if (argv[i] != NULL)
    {
      iFound = 0;
      pcgCurrParName=argv[i];
      pcgCurrParVal=strchr(pcgCurrParName,'=');
      /* 20041028 JIM: -h instead of -?, bash expands -? if file 'x\177'
                       exists
      */
      if (strncmp(pcgCurrParName,"-h",2) == 0)
      {
         iFound = 1; /* avoid unknown parameter message */
         rc = 1;
         if (strncmp(pcgCurrParName,"-hh",3) == 0)
         {
           rc = 2;
         }
         if (strncmp(pcgCurrParName,"-hhh",4) == 0)
         {
           rc = 3;
         }
      }
      else if (pcgCurrParVal!=NULL)
      {
         pcgCurrParVal[0]=0;
         pcgCurrParVal++;
         if (strncmp(pcgCurrParName,"que",3) == 0)
         {
            sgParameters.cQueue=pcgCurrParVal;
            iFound = 1;
         }
         else if (strncmp(pcgCurrParName,"prio",4) == 0)
         {
            sgParameters.cPriority=pcgCurrParVal;
						ilPrio = atoi(sgParameters.cPriority);
            iFound = 1;
         }
         else if ((strcmp(pcgCurrParName,"cmd") == 0) || (strcmp(pcgCurrParName,"command") == 0))
         {
            sgParameters.cCommand=pcgCurrParVal;
            iFound = 1;
         }
         else if (strncmp(pcgCurrParName,"tab",3) == 0)
         {
            sgParameters.cTable=pcgCurrParVal;
            iFound = 1;
         }
         else if ((strcmp(pcgCurrParName,"fld") == 0) || (strcmp(pcgCurrParName,"fields") == 0))
         {
            sgParameters.cFields=pcgCurrParVal;
            iFound = 1;
         }
         else if (strncmp(pcgCurrParName,"dat",3) == 0)
         {
            sgParameters.cData=pcgCurrParVal;
            iFound = 1;
         }
         else if (strncmp(pcgCurrParName,"sel",3) == 0)
         {
            sgParameters.cSelection=pcgCurrParVal;
            iFound = 1;
         }
         else if (strcmp(pcgCurrParName,"wait") == 0)
         {
            sgParameters.cTimeWait=pcgCurrParVal;
            iFound = 1;
         }
         else if ((strcmp(pcgCurrParName,"rcv") == 0) || (strcmp(pcgCurrParName,"wks") == 0))
         {
            sgParameters.cRcv=pcgCurrParVal;
            iFound = 1;
         }
         else if ((strcmp(pcgCurrParName,"dst") == 0) || (strcmp(pcgCurrParName,"user") == 0))
         {
            sgParameters.cDst=pcgCurrParVal;
            iFound = 1;
         }
         else if (strcmp(pcgCurrParName,"tws") == 0)
         {
            sgParameters.cTws=pcgCurrParVal; 
            iFound = 1;
         }
         else if (strcmp(pcgCurrParName,"twe") == 0)
         {
            sgParameters.cTwe=pcgCurrParVal;
            iFound = 1;
         }
         else if (strcmp(pcgCurrParName,"loop") == 0)
         {
            sgParameters.cLoop=pcgCurrParVal;
            iFound = 1;
         }
         else if (strcmp(pcgCurrParName,"delay") == 0)
         {
            sgParameters.cDelay=pcgCurrParVal;
            iFound = 1;
         }
         else if (strcmp(pcgCurrParName,"nap") == 0)
         {
            sgParameters.cNap=pcgCurrParVal;
            iFound = 1;
         }
         else if ((strcmp(pcgCurrParName,"debug") == 0) || (strcmp(pcgCurrParName,"dbg") == 0))
         {
            iFound = 1;
         }
      }
      else if (i==1)
      {  /* 20041108 JIM: default first 2 params as <process> and <cmd>
            "=" obsolet */
         sgParameters.cQueue=pcgCurrParName;
         iFound = 1;
      }
      else if (i==2)
      {
         sgParameters.cCommand=pcgCurrParName;
         iFound = 1;
      }
      else
      {
         dbglog (TRACE, "ERROR: missing '=' for parameter <%s> ",pcgCurrParName);
         printf ("\tqput: ERROR: missing '=' for parameter <%s>\n ",pcgCurrParName);
         iFound = 1; /* avoid unknown parameter message */
         rc = 1;
      }
      if (iFound == 0)
      {
         rc = 1;
         dbglog (TRACE, "ERROR: unknown parameter name <%s> ",pcgCurrParName);
         printf ("\tqput: ERROR: unknown parameter name <%s>\n ",pcgCurrParName);
      }
    }
  }
  dbglog (TRACE, "Module: %s",   sgParameters.cQueue);
  dbglog (TRACE, "Priority: %s", sgParameters.cPriority);
  dbglog (TRACE, "Command: %s",  sgParameters.cCommand);
  dbglog (TRACE, "Table: %s",    sgParameters.cTable);
  dbglog (TRACE, "Fields: %s",   sgParameters.cFields);
  dbglog (TRACE, "Data: %s",     sgParameters.cData);
  dbglog (TRACE, "Selection: %s",sgParameters.cSelection);
  dbglog (TRACE, "TimeWait: %s", sgParameters.cTimeWait);
  dbglog (TRACE, "Rcv: %s",      sgParameters.cRcv);
  dbglog (TRACE, "Dst: %s",      sgParameters.cDst);
  dbglog (TRACE, "Tws: %s",      sgParameters.cTws); 
  dbglog (TRACE, "Twe: %s",      sgParameters.cTwe);
  dbglog (TRACE, "Loop: %s",     sgParameters.cLoop);
  dbglog (TRACE, "Delay: %s",    sgParameters.cDelay);
  dbglog (TRACE, "Nap: %s",      sgParameters.cNap);
  if (sgParameters.cSelection[0]!=0)
  {
    if (sgParameters.cFields[0]==0)
    {
      dbglog (TRACE, "qput: selection without fields is invalid!");
      printf ("\tqput: ERROR: selection without fields is invalid!\n");
      fflush (stdout);
      cput45_terminate (XC_USAGE);
    }
    else if (sgParameters.cTable[0]==0)
    {
      dbglog (TRACE, "qput: selection without table is invalid!");
      printf ("\tqput: ERROR: selection without table is invalid!\n");
      fflush (stdout);
      cput45_terminate (XC_USAGE);
    }
  }
  if (rc == 0)
  {
    if ((argc < 2) || (sgParameters.cCommand[0]== 0))
    {
       rc = 1;
    }
  }

  if (rc > 0)
  {
     Usage(rc);
  }                             /* end if */
  
  /* 20041109 JIM: also use timeout for db-connect: */
  rc = init_cput (atoi(sgParameters.cTimeWait));
  if (rc != RC_SUCCESS)
  {
    cput45_terminate (XC_USAGE);
  }                             /* end of if */
  loop = atoi (sgParameters.cLoop);
  delay = atoi (sgParameters.cDelay);
  if (atoi (sgParameters.cQueue) == 0)
  {
    /*modname */
    dest = get_q_id (sgParameters.cQueue);
  }
  else
  {
    /*modid */
    dest = atoi (sgParameters.cQueue);
  }
  catch_all (handle_sig);       /* handles signal   */
  if (sgParameters.cSelection[0]!=0)
  {
    data = 1;
    /*this cput is used as real interface */
    dbglog (TRACE, "GOT %d ARGUMENTS. DEST=%d", argc, dest);
        handle_data_input (dest, atoi(sgParameters.cPriority), sgParameters.cCommand,
            sgParameters.cTable, sgParameters.cFields, sgParameters.cData,
            sgParameters.cSelection, atoi(sgParameters.cTimeWait),
            sgParameters.cRcv, sgParameters.cDst,
            sgParameters.cTws, sgParameters.cTwe,
            atoi(sgParameters.cLoop),atoi(sgParameters.cNap));
  }
  else
  {
    data = 0;
    /*this is only for shorttype commands */
    if ((!strcmp (sgParameters.cCommand, "-1")) || (!strcmp (sgParameters.cCommand, "shutdown")))
    {
      rc = -1;
      cmd = SHUTDOWN;
    }
    if ((!strcmp (sgParameters.cCommand, "-2")) || (!strcmp (sgParameters.cCommand, "reset")))
    {
      rc = -2;
      cmd = RESET;
    }
#ifdef WMQ
    { 
       char TmpCmd [64] ;
       sprintf (TmpCmd,",%s,",sgParameters.cCommand);
      if (strstr(",-99,-100,-101,-102,-103,-104,-106,-110044,",TmpCmd)!=NULL)
      {
        dbglog (TRACE, "qput: ERROR: SYSQCP command not supported (build for WMQ environment)!");
        printf ("qput: ERROR: SYSQCP command not supported (build for WMQ environment)!\n");
        fflush (stdout);
        cput45_terminate (XC_USAGE);
      }
    }
#endif
    if (!strcmp (sgParameters.cCommand, "-99"))
      one_queue_overview (dest);
    if (!strcmp (sgParameters.cCommand, "-100"))
      queue_overview (0, 0);
    if (!strcmp (sgParameters.cCommand, "-101"))
      queue_overview (1, 0);
    if (!strcmp (sgParameters.cCommand, "-102"))
      queue_overview (2, 0);
    if (!strcmp (sgParameters.cCommand, "-103"))
      queue_overview (3, 0);
    if (!strcmp (sgParameters.cCommand, "-104"))
      queue_overview (4, atoi (sgParameters.cQueue));
    /* 20041026 JIM: removed, TRACE / DEBUG to SYSQCP or '1' mapped internal */
    /* if (!strcmp (sgParameters.cCommand, "-105"))
     * set_sysqcp_dbg (atoi (sgParameters.cQueue));
     */
    if (!strcmp (sgParameters.cCommand, "-106"))
      set_sysqcp_nextnw (atoi (sgParameters.cQueue));
    if (!strcmp (sgParameters.cCommand, "-110044"))
      queue_overview (5, atoi (sgParameters.cQueue));

    if (!strcmp (sgParameters.cCommand, "-3"))
    {
      if (atoi (sgParameters.cQueue) != 0)
      {
        printf ("\n\tUSE ASSOCIATED NAME\n");
        fflush (stdout);
        cput45_terminate (XC_USAGE);
      }
      fp = fopen ("/ceda/bin/dummy", "r");
      if (fp == NULL)
      {
        fclose (fp);
        fflush (fp);
        printf ("\nFIRST CREATE \"dummy\" FILE !!!\n");
        fflush (stdout);
        cput45_terminate (XC_USAGE);
      }
      fclose (fp);
      fflush (fp);
      sprintf (filename, "/ceda/bin/%s.qput", sgParameters.cQueue);
      fp = fopen (filename, "r");
      if (fp != NULL)
      {
        fclose (fp);
        fflush (fp);
        printf ("\norg file %s.qput exists save first !!! and remove it\n", sgParameters.cQueue);
        fflush (stdout);
        cput45_terminate (XC_USAGE);
      }
      sprintf (filename, "/ceda/bin/%s", sgParameters.cQueue);
      rename (filename, "/ceda/bin/test.qput.save");
      rename (dummyfile, filename);
      sprintf (shellcommand, "ps -e | grep %s | awk ' { printf $1\",\" > \"%s\"}'", sgParameters.cQueue,pcgQputTmpFile);
      fp = popen (shellcommand, "w");
      pclose (fp);
      fp = fopen (pcgQputTmpFile, "r");
      if (fp != NULL)
      {
        fscanf (fp, "%s", pid);
        fclose (fp);
        fflush (fp);
      }
      pid[strlen (pid) - 1] = '\0';
      if (strchr (pid, ',') == NULL)
      {
        kill (atoi (pid), 9);
        printf ("\n\tkill pid= %s\n", pid);
        fflush (stdout);
        printf ("\n\t%s runs as dummy\n\n", sgParameters.cQueue);
        fflush (stdout);
        sleep (2);
      }
      else
      {
        printf ("\n\tFOUND MORE THAN ONE PID = %s MODNAME IS NOT UNIQUE\n\n", pid);
        fflush (stdout);
      }
      rename (filename, dummyfile);
      rename ("/ceda/bin/test.qput.save", filename);
      remove (pcgQputTmpFile);
      cput45_terminate (XC_USAGE);
    }
    if (!strcmp (sgParameters.cCommand, "-9") || !strcmp (sgParameters.cCommand, "-15"))
    {
      if (atoi (sgParameters.cQueue) != 0)
      {
        printf ("\n\tUSE ASSOCIATED NAME\n");
        fflush (stdout);
        cput45_terminate (XC_USAGE);
      }
      sprintf (shellcommand, "ps -e | grep %s | awk ' { printf $1\",\" > \"%s\"}'", sgParameters.cQueue,pcgQputTmpFile);
      fp = popen (shellcommand, "w");
      pclose (fp);
      fp = fopen (pcgQputTmpFile, "r");
      if (fp != NULL)
      {
        fscanf (fp, "%s", pid);
        fclose (fp);
        fflush (fp);
      }
      pid[strlen (pid) - 1] = '\0';
      if (strchr (pid, ',') == NULL)
      {
        kill (atoi (pid), atoi (sgParameters.cCommand) * -1);
        printf ("\n\tkill %d  %s \n\n", atoi (sgParameters.cCommand), pid);
        fflush (stdout);
      }
      else
      {
        printf ("\n\tFOUND MORE THAN ONE PID = %s MODNAME IS NOT UNIQUE\n\n", pid);
        fflush (stdout);
      }
      remove (pcgQputTmpFile);
      cput45_terminate (XC_USAGE);
    }
    if (rc >= 0 && atoi (sgParameters.cCommand) == 0)
    {
#ifndef WMQ
      if (strstr(",go,hold,empty,next_flag,ack,que_get,",sgParameters.cCommand)!= NULL)
      {  /* 20041124 JIM: added commands "go,hold,empty,next_flag,ack,que_get" */
        send_que_cmd (sgParameters.cCommand,dest);
        cput45_terminate (XC_OK);
      }
#endif      
      strcpy (cmd1, sgParameters.cCommand);
      iLen = sizeof (EVENT) + sizeof (BC_HEAD) + sizeof (CMDBLK);
      prlOutEvent = (EVENT *) malloc ((size_t) iLen);
      memset ((void *) prlOutEvent, 0x00, iLen);
      /* set event structure... */
      prlOutEvent->type = SYS_EVENT;
      prlOutEvent->command = EVENT_DATA;
      prlOutEvent->originator = 9;
      prlOutEvent->retry_count = 0;
      prlOutEvent->data_offset = 0;
      prlOutEvent->data_length = 0;
      prlOutBCHead = (BC_HEAD *) ((char *) prlOutEvent + sizeof (EVENT));
      prlOutCmdblk = (CMDBLK *) ((char *) prlOutBCHead->data);
      strcpy (prlOutCmdblk->command, cmd1);
      for (i = 0; i < loop; i++)
      {
        if ((rc = que (QUE_PUT, dest, 9, ilPrio, iLen, (char *) prlOutEvent)) != RC_SUCCESS)
        {
          handle_qerr (rc);
          cput45_terminate (XC_ERROR);
        }
        else
        {
          printf ("\n\tsend %d string command ok\n", i + 1);
          fflush (stdout);
        }                       /* end if */
        sleep (delay);
      }
    }
    else if (rc >= 0)
    {
      cmd = atoi (sgParameters.cCommand);
      if ((strcmp(sgParameters.cQueue,"sysqcp")==0) ||
          (strcmp(sgParameters.cQueue,"1")==0))
      { /* 20041026 JIM: TRACE, DEBUG to SYSQCP or '1' mapped internal here: */
         set_sysqcp_dbg (cmd);
      }
      else
      {
         event.type = SYS_EVENT;
         event.command = cmd;
         event.originator = 9;
         event.retry_count = 0;
         event.data_offset = 0;
         event.data_length = 0;
         for (i = 0; i < loop; i++)
         {
           if ((rc = que (QUE_PUT, dest, 9, ilPrio, sizeof (EVENT), (char *) &event)) != RC_SUCCESS)
           {
             handle_qerr (rc);
             cput45_terminate (XC_ERROR);
           }
           else
           {
             printf ("\n\tsend %d number command ok\n", i + 1);
             fflush (stdout);
           }                       /* end if */
           sleep (delay);
         }
      }
    }
    if (rc == -1 || rc == -2)
    {
      event.type = SYS_EVENT;
      event.command = cmd;
      event.originator = 9;
      event.retry_count = 0;
      event.data_offset = 0;
      event.data_length = 0;
      if ((rc = que (QUE_PUT, dest, 9, ilPrio, sizeof (EVENT), (char *) &event)) != RC_SUCCESS)
      {
        handle_qerr (rc);
        cput45_terminate (XC_ERROR);
      }
      else
      {
        printf ("\n\tsend reset or shutdown ok\n");
        fflush (stdout);
      }                         /* end if */
    }
  }
  cput45_terminate (XC_OK);
  return 0;
}                               /* end of MAIN */

/* ******************************************************************** */
/* The initialization routine						*/
/* ******************************************************************** */
/* 20041109 JIM: ipTimeWait: if timeout given, use also for db-connect: */
static int init_cput (int ipTimeWait)
{
  int cnt = 0;

  /* Attach to the SYMAP queues */
  while ((init_que () != 0) && (cnt <= ipTimeWait))
  {
    cnt++;
    sleep (1);                  /* Wait for QCP to create queues */
  }                             /* end while */

  if (cnt > ipTimeWait + 1)
  {
    dbglog (TRACE, "ERROR: UNABLE TO INIT QUEUES");
    printf ("QPUT: ERROR: unable to init que!\n");
    fflush (stdout);
    return (FATAL);
  }                             /* end of if */

  dbglog (TRACE, "ATTACHED TO CEDA MSG QUEUES");
  return (RC_SUCCESS);

}                               /* end of initialize */

/* ******************************************************************** */
/* The handle signals routine						*/
/* ******************************************************************** */
static void handle_sig ()
{

  return;

}                               /* end of handle_sig */

/* ******************************************************************** */
/* The handle general error routine					*/
/* ******************************************************************** */
/* static void handle_err(int err)
{
	return;

    }*//* end of handle_err */

/* ******************************************************************** */
/* The handle queuing error routine					*/
/* ******************************************************************** */
static void handle_qerr (int err)
{
  switch (err)
  {
    case QUE_E_FUNC:           /* Unknown function */
      printf ("\nQPUT ERROR:%d Unknown function\n", err);
      fflush (stdout);
      break;
    case QUE_E_MEMORY:         /* Malloc reports no memory */
      printf ("\nQPUT ERROR:%d Malloc reports no memory\n", err);
      fflush (stdout);
      break;
    case QUE_E_SEND:           /* Error using msgsnd */
      printf ("\nQPUT ERROR:%d Error using msgsnd\n", err);
      fflush (stdout);
      break;
    case QUE_E_GET:            /* Error using msgrcv */
      printf ("\nQPUT ERROR:%d Error using msgrcv\n", err);
      fflush (stdout);
      break;
    case QUE_E_EXISTS:         /* Route/Queue exists */
      printf ("\nQPUT ERROR:%d Route/Queue exists\n", err);
      fflush (stdout);
      break;
    case QUE_E_NOFIND:         /* Not found ( ex. route ) */
      printf ("\nQPUT ERROR:%d Not found ( ex. route )\n", err);
      fflush (stdout);
      break;
    case QUE_E_ACKUNEX:        /* Unexpected ACK received */
      printf ("\nQPUT ERROR:%d Unexpected ACK received\n", err);
      fflush (stdout);
      break;
    case QUE_E_STATUS:         /* Unknown queue status */
      printf ("\nQPUT ERROR:%d Unknown queue status\n", err);
      fflush (stdout);
      break;
    case QUE_E_INACTIVE:       /* Queue is inactive */
      printf ("\nQPUT ERROR:%d Queue is inactive\n", err);
      fflush (stdout);
      break;
    case QUE_E_MISACK:         /* Missing ACK */
      printf ("\nQPUT ERROR:%d Missing ACK\n", err);
      fflush (stdout);
      break;
    case QUE_E_NOQUEUES:       /* The queues don't exist */
      printf ("\nQPUT ERROR:%d The queues don't exist\n", err);
      fflush (stdout);
      break;
    case QUE_E_RESP:           /* No response on CREATE */
      printf ("\nQPUT ERROR:%d No response on CREATE\n", err);
      fflush (stdout);
      break;
    case QUE_E_FULL:           /* Table full */
      printf ("\nQPUT ERROR:%d Table full\n", err);
      fflush (stdout);
      break;
    case QUE_E_NOMSG:          /* No message on queue */
      printf ("\nQPUT ERROR:%d No message on queue\n", err);
      fflush (stdout);
      break;
    case QUE_E_INVORG:         /* Mod id by que call is 0 */
      printf ("\nQPUT ERROR:%d Mod id by que call is 0\n", err);
      fflush (stdout);
      break;
    case QUE_E_NOINIT:         /* Queues is not initialized */
      printf ("\nQPUT ERROR:%d Queues is not initialized\n", err);
      fflush (stdout);
      break;
    default:                   /* Unknown queue error */
      printf ("\nQPUT ERROR:%d\n", err);
      fflush (stdout);
      break;
  }                             /* end switch */
  return;

}                               /* end of handle_qerr */
static int get_q_id (char *Pnam)
{
  int rc = RC_SUCCESS;       /* Return code                  */
  int recno = 0;
  char buf[30];
  int cnt = 0;
  int ready = FALSE;

  rc = sgs_get_no_of_record (PNTAB, buf);

  if (rc > 0)
  {
    cnt = rc;
    rc = RC_SUCCESS;
  }
  else
  {
    rc = RC_FAIL;
  }                             /* fi */

  for (recno = 0; !ready && rc == RC_SUCCESS && recno < cnt; recno++)
  {
    rc = sgs_get_record (PNTAB, recno, buf);
    if (rc == RC_SUCCESS)
    {
      if (strncmp (buf, Pnam, strlen (Pnam)) == 0)
      {
        ready = TRUE;
#if defined(_SNI) || defined(_HPUX_SOURCE) || defined(_SOLARIS)|| defined(_LINUX)
        rc = *((short *) (buf + 8));
#else
        rc = *((int *) (buf + 8));
#endif
      }                         /* fi */
    }                           /* fi */
  }                             /* for */
  printf ("\n\tsend to %s -> %d \n", Pnam, rc);
  fflush (stdout);
  return rc;
}                               /* tool_get_q_id */

static void one_queue_overview (int pmod_id)
{
  ITEM item1;
  ITEM *item;
  QUE_INFO *que_info;
  int tag;
  int len;
  int return_value;

  /* 20070226 JIM: on AIX the pids are not continously increasing but random, so the
   * modulo may create same tag at 2 simultaniosly starting NETINs. Use complete PID
   * tag = ((getpid()%1000)+30000);
   */
  tag = getpid()+30000;
  dbglog (TRACE, "NOW ONE_QUEUE_OVERVIEW (CREATE QUEUE) with tag <%d>", tag);
  if (que (QUE_CREATE, 0, tag, 0, 5, "CPUT") != RC_SUCCESS)
  {
    dbglog (TRACE, "ERROR ON QUE_CREATE");
    cput45_terminate (XC_ERROR);
  }
  alarm (2);
  if (que (QUE_RESP, 0, tag, 0, sizeof (ITEM), (char *) &item1) != RC_SUCCESS)
  {
    dbglog (TRACE, "ERROR ON QUE_RESP");
    cput45_terminate (XC_ERROR);
  }                             /* end if */
  alarm (0);

  mod_id = item1.originator;
  dbglog (TRACE, "ATTACHED TO ANSWER QUEUE (%d)", mod_id);

  /* Request the queue overview */
  dbglog (TRACE, "SENDING QUE_O_QOVER...");

  return_value = que (QUE_O_QOVER, pmod_id, mod_id, 5, 0, 0);
  if (return_value != RC_SUCCESS)
  {
    dbglog (TRACE, "DELETING QUEUE %d", mod_id);
    que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
    cput45_terminate (XC_ERROR);
  }                             /* end if */
  len = sizeof (ITEM) + sizeof (QUE_INFO) + (sizeof (QINFO) * 200);
  item = (ITEM *) malloc (len);
  if (item == (ITEM *) NUL)
  {
    dbglog (TRACE, "DELETING QUEUE %d", mod_id);
    que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
    cput45_terminate (XC_ERROR);
  }                             /* end if */

  if (que (QUE_GET, 0, mod_id, 5, len, (char *) item) != RC_SUCCESS)
  {
    dbglog (TRACE, "DELETING QUEUE %d", mod_id);
    que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
    free ((char *) item);

    cput45_terminate (XC_ERROR);
  }                             /* end if */

  if (item->msg_length == FATAL)
  {
    dbglog (TRACE, "DELETING QUEUE %d", mod_id);
    que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
    free ((char *) item);
    cput45_terminate (XC_ERROR);
  }                             /* end if */
  dbglog (TRACE, "DELETING QUEUE %d", mod_id);
  que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
  que_info = (QUE_INFO *) & (item->text[0]);
  printf (QO_INFO (que_info->data[0]));
  fflush (stdout);
  printf ("\n");
  cput45_terminate (XC_OK);
}
   
static int handle_data_input (int ipDestination, int ipPriority, char *pcpCommand, char *pcpTable, char *pcpFields,
                              char *pcpData, char *pcpSelection, int ipTimeWait, char *pcpRcv, char *pcpDst,
                              char *pcpTws, char *pcpTwe, int ipLoop, int ipNap)
{
  int ilRc = RC_FAIL;
  int ilLen = 0;
  char *pclSelection = NULL;
  char *pclFields = NULL;
  char *pclData = NULL;
  ITEM item1;
  ITEM *item2 = NULL;
  EVENT *prlOutEvent = NULL;
  BC_HEAD *prlOutBCHead = NULL;
  CMDBLK *prlOutCmdblk = NULL;
  EVENT *prlEvent = NULL;
  BC_HEAD *prlBCHead = NULL;
  CMDBLK *prlCmdblk = NULL;
  int tag;
	int ilNWcounter = 0;

  dbglog (TRACE, "NOW HANDLE DATA INPUT (CREATE QUEUE)");
  mod_id = 9999;

  if (ipTimeWait >= 0)
  {
    /* 20070226 JIM: on AIX the pids are not continously increasing but random, so the
    * modulo may create same tag at 2 simultaniosly starting NETINs. Use complete PID
    * tag = ((getpid()%1000)+30000);
    */
    tag = getpid()+30000;
    if (que (QUE_CREATE, 0, tag, 0, 5, "CPUT") != RC_SUCCESS)
    {
      dbglog (TRACE, "ERROR ON QUE_CREATE");
      cput45_terminate (XC_ERROR);
    }
    alarm (2);
    if (que (QUE_RESP, 0, tag, 0, sizeof (ITEM), (char *) &item1) != RC_SUCCESS)
    {
      dbglog (TRACE, "ERROR ON QUE_RESP");
      cput45_terminate (XC_ERROR);
    }                           /* end if */
    alarm (0);
    mod_id = item1.originator;
    dbglog (TRACE, "ATTACHED TO ANSWER QUEUE (%d)", mod_id);
  }
  else
  {
    dbglog (TRACE, "NO ANSWER EXPECTED. USING MOD_ID (%d)", mod_id);
  }
  pclData= strstr(pcpTws,"<mod>");
  if (pclData!=NULL)
  { /* replace <mod> by mod_id (pclData is shortly misused...) */
    sprintf(pclData,"%05d",mod_id);
    pclData= NULL;
  }

  dbglog (TRACE, "DST: (%d)", ipDestination);
  dbglog (TRACE, "PRI: (%d)", ipPriority);
  dbglog (TRACE, "TIM: (%d)", ipTimeWait);
  dbglog (TRACE, "WKS: <%s>", pcpRcv);
  dbglog (TRACE, "USR: <%s>", pcpDst);
  dbglog (TRACE, "TWS: <%s>", pcpTws);
  dbglog (TRACE, "TWE: <%s>", pcpTwe);
  dbglog (TRACE, "CMD: <%s>", pcpCommand);
  dbglog (TRACE, "TBL: <%s>", pcpTable);
  dbglog (TRACE, "FLD: <%s>", pcpFields);
  dbglog (TRACE, "DAT: <%s>", pcpData);
  dbglog (TRACE, "SEL: <%s>", pcpSelection);

  /* size-calculation for prlOutEvent */
  ilLen =
    sizeof (EVENT) + sizeof (BC_HEAD) + sizeof (CMDBLK) + strlen (pcpSelection) + strlen (pcpFields) +
    strlen (pcpData) + 128;
  /* memory for prlOutEvent */
  if ((prlOutEvent = (EVENT *) malloc ((size_t) ilLen)) == NULL)
  {
    prlOutEvent = NULL;
    /* 20041117 JIM: avoid further work if malloc fails */
    dbglog (TRACE, "malloc prlOutEvent[%d] failed", ilLen);
    printf("\nmalloc prlOutEvent[%d] failed!!!\n", ilLen);
    return RC_FAIL;
  }
  /* clear whole outgoing event */
  memset ((void *) prlOutEvent, 0x00, ilLen);
  /* set event structure... */
  prlOutEvent->type = SYS_EVENT;
  prlOutEvent->command = EVENT_DATA;
  prlOutEvent->originator = (short) mod_id;
  prlOutEvent->retry_count = 0;
  prlOutEvent->data_offset = sizeof (EVENT);
  prlOutEvent->data_length = ilLen - sizeof (EVENT);
  /* BC_HEAD-Structure... */
  prlOutBCHead = (BC_HEAD *) ((char *) prlOutEvent + sizeof (EVENT));
  if (ipTimeWait >= 0)
  {
    /* prlOutBCHead->rc = (short)RC_SUCCESS; */
    prlOutBCHead->rc = (short) 0; /*spaeter nur bei disconnect */
  }
  else
  {
    prlOutBCHead->rc = NETOUT_NO_ACK;
  }
  strncpy (prlOutBCHead->dest_name, pcpDst, 10);
  strncpy (prlOutBCHead->recv_name, pcpRcv, 10);
  /* Cmdblk-Structure... */
  prlOutCmdblk = (CMDBLK *) ((char *) prlOutBCHead->data);
  strcpy (prlOutCmdblk->command, pcpCommand);
  if (pcpTable != NULL)
  {
    strcpy (prlOutCmdblk->obj_name, pcpTable);
  }
  /* setting tw_x entries */
  strncpy (prlOutCmdblk->tw_start, pcpTws, 32);
  strncpy (prlOutCmdblk->tw_end, pcpTwe, 32);
  /* means that no additional structure is used */
  /* STANDARD CEDA-ipcs between CEDA-processes */

  /* setting selection inside event */
  strcpy (prlOutCmdblk->data, pcpSelection);
  /* setting field-list inside event */
  strcpy (prlOutCmdblk->data + strlen (pcpSelection) + 1, pcpFields);
  /* setting data-list inside event */
  strcpy ((prlOutCmdblk->data + (strlen (pcpSelection) + 1) + (strlen (pcpFields) + 1)), pcpData);

  while (ipLoop-- > 0)
  {

    ilNWcounter = 0;
    dbglog (TRACE, "SENDING EVENT TO %d", ipDestination);
    if ((ilRc = que (QUE_PUT, ipDestination, mod_id, ipPriority, ilLen, (char *) prlOutEvent)) != RC_SUCCESS)
    {
      dbglog (TRACE, "GOT QUEUE ERROR %d", ilRc);
      dbglog (TRACE, "DELETING QUEUE %d", mod_id);
      que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
      printf ("SendEvent: QUE_PUT returns: <%d>\n", ilRc);
      cput45_terminate (XC_ERROR);
    }

/*  printf ("TimeWait=%d\n", ipTimeWait); */

    if (ipTimeWait >= 0)
    {
      int ilRc;
      time_t tlStart = time (NULL);

#ifndef WMQ
      dbglog (TRACE, "USING ALARM(%d)", ipTimeWait);
      alarm(ipTimeWait);
      ilRc = que (QUE_GETBIG, 0, mod_id, 4, 0, (char *) &item2);
      alarm(0);
#else
      dbglog (TRACE, "WAITING FOR ANSWER (WAKEUP=%d)", ipTimeWait);
      do
      {
        ilRc = que (QUE_GETBIGNW, 0, mod_id, 4, 0, (char *) &item2);
        if (ilRc == QUE_E_NOMSG)
        {
          time_t tlNow = time (NULL);
          if (tlNow - tlStart >= ipTimeWait)
          {
            printf ("\nNo Answer after %d  Start %d Now %d Now-Start %d sec.\n", ipTimeWait, tlStart, tlNow,
                    tlNow - tlStart);
            dbglog (TRACE, "No Answer after %d  Start %d Now %d Now-Start %d sec.", ipTimeWait, tlStart, tlNow,
                    tlNow - tlStart);
            dbglog (TRACE, "DELETING QUEUE %d", mod_id);
            que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
            cput45_terminate (XC_TIMEOUT);
          }
          if (ipNap > 0)
          {
             nap(ipNap); 
          }
					ilNWcounter++;
        }
        else if (ilRc == RC_SUCCESS)
        {
           dbglog (TRACE, "needed %d retries ",ilNWcounter);
        }
      }
      while (ilRc == QUE_E_NOMSG);
#endif
      if (ilRc != RC_SUCCESS)
      {
        dbglog (TRACE, "NO RESPONSE AFTER %d SECONDS", ipTimeWait);
        printf ("\nQCP failure %d\n", ipTimeWait);
        handle_qerr (ilRc);
        dbglog (TRACE, "DELETING QUEUE %d", mod_id);
        que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
        cput45_terminate (XC_ERROR);
      }                         /* end if */

      /* 20041124 JIM: added QUE_ACK */
      ilRc = que(QUE_ACK, 0, mod_id, 0, 0, NULL); 
      if (ilRc != RC_SUCCESS)
      {
         dbglog (TRACE, "QUE_ACK failed: <%d>",ilRc);
         printf ("\nQUE_ACK failed: <%d>",ilRc);
         dbglog (TRACE, "DELETING QUEUE %d", mod_id);
         que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
         cput45_terminate (XC_ERROR);
      }

      if (item2->msg_length == FATAL)
      {
        dbglog (TRACE, "item2->msg_length == FATAL");
        dbglog (TRACE, "DELETING QUEUE %d", mod_id);
        que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
        printf ("\nNo Answer after %d sec.", ipTimeWait);
        cput45_terminate (XC_TIMEOUT);
      }                           /* end if */
      dbglog (TRACE, "GOT THE ANSWER");
      if (item2 != NULL)
      {
        prlEvent = (EVENT *) item2->text;
        prlBCHead = (BC_HEAD *) ((char *) prlEvent + sizeof (EVENT));
        prlCmdblk = (CMDBLK *) ((char *) prlBCHead->data);
        pclSelection = prlCmdblk->data;
        pclFields = (char *) pclSelection + strlen (pclSelection) + 1;
        pclData = (char *) pclFields + strlen (pclFields) + 1;
        dbglog (TRACE, "ANSW SEL: <%s>", pclSelection);
        dbglog (TRACE, "ANSW FLD: <%s>", pclFields);
        dbglog (TRACE, "ANSW DAT: <%s>", pclData);
        printf ("\n%s\n", pclData);
      }
      else
      {
        printf ("\nNo Answer after %d sec.", ipTimeWait);
      }
    }
  } /* for ipLoop */

  /* free memory */
  free ((void *) prlOutEvent);
  if (ipTimeWait >= 0) /* 20050913 JIM: Only if queue created, delete it */
  {
    dbglog (TRACE, "DELETING QUEUE %d", mod_id);
    que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
  }
  /*
     else{

     dbglog(TRACE,"DELETING QUEUE %d", mod_id);
     que(QUE_DELETE,mod_id,mod_id,0,0,0);
     }
   */

  fflush (stdout);

  return ilRc;
}

/* ******************************************************************** */
/*									*/
/* The following routine displays the queue overview.			*/
/*									*/
/* ******************************************************************** */
/* 20041104 JIM: queue_overview uses SYSQCP_stat.log.<modd> to complete list */
static void queue_overview (int ipWhat, int ipDest)
{
  ITEM *item;
  ITEM item1;
  QUE_INFO *que_info;
  QUE_INFO *que_info2;
  char pclTmp[128];
  int info_count;
  int rc;
  int return_value;
  int tag;
  int ilMaxValid;
  int ilSomeFound;
  FILE *fp;
  char pclLF[4];
  char pcgQueueDumpName[200];

  /* 20040707 JIM: SYSQCP legt nur I_SIZE Bytes auf die Unix-Queue, daher
     kann es sein, da���die angebliche Anzahl der Queue-Infos 
     nicht mit der wirklich gelieferten Datenmenge bereinstimmt
   */
  ilMaxValid = (I_SIZE - sizeof (ITEM) - sizeof (QUE_INFO)) / sizeof (QINFO);

   /* 20070226 JIM: on AIX the pids are not continously increasing but random, so the
   * modulo may create same tag at 2 simultaniosly starting NETINs. Use complete PID
   * tag = ((getpid()%1000)+30000);
   */
  tag = getpid()+30000;
  dbglog (TRACE, "NOW QUEUE_OVERVIEW (CREATE QUEUE)");
  if (que (QUE_CREATE, 0, tag, 0, 5, "CPUT") != RC_SUCCESS)
  {
    dbglog (TRACE, "ERROR ON QUE_CREATE");
    cput45_terminate (XC_ERROR);
  }
  alarm (2);
  if (que (QUE_RESP, 0, tag, 0, sizeof (ITEM), (char *) &item1) != RC_SUCCESS)
  {
    dbglog (TRACE, "ERROR ON QUE_RESP");
    cput45_terminate (XC_ERROR);
  }                             /* end if */
  alarm (0);
  mod_id = item1.originator;
  dbglog (TRACE, "ATTACHED TO ANSWER QUEUE (%d)", mod_id);
  /* Request the queue overview */
  dbglog (TRACE, "SENDING QUE_QOVER...");

  errno = 10;
  return_value = que (QUE_QOVER, 0, mod_id, 1, 0, 0);
  if (return_value != RC_SUCCESS)
  {
    dbglog (TRACE, "DELETING QUEUE %d", mod_id);
    que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
    cput45_terminate (XC_ERROR);

  }                             /* end if */
/* 20040706 JIM: Allocation will be done in que(QUE_GETBIG,....  :
  len = sizeof(ITEM) + sizeof(QUE_INFO) + (sizeof(QINFO) * 200);
  item = (ITEM *) malloc(len);
  if ( item == (ITEM *) NUL ) 
    {
      dbglog(TRACE,"DELETING QUEUE %d", mod_id);
      que(QUE_DELETE,mod_id,mod_id,0,0,0);
      cput45_terminate(XC_OK);
    } * end if *
	
  if ( que(QUE_GET,0,mod_id,5,len,(char *)item) != RC_SUCCESS )
*/
  item = NULL;                  /* 20040706 JIM: flag to allocate item in que(QUE_GETBIG,.... */
  if (que (QUE_GETBIG, 0, mod_id, 5, 0, (char *) &item) != RC_SUCCESS)
  {
    free ((char *) item);
    dbglog (TRACE, "DELETING QUEUE %d", mod_id);
    que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
    cput45_terminate (XC_ERROR);
  }                             /* end if */

  if (item->msg_length == FATAL)
  {
    free ((char *) item);
    dbglog (TRACE, "DELETING QUEUE %d", mod_id);
    que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
    cput45_terminate (XC_ERROR);
  }                             /* end if */

  que_info = (QUE_INFO *) & (item->text[0]);
  info_count = 0;
/*    line_count = 3; */
  /* 20041103 JIM: new: we rely on file, not queue data */
  sprintf(pcgQueueDumpName,"%s/SYSQCP_Stat.log.%05d",pcgDbgPath,mod_id);
  dbglog (TRACE, "reading file <%s>....", pcgQueueDumpName);

  fp = fopen (pcgQueueDumpName, "r");
  if (fp == NULL)
  {
     dbglog (TRACE, "reading file <%s> failed ....", pcgQueueDumpName);
     sprintf(pcgQueueDumpName,"%s/SYSQCP_Stat.log",pcgDbgPath);
     dbglog (TRACE, "reading file <%s>....", pcgQueueDumpName);
  }
  fp = fopen (pcgQueueDumpName, "r");
  if (fp != NULL)
  {
     /* Overview sent by Queue is filtered (count of NETIF, BCQ, CDR), but file is complete */
     /* so use file content and add summary lines */
     fscanf (fp, "%[^\n]%c", pclTmp,pclLF);
     /*printf ("%s\n",pclTmp);*/
     info_count=atoi(strchr(pclTmp,':')+1);
     /*printf ("allocating: %i\n",info_count);*/
     que_info2 = (QUE_INFO *) malloc(sizeof(QUE_INFO) + (sizeof(QINFO) * (info_count + 3)));
     memset(que_info2,0,sizeof(QUE_INFO) + (sizeof(QINFO) * (info_count + 3)));
     que_info2->entries= info_count;
     info_count = 0;
     while (info_count < que_info2->entries)
     {
       fscanf (fp, "%[^\n]%c", pclTmp,pclLF);
       que_info2->data[info_count].id= atoi(strtok(pclTmp," "));
       strncpy(que_info2->data[info_count].name,strtok(NULL," "),MAX_PRG_NAME);
       que_info2->data[info_count].nbr_items[0]= atoi(strtok(NULL," "));
       que_info2->data[info_count].nbr_items[1]= atoi(strtok(NULL," "));
       que_info2->data[info_count].nbr_items[2]= atoi(strtok(NULL," "));
       que_info2->data[info_count].nbr_items[3]= atoi(strtok(NULL," "));
       que_info2->data[info_count].nbr_items[4]= atoi(strtok(NULL," "));
       que_info2->data[info_count].status= atol(strtok(NULL," "));
       que_info2->data[info_count].msg_count= atoi(strtok(NULL," "));
       /* name now is truncated, fill up to MAX_PRG_NAME */
       strcpy(pclTmp,que_info2->data[info_count].name);
       memset(que_info2->data[info_count].name,' ',MAX_PRG_NAME);
       que_info2->data[info_count].name[MAX_PRG_NAME+1]=0;
       strncpy(que_info2->data[info_count].name,pclTmp,strlen(pclTmp));
       /*printf ("%d: %s\n",info_count,que_info2->data[info_count].name);*/
       info_count++;
     }                         /* end while */
     fclose (fp);
     remove(pcgQueueDumpName);
     
     tag=  (min (ilMaxValid, que_info->entries))-2;
     /* printf ("old number: %i\n",tag); */
     if (tag>0)
     {
       while (info_count < que_info2->entries+3)
       {
       que_info2->data[info_count].id=              que_info->data[tag].id;
       strcpy(que_info2->data[info_count].name,     que_info->data[tag].name);
       que_info2->data[info_count].nbr_items[0]=    que_info->data[tag].nbr_items[0]; 
       que_info2->data[info_count].nbr_items[1]=    que_info->data[tag].nbr_items[1]; 
       que_info2->data[info_count].nbr_items[2]=    que_info->data[tag].nbr_items[2]; 
       que_info2->data[info_count].nbr_items[3]=    que_info->data[tag].nbr_items[3]; 
       que_info2->data[info_count].nbr_items[4]=    que_info->data[tag].nbr_items[4]; 
       que_info2->data[info_count].status=          que_info->data[tag].status;
       que_info2->data[info_count].msg_count=       que_info->data[tag].msg_count;
       /* printf ("%d: %s\n",info_count,que_info2->data[info_count].name); */
       info_count++;
       tag++;
       }
       que_info2->entries+=3;
     }
     que_info= que_info2;
  }
  else
  {
     if (que_info->entries > ilMaxValid)
     {
         que_info->entries= ilMaxValid;
	       if(ipWhat==0)
         {
	          printf("Only %d queues processed! \n",ilMaxValid);
         }
     }
  }
  info_count = 0;
  switch (ipWhat)
  {
    case 0:
      while (info_count < que_info->entries)
      {
        printf (QO_INFO (que_info->data[info_count]));
        info_count++;
        /*        line_count++; */
      }                         /* end while */
      printf ("\n");

      free ((char *) que_info2);
      free ((char *) item);
      break;
    case 1:
      printf ("\nDelete all CPUT queues with STAT==5 ....\n");
      ilSomeFound = 0;
      while (info_count < que_info->entries)
      {
        strcpy (pclTmp, que_info->data[info_count].name);
        TrimRight (pclTmp);
        if (!strcmp (pclTmp, "CPUT") && que_info->data[info_count].status == 5)
        {
          rc = delete_queue (que_info->data[info_count].id, mod_id);
          if (rc != RC_SUCCESS)
          {
            printf ("\nDelete queue %d of %s: Error %d \n", que_info->data[info_count].id,
                    que_info->data[info_count].name, rc);
          }
          else
          {
            printf ("\nDeleted queue %d of %s \n", que_info->data[info_count].id, que_info->data[info_count].name);
            ilSomeFound = 1;
          }
        }
        info_count++;
        /*        line_count++; */
      }                         /* end while */
      printf ("\n");
      if (ilSomeFound == 0)
      {
        printf ("No appliable queues of CPUT found!\n");
      }
      free ((char *) que_info2);
      free ((char *) item);
      break;
    case 2:
      printf ("\nDelete all CPUT queues....\n");
      ilSomeFound = 0;
      while (info_count < que_info->entries)
      {
        strcpy (pclTmp, que_info->data[info_count].name);
        TrimRight (pclTmp);
        if (!strcmp (pclTmp, "CPUT") && que_info->data[info_count].id != mod_id)
        {
          rc = delete_queue (que_info->data[info_count].id, mod_id);
          if (rc != RC_SUCCESS)
          {
            printf ("\nQueue %d of %s: Delete Error %d\n", que_info->data[info_count].id,
                    que_info->data[info_count].name, rc);
          }
          else
          {
            printf ("\nQueue %d of %s deleted\n", que_info->data[info_count].id, que_info->data[info_count].name);
            ilSomeFound = 1;
          }
        }
        else if (que_info->data[info_count].id == mod_id)
        {
          printf ("\nDelete not valid for %s, my queue %d\n", que_info->data[info_count].name, mod_id);
        }
        info_count++;
        /*        line_count++; */
      }                         /* end while */
      printf ("\n");
      if (ilSomeFound == 0)
      {
        printf ("No appliable queues of CPUT found!\n");
      }
      free ((char *) que_info2);
      free ((char *) item);
      break;
    case 3:
      printf ("\nDelete all UTIL queues....\n");
      ilSomeFound = 0;
      while (info_count < que_info->entries)
      {
        strcpy (pclTmp, que_info->data[info_count].name);
        TrimRight (pclTmp);
        if (strstr (pclTmp, "UTIL") != NULL)
        {
          rc = delete_queue (que_info->data[info_count].id, mod_id);
          if (rc != RC_SUCCESS)
          {
            printf ("\nQueue_delete Error %d\n", rc);
          }
          else
          {
            printf ("\nQueue %d of %s deleted\n", que_info->data[info_count].id, que_info->data[info_count].name);
            ilSomeFound = 1;
          }
        }
        info_count++;
        /*        line_count++; */
      }                         /* end while */
      printf ("\n");
      if (ilSomeFound == 0)
      {
        printf ("No queues of UTIL found!\n");
      }
      free ((char *) que_info2);
      free ((char *) item);
      break;

    case 4:
      if ((ipDest >= 20000) && (ipDest < 30000))
      {
        printf ("\nDelete one CUTIL queue: %d\n", ipDest);

        while (info_count < que_info->entries)
        {
          strcpy (pclTmp, que_info->data[info_count].name);
          TrimRight (pclTmp);
          if (((strstr (pclTmp, "UTIL") != NULL) || (strstr (pclTmp, "CPUT") != NULL))
              && (que_info->data[info_count].id == ipDest))
          {
            rc = delete_queue (que_info->data[info_count].id, mod_id);
            if (rc != RC_SUCCESS)
            {
              printf ("\nQueue_delete Error %d\n", rc);
            }
            else
            {
              printf ("\n%s queue %d deleted\n", que_info->data[info_count].name, que_info->data[info_count].id);
            }
          }
          info_count++;
          /*        line_count++; */
        }                       /* end while */
      }
      else
      {
        printf ("\ndelete queue: id %d out of range (20000-30000)\n", ipDest);
      }
      printf ("\n");
      free ((char *) que_info2);
      free ((char *) item);
      break;

    case 5:
      /* print one line and core without deleting QUEUE (for debugging) */
      printf (QO_INFO (que_info->data[info_count]));
      printf ("\n");
      free ((char *) que_info2);
      free ((char *) item);
      info_count = info_count / (ipWhat - 5);
      break;
    default:
      break;
  }
  dbglog (TRACE, "DELETING QUEUE %d", mod_id);
  que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
  cput45_terminate (XC_OK);
}                               /* end of queue_overview */

/* ******************************************************************** */
/*									*/
/* The following routine deletes a queue.				*/
/*									*/
/* ******************************************************************** */

static int delete_queue (int que_ID, int mod_id)
{
  ITEM *item;
  int len;

  /* Prompt for queue ID */
  if ((que_ID <= 0) || (que_ID >= 30000))
  {
    return RC_FAIL;
  }                             /* end if */

  dbglog (TRACE, "DELETING QUEUE (QUE: %d) (MOD: %d)", que_ID, mod_id);
  if (que (QUE_DELETE, que_ID, mod_id, 5, 0, 0) != RC_SUCCESS)
  {
    return RC_FAIL;
  }                             /* end if */

  len = sizeof (ITEM);
  item = (ITEM *) malloc (len);
  if (que (QUE_GETNW, 0, mod_id, 5, len, (char *) item) != RC_SUCCESS)
  {
    free ((char *) item);
    return RC_SUCCESS;
  }                             /* end if */

  free ((char *) item);
  return RC_SUCCESS;

}                               /* end of delete_queue */

/* ******************************************************************** */
/* The TrimRight() routine*/
/* ******************************************************************** */
static void TrimRight (char *pcpBuffer)
{
  int i = 0;

  for (i = strlen (pcpBuffer); i > 0 && isspace ((int) pcpBuffer[i - 1]); i--)
    ;
  pcpBuffer[i] = '\0';
}

static void dbglog (int level, char *fmt, ...)
{
  va_list args;
  char *s;
  char Errbuf[1000];
  int i;

  if (run_debug_level > 0)
  {
    if (pfgDbgLog == NULL)
    {
      pfgDbgLog = fopen (pcgCedaLogFile, "a");
      outp= pfgDbgLog;
    }

    if (pfgDbgLog != NULL)
    {

      va_start (args, fmt);

      if ((level == TRACE) && run_debug_level > 0)
      {
        (void) sprintf (Errbuf, "%s ", pcgMyName);
        s = Errbuf + strlen (Errbuf);
        get_time (s);
        s = Errbuf + strlen (Errbuf);
        strcat (s, fmt);
        strcat (s, "\n");
        fseek (pfgDbgLog, 0, SEEK_END);
        vfprintf (pfgDbgLog, Errbuf, args);
        fflush (pfgDbgLog);
      }                         /* end if */

      if (level == DEBUG && run_debug_level > TRACE)
      {
        (void) sprintf (Errbuf, "%s ", pcgMyName);
        s = Errbuf + strlen (Errbuf);
        get_time (s);
        s = Errbuf + strlen (Errbuf);
        strcat (s, fmt);
        strcat (s, "\n");
        fseek (pfgDbgLog, 0, SEEK_END);
        vfprintf (pfgDbgLog, Errbuf, args);
        fflush (pfgDbgLog);
      }                         /* end if */

      va_end (args);

    }

  }                             /* end if */
}

/* ******************************************************************** */
/* ******************************************************************** */
static void get_time (char *s)
{
  struct timeb tp;
  time_t _CurTime;
  struct tm *CurTime;
  struct tm rlTm;
  long secVal, minVal, hourVal;
  char tmp[16];

  if (s != NULL)
  {
    _CurTime = time (0L);
    CurTime = (struct tm *) localtime (&_CurTime);
    rlTm = *CurTime;
#if defined(_WINNT)
    strftime (s, 20, "%" "H:%" "M:%" "S", &rlTm);
#else
    strftime (s, 20, "%" "Y%" "m%" "d%" "H%" "M%" "S", &rlTm);
    /*   strftime(s,20,"%" "b%" "d %" "T",&rlTm); */
#endif

    ftime (&tp);
    minVal = tp.time / 60;
    secVal = tp.time % 60;
    hourVal = minVal / 60;
    minVal = minVal % 60;
    sprintf (tmp, ":%3.3d>> ", tp.millitm);
    strcat (s, tmp);
  }

}

/* ******************************************************************** */
/* ******************************************************************** */
static void cput45_terminate (int ipExVal)
{
  dbglog (TRACE, "============ TERMINATE (RC %d) ===========", ipExVal);
  exit (ipExVal);
}

/* ******************************************************************** */
/* added 20040715 JIM:                                                  */
/* The set_sysqcp_dbg	uses special commands to set the debug_level of   */
/* the SYSQCP process                                                   */
/* ******************************************************************** */
static void set_sysqcp_dbg (int level)
{

  ITEM item1;
  ITEM *item;
  int tag;
  int len;
  int return_value;

  if ((level == TRACE_ON) || (level == TRACE_OFF))
  {
    /* 20070226 JIM: on AIX the pids are not continously increasing but random, so the
    * modulo may create same tag at 2 simultaniosly starting NETINs. Use complete PID
    * tag = ((getpid()%1000)+30000);
    */
    tag = getpid()+30000;
    dbglog (TRACE, "NOW SET_SYSQCP_DBG (CREATE QUEUE)");
    if (que (QUE_CREATE, 0, tag, 0, 5, "CPUT") != RC_SUCCESS)
    {
      dbglog (TRACE, "ERROR ON QUE_CREATE");
      exit (XC_ERROR);
    }
    alarm (2);
    if (que (QUE_RESP, 0, tag, 0, sizeof (ITEM), (char *) &item1) != RC_SUCCESS)
    {
      dbglog (TRACE, "ERROR ON QUE_RESP");
      exit (XC_ERROR);
    }                           /* end if */
    alarm (0);

    mod_id = item1.originator;
    dbglog (TRACE, "ATTACHED TO ANSWER QUEUE (%d)", mod_id);

    /* send DBG flag for SYSQCP */

    switch (level)
    {
      case TRACE_ON:
        dbglog (TRACE, "SENDING QUE_SYSDBG...");
        return_value = que (QUE_SYSDBG, 9, mod_id, 5, 0, 0);
        break;
      case TRACE_OFF:
        dbglog (TRACE, "SENDING QUE_SYSTRCOFF...");
        return_value = que (QUE_SYSTRCOFF, 9, mod_id, 5, 0, 0);
        break;
    }
    if (return_value != RC_SUCCESS)
    {
      dbglog (TRACE, "DELETING QUEUE %d", mod_id);
      que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
      exit (XC_ERROR);
    }                           /* end if */
    len = sizeof (ITEM) + sizeof (QUE_INFO) + (sizeof (QINFO) * 200);
    item = (ITEM *) malloc (len);
    if (item == (ITEM *) NUL)
    {
      dbglog (TRACE, "DELETING QUEUE %d", mod_id);
      que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
      exit (XC_ERROR);
    }                           /* end if */

    if (que (QUE_GET, 0, mod_id, 5, len, (char *) item) != RC_SUCCESS)
    {
      dbglog (TRACE, "DELETING QUEUE %d", mod_id);
      que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
      free ((char *) item);

      exit (XC_ERROR);
    }                           /* end if */

    if (item->msg_length == FATAL)
    {
      dbglog (TRACE, "DELETING QUEUE %d", mod_id);
      que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
      free ((char *) item);
      exit (XC_ERROR);
    }                           /* end if */
    dbglog (TRACE, "DELETING QUEUE %d", mod_id);
    que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
    printf ("%s\n", item->text);
    fflush (stdout);
  }
  exit (XC_ERROR);

}

extern void SendNextForQueue (int module, int flag);
/* ******************************************************************** */
/* added 20041124 JIM:                                                  */
/* The send_que_cmd	uses special commands to manipulate queue status of */
/* the normal processes                                                 */
/* ******************************************************************** */
static void send_que_cmd (char *cmd, int que_id)
{

  ITEM item1;
  /*ITEM *item;*/
  ITEM *item2 = NULL;
  int tag;
  int len;
  int return_value;
  int ilQueCmd = 0;
  
  if (strcmp(cmd,"go")==0)
  {
     ilQueCmd = QUE_GO;
  }
  else if (strcmp(cmd,"hold")==0)
  {
     ilQueCmd = QUE_HOLD;
  }
  else if (strcmp(cmd,"empty")==0)
  {
     ilQueCmd = QUE_EMPTY;
  }
  else if (strcmp(cmd,"next_flag")==0)
  {
     ilQueCmd = QUE_NEXTNW;
     set_sysqcp_nextnw (que_id);
     return;
  }
  else if (strcmp(cmd,"ack")==0)
  {
     ilQueCmd = QUE_ACK;
  }
  else if (strcmp(cmd,"que_get")==0)
  {
     ilQueCmd = QUE_GETBIGNW;
  }
  if (ilQueCmd != 0)
  {
    /* 20070226 JIM: on AIX the pids are not continously increasing but random, so the
    * modulo may create same tag at 2 simultaniosly starting NETINs. Use complete PID
    * tag = ((getpid()%1000)+30000);
    */
    tag = getpid()+30000;
    dbglog (TRACE, "CREATE QUEUE FOR <QUE_%s>",cmd);
    if (que (QUE_CREATE, 0, tag, 0, 5, "CPUT") != RC_SUCCESS)
    {
      dbglog (TRACE, "ERROR ON QUE_CREATE");
      exit (XC_ERROR);
    }
    alarm (2);
    if (que (QUE_RESP, 0, tag, 0, sizeof (ITEM), (char *) &item1) != RC_SUCCESS)
    {
      dbglog (TRACE, "ERROR ON QUE_RESP");
      exit (XC_ERROR);
    }                           /* end if */
    alarm (0);

    mod_id = item1.originator;
    dbglog (TRACE, "ATTACHED TO ANSWER QUEUE (%d)", mod_id);

    dbglog (TRACE, "NOW SEND QUEUE CMD <%s>",cmd);
    if ((ilQueCmd == QUE_ACK) || (ilQueCmd == QUE_GETBIGNW))
    {     /* get big item and/or send ack flag for module */
       return_value = que (ilQueCmd, 0, que_id, 5, 0, (char *) &item2);
       if (return_value == RC_SUCCESS)
       {
         dbglog (TRACE, "SEND QUEUE CMD <%s> succeeded",cmd);
         SendNextForQueue (que_id,QUE_NEXTNW);
       }
       else
       {
         dbglog (TRACE, "SEND QUEUE CMD <%s> failed",cmd);
         if (ilQueCmd == QUE_GETBIGNW)
         {
           dbglog (TRACE, "SEND ACK FOR CMD <%s> ANYWAY TO DESTROY ITEM",cmd);
           return_value = que (QUE_ACK, 0, que_id, 0, 0, NULL);
           dbglog (TRACE, "SEND ACK FOR CMD <%s> ANYWAY TO DESTROY ITEM done ...",cmd);
           SendNextForQueue (que_id,QUE_NEXT); 
           dbglog (TRACE, "SendNextForQueue mod-id <%d> done", que_id);
           if (item2 != NULL)
           {
             free ((char *) item2);
             item2 = NULL;
           }
         }
       }
    }
    else
    {    /* send go,hold,empty,next flag for module */
       return_value = que (ilQueCmd, que_id, mod_id, 5, 0, 0);
       dbglog (TRACE, "sending <%s> returned %d", cmd, return_value);
    }                           /* end if */

    dbglog (TRACE, "anything else?");
    if (return_value != RC_SUCCESS)
    {
      dbglog (TRACE, "DELETING QUEUE %d", mod_id);
      que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
      exit (XC_ERROR);
    }                           /* end if */

    if (item2 != NULL)
    {
      free ((char *) item2);
      item2 = NULL;
    }
    if (que (QUE_GETBIGNW, 0, mod_id, 5, 0, (char *) &item2) != RC_SUCCESS)
    {
      dbglog (TRACE, "DELETING QUEUE %d", mod_id);
      que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
      if (item2 != NULL)
      {
        free ((char *) item2);
        item2 = NULL;
      }
      exit (XC_ERROR);
    }                           /* end if */

    dbglog (TRACE, "after QUE_GETBIGNW for mod-id <%d>", mod_id);
    if (item2->msg_length == FATAL)
    {
      dbglog (TRACE, "DELETING QUEUE %d", mod_id);
      que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
      if (item2 != NULL)
      {
        free ((char *) item2);
        item2 = NULL;
      }
      exit (XC_ERROR);
    }                           /* end if */
    dbglog (TRACE, "DELETING QUEUE %d", mod_id);
    que (QUE_DELETE, mod_id, mod_id, 0, 0, 0);
		switch(item2->msg_length) 
    {
			case FOUND:
        printf ("Status for %d changed to %s",que_id,cmd);
				break;
			case NOT_FOUND:
        printf ("queue %d not found...",que_id);
				break;
			default:
        printf ("Status for %d NOT changed ",que_id);
				break;
		} /* end switch */
    fflush (stdout);
    if (item2 != NULL)
    {
      free ((char *) item2);
      item2 = NULL;
    }
  }
  exit (XC_ERROR);

}

static void set_sysqcp_nextnw (int module)
{
  SendNextForQueue (module,QUE_NEXTNW);
  exit (XC_OK);
}

static void Usage(int ipMode)
{
#ifndef WMQ
    printf ("  qput: Usage:\n");
#else
    printf ("  qput (build for WMQ environment): Usage:\n");
#endif
    printf ("       qput que=<queue|mod-name> cmd=<command> [options]\n");
    printf ("   or: qput <queue|mod-name> <command> [options]\n");
    printf ("\n");
    printf ("  qput special commands: \n");
    printf ("        7: toggle debug mode: OFF -> TRACE -> DEBUG -> TRACE -> DEBUG\n");
    printf ("        8: switch debug mode OFF\n");
    printf ("       -1: SHUTDOWN to <queue> or <mod-name>\n");
    if (ipMode > 1 )
    {
      printf ("       -2: RESET to <queue> or <mod-name>\n");
      printf ("       -3: STOP (replaces process by dummy and kills running process!)\n");
      printf ("       -9, -15: like 'kill -9 <process>' or 'kill -15 <process>'\n");
      printf ("              but where <process> is mod-name, not pid.\n");
      printf ("              Use of -1 is preferred before -2, -9 or -15! \n");
#ifndef WMQ
      printf ("      -99: display one queue \n");
      printf ("     -100: display queue overview \n");
      if (ipMode > 2)
      {
        printf ("     -101: Delete all CPUT queues with STAT==5 \n");
        printf ("     -102: delete all queues of CPUT \n");
        printf ("     -103: delete all queues of *UTIL* \n");
        printf ("     -104: delete one dedicated queue of *UTIL* or *CPUT* \n");
        printf ("     -106: set NEXTNW flag for i.e. rulfndp in SYSQCP \n");
        printf ("     hold: set queue on HOLD \n");
        printf ("       go: set queue on GO \n");
        printf ("     next: set queue NEXT flag \n");
        printf ("  que_get: remove next item from queue, incl. SHM if available\n");
        printf ("      ack: send ACK to queue, expert only! For big items SHM will remain!\n");
        printf ("    empty: clear queue \n");
      }
#endif
    }
    printf ("\n");
    printf ("  qput options are one or more of:\n");
    printf ("    -h: this help\n");
    if (ipMode > 1)
    {
      printf ("    loop=<number of loops>     default: 1\n");
      printf ("    delay=<number of seconds>  default: 0\n");
    }
    printf ("    prio=<number>              default: 1\n");
    printf ("    tab=<name> (full)          default: empty\n");
    printf ("    fld=<fields>               default: empty\n");
    printf ("    dat=<data>                 default: empty\n");
    printf ("    sel=<selection>            default: empty\n");
    printf ("    wait=<timeout>             default: 120 sec, \n");
    printf ("                                0: wait for answer\n");
    printf ("                               -1: ignore answer\n");
    if (ipMode > 2)
    {
      printf ("    wks=<work station>         default: 'EXCO'\n");
      printf ("    user=<user>                default: 'QPUT'\n");
      printf ("    tws=<TW_START>             default: '%s'\n",sgParameters.cTws);
      printf ("    twe=<TW_END>               default: '%s'\n",sgParameters.cTwe);
      printf ("    debug=<0|1|2>              default: 1 (TRACE)\n");
    }
    printf ("    Use fields and data as comma seperated string.\n");
    printf ("    Use selection as 'where-clause' as allowed by process.\n");
    printf ("    Spaces before and after '=' are not allowed.\n");
    printf ("    Values of option 'dat=' and 'sel=' must be enclosed in <\">.\n");
    if (ipMode > 2)
    {
      printf ("  qput example: \n");
      printf ("     qput cmd=GFR que=fligro1 fld=FKEY tab=AFTTAB sel=\"WHERE URNO = 399096532\"\n");
    }
    fflush (stdout);
    if (ipMode>0)
    {
       cput45_terminate (XC_USAGE);
    }
    else
    {
       exit(0);
    }
  }
/* ******************************************************************** */
/* Space for Appendings (Source-Code Control)				*/
/* ******************************************************************** */
/* ******************************************************************** */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
