#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/stb.c 1.2 2006/01/23 23:19:00SGT jim Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
static char sccs_stb[] ="@(#) UFIS 4.4 (c) ABB AAT/I stb.c 44.1 / 00/01/07 09:55:32 / VBL / MIKE";
/****************************************************************************/
#define		UGCCS_PRG
/*$:
	---------------------------------------------------------------

	S  Y  S  T  E  M   T  A  B  L  E   B  U  I  L  D  E  R

	---------------------------------------------------------------

	Version		: 0.0 		May 1989
	Programmer	: KJK
	Name		: STB

	This task converts the ASCII system configuration file to the
	system table file that is used by 'SYSTRT' for generation of
	the 'SYSTAB' shared memory.

	There are two types of system tables:

	-	record access tables	(fixed sized records)
	-	normal tables		(no record structure)

	This module is started from the terminal with the following
	arguments:

	stb [-s -t -v] input-file-name output-file-name

		where the arguments within brackets are optional.

		Optional arguments:
		-------------------
		-s		simulate, output file not written

		-t		test, input file = 'sgs.tab',
				output file = 'systab.db1' under
				current directory if not given as arguments 

		-v		verbose, input file listed on the
				terminal

		File name arguments:
		--------------------

		input-file-name		/dir/dir/../sgs.tab (ASCII-file) 

		output-file-name        /dir/dir/../systab.db1 (internal
                                                                format)

	Example:
	--------

	stb -v sgs.tab systab.db1

	- converts 'sgs.tab' to 'systab.db1' in the current directory and 
	  lists the contents of 'sgs.tab' on the terminal

	File formats:
	-------------

	'sgs.tab'	an ASCII file, see 'sgs.tab'

	'systab.db1'	internal format

			______________________________	
			| size of system table file  |  type long
			------------------------------
			| current max table no.      |  type int
			------------------------------
			| 1st system table header    |  type TABHDR
			.                            .
			.                            .
			|                            |
			------------------------------
			.                            .
			.                            .
			.                            .
			------------------------------
			| nth system table header    | type TABHDR
			. where n is current max.    .
			. table no.                  .
			|                            |
			------------------------------
			| system table no. 1         |
			.			     .
			|                            |
			------------------------------
			.                            .
			.                            .
			.			     .
			------------------------------
			| system table no. n         |
			. where n is current max.    .
			. table no.                  .
			|                            |
			------------------------------
 
	H O W  T H E  T A B L E  F O R M A T S  A R E  D E F I N E D
        -------------------------------------------------------------

	Every system table configured is defined field by field
	with a STDFLD-structure, which defines the type of the
	field, offset of the field within a table or a record of a table
	and size of the field.

	Every time a new table is added to the system it must be defined
	in 'STB'. The field types are defined in <stbdef.h>. 
	If a new field will be needed it must be defined in <stbdef.h>. 

	The structures that are needed to bind a system table to 'STB'
  	are (see <stbdef.h> for the format):	

	- STDFLD	defines the fields of the table

	- STDDESC	binds the STDFLD-structures to the 
			table number

	- LABEL		defines the function that processes the
			table (internal subroutine)

	I N T E R N A L  E R R O R  M E S S A G E S
	--------------------------------------------

	STB has a set of internal error messages. The error numbers
	and the structure are defined in <stbdef.h>. The messages are
	defined in STB.
	If new error messages will be added to STB the numbers must
	be defined in <stbdef.h> and the messages added to the EMSG-struc-
	ture.
                                                                          $:*/

/*2+*/
/* 20060123 JIM: added flag AIX and avoide comment in comment */

#include <stdio.h>
#include <ctype.h>
#include <fcntl.h>
#ifndef _WINNT
#ifndef _SNI
#ifndef _SOLARIS
#ifndef _HPUX_SOURCE
#ifndef _UNIXWARE
#ifndef _LINUX
#ifndef _AIX
#include <prototypes.h>
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#include <sys/types.h>
#include <sys/stat.h>

#include <glbdef.h>
#include <stbdef.h>
#include <sptdef.h>


extern  int main(int argc,char * *argv);
static  int sgsread(char *sgsin);
extern  int sgswrite(char *sgsout);
int sgsline(FILE *iop); 
static  int sgslist(int argc,char * *argv);
static  int getrecord(char *buff,int size,FILE *iop); 
static  int mygetc(FILE *iop); 
static  int callf(char *label,int argc,char * *argv);
extern  struct _tabhdr *gethdr(int *pargc,char * * *pargv);
static  char *getfield(char *record);
static  char *getitem(char *field);
static  int stdcopy(char *p,char *fld,struct _stdfld *pstd);
static  int f_dummy(int argc,char * *argv);
static  int f_mtabnum(int argc,char * *argv);
static  int f_fsmem(int argc,char * *argv);
static  int f_sptab(int argc,char * *argv,struct _tabhdr *pth);
static  int f_include(int argc,char * *argv);
static  int f_prtdev(int argc,char * *argv);
static  int f_stdtab(int argc,char * *argv,struct _tabhdr *pth);
static  int f_sttab(int argc,char * *argv,struct _tabhdr *pth);
extern  int getstat(char *s);

/*	E X T E R N A L  F U N C T I O N S  (STBSUBR)                 */

extern int isint();      /* checks if an integer field                */
extern int ishex();      /* checks if a hex field                     */
extern int sb_move();    /* copies a string & pads it with space      */
extern int getyesno();   /* processes yes/no field                    */
extern int asctohex();   /* converts a hex string to binary           */
extern int blkmove();    /* memory copy                               */
extern char *strsave();  /* save a string somewhere                   */
extern char *sysalloc(); /* allocates dynamic memory & zeroes it      */

/*	E R R O R  M E S S A G E S                                    */

static struct EMSG etxtab[] =  /* STB error messages            */ 
     {
       { ERROPNINF, "cannot open input file", },
       { ERROPOUTF, "cannot open output file", },
       { ERRSGSNF,  "missing or invalid SGS card", },
       { ERRSGSMEM, "cannot allocate SGS work area(s)", },
       { ERRDYNMEM, "cannot allocate dynamic memory", },
       { ERRNTSTR,  "non terminated string encountered", },
       { ERRMAXTAB, "table no exceeds maximum table no", },
       { ERRTABHDR, "error in table description (NO,RSZ,ESZ,etc)", },
       { ERRSPTSZ,  "SPTAB size not specified", },
       { ERRINCLUDE,"invalid INCLUDE parameter", },
       { ERRPRTDEV, "PRTDEV - format error", },
       { ERRFSMEM,  "missing or invalid FS-specification", },
       { ERRNODESC, "missing std_table descriptor", },
       { ERRSPTAB,  "SPTAB must be the 1st table", },
       { ERRNOINT,  "integer expected", },
       { ERRSTAT,   "UP/DN expected", },
       { ERRYESNO,  "Y/N expected", },
       { ERRPARMF,  "invalid parameter format", },
       { ERRNOHEX,  "hexadecimal field expected", },
       { -1,        "", },             /* terminates the structure      */ 
    };

/*	S T A T I C  V A R I A B L E S                       */

static int simulate = 0;       /* -s command argument sets this variable */
                               /* (no output file generated)             */
static int test     = 0;       /* -t command argument sets this variable */
                               /* input file = 'sgs.tab_test'            */
                               /* output file = 'systab_test'            */
static int verbose  = 0;       /* -v command argument sets this variable */
                               /* the contents of the input file will be */
                               /* listed on the terminal                 */
static STHIDX *systab;         /* system table index                     */ 

static char *statwork = NULL;  /* address of the temporary status table     */
static int statwsz   = 0;      /* size of the temporary status table        */
static int statno    = 0;      /* current offset within the temporary       */
                               /* status table                              */

#ifdef _HPUX_SOURCE
static char *baseAdr  = NULL;
#endif


static char *syswork  = NULL;   /* systab work buffer   */
static char *sysbase  = NULL;   /* 1st table header     */
static char *recbuff  = NULL;   /* record work buffer   */
static char *crec     = NULL;   /* current record save area */
static char **recv    = NULL;   /* vector of pointers to records */

static long systabsz = 0l;      /* total size of the system tables */
static int  recsz    = 0;       /* size of the internal record     */
                                /* buffer                          */
static int  recvsz   = 0;       /* size of the record vector buffer */
static int  maxtab   = 0;       /* max. # of tables                */

static SPTREC *pspt = NULL;     /* system parameter table          */

int debug_level=0;


/*      I N T E R N A L   S U B R O U T I N E S  U S E D  F O R            */
/*	T H E  T A B L E  C O N V E R S I O N                              */
/*	(must be defined before LABEL-structure !!!)                       */

#ifndef _HPUX_SOURCE

int f_dummy();                /* 1st label is "DUMMY"                      */
int f_mtabnum();              /* "MTABNUM"                                 */
int f_sptab();                /* "SPTAB"                                   */
int f_fsmem();                /* "FSMEM"                                   */
int f_include();              /* "INCLUDE"                                 */
int f_prtdev();               /* "PRTDEV"                                  */
int f_stdtab();               /* standard system table                     */
int f_sttab();                /* "STTAB"                                   */

#endif

/*	I N T E R N A L  S U B R O U T I N E S  R E T U R N I N G          */
/*	A  C H A R A C T E R  P O I N T E R                                */

/*char *getfield();             * returns the address of a field            */
/*char *getitem();              * returns the address of an item within a   */
                              /* field                                     */

/*	D E S C R I P T O R S  O F  T H E  T A B L E  C O N V E R S I O N  */
/*	S U B R O U T I N E S                                              */

static struct LABEL labels[] =          /* label handling functions        */
  {
     { "DUMMY",   f_dummy,   },
     { "MTABNUM", f_mtabnum, },
     { "SPTAB",   f_sptab,   },
     { "FSMEM",   f_fsmem,   },
     { "INCLUDE", f_include, },
     { "PRTDEV",  f_prtdev,  },
     { "TETAB",   f_stdtab,  },
     { "ROTAB",   f_stdtab,  },
     { "PNTAB",   f_stdtab,  },
     { "ACTAB",   f_stdtab,  },
     { "APTAB",   f_stdtab,  },
     { "CKTAB",   f_stdtab,  },
     { "BCTAB",   f_stdtab,  },
     { "BDTAB",   f_stdtab,  },
     { "ALTAB",   f_stdtab,  },
     { "DETAB",   f_stdtab,  },
     { "ARTAB",   f_stdtab,  },
     { "GATAB",   f_stdtab,  },
     { "NATAB",   f_stdtab,  },
     { "PSTAB",   f_stdtab,  },
     { "HGTAB",   f_stdtab,  },
     { "SETAB",   f_stdtab,  },
     { "ASTAB",   f_stdtab,  },
     { "TTTAB",   f_stdtab,  },
     { "ATTAB",   f_stdtab,  },
     { "IXTAB",   f_stdtab,  },
     { "CTTAB",   f_stdtab,  },
     { "C1TAB",   f_stdtab,  },
     { "C2TAB",   f_stdtab,  },
     { "NMTAB",   f_stdtab,  },
     { "MTTAB",   f_stdtab,  },
     { "EXTAB",   f_stdtab,  },
     { "HSTAB",   f_stdtab,  },
     { "C3TAB",   f_stdtab,  },
     { "C4TAB",   f_stdtab,  },
     { NULL,      NULL,      },         /* descriptor terminator           */ 
  };

/*1:
                                                                          1:*/

/****************************************************************************/
/*                                                                          */
/*     m  a  i  n                                                           */
/*                                                                          */
/*     argument processing and setting of the defaults                      */
/*                                                                          */
/****************************************************************************/

main(int argc,char **argv)

{
        register char *s;           /* internal                       */
        int ret;                    /* return code from sub-funcs     */
        char *sgsin = NULL;         /* pointer to the input file name */
        char *sgsout = NULL;        /* pointer to the output file name */
        struct EMSG *pmsg;          /* error structure                */

        while (--argc > 0 && **++argv == '-')  /* process the '-' arguments */
              {
              s = *argv + 1;                   /* skip the '-'              */
              if (*s)                          /* argument given            */
                 {
                 switch (toupper(*s))          /* to upper case             */
                     {
                     case 'S':                 /* simulate only             */
                     simulate = 1;
                     break;

                     case 'T':                 /* test                      */
                     test = 1;
                     sgsin = "sgs.tab";    /* define the test files     */
                     sgsout = "systab.db1";
                     break;

                     case 'V':                 /* verbose                   */
                     verbose = 1;
                     break;

                     default:                  /* unknown '-' argument      */
                     printf("\nillegal option %c ignored\n",*s);
                     break;
                     }
                  }
              }

        if (argc-- > 0)                        /* file names defined        */
            sgsin = *argv++;                   /* input file                */
        if (argc-- > 0)
            sgsout = *argv;                    /* output file               */

        if ((ret = sgsread(sgsin)) !=0)        /* process input file        */
           {
           for (pmsg = etxtab; pmsg->emsgno != -1; pmsg++) /* error         */
                if (ret == pmsg->emsgno)        /* error defined            */
                    break;
           if (crec == NULL)                    /* no current record        */
               crec = "";
           printf("%.78s\n error %d: %s\n",crec,ret,pmsg->emsgtxt);
           }
         else if ((ret = sgswrite(sgsout)) !=0) /* process output file      */
           printf("\n error writing the %s\n",sgsout);        /* error      */

         if (systab != NULL)                    /* free memory, if          */
             free((char *) systab);             /* allocated                */
}                                               /* end of main              */

/*1:
                                                                          1:*/

/*2+*/
/****************************************************************************/
/*                                                                          */
/*      s g s r e a d                                                       */
/*                                                                          */
/****************************************************************************/

static sgsread(char *sgsin)

/* sgsin : pointer to input file name */

{
        register FILE *fp;
        /* FILE *fopen(); vbl */
        register char **precv;
        static char oldlabel[GROUPSZ+1] = "DUMMY";
        register int i, ret;

        if (verbose)
            printf("\nreading %s\n",sgsin);

        if ((fp = fopen(sgsin,"r")) == NULL)
             return(ERROPNINF);

        if ((ret = sgsline(fp)) !=0)     /* erroneous SGS-line              */
           {
           fclose(fp);
           return(ret);
           }

        precv = recv;                    /* init vector pointer             */

        while ((ret = getrecord(recbuff,recsz,fp)) != EOF) /* all records   */
        {      
               if (ret)                     /* error */
               {
                  strcpy(crec,recbuff);
                  return(ret);
               }

               if (strncmp(oldlabel,recbuff,GROUPSZ)) /* new label name     */
                  {
                  *precv = NULL;
                  if ((ret = callf(oldlabel,precv-recv,recv)) !=0)
                      return(ret);           /* error                       */
                  precv = recv;
                  strncpy(oldlabel,recbuff,GROUPSZ);
                  }

               if ((*precv++ = strsave(recbuff)) == NULL) /* build rec vec */
                    return(ERRDYNMEM);
               }

        *precv = NULL;                   /* last label                      */

        if ((ret = callf(oldlabel,precv-recv,recv)) !=0){ /* error           */
                return(ret);
	}
        if (verbose)
            printf("\nread of %s done\n",sgsin);

        fclose(fp);

        return(0);
}

/*1:
                                                                          1:*/

/*2+*/

/****************************************************************************/
/*                                                                          */
/*      s g s w r i t e                                                     */
/*                                                                          */
/*      save systab memory contents to output-file                          */
/*                                                                          */
/****************************************************************************/

sgswrite(char *sgsout)

/* sgsout : pointer to output-file name       */

{
        register char *p = (char *)systab; /* working pointer               */
        register unsigned int n;
        unsigned size = (unsigned) (syswork - (char *)systab);
        int fd;

        if (simulate)                   /* no output file                   */
            return(0);

        if (verbose)
            printf("\nwriting the %s\n",sgsout);

        if (pspt != NULL)
	    pspt->size = size;  /* size of systab */

        if ((fd = open(sgsout,(O_RDWR|O_CREAT))) < 0)
            {
            printf("\nerror opening %s\n",sgsout);
            return(ERROPOUTF);
            }
		chmod(sgsout,0666);
        write(fd,(char *)&size,sizeof(unsigned int)); /* size of systab */

        while (size)
              {
              n = (size > 512) ? 512 : size;
              write(fd,p,n);
              size -= n;
              p += n;
              p = (char *)p;
              }

        close(fd);

        if (verbose)
            printf("\n%s written\n",sgsout);

        return(0);
}

/*1:
                                                                         1:*/

/*2+*/
/****************************************************************************/
/*                                                                          */
/*      s g s l i n e                                                       */
/*                                                                          */
/*      read SGS parameter card and allocate memory                         */
/*                                                                          */
/****************************************************************************/

sgsline(register FILE *iop)

{
        char sgs[4];
        int recv_size;

        if (fscanf(iop,"%3s %ld %d %d %d\n",sgs,&systabsz,&recsz,
            &recvsz,&statwsz) != 5 || strcmp(sgs,"SGS"))
              return(ERRSGSNF);

        recv_size = recvsz * sizeof(char *);

        if ((syswork = sysalloc(systabsz)) == NULL ||
            (recbuff = sysalloc((unsigned) recsz)) == NULL ||
            (crec    = sysalloc((unsigned) recsz)) == NULL ||
            (recv    = (char **) sysalloc((unsigned) recv_size)) == NULL ||
            (statwork = sysalloc((unsigned) statwsz)) == NULL)
               return(ERRSGSMEM);

#ifdef _HPUX_SOURCE
	baseAdr=syswork;
#endif

        systab = (STHIDX *) syswork;
        sysbase = syswork = (char *) systab->sthtab;
        return(0);
}

/*1:
                                                                       1:*/

/*2+*/
static sgslist(register int argc,register char **argv)
	/* list all records of a label          */

/* argc : no. of records                       */
/* argv : record pointer vector                */

{
        while (argc-- > 0)
               printf("%.78s\n",*argv++);
}

/*1:
                                                                          1:*/
/*2+*/

/****************************************************************************/
/*                                                                          */
/*      g e t r e c o r d                                                   */
/*                                                                          */
/*      read a SGS record into recbuff (data only)                          */
/*                                                                          */
/****************************************************************************/

static getrecord(char *buff,register int size,register FILE *iop)

/* buff : record buffer                   */
/* size : record size                     */

{
        register c;
        register char *p = buff;
        int instring = 0;

        while ((c = mygetc(iop)) != EOF && isspace(c)) /* skip leading      */
                ;                                      /* white spaces      */

        for ( ; c != '\n' && c != EOF; c = mygetc(iop))
            {
            if (--size > 0)               /* copy into record buffer        */
                *p++ = c;
            if (c == QUOTE)               /* string termination             */
                instring ^= 127;
            }

        *p = '\0';

        if (instring)
            return(ERRNTSTR);

        return((strncmp(buff,"!EOF",4)) ? 0 : EOF);
}

/*1:
                                                                          1:*/

/*2+*/

/****************************************************************************/
/*                                                                          */
/*      m y g e t c                                                         */
/*                                                                          */
/*      provide the next logical character from input stream                */
/*      eliminate remarks and line continuation characters                  */
/*                                                                          */
/****************************************************************************/

static mygetc(register FILE *iop)


{
        static instring = 0;
        register c, c2;

        switch (c = getc(iop))
               {
               case QUOTE:
               instring ^= 1;
               break;

               case '\f':
               c = '\n';
               break;

               case '\t':
               if (!instring)
                   c = ' ';
               break;

               case ';':
               if (!instring)
                  {
                  while ((c = getc(iop)) != '\n' && c != EOF)
                          ;
                  if (c != EOF)
                      c = mygetc(iop);
                  }
               break;

               case '/':
               if (!instring)
                   if ((c2 = getc(iop)) != '*')
                        ungetc(c2,iop);
                   else
                        while ((c = getc(iop)) != '\n' && c != EOF)
                                ;
               break;
               }

        return(c);
}

/*1:
                                                                          1:*/

/*2+*/

/****************************************************************************/
/*                                                                          */
/*      c a l l f  (calls label handling function)                          */
/*                                                                          */
/****************************************************************************/

static callf(register char *label,int argc,char **argv)

{
        register struct LABEL *p;
        TABHDR *pth;
		/* TABHDR *gethdr(); */
        int ret, n;

        for (p = labels; p->lblname != NULL; p++)
            if (!strncmp(label,p->lblname,GROUPSZ))
                if (!strncmp(label+GROUPOFFS,"TAB",3))
                    if ((pth = gethdr(&argc,&argv)) != NULL)
                        {                /* nnTAB labels                    */
                        if (verbose)
                            sgslist(argc,argv);
                        ret = (*p->lblfunc)(argc,argv,pth);

                        if (pspt == NULL) /* SPTAB not 1st table            */
                            return(ERRSPTAB);

                        pspt->tabsiz[pth->t_tabno] = pth->t_tabsz;

                        if ((pspt->recsiz[pth->t_tabno] = pth->t_recsz) !=0)
                           {
                           pspt->mrecno[pth->t_tabno] = pth->t_tabsz/pth->t_recsz;

                           syswork += (pth->t_recsz + sizeof(RECHDR)) *
                                       pspt->mrecno[pth->t_tabno];
                           }
                        else if (pth->t_dummy2)
                           syswork += pth->t_dummy2 * argc;

                        pth->t_dummy2 = 0;

                        syswork = (char *) syswork;
                        }
                    else
                        ret = ERRTABHDR;
                else
                  {                      /* all other labels                */
                  if (verbose)
                      sgslist(argc,argv);

                  ret = (*p->lblfunc)(argc,argv);
                  }

        while (argc-- > 0)
               free(*argv++);

        return(ret);
}

/*1:
                                                                         1:*/

/*2+*/

/****************************************************************************/
/*                                                                          */
/*      g  e  t  h  d  r   (..TABNR, ..TABRSZ, etc)                         */
/*                                                                          */
/*      create and locate a standard system table header                    */
/*                                                                          */
/****************************************************************************/

TABHDR *gethdr(int *pargc,char ***pargv)

{
        static char *valid[] = { "NR","SZ","RSZ","ESZ","ICC",NULL };
        register char **p, **argv = *pargv;
        /*  vbl char *getfield(); */
        TABHDR *pth;
        int i, n = 0;

        do
          {
          for (p = valid; *p != NULL; p++)
               if (!strncmp(*argv + (GROUPOFFS + 3),*p,strlen(*p)))
                  {
                  ++n;

                  if ((i = atoi(getfield(*argv))) > 0)
                      switch (p - valid)
                             {
                             case 0:
                             if (i <= systab->sthmax)
                                {
                                pth = systab->sthtab + i - 1;
                                pth->t_tabno = i;
                                pth->t_offset = syswork - sysbase;
                                }
                             break;

                             case 1:
                             if (pth != NULL)
                                 pth->t_tabsz = i;
                             break;

                             case 2:
                             if (pth != NULL)
                                 pth->t_recsz = i;
                             break;

                             case 3:
                             if (pth != NULL)
                                 pth->t_dummy2 = i;
                             break;

                             case 4:
                             if (pth != NULL)
                                 pth->t_tabno |= 0x4000;
                             break;
                             }
                  free(*argv);
                  break;
                  }

          ++argv;
          }
        while (*argv != NULL);

        *pargc -= n;
        *pargv += n;

        return((n) ? pth : (TABHDR *) NULL);
}

/*1:
                                                                          1:*/

/*2+*/
/****************************************************************************/
/*                                                                          */
/*      g e t f i e l d  (within a record)                                  */
/*                                                                          */
/****************************************************************************/

static char *getfield(char *record)

{
        static char *next = NULL;
        register char *p;
        char *q;
        register c, instring = 0;

        if ((p = record) != NULL)        /* 1st call within a record        */
            {
            strcpy(crec,p);              /* save the record                 */
            while (*p++ != BLANK)        /* skip the label                  */
                   ;
            q = p;
            }
        else
            p = q = next;

        if (p != NULL)
           {
           while (*p == BLANK)
                  ++p, ++q;              /* skip leading blanks             */

           while ((c = *p++) !=0)        /* end of record ?                 */
                 {
                 if (c == QUOTE)
                     instring ^= 1;
                 else if (c == BLANK && !instring) /* end of field ?        */
                     {
                     *(p - 1) = '\0';
                     break;
                     }
                 }
           next = (c) ? p : (char *) NULL; /* value of next call            */
           }
	if (q != NULL && *q)
	{
	  return q;
	}
	else
	{
	  return NULL;
	}
/* OLD:         return((*q) ? q : (char *) NULL);*/
}

/*1:
                                                                          1:*/

/*2+*/
/****************************************************************************/
/*                                                                          */
/*      g e t i t e m  (within field)                                       */
/*                                                                          */
/****************************************************************************/

static char *getitem(char *field)

{
        static char *next = NULL;
        register char *p;
        register char *q;

        if ((p = q = (field == NULL) ? next : field) != NULL)
            {
            if (*p == QUOTE)             /* not retained                    */
               {
               ++p, ++q;
               while (*p++ != QUOTE)
                      ;
               *(p - 1) = '\0';
               }
            while (*p && *p != COMMA)    /* subfield delimiter              */
                   ++p;
            next = (*p) ? p + 1 : (char *) NULL; /* value of next call      */
            *p = '\0';
            }
        return(q);
}

/*1:
                                                                          1:*/

/*2+*/

/*****************************  f  _  s  t  d  c  o  p  y  ******************/

static stdcopy(char *p,char *fld,register STDFLD *pstd)


{
        register char *s;
        register i;
        long l;
		/* long atol(); vbl */
        char hex_out[10];
        char *h_fld;

        for ( ; fld != NULL ; fld = getfield(NULL))
             for (s = getitem(fld); s != NULL; s = getitem(NULL))
                 {
                 switch (pstd->std_type)
                   {
                   case FLDI:            /* 1 or 2 byte integer             */

                   if (!isint(s))
                       return(ERRNOINT);
                   i = atoi(s);
                   if (pstd->std_size == 1) /* 1 byte integer               */
                       *(p + pstd->std_offs) = i;
                   else
#ifdef _HPUX_SOURCE
		   {
		     short tmpshort;
		     tmpshort=(short) i;
		     *((short *) (p + pstd->std_offs)) = tmpshort;
		   }
#else
                       *((short *)(p + pstd->std_offs)) = i;
#endif
                   break;

                   case FLDL:            /* long integer                    */

                   if (!isint(s))
                       return(ERRNOINT);
                   l = atol(s);
                   *((long *)(p + pstd->std_offs)) = l;
                   break;

                   case FLDX:            /* 1 or 2 byte hex-field           */

                   h_fld = hex_out;
                   if (!asctohex(h_fld,s,strlen(s)))
                       return(ERRNOHEX);
                   if (pstd->std_size == 1)
                       *(p + pstd->std_offs) = *h_fld;
                   else
                       {
                       *(p + pstd->std_offs) = *(h_fld + 1);
                       *(p + pstd->std_offs + 1) = *h_fld;
                       }
                   break;

                   case FLDS:            /* ascii char string               */

                   strncpy(p + pstd->std_offs,s,pstd->std_size);
                   break;

                   case FLDSB:           /* ascii char string space padded  */

                   sb_move(p + pstd->std_offs,s,pstd->std_size);
                   break;

                   case FLDYN:           /* Y/N field                       */

                   if ((i = getyesno(s)) < 0)
                        return(ERRYESNO);
                   *(p + pstd->std_offs) = i;
                   break;

                   case FLDUD:           /* UP/DN field                     */

                   if ((i = getstat(s)) < 0)
                        return(ERRSTAT);
                   *((short *)(p + pstd->std_offs)) = i;
                   break;
                   }

                 if (pstd->std_type)
                     ++pstd;            /* next field                       */
                 }
        return(0);
}

/*1:
                                                                          1:*/

/*2+*/

/******************************    f  _  d  u  m  m  y  *********************/

static f_dummy(int argc,char **argv)


{
        return(0);
}

/*1:
                                                                          1:*/

/*2+*/

/******************************  f  _  m  t  a  b  n  u  m  *****************/

static f_mtabnum(int argc,char **argv)   /* set max table no                */

{
        register char *fld;
        int i;

        if ((fld = getfield(*argv)) != NULL && (i = atoi(fld)) > 0)
            {
            systab->sthmax = i;           /* max no of tables               */
            syswork +=  i * sizeof(TABHDR); /* size of index area           */
            }

        return((systab->sthmax) ? 0 : ERRMAXTAB);
}

/*1:
                                                                          1:*/

/*2+*/
/******************************  f  _  f  s  m  e  m  ***********************/

static f_fsmem(int argc,char **argv)


{
        register char *fld, *s;        /* working pointers                  */
        register i, pos = 0;           /* work variables                    */

        if (pspt != NULL && argc && (fld = getfield(*argv)) != NULL)
            for (s = getitem(fld); s != NULL; s = getitem(NULL))
                {
                if (!isint(s) || (i = atoi(s)) < 0)
                    break;

                switch (pos++)
                       {
                       case 0:
                       pspt->sptfsh.rt1sz = i;
                       break;

                       case 1:
                       pspt->sptfsh.rt2sz = i;
                       break;

                       case 2:
                       pspt->sptfsh.rt1p = i;
                       break;

                       case 3:
                       pspt->sptfsh.memsz = i;
                       break;
                       }
                }
        return((pos == 4) ? 0 : ERRFSMEM);
}

/*1:
                                                                          1:*/

/*2+*/
/******************************  f  _  s  p  t  a  b  ***********************/

static f_sptab(int argc,char **argv,TABHDR *pth)


{
        if (!pth->t_tabsz)                /* SPTAB size not defined         */
            return(ERRSPTSZ);

        pspt = (SPTREC *) syswork; /* init the pointer                */
        pspt->maxtab = systab->sthmax;
        syswork += pth->t_tabsz;
        return(0);
}

/*1:
                                                                          1:*/

/*2+*/

/******************************  f  _  i  n  c  l  u  d  e  *****************/

static f_include(int argc,char **argv)    /* process INCLUDEs              */


{
        struct CONFB
          {
          char *confname;
          long confbits;
          };

        static struct CONFB comp[] =
          {
            { "ICC",   CFICC,    },
            { "LOG1",  CFLOGA,   },
            { "ILOG",  CFILOG,   },
            { NULL,    0l,       },
          };

        register struct CONFB *p;
        register char *fld;

        if (pspt != NULL)
            while (argc-- > 0 && (fld = getfield(*argv++)) != NULL)
                  {
                  for (p = comp; p->confname != NULL; p++)
                       if (!strcmp(p->confname,fld))
                          {
                          pspt->config |= p->confbits;
                          break;
                          }

                  if (p->confname == NULL)
                      return(ERRINCLUDE);
                  }

        return(0);
}

/*1:
                                                                          1:*/

/*2+*/
/*******************************  f  _  p  r  t  d  e  v  *******************/

static f_prtdev(int argc,char **argv)

{
        register char *fld, *s;
        register pos = 0;
        /* long atol(); vbl */
		long i;

        if (pspt != NULL && argc && (fld = getfield(*argv)) != NULL)
            for (s = getitem(fld); s != NULL; s = getitem(NULL))
                 switch (pos++)
                        {
                        case 0:

                        strncpy(pspt->pmsgdev,s,sizeof(pspt->pmsgdev));
                        break;

                        case 1:

                        strncpy(pspt->smsgdev,s,sizeof(pspt->smsgdev));
                        break;

                        case 2:
 
                        if (!isint(s) || (i = atol(s)) < 0)
                            --pos;
			else
			    pspt->devindic = i;
		        break;
                        }

        return((pos == 3) ? 0 : ERRPRTDEV);
}

/*1:
                                                                          1:*/

/*2+*/
/******************************  f  _  s  t  d  t  a  b  ********************/

static f_stdtab(int argc,char **argv,TABHDR *pth)  
	/* process standard tables           */

{
        char *fld, *rec;
        register STDDESC *pdsc;
        int ret, recsz;

        rec = syswork;

	recsz = pth->t_recsz;
        if (!recsz)
              recsz = pth->t_dummy2;

        pth->t_tabsz = recsz * argc;

        if (pth->t_recsz)
           {
           recsz += sizeof(RECHDR);
           rec +=   sizeof(RECHDR);
           }

        for (pdsc = d_tabs; pdsc->d_tabno; pdsc++) /* find the descriptor   */
             if (pdsc->d_tabno == pth->t_tabno)
                 break;

        if (pdsc->d_tabno)                /* found                          */
           while (argc-- > 0 && (fld = getfield(*argv++)) != NULL)
                 {
                 if ((ret = stdcopy(rec,fld,pdsc->d_std)) !=0 )
                     return(ret);        /* error                           */
                 rec += recsz;
                 }
        else
           return(ERRNODESC);

        return(0);
}

/*1:
                                                                          1:*/

/*2+*/

/******************************  f  _  s  t  t  a  b  ***********************/

static f_sttab(int argc,char **argv,TABHDR *pth)


{
        blkmove(statwork,syswork,statno); /* move to final location         */

        if (statno & 1)                   /* odd # of bytes                 */
            statno++;                     /* make it even                   */

        pth->t_tabsz = statno;

        syswork += statno;

        return(0);
}

/*1:
	G  E  T  S  T  A  T  -  subfunction

	External routines:

	- strcmp
                                                                          1:*/

/*2+*/


getstat(register char *s)            /* status byte value                */

/* s : pointer to status parameter           */ 

{
        if (statno >= statwsz)    /* temp. status table exhausted          */ 
            return(-1);           /* yes, return error                     */ 

        if (strcmp(s,"UP"))       /* parameter not 'UP'                    */ 
            if (!strcmp(s,"DN"))  /* parameter is 'DN'                     */ 
                statwork[statno] = 1; /* set status byte                   */ 
            else
                return(-1);       /* invalid parameter value               */ 
        return(statno++);         /* OK, return status offset              */ 
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
