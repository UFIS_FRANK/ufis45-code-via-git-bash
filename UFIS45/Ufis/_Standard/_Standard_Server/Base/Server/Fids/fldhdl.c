#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h"  /* sets UFIS_VERSION, must be done defore mks_version */
  static char mks_versmks[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Fids/fldhdl.c 5.1.1.38 2010/01/19 13:50:13GMT akl Exp otr (2012/03/06) $";
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Standard/Fids/fldhdl.c SVN: 5.1.1.38 2012/03/06 13:50:13GMT ... $";
#endif /* _DEF_mks_version */

/******************************************************************************/
/*                                                                           */
/* ABB ACE/FC Program fldhdl.c                                                */
/*                                                                            */
/* Author         : Hans-Juergen Ebert                                        */
/* Date           : 05-02-2001                                                */
/* Description    : Flight Display Handler, creates data for FIDS             */
/*                                                                            */
/* Update history :                                                           */
/* 20020709: JIM: JHI hat gesagt, dass das dynamische an TriggerAction alles  */
/*                Pfusch ist, daher returned der sofort wieder                */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_version[] ="%Z% UFIS44 (c) ABB AAT/I %M% %I% / %E% %U% / VBL";
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#define XS_BUFF 128
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h"
#include "fldhdl.h"
#include "AATArray.h"
#include "syslib.h"
#include "libccstr.h"
#include "fditools.h"
#include "sys/resource.h"


#define NUMBER_COUNTERS 300
#define FDD_FIELD_MAX 32
#define ARR_NAME_LEN   (18)
#define ARR_FLDLST_LEN (256)
#define FIELDVAL(rrArray,pcpBuf,ipFieldNo) (&pcpBuf[rrArray.plrArrayFieldOfs[ipFieldNo]])

#define AFT_FIELDS_TTYP "ADID,URNO,FLNO,FLTI,FLNS,STOA,STOD,DES3,ORG3,VIA3,ETOA,ETOD,ONBL,OFBL,TTYP,REMP,BLT1,BLT2,GTD1,CKIT,GTD2,JFNO,ETAI,ETDI,TMB1,TIFA,TIFD,TISA,TISD,CKIF,WRO1,GD1X,GD1Y,GD2X,GD2Y,B1BA,B1EA,B2BA,B2EA,JCNT,VIAN,VIAL,LAND,BOAO,BOAC,ALC2,ALC3,AIRB,B1BS,B1ES,B2BS,B2ES,GD1B,GD1E,GD2B,GD2E,W1BA,W1EA,W1BS,W1ES,STEV,FTYP,NXTI,TGD1,TGD2,TET1,BAS1,BAE1,BAS2,BAE2"
#define AFT_FIELDS_STYP "ADID,URNO,FLNO,FLTI,FLNS,STOA,STOD,DES3,ORG3,VIA3,ETOA,ETOD,ONBL,OFBL,STYP,REMP,BLT1,BLT2,GTD1,CKIT,GTD2,JFNO,ETAI,ETDI,TMB1,TIFA,TIFD,TISA,TISD,CKIF,WRO1,GD1X,GD1Y,GD2X,GD2Y,B1BA,B1EA,B2BA,B2EA,JCNT,VIAN,VIAL,LAND,BOAO,BOAC,ALC2,ALC3,AIRB,B1BS,B1ES,B2BS,B2ES,GD1B,GD1E,GD2B,GD2E,W1BA,W1EA,W1BS,W1ES,STEV,FTYP,NXTI,TGD1,TGD2,TET1,BAS1,BAE1,BAS2,BAE2"
static char AFT_FIELDS[1024] = AFT_FIELDS_TTYP;

#define FLD_FIELDS_STANDARD "AURN,URNO,ABTI,ACTI,AFTI,AOTI,CDAT,CREC,CTYP,DCTI,DOTI,DSEQ,HOPO,LSTU,RNAM,RTAB,RURN,RTYP,SCTI,SOTI,STAT,USEC,USEU"
#define FLD_FIELDS_ADD_ON "TIFF,OBLF,STOF"
static char FLD_FIELDS[256] = "\0";
static char FLD_FIELDS_TAB[256] = "\0";

#define FDD_FIELDS_STD "ADID,AIRB,APC3,AURN,BLT1,BLT2,CDAT,CKIF,CKIT,DACO,DNAT,ETOF,FLNO,FTYP,GTD1,GTD2,HOPO,INDO,LAND,LSTU,OFBL,ONBL,REMP,STOF,TIFF,URNO,USEC,USEU,VIA3,WRO1,DSEQ,RMTI,NODE,OBL1,OBL2,OGT1,OGT2"
#define FDD_FIELDS_BOTI "ADID,AIRB,APC3,AURN,BLT1,BLT2,CDAT,CKIF,CKIT,DACO,DNAT,ETOF,FLNO,FTYP,GTD1,GTD2,HOPO,INDO,LAND,LSTU,OFBL,ONBL,REMP,STOF,TIFF,URNO,USEC,USEU,VIA3,WRO1,DSEQ,RMTI,GD1B,GD2B,NODE,OBL1,OBL2,OGT1,OGT2"
#define FDD_FIELDS_TAB_STD "ADID,AIRB,APC3,AURN,BLT1,BLT2,CDAT,CKIF,CKIT,DACO,DNAT,ETOF,FLNO,FTYP,GTD1,GTD2,HOPO,INDO,LAND,LSTU,OFBL,ONBL,REMP,STOF,TIFF,URNO,USEC,USEU,VIA3,WRO1,DSEQ,RMTI"
#define FDD_FIELDS_TAB_BOTI "ADID,AIRB,APC3,AURN,BLT1,BLT2,CDAT,CKIF,CKIT,DACO,DNAT,ETOF,FLNO,FTYP,GTD1,GTD2,HOPO,INDO,LAND,LSTU,OFBL,ONBL,REMP,STOF,TIFF,URNO,USEC,USEU,VIA3,WRO1,DSEQ,RMTI,GD1B,GD2B"
static char FDD_FIELDS[1024] = FDD_FIELDS_STD;
static char FDD_FIELDS_TAB[1024] = FDD_FIELDS_TAB_STD;

#define CCA_FIELDS_STANDARD "FLNU,URNO,CKIC,CKBS,CKES,CKBA,CKEA,CTYP,DISP,LSTU,CDAT,REMA"
#define CCA_FIELDS_ADD_ON "FLAG"
static char CCA_FIELDS[256] = "\0";
static char CCA_FIELDS_TAB[256] = "\0";

#define ALT_FIELDS "ALC2,URNO,ALC3"

#define CHA_FIELDS "LNAM,URNO,RURN,TIFB,TIFE,DES3,CHGF,FCLA,LAFX"

#define FDD_LEN_S  34
#define FLD_LEN_S  34
#define CHA_LEN_S  34
/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = TRACE;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;          /* The queue item pointer  */
static EVENT *prgEvent     = NULL;          /* The event pointer       */
static int   igItemLen     = 0;             /* length of incoming item */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];

static int       igTcpTimeOutAlarmFlag = FALSE;
static int       igConnectDeviceFlag  = FALSE;
static int       igAlarmCount = 0;
static int igIntAndDomCounters = 0;
static int igAsciiCounterCompare = 1;
static char pcgInvalidCounterFdd[1024] = "\0";
static int igFirstInt = 0;
static int igLastInt = 0;
static int igFirstDom = 0;
static int igLastDom = 0;
static int igRefreshTime = 0;
static int igGapCount = 0;
static int igGenerateFLD = 1;
static int igGenerateTM2 = 0;
static int igSuppressCntZero = 0;
static int igCheckNatureLength = 1;

/* parameters of [timepriority] from cfg-file*/
static int igSO = 0;   /* sched_open */
static int igSC = 0;   /* sched_close*/
static int igAO = 0;   /* act_open */
static int igAC = 0;   /* act_close */
static int igOAS = 0;  /* open_after_sched */
static int igCAS = 0;  /* close_after_sched */

static int igUpRe = 1;
static int igUpdateRemarksAFT = 1;
static int igUpdateBeltCREC = FALSE;
static char pcgBelt1Open[16] = "B1BA";
static char pcgBelt1Close[16] = "B1EA";
static char pcgBelt2Open[16] = "B2BA";
static char pcgBelt2Close[16] = "B2EA";

static int igIntAndDomDisplays = 0;

static char pcgFTYP[256] = "\0";
static char pcgDispBelt[10] = "\0";
static char pcgDispGate[10] = "\0";

static char pcgHdlDACO[16] = "STEV";
static int igGenerateStevFromCounter = FALSE;
static char pcgDisplayInT1AndT2[16] = "NO";
static char pcgShowAllFlightsInT2[16] = "NO";
static char pcgCheckCounterOnly[16] = "NO";
static char pcgFixCnt[16] = "";
static char pcgUseGateStatusFields[16] = "NO";
static char pcgUpdateCheckinFromTo[16] = "NO";
static char pcgSimpleHandlingOfMixedFlights[16] = "NO";
static char pcgIntFLTI[16] = "\0";
static char pcgDomFLTI[16] = "\0";
static char pcgMixedFLTI[16] = "\0";
static char pcgIntTerm[16] = "\0";
static char pcgDomTerm[16] = "\0";
static char pcgNatureOrServiceType[16] = "TTYP";
static char pcgFddWithBoardingTime[16] = "NO";
static char pcgCheckTermForCntAsgn[16] = "NO";
static char pcgCountersWithActualTimeOnly[16] = "NO";
static char pcgCountersPreDisplay[16] = "NO";
static int igFddCntEnd = 2;
static int igFddCntCheck = 0;
static char pcgSpecialCommonCountersForLis[16] = "NO";
static char pcgListOfSpecialRemarks[128] = "";
static char pcgSpecialDests[2048] = "";
static char pcgHandleGate2[16] = "NO";
static char pcgSetGd1xWithBoa[16] = "YES";
static char pcgSetBoardingRemarkWithGd1x[16] = "NO";
static char pcgSendBcForFdd[16] = "YES";
static char pcgSendBcForFld[16] = "YES";
static char pcgHandleChutes[16] = "YES";
static char pcgCheckFtypForChuteDisplays[16] = "NO";
static char pcgListOfStev[16] = "";
static char pcgCorrectPlanBag[16] = "NO";
static char pcgUseFLZTAB[16] = "NO";
static char pcgListOfFtypForCca[16] = "O";
static char pcgCloseCounter[16] = "NO";
static char pcgCloseGate[16] = "NO";

static char pcgRtypBELT[16] = "\0";
static char pcgRtypCOUNTER[16] = "\0";
static char pcgRtypGATE[16] = "\0";
static char pcgRtypCHUTE[16] = "\0";
static char pcgRtypWRO[16] = "\0";
static char pcgMyRtypes[80] = "\0";

static char pcgDynamicActionConfig[16] = "NO";

static char pcgUnknownDelays[64] = "'DEL','NXI'";

static char pcgUnknownDelayRemark[64] = "XXX";
static int igUnknownDelayTimeArr = 0;
static int igUnknownDelayTimeDep = 0;

static int igProcAirlGroups = FALSE;
static int igSelectCounterLogo = FALSE;

static int igInsertIntoFDD = FALSE;

/* Variables for Remarks */
static char pcgLanded[16] = "\0";
static char pcgArrived[16] = "\0";
static char pcgDelayed[16] = "\0";
static char pcgDelay[16] = "\0";
static char pcgExpected[16] = "\0";
static char pcgDeparted[16] = "\0";
static char pcgBoarding[16] = "\0";
static char pcgGateOpened[16] = "\0";
static char pcgCanceled[16] = "\0";
static char pcgGateclosed[16] = "\0";
static char pcgDelayedGateOpen[16] = "\0";
static char pcgAirborne[16] = "\0";
static char pcgFinalcall[16] = "\0";
static char pcgDiverted[16] = "\0";
static char pcgRerouted[16] = "\0";
static char pcgNextinfo[16] = "\0";
static char pcgNewGate[16] = "\0";
static char pcgFirstBag[16] = "FBG";
static char pcgLastBag[16] = "LBG";
static int igAutoNextInfo = TRUE;
static int igDelayTimeDiffArr = 0;
static int igDelayTimeDiffDep = 0;
static char pcgListOfETARem[1024] = "\0";
static char pcgListOfETDRem[1024] = "\0";

/* Variables for last counters */
static char pcgCKIF[16] = "\0";
static char pcgCKIT[16] = "\0";
static char pcgCKIF_C[16] = "\0";
static char pcgCKIT_C[16] = "\0";
static char pcgCKIF_D[16] = "\0";
static char pcgCKIT_D[16] = "\0";

#if 0
static char  cgTipc05[15];                  /* Delete from Belt Display after ONBL (Int) */
static char  cgTipc06[15];                  /* Delete from Arrival Display */
static char  cgTipc07[15];                  /* Delete on Gate Display */
static char  cgTipc08[15];                  /* Delete from Departure Display */
static char  cgTipc09[15];                  /* Display cancelled Flights */
static char  cgTipc10[15];                  /* Delete from Belt Display after Last Bag (Int) */
static char  cgTipc11[15];                  /* Delete from Gate Display after GCL (Int) */
static char  cgTipc12[15];                  /* Delete from Gate Display after GCL (Dom) */
static char  cgTipc13[15];                  /* Delete from Belt Display after ONBL (Dom) */
static char  cgTipc14[15];                  /* Delete from Belt Display after Last Bag (Dom) */
#endif
struct _timoffs{
  long  lgTipc01;            /* Offset  Timeframe Arrival Start default: -10H */
  long  lgTipc02;            /* Offset  Timeframe Arrival End  default: 2H */
  long  lgTipc03;            /* Offset  Timeframe Departure Start default: -2H */
  long  lgTipc04;            /* Offset  Timeframe Departure End   default: 10H */
  long  lgTipc05;            /* Offset  Delete from Belt Display after ONBL (Int) default: 10M*/
  long  lgTipc06;            /* Offset  Delete from Arrival Display default: 2H */
  long  lgTipc07;            /* Offset  Delete on Gate Display default: 2H */
  long  lgTipc08;            /* Offset  Delete from Departure Display default: 2H */
  long  lgTipc09;            /* Offset  Display cancelled Flights default: -1H */
  long  lgTipc10;            /* Offset  Delete from Belt Display after Last Bag (Int) default: 2H */
  long  lgTipc11;            /* Offset  Delete from Gate Display after GCL (Int) default: 2H*/
  long  lgTipc12;            /* Offset  Delete from Gate Display after GCL (Dom) default: 2H */
  long  lgTipc13;            /* Offset  Delete from Belt Display after ONBL (Dom) default: 2H */
  long  lgTipc14;            /* Offset  Delete from Belt Display after Last Bag (Dom) default: 2H */
};
typedef struct _timoffs TIMOFFS;

static TIMOFFS timeoffsets;




struct _fddfields {

  char pclFddADID[FDD_LEN_S] ;
  char pclFddAIRB[FDD_LEN_S] ;
  char pclFddAPC3[FDD_LEN_S] ;
  char pclFddAURN[FDD_LEN_S] ; 
  char pclFddBLT1[FDD_LEN_S] ;
  char pclFddBLT2[FDD_LEN_S] ;
  char pclFddCDAT[FDD_LEN_S] ;
  char pclFddCKIF[FDD_LEN_S] ;
  char pclFddCKIT[FDD_LEN_S] ;
  char pclFddDACO[FDD_LEN_S] ;
  char pclFddDNAT[FDD_LEN_S] ;                                                                          
  char pclFddETOF[FDD_LEN_S] ;
  char pclFddFLNO[FDD_LEN_S] ;
  char pclFddFTYP[FDD_LEN_S] ;
  char pclFddGTD1[FDD_LEN_S] ;
  char pclFddGTD2[FDD_LEN_S] ;
  char pclFddHOPO[FDD_LEN_S] ;
  char pclFddINDO[FDD_LEN_S] ;
  char pclFddLAND[FDD_LEN_S] ;
  char pclFddLSTU[FDD_LEN_S] ;
  char pclFddOFBL[FDD_LEN_S] ;
  char pclFddONBL[FDD_LEN_S] ;
  char pclFddREMP[FDD_LEN_S] ;
  char pclFddSTOF[FDD_LEN_S] ;
  char pclFddTIFF[FDD_LEN_S] ;
  char pclFddURNO[FDD_LEN_S] ;
  char pclFddUSEC[FDD_LEN_S] ;
  char pclFddUSEU[FDD_LEN_S] ;
  char pclFddVIA3[FDD_LEN_S] ;
  char pclFddWRO1[FDD_LEN_S] ; 
  char pclFddDSEQ[FDD_LEN_S] ; 
  char pclFddRMTI[FDD_LEN_S] ; 
  char pclFddGD1B[FDD_LEN_S] ; 
  char pclFddGD2B[FDD_LEN_S] ; 
  char pclFddNODE[FDD_LEN_S] ; 
  char pclFddOBL1[FDD_LEN_S] ; 
  char pclFddOBL2[FDD_LEN_S] ; 
  char pclFddOGT1[FDD_LEN_S] ; 
  char pclFddOGT2[FDD_LEN_S] ; 


};
typedef struct _fddfields FDDFIELDS;

static FDDFIELDS rgFddFlds;

struct _fldfields {
  char pclFldAURN[FLD_LEN_S];
  char pclFldURNO[FLD_LEN_S];
  char pclFldABTI[FLD_LEN_S];
  char pclFldACTI[FLD_LEN_S];
  char pclFldAFTI[FLD_LEN_S];
  char pclFldAOTI[FLD_LEN_S];
  char pclFldCDAT[FLD_LEN_S];
  char pclFldCREC[128];
  char pclFldCTYP[FLD_LEN_S];
  char pclFldDCTI[FLD_LEN_S];
  char pclFldDOTI[FLD_LEN_S];
  char pclFldDSEQ[FLD_LEN_S];
  char pclFldHOPO[FLD_LEN_S];
  char pclFldLSTU[116];
  char pclFldRNAM[FLD_LEN_S];
  char pclFldRTAB[FLD_LEN_S];
  char pclFldRURN[FLD_LEN_S];
  char pclFldRTYP[FLD_LEN_S];
  char pclFldSCTI[FLD_LEN_S];
  char pclFldSOTI[FLD_LEN_S];
  char pclFldSTAT[64];
  char pclFldUSEC[FLD_LEN_S];
  char pclFldUSEU[FLD_LEN_S];
  char pclFldTIFF[FLD_LEN_S];
  char pclFldOBLF[FLD_LEN_S];
  char pclFldSTOF[FLD_LEN_S];
  char pclFldFLNO[FLD_LEN_S];
  char pclFldJFNO[128];
  char pclFldJCNT[FLD_LEN_S];
};
typedef struct _fldfields FLDFIELDS;

static FLDFIELDS rgFldFlds;


static int igCurNoComCnt = 0;
static int igCurNoDediCnt = 0;

#define igMaxNoComCnt 500
#define igMaxNoDediCnt 500
#define igMaxNoComCntNam 200
#define igMaxNoDediCntNam 100

struct _CommonCounters {
  char pclComFLNU[11];
  char pclComALC2[3];
  char pclComALC3[4];
  char pclCntInt[igMaxNoComCntNam][6];
  char pclCntDom[igMaxNoComCntNam][6];
  char pclCntIntDisp[igMaxNoComCntNam][64];
  char pclCntDomDisp[igMaxNoComCntNam][64];
  int  ilCurNoIntNam;
  int  ilCurNoDomNam;
};
typedef struct _CommonCounters COMMONCOUNTERS;

static COMMONCOUNTERS rgCommonCounters[igMaxNoComCnt];

struct _DedicatedCounters {
  char pclDediFLNU[11];
  char pclDediFLNO[10];
  char pclCntInt[igMaxNoDediCntNam][6];
  char pclCntDom[igMaxNoDediCntNam][6];
  int  ilCurNoIntNam;
  int  ilCurNoDomNam;
};
typedef struct _DedicatedCounters DEDICATEDCOUNTERS;

static DEDICATEDCOUNTERS rgDedicatedCounters[igMaxNoDediCnt];


static CNTDISP *prgCntDisp = NULL;  /*zeiger auf beginn des Speicherbereichs f�r CntDisp*/
static DOMIX   *prgDoMix   = NULL;  /*zeiger auf beginn des Speicherbereichs f�r DoMix*/
static CNTDISP *prguCntDisp = NULL; /*zeiger auf CntDisp der bentuzt wird*/
static DOMIX   *prguDoMix   = NULL; /*zeiger auf DoMix der benutzt wird*/
static COMCODESHARE *prgComCodeShare = NULL;   /*zeiger auf beginn des Speicherbereichs */
static COMCODESHARE *prguComCodeShare = NULL;  /*zeiger auf current */

static char  cgErrMsg[1024];
static char  pcgTabEnd[8];      
static char  cgHopo[8];   


static EVENT *prgOutEvent  = NULL;


struct _arrayinfo {
    HANDLE   rrArrayHandle;
    char     crArrayName[ARR_NAME_LEN+1];
    char     crArrayFieldList[ARR_FLDLST_LEN+1];
    long     lrArrayFieldCnt;
    char   *pcrArrayRowBuf;
    long     lrArrayRowLen;
    long   *plrArrayFieldOfs;
    long   *plrArrayFieldLen;

    HANDLE   rrIdx01Handle;
    char     crIdx01Name[ARR_NAME_LEN+1];
    char     crIdx01FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx01FieldCnt;
    char   *pcrIdx01RowBuf;
    long     lrIdx01RowLen;
    long   *plrIdx01FieldPos;
    long   *plrIdx01FieldOrd;


    HANDLE   rrIdx02Handle;
    char     crIdx02Name[ARR_NAME_LEN+1];
    char     crIdx02FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx02FieldCnt;
    char   *pcrIdx02RowBuf;
    long     lrIdx02RowLen;
    long   *plrIdx02FieldPos;
    long   *plrIdx02FieldOrd;

    HANDLE   rrIdx03Handle;
    char     crIdx03Name[ARR_NAME_LEN+1];
    char     crIdx03FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx03FieldCnt;
    char   *pcrIdx03RowBuf;
    long     lrIdx03RowLen;
    long   *plrIdx03FieldPos;
    long   *plrIdx03FieldOrd;

    HANDLE   rrIdx04Handle;
    char     crIdx04Name[ARR_NAME_LEN+1];
    char     crIdx04FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx04FieldCnt;
    char   *pcrIdx04RowBuf;
    long     lrIdx04RowLen;
    long   *plrIdx04FieldPos;
    long   *plrIdx04FieldOrd;
#if 0
    HANDLE   rrIdx05Handle;
    char     crIdx05Name[ARR_NAME_LEN+1];
    char     crIdx05FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx05FieldCnt;
    char   *pcrIdx05RowBuf;
    long     lrIdx05RowLen;
    long   *plrIdx05FieldPos;
    long   *plrIdx05FieldOrd;

    HANDLE   rrIdx06Handle;
    char     crIdx06Name[ARR_NAME_LEN+1];
    char     crIdx06FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx06FieldCnt;
    char   *pcrIdx06RowBuf;
    long     lrIdx06RowLen;
    long   *plrIdx06FieldPos;
    long   *plrIdx06FieldOrd;
#endif
};
typedef struct _arrayinfo ARRAYINFO;


static ARRAYINFO rgAftArray;
static ARRAYINFO rgFldArray;
static ARRAYINFO rgFddArray;
static ARRAYINFO rgCcaArray;
static ARRAYINFO rgAltArray;
static ARRAYINFO rgChaArray;

/******** AFTTAB-FIELDS *********/
static int igAftURNO;
static int igAftADID;
static int igAftFLNO;
static int igAftFLTI;
static int igAftFLNS;
static int igAftSTOA;
static int igAftSTOD;
static int igAftDES3;
static int igAftORG3;
static int igAftVIA3;
static int igAftETOA;
static int igAftETOD;
static int igAftONBL;
static int igAftOFBL;
static int igAftTTYP;
static int igAftREMP;
static int igAftBLT1;
static int igAftBLT2;
static int igAftGTD1;
static int igAftCKIT;
static int igAftGTD2;
static int igAftJFNO;
static int igAftETAI;
static int igAftETDI;
static int igAftTMB1;
static int igAftTIFA;
static int igAftTIFD;
static int igAftTISA;
static int igAftTISD;
static int igAftCKIF;
static int igAftWRO1;
static int igAftGD1X;
static int igAftGD1Y;
static int igAftGD2X;
static int igAftGD2Y;
static int igAftB1BA;
static int igAftB1EA;
static int igAftB2BA;
static int igAftB2EA;
static int igAftJCNT;
static int igAftVIAN;
static int igAftVIAL;
static int igAftLAND;
static int igAftBOAO;
static int igAftBOAC;
static int igAftALC2;
static int igAftALC3;
static int igAftAIRB;
static int igAftB1BS;
static int igAftB1ES;
static int igAftB2BS;
static int igAftB2ES;
static int igAftGD1B;
static int igAftGD1E;
static int igAftGD2B;
static int igAftGD2E;
static int igAftW1BA;
static int igAftW1EA;
static int igAftW1BS;
static int igAftW1ES;
static int igAftSTEV;
static int igAftFTYP;
static int igAftNXTI;
static int igAftTGD1;
static int igAftTGD2;
static int igAftTET1;
static int igAftBAS1;
static int igAftBAE1;
static int igAftBAS2;
static int igAftBAE2;
static int igAftRGD1;
static int igAftRGD2;


/************ FLDTAB-Fields *************/
static int igFldURNO;
static int igFldAURN;
static int igFldABTI;
static int igFldACTI;
static int igFldAFTI;
static int igFldAOTI;
static int igFldCDAT;
static int igFldCREC;
static int igFldCTYP;
static int igFldDCTI;
static int igFldDOTI;
static int igFldDSEQ;
static int igFldHOPO;
static int igFldLSTU;
static int igFldRNAM;
static int igFldRTAB;
static int igFldRURN;
static int igFldRTYP;
static int igFldSCTI;
static int igFldSOTI;
static int igFldSTAT;
static int igFldUSEC;
static int igFldUSEU;
static int igFldTIFF;
static int igFldOBLF;
static int igFldSTOF;

/*********** FDDTAB-Fields ***************/

static int igFddADID;
static int igFddAIRB;
static int igFddAPC3;
static int igFddAURN;
static int igFddBLT1;
static int igFddBLT2;
static int igFddCDAT;
static int igFddCKIF;
static int igFddCKIT;
static int igFddDACO;
static int igFddDNAT;                                                                          
static int igFddETOF;
static int igFddFLNO;
static int igFddFTYP;
static int igFddGTD1;
static int igFddGTD2;
static int igFddHOPO;
static int igFddINDO;
static int igFddLAND;
static int igFddLSTU;
static int igFddOFBL;
static int igFddONBL;
static int igFddREMP;
static int igFddSTOF;
static int igFddTIFF;
static int igFddURNO;
static int igFddUSEC;
static int igFddUSEU;
static int igFddVIA3;
static int igFddWRO1; 
static int igFddDSEQ;
static int igFddRMTI;
static int igFddGD1B;
static int igFddGD2B;
static int igFddNODE;
static int igFddOBL1;
static int igFddOBL2;
static int igFddOGT1;
static int igFddOGT2;



/************ CCATAB-Fields ***************/

static int igCcaURNO;
static int igCcaFLNU;
static int igCcaCKIC;
static int igCcaCKBS;
static int igCcaCKES;
static int igCcaCKBA;
static int igCcaCKEA;
static int igCcaCTYP;
static int igCcaDISP;
static int igCcaLSTU;
static int igCcaCDAT;
static int igCcaREMA;
static int igCcaGRNU;
static int igCcaFLNO;
static int igCcaJFNO;
static int igCcaFLAG;


/*********** ALTTAB-Fields *****************/

static int igAltURNO;
static int igAltALC2;
static int igAltALC3;


/*********** CHATAB-Fields *****************/

static int igChaURNO;
static int igChaLNAM;
static int igChaRURN;
static int igChaTIFB;
static int igChaTIFE;
static int igChaDES3;
static int igChaCHGF;
static int igChaFCLA;
static int igChaLAFX;


static char  pcgHomeAp[XS_BUFF];      /* buffer for home airport */
static char  pcgTwEnd[XS_BUFF];          /* buffer for TABEND */


static int igSec1End = 0;
static int igSec2End = 0;
static int igSec4End = 0;


/******************************************************************************/
/* Function prototypes                                                          */
/******************************************************************************/
static int Init_fldhdl();
static int    Reset(void);                       /* Reset program          */
static void    Terminate(int);                   /* Terminate program      */
static void    HandleSignal(int);                 /* Handles signals        */
static void    HandleErr(int);                    /* Handles general errors */
static void    HandleQueErr(int);                 /* Handles queuing errors */
static int    HandleData(void);                  /* Handles event data     */
static void HandleQueues(void);                    /* Waiting for Sts.-switch*/
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
                           char *pcpCfgBuffer);    /*Reads a ConfigEntry*/
static int ReadConfigEntryRemark(char *pcpSection,char *pcpKeyword,
                                 char *pcpCfgBuffer);    /*Reads a Remark ConfigEntry*/
static int GetConfig();
static int FillCntDisp(char *charbuff);
static int FillDoMix(char *charbuff);
static int FillComCodeShare(char *charbuff);


static int ShowTimeOffsets(TIMOFFS *prpTimeOffsets);
static int SetDefaultTimeOffsets(TIMOFFS *prpTimeOffsets);
static int GetTimeFrame();
static int GetTimeOffsets();
static int CreateCEDAArrays();
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, 
                        char *pcpArrayFieldList,char *pcpAddFields,
                        long *pcpAddFieldLens,ARRAYINFO *prpArrayInfo, 
                        char *pcpSelection,BOOL bpFill);
static int  GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList, long *pipLen);
static int  GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina, long *pipLen);
static int  TriggerAction(char *pcpTableName);
static int  WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem);
static int  CheckQueue(int ipModId, ITEM **prpItem);
static int InitFieldIndex();
static int FindItemInArrayList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos);
static int Calculateoffsets(long *plloffset, char *pclTipc, int sign);
static void TrimRight(char *pcpBuffer);
static int UpdateFlight_A(char *pclCurRowAFT, long llRowNumFDD);
static int UpdateFlight_D(char *pclCurRowAFT, long llRowNumFDD);
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
             char *pcpTwStart, char *pcpTwEnd,
             char *pcpSelection,char *pcpFields,char *pcpData,
             char *pcpAddStruct,int ipAddStructSize);

static int Handle_Arrival(char *pclRowAFT, long llRowNumAFT, long *llRowNumFDD);
static int Handle_Departure(char *pclRowAFT, long llRowNumAFT, long *llRowNumFDD);

static int HandleFLTI_A (char *pclCurRowAFT, char *pclCurRowFDD, long *pllRowNumFDD);
static int HandleFLTI_Int_A(char *pclCurRowAFT, char *pclCurRowFDD, long *pllRowNumFDD, char *pcpFtyp);
static int HandleFLTI_Dom_A(char *pclCurRowAFT, char *pclCurRowFDD, long *plpRowNumFDD);
static int HandleFLTI_Mix_A(char *pclCurRowAFT, char *pclCurRowFDD, long *plpRowNumFDD);
static int HandleFLTI_D (char *pclCurRowAFT, char *pclCurRowFDD, long *pllRowNumFDD);
static int HandleFLTI_Int_D(char *pclCurRowAFT, char *pclCurRowFDD, long *pllRowNumFDD, char *pcpFtyp);
static int HandleFLTI_Dom_D(char *pclCurRowAFT, char *pclCurRowFDD, long *plpRowNumFDD);
static int HandleFLTI_Mix_D(char *pclCurRowAFT, char *pclCurRowFDD, long *plpRowNumFDD);
static int HandleTerminal_A(char *pcpCurRowAFT, char *pcpRemark, int *ipTerm);
static int HandleTerminal_D(char *pcpCurRowAFT, char *pcpRemark, int *ipTerm1, int *ipTerm2);
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo);
static int TimeToStr(char *pcpTime,time_t lpTime);
static int GetCfgOffsetTime(char *pclCfgBuffer,long *plloffset);
static int OffsettimeFromNow (long *pllOffset, char *pclCEDATime);
static int InitFddStruct();
static int ShowFddStruct();
static int StructPutFieldFDD(long *plpRowNumFDD);
static int StructAddRowPartFDD(long *plpRowNumFDD);
static int StructAddRowPartFDDArr(long *plpRowNumFDD);
static int StructAddRowPartFDDDep(long *plpRowNumFDD);
static int InitFldStruct();
static int ShowFldStruct();
static int StructPutFieldFLD(long *plpRowNumFDD);
static int StructAddRowPartFLD(long *plpRowNumFDD);
static int RUNCmd();
static int UFR_AFT_Cmd(char *pcpFields, char *pcpData);
static int GetDedicatedCounter(char *pcpType, char *pcpFlno, char *pcpCKIF, char *pcpCKIT);
static int GetCommonCounter(char *pcpType, char *pcpAirl, char *pcpCKIF, char *pcpCKIT, char *pcpDest);
static int GetCDT(char *pclCDA, char *pcpCDT, int pipNum);
static int GetCommonAndDedicatedCounter(char *pcpType, char *pcpAirl, char *pcpFlno, char *pcpCKIF, char *pcpCKIT,
                                        char *pcpCDA, char *pcpMain, char *pcpDest);
static int GetCounterTerm(char *pcpCDA, char *pcpSTEV, char *pcpALC, char *pcpFLNO,
                          char *pcpCKIF, char *pcpCKIT);
static int DisplayFlight (char *pcpINDO, char *pcpFTYP, char *pcpAftTTYP, int pipNum);
static int CCA_Update(char *pcpFields, char *pcpData);
static int FDD_Update(char *pcpAftURNO, char *pcpRowAFT, long lpRowNumAFT);
static int SetDomVia (char *pcpADID, char *pcpFTYP, int pipNumDomVia, char  *pclCurRowAFT);
static int NumDomVia(char *pcpTTYP);
static int SetDNAT(char *pcpSection);
static int UpdateFLDTAB(char *pclCurRowAFT);
static int TestCmd();
static int GetMaxTime(char *pcpTime1, char *pcpTime2, char *pcpMaxTime);
static int GetMinTime(char *pcpTime1, char *pcpTime2, char *pcpMinTime);
static int ValidTime(char *pclFldSOTI, char *pclFldSCTI, char *pclFldDOTI, char *pclFldDCTI);
static int PutFieldFLD (char *pcpField, char *pcpValue, long *plpRowNum);
static int SetTimeFieldFLD();
static int SetDSEQ(char *pcpRNAM, char *pcpRTYP);
static int UPDCmd();
static int DeleteDSEQ();
static int RebuildChutes();
static int DeleteAURN_FLD(char *pcpAURN, char *pcpDelete, char *pcpCurRowAFT);
static int CleanUpFLD();
static int RebuildCounters();
static int PutInCounterArray(char *pcpCTYP, char *pcpFLNU, char *pcpCKIC, char *pcpDISP);
static int ConvertCounterArray();
static int GetCommonCodeShares(int ilI);
static int SortCounters(int ilI);
static int CheckForUnknownDelays();
static int GetFLNOandJFNO(char *pcpFlnu, char *pcpFlno, char *pcpJfno, char *pcpJcnt);
static int AddSecondsToCEDATime12(char *,long,int);
static int GetChutes(char *pcpFlno, char *pcpUrno, char *pcpChaf, char *pcpChat);
static int CheckFtyp(char *pcpFlnu);
static int CheckCcaFtyp(char *pcpFlnu, char *pcpCtyp);
static int UpdateCheckInFromTo(char *pcpFddAurn, char *pcpCKIFInt, char *pcpCKITInt, char *pcpCKIFDom, char *pcpCKITDom);
static int GenerateStevFromCounter(char *pcpCurRowAFT, char *pcpNewSTEV);

extern int get_real_item(char *,char *,int); 
extern int BuildItemBuffer( char *pcpData, char *pcpFieldList, int ipNoOfFields, char *pcpSepChr);
extern int AddSecondsToCEDATime(char *,long,int);
extern int get_no_of_items(char *s);
extern int GetUrno(int *pipUrno);
extern int GetNoOfElements(char *s, char c);
extern int GetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel,
                       char *pcpDef, char *pcpTrim);


/******************************************************************************/
/* Function prototypes                                                          */
/******************************************************************************/
static int    Reset(void);                       /* Reset program          */

static void    HandleSignal(int);                 /* Handles signals        */


/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int    ilRC = RC_SUCCESS;            /* Return code           */
    int    ilCnt = 0;
    

    INITIALIZE;
    SetSignals(HandleSignal);/* General initialization    */
    dbg(TRACE,"MAIN: version <%s>",sccs_version);
    /* Attach to the MIKE queues */
    do
    {
        ilRC = init_que();
        if(ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRC != RC_SUCCESS));
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */
    do
    {
        ilRC = init_db();
        if (ilRC != RC_SUCCESS)
        {
            check_ret(ilRC);
            dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
            sleep(6);
        ilCnt++;
        } /* end of if */
    } while((ilCnt < 10) && (ilRC != RC_SUCCESS));
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
        sleep(60);
        exit(2);
    }else{
        dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */
    /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
    sprintf(cgConfigFile,"%s/fldhdl",getenv("BIN_PATH"));
    ilRC = TransferFile(cgConfigFile);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */
    /* uncomment if necessary */
    /* sprintf(cgConfigFile,"%s/fldhdl.cfg",getenv("CFG_PATH")); */
    /* ilRC = TransferFile(cgConfigFile); */
    /* if(ilRC != RC_SUCCESS) */
    /* { */
    /*     dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile); */
    /* } */ /* end of if */
    ilRC = SendRemoteShutdown(mod_id);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */
    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */
    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        if(igInitOK == FALSE)
        {
            ilRC = Init_fldhdl();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"Init_fldhdl: init failed!");
            } /* end of if */
        }/* end of if */
    } else {
        Terminate(1);
    }/* end of if */
    dbg(TRACE,"MAIN: initializing OK");
    for(;;)
    {
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
            
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                /* CloseConnection(); */
                HandleQueues();
                break;    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                break;    
            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;
            case    SHUTDOWN    :
                /* process shutdown - maybe from uutil */
                Terminate(1);
                break;
                    
            case    RESET        :
                ilRC = Reset();
                break;
                    
            case    EVENT_DATA    :
                if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                {
                  /*nap(500);*/
                    ilRC = HandleData();
                    if(ilRC != RC_SUCCESS)
                    {
                        HandleErr(ilRC);
                    }/* end of if */
                }else{
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            case    111 :
                    ilRC = RUNCmd();
                break;
            case    112 :
                    ilRC = UPDCmd();
                break;
            case    567 :
                    ilRC = TestCmd();
                break;

            default            :
                dbg(TRACE,"MAIN: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRC);
        } /* end else */
        
    } /* end for */
    
/*      exit(0); */
    
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int Init_fldhdl()
{
    int ilRC = RC_SUCCESS;            /* Return code */
        int ilTryAgain = 10;
        int ilCnt = 0;
        int ilWaitSec;
        int ilWaitTtl;                                                                                  
    char clSection[64];
    char clKeyword[64];
    int ilCurrent = 0;
    long llRowNum = ARR_FIRST;
    char *pclRow;
    char  pcgCfgBuffer[1024];    
        short slCursor;
    short slSqlFunc;
    char clSqlBuf[256];
    char clData[100];
    char pclTEST[10] = "1-03";

    dbg(DEBUG,"%05d: atoi(1-03): <%d>",__LINE__,atoi(pclTEST));

    ilRC = SetDefaultTimeOffsets(&timeoffsets);
    if (ilRC != RC_SUCCESS)
      {
        dbg(TRACE,"%05d: Init_fldhdl : SetDefaultTimeOffsets failed!",__LINE__);
        return RC_FAIL;
      }
    
/*  ilRC = ShowTimeOffsets(&timeoffsets);  */
    if (ilRC != RC_SUCCESS)
      {
        dbg(TRACE,"%05d: Init_fldhdl : ShowTimeOffsets failed!",__LINE__);
        return RC_FAIL;
      }
    


    /* reading default home-airport from sgs.tab */
    
    dbg(DEBUG,"%05d: reading home-ariport",__LINE__);
    memset(pcgHomeAp,0x00,sizeof(pcgHomeAp));
    dbg(DEBUG,"%05d: memset done",__LINE__);
    ilRC = tool_search_exco_data("SYS","HOMEAP",pcgHomeAp);
    if (ilRC != RC_SUCCESS)
      {
        dbg(TRACE,"%05d: Init_fldhdl : No HOMEAP entry in sgs.tab: EXTAB! Please add!",__LINE__);
        return RC_FAIL;
      }
    else
      {
        dbg(TRACE,"%05d: Init_fldhdl : HOMEAP = <%s>",__LINE__,pcgHomeAp);
      }
 
                               /********************* get TabEnd from systab ******************/
    sprintf(&clSection[0],"ALL");
    sprintf(&clKeyword[0],"TABEND");
    ilRC = tool_search_exco_data(&clSection[0], &clKeyword[0], &pcgTabEnd[0]);
    


    memset(pcgTwEnd,0x00,XS_BUFF);
    sprintf(pcgTwEnd,"%s,%s,%s",pcgHomeAp,pcgTabEnd,mod_name);
    dbg(TRACE,"%05d: Init_fldhdl : TW_END = <%s>",__LINE__,pcgTwEnd);



    /* now reading from configfile or from database */

                              /* preparing configfile-name*/

        sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
        dbg(TRACE,"ConfigFile <%s>",cgConfigFile);  


                               /*Check if config-file exists, if not -> try to read it every 60 seconds*/

    ilRC = ReadConfigEntry("MAIN","section1",pcgCfgBuffer);
    while (ilRC != 0)
      {
        dbg(TRACE,"GetConfig: **** ReadConfigEntry ****** failed");
        sleep (60);
        ilRC = ReadConfigEntry("MAIN","section1",pcgCfgBuffer);
      }

    dbg(TRACE,"Now call GetConfig");                              
    ilRC = GetConfig();  /*Read configfile*/
    dbg(TRACE,"Return from GetConfig: ilRC = %d",ilRC);
#if 0
    ilCnt = ilTryAgain;
    ilWaitSec = 5;
    ilWaitTtl = 0;
    do 
    {
       ilRC = init_db();
       if (ilRC != RC_SUCCESS) 
       {
          dbg(TRACE, "init_db failed, RC = <%d>", ilRC);
          sleep(ilWaitSec);
          ilCnt--;
          ilWaitTtl += ilWaitSec;
       }           /* end if */
    } while ((ilCnt > 0) && (ilRC != RC_SUCCESS));                  
    dbg(TRACE,"DB initialized");
#endif
    
    if (igGenerateFLD == 1)
    {
       sprintf(clSqlBuf,"DELETE FROM FLDTAB WHERE RTYP IN (%s)",pcgMyRtypes);
    }
    else
    {
       sprintf(clSqlBuf,"DELETE FROM FLDTAB WHERE RTYP = 'XXX'");
    }
    dbg(TRACE,"clSqlBuf: <%s>",clSqlBuf);    
    slSqlFunc= COMMIT;
    slCursor = 0;
    ilRC = sql_if(slSqlFunc,&slCursor,clSqlBuf,clData);
    if (ilRC == RC_FAIL) 
    {
       dbg(DEBUG, "%05d:SQL Buffer<%s>",__LINE__, clSqlBuf);
       get_ora_err(-1, cgErrMsg);
       dbg(TRACE, "Oracle Error <%s>", cgErrMsg);
    }                           
    dbg(TRACE,"Return from DELETE = %d",ilRC);
    close_my_cursor(&slCursor);

/*  ************ Now get config parameter for groups in CCATAB ********************/
    igProcAirlGroups = FALSE;
    ilRC = ReadConfigEntry("MAIN","CCA_AIRLINE_GROUP",pcgCfgBuffer);
    if (ilRC == RC_SUCCESS)
    {
       if (strcmp(pcgCfgBuffer,"YES") == 0)
       {
          igProcAirlGroups = TRUE;
       }
    }
    if (igProcAirlGroups == TRUE)
    {
       dbg(TRACE,"Processing of Airline Groups is switched on");
    }
    else
    {
       dbg(TRACE,"Processing of Airline Groups is switched off");
    }
    strcpy(CCA_FIELDS_TAB,CCA_FIELDS_STANDARD);
    if (igProcAirlGroups == TRUE)
    {
       strcat(CCA_FIELDS_TAB,",GRNU");
    }
    strcpy(CCA_FIELDS,CCA_FIELDS_TAB);
    strcat(CCA_FIELDS,",");
    strcat(CCA_FIELDS,CCA_FIELDS_ADD_ON);

/*  ************ Now get config parameter for selection of counter logos *******************/
    igSelectCounterLogo = FALSE;
    ilRC = ReadConfigEntry("MAIN","SELECT_COUNTER_LOGO",pcgCfgBuffer);
    if (ilRC == RC_SUCCESS)
    {
       if (strcmp(pcgCfgBuffer,"YES") == 0)
       {
          igSelectCounterLogo = TRUE;
       }
    }
    if (igSelectCounterLogo == TRUE)
    {
       dbg(TRACE,"Selection of Counter Logo is switched on");
    }
    else
    {
       dbg(TRACE,"Selection of Counter Logo is switched off");
    }
    strcpy(FLD_FIELDS_TAB,FLD_FIELDS_STANDARD);
    if (igSelectCounterLogo == TRUE)
    {
       strcat(FLD_FIELDS_TAB,",FLNO,JFNO,JCNT");
    }
    strcpy(FLD_FIELDS,FLD_FIELDS_TAB);
    strcat(FLD_FIELDS,",");
    strcat(FLD_FIELDS,FLD_FIELDS_ADD_ON);

    /********************************* Now get Time-Offsets from TipTab ******************/

   ilRC = GetTimeOffsets();      
    if (ilRC != RC_SUCCESS)
      {
    dbg(TRACE,"%05d: GetTimeOffsets() failed, ilRC: <%d>",__LINE__,ilRC);
      }
 
    /*now the timeOffsets  can be found in timeoffsets.cgTipc01 - cgTipc12*/

    

    /******************************** Now create CEDAArrays ******************************/

    dbg(TRACE,"%05d: now CreateCEDAArrays!!",__LINE__);

 
                                 /******************** create the array *************/
    ilRC = CreateCEDAArrays();
    if (ilRC != RC_SUCCESS)
    {
       dbg(TRACE,"%05d: CreateCEDAArrays failed, ilRC: <%d>",__LINE__,ilRC);
    }
    else
    {
       dbg(TRACE,"%05d: CreateCEDAArrays successful, ilRC: <%d>",__LINE__,ilRC);
    }

    /*ilRC = SetTimeFieldFLD();*/

    ilRC = UPDCmd();
    ilRC = RUNCmd();
   
    if (ilRC != RC_SUCCESS)
      {
    dbg(TRACE,"%05d: CreateCEDAArraysFirst RUN failed, ilRC: <%d>",__LINE__,ilRC);
      }


    /* debug_level = 0; */
    igInitOK = TRUE;
    return(ilRC);
    
} /* end of initialize */


/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int    ilRC = RC_SUCCESS;                /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRC;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
    /* unset SIGCHLD ! DB-Child will terminate ! */

    dbg(TRACE,"Terminate: now DB logoff ...");

    /*  signal(SIGCHLD,SIG_IGN); */

/*      logoff(); */

    dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


    dbg(TRACE,"Terminate: now leaving ...");

    fclose(outp);

    sleep(ipSleep < 1 ? 1 : ipSleep);
    
    exit(0);
    
} /* end of Terminate */


/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
  switch(pipSig)
    {
    case SIGALRM:
      if(igConnectDeviceFlag==TRUE&&igAlarmCount > 3)
    {
      dbg(DEBUG,"SIGALARM");
      igTcpTimeOutAlarmFlag=TRUE;
    }
      break;
    case SIGPIPE:
      dbg(TRACE,"Broken pipe");
      break;
    case SIGCHLD:
      break;
    case SIGTERM:
      Terminate(1);
      break;
    default    :
      Terminate(10);
      break;
    } /* end of switch */
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET    :    /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    int    ilBreakOut = FALSE;
    
    do
    {
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;    
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;    
    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                ilBreakOut = TRUE;
                break;    
            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;
            case    SHUTDOWN    :
                Terminate(1);
                break;
                        
            case    RESET        :
                ilRC = Reset();
                break;
                        
            case    EVENT_DATA    :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRC);
        } /* end else */
    } while (ilBreakOut == FALSE);
    if(igInitOK == FALSE)
    {
            ilRC = Init_fldhdl();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"Init_fldhdl: init failed!");
            } /* end of if */
    }/* end of if */
    /* OpenConnection(); */
} /* end of HandleQueues */
    

/******************************************************************************/
/*    ReadConfigEntry                                                         */
/******************************************************************************/

static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
                           char *pcpCfgBuffer)
{
  int ilRc = RC_SUCCESS;
  static char  pcgCfgBuffer[1024];
  char clSection[1024];
  char clKeyword[1024];

  strcpy(clSection,pcpSection);
  strcpy(clKeyword,pcpKeyword);
  ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
                         CFG_STRING,pcpCfgBuffer);
  if(ilRc != RC_SUCCESS)
  {
     dbg(TRACE,"Config Entry not found in %s: <%s><%s>",cgConfigFile,clSection,clKeyword);
  } 
  else
  {
     dbg(TRACE,"Config Entry <%s>,<%s>:<%s> found in %s",
         clSection,clKeyword,pcpCfgBuffer,cgConfigFile);
  }/* end of if */
  return ilRc;
} /*End ReadConfigEntry*/


/******************************************************************************/
/*    ReadConfigEntryRemark                                                         */
/******************************************************************************/

static int ReadConfigEntryRemark(char *pcpSection,char *pcpKeyword,
                                 char *pcpCfgBuffer)
{
  int ilRc = RC_SUCCESS;
  static char  pcgCfgBuffer[1024];
  char clSection[1024];
  char clKeyword[1024];

  strcpy(clSection,pcpSection);
  strcpy(clKeyword,pcpKeyword);
  ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
                         CFG_STRING,pcpCfgBuffer);
  if (ilRc != RC_SUCCESS)
  {
     dbg(TRACE,"Config Entry not found in %s: <%s><%s>",cgConfigFile,clSection,clKeyword);
     strcpy(pcpCfgBuffer,"EMPTY");
  } 
  else
  {
     if (strlen(pcpCfgBuffer) == 0)
        strcpy(pcpCfgBuffer,"EMPTY");
     dbg(TRACE,"Config Entry <%s>,<%s>:<%s> found in %s",
         clSection,clKeyword,pcpCfgBuffer,cgConfigFile);
  }/* end of if */
  return ilRc;
} /*End ReadConfigEntryRemark*/


/****************************************************************************/
/* GetConfig                                                                */
/*                                                                          */
/* reads the config-Files information into a local structure                */
/****************************************************************************/

static int GetConfig()
{
  int ilRC = RC_SUCCESS;
  int ilCntSec1 = 0;
  int ilCntSec2 = 0;
  int ilCntSec4 = 0;
  int ili = 0;
  int ilj = 0;
  char  pcgCfgBuffer[1024];
  char clchar ='0';
  char charbuff[1024];
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[1024];
  char pclDataBuf[1024];
  char pclResult[1024];
  int ilCount;
  char pclTmpBuf[1024];
  
  memset(pcgCfgBuffer,0x00,sizeof(pcgCfgBuffer));

  ilRC = ReadConfigEntry("MAIN","int-and-dom-counters",pcgCfgBuffer);
  igIntAndDomCounters = atoi(pcgCfgBuffer);
  ilRC = ReadConfigEntry("MAIN","invalid-counter-fdd",pcgCfgBuffer);
  strcpy(pcgInvalidCounterFdd,pcgCfgBuffer);
  ilRC = ReadConfigEntry("MAIN","first_int_counter",pcgCfgBuffer);
  igFirstInt = atoi(pcgCfgBuffer);
  ilRC = ReadConfigEntry("MAIN","last_int_counter",pcgCfgBuffer);
  igLastInt = atoi(pcgCfgBuffer);
  ilRC = ReadConfigEntry("MAIN","first_dom_counter",pcgCfgBuffer);
  igFirstDom = atoi(pcgCfgBuffer);
  ilRC = ReadConfigEntry("MAIN","last_dom_counter",pcgCfgBuffer);
  igLastDom = atoi(pcgCfgBuffer);
  ilRC = ReadConfigEntry("MAIN","ascii_counter_compare",pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
     igAsciiCounterCompare = atoi(pcgCfgBuffer);
  else
  {
     igAsciiCounterCompare = 1;
     dbg(TRACE,"Set to <%d>",igAsciiCounterCompare);
  }
  ilRC = ReadConfigEntry("MAIN","refresh-time",pcgCfgBuffer);
  igRefreshTime = atoi(pcgCfgBuffer);
  ilRC = ReadConfigEntry("MAIN","gap-count",pcgCfgBuffer);
  igGapCount = atoi(pcgCfgBuffer);
  ilRC = ReadConfigEntry("MAIN","int-and-dom-displays",pcgCfgBuffer);
  igIntAndDomDisplays = atoi(pcgCfgBuffer);
  dbg(TRACE,"int-and-dom-displays = <%d>",igIntAndDomDisplays);
  ilRC = ReadConfigEntry("MAIN","generate-FLD",pcgCfgBuffer);
  igGenerateFLD = atoi(pcgCfgBuffer);
  ilRC = ReadConfigEntry("MAIN","generate_TM2",pcgCfgBuffer);
  igGenerateTM2 = atoi(pcgCfgBuffer);
  ilRC = ReadConfigEntry("MAIN","suppress_counter_zeros",pcgCfgBuffer);
  igSuppressCntZero = atoi(pcgCfgBuffer);
  ilRC = ReadConfigEntry("MAIN","check-nature-length",pcgCfgBuffer);
  igCheckNatureLength = atoi(pcgCfgBuffer);
  ilRC = ReadConfigEntry("MAIN","dynamic-action-config",pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgDynamicActionConfig,pcgCfgBuffer);
  }
  dbg(TRACE,"Dynamic Action Config = <%s>",pcgDynamicActionConfig);
  ilRC = ReadConfigEntry("MAIN","UNKNOWN_DELAYS",pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgUnknownDelays,pcgCfgBuffer);
     dbg(TRACE,"Unknown Delay Remark(s) (New)= <%s>",pcgUnknownDelays);
  }
  ilRC = ReadConfigEntry("MAIN","UNKNOWN_DELAY_REM",pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgUnknownDelayRemark,pcgCfgBuffer);
     dbg(TRACE,"Unknown Delay Remark(s) = <%s>",pcgUnknownDelayRemark);
     ilRC = ReadConfigEntry("MAIN","UNKNOWN_DELAY_TIME_ARR",pcgCfgBuffer);
     igUnknownDelayTimeArr = atoi(pcgCfgBuffer);
     dbg(TRACE,"Unknown Delay Time Arr = <%d>",igUnknownDelayTimeArr);
     ilRC = ReadConfigEntry("MAIN","UNKNOWN_DELAY_TIME_DEP",pcgCfgBuffer);
     igUnknownDelayTimeDep = atoi(pcgCfgBuffer);
     dbg(TRACE,"Unknown Delay Time Dep = <%d>",igUnknownDelayTimeDep);
  }
  else
  {
     dbg(TRACE,"No Unknown Delay Remark configured");
  }

  ilRC = ReadConfigEntry("timepriority","sched_open",pcgCfgBuffer);
  igSO = atoi(pcgCfgBuffer);
  ilRC = ReadConfigEntry("timepriority","sched_close",pcgCfgBuffer);
  igSC = atoi(pcgCfgBuffer);
  ilRC = ReadConfigEntry("timepriority","act_open",pcgCfgBuffer);
  igAO = atoi(pcgCfgBuffer);
  ilRC = ReadConfigEntry("timepriority","act_close",pcgCfgBuffer);
  igAC = atoi(pcgCfgBuffer);
  ilRC = ReadConfigEntry("timepriority","open_after_sched",pcgCfgBuffer);
  igOAS = atoi(pcgCfgBuffer);
  ilRC = ReadConfigEntry("timepriority","close_after_sched",pcgCfgBuffer);
  igCAS = atoi(pcgCfgBuffer);
 
  ilRC = ReadConfigEntry("MAIN","update_remarks",pcgCfgBuffer);
  igUpRe = atoi(pcgCfgBuffer);
  ilRC = ReadConfigEntry("MAIN","update_remarks_AFT",pcgCfgBuffer);
  igUpdateRemarksAFT = atoi(pcgCfgBuffer);
  ilRC = ReadConfigEntry("MAIN","update_crec_for_belts",pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
     igUpdateBeltCREC = atoi(pcgCfgBuffer);
  ilRC = ReadConfigEntry("MAIN","fields_for_crec",pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     ilCount = GetNoOfElements(pcgCfgBuffer,',');
     GetDataItem(pcgBelt1Open,pcgCfgBuffer,1,',',""," ");
     if (ilCount > 1)
        GetDataItem(pcgBelt1Close,pcgCfgBuffer,2,',',""," ");
     if (ilCount > 2)
        GetDataItem(pcgBelt2Open,pcgCfgBuffer,3,',',""," ");
     if (ilCount > 3)
        GetDataItem(pcgBelt2Close,pcgCfgBuffer,4,',',""," ");
  }

  ilRC = ReadConfigEntry("MAIN","section1",pcgCfgBuffer); /*anzahl der Items in section1 ermitteln */
  while (clchar != '\0')
    {
      clchar = pcgCfgBuffer[ili];
      ili++;
      if (clchar == ',')
    {
      ilCntSec1++;
    }
    }
  ilCntSec1++;                                            /*anzahl ergibt sich aus gez�hlten Komma +1*/


  clchar = '0';
  ili = 0;
  ilRC = ReadConfigEntry("MAIN","section2",pcgCfgBuffer); /*anzahl der Items in section1 ermitteln */
   while (clchar != '\0')
     {
       clchar = pcgCfgBuffer[ili];
       ili++;
       if (clchar == ',')
      {
         ilCntSec2++;
     }
     }
   ilCntSec2++;                                            /*anzahl ergibt sich aus gez�hlten Komma +1*/
 

   igSec1End = ilCntSec1;
   igSec2End = ilCntSec2;

   prgCntDisp  = (CNTDISP*) malloc(sizeof(CNTDISP) * ilCntSec1);  /*Speicher f�r CntDisp allocieren*/
   prgDoMix    = (DOMIX*) malloc(sizeof(DOMIX) * ilCntSec2);      /*Speicher f�r DoMix allocieren*/
   prguCntDisp = prgCntDisp;
   prguDoMix   = prgDoMix;

 
       
   ilRC = ReadConfigEntry("MAIN","section1",pcgCfgBuffer); /*read sction1 config-entrys and store them intern*/

   for (ili = 1;get_real_item(charbuff,pcgCfgBuffer,ili) > 0; ili++)
     {
  
       ilRC = FillCntDisp(charbuff);
       prguCntDisp++;
     }

   ilRC = ReadConfigEntry("MAIN","section2",pcgCfgBuffer); /*read sction2 config-entrys and store them intern*/

   for (ili = 1;get_real_item(charbuff,pcgCfgBuffer,ili) > 0; ili++)
     {
       dbg(DEBUG,"%05d: charbuff: <%s>",__LINE__,charbuff);
     
       ilRC = FillDoMix(charbuff);
       prguDoMix++;
     }

   ilRC = ReadConfigEntry("MAIN","section4",pcgCfgBuffer); /* get no of items in section 4 */
   dbg(DEBUG,"CfgBuffer = <%s> , ilRC = %d",pcgCfgBuffer,ilRC);
   if (ilRC == RC_SUCCESS && strlen(pcgCfgBuffer) > 0)
   {
      ili = 0;
      clchar = '0';
      while (clchar != '\0')
      {
         clchar = pcgCfgBuffer[ili];
         ili++;
         if (clchar == ',')
         {
            ilCntSec4++;
         }
      }
      ilCntSec4++;
   }
   else
   {
      ilCntSec4 = 0;
   }
   igSec4End = ilCntSec4;
   dbg(DEBUG,"Section4 = <%s> , No = %d",pcgCfgBuffer,igSec4End);
   if (igSec4End > 0)
   {
      prgComCodeShare = (COMCODESHARE*) malloc(sizeof(COMCODESHARE) * ilCntSec4);
      prguComCodeShare = prgComCodeShare;
      for (ili = 1; get_real_item(charbuff,pcgCfgBuffer,ili) > 0; ili++)
      {
         ilRC = FillComCodeShare(charbuff);
         if (ilRC == RC_SUCCESS)
         {
            prguComCodeShare++;
         }
      }
   }
   dbg(DEBUG,"Section4 = %d items",igSec4End);
   prguComCodeShare = prgComCodeShare;
   for (ili=0; ili<igSec4End; ili++)
   {
      dbg(DEBUG,"Section4: No Code Shares for <%s> = %d",
          prguComCodeShare->MainAirline,prguComCodeShare->ilCntCS);
      for (ilj=0; ilj<prguComCodeShare->ilCntCS; ilj++)
      {
         dbg(DEBUG,"Section4: Code Share <%s>",&prguComCodeShare->CodeShare[ilj][0]);
      }
      prguComCodeShare++;
   }

   ilRC = ReadConfigEntry("MAIN","selection_ftyp",pcgFTYP); /*anzahl der Items in section1 ermitteln */
   dbg(TRACE,"%05d: selection_ftyp: <%s>",__LINE__,pcgFTYP);

   ilRC = ReadConfigEntry("MAIN","use-for-DACO",pcgHdlDACO);
   dbg(TRACE,"Use for DACO = <%s>",pcgHdlDACO);
   strcpy(pclTmpBuf,"NO");
   ilRC = ReadConfigEntry("MAIN","GENERATE_STEV_FROM_COUNTER",pclTmpBuf);
   if (strcmp(pclTmpBuf,"YES") == 0)
      igGenerateStevFromCounter = TRUE;
   else
      igGenerateStevFromCounter = FALSE;
   dbg(TRACE,"Generate STEV from 1st Char of Counter = <%s><%d>",pclTmpBuf,igGenerateStevFromCounter);

   ilRC = ReadConfigEntry("MAIN","DisplayInT1AndT2",pcgDisplayInT1AndT2);
   dbg(TRACE,"Display in T1 and T2 = <%s>",pcgDisplayInT1AndT2);
   ilRC = ReadConfigEntry("MAIN","ShowAllFlightsInT2",pcgShowAllFlightsInT2);
   dbg(TRACE,"Show all Flights in T2 = <%s>",pcgShowAllFlightsInT2);
   ilRC = ReadConfigEntry("MAIN","CheckCounterOnly",pcgCheckCounterOnly);
   dbg(TRACE,"Check Counter Only = <%s>",pcgCheckCounterOnly);
   ilRC = ReadConfigEntry("MAIN","FixCounterText",pcgFixCnt);
   dbg(TRACE,"Fix Counter Text = <%s>",pcgFixCnt);
   ilRC = ReadConfigEntry("MAIN","UpdateCheckinFromTo",pcgUpdateCheckinFromTo);
   dbg(TRACE,"Update Checkin From/To Fields in AFT = <%s>",pcgUpdateCheckinFromTo);
   ilRC = ReadConfigEntry("MAIN","SIMPLE_HANDLING_OF_MIXED_FLIGHTS",pcgSimpleHandlingOfMixedFlights);
   dbg(TRACE,"Use Simple Handling of Mixed Flights = <%s>",pcgSimpleHandlingOfMixedFlights);
   strcpy(pcgIntFLTI,"I");
   strcpy(pcgDomFLTI,"D");
   strcpy(pcgMixedFLTI,"M");
   ilRC = ReadConfigEntry("MAIN","int-FLTI",pcgIntFLTI);
   ilRC = ReadConfigEntry("MAIN","dom-FLTI",pcgDomFLTI);
   ilRC = ReadConfigEntry("MAIN","mixed-FLTI",pcgMixedFLTI);
   dbg(TRACE,"Valid FLTI = <%s><%s><%s>",pcgIntFLTI,pcgDomFLTI,pcgMixedFLTI);
   strcpy(pcgIntTerm,"1");
   strcpy(pcgDomTerm,"2");
   ilRC = ReadConfigEntry("MAIN","int-Terminal",pcgIntTerm);
   ilRC = ReadConfigEntry("MAIN","dom-Terminal",pcgDomTerm);
   dbg(TRACE,"Valid Terminal = <%s><%s>",pcgIntTerm,pcgDomTerm);

   ilRC = ReadConfigEntry("MAIN","USE_NATURE_OR_SERVICE_TYPE",pcgNatureOrServiceType);
   dbg(TRACE,"Use Nature Code or Service Type = <%s>",pcgNatureOrServiceType);
   if (strcmp(pcgNatureOrServiceType,"STYP") == 0)
      strcpy(AFT_FIELDS,AFT_FIELDS_STYP);
   else
      strcpy(AFT_FIELDS,AFT_FIELDS_TTYP);
   ilRC = ReadConfigEntry("MAIN","UseGateStatusFields",pcgUseGateStatusFields);
   dbg(TRACE,"Use Gate Status Fields = <%s>",pcgUseGateStatusFields);
   if (strcmp(pcgUseGateStatusFields,"YES") == 0)
      strcat(AFT_FIELDS,",RGD1,RGD2");

   ilRC = ReadConfigEntry("MAIN","FDD_WITH_BOARDING_TIME",pcgFddWithBoardingTime);
   dbg(TRACE,"Use FDD with Boarding Time = <%s>",pcgFddWithBoardingTime);
   if (strcmp(pcgFddWithBoardingTime,"YES") == 0)
   {
      strcpy(FDD_FIELDS,FDD_FIELDS_BOTI);
      strcpy(FDD_FIELDS_TAB,FDD_FIELDS_TAB_BOTI);
   }
   else
   {
      strcpy(FDD_FIELDS,FDD_FIELDS_STD);
      strcpy(FDD_FIELDS_TAB,FDD_FIELDS_TAB_STD);
   }
   ilRC = ReadConfigEntry("MAIN","FDD_CNT_CHECK",pcgCfgBuffer);
   if (ilRC == RC_SUCCESS)
      igFddCntCheck = atoi(pcgCfgBuffer);
   dbg(TRACE,"Checking Counters for Terminal Decision is %d",igFddCntCheck);
   ilRC = ReadConfigEntry("MAIN","FDD_CNT_END",pcgCfgBuffer);
   if (ilRC == RC_SUCCESS)
      igFddCntEnd = atoi(pcgCfgBuffer);
   dbg(TRACE,"Time Window for Checking Counters is %d Hours before Scheduled Open Time",igFddCntEnd);
   ilRC = ReadConfigEntry("MAIN","CHECK_TERM_FOR_CNT_ASGN",pcgCheckTermForCntAsgn);
   dbg(TRACE,"Check Terminal for Counter Assignment = <%s>",pcgCheckTermForCntAsgn);
   ilRC = ReadConfigEntry("MAIN","COUNTERS_WITH_ACTUAL_TIME_ONLY",pcgCountersWithActualTimeOnly);
   dbg(TRACE,"Use Counters with Actual Time only = <%s>",pcgCountersWithActualTimeOnly);
   ilRC = ReadConfigEntry("MAIN","COUNTERS_PRE_DISPLAY",pcgCountersPreDisplay);
   dbg(TRACE,"Counters Display before Scheduled Open Time = <%s>",pcgCountersPreDisplay);
   ilRC = ReadConfigEntry("MAIN","SPECIAL_COMMON_COUNTERS_FOR_LIS",pcgSpecialCommonCountersForLis);
   dbg(TRACE,"Use Special Common Counters for Lisbon = <%s>",pcgSpecialCommonCountersForLis);
   if (strcmp(pcgSpecialCommonCountersForLis,"YES") == 0)
   {
      ilRC = ReadConfigEntry("MAIN","LIST_OF_SPECIAL_REMARKS",pcgListOfSpecialRemarks);
      dbg(TRACE,"List of Special Remarks = <%s>",pcgListOfSpecialRemarks);
      sprintf(pclSqlBuf,"SELECT CODE,BEME FROM FIDTAB WHERE CODE IN (%s)",pcgListOfSpecialRemarks);
      slCursor = 0;
      slFkt = START;
      dbg(TRACE,"Special Destinations: <%s>",pclSqlBuf);
      ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
      while (ilRCdb == DB_SUCCESS)
      {
         BuildItemBuffer(pclDataBuf,"",2,",");
         get_real_item(pclResult,pclDataBuf,1);
         strcat(pcgSpecialDests,pclResult);
         strcat(pcgSpecialDests,",");
         get_real_item(pclResult,pclDataBuf,2);
         strcat(pcgSpecialDests,pclResult);
         strcat(pcgSpecialDests,"\n");
         slFkt = NEXT;
         ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
      }
      close_my_cursor(&slCursor);
      dbg(TRACE,"Special Destinations: <\n%s>",pcgSpecialDests);
   }

   ilRC = ReadConfigEntry("MAIN","HANDLE_GATE2",pcgHandleGate2);
   dbg(TRACE,"Handle Gate 2 = <%s>",pcgHandleGate2);

   ilRC = ReadConfigEntry("MAIN","SET_GD1X_WITH_BOA",pcgSetGd1xWithBoa);
   dbg(TRACE,"Set GD1X with Boarding Time = <%s>",pcgSetGd1xWithBoa);

   ilRC = ReadConfigEntry("MAIN","SET_BOARDING_REMARK_WITH_GD1X",pcgSetBoardingRemarkWithGd1x);
   dbg(TRACE,"Set Boarding Remark with GD1X = <%s>",pcgSetBoardingRemarkWithGd1x);

   ilRC = ReadConfigEntry("MAIN","SEND_BC_FOR_FDD",pcgSendBcForFdd);
   dbg(TRACE,"Set Broadcast for FDDTAB = <%s>",pcgSendBcForFdd);
   ilRC = ReadConfigEntry("MAIN","SEND_BC_FOR_FLD",pcgSendBcForFld);
   dbg(TRACE,"Set Broadcast for FLDTAB = <%s>",pcgSendBcForFld);

   ilRC = ReadConfigEntry("MAIN","HANDLE_CHUTES",pcgHandleChutes);
   dbg(TRACE,"Handle Chutes = <%s>",pcgHandleChutes);
   ilRC = ReadConfigEntry("MAIN","CHECK_FTYP_FOR_CHUTE_DISPLAYS",pcgCheckFtypForChuteDisplays);
   dbg(TRACE,"Check FTYP for Chute Displays = <%s>",pcgCheckFtypForChuteDisplays);
   ilRC = ReadConfigEntry("MAIN","LIST_OF_STEV",pcgListOfStev);
   dbg(TRACE,"LIST OF STEV = <%s>",pcgListOfStev);
   ilRC = ReadConfigEntry("MAIN","CORRECT_PLANBAG",pcgCorrectPlanBag);
   dbg(TRACE,"Correct Dummy Chute Records from PlanBag = <%s>",pcgCorrectPlanBag);
   ilRC = ReadConfigEntry("MAIN","USE_FLZTAB",pcgUseFLZTAB);
   dbg(TRACE,"Use FLZTAB = <%s>",pcgUseFLZTAB);

   ilRC = ReadConfigEntry("MAIN","LIST_OF_FTYP_FOR_CCA",pcgListOfFtypForCca);
   dbg(TRACE,"LIST OF VALID FTYP FOR COUNTERS = <%s>",pcgListOfFtypForCca);

   ilRC = ReadConfigEntry("MAIN","CLOSE_COUNTER",pcgCloseCounter);
   dbg(TRACE,"Close Counter with OffBlock/Airborne Time = <%s>",pcgCloseCounter);
   ilRC = ReadConfigEntry("MAIN","CLOSE_GATE",pcgCloseGate);
   dbg(TRACE,"Close Gate with OffBlock/Airborne Time = <%s>",pcgCloseGate);

   strcpy(pcgRtypBELT,"BELT");
   strcpy(pcgRtypCOUNTER,"CHECK");
   strcpy(pcgRtypGATE,"GATE");
   strcpy(pcgRtypCHUTE,"CHUTE");
   strcpy(pcgRtypWRO,"WRO");
   ilRC = ReadConfigEntry("MAIN","FLD-TYPE-BELT",pcgRtypBELT);
   ilRC = ReadConfigEntry("MAIN","FLD-TYPE-COUNTER",pcgRtypCOUNTER);
   ilRC = ReadConfigEntry("MAIN","FLD-TYPE-GATE",pcgRtypGATE);
   ilRC = ReadConfigEntry("MAIN","FLD-TYPE-CHUTE",pcgRtypCHUTE);
   ilRC = ReadConfigEntry("MAIN","FLD-TYPE-WAITINGROOM",pcgRtypWRO);
   sprintf(pcgMyRtypes,"'%s','%s','%s','%s','%s'",
           pcgRtypBELT,pcgRtypCOUNTER,pcgRtypGATE,pcgRtypCHUTE,pcgRtypWRO);
   dbg(TRACE,"Belt RTYP = <%s>",pcgRtypBELT);
   dbg(TRACE,"Counter RTYP = <%s>",pcgRtypCOUNTER);
   dbg(TRACE,"Gate RTYP = <%s>",pcgRtypGATE);
   dbg(TRACE,"Chute RTYP = <%s>",pcgRtypCHUTE);
   dbg(TRACE,"Waiting Room RTYP = <%s>",pcgRtypWRO);

   /*Now Get Remark-codes*/
   ilRC = ReadConfigEntryRemark("REMARK","LANDED",pcgLanded); 
   TrimRight(pcgLanded);
   ilRC = ReadConfigEntryRemark("REMARK","ARRIVED",pcgArrived); 
   TrimRight(pcgArrived);
   ilRC = ReadConfigEntryRemark("REMARK","DELAYED",pcgDelayed); 
   TrimRight(pcgDelayed);
   ilRC = ReadConfigEntryRemark("REMARK","DELAY",pcgDelay); 
   TrimRight(pcgDelay);
   ilRC = ReadConfigEntryRemark("REMARK","EXPECTED",pcgExpected); 
   TrimRight(pcgExpected);
   ilRC = ReadConfigEntryRemark("REMARK","DEPARTED",pcgDeparted); 
   TrimRight(pcgDeparted);
   ilRC = ReadConfigEntryRemark("REMARK","BOARDING",pcgBoarding); 
   TrimRight(pcgBoarding);
   ilRC = ReadConfigEntryRemark("REMARK","GATEOPENED",pcgGateOpened); 
   TrimRight(pcgGateOpened);
   ilRC = ReadConfigEntryRemark("REMARK","CANCELED",pcgCanceled); 
   TrimRight(pcgCanceled);
   ilRC = ReadConfigEntryRemark("REMARK","GATECLOSED",pcgGateclosed); 
   TrimRight(pcgGateclosed);
   ilRC = ReadConfigEntryRemark("REMARK","DELAYEDGATEOPEN",pcgDelayedGateOpen); 
   TrimRight(pcgDelayedGateOpen);
   ilRC = ReadConfigEntryRemark("REMARK","AIRBORNE",pcgAirborne); 
   TrimRight(pcgAirborne);
   ilRC = ReadConfigEntryRemark("REMARK","FINALCALL",pcgFinalcall); 
   TrimRight(pcgFinalcall);
   ilRC = ReadConfigEntryRemark("REMARK","DIVERTED",pcgDiverted); 
   TrimRight(pcgDiverted);
   ilRC = ReadConfigEntryRemark("REMARK","REROUTED",pcgRerouted); 
   TrimRight(pcgRerouted);
   ilRC = ReadConfigEntryRemark("REMARK","NEXTINFO",pcgNextinfo); 
   TrimRight(pcgNextinfo);
   ilRC = ReadConfigEntryRemark("REMARK","NEWGATE",pcgNewGate); 
   TrimRight(pcgNewGate);
   ilRC = ReadConfigEntryRemark("REMARK","FIRSTBAG",pcgFirstBag); 
   TrimRight(pcgFirstBag);
   ilRC = ReadConfigEntryRemark("REMARK","LASTBAG",pcgLastBag); 
   TrimRight(pcgLastBag);
   ilRC = ReadConfigEntry("REMARK","AUTONEXTINFO",pcgCfgBuffer);
   if (ilRC == RC_SUCCESS)
   {
      if (strcmp(pcgCfgBuffer,"NO") == 0)
      {
         igAutoNextInfo = FALSE;
      }
   }
   ilRC = ReadConfigEntry("REMARK","DELAYTIMEDIFFARR",pcgCfgBuffer);
   igDelayTimeDiffArr = atoi(pcgCfgBuffer);
   igDelayTimeDiffArr *= 60;
   ilRC = ReadConfigEntry("REMARK","DELAYTIMEDIFFDEP",pcgCfgBuffer);
   igDelayTimeDiffDep = atoi(pcgCfgBuffer);
   igDelayTimeDiffDep *= 60;
   ilRC = ReadConfigEntryRemark("REMARK","LISTOFETAREM",pcgListOfETARem); 
   TrimRight(pcgListOfETARem);
   ilRC = ReadConfigEntryRemark("REMARK","LISTOFETDREM",pcgListOfETDRem); 
   TrimRight(pcgListOfETDRem);

   strcpy(pcgDispBelt,"ONBL");
   ilRC = ReadConfigEntry("MAIN","display_belt",pcgCfgBuffer); 
   if (ilRC == RC_SUCCESS)
   {
      strcpy(pcgDispBelt,pcgCfgBuffer);
   }
   TrimRight(pcgDispBelt);
   strcpy(pcgDispGate,"NORMAL");
   ilRC = ReadConfigEntry("MAIN","display_gate",pcgCfgBuffer); 
   if (ilRC == RC_SUCCESS)
   {
      strcpy(pcgDispGate,pcgCfgBuffer);
   }
   TrimRight(pcgDispGate);

   return ilRC;
} /*end GetConfig()*/

/******************************************************************************/
/* FillCntDisp                                                                */
/* fills the CNTDISP structs with the corresponding section-data from cfg-file*/
/******************************************************************************/

static int FillCntDisp(char *charbuff)
{
  int ilRC = RC_SUCCESS;
  static char  pcgCfgBuffer[1024];


  strcpy(prguCntDisp->Section, charbuff);

  ilRC = ReadConfigEntry(charbuff,"COUNTERS",pcgCfgBuffer);
  strcpy(prguCntDisp->COUNTERS, pcgCfgBuffer);
  /*dbg(DEBUG,"%05d: Counters:charbuff: <%s>,",__LINE__,charbuff);*/
  ilRC = ReadConfigEntry(charbuff,"LOGO",pcgCfgBuffer);
  strcpy(prguCntDisp->LOGO, pcgCfgBuffer);

  ilRC = ReadConfigEntry(charbuff,"FLINUM",pcgCfgBuffer);
  strcpy(prguCntDisp->FLINUM,pcgCfgBuffer);

  ilRC = ReadConfigEntry(charbuff,"PUST",pcgCfgBuffer);
  strcpy(prguCntDisp->PUST, pcgCfgBuffer);
   
/* 
  dbg(TRACE,"section: %s",prguCntDisp->Section);
  dbg(TRACE,"COUNTERS: %s",prguCntDisp->COUNTERS);
  dbg(TRACE,"LOGO: %s",prguCntDisp->LOGO);
  dbg(TRACE,"FLINUM: %s",prguCntDisp->FLINUM);
  dbg(TRACE,"PUST: %s",prguCntDisp->PUST);
*/
    
  return ilRC;

} /*end of FillCntDisp*/

/******************************************************************************/
/* FillDoMix                                                                */
/* fills the DoMix structs with the corresponding section-data from cfg-file*/
/******************************************************************************/

static int FillDoMix(char *charbuff)
{
  int ilRC = RC_SUCCESS;
  static char  pcgCfgBuffer[1024];


  strcpy(prguDoMix->Section, charbuff);

  ilRC = ReadConfigEntry(charbuff,"DOMVIA",pcgCfgBuffer);
  strcpy(prguDoMix->DOMVIA, pcgCfgBuffer);
 
  ilRC = ReadConfigEntry(charbuff,"ITIP",pcgCfgBuffer);
  strcpy(prguDoMix->ITIP, pcgCfgBuffer);

  ilRC = ReadConfigEntry(charbuff,"ITDP",pcgCfgBuffer);
  strcpy(prguDoMix->ITDP,pcgCfgBuffer);

  ilRC = ReadConfigEntry(charbuff,"DTIP",pcgCfgBuffer);
  strcpy(prguDoMix->DTIP, pcgCfgBuffer);
 
  ilRC = ReadConfigEntry(charbuff,"DTDP",pcgCfgBuffer);
  strcpy(prguDoMix->DTDP, pcgCfgBuffer);

  /*
  dbg(TRACE,"section: %s",prguDoMix->Section);
  dbg(TRACE,"DOMVIA: %s",prguDoMix->DOMVIA);
  dbg(TRACE,"ITIP: %s",prguDoMix->ITIP);
  dbg(TRACE,"ITDP: %s",prguDoMix->ITDP);
  dbg(TRACE,"DTIP: %s",prguDoMix->DTIP);
  dbg(TRACE,"DTDP: %s",prguDoMix->DTDP);
  */


  return ilRC;

} /*end of FillDoMix*/

/************************************************************************************/
/* FillComCodeShare                                                                 */
/* fills the COMCODESHARE structs with the corresponding section-data from cfg-file */
/************************************************************************************/

static int FillComCodeShare(char *charbuff)
{
  int ilRC = RC_SUCCESS;
  static char  pcgCfgBuffer[1024];
  char clchar;
  int ilI, ilJ;

  prguComCodeShare->MainAirline[0] = '\0';
  prguComCodeShare->ilCntCS = 0;
  ilRC = ReadConfigEntry(charbuff,"MAIN",pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(prguComCodeShare->MainAirline,pcgCfgBuffer);
     ilRC = ReadConfigEntry(charbuff,"CODE-SHARE",pcgCfgBuffer);
     if (ilRC == RC_SUCCESS && strlen(pcgCfgBuffer) > 0)
     {
        ilI = 0;
        ilJ = 0;
        clchar = '0';
        while (clchar != '\0')
        {
           clchar = pcgCfgBuffer[ilI];
           ilI++;
           if (clchar == ',')
           {
              prguComCodeShare->ilCntCS++;
              ilJ = 0;
           }
           else
           {
              prguComCodeShare->CodeShare[prguComCodeShare->ilCntCS][ilJ] = clchar;
              prguComCodeShare->CodeShare[prguComCodeShare->ilCntCS][ilJ+1] = '\0';
              ilJ++;
           }
        }
        prguComCodeShare->ilCntCS++;
     }
     else
     {
        igSec4End--;
        ilRC = -1;
     }
  }
  else
  {
     igSec4End--;
     ilRC = -1;
  }

  dbg(DEBUG,"FillComCodeShare: No Code Shares for <%s> = %d",
      prguComCodeShare->MainAirline,prguComCodeShare->ilCntCS);
  for (ilI=0; ilI<prguComCodeShare->ilCntCS; ilI++)
  {
     dbg(DEBUG,"FillComCodeShare: Code Share <%s>",&prguComCodeShare->CodeShare[ilI][0]);
  }

  return ilRC;

} /*end of FillComCodeShare*/

/******************************************************************************/
/* The GetTimeOffsets  routine                                                    */
/******************************************************************************/
static int GetTimeOffsets()
{
  char clCfgBuffer[1024];

  /*Here the calculation of the offsets (from tiptab)is made and the resultig    */
  /*CEDA-Time is stored in the corresponding Variables                           */
  /*The functions first parameter is a Buffer to store the functions result      */
  /*the second parameter is the TIPC from iptab                                  */
  /*the third parameter is the offsets sign (1 or -1) to specify if the offset is*/
  /*to be added or subtracted                                                    */
  /*this was neccessary becaus in tiptab there are only unsignd values and       */
  /*there is no chance to read the sign form tiptab                              */
  int ilRC = RC_SUCCESS;


  ilRC = ReadConfigEntry("MAIN","section3",clCfgBuffer);
  
  if((ilRC == RC_SUCCESS) && (strstr(clCfgBuffer,"timeframe") != NULL))
    {
      ilRC = ReadConfigEntry("timeframe","TIMEPARAMETER_01",clCfgBuffer);
      ilRC = GetCfgOffsetTime(clCfgBuffer,&timeoffsets.lgTipc01);
      ilRC = ReadConfigEntry("timeframe","TIMEPARAMETER_02",clCfgBuffer);
      ilRC = GetCfgOffsetTime(clCfgBuffer,&timeoffsets.lgTipc02);
      ilRC = ReadConfigEntry("timeframe","TIMEPARAMETER_03",clCfgBuffer);
      ilRC = GetCfgOffsetTime(clCfgBuffer,&timeoffsets.lgTipc03);
      ilRC = ReadConfigEntry("timeframe","TIMEPARAMETER_04",clCfgBuffer);
      ilRC = GetCfgOffsetTime(clCfgBuffer,&timeoffsets.lgTipc04);
      ilRC = ReadConfigEntry("timeframe","TIMEPARAMETER_05",clCfgBuffer);
      ilRC = GetCfgOffsetTime(clCfgBuffer,&timeoffsets.lgTipc05);
      ilRC = ReadConfigEntry("timeframe","TIMEPARAMETER_06",clCfgBuffer);
      ilRC = GetCfgOffsetTime(clCfgBuffer,&timeoffsets.lgTipc06);
      ilRC = ReadConfigEntry("timeframe","TIMEPARAMETER_07",clCfgBuffer);
      ilRC = GetCfgOffsetTime(clCfgBuffer,&timeoffsets.lgTipc07);
      ilRC = ReadConfigEntry("timeframe","TIMEPARAMETER_08",clCfgBuffer);
      ilRC = GetCfgOffsetTime(clCfgBuffer,&timeoffsets.lgTipc08);
      ilRC = ReadConfigEntry("timeframe","TIMEPARAMETER_09",clCfgBuffer);
      ilRC = GetCfgOffsetTime(clCfgBuffer,&timeoffsets.lgTipc09);
      ilRC = ReadConfigEntry("timeframe","TIMEPARAMETER_10",clCfgBuffer);
      ilRC = GetCfgOffsetTime(clCfgBuffer,&timeoffsets.lgTipc10);
      ilRC = ReadConfigEntry("timeframe","TIMEPARAMETER_11",clCfgBuffer);
      ilRC = GetCfgOffsetTime(clCfgBuffer,&timeoffsets.lgTipc11);
      ilRC = ReadConfigEntry("timeframe","TIMEPARAMETER_12",clCfgBuffer);
      ilRC = GetCfgOffsetTime(clCfgBuffer,&timeoffsets.lgTipc12);
      ilRC = ReadConfigEntry("timeframe","TIMEPARAMETER_13",clCfgBuffer);
      ilRC = GetCfgOffsetTime(clCfgBuffer,&timeoffsets.lgTipc13);
      ilRC = ReadConfigEntry("timeframe","TIMEPARAMETER_14",clCfgBuffer);
      ilRC = GetCfgOffsetTime(clCfgBuffer,&timeoffsets.lgTipc14);
    
    }
  else
    {
      /*timeframe  from tiptab or cfgfile*/
      ilRC = Calculateoffsets(&timeoffsets.lgTipc01, "01",-1);
      ilRC = Calculateoffsets(&timeoffsets.lgTipc02, "02",-1);
      ilRC = Calculateoffsets(&timeoffsets.lgTipc03, "03",1);
      ilRC = Calculateoffsets(&timeoffsets.lgTipc04, "04",1);
      ilRC = Calculateoffsets(&timeoffsets.lgTipc05, "05",1);
      ilRC = Calculateoffsets(&timeoffsets.lgTipc06, "06",1);
      ilRC = Calculateoffsets(&timeoffsets.lgTipc07, "07",-1);
      ilRC = Calculateoffsets(&timeoffsets.lgTipc08, "08",1);
      ilRC = Calculateoffsets(&timeoffsets.lgTipc09, "09",-1);
      ilRC = Calculateoffsets(&timeoffsets.lgTipc10, "10",1);
      ilRC = Calculateoffsets(&timeoffsets.lgTipc11, "11",1);
      ilRC = Calculateoffsets(&timeoffsets.lgTipc12, "12",1);
      ilRC = Calculateoffsets(&timeoffsets.lgTipc13, "13",1);
      ilRC = Calculateoffsets(&timeoffsets.lgTipc14, "14",1);
    }
 
  ilRC = ShowTimeOffsets(&timeoffsets);

  return (ilRC);
} /* end of GetTimeOffsets */

/******************************************************************************/
/* The GetCfgOffsetTime  routine                                              */
/******************************************************************************/
static int GetCfgOffsetTime(char *pclBuffer,long *plloffset)
{
  int  ilRC = RC_SUCCESS;
  char clCharBuff[1024];
  long llValue = 0;
  char clUnit = '0';
  char clCfgBuffer[1024];

  strcpy(clCfgBuffer,pclBuffer);
  
  get_real_item(clCharBuff,clCfgBuffer,1);
  llValue = atol(clCharBuff);
  get_real_item(clCharBuff,clCfgBuffer,2);
  clUnit = clCharBuff[0];
  dbg(DEBUG,"%05d: llValue: %d, clUnit <%c>",__LINE__,llValue,clUnit);
     
  if (clUnit == 'H')
    {
      llValue *= 60;   /*Hours to Minutes*/
    }
  dbg(DEBUG,"%05d: llValue: %d",__LINE__,llValue);
      /*Minutes to Seconds */ 
      /*llValue in the Config-File is negative at this point, so no sign-change needed*/
  llValue *= 60;     
  *plloffset = llValue;
  
  return 0;
} /* end of GetCfgOffsetTime */




/******************************************************************************/
/* The Calculateoffsets  routine                                           */
/******************************************************************************/
static int Calculateoffsets(long *plloffset, char *pclTipc, int sign)
{
  int ilRC = RC_SUCCESS;
  int ilItemCnt = 0;
  long llValue = 0;
  short slCursor;
  short slSqlFunc;
  char clSqlBuf[256];
  char clData[100];
  char clUnit = '0';

                   /****** get Time-Unit (H or M) *********/
  sprintf(clSqlBuf,"SELECT TIPU FROM TIPTAB WHERE TIPC='%s'",pclTipc);
  dbg(DEBUG,"%05d: clSqlBuf: <%s>",__LINE__,clSqlBuf);  
 
  slSqlFunc= START;
  slCursor = 0;
  ilRC = sql_if(slSqlFunc,&slCursor,clSqlBuf,clData);
  if (ilRC == RC_FAIL) 
    {
      dbg(DEBUG, "%05d:SQL Buffer<%s>",__LINE__, clSqlBuf);
      get_ora_err(-1, cgErrMsg);
      dbg(TRACE, "Oracle Error <%s>", cgErrMsg);
     }                           
  dbg(DEBUG,"%05d: clData: <%s>,ilRC: %d",__LINE__,clData,ilRC);
  clUnit = clData[0];
  dbg(DEBUG,"%05d: clUnit: %c",__LINE__,clUnit);
  close_my_cursor(&slCursor);

  slCursor = 0;
                     /************ get Time-Value ****************/
  sprintf(clSqlBuf,"SELECT TIPV FROM TIPTAB WHERE TIPC='%s'",pclTipc);
  dbg(DEBUG,"%05d: clSqlBuf: <%s>",__LINE__,clSqlBuf);  

  ilRC = sql_if(slSqlFunc,&slCursor,clSqlBuf,clData);
  if (ilRC == RC_FAIL) 
    {
      dbg(DEBUG, "%05d:SQL Buffer<%s>",__LINE__, clSqlBuf);
      get_ora_err(-1, cgErrMsg);
      dbg(TRACE, "Oracle Error <%s>", cgErrMsg);
     }                           
  dbg(DEBUG,"%05d: clData: <%s>,ilRC: %d",__LINE__,clData,ilRC);
  llValue =atol(clData);
  dbg(DEBUG,"%05d: llValue: %d",__LINE__,llValue);
  close_my_cursor(&slCursor); 

  slCursor = 0;

  if (clUnit == 'H')
    {
      llValue *= 60;   /*Hours to Minutes*/
    }
  dbg(DEBUG,"%05d: llValue: %d",__LINE__,llValue);
         
  llValue *= 60 * sign;      /*Minutes to Seconds and sign of offset */

  *plloffset = llValue;

  dbg(DEBUG,"%05d: plloffset: %d ",__LINE__,*plloffset);
  



  return (ilRC);

}

/******************************************************************************/
/* The SetDefaultTimeOffsets routine                                          */
/******************************************************************************/
static int SetDefaultTimeOffsets(TIMOFFS *prpTimeOffsets)
{
  int ilRC = RC_SUCCESS;
  prpTimeOffsets->lgTipc01   = -36000;           /* Offset  Timeframe Arrival Start default: -10H */
  prpTimeOffsets->lgTipc02   = -7200;            /* Offset  Timeframe Departure Start default: -2H */
  prpTimeOffsets->lgTipc03   =  7200;            /* Offset  Timeframe Arrival End  default: 2H */
  prpTimeOffsets->lgTipc04   =  36000;           /* Offset  Timeframe Departure End   default: 10H */
  prpTimeOffsets->lgTipc05   =  600;             /* Offset  Delete from Belt Display after ONBL (Int) default: 10M*/
  prpTimeOffsets->lgTipc06   =  7200;            /* Offset  Delete from Arrival Display default: 2H */
  prpTimeOffsets->lgTipc07   = -7200;            /* Offset  Delete on Gate Display default: 2H */
  prpTimeOffsets->lgTipc08   =  7200;            /* Offset  Delete from Departure Display default: 2H */
  prpTimeOffsets->lgTipc09   = -3600;            /* Offset  Display cancelled Flights default: -1H */
  prpTimeOffsets->lgTipc10   =  7200;            /* Offset  Delete from Belt Display after Last Bag (Int) default: 2H */
  prpTimeOffsets->lgTipc11   =  7200;            /* Offset  Delete from Gate Display after GCL (Int) default: 2H*/
  prpTimeOffsets->lgTipc12   =  7200;            /* Offset  Delete from Gate Display after GCL (Dom) default: 2H */
  prpTimeOffsets->lgTipc13   =  7200;            /* Offset  Delete from Belt Display after ONBL (Dom) default: 2H */
  prpTimeOffsets->lgTipc14   =  7200;            /* Offset  Delete from Belt Display after Last Bag (Dom) default: 2H */



  return 0;
} /* end of SetDefaultTimeOffsets*/



/******************************************************************************/
/* The ShowTimeOffsets routine                                          */
/******************************************************************************/
static int ShowTimeOffsets(TIMOFFS *prpTimeOffsets)
{
  int ilRC = RC_SUCCESS;

  dbg(TRACE,"prpTimeOffsets->lgTipc01 = %d",prpTimeOffsets->lgTipc01);    /* Offset  Timeframe Arrival Start default: -10H */
  dbg(TRACE,"prpTimeOffsets->lgTipc02 = %d",prpTimeOffsets->lgTipc02);    /* Offset  Timeframe Arrival End  default: 2H */
  dbg(TRACE,"prpTimeOffsets->lgTipc03 = %d",prpTimeOffsets->lgTipc03);    /* Offset  Timeframe Departure Start default: -2H */
  dbg(TRACE,"prpTimeOffsets->lgTipc04 = %d",prpTimeOffsets->lgTipc04);    /* Offset  Timeframe Departure End   default: 10H */
  dbg(TRACE,"prpTimeOffsets->lgTipc05 = %d",prpTimeOffsets->lgTipc05);    /* Offset  Delete from Belt Display after ONBL (Int) default: 10M*/
  dbg(TRACE,"prpTimeOffsets->lgTipc06 = %d",prpTimeOffsets->lgTipc06);    /* Offset  Delete from Arrival Display default: 2H */
  dbg(TRACE,"prpTimeOffsets->lgTipc07 = %d",prpTimeOffsets->lgTipc07);    /* Offset  Delete on Gate Display default: 2H */ 
  dbg(TRACE,"prpTimeOffsets->lgTipc08 = %d",prpTimeOffsets->lgTipc08);    /* Offset  Delete from Departure Display default: 2H */
  dbg(TRACE,"prpTimeOffsets->lgTipc09 = %d",prpTimeOffsets->lgTipc09);    /* Offset  Display cancelled Flights default: -1H */
  dbg(TRACE,"prpTimeOffsets->lgTipc10 = %d",prpTimeOffsets->lgTipc10);    /* Offset  Delete from Belt Display after Last Bag (Int) default: 2H */
  dbg(TRACE,"prpTimeOffsets->lgTipc11 = %d",prpTimeOffsets->lgTipc11);    /* Offset  Delete from Gate Display after GCL (Int) default: 2H*/ 
  dbg(TRACE,"prpTimeOffsets->lgTipc12 = %d",prpTimeOffsets->lgTipc12);    /* Offset  Delete from Gate Display after GCL (Dom) default: 2H */
  dbg(TRACE,"prpTimeOffsets->lgTipc13 = %d",prpTimeOffsets->lgTipc13);    /* Offset  Delete from Belt Display after ONBL (Dom) default: 2H */
  dbg(TRACE,"prpTimeOffsets->lgTipc14 = %d",prpTimeOffsets->lgTipc14);    /* Offset  Delete from Belt Display after Last Bag (Dom) default: 2H */


  return 0;
} /* end of SetDefaultTimeOffsets*/



/******************************************************************************/
/* The OffsettimeFromNow  routine                                           */
/******************************************************************************/
static int OffsettimeFromNow (long *pllOffset, char *pclCEDATime)
{
  int ilRC = RC_SUCCESS;


  TimeToStr(pclCEDATime,(time(NULL)));
  /*dbg(DEBUG,"%05d: after TimeToStr: pclCEDATime: <%s> Offset: <%d>",__LINE__,pclCEDATime,*pllOffset);*/

  AddSecondsToCEDATime12(pclCEDATime,*pllOffset,1);
  /*dbg(DEBUG,"%05d: after AddsecondstoCEDATime: pclCEDATime: <%s>",__LINE__,pclCEDATime);*/
  return (ilRC);
} 


/******************************************************************************/
/* The TimeToStr routine                                                      */
/******************************************************************************/
static int TimeToStr(char *pcpTime,time_t lpTime) 
{                                                                              
    struct tm *_tm;
    char   _tmpc[6];
    
      /*_tm = (struct tm *)localtime(&lpTime); */
    _tm = (struct tm *)gmtime(&lpTime);
    /*      strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",_tm); */
 

    sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
            _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
            _tm->tm_min,_tm->tm_sec);

    return RC_SUCCESS;  
                        
}     /* end of TimeToStr */

/******************************************************************************/
/* The  GetRowLength                                                          */
/******************************************************************************/
static int GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList, long *plpLen)
{
  int  ilRc = RC_SUCCESS;            /* Return code */
  int  ilNoOfItems = 0;
  int  ilLoop = 0;
  long llFldLen = 0;
  long llRowLen = 0;
  char clFina[8];
    
  ilNoOfItems = get_no_of_items(pcpFieldList);

  ilLoop = 1;
  do{
      ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
      if(ilRc > 0)
    {
      ilRc = GetFieldLength(pcpHopo,pcpTana,&clFina[0],&llFldLen);
      if(ilRc == RC_SUCCESS)
        {
          llRowLen++;
          llRowLen += llFldLen;
        }/* end of if */
    }/* end of if */
      ilLoop++;
    }while((ilRc == RC_SUCCESS) && (ilLoop <= ilNoOfItems));

  if(ilRc == RC_SUCCESS)
    {
      *plpLen = llRowLen;
    }/* end of if */

  return(ilRc);
    
} /* end of GetRowLength */


/******************************************************************************/
/* The  GetFieldlength                                                          */
/******************************************************************************/
static int GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina, long *plpLen)
{
    int  ilRc = RC_SUCCESS;            /* Return code */
    int  ilCnt = 0;
    char clTaFi[128];
    char clFele[128];
    char clFldLst[128];

    ilCnt = 1;
    sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
    sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerst�rt */

    /*dbg(TRACE,"GetFieldLength Tana <%s> Fina <%s>",pcpTana,pcpFina);*/

    /*    dbg(TRACE,"%05d:\n \npcgTabEnd[0] <%s>, \n&clFldLst[0]: <%s>, \n&clTaFi[0]: <%s>, \nFELE, \n&clFele[0] <%s>, \n&ilCnt: %d \n",__LINE__, &pcgTabEnd[0],&clFldLst[0],&clTaFi[0],&clFele[0],ilCnt);
     */

    ilRc = syslibSearchSystabData(&pcgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
    

    switch (ilRc)
    {
    case RC_SUCCESS :
        *plpLen = atoi(&clFele[0]);
        /* dbg(DEBUG,"GetFieldLength: <%s,%s,%s> <%d>",pcpHopo,pcpTana,pcpFina,*plpLen);*/
        break;

    case RC_NOT_FOUND :
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s>",ilRc,&clTaFi[0]);
        break;

    case RC_FAIL :
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
        break;

    default :
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
        break;
    }/* end of switch */

    return(ilRc);
    
} /* end of GetFieldLength */



/******************************************************************************/
/* The  SetArrayInfo                                                          */
/******************************************************************************/
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, 
                        char *pcpArrayFieldList,char *pcpAddFields,
                        long *pcpAddFieldLens,ARRAYINFO *prpArrayInfo, 
                        char *pcpSelection,BOOL bpFill)
{
    int    ilRc = RC_SUCCESS;                /* Return code */
    int ilLoop = 0;
    char clOrderTxt[4];

    

    dbg(DEBUG,"%05d: *pcpArrayName: <%s>, *pcpTableName: <%s>, *pcpArrayFieldList: <%s>, *pcpSelection: <%s> ", __LINE__,pcpArrayName,pcpTableName,pcpArrayFieldList, pcpSelection);

    if(prpArrayInfo == NULL)
    {
        dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
        ilRc = RC_FAIL;
    }else
    {
    
        memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

        prpArrayInfo->rrArrayHandle = -1;
        prpArrayInfo->rrIdx01Handle = -1;

        if(pcpArrayName != NULL)
        {
            if(strlen(pcpArrayName) <= ARR_NAME_LEN)
            {
                strcpy(prpArrayInfo->crArrayName,pcpArrayName);
            }/* end of if */
        }/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);
        if (pcpAddFields != NULL)
        {
            prpArrayInfo->lrArrayFieldCnt += get_no_of_items(pcpAddFields);
        }

        prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
        if(prpArrayInfo->plrArrayFieldOfs == NULL)
        {
            dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
            ilRc = RC_FAIL;
        }/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
        if(prpArrayInfo->plrArrayFieldLen == NULL)
        {
            dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
            ilRc = RC_FAIL;
        }else{
            *(prpArrayInfo->plrArrayFieldLen) = -1;
        }/* end of if */
    }/* end of if */

       if(ilRc == RC_SUCCESS)
       {
           ilRc = GetRowLength(&cgHopo[0],pcpTableName,pcpArrayFieldList,&(prpArrayInfo->lrArrayRowLen));
           if(ilRc != RC_SUCCESS)
           {
               dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
          }/* end of if */
       }/* end of if */

       if(ilRc == RC_SUCCESS)
       {
        prpArrayInfo->lrArrayRowLen+=100;
        prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+10));
           if(prpArrayInfo->pcrArrayRowBuf == NULL)
           {
                  ilRc = RC_FAIL;
                  dbg(TRACE,"SetArrayInfo: calloc failed");
           }/* end of if */
       }/* end of if */

       if(ilRc == RC_SUCCESS)
       {
        prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx01RowLen+1));
           if(prpArrayInfo->pcrIdx01RowBuf == NULL)
           {
                  ilRc = RC_FAIL;
                  dbg(TRACE,"SetArrayInfo: calloc failed");
           }/* end of if */
       }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        dbg(TRACE,"CEDAArrayCreate follows Array <%s> Table <%s>",pcpArrayName,pcpTableName);
        ilRc = CEDAArrayCreate(&(prpArrayInfo->rrArrayHandle),
                pcpArrayName,pcpTableName,pcpSelection,pcpAddFields,
                pcpAddFieldLens,pcpArrayFieldList,
                &(prpArrayInfo->plrArrayFieldLen[0]),
                &(prpArrayInfo->plrArrayFieldOfs[0]));

        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"SetArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
        }/* end of if */
    }/* end of if */

        if(pcpArrayFieldList != NULL)
        {
            if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
            {
                strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
            }/* end of if */
        }/* end of if */
        if(pcpAddFields != NULL)
        {
            if(strlen(pcpAddFields) + 
                strlen(prpArrayInfo->crArrayFieldList) <= (size_t)ARR_FLDLST_LEN)
            {
                strcat(prpArrayInfo->crArrayFieldList,",");
                strcat(prpArrayInfo->crArrayFieldList,pcpAddFields);
                dbg(TRACE,"<%s>",prpArrayInfo->crArrayFieldList);
            }/* end of if */
        }/* end of if */


    if(ilRc == RC_SUCCESS && bpFill)
    {
        ilRc = CEDAArrayFill(&(prpArrayInfo->rrArrayHandle),pcpArrayName,NULL);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"SetArrayInfo: CEDAArrayFill failed <%d>",ilRc);
        }/* end of if */
    }/* end of if */

    return(ilRc);
} /*end SetArrayInfo */


/******************************************************************************/
/* The WaitAndCheckQueueutine                                                 */
/******************************************************************************/
static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem)
{
    int ilRc          = RC_SUCCESS;                      /* Return code */
    int ilWaitCounter = 0;

    do
    {
        sleep(1);
        ilWaitCounter += 1;

        ilRc = CheckQueue(ipModId,prpItem);
        if(ilRc != RC_SUCCESS)
        {
            if(ilRc != QUE_E_NOMSG)
            {
                dbg(TRACE,"WaitAndCheckQueue: CheckQueue <%d> failed <%d>",mod_id,ilRc);
            }/* end of if */
        }/* end of if */
    }while((ilRc == QUE_E_NOMSG) && (ilWaitCounter < ipTimeout));

    if(ilWaitCounter >= ipTimeout)
    {
        dbg(TRACE,"WaitAndCheckQueue: timeout reached <%d>",ipTimeout);
        ilRc = RC_FAIL;
    }/* end of if */

    return ilRc;
} /*end WaitAndCheckQueue*/



/******************************************************************************/
/* The  TriggerAction routine                                                 */
/******************************************************************************/
static int TriggerAction(char *pcpTableName)
{
  int           ilRc          = RC_SUCCESS ;    /* Return code */
  EVENT        *prlEvent      = NULL ;
  BC_HEAD      *prlBchead     = NULL ;
  CMDBLK       *prlCmdblk     = NULL ;
  ACTIONConfig *prlAction     = NULL ;
  char         *pclSelection  = NULL ;
  char         *pclFields     = NULL ;
  char         *pclData       = NULL ;
  long          llActSize     = 0 ;
  int           ilAnswerQueue = 0 ;
  char          clQueueName[16] ;

  
  /* 20020709: JIM: JHI hat gesagt, dass das dynamische alles Pfusch ist, daher 
     drehen wir hier sofort wieder um
  */    
  if (strcmp(pcgDynamicActionConfig,"YES") != 0)
  {
     dbg(TRACE,"TriggerAction: No dynamic action configuration defined");
     return (ilRc);
  }

  if (ilRc == RC_SUCCESS)
  {
   sprintf (&clQueueName[0],"%s2", mod_name) ;

   ilRc = GetDynamicQueue(&ilAnswerQueue,&clQueueName[0]);
   if (ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: GetDynamicQueue failed <%d>",ilRc);
   }
   else
   {
    dbg(DEBUG,"TriggerAction: got <%d> for <%s>",ilAnswerQueue, &clQueueName[0]);
   }/* end of if */
  }/* end of if */
    
  if (ilRc == RC_SUCCESS)
  {
   llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + sizeof(ACTIONConfig) + 666;
   prgOutEvent = realloc(prgOutEvent,llActSize);
   if(prgOutEvent == NULL)
   {
    dbg(TRACE,"TriggerAction: realloc out event <%d> bytes failed",llActSize);
    ilRc = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if(ilRc == RC_SUCCESS)
  {
   prlBchead     = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
   prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data) ;
   pclSelection  = prlCmdblk->data ;
   pclFields     = pclSelection + strlen (pclSelection) + 1 ;
   pclData       = pclFields + strlen (pclFields) + 1 ;
   strcpy (pclData, "DYN") ;
   prlAction     = (ACTIONConfig *) (pclData + strlen(pclData) + 1) ;
    
    prlBchead->rc = RC_SUCCESS;
    
   prgOutEvent->type        = USR_EVENT ;
   prgOutEvent->command     = EVENT_DATA ;
   prgOutEvent->originator  = ilAnswerQueue ;
   prgOutEvent->retry_count = 0 ;
   prgOutEvent->data_offset = sizeof(EVENT) ;
   prgOutEvent->data_length = llActSize-sizeof(EVENT) ;

   prlAction->iEmptyFieldHandling = iDELETE_ALL_BLANKS ;
   prlAction->iIgnoreEmptyFields = 0 ;
   strcpy(prlAction->pcSndCmd, "") ;
   sprintf(prlAction->pcSectionName,"%s_%s", mod_name,pcpTableName) ;
   strcpy(prlAction->pcTableName, pcpTableName) ;
   strcpy(prlAction->pcFields, "-") ;
   /******* HEB set data for different Tables *********/
   strcpy(prlAction->pcFields, AFT_FIELDS) ;
   strcpy(prlAction->pcSectionCommands, "URT,IRT,UFR,IFR,ISF,RT,RTA,RTB,DRT,DRF,UCD,DCD,IBT,WRD,GNU,GMU,UBT,DBT,GDF,SPR,DFR,CGM,DGM,JOF") ;
    
     if (!strcmp(pcpTableName,"CCATAB"))
       {
         strcpy(prlAction->pcFields, CCA_FIELDS) ;

       }
     if (!strcmp(pcpTableName,"ALTTAB"))
       {
         strcpy(prlAction->pcFields, ALT_FIELDS) ;

       }
     if (!strcmp(pcpTableName,"FLDTAB"))
       {
         strcpy(prlAction->pcFields, FLD_FIELDS) ;

       }
     /************/

   prlAction->iModID = mod_id ;

   /****************************************
     DebugPrintEvent(DEBUG,prgOutEvent) ;
     DebugPrintBchead(DEBUG,prlBchead) ;
     DebugPrintCmdblk(DEBUG,prlCmdblk) ;
     dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
     dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
     dbg (DEBUG,"TriggerAction: data      <%s>",pclData) ;
     DebugPrintACTIONConfig(DEBUG,prlAction) ;
   ****************************************/

   if(ilRc == RC_SUCCESS)
   {
    prlAction->iADFlag = iDELETE_SECTION ;

    ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize, (char *) prgOutEvent) ; 
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
    }
    else
    {
     ilRc = WaitAndCheckQueue(10, ilAnswerQueue,&prgItem) ;
     if(ilRc != RC_SUCCESS)
     {
      dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
     }
     else
     {
      /* return value for delete does not matter !!! */
        
      /****************************************
      prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
      prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
      prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
      pclSelection = (char *)    prlCmdblk->data ;
      pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1;
      pclData      = (char *)    pclFields + strlen(pclFields) + 1;
      DebugPrintItem(DEBUG, prgItem) ;
      DebugPrintEvent(DEBUG, prlEvent) ;
      DebugPrintBchead(DEBUG, prlBchead) ;
      DebugPrintCmdblk(DEBUG, prlCmdblk) ;
      dbg(DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg(DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ****************************************/
    } /* end of if */
   }/* end of if */
  }/* end of if */

  if(ilRc == RC_SUCCESS)
  {
   prlAction->iADFlag = iADD_SECTION ;

   ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize,
                                              (char *) prgOutEvent ) ;
   if(ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
   }
   else
   {
    ilRc = WaitAndCheckQueue(10,ilAnswerQueue,&prgItem) ;
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
    }
    else
    {
     prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
     prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
     prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
     pclSelection = (char *)    prlCmdblk->data ;
     pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
     pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;

     if(strcmp(pclData,"SUCCESS") != 0)
     {
      dbg(TRACE,"TriggerAction: add dynamic action config failed") ;
      DebugPrintItem(DEBUG,prgItem) ;
      DebugPrintEvent(DEBUG,prlEvent) ;
      DebugPrintBchead(DEBUG,prlBchead) ;
      DebugPrintCmdblk(DEBUG,prlCmdblk) ;
      dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ilRc = RC_FAIL ;
     }
     else
     {
      dbg(DEBUG,"TriggerAction: add dynamic action config OK") ;
     }/* end of if */
    }/* end of if */
   }/* end of if */
  }/* end of if */

  ilRc = que(QUE_DELETE,ilAnswerQueue,ilAnswerQueue,0,0,0) ;
  if(ilRc != RC_SUCCESS)
  {
   dbg(TRACE,"que QUE_DELETE <%d> failed <%d>",ilAnswerQueue, ilRc) ;
  }
  else
  {
   dbg(DEBUG,"TriggerAction: queue <%d> <%s> deleted", ilAnswerQueue, &clQueueName[0]) ;
  }/* end of if */
 }/* end of if */

 return (ilRc) ;
    
} /*end TriggerAction*/



/******************************************************************************/
/* The CheckQueue routine                                                     */
/******************************************************************************/
static int CheckQueue(int ipModId, ITEM **prpItem)
{
    int     ilRc       = RC_SUCCESS;                      /* Return code */
    int     ilItemSize = 0;
    EVENT *prlEvent    = NULL;
    
    ilItemSize = I_SIZE;
    
    ilRc = que(QUE_GETBIGNW,ipModId,ipModId,PRIORITY_3,ilItemSize,(char *) prpItem);
    if(ilRc == RC_SUCCESS)
    {
        prlEvent = (EVENT*) ((*prpItem)->text);

        switch( prlEvent->command )
        {
            case    HSB_STANDBY :
                dbg(TRACE,"CheckQueue: HSB_STANDBY");
                HandleQueues();
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_COMING_UP   :
                dbg(TRACE,"CheckQueue: HSB_COMING_UP");
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_ACTIVE  :
                dbg(TRACE,"CheckQueue: HSB_ACTIVE");
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_ACT_TO_SBY  :
                dbg(TRACE,"CheckQueue: HSB_ACT_TO_SBY");
                HandleQueues();
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_DOWN    :
                dbg(TRACE,"CheckQueue: HSB_DOWN");
                ctrl_sta = prlEvent->command;
                /* Acknowledge the item */
                ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
                if( ilRc != RC_SUCCESS )
                {
                    /* handle que_ack error */
                    HandleQueErr(ilRc);
                } /* fi */
                Terminate(30);
                break;
        
            case    HSB_STANDALONE  :
                dbg(TRACE,"CheckQueue: HSB_STANDALONE");
                ctrl_sta = prlEvent->command;
                break;
    
            case    SHUTDOWN    :
                dbg(TRACE,"CheckQueue: SHUTDOWN");
                /* Acknowledge the item */
                ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
                if( ilRc != RC_SUCCESS )
                {
                    /* handle que_ack error */
                    HandleQueErr(ilRc);
                } /* fi */
                Terminate(30);
                break;
    
            case    RESET       :
                ilRc = Reset();
                break;
    
            case    EVENT_DATA  :
                /* DebugPrintItem(DEBUG,prgItem); */
                /* DebugPrintEvent(DEBUG,prlEvent); */
                break;
    
            case    TRACE_ON :
                dbg_handle_debug(prlEvent->command);
                break;
    
            case    TRACE_OFF :
                dbg_handle_debug(prlEvent->command);
                break;
    
            default         :
                dbg(TRACE,"CheckQueue: unknown event");
                DebugPrintItem(TRACE,*prpItem);
                DebugPrintEvent(TRACE,prlEvent);
                break;
    
        } /* end switch */
    
        /* Acknowledge the item */
        ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
        if( ilRc != RC_SUCCESS )
        {
            /* handle que_ack error */
            HandleQueErr(ilRc);
        } /* fi */
    }else{
        if(ilRc != QUE_E_NOMSG)
        {
            dbg(TRACE,"CheckQueue: que(QUE_GETBIG,%d,%d,...) failed <%d>",ipModId,ipModId,ilRc);
            HandleQueErr(ilRc);
        }/* end of if */
    }/* end of if */
    
    return ilRc;
}/* end - CheckQueue */




/******************************************************************************/
/* The CreateCEDAArrays                                                       */
/******************************************************************************/
static int CreateCEDAArrays()
{
  int  ilRC =RC_SUCCESS;
  long pclAddFieldLens[12];
  char pclAddFields[256] = "\0";
  char pclSelection[1024] = "\0";
  char  cgArrStart[16] = "\0";                /* Timeframe Arrival Start*/
  char  cgArrEnd[16] = "\0";                  /* Timeframe Arrival End  */
  char  cgDepStart[16] = "\0";                /* Timeframe Departure Start */
  char  cgDepEnd[16] = "\0";                  /* Timeframe Departure End   */  
  char  cgArrStartPrev[16] = "\0";
  char  cgDepStartPrev[16] = "\0";
  char pclNowTime[16] = "\0";
  char pclNowTimeExt[16] = "\0";

  OffsettimeFromNow(&timeoffsets.lgTipc01, cgArrStart);
  OffsettimeFromNow(&timeoffsets.lgTipc02, cgDepStart);
  OffsettimeFromNow(&timeoffsets.lgTipc03, cgArrEnd);
  OffsettimeFromNow(&timeoffsets.lgTipc04, cgDepEnd);
  strcpy(cgArrStartPrev,cgArrStart);
  AddSecondsToCEDATime12(cgArrStartPrev,-86400,1);
  strcpy(cgDepStartPrev,cgDepStart);
  AddSecondsToCEDATime12(cgDepStartPrev,-86400,1);

  dbg(TRACE,"%05d: cgArrStart: <%s>",__LINE__,cgArrStart);
  dbg(TRACE,"%05d: cgArrEnd: <%s>",__LINE__,cgArrEnd);
  dbg(TRACE,"%05d: cgDepStart: <%s>",__LINE__,cgDepStart);
  dbg(TRACE,"%05d: cgDepEnd: <%s>",__LINE__,cgDepEnd);

  ilRC = CEDAArrayInitialize(20,20);
  if ( ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%05d: CEDAArrayInitialize failed!, ilRC: %d",__LINE__,ilRC);
    }

  /************** now create AftArray ***************/

  dbg(TRACE,"%05d: Now create AftArray !!",__LINE__);

/*
  sprintf(pclSelection,"WHERE ((TIFA > '%s' AND TIFA < '%s') OR ( TIFD > '%s' AND TIFD < '%s')) AND FTYP IN (%s) ",cgArrStart,cgArrEnd,cgDepStart,cgDepEnd,pcgFTYP);
*/
  sprintf(pclSelection,"WHERE ((TIFA BETWEEN '%s' AND '%s' AND FTYP IN (%s)) OR (TIFD BETWEEN '%s' AND '%s' AND FTYP IN (%s))) OR ((TIFA BETWEEN '%s' AND '%s' AND FTYP IN (%s) AND REMP IN (%s)) OR (TIFD BETWEEN '%s' AND '%s' AND FTYP IN (%s) AND REMP IN (%s)))",
      cgArrStart,cgArrEnd,pcgFTYP,cgDepStart,cgDepEnd,pcgFTYP,
      cgArrStartPrev,cgArrStart,pcgFTYP,pcgUnknownDelays,
      cgDepStartPrev,cgDepStart,pcgFTYP,pcgUnknownDelays);
  strcpy(pclAddFields,"");
  pclAddFieldLens[0] = 0;
  pclAddFieldLens[1] = 0;
  pclAddFieldLens[2] = 0;
  pclAddFieldLens[3] = 0;
  pclAddFieldLens[4] = 0;
  
  dbg(DEBUG,"%05d: AFT_FIELDS: <%s>, &rgAftArray: <%d>, pclSelection: <%s>",__LINE__,AFT_FIELDS,&rgAftArray,pclSelection);
  ilRC = SetArrayInfo("AFTTAB","AFTTAB",AFT_FIELDS,
                      NULL,NULL,&rgAftArray,pclSelection,TRUE);

  if(ilRC != RC_SUCCESS)
    {
    dbg(TRACE," SetArrayInfo AFT_TAB failed <%d>",ilRC);
    }
  else
    {
    dbg(DEBUG,"SetArrayInfo AFT_TAB OK");
    dbg(DEBUG,"<%s> %d",rgAftArray.crArrayName,
    rgAftArray.rrArrayHandle);
    ilRC = TriggerAction("AFTTAB");
    if(ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"TriggerAction failed <%d>",ilRC);
    }
    }

    /********************** now create ArrayIndex ***********************/

    strcpy(rgAftArray.crIdx01Name,"AFT_URNO");
    ilRC = CEDAArrayCreateSimpleIndexUp(&rgAftArray.rrArrayHandle,
                    rgAftArray.crArrayName,
                    &rgAftArray.rrIdx01Handle,
                    rgAftArray.crIdx01Name,"URNO");

 

  /***************** now create FldArray ***************************/

  dbg(TRACE,"%05d: Now create FldArray !!",__LINE__);

  TimeToStr(pclNowTime,time(NULL));
  /*sprintf(pclSelection,"WHERE (DSEQ <> '-1' AND DCTI >= '%s') AND ((DOTI <= '%s')  OR ((AOTI <> ' ') AND (AOTI <= '%s')))",pclNowTime,pclNowTime,pclNowTime);*/
  /*sprintf(pclSelection,"WHERE DSEQ <> '-1' AND ((SOTI >= '%s') AND (SCTI <= '%s'))",cgArrStart,cgDepEnd);*/
  sprintf(pclSelection,"WHERE RTYP IN (%s)",pcgMyRtypes);
  /*sprintf(pclSelection,"WHERE ((SOTI >= '%s') AND (SCTI <= '%s'))",cgArrStart,cgDepEnd);*/
  strcpy(pclAddFields,"TIFF,OBLF,STOF");
  pclAddFieldLens[0] = 16;
  pclAddFieldLens[1] = 16;
  pclAddFieldLens[2] = 16;
  pclAddFieldLens[3] = 0;
  pclAddFieldLens[4] = 0;
   
 
  dbg(DEBUG,"%05d: FLD_FIELDS: <%s>, &rgFldArray: <%d>, pclSelection: <%s>",__LINE__,FLD_FIELDS,&rgFldArray,pclSelection);
  ilRC = SetArrayInfo("FLDTAB","FLDTAB",FLD_FIELDS_TAB,
                      pclAddFields,pclAddFieldLens,&rgFldArray,pclSelection,TRUE);

  if(ilRC != RC_SUCCESS)
    {
    dbg(TRACE," SetArrayInfo FLD_TAB failed <%d>",ilRC);
    }
  else
    {
    dbg(DEBUG,"SetArrayInfo FLD_TAB OK");
    dbg(DEBUG,"<%s> %d",rgFldArray.crArrayName,
    rgFldArray.rrArrayHandle);
    /* ilRC = TriggerAction("FLDTAB"); */

    if(ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"TriggerAction failed <%d>",ilRC);
    }

    }

    /********************** now create FldArrayIndex ***********************/

    strcpy(rgFldArray.crIdx01Name,"FLD_URNO");
    ilRC = CEDAArrayCreateSimpleIndexUp(&rgFldArray.rrArrayHandle,
                    rgFldArray.crArrayName,
                    &rgFldArray.rrIdx01Handle,
                    rgFldArray.crIdx01Name,"URNO");

    strcpy(rgFldArray.crIdx02Name,"FLD_RURN");
    ilRC = CEDAArrayCreateSimpleIndexUp(&rgFldArray.rrArrayHandle,
                    rgFldArray.crArrayName,
                    &rgFldArray.rrIdx02Handle,
                    rgFldArray.crIdx02Name,"RURN");

    if(ilRC == RC_SUCCESS)
      {
    dbg(DEBUG,"%05d: CreateSimpleIndexUp succeeded",__LINE__);
      }
    else
      {
    dbg(DEBUG,"%05d: CreateSimpleIndexUp failed",__LINE__);
      }

    strcpy(rgFldArray.crIdx03Name,"FLD_CNT_INFO");
    ilRC = CEDAArrayCreateSimpleIndexUp(&rgFldArray.rrArrayHandle,
                    rgFldArray.crArrayName,
                    &rgFldArray.rrIdx03Handle,
                    rgFldArray.crIdx03Name,"RNAM,RTYP,TIFF,OBLF,STOF");
    if(ilRC == RC_SUCCESS)
      {
    dbg(DEBUG,"%05d: CreateSimpleIndexUp succeeded",__LINE__);
      }
    else
      {
    dbg(DEBUG,"%05d: CreateSimpleIndexUp failed",__LINE__);
      }

    strcpy(rgFldArray.crIdx04Name,"FLD_DSEQ");
    ilRC = CEDAArrayCreateSimpleIndexUp(&rgFldArray.rrArrayHandle,
                    rgFldArray.crArrayName,
                    &rgFldArray.rrIdx04Handle,
                    rgFldArray.crIdx04Name,"DSEQ");
    if(ilRC == RC_SUCCESS)
      {
    dbg(DEBUG,"%05d: CreateSimpleIndexUp succeeded",__LINE__);
      }
    else
      {
    dbg(DEBUG,"%05d: CreateSimpleIndexUp failed",__LINE__);
      }
#if 0
    strcpy(rgFldArray.crIdx05Name,"FLD_RNAM");
    ilRC = CEDAArrayCreateSimpleIndexUp(&rgFldArray.rrArrayHandle,
                    rgFldArray.crArrayName,
                    &rgFldArray.rrIdx05Handle,
                    rgFldArray.crIdx05Name,"RNAM");
    if(ilRC == RC_SUCCESS)
      {
    dbg(DEBUG,"%05d: CreateSimpleIndexUp succeeded",__LINE__);
      }
    else
      {
    dbg(DEBUG,"%05d: CreateSimpleIndexUp failed",__LINE__);
      }

    strcpy(rgFldArray.crIdx06Name,"FLD_AURN");
    ilRC = CEDAArrayCreateSimpleIndexUp(&rgFldArray.rrArrayHandle,
                    rgFldArray.crArrayName,
                    &rgFldArray.rrIdx06Handle,
                    rgFldArray.crIdx06Name,"AURN");
    if(ilRC == RC_SUCCESS)
      {
    dbg(DEBUG,"%05d: CreateSimpleIndexUp succeeded",__LINE__);
      }
    else
      {
    dbg(DEBUG,"%05d: CreateSimpleIndexUp failed",__LINE__);
      }
#endif

 
  /***************** now create FddArray ***************************/

  dbg(TRACE,"%05d: Now create FddArray !!",__LINE__);
  
  sprintf(pclSelection,"");
  if (strcmp(pcgFddWithBoardingTime,"YES") == 0)
  {
     strcpy(pclAddFields,"NODE,OBL1,OBL2,OGT1,OGT2");
     pclAddFieldLens[0] = 2;
     pclAddFieldLens[1] = 6;
     pclAddFieldLens[2] = 6;
     pclAddFieldLens[3] = 6;
     pclAddFieldLens[4] = 6;
  }
  else
  {
     strcpy(pclAddFields,"GD1B,GD2B,NODE,OBL1,OBL2,OGT1,OGT2");
     pclAddFieldLens[0] = 16;
     pclAddFieldLens[1] = 16;
     pclAddFieldLens[2] = 2;
     pclAddFieldLens[3] = 6;
     pclAddFieldLens[4] = 6;
     pclAddFieldLens[5] = 6;
     pclAddFieldLens[6] = 6;
  }
   
 
  dbg(DEBUG,"%05d: FDD_FIELDS: <%s>, &rgFddArray: <%d>, pclSelection: <%s>",__LINE__,FDD_FIELDS,&rgFddArray,pclSelection);
  ilRC = SetArrayInfo("FDDTAB","FDDTAB",FDD_FIELDS_TAB,
                      pclAddFields,pclAddFieldLens,&rgFddArray,pclSelection,TRUE);

  if(ilRC != RC_SUCCESS)
    {
    dbg(TRACE," SetArrayInfo FDD_TAB failed <%d>",ilRC);
    }
  else
    {
    dbg(DEBUG,"SetArrayInfo FDD_TAB OK");
    dbg(DEBUG,"<%s> %d",rgFddArray.crArrayName,
    rgFddArray.rrArrayHandle);

    }

    /********************** now create FddArrayIndex ***********************/

    strcpy(rgFddArray.crIdx01Name,"FDD_URNO");
    ilRC = CEDAArrayCreateSimpleIndexUp(&rgFddArray.rrArrayHandle,
                    rgFddArray.crArrayName,
                    &rgFddArray.rrIdx01Handle,
                    rgFddArray.crIdx01Name,"URNO");

    strcpy(rgFddArray.crIdx02Name,"FDD_RURN");
    ilRC = CEDAArrayCreateSimpleIndexUp(&rgFddArray.rrArrayHandle,
                    rgFddArray.crArrayName,
                    &rgFddArray.rrIdx02Handle,
                    rgFddArray.crIdx02Name,"RURN");


  /************** now create CcaArray ***************/

  dbg(TRACE,"%05d: Now create CcaArray !!",__LINE__);
  TimeToStr(pclNowTime,time(NULL));
  strcpy(pclNowTimeExt,pclNowTime);
  AddSecondsToCEDATime12(pclNowTimeExt,igRefreshTime,1);

  /*sprintf(pclSelection,"WHERE CKBS <= '%s' AND CKES >= '%s' AND CKIC <> ' '",pclNowTime,pclNowTime);*/
  /*sprintf(pclSelection,"WHERE CKBS > '%s' AND CKES < '%s'",cgArrStart,cgDepEnd);  */
  /*
  sprintf(pclSelection,"WHERE (((CKBS <= '%s' AND CKES >= '%s') OR (CKBA <= '%s' AND CKEA >= '%s')) OR (CKBS > '%s' AND CKES < '%s')) AND CKIC <> ' ' ORDER BY CKIC,CKBS",
      pclNowTime,pclNowTime,pclNowTimeExt,pclNowTimeExt,cgArrStart,cgDepEnd);
  */
  sprintf(pclSelection,"WHERE ((CKBS <= '%s' AND CKES >= '%s') OR (CKBA <= '%s' AND CKEA >= '%s')) AND CKIC <> ' ' AND FLNU <> 0 ORDER BY CKIC,CKBS",
      pclNowTime,pclNowTime,pclNowTimeExt,pclNowTimeExt);
  strcpy(pclAddFields,"FLAG");
  pclAddFieldLens[0] = 2;
  pclAddFieldLens[1] = 0;
  pclAddFieldLens[2] = 0;
  pclAddFieldLens[3] = 0;
  pclAddFieldLens[4] = 0;
  
  dbg(TRACE,"Fill CCA-Array: CCA_FIELDS: <%s>, &rgCcaArray: <%d>, pclSelection: <%s>",CCA_FIELDS_TAB,&rgCcaArray,pclSelection);
  ilRC = SetArrayInfo("CCATAB","CCATAB",CCA_FIELDS_TAB,
                      pclAddFields,pclAddFieldLens,&rgCcaArray,pclSelection,TRUE);

  if(ilRC != RC_SUCCESS)
    {
    dbg(TRACE," SetArrayInfo CCA_TAB failed <%d>",ilRC);
    }
  else
    {
    dbg(DEBUG,"SetArrayInfo CCA_TAB OK");
    dbg(DEBUG,"<%s> %d",rgCcaArray.crArrayName,
    rgCcaArray.rrArrayHandle);
    ilRC = TriggerAction("CCATAB");
    if(ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"TriggerAction failed <%d>",ilRC);
    }
    }

    /********************** now create ArrayIndex ***********************/
    strcpy(rgCcaArray.crIdx01Name,"CCA_URNO");
    ilRC = CEDAArrayCreateSimpleIndexUp(&rgCcaArray.rrArrayHandle,
                    rgCcaArray.crArrayName,
                    &rgCcaArray.rrIdx01Handle,
                    rgCcaArray.crIdx01Name,"URNO");

    strcpy(rgCcaArray.crIdx02Name,"CCA_FLAG");
    ilRC = CEDAArrayCreateSimpleIndexUp(&rgCcaArray.rrArrayHandle,
                    rgCcaArray.crArrayName,
                    &rgCcaArray.rrIdx02Handle,
                    rgCcaArray.crIdx02Name,"FLAG");

    strcpy(rgCcaArray.crIdx03Name,"CCA_FLNU");
    ilRC = CEDAArrayCreateSimpleIndexUp(&rgCcaArray.rrArrayHandle,
                    rgCcaArray.crArrayName,
                    &rgCcaArray.rrIdx03Handle,
                    rgCcaArray.crIdx03Name,"FLNU");

    strcpy(rgCcaArray.crIdx04Name,"CCA_CKIC");
    ilRC = CEDAArrayCreateSimpleIndexUp(&rgCcaArray.rrArrayHandle,
                    rgCcaArray.crArrayName,
                    &rgCcaArray.rrIdx04Handle,
                    rgCcaArray.crIdx04Name,"CKIC");


 
   /************** now create AltArray ***************/

  dbg(TRACE,"%05d: Now create AltArray !!",__LINE__);

  strcpy(pclSelection,"");
  strcpy(pclAddFields,"");
  pclAddFieldLens[0] = 0;
  pclAddFieldLens[1] = 0;
  pclAddFieldLens[2] = 0;
  pclAddFieldLens[3] = 0;
  pclAddFieldLens[4] = 0;
  
  dbg(DEBUG,"%05d: ALT_FIELDS: <%s>, &rgAltArray: <%d>, pclSelection: <%s>",__LINE__,ALT_FIELDS,&rgAltArray,pclSelection);
  ilRC = SetArrayInfo("ALTTAB","ALTTAB",ALT_FIELDS,
                      NULL,NULL,&rgAltArray,pclSelection,TRUE);

  if(ilRC != RC_SUCCESS)
    {
    dbg(TRACE," SetArrayInfo ALT_TAB failed <%d>",ilRC);
    }
  else
    {
    dbg(DEBUG,"SetArrayInfo ALT_TAB OK");
    dbg(DEBUG,"<%s> %d",rgAltArray.crArrayName,
    rgAltArray.rrArrayHandle);
    ilRC = TriggerAction("ALTTAB");
    if(ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"TriggerAction failed <%d>",ilRC);
    }
    }

    /********************** now create ArrayIndex ***********************/

    strcpy(rgAltArray.crIdx01Name,"ALT_URNO");
    ilRC = CEDAArrayCreateSimpleIndexUp(&rgAltArray.rrArrayHandle,
                    rgAltArray.crArrayName,
                    &rgAltArray.rrIdx01Handle,
                    rgAltArray.crIdx01Name,"URNO");

  /************** now create ChaArray ***************/

  dbg(TRACE,"%05d: Now create ChaArray !!",__LINE__);

  TimeToStr(pclNowTime,time(NULL));
  strcpy(pclSelection,"");
  sprintf(pclSelection,"WHERE TIFB <= '%s' AND TIFE >= '%s' AND LNAM <> ' ' ORDER BY LNAM,TIFB",
          pclNowTime,pclNowTime);
  strcpy(pclAddFields,"");
  pclAddFieldLens[0] = 0;
  pclAddFieldLens[1] = 0;
  pclAddFieldLens[2] = 0;
  pclAddFieldLens[3] = 0;
  pclAddFieldLens[4] = 0;
  
  dbg(DEBUG,"%05d: CHA_FIELDS: <%s>, &rgChaArray: <%d>, pclSelection: <%s>",__LINE__,CHA_FIELDS,&rgChaArray,pclSelection);
  ilRC = SetArrayInfo("CHATAB","CHATAB",CHA_FIELDS,
                      NULL,NULL,&rgChaArray,pclSelection,TRUE);

  if(ilRC != RC_SUCCESS)
  {
     dbg(TRACE," SetArrayInfo CHA_TAB failed <%d>",ilRC);
  }
  else
  {
     dbg(DEBUG,"SetArrayInfo CHATAB OK");
     dbg(DEBUG,"<%s> %d",rgChaArray.crArrayName,
     rgChaArray.rrArrayHandle);
     ilRC = TriggerAction("CHATAB");
     if(ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"TriggerAction failed <%d>",ilRC);
     }
  }

  /********************** now create ArrayIndex ***********************/

  strcpy(rgChaArray.crIdx01Name,"CHA_URNO");
  ilRC = CEDAArrayCreateSimpleIndexUp(&rgChaArray.rrArrayHandle,
                    rgChaArray.crArrayName,
                    &rgChaArray.rrIdx01Handle,
                    rgChaArray.crIdx01Name,"URNO");

 
  ilRC = InitFieldIndex();
  if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%05d: InitFieldIndex failed",__LINE__);
    }

  /********************** now trigger ACTION and BCHDL *****************/

  ilRC = CEDAArraySendChanges2ACTION(&rgFddArray.rrArrayHandle,
                                     rgFddArray.crArrayName,TRUE);
  if (ilRC != RC_SUCCESS)
  {
     dbg(TRACE,"CreateCEDAArrays: Trigger ACTION for FDDTAB failed");
  }
  if (strcmp(pcgSendBcForFdd,"YES") == 0)
     ilRC = CEDAArraySendChanges2BCHDL(&rgFddArray.rrArrayHandle,
                                       rgFddArray.crArrayName,TRUE);
  else
     ilRC = CEDAArraySendChanges2BCHDL(&rgFddArray.rrArrayHandle,
                                       rgFddArray.crArrayName,FALSE);
  if (ilRC != RC_SUCCESS)
  {
     dbg(TRACE,"CreateCEDAArrays: Trigger BCHDL for FDDTAB failed");
  }

  if (igGenerateFLD == 1)
  {
     ilRC = CEDAArraySendChanges2ACTION(&rgFldArray.rrArrayHandle,
                                        rgFldArray.crArrayName,TRUE);
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"CreateCEDAArrays: Trigger ACTION for FLDTAB failed");
     }
     if (strcmp(pcgSendBcForFld,"YES") == 0)
        ilRC = CEDAArraySendChanges2BCHDL(&rgFldArray.rrArrayHandle,
                                          rgFldArray.crArrayName,TRUE);
     else
        ilRC = CEDAArraySendChanges2BCHDL(&rgFldArray.rrArrayHandle,
                                          rgFldArray.crArrayName,FALSE);
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"CreateCEDAArrays: Trigger BCHDL for FLDTAB failed");
     }
  }

  return ilRC;
}  /* CreateCEDAArrays */




/******************************************************************************/
/* The FindItemInArrayList                                                    */
/******************************************************************************/
static int FindItemInArrayList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos)
{
    int ilRc = RC_SUCCESS;

    ilRc = FindItemInList(pcpInput,pcpPattern,cpDel,ipLine,ipCol,ipPos);
    *ipLine = (*ipLine) -1;
    
    return ilRc;
}/* FindItmeInArrayList*/



/******************************************************************************/
/* The InitFieldIndex                                                         */
/******************************************************************************/
static int InitFieldIndex()
{
    int ilRC = RC_SUCCESS;
    int ilLine,ilCol,ilPos;
 
    /*FindItemInArrayList(DEM_FIELDS,"ALID",',',&igDemALID,&ilCol,&ilPos);*/

    /*********** AFTTAB-FIELDS ***************/

    FindItemInArrayList(AFT_FIELDS,"URNO",',',&igAftURNO,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"ADID",',',&igAftADID,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"FLNO",',',&igAftFLNO,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"FLTI",',',&igAftFLTI,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"FLNS",',',&igAftFLNS,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"STOA",',',&igAftSTOA,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"STOD",',',&igAftSTOD,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"DES3",',',&igAftDES3,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"ORG3",',',&igAftORG3,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"VIA3",',',&igAftVIA3,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"ETOA",',',&igAftETOA,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"ETOD",',',&igAftETOD,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"ONBL",',',&igAftONBL,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"OFBL",',',&igAftOFBL,&ilCol,&ilPos);
    if (strcmp(pcgNatureOrServiceType,"STYP") == 0)
       FindItemInArrayList(AFT_FIELDS,"STYP",',',&igAftTTYP,&ilCol,&ilPos);
    else
       FindItemInArrayList(AFT_FIELDS,"TTYP",',',&igAftTTYP,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"REMP",',',&igAftREMP,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"BLT1",',',&igAftBLT1,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"BLT2",',',&igAftBLT2,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"GTD1",',',&igAftGTD1,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"CKIT",',',&igAftCKIT,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"GTD2",',',&igAftGTD2,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"JFNO",',',&igAftJFNO,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"ETAI",',',&igAftETAI,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"ETDI",',',&igAftETDI,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"TMB1",',',&igAftTMB1,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"TIFA",',',&igAftTIFA,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"TIFD",',',&igAftTIFD,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"TISA",',',&igAftTISA,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"TISD",',',&igAftTISD,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"CKIF",',',&igAftCKIF,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"WRO1",',',&igAftWRO1,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"GD1X",',',&igAftGD1X,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"GD1Y",',',&igAftGD1Y,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"GD2X",',',&igAftGD2X,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"GD2Y",',',&igAftGD2Y,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"B1BA",',',&igAftB1BA,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"B1EA",',',&igAftB1EA,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"B2BA",',',&igAftB2BA,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"B2EA",',',&igAftB2EA,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"JCNT",',',&igAftJCNT,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"VIAN",',',&igAftVIAN,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"VIAL",',',&igAftVIAL,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"LAND",',',&igAftLAND,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"BOAO",',',&igAftBOAO,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"BOAC",',',&igAftBOAC,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"ALC2",',',&igAftALC2,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"ALC3",',',&igAftALC3,&ilCol,&ilPos);    
    FindItemInArrayList(AFT_FIELDS,"AIRB",',',&igAftAIRB,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"B1BS",',',&igAftB1BS,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"B1ES",',',&igAftB1ES,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"B2BS",',',&igAftB2BS,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"B2ES",',',&igAftB2ES,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"GD1B",',',&igAftGD1B,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"GD1E",',',&igAftGD1E,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"GD2B",',',&igAftGD2B,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"GD2E",',',&igAftGD2E,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"W1BA",',',&igAftW1BA,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"W1EA",',',&igAftW1EA,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"W1BS",',',&igAftW1BS,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"W1ES",',',&igAftW1ES,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"STEV",',',&igAftSTEV,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"FTYP",',',&igAftFTYP,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"NXTI",',',&igAftNXTI,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"TGD1",',',&igAftTGD1,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"TGD2",',',&igAftTGD2,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"TET1",',',&igAftTET1,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"BAS1",',',&igAftBAS1,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"BAE1",',',&igAftBAE1,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"BAS2",',',&igAftBAS2,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"BAE2",',',&igAftBAE2,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"RGD1",',',&igAftRGD1,&ilCol,&ilPos);
    FindItemInArrayList(AFT_FIELDS,"RGD2",',',&igAftRGD2,&ilCol,&ilPos);


    /******************* FLDTAB-FIELDS **********************/

    FindItemInArrayList(FLD_FIELDS,"AURN",',',&igFldAURN,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"URNO",',',&igFldURNO,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"ABTI",',',&igFldABTI,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"ACTI",',',&igFldACTI,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"AFTI",',',&igFldAFTI,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"AOTI",',',&igFldAOTI,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"CDAT",',',&igFldCDAT,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"CREC",',',&igFldCREC,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"CTYP",',',&igFldCTYP,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"DCTI",',',&igFldDCTI,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"DOTI",',',&igFldDOTI,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"DSEQ",',',&igFldDSEQ,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"HOPO",',',&igFldHOPO,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"LSTU",',',&igFldLSTU,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"RNAM",',',&igFldRNAM,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"RTAB",',',&igFldRTAB,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"RURN",',',&igFldRURN,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"RTYP",',',&igFldRTYP,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"SCTI",',',&igFldSCTI,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"SOTI",',',&igFldSOTI,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"STAT",',',&igFldSTAT,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"USEC",',',&igFldUSEC,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"USEU",',',&igFldUSEU,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"TIFF",',',&igFldTIFF,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"OBLF",',',&igFldOBLF,&ilCol,&ilPos);
    FindItemInArrayList(FLD_FIELDS,"STOF",',',&igFldSTOF,&ilCol,&ilPos);

    /******************** FDDTAB - Fields ********************/

    FindItemInArrayList(FDD_FIELDS,"ADID",',',&igFddADID,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"AIRB",',',&igFddAIRB,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"APC3",',',&igFddAPC3,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"AURN",',',&igFddAURN,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"BLT1",',',&igFddBLT1,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"BLT2",',',&igFddBLT2,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"CDAT",',',&igFddCDAT,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"CKIF",',',&igFddCKIF,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"CKIT",',',&igFddCKIT,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"DACO",',',&igFddDACO,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"DNAT",',',&igFddDNAT,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"ETOF",',',&igFddETOF,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"FLNO",',',&igFddFLNO,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"FTYP",',',&igFddFTYP,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"GTD1",',',&igFddGTD1,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"GTD2",',',&igFddGTD2,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"HOPO",',',&igFddHOPO,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"INDO",',',&igFddINDO,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"LAND",',',&igFddLAND,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"LSTU",',',&igFddLSTU,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"OFBL",',',&igFddOFBL,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"ONBL",',',&igFddONBL,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"REMP",',',&igFddREMP,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"STOF",',',&igFddSTOF,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"TIFF",',',&igFddTIFF,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"URNO",',',&igFddURNO,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"USEC",',',&igFddUSEC,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"USEU",',',&igFddUSEU,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"VIA3",',',&igFddVIA3,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"WRO1",',',&igFddWRO1,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"DSEQ",',',&igFddDSEQ,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"RMTI",',',&igFddRMTI,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"GD1B",',',&igFddGD1B,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"GD2B",',',&igFddGD2B,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"NODE",',',&igFddNODE,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"OBL1",',',&igFddOBL1,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"OBL2",',',&igFddOBL2,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"OGT1",',',&igFddOGT1,&ilCol,&ilPos);
    FindItemInArrayList(FDD_FIELDS,"OGT2",',',&igFddOGT2,&ilCol,&ilPos);

    /********************** CCATAB - Fields ********************/

    FindItemInArrayList(CCA_FIELDS,"URNO",',',&igCcaURNO,&ilCol,&ilPos);
    FindItemInArrayList(CCA_FIELDS,"FLNU",',',&igCcaFLNU,&ilCol,&ilPos);
    FindItemInArrayList(CCA_FIELDS,"CKIC",',',&igCcaCKIC,&ilCol,&ilPos);
    FindItemInArrayList(CCA_FIELDS,"CKBS",',',&igCcaCKBS,&ilCol,&ilPos);
    FindItemInArrayList(CCA_FIELDS,"CKES",',',&igCcaCKES,&ilCol,&ilPos);
    FindItemInArrayList(CCA_FIELDS,"CKBA",',',&igCcaCKBA,&ilCol,&ilPos);
    FindItemInArrayList(CCA_FIELDS,"CKEA",',',&igCcaCKEA,&ilCol,&ilPos);
    FindItemInArrayList(CCA_FIELDS,"CTYP",',',&igCcaCTYP,&ilCol,&ilPos);
    FindItemInArrayList(CCA_FIELDS,"DISP",',',&igCcaDISP,&ilCol,&ilPos);
    FindItemInArrayList(CCA_FIELDS,"LSTU",',',&igCcaLSTU,&ilCol,&ilPos);
    FindItemInArrayList(CCA_FIELDS,"CDAT",',',&igCcaCDAT,&ilCol,&ilPos);
    FindItemInArrayList(CCA_FIELDS,"REMA",',',&igCcaREMA,&ilCol,&ilPos);
    FindItemInArrayList(CCA_FIELDS,"GRNU",',',&igCcaGRNU,&ilCol,&ilPos);
    FindItemInArrayList(CCA_FIELDS,"FLNO",',',&igCcaFLNO,&ilCol,&ilPos);
    FindItemInArrayList(CCA_FIELDS,"JFNO",',',&igCcaJFNO,&ilCol,&ilPos);
    FindItemInArrayList(CCA_FIELDS,"FLAG",',',&igCcaFLAG,&ilCol,&ilPos);


    /********************** ALTTAB - Fields ********************/

    FindItemInArrayList(ALT_FIELDS,"URNO",',',&igAltURNO,&ilCol,&ilPos);
    FindItemInArrayList(ALT_FIELDS,"ALC2",',',&igAltALC2,&ilCol,&ilPos);
    FindItemInArrayList(ALT_FIELDS,"ALC3",',',&igAltALC3,&ilCol,&ilPos);


    /********************** CHATAB - Fields ********************/

    FindItemInArrayList(CHA_FIELDS,"URNO",',',&igChaURNO,&ilCol,&ilPos);
    FindItemInArrayList(CHA_FIELDS,"LNAM",',',&igChaLNAM,&ilCol,&ilPos);
    FindItemInArrayList(CHA_FIELDS,"RURN",',',&igChaRURN,&ilCol,&ilPos);
    FindItemInArrayList(CHA_FIELDS,"TIFB",',',&igChaTIFB,&ilCol,&ilPos);
    FindItemInArrayList(CHA_FIELDS,"TIFE",',',&igChaTIFE,&ilCol,&ilPos);
    FindItemInArrayList(CHA_FIELDS,"DES3",',',&igChaDES3,&ilCol,&ilPos);
    FindItemInArrayList(CHA_FIELDS,"CHGF",',',&igChaCHGF,&ilCol,&ilPos);
    FindItemInArrayList(CHA_FIELDS,"FCLA",',',&igChaFCLA,&ilCol,&ilPos);
    FindItemInArrayList(CHA_FIELDS,"LAFX",',',&igChaLAFX,&ilCol,&ilPos);


    return (ilRC);

}/*end InitFieldIndex();*/


/******************************************************************************/
/* The TrimRight routine                                                      */
/******************************************************************************/
static void TrimRight(char *pcpBuffer)
{
    char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];
    if (strlen(pcpBuffer) == 0)
    {
       strcpy(pcpBuffer," ");
    }
    else
    {
       while(isspace(*pclBlank) && pclBlank != pcpBuffer)
       {
          *pclBlank = '\0';
          pclBlank--;
       }
    }
}/* end of TrimRight*/


/******************************************************************************/
/* The CheckFlightNature routine                                              */
/******************************************************************************/
static int CheckFlightNature (char *pclCurRowAFT)
{
  int  ilRC = RC_SUCCESS;
  char clTtyp[10];
  char pclURNO[128];
  char pclFLTI[128];
  char pclBuf[128];
  char pclSecBuf[128];
  int ilCntDispFound1 = 0;
  int ilCntDispFound2 = 0;
  int ilDoMixFound = 0;
  int ili = 0;

  strcpy(pclURNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftURNO));
  TrimRight(pclURNO);
  strcpy(clTtyp,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP));
  TrimRight(clTtyp);
  strcpy(pclFLTI,FIELDVAL(rgAftArray,pclCurRowAFT,igAftFLTI));
  TrimRight(pclFLTI);

  prguCntDisp = prgCntDisp;
  prguDoMix = prgDoMix;

  /*  dbg(DEBUG,"%05d: pclFLTI: <%s>, clTtyp: <%s>",__LINE__,pclFLTI,clTtyp);*/

  if (strstr(pcgMixedFLTI,pclFLTI) != NULL)
  {
     if (igCheckNatureLength == 1)
     {
        if (strlen(clTtyp) == 5)
        {
           strncpy(pclBuf,clTtyp,2);
           pclBuf[2] = '\0';
        }
        else
        {
           pclBuf[0] = '\0';
        }
     }
     else
     {
        strcpy(pclBuf,clTtyp);
     }
     ili = 1;
     while (ili <= igSec1End)  
     {
        if (strcmp(prguCntDisp->Section, pclBuf) == 0)
        {
           ilCntDispFound1 = 1;
        }
        prguCntDisp++;
        ili++;
     }/* end of while*/
     prguCntDisp = prgCntDisp;
     if (igCheckNatureLength == 1)
     {
        if (strlen(clTtyp) == 5)
        {
           strncpy(pclBuf,&clTtyp[2],2);
           pclBuf[2] = '\0';
        }
        else
        {
           pclBuf[0] = '\0';
        }
        ili = 1;
        while (ili <= igSec1End)  
        {
           if (strcmp(prguCntDisp->Section, pclBuf) == 0)
           {
              ilCntDispFound2 = 1;
           }
           prguCntDisp++;
           ili++;
        }/* end of while*/
     }
     else
     {
        strcpy(pclBuf,clTtyp);
        ilCntDispFound2 = 1;
     }
     prguDoMix = prgDoMix;
     if (igCheckNatureLength == 1)
     {
        if (strlen(clTtyp) == 5)
        {
           strncpy(pclBuf,&clTtyp[4],1);
           pclBuf[1] = '\0';
        }
        else
        {
           pclBuf[0] = '\0';
        }
        ili = 1;
        while (ili <= igSec2End)  
        {
           if (strcmp(prguDoMix->Section, pclBuf) == 0)
           {
              ilDoMixFound = 1;
           }
           prguDoMix++;
           ili++;
        }/* end of while*/
     }
     else
     {
        strcpy(pclBuf,clTtyp);
        ilDoMixFound = 1;
     }
  }
  else
  {
     if (strstr(pcgIntFLTI,pclFLTI) != NULL || strstr(pcgDomFLTI,pclFLTI) != NULL)
     {
        if (igCheckNatureLength == 1)
        {
           if (strlen(clTtyp) == 2)
           {
              strncpy(pclBuf,clTtyp,2);
              pclBuf[2] = '\0';
           }
           else
           {
              pclBuf[0] = '\0';
           }
        }
        else
        {
           strcpy(pclBuf,clTtyp);
        }
        ili = 1;
        while (ili <= igSec1End)  
        {
           if (strcmp(prguCntDisp->Section, pclBuf) == 0)
           {
              ilDoMixFound = 1;
           }
           prguCntDisp++;
           ili++;
        }/* end of while*/
     }
  }

  if (((ilCntDispFound1 == 1) && (ilCntDispFound2 == 1) && (ilDoMixFound == 1)) || (ilDoMixFound == 1))
  {
     ilRC = RC_SUCCESS;
     /*dbg(DEBUG,"%05d: ########################Flight-Nature is valid",__LINE__);*/
     /*      dbg(DEBUG,"%05d: ilCntDispFound1: <%d>, ilCntDispFound2: <%d>, ilDoMixFound: <%d>",__LINE__,ilCntDispFound1,ilCntDispFound2,ilDoMixFound);*/
  }
  else
  {
     ilRC = RC_FAIL;
     dbg(TRACE,"CheckFlightNature: Nature <%s> for Flti <%s> (URNO = <%s> is not valid",
         clTtyp,pclFLTI,pclURNO);
     /*dbg(TRACE,"%05d: ilCntDispFound1: <%d>, ilCntDispFound2: <%d>, ilDoMixFound: <%d>",__LINE__,ilCntDispFound1,ilCntDispFound2,ilDoMixFound);*/
  }

  return(ilRC);
}
/*********************************************************************
Function : SendEvent()
Paramter : IN: pcpCmd = command for cmdblk->command
           IN: ipModID = process-ID where the event is send to
       IN: ipPriority = priority for sending ( 1- 5, usuallay 3)
       IN: pcpTable = Name (3 letters) of cmdblk->obj_name (if
           necessary), will be expanded with "pcgTabEnd".
       IN: pcpTwStart = cmdblk->twstart
       IN: pcpTwEnd = cmdblk->twend (always HOMEAP,TABEND,processname)
       IN: pcpSelection = selection for event (cmdblk->data)
       IN: pcpFields = fieldlist (corresponding to pcpdata)
       IN: pcpData = datalist (comma separated, corresponding to 
                                   pcpFields)
       IN: pcpAddStruct = additional structure to be transmitted
       IN: ipAddStructSize = size of the additional structure
Return Code: RC_SUCCESS, RC_FAIL
Result:
Description: Sends an event to another CEDA-process using que(QUE_PUT).
             Sends the event in standard CEDA-format (BCHEAD,CMDBLK,
         selection,fieldlist,datalist) or sends a different
         data structure (special) at CMDBLK->data. !! Sends always
         only one type, standard OR special, with one event !!
*********************************************************************/
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
             char *pcpTwStart, char *pcpTwEnd,
             char *pcpSelection,char *pcpFields,char *pcpData,
             char *pcpAddStruct,int ipAddStructSize)
{
  int     ilRc             = RC_FAIL;
  int     ilLen            = 0;
  EVENT   *prlOutEvent  = NULL;
  BC_HEAD *prlOutBCHead = NULL;
  CMDBLK  *prlOutCmdblk = NULL;

  if (pcpAddStruct == NULL)
    ipAddStructSize = 0;
  if (ipAddStructSize == 0)
    pcpAddStruct = NULL;

  /* size-calculation for prlOutEvent */
  ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
    strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 
    ipAddStructSize + 128;

  /* memory for prlOutEvent */
  if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
    {
      dbg(TRACE,"SendEvent: cannot malloc <%d>-bytes for outgoing event!",ilLen);
      prlOutEvent = NULL;
    }else{
      /* clear whole outgoing event */
      memset((void*)prlOutEvent, 0x00, ilLen);

      /* set event structure... */
      prlOutEvent->type         = SYS_EVENT;
      prlOutEvent->command    = EVENT_DATA;
      prlOutEvent->originator   = (short)mod_id;
      prlOutEvent->retry_count  = 0;
      prlOutEvent->data_offset  = sizeof(EVENT);
      prlOutEvent->data_length  = ilLen - sizeof(EVENT); 

      /* BC_HEAD-Structure... */
      prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
      /* prlOutBCHead->rc = (short)RC_SUCCESS;*/
      prlOutBCHead->rc = (short)NETOUT_NO_ACK;/*spaeter nur bei disconnect*/
      strncpy(prlOutBCHead->dest_name,mod_name,10);
      strncpy(prlOutBCHead->recv_name, "EXCO",10);
 
      /* Cmdblk-Structure... */
      prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
      strcpy(prlOutCmdblk->command,pcpCmd);
      if (pcpTable != NULL)
    {
      strcpy(prlOutCmdblk->obj_name,pcpTable);
      strcat(prlOutCmdblk->obj_name,pcgTabEnd);
    }
        
      /* setting tw_x entries */
      strncpy(prlOutCmdblk->tw_start,pcpTwStart,32);
      strncpy(prlOutCmdblk->tw_end,pcpTwEnd,32);
        
      /* means that no additional structure is used */
      /* STANDARD CEDA-ipcs between CEDA-processes */
      if (pcpAddStruct == NULL)
    {
      /* setting selection inside event */
      strcpy(prlOutCmdblk->data,pcpSelection);
      /* setting field-list inside event */
      strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1,pcpFields);
      /* setting data-list inside event */
      strcpy((prlOutCmdblk->data + (strlen(pcpSelection)+1) + (strlen(pcpFields)+1)),pcpData);
    }else{
      /*an additional structure is used and will be copied to */
      /*cmdblk + sizeof(CMDBLK).!!! No STANDARD CEDA-ipcs is used !!!! */
      memcpy(prlOutCmdblk->data,(char*)pcpAddStruct,ipAddStructSize);    
    }

      /*DebugPrintEvent(DEBUG,prlOutEvent);*/ 
      /*snapit((char*)prlOutEvent,ilLen,outp);*/
      dbg(DEBUG,"SendEvent: sending event to mod_id <%d>",ipModID);

      if (ipModID != 0)
    {
      if ((ilRc = que(QUE_PUT,ipModID,mod_id,ipPriority,ilLen,(char*)prlOutEvent))
          != RC_SUCCESS)
        {
          dbg(TRACE,"SendEvent: QUE_PUT returns: <%d>", ilRc);
          Terminate(1);
        }
    }else{
      dbg(TRACE,"SendEvent: mod_id = <%d>! Can't send!",ipModID);
    }
      /* free memory */
      free((void*)prlOutEvent); 
    }
  return ilRc;
} /* end of SendEvent*/



/******************************************************************************/
/* The UpdateFlight_A routine                                                 */
/******************************************************************************/
static int UpdateFlight_A(char *pclCurRowAFT, long llRowNumFDD)
{
  int ilRC =RC_SUCCESS;
  char pclSqlBuf[1024] = "\0";
  char pclBufURNO[128] = "\0";
  char pclBufONBL[128] = "\0";
  char pclBufLAND[128] = "\0";
  char pclBufETOA[128] = "\0";
  char pclBufSTOA[128] = "\0";
  char pclBufNXTI[128] = "\0";
  char pclAftREMP[128] = "\0";
  char pclAftFTYP[128] = "\0";
  char pclAftADID[128] = "\0";
  char pclNewREMP[128] = "\0";
  char pclNewRMTI[128] = " ";
  char pclTmpTime[128] = "\0";
  int ilDoUpdate = 0;

  dbg(DEBUG,"======================  This is UpdateFlight_A Begin ====================");
 
  /********* get FTYP of Current Row ********/
  strcpy(pclAftFTYP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftFTYP));
  TrimRight(pclAftFTYP);

  /********* get REMP of Current Row ********/
  strcpy(pclAftREMP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP));
  TrimRight(pclAftREMP);

  /********* get ADID of Current Row ********/
  strcpy(pclAftADID,FIELDVAL(rgAftArray,pclCurRowAFT,igAftADID));
  TrimRight(pclAftADID);

  /********* get URNO of CurrentRow**********/
  strcpy(pclBufURNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftURNO));
  TrimRight(pclBufURNO);

  /********* get ONBL of Current Row ********/
  strcpy(pclBufONBL,FIELDVAL(rgAftArray,pclCurRowAFT,igAftONBL));
  TrimRight(pclBufONBL);

  /********* get LAND of CurrentRow *********/
  strcpy(pclBufLAND,FIELDVAL(rgAftArray,pclCurRowAFT,igAftLAND));
  TrimRight(pclBufLAND);

  /********* get ETOA of CurrentRow *********/
  strcpy(pclBufETOA,FIELDVAL(rgAftArray,pclCurRowAFT,igAftETOA));
  TrimRight(pclBufETOA);

  /********* get STOA of CurrentRow *********/
  strcpy(pclBufSTOA,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTOA));
  TrimRight(pclBufSTOA);

  /********* get NXTI of CurrentRow *********/
  strcpy(pclBufNXTI,FIELDVAL(rgAftArray,pclCurRowAFT,igAftNXTI));
  TrimRight(pclBufNXTI);

  strcpy(pclNewREMP,pclAftREMP);
  strcpy(pclNewRMTI," ");
  ilDoUpdate = 0;
  if (strcmp(pclBufONBL," ") != 0 && strcmp(pcgArrived,"EMPTY") != 0)
  {
     if (strcmp(pclAftREMP,pcgArrived) != 0)
     {
        ilDoUpdate = 1;
        strcpy(pclNewREMP,pcgArrived);
     }
  }
  else
  {
     if (strcmp(pclBufLAND," ") != 0 && strcmp(pcgLanded,"EMPTY") != 0)
     {
        if (strcmp(pclAftREMP,pcgLanded) != 0)
        {
           ilDoUpdate = 1;
           strcpy(pclNewREMP,pcgLanded);
        }
     }
     else
     {
        if (strcmp(pclAftREMP,pcgArrived) == 0 || strcmp(pclAftREMP,pcgLanded) == 0)
        {
           ilDoUpdate = 1;
           strcpy(pclNewREMP," ");
        }
        if (strcmp(pclBufETOA," ") != 0)
        {
           strcpy(pclTmpTime,pclBufSTOA);
           TrimRight(pclTmpTime);
           AddSecondsToCEDATime12(pclTmpTime,igDelayTimeDiffArr,1);
           if (strcmp(pclBufETOA, pclTmpTime) > 0)     /*ETOA > STOA ?*/
           {
              strcpy(pclNewREMP,pcgDelayed);
           }
           else
           {
              strcpy(pclNewREMP,pcgExpected);
           }
           if (strcmp(pclAftREMP,pcgCanceled) == 0 ||
               strcmp(pclAftREMP,pcgDelay) == 0 ||
               strstr(pcgUnknownDelayRemark,pclAftREMP) != NULL ||
               strcmp(pclAftREMP,pcgNextinfo) == 0 ||
               pclAftREMP[0] == cBLANK)
           {
              ilDoUpdate = 1;
           }
           else
           {
              if ((strcmp(pclAftREMP,pcgDelayed) == 0 && strcmp(pclNewREMP,pcgExpected) == 0) ||
                  (strcmp(pclAftREMP,pcgExpected) == 0 && strcmp(pclNewREMP,pcgDelayed) == 0))
              {
                 ilDoUpdate = 1;
              }
           }
           if (ilDoUpdate == 0)
           {
              strcpy(pclNewREMP,pclAftREMP);
           }
        }
        else
        {
           if (strcmp(pclBufNXTI," ") != 0)
           {
              if (strcmp(pclAftREMP,pcgCanceled) == 0 ||
                  strcmp(pclAftREMP,pcgDelayed) == 0 ||
                  strcmp(pclAftREMP,pcgExpected) == 0 ||
                  pclAftREMP[0] == cBLANK)
              {
                 if (igAutoNextInfo == TRUE)
                 {
                    ilDoUpdate = 1;
                    strcpy(pclNewREMP,pcgNextinfo);
                 }
                 else
                 {
                    ilDoUpdate = 1;
                    strcpy(pclNewREMP," ");
                 }
              }
           }
           else
           {
              if (strcmp(pclAftREMP,pcgDelayed) == 0 ||
                  strcmp(pclAftREMP,pcgExpected) == 0 ||
                  strcmp(pclAftREMP,pcgNextinfo) == 0)
              {
                 ilDoUpdate = 1;
                 strcpy(pclNewREMP," ");
              }
           }
        }
     }
  }

  if (pclAftFTYP[0] == 'X')
  {
     ilDoUpdate = 0;
     strcpy(pclNewREMP,pcgCanceled);
     if (strcmp(pclAftREMP,pcgCanceled) != 0)
     {
        ilDoUpdate = 1;
     }
  }
  else
  {
     if (pclAftFTYP[0] == 'D')
     {
        ilDoUpdate = 0;
        strcpy(pclNewREMP,pcgDiverted);
        if (strcmp(pclAftREMP,pcgDiverted) != 0)
        {
           ilDoUpdate = 1;
        }
     }
     else
     {
        if (pclAftFTYP[0] == 'R')
        {
           ilDoUpdate = 0;
           strcpy(pclNewREMP,pcgRerouted);
           if (strcmp(pclAftREMP,pcgRerouted) != 0)
           {
              ilDoUpdate = 1;
           }
        }
        else
        {
           if (strcmp(pclAftREMP,pcgCanceled) == 0 ||
               strcmp(pclAftREMP,pcgDiverted) == 0 ||
               strcmp(pclAftREMP,pcgRerouted) == 0)
           {
              ilDoUpdate = 1;
              strcpy(pclNewREMP," ");
           }
        }
     }
  }

  if (igUpdateRemarksAFT == 0 || pclAftADID[0] == 'B')
  {
     strcpy(pclNewREMP,pclAftREMP);
     ilDoUpdate = 0;
  }

  if (strcmp(pclNewREMP,"EMPTY") == 0 || strcmp(pclNewREMP,pclAftREMP) == 0)
  {
     strcpy(pclNewREMP,pclAftREMP);
     ilDoUpdate = 0;
  }
 
  if (ilDoUpdate == 1)
  {
     sprintf(pclSqlBuf,"WHERE URNO = %s",pclBufURNO);
     ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","REMP",pcgTwEnd,pclSqlBuf,
                      "REMP",pclNewREMP,NULL,0);
  }
  if (strcmp(pclNewREMP,pcgArrived) == 0)
  {
     strcpy(pclNewRMTI,pclBufONBL);
  }
  else
  {
     if (strcmp(pclNewREMP,pcgLanded) == 0)
     {
        strcpy(pclNewRMTI,pclBufLAND);
     }
     else
     {
        if (strstr(pcgListOfETARem,pclNewREMP) != NULL)
        {
           strcpy(pclNewRMTI,pclBufETOA);
        }
        else
        {
           if (strcmp(pclNewREMP,pcgNextinfo) == 0)
           {
              strcpy(pclNewRMTI,pclBufNXTI);
           }
           else
           {
              strcpy(pclNewRMTI," ");
           }
        }
     }
  }
  strcpy(rgFddFlds.pclFddREMP,pclNewREMP);
  strcpy(rgFddFlds.pclFddRMTI,pclNewRMTI);

  dbg(DEBUG,"======================  This is UpdateFlight_A End   ====================");
  return (ilRC);
}




/******************************************************************************/
/* The Handle_Arrival routine                                               */
/******************************************************************************/
static int Handle_Arrival(char *pclRowAFT, long llRowNumAFT, long *llRowNumFDD)
{
  int ilRC = RC_SUCCESS;
  int ilCurrent = 0;
  char *pclRowFDD = NULL;
  char *pclUpdSelLst = NULL;
  char *pclUpdFldLst = NULL;
  char *pclUpdDatLst = NULL;
  char *pclUpdCmdLst = NULL;
  char pclRowBuffer[1024];
  char pclBuffer[128];
  char pclSqlBuf[1024];
  char pclData[1024];
  short slSqlFunc = 0;
  short slCursor = 0;
 
  char pclAftJFNO[128] = "\0";

 
      /************ now check Flight-Nature ***************/
      ilRC = CheckFlightNature(pclRowAFT);
    

      /************ now update Flight-Record ***************/ 
      if (ilRC == RC_SUCCESS) /*that is: if FlightNature is valid*/
    {
    
 
      /************* add  Row to FDDTAB *****************************/
      ilRC = InitFddStruct();            /* initialize the FDDSTRUCT */
/*
      ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
      if (ilRC != RC_SUCCESS)
        {
          dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
        }
*/
      strcpy(rgFddFlds.pclFddADID, "A");
          
        
      ilRC = CEDAArrayGetRowPointer(&rgFddArray.rrArrayHandle,
                    rgFddArray.crArrayName,
                    *llRowNumFDD,(void *)&pclRowFDD);
    
/*        dbg(DEBUG,"%05d:  FDD-Array Row added pclRowFDD: <%s> llRowNumFDD: %d",__LINE__, */
/*            pclRowFDD,llRowNumFDD); */
    
/*        dbg(DEBUG,"%05d: pclRowAFT:<%s>, pclRowFDD:<%s>",__LINE__,pclRowAFT,pclRowFDD); */



      ilRC = UpdateFlight_A (pclRowAFT,*llRowNumFDD);

    
    
      /*  dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
      /* ilRC = ShowFddStruct();*/

    }
      /************* now HandleFLTI_A  ******************/
      if (ilRC == RC_SUCCESS)
    {
      ilRC = HandleFLTI_A(pclRowAFT,pclRowFDD,llRowNumFDD);
    }

      /* dbg(TRACE,"%05d: llRowNumFDD: %d",__LINE__,*llRowNumFDD);*/
      /* dbg(TRACE,"%05d: llRowNumAFT: %d",__LINE__,llRowNumAFT);*/

  return (ilRC);

} /*end of Handle_Arrival*/



/******************************************************************************/
/* The HandleFLTI_A routine                                                    */
/******************************************************************************/
static int HandleFLTI_A (char *pcpCurRowAFT, char *pcpCurRowFDD,long *plpRowNumFDD)
{
  int ilRC = RC_SUCCESS;
  char pclBuf[128];

  /*********** get FLTI of CurrentRow****************/
  strcpy(pclBuf,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftFLTI));
  TrimRight(pclBuf);
  /********** handle international arrivals **********/
  if (strstr(pcgIntFLTI,pclBuf) != NULL )       
  {
     ilRC = HandleFLTI_Int_A(pcpCurRowAFT,pcpCurRowFDD,plpRowNumFDD,"I");
     if (ilRC != RC_SUCCESS)
        dbg(TRACE,"%05d: HandleFLTI_Int_A failed !!",__LINE__);
  }
  /********** handle domestic arrivals **********/
  if (strstr(pcgDomFLTI,pclBuf) != NULL )       
  {
     ilRC = HandleFLTI_Dom_A(pcpCurRowAFT,pcpCurRowFDD,plpRowNumFDD);
     if (ilRC != RC_SUCCESS)
        dbg(TRACE,"%05d: HandleFLTI_Dom_A failed !!",__LINE__);
  }
  /********** handle mixed arrivals **********/
  if (strstr(pcgMixedFLTI,pclBuf) != NULL )       
  {
     if (strcmp(pcgSimpleHandlingOfMixedFlights,"NO") == 0)
        ilRC = HandleFLTI_Mix_A(pcpCurRowAFT,pcpCurRowFDD,plpRowNumFDD); 
     else
        ilRC = HandleFLTI_Int_A(pcpCurRowAFT,pcpCurRowFDD,plpRowNumFDD,"M");
     if (ilRC != RC_SUCCESS)
        dbg(TRACE,"%05d: HandleFLTI_Mix_A failed !!",__LINE__);
  }

  return (ilRC);
} /*end of HandleFLTI_A */



/******************************************************************************/
/* The HandleFLTI_Int_A routine                                                    */
/******************************************************************************/
static int HandleFLTI_Int_A(char *pclCurRowAFT, char *pclCurRowFDD, long *plpRowNumFDD, char *pcpFtyp)
{

  int ilRC = RC_SUCCESS;
  int ilStart = 0;
  char pclBuffer[1024];
  int ilMainCshCnt = 0;  /*Main + CodeShare-counter*/
  char pclCDA[10] = "\0";
  long llRowNumFdd = 0;
  char pclAltURNO[128] = "\0";
  char pclCKIF[128] = "\0";
  char pclCKIT[128] = "\0";
  char pclAftTTYP[128] = "\0";
  char pclAftJFNO[128] = "\0";
  char pclTTYP[128] = "\0";
  char pclOldRemark[128] = "\0";
  char pclNewRemark[128] = "\0";
  char pclSavBLT1[128] = "\0";
  char pclSavBLT2[128] = "\0";
  int ilTerm; 
  
  dbg(DEBUG,"%05d: ================== This is HandleFLTI_Int_A !!!  ===================",__LINE__);
  ilMainCshCnt = 0;

  
  /***************** Prepare Data for first FDD-Record ****************/
  ilMainCshCnt++; /* 1 = Main-Flight */

  sprintf(rgFddFlds.pclFddDSEQ,"%d",ilMainCshCnt);
  strcpy(rgFddFlds.pclFddNODE,"\0");
  strcpy(rgFddFlds.pclFddAURN,FIELDVAL(rgAftArray,pclCurRowAFT,igAftURNO)); 
  strcpy(rgFddFlds.pclFddFLNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftFLNO));
  strcpy(rgFddFlds.pclFddTIFF,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTIFA));
  strcpy(rgFddFlds.pclFddSTOF,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTOA));
  strcpy(rgFddFlds.pclFddETOF,FIELDVAL(rgAftArray,pclCurRowAFT,igAftETOA));
  strcpy(rgFddFlds.pclFddLAND,FIELDVAL(rgAftArray,pclCurRowAFT,igAftLAND));
  strcpy(rgFddFlds.pclFddONBL,FIELDVAL(rgAftArray,pclCurRowAFT,igAftONBL));
  strcpy(rgFddFlds.pclFddWRO1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftWRO1));
  strcpy(rgFddFlds.pclFddBLT1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT1));
  strcpy(rgFddFlds.pclFddBLT2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT2));
  strcpy(rgFddFlds.pclFddGTD1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD1));
  strcpy(rgFddFlds.pclFddOBL1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT1));
  strcpy(rgFddFlds.pclFddOBL2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT2));
  strcpy(rgFddFlds.pclFddOGT1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD1));
  strcpy(rgFddFlds.pclFddOGT2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD2));
  if (strcmp(pcgHdlDACO,"STEV") == 0)
  {
     strcpy(rgFddFlds.pclFddDACO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTEV));
  }
  else
  {
     strcpy(rgFddFlds.pclFddDACO,"1");
     strcpy(pclOldRemark,FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP));
     strcpy(pclNewRemark,pclOldRemark);
     ilRC = HandleTerminal_A(pclCurRowAFT,pclNewRemark,&ilTerm);
     strcpy(pclSavBLT1,rgFddFlds.pclFddBLT1);
     strcpy(pclSavBLT2,rgFddFlds.pclFddBLT2);
     if (strcmp(pclNewRemark,"TM2") == 0)
     {
        strcpy(rgFddFlds.pclFddREMP,pclNewRemark);
        strcpy(rgFddFlds.pclFddBLT1," ");
        strcpy(rgFddFlds.pclFddBLT2," ");
     }
  }

  strcpy(rgFddFlds.pclFddGD1B," ");
  strcpy(rgFddFlds.pclFddGD2B," ");
  /* Remp is already set from updateFlight */
  strcpy(rgFddFlds.pclFddAPC3,FIELDVAL(rgAftArray,pclCurRowAFT,igAftORG3));
  strcpy(rgFddFlds.pclFddVIA3,FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIA3));
  strcpy(rgFddFlds.pclFddINDO,"I");
  strcpy(rgFddFlds.pclFddFTYP,pcpFtyp);
  TimeToStr(rgFddFlds.pclFddLSTU,time(NULL)); 
  TimeToStr(rgFddFlds.pclFddCDAT,time(NULL));
  strcpy(rgFddFlds.pclFddUSEU,"FLDHDL");
  strcpy(rgFddFlds.pclFddUSEC,"FLDHDL");
  strcpy(rgFddFlds.pclFddHOPO,pcgHomeAp);
  if (igCheckNatureLength == 1)
  {
     strncpy(pclTTYP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP),2); 
     pclTTYP[2] = '\0';
  }
  else
  {
     strcpy(pclTTYP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP)); 
  }
  TrimRight(pclTTYP);
  SetDNAT(pclTTYP);
  strcpy(pclAftTTYP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP));

  strcpy(pclAftJFNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftJFNO));
  TrimRight(pclAftJFNO);
  if (strcmp(pclAftJFNO," ") == 0)
  {
     strcpy(rgFddFlds.pclFddDSEQ,"0");
  }

  /************** Main-Airline INDO =I *************************/

  ilRC = StructAddRowPartFDDArr(plpRowNumFDD); /* Hier checken */

  /* UpdateFLDTAB(pclCurRowAFT); */

  
  /************** Main-Airline INDO =D *************************/

  if (igIntAndDomDisplays != 0)
  {
/*
     ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
     }
*/
 
     strcpy(rgFddFlds.pclFddINDO,"D");
     if (strcmp(pcgHdlDACO,"STEV") != 0)
     {
        strcpy(rgFddFlds.pclFddDACO,"2");
        strcpy(rgFddFlds.pclFddREMP,pclOldRemark);
        strcpy(rgFddFlds.pclFddBLT1,pclSavBLT1);
        strcpy(rgFddFlds.pclFddBLT2,pclSavBLT2);
        if (ilTerm == 2)
           ilRC = StructAddRowPartFDDArr(plpRowNumFDD); /* Hier checken */
     }
     else
        ilRC = StructAddRowPartFDDArr(plpRowNumFDD); /* Hier checken */
  }
  /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
  /*ilRC = ShowFddStruct();*/


  /***************** prepare Data and handle Code-Share Flights (INDO=I)***************/

  strcpy(pclAftJFNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftJFNO));
  strncpy(rgFddFlds.pclFddFLNO,pclAftJFNO,9);
  rgFddFlds.pclFddFLNO[9] = '\0'; 
  TrimRight(rgFddFlds.pclFddFLNO);

  for (ilStart = 9; strlen(rgFddFlds.pclFddFLNO) > 3; ilStart += 9)
  {
     ilMainCshCnt++; /* 2-? are the CodeShareFlights */
     sprintf(rgFddFlds.pclFddDSEQ,"%d",ilMainCshCnt);
/*
     ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
     }
*/
     /*********** here INDO = I ******************/

     strcpy(rgFddFlds.pclFddINDO,"I");
     if (strcmp(pcgHdlDACO,"STEV") != 0)
     {
        strcpy(rgFddFlds.pclFddDACO,"1");
        if (strcmp(pclNewRemark,"TM2") == 0)
        {
           strcpy(rgFddFlds.pclFddREMP,pclNewRemark);
           strcpy(rgFddFlds.pclFddBLT1," ");
           strcpy(rgFddFlds.pclFddBLT2," ");
        }
     }

     ilRC = StructAddRowPartFDDArr(plpRowNumFDD); /* Hier checken */
     /* dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
     /*ilRC = ShowFddStruct();*/

     /*********** here INDO = D ******************/

     if (igIntAndDomDisplays != 0)
     {
/*
        ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
        if (ilRC != RC_SUCCESS)
        {
           dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
        }
*/

        strcpy(rgFddFlds.pclFddINDO,"D");
        if (strcmp(pcgHdlDACO,"STEV") != 0)
        {
           strcpy(rgFddFlds.pclFddDACO,"2");
           strcpy(rgFddFlds.pclFddREMP,pclOldRemark);
           strcpy(rgFddFlds.pclFddBLT1,pclSavBLT1);
           strcpy(rgFddFlds.pclFddBLT2,pclSavBLT2);
           if (ilTerm == 2)
              ilRC = StructAddRowPartFDDArr(plpRowNumFDD); /* Hier checken */
        }
        else 
           ilRC = StructAddRowPartFDDArr(plpRowNumFDD); /* Hier checken */
     }
     /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
     /*ilRC = ShowFddStruct();*/

     strncpy(rgFddFlds.pclFddFLNO,&pclAftJFNO[ilStart],9);
     rgFddFlds.pclFddFLNO[9] = '\0';
     TrimRight(rgFddFlds.pclFddFLNO);
  }

  dbg(DEBUG,"%05d: ================== End of HandleFLTI_Int_A !!!  ===================",__LINE__);
  return (ilRC);
}/*end of HandleFLTI_Int_A */

/******************************************************************************/
/* The HandleFLTI_Dom_A routine                                                    */
/******************************************************************************/
static int HandleFLTI_Dom_A(char *pclCurRowAFT, char *pclCurRowFDD, long *plpRowNumFDD)
{


  int ilRC = RC_SUCCESS;
  int ilStart = 0;
  char pclBuffer[1024];
  char pclRowBuffer[4096] = "\0";
  long llRowNumFdd = 0;
  char *pclRowFDD = NULL;
  int ilMainCshCnt = 0;
  char pclCDA[10] = "\0";
  char pclFddREMP_ORG[FDD_LEN_S];
  char pclAftJFNO[128] = "\0";
   char pclAltURNO[128] = "\0";
   char pclAftTTYP[128] = "\0";
  char pclCKIF[128] = "\0";
  char pclCKIT[128] = "\0";
  char pclTTYP[128] = "\0";
  char pclOldRemark[128] = "\0";
  char pclNewRemark[128] = "\0";
  char pclSavBLT1[128] = "\0";
  char pclSavBLT2[128] = "\0";
  int ilTerm; 

  dbg(DEBUG,"%05d: ================== This is HandleFLTI_Dom_A !!!  ===================",__LINE__);
  ilMainCshCnt = 0;


  /***************** Prepare Data for first FDD-Record ****************/
  ilMainCshCnt++;
  sprintf(rgFddFlds.pclFddDSEQ,"%d",ilMainCshCnt);
  strcpy(rgFddFlds.pclFddNODE,"\0");
  strcpy(rgFddFlds.pclFddAURN,FIELDVAL(rgAftArray,pclCurRowAFT,igAftURNO)); 
  strcpy(rgFddFlds.pclFddFLNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftFLNO));
  strcpy(rgFddFlds.pclFddTIFF,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTIFA));
  strcpy(rgFddFlds.pclFddSTOF,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTOA));
  strcpy(rgFddFlds.pclFddETOF,FIELDVAL(rgAftArray,pclCurRowAFT,igAftETOA));
  strcpy(rgFddFlds.pclFddLAND,FIELDVAL(rgAftArray,pclCurRowAFT,igAftLAND));
  strcpy(rgFddFlds.pclFddONBL,FIELDVAL(rgAftArray,pclCurRowAFT,igAftONBL));
  strcpy(rgFddFlds.pclFddWRO1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftWRO1));
  /*  strcpy(rgFddFlds.pclFddBLT1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT1));*/
  /*  strcpy(rgFddFlds.pclFddBLT2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT2));*/
  strcpy(rgFddFlds.pclFddBLT1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT1));
  strcpy(rgFddFlds.pclFddBLT2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT2));
  strcpy(rgFddFlds.pclFddGTD1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD1));    
  strcpy(rgFddFlds.pclFddOBL1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT1));
  strcpy(rgFddFlds.pclFddOBL2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT2));
  strcpy(rgFddFlds.pclFddOGT1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD1));
  strcpy(rgFddFlds.pclFddOGT2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD2));
  if (strcmp(pcgHdlDACO,"STEV") == 0)
  {
     strcpy(rgFddFlds.pclFddDACO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTEV));
  }
  else
  {
     strcpy(rgFddFlds.pclFddDACO,"2");
  }

  strcpy(rgFddFlds.pclFddGD1B," ");
  strcpy(rgFddFlds.pclFddGD2B," ");
  /* Remp is already set from updateFlight */
  strcpy(rgFddFlds.pclFddAPC3,FIELDVAL(rgAftArray,pclCurRowAFT,igAftORG3));
  strcpy(rgFddFlds.pclFddVIA3,FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIA3));
  strcpy(rgFddFlds.pclFddINDO,"D");
  strcpy(rgFddFlds.pclFddFTYP,"D");
  TimeToStr(rgFddFlds.pclFddLSTU,time(NULL)); 
  TimeToStr(rgFddFlds.pclFddCDAT,time(NULL));
  strcpy(rgFddFlds.pclFddUSEU,"FLDHDL");
  strcpy(rgFddFlds.pclFddUSEC,"FLDHDL");
  strcpy(rgFddFlds.pclFddHOPO,pcgHomeAp);
  if (igCheckNatureLength == 1)
  {
     strncpy(pclTTYP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP),2); 
     pclTTYP[2] = '\0';
  }
  else
  {
     strcpy(pclTTYP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP));
  }
  TrimRight(pclTTYP);
  SetDNAT(pclTTYP);


  strcpy(pclAftTTYP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP));


  strcpy(pclAftJFNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftJFNO));
  TrimRight(pclAftJFNO);
  if (strcmp(pclAftJFNO," ") == 0)
    {
      strcpy(rgFddFlds.pclFddDSEQ,"0");
    }

  /********* save REMP ******************/
 
  strcpy(pclFddREMP_ORG,rgFddFlds.pclFddREMP);

   /************** Main-Airline INDO =D *************************/
 
  if (igIntAndDomDisplays != 0)
  {
     if (strcmp(pcgHdlDACO,"STEV") != 0)
     {
        strcpy(pclOldRemark,FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP));
        strcpy(pclNewRemark,pclOldRemark);
        ilRC = HandleTerminal_A(pclCurRowAFT,pclNewRemark,&ilTerm);
        strcpy(pclSavBLT1,rgFddFlds.pclFddBLT1);
        strcpy(pclSavBLT2,rgFddFlds.pclFddBLT2);
        if (ilTerm == 2)
           ilRC = StructAddRowPartFDDArr(plpRowNumFDD); /* Hier checken */
     }
     else
        ilRC = StructAddRowPartFDDArr(plpRowNumFDD);
     /* UpdateFLDTAB(pclCurRowAFT); */
  }
  
  /************** Main-Airline INDO =I *************************/

/* 
  ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
  if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
    }
*/

  if (strcmp(pcgHdlDACO,"STEV") == 0)
  {
     strcpy(rgFddFlds.pclFddDACO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTEV));
     if (igGenerateTM2 == 1)
     {
        if (strncmp(rgFddFlds.pclFddREMP,pcgCanceled,3) != 0)
        {
           strcpy(rgFddFlds.pclFddREMP,"TM2");
        }
     }
  }
  else
  {
     strcpy(rgFddFlds.pclFddDACO,"1");
     if (strcmp(pclNewRemark,"TM2") == 0)
     {
        strcpy(rgFddFlds.pclFddREMP,pclNewRemark);
        strcpy(rgFddFlds.pclFddBLT1," ");
        strcpy(rgFddFlds.pclFddBLT2," ");
     }
  }
  strcpy(rgFddFlds.pclFddINDO,"I");

  ilRC = StructAddRowPartFDDArr(plpRowNumFDD);
  /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
  /*ilRC = ShowFddStruct();*/

  strcpy(rgFddFlds.pclFddREMP,pclFddREMP_ORG);

  /***************** prepare Data and handle Code-Share Flights (INDO=I)***************/

  strcpy(pclAftJFNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftJFNO));
  strncpy(rgFddFlds.pclFddFLNO,pclAftJFNO,9);
  rgFddFlds.pclFddFLNO[9] = '\0'; 
  TrimRight(rgFddFlds.pclFddFLNO);

  for (ilStart = 9; strlen(rgFddFlds.pclFddFLNO) > 3; ilStart += 9)
    {
  
      ilMainCshCnt++;
      sprintf(rgFddFlds.pclFddDSEQ,"%d",ilMainCshCnt);
/*
      ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
      if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
    }
*/
      /*********** here INDO = D ******************/
  
      strcpy(rgFddFlds.pclFddINDO,"D");

 
      if (igIntAndDomDisplays != 0)
      {
         if (strcmp(pcgHdlDACO,"STEV") != 0)
         {
            strcpy(rgFddFlds.pclFddDACO,"2");
            strcpy(rgFddFlds.pclFddREMP,pclOldRemark);
            strcpy(rgFddFlds.pclFddBLT1,pclSavBLT1);
            strcpy(rgFddFlds.pclFddBLT2,pclSavBLT2);
            if (ilTerm == 2)
               ilRC = StructAddRowPartFDDArr(plpRowNumFDD); /* Hier checken */
         }
         else
            ilRC = StructAddRowPartFDDArr(plpRowNumFDD);
         /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
         /*ilRC = ShowFddStruct();*/
      }


      /*********** here INDO = I ******************/
/*
      ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
      if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
    }
*/

      strcpy(rgFddFlds.pclFddINDO,"I");
      if (strcmp(pcgHdlDACO,"STEV") == 0)
      {
         if (igGenerateTM2 == 1)
         {
            if (strncmp(rgFddFlds.pclFddREMP,pcgCanceled,3) != 0)
            {
                strcpy(rgFddFlds.pclFddREMP,"TM2");
            }
         }
      }
      else
      {
         strcpy(rgFddFlds.pclFddDACO,"1");
         if (strcmp(pclNewRemark,"TM2") == 0)
         {
            strcpy(rgFddFlds.pclFddREMP,pclNewRemark);
            strcpy(rgFddFlds.pclFddBLT1," ");
            strcpy(rgFddFlds.pclFddBLT2," ");
         }
      }

      ilRC = StructAddRowPartFDDArr(plpRowNumFDD);
      /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
      /*ilRC = ShowFddStruct();*/

      strcpy(rgFddFlds.pclFddREMP,pclFddREMP_ORG);

      strncpy(rgFddFlds.pclFddFLNO,&pclAftJFNO[ilStart],9);
      rgFddFlds.pclFddFLNO[9] = '\0'; 
      TrimRight(rgFddFlds.pclFddFLNO);
    }

  dbg(DEBUG,"%05d: ================== End of  HandleFLTI_Dom_A !!!  ===================",__LINE__);
  return (ilRC);
}/*end of HandleFLTI_Dom_A */


/******************************************************************************/
/* The HandleFLTI_Mix_A routine                                                    */
/******************************************************************************/
static int HandleFLTI_Mix_A(char *pclCurRowAFT, char *pclCurRowFDD, long *plpRowNumFDD)
{

  int ilRC = RC_SUCCESS;
  int ilStart = 0;
  char pclBuffer[2048];
  char pclFddREMP_ORG[FDD_LEN_S] = "\0"; 
  char pclAftJFNO[128] = "\0";
  char pclCDA[10] = "\0";
  int ilMainCshCnt = 0;
  char pclAltURNO[128] = "\0";
  char pclCKIF[128] = "\0";
  char pclCKIT[128] = "\0";
   char pclAftTTYP[128] = "\0";
    int ilFlag = 0;
  char pclTTYP[128] = "\0";
  char pclLastTTYP[128] = "\0";
  dbg(DEBUG,"%05d: ================== This is HandleFLTI_Mix_A !!!  ===================",__LINE__);
  ilMainCshCnt = 0; 
  /********************************************* this is the international Part *******************************/

  /***************** Prepare Data for first FDD-Record ****************/
  ilMainCshCnt++;
  sprintf(rgFddFlds.pclFddDSEQ,"%d",ilMainCshCnt);
  strcpy(rgFddFlds.pclFddNODE,"\0");
  strcpy(rgFddFlds.pclFddAURN,FIELDVAL(rgAftArray,pclCurRowAFT,igAftURNO)); 
  strcpy(rgFddFlds.pclFddFLNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftFLNO));
  strcpy(rgFddFlds.pclFddTIFF,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTIFA));
  strcpy(rgFddFlds.pclFddSTOF,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTOA));
  strcpy(rgFddFlds.pclFddETOF,FIELDVAL(rgAftArray,pclCurRowAFT,igAftETOA));
  strcpy(rgFddFlds.pclFddLAND,FIELDVAL(rgAftArray,pclCurRowAFT,igAftLAND));
  strcpy(rgFddFlds.pclFddONBL,FIELDVAL(rgAftArray,pclCurRowAFT,igAftONBL));
  strcpy(rgFddFlds.pclFddWRO1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftWRO1));
  strcpy(rgFddFlds.pclFddBLT1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT1));
  strcpy(rgFddFlds.pclFddBLT2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT2));
  strcpy(rgFddFlds.pclFddGTD1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD1));
  strcpy(rgFddFlds.pclFddOBL1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT1));
  strcpy(rgFddFlds.pclFddOBL2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT2));
  strcpy(rgFddFlds.pclFddOGT1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD1));
  strcpy(rgFddFlds.pclFddOGT2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD2));
  if (strcmp(pcgHdlDACO,"STEV") == 0)
  {
     strcpy(rgFddFlds.pclFddDACO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTEV));
  }
  else
  {
     strcpy(rgFddFlds.pclFddDACO,"1");
  }

  strcpy(rgFddFlds.pclFddGD1B," ");
  strcpy(rgFddFlds.pclFddGD2B," ");
  /* Remp is already set from updateFlight */
  strcpy(rgFddFlds.pclFddAPC3,FIELDVAL(rgAftArray,pclCurRowAFT,igAftORG3));
  strcpy(rgFddFlds.pclFddVIA3,FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIA3));
  strcpy(rgFddFlds.pclFddINDO,"I");
  strcpy(rgFddFlds.pclFddFTYP,"I");
  TimeToStr(rgFddFlds.pclFddLSTU,time(NULL)); 
  TimeToStr(rgFddFlds.pclFddCDAT,time(NULL));
  strcpy(rgFddFlds.pclFddUSEU,"FLDHDL");
  strcpy(rgFddFlds.pclFddUSEC,"FLDHDL");
  strcpy(rgFddFlds.pclFddHOPO,pcgHomeAp);
  if (igCheckNatureLength == 1)
  {
    strncpy(pclTTYP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP),2);
    pclTTYP[2] = '\0';
  }
  else
  {
    strcpy(pclTTYP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP));
  }
  TrimRight(pclTTYP);
  SetDNAT(pclTTYP);

  strcpy(pclAftTTYP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP));

  strcpy(pclAftJFNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftJFNO));
  TrimRight(pclAftJFNO);
  if (strcmp(pclAftJFNO," ") == 0)
    {
      strcpy(rgFddFlds.pclFddDSEQ,"0");
    }


  /********* save REMP ******************/
 
  strcpy(pclFddREMP_ORG,rgFddFlds.pclFddREMP);



  /************** Main-Airline INDO =I *************************/


  strcpy(rgFddFlds.pclFddCKIF,pclCKIF);
  strcpy(rgFddFlds.pclFddCKIT,pclCKIT);
  /**************************** end Get Counters */

  /**************** lookup config-file to see if flight should be displayed ************/
  ilFlag = DisplayFlight (rgFddFlds.pclFddINDO, rgFddFlds.pclFddFTYP, pclAftTTYP, ilMainCshCnt);

  if(ilFlag == 1)
    {
      ilRC = SetDomVia(rgFddFlds.pclFddADID, rgFddFlds.pclFddFTYP, NumDomVia(pclAftTTYP), pclCurRowAFT);
      ilRC = StructAddRowPartFDDArr(plpRowNumFDD);
      /* UpdateFLDTAB(pclCurRowAFT); */
    }



  
  /************** Main-Airline INDO =D *************************/
/*
  ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
  if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
    }
*/

  strcpy(rgFddFlds.pclFddINDO,"D");
  /**************** lookup config-file to see if flight should be displayed ************/
  ilFlag = DisplayFlight (rgFddFlds.pclFddINDO, rgFddFlds.pclFddFTYP, pclAftTTYP, ilMainCshCnt);

  if(ilFlag == 1)
    {
      ilRC = SetDomVia(rgFddFlds.pclFddADID, rgFddFlds.pclFddFTYP, NumDomVia(pclAftTTYP), pclCurRowAFT);
      if (igIntAndDomDisplays != 0)
      {
         ilRC = StructAddRowPartFDDArr(plpRowNumFDD);
      }
    }


  /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
  /*ilRC = ShowFddStruct();*/



  /***************** prepare Data and handle Code-Share Flights (INDO=I)***************/

  strcpy(pclAftJFNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftJFNO));
  strncpy(rgFddFlds.pclFddFLNO,pclAftJFNO,9);
  rgFddFlds.pclFddFLNO[9] = '\0';
  TrimRight(rgFddFlds.pclFddFLNO);

  for (ilStart = 9; strlen(rgFddFlds.pclFddFLNO) > 3; ilStart += 9)
    {
 
      ilMainCshCnt++;
      sprintf(rgFddFlds.pclFddDSEQ,"%d",ilMainCshCnt);
/*
      ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
      if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
    }
*/
      /*********** here INDO = I ******************/
      strcpy(rgFddFlds.pclFddINDO,"I");

      /**************** lookup config-file to see if flight should be displayed ************/
      ilFlag = DisplayFlight (rgFddFlds.pclFddINDO, rgFddFlds.pclFddFTYP, pclAftTTYP, ilMainCshCnt);

      if(ilFlag == 1)
    {
      ilRC = SetDomVia(rgFddFlds.pclFddADID, rgFddFlds.pclFddFTYP, NumDomVia(pclAftTTYP), pclCurRowAFT);
      ilRC = StructAddRowPartFDDArr(plpRowNumFDD);
    }


      /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
      /*ilRC = ShowFddStruct();*/


      /*********** here INDO = D ******************/

/*
      ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
      if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
    }
*/
      strcpy(rgFddFlds.pclFddINDO,"D");

      /**************** lookup config-file to see if flight should be displayed ************/
      ilFlag = DisplayFlight (rgFddFlds.pclFddINDO, rgFddFlds.pclFddFTYP, pclAftTTYP, ilMainCshCnt);

      if(ilFlag == 1)
    {
      ilRC = SetDomVia(rgFddFlds.pclFddADID, rgFddFlds.pclFddFTYP, NumDomVia(pclAftTTYP), pclCurRowAFT);
      if (igIntAndDomDisplays != 0)
      {
         ilRC = StructAddRowPartFDDArr(plpRowNumFDD);
      }
    }

      /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
      /*ilRC = ShowFddStruct();*/

      strncpy(rgFddFlds.pclFddFLNO,&pclAftJFNO[ilStart],9);
      rgFddFlds.pclFddFLNO[9] = '\0';
      TrimRight(rgFddFlds.pclFddFLNO);
    }


  /************************ hier war return 0 und alles lief noch *******************/



  /********************************************** this is the domestic part ************************************************/

  /************** Main-Airline INDO =D *************************/
  ilMainCshCnt = 1;
  TrimRight(pclAftJFNO);
  if (strcmp(pclAftJFNO," ") == 0)
    {
      strcpy(rgFddFlds.pclFddDSEQ,"0");
    }
  else
    {
      sprintf(rgFddFlds.pclFddDSEQ,"%d",ilMainCshCnt);
    }

/*
  ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
  if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
    }
*/

  strcpy(rgFddFlds.pclFddREMP,pclFddREMP_ORG);
  strcpy(rgFddFlds.pclFddBLT1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT2));
  strcpy(rgFddFlds.pclFddGTD1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD2));    
  strcpy(pclBuffer,"\0");
  strcpy(pclBuffer,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP));
  if (igCheckNatureLength == 1)
  {
     strncpy(pclTTYP,&pclBuffer[2],2);
     pclTTYP[2] = '\0';
  }
  else
  {
     strcpy(pclTTYP,pclBuffer);
  }
  TrimRight(pclTTYP);
  SetDNAT(pclTTYP);

 
  strcpy(pclBuffer,"\0");
  strcpy(pclBuffer,FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIAL));
  strncpy(rgFddFlds.pclFddAPC3,&pclBuffer[1],3); /*set APC3 to 1. VIA*/
  rgFddFlds.pclFddAPC3[3] = '\0';

  strcpy(rgFddFlds.pclFddVIA3," ");

  strcpy(rgFddFlds.pclFddFLNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftFLNO));

  strcpy(rgFddFlds.pclFddINDO,"D");
  strcpy(rgFddFlds.pclFddFTYP,"D");
  /**************** lookup config-file to see if flight should be displayed ************/
  ilFlag = DisplayFlight (rgFddFlds.pclFddINDO, rgFddFlds.pclFddFTYP, pclAftTTYP, ilMainCshCnt);

  if(ilFlag == 1)
    {
      ilRC = SetDomVia(rgFddFlds.pclFddADID, rgFddFlds.pclFddFTYP, NumDomVia(pclAftTTYP), pclCurRowAFT);
      if (igIntAndDomDisplays != 0)
      {
         ilRC = StructAddRowPartFDDArr(plpRowNumFDD);
      }
    }
 
  /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
  /*ilRC = ShowFddStruct();*/


  
  /************** Main-Airline INDO =I *************************/
  if (igGenerateTM2 == 1)
  {
  if (strncmp(pclFddREMP_ORG,pcgCanceled,3) != 0)
    {
      strcpy(rgFddFlds.pclFddREMP,"TM2");
    }
  }
  
/*
  ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
  if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
    }
*/

  strcpy(rgFddFlds.pclFddINDO,"I");
  strcpy(rgFddFlds.pclFddFTYP,"D");
  /**************** lookup config-file to see if flight should be displayed ************/
  ilFlag = DisplayFlight (rgFddFlds.pclFddINDO, rgFddFlds.pclFddFTYP, pclAftTTYP, ilMainCshCnt);

  if(ilFlag == 1)
    {
      ilRC = SetDomVia(rgFddFlds.pclFddADID, rgFddFlds.pclFddFTYP, NumDomVia(pclAftTTYP), pclCurRowAFT);
      ilRC = StructAddRowPartFDDArr(plpRowNumFDD);
    }

  /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
  /*ilRC = ShowFddStruct();*/

  strcpy(rgFddFlds.pclFddREMP,pclFddREMP_ORG);


  /***************** prepare Data and handle Code-Share Flights (INDO=I)***************/

  strcpy(pclAftJFNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftJFNO));
  strncpy(rgFddFlds.pclFddFLNO,pclAftJFNO,9);
  rgFddFlds.pclFddFLNO[9] = '\0';
  TrimRight(rgFddFlds.pclFddFLNO);

  for (ilStart = 9; strlen(rgFddFlds.pclFddFLNO) > 3; ilStart += 9)
    {
      dbg(DEBUG,"%05d: The CODE_SHARE-FLNO <%s>",__LINE__,rgFddFlds.pclFddFLNO);
      ilMainCshCnt++;
      sprintf(rgFddFlds.pclFddDSEQ,"%d",ilMainCshCnt);
/*
      ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
      if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
    }
*/
      /*********** here INDO = D ******************/
      strcpy(rgFddFlds.pclFddREMP,pclFddREMP_ORG); 
      strcpy(rgFddFlds.pclFddINDO,"D");
      strcpy(rgFddFlds.pclFddFTYP,"D");
      /**************** lookup config-file to see if flight should be displayed ************/
      ilFlag = DisplayFlight (rgFddFlds.pclFddINDO, rgFddFlds.pclFddFTYP, pclAftTTYP, ilMainCshCnt);

      if(ilFlag == 1)
    {
      ilRC = SetDomVia(rgFddFlds.pclFddADID, rgFddFlds.pclFddFTYP, NumDomVia(pclAftTTYP), pclCurRowAFT);
      if (igIntAndDomDisplays != 0)
      {
         ilRC = StructAddRowPartFDDArr(plpRowNumFDD);
      }
    }


      /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
      /*ilRC = ShowFddStruct();*/


      /*********** here INDO = I ******************/
  if (igGenerateTM2 == 1)
  {
      if (strncmp(pclFddREMP_ORG,pcgCanceled,3) != 0)
    {
      strcpy(rgFddFlds.pclFddREMP,"TM2");
    }
  }
     
/* 
      ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
      if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
    }
*/
      strcpy(rgFddFlds.pclFddINDO,"I");
      strcpy(rgFddFlds.pclFddFTYP,"D");
      /**************** lookup config-file to see if flight should be displayed ************/
      ilFlag = DisplayFlight (rgFddFlds.pclFddINDO, rgFddFlds.pclFddFTYP, pclAftTTYP, ilMainCshCnt);

      if(ilFlag == 1)
    {
      ilRC = SetDomVia(rgFddFlds.pclFddADID, rgFddFlds.pclFddFTYP, NumDomVia(pclAftTTYP), pclCurRowAFT);
      ilRC = StructAddRowPartFDDArr(plpRowNumFDD);
    }

 
      /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
      /*ilRC = ShowFddStruct();*/

      strcpy(rgFddFlds.pclFddREMP, pclFddREMP_ORG);

      strncpy(rgFddFlds.pclFddFLNO,&pclAftJFNO[ilStart],9);
      rgFddFlds.pclFddFLNO[9] = '\0';
      TrimRight(rgFddFlds.pclFddFLNO);
    }

  dbg(DEBUG,"%05d: ================== End of HandleFLTI_Mix_A !!!  ===================",__LINE__);

  return (ilRC);
}/*end of HandleFLTI_Mix_A */






/******************************************************************************/
/*  The SaveIndexInfo routine                                                 */
/*****************************************************************************/
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo)
{
  int    ilRC       = RC_SUCCESS ;
  long   llRow      = 0 ;
  FILE  *prlFile    = NULL ;
  char   clDel      = ',' ;
  char   clFile[512] ;
  char  *pclTrimBuf = NULL ;
    long llRowCount;
    int ilCount = 0;
  char  *pclTestBuf = NULL ;


  dbg (DEBUG,"SaveIndexInfo: SetArrayInfo <%s>",prpArrayInfo->crArrayName) ;

  if (ilRC == RC_SUCCESS)
  {
   sprintf (&clFile[0],"%s/%s_%s.info",getenv("DBG_PATH"),mod_name,prpArrayInfo->crArrayName) ;

   errno = 0 ;
   prlFile = fopen (&clFile[0], "w") ;
   if (prlFile == NULL)
   {
    dbg (DEBUG, "SaveIndexInfo: fopen <%s> failed <%d-%s>", &clFile[0], errno, strerror(errno)) ;
    ilRC = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if (ilRC == RC_SUCCESS)
  {
   {
    dbg (DEBUG, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ;      
    dbg (DEBUG, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ;  
    dbg (DEBUG, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ;  
   }

   fprintf (prlFile, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ; fflush(prlFile) ;
   fprintf (prlFile, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ; fflush(prlFile) ;
   fprintf (prlFile, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ; fflush(prlFile) ;

   {
    dbg(DEBUG,"IndexData") ; 
   }
   fprintf(prlFile,"IndexData\n"); fflush(prlFile);

    dbg(DEBUG,"FindKey <%s>",prpArrayInfo->pcrIdx01RowBuf);
    *prpArrayInfo->pcrIdx01RowBuf = '\0';
   llRow = ARR_FIRST ;
   do
   {
    ilRC = CEDAArrayFindKey (&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), &(prpArrayInfo->rrIdx01Handle), prpArrayInfo->crIdx01Name, &llRow, prpArrayInfo->lrIdx01RowLen, prpArrayInfo->pcrIdx01RowBuf);
    if (ilRC == RC_SUCCESS)
    {
    
      dbg(DEBUG, "SaveIndexInfo: Key <%ld> <%s>",llRow,prpArrayInfo->pcrIdx01RowBuf) ;
    

     if (strlen (prpArrayInfo->pcrIdx01RowBuf) == 0)
     {
      ilRC = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
      if(ilRC == RC_SUCCESS)
      {
        dbg (DEBUG, "SaveIndexInfo: Row <%ld> <%s>", llRow,prpArrayInfo->pcrArrayRowBuf) ;
      } /* end of if */
     } /* end of if */

     pclTrimBuf = strdup (prpArrayInfo->pcrIdx01RowBuf) ;
     if (pclTrimBuf != NULL)
     {
      ilRC = GetDataItem (pclTrimBuf,prpArrayInfo->pcrIdx01RowBuf, 1, clDel, "", "  ") ;
      if (ilRC > 0)
      {
       ilRC = RC_SUCCESS ;
       fprintf (prlFile,"Key            <%ld><%s>\n",llRow,pclTrimBuf) ; fflush(prlFile) ;
      } /* end of if */

      free (pclTrimBuf) ;
      pclTrimBuf = NULL ;
     } /* end of if */

     llRow = ARR_NEXT;
    }
    else
    {
     dbg (DEBUG, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRC) ;
    } /* end of if */
   } while (ilRC == RC_SUCCESS) ;

   {
    dbg (DEBUG,"ArrayData") ;
   }
   fprintf (prlFile,"ArrayData\n"); fflush(prlFile);

   llRow = ARR_FIRST ;

     CEDAArrayGetRowCount(&(prpArrayInfo->rrArrayHandle),&(prpArrayInfo->crArrayName[0]),&llRowCount);

    llRow = ARR_FIRST;
    dbg(DEBUG,"Array  <%s> data follows Rows %ld",
            prpArrayInfo->crArrayName,llRowCount);
    do
    {
            ilRC = CEDAArrayGetRowPointer(&(prpArrayInfo->rrArrayHandle),
            &(prpArrayInfo->crArrayName[0]),llRow,(void *)&pclTestBuf);
            if (ilRC == RC_SUCCESS)
            {
                for( ilCount = 0; ilCount < prpArrayInfo->lrArrayFieldCnt; ilCount++)
                {
                
                    fprintf (prlFile,"<%s>",&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]) ; 
                    fflush(prlFile) ;
                    dbg (DEBUG, "SaveIndexInfo: ilCount <%ld> <%s>", ilCount,&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]) ;
                }
                fprintf (prlFile,"\n") ; 
                dbg (DEBUG, "\n") ;
                fflush(prlFile) ;
                llRow = ARR_NEXT;
            }
            else
            {
                dbg(DEBUG,"GetRowPointer failed RC=%d",ilRC);
            }
    } while (ilRC == RC_SUCCESS);

   if (ilRC == RC_NOTFOUND)
   {
    ilRC = RC_SUCCESS ;
   } /* end of if */
  } /* end of if */

  if (prlFile != NULL)
  {
   fclose(prlFile) ;
   prlFile = NULL ;
  
    dbg (DEBUG, "SaveIndexInfo: event saved - file <%s>", &clFile[0]) ;
  
  } /* end of if */

  return (ilRC) ;

}/* end of SaveIndexInfo */

/******************************************************************************/
/* The Handle_Dparture routine                                               */
/******************************************************************************/
static int Handle_Departure(char *pclRowAFT, long llRowNumAFT, long *llRowNumFDD)
{
  int ilRC = RC_SUCCESS;
  int ilCurrent = 0;
  /* long llRowNumAFT = ARR_FIRST;*/
  /* long llRowNumFDD = ARR_FIRST;*/
  /* char *pclRowAFT = NULL;*/
  char *pclRowFDD = NULL;
  char *pclUpdSelLst = NULL;
  char *pclUpdFldLst = NULL;
  char *pclUpdDatLst = NULL;
  char *pclUpdCmdLst = NULL;
  char pclBuffer[128];
  char pclSqlBuf[1024];
  char pclData[1024];
  short slSqlFunc = 0;
  short slCursor = 0;

  char pclAftJFNO[128] = "\0";

      dbg(DEBUG,"Handle_Departure");

      /************ now check Flight-Nature ***************/
      ilRC = CheckFlightNature(pclRowAFT);
    

      /************ now update Departure-Flight-Record ***************/ 
      if (ilRC == RC_SUCCESS) /*that is: if FlightNature is valid*/
    {
    
      /************* add  Row to FDDTAB *****************************/
      ilRC = InitFddStruct();            /* initialize the FDDSTRUCT */
/*
      ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
      if (ilRC != RC_SUCCESS)
        {
          dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
        }
*/

      strcpy(rgFddFlds.pclFddAURN,"0");
      strcpy(rgFddFlds.pclFddADID,"D");




      
    
      ilRC = CEDAArrayGetRowPointer(&rgFddArray.rrArrayHandle,
                    rgFddArray.crArrayName,
                    *llRowNumFDD,(void *)&pclRowFDD);
    
      /*
      dbg(DEBUG,"%05d:  FDD-Array Row added pclRowFDD: <%s> llRowNumFDD: %d",__LINE__,
          pclRowFDD,*llRowNumFDD);
    
      dbg(DEBUG,"%05d: pclRowAFT:<%s>, pclRowFDD:<%s>",__LINE__,*pclRowAFT,pclRowFDD);
      */



      ilRC = UpdateFlight_D (pclRowAFT,*llRowNumFDD);

     
    
      /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
      /*ilRC = ShowFddStruct();*/
    }

      /************* now HandleFLTI_A  ******************/
      if (ilRC == RC_SUCCESS)
    {
    
      ilRC = HandleFLTI_D(pclRowAFT,pclRowFDD,llRowNumFDD);
    
       
    }
  
      /*dbg(TRACE,"%05d: llRowNumFdd: %d",__LINE__,*llRowNumFDD);*/
      /*dbg(TRACE,"%05d: llRowNumAFT: %d",__LINE__,llRowNumAFT);*/





  return (ilRC);
} /*end of Handle_Departure*/
   





/******************************************************************************/
/* The UpdateFlight_D routine                                                 */
/******************************************************************************/
static int UpdateFlight_D(char *pclCurRowAFT, long llRowNumFDD)
{
  int ilRC =RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSelection[1024];
  char pclDataBuf[1024];
  char pclSelUpd[1024];
  char pclSqlBuf[1024] = "\0";
  char pclBufURNO[128] = "\0";
  char pclBufAIRB[128] = "\0";
  char pclBufOFBL[128] = "\0";
  char pclBufGD1Y[128] = "\0";
  char pclBufGD1X[128] = "\0";
  char pclBufGD2Y[128] = "\0";
  char pclBufGD2X[128] = "\0";
  char pclBufGTD2[128] = "\0";
  char pclBufETOD[128] = "\0";
  char pclBufSTOD[128] = "\0";
  char pclBufNXTI[128] = "\0";
  char pclBufRGD1[128] = "\0";
  char pclBufRGD2[128] = "\0";
  char pclAftREMP[128] = "\0";
  char pclAftFTYP[128] = "\0";
  char pclAftADID[128] = "\0";
  char pclNewREMP[128] = "\0";
  char pclNewRMTI[128] = "\0";
  char pclTmpTime[128] = "\0";
  char pclBoaRem[128] = "\0";
  int ilDoUpdate = 0;
  char pclTime[16];
  char pclFldLst[128];
  char pclDatLst[128];
  char pclGateRemarks[1024];
  char pclAftSTEV[128] = "\0";
  char pclNewSTEV[128] = "\0";

  dbg(DEBUG,"====================== This is UpdateFlight_D Begin ====================");
 
  /********* get FTYP of Current Row ********/
  strcpy(pclAftFTYP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftFTYP));
  TrimRight(pclAftFTYP);

  /********* get REMP of Current Row ********/
  strcpy(pclAftREMP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP));
  TrimRight(pclAftREMP);

  /********* get ADID of Current Row ********/
  strcpy(pclAftADID,FIELDVAL(rgAftArray,pclCurRowAFT,igAftADID));
  TrimRight(pclAftADID);

  /********* get URNO of CurrentRow *********/
  strcpy(pclBufURNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftURNO));
  TrimRight(pclBufURNO);

  /********* get AIRB of Current Row ********/
  strcpy(pclBufAIRB,FIELDVAL(rgAftArray,pclCurRowAFT,igAftAIRB));
  TrimRight(pclBufAIRB);

  /********* get ONBL of Current Row ********/
  strcpy(pclBufOFBL,FIELDVAL(rgAftArray,pclCurRowAFT,igAftOFBL));
  TrimRight(pclBufOFBL);

  /********* get GD1Y of CurrentRow *********/
  strcpy(pclBufGD1Y,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGD1Y));
  TrimRight(pclBufGD1Y);

  /********* get GD1X of CurrentRow *********/
  strcpy(pclBufGD1X,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGD1X));
  TrimRight(pclBufGD1X);

  /********* get GD2Y of CurrentRow *********/
  strcpy(pclBufGD2Y,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGD2Y));
  TrimRight(pclBufGD2Y);

  /********* get GD2X of CurrentRow *********/
  strcpy(pclBufGD2X,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGD2X));
  TrimRight(pclBufGD2X);

  /********* get GTD2 of CurrentRow *********/
  strcpy(pclBufGTD2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD2));
  TrimRight(pclBufGTD2);

  /********* get ETOD of CurrentRow *********/
  strcpy(pclBufETOD,FIELDVAL(rgAftArray,pclCurRowAFT,igAftETOD));
  TrimRight(pclBufETOD);
 
  /********* get STOD of CurrentRow *********/
  strcpy(pclBufSTOD,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTOD));
  TrimRight(pclBufSTOD);

  /********* get NXTI of CurrentRow *********/
  strcpy(pclBufNXTI,FIELDVAL(rgAftArray,pclCurRowAFT,igAftNXTI));
  TrimRight(pclBufNXTI);
 
  /********* get STEV of Current Row ********/
  strcpy(pclAftSTEV,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTEV));
  TrimRight(pclAftSTEV);

  if (strcmp(pcgUseGateStatusFields,"YES") == 0)
  {
     /********* get RGD1 of CurrentRow *********/
     strcpy(pclBufRGD1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftRGD1));
     TrimRight(pclBufRGD1);
     if (*pclBufRGD1 == '0')
        *pclBufRGD1 = ' ';

     /********* get RGD2 of CurrentRow *********/
     strcpy(pclBufRGD2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftRGD2));
     TrimRight(pclBufRGD2);
     if (*pclBufRGD2 == '0')
        *pclBufRGD2 = ' ';
  }
 
  strcpy(pclNewREMP,pclAftREMP);
  strcpy(pclNewRMTI," ");
  ilDoUpdate = 0;
  sprintf(pclGateRemarks,"%s,%s,%s,%s,%s",pcgGateOpened,pcgDelayedGateOpen,pcgBoarding,pcgFinalcall,pcgGateclosed);

  if (strcmp(pclBufAIRB," ") != 0 && strcmp(pcgAirborne,"EMPTY") != 0)
  {
     if (strcmp(pclAftREMP,pcgAirborne) != 0)
     {
        ilDoUpdate = 1;
        strcpy(pclNewREMP,pcgAirborne);
     }
  }
  else
  {
     if (strcmp(pclBufOFBL," ") != 0 && strcmp(pcgDeparted,"EMPTY") != 0)
     {
        if (strcmp(pclAftREMP,pcgDeparted) != 0)
        {
           ilDoUpdate = 1;
           strcpy(pclNewREMP,pcgDeparted);
        }
     }
     else
     {
        if (strcmp(pclAftREMP,pcgAirborne) == 0 || strcmp(pclAftREMP,pcgDeparted) == 0)
        {
           if (strcmp(pcgUseGateStatusFields,"YES") == 0)
           {
              if (strcmp(pclBufRGD1,pcgGateOpened) == 0 && strcmp(pclAftREMP,pcgDelayedGateOpen) == 0)
              {
              }
              else
              {
                 if (*pclBufRGD1 != ' ' || (*pclBufRGD1 == ' ' && strstr(pclGateRemarks,pclAftREMP) != NULL))
                 {
                    ilDoUpdate = 1;
                    strcpy(pclNewREMP,pclBufRGD1);
                 }
              }
           }
           else
           {
              if (pclBufGD1Y[0] != cBLANK)
              {
                 ilDoUpdate = 1;
                 strcpy(pclNewREMP,pcgGateclosed);
              }
              else
              {
                 if (pclBufGD1X[0] != cBLANK)
                 {
                    ilDoUpdate = 1;
                    strcpy(pclNewREMP,pcgBoarding);
                 }
                 else
                 {
                    ilDoUpdate = 1;
                    strcpy(pclNewREMP," ");
                 }
              }
           }
        }
        if (strcmp(pcgUseGateStatusFields,"YES") == 0)
        {
           if (strcmp(pclAftREMP,pclBufRGD1) != 0)
           {
              if (strcmp(pclBufRGD1,pcgGateOpened) == 0 && strcmp(pclAftREMP,pcgDelayedGateOpen) == 0)
              {
              }
              else
              {
                 if (*pclBufRGD1 != ' ' || (*pclBufRGD1 == ' ' && strstr(pclGateRemarks,pclAftREMP) != NULL))
                 {
                    ilDoUpdate = 1;
                    strcpy(pclNewREMP,pclBufRGD1);
                 }
              }
           }
        }
        if (pclBufETOD[0] != cBLANK)
        {
           strcpy(pclTmpTime,pclBufSTOD);
           TrimRight(pclTmpTime);
           AddSecondsToCEDATime12(pclTmpTime,igDelayTimeDiffDep,1);
           if (strcmp(pclBufETOD,pclTmpTime) > 0)  /* ETOD > STOD ? */
           {
              if (strcmp(pclAftREMP,pcgGateOpened) == 0)
              {
                 ilDoUpdate = 1;
                 strcpy(pclNewREMP,pcgDelayedGateOpen);
              }
              else
              {
                 if (strcmp(pclAftREMP,pcgCanceled) == 0 ||
                     strcmp(pclAftREMP,pcgDelay) == 0 ||
                     strstr(pcgUnknownDelayRemark,pclAftREMP) != NULL ||
                     strcmp(pclAftREMP,pcgNextinfo) == 0 ||
                     strcmp(pclAftREMP,pcgNewGate) == 0 ||
                     pclAftREMP[0] == cBLANK)
                 {
                    ilDoUpdate = 1;
                    strcpy(pclNewREMP,pcgDelayed);
                 }
              }
           }
           else
           {
              if (strcmp(pclAftREMP,pcgDelayedGateOpen) == 0)
              {
                 ilDoUpdate = 1;
                 strcpy(pclNewREMP,pcgGateOpened);
              }
              else
              {
                 if (strcmp(pclAftREMP,pcgDelayed) == 0 ||
                     strcmp(pclAftREMP,pcgNextinfo) == 0)
                 {
                    ilDoUpdate = 1;
                    strcpy(pclNewREMP," ");
                 }
              }
           }
           if (ilDoUpdate == 0)
           {
              strcpy(pclNewREMP,pclAftREMP);
           }
        }
        else
        {
           if (strcmp(pclAftREMP,pcgDelayedGateOpen) == 0)
           {
              ilDoUpdate = 1;
              strcpy(pclNewREMP,pcgGateOpened);
           }
           else
           {
              if (strcmp(pclBufNXTI," ") != 0)
              {
                 if (strcmp(pclAftREMP,pcgCanceled) == 0 ||
                     strcmp(pclAftREMP,pcgDelayed) == 0 ||
                     pclAftREMP[0] == cBLANK)
                 {
                    if (igAutoNextInfo == TRUE)
                    {
                       ilDoUpdate = 1;
                       strcpy(pclNewREMP,pcgNextinfo);
                    }
                    else
                    {
                       ilDoUpdate = 1;
                       strcpy(pclNewREMP," ");
                    }
                 }
              }
              else
              {
                 if (strcmp(pclAftREMP,pcgDelayed) == 0 ||
                     strcmp(pclAftREMP,pcgNextinfo) == 0)
                 {
                    ilDoUpdate = 1;
                    strcpy(pclNewREMP," ");
                 }
              }
           }
        }
     }
  }

  if (pclAftFTYP[0] == 'X')
  {
     ilDoUpdate = 0;
     strcpy(pclNewREMP,pcgCanceled);
     if (strcmp(pclAftREMP,pcgCanceled) != 0)
     {
        ilDoUpdate = 1;
     }
  }
  else
  {
     if (strcmp(pclAftREMP,pcgCanceled) == 0)
     {
        ilDoUpdate = 1;
        strcpy(pclNewREMP," ");
     }
  }

  if (igUpdateRemarksAFT == 0 || pclAftADID[0] == 'B')
  {
     strcpy(pclNewREMP,pclAftREMP);
     ilDoUpdate = 0;
  }

  if (strcmp(pclNewREMP,"EMPTY") == 0 || strcmp(pclNewREMP,pclAftREMP) == 0)
  {
     strcpy(pclNewREMP,pclAftREMP);
     ilDoUpdate = 0;
  }

  if (strcmp(pcgUseGateStatusFields,"YES") == 0)
  {
     if (strcmp(pclNewREMP," ") == 0 && strcmp(pclBufRGD1," ") != 0)
     {
        strcpy(pclAftREMP,pclNewREMP);
        ilDoUpdate = 0;
     }
  }
 
  sprintf(pclSqlBuf,"WHERE URNO = %s",pclBufURNO);
  if (ilDoUpdate == 1)
  {
     ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","REMP",pcgTwEnd,pclSqlBuf,
                      "REMP",pclNewREMP,NULL,0);
  }

  if (strcmp(pcgCloseGate,"YES") == 0 && (*pclBufAIRB != ' ' || *pclBufOFBL != ' '))
  {
     strcpy(pclFldLst,"");
     strcpy(pclDatLst,"");
     TimeToStr(pclTime,time(NULL));
     if (*pclBufGD1X != ' ' && *pclBufGD1Y == ' ' && *pclBufGD2X != ' ' && *pclBufGD2Y == ' ')
     {
        strcpy(pclFldLst,"GD1Y,GD2Y");
        sprintf(pclDatLst,"%s,%s",pclTime,pclTime);
     }
     else if (*pclBufGD1X != ' ' && *pclBufGD1Y == ' ')
     {
        strcpy(pclFldLst,"GD1Y");
        strcpy(pclDatLst,pclTime);
     }
     else if (*pclBufGD2X != ' ' && *pclBufGD2Y == ' ')
     {
        strcpy(pclFldLst,"GD2Y");
        strcpy(pclDatLst,pclTime);
     }
     if (strlen(pclFldLst) > 0)
        ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","CLOSE GATE",pcgTwEnd,pclSqlBuf,
                         pclFldLst,pclDatLst,NULL,0);
  }
  if (strcmp(pcgCloseCounter,"YES") == 0 && (*pclBufAIRB != ' ' || *pclBufOFBL != ' '))
  {
     TimeToStr(pclTime,time(NULL));
     sprintf(pclSelection,
             "SELECT URNO FROM CCATAB WHERE FLNU = %s AND CKBA <> ' ' AND CKEA = ' '",
             pclBufURNO);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"Sql: <%s>",pclSelection);
     ilRCdb = sql_if(slFkt,&slCursor,pclSelection,pclDataBuf);
     while (ilRCdb == DB_SUCCESS)
     {
        sprintf(pclSelUpd,"WHERE URNO = %s",pclDataBuf);
        ilRC = SendEvent("URT",506,PRIORITY_3,"CCA","CLOSE CIC",pcgTwEnd,pclSelUpd,
                         "CKEA",pclTime,NULL,0);
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,pclSelection,pclDataBuf);
     }
     close_my_cursor(&slCursor);
  }

  strcpy(pclFldLst,"");
  strcpy(pclDatLst,"");  
  if (igUpdateRemarksAFT == 1)
  {
     /*Insert here BOA or GCL time*/
     if (strcmp(pclNewREMP,pcgGateclosed) == 0)
     {
        if (pclBufGD1Y[0] == cBLANK)
        {
           if (strcmp(pcgSetBoardingRemarkWithGd1x,"YES") == 0)
           {
/*
              ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","REMP",pcgTwEnd,pclSqlBuf,
                               "REMP",pcgBoarding,NULL,0);
*/
              strcat(pclFldLst,"REMP,");
              strcat(pclDatLst,pcgBoarding);
              strcat(pclDatLst,",");
           }
           else
           {
              TimeToStr(pclBufGD1Y,time(NULL));
/*
              ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","GD1Y",pcgTwEnd,pclSqlBuf,
                               "GD1Y",pclBufGD1Y,NULL,0);
*/
              strcat(pclFldLst,"GD1Y,");
              strcat(pclDatLst,pclBufGD1Y);
              strcat(pclDatLst,",");
           }
        }
        if (strcmp(pcgUseGateStatusFields,"YES") != 0 && strcmp(pcgHandleGate2,"YES") == 0 && *pclBufGTD2 != cBLANK)
        {
           if (pclBufGD2Y[0] == cBLANK)
           {
              TimeToStr(pclBufGD2Y,time(NULL));
/*
              ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","GD2Y",pcgTwEnd,pclSqlBuf,
                               "GD2Y",pclBufGD2Y,NULL,0);
*/
              strcat(pclFldLst,"GD2Y,");
              strcat(pclDatLst,pclBufGD2Y);
              strcat(pclDatLst,",");
           }
        }
     }
     else
     {
        sprintf(pclBoaRem,"%s,%s",pcgBoarding,pcgFinalcall);
        if (strcmp(pcgSetGd1xWithBoa,"NO") == 0)
        {
           strcat(pclBoaRem,",");
           strcat(pclBoaRem,pcgGateOpened);
           strcat(pclBoaRem,",");
           strcat(pclBoaRem,pcgDelayedGateOpen);
        }
        if (strstr(pclBoaRem,pclNewREMP) != NULL)
        {
           if (pclBufGD1X[0] == cBLANK)
           {
              if (strcmp(pcgSetBoardingRemarkWithGd1x,"YES") == 0)
              {
/*
                 ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","REMP",pcgTwEnd,pclSqlBuf,
                                  "REMP"," ",NULL,0);
*/
                 strcat(pclFldLst,"REMP,");
                 strcat(pclDatLst," ,");
              }
              else
              {
                 TimeToStr(pclBufGD1X,time(NULL));
/*
                 ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","GD1X",pcgTwEnd,pclSqlBuf,
                                  "GD1X",pclBufGD1X,NULL,0);
*/
                 strcat(pclFldLst,"GD1X,");
                 strcat(pclDatLst,pclBufGD1X);
                 strcat(pclDatLst,",");
              }
           }
           if (pclBufGD1Y[0] != cBLANK)
           {
              if (strcmp(pcgSetBoardingRemarkWithGd1x,"YES") == 0)
              {
/*
                 ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","REMP",pcgTwEnd,pclSqlBuf,
                                  "REMP",pcgGateclosed,NULL,0);
*/
                 strcat(pclFldLst,"REMP,");
                 strcat(pclDatLst,pcgGateclosed);
                 strcat(pclDatLst,",");
              }
              else
              {
                 strcpy(pclBufGD1Y," ");
/*
                 ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","GD1Y",pcgTwEnd,pclSqlBuf,
                                  "GD1Y",pclBufGD1Y,NULL,0);
*/
                 strcat(pclFldLst,"GD1Y,");
                 strcat(pclDatLst,pclBufGD1Y);
                 strcat(pclDatLst,",");
              }
           }
           if (strcmp(pcgUseGateStatusFields,"YES") != 0 && strcmp(pcgHandleGate2,"YES") == 0 && *pclBufGTD2 != cBLANK)
           {
              if (pclBufGD2X[0] == cBLANK)
              {
                 TimeToStr(pclBufGD2X,time(NULL));
/*
                 ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","GD2X",pcgTwEnd,pclSqlBuf,
                                  "GD2X",pclBufGD2X,NULL,0);
*/
                 strcat(pclFldLst,"GD2X,");
                 strcat(pclDatLst,pclBufGD2X);
                 strcat(pclDatLst,",");
              }
              if (pclBufGD2Y[0] != cBLANK)
              {
                 strcpy(pclBufGD2Y," ");
/*
                 ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","GD2Y",pcgTwEnd,pclSqlBuf,
                                  "GD2Y",pclBufGD2Y,NULL,0);
*/
                 strcat(pclFldLst,"GD2Y,");
                 strcat(pclDatLst,pclBufGD2Y);
                 strcat(pclDatLst,",");
              }
           }
        }
        else
        {
           if (strcmp(pclNewREMP,pcgAirborne) != 0 && strcmp(pclNewREMP,pcgDeparted) != 0)
           {
              if (pclBufGD1X[0] != cBLANK && pclBufGD1Y[0] == cBLANK)
              {
                 if (strcmp(pcgSetBoardingRemarkWithGd1x,"YES") == 0)
                 {
/*
                    ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","REMP",pcgTwEnd,pclSqlBuf,
                                     "REMP",pcgBoarding,NULL,0);
*/
                    strcat(pclFldLst,"REMP,");
                    strcat(pclDatLst,pcgBoarding);
                    strcat(pclDatLst,",");
                 }
                 else
                 {
                    strcpy(pclBufGD1X," ");
/*
                    ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","GD1X",pcgTwEnd,pclSqlBuf,
                                     "GD1X",pclBufGD1X,NULL,0);
*/
                    strcat(pclFldLst,"GD1X,");
                    strcat(pclDatLst,pclBufGD1X);
                    strcat(pclDatLst,",");
                 }
              }
              if (pclBufGD1Y[0] != cBLANK)
              {
                 if (strcmp(pcgSetBoardingRemarkWithGd1x,"YES") == 0)
                 {
/*
                    ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","REMP",pcgTwEnd,pclSqlBuf,
                                     "REMP",pcgGateclosed,NULL,0);
*/
                    strcat(pclFldLst,"REMP,");
                    strcat(pclDatLst,pcgGateclosed);
                    strcat(pclDatLst,",");
                 }
                 else
                 {
                    strcpy(pclBufGD1Y," ");
/*
                    ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","GD1Y",pcgTwEnd,pclSqlBuf,
                                     "GD1Y",pclBufGD1Y,NULL,0);
*/
                    strcat(pclFldLst,"GD1Y,");
                    strcat(pclDatLst,pclBufGD1Y);
                    strcat(pclDatLst,",");
                 }
              }
              if (strcmp(pcgUseGateStatusFields,"YES") != 0 && strcmp(pcgHandleGate2,"YES") == 0 && *pclBufGTD2 != cBLANK)
              {
                 if (pclBufGD2X[0] != cBLANK)
                 {
                    strcpy(pclBufGD2X," ");
/*
                    ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","GD2X",pcgTwEnd,pclSqlBuf,
                                     "GD2X",pclBufGD2X,NULL,0);
*/
                    strcat(pclFldLst,"GD2X,");
                    strcat(pclDatLst,pclBufGD2X);
                    strcat(pclDatLst,",");
                 }
                 if (pclBufGD2Y[0] != cBLANK)
                 {
                    strcpy(pclBufGD2Y," ");
/*
                    ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","GD2Y",pcgTwEnd,pclSqlBuf,
                                     "GD2Y",pclBufGD1Y,NULL,0);
*/
                    strcat(pclFldLst,"GD2Y,");
                    strcat(pclDatLst,pclBufGD2Y);
                    strcat(pclDatLst,",");
                 }
              }
           }
        }
     }
     if (strcmp(pcgUseGateStatusFields,"YES") == 0)
     {
        if (strcmp(pclBufRGD2,pcgGateclosed) == 0)
        {
           if (pclBufGD2Y[0] == cBLANK)
           {
              if (strcmp(pcgSetBoardingRemarkWithGd1x,"YES") == 0)
              {
                 strcat(pclFldLst,"RGD2,");
                 strcat(pclDatLst,pcgBoarding);
                 strcat(pclDatLst,",");
              }
              else
              {
                 TimeToStr(pclBufGD2Y,time(NULL));
                 strcat(pclFldLst,"GD2Y,");
                 strcat(pclDatLst,pclBufGD2Y);
                 strcat(pclDatLst,",");
              }
           }
        }
        else
        {
           sprintf(pclBoaRem,"%s,%s",pcgBoarding,pcgFinalcall);
           if (strcmp(pcgSetGd1xWithBoa,"NO") == 0)
           {
              strcat(pclBoaRem,",");
              strcat(pclBoaRem,pcgGateOpened);
              strcat(pclBoaRem,",");
              strcat(pclBoaRem,pcgDelayedGateOpen);
           }
           if (strcmp(pclNewREMP,pcgAirborne) != 0 && strcmp(pclNewREMP,pcgDeparted) != 0 &&
               strstr(pclBoaRem,pclBufRGD2) != NULL)
           {
              if (pclBufGD2X[0] == cBLANK)
              {
                 if (strcmp(pcgSetBoardingRemarkWithGd1x,"YES") == 0)
                 {
                    strcat(pclFldLst,"RGD2,");
                    strcat(pclDatLst," ,");
                 }
                 else
                 {
                    TimeToStr(pclBufGD2X,time(NULL));
                    strcat(pclFldLst,"GD2X,");
                    strcat(pclDatLst,pclBufGD2X);
                    strcat(pclDatLst,",");
                 }
              }
              if (pclBufGD2Y[0] != cBLANK)
              {
                 if (strcmp(pcgSetBoardingRemarkWithGd1x,"YES") == 0)
                 {
                    strcat(pclFldLst,"RGD2,");
                    strcat(pclDatLst,pcgGateclosed);
                    strcat(pclDatLst,",");
                 }
                 else
                 {
                    strcpy(pclBufGD2Y," ");
                    strcat(pclFldLst,"GD2Y,");
                    strcat(pclDatLst,pclBufGD2Y);
                    strcat(pclDatLst,",");
                 }
              }
           }
           else
           {
              if (*pclBufRGD2 == ' ')
              {
                 if (pclBufGD2X[0] != cBLANK)
                 {
                    if (strcmp(pcgSetBoardingRemarkWithGd1x,"YES") == 0)
                    {
                       strcat(pclFldLst,"RGD2,");
                       strcat(pclDatLst,pcgBoarding);
                       strcat(pclDatLst,",");
                    }
                    else
                    {
                       TimeToStr(pclBufGD2X,time(NULL));
                       strcat(pclFldLst,"GD2X,");
                       strcat(pclDatLst," ,");
                    }
                 }
                 if (pclBufGD2Y[0] != cBLANK)
                 {
                    if (strcmp(pcgSetBoardingRemarkWithGd1x,"YES") == 0)
                    {
                       strcat(pclFldLst,"RGD2,");
                       strcat(pclDatLst,pcgGateclosed);
                       strcat(pclDatLst,",");
                    }
                    else
                    {
                       strcpy(pclBufGD2Y," ");
                       strcat(pclFldLst,"GD2Y,");
                       strcat(pclDatLst,pclBufGD2Y);
                       strcat(pclDatLst,",");
                    }
                 }
              }
           }
        }
     }
  }
  if (strlen(pclFldLst) > 0)
  {
     pclFldLst[strlen(pclFldLst)-1] = '\0';
     pclDatLst[strlen(pclDatLst)-1] = '\0';
     ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","fldhdl",pcgTwEnd,pclSqlBuf,
                      pclFldLst,pclDatLst,NULL,0);
  }
  if (strcmp(pclNewREMP,pcgAirborne) == 0)
  {
     strcpy(pclNewRMTI,pclBufAIRB);
  }
  else
  {
     if (strcmp(pclNewREMP,pcgDeparted) == 0)
     {
        strcpy(pclNewRMTI,pclBufOFBL);
     }
     else
     {
        if (strcmp(pclNewREMP,pcgGateclosed) == 0)
        {
           strcpy(pclNewRMTI,pclBufGD1Y);
        }
        else
        {
           if (strcmp(pclNewREMP,pcgBoarding) == 0 || strcmp(pclNewREMP,pcgFinalcall) == 0)
           {
              strcpy(pclNewRMTI,pclBufGD1X);
           }
           else
           {
              if (strstr(pcgListOfETDRem,pclNewREMP) != NULL)
              {
                 strcpy(pclNewRMTI,pclBufETOD);
              }
              else
              {
                 if (strcmp(pclNewREMP,pcgNextinfo) == 0)
                 {
                    strcpy(pclNewRMTI,pclBufNXTI);
                 }
                 else
                 {
                    strcpy(pclNewRMTI," ");
                 }
              }
           }
        }
     }
  }
  strcpy(rgFddFlds.pclFddREMP,pclNewREMP);
  strcpy(rgFddFlds.pclFddRMTI,pclNewRMTI);

  if (igGenerateStevFromCounter == TRUE)
  {
     ilRC = GenerateStevFromCounter(pclCurRowAFT,pclNewSTEV);
     if (*pclNewSTEV != ' ' && *pclNewSTEV != *pclAftSTEV)
     {
        ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","fldhdl",pcgTwEnd,pclSqlBuf,
                         "STEV",pclNewSTEV,NULL,0);
     }
  }

  dbg(DEBUG,"======================  This is UpdateFlight_D End   ====================");
  return (ilRC);

} /* end of UpdateFlight_D */




/******************************************************************************/
/* The HandleFLTI_D routine                                                    */
/******************************************************************************/
static int HandleFLTI_D (char *pclCurRowAFT, char *pclCurRowFDD,long *plpRowNumFDD)
{
  int ilRC = RC_SUCCESS;
  char pclBuf[128];

  /*********** get FLTI of CurrentRow****************/
  strcpy(pclBuf,FIELDVAL(rgAftArray,pclCurRowAFT,igAftFLTI));
  TrimRight(pclBuf);
  /********** handle international departures **********/
  if (strstr(pcgIntFLTI,pclBuf) != NULL )       
  {
     ilRC = HandleFLTI_Int_D(pclCurRowAFT,pclCurRowFDD,plpRowNumFDD,"I"); 
     if (ilRC != RC_SUCCESS)
        dbg(TRACE,"%05d: HandleFLTI_Int_D failed !!",__LINE__);
  }
  /********** handle domestic departures **********/
  if (strstr(pcgDomFLTI,pclBuf) != NULL )       
  {
     ilRC = HandleFLTI_Dom_D(pclCurRowAFT,pclCurRowFDD,plpRowNumFDD); 
     if (ilRC != RC_SUCCESS)
        dbg(TRACE,"%05d: HandleFLTI_Dom_D failed !!",__LINE__);
  }
  /********** handle mixed departures **********/
  if (strstr(pcgMixedFLTI,pclBuf) != NULL )       
  {
     if (strcmp(pcgSimpleHandlingOfMixedFlights,"NO") == 0)
        ilRC = HandleFLTI_Mix_D(pclCurRowAFT,pclCurRowFDD,plpRowNumFDD);
     else
        ilRC = HandleFLTI_Int_D(pclCurRowAFT,pclCurRowFDD,plpRowNumFDD,"M"); 
     if (ilRC != RC_SUCCESS)
        dbg(TRACE,"%05d: HandleFLTI_Mix_D failed !!",__LINE__);
  }

  return (ilRC);
} /*end of HandleFLTI_D */



/******************************************************************************/
/* The HandleFLTI_Mix_D routine                                                    */
/******************************************************************************/
static int HandleFLTI_Mix_D(char *pclCurRowAFT, char *pclCurRowFDD, long *plpRowNumFDD)
{

  int ilRC = RC_SUCCESS;
  int ilStart = 0;
  char pclBuffer[2048];
  int ilFlag = 0;

  long llRowNumFdd = 0;
  char *pclRowFDD = NULL;
  int ilMainCshCnt = 0;
  char pclCDA[10] = "\0";
  char pclFddREMP_ORG[16] = "\0"; 
  char pclAltURNO[128] = "\0";
  char pclCKIF[128] = "\0";
  char pclCKIT[128] = "\0";
  char pclTTYP[128] = "\0";
  char pclAftJFNO[128] = "\0";
  char pclAftTTYP[128] = "\0"; 
  char pclALC[128] = "\0";
  char pclFLNO[128] = "\0";
  char pclAftGD1B[128] = "\0";
  char pclAftGD2B[128] = "\0";
  char pclAftGD1X[128] = "\0";
  char pclAftGD2X[128] = "\0";
  char pclDest[128] = "\0";
  char pclFddAurn[128] = "\0";
  char pclFddChaf[128] = "\0";
  char pclFddChat[128] = "\0";
  
  dbg(DEBUG,"%05d: ================== This is HandleFLTI_Mix_D !!!  ===================",__LINE__);

  ilMainCshCnt = 0;
  /********************************************* this is the international Part *******************************/

  /***************** Prepare Data for first FDD-Record ****************/
  ilMainCshCnt++;


  strcpy(rgFddFlds.pclFddNODE,"\0");
  strcpy(rgFddFlds.pclFddAURN,FIELDVAL(rgAftArray,pclCurRowAFT,igAftURNO)); 
  strcpy(pclFddAurn,FIELDVAL(rgAftArray,pclCurRowAFT,igAftURNO)); 
  strcpy(rgFddFlds.pclFddFLNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftFLNO));
  strcpy(pclALC,FIELDVAL(rgAftArray,pclCurRowAFT,igAftFLNO));
  pclALC[3] = '\0';
  strcpy(pclFLNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftFLNO));
  TrimRight(pclFLNO);
  strcpy(rgFddFlds.pclFddTIFF,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTIFD));
  strcpy(rgFddFlds.pclFddSTOF,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTOD));
  strcpy(rgFddFlds.pclFddETOF,FIELDVAL(rgAftArray,pclCurRowAFT,igAftETOD));
  strcpy(rgFddFlds.pclFddAIRB,FIELDVAL(rgAftArray,pclCurRowAFT,igAftAIRB));
  strcpy(rgFddFlds.pclFddOFBL,FIELDVAL(rgAftArray,pclCurRowAFT,igAftOFBL));
  strcpy(rgFddFlds.pclFddBLT1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT1));
  strcpy(rgFddFlds.pclFddWRO1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftWRO1));

  /*strcpy(rgFddFlds.pclFddBLT2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT2));*/

  strcpy(rgFddFlds.pclFddGTD1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD1));
  strcpy(rgFddFlds.pclFddGTD2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD2));
  strcpy(rgFddFlds.pclFddOBL1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT1));
  strcpy(rgFddFlds.pclFddOBL2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT2));
  strcpy(rgFddFlds.pclFddOGT1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD1));
  strcpy(rgFddFlds.pclFddOGT2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD2));
  if (strcmp(pcgHdlDACO,"STEV") == 0)
  {
     strcpy(rgFddFlds.pclFddDACO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTEV));
  }
  else
  {
     strcpy(rgFddFlds.pclFddDACO,"1");
  }

  strcpy(pclAftGD1B,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBOAO));
  TrimRight(pclAftGD1B);
  strcpy(pclAftGD1X,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGD1X));
  TrimRight(pclAftGD1X);
  if (strlen(pclAftGD1X) > 1)
     strcpy(rgFddFlds.pclFddGD1B,pclAftGD1X);
  else
     strcpy(rgFddFlds.pclFddGD1B,pclAftGD1B);
  strcpy(pclAftGD2B,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBOAC));
  TrimRight(pclAftGD2B);
  strcpy(pclAftGD2X,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGD2X));
  TrimRight(pclAftGD2X);
  if (strlen(pclAftGD2X) > 1)
     strcpy(rgFddFlds.pclFddGD2B,pclAftGD2X);
  else
     strcpy(rgFddFlds.pclFddGD2B,pclAftGD2B);

  /* Remp is already set from updateFlight */
  strcpy(rgFddFlds.pclFddAPC3,FIELDVAL(rgAftArray,pclCurRowAFT,igAftDES3));
  strcpy(pclDest,FIELDVAL(rgAftArray,pclCurRowAFT,igAftDES3));
  strcpy(rgFddFlds.pclFddVIA3,FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIA3));
  strcpy(rgFddFlds.pclFddINDO,"I");
  strcpy(rgFddFlds.pclFddFTYP,"I");
  TimeToStr(rgFddFlds.pclFddLSTU,time(NULL)); 
  TimeToStr(rgFddFlds.pclFddCDAT,time(NULL));
  strcpy(rgFddFlds.pclFddUSEU,"FLDHDL");
  strcpy(rgFddFlds.pclFddUSEC,"FLDHDL");
  strcpy(rgFddFlds.pclFddHOPO,pcgHomeAp);
  if (igCheckNatureLength == 1)
  {
    strncpy(pclTTYP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP),2);
    pclTTYP[2] = '\0';
  }
  else
  {
    strcpy(pclTTYP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP));
  }
  TrimRight(pclTTYP);
  SetDNAT(pclTTYP);

  strcpy(pclAftTTYP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP));

  strcpy(pclAftJFNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftJFNO));
  TrimRight(pclAftJFNO);
  if (strcmp(pclAftJFNO," ") == 0)
  {
     strcpy(rgFddFlds.pclFddDSEQ,"0");
  }
  else
  {
     sprintf(rgFddFlds.pclFddDSEQ,"%d",ilMainCshCnt);
  }

  /********* save REMP ******************/
  strcpy(pclFddREMP_ORG, rgFddFlds.pclFddREMP);

  /************** Main-Airline INDO =I *************************/
      
  /************************* Get Chutes */
  ilRC = GetChutes(pclFLNO,pclFddAurn,pclFddChaf,pclFddChat);
  strcpy(rgFddFlds.pclFddBLT1,pclFddChaf);
  strcpy(rgFddFlds.pclFddBLT2,pclFddChat);

  /************************* Get Counters */

  ilRC = GetCDT(pclCDA, pclTTYP, ilMainCshCnt);
 
  if (strcmp(pcgCheckTermForCntAsgn,"NO") == 0)
  {
     if (strcmp(pclCDA,"D") == 0)
     {
        ilRC = GetDedicatedCounter("I",pclFLNO,pclCKIF,pclCKIT);
     }
     else
     {
        if (strcmp(pclCDA,"C") == 0)
        {
           ilRC = GetCommonCounter("I",pclALC,pclCKIF,pclCKIT,pclDest);
        }
        else
        {
           if (strcmp(pclCDA,"A") == 0 || strcmp(pclCDA,"X") == 0 || strcmp(pclCDA,"Y") == 0)
           {
              ilRC = GetCommonAndDedicatedCounter("I",pclALC,pclFLNO,pclCKIF,pclCKIT,pclCDA,"M",pclDest);
           }
        }
     }
  }
  else
  {
     ilRC = GetCounterTerm(pclCDA,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTEV),
                           pclALC,pclFLNO,pclCKIF,pclCKIT);
  }
  strcpy(rgFddFlds.pclFddCKIF,pclCKIF);
  strcpy(rgFddFlds.pclFddCKIT,pclCKIT);
  /**************************** end Get Counters */

  /**************** lookup config-file to see if flight should be displayed ************/
  ilFlag = DisplayFlight (rgFddFlds.pclFddINDO, rgFddFlds.pclFddFTYP, pclAftTTYP, ilMainCshCnt);

  if(ilFlag == 1)
  {
     ilRC = SetDomVia(rgFddFlds.pclFddADID, rgFddFlds.pclFddFTYP, NumDomVia(pclAftTTYP), pclCurRowAFT);
     ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
  }

  /************** Main-Airline INDO =D *************************/
/*
  ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
  if (ilRC != RC_SUCCESS)
  {
     dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
  }
*/
  strcpy(rgFddFlds.pclFddINDO,"D");

  /**************** lookup config-file to see if flight should be displayed ************/
  ilFlag = DisplayFlight (rgFddFlds.pclFddINDO, rgFddFlds.pclFddFTYP, pclAftTTYP, ilMainCshCnt);

  if(ilFlag == 1)
  { 
     ilRC = SetDomVia(rgFddFlds.pclFddADID, rgFddFlds.pclFddFTYP, NumDomVia(pclAftTTYP), pclCurRowAFT);
     if (igIntAndDomDisplays != 0)
     {
        ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
     }
  }

  /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
  /*ilRC = ShowFddStruct();*/

  /***************** prepare Data and handle Code-Share Flights (INDO=I)***************/

  strcpy(pclAftJFNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftJFNO));
  strncpy(rgFddFlds.pclFddFLNO,pclAftJFNO,9);
  rgFddFlds.pclFddFLNO[9] = '\0';
  TrimRight(rgFddFlds.pclFddFLNO);

  for (ilStart = 9; strlen(rgFddFlds.pclFddFLNO) > 3; ilStart += 9)
  {
     strncpy(pclALC,rgFddFlds.pclFddFLNO,3);
     pclALC[3] = '\0';
/*
     strcpy(pclFLNO,rgFddFlds.pclFddFLNO);
     TrimRight(pclFLNO);
*/
     ilMainCshCnt++;  
     sprintf(rgFddFlds.pclFddDSEQ,"%d",ilMainCshCnt);
/*
     ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
     }
*/
     /*********** here INDO = I ******************/
     strcpy(rgFddFlds.pclFddINDO,"I");

     /************************* Get Counters */

     ilRC = GetCDT(pclCDA, pclTTYP, ilMainCshCnt);

     if (strcmp(pcgCheckTermForCntAsgn,"NO") == 0)
     {
        if (strcmp(pclCDA,"D") == 0)
        {
           ilRC = GetDedicatedCounter("I",pclFLNO,pclCKIF,pclCKIT);
        }
        else
        {
           if (strcmp(pclCDA,"C") == 0)
           {
              ilRC = GetCommonCounter("I",pclALC,pclCKIF,pclCKIT,pclDest);
           }
           else
           {
              if (strcmp(pclCDA,"A") == 0 || strcmp(pclCDA,"X") == 0 || strcmp(pclCDA,"Y") == 0)
              {
                 ilRC = GetCommonAndDedicatedCounter("I",pclALC,pclFLNO,pclCKIF,pclCKIT,pclCDA,"J",pclDest);
              }
              else
              {
                 if (strcmp(pclCDA,"S") == 0)
                 {
                    strcpy(pclCKIF,pcgCKIF);
                    strcpy(pclCKIT,pcgCKIT);
                 }
                 else
                 {
                    if (strcmp(pclCDA,"P") == 0)
                    {
                       strcpy(pclCKIF,pcgCKIF_C);
                       strcpy(pclCKIT,pcgCKIT_C);
                    }
                    else
                    {
                       if (strcmp(pclCDA,"R") == 0)
                       {
                          strcpy(pclCKIF,pcgCKIF_D);
                          strcpy(pclCKIT,pcgCKIT_D);
                       }
                       else
                       {
                          strcpy(pclCKIF," ");
                          strcpy(pclCKIT," ");
                       }
                    }
                 }
              }
           }
        }
     }
     else
     {
        ilRC = GetCounterTerm(pclCDA,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTEV),
                              pclALC,pclFLNO,pclCKIF,pclCKIT);
     }
     strcpy(rgFddFlds.pclFddCKIF,pclCKIF);
     strcpy(rgFddFlds.pclFddCKIT,pclCKIT);
     /**************************** end Get Counters */

     /**************** lookup config-file to see if flight should be displayed ************/
     ilFlag = DisplayFlight (rgFddFlds.pclFddINDO, rgFddFlds.pclFddFTYP, pclAftTTYP, ilMainCshCnt);
     if(ilFlag == 1)
     {
        ilRC = SetDomVia(rgFddFlds.pclFddADID, rgFddFlds.pclFddFTYP, NumDomVia(pclAftTTYP), pclCurRowAFT);
        ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
     }

     /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
     /*ilRC = ShowFddStruct();*/

     /*********** here INDO = D ******************/
/*
     ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
     }
*/
     strcpy(rgFddFlds.pclFddINDO,"D");

     /**************** lookup config-file to see if flight should be displayed ************/
     ilFlag = DisplayFlight (rgFddFlds.pclFddINDO, rgFddFlds.pclFddFTYP, pclAftTTYP, ilMainCshCnt);
     if(ilFlag == 1)
     {
        ilRC = SetDomVia(rgFddFlds.pclFddADID, rgFddFlds.pclFddFTYP, NumDomVia(pclAftTTYP), pclCurRowAFT);
        if (igIntAndDomDisplays != 0)
        {
           ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
        }
     }
 
     /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
     /*ilRC = ShowFddStruct();*/

     strncpy(rgFddFlds.pclFddFLNO,&pclAftJFNO[ilStart],9);
     rgFddFlds.pclFddFLNO[9] = '\0';
     TrimRight(rgFddFlds.pclFddFLNO);
  }


  /************* this is the domestic part ************************************************/

  /************** Main-Airline INDO =D *************************/
  ilMainCshCnt = 1;
  TrimRight(pclAftJFNO);
  if (strcmp(pclAftJFNO," ") == 0)
  {
     strcpy(rgFddFlds.pclFddDSEQ,"0");
  }
  else
  {
     sprintf(rgFddFlds.pclFddDSEQ,"%d",ilMainCshCnt);
  }

/*
  ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
  if (ilRC != RC_SUCCESS)
  {
     dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
  }
*/

  strcpy(rgFddFlds.pclFddREMP,pclFddREMP_ORG);
  strcpy(rgFddFlds.pclFddBLT1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT2));
  strcpy(rgFddFlds.pclFddGTD1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD2));    
  strcpy(pclBuffer,"\0");
  strcpy(pclBuffer,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP));
  if (igCheckNatureLength == 1)
  {
     strncpy(pclTTYP,&pclBuffer[2],2);
     pclTTYP[2] = '\0';
  }
  else
  {
     strcpy(pclTTYP,pclBuffer);
  }
  TrimRight(pclTTYP);
  SetDNAT(pclTTYP);

  strcpy(pclBuffer,"\0");
  strcpy(pclBuffer,FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIAL));
  strncpy(rgFddFlds.pclFddAPC3,&pclBuffer[1],3); /*set APC3 to domestic part*/
  rgFddFlds.pclFddAPC3[3] = '\0';

  strcpy(rgFddFlds.pclFddVIA3," ");

  strcpy(rgFddFlds.pclFddFLNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftFLNO));
  strcpy(pclALC,FIELDVAL(rgAftArray,pclCurRowAFT,igAftFLNO));
  pclALC[3] = '\0';
  strcpy(pclFLNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftFLNO));
  TrimRight(pclFLNO);

  strcpy(rgFddFlds.pclFddINDO,"D");
  strcpy(rgFddFlds.pclFddFTYP,"D");

  strcpy(rgFddFlds.pclFddBLT1,pclFddChaf);
  strcpy(rgFddFlds.pclFddBLT2,pclFddChat);

  /************************* Get Counters */

  ilRC = GetCDT(pclCDA, pclTTYP, ilMainCshCnt);

  if (strcmp(pcgCheckTermForCntAsgn,"NO") == 0)
  {
     if (strcmp(pclCDA,"D") == 0)
     {
        ilRC = GetDedicatedCounter("D",pclFLNO,pclCKIF,pclCKIT);
     }
     else
     {
        if (strcmp(pclCDA,"C") == 0)
        {
           ilRC = GetCommonCounter("D",pclALC,pclCKIF,pclCKIT,pclDest);
        }
        else
        {
           if (strcmp(pclCDA,"A") == 0 || strcmp(pclCDA,"X") == 0 || strcmp(pclCDA,"Y") == 0)
           {
              ilRC = GetCommonAndDedicatedCounter("D",pclALC,pclFLNO,pclCKIF,pclCKIT,pclCDA,"M",pclDest);
           }
        }
     }
  }
  else
  {
     ilRC = GetCounterTerm(pclCDA,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTEV),
                           pclALC,pclFLNO,pclCKIF,pclCKIT);
  }
  strcpy(rgFddFlds.pclFddCKIF,pclCKIF);
  strcpy(rgFddFlds.pclFddCKIT,pclCKIT);
  /**************************** end Get Counters */

  /**************** lookup config-file to see if flight should be displayed ************/
  ilFlag = DisplayFlight (rgFddFlds.pclFddINDO, rgFddFlds.pclFddFTYP, pclAftTTYP, ilMainCshCnt);
  if(ilFlag == 1)
  {
     ilRC = SetDomVia(rgFddFlds.pclFddADID, rgFddFlds.pclFddFTYP, NumDomVia(pclAftTTYP), pclCurRowAFT);
     if (igIntAndDomDisplays != 0)
     {
        ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
     }
  }

  /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
  /*ilRC = ShowFddStruct();*/
  
  /************** Main-Airline INDO =I *************************/
  if (igGenerateTM2 == 1)
  {
  if (strncmp(pclFddREMP_ORG,pcgCanceled,3) != 0)
  {
     strcpy(rgFddFlds.pclFddREMP,"TM2");
  }
  }

/*
  ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
  if (ilRC != RC_SUCCESS)
  {
     dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
  }
*/
  strcpy(rgFddFlds.pclFddINDO,"I");
  strcpy(rgFddFlds.pclFddFTYP,"D");

  /**************** lookup config-file to see if flight should be displayed ************/
  ilFlag = DisplayFlight (rgFddFlds.pclFddINDO, rgFddFlds.pclFddFTYP, pclAftTTYP, ilMainCshCnt);
  if(ilFlag == 1)
  {
     ilRC = SetDomVia(rgFddFlds.pclFddADID, rgFddFlds.pclFddFTYP, NumDomVia(pclAftTTYP), pclCurRowAFT);
     ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
  }

  /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
  /*ilRC = ShowFddStruct();*/
 
  strcpy(rgFddFlds.pclFddREMP,pclFddREMP_ORG); 
 
  /***************** prepare Data and handle Code-Share Flights (INDO=I)***************/

  strcpy(pclAftJFNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftJFNO));
  strncpy(rgFddFlds.pclFddFLNO,pclAftJFNO,9);
  rgFddFlds.pclFddFLNO[9] = '\0';
  TrimRight(rgFddFlds.pclFddFLNO);

  for (ilStart = 9; strlen(rgFddFlds.pclFddFLNO) > 3; ilStart += 9)
  {
     strncpy(pclALC,rgFddFlds.pclFddFLNO,3);
     pclALC[3] = '\0';
/*
     strcpy(pclFLNO,rgFddFlds.pclFddFLNO);
     TrimRight(pclFLNO);
*/
     ilMainCshCnt++;
     sprintf(rgFddFlds.pclFddDSEQ,"%d",ilMainCshCnt);
/*
     ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
     }
*/
     /*********** here INDO = D ******************/
     strcpy(rgFddFlds.pclFddREMP,pclFddREMP_ORG); 
     strcpy(rgFddFlds.pclFddINDO,"D");

     /************************* Get Counters */

     ilRC = GetCDT(pclCDA, pclTTYP, ilMainCshCnt);
     if (strcmp(pcgCheckTermForCntAsgn,"NO") == 0)
     {
        if (strcmp(pclCDA,"D") == 0)
        {
           ilRC = GetDedicatedCounter("D",pclFLNO,pclCKIF,pclCKIT);
        }
        else
        {
           if (strcmp(pclCDA,"C") == 0)
           {
              ilRC = GetCommonCounter("D",pclALC,pclCKIF,pclCKIT,pclDest);
           }
           else
           {
              if (strcmp(pclCDA,"A") == 0 || strcmp(pclCDA,"X") == 0 || strcmp(pclCDA,"Y") == 0)
              {
                 ilRC = GetCommonAndDedicatedCounter("D",pclALC,pclFLNO,pclCKIF,pclCKIT,pclCDA,"J",pclDest);
              }
              else
              {
                 if (strcmp(pclCDA,"S") == 0)
                 {
                    strcpy(pclCKIF,pcgCKIF);
                    strcpy(pclCKIT,pcgCKIT);
                 }
                 else
                 {
                    if (strcmp(pclCDA,"P") == 0)
                    {
                       strcpy(pclCKIF,pcgCKIF_C);
                       strcpy(pclCKIT,pcgCKIT_C);
                    }
                    else
                    {
                       if (strcmp(pclCDA,"R") == 0)
                       {
                          strcpy(pclCKIF,pcgCKIF_D);
                          strcpy(pclCKIT,pcgCKIT_D);
                       }
                       else
                       {
                          strcpy(pclCKIF," ");
                          strcpy(pclCKIT," ");
                       }
                    }
                 }
              }
           }
        }
     }
     else
     {
        ilRC = GetCounterTerm(pclCDA,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTEV),
                              pclALC,pclFLNO,pclCKIF,pclCKIT);
     }
     strcpy(rgFddFlds.pclFddCKIF,pclCKIF);
     strcpy(rgFddFlds.pclFddCKIT,pclCKIT);
     /**************************** end Get Counters */

     /**************** lookup config-file to see if flight should be displayed ************/
     ilFlag = DisplayFlight (rgFddFlds.pclFddINDO, rgFddFlds.pclFddFTYP, pclAftTTYP, ilMainCshCnt);
     if(ilFlag == 1)
     {
        ilRC = SetDomVia(rgFddFlds.pclFddADID, rgFddFlds.pclFddFTYP, NumDomVia(pclAftTTYP), pclCurRowAFT);
        if (igIntAndDomDisplays != 0)
        {
           ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
        }
     }
  
     /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
     /*ilRC = ShowFddStruct();*/

     /*********** here INDO = I ******************/
  if (igGenerateTM2 == 1)
  {
     if (strncmp(pclFddREMP_ORG,pcgCanceled,3) != 0)
     {
        strcpy(rgFddFlds.pclFddREMP,"TM2");
     }
  }

/*
     ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
     }
*/
     strcpy(rgFddFlds.pclFddINDO,"I");

     /**************** lookup config-file to see if flight should be displayed ************/
     ilFlag = DisplayFlight (rgFddFlds.pclFddINDO, rgFddFlds.pclFddFTYP, pclAftTTYP, ilMainCshCnt);
     if(ilFlag == 1)
     {
        ilRC = SetDomVia(rgFddFlds.pclFddADID, rgFddFlds.pclFddFTYP, NumDomVia(pclAftTTYP), pclCurRowAFT);
        ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
     }

     /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
     /*ilRC = ShowFddStruct();*/
      
     strcpy(rgFddFlds.pclFddREMP,pclFddREMP_ORG); 

     strncpy(rgFddFlds.pclFddFLNO,&pclAftJFNO[ilStart],9);
     rgFddFlds.pclFddFLNO[9] = '\0';
     TrimRight(rgFddFlds.pclFddFLNO);
  }

  dbg(DEBUG,"%05d: ================== End of HandleFLTI_Mix_D !!!  ===================",__LINE__);

  /* UpdateFLDTAB(pclCurRowAFT); */

  return (ilRC);
}/*end of HandleFLTI_Mix_D */




/******************************************************************************/
/* The HandleFLTI_Int_D routine                                                    */
/******************************************************************************/
static int HandleFLTI_Int_D(char *pclCurRowAFT, char *pclCurRowFDD, long *plpRowNumFDD, char *pcpFtyp)
{

  int ilRC = RC_SUCCESS;
  int ilStart = 0;
  char pclBuffer[1024];
  char pclRowBuffer[4096] = "\0";
  long llRowNumFdd = 0;
  char *pclRowFDD = NULL;
  char pclAltURNO[128] = "\0";
  char pclCKIFInt[128] = "\0";
  char pclCKITInt[128] = "\0";
  char pclCKIFDom[128] = "\0";
  char pclCKITDom[128] = "\0";
  char pclAftTTYP[128] = "\0";
  char pclAftJFNO[128] = "\0";
  char pclTTYP[128] = "\0";
  char pclCDA[128] = "\0";
  char pclALC[128] = "\0";
  char pclFLNO[128] = "\0";
  int ilMainCshCnt = 0;
  char pclAftGD1B[128] = "\0";
  char pclAftGD2B[128] = "\0";
  char pclAftGD1X[128] = "\0";
  char pclAftGD2X[128] = "\0";
  char pclOldRemark[128] = "\0";
  char pclNewRemark[128] = "\0";
  char pclGate2Remark[128] = "\0";
  char pclTmpTime[128] = "\0";
  char pclSavAPC3[128] = "\0";
  char pclSavVIA3[128] = "\0";
  char pclSavGTD1[128] = "\0";
  char pclSavGTD2[128] = "\0";
  char pclDest[128] = "\0";
  int ilTerm; 
  int ilTerm2; 
  char pclFddAurn[128] = "\0";
  char pclFddChaf[128] = "\0";
  char pclFddChat[128] = "\0";

  dbg(DEBUG,"%05d: ================== This is HandleFLTI_Int_D !!!  ===================",__LINE__);

  ilMainCshCnt = 0;

  /***************** Prepare Data for first FDD-Record ****************/
  ilMainCshCnt++;
  sprintf(rgFddFlds.pclFddDSEQ,"%d",ilMainCshCnt);
  strcpy(rgFddFlds.pclFddNODE,"\0");
  strcpy(rgFddFlds.pclFddAURN,FIELDVAL(rgAftArray,pclCurRowAFT,igAftURNO)); 
  strcpy(pclFddAurn,FIELDVAL(rgAftArray,pclCurRowAFT,igAftURNO)); 
  strcpy(rgFddFlds.pclFddFLNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftFLNO));
  strcpy(pclALC,FIELDVAL(rgAftArray,pclCurRowAFT,igAftFLNO));
  pclALC[3] = '\0';
  strcpy(pclFLNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftFLNO));
  TrimRight(pclFLNO);
  strcpy(rgFddFlds.pclFddTIFF,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTIFD));
  strcpy(rgFddFlds.pclFddSTOF,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTOD));
  strcpy(rgFddFlds.pclFddETOF,FIELDVAL(rgAftArray,pclCurRowAFT,igAftETOD));
  strcpy(rgFddFlds.pclFddAIRB,FIELDVAL(rgAftArray,pclCurRowAFT,igAftAIRB));
  strcpy(rgFddFlds.pclFddOFBL,FIELDVAL(rgAftArray,pclCurRowAFT,igAftOFBL));
  strcpy(rgFddFlds.pclFddBLT1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT1));
  strcpy(rgFddFlds.pclFddWRO1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftWRO1));

  /*strcpy(rgFddFlds.pclFddBLT2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT2));*/

  strcpy(rgFddFlds.pclFddGTD1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD1));
  strcpy(rgFddFlds.pclFddGTD2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD2));
  strcpy(rgFddFlds.pclFddOBL1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT1));
  strcpy(rgFddFlds.pclFddOBL2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT2));
  strcpy(rgFddFlds.pclFddOGT1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD1));
  strcpy(rgFddFlds.pclFddOGT2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD2));

  strcpy(pclAftGD1B,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBOAO));
  TrimRight(pclAftGD1B);
  strcpy(pclAftGD1X,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGD1X));
  TrimRight(pclAftGD1X);
  if (strlen(pclAftGD1X) > 1)
     strcpy(rgFddFlds.pclFddGD1B,pclAftGD1X);
  else
     strcpy(rgFddFlds.pclFddGD1B,pclAftGD1B);
  strcpy(pclAftGD2B,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBOAC));
  TrimRight(pclAftGD2B);
  strcpy(pclAftGD2X,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGD2X));
  TrimRight(pclAftGD2X);
  if (strlen(pclAftGD2X) > 1)
     strcpy(rgFddFlds.pclFddGD2B,pclAftGD2X);
  else
     strcpy(rgFddFlds.pclFddGD2B,pclAftGD2B);

  /* Remp is already set from updateFlight */
  strcpy(rgFddFlds.pclFddAPC3,FIELDVAL(rgAftArray,pclCurRowAFT,igAftDES3));
  strcpy(pclDest,FIELDVAL(rgAftArray,pclCurRowAFT,igAftDES3));
  strcpy(rgFddFlds.pclFddVIA3,FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIA3));
  strcpy(rgFddFlds.pclFddINDO,"I");
  strcpy(rgFddFlds.pclFddFTYP,pcpFtyp);
  TimeToStr(rgFddFlds.pclFddLSTU,time(NULL)); 
  TimeToStr(rgFddFlds.pclFddCDAT,time(NULL));
  strcpy(rgFddFlds.pclFddUSEU,"FLDHDL");
  strcpy(rgFddFlds.pclFddUSEC,"FLDHDL");
  strcpy(rgFddFlds.pclFddHOPO,pcgHomeAp);
  if (igCheckNatureLength == 1)
  {
     strncpy(pclTTYP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP),2);
     pclTTYP[2] = '\0';
  }
  else
  {
     strcpy(pclTTYP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP));
  }
  TrimRight(pclTTYP);
  SetDNAT(pclTTYP);


  strcpy(pclAftTTYP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP));

  strcpy(pclAftJFNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftJFNO));
  TrimRight(pclAftJFNO);
  if (strcmp(pclAftJFNO," ") == 0)
  {
     strcpy(rgFddFlds.pclFddDSEQ,"0");
  }

  /********* save REMP ******************/
 
  strcpy(rgFddFlds.pclFddREMP,rgFddFlds.pclFddREMP);

  /************** Main-Airline INDO =I *************************/
  
  /************************* Get Chutes */
  ilRC = GetChutes(pclFLNO,pclFddAurn,pclFddChaf,pclFddChat);
  strcpy(rgFddFlds.pclFddBLT1,pclFddChaf);
  strcpy(rgFddFlds.pclFddBLT2,pclFddChat);

  /************************* Get Counters */

  ilRC = GetCDT(pclCDA, pclTTYP, ilMainCshCnt);
  dbg(DEBUG,"%05d: Get Counters for MainFlight pclCDA: <%s>",__LINE__,pclCDA);

  if (strcmp(pcgCheckTermForCntAsgn,"NO") == 0)
  {
     if (strcmp(pclCDA,"D") == 0)
     {
        ilRC = GetDedicatedCounter("I",pclFLNO,pclCKIFInt,pclCKITInt);
        if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
           ilRC = GetDedicatedCounter("D",pclFLNO,pclCKIFDom,pclCKITDom);
     }
     else
     {
        if (strcmp(pclCDA,"C") == 0)
        {
           ilRC = GetCommonCounter("I",pclALC,pclCKIFInt,pclCKITInt,pclDest);
           if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
              ilRC = GetCommonCounter("D",pclALC,pclCKIFDom,pclCKITDom,pclDest);
        }
        else
        {
           if (strcmp(pclCDA,"A") == 0 || strcmp(pclCDA,"X") == 0 || strcmp(pclCDA,"Y") == 0)
           {
              ilRC = GetCommonAndDedicatedCounter("I",pclALC,pclFLNO,pclCKIFInt,pclCKITInt,pclCDA,"M",pclDest);
              if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
                 ilRC = GetCommonAndDedicatedCounter("D",pclALC,pclFLNO,pclCKIFDom,pclCKITDom,pclCDA,"M",pclDest);
           }
        }
     }
  }
  else
  {
     ilRC = GetCounterTerm(pclCDA,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTEV),
                           pclALC,pclFLNO,pclCKIFInt,pclCKITInt);
     strcpy(pclCKIFDom,pclCKIFInt);
     strcpy(pclCKITDom,pclCKITInt);
  }
  if (strcmp(pcgUpdateCheckinFromTo,"YES") == 0)
     ilRC = UpdateCheckInFromTo(pclFddAurn,pclCKIFInt,pclCKITInt,pclCKIFDom,pclCKITDom);
  strcpy(rgFddFlds.pclFddCKIF,pclCKIFInt);
  strcpy(rgFddFlds.pclFddCKIT,pclCKITInt);
  if (strcmp(pcgUseGateStatusFields,"YES") == 0)
  {
     strcpy(pclGate2Remark,FIELDVAL(rgAftArray,pclCurRowAFT,igAftRGD2));
     TrimRight(pclGate2Remark);
     if (*pclGate2Remark == '0')
        *pclGate2Remark = ' ';
     if (strstr(FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP),pcgDeparted) != NULL ||
         strstr(FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP),pcgAirborne) != NULL)
        strcpy(pclGate2Remark," ");
     if (*pclGate2Remark == ' ' && strstr(FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP),pcgDelayedGateOpen) != NULL)
        strcpy(pclGate2Remark,pcgDelayed);
     if (strcmp(pclGate2Remark,pcgGateOpened) == 0 &&
         (strstr(FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP),pcgDelayedGateOpen) != NULL ||
          strstr(FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP),pcgDelayed) != NULL))
        strcpy(pclGate2Remark,pcgDelayedGateOpen);
     if (*pclGate2Remark == ' ' &&
         (strstr(FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP),pcgGateOpened) != NULL ||
          strstr(FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP),pcgBoarding) != NULL ||
          strstr(FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP),pcgFinalcall) != NULL ||
          strstr(FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP),pcgGateclosed) != NULL))
     {
        strcpy(pclGate2Remark,"XXX");
        if (strcmp(FIELDVAL(rgAftArray,pclCurRowAFT,igAftETOD)," ") != 0)
        {
           strcpy(pclTmpTime,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTOD));
           TrimRight(pclTmpTime);
           AddSecondsToCEDATime12(pclTmpTime,igDelayTimeDiffDep,1);
           if (strcmp(FIELDVAL(rgAftArray,pclCurRowAFT,igAftETOD), pclTmpTime) > 0)     /*ETOA > STOA ?*/
           {
              strcpy(pclGate2Remark,pcgDelayed);
           }
        }
     }
  }
  if (strcmp(pcgHdlDACO,"STEV") == 0)
  {
     strcpy(rgFddFlds.pclFddDACO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTEV));
     ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
  }
  else
  {
     if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
     {
        strcpy(rgFddFlds.pclFddDACO,"1");
        strcpy(rgFddFlds.pclFddINDO,"1");
        strcpy(pclOldRemark,FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP));
        strcpy(pclNewRemark,pclOldRemark);
        ilRC = HandleTerminal_D(pclCurRowAFT,pclNewRemark,&ilTerm,&ilTerm2);
        strcpy(pclSavAPC3,rgFddFlds.pclFddAPC3);
        strcpy(pclSavVIA3,rgFddFlds.pclFddVIA3);
        strcpy(pclSavGTD1,rgFddFlds.pclFddGTD1);
        strcpy(pclSavGTD2,rgFddFlds.pclFddGTD2);
        if (strcmp(pclNewRemark,"TM2") == 0 && strcmp(pcgCheckCounterOnly,"YES") != 0)
        {
           strcpy(rgFddFlds.pclFddINDO,"2");
           strcpy(rgFddFlds.pclFddREMP,pclNewRemark);
           strcpy(rgFddFlds.pclFddGTD1," ");
           strcpy(rgFddFlds.pclFddGTD2," ");
           strcpy(rgFddFlds.pclFddCKIF," ");
           strcpy(rgFddFlds.pclFddCKIT," ");
           ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
        }
        else if (ilTerm2 == 2 || (strcmp(pcgCheckCounterOnly,"YES") == 0 && strlen(pclCKIFDom) > 1))
        {
           strcpy(rgFddFlds.pclFddINDO,"1");
           strcpy(rgFddFlds.pclFddVIA3," ");
           strcpy(rgFddFlds.pclFddGTD2," ");
           strcpy(rgFddFlds.pclFddCKIF,pclCKIFInt);
           strcpy(rgFddFlds.pclFddCKIT,pclCKITInt);
           ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
/*
           ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
*/
           if (strcmp(pcgCheckCounterOnly,"YES") != 0)
           {
              strcpy(rgFddFlds.pclFddDACO,"2");
              strcpy(rgFddFlds.pclFddINDO,"1");
              strcpy(rgFddFlds.pclFddREMP,"TM1");
              strcpy(rgFddFlds.pclFddAPC3,pclSavAPC3);
              strcpy(rgFddFlds.pclFddVIA3," ");
              strcpy(rgFddFlds.pclFddGTD1," ");
              strcpy(rgFddFlds.pclFddGTD2," ");
              strcpy(rgFddFlds.pclFddCKIF," ");
              strcpy(rgFddFlds.pclFddCKIT," ");
              ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
           }
           strcpy(rgFddFlds.pclFddDACO,"1");
           strcpy(rgFddFlds.pclFddINDO,"I");
        }
        else
           ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
     }
     else
     {
        strcpy(rgFddFlds.pclFddDACO,"1");
        strcpy(pclOldRemark,FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP));
        strcpy(pclNewRemark,pclOldRemark);
        ilRC = HandleTerminal_D(pclCurRowAFT,pclNewRemark,&ilTerm,&ilTerm2);
        strcpy(pclSavGTD1,rgFddFlds.pclFddGTD1);
        strcpy(pclSavGTD2,rgFddFlds.pclFddGTD2);
        if (strcmp(pclNewRemark,"TM2") == 0)
        {
           strcpy(rgFddFlds.pclFddREMP,pclNewRemark);
           strcpy(rgFddFlds.pclFddGTD1," ");
           strcpy(rgFddFlds.pclFddGTD2," ");
           strcpy(rgFddFlds.pclFddCKIF," ");
           strcpy(rgFddFlds.pclFddCKIT," ");
        }
        ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
     }
  }
  /**************************** end Get Counters */

  /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
  /*ilRC = ShowFddStruct();*/
  
  /************** Main-Airline INDO =D *************************/

  if (igIntAndDomDisplays != 0)
  {
/*
     ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
     }
*/
     strcpy(rgFddFlds.pclFddINDO,"D");
     if (strcmp(pcgHdlDACO,"STEV") != 0)
     {
        if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
        {
           strcpy(rgFddFlds.pclFddINDO,"1");
           if (ilTerm == 2)
           {
              strcpy(rgFddFlds.pclFddDACO,"2");
              strcpy(rgFddFlds.pclFddINDO,"2");
              if (strcmp(pcgUseGateStatusFields,"YES") == 0 && *pclGate2Remark != ' ' && *pclSavGTD2 != ' ')
              {
                 if (strcmp(pclGate2Remark,"XXX") == 0)
                    strcpy(rgFddFlds.pclFddREMP," ");
                 else
                    strcpy(rgFddFlds.pclFddREMP,pclGate2Remark);
              }
              else
                 strcpy(rgFddFlds.pclFddREMP,pclOldRemark);
              strcpy(rgFddFlds.pclFddGTD1,pclSavGTD1);
              strcpy(rgFddFlds.pclFddGTD2,pclSavGTD2);
              strcpy(rgFddFlds.pclFddCKIF,pclCKIFDom);
              strcpy(rgFddFlds.pclFddCKIT,pclCKITDom);
              ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
           }
           else if (ilTerm2 == 2 || (strcmp(pcgCheckCounterOnly,"YES") == 0 && strlen(pclCKIFDom) > 1))
           {
              if (strcmp(pcgCheckCounterOnly,"YES") != 0)
              {
                 strcpy(rgFddFlds.pclFddINDO,"2");
                 strcpy(rgFddFlds.pclFddDACO,"1");
                 strcpy(rgFddFlds.pclFddREMP,"TM2");
                 strcpy(rgFddFlds.pclFddAPC3,pclSavVIA3);
                 strcpy(rgFddFlds.pclFddVIA3," ");
                 strcpy(rgFddFlds.pclFddGTD1," ");
                 strcpy(rgFddFlds.pclFddGTD2," ");
                 strcpy(rgFddFlds.pclFddCKIF," ");
                 strcpy(rgFddFlds.pclFddCKIT," ");
                 ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
/*
                 ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
*/
              }
              strcpy(rgFddFlds.pclFddINDO,"2");
              strcpy(rgFddFlds.pclFddDACO,"2");
              if (strcmp(pcgUseGateStatusFields,"YES") == 0 && *pclGate2Remark != ' ' && *pclSavGTD2 != ' ')
              {
                 if (strcmp(pclGate2Remark,"XXX") == 0)
                    strcpy(rgFddFlds.pclFddREMP," ");
                 else
                    strcpy(rgFddFlds.pclFddREMP,pclGate2Remark);
              }
              else
                 strcpy(rgFddFlds.pclFddREMP,pclOldRemark);
              if (strcmp(pcgCheckCounterOnly,"YES") != 0)
              {
                 strcpy(rgFddFlds.pclFddAPC3,pclSavVIA3);
                 strcpy(rgFddFlds.pclFddVIA3," ");
                 strcpy(rgFddFlds.pclFddGTD1,pclSavGTD2);
                 strcpy(rgFddFlds.pclFddGTD2," ");
              }
              else
              {
                 strcpy(rgFddFlds.pclFddAPC3,pclSavAPC3);
                 strcpy(rgFddFlds.pclFddVIA3,pclSavVIA3);
                 strcpy(rgFddFlds.pclFddGTD1,pclSavGTD1);
                 strcpy(rgFddFlds.pclFddGTD2,pclSavGTD2);
              }
              strcpy(rgFddFlds.pclFddCKIF,pclCKIFDom);
              strcpy(rgFddFlds.pclFddCKIT,pclCKITDom);
              ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
           }
           else if (ilTerm == 1 && strcmp(pcgShowAllFlightsInT2,"YES") == 0)
           {
              strcpy(rgFddFlds.pclFddDACO,"2");
              strcpy(rgFddFlds.pclFddINDO,"1");
              if (strcmp(pcgCheckCounterOnly,"YES") != 0)
                 strcpy(rgFddFlds.pclFddREMP,"TM1");
              strcpy(rgFddFlds.pclFddAPC3,pclSavAPC3);
              strcpy(rgFddFlds.pclFddVIA3,pclSavVIA3);
              if (strcmp(pcgCheckCounterOnly,"YES") != 0)
              {
                 strcpy(rgFddFlds.pclFddGTD1," ");
                 strcpy(rgFddFlds.pclFddGTD2," ");
                 strcpy(rgFddFlds.pclFddCKIF," ");
                 strcpy(rgFddFlds.pclFddCKIT," ");
              }
              else
              {
                 if (*pclCKIFDom == ' ' && strlen(pcgFixCnt) > 0)
                 {
                    strcpy(rgFddFlds.pclFddCKIF,pcgFixCnt);
                    strcpy(rgFddFlds.pclFddCKIT,pcgFixCnt);
                 }
                 else
                 {
                    strcpy(rgFddFlds.pclFddCKIF,pclCKIFDom);
                    strcpy(rgFddFlds.pclFddCKIT,pclCKITDom);
                 }
              }
              ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
           }
        }
        else
        {
           strcpy(rgFddFlds.pclFddDACO,"2");
           if (strcmp(pcgUseGateStatusFields,"YES") == 0 && *pclGate2Remark != ' ' && *pclSavGTD2 != ' ')
           {
              if (strcmp(pclGate2Remark,"XXX") == 0)
                 strcpy(rgFddFlds.pclFddREMP," ");
              else
                 strcpy(rgFddFlds.pclFddREMP,pclGate2Remark);
           }
           else
              strcpy(rgFddFlds.pclFddREMP,pclOldRemark);
           strcpy(rgFddFlds.pclFddGTD1,pclSavGTD1);
           strcpy(rgFddFlds.pclFddGTD2,pclSavGTD2);
           strcpy(rgFddFlds.pclFddCKIF,pclCKIFDom);
           strcpy(rgFddFlds.pclFddCKIT,pclCKITDom);
           if (ilTerm == 2)
              ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
        }
     }
     else
        ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
  }
  /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
  /*ilRC = ShowFddStruct();*/
  

  /***************** prepare Data and handle Code-Share Flights (INDO=I)***************/

  strcpy(pclAftJFNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftJFNO));
  strncpy(rgFddFlds.pclFddFLNO,pclAftJFNO,9);
  rgFddFlds.pclFddFLNO[9] = '\0'; 
  TrimRight(rgFddFlds.pclFddFLNO);

  for (ilStart = 9; strlen(rgFddFlds.pclFddFLNO) > 3; ilStart += 9)
  {
     strncpy(pclALC,rgFddFlds.pclFddFLNO,3);
     pclALC[3] = '\0';
/*
     strcpy(pclFLNO,rgFddFlds.pclFddFLNO);
     TrimRight(pclFLNO);
*/
     ilMainCshCnt++;
     sprintf(rgFddFlds.pclFddDSEQ,"%d",ilMainCshCnt);
/*
     ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
     }
*/
     /*********** here INDO = I ******************/
     strcpy(rgFddFlds.pclFddINDO,"I");

     /************************* Get Counters */

     ilRC = GetCDT(pclCDA, pclTTYP, ilMainCshCnt);

     dbg(DEBUG,"%05d: Get Counters for CodeShare pclCDA: <%s>",__LINE__,pclCDA);
     if (strcmp(pcgCheckTermForCntAsgn,"NO") == 0)
     {
        if (strcmp(pclCDA,"D") == 0)
        {
           ilRC = GetDedicatedCounter("I",pclFLNO,pclCKIFInt,pclCKITInt);
           if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
              ilRC = GetDedicatedCounter("D",pclFLNO,pclCKIFDom,pclCKITDom);
        }
        else
        {
           if (strcmp(pclCDA,"C") == 0)
           {
              ilRC = GetCommonCounter("I",pclALC,pclCKIFInt,pclCKITInt,pclDest);
              if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
                 ilRC = GetCommonCounter("D",pclALC,pclCKIFDom,pclCKITDom,pclDest);
           }
           else
           {
              if (strcmp(pclCDA,"A") == 0 || strcmp(pclCDA,"X") == 0 || strcmp(pclCDA,"Y") == 0)
              {
                 ilRC = GetCommonAndDedicatedCounter("I",pclALC,pclFLNO,pclCKIFInt,pclCKITInt,pclCDA,"J",pclDest);
                 if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
                    ilRC = GetCommonAndDedicatedCounter("D",pclALC,pclFLNO,pclCKIFDom,pclCKITDom,pclCDA,"J",pclDest);
              }
              else
              {
                 if (strcmp(pclCDA,"S") == 0)
                 {
/*
                    strcpy(pclCKIF,pcgCKIF);
                    strcpy(pclCKIT,pcgCKIT);
*/
                 }
                 else
                 {
                    if (strcmp(pclCDA,"P") == 0)
                    {
                       strcpy(pclCKIFInt,pcgCKIF_C);
                       strcpy(pclCKITInt,pcgCKIT_C);
                       strcpy(pclCKIFDom,pcgCKIF_C);
                       strcpy(pclCKITDom,pcgCKIT_C);
                    }
                    else
                    {
                       if (strcmp(pclCDA,"R") == 0)
                       {
                          strcpy(pclCKIFInt,pcgCKIF_D);
                          strcpy(pclCKITInt,pcgCKIT_D);
                          strcpy(pclCKIFDom,pcgCKIF_D);
                          strcpy(pclCKITDom,pcgCKIT_D);
                       }
                       else
                       {
                          strcpy(pclCKIFInt," ");
                          strcpy(pclCKITInt," ");
                          strcpy(pclCKIFDom," ");
                          strcpy(pclCKITDom," ");
                       }
                    }
                 }
              }
           }
        }
     }
     else
     {
        ilRC = GetCounterTerm(pclCDA,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTEV),
                              pclALC,pclFLNO,pclCKIFInt,pclCKITInt);
        strcpy(pclCKIFDom,pclCKIFInt);
        strcpy(pclCKITDom,pclCKITInt);
     }
     strcpy(rgFddFlds.pclFddCKIF,pclCKIFInt);
     strcpy(rgFddFlds.pclFddCKIT,pclCKITInt);
     if (strcmp(pcgHdlDACO,"STEV") != 0)
     {
        strcpy(rgFddFlds.pclFddGTD1,pclSavGTD1);
        strcpy(rgFddFlds.pclFddGTD2,pclSavGTD2);
        if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
        {
           strcpy(rgFddFlds.pclFddDACO,"1");
           strcpy(rgFddFlds.pclFddINDO,"1");
           strcpy(rgFddFlds.pclFddREMP,pclOldRemark);
           if (strcmp(pclNewRemark,"TM2") == 0 && strcmp(pcgCheckCounterOnly,"YES") != 0)
           {
              strcpy(rgFddFlds.pclFddINDO,"2");
              strcpy(rgFddFlds.pclFddREMP,pclNewRemark);
              strcpy(rgFddFlds.pclFddGTD1," ");
              strcpy(rgFddFlds.pclFddGTD2," ");
              strcpy(rgFddFlds.pclFddCKIF," ");
              strcpy(rgFddFlds.pclFddCKIT," ");
              ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
           }
           else if (ilTerm2 == 2 || (strcmp(pcgCheckCounterOnly,"YES") == 0 && strlen(pclCKIFDom) > 1))
           {
              strcpy(rgFddFlds.pclFddINDO,"1");
              strcpy(rgFddFlds.pclFddREMP,pclOldRemark);
              strcpy(rgFddFlds.pclFddAPC3,pclSavAPC3);
              strcpy(rgFddFlds.pclFddVIA3," ");
              strcpy(rgFddFlds.pclFddGTD1,pclSavGTD1);
              strcpy(rgFddFlds.pclFddGTD2," ");
              strcpy(rgFddFlds.pclFddCKIF,pclCKIFInt);
              strcpy(rgFddFlds.pclFddCKIT,pclCKITInt);
              ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
/*
              ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
*/
              if (strcmp(pcgCheckCounterOnly,"YES") != 0)
              {
                 strcpy(rgFddFlds.pclFddDACO,"2");
                 strcpy(rgFddFlds.pclFddINDO,"1");
                 strcpy(rgFddFlds.pclFddREMP,"TM1");
                 strcpy(rgFddFlds.pclFddAPC3,pclSavAPC3);
                 strcpy(rgFddFlds.pclFddVIA3," ");
                 strcpy(rgFddFlds.pclFddGTD1," ");
                 strcpy(rgFddFlds.pclFddGTD2," ");
                 strcpy(rgFddFlds.pclFddCKIF," ");
                 strcpy(rgFddFlds.pclFddCKIT," ");
                 ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
              }
              strcpy(rgFddFlds.pclFddDACO,"1");
           }
           else
              ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
        }
        else
        {
           strcpy(rgFddFlds.pclFddDACO,"1");
           if (strcmp(pclNewRemark,"TM2") == 0)
           {
              strcpy(rgFddFlds.pclFddREMP,pclNewRemark);
              strcpy(rgFddFlds.pclFddGTD1," ");
              strcpy(rgFddFlds.pclFddGTD2," ");
              strcpy(rgFddFlds.pclFddCKIF," ");
              strcpy(rgFddFlds.pclFddCKIT," ");
           }
           ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
        }
     }
     else
        ilRC = StructAddRowPartFDDDep(plpRowNumFDD);

     /**************************** end Get Counters */

     /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
     /*ilRC = ShowFddStruct();*/

     /*********** here INDO = D ******************/
     if (igIntAndDomDisplays != 0)
     {
/*
        ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
        if (ilRC != RC_SUCCESS)
        {
           dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
        }
*/
        strcpy(rgFddFlds.pclFddINDO,"D");
        if (strcmp(pcgHdlDACO,"STEV") != 0)
        {
           if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
           {
              if (ilTerm == 2)
              {
                 strcpy(rgFddFlds.pclFddDACO,"2");
                 strcpy(rgFddFlds.pclFddINDO,"2");
                 if (strcmp(pcgUseGateStatusFields,"YES") == 0 && *pclGate2Remark != ' ' && *pclSavGTD2 != ' ')
                 {
                    if (strcmp(pclGate2Remark,"XXX") == 0)
                       strcpy(rgFddFlds.pclFddREMP," ");
                    else
                       strcpy(rgFddFlds.pclFddREMP,pclGate2Remark);
                 }
                 else
                    strcpy(rgFddFlds.pclFddREMP,pclOldRemark);
                 strcpy(rgFddFlds.pclFddGTD1,pclSavGTD1);
                 strcpy(rgFddFlds.pclFddGTD2,pclSavGTD2);
                 strcpy(rgFddFlds.pclFddCKIF,pclCKIFDom);
                 strcpy(rgFddFlds.pclFddCKIT,pclCKITDom);
                 ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
              }
              if (ilTerm2 == 2 || (strcmp(pcgCheckCounterOnly,"YES") == 0 && strlen(pclCKIFDom) > 1))
              {
                 if (strcmp(pcgCheckCounterOnly,"YES") != 0)
                 {
                    strcpy(rgFddFlds.pclFddDACO,"1");
                    strcpy(rgFddFlds.pclFddINDO,"2");
                    strcpy(rgFddFlds.pclFddREMP,"TM2");
                    strcpy(rgFddFlds.pclFddAPC3,pclSavVIA3);
                    strcpy(rgFddFlds.pclFddVIA3," ");
                    strcpy(rgFddFlds.pclFddGTD1," ");
                    strcpy(rgFddFlds.pclFddGTD2," ");
                    strcpy(rgFddFlds.pclFddCKIF," ");
                    strcpy(rgFddFlds.pclFddCKIT," ");
                    ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
/*
                    ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
*/
                 }
                 strcpy(rgFddFlds.pclFddDACO,"2");
                 strcpy(rgFddFlds.pclFddINDO,"2");
                 if (strcmp(pcgUseGateStatusFields,"YES") == 0 && *pclGate2Remark != ' ' && *pclSavGTD2 != ' ')
                 {
                    if (strcmp(pclGate2Remark,"XXX") == 0)
                       strcpy(rgFddFlds.pclFddREMP," ");
                    else
                       strcpy(rgFddFlds.pclFddREMP,pclGate2Remark);
                 }
                 else
                    strcpy(rgFddFlds.pclFddREMP,pclOldRemark);
                 if (strcmp(pcgCheckCounterOnly,"YES") != 0)
                 {
                    strcpy(rgFddFlds.pclFddAPC3,pclSavVIA3);
                    strcpy(rgFddFlds.pclFddVIA3," ");
                    strcpy(rgFddFlds.pclFddGTD1,pclSavGTD2);
                    strcpy(rgFddFlds.pclFddGTD2," ");
                 }
                 else
                 {
                    strcpy(rgFddFlds.pclFddAPC3,pclSavAPC3);
                    strcpy(rgFddFlds.pclFddVIA3,pclSavVIA3);
                    strcpy(rgFddFlds.pclFddGTD1,pclSavGTD1);
                    strcpy(rgFddFlds.pclFddGTD2,pclSavGTD2);
                 }
                 strcpy(rgFddFlds.pclFddCKIF,pclCKIFDom);
                 strcpy(rgFddFlds.pclFddCKIT,pclCKITDom);
                 ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
              }
              else if (ilTerm == 1 && strcmp(pcgShowAllFlightsInT2,"YES") == 0)
              {
                 strcpy(rgFddFlds.pclFddDACO,"2");
                 strcpy(rgFddFlds.pclFddINDO,"1");
                 if (strcmp(pcgCheckCounterOnly,"YES") != 0)
                    strcpy(rgFddFlds.pclFddREMP,"TM1");
                 strcpy(rgFddFlds.pclFddAPC3,pclSavAPC3);
                 strcpy(rgFddFlds.pclFddVIA3,pclSavVIA3);
                 if (strcmp(pcgCheckCounterOnly,"YES") != 0)
                 {
                    strcpy(rgFddFlds.pclFddGTD1," ");
                    strcpy(rgFddFlds.pclFddGTD2," ");
                    strcpy(rgFddFlds.pclFddCKIF," ");
                    strcpy(rgFddFlds.pclFddCKIT," ");
                 }
                 else
                 {
                    if (*pclCKIFDom == ' ' && strlen(pcgFixCnt) > 0)
                    {
                       strcpy(rgFddFlds.pclFddCKIF,pcgFixCnt);
                       strcpy(rgFddFlds.pclFddCKIT,pcgFixCnt);
                    }
                    else
                    {
                       strcpy(rgFddFlds.pclFddCKIF,pclCKIFDom);
                       strcpy(rgFddFlds.pclFddCKIT,pclCKITDom);
                    }
                 }
                 ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
              }
           }
           else
           {
              strcpy(rgFddFlds.pclFddDACO,"2");
              if (strcmp(pcgUseGateStatusFields,"YES") == 0 && *pclGate2Remark != ' ' && *pclSavGTD2 != ' ')
              {
                 if (strcmp(pclGate2Remark,"XXX") == 0)
                    strcpy(rgFddFlds.pclFddREMP," ");
                 else
                    strcpy(rgFddFlds.pclFddREMP,pclGate2Remark);
              }
              else
                 strcpy(rgFddFlds.pclFddREMP,pclOldRemark);
              strcpy(rgFddFlds.pclFddGTD1,pclSavGTD1);
              strcpy(rgFddFlds.pclFddGTD2,pclSavGTD2);
              strcpy(rgFddFlds.pclFddCKIF,pclCKIFDom);
              strcpy(rgFddFlds.pclFddCKIT,pclCKITDom);
              if (ilTerm == 2)
                 ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
           }
        }
        else 
           ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
     }
     /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);*/
     /*ilRC = ShowFddStruct();*/
      
     strncpy(rgFddFlds.pclFddFLNO,&pclAftJFNO[ilStart],9);
     rgFddFlds.pclFddFLNO[9] = '\0'; 
     TrimRight(rgFddFlds.pclFddFLNO);
  }
  /* UpdateFLDTAB(pclCurRowAFT); */
  dbg(DEBUG,"%05d: ================== End of HandleFLTI_Int_D !!!  ===================",__LINE__);

  return (ilRC);
}/*end of HandleFLTI_Int_D */


/******************************************************************************/
/* The HandleFLTI_D routine                                                    */
/******************************************************************************/
static int HandleFLTI_Dom_D(char *pclCurRowAFT, char *pclCurRowFDD, long *plpRowNumFDD)
{
  int ilRC = RC_SUCCESS;
  int ilStart = 0;
  char pclBuffer[2048];
  char pclRowBuffer[4096] = "\0";
  long llRowNumFdd = 0;
  char *pclRowFDD = NULL;
  char pclCDA[10] = "\0";
  int ilMainCshCnt = 0;
  char pclFddREMP_ORG[FDD_LEN_S] = "\0";  /*This is REMP Original, after ReGet from FDD-TAB*/
  char pclAltURNO[128] = "\0";
  char pclCKIFInt[128] = "\0";
  char pclCKITInt[128] = "\0";
  char pclCKIFDom[128] = "\0";
  char pclCKITDom[128] = "\0";
  char pclAftTTYP[128] = "\0";
  char pclAftJFNO[128] = "\0";
  char pclTTYP[128] = "\0";
  char pclALC[128] = "\0";
  char pclFLNO[128] = "\0";
  char pclAftGD1B[128] = "\0";
  char pclAftGD2B[128] = "\0";
  char pclAftGD1X[128] = "\0";
  char pclAftGD2X[128] = "\0";
  char pclOldRemark[128] = "\0";
  char pclNewRemark[128] = "\0";
  char pclGate2Remark[128] = "\0";
  char pclTmpTime[128] = "\0";
  char pclSavAPC3[128] = "\0";
  char pclSavVIA3[128] = "\0";
  char pclSavGTD1[128] = "\0";
  char pclSavGTD2[128] = "\0";
  char pclDest[128] = "\0";
  int ilTerm; 
  int ilTerm2; 
  char pclFddAurn[128] = "\0";
  char pclFddChaf[128] = "\0";
  char pclFddChat[128] = "\0";

  dbg(DEBUG,"%05d: ================== Beginning of HandleFLTI_Dom_D !!!  ===================",__LINE__);

  ilMainCshCnt++;
  sprintf(rgFddFlds.pclFddDSEQ,"%d",ilMainCshCnt);

  /***************** Prepare Data for first FDD-Record ****************/

  strcpy(rgFddFlds.pclFddNODE,"\0");
  strcpy(rgFddFlds.pclFddAURN,FIELDVAL(rgAftArray,pclCurRowAFT,igAftURNO)); 
  strcpy(pclFddAurn,FIELDVAL(rgAftArray,pclCurRowAFT,igAftURNO)); 
  strcpy(rgFddFlds.pclFddFLNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftFLNO));
  strcpy(pclALC,FIELDVAL(rgAftArray,pclCurRowAFT,igAftFLNO));
  pclALC[3] = '\0';
  strcpy(pclFLNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftFLNO));
  TrimRight(pclFLNO);
  strcpy(rgFddFlds.pclFddTIFF,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTIFD));
  strcpy(rgFddFlds.pclFddSTOF,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTOD));
  strcpy(rgFddFlds.pclFddETOF,FIELDVAL(rgAftArray,pclCurRowAFT,igAftETOD));
  strcpy(rgFddFlds.pclFddAIRB,FIELDVAL(rgAftArray,pclCurRowAFT,igAftAIRB));
  strcpy(rgFddFlds.pclFddOFBL,FIELDVAL(rgAftArray,pclCurRowAFT,igAftOFBL));
  strcpy(rgFddFlds.pclFddWRO1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftWRO1));

  /*strcpy(rgFddFlds.pclFddBLT1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT1));*/
  strcpy(rgFddFlds.pclFddBLT1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT1));
  strcpy(rgFddFlds.pclFddGTD1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD1));
  strcpy(rgFddFlds.pclFddGTD2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD2));
  strcpy(rgFddFlds.pclFddOBL1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT1));
  strcpy(rgFddFlds.pclFddOBL2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBLT2));
  strcpy(rgFddFlds.pclFddOGT1,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD1));
  strcpy(rgFddFlds.pclFddOGT2,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGTD2));
  if (strcmp(pcgHdlDACO,"STEV") == 0)
  {
     strcpy(rgFddFlds.pclFddDACO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTEV));
  }
  else
  {
     strcpy(rgFddFlds.pclFddDACO,"2");
  }

  strcpy(pclAftGD1B,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBOAO));
  TrimRight(pclAftGD1B);
  strcpy(pclAftGD1X,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGD1X));
  TrimRight(pclAftGD1X);
  if (strlen(pclAftGD1X) > 1)
     strcpy(rgFddFlds.pclFddGD1B,pclAftGD1X);
  else
     strcpy(rgFddFlds.pclFddGD1B,pclAftGD1B);
  strcpy(pclAftGD2B,FIELDVAL(rgAftArray,pclCurRowAFT,igAftBOAO));
  TrimRight(pclAftGD2B);
  strcpy(pclAftGD2X,FIELDVAL(rgAftArray,pclCurRowAFT,igAftGD2X));
  TrimRight(pclAftGD2X);
  if (strlen(pclAftGD2X) > 1)
     strcpy(rgFddFlds.pclFddGD2B,pclAftGD2X);
  else
     strcpy(rgFddFlds.pclFddGD2B,pclAftGD2B);

  /* Remp is already set from updateFlight */
  strcpy(rgFddFlds.pclFddAPC3,FIELDVAL(rgAftArray,pclCurRowAFT,igAftDES3));
  strcpy(pclDest,FIELDVAL(rgAftArray,pclCurRowAFT,igAftDES3));
  strcpy(rgFddFlds.pclFddVIA3,FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIA3));
  strcpy(rgFddFlds.pclFddINDO,"D");
  strcpy(rgFddFlds.pclFddFTYP,"D");
  TimeToStr(rgFddFlds.pclFddLSTU,time(NULL)); 
  TimeToStr(rgFddFlds.pclFddCDAT,time(NULL));
  strcpy(rgFddFlds.pclFddUSEU,"FLDHDL");
  strcpy(rgFddFlds.pclFddUSEC,"FLDHDL");
  strcpy(rgFddFlds.pclFddHOPO,pcgHomeAp);
  if (igCheckNatureLength == 1)
  {
     strncpy(pclTTYP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP),2);
     pclTTYP[2] = '\0';
  }
  else
  {
     strcpy(pclTTYP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP));
  }
  TrimRight(pclTTYP);
  SetDNAT(pclTTYP);

  strcpy(pclAftTTYP,FIELDVAL(rgAftArray,pclCurRowAFT,igAftTTYP));

  strcpy(pclAftJFNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftJFNO));
  TrimRight(pclAftJFNO);
  if (strcmp(pclAftJFNO," ") == 0)
  {
     strcpy(rgFddFlds.pclFddDSEQ,"0");
  }

  /********* Save REMP ******************/
  strcpy(pclFddREMP_ORG,rgFddFlds.pclFddREMP);

  /************** Main-Airline INDO =D *************************/

  /************************* Get Chutes */
  ilRC = GetChutes(pclFLNO,pclFddAurn,pclFddChaf,pclFddChat);
  strcpy(rgFddFlds.pclFddBLT1,pclFddChaf);
  strcpy(rgFddFlds.pclFddBLT2,pclFddChat);

  /************************* Get Counters */

  ilRC = GetCDT(pclCDA, pclTTYP, ilMainCshCnt);

  dbg(DEBUG,"%05d: dom_d: ########## pclCDA: <%s>",__LINE__,pclCDA);

  if (strcmp(pcgCheckTermForCntAsgn,"NO") == 0)
  {
     if (strcmp(pclCDA,"D") == 0)
     {
        ilRC = GetDedicatedCounter("D",pclFLNO,pclCKIFDom,pclCKITDom);
        if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
           ilRC = GetDedicatedCounter("I",pclFLNO,pclCKIFInt,pclCKITInt);
     }
     else
     {
        if (strcmp(pclCDA,"C") == 0)
        {
           ilRC = GetCommonCounter("D",pclALC,pclCKIFDom,pclCKITDom,pclDest);
           if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
              ilRC = GetCommonCounter("I",pclALC,pclCKIFInt,pclCKITInt,pclDest);
        }
        else
        {
           if (strcmp(pclCDA,"A") == 0 || strcmp(pclCDA,"X") == 0 || strcmp(pclCDA,"Y") == 0)
           {
              ilRC = GetCommonAndDedicatedCounter("D",pclALC,pclFLNO,pclCKIFDom,pclCKITDom,pclCDA,"M",pclDest);
              if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
                 ilRC = GetCommonAndDedicatedCounter("I",pclALC,pclFLNO,pclCKIFInt,pclCKITInt,pclCDA,"M",pclDest);
           }
        }
     }
  }
  else
  {
     ilRC = GetCounterTerm(pclCDA,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTEV),
                           pclALC,pclFLNO,pclCKIFInt,pclCKITInt);
     strcpy(pclCKIFDom,pclCKIFInt);
     strcpy(pclCKITDom,pclCKITInt);
  }
  if (strcmp(pcgUpdateCheckinFromTo,"YES") == 0)
     ilRC = UpdateCheckInFromTo(pclFddAurn,pclCKIFInt,pclCKITInt,pclCKIFDom,pclCKITDom);
  strcpy(rgFddFlds.pclFddCKIF,pclCKIFDom);
  strcpy(rgFddFlds.pclFddCKIT,pclCKITDom);
  /**************************** end Get Counters */
  if (strcmp(pcgUseGateStatusFields,"YES") == 0)
  {
     strcpy(pclGate2Remark,FIELDVAL(rgAftArray,pclCurRowAFT,igAftRGD2));
     TrimRight(pclGate2Remark);
     if (*pclGate2Remark == '0')
        *pclGate2Remark = ' ';
     if (strstr(FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP),pcgDeparted) != NULL ||
         strstr(FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP),pcgAirborne) != NULL)
        strcpy(pclGate2Remark," ");
     if (*pclGate2Remark == ' ' && strstr(FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP),pcgDelayedGateOpen) != NULL)
        strcpy(pclGate2Remark,pcgDelayed);
     if (strcmp(pclGate2Remark,pcgGateOpened) == 0 &&
         (strstr(FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP),pcgDelayedGateOpen) != NULL ||
          strstr(FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP),pcgDelayed) != NULL))
        strcpy(pclGate2Remark,pcgDelayedGateOpen);
     if (*pclGate2Remark == ' ' &&
         (strstr(FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP),pcgGateOpened) != NULL ||
          strstr(FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP),pcgBoarding) != NULL ||
          strstr(FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP),pcgFinalcall) != NULL ||
          strstr(FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP),pcgGateclosed) != NULL))
     {
        strcpy(pclGate2Remark,"XXX");
        if (strcmp(FIELDVAL(rgAftArray,pclCurRowAFT,igAftETOD)," ") != 0)
        {
           strcpy(pclTmpTime,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTOD));
           TrimRight(pclTmpTime);
           AddSecondsToCEDATime12(pclTmpTime,igDelayTimeDiffDep,1);
           if (strcmp(FIELDVAL(rgAftArray,pclCurRowAFT,igAftETOD), pclTmpTime) > 0)     /*ETOA > STOA ?*/
           {
              strcpy(pclGate2Remark,pcgDelayed);
           }
        }
     }
  }

  strcpy(rgFddFlds.pclFddREMP,pclFddREMP_ORG);

  if (igIntAndDomDisplays != 0)
  {
     if (strcmp(pcgHdlDACO,"STEV") != 0)
     {
        if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
        {
           strcpy(pclOldRemark,FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP));
           strcpy(pclNewRemark,pclOldRemark);
           ilRC = HandleTerminal_D(pclCurRowAFT,pclNewRemark,&ilTerm,&ilTerm2);
           strcpy(pclSavAPC3,rgFddFlds.pclFddAPC3);
           strcpy(pclSavVIA3,rgFddFlds.pclFddVIA3);
           strcpy(pclSavGTD1,rgFddFlds.pclFddGTD1);
           strcpy(pclSavGTD2,rgFddFlds.pclFddGTD2);
           if (strcmp(pcgUseGateStatusFields,"YES") == 0 && *pclGate2Remark != ' ' && *pclSavGTD2 != ' ')
           {
              if (strcmp(pclGate2Remark,"XXX") == 0)
                 strcpy(rgFddFlds.pclFddREMP," ");
              else
                 strcpy(rgFddFlds.pclFddREMP,pclGate2Remark);
           }
           strcpy(rgFddFlds.pclFddINDO,"2");
           if (ilTerm == 2)
           {
              ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
           }
           else if (ilTerm2 == 2 || (strcmp(pcgCheckCounterOnly,"YES") == 0 && strlen(pclCKIFDom) > 1))
           {
              if (strcmp(pcgUseGateStatusFields,"YES") == 0 && *pclGate2Remark != ' ' && *pclSavGTD2 != ' ')
              {
                 if (strcmp(pclGate2Remark,"XXX") == 0)
                    strcpy(rgFddFlds.pclFddREMP," ");
                 else
                    strcpy(rgFddFlds.pclFddREMP,pclGate2Remark);
              }
              if (strcmp(pcgCheckCounterOnly,"YES") != 0)
              {
                 strcpy(rgFddFlds.pclFddAPC3,pclSavVIA3);
                 strcpy(rgFddFlds.pclFddVIA3," ");
                 strcpy(rgFddFlds.pclFddGTD1,pclSavGTD2);
                 strcpy(rgFddFlds.pclFddGTD2," ");
              }
              else
              {
                 strcpy(rgFddFlds.pclFddAPC3,pclSavAPC3);
                 strcpy(rgFddFlds.pclFddVIA3,pclSavVIA3);
                 strcpy(rgFddFlds.pclFddGTD1,pclSavGTD1);
                 strcpy(rgFddFlds.pclFddGTD2,pclSavGTD2);
              }
              ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
/*
              ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
*/
              if (strcmp(pcgCheckCounterOnly,"YES") != 0)
              {
                 strcpy(rgFddFlds.pclFddINDO,"1");
                 strcpy(rgFddFlds.pclFddREMP,"TM1");
                 strcpy(rgFddFlds.pclFddAPC3,pclSavAPC3);
                 strcpy(rgFddFlds.pclFddVIA3," ");
                 strcpy(rgFddFlds.pclFddGTD1," ");
                 strcpy(rgFddFlds.pclFddGTD2," ");
                 strcpy(rgFddFlds.pclFddCKIF," ");
                 strcpy(rgFddFlds.pclFddCKIT," ");
              }
              ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
           }
           else if (ilTerm == 1 && strcmp(pcgShowAllFlightsInT2,"YES") == 0)
           {
              strcpy(rgFddFlds.pclFddINDO,"1");
              strcpy(rgFddFlds.pclFddDACO,"2");
              if (strcmp(pcgCheckCounterOnly,"YES") != 0)
                 strcpy(rgFddFlds.pclFddREMP,"TM1");
              strcpy(rgFddFlds.pclFddAPC3,pclSavAPC3);
              strcpy(rgFddFlds.pclFddVIA3,pclSavVIA3);
              if (strcmp(pcgCheckCounterOnly,"YES") != 0)
              {
                 strcpy(rgFddFlds.pclFddGTD1," ");
                 strcpy(rgFddFlds.pclFddGTD2," ");
                 strcpy(rgFddFlds.pclFddCKIF," ");
                 strcpy(rgFddFlds.pclFddCKIT," ");
              }
              else
              {
                 if (*pclCKIFDom == ' ' && strlen(pcgFixCnt) > 0)
                 {
                    strcpy(rgFddFlds.pclFddCKIF,pcgFixCnt);
                    strcpy(rgFddFlds.pclFddCKIT,pcgFixCnt);
                 }
                 else
                 {
                    strcpy(rgFddFlds.pclFddCKIF,pclCKIFDom);
                    strcpy(rgFddFlds.pclFddCKIT,pclCKITDom);
                 }
              }
              ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
           }
        }
        else
        {
           strcpy(pclOldRemark,FIELDVAL(rgAftArray,pclCurRowAFT,igAftREMP));
           strcpy(pclNewRemark,pclOldRemark);
           ilRC = HandleTerminal_D(pclCurRowAFT,pclNewRemark,&ilTerm,&ilTerm2);
           strcpy(pclSavGTD1,rgFddFlds.pclFddGTD1);
           strcpy(pclSavGTD2,rgFddFlds.pclFddGTD2);
           if (strcmp(pcgUseGateStatusFields,"YES") == 0 && *pclGate2Remark != ' ' && *pclSavGTD2 != ' ')
           {
              if (strcmp(pclGate2Remark,"XXX") == 0)
                 strcpy(rgFddFlds.pclFddREMP," ");
              else
                 strcpy(rgFddFlds.pclFddREMP,pclGate2Remark);
           }
           if (ilTerm == 2)
              ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
        }
     }
     else
        ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
  }

  /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);
  ilRC = ShowFddStruct();*/
  
  /************** Main-Airline INDO =I *************************/

  if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
  {
     strcpy(rgFddFlds.pclFddCKIF,pclCKIFInt);
     strcpy(rgFddFlds.pclFddCKIT,pclCKITInt);
  }
/*
  ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
*/
  if (ilRC != RC_SUCCESS)
  {
     dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
  }
  if (strcmp(pcgHdlDACO,"STEV") == 0)
  {
     strcpy(rgFddFlds.pclFddINDO,"I");
     strcpy(rgFddFlds.pclFddDACO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTEV));
     if (igGenerateTM2 == 1)
     {
        if (strncmp(rgFddFlds.pclFddREMP,pcgCanceled,3) != 0)
        {
           strcpy(rgFddFlds.pclFddREMP,"TM2");
        }
     }
  }
  else
  {
     if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
     {
        strcpy(rgFddFlds.pclFddINDO,"1");
        strcpy(rgFddFlds.pclFddDACO,"1");
        strcpy(rgFddFlds.pclFddREMP,pclOldRemark);
        strcpy(rgFddFlds.pclFddGTD1,pclSavGTD1);
        strcpy(rgFddFlds.pclFddGTD2,pclSavGTD2);
        strcpy(rgFddFlds.pclFddCKIF,pclCKIFInt);
        strcpy(rgFddFlds.pclFddCKIT,pclCKITInt);
        if (strcmp(pclNewRemark,"TM2") == 0 && strcmp(pcgCheckCounterOnly,"YES") != 0)
        {
           strcpy(rgFddFlds.pclFddINDO,"2");
           strcpy(rgFddFlds.pclFddREMP,pclNewRemark);
           strcpy(rgFddFlds.pclFddGTD1," ");
           strcpy(rgFddFlds.pclFddGTD2," ");
           strcpy(rgFddFlds.pclFddCKIF," ");
           strcpy(rgFddFlds.pclFddCKIT," ");
        }
        else if (ilTerm2 == 2 || (strcmp(pcgCheckCounterOnly,"YES") == 0 && strlen(pclCKIFDom) > 1))
        {
           strcpy(rgFddFlds.pclFddINDO,"1");
           if (strcmp(pcgUseGateStatusFields,"YES") == 0)
              strcpy(rgFddFlds.pclFddREMP,pclOldRemark);
           if (strcmp(pcgCheckCounterOnly,"YES") != 0)
           {
              strcpy(rgFddFlds.pclFddAPC3,pclSavAPC3);
              strcpy(rgFddFlds.pclFddVIA3," ");
              strcpy(rgFddFlds.pclFddGTD1,pclSavGTD1);
              strcpy(rgFddFlds.pclFddGTD2," ");
           }
           else
           {
              strcpy(rgFddFlds.pclFddAPC3,pclSavAPC3);
              strcpy(rgFddFlds.pclFddVIA3,pclSavVIA3);
              strcpy(rgFddFlds.pclFddGTD1,pclSavGTD1);
              strcpy(rgFddFlds.pclFddGTD2,pclSavGTD2);
           }
           strcpy(rgFddFlds.pclFddCKIF,pclCKIFInt);
           strcpy(rgFddFlds.pclFddCKIT,pclCKITInt);
           ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
/*
           ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
*/
           if (strcmp(pcgCheckCounterOnly,"YES") != 0)
           {
              strcpy(rgFddFlds.pclFddINDO,"2");
              strcpy(rgFddFlds.pclFddREMP,"TM2");
              strcpy(rgFddFlds.pclFddAPC3,pclSavVIA3);
              strcpy(rgFddFlds.pclFddVIA3," ");
              strcpy(rgFddFlds.pclFddGTD1," ");
              strcpy(rgFddFlds.pclFddGTD2," ");
              strcpy(rgFddFlds.pclFddCKIF," ");
              strcpy(rgFddFlds.pclFddCKIT," ");
           }
        }
     }
     else
     {
        strcpy(rgFddFlds.pclFddDACO,"1");
        if (strcmp(pclNewRemark,"TM2") == 0)
        {
           strcpy(rgFddFlds.pclFddREMP,pclNewRemark);
           strcpy(rgFddFlds.pclFddGTD1," ");
           strcpy(rgFddFlds.pclFddGTD2," ");
           strcpy(rgFddFlds.pclFddCKIF," ");
           strcpy(rgFddFlds.pclFddCKIT," ");
        }
     }
  }

  ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
  /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);
  ilRC = ShowFddStruct();*/

  strcpy(rgFddFlds.pclFddINDO,"I");
  strcpy(rgFddFlds.pclFddREMP,pclFddREMP_ORG); 
  
  /***************** prepare Data and handle Code-Share Flights (INDO=I)***************/

  strcpy(pclAftJFNO,FIELDVAL(rgAftArray,pclCurRowAFT,igAftJFNO));
  strncpy(rgFddFlds.pclFddFLNO,pclAftJFNO,9);
  rgFddFlds.pclFddFLNO[9] = '\0'; 
  TrimRight(rgFddFlds.pclFddFLNO);

  for (ilStart = 9; strlen(rgFddFlds.pclFddFLNO) > 3; ilStart += 9)
  {
     dbg(DEBUG,"%05d: The CODE_SHARE-FLNO <%s>",__LINE__,rgFddFlds.pclFddFLNO);

     strncpy(pclALC,rgFddFlds.pclFddFLNO,3);
     pclALC[3] = '\0';
/*
     strcpy(pclFLNO,rgFddFlds.pclFddFLNO);
     TrimRight(pclFLNO);
*/
     ilMainCshCnt++;
     sprintf(rgFddFlds.pclFddDSEQ,"%d",ilMainCshCnt);
/*
     ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
*/
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
     }
     /*********** here INDO = D ******************/
     strcpy(rgFddFlds.pclFddREMP,pclFddREMP_ORG); 
     strcpy(rgFddFlds.pclFddINDO,"D");

     /************************* Get Counters */
     dbg(DEBUG,"%05d: rgFddFlds.pclFddDNAT: <%s>",__LINE__, rgFddFlds.pclFddDNAT);
     ilRC = GetCDT(pclCDA, pclTTYP, ilMainCshCnt);
     dbg(DEBUG,"%05d: Counterdisplaytyp: 01, nummer: 2, pclCDA: <%s>",__LINE__,pclCDA);
     if (strcmp(pcgCheckTermForCntAsgn,"NO") == 0)
     {
        if (strcmp(pclCDA,"D") == 0)
        {
           ilRC = GetDedicatedCounter("D",pclFLNO,pclCKIFDom,pclCKITDom);
           if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
              ilRC = GetDedicatedCounter("I",pclFLNO,pclCKIFInt,pclCKITInt);
        }
        else
        {
           if (strcmp(pclCDA,"C") == 0)
           {
              ilRC = GetCommonCounter("D",pclALC,pclCKIFDom,pclCKITDom,pclDest);
              if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
                 ilRC = GetCommonCounter("I",pclALC,pclCKIFInt,pclCKITInt,pclDest);
           }
           else
           {
              if (strcmp(pclCDA,"A") == 0 || strcmp(pclCDA,"X") == 0 || strcmp(pclCDA,"Y") == 0)
              {
                 ilRC = GetCommonAndDedicatedCounter("D",pclALC,pclFLNO,pclCKIFDom,pclCKITDom,pclCDA,"J",pclDest);
                 if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
                    ilRC = GetCommonAndDedicatedCounter("I",pclALC,pclFLNO,pclCKIFInt,pclCKITInt,pclCDA,"J",pclDest);
              }
              else
              {
                 if (strcmp(pclCDA,"S") == 0)
                 {
/*
                    strcpy(pclCKIFInt,pcgCKIF);
                    strcpy(pclCKITInt,pcgCKIT);
                    strcpy(pclCKIFDom,pcgCKIF);
                    strcpy(pclCKITDom,pcgCKIT);
*/
                 }
                 else
                 {
                    if (strcmp(pclCDA,"P") == 0)
                    {
                       strcpy(pclCKIFInt,pcgCKIF_C);
                       strcpy(pclCKITInt,pcgCKIT_C);
                       strcpy(pclCKIFDom,pcgCKIF_C);
                       strcpy(pclCKITDom,pcgCKIT_C);
                    }
                    else
                    {
                       if (strcmp(pclCDA,"R") == 0)
                       {
                          strcpy(pclCKIFInt,pcgCKIF_D);
                          strcpy(pclCKITInt,pcgCKIT_D);
                          strcpy(pclCKIFDom,pcgCKIF_D);
                          strcpy(pclCKITDom,pcgCKIT_D);
                       }
                       else
                       {
                          strcpy(pclCKIFInt," ");
                          strcpy(pclCKITInt," ");
                          strcpy(pclCKIFDom," ");
                          strcpy(pclCKITDom," ");
                       }
                    }
                 }
              }
           }
        }
     }
     else
     {
        ilRC = GetCounterTerm(pclCDA,FIELDVAL(rgAftArray,pclCurRowAFT,igAftSTEV),
                              pclALC,pclFLNO,pclCKIFInt,pclCKITInt);
        strcpy(pclCKIFDom,pclCKIFInt);
        strcpy(pclCKITDom,pclCKITInt);
     }
     strcpy(rgFddFlds.pclFddCKIF,pclCKIFDom);
     strcpy(rgFddFlds.pclFddCKIT,pclCKITDom);
     /**************************** end Get Counters */

     if (igIntAndDomDisplays != 0)
     {
        if (strcmp(pcgHdlDACO,"STEV") != 0)
        {
           if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
           {
              strcpy(rgFddFlds.pclFddDACO,"2");
              if (ilTerm == 2)
              {
                 if (strcmp(pcgUseGateStatusFields,"YES") == 0 && *pclGate2Remark != ' ' && *pclSavGTD2 != ' ')
                 {
                    if (strcmp(pclGate2Remark,"XXX") == 0)
                       strcpy(rgFddFlds.pclFddREMP," ");
                    else
                       strcpy(rgFddFlds.pclFddREMP,pclGate2Remark);
                 }
                 else
                    strcpy(rgFddFlds.pclFddREMP,pclOldRemark);
                 strcpy(rgFddFlds.pclFddGTD1,pclSavGTD1);
                 strcpy(rgFddFlds.pclFddGTD2,pclSavGTD2);
                 strcpy(rgFddFlds.pclFddCKIF,pclCKIFDom);
                 strcpy(rgFddFlds.pclFddCKIT,pclCKITDom);
                 strcpy(rgFddFlds.pclFddINDO,"2");
                 ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
              }
              else if (ilTerm2 == 2 || (strcmp(pcgCheckCounterOnly,"YES") == 0 && strlen(pclCKIFDom) > 1))
              {
                 if (strcmp(pcgUseGateStatusFields,"YES") == 0 && *pclGate2Remark != ' ' && *pclSavGTD2 != ' ')
                 {
                    if (strcmp(pclGate2Remark,"XXX") == 0)
                       strcpy(rgFddFlds.pclFddREMP," ");
                    else
                       strcpy(rgFddFlds.pclFddREMP,pclGate2Remark);
                 }
                 strcpy(rgFddFlds.pclFddINDO,"2");
                 if (strcmp(pcgCheckCounterOnly,"YES") != 0)
                 {
                    strcpy(rgFddFlds.pclFddAPC3,pclSavVIA3);
                    strcpy(rgFddFlds.pclFddVIA3," ");
                    strcpy(rgFddFlds.pclFddGTD1,pclSavGTD2);
                    strcpy(rgFddFlds.pclFddGTD2," ");
                 }
                 else
                 {
                    strcpy(rgFddFlds.pclFddAPC3,pclSavAPC3);
                    strcpy(rgFddFlds.pclFddVIA3,pclSavVIA3);
                    strcpy(rgFddFlds.pclFddGTD1,pclSavGTD1);
                    strcpy(rgFddFlds.pclFddGTD2,pclSavGTD2);
                 }
                 strcpy(rgFddFlds.pclFddCKIF,pclCKIFDom);
                 strcpy(rgFddFlds.pclFddCKIT,pclCKITDom);
                 ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
/*
                 ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
*/
                 if (strcmp(pcgCheckCounterOnly,"YES") != 0)
                 {
                    strcpy(rgFddFlds.pclFddINDO,"1");
                    strcpy(rgFddFlds.pclFddREMP,"TM1");
                    strcpy(rgFddFlds.pclFddAPC3,pclSavAPC3);
                    strcpy(rgFddFlds.pclFddVIA3," ");
                    strcpy(rgFddFlds.pclFddGTD1," ");
                    strcpy(rgFddFlds.pclFddGTD2," ");
                    strcpy(rgFddFlds.pclFddCKIF," ");
                    strcpy(rgFddFlds.pclFddCKIT," ");
                 }
                 ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
              }
              else if (ilTerm == 1 && strcmp(pcgShowAllFlightsInT2,"YES") == 0)
              {
                 strcpy(rgFddFlds.pclFddINDO,"1");
                 strcpy(rgFddFlds.pclFddDACO,"2");
                 if (strcmp(pcgCheckCounterOnly,"YES") != 0)
                    strcpy(rgFddFlds.pclFddREMP,"TM1");
                 strcpy(rgFddFlds.pclFddAPC3,pclSavAPC3);
                 strcpy(rgFddFlds.pclFddVIA3,pclSavVIA3);
                 if (strcmp(pcgCheckCounterOnly,"YES") != 0)
                 {
                    strcpy(rgFddFlds.pclFddGTD1," ");
                    strcpy(rgFddFlds.pclFddGTD2," ");
                    strcpy(rgFddFlds.pclFddCKIF," ");
                    strcpy(rgFddFlds.pclFddCKIT," ");
                 }
                 else
                 {
                    if (*pclCKIFDom == ' ' && strlen(pcgFixCnt) > 0)
                    {
                       strcpy(rgFddFlds.pclFddCKIF,pcgFixCnt);
                       strcpy(rgFddFlds.pclFddCKIT,pcgFixCnt);
                    }
                    else
                    {
                       strcpy(rgFddFlds.pclFddCKIF,pclCKIFDom);
                       strcpy(rgFddFlds.pclFddCKIT,pclCKITDom);
                    }
                 }
                 ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
              }
           }
           else
           {
              strcpy(rgFddFlds.pclFddDACO,"2");
              if (strcmp(pcgUseGateStatusFields,"YES") == 0 && *pclGate2Remark != ' ' && *pclSavGTD2 != ' ')
              {
                 if (strcmp(pclGate2Remark,"XXX") == 0)
                    strcpy(rgFddFlds.pclFddREMP," ");
                 else
                    strcpy(rgFddFlds.pclFddREMP,pclGate2Remark);
              }
              else
                 strcpy(rgFddFlds.pclFddREMP,pclOldRemark);
              strcpy(rgFddFlds.pclFddGTD1,pclSavGTD1);
              strcpy(rgFddFlds.pclFddGTD2,pclSavGTD2);
              strcpy(rgFddFlds.pclFddCKIF,pclCKIFDom);
              strcpy(rgFddFlds.pclFddCKIT,pclCKITDom);
              if (ilTerm == 2)
                 ilRC = StructAddRowPartFDDDep(plpRowNumFDD); /* Hier checken */
           }
        }
        else
           ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
     }
     /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);
     ilRC = ShowFddStruct();*/

     /*********** here INDO = I ******************/
     if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
     {
        strcpy(rgFddFlds.pclFddCKIF,pclCKIFInt);
        strcpy(rgFddFlds.pclFddCKIT,pclCKITInt);
     }
/*
     ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
*/
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
     }
     strcpy(rgFddFlds.pclFddINDO,"I");
      if (strcmp(pcgHdlDACO,"STEV") == 0)
      {
         if (igGenerateTM2 == 1)
         {
            if (strncmp(rgFddFlds.pclFddREMP,pcgCanceled,3) != 0)
            {
                strcpy(rgFddFlds.pclFddREMP,"TM2");
            }
         }
      }
      else
      {
         if (strcmp(pcgDisplayInT1AndT2,"YES") == 0)
         {
           strcpy(rgFddFlds.pclFddINDO,"1");
           strcpy(rgFddFlds.pclFddDACO,"1");
           strcpy(rgFddFlds.pclFddREMP,pclOldRemark);
           strcpy(rgFddFlds.pclFddGTD1,pclSavGTD1);
           strcpy(rgFddFlds.pclFddGTD2,pclSavGTD2);
           strcpy(rgFddFlds.pclFddCKIF,pclCKIFInt);
           strcpy(rgFddFlds.pclFddCKIT,pclCKITInt);
            if (strcmp(pclNewRemark,"TM2") == 0 && strcmp(pcgCheckCounterOnly,"YES") != 0)
            {
               strcpy(rgFddFlds.pclFddINDO,"2");
               strcpy(rgFddFlds.pclFddREMP,pclNewRemark);
               strcpy(rgFddFlds.pclFddGTD1," ");
               strcpy(rgFddFlds.pclFddGTD2," ");
               strcpy(rgFddFlds.pclFddCKIF," ");
               strcpy(rgFddFlds.pclFddCKIT," ");
            }
            else if (ilTerm2 == 2 || (strcmp(pcgCheckCounterOnly,"YES") == 0 && strlen(pclCKIFDom) > 1))
            {
               strcpy(rgFddFlds.pclFddINDO,"1");
               if (strcmp(pcgUseGateStatusFields,"YES") == 0)
                  strcpy(rgFddFlds.pclFddREMP,pclOldRemark);
               if (strcmp(pcgCheckCounterOnly,"YES") != 0)
               {
                  strcpy(rgFddFlds.pclFddAPC3,pclSavAPC3);
                  strcpy(rgFddFlds.pclFddVIA3," ");
                  strcpy(rgFddFlds.pclFddGTD1,pclSavGTD1);
                  strcpy(rgFddFlds.pclFddGTD2," ");
               }
               else
               {
                  strcpy(rgFddFlds.pclFddAPC3,pclSavAPC3);
                  strcpy(rgFddFlds.pclFddVIA3,pclSavVIA3);
                  strcpy(rgFddFlds.pclFddGTD1,pclSavGTD1);
                  strcpy(rgFddFlds.pclFddGTD2,pclSavGTD2);
               }
               strcpy(rgFddFlds.pclFddCKIF,pclCKIFInt);
               strcpy(rgFddFlds.pclFddCKIT,pclCKITInt);
               ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
/*
               ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
*/
               if (strcmp(pcgCheckCounterOnly,"YES") != 0)
               {
                  strcpy(rgFddFlds.pclFddINDO,"2");
                  strcpy(rgFddFlds.pclFddREMP,"TM2");
                  strcpy(rgFddFlds.pclFddAPC3,pclSavVIA3);
                  strcpy(rgFddFlds.pclFddVIA3," ");
                  strcpy(rgFddFlds.pclFddGTD1," ");
                  strcpy(rgFddFlds.pclFddGTD2," ");
                  strcpy(rgFddFlds.pclFddCKIF," ");
                  strcpy(rgFddFlds.pclFddCKIT," ");
               }
            }
         }
         else
         {
            strcpy(rgFddFlds.pclFddDACO,"1");
            if (strcmp(pclNewRemark,"TM2") == 0)
            {
               strcpy(rgFddFlds.pclFddREMP,pclNewRemark);
               strcpy(rgFddFlds.pclFddGTD1," ");
               strcpy(rgFddFlds.pclFddGTD2," ");
               strcpy(rgFddFlds.pclFddCKIF," ");
               strcpy(rgFddFlds.pclFddCKIT," ");
            }
         }
      }
 
     ilRC = StructAddRowPartFDDDep(plpRowNumFDD);
     /*dbg(TRACE,"%05d:  ****** NowShowFddStruct !! *********",__LINE__);
     ilRC = ShowFddStruct();*/
     strcpy(rgFddFlds.pclFddREMP,pclFddREMP_ORG); 
     strncpy(rgFddFlds.pclFddFLNO,&pclAftJFNO[ilStart],9);
     rgFddFlds.pclFddFLNO[9] = '\0'; 
     TrimRight(rgFddFlds.pclFddFLNO);
  }
  /* UpdateFLDTAB(pclCurRowAFT); */
  dbg(DEBUG,"%05d: ================== End of HandleFLTI_Dom_D !!!  ===================",__LINE__);

  return (ilRC);
}/*end of HandleFLTI_Dom_D */


/******************************************************************************/
/* The InitFddStruct routine                                                    */
/******************************************************************************/
static int InitFddStruct()
{
  int ilRC = RC_SUCCESS;

  memset(&rgFddFlds,0x00,sizeof(rgFddFlds));

    strcpy(rgFddFlds.pclFddADID, "\0");
    strcpy(rgFddFlds.pclFddAIRB, "\0");
    strcpy(rgFddFlds.pclFddAPC3, "\0");
    strcpy(rgFddFlds.pclFddAURN, "0");   /*because AURN is Number in FDDTAB*/
    strcpy(rgFddFlds.pclFddBLT1, "\0");
    strcpy(rgFddFlds.pclFddBLT2, "\0");
    strcpy(rgFddFlds.pclFddCDAT, "\0");
    strcpy(rgFddFlds.pclFddCKIF, "\0");
    strcpy(rgFddFlds.pclFddCKIT, "\0");
    strcpy(rgFddFlds.pclFddDACO, "\0");
    strcpy(rgFddFlds.pclFddDNAT, "\0");                                                                          
    strcpy(rgFddFlds.pclFddETOF, "\0");
    strcpy(rgFddFlds.pclFddFLNO, "\0");
    strcpy(rgFddFlds.pclFddFTYP, "\0");
    strcpy(rgFddFlds.pclFddGTD1, "\0"); 
    strcpy(rgFddFlds.pclFddGTD2, "\0");
    strcpy(rgFddFlds.pclFddHOPO, "\0");
    strcpy(rgFddFlds.pclFddINDO, "\0");
    strcpy(rgFddFlds.pclFddLAND, "\0");
    strcpy(rgFddFlds.pclFddLSTU, "\0");
    strcpy(rgFddFlds.pclFddOFBL, "\0");
    strcpy(rgFddFlds.pclFddONBL, "\0");
    strcpy(rgFddFlds.pclFddREMP, "\0");
    strcpy(rgFddFlds.pclFddSTOF, "\0");
    strcpy(rgFddFlds.pclFddTIFF, "\0");
    strcpy(rgFddFlds.pclFddURNO, "\0");
    strcpy(rgFddFlds.pclFddUSEC, "\0");
    strcpy(rgFddFlds.pclFddUSEU, "\0");
    strcpy(rgFddFlds.pclFddVIA3, "\0");
    strcpy(rgFddFlds.pclFddWRO1, "\0"); 
    strcpy(rgFddFlds.pclFddDSEQ, "0");   /*because it is a NUMBER*/ 
    strcpy(rgFddFlds.pclFddRMTI, "\0");
    strcpy(rgFddFlds.pclFddGD1B, "\0"); 
    strcpy(rgFddFlds.pclFddGD2B, "\0"); 
    strcpy(rgFddFlds.pclFddNODE, "\0"); 
    strcpy(rgFddFlds.pclFddOBL1, "\0"); 
    strcpy(rgFddFlds.pclFddOBL2, "\0"); 
    strcpy(rgFddFlds.pclFddOGT1, "\0"); 
    strcpy(rgFddFlds.pclFddOGT2, "\0"); 
  
    /*  pclFddREMP_ORG = "\0"; */ /*This is REMP Original, after ReGet from FDD-TAB*/

  return (ilRC);
}



/******************************************************************************/
/* The ShowFddStruct routine                                                    */
/******************************************************************************/
static int ShowFddStruct()
{
  int ilRC = RC_SUCCESS;

    dbg(DEBUG,"FddStruct: pclFddADID <%s>",rgFddFlds.pclFddADID);
    dbg(DEBUG,"FddStruct: pclFddAIRB <%s>",rgFddFlds.pclFddAIRB);
    dbg(DEBUG,"FddStruct: pclFddAPC3 <%s>",rgFddFlds.pclFddAPC3);
    dbg(DEBUG,"FddStruct: pclFddAURN <%s>",rgFddFlds.pclFddAURN);   /*because AURN is Number in FDDTAB*/
    dbg(DEBUG,"FddStruct: pclFddBLT1 <%s>",rgFddFlds.pclFddBLT1);
    dbg(DEBUG,"FddStruct: pclFddBLT2 <%s>",rgFddFlds.pclFddBLT2);
    dbg(DEBUG,"FddStruct: pclFddCDAT <%s>",rgFddFlds.pclFddCDAT);
    dbg(DEBUG,"FddStruct: pclFddCKIF <%s>",rgFddFlds.pclFddCKIF);
    dbg(DEBUG,"FddStruct: pclFddCKIT <%s>",rgFddFlds.pclFddCKIT);
    dbg(DEBUG,"FddStruct: pclFddDACO <%s>",rgFddFlds.pclFddDACO);
    dbg(DEBUG,"FddStruct: pclFddDNAT <%s>",rgFddFlds.pclFddDNAT);
    dbg(DEBUG,"FddStruct: pclFddETOF <%s>",rgFddFlds.pclFddETOF);
    dbg(DEBUG,"FddStruct: pclFddFLNO <%s>",rgFddFlds.pclFddETOF);
    dbg(DEBUG,"FddStruct: pclFddFTYP <%s>",rgFddFlds.pclFddFTYP);
    dbg(DEBUG,"FddStruct: pclFddGTD1 <%s>",rgFddFlds.pclFddGTD1); 
    dbg(DEBUG,"FddStruct: pclFddGTD2 <%s>",rgFddFlds.pclFddGTD2);
    dbg(DEBUG,"FddStruct: pclFddHOPO <%s>",rgFddFlds.pclFddHOPO);
    dbg(DEBUG,"FddStruct: pclFddINDO <%s>",rgFddFlds.pclFddINDO);
    dbg(DEBUG,"FddStruct: pclFddLAND <%s>",rgFddFlds.pclFddLAND);
    dbg(DEBUG,"FddStruct: pclFddLSTU <%s>",rgFddFlds.pclFddLSTU);
    dbg(DEBUG,"FddStruct: pclFddOFBL <%s>",rgFddFlds.pclFddOFBL);
    dbg(DEBUG,"FddStruct: pclFddONBL <%s>",rgFddFlds.pclFddONBL);
    dbg(DEBUG,"FddStruct: pclFddREMP <%s>",rgFddFlds.pclFddREMP);
    dbg(DEBUG,"FddStruct: pclFddSTOF <%s>",rgFddFlds.pclFddSTOF);
    dbg(DEBUG,"FddStruct: pclFddTIFF <%s>",rgFddFlds.pclFddTIFF);
    dbg(DEBUG,"FddStruct: pclFddURNO <%s>",rgFddFlds.pclFddURNO);
    dbg(DEBUG,"FddStruct: pclFddUSEC <%s>",rgFddFlds.pclFddUSEC);
    dbg(DEBUG,"FddStruct: pclFddUSEU <%s>",rgFddFlds.pclFddUSEU);
    dbg(DEBUG,"FddStruct: pclFddVIA3 <%s>",rgFddFlds.pclFddVIA3);
    dbg(DEBUG,"FddStruct: pclFddWRO1 <%s>",rgFddFlds.pclFddWRO1); 
    dbg(DEBUG,"FddStruct: pclFddDSEQ <%s>",rgFddFlds.pclFddDSEQ);
    dbg(DEBUG,"FddStruct: pclFddRMTI <%s>",rgFddFlds.pclFddRMTI);  
    dbg(DEBUG,"FddStruct: pclFddGD1B <%s>",rgFddFlds.pclFddGD1B); 
    dbg(DEBUG,"FddStruct: pclFddGD12 <%s>",rgFddFlds.pclFddGD2B); 
    dbg(DEBUG,"FddStruct: pclFddNODE <%s>",rgFddFlds.pclFddNODE); 
    dbg(DEBUG,"FddStruct: pclFddOBL1 <%s>",rgFddFlds.pclFddOBL1); 
    dbg(DEBUG,"FddStruct: pclFddOBL2 <%s>",rgFddFlds.pclFddOBL2); 
    dbg(DEBUG,"FddStruct: pclFddOGT1 <%s>",rgFddFlds.pclFddOGT1); 
    dbg(DEBUG,"FddStruct: pclFddOGT2 <%s>",rgFddFlds.pclFddOGT2); 

 

  return (ilRC);

}

/******************************************************************************/
/* The StructAddRowPartArr routine                                               */
/******************************************************************************/
static int StructAddRowPartFDDArr(long *plpRowNumFDD)
{
  int ilRC = RC_SUCCESS;
  long llRowNumFDD = ARR_FIRST;
  char *pclRowFDD = NULL;
  int ilFound;
  char pclFddADID[32] = "\0";
  char pclFddAURN[32] = "\0";
  char pclFddDACO[32] = "\0";
  char pclFddFLNO[32] = "\0";
  char pclFddFTYP[32] = "\0";
  char pclFddINDO[32] = "\0";
  char pclFddDSEQ[32] = "\0";
  char pclNewADID[32] = "\0";
  char pclNewAURN[32] = "\0";
  char pclNewDACO[32] = "\0";
  char pclNewFLNO[32] = "\0";
  char pclNewFTYP[32] = "\0";
  char pclNewINDO[32] = "\0";
  char pclNewDSEQ[32] = "\0";

  /*   Search FDD Record */
  ilFound = 0;
  llRowNumFDD = ARR_FIRST;
  while(ilFound == 0 && CEDAArrayGetRowPointer(&rgFddArray.rrArrayHandle,
                                               rgFddArray.crArrayName,
                                               llRowNumFDD,(void *)&pclRowFDD) == RC_SUCCESS)
  {
     llRowNumFDD = ARR_CURRENT;
     strcpy(pclFddADID,FIELDVAL(rgFddArray,pclRowFDD,igFddADID));
     TrimRight(pclFddADID);
     strcpy(pclFddAURN,FIELDVAL(rgFddArray,pclRowFDD,igFddAURN));
     TrimRight(pclFddAURN);
     strcpy(pclFddDACO,FIELDVAL(rgFddArray,pclRowFDD,igFddDACO));
     TrimRight(pclFddDACO);
     strcpy(pclFddFLNO,FIELDVAL(rgFddArray,pclRowFDD,igFddFLNO));
     TrimRight(pclFddFLNO);
     strcpy(pclFddFTYP,FIELDVAL(rgFddArray,pclRowFDD,igFddFTYP));
     TrimRight(pclFddFTYP);
     strcpy(pclFddINDO,FIELDVAL(rgFddArray,pclRowFDD,igFddINDO));
     TrimRight(pclFddINDO);
     strcpy(pclFddDSEQ,FIELDVAL(rgFddArray,pclRowFDD,igFddDSEQ));
     TrimRight(pclFddDSEQ);
     strcpy(pclNewADID,rgFddFlds.pclFddADID);
     TrimRight(pclNewADID);
     strcpy(pclNewAURN,rgFddFlds.pclFddAURN);
     TrimRight(pclNewAURN);
     strcpy(pclNewDACO,rgFddFlds.pclFddDACO);
     TrimRight(pclNewDACO);
     strcpy(pclNewFLNO,rgFddFlds.pclFddFLNO);
     TrimRight(pclNewFLNO);
     strcpy(pclNewFTYP,rgFddFlds.pclFddFTYP);
     TrimRight(pclNewFTYP);
     strcpy(pclNewINDO,rgFddFlds.pclFddINDO);
     TrimRight(pclNewINDO);
     strcpy(pclNewDSEQ,rgFddFlds.pclFddDSEQ);
     TrimRight(pclNewDSEQ);
     dbg(DEBUG,"<%s><%s><%s><%s><%s><%s>",
         pclFddADID,pclFddAURN,pclFddDACO,pclFddFLNO,pclFddFTYP,pclFddINDO,pclFddDSEQ);
     dbg(DEBUG,"<%s><%s><%s><%s><%s><%s>",
         pclNewADID,pclNewAURN,pclNewDACO,pclNewFLNO,
         pclNewFTYP,pclNewINDO,pclNewDSEQ);
     if (strcmp(pclFddADID,pclNewADID) == 0 &&
         strcmp(pclFddAURN,pclNewAURN) == 0 &&
         strcmp(pclFddDACO,pclNewDACO) == 0 &&
         strcmp(pclFddFLNO,pclNewFLNO) == 0 &&
         strcmp(pclFddFTYP,pclNewFTYP) == 0 &&
         strcmp(pclFddINDO,pclNewINDO) == 0 &&
         strcmp(pclFddDSEQ,pclNewDSEQ) == 0)
     {
        dbg(DEBUG,"StructAddRowPartFDDArr: <%s> found",pclFddFLNO);
        ilFound = 1;
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "APC3",llRowNumFDD,rgFddFlds.pclFddAPC3);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "BLT1",llRowNumFDD,rgFddFlds.pclFddBLT1);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "BLT2",llRowNumFDD,rgFddFlds.pclFddBLT2);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "DNAT",llRowNumFDD,rgFddFlds.pclFddDNAT);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "ETOF",llRowNumFDD,rgFddFlds.pclFddETOF);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "LAND",llRowNumFDD,rgFddFlds.pclFddLAND);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "ONBL",llRowNumFDD,rgFddFlds.pclFddONBL);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "REMP",llRowNumFDD,rgFddFlds.pclFddREMP);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "STOF",llRowNumFDD,rgFddFlds.pclFddSTOF);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "TIFF",llRowNumFDD,rgFddFlds.pclFddTIFF);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "VIA3",llRowNumFDD,rgFddFlds.pclFddVIA3);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "RMTI",llRowNumFDD,rgFddFlds.pclFddRMTI);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "GD1B",llRowNumFDD,rgFddFlds.pclFddGD1B);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "GD2B",llRowNumFDD,rgFddFlds.pclFddGD2B);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "NODE",llRowNumFDD," ");
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "OBL1",llRowNumFDD,rgFddFlds.pclFddOBL1);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "OBL2",llRowNumFDD,rgFddFlds.pclFddOBL2);
     }
     llRowNumFDD = ARR_NEXT;
  }

  if (ilFound == 0 && igInsertIntoFDD == TRUE)
  {
     ilRC = StructAddRowPartFDD(plpRowNumFDD);
  }

  return (ilRC);
} /* end of StructAddRowPartFddArr */
 

/******************************************************************************/
/* The StructAddRowPartDep routine                                               */
/******************************************************************************/
static int StructAddRowPartFDDDep(long *plpRowNumFDD)
{
  int ilRC = RC_SUCCESS;
  long llRowNumFDD = ARR_FIRST;
  char *pclRowFDD = NULL;
  int ilFound;
  char pclFddADID[32] = "\0";
  char pclFddAURN[32] = "\0";
  char pclFddDACO[32] = "\0";
  char pclFddFLNO[32] = "\0";
  char pclFddFTYP[32] = "\0";
  char pclFddINDO[32] = "\0";
  char pclFddDSEQ[32] = "\0";
  char pclNewADID[32] = "\0";
  char pclNewAURN[32] = "\0";
  char pclNewDACO[32] = "\0";
  char pclNewFLNO[32] = "\0";
  char pclNewFTYP[32] = "\0";
  char pclNewINDO[32] = "\0";
  char pclNewDSEQ[32] = "\0";

  /*   Search FDD Record */
  ilFound = 0;
  llRowNumFDD = ARR_FIRST;
  while(ilFound == 0 && CEDAArrayGetRowPointer(&rgFddArray.rrArrayHandle,
                                               rgFddArray.crArrayName,
                                               llRowNumFDD,(void *)&pclRowFDD) == RC_SUCCESS)
  {
     llRowNumFDD = ARR_CURRENT;
     strcpy(pclFddADID,FIELDVAL(rgFddArray,pclRowFDD,igFddADID));
     TrimRight(pclFddADID);
     strcpy(pclFddAURN,FIELDVAL(rgFddArray,pclRowFDD,igFddAURN));
     TrimRight(pclFddAURN);
     strcpy(pclFddDACO,FIELDVAL(rgFddArray,pclRowFDD,igFddDACO));
     TrimRight(pclFddDACO);
     strcpy(pclFddFLNO,FIELDVAL(rgFddArray,pclRowFDD,igFddFLNO));
     TrimRight(pclFddFLNO);
     strcpy(pclFddFTYP,FIELDVAL(rgFddArray,pclRowFDD,igFddFTYP));
     TrimRight(pclFddFTYP);
     strcpy(pclFddINDO,FIELDVAL(rgFddArray,pclRowFDD,igFddINDO));
     TrimRight(pclFddINDO);
     strcpy(pclFddDSEQ,FIELDVAL(rgFddArray,pclRowFDD,igFddDSEQ));
     TrimRight(pclFddDSEQ);
     strcpy(pclNewADID,rgFddFlds.pclFddADID);
     TrimRight(pclNewADID);
     strcpy(pclNewAURN,rgFddFlds.pclFddAURN);
     TrimRight(pclNewAURN);
     strcpy(pclNewDACO,rgFddFlds.pclFddDACO);
     TrimRight(pclNewDACO);
     strcpy(pclNewFLNO,rgFddFlds.pclFddFLNO);
     TrimRight(pclNewFLNO);
     strcpy(pclNewFTYP,rgFddFlds.pclFddFTYP);
     TrimRight(pclNewFTYP);
     strcpy(pclNewINDO,rgFddFlds.pclFddINDO);
     TrimRight(pclNewINDO);
     strcpy(pclNewDSEQ,rgFddFlds.pclFddDSEQ);
     TrimRight(pclNewDSEQ);
     dbg(DEBUG,"<%s><%s><%s><%s><%s><%s>",
         pclFddADID,pclFddAURN,pclFddDACO,pclFddFLNO,pclFddFTYP,pclFddINDO,pclFddDSEQ);
     dbg(DEBUG,"<%s><%s><%s><%s><%s><%s>",
         pclNewADID,pclNewAURN,pclNewDACO,pclNewFLNO,
         pclNewFTYP,pclNewINDO,pclNewDSEQ);
     dbg(DEBUG,"--------------------------------------------------------------------");
     if (strcmp(pclFddADID,pclNewADID) == 0 &&
         strcmp(pclFddAURN,pclNewAURN) == 0 &&
         strcmp(pclFddDACO,pclNewDACO) == 0 &&
         strcmp(pclFddFLNO,pclNewFLNO) == 0 &&
         strcmp(pclFddFTYP,pclNewFTYP) == 0 &&
         strcmp(pclFddINDO,pclNewINDO) == 0 &&
         strcmp(pclFddDSEQ,pclNewDSEQ) == 0)
     {
        dbg(DEBUG,"StructAddRowPartFDDDep: <%s> found",pclFddFLNO);
        ilFound = 1;
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "AIRB",llRowNumFDD,rgFddFlds.pclFddAIRB);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "APC3",llRowNumFDD,rgFddFlds.pclFddAPC3);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "CKIF",llRowNumFDD,rgFddFlds.pclFddCKIF);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "CKIT",llRowNumFDD,rgFddFlds.pclFddCKIT);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "DNAT",llRowNumFDD,rgFddFlds.pclFddDNAT);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "ETOF",llRowNumFDD,rgFddFlds.pclFddETOF);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "GTD1",llRowNumFDD,rgFddFlds.pclFddGTD1);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "GTD2",llRowNumFDD,rgFddFlds.pclFddGTD2);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "OFBL",llRowNumFDD,rgFddFlds.pclFddOFBL);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "REMP",llRowNumFDD,rgFddFlds.pclFddREMP);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "STOF",llRowNumFDD,rgFddFlds.pclFddSTOF);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "TIFF",llRowNumFDD,rgFddFlds.pclFddTIFF);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "VIA3",llRowNumFDD,rgFddFlds.pclFddVIA3);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "WRO1",llRowNumFDD,rgFddFlds.pclFddWRO1);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "RMTI",llRowNumFDD,rgFddFlds.pclFddRMTI);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "GD1B",llRowNumFDD,rgFddFlds.pclFddGD1B);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "GD2B",llRowNumFDD,rgFddFlds.pclFddGD2B);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "NODE",llRowNumFDD," ");
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "OGT1",llRowNumFDD,rgFddFlds.pclFddOGT1);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "OGT2",llRowNumFDD,rgFddFlds.pclFddOGT2);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "BLT1",llRowNumFDD,rgFddFlds.pclFddBLT1);
        ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "BLT2",llRowNumFDD,rgFddFlds.pclFddBLT2);
     }
     llRowNumFDD = ARR_NEXT;
  }

  if (ilFound == 0 && igInsertIntoFDD == TRUE)
  {
     ilRC = StructAddRowPartFDD(plpRowNumFDD);
  }

  return (ilRC);
} /* end of StructAddRowPartFddDep */
 



/******************************************************************************/
/* The StructAddRowPart routine                                               */
/******************************************************************************/
static int StructAddRowPartFDD(long *plpRowNumFDD)
{
  int ilRC = RC_SUCCESS;
  char pclRowBuffer[4096];

  ilRC = GetNextValues(rgFddFlds.pclFddURNO,1);
  sprintf(pclRowBuffer,
      "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
      rgFddFlds.pclFddADID,
      rgFddFlds.pclFddAIRB,
      rgFddFlds.pclFddAPC3,
      rgFddFlds.pclFddAURN, 
      rgFddFlds.pclFddBLT1,
      rgFddFlds.pclFddBLT2,
      rgFddFlds.pclFddCDAT,
      rgFddFlds.pclFddCKIF,
      rgFddFlds.pclFddCKIT,
      rgFddFlds.pclFddDACO,
      rgFddFlds.pclFddDNAT,                                                                          
      rgFddFlds.pclFddETOF,
      rgFddFlds.pclFddFLNO,
      rgFddFlds.pclFddFTYP,
      rgFddFlds.pclFddGTD1,
      rgFddFlds.pclFddGTD2,
      rgFddFlds.pclFddHOPO,
      rgFddFlds.pclFddINDO,
      rgFddFlds.pclFddLAND,
      rgFddFlds.pclFddLSTU,
      rgFddFlds.pclFddOFBL,
      rgFddFlds.pclFddONBL,
      rgFddFlds.pclFddREMP,
      rgFddFlds.pclFddSTOF,
      rgFddFlds.pclFddTIFF,
      rgFddFlds.pclFddURNO,
      rgFddFlds.pclFddUSEC,
      rgFddFlds.pclFddUSEU,
      rgFddFlds.pclFddVIA3,
      rgFddFlds.pclFddWRO1, 
      rgFddFlds.pclFddDSEQ,
      rgFddFlds.pclFddRMTI, 
      rgFddFlds.pclFddGD1B,
      rgFddFlds.pclFddGD2B,
      rgFddFlds.pclFddNODE,
      rgFddFlds.pclFddOBL1,
      rgFddFlds.pclFddOBL2,
      rgFddFlds.pclFddOGT1,
      rgFddFlds.pclFddOGT2);
 

  ilRC = CEDAArrayAddRowPart(&rgFddArray.rrArrayHandle, rgFddArray.crArrayName,plpRowNumFDD,
                 FDD_FIELDS, pclRowBuffer);
  if (ilRC != RC_SUCCESS)
  {
     dbg(TRACE,"%05d: StructAddRowPart:CEDAArrayAddRowPart-FDD failed !! RowNum: %d",
         __LINE__, *plpRowNumFDD);
     dbg(TRACE,"StructAddRowPart:Fields = <%s>",FDD_FIELDS);
     dbg(TRACE,"StructAddRowPart:Data = <%s>",pclRowBuffer);
  }


  return (ilRC);
} /* end of StructAddRowPartFdd */
 


/******************************************************************************/
/* The StructPutFieldFDD routine                                               */
/******************************************************************************/
static int StructPutFieldFDD(long *plpRowNumFDD)
{
  int ilRC = RC_SUCCESS;
  char pclRowBuffer[4096];

  sprintf(pclRowBuffer,
      "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,"
      "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
      rgFddFlds.pclFddADID,
      rgFddFlds.pclFddAIRB,
      rgFddFlds.pclFddAPC3,
      rgFddFlds.pclFddAURN, 
      rgFddFlds.pclFddBLT1,
      rgFddFlds.pclFddBLT2,
      rgFddFlds.pclFddCDAT,
      rgFddFlds.pclFddCKIF,
      rgFddFlds.pclFddCKIT,
      rgFddFlds.pclFddDACO,
      rgFddFlds.pclFddDNAT,                                                                          
      rgFddFlds.pclFddETOF,
      rgFddFlds.pclFddFLNO,
      rgFddFlds.pclFddFTYP,
      rgFddFlds.pclFddGTD1,
      rgFddFlds.pclFddGTD2,
      rgFddFlds.pclFddHOPO,
      rgFddFlds.pclFddINDO,
      rgFddFlds.pclFddLAND,
      rgFddFlds.pclFddLSTU,
      rgFddFlds.pclFddOFBL,
      rgFddFlds.pclFddONBL,
      rgFddFlds.pclFddREMP,
      rgFddFlds.pclFddSTOF,
      rgFddFlds.pclFddTIFF,
      rgFddFlds.pclFddURNO,
      rgFddFlds.pclFddUSEC,
      rgFddFlds.pclFddUSEU,
      rgFddFlds.pclFddVIA3,
      rgFddFlds.pclFddWRO1, 
      rgFddFlds.pclFddDSEQ, 
      rgFddFlds.pclFddRMTI, 
      rgFddFlds.pclFddGD1B,
      rgFddFlds.pclFddGD2B,
      rgFddFlds.pclFddNODE,
      rgFddFlds.pclFddOBL1,
      rgFddFlds.pclFddOBL2,
      rgFddFlds.pclFddOGT1,
      rgFddFlds.pclFddOGT2);

 
  ilRC = CEDAArrayPutFields(&rgFddArray.rrArrayHandle, rgFddArray.crArrayName,NULL,plpRowNumFDD,
                FDD_FIELDS
                ,pclRowBuffer);
  if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%05d: StructPutFieldFDD: CEDAArrayPutField failed !! RowNum: %d",__LINE__, plpRowNumFDD);
    }



  return (ilRC);
} /* end of StructPutFieldFDD */





/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData() 
{
  int    ilRC = RC_SUCCESS;            /* Return code */
  BC_HEAD *prlBchd;
  CMDBLK  *prlCmdblk;
  char    *pclSelect;
  char    *pclFields;
  char    *pclData;
  char    *pclPtr;  
  int     ilConfCount;
  int     ilRc = RC_SUCCESS;
  char pclCmdStart[16] = "\0";
  char pclCmdStop[16] = "\0";
  int ilFieldNum = 0;
  int ilCol = 0;
  int ilPos = 0;
  long llRowNumCCA = ARR_FIRST;
  char *pclRowCCA = NULL;
  char pclCcaURNO[128] = "\0";
  char pclURNOBuffer[128] = "\0";
  int ilFound = 0;
                                          
  prlBchd   = (BC_HEAD *)((char *)prgEvent + sizeof(EVENT));
  prlCmdblk = (CMDBLK *)((char *)prlBchd->data);
  pclSelect = (char *)prlCmdblk->data;
  pclFields = (char *)pclSelect + strlen(pclSelect) + 1;
  pclData   = (char *)pclFields + strlen(pclFields) + 1;                                                                      
  dbg(TRACE,"\n");
  dbg(TRACE,"====================== START ==========================");
  dbg(TRACE,"Command received:\n Command: <%s>\n Select:  <%s>\n Fields:  <%s>\n Data:    <%s>\n"
                                                     ,prlCmdblk,pclSelect,pclFields,pclData);
  dbg(DEBUG,"%05d: prlCmdblk->obj_name: <%s>",__LINE__,prlCmdblk->obj_name);



  if ((strcmp(prlCmdblk->command,"RUN") == 0) || (strcmp(prlCmdblk->command,"111") == 0))
    {
   
      ilRC = RUNCmd();
    }

  
  if ((strcmp(prlCmdblk->command,"UPD") == 0) || (strcmp(prlCmdblk->command,"112") == 0))
    {
      ilRC = UPDCmd();
    }


  if (((strcmp(prlCmdblk->command,"UFR") == 0) && (strcmp(prlCmdblk->obj_name,"AFTTAB") == 0)) ||
      ((strcmp(prlCmdblk->command,"URT") == 0) && (strcmp(prlCmdblk->obj_name,"AFTTAB") == 0)))
  {
     dbg(DEBUG,"================================================");
     dbg(DEBUG,"======= Handle Update START UFR_AFTTAB =========");
     dbg(DEBUG,"================================================");
     if (strlen(pclSelect) == 0)
     {
        dbg(TRACE,"Selection is empty ==> Don't process Command");
        return RC_SUCCESS;
     }
     strcpy(prlCmdblk->command,"URT");
     ilRC = CEDAArrayEventUpdate(prgEvent);
     if (ilRC != RC_SUCCESS)
     {
        dbg(DEBUG,"%05d: CEDAArrayEventUpdate UFR_AFT failed",__LINE__);
     }
     ilRC = UFR_AFT_Cmd(pclFields,pclData);
     dbg(DEBUG,"================================================");
     dbg(DEBUG,"======= Handle Update END UFR_AFTTAB =========");
     dbg(DEBUG,"================================================");
  }

 if ((strcmp(prlCmdblk->command,"IFR") == 0) && (strcmp(prlCmdblk->obj_name,"AFTTAB") == 0))
    {
      dbg(DEBUG,"================================================");
      dbg(DEBUG,"======= Handle Update START IFR_AFTTAB =========");
      dbg(DEBUG,"================================================");
      strcpy(prlCmdblk->command,"IRT");
      ilRC = CEDAArrayEventUpdate(prgEvent);
      if (ilRC == RC_SUCCESS)
    {
      ilRC = UFR_AFT_Cmd(pclFields,pclData);
    }
      else
    {
       dbg(DEBUG,"%05d: CEDAArrayEventUpdate IFR_AFTTAB failed",__LINE__);
    }
     
      ilRC = UFR_AFT_Cmd(pclFields,pclData);
      dbg(DEBUG,"================================================");
      dbg(DEBUG,"======= Handle Update END IFR_AFTTAB ===========");
      dbg(DEBUG,"================================================");
    }

 if ((strcmp(prlCmdblk->command,"DFR") == 0) && (strcmp(prlCmdblk->obj_name,"AFTTAB") == 0))
    {
      dbg(DEBUG,"================================================");
      dbg(DEBUG,"======= Handle Update START DFR_AFTTAB =========");
      dbg(DEBUG,"================================================");
      strcpy(prlCmdblk->command,"DFR");
      ilRC = CEDAArrayEventUpdate(prgEvent);
      if (ilRC == RC_SUCCESS)
    {
      ilRC = UFR_AFT_Cmd(pclFields,pclData);
    }
      else
    {
      dbg(DEBUG,"%05d: CEDAArrayEventUpdate DFR_AFT failed",__LINE__);
    }
   
         dbg(DEBUG,"%05d: --------------------",__LINE__);
      dbg(DEBUG,"================================================");
      dbg(DEBUG,"======= Handle Update END DFT_AFTTAB ===========");
      dbg(DEBUG,"================================================");
    }



 if (((strcmp(prlCmdblk->command,"URT") == 0) && (strcmp(prlCmdblk->obj_name,"CCATAB") == 0)) ||
     ((strcmp(prlCmdblk->command,"UFR") == 0) && (strcmp(prlCmdblk->obj_name,"CCATAB") == 0)))
    {
      if(strcmp(prlCmdblk->command,"UFR") == 0)
    {
      strcpy(prlCmdblk->command,"URT");
    }
      dbg(DEBUG,"================================================");
      dbg(DEBUG,"===== Handle Update START URT CCA_TAB=======");
      dbg(DEBUG,"================================================");
      
#if 0
      ilRC = CEDAArrayEventUpdate(prgEvent);
      if (ilRC == RC_SUCCESS)
      {
         llRowNumCCA = ARR_FIRST;
         FindItemInList(pclFields,"URNO",',',&ilFieldNum,&ilCol,&ilPos);
         get_real_item(pclCcaURNO,pclData,ilFieldNum);  
         TrimRight(pclCcaURNO);
	 ilFound = 0;
         while(ilFound == 0 && CEDAArrayGetRowPointer(&rgCcaArray.rrArrayHandle,
                                                      rgCcaArray.crArrayName,
                                                      llRowNumCCA,(void *)&pclRowCCA) == RC_SUCCESS)
         {
            llRowNumCCA = ARR_CURRENT;
            strcpy(pclURNOBuffer,FIELDVAL(rgCcaArray,pclRowCCA,igCcaURNO));
            TrimRight(pclURNOBuffer);
            if (strcmp(pclCcaURNO,pclURNOBuffer) == 0)
            {
	       ilFound = 1;
	    }
	    llRowNumCCA = ARR_NEXT;
	 }
	 if (ilFound == 0)
	 {
            strcpy(prlCmdblk->command,"IRT");
            ilRC = CEDAArrayEventUpdate(prgEvent);
            strcpy(prlCmdblk->command,"URT");
	 }
         ilRC = CCA_Update(pclFields,pclData);
      }
      else
      {
         dbg(TRACE,"Update of CCA Array failed");
      }
#endif
   
 

      dbg(DEBUG,"================================================");
      dbg(DEBUG,"====== Handle Update END  URT CCA_TAB=======");
      dbg(DEBUG,"================================================");
    }


 if (((strcmp(prlCmdblk->command,"IRT") == 0) && (strcmp(prlCmdblk->obj_name,"CCATAB") == 0)) ||
     ((strcmp(prlCmdblk->command,"IFR") == 0) && (strcmp(prlCmdblk->obj_name,"CCATAB") == 0)))
    {
      if(strcmp(prlCmdblk->command,"IFR") == 0)
    {
      strcpy(prlCmdblk->command,"IRT");
    }
      dbg(DEBUG,"================================================");
      dbg(DEBUG,"===== Handle Update START IRT CCA_TAB=======");
      dbg(DEBUG,"================================================");
      ilRC = CEDAArrayEventUpdate(prgEvent);
      if (ilRC == RC_SUCCESS)
    {

    }
      else
    {
      dbg(DEBUG,"%05d: CEDAArrayEventUpdate <%s>-CCA failed",__LINE__,prlCmdblk->command);
    }
   
 

      dbg(DEBUG,"================================================");
      dbg(DEBUG,"====== Handle Update END  IRT CCA_TAB=======");
      dbg(DEBUG,"================================================");
    }

  dbg(TRACE,"====================== END ============================");

  return ilRC;
    
} /* end of HandleData */







/******************************************************************************/
/* The RUNCmd routine                                                    */
/******************************************************************************/
static int RUNCmd()
{
  int ilRC = RC_SUCCESS;
  char pclBuffer[128] = "\0";
  char pclBufADID[128] = "\0";
  long llRowNumFDD = ARR_FIRST;
  long llRowNumAFT = ARR_FIRST;
  char *pclRowFDD = NULL;
  char *pclRowAFT = NULL;
  char pclSqlBuf[1024] = "\0";
  char pclCfgBuffer[10] = "\0";
  int ilOrigPriority = 0;
  int ilPriority = 0;
  short slSqlFunc = 0;
  short slCursor = 0;
  long llRowNumCCA = ARR_FIRST;
  char *pclRowCCA = NULL;
  char pclCcaCKIC[128] = "\0";
  char pclCcaCKBS[128] = "\0";
  char pclCcaCKES[128] = "\0";
  char pclCcaCKBA[128] = "\0";
  char pclCcaCKEA[128] = "\0";
  char pclCcaURNO[128] = "\0";
  char pclFddNODE[128] = "\0";
  char  cgArrStart[16] = "\0";                /* Timeframe Arrival Start*/
  char  cgArrEnd[16] = "\0";                  /* Timeframe Arrival End  */
  char  cgDepStart[16] = "\0";                /* Timeframe Departure Start */
  char  cgDepEnd[16] = "\0";                  /* Timeframe Departure End   */  
  char  cgArrStartPrev[16] = "\0";
  char  cgDepStartPrev[16] = "\0";
  char pclNowTime[16] = "\0";
  char pclNowTimeExt[16] = "\0";
  char pclCmdStart[16] = "\0";
  char pclCmdStop[16] = "\0";
  int ilDelCount;
  int ilMarkCount;
  char pclSelectBuf1[512];
  char pclSelectBuf2[512];
  int ilI;
  char *pclRowFLD = NULL;
  long llRowNumFLD = ARR_FIRST;
  char pclFldURNO[16];
  char pclFldRNAM[16];
  char pclFldRTYP[16];

  igInsertIntoFDD = TRUE;
  dbg(DEBUG,"==========================================");
  dbg(TRACE,"=========   Start WORK      ==============");
  dbg(DEBUG,"==========================================");

  TimeToStr(pclCmdStart,time(NULL));
  dbg(TRACE,"========= <%s>  ==============",pclCmdStart);

  ilOrigPriority = getpriority(PRIO_PROCESS,0);
  dbg(DEBUG,"********************** Init_fldhdl *******************");
  dbg(DEBUG,"Prozess Priority: <%d>",ilOrigPriority);
  dbg(DEBUG,"*****************************************************");
  ilRC = ReadConfigEntry("MAIN","priority",pclCfgBuffer);
  ilPriority = atoi(pclCfgBuffer);
  setpriority(PRIO_PROCESS,0,ilPriority);

  OffsettimeFromNow(&timeoffsets.lgTipc01, cgArrStart);
  OffsettimeFromNow(&timeoffsets.lgTipc02, cgDepStart);
  OffsettimeFromNow(&timeoffsets.lgTipc03, cgArrEnd);
  OffsettimeFromNow(&timeoffsets.lgTipc04, cgDepEnd);
  strcpy(cgArrStartPrev,cgArrStart);
  AddSecondsToCEDATime12(cgArrStartPrev,-86400,1);
  strcpy(cgDepStartPrev,cgDepStart);
  AddSecondsToCEDATime12(cgDepStartPrev,-86400,1);

  sprintf(pclSelectBuf1,"(TIFA BETWEEN '%s' AND '%s' AND ADID = 'A' AND FTYP IN (%s)) OR (TIFA BETWEEN '%s' AND '%s' AND ADID = 'A' AND FTYP IN (%s) AND REMP IN (%s))",cgArrStart,cgArrEnd,pcgFTYP,cgArrStartPrev,cgArrStart,pcgFTYP,pcgUnknownDelays);
  sprintf(pclSelectBuf2,"(TIFD BETWEEN '%s' AND '%s' AND ADID = 'D' AND FTYP IN (%s)) OR (TIFD BETWEEN '%s' AND '%s' AND ADID = 'D' AND FTYP IN (%s) AND REMP IN (%s))",cgDepStart,cgDepEnd,pcgFTYP,cgDepStartPrev,cgDepStart,pcgFTYP,pcgUnknownDelays);
  sprintf(pclSqlBuf,"WHERE (%s) OR (%s)",pclSelectBuf1,pclSelectBuf2);
  ilRC = CEDAArrayRefill(&rgAftArray.rrArrayHandle,rgAftArray.crArrayName,pclSqlBuf,AFT_FIELDS,ARR_FIRST);
  if (ilRC != RC_SUCCESS)
  {
     dbg(TRACE,"%05d: Refill CEDAArray failed",__LINE__);
  }
  else
  {
     dbg(TRACE,"Refill AFT CEDAArray successful");
  }
 
  TimeToStr(pclNowTime,time(NULL));
  strcpy(pclNowTimeExt,pclNowTime);
  AddSecondsToCEDATime12(pclNowTimeExt,igRefreshTime,1);

  while(CEDAArrayGetRowPointer(&rgAftArray.rrArrayHandle,
                   rgAftArray.crArrayName,
                   llRowNumAFT,(void *)&pclRowAFT) == RC_SUCCESS)
  {
     llRowNumAFT = ARR_CURRENT;
     ilRC = UpdateFLDTAB(pclRowAFT);
     llRowNumAFT = ARR_NEXT;
  }
  dbg(TRACE,"FLDTAB done");
  ilRC = RebuildCounters();
  dbg(TRACE,"RebuildCounters done");
  ilRC = RebuildChutes();
  dbg(TRACE,"RebuildChutes done");

  ilMarkCount = 0;
  llRowNumFDD = ARR_FIRST;
  while(CEDAArrayGetRowPointer(&rgFddArray.rrArrayHandle,
                               rgFddArray.crArrayName,
                               llRowNumFDD,(void *)&pclRowFDD) == RC_SUCCESS)
  {
     llRowNumFDD = ARR_CURRENT;
     ilRC = CEDAArrayPutField(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,
                                 "NODE",llRowNumFDD,"x");
     ilMarkCount++;
     llRowNumFDD = ARR_NEXT;
  }
  dbg(TRACE,"%d records of FDD marked",ilMarkCount);

/*AKLTEST*/
/*
for (ilI=0; ilI<igCurNoDediCnt; ilI++)
{
   dbg(TRACE,"<%s><%s><%s><%s><%s><%s><%s><%s><%d>",&(rgDedicatedCounters[ilI].pclDediFLNO[0]),
                                                    &(rgDedicatedCounters[ilI].pclDediFLNU[0]),
                                                    &(rgDedicatedCounters[ilI].pclCntInt[0][0]),
                                                    &(rgDedicatedCounters[ilI].pclCntInt[1][0]),
                                                    &(rgDedicatedCounters[ilI].pclCntInt[2][0]),
                                                    &(rgDedicatedCounters[ilI].pclCntInt[3][0]),
                                                    &(rgDedicatedCounters[ilI].pclCntInt[4][0]),
                                                    &(rgDedicatedCounters[ilI].pclCntInt[5][0]),
                                                    rgDedicatedCounters[ilI].ilCurNoIntNam);
}
*/
/*AKLTEST*/

  llRowNumFDD = ARR_FIRST;
  llRowNumAFT = ARR_FIRST;

  while (CEDAArrayGetRowPointer(&rgAftArray.rrArrayHandle,
                                rgAftArray.crArrayName,
                                llRowNumAFT,(void *)&pclRowAFT) == RC_SUCCESS)
  {
     /*dbg(TRACE,"%05d: HandleData: This is the Beginning of while (GetRowPointer) !!",__LINE__);*/
     strcpy(pclBufADID,FIELDVAL(rgAftArray,pclRowAFT,igAftADID));
     TrimRight(pclBufADID);
     /*dbg(TRACE,"%05d: in While: pclBufADID = <%s>, igAftADID: %d",__LINE__, pclBufADID,igAftADID);*/
     if ((strcmp(pclBufADID,"A") == 0) || (strcmp(pclBufADID,"B") == 0))
     { 
        /************** now Handle_Arrival - Flights  *********************/
        /*dbg(TRACE,"%05d: Call Handle_Arrival !!! llRowNumFDD: <%d>",__LINE__,llRowNumFDD);*/
        ilRC = Handle_Arrival(pclRowAFT,llRowNumAFT,&llRowNumFDD);
     }
     if ((strcmp(pclBufADID,"D") == 0) || (strcmp(pclBufADID,"B") == 0))
     {
        /************** now Handle_Departure - Flights  *********************/
        /* dbg(TRACE,"%05d: Call Handle_Departure !!! llRowNumFDD: <%d>",__LINE__,llRowNumFDD);*/
        ilRC = Handle_Departure(pclRowAFT,llRowNumAFT,&llRowNumFDD);
     }
     llRowNumFDD = ARR_NEXT;
     llRowNumAFT = ARR_NEXT;
  }
  dbg(TRACE,"Handle_Arrival and Handle_Departure done");
 
  ilDelCount = 0;
  llRowNumFDD = ARR_FIRST;
  while(CEDAArrayGetRowPointer(&rgFddArray.rrArrayHandle,
                               rgFddArray.crArrayName,
                               llRowNumFDD,(void *)&pclRowFDD) == RC_SUCCESS)
  {
     llRowNumFDD = ARR_CURRENT;
     strcpy(pclFddNODE,FIELDVAL(rgFddArray,pclRowFDD,igFddNODE));
     TrimRight(pclFddNODE);
     if (strcmp(pclFddNODE,"x") == 0)
     {
        ilRC = CEDAArrayDeleteRow(&rgFddArray.rrArrayHandle, rgFddArray.crArrayName, llRowNumFDD);
        ilDelCount++;
        llRowNumFDD = ARR_CURRENT;
     }
     else
     {
        llRowNumFDD = ARR_NEXT;
     }
  }
  dbg(TRACE,"%d remaining records from FDD deleted",ilDelCount);

  /* ilRC = CheckForUnknownDelays(); */

  dbg(TRACE,"RUNCmd: CEDAArrayWriteDB FDDTAB");
  ilRC = CEDAArrayWriteDB(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,NULL,NULL,NULL,
                          ARR_COMMIT_TILL_ERR);

  dbg(TRACE,"RUNCmd: CEDAArrayWriteDB CCATAB");
  ilRC = CEDAArrayWriteDB(&rgCcaArray.rrArrayHandle,rgCcaArray.crArrayName,NULL,NULL,NULL,NULL,
                          ARR_COMMIT_TILL_ERR);

  if (igGenerateFLD == 1)
  {
     dbg(TRACE,"RUNCmd: CEDAArrayWriteDB FLDTAB");
     ilRC = CEDAArrayWriteDB(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,NULL,NULL,NULL,NULL,
                             ARR_COMMIT_TILL_ERR);
  }
  else
  {
     rollback();
  }

  dbg(DEBUG,"==========================================");
  dbg(TRACE,"=========   END  WORK      ===============");
  dbg(DEBUG,"==========================================");

  TimeToStr(pclCmdStop,time(NULL));
  dbg(TRACE,"========= Start <%s>  ==============",pclCmdStart);
  dbg(TRACE,"========= Stop <%s>  ==============",pclCmdStop);

  igInsertIntoFDD = FALSE;
  setpriority(PRIO_PROCESS,0,ilOrigPriority);
  return (ilRC);
}


/********************************************************************************/
/* The GetDedicatedCounter routine                                              */
/*                                                                              */
/* This function gets the dedicated counters for a flight with the given        */
/* URNO of the AFTTAB                                                           */
/* The Parameters are the flights URNO and 2 char-buffers for retrieving        */
/* the Range of counters to be displayed                                        */
/* for example CKIF = 20, CHIT = 30 the counters 20-30 will be displayed        */
/* this block is build by searching the possible counters for the biggest block */
/* which doesn't include a hole of more than 5 counters which do not belong to  */
/* the given flight.                                                            */
/* biggest means the block with the most counters, not the widest range         */
/* if there are 2 or more blocks with the same number of counters then the first*/
/* block (the one with the lowest counter-numbers) will be displayed            */
/********************************************************************************/
static int GetDedicatedCounter(char *pcpType, char *pcpFlno, char *pcpCKIF, char *pcpCKIT)
{
  int ilRC = RC_SUCCESS;
  int ilI;
  int ilFound;
  int ilLastCounter;
  int ilCurCnt;
  int ilNextCnt;
  char pclCnt[16] = "\0";
  struct _CounterBlocks
  {
    int ilNo;
    int ilIndFirst;
    int ilIndLast;
  };
  typedef struct _CounterBlocks COUNTERBLOCKS;
  COUNTERBLOCKS rlCounterBlocks[50];
  int ilNoCounterBlocks;
  int ilMax;
  int ilMaxInd;
  int ilJ;

  dbg(DEBUG,"GetDedicatedCounter: Type: <%s> Flno: <%s>",pcpType,pcpFlno);

  strcpy(pcpCKIF," ");
  strcpy(pcpCKIT," ");
  strcpy(pcgCKIF," ");
  strcpy(pcgCKIT," ");
  strcpy(pcgCKIF_C," ");
  strcpy(pcgCKIT_C," ");
  strcpy(pcgCKIF_D," ");
  strcpy(pcgCKIT_D," ");

  ilFound = -1;
  for (ilI=0; ilI<igCurNoDediCnt && ilFound<0; ilI++)
  {
     dbg(DEBUG,"Flno: <%s> FLNO: <%s>",pcpFlno,&(rgDedicatedCounters[ilI].pclDediFLNO[0]));
     if (strcmp(pcpFlno,&(rgDedicatedCounters[ilI].pclDediFLNO[0])) == 0)
     {
        ilFound = ilI;
     }
  }

  dbg(DEBUG,"GetDedicatedCounter: Found = %d",ilFound);

  if (ilFound < 0)
  {
     return(ilRC);
  }

  if (pcpType[0] == 'I' || igIntAndDomCounters == 0)
  {
     ilLastCounter = rgDedicatedCounters[ilFound].ilCurNoIntNam-1;
     if (ilLastCounter >= 0)
     {
        strcpy(pcpCKIF,&(rgDedicatedCounters[ilFound].pclCntInt[0][0]));
        if (igGapCount == 99999 || ilLastCounter == 0)
        {
           strcpy(pcpCKIT,&(rgDedicatedCounters[ilFound].pclCntInt[ilLastCounter][0]));
        }
        else
        {
              strcpy(pcpCKIT,pcpCKIF);
              ilNoCounterBlocks = 0;
              ilI = 0;
              rlCounterBlocks[ilNoCounterBlocks].ilNo = 1;
              rlCounterBlocks[ilNoCounterBlocks].ilIndFirst = ilI;
              rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI;
              while (ilI < ilLastCounter)
              {
                 strcpy(pclCnt,&(rgDedicatedCounters[ilFound].pclCntInt[ilI][0]));
                 ilCurCnt = atoi(pclCnt);
                 strcpy(pclCnt,&(rgDedicatedCounters[ilFound].pclCntInt[ilI+1][0]));
                 ilNextCnt = atoi(pclCnt);
                 if (ilNextCnt-ilCurCnt-1 <= igGapCount)
                 {
                    rlCounterBlocks[ilNoCounterBlocks].ilNo = rlCounterBlocks[ilNoCounterBlocks].ilNo + 1;
                    rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI + 1;
                 }
                 else
                 {
                    ilNoCounterBlocks++;
                    rlCounterBlocks[ilNoCounterBlocks].ilNo = 1;
                    rlCounterBlocks[ilNoCounterBlocks].ilIndFirst = ilI + 1;
                    rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI + 1;
                 }
                 ilI++;
              }
              ilNoCounterBlocks++;
              ilMax = -1;
              ilMaxInd = 0;
              for (ilI = 0; ilI < ilNoCounterBlocks; ilI++)
              {
                 if (rlCounterBlocks[ilI].ilNo > ilMax)
                 {
                    ilMax = rlCounterBlocks[ilI].ilNo;
                    ilMaxInd = ilI;
                 }
              }
              ilI = rlCounterBlocks[ilMaxInd].ilIndFirst;
              ilJ = rlCounterBlocks[ilMaxInd].ilIndLast;
              strcpy(pcpCKIF,&(rgDedicatedCounters[ilFound].pclCntInt[ilI][0]));
              strcpy(pcpCKIT,&(rgDedicatedCounters[ilFound].pclCntInt[ilJ][0]));
        }
     }
  }
  else
  {
     ilLastCounter = rgDedicatedCounters[ilFound].ilCurNoDomNam-1;
     if (ilLastCounter >= 0)
     {
        strcpy(pcpCKIF,&(rgDedicatedCounters[ilFound].pclCntDom[0][0]));
        if (igGapCount == 99999 || ilLastCounter == 0)
        {
           strcpy(pcpCKIT,&(rgDedicatedCounters[ilFound].pclCntDom[ilLastCounter][0]));
        }
        else
        {
              strcpy(pcpCKIT,pcpCKIF);
              ilNoCounterBlocks = 0;
              ilI = 0;
              rlCounterBlocks[ilNoCounterBlocks].ilNo = 1;
              rlCounterBlocks[ilNoCounterBlocks].ilIndFirst = ilI;
              rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI;
              while (ilI < ilLastCounter)
              {
                 strcpy(pclCnt,&(rgDedicatedCounters[ilFound].pclCntDom[ilI][0]));
                 ilCurCnt = atoi(pclCnt);
                 strcpy(pclCnt,&(rgDedicatedCounters[ilFound].pclCntDom[ilI+1][0]));
                 ilNextCnt = atoi(pclCnt);
                 if (ilNextCnt-ilCurCnt-1 <= igGapCount)
                 {
                    rlCounterBlocks[ilNoCounterBlocks].ilNo = rlCounterBlocks[ilNoCounterBlocks].ilNo + 1;
                    rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI + 1;
                 }
                 else
                 {
                    ilNoCounterBlocks++;
                    rlCounterBlocks[ilNoCounterBlocks].ilNo = 1;
                    rlCounterBlocks[ilNoCounterBlocks].ilIndFirst = ilI + 1;
                    rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI + 1;
                 }
                 ilI++;
              }
              ilNoCounterBlocks++;
              ilMax = -1;
              ilMaxInd = 0;
              for (ilI = 0; ilI < ilNoCounterBlocks; ilI++)
              {
                 if (rlCounterBlocks[ilI].ilNo > ilMax)
                 {
                    ilMax = rlCounterBlocks[ilI].ilNo;
                    ilMaxInd = ilI;
                 }
              }
              ilI = rlCounterBlocks[ilMaxInd].ilIndFirst;
              ilJ = rlCounterBlocks[ilMaxInd].ilIndLast;
              strcpy(pcpCKIF,&(rgDedicatedCounters[ilFound].pclCntDom[ilI][0]));
              strcpy(pcpCKIT,&(rgDedicatedCounters[ilFound].pclCntDom[ilJ][0]));
        }
     }
  }

  strcpy(pcgCKIF,pcpCKIF);
  strcpy(pcgCKIT,pcpCKIT);
  strcpy(pcgCKIF_D,pcpCKIF);
  strcpy(pcgCKIT_D,pcpCKIT);

  return (ilRC);

}/* end of GetDedicatedCounter */


/********************************************************************************/
/* The GetCommonCounter routine                                                 */
/*                                                                              */
/* This function gets the common counters for a flight with the given           */
/* URNO of the AFTTAB                                                           */
/* The Parameters are the Airlines URNO and 2 char-buffers for retrieving        */
/* the Range of counters to be displayed                                        */
/* for example CKIF = 20, CHIT = 30 the counters 20-30 will be displayed        */
/* this block is build by searching the possible counters for the biggest block */
/* which doesn't include a hole of more than 5 counters which do not belong to  */
/* the given flight.                                                            */
/* biggest means the block with the most counters, not the widest range         */
/* if there are 2 or more blocks with the same number of counters then the first*/
/* block (the one with the lowest counter-numbers) will be displayed            */
/********************************************************************************/
static int GetCommonCounter(char *pcpType, char *pcpAirl, char *pcpCKIF, char *pcpCKIT, char *pcpDest)
{
  int ilRC = RC_SUCCESS;
  int ilI;
  int ilFound;
  int ilLastCounter;
  int ilCurCnt;
  int ilNextCnt;
  char pclCnt[16] = "\0";
  struct _CounterBlocks
  {
    int ilNo;
    int ilIndFirst;
    int ilIndLast;
  };
  typedef struct _CounterBlocks COUNTERBLOCKS;
  COUNTERBLOCKS rlCounterBlocks[50];
  int ilNoCounterBlocks;
  int ilMax;
  int ilMaxInd;
  int ilJ;
  char pclResult[128];
  char *pclTmpPtr1;
  char *pclTmpPtr2;

  dbg(DEBUG,"GetCommonCounter: Type: <%s> Airl: <%s>",pcpType,pcpAirl);

  strcpy(pcpCKIF," ");
  strcpy(pcpCKIT," ");
  strcpy(pcgCKIF," ");
  strcpy(pcgCKIT," ");
  strcpy(pcgCKIF_C," ");
  strcpy(pcgCKIT_C," ");
  strcpy(pcgCKIF_D," ");
  strcpy(pcgCKIT_D," ");

  ilFound = -1;
  if (pcpAirl[2] == cBLANK)
  {
     pcpAirl[2] = '\0';
     for (ilI=0; ilI<igCurNoComCnt && ilFound<0; ilI++)
     {
        dbg(DEBUG,"Airl: <%s> ALC2: <%s>",pcpAirl,&(rgCommonCounters[ilI].pclComALC2[0]));
        if (strcmp(pcpAirl,&(rgCommonCounters[ilI].pclComALC2[0])) == 0)
        {
           ilFound = ilI;
        }
     }
  }
  else
  {
     for (ilI=0; ilI<igCurNoComCnt && ilFound<0; ilI++)
     {
        dbg(DEBUG,"Airl: <%s> ALC3: <%s>",pcpAirl,&(rgCommonCounters[ilI].pclComALC3[0]));
        if (strcmp(pcpAirl,&(rgCommonCounters[ilI].pclComALC3[0])) == 0)
        {
           ilFound = ilI;
        }
     }
  }

  dbg(DEBUG,"GetCommonCounter: Found = %d",ilFound);

  if (ilFound < 0)
  {
     return(ilRC);
  }

  if (pcpType[0] == 'I' || igIntAndDomCounters == 0)
  {
     ilLastCounter = rgCommonCounters[ilFound].ilCurNoIntNam-1;
     if (ilLastCounter >= 0)
     {
        strcpy(pcpCKIF,&(rgCommonCounters[ilFound].pclCntInt[0][0]));
        if (igGapCount == 99999 || ilLastCounter == 0)
        {
/*
           if (strcmp(pcgSpecialCommonCountersForLis,"YES") == 0 && strcmp(pcpAirl,"TP") == 0)
           {
              dbg(TRACE,"AKL <%s><%s><%s><%s>",&(rgCommonCounters[ilFound].pclCntInt[ilLastCounter][0]),
                  &(rgCommonCounters[ilFound].pclCntIntDisp[ilLastCounter][0]),
                  pcpDest,pcpAirl);
           }
*/
           strcpy(pcpCKIT,&(rgCommonCounters[ilFound].pclCntInt[ilLastCounter][0]));
        }
        else
        {
           strcpy(pcpCKIT,pcpCKIF);
           ilNoCounterBlocks = 0;
           ilI = 0;
           if (strcmp(pcgSpecialCommonCountersForLis,"YES") == 0 && strcmp(pcpAirl,"TP") == 0)
           {
              strcpy(pclResult,"NOT ACCEPT");
              while (ilI <= ilLastCounter && strcmp(pclResult,"NOT ACCEPT") == 0)
              {
                 if (strstr(pcgSpecialDests,pcpDest) != NULL)
                 {
                    if (strlen(&(rgCommonCounters[ilFound].pclCntIntDisp[ilI][0])) == 3 &&
                        strstr(pcgSpecialDests,&(rgCommonCounters[ilFound].pclCntIntDisp[ilI][0])) != NULL)
                    {
                       pclTmpPtr1 = strstr(pcgSpecialDests,&(rgCommonCounters[ilFound].pclCntIntDisp[ilI][0]));
                       pclTmpPtr2 = strstr(pclTmpPtr1,"\n");
                       strncpy(pclResult,pclTmpPtr1,pclTmpPtr2-pclTmpPtr1);
                       pclResult[pclTmpPtr2-pclTmpPtr1] = '\0';
                       if (strstr(pclResult,pcpDest) != NULL)
                          strcpy(pclResult,"ACCEPT");
                       else
                          strcpy(pclResult,"NOT ACCEPT");
                    }
                    else
                       strcpy(pclResult,"NOT ACCEPT");
                 }
                 else
                 {
                    if (strlen(&(rgCommonCounters[ilFound].pclCntIntDisp[ilI][0])) == 3 &&
                        strstr(pcgSpecialDests,&(rgCommonCounters[ilFound].pclCntIntDisp[ilI][0])) != NULL)
                       strcpy(pclResult,"NOT ACCEPT");
                    else
                       strcpy(pclResult,"ACCEPT");
                 }
                 if (strcmp(pclResult,"NOT ACCEPT") == 0)
                    ilI++;
              }
           }
           if (ilI < ilLastCounter)
           {
              rlCounterBlocks[ilNoCounterBlocks].ilNo = 1;
              rlCounterBlocks[ilNoCounterBlocks].ilIndFirst = ilI;
              rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI;
              while (ilI < ilLastCounter)
              {
                 if (strcmp(pcgSpecialCommonCountersForLis,"YES") == 0 && strcmp(pcpAirl,"TP") == 0)
                 {
                    if (strstr(pcgSpecialDests,pcpDest) != NULL)
                    {
                       if (strlen(&(rgCommonCounters[ilFound].pclCntIntDisp[ilI+1][0])) == 3 &&
                           strstr(pcgSpecialDests,&(rgCommonCounters[ilFound].pclCntIntDisp[ilI+1][0])) != NULL)
                       {
                          pclTmpPtr1 = strstr(pcgSpecialDests,&(rgCommonCounters[ilFound].pclCntIntDisp[ilI+1][0]));
                          pclTmpPtr2 = strstr(pclTmpPtr1,"\n");
                          strncpy(pclResult,pclTmpPtr1,pclTmpPtr2-pclTmpPtr1);
                          pclResult[pclTmpPtr2-pclTmpPtr1] = '\0';
                          if (strstr(pclResult,pcpDest) != NULL)
                             strcpy(pclResult,"ACCEPT");
                          else
                             strcpy(pclResult,"NOT ACCEPT");
                       }
                       else
                          strcpy(pclResult,"NOT ACCEPT");
                    }
                    else
                    {
                       if (strlen(&(rgCommonCounters[ilFound].pclCntIntDisp[ilI+1][0])) == 3 &&
                           strstr(pcgSpecialDests,&(rgCommonCounters[ilFound].pclCntIntDisp[ilI+1][0])) != NULL)
                          strcpy(pclResult,"NOT ACCEPT");
                       else
                          strcpy(pclResult,"ACCEPT");
                    }
                 }
                 else
                     strcpy(pclResult,"ACCEPT");
                 if (strcmp(pclResult,"ACCEPT") == 0)
                 {
                    strcpy(pclCnt,&(rgCommonCounters[ilFound].pclCntInt[ilI][0]));
                    ilCurCnt = atoi(pclCnt);
                    strcpy(pclCnt,&(rgCommonCounters[ilFound].pclCntInt[ilI+1][0]));
                    ilNextCnt = atoi(pclCnt);
                    if (ilNextCnt-ilCurCnt-1 <= igGapCount)
                    {
                       rlCounterBlocks[ilNoCounterBlocks].ilNo = rlCounterBlocks[ilNoCounterBlocks].ilNo + 1;
                       rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI + 1;
                    }
                    else
                    {
                       ilNoCounterBlocks++;
                       rlCounterBlocks[ilNoCounterBlocks].ilNo = 1;
                       rlCounterBlocks[ilNoCounterBlocks].ilIndFirst = ilI + 1;
                       rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI + 1;
                    }
                 }
                 ilI++;
              }
              ilNoCounterBlocks++;
              ilMax = -1;
              ilMaxInd = 0;
              for (ilI = 0; ilI < ilNoCounterBlocks; ilI++)
              {
                 if (rlCounterBlocks[ilI].ilNo > ilMax)
                 {
                    ilMax = rlCounterBlocks[ilI].ilNo;
                    ilMaxInd = ilI;
                 }
              }
              ilI = rlCounterBlocks[ilMaxInd].ilIndFirst;
              ilJ = rlCounterBlocks[ilMaxInd].ilIndLast;
              strcpy(pcpCKIF,&(rgCommonCounters[ilFound].pclCntInt[ilI][0]));
              strcpy(pcpCKIT,&(rgCommonCounters[ilFound].pclCntInt[ilJ][0]));
           }
           else
           {
              if (ilI == ilLastCounter)
              {
                 strcpy(pcpCKIF,&(rgCommonCounters[ilFound].pclCntInt[ilI][0]));
                 strcpy(pcpCKIT,&(rgCommonCounters[ilFound].pclCntInt[ilI][0]));
              }
              else
              {
                 strcpy(pcpCKIF," ");
                 strcpy(pcpCKIT," ");
              }
           }
        }
     }
  }
  else
  {
     ilLastCounter = rgCommonCounters[ilFound].ilCurNoDomNam-1;
     if (ilLastCounter >= 0)
     {
        strcpy(pcpCKIF,&(rgCommonCounters[ilFound].pclCntDom[0][0]));
        if (igGapCount == 99999 || ilLastCounter == 0)
        {
           strcpy(pcpCKIT,&(rgCommonCounters[ilFound].pclCntDom[ilLastCounter][0]));
        }
        else
        {
           strcpy(pcpCKIT,pcpCKIF);
           ilNoCounterBlocks = 0;
           ilI = 0;
           rlCounterBlocks[ilNoCounterBlocks].ilNo = 1;
           rlCounterBlocks[ilNoCounterBlocks].ilIndFirst = ilI;
           rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI;
           while (ilI < ilLastCounter)
           {
              strcpy(pclCnt,&(rgCommonCounters[ilFound].pclCntDom[ilI][0]));
              ilCurCnt = atoi(pclCnt);
              strcpy(pclCnt,&(rgCommonCounters[ilFound].pclCntDom[ilI+1][0]));
              ilNextCnt = atoi(pclCnt);
              if (ilNextCnt-ilCurCnt-1 <= igGapCount)
              {
                 rlCounterBlocks[ilNoCounterBlocks].ilNo = rlCounterBlocks[ilNoCounterBlocks].ilNo + 1;
                 rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI + 1;
              }
              else
              {
                 ilNoCounterBlocks++;
                 rlCounterBlocks[ilNoCounterBlocks].ilNo = 1;
                 rlCounterBlocks[ilNoCounterBlocks].ilIndFirst = ilI + 1;
                 rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI + 1;
              }
              ilI++;
           }
           ilNoCounterBlocks++;
           ilMax = -1;
           ilMaxInd = 0;
           for (ilI = 0; ilI < ilNoCounterBlocks; ilI++)
           {
              if (rlCounterBlocks[ilI].ilNo > ilMax)
              {
                 ilMax = rlCounterBlocks[ilI].ilNo;
                 ilMaxInd = ilI;
              }
           }
           ilI = rlCounterBlocks[ilMaxInd].ilIndFirst;
           ilJ = rlCounterBlocks[ilMaxInd].ilIndLast;
           strcpy(pcpCKIF,&(rgCommonCounters[ilFound].pclCntDom[ilI][0]));
           strcpy(pcpCKIT,&(rgCommonCounters[ilFound].pclCntDom[ilJ][0]));
        }
     }
  }

  strcpy(pcgCKIF,pcpCKIF);
  strcpy(pcgCKIT,pcpCKIT);
  strcpy(pcgCKIF_C,pcpCKIF);
  strcpy(pcgCKIT_C,pcpCKIT);

  return(ilRC);

}/* end of GetCommoncounters*/



/********************************************************************************/
/* The GetCommonAndDedicatedCounter routine                                                 */
/*                                                                              */
/* This function gets the common counters for a flight with the given           */
/* URNO of the AFTTAB                                                           */
/* The Parameters are the flights URNO and 2 char-buffers for retrieving        */
/* the Range of counters to be displayed                                        */
/* for example CKIF = 20, CHIT = 30 the counters 20-30 will be displayed        */
/* this block is build by searching the possible counters for the biggest block */
/* which doesn't include a hole of more than 5 counters which do not belong to  */
/* the given flight.                                                            */
/* biggest means the block with the most counters, not the widest range         */
/* if there are 2 or more blocks with the same number of counters then the first*/
/* block (the one with the lowest counter-numbers) will be displayed            */
/********************************************************************************/
static int GetCommonAndDedicatedCounter(char *pcpType, char *pcpAirl, char *pcpFlno, char *pcpCKIF, char *pcpCKIT ,
                                        char *pcpCDA, char *pcpMain, char *pcpDest)
{
  int ilRC = RC_SUCCESS;
  int ilI;
  int ilFoundC;
  int ilFoundD;
  int ilLastCounter;
  char pclCkifC[16] = "\0";
  char pclCkitC[16] = "\0";
  char pclCkifD[16] = "\0";
  char pclCkitD[16] = "\0";
  int ilCurCnt;
  int ilNextCnt;
  char pclCnt[16] = "\0";
  int ilCkifC;
  int ilCkitC;
  int ilCkifD;
  int ilCkitD;
  struct _CounterBlocks
  {
    int ilNo;
    int ilIndFirst;
    int ilIndLast;
  };
  typedef struct _CounterBlocks COUNTERBLOCKS;
  COUNTERBLOCKS rlCounterBlocks[50];
  int ilNoCounterBlocks;
  int ilMax;
  int ilMaxInd;
  int ilJ;
  char pclResult[128];
  char *pclTmpPtr1;
  char *pclTmpPtr2;

  dbg(DEBUG,"GetCommonAndDedicatedCounter: Type: <%s> Airl: <%s> Flno: <%s>",pcpType,pcpAirl,pcpFlno);

  strcpy(pcpCKIF," ");
  strcpy(pcpCKIT," ");
  strcpy(pclCkifC," ");
  strcpy(pclCkitC," ");
  strcpy(pclCkifD," ");
  strcpy(pclCkitD," ");
/*
  strcpy(pcgCKIF," ");
  strcpy(pcgCKIT," ");
  strcpy(pcgCKIF_C," ");
  strcpy(pcgCKIT_C," ");
  strcpy(pcgCKIF_D," ");
  strcpy(pcgCKIT_D," ");
*/

  ilFoundC = -1;
  TrimRight(pcpAirl);
  if (strlen(pcpAirl) == 2)
  {
     for (ilI=0; ilI<igCurNoComCnt && ilFoundC<0; ilI++)
     {
        dbg(DEBUG,"Airl: <%s> ALC2: <%s>",pcpAirl,&(rgCommonCounters[ilI].pclComALC2[0]));
        if (strcmp(pcpAirl,&(rgCommonCounters[ilI].pclComALC2[0])) == 0)
        {
           ilFoundC = ilI;
        }
     }
  }
  else
  {
     for (ilI=0; ilI<igCurNoComCnt && ilFoundC<0; ilI++)
     {
        dbg(DEBUG,"Airl: <%s> ALC3: <%s>",pcpAirl,&(rgCommonCounters[ilI].pclComALC3[0]));
        if (strcmp(pcpAirl,&(rgCommonCounters[ilI].pclComALC3[0])) == 0)
        {
           ilFoundC = ilI;
        }
     }
  }

  dbg(DEBUG,"GetCommonAndDedicatedCounter: FoundC = %d",ilFoundC);

  if (ilFoundC >= 0)
  {
     if (pcpType[0] == 'I' || igIntAndDomCounters == 0)
     {
        ilLastCounter = rgCommonCounters[ilFoundC].ilCurNoIntNam-1;
        if (ilLastCounter >= 0)
        {
           strcpy(pclCkifC,&(rgCommonCounters[ilFoundC].pclCntInt[0][0]));
           if (igGapCount == 99999 || ilLastCounter == 0)
           {
/*
              if (strcmp(pcgSpecialCommonCountersForLis,"YES") == 0 && strcmp(pcpAirl,"TP") == 0)
              {
                 dbg(TRACE,"AKL <%s><%s><%s><%s>",&(rgCommonCounters[ilFoundC].pclCntInt[ilLastCounter][0]),
                     &(rgCommonCounters[ilFoundC].pclCntIntDisp[ilLastCounter][0]),
                     pcpDest,pcpAirl);
              }
*/
              strcpy(pclCkitC,&(rgCommonCounters[ilFoundC].pclCntInt[ilLastCounter][0]));
           }
           else
           {
              strcpy(pclCkitC,pclCkifC);
              ilNoCounterBlocks = 0;
              ilI = 0;
              if (strcmp(pcgSpecialCommonCountersForLis,"YES") == 0 && strcmp(pcpAirl,"TP") == 0)
              {
                 strcpy(pclResult,"NOT ACCEPT");
                 while (ilI <= ilLastCounter && strcmp(pclResult,"NOT ACCEPT") == 0)
                 {
                    if (strstr(pcgSpecialDests,pcpDest) != NULL)
                    {
                       if (strlen(&(rgCommonCounters[ilFoundC].pclCntIntDisp[ilI][0])) == 3 &&
                           strstr(pcgSpecialDests,&(rgCommonCounters[ilFoundC].pclCntIntDisp[ilI][0])) != NULL)
                       {
                          pclTmpPtr1 = strstr(pcgSpecialDests,&(rgCommonCounters[ilFoundC].pclCntIntDisp[ilI][0]));
                          pclTmpPtr2 = strstr(pclTmpPtr1,"\n");
                          strncpy(pclResult,pclTmpPtr1,pclTmpPtr2-pclTmpPtr1);
                          pclResult[pclTmpPtr2-pclTmpPtr1] = '\0';
                          if (strstr(pclResult,pcpDest) != NULL)
                             strcpy(pclResult,"ACCEPT");
                          else
                             strcpy(pclResult,"NOT ACCEPT");
                       }
                       else
                          strcpy(pclResult,"NOT ACCEPT");
                    }
                    else
                    {
                       if (strlen(&(rgCommonCounters[ilFoundC].pclCntIntDisp[ilI][0])) == 3 &&
                           strstr(pcgSpecialDests,&(rgCommonCounters[ilFoundC].pclCntIntDisp[ilI][0])) != NULL)
                          strcpy(pclResult,"NOT ACCEPT");
                       else
                          strcpy(pclResult,"ACCEPT");
                    }
                    if (strcmp(pclResult,"NOT ACCEPT") == 0)
                       ilI++;
                 }
              }
              if (ilI < ilLastCounter)
              {
                 rlCounterBlocks[ilNoCounterBlocks].ilNo = 1;
                 rlCounterBlocks[ilNoCounterBlocks].ilIndFirst = ilI;
                 rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI;
                 while (ilI < ilLastCounter)
                 {
                    if (strcmp(pcgSpecialCommonCountersForLis,"YES") == 0 && strcmp(pcpAirl,"TP") == 0)
                    {
                       if (strstr(pcgSpecialDests,pcpDest) != NULL)
                       {
                          if (strlen(&(rgCommonCounters[ilFoundC].pclCntIntDisp[ilI+1][0])) == 3 &&
                              strstr(pcgSpecialDests,&(rgCommonCounters[ilFoundC].pclCntIntDisp[ilI+1][0])) != NULL)
                          {
                             pclTmpPtr1 = strstr(pcgSpecialDests,&(rgCommonCounters[ilFoundC].pclCntIntDisp[ilI+1][0]));
                             pclTmpPtr2 = strstr(pclTmpPtr1,"\n");
                             strncpy(pclResult,pclTmpPtr1,pclTmpPtr2-pclTmpPtr1);
                             pclResult[pclTmpPtr2-pclTmpPtr1] = '\0';
                             if (strstr(pclResult,pcpDest) != NULL)
                                strcpy(pclResult,"ACCEPT");
                             else
                                strcpy(pclResult,"NOT ACCEPT");
                          }
                          else
                             strcpy(pclResult,"NOT ACCEPT");
                       }
                       else
                       {
                          if (strlen(&(rgCommonCounters[ilFoundC].pclCntIntDisp[ilI+1][0])) == 3 &&
                              strstr(pcgSpecialDests,&(rgCommonCounters[ilFoundC].pclCntIntDisp[ilI+1][0])) != NULL)
                             strcpy(pclResult,"NOT ACCEPT");
                          else
                             strcpy(pclResult,"ACCEPT");
                       }
                    }
                    else
                        strcpy(pclResult,"ACCEPT");
                    if (strcmp(pclResult,"ACCEPT") == 0)
                    {
                       strcpy(pclCnt,&(rgCommonCounters[ilFoundC].pclCntInt[ilI][0]));
                       ilCurCnt = atoi(pclCnt);
                       strcpy(pclCnt,&(rgCommonCounters[ilFoundC].pclCntInt[ilI+1][0]));
                       ilNextCnt = atoi(pclCnt);
                       if (ilNextCnt-ilCurCnt-1 <= igGapCount)
                       {
                          rlCounterBlocks[ilNoCounterBlocks].ilNo = rlCounterBlocks[ilNoCounterBlocks].ilNo + 1;
                          rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI + 1;
                       }
                       else
                       {
                          ilNoCounterBlocks++;
                          rlCounterBlocks[ilNoCounterBlocks].ilNo = 1;
                          rlCounterBlocks[ilNoCounterBlocks].ilIndFirst = ilI + 1;
                          rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI + 1;
                       }
                    }
                    ilI++;
                 }
                 ilNoCounterBlocks++;
                 ilMax = -1;
                 ilMaxInd = 0;
                 for (ilI = 0; ilI < ilNoCounterBlocks; ilI++)
                 {
                    if (rlCounterBlocks[ilI].ilNo > ilMax)
                    {
                       ilMax = rlCounterBlocks[ilI].ilNo;
                       ilMaxInd = ilI;
                    }
                 }
                 ilI = rlCounterBlocks[ilMaxInd].ilIndFirst;
                 ilJ = rlCounterBlocks[ilMaxInd].ilIndLast;
                 strcpy(pclCkifC,&(rgCommonCounters[ilFoundC].pclCntInt[ilI][0]));
                 strcpy(pclCkitC,&(rgCommonCounters[ilFoundC].pclCntInt[ilJ][0]));
              }
              else
              {
                 if (ilI == ilLastCounter)
                 {
                    strcpy(pclCkifC,&(rgCommonCounters[ilFoundC].pclCntInt[ilI][0]));
                    strcpy(pclCkitC,&(rgCommonCounters[ilFoundC].pclCntInt[ilI][0]));
                 }
                 else
                 {
                    strcpy(pclCkifC," ");
                    strcpy(pclCkitC," ");
                 }
              }
           }
        }
     }
     else
     {
        ilLastCounter = rgCommonCounters[ilFoundC].ilCurNoDomNam-1;
        if (ilLastCounter >= 0)
        {
           strcpy(pclCkifC,&(rgCommonCounters[ilFoundC].pclCntDom[0][0]));
           if (igGapCount == 99999 || ilLastCounter == 0)
           {
              strcpy(pclCkitC,&(rgCommonCounters[ilFoundC].pclCntDom[ilLastCounter][0]));
           }
           else
           {
              strcpy(pclCkitC,pclCkifC);
              ilNoCounterBlocks = 0;
              ilI = 0;
              rlCounterBlocks[ilNoCounterBlocks].ilNo = 1;
              rlCounterBlocks[ilNoCounterBlocks].ilIndFirst = ilI;
              rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI;
              while (ilI < ilLastCounter)
              {
                 strcpy(pclCnt,&(rgCommonCounters[ilFoundC].pclCntDom[ilI][0]));
                 ilCurCnt = atoi(pclCnt);
                 strcpy(pclCnt,&(rgCommonCounters[ilFoundC].pclCntDom[ilI+1][0]));
                 ilNextCnt = atoi(pclCnt);
                 if (ilNextCnt-ilCurCnt-1 <= igGapCount)
                 {
                    rlCounterBlocks[ilNoCounterBlocks].ilNo = rlCounterBlocks[ilNoCounterBlocks].ilNo + 1;
                    rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI + 1;
                 }
                 else
                 {
                    ilNoCounterBlocks++;
                    rlCounterBlocks[ilNoCounterBlocks].ilNo = 1;
                    rlCounterBlocks[ilNoCounterBlocks].ilIndFirst = ilI + 1;
                    rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI + 1;
                 }
                 ilI++;
              }
              ilNoCounterBlocks++;
              ilMax = -1;
              ilMaxInd = 0;
              for (ilI = 0; ilI < ilNoCounterBlocks; ilI++)
              {
                 if (rlCounterBlocks[ilI].ilNo > ilMax)
                 {
                    ilMax = rlCounterBlocks[ilI].ilNo;
                    ilMaxInd = ilI;
                 }
              }
              ilI = rlCounterBlocks[ilMaxInd].ilIndFirst;
              ilJ = rlCounterBlocks[ilMaxInd].ilIndLast;
              strcpy(pclCkifC,&(rgCommonCounters[ilFoundC].pclCntDom[ilI][0]));
              strcpy(pclCkitC,&(rgCommonCounters[ilFoundC].pclCntDom[ilJ][0]));
           }
        }
     }
  }

  ilFoundD = -1;
  for (ilI=0; ilI<igCurNoDediCnt && ilFoundD<0; ilI++)
  {
     dbg(DEBUG,"Flno: <%s> FLNO: <%s>",pcpFlno,&(rgDedicatedCounters[ilI].pclDediFLNO[0]));
     if (strcmp(pcpFlno,&(rgDedicatedCounters[ilI].pclDediFLNO[0])) == 0)
     {
        ilFoundD = ilI;
     }
  }

  dbg(DEBUG,"GetCommonAndDedicatedCounter: FoundD = %d",ilFoundD);

  if (ilFoundD >= 0)
  {
     if (pcpType[0] == 'I' || igIntAndDomCounters == 0)
     {
        ilLastCounter = rgDedicatedCounters[ilFoundD].ilCurNoIntNam-1;
        if (ilLastCounter >= 0)
        {
           strcpy(pclCkifD,&(rgDedicatedCounters[ilFoundD].pclCntInt[0][0]));
           if (igGapCount == 99999 || ilLastCounter == 0)
           {
              strcpy(pclCkitD,&(rgDedicatedCounters[ilFoundD].pclCntInt[ilLastCounter][0]));
           }
           else
           {
              strcpy(pclCkitD,pclCkifD);
              ilNoCounterBlocks = 0;
              ilI = 0;
              rlCounterBlocks[ilNoCounterBlocks].ilNo = 1;
              rlCounterBlocks[ilNoCounterBlocks].ilIndFirst = ilI;
              rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI;
              while (ilI < ilLastCounter)
              {
                 strcpy(pclCnt,&(rgDedicatedCounters[ilFoundD].pclCntInt[ilI][0]));
                 ilCurCnt = atoi(pclCnt);
                 strcpy(pclCnt,&(rgDedicatedCounters[ilFoundD].pclCntInt[ilI+1][0]));
                 ilNextCnt = atoi(pclCnt);
                 if (ilNextCnt-ilCurCnt-1 <= igGapCount)
                 {
                    rlCounterBlocks[ilNoCounterBlocks].ilNo = rlCounterBlocks[ilNoCounterBlocks].ilNo + 1;
                    rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI + 1;
                 }
                 else
                 {
                    ilNoCounterBlocks++;
                    rlCounterBlocks[ilNoCounterBlocks].ilNo = 1;
                    rlCounterBlocks[ilNoCounterBlocks].ilIndFirst = ilI + 1;
                    rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI + 1;
                 }
                 ilI++;
              }
              ilNoCounterBlocks++;
              ilMax = -1;
              ilMaxInd = 0;
              for (ilI = 0; ilI < ilNoCounterBlocks; ilI++)
              {
                 if (rlCounterBlocks[ilI].ilNo > ilMax)
                 {
                    ilMax = rlCounterBlocks[ilI].ilNo;
                    ilMaxInd = ilI;
                 }
              }
              ilI = rlCounterBlocks[ilMaxInd].ilIndFirst;
              ilJ = rlCounterBlocks[ilMaxInd].ilIndLast;
              strcpy(pclCkifD,&(rgDedicatedCounters[ilFoundD].pclCntInt[ilI][0]));
              strcpy(pclCkitD,&(rgDedicatedCounters[ilFoundD].pclCntInt[ilJ][0]));
           }
        }
     }
     else
     {
        ilLastCounter = rgDedicatedCounters[ilFoundD].ilCurNoDomNam-1;
        if (ilLastCounter >= 0)
        {
           strcpy(pclCkifD,&(rgDedicatedCounters[ilFoundD].pclCntDom[0][0]));
           if (igGapCount == 99999 || ilLastCounter == 0)
           {
              strcpy(pclCkitD,&(rgDedicatedCounters[ilFoundD].pclCntDom[ilLastCounter][0]));
           }
           else
           {
              strcpy(pclCkitD,pclCkifD);
              ilNoCounterBlocks = 0;
              ilI = 0;
              rlCounterBlocks[ilNoCounterBlocks].ilNo = 1;
              rlCounterBlocks[ilNoCounterBlocks].ilIndFirst = ilI;
              rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI;
              while (ilI < ilLastCounter)
              {
                 strcpy(pclCnt,&(rgDedicatedCounters[ilFoundD].pclCntDom[ilI][0]));
                 ilCurCnt = atoi(pclCnt);
                 strcpy(pclCnt,&(rgDedicatedCounters[ilFoundD].pclCntDom[ilI+1][0]));
                 ilNextCnt = atoi(pclCnt);
                 if (ilNextCnt-ilCurCnt-1 <= igGapCount)
                 {
                    rlCounterBlocks[ilNoCounterBlocks].ilNo = rlCounterBlocks[ilNoCounterBlocks].ilNo + 1;
                    rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI + 1;
                 }
                 else
                 {
                    ilNoCounterBlocks++;
                    rlCounterBlocks[ilNoCounterBlocks].ilNo = 1;
                    rlCounterBlocks[ilNoCounterBlocks].ilIndFirst = ilI + 1;
                    rlCounterBlocks[ilNoCounterBlocks].ilIndLast = ilI + 1;
                 }
                 ilI++;
              }
              ilNoCounterBlocks++;
              ilMax = -1;
              ilMaxInd = 0;
              for (ilI = 0; ilI < ilNoCounterBlocks; ilI++)
              {
                 if (rlCounterBlocks[ilI].ilNo > ilMax)
                 {
                    ilMax = rlCounterBlocks[ilI].ilNo;
                    ilMaxInd = ilI;
                 }
              }
              ilI = rlCounterBlocks[ilMaxInd].ilIndFirst;
              ilJ = rlCounterBlocks[ilMaxInd].ilIndLast;
              strcpy(pclCkifD,&(rgDedicatedCounters[ilFoundD].pclCntDom[ilI][0]));
              strcpy(pclCkitD,&(rgDedicatedCounters[ilFoundD].pclCntDom[ilJ][0]));
           }
        }
     }
  }
/*dbg(TRACE,"Found Counters <%s> <%s> <%s>",pcpFlno,pclCkifD,pclCkitD);*/

  if (ilFoundC < 0 && ilFoundD < 0)
  {
     if (pcpCDA[0] == 'X' || pcpCDA[0] == 'Y')
     {
        if (strcmp(pcpMain,"M") == 0)
        {
           strcpy(pcgCKIF," ");
           strcpy(pcgCKIT," ");
           strcpy(pcgCKIF_C," ");
           strcpy(pcgCKIT_C," ");
           strcpy(pcgCKIF_D," ");
           strcpy(pcgCKIT_D," ");
        }
        strcpy(pcpCKIF,pcgCKIF);
        strcpy(pcpCKIT,pcgCKIT);
     }
     else
     {
        strcpy(pcgCKIF," ");
        strcpy(pcgCKIT," ");
        strcpy(pcgCKIF_C," ");
        strcpy(pcgCKIT_C," ");
        strcpy(pcgCKIF_D," ");
        strcpy(pcgCKIT_D," ");
     }
dbg(DEBUG,"1: CKIF = %s, CKIT = %s",pcpCKIF,pcpCKIT);
     return(ilRC);
  }

  strcpy(pcgCKIF_C,pclCkifC);
  strcpy(pcgCKIT_C,pclCkitC);
  strcpy(pcgCKIF_D,pclCkifD);
  strcpy(pcgCKIT_D,pclCkitD);

  if (pcpCDA[0] == 'X')
  {
     if (ilFoundC >= 0)
     {
        ilFoundD = -1;
        strcpy(pcgCKIF_D," ");
        strcpy(pcgCKIT_D," ");
        strcpy(pclCkifD," ");
        strcpy(pclCkitD," ");
     }
  }
  else
  {
     if (pcpCDA[0] == 'Y')
     {
        if (ilFoundD >= 0)
        {
           ilFoundC = -1;
           strcpy(pcgCKIF_C," ");
           strcpy(pcgCKIT_C," ");
           strcpy(pclCkifC," ");
           strcpy(pclCkitC," ");
        }
     }
  }

  if (igGapCount != 99999)
  {
     if (pclCkifC[0] != cBLANK && pclCkitC[0] != cBLANK &&
         pclCkifD[0] != cBLANK && pclCkitD[0] != cBLANK)
     {
        ilCkifC = atoi(pclCkifC);
        ilCkitC = atoi(pclCkitC);
        ilCkifD = atoi(pclCkifD);
        ilCkitD = atoi(pclCkitD);
        if (ilCkitD < ilCkifC)
        {
           if (ilCkifC-ilCkitD-1 > igGapCount)
           {
              strcpy(pclCkifD," ");
              strcpy(pclCkitD," ");
           }
        }
        if (ilCkifD > ilCkitC)
        {
           if (ilCkifD-ilCkitC-1 > igGapCount)
           {
              strcpy(pclCkifD," ");
              strcpy(pclCkitD," ");
           }
        }
     }
  }

  if (pclCkifC[0] == cBLANK)
  {
     strcpy(pcpCKIF,pclCkifD);
  }
  else
  {
     if (pclCkifD[0] == cBLANK)
     {
        strcpy(pcpCKIF,pclCkifC);
     }
     else
     {
        if (strcmp(pclCkifC,pclCkifD) <= 0)
        {
           strcpy(pcpCKIF,pclCkifC);
        }
        else
        {
           strcpy(pcpCKIF,pclCkifD);
        }
     }
  }
  if (pclCkitC[0] == cBLANK)
  {
     strcpy(pcpCKIT,pclCkitD);
  }
  else
  {
     if (pclCkitD[0] == cBLANK)
     {
        strcpy(pcpCKIT,pclCkitC);
     }
     else
     {
        if (strcmp(pclCkitC,pclCkitD) >= 0)
        {
           strcpy(pcpCKIT,pclCkitC);
        }
        else
        {
           strcpy(pcpCKIT,pclCkitD);
        }
     }
  }

  strcpy(pcgCKIF,pcpCKIF);
  strcpy(pcgCKIT,pcpCKIT);

dbg(DEBUG,"2: CKIF = %s, CKIT = %s",pcpCKIF,pcpCKIT);
  return(ilRC);

}/* end of GetCommonanddedicatedcounters*/



/******************************************************************************/
/* The GetCDT  routine        CDT = CounterDisplayType                        */
/******************************************************************************/
static int GetCDT(char *pclCDA, char *pcpCDT, int pipNum)
{
 /* pclCDA  is CDT-Entry in .cfg ---- C=common, D=dedicated, A=C+D */

  int ilRC = RC_SUCCESS;
  int ili = 0;

  ili = 1;
  prguCntDisp = prgCntDisp;
  while (ili <= igSec1End)  
  {
     if (strcmp(prguCntDisp->Section, pcpCDT) == 0)
     {
        ilRC = get_real_item(pclCDA, prguCntDisp->COUNTERS, pipNum);
     }
     prguCntDisp++;
     ili++;
  }

  return (ilRC); 
}



/******************************************************************************/
/* The GetCDT  routine        CDT = CounterDisplayType                        */
/******************************************************************************/
static int UFR_AFT_Cmd(char *pcpFields, char *pcpData)
{
  int ilRC = RC_SUCCESS;

  char charbuff[128] = "\0";
  long llRowNumAFT = ARR_FIRST;
  long llRowNumFDD = ARR_FIRST;
  long llRowNumFLD = ARR_FIRST;
  char *pclRowAFT = NULL;
  char *pclRowFDD = NULL;
  char *pclRowFLD = NULL;
  char pclBufURNO[128] = "\0";
  char pclBufADID[128] = "\0";
  char pclBufAURN[128] = "\0";
  char pclBuffer[1024] = "\0";
  char pclTimeBuf[128] = "\0";
  char pclRNAM[128] = "\0";
  char pclSTAT[128] = "\0";
  int ilFieldNum = 0;
  int ilPos = 0;
  int ilCol = 0;

  /*ilRC = SaveIndexInfo(&rgAftArray);*/
 /*ilRC = SaveIndexInfo(&rgFddArray);*/

  dbg(DEBUG,"%05d: This is UFR_AFT_Cmd",__LINE__);

  FindItemInList(pcpFields,"URNO",',',&ilFieldNum,&ilCol,&ilPos);
  get_real_item(charbuff,pcpData,ilFieldNum);  
  TrimRight(charbuff);

  /* Change LSTU in FLDTAB to schedule DSPHDL */ 

  ilRC = InitFldStruct();
  llRowNumFLD = ARR_FIRST;
  while(CEDAArrayGetRowPointer(&rgFldArray.rrArrayHandle,
                               rgFldArray.crArrayName,
                               llRowNumFLD,(void *)&pclRowFLD) == RC_SUCCESS)
  {
     llRowNumFLD = ARR_CURRENT;
     strcpy(pclBufAURN,FIELDVAL(rgFldArray,pclRowFLD,igFldAURN));
     TrimRight(pclBufAURN);
     dbg(DEBUG,"<%s><%s>",charbuff,pclBufAURN);
     if (strcmp(pclBufAURN,charbuff) == 0)    /*charbuff contains received urno*/
     {
        strcpy(pclSTAT,FIELDVAL(rgFldArray,pclRowFLD,igFldSTAT));
        TimeToStr(pclTimeBuf,time(NULL));
        ilRC = CEDAArrayPutField(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,NULL,"LSTU",llRowNumFLD,pclTimeBuf);
	if (pclSTAT[0] == cBLANK)
	{
           ilRC = CEDAArrayPutField(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,NULL,"STAT",llRowNumFLD,"1");
	}
	else
	{
           ilRC = CEDAArrayPutField(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,NULL,"STAT",llRowNumFLD," ");
	}
/*
        ilRC = CEDAArrayGetRowPointer(&rgFldArray.rrArrayHandle,
                                      rgFldArray.crArrayName,
                                      llRowNumFLD,(void *)&pclRowFLD);
        strcpy(pclTimeBuf,FIELDVAL(rgFldArray,pclRowFLD,igFldLSTU));
        strcpy(pclRTYP,FIELDVAL(rgFldArray,pclRowFLD,igFldRTYP));
*/
     }
     llRowNumFLD = ARR_NEXT;
  }
 
#if 0 
  /*delete records corresponding to received URNO(charbuff) */
  
  llRowNumAFT = ARR_FIRST;
  llRowNumFDD = ARR_FIRST;

  while(CEDAArrayGetRowPointer(&rgFddArray.rrArrayHandle,
                   rgFddArray.crArrayName,
                   llRowNumFDD,(void *)&pclRowFDD) == RC_SUCCESS)
    {
      llRowNumFDD = ARR_CURRENT;
      strcpy(pclBufAURN,FIELDVAL(rgFddArray,pclRowFDD,igFddAURN));
      TrimRight(pclBufAURN);
      /* dbg(DEBUG,"%05d: while(getrowpointerFDD) in UFRCmd: Received-URNO: <%s>, pclBufAURN-FDD: <%s>",
     __LINE__,charbuff,pclBufAURN); */
      if (strcmp(pclBufAURN,charbuff) == 0)    /*charbuff contains received urno*/
    {
      
      ilRC = CEDAArrayDeleteRow(&rgFddArray.rrArrayHandle, rgFddArray.crArrayName, llRowNumFDD);
      if (ilRC != RC_SUCCESS)
        {
          dbg(DEBUG, "%05d: CEDAArrayDeleteRow failed",__LINE__); 
        }
      else
        {
          dbg(DEBUG, "%05d: CEDAArrayDeleteRow succeeded",__LINE__);
        } 
    }
      else
    {
      llRowNumFDD = ARR_NEXT;
    }
       
    }
#endif

  llRowNumAFT = ARR_FIRST;
  llRowNumFDD = ARR_FIRST;
  while (CEDAArrayGetRowPointer(&rgAftArray.rrArrayHandle,
                                rgAftArray.crArrayName,
                                llRowNumAFT,(void *)&pclRowAFT) == RC_SUCCESS)
  {
     /* dbg(TRACE,"%05d: UFRCmd: This is the Beginning of while (GetRowPointer) !!",__LINE__);*/
     strcpy(pclBufURNO,FIELDVAL(rgAftArray,pclRowAFT,igAftURNO));
     TrimRight(pclBufURNO);
     /* dbg(DEBUG,"%05d: Received-URNO: <%s>, AFT-URNO: <%s>",__LINE__, charbuff,pclBufURNO); */
     if (strcmp(charbuff,pclBufURNO) == 0)
     {
        dbg(DEBUG,"%05d: RCV-Urno and URNO are the same",__LINE__);
        strcpy(pclBufADID,FIELDVAL(rgAftArray,pclRowAFT,igAftADID));
        TrimRight(pclBufADID);
        dbg(DEBUG,"%05d: in While: pclBufADID = <%s>, igAftADID: %d",__LINE__, pclBufADID,igAftADID);
        if ((strcmp(pclBufADID,"A") == 0) || (strcmp(pclBufADID,"B") == 0))
        { 
           /************** now Handle_Arrival - Flights  *********************/
           dbg(DEBUG,"%05d: Call Handle_Arrival !!! llRowNumFDD: <%d>",__LINE__,llRowNumFDD);
           ilRC = Handle_Arrival(pclRowAFT,llRowNumAFT,&llRowNumFDD);
           if (ilRC != RC_SUCCESS)
           {
              dbg(TRACE,"%05d: Handle_Arrival() failed !!!",__LINE__);
           }
        }
        if ((strcmp(pclBufADID,"D") == 0) || (strcmp(pclBufADID,"B") == 0))
        {
           /************** now Handle_Departure - Flights  *********************/
           dbg(DEBUG,"%05d: Call Handle_Departure !!! llRowNumFDD: <%d>",__LINE__,llRowNumFDD);
           ilRC = Handle_Departure(pclRowAFT,llRowNumAFT,&llRowNumFDD);
           if (ilRC != RC_SUCCESS)
           {
              dbg(TRACE,"%05d: Handle_Departure() failed !!!",__LINE__);
           }
        }
        ilRC = UpdateFLDTAB(pclRowAFT);
     }
     llRowNumAFT = ARR_NEXT;
  }/* end of While */

  dbg(DEBUG,"%05d: While GetRowPointer ended !!!",__LINE__);

  dbg(TRACE,"UFR_AFT_Cmd: CEDAArrayWriteDB FDDTAB");
  ilRC = CEDAArrayWriteDB(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,NULL,NULL,NULL,
                          ARR_COMMIT_TILL_ERR);
 
  dbg(TRACE,"UFR_AFT_Cmd: CEDAArrayWriteDB CCATAB");
  ilRC = CEDAArrayWriteDB(&rgCcaArray.rrArrayHandle,rgCcaArray.crArrayName,NULL,NULL,NULL,NULL,
                          ARR_COMMIT_TILL_ERR);
  
  if (igGenerateFLD == 1)
  {
     dbg(TRACE,"UFR_AFT_Cmd: CEDAArrayWriteDB FLDTAB");
     ilRC = CEDAArrayWriteDB(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,NULL,NULL,NULL,NULL,
                             ARR_COMMIT_TILL_ERR);
  }
  else
  {
     rollback();
  }
 
  return (ilRC);
}

/******************************************************************************/
/* The DisplayFlight routine                                                  */
/******************************************************************************/
static int DisplayFlight (char *pcpINDO, char *pcpFTYP, char *pcpAftTTYP, int pipNum)
{
  int ilRC = RC_SUCCESS;
  int ili = 0;
  char pclItem[10] = "\0";
  char pclThisTTYP[10] = "\0";

  if (igCheckNatureLength == 1)
  {
     strncpy(pclThisTTYP,&pcpAftTTYP[4],1);
     pclThisTTYP[1] = '\0';
  }
  else
  {
     strcpy(pclThisTTYP,pcpAftTTYP);
  }
  TrimRight(pclThisTTYP);

  ili = 1;
  prguDoMix = prgDoMix;
  while (ili <= igSec2End)  
  {
     if (strcmp(prguDoMix->Section,pclThisTTYP) == 0)
     {
        if ((strcmp(pcpINDO,"I") == 0) && (strstr(pcgIntFLTI,pcpFTYP) != NULL))
        {
           ilRC = get_real_item(pclItem, prguDoMix->ITIP, pipNum);
        }

        if ((strcmp(pcpINDO,"I") == 0) && (strstr(pcgDomFLTI,pcpFTYP) != NULL))
        {
           ilRC = get_real_item(pclItem, prguDoMix->ITDP, pipNum);
        }    
  
        if ((strcmp(pcpINDO,"D") == 0) && (strstr(pcgIntFLTI,pcpFTYP) != NULL))
        {
           ilRC = get_real_item(pclItem, prguDoMix->DTIP, pipNum);
        }

        if ((strcmp(pcpINDO,"D") == 0) && (strstr(pcgDomFLTI,pcpFTYP) != NULL))
        {
           ilRC = get_real_item(pclItem, prguDoMix->DTDP, pipNum);
        }
     }
     prguDoMix++;
     ili++;
  }
  
  return (atoi(pclItem));

}/* end of DisplayFlight */




/******************************************************************************/
/* The CCA_Update  routine             */
/******************************************************************************/
static int CCA_Update(char *pcpFields, char *pcpData)
{
  int ilRC = RC_SUCCESS;
  long llRowNumCCA = ARR_FIRST;
  long llRowNumALT = ARR_FIRST;
  long llRowNumAFT = ARR_FIRST;
  char pclCcaURNO[128] = "\0";
  char pclCcaCTYP[128] = "\0";
  char pclCcaFLNU[128] = "\0";
  char pclCcaCKIC[128] = "\0";
  char pclAftURNO[128] = "\0";
  char pclAltURNO[128] = "\0";
  char pclAltALC2[128] = "\0";
  char pclAltALC3[128] = "\0";
  char pclAftALC2[128] = "\0";
  char pclAftALC3[128] = "\0";
  char pclCcaCKBS[128] = "\0";
  char pclCcaCKES[128] = "\0";
  char pclCcaCKBA[128] = "\0";
  char pclCcaCKEA[128] = "\0";
  int ilFieldNum = 0;
  int ilPos = 0;
  int ilCol = 0;
  char *pclRowAFT = NULL;
  char *pclRowCCA = NULL;
  char *pclRowALT = NULL;
  char pclURNOBuffer[128] = "\0";


 

  llRowNumCCA = ARR_FIRST;

  FindItemInList(pcpFields,"URNO",',',&ilFieldNum,&ilCol,&ilPos);
  get_real_item(pclCcaURNO,pcpData,ilFieldNum);  
  TrimRight(pclCcaURNO);
  FindItemInList(pcpFields,"CKIC",',',&ilFieldNum,&ilCol,&ilPos);
  get_real_item(pclCcaCKIC,pcpData,ilFieldNum);  
  TrimRight(pclCcaCKIC);
  FindItemInList(pcpFields,"CKBS",',',&ilFieldNum,&ilCol,&ilPos);
  get_real_item(pclCcaCKBS,pcpData,ilFieldNum);  
  TrimRight(pclCcaCKBS);
  FindItemInList(pcpFields,"CKES",',',&ilFieldNum,&ilCol,&ilPos);
  get_real_item(pclCcaCKES,pcpData,ilFieldNum);  
  TrimRight(pclCcaCKES);
  FindItemInList(pcpFields,"CKBA",',',&ilFieldNum,&ilCol,&ilPos);
  get_real_item(pclCcaCKBA,pcpData,ilFieldNum);  
  TrimRight(pclCcaCKBA);
  FindItemInList(pcpFields,"CKEA",',',&ilFieldNum,&ilCol,&ilPos);
  get_real_item(pclCcaCKEA,pcpData,ilFieldNum);  
  TrimRight(pclCcaCKEA);

  while(CEDAArrayGetRowPointer(&rgCcaArray.rrArrayHandle,
                   rgCcaArray.crArrayName,
                   llRowNumCCA,(void *)&pclRowCCA) == RC_SUCCESS)
    {
      llRowNumCCA = ARR_CURRENT;

      strcpy(pclURNOBuffer,FIELDVAL(rgCcaArray,pclRowCCA,igCcaURNO));
      TrimRight(pclURNOBuffer);

      if (strcmp(pclCcaURNO,pclURNOBuffer) == 0)
    {
      strcpy(pclCcaCTYP,FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP));
      TrimRight(pclCcaCTYP);
      if (strcmp(pclCcaCTYP," ") == 0)
        {
          strcpy(pclAftURNO,FIELDVAL(rgCcaArray,pclRowCCA,igCcaFLNU));  
          dbg(DEBUG,"CCA_Update: Call FDD_Update with AFT URNO <%s> (Dedicated)",pclAftURNO);
          ilRC = FDD_Update(pclAftURNO,NULL,0);
        }
      if (strcmp(pclCcaCTYP,"C") == 0)
      {
         strcpy(pclAltURNO,FIELDVAL(rgCcaArray,pclRowCCA,igCcaFLNU)); 
         TrimRight(pclAltURNO);

         llRowNumALT = ARR_FIRST;

         while(CEDAArrayGetRowPointer(&rgAltArray.rrArrayHandle,
                       rgAltArray.crArrayName,
                       llRowNumALT,(void *)&pclRowALT) == RC_SUCCESS)
         {
            llRowNumALT = ARR_CURRENT;
            strcpy(pclURNOBuffer,FIELDVAL(rgAltArray,pclRowALT,igAltURNO));
            TrimRight(pclURNOBuffer);
            if (strcmp(pclAltURNO,pclURNOBuffer) == 0)
            {
                strcpy(pclAltALC2,FIELDVAL(rgAltArray,pclRowALT,igAltALC2)); 
		TrimRight(pclAltALC2);
                strcpy(pclAltALC3,FIELDVAL(rgAltArray,pclRowALT,igAltALC3)); 
		TrimRight(pclAltALC3);
               
                llRowNumAFT = ARR_FIRST;
                while(CEDAArrayGetRowPointer(&rgAftArray.rrArrayHandle,
                           rgAftArray.crArrayName,
                           llRowNumAFT,(void *)&pclRowAFT) == RC_SUCCESS)
                {
                   llRowNumAFT = ARR_CURRENT;
                   strcpy(pclAftALC2,FIELDVAL(rgAftArray,pclRowAFT,igAftALC2));
                   TrimRight(pclAftALC2);
                   strcpy(pclAftALC3,FIELDVAL(rgAftArray,pclRowAFT,igAftALC3));
                   TrimRight(pclAftALC3);
                   if ((strcmp(pclAftALC2,pclAltALC2) == 0) || (strcmp(pclAftALC3,pclAltALC3) == 0))
                   {
                      strcpy(pclAftURNO,FIELDVAL(rgAftArray,pclRowAFT,igAftURNO));
                      TrimRight(pclAftURNO);
                      dbg(DEBUG,"CCA_Update: Call FDD_Update with AFT URNO <%s> (Common)",pclAftURNO);
                      ilRC = FDD_Update(pclAftURNO,pclRowAFT,llRowNumAFT);
                   }
                   llRowNumAFT = ARR_NEXT;

                 } /*end of While GetRowPointer AFT*/

              } /*if (strcmp(pclAltURNO,pclURNOBuffer) == 0)*/

              llRowNumALT = ARR_NEXT;

           } /* end of While GetRowPointer ALT*/

        } /*if (strcmp(pclCcaCTYP,"C") == 0)*/

    } /*if (strcmp(pclCcaURNO,pclURNOBuffer) == 0)*/


     llRowNumCCA = ARR_NEXT;
      
    
    } /* end of while GetRowPointer CCA*/


  dbg(TRACE,"CCA_Update: CEDAArrayWriteDB FDDTAB");
  ilRC = CEDAArrayWriteDB(&rgFddArray.rrArrayHandle,rgFddArray.crArrayName,NULL,NULL,NULL,NULL,
                          ARR_COMMIT_TILL_ERR);

  dbg(TRACE,"CCA_Update: CEDAArrayWriteDB CCATAB");
  ilRC = CEDAArrayWriteDB(&rgCcaArray.rrArrayHandle,rgCcaArray.crArrayName,NULL,NULL,NULL,NULL,
                          ARR_COMMIT_TILL_ERR);

  if (igGenerateFLD == 1)
  {
     dbg(TRACE,"CCA_Update: CEDAArrayWriteDB FLDTAB");
     ilRC = CEDAArrayWriteDB(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,NULL,NULL,NULL,NULL,
                             ARR_COMMIT_TILL_ERR);
  }
  else
  {
     rollback();
  }
  
  return (ilRC);
}

/******************************************************************************/
/* The FDD_Update  routine        CDT = CounterDisplayType                        */
/******************************************************************************/
static int FDD_Update(char *pcpAftURNO, char *pcpRowAFT, long lpRowNumAFT)
{
  int ilRC = RC_SUCCESS;

  char charbuff[128] = "\0";
  long llRowNumAFT = ARR_FIRST;
  long llRowNumFDD = ARR_FIRST;
  char *pclRowAFT = NULL;
  char *pclRowFDD = NULL;
  char pclBufURNO[128] = "\0";
  char pclBufADID[128] = "\0";
  char pclBufAURN[128] = "\0";
  char pclBuffer[1024] = "\0";
  int ilFieldNum = 0;
  int ilPos = 0;
  int ilCol = 0;
  int ilFound = 0;

  dbg(DEBUG,"FDD_Update called with URNO <%s>",pcpAftURNO);

  strcpy(charbuff,pcpAftURNO);
  TrimRight(charbuff);

  /*delete records corresponding to received URNO(charbuff) */

  llRowNumFDD = ARR_FIRST;
  while(CEDAArrayGetRowPointer(&rgFddArray.rrArrayHandle,
                               rgFddArray.crArrayName,
                               llRowNumFDD,(void *)&pclRowFDD) == RC_SUCCESS)
  {
     llRowNumFDD = ARR_CURRENT;
     strcpy(pclBufAURN,FIELDVAL(rgFddArray,pclRowFDD,igFddAURN));
     TrimRight(pclBufAURN);
     if (strcmp(pclBufAURN,charbuff) == 0)    /*charbuff contains received urno*/
     {
        ilRC = CEDAArrayDeleteRow(&rgFddArray.rrArrayHandle, rgFddArray.crArrayName, llRowNumFDD);
        if (ilRC != RC_SUCCESS)
        {
           dbg(DEBUG, "%05d: CEDAArrayDeleteRow failed",__LINE__);
        }
     }
     else
     {
         llRowNumFDD = ARR_NEXT;
     }
  }

  if (pcpRowAFT == NULL)
  {
     llRowNumAFT = ARR_FIRST;
     ilFound = 0;
  }
  else
  {
     pclRowAFT = pcpRowAFT;
     llRowNumAFT = lpRowNumAFT;
     ilFound = 1;
  }
  while(ilFound == 0 && CEDAArrayGetRowPointer(&rgAftArray.rrArrayHandle,
                                               rgAftArray.crArrayName,
                                               llRowNumAFT,(void *)&pclRowAFT) == RC_SUCCESS)
  {
      strcpy(pclBufURNO,FIELDVAL(rgAftArray,pclRowAFT,igAftURNO));
      TrimRight(pclBufURNO);
      /*   ilRC = SaveIndexInfo(&rgAftArray);*/
      /*ilRC = SaveIndexInfo(&rgFddArray);*/
      if (strcmp(charbuff,pclBufURNO) == 0)
      {
         ilFound = 1; 
      }
      else
      {
         llRowNumAFT = ARR_NEXT;
      }
  }/* end of While */
  if (ilFound == 1)
  {
     strcpy(pclBufADID,FIELDVAL(rgAftArray,pclRowAFT,igAftADID));
     TrimRight(pclBufADID);
     if ((strcmp(pclBufADID,"A") == 0) || (strcmp(pclBufADID,"B") == 0))
     { 
        /************** now Handle_Arrival - Flights  *********************/
        ilRC = Handle_Arrival(pclRowAFT,llRowNumAFT,&llRowNumFDD);
        if (ilRC != RC_SUCCESS)
        {
           dbg(TRACE,"%05d: Handle_Arrival() failed !!!",__LINE__);
        }
     }
     if ((strcmp(pclBufADID,"D") == 0) || (strcmp(pclBufADID,"B") == 0))
     {
        /************** now Handle_Departure - Flights  *********************/
        ilRC = Handle_Departure(pclRowAFT,llRowNumAFT,&llRowNumFDD);
        if (ilRC != RC_SUCCESS)
        {
           dbg(TRACE,"%05d: Handle_Departure() failed !!!",__LINE__);
        }
     }
  }

  return (ilRC);
}


/******************************************************************************/
/* The NumDomVia                                                              */
/******************************************************************************/
static int NumDomVia(char *pcpTTYP)
{
  int ili = 0;
  int ilNumber = -1;
  char pclNumber[128] = "\0";
  char pclThisTTYP[128] = "\0";
  prguDoMix = prgDoMix;
 
  if (igCheckNatureLength == 1)
  { 
     strncpy(pclThisTTYP,&pcpTTYP[4],1);
     pclThisTTYP[1] = '\0';
  }
  else
  {
     strcpy(pclThisTTYP,pcpTTYP);
  }
  TrimRight(pclThisTTYP);

  for (ili = 0; (ili <= igSec2End); ili ++)
    {
      /*dbg(TRACE,"%05d: NumDomVia: prguDoMix->Section: <%s>, pclThisTTYP: <%s>",__LINE__,prguDoMix->Section,pclThisTTYP);*/
      if (strcmp(prguDoMix->Section,pclThisTTYP) == 0)
    {
      strcpy(pclNumber,prguDoMix->DOMVIA);
      ilNumber = atoi(pclNumber);
    }
      prguDoMix++ ;
    }
 
  /*dbg(TRACE,"%05d: Number Domestic Vias : <%d>",__LINE__,ilNumber);*/

  return (ilNumber);
}



/******************************************************************************/
/* The NumDomVia                                                              */
/******************************************************************************/
static int SetDomVia (char *pcpADID, char *pcpFTYP, int pipNumDomVia, char  *pclCurRowAFT)
{
  int ilRC = RC_SUCCESS;
  int ilVIAN = 0;
  char pclVIAL[2048] = "\0";
  

  /*dbg(TRACE,"%05d: SetDomVia: pcpADID: <%s>, pcpFTYP: <%s>, pipNumDomVia: <%d>",__LINE__,pcpADID, pcpFTYP, pipNumDomVia);*/

  if ((strcmp(pcpADID,"A") == 0) &&
      (strstr(pcgIntFLTI,pcpFTYP) != NULL) &&
      (pipNumDomVia == 1))
    {
      
      strcpy(rgFddFlds.pclFddAPC3,FIELDVAL(rgAftArray,pclCurRowAFT,igAftORG3));
      strcpy(pclVIAL,FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIAL));
      ilVIAN = atoi(FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIAN));
      if (ilVIAN == 1)
    {
      strcpy(rgFddFlds.pclFddVIA3,FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIA3));
      /*strncpy(rgFddFlds.pclFddVIA3,&pclVIAL[1],3);*/
      /*rgFddFlds.pclFddVIA3[3] = '\0';*/
    }
      else
    {
      strncpy(rgFddFlds.pclFddVIA3,&pclVIAL[((ilVIAN-2)*120)+1],3);
      rgFddFlds.pclFddVIA3[3] = '\0';
    }

    }

  if ((strcmp(pcpADID,"A") == 0) &&
      (strstr(pcgIntFLTI,pcpFTYP) != NULL) &&
      (pipNumDomVia == 2))
    {
      strcpy(rgFddFlds.pclFddAPC3,FIELDVAL(rgAftArray,pclCurRowAFT,igAftORG3));
      strcpy(pclVIAL,FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIAL));
      ilVIAN = atoi(FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIAN));
      if (ilVIAN == 2)
    {
      strcpy(rgFddFlds.pclFddVIA3,"\0");
    }
      else
    {
      strncpy(rgFddFlds.pclFddVIA3,&pclVIAL[((ilVIAN-pipNumDomVia-1)*120)+1],3);
      rgFddFlds.pclFddVIA3[3] = '\0';
    }
    
    }


  if ((strcmp(pcpADID,"A") == 0) &&
      (strstr(pcgDomFLTI,pcpFTYP) != NULL) &&
      (pipNumDomVia == 1))
    {
      strcpy(pclVIAL,FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIAL));
      ilVIAN = atoi(FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIAN));
      strncpy(rgFddFlds.pclFddAPC3,&pclVIAL[((ilVIAN-1)*120)+1],3);
      rgFddFlds.pclFddAPC3[3] = '\0';
    }

  if ((strcmp(pcpADID,"A") == 0) &&
      (strstr(pcgDomFLTI,pcpFTYP) != NULL) &&
      (pipNumDomVia == 2))
    {
      strcpy(pclVIAL,FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIAL));
      ilVIAN = atoi(FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIAN));
      strncpy(rgFddFlds.pclFddAPC3,&pclVIAL[((ilVIAN-2)*120)+1],3);
      rgFddFlds.pclFddAPC3[3] = '\0';
      strncpy(rgFddFlds.pclFddVIA3,&pclVIAL[((ilVIAN-1)*120)+1],3);
      rgFddFlds.pclFddAPC3[3] = '\0';
    }


  if ((strcmp(pcpADID,"D") == 0) &&
      (strstr(pcgIntFLTI,pcpFTYP) != NULL) &&
      (pipNumDomVia == 1) && 
      (atoi(FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIAN)) > 1))
    {
      strcpy(rgFddFlds.pclFddAPC3,FIELDVAL(rgAftArray,pclCurRowAFT,igAftDES3));
      strcpy(pclVIAL,FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIAL));
      strncpy(rgFddFlds.pclFddVIA3,&pclVIAL[121],3);
      rgFddFlds.pclFddVIA3[3] = '\0';
    }

  if ((strcmp(pcpADID,"D") == 0) &&
      (strstr(pcgIntFLTI,pcpFTYP) != NULL) &&
      (pipNumDomVia == 2) &&
      (atoi(FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIAN)) > 2))
    {
      strcpy(rgFddFlds.pclFddAPC3,FIELDVAL(rgAftArray,pclCurRowAFT,igAftDES3));
      strcpy(pclVIAL,FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIAL));
      ilVIAN = atoi(FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIAN));
      strncpy(rgFddFlds.pclFddVIA3,&pclVIAL[((ilVIAN-1)*120)+1],3);
      rgFddFlds.pclFddVIA3[3] = '\0';
    }

  if ((strcmp(pcpADID,"D") == 0) &&
      (strstr(pcgDomFLTI,pcpFTYP) != NULL) &&
      (pipNumDomVia == 1))
    {
      strcpy(pclVIAL,FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIAL));
      strncpy(rgFddFlds.pclFddAPC3,&pclVIAL[1],3);
      rgFddFlds.pclFddAPC3[3] = '\0';
    }

  if ((strcmp(pcpADID,"D") == 0) &&
      (strstr(pcgDomFLTI,pcpFTYP) != NULL) &&
      (pipNumDomVia == 2))
    {
      strcpy(pclVIAL,FIELDVAL(rgAftArray,pclCurRowAFT,igAftVIAL));
      strncpy(rgFddFlds.pclFddAPC3,&pclVIAL[121],3);
      rgFddFlds.pclFddAPC3[3] = '\0';
      strncpy(rgFddFlds.pclFddVIA3,&pclVIAL[1],3);
      rgFddFlds.pclFddVIA3[3] = '\0';

    }

  /*dbg(TRACE,"%05d: SetDomVias : APC3: <%s>, VIA3: <%s>",__LINE__,rgFddFlds.pclFddAPC3,rgFddFlds.pclFddVIA3);*/
  return (ilRC);
} 



/******************************************************************************/
/* The SetDNAT                                                              */
/******************************************************************************/
static int SetDNAT(char *pcpSection)
{
  int ili = 0;
  /*dbg(TRACE,"%05d: ##############################pcpSection : <%s>",__LINE__,pcpSection);*/
  prguCntDisp = prgCntDisp;

  for (ili = 0; (ili <= igSec1End); ili ++)
    {
      if (strcmp(prguCntDisp->Section,pcpSection) == 0)
    {
      strcpy(rgFddFlds.pclFddDNAT,prguCntDisp->PUST);
           }
      prguCntDisp++ ;
    }

  /*dbg(TRACE,"%05d: PUST-DNAT : <%s>",__LINE__,rgFddFlds.pclFddDNAT);*/

  return (0);
}


/******************************************************************************/
/* The InitFldStruct routine                                                    */
/******************************************************************************/
static int InitFldStruct()
{
  int ilRC = RC_SUCCESS;

  memset(&rgFldFlds,0x00,sizeof(rgFldFlds));

  strcpy(rgFldFlds.pclFldAURN, "0");
  strcpy(rgFldFlds.pclFldURNO, "0");
  strcpy(rgFldFlds.pclFldABTI, "\0");
  strcpy(rgFldFlds.pclFldACTI, "\0");
  strcpy(rgFldFlds.pclFldAFTI, "\0");
  strcpy(rgFldFlds.pclFldAOTI, "\0");
  strcpy(rgFldFlds.pclFldCDAT, "\0");
  strcpy(rgFldFlds.pclFldCREC, "\0");
  strcpy(rgFldFlds.pclFldCTYP, "\0");
  strcpy(rgFldFlds.pclFldDCTI, "\0");
  strcpy(rgFldFlds.pclFldDOTI, "\0");
  strcpy(rgFldFlds.pclFldDSEQ, "0");
  strcpy(rgFldFlds.pclFldHOPO, "\0");
  strcpy(rgFldFlds.pclFldLSTU, "\0");
  strcpy(rgFldFlds.pclFldRNAM, "\0");
  strcpy(rgFldFlds.pclFldRTAB, "\0");
  strcpy(rgFldFlds.pclFldRURN, "0");
  strcpy(rgFldFlds.pclFldRTYP, "\0");
  strcpy(rgFldFlds.pclFldSCTI, "\0");
  strcpy(rgFldFlds.pclFldSOTI, "\0");
  strcpy(rgFldFlds.pclFldSTAT, "\0");
  strcpy(rgFldFlds.pclFldUSEC, "\0");
  strcpy(rgFldFlds.pclFldUSEU, "\0");
  strcpy(rgFldFlds.pclFldTIFF, "\0");
  strcpy(rgFldFlds.pclFldOBLF, "\0");
  strcpy(rgFldFlds.pclFldSTOF, "\0");
  strcpy(rgFldFlds.pclFldFLNO, "\0");
  strcpy(rgFldFlds.pclFldJFNO, "\0");
  strcpy(rgFldFlds.pclFldJCNT, "\0");
 
  return (ilRC);
}/* end of initFldStruct */


/******************************************************************************/
/* The ShowFldStruct routine                                                    */
/******************************************************************************/
static int ShowFldStruct()
{
  int ilRC = RC_SUCCESS;
 
  dbg(DEBUG,"FldStruct : pclFldAURN <%s>",rgFldFlds.pclFldAURN);
  dbg(DEBUG,"FldStruct : pclFldURNO <%s>",rgFldFlds.pclFldURNO);
  dbg(DEBUG,"FldStruct : pclFldABTI <%s>",rgFldFlds.pclFldABTI);
  dbg(DEBUG,"FldStruct : pclFldACTI <%s>",rgFldFlds.pclFldACTI);
  dbg(DEBUG,"FldStruct : pclFldAFTI <%s>",rgFldFlds.pclFldAFTI);
  dbg(DEBUG,"FldStruct : pclFldAOTI <%s>",rgFldFlds.pclFldAOTI);
  dbg(DEBUG,"FldStruct : pclFldCDAT <%s>",rgFldFlds.pclFldCDAT);
  dbg(DEBUG,"FldStruct : pclFldCREC <%s>",rgFldFlds.pclFldCREC);
  dbg(DEBUG,"FldStruct : pclFldCTYP <%s>",rgFldFlds.pclFldCTYP);
  dbg(DEBUG,"FldStruct : pclFldDCTI <%s>",rgFldFlds.pclFldDCTI);
  dbg(DEBUG,"FldStruct : pclFldDOTI <%s>",rgFldFlds.pclFldDOTI);
  dbg(DEBUG,"FldStruct : pclFldDSEQ <%s>",rgFldFlds.pclFldDSEQ);
  dbg(DEBUG,"FldStruct : pclFldHOPO <%s>",rgFldFlds.pclFldHOPO);
  dbg(DEBUG,"FldStruct : pclFldLSTU <%s>",rgFldFlds.pclFldLSTU);
  dbg(DEBUG,"FldStruct : pclFldRNAM <%s>",rgFldFlds.pclFldRNAM);
  dbg(DEBUG,"FldStruct : pclFldRTAB <%s>",rgFldFlds.pclFldRTAB);
  dbg(DEBUG,"FldStruct : pclFldRURN <%s>",rgFldFlds.pclFldRURN);
  dbg(DEBUG,"FldStruct : pclFldRTYP <%s>",rgFldFlds.pclFldRTYP);
  dbg(DEBUG,"FldStruct : pclFldSCTI <%s>",rgFldFlds.pclFldSCTI);
  dbg(DEBUG,"FldStruct : pclFldSOTI <%s>",rgFldFlds.pclFldSOTI);
  dbg(DEBUG,"FldStruct : pclFldSTAT <%s>",rgFldFlds.pclFldSTAT);
  dbg(DEBUG,"FldStruct : pclFldUSEC <%s>",rgFldFlds.pclFldUSEC);
  dbg(DEBUG,"FldStruct : pclFldUSEU <%s>",rgFldFlds.pclFldUSEU);
  dbg(DEBUG,"FldStruct : pclFldTIFF <%s>",rgFldFlds.pclFldTIFF);
  dbg(DEBUG,"FldStruct : pclFldOBLF <%s>",rgFldFlds.pclFldTIFF);
  dbg(DEBUG,"FldStruct : pclFldSTOF <%s>",rgFldFlds.pclFldTIFF);
  dbg(DEBUG,"FldStruct : pclFldFLNO <%s>",rgFldFlds.pclFldFLNO);
  dbg(DEBUG,"FldStruct : pclFldJFNO <%s>",rgFldFlds.pclFldJFNO);
  dbg(DEBUG,"FldStruct : pclFldJCNT <%s>",rgFldFlds.pclFldJCNT);
  return (ilRC);

}/*end of showfldstruct */


/******************************************************************************/
/* The StructPutFieldFLD routine                                               */
/******************************************************************************/
static int StructPutFieldFLD(long *plpRowNumFLD)
{
  int ilRC = RC_SUCCESS;
  char pclRowBuffer[4096];

  sprintf(pclRowBuffer,
          "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,"
          "%s,%s,%s",
          rgFldFlds.pclFldAURN,
          rgFldFlds.pclFldURNO,
          rgFldFlds.pclFldABTI,
          rgFldFlds.pclFldACTI,
          rgFldFlds.pclFldAFTI,
          rgFldFlds.pclFldAOTI,
          rgFldFlds.pclFldCDAT,
          rgFldFlds.pclFldCREC,
          rgFldFlds.pclFldCTYP,
          rgFldFlds.pclFldDCTI,
          rgFldFlds.pclFldDOTI,
          rgFldFlds.pclFldDSEQ,
          rgFldFlds.pclFldHOPO,
          rgFldFlds.pclFldLSTU,
          rgFldFlds.pclFldRNAM,
          rgFldFlds.pclFldRTAB,
          rgFldFlds.pclFldRURN,
          rgFldFlds.pclFldRTYP,
          rgFldFlds.pclFldSCTI,
          rgFldFlds.pclFldSOTI,
          rgFldFlds.pclFldSTAT,
          rgFldFlds.pclFldUSEC,
          rgFldFlds.pclFldUSEU);
  if (igSelectCounterLogo == TRUE)
  {
     strcat(pclRowBuffer,",");
     strcat(pclRowBuffer,rgFldFlds.pclFldFLNO);
     strcat(pclRowBuffer,",");
     strcat(pclRowBuffer,rgFldFlds.pclFldJFNO);
     strcat(pclRowBuffer,",");
     strcat(pclRowBuffer,rgFldFlds.pclFldJCNT);
  }
  strcat(pclRowBuffer,",");
  strcat(pclRowBuffer,rgFldFlds.pclFldTIFF);
  strcat(pclRowBuffer,",");
  strcat(pclRowBuffer,rgFldFlds.pclFldOBLF);
  strcat(pclRowBuffer,",");
  strcat(pclRowBuffer,rgFldFlds.pclFldSTOF);
 
  ilRC = CEDAArrayPutFields(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,NULL,
                            plpRowNumFLD,FLD_FIELDS,pclRowBuffer);
  if (ilRC != RC_SUCCESS)
  {
     dbg(TRACE,"%05d: StructPutFieldFLD: CEDAArrayPutField failed !! RowNum: %d",__LINE__, plpRowNumFLD);
  }

  return (ilRC);
} /* end of StructPutFieldFLD */


/******************************************************************************/
/* The StructAddRowPart routine                                               */
/******************************************************************************/
static int StructAddRowPartFLD(long *plpRowNumFLD)
{
  int ilRC = RC_SUCCESS;
  char pclRowBuffer[4096];

  sprintf(pclRowBuffer,
          "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,"
          "%s,%s,%s",
          rgFldFlds.pclFldAURN,
          rgFldFlds.pclFldURNO,
          rgFldFlds.pclFldABTI,
          rgFldFlds.pclFldACTI,
          rgFldFlds.pclFldAFTI,
          rgFldFlds.pclFldAOTI,
          rgFldFlds.pclFldCDAT,
          rgFldFlds.pclFldCREC,
          rgFldFlds.pclFldCTYP,
          rgFldFlds.pclFldDCTI,
          rgFldFlds.pclFldDOTI,
          rgFldFlds.pclFldDSEQ,
          rgFldFlds.pclFldHOPO,
          rgFldFlds.pclFldLSTU,
          rgFldFlds.pclFldRNAM,
          rgFldFlds.pclFldRTAB,
          rgFldFlds.pclFldRURN,
          rgFldFlds.pclFldRTYP,
          rgFldFlds.pclFldSCTI,
          rgFldFlds.pclFldSOTI,
          rgFldFlds.pclFldSTAT,
          rgFldFlds.pclFldUSEC,
          rgFldFlds.pclFldUSEU);
  if (igSelectCounterLogo == TRUE)
  {
     strcat(pclRowBuffer,",");
     strcat(pclRowBuffer,rgFldFlds.pclFldFLNO);
     strcat(pclRowBuffer,",");
     strcat(pclRowBuffer,rgFldFlds.pclFldJFNO);
     strcat(pclRowBuffer,",");
     strcat(pclRowBuffer,rgFldFlds.pclFldJCNT);
  }
  strcat(pclRowBuffer,",");
  strcat(pclRowBuffer,rgFldFlds.pclFldTIFF);
  strcat(pclRowBuffer,",");
  strcat(pclRowBuffer,rgFldFlds.pclFldOBLF);
  strcat(pclRowBuffer,",");
  strcat(pclRowBuffer,rgFldFlds.pclFldSTOF);

  ilRC = CEDAArrayAddRowPart(&rgFldArray.rrArrayHandle, rgFldArray.crArrayName,plpRowNumFLD,
                             FLD_FIELDS, pclRowBuffer);
  if (ilRC != RC_SUCCESS)
  {
     dbg(TRACE,"%05d: StructAddRowPart:CEDAArrayAddRowPart-FLD failed !! RowNum: %d",__LINE__, *plpRowNumFLD);
  }

  return (ilRC);
} /* end of StructAddRowPartFld */
 


/******************************************************************************/
/* The UpdateFLDTAB routine                                                    */
/******************************************************************************/
static int UpdateFLDTAB(char *pcpCurRowAFT)
{

  int ilRC = RC_SUCCESS;
  long llRowNumFLD = ARR_FIRST;
  long llRowNumFDD = ARR_FIRST;
  long llRowNumCCA = ARR_FIRST;
  long llRowNumCCATest = ARR_FIRST;
  long llRowNumAFT = ARR_FIRST;
  long llOffset = 0;

  int ilBLT1 = 0;
  int ilBLT2 = 0;
  int ilGTD1 = 0;
  int ilGTD2 = 0;
  int ilWRO1 = 0;
  int ilCHECK = 0;

  char *pclRowFLD = NULL;
  char *pclRowFDD = NULL;
  char *pclRowCCA = NULL;
  char *pclRowCCATest = NULL;
  char *pclRowAFT = NULL;
  char pclFldAURN[128] = "\0";

  char pclFldSOTI[128] = "\0";
  char pclFldSCTI[128] = "\0";
  char pclFldDOTI[128] = "\0";
  char pclFldDCTI[128] = "\0";
  char pclFldRNAM[128] = "\0";
  char pclFldRTYP[128] = "\0";
  char pclFldRURN[128] = "\0";

  char pclAftURNO[128] = "\0";
  char pclAftBLT1[128] = "\0";
  char pclAftBLT2[128] = "\0";
  char pclAftGTD1[128] = "\0";
  char pclAftGTD2[128] = "\0";
  char pclAftWRO1[128] = "\0";
  char pclAftFLTI[128] = "\0";
  char pclAftCKIF[128] = "\0";
  char pclOpen[128]= "\0";
  char pclOpen1[128]= "\0";
  char pclClose[128]= "\0";
  char pclFirstBag[128]= "\0";
  char pclLastBag[128]= "\0";
  char pclCcaCKIC[128] = "\0";
  char pclBUF[128] = "\0";
  char pclADID[128] = "\0";
  char pclAftREMP[128] = "\0";
  char pclGate2Remark[128] = "\0";
  char pclTmpTime[128] = "\0";
  char clKey[128] = "\0";
  char pclTimeBuf[128] = "\0";
  long llRowCount = 0;
  char pclTmpBuf[128];

  int ilDelInd = 0;

  int ilCrecField;
  int ilCol;
  int ilPos;

  int ilRCdb = DB_SUCCESS;
  short slFunction;
  short slCursor;
  char pclSqlBuf[1024] = "\0";
  char pclData[1024] = "\0";
  char pclFlzUrno[16];
  char pclFlzLnam[16];
  char pclSelection[1024];

  ilRC = InitFldStruct();
  
  strcpy(pclAftURNO,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftURNO));
  TrimRight(pclAftURNO);
  dbg(DEBUG,"%05d:                                                                                          ",__LINE__);
  dbg(DEBUG,"%05d: =========================================================================================",__LINE__);
  dbg(DEBUG,"%05d: ===============Beginning of UpdateFLDTAB, working on AFT-URNO: <%s> ===========",__LINE__, pclAftURNO);
  dbg(DEBUG,"%05d: =========================================================================================",__LINE__);

  strcpy(pclAftFLTI,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftFLTI));
  TrimRight(pclAftFLTI);

  strcpy(pclAftREMP,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP));
  TrimRight(pclAftREMP);

  strcpy(pclAftBLT1,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftBLT1));
  TrimRight(pclAftBLT1);
  if (strcmp(pclAftBLT1," ") != 0)  ilBLT1 = 1;

  strcpy(pclAftBLT2,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftBLT2));
  TrimRight(pclAftBLT2);
  if (strcmp(pclAftBLT2," ") != 0)  ilBLT2 = 1;
    
  strcpy(pclAftGTD1,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGTD1));
  TrimRight(pclAftGTD1);
  if (strcmp(pclAftGTD1," ") != 0)  ilGTD1 = 1;
    
  strcpy(pclAftGTD2,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGTD2));
  TrimRight(pclAftGTD2);
  if (strcmp(pclAftGTD2," ") != 0)  ilGTD2 = 1;
    
  strcpy(pclAftWRO1,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftWRO1));
  TrimRight(pclAftWRO1);
  if (strcmp(pclAftWRO1," ") != 0)  ilWRO1 = 1;

  while(CEDAArrayGetRowPointer(&rgFldArray.rrArrayHandle,
                               rgFldArray.crArrayName,
                               llRowNumFLD,(void *)&pclRowFLD) == RC_SUCCESS)
  {
     llRowNumFLD = ARR_CURRENT;
     strcpy(pclFldAURN,FIELDVAL(rgFldArray,pclRowFLD,igFldAURN));
     TrimRight(pclFldAURN);
     strcpy(pclFldRNAM,FIELDVAL(rgFldArray,pclRowFLD,igFldRNAM));
     TrimRight(pclFldRNAM);
     strcpy(pclFldRTYP,FIELDVAL(rgFldArray,pclRowFLD,igFldRTYP));
     TrimRight(pclFldRTYP);
     
     dbg(DEBUG,"%05d: Data of Actual FLD-Row: pclFldAURN: <%s>, pclFldRNAM: <%s>, pclFldRTYP: <%s>",__LINE__,pclFldAURN,pclFldRNAM,pclFldRTYP);
     if ((strcmp(pclFldAURN,pclAftURNO) == 0) || (strcmp(pclFldAURN,"0") == 0))
     {
        if ((ilBLT1 == 1) && (strcmp(pclFldRNAM,pclAftBLT1) == 0) && (strcmp(pclFldRTYP,pcgRtypBELT) == 0)) 
        {
           dbg(DEBUG,"%05d: Now Update BLT1",__LINE__);
           ilBLT1 = 0;
           ilRC = PutFieldFLD("TIFF",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftTIFA),&llRowNumFLD);
           ilRC = PutFieldFLD("OBLF",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftONBL),&llRowNumFLD);
           ilRC = PutFieldFLD("STOF",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftSTOA),&llRowNumFLD);
           if (igSelectCounterLogo == TRUE)
           {
              ilRC = PutFieldFLD("FLNO",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftFLNO),
                                 &llRowNumFLD);
              ilRC = PutFieldFLD("JFNO",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftJFNO),
                                 &llRowNumFLD);
              ilRC = PutFieldFLD("JCNT",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftJCNT),
                                 &llRowNumFLD);
           }
           if (igUpdateBeltCREC == TRUE &&
               strcmp(pcgFirstBag,"EMPTY") != 0 && strcmp(pcgLastBag,"EMPTY") != 0)
           {
              FindItemInArrayList(AFT_FIELDS,pcgBelt1Open,',',&ilCrecField,&ilCol,&ilPos);
              strcpy(pclFirstBag,FIELDVAL(rgAftArray,pcpCurRowAFT,ilCrecField));
              TrimRight(pclFirstBag);
              FindItemInArrayList(AFT_FIELDS,pcgBelt1Close,',',&ilCrecField,&ilCol,&ilPos);
              strcpy(pclLastBag,FIELDVAL(rgAftArray,pcpCurRowAFT,ilCrecField));
              TrimRight(pclLastBag);
              if (*pclLastBag != ' ')
                 ilRC = PutFieldFLD("CREC",pcgLastBag,&llRowNumFLD);
              else if (*pclFirstBag != ' ')
                 ilRC = PutFieldFLD("CREC",pcgFirstBag,&llRowNumFLD);
              else
                 ilRC = PutFieldFLD("CREC"," ",&llRowNumFLD);
           }
           TimeToStr(pclTimeBuf,time(NULL));
           ilRC = PutFieldFLD("LSTU",pclTimeBuf,&llRowNumFLD);
           ilRC = PutFieldFLD("USEU","FLDHDL",&llRowNumFLD);
           /************************get open and close time for Belt1****************************************/
           if (strcmp(pcgDispBelt,"MANUAL") == 0)
           {
              strcpy(pclOpen,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftB1BA));
              TrimRight(pclOpen);
              strcpy(pclClose,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftB1EA));
              TrimRight(pclClose);
              if (pclOpen[0] != cBLANK && pclClose[0] == cBLANK)
              {
                 strcpy(pclClose,"202031122359");
              }
           }
           else
           {
              if (strcmp(pcgDispBelt,"ONBL") == 0)
              {
                 strcpy(pclOpen1,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftONBL));
                 TimeToStr(pclOpen,time(NULL)); /* take Actual Time for pclOpen instead of ONBL for better testing */
                 TrimRight(pclOpen);
              }
              else
              {
                 if (strcmp(pcgDispBelt,"LAND") == 0)
                 {
                    strcpy(pclOpen1,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftLAND));
                    TimeToStr(pclOpen,time(NULL)); /* take Actual Time for pclOpen instead of LAND for better testing */
                    TrimRight(pclOpen);
                 }
              }          
              strcpy(pclLastBag,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftB1EA));
              TrimRight(pclLastBag);
              if (strcmp(pclLastBag," ") != 0)
              {
                 strcpy(pclClose,pclLastBag);
                 if (strstr(pcgIntFLTI,pclAftFLTI) != NULL || strstr(pcgMixedFLTI,pclAftFLTI) != NULL)
                 {
                    AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc10,1);
                 }
                 else
                 {
                    AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc14,1);
                 }
              }
              else
              {
                 strcpy(pclClose,pclOpen1);
                 if (strstr(pcgIntFLTI,pclAftFLTI) != NULL || strstr(pcgMixedFLTI,pclAftFLTI) != NULL)
                 {
                    AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc05,1);
                 }
                 else
                 {
                    AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc13,1);
                 }
              }
           }
           if (ValidTime(pclOpen,pclClose," "," ") != RC_SUCCESS)
           {
              dbg(DEBUG,"%05d: Time not Valid, fldrow will be deletet (former dseq= -1)",__LINE__);
              ilRC = CEDAArrayPutField(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,NULL,"DSEQ",llRowNumFLD,"-1");
           }
           /**************************************************************/
           SetDSEQ(pclFldRNAM,pclFldRTYP);
           ilRC = CEDAArrayGetRowPointer(&rgFldArray.rrArrayHandle,
                                         rgFldArray.crArrayName,
                                         llRowNumFLD,(void *)&pclRowFLD);
        }
        
        if ((ilBLT2 == 1) && (strcmp(pclFldRNAM,pclAftBLT2) == 0) && (strcmp(pclFldRTYP,pcgRtypBELT) == 0)) 
        {
           dbg(DEBUG,"%05d: Now Update BLT2",__LINE__);
           ilBLT2 = 0;
           ilRC = PutFieldFLD("TIFF",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftTIFA),&llRowNumFLD);
           ilRC = PutFieldFLD("OBLF",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftONBL),&llRowNumFLD);
           ilRC = PutFieldFLD("STOF",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftSTOA),&llRowNumFLD);
           if (igSelectCounterLogo == TRUE)
           {
              ilRC = PutFieldFLD("FLNO",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftFLNO),
                                 &llRowNumFLD);
              ilRC = PutFieldFLD("JFNO",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftJFNO),
                                 &llRowNumFLD);
              ilRC = PutFieldFLD("JCNT",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftJCNT),
                                 &llRowNumFLD);
           }
           if (igUpdateBeltCREC == TRUE &&
               strcmp(pcgFirstBag,"EMPTY") != 0 && strcmp(pcgLastBag,"EMPTY") != 0)
           {
              FindItemInArrayList(AFT_FIELDS,pcgBelt2Open,',',&ilCrecField,&ilCol,&ilPos);
              strcpy(pclFirstBag,FIELDVAL(rgAftArray,pcpCurRowAFT,ilCrecField));
              TrimRight(pclFirstBag);
              FindItemInArrayList(AFT_FIELDS,pcgBelt2Close,',',&ilCrecField,&ilCol,&ilPos);
              strcpy(pclLastBag,FIELDVAL(rgAftArray,pcpCurRowAFT,ilCrecField));
              TrimRight(pclLastBag);
              if (*pclLastBag != ' ')
                 ilRC = PutFieldFLD("CREC",pcgLastBag,&llRowNumFLD);
              else if (*pclFirstBag != ' ')
                 ilRC = PutFieldFLD("CREC",pcgFirstBag,&llRowNumFLD);
              else
                 ilRC = PutFieldFLD("CREC"," ",&llRowNumFLD);
           }
           TimeToStr(pclTimeBuf,time(NULL));
           ilRC = PutFieldFLD("LSTU",pclTimeBuf,&llRowNumFLD);
           ilRC = PutFieldFLD("USEU","FLDHDL",&llRowNumFLD);
           /************************get open and close time for Belt1****************************************/
           if (strcmp(pcgDispBelt,"MANUAL") == 0)
           {
              strcpy(pclOpen,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftB2BA));
              TrimRight(pclOpen);
              strcpy(pclClose,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftB2EA));
              TrimRight(pclClose);
              if (pclOpen[0] != cBLANK && pclClose[0] == cBLANK)
              {
                 strcpy(pclClose,"202031122359");
              }
           }
           else
           {
              if (strcmp(pcgDispBelt,"ONBL") == 0)
              {
                 strcpy(pclOpen1,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftONBL));
                 TimeToStr(pclOpen,time(NULL)); /* take Actual Time for pclOpen instead of ONBL for better testing */
                 TrimRight(pclOpen);
              }
              else
              {
                 if (strcmp(pcgDispBelt,"LAND") == 0)
                 {
                    strcpy(pclOpen1,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftLAND));
                    TimeToStr(pclOpen,time(NULL)); /* take Actual Time for pclOpen instead of LAND for better testing */
                    TrimRight(pclOpen);
                 }
              }
              strcpy(pclLastBag,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftB2EA));
              TrimRight(pclLastBag);
              if (strcmp(pclLastBag," ") != 0)
              {
                 strcpy(pclClose,pclLastBag);
                 if (strstr(pcgIntFLTI,pclAftFLTI) != NULL)
                 {
                    AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc10,1);
                 }
                 else
                 {
                    AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc14,1);
                 }
              }
              else
              {
                 strcpy(pclClose,pclOpen1);
                 if (strstr(pcgIntFLTI,pclAftFLTI) != NULL)
                 {
                    AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc05,1);
                 }
                 else
                 {
                    AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc13,1);
                 }
              }
           }
           if (ValidTime(pclOpen,pclClose," "," ") != RC_SUCCESS)
           {
              dbg(DEBUG,"%05d: Time not Valid, fldrow will be deletet (former dseq= -1)",__LINE__);
              ilRC = CEDAArrayPutField(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,NULL,"DSEQ",llRowNumFLD,"-1");
           }
           /**************************************************************/
           SetDSEQ(pclFldRNAM,pclFldRTYP);
           ilRC = CEDAArrayGetRowPointer(&rgFldArray.rrArrayHandle,
                                         rgFldArray.crArrayName,
                                         llRowNumFLD,(void *)&pclRowFLD);
        }
        
        if ((ilGTD1 == 1) && (strcmp(pclFldRNAM,pclAftGTD1) == 0) && (strcmp(pclFldRTYP,pcgRtypGATE) == 0)) 
        {
           dbg(DEBUG,"%05d: Now Update GTD1",__LINE__);
           if (strcmp(pcgUseFLZTAB,"YES") == 0)
           {
              sprintf(pclSqlBuf,"SELECT URNO,LNAM FROM FLZTAB WHERE RURN = %s AND UTYP IN ('G','B','A') AND LSEQ = '1'",
                      FIELDVAL(rgAftArray,pcpCurRowAFT,igAftURNO));
              slFunction= START;
              slCursor = 0;
              ilRCdb = sql_if(slFunction,&slCursor,pclSqlBuf,pclData);
              close_my_cursor(&slCursor);
              if (ilRCdb == DB_SUCCESS) 
              {
                 BuildItemBuffer(pclData,"",2,",");
                 get_real_item(pclFlzUrno,pclData,1);
                 TrimRight(pclFlzUrno);
                 get_real_item(pclFlzLnam,pclData,2);
                 TrimRight(pclFlzLnam);
                 if (strncmp(pclFlzLnam,pclAftGTD1,strlen(pclFlzLnam)) != 0)
                 {
                    sprintf(pclSelection,"WHERE URNO = %s",pclFlzUrno);
                    sprintf(pclSqlBuf,"UPDATE FLZTAB SET LNAM = '%s' %s",
                            pclAftGTD1,pclSelection);
                    slCursor = 0;
                    slFunction = START;
                    ilRCdb = sql_if(slFunction,&slCursor,pclSqlBuf,pclData);
                    commit_work();
                    close_my_cursor(&slCursor);
                 }
              }
           }
           ilGTD1 = 0;
           ilDelInd = 0;
           ilRC = PutFieldFLD("TIFF",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftTIFD),&llRowNumFLD);
           ilRC = PutFieldFLD("OBLF",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftOFBL),&llRowNumFLD);
           ilRC = PutFieldFLD("STOF",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftSTOD),&llRowNumFLD);
           if (igSelectCounterLogo == TRUE)
           {
              ilRC = PutFieldFLD("FLNO",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftFLNO),
                                 &llRowNumFLD);
              ilRC = PutFieldFLD("JFNO",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftJFNO),
                                 &llRowNumFLD);
              ilRC = PutFieldFLD("JCNT",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftJCNT),
                                 &llRowNumFLD);
           }
           TimeToStr(pclTimeBuf,time(NULL));
           ilRC = PutFieldFLD("LSTU",pclTimeBuf,&llRowNumFLD);
           ilRC = PutFieldFLD("USEU","FLDHDL",&llRowNumFLD);
           if (igUpRe == 1) PutFieldFLD("CREC",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),&llRowNumFLD); 
           /***************** Now get Open and Closing-Time *********************************/
           if (strcmp(pcgDispGate,"MANUAL") == 0)
           {
              strcpy(pclOpen,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGD1B));
              TrimRight(pclOpen);
              strcpy(pclClose,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGD1E));
              TrimRight(pclClose);
              ilRC = PutFieldFLD("TIFF",pclOpen,&llRowNumFLD);
           }
           else
           {
              strcpy(pclOpen,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftSTOD));
              TrimRight(pclOpen);
              AddSecondsToCEDATime12(pclOpen,timeoffsets.lgTipc07,1);
              if (strcmp(pcgDispGate,"MINGD1B") == 0)
              {
                 strcpy(pclTmpBuf,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGD1B));
                 TrimRight(pclTmpBuf);
                 if (strcmp(pclTmpBuf,pclOpen) < 0)
                    strcpy(pclOpen,pclTmpBuf);
                 strcpy(pclTmpBuf,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGD1X));
                 TrimRight(pclTmpBuf);
                 if (*pclTmpBuf != ' ' && strcmp(pclTmpBuf,pclOpen) < 0)
                    strcpy(pclOpen,pclTmpBuf);
              }
           }
           strcpy(pclBUF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftAIRB));
           TrimRight(pclBUF);
           if (strcmp(pclBUF," ") != 0)
           {
              strcpy(pclClose,pclBUF);
              ilDelInd = 1;
           }
           else
           {
              strcpy(pclBUF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftOFBL));
              TrimRight(pclBUF);
              if (strcmp(pclBUF," ") != 0)
              {
                 strcpy(pclClose,pclBUF);
                 ilDelInd = 1;
              }
              else
              {
                 if (strcmp(pcgDispGate,"MANUAL") != 0)
                 {
                    strcpy(pclBUF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGD1Y));
                    TrimRight(pclBUF);
                    if (strcmp(pclBUF," ") != 0)
                    {
                       if (strstr(pcgIntFLTI,pclAftFLTI) != NULL || strstr(pcgMixedFLTI,pclAftFLTI) != NULL)
                       {
                          strcpy(pclClose,pclBUF);
                          AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc11,1);
                       }
                       else
                       {
                          strcpy(pclClose,pclBUF);
                          AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc12,1);
                       }
                    }
                    else
                    {
                       strcpy(pclBUF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftTIFD));
                       TrimRight(pclBUF);
                       strcpy(pclClose,pclBUF);
                       llOffset = timeoffsets.lgTipc02;
                       llOffset = (-1)*llOffset;
                       dbg(DEBUG,"%05d: pclClose: <%s>, llOffset: <%d>",__LINE__,pclClose, llOffset);
                       AddSecondsToCEDATime12(pclClose,llOffset,1);
                    }
                 }
              }
           }
           if (ValidTime(pclOpen,pclClose," "," ") != RC_SUCCESS || ilDelInd == 1)
           {
              dbg(DEBUG,"%05d: Time not Valid, fldrow will be deleted",__LINE__);
              ilRC = CEDAArrayPutField(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,NULL,"DSEQ",llRowNumFLD,"-1");
           }
           SetDSEQ(pclFldRNAM,pclFldRTYP);
           ilRC = CEDAArrayGetRowPointer(&rgFldArray.rrArrayHandle,
                                         rgFldArray.crArrayName,
                                         llRowNumFLD,(void *)&pclRowFLD);
        }
        
        if ((ilGTD2 == 1) && (strcmp(pclFldRNAM,pclAftGTD2) == 0) && (strcmp(pclFldRTYP,pcgRtypGATE) == 0)) 
        {
           dbg(DEBUG,"%05d: Now Update GTD2",__LINE__);
           if (strcmp(pcgUseFLZTAB,"YES") == 0)
           {
              sprintf(pclSqlBuf,"SELECT URNO,LNAM FROM FLZTAB WHERE RURN = %s AND UTYP IN ('G','B','A') AND LSEQ = '2'",
                      FIELDVAL(rgAftArray,pcpCurRowAFT,igAftURNO));
              slFunction= START;
              slCursor = 0;
              ilRCdb = sql_if(slFunction,&slCursor,pclSqlBuf,pclData);
              close_my_cursor(&slCursor);
              if (ilRCdb == DB_SUCCESS) 
              {
                 BuildItemBuffer(pclData,"",2,",");
                 get_real_item(pclFlzUrno,pclData,1);
                 TrimRight(pclFlzUrno);
                 get_real_item(pclFlzLnam,pclData,2);
                 TrimRight(pclFlzLnam);
                 if (strncmp(pclFlzLnam,pclAftGTD2,strlen(pclFlzLnam)) != 0)
                 {
                    sprintf(pclSelection,"WHERE URNO = %s",pclFlzUrno);
                    sprintf(pclSqlBuf,"UPDATE FLZTAB SET LNAM = '%s' %s",
                            pclAftGTD2,pclSelection);
                    slCursor = 0;
                    slFunction = START;
                    ilRCdb = sql_if(slFunction,&slCursor,pclSqlBuf,pclData);
                    commit_work();
                    close_my_cursor(&slCursor);
                 }
              }
           }
           ilGTD2 = 0;
           ilDelInd = 0;
           ilRC = PutFieldFLD("TIFF",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftTIFD),&llRowNumFLD);
           ilRC = PutFieldFLD("OBLF",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftOFBL),&llRowNumFLD);
           ilRC = PutFieldFLD("STOF",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftSTOD),&llRowNumFLD);
           if (igSelectCounterLogo == TRUE)
           {
              ilRC = PutFieldFLD("FLNO",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftFLNO),
                                 &llRowNumFLD);
              ilRC = PutFieldFLD("JFNO",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftJFNO),
                                 &llRowNumFLD);
              ilRC = PutFieldFLD("JCNT",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftJCNT),
                                 &llRowNumFLD);
           }
           TimeToStr(pclTimeBuf,time(NULL));
           ilRC = PutFieldFLD("LSTU",pclTimeBuf,&llRowNumFLD);
           ilRC = PutFieldFLD("USEU","FLDHDL",&llRowNumFLD);
           if (igUpRe == 1)
           {
              if (strcmp(pcgUseGateStatusFields,"YES") == 0)
              {
                 strcpy(pclGate2Remark,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftRGD2));
                 TrimRight(pclGate2Remark);
                 if (*pclGate2Remark == '0')
                    *pclGate2Remark = ' ';
                 if (strstr(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),pcgDeparted) != NULL ||
                     strstr(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),pcgAirborne) != NULL)
                    strcpy(pclGate2Remark," ");
                 if (*pclGate2Remark == ' ' && strstr(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),pcgDelayedGateOpen) != NULL)
                    strcpy(pclGate2Remark,pcgDelayed);
                 if (strcmp(pclGate2Remark,pcgGateOpened) == 0 &&
                     (strstr(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),pcgDelayedGateOpen) != NULL ||
                      strstr(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),pcgDelayed) != NULL))
                    strcpy(pclGate2Remark,pcgDelayedGateOpen);
                 if (*pclGate2Remark == ' ' &&
                     (strstr(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),pcgGateOpened) != NULL ||
                      strstr(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),pcgBoarding) != NULL ||
                      strstr(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),pcgFinalcall) != NULL ||
                      strstr(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),pcgGateclosed) != NULL))
                 {
                    strcpy(pclGate2Remark,"XXX");
                    if (strcmp(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftETOD)," ") != 0)
                    {
                       strcpy(pclTmpTime,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftSTOD));
                       TrimRight(pclTmpTime);
                       AddSecondsToCEDATime12(pclTmpTime,igDelayTimeDiffDep,1);
                       if (strcmp(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftETOD),pclTmpTime) > 0) /*ETOA>STOA ?*/
                       {
                          strcpy(pclGate2Remark,pcgDelayed);
                       }
                    }
                 }
                 if (*pclGate2Remark != ' ')
                 {
                    if (strcmp(pclGate2Remark,"XXX") == 0)
                       PutFieldFLD("CREC"," ",&llRowNumFLD); 
                    else
                       PutFieldFLD("CREC",pclGate2Remark,&llRowNumFLD); 
                 }
                 else
                    PutFieldFLD("CREC",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),&llRowNumFLD); 
              }
              else
                 PutFieldFLD("CREC",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),&llRowNumFLD); 
           }
           /***************** Now get Open and Closing-Time *********************************/
           if (strcmp(pcgDispGate,"MANUAL") == 0)
           {
              strcpy(pclOpen,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGD2B));
              TrimRight(pclOpen);
              strcpy(pclClose,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGD2E));
              TrimRight(pclClose);
              ilRC = PutFieldFLD("TIFF",pclOpen,&llRowNumFLD);
           }
           else
           {
              strcpy(pclOpen,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftSTOD));
              TrimRight(pclOpen);
              AddSecondsToCEDATime12(pclOpen,timeoffsets.lgTipc07,1);
              if (strcmp(pcgDispGate,"MINGD1B") == 0)
              {
                 strcpy(pclTmpBuf,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGD2B));
                 TrimRight(pclTmpBuf);
                 if (strcmp(pclTmpBuf,pclOpen) < 0)
                    strcpy(pclOpen,pclTmpBuf);
                 strcpy(pclTmpBuf,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGD2X));
                 TrimRight(pclTmpBuf);
                 if (*pclTmpBuf != ' ' && strcmp(pclTmpBuf,pclOpen) < 0)
                    strcpy(pclOpen,pclTmpBuf);
              }
           }
           strcpy(pclBUF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftAIRB));
           TrimRight(pclBUF);
           if (strcmp(pclBUF," ") != 0)
           {
              strcpy(pclClose,pclBUF);
              ilDelInd = 1;
           }
           else
           {
              strcpy(pclBUF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftOFBL));
              TrimRight(pclBUF);
              if (strcmp(pclBUF," ") != 0)
              {
                 strcpy(pclClose,pclBUF);
                 ilDelInd = 1;
              }
              else
              {
                 if (strcmp(pcgDispGate,"MANUAL") != 0)
                 {
                    strcpy(pclBUF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGD2Y));
                    TrimRight(pclBUF);
                    if (strcmp(pclBUF," ") != 0)
                    {
                       if (strstr(pcgIntFLTI,pclAftFLTI) != NULL)
                       {
                          strcpy(pclClose,pclBUF);
                          AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc11,1);
                       }
                       else
                       {
                          strcpy(pclClose,pclBUF);
                          AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc12,1);
                       }
                    }
                    else
                    {
                       strcpy(pclBUF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftTIFD));
                       TrimRight(pclBUF);
                       strcpy(pclClose,pclBUF);
                       llOffset = timeoffsets.lgTipc02;
                       llOffset = (-1)*llOffset;
                       dbg(DEBUG,"%05d: pclClose: <%s>, llOffset: <%d>",__LINE__,pclClose, llOffset);
                       AddSecondsToCEDATime12(pclClose,llOffset,1);
                    }
                 }
              }
           }
           if (ValidTime(pclOpen,pclClose," "," ") != RC_SUCCESS || ilDelInd == 1)
           {
              dbg(DEBUG,"%05d: Time not Valid, fldrow will be deleted",__LINE__);
              ilRC = CEDAArrayPutField(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,NULL,"DSEQ",llRowNumFLD,"-1");
           }
           SetDSEQ(pclFldRNAM,pclFldRTYP);
           ilRC = CEDAArrayGetRowPointer(&rgFldArray.rrArrayHandle,
                                         rgFldArray.crArrayName,
                                         llRowNumFLD,(void *)&pclRowFLD);
        }
        
        if ((ilWRO1 == 1) && (strcmp(pclFldRNAM,pclAftWRO1) == 0) && (strcmp(pclFldRTYP,pcgRtypWRO) == 0)) 
        {
           dbg(DEBUG,"%05d: Now Update WRO1",__LINE__);
           ilWRO1 = 0;
           ilRC = PutFieldFLD("SOTI",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftW1BS),&llRowNumFLD);
           ilRC = PutFieldFLD("SCTI",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftW1ES),&llRowNumFLD);
           ilRC = PutFieldFLD("DOTI",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftW1BA),&llRowNumFLD);
           ilRC = PutFieldFLD("DCTI",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftW1EA),&llRowNumFLD);
           ilRC = PutFieldFLD("TIFF",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftTIFD),&llRowNumFLD);
           ilRC = PutFieldFLD("OBLF",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftOFBL),&llRowNumFLD);
           ilRC = PutFieldFLD("STOF",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftSTOD),&llRowNumFLD);
           if (igSelectCounterLogo == TRUE)
           {
              ilRC = PutFieldFLD("FLNO",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftFLNO),
                                 &llRowNumFLD);
              ilRC = PutFieldFLD("JFNO",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftJFNO),
                                 &llRowNumFLD);
              ilRC = PutFieldFLD("JCNT",FIELDVAL(rgAftArray,pcpCurRowAFT,igAftJCNT),
                                 &llRowNumFLD);
           }
           TimeToStr(pclTimeBuf,time(NULL));
           ilRC = PutFieldFLD("LSTU",pclTimeBuf,&llRowNumFLD);
           ilRC = PutFieldFLD("USEU","FLDHDL",&llRowNumFLD);
           ilRC = CEDAArrayGetRowPointer(&rgFldArray.rrArrayHandle,
                                         rgFldArray.crArrayName,
                                         llRowNumFLD,(void *)&pclRowFLD);
           strcpy(pclFldSOTI,FIELDVAL(rgFldArray,pclRowFLD,igFldSOTI));
           TrimRight(pclFldSOTI);
           strcpy(pclFldSCTI,FIELDVAL(rgFldArray,pclRowFLD,igFldSCTI));
           TrimRight(pclFldSCTI);
           strcpy(pclFldDOTI,FIELDVAL(rgFldArray,pclRowFLD,igFldDOTI));
           TrimRight(pclFldDOTI);
           strcpy(pclFldDCTI,FIELDVAL(rgFldArray,pclRowFLD,igFldDCTI));
           TrimRight(pclFldDCTI);
/*
           if (ValidTime(pclFldSOTI,pclFldSCTI,pclFldDOTI,pclFldDCTI) != RC_SUCCESS)
           {
              dbg(DEBUG,"%05d: Time not Valid, fldrow will be deleted",__LINE__);
              ilRC = CEDAArrayPutField(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,NULL,"DSEQ",llRowNumFLD,"-1");
           }
*/
           if (strcmp(pclAftREMP,pcgBoarding) != 0 &&
               strcmp(pclAftREMP,pcgFinalcall) != 0)
           {
              dbg(DEBUG,"%05d: Remark not Valid, fldrow will be deleted",__LINE__);
              ilRC = CEDAArrayPutField(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,NULL,"DSEQ",llRowNumFLD,"-1");
           }
           SetDSEQ(pclFldRNAM,pclFldRTYP);
           ilRC = CEDAArrayGetRowPointer(&rgFldArray.rrArrayHandle,
                                         rgFldArray.crArrayName,
                                         llRowNumFLD,(void *)&pclRowFLD);
        }
     }
     llRowNumFLD = ARR_NEXT;
  } /*end of while*/

  /* Hier neuen Datensatz anlegen */
  ilRC = InitFldStruct();
  if (ilBLT1 == 1)
  {
     dbg(DEBUG,"%05d:ilBLT1 == 1",__LINE__); 
     dbg(DEBUG,"%05d: pcgDispBelt = <%s>",__LINE__,pcgDispBelt); 
     /************************get open and close time for Belt1****************************************/
     if (strcmp(pcgDispBelt,"MANUAL") == 0)
     {
        strcpy(pclOpen,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftB1BA));
        TrimRight(pclOpen);
        strcpy(pclClose,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftB1EA));
        TrimRight(pclClose);
        if (pclOpen[0] != cBLANK && pclClose[0] == cBLANK)
        {
           strcpy(pclClose,"202031122359");
        }
     }
     else
     {
        if (strcmp(pcgDispBelt,"ONBL") == 0)
        {
           strcpy(pclOpen1,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftONBL));
           TimeToStr(pclOpen,time(NULL)); /* take Actual Time for pclOpen instead of ONBL for better testing */
           TrimRight(pclOpen);
        }
        else
        {
           if (strcmp(pcgDispBelt,"LAND") == 0)
           {
              strcpy(pclOpen1,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftLAND));
              TimeToStr(pclOpen,time(NULL)); /* take Actual Time for pclOpen instead of LAND for better testing */
              TrimRight(pclOpen);
           }
        }  
        strcpy(pclLastBag,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftB1EA));
        TrimRight(pclLastBag);
        dbg(DEBUG,"%05d: pclOpen: <%s>, pclLastBag: <%s>",__LINE__,pclOpen, pclLastBag);
        if (strcmp(pclLastBag," ") != 0)
        {
           strcpy(pclClose,pclLastBag);
           if (strstr(pcgIntFLTI,pclAftFLTI) != NULL || strstr(pcgMixedFLTI,pclAftFLTI) != NULL)
           {
              AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc10,1);
           }
           else
           {
              AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc14,1);
           }
        }
        else
        {
           strcpy(pclClose,pclOpen1);
           if (strstr(pcgIntFLTI,pclAftFLTI) != NULL || strstr(pcgMixedFLTI,pclAftFLTI) != NULL)
           {
              AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc05,1);
           }
           else
           {
              AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc13,1);
           }
        }
     }
     if (ValidTime(pclOpen,pclClose," "," ") == RC_SUCCESS)
     {
        ilBLT1 = 0;
        dbg(DEBUG,"%05d: Neuer Datensatz fuer BLT1",__LINE__);
        ilRC = GetNextValues(rgFldFlds.pclFldURNO,1);
        if (ilRC != RC_SUCCESS)
        {
           dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
        }
        strcpy(rgFldFlds.pclFldRNAM,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftBLT1));
        strcpy(rgFldFlds.pclFldRTYP,pcgRtypBELT);
        strcpy(rgFldFlds.pclFldAURN,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftURNO));
        strcpy(rgFldFlds.pclFldTIFF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftTIFA));
        strcpy(rgFldFlds.pclFldOBLF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftONBL));
        strcpy(rgFldFlds.pclFldSTOF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftSTOA));
        TimeToStr(rgFldFlds.pclFldLSTU,time(NULL)); 
        TimeToStr(rgFldFlds.pclFldCDAT,time(NULL));
        strcpy(rgFldFlds.pclFldUSEU,"FLDHDL");
        strcpy(rgFldFlds.pclFldUSEC,"FLDHDL");
        strcpy(rgFldFlds.pclFldHOPO,pcgHomeAp);
        if (igSelectCounterLogo == TRUE)
        {
           strcpy(rgFldFlds.pclFldFLNO,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftFLNO));
           strcpy(rgFldFlds.pclFldJFNO,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftJFNO));
           strcpy(rgFldFlds.pclFldJCNT,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftJCNT));
        }
        ilRC = StructAddRowPartFLD(&llRowNumFLD);
        SetDSEQ(rgFldFlds.pclFldRNAM,rgFldFlds.pclFldRTYP);
     }
     ilRC = InitFldStruct();
  }

  if (ilBLT2 == 1)
  {
     dbg(DEBUG,"%05d:ilBLT2 == 1",__LINE__); 
     /************************get open and close time for Belt2****************************************/
     if (strcmp(pcgDispBelt,"MANUAL") == 0)
     {
        strcpy(pclOpen,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftB2BA));
        TrimRight(pclOpen);
        strcpy(pclClose,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftB2EA));
        TrimRight(pclClose);
        if (pclOpen[0] != cBLANK && pclClose[0] == cBLANK)
        {
           strcpy(pclClose,"202031122359");
        }
     }
     else
     {
        if (strcmp(pcgDispBelt,"ONBL") == 0)
        {
           strcpy(pclOpen1,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftONBL));
           TimeToStr(pclOpen,time(NULL)); /* take Actual Time for pclOpen instead of ONBL for better testing */
           TrimRight(pclOpen);
        }
        else
        {
           if (strcmp(pcgDispBelt,"LAND") == 0)
           {
              strcpy(pclOpen1,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftLAND));
              TimeToStr(pclOpen,time(NULL)); /* take Actual Time for pclOpen instead of LAND for better testing */
              TrimRight(pclOpen);
           }
        }
        strcpy(pclLastBag,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftB2EA));
        TrimRight(pclLastBag);
        if (strcmp(pclLastBag," ") != 0)
        {
           strcpy(pclClose,pclLastBag);
           if (strstr(pcgIntFLTI,pclAftFLTI) != NULL)
           {
              AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc10,1);
           }
           else
           {
              AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc14,1);
           }
        }
        else
        {
           strcpy(pclClose,pclOpen1);
           if (strstr(pcgIntFLTI,pclAftFLTI) != NULL)
           {
              AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc05,1);
           }
           else
           {
              AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc13,1);
           }
        }
     }
     if (ValidTime(pclOpen,pclClose," "," ") == RC_SUCCESS)
     {
        ilBLT2 = 0;
        dbg(DEBUG,"%05d: Neuer Datensatz fuer BLT2",__LINE__);
        ilRC = GetNextValues(rgFldFlds.pclFldURNO,1);
        if (ilRC != RC_SUCCESS)
        {
           dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
        }
        strcpy(rgFldFlds.pclFldRNAM,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftBLT2));
        strcpy(rgFldFlds.pclFldRTYP,pcgRtypBELT);
        strcpy(rgFldFlds.pclFldAURN,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftURNO));
        strcpy(rgFldFlds.pclFldTIFF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftTIFA));
        strcpy(rgFldFlds.pclFldOBLF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftONBL));
        strcpy(rgFldFlds.pclFldSTOF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftSTOA));
        TimeToStr(rgFldFlds.pclFldLSTU,time(NULL)); 
        TimeToStr(rgFldFlds.pclFldCDAT,time(NULL));
        strcpy(rgFldFlds.pclFldUSEU,"FLDHDL");
        strcpy(rgFldFlds.pclFldUSEC,"FLDHDL");
        strcpy(rgFldFlds.pclFldHOPO,pcgHomeAp);
        if (igSelectCounterLogo == TRUE)
        {
           strcpy(rgFldFlds.pclFldFLNO,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftFLNO));
           strcpy(rgFldFlds.pclFldJFNO,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftJFNO));
           strcpy(rgFldFlds.pclFldJCNT,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftJCNT));
        }
        ilRC = StructAddRowPartFLD(&llRowNumFLD);
        ilRC = SetDSEQ(rgFldFlds.pclFldRNAM,rgFldFlds.pclFldRTYP);
     }
     ilRC = InitFldStruct();
  }    

  if (ilGTD1 == 1)
  {
     dbg(DEBUG,"%05d:ilGTD1 == 1",__LINE__); 
     if (strcmp(pcgUseFLZTAB,"YES") == 0)
     {
        sprintf(pclSqlBuf,"SELECT URNO,LNAM FROM FLZTAB WHERE RURN = %s AND UTYP IN ('G','B','A') AND LSEQ = '1'",
                FIELDVAL(rgAftArray,pcpCurRowAFT,igAftURNO));
        slFunction= START;
        slCursor = 0;
        ilRCdb = sql_if(slFunction,&slCursor,pclSqlBuf,pclData);
        close_my_cursor(&slCursor);
        if (ilRCdb == DB_SUCCESS) 
        {
           BuildItemBuffer(pclData,"",2,",");
           get_real_item(pclFlzUrno,pclData,1);
           TrimRight(pclFlzUrno);
           get_real_item(pclFlzLnam,pclData,2);
           TrimRight(pclFlzLnam);
           if (strncmp(pclFlzLnam,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGTD1),strlen(pclFlzLnam)) != 0)
           {
              sprintf(pclSelection,"WHERE URNO = %s",pclFlzUrno);
              sprintf(pclSqlBuf,"UPDATE FLZTAB SET LNAM = '%s' %s",
                      FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGTD1),pclSelection);
              slCursor = 0;
              slFunction = START;
              ilRCdb = sql_if(slFunction,&slCursor,pclSqlBuf,pclData);
              commit_work();
              close_my_cursor(&slCursor);
           }
        }
     }
     /***************** Now get Open and Closing-Time *********************************/
     ilDelInd = 0;
     if (strcmp(pcgDispGate,"MANUAL") == 0)
     {
        strcpy(pclOpen,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGD1B));
        TrimRight(pclOpen);
        strcpy(pclClose,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGD1E));
        TrimRight(pclClose);
     }
     else
     {
        strcpy(pclOpen,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftSTOD));
        TrimRight(pclOpen);
        AddSecondsToCEDATime12(pclOpen,timeoffsets.lgTipc07,1);
        if (strcmp(pcgDispGate,"MINGD1B") == 0)
        {
           strcpy(pclTmpBuf,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGD1B));
           TrimRight(pclTmpBuf);
           if (strcmp(pclTmpBuf,pclOpen) < 0)
              strcpy(pclOpen,pclTmpBuf);
           strcpy(pclTmpBuf,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGD1X));
           TrimRight(pclTmpBuf);
           if (*pclTmpBuf != ' ' && strcmp(pclTmpBuf,pclOpen) < 0)
              strcpy(pclOpen,pclTmpBuf);
        }
     }
     strcpy(pclBUF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftAIRB));
     TrimRight(pclBUF);
     if (strcmp(pclBUF," ") != 0)
     {
        strcpy(pclClose,pclBUF);
        ilDelInd = 1;
     }
     else
     {
        strcpy(pclBUF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftOFBL));
        TrimRight(pclBUF);
        if (strcmp(pclBUF," ") != 0)
        {
           strcpy(pclClose,pclBUF);
           ilDelInd = 1;
        }
        else
        {
           if (strcmp(pcgDispGate,"MANUAL") != 0)
           {
              strcpy(pclBUF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGD1Y));
              TrimRight(pclBUF);
              if (strcmp(pclBUF," ") != 0)
              {
                 if (strstr(pcgIntFLTI,pclAftFLTI) != NULL || strstr(pcgMixedFLTI,pclAftFLTI) != NULL)
                 {
                    strcpy(pclClose,pclBUF);
                    AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc11,1);
                 }
                 else
                 {
                    strcpy(pclClose,pclBUF);
                    AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc12,1);
                 }
              }
              else
              {
                 strcpy(pclBUF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftTIFD));
                 TrimRight(pclBUF);
                 strcpy(pclClose,pclBUF);
                 llOffset = timeoffsets.lgTipc02;
                 llOffset = (-1)*llOffset;
                 dbg(DEBUG,"%05d: pclClose: <%s>, llOffset: <%d>",__LINE__,pclClose, llOffset);
                 AddSecondsToCEDATime12(pclClose,llOffset,1);
              }
           }
        }
     }
     if (ValidTime(pclOpen,pclClose," "," ") == RC_SUCCESS && ilDelInd == 0)
     {
        ilGTD1 = 0;
        dbg(DEBUG,"%05d: Neuer Datensatz fuer GTD1",__LINE__);
        ilRC = GetNextValues(rgFldFlds.pclFldURNO,1);
        if (ilRC != RC_SUCCESS)
        {
           dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
        }
        strcpy(rgFldFlds.pclFldRNAM,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGTD1));
        strcpy(rgFldFlds.pclFldRTYP,pcgRtypGATE);
        strcpy(rgFldFlds.pclFldAURN,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftURNO));
        if (strcmp(pcgDispGate,"MANUAL") != 0)
        {
           strcpy(rgFldFlds.pclFldTIFF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftTIFD));
        }
        else
        {
           strcpy(rgFldFlds.pclFldTIFF,pclOpen);
        }
        strcpy(rgFldFlds.pclFldOBLF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftOFBL));
        strcpy(rgFldFlds.pclFldSTOF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftSTOD));
        if (igUpRe == 1)  strcpy(rgFldFlds.pclFldCREC,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP)); 
        TimeToStr(rgFldFlds.pclFldLSTU,time(NULL)); 
        TimeToStr(rgFldFlds.pclFldCDAT,time(NULL));
        strcpy(rgFldFlds.pclFldUSEU,"FLDHDL");
        strcpy(rgFldFlds.pclFldUSEC,"FLDHDL");
        strcpy(rgFldFlds.pclFldHOPO,pcgHomeAp);
        if (igSelectCounterLogo == TRUE)
        {
           strcpy(rgFldFlds.pclFldFLNO,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftFLNO));
           strcpy(rgFldFlds.pclFldJFNO,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftJFNO));
           strcpy(rgFldFlds.pclFldJCNT,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftJCNT));
        }
        ilRC = StructAddRowPartFLD(&llRowNumFLD);
        SetDSEQ(rgFldFlds.pclFldRNAM,rgFldFlds.pclFldRTYP);
     }
     ilRC = InitFldStruct();
  }    

  if (ilGTD2 == 1)
  {    
     dbg(DEBUG,"%05d:ilGTD2 == 1",__LINE__); 
     if (strcmp(pcgUseFLZTAB,"YES") == 0)
     {
        sprintf(pclSqlBuf,"SELECT URNO,LNAM FROM FLZTAB WHERE RURN = %s AND UTYP IN ('G','B','A') AND LSEQ = '2'",
                FIELDVAL(rgAftArray,pcpCurRowAFT,igAftURNO));
        slFunction= START;
        slCursor = 0;
        ilRCdb = sql_if(slFunction,&slCursor,pclSqlBuf,pclData);
        close_my_cursor(&slCursor);
        if (ilRCdb == DB_SUCCESS) 
        {
           BuildItemBuffer(pclData,"",2,",");
           get_real_item(pclFlzUrno,pclData,1);
           TrimRight(pclFlzUrno);
           get_real_item(pclFlzLnam,pclData,2);
           TrimRight(pclFlzLnam);
           if (strncmp(pclFlzLnam,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGTD2),strlen(pclFlzLnam)) != 0)
           {
              sprintf(pclSelection,"WHERE URNO = %s",pclFlzUrno);
              sprintf(pclSqlBuf,"UPDATE FLZTAB SET LNAM = '%s' %s",
                      FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGTD2),pclSelection);
              slCursor = 0;
              slFunction = START;
              ilRCdb = sql_if(slFunction,&slCursor,pclSqlBuf,pclData);
              commit_work();
              close_my_cursor(&slCursor);
           }
        }
     }
     /***************** Now get Open and Closing-Time *********************************/
     ilDelInd = 0;
     if (strcmp(pcgDispGate,"MANUAL") == 0)
     {
        strcpy(pclOpen,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGD2B));
        TrimRight(pclOpen);
        strcpy(pclClose,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGD2E));
        TrimRight(pclClose);
     }
     else
     {
        strcpy(pclOpen,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftSTOD));
        TrimRight(pclOpen);
        AddSecondsToCEDATime12(pclOpen,timeoffsets.lgTipc07,1);
        if (strcmp(pcgDispGate,"MINGD1B") == 0)
        {
           strcpy(pclTmpBuf,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGD2B));
           TrimRight(pclTmpBuf);
           if (strcmp(pclTmpBuf,pclOpen) < 0)
              strcpy(pclOpen,pclTmpBuf);
           strcpy(pclTmpBuf,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGD2X));
           TrimRight(pclTmpBuf);
           if (*pclTmpBuf != ' ' && strcmp(pclTmpBuf,pclOpen) < 0)
              strcpy(pclOpen,pclTmpBuf);
        }
     }
     strcpy(pclBUF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftAIRB));
     TrimRight(pclBUF);
     if (strcmp(pclBUF," ") != 0)
     {
        strcpy(pclClose,pclBUF);
        ilDelInd = 1;
     }
     else
     {
        strcpy(pclBUF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftOFBL));
        TrimRight(pclBUF);
        if (strcmp(pclBUF," ") != 0)
        {
           strcpy(pclClose,pclBUF);
           ilDelInd = 1;
        }
        else
        {
           if (strcmp(pcgDispGate,"MANUAL") != 0)
           {
              strcpy(pclBUF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGD2Y));
              TrimRight(pclBUF);
              if (strcmp(pclBUF," ") != 0)
              {
                 if (strstr(pcgIntFLTI,pclAftFLTI) != NULL)
                 {
                    strcpy(pclClose,pclBUF);
                    AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc11,1);
                 }
                 else
                 {
                    strcpy(pclClose,pclBUF);
                    AddSecondsToCEDATime12(pclClose,timeoffsets.lgTipc12,1);
                 }
              }
              else
              {
                 strcpy(pclBUF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftTIFD));
                 TrimRight(pclBUF);
                 strcpy(pclClose,pclBUF);
                 llOffset = timeoffsets.lgTipc02;
                 llOffset = (-1)*llOffset;
                 dbg(DEBUG,"%05d: pclClose: <%s>, llOffset: <%d>",__LINE__,pclClose, llOffset);
                 AddSecondsToCEDATime12(pclClose,llOffset,1);
              }
           }
        }
     }
     if (ValidTime(pclOpen,pclClose," "," ") == RC_SUCCESS && ilDelInd == 0)
     {
        ilGTD2 = 0;
        dbg(DEBUG,"%05d: Neuer Datensatz fuer GTD2",__LINE__);
        ilRC = GetNextValues(rgFldFlds.pclFldURNO,1);
        if (ilRC != RC_SUCCESS)
        {
           dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
        }
        strcpy(rgFldFlds.pclFldRNAM,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGTD2));
        strcpy(rgFldFlds.pclFldRTYP,pcgRtypGATE);
        strcpy(rgFldFlds.pclFldAURN,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftURNO));
        if (strcmp(pcgDispGate,"MANUAL") != 0)
        {
           strcpy(rgFldFlds.pclFldTIFF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftTIFD));
        }
        else
        {
           strcpy(rgFldFlds.pclFldTIFF,pclOpen);
        }
        strcpy(rgFldFlds.pclFldOBLF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftOFBL));
        strcpy(rgFldFlds.pclFldSTOF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftSTOD));
        if (igUpRe == 1)
        {
           if (strcmp(pcgUseGateStatusFields,"YES") == 0)
           {
              strcpy(pclGate2Remark,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftRGD2));
              TrimRight(pclGate2Remark);
              if (*pclGate2Remark == '0')
                 *pclGate2Remark = ' ';
              if (strstr(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),pcgDeparted) != NULL ||
                  strstr(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),pcgAirborne) != NULL)
                 strcpy(pclGate2Remark," ");
              if (*pclGate2Remark == ' ' && strstr(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),pcgDelayedGateOpen) != NULL)
                 strcpy(pclGate2Remark,pcgDelayed);
              if (strcmp(pclGate2Remark,pcgGateOpened) == 0 &&
                  (strstr(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),pcgDelayedGateOpen) != NULL ||
                   strstr(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),pcgDelayed) != NULL))
                 strcpy(pclGate2Remark,pcgDelayedGateOpen);
              if (*pclGate2Remark == ' ' &&
                  (strstr(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),pcgGateOpened) != NULL ||
                   strstr(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),pcgBoarding) != NULL ||
                   strstr(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),pcgFinalcall) != NULL ||
                   strstr(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP),pcgGateclosed) != NULL))
              {
                 strcpy(pclGate2Remark,"XXX");
                 if (strcmp(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftETOD)," ") != 0)
                 {
                    strcpy(pclTmpTime,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftSTOD));
                    TrimRight(pclTmpTime);
                    AddSecondsToCEDATime12(pclTmpTime,igDelayTimeDiffDep,1);
                    if (strcmp(FIELDVAL(rgAftArray,pcpCurRowAFT,igAftETOD),pclTmpTime) > 0) /*ETOA>STOA ?*/
                    {
                       strcpy(pclGate2Remark,pcgDelayed);
                    }
                 }
              }
              if (*pclGate2Remark != ' ')
              {
                 if (strcmp(pclGate2Remark,"XXX") == 0)
                    strcpy(rgFldFlds.pclFldCREC," "); 
                 else
                    strcpy(rgFldFlds.pclFldCREC,pclGate2Remark); 
              }
              else
                 strcpy(rgFldFlds.pclFldCREC,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP)); 
           }
           else
              strcpy(rgFldFlds.pclFldCREC,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP)); 
        }
        TimeToStr(rgFldFlds.pclFldLSTU,time(NULL)); 
        TimeToStr(rgFldFlds.pclFldCDAT,time(NULL));
        strcpy(rgFldFlds.pclFldUSEU,"FLDHDL");
        strcpy(rgFldFlds.pclFldUSEC,"FLDHDL");
        strcpy(rgFldFlds.pclFldHOPO,pcgHomeAp);
        if (igSelectCounterLogo == TRUE)
        {
           strcpy(rgFldFlds.pclFldFLNO,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftFLNO));
           strcpy(rgFldFlds.pclFldJFNO,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftJFNO));
           strcpy(rgFldFlds.pclFldJCNT,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftJCNT));
        }
        ilRC = StructAddRowPartFLD(&llRowNumFLD);    
        SetDSEQ(rgFldFlds.pclFldRNAM,rgFldFlds.pclFldRTYP);
     }
     ilRC = InitFldStruct();
  }    

  if (ilWRO1 == 1)
  {    
     dbg(DEBUG,"%05d:ilWRO1 == 1",__LINE__); 
     strcpy(rgFldFlds.pclFldSOTI,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftW1BS));
     strcpy(rgFldFlds.pclFldSCTI,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftW1ES));
     strcpy(rgFldFlds.pclFldDOTI,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftW1BA));
     strcpy(rgFldFlds.pclFldDCTI,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftW1EA)); 
/*
     if (ValidTime(rgFldFlds.pclFldSOTI,rgFldFlds.pclFldSCTI,rgFldFlds.pclFldDOTI,rgFldFlds.pclFldDCTI) == RC_SUCCESS)
*/
     if (strcmp(pclAftREMP,pcgBoarding) == 0 ||
         strcmp(pclAftREMP,pcgFinalcall) == 0)
     {
        dbg(DEBUG,"%05d: Neuer Datensatz fuer WRO1",__LINE__);
        ilRC = GetNextValues(rgFldFlds.pclFldURNO,1);
        if (ilRC != RC_SUCCESS)
        {
           dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
        }
        strcpy(rgFldFlds.pclFldRNAM,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftWRO1));
        strcpy(rgFldFlds.pclFldRTYP,pcgRtypWRO);
        strcpy(rgFldFlds.pclFldAURN,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftURNO));
        strcpy(rgFldFlds.pclFldTIFF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftTIFD));
        strcpy(rgFldFlds.pclFldOBLF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftOFBL));
        strcpy(rgFldFlds.pclFldSTOF,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftSTOD));
        if (igUpRe == 1)  strcpy(rgFldFlds.pclFldCREC,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP)); 
        TimeToStr(rgFldFlds.pclFldLSTU,time(NULL)); 
        TimeToStr(rgFldFlds.pclFldCDAT,time(NULL));
        strcpy(rgFldFlds.pclFldUSEU,"FLDHDL");
        strcpy(rgFldFlds.pclFldUSEC,"FLDHDL");
        strcpy(rgFldFlds.pclFldHOPO,pcgHomeAp);
        if (igSelectCounterLogo == TRUE)
        {
           strcpy(rgFldFlds.pclFldFLNO,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftFLNO));
           strcpy(rgFldFlds.pclFldJFNO,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftJFNO));
           strcpy(rgFldFlds.pclFldJCNT,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftJCNT));
        }
        ilRC = StructAddRowPartFLD(&llRowNumFLD);
        SetDSEQ(rgFldFlds.pclFldRNAM,rgFldFlds.pclFldRTYP);
     }
     ilRC = InitFldStruct();
  }    

  ilRC = DeleteDSEQ();

  if (strcmp(pclAftREMP,pcgDeparted) == 0 || strcmp(pclAftREMP,pcgAirborne) == 0)
  {
     ilRC = DeleteAURN_FLD(pclAftURNO,"D",pcpCurRowAFT);
  }
  else
  {
     ilRC = DeleteAURN_FLD(pclAftURNO," ",pcpCurRowAFT);
  }

  dbg(DEBUG,"%05d: =========================================================================",__LINE__);
  dbg(DEBUG,"%05d: =========================== End of UpdateFLDTAB =========================",__LINE__);
  dbg(DEBUG,"%05d: =========================================================================",__LINE__);

  return(ilRC);
} /*end updatefldtab */




/******************************************************************************/
/* The ValidTime routine                                                    */
/******************************************************************************/
static int ValidTime(char *pclFldSOTI, char *pclFldSCTI, char *pclFldDOTI, char *pclFldDCTI)
{
  int ilRC = RC_SUCCESS;
  char pclNowTime[128] = "\0";
  char pclStartTime[128] = "\0";
  char pclEndTime[128] = "\0";
  char pclTimeBuf[128] = "\0";

  if (strlen(pclFldSOTI) > 0 && *pclFldSOTI != ' ')
     TrimRight(pclFldSOTI);
  if (strlen(pclFldSCTI) > 0 && *pclFldSCTI != ' ')
     TrimRight(pclFldSCTI);
  if (strlen(pclFldDOTI) > 0 && *pclFldDOTI != ' ')
     TrimRight(pclFldDOTI);
  if (strlen(pclFldDCTI) > 0 && *pclFldDCTI != ' ')
     TrimRight(pclFldDCTI);
  if (strlen(pclFldSOTI) == 12)
     strcat(pclFldSOTI,"00");
  if (strlen(pclFldSCTI) == 12)
     strcat(pclFldSCTI,"00");
  if (strlen(pclFldDOTI) == 12)
     strcat(pclFldDOTI,"00");
  if (strlen(pclFldDCTI) == 12)
     strcat(pclFldDCTI,"00");
  dbg(DEBUG,"%05d: ========================== ValidTime Start ================================",__LINE__);
  dbg(DEBUG,"%05d: pclFldSOTI: <%s>, pclFldSCTI: <%s>, pclFldDOTI: <%s>, pclFldDCTI: <%s>",__LINE__,pclFldSOTI, pclFldSCTI, pclFldDOTI, pclFldDCTI);
  TimeToStr(pclNowTime,time(NULL));

  if (((igSO == 0) && (igAO == 0)) || ((igSO == 1) && (igAO == 1)))  
  {
     ilRC = GetMinTime(pclFldSOTI,pclFldDOTI,pclStartTime);
  }
  else
  {
    if (igSO == 1)
    {
       strcpy(pclStartTime,pclFldSOTI);
    }
    else
    {
       if (strcmp(pclFldDOTI," ") == 0) 
       {
          strcpy(pclTimeBuf,pclFldSOTI);
          AddSecondsToCEDATime12(pclTimeBuf,(igOAS*60),1);
          if (strcmp(pclTimeBuf,pclNowTime) <= 0)
          {
             strcpy(pclStartTime,pclTimeBuf); 
          }
          else
          {
             strcpy(pclStartTime,pclFldDOTI);
          }
       }
       else
       {
          strcpy(pclStartTime,pclFldDOTI);
       } 
    }
  } 

  if (((igSC == 0) && (igAC == 0)) || ((igSC == 1) && (igAC == 1)))
  {
     ilRC = GetMaxTime(pclFldSCTI,pclFldDCTI,pclEndTime);
  }
  else
  {
     if (igSC == 1)
     {
        strcpy(pclEndTime,pclFldSCTI);
     }
     else
     {
        if ((strcmp(pclFldDCTI," ") == 0) || (strcmp(pclFldDCTI,"\0") == 0)) 
        {
           strcpy(pclTimeBuf,pclFldSCTI);
           AddSecondsToCEDATime12(pclTimeBuf,(igCAS*60),1);
           strcpy(pclEndTime,pclTimeBuf); 
        }
        else
        {
          strcpy(pclEndTime,pclFldDCTI);
        } 
     }
  }
  

  dbg(DEBUG,"%05d: ������ StartTime <%s>, Endtime <%s>, NowTime: <%s> �����������",__LINE__,pclStartTime,pclEndTime,pclNowTime);
  
  if ((strcmp(pclStartTime," ") != 0) && (strcmp(pclEndTime," ") != 0))
  {
     if ((strcmp(pclStartTime,pclNowTime) <= 0) && (strcmp(pclEndTime,pclNowTime) > 0))
     {
        ilRC = RC_SUCCESS;
     }
     else
     {
        ilRC = RC_FAIL;
     }
  }
  else
  {
     ilRC = RC_FAIL;
  }
  
  dbg(DEBUG,"%05d: ============ ValidTime End : ilRC = <%d>",__LINE__,ilRC);
  return (ilRC);
}




/******************************************************************************/
/* The GetMinTime routine                                                    */
/******************************************************************************/
static int GetMinTime(char *pcpTime1, char *pcpTime2, char *pcpMinTime)
{
  int ilRC = RC_SUCCESS;
  
  if (pcpTime1[0] != cBLANK && strlen(pcpTime1) > 0)
  {
     if (pcpTime2[0] != cBLANK && strlen(pcpTime2) > 0)
     {
        strcpy(pcpMinTime,pcpTime2);
	/*
        if (strcmp(pcpTime1,pcpTime2) <= 0)
        {
           strcpy(pcpMinTime,pcpTime1);
        }
        else
        {
           strcpy(pcpMinTime,pcpTime2);
        }
	*/
     }
     else
     {
        strcpy(pcpMinTime,pcpTime1);
     }
  }
  else
  {
     strcpy(pcpMinTime,pcpTime2);
  }
  return(ilRC);
}




/******************************************************************************/
/* The GetMaxTime routine                                                    */
/******************************************************************************/
static int GetMaxTime(char *pcpTime1, char *pcpTime2, char *pcpMaxTime)
{
  int ilRC = RC_SUCCESS;

  if (pcpTime1[0] != cBLANK && strlen(pcpTime1) > 0)
  {
     if (pcpTime2[0] != cBLANK && strlen(pcpTime2) > 0)
     {
        strcpy(pcpMaxTime,pcpTime2);
	/*
        if (strcmp(pcpTime1,pcpTime2) >= 0)
        {
           strcpy(pcpMaxTime,pcpTime1);
        }
        else
        {
           strcpy(pcpMaxTime,pcpTime2);
        }
	*/
     }
     else
     {
        strcpy(pcpMaxTime,pcpTime1);
     }
  }
  else
  {
     strcpy(pcpMaxTime,pcpTime2);
  }
  return(ilRC);
}


/******************************************************************************/
/* The TestCmd routine                                                    */
/******************************************************************************/
static int TestCmd()
{
  char pclTime1[16] = "20010507080000"; /*SOTI*/
  char pclTime2[16] = "20010507120000"; /*SCTI*/
  char pclTime3[16] = "20010507075000"; /*DOTI20010507081000*/
  char pclTime4[16] = " "; /*DCTI20010507115500*/
  char pclNowTime[16] = "\0";

  TimeToStr(pclNowTime,time(NULL));
  dbg(DEBUG,"��������������� Nowtime: <%s> ��������������",pclNowTime);
  if (ValidTime(pclTime1,pclTime2,pclTime3,pclTime4) == RC_SUCCESS)
    {
      dbg(DEBUG,"�������������� ValidTime TRUE �����������������");
    }
  else
    {
      dbg(DEBUG,"�������������� ValidTime FALSE ������������������");
    }

  return 0;
}




/******************************************************************************/
/* The PutFieldFLD routine                                                    */
/******************************************************************************/
static int PutFieldFLD (char *pcpField, char *pcpValue, long *plpRowNum)
{
  int ilRC = RC_SUCCESS;

  ilRC = CEDAArrayPutField(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,NULL,pcpField,*plpRowNum,pcpValue);

  return (ilRC);
}



/******************************************************************************/
/* The SetTimeFieldFLD routine                                                */
/******************************************************************************/
static int SetTimeFieldFLD()
{
  int ilRC = RC_SUCCESS;
  long llRowNumFLD = ARR_FIRST;
  long llRowNumFDD = ARR_FIRST;
  long llRowNumAFT = ARR_FIRST;
  char *pclRowFLD = NULL;
  char *pclRowFDD = NULL;
  char *pclRowAFT = NULL;
  char pclFldAURN[128] = "\0";
  char pclTIFF[32] = "\0";
  char pclFldRTYP[128] = "\0";
  dbg(DEBUG,"%05d:--------- this is SetTimeFieldFLD --------- ",__LINE__);

  while(CEDAArrayGetRowPointer(&rgFldArray.rrArrayHandle,
                   rgFldArray.crArrayName,
                   llRowNumFLD,(void *)&pclRowFLD) == RC_SUCCESS)
    {
      llRowNumFLD = ARR_CURRENT;

      if (CEDAArrayFindRowPointer(&rgAftArray.rrArrayHandle, &rgAftArray.crArrayName[0],
                  &rgAftArray.rrIdx01Handle, &rgAftArray.crIdx01Name[0],
                  FIELDVAL(rgFldArray,pclRowFLD,igFldAURN),&llRowNumAFT,
                  (void *) &pclRowAFT) == RC_SUCCESS)
    {
      dbg(DEBUG,"%05d: cedaarrayfindrowpointer ilRC: <%d>",__LINE__,ilRC);

      strcpy(pclFldRTYP,FIELDVAL(rgFldArray,pclRowFLD,igFldRTYP));
      dbg(DEBUG,"%05d:",__LINE__);

      if(strcmp(pclFldRTYP,pcgRtypBELT) == 0)
        {
          dbg(DEBUG,"%05d:",__LINE__);

          strcpy(pclTIFF,FIELDVAL(rgAftArray,pclRowAFT,igAftTIFA));
          dbg(DEBUG,"%05d:",__LINE__);

        }
      else
        {
          dbg(DEBUG,"%05d:",__LINE__);

          strcpy(pclTIFF,FIELDVAL(rgAftArray,pclRowAFT,igAftTIFD));
          dbg(DEBUG,"%05d:",__LINE__);

        }

      
      dbg(DEBUG,"%05d: SetTimeFieldFLD pclTIFF <%s>",__LINE__,pclTIFF);
      ilRC = CEDAArrayPutField(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,NULL,"TIFF",llRowNumFLD,pclTIFF);
      if (ilRC == RC_SUCCESS)
        {
          dbg(DEBUG,"%05d: putField Succeeded",__LINE__);
        }
      else
        {
          dbg(DEBUG,"%05d: put Field failed",__LINE__);
        }
    }
      llRowNumFLD = ARR_NEXT;
    }

  return (ilRC);
} /* end of SetTimeFieldFLD */




 
/******************************************************************************/
/* The SetDSEQ  routine                                                       */
/******************************************************************************/
static int SetDSEQ(char *pcpRNAM, char *pcpRTYP)
{
  int ilRC = RC_SUCCESS;
  int ilDSEQ = 1;
  char pclKey[64] ="\0";
  long llRowNumFLD = ARR_FIRST;
  char *pclRowFLD = NULL;
  char pclBuf[128] = "\0";
  char pclDSEQ[128] = "\0";
  char pclURNO[128] = "\0";
  char pclRNAM[128] = "\0";
  char pclRTYP[128] = "\0";
  char pclTIFF[128] = "\0";
  char pclOBLF[128] = "\0";
  char pclSTOF[128] = "\0";

  dbg(DEBUG,"======================= Begin of SetDSEQ =========================================");
  /*ilRC = SaveIndexInfo(&rgFldArray);*/
  TrimRight(pcpRNAM);
  TrimRight(pcpRTYP);
  sprintf(pclKey,"%s,%s",pcpRNAM,pcpRTYP);
  dbg(DEBUG,"%05d: pclKey: <%s>",__LINE__,pclKey);

  while(CEDAArrayFindRowPointer(&rgFldArray.rrArrayHandle, rgFldArray.crArrayName,
                                &rgFldArray.rrIdx03Handle, rgFldArray.crIdx03Name,
                                pclKey,&llRowNumFLD,
                                (void *) &pclRowFLD) == RC_SUCCESS)
  {
     dbg(DEBUG,"%05d: RowNumFLD: <%d>",__LINE__,llRowNumFLD);
     strcpy(pclDSEQ,FIELDVAL(rgFldArray,pclRowFLD,igFldDSEQ));
     TrimRight(pclDSEQ);
     strcpy(pclURNO,FIELDVAL(rgFldArray,pclRowFLD,igFldURNO));
     TrimRight(pclURNO);
     strcpy(pclRNAM,FIELDVAL(rgFldArray,pclRowFLD,igFldRNAM));
     TrimRight(pclRNAM);
     strcpy(pclRTYP,FIELDVAL(rgFldArray,pclRowFLD,igFldRTYP));
     TrimRight(pclRTYP);
     strcpy(pclTIFF,FIELDVAL(rgFldArray,pclRowFLD,igFldTIFF));
     TrimRight(pclTIFF);
     strcpy(pclOBLF,FIELDVAL(rgFldArray,pclRowFLD,igFldOBLF));
     TrimRight(pclOBLF);
     strcpy(pclSTOF,FIELDVAL(rgFldArray,pclRowFLD,igFldSTOF));
     TrimRight(pclSTOF);
     dbg(DEBUG,"%05d: pclURNO: <%s>, pclDSEQ: <%s>",__LINE__,pclURNO, pclDSEQ);
     dbg(DEBUG,"%05d: pclRNAM: <%s>, pclRTYP: <%s>, pclTIFF: <%s>, pclOBLF: <%s>, pclSTOF: <%s>",__LINE__,pclRNAM, pclRTYP, pclTIFF, pclOBLF, pclSTOF);
     dbg(DEBUG,"%05d: RowNumFLD: <%d>",__LINE__,llRowNumFLD);
     TrimRight(pclDSEQ);
     /* if ((strcmp(pclDSEQ,"-1") != 0) && (strcmp(pclSTOF," ") != 0)) */
     if (strcmp(pclDSEQ,"-1") != 0)
     {
        sprintf(pclBuf,"%d",ilDSEQ);
        ilRC = CEDAArrayPutField(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,
                                 NULL,"DSEQ",llRowNumFLD,pclBuf);
        dbg(DEBUG,"%05d: RowNumFLD: <%d>",__LINE__,llRowNumFLD);
        llRowNumFLD = ARR_CURRENT;
        CEDAArrayGetRowPointer(&rgFldArray.rrArrayHandle,
                               rgFldArray.crArrayName,
                               llRowNumFLD,(void *)&pclRowFLD);
        dbg(DEBUG,"%05d: RowNumFLD: <%d>",__LINE__,llRowNumFLD);
        dbg(DEBUG,"%05d: DSEQ: <%s>",__LINE__,pclBuf);
        ilDSEQ++;
     }
     llRowNumFLD = ARR_NEXT;
     dbg(DEBUG,"%05d: RowNumFLD: <%d>",__LINE__,llRowNumFLD);
  }
  /*  ilRC = SaveIndexInfo(&rgFldArray);*/
  dbg(DEBUG,"=========== End of SetDSEQ =========================================");
  return (ilRC);
}



/******************************************************************************/
/* The updCMD  routine                                                       */
/******************************************************************************/
static int UPDCmd()
{
  int ilRC = 0;
  long llRowNumAFT = ARR_FIRST;
  char *pclRowAFT = NULL;
  char pclAftURNO[128] = "\0";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[256];
  char pclDataBuf[256];
  char *pclRowFLD = NULL;
  long llRowNumFLD = ARR_FIRST;
  char pclFldURNO[16];
  char pclFldRNAM[16];
  char pclFldRTYP[16];

  sprintf(pclSqlBuf,"DELETE FROM FLDTAB WHERE RTYP NOT IN (%s) AND DSEQ = '-1'",pcgMyRtypes);
  slCursor = 0;
  slFkt = START;
  dbg(TRACE,"UPDCmd: <%s>",pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  commit_work();
  close_my_cursor(&slCursor);

  while(CEDAArrayGetRowPointer(&rgAftArray.rrArrayHandle,
                   rgAftArray.crArrayName,
                   llRowNumAFT,(void *)&pclRowAFT) == RC_SUCCESS)
  {
     llRowNumAFT = ARR_CURRENT;
     ilRC = UpdateFLDTAB(pclRowAFT);
     llRowNumAFT = ARR_NEXT;
  }
  dbg(TRACE,"FLDTAB (Step 1) done");
  ilRC = CleanUpFLD();
  dbg(TRACE,"FLDTAB (Step 2) done");
  ilRC = RebuildCounters();
  dbg(TRACE,"RebuildCounters done");
  ilRC = RebuildChutes();
  dbg(TRACE,"RebuildChutes done");

  dbg(TRACE,"UPDCmd: CEDAArrayWriteDB CCATAB");
  ilRC = CEDAArrayWriteDB(&rgCcaArray.rrArrayHandle,rgCcaArray.crArrayName,NULL,NULL,NULL,NULL,
                          ARR_COMMIT_TILL_ERR);

  if (igGenerateFLD == 1)
  {
     dbg(TRACE,"UPDCmd: CEDAArrayWriteDB FLDTAB");
     ilRC = CEDAArrayWriteDB(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,NULL,NULL,NULL,NULL,
                             ARR_COMMIT_TILL_ERR);
  }
  else
  {
     rollback();
  }

  return (ilRC);
}


/******************************************************************************/
/* The DeleteDSEQ  routine                                       */
/******************************************************************************/
static int DeleteDSEQ()
{
  int ilRC = RC_SUCCESS;
  
  long llRowNumFLD = ARR_FIRST;
  char *pclRowFLD = NULL;
  char pclKey[4] = "-1";
  int ilDeleteRowCount = 0;
  char pclFldRNAM[128] = "\0";
  char pclFldRTYP[128] = "\0";
  char pclFldDSEQ[128] = "\0";
  char pclFldCTYP[128] = "\0";
  char pclFldRURN[128] = "\0";

  while (CEDAArrayGetRowPointer(&(rgFldArray.rrArrayHandle),
                                rgFldArray.crArrayName,
                                llRowNumFLD,(void *)&pclRowFLD) == RC_SUCCESS)
  {
     llRowNumFLD = ARR_CURRENT;
     strcpy(pclFldRNAM,FIELDVAL(rgFldArray,pclRowFLD,igFldRNAM));
     TrimRight(pclFldRNAM);
     strcpy(pclFldRTYP,FIELDVAL(rgFldArray,pclRowFLD,igFldRTYP));
     TrimRight(pclFldRTYP);
     strcpy(pclFldDSEQ,FIELDVAL(rgFldArray,pclRowFLD,igFldDSEQ));
     TrimRight(pclFldDSEQ);
     strcpy(pclFldCTYP,FIELDVAL(rgFldArray,pclRowFLD,igFldCTYP));
     TrimRight(pclFldCTYP);
     strcpy(pclFldRURN,FIELDVAL(rgFldArray,pclRowFLD,igFldRURN));
     TrimRight(pclFldRURN);
     if (strcmp(pclFldDSEQ,"-1") == 0)
     {
        dbg(TRACE,"DeleteDSEQ: <%s> <%s> <%s> <%s> <%s>",
            pclFldRNAM,pclFldRTYP,pclFldDSEQ,pclFldCTYP,pclFldRURN);
        ilRC = CEDAArrayDeleteRow(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,llRowNumFLD);
        ilDeleteRowCount++;
        llRowNumFLD = ARR_CURRENT;
     }
     else
     {
        llRowNumFLD = ARR_NEXT;
     }
  }

  if (ilDeleteRowCount > 0)
  {
     dbg(TRACE,"DeleteDSEQ: %d Rows deleted",ilDeleteRowCount);
  }
  return ilRC;
}

/******************************************************************************/
/* The DeleteAURN_FLD  routine                                       */
/******************************************************************************/
static int DeleteAURN_FLD(char *pcpAURN, char *pcpDelete, char *pcpCurRowAFT)
{
  int ilRC = RC_SUCCESS;
 
  long llRowNumFLD = ARR_FIRST;
  char *pclRowFLD = NULL;
  char pclFldAURN[128] = "\0";
  char pclFldRNAM[128] = "\0";
  char pclFldRTYP[128] = "\0";
  char pclAftBLT1[128] = "\0";
  char pclAftBLT2[128] = "\0";
  char pclAftGTD1[128] = "\0";
  char pclAftGTD2[128] = "\0";
  char pclAftWRO1[128] = "\0";
  char pclAftREMP[128] = "\0";
  char pclAftURNO[128] = "\0";
  int ilDeleteRowCount = 0;
  int ilDeleteFlag = 0;
  char pclSqlBuf[128];
 
  while (CEDAArrayGetRowPointer(&rgFldArray.rrArrayHandle,
                                rgFldArray.crArrayName,
                                llRowNumFLD,(void *)&pclRowFLD) == RC_SUCCESS)
  {
     strcpy(pclFldAURN,FIELDVAL(rgFldArray,pclRowFLD,igFldAURN));
     TrimRight(pclFldAURN);
     strcpy(pclFldRNAM,FIELDVAL(rgFldArray,pclRowFLD,igFldRNAM));
     TrimRight(pclFldRNAM);
     strcpy(pclFldRTYP,FIELDVAL(rgFldArray,pclRowFLD,igFldRTYP));
     TrimRight(pclFldRTYP);
     llRowNumFLD = ARR_CURRENT;
     ilDeleteFlag = 0;
     if (strcmp(pclFldAURN,pcpAURN) == 0)
     {
        if (pcpDelete[0] != cBLANK)
        {
           dbg(TRACE,"DeleteAURN_FLD: URNO <%s> deleted (Flight departed)",pclFldAURN);
           ilDeleteFlag = 1;
        }
        if (ilDeleteFlag == 0 && strcmp(pclFldRTYP,pcgRtypBELT) == 0)
        {
           strcpy(pclAftBLT1,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftBLT1));
           TrimRight(pclAftBLT1);
           strcpy(pclAftBLT2,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftBLT2));
           TrimRight(pclAftBLT2);
           if (strcmp(pclFldRNAM,pclAftBLT1) != 0 &&
               strcmp(pclFldRNAM,pclAftBLT2) != 0)
           {
              dbg(TRACE,"DeleteAURN_FLD: URNO <%s> deleted (Belt changed)",pclFldAURN);
              ilDeleteFlag = 1;
           }
        }
        if (ilDeleteFlag == 0 && strcmp(pclFldRTYP,pcgRtypGATE) == 0)
        {
           strcpy(pclAftGTD1,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGTD1));
           TrimRight(pclAftGTD1);
           strcpy(pclAftGTD2,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftGTD2));
           TrimRight(pclAftGTD2);
           if (strcmp(pclFldRNAM,pclAftGTD1) != 0 &&
               strcmp(pclFldRNAM,pclAftGTD2) != 0)
           {
              dbg(TRACE,"DeleteAURN_FLD: URNO <%s> deleted (Gate changed)",pclFldAURN);
              ilDeleteFlag = 1;
              strcpy(pclAftREMP,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftREMP));
              TrimRight(pclAftREMP);
              if (*pclAftREMP == ' ' && strlen(pcgNewGate) > 0)
              {
                 strcpy(pclAftURNO,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftURNO));
                 TrimRight(pclAftURNO);
                 sprintf(pclSqlBuf,"WHERE URNO = %s",pclAftURNO);
                 ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","REMP",pcgTwEnd,pclSqlBuf,
                                  "REMP",pcgNewGate,NULL,0);
              }
           }
        }
        if (ilDeleteFlag == 0 && strcmp(pclFldRTYP,pcgRtypWRO) == 0)
        {
           strcpy(pclAftWRO1,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftWRO1));
           TrimRight(pclAftWRO1);
           if (strcmp(pclFldRNAM,pclAftWRO1) != 0)
           {
              dbg(TRACE,"DeleteAURN_FLD: URNO <%s> deleted (Waiting Room changed)",pclFldAURN);
              ilDeleteFlag = 1;
           }
        }
     }
     if (ilDeleteFlag == 0)
     {
        llRowNumFLD = ARR_NEXT;
     }
     else
     {
        ilRC = CEDAArrayDeleteRow(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,llRowNumFLD);
        llRowNumFLD = ARR_CURRENT;
        ilDeleteRowCount++;
        SetDSEQ(pclFldRNAM,pclFldRTYP);
     }
  }

  if (ilDeleteRowCount > 0)
  {
     dbg(TRACE,"DeleteAURN_FLD: %d rows deleted",ilDeleteRowCount);
  }

  return ilRC;
}          

/******************************************************************************/
/* The CleanUpFLD  routine                                       */
/******************************************************************************/
static int CleanUpFLD()
{
  int ilRC = RC_SUCCESS;
  long llRowNumFLD = ARR_FIRST;
  char *pclRowFLD = NULL;
  long llRowNumAFT = ARR_FIRST;
  char *pclRowAFT = NULL;
  char pclFldAURN[128] = "\0";
  char pclFldRTYP[128] = "\0";
  char pclFldRNAM[128] = "\0";
  char pclAftURNO[128] = "\0";
  int ilFound = 0;
  int ilDeleteRowCount = 0;

  llRowNumFLD = ARR_FIRST; 
  while (CEDAArrayGetRowPointer(&rgFldArray.rrArrayHandle,
                                rgFldArray.crArrayName,
                                llRowNumFLD,(void *)&pclRowFLD) == RC_SUCCESS)
  {
     llRowNumFLD = ARR_CURRENT;
     strcpy(pclFldAURN,FIELDVAL(rgFldArray,pclRowFLD,igFldAURN));
     TrimRight(pclFldAURN);
     strcpy(pclFldRTYP,FIELDVAL(rgFldArray,pclRowFLD,igFldRTYP));
     TrimRight(pclFldRTYP);
     strcpy(pclFldRNAM,FIELDVAL(rgFldArray,pclRowFLD,igFldRNAM));
     TrimRight(pclFldRNAM);
     if (strcmp(pclFldRTYP,pcgRtypBELT) == 0 ||
         strcmp(pclFldRTYP,pcgRtypGATE) == 0 ||
         strcmp(pclFldRTYP,pcgRtypWRO) == 0)
     {
        llRowNumAFT = ARR_FIRST; 
        ilFound = 0;
        while (ilFound == 0 && 
               CEDAArrayGetRowPointer(&rgAftArray.rrArrayHandle,
                                      rgAftArray.crArrayName,
                                      llRowNumAFT,(void *)&pclRowAFT) == RC_SUCCESS)
        {
           llRowNumAFT = ARR_CURRENT;
           strcpy(pclAftURNO,FIELDVAL(rgAftArray,pclRowAFT,igAftURNO));
           TrimRight(pclAftURNO);
           if (strcmp(pclFldAURN,pclAftURNO) == 0)
           {
              ilFound = 1;
           }
           else
           {
              llRowNumAFT = ARR_NEXT;
           }
        }
        if (ilFound == 0)
        {
           ilRC = CEDAArrayDeleteRow(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,llRowNumFLD);
           llRowNumFLD = ARR_CURRENT;
           ilDeleteRowCount++;
           SetDSEQ(pclFldRNAM,pclFldRTYP);
        }
        else
        {
           llRowNumFLD = ARR_NEXT;
        }
     }
     else
     {
        llRowNumFLD = ARR_NEXT;
     }
  }

  if (ilDeleteRowCount > 0)
  {
     dbg(TRACE,"CleanUpFLD: %d rows deleted",ilDeleteRowCount);
  }

  return ilRC;
}          


/******************************************************************************/
/* The RebuildChutes  routine                                       */
/******************************************************************************/
static int RebuildChutes()
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[1024];
  char pclSelection[1024];
  char pclDataBuf[1024];
  long llRowNumFLD = ARR_FIRST;
  long llRowNumCHA = ARR_FIRST;
  char *pclRowFLD = NULL;
  char *pclRowCHA = NULL;
  char pclNowTime[128] = "\0";
  char pclFldRNAM[128] = "\0";
  char pclFldRTYP[128] = "\0";
  char pclFldAURN[128] = "\0";
  char pclFldFLNO[128] = "\0";
  char pclFldJFNO[128] = "\0";
  char pclFldJCNT[128] = "\0";
  char pclChaLNAM[128] = "\0";
  char pclChaRURN[128] = "\0";
  char pclChaDES3[128] = "\0";
  char pclChaCHGF[128] = "\0";
  int ilFound = 0;
  char pclChaUrno[16];
  char pclChaLnam[16];
  char pclChaRurn[16];
  char pclChaTifb[16];
  char pclChaDes3[16];
  char pclChaUrnoP[16];
  char pclChaLnamP[16];
  char pclChaRurnP[16];
  char pclChaTifbP[16];
  char pclChaDes3P[16];
  char pclUrnoList[1024];

  if (strcmp(pcgHandleChutes,"NO") == 0)
  {
     dbg(TRACE,"RebuildChutes: Chutes are not handled");
     return ilRC;
  }

  TimeToStr(pclNowTime,time(NULL));

  if (strcmp(pcgCorrectPlanBag,"YES") == 0)
  {
     strcpy(pclUrnoList,"");
     strcpy(pclChaUrnoP,"");
     strcpy(pclChaLnamP,"");
     strcpy(pclChaRurnP,"");
     strcpy(pclChaTifbP,"");
     strcpy(pclChaDes3P,"");
     strcpy(pclSqlBuf,"SELECT URNO,LNAM,RURN,TIFB,DES3 FROM CHATAB ");
     sprintf(pclSelection,"WHERE TIFB <= '%s' AND TIFE >= '%s' AND LNAM <> ' ' ORDER BY LNAM,RURN,TIFB,DES3",
             pclNowTime,pclNowTime);
     strcat(pclSqlBuf,pclSelection);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"RebuildChutes: <%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     while (ilRCdb == DB_SUCCESS)
     {
        BuildItemBuffer(pclDataBuf,"",5,",");
        get_real_item(pclChaUrno,pclDataBuf,1);
        get_real_item(pclChaLnam,pclDataBuf,2);
        get_real_item(pclChaRurn,pclDataBuf,3);
        get_real_item(pclChaTifb,pclDataBuf,4);
        get_real_item(pclChaDes3,pclDataBuf,5);
        if (strcmp(pclChaLnamP,pclChaLnam) == 0 && strcmp(pclChaRurnP,pclChaRurn) == 0 &&
            strcmp(pclChaTifbP,pclChaTifb) == 0 && strcmp(pclChaDes3P,"@@@") == 0)
        {
           strcat(pclUrnoList,pclChaUrnoP);
           strcat(pclUrnoList,",");
           strcpy(pclChaUrnoP,"");
           strcpy(pclChaLnamP,"");
           strcpy(pclChaRurnP,"");
           strcpy(pclChaTifbP,"");
           strcpy(pclChaDes3P,"");
        }
        else
        {
           strcpy(pclChaUrnoP,pclChaUrno);
           strcpy(pclChaLnamP,pclChaLnam);
           strcpy(pclChaRurnP,pclChaRurn);
           strcpy(pclChaTifbP,pclChaTifb);
           strcpy(pclChaDes3P,pclChaDes3);
        }
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     }
     close_my_cursor(&slCursor);
     if (strlen(pclUrnoList) > 0)
     {
        pclUrnoList[strlen(pclUrnoList)-1] = '\0';
        sprintf(pclSqlBuf,"DELETE FROM CHATAB WHERE URNO IN (%s)",pclUrnoList);
        slCursor = 0;
        slFkt = START;
        dbg(TRACE,"RebuildChutes: <%s>",pclSqlBuf);
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        commit_work();
        close_my_cursor(&slCursor);
     }
  }

  sprintf(pclSqlBuf,"WHERE TIFB <= '%s' AND TIFE >= '%s' AND LNAM <> ' ' ORDER BY LNAM,TIFB",
          pclNowTime,pclNowTime);
  dbg(DEBUG,"Refill CHA-Array: CHA_FIELDS: <%s>, &rgChaArray: <%d>, pclSelection: <%s>",
      CHA_FIELDS,&rgChaArray,pclSqlBuf);
  ilRC = CEDAArrayRefill(&rgChaArray.rrArrayHandle,rgChaArray.crArrayName,pclSqlBuf,CHA_FIELDS,ARR_FIRST);
  if (ilRC != RC_SUCCESS)
     dbg(TRACE,"%05d: Refill CHA CEDAArray failed",__LINE__);
  else
     dbg(DEBUG,"%05d: Refill CHA CEDAArray successful",__LINE__);

  /*   Mark all CHA Rows as not yet processed */
  llRowNumCHA = ARR_FIRST;
  while(CEDAArrayGetRowPointer(&rgChaArray.rrArrayHandle,
                               rgChaArray.crArrayName,
                               llRowNumCHA,(void *)&pclRowCHA) == RC_SUCCESS)
  {
     llRowNumCHA = ARR_CURRENT;
     strcpy(pclChaLNAM,FIELDVAL(rgChaArray,pclRowCHA,igChaLNAM));
     TrimRight(pclChaLNAM);
     strcpy(pclChaDES3,FIELDVAL(rgChaArray,pclRowCHA,igChaDES3));
     TrimRight(pclChaDES3);
     dbg(DEBUG,"RebuildChutes: LNAM = <%s><%s> found in CHA",pclChaLNAM,pclChaDES3);
     ilRC = CEDAArrayPutField(&rgChaArray.rrArrayHandle,rgChaArray.crArrayName,NULL,"CHGF",llRowNumCHA," ");
     llRowNumCHA = ARR_NEXT;
  }

  /*   Update or Delete FLD */
  llRowNumFLD = ARR_FIRST;
  while(CEDAArrayGetRowPointer(&rgFldArray.rrArrayHandle,
                               rgFldArray.crArrayName,
                               llRowNumFLD,(void *)&pclRowFLD) == RC_SUCCESS)
  {
     llRowNumFLD = ARR_CURRENT;
     strcpy(pclFldRNAM,FIELDVAL(rgFldArray,pclRowFLD,igFldRNAM));
     TrimRight(pclFldRNAM);
     strcpy(pclFldRTYP,FIELDVAL(rgFldArray,pclRowFLD,igFldRTYP));
     TrimRight(pclFldRTYP);
     strcpy(pclFldAURN,FIELDVAL(rgFldArray,pclRowFLD,igFldAURN));
     TrimRight(pclFldAURN);
     if (strcmp(pclFldRTYP,pcgRtypCHUTE) == 0)
     {
	llRowNumCHA = ARR_FIRST;
	ilFound = 0;
        while(ilFound == 0 && CEDAArrayGetRowPointer(&rgChaArray.rrArrayHandle,
                                                     rgChaArray.crArrayName,
                                                     llRowNumCHA,(void *)&pclRowCHA) == RC_SUCCESS)
        {
           llRowNumCHA = ARR_CURRENT;
           strcpy(pclChaLNAM,FIELDVAL(rgChaArray,pclRowCHA,igChaLNAM));
           TrimRight(pclChaLNAM);
           strcpy(pclChaRURN,FIELDVAL(rgChaArray,pclRowCHA,igChaRURN));
           TrimRight(pclChaRURN);
           strcpy(pclChaCHGF,FIELDVAL(rgChaArray,pclRowCHA,igChaCHGF));
           TrimRight(pclChaCHGF);
	   if (strcmp(pclFldRNAM,pclChaLNAM) == 0 && strcmp(pclFldAURN,pclChaRURN) == 0 &&
               *pclChaCHGF == ' ')
	   {
              ilRC = CheckFtyp(pclChaRURN);
              if (ilRC == RC_SUCCESS)
              {
                 dbg(DEBUG,"RebuildChutes: RNAM = <%s><%s> must be updated in FLD",pclChaLNAM,pclChaRURN);
                 ilFound = 1; /* Update FLD */
                 ilRC = PutFieldFLD("AURN",FIELDVAL(rgChaArray,pclRowCHA,igChaRURN),&llRowNumFLD);
                 ilRC = PutFieldFLD("LSTU",pclNowTime,&llRowNumFLD);
                 ilRC = PutFieldFLD("SOTI",FIELDVAL(rgChaArray,pclRowCHA,igChaTIFB),&llRowNumFLD);
                 ilRC = PutFieldFLD("SCTI",FIELDVAL(rgChaArray,pclRowCHA,igChaTIFE),&llRowNumFLD);
                 ilRC = PutFieldFLD("STAT",FIELDVAL(rgChaArray,pclRowCHA,igChaDES3),&llRowNumFLD);
                 ilRC = PutFieldFLD("RTAB",FIELDVAL(rgChaArray,pclRowCHA,igChaDES3),&llRowNumFLD);
                 ilRC = PutFieldFLD("CREC",FIELDVAL(rgChaArray,pclRowCHA,igChaFCLA),&llRowNumFLD);
                 ilRC = PutFieldFLD("CTYP",FIELDVAL(rgChaArray,pclRowCHA,igChaLAFX),&llRowNumFLD);
                 if (igSelectCounterLogo == TRUE)
                 {
                    ilRC = GetFLNOandJFNO(pclChaRURN,pclFldFLNO,pclFldJFNO,pclFldJCNT);
                    ilRC = PutFieldFLD("FLNO",pclFldFLNO,&llRowNumFLD);
                    ilRC = PutFieldFLD("JFNO",pclFldJFNO,&llRowNumFLD);
                    ilRC = PutFieldFLD("JCNT",pclFldJCNT,&llRowNumFLD);
                 }
                 ilRC = CEDAArrayPutField(&rgChaArray.rrArrayHandle,rgChaArray.crArrayName,
                                          NULL,"CHGF",llRowNumCHA,"D");
	      }
	   }
           llRowNumCHA = ARR_NEXT;
	}
	if (ilFound == 0)
	{
           dbg(DEBUG,"RebuildChutes: RNAM = <%s><%s> must be deleted from FLD",pclFldRNAM,pclFldAURN);
           ilRC = CEDAArrayDeleteRow(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,llRowNumFLD);
           llRowNumFLD = ARR_CURRENT;
	}
        else
        {
           llRowNumFLD = ARR_NEXT;
        }
        ilRC = SetDSEQ(pclFldRNAM,pclFldRTYP);
     }
     else
     {
        llRowNumFLD = ARR_NEXT;
     }
  }

  /*   Insert all remaining CHA Rows into FLD */
  llRowNumCHA = ARR_FIRST;
  while(CEDAArrayGetRowPointer(&rgChaArray.rrArrayHandle,
                               rgChaArray.crArrayName,
                               llRowNumCHA,(void *)&pclRowCHA) == RC_SUCCESS)
  {
     llRowNumCHA = ARR_CURRENT;
     strcpy(pclChaCHGF,FIELDVAL(rgChaArray,pclRowCHA,igChaCHGF));
     if (pclChaCHGF[0] == cBLANK)
     {
        strcpy(pclChaRURN,FIELDVAL(rgChaArray,pclRowCHA,igChaRURN));
        TrimRight(pclChaRURN);
        ilRC = CheckFtyp(pclChaRURN);
        if (ilRC == RC_SUCCESS)
        {
           dbg(DEBUG,"RebuildChutes: LNAM = <%s><%s> must be inserted into FLD",
               FIELDVAL(rgChaArray,pclRowCHA,igChaLNAM),FIELDVAL(rgChaArray,pclRowCHA,igChaRURN));
           ilRC = InitFldStruct();
           ilRC = GetNextValues(rgFldFlds.pclFldURNO,1);
           if (ilRC != RC_SUCCESS)
           {
              dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
           }
           strcpy(rgFldFlds.pclFldRNAM,FIELDVAL(rgChaArray,pclRowCHA,igChaLNAM));
           strcpy(rgFldFlds.pclFldRTYP,pcgRtypCHUTE);
           strcpy(rgFldFlds.pclFldAURN,FIELDVAL(rgChaArray,pclRowCHA,igChaRURN));
           TimeToStr(rgFldFlds.pclFldLSTU,time(NULL)); 
           TimeToStr(rgFldFlds.pclFldCDAT,time(NULL));
           strcpy(rgFldFlds.pclFldUSEU,"FLDHDL");
           strcpy(rgFldFlds.pclFldUSEC,"FLDHDL");
           strcpy(rgFldFlds.pclFldHOPO,pcgHomeAp);
           strcpy(rgFldFlds.pclFldTIFF,FIELDVAL(rgChaArray,pclRowCHA,igChaTIFB));
           strcpy(rgFldFlds.pclFldSTOF,FIELDVAL(rgChaArray,pclRowCHA,igChaTIFB));
           strcpy(rgFldFlds.pclFldSOTI,FIELDVAL(rgChaArray,pclRowCHA,igChaTIFB));
           strcpy(rgFldFlds.pclFldSCTI,FIELDVAL(rgChaArray,pclRowCHA,igChaTIFE));
           strcpy(rgFldFlds.pclFldSTAT,FIELDVAL(rgChaArray,pclRowCHA,igChaDES3));
           strcpy(rgFldFlds.pclFldRTAB,FIELDVAL(rgChaArray,pclRowCHA,igChaDES3));
           strcpy(rgFldFlds.pclFldCREC,FIELDVAL(rgChaArray,pclRowCHA,igChaFCLA));
           strcpy(rgFldFlds.pclFldCTYP,FIELDVAL(rgChaArray,pclRowCHA,igChaLAFX));
           if (igSelectCounterLogo == TRUE)
           {
              ilRC = GetFLNOandJFNO(FIELDVAL(rgChaArray,pclRowCHA,igChaRURN),
                                    pclFldFLNO,pclFldJFNO,pclFldJCNT);
              strcpy(rgFldFlds.pclFldFLNO,pclFldFLNO);
              strcpy(rgFldFlds.pclFldJFNO,pclFldJFNO);
              strcpy(rgFldFlds.pclFldJCNT,pclFldJCNT);
           }
           ilRC = StructAddRowPartFLD(&llRowNumFLD);
           ilRC = SetDSEQ(rgFldFlds.pclFldRNAM,rgFldFlds.pclFldRTYP);
        }
     }
     llRowNumCHA = ARR_NEXT;
  }

  return (ilRC);
}


/******************************************************************************/
/* The RebuildCounters routine                                                */
/******************************************************************************/
static int RebuildCounters()
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  long llRowNumFLD = ARR_FIRST;
  long llRowNumCCA = ARR_FIRST;
  char *pclRowFLD = NULL;
  char *pclRowCCA = NULL;
  char pclNowTime[128] = "\0";
  char pclNowTimeExt[128] = "\0";
  char pclNowTimeCca[128] = "\0";
  short slFunction;
  short slCursor;
  char pclSqlBuf[1024] = "\0";
  char pclBuf[1024] = "\0";
  char pclData[1024] = "\0";
  char pclFldRNAM[128] = "\0";
  char pclFldRTYP[128] = "\0";
  char pclFldRURN[128] = "\0";
  char pclFldURNO[128] = "\0";
  char pclCcaCKIC[128] = "\0";
  char pclCcaURNO[128] = "\0";
  char pclCcaFLAG[128] = "\0";
  int ilFound = 0;
  struct _GroupList
  {
     char pclGroupUrno[16];
     int ilNoOfGroupMembers;
     char pclMemberUrnos[50][16];
  } rlGroupList[50];
  int ilNoOfGroups = 0;
  char pclCcaGRNU[16] = "\0";
  char pclCcaFLNU[16] = "\0";
  int ilI;
  int ilJ;
  int ilCnt;
  int ilFoundMem = 0;
  int ilPutInCounterArrayProcessed;
  char pclFldFLNO[16];
  char pclFldJFNO[128];
  char pclFldJCNT[16];
  int ilRcCnt;
  char pclCntBegin[16];
  char pclCntEnd[16];
  char pclFlzUrno[16];
  char pclFlzFlgu[16];
  char pclFlzLnam[16];
  char pclSelection[1024];

  igCurNoComCnt = 0;
  igCurNoDediCnt = 0;

  TimeToStr(pclNowTime,time(NULL));
  strcpy(pclNowTimeExt,pclNowTime);
  AddSecondsToCEDATime12(pclNowTimeExt,igRefreshTime,1);
  if (strcmp(pcgCountersWithActualTimeOnly,"YES") == 0)
  {
     strcpy(pclNowTimeCca,pclNowTime);
     /*AddSecondsToCEDATime12(pclNowTimeCca,7200,1);*/
     AddSecondsToCEDATime12(pclNowTimeCca,igFddCntEnd*60*60,1);
     sprintf(pclSqlBuf,"WHERE ((CKBA != ' ' AND CKBA <= '%s' AND (CKEA = ' ' OR (CKEA != ' ' AND CKEA >= '%s')))) OR (CKBS <= '%s' AND CKES >= '%s') AND CKIC <> ' ' AND FLNU <> 0 ORDER BY CKIC,CKBA",
             pclNowTime,pclNowTime,pclNowTimeCca,pclNowTime);
  }
  else if (strcmp(pcgCountersPreDisplay,"YES") == 0)
  {
     strcpy(pclNowTimeCca,pclNowTime);
     AddSecondsToCEDATime12(pclNowTimeCca,igFddCntEnd*60*60,1);
     sprintf(pclSqlBuf,"WHERE ((CKBS <= '%s' AND CKES >= '%s') OR (CKBA <= '%s' AND CKEA >= '%s')) AND CKIC <> ' ' AND FLNU <> 0 ORDER BY CKIC,CKBS",
             pclNowTimeCca,pclNowTime,pclNowTimeCca,pclNowTime);
  }
  else
  {
     sprintf(pclSqlBuf,"WHERE ((CKBS <= '%s' AND CKES >= '%s') OR (CKBA <= '%s' AND CKEA >= '%s')) AND CKIC <> ' ' AND FLNU <> 0 ORDER BY CKIC,CKBS",
             pclNowTime,pclNowTime,pclNowTime,pclNowTime);
  }

  dbg(DEBUG,"Refill CCA-Array: CCA_FIELDS: <%s>, &rgCcaArray: <%d>, pclSelection: <%s>",CCA_FIELDS_TAB,&rgCcaArray,pclSqlBuf);

  ilRC = CEDAArrayRefill(&rgCcaArray.rrArrayHandle,rgCcaArray.crArrayName,pclSqlBuf,CCA_FIELDS,ARR_FIRST);
  if (ilRC != RC_SUCCESS)
  {
     dbg(TRACE,"%05d: Refill CCA CEDAArray failed",__LINE__);
  }
  else
  {
     dbg(TRACE,"Refill CCA CEDAArray successful");
  }

  /* Clear Counter Arrays */
  dbg(TRACE,"Clear %d Common Counters",igMaxNoComCnt);
  for (ilI=0; ilI<igMaxNoComCnt; ilI++)
  {
     strcpy(&rgCommonCounters[ilI].pclComALC2[0],"");
     strcpy(&rgCommonCounters[ilI].pclComALC3[0],"");
  }
  dbg(TRACE,"Clear %d Dedicated Counters",igMaxNoDediCnt);
  for (ilI=0; ilI<igMaxNoDediCnt; ilI++)
     strcpy(&rgDedicatedCounters[ilI].pclDediFLNO[0],"");
  /* Mark all CCA Rows as not yet processed */
  ilNoOfGroups = 0;
  llRowNumCCA = ARR_FIRST;
  while(CEDAArrayGetRowPointer(&rgCcaArray.rrArrayHandle,
                               rgCcaArray.crArrayName,
                               llRowNumCCA,(void *)&pclRowCCA) == RC_SUCCESS)
  {
     llRowNumCCA = ARR_CURRENT;
     strcpy(pclCcaCKIC,FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKIC));
     TrimRight(pclCcaCKIC);
     strcpy(pclCcaFLNU,FIELDVAL(rgCcaArray,pclRowCCA,igCcaFLNU));
     TrimRight(pclCcaFLNU);
     dbg(DEBUG,"RebuildCounters: CKIC = <%s> FLNU = <%s> found in CCA",
				 pclCcaCKIC,pclCcaFLNU);
     ilRC = CEDAArrayPutField(&rgCcaArray.rrArrayHandle,rgCcaArray.crArrayName,
                              NULL,"FLAG",llRowNumCCA,"x");
     if (igProcAirlGroups == TRUE)
     {
        strcpy(pclCcaGRNU,FIELDVAL(rgCcaArray,pclRowCCA,igCcaGRNU));
        TrimRight(pclCcaGRNU);
        if (strlen(pclCcaGRNU) > 1)
        {
           ilFound = 0;
           for (ilI = 0; ilI < ilNoOfGroups && ilFound == 0; ilI++)
           {
              if (strcmp(pclCcaGRNU,&rlGroupList[ilI].pclGroupUrno[0]) == 0)
              {
                 ilFound = 1;
              }
           }
           if (ilFound == 0)
           {
              ilCnt = 0;
              strcpy(&rlGroupList[ilNoOfGroups].pclGroupUrno[0],pclCcaGRNU);
              strcpy(pclCcaFLNU,FIELDVAL(rgCcaArray,pclRowCCA,igCcaFLNU));
              TrimRight(pclCcaFLNU);
              if (strlen(pclCcaFLNU) > 1)
              {
                 strcpy(&rlGroupList[ilNoOfGroups].pclMemberUrnos[ilCnt][0],pclCcaFLNU);
                 ilCnt++;
              }
              sprintf(pclSqlBuf,"SELECT VALU FROM GRMTAB WHERE GURN='%s'",pclCcaGRNU);
              slFunction= START;
              slCursor = 0;
              ilRCdb = sql_if(slFunction,&slCursor,pclSqlBuf,pclData);
              while (ilRCdb == DB_SUCCESS) 
              {
                 TrimRight(pclData);
                 ilFoundMem = 0;
                 for (ilI = 0; ilI < ilCnt && ilFoundMem == 0; ilI++)
                 {
                    if (strcmp(pclData,&rlGroupList[ilNoOfGroups].pclMemberUrnos[ilI][0]) == 0)
                    {
                       ilFoundMem = 1;
                    }
                 }
                 if (ilFoundMem == 0)
                 {
                    strcpy(&rlGroupList[ilNoOfGroups].pclMemberUrnos[ilCnt][0],pclData);
                    ilCnt++;
                 }
                 slFunction= NEXT;
                 ilRCdb = sql_if(slFunction,&slCursor,pclSqlBuf,pclData);
              }                           
              close_my_cursor(&slCursor);
              rlGroupList[ilNoOfGroups].ilNoOfGroupMembers = ilCnt;
              ilNoOfGroups++;
           }
        }
     }
     llRowNumCCA = ARR_NEXT;
  }

  /*   Update or Delete FLD */
  llRowNumFLD = ARR_FIRST;
  while(CEDAArrayGetRowPointer(&rgFldArray.rrArrayHandle,
                               rgFldArray.crArrayName,
                               llRowNumFLD,(void *)&pclRowFLD) == RC_SUCCESS)
  {
     llRowNumFLD = ARR_CURRENT;
     strcpy(pclFldRNAM,FIELDVAL(rgFldArray,pclRowFLD,igFldRNAM));
     TrimRight(pclFldRNAM);
     strcpy(pclFldRTYP,FIELDVAL(rgFldArray,pclRowFLD,igFldRTYP));
     TrimRight(pclFldRTYP);
     strcpy(pclFldRURN,FIELDVAL(rgFldArray,pclRowFLD,igFldRURN));
     TrimRight(pclFldRURN);
     if (strcmp(pclFldRTYP,pcgRtypCOUNTER) == 0)
     {
        llRowNumCCA = ARR_FIRST;
        ilFound = 0;
        while(ilFound == 0 && CEDAArrayGetRowPointer(&rgCcaArray.rrArrayHandle,
                                                     rgCcaArray.crArrayName,
                                                     llRowNumCCA,(void *)&pclRowCCA) == RC_SUCCESS)
        {
           llRowNumCCA = ARR_CURRENT;
           strcpy(pclCcaCKIC,FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKIC));
           TrimRight(pclCcaCKIC);
           strcpy(pclCcaURNO,FIELDVAL(rgCcaArray,pclRowCCA,igCcaURNO));
           TrimRight(pclCcaURNO);
           if (strcmp(pclFldRNAM,pclCcaCKIC) == 0 && strcmp(pclFldRURN,pclCcaURNO) == 0)
           {
              if (strcmp(pcgCountersWithActualTimeOnly,"YES") == 0)
              {
                 strcpy(pclCntBegin,FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKBA));
                 strcpy(pclCntEnd,FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKEA));
                 if (strlen(pclCntBegin) == 0 || *pclCntBegin == ' ')
                    strcpy(pclCntBegin,"20201231235959");
                 if (strlen(pclCntEnd) == 0 || *pclCntEnd == ' ')
                    strcpy(pclCntEnd,"20201231235959");
                 ilRcCnt = ValidTime(pclCntBegin,pclCntEnd,pclCntBegin,pclCntEnd);
                 if (ilRcCnt != RC_SUCCESS)
                 {
                    if (strcmp(pclCntBegin,"20201231235959") == 0)
                    {
                       ilRC = PutInCounterArray(FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP),
                                                FIELDVAL(rgCcaArray,pclRowCCA,igCcaFLNU),
                                                FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKIC),
                                                FIELDVAL(rgCcaArray,pclRowCCA,igCcaDISP));
                    }
                 }
              }
              else
              {
                 ilRcCnt = ValidTime(FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKBS),
                                     FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKES),
                                     FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKBA),
                                     FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKEA));
                 if (ilRcCnt != RC_SUCCESS && strcmp(pcgCountersPreDisplay,"YES") == 0)
                 {
                    ilRC = PutInCounterArray(FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP),
                                             FIELDVAL(rgCcaArray,pclRowCCA,igCcaFLNU),
                                             FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKIC),
                                             FIELDVAL(rgCcaArray,pclRowCCA,igCcaDISP));
                 }
              }
              ilRC = CheckCcaFtyp(FIELDVAL(rgCcaArray,pclRowCCA,igCcaFLNU),
                                  FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP));
              if (ilRcCnt == RC_SUCCESS && ilRC == RC_SUCCESS)
              {
                 dbg(DEBUG,"RebuildCounters: CKIC = <%s><%s> must be updated in FLD",pclCcaCKIC,pclCcaURNO);
                 ilFound = 2; /* Update FLD */
                 strcpy(pclBuf,FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP));
                 TrimRight(pclBuf);
                 if (strcmp(pclBuf,"C") == 0)
                 {
                    ilRC = PutFieldFLD("AURN","0",&llRowNumFLD);
                 }
                 else
                 {
                    ilRC = PutFieldFLD("AURN",FIELDVAL(rgCcaArray,pclRowCCA,igCcaFLNU),&llRowNumFLD);
                 }
                 if (strcmp(pcgUseFLZTAB,"YES") == 0)
                 {
                    sprintf(pclSqlBuf,"SELECT URNO,FLGU,LNAM FROM FLZTAB WHERE RURN = %s AND UTYP = 'C'",
                            FIELDVAL(rgCcaArray,pclRowCCA,igCcaURNO));
                    slFunction = START;
                    slCursor = 0;
                    ilRCdb = sql_if(slFunction,&slCursor,pclSqlBuf,pclData);
                    close_my_cursor(&slCursor);
                    if (ilRCdb == DB_SUCCESS) 
                    {
                       BuildItemBuffer(pclData,"",3,",");
                       get_real_item(pclFlzUrno,pclData,1);
                       TrimRight(pclFlzUrno);
                       get_real_item(pclFlzFlgu,pclData,2);
                       TrimRight(pclFlzFlgu);
                       get_real_item(pclFlzLnam,pclData,3);
                       TrimRight(pclFlzLnam);
                       if (strncmp(pclFlzLnam,
                                   FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKIC),strlen(pclFlzLnam)) != 0)
                       {
                          sprintf(pclSelection,"WHERE URNO = %s",pclFlzUrno);
                          sprintf(pclSqlBuf,"UPDATE FLZTAB SET LNAM = '%s' %s",
                                  FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKIC),pclSelection);
                          slCursor = 0;
                          slFunction = START;
                          ilRCdb = sql_if(slFunction,&slCursor,pclSqlBuf,pclData);
                          commit_work();
                          close_my_cursor(&slCursor);
                       }
                       sprintf(pclSqlBuf,"SELECT LGSN FROM FLGTAB WHERE URNO = %s",pclFlzFlgu);
                       slFunction= START;
                       slCursor = 0;
                       ilRCdb = sql_if(slFunction,&slCursor,pclSqlBuf,pclData);
                       close_my_cursor(&slCursor);
                       if (ilRCdb == DB_SUCCESS) 
                       {
                          TrimRight(pclData);
                          if (strcmp(FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP),"C") == 0)
                          {
                             if (strncmp(pclData,
                                         FIELDVAL(rgCcaArray,pclRowCCA,igCcaREMA),strlen(pclData)) != 0)
                                ilRC = CEDAArrayPutField(&rgCcaArray.rrArrayHandle,rgCcaArray.crArrayName,
                                                         NULL,"REMA",llRowNumCCA,pclData);
                          }
                          else
                          {
                             ilRC = PutFieldFLD("CREC",pclData,&llRowNumFLD);
                          }
                       } 
                    }
                    else
                    {
                       if (strcmp(FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP),"C") == 0)
                          ilRC = CEDAArrayPutField(&rgCcaArray.rrArrayHandle,rgCcaArray.crArrayName,NULL,
                                                   "REMA",llRowNumCCA," ");
                       else
                          ilRC = PutFieldFLD("CREC"," ",&llRowNumFLD);
                    }
                 }
                 else
                    ilRC = PutFieldFLD("CREC",FIELDVAL(rgCcaArray,pclRowCCA,igCcaDISP),&llRowNumFLD);
                 ilRC = PutFieldFLD("SOTI",FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKBS),&llRowNumFLD);
                 ilRC = PutFieldFLD("SCTI",FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKES),&llRowNumFLD);
                 ilRC = PutFieldFLD("DOTI",FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKBA),&llRowNumFLD);
                 ilRC = PutFieldFLD("DCTI",FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKEA),&llRowNumFLD);
                 ilRC = PutFieldFLD("STAT",FIELDVAL(rgCcaArray,pclRowCCA,igCcaREMA),&llRowNumFLD);
                 ilRC = PutFieldFLD("LSTU",pclNowTime,&llRowNumFLD);
                 ilRC = CEDAArrayPutField(&rgCcaArray.rrArrayHandle,rgCcaArray.crArrayName,NULL,"FLAG",
                                          llRowNumCCA," ");
                 ilPutInCounterArrayProcessed = FALSE;
                 if (strcmp(FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP),"C") != 0 &&
                     igSelectCounterLogo == TRUE)
                 {
                    ilRC = GetFLNOandJFNO(FIELDVAL(rgCcaArray,pclRowCCA,igCcaFLNU),
                                          pclFldFLNO,pclFldJFNO,pclFldJCNT);
                    ilRC = PutFieldFLD("FLNO",pclFldFLNO,&llRowNumFLD);
                    ilRC = PutFieldFLD("JFNO",pclFldJFNO,&llRowNumFLD);
                    ilRC = PutFieldFLD("JCNT",pclFldJCNT,&llRowNumFLD);
                 }
                 if (strcmp(FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP),"C") == 0 &&
                     igProcAirlGroups == TRUE)
                 {
                    strcpy(pclCcaGRNU,FIELDVAL(rgCcaArray,pclRowCCA,igCcaGRNU));
                    TrimRight(pclCcaGRNU);
                    if (strlen(pclCcaGRNU) > 1)
                    {
                       for (ilI = 0; ilI < ilNoOfGroups; ilI++)
                       {
                          if (strcmp(pclCcaGRNU,&rlGroupList[ilI].pclGroupUrno[0]) == 0)
                          {
                             for (ilJ = 0; ilJ < rlGroupList[ilI].ilNoOfGroupMembers; ilJ++)
                             {
                                ilRC = PutInCounterArray(FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP),
                                                         &rlGroupList[ilI].pclMemberUrnos[ilJ][0],
                                                         FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKIC),
                                                         FIELDVAL(rgCcaArray,pclRowCCA,igCcaDISP));
                             }
                             ilPutInCounterArrayProcessed = TRUE;
                          }
                       }
                    }
                 }
                 if (ilPutInCounterArrayProcessed == FALSE)
                 {
                    ilRC = PutInCounterArray(FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP),
                                             FIELDVAL(rgCcaArray,pclRowCCA,igCcaFLNU),
                                             FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKIC),
                                             FIELDVAL(rgCcaArray,pclRowCCA,igCcaDISP));
                 }
              }
              else
              {
                 ilFound = 1; /* Delete from FLD */
                 ilRC = CEDAArrayPutField(&rgCcaArray.rrArrayHandle,rgCcaArray.crArrayName,NULL,"FLAG",llRowNumCCA," ");
              }
           }
           llRowNumCCA = ARR_NEXT;
        }
        if (ilFound == 0 || ilFound == 1)
        {
           dbg(DEBUG,"RebuildCounters: RNAM = <%s><%s> must be deleted from FLD",pclFldRNAM,pclFldRURN);
           ilRC = CEDAArrayDeleteRow(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,llRowNumFLD);
           llRowNumFLD = ARR_CURRENT;
        }
        else
        {
           llRowNumFLD = ARR_NEXT;
        }
        ilRC = SetDSEQ(pclFldRNAM,pclFldRTYP);
     }
     else
     {
        if (pclFldRNAM[0] == cBLANK)
        {
           dbg(DEBUG,"RebuildCounters: RNAM = <%s><%s> must be deleted from FLD",pclFldRNAM,pclFldRTYP);
           ilRC = CEDAArrayDeleteRow(&rgFldArray.rrArrayHandle,rgFldArray.crArrayName,llRowNumFLD);
           llRowNumFLD = ARR_CURRENT;
        }
        else
        {
           llRowNumFLD = ARR_NEXT;
        }
     }
  }

  /*   Insert all remaining CCA Rows into FLD */
  llRowNumCCA = ARR_FIRST;
  while(CEDAArrayGetRowPointer(&rgCcaArray.rrArrayHandle,
                               rgCcaArray.crArrayName,
                               llRowNumCCA,(void *)&pclRowCCA) == RC_SUCCESS)
  {
     llRowNumCCA = ARR_CURRENT;
     strcpy(pclCcaFLAG,FIELDVAL(rgCcaArray,pclRowCCA,igCcaFLAG));
     TrimRight(pclCcaFLAG);
     if (strcmp(pclCcaFLAG,"x") == 0)
     {
        if (strcmp(pcgCountersWithActualTimeOnly,"YES") == 0)
        {
           strcpy(pclCntBegin,FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKBA));
           strcpy(pclCntEnd,FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKEA));
           if (strlen(pclCntBegin) == 0 || *pclCntBegin == ' ')
              strcpy(pclCntBegin,"20201231235959");
           if (strlen(pclCntEnd) == 0 || *pclCntEnd == ' ')
              strcpy(pclCntEnd,"20201231235959");
           ilRcCnt = ValidTime(pclCntBegin,pclCntEnd,pclCntBegin,pclCntEnd);
           if (ilRcCnt != RC_SUCCESS)
           {
              if (strcmp(pclCntBegin,"20201231235959") == 0)
              {
                 ilRC = PutInCounterArray(FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP),
                                          FIELDVAL(rgCcaArray,pclRowCCA,igCcaFLNU),
                                          FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKIC),
                                          FIELDVAL(rgCcaArray,pclRowCCA,igCcaDISP));
              }
           }
        }
        else
        {
           ilRcCnt = ValidTime(FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKBS),
                               FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKES),
                               FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKBA),
                               FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKEA));
           if (ilRcCnt != RC_SUCCESS && strcmp(pcgCountersPreDisplay,"YES") == 0)
           {
              ilRC = PutInCounterArray(FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP),
                                       FIELDVAL(rgCcaArray,pclRowCCA,igCcaFLNU),
                                       FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKIC),
                                       FIELDVAL(rgCcaArray,pclRowCCA,igCcaDISP));
           }
        }
        ilRC = CheckCcaFtyp(FIELDVAL(rgCcaArray,pclRowCCA,igCcaFLNU),
                            FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP));
        if (ilRcCnt == RC_SUCCESS && ilRC == RC_SUCCESS)
        {
           dbg(DEBUG,"RebuildCounters: CKIC = <%s><%s> must be inserted into FLD",FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKIC),FIELDVAL(rgCcaArray,pclRowCCA,igCcaURNO));
           ilRC = InitFldStruct();
           ilRC = GetNextValues(rgFldFlds.pclFldURNO,1);
           if (ilRC != RC_SUCCESS)
           {
              dbg(TRACE,"%05d: GetUrno failed !!",__LINE__);
           }
           strcpy(rgFldFlds.pclFldRNAM,FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKIC));
           strcpy(rgFldFlds.pclFldRTYP,pcgRtypCOUNTER);
           strcpy(rgFldFlds.pclFldRTAB,"CCA");
           strcpy(rgFldFlds.pclFldRURN,FIELDVAL(rgCcaArray,pclRowCCA,igCcaURNO));
           strcpy(pclBuf,FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP));
           TrimRight(pclBuf);
           if (strcmp(pclBuf,"C") == 0)
           {
              strcpy(rgFldFlds.pclFldAURN,"0");
           }
           else
           {
              strcpy(rgFldFlds.pclFldAURN,FIELDVAL(rgCcaArray,pclRowCCA,igCcaFLNU));
           }
           if (strcmp(pcgUseFLZTAB,"YES") == 0)
           {
              sprintf(pclSqlBuf,"SELECT URNO,FLGU,LNAM FROM FLZTAB WHERE RURN = %s AND UTYP = 'C'",
                      FIELDVAL(rgCcaArray,pclRowCCA,igCcaURNO));
              slFunction = START;
              slCursor = 0;
              ilRCdb = sql_if(slFunction,&slCursor,pclSqlBuf,pclData);
              close_my_cursor(&slCursor);
              if (ilRCdb == DB_SUCCESS) 
              {
                 BuildItemBuffer(pclData,"",3,",");
                 get_real_item(pclFlzUrno,pclData,1);
                 TrimRight(pclFlzUrno);
                 get_real_item(pclFlzFlgu,pclData,2);
                 TrimRight(pclFlzFlgu);
                 get_real_item(pclFlzLnam,pclData,3);
                 TrimRight(pclFlzLnam);
                 if (strncmp(pclFlzLnam,FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKIC),strlen(pclFlzLnam)) != 0)
                 {
                    sprintf(pclSelection,"WHERE URNO = %s",pclFlzUrno);
                    sprintf(pclSqlBuf,"UPDATE FLZTAB SET LNAM = '%s' %s",
                            FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKIC),pclSelection);
                    slCursor = 0;
                    slFunction = START;
                    ilRCdb = sql_if(slFunction,&slCursor,pclSqlBuf,pclData);
                    commit_work();
                    close_my_cursor(&slCursor);
                 }
                 sprintf(pclSqlBuf,"SELECT LGSN FROM FLGTAB WHERE URNO = %s",pclFlzFlgu);
                 slFunction= START;
                 slCursor = 0;
                 ilRCdb = sql_if(slFunction,&slCursor,pclSqlBuf,pclData);
                 close_my_cursor(&slCursor);
                 if (ilRCdb == DB_SUCCESS) 
                 {
                    TrimRight(pclData);
                    if (strcmp(FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP),"C") == 0)
                    {
                       if (strncmp(pclData,FIELDVAL(rgCcaArray,pclRowCCA,igCcaREMA),strlen(pclData)) != 0)
                          ilRC = CEDAArrayPutField(&rgCcaArray.rrArrayHandle,rgCcaArray.crArrayName,
                                                   NULL,"REMA",llRowNumCCA,pclData);
                    }
                    else
                    {
                       strcpy(rgFldFlds.pclFldCREC,pclData);
                    }
                 } 
              }
              else
              {
                 if (strcmp(FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP),"C") == 0)
                    ilRC = CEDAArrayPutField(&rgCcaArray.rrArrayHandle,rgCcaArray.crArrayName,NULL,
                                             "REMA",llRowNumCCA," ");
                 else
                    strcpy(rgFldFlds.pclFldCREC," ");
              }
           }
           else
              strcpy(rgFldFlds.pclFldCREC,FIELDVAL(rgCcaArray,pclRowCCA,igCcaDISP));
           strcpy(rgFldFlds.pclFldSOTI,FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKBS));
           strcpy(rgFldFlds.pclFldSCTI,FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKES));
           strcpy(rgFldFlds.pclFldDOTI,FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKBA));
           strcpy(rgFldFlds.pclFldDCTI,FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKEA));
           strcpy(rgFldFlds.pclFldSTAT,FIELDVAL(rgCcaArray,pclRowCCA,igCcaREMA));
           strcpy(rgFldFlds.pclFldTIFF," ");
           strcpy(rgFldFlds.pclFldOBLF," ");
           strcpy(rgFldFlds.pclFldSTOF," ");
           TimeToStr(rgFldFlds.pclFldLSTU,time(NULL)); 
           TimeToStr(rgFldFlds.pclFldCDAT,time(NULL));
           strcpy(rgFldFlds.pclFldUSEU,"FLDHDL");
           strcpy(rgFldFlds.pclFldUSEC,"FLDHDL");
           strcpy(rgFldFlds.pclFldHOPO,pcgHomeAp);
           if (strcmp(FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP),"C") != 0 &&
               igSelectCounterLogo == TRUE)
           {
              ilRC = GetFLNOandJFNO(FIELDVAL(rgCcaArray,pclRowCCA,igCcaFLNU),
                                    pclFldFLNO,pclFldJFNO,pclFldJCNT);
              strcpy(rgFldFlds.pclFldFLNO,pclFldFLNO);
              strcpy(rgFldFlds.pclFldJFNO,pclFldJFNO);
              strcpy(rgFldFlds.pclFldJCNT,pclFldJCNT);
           }
           ilRC = StructAddRowPartFLD(&llRowNumFLD);
           ilRC = SetDSEQ(rgFldFlds.pclFldRNAM,rgFldFlds.pclFldRTYP);
           ilPutInCounterArrayProcessed = FALSE;
           if (strcmp(FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP),"C") == 0 &&
               igProcAirlGroups == TRUE)
           {
              strcpy(pclCcaGRNU,FIELDVAL(rgCcaArray,pclRowCCA,igCcaGRNU));
              TrimRight(pclCcaGRNU);
              if (strlen(pclCcaGRNU) > 1)
              {
                 for (ilI = 0; ilI < ilNoOfGroups; ilI++)
                 {
                    if (strcmp(pclCcaGRNU,&rlGroupList[ilI].pclGroupUrno[0]) == 0)
                    {
                       for (ilJ = 0; ilJ < rlGroupList[ilI].ilNoOfGroupMembers; ilJ++)
                       {
                          ilRC = PutInCounterArray(FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP),
                                                   &rlGroupList[ilI].pclMemberUrnos[ilJ][0],
                                                   FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKIC),
                                                   FIELDVAL(rgCcaArray,pclRowCCA,igCcaDISP));
                       }
                       ilPutInCounterArrayProcessed = TRUE;
                    }
                 }
              }
           }
           if (ilPutInCounterArrayProcessed == FALSE)
           {
              ilRC = PutInCounterArray(FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP),
                                       FIELDVAL(rgCcaArray,pclRowCCA,igCcaFLNU),
                                       FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKIC),
                                       FIELDVAL(rgCcaArray,pclRowCCA,igCcaDISP));
           }
        }
     }
     llRowNumCCA = ARR_NEXT;
  }

  ilRC = ConvertCounterArray();

  return (ilRC);
}


/******************************************************************************/
/* The PutInCounterArray routine                                              */
/******************************************************************************/
static int PutInCounterArray(char *pcpCTYP, char *pcpFLNU, char *pcpCKIC, char *pcpDISP)
{
  int  ilRC = RC_SUCCESS;
  char pclCTYP[32] = "\0";
  char pclFLNU[32] = "\0";
  char pclCKIC[32] = "\0";
  char pclDISP[128] = "\0";
  int  ilCntNum = 0;
  int  ilI = 0;
  int  ilJ = 0;
  int  ilK = 0;
  int  ilFound = 0;
  int  ilFound2 = 0;
  int  ilIdx = 0;
  int ilCount;
  char pclItem[32];
  int ilCompare;

dbg(DEBUG,"CTYP = <%s> , FLNU = <%s> , CKIC = <%s> , DISP = <%s>",pcpCTYP,pcpFLNU,pcpCKIC,pcpDISP);
  strcpy(pclCTYP,pcpCTYP);
  TrimRight(pclCTYP);
  strcpy(pclFLNU,pcpFLNU);
  TrimRight(pclFLNU);
  strcpy(pclCKIC,pcpCKIC);
  TrimRight(pclCKIC);
  strcpy(pclDISP,pcpDISP);
  TrimRight(pclDISP);

  ilCount = GetNoOfElements(pcgInvalidCounterFdd,',');
  for (ilI = 1; ilI <= ilCount; ilI++)
  {
     GetDataItem(pclItem,pcgInvalidCounterFdd,ilI,',',""," ");
     if (strncmp(pclCKIC,pclItem,strlen(pclItem)) == 0)
     {
        return ilRC;
     }
  }

  if (pcpCTYP[0] == cBLANK)
  {
     dbg(DEBUG,"CTYP = <%s> , FLNU = <%s> , CKIC = <%s> , DISP = <%s>",pclCTYP,pclFLNU,pclCKIC,pclDISP);
     ilFound = 0;
     for (ilI=0; ilI<igCurNoDediCnt; ilI++)
     {
        if (strcmp(&(rgDedicatedCounters[ilI].pclDediFLNU[0]),pclFLNU) == 0)
        {
           ilFound = 1;
           if (igIntAndDomCounters != 0)
           {
              ilCntNum = atoi(pclCKIC);
              if (ilCntNum >= igFirstDom && ilCntNum <= igLastDom)
              {
                 ilIdx = rgDedicatedCounters[ilI].ilCurNoDomNam; 
                 ilFound2 = 0;
                 for (ilJ=0; ilJ<ilIdx && ilFound2==0; ilJ++)
                 {
                    dbg(DEBUG,"Dom <%s><%s> %d %d",&(rgDedicatedCounters[ilI].pclCntDom[ilJ][0]),
                                               pclCKIC,ilIdx,ilJ);
                    ilCompare = FALSE;
                    if (igAsciiCounterCompare == 1)
                    {
                       if (strcmp(&(rgDedicatedCounters[ilI].pclCntDom[ilJ][0]),pclCKIC) > 0)
                          ilCompare = TRUE;
                    }
                    else
                    {
                       if (atoi(&(rgDedicatedCounters[ilI].pclCntDom[ilJ][0])) >  atoi(pclCKIC))
                          ilCompare = TRUE;
                    }
                    if (ilCompare == TRUE)
                    {
                       ilFound2 = 1;
                       dbg(DEBUG,"Insert Dedi Dom <%s>%d,%d",pclCKIC,ilIdx,ilI);
                       for (ilK=ilIdx; ilK>ilJ; ilK--)
                       {
                          strcpy(&(rgDedicatedCounters[ilI].pclCntDom[ilK][0]),
                                 &(rgDedicatedCounters[ilI].pclCntDom[ilK-1][0]));
                       }
                       strcpy(&(rgDedicatedCounters[ilI].pclCntDom[ilJ][0]),pclCKIC);
                    }
                 }
                 if (ilFound2 == 0)
                 {
                    strcpy(&(rgDedicatedCounters[ilI].pclCntDom[ilIdx][0]),pclCKIC);
                 }
                 rgDedicatedCounters[ilI].ilCurNoDomNam++; 
              }
              else
              {
                 ilIdx = rgDedicatedCounters[ilI].ilCurNoIntNam; 
                 ilFound2 = 0;
                 for (ilJ=0; ilJ<ilIdx && ilFound2==0; ilJ++)
                 {
                    dbg(DEBUG,"Int 1 <%s><%s> %d %d",&(rgDedicatedCounters[ilI].pclCntInt[ilJ][0]),
                                               pclCKIC,ilIdx,ilJ);
                    ilCompare = FALSE;
                    if (igAsciiCounterCompare == 1)
                    {
                       if (strcmp(&(rgDedicatedCounters[ilI].pclCntInt[ilJ][0]),pclCKIC) > 0)
                          ilCompare = TRUE;
                    }
                    else
                    {
                       if (atoi(&(rgDedicatedCounters[ilI].pclCntInt[ilJ][0])) >  atoi(pclCKIC))
                          ilCompare = TRUE;
                    }
                    if (ilCompare == TRUE)
                    {
                       ilFound2 = 1;
                       dbg(DEBUG,"Insert Dedi Int <%s>%d,%d",pclCKIC,ilIdx,ilI);
                       for (ilK=ilIdx; ilK>ilJ; ilK--)
                       {
                          strcpy(&(rgDedicatedCounters[ilI].pclCntInt[ilK][0]),
                                 &(rgDedicatedCounters[ilI].pclCntInt[ilK-1][0]));
                       }
                       strcpy(&(rgDedicatedCounters[ilI].pclCntInt[ilJ][0]),pclCKIC);
                    }
                 }
                 if (ilFound2 == 0)
                 {
                    strcpy(&(rgDedicatedCounters[ilI].pclCntInt[ilIdx][0]),pclCKIC);
                 }
                 rgDedicatedCounters[ilI].ilCurNoIntNam++; 
              }
           }
           else
           {
              ilIdx = rgDedicatedCounters[ilI].ilCurNoIntNam; 
              ilFound2 = 0;
              for (ilJ=0; ilJ<ilIdx && ilFound2==0; ilJ++)
              {
                 dbg(DEBUG,"Int 2: <%s><%s> %d %d",&(rgDedicatedCounters[ilI].pclCntInt[ilJ][0]),
                                            pclCKIC,ilIdx,ilJ);
                 ilCompare = FALSE;
                 if (igAsciiCounterCompare == 1)
                 {
                    if (strcmp(&(rgDedicatedCounters[ilI].pclCntInt[ilJ][0]),pclCKIC) > 0)
                       ilCompare = TRUE;
                 }
                 else
                 {
                    if (atoi(&(rgDedicatedCounters[ilI].pclCntInt[ilJ][0])) >  atoi(pclCKIC))
                       ilCompare = TRUE;
                 }
                 if (ilCompare == TRUE)
                 {
                    ilFound2 = 1;
                    dbg(DEBUG,"Insert Dedi Int <%s>%d,%d",pclCKIC,ilIdx,ilI);
                    for (ilK=ilIdx; ilK>ilJ; ilK--)
                    {
                       strcpy(&(rgDedicatedCounters[ilI].pclCntInt[ilK][0]),
                              &(rgDedicatedCounters[ilI].pclCntInt[ilK-1][0]));
                    }
                    strcpy(&(rgDedicatedCounters[ilI].pclCntInt[ilJ][0]),pclCKIC);
                 }
              }
              if (ilFound2 == 0)
              {
                 strcpy(&(rgDedicatedCounters[ilI].pclCntInt[ilIdx][0]),pclCKIC);
              }
              rgDedicatedCounters[ilI].ilCurNoIntNam++; 
           }
        }
     }
     if (ilFound == 0)
     {
        rgDedicatedCounters[igCurNoDediCnt].ilCurNoIntNam = 0; 
        rgDedicatedCounters[igCurNoDediCnt].ilCurNoDomNam = 0; 
        strcpy(&(rgDedicatedCounters[igCurNoDediCnt].pclDediFLNU[0]),pclFLNU);
        if (igIntAndDomCounters != 0)
        {
           ilCntNum = atoi(pclCKIC);
           if (ilCntNum >= igFirstDom && ilCntNum <= igLastDom)
           {
              strcpy(&(rgDedicatedCounters[igCurNoDediCnt].pclCntDom[0][0]),pclCKIC);
              rgDedicatedCounters[igCurNoDediCnt].ilCurNoDomNam++; 
           }
           else
           {
              strcpy(&(rgDedicatedCounters[igCurNoDediCnt].pclCntInt[0][0]),pclCKIC);
              rgDedicatedCounters[igCurNoDediCnt].ilCurNoIntNam++; 
           }
        }
        else
        {
           strcpy(&(rgDedicatedCounters[igCurNoDediCnt].pclCntInt[0][0]),pclCKIC);
           rgDedicatedCounters[igCurNoDediCnt].ilCurNoIntNam++; 
        }
        igCurNoDediCnt++;
     }
     else
     {
     }
  }
  else
  {
     dbg(DEBUG,"CTYP = <%s> , FLNU = <%s> , CKIC = <%s> , DISP = <%s>",pclCTYP,pclFLNU,pclCKIC,pclDISP);
     ilFound = 0;
     for (ilI=0; ilI<igCurNoComCnt; ilI++)
     {
        if (strcmp(&(rgCommonCounters[ilI].pclComFLNU[0]),pclFLNU) == 0)
        {
           ilFound = 1;
           if (igIntAndDomCounters != 0)
           {
              ilCntNum = atoi(pclCKIC);
              if (ilCntNum >= igFirstDom && ilCntNum <= igLastDom)
              {
                 ilIdx = rgCommonCounters[ilI].ilCurNoDomNam; 
                 ilFound2 = 0;
                 for (ilJ=0; ilJ<ilIdx && ilFound2==0; ilJ++)
                 {
                    dbg(DEBUG,"<%s><%s> %d %d",&(rgCommonCounters[ilI].pclCntDom[ilJ][0]),
                                               pclCKIC,ilIdx,ilJ);
                    ilCompare = FALSE;
                    if (igAsciiCounterCompare == 1)
                    {
                       if (strcmp(&(rgCommonCounters[ilI].pclCntDom[ilJ][0]),pclCKIC) > 0)
                          ilCompare = TRUE;
                    }
                    else
                    {
                       if (atoi(&(rgCommonCounters[ilI].pclCntDom[ilJ][0])) >  atoi(pclCKIC))
                          ilCompare = TRUE;
                    }
                    if (ilCompare == TRUE)
                    {
                       ilFound2 = 1;
                       dbg(DEBUG,"Insert Com Dom <%s>%d,%d",pclCKIC,ilIdx,ilI);
                       for (ilK=ilIdx; ilK>ilJ; ilK--)
                       {
                          strcpy(&(rgCommonCounters[ilI].pclCntDom[ilK][0]),
                                 &(rgCommonCounters[ilI].pclCntDom[ilK-1][0]));
                          strcpy(&(rgCommonCounters[ilI].pclCntDomDisp[ilK][0]),
                                 &(rgCommonCounters[ilI].pclCntDomDisp[ilK-1][0]));
                       }
                       strcpy(&(rgCommonCounters[ilI].pclCntDom[ilJ][0]),pclCKIC);
                       strcpy(&(rgCommonCounters[ilI].pclCntDomDisp[ilJ][0]),pclDISP);
                    }
                 }
                 if (ilFound2 == 0)
                 {
                    strcpy(&(rgCommonCounters[ilI].pclCntDom[ilIdx][0]),pclCKIC);
                    strcpy(&(rgCommonCounters[ilI].pclCntDomDisp[ilIdx][0]),pclDISP);
                 }
                 rgCommonCounters[ilI].ilCurNoDomNam++; 
              }
              else
              {
                 ilIdx = rgCommonCounters[ilI].ilCurNoIntNam; 
                 ilFound2 = 0;
                 for (ilJ=0; ilJ<ilIdx && ilFound2==0; ilJ++)
                 {
                    dbg(DEBUG,"<%s><%s> %d %d",&(rgCommonCounters[ilI].pclCntInt[ilJ][0]),
                                               pclCKIC,ilIdx,ilJ);
                    ilCompare = FALSE;
                    if (igAsciiCounterCompare == 1)
                    {
                       if (strcmp(&(rgCommonCounters[ilI].pclCntInt[ilJ][0]),pclCKIC) > 0)
                          ilCompare = TRUE;
                    }
                    else
                    {
                       if (atoi(&(rgCommonCounters[ilI].pclCntInt[ilJ][0])) >  atoi(pclCKIC))
                          ilCompare = TRUE;
                    }
                    if (ilCompare == TRUE)
                    {
                       ilFound2 = 1;
                       dbg(DEBUG,"Insert Com Int <%s>%d,%d",pclCKIC,ilIdx,ilI);
                       for (ilK=ilIdx; ilK>ilJ; ilK--)
                       {
                          strcpy(&(rgCommonCounters[ilI].pclCntInt[ilK][0]),
                                 &(rgCommonCounters[ilI].pclCntInt[ilK-1][0]));
                          strcpy(&(rgCommonCounters[ilI].pclCntIntDisp[ilK][0]),
                                 &(rgCommonCounters[ilI].pclCntIntDisp[ilK-1][0]));
                       }
                       strcpy(&(rgCommonCounters[ilI].pclCntInt[ilJ][0]),pclCKIC);
                       strcpy(&(rgCommonCounters[ilI].pclCntIntDisp[ilJ][0]),pclDISP);
                    }
                 }
                 if (ilFound2 == 0)
                 {
                    strcpy(&(rgCommonCounters[ilI].pclCntInt[ilIdx][0]),pclCKIC);
                    strcpy(&(rgCommonCounters[ilI].pclCntIntDisp[ilIdx][0]),pclDISP);
                 }
                 rgCommonCounters[ilI].ilCurNoIntNam++; 
              }
           }
           else
           {
              ilIdx = rgCommonCounters[ilI].ilCurNoIntNam; 
              ilFound2 = 0;
              for (ilJ=0; ilJ<ilIdx && ilFound2==0; ilJ++)
              {
                 dbg(DEBUG,"<%s><%s> %d %d",&(rgCommonCounters[ilI].pclCntInt[ilJ][0]),
                                            pclCKIC,ilIdx,ilJ);
                 ilCompare = FALSE;
                 if (igAsciiCounterCompare == 1)
                 {
                    if (strcmp(&(rgCommonCounters[ilI].pclCntInt[ilJ][0]),pclCKIC) > 0)
                       ilCompare = TRUE;
                 }
                 else
                 {
                    if (atoi(&(rgCommonCounters[ilI].pclCntInt[ilJ][0])) >  atoi(pclCKIC))
                       ilCompare = TRUE;
                 }
                 if (ilCompare == TRUE)
                 {
                    ilFound2 = 1;
                    dbg(DEBUG,"Insert Com Int <%s>%d,%d",pclCKIC,ilIdx,ilI);
                    for (ilK=ilIdx; ilK>ilJ; ilK--)
                    {
                       strcpy(&(rgCommonCounters[ilI].pclCntInt[ilK][0]),
                              &(rgCommonCounters[ilI].pclCntInt[ilK-1][0]));
                       strcpy(&(rgCommonCounters[ilI].pclCntIntDisp[ilK][0]),
                              &(rgCommonCounters[ilI].pclCntIntDisp[ilK-1][0]));
                    }
                    strcpy(&(rgCommonCounters[ilI].pclCntInt[ilJ][0]),pclCKIC);
                    strcpy(&(rgCommonCounters[ilI].pclCntIntDisp[ilJ][0]),pclDISP);
                 }
              }
              if (ilFound2 == 0)
              {
                 strcpy(&(rgCommonCounters[ilI].pclCntInt[ilIdx][0]),pclCKIC);
                 strcpy(&(rgCommonCounters[ilI].pclCntIntDisp[ilIdx][0]),pclDISP);
              }
              rgCommonCounters[ilI].ilCurNoIntNam++; 
           }
        }
     }
     if (ilFound == 0)
     {
        rgCommonCounters[igCurNoComCnt].ilCurNoIntNam = 0; 
        rgCommonCounters[igCurNoComCnt].ilCurNoDomNam = 0; 
        strcpy(&(rgCommonCounters[igCurNoComCnt].pclComFLNU[0]),pclFLNU);
        if (igIntAndDomCounters != 0)
        {
           ilCntNum = atoi(pclCKIC);
           if (ilCntNum >= igFirstDom && ilCntNum <= igLastDom)
           {
              strcpy(&(rgCommonCounters[igCurNoComCnt].pclCntDom[0][0]),pclCKIC);
              strcpy(&(rgCommonCounters[igCurNoComCnt].pclCntDomDisp[0][0]),pclDISP);
              rgCommonCounters[igCurNoComCnt].ilCurNoDomNam++; 
           }
           else
           {
              strcpy(&(rgCommonCounters[igCurNoComCnt].pclCntInt[0][0]),pclCKIC);
              strcpy(&(rgCommonCounters[igCurNoComCnt].pclCntIntDisp[0][0]),pclDISP);
              rgCommonCounters[igCurNoComCnt].ilCurNoIntNam++; 
           }
        }
        else
        {
           strcpy(&(rgCommonCounters[igCurNoComCnt].pclCntInt[0][0]),pclCKIC);
           strcpy(&(rgCommonCounters[igCurNoComCnt].pclCntIntDisp[0][0]),pclDISP);
           rgCommonCounters[igCurNoComCnt].ilCurNoIntNam++; 
        }
        igCurNoComCnt++;
     }
     else
     {
     }
  }

  return (ilRC);
}


/******************************************************************************/
/* The ConvertCounterArray routine                                            */
/******************************************************************************/
static int ConvertCounterArray()
{
  int ilRC = RC_SUCCESS;
  int ilI;
  int ilJ;
  int ilFound;
  char pclAftURNO[32] = "\0";
  char pclAftFLNO[32] = "\0";
  char pclAltURNO[32] = "\0";
  char pclAltALC2[32] = "\0";
  char pclAltALC3[32] = "\0";
  long llRowNumAFT = ARR_FIRST;
  char *pclRowAFT = NULL;
  long llRowNumALT = ARR_FIRST;
  char *pclRowALT = NULL;

  for (ilI=0; ilI<igCurNoComCnt; ilI++)
  {
     ilFound = 0;
     llRowNumALT = ARR_FIRST;
     while (ilFound == 0 && CEDAArrayGetRowPointer(&rgAltArray.rrArrayHandle,
                                                   rgAltArray.crArrayName,
                                                   llRowNumALT,(void *)&pclRowALT) == RC_SUCCESS)
     {
        strcpy(pclAltURNO,FIELDVAL(rgAltArray,pclRowALT,igAltURNO));
        TrimRight(pclAltURNO);
        if (strcmp(pclAltURNO,&(rgCommonCounters[ilI].pclComFLNU[0])) == 0)
        {
           ilFound = 1;
           strcpy(pclAltALC2,FIELDVAL(rgAltArray,pclRowALT,igAltALC2));
           TrimRight(pclAltALC2);
           strcpy(pclAltALC3,FIELDVAL(rgAltArray,pclRowALT,igAltALC3));
           TrimRight(pclAltALC3);
           strcpy(&(rgCommonCounters[ilI].pclComALC2[0]),pclAltALC2);
           strcpy(&(rgCommonCounters[ilI].pclComALC3[0]),pclAltALC3);
        }
        else
        {
           llRowNumALT = ARR_NEXT;
        }
     }
  }
/*AKLTEST*/
/*
dbg(TRACE,"Before");
for (ilI=0; ilI<igCurNoDediCnt; ilI++)
{
   dbg(TRACE,"<%s><%s><%s><%s><%s><%s><%s><%s><%d>",&(rgDedicatedCounters[ilI].pclDediFLNO[0]),
                                                    &(rgDedicatedCounters[ilI].pclDediFLNU[0]),
                                                    &(rgDedicatedCounters[ilI].pclCntInt[0][0]),
                                                    &(rgDedicatedCounters[ilI].pclCntInt[1][0]),
                                                    &(rgDedicatedCounters[ilI].pclCntInt[2][0]),
                                                    &(rgDedicatedCounters[ilI].pclCntInt[3][0]),
                                                    &(rgDedicatedCounters[ilI].pclCntInt[4][0]),
                                                    &(rgDedicatedCounters[ilI].pclCntInt[5][0]),
                                                    rgDedicatedCounters[ilI].ilCurNoIntNam);
}
*/
/*AKLTEST*/

  for (ilI=0; ilI<igCurNoDediCnt; ilI++)
  {
     ilFound = 0;
     llRowNumAFT = ARR_FIRST;
     while (ilFound == 0 && CEDAArrayGetRowPointer(&rgAftArray.rrArrayHandle,
                                                   rgAftArray.crArrayName,
                                                   llRowNumAFT,(void *)&pclRowAFT) == RC_SUCCESS)
     {
        strcpy(pclAftURNO,FIELDVAL(rgAftArray,pclRowAFT,igAftURNO));
        TrimRight(pclAftURNO);
        if (strcmp(pclAftURNO,&(rgDedicatedCounters[ilI].pclDediFLNU[0])) == 0)
        {
           ilFound = 1;
           strcpy(pclAftFLNO,FIELDVAL(rgAftArray,pclRowAFT,igAftFLNO));
           TrimRight(pclAftFLNO);
           strcpy(&(rgDedicatedCounters[ilI].pclDediFLNO[0]),pclAftFLNO);
        }
        else
        {
           llRowNumAFT = ARR_NEXT;
        }
     }
  }
/*AKLTEST*/
/*
dbg(TRACE,"After");
for (ilI=0; ilI<igCurNoDediCnt; ilI++)
{
   dbg(TRACE,"<%s><%s><%s><%s><%s><%s><%s><%s><%d>",&(rgDedicatedCounters[ilI].pclDediFLNO[0]),
                                                    &(rgDedicatedCounters[ilI].pclDediFLNU[0]),
                                                    &(rgDedicatedCounters[ilI].pclCntInt[0][0]),
                                                    &(rgDedicatedCounters[ilI].pclCntInt[1][0]),
                                                    &(rgDedicatedCounters[ilI].pclCntInt[2][0]),
                                                    &(rgDedicatedCounters[ilI].pclCntInt[3][0]),
                                                    &(rgDedicatedCounters[ilI].pclCntInt[4][0]),
                                                    &(rgDedicatedCounters[ilI].pclCntInt[5][0]),
                                                    rgDedicatedCounters[ilI].ilCurNoIntNam);
}
*/
/*AKLTEST*/

  for (ilI=0; ilI<igCurNoComCnt; ilI++)
  {
     ilRC = GetCommonCodeShares(ilI);
  }
  for (ilI=0; ilI<igCurNoComCnt; ilI++)
  {
     dbg(DEBUG,"Common Counters: FLNU = <%s> , ALC2 = <%s> , ALC3 = <%s>",
         &(rgCommonCounters[ilI].pclComFLNU[0]),
         &(rgCommonCounters[ilI].pclComALC2[0]),
         &(rgCommonCounters[ilI].pclComALC3[0]));
     for (ilJ=0; ilJ<rgCommonCounters[ilI].ilCurNoIntNam; ilJ++)
     {
        if (igSuppressCntZero == 1)
        {
           if (rgCommonCounters[ilI].pclCntInt[ilJ][0] == '0')
           {
              rgCommonCounters[ilI].pclCntInt[ilJ][0] = rgCommonCounters[ilI].pclCntInt[ilJ][1];
              rgCommonCounters[ilI].pclCntInt[ilJ][1] = '\0';
           }
        }
        dbg(DEBUG,"                  (I) = <%s>",&(rgCommonCounters[ilI].pclCntInt[ilJ][0]));
     }
     for (ilJ=0; ilJ<rgCommonCounters[ilI].ilCurNoDomNam; ilJ++)
     {
        if (igSuppressCntZero == 1)
        {
           if (rgCommonCounters[ilI].pclCntDom[ilJ][0] == '0')
           {
              rgCommonCounters[ilI].pclCntDom[ilJ][0] = rgCommonCounters[ilI].pclCntDom[ilJ][1];
              rgCommonCounters[ilI].pclCntDom[ilJ][1] = '\0';
           }
        }
        dbg(DEBUG,"                  (D) = <%s>",&(rgCommonCounters[ilI].pclCntDom[ilJ][0]));
     }
  }
  for (ilI=0; ilI<igCurNoDediCnt; ilI++)
  {
     dbg(DEBUG,"Dedicated Counters: FLNU = <%s> , FLNO = <%s>",
         &(rgDedicatedCounters[ilI].pclDediFLNU[0]),
         &(rgDedicatedCounters[ilI].pclDediFLNO[0]));
     for (ilJ=0; ilJ<rgDedicatedCounters[ilI].ilCurNoIntNam; ilJ++)
     {
        if (igSuppressCntZero == 1)
        {
           if (rgDedicatedCounters[ilI].pclCntInt[ilJ][0] == '0')
           {
              rgDedicatedCounters[ilI].pclCntInt[ilJ][0] = rgDedicatedCounters[ilI].pclCntInt[ilJ][1];
              rgDedicatedCounters[ilI].pclCntInt[ilJ][1] = '\0';
           }
        }
        dbg(DEBUG,"                     (I) = <%s>",&(rgDedicatedCounters[ilI].pclCntInt[ilJ][0]));
     }
     for (ilJ=0; ilJ<rgDedicatedCounters[ilI].ilCurNoDomNam; ilJ++)
     {
        if (igSuppressCntZero == 1)
        {
           if (rgDedicatedCounters[ilI].pclCntDom[ilJ][0] == '0')
           {
              rgDedicatedCounters[ilI].pclCntDom[ilJ][0] = rgDedicatedCounters[ilI].pclCntDom[ilJ][1];
              rgDedicatedCounters[ilI].pclCntDom[ilJ][1] = '\0';
           }
        }
        dbg(DEBUG,"                     (D) = <%s>",&(rgDedicatedCounters[ilI].pclCntDom[ilJ][0]));
     }
  }

  return (ilRC);
}


/******************************************************************************/
/* The GetCommonCodeShares routine                                            */
/******************************************************************************/
static int GetCommonCodeShares(int ilI)
{
  int ilRC = RC_SUCCESS;
  int ilJ, ilK, ilL, ilM, ilP;
  int ilFound, ilFound1;
  char pclAltURNO[32] = "\0";
  char pclAltALC2[32] = "\0";
  char pclAltALC3[32] = "\0";
  long llRowNumALT = ARR_FIRST;
  char *pclRowALT = NULL;

  if (igSec4End > 0)
  {
     prguComCodeShare = prgComCodeShare;
     for (ilJ=0; ilJ<igSec4End; ilJ++)
     {
        if (strcmp(&(rgCommonCounters[ilI].pclComALC2[0]),
                   prguComCodeShare->MainAirline) == 0 ||
            strcmp(&(rgCommonCounters[ilI].pclComALC3[0]),
                   prguComCodeShare->MainAirline) == 0)
        {
           for (ilK=0; ilK<prguComCodeShare->ilCntCS; ilK++)
           {
              ilFound = 0;
              for (ilL=0; ilL<igCurNoComCnt; ilL++)
              {
                 if (strcmp(&(rgCommonCounters[ilL].pclComALC2[0]),
                            &(prguComCodeShare->CodeShare[ilK][0])) == 0 ||
                     strcmp(&(rgCommonCounters[ilL].pclComALC3[0]),
                            &(prguComCodeShare->CodeShare[ilK][0])) == 0)
                 {
                    ilFound = 1;
                    /* copy counters */
                    ilP = rgCommonCounters[ilL].ilCurNoIntNam;
                    rgCommonCounters[ilL].ilCurNoIntNam += rgCommonCounters[ilI].ilCurNoIntNam;
                    for (ilM=0; ilM< rgCommonCounters[ilI].ilCurNoIntNam; ilM++)
                    {
                       strcpy(&(rgCommonCounters[ilL].pclCntInt[ilP+ilM][0]),
                              &(rgCommonCounters[ilI].pclCntInt[ilM][0]));
                       strcpy(&(rgCommonCounters[ilL].pclCntIntDisp[ilP+ilM][0]),
                              &(rgCommonCounters[ilI].pclCntIntDisp[ilM][0]));
                    }
                    ilP = rgCommonCounters[ilL].ilCurNoDomNam;
                    rgCommonCounters[ilL].ilCurNoDomNam += rgCommonCounters[ilI].ilCurNoDomNam;
                    for (ilM=0; ilM< rgCommonCounters[ilI].ilCurNoDomNam; ilM++)
                    {
                       strcpy(&(rgCommonCounters[ilL].pclCntDom[ilP+ilM][0]),
                              &(rgCommonCounters[ilI].pclCntDom[ilM][0]));
                       strcpy(&(rgCommonCounters[ilL].pclCntDomDisp[ilP+ilM][0]),
                              &(rgCommonCounters[ilI].pclCntDomDisp[ilM][0]));
                    }
                    ilRC = SortCounters(ilL);
                 }
              }
              if (ilFound == 0)
              {
                 /* insert code share */
                 ilL = igCurNoComCnt;
                 ilFound1 = 0;
                 llRowNumALT = ARR_FIRST;
                 while (ilFound1 == 0 && 
                        CEDAArrayGetRowPointer(&rgAltArray.rrArrayHandle,
                                               rgAltArray.crArrayName,
                                               llRowNumALT,(void *)&pclRowALT) == RC_SUCCESS)
                 {
                    strcpy(pclAltURNO,FIELDVAL(rgAltArray,pclRowALT,igAltURNO));
                    TrimRight(pclAltURNO);
                    strcpy(pclAltALC2,FIELDVAL(rgAltArray,pclRowALT,igAltALC2));
                    TrimRight(pclAltALC2);
                    strcpy(pclAltALC3,FIELDVAL(rgAltArray,pclRowALT,igAltALC3));
                    TrimRight(pclAltALC3);
                    if (strcmp(pclAltALC2,&(prguComCodeShare->CodeShare[ilK][0])) == 0 ||
                        strcmp(pclAltALC2,&(prguComCodeShare->CodeShare[ilK][0])) == 0)
                    {
                       ilFound1 = 1;
                       strcpy(&(rgCommonCounters[ilL].pclComFLNU[0]),pclAltURNO);
                       strcpy(&(rgCommonCounters[ilL].pclComALC2[0]),pclAltALC2);
                       strcpy(&(rgCommonCounters[ilL].pclComALC3[0]),pclAltALC3);
                    }
                    else
                    {
                       llRowNumALT = ARR_NEXT;
                    }
                 }
                 rgCommonCounters[ilL].ilCurNoIntNam = rgCommonCounters[ilI].ilCurNoIntNam;
                 for (ilM=0; ilM< rgCommonCounters[ilI].ilCurNoIntNam; ilM++)
                 {
                    strcpy(&(rgCommonCounters[ilL].pclCntInt[ilM][0]),
                           &(rgCommonCounters[ilI].pclCntInt[ilM][0]));
                    strcpy(&(rgCommonCounters[ilL].pclCntIntDisp[ilM][0]),
                           &(rgCommonCounters[ilI].pclCntIntDisp[ilM][0]));
                 }
                 rgCommonCounters[ilL].ilCurNoDomNam = rgCommonCounters[ilI].ilCurNoDomNam;
                 for (ilM=0; ilM< rgCommonCounters[ilI].ilCurNoDomNam; ilM++)
                 {
                    strcpy(&(rgCommonCounters[ilL].pclCntDom[ilM][0]),
                           &(rgCommonCounters[ilI].pclCntDom[ilM][0]));
                    strcpy(&(rgCommonCounters[ilL].pclCntDomDisp[ilM][0]),
                           &(rgCommonCounters[ilI].pclCntDomDisp[ilM][0]));
                 }
                 igCurNoComCnt++;
              }
           }
        }
        prguComCodeShare++;
     }
  }

  return(ilRC);
}


/******************************************************************************/
/* The SortCounters routine                                            */
/******************************************************************************/
static int SortCounters(int ilI)
{
  int ilRC = RC_SUCCESS;
  int ilJ;
  char pclCounterName1[16];
  char pclCounterName2[16];
  char pclCounterDisp1[16];
  char pclCounterDisp2[16];
  int ilCounterName1;
  int ilCounterName2;
  int ilChangeDone = 0;

  if (rgCommonCounters[ilI].ilCurNoIntNam > 0)
  {
     strcpy(pclCounterName1,&(rgCommonCounters[ilI].pclCntInt[0][0]));
     strcpy(pclCounterDisp1,&(rgCommonCounters[ilI].pclCntIntDisp[0][0]));
     for (ilJ=1; ilJ<rgCommonCounters[ilI].ilCurNoIntNam; ilJ++)
     {
        strcpy(pclCounterName2,&(rgCommonCounters[ilI].pclCntInt[ilJ][0]));
        strcpy(pclCounterDisp2,&(rgCommonCounters[ilI].pclCntIntDisp[ilJ][0]));
        ilCounterName1 = atoi(pclCounterName1);
        ilCounterName2 = atoi(pclCounterName2);
        if (ilCounterName1 > ilCounterName2)
        {
           strcpy(&(rgCommonCounters[ilI].pclCntInt[ilJ-1][0]),pclCounterName2);
           strcpy(&(rgCommonCounters[ilI].pclCntInt[ilJ][0]),pclCounterName1);
           strcpy(&(rgCommonCounters[ilI].pclCntIntDisp[ilJ-1][0]),pclCounterDisp2);
           strcpy(&(rgCommonCounters[ilI].pclCntIntDisp[ilJ][0]),pclCounterDisp1);
           ilChangeDone = 1;
        }
        else
        {
           if (ilCounterName1 == ilCounterName2)
           {
              strcpy(&(rgCommonCounters[ilI].pclCntInt[ilJ][0]),
                     &(rgCommonCounters[ilI].pclCntInt[rgCommonCounters[ilI].ilCurNoIntNam-1][0]));
              strcpy(&(rgCommonCounters[ilI].pclCntIntDisp[ilJ][0]),
                     &(rgCommonCounters[ilI].pclCntIntDisp[rgCommonCounters[ilI].ilCurNoIntNam-1][0]));
              rgCommonCounters[ilI].ilCurNoIntNam--;
              ilChangeDone = 1;
           }
        }
        strcpy(pclCounterName1,&(rgCommonCounters[ilI].pclCntInt[ilJ][0]));
        strcpy(pclCounterDisp1,&(rgCommonCounters[ilI].pclCntIntDisp[ilJ][0]));
     }
  }
  if (rgCommonCounters[ilI].ilCurNoDomNam > 0)
  {
     strcpy(pclCounterName1,&(rgCommonCounters[ilI].pclCntDom[0][0]));
     strcpy(pclCounterDisp1,&(rgCommonCounters[ilI].pclCntDomDisp[0][0]));
     for (ilJ=1; ilJ<rgCommonCounters[ilI].ilCurNoDomNam; ilJ++)
     {
        strcpy(pclCounterName2,&(rgCommonCounters[ilI].pclCntDom[ilJ][0]));
        strcpy(pclCounterDisp2,&(rgCommonCounters[ilI].pclCntDomDisp[ilJ][0]));
        ilCounterName1 = atoi(pclCounterName1);
        ilCounterName2 = atoi(pclCounterName2);
        if (ilCounterName1 > ilCounterName2)
        {
           strcpy(&(rgCommonCounters[ilI].pclCntDom[ilJ-1][0]),pclCounterName2);
           strcpy(&(rgCommonCounters[ilI].pclCntDom[ilJ][0]),pclCounterName1);
           strcpy(&(rgCommonCounters[ilI].pclCntDomDisp[ilJ-1][0]),pclCounterDisp2);
           strcpy(&(rgCommonCounters[ilI].pclCntDomDisp[ilJ][0]),pclCounterDisp1);
           ilChangeDone = 1;
        }
        else
        {
           if (ilCounterName1 == ilCounterName2)
           {
              strcpy(&(rgCommonCounters[ilI].pclCntDom[ilJ][0]),
                     &(rgCommonCounters[ilI].pclCntDom[rgCommonCounters[ilI].ilCurNoDomNam-1][0]));
              strcpy(&(rgCommonCounters[ilI].pclCntDomDisp[ilJ][0]),
                     &(rgCommonCounters[ilI].pclCntDomDisp[rgCommonCounters[ilI].ilCurNoDomNam-1][0]));
              rgCommonCounters[ilI].ilCurNoDomNam--;
              ilChangeDone = 1;
           }
        }
        strcpy(pclCounterName1,&(rgCommonCounters[ilI].pclCntDom[ilJ][0]));
        strcpy(pclCounterDisp1,&(rgCommonCounters[ilI].pclCntDomDisp[ilJ][0]));
     }
  }

  if (ilChangeDone == 1)
  {
     ilRC = SortCounters(ilI);
  }

  return(ilRC);
}


static int CheckForUnknownDelays()
{
  int ilRC = RC_SUCCESS;
  long llRowNumFDD = ARR_FIRST;
  char *pclRowFDD = NULL;
  char pclFddREMP[16];
  char pclFddAURN[16];
  char pclFddADID[16];
  char pclFddSTOF[16];
  char pclSqlBuf[128];
  char pclNewTime[32];

  if (strcmp(pcgUnknownDelayRemark,"XXX") == 0)
  {
     dbg(TRACE,"Unknown Delay not defined");
     return(ilRC);
  }

  llRowNumFDD = ARR_FIRST;
  while(CEDAArrayGetRowPointer(&rgFddArray.rrArrayHandle,
                               rgFddArray.crArrayName,
                               llRowNumFDD,(void *)&pclRowFDD) == RC_SUCCESS)
  {
     llRowNumFDD = ARR_CURRENT;
     strcpy(pclFddREMP,FIELDVAL(rgFddArray,pclRowFDD,igFddREMP));
     TrimRight(pclFddREMP);
     if (strstr(pcgUnknownDelayRemark,pclFddREMP) != NULL)
     {
        strcpy(pclFddAURN,FIELDVAL(rgFddArray,pclRowFDD,igFddAURN));
        TrimRight(pclFddAURN);
        strcpy(pclFddADID,FIELDVAL(rgFddArray,pclRowFDD,igFddADID));
        TrimRight(pclFddADID);
        strcpy(pclFddSTOF,FIELDVAL(rgFddArray,pclRowFDD,igFddSTOF));
        TrimRight(pclFddSTOF);
        TimeToStr(pclNewTime,time(NULL));
        if (strcmp(pclNewTime,pclFddSTOF) < 0)
        {
           strcpy(pclNewTime,pclFddSTOF);
        }
        if (strcmp(pclFddADID,"A") == 0)
        {
           AddSecondsToCEDATime12(pclNewTime,igUnknownDelayTimeArr*60,1);
        }
        else
        {
           AddSecondsToCEDATime12(pclNewTime,igUnknownDelayTimeDep*60,1);
        }
        sprintf(pclSqlBuf,"WHERE URNO = %s",pclFddAURN);
        if (strcmp(pclFddADID,"A") == 0)
        {
           ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","ETAE",pcgTwEnd,pclSqlBuf,
                            "ETAE",pclNewTime,NULL,0);
           dbg(TRACE,"ETAE for AFT = <%s> set to <%s>",pclFddAURN,pclNewTime);
        }
        else
        {
           ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","ETDE",pcgTwEnd,pclSqlBuf,
                            "ETDE",pclNewTime,NULL,0);
           dbg(TRACE,"ETDE for AFT = <%s> set to <%s>",pclFddAURN,pclNewTime);
        }
     }
     llRowNumFDD = ARR_NEXT;
  }

  return(ilRC);
}

static int GetFLNOandJFNO(char *pcpFlnu, char *pcpFlno, char *pcpJfno, char *pcpJcnt)
{
  int ilRC = RC_SUCCESS;
  long llRowNumAFT = ARR_FIRST;
  char *pclRowAFT = NULL;
  int ilFound = FALSE;
  char pclFldFLNU[16];
  char pclAftURNO[16];
  char pclAftTTYP[16];
  char pclAftFLNO[128];
  char pclAftJFNO[128];
  char pclAftJCNT[16];
  CNTDISP *prlCntDisp = NULL;
  int ilI;
  char pclLogo[16];
  char pclTmpBuf[16];

  strcpy(pcpFlno," ");
  strcpy(pcpJfno," ");
  strcpy(pcpJcnt," ");

  ilFound = FALSE;
  strcpy(pclFldFLNU,pcpFlnu);
  TrimRight(pclFldFLNU);
  llRowNumAFT = ARR_FIRST;
  while (ilFound == FALSE && 
         CEDAArrayGetRowPointer(&rgAftArray.rrArrayHandle,
                                rgAftArray.crArrayName,
                                llRowNumAFT,(void *)&pclRowAFT) == RC_SUCCESS)
  {
     llRowNumAFT = ARR_CURRENT;
     strcpy(pclAftURNO,FIELDVAL(rgAftArray,pclRowAFT,igAftURNO));
     TrimRight(pclAftURNO);
     if (strcmp(pclFldFLNU,pclAftURNO) == 0)
     {
        strcpy(pclAftFLNO,FIELDVAL(rgAftArray,pclRowAFT,igAftFLNO));
        TrimRight(pclAftFLNO);
        strcpy(pclAftJFNO,FIELDVAL(rgAftArray,pclRowAFT,igAftJFNO));
        TrimRight(pclAftJFNO);
        strcpy(pclAftJCNT,FIELDVAL(rgAftArray,pclRowAFT,igAftJCNT));
        TrimRight(pclAftJCNT);
        strcpy(pclAftTTYP,FIELDVAL(rgAftArray,pclRowAFT,igAftTTYP));
        TrimRight(pclAftTTYP);
        ilI = 0;
        prlCntDisp = prgCntDisp;
        while (ilI < igSec1End)  
        {
           if (strncmp(prlCntDisp->Section,pclAftTTYP,strlen(prlCntDisp->Section)) == 0)
           {
              strcpy(pclLogo,prlCntDisp->LOGO);
              ilI = igSec1End;
              ilRC = get_real_item(pclTmpBuf,pclLogo,1);
              if (strcmp(pclTmpBuf,"X") == 0)
              {
                 strcpy(pcpFlno,pclAftFLNO);
                 strcpy(pcpJfno,pclAftJFNO);
                 strcpy(pcpJcnt,pclAftJCNT);
              }
              else
              {
                 ilRC = get_real_item(pclTmpBuf,pclLogo,2);
                 if (strcmp(pclTmpBuf,"X") == 0)
                 {
                    if (strlen(pclAftJFNO) > 1)
                    {
                       strncpy(pcpFlno,pclAftJFNO,9);
                       pcpFlno[9] = '\0';
                       strcpy(pcpJfno," ");
                       strcpy(pcpJcnt,"0");
                    }
                    else
                    {
                       strcpy(pcpFlno,pclAftFLNO);
                       strcpy(pcpJfno,pclAftJFNO);
                       strcpy(pcpJcnt,pclAftJCNT);
                    }
                 }
                 else
                 {
                    ilRC = get_real_item(pclTmpBuf,pclLogo,3);
                    if (strcmp(pclTmpBuf,"X") == 0)
                    {
                       if (strlen(pclAftJFNO) > 9)
                       {
                          strncpy(pcpFlno,&pclAftJFNO[9],9);
                          pcpFlno[9] = '\0';
                          strcpy(pcpJfno," ");
                          strcpy(pcpJcnt,"0");
                       }
                       else
                       {
                          strcpy(pcpFlno,pclAftFLNO);
                          strcpy(pcpJfno,pclAftJFNO);
                          strcpy(pcpJcnt,pclAftJCNT);
                       }
                    }
                    else
                    {
                       if (strlen(pclAftJFNO) > 18)
                       {
                          strncpy(pcpFlno,&pclAftJFNO[18],9);
                          pcpFlno[9] = '\0';
                          strcpy(pcpJfno," ");
                          strcpy(pcpJcnt,"0");
                       }
                       else
                       {
                          strcpy(pcpFlno,pclAftFLNO);
                          strcpy(pcpJfno,pclAftJFNO);
                          strcpy(pcpJcnt,pclAftJCNT);
                       }
                    }
                 }
              }
           }
           prlCntDisp++;
           ilI++;
        }
        ilFound = TRUE;
     }
     llRowNumAFT = ARR_NEXT;
  }

  return(ilRC);
} /* End of GetFLNOAndJFNO */



/******************************************************************************/
/* The HandleTerminal_A routine                                                    */
/******************************************************************************/
static int HandleTerminal_A (char *pcpCurRowAFT, char *pcpRemark, int *ipTerm)
{
  int ilRC = RC_SUCCESS;
  char pclBuf[128];
  char pclBufTMB1[128];
  char pclBufTET1[128];

  *ipTerm = 1;
  if (igGenerateTM2 == 1)
  {
     /*********** get TMB1 of CurrentRow****************/
     strcpy(pclBufTMB1,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftTMB1));
     TrimRight(pclBufTMB1);
     strcpy(pclBufTET1,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftTET1));
     TrimRight(pclBufTET1);
     if (strcmp(pclBufTMB1,pcgDomTerm) == 0 || strcmp(pclBufTET1,pcgDomTerm) == 0)       
     {
        *ipTerm = 2;
        if (strncmp(pcpRemark,pcgCanceled,3) != 0)
           strcpy(pcpRemark,"TM2");
     }
  }

  return (ilRC);
} /*end of HandleTerminal_A */



/******************************************************************************/
/* The HandleTerminal_D routine                                                    */
/******************************************************************************/
static int HandleTerminal_D (char *pcpCurRowAFT, char *pcpRemark, int *ipTerm1, int *ipTerm2)
{
  int ilRC = RC_SUCCESS;
  char pclBuf[128];
  char pclAftURNO[128];
  long llRowNumCCA = ARR_FIRST;
  char *pclRowCCA = NULL;
  char pclCcaFLNU[128];
  char pclCcaCKIC[128];
  char pclCcaCTYP[128];
  int ilFound;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[1024];
  char pclDataBuf[1024];

  *ipTerm1 = 1;
  *ipTerm2 = 0;
  if (igGenerateTM2 == 1 || strcmp(pcgCheckCounterOnly,"YES") == 0)
  {
     if (strcmp(pcgCheckCounterOnly,"YES") == 0)
     {
        strcpy(pclBuf," ");
     }
     else
     {
        /*********** get TGD1 of CurrentRow****************/
        strcpy(pclBuf,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftTGD1));
        TrimRight(pclBuf);
     }
     if (*pclBuf == ' ')
     {
        strcpy(pclAftURNO,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftURNO));
        TrimRight(pclAftURNO);
        if (igFddCntCheck == 0)
        {
           llRowNumCCA = ARR_FIRST;
           ilFound = FALSE;
           while (CEDAArrayGetRowPointer(&rgCcaArray.rrArrayHandle,
                                         rgCcaArray.crArrayName,
                                         llRowNumCCA,(void *)&pclRowCCA) == RC_SUCCESS &&
                  ilFound == FALSE)
           {
              strcpy(pclCcaFLNU,FIELDVAL(rgCcaArray,pclRowCCA,igCcaFLNU));
              TrimRight(pclCcaFLNU);
              if (strcmp(pclAftURNO,pclCcaFLNU) == 0)
              {
                 strcpy(pclCcaCTYP,FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP));
                 TrimRight(pclCcaCTYP);
                 if (*pclCcaCTYP == ' ')
                 {
                    strcpy(pclCcaCKIC,FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKIC));
                    TrimRight(pclCcaCKIC);
                    if (atoi(pclCcaCKIC) >= igFirstDom)
                       strcpy(pclBuf,pcgDomTerm);
                    else
                       strcpy(pclBuf,pcgIntTerm);
                    if (strcmp(pclBuf,pcgIntTerm) == 0)
                       ilFound = TRUE;
                 }
              }
              llRowNumCCA = ARR_NEXT;
           }
        }
        else
        {
           sprintf(pclSqlBuf,"SELECT CKIC FROM CCATAB WHERE FLNU = %s AND CKIC <> ' '",pclAftURNO);
           ilFound = FALSE;
           slCursor = 0;
           slFkt = START;
           ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclCcaCKIC);
           while (ilRCdb == DB_SUCCESS && ilFound == FALSE)
           {
              TrimRight(pclCcaCKIC);
              if (atoi(pclCcaCKIC) >= igFirstDom)
                 strcpy(pclBuf,pcgDomTerm);
              else
                 strcpy(pclBuf,pcgIntTerm);
              if (strcmp(pclBuf,pcgIntTerm) == 0)
                 ilFound = TRUE;
              slFkt = NEXT;
              ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclCcaCKIC);
           }
           close_my_cursor(&slCursor);
        }
     }
     if (strcmp(pclBuf,pcgDomTerm) == 0)       
     {
        *ipTerm1 = 2;
        if (strncmp(pcpRemark,pcgCanceled,3) != 0)
           strcpy(pcpRemark,"TM2");
     }
     if (strcmp(pcgDisplayInT1AndT2,"YES") == 0 && strcmp(pclBuf,pcgIntTerm) == 0)
     {
        if (strcmp(pcgCheckCounterOnly,"YES") == 0)
        {
           strcpy(pclBuf," ");
        }
        else
        {
           /*********** get TGD2 of CurrentRow****************/
           strcpy(pclBuf,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftTGD2));
           TrimRight(pclBuf);
        }
        if (*pclBuf == ' ')
        {
           strcpy(pclAftURNO,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftURNO));
           TrimRight(pclAftURNO);
           if (igFddCntCheck == 0)
           {
              llRowNumCCA = ARR_FIRST;
              ilFound = FALSE;
              while (CEDAArrayGetRowPointer(&rgCcaArray.rrArrayHandle,
                                            rgCcaArray.crArrayName,
                                            llRowNumCCA,(void *)&pclRowCCA) == RC_SUCCESS &&
                     ilFound == FALSE)
              {
                 strcpy(pclCcaFLNU,FIELDVAL(rgCcaArray,pclRowCCA,igCcaFLNU));
                 TrimRight(pclCcaFLNU);
                 if (strcmp(pclAftURNO,pclCcaFLNU) == 0)
                 {
                    strcpy(pclCcaCTYP,FIELDVAL(rgCcaArray,pclRowCCA,igCcaCTYP));
                    TrimRight(pclCcaCTYP);
                    if (*pclCcaCTYP == ' ')
                    {
                       strcpy(pclCcaCKIC,FIELDVAL(rgCcaArray,pclRowCCA,igCcaCKIC));
                       TrimRight(pclCcaCKIC);
                       if (atoi(pclCcaCKIC) >= igFirstDom)
                       {
                          strcpy(pclBuf,pcgDomTerm);
                          ilFound = TRUE;
                       }
                    }
                 }
                 llRowNumCCA = ARR_NEXT;
              }
           }
           else
           {
              sprintf(pclSqlBuf,"SELECT CKIC FROM CCATAB WHERE FLNU = %s AND CKIC <> ' '",pclAftURNO);
              ilFound = FALSE;
              slCursor = 0;
              slFkt = START;
              ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclCcaCKIC);
              while (ilRCdb == DB_SUCCESS && ilFound == FALSE)
              {
                 TrimRight(pclCcaCKIC);
                 if (atoi(pclCcaCKIC) >= igFirstDom)
                 {
                    strcpy(pclBuf,pcgDomTerm);
                    ilFound = TRUE;
                 }
                 slFkt = NEXT;
                 ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclCcaCKIC);
              }
              close_my_cursor(&slCursor);
           }
        }
        if (strcmp(pclBuf,pcgDomTerm) == 0)       
        {
           *ipTerm2 = 2;
        }
     }
  }

  return (ilRC);
} /*end of HandleTerminal_D */


static int AddSecondsToCEDATime12(char *pcpTime, long llVal, int ilTyp)
{
  int ilRC = RC_SUCCESS;

  if (strlen(pcpTime) > 0 && *pcpTime != ' ')
     TrimRight(pcpTime);
  if (strlen(pcpTime) == 12)
     strcat(pcpTime,"00");
  ilRC = AddSecondsToCEDATime(pcpTime,llVal,ilTyp);

  return ilRC;
} /*end of AddSecondsToCEDATime12 */


static int GetChutes(char *pcpFlno, char *pcpUrno, char *pcpChaf, char *pcpChat)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "GetChutes";
  long llRowNumCHA = ARR_FIRST;
  char *pclRowCHA = NULL;
  char pclUrno[16];
  char pclChaRURN[16] = "\0";
  char pclChaLNAM[16] = "\0";

  strcpy(pcpChaf," ");
  strcpy(pcpChat," ");
  strcpy(pclUrno,pcpUrno);
  TrimRight(pclUrno);

  llRowNumCHA = ARR_FIRST;
  while(CEDAArrayGetRowPointer(&rgChaArray.rrArrayHandle,
                               rgChaArray.crArrayName,
                               llRowNumCHA,(void *)&pclRowCHA) == RC_SUCCESS)
  {
     strcpy(pclChaRURN,FIELDVAL(rgChaArray,pclRowCHA,igChaRURN));
     TrimRight(pclChaRURN);
     if (strcmp(pclUrno,pclChaRURN) == 0)
     {
        strcpy(pclChaLNAM,FIELDVAL(rgChaArray,pclRowCHA,igChaLNAM));
        TrimRight(pclChaLNAM);
        if (*pcpChaf == ' ')
        {
           strcpy(pcpChaf,pclChaLNAM);
           strcpy(pcpChat,pclChaLNAM);
        }
        else if (strcmp(pclChaLNAM,pcpChaf) < 0)
           strcpy(pcpChaf,pclChaLNAM);
        else if (strcmp(pclChaLNAM,pcpChat) > 0)
           strcpy(pcpChat,pclChaLNAM);
     }
     llRowNumCHA = ARR_NEXT;
  }

  return ilRC;
} /* End of GetChutes */


static int CheckFtyp(char *pcpFlnu)
{
  int ilRC = RC_SUCCESS;
  long llRowNumAFT = ARR_FIRST;
  char *pclRowAFT = NULL;
  int ilFound = FALSE;
  char pclAftURNO[16];
  char pclAftFTYP[16] = " ";
  char pclAftSTEV[16] = " ";
  int ilCheck1;
  int ilCheck2;
  int ilI;
  int ilNoEle;
  char pclTmpBuf[128];

  ilRC = RC_FAIL;
  ilFound = FALSE;
  TrimRight(pcpFlnu);
  llRowNumAFT = ARR_FIRST;
  while (ilFound == FALSE && 
         CEDAArrayGetRowPointer(&rgAftArray.rrArrayHandle,
                                rgAftArray.crArrayName,
                                llRowNumAFT,(void *)&pclRowAFT) == RC_SUCCESS)
  {
     llRowNumAFT = ARR_CURRENT;
     strcpy(pclAftURNO,FIELDVAL(rgAftArray,pclRowAFT,igAftURNO));
     TrimRight(pclAftURNO);
     if (strcmp(pcpFlnu,pclAftURNO) == 0)
     {
        if (strcmp(pcgCheckFtypForChuteDisplays,"YES") == 0)
        {
           strcpy(pclAftFTYP,FIELDVAL(rgAftArray,pclRowAFT,igAftFTYP));
           TrimRight(pclAftFTYP);
           strcpy(pclAftSTEV,FIELDVAL(rgAftArray,pclRowAFT,igAftSTEV));
           TrimRight(pclAftSTEV);
           ilCheck1 = RC_FAIL;
           if (*pclAftFTYP != 'X' && *pclAftFTYP != 'N')
              ilCheck1 = RC_SUCCESS;
           ilCheck2 = RC_SUCCESS;
           if (strlen(pcgListOfStev) > 0)
           {
              ilCheck2 = RC_FAIL;
              ilNoEle = GetNoOfElements(pcgListOfStev,',');
              for (ilI = 1; ilI <= ilNoEle; ilI++)
              {
                 get_real_item(pclTmpBuf,pcgListOfStev,ilI);
                 if (strcmp(pclAftSTEV,pclTmpBuf) == 0)
                    ilCheck2 = RC_SUCCESS;
              }
           }
           if (ilCheck1 == RC_SUCCESS && ilCheck2 == RC_SUCCESS)
              ilRC = RC_SUCCESS;
        }
        else
           ilRC = RC_SUCCESS;
        ilFound = TRUE;
     }
     llRowNumAFT = ARR_NEXT;
  }

  return ilRC;
} /* End of CheckFtyp */


static int UpdateCheckInFromTo(char *pcpFddAurn, char *pcpCKIFInt, char *pcpCKITInt, char *pcpCKIFDom, char *pcpCKITDom)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "UpdateCheckInFromTo:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[1024];
  char pclDataBuf[1024];
  char pclFlno[16];
  char pclCkifInt[16];
  char pclCkitInt[16];
  char pclCkifDom[16];
  char pclCkitDom[16];
  char pclCkif[16];
  char pclCkit[16];
  char pclCkf2[16];
  char pclCkt2[16];
  char pclSelection[1024];
  char pclFieldList[1024];
  char pclDataList[1024];

  strcpy(pclCkifInt,pcpCKIFInt);
  strcpy(pclCkitInt,pcpCKITInt);
  strcpy(pclCkifDom,pcpCKIFDom);
  strcpy(pclCkitDom,pcpCKITDom);
  if (*pclCkifInt == ' ' && *pclCkifDom != ' ')
  {
     strcpy(pclCkifInt,pclCkifDom);
     strcpy(pclCkitInt,pclCkitDom);
     strcpy(pclCkifDom," ");
     strcpy(pclCkitDom," ");
  }
  sprintf(pclSqlBuf,"SELECT FLNO,CKIF,CKIT,CKF2,CKT2 FROM AFTTAB WHERE URNO = %s",pcpFddAurn);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"%s <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  if (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",5,",");
     get_real_item(pclFlno,pclDataBuf,1);
     TrimRight(pclFlno);
     get_real_item(pclCkif,pclDataBuf,2);
     TrimRight(pclCkif);
     get_real_item(pclCkit,pclDataBuf,3);
     TrimRight(pclCkit);
     get_real_item(pclCkf2,pclDataBuf,4);
     TrimRight(pclCkf2);
     get_real_item(pclCkt2,pclDataBuf,5);
     TrimRight(pclCkt2);
     if (strcmp(pclCkifInt,pclCkif) != 0 || strcmp(pclCkitInt,pclCkit) != 0 ||
         strcmp(pclCkifDom,pclCkf2) != 0 || strcmp(pclCkitDom,pclCkt2) != 0)
     {
        sprintf(pclSelection,"WHERE URNO = %s",pcpFddAurn);
        strcpy(pclFieldList,"CKIF,CKIT,CKF2,CKT2");
        sprintf(pclDataList,"%s,%s,%s,%s",pclCkifInt,pclCkitInt,pclCkifDom,pclCkitDom);
        ilRC = SendEvent("UFR",7800,PRIORITY_3,"AFT","GD2Y",pcgTwEnd,pclSelection,
                         pclFieldList,pclDataList,NULL,0);
     }
  }
  close_my_cursor(&slCursor);

  return ilRC;
} /* End of UpdateCheckInFromTo */


static int GenerateStevFromCounter(char *pcpCurRowAFT, char *pcpNewSTEV)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "GenerateStevFromCounter:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[1024];
  char pclAftURNO[16];
  char pclAftFLNO[16];
  char pclAftSTOD[16];
  char pclAltURNO[16];
  int ilCount;

  strcpy(pclAftURNO,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftURNO));
  TrimRight(pclAftURNO);
  sprintf(pclSqlBuf,"SELECT CKIC FROM CCATAB WHERE FLNU = %s AND CKIC <> ' '",pclAftURNO);
  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pcpNewSTEV);
  close_my_cursor(&slCursor);
  if (ilRCdb == DB_SUCCESS)
  {
     TrimRight(pcpNewSTEV);
     pcpNewSTEV[1] = '\0';
  }
  else
  {
     strcpy(pclAftFLNO,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftFLNO));
     TrimRight(pclAftFLNO);
     strcpy(pclAftSTOD,FIELDVAL(rgAftArray,pcpCurRowAFT,igAftSTOD));
     TrimRight(pclAftSTOD);
     ilCount = 1;
     if (pclAftFLNO[2] == ' ')
     {
        pclAftFLNO[2] = '\0';
        ilRC = syslibSearchDbData("ALTTAB","ALC2",pclAftFLNO,"URNO",pclAltURNO,&ilCount,"\n");
     }
     else
     {
        pclAftFLNO[3] = '\0';
        ilRC = syslibSearchDbData("ALTTAB","ALC3",pclAftFLNO,"URNO",pclAltURNO,&ilCount,"\n");
     }
     AddSecondsToCEDATime12(pclAftSTOD,60*60*(-1),1);
     sprintf(pclSqlBuf,
             "SELECT CKIC FROM CCATAB WHERE FLNU = %s AND CTYP = 'C' AND CKBS <= '%s' AND CKES >= '%s'",
             pclAltURNO,pclAftSTOD,pclAftSTOD);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pcpNewSTEV);
     close_my_cursor(&slCursor);
     if (ilRCdb == DB_SUCCESS)
     {
        TrimRight(pcpNewSTEV);
        pcpNewSTEV[1] = '\0';
     }
     else
     {
        strcpy(pcpNewSTEV," ");
     }
  }

  return ilRC;
} /* End of GenerateStevFromCounter */


static int CheckCcaFtyp(char *pcpFlnu, char *pcpCtyp)
{
  int ilRC = RC_SUCCESS;
  long llRowNumAFT = ARR_FIRST;
  char *pclRowAFT = NULL;
  int ilFound = FALSE;
  char pclAftURNO[16];
  char pclAftFTYP[16] = " ";
  int ilI;
  int ilNoEle;
  char pclTmpBuf[128];
  char pclFlnu[16];
  char pclCtyp[16];

  strcpy(pclFlnu,pcpFlnu);
  strcpy(pclCtyp,pcpCtyp);
  TrimRight(pclFlnu);
  TrimRight(pclCtyp);
  if (*pclCtyp == 'C')
     return(RC_SUCCESS);

  ilRC = RC_FAIL;
  ilFound = FALSE;
  llRowNumAFT = ARR_FIRST;
  while (ilFound == FALSE && 
         CEDAArrayGetRowPointer(&rgAftArray.rrArrayHandle,
                                rgAftArray.crArrayName,
                                llRowNumAFT,(void *)&pclRowAFT) == RC_SUCCESS)
  {
     llRowNumAFT = ARR_CURRENT;
     strcpy(pclAftURNO,FIELDVAL(rgAftArray,pclRowAFT,igAftURNO));
     TrimRight(pclAftURNO);
     if (strcmp(pclFlnu,pclAftURNO) == 0)
     {
        strcpy(pclAftFTYP,FIELDVAL(rgAftArray,pclRowAFT,igAftFTYP));
        TrimRight(pclAftFTYP);
        if (strlen(pcgListOfFtypForCca) > 0)
        {
           ilNoEle = GetNoOfElements(pcgListOfFtypForCca,',');
           for (ilI = 1; ilI <= ilNoEle; ilI++)
           {
              get_real_item(pclTmpBuf,pcgListOfFtypForCca,ilI);
              if (strcmp(pclAftFTYP,pclTmpBuf) == 0)
                 ilRC = RC_SUCCESS;
           }
        }
        ilFound = TRUE;
     }
     llRowNumAFT = ARR_NEXT;
  }

  return ilRC;
} /* End of CheckCcaFtyp */


static int GetCounterTerm(char *pcpCDA, char *pcpSTEV, char *pcpALC, char *pcpFLNO,
                          char *pcpCKIF, char *pcpCKIT)

{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "GetCounterTerm:";
  int ilI;
  int ilCount;
  char pclCnam[16];
  char pclTerm[16];
  int ilCurCnt;
  int ilNextCnt;
  int ilFoundC;
  char pclCkifC[16];
  char pclCkitC[16];
  int ilFoundD;
  char pclCkifD[16];
  char pclCkitD[16];

  strcpy(pcpCKIF," ");
  strcpy(pcpCKIT," ");

  strcpy(pclCkifC,"");
  strcpy(pclCkitC,"");
  ilFoundC = -1;
  TrimRight(pcpALC);
  for (ilI = 0; ilI < igCurNoComCnt && ilFoundC < 0; ilI++)
  {
     if (strlen(pcpALC) == 2)
     {
        if (strcmp(pcpALC,&rgCommonCounters[ilI].pclComALC2[0]) == 0)
           ilFoundC = ilI;
     }
     else
     {
        if (strcmp(pcpALC,&rgCommonCounters[ilI].pclComALC3[0]) == 0)
           ilFoundC = ilI;
     }
  }
  if (ilFoundC >= 0)
  {
     for (ilI = 0; ilI < rgCommonCounters[ilFoundC].ilCurNoIntNam; ilI++)
     {
        strcpy(pclCnam,&rgCommonCounters[ilFoundC].pclCntInt[ilI][0]);
        ilCount = 1;
        ilRC = syslibSearchDbData("CICTAB","CNAM",pclCnam,"TERM",pclTerm,&ilCount,"\n");
        if (strcmp(pcpSTEV,pclTerm) == 0)
        {
           if (strlen(pclCkifC) == 0)
           {
              strcpy(pclCkifC,pclCnam);
              strcpy(pclCkitC,pclCnam);
           }
           else if (strcmp(pclCnam,pclCkitC) > 0)
           {
              ilCurCnt = atoi(pclCkitC);
              ilNextCnt = atoi(pclCnam);
              if (ilNextCnt-ilCurCnt-1 <= igGapCount)
                 strcpy(pclCkitC,pclCnam);
           }
        }
     }
     for (ilI = 0; ilI < rgCommonCounters[ilFoundC].ilCurNoDomNam; ilI++)
     {
        strcpy(pclCnam,&rgCommonCounters[ilFoundC].pclCntDom[ilI][0]);
        ilCount = 1;
        ilRC = syslibSearchDbData("CICTAB","CNAM",pclCnam,"TERM",pclTerm,&ilCount,"\n");
        if (strcmp(pcpSTEV,pclTerm) == 0)
        {
           if (strlen(pclCkifC) == 0)
           {
              strcpy(pclCkifC,pclCnam);
              strcpy(pclCkitC,pclCnam);
           }
           else if (strcmp(pclCnam,pclCkitC) > 0)
           {
              ilCurCnt = atoi(pclCkitC);
              ilNextCnt = atoi(pclCnam);
              if (ilNextCnt-ilCurCnt-1 <= igGapCount)
                 strcpy(pclCkitC,pclCnam);
           }
        }
     }
  }

  strcpy(pclCkifD,"");
  strcpy(pclCkitD,"");
  ilFoundD = -1;
  TrimRight(pcpFLNO);
  for (ilI = 0; ilI < igCurNoDediCnt && ilFoundD < 0; ilI++)
  {
     if (strcmp(pcpFLNO,&rgDedicatedCounters[ilI].pclDediFLNO[0]) == 0)
        ilFoundD = ilI;
  }
  if (ilFoundD >= 0)
  {
     for (ilI = 0; ilI < rgDedicatedCounters[ilFoundD].ilCurNoIntNam; ilI++)
     {
        strcpy(pclCnam,&rgDedicatedCounters[ilFoundD].pclCntInt[ilI][0]);
        ilCount = 1;
        ilRC = syslibSearchDbData("CICTAB","CNAM",pclCnam,"TERM",pclTerm,&ilCount,"\n");
        if (strcmp(pcpSTEV,pclTerm) == 0)
        {
           if (strlen(pclCkifD) == 0)
           {
              strcpy(pclCkifD,pclCnam);
              strcpy(pclCkitD,pclCnam);
           }
           else if (strcmp(pclCnam,pclCkitD) > 0)
           {
              ilCurCnt = atoi(pclCkitD);
              ilNextCnt = atoi(pclCnam);
              if (ilNextCnt-ilCurCnt-1 <= igGapCount)
                 strcpy(pclCkitD,pclCnam);
           }
        }
     }
     for (ilI = 0; ilI < rgDedicatedCounters[ilFoundD].ilCurNoDomNam; ilI++)
     {
        strcpy(pclCnam,&rgDedicatedCounters[ilFoundD].pclCntDom[ilI][0]);
        ilCount = 1;
        ilRC = syslibSearchDbData("CICTAB","CNAM",pclCnam,"TERM",pclTerm,&ilCount,"\n");
        if (strcmp(pcpSTEV,pclTerm) == 0)
        {
           if (strlen(pclCkifD) == 0)
           {
              strcpy(pclCkifD,pclCnam);
              strcpy(pclCkitD,pclCnam);
           }
           else if (strcmp(pclCnam,pclCkitD) > 0)
           {
              ilCurCnt = atoi(pclCkitD);
              ilNextCnt = atoi(pclCnam);
              if (ilNextCnt-ilCurCnt-1 <= igGapCount)
                 strcpy(pclCkitD,pclCnam);
           }
        }
     }
  }

  if (*pcpCDA == 'D')
  {
     strcpy(pcpCKIF,pclCkifD);
     strcpy(pcpCKIT,pclCkitD);
  }
  else if (*pcpCDA == 'C')
  {
     strcpy(pcpCKIF,pclCkifC);
     strcpy(pcpCKIT,pclCkitC);
  }
  else if (*pcpCDA == 'A')
  {
     if (strlen(pclCkifC) > 0)
     {
        strcpy(pcpCKIF,pclCkifC);
        strcpy(pcpCKIT,pclCkitC);
        if (strlen(pclCkifD) > 0)
        {
           if (strcmp(pclCkifD,pcpCKIF) < 0)
           {
              ilCurCnt = atoi(pclCkitD);
              ilNextCnt = atoi(pcpCKIF);
              if (ilNextCnt-ilCurCnt-1 <= igGapCount)
                 strcpy(pcpCKIF,pclCkifD);
           }
           if (strcmp(pclCkitD,pcpCKIT) > 0)
           {
              ilCurCnt = atoi(pcpCKIT);
              ilNextCnt = atoi(pclCkifD);
              if (ilNextCnt-ilCurCnt-1 <= igGapCount)
                 strcpy(pcpCKIT,pclCkitD);
           }
        }
     }
     else
     {
        strcpy(pcpCKIF,pclCkifD);
        strcpy(pcpCKIT,pclCkitD);
     }
  }
  else if (*pcpCDA == 'X')
  {
     if (strlen(pclCkifC) > 0)
     {
        strcpy(pcpCKIF,pclCkifC);
        strcpy(pcpCKIT,pclCkitC);
     }
     else
     {
        strcpy(pcpCKIF,pclCkifD);
        strcpy(pcpCKIT,pclCkitD);
     }
  }
  else if (*pcpCDA == 'Y')
  {
     if (strlen(pclCkifD) > 0)
     {
        strcpy(pcpCKIF,pclCkifD);
        strcpy(pcpCKIT,pclCkitD);
     }
     else
     {
        strcpy(pcpCKIF,pclCkifC);
        strcpy(pcpCKIT,pclCkitC);
     }
  }
  else if (*pcpCDA == 'S')
  {
     strcpy(pcpCKIF,pcgCKIF);
     strcpy(pcpCKIT,pcgCKIT);
  }
  else
  {
     strcpy(pcpCKIF,pcgCKIF);
     strcpy(pcpCKIT,pcgCKIT);
  }
  strcpy(pcgCKIF,pcpCKIF);
  strcpy(pcgCKIT,pcpCKIT);

  return ilRC;
} /* End of GetCounterTerm */

