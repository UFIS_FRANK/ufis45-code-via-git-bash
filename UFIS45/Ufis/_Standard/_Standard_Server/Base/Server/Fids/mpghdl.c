#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Fids/mpghdl.c 1.1 2003/03/19 16:49:40SGT fei Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  Hans-Juergen Ebert (HEB)                                 */
/* Date           :  29-06-2001                                               */
/* Description    :  Process for scheduled starting and stopping of MPEG's    */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_version[] ="%Z% UFIS44 (c) ABB AAT/I %M% %I% / %E% %U% / VBL";
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h"
#include "mpghdl.h"
#include "AATArray.h"
#include "syslib.h"
#include "libccstr.h"
#include "fditools.h"
#include "sys/resource.h"

#define ARR_NAME_LEN   (18)
#define ARR_FLDLST_LEN (256)
#define FIELDVAL(rrArray,pcpBuf,ipFieldNo) (&pcpBuf[rrArray.plrArrayFieldOfs[ipFieldNo]])

#define DEV_FIELDS "GRPN,DEVN,URNO,DADR,ADID"
#define MVA_FIELDS_TAB "URNO,HOPO,USEC,USEU,DEVN,MPFC,MVSB,MVSE,MVAB,MVAE,RURN,PGNO,OBJN,REPC,BEME"
#define MVA_FIELDS "URNO,HOPO,USEC,USEU,DEVN,MPFC,MVSB,MVSE,MVAB,MVAE,RURN,PGNO,OBJN,REPC,BEME,ISGR" /*ISGR = ISGROUP*/

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = 0;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512] = "\0";

static int igTcpTimeOutAlarmFlag = FALSE;
static int igConnectDeviceFlag  = FALSE;
static int igAlarmCount = 0;
static char  cgErrMsg[1024] = "\0";
static char  pcgTabEnd[8] = "\0";      
static char  cgHopo[8] = "\0";
static char  pcgHomeAp[XS_BUF];      /* buffer for home airport */
static char  pcgTwEnd[XS_BUF];          /* buffer for TABEND */

static char *cgGroupList = NULL;
static char *cgUpDevList = NULL;
  
struct _arrayinfo {
  HANDLE   rrArrayHandle;
  char     crArrayName[ARR_NAME_LEN+1];
  char     crArrayFieldList[ARR_FLDLST_LEN+1];
  long     lrArrayFieldCnt;
  char   *pcrArrayRowBuf;
  long     lrArrayRowLen;
  long   *plrArrayFieldOfs;
  long   *plrArrayFieldLen;

  HANDLE   rrIdx01Handle;
  char     crIdx01Name[ARR_NAME_LEN+1];
  char     crIdx01FieldList[ARR_FLDLST_LEN+1];
  long     lrIdx01FieldCnt;
  char   *pcrIdx01RowBuf;
  long     lrIdx01RowLen;
  long   *plrIdx01FieldPos;
  long   *plrIdx01FieldOrd;

  HANDLE   rrIdx02Handle;
  char     crIdx02Name[ARR_NAME_LEN+1];
  char     crIdx02FieldList[ARR_FLDLST_LEN+1];
  long     lrIdx02FieldCnt;
  char   *pcrIdx02RowBuf;
  long     lrIdx02RowLen;
  long   *plrIdx02FieldPos;
  long   *plrIdx02FieldOrd;
};
typedef struct _arrayinfo ARRAYINFO;

static ARRAYINFO rgDevArray;
static ARRAYINFO rgMvaArray;

/****** DEVTAB-FIELDS ************/
static int igDevGRPN;
static int igDevDEVN;
static int igDevURNO;
static int igDevDADR;
static int igDevADID;

/****** MVATAB-FIELDS ************/
static int igMvaURNO;
static int igMvaHOPO;
static int igMvaUSEC;
static int igMvaUSEU;
static int igMvaDEVN;
static int igMvaMPFC;
static int igMvaMVSB;
static int igMvaMVSE;
static int igMvaMVAB;
static int igMvaMVAE;
static int igMvaRURN;
static int igMvaPGNO;
static int igMvaOBJN;
static int igMvaREPC;
static int igMvaBEME;
static int igMvaISGR;

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int Init_mpghdl();
static int	Reset(void);                       /* Reset program          */
static void	Terminate(int);                   /* Terminate program      */
static void	HandleSignal(int);                 /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void	HandleQueErr(int);                 /* Handles queuing errors */
static int	HandleData(void);                  /* Handles event data     */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
		     char *pcpTwStart, char *pcpTwEnd,
		     char *pcpSelection,char *pcpFields,char *pcpData,
		     char *pcpAddStruct,int ipAddStructSize);
static int CreateCEDAArrays();
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, 
                        char *pcpArrayFieldList,char *pcpAddFields,
                        long *pcpAddFieldLens,ARRAYINFO *prpArrayInfo, 
                        char *pcpSelection,BOOL bpFill);
static int InitFieldIndex();
static int FindItemInArrayList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos);
static int  GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList, long *pipLen);
static int  GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina, long *pipLen);
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo);
static int ShowGroupDisplays(char *pcpGroup);
static int GetGroupList();
static void TrimRight(char *pcpBuffer);
static int TimeToStr(char *pcpTime,time_t lpTime);
static int ExtractGroups();
extern int AddSecondsToCEDATime(char *,long,int);
static int SetGroupFlag();
static int GetUpDevList();
static int StartVideos();
static int StopVideos();

extern int get_no_of_items(char *s);

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
  int	ilRC = RC_SUCCESS;     	/* Return code			*/
  int	ilCnt = 0;
	
  INITIALIZE;			/* General initialization	*/
  dbg(TRACE,"MAIN: version <%s>",sccs_version);
  /* Attach to the MIKE queues */
  do
    {
      ilRC = init_que();
      if(ilRC != RC_SUCCESS)
	{
	  dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
	  sleep(6);
	  ilCnt++;
	}/* end of if */
    }while((ilCnt < 10) && (ilRC != RC_SUCCESS));
  if(ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
      sleep(60);
      exit(1);
    }else{
      dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */
  do
    {
      ilRC = init_db();
      if (ilRC != RC_SUCCESS)
	{
	  check_ret(ilRC);
	  dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
	  sleep(6);
	  ilCnt++;
	} /* end of if */
    } while((ilCnt < 10) && (ilRC != RC_SUCCESS));
  if(ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
      sleep(60);
      exit(2);
    }else{
      dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */
  /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
  sprintf(cgConfigFile,"%s/mpghdl",getenv("BIN_PATH"));
  ilRC = TransferFile(cgConfigFile);
  if(ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */
  /* uncomment if necessary */
  /* sprintf(cgConfigFile,"%s/mpghdl.cfg",getenv("CFG_PATH")); */
  /* ilRC = TransferFile(cgConfigFile); */
  /* if(ilRC != RC_SUCCESS) */
  /* { */
  /* 	dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile); */
  /* } */ /* end of if */
  ilRC = SendRemoteShutdown(mod_id);
  if(ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */
  if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
      dbg(DEBUG,"MAIN: waiting for status switch ...");
      HandleQueues();
    }/* end of if */
  if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: initializing ...");
      if(igInitOK == FALSE)
	{
	  ilRC = Init_mpghdl();
	  if(ilRC != RC_SUCCESS)
	    {
	      dbg(TRACE,"Init_mpghdl: init failed!");
	    } /* end of if */
	}/* end of if */
    } else {
      Terminate(1);
    }/* end of if */
  dbg(TRACE,"MAIN: initializing OK");
  for(;;)
    {
      ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
      /* depending on the size of the received item  */
      /* a realloc could be made by the que function */
      /* so do never forget to set event pointer !!! */
      prgEvent = (EVENT *) prgItem->text;
				
      if( ilRC == RC_SUCCESS )
	{
	  /* Acknowledge the item */
	  ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
	  if( ilRC != RC_SUCCESS ) 
	    {
				/* handle que_ack error */
	      HandleQueErr(ilRC);
	    } /* fi */
			
	  switch( prgEvent->command )
	    {
	    case	HSB_STANDBY	:
	      ctrl_sta = prgEvent->command;
	      HandleQueues();
	      break;	
	    case	HSB_COMING_UP	:
	      ctrl_sta = prgEvent->command;
	      HandleQueues();
	      break;	
	    case	HSB_ACTIVE	:
	      ctrl_sta = prgEvent->command;
	      break;	
	    case	HSB_ACT_TO_SBY	:
	      ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
	      HandleQueues();
	      break;	
	    case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
	      ctrl_sta = prgEvent->command;
	      Terminate(1);
	      break;	
	    case	HSB_STANDALONE	:
	      ctrl_sta = prgEvent->command;
	      ResetDBCounter();
	      break;	
	    case	REMOTE_DB :
				/* ctrl_sta is checked inside */
	      HandleRemoteDB(prgEvent);
	      break;
	    case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
	      Terminate(1);
	      break;
					
	    case	RESET		:
	      ilRC = Reset();
	      break;
					
	    case	EVENT_DATA	:
	      if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
		{
		  ilRC = HandleData();
		  if(ilRC != RC_SUCCESS)
		    {
		      HandleErr(ilRC);
		    }/* end of if */
		}else{
		  dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
		  DebugPrintItem(TRACE,prgItem);
		  DebugPrintEvent(TRACE,prgEvent);
		}/* end of if */
	      break;
					
	    case	TRACE_ON :
	      dbg_handle_debug(prgEvent->command);
	      break;
	    case	TRACE_OFF :
	      dbg_handle_debug(prgEvent->command);
	      break;
	    case    111 :
	      ilRC = RUNCmd();
	      break;
 
	    default			:
	      dbg(TRACE,"MAIN: unknown event");
	      DebugPrintItem(TRACE,prgItem);
	      DebugPrintEvent(TRACE,prgEvent);
	      break;
	    } /* end switch */
	} else {
	  /* Handle queuing errors */
	  HandleQueErr(ilRC);
	} /* end else */
		
    } /* end for */
	
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_mpghdl()
{
  int	ilRC = RC_SUCCESS;			/* Return code */
  char clSection[64];
  char clKeyword[64];

  /* reading default home-airport from sgs.tab */
    
  dbg(DEBUG,"%05d: reading home-ariport",__LINE__);
  memset(pcgHomeAp,0x00,sizeof(pcgHomeAp));
  dbg(DEBUG,"%05d: memset done",__LINE__);
  ilRC = tool_search_exco_data("SYS","HOMEAP",pcgHomeAp);
  if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%05d: Init_fldhdl : No HOMEAP entry in sgs.tab: EXTAB! Please add!",__LINE__);
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"%05d: Init_fldhdl : HOMEAP = <%s>",__LINE__,pcgHomeAp);

    }
 
  /********************* get TabEnd from systab ******************/
  /*sprintf(&clSection[0],"ALL");*/
  /*sprintf(&clKeyword[0],"TABEND");*/
  sprintf(clSection,"ALL");
  sprintf(clKeyword,"TABEND");
  ilRC = tool_search_exco_data(&clSection[0], &clKeyword[0], &pcgTabEnd[0]);
    


  memset(pcgTwEnd,0x00,XS_BUF);
  sprintf(pcgTwEnd,"%s,%s,%s",pcgHomeAp,pcgTabEnd,mod_name);
  dbg(TRACE,"%05d: Init_fldhdl : TW_END = <%s>",__LINE__,pcgTwEnd);



  ilRC = CreateCEDAArrays();
  dbg(DEBUG,"%05d: After CreateCEDAArrays");
	

  /*	ilRC = ShowGroupDisplays("GVC00");*/
  /*ilRC = ExtractGroups();*/
  ilRC = RUNCmd();

  debug_level = 0;
  igInitOK = TRUE;
  return(ilRC);
	
} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
  int	ilRC = RC_SUCCESS;				/* Return code */
	
  dbg(TRACE,"Reset: now resetting");
	
  return ilRC;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(ipSleep)
{
  /* unset SIGCHLD ! DB-Child will terminate ! */

  dbg(TRACE,"Terminate: now DB logoff ...");

	/*  signal(SIGCHLD,SIG_IGN); */

/*  	logoff(); */

  dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


  dbg(TRACE,"Terminate: now leaving ...");

  fclose(outp);

  sleep(ipSleep < 1 ? 1 : ipSleep);
	
  exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
  switch(pipSig)
    {
    case SIGALRM:
      if(igConnectDeviceFlag==TRUE&&igAlarmCount > 3)
	{
	  dbg(DEBUG,"SIGALARM");
	  igTcpTimeOutAlarmFlag=TRUE;
	}
      break;
    case SIGPIPE:
      dbg(TRACE,"Broken pipe");
      break;
    case SIGCHLD:
      break;
    case SIGTERM:
      Terminate(1);
      break;
    default    :
      Terminate(10);
      break;
    } /* end of switch */
	
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
  int	ilRC = RC_SUCCESS;
  return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
  int	ilRC = RC_SUCCESS;
	
  switch(pipErr) {
  case	QUE_E_FUNC	:	/* Unknown function */
    dbg(TRACE,"<%d> : unknown function",pipErr);
    break;
  case	QUE_E_MEMORY	:	/* Malloc reports no memory */
    dbg(TRACE,"<%d> : malloc failed",pipErr);
    break;
  case	QUE_E_SEND	:	/* Error using msgsnd */
    dbg(TRACE,"<%d> : msgsnd failed",pipErr);
    break;
  case	QUE_E_GET	:	/* Error using msgrcv */
    dbg(TRACE,"<%d> : msgrcv failed",pipErr);
    break;
  case	QUE_E_EXISTS	:
    dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
    break;
  case	QUE_E_NOFIND	:
    dbg(TRACE,"<%d> : route not found ",pipErr);
    break;
  case	QUE_E_ACKUNEX	:
    dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
    break;
  case	QUE_E_STATUS	:
    dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
    break;
  case	QUE_E_INACTIVE	:
    dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
    break;
  case	QUE_E_MISACK	:
    dbg(TRACE,"<%d> : missing ack ",pipErr);
    break;
  case	QUE_E_NOQUEUES	:
    dbg(TRACE,"<%d> : queue does not exist",pipErr);
    break;
  case	QUE_E_RESP	:	/* No response on CREATE */
    dbg(TRACE,"<%d> : no response on create",pipErr);
    break;
  case	QUE_E_FULL	:
    dbg(TRACE,"<%d> : too many route destinations",pipErr);
    break;
  case	QUE_E_NOMSG	:	/* No message on queue */
    dbg(TRACE,"<%d> : no messages on queue",pipErr);
    break;
  case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
    dbg(TRACE,"<%d> : invalid originator=0",pipErr);
    break;
  case	QUE_E_NOINIT	:	/* Queues is not initialized*/
    dbg(TRACE,"<%d> : queues are not initialized",pipErr);
    break;
  case	QUE_E_ITOBIG	:
    dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
    break;
  case	QUE_E_BUFSIZ	:
    dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
    break;
  default			:	/* Unknown queue error */
    dbg(TRACE,"<%d> : unknown error",pipErr);
    break;
  } /* end switch */
         
  return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
  int	ilRC = RC_SUCCESS;			/* Return code */
  int	ilBreakOut = FALSE;
	
  do
    {
      ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
      /* depending on the size of the received item  */
      /* a realloc could be made by the que function */
      /* so do never forget to set event pointer !!! */
      prgEvent = (EVENT *) prgItem->text;	
      if( ilRC == RC_SUCCESS )
	{
	  /* Acknowledge the item */
	  ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
	  if( ilRC != RC_SUCCESS ) 
	    {
				/* handle que_ack error */
	      HandleQueErr(ilRC);
	    } /* fi */
		
	  switch( prgEvent->command )
	    {
	    case	HSB_STANDBY	:
	      ctrl_sta = prgEvent->command;
	      break;	
	
	    case	HSB_COMING_UP	:
	      ctrl_sta = prgEvent->command;
	      break;	
	
	    case	HSB_ACTIVE	:
	      ctrl_sta = prgEvent->command;
	      ilBreakOut = TRUE;
	      break;	
	    case	HSB_ACT_TO_SBY	:
	      ctrl_sta = prgEvent->command;
	      break;	
	
	    case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
	      ctrl_sta = prgEvent->command;
	      Terminate(1);
	      break;	
	
	    case	HSB_STANDALONE	:
	      ctrl_sta = prgEvent->command;
	      ResetDBCounter();
	      ilBreakOut = TRUE;
	      break;	
	    case	REMOTE_DB :
				/* ctrl_sta is checked inside */
	      HandleRemoteDB(prgEvent);
	      break;
	    case	SHUTDOWN	:
	      Terminate(1);
	      break;
						
	    case	RESET		:
	      ilRC = Reset();
	      break;
						
	    case	EVENT_DATA	:
	      dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
	      DebugPrintItem(TRACE,prgItem);
	      DebugPrintEvent(TRACE,prgEvent);
	      break;
					
	    case	TRACE_ON :
	      dbg_handle_debug(prgEvent->command);
	      break;
	    case	TRACE_OFF :
	      dbg_handle_debug(prgEvent->command);
	      break;
	    default			:
	      dbg(TRACE,"HandleQueues: unknown event");
	      DebugPrintItem(TRACE,prgItem);
	      DebugPrintEvent(TRACE,prgEvent);
	      break;
	    } /* end switch */
	} else {
	  /* Handle queuing errors */
	  HandleQueErr(ilRC);
	} /* end else */
    } while (ilBreakOut == FALSE);
  if(igInitOK == FALSE)
    {
      ilRC = Init_mpghdl();
      if(ilRC != RC_SUCCESS)
	{
	  dbg(TRACE,"Init_mpghdl: init failed!");
	} /* end of if */
    }/* end of if */
  /* OpenConnection(); */
} /* end of HandleQueues */
	

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData()
{
  int	ilRC = RC_SUCCESS;			/* Return code */
  BC_HEAD *prlBchd;
  CMDBLK  *prlCmdblk;
  char    *pclSelect;
  char    *pclFields;
  char    *pclData;
  char    *pclPtr;  
  int     ilConfCount;
  int     ilRc = RC_SUCCESS;
  char pclCmdStart[16] = "\0";
  char pclCmdStop[16] = "\0";

                                          
  prlBchd   = (BC_HEAD *)((char *)prgEvent + sizeof(EVENT));
  prlCmdblk = (CMDBLK *)((char *)prlBchd->data);
  pclSelect = (char *)prlCmdblk->data;
  pclFields = (char *)pclSelect + strlen(pclSelect) + 1;
  pclData   = (char *)pclFields + strlen(pclFields) + 1;                                                                      
  dbg(TRACE,"empfangenes Commando:\n prlCmdblk:<%s>\n pclSelect:<%s>\n pclFields: <%s>\n pclData: <%s>\n"
      ,prlCmdblk,pclSelect,pclFields,pclData);
  dbg(DEBUG,"%05d: prlCmdblk->obj_name: <%s>",__LINE__,prlCmdblk->obj_name);

  if ((strcmp(prlCmdblk->command,"RUN") == 0) || (strcmp(prlCmdblk->command,"111") == 0))
    {
   
      ilRC = RUNCmd();
    }

  return ilRC;
	
} /* end of HandleData */


/******************************************************************************/
/* The CreateCedaArrays routine                                                    */
/******************************************************************************/
static int CreateCEDAArrays()
{
  int  ilRC =RC_SUCCESS;
  long pclAddFieldLens[12];
  char pclAddFields[256] = "\0";
  char pclSelection[RES_BUF_SIZE] = "\0";
  char pclBegin[32] = "\0";
  char pclEnd[32] = "\0";

  ilRC = CEDAArrayInitialize(5,5);
  if ( ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%05d: CEDAArrayInitialize failed!, ilRC: %d",__LINE__,ilRC);
    }

  /************** now create DEVArray ***************/

  dbg(TRACE,"%05d: Now create DEVArray !!",__LINE__);

  sprintf(pclSelection,"WHERE STAT = 'U'");
  strcpy(pclAddFields,"");
  pclAddFieldLens[0] = 0;
  pclAddFieldLens[1] = 0;
  pclAddFieldLens[2] = 0;
  pclAddFieldLens[3] = 0;
  pclAddFieldLens[4] = 0;
  
  dbg(DEBUG,"%05d: DEV_FIELDS: <%s>, &rgDevArray: <%d>, pclSelection: <%s>",__LINE__,DEV_FIELDS,&rgDevArray,pclSelection);
  ilRC = SetArrayInfo("DEVTAB","DEVTAB",DEV_FIELDS,
                      NULL,NULL,&rgDevArray,pclSelection,TRUE);

  if(ilRC != RC_SUCCESS)
    {
      dbg(TRACE," SetArrayInfo DEV_TAB failed <%d>",ilRC);
    }

  /********************** now create ArrayIndex ***********************/

  strcpy(rgDevArray.crIdx01Name,"DEV_ADID");
  ilRC = CEDAArrayCreateSimpleIndexUp(&rgDevArray.rrArrayHandle,
				      rgDevArray.crArrayName,
				      &rgDevArray.rrIdx01Handle,
				      rgDevArray.crIdx01Name,"ADID");

  strcpy(rgDevArray.crIdx02Name,"DEV_DEVN");
  ilRC = CEDAArrayCreateSimpleIndexUp(&rgDevArray.rrArrayHandle,
				      rgDevArray.crArrayName,
				      &rgDevArray.rrIdx02Handle,
				      rgDevArray.crIdx02Name,"DEVN");

  /************** now create MVAArray ***************/

  TimeToStr(pclBegin,time(NULL));
  AddSecondsToCEDATime(pclBegin,300,1);
  TimeToStr(pclEnd,time(NULL));


  ilRC = GetGroupList();
  dbg(TRACE,"%05d: Now create MVAArray !!",__LINE__);

  sprintf(pclSelection,"WHERE DEVN IN(%s) AND MVSB <= '%s' AND MVSE > '%s' AND MVAB = ' '",cgGroupList,pclBegin,pclEnd);
  strcpy(pclAddFields,"ISGR");
  pclAddFieldLens[0] = 10;
  pclAddFieldLens[1] = 0;
  pclAddFieldLens[2] = 0;
  pclAddFieldLens[3] = 0;
  pclAddFieldLens[4] = 0;

  dbg(DEBUG,"%05d: MVA_TAB -Array will be created",__LINE__);
  
  dbg(DEBUG,"%05d: MVA_FIELDS: <%s>, &rgMvaArray: <%d>, pclSelection: <%s>",__LINE__,MVA_FIELDS_TAB,&rgMvaArray,pclSelection);
  ilRC = SetArrayInfo("MVATAB","MVATAB",MVA_FIELDS_TAB,
                      pclAddFields,pclAddFieldLens,&rgMvaArray,pclSelection,TRUE);

  if(ilRC != RC_SUCCESS)
    {
      dbg(DEBUG," *****************************SetArrayInfo MVA_TAB failed <%d>",ilRC);
    }
  else
    {
      dbg(DEBUG," ****************************SetArrayInfo MVA_TAB succeeded <%d>",ilRC);
    }
  /********************** now create ArrayIndex ***********************/
  dbg(DEBUG,"%05d: MVA_TAB Index will be created",__LINE__);

  strcpy(rgMvaArray.crIdx01Name,"MVA_ISGR");
  ilRC = CEDAArrayCreateSimpleIndexUp(&rgMvaArray.rrArrayHandle,
				      rgMvaArray.crArrayName,
				      &rgMvaArray.rrIdx01Handle,
				      rgMvaArray.crIdx01Name,"ISGR");

  dbg(DEBUG,"%05d: Now InitFieldIndex",__LINE__);

  ilRC = InitFieldIndex();
  if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%05d: InitFieldIndex failed",__LINE__);
    }

  dbg(DEBUG,"%05d: Now SetGroupFlag",__LINE__);
  ilRC = SetGroupFlag();

  dbg(DEBUG,"%05d: end of CreateCEDAArrays",__LINE__);
  return (ilRC);
} /*end of CreateCEDAArrays*/



/******************************************************************************/
/* The  SetArrayInfo                                                          */
/******************************************************************************/
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, 
                        char *pcpArrayFieldList,char *pcpAddFields,
                        long *pcpAddFieldLens,ARRAYINFO *prpArrayInfo, 
                        char *pcpSelection,BOOL bpFill)
{
  int    ilRc = RC_SUCCESS;                /* Return code */
  int ilLoop = 0;
  char clOrderTxt[4];

    

  dbg(DEBUG,"%05d: *pcpArrayName: <%s>, *pcpTableName: <%s>, *pcpArrayFieldList: <%s>, *pcpSelection: <%s> ", __LINE__,pcpArrayName,pcpTableName,pcpArrayFieldList, pcpSelection);

  if(prpArrayInfo == NULL)
    {
      dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
      ilRc = RC_FAIL;
    }else
      {
    
        memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

        prpArrayInfo->rrArrayHandle = -1;
        prpArrayInfo->rrIdx01Handle = -1;

        if(pcpArrayName != NULL)
	  {
            if(strlen(pcpArrayName) <= ARR_NAME_LEN)
	      {
                strcpy(prpArrayInfo->crArrayName,pcpArrayName);
	      }/* end of if */
	  }/* end of if */
      }/* end of if */

  if(ilRc == RC_SUCCESS)
    {
      prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);
      if (pcpAddFields != NULL)
        {
	  prpArrayInfo->lrArrayFieldCnt += get_no_of_items(pcpAddFields);
        }

      prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
      if(prpArrayInfo->plrArrayFieldOfs == NULL)
        {
	  dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
	  ilRc = RC_FAIL;
        }/* end of if */
    }/* end of if */

  if(ilRc == RC_SUCCESS)
    {
      prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
      if(prpArrayInfo->plrArrayFieldLen == NULL)
        {
	  dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
	  ilRc = RC_FAIL;
        }else{
	  *(prpArrayInfo->plrArrayFieldLen) = -1;
        }/* end of if */
    }/* end of if */

  if(ilRc == RC_SUCCESS)
    {
      ilRc = GetRowLength(&cgHopo[0],pcpTableName,pcpArrayFieldList,&(prpArrayInfo->lrArrayRowLen));
      if(ilRc != RC_SUCCESS)
	{
	  dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
	}/* end of if */
    }/* end of if */

  if(ilRc == RC_SUCCESS)
    {
      prpArrayInfo->lrArrayRowLen+=100;
      prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+10));
      if(prpArrayInfo->pcrArrayRowBuf == NULL)
	{
	  ilRc = RC_FAIL;
	  dbg(TRACE,"SetArrayInfo: calloc failed");
	}/* end of if */
    }/* end of if */

  if(ilRc == RC_SUCCESS)
    {
      prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx01RowLen+1));
      if(prpArrayInfo->pcrIdx01RowBuf == NULL)
	{
	  ilRc = RC_FAIL;
	  dbg(TRACE,"SetArrayInfo: calloc failed");
	}/* end of if */
    }/* end of if */

  if(ilRc == RC_SUCCESS)
    {
      dbg(TRACE,"CEDAArrayCreate follows Array <%s> Table <%s>",pcpArrayName,pcpTableName);
      ilRc = CEDAArrayCreate(&(prpArrayInfo->rrArrayHandle),
			     pcpArrayName,pcpTableName,pcpSelection,pcpAddFields,
			     pcpAddFieldLens,pcpArrayFieldList,
			     &(prpArrayInfo->plrArrayFieldLen[0]),
			     &(prpArrayInfo->plrArrayFieldOfs[0]));

      if(ilRc != RC_SUCCESS)
        {
	  dbg(TRACE,"SetArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
        }/* end of if */
    }/* end of if */

  if(pcpArrayFieldList != NULL)
    {
      if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
	{
	  strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
	}/* end of if */
    }/* end of if */
  if(pcpAddFields != NULL)
    {
      if(strlen(pcpAddFields) + 
	 strlen(prpArrayInfo->crArrayFieldList) <= (size_t)ARR_FLDLST_LEN)
	{
	  strcat(prpArrayInfo->crArrayFieldList,",");
	  strcat(prpArrayInfo->crArrayFieldList,pcpAddFields);
	  dbg(TRACE,"<%s>",prpArrayInfo->crArrayFieldList);
	}/* end of if */
    }/* end of if */


  if(ilRc == RC_SUCCESS && bpFill)
    {
      ilRc = CEDAArrayFill(&(prpArrayInfo->rrArrayHandle),pcpArrayName,NULL);
      if(ilRc != RC_SUCCESS)
        {
	  dbg(TRACE,"SetArrayInfo: CEDAArrayFill failed <%d>",ilRc);
        }/* end of if */
    }/* end of if */

  return(ilRc);
} /*end SetArrayInfo */


/******************************************************************************/
/* The FindItemInArrayList                                                    */
/******************************************************************************/
static int FindItemInArrayList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos)
{
  int ilRc = RC_SUCCESS;

  ilRc = FindItemInList(pcpInput,pcpPattern,cpDel,ipLine,ipCol,ipPos);
  *ipLine = (*ipLine) -1;
    
  return ilRc;
}/* FindItmeInArrayList*/



/******************************************************************************/
/* The InitFieldIndex                                                         */
/******************************************************************************/
static int InitFieldIndex()
{
  int ilRC = RC_SUCCESS;
  int ilLine = 0;
  int ilCol = 0;
  int ilPos = 0;
 
  /*********** DEVTAB-FIELDS ***************/

  FindItemInArrayList(DEV_FIELDS,"GRPN",',',&igDevGRPN,&ilCol,&ilPos);
  FindItemInArrayList(DEV_FIELDS,"DEVN",',',&igDevDEVN,&ilCol,&ilPos);
  FindItemInArrayList(DEV_FIELDS,"URNO",',',&igDevURNO,&ilCol,&ilPos);
  FindItemInArrayList(DEV_FIELDS,"DADR",',',&igDevDADR,&ilCol,&ilPos);
  FindItemInArrayList(DEV_FIELDS,"ADID",',',&igDevADID,&ilCol,&ilPos);

  /*********** MVATAB-FIELDS ***************/

  FindItemInArrayList(MVA_FIELDS,"URNO",',',&igMvaURNO,&ilCol,&ilPos);
  FindItemInArrayList(MVA_FIELDS,"HOPO",',',&igMvaHOPO,&ilCol,&ilPos);
  FindItemInArrayList(MVA_FIELDS,"USEC",',',&igMvaUSEC,&ilCol,&ilPos);
  FindItemInArrayList(MVA_FIELDS,"USEU",',',&igMvaUSEU,&ilCol,&ilPos);
  FindItemInArrayList(MVA_FIELDS,"DEVN",',',&igMvaDEVN,&ilCol,&ilPos);
  FindItemInArrayList(MVA_FIELDS,"MPFC",',',&igMvaMPFC,&ilCol,&ilPos);
  FindItemInArrayList(MVA_FIELDS,"MVSB",',',&igMvaMVSB,&ilCol,&ilPos);
  FindItemInArrayList(MVA_FIELDS,"MVSE",',',&igMvaMVSE,&ilCol,&ilPos);
  FindItemInArrayList(MVA_FIELDS,"MVAB",',',&igMvaMVAB,&ilCol,&ilPos);
  FindItemInArrayList(MVA_FIELDS,"MVAE",',',&igMvaMVAE,&ilCol,&ilPos);
  FindItemInArrayList(MVA_FIELDS,"RURN",',',&igMvaRURN,&ilCol,&ilPos);
  FindItemInArrayList(MVA_FIELDS,"PGNO",',',&igMvaPGNO,&ilCol,&ilPos);
  FindItemInArrayList(MVA_FIELDS,"OBJN",',',&igMvaOBJN,&ilCol,&ilPos);
  FindItemInArrayList(MVA_FIELDS,"REPC",',',&igMvaREPC,&ilCol,&ilPos);
  FindItemInArrayList(MVA_FIELDS,"BEME",',',&igMvaBEME,&ilCol,&ilPos);
  FindItemInArrayList(MVA_FIELDS,"ISGR",',',&igMvaISGR,&ilCol,&ilPos);

  return (ilRC);
}



/******************************************************************************/
/* The  GetRowLength                                                          */
/******************************************************************************/
static int GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList, long *plpLen)
{
  int  ilRc = RC_SUCCESS;            /* Return code */
  int  ilNoOfItems = 0;
  int  ilLoop = 0;
  long llFldLen = 0;
  long llRowLen = 0;
  char clFina[8] = "\0";
    
  ilNoOfItems = get_no_of_items(pcpFieldList);

  ilLoop = 1;
  do{
    ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
    if(ilRc > 0)
      {
	ilRc = GetFieldLength(pcpHopo,pcpTana,&clFina[0],&llFldLen);
	if(ilRc == RC_SUCCESS)
	  {
	    llRowLen++;
	    llRowLen += llFldLen;
	  }/* end of if */
      }/* end of if */
    ilLoop++;
  }while((ilRc == RC_SUCCESS) && (ilLoop <= ilNoOfItems));

  if(ilRc == RC_SUCCESS)
    {
      *plpLen = llRowLen;
    }/* end of if */

  return(ilRc);
    
} /* end of GetRowLength */


/******************************************************************************/
/* The  GetFieldlength                                                          */
/******************************************************************************/
static int GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina, long *plpLen)
{
  int  ilRc = RC_SUCCESS;            /* Return code */
  int  ilCnt = 0;
  char clTaFi[32] = "\0";
  char clFele[16] = "\0";
  char clFldLst[16] = "\0";

  ilCnt = 1;
  sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
  sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerst�rt */

  /*dbg(TRACE,"GetFieldLength Tana <%s> Fina <%s>",pcpTana,pcpFina);*/

  /*    dbg(TRACE,"%05d:\n \npcgTabEnd[0] <%s>, \n&clFldLst[0]: <%s>, \n&clTaFi[0]: <%s>, \nFELE, \n&clFele[0] <%s>, \n&ilCnt: %d \n",__LINE__, &pcgTabEnd[0],&clFldLst[0],&clTaFi[0],&clFele[0],ilCnt);
   */

  ilRc = syslibSearchSystabData(&pcgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
    

  switch (ilRc)
    {
    case RC_SUCCESS :
      *plpLen = atoi(&clFele[0]);
      /* dbg(DEBUG,"GetFieldLength: <%s,%s,%s> <%d>",pcpHopo,pcpTana,pcpFina,*plpLen);*/
      break;

    case RC_NOT_FOUND :
      dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s>",ilRc,&clTaFi[0]);
      break;

    case RC_FAIL :
      dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);

      break;

    default :
      dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
      break;
    }/* end of switch */

  return(ilRc);
    
} /* end of GetFieldLength */

/******************************************************************************/
/*  The SaveIndexInfo routine                                                 */
/*****************************************************************************/
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo)
{
  int    ilRC       = RC_SUCCESS ;
  long   llRow      = 0 ;
  FILE  *prlFile    = NULL ;
  char   clDel      = ',' ;
  char   clFile[512] = "\0";
  char  *pclTrimBuf = NULL ;
  long llRowCount;
  int ilCount = 0;
  char  *pclTestBuf = NULL ;


  dbg (DEBUG,"SaveIndexInfo: SetArrayInfo <%s>",prpArrayInfo->crArrayName) ;

  if (ilRC == RC_SUCCESS)
    {
      sprintf (&clFile[0],"%s/%s_%s.info",getenv("DBG_PATH"),mod_name,prpArrayInfo->crArrayName) ;

      errno = 0 ;
      prlFile = fopen (&clFile[0], "w") ;
      if (prlFile == NULL)
	{
	  dbg (DEBUG, "SaveIndexInfo: fopen <%s> failed <%d-%s>", &clFile[0], errno, strerror(errno)) ;
	  ilRC = RC_FAIL ;
	} /* end of if */
    } /* end of if */

  if (ilRC == RC_SUCCESS)
    {
      {
	dbg (DEBUG, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ;      
	dbg (DEBUG, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ;  
	dbg (DEBUG, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ;  
      }

      fprintf (prlFile, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ; fflush(prlFile) ;
      fprintf (prlFile, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ; fflush(prlFile) ;
      fprintf (prlFile, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ; fflush(prlFile) ;

      {
	dbg(DEBUG,"IndexData") ; 
      }
      fprintf(prlFile,"IndexData\n"); fflush(prlFile);

      dbg(DEBUG,"FindKey <%s>",prpArrayInfo->pcrIdx01RowBuf);
      *prpArrayInfo->pcrIdx01RowBuf = '\0';
      llRow = ARR_FIRST ;
      do
	{
	  ilRC = CEDAArrayFindKey (&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), &(prpArrayInfo->rrIdx01Handle), prpArrayInfo->crIdx01Name, &llRow, prpArrayInfo->lrIdx01RowLen, prpArrayInfo->pcrIdx01RowBuf);
	  if (ilRC == RC_SUCCESS)
	    {
    
	      dbg(DEBUG, "SaveIndexInfo: Key <%ld> <%s>",llRow,prpArrayInfo->pcrIdx01RowBuf) ;
    

	      if (strlen (prpArrayInfo->pcrIdx01RowBuf) == 0)
		{
		  ilRC = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
		  if(ilRC == RC_SUCCESS)
		    {
		      dbg (DEBUG, "SaveIndexInfo: Row <%ld> <%s>", llRow,prpArrayInfo->pcrArrayRowBuf) ;
		    } /* end of if */
		} /* end of if */

	      pclTrimBuf = strdup (prpArrayInfo->pcrIdx01RowBuf) ;
	      if (pclTrimBuf != NULL)
		{
		  ilRC = GetDataItem (pclTrimBuf,prpArrayInfo->pcrIdx01RowBuf, 1, clDel, "", "  ") ;
		  if (ilRC > 0)
		    {
		      ilRC = RC_SUCCESS ;
		      fprintf (prlFile,"Key            <%ld><%s>\n",llRow,pclTrimBuf) ; fflush(prlFile) ;
		    } /* end of if */

		  free (pclTrimBuf) ;
		  pclTrimBuf = NULL ;
		} /* end of if */

	      llRow = ARR_NEXT;
	    }
	  else
	    {
	      dbg (DEBUG, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRC) ;
	    } /* end of if */
	} while (ilRC == RC_SUCCESS) ;

      {
	dbg (DEBUG,"ArrayData") ;
      }
      fprintf (prlFile,"ArrayData\n"); fflush(prlFile);

      llRow = ARR_FIRST ;

      CEDAArrayGetRowCount(&(prpArrayInfo->rrArrayHandle),&(prpArrayInfo->crArrayName[0]),&llRowCount);

      llRow = ARR_FIRST;
      dbg(DEBUG,"Array  <%s> data follows Rows %ld",
	  prpArrayInfo->crArrayName,llRowCount);
      do
	{
	  ilRC = CEDAArrayGetRowPointer(&(prpArrayInfo->rrArrayHandle),
					&(prpArrayInfo->crArrayName[0]),llRow,(void *)&pclTestBuf);
	  if (ilRC == RC_SUCCESS)
            {
	      for( ilCount = 0; ilCount < prpArrayInfo->lrArrayFieldCnt; ilCount++)
                {
                
		  fprintf (prlFile,"<%s>",&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]) ; 
		  fflush(prlFile) ;
		  dbg (DEBUG, "SaveIndexInfo: ilCount <%ld> <%s>", ilCount,&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]) ;
                }
	      fprintf (prlFile,"\n") ; 
	      dbg (DEBUG, "\n") ;
	      fflush(prlFile) ;
	      llRow = ARR_NEXT;
            }
	  else
            {
	      dbg(DEBUG,"GetRowPointer failed RC=%d",ilRC);
            }
	} while (ilRC == RC_SUCCESS);

      if (ilRC == RC_NOTFOUND)
	{
	  ilRC = RC_SUCCESS ;
	} /* end of if */
    } /* end of if */

  if (prlFile != NULL)
    {
      fclose(prlFile) ;
      prlFile = NULL ;
  
      dbg (DEBUG, "SaveIndexInfo: event saved - file <%s>", &clFile[0]) ;
  
    } /* end of if */

  return (ilRC) ;

}/* end of SaveIndexInfo */


/******************************************************************************/
/*  The ShowGroupDisplays routine                                                 */
/*****************************************************************************/
static int ShowGroupDisplays(char *pcpGroup)
{
  int ilRC = RC_SUCCESS;
  long llRowNumDEV = ARR_FIRST;
  char *pclRowDEV = NULL;
  char pclDevDEVN[32] = "\0";

  while(CEDAArrayFindRowPointer(&(rgDevArray.rrArrayHandle),
				&(rgDevArray.crArrayName[0]),
				&(rgDevArray.rrIdx01Handle),
				&(rgDevArray.crIdx01Name[0]),
				pcpGroup,&llRowNumDEV,
				(void *) &pclRowDEV ) == RC_SUCCESS)
    {
      strcpy(pclDevDEVN,FIELDVAL(rgDevArray,pclRowDEV,igDevDEVN));
      dbg(DEBUG,"%05d: Group: <%s>, DEVN: <%s>",__LINE__,pcpGroup,pclDevDEVN);
      llRowNumDEV = ARR_NEXT;
    }

  return (ilRC);
}


/******************************************************************************/
/*  The GetGroupList routine                                                 */
/*****************************************************************************/
static int GetGroupList()
{
  int ilRC = RC_SUCCESS;
  short slCursor = 0;
  short slSqlFunc = 0;
  char clSqlBuf[4096] ="\0";
  char clData[4096] = "\0";
  int ilCount = 0;
  int ilLength = 0;


  /*sprintf(clSqlBuf,"SELECT COUNT (DISTINCT GRPN) FROM DEVTAB");*/
  strcpy(clSqlBuf,"SELECT COUNT (DISTINCT ADID) FROM DEVTAB");
  dbg(TRACE,"%05d: clSqlBuf: <%s>",__LINE__,clSqlBuf);    

  slSqlFunc= START;
  slCursor = 0;
  ilRC = sql_if(slSqlFunc,&slCursor,clSqlBuf,clData);
  if (ilRC == RC_FAIL) 
    {
      dbg(DEBUG, "%05d:SQL Buffer<%s>",__LINE__, clSqlBuf);
      get_ora_err(-1, cgErrMsg);
      dbg(TRACE, "Oracle Error <%s>", cgErrMsg);
    }                           
  dbg(TRACE,"%05d: clData: <%s>,ilRC: %d",__LINE__,clData,ilRC);
  close_my_cursor(&slCursor);
  
  ilCount = atoi(clData);

  if (cgGroupList == NULL)
    {
      cgGroupList = (char*) malloc(XXS_BUF * ilCount);
      cgGroupList[0] ='\0';
    }
  else if (strlen(cgGroupList) < ilCount * XXS_BUF)
    {
      free(cgGroupList);
      cgGroupList = (char*) malloc(XXS_BUF * ilCount);
      cgGroupList[0] ='\0'; 
    }
  else
    {
      cgGroupList[0] = '\0';
    }

  /*sprintf(clSqlBuf,"SELECT DISTINCT GRPN FROM DEVTAB");*/
  strcpy(clSqlBuf,"SELECT DISTINCT ADID FROM DEVTAB");
  dbg(TRACE,"%05d: clSqlBuf: <%s>",__LINE__,clSqlBuf);    

  slSqlFunc= START;
  slCursor = 0;

  while (sql_if(slSqlFunc,&slCursor,clSqlBuf,clData) == RC_SUCCESS)
    {
      
      TrimRight(clData);
      strcat(cgGroupList,"'");
      strcat(cgGroupList,clData);
      strcat(cgGroupList,"',");
      slSqlFunc = NEXT;
    }
  close_my_cursor(&slCursor);


  ilLength = strlen(cgGroupList);
  cgGroupList[ilLength-1] = '\0';
  if (ilLength > ilCount*XXS_BUF)
    {
      dbg(TRACE,"%05d: The Application will crash !!!!!!!!!! SOON !!!!!!!",__LINE__);
    }
  else
    {
      dbg(DEBUG,"%05d: Dies ist die GroupList: <%s>",__LINE__,cgGroupList);
    }
  dbg(DEBUG,"%05d: GetGroupList ENDE ***************** ",__LINE__);

  return (ilRC);
}



/******************************************************************************/
/* The TrimRight routine                                                      */
/******************************************************************************/
static void TrimRight(char *pcpBuffer)
{
  char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];
  while(isspace(*pclBlank) && pclBlank != pcpBuffer)
    {
      *pclBlank = '\0';
      pclBlank--;
    }
}/* end of TrimRight*/


/******************************************************************************/
/* The TimeToStr routine                                                      */
/******************************************************************************/
static int TimeToStr(char *pcpTime,time_t lpTime) 
{                                                                              
  struct tm *_tm;
  char   _tmpc[6];
    
  _tm = (struct tm *)localtime(&lpTime);
  /*_tm = (struct tm *)gmtime(&lpTime);*/
  /*      strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",_tm); */
 

  sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
	  _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
	  _tm->tm_min,_tm->tm_sec);

  return RC_SUCCESS;  
                        
}     /* end of TimeToStr */


/******************************************************************************/
/* The SetGroupFlag routine                                                      */
/******************************************************************************/
static int SetGroupFlag()
{
  int ilRC = RC_SUCCESS;
  long llRowNumMVA = ARR_FIRST;
  char *pclRowMVA = NULL;

  while (CEDAArrayGetRowPointer(&rgMvaArray.rrArrayHandle,
				rgMvaArray.crArrayName,
				llRowNumMVA,(void *)&pclRowMVA) == RC_SUCCESS)
    {
      ilRC = CEDAArrayPutField(&rgMvaArray.rrArrayHandle,rgMvaArray.crArrayName,NULL,"ISGR",llRowNumMVA,"GROUP");
      llRowNumMVA = ARR_NEXT;
    }

  return (ilRC);
}


/******************************************************************************/
/* The ExtractGroups routine                                                      */
/******************************************************************************/
static int ExtractGroups()
{
  int ilRC = RC_SUCCESS;
  long llRowNumMVA = ARR_FIRST;
  long llRowNumDEV = ARR_FIRST;
  long llRowNumDEVLast = ARR_LAST;
  char *pclRowMVA = NULL;
  char *pclRowDEV = NULL;
  char pclMvaURNO[34] = "\0";
  char pclMvaHOPO[34] = "\0";
  char pclMvaUSEC[34] = "\0";
  char pclMvaUSEU[34] = "\0";
  char pclGroupName[34] = "\0";
  char pclMvaMPFC[34] = "\0";
  char pclMvaMVSB[34] = "\0";
  char pclMvaMVSE[34] = "\0";
  char pclMvaMVAB[34] = "\0";
  char pclMvaMVAE[34] = "\0";
  char pclMvaRURN[34] = "\0";
  char pclMvaPGNO[34] = "\0";
  char pclMvaOBJN[34] = "\0";
  char pclMvaREPC[34] = "\0";
  char pclMvaDEVN[34] = "\0";
  char pclMvaBEME[130] = "\0";
  char pclRowBuffer[4096];
  char pclSqlBuf[RES_BUF_SIZE] = "\0";
  char pclBegin[34] = "\0";
  char pclEnd[34] = "\0";
  char pclNowTime[34] = "\0";


  strcpy(pclSqlBuf,"WHERE STAT ='U'");

  ilRC = CEDAArrayRefill(&rgDevArray.rrArrayHandle,rgDevArray.crArrayName,pclSqlBuf,DEV_FIELDS,ARR_FIRST);
  if (ilRC != RC_SUCCESS)
    {
      dbg(DEBUG,"%05d: Refill CEDAArray failed",__LINE__);
    }


  TimeToStr(pclBegin,time(NULL));
  AddSecondsToCEDATime(pclBegin,300,1);
  TimeToStr(pclEnd,time(NULL));

  dbg(DEBUG,"%05d: Now refill MVAArray !!",__LINE__);

  ilRC = GetGroupList();

  sprintf(pclSqlBuf,"WHERE DEVN IN(%s) AND MVSB <= '%s' AND MVSE > '%s' AND MVAB = ' '",cgGroupList,pclBegin,pclEnd);

  ilRC = CEDAArrayRefill(&rgMvaArray.rrArrayHandle,rgMvaArray.crArrayName,pclSqlBuf,MVA_FIELDS,ARR_FIRST);
  if (ilRC != RC_SUCCESS)
    {
      dbg(DEBUG,"%05d: Refill CEDAArray failed",__LINE__);
    }

  ilRC = SetGroupFlag();

 
  while (CEDAArrayFindRowPointer(&(rgMvaArray.rrArrayHandle),
				 &(rgMvaArray.crArrayName[0]),
				 &(rgMvaArray.rrIdx01Handle),
				 &(rgMvaArray.crIdx01Name[0]),
				 "GROUP",&llRowNumMVA,
				 (void *) &pclRowMVA ) == RC_SUCCESS)
    {
      dbg(DEBUG,"%05d: Mva-Record found **************",__LINE__);
      /*ilRC = SaveIndexInfo(&rgMvaArray);*/
      /*ilRC = SaveIndexInfo(&rgDevArray);*/

      strcpy(pclMvaHOPO,FIELDVAL(rgMvaArray,pclRowMVA,igMvaHOPO));
      strcpy(pclMvaUSEC,"MPGHDL");
      strcpy(pclMvaUSEU," ");
      strcpy(pclGroupName,FIELDVAL(rgMvaArray,pclRowMVA,igMvaDEVN));
      TrimRight(pclGroupName);
      strcpy(pclMvaMPFC,FIELDVAL(rgMvaArray,pclRowMVA,igMvaMPFC));
      strcpy(pclMvaMVSB,FIELDVAL(rgMvaArray,pclRowMVA,igMvaMVSB));
      strcpy(pclMvaMVSE,FIELDVAL(rgMvaArray,pclRowMVA,igMvaMVSE));
      strcpy(pclMvaMVAB,FIELDVAL(rgMvaArray,pclRowMVA,igMvaMVAB));
      strcpy(pclMvaMVAE,FIELDVAL(rgMvaArray,pclRowMVA,igMvaMVAE));
      strcpy(pclMvaRURN,FIELDVAL(rgMvaArray,pclRowMVA,igMvaRURN));
      strcpy(pclMvaPGNO,FIELDVAL(rgMvaArray,pclRowMVA,igMvaPGNO));
      strcpy(pclMvaOBJN,FIELDVAL(rgMvaArray,pclRowMVA,igMvaOBJN));
      strcpy(pclMvaREPC,FIELDVAL(rgMvaArray,pclRowMVA,igMvaREPC));
      strcpy(pclMvaBEME,FIELDVAL(rgMvaArray,pclRowMVA,igMvaBEME));
     
      ilRC = CEDAArrayDeleteRow(&rgMvaArray.rrArrayHandle,rgMvaArray.crArrayName,llRowNumMVA);
      

      dbg(DEBUG,"%05d: pclGroupName: <%s>",__LINE__,pclGroupName);
      while(CEDAArrayFindRowPointer(&(rgDevArray.rrArrayHandle),
				    &(rgDevArray.crArrayName[0]),
				    &(rgDevArray.rrIdx01Handle),
				    &(rgDevArray.crIdx01Name[0]),
				    pclGroupName,&llRowNumDEV,
				    (void *) &pclRowDEV) == RC_SUCCESS)
	{
	  dbg(DEBUG,"%05d: GroupNameFound IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII",__LINE__);
	  strcpy(pclMvaDEVN,FIELDVAL(rgDevArray,pclRowDEV,igDevDEVN));
	  ilRC = GetNextValues(pclMvaURNO,1);
	  sprintf(pclRowBuffer,"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
		  pclMvaURNO,pclMvaHOPO,pclMvaUSEC,pclMvaUSEU,pclMvaDEVN,
		  pclMvaMPFC,pclMvaMVSB,pclMvaMVSE,pclMvaMVAB,pclMvaMVAE,
		  pclMvaRURN,pclMvaPGNO,pclMvaOBJN,pclMvaREPC,pclMvaBEME);
	  ilRC = CEDAArrayAddRowPart(&rgMvaArray.rrArrayHandle, rgMvaArray.crArrayName,&llRowNumDEVLast,
				     MVA_FIELDS_TAB, pclRowBuffer);
	  llRowNumDEV = ARR_NEXT;
	}

    }

  dbg(DEBUG,"%05d: CEDAArrayWriteDB MVATAB",__LINE__);
  ilRC = CEDAArrayWriteDB(&rgMvaArray.rrArrayHandle,rgMvaArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
  if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%05d: CEDAArrayWriteDB MVATAB failed !!",__LINE__);
      rollback();
    }
  else
    { 
      commit_work();
    }

  TimeToStr(pclNowTime,time(NULL));
  dbg(DEBUG,"%05d: Nowtime: <%s>",__LINE__,pclNowTime);

  return (ilRC);
}

/******************************************************************************/
/* The RUNCmd routine                                                      */
/******************************************************************************/
static int RUNCmd()
{
  int ilRC = RC_SUCCESS;
  char pclNowTime[32] = "\0";

  dbg(TRACE,"%05d: ============================= START of RUN ===============================",__LINE__);
  ilRC = ExtractGroups();
  ilRC = StartVideos();
  ilRC = StopVideos();
  dbg(TRACE,"%05d: ============================= END of RUN ================================",__LINE__);

  return (ilRC);
}



/******************************************************************************/
/*  The GetUpDevList routine                                                 */
/*****************************************************************************/
static int GetUpDevList()
{
  int ilRC = RC_SUCCESS;
  short slCursor = 0;
  short slSqlFunc = 0;
  char clSqlBuf[4096] ="\0";
  char clData[4096] = "\0";
  int ilCount = 0;
  int ilLength = 0;


  strcpy(clSqlBuf,"SELECT COUNT (DISTINCT DEVN) FROM DEVTAB WHERE STAT = 'U'");
  dbg(TRACE,"%05d: clSqlBuf: <%s>",__LINE__,clSqlBuf);    

  slSqlFunc= START;
  slCursor = 0;
  ilRC = sql_if(slSqlFunc,&slCursor,clSqlBuf,clData);
  if (ilRC == RC_FAIL) 
    {
      dbg(DEBUG, "%05d:SQL Buffer<%s>",__LINE__, clSqlBuf);
      get_ora_err(-1, cgErrMsg);
      dbg(TRACE, "Oracle Error <%s>", cgErrMsg);
    }                           
  dbg(TRACE,"%05d: clData: <%s>,ilRC: %d",__LINE__,clData,ilRC);
  close_my_cursor(&slCursor);
  
  ilCount = atoi(clData);

  if (cgUpDevList == NULL)
    {
      cgUpDevList = (char*) malloc(XXS_BUF * ilCount);
      cgUpDevList[0] ='\0';
    }
  else if (strlen(cgUpDevList) < ilCount * XXS_BUF)
    {
      free(cgUpDevList);
      cgUpDevList = (char*) malloc(XXS_BUF * ilCount);
      cgUpDevList[0] ='\0'; 
    }
  else
    {
      cgUpDevList[0] = '\0';
    }

  strcpy(clSqlBuf,"SELECT DISTINCT DEVN FROM DEVTAB WHERE STAT = 'U'");
  dbg(TRACE,"%05d: clSqlBuf: <%s>",__LINE__,clSqlBuf);    

  slSqlFunc= START;
  slCursor = 0;

  while (sql_if(slSqlFunc,&slCursor,clSqlBuf,clData) == RC_SUCCESS)
    {
      TrimRight(clData);
      strcat(cgUpDevList,"'");
      strcat(cgUpDevList,clData);
      strcat(cgUpDevList,"',");
      slSqlFunc = NEXT;
    }
  close_my_cursor(&slCursor);


  ilLength = strlen(cgUpDevList);
  cgUpDevList[ilLength-1] = '\0';
  if (ilLength > ilCount*XXS_BUF)
    {
      dbg(TRACE,"%05d: The Application will crash !!!!!!!!!! SOON !!!!!!!",__LINE__);
    }
  else
    {
      dbg(DEBUG,"%05d: Dies ist die UpDevList: <%s>",__LINE__,cgUpDevList);
    }
  dbg(DEBUG,"%05d: GetUpDevList ENDE ***************** ",__LINE__);

  return (ilRC);
}



/******************************************************************************/
/*  The StartStopVideos() routine                                                 */
/*****************************************************************************/
static int StartVideos()
{
  int ilRC = RC_SUCCESS;
  int ilQ = 0;
  long llRowNumMVA = ARR_FIRST;
  char *pclRowMVA = NULL;
  long llRowNumDEV = ARR_FIRST;
  char *pclRowDEV = NULL;
  char pclBegin[32] = "\0";
  char pclEnd[32] = "\0";
  char pclMvaMVSB[32] = "\0";
  char pclMvaDEVN[32] = "\0";
  char pclMvaMPFC[32] = "\0";
  char pclMvfMPFN[32] = "\0";
  char pclDevGRPN[32] = "\0";
  char pclDevURNO[32] = "\0";
  char pclDevDADR[32] = "\0";
  char pclMvaPGNO[32] = "\0";
  char pclMvaOBJN[32] = "\0";
  char pclMvaREPC[32] = "\0";
  char pclFields[4096] = "\0";
  char pclData[4096] = "\0";
  short slCursor = 0;
  short slSqlFunc = 0;
  char clSqlBuf[4096] ="\0";
  char pclSqlBuf[RES_BUF_SIZE] ="\0";
  char clData[4096] = "\0";
  char pclNowTime[32] = "\0";

  dbg(DEBUG,"******************* this ist StartVideos ****************");
  TimeToStr(pclBegin,time(NULL));
  AddSecondsToCEDATime(pclBegin,300,1);
  TimeToStr(pclEnd,time(NULL));

  dbg(TRACE,"%05d: Now refill MVAArray !!",__LINE__);

  /*ilRC = GetUpDevList();*/

  /*sprintf(pclSqlBuf,"WHERE DEVN IN(%s) AND MVSB <= '%s' AND MVSE > '%s' AND MVAB = ' '",cgUpDevList,pclBegin,pclEnd);*/
  sprintf(pclSqlBuf,"WHERE MVSB <= '%s' AND MVSE > '%s' AND (MVAB = ' ' OR MVAB = '')",pclBegin,pclEnd);

  ilRC = CEDAArrayRefill(&rgMvaArray.rrArrayHandle,rgMvaArray.crArrayName,pclSqlBuf,MVA_FIELDS,ARR_FIRST);
  if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%05d: Refill CEDAArray failed",__LINE__);
    }
  else
    {
      dbg(TRACE,"%05d: Refill CEDAArray succeede",__LINE__);
    }

  TimeToStr(pclNowTime,time(NULL));
  TrimRight(pclNowTime);
#if 0
  dbg(DEBUG,"%05d: BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB",__LINE__);
  dbg(DEBUG,"%05d: BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB",__LINE__);
  ilRC = SaveIndexInfo(&rgMvaArray);
  dbg(DEBUG,"%05d: BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB",__LINE__);
  dbg(DEBUG,"%05d: BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB",__LINE__);
  llRowNumMVA = ARR_FIRST;
#endif

  while (CEDAArrayGetRowPointer(&rgMvaArray.rrArrayHandle,
				rgMvaArray.crArrayName,
				llRowNumMVA,(void *)&pclRowMVA) == RC_SUCCESS)
    {
      llRowNumMVA = ARR_CURRENT;

      dbg(DEBUG,"%05d: MVA-Row found++++++++++++++++++++++++++++",__LINE__);
      strcpy(pclMvaMVSB,FIELDVAL(rgMvaArray,pclRowMVA,igMvaMVSB));
      TrimRight(pclMvaMVSB);
      if (strcmp(pclMvaMVSB,pclNowTime) <= 0)
	{
	  strcpy(pclMvaDEVN,FIELDVAL(rgMvaArray,pclRowMVA,igMvaDEVN));
	  /*TrimRight(pclMvaDEVN);*/
	  llRowNumDEV = ARR_FIRST;
	  ilRC = CEDAArrayFindRowPointer(&(rgDevArray.rrArrayHandle),
					 &(rgDevArray.crArrayName[0]),
					 &(rgDevArray.rrIdx02Handle),
					 &(rgDevArray.crIdx02Name[0]),
					 pclMvaDEVN ,&llRowNumDEV,
					 (void *) &pclRowDEV );
	  if (ilRC != RC_SUCCESS)
	    {
	      dbg(DEBUG,"%05d: FindRowPointer Failed for pclMvaDEVN: <%s>",__LINE__,pclMvaDEVN);
	    }
	  else
	    {
	      strcpy(pclDevGRPN,FIELDVAL(rgDevArray,pclRowDEV,igDevGRPN));
	      TrimRight(pclDevGRPN);
	      ilQ = tool_get_q_id(pclDevGRPN);
	      dbg(DEBUG,"%05d: QueueID of <%s> : <%d>",__LINE__,pclDevGRPN,ilQ);

	    
	      strcpy(pclMvaMPFC,FIELDVAL(rgMvaArray,pclRowMVA,igMvaMPFC));
	      TrimRight(pclMvaMPFC);
	  
	      sprintf(clSqlBuf,"SELECT MPFN FROM MVFTAB WHERE MPFC = '%s'",pclMvaMPFC);
	      dbg(DEBUG,"%05d: clSqlBuf: <%s>",__LINE__,clSqlBuf);   

	      slSqlFunc= START;
	      slCursor = 0;
	      ilRC = sql_if(slSqlFunc,&slCursor,clSqlBuf,clData);
	      if (ilRC == RC_FAIL) 
		{
		  dbg(DEBUG, "%05d:SQL Buffer<%s>",__LINE__, clSqlBuf);
		  get_ora_err(-1, cgErrMsg);
		  dbg(TRACE, "Oracle Error <%s>", cgErrMsg);
		}                           
	      dbg(TRACE,"%05d: clData: <%s>,ilRC: %d",__LINE__,clData,ilRC);
	      close_my_cursor(&slCursor);
	      strcpy(pclMvfMPFN,clData);
	      TrimRight(pclMvfMPFN);

	      strcpy(pclMvaPGNO,FIELDVAL(rgMvaArray,pclRowMVA,igMvaPGNO));
	      TrimRight(pclMvaPGNO);
	      strcpy(pclMvaOBJN,FIELDVAL(rgMvaArray,pclRowMVA,igMvaOBJN));
	      TrimRight(pclMvaOBJN);
	      strcpy(pclMvaREPC,FIELDVAL(rgMvaArray,pclRowMVA,igMvaREPC));
	      TrimRight(pclMvaREPC);
	      strcpy(pclDevURNO,FIELDVAL(rgDevArray,pclRowDEV,igDevURNO));
	      TrimRight(pclDevURNO);
	      strcpy(pclDevDADR,FIELDVAL(rgDevArray,pclRowDEV,igDevDADR));
	      TrimRight(pclDevDADR);

	      strcpy(pclFields,"WHAT,URNO,DADR,DEVN,MPFN,PGNO,OBJN,REPC");
	      sprintf(pclData,"START,%s,%s,%s,%s,%s,%s,%s",pclDevURNO,pclDevDADR,pclMvaDEVN,pclMvfMPFN,pclMvaPGNO,pclMvaOBJN,pclMvaREPC);
	      dbg(DEBUG,"%05d: SendEvent: pclFields: <%s>",__LINE__,pclFields);
	      dbg(DEBUG,"%05d: SendEvent pclData: <%s>",__LINE__,pclData);

	      ilRC = SendEvent("MPG",ilQ,PRIORITY_4,NULL,NULL,pcgTwEnd,NULL,
			       pclFields,pclData,NULL,0);

	      if (ilRC = RC_SUCCESS)
		{
		  dbg(DEBUG,"%05d: SendEvent succeeded",__LINE__);
		}
	      else
		{
		  dbg(DEBUG,"%05d: SendEvent failed",__LINE__);
		}

	  
	      ilRC = CEDAArrayPutField(&rgMvaArray.rrArrayHandle,rgMvaArray.crArrayName,NULL,"MVAB",llRowNumMVA,pclNowTime);
	  
	    }
	}
      llRowNumMVA = ARR_NEXT;

    }

  dbg(DEBUG,"%05d: CEDAArrayWriteDB MVATAB",__LINE__);
  ilRC = CEDAArrayWriteDB(&rgMvaArray.rrArrayHandle,rgMvaArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
  if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%05d: CEDAArrayWriteDB MVATAB failed !!",__LINE__);
      rollback();
    }
  else
    { 
      commit_work();
    }

  dbg(DEBUG,"******************* this is the end of StartVideos ****************");

  return (ilRC);
}




/*********************************************************************
Function : SendEvent()
Paramter : IN: pcpCmd = command for cmdblk->command
           IN: ipModID = process-ID where the event is send to
       IN: ipPriority = priority for sending ( 1- 5, usuallay 3)
       IN: pcpTable = Name (3 letters) of cmdblk->obj_name (if
           necessary), will be expanded with "pcgTabEnd".
       IN: pcpTwStart = cmdblk->twstart
       IN: pcpTwEnd = cmdblk->twend (always HOMEAP,TABEND,processname)
       IN: pcpSelection = selection for event (cmdblk->data)
       IN: pcpFields = fieldlist (corresponding to pcpdata)
       IN: pcpData = datalist (comma separated, corresponding to 
                                   pcpFields)
       IN: pcpAddStruct = additional structure to be transmitted
       IN: ipAddStructSize = size of the additional structure
Return Code: RC_SUCCESS, RC_FAIL
Result:
Description: Sends an event to another CEDA-process using que(QUE_PUT).
             Sends the event in standard CEDA-format (BCHEAD,CMDBLK,
         selection,fieldlist,datalist) or sends a different
         data structure (special) at CMDBLK->data. !! Sends always
         only one type, standard OR special, with one event !!
*********************************************************************/
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
		     char *pcpTwStart, char *pcpTwEnd,
		     char *pcpSelection,char *pcpFields,char *pcpData,
		     char *pcpAddStruct,int ipAddStructSize)
{
  int     ilRc             = RC_FAIL;
  int     ilLen            = 0;
  EVENT   *prlOutEvent  = NULL;
  BC_HEAD *prlOutBCHead = NULL;
  CMDBLK  *prlOutCmdblk = NULL;

  if (pcpAddStruct == NULL)
    ipAddStructSize = 0;
  if (ipAddStructSize == 0)
    pcpAddStruct = NULL;

  /* size-calculation for prlOutEvent */
  ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
    strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 
    ipAddStructSize + 128;

  /* memory for prlOutEvent */
  if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
    {
      dbg(TRACE,"SendEvent: cannot malloc <%d>-bytes for outgoing event!",ilLen);
      prlOutEvent = NULL;
    }else{
      /* clear whole outgoing event */
      memset((void*)prlOutEvent, 0x00, ilLen);

      /* set event structure... */
      prlOutEvent->type         = SYS_EVENT;
      prlOutEvent->command    = EVENT_DATA;
      prlOutEvent->originator   = (short)mod_id;
      prlOutEvent->retry_count  = 0;
      prlOutEvent->data_offset  = sizeof(EVENT);
      prlOutEvent->data_length  = ilLen - sizeof(EVENT); 

      /* BC_HEAD-Structure... */
      prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
      /* prlOutBCHead->rc = (short)RC_SUCCESS;*/
      prlOutBCHead->rc = (short)NETOUT_NO_ACK;/*spaeter nur bei disconnect*/
      strncpy(prlOutBCHead->dest_name,mod_name,10);
      strncpy(prlOutBCHead->recv_name, "EXCO",10);
 
      /* Cmdblk-Structure... */
      prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
      strcpy(prlOutCmdblk->command,pcpCmd);
      if (pcpTable != NULL)
	{
	  strcpy(prlOutCmdblk->obj_name,pcpTable);
	  strcat(prlOutCmdblk->obj_name,pcgTabEnd);
	}
        
      /* setting tw_x entries */
      strncpy(prlOutCmdblk->tw_start,pcpTwStart,32);
      strncpy(prlOutCmdblk->tw_end,pcpTwEnd,32);
        
      /* means that no additional structure is used */
      /* STANDARD CEDA-ipcs between CEDA-processes */
      if (pcpAddStruct == NULL)
	{
	  /* setting selection inside event */
	  strcpy(prlOutCmdblk->data,pcpSelection);
	  /* setting field-list inside event */
	  strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1,pcpFields);
	  /* setting data-list inside event */
	  strcpy((prlOutCmdblk->data + (strlen(pcpSelection)+1) + (strlen(pcpFields)+1)),pcpData);
	}else{
	  /*an additional structure is used and will be copied to */
	  /*cmdblk + sizeof(CMDBLK).!!! No STANDARD CEDA-ipcs is used !!!! */
	  memcpy(prlOutCmdblk->data,(char*)pcpAddStruct,ipAddStructSize);    
	}

      /*DebugPrintEvent(DEBUG,prlOutEvent);*/ 
      /*snapit((char*)prlOutEvent,ilLen,outp);*/
      dbg(DEBUG,"SendEvent: sending event to mod_id <%d>",ipModID);

      if (ipModID != 0)
	{
	  if ((ilRc = que(QUE_PUT,ipModID,mod_id,ipPriority,ilLen,(char*)prlOutEvent))
	      != RC_SUCCESS)
	    {
	      dbg(TRACE,"SendEvent: QUE_PUT returns: <%d>", ilRc);
	      Terminate(1);
	    }
	}else{
	  dbg(TRACE,"SendEvent: mod_id = <%d>! Can't send!",ipModID);
	}
      /* free memory */
      free((void*)prlOutEvent); 
    }
  return ilRc;
} /* end of SendEvent*/






/******************************************************************************/
/*  The StopVideos() routine                                                 */
/*****************************************************************************/
static int StopVideos()
{
  int ilRC = RC_SUCCESS;
  int ilQ = 0;
  long llRowNumMVA = ARR_FIRST;
  char *pclRowMVA = NULL;
  long llRowNumDEV = ARR_FIRST;
  char *pclRowDEV = NULL;
  char pclBegin[32] = "\0";
  char pclEnd[32] = "\0";
  char pclMvaMVSB[32] = "\0";
  char pclMvaMVSE[32] = "\0";
  char pclMvaDEVN[32] = "\0";
  char pclMvaMPFC[32] = "\0";
  char pclMvfMPFN[32] = "\0";
  char pclDevGRPN[32] = "\0";
  char pclDevURNO[32] = "\0";
  char pclDevDADR[32] = "\0";
  char pclMvaPGNO[32] = "\0";
  char pclMvaOBJN[32] = "\0";
  char pclMvaREPC[32] = "\0";
  char pclFields[4096] = "\0";
  char pclData[4096] = "\0";
  short slCursor = 0;
  short slSqlFunc = 0;
  char clSqlBuf[4096] ="\0";
  char pclSqlBuf[RES_BUF_SIZE] ="\0";
  char clData[4096] = "\0";
  char pclNowTime[32] = "\0";

  dbg(DEBUG,"******************* this ist StopVideos ****************");
  TimeToStr(pclEnd,time(NULL));
  AddSecondsToCEDATime(pclEnd,300,1);

  dbg(TRACE,"%05d: Now refill MVAArray !!",__LINE__);

  /*ilRC = GetUpDevList();*/

  sprintf(pclSqlBuf,"WHERE MVSE <= '%s' AND MVAB <> ' ' AND (MVAE = ' ' OR MVAE = '')",pclEnd);
  /*sprintf(pclSqlBuf,"WHERE DEVN IN(%s) AND MVSE <= '%s' AND MVAB <> ' ' AND MVAE = ' '",cgUpDevList,pclEnd);*/

  ilRC = CEDAArrayRefill(&rgMvaArray.rrArrayHandle,rgMvaArray.crArrayName,pclSqlBuf,MVA_FIELDS,ARR_FIRST);
  if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%05d: Refill CEDAArray failed",__LINE__);
    }


  TimeToStr(pclNowTime,time(NULL));
  TrimRight(pclNowTime);
  llRowNumMVA = ARR_FIRST;

  while (CEDAArrayGetRowPointer(&rgMvaArray.rrArrayHandle,
				rgMvaArray.crArrayName,
				llRowNumMVA,(void *)&pclRowMVA) == RC_SUCCESS)
    {
      llRowNumMVA = ARR_CURRENT;

      /*dbg(DEBUG,"%05d: MVA-Row found++++++++++++++++++++++++++++",__LINE__);*/
      strcpy(pclMvaMVSE,FIELDVAL(rgMvaArray,pclRowMVA,igMvaMVSE));
      TrimRight(pclMvaMVSE);
      if (strcmp(pclMvaMVSE,pclNowTime) <= 0)
	{
	  strcpy(pclMvaDEVN,FIELDVAL(rgMvaArray,pclRowMVA,igMvaDEVN));
	  /*TrimRight(pclMvaDEVN);*/
	  llRowNumDEV = ARR_FIRST;

	  ilRC = CEDAArrayFindRowPointer(&(rgDevArray.rrArrayHandle),
					 &(rgDevArray.crArrayName[0]),
					 &(rgDevArray.rrIdx02Handle),
					 &(rgDevArray.crIdx02Name[0]),
					 pclMvaDEVN ,&llRowNumDEV,
					 (void *) &pclRowDEV );
	  if (ilRC != RC_SUCCESS)
	    {
	      dbg(DEBUG,"%05d: FindRowPointer Failed for pclMvaDEVN: <%s>",__LINE__,pclMvaDEVN);
	    }
	  else
	    {
	      strcpy(pclDevGRPN,FIELDVAL(rgDevArray,pclRowDEV,igDevGRPN));
	      TrimRight(pclDevGRPN);
	      ilQ = tool_get_q_id(pclDevGRPN);
	      dbg(DEBUG,"%05d: QueueID of <%s> : <%d>",__LINE__,pclDevGRPN,ilQ);

	    
	      strcpy(pclMvaMPFC,FIELDVAL(rgMvaArray,pclRowMVA,igMvaMPFC));
	      TrimRight(pclMvaMPFC);
	  
	      sprintf(clSqlBuf,"SELECT MPFN FROM MVFTAB WHERE MPFC = '%s'",pclMvaMPFC);
	      dbg(DEBUG,"%05d: clSqlBuf: <%s>",__LINE__,clSqlBuf);   

	      slSqlFunc= START;
	      slCursor = 0;
	      ilRC = sql_if(slSqlFunc,&slCursor,clSqlBuf,clData);
	      if (ilRC == RC_FAIL) 
		{
		  dbg(DEBUG, "%05d:SQL Buffer<%s>",__LINE__, clSqlBuf);
		  get_ora_err(-1, cgErrMsg);
		  dbg(TRACE, "Oracle Error <%s>", cgErrMsg);
		}                           
	      dbg(TRACE,"%05d: clData: <%s>,ilRC: %d",__LINE__,clData,ilRC);
	      close_my_cursor(&slCursor);
	      strcpy(pclMvfMPFN,clData);
	      TrimRight(pclMvfMPFN);

	      strcpy(pclMvaPGNO,FIELDVAL(rgMvaArray,pclRowMVA,igMvaPGNO));
	      TrimRight(pclMvaPGNO);
	      strcpy(pclMvaOBJN,FIELDVAL(rgMvaArray,pclRowMVA,igMvaOBJN));
	      TrimRight(pclMvaOBJN);
	      strcpy(pclMvaREPC,FIELDVAL(rgMvaArray,pclRowMVA,igMvaREPC));
	      TrimRight(pclMvaREPC);
	      strcpy(pclDevURNO,FIELDVAL(rgDevArray,pclRowDEV,igDevURNO));
	      TrimRight(pclDevURNO);
	      strcpy(pclDevDADR,FIELDVAL(rgDevArray,pclRowDEV,igDevDADR));
	      TrimRight(pclDevDADR);

	      strcpy(pclFields,"WHAT,URNO,DADR,DEVN,MPFN,PGNO,OBJN,REPC");
	      sprintf(pclData,"STOP,%s,%s,%s,%s,%s,%s,%s",pclDevURNO,pclDevDADR,pclMvaDEVN,pclMvfMPFN,pclMvaPGNO,pclMvaOBJN,pclMvaREPC);
	      dbg(DEBUG,"%05d: SendEvent pclFields: <%s>",__LINE__,pclFields);
	      dbg(DEBUG,"%05d: SendEvent pclData: <%s>",__LINE__,pclData);


		if(strcmp(pclMvaREPC,"-1") != 0)
		{ 
	      		ilRC = SendEvent("MPG",ilQ,PRIORITY_4,NULL,NULL,pcgTwEnd,NULL,
			       pclFields,pclData,NULL,0);
		}
	
	      if (ilRC = RC_SUCCESS)
		{
		  dbg(DEBUG,"%05d: SendEvent succeeded",__LINE__);
		}
	      else
		{
		  dbg(DEBUG,"%05d: SendEvent failed",__LINE__);
		}

	      ilRC = CEDAArrayPutField(&rgMvaArray.rrArrayHandle,rgMvaArray.crArrayName,NULL,"MVAE",llRowNumMVA,pclNowTime);

	    }
	}
      llRowNumMVA = ARR_NEXT;

    }

  dbg(DEBUG,"%05d: CEDAArrayWriteDB MVATAB",__LINE__);
  ilRC = CEDAArrayWriteDB(&rgMvaArray.rrArrayHandle,rgMvaArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
  if (ilRC != RC_SUCCESS)
    {
      /*dbg(TRACE,"%05d: CEDAArrayWriteDB MVATAB failed !!",__LINE__);*/
      rollback();
    }
  else
    { 
      commit_work();
    }

  dbg(DEBUG,"******************* this is the end of StopVideos ****************");

  return (ilRC);
}
























