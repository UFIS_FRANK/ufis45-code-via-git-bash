CREATE OR REPLACE  FUNCTION GETLOCTEXT
(v_rtyp in fldtab.rtyp%TYPE, v_rnam in fldtab.rnam%TYPE, v_num in number)
return char
is
rc_txt char(128);
CURSOR fxttab_curs (p_rtyp in fldtab.rtyp%TYPE, p_rnam in fldtab.rnam%TYPE) IS SELECT text FROM fxttab WHERE fxttab.fldu in (select urno from fldtab where rtyp = p_rtyp and rnam = p_rnam and dseq = 0);
BEGIN
	open fxttab_curs (v_rtyp,v_rnam);
	for i in 1..v_num loop
		fetch fxttab_curs into rc_txt;
		IF fxttab_curs%NOTFOUND THEN
			rc_txt := ' ';
		END IF;
		EXIT WHEN fxttab_curs%NOTFOUND;
	END LOOP;
	close fxttab_curs;
	RETURN (rtrim(rc_txt,' '));
END;
/	
