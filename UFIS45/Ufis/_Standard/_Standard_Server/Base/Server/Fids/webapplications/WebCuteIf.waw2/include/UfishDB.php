<?php

function Global_Status($Global_status) {
    $Session = $GLOBALS['Session'];
    $logger = LoggerManager::getRootLogger('CuteIf');
    $Session["Global_status"] = $Global_status;
    //    if ($Global_status=="Logo") {
    //        $Session["PredefinedSelection"] = false;
    //        $Session["logoSelection"] = true;
    //    }
    //    if ($Global_status=="Remarks") {
    //        $Session["PredefinedSelection"] = true;
    //        $Session["logoSelection"] = false;
    //
    //    }
    $logger->debug('Global_status  :' . $Session["Global_status"]);
    $GLOBALS['Session'] = $Session;
}

function updatecmd($FlightURNO, $Button, $PRemarkCode, $Text1, $Text2, $LogoSelect, $GateBoardingExValue) {
    $Session = $GLOBALS['Session'];
    $RemarkGateOpen = $GLOBALS['RemarkGateOpen'];
    $RemarkGateBoardingEx = $GLOBALS['RemarkGateBoardingEx'];
    $RemarkGateBoarding = $GLOBALS['RemarkGateBoarding'];
    $RemarkGateFinalCall = $GLOBALS['RemarkGateFinalCall'];
    $RemarkGateClose = $GLOBALS['RemarkGateClose'];

    //$Text1= $GLOBALS['Text1'];
    //$Text2= $GLOBALS['Text2'];
    //$LogoSelect = LogoPreview($LogoSelect,'');


    $logger = LoggerManager::getRootLogger('CuteIf');
    $logger->debug("updatecmd : " . $Button . "|" . $FlightURNO . "|" . $PRemarkCode . "|" . $Text1 . "|" . $Text2 . "|" . $Session["AirlineURNO"] . "|" . $Session["PicName"]);

    if (isset($FlightURNO)) {

        $Session["FlightURNO"] = $FlightURNO;
        if ($Session["Type"] == "GATE") {
            if ($Button == "Refresh Data") {
                $Button = "Refresh";
                $PRemarkCode = "";
            }
            if ($Button == "Clear Gate") {
                $Button = "Clear";
                $PRemarkCode = "Clear";
            }
            if ($Button == "Gate Open") {
                $Button = "Open";
                $PRemarkCode = $RemarkGateOpen;
            }
            if ($Button == "Boarding Expected") {
                $Button = "Transmit";
                $PRemarkCode = $RemarkGateBoardingEx;
            }
            if ($Button == "Boarding") {
                $Button = "Transmit";
                $PRemarkCode = $RemarkGateBoarding;
            }
            if ($Button == "Final Call") {
                $Button = "Transmit";
                $PRemarkCode = $RemarkGateFinalCall;
            }
            if ($Button == "Gate Closed") {
                $Button = "Close";
                $PRemarkCode = $RemarkGateClose;
            }
        }


        if (ereg("Clear", $Button) == True || ereg("Open", $Button) == True || ereg("Update", $Button) == True || ereg("Transmit", $Button) == True || ereg("LogoTransmit", $Button) == True && $PRemarkCode != $RemarkGateClose) {
            //$Session["AirlineURNO"]="0";
            $Session["Button"]=$Button;
            $UpdateStatus = UpdateUfisLH($FlightURNO, $Button, $PRemarkCode, $Text1, $Text2, $LogoSelect, $GateBoardingExValue);
            $logger->debug("Counter :" . $Session["Number"] . " updated");
            //if ($UpdateStatus==1){echo "<script language=\"JavaScript1.2\" type=\"text/javascript\">WaitAlert()</script>";}
        }


        if (ereg("Close", $Button) == True || $PRemarkCode == $RemarkGateClose) {
            // Close Counter
            $Session["Button"]=$Button;
            CloseCounter($FlightURNO);

            $logger->debug("Counter :" . $Session["Number"] . " closed");
            //if ($Session["Type"]!="GATE") {
            //	echo "<script language=\"JavaScript1.2\">parent.location.href=\"index.php\"</script>";
            //}
        }

        $GLOBALS['Session'] = $Session;

    } else {
        $logger->debug("!!!!!!!!!FlightUrno was not Set (SessionUrno)" . $Session["FlightURNO"]);
    }
}

function LogoPreview($LogoSelect, $Request) {
    //Global $Session,$logoPath,$logoExt,$FLZTABuse;

    $Session = $GLOBALS['Session'];
    $logoPath = $GLOBALS['logoPath'];
    $logoExt = $GLOBALS['logoExt'];
    $FLZTABuse = $GLOBALS['FLZTABuse'];


    $AirlineURNO = 0;
    $PicName = "pics/dot.gif";
    //Added Display LogoName
    //Different if Gate and Dedicated Checkin from Common CheckIN
    //WAW Project
    $logger = LoggerManager::getRootLogger('CuteIf');
    $logger->debug('LogoSelect :' . $LogoSelect . "-- Request:" . $Request);
    $tmplogo = "";
    //if ($FLZTABuse==true) {
    //     $logoExt =".GIF";
    // }



    if ($Request == "DisableLogo") {
        $Session["logoSelection"] = !$Session["logoSelection"];
    }

    if (strlen($LogoSelect) > 1) {
        if ($LogoSelect == "None") {
            $Session["PicName"] = "None";
        } else {
            if ($FLZTABuse == true) {
                $tok = strtok($LogoSelect, "|");
                $i = 0;

                while ($tok) {

                    if ($i == 0) {
                        $AirlineURNO = $tok;
                        $Session["AirlineURNO"] = $tok;
                    }

                    if ($i == 1) {
                        $PicName = $logoPath . $tok . $logoExt;
                        $Session["PicName"] = $tok;
                    }

                    $i++;
                    $tok = strtok("|");
                }
            } else {

                $PicName = $logoPath . $LogoSelect . $logoExt;
                // echo $logoPath ."--".$LogoSelect."--".$logoExt;
                //if ($Session["LogoNameALC"]==false) {$Session["PicName"] = $LogoSelect;}
                $Session["PicName"] = $LogoSelect;
            }
        }
    } else {

        if ($FLZTABuse == true) {
            //if (strlen($LogoSelect)==0 )  {
            //    $LogoSelect=Currentlogos($FLZTABuse);
            // }
            $tmplogo = Currentlogos($FLZTABuse);
            if (strlen(trim($tmplogo)) > 1) {
                $LogoSelect = $tmplogo;
            } else {
                //echo $Session["LogoName"];
                $LogoSelect = $Session["LogoName"];
            }

            $tok = strtok($LogoSelect, "|");
            $i = 0;

            while ($tok) {

                if ($i == 0) {
                    $AirlineURNO = $tok;
                    $Session["AirlineURNO"] = $tok;
                }

                if ($i == 1) {
                    $PicName = $logoPath . $tok . $logoExt;
                    $Session["PicName"] = $tok;
                }

                $i++;
                $tok = strtok("|");
            }
        } else {
            if ($Session["CommonF"] == 0) {

                $LogoSelect = $Session["LogoName"];
            } else {
                $tmplogo = Currentlogos($FLZTABuse);
                if (strlen(trim($tmplogo)) > 1) {
                    $LogoSelect = $tmplogo;
                } else {
                    //echo $Session["LogoName"];
                    $LogoSelect = $Session["LogoName"];
                }
            }

            if (strlen($LogoSelect) > 1) {
                $PicName = $logoPath . $LogoSelect . $logoExt;
                if ($Session["LogoNameALC"] == false) {
                    $Session["PicName"] = $LogoSelect;
                }
            }
        }
    }
    $GLOBALS['AirlineURNO'] = $AirlineURNO;
    $GLOBALS['Session'] = $Session;
    $logger->debug('PicName :' . $PicName);
    return $PicName;
}

function CurrentTime() {
    $ctime = "";

    $ctime = strftime("%B", time()) . "&nbsp;" . strftime("%d", time()) . "," . strftime("%Y", time()) . "<br>";
    $ctime = $ctime . strftime("%H", time()) . ":" . strftime("%M", time()) . " &nbsp;&nbsp;&nbsp;&nbsp;";

    return $ctime;
}

function Currentlogos($FLZTABuse) {
    //Global $db,$Session,$utcdate;

    $db = $GLOBALS['db'];
    $Session = $GLOBALS['Session'];
    $utcdate = $GLOBALS['utcdate'];
    $FLDUse = $GLOBALS['FLDUse'];

    $StrStatus = "";

    $logger = LoggerManager::getRootLogger('CuteIf');

    if ($Session["Type"] == "GATE") {
        $tmp = "GTD1";
        //20081212 GFO
        //Changed frm A to G
        $UTYP = "G";
    } else {
        $tmp = "C";
        $UTYP = "C";
    }

    if ($FLZTABuse == true) {
        if ($FLDUse == false) {
            $logger->debug('FlightURNO :' . $Session["FlightURNO"]);
            if (isset($Session["FlightURNO"]) && $Session["FlightURNO"] != 'undefined') {
                $cmdTempCommandText = "SELECT FLGU ,flgtab.LGSN as LGSN FROM FLZTAB,flgtab  WHERE flgtab.urno=flztab.flgu and RURN =" . $Session["FlightURNO"] . " AND UTYP = '" . $UTYP . "' and LNAM='" . $Session["Number"] . "'";
            } else {
                $cmdTempCommandText = "SELECT FLGU, ' ' as LGSN  FROM FLZTAB WHERE  RURN =0  AND UTYP = '" . $UTYP . "' and LNAM='" . $Session["Number"] . "'";
            }
        } else {
            $cmdTempCommandText = "SELECT FLGU ,flgtab.LGFN FROM FLZTAB,flgtab  WHERE flgtab.urno=flztab.flgu and WHERE  FLDU in (SELECT URNO FROM FLDTAB WHERE RNAM='" . $Session["Number"] . "' AND RTYP='" . $tmp . "' and DSEQ>0)";
        }
    } else {
        $cmdTempCommandText = "SELECT REMA FROM ccatab WHERE  ccatab.ckic='" . $Session["Number"] . "'  AND   ccatab.ctyp='" . $tmp . "' AND ( ccatab.CKBS BETWEEN  " . CedaSYSDATE('d', -1) . " AND " . CedaSYSDATE('n', -30) . ") AND  (ccatab.CKES >" . CedaSYSDATE('h', 0) . ") AND (CKBA=' ' or CKBA <=  " . CedaSYSDATE('h', 0) . " )AND (CKEA=' ')  ";
    }
    $logger->debug('FLDUse :' . $FLDUse);
    $logger->debug('Currentlogos :' . $cmdTempCommandText);
    //echo $cmdTempCommandText;
    $rs = $db->Execute($cmdTempCommandText);


    if (!$rs->EOF) {
        if ($FLZTABuse == true) {
            $StrStatus = trim($rs->fields("FLGU")) . "|" . trim($rs->fields("LGSN"));
            //$StrStatus=trim($rs->fields("LGSN"));
        } else {
            $StrStatus = trim($rs->fields("REMA"));
        }
        $Session["LogoNameALC"] = false;
    }// If End
    if ($rs)
        $rs->Close();
    return $StrStatus;
}

// End Function

function Status() {
    //Global $db,$Session;
    // Global $RemarkGateOpen,$RemarkGateBoarding,$RemarkGateFinalCall,$RemarkGateClose;

    $db = $GLOBALS['db'];
    $Session = $GLOBALS['Session'];
    $RemarkGateOpen = $GLOBALS['RemarkGateOpen'];
    $RemarkGateBoardingEx = $GLOBALS['RemarkGateBoardingEx'];
    $RemarkGateBoarding = $GLOBALS['RemarkGateBoarding'];
    $RemarkGateFinalCall = $GLOBALS['RemarkGateFinalCall'];
    $RemarkGateClose = $GLOBALS['RemarkGateClose'];

    $logger = LoggerManager::getRootLogger('CuteIf');

    $StrStatus = " NoEntry";
    if ($Session["Type"] == "GATE") {
        $tmp = "GTD1";
    } else {
        $tmp = "CKIF";
    }

    //Optimizing the Query 23-02-2005
    //$cmdTempCommandText = "select dseq,urno,ctyp,aurn,rurn from fldtab where rnam = '".$Session["Number"]."' and rtyp = '".$tmp."' and urno > 0 order by cdat desc";
    if ($Session["Type"] == "GATE") {
        $StrStatus = " ";
    } else {
        $StrStatus = "CLOSED";
    }
    $Session["STATUS"] = 0;


    if (isset($Session["FlightURNO"])) {
        if ($Session["Type"] == "GATE") {
            $tmp = "SELECT GD1X openA,GD1Y closeA,REMP FROM AFTTAB WHERE  URNO= " . $Session["FlightURNO"];
        } else {
            $tmp = "SELECT CKBA openA,CKEA closeA,'' as REMP FROM CCATAB WHERE CKIC='" . $Session["Number"] . "' and (FLNU='" . $Session["FlightURNO"] . "' or URNO='" . $Session["FlightURNO"] . "') ";
            $tmp = $tmp . " and ((ckba=' ') or ((ckba< " . CedaSYSDATE('n', 30) . " and ckba<>' ') ))";
        }
        $cmdTempCommandText = $tmp;


        //echo $cmdTempCommandText;
        $logger->debug($cmdTempCommandText);

        $rs = $db->Execute($cmdTempCommandText);
        if (!$rs->EOF) {


            //$Session["Fldaurn"]=trim($rs->fields("aurn"));
            //$Session["Fldrurn"]=trim($rs->fields("rurn"));
            if (strlen(trim($rs->fields("closeA"))) == 0 && strlen(trim($rs->fields("openA"))) > 1) {
                $StrStatus = "OPEN";
                //$Session["CTYP"]=$rs->fields("ctyp");
                $Session["STATUS"] = 1;
            } else {
                $StrStatus = "CLOSED";
                $Session["STATUS"] = 0;
            }

            if ($Session["Type"] == "GATE") {
                $StrStatus = trim($rs->fields("REMP"));
                if (trim($rs->fields("REMP")) == $RemarkGateOpen) {
                    $StrStatus = "Gate Open";
                }
                if (trim($rs->fields("REMP")) == $RemarkGateBoardingEx) {
                    $StrStatus = "Boarding Expected";
                }
                if (trim($rs->fields("REMP")) == $RemarkGateBoarding) {
                    $StrStatus = "Boarding";
                }
                if (trim($rs->fields("REMP")) == $RemarkGateFinalCall) {
                    $StrStatus = "Final Call";
                }
                if (trim($rs->fields("REMP")) == $RemarkGateClose) {
                    $StrStatus = "Gate Closed";
                }

                $logger->debug($StrStatus);
            }
        }
        if ($rs)
            $rs->Close();
    }

    $GLOBALS['Session'] = $Session;
    return $StrStatus;
}

// End Function

function CurrentPRemarks() {
    //Global $db,$Session ;
    $db = $GLOBALS['db'];
    $Session = $GLOBALS['Session'];

    $StrStatus = "";

    if (isset($Session["FlightURNO"])) {


        if ($Session["Type"] == "GATE") {
            $tmp = "SELECT REMP as Remark FROM AFTTAB WHERE  URNO= " . $Session["FlightURNO"];
        } else {
            $tmp = "SELECT DISP as Remark FROM CCATAB WHERE CKIC='" . $Session["Number"] . "' and (FLNU='" . $Session["FlightURNO"] . "' or URNO='" . $Session["FlightURNO"] . "') ";
            $tmp = $tmp . " and ((ckba=' ') or ((ckba< " . CedaSYSDATE('n', 30) . " and ckba<>' ') ))";
        }

        $cmdTempCommandText = $tmp;
        $rs = $db->Execute($cmdTempCommandText);
        //echo $tmp;
        if (!$rs->EOF) {
            $StrStatus = trim($rs->fields("Remark"));
        }// If End

        if ($rs)
            $rs->Close();
    }
    return $StrStatus;
}

// End Function

function PRemarks() {
    //Global $db,$Session,$hopo,$CPRemark,$PRemarksRemTYPGate,$PRemarksRemTYPCheckin,$UseUTFOracleFunction;

    $db = $GLOBALS['db'];
    $Session = $GLOBALS['Session'];
    $hopo = $GLOBALS['hopo'];
    $CPRemark = $GLOBALS['CPRemark'];
    $PRemarksRemTYPGate = $GLOBALS['PRemarksRemTYPGate'];
    $PRemarksRemTYPCheckin = $GLOBALS['PRemarksRemTYPCheckin'];
    $UseUTFOracleFunction = $GLOBALS['UseUTFOracleFunction'];
    $FLDUse = $GLOBALS['FLDUse'];
    $logger = LoggerManager::getRootLogger('CuteIf');

    $rValues = Array();
    $rOutput = Array();
    $rSelected = Array();
    //rType
    $rType = Array();

    //Changed REMT type
    //WAW Project
    if ($Session["Type"] == "GATE") {
        $tmp = $PRemarksRemTYPGate;
    } else {
        $tmp = $PRemarksRemTYPCheckin;
    }

    //Subject to change the RTYP for the GATE / CHECKIN
    // Change the  beme to receive ascii from unicode in the db
    //Function must be created in the DB

    if ($UseUTFOracleFunction == true) {
        $cmdTempCommandText = "Select distinct  decode(unicode_str(trim(beme)),'', beme,unicode_str(trim(beme))) beme,code,remi,remt,urno from fidtab where remt in (" . $tmp . ") and beme<>' ' and hopo='$hopo' order by beme";
    } else {
        $cmdTempCommandText = "Select distinct  decode(trim(beme),'', beme,trim(beme)) beme,code,remi,remt,urno from fidtab where remt in (" . $tmp . ") and beme<>' ' and hopo='$hopo' order by beme";
    }
    $logger->debug($cmdTempCommandText);

    //    echo $cmdTempCommandText;
    //    exit;
    $rs = $db->Execute($cmdTempCommandText);

    // Add None 20081126 GFO for WAW Project
    $rValues[] = "None";
    $rOutput[] = "None";
    $rSelected[0] = "None";

    if (!$rs->EOF) {
        while (!$rs->EOF) {
            $tmp = "";
            $CPRemark = CurrentPRemarks();
            if ($CPRemark == trim($rs->fields("code"))) {
                $tmp = " selected";
                $rSelected[0] = trim($rs->fields("code"));
                $rType[0] = trim($rs->fields("remt"));
            }
            $rValues[] = trim($rs->fields("code"));
            // Add code to Description  20081126 GFO for WAW Project
            $rOutput[] = trim($rs->fields("beme")) . " " . trim($rs->fields("code"));
            //echo '<option value="'.trim($rs->fields("code")).'" '.$tmp.'>'.trim($rs->fields("beme"))."\n";
            $rs->MoveNext();
        }
    }// If End
    if ($rs)
        $rs->Close();
    return Array($rValues, $rOutput, $rSelected, $rType);
}

// End Function

function FreeRemarks($line) {
    //Global $db,$Session,$FLDRTYPGate,$FLDRTYPCheckin;

    $db = $GLOBALS['db'];
    $Session = $GLOBALS['Session'];
    $FLDRTYPGate = $GLOBALS['FLDRTYPGate'];
    $FLDRTYPCheckin = $GLOBALS['FLDRTYPCheckin'];
    $FLDUse = $GLOBALS['FLDUse'];
    $logger = LoggerManager::getRootLogger('CuteIf');
    $StrString = "";
    if ($Session["Type"] == "GATE") {
        $tmp = $FLDRTYPGate;
    } else {
        $tmp = $FLDRTYPCheckin;
    }
    if ($Session["Type"] == "GATE") {
        //20081212 GFO
        //Changed frm A to G
        $UTYP = "G";
    } else {
        $UTYP = "C";
    }
    //if ($FLDUse==false) {
    //    $cmdTempCommandText = "SELECT TEXT,RURN AS URNO FROM FXTTAB WHERE  SORT='".$line."' AND RURN =".$Session["FlightURNO"] ." AND UTYP = '".$UTYP."' and LNAM='".$Session["Number"]."'";
    //}
    //else  {
    $cmdTempCommandText = "SELECT TEXT,FLDU AS URNO FROM FXTTAB WHERE  SORT='" . $line . "' AND FLDU IN (SELECT URNO FROM FLDTAB WHERE RTYP = '" . $tmp . "' AND RNAM = '" . $Session["Number"] . "' AND DSEQ > 0)";
    // }
    $logger->debug('FreeText ' . $cmdTempCommandText);
    $rs = $db->Execute($cmdTempCommandText);

    if (!$rs->EOF) {
        $StrString = trim($rs->fields("TEXT"));
    }// If End

    if ($rs)
        $rs->Close();
    return $StrString;
}

// End Function

function CheckBlock() {
    //    Global $db,$Session;
    $db = $GLOBALS['db'];
    $Session = $GLOBALS['Session'];
    $logger = & LoggerManager::getLogger('CuteIf');
    // GFO : 20080724 Correcting the Blocking Check

    $StrString = 0;
    if ($Session["Type"] == "GATE") {
        //$tmp="select count(*) as count from BLKTAB,GATTAB where  (((BLKTAB.NAFR< " . CedaSYSDATE ('h',  0)." and BLKTAB.nafr<>' ') AND (BLKTAB.NATO>= " . CedaSYSDATE ('h',  0)." or BLKTAB.nato=' ')) and  (BLKTAB.BURN=GATTAB.URNO) AND GATTAB.GNAM='".$Session["Number"]."')";
        $tmp = "SELECT COUNT (*) AS COUNT";
        $tmp = $tmp . " FROM blktab, gattab";
        $tmp = $tmp . " WHERE (    (    (    blktab.nafr < " . CedaSYSDATE('h', 0) . "";
        $tmp = $tmp . " AND blktab.nafr <> ' '";
        $tmp = $tmp . " )";
        $tmp = $tmp . " AND (   blktab.nato >= " . CedaSYSDATE('h', 0) . "";
        $tmp = $tmp . " OR blktab.nato = ' '";
        $tmp = $tmp . " )";
        $tmp = $tmp . " )";
        $tmp = $tmp . " AND (blktab.days like '%" . WeekDay('h', 0) . "%')";
        $tmp = $tmp . " ";
        $tmp = $tmp . "		AND (blktab.tifr <" . CedaSYSTIME('h', 0) . " and blktab.tito > " . CedaSYSTIME('h', 0) . ")";
        $tmp = $tmp . " AND (blktab.burn = gattab.urno)";
        $tmp = $tmp . " AND gattab.gnam = '" . $Session["Number"] . "'";
        $tmp = $tmp . " )";
    } else {
        // $tmp="select count(*) as count from BLKTAB,CICTAB where (((BLKTAB.NAFR< " . CedaSYSDATE ('h',  0)." and BLKTAB.nafr<>' ') AND (BLKTAB.NATO>= " . CedaSYSDATE ('h',  0)." or BLKTAB.nato=' ')) and (BLKTAB.BURN=CICTAB.URNO) AND CICTAB.CNAM='" .$Session["Number"]. "')";
        $tmp = "SELECT COUNT (*) AS COUNT";
        $tmp = $tmp . " FROM blktab, CICTAB";
        $tmp = $tmp . " WHERE (    (    (    blktab.nafr < " . CedaSYSDATE('h', 0) . "";
        $tmp = $tmp . " AND blktab.nafr <> ' '";
        $tmp = $tmp . " )";
        $tmp = $tmp . " AND (   blktab.nato >= " . CedaSYSDATE('h', 0) . "";
        $tmp = $tmp . " OR blktab.nato = ' '";
        $tmp = $tmp . " )";
        $tmp = $tmp . " )";
        $tmp = $tmp . " AND (blktab.days like '%" . WeekDay('h', 0) . "%')";
        $tmp = $tmp . " ";
        $tmp = $tmp . "		AND (blktab.tifr <" . CedaSYSTIME('h', 0) . " and blktab.tito > " . CedaSYSTIME('h', 0) . ")";
        $tmp = $tmp . " AND (blktab.burn = CICTAB.URNO)";
        $tmp = $tmp . " AND  CICTAB.CNAM = '" . $Session["Number"] . "'";
        $tmp = $tmp . " )";
    }
    //echo $tmp;
    $cmdTempCommandText = $tmp;
    $rs = $db->Execute($cmdTempCommandText);
    $logger->debug('CheckBlock cmdTemp:' . $cmdTempCommandText);
    if (!$rs->EOF) {
        if ($rs->fields("count") > 1) {
            $StrString = 1;
        }
    }// If End
    //echo $tmp;
    if ($rs)
        $rs->Close();
    return $StrString;
}

//Added alc2 for Gates and Dedicted Checkin IN
//WAW Project
function CheckAlloc($ShowResults=0) {
    //Global $db,$Session,$FlightTimeDiffGates;
    //Global $GateFrom,$GateTo,$CheckinFrom,$CheckinTo,$CommonCheckinFrom,$CommonCheckinTo;
    //Global $RemarkGateOpen,$RemarkGateClose;

    $db = $GLOBALS['db'];
    $Session = $GLOBALS['Session'];
    $FlightTimeDiffGates = $GLOBALS['FlightTimeDiffGates'];
    $GateTo = $GLOBALS['GateTo'];
    $GateFrom = $GLOBALS['GateFrom'];
    $CheckinFrom = $GLOBALS['CheckinFrom'];
    $CheckinTo = $GLOBALS['CheckinTo'];
    $CommonCheckinFrom = $GLOBALS['$CommonCheckinFrom'];
    $CommonCheckinTo = $GLOBALS['CommonCheckinTo'];
    $RemarkGateOpen = $GLOBALS['RemarkGateOpen'];
    $RemarkGateClose = $GLOBALS['RemarkGateClose'];

    $enableBoardingTime = $GLOBALS['enableBoardingTime'];

    $BOAE = "";
    if ($enableBoardingTime) {
         $BOAE = ",afttab.BOAE";
    }

    $logger = & LoggerManager::getLogger('CuteIf');

    //Prf 9764 Added the BOAE Field to hold the Expected time
    //this is a common fields for all gate allocations

    //To Enable BOAE  add ,BOAE to the next line
    //and in the UNION
    $StrString = 0;
    if ($Session["Type"] == "GATE") {
        $tmp = "SELECT jfno,flno,alc3,alc2,urno,'1' as cmd,tifd,GD1X openA,GD1Y closeA,REMP,stod,etod $BOAE  FROM afttab WHERE ";
        $tmp = $tmp . " ((gtd1 = '" . $Session["Number"] . "')  ";
        //              $tmp = $tmp ." AND (DECODE(GD1Y,'              ', GD1E, GD1Y) > " . CedaSYSDATE ('n',  0).")   ";
        //                $tmp = $tmp ." AND GD1Y < " . CedaSYSDATE ('n',  10)."   ";
        $tmp = $tmp . " )";
        //              $tmp = $tmp ." AND ((DECODE(GD1X,'              ', GD1B, GD1X) BETWEEN  " . CedaSYSDATE ('n',  -350)." AND " . CedaSYSDATE ('n',  30).") ";
        //      $tmp = $tmp ." AND (GD1B  BETWEEN  " . CedaSYSDATE ('n',  -60)." AND " . CedaSYSDATE ('n',  120)." ";
        //$tmp = $tmp ." )";
        $tmp = $tmp . " AND OFBL = ' '";
        $tmp = $tmp . " AND FTYP='O' AND (TIFD BETWEEN " . CedaSYSDATE('n', $GateFrom) . " AND " . CedaSYSDATE('n', $GateTo) . ") ";
        $tmp = $tmp . " UNION ALL SELECT jfno,flno,alc3,alc2,urno,'2' as cmd,tifd,GD2X openA,GD2Y closeA,REMP,stod,etod $BOAE   FROM afttab WHERE";
        $tmp = $tmp . " ((gtd2 = '" . $Session["Number"] . "')  ";
        //              $tmp = $tmp ." AND (DECODE(GD2Y,'              ', GD2E, GD2Y) > " . CedaSYSDATE ('n',  -15).")   ";
        //                $tmp = $tmp ." AND GD2Y < " . CedaSYSDATE ('n',  10)."   ";
        //$tmp = $tmp ." )";
        //              $tmp = $tmp ." AND ((DECODE(GD2X,'              ', GD2B, GD2X) BETWEEN  " . CedaSYSDATE ('n',  -350)." AND " . CedaSYSDATE ('n',  30).") ";
        //      $tmp = $tmp ." AND (GD2B  BETWEEN  " . CedaSYSDATE ('n',  -60)." AND " . CedaSYSDATE ('n',  120)." ";
        $tmp = $tmp . " )";
        $tmp = $tmp . " AND OFBL = ' '";
        $tmp = $tmp . " AND FTYP='O' AND (TIFD BETWEEN " . CedaSYSDATE('n', $GateFrom) . " AND " . CedaSYSDATE('n', $GateTo) . ") ORDER by TIFD";
        $tmp1 = "";
        //		echo $tmp;
        //		exit;
    } else {
        //		$tmp="SELECT FLNO, JFNO, ALC3, URNO, 'D' as cmd1 from AFTTAB WHERE (TIFD BETWEEN TO_CHAR(SYSDATE -30/1440,'YYYYMMDDHH24MISS') AND TO_CHAR(SYSDATE+720/1440,'YYYYMMDDHH24MISS')) AND FTYP='O'  AND (URNO IN  (SELECT FLNU from CCATAB  WHERE  (((CKIC = '".$Session["Number"]."')  AND ((DECODE(CKBA,'              ', CKBS, CKBA) < TO_CHAR(SYSDATE+30/1440,'YYYYMMDDHH24MISS') AND CKBS<>' ')) AND ((CKEA > TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS')) OR ((CKES LIKE ' %') AND (CKEA LIKE ' %'))  OR (CKEA LIKE ' %'))))))  ORDER by tifd";
        //Optimizing the Query 23-02-2005
        $tmp = " SELECT   afttab.flno, afttab.jfno, afttab.alc3, afttab.alc2,ccatab.urno, 'D' AS cmd1,";
        $tmp = $tmp . " CKBA openA,CKEA closeA,disp as REMP,afttab.stod,afttab.etod ";
        $tmp = $tmp . "    FROM afttab,ccatab ";
        $tmp = $tmp . "   WHERE ";
        $tmp = $tmp . "     afttab.urno=ccatab.flnu";
        $tmp = $tmp . "   	 AND (afttab.tifd BETWEEN " . CedaSYSDATE('n', $CheckinFrom) . " AND " . CedaSYSDATE('n', $CheckinTo) . ")";
        //$tmp = $tmp . " AND ( " . CedaSYSDATE ('h',  0)." BETWEEN ccatab.ckbs AND ccatab.ckes )";
        $tmp = $tmp . "     AND afttab.ftyp = 'O'";
        $tmp = $tmp . "     AND  (    ";
        $tmp = $tmp . "	 	  	   (ccatab.ckic = '" . $Session["Number"] . "')";
        //$tmp=$tmp."           AND (";
        //$tmp=$tmp."		         ( DECODE (ccatab.ckba, '              ', ccatab.ckbs, ccatab.ckba) < " . CedaSYSDATE ('n',  30)."";
        //$tmp=$tmp."                   AND ccatab.ckbs <> ' '";
        //$tmp=$tmp."                 )";
        //$tmp=$tmp."               )";
        //$tmp=$tmp."           AND (   ";
        //$tmp=$tmp."		   	   	   (ccatab.ckea > " . CedaSYSDATE ('h',  0)." )";
        //$tmp=$tmp."				   ";
        //$tmp=$tmp."                   OR ((ccatab.ckes LIKE ' %') AND (ccatab.ckea LIKE ' %'))";
        //$tmp=$tmp."                   OR (ccatab.ckea LIKE ' %')";
        //$tmp=$tmp."                )";
        $tmp = $tmp . "                 ";
        $tmp = $tmp . "               ";
        $tmp = $tmp . "            )";
        $tmp = $tmp . "  UNION ALL ";
        $tmp = $tmp . " SELECT 'Common' AS flno , ' ' AS jfno,alttab.alc3 , alttab.alc2,";
        $tmp = $tmp . "ccatab.urno AS urno,'C' AS cmd1,ckba opena, ckea closea,  ";
        $tmp = $tmp . "disp AS remp,'' as stod, '' as etod ";
        $tmp = $tmp . " FROM alttab, ccatab ";
        $tmp = $tmp . " WHERE ccatab.flnu = alttab.urno ";
        $tmp = $tmp . " AND ccatab.ckic = '" . $Session["Number"] . "' ";
        $tmp = $tmp . " AND ccatab.ctyp = 'C' ";
        $tmp = $tmp . " AND (ccatab.ckbs BETWEEN " . CedaSYSDATE('d', $CommonCheckinFrom) . " AND " . CedaSYSDATE('n', $CommonCheckinTo) . ")";
        $tmp = $tmp . " AND (ccatab.ckes > " . CedaSYSDATE('h', 0) . ") ";
        // $tmp = $tmp . " AND ( " . CedaSYSDATE ('h',  0)." BETWEEN ccatab.ckbs AND ccatab.ckes )";
//235
        //$tmp=$tmp." ORDER BY afttab.tifd ";
        //Optimizing the Query 23-02-2005
        //		$tmp1="SELECT ccatab.urno as urno, ' ' as jfno, 'Common' as flno, alttab.alc2, 'C' as cmd1 FROM alttab,ccatab WHERE  ccatab.flnu=alttab.urno(+)  AND   ccatab.ckic='".$Session["Number"]."'  AND   ccatab.ctyp='C' AND ( ccatab.CKBS BETWEEN  TO_CHAR(SYSDATE-1,'YYYYMMDDHH24MISS') AND TO_CHAR(SYSDATE+30/1440,'YYYYMMDDHH24MISS')) AND  (ccatab.CKES > TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS')) AND (CKBA=' ' or CKBA <=  TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS') )AND (CKEA=' ') ";
        //It will not return any data if the the flnu field in the ccatab is empty for the Common Checkin
        //Added ALC2, ALC3, REMA
        //WAW Project
        //		$tmp1="SELECT ccatab.urno as urno, ' ' as jfno, 'Common' as flno, alttab.alc2,alttab.alc3,ccatab.rema, 'C' as cmd1,CKBA openA,CKEA closeA,REMA as REMP FROM alttab,ccatab WHERE  ccatab.flnu=alttab.urno  AND   ccatab.ckic='".$Session["Number"]."'  AND   ccatab.ctyp='C' AND ( ccatab.CKBS BETWEEN  " . CedaSYSDATE ('d',  -1)." AND " . CedaSYSDATE ('n',  30).") AND  (ccatab.CKES > " . CedaSYSDATE ('h',  0).") AND (CKBA=' ' or CKBA <=  " . CedaSYSDATE ('h',  0)." )AND (CKEA=' ') ";
        /*
          $tmp1 = "SELECT ccatab.urno AS urno, ' ' AS jfno, 'Common' AS flno, alttab.alc2,";
          $tmp1 = $tmp1 . "alttab.alc3, ccatab.rema, 'C' AS cmd1, ckba opena, ckea closea,";
          $tmp1 = $tmp1 . " rema AS remp ";
          $tmp1 = $tmp1 . " FROM alttab, ccatab ";
          $tmp1 = $tmp1 . " WHERE ccatab.flnu = alttab.urno ";
          $tmp1 = $tmp1 . "   AND ccatab.ckic = '".$Session["Number"]."' ";
          $tmp1 = $tmp1 . "   AND ccatab.ctyp = 'C' ";
          $tmp1 = $tmp1 . "   AND (ccatab.ckbs BETWEEN " . CedaSYSDATE ('d',  $CommonCheckinFrom)." AND " . CedaSYSDATE ('n',  $CommonCheckinTo).")";
          $tmp1 = $tmp1 . "   AND (ccatab.ckes > " . CedaSYSDATE ('h',  0).") ";
         */
        //$tmp1 = $tmp1 . "   AND (ckba = ' ' OR ckba <= " . CedaSYSDATE ('h',  0).") ";
        //$tmp1 = $tmp1 . "   AND (ckea = ' ') ";
    }

    $cmdTempCommandText = $tmp;
    //echo $tmp;
    //exit;

    $logger->debug('CheckAlloc cmdTemp:' . $tmp);
    //$logger->debug('CheckAlloc cmdTemp1:'.$tmp1);

    $Session["FldRows"] = 0;
    $Session["OldCounterOpen"] = True;
    $rs = $db->Execute($cmdTempCommandText);
    $tdi = FindTDI();
    if (!$rs->EOF) {

        while (!$rs->EOF) {

            $closeA = trim($rs->fields("closeA"));
            $ctime = CedaSYSDATE('n', 0);
            $ctime = str_replace("'", "", $ctime);

            $biggFive = false;
            if ($Session["Type"] == "GATE" && len($closeA) > 0) {
                $closeA = ChopString("n", 0, $closeA);
                $nowtime = ChopString("n", 0, $ctime);
                if (DateDiff("n", $closeA, $nowtime) >= $FlightTimeDiffGates) {
                    $biggFive = true;
                }
            }
            $Session["GateNumber"] = trim($rs->fields("cmd"));

            if ($biggFive == false) {
                $tmp = "";
                $StrString = 1;

                $Session["CommonF"] == 0;
                if ($Session["Type"] != "GATE") {
                    if ($Session["GateNumber"] == 'D') {
                        $Session["CommonF"] = 0; //Dedicated
                    } else {
                        $Session["CommonF"] = 1; //Common
                    }
                }

                //Added $Session["LogoName"] for Gates and Dedicted Checkin IN
                //WAW Project
                //echo $rs->fields("REMP");

                $Session["LogoNameALC"] = false;
                if (strlen(trim($rs->fields("openA")) > 1)
                        || trim($rs->fields("REMP")) == $RemarkGateOpen || trim($rs->fields("REMP")) == $RemarkGateClose) {

                    if ($Session["Type"] == "GATE") {
                        if (strlen(trim($rs->fields("alc2"))) > 1) {
                            $Session["LogoName"] = trim($rs->fields("alc2"));
                        } else {
                            $Session["LogoName"] = trim($rs->fields("alc3"));
                        }
                    } else {

                        if (strlen(trim($rs->fields("REMP"))) > 1) {
                            $Session["LogoName"] = trim($rs->fields("REMP"));
                        } else {
                            if (strlen(trim($rs->fields("alc2"))) > 1) {
                                $Session["LogoName"] = trim($rs->fields("alc2"));
                            } else {
                                $Session["LogoName"] = trim($rs->fields("alc3"));
                            }
                            $Session["LogoNameALC"] = true;
                        }
                    }
                    $Session["FlightURNO"] = trim($rs->fields("urno"));

                    $tmp = " selected";
                }

                $optionstr = "";
                $optionstr_sep = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                if ($ShowResults == 1) {



                    //				$tmp="";
                    //echo $Session["Fldaurn"]."--".trim($rs->fields("urno"));
                    //if ($Session["STATUS"]==0 && $Session["FldRows"]==0) { $tmp=" selected";}
                    //				if ($Session["STATUS"]==0 ) { $tmp=" selected";}
                    $Session["OldCounterOpen"] = False;
                    //				$tmp=" selected";
                    /*
                      if ($Session["Fldaurn"]== trim($rs->fields("urno")) ) {

                      $Session["OldCounterOpen"]=False;
                      } */

                    //Changed by GFO 20080219
                    //PRF 8841
                    $etod = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    $stod = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    if ($Session["CommonF"] == 0 || $Session["Type"] == "GATE") {

                        $stod = date("H:i", ChopString("n", $tdi, $rs->Fields("stod")));

                        if (len(trim($rs->Fields("etod"))) > 0) {
                            $etod = date("H:i", ChopString("n", $tdi, $rs->Fields("etod")));
                        }
                    }

                    $optionstr = $optionstr . trim($rs->fields("flno")) . $optionstr_sep;
                    $optionstr = $optionstr . trim($rs->fields("alc2")) . $optionstr_sep;
                    if (len(trim($rs->fields("remp"))) > 0) {
                        $optionstr = $optionstr . trim($rs->fields("remp")) . $optionstr_sep;
                    } else {
                        if ($Session["Type"] == "GATE") {
                            $optionstr = $optionstr . $optionstr_sep . "&nbsp;&nbsp;&nbsp;";
                        }
                    }

                    $optionstr = $optionstr . $stod . $optionstr_sep;
                    $optionstr = $optionstr . $etod . $optionstr_sep;

                    //$optionstr = $optionstr .$rs->Fields("stod") .$optionstr_sep;
                    //$optionstr = $optionstr .$rs->Fields("etod") .$optionstr_sep;




                    echo "<option value=" . trim($rs->fields("urno")) . " " . $tmp . ">" . $optionstr . "\n";
                    $logger->debug("lenjfno:" . $rs->fields("jfno"));
                    //Code Share Flights
                    $lenJFNO = (int) (strlen(trim($rs->fields("jfno"))) / 9);
                    $logger->debug("lenjfno" . $lenJFNO);
                    $Sstart = 0;
                    $Send = 9;
                    //if ($lenJFNO>0) {$Session["CTYP"]="M".$lenJFNO+1;}
                    for ($i = 0; $i <= $lenJFNO; $i++) {
                        $st1 = substr(trim($rs->fields("jfno")), $Sstart, $Send);
                        if (len($st1) > 0) {
                            $optionstr = "";
                            $optionstr = $optionstr . trim($st1) . $optionstr_sep;
                            $optionstr = $optionstr . $optionstr_sep . "&nbsp;&nbsp;";
                            if (len(trim($rs->fields("remp"))) > 0) {
                                $optionstr = $optionstr . trim($rs->fields("remp")) . $optionstr_sep;
                            } else {
                                if ($Session["Type"] == "GATE") {
                                    $optionstr = $optionstr . $optionstr_sep . "&nbsp;&nbsp;&nbsp;";
                                }
                            }
                            //$optionstr = $optionstr .trim($rs->fields("remp")) .$optionstr_sep;
                            $optionstr = $optionstr . "" . $stod . $optionstr_sep;
                            $optionstr = $optionstr . $etod . $optionstr_sep;

                            echo "<option value=" . trim($rs->fields("urno")) . ">" . $optionstr . "\n";
                            $logger->debug("Start" . $Sstart . "--End" . $Send . "--St1" . $st1, 'st1');
                        }
                        $Sstart = $Sstart + 9;
                    }
                    $Session["FldRows"] = $Session["FldRows"] + 1;
                }
                //else {
                //while (!$rs->EOF) {
                //	$Session["AlocUrno"]=trim($rs->fields("urno"));
                //$rs->MoveNext();
                //}
                //}
            } //End BigFive

            $rs->MoveNext();
        }
    } else {
        /*
          // If it is a CheckIn then Perhaps it is a COMMON CheckIn
          if (len($tmp1)>0) {

          $cmdTempCommandText = $tmp1;
          $rs1=$db->Execute($cmdTempCommandText);
          if (!$rs1->EOF)  {
          while (!$rs1->EOF) {
          $tmp="";
          $StrString=1;
          $Session["CommonF"]=1;
          $Session["LogoNameALC"]=false;

          if ((strlen(trim($rs1->fields("closeA")))==0 && strlen(trim($rs1->fields("openA")))>1)  || $rs1->fields("REMP")==$RemarkGateOpen) {
          if (strlen(trim($rs1->fields("REMP")))>1) {
          $Session["LogoName"]=trim($rs1->fields("REMP"));
          } else {
          if (strlen(trim($rs1->fields("alc2")))>1) {
          $Session["LogoName"]=trim($rs1->fields("alc2"));
          } else {
          $Session["LogoName"]=trim($rs1->fields("alc3"));
          }
          $Session["LogoNameALC"]=true;
          }

          $Session["FlightURNO"] = trim($rs1->fields("urno"));
          $tmp=" selected";
          }

          //if (strlen(trim($rs->fields("rema")))>1) {
          //	$Session["LogoName"]=trim($rs->fields("rema"));
          //}

          if ($ShowResults==1) {
          //						$tmp="";
          //if ($Session["STATUS"]==0 && $Session["FldRows"]==0) { $tmp=" selected";}
          if ($Session["STATUS"]==0 ) { $tmp=" selected";}
          $Session["OldCounterOpen"]=False;
          //						$tmp=" selected";

          echo "<option value=".trim($rs1->fields("urno"))." ".$tmp.">".trim($rs1->fields("flno"))."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".trim($rs1->fields("alc2"))."\n";

          //Code Share Flights
          $lenJFNO=(int)(strlen(trim($rs->fields("jfno")))/9);
          $logger->debug("lenjfno".$lenJFNO);
          $Sstart=0;
          $Send=8;
          //if ($lenJFNO>0) {$Session["CTYP"]="M".$lenJFNO+1;}
          for($i=0; $i<=$lenJFNO; $i++) {
          $st1=substr( trim($rs1->fields("jfno")), $Sstart, $Send);
          if (len($st1)>0) {
          echo "<option value=".trim($rs1->fields("urno")).">".trim($st1)."\n";
          }
          $Sstart = $Sstart+9;
          }
          $Session["FldRows"]=$Session["FldRows"]+1;
          }

          //else {
          //while (!$rs->EOF) {
          //	$Session["AlocUrno"]=trim($rs->fields("urno"));
          //$rs->MoveNext();
          //}
          //}
          $rs1->MoveNext();
          }
          if ($rs1) $rs1->Close();

          }// If End

          }// If End
         */
    }// If End
    if ($rs)
        $rs->Close();
    //	exit;
    $GLOBALS['Session'] = $Session;
    return $StrString;
}

//Changed for the Ajax
function CheckAlloc2($ShowResults=0) {
    //Global $db,$Session,$FlightTimeDiffGates;
    //Global $GateFrom,$GateTo,$CheckinFrom,$CheckinTo,$CommonCheckinFrom,$CommonCheckinTo;
    //Global $RemarkGateOpen,$RemarkGateClose;
    //Global $CheckinAllocTIF,$GateAllocTIF;



    $db = $GLOBALS['db'];
    $Session = $GLOBALS['Session'];
    $FlightTimeDiffGates = $GLOBALS['FlightTimeDiffGates'];
    $GateTo = $GLOBALS['GateTo'];
    $GateFrom = $GLOBALS['GateFrom'];
    $CheckinFrom = $GLOBALS['CheckinFrom'];
    $CheckinTo = $GLOBALS['CheckinTo'];
    $CommonCheckinFrom = $GLOBALS['CommonCheckinFrom'];
    $CommonCheckinTo = $GLOBALS['CommonCheckinTo'];
    $Ftyp = $GLOBALS['Ftyp'];
    $RemarkGateOpen = $GLOBALS['RemarkGateOpen'];
    $RemarkGateBoardingEx = $GLOBALS['RemarkGateBoardingEx'];
    $RemarkGateBoarding = $GLOBALS['RemarkGateBoarding'];
    $RemarkGateFinalCall = $GLOBALS['RemarkGateFinalCall'];
    $RemarkGateClose = $GLOBALS['RemarkGateClose'];
    $CheckinAllocTIF = $GLOBALS['CheckinAllocTIF'];
    $GateAllocTIF = $GLOBALS['GateAllocTIF'];
    $GateAllocTIFExtraTime = $GLOBALS['GateAllocTIFExtraTime'];
    $FLDRTYPGate = $GLOBALS['FLDRTYPGate'];
    $OldOpenFlights = $GLOBALS['OldOpenFlights'];

    $AjaxEnabled = $GLOBALS['AjaxEnabled'];
    $openCounterWithinScheduleTimes = $GLOBALS['openCounterWithinScheduleTimes'];

    $gateHint =  $GLOBALS['gateHint']; //"/*+ index(afttab AFTTAB_TIFD_NEW) */";
    $enableGroupCheckin = $GLOBALS['enableGroupCheckin'];
    $enableBoardingTime = $GLOBALS['enableBoardingTime'];

    $BOAE = "";
    if ($enableBoardingTime) {
         $BOAE = ",afttab.BOAE";
    }

    $ReturnArray = Array();
    $logger = LoggerManager::getRootLogger();

    $StrString = 0;

    //And ,afttab.BOAE to the query to enable BOAE
    //and in the UNION also

    if ($Session["Type"] == "GATE") {
        $tmp = "SELECT ". $gateHint ."  afttab.jfno,afttab.flno,afttab.alc3,afttab.alc2,afttab.urno,'1' as cmd,afttab.tifd,afttab.GD1X openA,afttab.GD1Y closeA,afttab.REMP,afttab.stod,afttab.etod $BOAE  FROM afttab,fldtab  WHERE ";

        $tmp = $tmp . " afttab.urno = fldtab.aurn  AND  fldtab.rtyp = '" . $FLDRTYPGate . "' AND ";
        /*
          $tmp = $tmp ." (gtd1 = '".$Session["Number"]."')  ";
          $tmp = $tmp ." ";

          if ($GateAllocTIF) {
          $tmp = $tmp ." AND (TIFD BETWEEN " . CedaSYSDATE ('n',  $GateFrom)." AND " . CedaSYSDATE ('n',  $GateTo).") ";
          } else {
          $tmp = $tmp ." AND (DECODE(GD1Y,'              ', GD1E, GD1Y) > " . CedaSYSDATE ('n',  $GateFrom).")   ";
          $tmp = $tmp ." AND GD1Y < " . CedaSYSDATE ('n',  $GateTo)."   ";
          $tmp = $tmp ." AND (DECODE(GD1X,'              ', GD1B, GD1X) BETWEEN  " . CedaSYSDATE ('n',  $GateFrom)." AND " . CedaSYSDATE ('n',  $GateTo).") ";
          $tmp = $tmp ." AND GD1B  BETWEEN  " . CedaSYSDATE ('n',  $GateFrom)." AND " . CedaSYSDATE ('n',  $GateTo)." ";

          }
          $tmp = $tmp ." AND OFBL = ' ' AND FTYP='O'  ";
         * */


        $tmp = $tmp . " (    (gtd1 = '" . $Session["Number"] . "') ";
        /*
          if (!$GateAllocTIF) {
          $tmp = $tmp ."   AND (   (DECODE (gd1y, '              ', gd1e, gd1y) > ". CedaSYSDATE ('n',  $GateFrom);
          $tmp = $tmp ."         ) ";
          $tmp = $tmp ." OR (gd1y LIKE '  %') ";
          $tmp = $tmp .")";
          $tmp = $tmp ." AND (    (DECODE (gd1x, '              ', gd1b, gd1x)";
          $tmp = $tmp ." BETWEEN ".CedaSYSDATE ('n',  $GateFrom);
          $tmp = $tmp ." AND ".CedaSYSDATE ('n',  $GateTo );
          $tmp = $tmp .")";
          $tmp = $tmp ." AND (gd1b <> ' ')";
          $tmp = $tmp .")";

          }
         */
        $tmp = $tmp . "AND OFBL = ' ' AND ftyp  in (" . $Ftyp . ")";
        $tmp = $tmp . " AND (tifd BETWEEN " . CedaSYSDATE('n', $GateFrom);
        $tmp = $tmp . " AND " . CedaSYSDATE('n', $GateTo + $GateAllocTIFExtraTime);
        $tmp = $tmp . ")";
        $tmp = $tmp . ")";


        //$tmp = $tmp ." )";

        $tmp = $tmp . " UNION ALL SELECT ". $gateHint ."  afttab.jfno,afttab.flno,afttab.alc3,afttab.alc2,afttab.urno,'2' as cmd,afttab.tifd,afttab.GD2X openA,afttab.GD2Y closeA,afttab.REMP,afttab.stod,afttab.etod  $BOAE   FROM afttab,fldtab WHERE";
        $tmp = $tmp . " afttab.urno = fldtab.aurn   AND  fldtab.rtyp = '" . $FLDRTYPGate . "' AND ";
        /*
         *
         *
          $tmp = $tmp ." (gtd2 = '".$Session["Number"]."')  ";
          $tmp = $tmp ." ";
          if ($GateAllocTIF) {
          $tmp = $tmp ." AND (TIFD BETWEEN " . CedaSYSDATE ('n',  $GateFrom)." AND " . CedaSYSDATE ('n',  $GateTo).")  ";
          } else {
          $tmp = $tmp ." AND (DECODE(GD2Y,'              ', GD2E, GD2Y) > " . CedaSYSDATE ('n', $GateFrom).")   ";
          $tmp = $tmp ." AND GD2Y < " . CedaSYSDATE ('n',  $GateTo)."   ";
          $tmp = $tmp ." AND (DECODE(GD2X,'              ', GD2B, GD2X) BETWEEN  " . CedaSYSDATE ('n',  $GateFrom)." AND " . CedaSYSDATE ('n',  30).") ";
          $tmp = $tmp ." AND GD2B  BETWEEN  " . CedaSYSDATE ('n',  $GateFrom)." AND " . CedaSYSDATE ('n',  $GateTo)." ";

          }
          //$tmp = $tmp ." )";

          $tmp = $tmp ." AND OFBL = ' ' AND FTYP='O' ORDER by TIFD ";
         *
         */
        $tmp = $tmp . " (    (gtd2 = '" . $Session["Number"] . "') ";
        /*
          if ($GateAllocTIF==false) {
          $tmp = $tmp ."   AND (   (DECODE (gd2y, '              ', gd2e, gd2y) > ". CedaSYSDATE ('n',  $GateFrom);
          $tmp = $tmp ."         ) ";
          $tmp = $tmp ." OR (gd2y LIKE '  %') ";
          $tmp = $tmp ." ) ";
          $tmp = $tmp ." AND (    (DECODE (gd2x, '              ', gd2b, gd2x) ";
          $tmp = $tmp ." BETWEEN ".CedaSYSDATE ('n',  $GateFrom);
          $tmp = $tmp ." AND ".CedaSYSDATE ('n',  $GateTo);
          $tmp = $tmp ." ) ";
          $tmp = $tmp ." AND (gd2b <> ' ') ";
          $tmp = $tmp ." )";

          } */
        $tmp = $tmp . " AND OFBL = ' ' AND ftyp in (" . $Ftyp . ") ";
        $tmp = $tmp . " AND (tifd BETWEEN " . CedaSYSDATE('n', $GateFrom);
        $tmp = $tmp . " AND " . CedaSYSDATE('n', $GateFrom + $GateAllocTIFExtraTime);
        $tmp = $tmp . " ) ";
        $tmp = $tmp . " ) ";
        $tmp = $tmp . " ORDER by TIFD";
        $tmp1 = "";
        //		echo $tmp;
        //		exit;
    } else {
        //		$tmp="SELECT FLNO, JFNO, ALC3, URNO, 'D' as cmd1 from AFTTAB WHERE (TIFD BETWEEN TO_CHAR(SYSDATE -30/1440,'YYYYMMDDHH24MISS') AND TO_CHAR(SYSDATE+720/1440,'YYYYMMDDHH24MISS')) AND FTYP='O'  AND (URNO IN  (SELECT FLNU from CCATAB  WHERE  (((CKIC = '".$Session["Number"]."')  AND ((DECODE(CKBA,'              ', CKBS, CKBA) < TO_CHAR(SYSDATE+30/1440,'YYYYMMDDHH24MISS') AND CKBS<>' ')) AND ((CKEA > TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS')) OR ((CKES LIKE ' %') AND (CKEA LIKE ' %'))  OR (CKEA LIKE ' %'))))))  ORDER by tifd";
        //Optimizing the Query 23-02-2005
        //Change REMP field to DISP
        $tmp = " SELECT   afttab.flno, afttab.jfno, afttab.alc3, afttab.alc2,ccatab.urno, 'D' AS cmd,";
        $tmp = $tmp . " CKBA openA,CKEA closeA,disp as REMP,afttab.stod,afttab.etod,ckbs,ckes ";
        $tmp = $tmp . "    FROM afttab,ccatab ";
        $tmp = $tmp . "   WHERE ";
        $tmp = $tmp . "     afttab.urno=ccatab.flnu";
        if ($CheckinAllocTIF == true) {
            $tmp = $tmp . "   	 AND (afttab.tifd BETWEEN " . CedaSYSDATE('n', $CheckinFrom) . " AND " . CedaSYSDATE('n', $CheckinTo) . ")";
        }
        $tmp = $tmp . "     AND afttab.ftyp in (" . $Ftyp . ")";
        $tmp = $tmp . "     AND  (    ";
        $tmp = $tmp . "	 	  	   (ccatab.ckic = '" . $Session["Number"] . "')";
        if ($CheckinAllocTIF == false) {
            $tmp = $tmp . " AND ( ";
            $tmp = $tmp . "       (    DECODE (ccatab.ckba,'              ', ccatab.ckbs,ccatab.ckba) ";
            $tmp = $tmp . "        BETWEEN " . CedaSYSDATE('n', $CheckinFrom) . " AND " . CedaSYSDATE('n', $CheckinTo);
            $tmp = $tmp . "        AND ccatab.ckbs <> ' ' ";
            //Added by GFO 20090324
            $tmp = $tmp . " AND (ccatab.ckes > " . CedaSYSDATE('h', 0) . ") ";
            $tmp = $tmp . "       ) ";
            if ($OldOpenFlights == true) {
                $tmp = $tmp . "     OR ((ccatab.ckba <> ' ') AND (ccatab.ckea = ' ')) ";
            }
            $tmp = $tmp . " ) ";
            // To be evaluated it receives all the Allocations
            //
            //$tmp=$tmp." OR ((ccatab.ckba <> ' ') AND (ccatab.ckea = ' ')) ";
        }
        $tmp = $tmp . "            )";
        $tmp = $tmp . "  UNION ALL ";
        //Common Counters
        $tmp = $tmp . " SELECT 'Common' AS flno , ' ' AS jfno,alttab.alc3 , alttab.alc2,";
        $tmp = $tmp . " ccatab.urno AS urno,'C' AS cmd,ckba openA, ckea closeA, ";
        $tmp = $tmp . " disp AS REMP,'' as stod, '' as etod,ckbs,ckes ";
        $tmp = $tmp . " FROM alttab, ccatab ";
        $tmp = $tmp . " WHERE ccatab.flnu = alttab.urno ";
        $tmp = $tmp . " AND ccatab.ckic = '" . $Session["Number"] . "' ";
        $tmp = $tmp . " AND ccatab.ctyp = 'C' ";
        $tmp = $tmp . " AND ( ";
        if ($OldOpenFlights == true) {
            $tmp = $tmp . "       (DECODE (ccatab.ckba,'              ', ccatab.ckbs,ccatab.ckba)";
        } else {
            $tmp = $tmp . "      ( ccatab.ckbs ";
        }
        $tmp = $tmp . "       BETWEEN " . CedaSYSDATE('d', $CommonCheckinFrom) . " AND " . CedaSYSDATE('n', $CommonCheckinTo);
        $tmp = $tmp . "       AND ccatab.ckbs <> ' ' ";
        //Added by GFO 20090324
        $tmp = $tmp . " AND (ccatab.ckes > " . CedaSYSDATE('h', 0) . ") ";
        $tmp = $tmp . "       ) ";
        // To be evaluated it receives all the Allocations
        //
        if ($OldOpenFlights == true) {
            $tmp = $tmp . "      OR ((ccatab.ckba <> ' ') AND (ccatab.ckea = ' ')) ";
        }
        $tmp = $tmp . " ) ";

        //Common Counters (AirlineGroups)
        if ($enableGroupCheckin) {
            $tmp = $tmp . "  UNION ALL ";
            $tmp = $tmp . " SELECT 'Common Group' AS flno , ' ' AS jfno,grntab.GRPN as alc3 , grntab.GRSN as alc2,";
            $tmp = $tmp . " ccatab.urno AS urno,'C' AS cmd,ckba openA, ckea closeA, ";
            $tmp = $tmp . " disp AS REMP,'' as stod, '' as etod  ,ckbs,ckes";
            $tmp = $tmp . " FROM grntab, ccatab ";
            $tmp = $tmp . " WHERE ccatab.gurn = grntab.urno ";
            $tmp = $tmp . " AND ccatab.ckic = '" . $Session["Number"] . "' ";
            $tmp = $tmp . " AND ccatab.ctyp = 'C' ";
            $tmp = $tmp . " AND ( ";
            if ($OldOpenFlights == true) {
                $tmp = $tmp . "       (DECODE (ccatab.ckba,'              ', ccatab.ckbs,ccatab.ckba)";
            } else {
                $tmp = $tmp . "      ( ccatab.ckbs ";
            }
            $tmp = $tmp . "       BETWEEN " . CedaSYSDATE('d', $CommonCheckinFrom) . " AND " . CedaSYSDATE('n', $CommonCheckinTo);
            $tmp = $tmp . "       AND ccatab.ckbs <> ' ' ";
            //Added by GFO 20090324
            $tmp = $tmp . " AND (ccatab.ckes > " . CedaSYSDATE('h', 0) . ") ";
            $tmp = $tmp . "       ) ";
            // To be evaluated it receives all the Allocations
            //
            if ($OldOpenFlights == true) {
                $tmp = $tmp . "      OR ((ccatab.ckba <> ' ') AND (ccatab.ckea = ' ')) ";
            }
            $tmp = $tmp . " ) ";
        }


        //        $tmp = $tmp . " AND (ccatab.ckbs BETWEEN " . CedaSYSDATE ('d',  $CommonCheckinFrom)." AND " . CedaSYSDATE ('n',  $CommonCheckinTo).")";
        //        $tmp = $tmp . " AND (ccatab.ckes > " . CedaSYSDATE ('h',  0).") ";
        //$tmp=$tmp." ORDER BY afttab.tifd ";
        //Optimizing the Query 23-02-2005
        //		$tmp1="SELECT ccatab.urno as urno, ' ' as jfno, 'Common' as flno, alttab.alc2, 'C' as cmd1 FROM alttab,ccatab WHERE  ccatab.flnu=alttab.urno(+)  AND   ccatab.ckic='".$Session["Number"]."'  AND   ccatab.ctyp='C' AND ( ccatab.CKBS BETWEEN  TO_CHAR(SYSDATE-1,'YYYYMMDDHH24MISS') AND TO_CHAR(SYSDATE+30/1440,'YYYYMMDDHH24MISS')) AND  (ccatab.CKES > TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS')) AND (CKBA=' ' or CKBA <=  TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS') )AND (CKEA=' ') ";
        //It will not return any data if the the flnu field in the ccatab is empty for the Common Checkin
        //Added ALC2, ALC3, REMA
        //WAW Project
        //		$tmp1="SELECT ccatab.urno as urno, ' ' as jfno, 'Common' as flno, alttab.alc2,alttab.alc3,ccatab.rema, 'C' as cmd1,CKBA openA,CKEA closeA,REMA as REMP FROM alttab,ccatab WHERE  ccatab.flnu=alttab.urno  AND   ccatab.ckic='".$Session["Number"]."'  AND   ccatab.ctyp='C' AND ( ccatab.CKBS BETWEEN  " . CedaSYSDATE ('d',  -1)." AND " . CedaSYSDATE ('n',  30).") AND  (ccatab.CKES > " . CedaSYSDATE ('h',  0).") AND (CKBA=' ' or CKBA <=  " . CedaSYSDATE ('h',  0)." )AND (CKEA=' ') ";
        /*
          $tmp1 = "SELECT ccatab.urno AS urno, ' ' AS jfno, 'Common' AS flno, alttab.alc2,";
          $tmp1 = $tmp1 . "alttab.alc3, ccatab.rema, 'C' AS cmd1, ckba opena, ckea closea,";
          $tmp1 = $tmp1 . " rema AS remp ";
          $tmp1 = $tmp1 . " FROM alttab, ccatab ";
          $tmp1 = $tmp1 . " WHERE ccatab.flnu = alttab.urno ";
          $tmp1 = $tmp1 . "   AND ccatab.ckic = '".$Session["Number"]."' ";
          $tmp1 = $tmp1 . "   AND ccatab.ctyp = 'C' ";
          $tmp1 = $tmp1 . "   AND (ccatab.ckbs BETWEEN " . CedaSYSDATE ('d',  $CommonCheckinFrom)." AND " . CedaSYSDATE ('n',  $CommonCheckinTo).")";
          $tmp1 = $tmp1 . "   AND (ccatab.ckes > " . CedaSYSDATE ('h',  0).") ";
         */
        //$tmp1 = $tmp1 . "   AND (ckba = ' ' OR ckba <= " . CedaSYSDATE ('h',  0).") ";
        //$tmp1 = $tmp1 . "   AND (ckea = ' ') ";
    }



    $cmdTempCommandText = $tmp;
    //echo $tmp;
    //exit;

    $logger->debug("CheckAlloc cmdTemp" . $tmp);
    $logger->debug("CheckAlloc CheckinAllocTIF:" . $CheckinAllocTIF);

    //$logger->debug("CheckAlloc cmdTemp1".$tmp1);


    $Session["FldRows"] = 0;
    $Session["OldCounterOpen"] = False;
    $rs = $db->Execute($cmdTempCommandText);
    $myrows = 0;
    // create a JSON service
    $json = new Services_JSON();
    // constructing a JSON
    $responce->page = 1;
    $responce->total = 1;
    $responce->records = 10;

    if (!$rs->EOF) {
        $tdi = FindTDI();
        while (!$rs->EOF) {

            $closeA = trim($rs->fields("closeA"));
            $ctime = CedaSYSDATE('n', 0);
            $ctime = str_replace("'", "", $ctime);


            $biggFive = false;
            if ($Session["Type"] == "GATE" && len($closeA) > 0) {
                $closeA = ChopString("n", 0, $closeA);
                $nowtime = ChopString("n", 0, $ctime);
                if (DateDiff("n", $closeA, $nowtime) >= $FlightTimeDiffGates) {
                    $biggFive = true;
                }
            }

            $Session["GateNumber"] = trim($rs->fields("cmd"));


            if ($biggFive == false) {
                $tmp = "";
                $StrString = 1;
                $remp = "&nbsp;&nbsp;&nbsp;";
                $Session["CommonF"] == 0;
                if ($Session["Type"] != "GATE") {
                    if ($Session["GateNumber"] == 'D') {
                        $Session["CommonF"] = 0; //Dedicated
                    } else {
                        $Session["CommonF"] = 1; //Common
                    }
                }

                //Added $Session["LogoName"] for Gates and Dedicted Checkin IN
                //WAW Project
                //echo $rs->fields("REMP");

                $Session["LogoNameALC"] = false;
                $open = "false";
                //  || trim($rs->fields("REMP"))==$RemarkGateOpen
                //  || trim($rs->fields("REMP"))==$RemarkGateClose
                //   || trim($rs->fields("REMP"))==$RemarkGateBoarding
                //  || trim($rs->fields("REMP"))==$RemarkGateFinalCall

                $Session["CKICOpen"] = false;
                if ($Session["Type"] == "GATE") {
                    if (len(trim($rs->fields("BOAE")))>0) {
                        $logger->debug("CheckAlloc BOAE:" . trim($rs->fields("BOAE")));
                        $logger->debug("CheckAlloc tdi" . $tdi);
                        $logger->debug("CheckAlloc BOAE" . ChopString("n", $tdi, trim($rs->fields("BOAE"))));

                        $Session["BOAE"] = date('Hi', ChopString("n", $tdi, trim($rs->fields("BOAE"))));
                    } else {
                        $Session["BOAE"] = "0000";
                    }
                } else {
                    if ($openCounterWithinScheduleTimes) {
                        $ckbs = ChopString("n", 0, trim($rs->fields("ckbs")));
                        $nowtime = ChopString("n", 0, $ctime);
                        $logger->debug("CheckAlloc ckbs:" . $ckbs);
                        $logger->debug("CheckAlloc nowtime:" . $nowtime);
                        if ($ckbs < $nowtime) {
                            $Session["CKICOpen"] = true;
                        }
                    }
                }
                $logger->debug("CheckAlloc CKICOpen" . $Session["CKICOpen"]);


                if ( strlen(trim($rs->fields("closeA"))) > 1) {
                    $logger->debug("CheckAlloc Counter is already close" . $Session["CKICOpen"]);
                    $Session["Button"] = "Close";
                }


                if (( strlen(trim($rs->fields("openA"))) > 1)
                        || len(trim($rs->fields("REMP"))) > 0) {



                    if ($Session["Type"] == "GATE") {
                        if (strlen(trim($rs->fields("alc2"))) > 1) {
                            $Session["LogoName"] = trim($rs->fields("alc2"));
                        } else {
                            $Session["LogoName"] = trim($rs->fields("alc3"));
                        }
                    } else {

                        if (strlen(trim($rs->fields("REMP"))) > 1) {
                            $Session["LogoName"] = trim($rs->fields("REMP"));
                        } else {
                            if (strlen(trim($rs->fields("alc2"))) > 1) {
                                $Session["LogoName"] = trim($rs->fields("alc2"));
                            } else {
                                $Session["LogoName"] = trim($rs->fields("alc3"));
                            }

                            //Ungly Hack for the Common Checkin Counters
                            //with Airline Group
                            if (trim($rs->fields("flno")) == "Common Group") {
                                $Session["LogoNameALC"] = false;
                            } else {
                                $Session["LogoNameALC"] = true;
                            }
                        }
                    }
                    $logger->debug("FlightURNO :" . $Session["FlightURNO"] . "--" . trim($rs->fields("urno")));
                    if (!isset($Session["FlightURNO"])) {
                        $Session["FlightURNO"] = trim($rs->fields("urno"));
                    }

                    if (isset($Session["FlightURNO"])) {
                        if ($Session["FlightURNO"] == trim($rs->fields("urno"))) {
                            $tmp = " selected";
                            $open = "true";
                            $Session["OldCounterOpen"] = true;
                        }
                    } else {
                        $tmp = " selected";
                        $open = "true";
                        $Session["OldCounterOpen"] = true;
                        $Session["FlightURNO"] = trim($rs->fields("urno"));
                    }
                }
                $logger->debug("FlightURNO" . $Session["FlightURNO"]);

                $Session["ALC2"] = trim($rs->fields("alc2"));
                $Session["ALC3"] = trim($rs->fields("alc3"));

                $alc2 = trim($rs->fields("alc2"));

                if ($alc2 == "") {
                    $alc2 = trim($rs->fields("alc3"));
                }



                $optionstr = "";
                $optionstr_sep = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";


                if ($ShowResults == 1) {



                    //				$tmp="";
                    //echo $Session["Fldaurn"]."--".trim($rs->fields("urno"));
                    //if ($Session["STATUS"]==0 && $Session["FldRows"]==0) { $tmp=" selected";}
                    //				if ($Session["STATUS"]==0 ) { $tmp=" selected";}
                    //$Session["OldCounterOpen"]=False;
                    //				$tmp=" selected";
                    /*
                      if ($Session["Fldaurn"]== trim($rs->fields("urno")) ) {

                      $Session["OldCounterOpen"]=False;
                      } */

                    //Changed by GFO 20080219
                    //PRF 8841
                    $etod = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    $stod = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    if ($Session["CommonF"] == 0 || $Session["Type"] == "GATE") {
                        //$tdi = FindTDI();
                        $stod = date("H:i", ChopString("n", $tdi, $rs->Fields("stod")));

                        if (len(trim($rs->Fields("etod"))) > 0) {
                            $etod = date("H:i", ChopString("n", $tdi, $rs->Fields("etod")));
                        }
                    }

                    $optionstr = $optionstr . trim($rs->fields("flno")) . $optionstr_sep;
                    $optionstr = $optionstr . $alc2 . $optionstr_sep;
                    if (len(trim($rs->fields("remp"))) > 0) {
                        //$optionstr = $optionstr .trim($rs->fields("remp")) .$optionstr_sep;
                        $remp = trim($rs->fields("remp"));
                    } else {
                        if ($Session["Type"] == "GATE") {
                            $remp = "&nbsp;&nbsp;&nbsp;";
                        }
                    }

                    $optionstr = $optionstr . $stod . $optionstr_sep;
                    $optionstr = $optionstr . $etod . $optionstr_sep;

                    //$optionstr = $optionstr .$rs->Fields("stod") .$optionstr_sep;
                    //$optionstr = $optionstr .$rs->Fields("etod") .$optionstr_sep;

                    if (!$AjaxEnabled) {
                        echo "<option value=" . trim($rs->fields("urno")) . " " . $tmp . ">" . $optionstr . "\n";
                    } else {
                        $responce->rows[$myrows]['urno'] = $rs->Fields("urno");
                        $responce->rows[$myrows]['cell'] = array($rs->Fields("urno"), $open, $rs->Fields("flno"), $alc2, $remp, $stod, $etod);
                    }
                    $myrows++;
                    //echo "<option value=".trim($rs->fields("urno"))." ".$tmp.">". $optionstr ."\n";
                    $logger->debug("lenjfno:" . $rs->fields("jfno"));
                    //Code Share Flights
                    $lenJFNO = (int) (strlen(trim($rs->fields("jfno"))) / 9);
                    $logger->debug("lenjfno:" . $lenJFNO);
                    $Sstart = 0;
                    $Send = 9;
                    //if ($lenJFNO>0) {$Session["CTYP"]="M".$lenJFNO+1;}
                    for ($i = 0; $i <= $lenJFNO; $i++) {
                        $st1 = substr(trim($rs->fields("jfno")), $Sstart, $Send);
                        if (len($st1) > 0) {
                            $optionstr = "";
                            $optionstr = $optionstr . trim($st1) . $optionstr_sep;
                            $optionstr = $optionstr . $optionstr_sep . "&nbsp;&nbsp;";
                            /*
                              if (len(trim($rs->fields("remp")))>0) {
                              $optionstr = $optionstr .trim($rs->fields("remp")) .$optionstr_sep;
                              } else {
                              if ($Session["Type"]=="GATE") {
                              $optionstr = $optionstr .$optionstr_sep ."&nbsp;&nbsp;&nbsp;";
                              }
                              }
                             * */

                            //$optionstr = $optionstr .trim($rs->fields("remp")) .$optionstr_sep;
                            $optionstr = $optionstr . "" . $stod . $optionstr_sep;
                            $optionstr = $optionstr . $etod . $optionstr_sep;

                            if (!$AjaxEnabled) {
                                echo "<option value=" . trim($rs->fields("urno")) . " " . $tmp . ">" . $optionstr . "\n";
                            } else {
                                $responce->rows[$myrows]['urno'] = $rs->Fields("urno");
                                //$responce->rows[$myrows]['cell']=array($rs->Fields("urno"),$st1,$stod);
                                $responce->rows[$myrows]['cell'] = array($rs->Fields("urno"), $open, $st1, $alc2, $remp, $stod, $etod);
                            }
                            $myrows++;
                            // echo "<option value=".trim($rs->fields("urno")).">". $optionstr ."\n";
                            $logger->debug("Start:" . $Sstart . "--End" . $Send . "--St1" . $st1);
                        }
                        $Sstart = $Sstart + 9;
                    }
                    $Session["FldRows"] = $Session["FldRows"] + 1;
                }
            } //End BigFive

            $rs->MoveNext();
        }
    } // If End

    if ($rs)
        $rs->Close();
    //	exit;
    $logger->debug("ShowResults " . $ShowResults);

    $GLOBALS['Session'] = $Session;

    if ($ShowResults == "0") {
        $logger->debug("StrString " . $StrString);
        return $StrString;
    } else {
        //$logger->debug("responce ".$responce);
        // exit;
        if (!$AjaxEnabled) {
            //
            return "";
        } else {
            return $json->encode($responce);
        }
    }
}

function UpdateUfisLH($FlightURNO, $CallButton, $PRemarkCode, $Text1, $Text2, $LogoSelect, $GateBoardingExValue) {
    //Global $db, $Session, $FlightURNO;
    //Global $PRemarkCode,$Text1,$Text2,$RSHIP;
    //Global $timer,$FreeTextSelection,$logoSelectionType,$logoSelection,$FLZTABuse;
    //Global $FLDRTYPGate,$FLDRTYPCheckin;
    //Global $RemarkGateOpen,$RemarkGateBoarding,$RemarkGateFinalCall,$RemarkGateClose;
    //Global $FLDUse;

    $db = $GLOBALS['db'];
    $Session = $GLOBALS['Session'];



    //$FlightURNO = $GLOBALS['FlightURNO'];
    //$PRemarkCode = $GLOBALS['PRemarkCode'];
    //$Text1 = $GLOBALS['Text1'];
    //$Text1 = $GLOBALS['Text2'];
    /* Set Timer Instance */
    $timer = new PHP_timer;
    $timer->start();

    $RSHIP = $GLOBALS['RSHIP'];
    //$timer = $GLOBALS['timer'];
    $FreeTextSelection = $GLOBALS['FreeTextSelection'];
    $logoSelectionType = $GLOBALS['logoSelectionType'];
    // $logoSelection = $GLOBALS['logoSelection'];
    $FLZTABuse = $GLOBALS['FLZTABuse'];
    $FLDRTYPGate = $GLOBALS['FLDRTYPGate'];
    $FLDRTYPCheckin = $GLOBALS['FLDRTYPCheckin'];
    $RemarkGateOpen = $GLOBALS['RemarkGateOpen'];
    $RemarkGateBoardingEx = $GLOBALS['RemarkGateBoardingEx'];
    $RemarkGateBoarding = $GLOBALS['RemarkGateBoarding'];
    $RemarkGateFinalCall = $GLOBALS['RemarkGateFinalCall'];
    $RemarkGateClose = $GLOBALS['RemarkGateClose'];
    $FLDUse = $GLOBALS['FLDUse'];
    $NoTimeout = $GLOBALS['NoTimeout'];
    $logoSelection = $Session["logoSelection"];
    //RemarksType
    $PRemarksRemTYPGate = $GLOBALS['PRemarksRemTYPGate'];
    $callcput = true;

    ////////////////////////////////////////////////////////////
    //
    //		FLIGHT LISTBOX FOR FLDTAB
    //
    //	If the counter is opened just send the values to the
    //	FLDTAB. The Flight list to show will be queried from
    //	the AODB.
    //	In case of a closed counter create this flight list
    //	from the user's listbox selections and send the string
    //	aling with the other formular values to the
    //	FLDTAB.
    ///////////////////////////////////////////////////////////

    $AOTI = " ";
    $ABTI = " ";
    $AFTI = " ";
    $CTYP = " ";
    $STAT = " ";

    $logger = & LoggerManager::getLogger('CuteIf');


    $Session["FlightURNO"] = $FlightURNO;

    if ($Session["Type"] == "GATE") {
        // SELECT DATES FROM AFTTAB FOR GATES
        $cmdTempCommandText = "SELECT GD1B,GD1E,GD2B,GD2E FROM AFTTAB WHERE  URNO= " . $FlightURNO;
        $rs = $db->Execute($cmdTempCommandText);
        if (!$rs->EOF) {
            //IF AG1
            $GD1B = $rs->fields("GD1B");
            $GD1E = $rs->fields("GD1E");

            //IF AG2
            $GD2B = $rs->fields("GD2B");
            $GD2E = $rs->fields("GD2E");
            $UPDATEURNO = $FlightURNO;
        } else {
            // EXIT From the Function
            $logger->debug('IBT NoData Found :Exit From Update Function Gate Select' . $Session["Number"]);
            return 0;
        }// If End
        if ($rs)
            $rs->Close();
    } else {
        // SELECT VALUES FROM CCATAB FOR CHECKIN
        $cmdTempCommandText = "SELECT URNO,FLNU ,CKBS,CKES FROM CCATAB WHERE CKIC='" . $Session["Number"] . "' and (FLNU='" . $FlightURNO . "' or URNO='" . $FlightURNO . "') ";
        //and ((ckba=' ') or ((ckba< " . CedaSYSDATE ('n',  30)." and ckba<>' ') and (ckea=' ' or ckea> " . CedaSYSDATE ('h',  0).")))";
        $rs = $db->Execute($cmdTempCommandText);
        if (!$rs->EOF) {
            //If common
            //if ($Session["CommonF"]==1) {
            //} else {
            //     $CCAURNO=$rs->fields("FLNU");
            // }
            $CCAURNO = $rs->fields("URNO");
            $CKBS = $rs->fields("CKBS");
            $CKES = $rs->fields("CKES");

            $UPDATEURNO = $CCAURNO;
        } else {
            // EXIT From the Function
            //echo 'exit';
            $logger->debug('IBT NoData Found :Exit From Update Function Checkin Select' . $Session["Number"]);
            return 0;
        }// If End
        if ($rs)
            $rs->Close();
        // SELECT URNO FROM FLDTAB
    }

    // PRemark
    //If empty
    if (len($PRemarkCode) == 0) {
        $CREC = " ";
    } else {
        $CREC = $PRemarkCode;
    }
    //if None
    if ($PRemarkCode == "None") {
        $CREC = " ";
    } else {
        $CREC = $PRemarkCode;
    }
    //if 00000
    if ($PRemarkCode == "00000000") {
        $CREC = " ";
        $PRemarkCode = "None";
    }
    //undefined
    if ($PRemarkCode == "undefined") {
        $CREC = " ";
    }
    if ($PRemarkCode == "Undefined") {
        $CREC = " ";
    }

    // Replace any occurence of ',' or '?' with ' '
    if (len($Text1) == 0) {
        $Text1 = " ";
    } else {
        $Text1 = str_replace(",", " ", $Text1);
        $Text1 = str_replace(";", "?", $Text1);
    }
    if (len($Text2) == 0) {
        $Text2 = " ";
    } else {
        $Text2 = str_replace(",", " ", $Text2);
        $Text2 = str_replace(";", "?", $Text2);
    }

    if (len($AOTI) == 0) {
        $AOTI = " ";
    }
    if (len($ABTI) == 0) {
        $ABTI = " ";
    }
    if (len($AFTI) == 0) {
        $AFTI = " ";
    }

    if (len($CREC) == 0) {
        $CREC = " ";
    }
    if (len($CTYP) == 0) {
        // COMMON CHECKIN
        if ($Session["CommonF"] == 1) {
            $CTYP = " ";
        } else {
            $CTYP = "M";
        }
    }
    $PicName = $Session["PicName"];
    if ($Session["PicName"] == 'None') {
        $PicName = ' ';
    }

    $tmp = "";
    if ($Session["Type"] == "GATE") {
        $tmp = $FLDRTYPGate;
    } else {
        $tmp = $FLDRTYPCheckin;
    }

    //* to be removed when FXTTAB is changed
    $cmdTempCommandText = "SELECT URNO FROM FLDTAB WHERE RTYP = '" . $tmp . "' AND RNAM = '" . $Session["Number"] . "' AND DSEQ > 0";
    //echo $cmdTempCommandText;
    $logger->debug('FLDTAB Select :' . $cmdTempCommandText);
    $rs = $db->Execute($cmdTempCommandText);
    if (!$rs->EOF) {
        $UPDATE_FLDURNOFXT = trim($rs->fields("URNO"));
    } else {
        $UPDATE_FLDURNOFXT = 0;
        // EXIT From the Function
        //echo 'exit2';
        //$logger->debug('Select NoData Found :Exit From Update Function Checkin Select'.$Session["Number"]);
        //return 0;
    }


    if ($FLDUse == true) {
        $RField = "FLDU";

        $cmdTempCommandText = "SELECT URNO FROM FLDTAB WHERE RTYP = '" . $tmp . "' AND RNAM = '" . $Session["Number"] . "' AND DSEQ > 0";
        //echo $cmdTempCommandText;

        $rs = $db->Execute($cmdTempCommandText);
        if (!$rs->EOF) {
            $UPDATE_FLDURNO = trim($rs->fields("URNO"));
        } else {
            $UPDATE_FLDURNO = 0;
            // EXIT From the Function
            //echo 'exit2';
            //$logger->debug('Select NoData Found :Exit From Update Function Checkin Select'.$Session["Number"]);
            //return 0;
        }
    } else {
        $UPDATE_FLDURNO = $UPDATEURNO;
        $RField = "RURN";
    }

    $Session["UPDATE_FLDURNO"] = $UPDATE_FLDURNO;

    if (len($STAT) == 0) {
        $STAT = " ";
    }
    if ($Session["Type"] == "GATE") {
        //20081212 GFO
        //Changed frm A to G
        $UTYP = "G"; //A
    } else {
        $UTYP = "C";
    }
    if ($CallButton == 'LogoTransmit') {
        $logger->debug('LogoSelect:' . $LogoSelect);
        if (len($LogoSelect) > 1 && $CallButton == "LogoTransmit") {
            $logger->debug('logoSelection:' . $logoSelection);
            if ($logoSelection == true) {


                if ($FLZTABuse == true) {
                    $logger->debug('FLDTAB Select True');
                    // SEND LOGOS INTO FLZTAB
                    // SELECT URNO FROM FLZTAB where fldu = fldtab.urno

                    $cmdTempCommandText = "SELECT URNO FROM FLZTAB WHERE " . $RField . "=" . $UPDATE_FLDURNO . " AND UTYP = '" . $UTYP . "' and LNAM='" . $Session["Number"] . "'";
                    $rs = $db->Execute($cmdTempCommandText);
                    $logger->debug('FLZTAB Select :' . $cmdTempCommandText);
                    if (!$rs->EOF) {

                        //20081202 add the Clear Options
                        // if ($LogoSelect=="None" || $CREC=="Clear" || $CallButton=="Clear") {
                        //20081212 GFO
                        //Removed the  clear option
                        if ($LogoSelect == "None") {
                            //Delete
                            $Command = "\'DRT\'";
                        } else {
                            $Command = "\'URT\'";
                        }

                        $FLZURNO = trim($rs->fields("URNO"));
                        $Dbg_level = 101;
                        $Dest = "\'1200\'";
                        $Prio = "\'3\'";

                        $Table = "\'FLZTAB\'";
                        $Fields = "\'FLGU\'";
                        $Data = "\'" . $Session["AirlineURNO"] . "\'";
                        $Selection = "\'WHERE URNO = " . trim($FLZURNO) . "\'";
                        $Timeout = $NoTimeout;

                        //Send that stuff
                        CallCput($Dbg_level, $Dest, $Prio, $Command, $Table, $Fields, $Data, $Selection, $Timeout, $currenttime);
                    } else {
                        $Dbg_level = 101;
                        $Dest = "\'1200\'";
                        $Prio = "\'3\'";
                        $Command = "\'IBT\'";
                        $Table = "\'FLZTAB\'";
                        //20081212 GFO
                        //Add the LSEQ Field
                        $Fields = "\'$RField,FLGU,UTYP,LSEQ,LNAM\'";
                        $LSEQ = " ";
                        if ($Session["Type"] == "GATE") {
                            $LSEQ = $Session["GateNumber"];
                        }

                        $Data = "\'" . $UPDATE_FLDURNO . "," . $Session["AirlineURNO"] . "," . $UTYP . "," . $LSEQ . "," . $Session["Number"] . "\'";
                        $Selection = "\'\'";
                        $Timeout = $NoTimeout;

                        //Send that stuff
                        CallCput($Dbg_level, $Dest, $Prio, $Command, $Table, $Fields, $Data, $Selection, $Timeout, $currenttime);
                    }// If End
                    if ($rs)
                        $rs->Close();
                    $timer->addmarker("Second Cput (FLZTAB)");
                } else {
                    if ($Session["Type"] == "GATE") {
                        //Nothing
                    } else {

                        $Dbg_level = 101;
                        $Dest = "\'1200\'";
                        $Prio = "\'3\'";
                        $Command = "\'URT\'";
                        $Table = "\'CCATAB\'";
                        $Fields = "\'REMA\'";
                        $Data = "\'" . $PicName . ",CUTE-IF\'";
                        $Selection = "\'WHERE URNO = " . trim($CCAURNO) . "\'";
                        $Timeout = $NoTimeout;
                        //Send that stuff
                        CallCput($Dbg_level, $Dest, $Prio, $Command, $Table, $Fields, $Data, $Selection, $Timeout, $currenttime);
                    }
                }
            }
        }
    }

    if ($CallButton == 'Transmit') {
        if (len($Text1) > 1 || len($Text2) > 1) {
            if ($FreeTextSelection == true) {
                // SEND FREE TEXTES INTO FXTTAB
                // SELECT URNO FROM FXTTAB where fldu = fldtab.urno
                // FREE TEXT 1
                if (len(trim($Text1)) == 0) {
                    $Text1 = " ";
                }

                // $cmdTempCommandText = "SELECT URNO FROM FXTTAB WHERE FLDU=" .$UPDATE_FLDURNO." AND UTYP = '".$UTYP."' AND SORT = 1";
                $cmdTempCommandText = "SELECT URNO FROM FXTTAB WHERE FLDU=" . $UPDATE_FLDURNOFXT . "  AND SORT = 1";
                //echo $cmdTempCommandText;
                $rs = $db->Execute($cmdTempCommandText);
                $logger->debug('FXTTAB Select :' . $cmdTempCommandText);
                if (!$rs->EOF) {
                    $FLXURNO = trim($rs->fields("URNO"));
                    $Dbg_level = 101;
                    $Dest = "\'1200\'";
                    $Prio = "\'3\'";
                    $Command = "\'URT\'";
                    $Table = "\'FXTTAB\'";
                    $Fields = "\'TEXT\'";
                    $Data = "\'" . $Text1 . "\'";
                    $Selection = "\'WHERE URNO = " . trim($FLXURNO) . "\'";
                    $Timeout = $NoTimeout;

                    //Send that stuff
                    CallCput($Dbg_level, $Dest, $Prio, $Command, $Table, $Fields, $Data, $Selection, $Timeout, $currenttime);
                } else {
                    $Dbg_level = 101;
                    $Dest = "\'1200\'";
                    $Prio = "\'3\'";
                    $Command = "\'IBT\'";
                    $Table = "\'FXTTAB\'";
                    //$Fields="\'FLDU,TEXT,UTYP,SORT\'";
                    $Fields = "\'FLDU,TEXT,SORT\'";
                    $Data = "\'" . $UPDATE_FLDURNOFXT . "," . $Text1 . ",1\'";
                    $Selection = "\'\'";
                    $Timeout = $NoTimeout;

                    //Send that stuff
                    CallCput($Dbg_level, $Dest, $Prio, $Command, $Table, $Fields, $Data, $Selection, $Timeout, $currenttime);
                }// If End
                if ($rs)
                    $rs->Close();
                $timer->addmarker("3rd Cput (FXTTAB)");




                if (len(trim($Text2)) == 0) {
                    $Text2 = " ";
                }
                // FREE TEXT 2
                $cmdTempCommandText = "SELECT URNO FROM FXTTAB WHERE FLDU=" . $UPDATE_FLDURNOFXT . "   AND SORT = 2";
                $rs = $db->Execute($cmdTempCommandText);
                if (!$rs->EOF) {
                    $FLXURNO = trim($rs->fields("URNO"));
                    $Dbg_level = 101;
                    $Dest = "\'1200\'";
                    $Prio = "\'3\'";
                    $Command = "\'URT\'";
                    $Table = "\'FXTTAB\'";
                    $Fields = "\'TEXT\'";
                    $Data = "\'" . $Text2 . "\'";
                    $Selection = "\'WHERE URNO = " . trim($FLXURNO) . "\'";
                    $Timeout = $NoTimeout;

                    //Send that stuff
                    CallCput($Dbg_level, $Dest, $Prio, $Command, $Table, $Fields, $Data, $Selection, $Timeout, $currenttime);
                } else {
                    $Dbg_level = 101;
                    $Dest = "\'1200\'";
                    $Prio = "\'3\'";
                    $Command = "\'IBT\'";
                    $Table = "\'FXTTAB\'";
                    $Fields = "\'FLDU,TEXT,SORT\'";
                    $Data = "\'" . $UPDATE_FLDURNOFXT . "," . $Text2 . ",2\'";
                    $Selection = "\'\'";
                    $Timeout = $NoTimeout;

                    //Send that stuff
                    CallCput($Dbg_level, $Dest, $Prio, $Command, $Table, $Fields, $Data, $Selection, $Timeout, $currenttime);
                }// If End
                if ($rs)
                    $rs->Close();
                $timer->addmarker("4th Cput (FXTTAB)");
            }
        }
    }


    //		COUNTER CLOSED -> OPENING IT
    //if ($Session["STATUS"]==0) {
    if ($CallButton != "LogoTransmit") {


        $currenttime = getCedaTime();
        if ($Session["Type"] == "GATE") {

            // Set gate flag ??
            $Dbg_level = 101;
            $Dest = "\'1200\'";
            $Prio = "\'3\'";
            $Command = "\'UFR\'";
            $Table = "\'AFTTAB\'";
            if ($Session["STATUS"] == 0 && $CREC == $RemarkGateBoardingEx) {
                /* UFIS-1606
                 * Change field from BOAE to BOAO */
                $Fields = "\'REMP,GD" . $Session["GateNumber"] . "X,GD" . $Session["GateNumber"] . "Y,BOAO,LSTU,CHGI,USEU\'";
                //$Fields="\'GD2X,LSTU,USEU\'";
                //Get The Current Date and Add the time form the user
                //Should we make a check for the time value or not ?
                $cl_utc = "";

                $tdi = FindTDI();
                $GateBoardingExValue=str_replace (":", "",$GateBoardingExValue);
                $cl = date("Ymd", time()) . $GateBoardingExValue . "00";
                $cl_utc = date("YmdHi", ChopString("n", -$tdi, $cl));
                $CHGI = date("H:i", ChopString("n", 0, $cl_utc));
                $logger->debug('GateBoardingExValue ' . $cl_utc);

                $Data = "\'" . $CREC . "," . $currenttime . ",," . $cl_utc . "," . $currenttime . ",GD" . $Session["GateNumber"] . "X=" . $CHGI . ",CUTE-IF\'";
            } else if ($Session["STATUS"] == 0 && $CREC == $RemarkGateBoarding) {
                $Fields = "\'REMP,GD" . $Session["GateNumber"] . "X,GD" . $Session["GateNumber"] . "Y,LSTU,CHGI,USEU\'";
                //$Fields="\'GD2X,LSTU,USEU\'";
                $CHGI = date("H:i", ChopString("n", 0, $currenttime));
                $Data = "\'" . $CREC . "," . $currenttime . ",," . $currenttime . ",GD" . $Session["GateNumber"] . "X=" . $CHGI . ",CUTE-IF\'";
            } elseif ($CREC == "Clear") {
                $Fields = "\'REMP,GD" . $Session["GateNumber"] . "X,GD" . $Session["GateNumber"] . "Y,BOAE,LSTU,CHGI,USEU\'";
                //$Fields="\'GD2X,LSTU,USEU\'";
                $CHGI = date("H:i", ChopString("n", 0, $currenttime));
                $Data = "\',,,,,ClearGate=" . $CHGI . ",CUTE-IF\'";
            } else {
                if ($CREC == $RemarkGateOpen) {
                    $Fields = "\'REMP,GD" . $Session["GateNumber"] . "X,GD" . $Session["GateNumber"] . "Y,LSTU,USEU\'";
                    //$Fields="\'GD2X,LSTU,USEU\'";
                    $Data = "\'" . $CREC . ",,," . $currenttime . ",CUTE-IF\'";
                } else {


                    if ($CREC != " " || $PRemarkCode == "None" || $CREC == $RemarkGateFinalCall) {
                        if ($PRemarkCode == "None") {
                            //Upon pressing "Transmit" key in gate part of
                            //Cute_IF only $PRemarksRemTYPGate type remark can be inserted or removed.
                            $rArray = PRemarks();
                            $pos = strpos($PRemarksRemTYPGate, "'" . $rArray[3][0] . "'");
                            if ($pos === false) {
                                // string was not found so we cannot update
                                $logger->debug('A try to update wrong Remark Type was found ' . $rArray[3][0]);
                                $callcput = false;
                            } else {

                                $Fields = "\'REMP,LSTU,USEU\'";
                                //$Fields="\'GD2X,LSTU,USEU\'";
                                $Data = "\'" . $CREC . "," . $currenttime . ",CUTE-IF\'";
                            }
                        } else {
                            if ($CREC == $RemarkGateBoardingEx) {
                                $Fields = "\'REMP,BOAE,LSTU,CHGI,USEU\'";
                                //$Fields="\'GD2X,LSTU,USEU\'";
                                //Get The Current Date and Add the time form the user
                                //Should we make a check for the time value or not ?
                                $tdi = FindTDI();
                                $GateBoardingExValue=str_replace (":", "",$GateBoardingExValue);
                                $cl = date("Ymd", time()) . $GateBoardingExValue . "00";
                                $logger->debug('GateBoardingExValue ' . $cl);
                                $cl_utc = date("YmdHi", ChopString("n", -$tdi, $cl));
                                $CHGI = date("H:i", ChopString("n", 0, $cl_utc));
                                $logger->debug('GateBoardingExValue ' . $cl_utc);

                                $Data = "\'" . $CREC . "," . $cl_utc . "," . $currenttime . ",GD" . $Session["GateNumber"] . "BOAE=" . $CHGI . ",CUTE-IF\'";
                            } else {
                                $Fields = "\'REMP,LSTU,USEU\'";
                                //$Fields="\'GD2X,LSTU,USEU\'";
                                $Data = "\'" . $CREC . "," . $currenttime . ",CUTE-IF\'";
                            }
                        }
                    }
                }
            }
            $Selection = "\'WHERE URNO = " . trim($FlightURNO) . "\'";
            $Timeout = $NoTimeout;

            //Send that stuff
            if ($callcput == true) {
                CallCput($Dbg_level, $Dest, $Prio, $Command, $Table, $Fields, $Data, $Selection, $Timeout, $currenttime);
            }
        } else {

            $Dbg_level = 101;
            $Dest = "\'1200\'";
            $Prio = "\'3\'";
            $Command = "\'URT\'";
            $Table = "\'CCATAB\'";
            if ($Session["STATUS"] == 0 && ($CallButton != "Clear")) {
                if ($CREC != " " || $PRemarkCode == "None") {
                    $Fields = "\'DISP,CKBA,CKEA,LSTU,USEU\'";
                    //$Data="\'".$currenttime . ",".$CREC. "," . $currenttime.",CUTE-IF\'";
                    $Data = "\'" . $CREC . "," . $currenttime . ", ," . $currenttime . ",CUTE-IF\'";
                } else {
                    $Fields = "\'CKBA,CKEA,LSTU,USEU\'";
                    //$Data="\'".$currenttime . ",".$CREC. "," . $currenttime.",CUTE-IF\'";
                    $Data = "\'" . $currenttime . ", ," . $currenttime . ",CUTE-IF\'";
                }
            } else if ($CallButton == "Clear") {

                $Fields = "\'CKEA,LSTU,USEU\'";
                //$Data="\'".$currenttime . ",".$CREC. "," . $currenttime.",CUTE-IF\'";
                $Data = "\'," . $currenttime . ",CUTE-IF\'";
                //$Fields="\'GD2X,LSTU,USEU\'";
            } elseif ($CallButton == 'Transmit' || $CallButton == 'Open') {
                if ($CREC != " " || $PRemarkCode == "None") {
                    $Fields = "\'DISP,LSTU,USEU\'";
                    $Data = "\'" . $CREC . "," . $currenttime . ",CUTE-IF\'";
                } else {
                    $Fields = "\'LSTU,USEU\'";
                    $Data = "\'" . $currenttime . ",CUTE-IF\'";
                }
            }

            $Selection = "\'WHERE URNO = " . trim($CCAURNO) . "\'";
            $Timeout = $NoTimeout;
            //Send that stuff
            CallCput($Dbg_level, $Dest, $Prio, $Command, $Table, $Fields, $Data, $Selection, $Timeout, $currenttime);
        }
    }

    $GLOBALS["Session"] = $Session;
    $timer->addmarker("6th Cput (AFFTAB, CCATAB)");
    $timer->stop();
    //$timer->debug();
    //$timer->showtime();



    return 0;
}

function CloseCounter($FlightURNO) {

    //Global $db, $Session, $FlightURNO;
    //Global $RemarkGateClose;
    //Global $FLDUse;

    $db = $GLOBALS['db'];
    $Session = $GLOBALS['Session'];
    //$FlightURNO = $GLOBALS['FlightURNO'];
    $RemarkGateClose = $GLOBALS['RemarkGateClose'];
    $FLDUse = $GLOBALS['FLDUse'];
    $NoTimeout = $GLOBALS['NoTimeout'];

    $logger = & LoggerManager::getLogger('CuteIf');


    ///////////////////////////////////////////////////////////
    //
    // FIRST UPDATE FLDTAB: SET DSEQ FOR ACTUAL ENTRIES = -1
    //
    ///////////////////////////////////////////////////////////
    if ($Session["Type"] == "GATE") {
        /*
          $tmp="SELECT URNO FROM FLDTAB WHERE RNAM='" . $Session["Number"] ."' AND RTYP = 'GTD1' AND DSEQ > 0 AND AURN=" . $FlightURNO;
          $cmdTempCommandText = $tmp;
          $rs=$db->Execute($cmdTempCommandText);
          if (!$rs->EOF)  {
          $FLDURNO=trim($rs->fields("URNO"));
          }
          if ($rs) $rs->Close();
         */
    } else {
        $CCAURNO = "";
        //$tmp="SELECT URNO FROM CCATAB WHERE CKIC='".$Session["Number"]."' and (FLNU='" . $FlightURNO . "' or URNO='" . $FlightURNO . "') and ((ckba=' ') or ((ckba< TO_CHAR(SYSDATE+30/1440,'YYYYMMDDHH24MISS') and ckba<>' ') and (ckea=' ' or ckea> TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS'))))";
        $tmp = "SELECT URNO FROM CCATAB WHERE CKIC='" . $Session["Number"] . "' and (FLNU='" . $FlightURNO . "' or URNO='" . $FlightURNO . "') ";
        $tmp = $tmp . " and ((ckba=' ') or ((ckba< " . CedaSYSDATE('n', 30) . " and ckba<>' ') ))";
        $cmdTempCommandText = $tmp;
        $rs = $db->Execute($cmdTempCommandText);
        if (!$rs->EOF) {
            $CCAURNO = trim($rs->fields("URNO"));
        } else {
            // EXIT From the Function
            $logger->debug('Error :Exit From Close Function for Checkin ' . $Session["Number"]);
            //return;
        }// If End
        if ($rs)
            $rs->Close();
        /*
          if ($CCAURNO=="" && $Session["CommonF"]==0) {
          //The Counter was not Closed corectly
          $tmp="SELECT URNO FROM FLDTAB WHERE RNAM='" . $Session["Number"] ."' AND RTYP = 'CKIF' AND DSEQ > 0 AND AURN=" . $FlightURNO;
          } else {
          $tmp="SELECT URNO FROM FLDTAB WHERE RNAM='" . $Session["Number"] ."' AND RTYP = 'CKIF' AND DSEQ > 0 AND RURN=" . $CCAURNO;
          }
          $cmdTempCommandText = $tmp;
          $rs=$db->Execute($cmdTempCommandText);

          if (!$rs->EOF)  {
          $FLDURNO=trim($rs->fields("URNO"));
          //echo $FLDURNO;
          } else {
          // EXIT From the Function
          $logger->debug('Error :Exit From Close Function for Checkin '.$Session["Number"]);
          return;
          }// If End
          if ($rs) $rs->Close();
         */
    }
    //	echo $tmp;
    //	exit;
    /*
      $Dbg_level=101;
      $Dest="\'1200\'";
      $Prio="\'3\'";
      $Command="\'URT\'";
      $Table="\'FLDTAB\'";
      $Fields="\'LSTU,DSEQ,ACTI,USEU\'";

      $currenttime=getCedaTime();

      if ($Session["Type"]=="GATE") {
      $Data="\'".$currenttime. ",-1," . $currenttime . ",CUTE-IF\'";
      } else {
      $Data="\'".$currenttime . ",-1, ,CUTE-IF\'";
      }

      $Selection="\'WHERE URNO = ". trim($FLDURNO) ."\'";
      $Timeout="\'-1\'";

      //Send that stuff
      if ((trim($FLDURNO)) != "" ) {
      $result=CallCput($Dbg_level,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$currenttime);
      }
     */

    $Dbg_level = 101;
    $Dest = "\'1200\'";
    $Prio = "\'3\'";

    $currenttime = getCedaTime();
    if ($Session["Type"] == "GATE") {
        $Command = "\'UFR\'";
        $Table = "\'AFTTAB\'";

        //$Fields="\'GD1Y or GD2Y,LSTU,USEU\'";
        $Fields = "\'GD" . $Session["GateNumber"] . "Y,REMP,LSTU,CHGI,USEU\'";
        $CHGI = date("H:i", ChopString("n", 0, $currenttime));
        $Data = "\'" . $currenttime . "," . $RemarkGateClose . "," . $currenttime . ",GD" . $Session["GateNumber"] . "Y=" . $CHGI . ",CUTE-IF\'";

        $Selection = "\'WHERE URNO = " . trim($FlightURNO) . "\'";
    } else {
        // UPDATE CCATAB CKEA,LSTU,USEU
        $Command = "\'URT\'";
        $Table = "\'CCATAB\'";
        $Fields = "\'CKEA,LSTU,USEU\'";

        $Data = "\'" . $currenttime . "," . $currenttime . ",CUTE-IF\'";
        $Selection = "\'WHERE URNO = " . trim($CCAURNO) . "\'";
    }


    $Timeout = $NoTimeout;

    //Send that stuff
    if ($Session["Type"] == "GATE") {
        $result = CallCput($Dbg_level, $Dest, $Prio, $Command, $Table, $Fields, $Data, $Selection, $Timeout, $currenttime);
    } else {
        if (($CCAURNO) != "") {
            $result = CallCput($Dbg_level, $Dest, $Prio, $Command, $Table, $Fields, $Data, $Selection, $Timeout, $currenttime);
        }
    }

    if ($FLDUse == true) {
        $RField = "FLDU";
    } else {
        $RField = "RURN";
    }

    /*
      // Delete From FXTTAB
      if (isset($Session["UPDATE_FLDURNO"])) {
      // DELETE FXTTAB WHERE FLDU = SESSION FLDU
      $Command="\'DRT\'";
      $Table="\'FXTTAB\'";
      $Fields="\'\'";
      $Data="\''";
      $Selection="\'WHERE FLDU = ". $Session["UPDATE_FLDURNO"] ."\'";
      $result=CallCput($Dbg_level,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$currenttime);
      }

      // Delete From FLZTAB
      if (isset($Session["UPDATE_FLDURNO"])) {
      // DELETE FXTTAB WHERE FLDU = SESSION FLDU
      $Command="\'DRT\'";
      $Table="\'FLZTAB\'";
      $Fields="\'\'";
      $Data="\''";
      $Selection="\'WHERE ".$RField." = ". $Session["UPDATE_FLDURNO"] ."\'";
      $result=CallCput($Dbg_level,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$currenttime);
      }
     */

    //Infrom local Session
    $Session["STATUS"] = 0;
    $GLOBALS['Session'] = $Session;

    return 0;
}
?>