<html>
    <head>
    <title>{$PageTitle}</title>
    <META NAME="description" CONTENT="">
    <META NAME="author" CONTENT="UFIS AS">
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset={$CharSet}">
    <META HTTP-EQUIV="Expires" CONTENT="{$ContenetExpires}">
    <META HTTP-EQUIV="Refresh" CONTENT="{$RefreshRate}">
    <link rel="STYLESHEET" type="text/css" href="style/style.css">
    <!-- In head section we should include the style sheet for the grid -->
    <link rel="stylesheet" type="text/css" media="screen" href="include/jquery/themes/basic/grid.css" />
    <!-- Of course we should load the jquery library -->
    <script src="include/jquery/js/jquery.js" type="text/javascript"></script>
    <!-- Load Masked Input Plugin -->
    <script src="include/jquery/js/jquery.maskedinput.js" type="text/javascript"></script>
    <!-- and at end the jqGrid Java Script file -->
    <script src="include/jquery/js/jquery.jqGrid.js" type="text/javascript"></script>
    <script type="text/javascript">
            var GateRemarks = new Array();
            GateRemarks[0] = "{$RemarkGateOpen}";
            GateRemarks[1] = "{$RemarkGateBoardingEx}";
            GateRemarks[2] = "{$RemarkGateBoarding}";
            GateRemarks[3] = "{$RemarkGateFinalCall}";
            GateRemarks[4] = "{$RemarkGateClose}";
            
    </script>
    <!-- and at end the Flight Grid Java Script file -->
    <script src="include/js/flightGrid.js" type="text/javascript"></script>
    <script src="include/js/workflow.js" type="text/javascript"></script>
    </head>
    <body style="font-family:Arial" bgcolor="#ffffff">
        <div id="#container">
            <div id="top" >
                <table border="0" width="100%" cellpadding="0" cellspacing="0" >
                    <tr>
                        <td width="117"><img src="pics/UFIS-Logo300dpii.jpg" alt="UFIS AS GmbH " width="117" hspace="34" border="0" title="UFIS AS GmbH "></td>
                        <td width="100%" class="bluebg" align="center">
                            <table>
                                <tr>
                                    <td><font class="StatusTitle">{$Type}&nbsp;{$TypeNumber}&nbsp;</font></td>
                                    <td><font class="StatusTitle"><div id ="Status">{$Status}</div></font></td><td><font class="StatusTitle">-</font></td>
                                </tr>
                            </table>
                        </td>
                        <td width="250"  class="bluebg"><div id="idtime" class="Clock">{$CurrentTime}</div>
                        </td>
                    </tr>
                </table>
            </div>
            <table width="100%">
            <tr>
            <td>
            <div id="left"  class="left" >
                <div id ="LeftLogoSelection" style="{$LogoSelectionStyle}">
                    {if $LogoSelection eq true}
                    {include file="logo.tpl"}
                    {/if}
                </div>
            </div>
            </td>
            <td>
            <div id="right"  >
                <div id ="MainSelection">
                    <!-- the grid definition in html is a table tag with class 'scroll' -->
                        <table border=0>
                            <tr>
                                <td>
                                    <table id="FlightData" class="scroll" cellpadding="0" cellspacing="0"></table>
                                    <div id="pager" class="scroll" style="text-align:center;"></div>
                                    {if $DisplayUnclosedMessage eq true}
                                    <div id="UnclosedMessage" class="PreviousOpened">
                                    <table border="1" width="100%">
                                        <tr>
                                        <td>
                                        {include file="UnclosedMessage.tpl"}
                                        </td></tr>
                                        </table>
                                    </div>
                                    {/if}                                    
                                    <!--<button onclick="gridReload()" id="submitButton" style="margin-left:30px;">Reload</button>-->
                                </td>
                            </tr>
                        </table>
                        <div id ="PRemarks" style="{$PredefinedSelectionStyle}">
                        <table>
                            <tr>
                                <td valign="top">
                                    {if $PredefinedSelection eq true}
                                    {include file="predefine.tpl"}
                                    {/if}
                                </td>
                                <td valign="top">
                                    {if $FreeTextSelection eq true}
                                    {include file="freetext.tpl"}
                                    {/if}
                                </td>
                            </tr>
                        </table>
                        </div>
                        {if $Type eq 'GATE'}
                            {include file="gateButtons.tpl"}
                        {else}
                        <table border=0 width="100%">
                            <tr>
                                <td align="right">
                                    <input type="button" id="Checkin_BtnOpen" name="Button" value="{$bValueOpen}" class="{$btnOpenhide}" onclick="cput(this.value,'{$Type}')">
                                    <input type="button" id="Checkin_BtnTransmit" name="Button" value="{$bValueTransmit}" class="{$btnTransmithide}" onclick="cput(this.value,'{$Type}')">
                                    <input type="button" id="Checkin_BtnClose" name="Button" value="{$bValue}" class="{$btnhide}" onclick="cput(this.value,'{$Type}')"  >
                                </td>
                            </tr>
                        </table>
                        {/if}
                       <div id="bottom">
                        {include file="bottom.tpl"}
                       </div>
                </div>
            </div>
            </td>
            </tr>
            </table>
        </div>
        {$pqeryEndJS}
    </body>
</html>