<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>:::ERROR:::</title>
</head>
<body>
<h1 style="color : red;">System in trouble</h1>
<p>
While responding to your request an error encountered in the application.
Administrator has been notified.<br />

Sorry for the inconvenience, we correct the error as soon as possible.
</p>
<hr>
<address>Last modified: <?php echo strftime("%x %X (%z)", getlastmod()); ?></address>
</body>
</html>
