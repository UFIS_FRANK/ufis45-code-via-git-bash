<?php
include("include/manager.php");
include("include/maininclude.php");

$logger = LoggerManager::getRootLogger('CuteIf');



$smarty = new Smarty;

$smarty->caching=0;
$smarty->compile_check =true;
$smarty->debugging = false;

//Header
$smarty->assign("PageTitle",$PageTitle);
$smarty->assign("CharSet",$CharSet);
$smarty->assign("ContenetExpires",$Cexpires);
$smarty->assign("Type",$Session["Type"]);

$smarty->assign("ErrorMsg",errorMsg());

$smarty->display('error.tpl');

if ($Session["userid"]) {
		session_unregister("Session");
		session_destroy();
}

//Safely close all appenders with...

LoggerManager::shutdown();
?>


