<?
/*
Copyright ��� 2004 by INFOMAP S.A.
This computer program is protected by copyright law
of Greece and International treaties. Unauthorized
reproduction of this program, or any portion of it,
may result in severe civil and criminal penalties, and
will be prosecuted to the maximum extent possible under
the law.
*/
class LoginInit {
	// Public Variables
	//	$db=&GLOBALS["db"];

	function Login_Mysql($UserName,$Password) {
		Global $dbMysql,$Session,$ClientIP,$NoIp;
		$logedIn=0;
		
		
	  	$cmdTempCommandText = "SELECT  * from user where dtName='".strtolower($UserName)."' and dtPwd='".strtolower($Password)."'";
		$rs=$dbMysql->Execute($cmdTempCommandText);

		if ($rs )  {
				
				$Session["userid"]=$rs->fields("pkuserid");
				$Session["username"]=strtolower($rs->fields("dtName"));		
				$UserIP=$rs->fields("dtIP");
				//Check the type (Gate or Checkin) and parse the dtName
				
				if (strstr($Session["username"],"gate")) {
					// It is a Gate
					$Session["Type"]="GATE";
					$Session["Number"]=	strtoupper(str_replace('gate', '', $Session["username"]));
				} else {
					// It is a ChekIn
					$Session["Type"]="CHECK-IN";
					$Session["Number"]=strtoupper(str_replace( 'checkin','', $Session["username"]));
					
				}
				
				if ($NoIp!="true") {
					if ($UserIP!=$ClientIP){
						header ("Location: error.php?Error=2"); 
						exit ;
					}
				}
				
				//We Must check About The Blocking				
				if (CheckBlock()==1) {
					header ("Location: error.php?Error=3"); 
					exit ;
				}
				
				// We Must for  allocation
				if (CheckAlloc()==0) {
					header ("Location: error.php?Error=4"); 
					exit ;
				}
				
				
				
				$logedIn=1;

		  } else {
				header ("Location: index.php?Error=1"); 
				exit ;
				
		  }// If End
    	if ($rs) $rs->Close();

		return $logedIn;
	}// End Function 


	function Login_Oracle($UserName,$Password) {
		Global $dbMysql,$Session,$ClientIP,$NoIp;
		$logedIn=0;
		
		
	  	$cmdTempCommandText = "SELECT  * from user where dtName='".strtolower($UserName)."' and dtPwd='".strtolower($Password)."'";
		$rs=$db->Execute($cmdTempCommandText);

		if ($rs )  {
				
				$Session["userid"]=$rs->fields("pkuserid");
				$Session["username"]=strtolower($rs->fields("dtName"));		
				$UserIP=$rs->fields("dtIP");
				//Check the type (Gate or Checkin) and parse the dtName
				
				if (strstr($Session["username"],"gate")) {
					// It is a Gate
					$Session["Type"]="GATE";
					$Session["Number"]=	strtoupper(str_replace('gate', '', $Session["username"]));
				} else {
					// It is a ChekIn
					$Session["Type"]="CHECK-IN";
					$Session["Number"]=strtoupper(str_replace( 'checkin','', $Session["username"]));
					
				}
				
				if ($NoIp!="true") {
					if ($UserIP!=$ClientIP){
						header ("Location: error.php?Error=2"); 
						exit ;
					}
				}
				
				//We Must check About The Blocking
				if (CheckBlock()==1) {
					header ("Location: error.php?Error=3"); 
					exit ;
				}
				
				// We Must for  allocation
				if (CheckAlloc()==0) {
					header ("Location: error.php?Error=4"); 
					exit ;
				}
				
				
				
				$logedIn=1;

		  } else {
				header ("Location: index.php?Error=1"); 
				exit ;
				
		  }// If End
    	if ($rs) $rs->Close();

		return $logedIn;
	}// End Function 
	
	function Login_File($UserName,$Password) {
		Global $Session,$ClientIP,$NoIp,$UserFilename,$FLZTABuse,$logoSelection,$CheckinPredifendSelection;
		$logedIn=0;
		$auth=false;

		// Read the entire file into the variable $file_contents 

	    //$filename = '/path/to/file.txt'; 
    	$fp = fopen( $UserFilename, 'r' ); 
	    $file_contents = fread( $fp, filesize( $UserFilename ) ); 
	    fclose( $fp ); 

    	// Place the individual lines from the file contents into an array. 

	    $lines = explode ( "\n", $file_contents ); 

    	// Split each of the lines into a username and a password pair 
	    // and attempt to match them to $PHP_AUTH_USER and $PHP_AUTH_PW. 

    	foreach ( $lines as $line ) { 

        	list( $userid,$username, $password,$UserIP,$UserlogoSelection,$UserCheckinPredifendSelection ) = explode( ':', $line ); 

	        if ( ( strtolower($username) == strtolower($UserName) ) && 
    	         ( strtolower($password) == strtolower($Password) ) &&
                    strlen($UserName) > 0             ) {
                 

        	    // A match is found, meaning the user is authenticated. 
            	// Stop the search. 

	            $auth = true; 
    	        break; 

        	} 
	    } 

		if ($auth )  {
				
				$Session["userid"]=$userid;
				$Session["username"]=strtoupper($username);		
				$UserIP=$UserIP;

				//Check the type (Gate or Checkin) and parse the dtName
				
				if ($NoIp!="true") {
					if ($UserIP!=$ClientIP){
						header ("Location: error.php?Error=2"); 
						exit ;
					}
				}
				
				if (strstr($Session["username"],"GATE"))  {
					// It is a Gate
					$Session["Type"]="GATE";
					$Session["Number"]=	strtoupper(str_replace('gate', '', $Session["username"]));
					$Session["Number"]=	strtoupper(str_replace('GATE', '', $Session["username"]));
				} else {
					// It is a ChekIn
					$Session["Type"]="CHECK-IN";
					$Session["Number"]=strtoupper(str_replace( 'checkin','', $Session["username"]));
					$Session["Number"]=strtoupper(str_replace( 'CHECKIN','', $Session["username"]));
					
				}
				
				$Session["logoSelection"] = $logoSelection;
				$Session["CheckinPredifendSelection"] = $CheckinPredifendSelection;

				$Session["logoSelectionUser"] = true;
				$Session["CheckinPredifendSelectionUser"] = true;
								
				
				
				if ($logoSelection==true) {
					if (strlen(trim($UserlogoSelection))>0) {
						//$Session["logoSelection"] = strtoboolean(strtoupper($UserlogoSelection));
						$Session["logoSelectionUser"] = strtoboolean(strtoupper(trim($UserlogoSelection)));
				
					
					}
				}
				if ($CheckinPredifendSelection==true) {
					if (strlen(trim($UserCheckinPredifendSelection))>0) {
						//$Session["CheckinPredifendSelection"] = strtoboolean(strtoupper($UserCheckinPredifendSelection));
						$Session["CheckinPredifendSelectionUser"] = strtoboolean(strtoupper(trim($UserCheckinPredifendSelection)));
						//echo strtoupper($UserCheckinPredifendSelection) ."aaaaaaaaaa<br>";
					}
				}

				
				//$logoSelection,$CheckinPredifendSelection
				
				//We Must check About The Blocking
				if (CheckBlock()==1) {
					header ("Location: error.php?Error=3");
					exit ;
				}
				//exit;
				//$StatusFlag=Status();


				// We Must check for  allocation
				if (CheckAlloc()==0) {
					header ("Location: error.php?Error=4"); 
					exit ;
				}
				//exit;
				// We must Check the Status Of the GateCheckin
				
				//Find Current Logo
				//$tmplogo = Currentlogos($FLZTABuse);
				
				//if (strlen(trim($tmplogo))>1) {
				//	$Session["LogoName"] = $tmplogo;
				//}
				
				
				//if ($StatusFlag=="OPEN" &&) {
				//	header ("Location: error.php?Error=6&oldUrno=".$Session["Oldurno"]."&urno=".$Session["urno"]); 
				//	exit ;
				//}
				
				$logedIn=1;

		  } else {
				header ("Location: index.php?Error=1"); 
				exit ;
				
		  }// If End

		return $logedIn;
	}// End Function 
	
		function Login_Variable($UserName,$Password) {
		Global $Session,$ClientIP,$P01,$P02,$NoIp,$UserFilename,$Loginhopo;
		$logedIn=0;
		$auth=false;
		
		//Inactive Counter
		if ($P02==1){
			header ("Location: error.php?Error=2&P01=".$P01."&P02=".$P02); 
			exit ;
		}		
		//Change LoginMethod 20/09/2006 
		//In order to fullfill the SITA DHCP request

		//New Global Variable P01 - ComputerName
		//Format  ATHCKB001, ATHGBP21A
		//B28:GateB28:test:57.30.224.68 ATH2GBPB28
		//B28A:GateB28A:test:57.30.224.83 ATH2GTCB28 
		
		//		CKB checkin
		//		JWS (finger gates)
		//		GTC (ta diplana apo ola) 
		//		GBP (bus gate)
		
		
		$MyP01 = "";
		$MyType = "";
		$MyP01 = strtoupper(str_replace($Loginhopo, '', $P01));
		
		if (strstr($MyP01,"JWS")) {
			// It is a Gate (finger gates)
			$MyType = "GATE";
			$MyP01=	strtoupper(str_replace('JWS', '', $MyP01));
		} elseif (strstr($MyP01,"GTC")) {
			// It is a Gate (A)(bus gate besind)
			$MyType = "GATE";
			$MyP01=strtoupper(str_replace( 'GTC','', $MyP01));
			$MyP01=$MyP01."A";
		} elseif (strstr($MyP01,"GBP")) {
			// It is a Gate (bus gate)	
			$MyType = "GATE";
			$MyP01=strtoupper(str_replace( 'GBP','',$MyP01));
		} elseif (strstr($MyP01,"CKB")) {
			// It is a ChekIn
			$MyType = "CHECKIN";
			$MyP01=strtoupper(str_replace( 'CKB','', $MyP01));
		}
//		echo $logedIn."aa";

				
		// Read the entire file into the variable $file_contents 

	    //$filename = '/path/to/file.txt'; 
    	$fp = fopen( $UserFilename, 'r' ); 
	    $file_contents = fread( $fp, filesize( $UserFilename ) ); 
	    fclose( $fp ); 

    	// Place the individual lines from the file contents into an array. 

	    $lines = explode ( "\n", $file_contents ); 

    	// Split each of the lines into a username and a password pair 
	    // and attempt to match them to $PHP_AUTH_USER and $PHP_AUTH_PW. 

    	foreach ( $lines as $line ) { 

        	list( $userid,$username, $password,$UserIP ) = explode( ':', $line ); 

	        if ( ( strtolower($username) == strtolower($UserName) ) && 
    	         ( strtolower($password) == strtolower($Password) ) && strlen($UserName) > 0) {

        	    // A match is found, meaning the user is authenticated. 
            	// Stop the search. 

	            $auth = true; 
    	        break; 

        	} 
	    } 
//		echo $UserName . "--" .$Password ;
//exit;
		if ($auth )  {
				
				$Session["userid"]=$userid;
				$Session["username"]=strtoupper($username);		
				$UserIP=$UserIP;

				//Check the type (Gate or Checkin) and parse the dtName
				//Check UserName with The Parameter	
				
				$MyP01 = $MyType . $MyP01;
				
				if ($MyP01!=strtoupper($username)){
					if ($NoIp!="true") {
						if ($UserIP!=$ClientIP){
						header ("Location: error.php?Error=2&P01=".$P01."&P02=".$P02); 
						exit ;
						}
					}

//						header ("Location: error.php?Error=2&P01=".$P01); \
//						exit ;
				} 				
				
				if (strstr($Session["username"],"GATE"))  {
					// It is a Gate
					$Session["Type"]="GATE";
					$Session["Number"]=	strtoupper(str_replace('gate', '', $Session["username"]));
					$Session["Number"]=	strtoupper(str_replace('GATE', '', $Session["username"]));
				} else {
					// It is a ChekIn
					$Session["Type"]="CHECK-IN";
					$Session["Number"]=strtoupper(str_replace( 'checkin','', $Session["username"]));
					$Session["Number"]=strtoupper(str_replace( 'CHECKIN','', $Session["username"]));
					
				}

				$Session["P01"] = $P01;
				$Session["P02"] = $P02;
				
				//We Must check About The Blocking
				if (CheckBlock()==1) {
					header ("Location: error.php?Error=3&P01=".$P01."&P02=".$P02); 
					exit ;
				}

				//$StatusFlag=Status();


				// We Must check for  allocation
				if (CheckAlloc()==0) {
					header ("Location: error.php?Error=4&P01=".$P01."&P02=".$P02); 
					exit ;
				}
				
				// We must Check the Status Of the GateCheckin
				
				//if ($StatusFlag=="OPEN" &&) {
				//	header ("Location: error.php?Error=6&oldUrno=".$Session["Oldurno"]."&urno=".$Session["urno"]); 
				//	exit ;
				//}
				$logedIn=1;

		  } else {
				header ("Location: index.php?Error=1&P01=".$P01."&P02=".$P02); 
				exit ;
				
		  }// If End
		  
		  
		return $logedIn;
	}// End Function 

	function logosAirDB() {
		Global $db,$AirlineURNO,$hopo,$Session;
		if ($Session["Type"]=="GATE") {
			$tmp="G";		
		} else {
			$tmp="C";
		}
	  	$cmdTempCommandText = "Select decode (trim(alc2),'' ,alc3,alc2) alc2 ,alc2 as alc,alc3,alfn  from alttab order by alfn";
		
		$rs=$db->Execute($cmdTempCommandText);
		if (!$rs->EOF)  {
		    if ($Session["PicName"]=="None") {$tmp=" selected";}
		
			echo '<option value="None" '.$tmp.'>None';
			while (!$rs->EOF) {
			  $tmp="";
			  if ($Session["PicName"]==trim($rs->fields("alc2"))) {$tmp=" selected";}
			  
			  echo '<option value="'.trim($rs->fields("alc2")).'" '.$tmp.'>'.trim(substr($rs->fields("alfn"), 0, 41)) ."|".trim($rs->fields("alc2"))."\n";
			$rs->MoveNext();
			}
		} else {
			echo "<option >No Airlines Yet";
		}// If End
   	if ($rs) $rs->Close();
	}// End Function 
	
	function logosDB() {
		Global $db,$AirlineURNO,$hopo,$Session;
		if ($Session["Type"]=="GATE") {
			$tmp="G";		
		} else {
			$tmp="C";
		}
	  	$cmdTempCommandText = "Select distinct beme,code,remi,urno from fidtab where remt='".$tmp."' and beme<>' ' and hopo='$hopo' order by beme";
		
		$rs=$db->Execute($cmdTempCommandText);
		if (!$rs->EOF)  {
		    if ($Session["PicName"]=="None") {$tmp=" selected";}
		
			echo '<option value="None" '.$tmp.'>None';
			while (!$rs->EOF) {
			  $tmp="";
			  if ($Session["PicName"]==trim($rs->fields("code"))) {$tmp=" selected";}
			  
			  echo '<option value="'.trim($rs->fields("code")).'" '.$tmp.'>'.trim(substr($rs->fields("beme"), 0, 41)) ."|".trim($rs->fields("code"))."\n";
			$rs->MoveNext();
			}
		} else {
			echo "<option >No Airlines Yet";
		}// If End
   	if ($rs) $rs->Close();
	}// End Function 

	function logosDBv1() {
		Global $db,$AirlineURNO;
  		$cmdTempCommandText = "Select lgfn,urno,lgsn from flgtab order by upper(lgfn)";
		
		$rs=$db->Execute($cmdTempCommandText);
		if (!$rs->EOF)  {
			while (!$rs->EOF) {
			  $tmp="";
			  if ($AirlineURNO==trim($rs->fields("urno"))) {$tmp=" selected";}
			  echo '<option value="'.$rs->fields("urno").'|'.trim($rs->fields("lgsn")).'" '.$tmp.'>'.trim(substr($rs->fields("lgfn"), 0, 41)) ."\n";
			$rs->MoveNext();
			}
		} else {
			echo "<option >No Airlines Yet";
		}// If End
   	if ($rs) $rs->Close();
	}// End Function 

	function logosFile() {
		Global $logoRealPath,$logoExt,$Session;
		// Define the full path to your folder from root
	    $file_count = 0;
	    $path = $logoRealPath;
	    $file_types = $logoExt;
	    //echo $logoPath.'--'.$path;
	
    	// Open the folder
	    $dir_handle = opendir($path) or die("Unable to open $path");
		while (false !== ($filename = readdir($dir_handle))) {
        	 $files[] = $filename;
	    }
		// Loop through the files
		sort($files);
			if ($Session["PicName"]=="None") {$tmp=" selected";}
		
		 echo '<option value="None" '.$tmp.'>None';
    	   foreach ($files as $file) {
    	   
			
        	   //$extension = file_type($file);
        	   $new_filename = stristr ($file, $logoExt);
        	   $new_filename_value = str_replace ($logoExt,'',$file);
               //echo $new_filename.'--'.$new_filename_value."<br>";
	           //if($file != '.' && $file != '..' && array_search($file, $file_types) !== false){
        	    if($file != '.' && $file != '..' &&  strlen($new_filename)>0){
    	           $file_count++;
    	           $tmp="";
    	           
        	    if ($Session["PicName"]==$new_filename_value) {$tmp=" selected";}
        	       echo '<option value="'.$new_filename_value.'" '.$tmp.'>'.trim(substr($new_filename_value, 0, 41)) ."\n";
	           }
    	   }
	    // Close
    	closedir($dir_handle);
	}// End Function 
	
	
	
} // End Class


?>
