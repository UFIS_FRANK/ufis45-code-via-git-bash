<?
	class Overlib {
	    var $ol_fcolor     = "#ccccff";
	    var $ol_backcolor  = "#333399";
	    var $ol_textcolor  = "#000000";
	    var $ol_capcolor   = "#ffffff";
	    var $ol_closecolor = "#9999ff";
	    var $ol_width      = 200;
	    var $ol_border     = 1;
	    var $ol_offsetx    = 10;
	    var $ol_offsety    = 50;
	    var $ol_path       = "class.overlib";

	  function init ( ) {
?>
<link rel='stylesheet' href=<? echo "'$this->ol_path/overlib.css' "; ?> type='text/css'>
<div id='overDiv' style='position:absolute; visibility:hide; z-index: 1;'>
</div>
<script language='javascript'>
if (typeof fcolor == 'undefined') { var fcolor = <? echo "\"$this->ol_fcolor\""; ?>;}
if (typeof backcolor == 'undefined') { var backcolor = <? echo "\"$this->ol_backcolor\""; ?>;}
if (typeof textcolor == 'undefined') { var textcolor = <? echo "\"$this->ol_textcolor\""; ?>;}
if (typeof capcolor == 'undefined') { var capcolor = <? echo "\"$this->ol_capcolor\""; ?>;}
if (typeof closecolor == 'undefined') { var closecolor = <? echo "\"$this->ol_closecolor\""; ?>;}
if (typeof width == 'undefined') { var width = <? echo "\"$this->ol_width\""; ?>;}
if (typeof border == 'undefined') { var border = <? echo "\"$this->ol_border\""; ?>;}
if (typeof offsetx == 'undefined') { var offsetx = <? echo $this->ol_offsetx; ?>;}
if (typeof offsety == 'undefined') { var offsety = <? echo $this->ol_offsety; ?>;}
</script><script language='javascript' src=<? echo "'$this->ol_path/overlib.js'"; ?>>
</script>
<?
	  }

	  function over ($title, $text, $autoclose=true, $align=2,$follow=false)
	  {
	    if ($autoclose == true) {
		$func_1 = "d";

	    	if ($follow == true)
			$func_3 = "c";
	    	else
			$func_3 = "s";
	    } else {
		$func_1 = "s";
		$func_3 = "c";
	    }

	    switch ($align) {
		case 0:		$func_2 = "l"; break;
		case 1:		$func_2 = "c"; break;
		default:	$func_2 = "r"; break;
	    } 

	    $jsfunc = $func_1 . $func_2 . $func_3;

	    $output=" onMouseOver=\"$jsfunc('$text','$title'); return true;\" ";
	    $output.=" onMouseOut=\"nd(); return true;\" ";

	    return ($output);
	  }
	}
?>
