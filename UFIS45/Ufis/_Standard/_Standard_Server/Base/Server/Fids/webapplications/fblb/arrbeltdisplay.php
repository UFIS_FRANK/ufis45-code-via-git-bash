<html>
    <head>
        <link rel=stylesheet href=styles/fblb.css type=text/css>
        <?php
        //result returned - send to sqlhdl3 -> -1
        $errPNA='Error in CEDA-communication. Updates will not work properly!';
        //result returned - send to sqlhdl3 -> 7170
        $errINA='CEDA Process not initialised properly. Updates will not work!';
        $spaceBwRows = 20;

        error_reporting (E_ERROR | E_WARNING | E_NOTICE);
        $ErrorPage = 'Error.php';
        function GenErrorHandler($errno, $errstr, $errfile='?', $errline= '?') {
            global $ErrorPage;
            if (($errno & error_reporting()) == 0)
                return;
            $err = '';
            switch($errno) {
                case E_ERROR: $err = 'FATAL';
                    break;
                case E_WARNING: $err = 'ERROR';
                    break;
                case E_NOTICE: return;
            }
//print $errno;
//print $errstr;
//print $errfile;
//print $errline;
            print '<META HTTP-EQUIV="Refresh" CONTENT="0;url='.$ErrorPage.'?msg='.$errstr.'">';

            //$ErrorPage = '/fblb/Error.php?msg='.$errstr.'&rprnt=/fblb/arrbeltdisplay.php';
            //print '<META HTTP-EQUIV="Refresh" CONTENT="0;url='.$ErrorPage.'">';
            die();
        }
        set_error_handler("GenErrorHandler");

        include 'ArrBlt.php';
        $arrblt = new ArrBlt();
        $rcvD = $arrblt->getTimeConf();
        $refTime = $rcvD[20];
        $tp_future = $rcvD[21];
        $tp_lbg = $rcvD[22];
        $tp_fbg = $rcvD[23];
        $tp_lnd = $rcvD[24];
        $tp_exp = $rcvD[25];
        ?>
              <meta http-equiv="Refresh" content="<?php echo $refTime ?>">
        <style>BODY{MARGIN-LEFT:0pt;MARGIN-RIGHT:-1pt;MARGIN-TOP:-1pt}</style>
        <title>Arrival Belt Display</title>

        <script language="JavaScript">
            function pros(v) {
                //alert(v);
                if (v=='S')
                    window.close();
                if (v=='R') {
                    status=true;
                } else {
                    var sst=v.split("#");
                    var status=false;
                    if (sst[1]=='C' && (sst[3]=='LBG' || sst[3]=='FBG')) {
                        status=true;
                    } else if (sst[1]=='F' && sst[3]=='OBL') {
                        status=true;
                    } else if (sst[1]=='L' && (sst[3]=='FBG' || sst[3]=='OBL')) {
                        status=true;
                    }
                }
                if (status) {
<?php if ($enableRefreshButton) { ?>
            document.arrbelt.refresh.disabled=true;
    <?php } ?>
                document.body.style.cursor ='wait';
                if (v != 'R') {
                    document.arrbelt.usrparam.value=v;
                } else {
                    document.arrbelt.usrparam.value='';
                }
                document.arrbelt.action='arrbeltdisplay.php';
                document.arrbelt.method='POST';
                document.arrbelt.submit();
            }
        }
        </script>
        <script language="JavaScript" type="text/javascript" src="js/genfunctions.js"></script>
    </head>

    <body onload="showtime();">
        <?php
        $dev = $arrblt->getDeviceInfo($_SERVER['REMOTE_ADDR']);
        $devty = trim($dev[0][0]);
        $devid=trim($dev[0][1]);
        $shtdwn=trim($dev[0][2]);
        $locDate=date("d.m.Y H:i:s");

        //$res=$arrblt->updateTime($_SERVER['REMOTE_ADDR']);
        //$res=$arrblt->updateTime($_SERVER['REMOTE_ADDR'],$devid);
        $error='';
        // updateTime() uses wait=-1 option for qput. So no answer is returned in case of success.
        // If any text is given it must be something wrong.
        // if (trim($res) == 'send to sqlhdl3 -> -1' || trim($res) == '') {
        trim($res);
        if (!empty($res)) {
            //$error="[".$res."] - ".$errPNA;
        }

        $utcDate=gmdate("YmdHis");
        if ($_POST['usrparam'] != '') {

            $result = $arrblt->updateFlight($_POST['usrparam']);
            $error='';
            trim($result);
            //if (trim($result) == 'send to flight -> -1' || trim($res) == '') {
            // updateFlight() uses wait=10 option for qput. So an answer is returned in case of success.
            // If this answer does not contain the text "send to flight" something must be wrong.
            //if (strstr($result,"send to flight") == FALSE) {
            if ($isCput) {
            if (strlen($result)<=0 ) {
                $error="[".$result."] - ".$errPNA;
            }
                
            } else {
            if (strstr($result,"send to flight") == FALSE) {
                $error="[".$result."] - ".$errPNA;
            }
                
            }
            $_POST['usrparam'] = '';
            $_POST['usrparamO'] = '';
        }
        ?>

        <form name="arrbelt">
            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" width="100%">
                <tr class="hdr">
                    <?php if ($enableRefreshButton) { ?>
                    <td width="20%"><img src="graphics/refresh.gif" readonly onMouseOver="this.style.cursor='hand';" onMouseOut="this.style.cursor='default';document.images['refresh'].src='graphics/refresh.gif';" onMouseDown="document.images['refresh'].src='graphics/refreshp.gif';" onMouseUp="document.images['refresh'].src='graphics/refresh.gif';" onDragStart="event.returnValue=false" width="190" height="65" name="refresh" onClick="pros('R');"/></td>
                        <?php } else { ?>
                    <td width="20%"></td>
                        <?php } ?>
                    <td width="60%" colspan="5">Belt <?php echo $devid?></td>
                    <!--<td width="20%"><input type="button" name="submit2" value="Shutdown" onclick="pros('S')"></td>-->
                    <td width="20%"><img src="graphics/ufis.gif" onDragStart="event.returnValue=false" width="161" height="142"/></td>
                </tr>
                <tr class="infor" align="right">
                  <!--td width="90%" colspan="6">IP Address:<?php echo $_SERVER['REMOTE_ADDR'];?></td-->
                    <?php
                    if ($shtdwn != '' || !empty($error)) {
                        ?>
                  <!--td width="10%" rowspan="2"><img src="graphics/shtdwn.gif" onMouseOver="this.style.cursor='hand';" onMouseOut="this.style.cursor='default';document.images['shtdwn'].src='graphics/shtdwn.gif';" onMouseDown="document.images['shtdwn'].src='graphics/shtdwnp.gif';" onMouseUp="document.images['shtdwn'].src='graphics/shtdwn.gif';" onDragStart="event.returnValue=false" width="140" height="45" name="shtdwn" onClick="pros('S');"/></td-->
                    <td width="10%" colspan="7"><img src="graphics/shtdwn.gif" onMouseOver="this.style.cursor='hand';" onMouseOut="this.style.cursor='default';document.images['shtdwn'].src='graphics/shtdwn.gif';" onMouseDown="document.images['shtdwn'].src='graphics/shtdwnp.gif';" onMouseUp="document.images['shtdwn'].src='graphics/shtdwn.gif';" onDragStart="event.returnValue=false" width="140" height="45" name="shtdwn" onClick="pros('S');"/></td>
                        <?php
                    } else {
                        ?>
                    <td width="10%" rowspan="2">&nbsp;</td>

                        <?php
                    }
                    ?>
                </tr>
                <!--
                <tr  align="right">
                  <td width="100%" colspan="6">Last refresh:<?php echo $locDate;?></td>

                </tr>
                -->
            </table>
            <?php
            if ($devty=='BELT') {
                ?>
            <table border="0" cellpadding="0" cellspacing="2" style="border-collapse: collapse" width="100%">
                <tr colspan="7"></td></tr>
                <tr class="tblHdr">
                    <!--td width="24%">Flight Number</td-->
                    <td width="24%">Flight</td>
                    <td width="14%">Status</td>
                    <!--td width="14%">Time</td-->
                    <td width="14%"><?php echo $TimeHeader ?></td><!--STA-->
                    <td width="16%">&nbsp;</td>
                    <td width="16%">&nbsp;</td>
                    <!--td width="16%">&nbsp;</td-->
                        <?php
                        //$locDate;
                        ?>

                    <td class="infor" bgcolor=lightgrey width="16%">
                        <?php
                           if ($enableLastRefreshTime) {
                             echo "IP Address:".$_SERVER['REMOTE_ADDR'];
                           }
                          ?>
                        <br><div name="DateTime" id="DateTime"></div>
                        <?php
                           if ($enableLastRefreshTime) {
                            echo "<br>Last refr.:".$locDate;
                           }
                         ?>
                    </td>

                </tr>
                    <?php
                    $rs_fl = $arrblt->getFlights($tp_future,$tp_lbg,$tp_fbg,$tp_lnd,$tp_exp,$devid);
        //echo "Record count: ".$rs_fl->RecordCount();
//rs2html($rs_fl,'border=2 cellpadding=3','');
                    while (!$rs_fl->EOF) {
                        for($r=1;$r<=$rs_fl->RecordCount();$r++) {

                            if ($ActualSamewithFB) {
                                $b1ba=trim($rs_fl->fields['$beginBelt1']);
                                $b1ea=trim($rs_fl->fields['$beginBelt1']);
                            } else {
                                $b1ba=trim($rs_fl->fields['$ActualBeginBelt1']);
                                $b1ea=trim($rs_fl->fields['$ActualEndBelt1']);
                            }

                            $UtcNow=gmdate("YmdHis");
//echo "B1BA:".$b1ba."</br>";
//echo "B1EA:".$b1ea."</br>";
//echo "NOW :".$UtcNow."</br>";
					//if ((!empty($b1ba) && $b1ba <= $UtcNow) && ($b1ea>$UtcNow || empty($b1ea)))
					{
                            echo "<tr height=\"".$spaceBwRows."\"><td>&nbsp;</td></tr>";
                            echo "<tr class=\"dat\">";
                            echo "<td height=\"65\">".$rs_fl->fields[1]."</td>";
                            $stat = '';
                            $statTime = '';
                            $beltno='';
                            $timeField='';
                            //GFO 20100223 :Changed for BLR
                            //We select the proper field depending on the Status
                            //Default is STOA
                            $timeField = $rs_fl->fields[11];
                            $sta = $rs_fl->fields[11];

                            if (trim($rs_fl->fields[2]) == $devid) {
                                if (trim($rs_fl->fields[4]) != '') {
                                    //$statTime=trim($rs_fl->fields[4]);
                                    $stat='LBG';
                                } elseif (trim($rs_fl->fields[5]) != '') {
                                    //$statTime=trim($rs_fl->fields[5]);
                                    $stat='FBG';
                                }
                                $beltno = 1;
                            } elseif (trim($rs_fl->fields[3]) == $devid) {
                                if (trim($rs_fl->fields[6]) != '') {
                                    //$statTime=trim($rs_fl->fields[6]);
                                    $stat='LBG';
                                } elseif (trim($rs_fl->fields[7]) != '') {
                                    //$statTime=trim($rs_fl->fields[7]);
                                    $stat='FBG';
                                }
                                $beltno = 2;
                            }
                            if ($stat=='') {
                                if (trim($rs_fl->fields[8]) != '') {
                                    //$statTime=trim($rs_fl->fields[8]);
                                    $stat='OBL';
                                } elseif (trim($rs_fl->fields[9]) != '') {
                                    //$statTime=trim($rs_fl->fields[9]);
                                    $stat='LND';
                                } elseif (trim($rs_fl->fields[10]) != '') {
                                    //$statTime=trim($rs_fl->fields[10]);
                                    $stat='ETA';
                                } else {
                                    //$statTime=trim($rs_fl->fields[11]);
                                    $stat='STA';
                                }
                            }



                            //Time Section
                            if ($stat=='OBL' && trim($rs_fl->fields[8]) != '') {
                                $timeField=$rs_fl->fields[8];
                            }
                            if ($stat=='LND' && trim($rs_fl->fields[9]) != '') {
                                $timeField=$rs_fl->fields[9];
                            }
                            if ($stat=='ETA' && trim($rs_fl->fields[10]) != '') {
                                $timeField=$rs_fl->fields[10];
                            }
                            if ($beltno == "1") {
                                if ($stat=='FBG' && trim($rs_fl->fields[5]) != '') {
                                    $timeField=$rs_fl->fields[5];
                                }
                                if ($stat=='LBG' && trim($rs_fl->fields[4]) != '') {
                                    $timeField=$rs_fl->fields[4];
                                }
                            } else {
                                if ($stat=='FBG' && trim($rs_fl->fields[7]) != '') {
                                    $timeField=$rs_fl->fields[7];
                                }
                                if ($stat=='LBG' && trim($rs_fl->fields[6]) != '') {
                                    $timeField=$rs_fl->fields[6];
                                }

                            }

                            //if this is true then the tifa
                            //will be displayed , otherwise the STA will be displayed
                            if (!$displayBestTime) {
                                $timeField = $sta;
                            }
                            //$stat='OBL';
                            //Display STOA always
                            //$statTime=trim($rs_fl->fields[11]);
                            $statTime= $timeField ;

                            if ($statTime != '' ) {
                                $year = substr ($statTime, 0, 4);
                                $month = substr ($statTime, 4,2);
                                $day = substr ($statTime, 6, 2);
                                $hour = substr ($statTime, 8, 2);
                                $min = substr ($statTime, 10, 2);
                                $sec = substr ($statTime, 12, 2);
                                $statTime=gmmktime ($hour,$min,$sec,$month,$day,$year);
                            }
                            if ($stat=='FBG' ) {
                                echo "<td class=\"bFb\">".$stat."</td>";
                            } else if ($stat=='LBG') {
                                echo "<td class=\"bLb\">".$stat."</td>";
                            } else {
                                echo "<td>".$stat."</td>";
                            }
                            echo "<td>".date("H:i",$statTime)."</td>";
                            if ($stat=='OBL' || $ActualBeltOnly==true ) {
                                echo "<td><img src=\"graphics/fb.gif\" onMouseOver=\"this.style.cursor='hand';\" onMouseOut=\"this.style.cursor='default';document.images['fb".$rs_fl->fields[0]."'].src='graphics/fb.gif';\" onMouseDown=\"document.images['fb".$rs_fl->fields[0]."'].src='graphics/fbp.gif';\" onMouseUp=\"document.images['fb".$rs_fl->fields[0]."'].src='graphics/fb.gif';\" onDragStart=\"event.returnValue=false\" width=\"190\" height=\"65\" name=\"fb".$rs_fl->fields[0]."\" onClick=\"pros('".$rs_fl->fields[0]."#F#".$beltno."#".$stat."');\"/></td>";
                            } else {
                                echo "<td class=\"bFb\">First Bag</td>";
                            }
                            // || $stat=='OBL'
                            if ($stat=='FBG') {
                                echo "<td><img src=\"graphics/lb.gif\" onMouseOver=\"this.style.cursor='hand';\" onMouseOut=\"this.style.cursor='default';document.images['lb".$rs_fl->fields[0]."'].src='graphics/lb.gif';\" onMouseDown=\"document.images['lb".$rs_fl->fields[0]."'].src='graphics/lbp.gif';\" onMouseUp=\"document.images['lb".$rs_fl->fields[0]."'].src='graphics/lb.gif';\" onDragStart=\"event.returnValue=false\" width=\"190\" height=\"65\" name=\"lb".$rs_fl->fields[0]."\" onClick=\"pros('".$rs_fl->fields[0]."#L#".$beltno."#".$stat."');\"/></td>";
                            } else {
                                echo "<td class=\"bLb\">Last Bag</td>";
                            }
                            if ($enableClearButton) {
                                if ($stat=='FBG' || $stat=='LBG' ) {
                                    echo "<td><img src=\"graphics/cl.gif\" onMouseOver=\"this.style.cursor='hand';\" onMouseOut=\"this.style.cursor='default';document.images['cl".$rs_fl->fields[0]."'].src='graphics/cl.gif';\" onMouseDown=\"document.images['cl".$rs_fl->fields[0]."'].src='graphics/clp.gif';\" onMouseUp=\"document.images['cl".$rs_fl->fields[0]."'].src='graphics/cl.gif';\" onDragStart=\"event.returnValue=false\" width=\"190\" height=\"65\" name=\"cl".$rs_fl->fields[0]."\" onClick=\"pros('".$rs_fl->fields[0]."#C#".$beltno."#".$stat."');\"/></td>";
                                } else {
                                    echo "<td class=\"bCl\">Clear</td>";
                                }
                            }
                            echo "</tr>";
						}
                            $rs_fl->MoveNext();
                        }
                    }
                    $rs_fl->close();
                    ?>
            </table>

                <?php
            } else {
                echo "<img src=\"graphics/cl.gif\" onMouseOver=\"this.style.cursor='hand';\" onMouseOut=\"this.style.cursor='default';document.images['cl1'].src='graphics/cl.gif';\" onMouseDown=\"document.images['cl1'].src='graphics/clp.gif';\" onMouseUp=\"document.images['cl1'].src='graphics/cl.gif';\" onDragStart=\"event.returnValue=false\" width=\"190\" height=\"65\" name=\"cl1\" onClick=\"pros('1#F#1#OBL');\"/>";
                ?>
                    <p class="hdr">Display not configured to view Belt information!
                <?php
                }
                if ($error!='') {
                    echo "<p class=\"msgS\">".$error."";
                }
                ?>

                <input type="hidden" name="usrparam"></input>
        </form>
    </body>
</html>
