// BltDiagram.cpp : implementation file
// Modification History:
// 19-feb-01	rkr	PRF 1305 Dubai: Gantchart-Anzeige unter local unterstützt
//

#include <stdafx.h>
#include <WoResTableDlg.h>
#include <BltDiagram.h>
#include <BltDiaPropertySheet.h>
#include <DiaCedaFlightData.h>
#include <BltChart.h>
#include <BltGantt.h>
#include <TimePacket.h>
#include <RotationISFDlg.h>
#include <PrivList.h>
#include <BltOverviewTableDlg.h>
#include <BltKonflikteDlg.h>
#include <Konflikte.h>
#include <FlightSearchTableDlg.h>
#include <BltCcaTableDlg.h>
#include <Utils.h>
#include <PosDiagram.h>
#include <CcaExpandDlg.h>
#include <SpotAllocation.h>
#include <Utils.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static void BltDiagramCf(void *popInstance, int ipDDXType,
						 void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// BltDiagram
extern int igAnsichtPageIndex;

IMPLEMENT_DYNCREATE(BltDiagram, CFrameWnd)

BltDiagram::BltDiagram()
{
	igAnsichtPageIndex = -1;
	omMaxTrackSize = CPoint(4000/*1024*/, 1500);
	omMinTrackSize = CPoint(1024 / 4, 768 / 4);
	omViewer.SetViewerKey("POSDIA");
	omViewer.SelectView("<Default>");
    imStartTimeScalePos = 104;
    imStartTimeScalePos++;              // plus one for left border of chart
    imFirstVisibleChart = -1;
	lmBkColor = lgBkColor;
	pogBltDiagram = this;
	bmIsViewOpen = false;
	bgKlebefunktion = false;
	bmRepaintAll = false;
	pomBltCcaTableDlg = NULL;
	pomBltKonflikteDlg = NULL;
	pomBltOverviewTableDlg = NULL;
	m_key = "DialogPosition\\BaggageBeltDiagram";
	
    lmBkColor = COLORREF(SILVER);;
    lmTextColor = COLORREF(BLACK);;
    lmHilightColor = COLORREF(BLACK);;
	
	
}

BltDiagram::~BltDiagram()
{
	pogBltDiagram = NULL;
}


BEGIN_MESSAGE_MAP(BltDiagram, CFrameWnd)
//{{AFX_MSG_MAP(BltDiagram)
ON_WM_CREATE()
ON_WM_DESTROY()
ON_BN_CLICKED(IDC_WORES_BLT, OnWoRes)
ON_BN_CLICKED(IDC_BLTOVERVIEW, OnOverview)
ON_BN_CLICKED(IDC_BLTCCA, OnCca)
ON_BN_CLICKED(IDC_BLTATTENTION, OnAttention)
ON_BN_CLICKED(IDC_BEENDEN, OnBeenden)
ON_BN_CLICKED(IDC_ANSICHT, OnAnsicht)
ON_BN_CLICKED(IDC_INSERT, OnInsert)
ON_WM_GETMINMAXINFO()
ON_WM_PAINT()
ON_WM_ERASEBKGND()
ON_WM_SIZE()
ON_WM_HSCROLL()
// Grouping
ON_BN_CLICKED(IDC_PREV, OnNextChart)
ON_BN_CLICKED(IDC_NEXT, OnPrevChart)

ON_WM_TIMER()
ON_MESSAGE(WM_POSITIONCHILD, OnPositionChild)
ON_MESSAGE(WM_UPDATEDIAGRAM, OnUpdateDiagram)
ON_CBN_SELCHANGE(IDC_VIEW, OnViewSelChange)
ON_CBN_CLOSEUP(IDC_VIEW, OnCloseupView)
ON_BN_CLICKED(IDC_ZEIT, OnZeit)
ON_BN_CLICKED(IDC_SEARCH, OnSearch)
ON_BN_CLICKED(IDC_OFFLINE, OnOffline)
ON_WM_KEYDOWN()
ON_WM_KEYUP()
ON_WM_CLOSE()
ON_MESSAGE(WM_REPAINT_ALL, RepaintAll)
ON_WM_ACTIVATE()
ON_BN_CLICKED(IDC_EXPAND, OnExpand )
ON_BN_CLICKED(IDC_PRINT, OnPrint)
ON_BN_CLICKED(IDC_CHANGES, OnChanges)
ON_BN_CLICKED(IDC_ALLOCATE, OnAllocate)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()


static void BltDiagramCf(void *popInstance, int ipDDXType,
						 void *vpDataPointer, CString &ropInstanceName)
{
	BltDiagram *polDiagram = (BltDiagram *)popInstance;
	
	TIMEPACKET *polTimePacket ;
	
	switch (ipDDXType)
	{
	case REDISPLAY_ALL:
		polDiagram->RedisplayAll();
		break;
	case STAFFDIAGRAM_UPDATETIMEBAND:
		polTimePacket = (TIMEPACKET *)vpDataPointer;
		polDiagram->SetTimeBand(polTimePacket->StartTime, polTimePacket->EndTime);
		polDiagram->UpdateTimeBand();
		break;
	case SAS_VIEW_CHANGED:
        polDiagram->ViewSelChange( (char *) vpDataPointer);
		break;
	case SAS_GET_CUR_VIEW:
		polDiagram->GetCurView((char *) vpDataPointer);
		break;
	case D_FLIGHT_CHANGE_WORES:
		polDiagram->UpdateWoResButton();
		break;
	}
}


void BltDiagram::GetCurView( char * pcpView)
{
	strcpy(pcpView, omViewer.GetViewName());
}


void BltDiagram::ViewSelChange(char *pcpView)
{
	
	omViewer.SelectView(pcpView);
	
	
	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
	polCB->SetCurSel(polCB->FindString( 0, pcpView ) );
	
	UpdateDia();
	//PRF 8363
	SetFocusToDiagram();
}


/////////////////////////////////////////////////////////////////////////////
// BltDiagram message handlers
void BltDiagram::PositionChild()
{
	CRect olRect;
    CRect olChartRect;
    
    int ilLastY = 0;
    omClientWnd.GetClientRect(&olRect);
	
	
	// Grouping
	BltChart *polChart;
	for (int ilIndex = 0; ilIndex < omChartArray.GetSize(); ilIndex++)
    {
        polChart = (BltChart *) omChartArray.GetAt(ilIndex);
		
        if (ilIndex < imFirstVisibleChart)
        {
            polChart->ShowWindow(SW_HIDE);
            continue;
        }
		
        polChart->GetClientRect(&olChartRect);
		olChartRect.right = olRect.right;
		
		olChartRect.top = ilLastY;
		
        ilLastY += polChart->GetHeight();
		olChartRect.bottom = ilLastY;
        
		// check
        if ((polChart->GetState() != Minimized) &&
			(olChartRect.top < olRect.bottom) &&
			(olChartRect.bottom > olRect.bottom))
		{
			olChartRect.bottom = olRect.bottom;
            polChart->SetState(Normal);
		}
		//
        
        polChart->MoveWindow(&olChartRect, FALSE);
        polChart->ShowWindow(SW_SHOW);
	}
	
	
    
	omClientWnd.Invalidate(TRUE);
	SetAllAreaButtonsColor();
	UpdateTimeBand();
}

// Grouping
void BltDiagram::OnUpdatePrevNext(void)
{
	
	//	return;
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();
	
	if (imFirstVisibleChart == 0)
		GetDlgItem(IDC_NEXT)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_NEXT)->EnableWindow(TRUE);
	
	if (imFirstVisibleChart == omChartArray.GetUpperBound())
		GetDlgItem(IDC_PREV)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_PREV)->EnableWindow(TRUE);
}

// Grouping
void BltDiagram::SetPosAreaButtonColor(int ipGroupno)
{
	if (bgDiaGrouping) {
		CTime ilStart = omViewer.GetGeometrieStartTime(); //ogBasicData.GetTime();
		CTime ilEnd = ilStart + CTimeSpan(0,3,0,0);
		BltChart *polChart;
		int ilColorIndex; //x;
		ilColorIndex = 0; //FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2);
		//	ilColorIndex = omViewer.GetGroupColorIndex(ipGroupno,ilStart,ilEnd);
		
		polChart = (BltChart *) omChartArray.GetAt(ipGroupno);
		if (polChart != NULL)
		{
			CCSButtonCtrl *prlButton =polChart->GetChartButtonPtr();
			if (prlButton != NULL)
			{
				if (ilColorIndex == 0) //FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2))
				{
					prlButton->SetColors(::GetSysColor(COLOR_BTNFACE),
						::GetSysColor(COLOR_BTNSHADOW),
						::GetSysColor(COLOR_BTNHIGHLIGHT));
				}
				else
				{
					prlButton->SetColors(ogColors[ilColorIndex],
						::GetSysColor(COLOR_BTNSHADOW),
						::GetSysColor(COLOR_BTNHIGHLIGHT));
				}
			}
		}
	}
}

// Grouping
void BltDiagram::SetAllAreaButtonsColor(void)
{
	if (bgDiaGrouping) {
		int ilGroupCount = omViewer.GetGroupCount();
		for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
		{
			SetPosAreaButtonColor(ilGroupno);
		}
	}
}



void BltDiagram::SetTSStartTime(CTime opTSStartTime)
{
    omTSStartTime = opTSStartTime;
	
	CTime olTSStartTime(omTSStartTime);
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
	
	
    char clBuf[8];
    sprintf(clBuf, "%02d%02d%02d",
        olTSStartTime.GetDay(), olTSStartTime.GetMonth(), olTSStartTime.GetYear() % 100);
    omTSDate.SetWindowText(clBuf);
    omTSDate.Invalidate(FALSE);
}

void BltDiagram::ActivateTimer()
{
	SetTimer(1, (UINT) 60 * 1000 * ogPosDiaFlightData.cimBartimesUpdateMinute, NULL);  // Update bar times
}

void BltDiagram::DeActivateTimer()
{
	KillTimer(1);
}

int BltDiagram::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
    SetTimer(0, (UINT) 60 * 1000, NULL);
	ActivateTimer();
	//	SetTimer(1, (UINT) 60 * 1000 * ogPosDiaFlightData.cimBartimesUpdateMinute, NULL);  // Update bar times
	
	omDialogBar.Create( this, IDD_BLTDIAGRAM, CBRS_TOP, IDD_BLTDIAGRAM );
	SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	
    omViewer.Attach(this);
	UpdateComboBox();
	
	
    CRect olRect; GetClientRect(&olRect);
	CTime olCurrentTime;
	olCurrentTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrentTime);
	
	// Create 'BltAttention'-Button
	CButton *polButton = (CButton *)omDialogBar.GetDlgItem(IDC_BLTATTENTION2);	
	WINDOWPLACEMENT olWndPlace;
	polButton->GetWindowPlacement( &olWndPlace ); 
	m_CB_BltAttention.Create(GetString(IMFK_ATTENTION), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_BLTATTENTION);
    m_CB_BltAttention.SetFont(polButton->GetFont());
	m_CB_BltAttention.EnableWindow(true);
	// Update Attention-Button
	ogKonflikte.CheckAttentionButton();
	
	// Create 'Wores'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_WORES_BLT2);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	CString olTmp;
	olTmp.Format("%s (%d)", GetString(IDS_STRING1914),0);
	m_CB_WoRes.Create(olTmp, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_WORES_BLT);
    m_CB_WoRes.SetFont(polButton->GetFont());
	m_CB_WoRes.EnableWindow(true);
	m_CB_WoRes.SetSecState(ogPrivList.GetStat("BLTDIAGRAMM_CB_WORes"));	
	
	// Create 'Offline'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_OFFLINE2);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	m_CB_Offline.Create(GetString(IDS_STRING1258), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_OFFLINE);
    m_CB_Offline.SetFont(polButton->GetFont());
	m_CB_Offline.EnableWindow(true);
	//m_CB_Offline.ShowWindow(SW_HIDE);
	
	// Create 'Changes'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_CHANGES);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	m_CB_Changes.Create(GetString(IDS_CHANGES), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_CHANGES);
    m_CB_Changes.SetFont(polButton->GetFont());
	m_CB_Changes.EnableWindow(true);
	m_CB_Changes.SetSecState(ogPrivList.GetStat("DESKTOP_CB_History"));
	
#ifdef _FIPSVIEWER
	{
		m_CB_Offline.EnableWindow(false);
	}
#else
	{
	}
#endif
	
	//	m_KlebeFkt.Create( "&Klebefunktion", SS_CENTER | WS_CHILD | WS_VISIBLE, 
	//						CRect(olRect.left + 400, 6,olRect.left + 480, 23), this, IDC_KLEBEFKT);
	
    omTSStartTime = olCurrentTime;// - CTimeSpan(0, 1, 0, 0);
    omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
        0, 0, 0);// - CTimeSpan(1, 0, 0, 0);
	
    omDuration = CTimeSpan(3, 0, 0, 0);
    omTSDuration = CTimeSpan(0, 6, 0, 0);
    omTSInterval = CTimeSpan(0, 0, 10, 0);
	
	//Die wollen local
    CTime olUCT = CTime::GetCurrentTime();//olCurrentTime;
    CTime olRealUTC = olCurrentTime;
    char olBuf[16];
    sprintf(olBuf, "%02d%02d/%02d%02dz",
        olUCT.GetHour(), olUCT.GetMinute(),        
        olRealUTC.GetHour(), olRealUTC.GetMinute()
		);
	
    
	
	CTime olLocalTime = CTime::GetCurrentTime();
	CTime olUtcTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olUtcTime);
	
	char pclTimes[36];
	char pclDate[36];
	
	
	CTime olUtcTimeTmp = CTime::GetCurrentTime();
	olUtcTimeTmp -= ogUtcDiff;
	
	if(ogUtcDiff != 0)
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
	}
	else
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
	}
    
	
	omTime.Create(pclTimes, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 192, 3, olRect.right - 90, 22), this);
	
    omDate.Create(pclDate, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 88, 3, olRect.right - 8, 22), this);
	
    omDate.SetTextColor(RGB(255,0,0));
	
	omTime.ShowWindow(SW_HIDE);
	omDate.ShowWindow(SW_HIDE);
	
	
	
    sprintf(olBuf, "%02d%02d%02d",  omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100);
    int ilPos = (imStartTimeScalePos - olRect.left - 80) / 2;
    omTSDate.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(ilPos, 35, ilPos + 80, 52), this);
	
    // CBitmapButton
    omBB1.Create("PREV", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 36, 21 + 5, olRect.right - 19, (21 + 5) + 25), this, IDC_PREV);
    omBB1.LoadBitmaps("PREVU", "PREVD", NULL, "NEXTX");
	
    omBB2.Create("NEXT", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 19, 21 + 5, olRect.right - 2, (21 + 5) + 25), this, IDC_NEXT);
    omBB2.LoadBitmaps("NEXTU", "NEXTD", NULL, "NEXTX");
	
	if (!bgDiaGrouping)
	{
		omBB1.ShowWindow(SW_HIDE);
		omBB2.ShowWindow(SW_HIDE);
	}
	
	omTimeScale.lmBkColor = lgBkColor;
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
        CRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62), this, 0, NULL);
	//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
    
	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
	//	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
	
	if(bgGatPosLocal)
		omTimeScale.UpdateCurrentTimeLine(olLocalTime);
	else
		omTimeScale.UpdateCurrentTimeLine(olRealUTC);
	
	// must be static
    static UINT ilIndicators[] = { ID_SEPARATOR, ID_SEPARATOR, ID_SEPARATOR, ID_SEPARATOR };
    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,0);
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));
	
    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_NORMAL | SBPS_NORMAL, 400);
	
    omStatusBar.GetPaneInfo(1, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(1, nID, SBPS_NORMAL, 130);
	
    omStatusBar.GetPaneInfo(2, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(2, nID, SBPS_STRETCH | SBPS_NORMAL, 600);
	
    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    omClientWnd.Create(NULL, "ClientWnd", WS_CHILD  | WS_VISIBLE, olRect, this,
        0 , NULL);
	
	// This will fix the bug for the horizontal scroll bar in the preplan mode
    long llTSMin = (omTSStartTime - omStartTime).GetTotalMinutes();
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
    SetScrollRange(SB_HORZ, 0, 1000, FALSE);
    SetScrollPos(SB_HORZ, (int) (1000 * llTSMin / llTotalMin), FALSE);
	
	//    BltChart *polChart;
    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
    imFirstVisibleChart = 0;
	
	
	// Grouping
    int ilLastY = 0;
	BltChart *polChart;
	int ilCount = omViewer.GetGroupCount();
	
	if (!bgDiaGrouping) 
		ilCount = 1;
	
    for (int ilI = 0; ilI <ilCount; ilI++)
    {
		olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
		
        polChart = new BltChart;
        polChart->SetTimeScale(&omTimeScale);
        polChart->SetViewer(&omViewer, ilI);
        polChart->SetStatusBar(&omStatusBar);
        polChart->SetStartTime(omTSStartTime);
        polChart->SetInterval(omTimeScale.GetDisplayDuration());
        polChart->Create(NULL, "BltChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
			olRect, &omClientWnd,
            0 /* IDD_CHART */, NULL);
        
        omChartArray.Add(polChart);
		
        ilLastY += polChart->GetHeight();
    }
    
	
    
	
    OnTimer(0);
	
	// Register DDX call back function
	//TRACE("BltDiagram: DDX Registration\n");
	ogDdx.Register(this, REDISPLAY_ALL, CString("STAFFDIAGRAM"),
		CString("Redisplay all from What-If"), BltDiagramCf);	// for what-if changes
	ogDdx.Register(this, STAFFDIAGRAM_UPDATETIMEBAND, CString("STAFFDIAGRAM"),
		CString("Update Time Band"), BltDiagramCf);	// for updating the yellow lines
	
	ogDdx.Register(this, SAS_VIEW_CHANGED,CString(" "), CString("DATA_RELOAD"),BltDiagramCf);
	ogDdx.Register(this, SAS_GET_CUR_VIEW,CString(" "), CString("DATA_RELOAD"),BltDiagramCf);
	ogDdx.Register(this, D_FLIGHT_CHANGE_WORES, CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), BltDiagramCf);
	
	
	CButton *polCB; 
	
	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ALLOCATE);
	polCB->ShowWindow(SW_SHOW);
	SetpWndStatAll(ogPrivList.GetStat("BLTDIAGRAMM_CB_Allocate"),polCB);
#ifdef _FIPSVIEWER
	{
		SetpWndStatAll('0',polCB);
	}
#else
	{
	}
#endif
	
	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_SEARCH);
	SetpWndStatAll(ogPrivList.GetStat("BLTDIAGRAMM_CB_Search"),polCB);
	
	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ANSICHT);
	SetpWndStatAll(ogPrivList.GetStat("BLTDIAGRAMM_CB_Ansicht"),polCB);
	
	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_EXPAND);
	SetpWndStatAll(ogPrivList.GetStat("BLTDIAGRAMM_CB_Expand"),polCB);
	
#ifdef _FIPSVIEWER
	{
		SetpWndStatAll('0',polCB);
	}
#else
	{
	}
#endif
	
	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_PRINT);
	SetpWndStatAll(ogPrivList.GetStat("BLTDIAGRAMM_CB_Print"),polCB);
	
	if (!bgOffRelFuncGatpos)
	{
		m_CB_Offline.ShowWindow(SW_HIDE);
	}
	
	// Create BltCcaDlg
	if (pomBltCcaTableDlg == NULL)
	{
		pomBltCcaTableDlg = new BltCcaTableDlg(this);
		//pomBltCcaTableDlg->SetWindowPos(&wndTop,100,200, 0, 0, SWP_HIDEWINDOW | SWP_NOSIZE);
	}
	
	// Create OverviewTableDlg
	if (pomBltOverviewTableDlg == NULL)
	{
		pomBltOverviewTableDlg = new BltOverviewTableDlg(this);
		//pomBltOverviewTableDlg->SetWindowPos(&wndTop,100,200,850,400, SWP_HIDEWINDOW);
		pomBltOverviewTableDlg->SetResource("BLT");
	}
	
	// Create ConflictDlg
	if (pomBltKonflikteDlg == NULL)
	{
		pomBltKonflikteDlg = new BltKonflikteDlg(this);
		//pomBltKonflikteDlg->SetWindowPos(&wndTop,100,200,700,400, SWP_HIDEWINDOW);
	}	
	
	// Connect to the GatPosFlightData-Class
	ogPosDiaFlightData.Connect();
	// Connect to the FlightsWithoutResource - Table
	pogWoResTableDlg->Connect();
	
	
	olRect = CRect(0, 95, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));
    CRect olTSRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62);
    omTimeScale.MoveWindow(&olTSRect, TRUE);
	
	if (bgNewGantDefault)
	{
		char pclView[100];
		strcpy(pclView, "<Default>");
		ogDdx.DataChanged((void *)this, SAS_GET_CUR_VIEW,(void *)pclView );
		omViewer.SelectView(pclView);
		CTime olFrom = TIMENULL;
		CTime olTo = TIMENULL;
		CString olFtyps;
		omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
		if (olFrom != TIMENULL && olTo != TIMENULL)
		{
			omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
			omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);
			
			ogSpotAllocation.MakeBltBlkData(omStartTime, omStartTime + omDuration);
			
		}
		ViewSelChange(pclView);
	}
	else
	{
		char pclView[100];
		strcpy(pclView, "<Default>");
		ogDdx.DataChanged((void *)this, SAS_GET_CUR_VIEW,(void *)pclView );
		omViewer.SelectView(pclView);
		
		CTime olFrom = TIMENULL;
		CTime olTo = TIMENULL;
		CString olFtyps;
		omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
		if (olFrom != TIMENULL && olTo != TIMENULL)
		{
			omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
			omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);
			ogSpotAllocation.MakeBltBlkData(omStartTime, omStartTime + omDuration);
		}
		ViewSelChange(pclView);
	}
	
	// set caption
	CString olTimes;
	if (bgGatPosLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	CString olCaption = GetString(IDS_STRING1010);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);
	
	olRect = CRect(0, 95, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));
	GetDialogFromReg(olRect, m_key);
	//	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );
	MoveWindow(olRect);
	
	return 0;
}

void BltDiagram::SaveToReg() 
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void BltDiagram::OnBeenden() 
{
	//ogPosDiaFlightData.UnRegister();	 
	//ogPosDiaFlightData.ClearAll();	 
	DestroyWindow();	
}


void BltDiagram::OnInsert()
{
	RotationISFDlg dlg;
	dlg.DoModal();
}


void BltDiagram::OnAnsicht()
{
	// 	BltDiaPropertySheet olDlg("POSDIA", this, &omViewer, 0, GetString(IDS_STRING1645));
	BltDiaPropertySheet olDlg("BLTDIA", this, &omViewer, 0, GetString(IDS_STRING1645));
	bmIsViewOpen = true;
	int ilDlgRet = olDlg.DoModal();
	bmIsViewOpen = false;
	
	if (ilDlgRet != IDCANCEL)
	{
		ogSpotAllocation.InitializePositionMatrix();
		
		if (olDlg.FilterChanged())
			LoadFlights();
		else
		{
			// upate status bar
			omStatusBar.SetPaneText(0, GetString(IDS_SAS_CHECK));
			omStatusBar.UpdateWindow();
			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			ogKonflikte.SASCheckAll();
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
			
			// upate status bar
			omStatusBar.SetPaneText(0, "");
			omStatusBar.UpdateWindow();
		}
		
		//		UpdateDia();
		
		char pclView[100];
		strcpy(pclView, omViewer.GetViewName());
		ogDdx.DataChanged((void *)this, SAS_VIEW_CHANGED,(void *)pclView );
		
		
	}
	
	UpdateComboBox();
	
	//PRF 8363
	SetFocusToDiagram();
}



void BltDiagram::ResetStateVariables()
{
	CTime olUtcTime;
	GetCurrentUtcTime(olUtcTime);
	
    omTSStartTime = olUtcTime;// - CTimeSpan(0, 1, 0, 0);
    omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
        0, 0, 0);// - CTimeSpan(1, 0, 0, 0);
	
    omDuration = CTimeSpan(3, 0, 0, 0);
    omTSDuration = CTimeSpan(0, 6, 0, 0);
    omTSInterval = CTimeSpan(0, 0, 10, 0);
	
}



bool BltDiagram::UpdateDia()
{
	CString olViewName = omViewer.GetViewName();
	if (olViewName == "<Default>")
	{
		ogPosDiaFlightData.ClearAll();
		ResetStateVariables();
	}
	
	if(IsIconic())  //RST
		return false;
	
	
	
	// set internal variables
	CTime olFrom = TIMENULL;
	CTime olTo = TIMENULL;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
	if (olFrom != TIMENULL && olTo != TIMENULL)
	{
		omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
		omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);
	}
	
	
	omViewer.MakeMasstab();
	
	CTime olT = omViewer.GetGeometrieStartTime();
	SetTSStartTime(olT);
	
	omTSDuration = omViewer.GetGeometryTimeSpan();
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if (!bgNewGantDefault)
	{
		if(bgGatPosLocal)
			ogBasicData.UtcToLocal(olTSStartTime);
	}
	
	
	//	CString t1 = olT.Format("%H:%M %d.%m.%y");
	//	CString t2 = olTSStartTime.Format("%H:%M %d.%m.%y");
	if (bgNewGantDefault)
	{
		if (olViewName != "<Default>")
		{
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);
		}
		else
		{
			if(!bgGatPosLocal)
				ogBasicData.LocalToUtc(olTSStartTime);
		}
	}
	//	CString t3 = olTSStartTime.Format("%H:%M %d.%m.%y");
	
	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
	
	if (bgNewGantDefault)
		omTimeScale.SetDisplayStartTime(olTSStartTime);
	
    omTimeScale.Invalidate(TRUE);
	ChangeViewTo(omViewer.GetViewName(), false);
	
	long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	if (llTotalMin > 0)
		nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);
	
	pogWoResTableDlg->Rebuild();
	
	return true;
}




bool BltDiagram::LoadFlights(const char *pspView)
{
	
	if(pspView == NULL)
	{
		CString olView = omViewer.GetViewName();
		pspView = olView.GetBuffer(0);
	}
	
	
	omViewer.SelectView(pspView);
	if (strcmp(pspView, "<Default>") == 0)
	{
		return false;
	}
	
	
	// get Where-Clause
	CString olWhere;
	bool blRotation;
	
	CString olFOGTAB = "";
	bool blWithAcOnGround = bgShowOnGround;
	if (pogPosDiagram)
		blWithAcOnGround = pogPosDiagram->WithAcOnGround();
	
	if (!omViewer.GetZeitraumWhereString(olWhere, blRotation, olFOGTAB, blWithAcOnGround))
		return false;
	// PRF 8382
	//The following function is called to frame the query for the Genral filter(Unifilter) page.
	CString  olstring = omViewer.GetUnifilterWhereString();
	olstring.TrimLeft();
	olstring.TrimRight();
	if(!olstring.IsEmpty())
	{
		olWhere = olWhere + olstring;	
	}
	// PRF 8382
	
	olWhere.TrimRight();
	if (olWhere.IsEmpty())
		return false;
	
	if (bgMustHaveFLNO)
		olWhere += CString(" AND FLNO <> ' '");
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	
	// Update Status Bar
	omStatusBar.SetPaneText(0, GetString(IDS_STRING1205));
	omStatusBar.UpdateWindow();
	
	ogPosDiaFlightData.Register();	 
	
	// get timeframe
	CTime olFrom;
	CTime olTo;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
	
	//check timelimit for on gound (at moment we don#t know if we should use global or local)
	if (bgShowOnGround && pogPosDiagram)
	{
		if (olFrom != TIMENULL && olTo != TIMENULL)
		{
			CTimeSpan olDuration = olTo - olFrom;
			int ilTotalMinutes = olDuration.GetTotalMinutes();
			if (ogOnGroundTimelimit.GetTotalMinutes() >= ilTotalMinutes /*&& !bmMessageDone*/)
			{
				CString olMess;
				CString olTimeLimit;
				int ilHours = ogOnGroundTimelimit.GetTotalHours();
				int ilMinutes = ogOnGroundTimelimit.GetTotalMinutes() - ilHours*60;
				olTimeLimit.Format("%d.%d",ilHours,ilMinutes);
				olMess.Format(GetString(IDS_ONGROUND_TIMELIMIT), olTimeLimit );
				
				if (CFPMSApp::MyTopmostMessageBox(NULL, olMess, GetString(ST_WARNING), MB_OKCANCEL | MB_ICONWARNING) == IDOK)
					bool blok = true;
				else
					return false;
			}
		}
	}
	
	ogSpotAllocation.MakePstWroBltBlkData( olFrom - CTimeSpan(1,0,0,0) , olTo + CTimeSpan(1,0,0,0));
	
	
	// set pre-filter
	ogPosDiaFlightData.SetPreSelection(olFrom, olTo, olFtyps);
	
	// read flights
	ogPosDiaFlightData.ReadAllFlights(olWhere, blRotation, olFOGTAB);
	
	// set internal variables
	omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
	omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);
	
	// upate status bar
	omStatusBar.SetPaneText(0, "");
	omStatusBar.UpdateWindow();
	
	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	
	return true;
}

void BltDiagram::OnDestroy() 
{
	SaveToReg();
	
	//	if (bgModal == TRUE)
	//		return;
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	ogDdx.UnRegister(&omViewer, NOTUSED);
	
	if (pomBltOverviewTableDlg)
	{
		pomBltOverviewTableDlg->SaveToReg();
		delete pomBltOverviewTableDlg;
		pomBltOverviewTableDlg = NULL;
	}
	
	if (pomBltKonflikteDlg)
	{
		pomBltKonflikteDlg->SaveToReg();
		delete pomBltKonflikteDlg;
		pomBltKonflikteDlg = NULL;
	}
	
	if (pomBltCcaTableDlg)
	{
		pomBltCcaTableDlg->SaveToReg();
		delete pomBltCcaTableDlg;
		pomBltCcaTableDlg = NULL;
	}
	
	// Unregister DDX call back function
	TRACE("BltDiagram: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogDdx.UnRegister(this, NOTUSED);
	
	// Disconnect from the GatPosFlightData-Class
	ogPosDiaFlightData.Disconnect();
	// Disconnect from the FlightsWithoutResource - Table
	if (pogWoResTableDlg)
		pogWoResTableDlg->Disconnect();
	
	CFrameWnd::OnDestroy();
}

void BltDiagram::OnClose() 
{
    // Ignore close -- This makes Alt-F4 keys no effect
	CFrameWnd::OnClose();
}

void BltDiagram::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
    // TODO: Add your message handler code here and/or call default
    CFrameWnd::OnGetMinMaxInfo(lpMMI);
	
    lpMMI->ptMaxTrackSize = omMaxTrackSize;
    lpMMI->ptMinTrackSize = omMinTrackSize;
}

BOOL BltDiagram::OnEraseBkgnd(CDC* pDC) 
{
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);
	
    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
    
    //return CFrameWnd::OnEraseBkgnd(pDC);
}

void BltDiagram::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CRect olRect;
    GetClientRect(&olRect);
	
	
    // draw horizontal line
    CPen olHPen(PS_SOLID, 1, lmHilightColor);
    CPen *polOldPen = dc.SelectObject(&olHPen);
    dc.MoveTo(olRect.left, 27); dc.LineTo(olRect.right, 27);
    dc.MoveTo(olRect.left, 62); dc.LineTo(olRect.right, 62);
	
    CPen olTPen(PS_SOLID, 1, lmTextColor);
    dc.SelectObject(&olTPen);
    dc.MoveTo(olRect.left, 63); dc.LineTo(olRect.right, 63);
    
    
    // draw vertical line
    dc.SelectObject(&olTPen);
    dc.MoveTo(imStartTimeScalePos - 2, 27);
    dc.LineTo(imStartTimeScalePos - 2, 63);
	
    dc.SelectObject(&olHPen);
    dc.MoveTo(imStartTimeScalePos - 1, 27);
    dc.LineTo(imStartTimeScalePos - 1, 63);
	
    dc.SelectObject(polOldPen);
    // Do not call CFrameWnd::OnPaint() for painting messages
}


void BltDiagram::OnSize(UINT nType, int cx, int cy) 
{
    CFrameWnd::OnSize(nType, cx, cy);
    
    // TODO: Add your message handler code here
    CRect olRect; GetClientRect(&olRect);
	
	if (bgDiaGrouping)
	{
		CRect olBB1Rect(olRect.right - 36, 27 + 5, olRect.right - 19, (27 + 5) + 25);
		omBB1.MoveWindow(&olBB1Rect, TRUE);
		
		CRect olBB2Rect(olRect.right - 19, 27 + 5, olRect.right - 2, (27 + 5) + 25);
		omBB2.MoveWindow(&olBB2Rect, TRUE);
	}
    CRect olTSRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62);
    omTimeScale.MoveWindow(&olTSRect, TRUE);
	
    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    // LeftTop, RightBottom
    omClientWnd.MoveWindow(&olRect, TRUE);
	
	GetClientRect(&olRect);
	omTime.MoveWindow( CRect(olRect.right - 192, 3, olRect.right - 90, 22));
    omDate.MoveWindow( CRect(olRect.right - 88, 3, olRect.right - 8, 22));
	
	PositionChild();
}

void BltDiagram::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
    // TODO: Add your message handler code here and/or call default
    
    //CFrameWnd::OnHScroll(nSBCode, nPos, pScrollBar);
	
	
	CTime olTSStartTime = omTSStartTime;
	
	// Grouping
	//	int ilCurrLine = pomChart->GetGanttPtr()->GetTopIndex();
    long llTotalMin;
    int ilPos;
    
	CRect olRect;
	omTimeScale.GetClientRect(&olRect);
    switch (nSBCode)
    {
	case SB_LINEUP :
		//OutputDebugString("LineUp\n\r");
		
		llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
		ilPos = GetScrollPos(SB_HORZ) - int (60 * 1000L / llTotalMin);
		
		if (ilPos <= 0)
		{
			ilPos = 0;
			ShowTime(omStartTime);
		}
		else
			ShowTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));
		
		omViewer.UpdateLineHeights();
        break;
        
	case SB_LINEDOWN :
		//OutputDebugString("LineDown\n\r");
		
		llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
		ilPos = GetScrollPos(SB_HORZ) + int (60 * 1000L / llTotalMin);
		if (ilPos >= 1000)
		{
			ilPos = 1000;
			ShowTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin+1), 0));
		}
		else
			ShowTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));
		
		omViewer.UpdateLineHeights();
        break;
        
	case SB_PAGEUP :
		//OutputDebugString("PageUp\n\r");
		
		llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
		ilPos = GetScrollPos(SB_HORZ)
			- int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
		if (ilPos <= 0)
		{
			ilPos = 0;
			ShowTime(omStartTime);
		}
		else
			ShowTime(omTSStartTime - CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));
		
		omViewer.UpdateLineHeights();
        break;
        
	case SB_PAGEDOWN :
		//OutputDebugString("PageDown\n\r");
		
		llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
		ilPos = GetScrollPos(SB_HORZ)
			+ int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
		if (ilPos >= 1000)
		{
			ilPos = 1000;
			ShowTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin+1), 0));
		}
		else
			ShowTime(omTSStartTime + CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));
		
		omViewer.UpdateLineHeights();
        break;
        
	case SB_THUMBTRACK /* pressed, any drag time */:
		//OutputDebugString("ThumbTrack\n\r");
		
		llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
		
		SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin * nPos / 1000), 0));
		
		olTSStartTime = omTSStartTime;
		if(bgGatPosLocal)
			ogBasicData.UtcToLocal(olTSStartTime);
		
		omTimeScale.SetDisplayStartTime(olTSStartTime);
		omTimeScale.Invalidate(TRUE);
		SetScrollPos(SB_HORZ, nPos, TRUE);
		omClientWnd.Invalidate(FALSE);
        break;
		
	case SB_THUMBPOSITION:	// the thumb was just released?
		omClientWnd.Invalidate(FALSE);
		
		
	case SB_TOP :
        //break;
	case SB_BOTTOM :
		//OutputDebugString("TopBottom\n\r");
        break;
        
	case SB_ENDSCROLL :
		//OutputDebugString("EndScroll\n\r");
		//OutputDebugString("\n\r");
        break;
    }
	
	//	pomChart->GetGanttPtr()->SetTopIndex(ilCurrLine);
}


void BltDiagram::OnTimer(UINT nIDEvent)
{
	if (nIDEvent == 1 && !bgConfCheckRuns)
	{
		DeActivateTimers();
		ogPosDiaFlightData.UpdateBarTimes();
		ActivateTimers();
	}
	
	///// Update time line!
	if(bmNoUpdatesNow == FALSE)
	{
		
		char pclTimes[100];
		char pclDate[100];
		
		CTime olLocalTime = CTime::GetCurrentTime();
		CTime olUtcTime = CTime::GetCurrentTime();
		ogBasicData.LocalToUtc(olUtcTime);
		
		CTime olUtcTimeTmp = CTime::GetCurrentTime();
		olUtcTimeTmp -= ogUtcDiff;
		
		if(ogUtcDiff != 0)
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
		}
		else
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
		}
		
		if(bgGatPosLocal)
			omTimeScale.UpdateCurrentTimeLine(olLocalTime);
		else
			omTimeScale.UpdateCurrentTimeLine(olUtcTime);
		// Grouping
		
		for (int ilChartNo = 0; ilChartNo < omChartArray.GetSize(); ilChartNo++)
		{
			if(bgGatPosLocal)
				((BltChart *)omChartArray[ilChartNo])->GetGanttPtr()->SetCurrentTime(olLocalTime);
			else
				((BltChart *)omChartArray[ilChartNo])->GetGanttPtr()->SetCurrentTime(olUtcTime);
		}
		// Grouping	end	
		
		omTime.SetWindowText(pclTimes);
		omTime.Invalidate(FALSE);
		
		omDate.SetWindowText(pclDate);
		omDate.Invalidate(FALSE);
		
		CFrameWnd::OnTimer(nIDEvent);
	}
	///// Update time line! (END)
}

void BltDiagram::SetMarkTime(CTime opStartTime, CTime opEndTime)
{
	// Grouping
	for (int ilChartNo = 0; ilChartNo < omChartArray.GetSize(); ilChartNo++)
	{
		((BltChart *)omChartArray[ilChartNo])->SetMarkTime(opStartTime, opEndTime);
	}
	
}


LONG BltDiagram::OnPositionChild(WPARAM wParam, LPARAM lParam)
{
    PositionChild();
    return 0L;
}

/* before grouping
LONG BltDiagram::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
//	UpdateWoResButton();

  CString olStr;
  int ilGroupNo = HIWORD(lParam);
  int ilLineNo = LOWORD(lParam);
  
	int ilItemID = omViewer.GetItemID(ilGroupNo, ilLineNo);
	
	  CTime olTime = CTime((time_t) lParam);
	  
		char clBuf[255];
		int ilCount;
		
		  BltGantt *polBltGantt = pomChart -> GetGanttPtr();
		  switch (wParam)
		  {
		  // group message
		  case UD_INSERTGROUP :
		  
			PositionChild();
			break;
			
			  case UD_UPDATEGROUP :
			  olStr = omViewer.GetGroupText(ilGroupNo);
			  pomChart->GetChartButtonPtr()->SetWindowText(olStr);
			  pomChart->GetChartButtonPtr()->Invalidate(FALSE);
			  
				olStr = omViewer.GetGroupTopScaleText(ilGroupNo);
				pomChart->GetTopScaleTextPtr()->SetWindowText(olStr);
				pomChart->GetTopScaleTextPtr()->Invalidate(TRUE);
				//SetStaffAreaButtonColor(ilGroupNo);
				break;
				
				  case UD_DELETEGROUP :
				  PositionChild();
				  break;
				  
					// line message
					case UD_INSERTLINE :
					polBltGantt->InsertString(ilItemID, "");
					polBltGantt->RepaintItemHeight(ilItemID);
					
					  ilCount = omViewer.GetLineCount(ilGroupNo);
					  sprintf(clBuf, "%d", ilCount);
					  pomChart->GetCountTextPtr()->SetWindowText(clBuf);
					  pomChart->GetCountTextPtr()->Invalidate(TRUE);
					  
						//SetStaffAreaButtonColor(ilGroupNo);
						PositionChild();
						break;
						
						  case UD_UPDATELINE :
						  {
						  polBltGantt->RepaintVerticalScale(ilItemID);
						  if (polBltGantt->GetItemHeight(ilItemID) != polBltGantt->GetLineHeight(ilItemID))
						  {
						  polBltGantt->RepaintItemHeight(ilItemID);
						  PositionChild();
						  }
						  else
						  {
						  polBltGantt->RepaintGanttChart(ilItemID);
						  }
						  }
						  break;
						  
							case UD_DELETELINE :
							polBltGantt->DeleteString(ilItemID);
							
							  ilCount = omViewer.GetLineCount(ilGroupNo);
							  sprintf(clBuf, "%d", ilCount);
							  pomChart->GetCountTextPtr()->SetWindowText(clBuf);
							  pomChart->GetCountTextPtr()->Invalidate(TRUE);
							  
								//SetStaffAreaButtonColor(ilGroupNo);
								PositionChild();
								break;
								
								  case UD_UPDATELINEHEIGHT :
								  pomChart->omGantt.RepaintItemHeight(ilItemID);
								  //SetStaffAreaButtonColor(ilGroupNo);
								  PositionChild();
								  break;
								  }
								  
									return 0L;
									}
*/
// Grouping
LONG BltDiagram::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
    CString olStr;
    int ipGroupNo = HIWORD(lParam);
    int ipLineNo = LOWORD(lParam);
	CTime olTime = CTime((time_t) lParam);
	
    BltChart *polChart;
	if (bgDiaGrouping) {
		polChart = (BltChart *) omChartArray.GetAt(ipGroupNo);
	} else {
		polChart = (BltChart *) omChartArray.GetAt(0);
		if (ipGroupNo > 0) {
			ipLineNo = omViewer.GetAbsoluteLine(ipGroupNo,ipLineNo);
		}
	}
	
	if(polChart == NULL)
	{
		return 0L;
	}
    BltGantt *polGantt = polChart -> GetGanttPtr();
	if(polGantt == NULL)
	{
		return 0L;
	}
	
    switch (wParam)
    {
        // group message
	case UD_INSERTGROUP :
		// CreateChild(ipGroupNo);
		if (bgDiaGrouping) {
            if (ipGroupNo <= imFirstVisibleChart)
			{
                imFirstVisibleChart++;
				OnUpdatePrevNext();
			}
            PositionChild();
		}
        break;
		
	case UD_UPDATEGROUP :
		if (bgDiaGrouping) {
		olStr = omViewer.GetGroupText(ipGroupNo);
		polChart->GetChartButtonPtr()->SetWindowText(olStr);
		polChart->GetChartButtonPtr()->Invalidate(FALSE);
		
		olStr = omViewer.GetGroupTopScaleText(ipGroupNo);
		polChart->GetTopScaleTextPtr()->SetWindowText(olStr);
		polChart->GetTopScaleTextPtr()->Invalidate(TRUE);
		SetPosAreaButtonColor(ipGroupNo);
		}
        break;
        
	case UD_DELETEGROUP :
		if (bgDiaGrouping) {
		delete omChartArray.GetAt(ipGroupNo);
		//omChartArray.GetAt(ipGroupNo) -> DestroyWindow();
		omChartArray.RemoveAt(ipGroupNo);
		
		if (ipGroupNo <= imFirstVisibleChart)
		{
			imFirstVisibleChart--;
			OnUpdatePrevNext();
		}
		PositionChild();
		}
        break;
		
        // line message
	case UD_INSERTLINE :
		polGantt->InsertString(ipLineNo, "");
		polGantt->RepaintItemHeight(ipLineNo);
		SetPosAreaButtonColor(ipGroupNo);
		PositionChild();
        break;
        
	case UD_UPDATELINE :
		polGantt->RepaintVerticalScale(ipLineNo);
		if (polGantt->GetItemHeight(ipLineNo) != polGantt->GetLineHeight(ipLineNo))
		{
			polGantt->RepaintItemHeight(ipLineNo);
			PositionChild();
		}
		else
		{
			polGantt->RepaintGanttChart(ipLineNo);
		}
		
		SetPosAreaButtonColor(ipGroupNo);
        break;
		
	case UD_DELETELINE :
		polGantt->DeleteString(ipLineNo);
		SetPosAreaButtonColor(ipGroupNo);
		PositionChild();
        break;
		
	case UD_UPDATELINEHEIGHT :
		polGantt->RepaintItemHeight(ipLineNo);
		SetPosAreaButtonColor(ipGroupNo);
		PositionChild();
        break;
		
    }
	
    return 0L;
}




///////////////////////////////////////////////////////////////////////////////
// Damkerng and Pichet: Date: 5 July 1995

void BltDiagram::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
	polCB->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{
		
		olViewName = omViewer.SelectView();//ogCfgData.rmUserSetup.STCV;
	}
	
	ilIndex = polCB->FindString(-1,olViewName);
	
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}
}

void BltDiagram::ChangeViewTo(const char *pcpViewName,bool RememberPositions)
{
	if (bgNoScroll == TRUE)
		return;
	
	
	for (int ilLc = 0; ilLc < omChartArray.GetSize(); ilLc++)
	{
		BltChart *polChart = (BltChart *)omChartArray[ilLc];
		if (polChart != NULL)
		{
			if (!polChart->omGantt.bmRepaint)
			{
				bmRepaintAll = true;
				return;
			}
		}
	}
	
	
	int ilDwRef = m_dwRef;
	AfxGetApp()->DoWaitCursor(1);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	
	//	int ilChartState;
	
	CCSPtrArray <int> olChartStates;
	olChartStates.RemoveAll();
	CCSPtrArray <int> olChartTopIndices;
	olChartTopIndices.RemoveAll();
	
	CRect olRect; omTimeScale.GetClientRect(&olRect);
	
	/*
	//rkr25042001
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
	ogBasicData.UtcToLocal(olTSStartTime);
    omViewer.ChangeViewTo(pcpViewName, olTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
	*/
	// viewer always in UTC
	omViewer.ChangeViewTo(pcpViewName, omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
	// Grouping
#if 0
	
	if (RememberPositions == TRUE)
	{
		ilChartState = pomChart->GetState();
	}
	// Id 30-Sep-96
	// This will remove a lot of warning message when the user change view.
	// If we just delete a staff chart, MFC will produce two warning message.
	// First, Revoke not called before the destructor.
	// Second, calling DestroyWindow() in CWnd::~CWnd.
	//delete (BltChart *)omPtrArray.GetAt(ilIndex);
	pomChart->DestroyWindow();
#endif
    
	
    for (int ilIndex = 0; ilIndex <  omChartArray.GetSize(); ilIndex++)
    {

		if (RememberPositions == TRUE)
		{
			int ilState = ((BltChart *)omChartArray[ilIndex])->GetState();
			olChartStates.NewAt(ilIndex,ilState);
			
			
			int ilTopIndex = 0;
			if (((BltChart *)omChartArray[ilIndex])->GetGanttPtr() != NULL)
				ilTopIndex = ((BltChart *)omChartArray[ilIndex])->GetGanttPtr()->GetTopIndex();
			
			olChartTopIndices.NewAt(ilIndex,ilTopIndex);
		}
		((BltChart *)omChartArray[ilIndex])->DestroyWindow();
    }
    omChartArray.RemoveAll();
	
    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
	if (RememberPositions == FALSE)
	{
		imFirstVisibleChart = 0;
	}
	
	
	CTime olCurr = CTime::GetCurrentTime();
	//Die wollen local
	if(!bgGatPosLocal)
		ogBasicData.LocalToUtc(olCurr);
	int ilLastY = 0;
	BltChart *polChart;
 		int ilCount = omViewer.GetGroupCount();
	
	if (!bgDiaGrouping) 
		ilCount = 1;
	
   for (int ilI = 0; ilI < ilCount; ilI++)
    {
        olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
        
        polChart = new BltChart;
        polChart->SetTimeScale(&omTimeScale);
        polChart->SetViewer(&omViewer, ilI);
        polChart->SetStatusBar(&omStatusBar);
        polChart->SetStartTime(omTSStartTime);
        polChart->SetInterval(omTimeScale.GetDisplayDuration());
        polChart->Create(NULL, "BltChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
            olRect, &omClientWnd,
            0 /* IDD_CHART */, NULL);
		
		polChart->GetGanttPtr()->SetCurrentTime(olCurr);
        omChartArray.Add(polChart);
		
        ilLastY += polChart->GetHeight();
    }
	for (int ilChartNo = 0; ilChartNo < omChartArray.GetSize(); ilChartNo++)
	{
		if (RememberPositions == TRUE)
		{
			int ilChartState = olChartStates[ilChartNo];
			int ilTopIndex = olChartTopIndices[ilChartNo];
			
			((BltChart *)omChartArray[ilChartNo])->SetState(olChartStates[ilChartNo]);
			if (((BltChart *)omChartArray[ilChartNo])->GetGanttPtr() != NULL)
				((BltChart *)omChartArray[ilChartNo])->GetGanttPtr()->SetTopIndex(olChartTopIndices[ilChartNo]);
		}
		((BltChart *)omChartArray[ilChartNo])->GetGanttPtr()->SetCurrentTime(olCurr);
	}      
	
	
	PositionChild();
	
	bmNoUpdatesNow = FALSE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	
	
	if (pomBltKonflikteDlg)
		pomBltKonflikteDlg->ChangeViewTo();
	
	if (pomBltCcaTableDlg)
		pomBltCcaTableDlg->ReloadData(&ogPosDiaFlightData);
	
	if (pomBltOverviewTableDlg)
		pomBltOverviewTableDlg->Update();
	
	UpdateWoResButton();
	
	AfxGetApp()->DoWaitCursor(-1);
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();
}


void BltDiagram::UpdateWoResButton()
{
/*	int ilCount = pogWoResTableDlg->GetCount(BLTDIA);
if(ilCount >= 0)
{
CString olTmp;
olTmp.Format("%s (%d)", GetString(IDS_STRING1914),ilCount);

		CButton *polSave = (CButton *)omDialogBar.GetDlgItem(IDC_WORES);	
		if(polSave != NULL)
		polSave->SetWindowText(olTmp);
		}
	*/
	
	CString olTmp;
	COLORREF olButtonColor;
	
	int ilCount = pogWoResTableDlg->GetCount(BLTDIA);
	if(ilCount > 0)
	{
		if(bgFwrBlt)
			olButtonColor = ogColors[IDX_ORANGE];
		else
			olButtonColor = ::GetSysColor(COLOR_BTNFACE);
		
		olTmp.Format("%s (%d)", GetString(IDS_STRING2624),ilCount);
	}
	else
	{
		olTmp.Format("%s (%d)", GetString(IDS_STRING2624),ilCount);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}
	
	m_CB_WoRes.SetWindowText(olTmp);
	m_CB_WoRes.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_WoRes.UpdateWindow();
	
}

void BltDiagram::UpdateChangesButton(CString& opText, COLORREF opColor)
{
	CString olTmp;
	olTmp.Format("%s (%s)", GetString(IDS_CHANGES),opText);
	
	m_CB_Changes.SetWindowText(olTmp);
	m_CB_Changes.SetColors(opColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_Changes.UpdateWindow();
}

void BltDiagram::OnViewSelChange()
{
    //PRF 8363, Change the focus to gantt chart //
	SetFocusToDiagram();
	
    char clText[64];
    CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
    polCB->GetLBText(polCB->GetCurSel(), clText);
	if(strcmp(clText, "<Default>") == 0)
		return;
	
	ogSpotAllocation.InitializePositionMatrix();
	if (!LoadFlights(clText))
		return;
	
	//	UpdateDia();
	
	ogDdx.DataChanged((void *)this, SAS_VIEW_CHANGED,(void *)clText );
}


void BltDiagram::OnCloseupView() 
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();
}

void BltDiagram::OnZeit()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();
	
	// TODO: Add your command handler code here
	CTime olTime;
	GetCurrentUtcTime(olTime);
	olTime -= CTimeSpan(0, 1, 0, 0);
	
	ShowTime(olTime);
	/*
	
	  olTime -= CTimeSpan(0, 1, 0, 0);
	  
		SetTSStartTime(olTime);
		//TRACE("Time: [%s]\n", olTime.Format("%H:%M"));
		
		  CTime olTSStartTime(omTSStartTime);
		  if(bgGatPosLocal)	
		  ogBasicData.UtcToLocal(olTSStartTime);
		  
			
			  omTimeScale.SetDisplayStartTime(olTSStartTime);
			  omTimeScale.Invalidate(TRUE);
			  
				// update scroll bar position
				long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
				long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
				nPos = nPos * 1000L / llTotalMin;
				SetScrollPos(SB_HORZ, int(nPos), TRUE);
				
				  //ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
				  
					CRect olRect;
					omTimeScale.GetClientRect(&olRect);
					omViewer.ChangeViewTo(omViewer.SelectView(), omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
					PositionChild();
					
					  omClientWnd.Invalidate(FALSE);
	*/
}


void BltDiagram::OnSearch()
{
	pogFlightSearchTableDlg->SetUrnoFilter( &ogPosDiaFlightData.omUrnoMap);
	pogFlightSearchTableDlg->Activate(omViewer.GetBaseViewName(), omViewer.GetViewName(), BLTDIA);
}

void BltDiagram::OnChanges()
{
	CallHistory(CString("BLTBELT"));
}

BOOL BltDiagram::DestroyWindow() 
{
	
	if ((bmIsViewOpen))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}
	pogBltDiagram = NULL; 
	
	ogDdx.UnRegister(&omViewer, NOTUSED);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	BOOL blRc = CWnd::DestroyWindow();
	return blRc;
}

/////////XXXXX
////////////////////////////////////////////////////////////////////////
// BltDiagram keyboard handling
// Grouping
void BltDiagram::OnPrevChart()
{
    if (imFirstVisibleChart > 0)
    {
        imFirstVisibleChart--;
		OnUpdatePrevNext();
        PositionChild();
    }
}

void BltDiagram::OnNextChart()
{
    if (imFirstVisibleChart < omChartArray.GetUpperBound())
    {
        imFirstVisibleChart++;
		OnUpdatePrevNext();
        PositionChild();
    }
}

// Grouping
CListBox *BltDiagram::GetBottomMostGantt()
{
	// Check the size of the area for displaying charts.
	// Pichet used "omClientWnd" not the diagram itself, so we will get the size of this window
	CRect olClientRect;
	omClientWnd.GetClientRect(olClientRect);
	omClientWnd.ClientToScreen(olClientRect);
	
	// Searching for the bottommost chart
	BltChart *polChart;
	for (int ilLc = imFirstVisibleChart; ilLc < omChartArray.GetSize(); ilLc++)
	{
		polChart = (BltChart *)omChartArray[ilLc];
		CRect olRect, olChartRect;
		polChart->GetClientRect(olChartRect);
		polChart->ClientToScreen(olChartRect);
		if (!olRect.IntersectRect(&olChartRect, &olClientRect))
			break;
	}
	
	// Check if the chart we have found is a valid one
	--ilLc;
	if (!(0 <= ilLc && ilLc <= omChartArray.GetSize()-1))
		return NULL;
	
	return &((BltChart *)omChartArray[ilLc])->omGantt;
}


void BltDiagram::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// check if the control key is pressed
    BOOL blIsControl = ::GetKeyState(VK_CONTROL) & 0x8080;
	// This statement has to be fixed for using both in Windows 3.11 and NT.
	// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
	// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.
	
	switch (nChar)
	{
	case VK_UP:		// move the bottom most gantt chart up/down one line
	case VK_DOWN:
		CListBox *polGantt;
		// Grouping
		if ((polGantt = GetBottomMostGantt()) != NULL)
			polGantt->SendMessage(WM_USERKEYDOWN, nChar);
		break;
	case VK_HOME:
		SetScrollPos(SB_HORZ, 0, FALSE);
		OnHScroll(SB_LINEUP, 0, NULL);
		break;
	case VK_END:
		SetScrollPos(SB_HORZ, 1000, FALSE);
		OnHScroll(SB_LINEDOWN, 0, NULL);
		break;
	default:
		omDialogBar.SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
		CFrameWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		break;
	}
}

void BltDiagram::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CFrameWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}

////////////////////////////////////////////////////////////////////////
// BltDiagram -- implementation of DDX call back function


void BltDiagram::RedisplayAll()
{
	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
}

void BltDiagram::SetTimeBand(CTime opStartTime, CTime opEndTime)
{
	omTimeBandStartTime = opStartTime;
	omTimeBandEndTime = opEndTime;
}

void BltDiagram::UpdateTimeBand()
{
	// Grouping
	//	pomChart->omGantt.SetMarkTime(omTimeBandStartTime, omTimeBandEndTime);
	for (int ilLc = imFirstVisibleChart; ilLc < omChartArray.GetSize(); ilLc++)
	{
		BltChart *polChart = (BltChart *)omChartArray[ilLc];
		polChart->omGantt.SetMarkTime(omTimeBandStartTime, omTimeBandEndTime);
	}
}

LONG BltDiagram::RepaintAll(WPARAM wParam, LPARAM lParam)
{
	ChangeViewTo(omViewer.GetViewName(), true);
	return 0L;
}


void BltDiagram::OnOverview()
{
	if (pomBltOverviewTableDlg != NULL)
		pomBltOverviewTableDlg->Activate();
}


void BltDiagram::OnCca()
{
	if (pomBltCcaTableDlg != NULL)
		pomBltCcaTableDlg->Activate();
}



void BltDiagram::OnAttention()
{	
	if (pomBltKonflikteDlg != NULL)
		pomBltKonflikteDlg->Attention();
}

void BltDiagram::UpdateKonflikteDlg()
{	
	if (pomBltKonflikteDlg != NULL)
		//		pomBltKonflikteDlg->UpdateDisplay();
		pomBltKonflikteDlg->ChangeViewTo();
}

void BltDiagram::OnOffline()
{	

	if(ogPosDiaFlightData.bmOffLine)
	{
		if (MessageBox(GetString(IDS_STRING1772), GetString(IMFK_RELEASE), MB_YESNO | MB_ICONQUESTION ) == IDNO)
			return;
	}


	if(bgReasonFlag)
	{
		
		if(ogPosDiaFlightData.bmOffLine && ogPosDiaFlightData.lmPosReasonUrno == 0 && ogPosDiaFlightData.bmPosReason == true) 				
		{
			CString olReason = CFPMSApp::GetSelectedReason(this);
			ogPosDiaFlightData.lmPosReasonUrno = atof(olReason);
			if(ogPosDiaFlightData.lmPosReasonUrno == 0)
				return;
		}
		
		if(ogPosDiaFlightData.bmOffLine && ogPosDiaFlightData.lmGatReasonUrno == 0 && ogPosDiaFlightData.bmGatReason == true) 				
		{
			CString olReason = CFPMSApp::GetSelectedReason(this, CString("GATF"), CString(""));
			ogPosDiaFlightData.lmGatReasonUrno = atof(olReason);
			if(ogPosDiaFlightData.lmGatReasonUrno == 0)
				return;
		}
	}
	
	ogPosDiaFlightData.ToggleOffline();
	
	
	ogPosDiaFlightData.bmGatReason = false;
	ogPosDiaFlightData.lmGatReasonUrno = 0;
	ogPosDiaFlightData.bmPosReason = false;
	ogPosDiaFlightData.lmPosReasonUrno = 0;
	
}



void BltDiagram::OnAllocate()
{
	if (!bgOffRelFuncGatpos)
	{
		ogPosDiaFlightData.ToggleOffline();
	}
	
	if (bgOffRelFuncGatpos && !ogPosDiaFlightData.bmOffLine)
	{
		MessageBox(GetString(IDS_STRING2003), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
		return;
	}
	
	SpotAllocateDlg *polDlg = new SpotAllocateDlg(this);
	if(polDlg->DoModal() == IDOK)
	{
		omStatusBar.SetPaneText(0, GetString(IDS_STRING1493));
		omStatusBar.UpdateWindow();
	}
	omStatusBar.SetPaneText(0, "");
	omStatusBar.UpdateWindow();
	delete polDlg;
	
	if (!bgOffRelFuncGatpos)
	{
		ogPosDiaFlightData.ToggleOffline();
	}
	
}


void BltDiagram::OnWoRes()
{
	pogWoResTableDlg->Activate(BLTDIA);
}


void BltDiagram::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) 
{
	CFrameWnd::OnActivate(nState, pWndOther, bMinimized);
	
	pogWoResTableDlg->Activate(BLTDIA, false);
	
	
}


void BltDiagram::ShowTime(const CTime &ropTime)
{
	
	if(omTSStartTime == ropTime)
		return;
	
	
	SetTSStartTime(ropTime);
	//TRACE("Time: [%s]\n", olTime.Format("%H:%M"));
	
	CTime olTSStartTime(omTSStartTime);
	if(bgGatPosLocal)	
		ogBasicData.UtcToLocal(olTSStartTime);
	
	
	omTimeScale.SetDisplayStartTime(olTSStartTime);
    omTimeScale.Invalidate(TRUE);
	
	// update scroll bar position
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);
	
	//ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
	
	//	CRect olRect;
	//	omTimeScale.GetClientRect(&olRect);
	//	omViewer.ChangeViewTo(omViewer.SelectView(), omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
	//	PositionChild();
	
    omClientWnd.Invalidate(FALSE);
}




bool BltDiagram::ShowFlight(long lpUrno)
{
	DIAFLIGHTDATA *prlFlight = ogPosDiaFlightData.GetFlightByUrno(lpUrno);
	if (prlFlight == NULL)
		return false;
	
	int ilChartNo;
	int index = omViewer.AdjustBar(prlFlight, NULL, ilChartNo);
	if (index != -1)
	{
		BltChart *polChart = (BltChart *) omChartArray[ilChartNo];
		polChart->omGantt.SetTopIndex(index);
	}
	else
	{
		CTime olStart;
		CTime olEnd;
		if (omViewer.CalculateTimes(*prlFlight, olStart, olEnd))
		{		
			CTime olTime = olStart - CTimeSpan(0, 1, 0, 0);
			ShowTime(olTime);
			if(bgGatPosLocal) ogBasicData.UtcToLocal(olStart);
			if(bgGatPosLocal) ogBasicData.UtcToLocal(olEnd);
			SetMarkTime(olStart, olEnd);
		}	
	}
	
	return true;
}

void BltDiagram::OnExpand()
{
	
	CString olStev;
	omViewer.GetStev(olStev);
	
	
	long llMinutes = omDuration.GetTotalMinutes();
	
	CTime olRefDate = omStartTime + CTimeSpan(0,0, llMinutes / 2  ,0);
	
	
	
	
	CTime olFrom = TIMENULL;
	CTime olTo = TIMENULL;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
	
	
	CCAExpandDlg olDlg (this, SUB_MOD_BLT, olStev, olRefDate, olFrom, olTo);
	olDlg.DoModal();
	
	
}

void BltDiagram::OnPrint() 
{
	bool blFirstChart = true;
	
    for (int ilIndex = 0; ilIndex < omChartArray.GetSize(); ilIndex++)
    {
        BltChart *polChart = (BltChart *) omChartArray.GetAt(ilIndex);
		if ((polChart->GetState() == Maximized || polChart->GetState() == Normal) &&
			omViewer.GetLineCount(ilIndex) > 0)
		{
			omViewer.PrintGantt(polChart,blFirstChart);
			blFirstChart = false;
		}
	}
	omViewer.PrintGanttEnd();
}

bool BltDiagram::GetLoadedFlights(CCSPtrArray<DIAFLIGHTDATA> &opFlights)
{
	return omViewer.GetLoadedFlights(opFlights);
}


void BltDiagram::SetFocusToDiagram()
{
	//PRF 8363
	
	// Grouping
	if (omChartArray.GetSize() > 0)
	{
		BltGantt *polGantt = ((BltChart *)omChartArray[0])-> GetGanttPtr();
		CWnd* polTopWindow = CWnd::GetActiveWindow();
		CWnd* polParent = ((BltChart *)omChartArray[0])->GetParent()->GetParent();
		//if(polWroGantt != NULL)
		if(polTopWindow != NULL && polParent != NULL)
		{		
			if(polParent->m_hWnd != NULL && polTopWindow->m_hWnd == polParent->m_hWnd)
				polGantt->SetFocus();
		}
	}
}
