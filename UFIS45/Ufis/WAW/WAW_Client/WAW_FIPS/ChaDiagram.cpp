// ChaDiagram.cpp : implementation file
// Modification History:
// 19-feb-01	rkr	PRF 1305 Dubai: Gantchart-Anzeige unter local unterstützt
//

#include <stdafx.h>
#include <FPMS.h>
#include <ChaDiagram.h>
#include <ButtonListDlg.h>
#include <ChaDiaPropertySheet.h>
#include <CcaCedaFlightData.h>
#include <ChaChart.h>
#include <ChaGantt.h>
#include <TimePacket.h>
#include <DataSet.h>
#include <FlightSearchTableDlg.h>
#include <PrivList.h>
#include <Utils.h>
#include <CCSGlobl.h>
#include <process.h>
#include <Utils.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static void ChaDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// ChaDiagram
extern int igAnsichtPageIndex;

IMPLEMENT_DYNCREATE(ChaDiagram, CFrameWnd)

ChaDiagram::ChaDiagram()
{
	igAnsichtPageIndex = -1;
	omMaxTrackSize = CPoint(4000/*1024*/, 1500);
	omMinTrackSize = CPoint(1024 / 4, 768 / 4);
	omViewer.SetViewerKey("CCADIA");
	omViewer.SelectView("<Default>");
    imStartTimeScalePos = 104;
    imStartTimeScalePos++;              // plus one for left border of chart
    imFirstVisibleChart = -1;
	lmBkColor = lgBkColor;
	pogChaDiagram = this;
	bmIsViewOpen = false;
	bgKlebefunktion = false;
	bmRepaintAll = false;
	bmTimeLine = true;
	bmAlreadyAlloc = false;	
	m_key = "DialogPosition\\CheckInDiagram";

    lmBkColor = COLORREF(SILVER);;
    lmTextColor = COLORREF(BLACK);;
    lmHilightColor = COLORREF(BLACK);;

}

ChaDiagram::~ChaDiagram()
{

	pogChaDiagram = NULL;
}


BEGIN_MESSAGE_MAP(ChaDiagram, CFrameWnd)
	//{{AFX_MSG_MAP(ChaDiagram)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BEENDEN, OnBeenden)
	ON_BN_CLICKED(IDC_ANSICHT, OnAnsicht)
	ON_BN_CLICKED(IDC_PRINT_GANTT, OnPrintGantt)
	ON_BN_CLICKED(IDC_SEARCH, OnSearch)
    ON_WM_GETMINMAXINFO()
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
    ON_WM_SIZE()
    ON_WM_HSCROLL()
    ON_WM_TIMER()
    ON_MESSAGE(WM_POSITIONCHILD, OnPositionChild)
    ON_MESSAGE(WM_UPDATEDIAGRAM, OnUpdateDiagram)
    ON_CBN_SELCHANGE(IDC_VIEW, OnViewSelChange)
	ON_CBN_CLOSEUP(IDC_VIEW, OnCloseupView)
	ON_BN_CLICKED(IDC_ZEIT, OnZeit)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_CLOSE()
	ON_MESSAGE(WM_REPAINT_ALL, RepaintAll)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


static void ChaDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	ChaDiagram *polDiagram = (ChaDiagram *)popInstance;

	TIMEPACKET *polTimePacket;

	switch (ipDDXType)
	{
	case CCA_VIEW_CHANGED:
        polDiagram->ViewSelChange( (char *) vpDataPointer);
		break;
	case CCA_GET_CUR_VIEW:
		polDiagram->GetCurView((char *) vpDataPointer);
		break;
	case REDISPLAY_ALL:
		polDiagram->RedisplayAll();
		break;
	case STAFFDIAGRAM_UPDATETIMEBAND:
		polTimePacket = (TIMEPACKET *)vpDataPointer;
		polDiagram->SetTimeBand(polTimePacket->StartTime, polTimePacket->EndTime);
		polDiagram->UpdateTimeBand();
		break;
	}
}

bool ChaDiagram::GetLoadedAllocations(CMapStringToString& opCicUrnoMap)
{
	return omViewer.GetLoadedAllocations(opCicUrnoMap);
}

/////////////////////////////////////////////////////////////////////////////
// ChaDiagram message handlers
void ChaDiagram::PositionChild()
{

    CRect olRect;
    CRect olChartRect;
    
    int ilLastY = 0;
    omClientWnd.GetClientRect(&olRect);
         
    pomChart->GetClientRect(&olChartRect);
    olChartRect.right = olRect.right;

    olChartRect.top = ilLastY;

    ilLastY += pomChart->GetHeight();
    olChartRect.bottom = ilLastY;
        
    // check
    if ((pomChart->GetState() != Minimized) &&
		(olChartRect.top < olRect.bottom) && (olChartRect.bottom > olRect.bottom))
    {
        olChartRect.bottom = olRect.bottom;
        pomChart->SetState(Normal);
    }
    //
        
    pomChart->MoveWindow(&olChartRect, FALSE);
	pomChart->ShowWindow(SW_SHOW);

    
	omClientWnd.Invalidate(TRUE);
	//SetAllStaffAreaButtonsColor();
 	UpdateTimeBand();
    
	
}


void ChaDiagram::SetTSStartTime(CTime opTSStartTime)
{

    omTSStartTime = opTSStartTime;

	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);

    char clBuf[20];
    sprintf(clBuf, "  %02d%02d%02d  ", 
        olTSStartTime.GetDay(), olTSStartTime.GetMonth(), olTSStartTime.GetYear() % 100);
//        omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100);
    omTSDate.SetWindowText(clBuf);
    omTSDate.Invalidate(FALSE);
}


int ChaDiagram::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;


    SetTimer(0, (UINT) 60 * 1000, NULL);

    SetTimer(1, (UINT) 60 * 1000 * ogPosDiaFlightData.cimBartimesUpdateMinute, NULL);  // Update bar times

	omDialogBar.Create( this, IDD_CHADIAGRAM, CBRS_TOP, IDD_CHADIAGRAM );
	SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

	//CButton *polCB;



    omViewer.Attach(this);
	UpdateComboBox();

    CRect olRect; GetClientRect(&olRect);
	CTime olCurrentTime;
	olCurrentTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrentTime);


    omTSStartTime = olCurrentTime;// - CTimeSpan(0, 1, 0, 0);
    omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
        0, 0, 0);// - CTimeSpan(1, 0, 0, 0);

    omDuration = CTimeSpan(3, 0, 0, 0);
    omTSDuration = CTimeSpan(0, 6, 0, 0);
    omTSInterval = CTimeSpan(0, 0, 10, 0);

	//Die wollen local
    CTime olUCT = CTime::GetCurrentTime();//olCurrentTime;
    CTime olRealUTC = olCurrentTime;
    char olBuf[64];
    sprintf(olBuf, "%02d%02d/%02d%02dz",
        olUCT.GetHour(), olUCT.GetMinute(),        
        olRealUTC.GetHour(), olRealUTC.GetMinute()
    );

    
	
	CTime olLocalTime = CTime::GetCurrentTime();
	CTime olUtcTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olUtcTime);

	char pclTimes[36];
	char pclDate[36];
	
	CTime olUtcTimeTmp = CTime::GetCurrentTime();
	olUtcTimeTmp -= ogUtcDiff;
	
	if(ogUtcDiff != 0)
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
	}
	else
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
	}
    
	
	omTime.Create(pclTimes, SS_CENTER | WS_CHILD | WS_VISIBLE, CRect(olRect.right - 192, 3, olRect.right - 90, 22), this);

    omDate.Create(pclDate, SS_CENTER | WS_CHILD | WS_VISIBLE,  CRect(olRect.right - 88, 3, olRect.right - 8, 22), this);
        
    omDate.SetTextColor(RGB(255,0,0));
	
	omTime.ShowWindow(SW_HIDE);
	omDate.ShowWindow(SW_HIDE);
	

    sprintf(olBuf, "%02d%02d%02d  %d",  omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100, GetDayOfWeek(omTSStartTime));
    int ilPos = (imStartTimeScalePos - olRect.left - 80) / 2;
    omTSDate.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(ilPos, 35, ilPos + 80, 52), this);

    // CBitmapButton
    omBB1.Create("PREV", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 36, 21 + 5, olRect.right - 19, (21 + 5) + 25), this, IDC_PREV);
    omBB1.LoadBitmaps("PREVU", "PREVD", NULL, "NEXTX");

    omBB2.Create("NEXT", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 19, 21 + 5, olRect.right - 2, (21 + 5) + 25), this, IDC_NEXT);
    omBB2.LoadBitmaps("NEXTU", "NEXTD", NULL, "NEXTX");

	omBB1.ShowWindow(SW_HIDE);
	omBB2.ShowWindow(SW_HIDE);

	omTimeScale.lmBkColor = lgBkColor;
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
        CRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62), this, 0, NULL);
//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
    
	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
//	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
	if(bgGatPosLocal)
		omTimeScale.UpdateCurrentTimeLine(olLocalTime);
	else
		omTimeScale.UpdateCurrentTimeLine(olRealUTC);
     
    // must be static
    static UINT ilIndicators[] = { ID_SEPARATOR };
    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,0);
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));

    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH | SBPS_NORMAL, cxWidth);

    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    omClientWnd.Create(NULL, "ClientWnd", WS_CHILD /*| WS_HSCROLL */ | WS_VISIBLE, olRect, this,
        0 /* IDD_CLIENTWND */, NULL);
        
	// This will fix the bug for the horizontal scroll bar in the preplan mode
    long llTSMin = (omTSStartTime - omStartTime).GetTotalMinutes();
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
    SetScrollRange(SB_HORZ, 0, 1000, FALSE);
    SetScrollPos(SB_HORZ, (int) (1000 * llTSMin / llTotalMin), FALSE);

    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
    imFirstVisibleChart = 0;
    int ilLastY = 0;

    olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
    
    pomChart = new ChaChart;
    pomChart->SetTimeScale(&omTimeScale);
    pomChart->SetViewer(&omViewer);
    pomChart->SetStatusBar(&omStatusBar);
    pomChart->SetStartTime(omTSStartTime);
    pomChart->SetInterval(omTimeScale.GetDisplayDuration());
    pomChart->Create(NULL, "ChaChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
        olRect, &omClientWnd,
        0 /* IDD_CHART */, NULL);
    

    OnTimer(0);

	// Register DDX call back function
	//TRACE("ChaDiagram: DDX Registration\n");
	ogDdx.Register(this, REDISPLAY_ALL, CString("STAFFDIAGRAM"),
		CString("Redisplay all from What-If"), ChaDiagramCf);	// for what-if changes
	ogDdx.Register(this, STAFFDIAGRAM_UPDATETIMEBAND, CString("STAFFDIAGRAM"),
		CString("Update Time Band"), ChaDiagramCf);	// for updating the yellow lines

 	ogDdx.Register(this, CCA_VIEW_CHANGED,CString(" "), CString("DATA_RELOAD"),ChaDiagramCf);
 	ogDdx.Register(this, CCA_GET_CUR_VIEW,CString(" "), CString("DATA_RELOAD"),ChaDiagramCf);

  
	// SEC

   CButton *polCB;

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ALLOCATE);
	SetpWndStatAll(ogPrivList.GetStat("ChaDIAGRAMM_CB_Allocate"),polCB);
	#ifdef _FIPSVIEWER
	{
		SetpWndStatAll('0',polCB);
	}
	#else
	{
	}
	#endif


	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_PRINT_GANTT);
	SetpWndStatAll(ogPrivList.GetStat("ChaDIAGRAMM_CB_Print"),polCB);

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_SEARCH);
	SetpWndStatAll(ogPrivList.GetStat("ChaDIAGRAMM_CB_Search"),polCB);


	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_DELETE_TIMEFRAME);
	SetpWndStatAll(ogPrivList.GetStat("ChaDIAGRAMM_CB_Delete"),polCB);
	#ifdef _FIPSVIEWER
	{
		SetpWndStatAll('0',polCB);
	}
	#else
	{
	}
	#endif


	#ifdef _FIPSVIEWER
	{
		SetpWndStatAll('0',polCB);
	}
	#else
	{
	}
	#endif

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_EXPAND);
	#ifdef _FIPSVIEWER
	{
		polCB->EnableWindow(false);
	}
	#else
	{
	}
	#endif



	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ANSICHT);

	//SetpWndStatAll(ogPrivList.GetStat("ChaDIAGRAMM_CB_Ansicht"),polCB);




	ogCcaDiaFlightData.Register();	 

	//SetWndPos();	


	if (bgNewGantDefault)
	{
		char pclView[100];
		strcpy(pclView, "<Default>");
		ogDdx.DataChanged((void *)this, CCA_GET_CUR_VIEW,(void *)pclView );
		omViewer.SelectView(pclView);
		ViewSelChange(pclView);
		
		omTSDuration = omViewer.GetGeometryTimeSpan();
		olTSStartTime = omTSStartTime;
		if(bgGatPosLocal)
			ogBasicData.UtcToLocal(olTSStartTime);
		omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
		omTimeScale.Invalidate(TRUE);
		//ChangeViewTo(omViewer.GetViewName(), false);
	}

	// set caption
	CString olTimes;
	if (bgGatPosLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	CString olCaption = GetString(IDS_STRING2871);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);


	olRect = CRect(0, 95, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));
	GetDialogFromReg(olRect, m_key);
	//SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );
	MoveWindow(olRect);
	SetFocusToDiagram(); //PRF 8363
	return 0;

}

void ChaDiagram::SaveToReg() 
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void ChaDiagram::OnBeenden() 
{
	ogCcaDiaFlightData.UnRegister();	 
	ogCcaDiaFlightData.ClearAll();	 
	DestroyWindow();	
}



void ChaDiagram::OnSearch()
{
	pogFlightSearchTableDlg->SetUrnoFilter( &ogCcaDiaFlightData.omUrnoMap);
	pogFlightSearchTableDlg->Activate(omViewer.GetBaseViewName(), omViewer.GetViewName(), CHADIA);
}

bool ChaDiagram::ShowFlight(long lpUrno)
{
	CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(lpUrno);
	
	if (prlFlight == NULL)
		return false;

	pomChart->omGantt.SetTopIndex(omViewer.AdjustBar(NULL, prlFlight));

	return true;
}

void ChaDiagram::OnAnsicht()
{

	//MWO
	SetFocus();
	//END MWO

	ChaDiaPropertySheet olDlg("CCADIA", this, &omViewer, 0, GetString(IDS_STRING2870));
	bmIsViewOpen = true;
	if(olDlg.DoModal() != IDCANCEL)
	{
		TRACE("\nBegin OnAnsicht:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

		omViewer.SelectView(omViewer.GetViewName());

		if (olDlg.FilterChanged())
		{
			LoadFlights();
		}
		else
		{
			// upate status bar
			omStatusBar.SetPaneText(0, GetString(IDS_SAS_CHECK));
			omStatusBar.UpdateWindow();

			// upate status bar
			omStatusBar.SetPaneText(0, "");
			omStatusBar.UpdateWindow();
		}
		
 		omViewer.MakeMasstab();
		CTime olT = omViewer.GetGeometrieStartTime();

		CTime olFrom;
		CTime olTo;
		CString olFtyps;
		omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);


		CString olS = olT.Format("%d.%m.%Y-%H:%M");

		SetTSStartTime(olT);
		omTSDuration = omViewer.GetGeometryTimeSpan();
//rkr
		//Set begin to local if local
		CTime olTSStartTime = omTSStartTime;
		if(bgGatPosLocal)
			ogBasicData.UtcToLocal(olTSStartTime);
    
		omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
//		omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
        omTimeScale.Invalidate(TRUE);
		TRACE("\nBefin Activate Tables:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
		ActivateTables();
		TRACE("\nEnd Activate Tables:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
		ChangeViewTo(omViewer.GetViewName(), false);
		long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
		long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
		nPos = nPos * 1000L / llTotalMin;
		SetScrollPos(SB_HORZ, int(nPos), TRUE);
		UpdateComboBox();
//YYY		ActivateTables();


		char pclView[100];
		strcpy(pclView, omViewer.GetViewName());
		ogDdx.DataChanged((void *)this, CCA_VIEW_CHANGED,(void *)pclView );


	}
	bmIsViewOpen = false;
	TRACE("\nEnd OnAnsicht:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	SetFocusToDiagram() ; //PRF 8363
}

bool ChaDiagram::IsPassFlightFilter(CCAFLIGHTDATA *prpFlight)
{
	bool blRet = false;
	{
		/*

		if(strcmp(prpFlight->Htyp, "K") != 0)
		{
			if(strcmp(prpFlight->Ttyp, "CG") != 0)
			{
				if(strcmp(prpFlight->Flns, "F") != 0)
				{
					if(strcmp(prpFlight->Htyp, "T") != 0)
					{
						blRet = true;
					}
				}
			}
		}
		*/
	}
	return blRet;
}

bool ChaDiagram::HasAllocatedChas(CCSPtrArray<CHADATA> &ropChaList)
{
	bool blRet = false;
	int ilCount = ropChaList.GetSize();
	for(int i = 0; ((i < ilCount) && (blRet == false)); i++)
	{
	}
	return blRet;
}


void ChaDiagram::LoadFlights(char *pspView)
{
	bmReadAll = true;
	CString olView;

	if(pspView != NULL)
	{
		omViewer.SelectView(pspView);
		olView = CString(pspView);
	}
	else
	{
		omViewer.SelectView(omViewer.GetViewName());
		olView = CString(omViewer.GetViewName());
	}
	
	CString olWhere;
	CTime olFrom;
	CTime olTo;
	CString olFtyps;
	int ilDOO;
	bool blRotation;



	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	omStatusBar.SetPaneText(0, GetString(IDS_STRING1205));
	omStatusBar.UpdateWindow();
	

	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
	TRACE("\nBegin Load Flights:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("olFrom: %s,   olTo: %s\n", olFrom.Format("%d.%m.%Y %H:%M:%S"), olTo.Format("%d.%m.%Y %H:%M:%S"));
	ogCcaDiaFlightData.SetPreSelection(olFrom, olTo, olFtyps);

	//now button
	CButton *polCB1 = (CButton *) omDialogBar.GetDlgItem(IDC_ZEIT);
	CTime olCurr;
	CTime olT1 = olFrom;
	CTime olT2 = olTo;
	GetCurrentUtcTime(olCurr);

	if (bgGatPosLocal)
	{
		ogBasicData.LocalToUtc(olT1);
		ogBasicData.LocalToUtc(olT2);
	}
/*
	CString olf=olT1.Format("%d.%m.%Y %H:%M:%S");
	CString olt=olT2.Format("%d.%m.%Y %H:%M:%S");
	CString olC=olCurr.Format("%d.%m.%Y %H:%M:%S");
*/
	if(olT1 < olCurr && olT2 > olCurr)
	{
		polCB1->EnableWindow(TRUE);
		bmTimeLine = true;
	}
	else
	{
		polCB1->EnableWindow(FALSE);
		bmTimeLine = false;
	}


	// set duration and starttime
	omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
//	if (bgGatPosLocal)
//		ogBasicData.UtcToLocal(olFrom);
	omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);

/*

	CTime olCurr = CTime::GetCurrentTime();
	if(bgGatPosLocal) ogBasicData.LocalToUtc(olCurr);


	CButton *polCB1 = (CButton *) omDialogBar.GetDlgItem(IDC_ZEIT);

	if(olFrom < olCurr && olTo > olCurr)
	{
		polCB1->EnableWindow(TRUE);
	}
	else
	{
		polCB1->EnableWindow(FALSE);
	}
*/

	ilDOO = omViewer.GetDOO();

	char pclSelection[7000] = "";



	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\nBegin read:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));


	if (!omViewer.GetZeitraumWhereString(olWhere, blRotation, CString("") ))
		return;

	//olWhere += CString(" AND FLNO > ' '");
	// PRF 8382
	////Forms the query for the Genral filter page.
	CString  olstring = omViewer.GetUnifilterWhereString();
	olstring.TrimLeft();
	olstring.TrimRight();
	if(!olstring.IsEmpty())
	{
		olWhere = olWhere + olstring;	
	}

	ViewerFilter olFilter;
	omViewer.GetViewerFilter(olFilter);
	CMapStringToString olFilterMap;
	CString olAlc;

	if(!olFilter.omAlc.IsEmpty())
	{
		bool blok = ogBCD.GetField("ALT", "ALC2", olFilter.omAlc, "URNO", olAlc);
		if(blok == false)
			blok = ogBCD.GetField("ALT", "ALC3", olFilter.omAlc, "URNO", olAlc);

		if (blok)
			olFilterMap.SetAt("FLNU",olAlc);
	}


//	ogCcaDiaFlightData.ReadAllFlights(olWhere, blRotation);
	ogCcaDiaFlightData.ReadAllFlights(olWhere, blRotation, olFilterMap);

	TRACE("\nEnd read:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\nBegin read BLK:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));


	CString olText = CString("Blk... ");
	omStatusBar.SetPaneText(0, olText);
	omStatusBar.UpdateWindow();

	char pclWhere[512]="";
	
	sprintf(pclWhere, " WHERE (NATO >= '%s' and NAFR <= '%s') OR (NAFR <= '%s' and NATO = ' ') OR  (NATO >= '%s'  and NAFR = ' ')",
												 olFrom.Format("%Y%m%d%H%M00"), olTo.Format("%Y%m%d%H%M59"),
												 olTo.Format("%Y%m%d%H%M59"),
												 olFrom.Format("%Y%m%d%H%M00"));
	ogBCD.Read(CString("BLK"), pclWhere);


	TRACE("\nEnd read BLK:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	CString olCnams = ogCcaDiaFlightData.omChaData.MakeBlkData(TIMENULL, ilDOO);

	TRACE("\nEnd Make BLK:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");

	omViewer.SetCnams(olCnams);
	omViewer.SetDOO(ilDOO);

	//int ilC5 = ogBCD.GetDataCount("BLK");


	//int ilBnam = ogBCD.GetFieldIndex("CHU", "CNAM");
	//ogBCD.SetSort("CHU", "TERM+,CNAM+", true); //true ==> sort immediately


	int ilCurrentFlight=1;
	char pclPaneText[512]="";
	int ilFltAmount = ogCcaDiaFlightData.omUrnoMap.GetCount();

	omStatusBar.UpdateWindow();

	olText = CString("Load tables... ");
	omStatusBar.SetPaneText(0, olText);
	omStatusBar.UpdateWindow();

 

	TRACE("\n ANZ FLIGHT: %d\n", ogCcaDiaFlightData.omData.GetSize());
	TRACE("\n ANZ Cha   : %d\n", ogCcaDiaFlightData.omChaData.omData.GetSize());
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");

/*
	int ilBlk = 0;
	int ilFlight = 0;
	CHADATA *prlCha;

	for(int g = ogCcaDiaFlightData.omChaData.omData.GetSize() -1 ; g >= 0; g-- )
	{
		prlCha = &ogCcaDiaFlightData.omChaData.omData[g];

		if(CString(prlCha->Ctyp) == "")
			ilFlight++;

		if(CString(prlCha->Ctyp) == "N")
			ilBlk++;

	}

	TRACE("\n ANZ FLIGHTCha: %d\n", ilFlight);
	TRACE("\n ANZ BLKCha   : %d\n", ilBlk);
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
*/
	olText = CString("Check overlapping... ");
	omStatusBar.SetPaneText(0, olText);
	omStatusBar.UpdateWindow();
	
	TRACE("\nBegin Overlap:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	int ilCount = ogBCD.GetDataCount("CHU");

	/*
	for(int i = 0; i < ilCount; i++)
	{
		ogDataSet.CheckChaForOverlapping(ogBCD.GetField("CHU", i, "CNAM"));
	}
	*/
	TRACE("\nEnd Overlap:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");




	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	omStatusBar.SetPaneText(0, "");
	omStatusBar.UpdateWindow();

	bmReadAll = false;

}

void ChaDiagram::OnDestroy() 
{
	SaveToReg();

//	if (bgModal == TRUE)
//		return;
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	ogDdx.UnRegister(&omViewer, NOTUSED);

	// Unregister DDX call back function
	TRACE("ChaDiagram: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogDdx.UnRegister(this, NOTUSED);

	CFrameWnd::OnDestroy();
}

void ChaDiagram::OnClose() 
{
    // Ignore close -- This makes Alt-F4 keys no effect
	CFrameWnd::OnClose();
}

void ChaDiagram::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
    // TODO: Add your message handler code here and/or call default
    CFrameWnd::OnGetMinMaxInfo(lpMMI);

    lpMMI->ptMaxTrackSize = omMaxTrackSize;
    lpMMI->ptMinTrackSize = omMinTrackSize;
}

BOOL ChaDiagram::OnEraseBkgnd(CDC* pDC) 
{
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);

    CBrush olBrush(SILVER/*lmBkColor*/);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
    
    //return CFrameWnd::OnEraseBkgnd(pDC);
}

void ChaDiagram::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CRect olRect;
    GetClientRect(&olRect);
    
    // draw horizontal line
    CPen olHPen(PS_SOLID, 1, lmHilightColor);
    CPen *polOldPen = dc.SelectObject(&olHPen);
    dc.MoveTo(olRect.left, 27); dc.LineTo(olRect.right, 27);
    dc.MoveTo(olRect.left, 62); dc.LineTo(olRect.right, 62);

    CPen olTPen(PS_SOLID, 1, lmTextColor);
    dc.SelectObject(&olTPen);
    dc.MoveTo(olRect.left, 63); dc.LineTo(olRect.right, 63);
    
    
    // draw vertical line
    dc.SelectObject(&olTPen);
    dc.MoveTo(imStartTimeScalePos - 2, 27);
    dc.LineTo(imStartTimeScalePos - 2, 63);

    dc.SelectObject(&olHPen);
    dc.MoveTo(imStartTimeScalePos - 1, 27);
    dc.LineTo(imStartTimeScalePos - 1, 63);

    dc.SelectObject(polOldPen);
    // Do not call CFrameWnd::OnPaint() for painting messages
}


void ChaDiagram::OnSize(UINT nType, int cx, int cy) 
{
    CFrameWnd::OnSize(nType, cx, cy);
    
    // TODO: Add your message handler code here
    CRect olRect; GetClientRect(&olRect);

    CRect olBB1Rect(olRect.right - 36, 27 + 5, olRect.right - 19, (27 + 5) + 25);
    omBB1.MoveWindow(&olBB1Rect, TRUE);
    
    CRect olBB2Rect(olRect.right - 19, 27 + 5, olRect.right - 2, (27 + 5) + 25);
    omBB2.MoveWindow(&olBB2Rect, TRUE);

    CRect olTSRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62);
    omTimeScale.MoveWindow(&olTSRect, TRUE);

    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    // LeftTop, RightBottom
    omClientWnd.MoveWindow(&olRect, TRUE);

	PositionChild();
	//ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
//	SetAllStaffAreaButtonsColor();

	GetClientRect(&olRect);
	
	omTime.MoveWindow( CRect(olRect.right - 192, 3, olRect.right - 90, 22));

    omDate.MoveWindow( CRect(olRect.right - 88, 3, olRect.right - 8, 22));


}

void ChaDiagram::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
    // TODO: Add your message handler code here and/or call default
    
    //CFrameWnd::OnHScroll(nSBCode, nPos, pScrollBar);


	CTime olTSStartTime = omTSStartTime;
 	int ilCurrLine = pomChart->omGantt.GetTopIndex();
    long llTotalMin;
    int ilPos;
    
    switch (nSBCode)
    {
        case SB_LINEUP :
            //OutputDebugString("LineUp\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ) - int (60 * 1000L / llTotalMin);
            if (ilPos <= 0)
            {
                ilPos = 0;
                SetTSStartTime(omStartTime);
            }
            else
                SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));

			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
            omTimeScale.Invalidate(TRUE);
            SetScrollPos(SB_HORZ, ilPos, TRUE);
            omClientWnd.Invalidate(FALSE);
        break;
        
        case SB_LINEDOWN :
            //OutputDebugString("LineDown\n\r");

            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ) + int (60 * 1000L / llTotalMin);
            if (ilPos >= 1000)
            {
                ilPos = 1000;
                SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin+1), 0));
            }
            else
                SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));

			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
            omTimeScale.Invalidate(TRUE);
			SetScrollPos(SB_HORZ, ilPos, TRUE);
            omClientWnd.Invalidate(FALSE);
        break;
        
        case SB_PAGEUP :
            //OutputDebugString("PageUp\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ)
                - int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
            if (ilPos <= 0)
            {
                ilPos = 0;
                SetTSStartTime(omStartTime);
            }
            else
                SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
            omTimeScale.Invalidate(TRUE);
            SetScrollPos(SB_HORZ, ilPos, TRUE);
            omClientWnd.Invalidate(FALSE);
        break;
        
        case SB_PAGEDOWN :
            //OutputDebugString("PageDown\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ)
                + int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
            if (ilPos >= 1000)
            {
                ilPos = 1000;
                SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin+1), 0));
            }
            else
                SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
            omTimeScale.Invalidate(TRUE);
            SetScrollPos(SB_HORZ, ilPos, TRUE);
            omClientWnd.Invalidate(FALSE);
        break;
        
        case SB_THUMBTRACK /* pressed, any drag time */:
            //OutputDebugString("ThumbTrack\n\r");

            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
            
            SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin * nPos / 1000), 0));
            
			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
            omTimeScale.Invalidate(TRUE);
            SetScrollPos(SB_HORZ, nPos, TRUE);
            omClientWnd.Invalidate(FALSE);
        break;
        case SB_THUMBPOSITION:	// the thumb was just released?
            omClientWnd.Invalidate(FALSE);
        case SB_TOP :
        //break;
        case SB_BOTTOM :
            //OutputDebugString("TopBottom\n\r");
        break;
        
        case SB_ENDSCROLL :
            //OutputDebugString("EndScroll\n\r");
            //OutputDebugString("\n\r");
        break;
    }

 	pomChart->omGantt.SetTopIndex(ilCurrLine);

}


void ChaDiagram::OnTimer(UINT nIDEvent)
{
	if (bmTimeLine == false)
		return;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	if (nIDEvent == 1)
	{
		CTime olCurr1 = CTime::GetCurrentTime();

		/*
		// update cic, current time is set in check overlapping
		int ilCount = ogBCD.GetDataCount("CHU");


		for(int i = 0; i < ilCount; i++)
		{
			ogDataSet.CheckChaForOverlapping(ogBCD.GetField("CHU", i, "CNAM"));
		}
		*/
	
		pogChaDiagram->PostMessage(WM_REPAINT_ALL, 0, 0);

		CTime olCurr2 = CTime::GetCurrentTime();

		TRACE("ChaDiagram::OnTimer: Duration of FollowTimeLine-Calculation: %s\n", (olCurr2 - olCurr1).Format("%M:%S"));
	}


	//// Update time line
	if(bmNoUpdatesNow == FALSE)
	{
		CTime olLocalTime = CTime::GetCurrentTime();
		CTime olUtcTime = CTime::GetCurrentTime();
		ogBasicData.LocalToUtc(olUtcTime);

		char pclTimes[100];
		char pclDate[100];
		


		CTime olUtcTimeTmp = CTime::GetCurrentTime();
		olUtcTimeTmp -= ogUtcDiff;
		
		if(ogUtcDiff != 0)
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
		}
		else
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
		}

		if(bgGatPosLocal)
			omTimeScale.UpdateCurrentTimeLine(olLocalTime);
		else
			omTimeScale.UpdateCurrentTimeLine(olUtcTime);

 
		omTime.SetWindowText(pclTimes);
		omTime.Invalidate(FALSE);

		omDate.SetWindowText(pclDate);
		omDate.Invalidate(FALSE);

		if(bgGatPosLocal)
			pomChart->GetGanttPtr()->SetCurrentTime(olLocalTime);
		else
			pomChart->GetGanttPtr()->SetCurrentTime(olUtcTime);

		CFrameWnd::OnTimer(nIDEvent);
	}

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}

void ChaDiagram::SetMarkTime(CTime opStartTime, CTime opEndTime)
{
		pomChart->SetMarkTime(opStartTime, opEndTime);
}

void ChaDiagram::ShowChaDuration(CTime opCkbs, CTime opCkes)
{
	CTime olTime = opCkbs - CTimeSpan(0, 1, 0, 0);
	ShowTime(olTime);
	SetMarkTime(opCkbs, opCkes);
}

void ChaDiagram::ShowTime(const CTime &ropTime) 
{
	SetTSStartTime(ropTime);
	//TRACE("Time: [%s]\n", olTime.Format("%H:%M"));

	CTime olTSStartTime(omTSStartTime);

	omTimeScale.SetDisplayStartTime(olTSStartTime);
    omTimeScale.Invalidate(TRUE);
		
	// update scroll bar position
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);

	//ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);

	CRect olRect;
	omTimeScale.GetClientRect(&olRect);
	omViewer.ChangeViewTo(omViewer.SelectView(), omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
	PositionChild();

    omClientWnd.Invalidate(FALSE);
}

LONG ChaDiagram::OnPositionChild(WPARAM wParam, LPARAM lParam)
{
    PositionChild();
    return 0L;
}

LONG ChaDiagram::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{

    CString olStr;

    int ilGroupNo = HIWORD(lParam);
    int ilLineNo = LOWORD(lParam);

	int ilItemID = omViewer.GetItemID(ilGroupNo, ilLineNo);

	CTime olTime = CTime((time_t) lParam);

	char clBuf[255];
	int ilCount;

    ChaGantt *polChaGantt = pomChart -> GetGanttPtr();
    switch (wParam)
    {
        // group message

        case UD_UPDATEGROUP :
            olStr = omViewer.GetGroupText(ilGroupNo);
            pomChart->GetChartButtonPtr()->SetWindowText(olStr);
            pomChart->GetChartButtonPtr()->Invalidate(FALSE);

            olStr = omViewer.GetGroupTopScaleText(ilGroupNo);
            pomChart->GetTopScaleTextPtr()->SetWindowText(olStr);
            pomChart->GetTopScaleTextPtr()->Invalidate(TRUE);
			//SetStaffAreaButtonColor(ilGroupNo);
        break;
        

        // line message
        case UD_INSERTLINE :
            polChaGantt->InsertString(ilItemID, "");
			polChaGantt->RepaintItemHeight(ilItemID);
			
			ilCount = omViewer.GetLineCount(ilGroupNo);
			sprintf(clBuf, "%d", ilCount);
			pomChart->GetCountTextPtr()->SetWindowText(clBuf);
			pomChart->GetCountTextPtr()->Invalidate(TRUE);

			//SetStaffAreaButtonColor(ilGroupNo);
            PositionChild();
        break;
        
        case UD_UPDATELINE :
            polChaGantt->RepaintVerticalScale(ilItemID);
            polChaGantt->RepaintGanttChart(ilItemID);
			//SetStaffAreaButtonColor(ilGroupNo);
        break;

        case UD_DELETELINE :
            polChaGantt->DeleteString(ilItemID);
            
			ilCount = omViewer.GetLineCount(ilGroupNo);
			sprintf(clBuf, "%d", ilCount);
			pomChart->GetCountTextPtr()->SetWindowText(clBuf);
			pomChart->GetCountTextPtr()->Invalidate(TRUE);

			//SetStaffAreaButtonColor(ilGroupNo);
			PositionChild();
        break;

        case UD_UPDATELINEHEIGHT :
            polChaGantt->RepaintItemHeight(ilItemID);
			//SetStaffAreaButtonColor(ilGroupNo);
            PositionChild();
		break;
    }

    return 0L;
}


///////////////////////////////////////////////////////////////////////////////
// Damkerng and Pichet: Date: 5 July 1995

void ChaDiagram::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
	polCB->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{

		olViewName = omViewer.SelectView();//ogCfgData.rmUserSetup.STCV;
	}

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}
}





void ChaDiagram::ChangeViewTo(const char *pcpViewName,bool RememberPositions)
{
	
	//PRF 8363
	SetFocusToDiagram();


	if (bgNoScroll == TRUE)
		return;

	if (pomChart != NULL)
	{
		if (!pomChart->omGantt.bmRepaint)
		{
			bmRepaintAll = true;
			return;
		}
    }
			

	int ilDwRef = m_dwRef;
	AfxGetApp()->DoWaitCursor(1);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	CCSPtrArray <int> olChartStates;
	olChartStates.RemoveAll();

	CRect olRect; omTimeScale.GetClientRect(&olRect);

//rkr25042001
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
//    omViewer.ChangeViewTo(pcpViewName, omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
    omViewer.ChangeViewTo(pcpViewName, olTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));

	int ilChartState;
	int ilTopIndex;
 	if (RememberPositions == TRUE && pomChart != NULL)
	{
		ilChartState = pomChart->GetState();
		if (pomChart->GetGanttPtr() != NULL)
			ilTopIndex = pomChart->GetGanttPtr()->GetTopIndex();
	}


    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
	if (RememberPositions == FALSE)
	{
	    imFirstVisibleChart = 0;
	}
    
	int ilLastY = 0;

	pomChart->DestroyWindow();
    omClientWnd.GetClientRect(&olRect);
        
	olRect.SetRect(olRect.left, 0, olRect.right, 0);
        
    pomChart = new ChaChart;
    pomChart->SetTimeScale(&omTimeScale);
    pomChart->SetViewer(&omViewer);
    pomChart->SetStatusBar(&omStatusBar);
    pomChart->SetStartTime(omTSStartTime);
    pomChart->SetInterval(omTimeScale.GetDisplayDuration());
 	if (RememberPositions == TRUE)
	{
		pomChart->SetState(ilChartState);
	}
	else
	{
		pomChart->SetState(Maximized);
	}

    pomChart->Create(NULL, "ChaChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
            olRect, &omClientWnd, 0 /* IDD_CHART */, NULL);


 	if (RememberPositions == TRUE)
	{
		if (pomChart->GetGanttPtr() != NULL)
 			pomChart->GetGanttPtr()->SetTopIndex(ilTopIndex);
	}


	CTime olCurr = CTime::GetCurrentTime();
	//Die wollen local
 	if(!bgGatPosLocal)
		ogBasicData.LocalToUtc(olCurr);
	if (pomChart->GetGanttPtr() != NULL)
	{
 			pomChart->GetGanttPtr()->SetTopIndex(ilTopIndex);
			pomChart->GetGanttPtr()->SetCurrentTime(olCurr);
//			pomChart->GetGanttPtr()->SetFonts(igFontIndex1, igFontIndex2);

	}
        
 
	PositionChild();
	AfxGetApp()->DoWaitCursor(-1);
	bmNoUpdatesNow = FALSE;
	omViewer.AllowUpdates(bmNoUpdatesNow);


}






void ChaDiagram::OnViewSelChange()
{

	SetFocusToDiagram(); //PRF 8363


    char clText[64];
    CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
    polCB->GetLBText(polCB->GetCurSel(), clText);
	if(strcmp(clText, "<Default>") == 0)
		return;


	LoadFlights(clText);

	
	ogDdx.DataChanged((void *)this, CCA_VIEW_CHANGED,(void *)clText );
	
	
}


void ChaDiagram::OnCloseupView() 
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//MWO
	//SetFocus();
	//END MWO
}

void ChaDiagram::OnZeit()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//MWO
	//SetFocus();
	//END MWO

	// set new start time
	CTime olTime = CTime::GetCurrentTime();
		ogBasicData.LocalToUtc(olTime);
	olTime -= CTimeSpan(0, 1, 0, 0);
	SetTSStartTime(olTime);

	// adjust timescale
	olTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTime);
    omTimeScale.SetDisplayStartTime(olTime);
	//omTimeScale.SetDisplayStartTime(omTSStartTime);
    omTimeScale.Invalidate(TRUE);
		
	// update scroll bar position
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);

	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
    omClientWnd.Invalidate(FALSE);
}


BOOL ChaDiagram::DestroyWindow() 
{

	if ((bmIsViewOpen))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}
	pogChaDiagram = NULL; 

	ogDdx.UnRegister(&omViewer, NOTUSED);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	BOOL blRc = CWnd::DestroyWindow();
	return blRc;
}

/////////XXXXX
////////////////////////////////////////////////////////////////////////
// ChaDiagram keyboard handling

void ChaDiagram::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// check if the control key is pressed
    BOOL blIsControl = ::GetKeyState(VK_CONTROL) & 0x8080;
		// This statement has to be fixed for using both in Windows 3.11 and NT.
		// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
		// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.

	switch (nChar) 
	{
	case VK_LEFT:
		OnHScroll(blIsControl? SB_PAGEUP: SB_LINEUP, 0, NULL);
		break;
	case VK_RIGHT:
		OnHScroll(blIsControl? SB_PAGEDOWN: SB_LINEDOWN, 0, NULL);
		break;
	case VK_HOME:
		SetScrollPos(SB_HORZ, 0, FALSE);
		OnHScroll(SB_LINEUP, 0, NULL);
		break;
	case VK_END:
		SetScrollPos(SB_HORZ, 1000, FALSE);
		OnHScroll(SB_LINEDOWN, 0, NULL);
		break;
	default:
		omDialogBar.SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
		CFrameWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		break;
	}
}

void ChaDiagram::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CFrameWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}

////////////////////////////////////////////////////////////////////////
// ChaDiagram -- implementation of DDX call back function

static void ChaDiagramCf(void *popInstance, enum enumDDXTypes ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	ChaDiagram *polDiagram = (ChaDiagram *)popInstance;

	TIMEPACKET *polTimePacket;

	switch (ipDDXType)
	{
	case CCA_VIEW_CHANGED:
        polDiagram->ViewSelChange( (char *) vpDataPointer);
		break;
	case CCA_GET_CUR_VIEW:
		polDiagram->GetCurView((char *) vpDataPointer);
		break;
	case REDISPLAY_ALL:
		polDiagram->RedisplayAll();
		break;
	case STAFFDIAGRAM_UPDATETIMEBAND:
		polTimePacket = (TIMEPACKET *)vpDataPointer;
		polDiagram->SetTimeBand(polTimePacket->StartTime, polTimePacket->EndTime);
		polDiagram->UpdateTimeBand();
		break;

	}
}

void ChaDiagram::RedisplayAll()
{
	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
}

void ChaDiagram::SetTimeBand(CTime opStartTime, CTime opEndTime)
{
	omTimeBandStartTime = opStartTime;
	omTimeBandEndTime = opEndTime;
}

void ChaDiagram::UpdateTimeBand()
{
	pomChart->omGantt.SetMarkTime(omTimeBandStartTime, omTimeBandEndTime);
}

LONG ChaDiagram::RepaintAll(WPARAM wParam, LPARAM lParam)
{
	ChangeViewTo(omViewer.GetViewName(), true);
	return 0L;
}







void ChaDiagram::OnPrintGantt()
{
	omViewer.PrintGantt(pomChart);
}






void ChaDiagram::SetWndPos()
{
	CRect olRect;

	GetWindowRect(&olRect);

	int ilWidth = olRect.right -olRect.left;
	int ilWhichMonitor = ogCfgData.GetMonitorForWindow(CString(MON_CHADIA_STRING));
	int ilMonitors = ogCfgData.GetMonitorForWindow(CString(MON_COUNT_STRING));
	int left, top, right, bottom;
	int ilCXMonitor;

	bottom = ::GetSystemMetrics(SM_CYSCREEN);

	top = 0;  


	imWhichMonitor = ilWhichMonitor;
	imMonitorCount = ilMonitors;

	if(ilWhichMonitor == 1)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 2)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 3)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(2*ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}

	MoveWindow(CRect(left, top, right, ::GetSystemMetrics(SM_CYSCREEN)));


	//SetWindowPos(&wndTop,left , top, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE);



}




void ChaDiagram::ActivateTables()
{

}


bool ChaDiagram::GetLoadedCha(CCSPtrArray<CHADATA> &opCha, CMapStringToString& opTypMap, bool bpOnlySel, bool bpMustBeInOneDay)
{
	return omViewer.GetLoadedCha(opCha, opTypMap, bpOnlySel, bpMustBeInOneDay);
}

void ChaDiagram::SetFocusToDiagram()
{
	//PRF 8363, Change the focus to gantt chart sisl//
	ChaGantt *polChaGantt = pomChart -> GetGanttPtr();
	if((polChaGantt != NULL)  && CWnd::GetForegroundWindow() == this)
		polChaGantt->SetFocus();

}


void ChaDiagram::ViewSelChange(char *pcpView)
{

	omViewer.SelectView(pcpView);

	
	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
	polCB->SetCurSel(polCB->FindString( 0, pcpView ) );

	UpdateDia();
	//PRF 8363
	SetFocusToDiagram();
}



void ChaDiagram::GetCurView( char * pcpView)
{
	strcpy(pcpView, omViewer.GetViewName());
}



bool ChaDiagram::UpdateDia()
{

	CString olViewName = omViewer.GetViewName();
	
	
	omViewer.SelectView(olViewName);
	omViewer.MakeMasstab();
	CTime olT = omViewer.GetGeometrieStartTime();


	CTime olFrom;
	CTime olTo;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);

	//if((olFrom > olT) || (olTo < olT))
	//	olT = olFrom;

	CString olS = olT.Format("%d.%m.%Y-%H:%M");
	SetTSStartTime(olT);
	omTSDuration = omViewer.GetGeometryTimeSpan();

    
	long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);
	
//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
    
	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
//	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
    omTimeScale.Invalidate(TRUE);
	SetTSStartTime(omTSStartTime);
	ChangeViewTo(olViewName, false);

	ActivateTables();

	//ogDdx.DataChanged((void *)this, CCA_VIEW_CHANGED,(void *)olViewName.GetBuffer(0) );

	SetFocusToDiagram(); //PRF 8363
	
	
	
	
	
////////////////////////////////	
	
	return true;
}




