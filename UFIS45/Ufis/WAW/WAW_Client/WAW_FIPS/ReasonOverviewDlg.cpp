// ReasonOverviewDlg.cpp : implementation file
//
// Modification History: 
//	161100	rkr	Dialog nach Vorgaben PRF Athen umgestellt
//	171100	rkr	Dialog unabhängig von Datenstruktur

#include <stdafx.h>
#include <fpms.h>
#include <CCSDdx.h>
#include <CCSTime.h>
#include <CCSGlobl.h>
#include <RotationDlgCedaFlightData.h>
#include <ButtonListDlg.h>
#include <PrivList.h>
#include <CedaCraData.h>
#include <CedaBasicData.h>
#include <ReasonOverviewDlg.h>
#include <AskBox.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


static int CompareReasonCdat(const CRADATA **e1, const CRADATA **e2); 
static int CompareReasonCdat(const CRADATA **e1, const CRADATA **e2); 


/////////////////////////////////////////////////////////////////////////////
// ReasonOverviewDlg dialog

ReasonOverviewDlg::ReasonOverviewDlg(CWnd* pParent, long lpFurn)
	           :CDialog(ReasonOverviewDlg::IDD, pParent)

{
	//{{AFX_DATA_INIT(ReasonOverviewDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	pomParent = pParent;
	pomTable = new CCSTable;
	lmFurn = lpFurn;

}


ReasonOverviewDlg::~ReasonOverviewDlg()
{

	delete pomTable;
}


void ReasonOverviewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ReasonOverviewDlg)
	DDX_Control(pDX, IDC_GMBORDER2, m_CS_GMBorder);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ReasonOverviewDlg, CDialog)
	//{{AFX_MSG_MAP(ReasonOverviewDlg)
	ON_BN_CLICKED(IDC_CLOSE, OnClose)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ReasonOverviewDlg message handlers
BOOL ReasonOverviewDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	DrawHeader();

	if(!FillTableLine())
		OnClose();



	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void ReasonOverviewDlg::InitDialog()
{

	DrawHeader();
	
}

void ReasonOverviewDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);

	m_resizeHelper.OnSize();

	if (this->pomTable != NULL && ::IsWindow(this->pomTable->m_hWnd))
	{
		CRect olrectTable;
		m_CS_GMBorder.GetWindowRect(&olrectTable);
		ScreenToClient(olrectTable);
		olrectTable.DeflateRect(2,2);     // hiding the CTable window border
		this->pomTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);

		this->pomTable->DisplayTable();
	}
	this->Invalidate();
}

void ReasonOverviewDlg::OnClose() 
{

	// TODO: Add your control notification handler code here
	CCS_TRY

	CDialog::OnCancel();

	CCS_CATCH_ALL
	
}


void ReasonOverviewDlg::OnCancel()
{

	CDialog::OnCancel();

}


void ReasonOverviewDlg::DrawHeader()
{

	CCS_TRY

	CRect olRectBorder;
	m_CS_GMBorder.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);


	pomTable->SetHeaderSpacing(0);
    pomTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);



	int  ilTotalLines = 0;
	bool blNewLogicLine = false;

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;
	TABLE_HEADER_COLUMN *prlHeader[8];
	int ilFontFactor=9;

	// resourcetype (PSTA,GTA1 etc)
	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 8*ilFontFactor;
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING2812);
	//old value
	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 8*ilFontFactor;
	prlHeader[1]->Font = &ogCourier_Bold_10;
	prlHeader[1]->Text = GetString(IDS_STRING2814);
	//new value	
	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 8*ilFontFactor;
	prlHeader[2]->Font = &ogCourier_Bold_10;
	prlHeader[2]->Text = GetString(IDS_STRING2815);
	//reason code
	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 5*ilFontFactor;
	prlHeader[3]->Font = &ogCourier_Bold_10;
	prlHeader[3]->Text = GetString(IDS_STRING2816);
	//Reason text
	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 30*ilFontFactor;
	prlHeader[4]->Font = &ogCourier_Bold_10;
	prlHeader[4]->Text = GetString(IDS_STRING2817);
	//User 
	prlHeader[5] = new TABLE_HEADER_COLUMN;
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 15*ilFontFactor;
	prlHeader[5]->Font = &ogCourier_Bold_10;
	prlHeader[5]->Text = GetString(IDS_STRING2818);
	//time of change
	prlHeader[6] = new TABLE_HEADER_COLUMN;
	prlHeader[6]->Alignment = COLALIGN_CENTER;
	prlHeader[6]->Length = 16*ilFontFactor;
	prlHeader[6]->Font = &ogCourier_Bold_10;
	prlHeader[6]->Text = GetString(IDS_STRING2813);
	


	for(int ili = 0; ili < 7; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(false);

	pomTable->DisplayTable();

	CCS_CATCH_ALL
	
}


bool ReasonOverviewDlg::FillTableLine()
{

	CCS_TRY

	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.Alignment = COLALIGN_LEFT;

	CCSEDIT_ATTRIB rlAttrib;
	rlAttrib.Style = ES_UPPERCASE;

	
	CCSPtrArray<CRADATA> olList;
	CString pspWhere;
	char pclSelection[20];
	char pclText[256];
	CString olCode;
	CString olRema;
	
	CTime olTime;

	pspWhere.Format("WHERE FURN = '%d' ",lmFurn);
	ogCraData.ReadSpecial(olList, pspWhere.GetBuffer(0));
	


	CRADATA polCraData;
	CString olrDisplay;

	if(olList.GetSize() == 0)
	{
		ShowWindow(SW_HIDE);
		MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING1930), MB_ICONEXCLAMATION | MB_OK);
		OnCancel();

	}


	olList.Sort(CompareReasonCdat);


	for(int i = 0 ; i < olList.GetSize() ; i++)
	{
		CString olDisplay;
		polCraData = olList.GetAt(i);

		
		sprintf(pclSelection, "%ld", polCraData.Curn);

		ogBCD.GetField("CRC", "URNO", CString(pclSelection), "CODE", olCode);
		ogBCD.GetField("CRC", "URNO", CString(pclSelection), "REMA", olRema);


		

		sprintf(pclText,"%s:4: %s:5->%s:5  - %s:5-%s:30- %s:15-%s\n",polCraData.Aloc,
														polCraData.Oval,
														polCraData.Nval,
														olCode,
														olRema,
														polCraData.Usec,
														polCraData.Cdat.Format("%d.%m.%Y  %H:%M"));
		

		rlColumnData.Text = polCraData.Aloc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = polCraData.Oval;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = polCraData.Nval;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = olCode;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = olRema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = polCraData.Usec;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		olTime = polCraData.Cdat;
		
		if(bgGatPosLocal)			
			ogBasicData.UtcToLocal(olTime);

		rlColumnData.Text = olTime.Format("%d.%m.%Y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		pomTable->AddTextLine(olColList, NULL);
		olColList.DeleteAll();
	}

	pomTable->DisplayTable();

	CCS_CATCH_ALL

	return true;

}


static int CompareReasonCdat(const CRADATA **e1, const CRADATA **e2)
{
	return ((**e1).Cdat >  (**e2).Cdat)? 1: -1;
}


