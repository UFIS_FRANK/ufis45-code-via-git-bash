// ReportSameREGNTableViewer.cpp : implementation file
// 
// List of all flights that the same AC-Type
// Angabe eines AC-Typen

#include <stdafx.h>
#include <ReportSameREGNTableViewer.h>
#include <CcsGlobl.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <BasicData.h>
#include <resrc1.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// ReportSameREGNTableViewer
//

int ReportSameREGNTableViewer::GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight)
{
	opVias->DeleteAll();

	if(prpFlight == NULL)
		return 0;
	if(strlen(prpFlight->Vial) == 0)
		return 0;

	CString olVias(prpFlight->Vial);
	CString olFids;
	CString olApc3;
	CString olApc4;

	VIADATA *prlVia;

	while(olVias.IsEmpty() != TRUE)
	{
			prlVia = new VIADATA;
			opVias->Add(prlVia);

			if(olVias.GetLength() < 120)
			{
				olVias += "                                                                                                                             ";
				olVias = olVias.Left(120);
			}


			olApc3 = olVias.Mid(1,3);
			olApc3.TrimLeft();
			sprintf(prlVia->Apc3, olApc3);

			if(olVias.GetLength() >= 120)
				olVias = olVias.Right(olVias.GetLength() - 120);

	}
	return opVias->GetSize();
}



ReportSameREGNTableViewer::ReportSameREGNTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo, char *pcpSelect)
{
	pomData = popData;
	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;


	bmIsFromSearch = false;
    pomTable = NULL;
}

ReportSameREGNTableViewer::~ReportSameREGNTableViewer()
{
	omPrintHeadHeaderArray.DeleteAll();
    DeleteAll();
}


void ReportSameREGNTableViewer::SetParentDlg(CDialog* ppParentDlg)
{

	pomParentDlg = ppParentDlg;

}


void ReportSameREGNTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}



void ReportSameREGNTableViewer::ChangeViewTo(const char *pcpViewName)
{

    pomTable->ResetContent();
    DeleteAll();    
    MakeLines();

	UpdateDisplay();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}


/////////////////////////////////////////////////////////////////////////////
// ReportSameREGNTableViewer -- code specific to this class

void ReportSameREGNTableViewer::MakeLines()
{
	ROTATIONDLGFLIGHTDATA *prlAFlight;
	ROTATIONDLGFLIGHTDATA *prlDFlight;
	ROTATIONDLGFLIGHTDATA *prlNextFlight = NULL;
	ROTATIONDLGFLIGHTDATA *prlFlight;
	int ilLineNo;

	bool blRDeparture = false;

	int ilFlightCount = pomData->GetSize();

	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &(*pomData)[ilLc];

		if(ilLc + 1 < ilFlightCount)
			prlNextFlight = &(*pomData)[ilLc + 1];	
		else
			prlNextFlight = NULL;	

		//Arrival
		if((strcmp(prlFlight->Org3, pcgHome) != 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
		{
			prlAFlight = prlFlight;
			prlDFlight = NULL;
			if(prlNextFlight != NULL)
			{
				if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
				{
					prlDFlight = prlNextFlight;

					if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
					{
						ilLc++;
					}
					else
						blRDeparture = true;
				}
			}
		}
		else
		{
			// Departure
			if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) != 0))
			{
				prlAFlight = NULL;
				prlDFlight = prlFlight;
				blRDeparture = false;
			}
			else
			{
				//Turnaround
				if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
				{
					if(blRDeparture)
					{
						blRDeparture = false;
						prlAFlight = prlFlight;
						prlDFlight = NULL;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								prlDFlight = prlNextFlight;
								if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
								{
									ilLc++;
								}
								else
									blRDeparture = true;
							}
						}
					}
					else
					{
						prlAFlight		= prlFlight;
						prlDFlight		= prlFlight;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								ilLc--;
								prlAFlight	 = NULL;
								blRDeparture = true;
							}
						}
					}
				}
			}
		}
		ilLineNo = MakeLine(prlAFlight, prlDFlight);

	}

//generate the headerinformation
	//calculate the numbers of ARR and DEP
	imArr = 0;
	imDep = 0;
	for(int j = 0; j < omLines.GetSize(); j++ ) 
	{
		SAMEREGNTABLE_LINEDATA rlLine = omLines[j];
		if (!rlLine.AFlno.IsEmpty())
			imArr++;
		if (!rlLine.DFlno.IsEmpty())
			imDep++;
	}

	CString olTimeSet = GetString(IDS_STRING1920); //UTC
	if(bgReportLocal)
		olTimeSet = GetString(IDS_STRING1921); //LOCAL

	char pclHeader[256];
	sprintf(pclHeader, GetString(IDS_R_HEADER1), GetString(REPORTS_RB_FlightsByRegistration), pcmSelect, pcmInfo, imArr+imDep, imArr, imDep, olTimeSet);
	if(bgReports)
	sprintf(pclHeader, "%s%s with %s from %s (Flights: %d / ARR: %d / DEP: %d) - %s",ogPrefixReports, GetString(REPORTS_RB_FlightsByRegistration), pcmSelect, pcmInfo, imArr+imDep, imArr, imDep, olTimeSet);
	omTableName = pclHeader;

}

		

int ReportSameREGNTableViewer::MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
{
    SAMEREGNTABLE_LINEDATA rlLine;

	if((prpAFlight != NULL) && (prpDFlight != NULL) )
	{
		// Ankunft und Abflug getrennte Zeilen
		MakeLineData(prpAFlight, NULL, rlLine);
		CreateLine(rlLine);
		MakeLineData(NULL, prpDFlight, rlLine);
		return CreateLine(rlLine);
	}
	if (prpAFlight != NULL)
	{
		MakeLineData(prpAFlight, NULL, rlLine);
		return CreateLine(rlLine);
	}
	if (prpDFlight != NULL)
	{
		MakeLineData(NULL, prpDFlight, rlLine);
		return CreateLine(rlLine);
	}
	return -1;
}




void ReportSameREGNTableViewer::MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, SAMEREGNTABLE_LINEDATA &rpLine)
{

	CString olStr;
	if(prpAFlight != NULL)
	{
		rpLine.Adid = "A";
		rpLine.AUrno =  prpAFlight->Urno;
		rpLine.ARkey =  prpAFlight->Rkey;
		rpLine.AFlno = CString(prpAFlight->Flno);
		rpLine.AStoa = prpAFlight->Stoa; 
		if(bgReportLocal)
			ogBasicData.UtcToLocal(rpLine.AStoa);
		rpLine.ADate = rpLine.AStoa.Format("%d.%m.%y");
		rpLine.AOrg3 = CString(prpAFlight->Org3);
		rpLine.AOrg4 = CString(prpAFlight->Org4);
		rpLine.AAct  = CString(prpAFlight->Act3);
		rpLine.ARegn = CString(prpAFlight->Regn);
		rpLine.ALand = CTime(prpAFlight->Land); 
		if(bgReportLocal) 
			ogBasicData.UtcToLocal(rpLine.ALand);
		if(bgReports)
		{
			rpLine.APsta=prpAFlight->Psta;
			rpLine.Ttyp=prpAFlight->Ttyp;
			rpLine.Regn=prpAFlight->Regn;
		}	

		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prpAFlight);

		if(opVias.GetSize() == 0)
			rpLine.AVia3 = "";
		else
		{
			rpLine.AVia3 =  opVias[ilViaCnt-1].Apc3;
		}
		opVias.DeleteAll();
	}
	else
	{

		rpLine.AUrno =  0;
		rpLine.ARkey =  0;
		rpLine.AFlno = "";
		rpLine.AStoa = TIMENULL; 
		rpLine.ALand = TIMENULL; 
		rpLine.AEtai = TIMENULL; 
		rpLine.ARegn = "";
		rpLine.ADate = ""; 
		rpLine.AOrg3 = "";
		rpLine.AOrg4 = "";
		rpLine.AVia3 = "";
		rpLine.AAct = "";
	}


	if(prpDFlight != NULL)
	{
		rpLine.Adid = "D";

		rpLine.DUrno =  prpDFlight->Urno;
		rpLine.DRkey =  prpDFlight->Rkey;
		rpLine.DFlno = CString(prpDFlight->Flno); 
		rpLine.DDes3 = CString(prpDFlight->Des3);
		rpLine.DDes4 = CString(prpDFlight->Des4);
		rpLine.DAct  = CString(prpDFlight->Act3);
		rpLine.DRegn = CString(prpDFlight->Regn);

		rpLine.DAirb = CTime(prpDFlight->Airb);
		if(bgReportLocal) 
			ogBasicData.UtcToLocal(rpLine.DAirb);
		rpLine.DEtdi = CTime(prpDFlight->Etdi);
		if(bgReportLocal) 
			ogBasicData.UtcToLocal(rpLine.DEtdi);
		rpLine.DStod = prpDFlight->Stod;
		if(bgReportLocal)
			ogBasicData.UtcToLocal(rpLine.DStod);
		rpLine.DDate = rpLine.DStod.Format("%d.%m.%y");
		rpLine.DVian = prpDFlight->Vian;

		if (rpLine.AAct.IsEmpty())
			rpLine.AAct = rpLine.DAct;
		if(bgReports)
		{
			rpLine.DPstd=prpDFlight->Pstd;
			rpLine.Ttyp=prpDFlight->Ttyp;
			rpLine.Regn=prpDFlight->Regn;
		}


		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prpDFlight);

		if(opVias.GetSize() == 0)
			rpLine.DVia3 = "";
		else
			rpLine.DVia3 =  opVias[0].Apc3;	// erster Via nach Homeairport = erste Zeile in Vial
		opVias.DeleteAll();

	
	}
	else
	{
		rpLine.DUrno =  0;
		rpLine.DRkey =  0;
		rpLine.DEtdi = TIMENULL; 
		rpLine.DAirb = TIMENULL; 
		rpLine.DRegn = "";
		rpLine.DFlno = "";
		rpLine.DDate = ""; 
		rpLine.DDes3 = "";
		rpLine.DDes4 = "";
		rpLine.DVia3 = "";
		rpLine.DAct = "";
	}

    return;

}



int ReportSameREGNTableViewer::CreateLine(SAMEREGNTABLE_LINEDATA &rpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


void ReportSameREGNTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}



void ReportSameREGNTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


/////////////////////////////////////////////////////////////////////////////
// ReportSameREGNTableViewer - display drawing routine



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void ReportSameREGNTableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	DrawHeader();

	SAMEREGNTABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}


    pomTable->DisplayTable();
}




void ReportSameREGNTableViewer::DrawHeader()
{
	int  ilTotalLines = 0;
	bool blNewLogicLine = false;

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();
	
	TABLE_HEADER_COLUMN *prlHeader[6];

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 67; 
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING1078);//FLIGHT

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 80;
	prlHeader[1]->Font = &ogCourier_Bold_10;
	prlHeader[1]->Text = GetString(IDS_STRING1584);//TYPE

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 60; 
	prlHeader[2]->Font = &ogCourier_Bold_10;
	prlHeader[2]->Text = GetString(IDS_STRING1585);// ORG/DES

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 70; 
	prlHeader[3]->Font = &ogCourier_Bold_10;
	prlHeader[3]->Text = GetString(IDS_STRING332);//Date

	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 45; 
	prlHeader[4]->Font = &ogCourier_Bold_10;
	prlHeader[4]->Text = GetString(IDS_STRING1421);//Schedule

	prlHeader[5] = new TABLE_HEADER_COLUMN;
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 45; 
	prlHeader[5]->Font = &ogCourier_Bold_10;
	prlHeader[5]->Text = GetString(IDS_STRING1586);//Actual

	for(int ili = 0; ili < 6; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}

  if(bgReports)
  {
	TABLE_HEADER_COLUMN *prlHeader[9];
	prlHeader[6] = new TABLE_HEADER_COLUMN;
	prlHeader[6]->Alignment = COLALIGN_CENTER;
	prlHeader[6]->Length = 45; 
	prlHeader[6]->Font = &ogCourier_Bold_10;
	prlHeader[6]->Text = GetString(IDS_STRING2787);

	prlHeader[7] = new TABLE_HEADER_COLUMN;
	prlHeader[7]->Alignment = COLALIGN_CENTER;
	prlHeader[7]->Length = 45; 
	prlHeader[7]->Font = &ogCourier_Bold_10;
	prlHeader[7]->Text = GetString(IDS_STRING2786);
	
	prlHeader[8] = new TABLE_HEADER_COLUMN;
	prlHeader[8]->Alignment = COLALIGN_CENTER;
	prlHeader[8]->Length = 65; 
	prlHeader[8]->Font = &ogCourier_Bold_10;
	prlHeader[8]->Text = GetString(IDS_STRING2788);
	
	for(int ili = 6; ili < 9; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}

	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}



void ReportSameREGNTableViewer::MakeColList(SAMEREGNTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList)
{

	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_9;

	rlColumnData.Alignment = COLALIGN_LEFT;
	if ((strcmp (prlLine->Adid, "A")) == 0)
		rlColumnData.Text = prlLine->AFlno;
	if ((strcmp (prlLine->Adid, "D")) == 0)
		rlColumnData.Text = prlLine->DFlno;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if ((strcmp (prlLine->Adid, "A")) == 0)
		rlColumnData.Text = "Arrival";
	if ((strcmp (prlLine->Adid, "D")) == 0)
		rlColumnData.Text = "Departure";
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if ((strcmp (prlLine->Adid, "A")) == 0)
		rlColumnData.Text = prlLine->AOrg3;
	if ((strcmp (prlLine->Adid, "D")) == 0)
		rlColumnData.Text = prlLine->DDes3;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if ((strcmp (prlLine->Adid, "A")) == 0)
	{
		rlColumnData.Text = prlLine->ADate;
	}
	if ((strcmp (prlLine->Adid, "D")) == 0)
	{
		rlColumnData.Text = prlLine->DDate;
	}
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if ((strcmp (prlLine->Adid, "A")) == 0)
		rlColumnData.Text = prlLine->AStoa.Format("%H:%M");
	if ((strcmp (prlLine->Adid, "D")) == 0)
		rlColumnData.Text = prlLine->DStod.Format("%H:%M");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if ((strcmp (prlLine->Adid, "A")) == 0)
		rlColumnData.Text = prlLine->ALand.Format("%H:%M");
	if ((strcmp (prlLine->Adid, "D")) == 0)
		rlColumnData.Text = prlLine->DAirb.Format("%H:%M");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	if(bgReports)
	{
		rlColumnData.Alignment = COLALIGN_LEFT;
		if ((strcmp (prlLine->Adid, "A")) == 0)
			rlColumnData.Text = prlLine->APsta;
		if ((strcmp (prlLine->Adid, "D")) == 0)
			rlColumnData.Text = prlLine->DPstd;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		if ((strcmp (prlLine->Adid, "A")) == 0)
			rlColumnData.Text = prlLine->Ttyp;
		if ((strcmp (prlLine->Adid, "D")) == 0)
			rlColumnData.Text = prlLine->Ttyp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
			
		rlColumnData.Alignment = COLALIGN_LEFT;
		if ((strcmp (prlLine->Adid, "A")) == 0)
			rlColumnData.Text = prlLine->Regn;
		if ((strcmp (prlLine->Adid, "D")) == 0)
			rlColumnData.Text = prlLine->Regn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
	}

}




int ReportSameREGNTableViewer::CompareFlight(SAMEREGNTABLE_LINEDATA *prpLine1, SAMEREGNTABLE_LINEDATA *prpLine2)
{
	// Sort fields order: Date, Schedule
 	CTime *polTime1;
	CTime *polTime2;
	CString *polDate1;
	CString *polDate2;

	if (prpLine1->Adid == "A")
	{
		polDate1 = &prpLine1->ADate;
		polTime1 = &prpLine1->AStoa;
	}
	else
	{
		polDate1 = &prpLine1->DDate;
		polTime1 = &prpLine1->DStod;
	}
	if (prpLine2->Adid == "A")
	{
		polDate2 = &prpLine2->ADate;
		polTime2 = &prpLine2->AStoa;
	}
	else
	{
		polDate2 = &prpLine2->DDate;
		polTime2 = &prpLine2->DStod;
	}

	if (*polDate1 == *polDate2)
	{
		return (*polTime1 == *polTime2)? 0:
			(*polTime1 > *polTime2)? 1: -1;
	}
	else
	{
		return strcmp(*polDate1, *polDate2);
	}

}



//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------


void ReportSameREGNTableViewer::GetHeader()
{
		
	omPrintHeadHeaderArray.DeleteAll();  // BWi

	TABLE_HEADER_COLUMN *prlHeader[6];
	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 67; 
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING1078);

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 80; 
	prlHeader[1]->Font = &ogCourier_Bold_10;
	prlHeader[1]->Text = GetString(IDS_STRING1584);

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 60; 
	prlHeader[2]->Font = &ogCourier_Bold_10;
	prlHeader[2]->Text = GetString(IDS_STRING1585);

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 70; 
	prlHeader[3]->Font = &ogCourier_Bold_10;
	prlHeader[3]->Text = GetString(IDS_STRING332);

	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 67; 
	prlHeader[4]->Font = &ogCourier_Bold_10;
	prlHeader[4]->Text = GetString(IDS_STRING1421);

	prlHeader[5] = new TABLE_HEADER_COLUMN;
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 67; 
	prlHeader[5]->Font = &ogCourier_Bold_10;
	prlHeader[5]->Text = GetString(IDS_STRING1586);

	for(int ili = 0; ili < 6; ili++)
	{
		omPrintHeadHeaderArray.Add( prlHeader[ili]);
	}


	if(bgReports)
	{
		TABLE_HEADER_COLUMN *prlHeader[9];
		prlHeader[6] = new TABLE_HEADER_COLUMN;
		prlHeader[6]->Alignment = COLALIGN_CENTER;
		prlHeader[6]->Length = 45; 
		prlHeader[6]->Font = &ogCourier_Bold_10;
		prlHeader[6]->Text = GetString(IDS_STRING2787);

		prlHeader[7] = new TABLE_HEADER_COLUMN;
		prlHeader[7]->Alignment = COLALIGN_CENTER;
		prlHeader[7]->Length = 45; 
		prlHeader[7]->Font = &ogCourier_Bold_10;
		prlHeader[7]->Text = GetString(IDS_STRING2786);
		
		
		prlHeader[8] = new TABLE_HEADER_COLUMN;
		prlHeader[8]->Alignment = COLALIGN_CENTER;
		prlHeader[8]->Length = 65; 
		prlHeader[8]->Font = &ogCourier_Bold_10;
		prlHeader[8]->Text = GetString(IDS_STRING2788);
		
		
		for(int ili = 6; ili < 9; ili++)
		{
		omPrintHeadHeaderArray.Add( prlHeader[ili]);
		}

	}

}




void ReportSameREGNTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[256];

	sprintf(pclFooter, omTableName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));

	omFooterName = pclFooter;
	CString olTableName = GetString(REPORTS_RB_FlightsByRegistration);

	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	pomPrint->imMaxLines = 38;  // (P=57,L=38)

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			//rlDocInfo.lpszDocName = TABLENAME;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();  

			olFooter1.Format("%s -   %s", omFooterName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );
			if(bgReports)
			olFooter1.Format("%s -   %s-  %s", omFooterName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pcgUser );
			
			GetHeader();

			if (ilLines == 0)
				PrintTableHeader(); // Drucken leerer Tabelle
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool ReportSameREGNTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;

	pomPrint->PrintUIFHeader("",omTableName,pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	//int ilSize = omHeaderDataArray.GetSize();
	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
			rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omPrintHeadHeaderArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool ReportSameREGNTableViewer::PrintTableLine(SAMEREGNTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7 ;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft = PRINT_FRAMETHIN;
		rlElement.FrameRight = PRINT_NOFRAME;


		switch(i)
		{
			case 0:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->AFlno;
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DFlno;
			}
			break;
			case 1:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = "Arrival";
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = "Departure";
			}
			break;
			case 2:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->AOrg3;
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DDes3;
			}
			break;
			case 3:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->ADate;
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DDate;
			}
			break;
			case 4:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->AStoa.Format("%H:%M");
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DStod.Format("%H:%M");
			}
			break;
			case 5:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->ALand.Format("%H:%M");
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DAirb.Format("%H:%M");
				rlElement.FrameRight = PRINT_FRAMETHIN;
			}
			break;
		}
		if(bgReports)
		{
			switch(i)
		{
			case 0:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->AFlno;
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DFlno;
			}
			break;
			case 1:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = "Arrival";
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = "Departure";
			}
			break;
			case 2:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->AOrg3;
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DDes3;
			}
			break;
			case 3:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->ADate;
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DDate;
			}
			break;
			case 4:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->AStoa.Format("%H:%M");
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DStod.Format("%H:%M");
			}
			break;
			case 5:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->ALand.Format("%H:%M");
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DAirb.Format("%H:%M");
			}
			break;
		
			case 6:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->APsta;
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DPstd;
			}
			break;
			case 7:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->Ttyp;
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->Ttyp;
				rlElement.FrameRight = PRINT_FRAMETHIN;
			}
			break;
			case 8:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->Regn;
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->Regn;
				rlElement.FrameRight = PRINT_FRAMETHIN;
			}
			break;
			
		}	
		}
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
bool ReportSameREGNTableViewer::PrintPlanToFile(char *opTrenner)
{
	ofstream of;

	CString olFileName = omTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);

	int ilwidth = 1;

	of  << setw(ilwidth) << omTableName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;
 
 if(!bgReports)
 {
	of	<< setw(ilwidth) << GetString(IDS_STRING1078) 
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1584) 
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1585)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING332)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1421)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1586)
		<< endl;


	int ilCount = omLines.GetSize();

	CStringArray olDaten;
	for(int i = 0; i < ilCount; i++)
	{
		SAMEREGNTABLE_LINEDATA rlD = omLines[i];

		if ((strcmp (rlD.Adid, "A")) == 0)
		{
			olDaten.Add(rlD.AFlno);
			olDaten.Add("Arrival");
			olDaten.Add(rlD.AOrg3);
			olDaten.Add(rlD.ADate);
			olDaten.Add(rlD.AStoa.Format("%H:%M"));
			olDaten.Add(rlD.ALand.Format("%H:%M"));
		
		}
		if ((strcmp (rlD.Adid, "D")) == 0)
		{
			olDaten.Add(rlD.DFlno);
			olDaten.Add("Departure");
			olDaten.Add(rlD.DDes3);
			olDaten.Add(rlD.DDate);
			olDaten.Add(rlD.DStod.Format("%H:%M"));
			olDaten.Add(rlD.DAirb.Format("%H:%M"));
		
		}

		of.setf(ios::left, ios::adjustfield);

		of   << setw(ilwidth) << olDaten[0].GetBuffer(0)
		     << setw(1) << opTrenner 
		     << setw(ilwidth) << olDaten[1].GetBuffer(0)
		     << setw(1) << opTrenner 
		     << setw(ilwidth) << olDaten[2].GetBuffer(0)
		     << setw(1) << opTrenner 
		     << setw(ilwidth) << olDaten[3].GetBuffer(0)
		     << setw(1) << opTrenner 
		     << setw(ilwidth) << olDaten[4].GetBuffer(0)
		     << setw(1) << opTrenner 
		     << setw(ilwidth) << olDaten[5].GetBuffer(0)
			 << endl;

		olDaten.RemoveAll();
	}
 }
	


/*
	CCS_TRY

	char pclHeadlineSelect[100];
	sprintf(pclHeadlineSelect, GetString(IDS_STRING1593), pcmSelect, pcmInfo);

	ofstream of;
	of.open( pcpDefPath, ios::out);
	of << CString(pclHeadlineSelect) << " "
	   << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

	of << GetString(IDS_STRING1078) << "     "<< GetString(IDS_STRING1584) << "      " 
	   << GetString(IDS_STRING1585)  << "   "  << GetString(IDS_STRING332) << "    " 
	   << GetString(IDS_STRING1421) << "   " << GetString(IDS_STRING1586) << endl;
	of << "--------------------------------------------------------" << endl;
	int ilCount = omLines.GetSize();


	CStringArray olDaten;
	for(int i = 0; i < ilCount; i++)
	{
		SAMEREGNTABLE_LINEDATA rlD = omLines[i];

		if ((strcmp (rlD.Adid, "A")) == 0)
		{
			olDaten.Add(rlD.AFlno);
			olDaten.Add("Arrival");
			olDaten.Add(rlD.AOrg3);
			olDaten.Add(rlD.ADate);
			olDaten.Add(rlD.AStoa.Format("%H:%M"));
			olDaten.Add(rlD.ALand.Format("%H:%M"));
		}
		if ((strcmp (rlD.Adid, "D")) == 0)
		{
			olDaten.Add(rlD.DFlno);
			olDaten.Add("Departure");
			olDaten.Add(rlD.DDes3);
			olDaten.Add(rlD.DDate);
			olDaten.Add(rlD.DStod.Format("%H:%M"));
			olDaten.Add(rlD.DAirb.Format("%H:%M"));
		}

		of.setf(ios::left, ios::adjustfield);
		of   << setw(10) << olDaten[0].GetBuffer(0)
			 << setw(12) << olDaten[1].GetBuffer(0)
			 << setw(8) << olDaten[2].GetBuffer(0)
			 << setw(11) << olDaten[3].GetBuffer(0)
			 << setw(9) << olDaten[4].GetBuffer(0)
			 << setw(9) << olDaten[5].GetBuffer(0)
			 << endl;
		
		olDaten.RemoveAll();
	}
	of.close();

	CCS_CATCH_ALL
*/

 if(bgReports)
 {
  of	<< setw(ilwidth) << GetString(IDS_STRING1078) 
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1584) 
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1585)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING332)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1421)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1586)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2787)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2786)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2788)
		<< endl;


	int ilCount = omLines.GetSize();

	CStringArray olDaten;
	for(int i = 0; i < ilCount; i++)
	{
		SAMEREGNTABLE_LINEDATA rlD = omLines[i];

		if ((strcmp (rlD.Adid, "A")) == 0)
		{
			olDaten.Add(rlD.AFlno);
			olDaten.Add("Arrival");
			olDaten.Add(rlD.AOrg3);
			olDaten.Add(rlD.ADate);
			olDaten.Add(rlD.AStoa.Format("%H:%M"));
			olDaten.Add(rlD.ALand.Format("%H:%M"));
			olDaten.Add(rlD.APsta);
			olDaten.Add(rlD.Ttyp);
			olDaten.Add(rlD.Regn);
		}
		if ((strcmp (rlD.Adid, "D")) == 0)
		{
			olDaten.Add(rlD.DFlno);
			olDaten.Add("Departure");
			olDaten.Add(rlD.DDes3);
			olDaten.Add(rlD.DDate);
			olDaten.Add(rlD.DStod.Format("%H:%M"));
			olDaten.Add(rlD.DAirb.Format("%H:%M"));
			olDaten.Add(rlD.DPstd);
			olDaten.Add(rlD.Ttyp);
			olDaten.Add(rlD.Regn);
		}

		of.setf(ios::left, ios::adjustfield);

		of   << setw(ilwidth) << olDaten[0].GetBuffer(0)
		     << setw(1) << opTrenner 
		     << setw(ilwidth) << olDaten[1].GetBuffer(0)
		     << setw(1) << opTrenner 
		     << setw(ilwidth) << olDaten[2].GetBuffer(0)
		     << setw(1) << opTrenner 
		     << setw(ilwidth) << olDaten[3].GetBuffer(0)
		     << setw(1) << opTrenner 
		     << setw(ilwidth) << olDaten[4].GetBuffer(0)
		     << setw(1) << opTrenner 
		     << setw(ilwidth) << olDaten[5].GetBuffer(0)
			 << setw(1) << opTrenner 
		     << setw(ilwidth) << olDaten[6].GetBuffer(0)
			 << setw(1) << opTrenner 
		     << setw(ilwidth) << olDaten[7].GetBuffer(0)
			 << setw(1) << opTrenner 
		     << setw(ilwidth) << olDaten[8].GetBuffer(0)
			 << endl;

		olDaten.RemoveAll();
	}
 }
	of.close();
	return true;
}
