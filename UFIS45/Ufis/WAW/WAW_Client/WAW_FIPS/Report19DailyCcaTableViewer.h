#if !defined(AFX_REPORT19DAILYCCATABLEVIEWER_H__657FF802_4EDA_11D2_8369_0000C03B916B__INCLUDED_)
#define AFX_REPORT19DAILYCCATABLEVIEWER_H__657FF802_4EDA_11D2_8369_0000C03B916B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// Report19DailyCcaTableViewer.h : header file
//


#include <stdafx.h>
#include <Table.h>
#include <CViewer.h>
#include <CCSPrint.h>
#include <DiaCedaFlightData.h>
#include <CedaCcaData.h>

// Konstanten
const char ccmBlkTyp = 'N' ;
const char ccmComTyp = 'C' ;
const char ccmFlugTyp = ' ' ;


struct REPORT19TABLE_LINEDATA
{
	long		Urno; 	// Eindeutige Datensatz-Nr.

	CString	Alc ;		
	CString	FltnOut ;
	CString	Act ;
	CString	Des ;
	CString	Std ;
	CString	ViaOut ;

	CString CAnz1 ;
	CString Ckift1 ;
	CString CMin1 ;
	CString CkiftTime1 ;

	bool Common ;		// true <=> Common oder Blk CIC
	char Typ ;			// 'C'ommon / 'N'ot available / Rest flugabh�ngig ( blank )
	int Folgezeile ;
	
	REPORT19TABLE_LINEDATA()
	{ 
		Alc.Empty() ;		
		FltnOut.Empty() ;
		Act.Empty() ;
		Des.Empty() ;
		Std.Empty() ;
		ViaOut.Empty() ;

		CAnz1.Empty() ;
		Ckift1.Empty() ;
		CMin1.Empty() ;
		CkiftTime1.Empty() ;
		Folgezeile=0; Urno=0L ; Common=false ; Typ = ccmFlugTyp ;
	}
};


struct DAILYCCAURNOKEYLIST
{
	CCSPtrArray<REPORT19TABLE_LINEDATA> Rotation;
}; 


// Counterbelegungsdaten pro Flug
struct CCALLOCDATA
{
	int Anz ;		// Anzahl der einem Flug zugeordneten Counter gleicher Zeiten
	bool Typ ;		// true <=> flugunabh�ngiger Schalter ( Common / gesperrt )
	char CTyp ;		// C / N oder flugabh�ngig
	CString Alc2 ;  // Airline-Code bei Common Checkin Schalter
	CString Cicf ;	// Start Counter
	CString Cict ;	// End Counter
	CTime Open ;	// �ffnung
	CTime Close ;	// Schlie�ung
	CString Disp ;	// Bemerkung
	
	CCALLOCDATA()
	{ 
		Anz=0; Typ=false ; Alc2.Empty(); Cicf.Empty() ; Cict.Empty() ; 
		Open=TIMENULL ; Close=TIMENULL ; Disp.Empty() ; CTyp = ' ' ;
	}

};



/////////////////////////////////////////////////////////////////////////////
// Befehlsziel Report19DailyCcaTableViewer 


class Report19DailyCcaTableViewer : public CViewer
{
// Constructions
public:
    Report19DailyCcaTableViewer(char *pcpInfo=NULL, char *pcpSelect=NULL, char *pspVR=NULL);
    ~Report19DailyCcaTableViewer();

    void Attach(CTable *popAttachWnd);
    virtual void ChangeViewTo(const CTime &ropFromTime, const CTime &ropToTime);
// Internal data processing routines
private:
    void MakeLines(const CTime &ropDay);
	void MakeLine(long AUrno, long DUrno, bool bpFirstCall = false );		
	int  CreateLine( REPORT19TABLE_LINEDATA &rpLine );
	int  CompareCcaLine( REPORT19TABLE_LINEDATA &rpLine, REPORT19TABLE_LINEDATA &rpCmpLine ) ;

	void GetHeader(void);
	void GetPrintHeader(void);

// Operations
public:
	void DeleteAll(void);
	void DeleteLine(int ipLineno);
	void UpdateDisplay(void);
	void PrintTableView(void);
	bool PrintTableHeader(void);
	bool PrintPlanToFile(char *pcpDefPath);
	bool PrintTableLine(REPORT19TABLE_LINEDATA *prpLine, bool bpLastLine);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
// Attributes
private:
	bool bmSeason;
    CTable *pomTable;
	CString omFromStr;
	CString omToStr;
	CString omFlugUrnos ;	// kommagetrennte Liste der Flugurnos
	CedaCcaData omCcaData;	// Datens�tze CounterAllocation

///////
//Print 
	CCSPrint *pomPrint;
//	CString omTableName;
	CString omFooterName;
	char *pcmInfo;
	char *pcmSelect;
	CString olVerkehrsRechte ;

public:
    CCSPtrArray<REPORT19TABLE_LINEDATA> omLines;
	CMapStringToPtr omLineKeyMap;
    // blockierte CheckInCounter
	CCSPtrArray<DIACCADATA> omBlkData;
	CString omTableName;
	CString omFileName;

protected:
	bool GetVias( CString&, ROTATIONDLGFLIGHTDATA* );
	void SetPrintItem( PRINTELEDATA&, CString, bool bpLF=false, bool bpRF=false);
	bool CopyDiaCcaToCca( CCADATA& rrpCca, DIACCADATA rpDiaCca ) ;
	bool CompressCcaAlloc(CCSPtrArray<CCALLOCDATA> &ropCcaAlloc) const;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REPORT19DAILYCCATABLEVIEWER_H__657FF802_4EDA_11D2_8369_0000C03B916B__INCLUDED_)
