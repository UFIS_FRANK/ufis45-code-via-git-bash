// CButtonListDlg.cpp : implementation file
//
#include <stdafx.h>
#include <tab.h>
#include <process.h>
#include <FPMS.h>
#include <ButtonListDlg.h>
#include <SeasonTableDlg.h>
#include <SeasonCollectDlg.h>
#include <RotationTables.h>
#include <FlightDiagram.h>
#include <BltDiagram.h>
#include <GatDiagram.h>
#include <PosDiagram.h>
#include <WroDiagram.h>
#include <CcaDiagram.h>
#include <ChaDiagram.h>
#include <ProxyDlg.h>
#include <CCSGlobl.h>
#include <BasicData.h>
#include <CCSCedaCom.h>
#include <CCSBCHandle.h>
#include <BasicData.h>
#include <PrivList.h>
#include <CCSDdx.h>
#include <AcirrDlg.h>
#include <AirportInformationDlg.h>
#include <CcaCommonTableDlg.h>
#include <KonflikteDlg.h>
#include <SpecialConflictDlg.h>
#include <SetupDlg.h>
#include <WoResTableDlg.h>
#include <CcaCommonDlg.h>
#include <CcaCommonDlg.h>
#include <CicDemandTableDlg.h>
#include <DailyScheduleTableDlg.h>
#include <ReportSelectDlg.h>
#include <FlightSearchTableDlg.h>
#include <RotationDlg.h>
#include <RotGroundDlg.h>
#include <Utils.h>
#include <tlhelp32.h>

#include <UFISAmSink.h>
#include <ClntTypes.h>
#include <CedaAptLocalUtc.h>
#include <LibGlobl.h>
#include <LibGlobl.h>
#include <BC_Monitor.h>
#include <DiaCedaFlightData.h>
#include <resrc1.h>
#include <InfoDlg.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define DE_ICING_WINDOW_CFG_NAME "DE_ICE_WINDOW_NAME"			

#define BCActiveTestSecs 60
#define BCTestSecs 180


HDDEDATA CALLBACK DdeCallback(UINT, UINT, HCONV, HSZ, HSZ, HDDEDATA, DWORD, DWORD);


static void ButtonListDlgCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

#ifdef SATS_PERFORMANCE
void  ProcessTrafficLight(long BcNum, int ipState)
{
	if (pogButtonList != NULL)
		pogButtonList->UpdateTrafficLight(ipState);
}
#endif	// SATS_PERFORMANCE


/////////////////////////////////////////////////////////////////////////////
// CCButtonListDlg dialog

IMPLEMENT_DYNAMIC(CButtonListDlg, CDialog);

CButtonListDlg::CButtonListDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CButtonListDlg::IDD, pParent), omBcTestData("AFT")
{
	//{{AFX_DATA_INIT(CCButtonListDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDI_UFIS);
	m_pAutoProxy = NULL;
	pomParent = pParent;
	pogSeasonTableDlg = NULL;
	pogRotationTables = NULL;
	pogFlightDiagram = NULL;
 	pogCommonCcaTable = NULL;
	pogCicDemandTableDlg = NULL;
	pogDailyScheduleTableDlg = NULL;
	pmExitDlg = NULL;

	//bc_monitor
	pomBcMonitor = NULL;
	mdDeIcingProcId = NULL;			

	bmWait = false;
	bmIsCuteIFStarted = false;
	bmBcTestActive = false;
	bmBCOK = true;

    ogDdx.Register(this, INF_CHANGE, CString("BUTTONLISTDLG"), CString("ButtonListDlg Update"), ButtonListDlgCf);
    ogDdx.Register(this, INF_NEW,    CString("BUTTONLISTDLG"), CString("ButtonListDlg New"),    ButtonListDlgCf);
    ogDdx.Register(this, BC_INF_CHANGE, CString("BUTTONLISTDLG"), CString("ButtonListDlg Update"), ButtonListDlgCf);
    ogDdx.Register(this, BC_INF_NEW,    CString("BUTTONLISTDLG"), CString("ButtonListDlg New"),    ButtonListDlgCf);
    ogDdx.Register(this, BC_INF_DELETE, CString("BUTTONLISTDLG"), CString("ButtonListDlg Delete"), ButtonListDlgCf);

//	ogDdx.Register(this, BC_TEST, CString("BC_TEST"), CString("CButtonListDlg"), ButtonListDlgCf);
	ogDdx.Register((void *)this,BROADCAST_CHECK,CString("BROADCAST_CHECK"), CString("Broadcast received"),ButtonListDlgCf);
	ogDdx.Register(this, BC_LOCCHNG, CString("BC_LOCCHNG"), CString("CButtonListDlg"), ButtonListDlgCf);

#ifdef SATS_PERFORMANCE
	ogBcHandle.SetTrafficLightCallBack(ProcessTrafficLight);
#endif	// SATS_PERFORMANCE

	m_UFISAmSink.SetLauncher(this);
	m_key = "DialogPosition\\ButtonList";
}




CButtonListDlg::~CButtonListDlg()
{
	// If there is an automation proxy for this dialog, set
	//  its back pointer to this dialog to NULL, so it knows
	//  the dialog has been deleted.
	if (m_pAutoProxy != NULL)
		m_pAutoProxy->m_pDialog = NULL;

	ogDdx.UnRegister(this, NOTUSED);

	if (mdDeIcingProcId!=NULL)			
	{		
		killProcess( mdDeIcingProcId );	
	}		


}

void CButtonListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CButtonListDlg)
	DDX_Control(pDX, IDC_CHANGES, m_CB_Changes);
	DDX_Control(pDX, IDC_WARTERAEUME, m_CB_Warteraeume);
	DDX_Control(pDX, IDC_CHUTE, m_CB_Chute);
	DDX_Control(pDX, IDC_TELEXPOOL, m_CB_Telexpool);
	DDX_Control(pDX, IDC_TAGESFLUGPLANERFASSEN, m_CB_Tagesflugplanerfassen);
	DDX_Control(pDX, IDC_STAMMDATEN, m_CB_Stammdaten);
	DDX_Control(pDX, IDC_SETUP, m_CB_Setup);
	DDX_Control(pDX, IDC_RUECKGAENGIG, m_CB_Rueckgaengig);
	DDX_Control(pDX, IDC_REGELN, m_CB_Regeln);
 	DDX_Control(pDX, IDC_REPORTS, m_CB_Reports);
	DDX_Control(pDX, IDC_LFZPOSITIONEN, m_CB_LFZPositionen);
	DDX_Control(pDX, IDC_KONFLIKTE, m_CB_Konflikte);
	DDX_Control(pDX, IDC_INFO, m_CB_Info);
	DDX_Control(pDX, IDC_HILFE, m_CB_Hilfe);
	DDX_Control(pDX, IDC_GEPAECKBAENDER, m_CB_Gepaeckbaender);
	DDX_Control(pDX, IDC_GATES, m_CB_Gates);
	DDX_Control(pDX, IDC_FLUGDIAGRAMM, m_CB_Flugdiagramm);
	DDX_Control(pDX, IDC_COVERAGE, m_CB_Coverage);
	DDX_Control(pDX, IDC_CHECKIN, m_CB_Checkin);
	DDX_Control(pDX, IDC_ACIN, m_CB_Acin);
	DDX_Control(pDX, IDC_ACHTUNG, m_CB_Achtung);
	DDX_Control(pDX, IDC_FLUGPLANPFLEGEN, m_CB_Flugplanpflegen);
	DDX_Control(pDX, IDC_FLUGPLANERFASSEN, m_CB_Flugplanerfassen);
	DDX_Control(pDX, IDC_BEENDEN, m_CB_Beenden);
	DDX_Control(pDX, IDC_BC_STATUS, m_CB_BCStatus);
	DDX_Control(pDX, IDC_ABOUT, m_CB_About);
	DDX_Control(pDX, IDC_CHECK, m_Check);
	DDX_Control(pDX, IDC_SPIN_CHECK, m_SpinCheck);
	DDX_Control(pDX, IDC_COMMONCCA, m_CB_CommonCCA);
	DDX_Control(pDX, IDC_TAB_SYSTAB, m_Tab);
	DDX_Control(pDX, IDC_DEICING, m_CB_DeIcing);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CButtonListDlg, CDialog)
	//{{AFX_MSG_MAP(CButtonListDlg)
	ON_WM_TIMER()
	ON_WM_SYSCOMMAND()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_CHUTE, OnChutes)
	ON_BN_CLICKED(IDC_BEENDEN, OnBeenden)
	ON_BN_CLICKED(IDC_DRUCKEN, OnDrucken)
	ON_BN_CLICKED(IDC_HILFE, OnHilfe)
	ON_BN_CLICKED(IDC_ABOUT, OnAbout)
	ON_BN_CLICKED(IDC_SETUP, OnSetup)
	ON_BN_CLICKED(IDC_INFO, OnInfo)
	ON_BN_CLICKED(IDC_KONFLIKTE, OnKonflikte)
	ON_BN_CLICKED(IDC_COVERAGE, OnCoverage)
	ON_BN_CLICKED(IDC_FLUGPLANERFASSEN, OnFlugplanErfassen)
	ON_BN_CLICKED(IDC_FLUGPLANPFLEGEN, OnFlugplanPflegen)
	ON_BN_CLICKED(IDC_TAGESFLUGPLANERFASSEN, OnTagesflugplanErfassen)
	ON_BN_CLICKED(IDC_FLUGDIAGRAMM, OnFlugdiagramm)
	ON_BN_CLICKED(IDC_CHECKIN, OnCheckin)
	ON_BN_CLICKED(IDC_GEPAECKBAENDER, OnGepaeckbaender)
	ON_BN_CLICKED(IDC_GATES, OnGates)
	ON_BN_CLICKED(IDC_WARTERAEUME, OnWarteraeume)
	ON_BN_CLICKED(IDC_LFZPOSITIONEN, OnLFZPositionen)
	ON_BN_CLICKED(IDC_STAMMDATEN, OnStammdaten)
	ON_BN_CLICKED(IDC_REGELN, OnRegeln)
	ON_BN_CLICKED(IDC_ACHTUNG, OnAchtung)
	ON_BN_CLICKED(IDC_RUECKGAENGIG, OnRueckgaengig)
	ON_BN_CLICKED(IDC_REPORTS, OnReports)
	ON_BN_CLICKED(IDC_ACIN, OnACIn)
	ON_BN_CLICKED(IDC_TELEXPOOL, OnTelexpool)
	ON_MESSAGE(WM_BCADD,OnBcAdd)
	ON_WM_NCLBUTTONDOWN()
	ON_BN_CLICKED(IDC_CHANGES, OnChanges)
	ON_BN_CLICKED(IDC_BC_STATUS, OnBCStatus)
	ON_MESSAGE(BC_TO_APPLICATION, OnShowBcInMonitor)
	ON_BN_CLICKED(IDC_BC, OnBC)
	ON_MESSAGE(BC_TO_REREAD, OnTextBcMonitor)
	ON_EN_KILLFOCUS(IDC_CHECK, OnKillfocusCheck)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_CHECK, OnSpinCheck)
	ON_BN_CLICKED(IDC_COMMONCCA, OnCommoncca)
	ON_BN_CLICKED(IDC_DEICING, OnDeicing)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCButtonListDlg message handlers
/*
LONG CButtonListDlg::OnEditChanged( UINT wParam, LPARAM lParam)
{
	OnEditKillfocus(wParam,lParam);
	return 0L;

}

LONG CButtonListDlg::OnEditKillfocus( UINT wParam, LPARAM lParam)
{
	OnKillfocusCheck();
	return 0L;
}
*/
void CButtonListDlg::OnKillfocusCheck() 
{

	// TODO: Add your control notification handler code here
	CString olText;
	int ilPos ;
	int ilMinRange,ilMaxRange;

	m_SpinCheck.GetRange(ilMinRange,ilMaxRange);
	m_Check.GetWindowText(olText);
	ilPos = atoi(olText);
	if(ilPos>=ilMinRange && ilPos<=ilMaxRange)
	{
		m_SpinCheck.SetPos(ilPos);
	}
	else if(ilPos>ilMaxRange)
	{
		m_SpinCheck.SetPos(ilMaxRange);
		olText.Format("%d",ilMaxRange);
		m_Check.SetWindowText(olText);
	}
	else
	{
		m_SpinCheck.SetPos(ilMinRange);
		olText.Format("%d",ilMinRange);
		m_Check.SetWindowText(olText);
	}

	SetTimerCheck(2, m_SpinCheck.GetPos());
}

void CButtonListDlg::OnSpinCheck(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	int ilMinRange,ilMaxRange;
	// TODO: Add your control notification handler code here
	int ilPos = pNMUpDown->iPos;
	int ilDelta = pNMUpDown->iDelta;
 	CString olText;
	olText.Format("%d",ilPos+ilDelta);
	m_SpinCheck.GetRange(ilMinRange,ilMaxRange);
	if((ilPos+ilDelta)>=ilMinRange && (ilPos+ilDelta)<=ilMaxRange)
	{
		(m_SpinCheck.GetBuddy())->SetWindowText(olText);
	}
	*pResult = 0;

	SetTimerCheck(2, m_SpinCheck.GetPos());
}

void CButtonListDlg::SetTimerCheck(int ipTimer, int ipInterval)
{
	bgConfCheck = true;
	CString olText;
	int ilPos ;
	m_Check.GetWindowText(olText);
	ilPos = atoi(olText);

	KillTimer(ipTimer);

	if (ilPos > 0)
	{
		SetTimer(ipTimer, (UINT) ilPos * 1000 , NULL);
		bgConfCheck = false;
	}
	else if (ilPos < 0)
	{
		bgConfCheck = false;
	}

	if (ilPos <= 0)
		ogKonflikte.SASCheckAll(true);
}

BOOL CButtonListDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon




	InitTab();


#ifdef SATS_PERFORMANCE
	//TCP/IP-Broadcast
//	ogCommHandler.RegisterBcWindow(this);

	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, __GetConfigPath() );

	if (stricmp(pclUseBcProxy,"BCSERV") == 0)
	{
#endif	// SATS_PERFORMANCE
		ogCommHandler.RegisterBcWindow(this);
#ifdef SATS_PERFORMANCE
	}
	else
	{
		ogBcHandle.StartBc(); 
	}
	//TCP/IP-Broadcast
#endif	// SATS_PERFORMANCE

	ogBcHandle.GetBc();

	pogButtonList = this;


	if(bgPositionConfirmation)
		pogSpecialConflictDlg = new SpecialConflictDlg(this);

	pogKonflikteDlg = new KonflikteDlg(this);

	pogWoResTableDlg = new WoResTableDlg(this);

	pogSeasonDlg = new CSeasonDlg(this);

	pogCommonCcaDlg = new CCcaCommonDlg(this);

	pogCommonCcaDlg->ShowWindow(SW_HIDE);

//	pogCicDemandTableDlg = new CicDemandTableDlg(this);

//	pogCicDemandTableDlg->ShowWindow(SW_HIDE);

	pogRotationDlg = new RotationDlg(this);

	pogReportSelectDlg = new CReportSelectDlg(this);

	pogFlightSearchTableDlg = new FlightSearchTableDlg(this);

	//pogModeOfPay= new CModeOfPay(this,0,"A");

    // calculate the window height
	CRect olRect;
    this->GetClientRect(&olRect);
    this->GetWindowRect(&olRect);
    m_nDialogBarHeight = olRect.bottom - olRect.top;

	olRect = CRect(0,0,::GetSystemMetrics(SM_CXSCREEN),::GetSystemMetrics(SM_CYSCREEN));
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );


	/*
    CRect rectScreen(0, 0, ::GetSystemMetrics(SM_CXSCREEN), 65); //  ::GetSystemMetrics(SM_CYSCREEN));
	CWnd::MoveWindow(&rectScreen, TRUE);

	CRect rect;
    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;
	*/

    SetWindowText(ogAppName);

	SetIcon(m_hIcon, FALSE);
	SetIcon(m_hIcon, TRUE);			// Set big icon


	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Flugplanpflegen"),m_CB_Flugplanpflegen);
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Flugplanerfassen"),m_CB_Flugplanerfassen);

	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Tagesflugplanerfassen"),m_CB_Tagesflugplanerfassen);
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Flugdiagramm"),m_CB_Flugdiagramm);

	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Gepaeckbaender"),m_CB_Gepaeckbaender);
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Gates"),m_CB_Gates);
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_LFZPositionen"),m_CB_LFZPositionen);
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Warteraeume"),m_CB_Warteraeume);
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Checkin"),m_CB_Checkin);

	if(bgChuteDisplay)
	{
		SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Chute"),m_CB_Chute);

		CRect olRect;
		m_CB_Regeln.GetWindowRect(olRect);
		ScreenToClient(olRect);
		olRect.left = olRect.left+43;
		m_CB_Regeln.MoveWindow(olRect);	
	}
	else
	{
		m_CB_Chute.ShowWindow(SW_HIDE);
	}


	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Regeln"),m_CB_Regeln);

	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Stammdaten"),m_CB_Stammdaten);
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Telexpool"),m_CB_Telexpool);

	
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Hilfe"),m_CB_Hilfe); 
 	m_CB_Info.SetSecState(ogPrivList.GetStat("DESKTOP_CB_Info"));
//	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Acin"),m_CB_Acin); 
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Fluggastinfo"),m_CB_CommonCCA);
//	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_LogTab"),m_CB_Coverage);
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Reports"),m_CB_Reports);

	m_CB_Konflikte.SetSecState(ogPrivList.GetStat("DESKTOP_CB_Konflikte"));
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Setup"),m_CB_Setup);
//	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_UserButton"),m_CB_Rueckgaengig);

	/*
	if (ogPrivList.GetStat("DESKTOP_CB_Achtung") != '1')
	{
		UINT ilBStyle = m_CB_Achtung.GetButtonStyle();
		m_CB_Achtung.SetButtonStyle(ilBStyle & !BS_OWNERDRAW, 1);
	}
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Achtung"),m_CB_Achtung);
	*/
	m_CB_Achtung.SetSecState(ogPrivList.GetStat("DESKTOP_CB_Achtung"));

	m_CB_Flugplanerfassen.SetWindowText(GetString(IDS_STRING1552));
	m_CB_Changes.SetSecState(ogPrivList.GetStat("DESKTOP_CB_History"));
	
	char pclExcelPath[256];

	GetPrivateProfileString(ogAppName, "USER_BUTTON_LABEL", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, __GetConfigPath() );

//	SetTimer(1, 120 * 1000,NULL);
#ifdef SATS_PERFORMANCE
	char pclBcMethod[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclBcMethod, sizeof pclBcMethod, __GetConfigPath() );
	if (stricmp(pclBcMethod,"BCCOM") == 0)
	{
		SetTimer(1, (UINT) 60 * 1000 , NULL);
	}
	else
	{
#endif	// SATS_PERFORMANCE
		SetTimer(1, (UINT) 60 * 1000 , NULL);
#ifdef SATS_PERFORMANCE
	}
#endif	// SATS_PERFORMANCE

	omLastBcTime = CTime::GetCurrentTime();
	bmBCOK = false;
	m_CB_BCStatus.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_About.SetCheck(1);


//######
	CTime olLocalTime = CTime::GetCurrentTime();
	CTime olUtcTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olUtcTime);

	char pclTimes[36];
	char pclDate[36];

	
	CTime olUtcTimeTmp = CTime::GetCurrentTime();
	olUtcTimeTmp -= ogUtcDiff;
	
	if(ogUtcDiff != 0)
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
	}
	else
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
	}

    
	GetDlgItem(IDC_DUMMYTIME)->GetWindowRect(olRect);
	ScreenToClient(olRect);

	omTime.Create(pclTimes, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 190, olRect.top - 2, olRect.right - 89, olRect.top + 23), this);

    omDate.Create(pclDate, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 86,  olRect.top - 2, olRect.right, olRect.top + 23), this);
        
    omDate.SetTextColor(RGB(255,0,0));
//######

	char pclBCMon[256];

	CString	m_key = "DialogPosition\\BcMonitor";

    GetPrivateProfileString(ogAppName, "BCMONITOR", "FALSE", pclBCMon, sizeof pclBCMon, __GetConfigPath() );
	if(strcmp(pclBCMon, "FALSE") == 0)
	{
		CWnd* polWnd = GetDlgItem(IDC_BC);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);

		CRect olRectHelp;
		polWnd = GetDlgItem(IDC_HILFE);
		polWnd->GetWindowRect(olRectHelp);
		ScreenToClient(olRectHelp);
		olRectHelp.left = olRect.left-8;
		polWnd->MoveWindow(olRectHelp);	
		polWnd->SetWindowText("Help");	
	}

//#####

	char pclCheck[256];
    GetPrivateProfileString(ogAppName, "TIMER_CONFLICT", "-100", pclCheck, sizeof pclCheck, __GetConfigPath() );
	int ilPos = atoi(pclCheck);

	if(ilPos == -100)
	{
		CWnd* polWnd = GetDlgItem(IDC_SPIN_CHECK);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CHECK);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);

		CRect olRectHelp;
		polWnd = GetDlgItem(IDC_SPIN_CHECK);
		if (polWnd)
		{
			polWnd->GetWindowRect(olRectHelp);
			ScreenToClient(olRectHelp);
		}

		CRect olRectExit;
		polWnd = GetDlgItem(IDC_BEENDEN);
		if (polWnd)
		{
			polWnd->GetWindowRect(olRectExit);
			ScreenToClient(olRectExit);
			olRectExit.right = olRectHelp.right;
			polWnd->MoveWindow(olRectExit);	
		}
	}
	else
	{
		m_SpinCheck.SetRange(-1,300);
		m_SpinCheck.SetPos(ilPos);
		m_SpinCheck.SetBuddy(GetDlgItem(IDC_CHECK));

		CString olText;
		olText.Format("%d",ilPos);
		m_Check.SetWindowText(olText);//UD_MINVAL

		SetTimerCheck(2, m_SpinCheck.GetPos());

		SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_SpinCheck"),m_SpinCheck);
		SetWndStatAll(ogPrivList.GetStat("DESKTOP_CE_Check"),m_Check);
	}

	omChangeButtonLabel  = GetString(IDS_CHANGES);

		
	if(ogDeicingApplName.IsEmpty())	
	{	
		m_CB_DeIcing.ShowWindow( SW_HIDE );
	}
	else
	{
		omChangeButtonLabel = CString("DC");
		CRect olRect;
		CRect olRectInfo;
		m_CB_Info.GetWindowRect(olRectInfo);
		ScreenToClient(olRectInfo);
		m_CB_Changes.GetWindowRect(olRect);
		ScreenToClient(olRect);
	
		olRect.right = olRectInfo.right;
		m_CB_Changes.MoveWindow(olRect);	
		m_CB_Changes.SetWindowText(omChangeButtonLabel);	
		m_CB_DeIcing.SetSecState(ogPrivList.GetStat("DESKTOP_CB_Deicing"));

	}

	GetPrivateProfileString(ogAppName, DE_ICING_WINDOW_CFG_NAME, "DE-ICING", pclCheck, sizeof pclCheck, __GetConfigPath());			
	omDeIcingWindowName = pclCheck ;		


	omChanges = ",,,,,";
	UpdateChangesButton();

	m_bottom = 95;

	return TRUE;  // return TRUE  unless you set the focus to a control
}

//*******************************************************
// MWO: 13.04.05: InitTab:
// We use a tab to read from systab where TANA=DCF AND FINA=FURN
// to identify if the table exists where the user can insert
// as many delay codes as he likes to. The functionality must
// be made available if the tab contains one row otherwise the
// Old functionality must be used instead where the delays are
// stored in the AFTTAB.
void CButtonListDlg::InitTab()
{
	m_Tab.ResetContent();
	m_Tab.SetHeaderString("TANA,FINA");
	m_Tab.SetLogicalFieldList("TANA,FINA");
	m_Tab.SetHeaderLengthString("100,100");
    m_Tab.SetCedaServerName(ogCommHandler.pcmRealHostName);
    m_Tab.SetCedaPort("3357");
    m_Tab.SetCedaHopo(pcgHome);
    m_Tab.SetCedaCurrentApplication("FIPS");
    m_Tab.SetCedaTabext("TAB");
    m_Tab.SetCedaUser( ogCommHandler.pcmUser);
    m_Tab.SetCedaWorkstation(ogCommHandler.pcmHostName);
    m_Tab.SetCedaSendTimeout(3);
    m_Tab.SetCedaReceiveTimeout(240);
    m_Tab.SetCedaRecordSeparator("\n");
    m_Tab.SetCedaIdentifier("FIPS");
    m_Tab.ShowHorzScroller( TRUE );
    m_Tab.EnableHeaderSizing( TRUE);

	if(m_Tab.CedaAction("RT", "SYSTAB", "TANA,FINA", "", "WHERE TANA='DCF' AND FINA='FURN'") == FALSE)
	{
		CString olMsg = "Error Occured reading systab\n" + m_Tab.GetLastCedaError();
		MessageBox(olMsg);
		return;
	}
	if(m_Tab.GetLineCount() > 0)
	{
		bgMulitpleDelayCodes = true;
	}

	char pclAllowMultiDelayCode[256];
	GetPrivateProfileString("FIPS", "ALLOW_MULTIPLE_DELAY_CODE", "-",pclAllowMultiDelayCode, sizeof pclAllowMultiDelayCode, __GetConfigPath() );

	if (stricmp(pclAllowMultiDelayCode,"N") == 0)
	{
		bgMulitpleDelayCodes = false;
	}

}

COLORREF CButtonListDlg::ChangesColor(CString& opChanges)
{
	if (opChanges.IsEmpty())
		return ::GetSysColor(COLOR_BTNFACE);;

	CString olNotRead = opChanges;
	int ilNotRead = atoi(olNotRead);

	if (ilNotRead == 0)
	{
		opChanges = "0";
		return ::GetSysColor(COLOR_BTNFACE);
	}
	else
	{
		if (ilNotRead > 0)
		{
			return	ogColors[IDX_ORANGE];
		}
		else
		{
			return	ogColors[IDX_LLGREEN];
		}
	}

	return ::GetSysColor(COLOR_BTNFACE);;
}

void CButtonListDlg::UpdateChangesButton()
{

	CStringArray olStrArray;
	ExtractItemList(omChanges, &olStrArray, ',');
	int ilItemCount = olStrArray.GetSize();
	if (ilItemCount != 6 || omChanges.IsEmpty())
	{
		if (omChanges == "-1")
			bgLoacationChangesStarted = false;

		omChanges = ",,,,,";
		ExtractItemList(omChanges, &olStrArray, ','); 
	}

	CString olChange = olStrArray.GetAt(0);
	m_CB_Changes.SetColors(ChangesColor(olChange), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

	CString olTmp;
	olTmp.Format("%s (%s)", omChangeButtonLabel,olChange);
	m_CB_Changes.SetWindowText(olTmp);
	m_CB_Changes.UpdateWindow();


	if (pogFlightDiagram != NULL)
		pogFlightDiagram->UpdateChangesButton(olStrArray.GetAt(0), ChangesColor(olStrArray.GetAt(0)));

	if (pogPosDiagram != NULL)
		pogPosDiagram->UpdateChangesButton(olStrArray.GetAt(1), ChangesColor(olStrArray.GetAt(1)));

	if (pogGatDiagram != NULL)
		pogGatDiagram->UpdateChangesButton(olStrArray.GetAt(2), ChangesColor(olStrArray.GetAt(2)));

	if (pogBltDiagram != NULL)
		pogBltDiagram->UpdateChangesButton(olStrArray.GetAt(3), ChangesColor(olStrArray.GetAt(3)));

	if (pogWroDiagram != NULL)
		pogWroDiagram->UpdateChangesButton(olStrArray.GetAt(4), ChangesColor(olStrArray.GetAt(4)));

	if (pogCcaDiagram != NULL)
		pogCcaDiagram->UpdateChangesButton(olStrArray.GetAt(5), ChangesColor(olStrArray.GetAt(5)));

}

void CButtonListDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		InfoDlg dlgInfo;
		dlgInfo.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam); 
	}
}

void CButtonListDlg::OnDestroy()
{
	WinHelp(0L, HELP_QUIT);
	CDialog::OnDestroy();
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CButtonListDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CButtonListDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

LONG CButtonListDlg::OnShowBcInMonitor(UINT wParam, LONG /*lParam*/)
{
	char *pclMyBc;
	pclMyBc = (char*)wParam;
	if(pomBcMonitor != NULL)
	{
		pomBcMonitor->InsertBc(CString(pclMyBc));
	}
	return TRUE;
}

LONG CButtonListDlg::OnTextBcMonitor(UINT wParam, LONG )
{
	if(pomBcMonitor != NULL)
	{
		pomBcMonitor->InsertCount();
	}
	return TRUE;
}

// Automation servers should not exit when a user closes the UI
//  if a controller still holds on to one of its objects.  These
//  message handlers make sure that if the proxy is still in use,
//  then the UI is hidden but the dialog remains around if it
//  is dismissed.

LONG CButtonListDlg::OnBcAdd(UINT wParam, LONG /*lParam*/)
{
	if(bgUseBccom)
		return 0L;
	
	
	//	return TRUE;
	TRACE("Broadcast num %d\n",wParam);

	m_CB_BCStatus.SetColors(RGB(236, 236,   0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_BCStatus.UpdateWindow();

	bool blStatus = ogBcHandle.GetBc(wParam);

	bmBCOK = true;

	m_CB_BCStatus.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_BCStatus.UpdateWindow();

	omLastBcTime = CTime::GetCurrentTime();

	return TRUE;

/*


	ProcessBcTest();
	
	if (bmBCOK)
	{
		m_CB_BCStatus.SetColors(RGB(236, 236,   0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		m_CB_BCStatus.UpdateWindow();
	}

	ogBcHandle.GetBc(wParam);
	//ogBcHandle.GetBc();

	if (bmBCOK)
	{
		m_CB_BCStatus.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		m_CB_BCStatus.UpdateWindow();
	}
	return TRUE;
*/
}



void CButtonListDlg::UpdateTrafficLight(int ipState)
{
	if (::IsWindow(m_hWnd)) // after closing OPSSPM, this callback function may still be called causing crash in KillTimer()
	{
		KillTimer(3);
		if(ipState == 0)
		{

			m_CB_BCStatus.SetColors(RGB(236, 236,   0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			m_CB_BCStatus.UpdateWindow();
		}
		else if(ipState == 1)
		{
			m_CB_BCStatus.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			m_CB_BCStatus.UpdateWindow();
		}

		SetTimer(3, 60 * 1000,NULL);
		omLastBcTime = CTime::GetCurrentTime();
	}

}





void CButtonListDlg::TestBc()
{
#ifndef SATS_PERFORMANCE		// SendBC is not called in SATSPerformance version
	omBcTestData.SendBC("TIMERS_FIPS", "", "", "");
#endif	// SATS_PERFORMANCE
}

void CButtonListDlg::ActivateTimer()
{
	SetTimerCheck(2, m_SpinCheck.GetPos());
}

void CButtonListDlg::DeActivateTimer()
{
	KillTimer(2);
}

void CButtonListDlg::OnTimer(UINT nIDEvent)
{
	if (nIDEvent == 1)
	{
		CTime olLocalTime = CTime::GetCurrentTime();
		CTime olUtcTime = CTime::GetCurrentTime();
		ogBasicData.LocalToUtc(olUtcTime);

		char pclTimes[100];
		char pclDate[100];
		
		CTime olUtcTimeTmp = CTime::GetCurrentTime();
		olUtcTimeTmp -= ogUtcDiff;
		
		if(ogUtcDiff != 0)
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
		}
		else
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
		}

		omTime.SetWindowText(pclTimes);
		omTime.Invalidate(FALSE);

		omDate.SetWindowText(pclDate);
		omDate.Invalidate(FALSE);


		long ilSeconds = (CTime::GetCurrentTime()- omLastBcTime).GetTotalSeconds(); 
		if (ilSeconds >= 120)
			m_CB_BCStatus.SetColors(RED, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

/*

	CTime olTime = CTime::GetCurrentTime();
	TRACE("\nBegin ConflictCheckTimeline  %s", olTime.Format("%H:%M:%S"));

		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
		ogKonflikte.SASCheckAll(!bgConfCheck, true);
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	olTime = CTime::GetCurrentTime();
	TRACE("\nEnd ConflictCheckTimeline  %s", olTime.Format("%H:%M:%S"));
	TRACE("\nNumber of Flights %d\n",ogUrnoMapConfCheckTimer.GetCount());

*/

	}
	else if (nIDEvent == 2)
	{
		DeActivateTimer();
		CTime olTime = CTime::GetCurrentTime();
		TRACE("\nBegin ConflictCheck  %s", olTime.Format("%H:%M:%S"));

	//		ogUrnoMapConfCheck.RemoveAll();

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
			bgConfCheckRuns = true;
			ogKonflikte.SASCheckAll(!bgConfCheck);
			bgConfCheckRuns = false;
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

		olTime = CTime::GetCurrentTime();
		TRACE("\nEnd ConflictCheck  %s", olTime.Format("%H:%M:%S"));
		TRACE("\nNumber of Flights %d\n",ogUrnoMapConfCheck.GetCount());
		ActivateTimer();

		return;

		CString olText;
		int ilPos ;
		m_Check.GetWindowText(olText);
		ilPos = atoi(olText);

		if (!bgConfCheck && ilPos > 0)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));


			POSITION pos;
			for( pos = ogUrnoMapConfCheck.GetStartPosition(); pos != NULL; )
			{
				DIAFLIGHTDATA *prlFlight = NULL;
				long llUrno = 0;
				ogUrnoMapConfCheck.GetNextAssoc(pos, (void *&)llUrno, (void *&)prlFlight);
				if(prlFlight != NULL)
				{
					if (prlFlight->Urno > 0)
						ogPosDiaFlightData.CheckFlightConflicts(prlFlight);
				}
			}


			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		}


		ogUrnoMapConfCheck.RemoveAll();

/*
		int il = m_SpinCheck.GetPos();
		CString olText;
		m_Check.GetWindowText(olText);

		::MessageBox(NULL,olText,"",MB_OK);
*/
	}
}


bool CButtonListDlg::Exit() 
{
	if (CanExit())
	{
		CleanUp();
		BOOL ok = WriteDialogToReg((CWnd*) this, m_key);

		//lgpClosing
		char e[]={"FIPS.exe"};

		Terminate(e);
		GetParent()->SendMessage(WM_CLOSE);
		
//		CDialog::OnOK();
		if(pomBcMonitor != NULL)
		{
			delete pomBcMonitor;
		}
		return true;
	}
	return false;
}

void CButtonListDlg::Terminate(char *a) 
{
	
	HANDLE handle; 
	HANDLE handle1; 
	handle=CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,0);
	PROCESSENTRY32 *info; 
	info=new PROCESSENTRY32; 
	info->dwSize=sizeof(PROCESSENTRY32); 
	Process32First(handle,info); 
	while(Process32Next(handle,info)!=FALSE) 
	{   
	   info->szExeFile;  
	   if( strcmp(a,info->szExeFile) == 0)   
	   {   
		handle1=OpenProcess(PROCESS_TERMINATE,FALSE,info->th32ProcessID); 
		TerminateProcess(handle1,0);
		WaitForSingleObject(handle1,50000);
	   }  

	} 

	CloseHandle(handle);
}

void CButtonListDlg::OnClose() 
{
	Exit();
}

void CButtonListDlg::OnOK() 
{
	OnKillfocusCheck() ;
//	Exit();
}

void CButtonListDlg::OnCancel() 
{
	OnBeenden();
}

BOOL CButtonListDlg::CanExit()
{
	// If the proxy object is still around, then the automation
	//  controller is still holding on to this application.  Leave
	//  the dialog around, but hide its UI.
	if (m_pAutoProxy != NULL)
	{
		ShowWindow(SW_HIDE);
		return FALSE;
	}

	return TRUE;
}


void CButtonListDlg::OnBeenden() 
{
	if (!pmExitDlg)
		pmExitDlg = new AskBox(this, GetString(ST_FRAGE), GetString(IDS_STRING980), GetString(IDS_STRING1985), GetString(IDS_STRING1986), CString(""));

	int ilExitDlg = pmExitDlg->DoModal();

	//print actual view
	if (ilExitDlg == 1)
		Exit();
	else
		return;

//	if(pomParent->MessageBox(GetString(IDS_STRING980), GetString(ST_FRAGE), MB_YESNO | MB_ICONQUESTION) == IDNO)
//		return;

//	Exit();
}

void CButtonListDlg::OnBC() 
{
	//bc_monitor
	char pclBCMon[256];
//	pomBcMonitor = NULL;

	CString	m_key = "DialogPosition\\BcMonitor";

    GetPrivateProfileString(ogAppName, "BCMONITOR", "FALSE", pclBCMon, sizeof pclBCMon, __GetConfigPath() );
	if(strcmp(pclBCMon, "FALSE") != 0)
	{
		if (pomBcMonitor)
		{
			BOOL ok = WriteDialogToReg((CWnd*) pomBcMonitor, m_key);
			pomBcMonitor->DestroyWindow();
			delete pomBcMonitor;
			pomBcMonitor = NULL;
		}

		if (pomBcMonitor == NULL)
			pomBcMonitor = new BC_Monitor(this);

		ogBcHandle.pomMsgDlg = 	(CWnd*)this; //pomBcMonitor;

	}
	//bc_monitor
}


void CButtonListDlg::CleanUp() 
{ 
	// Send EXIT-Message to CuteIF
	if (bgComOk)
	{
		if (bmIsCuteIFStarted) // soll in der Zukunft von UfisAppManag ermittelt werden
		{
			CallCuteIF(CString("EXIT"));
		}
		pConnect->DetachApp(static_cast<long>(Fips));
		bgComOk = false;
	}

	//RST-REL
	ogCommHandler.CleanUpCom();	

	ogExandData.DeleteAll();

	CCS_TRY
		if (UIFappl)
		{
			delete UIFappl;
		}
		ogUrnoMapConfCheck.RemoveAll();
		ogUrnoMapConfCheckTimer.RemoveAll();

		if (pmExitDlg != NULL)
		{
			pmExitDlg->DestroyWindow();
			delete pmExitDlg;
			pmExitDlg = NULL;
		}


		if (pogFlightSearchTableDlg != NULL)
		{
			pogFlightSearchTableDlg->DestroyWindow();
			delete pogFlightSearchTableDlg;
			pogFlightSearchTableDlg = NULL;
		}

		if (pogReportSelectDlg != NULL)
		{
			pogReportSelectDlg->DestroyWindow();
			delete pogReportSelectDlg;
			pogReportSelectDlg = NULL;
		}

		if (pogSeasonCollectDlg != NULL)
		{
			pogSeasonCollectDlg->DestroyWindow();
			delete pogSeasonCollectDlg;
			pogSeasonCollectDlg = NULL;
		}

		if (pogRotationDlg != NULL && ::IsWindow(this->m_hWnd))
		{
			pogRotationDlg->DestroyWindow();
			delete pogRotationDlg;
			pogRotationDlg = NULL;
		}

		if (pogCommonCcaDlg != NULL)
		{
			pogCommonCcaDlg->DestroyWindow();
			delete pogCommonCcaDlg;
			pogCommonCcaDlg = NULL;
		}

		if (pogSeasonDlg != NULL)
		{
			pogSeasonDlg->DestroyWindow();
			delete pogSeasonDlg;
			pogSeasonDlg = NULL;
		}


		if (pogSeasonTableDlg != NULL)
		{
			pogSeasonTableDlg->DestroyWindow();
			delete pogSeasonTableDlg;
			pogSeasonTableDlg = NULL;
		}

		if (pogRotationTables != NULL)
		{
			pogRotationTables->DestroyWindow();
			delete pogRotationTables;
			pogRotationTables = NULL;
		}

		if (pogDailyScheduleTableDlg != NULL)
		{
			pogDailyScheduleTableDlg->DestroyWindow();
			delete pogDailyScheduleTableDlg;
			pogDailyScheduleTableDlg = NULL;
		}

		if (pogFlightDiagram != NULL)
		{
			pogFlightDiagram->DestroyWindow();
			delete pogFlightDiagram;
			pogFlightDiagram = NULL;
		}

		if (pogCcaDiagram != NULL)
		{
			pogCcaDiagram->DestroyWindow();
			delete pogCcaDiagram;
			pogCcaDiagram = NULL;
		}

		if (pogPosDiagram != NULL)
		{
			pogPosDiagram->DestroyWindow();
			delete pogPosDiagram;
			pogPosDiagram = NULL;
		}

		if (pogGatDiagram != NULL)
		{
			pogGatDiagram->DestroyWindow();
			delete pogGatDiagram;
			pogGatDiagram = NULL;
		}

		if (pogBltDiagram != NULL)
		{
			pogBltDiagram->DestroyWindow();
			delete pogBltDiagram;
			pogBltDiagram = NULL;
		}

		if (pogWroDiagram != NULL)
		{
			pogWroDiagram->DestroyWindow();
			delete pogWroDiagram;
			pogWroDiagram = NULL;
		}

		if (pogWoResTableDlg != NULL)
		{
			pogWoResTableDlg->DestroyWindow();
			delete pogWoResTableDlg;
			pogWoResTableDlg = NULL;
		}

		if (pogKonflikteDlg != NULL)
		{
			pogKonflikteDlg->DestroyWindow();
			delete pogKonflikteDlg;
			pogKonflikteDlg = NULL;
		}

		if (pogRotGroundDlg != NULL)
		{
			pogRotGroundDlg->DestroyWindow();
			delete pogRotGroundDlg;
			pogRotGroundDlg = NULL;
		}

		if (pogSpecialConflictDlg != NULL)
		{
			pogSpecialConflictDlg->DestroyWindow();
			delete pogSpecialConflictDlg ;
			pogSpecialConflictDlg = NULL;
		}



		CedaAptLocalUtc::FreeCedaAptLocalUtc ();  


	CCS_CATCH_ALL
}



void CButtonListDlg::OnFlugplanErfassen() 
{

	// dynamische DailyTable fuer Shanghai

	// to configure the header of the table
		CStringArray olColumnsToShow; 
/*
		// Arrival
		olColumnsToShow.Add("AFLNO");
		olColumnsToShow.Add("ASTOADate");
		olColumnsToShow.Add("ASTOATime");
		olColumnsToShow.Add("ATTYP");
		olColumnsToShow.Add("AVIA4");
		olColumnsToShow.Add("AVN");
		olColumnsToShow.Add("AAIRB");
		olColumnsToShow.Add("ATMOA");
		olColumnsToShow.Add("AETAI");
		olColumnsToShow.Add("ALAND");
		olColumnsToShow.Add("APSTA");
		olColumnsToShow.Add("ABLT1/ABLT2");
		olColumnsToShow.Add("VIP");
		olColumnsToShow.Add("AREMP/DREMP");

		// Rotation
		olColumnsToShow.Add("AREGN");
		olColumnsToShow.Add("AACT5");

		// Departure
		olColumnsToShow.Add("DFLNO");
		olColumnsToShow.Add("DSTODDate");
		olColumnsToShow.Add("DSTODTime");
		olColumnsToShow.Add("DTTYP");
		olColumnsToShow.Add("DDES4");
		olColumnsToShow.Add("DVN");
		olColumnsToShow.Add("DETDI");
		olColumnsToShow.Add("DAIRB");
		olColumnsToShow.Add("DPSTD");
		olColumnsToShow.Add("DGTD1");
		olColumnsToShow.Add("DGTD2");
		olColumnsToShow.Add("DCKIF/DCKIT");
	
*/
		// Arrival
		olColumnsToShow.Add("AFLNO");		//arr flnr
		
		if ((strcmp(pcgHome, "ATH") == 0))
			olColumnsToShow.Add("ASTOADateFull");	//date
		else
			olColumnsToShow.Add("ASTOADate");	//date

		olColumnsToShow.Add("AFLTI");		//flight id
		olColumnsToShow.Add("ATTYP");		//nature
		olColumnsToShow.Add("AORG3");		//orig
		olColumnsToShow.Add("ASTEV");		//Keycode 1
/*
		if (bgAddKeyCodesDR)
		{
			olColumnsToShow.Add("ASTE2");		//Keycode 2
			olColumnsToShow.Add("ASTE3");		//Keycode 3
			olColumnsToShow.Add("ASTE4");		//Keycode 4
		}
*/
		olColumnsToShow.Add("AVIA3L");		//last via
		olColumnsToShow.Add("AAIRB");		//atd
		olColumnsToShow.Add("ASTOATime");	//sta
		olColumnsToShow.Add("AETAI");		//eta
		olColumnsToShow.Add("ATMOA");		//tmo
		olColumnsToShow.Add("ALAND/AONLB");	//ata/obl
		if( __CanSeeDailyRotationsColumnBelts() )		// 050308 MVy: depending on ceda ini entry, show Belts column 
			olColumnsToShow.Add("X_BLT1/BLT2");		// 050308 MVy: baggage belts; define field
		olColumnsToShow.Add("APSTA/DPSTD");	//pos arr/dep

		/*
		if(bgExcelFLBag)
		{
			olColumnsToShow.Add("AB1BA");		//tmo
			olColumnsToShow.Add("AB1EA");		//tmo
		}
		*/

		// Rotation
		olColumnsToShow.Add("RACT3");		//a/c
		olColumnsToShow.Add("RREGN");		//registr

		// Departure
		olColumnsToShow.Add("DFLNO");		//dep flnr


		if ((strcmp(pcgHome, "ATH") == 0))
			olColumnsToShow.Add("DSTODDateFull");	//date
		else
			olColumnsToShow.Add("DSTODDate");	//date

		olColumnsToShow.Add("DFLTI");		//flight id
		olColumnsToShow.Add("DTTYP");		//nature
		olColumnsToShow.Add("DVIA3L");		//next via 
		olColumnsToShow.Add("DDES3");		//dest

		olColumnsToShow.Add("DSTEV");		//Keycode 1

/*
		if (bgAddKeyCodesDR)
		{
			olColumnsToShow.Add("DSTE2");		//Keycode 2
			olColumnsToShow.Add("DSTE3");		//Keycode 3
			olColumnsToShow.Add("DSTE4");		//Keycode 4
		}
*/
		olColumnsToShow.Add("DSTODTime");	//std
		olColumnsToShow.Add("DETDI");		//etd
		olColumnsToShow.Add("DOFBL/DAIRB");	//ofbl/airb
		olColumnsToShow.Add("DCKIF/DCKIT");	//cki
		olColumnsToShow.Add("DGTD1/DGTD2");	//gate

		if( __CanSeeDailyRotationsColumnVIPs() )	// 050309 MVy: depending on ceda ini entry, show VIPs column 
			olColumnsToShow.Add("X_VIP");					// 050308 MVy: VIP added

		if( __CanSeeDailyRotationsColumnRemarks() )	// 050309 MVy: depending on ceda ini entry, show Remarks column 
			olColumnsToShow.Add("X_REMP");				// 050308 MVy: Remark added

		// Dialog nicht mehr/noch nicht offen
		if (pogDailyScheduleTableDlg == NULL)
		{
			pogDailyScheduleTableDlg = new DailyScheduleTableDlg(olColumnsToShow, this);
			// StringArray mit den anzuzeigenden Spalten uebergeben
			//pogDailyScheduleTableDlg->SetColumnsToShow(olColumnsToShow);
			//pogDailyScheduleTableDlg->ShowAnsicht();
		}
		else
		{
			pogDailyScheduleTableDlg->SetWindowPos(&wndTop, 0,0,0,0, 
												   SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE);
		}

		olColumnsToShow.RemoveAll();
}

void CButtonListDlg::OnFlugplanPflegen() 
{
	if (pogSeasonTableDlg == NULL)
	{
		pogSeasonTableDlg = new CSeasonTableDlg(this);
		//pogSeasonTableDlg->ShowAnsicht();
	}
	else
	{
		pogSeasonTableDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}
}


void CButtonListDlg::OnTagesflugplanErfassen() 
{
	if (pogRotationTables == NULL)
	{
		pogRotationTables = new RotationTables(this);
	}
	else
	{
		pogRotationTables->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}
}


void CButtonListDlg::OnDrucken() 
{
	// TODO: Add your control notification handler code here
	
}

void CButtonListDlg::OnHilfe() 
{
	//AfxGetApp()->WinHelp(0,HELP_FINDER);
	//AfxGetApp()->WinHelp(0,HELP_CONTENTS);

	char File[64];

	strcpy(File, ogAppName);
	strcat(File, ".HLP");

	CWnd *pwnd = AfxGetMainWnd();
	
	ASSERT(pwnd);

//---------------------------------------------------
//Neu MWO: Da AatHelp das Hilfe Fenster unter den ButtonListDlg schiebt und man nicht dran kommt
	CString omFileName;
	CString omPath;
	char pclTmpText[512];

	GetPrivateProfileString("GLOBAL", "HELPDIRECTORY", "c:\\Ufis\\Help",pclTmpText,sizeof pclTmpText, __GetConfigPath() );
	omFileName = pclTmpText;												
	if(!omFileName.IsEmpty())
	{
		GetPrivateProfileString(ogAppName, "HELPFILE", ogAppName + ".chm",pclTmpText,sizeof pclTmpText, __GetConfigPath() );
		if (!omFileName.IsEmpty())
		{
			if (omFileName[omFileName.GetLength() - 1] != '\\')
				omFileName += '\\';
			omPath = omFileName;												
			omPath += "HH.exe";												
			omFileName += pclTmpText;
			char *args[4];
			args[0] = "child";
			args[1] = omFileName.GetBuffer(0);
			args[2] = NULL;
			args[3] = NULL;
//			int ilReturn = _spawnv(_P_NOWAIT,"C:\\Winnt\\HH.exe",args);
			int ilReturn = _spawnv(_P_NOWAIT,omPath,args); 
		}
	}

	//	AfxGetApp()->WinHelp(0, HELP_CONTENTS);
	//::WinHelp(pwnd->m_hWnd, File, HELP_FINDER, 0);
//End MWO
}

void CButtonListDlg::OnSetup() 
{
	SetupDlg olDlg;
	olDlg.DoModal();

}


void CButtonListDlg::OnInfo() 
{
	AirportInformationDlg olInfoDlg;//(pomBackGround)
	olInfoDlg.DoModal();
	m_CB_Info.SetColors(SILVER,GRAY,WHITE);
}



void CButtonListDlg::OnCoverage() 
{
/*
//	start logtab_viewer
	char pclExcelPath[256];


	GetPrivateProfileString(ogAppName, "LOGTABVIEWER", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, __GetConfigPath() );


	if(!strcmp(pclExcelPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING2540), GetString(ST_FEHLER), MB_ICONERROR);


	char *args[4];
	char slRunTxt[256] = "";
	char buffer[64] = "";
	args[0] = "child";
	sprintf(slRunTxt,"Debug");
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
*/
}


void CButtonListDlg::OnFlugdiagramm() 
{
	CRect olRect;
	GetWindowRect(&olRect);

	if(	pogFlightDiagram  == NULL)
	{	
		pogFlightDiagram = new FlightDiagram();
		pogFlightDiagram->Create(NULL, GetString(IDS_STRING1257), WS_HSCROLL | WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_MAXIMIZEBOX | WS_SYSMENU | WS_MINIMIZEBOX ,
			CRect(0, m_bottom, olRect.right/*GetSystemMetrics(SM_CXSCREEN)*/, GetSystemMetrics(SM_CYSCREEN)),
			this,NULL,0,NULL);
	}	
	else
	{
		pogFlightDiagram->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}
}




void CButtonListDlg::OnRegeln() 
{
		char pclExcelPath[256];

		GetPrivateProfileString(ogAppName, "RULES", "DEFAULT",
			pclExcelPath, sizeof pclExcelPath, __GetConfigPath() );

		if(!strcmp(pclExcelPath, "DEFAULT"))
			MessageBox(GetString(IDS_STRING1478), GetString(ST_FEHLER), MB_ICONERROR);


		/*
		HWND hw=::FindWindow("#32770","Duty Preferences");
		//::ShowWindow(hw,SW_RESTORE   );
		::ShowWindow(hw,SW_MINIMIZE);
		::ShowWindow(hw,SW_SHOWNORMAL      );
		//::SetActiveWindow(hw);
		*/

		char *args[4];
		char slRunTxt[256] = "";
		args[0] = "child";
		sprintf(slRunTxt,"%s,%s",pcgUser,pcgPasswd);
		args[1] = slRunTxt;
		args[2] = NULL;
		args[3] = NULL;
		int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);

}


void CButtonListDlg::UpdateFlightDia() 
{
	if (pogFlightDiagram)
		pogFlightDiagram->RedisplayAll();
}

void CButtonListDlg::OnRueckgaengig() 
{
		char pclExcelPath[256];

		GetPrivateProfileString(ogAppName, "USER_BUTTON_PATH", "DEFAULT",
			pclExcelPath, sizeof pclExcelPath, __GetConfigPath() );


		if(!strcmp(pclExcelPath, "DEFAULT"))
			MessageBox(GetString(IDS_STRING1478), GetString(ST_FEHLER), MB_ICONERROR);


		/*
		HWND hw=::FindWindow("#32770","Duty Preferences");
		//::ShowWindow(hw,SW_RESTORE   );
		::ShowWindow(hw,SW_MINIMIZE);
		::ShowWindow(hw,SW_SHOWNORMAL      );
		//::SetActiveWindow(hw);
		*/

		char *args[4];
		char slRunTxt[256] = "";
		args[0] = "child";
		sprintf(slRunTxt,"%s,%s",pcgUser,pcgPasswd);
		args[1] = slRunTxt;
		args[2] = NULL;
		args[3] = NULL;
		int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);


}

/*
void CButtonListDlg::OnOnline() 
{

	if(bmOnline)
	{
		m_CB_Online.SetWindowText(GetString(IDS_STRING1258));
		m_CB_Online.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, true);

		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, true);

		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, true);

		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("SPR"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("ISF"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("UFR"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("DFR"), BC_FLIGHT_DELETE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("JOF"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("UPS"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("UPJ"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("IFR"), BC_FLIGHT_CHANGE, true);

		ogBcHandle.TableCommandSetOnlyOwn(CString("CCA") + CString(pcgTableExt), CString("IRT"), BC_CCA_NEW, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("CCA") + CString(pcgTableExt), CString("DRT"), BC_CCA_DELETE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("CCA") + CString(pcgTableExt), CString("URT"), BC_CCA_CHANGE, true);

	}
	else
	{
		m_CB_Online.SetWindowText(GetString(IDS_STRING1259));
		m_CB_Online.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));		

		
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, false);

		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, false);

		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, false);

		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("SPR"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("ISF"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("UFR"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("DFR"), BC_FLIGHT_DELETE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("JOF"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("UPS"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("UPJ"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("IFR"), BC_FLIGHT_CHANGE, false);

		ogBcHandle.TableCommandSetOnlyOwn(CString("CCA") + CString(pcgTableExt), CString("IRT"), BC_CCA_NEW, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("CCA") + CString(pcgTableExt), CString("DRT"), BC_CCA_DELETE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("CCA") + CString(pcgTableExt), CString("URT"), BC_CCA_CHANGE, false);
		
		ogDdx.DataChanged((void *)this, DATA_RELOAD, NULL);

		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));		

	}
	bmOnline = !bmOnline;

}


*/



void CButtonListDlg::OnACIn() 
{
	CAcirrDlg olAcirrDlg(this,"");
	olAcirrDlg.DoModal();
}


void CButtonListDlg::OnStammdaten() 
{
	char pclExcelPath[256];

  GetPrivateProfileString(ogAppName, "BDPS-UIF", "DEFAULT",
      pclExcelPath, sizeof pclExcelPath, __GetConfigPath() );


	if(!strcmp(pclExcelPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING981), GetString(ST_FEHLER), MB_ICONERROR);

	//CString	olTables = "ALTACTACRAPTRWYTWYPSTGATCICBLTEXTDENMVTNATHAGWROHTYSTYFID-STRSPHSEA";//Alle
	// Update der Liste von ARE 07.05.99
	//CString	olTables = "ALT ACT ACR APT RWY TWY PST GAT CIC BLT EXT DEN MVT NAT HAG WRO HTY STY FID STR SPH SEA GHS GEG PER PEF GRM GRN ORG PFC COT ASF BSS BSD ODA TEA STF HTC WAY PRC CHT TIP HOL WGP SWG";
	// Update der Liste von ARE 17.05.99
	CString	olTables = "ALT-ACT-ACR-APT-RWY-TWY-PST-GAT-CIC-BLT-EXT-DEN-MVT-NAT-HAG-WRO-HTY-STY-FID-STR-SPH-SEA-GHS-GEG-PER-PEF-GRM-GRN-ORG-PFC-COT-ASF-BSS-BSD-ODA-TEA-STF-HTC-WAY-PRC-CHT-TIP-BLK-HOL-WGP-SWG-PGP-AWI-CCC-VIP";


	char *args[4];
	char slRunTxt[256];
	args[0] = "child";
	sprintf(slRunTxt,"%s,%s,%s,%s",ogAppName,pcgUser,pcgPasswd,olTables);
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
}




void CButtonListDlg::OnTelexpool() 
{
	CallTelexpool(-1, "", "", "", "", "", "", "", "", TIMENULL, TIMENULL);
}



void CButtonListDlg::CallTelexpool(long lpUrno, const CString &ropAlc3, const CString &ropFltn,
								   const CString &ropFlns, const CString &ropTifFrom, const CString &ropTifTo,
								   const CString &ropRegn, const CString &ropFlda, const CString &ropAdid, const CTime &ropStoa, const CTime &ropStod) 
{
	// The product works with the new telexpool

	//if ((strcmp(pcgHome, "ATH") == 0 || strcmp(pcgHome, "DXB") == 0) && bgComOk)
	if (bgComOk)
		CallTelexpoolNew(lpUrno, ropAlc3, ropFltn, ropFlns, ropRegn, ropFlda, ropAdid, ropStoa, ropStod);
	//else
	//	CallTelexpoolOld(ropAlc3, ropFltn, ropFlns, ropTifFrom, ropTifTo);

}


void CButtonListDlg::CallTelexpoolNew(long lpUrno, const CString &ropAlc3, const CString &ropFltn, const CString &ropFlns, 
									  const CString &ropRegn, const CString &ropFlda, const CString &ropAdid, const CTime &ropStoa, const CTime &ropStod) 
{

	CString olSendStr;
	CString olFieldList("URNO,ALC3,FLTN,FLNS,REGN,FLDA,ADID,STOA,STOD");
	CString olData;

	//PRF  8476 User added
	olData.Format("%ld,%s,%s,%s,%s,%s,%s,%s,%s %s", lpUrno, ropAlc3, ropFltn, ropFlns, ropRegn, ropFlda,
		ropAdid, ropStoa.Format("%Y%m%d%H%M00"), ropStod.Format("%Y%m%d%H%M00"), CString(pcgUser));


	olSendStr.Format("%s\n%s", olFieldList, olData);

 	_bstr_t tmpStr = olSendStr.GetBuffer(olSendStr.GetLength());


	try
	{
		pConnect->TransferData(static_cast<long>(TlxPool),
							   static_cast<long>(Fips),tmpStr);
	}
	catch(_com_error error)
	{
		CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2010), GetString(ST_FEHLER), MB_ICONERROR | MB_OK);
		return;
	}


	return;
}



void CButtonListDlg::CallTelexpoolOld(const CString &ropAlc3, const CString &ropFltn, const CString &ropFlns,
									  const CString &ropTifFrom, const CString &ropTifTo) 
{
	
	CWaitCursor olWaitCursor; // Wait cursor displayed on construction and removed on destruction

	char pclData[500];

	CString olTtyp("");
	sprintf(pclData,"%s,%s,%s,%s,%s,%s", ropAlc3, ropFltn, ropFlns, ropTifFrom, ropTifTo, olTtyp);

	DWORD olDdeInstance = 0;
	HSZ olApplHandle;
	HSZ olTopicHandle;
	bool blRc = true;

	// initialize DDE
	if( DdeInitialize(&olDdeInstance, (PFNCALLBACK) DdeCallback, APPCMD_FILTERINITS, 0) != DMLERR_NO_ERROR )
	{
		HandleDdeError(olDdeInstance, "RunTool - Error Initializing DDE !");
		AfxMessageBox(GetString(IDS_STRING982));
		blRc = false;
	}
	else
	{
		TRACE("DDE Initialized Successfully.\n");
	}

	// DDE - get a handle to the telexpool application string
	if( blRc )
	{
		olApplHandle = DdeCreateStringHandle(olDdeInstance, "TelexPool", CP_WINANSI);
		if( olApplHandle == 0 )
		{
			HandleDdeError(olDdeInstance, "RunTool - Error Creating a DDE String handle !");
			AfxMessageBox(GetString(IDS_STRING982));
			blRc = false;
		}
		else
		{
			TRACE("Created DDE string handle for the application.\n");
		}
	}

	// DDE - get a handle to a telexpool topic string
	if( blRc )
	{
		olTopicHandle = DdeCreateStringHandle(olDdeInstance, pcgUser, CP_WINANSI);
		if( olApplHandle == 0 )
		{
			HandleDdeError(olDdeInstance, "RunTool - Error Creating a DDE String handle !");
			AfxMessageBox(GetString(IDS_STRING982));
			blRc = false;
		}
		else
		{
			TRACE("Created DDE string handle for a topic.\n");
		}
	}

	// attempt to connect to telexPool
	if( blRc )
	{
		HCONV olConversation = DdeConnect(olDdeInstance, olApplHandle, olTopicHandle, (PCONVCONTEXT) NULL);
		if( olConversation == NULL )
		{
			DWORD olErr = HandleDdeError(olDdeInstance, "RunTool - Error Connecting to the telexpool");

			if(olErr == DMLERR_NO_CONV_ESTABLISHED)
			{
				// cannot connect to telex pool so spawn a new telexpool
				RunTelexPool(ropAlc3, ropFltn, ropFlns, ropTifFrom, ropTifTo, olTtyp);
			}
			else
			{
				// error in connection attempt
				AfxMessageBox(GetString(IDS_STRING982));
			}
		}
		else // connected to the telexpool, so update the data
		{
			// create a handle to the data to be sent
			HDDEDATA olDataHandle;

			// send the data
//				DWORD ilTimeout = 20000; // 20 second timeout
			DWORD ilResult;
//				olDataHandle = DdeClientTransaction((LPBYTE) pclData, strlen(pclData)+1, olConversation, olApplHandle, CF_TEXT, XTYP_POKE, ilTimeout, &ilResult);
			olDataHandle = DdeClientTransaction((LPBYTE) pclData, strlen(pclData)+1, olConversation, olApplHandle, CF_TEXT, XTYP_POKE, TIMEOUT_ASYNC, &ilResult);
			if( olDataHandle == 0 )
			{
				switch(ilResult)
				{
				case DDE_FACK:

					// this is SUCCESS !
					TRACE("DdeClientTransaction returns DDE_FACK\n");
					break;

				case DDE_FBUSY:

					// a modal telexpool dialog was open or the process was busy - tell the user to retry later
//						AfxMessageBox("The telexpool is currently busy.\nPlease check if any telexpool dialogs are open, if so close them and retry.\nIf this error persists contact your system adminstrator.");
					TRACE("DdeClientTransaction returns DDE_FBUSY\n");
					break;

				case DDE_FNOTPROCESSED:

					AfxMessageBox(GetString(IDS_STRING982));
					TRACE("DdeClientTransaction returns DDE_FNOTPROCESSED\n");
					break;

				default:
					DWORD olRc = HandleDdeError(olDdeInstance, "RunTool - Error DdeClientTransaction()");
//						AfxMessageBox("Error cannot communicate with the telexpool, please contact your system adminstrator.");
					break;
				}
			}

			// free the data handle
//				if( DdeFreeDataHandle(olDataHandle) == 0 )
//				{
//					HandleDdeError(olDdeInstance, "RunTool - Error DdeFreeDataHandle()");
//					blRc = false;
//				}
		}
	}



	// end the DDE communication
	if( olDdeInstance != 0 )
	{
		if( DdeFreeStringHandle(olDdeInstance, olApplHandle) == FALSE )
			TRACE("Error deleting the application's DDE string handle !\n");
		else
			TRACE("Deleted the application's DDE string handle.\n");

		if( DdeFreeStringHandle(olDdeInstance, olTopicHandle) == FALSE )
			TRACE("Error deleting the topic's DDE string handle !\n");
		else
			TRACE("Deleted the topic's DDE string handle.\n");

		if(DdeUninitialize(olDdeInstance) == 0)
			TRACE("Error Uninitialzing!\n");
		else
			TRACE("Uninitialized the connection.\n");
	}


//	AfxGetApp()->DoWaitCursor(-1);


}


// debug logging of DDE errors
DWORD CButtonListDlg::HandleDdeError(DWORD opDdeInstance, const char *pcpUserText)
{
	DWORD olErr = DdeGetLastError(opDdeInstance);
	CString olErrText;

	switch(olErr)
	{
	case DMLERR_ADVACKTIMEOUT:
		olErrText = "DMLERR_ADVACKTIMEOUT";
		break;
	case DMLERR_BUSY:
		olErrText = "DMLERR_BUSY";
		break;
	case DMLERR_DATAACKTIMEOUT:
		olErrText = "DMLERR_DATAACKTIMEOUT";
		break;
	case DMLERR_EXECACKTIMEOUT:
		olErrText = "DMLERR_EXECACKTIMEOUT";
		break;
	case DMLERR_NOTPROCESSED:
		olErrText = "DMLERR_NOTPROCESSED";
		break;
	case DMLERR_POKEACKTIMEOUT:
		olErrText = "DMLERR_POKEACKTIMEOUT";
		break;
	case DMLERR_POSTMSG_FAILED:
		olErrText = "DMLERR_POSTMSG_FAILED";
		break;
	case DMLERR_REENTRANCY:
		olErrText = "DMLERR_REENTRANCY";
		break;
	case DMLERR_SERVER_DIED:
		olErrText = "DMLERR_SERVER_DIED";
		break;
	case DMLERR_UNADVACKTIMEOUT:
		olErrText = "DMLERR_UNADVACKTIMEOUT";
		break;
	case DMLERR_MEMORY_ERROR:
		olErrText = "DMLERR_MEMORY_ERROR";
		break;
	case DMLERR_NO_CONV_ESTABLISHED:
		olErrText = "DMLERR_NO_CONV_ESTABLISHED";
		break;
	case DMLERR_DLL_NOT_INITIALIZED:
		olErrText = "DMLERR_DLL_NOT_INITIALIZED";
		break;
	case DMLERR_DLL_USAGE:
		olErrText = "DMLERR_DLL_USAGE";
		break;
	case DMLERR_INVALIDPARAMETER:
		olErrText = "DMLERR_INVALIDPARAMETER";
		break;
	case DMLERR_SYS_ERROR:
		olErrText = "DMLERR_SYS_ERROR";
		break;
	case DMLERR_NO_ERROR:
		olErrText = "DMLERR_NO_ERROR";
		break;
	default:
		olErrText = "Undefined Error";
		break;
	}
	TRACE("%s - %s !\n",pcpUserText,olErrText);

	return olErr;
}


// callback function to handle DDE transactions
HDDEDATA CALLBACK
DdeCallback(UINT uType, UINT uFmt, HCONV hConv, HSZ hszTopic, HSZ hszItem, HDDEDATA hData, DWORD lData1, DWORD lData2)
{
	TRACE("DdeCallback() Received message num %d !\n",uType);
    return((HDDEDATA)FALSE);
}








// spawns a new telexpool
void CButtonListDlg::RunTelexPool(CString opAlc, CString opFltn, CString opFlns, CString opTifFrom, CString opTifTo, CString opTtyp)
{

/*
	char *pclArgs[4];
	char clCmdLine[500];

	sprintf(clCmdLine,"%s,%s,%s,%s,%s,%s,%s,%s",dlg.m_Usid,dlg.m_Pass,dlg.m_Alc3,dlg.m_Fltn,dlg.m_Flns,dlg.m_Tifr,dlg.m_Tito,dlg.m_Ttyp);

	pclArgs[0] = "child";
	pclArgs[1] = clCmdLine;
	pclArgs[2] = NULL;
	_spawnv(_P_NOWAIT,dlg.m_Appl,pclArgs);
*/

	char pclPath[256];

    GetPrivateProfileString(ogAppName, "TELEXPOOL", "DEFAULT",
        pclPath, sizeof pclPath, __GetConfigPath() );


	if(!strcmp(pclPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING983), GetString(ST_FEHLER), MB_ICONERROR);


	char *pclArgs[4];
	char clCmdLine[500];

	sprintf(clCmdLine,"%s,%s,%s,%s,%s,%s,%s,%s",pcgUser,pcgPasswd,opAlc,opFltn,opFlns,opTifFrom,opTifTo,opTtyp);


	pclArgs[0] = "child";
	pclArgs[1] = clCmdLine;
	pclArgs[2] = NULL;
	_spawnv(_P_NOWAIT,pclPath,pclArgs);

}


static void ButtonListDlgCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CButtonListDlg *polDlg = (CButtonListDlg *)popInstance;
	//INFDATA	*polInf = (INFDATA *)vpDataPointer;
    if (ipDDXType == BC_INF_NEW || ipDDXType == BC_INF_CHANGE)
	{
//		polDlg->m_CB_Info.SetColors(RED,GRAY,WHITE);
		polDlg->m_CB_Info.SetColors(YELLOW,GRAY,WHITE);
/*		COLORREF olButtonColor = COLORREF(RGB(0,255,0));
		CString olButtonText;//CWnd SetBk
		olButtonText.Format(GetString(IDS_STRING1936), 12);

		polDlg->m_CB_Info.SetWindowText("EXTERN");
		polDlg->m_CB_Info.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		
		CDC* pDC = polDlg->m_CB_Info.GetDC();
		COLORREF ol = pDC->SetBkColor( olButtonColor );

		polDlg->m_CB_Info.UpdateWindow();
*/
	}
	if (ipDDXType == BROADCAST_CHECK)
	{
		struct BcStruct *prlRequestData;
		prlRequestData = (struct BcStruct *) vpDataPointer;
	}
    if (ipDDXType == BC_LOCCHNG)
	{
		BcStruct *prlBcStruct = (BcStruct *) vpDataPointer;

		if(prlBcStruct == NULL)
			return;

		CString olWks = CString(ogCommHandler.pcmReqId);
		CString olListener = olWks + CString(pcgUser);

		if (olListener != CString(prlBcStruct->Selection))
			return;

		polDlg->SetChanges(CString(prlBcStruct->Data));
	}
} 

void CButtonListDlg::SetChanges(CString opChanges)
{
	omChanges = opChanges;
//test//	omChanges = "150,10,20,30,40,50";
	UpdateChangesButton();
}

void CButtonListDlg::OnChanges() 
{
	CallHistory(CString("BLIPOSITION;GATE;BELT;LOUNGE;CHECKIN"));
}

void CButtonListDlg::OnCommoncca() 
{

		char pclExcelPath[256];

		GetPrivateProfileString(ogAppName, "PASSINFO", "DEFAULT",
			pclExcelPath, sizeof pclExcelPath, __GetConfigPath() );


		if(!strcmp(pclExcelPath, "DEFAULT"))
		{
			MessageBox(GetString(IDS_STRING984), GetString(ST_FEHLER), MB_ICONERROR);
			return;
		}


		char *args[4];
		char slRunTxt[256];
		args[0] = "child";
		sprintf(slRunTxt,"%s,%s",pcgUser,pcgPasswd);
		args[1] = slRunTxt;
		args[2] = NULL;
		args[3] = NULL;
		int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);

}

void CButtonListDlg::OnGepaeckbaender() 
{
	CRect olRect;
	GetWindowRect(&olRect);

	if(	pogBltDiagram  == NULL)
	{	
		pogBltDiagram = new BltDiagram();
		pogBltDiagram->Create(NULL, GetString(IDS_STRING1010), WS_HSCROLL | WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_MAXIMIZEBOX | WS_SYSMENU | WS_MINIMIZEBOX ,
			CRect(0, m_bottom, olRect.right/*GetSystemMetrics(SM_CXSCREEN)*/, GetSystemMetrics(SM_CYSCREEN)),
			this,NULL,0,NULL);
	}	
	else
	{
		pogBltDiagram->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}

	UpdateChangesButton();
}




void CButtonListDlg::OnChutes() 
{
	CRect olRect;
	GetWindowRect(&olRect);

	if(	pogChaDiagram  == NULL)
	{	
		pogChaDiagram = new ChaDiagram();
		pogChaDiagram->Create(NULL, GetString(IDS_STRING1010), WS_HSCROLL | WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_MAXIMIZEBOX | WS_SYSMENU | WS_MINIMIZEBOX ,
			CRect(0, m_bottom, olRect.right/*GetSystemMetrics(SM_CXSCREEN)*/, GetSystemMetrics(SM_CYSCREEN)),
			this,NULL,0,NULL);
	}	
	else
	{
		pogChaDiagram->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}

}





void CButtonListDlg::OnGates() 
{
	CRect olRect;
	GetWindowRect(&olRect);


	if(	pogGatDiagram  == NULL)
	{	
		pogGatDiagram = new GatDiagram();
		pogGatDiagram->Create(NULL, GetString(IDS_STRING1019), WS_HSCROLL | WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_MAXIMIZEBOX | WS_SYSMENU | WS_MINIMIZEBOX ,
			CRect(0, m_bottom, olRect.right, GetSystemMetrics(SM_CYSCREEN)),
			this,NULL,0,NULL);
	}	
	else
	{
		pogGatDiagram->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}

	UpdateChangesButton();

}

void CButtonListDlg::OnWarteraeume() 
{
	CRect olRect;
	GetWindowRect(&olRect);

	if(	pogWroDiagram  == NULL)
	{	
		pogWroDiagram = new WroDiagram();
		pogWroDiagram->Create(NULL, GetString(IDS_STRING823), WS_HSCROLL | WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_MAXIMIZEBOX | WS_SYSMENU | WS_MINIMIZEBOX ,
			CRect(0, m_bottom, olRect.right/*GetSystemMetrics(SM_CXSCREEN)*/, GetSystemMetrics(SM_CYSCREEN)),
			this,NULL,0,NULL);
	}	
	else
	{
		pogWroDiagram->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}

	UpdateChangesButton();
}

void CButtonListDlg::OnLFZPositionen() 
{
	CRect olRect;
	GetWindowRect(&olRect);

	if(	pogPosDiagram  == NULL)
	{	
		pogPosDiagram = new PosDiagram();
		pogPosDiagram->Create(NULL, GetString(IMFK_AIRCRAFT_POS), WS_HSCROLL | WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_MAXIMIZEBOX | WS_SYSMENU | WS_MINIMIZEBOX ,
			CRect(0, m_bottom, olRect.right/*GetSystemMetrics(SM_CXSCREEN)*/, GetSystemMetrics(SM_CYSCREEN)),
			this,NULL,0,NULL);
	}	
	else
	{
		pogPosDiagram->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}

	UpdateChangesButton();
}


void CButtonListDlg::OnCheckin() 
{
	CRect olRect;
	GetWindowRect(&olRect);

	if(	pogCcaDiagram  == NULL)
	{	
		pogCcaDiagram = new CcaDiagram();
		pogCcaDiagram->Create(NULL, GetString(IDS_STRING1012), WS_HSCROLL | WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_MAXIMIZEBOX | WS_SYSMENU | WS_MINIMIZEBOX ,
			CRect(0, m_bottom, olRect.right/*GetSystemMetrics(SM_CXSCREEN)*/, GetSystemMetrics(SM_CYSCREEN)),
			this,NULL,0,NULL);
	}	
	else
	{
		pogCcaDiagram->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}
	
	UpdateChangesButton();
}


void CButtonListDlg::OnReports() 
{
/*	CWnd* olpCwnd = GetDlgItem(IDC_BEENDEN);
	if (olpCwnd)
		olpCwnd->EnableWindow(FALSE);
*/

		char pclExcelPath[256];

		GetPrivateProfileString(ogAppName, "FIPSREPORTS", "DEFAULT",
			pclExcelPath, sizeof pclExcelPath, __GetConfigPath() );


		if(!strcmp(pclExcelPath, "DEFAULT"))
		{
		 	pogReportSelectDlg->SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
//			MessageBox(GetString(IDS_STRING2743), GetString(ST_FEHLER), MB_ICONERROR);
			return;
		}


		char *args[4];
		char slRunTxt[256];
		char slRunTxt2[256];
		args[0] = "child";
		sprintf(slRunTxt,"%s",pcgUser);
		sprintf(slRunTxt2,"%s",pcgPasswd);
		args[1] = slRunTxt;
//		args[1] = NULL;
		args[2] = slRunTxt2;
		args[3] = NULL;
		int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
}




void CButtonListDlg::OnAchtung() 
{
	pogKonflikteDlg->Attention();

}

 
void CButtonListDlg::OnKonflikte() 
{
	pogKonflikteDlg->Conflicts();

}


bool CButtonListDlg::CallCuteIF(CString opMessage)
{
	// Only Athens has the CuteIF-Application !!
	if (!bgComOk || strcmp(pcgHome, "ATH") != 0)
		return false;

 	_bstr_t tmpStr = opMessage.GetBuffer(opMessage.GetLength());

	try
	{
		pConnect->TransferData(static_cast<long>(FipsCUTE),
							   static_cast<long>(Fips),tmpStr);
		bmIsCuteIFStarted = true;
	}
	catch(_com_error error)
	{
		CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2010), GetString(ST_FEHLER), MB_ICONERROR | MB_OK);
		return false;
	}

	return true;
}


void CButtonListDlg::MessTlxpool(CString opInMessage)
{

	CString olWhere;
	if (!GetWhereFromTlxpoolMsg(opInMessage, olWhere))
	{
		CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2050) + opInMessage, GetString(ST_FEHLER), MB_ICONERROR);
		return;
	}


	if (pogFlightSearchTableDlg)
	{
		pogFlightSearchTableDlg->Activate(olWhere);
	}

}




bool CButtonListDlg::GetWhereFromTlxpoolMsg(const CString &ropInMessage, CString &ropWhere)
{
	// build where string
	CStringArray olMessageLines;

	// get fieldlist and datalist
	ExtractItemList(ropInMessage, &olMessageLines, '\n');
	if (olMessageLines.GetSize() < 2)
	{
		CString olWarn;
		olWarn.Format("WARNING (CButtonListDlg::GetWhereFromTlxpoolMsg): Not two lines received! ('%s')", ropInMessage);
		CFPMSApp::DumpDebugLog(olWarn);
		return false;
	}

	// get fields and data
	CStringArray olFieldList;
	CStringArray olDataList;

	ExtractItemList(olMessageLines[0], &olFieldList, ',');
	ExtractItemList(olMessageLines[1], &olDataList, ',');
	
	if (olFieldList.GetSize() != olDataList.GetSize())
	{
		CString olWarn;
		olWarn.Format("WARNING (CButtonListDlg::GetWhereFromTlxpoolMsg): Item count not equal! (Fields: '%s'  Data: '%s')", olMessageLines[0], olMessageLines[1]);
		CFPMSApp::DumpDebugLog(olWarn);
		return false;
	}


	ropWhere.Empty();

	int ilId;
	int ilId2;

	//// scan for fields!

	CString olData;
	CString olData2;
	ilId = FindIdInStrArray(olFieldList, "FLNU");
	if (ilId >= 0)
	{
		olData = olDataList[ilId];
		olData.TrimLeft();
		if (!olData.IsEmpty())
		{
			// VOLLTREFFER! AFT:URNO vorhanden!
			ropWhere.Format("URNO = '%s'", olData);
			return true;
		}
	}

	bool blTimeframe = false;
	bool blRegn = false;

	CString olTmpWhere;
	ilId = FindIdInStrArray(olFieldList, "VPFR");
	ilId2 = FindIdInStrArray(olFieldList, "VPTO");
	if (ilId >= 0 && ilId2 >= 0)
	{
		olData = olDataList[ilId];
		olData.TrimLeft();
		olData2 = olDataList[ilId2];
		olData2.TrimLeft();
		if (!olData.IsEmpty() && !olData2.IsEmpty())
		{
			// Zeitraum
			olTmpWhere.Format("((TIFA BETWEEN '%s' AND '%s') OR (TIFD BETWEEN '%s' AND '%s'))", olData, olData2, olData, olData2);
			ropWhere += olTmpWhere;
			blTimeframe = true;
		}
	}

	ilId = FindIdInStrArray(olFieldList, "ALC3");
	if (ilId >= 0)
	{
		// Airline 3-Letter-Code
		olData = olDataList[ilId];
		olData.TrimLeft();
		if (!olData.IsEmpty())
		{
			olTmpWhere.Format(" AND ALC3 = '%s'", olData);
			ropWhere += olTmpWhere;
		}
	}

	ilId = FindIdInStrArray(olFieldList, "FLTN");
	if (ilId >= 0)
	{
		// Flightnumber
		olData = olDataList[ilId];
		olData.TrimLeft();
		if (!olData.IsEmpty())
		{
			olTmpWhere.Format(" AND FLTN = '%s'", olData);
			ropWhere += olTmpWhere;
		}
	}

	ilId = FindIdInStrArray(olFieldList, "FLNS");
	if (ilId >= 0)
	{
		// Flight-Suffix
		olTmpWhere.Format(" AND FLNS = '%1s'", olDataList[ilId]);
		ropWhere += olTmpWhere;
	}

	ilId = FindIdInStrArray(olFieldList, "REGN");
	if (ilId >= 0)
	{
		// Registration
		CString olRegn = olDataList[ilId];
		olRegn.TrimLeft();
		// delete existing '-'
		olRegn.Remove('-');
		if (!olRegn.IsEmpty())
		{
			olTmpWhere.Format(" AND REGN = '%s'", olRegn);
			ropWhere += olTmpWhere;
			blRegn = true;
		}
	}

	if (!ropWhere.IsEmpty() && (blRegn || blTimeframe))
		return true;
	else
		return false;
}



void CButtonListDlg::ProcessBcTest()
{
	return;
}



void CButtonListDlg::OnAbout() 
{
	InfoDlg dlgInfo;
	dlgInfo.DoModal();

	if (pogBackGround)
	{
		if (pogBackGround->IsWindowVisible())
			pogBackGround->ShowWindow(SW_HIDE);
		else
			pogBackGround->ShowWindow(SW_SHOW);
	}

}

void CButtonListDlg::OnBCStatus() 
{
	TestBc();
}


void CButtonListDlg::FlightDiaShowFlight(long lpUrno) 
{
	if (pogFlightDiagram && lpUrno  > 0)
		pogFlightDiagram->ShowFlight(lpUrno, true);
}



//AM:20101102:DE-ICING	
void CButtonListDlg::OnDeicing() 	
{	
		mdDeIcingProcId = LaunchTool( "DE-ICING", "0", "", "", DE_ICING_CFG_NAME, omDeIcingWindowName, mdDeIcingProcId );		
}	




