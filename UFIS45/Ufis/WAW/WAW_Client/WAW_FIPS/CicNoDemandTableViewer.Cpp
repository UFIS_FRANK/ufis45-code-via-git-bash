// seasontableviewer.cpp : implementation file
// 
// Modification History: 


#include <stdafx.h>
#include <CicNoDemandTableViewer.h>
#include <CcsGlobl.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <DataSet.h>
#include <CcaDiagram.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif




// Local function prototype
static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))


/////////////////////////////////////////////////////////////////////////////
// CicNoDemandTableViewer
//

CicNoDemandTableViewer::CicNoDemandTableViewer()
{
    pomTable = NULL;
	bmInit = false;
}


void CicNoDemandTableViewer::UnRegister()
{
	ogDdx.UnRegister(this, NOTUSED);
 
}

CicNoDemandTableViewer::~CicNoDemandTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
}

void CicNoDemandTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}

void CicNoDemandTableViewer::ClearAll()
{
	bmInit = false;
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
    pomTable->ResetContent();
}



void CicNoDemandTableViewer::ChangeViewTo(const char *pcpViewName)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	
	ogDdx.UnRegister(this, NOTUSED);

	ogDdx.Register(this, DIACCA_CHANGE, CString("DIACCA_CHANGE"), CString("DIACCA_CHANGE"), FlightTableCf);


    pomTable->ResetContent();
    DeleteAll();    
	MakeLines();
   	UpdateDisplay();

	bmInit = true;
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}







/////////////////////////////////////////////////////////////////////////////
// CicNoDemandTableViewer -- code specific to this class




bool CicNoDemandTableViewer::IsPassFilter(CCSPtrArray<DIACCADATA> &opCcas)
{
	if(opCcas.GetSize() == 0)
		return true;

	DIACCADATA *prlCca;

	bool blRet = false;

	for(int i = opCcas.GetSize() -1; i >= 0; i--)
	{
		prlCca = &opCcas[i];

		CString olCkic = prlCca->Ckic;
		olCkic.TrimLeft();

		if(prlCca->Stat[9] == '0')// || olCkic.IsEmpty())
		{
			opCcas.RemoveAt(i);
		}
		if (!olCkic.IsEmpty())
			return false;
	}

	if(opCcas.GetSize() > 0)
//	if(opCcas.GetSize() == 0)
		blRet = true;

	return blRet;
}




void CicNoDemandTableViewer::MakeLines()
{
	POSITION pos;
	void *pVoid;
	CCAFLIGHTDATA *prlFlight;
	CString olTmp;

	for( pos = ogCcaDiaFlightData.omUrnoMap.GetStartPosition(); pos != NULL; )
	{
		ogCcaDiaFlightData.omUrnoMap.GetNextAssoc( pos, pVoid , (void *&)prlFlight );
	
		MakeLine(prlFlight);
	}
}


bool  CicNoDemandTableViewer::MakeLine(CCAFLIGHTDATA *prpFlight)
{
	CICNODEMANDTABLE_LINEDATA rlLineData;
	if(MakeLineData(&rlLineData, prpFlight))
	{
		int ilLineNo = CreateLine(rlLineData);
		InsertDisplayLine(ilLineNo);
	}
	return true;
}


bool  CicNoDemandTableViewer::MakeLine( CCSPtrArray<CCAFLIGHTDATA> &opFlights)
{
	CICNODEMANDTABLE_LINEDATA rlLineData;
	if(MakeLineData(&rlLineData, opFlights))
	{
		int ilLineNo = CreateLine(rlLineData);
		InsertDisplayLine(ilLineNo);
	}
	return true;
}




	
bool CicNoDemandTableViewer::MakeLineData(CICNODEMANDTABLE_LINEDATA *prpLineData, CCSPtrArray<CCAFLIGHTDATA> &opFlights)
{
	CCAFLIGHTDATA *prlFlight;
	CCAFLIGHTDATA *prlFlight2;
	CCSPtrArray<DIACCADATA> olFlightCcaList;


	if(opFlights.GetSize() > 0)
	{
		prlFlight = &opFlights[0];
	
		if(opFlights.GetSize() == 1)
			prlFlight2 = prlFlight;
		else
			prlFlight2 = &opFlights[opFlights.GetSize()-1];

		//PRF 8084	
		if(bgPreviousDaycheckIn)
		{
			CTimeSpan olStepInit (0,0,1439,59);
			CTime olFrom = ogCcaDiaFlightData.omFrom;
			CTime olTo   = olFrom + olStepInit;

			if (olTo > ogCcaDiaFlightData.omTo)
				olTo = ogCcaDiaFlightData.omTo;

			if(!(prlFlight->Stod >= olFrom && prlFlight->Stod <= olTo))
				return false;
		}


		olFlightCcaList.RemoveAll();
		ogCcaDiaFlightData.omCcaData.GetCcasByFlnu(olFlightCcaList, prlFlight->Urno);

		if(!IsPassFilter(olFlightCcaList))
			return false;

		prpLineData->Sto = prlFlight->Stod;
		prpLineData->Sto2 = prlFlight2->Stod;
		prpLineData->Freq = prlFlight2->Freq;

		prpLineData->Urno = prlFlight->Urno;
		prpLineData->Flno = CString(prlFlight->Flno); 
		prpLineData->OrgDes = prlFlight->Des3;

		if(olFlightCcaList.GetSize() == 0)
		{
			prpLineData->Remark = GetString(ST_FEHLER); // CString("Fehler");
		}
		else
		{
			DIACCADATA *prlCca = &olFlightCcaList[0];

			
			CString olRulesNames(prlCca->Disp);
			
			CStringArray olRulesList;

			if(!olRulesNames.IsEmpty())
			{
				ExtractItemList(olRulesNames, &olRulesList, ';');

				olRulesNames = CString("");

				for(int i = 0; i < olRulesList.GetSize(); i++)
				{
					if(!olRulesList[i].IsEmpty())
						olRulesNames +=   ogBCD.GetField("GHP", "URNO", olRulesList[i] , "PRSN" ) +    CString(" / ");
				}
			}

			if(prlCca->Stat[9] == 'X')
			{

				prpLineData->Remark = GetString(IDS_STRING1509) + olRulesNames; //Mehrere Regeln gefunden: 
			}

			if(prlCca->Stat[9] == 'N')
			{
				prpLineData->Remark = GetString(IDS_STRING1510) ;//"Keine Regel gefunden";
			}

			if(prlCca->Stat[9] == 'W')
			{

				prpLineData->Remark = olRulesNames + GetString(IDS_STRING1511);//CString(" -> Regel ohne Leistung");
			}
		}

		if (bgGatPosLocal)
		{
			UtcToLocal(*prpLineData);
		}

	}
	
	olFlightCcaList.RemoveAll();
	return true;	
	
}


bool CicNoDemandTableViewer::MakeLineData(CICNODEMANDTABLE_LINEDATA *prpLineData, CCAFLIGHTDATA *prpFlight)
{
	CCSPtrArray<CCAFLIGHTDATA> olFlights;
	olFlights.Add(prpFlight);
	bool blRet = MakeLineData(prpLineData, olFlights);
	olFlights.RemoveAll();
	return blRet;
}





int CicNoDemandTableViewer::CreateLine(CICNODEMANDTABLE_LINEDATA &rpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        //if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
		if (CompareLines(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


void CicNoDemandTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);

	pomTable->DeleteTextLine(ipLineno);

}



bool CicNoDemandTableViewer::FindLine(long lpUrno, int &rilLineno)
{
	rilLineno = -1;
    for (int i = omLines.GetSize() - 1; i >= 0; i--)
	{
	  if((omLines[i].Urno == lpUrno))
	  {
		rilLineno = i;
		return true;
	  }
	}
	return false;
}






/////////////////////////////////////////////////////////////////////////////
// CicNoDemandTableViewer - CICNODEMANDTABLE_LINEDATA array maintenance

void CicNoDemandTableViewer::DeleteAll()
{
    omLines.DeleteAll();
}


/////////////////////////////////////////////////////////////////////////////
// CicNoDemandTableViewer - display drawing routine



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void CicNoDemandTableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	DrawHeader();

	CICNODEMANDTABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}


    pomTable->DisplayTable();
}



void CicNoDemandTableViewer::InsertDisplayLine( int ipLineNo)
{
	if(!((ipLineNo >= 0) && (ipLineNo < omLines.GetSize())))
		return;
	CCSPtrArray<TABLE_COLUMN> olColList;
	MakeColList(&omLines[ipLineNo], olColList);
	pomTable->InsertTextLine(ipLineNo, olColList, &omLines[ipLineNo]);
	olColList.DeleteAll();
    pomTable->DisplayTable();
}






void CicNoDemandTableViewer::DrawHeader()
{
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Font = &ogCourier_Bold_10;


	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 67; 
	rlHeader.Text =  GetString(IDS_STRING347);//CString("Flugnummer");
	omHeaderDataArray.New(rlHeader);


	rlHeader.Length = 40; 
	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Text = GetString(IDS_STRING316);//CString("STD");
	omHeaderDataArray.New(rlHeader);


	rlHeader.Length = 65; 
	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Text = GetString(IDS_STRING332);//CString("Datum");
	omHeaderDataArray.New(rlHeader);


	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 30; 
	rlHeader.Text = GetString(IDS_STRING1447);//CString("ORG/DES");
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 400; 
	rlHeader.Text = GetString(IDS_STRING1506) ;//"Regel"
	omHeaderDataArray.New(rlHeader);

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}









bool CicNoDemandTableViewer::MakeColList(CICNODEMANDTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList)
{
		TABLE_COLUMN rlColumnData;

		if(prlLine->Error == true)
		{
			rlColumnData.TextColor = COLORREF(RED);
		}
		rlColumnData.VerticalSeparator = SEPA_NONE;
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.Font = &ogCourier_Regular_9;
	
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Flno;
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		rlColumnData.Text = prlLine->Sto.Format("%H:%M");
		rlColumnData.Alignment = COLALIGN_LEFT;
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		rlColumnData.Text = prlLine->Sto.Format("%d.%m.%y");
		rlColumnData.Alignment = COLALIGN_LEFT;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->OrgDes;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Remark;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		return true;
}



static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    CicNoDemandTableViewer *polViewer = (CicNoDemandTableViewer *)popInstance;

	if(ipDDXType == DIACCA_CHANGE)
		polViewer->ProcessCcaChange((long *)vpDataPointer);

	if(ipDDXType == DIACCA_CHANGE || ipDDXType == CCA_KKEY_CHANGE)
	{
		if (pogCcaDiagram)
			pogCcaDiagram->UpdateWoDemButton();
	}
}








/*
int CicNoDemandTableViewer::CompareFlight(CICNODEMANDTABLE_LINEDATA *prpLine1, CICNODEMANDTABLE_LINEDATA *prpLine2)
{
		return (prpLine1->Sto == prpLine2->Sto)? 0:	(prpLine1->Sto > prpLine2->Sto)? 1: -1;
}
*/

int CicNoDemandTableViewer::CompareLines(CICNODEMANDTABLE_LINEDATA *prpLine1, CICNODEMANDTABLE_LINEDATA *prpLine2)
{

	return (prpLine1->Sto == prpLine2->Sto)? 0:	
		   (prpLine1->Sto > prpLine2->Sto)? 1: -1;
}



bool CicNoDemandTableViewer::DeleteLine(long lpUrno)
{
	bool blRet = false;
    for (int i = omLines.GetSize() - 1; i >= 0; i--)
	{
	  if((omLines[i].Urno == lpUrno))
	  {
		omLines.DeleteAt(i);
		pomTable->DeleteTextLine(i);
		blRet = true;
	  }
	}
	return blRet;
}



void CicNoDemandTableViewer::ProcessCcaChange(long *plpCcaUrno)
{
	DIACCADATA *prlCca =ogCcaDiaFlightData.omCcaData.GetCcaByUrno(*plpCcaUrno);
	if(prlCca == NULL)
	{
		ChangeViewTo("");
		
		return;
	}

	DeleteLine(prlCca->Flnu);

	CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(prlCca->Flnu);	

	if(prlFlight != NULL)
	{
		MakeLine(prlFlight);
	}
}



void CicNoDemandTableViewer::UtcToLocal(CICNODEMANDTABLE_LINEDATA &rrpLine)
{
	ogBasicData.UtcToLocal(rrpLine.Sto);
	ogBasicData.UtcToLocal(rrpLine.Sto2);
}


int CicNoDemandTableViewer::GetCount ()
{
	return omLines.GetSize();
}

