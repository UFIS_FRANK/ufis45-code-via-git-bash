// DiffPosTableDlg.cpp : implementation file
//
// Modification History: 

#include <stdafx.h>
#include <fpms.h>
#include <resrc1.h>
#include <DiffPosTableDlg.h>
#include <SeasonDlg.h>
#include <RotationDlg.h>
#include <DiaCedaFlightData.h>
#include <PrivList.h>

#include <PosDiagram.h>
#include <Utils.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DiffPosTableDlg dialog


DiffPosTableDlg::DiffPosTableDlg(CWnd* pParent /*=NULL*/)
	: CDialog(DiffPosTableDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(DiffPosTableDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	isCreated = false;
	pomParent = pParent;
	pomTable = NULL;
    CDialog::Create(DiffPosTableDlg::IDD, NULL);
	m_key = "DialogPosition\\PositionCheck";
	isCreated = true;
}


DiffPosTableDlg::~DiffPosTableDlg()
{
	pomViewer->UnRegister();

	delete pomTable;
	pomTable = NULL;
	delete pomViewer;
	pomViewer = NULL;
}


void DiffPosTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DiffPosTableDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DiffPosTableDlg, CDialog)
	//{{AFX_MSG_MAP(DiffPosTableDlg)
	ON_WM_CLOSE()
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_MESSAGE(WM_TABLE_RETURN_PRESSED, OnTableReturnPressed)
	ON_MESSAGE(WM_TABLE_SELCHANGE, OnTableSelChanged)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_APPLY_ARR, OnArr)
	ON_BN_CLICKED(IDC_APPLY_DEP, OnDep)
 	ON_WM_DESTROY()
	ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DiffPosTableDlg message handlers
void DiffPosTableDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}

void DiffPosTableDlg::OnDestroy() 
{
	SaveToReg();
	isCreated = false;
	CDialog::OnDestroy();
	delete pomTable;//AM 20070928
	pomTable = NULL;
	delete pomViewer;//AM 20070928
	pomViewer = NULL;//AM 20070928
}

void DiffPosTableDlg::OnSize(UINT nType, int cx, int cy) 
{
	if(isCreated != false)
	{
		CDialog::OnSize(nType, cx, cy);
		//m_resizeHelper.OnSize();
	
		if (nType != SIZE_MINIMIZED)
		{
			if (pomTable)
			{
				CRect olRect;
			    GetClientRect(&olRect);
			    olRect.InflateRect(1, 1);     // hiding the CTable window border
				pomTable->SetPosition(olRect.left, olRect.right, olRect.top + 30, olRect.bottom);
				Rebuild();
			}
		}


/*
		CDialog::OnSize(nType, cx, cy);
	
		if (nType != SIZE_MINIMIZED)
		{
			if (pomTable)
				pomTable->SetPosition(1, cx+1, 30, cy-1);
		}
*/
	}
}


void DiffPosTableDlg::Rebuild()
{
	pomViewer->ChangeViewTo();

	UpdateCaption();
}


int DiffPosTableDlg::GetCount() const
{
	return pomViewer->GetCount();
}

void DiffPosTableDlg::UpdateCaption()
{
	CString olGantt;

	//// set caption
	CString olCaption;
	olCaption.Format("%s   (%d)", GetString(IDS_STRING2110), pomViewer->GetCount());
	// additional text for time mode
	CString olTimes;
	if (bgGatPosLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);
	
}



BOOL DiffPosTableDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_key = "DialogPosition\\PositionCheck";
	
	SetWindowPos(pomParent, 0,0,0,0, SWP_NOSIZE  | SWP_NOMOVE);

	pomViewer = new DiffPosTableViewer(this);

	pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
	pomTable->SetHeaderSpacing(0);

	isCreated = true;
	
	CRect olRect;
    GetClientRect(&olRect);

	//olRect.top = olRect.top + imDialogBarHeight;
	//olRect.bottom = olRect.bottom;// - imDialogBarHeight;

    olRect.InflateRect(1, 1);     // hiding the CTable window border
    
	pomTable->SetTableData(this, olRect.left, olRect.right, olRect.top + 30, olRect.bottom);

	pomViewer->Attach(pomTable);

	pomViewer->ChangeViewTo();

	UpdateCaption();

	CButton* polCB = (CButton *) GetDlgItem(IDC_APPLY_ARR);
	if (polCB)
		SetpWndStatAll(ogPrivList.GetStat("POSDIAGRAMM_CB_Apply_ARR"),polCB);

	polCB = (CButton *) GetDlgItem(IDC_APPLY_DEP);
	if (polCB)
		SetpWndStatAll(ogPrivList.GetStat("POSDIAGRAMM_CB_Apply_DEP"),polCB);

	m_resizeHelper.Init(this->m_hWnd);

	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_HIDEWINDOW);// | SWP_NOMOVE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void DiffPosTableDlg::OnClose() 
{
	ShowWindow(SW_HIDE);	

	//pomViewer->UnRegister();

	//CDialog::OnClose();
}

void DiffPosTableDlg::OnApply(int ipLeg) 
{
	DIFFPOSTABLE_LINEDATA *prlTableLine;
	CString olUrnoList;
//	char buffer[32];
	int ilAnzSel = 0;

	CListBox *polLB = pomTable->GetCTableListBox();
	if (polLB == NULL)
		return;

	int *ilItems = NULL;
	int ilAnz = polLB->GetSelCount();
	if(ilAnz > 0)
	{
		ilItems = new int[ilAnz];
	}

	if(ilItems != NULL)
		ilAnz = polLB->GetSelItems(ilAnz, ilItems);
	else
	{
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING968), GetString(ST_FEHLER), MB_ICONWARNING);
		return;
	}

	if (ilAnz == LB_ERR)
	{
		CFPMSApp::MyTopmostMessageBox(this,"LB_ERR", GetString(ST_FEHLER), MB_ICONWARNING);
		return;
	}

	BOOL blAllertForPostflight = FALSE;
	for(int i = 0; i < ilAnz; i++)
	{
		DIAFLIGHTDATA *prlFlight = NULL;
		DIAFLIGHTDATA *prlFlightA = NULL;
		DIAFLIGHTDATA *prlFlightD = NULL;
		DIAFLIGHTDATA *prlFlightT = NULL;
		DIAFLIGHTDATA prlFlightSave;

		prlTableLine = (DIFFPOSTABLE_LINEDATA *)pomTable->GetTextLineData(ilItems[i]);
		long llUrno = prlTableLine->AUrno;
		if(llUrno == 0)
			llUrno = prlTableLine->DUrno;

		if(llUrno != 0)
		{
			prlFlightA = ogPosDiaFlightData.GetFlightByUrno(prlTableLine->AUrno);
			prlFlightD = ogPosDiaFlightData.GetFlightByUrno(prlTableLine->DUrno);
			if (prlFlightA || prlFlightD)
			{
				if (prlFlightA)
					llUrno = prlFlightA->Rkey;
				else if (prlFlightD)
					llUrno = prlFlightD->Rkey;

				DiaCedaFlightData::RKEYLIST* prlRkey = ogPosDiaFlightData.GetRotationByRkey(llUrno);

				if (!prlFlightA && !prlFlightD)
					return;

				if (ipLeg == 1)
				{
					CString olPos = "";
					prlFlightT = ogPosDiaFlightData.GetLastTowInRot(prlRkey);
					if(prlFlightT)
					{
						if (CString(prlFlightT->Psta).IsEmpty())
						{
							if (prlFlightA && !CString(prlFlightA->Psta).IsEmpty())
								olPos = prlFlightA->Psta;
						}
						else
						{
							olPos = prlFlightT->Psta;
						}
					}
					else
					{
						if (prlFlightA && !CString(prlFlightA->Psta).IsEmpty())
							olPos = prlFlightA->Psta;
					}

					if (!olPos.IsEmpty())
					{
						prlFlightSave = *prlFlightD;
						strcpy(prlFlightD->Pstd, olPos);
						ogPosDiaFlightData.UpdateFlight(prlFlightD, &prlFlightSave);
					}
				}
				else if (ipLeg == 2)
				{
					if (prlFlightA->Onbl != TIMENULL)
					{
						CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING1546), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
						return ;
					}

					CString olPos = "";
					prlFlightT = ogPosDiaFlightData.GetFirstTowInRot(prlRkey);
					if(prlFlightT)
					{
						if (CString(prlFlightT->Pstd).IsEmpty())
						{
							if (prlFlightD && !CString(prlFlightD->Pstd).IsEmpty())
								olPos = prlFlightD->Pstd;
						}
						else
						{
							olPos = prlFlightT->Pstd;
						}
					}
					else
					{
						if (prlFlightD && !CString(prlFlightD->Pstd).IsEmpty())
							olPos = prlFlightD->Pstd;
					}

					if (!olPos.IsEmpty())
					{
						prlFlightSave = *prlFlightA;
						strcpy(prlFlightA->Psta, olPos);
						ogPosDiaFlightData.UpdateFlight(prlFlightA, &prlFlightSave);
					}
				}
			}
		}
	}
}
void DiffPosTableDlg::OnArr() 
{
	OnApply(1);
}
void DiffPosTableDlg::OnDep() 
{
	OnApply(2);
}
////////////////////////////////////////////////////////////////////////////
// linke Maustaste auf der Flighttable gedr�ckt
//
LONG DiffPosTableDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	UINT ipItem = wParam;
	DIFFPOSTABLE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (DIFFPOSTABLE_LINEDATA *)pomTable->GetTextLineData(ipItem);
	if (prlTableLine != NULL)
	{
		// get flight data
		const DIAFLIGHTDATA *prlFlight = NULL;
		char clAdid=' ';
		if (prlTableLine->AUrno != 0)
		{
			prlFlight = ogPosDiaFlightData.GetFlightByUrno(prlTableLine->AUrno);
			clAdid = 'A';
		}
		if (prlFlight==NULL && prlTableLine->DUrno != 0) 
		{
			prlFlight = ogPosDiaFlightData.GetFlightByUrno(prlTableLine->DUrno);
			clAdid = 'D';
		}
		

		if(prlFlight != NULL)
		{		
			if (ogPosDiaFlightData.bmOffLine)
			{
 				pogSeasonDlg->NewData(this, prlFlight, bgGatPosLocal);
			}
			else
			{
	 			if (clAdid == 'A')
				{
					CFPMSApp::ShowFlightRecordData(this, prlFlight->Ftyp[0], prlFlight->Urno, prlFlight->Rkey, clAdid, prlFlight->Tifa, bgGatPosLocal);
				}
				else
				{
					CFPMSApp::ShowFlightRecordData(this, prlFlight->Ftyp[0], prlFlight->Urno, prlFlight->Rkey, clAdid, prlFlight->Tifd, bgGatPosLocal);
				}
			}
		}
	}
	return 0L;
}



LONG DiffPosTableDlg::OnTableReturnPressed( UINT wParam, LPARAM lParam)
{
	OnTableLButtonDblclk(wParam, lParam); 
	return 0L;
}

LONG DiffPosTableDlg::OnTableSelChanged( UINT wParam, LPARAM lParam)
{
    int ilLineNo = pomTable->pomListBox->GetCurSel();    
	
	DIFFPOSTABLE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (DIFFPOSTABLE_LINEDATA *)pomTable->GetTextLineData(ilLineNo);
	if (prlTableLine->AUrno != 0)
		CheckPostFlightPosDia(prlTableLine->AUrno,this);
	else
		CheckPostFlightPosDia(prlTableLine->DUrno,this);
	
	return 0L;
}


LONG DiffPosTableDlg::OnTableLButtonDown(UINT wParam, LONG lParam)
{

	CString olUrnoList;
	int ilAnzSel = 0;

	CListBox *polLB = pomTable->GetCTableListBox();
	if (polLB == NULL)
		return 0L;

	int *ilItems = NULL;
	int ilAnz = polLB->GetSelCount();
	if(ilAnz > 0)
	{
		ilItems = new int[ilAnz];
	}

	if(ilItems != NULL)
	{
		ilAnz = polLB->GetSelItems(ilAnz, ilItems);

		pomViewer->SetSelection(ilAnz, ilItems);		

	}
	else
		return 0L;

/*

	BOOL blpOnlyPostflights = TRUE;
	for(int i = 0; i < ilAnz; i++)
	{
		prlTableLine = (SEASONTABLE_LINEDATA *)pomSeasonTable->GetTextLineData(ilItems[i]);

		if (!CheckPostFlight(prlTableLine))
		{
			blpOnlyPostflights = FALSE;
			break;
		}
	}


	if(ilItems != NULL)
		delete [] ilItems;

*/

	return 0L;

}



