#if !defined(AFX_BLTOVERVIEWTABLEDLG_H__39D1ABF1_278C_11D2_8588_0000C04D916B__INCLUDED_)
#define AFX_BLTOVERVIEWTABLEDLG_H__39D1ABF1_278C_11D2_8588_0000C04D916B__INCLUDED_



#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// BltOverviewTableDlg.h : header file
//

#include <BltOverviewTableViewer.h>
#include <PosOverviewTableViewer.h>
#include <CCSTable.h>
#include <CCSDragDropCtrl.h>
#include <resrc1.h>
 
/////////////////////////////////////////////////////////////////////////////
// BltOverviewTableDlg dialog

class BltOverviewTableDlg : public CDialog
{
// Construction
public:
	BltOverviewTableDlg(CWnd* pParent = NULL, CString opResource = "BLT");   // standard constructor
	~BltOverviewTableDlg();

	void Activate(void);
	void Update(void);
	void SetResource(CString opResource);
	void SaveToReg();

// Dialog Data
	//{{AFX_DATA(BltOverviewTableDlg)
	enum { IDD = IDD_BLTOVERVIEW };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BltOverviewTableDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(BltOverviewTableDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnSave();
	afx_msg void OnPrint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg LONG OnTableReturnPressed(UINT wParam, LONG lParam);
	afx_msg LONG OnTableSelChanged( UINT wParam, LPARAM lParam);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CCSTable *pomTable;

	BltOverviewTableViewer omViewer;
	PosOverviewTableViewer omPosViewer;


private:
	bool isCreated;
	int imButtonSpace;
	int imMinDlgWidth;
	CString myResource;
	CString myCaption;
	CString m_key; 
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BLTOVERVIEWTABLEDLG_H__39D1ABF1_278C_11D2_8588_0000C04D916B__INCLUDED_)
