// HEADER 
#ifndef _CEDACOUNTDATA_H_ 
#define _CEDACOUNTDATA_H_ 

#include <CCSCedaData.h> 


struct COUNTDATA 
{
 	long 	 Urno; 	// Eindeutige Datensatz-Nr.
 

	COUNTDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
	}

}; // end COUNTDATA

 
class CedaCountData : public CCSCedaData
{
public:
	CedaCountData(const CString &ropTableName);
	~CedaCountData();

 	char pcmFList[512];

	int GetCount(CString popSelection);
	bool SendBC(CString opTable, CString opSel, CString opFields, CString opData);
   
	//////////////////////////////////////

private:

 
};

 
#endif // _CEDACOUNTDATA_H_ 
