// FPMS.h : main header file for the FPMS application
//

#if !defined(AFX_CCSPRJ_H__FC3BA6F7_1AED_11D1_82C3_0080AD1DC701__INCLUDED_)
#define AFX_CCSPRJ_H__FC3BA6F7_1AED_11D1_82C3_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include <resource.h>		// main symbols
//#include "ccsglobl.h"		
#include <BackGround.h>	
#include <CcsCedaData.h>	

/////////////////////////////////////////////////////////////////////////////
// CFPMSApp:
// See FPMS.cpp for the implementation of this class
//
#include <UFISAmSink.h>

class CFPMSApp : public CWinApp
{
public:
	CFPMSApp();
	~CFPMSApp();

    void InitialLoad(CWnd *pParent);
	void Customize();

	static void UnsetTopmostWnds();
	static void SetTopmostWnds();
	static bool ShowFlightRecordData(CWnd *opParent, char cpFTyp, long lpUrno, long lpRkey, char cpAdid, const CTime &ropTifad, bool bpLocalTimes, char cpMask = ' ');
	static int MyTopmostMessageBox(CWnd *polWnd, const CString &ropText, const CString &ropCaption, UINT ipType = 0);
	static bool DumpDebugLog(const CString &ropStr);
	static bool CheckCedaError(const CCSCedaData &ropCedaData);
	static CString GetSelectedReason(CWnd *,bool blHideCancel=false); 
	static CString GetSelectedReason(CWnd* rppWnd, const CString& rolField,const CString& ropSelStr, CString opCaption = ""); 

	// For Com-Interface to the new TelexPool
	DWORD m_dwCookie;
	//


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFPMSApp)
	public:
	virtual BOOL InitInstance();
	virtual void WinHelp(DWORD dwData, UINT nCmd = HELP_CONTEXT);
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CFPMSApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	int ExitInstance();
	void CheckAloTab();

	bool InitCOMInterface();
	bool SetCustomerConfig();
	void LoadPortAndGate();
	void LoadGate(const CString polWhereClause);
	void LoadPort(const CString polWhereClause);

	char pcmLKey[32];

private:
	char m_pclConfigPath[2048];		// 050307 MVy: moved from method and made to attribute
public:
	void UpdateConfigPath();		// 050502 MVy: initialize the variable
	char* GetConfigPath();		// 050502 MVy: unified all local path setting
#define __GetConfigPath		( (CFPMSApp *)AfxGetApp() )->GetConfigPath


// 050309 MVy: for debugging purposes, waste less paper on printing, for testing print only one page
private:
	unsigned long m_ulDebugPrintOnlyFirstLines ;
public:
	void UpdateDebugPrintOnlyFirstLines();		// get the current configuration
	unsigned long GetDebugPrintOnlyFirstLines();		// return amount of lines to print
#define __UpdateDebugPrintOnlyFirstLines		( (CFPMSApp *)AfxGetApp() )->UpdateDebugPrintOnlyFirstLines
#define __GetDebugPrintOnlyFirstLines		( (CFPMSApp *)AfxGetApp() )->GetDebugPrintOnlyFirstLines

// 050307 MVy: the current user sees only confirmations of conflicts he confirmed by himself, otherwise he can see all confirmations
private:
	bool m_bShowOnlyOwnConfirmations ;
public:
	void UpdateShowOnlyOwnConfirmations();		// get the current configuration
	bool CanSeeOnlyOwnConfirmations();		// return true if user can only see his own confirmations
#define __CanSeeOnlyOwnConfirmations		( (CFPMSApp *)AfxGetApp() )->CanSeeOnlyOwnConfirmations

// 050308 MVy: daily schedule lists should contain additional column for display data: ETA(D) and ETD(D)
private:
	bool m_bShowDailyScheduleColumnsDisplay ;
public:
	void UpdateShowDailyScheduleColumnsDisplay();		// get the current configuration
	bool CanSeeDailyScheduleColumnsDisplay();		// return true if user can only see his own confirmations
#define __CanSeeDailyScheduleColumnsDisplay		( (CFPMSApp *)AfxGetApp() )->CanSeeDailyScheduleColumnsDisplay

// 050308 MVy: daily schedule lists should show (ETA(D) and ETD(D) ) or (ETA(FDIS) and ETD(FDIS))
private:
	bool m_bShowFIDS ;
public:
	void UpdateShowFIDSDisplay();		// get the current configuration
	bool CanUseFIDSHeader();		// return true if user can only see his own confirmations
#define __CanUseFIDSHeader	( (CFPMSApp *)AfxGetApp() )->CanUseFIDSHeader

// 050308 MVy: daily rotations list should contain additional column for baggage belts BLT1/BLT2
private:
	unsigned long m_ulShowDailyRotationsColumnBelts ;		//  a bit a belt
public:
	void UpdateShowDailyRotationsColumnBelts();		// get the current configuration
	bool CanSeeDailyRotationsColumnBelts( unsigned long ulMask = 0xFFFFFFFF );		// return true if user can only see his own confirmations
#define __UpdateShowDailyRotationsColumnBelts		( (CFPMSApp *)AfxGetApp() )->UpdateShowDailyRotationsColumnBelts
#define __CanSeeDailyRotationsColumnBelts		( (CFPMSApp *)AfxGetApp() )->CanSeeDailyRotationsColumnBelts

// 050309 MVy: daily rotations list column for VIPs
private:
	unsigned char m_ucShowDailyRotationsColumnVIPs ;		// maybe later Arrival and Departure are handled separate
public:
	void UpdateShowDailyRotationsColumnVIPs();		// get the current configuration
	bool CanSeeDailyRotationsColumnVIPs( unsigned char ucMask = 0xFF );
#define __UpdateShowDailyRotationsColumnVIPs		( (CFPMSApp *)AfxGetApp() )->UpdateShowDailyRotationsColumnVIPs
#define __CanSeeDailyRotationsColumnVIPs		( (CFPMSApp *)AfxGetApp() )->CanSeeDailyRotationsColumnVIPs

// 050309 MVy: daily rotations list column for Remarks
private:
	unsigned char m_ucShowDailyRotationsColumnRemarks ;		// maybe later Arrival and Departure are handled separate
public:
	void UpdateShowDailyRotationsColumnRemarks();		// get the current configuration
	bool CanSeeDailyRotationsColumnRemarks( unsigned char ucMask = 0xFF );
#define __UpdateShowDailyRotationsColumnRemarks		( (CFPMSApp *)AfxGetApp() )->UpdateShowDailyRotationsColumnRemarks
#define __CanSeeDailyRotationsColumnRemarks		( (CFPMSApp *)AfxGetApp() )->CanSeeDailyRotationsColumnRemarks

// 050310 MVy: today only or period for position flight check
private:
	bool m_bAllowPositionFlightTodayOnlyCheck ;		// check button visible in Fips/Setup and handling in Utils.IsPostFlight()
public:
	void UpdateHandlePositionFlightTodayOnlyCheck();		// get the current configuration
	bool CanHandlePositionFlightTodayOnlyCheck();
#define __UpdateHandlePositionFlightTodayOnlyCheck		( (CFPMSApp *)AfxGetApp() )->UpdateHandlePositionFlightTodayOnlyCheck
#define __CanHandlePositionFlightTodayOnlyCheck		( (CFPMSApp *)AfxGetApp() )->CanHandlePositionFlightTodayOnlyCheck

// 050311 MVy: PRF6903: Check-In Counter Restrictions for Handling Agents
private:
	bool m_bAllowCheckinCounterRestrictionsForHandlingAgents ;
public:
	void UpdateHandleCheckinCounterRestrictionsForHandlingAgents();		// get the current configuration
	bool CanHandleCheckinCounterRestrictionsForHandlingAgents();		// return true if handling agent restriction are used with checkin counters
#define __UpdateHandleCheckinCounterRestrictionsForHandlingAgents		( (CFPMSApp *)AfxGetApp() )->UpdateHandleCheckinCounterRestrictionsForHandlingAgents
#define __CanHandleCheckinCounterRestrictionsForHandlingAgents		( (CFPMSApp *)AfxGetApp() )->CanHandleCheckinCounterRestrictionsForHandlingAgents

// 050323 MVy: PRF6981: conflict priority, make configurable, addendum
private:
	bool m_bShowConflictPriorityColor ;
	bool m_bShowSSIM;//UFIS-1078:SSIM, AM:20111028
public:
	void UpdateShowConflictPriorityColor();		// get the current configuration
	bool CanSeeConflictPriorityColor();		// return true if user can modify the conflict colors, -> conflict list dialog, setup  conflict column
#define __UpdateShowConflictPriorityColor			( (CFPMSApp *)AfxGetApp() )->UpdateShowConflictPriorityColor
#define __CanSeeConflictPriorityColor		( (CFPMSApp *)AfxGetApp() )->CanSeeConflictPriorityColor
	//UFIS-1078:SSIM, AM:20111028 - Start
	void UpdateSSIM();
	bool CanSeeSSIM();
#define __UpdateSSIM		( (CFPMSApp *)AfxGetApp() )->UpdateSSIM
#define __CanSeeSSIM			( (CFPMSApp *)AfxGetApp() )->CanSeeSSIM
	//UFIS-1078:SSIM, AM:20111028 - End

};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CCSPRJ_H__FC3BA6F7_1AED_11D1_82C3_0080AD1DC701__INCLUDED_)
