#ifndef _CcaCedaFlightData_H_
#define _CcaCedaFlightData_H_
 
#include <BasicData.h>
#include <CCSGlobl.h>
#include <CCSDefines.h>
#include <CedaDiaCcaData.h>
#include <CedaChaData.h>


 
struct CCAFLIGHTDATA 
{
public:
	CCAFLIGHTDATA();
	//virtual ~CCAFLIGHTDATA();

public:
	char 	 Baa1[5]; 	// FlightReports
	char 	 Baa2[5]; 	// SpecialReq(Read)
	char 	 Baa3[5]; 	// SpecialReq(UnRead)
	char 	 Act3[4]; 	// Flugzeug-3-Letter Code (IATA)
	char 	 Act5[6]; 	// Flugzeug-5-Letter Code
	char 	 Alc2[3]; 	// Fluggesellschaft (Airline 2-Letter Code)
	char 	 Alc3[4]; 	// Fluggesellschaft (Airline 3-Letter Code)
	char 	 Des3[4]; 	// Bestimmungsflughafen 3-Lettercode
	char 	 Des4[5]; 	// Bestimmungsflughafen 4-Lettercode
	CTime 	 Etai; 	// ETA-Intern (Beste Zeit)
	CTime 	 Etdi; 	// ETD-Intern (Beste Zeit)
	char 	 Flno[10]; 	// komplette Flugnummer (Airline, Nummer, Suffix)
	char 	 Flns[2]; 	// Suffix
	char 	 Fltn[6]; 	// Flugnummer
	char 	 Ftyp[2]; 	// Type des Flugs [F, P, R, X, D, T, ...]
	char 	 Org3[4]; 	// Ausgangsflughafen 3-Lettercode
	char 	 Org4[5]; 	// Ausgangsflughafen 4-Lettercode
	long 	 Rkey; 	// Rotationsschl�ssel
	CTime 	 Stoa; 	// Planm��ige Ankunftszeit STA
	CTime 	 Stod; 	// Planm��ige Abflugzeit
	CTime 	 Tifa; 	// Zeitrahmen Ankunft
	CTime 	 Tifd; 	// Zeitrahmen Abflug
	char 	 Tisa[2]; 	// Statusdaten f�r TIFA
	char 	 Tisd[2]; 	// Statusdaten f�r TISD
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	char 	 Via3[4]; 	// Letzte Station vor Ankunft - Abflug
	char 	 Via4[5]; 	// Letzte Station vor Ankunft - Abflug
	char 	 Adid[2]; 	// 
	char 	 Ckif[6]; 	// Check-In From
	char 	 Ckit[6]; 	// Check-In To
	char 	 Dooa[2]; 	
	char 	 Dood[2]; 	
	char 	 Htyp[6];	 	
	char 	 Ttyp[6]; 	
	char 	 Vian[5]; 	
	char	 Nose[4] ;	// seats - COnfiguration
	char 	 Psta[7]; 	// Position Ankunft
	char 	 Pstd[7]; 	// Position Abflug
	char 	 Regn[13]; 	// LFZ-Kennzeichen
	char 	 Baz1[7]; 	// LFZ-Kennzeichen
	char 	 Baz4[7]; 	// LFZ-Kennzeichen
	CTime	 Bao1;
	CTime	 Bac1;
	CTime	 Bao4;
	CTime	 Bac4;
	char 	 Flti[3]; 	// LFZ-Kennzeichen
	char 	 Gtd1[7]; 	// Gate1 Abflug
	char 	 Wro1[7]; 	// Lounge1 Abflug
	char 	 Csgn[10]; 	// Call- Sign
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	CTime	 Ofbl;
	char     Vers[22];  // Version.
	char     Paxt[4];  // .
	char     Ext1[6];  // Version.
	char     Ext2[6];  // Version.
	CTime	 Airb;  
	CTime	 Land;  
	//AM 20080307. Vers is used to keep the Alert Icon Chars for GOCC

	char	 Pcom[201]; //Added for Comment for PAID by Christine
	int      Vcnt;//Vip Count 
	char	 Mopa[2]; // For new Mode of Payment implementation
	char	 Rreq[2]; // For new Mode of Payment implementation
	char     Rtow[2]; // Ready for Towing

	// Data from cca - table

	int NextF;
	int PrevF;
	int Freq;
	int ActPeri;
	int AllocPrio ;	


const CCAFLIGHTDATA& operator= ( const SEASONDLGFLIGHTDATA& ropFlight);
const CCAFLIGHTDATA& operator= ( const ROTATIONDLGFLIGHTDATA& ropFlight);

public:
	bool IsFlightOfAirline( CStringArray& strarrAirlineCodes );		// 050323 MVy: return whether this flight belongs to one of the specified airlines

}; // end CCAFLIGHTDATA
	




/////////////////////////////////////////////////////////////////////////////
// Class declaration

//@Man:
//@Memo: Read and write flight data
//@See: CedaData
/*@Doc:
  Reads and writes flight data from and to database. Stores flight data in memory 
  and provides methods for searching and retrieving flights from its internal list. 
  Does some data preparations like pre-formatting the flight number, storing flight 
  number and gate information for the corresponding inbound/outbound flight.

  Data of this class has to be updated by CEDA through broadcasts. This procedure 
  and the necessary methods will be described later with the description of 
  broadcast handling.

  {\bf CcaCedaFlightData} handles data from the EIOHDL server process.
	See the specification for table descriptions.
*/
class CcaCedaFlightData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: A map, containing the Keya/Keyd fields of all loaded flights.
    //CMapStringToPtr omKeyMap;
    CMapPtrToPtr omAlcMap;
    CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omRkeyMap;
    CMapStringToPtr omKompMap;

   //@ManMemo: CCAFLIGHTDATA records read by ReadAllFlight().
    CCSPtrArray<CCAFLIGHTDATA> omData;
    CCSPtrArray<CCAFLIGHTDATA> omSaveData;

 	char pcmAftFieldList[2048];

	bool bmOffLine;


	void SetOffLine();

	void ReleaseCca();
	void Release();

// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in 
	  {\bf CCAFLIGHTDATA}, the {\bf pcmTableName} with table name {\bf AFTLSG}, 
	  the data members {\bf pcmFieldList} contains a list of used fields of 
	  {\bf CCAFLIGHTDATAStruct}.
    */
    //CcaCedaFlightData(CCSCedaCom *popCommHandler = NULL);  : CCSCedaData(&ogCommHandler)
    CcaCedaFlightData();
    //@ManMemo: Default destructor
	~CcaCedaFlightData();
	
	void UnRegister();
	void Register();

	struct RKEYLIST
	{
		CCSPtrArray<CCAFLIGHTDATA> Rotation;

	};


	bool UpdateOfflineButtons();
	void ToggleOffline();


    //@ManMemo: Clear all flights.
    /*@Doc:
      Clear all flights.
      This method will clear {\bf omData} and {\bf omKeyMap}.
    */
	void ClearAll(void);

//	void ReadAllCca();
	void ReadAllCca(CMapStringToString& opFilterMap);
	void ReadAllCha(CMapStringToString& opFilterMap);


    // internal data access.
	bool AddFlightInternal(CCAFLIGHTDATA *prpFlight, bool bpDdx = true);
	bool DeleteFlightInternal(long lpUrno, bool bpDdx = true);

//	bool ReadAllFlights(CString &opWhere, bool bpRotation);
	bool ReadAllFlights(CString &opWhere, bool bpRotation, CMapStringToString& opFilterMap);

	//bool CheckDeparture(CCAFLIGHTDATA *prpFlight);
	
	void SetPreSelection(CTime &opFrom, CTime &opTo, CString &opFtyps);
	bool PreSelectionCheck(CString &opData, CString &opFields, bool bpCheckOldNewVal = false);

	bool ReadFlights(char *pcpSelection, bool bpDdx = true, bool bpIngoreView = false);
	bool ReadRkeys(CString &olRkeys, char *pcpSelection);

	void ReadCcas(CTime opFrom , CTime opTo);
	
	RKEYLIST *GetRotationByRkey(long lpRkey);

	bool UpdateFlight(char *pclSelection, char *pclField, char *pclValue, bool bpSave = true);

	bool UpdateFlight(CCAFLIGHTDATA *prpFlight, CCAFLIGHTDATA *prpFlightSave);

    //@ManMemo: Handle Broadcasts for flights.
	void ProcessFlightBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	
	bool CheckCkiAllocate(const CCAFLIGHTDATA *prpFlight, CString opCNam);

	bool CheckCkiAllocate(long lpUrno, CString opCNam);

	
	CCAFLIGHTDATA *GetArrival(CCAFLIGHTDATA *prpFlight);
	CCAFLIGHTDATA *GetDeparture(CCAFLIGHTDATA *prpFlight);
	CCAFLIGHTDATA *GetJoinFlight(CCAFLIGHTDATA *prpFlight);

	bool AddToKeyMap(CCAFLIGHTDATA *prpFlight );
	bool DeleteFromKeyMap(CCAFLIGHTDATA *prpFlight );
	CCAFLIGHTDATA *GetFlightByUrno(long lpUrno);



	bool ProcessFlightUPS(BcStruct *prlBcStruct);
	bool ProcessFlightUFR(BcStruct *prlBcStruct);
	bool ProcessFlightIFR(BcStruct *prlBcStruct);
	bool ProcessFlightISF(BcStruct *prlBcStruct);

	void ProcessCcaChange(long *lpUrno);

	bool bmRotation;
	
	CString omFtyps;
	CTime omFrom;
	CTime omTo;

	CString omSelection;
	bool bmReadAllFlights;


	CedaDiaCcaData omCcaData;
	CedaChaData omChaData;


	void CheckAllFlightsBazFields();

	void CheckFlightsBazFields(CCAFLIGHTDATA *prpFlight);

	bool CheckDemands(long lpUrno);

	void Writefile( CCSPtrArray<CCAFLIGHTDATA> *prlArray);
	bool IsFlightInTimerange(long& lpAlcUrno, CTime& opBeg, CTime& opEnd);
private:
	bool IsFlightOfAirline( unsigned long urnoFlight, CStringArray& strarrAirlineCodes );		// 050323 MVy: return whether this flight belongs to one of the specified airlines; hint: this is only for flight ctypes, do not use for common or precheck!
public:
	bool IsFlightOfAirline( DIACCADATA *pFlight, CStringArray& strarrAirlineCodes );		// 050324 MVy: return whether this flight belongs to one of the specified airlines
	bool IsFlightOfAirline( CCAFLIGHTDATA *pFlight, CStringArray& strarrAirlineCodes );		// 050324 MVy: return whether this flight belongs to one of the specified airlines

	unsigned long GetCicUrnoByCcaUrno( unsigned long urnoCCA );		// 050329 MVy: bars only provide a cca urno but to compare with occs I need a cic urno

};

#endif
