// CedaInfData.cpp
 
#include <stdafx.h>
#include <CedaInfData.h>


void ProcessInfCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaInfData::CedaInfData() : CCSCedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(INFDATA, InfDataRecInfo)
		CCS_FIELD_CHAR_TRIM	(Apc3,"APC3","Airport 3-Letter Code",0)
		CCS_FIELD_CHAR_TRIM	(Appl,"APPL","Applikation",0)
		CCS_FIELD_CHAR_TRIM	(Area,"AREA","Arbeitsbereich",0)
		CCS_FIELD_DATE		(Cdat,"CDAT","Erstellungsdatum",0)
		CCS_FIELD_CHAR_TRIM	(Funk,"FUNK","Funktionsbereich",0)
		CCS_FIELD_DATE		(Lstu,"LSTU","Datum letzte �nderung",0)
		CCS_FIELD_LONG		(Urno,"URNO","Eindeutige Datensatz-Nr.",0)
		CCS_FIELD_CHAR_TRIM	(Usec,"USEC","Anwender (Ersteller)",0)
		CCS_FIELD_CHAR_TRIM	(Useu,"USEU","Anwender (letzte �nderung)",0)
		CCS_FIELD_CHAR_TRIM	(Text,"TEXT","Infotext",0)
	END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(InfDataRecInfo)/sizeof(InfDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&InfDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"INF");

    sprintf(pcmListOfFields,"APC3,APPL,AREA,CDAT,FUNK,LSTU,URNO,USEC,USEU,TEXT");


	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
	Register();
}




//---------------------------------------------------------------------------------------------------------

void CedaInfData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
/*
	// Nur f�r Ansichten notwendig!
	// z.B.:
	ropFields.Add("CDAT");
	ropDesription.Add("Erstellungsdatum");
	ropType.Add("Date");
	ropType.Add("String");
*/
}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaInfData::Register(void)
{
	ogDdx.Register((void *)this,BC_INF_CHANGE,	CString("INFDATA"), CString("Inf-changed"),	ProcessInfCf);
	ogDdx.Register((void *)this,BC_INF_NEW,		CString("INFDATA"), CString("Inf-new"),		ProcessInfCf);
	ogDdx.Register((void *)this,BC_INF_DELETE,	CString("INFDATA"), CString("Inf-deleted"),	ProcessInfCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaInfData::~CedaInfData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaInfData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
	omNewInfoData.DeleteAll();
	CString olText;
	Apc3Info *prlInf;
  	for(POSITION olPos = omApc3Map.GetStartPosition(); olPos != NULL; )
	{
		omApc3Map.GetNextAssoc(olPos,olText,(void*&)prlInf);
		delete prlInf;
	}
	omApc3Map.RemoveAll();

    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

bool CedaInfData::Delete(char *pspWhere)
{
	return CedaAction("DRT",pspWhere);
}


//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaInfData::Read(char *pspFieldList/*NULL*/,char *pspWhere/*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
	char pclFieldList[256] = " ";

    omUrnoMap.RemoveAll();

	//Remove all Maps///////////
	CString olText;
	Apc3Info *prlInf;
  	for(POSITION olPos = omApc3Map.GetStartPosition(); olPos != NULL; )
	{
		omApc3Map.GetNextAssoc(olPos,olText,(void*&)prlInf);
		delete prlInf;
	}
	omApc3Map.RemoveAll();
	////////////////////////////

    omData.DeleteAll();

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}

	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT",pcmTableName,pclFieldList,"","",pcgDataBuf);
	}
	else
	{
		ilRc = CedaAction("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		INFDATA *prlInf = new INFDATA;
		if ((ilRc = GetFirstBufferRecord(prlInf)) == true)
		{
			omData.Add(prlInf);//Update omData
			omUrnoMap.SetAt((void *)prlInf->Urno,prlInf);
			AddInf(prlInf);
		}
		else
		{
			delete prlInf;
		}

	}
	return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaInfData::Insert(INFDATA *prpInf)
{
	prpInf->IsChanged = DATA_NEW;
	if(Save(prpInf) == false) return false; //Update Database
	InsertInternal(prpInf);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaInfData::InsertInternal(INFDATA *prpInf)
{
	prpInf->Text[0] = '\0';
	omData.Add(prpInf);//Update omData
	omUrnoMap.SetAt((void *)prpInf->Urno,prpInf);
	AddInf(prpInf);
	ogDdx.DataChanged((void *)this, INF_NEW,(void *)prpInf ); //Update Dlg
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaInfData::Delete(long lpUrno)
{
	INFDATA *prlInf = GetInfByUrno(lpUrno);
	if (prlInf != NULL)
	{
		prlInf->IsChanged = DATA_DELETED;
		if(Save(prlInf) == false) return false; //Update Database
		DeleteInternal(prlInf);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaInfData::DeleteInternal(INFDATA *prpInf)
{
	ogDdx.DataChanged((void *)this,INF_DELETE,(void *)prpInf); //Update Dlg
	omUrnoMap.RemoveKey((void *)prpInf->Urno);
	int ilInfCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilInfCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpInf->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaInfData::Update(INFDATA *prpInf)
{
	if (GetInfByUrno(prpInf->Urno) != NULL)
	{
		if (prpInf->IsChanged == DATA_UNCHANGED)
		{
			prpInf->IsChanged = DATA_CHANGED;
		}
		if(Save(prpInf) == false) return false; //Update Database
		UpdateInternal(prpInf);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaInfData::UpdateInternal(INFDATA *prpInf)
{
	INFDATA *prlInf = GetInfByUrno(prpInf->Urno);
	if (prlInf != NULL)
	{
		prpInf->Text[0] = '\0';
		*prlInf = *prpInf; //Update omData
		ogDdx.DataChanged((void *)this,INF_CHANGE,(void *)prlInf); //Update Dlg
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

INFDATA *CedaInfData::GetInfByUrno(long lpUrno)
{
	INFDATA  *prlInf;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlInf) == TRUE)
	{
		return prlInf;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaInfData::ReadSpecialData(CCSPtrArray<INFDATA> *popInf,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popInf != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			INFDATA *prpInf = new INFDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpInf,CString(pclFieldList))) == true)
			{
				popInf->Add(prpInf);
			}
			else
			{
				delete prpInf;
			}
		}
		if(popInf->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaInfData::Save(INFDATA *prpInf)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[4048];

	if (prpInf->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpInf->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpInf);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpInf->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpInf->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpInf);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpInf->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpInf->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessInfCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogInfData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaInfData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlInfData;
	prlInfData = (struct BcStruct *) vpDataPointer;
	INFDATA *prlInf;
	if(ipDDXType == BC_INF_NEW)
	{
		prlInf = new INFDATA;
		GetRecordFromItemList(prlInf,prlInfData->Fields,prlInfData->Data);

		NEWINFDATA *prlNewInf = new NEWINFDATA;
		prlNewInf->Urno = prlInf->Urno;
		omNewInfoData.Add(prlNewInf);

		InsertInternal(prlInf);
	}
	if(ipDDXType == BC_INF_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlInfData->Selection);
		prlInf = GetInfByUrno(llUrno);
		if(prlInf != NULL)
		{
			GetRecordFromItemList(prlInf,prlInfData->Fields,prlInfData->Data);

			NEWINFDATA *prlNewInf = new NEWINFDATA;
			prlNewInf->Urno = prlInf->Urno;
			omNewInfoData.Add(prlNewInf);

			UpdateInternal(prlInf);
		}
	}
	if(ipDDXType == BC_INF_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlInfData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlInfData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+1;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlInf = GetInfByUrno(llUrno);
		if (prlInf != NULL)
		{
			DeleteInternal(prlInf);
		}
	}
}

//-----TRANSLATETEXT--------------------------------------------------------------------------------------------

// Copies content of pclOldText to pclNewText, translating special chars and returns a pointer to pclNewText
// If blFromDb is true, translates from DB chars to display chars, else translates from display chars to DB chars
//
// DB chars		Display Chars
//   176    -->    34        
//   177    -->    39        
//   178    -->    44        
//   179    -->    10        

LPCTSTR CedaInfData::TranslateText(INFDATA *prpInfRec, CString &ropText, bool blFromDb)
{
	int ilTextLen;
	int ilInfoLen = 2001;
	char *pclOldText; // current text
	char *pclNewText; // translated text
	const short NUMCHARS = 4;
	imTextLength = 0;

	if(blFromDb)
	{
		ilTextLen = strlen(prpInfRec->Text);

		if(ilTextLen <= 0)
		{
			strcpy(prpInfRec->Text," ");
			ilTextLen = strlen(prpInfRec->Text);
		}

		pclOldText = new char[ilTextLen+1]; // current text
		sprintf(pclOldText,"%s",prpInfRec->Text);

		pclNewText = new char[ilTextLen*2]; // translated text
		memset(pclNewText,0,(ilTextLen*2));
	}
	else
	{
		ilTextLen = ropText.GetLength();

		if(ilTextLen <= 0)
		{
			ropText = " ";
			ilTextLen = ropText.GetLength();
		}

		pclOldText = new char[ilTextLen+1]; // current text
		strcpy(pclOldText,ropText);

		pclNewText = new char[ilTextLen*2]; // translated text
		memset(pclNewText,0,(ilTextLen*2));
	}
	
	char pclDbChars[NUMCHARS],pclDisplayChars[NUMCHARS];

	pclDbChars[0] = (char) 176;
	pclDbChars[1] = (char) 177;
	pclDbChars[2] = (char) 178;
	pclDbChars[3] = (char) 179;
	pclDisplayChars[0] = (char) 34;
	pclDisplayChars[1] = (char) 39;
	pclDisplayChars[2] = (char) 44;
	pclDisplayChars[3] = (char) 10;

	char *pclFromChars, *pclToChars;
	int ilLen = strlen(pclOldText), ilNew = 0, ilOld = 0;

	if( blFromDb )
	{
		// copy the text adding linefeeds to carriage returns, when required
		for( ; ilOld < ilLen; ilOld++ )
		{
			pclNewText[ilNew++] = pclOldText[ilOld];
			if( (int) pclOldText[ilOld] == 13  )
				pclNewText[ilNew++] = (char) 10;
		}
		
		// translate from DB format to display format
		pclFromChars = pclDbChars;
		pclToChars = pclDisplayChars;
	}
	else
	{
		// copy the text removing linefeeds
		for( ; ilOld < ilLen; ilOld++ )
			if( (int) pclOldText[ilOld] != 10 )
				pclNewText[ilNew++] = pclOldText[ilOld];

		// translate from display format to DB format
		pclFromChars = pclDisplayChars;
		pclToChars = pclDbChars;
	}

	char *pclChar;

	// loop through the list of chars to search for (pclFromChars), changing them to pclToChars in pclNewText
	for( int ilChar = 0; ilChar < NUMCHARS; ilChar++ )
		while((pclChar = strchr(pclNewText,pclFromChars[ilChar])) != NULL)
			*pclChar = pclToChars[ilChar];

	if(blFromDb)
	{
		// copy the new text to the return string
		ropText.Empty();
		ropText = CString(pclNewText);
	}
	else
	{
		// copy new text to the text fields
		memset(prpInfRec->Text,0,ilInfoLen);
		CString olFormat;
		olFormat.Format("%%.%ds",ilInfoLen-1);
		sprintf(prpInfRec->Text,olFormat,pclNewText);
	}
	imTextLength = strlen(pclNewText);

	delete [] pclOldText;
	delete [] pclNewText;

	return (LPCTSTR) ropText;

} // end TranslateText()

//-------------------------------------------------------------------------------------------------

void CedaInfData::AddInf(INFDATA *prpInf)
{
	Apc3Info *polInf;
	polInf = FindInf((CString)prpInf->Apc3);
	if(polInf == NULL)
	{
		Apc3Info *prlInf = new Apc3Info;
		omApc3Map.SetAt((CString)prpInf->Apc3,prlInf);
		prlInf->omApc3 = (CString)prpInf->Apc3;
		prlInf->AddInf(prpInf);
	}
	else
	{
		polInf->AddInf(prpInf);
	}
}
//-------------------------------------------------------------------------------------------------

Apc3Info *CedaInfData::FindInf(CString opApc3)
{
	Apc3Info *prlInf;
	if(omApc3Map.Lookup(opApc3,(void *&)prlInf) == TRUE)
	{
		return prlInf;
	}
	return NULL;
}

// End CedaInfData ////////////////////////////////////////////////////////////////////////////////
//-------------------------------------------------------------------------------------------------

Apc3Info::Apc3Info()
{
}

Apc3Info::~Apc3Info()
{
	CString olText;
	ApplInfo *prlInf;
  	for(POSITION olPos = omApplMap.GetStartPosition(); olPos != NULL; )
	{
		omApplMap.GetNextAssoc(olPos,olText,(void*&)prlInf);
		delete prlInf;
	}
    omApplMap.RemoveAll();
}

void Apc3Info::AddInf(INFDATA *prpInf)
{
	ApplInfo *polInf;
	polInf = FindInf((CString)prpInf->Appl);
	if(polInf == NULL)
	{
		ApplInfo *prlInf = new ApplInfo;
		omApplMap.SetAt((CString)prpInf->Appl,prlInf);
		prlInf->omAppl = (CString)prpInf->Appl;
		prlInf->AddInf(prpInf);
	}
	else
	{
		polInf->AddInf(prpInf);
	}
}

ApplInfo *Apc3Info::FindInf(CString opAppl)
{
	ApplInfo *prlInf;
	if(omApplMap.Lookup(opAppl,(void *&)prlInf) == TRUE)
	{
		return prlInf;
	}
	return NULL;
}

//-------------------------------------------------------------------------------------------------

ApplInfo::ApplInfo()
{
}

ApplInfo::~ApplInfo()
{
	CString olText;
	FunkInfo *prlInf;
  	for(POSITION olPos = omFunkMap.GetStartPosition(); olPos != NULL; )
	{
		omFunkMap.GetNextAssoc(olPos,olText,(void*&)prlInf);
		delete prlInf;
	}
    omFunkMap.RemoveAll();
}

void ApplInfo::AddInf(INFDATA *prpInf)
{
	FunkInfo *polInf;
	polInf = FindInf((CString)prpInf->Funk);
	if(polInf == NULL)
	{
		FunkInfo *prlInf = new FunkInfo;
		omFunkMap.SetAt((CString)prpInf->Funk,prlInf);
		prlInf->omFunk = (CString)prpInf->Funk;
		prlInf->AddInf(prpInf);
	}
	else
	{
		polInf->AddInf(prpInf);
	}
}

FunkInfo *ApplInfo::FindInf(CString opFunk)
{
	FunkInfo *prlInf;
	if(omFunkMap.Lookup(opFunk,(void *&)prlInf) == TRUE)
	{
		return prlInf;
	}
	return NULL;
}


//-------------------------------------------------------------------------------------------------

FunkInfo::FunkInfo()
{
}

FunkInfo::~FunkInfo()
{
	CString olText;
	AreaInfo *prlInf;
  	for(POSITION olPos = omAreaMap.GetStartPosition(); olPos != NULL; )
	{
		omAreaMap.GetNextAssoc(olPos,olText,(void*&)prlInf);
		delete prlInf;
	}
	omAreaMap.RemoveAll();
}

void FunkInfo::AddInf(INFDATA *prpInf)
{
	AreaInfo *polInf;
	polInf = FindInf((CString)prpInf->Area);
	if(polInf == NULL)
	{
		AreaInfo *prlInf = new AreaInfo;
		omAreaMap.SetAt((CString)prpInf->Area,prlInf);
		prlInf->omArea = (CString)prpInf->Area;
		prlInf->AddInf(prpInf);
	}
	else
	{
		polInf->AddInf(prpInf);
	}
}

AreaInfo *FunkInfo::FindInf(CString opArea)
{
	AreaInfo *prlInf;
	if(omAreaMap.Lookup(opArea,(void *&)prlInf) == TRUE)
	{
		return prlInf;
	}
	return NULL;
}

//-------------------------------------------------------------------------------------------------

AreaInfo::AreaInfo()
{
}

AreaInfo::~AreaInfo()
{
	omUrnoMap.RemoveAll();
}

void AreaInfo::AddInf(INFDATA *prpInf)
{
	INFDATA *polInf;
	polInf = FindInf(prpInf->Urno);
	if(polInf == NULL)
	{
		omUrnoMap.SetAt((void *)prpInf->Urno,prpInf);
	}
}

INFDATA *AreaInfo::FindInf(long lpUrno)
{
	INFDATA *prlInf;
	if(omUrnoMap.Lookup((void *)lpUrno,(void *&)prlInf) == TRUE)
	{
		return prlInf;
	}
	return NULL;
}

//-------------------------------------------------------------------------------------------------
