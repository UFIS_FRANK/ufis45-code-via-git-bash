// BltCcaTableViewer.cpp : implementation file
// 
// Modification History: 


#include <stdafx.h>
#include <BltCcaTableViewer.h>
#include <BltCcaTableDlg.h>
#include <CcsGlobl.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <math.h>
#include <CedaBasicData.h>
#include <Utils.h>

#include <resrc1.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif




// Local function prototype
static void CcaTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// BltCcaTableViewer
//


int BltCcaTableViewer::imTableColCharWidths[BLTCCATABLE_COLCOUNT]={12, 9, 14, 8, 14, 14};


BltCcaTableViewer::BltCcaTableViewer(BltCcaTableDlg *popParentDlg):
	imOrientation(PRINT_PORTRAET),
 	romTableHeaderFont(ogCourier_Bold_10), romTableLinesFont(ogCourier_Regular_10),
	fmTableHeaderFontWidth(9), fmTableLinesFontWidth(9)
{
	pomParentDlg = popParentDlg;
	ASSERT(pomParentDlg != NULL);

	dgCCSPrintFactor = 3;
 
	// Table header strings
	omTableHeadlines[0]=GetString(IDS_STRING1153);	// Counter
	omTableHeadlines[1]=GetString(IDS_STRING296);	// Flight 
	omTableHeadlines[2]=GetString(IDS_STRING316);	// STD 
	omTableHeadlines[3]=GetString(IDS_STRING792);	// Remark 
	omTableHeadlines[4]=GetString(IDS_STRING1897);	// Open 
	omTableHeadlines[5]=GetString(IDS_STRING1898);  // Close
  
	// calculate table column widths
	for (int i=0; i < BLTCCATABLE_COLCOUNT; i++)
	{
 		imTableColWidths[i] = (int) max(imTableColCharWidths[i]*fmTableLinesFontWidth, 
								        omTableHeadlines[i].GetLength()*fmTableHeaderFontWidth);
	}

    pomTable = NULL;
	pomFlightData = NULL;
}


void BltCcaTableViewer::UnRegister()
{
	ogDdx.UnRegister(this, NOTUSED);
 
}

BltCcaTableViewer::~BltCcaTableViewer()
{
	UnRegister();
    DeleteAll();
}

void BltCcaTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}



void BltCcaTableViewer::ChangeViewTo(DiaCedaFlightData *popFlightData)
{

	pomFlightData = popFlightData;

	if (pomTable == NULL)
		return;

	// Register broadcasts
	UnRegister();
	ogDdx.Register(this, DIACCA_CHANGE,	CString("BLTCCAVIEWER"), CString("BltCcaTableViewer"), CcaTableCf);

	// Rebuild table
    pomTable->ResetContent();
    DeleteAll();  
	if (pomFlightData)
	 	MakeLines();
	UpdateDisplay();
}



/////////////////////////////////////////////////////////////////////////////
// BltCcaTableViewer -- code specific to this class

// make intern lines
void BltCcaTableViewer::MakeLines(void)
{
	DIACCADATA *prlData;

	int ilDataCount = pomFlightData->omCcaData.omData.GetSize();

	// Scan all loaded data
	for (int ilLc = 0; ilLc < ilDataCount; ilLc++)
	{
		prlData = &(pomFlightData->omCcaData.omData[ilLc]);

 		MakeLine(*prlData);
	}

}

		


int BltCcaTableViewer::MakeLine(const DIACCADATA &rrpData)
{
	if (!IsPassFilter(rrpData))
		return -1;
    
	BLTCCATABLE_LINEDATA rlLine;
 
 	MakeLineData(rrpData, rlLine);
	return CreateLine(rlLine);
 
}



// fill the intern line data
void BltCcaTableViewer::MakeLineData(const DIACCADATA &rrpData, BLTCCATABLE_LINEDATA &rrpLine)
{
	CString olHall;
	ogBCD.GetField("CIC","CNAM", rrpData.Ckic , "HALL", olHall);

	rrpLine.Urno = rrpData.Urno;
	rrpLine.FUrno =  rrpData.Flnu;

	if (bgCnamAtr)
	{
		rrpLine.Counter = olHall + GetCnamExt(CString(rrpData.Ckic), CString(""));
	}
	else
	{
		rrpLine.Counter = olHall + rrpData.Ckic;
	}

	rrpLine.FidsRemark = CString(rrpData.Disp);
	rrpLine.Open = rrpData.Ckbs;
	rrpLine.Close = rrpData.Ckes;

	DIAFLIGHTDATA *polFlightData = pomFlightData->GetFlightByUrno(rrpLine.FUrno);
	if (polFlightData)
	{
		rrpLine.Flno = polFlightData->Flno;
		rrpLine.Stod = polFlightData->Stod;
	}

	// local times requested?
	if(bgGatPosLocal) UtcToLocal(rrpLine);

    return;
}


// convert all the times in the given line in local time
bool BltCcaTableViewer::UtcToLocal(BLTCCATABLE_LINEDATA &rrpLine)
{
	ogBasicData.UtcToLocal(rrpLine.Open);
	ogBasicData.UtcToLocal(rrpLine.Close);
	ogBasicData.UtcToLocal(rrpLine.Stod);
 
	return true;
}


bool BltCcaTableViewer::IsPassFilter(const DIACCADATA &rrpData) const
{
	if (strlen(rrpData.Ckic) < 1) return false;
	//if (strlen(rrpData.Ctyp) != 0) return false;

	return true;
}




// create one internal line
int BltCcaTableViewer::CreateLine(const BLTCCATABLE_LINEDATA &rrpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareLines(rrpLine, omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rrpLine);
	return ilLineno;
}


void BltCcaTableViewer::DeleteLine(int ipLineno)
{
	// delete internal line
	omLines.DeleteAt(ipLineno);
	// delete table line
	pomTable->DeleteTextLine(ipLineno);

}



bool BltCcaTableViewer::FindLine(long lpUrno, int &ripLineno) const
{
	ripLineno = -1;
	// scan all intern lines
    for (int i = omLines.GetSize() - 1; i >= 0; i--)
	{
	  if(omLines[i].FUrno == lpUrno)
	  {
		ripLineno = i;
		return true;
	  }
	}
	return false;
}






/////////////////////////////////////////////////////////////////////////////
// BltCcaTableViewer - BLTCCATABLE_LINEDATA array maintenance

void BltCcaTableViewer::DeleteAll()
{
    omLines.DeleteAll();
}


/////////////////////////////////////////////////////////////////////////////
// BltCcaTableViewer - display drawing routine


void BltCcaTableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	DrawHeader();

	BLTCCATABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
 		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(*prlLine, olColList);
		// add table line
		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}


    pomTable->DisplayTable();
}

// Insert new line for the given cca
void BltCcaTableViewer::InsertCca(const DIACCADATA &rrpData)
{
 	InsertDisplayLine(MakeLine(rrpData));
}



void BltCcaTableViewer::InsertDisplayLine( int ipLineNo)
{
	if(!((ipLineNo >= 0) && (ipLineNo < omLines.GetSize())))
		return;
	CCSPtrArray<TABLE_COLUMN> olColList;
	MakeColList(omLines[ipLineNo], olColList);
	pomTable->InsertTextLine(ipLineNo, olColList, &omLines[ipLineNo]);
	olColList.DeleteAll();
}






void BltCcaTableViewer::DrawHeader()
{
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Font = &romTableHeaderFont;

	// for all rows
	for (int i=0; i < BLTCCATABLE_COLCOUNT; i++)
	{
		rlHeader.Alignment = COLALIGN_CENTER;
		rlHeader.Length = imTableColWidths[i]; 
		rlHeader.Text = omTableHeadlines[i]; 
		omHeaderDataArray.New(rlHeader);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}


// fill the table line with the data of the given intern line
void BltCcaTableViewer::MakeColList(const BLTCCATABLE_LINEDATA &rrpLine, CCSPtrArray<TABLE_COLUMN> &ropColList) const
{
	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &romTableLinesFont;
	rlColumnData.Alignment = COLALIGN_LEFT;

	rlColumnData.Text = rrpLine.Counter;
	ropColList.New(rlColumnData);
 
 	rlColumnData.Text = rrpLine.Flno;
	ropColList.New(rlColumnData);

 	rlColumnData.Text = rrpLine.Stod.Format("%d.%m.%y %H:%M");
	ropColList.New(rlColumnData);

 	rlColumnData.Text = rrpLine.FidsRemark;
	ropColList.New(rlColumnData);

 	rlColumnData.Text = rrpLine.Open.Format("%d.%m.%y %H:%M");
	ropColList.New(rlColumnData);

 	rlColumnData.Text = rrpLine.Close.Format("%d.%m.%y %H:%M");
	ropColList.New(rlColumnData);

}


//////////////////////////////////////////////////////////////////
//// Printing routines
 
// Print table to paper
void BltCcaTableViewer::PrintTableView(void)
{

	if (pomTable==NULL)
		return;

	CCSPrint olPrint(NULL, imOrientation, 45);

	// Set printing fonts
	pomPrintHeaderFont = &olPrint.ogCourierNew_Bold_8;
	pomPrintLinesFont = &olPrint.ogCourierNew_Regular_8;
	fmPrintHeaderFontWidth = 6.5;
	fmPrintLinesFontWidth = 6.5;

	// calculate table column widths for printing
	for (int i=0; i < BLTCCATABLE_COLCOUNT; i++)
	{
  		imPrintColWidths[i] = (int) max(imTableColCharWidths[i]*fmPrintLinesFontWidth, 
								        omTableHeadlines[i].GetLength()*fmPrintHeaderFontWidth);
	}

	CString olFooter1,olFooter2;
	// Set left footer to: "printed at: <date>"
 	olFooter1.Format("%s %s", GetString(IDS_STRING1481),
		(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));

  	if (olPrint.InitializePrinter(imOrientation) == TRUE)
	{ 
		olPrint.imMaxLines = 57;	// Def. imMaxLines: 57 Portrait / 38 Landscape
		// Calculate number of pages
		const double dlPages = ceil((double)pomTable->GetLinesCount() / (double)(olPrint.imMaxLines - 1));
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		// At first a pagebreak
		olPrint.imLineNo = olPrint.imMaxLines + 1;	
		DOCINFO	rlDocInfo;
		memset(&rlDocInfo, 0, sizeof(DOCINFO));
		rlDocInfo.cbSize = sizeof( DOCINFO );
		rlDocInfo.lpszDocName = GetString(IDS_STRING1937);	
		olPrint.omCdc.StartDoc( &rlDocInfo );
		olPrint.imPageNo = 0;
		// Print all table lines
		for (int ilLc = 0; ilLc < pomTable->GetLinesCount(); ilLc++)
		{
			// Page break
 			if(olPrint.imLineNo >= olPrint.imMaxLines)
			{
				if(olPrint.imPageNo > 0)
				{
					// Set right footer to: "Page: %d"
					olFooter2.Format(GetString(IDS_STRING1199),olPrint.imPageNo, dlPages);
					// print footer
					olPrint.PrintUIFFooter(olFooter1,"",olFooter2);
					olPrint.omCdc.EndPage();
				}
				// print header
				PrintTableHeader(olPrint);
			}				
			// print line
			PrintTableLine(olPrint, ilLc);
		}
		// print footer
		olFooter2.Format(GetString(IDS_STRING1199),olPrint.imPageNo, dlPages);
		olPrint.PrintUIFFooter(olFooter1,"",olFooter2);
		olPrint.omCdc.EndPage();
		olPrint.omCdc.EndDoc();
	}  // if (olPrint.InitializePrin...
 	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}




bool BltCcaTableViewer::PrintTableHeader(CCSPrint &ropPrint) const
{
	ropPrint.omCdc.StartPage();
	ropPrint.imPageNo++;
	ropPrint.imLineNo = 0;
	//double dgCCSPrintFactor = 2.7 ;
	CString olHeadline;

	// Headline
 	olHeadline.Format(GetString(IDS_STRING1937), pomTable->GetLinesCount());
 
	// print page headline
	ropPrint.imLeftOffset = 150;
	ropPrint.PrintUIFHeader(CString(), olHeadline, ropPrint.imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_NOFRAME;
	rlElement.pFont       = pomPrintHeaderFont;

	// Create table headline
	for (int ilCc = 0; ilCc < BLTCCATABLE_COLCOUNT; ilCc++)
	{
		rlElement.Length = (int)(imPrintColWidths[ilCc]*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength) 
			rlElement.Length+=igCCSPrintMoreLength; 

		rlElement.Text = omTableHeadlines[ilCc]; 
		
		rlPrintLine.NewAt(rlPrintLine.GetSize(), rlElement);
 	}
	// Print table headline
 	ropPrint.PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	return true;
}


 

bool BltCcaTableViewer::PrintTableLine(CCSPrint &ropPrint, int ipLineNo) const {
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	
	//double dgCCSPrintFactor = 2.7 ;

	PRINTELEDATA rlElement;
	rlElement.pFont = pomPrintLinesFont;
	rlElement.FrameTop = PRINT_FRAMETHIN;
 	rlElement.FrameBottom = PRINT_FRAMETHIN;
	rlElement.FrameLeft  = PRINT_FRAMETHIN;
	rlElement.FrameRight = PRINT_FRAMETHIN;

	CString olCellValue;
	// create table line
	for (int ilCc = 0; ilCc < BLTCCATABLE_COLCOUNT; ilCc++)
	{
		rlElement.Alignment  = PRINT_LEFT;
 		rlElement.Length = (int)(imPrintColWidths[ilCc]*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength) 
			rlElement.Length+=igCCSPrintMoreLength; 
		// Get printing text from table	
		pomTable->GetTextFieldValue(ipLineNo, ilCc, olCellValue);
		rlElement.Text = olCellValue; 
		
 		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	// print table line
	ropPrint.PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll(); 
	
	return true;
}



// Print table to file
bool BltCcaTableViewer::PrintPlanToFile(CString opFilePath) const
{

	if (pomTable==NULL)
		return false;


	if(opFilePath.GetLength() != 0)
	{
		// Get Seperator for Excel-File
		char pclConfigPath[256];
		char pclTrenner[2];
		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
		    strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(ogAppName, "ExcelSeparator", ";",
		    pclTrenner, sizeof pclTrenner, pclConfigPath);

		ofstream of;
		of.open(opFilePath.GetBuffer(0), ios::out);

		// Header
		for (int ilCc = 0; ilCc < BLTCCATABLE_COLCOUNT; ilCc++)
		{
			of  << omTableHeadlines[ilCc];
			if (ilCc < BLTCCATABLE_COLCOUNT-1)
			{
				of << pclTrenner;
			}
		}
		of << endl;

		CString olCellValue;
		 // Lines
		for (int ilLc = 0; ilLc < pomTable->GetLinesCount(); ilLc++)
		{
			for (int ilCc = 0; ilCc < BLTCCATABLE_COLCOUNT; ilCc++)
			{
				// get text from table
				pomTable->GetTextFieldValue(ilLc, ilCc, olCellValue);
				of  << olCellValue;
				if (ilCc < BLTCCATABLE_COLCOUNT-1)
				{
					of << pclTrenner;
				}
			}
			of << endl;
		}

		of.close();
 
		return true;
	} // 	if(GetSaveFileName(polOfn) != 0)

	return false;
}





// broadcast-function
static void CcaTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	if (vpDataPointer == NULL) 
		return;

    BltCcaTableViewer *polViewer = (BltCcaTableViewer *)popInstance;

    if (ipDDXType == DIACCA_CHANGE)
        polViewer->ProcessCcaChange(*((long *)vpDataPointer));
	if (ipDDXType == DIACCA_DELETE)
        polViewer->ProcessCcaDelete(*((long *)vpDataPointer));
}




void BltCcaTableViewer::ProcessCcaChange(long lpCcaUrno)
{
	if (!pomFlightData) return;

	// Delete the cca line
	ProcessCcaDelete(lpCcaUrno);
 	
	const DIACCADATA *prlData = pomFlightData->omCcaData.GetCcaByUrno(lpCcaUrno);
	//if (prlData) TRACE("BltCcaTableViewer::ProcessCcaChange: Urno=%ld Ckit=%s Ckic=%s\n", prlData->Urno, prlData->Ckit, prlData->Ckic);
	// Insert the changed cca
	if (prlData)
			InsertCca(*prlData);

	pomParentDlg->UpdateCaption();

	return;
}







bool BltCcaTableViewer::ProcessCcaDelete(long lpCcaUrno)
{
 
	// Delete the cca line 
    for (int ilLineNo = omLines.GetSize() - 1; ilLineNo >= 0; ilLineNo--)
	{
	  if(omLines[ilLineNo].Urno == lpCcaUrno)
	  {
		DeleteLine(ilLineNo);
		pomParentDlg->UpdateCaption();
		return true;
	  }
	}
	return false;
}




// compare function for sorting
int BltCcaTableViewer::CompareLines(const BLTCCATABLE_LINEDATA &rrpLine1, const BLTCCATABLE_LINEDATA &rrpLine2) const
{
	if (rrpLine1.Counter > rrpLine2.Counter) return 1;
	if (rrpLine1.Counter < rrpLine2.Counter) return -1;

	if (rrpLine1.Stod > rrpLine2.Stod) return 1;
	if (rrpLine1.Stod < rrpLine2.Stod) return -1;

	if (rrpLine1.Flno > rrpLine2.Flno) return 1;
	if (rrpLine1.Flno < rrpLine2.Flno) return -1;

	return 0;
}








