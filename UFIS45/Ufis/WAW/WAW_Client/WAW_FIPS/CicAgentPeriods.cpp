// CicAgentPeriods.cpp: implementation of the CCicAgentPeriods class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "fpms.h"
#include "CicAgentPeriods.h"
#include "CCSglobl.h"
#include "CedaBasicData.h"
#include "BasicData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCicAgentPeriods::CCicAgentPeriods()
{
	m_pbrshBackground = 0 ;
};	// constructor

CCicAgentPeriods::~CCicAgentPeriods()
{
	DeepKill();
	DeleteBackBrush();
};	// destructor

// use before generating new one
void CCicAgentPeriods::DeleteBackBrush()
{
	if( m_pbrshBackground )
		delete m_pbrshBackground ;
	m_pbrshBackground = 0 ;
};	// DeleteBackBrush

// create brush with specified color
void CCicAgentPeriods::SetBackColor( unsigned long ulRGB )
{
	CBrush* pBrush = new CBrush();
	VERIFY( pBrush->CreateSolidBrush( ulRGB ) );
	SetBackBrush( pBrush );
};	// SetBackColor

// set new brush
void CCicAgentPeriods::SetBackBrush( CBrush* pBrush )
{
	DeleteBackBrush();
	m_pbrshBackground = pBrush ;
};	// SetBackBrush

// return pointer to current brush or null if no one was set
CBrush* CCicAgentPeriods::GetBackBrush()
{
	return m_pbrshBackground ;
};	// GetBackBrush

// for iteration, check if at last element
// attention: do not use in for-loops in combination with getfirst, getnext, you wont get the last element in list
bool CCicAgentPeriods::AtEnd( long pos )
{
	long size = GetSize();
	ASSERT( size >= 0 );
	return !size || (pos >= size) ;		// if empty return true, too
};	// AtEnd

// find the first period regarding to agent
CCicAgentPeriod* CCicAgentPeriods::FindFirstByAgent( long &pos, long urno )
{
	pos = 0 ;
	return FindNextByAgent( pos, urno );
};	// FindFirstByAgent

// find the next period regarding to agent
CCicAgentPeriod* CCicAgentPeriods::FindNextByAgent( long &pos, long urno )
{
	while( !AtEnd( pos ) )
	{
		CCicAgentPeriod* pPeriod = (CCicAgentPeriod *)GetAt( pos++ );
		ASSERT( pPeriod );
		if( pPeriod->IsAgentUrno( urno ) )
			return pPeriod ;
	};
	return 0 ;
};	// FindNextByAgent

// find the first period regarding to counter
CCicAgentPeriod* CCicAgentPeriods::FindFirstByCounter( long &pos, long urno )
{
	pos = 0 ;
	return FindNextByCounter( pos, urno );
};	// FindFirstByCounter

// find the next period regarding to agent
CCicAgentPeriod* CCicAgentPeriods::FindNextByCounter( long &pos, long urno )
{
	while( !AtEnd( pos ) )
	{
		CCicAgentPeriod* pPeriod = (CCicAgentPeriod *)GetAt( pos++ );
		ASSERT( pPeriod );
		if( pPeriod->IsCounterUrno( urno ) )
			return pPeriod ;
	};
	return 0 ;
};	// FindNextByCounter

// find the first period regarding to counter and counter
CCicAgentPeriod* CCicAgentPeriods::FindFirstByAgentAndCounter( long &pos, long urnoAgent, long urnoCounter )
{
	pos = 0 ;
	return FindNextByAgentAndCounter( pos, urnoAgent, urnoCounter );
};	// FindFirstByAgentAndCounter

// find the next period regarding to agent and counter; use ANY_COUNTER_URNO to ignore counter
CCicAgentPeriod* CCicAgentPeriods::FindNextByAgentAndCounter( long &pos, long urnoAgent, long urnoCounter )
{
	while( !AtEnd( pos ) )
	{
		CCicAgentPeriod* pPeriod = (CCicAgentPeriod *)GetAt( pos++ );
		ASSERT( pPeriod );
		if( ( urnoCounter == ANY_COUNTER_URNO		// ignore urno
				|| pPeriod->IsCounterUrno( urnoCounter )
			&& pPeriod->IsAgentUrno( urnoAgent ) )
			)
			return pPeriod ;
	};
	return 0 ;
};	// FindNextByAgentAndCounter

CCicAgentPeriod* CCicAgentPeriods::FindFirstByTime( long &pos, const CTime &time )
{
	pos = 0 ;
	return FindNextByTime( pos, time );
};	// FindFirstByTime

// TODO: I guess this will need speedup due to usage within mouse movement and many other functions
CCicAgentPeriod* CCicAgentPeriods::FindNextByTime( long &pos, const CTime &time )
{
	while( !AtEnd( pos ) )
	{
		CCicAgentPeriod* pPeriod = (CCicAgentPeriod *)GetAt( pos++ );
		ASSERT( pPeriod );
		if( pPeriod->IsInRange( time ) ) 
			return pPeriod ;
	};
	return 0 ;
};	// FindNextByTime


// return wheter specified time is within a period or not
CCicAgentPeriod* CCicAgentPeriods::IsInRange( const CTime &time )
{
	long pos = 0 ;
	while( !AtEnd( pos ) )
	{
		CCicAgentPeriod* pPeriod = (CCicAgentPeriod *)GetAt( pos++ );
		ASSERT( pPeriod );
		if( pPeriod->IsInRange( time ) )
			return pPeriod ;
	};
	return 0 ;
};	// IsInRange

// return wheter specified time is within a period or not, look for counter
CCicAgentPeriod* CCicAgentPeriods::IsInRange( const CTime &time, long urnoCounter )
{
	long pos = 0 ;
	while( !AtEnd( pos ) )
	{
		CCicAgentPeriod* pPeriod = (CCicAgentPeriod *)GetAt( pos++ );
		ASSERT( pPeriod );
		if( pPeriod->IsInRange( time ) 
			&& pPeriod->IsCounterUrno( urnoCounter ) )
			return pPeriod ;
	};
	return 0 ;
};	// IsInRange

// return wheter specified time is within a period or not, look for counter and agent; use ANY_COUNTER_URNO to ignore counter
CCicAgentPeriod* CCicAgentPeriods::IsInRange( const CTime &time, long urnoCounter, long urnoAgent )
{
	long pos = 0 ;
	while( !AtEnd( pos ) )
	{
		CCicAgentPeriod* pPeriod = (CCicAgentPeriod *)GetAt( pos++ );
		ASSERT( pPeriod );
		if( pPeriod->IsInRange( time ) 
			&& ( urnoCounter == ANY_COUNTER_URNO		// ignore urno
				|| pPeriod->IsCounterUrno( urnoCounter )
			&& pPeriod->IsAgentUrno( urnoAgent ) )
			)
			return pPeriod ;
	};
	return 0 ;
};	// IsInRange


// return wheter specified time range is completely within a period or not
CCicAgentPeriod* CCicAgentPeriods::IsInRange( const CTimeRange &range )
{
	long pos = 0 ;
	while( !AtEnd( pos ) )
	{
		CCicAgentPeriod* pPeriod = (CCicAgentPeriod *)GetAt( pos++ );
		ASSERT( pPeriod );
		if( pPeriod->IsInRange( range ) )
			return pPeriod ;
	};
	return 0 ;
};	// IsInRange

// return wheter specified time range is completely within a period or not, look for counter
CCicAgentPeriod* CCicAgentPeriods::IsInRange( const CTimeRange &range, long urnoCounter )
{
	long pos = 0 ;
	while( !AtEnd( pos ) )
	{
		CCicAgentPeriod* pPeriod = (CCicAgentPeriod *)GetAt( pos++ );
		ASSERT( pPeriod );
		if( pPeriod->IsInRange( range ) 
			&& pPeriod->IsCounterUrno( urnoCounter ) )
			return pPeriod ;
	};
	return 0 ;
};	// IsInRange

// return wheter specified time range is completely within a period or not, look for counter and agent; use ANY_COUNTER_URNO to ignore counter
CCicAgentPeriod* CCicAgentPeriods::IsInRange( const CTimeRange &range, long urnoCounter, long urnoAgent )
{
	long pos = 0 ;
	while( !AtEnd( pos ) )
	{
		CCicAgentPeriod* pPeriod = (CCicAgentPeriod *)GetAt( pos++ );
		ASSERT( pPeriod );
		if( ( urnoCounter == ANY_COUNTER_URNO		// ignore urno
				|| pPeriod->IsCounterUrno( urnoCounter )
			&& pPeriod->IsAgentUrno( urnoAgent )
			&& pPeriod->IsInRange( range ) )
			)
			return pPeriod ;
	};
	return 0 ;
};	// IsInRange


// return wheter specified timerange overlaps with a period or not
CCicAgentPeriod* CCicAgentPeriods::IsOverlapping( const CTimeRange &range )
{
	long pos = 0 ;
	while( !AtEnd( pos ) )
	{
		CCicAgentPeriod* pPeriod = (CCicAgentPeriod *)GetAt( pos++ );
		ASSERT( pPeriod );
		if( pPeriod->IsOverlapping( range ) )
			return pPeriod ;
	};
	return 0 ;
};	// IsOverlapping

// return wheter specified timerange overlaps with a period or not, look for counter
CCicAgentPeriod* CCicAgentPeriods::IsOverlapping( const CTimeRange &range, long urnoCounter )
{
	long pos = 0 ;
	while( !AtEnd( pos ) )
	{
		CCicAgentPeriod* pPeriod = (CCicAgentPeriod *)GetAt( pos++ );
		ASSERT( pPeriod );
		if( pPeriod->IsOverlapping( range ) 
			&& pPeriod->IsCounterUrno( urnoCounter ) )
			return pPeriod ;
	};
	return 0 ;
};	// IsOverlapping

// return wheter specified timerange overlaps with a period or not, look for counter and agent; use ANY_COUNTER_URNO to ignore counter
CCicAgentPeriod* CCicAgentPeriods::IsOverlapping( const CTimeRange &range, long urnoCounter, long urnoAgent )
{
	long pos = 0 ;
	while( !AtEnd( pos ) )
	{
		CCicAgentPeriod* pPeriod = (CCicAgentPeriod *)GetAt( pos++ );
		ASSERT( pPeriod );
		if( pPeriod->IsOverlapping( range ) 
			&& ( urnoCounter == ANY_COUNTER_URNO		// ignore urno
				|| pPeriod->IsCounterUrno( urnoCounter )
			&& pPeriod->IsAgentUrno( urnoAgent ) )
			)
			return pPeriod ;
	};
	return 0 ;
};	// IsOverlapping


// remove all items and their childs
void CCicAgentPeriods::DeepKill()
{
	try
	{
		for( int ndx = 0 ; ndx < GetSize() ; ndx++ )
		{
			CCicAgentPeriod* pPeriod = (CCicAgentPeriod *)GetAt( ndx );
			ASSERT( pPeriod );
			delete pPeriod ;
			pPeriod = 0 ;
		};
	}
	catch(...)
	{
		int i = 0 ;
	};
	RemoveAll();
};	// DeepKill

// helper function to generate some data for testing
void CCicAgentPeriods::GenerateDummies( unsigned long urnoAgent )
{
	DeepKill();

	CTime now = CTime::GetCurrentTime();
	unsigned char day = now.GetDay();
	unsigned char month = now.GetMonth();
	unsigned short year = now.GetYear();
	CTime minTime( year, month, day -1, 24,00,00 );		// bars start at date
	CTime maxTime( year, month, day +1, 24,00,00 );		// bars start at date
	CTimeSpan rangeTime = maxTime - minTime ;

	int ndxColUrno = ogBCD.GetFieldIndex( "CIC", "URNO" );
	int ndxColName = ogBCD.GetFieldIndex( "CIC", "CNAM" );
	int maxCIC = ogBCD.GetDataCount( "CIC" );
	for( int ndxCIC = 0 ; ndxCIC < maxCIC ; ndxCIC++ )
	{
		unsigned long urnoCIC = 0 ;
		CString sName ;

		RecordSet record ;
		if( ogBCD.GetRecord( "CIC", ndxCIC, record ) )
		{
			CString sItem = record[ ndxColUrno ];
			urnoCIC = atoi( sItem );
			sName = record[ ndxColName ];
		}
		else
		{
			sName.Format( "CIC#%d", ndxCIC );
			ASSERT(0);
		};	// data not found ... maybe some inconsistency

		int maxOCC = 6 * rand() / RAND_MAX ;		// maximal amount of occupations per counter
		for( int ndxOCC = 0 ; ndxOCC < maxOCC ; ndxOCC++ )
		{
			CCicAgentPeriod* pPeriod = new CCicAgentPeriod ;

			unsigned long startMinute  = (rangeTime.GetTotalMinutes() - 120 ) * rand() / RAND_MAX ;		// do not start 2 hours before end of total timerange
			CTimeSpan start( 0,0,startMinute,0 );
			unsigned long rangeMinutes = 60 + 600 * rand() / RAND_MAX ;		// range of timespan
			CTimeSpan span( 0,0,rangeMinutes,0 );

			pPeriod->SetPeriod( minTime + start, minTime + start + span );
			pPeriod->SetAgentUrno( urnoAgent );
			pPeriod->SetCounterUrno( urnoCIC );

			Add( pPeriod );

			long mins = CTimeSpan( pPeriod->EndTime - pPeriod->StartTime ).GetTotalMinutes();
			TRACE( "dummy period for CIC=%5s=%0.9d HAG=%d: %s,%3dmin\n",
				sName, 
				pPeriod->GetCounterUrno(), pPeriod->GetAgentUrno(), 
				pPeriod->StartTime.Format( "%x,%X" ),
				mins
			);
		};	// for each occupation
	};	// for each checkin counter
};	// GenerateDummies

void CCicAgentPeriods::Generate( unsigned long urnoAgent, CTimeRange *pRange )
{
	ASSERT( __CanHandleCheckinCounterRestrictionsForHandlingAgents() );		// 050311 MVy: Handling Agents, who calls me if not configured ???

	DeepKill();		// TODO: later update current periods, most time the same are loaded again

	// 050620 MVy: midnight shifts due to UTC time shift will cause from-to time swap but no new day
	//	for each day in (OCFR,OCTO)
	//		if day is in daily list
	//			make a new period (DAY+TIFR,DAY(+1)+TITO), (+1) day if TITO<TIFR

	CString sViewStart ;
	CString sViewEnd ;

	if( pRange )
	{
		sViewStart = FromTime( pRange->StartTime );
		sViewEnd = FromTime( pRange->EndTime );
	}
	else
	{
		sViewStart = "19700101000000" ;
		sViewEnd = "20380115000000" ;
		TRACE( "%s(%d): CCicAgentPeriods::Generate, Performance warning, default time range (about 70 years) for data genrator is really big.\n", __FILE__, __LINE__ );
	};

	CTimeSpan oneday( 1,0,0,0 );
	CTimeRange rangeView( ToTime( sViewStart ), ToTime( sViewEnd ) );
	ASSERT( rangeView.StartTime < rangeView.EndTime );
	TRACE( "\ncreate periods; view range : %s\n", rangeView.Dump() );

	CCSPtrArray< RecordSet > Records ;
	// get some data, so use hopo 
	ogBCD.GetRecords( "OCC", "OCBY", ToString( urnoAgent ), &Records );
	for( int ndxRecord = 0 ; ndxRecord < Records.GetSize() ; ndxRecord++ )
	{
		RecordSet rec = Records.GetAt( ndxRecord );		// DO NOT DECLARE REFERENCES ! -> manual copy of record structure
		RecordSet* pRecord = &rec ;

		// loop over days the occupation is valid OCFR -> OCTO
		// and create period for each day in daytable
		CString sTitle = GetOCCValue( pRecord, "REMA");
		CString sOccFrom = GetOCCValue( pRecord, "OCFR" );
		CString sOccTo = GetOCCValue( pRecord, "OCTO" );
		// Attention: it can happen, that a occupation starts after the viewable range and has an open end
		//		if so the end should not as default be set to the end of the viewable range, then end is before start !
		//		but this occupation range will stil be AFTER the viewable range and so will not be shown
		CTimeRange rangeOccupation( ToTime( sOccFrom, sViewStart ), ToTime( sOccTo, sViewEnd ) );
		if( rangeOccupation.IsReverse() )
		{
			ASSERT( sOccTo == "" );		// I only know the occurange of this if the occupation end is open, so what had happened
			continue ;
		};

		// 050425 MVy: dates before the lowest and after the highest date of the current view need not to be used
		CTimeRange rangeOccupationPeriod = rangeOccupation.Clip( rangeView );
		if( rangeOccupationPeriod.IsEmpty() )
		{
			continue ;
		};

		CString sWeekDays = GetOCCValue( pRecord, "DAYS" );
		CTime hourFrom = ToTime( GetOCCValue( pRecord, "TIFR" ), "0000" );
		CTime hourTo   = ToTime( GetOCCValue( pRecord, "TITO" ), "2359" );
		CTimeSpan rangeMidnightSkip = 0 ;		// 050620 MVy: midnight shifts due to UTC time shift will cause from-to time swap but no new day
		if( hourFrom > hourTo		// end before it start on current day -> swap times
			&& ( hourFrom != -1 && hourTo != -1 )		// and times valid
			)
		{
			//CTime t = hourFrom ;
			//hourFrom = hourTo ;
			//hourTo = t ;
			rangeMidnightSkip = oneday ;
		};

		CTimeRange rangeValid( 
			ToTime( GetOCCValue( pRecord, "OCFR" ), sViewStart ),
			ToTime( GetOCCValue( pRecord, "OCTO" ), sViewEnd ) );
		ASSERT( !rangeValid.IsReverse() );

		for( CTime today = rangeOccupationPeriod.StartTime ; today <= rangeOccupationPeriod.EndTime + oneday + oneday ; today += oneday )
		{			
			int dow = today.GetDayOfWeek();		// 1=sunday
			ASSERT( INRANGE( 1, dow, 7 ) );		// this would be a real internal error

			static char daymap[] = "7123456" ;		// Microsoft starts on sunday, Ufis on monday
			char cdotw = daymap[ dow -1 ];
			if( sWeekDays.Find( cdotw ) != -1 )
			{
				// 050421 MVy: hourFrom is -1; program crashed on loading a checkin counter view

				CTimeRange rangeOccupationDaily( today, today + rangeMidnightSkip );		// 050620 MVy: midnight shifts due to UTC time shift will cause from-to time swap but no new day

				if( hourFrom != -1 ) OverwriteTime( rangeOccupationDaily.StartTime, hourFrom );
					else OverwriteTime( rangeOccupationDaily.StartTime, 0,0,0 );		// this should not be necessary, because 0,0,0 is default ;

				if( hourTo != -1 ) OverwriteTime( rangeOccupationDaily.EndTime, hourTo );
					else OverwriteTime( rangeOccupationDaily.EndTime, 23,59,59 );

				if( bgGatPosLocal )		// 050323 MVy: database contains only UTC times, but we need local time
				{
					ogBasicData.UtcToLocal( rangeOccupationDaily.StartTime );
					ogBasicData.UtcToLocal( rangeOccupationDaily.EndTime );
				};

				CTimeRange rangeClipped = rangeOccupationDaily.Clip( rangeValid );
				//TRACE( "   ----------------------------\n" );
				//TRACE( "   occupation: %s\n", rangeOccupationDaily.Dump() );
				//TRACE( "   valid     : %s\n", rangeValid.Dump() );
				//TRACE( "   clipped   : %s\n", rangeOccupationDaily.Dump() );
				//TRACE( "   ----------------------------\n" );
				if( !rangeClipped.IsEmpty() )
				{
					CCicAgentPeriod* pPeriod = new CCicAgentPeriod( rangeClipped );
					pPeriod->SetAgentUrno( ToLong( GetOCCValue( pRecord, "OCBY") ) );		// occupied by = HAG.URNO
					pPeriod->SetCounterUrno( ToLong( GetOCCValue( pRecord, "ALID") ) );		// allocation identifier = CIC.URNO
					pPeriod->SetTitle( sTitle );		// name shown in bkbar = CIC.REMA
					//pPeriod->SetDays( GetOCCValue( pRecord, "DAYS") );
					// TODO: remove storing days in agent�s period
					Add( pPeriod );

					TRACE( "create period: %s\n", pPeriod->Dump() );
				}
				else
				{
					TRACE( "period not created, not in valid range: %s\n", rangeOccupation.Dump() );
				};
			};	// valid day
		};	// for each valid day
	};	// for each occupation

};	// Generate
