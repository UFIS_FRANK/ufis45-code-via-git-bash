// ReportFlightDelayViewer.cpp : implementation file
// 
// Modification History: 


#include <stdafx.h>
#include <ReportFlightDelayViewer.h>
#include <CcsGlobl.h>
#include <resource.h>
#include <resrc1.h>
#include <Utils.h>

#include <math.h>
 
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif 


int ReportFlightDelayViewer::imTableColCharWidths[REPORTFLIGHTDELAY_COLCOUNT]={9, 9, 3, 8, 5, 5, 9, 3, 4, 3, 4, 6,3,3,3,8,3};

/////////////////////////////////////////////////////////////////////////////
// ReportFlightDelayViewer
//

ReportFlightDelayViewer::ReportFlightDelayViewer() :
	imOrientation(PRINT_PORTRAET),
	romTableHeaderFont(ogCourier_Bold_10), romTableLinesFont(ogCourier_Regular_9),
	fmTableHeaderFontWidth(10), fmTableLinesFontWidth(7.5)
{


	dgCCSPrintFactor = 3;
	int i=0;
	// Table header strings
	omTableHeadlines[i++]=GetString(IDS_STRING296);		// Flight
 	omTableHeadlines[i++]=GetString(IDS_STRING337);		// Type 
 	omTableHeadlines[i++]=GetString(IDS_STRING1446);	// ORG/DES
	omTableHeadlines[i++]=GetString(IDS_STRING332);		// Date
 	omTableHeadlines[i++]=GetString(IDS_STRING1881);	// STA/STD  
 	omTableHeadlines[i++]=GetString(IDS_STRING1972);	// ONBL/OFBL  
 	omTableHeadlines[i++]=GetString(IDS_STRING311);		// A/C 
 	omTableHeadlines[i++]=GetString(IDS_STRING1910);	// DEL1  
 	omTableHeadlines[i++]=GetString(IDS_STRING1911);	// MIN1  
 	omTableHeadlines[i++]=GetString(IDS_STRING1912);	// DEL2 
 	omTableHeadlines[i++]=GetString(IDS_STRING1913);	// MIN2
 	omTableHeadlines[i++]=GetString(IDS_STRING1969);	// DIFF  
    if(bgReports)
	{
	omTableHeadlines[i++]=GetString(IDS_STRING2785);    //Gate1 
	omTableHeadlines[i++]=GetString(IDS_STRING2786);    //Postion
    omTableHeadlines[i++]=GetString(IDS_STRING2787);    //Nature  
	omTableHeadlines[i++]=GetString(IDS_STRING2788);    //Registration 
   	omTableHeadlines[i++]=GetString(IDS_STRING2790);    //Gate 2
	}

	// calculate table column widths for displaying

	if(bgReports)
	{
	for (i=0; i < REPORTFLIGHTDELAY_COLCOUNT; i++)
		{
 		imTableColWidths[i] = (int) max(imTableColCharWidths[i]*fmTableLinesFontWidth, 
							        omTableHeadlines[i].GetLength()*fmTableHeaderFontWidth);

		}
	
	}
	if(!bgReports)
	{
	for (i=0; i < 12; i++)
		{
 		imTableColWidths[i] = (int) max(imTableColCharWidths[i]*fmTableLinesFontWidth, 
							        omTableHeadlines[i].GetLength()*fmTableHeaderFontWidth);

		}
	
	}

	pomTable = NULL;

	}
	
ReportFlightDelayViewer::~ReportFlightDelayViewer()
{
     DeleteAll();
}


// Connects the viewer with a table
void ReportFlightDelayViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}

// delete intern table lines
void ReportFlightDelayViewer::DeleteAll(void)
{
    omLines.DeleteAll();
}




// Load the intern line data from the given data and displays the table
void ReportFlightDelayViewer::ChangeViewTo(const CCSPtrArray<ROTATIONDLGFLIGHTDATA> &ropData, char *popDateStr, char *popAirline, char *popDelayCode, int ipDelayTime)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	pomDateStr = popDateStr;
	pomAirline = popAirline;
	pomDelayCode = popDelayCode;
	ipmDelayTime = ipDelayTime;
	imNoArr = 0;
	imNoDep = 0;
	// Rebuild intern data    
    MakeLines(ropData);
	// Rebuild table
	UpdateDisplay();
  
 	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}


// Transfer the data of the database to the intern data structures
void ReportFlightDelayViewer::MakeLines(const CCSPtrArray<ROTATIONDLGFLIGHTDATA> &ropData)
{ 
	// Delete the intern lines
	DeleteAll();

 	// Create the intern lines
	REPORTFLIGHTDELAY_LINEDATA rlLineData;
 	
	for(int j = ropData.GetSize() -1 ; j >= 0; j --)
	{
		if (strlen(ropData[j].Dcd1) != 0 || strlen(ropData[j].Dcd2) != 0 ||
			(ropData[j].Stoa != TIMENULL && ropData[j].Onbl != TIMENULL && ropData[j].Stoa != ropData[j].Onbl) ||
			(ropData[j].Stod != TIMENULL && ropData[j].Ofbl != TIMENULL && ropData[j].Stod != ropData[j].Ofbl))
		{

			CTimeSpan olDelay;
			if (IsArrivalFlight(ropData[j].Org3, ropData[j].Des3, ropData[j].Ftyp[0]))
				olDelay = ropData[j].Onbl - ropData[j].Stoa;
			else
				olDelay = ropData[j].Ofbl - ropData[j].Stod;

			long llDelay = olDelay.GetTotalMinutes();
			if (llDelay >= ipmDelayTime)
			{
				if (MakeLineData(rlLineData, ropData[j]))
				{
					CreateLine(rlLineData);
				}
			}
		}
	}

//generate the headerinformation
	CString olTimeSet = GetString(IDS_STRING1920); //UTC 
	if(bgReportLocal)
		olTimeSet = GetString(IDS_STRING1921); //LOCAL

	char pclHeader[256];
	
	sprintf(pclHeader,GetString(IDS_HEADER_DELAY_LONG), pomDateStr, imNoArr+imNoDep, imNoArr, imNoDep, pomAirline, pomDelayCode, ipmDelayTime, olTimeSet);
	omTableName = pclHeader;
	
	sprintf(pclHeader,GetString(IDS_HEADER_DELAY_SHORT), pomDateStr, imNoArr+imNoDep, olTimeSet);
	if(bgReports)
	sprintf(pclHeader,"%s Delay Report: %s (Flights:%d) - %s -",ogPrefixReports, pomDateStr, imNoArr+imNoDep, olTimeSet);
	omPrintName = pclHeader;

}



 
// Copy the data from the db-record to the table-record
bool ReportFlightDelayViewer::MakeLineData(REPORTFLIGHTDELAY_LINEDATA &rrpLineData, const ROTATIONDLGFLIGHTDATA &rrpFlight)
{
	rrpLineData.Urno = rrpFlight.Urno;
	rrpLineData.Flno = rrpFlight.Flno;
	rrpLineData.Act3 = rrpFlight.Act3;
	rrpLineData.Dcd1 = rrpFlight.Dcd1;
	rrpLineData.Dtd1 = rrpFlight.Dtd1;
	rrpLineData.Dcd2 = rrpFlight.Dcd2;
	rrpLineData.Dtd2 = rrpFlight.Dtd2;
   	rrpLineData.Act5 = rrpFlight.Act5;
	
	if(bgReports)
	{
	rrpLineData.Ttyp = rrpFlight.Ttyp;
	rrpLineData.Regn = rrpFlight.Regn; 
	}
	
	if (IsArrivalFlight(rrpFlight.Org3, rrpFlight.Des3, rrpFlight.Ftyp[0]))
	{
		imNoArr++;
		rrpLineData.Type = "Arrival";
		rrpLineData.OrgDes = rrpFlight.Org3;
		rrpLineData.StoaStod = rrpFlight.Stoa;
		rrpLineData.OnblOfbl = rrpFlight.Onbl;
        rrpLineData.Gta1Gtd1 = rrpFlight.Gta1;
		if(bgReports)
		{
		rrpLineData.PstaPstd = rrpFlight.Psta;
		rrpLineData.Gta2Gtd2 = rrpFlight.Gta2;	
		}	
	}	
	else
	{
		imNoDep++;
		rrpLineData.Type = "Departure";
		rrpLineData.OrgDes = rrpFlight.Des3;
		rrpLineData.StoaStod = rrpFlight.Stod;
		rrpLineData.OnblOfbl = rrpFlight.Ofbl;
		rrpLineData.Gta1Gtd1 = rrpFlight.Gtd1;
		if(bgReports)
		{
		rrpLineData.PstaPstd = rrpFlight.Pstd;
        rrpLineData.Gta2Gtd2 = rrpFlight.Gtd2;
		}
	}
	

	// Local times choosen?
	if(bgReportLocal)
	{
		ogBasicData.UtcToLocal(rrpLineData.StoaStod);
		ogBasicData.UtcToLocal(rrpLineData.OnblOfbl);
	}


	return true;		
}


// Create a intern table data record
int ReportFlightDelayViewer::CreateLine(const REPORTFLIGHTDELAY_LINEDATA &rrpLine)
{
    for (int ilLineno = omLines.GetSize(); ilLineno > 0; ilLineno--)
	{
		if (CompareLines(rrpLine, omLines[ilLineno-1]) >= 0)
		{
            break;  // should be inserted after Lines[ilLineno-1]
		}
	}
	// Insert new line
    omLines.NewAt(ilLineno, rrpLine);
	return ilLineno;
}


// Rebuild the table from the intern data
void ReportFlightDelayViewer::UpdateDisplay()
{
	// Clear the table
	pomTable->ResetContent();
	
	DrawTableHeader();
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(omLines[ilLc], olColList);
		// add table line
		pomTable->AddTextLine(olColList, (void*)(&omLines[ilLc]));
			
	}

    pomTable->DisplayTable();
}

 


void ReportFlightDelayViewer::DrawTableHeader()
{
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Font = &romTableHeaderFont;

	// for all rows
 if(bgReports)
 {
	for (int i=0; i < REPORTFLIGHTDELAY_COLCOUNT; i++)
	{
		rlHeader.Alignment = COLALIGN_CENTER;
		rlHeader.Length = imTableColWidths[i]; 
		rlHeader.Text = omTableHeadlines[i]; 
		omHeaderDataArray.New(rlHeader);
	}
 }
 if(!bgReports)
 {
	for (int i=0; i < 12; i++)
	{
		rlHeader.Alignment = COLALIGN_CENTER;
		rlHeader.Length = imTableColWidths[i]; 
		rlHeader.Text = omTableHeadlines[i]; 
		omHeaderDataArray.New(rlHeader);
	}
	
 }

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}




// Fills one row of the table
bool ReportFlightDelayViewer::MakeColList(const REPORTFLIGHTDELAY_LINEDATA &rrpLine, CCSPtrArray<TABLE_COLUMN> &ropColList) const
{
	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &romTableLinesFont;
	rlColumnData.Alignment = COLALIGN_LEFT;

	rlColumnData.Text = rrpLine.Flno;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
 
 	rlColumnData.Text = rrpLine.Type;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

 	rlColumnData.Text = rrpLine.OrgDes;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
	
	rlColumnData.Text = rrpLine.StoaStod.Format("%d.%m.%y");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

 	rlColumnData.Text = rrpLine.StoaStod.Format("%H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
 
 	rlColumnData.Text = rrpLine.OnblOfbl.Format("%H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
	
 	if(!bgReports)
	{
	rlColumnData.Text = rrpLine.Act3;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
	}
	else
	{
	rlColumnData.Text = rrpLine.Act3+"/"+rrpLine.Act5;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
	}
	
	//prf 8383
	rlColumnData.Text = rrpLine.Dcd1;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = rrpLine.Dtd1;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = rrpLine.Dcd2;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = rrpLine.Dtd2;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
	
	
	CTimeSpan olDiff = rrpLine.OnblOfbl - rrpLine.StoaStod;
	if (rrpLine.OnblOfbl != TIMENULL)
	{
		if (olDiff < CTimeSpan(0,0,0,0))
			rlColumnData.Text.Format("-%02d:%02d", abs(olDiff.GetTotalHours()), abs(olDiff.GetMinutes()));
		else
			rlColumnData.Text.Format("%02d:%02d", abs(olDiff.GetTotalHours()), abs(olDiff.GetMinutes()));
//			rlColumnData.Text = olDiff.Format(" %H:%M");
	}
	else
		rlColumnData.Text = "";
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

 if(bgReports)
 {

	rlColumnData.Text = rrpLine.Gta1Gtd1;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

 	rlColumnData.Text = rrpLine.Ttyp;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
 	
	rlColumnData.Text = rrpLine.PstaPstd;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
	
 	rlColumnData.Text = rrpLine.Regn;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
    
	rlColumnData.Text = rrpLine.Gta2Gtd2;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
 }
	return true;
}


// Compares two lines of the table
int ReportFlightDelayViewer::CompareLines(const REPORTFLIGHTDELAY_LINEDATA &rrpLine1, const REPORTFLIGHTDELAY_LINEDATA &rrpLine2) const 
{
	// Sorted by Dep/Arr, StoaStod, Flno

	if (rrpLine1.Type == "Departure" && rrpLine2.Type == "Arrival") return -1;	
	if (rrpLine1.Type == "Arrival" && rrpLine2.Type == "Departure") return 1;	

	if (rrpLine1.StoaStod < rrpLine2.StoaStod) return -1;
	if (rrpLine1.StoaStod > rrpLine2.StoaStod) return 1;

	if (rrpLine1.Flno < rrpLine2.Flno) return -1;
	if (rrpLine1.Flno > rrpLine2.Flno) return 1;

	return 0;	
}


int ReportFlightDelayViewer::GetFlightCount(void) const
{
	return pomTable->GetLinesCount();
}



//////////////////////////////////////////////////////////////////
//// Printing routines
 
// Print table to paper
void ReportFlightDelayViewer::PrintTableView(void) {

	CCSPrint olPrint(NULL, imOrientation, 45);

	// Set printing fonts
	pomPrintHeaderFont = &olPrint.ogCourierNew_Bold_8;
	pomPrintLinesFont = &olPrint.ogCourierNew_Regular_8;
	fmPrintHeaderFontWidth = 7;
	fmPrintLinesFontWidth = 7;



	// calculate table column widths for printing
 if(bgReports)
 {
	for (int i=0; i < REPORTFLIGHTDELAY_COLCOUNT; i++)
	{
  		imPrintColWidths[i] = (int) max(imTableColCharWidths[i]*fmPrintLinesFontWidth, 
								        omTableHeadlines[i].GetLength()*fmPrintHeaderFontWidth);
	}
 }
 if(!bgReports)
 {
	for (int i=0; i < 12; i++)
	{
  		imPrintColWidths[i] = (int) max(imTableColCharWidths[i]*fmPrintLinesFontWidth, 
								        omTableHeadlines[i].GetLength()*fmPrintHeaderFontWidth);
	}
 }

	CString olFooter1,olFooter2;
	char pclFooter[256];

	sprintf(pclFooter, omTableName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
	omFooterName = pclFooter;
    CString olTablename = GetString(REPORTS_RB_DelayReport);

	// Set left footer to: "printed at: <date>"
   olFooter1.Format("%s -   %s", omFooterName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );

	if(bgReports)
	olFooter1.Format("%s %s-   %s-   %s",ogPrefixReports, omFooterName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pcgUser );
// 	olFooter1.Format("%s %s", GetString(IDS_STRING1481),
//		(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));

  	if (olPrint.InitializePrinter(imOrientation) == TRUE)
	{ 
		olPrint.imMaxLines = 57;	// Def. imMaxLines: 57 Portrait / 38 Landscape
		// Calculate number of pages
		const double dlPages = ceil((double)pomTable->GetLinesCount() / (double)(olPrint.imMaxLines - 1));
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		// At first a pagebreak
		olPrint.imLineNo = olPrint.imMaxLines + 1;	
		DOCINFO	rlDocInfo;
		memset(&rlDocInfo, 0, sizeof(DOCINFO));
		rlDocInfo.cbSize = sizeof( DOCINFO );
		rlDocInfo.lpszDocName = olTablename ;	
		olPrint.omCdc.StartDoc( &rlDocInfo );
		olPrint.imPageNo = 0;
		// Print all table lines
		for (int ilLc = 0; ilLc < pomTable->GetLinesCount(); ilLc++)
		{
			// Page break
 			if(olPrint.imLineNo >= olPrint.imMaxLines)
			{
				if(olPrint.imPageNo > 0)
				{
					// Set right footer to: "Page: %d"
					olFooter2.Format(GetString(IDS_STRING1199),olPrint.imPageNo, dlPages);
					// print footer
					olPrint.PrintUIFFooter(olFooter1,"",olFooter2);
					olPrint.omCdc.EndPage();
				}
				// print header
				PrintTableHeader(olPrint);
			}				
			// print line
			PrintTableLine(olPrint, ilLc);
		}
		// print footer
		olFooter2.Format(GetString(IDS_STRING1199),olPrint.imPageNo, dlPages);
		olPrint.PrintUIFFooter(olFooter1,"",olFooter2);
		olPrint.omCdc.EndPage();
		olPrint.omCdc.EndDoc();
	}  // if (olPrint.InitializePrin...
 	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}




bool ReportFlightDelayViewer::PrintTableHeader(CCSPrint &ropPrint)
{
	ropPrint.omCdc.StartPage();
	ropPrint.imPageNo++;
	ropPrint.imLineNo = 0;
	//double dgCCSPrintFactor = 2.7 ;
//	CString olHeadline;

	// Headline
// 	olHeadline.Format(GetString(IDS_STRING1971), pomDateStr, pomTable->GetLinesCount());
 
	// print page headline
	ropPrint.imLeftOffset = 150;
//	ropPrint.PrintUIFHeader(CString(), olHeadline, ropPrint.imFirstLine-10);
	ropPrint.PrintUIFHeader("",omPrintName,ropPrint.imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_NOFRAME;
	rlElement.pFont       = pomPrintHeaderFont;

	// Create table headline
 if(bgReports)
 {
	for (int ilCc = 0; ilCc < REPORTFLIGHTDELAY_COLCOUNT; ilCc++)
	{
		rlElement.Length = (int)(imPrintColWidths[ilCc]*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength) 
			rlElement.Length+=igCCSPrintMoreLength; 

		rlElement.Text = omTableHeadlines[ilCc]; 
		
		rlPrintLine.NewAt(rlPrintLine.GetSize(), rlElement);
 	}
 }
 if(!bgReports)
 {
	
	for (int ilCc = 0; ilCc < 12; ilCc++)
	{
		rlElement.Length = (int)(imPrintColWidths[ilCc]*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength) 
			rlElement.Length+=igCCSPrintMoreLength; 

		rlElement.Text = omTableHeadlines[ilCc]; 
		
		rlPrintLine.NewAt(rlPrintLine.GetSize(), rlElement);
 	}
 }
	// Print table headline
 	ropPrint.PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	return true;
}


 

bool ReportFlightDelayViewer::PrintTableLine(CCSPrint &ropPrint, int ipLineNo) {
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	
	//double dgCCSPrintFactor = 2.7 ;

	PRINTELEDATA rlElement;
	rlElement.pFont = pomPrintLinesFont;
	rlElement.FrameTop = PRINT_FRAMETHIN;
 	rlElement.FrameBottom = PRINT_FRAMETHIN;
	rlElement.FrameLeft  = PRINT_FRAMETHIN;
	rlElement.FrameRight = PRINT_FRAMETHIN;

	CString olCellValue;
	// create table line
 if(bgReports)
 {
	for (int ilCc = 0; ilCc < REPORTFLIGHTDELAY_COLCOUNT; ilCc++)
	{
		rlElement.Alignment  = PRINT_LEFT;
 		rlElement.Length = (int)(imPrintColWidths[ilCc]*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength) 
			rlElement.Length+=igCCSPrintMoreLength; 
		// Get printing text from table	
		pomTable->GetTextFieldValue(ipLineNo, ilCc, olCellValue);
		rlElement.Text = olCellValue; 
		
 		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}	
	
 }
 if(!bgReports)
 {
  for (int ilCc = 0; ilCc < 12; ilCc++)
	{
		rlElement.Alignment  = PRINT_LEFT;
 		rlElement.Length = (int)(imPrintColWidths[ilCc]*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength) 
			rlElement.Length+=igCCSPrintMoreLength; 
		// Get printing text from table	
		pomTable->GetTextFieldValue(ipLineNo, ilCc, olCellValue);
		rlElement.Text = olCellValue; 
		
 		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
 }	
 // print table line
	ropPrint.PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll(); 
	
	return true;
}


 
// Print table to file
bool ReportFlightDelayViewer::PrintPlanToFile(char *pclTrenner)
{
	ofstream of;
	CString olFileName = omTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);

	// Header
	for (int ilCc = 0; ilCc < REPORTFLIGHTDELAY_COLCOUNT; ilCc++)
	{
		of  << omTableHeadlines[ilCc];
		if (ilCc < REPORTFLIGHTDELAY_COLCOUNT-1)
		{
			of << pclTrenner;
		}
	}
	of << endl;

	CString olCellValue;
	 // Lines
	for (int ilLc = 0; ilLc < pomTable->GetLinesCount(); ilLc++)
	{
		for (int ilCc = 0; ilCc < REPORTFLIGHTDELAY_COLCOUNT; ilCc++)
		{
			// get text from table
			pomTable->GetTextFieldValue(ilLc, ilCc, olCellValue);
			of  << olCellValue;
			if (ilCc < REPORTFLIGHTDELAY_COLCOUNT-1)
			{
				of << pclTrenner;
			}
		}
		of << endl;
	}		
	
	of.close();

	return true;
}






 