// WoResTableViewer.cpp : implementation file
// 
// Modification History: 
//	061100	rkr		Table um die Spalten ETA(i), ETD(i), NAture f�r arrival und departure erweitert


#include <stdafx.h>
#include <WoResTableViewer.h>
#include <WoResTableDlg.h>
#include <CcsGlobl.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <BasicData.h>
#include <Utils.h>
#include <SpotAllocation.h>
#include <resrc1.h>




#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif




// Local function prototype
static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// WoResTableViewer
//

int WoResTableViewer::imTableColCharWidths[WORESTABLE_COLCOUNT]={
	9, 5, 5, 2, 3, 5, 5, 
	5,	// 050309 MVy: Gate1 added for arrival
	5,	// 050309 MVy: Gate2 added for arrival
	5, 5, 3, 7, 9, 5, 5, 2, 3, 5, 5, 5, 
	5,	// 050309 MVy: Gate2 added for departure
	5,
	6
};
 

WoResTableViewer::WoResTableViewer(WoResTableDlg *popParentDlg):
 	romTableHeaderFont(ogCourier_Bold_10), romTableLinesFont(ogCourier_Regular_10),
	fmTableHeaderFontWidth(9), fmTableLinesFontWidth(9)
{
 	// Table header strings
	int i=0;
	omTableHeadlines[i++]=GetString(IDS_STRING296);	// Flight
	omTableHeadlines[i++]=GetString(IDS_STRING323);	// STA
	omTableHeadlines[i++]=GetString(IDS_STRING302);	// ETA(i)
	omTableHeadlines[i++]=GetString(IDS_STRING1131);// D
	omTableHeadlines[i++]=GetString(IDS_STRING298);	// ORG
	omTableHeadlines[i++]=GetString(IDS_STRING1903);// NA(ture)
	omTableHeadlines[i++]=GetString(IDS_STRING308);	// POS
	omTableHeadlines[i++]=GetString(IDS_STRING335); // Gate1		// 050309 MVy: Gate1 added for arrival
	omTableHeadlines[i++]=GetString(IDS_STRING336); // Gate2		// 050309 MVy: Gate2 added for arrival
	omTableHeadlines[i++]=GetString(IDS_STRING1939);// Belt1
	omTableHeadlines[i++]=GetString(IDS_STRING1940);// Belt2
	omTableHeadlines[i++]=GetString(IDS_STRING311);	// A/C
	omTableHeadlines[i++]=GetString(IDS_STRING310);	// Reg
	omTableHeadlines[i++]=GetString(IDS_STRING296);	// Flight
	omTableHeadlines[i++]=GetString(IDS_STRING316);	// STD
	omTableHeadlines[i++]=GetString(IDS_STRING317); // ETD(i)
	omTableHeadlines[i++]=GetString(IDS_STRING1131);// D
	omTableHeadlines[i++]=GetString(IDS_STRING315);	// DES
	omTableHeadlines[i++]=GetString(IDS_STRING1903);// NA(ture)
	omTableHeadlines[i++]=GetString(IDS_STRING308);	// POS
	omTableHeadlines[i++]=GetString(IDS_STRING335); // Gate1
	omTableHeadlines[i++]=GetString(IDS_STRING336); // Gate2		// 050309 MVy: Gate2 added for departure
	omTableHeadlines[i++]=GetString(IDS_STRING1941);// Lounge
	omTableHeadlines[i++]=GetString(IDS_STRING1824);// Towing
   	// calculate table column widths
	for (i=0; i < WORESTABLE_COLCOUNT; i++)
	{
 		imTableColWidths[i] = (int) max(imTableColCharWidths[i]*fmTableLinesFontWidth, 
								        omTableHeadlines[i].GetLength()*fmTableHeaderFontWidth);
	}

	pomParentDlg = popParentDlg;
    pomTable = NULL;
	
}


void WoResTableViewer::UnRegister()
{
	ogDdx.UnRegister(this, NOTUSED);
 
}


WoResTableViewer::~WoResTableViewer()
{
	UnRegister();
    DeleteAll();
}


void WoResTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}



void WoResTableViewer::ChangeViewTo(FipsModules ipMode, bool bpTowing)
{
	if (!pomTable) return;

	bmTowing = bpTowing;

	UnRegister();

	ogDdx.Register(this, D_FLIGHT_CHANGE, CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);
	//ogDdx.Register(this, D_CHANGE_WORES, CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);
	ogDdx.Register(this, D_FLIGHT_DELETE, CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);
	ogDdx.Register(this, D_FLIGHT_INSERT, CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);

	imMode = ipMode;

	// rebuild table
    pomTable->ResetContent();
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}


// how many flights have not the current resource assigned?
int WoResTableViewer::GetCount(FipsModules ipMode) const
{
	int ilCount = 0;
/*
	switch (ipMode)
	{
	case POSDIA:
		ilCount = ogPosDiaFlightData.omPosWoResMap.GetCount();
	break;
	case GATDIA:
		ilCount = ogPosDiaFlightData.omGatWoResMap.GetCount();
	break;
	case BLTDIA:
		ilCount = ogPosDiaFlightData.omBltWoResMap.GetCount();
	break;
	case WRODIA:
		ilCount = ogPosDiaFlightData.omWroWoResMap.GetCount();
	break;
	}

*/

	DIAFLIGHTDATA *prlFlight;
	int ilFlightCount = ogPosDiaFlightData.omData.GetSize();

	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &ogPosDiaFlightData.omData[ilLc];

		if(IsPassFilter(*prlFlight, prlFlight->Adid[0], ipMode))
 			ilCount++;
	}

	return ilCount;
}





// Is the current resource not assigned to the flight?
bool WoResTableViewer::IsPassFilter(const DIAFLIGHTDATA &rrpFlight, char cpFPart, FipsModules ipMode) const
{
	// NO TOWINGS
	if ((rrpFlight.Ftyp[0] == 'G' || rrpFlight.Ftyp[0] == 'T') && ipMode != POSDIA)
			return false;
/*
 		if(cpFPart == 'A')
		{
		if((rrpFlight.Tifa < ogAllocPeriodFrom) || (rrpFlight.Tifa > ogAllocPeriodTo))
			return false;
		}
		if (cpFPart == 'D')
		{
		if((rrpFlight.Tifd < ogAllocPeriodFrom) || (rrpFlight.Tifd > ogAllocPeriodTo))
			return false;
	}
*/

	switch (ipMode)
	{
	case POSDIA:
		if(bgAutoAllocGateWithoutGlobalNature && FindInStrArray(ogSpotAllocation.omPstNatNoAllocList, rrpFlight.Ttyp))
//		if( ogSpotAllocation.omPstNatNoAllocList.Find(rrpFlight.Ttyp) >= 0)
			return false;


		if(!bmTowing)
		{
			if ((rrpFlight.Ftyp[0] == 'G' || rrpFlight.Ftyp[0] == 'T') )
				return false;
		}

 		if(cpFPart == 'A' || cpFPart == 'B')
		{
			if(rrpFlight.Psta[0] == '\0')
				return true;
		}
		if (cpFPart == 'D' || cpFPart == 'B')
		{
			if(rrpFlight.Pstd[0] == '\0')
				return true;
		}
	break;
	case GATDIA:
		/*
 		if(IsArrival(rrpFlight.Org3, rrpFlight.Des3))
		{
			if(CString(rrpFlight.Gta1).IsEmpty())
				return true;

			if(CString(rrpFlight.Flti) == "M")
			{
				if(CString(rrpFlight.Gta2).IsEmpty())
					return true;
			}

		}
		*/
		//6389
		if(bgAutoAllocGateWithoutGlobalNature && FindInStrArray(ogSpotAllocation.omGatNatNoAllocList, rrpFlight.Ttyp))
//		if( ogSpotAllocation.omGatNatNoAllocList.Find(rrpFlight.Ttyp) >= 0)
			return false;

		if (bgNewFwRGates)
		{
			if (cpFPart == 'A')
			{
				if(rrpFlight.Flti[0] == 'M')
				{
					if(rrpFlight.Gta1[0] == '\0' && rrpFlight.Gta2[0] == '\0')
						return true;
				}
				else
				{
					if(rrpFlight.Gta1[0] == '\0')
						return true;
				}
			}

			if (cpFPart == 'D')
			{
				if(rrpFlight.Flti[0] == 'M')
				{
					if(rrpFlight.Gtd1[0] == '\0' && rrpFlight.Gtd2[0] == '\0')
						return true;
				}
				else
				{
					if(rrpFlight.Gtd1[0] == '\0')
						return true;
				}
			}
		}
		else
		{
			if (cpFPart == 'D')
			{
				if(rrpFlight.Flti[0] == 'M')
				{
					if(rrpFlight.Gtd1[0] == '\0' && rrpFlight.Gtd2[0] == '\0')
						return true;
				}
				else
				{
					if(rrpFlight.Gtd1[0] == '\0')
							return true;
				}
			}
		}
	break;
	case BLTDIA:
		if(bgAutoAllocGateWithoutGlobalNature && FindInStrArray(ogSpotAllocation.omBltNatNoAllocList, rrpFlight.Ttyp))
//		if( ogSpotAllocation.omBltNatNoAllocList.Find(rrpFlight.Ttyp) >= 0)
			return false;

 		if(cpFPart == 'A')
		{
			if(rrpFlight.Blt1[0] == '\0')
				return true;

			if(rrpFlight.Flti[0] == 'M')
			{
				if(rrpFlight.Blt2[0] == '\0')
					return true;
			}

		}
	break;
	case WRODIA:
		if(bgAutoAllocGateWithoutGlobalNature && FindInStrArray(ogSpotAllocation.omWroNatNoAllocList, rrpFlight.Ttyp))
//		if( ogSpotAllocation.omWroNatNoAllocList.Find(rrpFlight.Ttyp) >= 0)
			return false;

 		if (cpFPart == 'D')
		{
			if(rrpFlight.Wro1[0] == '\0')
				return true;

		}
	break;
	}


    return false;
}



/////////////////////////////////////////////////////////////////////////////
// WoResTableViewer -- code specific to this class

void WoResTableViewer::MakeLines(CCSPtrArray<DIAFLIGHTDATA> *popData)
{

	ogPosDiaFlightData.omData.Sort(ogPosDiaFlightData.CompareRotationFlight);


    CCSPtrArray<DIAFLIGHTDATA> *polData;


	if(popData == NULL)
		polData = &ogPosDiaFlightData.omData;
	else
		polData = popData;


	int ilFlightCount = polData->GetSize();



	DIAFLIGHTDATA *prlFlight;
	int ilTmp;
	int ilTmp2;
	// Scan all loaded flights
	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &((*polData)[ilLc]);

		//prlFlight = &(ogPosDiaFlightData.omData[ilLc]);

		if (IsCircular(prlFlight->Org3, prlFlight->Des3))
		{
			if(IsPassFilter(*prlFlight, 'A', imMode))
			{
				if (!FindLine(prlFlight->Urno, 'A', ilTmp))
				{
					// make arrival part of circular flight
					ilTmp = MakeLine(prlFlight, ogPosDiaFlightData.GetDeparture(prlFlight, !bmTowing));
					if(popData != NULL) InsertDisplayLine( ilTmp);
				}
			}
			if(IsPassFilter(*prlFlight, 'D', imMode))
			{
				if (!FindLine(prlFlight->Urno, 'D', ilTmp))
				{
					// make arrival part of circular flight
					ilTmp2 = MakeLine(ogPosDiaFlightData.GetArrival(prlFlight, !bmTowing), prlFlight);
					if(popData != NULL) InsertDisplayLine( ilTmp2);
				}
			}
		}
		else if (IsArrival(prlFlight->Org3, prlFlight->Des3))
		{
			if(IsPassFilter(*prlFlight, 'A', imMode))
			{
				if (!FindLine(prlFlight->Urno, 'A', ilTmp))
				{
					// make arrival part of flight
					ilTmp = MakeLine(prlFlight, ogPosDiaFlightData.GetDeparture(prlFlight, !bmTowing));
					if(popData != NULL) InsertDisplayLine( ilTmp);
				}
			}
		}
		else if (IsDeparture(prlFlight->Org3, prlFlight->Des3))
		{
			if(IsPassFilter(*prlFlight, 'D', imMode))
			{
				if (!FindLine(prlFlight->Urno, 'D', ilTmp))
				{
					// make arrival part of flight
					ilTmp = MakeLine(ogPosDiaFlightData.GetArrival(prlFlight, !bmTowing), prlFlight);
					if(popData != NULL) InsertDisplayLine( ilTmp);
				}
			}
		}
	}

}



int WoResTableViewer::MakeLine(const DIAFLIGHTDATA *prpAFlight, const DIAFLIGHTDATA *prpDFlight)
{
    WORESTABLE_LINEDATA rlLine;
	
 	MakeLineData(prpAFlight, prpDFlight, rlLine);
	return CreateLine(rlLine);
 
}

void WoResTableViewer::MakeLineData(const DIAFLIGHTDATA *prpAFlight, const DIAFLIGHTDATA *prpDFlight, WORESTABLE_LINEDATA &rrpLine) const
{
	bool olFlag = false;
	bool olFlag1 = false;
	// for arrival flights
	if (prpAFlight)
	{
		bool blGlobalRes = false;
		bool blHasAlloc  = true;

		switch (imMode)
		{
		case GATDIA:
//			if( !CString(prpAFlight->Ttyp).IsEmpty() )
			{
				if(bgAutoAllocGateWithoutGlobalNature && FindInStrArray(ogSpotAllocation.omGatNatNoAllocList,prpAFlight->Ttyp))
//				if( ogSpotAllocation.omGatNatNoAllocList.Find(prpAFlight->Ttyp) >= 0)
				{
					blGlobalRes = true;
				}
			}
			if ( CString(prpAFlight->Gta1).IsEmpty() && CString(prpAFlight->Gta2).IsEmpty())
				blHasAlloc  = false;

		break;
		case POSDIA:
//			if( !CString(prpAFlight->Ttyp).IsEmpty() )
			{
				if(bgAutoAllocGateWithoutGlobalNature && FindInStrArray(ogSpotAllocation.omPstNatNoAllocList,prpAFlight->Ttyp))
//				if( ogSpotAllocation.omPstNatNoAllocList.Find(prpAFlight->Ttyp) >= 0)
				{
					blGlobalRes = true;
				}
			}
			if ( CString(prpAFlight->Psta).IsEmpty())
				blHasAlloc  = false;
		break;
		case BLTDIA:
//			if( !CString(prpAFlight->Ttyp).IsEmpty() )
			{
				if(bgAutoAllocGateWithoutGlobalNature && FindInStrArray(ogSpotAllocation.omBltNatNoAllocList,prpAFlight->Ttyp))
				{
					blGlobalRes = true;
				}
			}
			if ( CString(prpAFlight->Blt1).IsEmpty() && CString(prpAFlight->Blt2).IsEmpty())
				blHasAlloc  = false;

		break;
		}


		if (blGlobalRes == false && blHasAlloc  == false)
		{
			rrpLine.AUrno = prpAFlight->Urno;

			rrpLine.AFlno = prpAFlight->Flno;
			rrpLine.AFlno.TrimRight();
			if (rrpLine.AFlno.IsEmpty())
  				rrpLine.AFlno = prpAFlight->Csgn;


			rrpLine.AStoa = prpAFlight->Stoa;
			rrpLine.AEtai = prpAFlight->Etai;
			rrpLine.AOrg3 = prpAFlight->Org3;
			rrpLine.ATtyp = prpAFlight->Ttyp;
			rrpLine.APos = prpAFlight->Psta;
			rrpLine.AGate1 = prpAFlight->Gta1;		// 050309 MVy: Gate1 added for arrival
			rrpLine.AGate2 = prpAFlight->Gta2;		// 050309 MVy: Gate2 added for arrival
			rrpLine.ABelt1 = prpAFlight->Blt1;
			rrpLine.ABelt2 = prpAFlight->Blt2;
 
			rrpLine.Regn = prpAFlight->Regn;
			rrpLine.Act3 =  prpAFlight->Act3;
			/*RST123
			if(imMode == POSDIA && bgTowRule)
			{
				if(olFlag && (prpAFlight != NULL && prpDFlight != NULL))
				{
					rrpLine.Towing = "TOWING";
				}
			}
			RST123*/



		}
	}

	// for departure flights
	if (prpDFlight)
	{
		bool blGlobalRes = false;
		bool blHasAlloc  = true;

		switch (imMode)
		{
		case GATDIA:
//			if( !CString(prpDFlight->Ttyp).IsEmpty() )
			{
				if(bgAutoAllocGateWithoutGlobalNature && FindInStrArray(ogSpotAllocation.omGatNatNoAllocList,prpDFlight->Ttyp))
//				if( ogSpotAllocation.omGatNatNoAllocList.Find(prpDFlight->Ttyp) >= 0)
				{
					blGlobalRes = true;
				}
			}
			if ( CString(prpDFlight->Gtd1).IsEmpty() && CString(prpDFlight->Gtd2).IsEmpty())
				blHasAlloc  = false;

		break;
		case POSDIA:
//			if( !CString(prpDFlight->Ttyp).IsEmpty() )
			{
				if(bgAutoAllocGateWithoutGlobalNature && FindInStrArray(ogSpotAllocation.omPstNatNoAllocList,prpDFlight->Ttyp))
//				if( ogSpotAllocation.omPstNatNoAllocList.Find(prpDFlight->Ttyp) >= 0)
				{
					blGlobalRes = true;
				}
			}
			if ( CString(prpDFlight->Pstd).IsEmpty())
				blHasAlloc  = false;

		break;
		case WRODIA:
//			if( !CString(prpDFlight->Ttyp).IsEmpty() )
			{
				if(bgAutoAllocGateWithoutGlobalNature && FindInStrArray(ogSpotAllocation.omWroNatNoAllocList,prpDFlight->Ttyp))
//				if( ogSpotAllocation.omWroNatNoAllocList.Find(prpDFlight->Ttyp) >= 0)
				{
					blGlobalRes = true;
				}
			}
			if ( CString(prpDFlight->Wro1).IsEmpty() && CString(prpDFlight->Wro2).IsEmpty())
				blHasAlloc  = false;

		break;
		}


		if (blGlobalRes == false && blHasAlloc  == false)
		{
			rrpLine.DUrno = prpDFlight->Urno;

			rrpLine.DFlno = prpDFlight->Flno;
			rrpLine.DFlno.TrimRight();
			if (rrpLine.DFlno.IsEmpty())
  				rrpLine.DFlno = prpDFlight->Csgn;

			rrpLine.DStod = prpDFlight->Stod;
			rrpLine.DEtdi = prpDFlight->Etdi;
			rrpLine.DDes3 = prpDFlight->Des3;
			rrpLine.DTtyp = prpDFlight->Ttyp;
			rrpLine.DPos = prpDFlight->Pstd;
			rrpLine.DGate1 = prpDFlight->Gtd1;
			rrpLine.DGate2 = prpDFlight->Gtd2;		// 050309 MVy: Gate2 added for departure
			if (!CString(prpDFlight->Wro2).IsEmpty())
				rrpLine.DWro = CString(prpDFlight->Wro1) + "/" + CString(prpDFlight->Wro2);
			else
				rrpLine.DWro = CString(prpDFlight->Wro1);

			rrpLine.Regn = prpDFlight->Regn;
			rrpLine.Act3 =  prpDFlight->Act3;



		}
	}
	
	if(imMode == POSDIA )
	{

		if(prpDFlight != NULL && prpAFlight != NULL)
		{
			if((prpDFlight->Ftyp[0] == 'T' || prpDFlight->Ftyp[0] == 'G') && (prpAFlight->Ftyp[0] == 'T' || prpAFlight->Ftyp[0] == 'G'))
			{
				rrpLine.Towing = "TOWING";
			}
			else
			{
				if((prpDFlight->Ftyp[0] == 'T' || prpDFlight->Ftyp[0] == 'G') && (prpAFlight->Ftyp[0] != 'T' && prpAFlight->Ftyp[0] != 'G'))
				{
					rrpLine.Towing = "A /TOW";
				}
				else
				{
					if((prpAFlight->Ftyp[0] == 'T' || prpAFlight->Ftyp[0] == 'G') && (prpDFlight->Ftyp[0] != 'T' && prpDFlight->Ftyp[0] != 'G'))
					{
						rrpLine.Towing = "TOW/ D";
					}
				}
			}

		}
		else
		{
			if(prpDFlight != NULL && prpAFlight == NULL)
			{
				if((prpDFlight->Ftyp[0] == 'T' || prpDFlight->Ftyp[0] == 'G') )
				{
					rrpLine.Towing = "TOWING";
				}

			}

			if(prpAFlight != NULL && prpDFlight == NULL)
			{
				if((prpAFlight->Ftyp[0] == 'T' || prpAFlight->Ftyp[0] == 'G') )
				{
					rrpLine.Towing = "TOWING";
				}
			}
		}
	}
	
  
	// local times requested?
	if(bgGatPosLocal) UtcToLocal(rrpLine);

    return;
}


// convert all the times in the given line in local time
bool WoResTableViewer::UtcToLocal(WORESTABLE_LINEDATA &rrpLine) const
{
	if (rrpLine.AStoa != TIMENULL)
		ogBasicData.UtcToLocal(rrpLine.AStoa);
	if (rrpLine.AEtai != TIMENULL)
		ogBasicData.UtcToLocal(rrpLine.AEtai);
	if (rrpLine.DStod != TIMENULL)
		ogBasicData.UtcToLocal(rrpLine.DStod);
	if (rrpLine.DEtdi != TIMENULL)
		ogBasicData.UtcToLocal(rrpLine.DEtdi);
 
	return true;
}




int WoResTableViewer::CreateLine(WORESTABLE_LINEDATA &rrpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareLines(rrpLine, omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rrpLine);
	return ilLineno;
}


void WoResTableViewer::DeleteLine(int ipLineno)
{
	// delete internal line
	omLines.DeleteAt(ipLineno);
	// delete table line
	pomTable->DeleteTextLine(ipLineno);

}



bool WoResTableViewer::FindLine(long lpUrno, char cpFPart, int &ripLineno) const
{
	ripLineno = -1;
	// scan all intern lines
    for (int i = omLines.GetSize() - 1; i >= 0; i--)
	{
		switch (cpFPart)
		{
		case 'A':
		  if(omLines[i].AUrno == lpUrno)
		  {
			ripLineno = i;
			return true;
		  }
		  break;
		case 'D':
		  if(omLines[i].DUrno == lpUrno)
		  {
			ripLineno = i;
			return true;
		  }
		  break;
		}
	}
	return false;
}


void WoResTableViewer::DeleteAll()
{
    omLines.DeleteAll();
}


 

// Load intern data to the table
void WoResTableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	DrawHeader();

	WORESTABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));

//CGMSHMA		pomTable->SetTextLineColor(ilLc, RGB(255,255,255), RGB(0,0,255));

		olColList.DeleteAll();
	}


    pomTable->DisplayTable();
}



// load the given line from the intern data to the table
void WoResTableViewer::InsertDisplayLine( int ipLineNo)
{
	if(!((ipLineNo >= 0) && (ipLineNo < omLines.GetSize())))
		return;
	CCSPtrArray<TABLE_COLUMN> olColList;
	MakeColList(&omLines[ipLineNo], olColList);
	pomTable->InsertTextLine(ipLineNo, olColList, &omLines[ipLineNo]);
	olColList.DeleteAll();
}






void WoResTableViewer::DrawHeader()
{
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Font = &romTableHeaderFont;

	// for all rows
	for (int i=0; i < WORESTABLE_COLCOUNT; i++)
	{
		rlHeader.Alignment = COLALIGN_CENTER;
		rlHeader.Length = imTableColWidths[i]; 
		rlHeader.Text = omTableHeadlines[i]; 
		
		if((imMode != POSDIA) && (omTableHeadlines[i].CompareNoCase(GetString(IDS_STRING1824)) == 0))
			continue;

		omHeaderDataArray.New(rlHeader);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}





void WoResTableViewer::MakeColList(WORESTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList)
{
		TABLE_COLUMN rlColumnData;

		rlColumnData.VerticalSeparator = SEPA_NONE;
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.Font = &romTableLinesFont;

		CTime chkStoa = prlLine->AStoa;
		ogBasicData.LocalToUtc(chkStoa);
		/*
		if((chkStoa < ogAllocPeriodFrom) || (chkStoa > ogAllocPeriodTo))
			rlColumnData.TextColor = RGB(185,185,185);
		else
		*/
			rlColumnData.TextColor = BLACK;

		// Arrival
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->AFlno;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

 		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->AStoa.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

 		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->AEtai.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->AStoa.Format("%d");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->AOrg3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->ATtyp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
 
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->APos;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
 
		rlColumnData.Alignment = COLALIGN_LEFT;		// 050309 MVy: Gate1 added for arrival
		rlColumnData.Text = prlLine->AGate1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;		// 050309 MVy: Gate2 added for arrival
		rlColumnData.Text = prlLine->AGate2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->ABelt1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->ABelt2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.TextColor = BLACK;

		// Common
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Act3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

 		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Regn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		CTime chkStod = prlLine->DStod;
		ogBasicData.LocalToUtc(chkStod);
		/*
		if((chkStod < ogAllocPeriodFrom) || (chkStod > ogAllocPeriodTo))
			rlColumnData.TextColor = RGB(185,185,185);
		else
		*/
			rlColumnData.TextColor = BLACK;

		// Departure
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DFlno;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

 		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DStod.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

 		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DEtdi.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DStod.Format("%d");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DDes3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DTtyp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DPos;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DGate1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;		// 050309 MVy: Gate2 added for departure
		rlColumnData.Text = prlLine->DGate2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DWro;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.TextColor = BLACK;
		if(imMode == POSDIA && bgTowingDemand)
		{
			rlColumnData.Alignment = COLALIGN_LEFT;
			rlColumnData.Text = prlLine->Towing;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}
}



// compare function for sorting
int WoResTableViewer::CompareLines(const WORESTABLE_LINEDATA &rrpLine1, const WORESTABLE_LINEDATA &rrpLine2) const
{

	if (imMode == POSDIA && bgFWRSortPOSByType)
	{
		// Sort order: Arr/Dep/Rot, Stoa, Stod

		// First calculate flight type 
		char clTyp1='R';
		if (rrpLine1.AUrno == 0) clTyp1 = 'D';
		if (rrpLine1.DUrno == 0) clTyp1 = 'A';
		char clTyp2='R';
		if (rrpLine2.AUrno == 0) clTyp2 = 'D';
		if (rrpLine2.DUrno == 0) clTyp2 = 'A';


		// flight type
		if (clTyp1 != clTyp2)
		{
			if (clTyp1 == 'R') return -1;
			if (clTyp2 == 'R') return 1;

			if (clTyp1 == 'A') return -1;
			if (clTyp2 == 'A') return 1;

		}
		// Stoa
		if (rrpLine1.AStoa < rrpLine2.AStoa) return -1;
		if (rrpLine1.AStoa > rrpLine2.AStoa) return 1;
		// Stod
		if (rrpLine1.DStod < rrpLine2.DStod) return -1;
		if (rrpLine1.DStod > rrpLine2.DStod) return 1;
 
		return 0;
	}


	int	ilCompareResult;
	CTime olTime1;
	CTime olTime2;

	if(rrpLine1.AStoa == TIMENULL)
		olTime1 = rrpLine1.DStod;
	else
		olTime1 = rrpLine1.AStoa;

	if(rrpLine2.AStoa == TIMENULL)
		olTime2 = rrpLine2.DStod;
	else
		olTime2 = rrpLine2.AStoa;

	ilCompareResult = (olTime1 == olTime2)? 0:
		(olTime1 > olTime2)? 1: -1;

	 return ilCompareResult;


}






////////////////////////////////////////////////////////////////////////////////
///  WoResTableViewer broadcast functions


static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	if (vpDataPointer == NULL) 
		return;

    WoResTableViewer *polViewer = (WoResTableViewer *)popInstance;

    if (ipDDXType == D_FLIGHT_CHANGE)
        polViewer->ProcessFlightChange(*(DIAFLIGHTDATA *)vpDataPointer);
//    if (ipDDXType == D_FLIGHT_CHANGE_WORES)
//    if (ipDDXType == D_CHANGE_WORES)
//        polViewer->ProcessFlightChange(*(DIAFLIGHTDATA *)vpDataPointer);
	if (ipDDXType == D_FLIGHT_DELETE)
        polViewer->ProcessFlightDelete(*(DIAFLIGHTDATA *)vpDataPointer);
}




void WoResTableViewer::ProcessFlightChange(const DIAFLIGHTDATA &rrpFlight)
{
	if (!pomTable) return;

	if (!pomParentDlg->bmUpdate)
		return;

	int ilTopIndex = pomTable->pomListBox->GetTopIndex();
	


	DiaCedaFlightData::RKEYLIST *prlRkey = ogPosDiaFlightData.GetRotationByRkey(rrpFlight.Rkey);
	

	if(prlRkey != NULL)
	{
	
		int ilCount = prlRkey->Rotation.GetSize();

		DIAFLIGHTDATA *prlFlight = NULL; 

		for( int i = 0; i < ilCount; i++)
		{
			prlFlight = &prlRkey->Rotation[i];

			for (int ilLineNo = omLines.GetSize() - 1; ilLineNo >= 0; ilLineNo--)
			{
			  if(omLines[ilLineNo].AUrno == prlFlight->Urno || omLines[ilLineNo].DUrno == prlFlight->Urno)
			  {
				DeleteLine(ilLineNo);
			  }
			}
		}

		MakeLines(&prlRkey->Rotation);	
		pomTable->pomListBox->SetTopIndex(ilTopIndex);

	}

	return;
}







bool WoResTableViewer::ProcessFlightDelete(const DIAFLIGHTDATA &rrpFlight)
{
	if (!pomTable)
		return false;

 	if (!pomParentDlg->bmUpdate)
		return false;

	int ilTopIndex = pomTable->pomListBox->GetTopIndex();

	bool blChanged = false;
	// Delete all lines with the urno of the changed flight
    for (int ilLineNo = omLines.GetSize() - 1; ilLineNo >= 0; ilLineNo--)
	{
	  if(omLines[ilLineNo].AUrno == rrpFlight.Urno || omLines[ilLineNo].DUrno == rrpFlight.Urno)
	  {
		DeleteLine(ilLineNo);
		blChanged = true;
	  }
	}

	const DIAFLIGHTDATA *polOtherFlight = NULL;
	if (IsArrival(rrpFlight.Org3, rrpFlight.Des3))
		polOtherFlight = ogPosDiaFlightData.GetDeparture(&rrpFlight);
	else if (IsDeparture(rrpFlight.Org3, rrpFlight.Des3))
		polOtherFlight = ogPosDiaFlightData.GetArrival(&rrpFlight);


	if (polOtherFlight)
	{
		for (int ilLineNo = omLines.GetSize() - 1; ilLineNo >= 0; ilLineNo--)
		{
		  if(omLines[ilLineNo].AUrno == polOtherFlight->Urno || omLines[ilLineNo].DUrno == polOtherFlight->Urno)
		  {
			DeleteLine(ilLineNo);
			blChanged = true;
		  }
		}
	}

//	pomParentDlg->UpdateCaption();

	if (blChanged)
		pomTable->pomListBox->SetTopIndex(ilTopIndex);

	return blChanged;
}



bool WoResTableViewer::InsertFlight(const DIAFLIGHTDATA &rrpFlight)
{
	const DIAFLIGHTDATA *polOtherFlight = NULL;
	 
	// Insert flight
	int ilLineNo1 = -1;
	int ilLineNo2 = -1;

	
	if (IsCircular(rrpFlight.Org3, rrpFlight.Des3))
	{
		polOtherFlight = ogPosDiaFlightData.GetDeparture(&rrpFlight);
		if (IsPassFilter(rrpFlight, 'A', imMode)  ||
			(polOtherFlight != NULL && IsPassFilter(*polOtherFlight, 'D', imMode)))
		{
			// make arrival part of circular flight
			ilLineNo1 = MakeLine(&rrpFlight, polOtherFlight);
		}

		polOtherFlight = ogPosDiaFlightData.GetArrival(&rrpFlight);
		if (IsPassFilter(rrpFlight, 'D', imMode)  ||
			(polOtherFlight != NULL && IsPassFilter(*polOtherFlight, 'A', imMode)))
		{
			// make departure part of circular flight
			ilLineNo2 = MakeLine(polOtherFlight, &rrpFlight);
		}
	}
	else if (IsArrival(rrpFlight.Org3, rrpFlight.Des3))
	{
		polOtherFlight = ogPosDiaFlightData.GetDeparture(&rrpFlight);
		if(IsPassFilter(rrpFlight, 'A', imMode) ||
			(polOtherFlight != NULL && IsPassFilter(*polOtherFlight, 'D', imMode)))
		{
			// make arrival part of flight
			ilLineNo1 = MakeLine(&rrpFlight, ogPosDiaFlightData.GetDeparture(&rrpFlight));
		}
	}
	else if (IsDeparture(rrpFlight.Org3, rrpFlight.Des3))
	{
		polOtherFlight = ogPosDiaFlightData.GetArrival(&rrpFlight);
		if(IsPassFilter(rrpFlight, 'D', imMode) ||
			(polOtherFlight != NULL && IsPassFilter(*polOtherFlight, 'A', imMode)))
		{
			// make departure part of flight
			ilLineNo1 = MakeLine(ogPosDiaFlightData.GetArrival(&rrpFlight), &rrpFlight);
		}
	}

	InsertDisplayLine(ilLineNo1);
	InsertDisplayLine(ilLineNo2);

	if (ilLineNo1 > -1 || ilLineNo2 > -1)
		return true;
	else
		return false;
}











