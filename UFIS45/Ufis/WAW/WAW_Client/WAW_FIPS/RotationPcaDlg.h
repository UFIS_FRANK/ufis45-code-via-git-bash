#if !defined(AFX_ROTATIONPCADLG_H__4F349052_D3DE_11D3_806D_00008638F9E1__INCLUDED_)
#define AFX_ROTATIONPCADLG_H__4F349052_D3DE_11D3_806D_00008638F9E1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RotationPcaDlg.h : header file
//

#include <resrc1.h>
#include <CCSEdit.h>
#include <RotationDlgCedaFlightData.h>
 
/////////////////////////////////////////////////////////////////////////////
// RotationPcaDlg dialog

class RotationPcaDlg : public CDialog
{
// Construction
public:
	RotationPcaDlg(CWnd* pParent = NULL, ROTATIONDLGFLIGHTDATA *prpFlight = NULL, bool bpLocal = false);   // standard constructor


	ROTATIONDLGFLIGHTDATA *prmFlight;
	bool bmLocal;


// Dialog Data
	//{{AFX_DATA(RotationPcaDlg)
	enum { IDD = IDD_ROTATION_PCA };
	CCSEdit	m_CE_Gaae3;
	CCSEdit	m_CE_Gaae2;
	CCSEdit	m_CE_Gaae1;
	CCSEdit	m_CE_Gaab3;
	CCSEdit	m_CE_Gaab2;
	CCSEdit	m_CE_Gaab1;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RotationPcaDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(RotationPcaDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROTATIONPCADLG_H__4F349052_D3DE_11D3_806D_00008638F9E1__INCLUDED_)
