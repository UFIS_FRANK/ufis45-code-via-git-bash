// HEADER 
#ifndef _CEDAHAIDATA_H_ 
#define _CEDAHAIDATA_H_ 

#include <CCSCedaData.h> 


struct HAIDATA 
{
	long 	 Flnu; 	// Eindeutige Datensatz-Nr.
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
 
	char 	 Hsna[6]; 	// Handling Agent
	char 	 Task[129];
	char 	 Rema[256];
 
 
	HAIDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
	}

}; // end HAIDATA

 
class CedaHaiData : public CCSCedaData
{
public:
	CedaHaiData();
	~CedaHaiData();

 	char pcmFList[512];

	void Clear();
	bool ExecRelCommand(char cpMode, const CString &ropFields, const CString &ropData);
 	CString GetHsna(long lpFlightUrno, int ipNo) const;
  	bool ReadFlnus(const CString &ropFlnus);
	bool UpdateSpecialRELHDL(const CPtrArray &ropFlightUrnos, const CPtrArray &ropRecNums, const CStringArray &ropHSANs, const CStringArray &ropTASKs, const CStringArray &ropREMAs);
 
	//////////////////////////////////////

private:
	CCSPtrArray<HAIDATA> omData;
    CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omFlnuMap;

 	void InsertInternal(HAIDATA *prpHai);
 

};

 
#endif // _CEDAHAIDATA_H_ 
