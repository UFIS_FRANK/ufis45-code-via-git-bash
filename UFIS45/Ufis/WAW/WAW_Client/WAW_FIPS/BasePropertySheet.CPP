// BasePropertySheet.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>
#include <afxpriv.h>
#include <ccsddx.h>
// These following include files are necessary
#include <BasePropertySheet.h>
#include <StringConst.h>
#include <resource.h>
#include <resrc1.h>
#include <FlightSearchTableViewer.h>
#include <PrivList.h>


IMPLEMENT_DYNAMIC(BasePropertySheet, CPropertySheet)


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define WM_RESIZEPAGE WM_APP+100

enum { CDF_CENTER, CDF_TOPLEFT, CDF_NONE };


// helper function which sets the font for a window and all its children
// and also resizes everything according to the new font
void ChangeDialogFont(CWnd* pWnd, CFont* pFont, int nFlag)
{
	CRect windowRect;

	// grab old and new text metrics
	TEXTMETRIC tmOld, tmNew;
	CDC * pDC = pWnd->GetDC();
	CFont * pSavedFont = pDC->SelectObject(pWnd->GetFont());
	pDC->GetTextMetrics(&tmOld);
	pDC->SelectObject(pFont);
	pDC->GetTextMetrics(&tmNew);
	pDC->SelectObject(pSavedFont);
	pWnd->ReleaseDC(pDC);

	long oldHeight = tmOld.tmHeight+tmOld.tmExternalLeading;
	long newHeight = tmNew.tmHeight+tmNew.tmExternalLeading;

	if (nFlag != CDF_NONE)
	{
		// calculate new dialog window rectangle
		CRect clientRect, newClientRect, newWindowRect;

		pWnd->GetWindowRect(windowRect);
		pWnd->GetClientRect(clientRect);
		long xDiff = windowRect.Width() - clientRect.Width();
		long yDiff = windowRect.Height() - clientRect.Height();
	
		newClientRect.left = newClientRect.top = 0;
		newClientRect.right = clientRect.right * tmNew.tmAveCharWidth / tmOld.tmAveCharWidth;
		newClientRect.bottom = clientRect.bottom * newHeight / oldHeight;

		if (nFlag == CDF_TOPLEFT) // resize with origin at top/left of window
		{
			newWindowRect.left = windowRect.left;
			newWindowRect.top = windowRect.top;
			newWindowRect.right = windowRect.left + newClientRect.right + xDiff;
			newWindowRect.bottom = windowRect.top + newClientRect.bottom + yDiff;
		}
		else if (nFlag == CDF_CENTER) // resize with origin at center of window
		{
			newWindowRect.left = windowRect.left - 
							(newClientRect.right - clientRect.right)/2;
			newWindowRect.top = windowRect.top -
							(newClientRect.bottom - clientRect.bottom)/2;
			newWindowRect.right = newWindowRect.left + newClientRect.right + xDiff;
			newWindowRect.bottom = newWindowRect.top + newClientRect.bottom + yDiff;
		}
		pWnd->MoveWindow(newWindowRect);
	}

	pWnd->SetFont(pFont);

	// iterate through and move all child windows and change their font.
	CWnd* pChildWnd = pWnd->GetWindow(GW_CHILD);

	while (pChildWnd)
	{
		pChildWnd->SetFont(pFont);
		pChildWnd->GetWindowRect(windowRect);

		CString strClass;
		::GetClassName(pChildWnd->m_hWnd, strClass.GetBufferSetLength(32), 31);
		strClass.MakeUpper();
		if(strClass==_T("COMBOBOX"))
		{
			CRect rect;
			pChildWnd->SendMessage(CB_GETDROPPEDCONTROLRECT,0,(LPARAM) &rect);
			windowRect.right = rect.right;
			windowRect.bottom = rect.bottom;
		}

		pWnd->ScreenToClient(windowRect);
		windowRect.left = windowRect.left * tmNew.tmAveCharWidth / tmOld.tmAveCharWidth;
		windowRect.right = windowRect.right * tmNew.tmAveCharWidth / tmOld.tmAveCharWidth;
		windowRect.top = windowRect.top * newHeight / oldHeight;
		windowRect.bottom = windowRect.bottom * newHeight / oldHeight;
		pChildWnd->MoveWindow(windowRect);
		
		pChildWnd = pChildWnd->GetWindow(GW_HWNDNEXT);
	}
}

/////////////////////////////////////////////////////////////////////////////
// BasePropertySheet
//
int BasePropertySheet::imCount = 0;


BasePropertySheet::BasePropertySheet(LPCSTR pszCaption,
	CWnd* pParentWnd, CViewer *popViewer, UINT iSelectPage)
	:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
	bmViews = true;
	pomViewer = popViewer;
	bmViewChanged = false;
}

void BasePropertySheet::LoadDataFromViewer()
{
}

void BasePropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
}

int BasePropertySheet::QueryForDiscardChanges()
{
	return IDOK;
}

BasePropertySheet::~BasePropertySheet()
{
	ogDdx.UnRegister(this,NOTUSED);
	if (m_fntPage.m_hObject)
		VERIFY (m_fntPage.DeleteObject ());
}




BEGIN_MESSAGE_MAP(BasePropertySheet, CPropertySheet)
	//{{AFX_MSG_MAP(BasePropertySheet)
	ON_CBN_SELCHANGE(IDC_SHEET_VIEW, OnViewSelChange)
	ON_BN_CLICKED(IDC_SHEET_SAVE, OnSave)
	ON_BN_CLICKED(IDC_SHEET_DELETE, OnDelete)
	ON_BN_CLICKED(IDC_SHEET_APPLY, OnApply)
	ON_BN_CLICKED(IDOK, OnOK)
	ON_BN_CLICKED(IDCANCEL, OnCancel)
	//}}AFX_MSG_MAP
	ON_MESSAGE (WM_RESIZEPAGE, OnResizePage)	
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BasePropertySheet implementation

int BasePropertySheet::DoModal() 
{
	if (imCount > 0)
		return IDCANCEL;

	imCount++;

	// Checking the given viewer, don't open the dialog if the viewer is invalid
	if (pomViewer == NULL)
//	if (pomViewer == NULL || pomViewer->SelectView().IsEmpty())
	{
		TRACE("BasePropertySheet: Invalid viewer was given.\n");
		imCount--;
		return IDCANCEL;
	}

	// This line do nothing, just fix bug in CViewer (by setting m_ViewName)
	pomViewer->SelectView(pomViewer->SelectView());
	int ilRc =  CPropertySheet::DoModal();

	imCount--;

	return ilRc;

}

/////////////////////////////////////////////////////////////////////////////
// BasePropertySheet message handlers

BOOL BasePropertySheet::OnInitDialog() 
{
	BOOL bResult = CPropertySheet::OnInitDialog();

	// Resize the window for new combo-box and buttons
	CRect rcWindow;
	GetWindowRect(rcWindow);
	rcWindow.InflateRect(0, 20);
	MoveWindow(rcWindow);

	CPoint point;
	::GetCursorPos(&point);
	//changed for the PRF 8269
	if (strcmp(pcgHome, "LIS") != 0)
	{
	    this->SetWindowPos(&wndTop, point.x, point.y,0,0, SWP_SHOWWINDOW | SWP_NOSIZE);// | SWP_NOMOVE );
	}

	// Prepare the position for new control objects
	CRect rcApply;
	GetDlgItem(ID_APPLY_NOW)->GetWindowRect(rcApply);
	ScreenToClient(rcApply);
	CRect rcCancel;
	GetDlgItem(IDCANCEL)->GetWindowRect(rcCancel);
	ScreenToClient(rcCancel);


	CButton *polButton = (CButton *)GetDlgItem(IDOK);
	if (polButton)
		polButton->ShowWindow(SW_HIDE);


	// Create the combo box, save button, and delete button
	CComboBox *polView = new CComboBox;
	polView->Create(WS_VISIBLE | CBS_DROPDOWN | WS_VSCROLL | CBS_SORT,
		CRect(6, rcApply.top, 140, rcApply.top + 300),
		this, IDC_SHEET_VIEW);

	//polView->SetDroppedWidth( 200);

	polView->SetFont(GetDlgItem(IDCANCEL)->GetFont());
	CButton *polSave = new CButton;
	polSave->Create(ID_BUTTON_SAVE, WS_VISIBLE | BS_PUSHBUTTON,
		CRect(145, rcApply.top, 145 + rcApply.Width(), rcApply.bottom),
		this, IDC_SHEET_SAVE);
	polSave->SetFont(GetDlgItem(IDCANCEL)->GetFont());
	CButton *polDelete = new CButton;
	polDelete->Create(ID_BUTTON_DELETE, WS_VISIBLE | BS_PUSHBUTTON,
		CRect(147 + rcApply.Width(), rcApply.top, 147 + 2*rcApply.Width(), rcApply.bottom),
		this, IDC_SHEET_DELETE);
    polDelete->SetFont(GetDlgItem(IDCANCEL)->GetFont());
	CButton *polApply = new CButton;
	polApply->Create(ID_BUTTON_APPLY, WS_VISIBLE | BS_PUSHBUTTON,
		CRect(149 + 2*rcApply.Width(), rcApply.top, 149 + 3*rcApply.Width(), rcApply.bottom),
		this, IDC_SHEET_APPLY);
	polApply->SetFont(GetDlgItem(IDCANCEL)->GetFont());


	if (!bmViews)
	{
		polView->ShowWindow(SW_HIDE);
		polSave->ShowWindow(SW_HIDE);
		polDelete->ShowWindow(SW_HIDE);
		polApply->ShowWindow(SW_HIDE);
		CButton *polButtonOK = (CButton *)GetDlgItem(IDOK);
		if (polButtonOK)
			polButtonOK->ShowWindow(SW_SHOW);
	}

	if (bmViews)
	{
		// Draw the separator between save/delete buttons and standard close/help buttons
		CRect rcClient;
		GetClientRect(rcClient);
		omSeparator.Create("", WS_VISIBLE | SS_BLACKFRAME,
			CRect(6, rcApply.bottom + 7, rcClient.Width() - 6, rcApply.bottom + 9), this);
	}


	// Remove the button "Apply"; also shift "OK" and "Cancel" buttons
	GetDlgItem(ID_APPLY_NOW)->DestroyWindow();
	CRect rcOK = rcCancel;
	rcOK.OffsetRect(5, rcOK.Height() + 16);
	GetDlgItem(IDOK)->MoveWindow(rcOK);
	rcCancel = rcApply;
	rcCancel.OffsetRect(5, rcCancel.Height() + 16);
	GetDlgItem(IDCANCEL)->MoveWindow(rcCancel);
	GetDlgItem(IDCANCEL)->SetWindowText(GetString(IDS_STRING737));


	// move cancel
	if (polApply)
	{
		CRect olRect;
		polApply->GetWindowRect(olRect);
		ScreenToClient(olRect);

		int ilCY = olRect.bottom - olRect.top;

		CWnd *polButtonCancel = (CWnd *)GetDlgItem(IDCANCEL);
		if (polButtonCancel)
		{
			CRect olRectCancel;
			polButtonCancel->GetWindowRect(olRectCancel);
			ScreenToClient(olRectCancel);
			polButtonCancel->MoveWindow(olRect.left, olRectCancel.top, olRect.Width(), olRect.Height());
		}
	}

	// move ok
	if (polApply)
	{
		CRect olRect;
		polDelete->GetWindowRect(olRect);
		ScreenToClient(olRect);

		//int ilCY = olRect.bottom - olRect.top;

		CWnd *polButtonOK = (CWnd *)GetDlgItem(IDOK);
		if (polButtonOK)
		{
			CRect olRectCancel;
			polButtonOK->GetWindowRect(olRectCancel);
			ScreenToClient(olRectCancel);
			polButtonOK->MoveWindow(olRect.left, olRectCancel.top, olRect.Width(), olRect.Height());
		}
	}




	CWnd *olHelpButton = GetDlgItem(IDHELP);
	if (olHelpButton != NULL)
	{
		CRect rcHelp;
		olHelpButton->GetWindowRect(rcHelp);
		ScreenToClient(rcHelp);
		rcHelp.OffsetRect(0, rcApply.Height() + 16);
		olHelpButton->MoveWindow(rcHelp);
		olHelpButton->SetWindowPos(&wndBottom,0,0,0,0,SWP_HIDEWINDOW | SWP_NOMOVE );
		olHelpButton->EnableWindow(FALSE);
	}

	// get the font for the first active page
	CPropertyPage* pPage = GetActivePage ();
	ASSERT (pPage);

	if (m_fntPage.m_hObject == NULL)
	{
		BuildPropPageArray();		
	}


	// change the font for the sheet
	ChangeDialogFont (this, &m_fntPage, CDF_CENTER);

	// change the font for each page
	for (int iCntr = 0; iCntr < GetPageCount (); iCntr++)
	{
		VERIFY (SetActivePage (iCntr));
		CPropertyPage* pPage = GetActivePage ();
		ASSERT (pPage);
		ChangeDialogFont (pPage, &m_fntPage, CDF_CENTER);
	}

	VERIFY (SetActivePage (pPage));

	// set and save the size of the page
	CTabCtrl* pTab = GetTabControl ();
	ASSERT (pTab);

	if (m_psh.dwFlags & PSH_WIZARD)
	{
		pTab->ShowWindow (SW_HIDE);
		GetClientRect (&m_rctPage);

		CWnd* pButton = GetDlgItem (ID_WIZBACK);
		ASSERT (pButton);
		CRect rc;
		pButton->GetWindowRect (&rc);
		ScreenToClient (&rc);
		m_rctPage.bottom = rc.top-2;
	}
	else
	{
		pTab->GetWindowRect (&m_rctPage);
		ScreenToClient (&m_rctPage);
		pTab->AdjustRect (FALSE, &m_rctPage);
	}

	// resize the page	
	pPage->MoveWindow (&m_rctPage);

	UpdateComboBox();	// update data in the combo-box
	LoadDataFromViewer();	// update data in every pages
	GetActivePage()->UpdateData(FALSE);


	return bResult;
}

BOOL BasePropertySheet::DestroyWindow() 
{
	/***************++
	#define IDC_SHEET_VIEW		10
	#define IDC_SHEET_SAVE		11
	#define IDC_SHEET_DELETE	12
	#define IDC_SHEET_APPLY		13
	*********************/

	if(m_hWnd != NULL)
	{
	
		CComboBox *polCombo = (CComboBox *)GetDlgItem(IDC_SHEET_VIEW);
			
		if(polCombo != NULL)
			delete polCombo;

		CButton *polButton = (CButton *)GetDlgItem(IDC_SHEET_SAVE);
		if(polButton != NULL)
			delete polButton;
		
		polButton = (CButton *)GetDlgItem(IDC_SHEET_DELETE);
		if(polButton != NULL)
			delete polButton;

		polButton = (CButton *)GetDlgItem(IDC_SHEET_APPLY);
		if(polButton != NULL)
			delete polButton;
	}

	return CPropertySheet::DestroyWindow();
}

bool BasePropertySheet::CheckXDays()
{
	CTime olFrom;
	CTime olTo;
	CString olFtyps;
	pomViewer->GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);

//	olFrom = TIMENULL;
//	olTo = TIMENULL;

	//check for valid times
	if (olTo == TIMENULL || olFrom == TIMENULL)
	{
		CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2649), GetString(ST_HINWEIS), MB_OK | MB_ICONWARNING);
		return false;
	}

	if (olTo < olFrom)
	{
		CString olText;
		CString olTextTo = olTo.Format("%d.%m.%y %H:%M");
		CString olTextFrom = olFrom.Format("%d.%m.%y %H:%M");

		olText.Format(GetString(IDS_STRING2648), olTextFrom, olTextTo);

		CFPMSApp::MyTopmostMessageBox(this, olText, GetString(ST_HINWEIS), MB_OK | MB_ICONWARNING);
		return false;
	}

	//check timespan with setup entry
	if (ogXDays.GetTotalMinutes() > 0 && (olTo-olFrom).GetTotalMinutes() > ogXDays.GetTotalMinutes())
	{
		CString olText;
		CString olTextForbidden;
		olText.Format(GetString(IDS_STRING2609), (olTo-olFrom).GetDays(), ogXDays.GetDays());
		olTextForbidden.Format(GetString(IDS_STRING2710), (olTo-olFrom).GetDays(), ogXDays.GetDays());

		if(ogPrivList.GetStat("SETUP_XDAYS") == '1')
		{
			if (CFPMSApp::MyTopmostMessageBox(this, olText, GetString(ST_HINWEIS), MB_YESNO | MB_ICONWARNING) != IDYES)
 				return false;
			else
 				return true;
		}
		else
		{
			if (CFPMSApp::MyTopmostMessageBox(this, olTextForbidden, GetString(ST_HINWEIS), MB_ICONERROR) != IDYES)
 				return false;
			else
 				return true;
		}
	}

	return true;
}

void BasePropertySheet::OnViewSelChange()
{
	bmViewChanged = true;

	//TRACE("OnViewSelChange\n");
	if (GetActivePage()->UpdateData() == FALSE)
		return;
	if (QueryForDiscardChanges() != IDOK)
	{
		UpdateComboBox();
		return;
	}

	// Check if user didn't change anything, so we don't have to reload the view
	if (GetDlgItem(IDCANCEL)->IsWindowEnabled() &&		// no change made before?
		GetComboBoxText() == pomViewer->SelectView())	// still on the old view?
		return;

	// Discard changes and restore data from the registry database
	CString olViewName = GetComboBoxText();
	pomViewer->SelectView(olViewName);
	UpdateComboBox();
	LoadDataFromViewer();
	GetActivePage()->UpdateData(FALSE);
//	GetActivePage()->CancelToClose();
}

void BasePropertySheet::OnSave()
{
	TRACE("OnSave\n");
	// Don't allow the user to change the default view
	CString olViewName = GetComboBoxText();
	if (olViewName == "<Default>")
		return;

	// no invalid characters allowed
	if (!CheckViewName(olViewName))
		return;

	// Validate the current page before saving
	if (GetActivePage()->UpdateData() == FALSE)
	     return;

	// Save data to the registry database
	CStringArray olFilters;
	pomViewer->GetFilterPage(olFilters);	// use filters in the last view
	pomViewer->CreateView(olViewName, olFilters, !bmViews);
	pomViewer->SelectView(olViewName);	// just make sure that CViewer() work correctly

	SaveDataToViewer(olViewName,TRUE);

	if (!CheckXDays())
		return;

	UpdateComboBox();
	CString olWhere;
/*	
	bool blRotation;
	if(pomViewer->GetFlightWhereString(olWhere, blRotation) == true)
	{
		ofstream of;
		of.open( "c:\\tmp\\where.txt", ios::out);
		of << olWhere.GetBuffer(0);
		of.close();
	}
*/	
	EndDialog(IDOK);
}

void BasePropertySheet::OnApply()
{
	bmOK = false;

	//TRACE("OnApply\n");
	// Don't allow the user to change the default view
	CString olViewName = GetComboBoxText();
	if (olViewName == "<Default>")
		return ;

	// no invalid characters allowed
	if (pomViewer && !pomViewer->IsKindOf( RUNTIME_CLASS( FlightSearchTableViewer ) ) )
	{
		if (!CheckViewName(olViewName))
		{
			return ;
		}
	}

	// Validate the current page before saving
	if (GetActivePage()->UpdateData() == FALSE)
		return ;

	// Save data to the registry database
	if (pomViewer && pomViewer->IsKindOf( RUNTIME_CLASS( FlightSearchTableViewer ) ) )
	{
		CStringArray olFilters;
		pomViewer->GetFilterPage(olFilters);	// use filters in the last view
		pomViewer->CreateView("OnlyOne", olFilters, false);
		pomViewer->SelectView("OnlyOne");	// just make sure that CViewer() work correctly
		UpdateWindow();
		SaveDataToViewer("Dummy");
	}
	else
	{
		CStringArray olFilters;
		pomViewer->GetFilterPage(olFilters);	// use filters in the last view
		bool blWithDBDelete = false;
		pomViewer->CreateView(olViewName, olFilters, blWithDBDelete);
		pomViewer->SelectView(olViewName);	// just make sure that CViewer() work correctly
		SaveDataToViewer(olViewName,FALSE);
	}

	if (!CheckXDays())
		return ;

	UpdateComboBox();

	bmOK = true;
	
	EndDialog(IDOK);
	return ;
}


void BasePropertySheet::OnDelete()
{
	//TRACE("OnDelete\n");
	// Don't allow the user to delete the default view
	CString olViewName = GetComboBoxText();
	if (olViewName == "<Default>")
		return;

	// Delete the selected view from the registry database
	pomViewer->DeleteView(olViewName);
	pomViewer->SelectView("<Default>");
	UpdateComboBox();
	LoadDataFromViewer();
	GetActivePage()->UpdateData(FALSE);

	//todo: must call twice, view still remains in combobox (don�t know why at the moment, no time)
	pomViewer->DeleteView(olViewName);
	pomViewer->SelectView("<Default>");
	UpdateComboBox();
	LoadDataFromViewer();
	GetActivePage()->UpdateData(FALSE);
}

void BasePropertySheet::OnOK()
{
	// no invalid characters allowed
	CString olViewName = GetComboBoxText();
	if (!CheckViewName(olViewName))
		return;

	if (pomViewer)
	{
		if (!pomViewer->IsKindOf( RUNTIME_CLASS( FlightSearchTableViewer ) ) )
		{
			OnApply();
			if (!bmOK)
				return;
		}
		else
		{
			OnApply();
			if (!bmOK)
				return;
		}

	}

	//OnApply();
	//TRACE("OnOK\n");
	if (GetActivePage()->UpdateData() == FALSE)
		return;

	if (QueryForDiscardChanges() != IDOK)
	{
		UpdateComboBox();
		return;
	}

	// Terminate the PropertySheet
	EndDialog(IDOK);
}

void BasePropertySheet::OnCancel()
{
	EndDialog(IDCANCEL);
}



/////////////////////////////////////////////////////////////////////////////
// BasePropertySheet -- help routines

void BasePropertySheet::UpdateComboBox()
{

	CComboBox *polView = (CComboBox *)GetDlgItem(IDC_SHEET_VIEW);
	polView->ResetContent();
	CStringArray olViews;
	pomViewer->GetViews(olViews);
	for (int ilIndex = 0; ilIndex < olViews.GetSize(); ilIndex++)
	{
		polView->AddString(olViews[ilIndex]);
		if (olViews[ilIndex] == pomViewer->SelectView())
			polView->SetCurSel(ilIndex);
	}

	BOOL blIsDefaultView = pomViewer->SelectView() == "<Default>";


	polView->SelectString(-1, pomViewer->SelectView());

	//GetDlgItem(IDC_SHEET_DELETE)->EnableWindow(!blIsDefaultView);
}

CString BasePropertySheet::GetComboBoxText()
{
	CString olViewName;
	CComboBox *polView = (CComboBox *)GetDlgItem(IDC_SHEET_VIEW);
	int ilItemID;
	if ((ilItemID = polView->GetCurSel()) == CB_ERR)
		polView->GetWindowText(olViewName);
	else
		polView->GetLBText(ilItemID, olViewName);

	//TRACE("Viewname: %s\n", (const char *)olViewName);
	return olViewName;
}

BOOL BasePropertySheet::IsIdentical(CStringArray &ropArray1, CStringArray &ropArray2)
{
	if (ropArray1.GetSize() != ropArray2.GetSize())	// incompatible array size?
		return FALSE;

	for (int i = ropArray1.GetSize()-1; i >= 0; i--)
		if (!IsInArray(ropArray1[i], ropArray2))	// unidentical element found?
			return FALSE;

	return TRUE;
}

BOOL BasePropertySheet::IsInArray(CString &ropString, CStringArray &ropArray)
{
	for (int i = ropArray.GetSize()-1; i >= 0; i--)
		if (ropString == ropArray[i])	// identical string found?
			return TRUE;
	
	return FALSE;
}

BOOL BasePropertySheet::ContinueModal() 
{
	// TODO: Speziellen Code hier einf�gen und/oder Basisklasse aufrufen
	
	return CPropertySheet::ContinueModal();
}

BOOL BasePropertySheet::CheckViewName(CString &olViewName)
{
	if (olViewName.FindOneOf("#@%,'") != -1)
	{
		::MessageBox(NULL, GetString(IDS_CHAR_FAILED),GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
		return FALSE;
	}

	return TRUE;
}

void BasePropertySheet::BuildPropPageArray()
{
	CPropertySheet::BuildPropPageArray();

	// get first page
	CPropertyPage* pPage = GetPage (0);
	ASSERT (pPage);
	
	// dialog template class in afxpriv.h
	CDialogTemplate dlgtemp;
	// load the dialog template
	VERIFY (dlgtemp.Load (pPage->m_psp.pszTemplate));
	// get the font information
	CString strFace;
	WORD	wSize;
	VERIFY (dlgtemp.GetFont (strFace, wSize));
	if (m_fntPage.m_hObject)
		VERIFY (m_fntPage.DeleteObject ());
	// create a font using the info from first page
	VERIFY (m_fntPage.CreatePointFont (wSize*10, strFace));
}

BOOL BasePropertySheet::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
	NMHDR* pnmh = (LPNMHDR) lParam;

	// the sheet resizes the page whenever it is activated so we need to size it correctly
	if (TCN_SELCHANGE == pnmh->code)
		PostMessage (WM_RESIZEPAGE);
	
	return CPropertySheet::OnNotify(wParam, lParam, pResult);
}

LONG BasePropertySheet::OnResizePage (UINT, LONG)
{
	// resize the page
	CPropertyPage* pPage = GetActivePage ();
	ASSERT (pPage);
	pPage->MoveWindow (&m_rctPage);

	return 0;
}

BOOL BasePropertySheet::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	// the sheet resizes the page whenever the Apply button is clicked so we need to size it correctly
	if (ID_APPLY_NOW == wParam ||
		ID_WIZNEXT == wParam ||
		ID_WIZBACK == wParam)
		PostMessage (WM_RESIZEPAGE);
	
	return CPropertySheet::OnCommand(wParam, lParam);
}
