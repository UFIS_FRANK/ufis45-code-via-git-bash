//{{AFX_INCLUDES()
#include "tab.h"
#include "ufiscom.h"
//}}AFX_INCLUDES
#if !defined(AFX_MULTIDELAYDLG_H__58006FC7_0C5B_4F94_9A39_7DAC65FE7939__INCLUDED_)
#define AFX_MULTIDELAYDLG_H__58006FC7_0C5B_4F94_9A39_7DAC65FE7939__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MultiDelayDlg.h : header file
//
#include <resrc1.h>
/////////////////////////////////////////////////////////////////////////////
// MultiDelayDlg dialog

class MultiDelayDlg : public CDialog
{
// Construction
public:
	ROTATIONDLGFLIGHTDATA *prmFlight;
	CTAB *prmTabSrc;
	MultiDelayDlg(CWnd* pParent, ROTATIONDLGFLIGHTDATA *prpFlight, CTAB *prpSourceTab);   // standard constructor
	bool bmOpen;


// Dialog Data
	//{{AFX_DATA(MultiDelayDlg)
	enum { IDD = IDD_MULTI_DELAYCODES };
	CStatic	m_HelpText;
	CButton	m_OK;
	CButton	m_Cancel;
	CButton	m_New;
	CButton	m_Delete;
	CTAB	myTab;
	CUfisCom	myUfisCom;
	//}}AFX_DATA

	bool isInit;

	CString GetEmptyLine();
	void InitTab();
	void InitUfisCom();
	void FillTab();
	void CheckValuesChanged(int LineNo);
	void HandleDelete();
	void HandleSizeAndPosition();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(MultiDelayDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(MultiDelayDlg)
	afx_msg void OnNew();
	afx_msg void OnDelete();
	afx_msg void OnSave();
	afx_msg void OnCloseInplaceEditTab(long LineNo, long ColNo);
	afx_msg void OnEditPositionChangedTab(long LineNo, long ColNo, LPCTSTR Value);
	afx_msg void OnHitKeyOnLineTab(short Key, long LineNo);
	afx_msg void OnRowSelectionChangedTab(long LineNo, BOOL Selected);
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnBoolPropertyChangedTab(long LineNo, long ColNo, BOOL NewValue);
	afx_msg void OnSendLButtonClickTab(long LineNo, long ColNo);
	afx_msg void OnSendLButtonDblClickTab(long LineNo, long ColNo);
	afx_msg void OnClose();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg void OnInplaceEditCellTab(long LineNo, long ColNo, LPCTSTR NewValue, LPCTSTR OldValue);
	afx_msg void OnSendRButtonClickTab(long LineNo, long ColNo);
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MULTIDELAYDLG_H__58006FC7_0C5B_4F94_9A39_7DAC65FE7939__INCLUDED_)
