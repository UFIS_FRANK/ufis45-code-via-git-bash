// SpotAllocateDlg.cpp : implementation file
// 09-jan-01	rkr		PRF_ATH:08012001 e-mail_ebe; Dr watson if fileds are empty 
//

#include <iostream.h> 
#include <fstream.h>
#include <string>
#include <stdafx.h>
#include <fpms.h>
#include <SpotAllocateDlg.h>
#include <SpotAllocation.h>
#include <Konflikte.h>
#include <WoResTableDlg.h>
#include <PosDiagram.h>
#include <GatDiagram.h>
#include <BltDiagram.h>
#include <WroDiagram.h>
#include <resrc1.h>
#include <CCSGlobl.h>
#include <CCSTime.h>
#include <process.h>
#include <PrivList.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SpotAllocateDlg dialog


SpotAllocateDlg::SpotAllocateDlg(CWnd* pParent /*=NULL*/) 
	: CDialog(SpotAllocateDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(SpotAllocateDlg)
	m_GatLeftBuffer = 0;
	m_GatRightBuffer = 0;
	m_PstLeftBuffer = 0;
	m_PstRightBuffer = 0;
	//}}AFX_DATA_INIT
}


void SpotAllocateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SpotAllocateDlg)
	DDX_Control(pDX, IDC_TIME_FROM,    m_CE_Time_From);
	DDX_Control(pDX, IDC_TIME_TO,    m_CE_Time_To);
	DDX_Control(pDX, IDC_DATE_TO,    m_CE_Date_To);
	DDX_Control(pDX, IDC_DATE_FROM,    m_CE_Date_From);
	DDX_Control(pDX, IDC_ALLOCATE_WRO,    m_CB_Allocate_Wro);
	DDX_Control(pDX, IDC_ALLOCATE_POS,    m_CB_Allocate_Pos);
	DDX_Control(pDX, IDC_ALLOCATE_GAT,    m_CB_Allocate_Gat);
	DDX_Control(pDX, IDC_ALLOCATE_BLT,	  m_CB_Allocate_Blt);
	DDX_Control(pDX, IDC_CBOUSERALLOACTE, m_CB_Allocate_User);
	DDX_Control(pDX, IDC_PROGRESS1, m_Progress);
	DDX_Control(pDX, IDC_GATLEFTBUFFER, m_CE_GatLeftBuffer);
 	DDX_Control(pDX, IDC_GATRIGHTBUFFER, m_CE_GatRightBuffer);
 	DDX_Control(pDX, IDC_PSTLEFTBUFFER, m_CE_PstLeftBuffer);
 	DDX_Control(pDX, IDC_PSTRIGHTBUFFER, m_CE_PstRightBuffer);
	DDX_Text(pDX, IDC_GATLEFTBUFFER, m_GatLeftBuffer);
 	DDX_Text(pDX, IDC_GATRIGHTBUFFER, m_GatRightBuffer);
 	DDX_Text(pDX, IDC_PSTLEFTBUFFER, m_PstLeftBuffer);
 	DDX_Text(pDX, IDC_PSTRIGHTBUFFER, m_PstRightBuffer);
	//DDV_MinMaxInt(pDX, m_PstRightBuffer, 0, 99);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SpotAllocateDlg, CDialog)
	//{{AFX_MSG_MAP(SpotAllocateDlg)
	ON_BN_CLICKED(IDC_RESET, OnReset)
	ON_BN_CLICKED(IDC_USERALLOCATE, OnUserAllocate)	
    ON_MESSAGE(WM_SAS_PROGESS_UPDATE, OnProgressUpdate)
    ON_MESSAGE(WM_SAS_PROGESS_INIT, OnProgressInit)
	ON_CBN_SELCHANGE(IDC_CBOUSERALLOACTE, OnSelchangeCombo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SpotAllocateDlg message handlers

BOOL SpotAllocateDlg::OnInitDialog() 
{


	CDialog::OnInitDialog();
	m_Progress.SetRange(0, 100);
 
	m_CB_Allocate_Blt.SetCheck(TRUE);
	m_CB_Allocate_Gat.SetCheck(TRUE);
	m_CB_Allocate_Wro.SetCheck(TRUE);
	m_CB_Allocate_Pos.SetCheck(TRUE);

	//TSC 0909
	// SEC
    CButton *polCB;
	polCB = (CButton *) GetDlgItem(IDC_CBOUSERALLOACTE);
	SetpWndStatAll(ogPrivList.GetStat("AUTOALLOCDLG_SCENARIO_COMBO"),polCB);
	
	polCB = (CButton *) GetDlgItem(IDC_USERALLOCATE);
	SetpWndStatAll(ogPrivList.GetStat("AUTOALLOCDLG_SCENARIO"),polCB);

	if(ogPrivList.GetStat( "AUTOALLOCDLG_POS") == '0' || ogPrivList.GetStat( "AUTOALLOCDLG_POS") == '-')
	{
		m_CB_Allocate_Pos.SetCheck(FALSE);
		m_CB_Allocate_Pos.EnableWindow(FALSE);
		if(ogPrivList.GetStat( "AUTOALLOCDLG_POS") == '-')
			m_CB_Allocate_Pos.ShowWindow(SW_HIDE);
	}

	if(ogPrivList.GetStat( "AUTOALLOCDLG_GAT") == '0' || ogPrivList.GetStat( "AUTOALLOCDLG_GAT") == '-')
	{
		m_CB_Allocate_Gat.SetCheck(FALSE);
		m_CB_Allocate_Gat.EnableWindow(FALSE);
		if(ogPrivList.GetStat( "AUTOALLOCDLG_GAT") == '-')
			m_CB_Allocate_Gat.ShowWindow(SW_HIDE);
	}

	if(ogPrivList.GetStat( "AUTOALLOCDLG_BLT") == '0' || ogPrivList.GetStat( "AUTOALLOCDLG_BLT") == '-')
	{
		m_CB_Allocate_Blt.SetCheck(FALSE);
		m_CB_Allocate_Blt.EnableWindow(FALSE);
		if(ogPrivList.GetStat( "AUTOALLOCDLG_BLT") == '-')
			m_CB_Allocate_Blt.ShowWindow(SW_HIDE);
	}

	if(ogPrivList.GetStat( "AUTOALLOCDLG_WRO") == '0' || ogPrivList.GetStat( "AUTOALLOCDLG_WRO") == '-')
	{
		m_CB_Allocate_Wro.SetCheck(FALSE);
		m_CB_Allocate_Wro.EnableWindow(FALSE);
		if(ogPrivList.GetStat( "AUTOALLOCDLG_WRO") == '-')
			m_CB_Allocate_Wro.ShowWindow(SW_HIDE);
	}


	if(!bgscenario_autoallocate) // CEDA
	{
		GetDlgItem(IDC_CBOUSERALLOACTE)->ShowWindow(false);
		GetDlgItem(IDC_USERALLOCATE)   ->ShowWindow(false);
		oguseralloctepath="<Default>";
	
	}
	else
	{
		GetDlgItem(IDC_CBOUSERALLOACTE)->ShowWindow(true);
		GetDlgItem(IDC_USERALLOCATE)   ->ShowWindow(true);
		oguseralloctepath="<Default>";//SAFTY
		CheckUserAllocateCBO();
	}

	CRect r;
	if(bgAutoAllocTimeFrame)
	{
		m_CE_Time_From.SetTypeToTime();
		m_CE_Time_To.SetTypeToTime();
		m_CE_Date_From.SetTypeToDate();
		m_CE_Date_To.SetTypeToDate();

		CTime olFrom = ogPosDiaFlightData.omFrom;
		CTime olTo = ogPosDiaFlightData.omTo;
		
		if(bgGatPosLocal)
		{
			ogBasicData.UtcToLocal(olFrom);
			ogBasicData.UtcToLocal(olTo);
		}

		m_CE_Time_From.SetInitText(olFrom.Format("%H%M"));
		m_CE_Time_To.SetInitText(olTo.Format("%H%M"));

		m_CE_Date_From.SetInitText(olFrom.Format("%d.%m.%Y"));
		m_CE_Date_To.SetInitText(olTo.Format("%d.%m.%Y"));

	}
	else
	{
		m_CE_Time_From.ShowWindow(SW_HIDE);
		m_CE_Time_To.ShowWindow(SW_HIDE);
		m_CE_Date_From.ShowWindow(SW_HIDE);
		m_CE_Date_To.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_FROMTO_FRAME)->ShowWindow(false);

		int ilMoveUp = 75;
		polCB = (CButton *) GetDlgItem(IDC_RESET);
		polCB->GetWindowRect(r);
		ScreenToClient(r);
		r.top -= ilMoveUp; r.bottom -= ilMoveUp;
		polCB->MoveWindow(r);

		polCB = (CButton *) GetDlgItem(IDC_PROGRESS1);
		polCB->GetWindowRect(r);
		ScreenToClient(r);
		r.top -= ilMoveUp; r.bottom -= ilMoveUp;
		polCB->MoveWindow(r);

		polCB = (CButton *) GetDlgItem(IDOK);
		polCB->GetWindowRect(r);
		ScreenToClient(r);
		r.top -= ilMoveUp; r.bottom -= ilMoveUp;
		polCB->MoveWindow(r);

		polCB = (CButton *) GetDlgItem(IDCANCEL);
		polCB->GetWindowRect(r);
		ScreenToClient(r);
		r.top -= ilMoveUp; r.bottom -= ilMoveUp;
		polCB->MoveWindow(r);

		this->GetWindowRect(r);
		ScreenToClient(r);
		r.bottom -= ilMoveUp;
		this->MoveWindow(r);
		

	}


	m_CE_GatLeftBuffer.SetTypeToInt(0, 99);
	m_CE_GatLeftBuffer.SetBKColor(YELLOW);

	m_CE_GatRightBuffer.SetTypeToInt(0, 99);
	m_CE_GatRightBuffer.SetBKColor(YELLOW);

	m_CE_PstLeftBuffer.SetTypeToInt(0, 99);
	m_CE_PstLeftBuffer.SetBKColor(YELLOW);

	m_CE_PstRightBuffer.SetTypeToInt(0, 99);
	m_CE_PstRightBuffer.SetBKColor(YELLOW);

//	m_GatLeftBuffer = 5;
//	m_GatRightBuffer = 5;

	if (ogGateAllocBufferTime.GetTotalMinutes() == 0)
	{
		m_GatLeftBuffer = 5;
		m_GatRightBuffer = 5;
	}
	else
	{
		m_GatLeftBuffer = ogGateAllocBufferTime.GetTotalMinutes();
		m_GatRightBuffer = ogGateAllocBufferTime.GetTotalMinutes();

		m_CE_GatLeftBuffer.EnableWindow(FALSE);
		m_CE_GatRightBuffer.EnableWindow(FALSE);
	}


	if (ogPosAllocBufferTime.GetTotalMinutes() == 0)
	{
		m_PstLeftBuffer = 5;
		m_PstRightBuffer = 5;
	}
	else
	{
		m_PstLeftBuffer = ogPosAllocBufferTime.GetTotalMinutes();
		m_PstRightBuffer = ogPosAllocBufferTime.GetTotalMinutes();

		m_CE_PstLeftBuffer.EnableWindow(FALSE);
		m_CE_PstRightBuffer.EnableWindow(FALSE);
	}

	CString olTmpStr;
	olTmpStr.Format("%d", m_GatLeftBuffer);
	m_CE_GatLeftBuffer.SetWindowText(olTmpStr);
	olTmpStr.Format("%d", m_GatRightBuffer);
	m_CE_GatRightBuffer.SetWindowText(olTmpStr);
	olTmpStr.Format("%d", m_PstLeftBuffer);
	m_CE_PstLeftBuffer.SetWindowText(olTmpStr);
	olTmpStr.Format("%d", m_PstRightBuffer);
	m_CE_PstRightBuffer.SetWindowText(olTmpStr);

	//UpdateData(FALSE);
	// TODO: Add extra initialization here
/*
	// SEC
    CButton *polCB;
	polCB = (CButton *) GetDlgItem(IDC_CBOUSERALLOACTE);
	SetpWndStatAll(ogPrivList.GetStat("AUTOALLOCDLG_SCENARIO_COMBO"),polCB);
	
	polCB = (CButton *) GetDlgItem(IDC_USERALLOCATE);
	SetpWndStatAll(ogPrivList.GetStat("AUTOALLOCDLG_SCENARIO"),polCB);
*/	

	CPoint point;
	::GetCursorPos(&point);
	this->SetWindowPos(&wndTop, point.x, point.y,0,0, SWP_SHOWWINDOW | SWP_NOSIZE);// | SWP_NOMOVE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void SpotAllocateDlg::GetParameters() 
{
	//UpdateData(TRUE);



	CString olTmpStr;
	m_CE_GatLeftBuffer.GetWindowText(olTmpStr);
	if (olTmpStr.IsEmpty())
	{
		m_CE_GatLeftBuffer.SetWindowText("0");
		m_GatLeftBuffer = 0;
	}
	else
		m_GatLeftBuffer = atoi(olTmpStr);

	m_CE_GatRightBuffer.GetWindowText(olTmpStr);
	if (olTmpStr.IsEmpty())
	{
		m_CE_GatRightBuffer.SetWindowText("0");
		m_GatRightBuffer = 0;
	}
	else
		m_GatRightBuffer = atoi(olTmpStr);

	m_CE_PstLeftBuffer.GetWindowText(olTmpStr);
	if (olTmpStr.IsEmpty())
	{
		m_CE_PstLeftBuffer.SetWindowText("0");
		m_PstLeftBuffer = 0;
	}
	else
		m_PstLeftBuffer = atoi(olTmpStr);

	m_CE_PstRightBuffer.GetWindowText(olTmpStr);
	if (olTmpStr.IsEmpty())
	{
		m_CE_PstRightBuffer.SetWindowText("0");
		m_PstRightBuffer = 0;
	}
	else
		m_PstRightBuffer = atoi(olTmpStr);

	omAllocParameter.omGatLeftBuffer   = CTimeSpan(0,0,m_GatLeftBuffer,0);
	omAllocParameter.omGatRightBuffer  = CTimeSpan(0,0,m_GatRightBuffer,0);
	omAllocParameter.omPstLeftBuffer   = CTimeSpan(0,0,m_PstLeftBuffer,0);
	omAllocParameter.omPstRightBuffer  = CTimeSpan(0,0,m_PstRightBuffer,0);


	if(m_CB_Allocate_Wro.GetCheck() == TRUE)
		omAllocParameter.bmAllocateWro  = true;
	else
		omAllocParameter.bmAllocateWro  = false;

	if(m_CB_Allocate_Pos.GetCheck() == TRUE)
		omAllocParameter.bmAllocatePst  = true;
	else
		omAllocParameter.bmAllocatePst  = false;

	if(m_CB_Allocate_Gat.GetCheck() == TRUE)
		omAllocParameter.bmAllocateGat  = true;
	else
		omAllocParameter.bmAllocateGat  = false;

	if(m_CB_Allocate_Blt.GetCheck() == TRUE)
		omAllocParameter.bmAllocateBlt  = true;
	else
		omAllocParameter.bmAllocateBlt  = false;

	CString olDateFrom;
	CString olDateTo;
	CString olTimeFrom;
	CString olTimeTo;

	m_CE_Date_From.GetWindowText(olDateFrom);
	m_CE_Time_From.GetWindowText(olTimeFrom);
	m_CE_Date_To.GetWindowText(olDateTo);
	m_CE_Time_To.GetWindowText(olTimeTo);


	omAllocParameter.omFrom = ogPosDiaFlightData.omFrom;
	omAllocParameter.omTo = ogPosDiaFlightData.omTo;


	if(bgGatPosLocal)
	{
		ogBasicData.UtcToLocal(omAllocParameter.omFrom);
		ogBasicData.UtcToLocal(omAllocParameter.omTo);
	}


	if(!olDateFrom.IsEmpty() && !olTimeFrom.IsEmpty())
	{
		omAllocParameter.omFrom = DateHourStringToDate(olDateFrom, olTimeFrom);
		if (bgGatPosLocal)
			ogBasicData.LocalToUtc(omAllocParameter.omFrom);
	}

	if(!olDateTo.IsEmpty() && !olTimeTo.IsEmpty())
	{
		omAllocParameter.omTo = DateHourStringToDate(olDateTo, olTimeTo);
		if (bgGatPosLocal)
			ogBasicData.LocalToUtc(omAllocParameter.omTo);
	}

	if(omAllocParameter.omFrom < ogPosDiaFlightData.omFrom)
		omAllocParameter.omFrom = ogPosDiaFlightData.omFrom;

}


bool SpotAllocateDlg::CheckAll(void)
{
	CString olErrMes;
	CString olDlgText;
	if (!m_CE_PstLeftBuffer.GetStatus())
	{
		olErrMes.Format(GetString(IDS_STRING1892), GetString(IDS_STRING1893), 0, 99);
		MessageBox(olErrMes, GetString(ST_FEHLER), MB_ICONERROR | MB_OK);
		return false;		
	}
	if (!m_CE_PstRightBuffer.GetStatus())
	{
		olErrMes.Format(GetString(IDS_STRING1892), GetString(IDS_STRING1894), 0, 99);
		MessageBox(olErrMes, GetString(ST_FEHLER), MB_ICONERROR | MB_OK);
		return false;		
	}
	if (!m_CE_GatLeftBuffer.GetStatus())
	{
		olErrMes.Format(GetString(IDS_STRING1892), GetString(IDS_STRING1895), 0, 99);
		MessageBox(olErrMes, GetString(ST_FEHLER), MB_ICONERROR | MB_OK);
		return false;		
	}
	if (!m_CE_GatRightBuffer.GetStatus())
	{
		olErrMes.Format(GetString(IDS_STRING1892), GetString(IDS_STRING1896), 0, 99);
		MessageBox(olErrMes, GetString(ST_FEHLER), MB_ICONERROR | MB_OK);
		return false;		
	}

	if(!m_CE_Date_From.GetStatus() || !m_CE_Date_To.GetStatus() || !m_CE_Time_From.GetStatus() || !m_CE_Time_To.GetStatus())
	{
		MessageBox( GetString(IDS_STRING2649) , GetString(ST_FEHLER), MB_ICONERROR | MB_OK);
		return false;		
	}




	return true;
}


void SpotAllocateDlg::OnOK() 
{
/*	test 
	HMODULE hModule = ::GetModuleHandle("fips.exe");
	HMODULE hResHandle = AfxGetResourceHandle();
	AfxMessageBox(61712);
testende */

	if (!CheckAll())
		return;

	if(MessageBox( GetString(IDS_STRING1772), GetString(IDS_STRING1811), MB_YESNO ) != IDYES)
		return;
	
	bgPosDiaAutoAllocateInWork = true;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));		


	GetParameters();

	ogSpotAllocation.SetParent( this);

	ogSpotAllocation.SetAllocateParameters( &omAllocParameter );

	omSaveData.DeleteAll();

	for(int ilLc = ogPosDiaFlightData.omData.GetSize() - 1; ilLc >= 0; ilLc--)
	{
		omSaveData.New(ogPosDiaFlightData.omData[ilLc]);
	}


	ogSpotAllocation.CreateFlightList();


	if(omAllocParameter.bmAllocatePst)	
	{
		ogSpotAllocation.AutoPstAllocate();
		ogSpotAllocation.ReCheckAllChangedFlights(true, false, false, false);
	}

	if(omAllocParameter.bmAllocateGat)	
		ogSpotAllocation.AutoGatAllocate();

	if(omAllocParameter.bmAllocateWro)	
		ogSpotAllocation.AutoWroAllocate();
	
	if(omAllocParameter.bmAllocateBlt)	
		ogSpotAllocation.AutoBltAllocate();
	
	if	(	omAllocParameter.bmAllocatePst
		||	omAllocParameter.bmAllocateGat
		||	omAllocParameter.bmAllocateBlt
		)	
	{
//		bgCreateAutoAllocFiles = false; 
		if (bgCreateAutoAllocFiles)
			ogSpotAllocation.PrintAutoAlloc(omAllocParameter.bmAllocatePst,omAllocParameter.bmAllocateGat,omAllocParameter.bmAllocateBlt);
	}

	bool blSave = bgBeltAlocEqualDistribution;
	bgBeltAlocEqualDistribution = false; // block the check for multiple flights (imActMaxF==1 in spotallocaion::checkblt())
	ogSpotAllocation.ReCheckAllChangedFlights(false/*omAllocParameter.bmAllocatePst*/, omAllocParameter.bmAllocateGat, omAllocParameter.bmAllocateWro, omAllocParameter.bmAllocateBlt);

	m_Progress.SetRange(0, omSaveData.GetSize());
	m_Progress.SetStep(1);
	m_Progress.SetPos(0);


	DIAFLIGHTDATA *prlFlight; 
	DIAFLIGHTDATA *prlFlightSave; 


 	CString olData;
	CString olField = ogPosDiaFlightData.GetFieldList();

	CString olSelection;
 
 
	for( ilLc = omSaveData.GetSize() - 1; ilLc >= 0; ilLc--)
	{
		prlFlightSave = &omSaveData[ilLc];
		

		prlFlight = ogPosDiaFlightData.GetFlightByUrno(prlFlightSave->Urno);


		if(strlen(prlFlight->Gta1) != 0 && strcmp(prlFlight->Gta1, prlFlightSave->Gta1) != 0)
		{
			prlFlightSave->Ga1b = TIMENULL;
			prlFlightSave->Ga1e = TIMENULL;
		}
		else
		{
			prlFlightSave->Ga1b = prlFlight->Ga1b;
			prlFlightSave->Ga1e = prlFlight->Ga1e;
		}


		if(strlen(prlFlight->Gta2) != 0 && strcmp(prlFlight->Gta2, prlFlightSave->Gta2) != 0)
		{
			prlFlightSave->Ga2b = TIMENULL;
			prlFlightSave->Ga2e = TIMENULL;
		}
		else
		{
			prlFlightSave->Ga2b = prlFlight->Ga2b;
			prlFlightSave->Ga2e = prlFlight->Ga2e;
		}

		if(strlen(prlFlight->Gtd1) != 0 && strcmp(prlFlight->Gtd1, prlFlightSave->Gtd1) != 0)
		{
			prlFlightSave->Gd1b = TIMENULL;
			prlFlightSave->Gd1e = TIMENULL;
		}
		else
		{
			prlFlightSave->Gd1b = prlFlight->Gd1b;
			prlFlightSave->Gd1e = prlFlight->Gd1e;
		}



		if(strlen(prlFlight->Gtd2) != 0 && strcmp(prlFlight->Gtd2, prlFlightSave->Gtd2) != 0)
		{
			prlFlightSave->Gd2b = TIMENULL;
			prlFlightSave->Gd2e = TIMENULL;
		}
		else
		{
			prlFlightSave->Gd2b = prlFlight->Gd2b;
			prlFlightSave->Gd2e = prlFlight->Gd2e;
		}



		if(strlen(prlFlight->Psta) != 0 && strcmp(prlFlight->Psta, prlFlightSave->Psta) != 0)
		{
			prlFlightSave->Pabs = TIMENULL;
			prlFlightSave->Paes = TIMENULL;
		}
		else
		{
			prlFlightSave->Pabs = prlFlight->Pabs;
			prlFlightSave->Paes = prlFlight->Paes;
		}

		if(strlen(prlFlight->Pstd) != 0 && strcmp(prlFlight->Pstd, prlFlightSave->Pstd) != 0)
		{
			prlFlightSave->Pdbs = TIMENULL;
			prlFlightSave->Pdes = TIMENULL;
		}
		else
		{
			prlFlightSave->Pdbs = prlFlight->Pdbs;
			prlFlightSave->Pdes = prlFlight->Pdes;
		}


		if(strlen(prlFlight->Blt1) != 0 && strcmp(prlFlight->Blt1, prlFlightSave->Blt1) != 0)
		{
			prlFlightSave->B1bs = TIMENULL;
			prlFlightSave->B1es = TIMENULL;
		}
		else
		{
			prlFlightSave->B1bs = prlFlight->B1bs;
			prlFlightSave->B1es = prlFlight->B1es;
		}


		if(strlen(prlFlight->Blt2) != 0 && strcmp(prlFlight->Blt2, prlFlightSave->Blt2) != 0)
		{
			prlFlightSave->B2bs = TIMENULL;
			prlFlightSave->B2es = TIMENULL;
		}
		else
		{
			prlFlightSave->B2bs = prlFlight->B2bs;
			prlFlightSave->B2es = prlFlight->B2es;
		}


		if(strlen(prlFlight->Wro1) != 0 && strcmp(prlFlight->Wro1, prlFlightSave->Wro1) != 0)
		{
			prlFlightSave->W1bs = TIMENULL;
			prlFlightSave->W1es = TIMENULL;
		}
		else
		{
			prlFlightSave->W1bs = prlFlight->W1bs;
			prlFlightSave->W1es = prlFlight->W1es;
		}


 		/////////////////////////////////////////////////////
//		prlFlight->bmNCC = true;
		ogPosDiaFlightData.UpdateFlight(prlFlight, prlFlightSave, true);
//		prlFlight->bmNCC = false;

		/*
		olData = "";
		ogPosDiaFlightData.MakeCedaData(olData, olField, prlFlightSave, prlFlight);

  		olSelection.Format("WHERE URNO = %ld", prlFlight->Urno);
  		
		ogPosDiaFlightData.UpdateFlight(prlFlight->Urno, olSelection, olField, olData);
		*/
		/////////////////////////////////////////////////////


		m_Progress.OffsetPos(1);

	} 
	omSaveData.DeleteAll();

	if (pogWoResTableDlg != NULL)
		pogWoResTableDlg->Rebuild();


	UpdateGatPosDiagrams();


	bgPosDiaAutoAllocateInWork = false;
	bgBeltAlocEqualDistribution = blSave;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));		

	CDialog::OnOK();
}


void SpotAllocateDlg::UpdateGatPosDiagrams()
{
	if (pogPosDiagram)
		pogPosDiagram->SendMessage(WM_REPAINT_ALL, 0, 0);

	if (pogGatDiagram)
		pogGatDiagram->SendMessage(WM_REPAINT_ALL, 0, 0);

	if (pogBltDiagram)
		pogBltDiagram->SendMessage(WM_REPAINT_ALL, 0, 0);

	if (pogWroDiagram)
		pogWroDiagram->SendMessage(WM_REPAINT_ALL, 0, 0);
}




void SpotAllocateDlg::OnReset()
{

	if (!CheckAll())
		return;

	if(MessageBox( GetString(IDS_STRING1772), GetString(IDS_STRING1812), MB_YESNO ) != IDYES)
		return;


	if(bgShowOnGround)
	{
		if ((strcmp(pcgHome, "ATH") == 0))
		{
			MessageBox( GetString(IDS_STRING2901), GetString(IDS_STRING1812), MB_OK );
				return;
		}
	}

	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));		
	
	ogKonflikte.DisableAttentionButtonsUpdate();

	ogSpotAllocation.SetParent( this);

	GetParameters();

	ogSpotAllocation.ResetAllocation(omAllocParameter.bmAllocatePst, omAllocParameter.bmAllocateGat, omAllocParameter.bmAllocateBlt, omAllocParameter.bmAllocateWro, omAllocParameter.omFrom, omAllocParameter.omTo);

	if (pogWoResTableDlg != NULL)
		pogWoResTableDlg->Rebuild();

	ogKonflikte.EnableAttentionButtonsUpdate();

	omSaveData.DeleteAll();

	UpdateGatPosDiagrams();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));		

	EndDialog(IDOK);

}


LONG SpotAllocateDlg::OnProgressUpdate(UINT wParam, LPARAM lParam)
{
	m_Progress.OffsetPos(wParam);



	return 0L;
}


LONG SpotAllocateDlg::OnProgressInit(UINT wParam, LPARAM lParam)
{
	
	m_Progress.SetRange(0,wParam);
	m_Progress.SetStep(1);
	m_Progress.SetPos(0);

	return 0L;
}


void SpotAllocateDlg::OnUserAllocate()
{
	CString olUser = ogBasicData.omUserID;
	CString olEditScenario = "EDIT_SCENARIO";
	CString olEditRules    = "EDIT_RULES";

	if(ogPrivList.GetStat("FIPSRules_Scenario") != '1')
		olEditScenario = "NOEDIT_SCENARIO";

	if(ogPrivList.GetStat("FIPSRules_Rules") != '1')
		olEditRules = "NOEDIT_RULES";

	CString olArgs = olUser + "," + olEditScenario + "," + olEditRules;
	char pclArgs[256] = "";
	strcpy (pclArgs, olArgs);

	char pclConfigPath[256]="";
	char pclscenarioexepath[256]="";


	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "FIPSRULESPATH", "DEFAULT",
		pclscenarioexepath, sizeof pclscenarioexepath, pclConfigPath);

	char *args[4];
	args[0] = "child";
	args[1] = pclArgs;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclscenarioexepath,args);

	
// TSC 0909
/*
	
		char pclConfigPath[256]="";
	    char pclscenarioexepath[256]="";
	
	
		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(ogAppName, "FIPSRULESPATH", "DEFAULT",
			pclscenarioexepath, sizeof pclscenarioexepath, pclConfigPath);
	
	STARTUPINFO si; 
    PROCESS_INFORMATION pi; 

    ZeroMemory( &si, sizeof(si) ); 
    si.cb = sizeof(si); 
    ZeroMemory( &pi, sizeof(pi) ); 



	if( !CreateProcess( 
			NULL,								  // No module name (use command line). 
			TEXT(pclscenarioexepath),			  // Command line. 
			NULL,					              // Process handle not inheritable. 
			NULL,								  // Thread handle not inheritable. 
			FALSE,								  // Set handle inheritance to FALSE. 
			0,									  // No creation flags. 
			NULL,								  // Use parent's environment block. 
			NULL,								  // Use parent's starting directory. 
			&si,								  // Pointer to STARTUPINFO structure. 
			&pi )								  // Pointer to PROCESS_INFORMATION structure. 
	  ) 
	{   printf( "CreateProcess failed (%d).\n", GetLastError()) ; }; 
	
  
//    WinExec(pclscenarioexepath,SW_SHOWNORMAL);
 

  
	EndDialog(IDCANCEL);
*/	
		 
}


void SpotAllocateDlg::CheckUserAllocateCBO()
{	//TSC 0909

 LoadUserAllocate();
 
}

void SpotAllocateDlg::OnSelchangeCombo()
{// TSC 0909

    int index; 
	index= m_CB_Allocate_User.GetCurSel();

	if(index!=0) 
	{
		oguseralloctepath = vCBOData[index-1];
	}
	else
	{ 
      oguseralloctepath = "<Default>";
	  m_CB_Allocate_User.SelectString(0,"<Default>");
	}
	 
}


void SpotAllocateDlg::LoadUserAllocate()
{
		vCBOData.clear();
	
		// DIRECTORY NAME IN CBO FPR CHOICE
	
	

			
	char pclConfigPath[256]="";
	char pclscenariopath[256]="";
	
	
		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(ogAppName, "SCENARIOPATH", "DEFAULT",
			pclscenariopath, sizeof pclscenariopath, pclConfigPath);
	
	// THIS THE PATH
	CString path=pclscenariopath ;
    path+="\\*";
    
/*	//	HANDLE fHandle; 
	//	WIN32_FIND_DATA wfd; 
    fHandle=FindFirstFile(path,&wfd); 

			FindNextFile(fHandle,&wfd); 


			while (FindNextFile(fHandle,&wfd)) 
			{ 
				if (!(wfd.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY) )
				{ 
       
						CString strua = wfd.cFileName; 
						int t =  strua.Find("ua");
						int u =  strua.Find("show");
						int v =  strua.Find("usa");
						if(t==0 && u==-1 && v > 0)//// ONLY WITH ua 
								{ 
							std::string strsub  = wfd.cFileName;
							std::string strsub2 = strsub.substr(2,strsub.length()-6);// "ua" AND ".usa"
							vCBOData.push_back(strsub2.c_str());
										
								}
       
				} 
			} 
		//	vCBOData.push_back("END");
			
	FindClose(fHandle);
*/
   
   CFileFind finder; 
   LPCTSTR pstr = NULL;;
   CString strWildcard(pstr); 
   CString strpath = pclscenariopath;
   strWildcard += strpath+"\\*.*"; 

   // start working for files 
   BOOL bWorking = finder.FindFile(strWildcard); 

   while (bWorking) 
   { 
      bWorking = finder.FindNextFile(); 

	   
						CString strua = finder.GetFileName();
						int t =  strua.Find("ua");
						int u =  strua.Find("show");
						int v =  strua.Find("usa");
						if(t==0 && u==-1 && v > 0)//// ONLY WITH ua 
								{ 
							std::string strsub  = finder.GetFileName();
							std::string strsub2 = strsub.substr(2,strsub.length()-6);// "ua" AND ".usa"
							vCBOData.push_back(strsub2.c_str());
										
								}

   } 

   finder.Close();
   
			m_CB_Allocate_User.ResetContent();
			m_CB_Allocate_User.AddString("<Default>");
			m_CB_Allocate_User.SelectString(0,"<Default>");
			
			// FOR COMOBOBOX
			for( int g = 0;g < vCBOData.size() ;g++)
			{
  				m_CB_Allocate_User.AddString(vCBOData[g]);
				int counter = g;
			}	
			
			UpdateData(FALSE); 

			
}
