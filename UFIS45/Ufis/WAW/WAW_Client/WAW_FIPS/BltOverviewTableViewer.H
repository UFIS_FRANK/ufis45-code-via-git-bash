#ifndef __BltOverviewTableViewer_H__

#include <stdafx.h>
#include <Fpms.h>
#include <CCSGlobl.h>
#include <CCSTable.h>
#include <DiaCedaFlightData.h>
#include <CViewer.h>
#include <CCSPrint.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct BLTOVERVIEWTABLE_LINEDATA
{
	long FUrno;
	CString		Ttyp;
	CString		Flti;
	CString		Belt;
	CString		Flno;
	CString		Org3;
	CString		Ext;
	CString		Pos;
	CTime		Open;
	CTime		Close;
	CTime		Stoa;
	CTime		Etai;
	CTime		Onbl;
	CTime		Land;
	CTime		FBag;
	CTime		LBag;
 
	BLTOVERVIEWTABLE_LINEDATA(void)
	{ 
		FUrno = 0;
	}
};
 


/////////////////////////////////////////////////////////////////////////////
// BltOverviewTableViewer

/////////////////////////////////////////////////////////////////////////////
// Class declaration of BltOverviewTableViewer

//@Man:
//@Memo: BltOverviewTableViewer
//@See:  STDAFX, CCSCedaData, CedaDIAFLIGHTDATA, CCSPTRARRAY,  CCSTable
/*@Doc:
  No comment on this up to now.
*/

#define BLTOVERVIEWTABLE_COLCOUNT 15

class BltOverviewTableViewer : public CViewer
{

public:
    //@ManMemo: Default constructor
    BltOverviewTableViewer();
    //@ManMemo: Default destructor
    ~BltOverviewTableViewer();
	
	void ProcessFlightChange(const DIAFLIGHTDATA &rrpFlight);
	void ProcessFlightDelete(const DIAFLIGHTDATA &rrpFlight);
	void CCAConvertDIA(CCAFLIGHTDATA* prpFlight, DIAFLIGHTDATA& prlDiaFlight);
    void ProcessCcaFlightDelete(CCAFLIGHTDATA* prpFlight);
    void ProcessCcaFlightChange(CCAFLIGHTDATA* prpFlight);
    void ProcessCcaChange(long lpCcaUrno);

	//void ProcessFlightInsert(DiaCedaFlightData::RKEYLIST  *prpRotation);

	void UnRegister();

  	void Attach(CCSTable *popAttachWnd);
    void ChangeViewTo(CString opView);

	// Print table to paper
	void PrintTableView(void);
	// Print table to file
	bool PrintPlanToFile(CString opFilePath) const;
	void SetResource(CString opResource);

	int GetFlightCount();
	void SetParentDlg(CDialog* ppParentDlg);

// Internal data processing routines
private:
	const int imOrientation;

	static int imTableColCharWidths[BLTOVERVIEWTABLE_COLCOUNT];
	CString omTableHeadlines[BLTOVERVIEWTABLE_COLCOUNT];
	int imTableColWidths[BLTOVERVIEWTABLE_COLCOUNT];
	int imPrintColWidths[BLTOVERVIEWTABLE_COLCOUNT];

	// Table fonts and widths
	CFont &romTableHeaderFont; 
	CFont &romTableLinesFont;
	const float fmTableHeaderFontWidth; 
	const float fmTableLinesFontWidth;

	// Table fonts and widths for printing
	CFont *pomPrintHeaderFont; 
	CFont *pomPrintLinesFont;
	float fmPrintHeaderFontWidth; 
	float fmPrintLinesFontWidth;

 	void DeleteAll();
 	void DeleteLine(int ipLineno);
 
  	void UpdateDisplay();

	void DrawHeader();

 	int CompareLines(const BLTOVERVIEWTABLE_LINEDATA &rrpLine1, const BLTOVERVIEWTABLE_LINEDATA &rrpLine2) const;

    void MakeLines(CCSPtrArray<DIAFLIGHTDATA> *popFlights);
    void MakeLines(CCSPtrArray<CCAFLIGHTDATA> *popFlights);
	void MakeLines(CCSPtrArray<DIACCADATA> *popDiaCca);

	int Make(const DIAFLIGHTDATA *rrpFlight, DIACCADATA* prpCca, bool bpInsertDisplay = false);
	int  MakeLine(const DIAFLIGHTDATA &rrpFlight, int ipBltNo, DIACCADATA* prpCca, CString opAdid);
	void MakeLineData(const DIAFLIGHTDATA &rrpFlight, BLTOVERVIEWTABLE_LINEDATA &rrpLine, int ipBltNo, DIACCADATA* prpCca, CString opAdid);
	void MakeColList(const BLTOVERVIEWTABLE_LINEDATA &rrpLine, CCSPtrArray<TABLE_COLUMN> &ropColList);
	int  CreateLine(BLTOVERVIEWTABLE_LINEDATA &rrpLine);

	void InsertFlight(const DIAFLIGHTDATA &rrpFlight);

	bool FindLine(long lpUrno, int &ripLineno) const;
	
	void InsertDisplayLine(int ipLineNo);

	bool PrintTableHeader(CCSPrint &ropPrint);
 	bool PrintTableLine(CCSPrint &ropPrint, int ipLineNo);
	
	bool UtcToLocal(BLTOVERVIEWTABLE_LINEDATA &rrpLine);

    CCSPtrArray<BLTOVERVIEWTABLE_LINEDATA> omLines;

	CCSTable *pomTable;
 	CString myResource;
	CString myCaption;
    CMapPtrToPtr omUrnoMap;
	CDialog* pomParentDlg;
	CString GetCaption();

	int imColCount;

};

#endif //__BltOverviewTableViewer_H__
