
// CPP-FILE 

#include <stdafx.h>
#include <afxwin.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CedaFlnoData.h>
#include <BasicData.h>


CedaFlnoData ogFlnoData;

CedaFlnoData::CedaFlnoData()
{
	// Create an array of CEDARECINFO for FlnoData
	BEGIN_CEDARECINFO(FLNODATA,GrmDataRecInfo)
		FIELD_CHAR_TRIM(Flno,"FLNO")
		FIELD_CHAR_TRIM(Fltn,"FLTN")
		FIELD_CHAR_TRIM(Flns,"FLNS")
		FIELD_CHAR_TRIM(Adid,"ADID")
 	END_CEDARECINFO //(FlnoData)

	// Copy the record structure
	for (int i=0; i< sizeof(GrmDataRecInfo)/sizeof(GrmDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&GrmDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"AFT");
    pcmFieldList = "FLNO,FLTN,FLNS,ADID";


}; // end Constructor



CedaFlnoData::~CedaFlnoData()
{
	TRACE("CedaFlnoData::~CedaFlnoData called\n");
	ClearAll();
}

void CedaFlnoData::ClearAll()
{
	omFlnoMap.RemoveAll();
	omData.DeleteAll();
}

BOOL CedaFlnoData::ReadFlnoData(CString opSeason, CString opAlc)
{
	// Select data from the database
	char pclWhere[100] = "";
	char pclAction[10] = "RT";
	char pclCom[10] = "RT";
	char pclSort[124] = " ORDER BY FLNO ";
	char pclAddCmd[20] = " DISTINCT ";

#if 0
bool CCSCedaData::CedaAction(char *pcpAction,
    char *pcpTableName, char *pcpFieldList,
    char *pcpSelection, char *pcpSort,
    char *pcpData, char *pcpDest,bool bpIsWriteAction /* false */, long lpID /* = 0*/, CCSPtrArray<RecordSet> *pomData, int ipFieldCount 
	, CString add )
{
#endif

//		if(!ogBCD.GetField("ALT", "ALC2", "ALC3", m_Alc, olTmp, olTmp))

	
	if (omSeason == opSeason && omAlc3 == opAlc)
	{
		// flnos already loaded
		return TRUE;
	}
	ClearAll();
	omSeason = opSeason;
	if (opAlc.GetLength() == 3) {
		omAlc3 = opAlc;
	} else 	if (opAlc.GetLength() == 2) {
			return FALSE;
	} else {
		return FALSE;
	}
	
	sprintf(pclWhere," WHERE SEAS = '%s' and ALC3 = '%s' ",omSeason,omAlc3);
 if (CedaAction(pclAction,
			CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,
			pclWhere,pclSort,pcgDataBuf,"BUF1",
			false,0L,NULL,1,pclAddCmd)  != true)
	{
		return false;
	}
	else
	{
		bool ilRc = true;
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			FLNODATA *prlFlno = new FLNODATA;
			if ((ilRc = GetBufferRecord(ilLc,prlFlno)) == true)
			{
				AddFlnoInternal(prlFlno);
			}
			else
			{
				delete prlFlno;
			}
		}
	}

	return TRUE;
}


BOOL CedaFlnoData::AddFlnoInternal(FLNODATA *prpFlno)
{
	omData.Add(prpFlno);
	omFlnoMap.SetAt(prpFlno->Flno,prpFlno);
	return TRUE;
}



FLNODATA *CedaFlnoData::GetFlnoByFlno(char *pcpFlno)
{
	FLNODATA *polTmpFlno;
	omFlnoMap.Lookup(pcpFlno,(void *&)polTmpFlno);
	return polTmpFlno;
}