// SeasonCollectADAskDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <fpms.h>
#include <SeasonCollectADAskDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld SeasonCollectADAskDlg 


SeasonCollectADAskDlg::SeasonCollectADAskDlg(CWnd* pParent, SEASONFLIGHTDATA *prpAFlight, SEASONFLIGHTDATA *prpDFlight)
	: CDialog(SeasonCollectADAskDlg::IDD, pParent)
{
	prmAFlight = prpAFlight;
	prmDFlight = prpDFlight;

	prmSelFlight = NULL;
	//{{AFX_DATA_INIT(SeasonCollectADAskDlg)
	//}}AFX_DATA_INIT
}


void SeasonCollectADAskDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SeasonCollectADAskDlg)
	DDX_Control(pDX, IDOKD, m_CB_OKD);
	DDX_Control(pDX, IDOKA, m_CB_OKA);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SeasonCollectADAskDlg, CDialog)
	//{{AFX_MSG_MAP(SeasonCollectADAskDlg)
	ON_BN_CLICKED(IDOKA, OnOka)
	ON_BN_CLICKED(IDOKD, OnOkd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten SeasonCollectADAskDlg 



BOOL SeasonCollectADAskDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString olArr = prmAFlight->Flno;
	CString olDep = prmDFlight->Flno;
	olArr.TrimRight();
	olDep.TrimRight();

	if (olArr.IsEmpty())
		m_CB_OKA.SetWindowText(prmAFlight->Csgn); 
	else
		m_CB_OKA.SetWindowText(prmAFlight->Flno);

	if (olDep.IsEmpty())
		m_CB_OKD.SetWindowText(prmDFlight->Csgn);
	else
		m_CB_OKD.SetWindowText(prmDFlight->Flno);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}





void SeasonCollectADAskDlg::OnOka() 
{
	prmSelFlight = prmAFlight;
	omSto = prmAFlight->Stoa;
	omAdid = "A";
	
	CDialog::OnOK();
}

void SeasonCollectADAskDlg::OnOkd() 
{
	prmSelFlight = prmDFlight;
	omSto = prmDFlight->Stod;
	omAdid = "D";

	CDialog::OnOK();
}

