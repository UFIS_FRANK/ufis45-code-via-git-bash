#ifndef __REPORT16TABLEVIEWER_H__
#define __REPORT16TABLEVIEWER_H__

#include <stdafx.h>
#include <RotationDlgCedaFlightData.h>
#include <Table.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct REPORT16TABLE_LINEDATA
{
//	long	Urno; 		// Eindeutige Datensatz-Nr.
	int		iPos;		// Position im omData

};


/////////////////////////////////////////////////////////////////////////////
// Report16TableViewer

class Report16TableViewer : public CViewer
{
// Constructions
public:
    Report16TableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo = NULL);
    ~Report16TableViewer();

    void Attach(CTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	void GetPrintHeader(void);

// Operations
public:
	void DeleteAll(void);
	void DeleteLine(int ipLineno);
	void UpdateDisplay(void);
	void PrintTableView(void);
	bool PrintTableHeader(void);
	bool PrintPlanToFile(char *pcpDefPath);
	//bool PrintTableLine(REPORT16TABLE_LINEDATA *prpLine, bool bpLastLine);
	bool PrintTableLine(int plLine, bool bpLastLine, int ipCnt);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
	void ShowLine(ROTATIONDLGFLIGHTDATA *prlFlight);

	CCSPtrArray <ROTATIONDLGFLIGHTDATA> omData;


// Attributes
private:
    CTable *pomTable;
	CString omFromStr;
	CString omToStr;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
///////
//Print 
	CCSPrint *pomPrint;
//	CString omTableName;
	CString omFooterName;
	char *pcmSelect;

	int imCount;

public:
    CCSPtrArray<REPORT16TABLE_LINEDATA> omLines;

	CMapStringToPtr omLineKeyMap;
	CString omTableName;
	CString omFileName;

};

#endif //__REPORT16TABLEVIEWER_H__
