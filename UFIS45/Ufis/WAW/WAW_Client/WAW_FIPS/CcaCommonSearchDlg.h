#ifndef AFX_CCACOMMONSEARCHDLG_H__21A349A1_BA4B_11D1_815A_0000B43C4B01__INCLUDED_
#define AFX_CCACOMMONSEARCHDLG_H__21A349A1_BA4B_11D1_815A_0000B43C4B01__INCLUDED_

// CcaCommonSearchDlg.h : Header-Datei
//
#include <resource.h>		// Hauptsymbole
#include <CCSEdit.h>
#include <CedaCcaData.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CCcaCommonSearchDlg 

class CCcaCommonSearchDlg : public CDialog
{
// Konstruktion
public:
	CCcaCommonSearchDlg(CWnd* pParent = NULL, char *opSelect = NULL, CTime *popFrom = NULL, CTime *popTo = NULL);

// Dialogfelddaten
	//{{AFX_DATA(CCcaCommonSearchDlg)
	enum { IDD = IDD_CCA_COMMON_SEARCH };
	CCSEdit	m_CE_Terminal;
	CCSEdit	m_CE_Schalter;
	CCSEdit	m_CE_Fluggs;
	CCSEdit	m_CE_Ende_t;
	CCSEdit	m_CE_Ende_d;
	CCSEdit	m_CE_Beginn_t;
	CCSEdit	m_CE_Beginn_d;
	CString	m_Beginn_d;
	CString	m_Beginn_t;
	CString	m_Ende_d;
	CString	m_Ende_t;
	CString	m_Fluggs;
	CString	m_Schalter;
	CString	m_Terminal;
	//}}AFX_DATA
	char *pcmSelect;

	CTime *pomFrom;
	CTime *pomTo;

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CCcaCommonSearchDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CCcaCommonSearchDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_CCACOMMONSEARCHDLG_H__21A349A1_BA4B_11D1_815A_0000B43C4B01__INCLUDED_
