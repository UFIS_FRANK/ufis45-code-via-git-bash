#if !defined(AFX_CCADIAGRAM_H__DAC99481_8801_11D1_B43F_0000B45A33F5__INCLUDED_)
#define AFX_CCADIAGRAM_H__DAC99481_8801_11D1_B43F_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CcaDiagram.h : header file
//

#include <CcaDiaViewer.h>
#include <CCSClientWnd.h>
#include <CCS3dStatic.h>
#include <CCSButtonCtrl.h>
#include <CCSTimeScale.h>
#include <CViewer.h>
#include <CcaChart.h>
#include <CCSEdit.h>
//#include <vector>
/////////////////////////////////////////////////////////////////////////////
// CcaDiagram frame

enum 
{
	CCA_ZEITRAUM_PAGE,
	CCA_FLUGSUCHEN_PAGE,
	CCA_GEOMETRIE_PAGE
};


class CcaDiagram : public CFrameWnd
{
	DECLARE_DYNCREATE(CcaDiagram)
public:
	CcaDiagram();           // protected constructor used by dynamic creation
	~CcaDiagram();

	bool ShowFlight(long lpUrno);
	bool UpdateDia();

	void ViewSelChange(char *pcpView);
	CDialogBar omDialogBar;
	void GetCurView( char * pcpView);

	void SetWndPos();
    void PositionChild();
    void SetTSStartTime(CTime opTSStartTime);
	void UpdateComboBox();
	void ChangeViewTo(const char *pcpViewName,bool RememberPositions = true);
	void PrePlanMode(BOOL bpToSet,CTime opPrePlanTime);
	void ToggleOnline(void);
	bool IsPassFlightFilter(CCAFLIGHTDATA *prpFlight);
	bool HasAllocatedCcas(CCSPtrArray<DIACCADATA> &ropCcaList);
	void ShowCcaDuration(CTime opCkbs, CTime opCkes);
	void ShowTime(const CTime &ropTime);

	void ActivateTables();
	void UpdateWoResButton();
	void UpdateWoDemButton();
	void UpdateConfButton();
	void UpdateKonflikteDlg();
	void UpdateChangesButton(CString& opText, COLORREF opColor);

	void LoadDataCBO();					// TSC 2009

	std::vector <CString> vCBOCICData;  // TSC 2009

	bool GetLoadedAllocations(CMapStringToString& opCicUrnoMap);
	bool GetLoadedCca(CCSPtrArray<DIACCADATA> &opCca, CMapStringToString& opTypMap, bool bpOnlySel, bool bpMustBeInOneDay);
	void SaveToReg();
	void SetFocusToDiagram();	 //PRF 8363
	void SetAllAreaButtonsColor();
	void OnUpdatePrevNext();
	void SetPosAreaButtonColor(int ipGroupno);
	CListBox *GetBottomMostGantt();

// Attributes
public:

	CcaDiagramViewer omViewer;

	CString omOldWhere;
	bool LoadFlights(char *pspView = NULL);

    CCS3DStatic omTime;
    CCS3DStatic omDate;
    CCS3DStatic omTSDate;
	bool bmAlreadyAlloc;

    CButton m_KlebeFkt;
    CCSTimeScale omTimeScale;
    CBitmapButton omBB1, omBB2;
    
    CCSClientWnd omClientWnd;
    CStatusBar omStatusBar;
    
    CTime omStartTime;
    CTimeSpan omDuration;
    CTime omTSStartTime;
    CTimeSpan omTSDuration;
    CTimeSpan omTSInterval;
    
	//CcaChart *pomChart;
	CPtrArray omChartArray;
  

    int imFirstVisibleChart;
    int imStartTimeScalePos;
    
	CString omCaptionText;
	bool bmIsViewOpen;


	bool bmLoadFlights;

	int imWhichMonitor; 
	int imMonitorCount;

	CCSButtonCtrl m_CB_Offline;
	CCSButtonCtrl m_CB_WoRes;
	CCSButtonCtrl m_CB_WoDem;
	CCSButtonCtrl m_CB_Conf;
	CCSButtonCtrl m_CB_CcaAttention;
	CCSButtonCtrl m_CB_Changes;

	

// Redisplay all methods for DDX call back function
public:
	BOOL bmNoUpdatesNow;
	bool bmRepaintAll;

	void RedisplayAll();
	bool bmReadAll;
// Time Band data, use the TIMENULL value for hiding the marker
public:
	void SetTimeBand(CTime opStartTime, CTime opEndTime);
	void UpdateTimeBand();
 	void SetMarkTime(CTime opStartTime, CTime opEndTime);
private:
	CTime omTimeBandStartTime;
	CTime omTimeBandEndTime;

	BltKonflikteDlg *pomCicKonflikteDlg;
	BltOverviewTableDlg *pomBltOverviewTableDlg;
	CString m_key;

private:
    CPoint omMaxTrackSize;
    CPoint omMinTrackSize;
    COLORREF lmBkColor;
    COLORREF lmTextColor;
    COLORREF lmHilightColor;
	bool bmTimeLine;

	void OnPrevChart();
	void OnNextChart();
	void OnFirstChart();
	void OnLastChart();


// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CcaDiagram)
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation

	// Generated message map functions
	//{{AFX_MSG(CcaDiagram)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnOffline();
	afx_msg void OnBeenden();
	afx_msg void OnDemand();
	afx_msg void OnCicConf();
	afx_msg void OnCommonCca();
	afx_msg void OnExpand();
	afx_msg void OnAllocate();
	afx_msg void OnFlightsWithoutResource();
	afx_msg void OnPrintGantt();
	afx_msg void OnDeleteTimeFrame();
	afx_msg void OnDestroy();
	afx_msg void OnAnsicht();
	afx_msg void OnInsert();
	afx_msg void OnClose();
    afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
    afx_msg void OnPaint();
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg LONG OnPositionChild(WPARAM wParam, LPARAM lParam);
    afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
	afx_msg void OnViewSelChange();
	afx_msg void OnCloseupView();
	afx_msg void OnZeit();
	afx_msg void OnSearch();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg LONG RepaintAll(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGatposConf();
	afx_msg void OnScenario();		 // TSC 2009
	afx_msg void OnScenarioChange(); // TSC 2009
	afx_msg void OnChanges();
	afx_msg LRESULT OnHandlingAgentColorChanged(WPARAM wParam, LPARAM lParam);		// 050329 MVy: color changed in setup dialog
	afx_msg void OnOverview();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.


#endif // !defined(AFX_CCADIAGRAM_H__DAC99481_8801_11D1_B43F_0000B45A33F5__INCLUDED_)
