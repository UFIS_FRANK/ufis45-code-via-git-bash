// stviewer.h : header file
//
// 07-Mar-08    Aung Moe    Add-in for Popup Alert Icon Menu when right click on the bar.

#ifndef _CcaDIAVIEWER_H_
#define _CcaDIAVIEWER_H_

#include <cviewer.h>
#include <CCSPrint.h>
#include <CcaCedaFlightData.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <Utils.h>
//#include <CcaChart.h> // *** //

#include "CicAgentPeriods.h"

class CcaChart;

struct CcaDIA_INDICATORDATA
{
	// standard data for indicators in general GanttBar
	CTime StartTime;
	CTime EndTime;
	COLORREF Color;
};

////////////////////////////////////////////////////////////////////////////////////
// structure for bars

class CcaDIA_BARDATA 
{
private:
	bool m_bEnabled ;		// 050316 MVy: color is grayed, user actions prohibited
	bool m_bEnabledButNotInOccupationTime ;		// 050323 MVy: flight belongs to agent but is not completely in his counter occupation time, special additional information

public:		// wrapper functions for state atribute access
	void Enable();		// 050316 MVy: allow user actions and coloring
	void Enable( bool bEnabledButNotInOccupationTime );		// 050316 MVy: allow user actions and coloring
	void Disable();		// 050316 MVy: no user actions and coloring are allowed
	bool IsEnabled();		// 050316 MVy: get current state
	bool IsInNotOccupationTime();		// 050316 MVy: get current special state

protected:
	void Create();
	void Copy( const CcaDIA_BARDATA& source );
public:
	const CcaDIA_BARDATA & operator= ( const CcaDIA_BARDATA& source );
	CcaDIA_BARDATA( const CcaDIA_BARDATA& source );

public:
	long Urno; // CCA
	
	long DUrno;
	long Rkey;
    CString Text;
	CString Ctyp;
    CTime StartTime;
    CTime EndTime;
	CString AAlert;//AM 20080307: Arrival Flight Alert string to use to show the popup icon menu
	CString DAlert;//AM 20080307: Departure Flight Alert string to use to show the popup icon menu
	long AUrno;//AM 20080307: Arrival Flight Urno


	//CCSPtrArray<TIMEFRAMEDATA> Times;

	long OrdDigit;

	//bool IsSelected;
	//bool AlcKonf;
	CString StatusText;
    int FrameType;
    int MarkerType;
    CBrush *MarkerBrush;
	CBrush *TriangleLeft;
	CBrush *TriangleRight;
	COLORREF TextColor;
	COLORREF TriangelColorRight;
	COLORREF TriangelColorLeft;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	CCSPtrArray<CcaDIA_INDICATORDATA> Indicators;

	CString SpecialREQ;
	int SpecialREQStaus;

public:
	CcaDIA_BARDATA();
	virtual ~CcaDIA_BARDATA();

};	// bar data class

//
////////////////////////////////////////////////////////////////////////////////////

struct CcaDIA_BKBARDATA
{
	long AUrno;
	long DUrno;
	long ARkey;
	long DRkey;
    CString Text;
	CTime StartTime;
    CTime EndTime;
    CBrush *MarkerBrush;	// background bar will be only displayed in FRAMENONE, MARKFULL
    CBrush *SepBrush;

	CcaDIA_BKBARDATA(void)
	{
		AUrno = 0;
		DUrno = 0;
		ARkey = 0;
		DRkey = 0;
		Text = "";
		StartTime = TIMENULL;
		EndTime  = TIMENULL;
		MarkerBrush = NULL;
		SepBrush = NULL ;
		::SetRectEmpty( &rcBounds );		// 050321 MVy: before drawn its rectangle is calculated, I will use this for faster detection of backbars while mouse movement -> no more calculation needed
	}

public:
	RECT rcBounds ;		// 050321 MVy: before drawn its rectangle is calculated, I will use this for faster detection of backbars while mouse movement -> no more calculation needed
	CString GetInfo( int iType = 0 );		// 050321 MVy: return string containing information in uman reaedable clear text; this may differ in release and debug mode

};


struct CCADIA_LINEDATA 
{

	long Urno;
	CString Bnam;
	CString Term;
	CTime StartTime;
	CTime EndTime;
	CString Text;
	bool IsExpanded;
	int MaxOverlapLevel;    // maximum number of overlapped level of bars in this line
	CCSPtrArray<CcaDIA_BARDATA> Bars;		// in order of painting (leftmost is the bottommost)
	CCSPtrArray<CcaDIA_BKBARDATA> BkBars;	// background bar
	CCSPtrArray<int> TimeOrder;				// maintain overlapped bars, see below
	COLORREF TextColor;
	COLORREF TextColor2;
	COLORREF TextColor3;
	bool Show;
	bool bmFullHeight;
		// Grouping
	CBrush *BkBrush;
	CBrush *SepBrush;

	CCADIA_LINEDATA(void)
	{
		Show = true;
		IsExpanded = false;
		bmFullHeight = false;
	}
};


struct CCADIA_GROUPDATA 
{
	int GroupNo;
	CString GroupName;			// Gruppen ID
	CString GroupType;			// 
	CCSPtrArray<CString> Codes; // For future Airlinecodes
	// standard data for groups in general Diagram
	CBrush *BkBrush;
	CBrush *SepBrush;
    CString Text;
    CCSPtrArray<CCADIA_LINEDATA> Lines;
	bool bmFullHeight;
	CCADIA_GROUPDATA(void)
	{
		bmFullHeight = false;
	}
};

// Attention:
// Element "TimeOrder" in the struct "CCADIA_LINEDATA".
//
// This array will maintain bars sorted by StartTime of each bar.
// You can see it the same way you see an array of index in the array CcaDIA_BARDATA[].
// For example, if there are five bars in the first overlapped group, and their
// overlap level are [3, 0, 4, 1, 2]. So the indexes to the CcaDIA_BARDATA[] sorted by
// the beginning of time should be [1, 3, 4, 0, 2]. You can see that the indexes
// in these two arrays are cross-linked together. Please notice that we define
// the overlap level to be the same number as the order of the bar when sorted
// by StartTime.
//
// However, I keep the relative offset in this array, not the absolute offset.
// The relative offset helps us separate each group of bar more efficiently.
// For example, using the same previous example, the relative offset of index
// to the array CcaDIA_BARDATA[] kept in this array would be [+1, +2, +3, -3, -2].
//
// For a given "i", the bar in the same overlap group will always have the
// same value of "i - Bars[i + TimeOrder[i]].OverlapLevel". This value will be the
// index of the first member in that group.
//
// Notes: This bar order array should be implemented with CCSArray <int>.


/////////////////////////////////////////////////////////////////////////////
// CcaDiagramViewer window

class CcaGantt;

class CcaDiagramViewer: public CViewer
{
	friend CcaGantt;

// Constructions
public:
    CcaDiagramViewer();
    ~CcaDiagramViewer();

	int AdjustBar(CCAFLIGHTDATA *prpAFlight, CCAFLIGHTDATA *prpDFlight,int &ripChartNo);
	int FindFirstBar(long lpUrno,int &ripChartNo);

	void Attach(CWnd *popAttachWnd);
	void ChangeViewTo(const char *pcpViewName, CTime opStartTime, CTime opEndTime);

	void SetDOO( int ipDOO) { imDOO = ipDOO;};

	int GetLastOverlappleverInTimeframe(int ipGroupno, int ipLineno,	CTime opTime1, CTime opTime2, int ipActualOverlaplevel);
	bool GetLoadedAllocations(CMapStringToString& opCicUrnoMap);
	bool GetLoadedCca(CCSPtrArray<DIACCADATA> &opCca, CMapStringToString& opTypMap, bool bpOnlySel, bool bpMustBeInOneDay);

// Internal data processing routines
private:
	void PrepareGrouping();
	void PrepareFilter();
	void PrepareSorter();
	bool IsPassFilter(RecordSet &ropRec);
	bool IsPassFilter(DIACCADATA *prpCca);

	int CompareGroup(CCADIA_GROUPDATA *prpGroup1, CCADIA_GROUPDATA *prpGroup2);
	int CompareLine(CCADIA_LINEDATA *prpLine1, CCADIA_LINEDATA *prpLine2);

	bool MakeGroups();
	bool MakeGroupsOld();
	bool CcaDiagramViewer::MakeGroupAll();
	void MakeBars();
	bool CompressOverlapping(); 

	void MakeLine(CCADIA_GROUPDATA &rrpGroup, RecordSet &ropRec);
//	void MakeLine(CCADIA_GROUPDATA &rrpGroup, RecordSet &ropRec)
	void MakeLineData(CCADIA_GROUPDATA &rrpGroup,CCADIA_LINEDATA *prlLine, RecordSet &ropRot, bool blShowLine = true);
	void CheckLines();
	long OccupyFlightBar( CStringArray* pstrarrHandlingAgentAirlines, CcaDIA_BARDATA *pBar );		// 050323 MVy: enable or disable flight bar depending on handling agent
	long OccupyFlightBars( CStringArray* pstrarrAirlineCodes = 0 );		// 050316 MVy: enable or disable flight bar depending on handling agent
	long GenerateOccupationBars();		// 050316 MVy: generate background bars that symbolize occupation areas
	
	void MakeTriangleColors(CCAFLIGHTDATA *prpFlightA, CCAFLIGHTDATA *prpFlightD, CcaDIA_BARDATA *prpBar);

	void MakeBarData(CcaDIA_BARDATA *prlBar, DIACCADATA *popCca);
	bool MakeBar(int ipGroupno, int ipLineno, DIACCADATA *popCca);

	bool CheckBkBarParameters(int ipGroupno, int ipLineno, int ipBkBarno) const;
	bool CheckBarParameters(int ipGroupno, int ipLineno, int ipBarno) const;
	bool CheckBarParameters(int ipGroupno, int ipLineno) const;

	long GetRkeyFromRotation(CcaCedaFlightData::RKEYLIST *popRotation);
	
	//bool GetFlightsInRotation(CcaCedaFlightData::RKEYLIST *popRotation, CCAFLIGHTDATA *prpFlightA, CCAFLIGHTDATA *prpFlightD);
	CCAFLIGHTDATA * GetFlightAInRotation(CcaCedaFlightData::RKEYLIST *popRotation);
	CCAFLIGHTDATA * GetFlightDInRotation(CcaCedaFlightData::RKEYLIST *popRotation);

	CString GroupText(int ipGroupNo);
	CString LineText(CcaCedaFlightData::RKEYLIST *popRot);
	CString BarText(CcaCedaFlightData::RKEYLIST *popRot);
	CString BarTextAndValues(CcaDIA_BARDATA *prlBar);
	CString StatusText(CcaCedaFlightData::RKEYLIST *popRot);
	int FindGroup(CString opGroupName);
	bool FindLine(CString opBnam, CUIntArray &ropGroups, CUIntArray &ropLines);
	bool FindGroupsForType(CUIntArray &ropGroupNos, CString opType, CString opPfc);
	BOOL FindDutyBar(long lpUrno, int &ripGroupno, int &ripLineno);
	bool FindDutyBars(long lpUrno, CUIntArray &ropGroups, CUIntArray &ropLines);
	BOOL FindBar(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBarno);
	bool FindBarsGlobal(long lpUrno, CUIntArray &ropGroups, 
						CUIntArray &ropLines, CUIntArray &ropBars);

	COLORREF GetTextColor(long DUrno);
	CBrush* GetBarColor(long DUrno);

// Operations
private:
	int omGeometryFontIndex;
	CTimeSpan omGeometryTimeSpan;
	bool bmGeometryTimeLines;
	bool bmGeometryMinimize;
	CTime omStartShowTime;
	int   imGeometrieHours;
	CImageList* omImageListNotValid;
public:

	void SelectBar(const CcaDIA_BARDATA *prpBar, bool bpHighLight = false);
	void SelectBar(long lpUrno, bool bpHighLight = false);
	void DeSelectAll();
    int GetGroupCount();
    CCADIA_GROUPDATA *GetGroup(int ipGroupno);
    CString GetGroupText(int ipGroupno);
    CString GetGroupTopScaleText(int ipGroupno);
	int GetGroupColorIndex(int ipGroupno,CTime opStart,CTime opEnd);
    int GetAllLineCount();
	int GetLineCount(int ipGroupno);
	int GetAbsoluteLine(int ipGroupNo, int ipLineNo);
    CCADIA_LINEDATA *GetLine(int ipGroupno, int ipLineno);
	bool CcaDiagramViewer::GetGroupAndLineNoOfItem(int ipItemID, int &ripGroupNo, int &ripLineNo) const;
	int GetItemID(int ipGroupNo, int ipLineNo) const;
    CString GetLineText(int ipGroupno, int ipLineno);
    int GetMaxOverlapLevel(int ipGroupno, int ipLineno);
	bool IsExpandedLine(int ipGroupno, int ipLineno);
	void SetExpanded(int ipGroupno, int ipLineno);
	int GetVisualMaxOverlapLevel(int ipGroupno, int ipLineno);
    int GetBkBarCount(int ipGroupno, int ipLineno);
    CcaDIA_BKBARDATA *GetBkBar(int ipGroupno, int ipLineno, int ipBkBarno);
		CcaDIA_BKBARDATA *GetBkBar( POINT pt );		// 050321 MVy: iterate ALL backbars and return the one the specified point is within the previously calculated bounds
    CString GetBkBarText(int ipGroupno, int ipLineno, int ipBkBarno);
    int GetBarCount(int ipGroupno, int ipLineno);
    CcaDIA_BARDATA *GetBar(int ipGroupno, int ipLineno, int ipBarno);
    CString GetBarText(int ipGroupno, int ipLineno, int ipBarno);
    CString GetStatusBarText(int ipGroupno, int ipLineno, int ipBarno);
	int GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno);
	CcaDIA_INDICATORDATA *GetIndicator(int ipGroupno, int ipLineno, int ipBarno,
		int ipIndicatorno);

    void DeleteAll();
    int CreateGroup(CCADIA_GROUPDATA *prpGroup);
    void DeleteGroup(int ipGroupno);
	int CreateLine(CCADIA_GROUPDATA &rrpGroup, CCADIA_LINEDATA *prpLine);
    void DeleteLine(int ipGroupno, int ipLineno);
    int CreateBkBar(int ipGroupno, int ipLineno, CcaDIA_BKBARDATA *prpBkBar);
    void DeleteBkBar(int ipGroupno, int ipLineno, int ipBkBarno);
    int CreateBar(int ipGroupno, int ipLineno, CcaDIA_BARDATA *prpBar, BOOL bpFrontBar = TRUE);
    void DeleteBar(int ipGroupno, int ipLineno, int ipBarno, bool bpSetOverlapColor = true);
    int CreateIndicator(int ipGroupno, int ipLineno, int ipBarno,
    	CcaDIA_INDICATORDATA *prpIndicator);
	void DeleteIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicator);

    int GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2,
        int ipOverlapLevel1, int ipOverlapLevel2);
    int GetBkBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2);
	void AllowUpdates(BOOL bpNoUpdatesNow);
	int GetGeometryFontIndex(){return omGeometryFontIndex;}
	CTimeSpan GetGeometryTimeSpan(){return omGeometryTimeSpan;}
	bool GetGeometryTimeLines(){return bmGeometryTimeLines;}
	bool GetGeometryMinimize(){return bmGeometryMinimize;}
	CTime GetGeometrieStartTime(){return omStartShowTime;}
	void MakeMasstab();

	void UtcToLocal(CcaDIA_BKBARDATA *prlBar);
	void UtcToLocal(CcaDIA_BARDATA *prlBar);

	void SetCnams( CString opCnams ) { omCnams = opCnams;};

	void SetLoadTimeFrame(CTime opStartDate, CTime opEndDate);

	bool GetDispoZeitraumFromTo(CTime &ropFrom, CTime &ropTo);

	bool GetStartAllocTime(DIACCADATA *popCca, CCAFLIGHTDATA *prlDFlight, CTime &ropStartTime);
	bool GetEndAllocTime(DIACCADATA *popCca, CCAFLIGHTDATA *prlDFlight, CTime &ropEndTime);
	bool GetFullHeight(int ipGroupNo, int ipLineNo) const;


// Private helper routines
private:
	void GetOverlappedBarsFromTime(int ipGroupno, int ipLineno,
		CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList);
//	void GetGroupsFromViewer(CCSPtrArray<CCADIA_GROUPDATA> &ropGroups);
	bool CcaDiagramViewer::GetGroupsFromViewer(CStringArray &ropGroups);

	void PrintPrepareLineData(int ipGroupNo,int ipLineno,CCSPtrArray<PRINTBARDATA> &ropPrintLine);
	void PrintDiaArea(CcaChart *popChart);

	void CcaDiagramViewer::PrintDiagramHeader();
// Attributes used for filtering condition
public:
	int omGroupBy;			// enumerated value -- define in "stviewer.cpp"
	CStringArray omSortOrder;
	CTime omStartTime;
	CTime omEndTime;
	CTime omLoadStartTime;
	CTime omLoadEndTime;
// Attributes
private:
	CWnd *pomAttachWnd;
	CBrush omBkBrush;
	CBrush omBkBrush2;
	CBrush omBreakBrush;
	CBrush omBkBreakBrush;
	CBrush omWIFBkBrush;
	int	igFirstGroupOnPage;

	CString omCnams;
	int imDOO;

// Methods which handle changes (from Data Distributor)
public:
   // CCSPtrArray<CCADIA_GROUPDATA> omGroups;

	
/*	
	void ProcessFlightInsert(CUIntArray *popRkeys);
	void ProcessFlightChange(CCAFLIGHTDATA *prpFlight);
	void ProcessFlightDelete(CCAFLIGHTDATA *prpFlight);
*/

// MCU 22.09.98	void ProcessCcaChange(long *prpCcaUrno);
	void ProcessCcaBarUpdate(long *prpCcaUrno,int ipDDXType);

	void ProcessCcaChange(long *prpCcaUrno,int ipDDXType);

	void ProcessBlkChange( RecordSet * popBlkRecord );
	void ProcessCicChange( RecordSet * popBlkRecord );
	void ProcessKonfliktChange(long lpUrno);

public:
	BOOL bmNoUpdatesNow;
	BOOL bmIsFirstGroupOnPage;

//Printing routines
	void PrintGantt(CcaChart *popChart, bool bpNewPrint = true);
	void CcaDiagramViewer::PrintGanttEnd();
	


///////////////////////////////////////////////////////////////////////////////////////////
// Printing routines
private:
	CCSPrint *pomPrint;
    CCSPtrArray<CCADIA_GROUPDATA> omGroups;
	void PrintSingleLine(int ipGroupNo, int ipLineNo);

public:
	CcaDIA_BARDATA* Test_FindFirstDisabled();

};

/////////////////////////////////////////////////////////////////////////////

#endif
