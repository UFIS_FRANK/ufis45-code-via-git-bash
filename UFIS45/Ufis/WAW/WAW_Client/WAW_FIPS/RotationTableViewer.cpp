// RotationTableViewer.cpp : implementation file
//

#include <stdafx.h>
#include <FPMS.h>
#include <CCSGlobl.h>
#include <RotationTableViewer.h>
#include <RotationTableChart.h>
#include <CCSTable.h>
#include <PrivList.h>
#include <resrc1.h>
#include <utils.h>
#include <RotationTables.h>		// 050302 MVy: GenerateTableName needs access to the shown tables in dialog to fetch the selected items
#include <CedaFpeData.h>
#include <CedaVipData.h>

#include <iostream.h>
#include <fstream.h>

//#include <sstream>		// 050225 MVy: using streams in creating output strings
//#include <string>
//using namespace std ;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void RotationFlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
// RotationTableViewer


RotationTableViewer::RotationTableViewer()
{
	m_dblPrintFactor = 1 ;		// 050309 MVy: for automatic calculation

	pomDefaultFont = &ogSetupFont;
	pomUserFont = NULL;
	pomFont = pomDefaultFont;

	bmAutoNow = false;
	if(ogFpeData.bmFlightPermitsEnabled)
	{
		if (bgAddKeyCodesDS)
		{
		omArrFields = "FLPE;FLNO;CSGN;VIAL;TTYP;REGN;ACT5;CHGI;ISRE;REMP;ORIG;IFRA;STEV;STE2,STE3,STE4;STOA;TMOA;ETOA;LAND;RWYA;ONBE;ONBL;PSTA;GTA1";
		omDepFields = "FLPE;FLNO;CSGN;VIAL;TTYP;REGN;ACT5;CHGI;ISRE;REMP;DEST;IFRD;STEV;STE2,STE3,STE4;STOD;ETOD;OFBL;RWYD;AIRB;PSTD;GTD1;GTD2;ETDC";
		} else
		{
		omArrFields = "FLPE;FLNO;CSGN;VIAL;TTYP;REGN;ACT5;CHGI;ISRE;REMP;ORIG;IFRA;STOA;TMOA;ETOA;LAND;RWYA;ONBE;ONBL;PSTA;GTA1";
		omDepFields = "FLPE;FLNO;CSGN;VIAL;TTYP;REGN;ACT5;CHGI;ISRE;REMP;DEST;IFRD;STOD;ETOD;OFBL;RWYD;AIRB;PSTD;GTD1;GTD2;ETDC";
		}
	}
	else
	{
		if (bgAddKeyCodesDS)
		{
		omArrFields = "FLNO;CSGN;VIAL;TTYP;REGN;ACT5;CHGI;ISRE;REMP;ORIG;IFRA;STEV;STE2,STE3,STE4;STOA;TMOA;ETOA;LAND;RWYA;ONBE;ONBL;PSTA;GTA1";
		omDepFields = "FLNO;CSGN;VIAL;TTYP;REGN;ACT5;CHGI;ISRE;REMP;DEST;IFRD;STEV;STE2,STE3,STE4;STOD;ETOD;OFBL;RWYD;AIRB;PSTD;GTD1;GTD2;ETDC";
		} else
		{
		omArrFields = "FLNO;CSGN;VIAL;TTYP;REGN;ACT5;CHGI;ISRE;REMP;ORIG;IFRA;STOA;TMOA;ETOA;LAND;RWYA;ONBE;ONBL;PSTA;GTA1";
		omDepFields = "FLNO;CSGN;VIAL;TTYP;REGN;ACT5;CHGI;ISRE;REMP;DEST;IFRD;STOD;ETOD;OFBL;RWYD;AIRB;PSTD;GTD1;GTD2;ETDC";
		}
	}

	if(bgShowCashComment)
	{
		omArrFields+=";PCOM";
		omDepFields+=";PCOM";
	}

	ogDdx.Register(this, TABLE_FONT_CHANGED, CString("RotationTableViewer"), CString("Flight Update/new"), RotationFlightTableCf);
	ogDdx.Register(this, R_FLIGHT_CHANGE, CString("RotationTableViewer"), CString("Flight Update/new"), RotationFlightTableCf);
    ogDdx.Register(this, R_FLIGHT_DELETE, CString("RotationTableViewer"), CString("Flight Update/new"), RotationFlightTableCf);
    ogDdx.Register(this, DATA_RELOAD, CString("RotationTableViewer"), CString("Flight Update/new"), RotationFlightTableCf);
    ogDdx.Register(this, FPE_INSERT, CString("RotationTableViewer"), CString("Flight Permit Insert"), RotationFlightTableCf);
    ogDdx.Register(this, FPE_UPDATE, CString("RotationTableViewer"), CString("Flight Permit Update"), RotationFlightTableCf);
    ogDdx.Register(this, FPE_DELETE, CString("RotationTableViewer"), CString("Flight Permit Delete"), RotationFlightTableCf);
}

RotationTableViewer::~RotationTableViewer()
{
	ogDdx.UnRegister(this,NOTUSED);

	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		GROUP_DATA *prlGroupData = &omGroups[i];
		
		//prlGroupData->Table->ResetContent();
		prlGroupData->Lines.DeleteAll();
	}
	
	omGroups.DeleteAll();

	if(pomUserFont != NULL)
	{
		pomUserFont->DeleteObject();
		delete pomUserFont;
	}
}


void RotationTableViewer::SetUserFont(LPLOGFONT lpLogFont)
{

	pomUserFont = new CFont;

	pomUserFont->CreateFontIndirect(lpLogFont); 

	pomFont = pomUserFont;



	DeleteAll();	
	MakeLines();
	ShowAllTables();
	
/*	
	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		omGroups[i].Table->DisplayTable();
	}
*/

}


void RotationTableViewer::Repaint()
{
	DeleteAll();	
	MakeLines();
	ShowAllTables();
}






void RotationTableViewer::AddGroup(int ipGroupID,  CString opGroupText, CCSTable *popTable, RotationTableChart *popChart,  CCS3DStatic *pomTopScaleText)
{
	GROUP_DATA *prlGroupData = new GROUP_DATA;

	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		if(omGroups[i].GroupID == ipGroupID)
			omGroups.DeleteAt(i);
	}
	prlGroupData->GroupID		= ipGroupID;
	prlGroupData->GroupText		= opGroupText;
	prlGroupData->Table			= popTable;
	prlGroupData->Chart			= popChart;
	prlGroupData->TopScaleText	= pomTopScaleText;
	/*
	TRACE("\n----------------------------------------------------------------------------------------------------------");
	TRACE("\nAddGroup: ID:<%d> <%s> Table:<%p> Chart:<%p>", ipGroupID, opGroupText, popTable, popChart );
	TRACE("\n----------------------------------------------------------------------------------------------------------");
	*/
	omGroups.Add(prlGroupData);
}



void RotationTableViewer::DeleteAll()
{
	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		GROUP_DATA *prlGroupData = &omGroups[i];
		
		prlGroupData->Table->ResetContent();
		prlGroupData->Lines.DeleteAll();
	}
}



void RotationTableViewer::ChangeViewTo(const char *pcpViewName)
{

	if(strcmp(pcpViewName, "") != 0)
		SelectView(pcpViewName);

	CString olWhere;

	bool blRotation;
	bool blRead = true;

	blRead = GetFlightWhereString(olWhere, blRotation);

	SetTableSort();

	CTime olFrom;
	CTime olTo;
	CString olFtyps;


	if(strcmp(pcpViewName, "<Default>") == 0)
		olWhere = "";
	
	GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
	ogRotationFlights.SetPreSelection(olFrom, olTo, olFtyps);

	

	if (blRead)
		ogRotationFlights.ReadAllFlights(olWhere, blRotation);


	DeleteAll();	
	MakeLines();
	ShowAllTables();

	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		SetTopScaleText(omGroups[i].GroupID);
	}



}










void RotationTableViewer::SetTableSort()
{
	CStringArray olSpezSort;
	CStringArray olUniSort;

	omSort.RemoveAll();

	GetSort("FLIGHTSORT", olSpezSort);
	GetSort("UNISORT", olUniSort);


	//////////////////////////////////////////////////////
	// Sortarray  zusammenbasteln

	CString olTmp("|");

	for(int i = 0; i < olSpezSort.GetSize(); i ++)
	{

		if(olSpezSort[i] == "Flugnummer")
		{
			olTmp = olTmp + CString("FLNO") + CString("|") + CString("DFLNO") + CString("|");		
			omSort.Add("AFLNO+");
			omSort.Add("DFLNO+");
		}
		if(olSpezSort[i] == "A/C-Type")
		{
			olTmp = olTmp + CString("ACT5") + CString("|");
			omSort.Add("ACT5+");
		}
		if(olSpezSort[i] == "Origin")
		{
			olTmp = olTmp + CString("ORG3") + CString("|");
			omSort.Add("AORG3+");
		}
		if(olSpezSort[i] == "Destination")
		{
			olTmp = olTmp + CString("DES3") + CString("|");
			omSort.Add("DDES3+");
		}
		if(olSpezSort[i] == "Ankunftzeit")
		{
			olTmp = olTmp + CString("STOA") + CString("|");
			omSort.Add("ASTOA+");
		}
		if(olSpezSort[i] == "Abflugzeit")
		{
			olTmp = olTmp + CString("STOD") + CString("|");
			omSort.Add("DSTOD+");
		}
	}
	
	bmDepField = false;
	bmArrField = false;

	for( i = 0; i < olUniSort.GetSize(); i ++)
	{
		CString ol = olUniSort[i];
		if(olTmp.Find(olUniSort[i].Left(5)) < 0)
		{
			olUniSort[i].TrimRight();
			olUniSort[i].TrimLeft();
			if (!olUniSort[i].IsEmpty())
			{
				omSort.Add(olUniSort[i]);
				if (omDepFields.Find(omSort[i].Left(4)) != -1)
					bmDepField = true;
				if (omArrFields.Find(omSort[i].Left(4)) != -1)
					bmArrField = true;
			}
		}
	}

/*
	TRACE("\n---------------------");
	for( i = 0; i < omSort.GetSize(); i ++)
	{
		TRACE("\nSORT: <%d> Field<%s>  ", i, omSort[i]);
	}
	TRACE("\n---------------------");
*/
	//
	//////////////////////////////////////////////////////
}




GROUP_DATA *RotationTableViewer::GetGroupData(int ipGroupID)
{
	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		if(ipGroupID == omGroups[i].GroupID)
			return &omGroups[i];
	}
	return NULL;
}



void RotationTableViewer::ShowAllTables()
{
	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		GROUP_DATA *prlGroupData = &omGroups[i];
		
		ShowTable(prlGroupData->GroupID);
	}

	UpdateWindowTitle();		// 050224 MVy: put the current counts of flights / arrivals / departures into the dialog title
}


void RotationTableViewer::ShowTable(int ipGroupID)
{
	ShowTableHeader(ipGroupID);
	ShowTableLines(ipGroupID);
}



void RotationTableViewer::ShowTableHeader(int ipGroupID)
{
	GROUP_DATA *prlGroupData = GetGroupData(ipGroupID);

	if(prlGroupData == NULL)
		return;

	CCSTable *prlTable = prlGroupData->Table; 

	prlTable->ResetContent();
	
	
	CCSPtrArray <TABLE_HEADER_COLUMN> olHeaderDataArray;

	prlTable->SetShowSelection(true);
	prlTable->ResetContent();
	prlTable->SetDefaultSeparator();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Font = pomFont;

	if((ipGroupID == UD_ARRIVAL) || (ipGroupID == UD_AGROUND))
	{
		if(ogFpeData.bmFlightPermitsEnabled)
		{
			rlHeader.Length = 15;	
			rlHeader.AnzChar = 9;	
			rlHeader.Text = "";
			olHeaderDataArray.New(rlHeader);
		}

		rlHeader.Length = 70;	
		rlHeader.AnzChar = 9;	
		rlHeader.String = "MWW88888X";	
		rlHeader.Text = GetString(IDS_STRING296);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 60;	
		rlHeader.AnzChar = 9;	
		rlHeader.Text = GetString(IDS_STRING297);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 35;	
		rlHeader.AnzChar = 4;	
		rlHeader.String = "MMWW";	
		rlHeader.Text = GetString(IDS_STRING298);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 35;	
		rlHeader.AnzChar = 4;	
		rlHeader.Text = GetString(IDS_STRING299);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 12;	
		rlHeader.AnzChar = 1;	
		rlHeader.String = "8";	
		rlHeader.Text = CString("");
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 13;	
		rlHeader.AnzChar = 1;	
		rlHeader.String = "V";	
		rlHeader.Text = GetString(IDS_STRING300);
		olHeaderDataArray.New(rlHeader);

		rlHeader.String = "88888";	
		rlHeader.Text = GetString(IDS_STRING301);
		olHeaderDataArray.New(rlHeader);
		
		if (bgAddKeyCodesDS) {
			rlHeader.Length = 40;	
			rlHeader.AnzChar = 3;	
			rlHeader.String = "XXX";	
			rlHeader.Text = GetString(IDS_KEYCODE1);
			olHeaderDataArray.New(rlHeader);
			
			rlHeader.Length = 40;	
			rlHeader.AnzChar = 3;	
			rlHeader.String = "XXX";	
			rlHeader.Text = GetString(IDS_KEYCODE2);
			olHeaderDataArray.New(rlHeader);
			rlHeader.Length = 40;	
			rlHeader.AnzChar = 3;	
			rlHeader.String = "XXX";	
			rlHeader.Text = GetString(IDS_KEYCODE3);
			olHeaderDataArray.New(rlHeader);
			rlHeader.Length = 40;	
			rlHeader.AnzChar = 3;	
			rlHeader.String = "XXX";	
			rlHeader.Text = GetString(IDS_KEYCODE4);
			olHeaderDataArray.New(rlHeader);
			
		}
		rlHeader.Length = 40;	
		rlHeader.AnzChar = 5;	
		rlHeader.String = "88:88";	
		rlHeader.Text = GetString(IDS_STRING323);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 40;	
		rlHeader.String = "88:88";	
		rlHeader.Text = GetString(IDS_STRING302);		// "ETA"
		olHeaderDataArray.New(rlHeader);

		// Shanghai want to see ETOA (ETA Fids)
		if (strcmp(pcgHome, "PVG") == 0 ||
			__CanSeeDailyScheduleColumnsDisplay() )		// 050308 MVy: daily schedule lists should contain additional column for display data: ETA(D) and ETD(D)
		{
			rlHeader.Length = 40;	
			rlHeader.String = "88:88";	

			if(__CanUseFIDSHeader())
			{
				rlHeader.Text = GetString(IDS_STRING2909);		// "ETA(FDIS)"
			}
			else
			{
				rlHeader.Text = GetString(IDS_STRING2012);		// "ETA(D)"
			}
			olHeaderDataArray.New(rlHeader);
		}

		rlHeader.Length = 40;	
		rlHeader.String = "88:88";	
		rlHeader.Text = GetString(IDS_STRING303);		// "TMO"
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 40;	
		rlHeader.String = "88:88";	
		rlHeader.Text = GetString(IDS_STRING304);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 25;	
		rlHeader.AnzChar = 3;	
		rlHeader.String = "88W";	
		rlHeader.Text = GetString(IDS_STRING305);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 40;	
		rlHeader.AnzChar = 5;	
		rlHeader.String = "88:88";	
		rlHeader.Text = GetString(IDS_STRING306);
		olHeaderDataArray.New(rlHeader);
		
		rlHeader.Length = 40;		
		rlHeader.String = "88:88";	
		rlHeader.Text = GetString(IDS_STRING307);
		olHeaderDataArray.New(rlHeader);
		
		// Shanghai want to see  AIRB (ATD of Origin)
		if (strcmp(pcgHome, "PVG") == 0)
		{
			rlHeader.Length = 40;	
			rlHeader.String = "88:88";	
			rlHeader.Text = GetString(IDS_STRING321);
			olHeaderDataArray.New(rlHeader);
		}


		rlHeader.Length = 39;	
		rlHeader.AnzChar = 3;	
		rlHeader.String = "88W";	
		rlHeader.Text = GetString(IDS_STRING308);
		olHeaderDataArray.New(rlHeader);
		
		rlHeader.Length = 39;	
		rlHeader.AnzChar = 3;	
		rlHeader.String = "88W";	
		rlHeader.Text = GetString(IDS_STRING309);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 39;	
		rlHeader.AnzChar = 3;	
		rlHeader.String = "88/88";	
		rlHeader.Text = GetString(IDS_STRING1151);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 80;	 
		rlHeader.AnzChar = 9;	
		rlHeader.String = "MMMMMMMMM";	
		rlHeader.Text = GetString(IDS_STRING310);
		olHeaderDataArray.New(rlHeader);
		
		rlHeader.Length = 39;	
		rlHeader.AnzChar = 5;	
		rlHeader.String = "WWW88";	
		rlHeader.Text = GetString(IDS_STRING311);
		olHeaderDataArray.New(rlHeader);

		if(ipGroupID == UD_ARRIVAL)
		{
			rlHeader.Length = 58;	
			rlHeader.Text = GetString(IDS_STRING324);
			olHeaderDataArray.New(rlHeader);
		}
		else
		{
			rlHeader.Length = 58;	
			rlHeader.Text = GetString(IDS_STRING1394);
			olHeaderDataArray.New(rlHeader);
		}

		rlHeader.Length = 13;	 
		rlHeader.AnzChar = 1;	
		rlHeader.String = "+";	
		rlHeader.Text = GetString(IDS_STRING313);
		olHeaderDataArray.New(rlHeader);

		if(bgDailyShowVIP)
		{
			rlHeader.Length = 13;	 
			rlHeader.AnzChar = 1;	
			rlHeader.String = "+";	
			rlHeader.Text = GetString(IDS_STRING2946);
			olHeaderDataArray.New(rlHeader);
		}
		
		rlHeader.Length = 40;//102;	 
		rlHeader.AnzChar = 4;	
		rlHeader.String = "MMMM";	
		rlHeader.Text = GetString(IDS_STRING314);
		olHeaderDataArray.New(rlHeader);
		

	
		
		rlHeader.Length = 100;	 
//		rlHeader.AnzChar = 4;	
//		rlHeader.String = "MMMM";	
		rlHeader.Text = GetString(IDS_STRING2120);
		olHeaderDataArray.New(rlHeader);


		if(bgAdditionalRemark)
		{
			//Rem. 2 ARR Daily
			rlHeader.Length = 100;	 
			rlHeader.Text = GetString(IDS_STRING2925);
			olHeaderDataArray.New(rlHeader);
		}

		rlHeader.Length = 100;	 
//		rlHeader.AnzChar = 4;	
//		rlHeader.String = "MMMM";	
		rlHeader.Text = GetString(IDS_STRING2442);
		olHeaderDataArray.New(rlHeader);

	}

	if((ipGroupID == UD_DEPARTURE) || (ipGroupID == UD_DGROUND))
	{
		if(ogFpeData.bmFlightPermitsEnabled)
		{
			rlHeader.Length = 15;	
			rlHeader.AnzChar = 9;	
			rlHeader.Text = "";
			olHeaderDataArray.New(rlHeader);
		}

		rlHeader.Length = 70; 
		rlHeader.AnzChar = 9;	
		rlHeader.String = "MWW88888X";	
		rlHeader.Text = GetString(IDS_STRING296);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 60; 
		rlHeader.AnzChar = 9;	
		rlHeader.Text = GetString(IDS_STRING297);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 35; 
		rlHeader.AnzChar = 4;	
		rlHeader.String = "MMWW";	
		rlHeader.Text = GetString(IDS_STRING315);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 35; 
		rlHeader.AnzChar = 4;	
		rlHeader.Text = GetString(IDS_STRING299);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 12;	
		rlHeader.AnzChar = 1;	
		rlHeader.String = "8";	
		rlHeader.Text = CString("");
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 13; 
		rlHeader.AnzChar = 1;	
		rlHeader.String = "V";	
		rlHeader.Text = GetString(IDS_STRING300);
		olHeaderDataArray.New(rlHeader);
		

		rlHeader.String = "88888";	
		rlHeader.Text = GetString(IDS_STRING301);
		olHeaderDataArray.New(rlHeader);

				if (bgAddKeyCodesDS) {
			rlHeader.Length = 40;	
			rlHeader.AnzChar = 3;	
			rlHeader.String = "XXX";	
			rlHeader.Text = GetString(IDS_KEYCODE1);
			olHeaderDataArray.New(rlHeader);
			
			rlHeader.Length = 40;	
			rlHeader.AnzChar = 3;	
			rlHeader.String = "XXX";	
			rlHeader.Text = GetString(IDS_KEYCODE2);
			olHeaderDataArray.New(rlHeader);
			rlHeader.Length = 40;	
			rlHeader.AnzChar = 3;	
			rlHeader.String = "XXX";	
			rlHeader.Text = GetString(IDS_KEYCODE3);
			olHeaderDataArray.New(rlHeader);
			rlHeader.Length = 40;	
			rlHeader.AnzChar = 3;	
			rlHeader.String = "XXX";	
			rlHeader.Text = GetString(IDS_KEYCODE4);
			olHeaderDataArray.New(rlHeader);
			
		}

		rlHeader.Length = 37; 
		rlHeader.AnzChar = 5;	
		rlHeader.String = "88:88";	
		rlHeader.Text = GetString(IDS_STRING316);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 40; 
		rlHeader.Text = GetString(IDS_STRING317);		// "ETD"
		olHeaderDataArray.New(rlHeader);

		// Shanghai want to see ETOD (ETD Fids)
		if (strcmp(pcgHome, "PVG") == 0 ||
			__CanSeeDailyScheduleColumnsDisplay() )		// 050308 MVy: daily schedule lists should contain additional column for display data: ETA(D) and ETD(D)
		{
			rlHeader.Length = 40;	
			if(__CanUseFIDSHeader())
			{
				rlHeader.Text = GetString(IDS_STRING2910);		// "ETD(FIDS)"
			}
			else
			{
				rlHeader.Text = GetString(IDS_STRING2013);		// "ETD(D)"
			}
			olHeaderDataArray.New(rlHeader);
		}



		if(bgCDMPhase1)
		{
			// Showing TOBT instead of COB -- Lisbon CDM
			rlHeader.Length = 45; 
			rlHeader.Text = GetString(IDS_STRING2880);		// "TOBT"
		}
		else
		{
			rlHeader.Length = 40; 
			rlHeader.Text = GetString(IDS_STRING318);		// "COB"
		}

		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 40; 
		rlHeader.Text = GetString(IDS_STRING319);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 40; 
		rlHeader.Text = GetString(IDS_STRING320);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 25; 
		rlHeader.AnzChar = 3;	
		rlHeader.String = "88W";	
		rlHeader.Text = GetString(IDS_STRING305);
		olHeaderDataArray.New(rlHeader);
		
		rlHeader.Length = 40; 
		rlHeader.AnzChar = 5;	
		rlHeader.String = "88:88";	
		rlHeader.Text = GetString(IDS_STRING321);
		olHeaderDataArray.New(rlHeader);
		
		rlHeader.Length = 37; 
		rlHeader.AnzChar = 3;	
		rlHeader.String = "88W";	
		rlHeader.Text = GetString(IDS_STRING308);
		olHeaderDataArray.New(rlHeader);
		
		rlHeader.Length = 37; 
		rlHeader.AnzChar = 3;	
		rlHeader.String = "88W";	
		rlHeader.Text = GetString(IDS_STRING2014);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 37; 
		rlHeader.AnzChar = 3;	
		rlHeader.String = "88W";	
		rlHeader.Text = GetString(IDS_STRING2015);
		olHeaderDataArray.New(rlHeader);
		
		if(bgSecondGateDemandFlag)
		{
				rlHeader.AnzChar = 15;	
				rlHeader.String = "000-000-000-000";	
		}
		else
		{
			//rlHeader.Length = 37; 
			if (bgCnamAtr)
			{
				rlHeader.AnzChar = 20;	
				rlHeader.String = "0000-00000-0000-00000";	
			}
			else
			{
				rlHeader.AnzChar = 7;	
				rlHeader.String = "000-000"; 	
			}
		}

		rlHeader.Text = GetString(IDS_STRING1508); // CKI
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 80; 
		rlHeader.AnzChar = 9;	
		rlHeader.String = "MMMMMMMMM";	
		rlHeader.Text = GetString(IDS_STRING310);
		olHeaderDataArray.New(rlHeader);
		
		rlHeader.Length = 40;
		rlHeader.AnzChar = 5;	
		rlHeader.String = "WWW88";	
		rlHeader.Text = GetString(IDS_STRING311);
		olHeaderDataArray.New(rlHeader);

		if(ipGroupID == UD_DEPARTURE)
		{
			rlHeader.Length = 58;	
			rlHeader.Text = GetString(IDS_STRING1396);
			olHeaderDataArray.New(rlHeader);
		}
		else
		{
			rlHeader.Length = 58;	
			rlHeader.Text = GetString(IDS_STRING1395);
			olHeaderDataArray.New(rlHeader);
		}
		rlHeader.Length = 13;
		rlHeader.AnzChar = 1;	
		rlHeader.String = "+";	
		rlHeader.Text = GetString(IDS_STRING313);
		olHeaderDataArray.New(rlHeader);

		if(bgDailyShowVIP)
		{
			rlHeader.Length = 13;
			rlHeader.AnzChar = 1;	
			rlHeader.String = "+";	
			rlHeader.Text = GetString(IDS_STRING2946);
			olHeaderDataArray.New(rlHeader);
		}

		rlHeader.Length = 40;
		rlHeader.AnzChar = 4;	
		rlHeader.String = "MMMM";	
		rlHeader.Text = GetString(IDS_STRING314);
		olHeaderDataArray.New(rlHeader);


		
		rlHeader.Length = 100;	 
//		rlHeader.AnzChar = 4;	
//		rlHeader.String = "MMMM";	
		rlHeader.Text = GetString(IDS_STRING2122);
		olHeaderDataArray.New(rlHeader);

		if(bgAdditionalRemark)
		{
			//Rem. 2 DEP Daily
			rlHeader.Length = 100;	 
			rlHeader.Text = GetString(IDS_STRING2926);
			olHeaderDataArray.New(rlHeader);
		}

		rlHeader.Length = 100;	 
//		rlHeader.AnzChar = 4;	
//		rlHeader.String = "MMMM";	
		rlHeader.Text = GetString(IDS_STRING2121);
		olHeaderDataArray.New(rlHeader);
	}
	prlTable->SetHeaderFields(olHeaderDataArray);
	olHeaderDataArray.DeleteAll();

}


void RotationTableViewer::ShowTableLines(int ipGroupID)
{

	GROUP_DATA *prlGroupData = GetGroupData(ipGroupID);

	if(prlGroupData == NULL)
		return;
    
	
	int ilCount = prlGroupData->Lines.GetSize();
	for (int ilLc = 0; ilLc < ilCount; ilLc++)
	{
		ShowTableLine(ipGroupID, ilLc, ADD_LINE, true);
	}

	//ShowTableLine(ipGroupID, popTable, 0, ADD_LINE);
 
	prlGroupData->Table->DisplayTable();
}

bool RotationTableViewer::GetFieldEditable(ROTATIONTABLE_LINEDATA* prpLine, CString& opStrFieldName)
{
	if(ogPrivList.GetStat (opStrFieldName) != '1')
		return false;	
	else
	{
		if (CheckPostFlight(prpLine))
			return false;	
		else
			return true;	
	}

	return false;
}


bool RotationTableViewer::ShowTableLine(int ipGroupID, int ipLineNo, int ipModus, bool bpReadAll)
{
	GROUP_DATA *prlGroupData = GetGroupData(ipGroupID);

	if(prlGroupData == NULL)
		return false;

	CCSTable *prlTable = prlGroupData->Table; 

		
	int ilTopIndex  = -1;

	if(!bpReadAll)
	{
		ilTopIndex = prlTable->pomListBox->GetTopIndex();
	}

	if((ipModus == CHANGE_LINE) && !((ipLineNo >= 0) && (ipLineNo < prlGroupData->Lines.GetSize())))
		return false;

	ROTATIONTABLE_LINEDATA *prlLine = 	&prlGroupData->Lines[ipLineNo]; 
	
	if(prlLine == NULL)
		return false;

	CString olTmp;

	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	//rlColumnData.Font = pomFont;

	rlColumnData.Font = pomFont;
	CString olSecId;

	CCSEDIT_ATTRIB rlAttrib;
	rlAttrib.Style = ES_UPPERCASE | ES_WANTRETURN;
	//rlAttrib.ExStyle = WS_EX_STATICEDGE | WS_EX_CLIENTEDGE;

	if((ipGroupID == UD_ARRIVAL) || (ipGroupID == UD_AGROUND))
	{
		if ((strcmp(pcgHome, "ATH") == 0))
		{
			if((strcmp(prlLine->Ftyp, "X") == 0) || (strcmp(prlLine->Ftyp, "Z") == 0) || (strcmp(prlLine->Ftyp, "B") == 0))
				rlColumnData.TextColor = RED;
		}
		//textcolour for return taxt shall be black
		if((strcmp(prlLine->Ftyp, "X") == 0))// || (strcmp(prlLine->Ftyp, "B") == 0))
			rlColumnData.TextColor = RED;

		if(strcmp(prlLine->Ftyp, "N") == 0)
			rlColumnData.TextColor = ORANGE;


		rlColumnData.BkColor = WHITE;

//backcolour for return flights
		if( (strcmp(prlLine->Ftyp, "Z") == 0) && (strcmp(pcgHome, "ATH") != 0) )
			rlColumnData.BkColor = RED;

		if((prlLine->Tmoa != TIMENULL) && (prlLine->Land == TIMENULL))
			rlColumnData.BkColor = YELLOW;


		if( (prlLine->Ftyp == "T") || (prlLine->Ftyp == "G") )
		{

			if((prlLine->Onbl == TIMENULL) && (prlLine->Ofbl != TIMENULL))
//			if(prlLine->Ofbl != TIMENULL)
				rlColumnData.BkColor = LIME;
		}
		else
		{
			if((prlLine->Onbl == TIMENULL) && (prlLine->Land != TIMENULL))
			{
				if (strcmp(pcgHome, "DXB") == 0)
				{
					rlColumnData.BkColor = LIGHTBLUE;
				}
				else
					rlColumnData.BkColor = LIME;
			}
		}

		// If requested, paint white lines of arrival flights with an !ATD! light blue (aqua)
		if (bgCreateDaily_OrgAtdBlue && CString("TGZB").Find(prlLine->Ftyp) == -1 
			&& rlColumnData.BkColor == WHITE && prlLine->Airb != TIMENULL && prlLine->Onbl == TIMENULL)
		{
			rlColumnData.BkColor = AQUA;
		}


		if(ipGroupID == UD_ARRIVAL)
			olSecId = CString("ROTATIONTAB_A_");
		else
			olSecId = CString("ROTATIONTAB_AG_");

		if(ogFpeData.bmFlightPermitsEnabled)
		{
			rlColumnData.blIsEditable = false;
			rlColumnData.Alignment = COLALIGN_CENTER;
			rlColumnData.Text =  "";
			COLORREF olOldColour = rlColumnData.BkColor;
			rlColumnData.BkColor = prlLine->FlightPermitColour;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.BkColor = olOldColour;
		}

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("FLNO"));
/*
rkr		if(ogPrivList.GetStat( olSecId + CString("FLNO")) != '1')
			rlColumnData.blIsEditable = false;	
		else
			rlColumnData.blIsEditable = true;	
*/
		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXXXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 9;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
	
		if(ogPrivList.GetStat( olSecId + CString("FLNO")) != '-')
			olTmp = CString(prlLine->Flno);
		olTmp.TrimRight();
		rlColumnData.Text =	olTmp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("CSGN"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	


		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 8;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("CSGN")) != '-')
			rlColumnData.Text = CString(prlLine->Csgn);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.TextColor = BLACK;

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("ORG4"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G") || (prlLine->Ftyp == "B") || (prlLine->Ftyp == "Z"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 4;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;

		if(ogPrivList.GetStat( olSecId + CString("ORG4")) != '-')
			rlColumnData.Text = CString(prlLine->Org4);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		
		/*
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("VIAL"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
		*/
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXXX";		// 050311 MVy: width increased from 4 to 6 chars
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 6;		// 050311 MVy: width increased from 4 to 6 chars
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if((strcmp(prlLine->Ftyp, "Z") == 0) || (strcmp(prlLine->Ftyp, "B") == 0))
		{
			rlColumnData.blIsEditable = false;	
			rlColumnData.Text =  CString("");
		}
		else
		{
			if(ogPrivList.GetStat( olSecId + CString("VIAL")) != '-')
				rlColumnData.Text =  CString(prlLine->Vial);
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		if((strcmp(prlLine->Ftyp, "Z") == 0) || (strcmp(prlLine->Ftyp, "B") == 0))
		{
			rlColumnData.Text =  CString("");
		}
		else
		{
			if(ogPrivList.GetStat( olSecId + CString("VIAL")) != '-')
				rlColumnData.Text =  CString(prlLine->Vian);
		}

		olColList.NewAt(olColList.GetSize(), rlColumnData);


		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("IFRA"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "'I'|'V'";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 1;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("IFRA")) != '-')
			rlColumnData.Text = CString(prlLine->Ifra);         
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("TTYP"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXX";
		rlAttrib.TextMinLenght = 2;
		rlAttrib.TextMaxLenght = 5;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("TTYP")) != '-')
			rlColumnData.Text = CString(prlLine->Ttyp);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		if (bgAddKeyCodesDS)
		{
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		rlColumnData.Text = CString(prlLine->Stev);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("STEV"));

		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		rlColumnData.Text = CString(prlLine->Ste2);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("STE2"));

		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		rlColumnData.Text = CString(prlLine->Ste3);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("STE3"));

		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		rlColumnData.Text = CString(prlLine->Ste4);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("STE4"));

		}
		
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		rlColumnData.Text = prlLine->Stoa.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("ETAI"));

		rlAttrib.Type = KT_TIME;
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 7;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("ETAI")) != '-')
			rlColumnData.Text = prlLine->Etai.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		// Shanghai want to see ETOA (ETA Fids)
		if (strcmp(pcgHome, "PVG") == 0 ||
			__CanSeeDailyScheduleColumnsDisplay() )		// 050311 MVy: daily schedule lists should contain additional column for display data: ETA(D) and ETD(D)
		{
			rlColumnData.blIsEditable = true;	
			if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
				rlColumnData.blIsEditable = false;	

			rlAttrib.Type = KT_TIME;
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 7;
			rlColumnData.EditAttrib = rlAttrib;
			rlColumnData.Alignment = COLALIGN_CENTER;
			rlColumnData.Text = prlLine->Etoa.Format("%H:%M");
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}


		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("TMOA"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_TIME;
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 7;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("TMOA")) != '-')
			rlColumnData.Text = prlLine->Tmoa.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("LAND"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		

		if(prlLine->Ftyp == "B")
		{
			rlColumnData.Text = CString("");
		}
		else
		{
			if(ogPrivList.GetStat( olSecId + CString("LAND")) != '-')
			{
				if(prlLine->Ftyp == "T" || prlLine->Ftyp == "G")
				{
					if(prlLine->Ftyp == "T")
						rlColumnData.Text = prlLine->Ofbl.Format("T%H%M");
					else
						rlColumnData.Text = prlLine->Ofbl.Format("G%H%M");
				}
				else
				{
					rlColumnData.Text = prlLine->Land.Format("%H:%M");
				}
			}
		}


		rlAttrib.Type = KT_TIME;
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 7;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("RWYA"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 4;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("RWYA")) != '-')
			rlColumnData.Text = CString(prlLine->Rwya);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		rlColumnData.Text = prlLine->Onbe.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("ONBL"));

		rlAttrib.Type = KT_TIME;
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 7;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("ONBL")) != '-')
			rlColumnData.Text = prlLine->Onbl.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		

		// Shanghai want to see AIRB (ATD of origin)
		if (strcmp(pcgHome, "PVG") == 0)
		{
			rlColumnData.blIsEditable = false;	

			rlAttrib.Type = KT_TIME;
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 7;
			rlColumnData.EditAttrib = rlAttrib;
			rlColumnData.Alignment = COLALIGN_CENTER;
			rlColumnData.Text = prlLine->Airb.Format("%H:%M");
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("PSTA"));
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 5;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("PSTA")) != '-')
			rlColumnData.Text = CString(prlLine->Psta);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("GTA1"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 5;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("GTA1")) != '-')
			rlColumnData.Text = CString(prlLine->Gta1);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 5;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("BLT1"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("BLT1")) != '-')
			rlColumnData.Text = CString(prlLine->Blt1);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("REGN"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXXXXXXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 12;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("REGN")) != '-')
			rlColumnData.Text = CString(prlLine->Regn);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("ACT5"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 5;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("ACT5")) != '-')
			rlColumnData.Text = CString(prlLine->Act5);
		olColList.NewAt(olColList.GetSize(), rlColumnData);


/*		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("ISRE")) != '-')
			rlColumnData.Text = CString(prlLine->Isre);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("REMP")) != '-')
			rlColumnData.Text = CString(prlLine->Remp);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
*/	
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ipGroupID == UD_ARRIVAL)
			rlColumnData.Text = prlLine->Stoa.Format("%d.%m.%y");
		else
			rlColumnData.Text = prlLine->Land.Format("%d.%m.%y");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

/*
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("ISRE")) != '-')
			rlColumnData.Text = CString(prlLine->Isre);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
*/
		COLORREF oldRef = rlColumnData.BkColor;
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("ISRE")) != '-')
		{
			rlColumnData.Text = CString(prlLine->Isre);
			

			if(bgShowCashComment)
			{
				if(prlLine->Pcom.GetLength() > 0)
				{
					rlColumnData.BkColor = GREEN;	
				}
				else
				{
					if (prlLine->Paid == "B" || prlLine->Paid == " " || prlLine->Paid == "")
						rlColumnData.BkColor = RED;
					else if (prlLine->Paid == "K")
						rlColumnData.BkColor = WHITE;
					else
						rlColumnData.BkColor = GREEN;
							
				}
				
				
			}
			else
			{
				if (bgDisplayPaid || bgShowCashButtonInBothFlight)
				{
					if (prlLine->Paid == "B" || prlLine->Paid == " " || prlLine->Paid == "")
						rlColumnData.BkColor = RED;
					else if (prlLine->Paid == "K")
						rlColumnData.BkColor = WHITE;
					else
						rlColumnData.BkColor = GREEN;
				}
				else
				{
					rlColumnData.BkColor = oldRef;
				}
			}
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.BkColor = oldRef;

		
		if(bgDailyShowVIP)
		{
			rlColumnData.blIsEditable = false;	
			rlColumnData.Alignment = COLALIGN_LEFT;
			rlColumnData.BkColor = oldRef;
			rlColumnData.Text = "";
			if(prlLine->Vcnt>0)
			{
				rlColumnData.BkColor = PURPLE;
			}		
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}

			
	

		rlColumnData.BkColor = oldRef;

		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("REMP")) != '-')
			rlColumnData.Text = CString(prlLine->Remp);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
	
	
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_LEFT;
			rlColumnData.Text = CString(prlLine->Rem1);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		if(bgAdditionalRemark)
		{
			rlColumnData.blIsEditable = false;	
			rlColumnData.Alignment = COLALIGN_LEFT;
				rlColumnData.Text = CString(prlLine->Dela);
			olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		}
	
			rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_LEFT;
			rlColumnData.Text = CString(prlLine->Rem2);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

	}

	if((ipGroupID == UD_DEPARTURE) || (ipGroupID == UD_DGROUND))
	{
		if ((strcmp(pcgHome, "ATH") == 0))
		{
			if((strcmp(prlLine->Ftyp, "X") == 0) || (strcmp(prlLine->Ftyp, "Z") == 0) || (strcmp(prlLine->Ftyp, "B") == 0))
				rlColumnData.TextColor = RED;
		}
		
		if((strcmp(prlLine->Ftyp, "X") == 0))// || (strcmp(prlLine->Ftyp, "B") == 0))
			rlColumnData.TextColor = RED;

		if(strcmp(prlLine->Ftyp, "N") == 0)
			rlColumnData.TextColor = ORANGE;

		rlColumnData.BkColor = WHITE;


		if( (prlLine->Ftyp == "T") || (prlLine->Ftyp == "G") )
		{
		
		}
		else
		{
			if((prlLine->Ofbl != TIMENULL) && (prlLine->Airb == TIMENULL))
				rlColumnData.BkColor = LIME;
		}

		//backcolour for return flights

		if( (strcmp(prlLine->Ftyp, "Z") == 0) && (strcmp(pcgHome, "ATH") != 0) )
			rlColumnData.BkColor = RED;


		COLORREF olBkColor = rlColumnData.BkColor;

		if(ipGroupID == UD_DEPARTURE)
			olSecId = CString("ROTATIONTAB_D_");
		else
			olSecId = CString("ROTATIONTAB_DG_");

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("FLNO"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	

		if(ogFpeData.bmFlightPermitsEnabled)
		{
			rlColumnData.blIsEditable = false;
			rlColumnData.Alignment = COLALIGN_CENTER;
			rlColumnData.Text =  "";
			COLORREF olOldColour = rlColumnData.BkColor;
			rlColumnData.BkColor = prlLine->FlightPermitColour;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.BkColor = olOldColour;
		}
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXXXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 9;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("FLNO")) != '-')
			olTmp = CString(prlLine->Flno);
		olTmp.TrimRight();
		rlColumnData.Text =	olTmp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("CSGN"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 8;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("CSGN")) != '-')
			rlColumnData.Text = CString(prlLine->Csgn);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.TextColor = BLACK;

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("DES4"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G") || (prlLine->Ftyp == "B") || (prlLine->Ftyp == "Z"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 4;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("DES4")) != '-')
			rlColumnData.Text = CString(prlLine->Des4);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		/*
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("VIAL"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
		*/
			rlColumnData.blIsEditable = false;	


		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXXX";		// 050311 MVy: width increased from 4 to 6 chars
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 6;		// 050311 MVy: width increased from 4 to 6 chars
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if((strcmp(prlLine->Ftyp, "Z") == 0) || (strcmp(prlLine->Ftyp, "B") == 0))
		{
			rlColumnData.blIsEditable = false;	
			rlColumnData.Text =  CString("");
		}
		else
		{
			if(ogPrivList.GetStat( olSecId + CString("VIAL")) != '-')
				rlColumnData.Text =  CString(prlLine->Vial);
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		

		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		if((strcmp(prlLine->Ftyp, "Z") == 0) || (strcmp(prlLine->Ftyp, "B") == 0))
		{
			rlColumnData.Text =  CString("");
		}
		else
		{
			if(ogPrivList.GetStat( olSecId + CString("VIAL")) != '-')
				rlColumnData.Text =  CString(prlLine->Vian);
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("IFRD"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "'I'|'V'";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 1;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("IFRD")) != '-')
			rlColumnData.Text =  CString(prlLine->Ifrd);         
		olColList.NewAt(olColList.GetSize(), rlColumnData);

	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("TTYP"));
		
		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	

		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXX";
		rlAttrib.TextMinLenght = 2;
		rlAttrib.TextMaxLenght = 5;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("TTYP")) != '-')
			rlColumnData.Text = CString(prlLine->Ttyp);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	

				if (bgAddKeyCodesDS)
		{
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		rlColumnData.Text = CString(prlLine->Stev);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("STEV"));

		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		rlColumnData.Text = CString(prlLine->Ste2);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("STE2"));

		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		rlColumnData.Text = CString(prlLine->Ste3);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("STE3"));

		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		rlColumnData.Text = CString(prlLine->Ste4);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("STE4"));

		}

		
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		rlColumnData.Text = prlLine->Stod.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("ETDI"));
		rlAttrib.Type = KT_TIME;
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 7;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("ETDI")) != '-')
			rlColumnData.Text = prlLine->Etdi.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	

		// Shanghai want to see ETOD (ETD Fids)
		if (strcmp(pcgHome, "PVG") == 0 ||
			__CanSeeDailyScheduleColumnsDisplay() )		// 050311 MVy: daily schedule lists should contain additional column for display data: ETA(D) and ETD(D)
		{
			rlColumnData.blIsEditable = true;	
			if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
				rlColumnData.blIsEditable = false;	

			rlAttrib.Type = KT_TIME;
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 7;
			rlColumnData.EditAttrib = rlAttrib;
			rlColumnData.Alignment = COLALIGN_CENTER;
			rlColumnData.Text = prlLine->Etod.Format("%H:%M");
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("ETDC"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
				
		rlAttrib.Type = KT_TIME;
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 7;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		
		if(ogPrivList.GetStat( olSecId + CString("ETDC")) != '-')
		{
			if(bgCDMPhase1)
			{
				rlColumnData.Text = prlLine->Tobt.Format("%H:%M") + CString(prlLine->Tobf);

				if(CString(prlLine->Tobf) == "C")
					rlColumnData.BkColor = RGB(0,220,0);
			}
			else
			{
				rlColumnData.Text = prlLine->Etdc.Format("%H:%M");
			}

		}

		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("OFBL"));
		
		if((prlLine->Ftyp == "B") || (prlLine->Ftyp == "Z") || bgCDMPhase1)
			rlColumnData.blIsEditable = false;	
		

		rlColumnData.BkColor = olBkColor;
		

		rlAttrib.Type = KT_TIME;
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 7;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("OFBL")) != '-')
			rlColumnData.Text = prlLine->Ofbl.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlAttrib.Type = KT_TIME;
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 7;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		rlColumnData.Text = prlLine->Slot.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("RWYD"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 4;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("RWYD")) != '-')
			rlColumnData.Text = CString(prlLine->Rwyd);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("AIRB"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		if((prlLine->Ftyp == "B") || (prlLine->Ftyp == "Z"))
			rlColumnData.blIsEditable = false;	

		
		if(prlLine->Ftyp == "B")
		{
			rlColumnData.Text = CString("");
		}
		else
		{
			if(ogPrivList.GetStat( olSecId + CString("AIRB")) != '-')
				rlColumnData.Text = prlLine->Airb.Format("%H:%M");
		}

		rlAttrib.Type = KT_TIME;
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 7;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("PSTD"));
		rlAttrib.Format = "XXXXX";
		rlAttrib.Type = KT_STRING;
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 5;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("PSTD")) != '-')
			rlColumnData.Text = CString(prlLine->Pstd);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("GTD1"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 5;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("GTD1")) != '-')
			rlColumnData.Text = CString(prlLine->Gtd1);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("GTD2"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 5;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("GTD2")) != '-')
			rlColumnData.Text = CString(prlLine->Gtd2);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = false;	
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 7;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("CKI")) != '-')
			rlColumnData.Text = CString(prlLine->Cic);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("REGN"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXXXXXXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 12;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("REGN")) != '-')
			rlColumnData.Text = CString(prlLine->Regn);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("ACT5"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 5;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("ACT5")) != '-')
			rlColumnData.Text = CString(prlLine->Act5);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

/*		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("ISRE")) != '-')
			rlColumnData.Text = CString(prlLine->Isre);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("REMP")) != '-')
			rlColumnData.Text = CString(prlLine->Remp);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
*/
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ipGroupID == UD_DEPARTURE)
			rlColumnData.Text = prlLine->Airb.Format("%d.%m.%y");
		else
			rlColumnData.Text = prlLine->Stod.Format("%d.%m.%y");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

/*
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("ISRE")) != '-')
			rlColumnData.Text = CString(prlLine->Isre);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
*/
		COLORREF oldRef = rlColumnData.BkColor;
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("ISRE")) != '-')
		{
			rlColumnData.Text = CString(prlLine->Isre);
		

			if(bgShowCashComment)
			{
				if(prlLine->Pcom.GetLength() > 0)
				{
					rlColumnData.BkColor = GREEN;	
				}
				else
				{
					if (prlLine->Paid == "B" || prlLine->Paid == " " || prlLine->Paid == "")
						rlColumnData.BkColor = RED;
					else if (prlLine->Paid == "K")
						rlColumnData.BkColor = WHITE;
					else
						rlColumnData.BkColor = GREEN;
							
				}
				
			}
			else
			{
				if (bgDisplayPaid || bgShowCashButtonInBothFlight)
				{
					if (prlLine->Paid == "B" || prlLine->Paid == " " || prlLine->Paid == "")
						rlColumnData.BkColor = RED;
					else if (prlLine->Paid == "K")
						rlColumnData.BkColor = WHITE;
					else
						rlColumnData.BkColor = GREEN;
				}
				else
				{
					rlColumnData.BkColor = oldRef;
				}
			}

		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.BkColor = oldRef;

		if(bgDailyShowVIP)
		{
			
			rlColumnData.blIsEditable = false;	
			rlColumnData.Alignment = COLALIGN_LEFT;
			rlColumnData.BkColor = oldRef;
			//int llCountVIP = 0;
			//llCountVIP = ogVipData.GetCount( prlLine->Urno );
			rlColumnData.Text = "";
			if(prlLine->Vcnt>0)
			{
				rlColumnData.BkColor = PURPLE;
			}
			olColList.NewAt(olColList.GetSize(), rlColumnData);	
		}

		rlColumnData.BkColor = oldRef;
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("REMP")) != '-')
			rlColumnData.Text = CString(prlLine->Remp);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		
	
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_LEFT;
			rlColumnData.Text = CString(prlLine->Rem1);
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		if(bgAdditionalRemark)
		{
			
				rlColumnData.blIsEditable = false;	
			rlColumnData.Alignment = COLALIGN_LEFT;
				rlColumnData.Text = CString(prlLine->Dela);
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}

		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_LEFT;
			rlColumnData.Text = CString(prlLine->Rem2);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

	}	

	if(ipModus == ADD_LINE)
	{
		prlTable->AddTextLine(olColList, (void*)(prlLine));
	}
	else if(ipModus == CHANGE_LINE)
	{
		prlTable->ChangeTextLine(ipLineNo, &olColList, (void*)(prlLine));
	}
	else if(ipModus == INSERT_LINE)
	{
		prlTable->InsertTextLine(ipLineNo, olColList, (void *)prlLine);
	}
	else if (ipModus == HIGHLIGHT_LINE)
	{
		if(bgDailyHightligh)
		{
			for( int i=olColList.GetSize()-1; i>=0; i--)
			{
				//TABLE_COLUMN rlCol = olColList[i];
				olColList[i].BkColor = LIGHTPINK;
			}
			prlTable->ChangeTextLine(ipLineNo, &olColList, (void*)(prlLine));
		}
	}
		
	
	olColList.DeleteAll();



	if(!bpReadAll)
	{
		if(ilTopIndex >= 0)
			prlTable->pomListBox->SetTopIndex(ilTopIndex);
	}

	
	if((!bpReadAll) && (prlGroupData->Lines.GetSize() == 1))
		prlTable->DisplayTable();
	
	return true;
}





void RotationTableViewer::MakeLine(ROTATIONFLIGHTDATA *prlFlight, ROTATIONTABLE_LINEDATA &rpLine)
{
	CString olStr;
	if(prlFlight != NULL)
	{
		rpLine.FlightPermitColour = GetFlightPermitColour(prlFlight);
		rpLine.Tifa = prlFlight->Tifa;
		rpLine.Tifd = prlFlight->Tifd;
		rpLine.Urno = prlFlight->Urno;
		
		if (bgAddKeyCodesDS)
		{
			rpLine.Stev = prlFlight->Stev;
			rpLine.Ste2 = prlFlight->Ste2;
			rpLine.Ste3 = prlFlight->Ste3;
			rpLine.Ste4 = prlFlight->Ste4;
		}
		rpLine.Stoa = prlFlight->Stoa; 
		rpLine.Etai = prlFlight->Etai; 
		rpLine.Etoa = prlFlight->Etoa;
		rpLine.Tmoa = prlFlight->Tmoa; 
		rpLine.Land = prlFlight->Land; 
		rpLine.Onbe = prlFlight->Onbe; 
		rpLine.Onbl = prlFlight->Onbl; 
		rpLine.Stod = prlFlight->Stod; 
		rpLine.Etdi = prlFlight->Etdi;
		rpLine.Etod = prlFlight->Etod;		
		rpLine.Ofbl = prlFlight->Ofbl; 
		rpLine.Slot = prlFlight->Slot; 
		rpLine.Airb = prlFlight->Airb; 
		rpLine.Etdc = prlFlight->Etdc; 
		rpLine.B1ba = prlFlight->B1ba; 
		rpLine.B1ea = prlFlight->B1ea; 
		rpLine.Tobt = prlFlight->Tobt; 

		if (bgDailyLocal)
		{
			ogBasicData.UtcToLocal(rpLine.Tifa);
			ogBasicData.UtcToLocal(rpLine.Tifd);
			ogBasicData.UtcToLocal(rpLine.Stoa);
			ogBasicData.UtcToLocal(rpLine.Etai);
			ogBasicData.UtcToLocal(rpLine.Etoa);
			ogBasicData.UtcToLocal(rpLine.Tmoa);
			ogBasicData.UtcToLocal(rpLine.Land);
			ogBasicData.UtcToLocal(rpLine.Onbe);
			ogBasicData.UtcToLocal(rpLine.Onbl);
			ogBasicData.UtcToLocal(rpLine.Stod);
			ogBasicData.UtcToLocal(rpLine.Etdi);
			ogBasicData.UtcToLocal(rpLine.Etod);
			ogBasicData.UtcToLocal(rpLine.Ofbl);
			ogBasicData.UtcToLocal(rpLine.Slot);
			ogBasicData.UtcToLocal(rpLine.Airb);
			ogBasicData.UtcToLocal(rpLine.Etdc);
			ogBasicData.UtcToLocal(rpLine.B1ea);
			ogBasicData.UtcToLocal(rpLine.B1ba);
			ogBasicData.UtcToLocal(rpLine.Tobt);
		}


		rpLine.Toba = CString(prlFlight->Toba);
		rpLine.Tobf = CString(prlFlight->Tobf);



		rpLine.Flno = CString(prlFlight->Flno);
		rpLine.Csgn = CString(prlFlight->Csgn); 
		if (bgAirport4LC)
			rpLine.Org4 = CString(prlFlight->Org4);
		else
			rpLine.Org4 = CString(prlFlight->Org3);
		rpLine.Ifra = CString(prlFlight->Ifra);
		rpLine.Ttyp = CString(prlFlight->Ttyp); 
		rpLine.Rwya = CString(prlFlight->Rwya);
		rpLine.Psta = CString(prlFlight->Psta);
		rpLine.Gta1 = CString(prlFlight->Gta1); 
		rpLine.Regn = CString(prlFlight->Regn);
		rpLine.Act5 = CString(prlFlight->Act5); 
		rpLine.Chgi = CString(prlFlight->Chgi);
		rpLine.Isre = CString(prlFlight->Isre); 
		rpLine.Paid = prlFlight->Paid[0]; 
		rpLine.Remp = CString(prlFlight->Remp);
		rpLine.Rem2 = prlFlight->Rem2;
		if (bgAirport4LC)
			rpLine.Des4 = CString(prlFlight->Des4); 
		else
			rpLine.Des4 = CString(prlFlight->Des3); 
		rpLine.Ifrd = CString(prlFlight->Ifrd);
		rpLine.Rwyd = CString(prlFlight->Rwyd);
		rpLine.Pstd = CString(prlFlight->Pstd); 
		rpLine.Gtd1 = CString(prlFlight->Gtd1);
		rpLine.Gtd2 = CString(prlFlight->Gtd2); 
		rpLine.Ftyp = CString(prlFlight->Ftyp); 
		if (bgAirport4LC)		
			rpLine.Vial = CString(prlFlight->Via4); 
		else
			rpLine.Vial = CString(prlFlight->Via3); 
		rpLine.Vian = CString(prlFlight->Vian); 
		rpLine.Blt1 = CString(prlFlight->Blt1); 

		if(!CString(prlFlight->Blt2).IsEmpty())
		{
			rpLine.Blt1 += CString("/") + CString(prlFlight->Blt2);

		}


		rpLine.Cic.Empty();
		if (IsRegularFlight(prlFlight->Ftyp[0]))
		{
			CString opMin;
			CString opMax;

			if(bgSecondGateDemandFlag)
			{
				CString opMin2;
				CString opMax2;
				CString olStr;
				opMin = (CString)prlFlight->Ckif;
				opMax = (CString)prlFlight->Ckit;
				opMin2 = (CString)prlFlight->Ckf2;
				opMax2 = (CString)prlFlight->Ckt2;
				
				if(!opMin.IsEmpty() || !opMax.IsEmpty())
				{
					olStr = opMin + CString("-") + opMax; 
				}

				if(!opMin2.IsEmpty() || !opMax2.IsEmpty())
				{
					if(!olStr.IsEmpty() )
						olStr += CString(" ");

					olStr += opMin2 + CString("-") + opMax2; 
				}



				rpLine.Cic = olStr;
			}
			else
			{

				opMin = prlFlight->Ckif;
				opMax = prlFlight->Ckit;

				if( opMax.IsEmpty() && opMin.IsEmpty() )
					ogRotationFlights.omCcaData.GetMinMaxCkic(prlFlight->Urno, opMin, opMax);

				if(opMax.IsEmpty() || opMax == opMin)
				{
					rpLine.Cic = opMin;
					if (bgCnamAtr)
						rpLine.Cic = GetCnamExt(opMin, CString(""));

				}
				else
				{
					rpLine.Cic = opMin + CString("-") + opMax;
					if (bgCnamAtr)
						rpLine.Cic = GetCnamExt(opMin, opMax);
				}
			}
		}

		if((rpLine.Ftyp == "T") || (rpLine.Ftyp == "G"))
		{
			rpLine.Des4 = CString(""); 
			rpLine.Org4 = CString(""); 
		}

		
		char olRem1[258];
		CString olSrc = prlFlight->Rem1;
		CleanSteurzeichen (olRem1, olSrc);
		rpLine.Rem1 = CString(olRem1);

		char olRem2[258];
		CString olSrc2 = prlFlight->Rem2;
		CleanSteurzeichen (olRem2, olSrc2);
		rpLine.Rem2 = CString(olRem2);


		if(bgDailyShowVIP)
		{
			rpLine.Vcnt = prlFlight->Vcnt;
		}


		if (bgAdditionalRemark)
		{
			char olDela[255];
			CString olSrc3 = prlFlight->Dela;
			CleanSteurzeichen (olDela, olSrc3);
			rpLine.Dela=CString(olDela);
		}

		if(bgShowCashComment)
		{
			CString olPcom = prlFlight->Pcom;
			rpLine.Pcom= CString(olPcom);
		}

	}
    return;
}




void RotationTableViewer::MakeLines()
{
	ROTATIONFLIGHTDATA *prlFlight;
    ROTATIONTABLE_LINEDATA rlLine;
	int ilGroup1 = 0;
	int ilGroup2 = 0;

		// load VIP data
	CString olFlightUrnos; 
	char buffer[65];

	for(int i = ogRotationFlights.omData.GetSize() - 1; i >= 0; i--)
	{
		prlFlight = &ogRotationFlights.omData[i];
		
		
		if(IsPassFilter(prlFlight))
		{
			ltoa (prlFlight->Urno, buffer, 10);
			olFlightUrnos += CString(buffer) + CString(",");	

			MakeLine(prlFlight, rlLine);
			GetGroup(prlFlight, ilGroup1, ilGroup2);
			CreateLine(ilGroup1, rlLine);
			CreateLine(ilGroup2, rlLine);
		}
	}

/*	
	if(!olFlightUrnos.IsEmpty())
	{
		olFlightUrnos = olFlightUrnos.Left(olFlightUrnos.GetLength() - 1);
	}
	ogVipData.ReadFlnus(olFlightUrnos); */

	//UpdateWindowTitle();
}

// 050302 MVy: generate and change the dialog title
void RotationTableViewer::UpdateWindowTitle()
{
	omTableName = GenerateTableName();		// 050224 MVy: code moved to GenerateTableName()
	SetWindowTitle( omTableName );		// 050224 MVy: put the current counts of flights / arrivals / departures into the dialog title
};	// UpdateWindowTitle

// 050224 MVy: create a string containing information about current table data
CString RotationTableViewer::GenerateTableName()
{
	//std::stringstream ss ;

	int iTotal = 0 ;	// contains the sum of the following amounts
	int iArrivals = -1 ;		// -1 for error detection, if there are some unset values the corresponding tab is not found
	int iLanded = -1 ;
	int iDepatures = -1 ;
	int iAirborns = -1 ;

	// DO NOT SHOW MORE DETAILS, only one entry can be marked
	/*
	// 050302 MVy: more details, count rotations, count marked and unmarked lines
	int iArrMarked = -1 ;
	int iLanMarked = -1 ;
	int iDepMarked = -1 ;
	int iAirMarked = -1 ;

	RotationTableChart* pChart = GetParent();
	ASSERT( pChart );
	RotationTables* pTables = pChart->GetParent();
	ASSERT( pTables );

	int iGroups = pTables->omPtrArray.GetSize();
	ASSERT( iGroups == 4 );	// must be 4 tables herein
	for( int ndxGroup = 0 ; ndxGroup < iGroups  ; ndxGroup++ )		// for each group in view
	{
		RotationTableChart *polChart = pTables->GetChartByIndex( ndxGroup );
		ASSERT( polChart );		// a chart must exist on any index, the generation of the chart window already must have been done
		if( polChart )
		{
			CCSTable* pTable = polChart->GetTable();
			int iCount = pTable->GetSelCount();

			int imGroupID = polChart->imGroupID ;
			switch( imGroupID )
			{
				case UD_ARRIVAL : iArrMarked = iCount ; break ;
				case UD_AGROUND : iLanMarked = iCount ; break ;
				case UD_DGROUND : iDepMarked = iCount ; break ;
				case UD_DEPARTURE : iAirMarked = iCount ; break ;	// HINT: namings are mixed up, see RotationTableChart::OnCreate() and look for "if(imGroupID == ..."
				default : ASSERT(0); 	// this ID is not handled
			};
		};
	};
	ASSERT( iArrMarked > -1 );
	ASSERT( iLanMarked > -1 );
	ASSERT( iDepMarked > -1 );
	ASSERT( iAirMarked > -1 );
	*/

	ASSERT( omGroups.GetSize() == 4 );	// must be 4 tables herein
	for( int i = omGroups.GetSize() - 1 ; i >= 0 ; i-- )
	{
		int imGroupID = omGroups[i].GroupID ;
		int iCount = omGroups[i].Lines.GetSize();		// the size is the number of entries which is equal to the amount of lines
		iTotal += iCount ;
		//ss << "   " << (LPCTSTR )(omGroups[i].GroupText) << "=" << iCount ;

		switch( imGroupID )
		{
			case UD_ARRIVAL : iArrivals = iCount ; break ;
			case UD_AGROUND : iLanded = iCount ; break ;
			case UD_DGROUND : iDepatures = iCount ; break ;
			case UD_DEPARTURE : iAirborns = iCount ; break ;	// HINT: namings are mixed up, see RotationTableChart::OnCreate() and look for "if(imGroupID == ..."
			default : ASSERT(0); 	// this ID is not handled
		};
	};

	ASSERT( iArrivals > -1 );
	ASSERT( iLanded > -1 );
	ASSERT( iDepatures > -1 );
	ASSERT( iAirborns > -1 );

	//std::stringstream ssTitle ;
	//ssTitle << "Total=" << iTotal << ss.str() ;

	CString olTimes;
	bgDailyLocal ? olTimes = GetString(IDS_STRING1921) : olTimes = GetString(IDS_STRING1920);

	CString olTableName = GetString( IDS_STRING1029 );//"Daily Schedule";
	char pclHeader[256];
	// 050225 MVy: Daily Schedule, format string for special information shown in window title
	sprintf(pclHeader, GetString(IDS_DAY_SCH_CAPT), olTableName, iTotal, iArrivals, iLanded, iDepatures, iAirborns, olTimes);

	return pclHeader ;
	//return ssTitle.str().data();
};	// GenerateTableName

// 050224 MVy: return the parent window, a Chart in this case
// 050427 MVy: PRF6984.3: name changed from GetParent() to GetParentRotationTableChart()
RotationTableChart *RotationTableViewer::GetParentRotationTableChart()
{
	RotationTableChart *pChart = 0 ;
	unsigned int uiCount = omGroups.GetSize();
	if( uiCount )		// cannot return window because the only way to get the parent window is to read an entry from a group, but if there is no group there is no window referenced
	{
		GROUP_DATA *prlGroupData = &omGroups[0];
		ASSERT( prlGroupData );
		pChart = prlGroupData->Chart ;
	};
	ASSERT( pChart );		// Warning: requestor may work with null window. This need not cause a crash.
	return pChart ;
};	// GetParentRotationTableChart

// 050224 MVy: modifiy the window title by specified text
// 050427 MVy: PRF6984.3: using renamed GetParent...()
void RotationTableViewer::SetWindowTitle( CString sTitle )
{
	RotationTableChart* pChart = GetParentRotationTableChart();
	ASSERT( pChart );
	CFrameWnd *pTables = (CFrameWnd *)pChart->GetParentRotationTable();		// RotationTables object inherits from CFrameWnd
	ASSERT( pTables );
	pTables->SetWindowText( sTitle );
};	// SetWindowTitle

void RotationTableViewer::DeleteLine(int ipGroupID, int ipLineNo)
{
	if(	ipGroupID == UD_FAIL)
		return;

	int ilTopIndex;

   	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		if(omGroups[i].GroupID == ipGroupID)
		{
   			if((0 <= ipLineNo) && (ipLineNo < omGroups[i].Lines.GetSize()))
			{
				/*
				TRACE("\n----------------------------------------------------------------------------------------------------------");
				TRACE("\nDeleteLine: Urno:<%ld> Flno:<%s> ID:<%d> <%s> Table:<%p> Chart:<%p>", omGroups[i].Lines[ipLineNo].Urno, omGroups[i].Lines[ipLineNo].Flno, omGroups[i].GroupID, omGroups[i].GroupText, omGroups[i].Table, omGroups[i].Chart );
				TRACE("\n----------------------------------------------------------------------------------------------------------");
				*/

				ilTopIndex = omGroups[i].Table->pomListBox->GetTopIndex();
				omGroups[i].Lines.DeleteAt(ipLineNo);
				omGroups[i].Table->DeleteTextLine(ipLineNo);

				if(ilTopIndex >= 0)
					omGroups[i].Table->pomListBox->SetTopIndex(ilTopIndex);
			}
			break;
		}
	}
}




int RotationTableViewer::CreateLine(int ipGroupID, ROTATIONTABLE_LINEDATA &rpLine)
{
	if(	ipGroupID == UD_FAIL)
		return -1;

   	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		if(omGroups[i].GroupID == ipGroupID)
		{
			int ilLineCount = omGroups[i].Lines.GetSize();

			for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
				if (CompareFlight(ipGroupID, &rpLine, &omGroups[i].Lines[ilLineno-1]) >= 0)
					break;  // should be inserted after Lines[ilLineno-1]

			omGroups[i].Lines.NewAt(ilLineno, rpLine);
			
			return ilLineno;
		}
	}
	return 0;
}






void RotationTableViewer::ChangeLine(int ipGroupID, ROTATIONTABLE_LINEDATA &rpLine, int ipLineNo)
{
	if(	ipGroupID == UD_FAIL)
		return;

   	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		if(omGroups[i].GroupID == ipGroupID)
		{
			if(((ipLineNo >= 0) && (ipLineNo < omGroups[i].Lines.GetSize()))) 
			{
				/*
				TRACE("\n----------------------------------------------------------------------------------------------------------");
				TRACE("\nChangeLine: Urno:<%ld> Flno:<%s> ID:<%d> <%s> Table:<%p> Chart:<%p>", omGroups[i].Lines[ipLineNo].Urno, omGroups[i].Lines[ipLineNo].Flno, omGroups[i].GroupID, omGroups[i].GroupText, omGroups[i].Table, omGroups[i].Chart );
				TRACE("\n----------------------------------------------------------------------------------------------------------");
				*/
				omGroups[i].Lines.DeleteAt(ipLineNo);
				omGroups[i].Lines.NewAt(ipLineNo, rpLine);
			}
			break;
		}
	}
}




bool RotationTableViewer::IsPassFilter(ROTATIONFLIGHTDATA *prlFlight)
{
	return true;
}



void RotationTableViewer::MakeAndShowLine(ROTATIONFLIGHTDATA *prlFlight)
{
	if(prlFlight == NULL)
		return;
   	int ilGroupID1Now;
   	int ilGroupID2Now;
	int ilLineNo1Now;
	int ilLineNo2Now;

   	int ilGroupID1;
	int ilLineNo1;
   	int ilGroupID2;
	int ilLineNo2;
	int ilModus;

    ROTATIONTABLE_LINEDATA rlLine;

	MakeLine(prlFlight, rlLine);

	// the current group and line
	GetGroupAndLineByUrno(prlFlight, ilGroupID1Now, ilLineNo1Now, ilGroupID2Now, ilLineNo2Now);

	bool blTurnArround = GetGroup(prlFlight, ilGroupID1, ilGroupID2);

	
	if(((ilGroupID1Now != UD_FAIL) && (ilGroupID1 == UD_FAIL)) ||
	   ((ilGroupID2Now != UD_FAIL) && (ilGroupID2 == UD_FAIL)))
	{

		ProcessFlightDelete(prlFlight);
		ilGroupID1Now = UD_FAIL;
		ilGroupID1Now = UD_FAIL;

	}


	ilLineNo1 = GetLinePos(ilGroupID1, rlLine);

	if(	ilGroupID1Now != UD_FAIL)	
	{
			if((ilLineNo1Now == ilLineNo1) && (ilGroupID1 == ilGroupID1Now))
			{
				//TRACE("CHANGE LINE\n");
				ilModus = CHANGE_LINE;
				ChangeLine(ilGroupID1, rlLine,ilLineNo1);
				ShowTableLine(ilGroupID1, ilLineNo1, ilModus);
			}
			else
			{
				//TRACE("DELETE-INSERT LINE\n");
				DeleteLine(ilGroupID1Now, ilLineNo1Now);
				SetTopScaleText(ilGroupID1Now);
				ilModus = INSERT_LINE;
				ilLineNo1 = CreateLine(ilGroupID1, rlLine);
				ShowTableLine(ilGroupID1, ilLineNo1, ilModus);
			}
			SetTopScaleText(ilGroupID1);
	}
	else
	{
		ilModus = INSERT_LINE;
		ilLineNo1 = CreateLine(ilGroupID1, rlLine);
		ShowTableLine(ilGroupID1, ilLineNo1, ilModus);
		SetTopScaleText(ilGroupID1);
	}


	if(blTurnArround)	
	{
		ilLineNo2 = GetLinePos(ilGroupID2, rlLine);

		if(	ilGroupID2Now != UD_FAIL)	
		{
			if((ilLineNo2 == ilLineNo2Now) && (ilGroupID2 == ilGroupID2Now))
			{
				ilModus = CHANGE_LINE;
				ChangeLine(ilGroupID2, rlLine,ilLineNo2);
				ShowTableLine(ilGroupID2, ilLineNo2, ilModus);
			}
			else
			{
				DeleteLine(ilGroupID2Now, ilLineNo2Now);
				SetTopScaleText(ilGroupID2Now);
				ilModus = INSERT_LINE;
				ilLineNo2 = CreateLine(ilGroupID2, rlLine);
				ShowTableLine(ilGroupID2, ilLineNo2, ilModus);
			}
			SetTopScaleText(ilGroupID2);
		}
		else
		{
			ilModus = INSERT_LINE;
			ilLineNo2 = CreateLine(ilGroupID2, rlLine);
			ShowTableLine(ilGroupID2, ilLineNo2, ilModus);
			SetTopScaleText(ilGroupID2);
		}
	}
}



int RotationTableViewer::GetLinePos(int ipGroupID, ROTATIONTABLE_LINEDATA &rpLine)
{
	if(	ipGroupID == UD_FAIL)
		return -1;

   	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		if(omGroups[i].GroupID == ipGroupID)
		{
			int ilLineCount = omGroups[i].Lines.GetSize();

			for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
				if (CompareFlight(ipGroupID, &rpLine, &omGroups[i].Lines[ilLineno-1]) > 0)
					break;  // should be inserted after Lines[ilLineno-1]
			/*
			if(ilLineno > 0)
			{
				if(omGroups[i].Lines[ilLineno-1].Urno == rpLine.Urno)
					ilLineno--;
			}

			if(ilLineno < (ilLineCount - 1))
			{
				if(omGroups[i].Lines[ilLineno + 1].Urno == rpLine.Urno)
					ilLineno++;
			}
			*/
			return ilLineno;
		}
	}
	return 0;
}




void RotationTableViewer::SetTopScaleText(int ipGroupID)
{
    ROTATIONTABLE_LINEDATA *prlLine;
	int ilLineNo;
	char pclTSText[256];

	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		GROUP_DATA *prlGroup = &omGroups[i];
		if(ipGroupID == prlGroup->GroupID)
		{
			ilLineNo = GetNext(prlGroup->GroupID);

			if((ilLineNo >= 0) && (ilLineNo < prlGroup->Lines.GetSize()))
			{
				prlLine = &prlGroup->Lines[ilLineNo];
				if(ipGroupID == UD_DGROUND)
				{
					sprintf(pclTSText,"%9s  %12s  STD: %5s     OFB: %5s     POS: %5s     RWY: %5s",prlLine->Flno, prlLine->Regn, prlLine->Stod.Format("%H:%M"), prlLine->Ofbl.Format("%H:%M"), prlLine->Pstd , prlLine->Rwyd);
				}	
				if(ipGroupID == UD_DEPARTURE)
				{
					sprintf(pclTSText,"%9s  %12s  STD: %5s     ATD: %5s     POS: %5s     RWY: %5s",prlLine->Flno, prlLine->Regn, prlLine->Stod.Format("%H:%M"), prlLine->Airb.Format("%H:%M"), prlLine->Pstd  , prlLine->Rwyd);
				}	
				if(ipGroupID == UD_ARRIVAL)
				{
					sprintf(pclTSText,"%9s  %12s  STA: %5s     TMO: %5s     POS: %5s     RWY: %5s",prlLine->Flno, prlLine->Regn, prlLine->Stoa.Format("%H:%M"), prlLine->Tmoa.Format("%H:%M"), prlLine->Psta , prlLine->Rwya );
				}	
				if(ipGroupID == UD_AGROUND)
				{
					sprintf(pclTSText,"%9s  %12s  STA: %5s     ATA: %5s     POS: %5s     RWY: %5s",prlLine->Flno, prlLine->Regn, prlLine->Stoa.Format("%H:%M"), prlLine->Land.Format("%H:%M"), prlLine->Psta , prlLine->Rwya);
				}	
			}
			else
				strcpy(pclTSText,"");
				prlGroup->TopScaleText->SetWindowText(pclTSText);
				prlGroup->TopScaleText->InvalidateRect(NULL);
				prlGroup->TopScaleText->UpdateWindow();
		}
	}
}



int RotationTableViewer::GetNext(int ipGroupID)
{
/*
    ROTATIONTABLE_LINEDATA *prlLine;
	CTime olCurrTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrTime);
	CTime olBestTime(2030,1,1,1,1,1,1);
	CTime olFutur(2030,1,1,1,1,1,1);
	CTime olTmpTime;
	int ilLineNo = -1;
*/
	GROUP_DATA *prlGroup ;

	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		prlGroup = &omGroups[i];
		if(ipGroupID == prlGroup->GroupID)
			break;
	}



	if(ipGroupID == UD_ARRIVAL)
		return prlGroup->Lines.GetSize() - 1;


	if(ipGroupID == UD_DEPARTURE)
	{
		if(prlGroup->Lines.GetSize() == 0)
			return -1;
		else
			return 0;
	}


	if(ipGroupID == UD_DGROUND)
		return prlGroup->Lines.GetSize() - 1;



	if(ipGroupID == UD_AGROUND)
	{
		for(int j = prlGroup->Lines.GetSize() - 1; j >= 0; j--)
		{
			if(prlGroup->Lines[j].Onbl == TIMENULL)
				return j;

		}
	}

	return -1;


/*
	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		GROUP_DATA *prlGroup = &omGroups[i];
		if(ilGroupID == prlGroup->GroupID)
		{
			for(int j = prlGroup->Lines.GetSize() - 1; j >= 0; j--)
			{
				prlLine = &prlGroup->Lines[j];

				if(ipGroupID == UD_ARRIVAL)
					olTmpTime = prlGroup->Lines[j].Stoa;

				if(ipGroupID == UD_AGROUND)
				{
					if(prlGroup->Lines[j].Onbl == TIMENULL)
						olTmpTime = prlGroup->Lines[j].Land;
					else
						olTmpTime = olFutur;
				}

				if(ipGroupID == UD_DGROUND)
				{
					if(prlGroup->Lines[j].Ofbl == TIMENULL)
						olTmpTime = prlGroup->Lines[j].Stod;
					else
						olTmpTime = prlGroup->Lines[j].Ofbl;
				}

				if(ipGroupID == UD_DEPARTURE)
				{
					if(prlGroup->Lines[j].Ofbl == TIMENULL)
						olTmpTime = olFutur;
					else
						olTmpTime = prlGroup->Lines[j].Ofbl;
				}


				if(olTmpTime < olBestTime)
				{
					olBestTime = olTmpTime;
					ilLineNo = j;		
				}
			}
		}
	}
	if(olBestTime == olFutur)
		return -1;
	else
		return ilLineNo;
*/
}



void RotationTableViewer::ShowNextLines()
{
	for(int l = omGroups.GetSize() - 1; l >= 0; l--)
	{
		GROUP_DATA *prlGroup = &omGroups[l];
		if (prlGroup->Table->IsInplaceEditMode)
			return;
	}

	int ilLineNo;

	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		GROUP_DATA *prlGroup = &omGroups[i];

		ilLineNo = 0;
		ilLineNo = GetNext(prlGroup->GroupID);

		if((ilLineNo  == -1) && (prlGroup->GroupID == UD_AGROUND))
			ilLineNo  = 0;

		prlGroup->Table->EndInplaceEditing();
		
		if(prlGroup->GroupID == UD_AGROUND)
		{
			int ilFirst = prlGroup->Table->pomListBox->GetFirstVisibleItem();
			int ilLast = prlGroup->Table->pomListBox->GetLastVisibleItem();

			ilLineNo = ilLineNo - (ilLast - ilFirst) + 1;

			if(ilLineNo < 0)
				prlGroup->Table->pomListBox->SetTopIndex(0);
			else
				prlGroup->Table->pomListBox->SetTopIndex(ilLineNo);

		}
		else
		{
			prlGroup->Table->pomListBox->SetTopIndex(ilLineNo);
		}

	}

}


bool RotationTableViewer::ShowFlightLine(long lpUrno)
{
	if(lpUrno >= 0)
	{
		GROUP_DATA *prlGroupData;
 
		for(int i = omGroups.GetSize() - 1; i >= 0; i--)
		{
			prlGroupData = &omGroups[i];
			for(int j = prlGroupData->Lines.GetSize() - 1; j >= 0; j--)
			{
			
				if(prlGroupData->Lines[j].Urno == lpUrno)
				{
					prlGroupData->Table->EndInplaceEditing();
 					prlGroupData->Table->pomListBox->SetTopIndex(j);

 					return true;
				}
			}
		}
	}

	return false;
}




bool RotationTableViewer::GetGroup(ROTATIONFLIGHTDATA *prlFlight, int &ipGroup1, int &ipGroup2)
{
	ipGroup1 = UD_FAIL;
	ipGroup2 = UD_FAIL;
	bool blRet = false;

	if((strcmp(prlFlight->Des3,pcgHome) == 0) && (strcmp(prlFlight->Org3,pcgHome) == 0))
	{
		blRet = true;

//rkr20032001
/*		if((strcmp(prlFlight->Ftyp,"T") == 0) || (strcmp(prlFlight->Ftyp,"G") == 0))
		{
			ipGroup1 = UD_AGROUND;

			if(prlFlight->Ofbl != TIMENULL)
			{
				ipGroup2 = UD_DEPARTURE;
			}
			else
			{
				ipGroup2 = UD_DGROUND;
			}
		}
*/
//rkr20032001
		if((strcmp(prlFlight->Ftyp,"T") == 0) || (strcmp(prlFlight->Ftyp,"G") == 0))
		{
			if(prlFlight->Ofbl != TIMENULL)
			{
				ipGroup1 = UD_AGROUND;
//				ipGroup2 = UD_DGROUND;
				ipGroup2 = UD_DEPARTURE;
			}
			else
			{
				ipGroup1 = UD_FAIL;
				ipGroup2 = UD_DGROUND;
			}
		}
		else
		{

			if(prlFlight->Land == TIMENULL)	
				ipGroup1 = UD_ARRIVAL;
			else
				ipGroup1 = UD_AGROUND;


			if(prlFlight->Airb == TIMENULL)
				ipGroup2 = UD_DGROUND;
			else
				ipGroup2 = UD_DEPARTURE;
		}

	}
	else
	{
		if(strcmp(prlFlight->Des3,pcgHome) == 0)
		{
			if(prlFlight->Land == TIMENULL)	
				ipGroup1 = UD_ARRIVAL;
			else
				ipGroup1 = UD_AGROUND;
		}
		if(strcmp(prlFlight->Org3,pcgHome) == 0)
		{
			if(prlFlight->Airb == TIMENULL)	
				ipGroup1 = UD_DGROUND;
			else
				ipGroup1 = UD_DEPARTURE;
		}
	}
	return blRet;
}



bool RotationTableViewer::SelectFlightLine(long lpUrno)
{
	if(lpUrno >= 0)
	{
		GROUP_DATA *prlGroupData;
 
		for(int i = omGroups.GetSize() - 1; i >= 0; i--)
		{
			prlGroupData = &omGroups[i];
			for(int j = prlGroupData->Lines.GetSize() - 1; j >= 0; j--)
			{
			
				if(prlGroupData->Lines[j].Urno == lpUrno)
				{
					prlGroupData->Table->SelectLine(j, TRUE);
					return true;
				}
			}
		}
	}

	return false;
}



////////////////////////////////////////////////////////////////////////////
// DDX
//
static void RotationFlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    RotationTableViewer *polViewer = (RotationTableViewer *)popInstance;

    if (ipDDXType == R_FLIGHT_CHANGE)
        polViewer->ProcessFlightChange((ROTATIONFLIGHTDATA *)vpDataPointer);
	if (ipDDXType == R_FLIGHT_DELETE)
        polViewer->ProcessFlightDelete((ROTATIONFLIGHTDATA *)vpDataPointer);
	if (ipDDXType == DATA_RELOAD)
        polViewer->ChangeViewTo("");
	if (ipDDXType == TABLE_FONT_CHANGED)
        polViewer->Repaint();
    if (ipDDXType == FPE_INSERT || ipDDXType == FPE_UPDATE || ipDDXType == FPE_DELETE)
        polViewer->ProcessFpeChange((FPEDATA *)vpDataPointer);

	if(polViewer->bmAutoNow)
	{
	    if (ipDDXType == R_FLIGHT_CHANGE || ipDDXType == R_FLIGHT_DELETE || ipDDXType == DATA_RELOAD || ipDDXType == TABLE_FONT_CHANGED)
		{
			polViewer->ShowNextLines();
		}
	}

	polViewer->UpdateWindowTitle();	// 050224 MVy: put the current counts of flights / arrivals / departures into the dialog title
}



void RotationTableViewer::ProcessFlightChange(ROTATIONFLIGHTDATA *prlFlight)
{
	/*
	TRACE("\n----------------------------------------------------------------------------------------------------------");
	TRACE("\nDDX-Change: Urno:<%ld> Flno:<%s>", prlFlight->Urno, prlFlight->Flno);
	TRACE("\n----------------------------------------------------------------------------------------------------------");
	*/

	long llSelUrno = 0L;
	GROUP_DATA *prlGroupData;
	int ilLine;
	ROTATIONTABLE_LINEDATA *prlLine;

	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		prlGroupData = &omGroups[i];
		
		ilLine = prlGroupData->Table->GetCurSel();
		if(ilLine >= 0)
		{
			prlLine = (ROTATIONTABLE_LINEDATA*) prlGroupData->Table->GetTextLineData(ilLine);

			if(prlLine != NULL)
				llSelUrno = prlLine->Urno;
			break;
		}
		
	}



	MakeAndShowLine(prlFlight);

	SelectFlightLine(llSelUrno);

}

void RotationTableViewer::ProcessFpeChange(FPEDATA *prpFpe)
{
	if(!ogFpeData.bmFlightPermitsEnabled)
		return;

	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		GROUP_DATA *prlGroupData = &omGroups[i];
		for(int l = 0; l < prlGroupData->Lines.GetSize(); l++)
		{
			ROTATIONTABLE_LINEDATA *prpLine = &prlGroupData->Lines[l];
			ROTATIONFLIGHTDATA *prlFlight = ogRotationFlights.GetFlightByUrno(prpLine->Urno);
			COLORREF olFlightPermitColour = GetFlightPermitColour(prlFlight);
			if(prpLine->FlightPermitColour != olFlightPermitColour)
				MakeAndShowLine(prlFlight);
		}
	}
}

COLORREF RotationTableViewer::GetFlightPermitColour(ROTATIONFLIGHTDATA *prpFlight)
{
	COLORREF olFlightPermitColour = WHITE;
	if(prpFlight != NULL && ogFpeData.bmFlightPermitsEnabled)
	{
		bool blArr = (!strcmp(prpFlight->Adid,"A")) ? true : false;
		if(ogFpeData.FtypEnabled(prpFlight->Ftyp))
		{
			bool blArr = (!strcmp(prpFlight->Adid,"A")) ? true : false;
			if(ogFpeData.FlightHasPermit(blArr ? prpFlight->Stoa : prpFlight->Stod, blArr, prpFlight->Regn, prpFlight->Alc3, prpFlight->Fltn, prpFlight->Flns))
				olFlightPermitColour = LIME;
			else
				olFlightPermitColour = RED;
		}
	}

	return olFlightPermitColour;
}

void RotationTableViewer::ProcessFlightDelete(ROTATIONFLIGHTDATA *prlFlight)
{
   	int ilGroupID1;
	int ilLineNo1;
   	int ilGroupID2;
	int ilLineNo2;
	if(prlFlight == NULL)
		return;

	/*
	TRACE("\n----------------------------------------------------------------------------------------------------------");
	TRACE("\nDDX-Delete: Urno:<%ld> Flno:<%s>", prlFlight->Urno, prlFlight->Flno);
	TRACE("\n----------------------------------------------------------------------------------------------------------");
	*/
	if(GetGroupAndLineByUrno(prlFlight, ilGroupID1, ilLineNo1, ilGroupID2, ilLineNo2))
	{
		DeleteLine(ilGroupID1, ilLineNo1);
		DeleteLine(ilGroupID2, ilLineNo2);
	}
}


bool RotationTableViewer::GetGroupAndLineByUrno(ROTATIONFLIGHTDATA *prlFlight, int &ipGroupID1, int &ipLineNo1, int &ipGroupID2, int &ipLineNo2)
{
	//Holzhammermethode!!!

	ipGroupID1 = UD_FAIL;
	ipGroupID2 = UD_FAIL;
	ipLineNo1 = -1;
	ipLineNo2 = -1;
	bool blRet = false;

	long llUrno;
	long lpUrno = prlFlight->Urno;
	bool blTurnArround = false;

	bool blTreffer = false;

	if((strcmp(prlFlight->Des3,pcgHome) == 0) && (strcmp(prlFlight->Org3,pcgHome) == 0))
	{
		blTurnArround = true;
	}

	int ilGroupCount = omGroups.GetSize();

	for(int i = 0; i < ilGroupCount; i++)
	{
		GROUP_DATA *prlGroup = &omGroups[i];
		for(int j = prlGroup->Lines.GetSize() - 1; j >= 0; j--)
		{
			llUrno = prlGroup->Lines[j].Urno; 

			if( llUrno == lpUrno)
			{
				if(!blTreffer)
				{
					blTreffer = true;
					ipGroupID1 = prlGroup->GroupID;
					ipLineNo1 = j;
					blRet = true;
					break;
				}
				else
				{
					ipGroupID2 = prlGroup->GroupID;
					ipLineNo2 = j;
					blRet = true;
					break;
				}
			}
		}
	}

	return blRet;
}






















int RotationTableViewer::CompareFlight(int ipGroup, ROTATIONTABLE_LINEDATA *prpLine1, ROTATIONTABLE_LINEDATA *prpLine2)
{
	int	ilCompareResult = 0;
	int ilCount = omSort.GetSize();
	CTime olTime1;
	CTime olTime2;


	if(ipGroup == UD_ARRIVAL)
	{
		if(ilCount == 0 || !bmArrField)
		{

			if(	prpLine1->Tmoa != TIMENULL)
			{
				olTime1 = prpLine1->Tmoa;
			}
			else
			{
				if(	prpLine1->Etai != TIMENULL)
					olTime1 = prpLine1->Etai;
				else
					olTime1 = prpLine1->Stoa;

			}

			if(	prpLine2->Tmoa != TIMENULL)
			{
				olTime2 = prpLine2->Tmoa;
			}
			else
			{
				if(	prpLine2->Etai != TIMENULL)
					olTime2 = prpLine2->Etai;
				else
					olTime2 = prpLine2->Stoa;

			}

			if(olTime2 == olTime1)
			{
				ilCompareResult =  (prpLine1->Flno <  prpLine2->Flno)? 1: -1;
			}
			else
			{
				ilCompareResult =  (olTime1 < olTime2)? 1: -1;
			}


			if((prpLine1->Tmoa != TIMENULL) && (prpLine2->Tmoa == TIMENULL))
				ilCompareResult =  1;

			if((prpLine1->Tmoa == TIMENULL) && (prpLine2->Tmoa != TIMENULL))
				ilCompareResult =  -1;


			return ilCompareResult;
		}
		else
		{
			for (int i = 0; i < ilCount; i++)
			{
				ilCompareResult = 0;

				int il = omSort[i].GetLength();
//				CString cll = omSort[i];
				if (omSort[i].GetLength() > 5)
				{
					omSort[i].TrimLeft();
					omSort[i].TrimRight();
				}
				if (omSort[i] == "STOD+")
					int h = 1;

				if (omSort[i].GetLength() == 5)
				{
					
					if (omArrFields.Find(omSort[i].Left(4)) != -1)
					{
						if (strcmp(omSort[i].Right(1), "+") == 0)
							ilCompareResult = (GetFieldContent(prpLine1, omSort[i].Left(4))) == (GetFieldContent(prpLine2, omSort[i].Left(4))) ? 0 : (GetFieldContent(prpLine1, omSort[i].Left(4))) > (GetFieldContent(prpLine2, omSort[i].Left(4))) ? 1 : -1;
						if (strcmp(omSort[i].Right(1), "-") == 0)
							ilCompareResult = (GetFieldContent(prpLine1, omSort[i].Left(4))) == (GetFieldContent(prpLine2, omSort[i].Left(4))) ? 0 : (GetFieldContent(prpLine1, omSort[i].Left(4))) < (GetFieldContent(prpLine2, omSort[i].Left(4))) ? 1 : -1;
					}
				}

				// Check the result of this sorting order, return if unequality is found
				if (ilCompareResult != 0)
					return ilCompareResult;

			}
			return 0;	// we can say that these two lines are equal
		}
	}

	if(ipGroup == UD_AGROUND)
	{
		if(ilCount == 0 || !bmArrField)
		{
			if((prpLine1->Onbl == TIMENULL) && (prpLine2->Onbl == TIMENULL))
			{

				CTime olTime1 = prpLine1->Land;
				CTime olTime2 = prpLine2->Land;

				if(prpLine1->Ftyp == "T" ||  prpLine1->Ftyp == "G")
					olTime1 = prpLine1->Ofbl;

				if(prpLine2->Ftyp == "T" ||  prpLine2->Ftyp == "G")
					olTime2 = prpLine2->Ofbl;


				if(olTime1 == olTime2)
					return (prpLine1->Flno <  prpLine2->Flno)? 1: -1;
				else
					return (olTime1 <  olTime2)? 1: -1;

				/*			
				if(prpLine1->Land == prpLine2->Land)
					return (prpLine1->Flno <  prpLine2->Flno)? 1: -1;
				else
					return (prpLine1->Land <  prpLine2->Land)? 1: -1;
				*/
			
			}

			if((prpLine1->Onbl != TIMENULL) && (prpLine2->Onbl != TIMENULL))
			{
				if(prpLine1->Onbl == prpLine2->Onbl)
					return (prpLine1->Flno <  prpLine2->Flno)? 1: -1;
				else
					return (prpLine1->Onbl <  prpLine2->Onbl)? 1: -1;
			}


			if((prpLine1->Onbl != TIMENULL) && (prpLine2->Onbl == TIMENULL))
				return 1;
			else
				return -1;
		}
		else
		{
			for (int i = 0; i < ilCount; i++)
			{
				ilCompareResult = 0;

//				int il = omSort[i].GetLength();
//				CString cll = omSort[i];
				if (omSort[i].GetLength() > 5)
				{
					omSort[i].TrimLeft();
					omSort[i].TrimRight();
				}

				if (omSort[i].GetLength() == 5)
				{
					if (omArrFields.Find(omSort[i].Left(4)) != -1)
					{
						if (strcmp(omSort[i].Right(1), "+") == 0)
							ilCompareResult = (GetFieldContent(prpLine1, omSort[i].Left(4))) == (GetFieldContent(prpLine2, omSort[i].Left(4))) ? 0 : (GetFieldContent(prpLine1, omSort[i].Left(4))) > (GetFieldContent(prpLine2, omSort[i].Left(4))) ? 1 : -1;
						if (strcmp(omSort[i].Right(1), "-") == 0)
							ilCompareResult = (GetFieldContent(prpLine1, omSort[i].Left(4))) == (GetFieldContent(prpLine2, omSort[i].Left(4))) ? 0 : (GetFieldContent(prpLine1, omSort[i].Left(4))) < (GetFieldContent(prpLine2, omSort[i].Left(4))) ? 1 : -1;
					}
				}

				// Check the result of this sorting order, return if unequality is found
				if (ilCompareResult != 0)
					return ilCompareResult;

			}
			return 0;	// we can say that these two lines are equal
		}
	}

	if(ipGroup == UD_DGROUND)
	{
		if(ilCount == 0 || !bmDepField)
		{
			if((prpLine1->Ofbl == TIMENULL) && (prpLine2->Ofbl == TIMENULL))
			{
				if(prpLine1->Etdi == TIMENULL)
					olTime1 = prpLine1->Stod;
				else
					olTime1 = prpLine1->Etdi;


				if(prpLine2->Etdi == TIMENULL)
					olTime2 = prpLine2->Stod;
				else
					olTime2 = prpLine2->Etdi;


				if(olTime1 == olTime2)
					return (prpLine1->Flno <  prpLine2->Flno)? 1: -1;
				else
					return (olTime1 <  olTime2)? 1: -1;
			}


			if((prpLine1->Ofbl != TIMENULL) && (prpLine2->Ofbl != TIMENULL))
			{
				if(prpLine1->Ofbl == prpLine2->Ofbl)
					return (prpLine1->Flno <  prpLine2->Flno)? 1: -1;
				else
					return (prpLine1->Ofbl <  prpLine2->Ofbl)? 1: -1;
			}

			if((prpLine1->Ofbl != TIMENULL) && (prpLine2->Ofbl == TIMENULL))
				return 1;
			else
				return -1;
		}
		else
		{
			for (int i = 0; i < ilCount; i++)
			{
				ilCompareResult = 0;

//				int il = omSort[i].GetLength();
				CString cll = omSort[i];
				if (omSort[i].GetLength() > 5)
				{
					omSort[i].TrimLeft();
					omSort[i].TrimRight();
				}

				if (omSort[i].GetLength() == 5)
				{
					if (omDepFields.Find(omSort[i].Left(4)) != -1)
					{
						if (strcmp(omSort[i].Right(1), "+") == 0)
							ilCompareResult = (GetFieldContent(prpLine1, omSort[i].Left(4))) == (GetFieldContent(prpLine2, omSort[i].Left(4))) ? 0 : (GetFieldContent(prpLine1, omSort[i].Left(4))) > (GetFieldContent(prpLine2, omSort[i].Left(4))) ? 1 : -1;
						if (strcmp(omSort[i].Right(1), "-") == 0)
							ilCompareResult = (GetFieldContent(prpLine1, omSort[i].Left(4))) == (GetFieldContent(prpLine2, omSort[i].Left(4))) ? 0 : (GetFieldContent(prpLine1, omSort[i].Left(4))) < (GetFieldContent(prpLine2, omSort[i].Left(4))) ? 1 : -1;
					}
				}

				// Check the result of this sorting order, return if unequality is found
				if (ilCompareResult != 0)
					return ilCompareResult;

			}
			return 0;	// we can say that these two lines are equal
		}

	}

	if(ipGroup == UD_DEPARTURE)
	{
		if(ilCount == 0 || !bmDepField)
		{
			if(prpLine1->Airb == prpLine2->Airb)
				return (prpLine1->Flno <  prpLine2->Flno)? 1: -1;
			else
				return (prpLine1->Airb <  prpLine2->Airb)? 1: -1;
		}
		else
		{
			for (int i = 0; i < ilCount; i++)
			{
				ilCompareResult = 0;

//				int il = omSort[i].GetLength();
//				CString cll = omSort[i];
				if (omSort[i].GetLength() > 5)
				{
					omSort[i].TrimLeft();
					omSort[i].TrimRight();
				}

				if (omSort[i].GetLength() == 5)
				{
					if (omDepFields.Find(omSort[i].Left(4)) != -1)
					{
						if (strcmp(omSort[i].Right(1), "+") == 0)
							ilCompareResult = (GetFieldContent(prpLine1, omSort[i].Left(4))) == (GetFieldContent(prpLine2, omSort[i].Left(4))) ? 0 : (GetFieldContent(prpLine1, omSort[i].Left(4))) > (GetFieldContent(prpLine2, omSort[i].Left(4))) ? 1 : -1;
						if (strcmp(omSort[i].Right(1), "-") == 0)
							ilCompareResult = (GetFieldContent(prpLine1, omSort[i].Left(4))) == (GetFieldContent(prpLine2, omSort[i].Left(4))) ? 0 : (GetFieldContent(prpLine1, omSort[i].Left(4))) < (GetFieldContent(prpLine2, omSort[i].Left(4))) ? 1 : -1;
					}
				}

				// Check the result of this sorting order, return if unequality is found
				if (ilCompareResult != 0)
					return ilCompareResult;

			}
			return 0;	// we can say that these two lines are equal
		}
	}
	return 0;
}





//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void RotationTableViewer::GetHeader(int ipGroupID)
{
	omPrintHeadHeaderArray.DeleteAll();
	

	TABLE_HEADER_COLUMN *prlHeader;

	if(ipGroupID == UD_ARRIVAL)
	{
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 66;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING296);
		omPrintHeadHeaderArray.Add( prlHeader );


		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 59;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING297);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 31;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING298);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 38;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING299);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 10;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING300);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 40;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING301);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING323);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING302);		// "ETA"
		omPrintHeadHeaderArray.Add( prlHeader );

		// Shanghai want to see ETOA (ETA Fids)
		if (strcmp(pcgHome, "PVG") == 0 ||
			__CanSeeDailyScheduleColumnsDisplay() )		// 050308 MVy: daily schedule lists should contain additional column for display data: ETA(D) and ETD(D)
		{
			prlHeader = new TABLE_HEADER_COLUMN;
			prlHeader->Alignment = COLALIGN_CENTER;
			prlHeader->Length = 36;	
			prlHeader->Font = pomFont;
			if(__CanUseFIDSHeader())
			{
				prlHeader->Text = GetString(IDS_STRING2909);		// "ETA(FIDS)"
			}
			else
			{
				prlHeader->Text = GetString(IDS_STRING2012);		// "ETA(D)"
			}
			omPrintHeadHeaderArray.Add( prlHeader );
		}

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING303);		// "TMO"
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING304);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 24;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING305);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING306);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;		
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING307);
		omPrintHeadHeaderArray.Add( prlHeader );

		
		// Shanghai want to see  AIRB (ATD of Origin)
		if (strcmp(pcgHome, "PVG") == 0)
		{
			prlHeader = new TABLE_HEADER_COLUMN;
			prlHeader->Alignment = COLALIGN_CENTER;
			prlHeader->Length = 36;	
			prlHeader->Font = pomFont;
			prlHeader->Text = GetString(IDS_STRING321);
			omPrintHeadHeaderArray.Add( prlHeader );
		}


		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING308);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING309);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING1151);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 87;	 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING310);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 38;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING311);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 65;	 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING324);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 35;		
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING2123);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 10;	 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING313);
		omPrintHeadHeaderArray.Add( prlHeader );

		
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 70;//102;	 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING314);
		omPrintHeadHeaderArray.Add( prlHeader );

		
	}


	if(ipGroupID == UD_AGROUND)
	{

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 66;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING296);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 59;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING297);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 31;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING298);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 38;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING299);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 10;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING300);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 40;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING301);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING323);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING302);		// "ETA"
		omPrintHeadHeaderArray.Add( prlHeader );

		// Shanghai want to see ETOA (ETA Fids)
		if (strcmp(pcgHome, "PVG") == 0 ||
			__CanSeeDailyScheduleColumnsDisplay() )		// 050308 MVy: daily schedule lists should contain additional column for display data: ETA(D) and ETD(D)
		{
			prlHeader = new TABLE_HEADER_COLUMN;
			prlHeader->Alignment = COLALIGN_CENTER;
			prlHeader->Length = 36;	
			prlHeader->Font = pomFont;

			//Bankok AOT wnats to see ETA(FIDS)
			if(__CanUseFIDSHeader())
			{
				prlHeader->Text = GetString(IDS_STRING2909);		// "ETA(FIDS)"
			}
			else
			{
				prlHeader->Text = GetString(IDS_STRING2012);		// "ETA(D)"
			}
			omPrintHeadHeaderArray.Add( prlHeader );
		}

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING303);		// "TMO"
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING304);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 24;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING305);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING306);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;		
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING307);
		omPrintHeadHeaderArray.Add( prlHeader );

		
		// Shanghai want to see  AIRB (ATD of Origin)
		if (strcmp(pcgHome, "PVG") == 0)
		{
			prlHeader = new TABLE_HEADER_COLUMN;
			prlHeader->Alignment = COLALIGN_CENTER;
			prlHeader->Length = 36;	
			prlHeader->Font = pomFont;
			prlHeader->Text = GetString(IDS_STRING321);
			omPrintHeadHeaderArray.Add( prlHeader );
		}


		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING308);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING309);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING1151);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 87;	 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING310);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 38;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING311);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 65;	 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING1394);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 35;		
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING2123);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 10;	 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING313);
		omPrintHeadHeaderArray.Add( prlHeader );

	
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 70;//102;	 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING314);
		omPrintHeadHeaderArray.Add( prlHeader );

	}
	
	
	
	if(ipGroupID == UD_DGROUND)
	{
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 66; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING296);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 58; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING297);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 29; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING315);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING299);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 10; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING300);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 40;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING301);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING316);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING317);		// "ETD"
		omPrintHeadHeaderArray.Add( prlHeader );

		// Shanghai want to see ETOD (ETD Fids)
		if (strcmp(pcgHome, "PVG") == 0 ||
			__CanSeeDailyScheduleColumnsDisplay() )		// 050308 MVy: daily schedule lists should contain additional column for display data: ETA(D) and ETD(D)
		{
			prlHeader = new TABLE_HEADER_COLUMN;
			prlHeader->Alignment = COLALIGN_CENTER;
			prlHeader->Length = 36; 
			prlHeader->Font = pomFont;
			if(__CanUseFIDSHeader())
			{
				prlHeader->Text = GetString(IDS_STRING2013);		// "ETD(FIDS)"
			}
			else
			{
				prlHeader->Text = GetString(IDS_STRING2910);		// "ETD(D)"
			}
			omPrintHeadHeaderArray.Add( prlHeader );
		}

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;

		if(bgCDMPhase1)
		{
			// Showing TOBT instead of COB -- Lisbon CDM
			prlHeader->Length = 45; 
			prlHeader->Text = GetString(IDS_STRING2880);		// "TOBT"
		}
		else
		{
			prlHeader->Length = 36; 
			prlHeader->Text = GetString(IDS_STRING318);		// "COB"
		}

		prlHeader->Font = pomFont;
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING319);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING320);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 24; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING305);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING321);
		omPrintHeadHeaderArray.Add( prlHeader );
	
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING308);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING2014);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING2015);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		// Checkin-Counter from-to
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;

		if(bgSecondGateDemandFlag)
			prlHeader->Length = 120; 
		else
		{
			prlHeader->Length = 50; 
			if (bgCnamAtr)
				prlHeader->Length = 130; 
		}
		
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING2126);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 87; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING310);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING311);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 65; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING1395);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 35;		
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING2123);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 10; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING313);
		omPrintHeadHeaderArray.Add( prlHeader );

		

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 50; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING314);
		omPrintHeadHeaderArray.Add( prlHeader );
		

	}

	if(ipGroupID == UD_DEPARTURE)
	{
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 66; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING296);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 58; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING297);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 29; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING315);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING299);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 10; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING300);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 40;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING301);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING316);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING317);		// "ETD"
		omPrintHeadHeaderArray.Add( prlHeader );

		// Shanghai want to see ETOD (ETD Fids)
		if (strcmp(pcgHome, "PVG") == 0 ||
			__CanSeeDailyScheduleColumnsDisplay() )		// 050308 MVy: daily schedule lists should contain additional column for display data: ETA(D) and ETD(D)
		{
			prlHeader = new TABLE_HEADER_COLUMN;
			prlHeader->Alignment = COLALIGN_CENTER;
			prlHeader->Length = 36; 
			prlHeader->Font = pomFont;
			if (__CanUseFIDSHeader())
			{
				prlHeader->Text = GetString(IDS_STRING2910);		// "ETD(FIDS)"
			}
			else
			{
				prlHeader->Text = GetString(IDS_STRING2013);		// "ETD(D)"
			}
			omPrintHeadHeaderArray.Add( prlHeader );
		}

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;

		if(bgCDMPhase1)
		{
			// Showing TOBT instead of COB -- Lisbon CDM
			prlHeader->Length = 45; 
			prlHeader->Text = GetString(IDS_STRING2880);		// "TOBT"
		}
		else
		{
			prlHeader->Length = 36; 
			prlHeader->Text = GetString(IDS_STRING318);		// "COB"
		}
		
		prlHeader->Font = pomFont;
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING319);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING320);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 24; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING305);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING321);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING308);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING2014);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING2015);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 50; 
		if (bgCnamAtr)
			prlHeader->Length = 130; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING2126);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 87; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING310);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING311);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 65; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING1396);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 35;		
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING2123);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 10; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING313);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 50; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING314);
		omPrintHeadHeaderArray.Add( prlHeader );

		
	
	}

}


void RotationTableViewer::PrintAllTableViews(CUIntArray& opPrintArray)  
{
	 CString olFooter1,olFooter2;
	 CString olTableName = "";
	 int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	 int ilInitprinter = TRUE;

	CString olTimes;
	if (bgDailyLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);


	int testPrint = 0;
	for (int k = opPrintArray.GetSize()-1; k >=0; k--)
		testPrint += opPrintArray.GetAt(k);

	if (testPrint == 0)
		return;


	ASSERT(omGroups.GetSize() == opPrintArray.GetSize());
	bool blInitPrinter = true;

 for(int ipGroupID = omGroups.GetSize()-1; ipGroupID >= 0; ipGroupID--)  // der letzte == Endekennung
 {
	switch(ipGroupID)
	{
		case 0:
			{
				olTableName  = GetString(IDS_STRING325); //"Tagesflugplan - Anflug";
			}
			break;
		case 1:
			{
				olTableName  = GetString(IDS_STRING326);//"Tagesflugplan - Boden - Anflug";
			}
			break;
		case 2:
			{
				olTableName  = GetString(IDS_STRING327);//"Tagesflugplan - Boden - Abflug";
			}
			break;
		case 3:
			{
				olTableName  = GetString(IDS_STRING328);//"Tagesflugplan - Abflug";
			}
			break;
	}

	if (opPrintArray.GetAt(ipGroupID) == 0)
		continue;

//	if (ipGroupID == omGroups.GetSize() - 1)	// beim ersten
	if (blInitPrinter)	// beim ersten
		pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)

	GROUP_DATA *prlGroupData = &omGroups[ipGroupID];
	//int ilRet = TRUE;

	if (pomPrint != NULL)
	{
//		if (ipGroupID == omGroups.GetSize() - 1)	// beim ersten
		if (blInitPrinter)	// beim ersten
			ilInitprinter = pomPrint->InitializePrinter(ilOrientation);

		if (ilInitprinter == TRUE)
		{
			int ilLines = prlGroupData->Lines.GetSize();
			omTableName.Format(GetString(IDS_ROTTABLES_EXC), olTableName, ilLines, olTimes, /*(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M"))*/" " );

			char pclFooter[256];
			sprintf(pclFooter, omTableName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
			omFooterName = pclFooter;


			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	

//			if (ipGroupID == omGroups.GetSize() - 1)	// beim ersten
			if (blInitPrinter)	// beim ersten
				pomPrint->omCdc.StartDoc( &rlDocInfo );
			blInitPrinter = false;

			pomPrint->imPageNo = 0;
			//pomPrint->imLineNo = 0;	// neu
//			int ilLines = prlGroupData->Lines.GetSize();
			olFooter1.Format("%s -   %s", omTableName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );
//			olFooter1.Format(GetString(IDS_STRING1445),ilLines,olTableName);

			if (ilLines == 0)
				PrintTableHeader(ipGroupID); // Druckt auch leere Tabelle
				
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
						pomPrint->imLineNo = 0;	// neu
					}
					
					PrintTableHeader(ipGroupID);
				
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&prlGroupData->Lines[i],true,ipGroupID);
					
				}
				else
				{
					PrintTableLine(&prlGroupData->Lines[i],false,ipGroupID);
					
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();	//--
			//--pomPrint->omCdc.EndDoc();	
		}
		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		//--delete pomPrint;
		//--pomPrint = NULL;
	}


 }

 if (ilInitprinter == TRUE)
 {
	pomPrint->omCdc.EndDoc();
 }
 delete pomPrint;
 pomPrint = NULL;

}

//-----------------------------------------------------------------------------------------------

bool RotationTableViewer::PrintTableHeader(int ipGroupID)
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	m_dblPrintFactor = 1 ;		// 050309 MVy: for automatic calculation

	CString olTableName(GetString(IDS_STRING329)); //"Tagesflugplan"
/*
	switch(ipGroupID)
	{
		case 0:
			{
				olTableName  = GetString(IDS_STRING325);//"Tagesflugplan - Anflug";
				dgCCSPrintFactor = 3.0;
			}
			break;
		case 1:
			{
				olTableName  = GetString(IDS_STRING326);//"Tagesflugplan - Boden - Anflug";
				dgCCSPrintFactor = 3.0;
			}
			break;
		case 2:
			{
				olTableName  = GetString(IDS_STRING327);//"Tagesflugplan - Boden - Abflug";
				dgCCSPrintFactor = 2.7;
			}
			break;
		case 3:
			{
				olTableName  = GetString(IDS_STRING328);//"Tagesflugplan - Abflug";
				dgCCSPrintFactor = 2.7;

			}
			break;
		default:
				olTableName  = GetString(IDS_STRING329);//"Tagesflugplan";
	}
*/
	GetHeader(ipGroupID);
	//pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);
//	pomPrint->PrintUIFHeader(olTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);
	pomPrint->PrintUIFHeader("", omTableName, pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	// 050309 MVy: a variing number of enabled or disabled columns causes different table width
	//	try a calculation of needed space / print factor
	{
		int ilSize = omPrintHeadHeaderArray.GetSize();
		int sum = 0 ;
		TRACE( "print rotation table\n" );
		for(int i=0;i<ilSize;i++)
		{
				rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length );
				sum += rlElement.Length ;
				TRACE( "  %d\t%s\t%d\n", i+1, omPrintHeadHeaderArray[i].Text, omPrintHeadHeaderArray[i].Length );
		};
		TRACE( "%d items with length %d\n", ilSize, sum );
		m_dblPrintFactor = (double )2250 / sum ;
	}

	//int ilSize = omHeaderDataArray.GetSize();
	int ilSize = omPrintHeadHeaderArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
			//rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length * m_dblPrintFactor );
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			//rlElement.Text   = omHeaderDataArray[i].Text;
			rlElement.Text   = omPrintHeadHeaderArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------
/*
void RotationTableViewer::PrintTableLine(ROTATIONTABLE_LINEDATA *prpLine,bool bpLastLine,int ipGroupID)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	//int ilSize = omHeaderDataArray.GetSize();

	if((ipGroupID == UD_ARRIVAL) || (ipGroupID == UD_AGROUND))
		dgCCSPrintFactor = 3.0; 

	int ilSize = omPrintHeadHeaderArray.GetSize();
	for(int i = 0; i < ilSize; i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if (rlElement.Length < igCCSPrintMinLength)
			rlElement.Length+=igCCSPrintMoreLength;
		
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;


		if((ipGroupID == UD_ARRIVAL) || (ipGroupID == UD_AGROUND))
		{
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING296))
			{
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Flno;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING297))
			{
				rlElement.Text		= prpLine->Csgn;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING298))
			{
				rlElement.Text		= prpLine->Org4;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING299))
			{
				rlElement.Text		= prpLine->Vial;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING300))
			{
				rlElement.Text		= prpLine->Ifra;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING301))
			{
				//rlElement.Alignment = PRINT_RIGHT;
				rlElement.Text		= prpLine->Ttyp;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING323))
			{
				rlElement.Text		= prpLine->Stoa.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING302))
			{
				rlElement.Text		= prpLine->Etai.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING2012))
			{
				rlElement.Text		= prpLine->Etoa.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING303))
			{
				rlElement.Text		= prpLine->Tmoa.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING304))
			{
				rlElement.Text		= prpLine->Land.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING305))
			{
				rlElement.Text		= prpLine->Rwya;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING306))
			{
				rlElement.Text		= prpLine->Onbe.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING307))
			{
				rlElement.Text		= prpLine->Onbl.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING321))
			{
				rlElement.Text		= prpLine->Airb.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING308))
			{
				rlElement.Text		= prpLine->Psta;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING309))
			{
				rlElement.Text		= prpLine->Gta1;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING1151))
			{
				rlElement.Text		= prpLine->Blt1;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING310))
			{
				rlElement.Text		= prpLine->Regn;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING311))
			{
				rlElement.Text		= prpLine->Act5;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING324))
			{
				rlElement.Text		= prpLine->Stoa.Format("%d.%m.%y");
			}								
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING1394))
			{
				rlElement.Text		= prpLine->Land.Format("%d.%m.%y");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING313))
			{
				rlElement.Text		= prpLine->Isre;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING314))
			{
				rlElement.Text		= prpLine->Remp;
				rlElement.FrameRight = PRINT_FRAMETHIN;
			}

		}
		else 
		{
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING296))
			{
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Flno;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING297))
			{
				rlElement.Text		= prpLine->Csgn;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING315))
			{
				rlElement.Text		= prpLine->Des4;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING299))
			{
				rlElement.Text		= prpLine->Vial;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING300))
			{
				rlElement.Text		= prpLine->Ifrd;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING301))
			{
				rlElement.Text		= prpLine->Ttyp;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING316))
			{
				rlElement.Text		= prpLine->Stod.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING317))
			{
				rlElement.Text		= prpLine->Etdi.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING2013))
			{
				rlElement.Text		= prpLine->Etod.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING318))
			{
				rlElement.Text		= prpLine->Etdc.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING319))
			{
				rlElement.Text		= prpLine->Ofbl.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING320))
			{
				rlElement.Text		= prpLine->Slot.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING305))
			{
				rlElement.Text		= prpLine->Rwyd;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING321))
			{
				rlElement.Text		= prpLine->Airb.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING308))
			{
				rlElement.Text		= prpLine->Pstd;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING2014))
			{
				rlElement.Text		= prpLine->Gtd1;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING2015))
			{
				rlElement.Text		= prpLine->Gtd2;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING310))
			{
				rlElement.Text		= prpLine->Regn;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING311))
			{
				rlElement.Text		= prpLine->Act5;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING1395))
			{
				rlElement.Text		= prpLine->Stod.Format("%d.%m.%y");
			}

			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING1396))
			{
					rlElement.Text		= prpLine->Airb.Format("%d.%m.%y");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING312))
			{
				rlElement.Text		= prpLine->Isre;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING313))
			{
				rlElement.Text		= prpLine->Remp;
				rlElement.FrameRight = PRINT_FRAMETHIN;
			}
		}


		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
}
*/

void RotationTableViewer::PrintTableLine(ROTATIONTABLE_LINEDATA *prpLine,bool bpLastLine,int ipGroupID)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	if (bgCnamAtr)
		rlElement.pFont      = &pomPrint->ogCourierNew_Regular_6;
	//int ilSize = omHeaderDataArray.GetSize();

//	if((ipGroupID == UD_ARRIVAL) || (ipGroupID == UD_AGROUND))
//		dgCCSPrintFactor = 3.0; 

	int ilSize = omPrintHeadHeaderArray.GetSize();
	for(int i = 0; i < ilSize; i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length * m_dblPrintFactor );
		if (rlElement.Length < igCCSPrintMinLength)
			rlElement.Length+=igCCSPrintMoreLength;
		
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_FRAMETHIN;
		rlElement.FrameRight = PRINT_NOFRAME;


		if((ipGroupID == UD_ARRIVAL) || (ipGroupID == UD_AGROUND))
		{
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING296))
			{
				rlElement.Text		= prpLine->Flno;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING297))
			{
				rlElement.Text		= prpLine->Csgn;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING298))
			{
				rlElement.Text		= prpLine->Org4;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING299))
			{
				rlElement.Text		= prpLine->Vial;
				if (!rlElement.Text.IsEmpty())
					rlElement.Text		+= "," + prpLine->Vian;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING300))
			{
				rlElement.Text		= prpLine->Ifra;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING301))
			{
				//rlElement.Alignment = PRINT_RIGHT;
				rlElement.Text		= prpLine->Ttyp;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING323))
			{
				rlElement.Text		= prpLine->Stoa.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING302))
			{
				rlElement.Text		= prpLine->Etai.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING2012) || omPrintHeadHeaderArray[i].Text== GetString(IDS_STRING2909))
			{
				rlElement.Text		= prpLine->Etoa.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING303))
			{
				rlElement.Text		= prpLine->Tmoa.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING304))
			{
				rlElement.Text		= prpLine->Land.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING305))
			{
				rlElement.Text		= prpLine->Rwya;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING306))
			{
				rlElement.Text		= prpLine->Onbe.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING307))
			{
				rlElement.Text		= prpLine->Onbl.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING321))
			{
				rlElement.Text		= prpLine->Airb.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING308))
			{
				rlElement.Text		= prpLine->Psta;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING309))
			{
				rlElement.Text		= prpLine->Gta1;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING1151))
			{
				rlElement.Text		= prpLine->Blt1;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING310))
			{
				rlElement.Text		= prpLine->Regn;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING311))
			{
				rlElement.Text		= prpLine->Act5;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING324))
			{
				rlElement.Text		= prpLine->Stoa.Format("%d.%m.%y");
			}								
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING1394))
			{
				rlElement.Text		= prpLine->Land.Format("%d.%m.%y");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING313))
			{
				rlElement.Text		= prpLine->Isre;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING2946))
			{
				rlElement.Text		= "";
			}

			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING2123))
			{
				rlElement.Text		= prpLine->Ftyp;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING314))
			{
				rlElement.Text		= prpLine->Remp;
				rlElement.FrameRight = PRINT_FRAMETHIN;
			}

		}
		else 
		{
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING296))
			{
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Flno;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING297))
			{
				rlElement.Text		= prpLine->Csgn;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING315))
			{
				rlElement.Text		= prpLine->Des4;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING299))
			{
				rlElement.Text		= prpLine->Vial;
				if (!rlElement.Text.IsEmpty())
					rlElement.Text		+= "," + prpLine->Vian;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING300))
			{
				rlElement.Text		= prpLine->Ifrd;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING301))
			{
				rlElement.Text		= prpLine->Ttyp;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING316))
			{
				rlElement.Text		= prpLine->Stod.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING317))
			{
				rlElement.Text		= prpLine->Etdi.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING2013) || omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING2910)) 
			{
				rlElement.Text		= prpLine->Etod.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING2880))
			{
				rlElement.Text		= prpLine->Tobt.Format("%H:%M") + CString(prpLine->Tobf);
			}

			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING318))
			{
				rlElement.Text		= prpLine->Etdc.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING319))
			{
				rlElement.Text		= prpLine->Ofbl.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING320))
			{
				rlElement.Text		= prpLine->Slot.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING305))
			{
				rlElement.Text		= prpLine->Rwyd;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING321))
			{
				rlElement.Text		= prpLine->Airb.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING308))
			{
				rlElement.Text		= prpLine->Pstd;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING2014))
			{
				rlElement.Text		= prpLine->Gtd1;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING2015))
			{
				rlElement.Text		= prpLine->Gtd2;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING2126))
			{
				rlElement.Text		= prpLine->Cic;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING310))
			{
				rlElement.Text		= prpLine->Regn;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING311))
			{
				rlElement.Text		= prpLine->Act5;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING1395))
			{
				rlElement.Text		= prpLine->Stod.Format("%d.%m.%y");
			}

			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING1396))
			{
					rlElement.Text		= prpLine->Airb.Format("%d.%m.%y");
			}
/*			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING312))
			{
				rlElement.Text		= prpLine->Isre;
			}*/
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING2123))
			{
				rlElement.Text		= prpLine->Ftyp;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING313))
			{
				rlElement.Text		= prpLine->Isre;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING2946))
			{
				rlElement.Text		= "";
			}

			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING314))
			{
				rlElement.Text		= prpLine->Remp;
				rlElement.FrameRight = PRINT_FRAMETHIN;
			}
		}


		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------
BOOL RotationTableViewer::CheckPostFlight(const ROTATIONTABLE_LINEDATA *prpTableLine)
{

	if (prpTableLine == NULL)
		return FALSE;

	ROTATIONFLIGHTDATA	*prlFlight = ogRotationFlights.GetFlightByUrno(prpTableLine->Urno);

	if (prlFlight == NULL)
		return FALSE;

	ROTATIONFLIGHTDATA *prlAFlight = ogRotationFlights.GetArrival(prlFlight);
	ROTATIONFLIGHTDATA *prlDFlight = ogRotationFlights.GetDeparture(prlFlight);

	return CheckPostFlight (prlAFlight, prlDFlight);

}


BOOL RotationTableViewer::CheckPostFlight(const ROTATIONFLIGHTDATA *prlAFlight, const ROTATIONFLIGHTDATA *prlDFlight)
{

	BOOL blPost = FALSE;

	if (prlAFlight && prlDFlight)
	{
		if (prlAFlight->Tifa != TIMENULL && prlDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prlAFlight->Tifa,false,prlAFlight->Prfl) && IsPostFlight(prlDFlight->Tifd,false,prlDFlight->Prfl))
				blPost = TRUE;
		}
	}
	else if (!prlAFlight && prlDFlight)
	{
		if (prlDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prlDFlight->Tifd,false,prlDFlight->Prfl))
				blPost = TRUE;
		}
	}
	else if (prlAFlight && !prlDFlight)
	{
		if (prlAFlight->Tifa != TIMENULL)
		{
			if (IsPostFlight(prlAFlight->Tifa,false,prlAFlight->Prfl))
				blPost = TRUE;
		}
	}


	return blPost;
}


bool RotationTableViewer::CreateExcelFile( CString opTrenner, bool bArrival, bool bLanded, bool bDeparture, bool bAirborn )
{
	ofstream of;
	mySeparator = opTrenner;

	CString olTimes;
	if (bgDailyLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);


	CString olTableName = GenerateTableName();		// 050302 MVy: use same for dialog title and excel export
	olTableName += "  " + CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M");

	CString olFileName = olTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);


	of  << setw(1) << olTableName << endl;

	for(int ipGroupID = omGroups.GetSize()-1; ipGroupID >= 0; ipGroupID--)  // der letzte == Endekennung
	{
		bool bHandleCurrentGroup = true ;		// check wheter the current group should be handled or not
		// 050302 MVy: selection what part should be exported
		switch( ipGroupID )
		{
			case UD_ARRIVAL: bHandleCurrentGroup = bArrival ; break ;
			case UD_AGROUND: bHandleCurrentGroup = bLanded ; break ;
			case UD_DGROUND: bHandleCurrentGroup = bDeparture ; break ;
			case UD_DEPARTURE: bHandleCurrentGroup = bAirborn ; break ;
			default : { ASSERT(0); };		// some unexpected data arrived here
		};

		if( bHandleCurrentGroup )
		{
			GROUP_DATA *prlGroupData = &omGroups[ipGroupID];
			int ilLines = prlGroupData->Lines.GetSize();

			PrintExcelHeader(of, opTrenner, ipGroupID, ilLines); // Druckt auch leere Tabelle
				
			for(int i = 0; i < ilLines; i++ ) 
			{
				ROTATIONTABLE_LINEDATA rlLine = prlGroupData->Lines[i];
				PrintExcelLine(of, &rlLine, opTrenner, ipGroupID);
			}
		};
	}

	of.close();
	return true;

}

bool RotationTableViewer::PrintExcelHeaderArr(ofstream &of, CString opTrenner)
{
		of	<< setw(1) << GetString(IDS_STRING296)//Flight
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING297)//C/S
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING298)//ORG
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING299)//VIA
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2125)//nVIA
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING300)//A
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING301)//Na
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING323)//STA
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING302)//ETA
		;
		if( __CanSeeDailyScheduleColumnsDisplay() )		// 050311 MVy: daily schedule lists should contain additional column for display data: ETA(D) and ETD(D)
		{
			if(__CanUseFIDSHeader())
			{
				of << setw(1) << opTrenner
					<< setw(1) << GetString(IDS_STRING2909) ;//"ETA(FIDS)"
			}
			else
			{
				of << setw(1) << opTrenner
					<< setw(1) << GetString(IDS_STRING2012) ;//"ETA(D)"
			}
		};
		of << setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING303)//TMO
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING304)//ATA
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING305)//RWY
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING306)//EON
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING307)//ONB
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING308)//POS
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING309)//GAT
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING1151)//Belt
			<< setw(1) << opTrenner;


			if(bgExcelFLBag)
			{
				of << setw(7) << GetString(IDS_STRING2873) //CString("F Bag");
				<< setw(1) << opTrenner
				<< setw(7) << GetString(IDS_STRING2874) //CString("L Bag");
				<< setw(1) << opTrenner;
			}

			of << setw(1) << GetString(IDS_STRING310)//REG
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING311)//A/C
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING324)//DateSTA
			<< setw(1) << opTrenner
//			<< setw(1) << GetString(IDS_STRING313)//I
//			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2123)//State
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING314)//REM
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2442)//RemARRSeas
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2120);//RemARRDaily
			if(bgAdditionalRemark)
			{
				of << setw(1) << opTrenner
				<< setw(1) << GetString(IDS_STRING2925);//Rem 2 ARRDaily
			}
			of << endl;

		return true;
}

bool RotationTableViewer::PrintExcelHeaderArrGround(ofstream &of, CString opTrenner)
{
		of	<< setw(1) << GetString(IDS_STRING296)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING297)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING298)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING299)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2125)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING300)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING301)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING323)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING302)
		;
		if( __CanSeeDailyScheduleColumnsDisplay() )		// 050311 MVy: daily schedule lists should contain additional column for display data: ETA(D) and ETD(D)
		{
			if(__CanUseFIDSHeader())
			{
				of << setw(1) << opTrenner
					<< setw(1) << GetString(IDS_STRING2909);//"ETA(FIDS)" 
			}
			else
			{
				of << setw(1) << opTrenner
					<< setw(1) << GetString(IDS_STRING2012);//"ETA(D)" 
			}
		};
		of << setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING303)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING304)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING305)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING306)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING307)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING308)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING309)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING1151)
			<< setw(1) << opTrenner;


			if(bgExcelFLBag)
			{
				of << setw(7) << GetString(IDS_STRING2873) //CString("F Bag");
				<< setw(1) << opTrenner
				<< setw(7) << GetString(IDS_STRING2874) //CString("L Bag");
				<< setw(1) << opTrenner;
			}

			of << setw(1) << GetString(IDS_STRING310)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING311)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING1394)
			<< setw(1) << opTrenner
//			<< setw(1) << GetString(IDS_STRING312)
//			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2123)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING314)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2442)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2120)
			<< endl;

		return true;
}

bool RotationTableViewer::PrintExcelHeaderDep(ofstream &of, CString opTrenner)
{
		of	<< setw(1) << GetString(IDS_STRING296)//Flight
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING297)//C/S
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING315)//DES
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING299)//VIA
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2125)//nVIA
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING300)//A
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING301)//Na
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING316)//STD
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING317)//ETD
		;
		if( __CanSeeDailyScheduleColumnsDisplay() )		// 050311 MVy: daily schedule lists should contain additional column for display data: ETA(D) and ETD(D)
		{
			if(__CanUseFIDSHeader())
			{
				of << setw(1) << opTrenner
					<< setw(1) << GetString(IDS_STRING2910) ;//ETD(FIDS)
			}
			else
			{
				of << setw(1) << opTrenner
					<< setw(1) << GetString(IDS_STRING2013) ;//ETD(D) 
			}
		};
		of << setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING318)//COB
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING319)//OFB
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING320)//CTOT
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING305)//RWY
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING321)//ATD
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING308)//POS
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2014)//GAT1
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2015)//GAT2
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2126)//CTR
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING310)//REG
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING311)//A/C
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING1396)//DateATD
			<< setw(1) << opTrenner
//			<< setw(1) << GetString(IDS_STRING312)//UPD
//			<< setw(1) << opTrenner
//			<< setw(1) << GetString(IDS_STRING313)//I
//			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2123)//STATE
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING314)//REM
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2121)//RemDEPSeas
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2122);//RemDEPDaily
			if(bgAdditionalRemark)
			{
				of << setw(1) << opTrenner
				<< setw(1) << GetString(IDS_STRING2926);//Rem 2 DEP Daily
			}
			of << endl;

		return true;

}

bool RotationTableViewer::PrintExcelHeaderDepGround(ofstream &of, CString opTrenner)
{
		of	<< setw(1) << GetString(IDS_STRING296)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING297)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING315)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING299)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2125)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING300)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING301)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING316)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING317)
		;
		if( __CanSeeDailyScheduleColumnsDisplay() )		// 050311 MVy: daily schedule lists should contain additional column for display data: ETA(D) and ETD(D)
		{
			if(__CanUseFIDSHeader())
			{
				of << setw(1) << opTrenner
					<< setw(1) << GetString(IDS_STRING2910) ; //ETD(FIDS) 
			}
			else
			{
				of << setw(1) << opTrenner
					<< setw(1) << GetString(IDS_STRING2013) ; //ETD(D) 
			}
		};
		of << setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING318)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING319)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING320)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING305)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING321)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING308)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2014)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2015)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2126)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING310)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING311)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING1395)
			<< setw(1) << opTrenner
//			<< setw(1) << GetString(IDS_STRING312)
//			<< setw(1) << opTrenner
//			<< setw(1) << GetString(IDS_STRING313)
//			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2123)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING314)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2121)
			<< setw(1) << opTrenner
			<< setw(1) << GetString(IDS_STRING2122)
			<< endl;

		return true;

}


bool RotationTableViewer::PrintExcelHeader(ofstream &of, CString opTrenner, int ipGroupID, int ipLines)
{
	of	<< endl;

	CString olTimes;
	if (bgDailyLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);

	CString olGroup ("");

	if(ipGroupID == UD_ARRIVAL)
		olGroup = GetString(IDS_STRING325);
	else if(ipGroupID == UD_AGROUND)
		olGroup = GetString(IDS_STRING326);
	else if(ipGroupID == UD_DEPARTURE)
		olGroup = GetString(IDS_STRING328);
	else if(ipGroupID == UD_DGROUND)
		olGroup = GetString(IDS_STRING327);


	CString olTableName ("");
	olTableName.Format(GetString(IDS_ROTTABLES_EXC), olGroup, ipLines, olTimes, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );

	of  << setw(1) << olTableName
		<< endl;


	if(ipGroupID == UD_ARRIVAL || ipGroupID == UD_AGROUND)
	{
		if(ipGroupID == UD_ARRIVAL)
			return PrintExcelHeaderArr(of, opTrenner);
		else
			return PrintExcelHeaderArrGround(of, opTrenner);

	}

	if(ipGroupID == UD_DGROUND || ipGroupID == UD_DEPARTURE)
	{
		if(ipGroupID == UD_DEPARTURE)
			return PrintExcelHeaderDep(of, opTrenner);
		else
			return PrintExcelHeaderDepGround(of, opTrenner);

	}

	return true;
}

bool RotationTableViewer::CleanSteurzeichen (char *opChar, CString &opString)
{
	opString.Replace(mySeparator, " ");
	opString.Replace(",", " ");
	strcpy (opChar,  opString);

	if (strlen(opChar) > 0)
	{
		for (int z1 = 0; opChar[z1] != '\0'; ++z1)
		{
			if (opChar[z1] >= 0 && opChar[z1] <= 31)
				opChar[z1] = ' ';
		}
	}
	return true;
}


void RotationTableViewer::PrintExcelLineArr(ofstream &of, ROTATIONTABLE_LINEDATA *prpLine, CString opTrenner)
{

		char olRem1[256];
		CleanSteurzeichen (olRem1, prpLine->Rem1);
		CString Rem1 = olRem1;
		Rem1.Remove('\0');

		char olRem2[256];
		CleanSteurzeichen (olRem2, prpLine->Rem2);
		CString Rem2 = olRem2;
		Rem2.Remove('\0');

		char olDela[256];
		CleanSteurzeichen (olDela, prpLine->Dela);
		CString Dela = olDela;
		Dela.Remove('\0');

		prpLine->Blt1.Replace("/"," ");
		of.setf(ios::left, ios::adjustfield);

		of  << setw(1) << prpLine->Flno 
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Csgn
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Org4
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Vial
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Vian
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Ifra
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Ttyp
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Stoa.Format("%H:%M")
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Etai.Format("%H:%M")
		;
		if( __CanSeeDailyScheduleColumnsDisplay() )		// 050311 MVy: daily schedule lists should contain additional column for display data: ETA(D) and ETD(D)
		{
			
			of << setw(1) << opTrenner
				<< setw(1) << prpLine->Etoa.Format( "%H:%M" );
		
		};
		of << setw(1) << opTrenner 
		    << setw(1) << prpLine->Tmoa.Format("%H:%M")
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Land.Format("%H:%M")
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Rwya
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Onbe.Format("%H:%M")
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Onbl.Format("%H:%M")
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Psta
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Gta1
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Blt1;


			if(bgExcelFLBag)
			{
		     of << setw(1) << opTrenner 
			 << setw(7) << prpLine->B1ba.Format("%H:%M")
		     << setw(1) << opTrenner 
			 << setw(7) << prpLine->B1ea.Format("%H:%M");
			}

		    of << setw(1) << opTrenner
		    << setw(1) << prpLine->Regn
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Act5
		    << setw(1) << opTrenner 
				<< setw(1) << prpLine->Stoa.Format(" %d.%m.%y")
				<< setw(1) << opTrenner 
//		    << setw(1) << prpLine->Isre
//		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Ftyp
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Remp
		    << setw(1) << opTrenner 
//		    << setw(1) << prpLine->Rem2
		    << setw(1) << Rem2
		    << setw(1) << opTrenner 
//		    << setw(1) << prpLine->Rem1
		    << setw(1) << Rem1;
			if(bgAdditionalRemark)
			{
				of << setw(1) << opTrenner 		    
				<< setw(1) << Dela;
			}
			of << endl;

}

void RotationTableViewer::PrintExcelLineArrGround(ofstream &of, ROTATIONTABLE_LINEDATA *prpLine, CString opTrenner)
{

		char olRem1[256];
		CleanSteurzeichen (olRem1, prpLine->Rem1);
		CString Rem1 = olRem1;
		Rem1.Remove('\0');

		char olRem2[256];
		CleanSteurzeichen (olRem2, prpLine->Rem2);
		CString Rem2 = olRem2;
		Rem2.Remove('\0');

		prpLine->Blt1.Replace("/"," ");
		of.setf(ios::left, ios::adjustfield);

		of  << setw(1) << prpLine->Flno 
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Csgn
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Org4
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Vial
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Vian
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Ifra
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Ttyp
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Stoa.Format("%H:%M")
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Etai.Format("%H:%M")
		;
		if( __CanSeeDailyScheduleColumnsDisplay() )		// 050311 MVy: daily schedule lists should contain additional column for display data: ETA(D) and ETD(D)
		{
			of << setw(1) << opTrenner
				<< setw(1) << prpLine->Etoa.Format( "%H:%M" );
		};
		of << setw(1) << opTrenner 
		    << setw(1) << prpLine->Tmoa.Format("%H:%M")
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Land.Format("%H:%M")
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Rwya
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Onbe.Format("%H:%M")
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Onbl.Format("%H:%M")
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Psta
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Gta1
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Blt1;

			if(bgExcelFLBag)
			{
		     of << setw(1) << opTrenner 
			 << setw(7) << prpLine->B1ba.Format("%H:%M")
		     << setw(1) << opTrenner 
			 << setw(7) << prpLine->B1ea.Format("%H:%M");
			}


		    of << setw(1) << opTrenner 
		    << setw(1) << prpLine->Regn
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Act5
		    << setw(1) << opTrenner 
				<< setw(1) << prpLine->Land.Format(" %d.%m.%y")
				<< setw(1) << opTrenner 
//				<< setw(1) << "???"
//				<< setw(1) << opTrenner 
//		    << setw(1) << prpLine->Isre
//		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Ftyp
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Remp
		    << setw(1) << opTrenner 
		    << setw(1) << Rem2
		    << setw(1) << opTrenner 
		    << setw(1) << Rem1
			<< endl;

}

void RotationTableViewer::PrintExcelLineDepGround(ofstream &of, ROTATIONTABLE_LINEDATA *prpLine, CString opTrenner)
{

		char olRem1[256];
		CleanSteurzeichen (olRem1, prpLine->Rem1);
		CString Rem1 = olRem1;
		Rem1.Remove('\0');

		char olRem2[256];
		CleanSteurzeichen (olRem2, prpLine->Rem2);
		CString Rem2 = olRem2;
		Rem2.Remove('\0');

		of.setf(ios::left, ios::adjustfield);

		of  << setw(1) << prpLine->Flno 
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Csgn
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Des4
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Vial
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Vian
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Ifrd
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Ttyp
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Stod.Format("%H:%M")
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Etdi.Format("%H:%M")
		;
		if( __CanSeeDailyScheduleColumnsDisplay() )		// 050311 MVy: daily schedule lists should contain additional column for display data: ETA(D) and ETD(D)
		{
			of << setw(1) << opTrenner
				<< setw(1) << prpLine->Etod.Format( "%H:%M" );
		};
		if(bgCDMPhase1)
		{
			of << setw(1) << opTrenner 
				<< setw(1) << prpLine->Tobt.Format("%H:%M")
				<< setw(1) << prpLine->Tobf;
		}
		else
		{
			of << setw(1) << opTrenner 
				<< setw(1) << prpLine->Etdc.Format("%H:%M");
		}

		of  << setw(1) << opTrenner 
		    << setw(1) << prpLine->Ofbl.Format("%H:%M")
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Slot.Format("%H:%M")
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Rwyd
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Airb.Format("%H:%M")
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Pstd
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Gtd1
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Gtd2
		    << setw(1) << opTrenner 
		    << setw(1) << CString(" ") + prpLine->Cic
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Regn
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Act5
		    << setw(1) << opTrenner 
				<< setw(1) << prpLine->Stod.Format(" %d.%m.%y")
				<< setw(1) << opTrenner 
//		    << setw(1) << prpLine->Isre
//		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Ftyp
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Remp
		    << setw(1) << opTrenner 
		    << setw(1) << Rem2
		    << setw(1) << opTrenner 
		    << setw(1) << Rem1
			<< endl;

}

void RotationTableViewer::PrintExcelLineDep(ofstream &of, ROTATIONTABLE_LINEDATA *prpLine, CString opTrenner)
{

		char olRem1[256];
		CleanSteurzeichen (olRem1, prpLine->Rem1);
		CString Rem1 = olRem1;
		Rem1.Remove('\0');

		char olRem2[256];
		CleanSteurzeichen (olRem2, prpLine->Rem2);
		CString Rem2 = olRem2;
		Rem2.Remove('\0');
		
		char olDela[256];
		CleanSteurzeichen (olDela, prpLine->Dela);
		CString Dela = olDela;
		Dela.Remove('\0');


		of.setf(ios::left, ios::adjustfield);

		of  << setw(1) << prpLine->Flno 
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Csgn
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Des4
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Vial
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Vian
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Ifrd
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Ttyp
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Stod.Format("%H:%M")
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Etdi.Format("%H:%M")
		;
		if( __CanSeeDailyScheduleColumnsDisplay() )		// 050311 MVy: daily schedule lists should contain additional column for display data: ETA(D) and ETD(D)
		{
			of << setw(1) << opTrenner
				<< setw(1) << prpLine->Etod.Format( "%H:%M" );
		};

		if(bgCDMPhase1)
		{
			of << setw(1) << opTrenner 
				<< setw(1) << prpLine->Tobt.Format("%H:%M")
				<< setw(1) << prpLine->Tobf;
		}
		else
		{
			of << setw(1) << opTrenner 
				<< setw(1) << prpLine->Etdc.Format("%H:%M");
		}
		
		of  << setw(1) << opTrenner 
		    << setw(1) << prpLine->Ofbl.Format("%H:%M")
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Slot.Format("%H:%M")
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Rwyd
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Airb.Format("%H:%M")
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Pstd
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Gtd1
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Gtd2
		    << setw(1) << opTrenner 
		    << setw(1) << CString(" ") + prpLine->Cic
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Regn
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Act5
		    << setw(1) << opTrenner 
				<< setw(1) << prpLine->Airb.Format(" %d.%m.%y")
				<< setw(1) << opTrenner 
//		    << setw(1) << prpLine->Isre
//		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Ftyp
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Remp
		    << setw(1) << opTrenner 
		    << setw(1) << Rem2
		    << setw(1) << opTrenner 
		    << setw(1) << Rem1;
			if(bgAdditionalRemark)
			{
				of << setw(1) << opTrenner 		    
				<< setw(1) << Dela;
			}
			of << endl;

}


void RotationTableViewer::PrintExcelLine(ofstream &of, ROTATIONTABLE_LINEDATA *prpLine, CString opTrenner, int ipGroupID)
{
	if(ipGroupID == UD_ARRIVAL || ipGroupID == UD_AGROUND)
	{
		if(ipGroupID == UD_ARRIVAL)
			PrintExcelLineArr(of, prpLine, opTrenner);
		else
			PrintExcelLineArrGround(of, prpLine, opTrenner);

	}

	if(ipGroupID == UD_DEPARTURE || ipGroupID == UD_DGROUND)
	{
		if(ipGroupID == UD_DEPARTURE)
			PrintExcelLineDep(of, prpLine, opTrenner);
		else
			PrintExcelLineDepGround(of, prpLine, opTrenner);

	}

}

CString RotationTableViewer::GetFieldContent(ROTATIONTABLE_LINEDATA* prlLine, CString opCurrentColumns)
{
	CString olFormat;

	if (opCurrentColumns == "FLNO")
	{
		return prlLine->Flno;
	}
	if (opCurrentColumns == "CSGN")
	{
		return prlLine->Csgn;
	}
	if (opCurrentColumns == "ORIG")
	{
		return prlLine->Org4;
	}
	if (opCurrentColumns == "VIAL")
	{
		return prlLine->Vial;
	}
	if (opCurrentColumns == "IFRA")
	{
		return prlLine->Ifra;
	}
	if (opCurrentColumns == "TTYP")
	{
		return prlLine->Ttyp;
	}
	if (opCurrentColumns == "STOA")
	{
		return CTimeToDBString(prlLine->Stoa, prlLine->Stoa); //falls die Uhrzeit ein anderer Tag ist
	}
	if (opCurrentColumns == "ETOA")
	{
		return CTimeToDBString(prlLine->Etoa, prlLine->Stoa);
	}
	if (opCurrentColumns == "TMOA")
	{
		return CTimeToDBString(prlLine->Tmoa, prlLine->Stoa);
	}
	if (opCurrentColumns == "LAND")
	{
		return CTimeToDBString(prlLine->Land, prlLine->Stoa);
	}
	if (opCurrentColumns == "RWYA")
	{
		return prlLine->Rwya;
	}
	if (opCurrentColumns == "ONBE")
	{
		return CTimeToDBString(prlLine->Onbe, prlLine->Stoa);
	}
	if (opCurrentColumns == "ONBL")
	{
		return CTimeToDBString(prlLine->Onbl, prlLine->Stoa);
	}
	if (opCurrentColumns == "PSTA")
	{
		return prlLine->Psta;
	}
	if (opCurrentColumns == "GTA1")
	{
		return prlLine->Gta1;
	}
	if (opCurrentColumns == "REGN")
	{
		return prlLine->Regn;
	}
	if (opCurrentColumns == "ACT5")
	{
		return prlLine->Act5;
	}
	if (opCurrentColumns == "CHGI")
	{
		return prlLine->Chgi;
	}
	if (opCurrentColumns == "ISRE")
	{
		return prlLine->Isre;
	}
	if (opCurrentColumns == "REMP")
	{
		return prlLine->Remp;
	}
	if (opCurrentColumns == "DEST")
	{
		return prlLine->Des4;
	}
	if (opCurrentColumns == "IFRD")
	{
		return prlLine->Ifrd;
	}
	if (opCurrentColumns == "STOD")
	{
		return CTimeToDBString(prlLine->Stod, prlLine->Stod);
	}
	if (opCurrentColumns == "ETOD")
	{
		return CTimeToDBString(prlLine->Etod, prlLine->Stod);
	}
	if (opCurrentColumns == "OFBL")
	{
		return CTimeToDBString(prlLine->Ofbl, prlLine->Stod);
	}
	if (opCurrentColumns == "RWYD")
	{
		return prlLine->Rwyd;
	}
	if (opCurrentColumns == "AIRB")
	{
		return CTimeToDBString(prlLine->Airb, prlLine->Stod);
	}
	if (opCurrentColumns == "PSTD")
	{
		return prlLine->Pstd;
	}
	if (opCurrentColumns == "GTD1")
	{
		return prlLine->Gtd1;
	}
	if (opCurrentColumns == "GTD2")
	{
		return prlLine->Gtd2;
	}
	if (opCurrentColumns == "ETDC")
	{
		return CTimeToDBString(prlLine->Etdc, prlLine->Stod);
	}

	if (opCurrentColumns == "TOBT")
	{
		CString olText = CTimeToDBString(prlLine->Tobt, prlLine->Stod) + CString(prlLine->Tobf);		

		return olText;
	}


	return "";
}

bool RotationTableViewer::HighlightFlightLine(int ipGroupId, long lpUrno, long lpResetUrno1, long lpResetUrno2)
{
	bool blResult = false;

	if(lpUrno >= 0)
	{
		int ilGroupId2 = ipGroupId+1;

		if (ilGroupId2>= omGroups.GetSize()) return false;
		//TRACE("\nTo Reset %d, %d", lpResetUrno1, lpResetUrno2 );

		GROUP_DATA *prlGroupData;
		long llUrno = 0;
		

		int ilResetCnt = 0;
		if (lpResetUrno1>0) ilResetCnt++;
		if (lpResetUrno2>0) ilResetCnt++;

		for(int i = omGroups.GetSize() - 1; i >= 0; i--)
		{
			prlGroupData = &omGroups[i];
			for(int j = prlGroupData->Lines.GetSize() - 1; j >= 0; j--)
			{
				llUrno = prlGroupData->Lines[j].Urno;
				if((llUrno == lpResetUrno1) || (llUrno == lpResetUrno2))
				{
					ShowTableLine(i, j, CHANGE_LINE, TRUE);
					//TRACE( "\nReset %d, %d: %d", i, j, llUrno);
					ilResetCnt--;					
				}
				
				if ( lpUrno>0 && (!blResult) && ((i==ipGroupId) || (i==ilGroupId2))  && ( llUrno == lpUrno))
				{
					prlGroupData->Table->SelectLine(j, TRUE);
					prlGroupData->Table->GetCTableListBox()->SetCurSel(-1);
					ShowTableLine(i, j, HIGHLIGHT_LINE, TRUE);
					//TRACE( "\nSet %d, %d: %d", i, j, llUrno);
					
					blResult = true;							
				}
				
			}
			if (blResult && ilResetCnt<=0) break;
		}

		/*
		if (lpUrno>0 && (!blResult)) 
		{
			for(int i = 0; i<2; i++)
			{
				prlGroupData = &omGroups[ipGroupId];
				for(int j = prlGroupData->Lines.GetSize() - 1; j >= 0; j--)
				{		
					llUrno = prlGroupData->Lines[j].Urno;
					if( llUrno == lpUrno)
					{
						prlGroupData->Table->SelectLine(j, TRUE);
						prlGroupData->Table->GetCTableListBox()->SetCurSel(-1);
						ShowTableLine(ipGroupId, j, HIGHLIGHT_LINE, TRUE);
						TRACE( "\nSet %d, %d: %d", ipGroupId, j, llUrno);
					
						//prlGroupData->Table->GetCTableListBox()->SetSel( j, TRUE );
						
						//prlGroupData->Table->SelectLine(j, TRUE);
						//prlGroupData->Table->GetCTableListBox()->SetCurSel(j);
						//prlGroupData->Table->GetCTableListBox()->SetCurSel(-1);
						//prlGroupData->Table->SetTextLineColor(j, WHITE, LIGHTBLUE);
					
						blResult = true;
						break;
					}
				}
				if (blResult) break;
				ipGroupId++;
			}
		}
		*/
		
	}

	return blResult;
}