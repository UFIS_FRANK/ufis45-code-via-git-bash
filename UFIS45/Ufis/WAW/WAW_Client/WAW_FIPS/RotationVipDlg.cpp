// RotationVipDlg.cpp: Implementierungsdatei
//
// Modification History: 
// 22-nov-00	rkr		Status for dialog supported

#include <stdafx.h>
#include <fpms.h>
#include <RotationVipDlg.h>
#include <CCSDdx.h>
#include <CCSTime.h>
#include <CCSGlobl.h>
#include <RotationDlgCedaFlightData.h>
#include <PrivList.h>
#include <VIPDlg.h>
#include <CedaBasicData.h>
#include <CedaVipData.h>
#include <utils.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif




/////////////////////////////////////////////////////////////////////////////
// Dialogfeld RotationVipDlg  


RotationVipDlg::RotationVipDlg(CWnd* pParent, ROTATIONDLGFLIGHTDATA* prpFlight )
	: CDialog(RotationVipDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(RotationVipDlg)
	//}}AFX_DATA_INIT

                         
	pomParent = pParent;
    pomTable = new CCSTable;

	pomFlight = prpFlight;

	emStatus = UNKNOWN;

	omVIPUrnos.RemoveAll();

}


RotationVipDlg::~RotationVipDlg()
{

	ogDdx.UnRegister(this, NOTUSED);

	delete pomTable;
	pomTable = NULL;

}


void RotationVipDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationVipDlg)
	DDX_Control(pDX, IDC_GMBORDER, m_CS_GMBorder);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotationVipDlg, CDialog)
	//{{AFX_MSG_MAP(RotationVipDlg)
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
	ON_BN_CLICKED(IDC_UPDATE, OnUpdate)
	ON_BN_CLICKED(IDC_INSERT, OnInsert)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	ON_BN_CLICKED(IDC_CLOSE, OnClose)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten RotationVipDlg 

BOOL RotationVipDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	pomParent->EnableWindow(TRUE);
	
	InitTable();

	CString olCaption;

	if (pomFlight && pomFlight->Urno > 0)
	{
		FillTableLine();

		if(strcmp(pomFlight->Adid,"A") == 0)
			olCaption = "VIP / Flight: " + CString(pomFlight->Flno) + "  (" + CString(pomFlight->Adid) + ")  " + pomFlight->Stoa.Format("%d.%m.%y %H:%M");
		else
			olCaption = "VIP / Flight: " + CString(pomFlight->Flno) + "  (" + CString(pomFlight->Adid) + ")  " + pomFlight->Stod.Format("%d.%m.%y %H:%M");

		SetWindowText(olCaption);

	}

	SetSecState();
	if (emStatus == POSTFLIGHT)
		InitialStatus();

	m_resizeHelper.Init(this->m_hWnd);
	m_resizeHelper.Fix(IDC_UPDATE,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_INSERT,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_DELETE,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_CLOSE,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);





	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void RotationVipDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);

	m_resizeHelper.OnSize();

	if (this->pomTable != NULL && ::IsWindow(this->pomTable->m_hWnd))
	{
		CRect olrectTable;
		m_CS_GMBorder.GetWindowRect(&olrectTable);
		ScreenToClient(olrectTable);
		olrectTable.DeflateRect(2,2);     // hiding the CTable window border
		this->pomTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);

		this->pomTable->DisplayTable();
	}
		
	this->Invalidate();
}



void RotationVipDlg::SetSecState()
{
	CWnd* plWnd = GetDlgItem(IDC_INSERT);
	if(ogPrivList.GetStat("VIP_CB_Insert") == '0')
	{
		if (plWnd) 
			plWnd->EnableWindow(false);
		if (pomTable) 
			pomTable->SetTableEditable(false);
	}
	else if(ogPrivList.GetStat("VIP_CB_Insert") == '1')
	{
		if (plWnd) 
			plWnd->EnableWindow(true);
		if (pomTable) 
			pomTable->SetTableEditable(true);
	}
	else if(ogPrivList.GetStat("VIP_CB_Insert") == '-')
	{
		if (plWnd) 
			plWnd->ShowWindow(SW_HIDE);
		if (pomTable) 
			pomTable->SetTableEditable(false);
	}


	plWnd = GetDlgItem(IDC_UPDATE);
	if(ogPrivList.GetStat("VIP_CB_Update") == '1' && plWnd)
		plWnd->EnableWindow(true);
	else if(ogPrivList.GetStat("VIP_CB_Update") == '0' && plWnd)
		plWnd->EnableWindow(false);
	else if(ogPrivList.GetStat("VIP_CB_Update") == '-' && plWnd)
		plWnd->ShowWindow(SW_HIDE);

	plWnd = GetDlgItem(IDC_DELETE);
	if(ogPrivList.GetStat("VIP_CB_Delete") == '1' && plWnd)
		plWnd->EnableWindow(true);
	else if(ogPrivList.GetStat("VIP_CB_Delete") == '0' && plWnd)
		plWnd->EnableWindow(false);
	else if(ogPrivList.GetStat("VIP_CB_Delete") == '-' && plWnd)
		plWnd->ShowWindow(SW_HIDE);
}


void RotationVipDlg::FillTableLine()
{
	if(pomFlight == NULL || pomFlight->Urno == 0)
		return;


	CCSPtrArray<VIPDATA> olData ;

	VIPDATA *polVip;

	ogVipData.GetVips(  pomFlight->Urno , &olData);



	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_9;


	CCSEDIT_ATTRIB rlAttrib;
	rlAttrib.Style = ES_UPPERCASE;

	for(int i = 0; i < olData.GetSize(); i++)
	{

		polVip = &olData[i];

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = polVip->Paxn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = polVip->Paxt;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = polVip->Seat;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = polVip->Refr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = polVip->Refe;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = polVip->Paxr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomTable->AddTextLine(olColList, NULL);
		olColList.DeleteAll();

	}

	olData.RemoveAll();

	DrawHeader();

	pomTable->DisplayTable();

}


void RotationVipDlg::InitTable()
{

	CRect olRectBorder;
	m_CS_GMBorder.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	omVIPUrnos.RemoveAll();

	pomTable->SetHeaderSpacing(0);
    pomTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomTable->SetSelectMode(0);
	pomTable->SetShowSelection(true);
	pomTable->ResetContent();


	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	
	TABLE_HEADER_COLUMN rlHeader;

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Font = &ogCourier_Regular_10;

	// Name
	rlHeader.Length = 15; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// Title
	rlHeader.Length = 35; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// Seat No
	rlHeader.Length = 60; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// Received from
	rlHeader.Length = 40; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// Reference
	rlHeader.Length = 50; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// Remark
	rlHeader.Length = 50; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	
	pomTable->SetHeaderFields(omHeaderDataArray);
	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(false);
	omHeaderDataArray.DeleteAll();

	pomTable->DisplayTable();

}


void RotationVipDlg::DrawHeader()
{
	int  ilTotalLines = 0;
	bool blNewLogicLine = false;

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

//	pomTable->SetShowSelection(true);
//	pomTable->ResetContent();
	
	TABLE_HEADER_COLUMN *prlHeader[6];

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 150;
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING1708);//PAXN - Name

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 75;
	prlHeader[1]->Font = &ogCourier_Bold_10;
	prlHeader[1]->Text = GetString(IDS_STRING2918);//PAXT - Title

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 18; 
	prlHeader[2]->Font = &ogCourier_Bold_10;
	prlHeader[2]->Text = GetString(IDS_STRING2919);// SEAT - Seat

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 75; 
	prlHeader[3]->Font = &ogCourier_Bold_10;
	prlHeader[3]->Text = GetString(IDS_STRING2920);// REFR - Received from

	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 100; 
	prlHeader[4]->Font = &ogCourier_Bold_10;
	prlHeader[4]->Text = GetString(IDS_STRING2921);//REFE - Reference

	prlHeader[5] = new TABLE_HEADER_COLUMN;
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 350; 
	prlHeader[5]->Font = &ogCourier_Bold_10;
	prlHeader[5]->Text = GetString(ST_EQU_REMA);//PAXR - Remark


	for(int ili = 0; ili < 6; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}


void RotationVipDlg::OnCancel() 
{

	CDialog::OnCancel();

}


LONG RotationVipDlg::OnTableLButtonDown(UINT wParam, LONG lParam)
{
	CCSTABLENOTIFY *prlNotify = (CCSTABLENOTIFY*)lParam;
	pomTable->SelectLine(prlNotify->Line);
	return 0L;
}



void RotationVipDlg::InitDialog()
{

	InitTable();
	FillTableLine();
	InitialStatus();

}


void RotationVipDlg::OnUpdate()
{

	int ilAnz = (pomTable->GetCTableListBox())->GetCurSel();

	if(ilAnz < 0)
	{
		MessageBox(GetString(IDS_STRING909), GetString(ST_HINWEIS));
		return;
	}
	
	CCSPtrArray<VIPDATA> olData ;
	VIPDATA *polVip;

	ogVipData.GetVips(  pomFlight->Urno , &olData);


	if (ilAnz >= olData.GetSize())
		return;

	polVip = &olData[ilAnz];


	VIPDlg polDlg(this, pomFlight->Urno, polVip);


	if(ogPrivList.GetStat("VIP_CB_Update") != '1')
		polDlg.setStatus(POSTFLIGHT); 
	else
		polDlg.setStatus(emStatus);

	polDlg.DoModal();


	pomTable->ResetContent();
	FillTableLine();

}


void RotationVipDlg::OnInsert()
{

	VIPDlg polDlg(this, pomFlight->Urno, NULL);

	polDlg.DoModal();

	pomTable->ResetContent();
	FillTableLine();

	InitDialog();

}


void RotationVipDlg::OnDelete()
{

	int ilAnz = (pomTable->GetCTableListBox())->GetCurSel();

	if(ilAnz < 0)
	{
		MessageBox(GetString(IDS_STRING909), GetString(ST_HINWEIS));
		return;
	}

	CCSPtrArray<VIPDATA> olData ;
	VIPDATA *polVip;

	ogVipData.GetVips(  pomFlight->Urno , &olData);


	if (ilAnz >= olData.GetSize())
		return;

	polVip = &olData[ilAnz];
	int llCountA = 0;

	if(MessageBox(GetString(ST_DATENSATZ_LOESCHEN), GetString(ST_HINWEIS), MB_YESNO ) == IDYES)
	{
		polVip->IsChanged = DATA_DELETED;
		ogVipData.Save(polVip);

		if(bgDailyShowVIP)
		{
			llCountA = ogVipData.GetCount(pomFlight->Urno);

			char alCount[4] = "";		
			ltoa(llCountA, alCount,3);
			ogRotationDlgFlights.UpdateFlight(pomFlight->Urno, "VCNT", alCount);
		}
		InitDialog();
	}

}


LONG RotationVipDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
#if 0
	// anhand der Mousposition die Spalte ermitteln
	int ilCol = 0;
	CPoint point;
    ::GetCursorPos(&point);
    pomDailyScheduleTable->pomListBox->ScreenToClient(&point);    

	UINT ipItem = wParam;
	DAILYSCHEDULE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (DAILYSCHEDULE_LINEDATA *)pomDailyScheduleTable->GetTextLineData(ipItem);

	// Inhalt in der Zeile?
	if (prlTableLine != NULL)
	{
		// Spaltennummer
		ilCol = pomDailyScheduleTable->GetColumnnoFromPoint(point);
	}

	DAILYFLIGHTDATA *prlFlight;
	if((prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->AUrno)) == NULL)
		prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->DUrno);


	// Behandlung: Groundmovement
	if (strcmp (prlFlight->Ftyp, "G") == 0)
	{
		if(pogRotGroundDlg == NULL)
		{
			pogRotGroundDlg = new RotGroundDlg(this);
			pogRotGroundDlg->Create(IDD_ROTGROUND);
		}

		// gueltiger Wert der Spalte
		if (ilCol < 0)
			return 0L;

		// Klick auf Arrival oder Departure
		if (omViewer.GetColumnByIndex(ilCol).Left(1) == "A")
		{
			prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->AUrno);
			if(prlFlight == NULL) 
				return 0L;
		}
		else
		{
			prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->DUrno);
			if(prlFlight == NULL) 
				return 0L;
		}

		pogRotGroundDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
		pogRotGroundDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno);
	}
	else
	{
		if (prlTableLine != NULL)
		{
			CString olAdid("");
			//DAILYFLIGHTDATA *prlFlight;

			if((prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->AUrno)) == NULL)
			{
				prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->DUrno);
				olAdid = "D";
			}
			else
			{
				olAdid = "A";
			}
			
			// Rotationsmaske aufrufen
			if(prlFlight != NULL)
			{
				pogRotationDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, olAdid);
				omViewer.UpdateDisplay();
			}
		}
	}
#endif

	OnUpdate();

	return 0L;
}


void RotationVipDlg::OnClose()
{

	CDialog::OnCancel();

}




void RotationVipDlg::InitialStatus()
{
	CWnd* plWnd = NULL;
	switch (emStatus)
	{
		case UNKNOWN:
			InitialPostFlight(TRUE);
			break;
		case POSTFLIGHT:
			InitialPostFlight(FALSE);
			break;
		default:
			InitialPostFlight(TRUE);
			break;
	}
}

void RotationVipDlg::setStatus(Status epStatus)
{
	emStatus = epStatus;
}

RotationVipDlg::Status RotationVipDlg::getStatus() const
{
	return emStatus;
}

void RotationVipDlg::InitialPostFlight(BOOL bpPostFlight)
{
	CWnd* plWnd = NULL;
	plWnd = GetDlgItem(IDC_INSERT);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);

	plWnd = GetDlgItem(IDC_DELETE);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);

	plWnd = GetDlgItem(IDC_UPDATE);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);

	ModifyWindowText(this, GetString(IDS_STRINGWND_POSTFLIGHT), bpPostFlight);

}
