#ifndef __PosOverviewTableViewer_H__

#include <stdafx.h>
#include <Fpms.h>
#include <CCSGlobl.h>
#include <CCSTable.h>
#include <DiaCedaFlightData.h>
#include <CViewer.h>
#include <CCSPrint.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct POSOVERVIEWTABLE_LINEDATA
{
	long AUrno;
	long DUrno;
	long Rkey;
	CString		Name;
	CString		AFlno;
	CString		AFlti;
	CString		AFtyp;
	CString		ANat;
	CString		Org34;
	CTime		Stoa;
	CTime		Onbl;
	CTime		AAkt;
	CString		AAktStr;

	CString 	Act35;
	CString 	Regn;

	CString		DFlno;
	CString		DFlti;
	CString		DFtyp;
	CString		DNat;
	CString		Des34;
	CTime		Stod;
	CTime		Ofbl;
	CTime		DAkt;
	CString		DAktStr;

	CTime		Begin;
	CTime		End;
 
	POSOVERVIEWTABLE_LINEDATA(void)
	{ 
		Stoa = -1;
		Onbl = -1;
		AAkt = -1;
		Stod = -1;
		Ofbl = -1;
		DAkt = -1;
	}
};
 


/////////////////////////////////////////////////////////////////////////////
// PosOverviewTableViewer

/////////////////////////////////////////////////////////////////////////////
// Class declaration of PosOverviewTableViewer

//@Man:
//@Memo: PosOverviewTableViewer
//@See:  STDAFX, CCSCedaData, CedaDIAFLIGHTDATA, CCSPTRARRAY,  CCSTable
/*@Doc:
  No comment on this up to now.
*/

#define POSOVERVIEWTABLE_COLCOUNT 19

class PosOverviewTableViewer : public CViewer
{

public:
    //@ManMemo: Default constructor
    PosOverviewTableViewer();
    //@ManMemo: Default destructor
    ~PosOverviewTableViewer();
	
	void ProcessFlightChange(const DIAFLIGHTDATA &rrpFlight);
	void ProcessFlightDelete(const DIAFLIGHTDATA &rrpFlight);
	void CCAConvertDIA(CCAFLIGHTDATA* prpFlight, DIAFLIGHTDATA& prlDiaFlight);
    void ProcessCcaFlightDelete(CCAFLIGHTDATA* prpFlight);
    void ProcessCcaFlightChange(CCAFLIGHTDATA* prpFlight);
    void ProcessCcaChange(long lpCcaUrno);

	//void ProcessFlightInsert(DiaCedaFlightData::RKEYLIST  *prpRotation);

	void UnRegister();

  	void Attach(CCSTable *popAttachWnd);
    void ChangeViewTo(CString opView);

	// Print table to paper
	void PrintTableView(void);
	// Print table to file
	bool PrintPlanToFile(CString opFilePath) const;
	void SetResource(CString opResource);

	int GetFlightCount();
	void SetParentDlg(CDialog* ppParentDlg);

// Internal data processing routines
private:
	const int imOrientation;

	static int imTableColCharWidths[POSOVERVIEWTABLE_COLCOUNT];
	CString omTableHeadlines[POSOVERVIEWTABLE_COLCOUNT];
	int imTableColWidths[POSOVERVIEWTABLE_COLCOUNT];
	int imPrintColWidths[POSOVERVIEWTABLE_COLCOUNT];

	// Table fonts and widths
	CFont &romTableHeaderFont; 
	CFont &romTableLinesFont;
	const float fmTableHeaderFontWidth; 
	const float fmTableLinesFontWidth;

	// Table fonts and widths for printing
	CFont *pomPrintHeaderFont; 
	CFont *pomPrintLinesFont;
	float fmPrintHeaderFontWidth; 
	float fmPrintLinesFontWidth;

 	void DeleteAll();
 	void DeleteLine(int ipLineno);
 
  	void UpdateDisplay();

	void DrawHeader();

 	int CompareLines(const POSOVERVIEWTABLE_LINEDATA &rrpLine1, const POSOVERVIEWTABLE_LINEDATA &rrpLine2) const;

    void MakeLines(CCSPtrArray<DIAFLIGHTDATA> *popFlights);
    void MakeLines(CCSPtrArray<CCAFLIGHTDATA> *popFlights);
	void MakeLines(CCSPtrArray<DIACCADATA> *popDiaCca);
	void MakeLines();

	int Make(const DIAFLIGHTDATA *rrpFlight, DIACCADATA* prpCca, bool bpInsertDisplay = false);
	int Make(const DIAFLIGHTDATA *rrpFlightA, const DIAFLIGHTDATA *rrpFlightD, bool bpInsertDisplay = false);
	void MakeRkey(DiaCedaFlightData::RKEYLIST *prlRkey, bool bpInsertDisplay = false);
	int  MakeLine(const DIAFLIGHTDATA *rrpFlightA, const DIAFLIGHTDATA *rrpFlightD);
	int  MakeLine(const DIAFLIGHTDATA &rrpFlight, int ipBltNo, DIACCADATA* prpCca, CString opAdid);
	void MakeLineData(const DIAFLIGHTDATA &rrpFlight, POSOVERVIEWTABLE_LINEDATA &rrpLine, int ipBltNo, DIACCADATA* prpCca, CString opAdid);
	void MakeLineData(const DIAFLIGHTDATA *rrpFlightA, const DIAFLIGHTDATA *rrpFlightD, POSOVERVIEWTABLE_LINEDATA &rrpLine);
	void MakeColList(const POSOVERVIEWTABLE_LINEDATA &rrpLine, CCSPtrArray<TABLE_COLUMN> &ropColList);
	int  CreateLine(POSOVERVIEWTABLE_LINEDATA &rrpLine);

	void InsertFlight(const DIAFLIGHTDATA &rrpFlight);

	bool FindLine(long lpUrno, int &ripLineno) const;
	
	void InsertDisplayLine(int ipLineNo);

	bool PrintTableHeader(CCSPrint &ropPrint);
 	bool PrintTableLine(CCSPrint &ropPrint, int ipLineNo);
	
	bool UtcToLocal(POSOVERVIEWTABLE_LINEDATA &rrpLine);

    CCSPtrArray<POSOVERVIEWTABLE_LINEDATA> omLines;

	CCSTable *pomTable;
 	CString myResource;
	CString myCaption;
    CMapPtrToPtr omUrnoMap;
	CDialog* pomParentDlg;
	CString GetCaption();


};

#endif //__PosOverviewTableViewer_H__
