// Utils.h : header file
//
// Modification History: 
// 22-nov-00	rkr		IsPostFlight() added
// 05-dec-00	rkr		ModifyWindowText() added

#if !defined(Utils_H__INCLUDED_)
#define Utils_H__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

enum SPECIALREQSTATUS
{
	NO_REQ,
	REQ,
	NOT_ALL_READ
};

void ActivateTimers();
void DeActivateTimers();

int GetSpecialREQ (const CString& opReq1, const CString& opReq2, CString& opBarSpecialREQ, int& ipRead, int& ipNotRead);
int InsertSortInStrArray(CStringArray &ropStrArray, const CString &ropStr);
bool FindInStrArray(const CStringArray &ropStrArray, const CString &ropStr);
int FindIdInStrArray(const CStringArray &ropStrArray, const CString &ropStr);

bool IsRegularFlight(char Ftyp);
bool IsArrivalFlight(const char *pcpOrg, const char *pcpDes, char Ftyp);
bool IsArrival(const char *pcpOrg, const char *pcpDes);
bool IsDepartureFlight(const char *pcpOrg, const char *pcpDes, char Ftyp);
bool IsDeparture(const char *pcpOrg, const char *pcpDes);
bool IsCircularFlight(const char *pcpOrg, const char *pcpDes, char Ftyp);
bool IsCircular(const char *pcpOrg, const char *pcpDes);

bool DelayCodeToAlpha(char *pcpDcd);
bool DelayCodeToNum(char *pcpDcd);

int FindInListBox(const CListBox &ropListBox, const CString &ropStr);

// A substitute for CListBox::GetSelItems(...)
// because this function dont't work on some WinNT machines
int MyGetSelItems(CListBox &ropLB, int ipMaxAnz, int *prpItems);

COLORREF GetColorOfFlight(char FTyp);

long MyGetUrnoFromSelection(const CString &ropSel);
CString GetUrnoListFromSelection(const CString &ropSel);
long MyGetUrnoFromSelection(const CString &ropSel, CString opKeyField);


bool GetPosDefAllocDur(const CString &ropPos, int ipMinGT, CTimeSpan &ropDura);
bool GetGatDefAllocDur(const CString &ropGate, CTimeSpan &ropDura, bool bpArrival = false);
bool GetBltDefAllocDur(const CString &ropBlt, CTimeSpan &ropDura);
//bool GetBltDefAllocDur(const char *pcpFlightId, const CString &ropBlt, CTimeSpan &ropDura);
bool GetBltDefAllocDur(const char *pcpFlightId, const char *pcpAct3, const char *pcpAct5,
					   const char *pcpApc3, const char *pcpApc4, const CString &ropBlt, CTimeSpan &ropDura);
bool GetWroDefAllocDur(const CString &ropWro, CTimeSpan &ropDura);
bool GetPosDefAllocDur(const CString &ropPos, int ipMinGT, CTimeSpan &ropDura, CString opAdid, CString opBaa4, CString opBaa5);

bool GetFltnInDBFormat(const CString &ropUserFltn, CString &ropDBFltn);

bool GetCurrentUtcTime(CTime &ropUtcTime);

// true/false; outside/inside the timelimit
//bool IsPostFlight(CTime opTimeFromFlight, bool bpLocalTime = false);
bool IsPostFlight(const CTime& opTimeFromFlight, CString opPrfl);
bool IsPostFlight(CTime opTimeFromFlight, bool bpLocalTime, CString opPrfl);
bool IsPostFlight(CTime opTimeFromFlight, bool bpLocalTime);
bool IsPostFlight(const CTime& opTimeFromFlight);
BOOL ModifyWindowText(CWnd *opWnd, CString& opStrWnd, BOOL bpClean = FALSE);
BOOL CheckPostFlightPosDia(const long lpBarUrno, CWnd* opCWnd);
BOOL CheckPostFlightCcaDia(const long lpBarUrno, CWnd* opCWnd);

bool CallUIFforBLK(CString& opTable, const long lpUrno);
bool DBStrIsEmpty(const char *prpStr);


CString GetFtypLabel(CString opFtyp);
CString IsDupFlights(long lpUrno, CString opAdid, CString opAlc3, CString opFltn, CString opFlns, CTime opDay, bool bpLocal = false);
CString IsDupFlight2(long lpUrno, CString opAdid, CString opFlno, CTime opDay, bool bpLocal /*=false*/);
CString IsDupFlight (long lpUrno, CString opAdid, CString opAlc, CString opFltn, CString opFlns, CTime opDay, bool bpLocal, bool bpInsertion = false);
CString CreateFlno(CString opAlc, CString opFltn, CString opFlns);
CString IsDupFlightSeason(long lpUrno, CString opAdid, CString opAlc, CString opFltn, CString opFlns, CTime opDayBeg, CTime opDayEnd, CString opDaySel, bool bpLocal, bool bpInsertion);
CString IsDupFlight2Season(long lpUrno, CString opAdid, CString opFlno, CTime opDayBeg, CTime opDayEnd, CString opDaySel, bool bpLocal);
bool NewPreSelectionCheck(CString &opData, CString &opFields, CString opFtyps, CTime opFrom, CTime opTo, CString opSelection = "");
bool CalculateSeasonTimes(CTime& NewBS, CTime& NewES, CTime OldBS, CTime OldES, CTime opDateES, CTimeSpan olStandardDiff, bool bpOverwrite); 

BOOL WriteDialogToReg(CWnd* opWnd, CString& opKey);
bool GetDialogFromReg(CRect& opRect, CString& opKey);
void CallHistory(CString opField);

bool GetDefAlloc(CString& opAlloc, CString& opData, CString& opFields, CStringArray& opReqData, CStringArray& opReqFields);
bool GetDefAllocFC(CString& opAlloc, const CString& opRes, CMapStringToString& opForcastMap, CStringArray& opReqFields);
CString GetCnamExt(CString& opCnam1, CString& opCnam2);
void CreateForcastMap(CMapStringToString& opForecastMap, CStringArray& opValues);

bool ConvOctStToByteArr( CString pspSt, CString&);
bool ConvOctStToByte( char* st, BYTE& bpByte );
bool ConvOctCharToInt( char cpCh , int& num);

bool DayStringShift( CUIntArray &opDayArray, int ipShift);


bool GetConfigInfo(const char* pcpConfigName, CString& opConfigValue );					
								
		//void LaunchTool( CString opCmd, CString opArrUrno, CString opDepUrno, CString opPermit, CString opConfigName);  			
DWORD LaunchTool( CString opCmd, CString opArrUrno, CString opDepUrno, CString opPermit,	
				 CString opConfigName,bool opLocalTime=true);		
			
DWORD LaunchTool(			
	CString opCmd, 		
	CString opArrUrno, CString opDepUrno, 		
	CString opPermit,		
	CString opConfigName,		
	CString processWindowName,		
	DWORD dpPrevId,
	bool opLocalTime=true)	;	
			
bool IsProcessAlive( CString opProcessWindowName, DWORD dpProcId );			
DWORD startProcess(CString opExePath, CString opArgs );			
void killProcess(DWORD processPid);			
static BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam);			


#endif