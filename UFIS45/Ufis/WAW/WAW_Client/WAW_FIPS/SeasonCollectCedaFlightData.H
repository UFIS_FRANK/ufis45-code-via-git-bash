#ifndef _SeasonCollectCedaFlightData_H_
#define _SeasonCollectCedaFlightData_H_
 
#include <BasicData.h>
#include <CCSGlobl.h>
#include <CCSDefines.h>
#include <CedaCcaData.h>

//void ProcessFlightCf(void *vpInstance,enum enumDDXTypes ipDDXType, void *vpDataPointer, CString &ropInstanceName);




/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

//@Man: COLLECTFLIGHTDATAStruct
//@See: SeasonCollectCedaFlightData
/*@Doc:
  A structure for reading flight data. We read all data from database (or get 
  the data from AFTLSG) into this struct and store the data in omData.

*/

 
struct COLLECTFLIGHTDATA 
{
	char 	 Adid[2]; 	// 
	char 	 Act3[5]; 	// Flugzeug-3-Letter Code (IATA)
	char 	 Act5[7]; 	// Flugzeug-5-Letter Code
	char 	 Alc2[4]; 	// Fluggesellschaft (Airline 2-Letter Code)
	char 	 Alc3[5]; 	// Fluggesellschaft (Airline 3-Letter Code)
	char 	 Blt1[7]; 	// Gep�ckband 1
	char 	 Blt2[7]; 	// Gep�ckband 2
	char 	 Des3[5]; 	// Bestimmungsflughafen 3-Lettercode
	CTime 	 Etai; 	// ETA-Intern (Beste Zeit)
	CTime 	 Etau; 	// ETA- Anwender
	CTime 	 Etdi; 	// ETD-Intern (Beste Zeit)
	CTime 	 Etdu; 	// ETD-Anwender
	CTime 	 Etoa; 	// ETA-Puplikumsanzeige
	CTime 	 Etod; 	// ETD-Publikumsanzeige
	char 	 Ext1[7]; 	// Ausgang 1 Ankunft
	char 	 Ext2[7]; 	// Ausgang 2 Ankunft
	char 	 Flno[11]; 	// komplette Flugnummer (Airline, Nummer, Suffix)
	char 	 Flns[3]; 	// Suffix
	char 	 Fltn[7]; 	// Flugnummer
	char 	 Ftyp[3]; 	// Type des Flugs [F, P, R, X, D, T, ...]
	char 	 Gta1[7]; 	// Gate 1 Ankunft
	char 	 Gta2[7]; 	// Gate 2 Ankunft
	char 	 Gtd1[7]; 	// Gate 1 Abflug
	char 	 Gtd2[7]; 	// Doppelgate Abflug
	char 	 Htyp[4]; 	// Abfertigungsart
	char 	 Isre[3]; 	// Bemerkungskennzeichen intern
	char 	 Jcnt[3]; 	// Anzahl verbundene Flugnummern
	char 	 Jfno[112]; 	// Verbundene Flugnummern (max. 10)
	CTime 	 Lstu; 	// Datum letzte �nderung
	char 	 Org3[5]; 	// Ausgangsflughafen 3-Lettercode
	char 	 Psta[7]; 	// Position Ankunft
	char 	 Pstd[7]; 	// Position Abflug
	char 	 Rem1[258]; 	// Bemerkung zum Flug
	long 	 Rkey; 	// Rotationsschl�ssel
	char 	 Rtyp[3]; 	// Verkettungstyp (Art/Quelle der Verkettung)
	char 	 Stat[12]; 	// Status
	char 	 Stev[5]; 	// Statistikkennung
	char 	 Stht[3]; 	// Datenstatus HTYP
	CTime 	 Stoa; 	// Planm��ige Ankunftszeit STA
	CTime 	 Stod; 	// Planm��ige Abflugzeit
	char 	 Sttt[3]; 	// Datenstatus TTYP
	char 	 Styp[4]; 	// Service Typ (Fluko)
	CTime 	 Tifa; 	// Zeitrahmen Ankunft
	CTime 	 Tifd; 	// Zeitrahmen Abflug
	char 	 Tisa[3]; 	// Statusdaten f�r TIFA
	char 	 Tisd[3]; 	// Statusdaten f�r TISD
	char 	 Ttyp[7]; 	// Verkehrsart
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Vian[4]; 	// Anzahl der Zwischenstationen - 1
	char 	 Wro1[7]; 	// Warteraum1
	char 	 Wro2[7]; 	// Warteraum2
	char 	 Vial[1026]; 	// Liste der Zwischenstationen/-zeiten
	char 	 Ckif[7]; 	// Check-In From
	char 	 Ckit[7]; 	// Check-In To
	char 	 Nose[5]; 	// Check-In To
	char 	 Rem2[258]; 	// Bemerkung zum Flug
	char	 Flti[3];
	char 	 Csgn[10]; 	// Call- Sign
	char 	 Prfl[3]; 	// Prfl
	char 	 Gd2d[2]; 	// Prfl
	char 	 Ckf2[6];	// CKI second set (Terminal 2 CKI for mixed flights in Lisbon) 	
	char 	 Ckt2[6]; 	// CKI second set (Terminal 2 CKI for mixed flights in Lisbon)

	char	 Mopa[2]; // For new Mode of Payment implementation
	char 	 Rreq[2];	//Resource required 0 -no 1 -yes
	
	CTime	 Nfes;

	//DataCreated by this class
	int      IsChanged;

	COLLECTFLIGHTDATA(void)
	{ memset(this,'\0',sizeof(*this));
		Nfes = -1;
		Etai = -1;
		Etau = -1;
		Etdi = -1;
		Etdu = -1;
		Etoa = -1;
		Etod = -1;
		Lstu = -1;
		Stoa = -1;
		Stod = -1;
		Tifa = -1;
		Tifd = -1;
	}

}; // end COLLECTFLIGHTDATA
	




/////////////////////////////////////////////////////////////////////////////
// Class declaration

//@Man:
//@Memo: Read and write flight data
//@See: CedaData
/*@Doc:
  Reads and writes flight data from and to database. Stores flight data in memory 
  and provides methods for searching and retrieving flights from its internal list. 
  Does some data preparations like pre-formatting the flight number, storing flight 
  number and gate information for the corresponding inbound/outbound flight.

  Data of this class has to be updated by CEDA through broadcasts. This procedure 
  and the necessary methods will be described later with the description of 
  broadcast handling.

  {\bf SeasonCollectCedaFlightData} handles data from the EIOHDL server process.
	See the specification for table descriptions.
*/
class SeasonCollectCedaFlightData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: A map, containing the Keya/Keyd fields of all loaded flights.
    //CMapStringToPtr omKeyMap;
    CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omRkeyMap;
   //@ManMemo: COLLECTFLIGHTDATA records read by ReadAllFlight().
    CCSPtrArray<COLLECTFLIGHTDATA> omData;

	CMapStringToPtr omLocalDayMap;

	char pcmAftFieldList[2048];
	CCSPtrArray<COLLECTFLIGHTDATA> omJoinData;

	char pcmGFRcommand[256];


// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in 
	  {\bf COLLECTFLIGHTDATA}, the {\bf pcmTableName} with table name {\bf AFTLSG}, 
	  the data members {\bf pcmFieldList} contains a list of used fields of 
	  {\bf COLLECTFLIGHTDATAStruct}.
    */
    //SeasonCollectCedaFlightData(CCSCedaCom *popCommHandler = NULL);  : CCSCedaData(&ogCommHandler)
    SeasonCollectCedaFlightData();
    //@ManMemo: Default destructor
	~SeasonCollectCedaFlightData();

	void SetFieldList(void);
	
	void UnRegister();
	void Register();



    //@ManMemo: Clear all flights.
    /*@Doc:
      Clear all flights.
      This method will clear {\bf omData} and {\bf omKeyMap}.
    */
	void ClearAll(void);
   

    // internal data access.
	bool AddFlightInternal(COLLECTFLIGHTDATA *prpFlight, bool bpDdx = true);
	bool DeleteFlightInternal(long lpUrno, bool bpDdx = true);
	void DeleteInternalByRkey(CString opRkey);

	bool ReadAllFlights(CTime opFrom, CTime opTo, CString opAlc3, CString opFltn, CString opFlns, CString opAdid, CString opCsgn = CString(""), CString opStev = CString(""));

	bool PreSelectionCheck(CString &opData, CString &opFields);

	bool ReadRkeys(CString &olRkeys, CString opSelection = "");

	bool ReadFlights(CString opSelection, bool bpDdx = true);

	bool UpdateFlight(COLLECTFLIGHTDATA *prpFlight, COLLECTFLIGHTDATA *prpFlightSave);
	bool UpdateFlight(long lpUrno, CString opFieldList, CString opDataList);
	bool UpdateFlight(CString opSelection, CString opFieldList, CString opDataList);

	bool SaveSpecial(char *pclSelection, char *pclField, char *pclValue);
	
	bool JoinFlightPreCheck(long &ll1Urno, long &ll2Urno);
	
//	bool SearchFlight(CString olFlno, CTime opDate, char clAdid, long &lpUrno, long &lpRkey);
	bool SearchFlight(CString opAct3, CString opAct5, CString olFlno, CTime opDateFrom, CTime opDateTo, char clAdid, long &lpUrno, long &lpRkey);
	bool SearchFlightsToJoin(CString olFlno, CTime opDateFrom, CTime opDateTo, char clAdid, CCSPtrArray<COLLECTFLIGHTDATA> &opJoinData , CString opStev = CString(""));


	bool SplitFlight(const COLLECTFLIGHTDATA *prpFlight, char cpAdid, bool bpUseGlobalData = false) const;

	bool DeleteFlight(CString opUrnoList );
	bool DeleteFlight(long lpUrno);

    //@ManMemo: Handle Broadcasts for flights.
	void ProcessFlightBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	
	
	COLLECTFLIGHTDATA *GetArrival(const COLLECTFLIGHTDATA *prpFlight) const;
	COLLECTFLIGHTDATA *GetDeparture(const COLLECTFLIGHTDATA *prpFlight) const;
	COLLECTFLIGHTDATA *GetJoinFlight(COLLECTFLIGHTDATA *prpFlight);

	int GetJfnoArray(CCSPtrArray<JFNODATA> *opJfno, COLLECTFLIGHTDATA *prpFlight);
	int GetViaArray(CCSPtrArray<VIADATA> *opVias, COLLECTFLIGHTDATA *prpFlight);


	CCSPtrArray<COLLECTFLIGHTDATA> *GetFlightsByDayKey(CString olKey);

	bool AddToKeyMap(COLLECTFLIGHTDATA *prpFlight );
	bool DeleteFromKeyMap(COLLECTFLIGHTDATA *prpFlight );
	COLLECTFLIGHTDATA *GetFlightByUrno(long lpUrno);

	CString CreateFlno(CString opAlc, CString opFltn, CString opFlns);


	bool ProcessFlightUFR(BcStruct *prlBcStruct);
	bool ProcessFlightIFR(BcStruct *prlBcStruct);
	bool ProcessFlightISF(BcStruct *prlBcStruct);
	void ReloadFlights(void *vpDataPointer);

	bool CreateFlno(char* pcpFlno, CString &olFlno, CString &opAlc, CString &opFltn, CString &opFlns);


	void Debug();

	struct RKEYLIST
	{
		CCSPtrArray<COLLECTFLIGHTDATA> Rotation;

	};
	
	CString omFtyps;
	CTime omFrom;
	CTime omTo;
	CString omAlc3;
	CString omFltn;
	CString omFlns;
	CString omAdid;
	CString omCsgn;
	CString omStev;

	bool bmReadAllFlights;

};


#endif
