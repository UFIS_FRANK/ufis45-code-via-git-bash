// TableLineData.cpp: implementation of the CTableLineData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "fpms.h"
#include "TableLineData.h"
#include "CcaCedaFlightData.h"		// type of ogCcaDiaFlightData
#include "CCSGlobl.h"		// ogCcaDiaFlightData

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTableLineData::CTableLineData()
{
	clrBackground	= WHITE ;		// 050321 MVy: set line background color
	Enable();
};	// constructor

CTableLineData::~CTableLineData()
{
};	// destructor

// wrapper functions for state atribute access

// 050321 MVy: allow user actions
void CTableLineData::Enable()
{
	m_bEnabled = true ;
};	// Enable

// 050321 MVy: no user actions are allowed
void CTableLineData::Disable()
{
	m_bEnabled = false ;
};	// Disable

// 050321 MVy: get current state
bool CTableLineData::IsEnabled()
{
	return m_bEnabled ;
};	// IsEnabled

// 050329 MVy: do stuff for agents if possible
void CTableLineData::HandleCheckinCounterRestrictionsForHandlingAgents( DIACCADATA *pCounter )
{
	if( !__CanHandleCheckinCounterRestrictionsForHandlingAgents() 		// 050311 MVy: PRF6903: who is calling me without permission ?
		|| !g_ulHandlingAgent 	// or if there are no agent choosen
		|| !pCounter )		// bad guy ... calling me without data
	{
		// coloring is done only if agernt is selected, otherwise no changes at all
		Enable();
	}
	else
	{
		if( ogCcaDiaFlightData.IsFlightOfAirline( pCounter, g_strarrHandlingAgentAirlinesCodes ) )
		{
			// current dataset is not blocked and so is valid for user handling
			Enable();
			clrBackground = g_rgbHandlingAgentOccBkBar ;		// 050321 MVy: set line background color
		}
		else
			Disable();
	};
};	// HandleCheckinCounterRestrictionsForHandlingAgents
