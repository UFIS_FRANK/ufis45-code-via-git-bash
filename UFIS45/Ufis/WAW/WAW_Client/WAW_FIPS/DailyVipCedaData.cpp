// cflightd.cpp - Read flight data
// 

#include <stdafx.h>
#include <ccsglobl.h>
#include <BasicData.h>
#include <DailyVipCedaData.h>





////////////////////////////////////////////////////////////////////////////
// Construktor   
//
DailyVipCedaData::DailyVipCedaData() : CCSCedaData(&ogCommHandler)
{

	BEGIN_CEDARECINFO(DAILYVIPDATA,AftDataRecInfo)

	CCS_FIELD_CHAR_TRIM(Flnu,"FLNU","URNO of Flight Record in Flight Table", 1)

	END_CEDARECINFO //(AFTDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(AftDataRecInfo)/sizeof(AftDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&AftDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for


	// set Tablename
    strcpy(pcmTableName,"VIP");
	// initialize field names
	strcpy(pcmAftFieldList,"FLNU");
	pcmFieldList = pcmAftFieldList;

	int ilrst = sizeof(DAILYVIPDATA);

	lmBaseID = IDM_ROTATION;


}




////////////////////////////////////////////////////////////////////////////
// Destruktor   
//
DailyVipCedaData::~DailyVipCedaData(void)
{
	TRACE("DailyVipCedaData::~DailyVipCedaData called\n");
	ogDdx.UnRegister(this,NOTUSED);
	omRecInfo.DeleteAll();
	ClearAll();
}






////////////////////////////////////////////////////////////////////////////
// alle Datens�tze l�schen   
//
void DailyVipCedaData::ClearAll(void)
{

	//VIP
 	omFlnuMap.RemoveAll();
	omData.DeleteAll();

}


bool DailyVipCedaData::ReadAllVips(const CUIntArray& olUrnos)
{

	CString olSelection;
	char pclSelection[8000] = "WHERE FLNU IN (";
	char pclUrno[32];
	
	bool blRet;
	bool blRc;
	int ilLc;

	// kein Statement bei keinen Urnos absetzten
	if (olUrnos.GetSize() == 0)
		return false;

	// Urnos aus Array extrahieren
	for (ilLc = 0; ilLc < olUrnos.GetSize(); ilLc++)
	{
		sprintf(pclUrno, "'%ld'", olUrnos[ilLc]);
		olSelection += pclUrno;
		if (ilLc < olUrnos.GetSize() - 1)
			olSelection += ",";
		pclUrno[0] = 0;
	}
	// Endeklammer der Wehre-Klausel setzen
	olSelection += ")";
	// beide Select-Teile zusammensetzen
	strcat(pclSelection, olSelection.GetBuffer(0));

	                 // ReadTable                                       // Ergebnisbuffer
	blRet = CedaAction("RT", pcmTableName, pcmFieldList, pclSelection, "", pcgDataBuf,
		               "BUF1");
	blRc = blRet;

	for (ilLc = 0; blRc == true; ilLc++)
	{
		DAILYVIPDATA *prFlnu = new DAILYVIPDATA();
		if ((blRc = GetFirstBufferRecord(prFlnu)) == true)
		{
			omData.Add(prFlnu);
			omFlnuMap.SetAt((void *)atol(prFlnu->Flnu), prFlnu);

/*Test		POSITION rlPos;
			for(rlPos = omFlnuMap.GetStartPosition();rlPos!=NULL;)
			{
				long llUrno;
				DAILYVIPDATA *prlFlnu;
				omFlnuMap.GetNextAssoc(rlPos,(void *&)llUrno,(void *& )prlFlnu);
			}
*/
		}
		else
		{
			delete prFlnu;
		}
	}

	return blRet;

}


bool DailyVipCedaData::LookUpUrno(long lpUrno)
{

	DAILYVIPDATA *prpFlnu;
/*Test
	for(POSITION rlPos = omFlnuMap.GetStartPosition();rlPos!=NULL;)
	{
		long llUrno;
		DAILYVIPDATA *prlFlnu;
		omFlnuMap.GetNextAssoc(rlPos,(void *&)llUrno,(void *& )prlFlnu);
	}
*/
 	if ( (omFlnuMap.Lookup((void *)lpUrno, (void *& )prpFlnu)) == TRUE)
	{
		return true;
	}
	else
	{
		return false;
	}

}


////////////////////////////////////////////////////////////////////////////
// Datensatz in die Keymaps einf�gen 
//
bool DailyVipCedaData::AddToKeyMap(DAILYVIPDATA *prpFlight )
{
    return true;
}




////////////////////////////////////////////////////////////////////////////
// Datensatz aus den Keymaps l�schen 
//
bool DailyVipCedaData::DeleteFromKeyMap(DAILYVIPDATA *prpFlight )
{
    return true;
}
