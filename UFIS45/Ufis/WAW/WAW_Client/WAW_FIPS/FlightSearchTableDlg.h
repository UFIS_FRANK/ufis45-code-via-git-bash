#if !defined(FLIGHTSEARCHTABLEDLG_H__INCLUDED_)
#define FLIGHTSEARCHTABLEDLG_H__INCLUDED_



#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// FlightSearchTableDlg.h : header file
//

#include <FlightSearchTableViewer.h>
#include <CCSTable.h>
#include <CCSDragDropCtrl.h>
#include <resrc1.h>

/////////////////////////////////////////////////////////////////////////////
// FlightSearchTableDlg dialog

class FlightSearchTableDlg : public CDialog
{
// Construction
public:
	FlightSearchTableDlg(CWnd* pParent = NULL);   // standard constructor
	~FlightSearchTableDlg();

	void Activate(const CString &ropWhere);
	int Activate(const CString &ropViewerKey, const CString &ropView, FipsModules ipMode);

	void SetUrnoFilter( CMapPtrToPtr *popUrnoMap);

    CMapPtrToPtr omUrnoMap;

	CString omWhere;
	CString omWhereTime;
	CString omFtyps;
	bool bmRotation;
	CTime omFrom;
	CTime omTo;

// Dialog Data
	//{{AFX_DATA(FlightSearchTableDlg)
	enum { IDD = IDD_FLIGHTSEARCH };
	CCSButtonCtrl	m_CB_Show;
	CCSButtonCtrl	m_CB_Cancel;
 		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FlightSearchTableDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FlightSearchTableDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
	afx_msg void OnShowObject();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
    afx_msg LONG OnTableReturnPressed(UINT wParam, LONG lParam);
	afx_msg LONG OnTableSelChanged( UINT wParam, LPARAM lParam);
	afx_msg void OnSize(UINT nType, int cx, int cy);
 	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void ShowFlight(long lpAUrno, long lpDUrno);
	void ShowMask(const DIAFLIGHTDATA *prlFlight);

	bool UpdateTable();


	bool bmLocal;
	int imButtonSpace;
	FipsModules imMode;
	int imModeStringId;
	CCSTable *pomTable;

	FlightSearchTableViewer omViewer;

	DiaCedaFlightData *pomFlightData;


private:
	bool isCreated;

//	void OnSize(UINT nType, int cx, int cy);

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(FLIGHTSEARCHTABLEDLG_H__INCLUDED_)
