REM 
REM Generate UFIS Client-Software for WAW
REM
REM 

Rem BEGIN
date /T
time /T 
set path=%path%;C:\Program Files\Microsoft SDKs\Windows\v6.0A\bin
rem set /p UserInputPath="Enter FIPS and Rules path for your project: "

Rem ---Check for folder c:\ufis\system---
IF EXIST c:\ufis\system\nul GOTO cont1
Echo Create folder C:\ufis\system
md c:\ufis\system
:cont1

IF EXIST C:\Ufis_Bin\Runtimedlls\nul goto cont2
Echo Create folder C:\ufis_Bin\Runtimedlls
md C:\Ufis_Bin\Runtimedlls
:cont2

IF EXIST C:\tmp\nul goto cont3
Echo Create folder C:\tmp
md C:\tmp
:cont3


ECHO Build Project Warsaw Terminal 2 > con
ECHO Projekt Warsaw Terminal 2 > C:\Ufis_Bin\WAW45.txt


mkdir c:\Ufis_Bin\ClassLib
mkdir c:\Ufis_Bin\ClassLib\Include
mkdir c:\Ufis_Bin\ClassLib\Include\jm
mkdir C:\Ufis_Bin\ForaignLibs
mkdir C:\Ufis_Bin\Help
mkdir c:\Ufis_Bin\ClassLib\Release
mkdir c:\Ufis_Bin\Release



copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\lib\*.lib C:\Ufis_Bin\ForaignLibs
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\lib\*.pdb C:\Ufis_Bin\ForaignLibs
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\lib\*.dll C:\Ufis_Bin\Runtimedlls
copy ..\..\..\_Standard\_Standard_client\_Standard_configuration\Clientruntime\*.* C:\Ufis_Bin\Runtimedlls
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\include\*.h c:\Ufis_Bin\ClassLib\Include
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\include\jm\*.h c:\Ufis_Bin\ClassLib\Include\jm
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_ClassLib\*.h c:\Ufis_Bin\ClassLib\Include
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Wrapper\*.h c:\Ufis_Bin\ClassLib\Include
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Classlib\Classlib.rc c:\Ufis_Bin\ClassLib\Include

copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_HtmlHelp\lib\*.lib C:\Ufis_Bin\ForaignLibs
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_HtmlHelp\include\*.h c:\Ufis_Bin\ClassLib\Include
rem copy ..\..\..\_Standard\_Standard_client\_Standard_help\*.chm c:\Ufis_Bin\help
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_CommonLib\*.*  C:\Ufis_Bin\ClassLib\Release
copy ..\..\..\_Standard\_Standard_Client\_Standard_Flight\_Standard_CDID\DeicingClass\*.dll C:\Ufis_Bin\Release
rem copy %UserInputPath%\_Standard\_Standard_Client\_Standard_Flight\_Standard_Fips\*.* ..\..\..\_Standard\_Standard_Client\_Standard_Flight\_Standard_Fips
rem copy %UserInputPath%\_Standard\_Standard_Client\_Standard_Flight\_Standard_Ruleseditorflight\*.* ..\..\..\_Standard\_Standard_Client\_Standard_Flight\_Standard_Ruleseditorflight


copy ..\WAW_Configuration\*.ini c:\Ufis_Bin
copy ..\WAW_Configuration\*.cfg c:\Ufis_Bin
copy ..\WAW_configuration\*.bmp c:\Ufis_Bin
copy copy_for_install_waw.bat c:\Ufis_Bin


REM ------------------Cients build from _Standard-Project-----------------------------------

REM ------------------------Share-----------------------------------------------------------

cd ..\..\..\_Standard\_Standard_client

ECHO Build Ufis32.dll... > con
cd _Standard_Share\_Standard_Ufis32dll
msdev Ufis32.dsp /MAKE "Ufis32 - Debug" /REBUILD
msdev Ufis32.dsp /MAKE "Ufis32 - Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Ufis32.ilk del c:\Ufis_Bin\Debug\Ufis32.ilk
If exist c:\Ufis_Bin\Debug\Ufis32.pdb del c:\Ufis_Bin\Debug\Ufis32.pdb
If exist c:\Ufis_Bin\Debug\Ufis32.pdb del c:\Ufis_Bin\Debug\Ufis32.exp
If exist c:\Ufis_Bin\Release\Ufis32.ilk del c:\Ufis_Bin\Release\Ufis32.ilk
If exist c:\Ufis_Bin\Release\Ufis32.pdb del c:\Ufis_Bin\Release\Ufis32.pdb
If exist c:\Ufis_Bin\Release\Ufis32.pdb del c:\Ufis_Bin\Release\Ufis32.exp
copy c:\ufis_bin\release\Ufis32.dll c:\ufis_bin\debug\*.*
copy c:\ufis_bin\release\Ufis32.lib c:\ufis_bin\debug\*.*
cd ..\..

ECHO Build Classlib... > con
cd _Standard_Share\_Standard_ClassLib
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Debug" /REBUILD
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Release" /REBUILD
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Dll Debug" /REBUILD
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Dll Release" /REBUILD
cd ..\..

ECHO Build BcServ... > con
cd _Standard_Share\_Standard_BcServ
rem msdev BcServ32.dsp /MAKE "BcServ - Debug" /REBUILD
msdev BcServ32.dsp /MAKE "BcServ - Release" /REBUILD
If exist c:\Ufis_Bin\Release\BcServ32.ilk del c:\Ufis_Bin\Release\BcServ32.ilk
If exist c:\Ufis_Bin\Release\BcServ32.pdb del c:\Ufis_Bin\Release\BcServ32.pdb
cd ..\..

ECHO Build BcProxy... > con
cd _Standard_Share\_Standard_BcProxy
Rem ---remove read-only state---
attrib *.* -r
rem msdev BcProxy.dsp /MAKE "BcProxy - Win32 Debug" /REBUILD
msdev BcProxy.dsp /MAKE "BcProxy - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\BcProxy.ilk del c:\Ufis_Bin\Release\BcProxy.ilk
If exist c:\Ufis_Bin\Release\BcProxy.pdb del c:\Ufis_Bin\Release\BcProxy.pdb
cd ..\..

ECHO Build UfisAppMng... > con
cd _Standard_Share\_Standard_UfisApManager
Rem ---remove read-only state---
attrib *.* -r
rem msdev UfisAppMng.dsp /MAKE "UfisAppMng - Win32 Debug" /REBUILD
msdev UfisAppMng.dsp /MAKE "UfisAppMng - Win32 Release" /REBUILD
copy UFISAppMng.tlb c:\ufis\system
If exist c:\Ufis_Bin\Release\UfisAppMng.ilk del c:\Ufis_Bin\Release\UfisAppMng.ilk
If exist c:\Ufis_Bin\Release\UfisAppMng.pdb del c:\Ufis_Bin\Release\UfisAppMng.pdb
cd ..\..

ECHO Build Tabocx... > con
cd _Standard_Share\_Standard_TABocx
rem msdev TAB.dsp /MAKE "TAB - Win32 Debug" /REBUILD
msdev TAB.dsp /MAKE "TAB - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\TAB.ilk del c:\Ufis_Bin\Release\TAB.ilk
If exist c:\Ufis_Bin\Release\TAB.pdb del c:\Ufis_Bin\Release\TAB.pdb
cd ..\..

ECHO Build UfisCedaocx... > con
cd _Standard_Share\_Standard_Ufiscedaocx
rem msdev UfisCom.dsp /MAKE "UfisCom - Win32 Debug" /REBUILD
msdev UfisCom.dsp /MAKE "UfisCom - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\UfisCom.ilk del c:\Ufis_Bin\Release\UfisCom.ilk
If exist c:\Ufis_Bin\Release\UfisCom.pdb del c:\Ufis_Bin\Release\UfisCom.pdb
cd ..\..

ECHO Build Ganttocx... > con
cd _Standard_Share\_Standard_Ganttocx
rem msdev UGantt.dsp /MAKE "UGantt - Win32 Debug" /REBUILD
rem msdev UGantt.dsp /MAKE "UGantt - Win32 Release" /REBUILD
devenv UGantt.sln /REBUILD Release /project UGantt
If exist c:\Ufis_Bin\Release\UGantt.ilk del c:\Ufis_Bin\Release\UGantt.ilk
If exist c:\Ufis_Bin\Release\UGantt.pdb del c:\Ufis_Bin\Release\UGantt.pdb
cd ..\..

ECHO Build UComocx... > con
cd _Standard_Share\_Standard_UCom
rem msdev UCom.dsp /MAKE "UCom - Win32 Debug" /REBUILD
msdev UCom.dsp /MAKE "UCom - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\UCom.ilk del c:\Ufis_Bin\Release\UCom.ilk
If exist c:\Ufis_Bin\Release\UCom.pdb del c:\Ufis_Bin\Release\UCom.pdb
cd ..\..

ECHO Build AatLoginocx... > con
cd _Standard_Share\_Standard_Login
rem msdev AatLogin.dsp /MAKE "AatLogin - Win32 Debug" /REBUILD
msdev AatLogin.dsp /MAKE "AatLogin - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\AatLogin.ilk del c:\Ufis_Bin\Release\AatLogin.ilk
If exist c:\Ufis_Bin\Release\AatLogin.pdb del c:\Ufis_Bin\Release\AatLogin.pdb
cd ..\..
sn -k AatLogin.snk
AxImp c:\ufis_Bin\release\AatLogin.ocx /keyfile:AatLogin.snk /out:C:\ufis_bin\release\AxAATLOGINLib.dll

ECHO Build Deicing... > con
cd _Standard_UfisAddOn\_Standard_DataChanges_DeIcing
sn -k DeicingClass.snk
TlbImp c:\ufis_Bin\release\DeicingClass.dll  /keyfile:DeicingClass.snk /out:C:\ufis_bin\release\AxDeicingClass.dll
cd ..\..


ECHO Build BdpsPass... > con
cd _Standard_Share\_Standard_Bdpspass
msdev Bdpspass.dsp /MAKE "Bdpspass - Win32 Debug" /REBUILD
rem msdev Bdpspass.dsp /MAKE "Bdpspass - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Bdpspass.ilk del c:\Ufis_Bin\Debug\Bdpspass.ilk
If exist c:\Ufis_Bin\Debug\Bdpspass.pdb del c:\Ufis_Bin\Debug\Bdpspass.pdb
cd ..\..

ECHO Build BdpsSec... > con
cd _Standard_Share\_Standard_Bdpssec
msdev Bdps_sec.dsp /MAKE "Bdps_sec - Win32 Debug" /REBUILD
rem msdev Bdps_sec.dsp /MAKE "Bdps_sec - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Bdps_sec.ilk del c:\Ufis_Bin\Debug\Bdps_sec.ilk
If exist c:\Ufis_Bin\Debug\Bdps_sec.pdb del c:\Ufis_Bin\Debug\Bdps_sec.pdb
cd ..\..

ECHO Build BdpsUif... > con
cd _Standard_Share\_Standard_Bdpsuif
msdev Bdpsuif.dsp /MAKE "Bdpsuif - Win32 Debug" /REBUILD
rem msdev Bdpsuif.dsp /MAKE "Bdpsuif - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Bdpsuif.ilk del c:\Ufis_Bin\Debug\Bdpsuif.ilk
If exist c:\Ufis_Bin\Debug\Bdpsuif.pdb del c:\Ufis_Bin\Debug\Bdpsuif.pdb
cd ..\..


REM ------------------------Flight--------------------------------------

ECHO Build Fips... > con
cd ..\..\..\branches\4.5.05\4.5.05.28_STD\_Standard\_Standard_Client\_Standard_Flight\_Standard_Fips
rem cd _Standard_Flight\_Standard_Fips
msdev FPMS.dsp /MAKE "FPMS - Win32 Debug" /REBUILD
rem msdev FPMS.dsp /MAKE "FPMS - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\FIPS.ilk del c:\Ufis_Bin\Debug\FIPS.ilk
If exist c:\Ufis_Bin\Debug\FIPS.pdb del c:\Ufis_Bin\Debug\FIPS.pdb
cd ..\..

ECHO Build FipsRules... > con
cd _Standard_Flight\_Standard_Ruleseditorflight
msdev Rules.dsp /MAKE "Rules - Win32 Debug" /REBUILD
rem msdev Rules.dsp /MAKE "Rules - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Rules.ilk del c:\Ufis_Bin\Debug\Rules.ilk
If exist c:\Ufis_Bin\Debug\Rules.pdb del c:\Ufis_Bin\Debug\Rules.pdb
rem cd ..\..
cd ..\..\..\..\..\..\..\Ufis\_Standard\_Standard_Client

REM ------------------------Fids----------------------------------------

ECHO Build Fidas... > con
cd _Standard_Fids\_Standard_Fidas
msdev Fidas.dsp /MAKE "Fidas - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\Fidas.ilk del c:\Ufis_Bin\Release\Fidas.ilk
If exist c:\Ufis_Bin\Release\Fidas.pdb del c:\Ufis_Bin\Release\Fidas.pdb
cd ..\..


REM ------------Visual Bacic Clients have to be build--------------------

ECHO Build Telexpool... > con
cd _Standard_Flight\_Standard_Telexpool
vb6 /make Telexpool.vbp /out C:\Ufis_Bin\vbappl.log
cd ..\..

ECHO Build LoadTabViewer... > con
cd _Standard_Flight\_Standard_LoaTabViewer
vb6 /make LoadTabViewer.vbp /out C:\Ufis_Bin\vbappl.log
cd ..\..

REM ------------C# Clients have to be build------------------------------

ECHO Build Ufis.Utils.dll... > con
cd _Standard_UfisAddOn\_Standard_Utilities
devenv Utilities.sln /Rebuild Release /project ufis.utils
cd ..\..

ECHO Build ZedGraph... > con
cd _Standard_UfisAddOn\_Standard_ZedGraph
sn -k ..\_Standard_AddOnWrapper\InteropDll\ufis.snk
devenv ZedGraph.sln /Rebuild Release /project ZedGraph
cd ..\..

ECHO Build LocationChanges... > con
cd _Standard_UfisAddOn\_Standard_DataChanges\Data_Changes
devenv Data_Changes.sln /Rebuild Release /project Data_Changes
cd ..\..\..

ECHO Build FIPS Reports UserControls... > con
cd _Standard_Flight\_Standard_FIPSReport
devenv FIPS_Reports.sln /Rebuild Release /project UserControls
cd ..\..

ECHO Build FIPS Reports... > con
cd _Standard_Flight\_Standard_FIPSReport
devenv FIPS_Reports.sln /Rebuild Release /project FIPS_Reports
cd ..\..


REM ------------------Cients build from Customer-Project-----------------

cd ..\..\WAW\WAW_Client

REM ECHO Build GateState... > con
REM cd WAW_Fids\WAW_GateState
REM vb6 /make GateState.vbp /out C:\Ufis_Bin\vbappl.log
REM cd ..\..

ECHO Build BasTabViewer... > con
cd WAW_Share\WAW_Bas
vb6 /make Bas.vbp /out C:\Ufis_Bin\vbappl.log
cd ..\..

ECHO Build MasterClock... > con
cd WAW_Share\WAW_Masterclock
vb6 /make MasterClock.vbp /out C:\Ufis_Bin\vbappl.log
cd ..\..



ECHO ...Done > con


:end
Rem END
time/T
