rem Copies all generated files from the SCM-Structue into the Install-Shield directory.


rem set drive to J: (IS 6.2) or K: (IS 8.0) 
set drive=C:\InstallShield

rem Copy ini- and config-files
copy c:\ufis_bin\*.ini %drive%\waw_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.cfg %drive%\waw_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.bmp %drive%\waw_build\ufis_client_appl\system\*.*

rem copy applications
copy c:\ufis_bin\debug\bdps_sec.exe %drive%\waw_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpspass.exe %drive%\waw_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpsuif.exe %drive%\waw_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\fips.exe %drive%\waw_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\rules.exe %drive%\waw_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\fidas.exe %drive%\waw_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\telexpool.exe %drive%\waw_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\importflights.exe %drive%\waw_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\masterclock.exe %drive%\waw_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\loadtabviewer.exe %drive%\waw_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\bastabviewer.exe %drive%\waw_build\ufis_client_appl\applications\*.*


REM copy system-files
copy c:\ufis_bin\release\bcproxy.exe %drive%\waw_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.tlb %drive%\waw_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcserv32.exe %drive%\waw_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufis32.dll %drive%\waw_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.exe %drive%\waw_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.tlb %drive%\waw_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.ocx %drive%\waw_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.tlb %drive%\waw_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.ocx %drive%\waw_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.tlb %drive%\waw_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.ocx %drive%\waw_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.tlb %drive%\waw_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.ocx %drive%\waw_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.tlb %drive%\waw_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.ocx %drive%\waw_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.tlb %drive%\waw_build\ufis_client_appl\system\*.*

copy c:\ufis_bin\release\data_changes.exe %drive%\waw_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\fips_reports.exe %drive%\waw_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\Ufis.Utils.dll %drive%\waw_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\UserControls.dll %drive%\waw_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\ZedGraph.dll %drive%\waw_build\ufis_client_appl\applications\interopdll\*.*


rem Copy Help-Files
rem copy c:\ufis_bin\Help\*.chm %drive%\waw_build\ufis_client_appl\help\*.*
