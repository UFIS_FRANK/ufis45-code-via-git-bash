VERSION 5.00
Begin VB.Form frmLoadDefinition 
   Caption         =   "Security Logging Analyser ..."
   ClientHeight    =   1395
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4995
   LinkTopic       =   "Form1"
   ScaleHeight     =   1395
   ScaleWidth      =   4995
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      Height          =   315
      Left            =   4200
      TabIndex        =   7
      Top             =   360
      Width           =   675
   End
   Begin VB.TextBox txtToTime 
      Height          =   285
      Left            =   3480
      TabIndex        =   5
      Text            =   "0000"
      Top             =   360
      Width           =   615
   End
   Begin VB.TextBox txtToDate 
      Height          =   285
      Left            =   2520
      TabIndex        =   4
      Text            =   "20.03.2003"
      Top             =   360
      Width           =   975
   End
   Begin VB.TextBox txtFromTime 
      Height          =   285
      Left            =   1560
      TabIndex        =   3
      Text            =   "0000"
      Top             =   360
      Width           =   615
   End
   Begin VB.TextBox txtFromDate 
      Height          =   285
      Left            =   600
      TabIndex        =   2
      Text            =   "20.03.2003"
      Top             =   360
      Width           =   975
   End
   Begin VB.CommandButton cmdLoad 
      Caption         =   "&Load"
      Height          =   375
      Left            =   1920
      TabIndex        =   0
      Top             =   900
      Width           =   1155
   End
   Begin VB.Label Label2 
      Caption         =   "to"
      Height          =   195
      Left            =   2220
      TabIndex        =   6
      Top             =   420
      Width           =   255
   End
   Begin VB.Label Label1 
      Caption         =   "Period:"
      Height          =   195
      Left            =   60
      TabIndex        =   1
      Top             =   420
      Width           =   495
   End
End
Attribute VB_Name = "frmLoadDefinition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public bgReload As Boolean
Private Sub cbField_Change()
'    If bgReload = True Then frmMain.SetGridFromLogTable
End Sub

Private Sub cbField_Click()
    If bgReload = True Then FrmMain.SetGridFromLogTable
End Sub

Private Sub cbType_Change()
'    If bgReload = True Then frmMain.SetGridFromLogTable
End Sub

Private Sub cbType_Click()
    If bgReload = True Then FrmMain.SetGridFromLogTable
End Sub

Private Sub cbUsers_Change()
'    If bgReload = True Then frmMain.SetGridFromLogTable
End Sub

Private Sub cbUsers_Click()
    If bgReload = True Then FrmMain.SetGridFromLogTable
End Sub

Private Sub cmdLoad_Click()
    Dim strWhere As String
    LoadMyData
End Sub




Private Sub Form_Load()
    Dim today As Date
    Dim datFrom As Date
    
    today = Now
    datFrom = DateAdd("n", -2, today)
    txtFromDate = Format(datFrom, "DD.MM.YYYY")
    txtFromTime = Format(datFrom, "hh:mm")
    txtToDate = Format(today, "DD.MM.YYYY")
    txtToTime = Format(today, "hh:mm")
'END: Read position from registry and move the window
End Sub

'===============================================
' generates the where string from the field
' settings
'===============================================
Public Sub LoadMyData()
    Dim strFromDate As String
    Dim strToDate As String
    Dim strCedaFrom As String
    Dim strCedaTo As String
    Dim datFrom As Date
    Dim datTo As Date
    Dim strTable As String
    Dim strLogTab As String
    Dim strUser As String
    Dim strType As String
    Dim strWhere As String
    Dim datReadTo As Date
    Dim tmpFr As String
    Dim tmpTo As String
    Dim diffValue As Integer
    
    frmHiddenData.tabData(2).ResetContent
    frmHiddenData.tabData(2).HeaderString = "URNO,ORNO,SFKT,STAB,TIME,USEC,FLST,FVAL"
    frmHiddenData.tabData(2).HeaderLengthString = "80,80,80,80,100,80,200,200"
    frmHiddenData.tabData(2).LogicalFieldList = "URNO,ORNO,SFKT,STAB,TIME,USEC,FLST,FVAL"
    frmHiddenData.tabData(2).CedaServerName = strServer
    frmHiddenData.tabData(2).CedaPort = "3357"
    frmHiddenData.tabData(2).CedaHopo = strHopo
    frmHiddenData.tabData(2).CedaCurrentApplication = "LogTabViewer"
    frmHiddenData.tabData(2).CedaTabext = "TAB"
    frmHiddenData.tabData(2).CedaUser = GetWindowsUserName()
    frmHiddenData.tabData(2).CedaWorkstation = GetWorkstationName()
    frmHiddenData.tabData(2).CedaSendTimeout = "3"
    frmHiddenData.tabData(2).CedaReceiveTimeout = "240"
    frmHiddenData.tabData(2).CedaRecordSeparator = Chr(10)
    frmHiddenData.tabData(2).CedaIdentifier = "LogTab"
    frmHiddenData.tabData(2).ShowHorzScroller True
    frmHiddenData.tabData(2).EnableHeaderSizing True
    
    strUser = cbUsers.Text
    strType = cbType.Text
    strTable = txtTable
    strLogTab = txtLogTable
    If strTable = "" Or strLogTab = "" Then
        MsgBox "Please specify a table!!"
        Exit Sub
    End If
    
'---***Read the data
    strCedaFrom = txtFromDate.Tag + txtFromTime + "00"
    strCedaTo = txtToDate.Tag + txtToTime + "00"
    datFrom = CedaFullDateToVb(strCedaFrom)
    datTo = CedaFullDateToVb(strCedaTo)
    progress.Min = 0
    progress.Value = 0
    diffValue = DateDiff("d", datFrom, datTo)
    If diffValue < 1 Then
        progress.Max = 1
    Else
        progress.Max = diffValue + 2
    End If
    If progress.Max = 1 Then progress.Max = 3
    Do
        datReadTo = DateAdd("d", 1, datFrom)
        If datReadTo > datTo Then datReadTo = datTo
        tmpFr = Format(datFrom, "yyyymmdd")
        tmpTo = Format(datReadTo, "yyyymmdd")
        strWhere = " WHERE TIME BETWEEN '" & tmpFr & "000000' AND '" & tmpTo & "235959' AND STAB='" & strTable & "TAB" & "'"
        If strUser <> "**ALL USERS**" Then
            strWhere = strWhere + " AND USEC='" + strUser + "'"
        End If
        If strType <> "**ALL COMMANDS**" Then
            strWhere = strWhere + " AND SFKT='" + strType + "'"
        End If
        strWhere = strWhere + " ORDER BY TIME"
        frmHiddenData.tabData(2).CedaAction "RT", strLogTab, frmHiddenData.tabData(2).HeaderString, "", strWhere
        frmHiddenData.tabData(2).Refresh
        frmHiddenData.lblTab(2).Caption = strLogTab & ": Records = " & CStr(frmHiddenData.tabData(2).GetLineCount())
        frmHiddenData.lblTab(2).Refresh
        datFrom = DateAdd("d", 1, datReadTo)
        progress.Value = (progress.Value + 2)
    Loop While (datFrom <= datTo)
'---***Prepare the viewer for the lines
    FrmMain.SetGridFromLogTable
    progress.Value = 0
End Sub

Private Sub txtFromDate_Change()
    CheckDateField txtFromDate, "dd.mm.yyyy", False
End Sub

Private Sub txtToDate_Change()
    CheckDateField txtToDate, "dd.mm.yyyy", False
End Sub
