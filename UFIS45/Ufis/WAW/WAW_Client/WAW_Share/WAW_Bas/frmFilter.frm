VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "Tab.ocx"
Begin VB.Form frmFilter 
   Caption         =   "Filter ..."
   ClientHeight    =   1800
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5160
   LinkTopic       =   "Form1"
   ScaleHeight     =   1800
   ScaleWidth      =   5160
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton btnActivateFilter 
      Caption         =   "&Activate filter"
      Height          =   375
      Left            =   3420
      TabIndex        =   3
      Top             =   1080
      Width           =   1695
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Clear all"
      Height          =   375
      Left            =   3420
      TabIndex        =   2
      Top             =   660
      Width           =   1695
   End
   Begin TABLib.TAB TAB1 
      Height          =   1215
      Left            =   0
      TabIndex        =   0
      Top             =   660
      Width           =   3315
      _Version        =   65536
      _ExtentX        =   5847
      _ExtentY        =   2143
      _StockProps     =   0
   End
   Begin VB.Label Label1 
      Caption         =   "Please enter criteria, by double clicking a line. In case of no crieria is set, all records will be displayed."
      Height          =   495
      Left            =   60
      TabIndex        =   1
      Top             =   60
      Width           =   5055
   End
End
Attribute VB_Name = "frmFilter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub btnActivateFilter_Click()
    Dim ilc As Integer, i As Integer
    Dim strFilter As String, strTmp As String
    
    ilc = TAB1.GetLineCount
    FrmMain.tabFilter.ResetContent
    strFilter = ""
    For i = 0 To ilc
        strTmp = TAB1.GetLineValues(i)
        If strTmp <> ",," And strTmp <> "" Then
            strFilter = strFilter + strTmp + Chr(10)
        End If
    Next i
    FrmMain.tabFilter.InsertBuffer strFilter, Chr(10)
    FrmMain.strFilter = strFilter
    FrmMain.ReadData
    Unload Me
End Sub

Private Sub Command1_Click()
    Dim i As Integer
    TAB1.ResetContent
    TAB1.HeaderLengthString = 210
    TAB1.HeaderString = "Filter criteria"
    For i = 0 To 15
        TAB1.InsertTextLine "", True
    Next i
    TAB1.EnableInlineEdit True
    FrmMain.strFilter = ""
    'FrmMain.ReadData
End Sub

Private Sub Form_Load()
    Dim ilW As Integer
    Dim ilH As Integer
    ilW = Me.Width
    ilH = Screen.Height
    Move 0, ilH - Me.Height, ilW, Me.Height
    
    InitTab
End Sub

Private Sub TAB1_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    TAB1.SetInplaceEdit LineNo, ColNo, False
    TAB1.SelectBackColor = vbWhite
    TAB1.SelectTextColor = vbBlack
End Sub
Public Sub InitTab()
    Dim i As Integer
    Dim ilCount As Integer
    Dim strFilter As String
    
    TAB1.ResetContent
    TAB1.HeaderLengthString = 210
    TAB1.HeaderString = "Filter criteria"
    If FrmMain.strFilter <> "" Then
        TAB1.InsertBuffer FrmMain.strFilter, Chr(10)
    End If
    ilCount = TAB1.GetLineCount
    For i = ilCount To 15
        TAB1.InsertTextLine "", True
    Next i
    TAB1.EnableInlineEdit True

End Sub
