VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form FrmMain 
   Caption         =   "Building automation system events ..."
   ClientHeight    =   2025
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13230
   Icon            =   "FrmMain.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   2025
   ScaleWidth      =   13230
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdDeleteAllFilters 
      Caption         =   "Delete &All Filters"
      Height          =   315
      Left            =   5880
      TabIndex        =   14
      Top             =   0
      Width           =   1455
   End
   Begin VB.CommandButton cmdDelCurrFileter 
      Caption         =   "Delete &Curr. Filter"
      Height          =   315
      Left            =   4380
      TabIndex        =   13
      Top             =   0
      Width           =   1455
   End
   Begin VB.ComboBox cbFilter 
      Height          =   315
      Left            =   540
      TabIndex        =   11
      Top             =   0
      Width           =   3855
   End
   Begin VB.CommandButton cmdLoad 
      Caption         =   "&Load"
      Default         =   -1  'True
      Height          =   315
      Left            =   7560
      TabIndex        =   10
      Top             =   0
      Width           =   915
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      Height          =   315
      Left            =   12600
      TabIndex        =   9
      Top             =   0
      Width           =   615
   End
   Begin VB.TextBox txtFromDate 
      Height          =   285
      Left            =   9120
      TabIndex        =   3
      Text            =   "20.03.2003"
      Top             =   0
      Width           =   975
   End
   Begin VB.TextBox txtFromTime 
      Height          =   285
      Left            =   10080
      TabIndex        =   5
      Text            =   "0000"
      Top             =   0
      Width           =   615
   End
   Begin VB.TextBox txtToDate 
      Height          =   285
      Left            =   11040
      TabIndex        =   7
      Text            =   "20.03.2003"
      Top             =   0
      Width           =   975
   End
   Begin VB.TextBox txtToTime 
      Height          =   285
      Left            =   12000
      TabIndex        =   8
      Text            =   "0000"
      Top             =   0
      Width           =   615
   End
   Begin TABLib.TAB tabConfirmed 
      Height          =   975
      Left            =   5400
      TabIndex        =   2
      Top             =   960
      Width           =   1395
      _Version        =   65536
      _ExtentX        =   2461
      _ExtentY        =   1720
      _StockProps     =   64
   End
   Begin TABLib.TAB tabFilter 
      Height          =   975
      Left            =   3960
      TabIndex        =   1
      Top             =   960
      Width           =   1395
      _Version        =   65536
      _ExtentX        =   2461
      _ExtentY        =   1720
      _StockProps     =   64
   End
   Begin TABLib.TAB TAB1 
      Height          =   1695
      Left            =   0
      TabIndex        =   0
      Top             =   300
      Width           =   3915
      _Version        =   65536
      _ExtentX        =   6906
      _ExtentY        =   2990
      _StockProps     =   64
   End
   Begin VB.Label Label3 
      Caption         =   "Filter:"
      Height          =   195
      Left            =   0
      TabIndex        =   12
      Top             =   60
      Width           =   495
   End
   Begin VB.Label Label1 
      Caption         =   "Period:"
      Height          =   195
      Left            =   8580
      TabIndex        =   6
      Top             =   60
      Width           =   495
   End
   Begin VB.Label Label2 
      Caption         =   "to"
      Height          =   195
      Left            =   10740
      TabIndex        =   4
      Top             =   60
      Width           =   255
   End
End
Attribute VB_Name = "FrmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public strFields As String
Public strTable As String
Public strWhere As String
Public strHopo As String
Public strServer As String
Public imTimerValue As Long
Public strConfigFileName As String
Public strFilter As String

Private Sub cmdDelCurrFileter_Click()
    Dim strFilter As String
    Dim i As Integer
    Dim found As Boolean
    Dim idxFound As Integer
    
    idxFound = -1
    found = False
    strFilter = cbFilter.Text
    For i = 0 To cbFilter.ListCount - 1
        If cbFilter.List(i) = strFilter Then
            idxFound = i
        End If
    Next i
    If idxFound > -1 Then
        cbFilter.RemoveItem (idxFound)
    End If
    cbFilter.Text = ""
End Sub

Private Sub cmdDeleteAllFilters_Click()
    While cbFilter.ListCount > 0
        cbFilter.RemoveItem (0)
    Wend
    cbFilter.Text = ""
End Sub

Private Sub cmdLoad_Click()
    Dim strFilter As String
    Dim i As Integer
    Dim found As Boolean
    
    found = False
    strFilter = cbFilter.Text
    For i = 0 To cbFilter.ListCount - 1
        If cbFilter.List(i) = strFilter Then
            found = True
        End If
    Next i
    If found = False Then
        cbFilter.AddItem strFilter
    End If
    ReadData
End Sub

Private Sub cmdReset_Click()
    ResetTimeFrame
End Sub

Private Sub Form_Load()
    Dim ilW As Long
    Dim ilH As Long
    Dim l As Long
    Dim i As Long
    Dim strValue As String
    Dim isResult As String
    Dim isConfigString As String
    Dim ret As Integer
    
    GetUfisDir
    ResetTimeFrame
    strFields = "TIME,TEXT,STAT,ADDR"
    'strConfigFileName = "C:\Ufis\System\ceda.ini"
    strConfigFileName = DEFAULT_CEDA_INI
    isConfigString = GetFileContent(strConfigFileName)
    
    strServer = GetIniEntry("", "BASTABVIEWER", "GLOBAL", "HOSTNAME", "")
    strHopo = GetIniEntry("", "BASTABVIEWER", "GLOBAL", "HOMEAIRPORT", "")
    
    ilW = Screen.Width
    ilH = Screen.Height
    Move 0, ilH - Me.Height, ilW, Me.Height
    TAB1.ResetContent
    TAB1.HeaderString = "TIME,TEXT,STAT,ADDR"
    TAB1.LogicalFieldList = "TIME,TEXT,STAT,ADDR"
    TAB1.HeaderLengthString = "70,600,130,100"
    TAB1.DateTimeSetColumn 0
    TAB1.DateTimeSetColumnFormat 0, "YYYYMMDDhhmmss", "DDMMMYY'/'hh':'mm"
    TAB1.EnableHeaderSizing True
    TAB1.AutoSizeByHeader = True
    TAB1.LineHeight = 16
    tabFilter.ResetContent
    tabFilter.HeaderString = "Filter"
    tabFilter.HeaderLengthString = "100"
    'tabFilter.ReadFromFile "C:\Ufis\System\BasTabViewerFilter.cfg"
    tabFilter.ReadFromFile UFIS_SYSTEM & "\BasTabViewerFilter.cfg"
    tabFilter.Visible = False
    For i = 0 To tabFilter.GetLineCount - 1
        cbFilter.AddItem tabFilter.GetColumnValue(i, 0)
    Next i
    
    
    tabConfirmed.ResetContent
    tabConfirmed.HeaderString = "Confirmed"
    tabConfirmed.HeaderLengthString = "100"
    'tabConfirmed.ReadFromFile "C:\Ufis\System\BAS.cff"
    tabConfirmed.ReadFromFile UFIS_SYSTEM & "\BAS.cff"
    tabConfirmed.Visible = False
    
    
    TAB1.CedaServerName = strServer
    TAB1.CedaPort = "3357"
    TAB1.CedaHopo = strHopo
    TAB1.CedaCurrentApplication = "LogTabViewer"
    TAB1.CedaTabext = "TAB"
    TAB1.CedaUser = GetWindowsUserName()
    TAB1.CedaWorkstation = GetWorkStationName()
    TAB1.CedaSendTimeout = "3"
    TAB1.CedaReceiveTimeout = "240"
    TAB1.CedaRecordSeparator = Chr(10)
    TAB1.CedaIdentifier = "LogTab"
    TAB1.ShowHorzScroller False
    TAB1.EnableHeaderSizing True
    
'    ReadData
        
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim i As Integer
    tabFilter.ResetContent
    For i = 0 To cbFilter.ListCount - 1
        tabFilter.InsertTextLine cbFilter.List(i), False
    Next i
    'tabFilter.WriteToFile "C:\Ufis\System\BasTabViewerFilter.cfg", False
    tabFilter.WriteToFile UFIS_SYSTEM & "\BasTabViewerFilter.cfg", False
End Sub

'----------------------------------------------------
' Size the tab to fit the sizes of the form
'----------------------------------------------------
Private Sub Form_Resize()
    Dim ilW As Long
    Dim ilH As Long
    ilW = Me.Width
    ilH = Me.Height
    ilH = ilH
    If (ilW - 100 > 0) And (Me.Height - 570) > 0 Then
        TAB1.Move 0, cmdLoad.Height + 1, ilW - 100, Me.Height - 570
    End If
End Sub

'------------------------------------------------------
' Read the data within the last 24 hours
'------------------------------------------------------
Public Function ReadData()
    Dim datFrom As Date
    Dim datTo As Date
    Dim recCount As Long
    Dim i As Long, j As Long
    Dim tmpStr As String
    Dim rawString As String
    Dim blFound As Boolean
    Dim strTmpF As String
    Dim strTo As String, strFrom As String
    Dim strFilter As String
    
    TAB1.ResetContent
    strWhere = ""
    
    strFilter = cbFilter.Text
    strFrom = txtFromDate.Tag + txtFromTime + "00"
    strTo = txtToDate.Tag + txtToTime + "00"
    datFrom = CedaFullDateToVb(strFrom)
    datTo = CedaFullDateToVb(strTo)
    strFrom = Format(datFrom, "YYYYMMDDhhmmss")
    strTo = Format(datTo, "YYYYMMDDhhmmss")
       
    strWhere = " WHERE TIME BETWEEN '" + strFrom + "' AND '" + strTo + "'"
    If strFilter <> "" Then
        strWhere = strWhere + " AND TEXT LIKE " + "'%" + strFilter + "%'"
    End If
    strWhere = strWhere + " ORDER BY TIME"
    
    TAB1.CedaAction "RT", "BASTAB", TAB1.HeaderString, "", strWhere
    
    TAB1.RedrawTab
    'TAB1.AutoSizeColumns
End Function

Public Sub ResetTimeFrame()
    Dim today As Date
    Dim datFrom As Date
    
    today = Now
    datFrom = DateAdd("n", -120, today)
    txtFromDate = Format(datFrom, "DD.MM.YYYY")
    txtFromTime = Format(datFrom, "hh:mm")
    txtToDate = Format(today, "DD.MM.YYYY")
    txtToTime = Format(today, "hh:mm")
End Sub

Private Sub txtFromDate_Change()
    CheckDateField txtFromDate, "dd.mm.yyyy", False
End Sub

Private Sub txtToDate_Change()
    CheckDateField txtToDate, "dd.mm.yyyy", False
End Sub
