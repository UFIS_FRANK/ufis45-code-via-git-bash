VERSION 5.00
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Begin VB.Form Form1 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "UFIS-Masterclock-interface"
   ClientHeight    =   3135
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   3975
   FillStyle       =   0  'Solid
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Masterclock.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3135
   ScaleWidth      =   3975
   StartUpPosition =   2  'CenterScreen
   Begin MSCommLib.MSComm MC1 
      Left            =   3000
      Top             =   600
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DTREnable       =   0   'False
      InputLen        =   1024
      RThreshold      =   5
      RTSEnable       =   -1  'True
      SThreshold      =   5
   End
   Begin VB.TextBox Text2 
      Alignment       =   1  'Right Justify
      BeginProperty DataFormat 
         Type            =   0
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1031
         SubFormatType   =   0
      EndProperty
      Height          =   375
      Left            =   1680
      MaxLength       =   3
      TabIndex        =   0
      Text            =   "15"
      Top             =   2520
      Width           =   450
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Test"
      Height          =   375
      Left            =   2760
      TabIndex        =   1
      Top             =   2520
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Caption         =   "Timer settings "
      Height          =   735
      Left            =   120
      TabIndex        =   2
      Top             =   2280
      Width           =   2295
      Begin VB.Label Label1 
         Caption         =   "Poll rate (min.)"
         Height          =   315
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   1305
      End
   End
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   2640
      TabIndex        =   3
      Top             =   2280
      Width           =   1215
   End
   Begin VB.Frame Frame3 
      Caption         =   "Clock-I/O-communication"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   3735
      Begin VB.Timer Timer2 
         Interval        =   60000
         Left            =   240
         Top             =   720
      End
      Begin VB.ListBox Response 
         Height          =   1530
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   3495
      End
   End
   Begin VB.Menu File 
      Caption         =   "File"
      Begin VB.Menu Exit 
         Caption         =   "Exit"
      End
   End
   Begin VB.Menu Info 
      Caption         =   "Info"
      Begin VB.Menu About 
         Caption         =   "About"
      End
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim timerCount As Integer
Dim PollRate As Long


Private Sub About_Click()
    frmAbout.Show vbModal
End Sub

Private Sub Command1_Click()
    Machmal
End Sub



Private Sub Exit_Click()
    End
End Sub

Private Sub Form_Load()
    timerCount = 0
    '  PollRate = 1000 'about 1 minute
'    Timer1.Interval = PollRate
'    Timer1.Enabled = True
    
   'If Not MC1.PortOpen Then
    '    MC1.CommPort = 1
    '    MC1.PortOpen = True
   'End If
    
    
End Sub


Private Sub Timer1_Timer()
    timerCount = timerCount + 1
    If timerCount = CInt(Text2.Text) Then
        timerCount = 0
        Machmal
    End If
End Sub


Public Sub Machmal()
   Dim Buffer As String
   Dim InBuffer As String
   Dim szBufferFromComm As String
    Dim strTmp As String
    Dim abbruch As Boolean
    Buffer = "T"
    If Not MC1.PortOpen Then
        MC1.CommPort = 1
        MC1.PortOpen = True
    End If
    MC1.Output = Buffer
    Response.AddItem Buffer, 0
    On Error GoTo StartError1
    abbruch = False
    szBufferFromComm = ""
     While abbruch = False
         DoEvents
         ' vedi se c'� qualcosa nel buffer della seriale da elaborare
         If MC1.InBufferCount > 0 Then
             Dim Pos As Integer
             
             'szBufferFromComm = szBufferFromComm & MC1.Input
             szBufferFromComm = MC1.Input
             'While szBufferFromComm <> ""
    '         While szBufferFromComm = " "
    '                lStatus(1).Caption = " Serial COM" & ArpaCom.CommPort & " out of sync"
    '                lStatus(1).BackColor = Comm.BackColor
    '              szBufferFromComm = Right$(szBufferFromComm, Len(szBufferFromComm) - 1)
    '                logga "SYNC " & szBufferFromComm, "SYNC " & szBufferFromComm
    '         Wend
        End If
        If Len(szBufferFromComm) > 22 Then
            Response.AddItem szBufferFromComm, 0
            'DATE FORMATTING
            strTmp = Mid(szBufferFromComm, 6, 8)
            strTmp = strTmp + Mid(szBufferFromComm, 16, 6)
            Date = CedaDateToVb(strTmp)
            Time = CedaTimeToVb(strTmp)
            szBufferFromComm = ""
            If Response.ListCount >= 100 Then
                Response.RemoveItem 100
            End If
        End If
        Exit Sub
                    
    Wend
    MC1.PortOpen = False
    Exit Sub
    
StartError1:
    'ArpaComTxt.AddItem "UNEXP ERR -> " & Err & " - " & Error$
     Resume Next
    

End Sub

Private Sub MC1_OnComm()
    'MsgBox "Hallo"
End Sub

Private Sub Timer2_Timer()
    timerCount = timerCount + 1
    If timerCount >= CInt(Text2.Text) Then
        timerCount = 0
        Machmal
    End If

End Sub
