#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/WAW/WAW_Server/Base/Server/Interface/wpplif.c 1.1 2006/08/25 17:48:06SGT jim Exp  $";
#endif /*_DEF_mks_version*/

/* *******************************************************
 * Modulname:		wpplif
 * Author:			Joern Weerts
 * Date:				28th April 2003
 * Description:	handles the following commands:	
 *							WPPL  	- Incoming WPPL-information telex
 *							about General Aviation - flights
 * 
 * JWE - 28th April 2003: start of development
 * JWE - 30th April 2003: first revision ready
***********************************************************/

/* This program is a CEDA main program */
#define U_MAIN
#define CEDA_PRG

/* The master header file */
#include  "wpplif.h"
#include  "db_if.h"
#include  "tools.h"
#include  "new_catch.h"
#include <time.h>
#ifdef UFIS43
#include "syslib.h"
#endif
/***************
Global variables
****************/
ITEM 			*prgItem = NULL;
EVENT 		*prgEvent = NULL;

static int igItemLen = 0;/* length of incoming item *
static int que_out;   /* Sender que id */

FILE *outp;

int que_out;   /* Sender que id */
int gl_no_ack = FALSE;
int debug_level= TRACE;
char    pcgCfgFile[128];

static int igStartUpMode = TRACE;
static int igRuntimeMode = TRACE;

static char pcgDataArea[DATABLK_SIZE];
static char pcgSqlBuf[DATABLK_SIZE];
static char pcgWhere[DATABLK_SIZE];
static char pcgErrMsg[512];

static char pcgHome[32];
static char pcgInsertCmd[32];
static char pcgInsertModid[32];
static int  igInsertRoute;
static char pcgUpdateCmd[32];
static char pcgInsBc[32];
static char pcgUpdBc[32];
static char pcgIncomingFields[2048];
static char pcgUfisEquiv[2048];
static char pcgFieldPolicy[2048];
static char pcgHandlingMask[2048];
static char pcgSelectFields[2048];

static char pcgArrFrom[32];
static time_t tgArrFrom = 0;
static char pcgArrTo[32];
static time_t tgArrTo = 0;
static char pcgDepFrom[32];
static time_t tgDepFrom = 0;
static char pcgDepTo[32];
static time_t tgDepTo = 0;

static char pcgDefTblExt[128];
static char pcgDefH3LC[128];
static char pcgTblExt[128];
static char pcgH3LC[128];

static time_t tgBeginStamp = 0;
static time_t tgEndStamp = 0;
static time_t tgStampDiff = 0;
/******************
Function prototypes
*******************/
static void InitWpplif();    /* Perform general initialization */
static int 	HandleData();	/* Handles event data	*/
static int reset();		/* Reset program		*/
static void terminate();	/* Terminate program	*/
static void handle_sig();	/* Handles signals	*/
static void handle_err(int);	/* Handles general errors	*/
static void handle_qerr(int);	/* Handles queuing errors	*/
static void HandleQueues(void);

extern SendRemoteShutdown(int);
extern void str_chg_upc(char *);
extern int StrgPutStrg(char *, int *, char *, int, int, char *);
extern int get_real_item(char *, char *, int);
extern void str_trm_all(char *,char *, int);
extern int  get_item_no(char *,char *, short);
extern void BuildItemBuffer(char *,char *,int,char *);
extern int GetNoOfElements(char *, char);
extern void nap(int);
extern void GetServerTimeStamp(char *, int, long, char *);

static int WriteRecordData(char *,char *,char *,char *);

static int ReadCfg(char *, char *, char *, char *, char *);
static void GetLogFileMode(int *ipModeLevel, char *pcpLine, char *pcpDefault);

static int new_tools_send_sql_perf(int ipRouteId, BC_HEAD *prpBchd, CMDBLK *prpCmdblk,
                                   char *pcpFields, char *pcpData);
static int tools_send_info_flag_perf(int ipRouteId, int ipOrigId, char *pcpDestName,
                                     char *pcpOrigName, char *pcpRecvName, char *pcpSeqId,
                                     char *pcpRefSeqId, char *pcpTwStart, char *pcpTwEnd,
                                     char *pcpCommand, char *pcpTabName, char *pcpSelection,
                                     char *pcpFields, char *pcpData, int ipFlag);
static int tools_send_sql_rc_perf(int ipRouteId, char *pcpCommand, char *pcpTabName,
                                  char *pcpSelection, char *pcpFields, char *pcpData,
                                  int ipRetCode);
static int GetOraErr(int ipErrCode, char *pcpResultBuffer, char *pcpSqlStatement,
                     char *pcpRoutine);
static void TrimAll(char *pcpBuffer);
static int StrToTime(char *pcpTime,time_t *plpTime,int ipConvert);
static void TimeToStr(char *pcpTime,time_t lpTime,int ipType);
static int SendEvent(char *pcpCmd,char *pcpSelection,char *pcpFields, char *pcpData,
											char *pcpAddStruct, int ipAddLen,int ipModIdSend,int ipModIdRecv,
											int ipPriority,char *pcpTable,char *pcpType,char *pcpFile,char *pcpResult,int ipWaitForAck);
static int PrepareData(char cpType,char *pcpHandlingMask,char *pcpFld,char *pcpData, char *pcpSelectFields, char *pcpSelectData
											,char *pcpResultFields,char *pcpResultData);
/***************
The MAIN program
****************/
MAIN 
{
	int 		rc; 
	int		ilRC;
	int		ilCnt;
	char		pclFileName[512];
char pclTstFields[128];
char pclTstData[128];

	int ilMsgCounter = 0;
    int ilDbgFileCount = 0;

	/* General initialization	*/
	INITIALIZE;			
	dbg(TRACE,"<MAIN VERSION> %s",mks_version);

	/* handles signal		*/
	(void)SetSignals(handle_sig);
	(void)UnsetSignals();

	/* Attach to the CCS queues */
	ilCnt = 0;
	do
	{
		if ((ilRC = init_que()) != RC_SUCCESS)
		{
			sleep(6);		/* Wait for QCP to create queues */
			ilCnt++;
		}
	} while ((ilCnt < 10) && (ilRC != RC_SUCCESS));

	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que failed!");
		sleep(30);
		terminate();
	}

	/* logon to DB */
	ilCnt = 0;
	do
	{
		dbg(DEBUG,"<MAIN> calling init_db()");
		if ((ilRC = init_db()) != RC_SUCCESS)
		{
			sleep(6);		/* Wait for next try */
			ilCnt++;
		}
	}while ((ilCnt < 10) && (ilRC != RC_SUCCESS));

	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db failed!");
		sleep(30);
		terminate();
	}

	pclFileName[0] = 0x00;
	sprintf(pclFileName, "%s/%s.cfg", getenv("CFG_PATH"),mod_name);
	strcpy(pcgCfgFile,pclFileName);
	dbg(TRACE,"<MAIN> Conf.-file: <%s>",pcgCfgFile);

	GetLogFileMode(&igStartUpMode,"STARTUP_MODE","TRACE");
	debug_level = igStartUpMode;
	GetLogFileMode(&igRuntimeMode,"RUNTIME_MODE","TRACE");

	if ((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"<MAIN> waiting for status switch ...");
		HandleQueues();
	}

	if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"<MAIN> initializing ...");
		InitWpplif();
	} 
	else 
	{
		sleep(30);
		terminate();
	}

	/* only a message */
	dbg(TRACE,"<MAIN> initializing OK");
  debug_level = igRuntimeMode;

	/* forever */
	while(1) 
	{
    dbg(TRACE,"<MAIN> =================== START/END ==================");  
		/* get item */
		rc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,0,(char*)&prgItem);

		/* set event pointer */
		prgEvent = (EVENT*)prgItem->text;

		/* check returncode */
		if (rc == RC_SUCCESS) 
		{
			if ((rc = que(QUE_ACK,0,mod_id,0,0,NULL)) != RC_SUCCESS)
			{
				handle_qerr(rc);
			} 

			switch (prgEvent->command) 
			{
			case REMOTE_DB: 
				HandleRemoteDB(prgEvent);
				break;

			case SHUTDOWN:
				terminate();
				break;

			case RESET:
				rc = reset();
				break;

			case EVENT_DATA:
				if(ctrl_sta==HSB_ACTIVE || ctrl_sta==HSB_STANDALONE || ctrl_sta==HSB_ACT_TO_SBY )
				{
					rc = HandleData();
				} 
				else
				{
					dbg(DEBUG,"MAIN %05d wrong hsb status <%d>", __LINE__, ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}
				break;

			case TRACE_ON:  /* 7 */
			case TRACE_OFF: /* 8 */
				dbg_handle_debug(prgEvent->command);
				break;

			case LOG_SWITCH: /* 2 */
				SwitchDebugFile(debug_level,&ilDbgFileCount);
				break;

			case HSB_DOWN:
				ctrl_sta = prgEvent->command;
				terminate(RC_SHUTDOWN);
				break;

			case HSB_STANDALONE:
				ctrl_sta = prgEvent->command;
				send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
				ResetDBCounter();
				break;

			case HSB_STANDBY:
				ctrl_sta = prgEvent->command;
				send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
				HandleQueues();
				break;

			case HSB_ACTIVE:
				ctrl_sta = prgEvent->command;
				send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
				break;

			case HSB_COMMING_UP:
				ctrl_sta = prgEvent->command;
				send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
				HandleQueues();
				break;

			case HSB_ACT_TO_SBY:
				ctrl_sta = prgEvent->command;
				send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
				HandleQueues();
				break;

			default:
				dbg(TRACE,"<MAIN> unknown command =%d",
				prgEvent->command); 
				break;
			}

			/* Handle error conditions */
			if (rc!=RC_SUCCESS) 
			{
				handle_err(rc);
			}
		}
		else 
		{
			handle_qerr(rc);
		} 
	}
}

/* ******************************************
 * Function: 	InitWpplif
*********************************************/
static void InitWpplif() 
{
  int rc=RC_SUCCESS;
  int ilRC;
  int ilCount;
	int ilFields = 0, ilUfisEq=0 , ilFieldPol=0;
  char *pclCfgPath;
  char pclActPath[128];
  char pclCfgFile[128];
  char pclTmpBuf[128];
  char pclCfgSec[128];
  char pclCfgDet[128];
  char pclTmpItem[128];
	char pclField[128];
	char pclUfisEq[128];
	char pclFieldPol[128];

  pcgDefTblExt[0] = 0x00;
  pcgDefH3LC[0] = 0x00;
  rc = tool_search_exco_data("ALL","TABEND",pcgDefTblExt);
  strcpy(pcgTblExt,pcgDefTblExt);
  rc = tool_search_exco_data("SYS","HOMEAP",pcgDefH3LC);
  strcpy(pcgH3LC,pcgDefH3LC);
  dbg(TRACE,"InitWpplif: DEFAULTS: HOME <%s> EXT <%s>",pcgDefH3LC,pcgDefTblExt);

	memset(pcgIncomingFields,0x00,2048);
	memset(pcgUfisEquiv,0x00,2048);
	memset(pcgFieldPolicy,0x00,2048);

	ReadCfg(pcgCfgFile,pcgHome,"MAIN","HOME","EPWA");
	ReadCfg(pcgCfgFile,pcgInsertCmd,"MAIN","INS_CMD","IDF");

	ReadCfg(pcgCfgFile,pcgInsertModid,"MAIN","INS_ROUTE","7800");
	igInsertRoute=atoi(pcgInsertModid);

	ReadCfg(pcgCfgFile,pcgInsBc,"MAIN","INS_BC","N");
	if (strncmp(pcgInsBc,"N",1)==0)
		strcpy(pcgInsBc,".NBC.");
	else
		strcpy(pcgInsBc,"");
	ReadCfg(pcgCfgFile,pcgUpdateCmd,"MAIN","UPD_CMD","UFR");
	ReadCfg(pcgCfgFile,pcgUpdBc,"MAIN","UPD_BC","Y");
	if (strncmp(pcgUpdBc,"N",1)==0)
		strcpy(pcgUpdBc,".NBC.");
	else
		strcpy(pcgUpdBc,"");
	dbg(TRACE,"InitWpplif: HOME=<%s>",pcgHome);
	ReadCfg(pcgCfgFile,pcgIncomingFields,"MAIN","INCOMING_FIELDS"," ");
	ReadCfg(pcgCfgFile,pcgUfisEquiv,"MAIN","UFIS_EQUIV"," ");
	ReadCfg(pcgCfgFile,pcgFieldPolicy,"MAIN","FIELD_POLICY"," ");

	ReadCfg(pcgCfgFile,pcgArrFrom,"MAIN","ARR_TIFR_FROM","15");
	tgArrFrom = atol(pcgArrFrom)*60;
	ReadCfg(pcgCfgFile,pcgArrTo,"MAIN","ARR_TIFR_TO","15");
	tgArrTo = atol(pcgArrTo)*60;
	ReadCfg(pcgCfgFile,pcgDepFrom,"MAIN","DEP_TIFR_FROM","15");
	tgDepFrom = atol(pcgDepFrom)*60;
	ReadCfg(pcgCfgFile,pcgDepTo,"MAIN","DEP_TIFR_TO","15");
	tgDepTo = atol(pcgDepTo)*60;

 	ilFields	= (int)GetNoOfElements(pcgIncomingFields,',');
 	ilUfisEq	= (int)GetNoOfElements(pcgUfisEquiv,',');
 	ilFieldPol= (int)GetNoOfElements(pcgFieldPolicy,',');

	dbg(DEBUG,"InitWpplif: Fields<%d> FieldPol=<%d>",ilFields,ilFieldPol);
	if ((ilFields != ilFieldPol))
	{
		dbg(TRACE,"InitWpplif: incorrect number of field-config-entries found! Terminating!");
		terminate();
	}
	else
	{
			memset(pcgHandlingMask,0x00,2048);
			memset(pcgSelectFields,0x00,2048);
			strcat(pcgSelectFields,"URNO");
			strcat(pcgSelectFields,",");
			for (ilCount=0; ilCount < ilFields;ilCount++)
			{
				memset(pclField,0x00,128);
				memset(pclUfisEq,0x00,128);
				memset(pclFieldPol,0x00,128);
				GetDataItem(pclField,pcgIncomingFields,ilCount+1,',',"","");
				GetDataItem(pclUfisEq,pcgUfisEquiv,ilCount+1,',',"","");
				GetDataItem(pclFieldPol,pcgFieldPolicy,ilCount+1,',',"","");
				TrimAll(pclField);
				TrimAll(pclUfisEq);
				TrimAll(pclFieldPol);
				if (strlen(pclUfisEq)>0)
				{
					strcat(pcgSelectFields,pclUfisEq);
					strcat(pcgSelectFields,",");
				}
				pclFieldPol[1]=0x00;
				switch(pclFieldPol[0])
				{
					case 'K':
						pcgHandlingMask[ilCount]='K';
						break;
					case 'k':
						pcgHandlingMask[ilCount]='K';
						break;
					case 'O':
						pcgHandlingMask[ilCount]='O';
						break;
					case 'o':
						pcgHandlingMask[ilCount]='O';
						break;
					default:
						pcgHandlingMask[ilCount]='-';
						break;
				}
				dbg(TRACE,"InitWpplif: FIELD=<%s>\tUFISEQ=<%s>\tPOL.=<%c>",pclField,pclUfisEq,pclFieldPol[0]);
			}
			pcgSelectFields[strlen(pcgSelectFields)-1]=0x00;
			dbg(TRACE,"InitWpplif: SELECT-FLDS=<%s>",pcgSelectFields);
			dbg(TRACE,"InitWpplif: HANDLE-MASK=<%s>",pcgHandlingMask);
	}
  return;
} /* end of initialize */

/* *************************************************** 
 * Function: 		HandleData
 * Parameter: 	void
 * Return: 			RC_SUCCESS, RC_FAIL, SQL_NOTFOUND
 * Description: The general handle data routine.
 * Commands recognized: RT,RTA,IBT,GNU,IRT,UBT
 * URT,DRT,DBT.	
 *****************************************************/
static int HandleData() 
{
  int rc = 0;			/* Return code 			*/
  int ilRc,ilRC_Replace;
  char pclModifyAllowed[128];
  int ilTwEndFldCnt;
  BC_HEAD *prlBchd = NULL;		/* Broadcast header		*/
  CMDBLK  *prlCmdblk = NULL;	/* Command Block 		*/
  char *pclSel=NULL,*pclFld=NULL,*pclData=NULL;	
	int ilCnt=0;
	int ilCount=0;
	int ilPosInFields=0, ilCol=0, ilFromStart=0;
  char pclResultData[DATABLK_SIZE];
  char pclResultFields[DATABLK_SIZE];
  char pclTmpBuf[DATABLK_SIZE];
	char pclAdid[32];
	char pclCsgn[32];
	char pclOrg4[32];
	char pclDes4[32];
	char pclStod[32];
	char pclStoa[32];
	char pclUrno[32];
	char pclFlno[32];
	char pclFlnoSave[32];
	char pclAlc3[32];
	char pclAlc2[32];
	char pclFltn[32];
	char pclFlns[32];
	char pclTimeFrom[32];
	char pclTimeTo[32];
	time_t tlTime = 0;
	short slCursor = 0;
	short slFkt = 0;

  que_out = prgEvent->originator; 	/* Queue-id des Absenders */
  prlBchd = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  prlCmdblk = (CMDBLK  *) ((char *)prlBchd->data);
  pclSel = prlCmdblk->data;
  pclFld = pclSel + strlen(pclSel)+1;
  pclData = pclFld + strlen(pclFld)+1;

  dbg(TRACE,"HD: CMD<%s> FROM<%d> TBL<%s> RC=<%d>", prlCmdblk->command,que_out,prlCmdblk->obj_name,prlBchd->rc);
  dbg(DEBUG,"HD: WKS<%s> USR <%s>",prlBchd->recv_name,prlBchd->dest_name);
  dbg(TRACE,"HD: TWS<%s> TWE <%s>",prlCmdblk->tw_start,prlCmdblk->tw_end);
  dbg(TRACE,"HD: SEL<%s>",pclSel);
  dbg(TRACE,"HD: FLD<%s>",pclFld);
  dbg(TRACE,"HD: DAT<%s>",pclData);

  if (strcmp(prlCmdblk->command, "WPPL")==0)
  {
		memset(pclAdid,0x00,32);
		memset(pclCsgn,0x00,32);
		memset(pclOrg4,0x00,32);
		memset(pclDes4,0x00,32);
		memset(pclStod,0x00,32);
		memset(pclStoa,0x00,32);
		memset(pclUrno,0x00,32);
		memset(pclFlno,0x00,32);
		memset(pclAlc3,0x00,32);
		memset(pclAlc2,0x00,32);
		memset(pclFltn,0x00,32);
		memset(pclFlns,0x00,32);
		memset(pclTimeFrom,0x00,32);
		memset(pclTimeTo,0x00,32);

		FindItemInList(pclFld,"ADID",',',&ilPosInFields,&ilCol,&ilFromStart);
		GetDataItem(pclAdid,pclData,ilPosInFields,',',"","");
		FindItemInList(pclFld,"FLID",',',&ilPosInFields,&ilCol,&ilFromStart);
		GetDataItem(pclCsgn,pclData,ilPosInFields,',',"","");
		FindItemInList(pclFld,"ORG4",',',&ilPosInFields,&ilCol,&ilFromStart);
		GetDataItem(pclOrg4,pclData,ilPosInFields,',',"","");
		FindItemInList(pclFld,"DES4",',',&ilPosInFields,&ilCol,&ilFromStart);
		GetDataItem(pclDes4,pclData,ilPosInFields,',',"","");
		FindItemInList(pclFld,"STOD",',',&ilPosInFields,&ilCol,&ilFromStart);
		GetDataItem(pclStod,pclData,ilPosInFields,',',"","");
		FindItemInList(pclFld,"STOA",',',&ilPosInFields,&ilCol,&ilFromStart);
		GetDataItem(pclStoa,pclData,ilPosInFields,',',"","");
		FindItemInList(pclFld,"FLNO",',',&ilPosInFields,&ilCol,&ilFromStart);
		GetDataItem(pclFlno,pclData,ilPosInFields,',',"","");
		FindItemInList(pclFld,"ALC3",',',&ilPosInFields,&ilCol,&ilFromStart);
		GetDataItem(pclAlc3,pclData,ilPosInFields,',',"","");
		TrimAll(pclAlc3);
		FindItemInList(pclFld,"FLTN",',',&ilPosInFields,&ilCol,&ilFromStart);
		GetDataItem(pclFltn,pclData,ilPosInFields,',',""," ");
		TrimAll(pclFltn);
		FindItemInList(pclFld,"FLNS",',',&ilPosInFields,&ilCol,&ilFromStart);
		GetDataItem(pclFlns,pclData,ilPosInFields,',',"","");
		TrimAll(pclFlns);
		if (strlen(pclFlns)==0)
			strncat(pclFlns," ",1);

		/******************************************************************/
		/* getting airline 2 letter code for build of flno with that code */
		/******************************************************************/
		if (strlen(pclFlno)>0)
		{
			*pcgSqlBuf=0x00;
			sprintf(pcgSqlBuf,"SELECT ALC2 FROM ALTTAB WHERE ALC3='%s'",pclAlc3);
			slFkt=START|REL_CURSOR;
			slCursor=0;
			dbg(DEBUG,"HD: SQL: <%s>",pcgSqlBuf);
			ilRc = sql_if(slFkt,&slCursor,pcgSqlBuf,pcgDataArea);
			dbg(DEBUG,"HD: SQL-RC=<%d>",ilRc);
			if (ilRc == RC_SUCCESS)
			{
				GetDataItem(pclAlc2,pcgDataArea,1,',',""," ");
				TrimAll(pclAlc2);
				if (strlen(pclAlc2)==0)
				{
					strncat(pclAlc2," ",1);
				}
				else
				{
					strcpy(pclFlnoSave,pclFlno);
					memset(pclFlno,0x00,32);
					memset(pclFlno,0x20,9);
					strncpy(pclFlno,pclAlc2,2);
					strncpy((char*)&pclFlno[3],pclFltn,strlen(pclFltn));
					strncpy((char*)&pclFlno[8],pclFlns,1);
					TrimAll(pclFlno);
					dbg(DEBUG,"HD: REBUILD FLNO WITH ALC2=<%s> --> <%s>",pclAlc2,pclFlno);
					if ((ilRC_Replace = SearchStringAndReplace(pclData,pclFlnoSave,pclFlno)) != RC_SUCCESS)
					{
						dbg(TRACE,"HD: SearchStringAndReplace() failed for <%s>!",pclFlnoSave);
					}
					else
					{
						dbg(DEBUG,"HD: REPLACED <%s> WITH <%s> IN DATA-LIST",pclFlnoSave,pclFlno);
					}
				}
				dbg(DEBUG,"HD: ALC3=<%s> --- ALC2=<%s>",pclAlc3,pclAlc2);
			}
			else
			{
				dbg(TRACE,"HD: no ALC2 for ALC3=<%s> found! Now using CSGN=<%s>!",pclCsgn);
				memset(pclAlc3,0x00,32);
				memset(pclAlc2,0x00,32);
			}
		}

		*pcgSqlBuf=0x00;
		*pcgDataArea=0x00;
		if (*pclAdid=='D' && strcmp(pclOrg4,pcgHome)==0)
		{
			StrToTime(pclStod,&tlTime,FALSE);		
			tlTime=tlTime - tgDepFrom;
			TimeToStr(pclTimeFrom,tlTime,1);
			StrToTime(pclStod,&tlTime,FALSE);		
			tlTime=tlTime + tgDepTo;
			TimeToStr(pclTimeTo,tlTime,1);

			if (strlen(pclFlno)>0 && ((strlen(pclAlc3)>0 && strlen(pclAlc2)>0) || (strlen(pclAlc3)>0 && strlen(pclAlc2)==0)))
			{
				sprintf(pcgWhere,"WHERE STOD BETWEEN '%s' AND '%s' AND ADID='%s' AND ORG4='%s' AND FLTN='%s' AND FLNS='%s' AND (ALC2='%s' or ALC3='%s') ORDER BY URNO",pclTimeFrom,pclTimeTo,pclAdid,pclOrg4,pclFltn,pclFlns,pclAlc2,pclAlc3);
			}
			else if (strlen(pclCsgn)>0)
			{
				sprintf(pcgWhere,"WHERE STOD BETWEEN '%s' AND '%s' AND CSGN='%s' AND ADID='%s' AND ORG4='%s' ORDER BY URNO"
					,pclTimeFrom,pclTimeTo,pclCsgn,pclAdid,pclOrg4);
			}
			else
			{
				dbg(TRACE,"HD: NO FLNO/CSGN FOUND! CAN'T PROCEED WITH WPPL-TELEX!");	
				return RC_FAIL;
			}
			sprintf(pcgSqlBuf,"SELECT %s FROM AFTTAB %s",pcgSelectFields,pcgWhere);
		}
		else
		{
			if (*pclAdid=='A' && strcmp(pclDes4,pcgHome)==0)
			{
				StrToTime(pclStoa,&tlTime,FALSE);		
				tlTime=tlTime - tgArrFrom;
				TimeToStr(pclTimeFrom,tlTime,1);
				StrToTime(pclStoa,&tlTime,FALSE);		
				tlTime=tlTime + tgArrTo;
				TimeToStr(pclTimeTo,tlTime,1);

				if (strlen(pclFlno)>0 && ((strlen(pclAlc3)>0 && strlen(pclAlc2)>0) || (strlen(pclAlc3)>0 && strlen(pclAlc2)==0)))
				{
					sprintf(pcgWhere,"WHERE STOA BETWEEN '%s' AND '%s' AND ADID='%s' AND DES4='%s' AND FLTN='%s' AND FLNS='%s' AND (ALC2='%s' or ALC3='%s') ORDER BY URNO",pclTimeFrom,pclTimeTo,pclAdid,pclDes4,pclFltn,pclFlns,pclAlc2,pclAlc3);
				}
				else if (strlen(pclCsgn)>0)
				{
					sprintf(pcgWhere,"WHERE STOA BETWEEN '%s' AND '%s' AND CSGN='%s' AND ADID='%s' AND DES4='%s' ORDER BY URNO"
						,pclTimeFrom,pclTimeTo,pclCsgn,pclAdid,pclDes4);
				}
				else
				{
					dbg(TRACE,"HD: NO FLNO/CSGN FOUND! CAN'T PROCEED WITH WPPL-TELEX!");	
					return RC_FAIL;
				}
				sprintf(pcgSqlBuf,"SELECT %s FROM AFTTAB %s",pcgSelectFields,pcgWhere);
			}
			else
			{
				dbg(TRACE,"HD: NO ADID FOUND! CAN'T PROCEED WITH WPPL-TELEX!");	
			}
		}

		if (strlen(pcgSqlBuf) > 0)
		{
			slFkt=START|REL_CURSOR;
			slCursor=0;
			dbg(DEBUG,"HD: SQL: <%s>",pcgSqlBuf);
			ilRc = sql_if(slFkt,&slCursor,pcgSqlBuf,pcgDataArea);
			dbg(DEBUG,"HD: SQL-RC=<%d>",ilRc);
			if (ilRc != RC_SUCCESS)
			{
				if (ilRc != 1)
					GetOraErr(ilRc,pcgErrMsg,pcgSqlBuf,"HandleData");
				else
				{
					dbg(DEBUG,"HD: NO DATA FOUND! NOW INSERTING RECORD INTO AFTTAB!");
					memset(pclResultFields,0x00,DATABLK_SIZE);
					memset(pclResultData,0x00,DATABLK_SIZE);
					if ((ilRc = PrepareData('I',pcgHandlingMask,pclFld,pclData,pcgSelectFields,pcgDataArea,pclResultFields,pclResultData)
										== RC_SUCCESS))
					{
						if (strlen(pclAlc2)>0)
						{
							strcat(pclResultFields,",");
							strcat(pclResultFields,"ALC2");
							strcat(pclResultData,",");
							strcat(pclResultData,pclAlc2);
							dbg(DEBUG,"HD: ADD ALC2=<%s> TO FIELD/DATALIST",pclAlc2);
						}
						if ((ilRc=SendEvent(pcgInsertCmd,pclSel,pclResultFields,pclResultData,NULL,0,igInsertRoute
											,mod_id,PRIORITY_4,"AFTTAB",pcgInsBc,"",NULL,FALSE)) != RC_SUCCESS)
						{
							dbg(TRACE,"HD: AFTTAB-SendEvent(%s) to (%d) failed! ilRc=<%d>",pcgInsertCmd,igInsertRoute,ilRc);
							dbg(TRACE,"HD: WPLTAB-SendEvent(IBT) ignored! Due to RC-error on AFTTAB!");
						}
						else
						{
							/* only works if flight supports IFR command which it currently does NOT do */
							/* flights in WPLTAB may be found using CSGN & STOD,STOA                    */
							#if 0
							*pcgDataArea=0x00;
							sprintf(pcgSqlBuf,"SELECT URNO FROM AFTTAB %s",pcgWhere);
							dbg(DEBUG,"HD: SQL: <%s>",pcgSqlBuf);
							nap(500);
							ilRc = sql_if(slFkt,&slCursor,pcgSqlBuf,pcgDataArea);
							dbg(DEBUG,"HD: SQL-RC=<%d>",ilRc);
							GetDataItem(pclUrno,pcgDataArea,1,0x00,"","");
							TrimAll(pclUrno);
							dbg(TRACE,"HD: AFT-URNO <%s>",pclUrno);
							if (strlen(pclUrno) > 0)
							{
								strcat(pclFld,",");
								strcat(pclFld,"RURN");
								strcat(pclData,",");
								strcat(pclData,pclUrno);
								if ((ilRc=SendEvent("IBT","",pclFld,pclData,NULL,0,1200
													,mod_id,PRIORITY_4,"WPLTAB","","",NULL,FALSE)) != RC_SUCCESS)
								{
									dbg(TRACE,"HD: WPLTAB-SendEvent(IBT) failed! ilRc=<%d>",ilRc);
								}
							}
							else
							{
								dbg(TRACE,"HD: CAN'T CONNECT WPL-RECORD TO AFT-RECORD BECAUSE OF MISS. URNO VALUE!");	
							}
							#endif
						}
					}
					else
					{
						dbg(TRACE,"HD: PrepareData() failed!");	
					}
				}
			} 
			else
			{
					dbg(DEBUG,"HD: FLIGHT FOUND! NOW CHECK DATA FOR UPDATE-PERMISSION!");
        	BuildItemBuffer(pcgDataArea,"",GetNoOfElements(pcgSelectFields,','),",");
					if ((ilRc = PrepareData('U',pcgHandlingMask,pclFld,pclData,pcgSelectFields,pcgDataArea,pclResultFields,pclResultData)
										== RC_SUCCESS))
					{
						GetDataItem(pclUrno,pcgDataArea,1,',',"","");
						sprintf(pclTmpBuf,"WHERE URNO=%s",pclUrno);
						if ((ilRc=SendEvent(pcgUpdateCmd,pclTmpBuf,pclResultFields,pclResultData,NULL,0,igInsertRoute
											,mod_id,PRIORITY_4,"AFTTAB",pcgUpdBc,"",NULL,FALSE)) != RC_SUCCESS)
						{
							dbg(TRACE,"HD: SendEvent() failed!");
						}
						else
						{
							#if 0
							if ((ilRc=SendEvent("URT",pcgWhere,pclFld,pclData,NULL,0,1200
												,mod_id,PRIORITY_4,"WPLTAB","","",NULL,FALSE)) != RC_SUCCESS)
							{
								dbg(TRACE,"HD: WPLTAB-SendEvent(IBT) failed! ilRc=<%d>",ilRc);
							}
							#endif
						}
					}
					else
					{
						dbg(TRACE,"HD: PrepareData() failed!");	
					}
			}
			commit_work();
			close_my_cursor(&slCursor);
		}
	}

	#if 0
  if (strcmp(prlCmdblk->command, "WPPL")==0)
  {
		 /* expecting single line or LF-delimited package of lines for WPPL command */
		 ilCount = (int)GetNoOfElements(pclData,0x0A);
		 if (ilCount > 0)
		 {
			 for (ilCnt=0;ilCnt<ilCount;ilCnt++)
			 {
		 		*pclWRDSelection=0x00;
		 		strcpy(pclWRDSelection,pclSel);
				memset(pclWRDSingleDataLine,0x00,DATABLK_SIZE);
				GetDataItem(pclWRDSingleDataLine,pclData,ilCnt+1,0x0A,"","");
				dbg(TRACE,"WPPL: processing line <%d>/<%d>!",ilCnt+1,ilCount);
				rc=WriteRecordData(prlCmdblk->obj_name,pclWRDSelection,pclFld,pclWRDSingleDataLine);
				if (rc != RC_SUCCESS)
					dbg(TRACE,"WPPL: line <%d>/<%d>=<%s> failed to be processed!",ilCnt+1,ilCount,pclWRDSingleDataLine);
			 }
		 }
		 else if (ilCount==0 && strlen(pclData)>0)
		 {
		 		*pclWRDSelection=0x00;
		 		strcpy(pclWRDSelection,pclSel);
     		rc=WriteRecordData(prlCmdblk->obj_name,pclWRDSelection,pclFld,pclData);
					if (rc != RC_SUCCESS)
						dbg(TRACE,"WPPL: line <%s> failed to be processed!",pclData);
		 }
  }
	else
  {
     dbg(TRACE,"HD: <%s>: This is not a valid command", prlCmdblk->command);
     rc = RC_FAIL;
  } /* end else ifs */
	#endif

  return rc;
} /* HandleData */

static int PrepareData(char cpType,char *pcpHandlingMask,char *pcpFld,char *pcpData, char *pcpSelectFields, char *pcpSelectData
											,char *pcpResultFields,char *pcpResultData)
{
	int ilRc = RC_SUCCESS;
	int ilCount = 0;
	int ilFld = FALSE;
	int ilPosInFields=0,ilCol=0,ilFromStart=0;
	char pclField[2048];
	char pclUfisEq[2048];
	char pclData[2048];
	char pclSelectField[2048];
	char pclSelectDat[2048];

	*pcpResultFields=0x00;
	*pcpResultData=0x00;

	for (ilCount=0;ilCount<strlen(pcpHandlingMask);ilCount++)
	{
		memset(pclSelectField,0x00,2048);
		memset(pclSelectDat,0x00,2048);

		memset(pclField,0x00,2048);
		memset(pclUfisEq,0x00,2048);
		memset(pclData,0x00,2048);

		GetDataItem(pclSelectField,pcpSelectFields,ilCount+1,',',"","");
		TrimAll(pclSelectField);

		GetDataItem(pclField,pcpFld,ilCount+1,',',"","");
		GetDataItem(pclData,pcpData,ilCount+1,',',"","");
		GetDataItem(pclUfisEq,pcgUfisEquiv,ilCount+1,',',"","");
		TrimAll(pclField);
		TrimAll(pclData);
		TrimAll(pclUfisEq);

		if (strlen(pclUfisEq)>0 && (pcpHandlingMask[ilCount]=='K' || pcpHandlingMask[ilCount]=='O'))
		{
			FindItemInList(pcpSelectFields,pclUfisEq,',',&ilPosInFields,&ilCol,&ilFromStart);
			GetDataItem(pclSelectDat,pcpSelectData,ilPosInFields,',',"","");
			TrimAll(pclSelectDat);

			dbg(DEBUG,"PD: %2d.HDL<%c> UFIS-EQ<%s> DB-DAT<%s>\tFLD<%s> RCV-DAT<%s>"
				,ilCount+1,pcpHandlingMask[ilCount],pclUfisEq,pclSelectDat,pclField,pclData);

			ilFld=FALSE;
			if (strcmp(pclSelectDat,pclData)!=0)
			{
				dbg(DEBUG,"PD:   - FLD<%s>=UFIS-EQ<%s>: DB-DAT<%s> RCV-DAT<%s> => <%c>"
					,pclField,pclUfisEq,pclSelectDat,pclData,pcpHandlingMask[ilCount]);

				if (pcpHandlingMask[ilCount]=='K' && strlen(pclSelectDat)==0 && strlen(pclData)>0)
					{ strcat(pcpResultData,pclData); strcat(pcpResultData,","); ilFld=TRUE;}
				if (pcpHandlingMask[ilCount]=='K' && strlen(pclSelectDat)==0 && strlen(pclData)==0)
					{ strcat(pcpResultData," "); strcat(pcpResultData,","); ilFld=TRUE;}
				if (pcpHandlingMask[ilCount]=='K' && strlen(pclSelectDat)>0 && strlen(pclData)==0)
					{/*strcat(pcpResultData,pclSelectDat); strcat(pcpResultData,","); ilFld=TRUE;*/}
				if (pcpHandlingMask[ilCount]=='K' && strlen(pclSelectDat)>0 && strlen(pclData)>0)
					{/*strcat(pcpResultData,pclSelectDat); strcat(pcpResultData,","); ilFld=TRUE;*/}

				if (pcpHandlingMask[ilCount]=='O' && strlen(pclSelectDat)==0 && strlen(pclData)>0)
					{ strcat(pcpResultData,pclData); strcat(pcpResultData,","); ilFld=TRUE;}
				if (pcpHandlingMask[ilCount]=='O' && strlen(pclSelectDat)==0 && strlen(pclData)==0)
					{ strcat(pcpResultData," "); strcat(pcpResultData,","); ilFld=TRUE;}
				if (pcpHandlingMask[ilCount]=='O' && strlen(pclSelectDat)>0 && strlen(pclData)==0)
					{ strcat(pcpResultData," "); strcat(pcpResultData,","); ilFld=TRUE;}
				if (pcpHandlingMask[ilCount]=='O' && strlen(pclSelectDat)>0 && strlen(pclData)>0)
					{ strcat(pcpResultData,pclData); strcat(pcpResultData,","); ilFld=TRUE;}

				if (ilFld==TRUE)
				{
					strcat(pcpResultFields,pclUfisEq);
					strcat(pcpResultFields,",");
				}
			}

		}
		else if (pcpHandlingMask[ilCount]=='-' && strlen(pclUfisEq)>0)
		{
			dbg(TRACE,"PD: UFIS_EQUIV-FIELD MUST HAVE EITHER 'K' or 'O' VALUE FOR FIELD_POLICY! PLEASE CORRECT CONFIG.!");	
			return RC_FAIL;
		}
	}
	pcpResultFields[strlen(pcpResultFields)-1]=0x00;
	dbg(DEBUG,"PD: RESULT-FLD<%s>",pcpResultFields);
	pcpResultData[strlen(pcpResultData)-1]=0x00;
	dbg(DEBUG,"PD: RESULT-DAT<%s>",pcpResultData);
	if (GetNoOfElements(pcpResultFields,',') != GetNoOfElements(pcpResultData,',') ||
			(strlen(pcpResultFields)==0 || strlen(pcpResultData)==0))
		ilRc = RC_FAIL;
	return ilRc;
}

/* *********************************
 * Function:		reset
 * Parameter: 	void
 * Return: 			int 
 * Description: Reset return code
 * ********************************/
static int reset() {
	terminate();
} /* end of reset */

/* ***************************************
 * Function: 		terminate
 * Parameter:		void
 * Return: 			void
 * Description:	Terminates 
* ****************************************/
static void terminate() {
	(void)UnsetSignals();
	free(prgItem);
	prgItem = NULL;
	logoff();  /* disconnect from oracle */
	dbg(TRACE,"terminate: Now Leaving.....");
	exit(0);
} /* end of terminate */

/* ***************************************
 * Function: 		handle_sig
 * Parameter:		IN: int ipSig Process signal
 * Return: 			void
 * Description:	Handles signals SIGTERM and 
 *							SIGALRM. 
 ******************************************/
static void handle_sig(ipSig) {
	switch (ipSig) {
	case SIGTERM:
		dbg(TRACE,"handle_sig: Received Signal <%d> (SIGTERM). Terminating now...",ipSig);
		break;
	case SIGALRM:
		dbg(TRACE,"handle_sig: Received Signal<%d>(SIGALRM)",ipSig);
		break;
	default:
		dbg(TRACE,"handle_sig: Received Signal<%d>",ipSig);
		terminate();
		break;
	} /* end switch; */
} /* end of handle_sig */

/* ***************************************
 * Function: 		handle_err
 * Parameter:		IN: int ipErr  Error code
 * Return: 			void
 * Description:	Processes error codes. 
 ******************************************* */
static void handle_err(int ipErr) {	
	return;
} /* end of handle_err */

/* **************************************
 * Function: 		handle_qerr
 * Parameter:		IN: int ipErr  Error code
 * Return: 			void
 * Description:	Handles queueing errors 
 *************************************** */
static void handle_qerr(int ipErr) {	
	switch(ipErr) {
	case  QUE_E_FUNC:			/* Unknown function */
					break;
	case QUE_E_MEMORY:		/* Malloc reports no memory */
					break;
	case QUE_E_SEND:			/* Error using msgsnd */
					break;
	case QUE_E_GET:				/* Error using msgrcv */
					break;
	case QUE_E_EXISTS:		/* Route/Queue exists */
					break;
	case QUE_E_NOFIND:		/* Not found (ex. route ) */
					break;
	case QUE_E_ACKUNEX:		/* Unexpected ACK received */
					break;
	case QUE_E_STATUS:		/* Unknown queue status */
					break;
	case QUE_E_INACTIVE:	/* Queue is inactive */
					break;
	case QUE_E_MISACK:		/* Missing ACK */
					break;
	case QUE_E_NOQUEUES:	/* The queues don't exist */
					break;
	case QUE_E_RESP:			/* No response on CREATE */
					break;
	case QUE_E_FULL:			/* Table full */
					break;
	case QUE_E_NOMSG:			/* No message on queue */
					break;
	case QUE_E_INVORG:		/* Mod id by que call is 0 */
					break;
	case QUE_E_NOINIT:		/* Queues is not initialized*/
					break;
	default:							/* Unknown queue error */
					break;
	} /* end switch */
	return;
} /* end of handle_qerr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		/* get next item */
		ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,0,(char*)&prgItem);

		/* set event pointer */
		prgEvent = (EVENT*)prgItem->text;

		/* check ret code */
		if( ilRC == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRC != RC_SUCCESS ) 
			{
				handle_qerr(ilRC);
			} 
		
			switch (prgEvent->command)
			{
				case HSB_STANDBY	:
					ctrl_sta = prgEvent->command;
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					break;	
		
				case HSB_COMING_UP	:
					ctrl_sta = prgEvent->command;
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					break;	
		
				case HSB_ACTIVE	:
					ctrl_sta = prgEvent->command;
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					ilBreakOut = TRUE;
					break;

				case HSB_STANDALONE	:
					ctrl_sta = prgEvent->command;
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					ResetDBCounter();
					ilBreakOut = TRUE;
					break;	

				case HSB_ACT_TO_SBY	:
					ctrl_sta = prgEvent->command;
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					break;	
		
				case HSB_DOWN	:
					ctrl_sta = prgEvent->command;
					terminate();
					break;	
		
				case SHUTDOWN	:
					terminate();
					break;
							
				case RESET		:
					ilRC = reset();
					break;

				case EVENT_DATA	:
					dbg(DEBUG,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;

				case REMOTE_DB:
					HandleRemoteDB(prgEvent);
					break;
						
				case TRACE_ON :
					dbg_handle_debug(prgEvent->command);
					break;

				case TRACE_OFF :
					dbg_handle_debug(prgEvent->command);
					break;

				default			:
					dbg(DEBUG,"HandleQueues: unknown event");
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;
			} /* end switch */
			
			/* Handle error conditions */
			if(ilRC != RC_SUCCESS)
			{
				handle_err(ilRC);
			} /* end if */
		} 
		else 
		{
			handle_qerr(ilRC);
		} 
	} while (ilBreakOut == FALSE);
} 
/* *******************************************************/
/* *******************************************************/
static void CheckPerformance(int ipFlag)
{
  if (ipFlag == TRUE)
  {
     dbg(TRACE,"---- START");
     tgBeginStamp = time(0L);
  } /* end if */
  else
  {
     tgEndStamp = time(0L);
     tgStampDiff = tgEndStamp - tgBeginStamp;
     dbg(TRACE,"---- READY (%d SEC)",tgStampDiff);
     if (tgStampDiff > 10)
     {
        dbg(TRACE,"PLEASE CHECK PERFORMANCE (%d SEC)",tgStampDiff);
     } /* end if */
   } /* end else */

   return;
} /* end CheckPerformance */

/* *******************************************************/
/* *******************************************************/
#if 0
static int WriteRecordData(char *pcpTbl, char *pcpSel,char *pcpFld,char *pcpDat)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilGetRcShm = RC_SUCCESS;
  int ilError = TRUE;
  int ilItmCnt = 0;
  int ilItm = 0;
  int ilFld = 0;
  int ilSelPos = 0;
  int ilDatPos = 0;
  short slCursor = 0;
  short slFkt = START;
  char pclActCmd[8];
  char pclFldNam[8];
  char pclFldVal[8];
  char pclFldDat[2048];
  char pclFldLst[4096];
  char pclNewUrno[64];
  char *pclPtr = NULL;
  char *pclSelPtr = NULL;

	/* JWE: 21.01.2003: added so that also WRD commands update SHM */
	char clCmdId = 0x00;
	char pclTmpUrno[15];
	int  ilTmpUrno=0;
	char pclShmUpdateSel[iMAX_BUF_SIZE];
	int ilLen = 0;
	int rc = RC_SUCCESS;
	/* JWE: 21.01.2003: END */

	/* JWE: 19.02.2003: added function to process SQL-like selections */
  int ilCount = 0;
  int ilTokenCount = 0;
  int ilCnt = 0;
  int ilCnt2 = 0;
  int ilRC_Replace = 0;
  int ilUseRealSqlSelect = FALSE;
	char pclSubs[12];
	char *pclSubsPointer;
	char *pclSubsCountPointer;
	char pclData[iMAX_BUF_SIZE];
	char pclField[iMIN];
	/* JWE: 19.02.2003: END */

	memset(pclShmUpdateSel,0x00,iMAX_BUF_SIZE);
	
	if (strlen(pcpDat)<=0)
		return RC_FAIL;

	/* JWE: 05.02.2003: now on WRD command also LSTU;CDAT;USEC,USEU*/
	/*                  are checked and inserted/removed */
	/* first we assume an update to happen and check if we need to add */
	/* some generic table fields like LSTU,CDAT,etc.                   */
  dbg(TRACE,"WRD: ---------------- <START> ---------------");
  dbg(TRACE,"WRD: --- NOW TRYING TO UPDATE GIVEN DATA! ---");
	pcgDataArea[0] = 0x00;
	pcgFldLst[0] = 0x00;
	pcgDatLst[0] = 0x00;
	strcpy(pcgFldLst,pcpFld);
	strcpy(pcgDatLst,pcpDat);
	if (igCurTabIdx >= 0)
	{
		rc = CheckCDAT('U',prgTabDescr[igCurTabIdx].cgCdatType,
									 pcgFldLst,pcgDataArea,pcgDatLst);
		rc = CheckUSEC('U',prgTabDescr[igCurTabIdx].cgUsecType,
									 pcgFldLst,pcgDataArea,pcgDatLst);
		rc = CheckLSTU('U',prgTabDescr[igCurTabIdx].cgLstuType,
									 pcgFldLst,pcgDataArea,pcgDatLst);
		rc = CheckUSEU('U',prgTabDescr[igCurTabIdx].cgUseuType,
									 pcgFldLst,pcgDataArea,pcgDatLst);
	}

	/**********************************************/
	/* Now prepare selection from new-style input */
	/**********************************************/
	if (strstr(pcpSel,"#FC:") != NULL)
	{
		dbg(DEBUG,"WRD: SUBST.-SEL=<%s>",pcpSel);
		ilUseRealSqlSelect = TRUE;
  	ilCount = GetNoOfElements(pcpFld,',');
		for (ilCnt=0; ilCnt<ilCount;ilCnt++)
		{
			ilError=FALSE;
			*pclSubs=0x00;
			memset(pclField,0x00,iMIN);
			get_real_item(pclField,pcpFld,ilCnt+1);
			memset(pclData,0x00,iMAX_BUF_SIZE);
			get_real_item(pclData,pcpDat,ilCnt+1);
			sprintf(pclSubs,"#FC:%s#",pclField);
			/*dbg(DEBUG,"WRD: check <%s> token.",pclSubs);*/
			pclSubsPointer=NULL;
			while ((pclSubsPointer=strstr(pcpSel,pclSubs))!=NULL)
			{
				if ((ilRC_Replace = SearchStringAndReplace(pcpSel,pclSubs,pclData)) != RC_SUCCESS)
				{
					dbg(TRACE,"WRD: SearchStringAndReplace() failed for <%s>!",pclSubs);
					ilError=TRUE;
				}
				else
				{
					ilError=FALSE;
					dbg(DEBUG,"WRD: Replaced <%s> with <%s> for <%s>",pclSubs,pclData,pclField);
				}
			}
		}
		if (ilError == TRUE)
		{
			dbg(TRACE,"WRD: failed to replace at least one substitute! Can't continue!");
  		strcpy(pcgErrMsg,"ERROR: substitute of pattern inside selection failed");
		}
		else
		{
			dbg(DEBUG,"WRD: SELECTION :<%s>",pcpSel);
			pcgDataArea[0] = 0x00;
			sprintf(pcgDataArea,"%s",pcgDatLst);
			dbg(DEBUG,"WRD: DATA-AREA :<%s>",pcgDataArea);
		}
	}

	/**************************/
	/* Now try to update data */
	/**************************/
  if (ilError == FALSE)
  {
     strcpy(pclActCmd,"URT");
		 dbg(DEBUG,"WRD: FIELDLIST :<%s>",pcgFldLst);
		 if (ilUseRealSqlSelect == TRUE)
		 {
     	 BuildSqlFldStrg(pcgRcvFldLst,pcgFldLst,FOR_UPDATE);
			 CheckWhereClause(TRUE, pcpSel, FALSE, TRUE, "\0");
    	 sprintf(pcgSqlBuf,"UPDATE %s SET %s %s ",pcpTbl,pcgRcvFldLst,pcpSel);
		 }
		 else
		 {
     	 strcpy(pclFldLst,pcgFldLst);
     	 BuildSqlFldStrg(pcgRcvFldLst,pcgFldLst,FOR_UPDATE);
			 CheckWhereClause(TRUE, pcgRcvSelKey, FALSE, TRUE, "\0");
			 CheckWhereClause(TRUE, pclShmUpdateSel, FALSE, TRUE, "\0");
			sprintf(pcgSqlBuf,"UPDATE %s SET %s %s ",pcpTbl,pcgRcvFldLst,pcgRcvSelKey);
		 }
		 dbg(DEBUG,"WRD: SQL :<%s>",pcgSqlBuf);
     dbg(DEBUG,"WRD: DATA:<%s>",pcgDataArea);
     strcpy(pcgRcvDatBlk,pcgDatLst);
     delton(pcgDataArea);
     slCursor = 0;
     slFkt=START;
     ilGetRc = sql_if(slFkt,&slCursor,pcgSqlBuf,pcgDataArea);
     if (ilGetRc != RC_SUCCESS)
     {
        GetOraErr(ilGetRc,pcgErrMsg,pcgSqlBuf,"WriteRecodData");
     } /* end if */
		 else
		 {
			  commit_work();
			  close_my_cursor(&slCursor);

				dbg(DEBUG,"WRD: Update via WRD-command successfull! Now updating SHM!");
				/* gettting the corresponding URNO to update the SHM */
				*pcgSqlBuf=0x00;
				*pcgDataArea=0x00;
				if (ilUseRealSqlSelect == TRUE)
				{
     			sprintf(pcgSqlBuf,"SELECT URNO FROM %s %s ",pcpTbl,pcpSel);
				}
		 		else
		 		{
     			sprintf(pcgSqlBuf,"SELECT URNO FROM %s %s ",pcpTbl,pclShmUpdateSel);
				}
				dbg(DEBUG,"WRD: SQL <%s>",pcgSqlBuf);
				ilGetRcShm = sql_if(slFkt,&slCursor,pcgSqlBuf,pcgDataArea);
				if (ilGetRcShm != RC_SUCCESS)
				{
					GetOraErr(ilGetRcShm,pcgErrMsg,pcgSqlBuf,"WriteRecodData");
					dbg(TRACE,"WRD: unable to update SHM via WRD-command! Inconsistent SHM for table <%s>!",pcpTbl);
				} /* end if */
				else
				{
        	(void) BuildItemBuffer(pcgDataArea,"",1,",");
					*pclTmpUrno=0x00;
        	ilLen = get_real_item(pclTmpUrno,pcgDataArea,1);
          tool_filter_spaces(pclTmpUrno);
					clCmdId='U';
					dbg(DEBUG,"WRD: Calling SYSLIB UPDATE: TAB <%s> FKT <%c> URNO <%s>",pcpTbl,clCmdId,pclTmpUrno);
          tool_filter_spaces(pclTmpUrno);
					ilTmpUrno=atoi(pclTmpUrno);
					CheckUrnoValue(pcpTbl,pclTmpUrno,ilTmpUrno);
					rc=syslibUpdateDbData(clCmdId,pcpTbl,"URNO",pclTmpUrno);
					if (rc != RC_SUCCESS)
					{
						dbg(TRACE,"WRD: Error returned from syslibUpdateDbData with rc = <%d>",rc);
						rc=syslibReloadDbData(pcpTbl);
						dbg(TRACE,"WRD: Reloading Table %s rc = <%d>", pcpTbl,rc);
						rc = RC_SUCCESS; /* bad thing, but what shall we do? */
					} /* end if */
				}
		 }
     commit_work();
     close_my_cursor(&slCursor);
		 /************************************************/
		 /* Now inserting data because update has failed */
		 /************************************************/
     if (ilGetRc != RC_SUCCESS)
     {
        if (ilGetRc == SQL_NOTFOUND)
        {
          dbg(TRACE,"WRD: ---- DATA FOR SQL-UPDATE NOT FOUND. SO INSERT DATA! ----");
					if (igCurTabIdx >= 0)
					{
						rc = CheckCDAT('I',prgTabDescr[igCurTabIdx].cgCdatType,
													 pcgFldLst,pcgDataArea,pcgDatLst);
						rc = CheckUSEC('I',prgTabDescr[igCurTabIdx].cgUsecType,
													 pcgFldLst,pcgDataArea,pcgDatLst);
						rc = CheckLSTU('I',prgTabDescr[igCurTabIdx].cgLstuType,
													 pcgFldLst,pcgDataArea,pcgDatLst);
						rc = CheckUSEU('I',prgTabDescr[igCurTabIdx].cgUseuType,
													 pcgFldLst,pcgDataArea,pcgDatLst);
					 }
           strcpy(pcgDataArea,pcgDatLst);
           if (igAddHopo == TRUE)
           {
              if (strstr(pcgFldLst,"HOPO") == NULL)
              {
                 strcat(pcgFldLst,",HOPO");
                 strcat(pcgDataArea,",");
                 strcat(pcgDataArea,pcgH3LC);
              } /* end if */
           } /* end if */
           if (strstr(pcgFldLst,"URNO") == NULL)
           {
              strcpy(pclActCmd,"IRT");
              ilGetRc = GetNextValues(pclNewUrno, 1);
              strcat(pcgFldLst,",URNO");
              strcat(pcgDataArea,",");
              strcat(pcgDataArea,pclNewUrno);
           } /* end if */
           else
           {
              strcpy(pclActCmd,"IRT");
           } /* end else */

           BuildSqlFldStrg(pcgRcvFldLst,pcgFldLst,FOR_INSERT);
           sprintf(pcgSqlBuf,"INSERT INTO %s %s", pcpTbl,pcgRcvFldLst);
           dbg(DEBUG,"WRD: SQL :<%s>",pcgSqlBuf);
           dbg(DEBUG,"WRD: DATA:<%s>",pcgDataArea);
           strcpy(pcgRcvDatBlk,pcgDataArea);
           delton(pcgDataArea);
           slCursor = 0;
           slFkt=START;
           ilGetRc = sql_if(slFkt,&slCursor,pcgSqlBuf,pcgDataArea);
           if (ilGetRc != RC_SUCCESS)
           {
              GetOraErr(ilGetRc,pcgDataArea,pcgSqlBuf,"WriteRecordData");
           } /* end if */
					else
					{
						*pclTmpUrno=0x00;
						strcpy(pclTmpUrno,pclNewUrno);
						tool_filter_spaces(pclTmpUrno);
						clCmdId='I';
						dbg(DEBUG,"WRD: Calling SYSLIB UPDATE: TAB <%s> FKT <%c> URNO <%s>",pcpTbl,clCmdId,pclTmpUrno);
						tool_filter_spaces(pclTmpUrno);
						ilTmpUrno=atoi(pclTmpUrno);
						CheckUrnoValue(pcpTbl,pclTmpUrno,ilTmpUrno);
						rc=syslibUpdateDbData(clCmdId,pcpTbl,"URNO",pclTmpUrno);
						if (rc != RC_SUCCESS)
						{
							dbg(TRACE,"WRD: Error returned from syslibUpdateDbData with rc = <%d>",rc);
							rc=syslibReloadDbData(pcpTbl);
							dbg(TRACE,"WRD: Reloading Table %s rc = <%d>",
									pcpTbl,rc);
							rc = RC_SUCCESS; /* bad thing, but what shall we do? */
						} /* end if */
					}
           commit_work();
           close_my_cursor(&slCursor);
        } /* end if */
     } /* end if */
		 /******************************************/
		 /* Now broadcasting, send to action , etc.*/
		 /******************************************/
     if (ilGetRc == RC_SUCCESS)
     {
			 if (ilUseRealSqlSelect == TRUE)
			 {
				 dbg(TRACE,"WRD: RESULT SEL. <%s>",pcpSel);
			 }
			 else
			 {
				 dbg(TRACE,"WRD: RESULT SEL. <%s>",pcgRcvSelKey);
			 }
			 dbg(TRACE,"WRD: RESULT FLD. <%s>",pcgFldLst);
			 dbg(TRACE,"WRD: RESULT DAT. <%s>",pcgRcvDatBlk);
			 dbg(TRACE,"WRD: RESULT URNO <%s>",pclTmpUrno);
			 dbg(DEBUG,"WRD: sending info to <BCHDL,ACTION,LOGHDL,ORIGINATOR>");
			 dbg(DEBUG,"WRD: OLD-DATA forward to <ACTION> IS NOT SUPPORTED!");
       strcpy(pcgActDatBlk,pclTmpUrno);
       if (strcmp(pclActCmd,"URT") == 0)
       {
         sprintf(pcgRcvSelKey,"WHERE URNO=%s",pclTmpUrno);
       } /* end if */
       else
       {
          strcpy(pcgRcvSelKey,pclTmpUrno);
       } /* end else */
       delton(pcgRcvDatBlk);
       BuildItemBuffer(pcgRcvDatBlk,pcgFldLst,0,",");
       if (gl_no_ack != TRUE)
       {
           (void) tools_send_info_flag_perf(que_out,0, pcgDestName, "", pcgRecvName,
                                            "", "", pcgTwStart, pcgTwEnd,
                                            pclActCmd,pcpTbl,pcgRcvSelKey,pcgFldLst,
                                            pcgRcvDatBlk,0);
        } /* end if */
        (void) tools_send_info_flag_perf(igToBcHdl,0, pcgDestName, "", pcgRecvName,
                                         "", "", pcgTwStart, pcgTwEnd,
                                         pclActCmd,pcpTbl,pcgRcvSelKey,pcgFldLst,
                                         pcgRcvDatBlk,0);
				pcgOldData[0]=0x00;
        /* Sending to Action (not on mod_id+1) */
        (void) ReleaseActionInfo("7400",pcpTbl,pclActCmd,
									 pcgActDatBlk,pcgRcvSelKey,pcgFldLst,
                                 pcgRcvDatBlk,pcgOldData);

        /* Sending to LOGHDL,if it is existing */
				if (atoi(pcgOutRoute) > 0)
				{
        	(void) ReleaseActionInfo(pcgOutRoute,pcpTbl,pclActCmd,
									 pcgActDatBlk,pcgRcvSelKey,pcgFldLst,
									 pcgRcvDatBlk,pcgOldData);
			  }
				ilError=FALSE;
     } /* end if */
     else
     {
        dbg(TRACE,"WRD: ORACLE ERROR !!");
        ilError = TRUE;
     } /* end else */
  } /* end if */

  if (ilError == TRUE)
  {
     dbg(TRACE,"WRD: ERR MSG <%s>",pcgErrMsg);
     if (gl_no_ack != TRUE)
     {
        tools_send_sql_rc_perf(que_out,"WRD","", "","",pcgErrMsg,RC_FAIL);
     } /* end if */
		 ilRC = RC_FAIL;
  } /* end if */
  dbg(TRACE,"WRD: ---------------- <END> RC=<%d> ---------",ilRC);

  return ilRC;
} /* end WriteRecordData */
#endif
/* *******************************************************/
/* *******************************************************/
static int ReadCfg(char *pcpFile, char *pcpVar, char *pcpSection,
                   char *pcpEntry, char *pcpDefVar)
{
  int ilRC = RC_SUCCESS;

  ilRC = iGetConfigRow(pcpFile,pcpSection,pcpEntry,CFG_STRING,pcpVar);
  if (ilRC != RC_SUCCESS || strlen(pcpVar) <= 0)
  {
     strcpy(pcpVar,pcpDefVar);
     ilRC = RC_FAIL;
		dbg(TRACE,"ReadCfg: DEF set <%s> = <%s>",pcpEntry,pcpVar);
  } /* end if */
	else
	{
		dbg(TRACE,"ReadCfg: CFG set <%s> = <%s>",pcpEntry,pcpVar);
	}

  return ilRC;
} /* end ReadCfg */
/*******************************************************/
/*******************************************************/
static void GetLogFileMode(int *ipModeLevel, char *pcpLine, char *pcpDefault)
{
  int ilRC = RC_SUCCESS;
  char pclTmp[32];

  ReadCfg(pcgCfgFile,pclTmp,"MAIN",pcpLine,pcpDefault);
  if (strcmp(pclTmp,"TRACE")==0)
  {
     *ipModeLevel = TRACE;
  }
  else if(strcmp(pclTmp,"DEBUG")==0)
  {
     *ipModeLevel = DEBUG;
  } /* end else if */
  else
  {
     *ipModeLevel = 0;
  } /* end else */

  return;
} /* end GetLogFileMode */

static int GetOraErr(int ipErrCode, char *pcpResultBuffer, char *pcpSqlStatement,
                     char *pcpRoutine)
{
  int ilRC = RC_SUCCESS;
  char *pclTmpPtr;
  int ilDebugLevel;

  strcpy(pcpResultBuffer,"");
  ilRC = get_ora_err(ipErrCode,pcpResultBuffer);
  pclTmpPtr = strstr(pcpResultBuffer,"\r");
  if (pclTmpPtr == NULL)
  {
     pclTmpPtr = strstr(pcpResultBuffer,"\n");
  }
  if (pclTmpPtr != NULL)
  {
     *pclTmpPtr = '\0';
  }
  TrimAll(pcpResultBuffer);
  ilDebugLevel = debug_level;
  debug_level = TRACE;
  dbg(TRACE,"GetOraErr: <%d>=<%s>",ipErrCode,pcpResultBuffer);
  dbg(TRACE,"GetOraErr: Sql-Statement:<%s>",pcpSqlStatement);
  debug_level = ilDebugLevel;

  return ilRC;
} /* End of GetOraErr */

/* ******************************************************************** */
/* The TrimAll() routine                                            */
/* ******************************************************************** */
static void TrimAll(char *pcpBuffer)
{
	const char *p = pcpBuffer;
	int i = 0;

	for (i = strlen(pcpBuffer); i > 0 && isspace(pcpBuffer[i-1]); i--);
			pcpBuffer[i] = '\0';
	for ( *p ; isspace(*p); p++);
	while ((*pcpBuffer++ = *p++) != '\0');
}
/* ******************************************************************** */
/* The TimeToStr() routine																	*/
/* ******************************************************************** */
static void TimeToStr(char *pcpTime,time_t lpTime,int ipType)
{
	int ilRc = 0;
	char pclTime[20];
	struct tm *_tm;
	struct tm rlTm;

	*pclTime = 0x00;
	*pcpTime = 0x00;
	if (lpTime == 0)
	{
		lpTime = time(NULL);
	}
	_tm = (struct tm *) localtime(&lpTime);
	rlTm = *_tm;
	rlTm.tm_isdst=-1;
	switch(ipType)
	{
		case 0: /* Returns "now" in CEDA-format YYYYMMDDHHMMSS */
			/* Unusal format because of sccs !! */
			strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTm);
			/*dbg (DEBUG,"TimeToStr : return <%s>",pcpTime);*/
			break;
		case 1:/* Returns lpTime in CEDA-format YYYYMMDDHHMMSS */
			/* Unusal format because of sccs !! */
			strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTm);
			dbg(DEBUG,"TimeToStr: return <%s>",pcpTime);
			break;
		default:
			dbg(TRACE,"TimeToStr : CEDA-ERROR: unknown time-type received!");
			break;
	}
}
/* ******************************************************************** */
/* The StrToTime() routine																	*/
/* Format of Date has to be CEDA-Format - 14 Byte													*/
/* ******************************************************************** */
static int StrToTime(char *pcpTime,time_t *plpTime,int ipConvert)
{
	int ilUtcLocalDiff;
	struct tm *_tm,_tmTime,_tmUtc;
	time_t tltime,tltime1,tltime2;
	char  _tmpc[6];

	if (strlen(pcpTime) < 12 )
	{
		*plpTime = time(0L);
		return RC_FAIL;
	} 

	tltime = time(0L);
	_tm = (struct tm *)gmtime(&tltime);
	memcpy(&_tmTime,_tm,sizeof(struct tm));
	/* filling tm struct */
	_tmpc[2] = '\0';
	_tmTime.tm_sec = 0;
	strncpy(_tmpc,pcpTime+10,2);
	_tmTime.tm_min = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+8,2);
	_tmTime.tm_hour = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+6,2);
	_tmTime.tm_mday = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+4,2);
	_tmTime.tm_mon = atoi(_tmpc)-1;
	strncpy(_tmpc,pcpTime,4);
	_tmpc[4] = '\0';
	_tmTime.tm_year = atoi(_tmpc)-1900;
	_tmTime.tm_wday = 0;
	_tmTime.tm_yday = 0;
	if (ipConvert==FALSE)
	{
		_tmTime.tm_isdst = -1;
	}
	else if (ipConvert==TRUE)
	{
		_tmTime.tm_isdst = 0;
	}
	tltime1 = mktime(&_tmTime);
	if (tltime != (time_t) -1)
	{
		dbg(DEBUG,"StrToTime: IN:<%s> => OUT:<%02d.%02d.%04d %02d:%02d> => SEC<%ld>"
			,pcpTime,_tmTime.tm_mday,_tmTime.tm_mon+1,_tmTime.tm_year+1900,_tmTime.tm_hour,_tmTime.tm_min,tltime1);
			*plpTime = tltime1;
			return RC_SUCCESS;
	}
	*plpTime = time(NULL);
	return RC_FAIL;
}
/* **************************************************************** */
/* The SendEvent routine                                         */
/* prepares an internal CEDA-event                                  */
/* **************************************************************** */
static int SendEvent(char *pcpCmd,char *pcpSelection,char *pcpFields, char *pcpData,
											char *pcpAddStruct, int ipAddLen,int ipModIdSend,int ipModIdRecv,
											int ipPriority,char *pcpTable,char *pcpType,char *pcpFile,char *pcpResult,int ipWaitForAck)
{
	int     ilRc        		= RC_FAIL;
	int     ilLen        		= 0;
  EVENT   *prlOutEvent    = NULL;
  BC_HEAD *prlOutBCHead   = NULL;
 	BC_HEAD *prlInBchd			= NULL;		/* Broadcast header		*/
  CMDBLK  *prlOutCmdblk   = NULL;

	if (ipModIdSend == 0)
	{
		dbg(TRACE,"SE: Can't send event to ModID=<0> !");
	}
	else
	{
		/* size-calculation for prlOutEvent */
		ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) +
						strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + ipAddLen + 10;
		
		/*dbg(DEBUG,"Total-Len = %d+%d+%d+%d+%d+%d+%d+%d = %d",sizeof(EVENT),sizeof(BC_HEAD),sizeof(CMDBLK),strlen(pcpSelection),strlen(pcpFields),strlen(pcpData),ipAddLen,10,ilLen);*/

		/* memory for prlOutEvent */
		if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
		{
			dbg(TRACE,"SE: cannot malloc <%d>-bytes for outgoing event!",ilLen);
			prlOutEvent = NULL;
		}
		else
		{
			/* clear whole outgoing event */
			memset((void*)prlOutEvent, 0x00, ilLen);

			/* set event structure... */
			prlOutEvent->type	     		= SYS_EVENT;
			prlOutEvent->command   		= EVENT_DATA;

			/*if (ipModIdRecv == igSecondQueue)*/
			if (ipModIdRecv == mod_id)
			{
				/*prlOutEvent->originator  = (short)igSecondQueue;*/
				prlOutEvent->originator  = (short)mod_id;
			}
			else
			{
				prlOutEvent->originator  = (short)mod_id;
			}
			prlOutEvent->retry_count  = 0;
			prlOutEvent->data_offset  = sizeof(EVENT);
			prlOutEvent->data_length  = ilLen - sizeof(EVENT); 

			/* BC_HEAD-Structure... */
			prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
			prlOutBCHead->rc = (short)RC_SUCCESS;
			strncpy(prlOutBCHead->dest_name,mod_name,10);
			strncpy(prlOutBCHead->recv_name, "WPPL",10);

			/* Cmdblk-Structure... */
			prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
			strcpy(prlOutCmdblk->command,pcpCmd);
			strcpy(prlOutCmdblk->obj_name,pcpTable);
			strcat(prlOutCmdblk->obj_name,pcgTblExt);
			
			/* setting tw_start entries */
			sprintf(prlOutCmdblk->tw_start,"%s",pcpType);
			
			/* setting tw_end entries */
			sprintf(prlOutCmdblk->tw_end,"%s,%s,%s",pcgH3LC,pcgTblExt,mod_name);
			
			/* setting selection inside event */
			strcpy(prlOutCmdblk->data,pcpSelection);

			/* setting field-list inside event */
			strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1,pcpFields);

			/* setting data-list inside event */
			strcpy((prlOutCmdblk->data +
						(strlen(pcpSelection)+1) +
						(strlen(pcpFields)+1)),pcpData);

			if (pcpAddStruct != NULL)
			{
				memcpy((prlOutCmdblk->data +
							(strlen(pcpSelection)+1) +
							(strlen(pcpFields)+1)) + 
							(strlen(pcpData)+1),pcpAddStruct,ipAddLen);
			}

			/*DebugPrintEvent(DEBUG,prlOutEvent);*/ 
			/*snapit((char*)prlOutEvent,ilLen,outp);*/

			dbg(TRACE,"SE: <%d> --<%s>--> <%d>",prlOutEvent->originator,pcpCmd,ipModIdSend);
			dbg(TRACE,"SE: SEL <%s>",pcpSelection);
			dbg(TRACE,"SE: FLD <%s>",pcpFields);
			dbg(TRACE,"SE: DAT <%s>",pcpData);

			if ((ilRc = que(QUE_PUT,ipModIdSend,ipModIdRecv,ipPriority,ilLen,(char*)prlOutEvent))
				!= RC_SUCCESS)
			{
				dbg(TRACE,"SE: QUE_PUT to <%d> returns: <%d>",ipModIdSend,ilRc);
				handle_qerr(ilRc);
			}
			/* free memory */
			free((void*)prlOutEvent); 

			if (ilRc==RC_SUCCESS && ipWaitForAck==TRUE)
			{
				dbg(DEBUG,"SE: LISTEN on <%d> for ACK.",mod_id);
				do	
				{
					ilRc = que(QUE_GETBIG,0,mod_id,ipPriority,igItemLen,(char *)&prgItem);
					/* depending on the size of the received item  */
					/* a realloc could be made by the que function */
					/* so do never forget to set event pointer !!! */
					prgEvent = (EVENT *) prgItem->text;	
					if( ilRc == RC_SUCCESS )
					{
						ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
						if( ilRc != RC_SUCCESS ) 
						{
							/* handle que_ack error */
							handle_qerr(ilRc);
						} /* fi */
					
						switch(prgEvent->command)
						{
							case	EVENT_DATA	:
								if((ctrl_sta == HSB_STANDALONE) ||
									(ctrl_sta == HSB_ACTIVE) ||
									(ctrl_sta == HSB_ACT_TO_SBY))
								{
 									prlInBchd = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
									ilRc = prlInBchd->rc;
									dbg(DEBUG,"SE: EVENT-RC=<%d>",ilRc);
								}
								else
								{
									dbg(TRACE,"SE: wrong HSB-status <%d>",ctrl_sta);
									DebugPrintItem(TRACE,prgItem);
									DebugPrintEvent(TRACE,prgEvent);
								}/* end of if */
								break;
							default	:
								dbg(TRACE,"SE: unknown/invalid event for Ack! Ignoring!");
								DebugPrintItem(TRACE,prgItem);
								DebugPrintEvent(TRACE,prgEvent);
								break;
						}
					} 
					else
					{
						/* Handle queuing errors */
						handle_qerr(ilRc);
					} /* end else */
				}while (ilRc != RC_SUCCESS);
			}
			else
			{
				dbg(DEBUG,"SE: NOT LISTEN on <%d> for ACK.",mod_id);
			}
		}
	}
	return ilRc;
}
