#!/usr/bin/bash
######################################################################
# SCRIPT: wppl_data_to_ufis.sh                                       #
# PURPOSE: conversion from WPPL data files to internal UFIS format   #
#          and forwarding of that data to wpplhdl-process            #
#          for processing the records                                #
# SCHEDULE: run via CEDA-crontab twice a day                         #
# AUTHOR: ABB AIRPORT TECHNOLOGIES GMBH / J.WEERTS                   #
# DATE: 24th April 2003 - first version                              #
#                                                                    #
# NOTE: No part of this script may be changed/reproduced or          #
#       copied without written permission of ABB Airport Tech-       #
#       nologies GmbH                                                #
#                                                                    #
# (c) ABB AIRPORT TECHNOLOGIES GMBH - 2002-2003                      #
######################################################################
. /ceda/etc/UfisEnv

ECHO="echo -e"
FILE0=/ceda/exco/WPPL/wppl.dat

# now check if I'm on the active node
cnt1=`ps -ef | grep sysmon| grep -v grep | grep -c sysmon`
cnt2=`ps -ef | grep sysqcp| grep -v grep | grep -c sysqcp`
if [ $cnt1 -eq 0 ] && [ $cnt2 -eq 0 ] ; then
          ${ECHO} "WPPL: SYSMON/SYSQCP NOT RUNNING! ASSUMING CEDA IS DOWN!"
          ${ECHO} "WPPL: DUE TO REPLICATION THIS SCRIPT IS ONLY RUN ON THE ACTIVE SERVER!"
          ${ECHO} "WPPL: TERMINATING!"
          exit 1
fi

${ECHO} "WPPL: Trigger FTPHDL to receive WPPL-data from WPPLPC!"
VAR=`unalias cput`
VAR=`cput 0 ftphdl WPR`
${ECHO} "WPPL: Sleeping 300 sec. to wait for complete FTP-transmission!"
lcnt=1
while [ $lcnt -le 300 ]
do
        sleep 1
        ${ECHO} "#\c"
        lcnt=`expr $lcnt + 1`
done
${ECHO} "\nWPPL: Now merging files to wppl.dat!"
VAR=`/ceda/exco/WPPL/MergeFiles.sh`
${ECHO} "WPPL: Now starting!"
if [ -f $FILE0 ]; then
	${ECHO} "WPPL: Now parsing '$FILE0' and send to WPPLIF !!"

	nawk ' BEGIN {
	 FS="\n";Cnt=0;S="";HOPO="WAW";TTYP="WPPL"
	} 

	function trim(S,maxlen) {
		#printf "\nIN-TRIM:(%s)",S;
		idxr=0;
		idxl=0;
		for (i = length(S) ; i > 0 ; i--)
		{
				if (substr(S,i,1)!=" ")
				{ idxr=i; break; }
		}
		for (i = 0; i < length(S) ; i++)
		{
				if (substr(S,i,1)!=" ")
				{ idxl=i; break; }
		}
		if ( idxr-idxl < maxlen)
		{
			#printf "\nOU-TRIM:(%s)\n",substr(S,idxl,idxr-idxl);
			return substr(S,idxl,idxr-idxl);
		}
		else
		{
			#printf "\nOU-TRIM:(%s)\n",substr(S,idxl,maxlen);
			return substr(S,idxl,maxlen);
		}
	 }
	function GuessFlno(StringFlid) {
		ALC=trim(substr(StringFlid,1,2),2);
		ALC3=trim(substr(StringFlid,3,1),1);
		NUM="";
		FLNS="";
		FLTN="";
		FLNO="";
		CSGN="";
		if ((ALC3 >= "A") && (ALC3 <= "Z"))
		{
			ALC=ALC""ALC3;
			ALC3=ALC;
			ALC2="";
			NUM=substr(StringFlid,4);
		}
		else
		{
			ALC2=ALC;
			ALC3="";
			NUM=substr(StringFlid,3);
		}
		match(NUM,"[A-Z]");	
		if (RSTART==length(NUM))
		{
			FLNS=trim(substr(NUM,RSTART,RLENGTH),1);
			FLTN=substr(sprintf("%.3d     ",substr(NUM,1,RSTART-1)),1,5);
			CSGN="";
			FLNO=sprintf("%s%s%s",ALC,FLTN,FLNS);
		}
		if (RSTART!=0 && RSTART!=length(NUM))
		{
			FLTN=sprintf("%s     ",NUM);
			CSGN=sprintf("%s%s%s",ALC,FLTN,FLNS);
			FLNO="";
		}
		else if (RSTART==0) 
		{
			FLTN=substr(sprintf("%.3d     ",NUM),1,5);
			CSGN="";
			FLNO=sprintf("%s%s%s",ALC,FLTN,FLNS);
		}
	}
	{
		Cnt++
		gsub(",","�",$0);
		FLID=trim(substr($0,0,10),10);
		GuessFlno(FLID);
		CSGN=trim(FLID,8);
		ACT5=trim(substr($0,11,7),7);
		NOAR=trim(substr($0,18,7),7);
		ORG4=trim(substr($0,25,7),7);
		DES4=trim(substr($0,32,7),7);
		ROUT=trim(substr($0,39,160),160);
		OINF=trim(substr($0,199,60),60);
		AFSS=trim(substr($0,259,12),12);
		AFSE=trim(substr($0,271,12),12);
		ATSS=trim(substr($0,283,16),16);
		ATSE=trim(substr($0,299,16),16);
		RULE=trim(substr($0,315,7),7);
		DIST=trim(substr($0,322,6),6);
		ITYP=trim(substr($0,328,7),7);
		STOA=trim(substr($0,335,16),16);
		STOD=trim(substr($0,351,16),16);
		DAOF=trim(substr($0,367,10),10);
		OFBL=trim(substr($0,377,16),14);
		NOOP=trim(substr($0,393,1),1);
		"date -u +%Y%m%d%H%M%S" | getline TIME ;

		ADID=" ";
		if (ORG4 == "EPWA" || ORG4 == "WAW")
			ADID="D";
		if (DES4 == "EPWA" || DES4 == "WAW")
			ADID="A";

		# processing delay to keep CPU-utilization low
		system("sleep 1");

		###################################################################
		# now sending to wpplif/telexpool if record seems to be WAW-related
		###################################################################
		if (ADID == "A" || ADID == "D" )
		{
			printf "============================================";
			#printf "\nFLNO:(%s)(%s)--(%s),(%s),(%s)\n",FLNO,NUM,ALC,FLTN,FLNS;
			printf "\nFLNO:(%s)--CSGN:(%s)\n",FLNO,CSGN;
			printf "-------------------\n";
			if (NOOP=="0")
			{
				UFLG="P";

				CMD=sprintf("cput 0 wpplif 4 WPPL \"WPLTAB\" \"ADID,FLID,ALC2,ALC3,FLTN,FLNS,ACT5,NOAR,ORG4,DES4,ROUT,OINF,AFSS,AFSE,AIRA,LNDA,RULE,DIST,ITYP,STOA,STOD,DAOF,OFBL,TTYP,FLNO,CSGN,UFLG,HOPO\" \"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\" \"%s,%s,1234567,1\" -1 \"WPPL\" \"WPPL\" \"\" \"WAW,TAB,EXCO-WPPL\" ",ADID,FLID,ALC2,ALC3,FLTN,FLNS,ACT5,NOAR,ORG4,DES4,ROUT,OINF,AFSS,AFSE,ATSS,ATSE,RULE,DIST,substr(ITYP,0,2),STOA,STOD,DAOF,OFBL,TTYP,FLNO,CSGN,UFLG,HOPO,DAOF,DAOF);

				printf "<"Cnt">: FLID=<%s>\tADID=<%s>-PROCESS.:\n<%s>\n",FLID,ADID,CMD;
				system(CMD);
			}
			else
			{
				if (NOOP=="1")
				{
					UFLG="P";
					TXT1=sprintf("INFO: <%s> IS SET TO NO-OPERATION FROM WPPL\263"\
						      "---------------\263"\
						      "CALLSIGN      : <%s> \263"\
						      "DATE OF FLIGHT: <%s> \263"\
						      "---------------\263"\
						      "MESSAGE CONT. : <%s> \263",FLID,FLID,DAOF,$0);

					CMD=sprintf("cput 0 router 4 IBT \"TLXTAB\" \"ALC3,FLTN,FLNS,FLNU,SERE,TIME,STAT,TTYP,TXT1\" \"%s,%s,%s,%s,%s,%s,%s,%s,%s\" \"\" -1 \"WPPL\" \"WPPL\" \"\" \"WAW,TAB,EXCO-WPPL\" ",substr(FLID,0,3),substr(FLID,4,5)," ","0","R",TIME,"E","WPPL",TXT1);

					printf "<"Cnt">: FLID=<%s>\tADID=<%s>-PROCESS.:\n<%s>\n",FLID,ADID,CMD;
					system(CMD);
				}

			}
		}
		else
		{
			UFLG="I";
		}
		################################################
		# now inserting into WPLTAB all received telexes
		################################################
		if (UFLG == "P")
		{
			CMD=sprintf("cput 0 router 4 IBT \"WPLTAB\" \"ADID,FLID,ACT5,NOAR,ORG4,DES4,ROUT,OINF,AFSS,AFSE,ATSS,ATSE,RULE,DIST,ITYP,STOA,STOD,DAOF,OFBL,UFLG,HOPO\" \"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\" \"%s,%s,1234567,1\" -1 \"WPPL\" \"WPPL\" \"\" \"WAW,TAB,EXCO-WPPL\" ",ADID,FLID,ACT5,NOAR,ORG4,DES4,ROUT,OINF,AFSS,AFSE,ATSS,ATSE,RULE,DIST,ITYP,STOA,STOD,DAOF,OFBL,UFLG,HOPO,DAOF,DAOF);
			printf "<"Cnt">: FLID=<%s>\tADID=<%s>-INSERT:\n<%s>\n",FLID,ADID,CMD;
			system(CMD);
		}
	}' $FILE0

	${ECHO} "WPPL: RECORDS PARSED!"
else
	${ECHO} "WPPL: ERROR! NO WPPL-DATAFILE FOUND!"
fi
${ECHO} "WPPL: NOW EXITING!"
exit 0;
