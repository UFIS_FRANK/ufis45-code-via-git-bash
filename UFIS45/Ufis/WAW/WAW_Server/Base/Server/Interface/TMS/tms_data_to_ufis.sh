#!/usr/bin/bash
######################################################################
# SCRIPT: tmsdata_to_ufis.sh                                         #
# PURPOSE: conversion from TMS runway status data into HTML-code     #
#          , as well as loading the converted data into the          #
#          the table TMSTAB inside the UFIS-database via the         #
#          oracle sql-loader program.                                #
# SCHEDULE: run via CEDA-crontab each minute with a sleep-time       #
#           to avoid racing conditions between FTP-transfer and      #
#           access to this file  by this script                      #
# AUTHOR: ABB AIRPORT TECHNOLOGIES GMBH / J.WEERTS                   #
# DATE: 29th November 2002 - first version                           #
#       23th January  2003 - last changes to get it working          #
#                                                                    #
# NOTE: No part of this script may be changed/reproduced or          #
#       copied without written permission of ABB Airport Tech-       #
#       nologies GmbH                                                #
#                                                                    #
# (c) ABB AIRPORT TECHNOLOGIES GMBH - 2002-2003                      #
######################################################################
. /ceda/etc/UfisEnv

ECHO="echo -e"
FILE0=/ceda/exco/TMS/TMS.txt
FILE1=/ceda/exco/TMS/htm_tms.txt
# if you look for FILE2 inside here; use tms.txt instead for your search!
FILE2=/ceda/exco/TMS/tms.txt
DEST=/ceda/www/staff/temp
SQLCLEANUPSCRIPT=/ceda/exco/TMS/tms_data_cleanup.sh
SQLLOADERCTL=/ceda/exco/TMS/tms_sqlldr.ctl

# now check if I'm on the active node
cnt1=`ps -ef | grep sysmon| grep -v grep | grep -c sysmon`
cnt2=`ps -ef | grep sysqcp| grep -v grep | grep -c sysqcp`
if [ $cnt1 -eq 0 ] && [ $cnt2 -eq 0 ] ; then
          ${ECHO} "TDC: SYSMON/SYSQCP NOT RUNNING! ASSUMING CEDA IS DOWN!"
          ${ECHO} "TDC: DUE TO REPLICATION THIS SCRIPT IS ONLY RUN ON THE ACTIVE SERVER!"
          ${ECHO} "TDC: TERMINATING!"
          exit 1
fi

if [ -f $FILE0 ]; then
	rm $FILE0
fi
${ECHO} "TDC: Trigger FTPHDL to receive TMS.txt from TMS-Server!"
VAR=`unalias cput`
VAR=`cput 0 ftphdl TMS`
${ECHO} "TDC: Sleeping !"
lcnt=1
while [ $lcnt -le 35 ]
do
	sleep 1
	${ECHO} -e "#\c"
	lcnt=`expr $lcnt + 1`
done
${ECHO} -e "\nTDC: Now starting!"
if [ -f $FILE0 ]; then
	if [ -f $FILE1 ]; then
		rm $FILE1
	fi
	if [ -f $FILE2 ]; then
		rm $FILE2
	fi
	${ECHO} "TDC: Now convert '$FILE0' into '$FILE1' !!"
	awk ' BEGIN {FS=",";stat=" ";Cnt=0}
	{
		Cnt++
		if (index($1,"Cat")>0)
		{
			if ($2==0)
			{
				stat="0"
				$2=stat
			}
			if ($2==1)
			{
				stat="1"
				$2=stat
			}
			if ($2==2)
			{
				stat="2"
				$2=stat
			}
			if ($2==3)
			{
				stat="3"
				$2=stat
			}
		}
		if (index($1,"PAP")>0)
		{
			if ($2==0)
			{
				stat="OFF"
				$2=stat
			}
			if ($2==1)
			{
				stat="ON"
				$2=stat
			}
		}
		$1=$1
		$3=$3
		printf "%d|%s|%s|%s|\n",Cnt,$1,$2,$3 
		printf "%s|%s|%s\n",$1,$2,$3 >> "/ceda/exco/TMS/tms.txt"
	}' $FILE0 >> $FILE1

	if [ -f $FILE1 ]; then
		${ECHO} "TDC: CONVERTED!"
		${ECHO} "{!EOD!}" >> $FILE1
		cp $FILE1 $DEST
		VAR=`cput 0 8130 TMSTU`
	fi
	#rm -f $FILE0
	if [ -f $FILE2 ]; then
		if [ -f $SQLLOADERCTL ]; then
			${ECHO} "TDC: NOW RUNNING TMSTAB CLEANUP!"
			sqlplus $CEDADBUSER/$CEDADBPW @$SQLCLEANUPSCRIPT
			${ECHO} "TDC: NOW RUNNING SQL-LOADER FOR DB-IMPORT!"
			sqlldr userid=$CEDADBUSER/$CEDADBPW control=$SQLLOADERCTL data=$FILE2 log=$SQLLOADERCTL.log
		else
			${ECHO} "TDC: SQLLOADER-CTL FILE NOT FOUND! CAN'T IMPORT DATA TO UFIS_DB!"
		fi
	else
		${ECHO} "TDC: NO TXT-FILE FOR UFIS-DB-IMPORT FOUND!"
	fi
else
	${ECHO} "TDC: ERROR! NO TMS-DATAFILE FOUND!"
	${ECHO} "1||NO ACTUAL CAT.-INFO AVAILABLE||" > $FILE1
	${ECHO} "2||(check FTP-connection to TMS)||" >> $FILE1
	${ECHO} "3||(c) ABB Airport Technologies GmbH||" >> $FILE1
	${ECHO} "{!EOD!}" >> $FILE1
	cp $FILE1 $DEST
	VAR=`cput 0 8130 TMSTU`
fi
${ECHO} "TDC: NOW EXITING!"
exit 0
