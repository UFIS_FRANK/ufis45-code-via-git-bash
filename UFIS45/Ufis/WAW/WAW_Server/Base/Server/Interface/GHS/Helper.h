
#include "stdio.h"
#include "malloc.h"
#include "string.h"

#include <sys/stat.h>		// permissions for _chmod
#if _MSC_VER
#include <io.h>		// _chmod
#include <direct.h>		// _mkdir
#endif
#include <stdlib.h>		// errno
#include <errno.h>		// ENOENT, EEXIST

#ifndef _MSC_VER
#include <varargs.h>
#endif

// allow most detailed output of debugging information into logfile and/or special data dump files
#undef	ALLOW_DEBUG_OUTPUT

#pragma once

/*================================================================================
		Intension: implement the functionality of billing for ghs
		Runtime:   this code is designed for Windows and Unix
		Author:    Mark Vybiral
		Abbrev.:   MVy / Ufis Airport Solutions / Heusenstamm / Germany
		Contact:   M.Vybiral@gmx.de
		History:
			20.05.05	1.00	Creation
	================================================================================*/

#if !defined( _HELPER_ )
#define _HELPER_

/*================================================================================
		Macros
	================================================================================*/

#define __STRING( STR ) #STR
#define STRING( STR ) __STRING( STR )

#ifdef _MSC_VER

	#pragma message( __FILE__ "(" STRING(__LINE__) "): " "using Windows" ) 


	#ifdef TRACELN
		#pragma message( "TRACELN already defined!" ) 
	#else
		#define TRACELN( STR )\
			{\
				OutputDebugString( STR );\
				OutputDebugString( "\n" );\
			}
	#endif	TRACELN


	#define DEBUGLN\
		TRACELN

	#define dbg( LVL, MSG )\
		printf( "dbg msg:" ); printf( MSG ); printf( "\n" );


	#ifdef ASSERT
		#pragma message( "ASSERT already defined!" ) 
	#else
		#define ASSERT( COND )\
			if( !( COND ) )\
			{\
				static char buffer[100];\
				sprintf( buffer, "assertion '%s' failed at %s(%d)", ##COND, __FILE__, __LINE__ );\
				dbg( TRACE, buffer );\
			}
	#endif ASSERT


	#ifdef VERIFY
		#pragma message( "VERIFY already defined!" ) 
	#else
		#ifdef _DEBUG
			#define VERIFY( COND )\
				if( !( COND ) )\
				{\
					static char buffer[100];\
					sprintf( buffer, "verification '%s' failed at %s(%d)", ##COND, __FILE__, __LINE__ );\
					dbg( TRACE, buffer );\
				}
		#else
			#define VERIFY( COND )\
				COND
		#endif _DEBUG
	#endif VERIFY


#else

	#pragma message( "not using Windows" ) 


	#ifdef TRACELN
		#pragma message( "TRACELN already defined!" ) 
	#else
		#define TRACELN( STR )\
			dbg( TRACE, STR );
	#endif	TRACELN

	#define DEBUGLN( STR )\
		dbg( DEBUG, STR )

	#ifdef ASSERT
		#pragma message( "ASSERT already defined!" ) 
	#else
		#define ASSERT( COND )\
			if( !( COND ) ){ dbg( 0x1000, "assertion '" #COND "' failed" ); }
	#endif ASSERT


	#ifdef VERIFY
		#pragma message( "VERIFY already defined!" ) 
	#else
		#define VERIFY( COND )\
			if( !( COND ) ){ dbg( 0x1000, "verification '" #COND "' failed" ); }
	#endif VERIFY


#endif _MSC_VER




#ifdef _MSC_VER

	static BOOL IsTraceLevel_Debug()
	{
#ifdef _DEBUG
		return TRUE ;
#else
		return FALSE ;
#endif
	}


#else

	int debug_level = 0 ;

	static BOOL IsTraceLevel_Debug()
	{
		return debug_level == DEBUG ;
	}

	static BOOL IsTraceLevel_Trace()
	{
		return debug_level == TRACE || debug_level == DEBUG ;
	}

#endif _MSC_VER



static char cTemp[4000];

#ifdef _MSC_VER

static void TraceLine( const char* format, ... )
{
	//static char cTemp[2000];
	va_list args ;
	va_start( args, format );
	vsprintf( cTemp, format, args );
	va_end( args );
	TRACELN( cTemp );
}

static void DebugLine( const char* format, ... )
{
	//static char cTemp[2000];
	va_list args ;
	va_start( args, format );
	vsprintf( cTemp, format, args );
	va_end( args );
	TRACELN( cTemp );
}

#else

static void TraceLine( va_alist )
va_dcl
{
	//static char cTemp[2000];
	char* format = 0 ;
	va_list args ;
	va_start( args );
	format = va_arg( args, char* );
	vsprintf( cTemp, format, args );
	va_end( args );
	TRACELN( cTemp );
}

static void DebugLine( va_alist )
va_dcl
{
	//static char cTemp[2000];
	char* format = 0 ;
	va_list args ;
	va_start( args );
	format = va_arg( args, char* );
	vsprintf( cTemp, format, args );
	va_end( args );
	DEBUGLN( cTemp );
}

#endif _MSC_VER



#ifdef _MSC_VER

	#pragma warning( disable : 4003 )

	#ifdef ALLOW_DEBUG_OUTPUT
		#define DEBUG_OUTPUT(x1,x2,x3,x4,x5,x6)		DebugLine(x1,x2,x3,x4,x5,x6)
	#else
		#define DEBUG_OUTPUT(x1,x2,x3,x4,x5,x6)
	#endif

	#define DEBUG_OUTPUT_1		DEBUG_OUTPUT
	#define DEBUG_OUTPUT_2		DEBUG_OUTPUT
	#define DEBUG_OUTPUT_3		DEBUG_OUTPUT
	#define DEBUG_OUTPUT_4		DEBUG_OUTPUT

#else

	#ifdef ALLOW_DEBUG_OUTPUT
		#define DEBUG_OUTPUT_1(x1)		DebugLine(x1)
		#define DEBUG_OUTPUT_2(x1,x2)		DebugLine(x1,x2)
		#define DEBUG_OUTPUT_3(x1,x2,x3)		DebugLine(x1,x2,x3)
		#define DEBUG_OUTPUT_4(x1,x2,x3,x4)		DebugLine(x1,x2,x3,x4)
	#else
		#define DEBUG_OUTPUT_1(x1)
		#define DEBUG_OUTPUT_2(x1,x2)
		#define DEBUG_OUTPUT_3(x1,x2,x3)
		#define DEBUG_OUTPUT_4(x1,x2,x3,x4)
	#endif

#endif _MSC_VER



/*================================================================================
		Function: debugging help
	================================================================================*/

static char sMyDebugOutputDirectory[1000] = 
#ifdef _MSC_VER
	"\\\\Linscm02\\SandboxMVY""\\WAW44_Server\\Ufis\\WAW\\WAW_Server\\Base\\Server\\Kernel\\tmp\\" ;
#else
	"/sandbox/mvy""/WAW44_Server/Ufis/WAW/WAW_Server/Base/Server/Kernel/tmp/" ;
#endif _MSC_VER


static char sMyWorkingDirectory[1000] = 
#ifdef _MSC_VER
	"\\\\Linscm02\\SandboxMVY""\\WAW44_Server\\Ufis\\WAW\\WAW_Server\\Base\\Server\\Kernel\\tmp\\" ;
#else
	"/sandbox/mvy""/WAW44_Server/Ufis/WAW/WAW_Server/Base/Server/Kernel/tmp/" ;
#endif _MSC_VER


#ifndef SG_PCTEMPBUFFER
// only at first include this will be compiled
#define SG_PCTEMPBUFFER
static char sg_pcTempBuffer[ 64*1024 ];
#endif



static char sg_pcPathBuffer[ 1024 ];

BOOL DebuggingPathSet()
{
	return sMyDebugOutputDirectory && *sMyDebugOutputDirectory != 0 ;
}

BOOL WorkingPathSet()
{
	return sMyWorkingDirectory && *sMyWorkingDirectory != 0 ;
}

char* MakeMyDebugOutputFilename( char* filename )
{
	sprintf( sg_pcPathBuffer, "%s%s", sMyDebugOutputDirectory, filename );
	return sg_pcPathBuffer ;
}

char* MakeMyDebugOutputFilename2( char* filename, char* extension )
{
	char dot[] = "." ;
	if( !extension ) *dot = 0 ;
	sprintf( sg_pcPathBuffer, "%s%s%s%s", sMyDebugOutputDirectory, filename, dot, extension );
	return sg_pcPathBuffer ;
}

// eg. used for reading sql files
char* MakeMyWorkingFilename( char* filename )
{
	sprintf( sg_pcPathBuffer, "%s%s", sMyWorkingDirectory, filename );
	return sg_pcPathBuffer ;
}

char* MakeMyWorkingFilename2( char* filename, char* extension )
{
	char dot[] = "." ;
	if( !extension ) *dot = 0 ;
	sprintf( sg_pcPathBuffer, "%s%s%s%s", sMyWorkingDirectory, filename, dot, extension );
	return sg_pcPathBuffer ;
}


/*================================================================================
		Function: string handling stuff
	================================================================================*/

// return the first occurence of one char from set in the buffer or 0 if not found
char* chrfind( char* buf, char* set )
{
	int pos = 0 ;
	if( !buf ) return 0 ;		// can happen on iteration, already at end
	pos = strcspn( buf, set );
	if( !pos )		// because no value indcates an error check it out:
		if( buf[pos] && strchr( set, buf[pos] ) ) return buf ;		// not at end (terminating zero) and found at beginning
		else return 0 ;		// not found
	else
		return buf + pos ;
}

// find one character in string and replace by another
char* chrxchg( char* buf, char search, char replace )
{
	int pos = 0 ;
	char* find = 0 ;
	if( !buf ) return 0 ;		// can happen on iteration, already at end
	while( find = strchr( buf, search ) )
		*find = replace ;
	return buf ;
}

// return the first occurence of one char not in set in the buffer or 0 if at end
char* chrfindother( char* buf, char* set )
{
	int pos = 0 ;
	if( !buf ) return 0 ;		// can happen on iteration, already at end
	pos = strspn( buf, set );
	if( !pos )		// because no value indcates an error check it out:
		if( buf[pos] && !strchr( set, buf[pos] ) ) return buf ;		// not at end (terminating zero)
		else return 0 ;		// not valid
	else
		return buf + pos ;
}

// return the next newline
char* findnewline( char* pc )
{
	return chrfind( pc, "\n\r" );
}

// return char after the next newline
char* skipnewline( char* pc )
{
	char* eoln = chrfind( pc, "\n\r" );
	if( eoln )		// maybe we are already at the end of string
	{
		char* other = eoln +1 ;		// skip the current char, otherwise the current char would be found because it is an other one
		if( *eoln == '\n' ) other = chrfindother( other, "\r" );
		else
		if( *eoln == '\r' ) other = chrfindother( other, "\n" );
		else
		{ ASSERT( 0 ) };		// how to come here ???
		if( other ) eoln = other ;		// eoln consists of multiple characters
			else eoln++ ;		// we found only one eoln char and no others, so we need the following char
	};
	return eoln ;
}

// go back until beginning of line or a non white character (but not eoln) is found
char* gobackWhiteSpaces( char* pc )
{
	char* pcWhite = pc -1 ;
	while( *pcWhite == ' ' || *pcWhite == '\t' || *pcWhite == 255 ) pcWhite-- ;
	return ++pcWhite ;
}

char* gobackWhiteSpacesAndEoln( char* pc )
{
	char* pcWhite = pc -1 ;
	while( *pcWhite == ' ' || *pcWhite == '\t' || *pcWhite == 255 
		|| *pcWhite == '\r' || *pcWhite == '\n' ) pcWhite-- ;
	return ++pcWhite ;
}

unsigned int GetLengthTrimmedRight( char* pcData, unsigned int uiSize )
{
	if( uiSize )
		while( *( pcData + uiSize -1 ) == ' ' ) uiSize-- ;
	return uiSize ;
}

// Attention: the buffer must be big enough to expand any occurence
//	hint: the replace string should not contain the search string
char* String_Replace( char* pcBuffer, char* pcSearch, char* pcReplace )
{
	char* pos = 0 ;
	int iLenSrc = strlen( pcSearch );
	int iLenDst = strlen( pcReplace );
	while( pos = strstr( pcBuffer, pcSearch ) )
	{
		// -> code like Table_SetColumnData()
		int iLen = strlen( pos + iLenSrc );
		memmove( pos + iLenDst, pos + iLenSrc, iLen +1 );		// make space and "remove" old data
		//*( pos + iLenSrc + iLen ) = 0 ;
		memcpy( pos, pcReplace, iLenDst );	// put in new data
	};
	return pcBuffer ;
}

/*
void AppendToFile( char* pcFilename, char* pcBuffer );

char* String_Replace1( char* pcBuffer, char* pcSearch, char* pcReplace )
{
	char cTemp[200];

	char* pos = 0 ;
	int iLenSrc = strlen( pcSearch );
	int iLenDst = strlen( pcReplace );
	if( pos = strstr( pcBuffer, pcSearch ) )
	{
		// -> code like Table_SetColumnData()
		int iLen = strlen( pos + iLenSrc );

		//if( DebuggingPathSet() )
		//{
		//	sprintf( cTemp, "iLenSrc=%d, iLenDst=%d, iLen=%d, pos='%s'\n", iLenSrc, iLenDst, iLen, pos );
		//	AppendToFile( MakeMyDebugOutputFilename( "Test.info.txt" ), cTemp );		// windows reachable and readable output
		//};

		memmove( pos + iLenDst, pos + iLenSrc, iLen +1 );		// make space and "remove" old data

		//if( DebuggingPathSet() )
		//{
		//	sprintf( cTemp, "  dest=%x, source=%x, pos='%s'\n", pos + iLenDst, pos + iLenSrc, pos );
		//	AppendToFile( MakeMyDebugOutputFilename( "Test.info.txt" ), cTemp );		// windows reachable and readable output
		//};

		memcpy( pos, pcReplace, iLenDst );	// put in new data

		//if( DebuggingPathSet() )
		//{
		//	sprintf( cTemp, "  replace='%s', pos='%s'\n", pcReplace, pos );
		//	AppendToFile( MakeMyDebugOutputFilename( "Test.info.txt" ), cTemp );		// windows reachable and readable output
		//};
	};
	return pcBuffer ;
}
*/


/*================================================================================
		Function: remove single line comment like sql '--' or c++ '//'
		Behaviour:
			remove text beginning with string pcCommentStart
			a comment is finished when the line ends = '\n' found
			the original buffer is changed!
			no new buffer is allocated because the new data cannot need more space
	================================================================================*/
char* RemoveSingleLineComments( char* pcBuffer, char* pcCommentStart )
{
	char* pc = pcBuffer ;
	char* pcEOD = pcBuffer + strlen( pcBuffer );
	while( pc = strstr( pc, pcCommentStart ) )
	{
		// 1. pc points to beginning of a comment
		// 2. search for the end of the comment
		// 3. then go back to eliminate white spaces, too
		char* eoln = findnewline( pc );		// find next eoln
		if( !eoln ) eoln = pc + strlen( pc );		// no eoln so we must be at end of buffer
		// copy the complete part behind the end of the comment to the beginning of the comment
		ASSERT( eoln );		// cannot and must not be null !!!
		pc = gobackWhiteSpaces( pc );
		strcpy( pc, eoln );
	};
	return pcBuffer ;
}

// trim line and check if the line would be emtpy, then remove it
char* RemoveEmptyLines( char* pcBuffer )
{
	char* pc = pcBuffer ;
	char* pcNext = 0 ;		// last line, use to determine if a line is empty
	char* pcEOD = pcBuffer + strlen( pcBuffer );
	char* pcAfterEoln ;

	// remove leading empty lines
	while( ( pcNext = findnewline( pc ) ) == pc )
	{
		pcNext = skipnewline( pc );
		strcpy( pc, pcNext );
	};

	// last NON eoln char after eoln
	pcAfterEoln = pc ;
	while( pcNext = findnewline( pc ) )
	{
		int iLineLength = 0 ;

		char* pcWhite = gobackWhiteSpaces( pcNext );
		if( pcWhite < pcNext  )		// remember that we are one char before what we are looking for
		{
			strcpy( pcWhite, pcNext );
			pcNext = pcWhite ;
		};

		iLineLength = pcNext - pcAfterEoln ;
		ASSERT( iLineLength >= 0 );
		if( !iLineLength )
		{
			pcNext = skipnewline( pcNext );
			strcpy( pc, pcNext );
		}
		else
		if( iLineLength < 0 )
		{
			TRACELN( "error in RemoveEmptyLines(), calculated line length is negative" );
		}
		else
		{
			pcAfterEoln = skipnewline( pcNext );
			ASSERT( pcAfterEoln );		// can happen but is not expected
			if( pcAfterEoln && !*pcAfterEoln )		// this is necessary to remove eoln at end of string
				pcAfterEoln = pcNext ;		// point to the previous
			pc = pcAfterEoln ;
		};

		// from the current eoln up to the next NON eoln

		/*
		while( ( pcNext = findnewline( pc ) ) == pc )
		{	// remove the eoln characters
			pcNext = skipnewline( pc );
			if( !pcNext || !*pcNext )		// string ends with a newline
			{
				if( pc ) *pc = 0 ;
				break ;
			};
			strcpy( pc, pcNext );
		};
		//if( pcNext != pc )		// this line is not empty, quite not necessary this operation; only for understanding
		//	pc = pcNext ;
		{
			char* pcWhite = gobackWhiteSpaces( pc );
			if( pcWhite < pc )		// remember that we are one char before what we are looking for
			{
				strcpy( pcWhite, pc );
				pc = pcWhite ;
			};
		}

		pc = findnewline( pc );
		pcNext = pc ;
		while( pcNext = skipnewline( pcNext ) );
		if( pcNext && *pcNext && !*( pcNext +1 ) )		// string ends with a newline
		{
			*pc = 0 ;
			break ;
		};
		pc = pcNext ;
		*/
	};
	return pcBuffer ;
}

void RemoveSingleLineCommentsSQL( char* pcBuffer )
{
	RemoveSingleLineComments( pcBuffer, "--" );
}


/*================================================================================
		Function: returns a list of all found string quoted like specified
		Behaviour:
			search for a quote and uses the string ended by the next quote
	================================================================================*/
/*
void GetQuotedStrings( LPChain pChain, char* pcBuffer, char* pcQuotes )
{
	... moved to Chain.h because cannot include here when not using c and h files
}
*/

/*================================================================================
		file and path handling
	================================================================================*/

BOOL InitPathIfPossible( char* path )
{
	BOOL result = FALSE ;
	if( path && *path )
	{
		int error = 0 ;
		TraceLine( "  directory is '%s'", path );
		error = _mkdir( path );
		if( error != -1 )
		{
			int error = 0 ;
#if _MSC_VER
			int mode = _S_IREAD | _S_IWRITE ;
#else
			int mode = S_IRWXG | S_IRWXU | S_IRWXO ;
#endif
			TraceLine( "  directory '%s' created, setting permissions %d", path, mode );
			error = _chmod( path, mode );
			if( error == -1 ) TraceLine( "  some error occured changing permissions" );
			else result = TRUE ;
		}
		else if( errno == ENOENT ) TraceLine( "  error: path not found for creation" );
		else if( errno == EEXIST )
		{
			TraceLine( "  directory does exist" );
			result = TRUE ;
		};
	}
	else
		TraceLine( "  directory is not set" );
	return result ;
}


/*================================================================================
		Function: read / write string buffer from / to file
	================================================================================*/
void RemoveFile( char* pcFilename )
{
	remove( pcFilename );
}

void WriteToFile( char* pcFilename, char* pcBuffer )
{
	FILE* file = fopen( pcFilename, "w" );
	TraceLine( "write string to file '%s'", pcFilename );
	if( file ) fprintf( file, pcBuffer );
	else TraceLine( "  failed" );
	fclose( file );
}

void AppendToFile( char* pcFilename, char* pcBuffer )
{
	FILE* file = fopen( pcFilename, "a" );
	TraceLine( "append string at file '%s'", pcFilename );
	if( file ) fprintf( file, pcBuffer );
	else TraceLine( "  failed" );
	fclose( file );
}

void WriteBufferToFile( char* pcFilename, char* pcBuffer, unsigned long ulSize )
{
	FILE* file = fopen( pcFilename, "w" );
	if( file )
	{
		unsigned long written = fwrite( pcBuffer, ulSize, 1, file );
		if( written != ulSize )
			TraceLine( "file '%s' not written correctly, %d of %d bytes", pcFilename, written, ulSize );
		fclose( file );
	};
}

void AppendBufferToFile( char* pcFilename, char* pcBuffer, unsigned long ulSize )
{
	FILE* file = fopen( pcFilename, "a" );
	if( file )
	{
		unsigned long written = fwrite( pcBuffer, ulSize, 1, file );
		if( written != ulSize )
			TraceLine( "file '%s' not written correctly, %d of %d bytes", pcFilename, written, ulSize );
		fclose( file );
	};
}

// Attention: the buffer must be big enough!
unsigned long ReadFromFile( char* pcFilename, char* pcBuffer )
{
	size_t read = 0 ;
	FILE* file = fopen( pcFilename, "r" );
	if( file )
	{
		if( !fseek( file, 0, SEEK_END ) )
		{
			long size = ftell( file );
			VERIFY( !fseek( file, 0, SEEK_SET ) );
			read = fread( pcBuffer, size, 1, file );
		};
		fclose( file );
	};
	return read ;
}

// Attention: dynamically allocated buffer must be freed manually !!!
//	a buffer pointer is set; if buffer already points to data the space must be big enough
//	return size of buffer
unsigned long AllocateFromFile( char* pcFilename, char** ppcBuffer )
{
	unsigned long size = 0 ;
	FILE* file = fopen( pcFilename, "r" );
	if( !file ) return 0 ;
	if( !fseek( file, 0, SEEK_END ) )
	{
		size = ftell( file );
		if( size )
		{
			unsigned long read = 0 ;
			TraceLine( "read file '%s'", pcFilename );

			ASSERT( ppcBuffer );
			if( !*ppcBuffer )
			{
				*ppcBuffer = (char *)malloc( size +1 );
				TraceLine( "  %d bytes allocated", size +1 );
			}
			else
			{
				*ppcBuffer = (char *)realloc( *ppcBuffer, size +1 );
				TraceLine( "  %d bytes reallocated", size +1 );
			};

			memset( *ppcBuffer, 0, size +1 );

			VERIFY( !fseek( file, 0, SEEK_SET ) );

			read = fread( *ppcBuffer, 1, size, file );
			if( read != size )
			{
				TraceLine( "  file not read completely, %d of %d", pcFilename, read, size );
			};
		}
		else
		{
			TraceLine( "file '%s' has no size said ftell()", pcFilename );
		};
	};
	fclose( file );
	return size ;
}


/*================================================================================
		Function: memory dump
		------------------------------------------------------------------------------
		Attention: trying to access memory that is prohibited will cause exception, so 
			do not go over the end of you buffer!
	================================================================================*/

void TraceDumpMemory( char* buffer, unsigned long size )
{
	char cLine[200];
	char* pcLine = (char *)&cLine ;
	unsigned int uiItemsPerLine = 32 ;
	unsigned long posOld = 0 ;
	unsigned long pos = 0 ;
	*pcLine = 0 ;

	pcLine += sprintf( pcLine, "0x%0.8X : ", &buffer[ pos ] );
	for( pos = 0 ; pos < size ; pos++ )
	{
		// write data in sedecimal form
		unsigned char c = buffer[ pos ];
		pcLine += sprintf( pcLine, "%.2X ", c );
		if( pos % 8 == 7 && pos % uiItemsPerLine != (uiItemsPerLine -1) )
			pcLine += sprintf( pcLine, "! " );

		if( pos % uiItemsPerLine == (uiItemsPerLine -1) || pos == size -1 )
		{
			unsigned long pos2 = 0 ;
			// fill up a non complete line
			for( pos2 = pos ; pos2 % uiItemsPerLine < uiItemsPerLine -1 ; pos2++ )
			{
				if( pos2 % 8 == 7 && pos2 % uiItemsPerLine != (uiItemsPerLine -1) )
					pcLine += sprintf( pcLine, "! " );
				pcLine += sprintf( pcLine, "   " );
			};

			// write text in clear form
			pcLine += sprintf( pcLine, ": \"" );
			for( pos2 = posOld ; pos2 < posOld + uiItemsPerLine && pos2 < size ; pos2++ )
			{
				char c = buffer[ pos2 ];
				if( !isprint( c ) ) c = '.' ;
				pcLine += sprintf( pcLine, "%c", c );
			};
			pcLine += sprintf( pcLine, "\"" );

			TraceLine( cLine );
			pcLine = (char *)&cLine ;
			*pcLine = 0 ;
			posOld = pos +1 ;
			pcLine += sprintf( pcLine, "0x%0.8X : ", &buffer[ posOld ] );
		};
	};
}

/*================================================================================
		END
	================================================================================*/

#endif _HELPER_
