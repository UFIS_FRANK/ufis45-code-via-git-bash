------------------------------------------------------------
--	GetBackDatedFlightUrnos( @@MONTH )
--	get a list of all flight urnos older than n month
--	the unquoted fields does not come into db_if fieldlist; they must be at the end of the list otherwise the order of fieldname and datacolumn will be confuse
------------------------------------------------------------
select "URNO"
from	afttab
where	tifa between to_char( ADD_MONTHS( sysdate, -@@MONTHS ) -@@DAYS, 'yyyymmdd' ) and to_char( sysdate, 'yyyymmdd' ) || '235959'
  and	tifd between to_char( ADD_MONTHS( sysdate, -@@MONTHS ) -@@DAYS, 'yyyymmdd' ) and to_char( sysdate, 'yyyymmdd' ) || '235959'
--where	to_date( tifa, 'yyyymmddhh24miss' ) > ADD_MONTHS( sysdate, -@@MONTHS )
--  and	to_date( tifd, 'yyyymmddhh24miss' ) > ADD_MONTHS( sysdate, -@@MONTHS )
--;
