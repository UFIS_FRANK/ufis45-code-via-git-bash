------------------------------------------------------------
--	GetBillingRecords
--	double quoted strings like "xxxx" will be used in fieldlist; needed for ceda client db interface
--	the unquoted fields does not come into db_if fieldlist; they must be at the end of the list otherwise the order of fieldname and datacolumn will be confuse
--	@@WHERE will be exchanged after loading the statement by a list of flight URNOs
------------------------------------------------------------
select "AUID"
,	"EXID"		-- contains the urno of the depending flight
,	"FLNO"
,	"ADID"
,	"CSGN"
,	"ALC2"
,	"ALC3"
,	"ACT3"
,	"ACT5"
,	"REGN"
,	"STYP"
,	"STOX"
,	"ETOX"
,	"TMOA"
,	"ACTI"
,	"OXBL"
,	"CTOT"
,	"FTYP"
,	"PSTA"
,	"GTD1"
,	"RKEY"
,	"FLTI"
,	"REM1"
,	"APC4"
,	"APCI"
,	"VIA4"
,	"PAXT"
,	"PAXI"
,	"PAXD"
,	"PXTR"
,	"PXID"
,	"PAX1"
,	"PAXB"
,	"PAXE"
,	"PAXF"
,	"PAXM"
,	"PAXC"
,	"WTOT"
,	"WCAG"
,	"WMAI"
,	"WBAG"
,	"PU1B"
,	"PU1E"
,	"PU2B"
,	"PU2E"
,	"PU3B"
,	"PU3E"
,	"VIAL"
-- the following field does not come into db_if fieldlist; they must be at the end of the list otherwise the order of fieldname and datacolumn will be confuse
,	IFST
,	IFAC
,	CDAT
,	IDAT
from	BILTAB
where	EXID in ( @@WHERE )
--;
