------------------------------------------------------------
--	GetFlightRecords
--	@@WHERE will be exchanged after loading the statement by a list of flight URNOs
--	double quoted strings like "xxxx" will be used in fieldlist; needed for ceda client db interface
--	the unquoted fields does not come into db_if fieldlist; they must be at the end of the list otherwise the order of fieldname and datacolumn will be confuse
------------------------------------------------------------
select "FLNU"
--,	TYPEKEY
,	trim( "APC3" )
,	sum( VALU ) as "TOTAL"
,	sum( decode( TYPEKEY, 'LDMPAXS        ', VALU ) ) as "PAXT"	-- F01
,	sum( decode( TYPEKEY, 'LDMPAX      I  ', VALU ) ) as "PAXI"	-- F02
,	sum( decode( TYPEKEY, 'LDMPAXS  R     ', VALU ) ) as "PAXD"	-- F03
,	sum( decode( TYPEKEY, 'PTMPAXS  TD    ', VALU ) ) as "PXTR"	-- F04
,	sum( decode( TYPEKEY, 'LDMPADS        ', VALU ) ) as "PXID"	-- F05
,	sum( decode( TYPEKEY, 'LDMPAXF        ', VALU ) ) as "PAX1"	-- F06
,	sum( decode( TYPEKEY, 'LDMPAXB        ', VALU ) ) as "PAXB"	-- F07
,	sum( decode( TYPEKEY, 'LDMPAXE        ', VALU ) ) as "PAXE"	-- F08
,	sum( decode( TYPEKEY, 'LDMPAX      F  ', VALU ) ) as "PAXF"	-- F09
,	sum( decode( TYPEKEY, 'LDMPAX      M  ', VALU ) ) as "PAXM"	-- F10
,	sum( decode( TYPEKEY, 'LDMPAX      C  ', VALU ) ) as "PAXC"	-- F11
,	sum( decode( TYPEKEY, 'LDMLOAS        ', VALU ) ) as "WTOT"	-- F15
,	sum( decode( TYPEKEY, 'LDMLOAC        ', VALU ) ) as "WCAG"	-- F16
,	sum( decode( TYPEKEY, 'LDMLOAM        ', VALU ) ) as "WMAI"	-- F17
,	sum( decode( TYPEKEY, 'LDMLOAB        ', VALU ) ) as "WBAG"	-- F18
	-- specials, found in testdata -> ask JIM/Jibbo M�ller
--,	sum( decode( TYPEKEY, 'MVTPAXT        ', VALU ) ) as "S01"
--,	sum( decode( TYPEKEY, 'LDMPAX      A  ', VALU ) ) as "S02"
--,	sum( decode( TYPEKEY, 'LDMPAXT        ', VALU ) ) as "S03"
--,	sum( decode( TYPEKEY, 'LDMPADF        ', VALU ) ) as "S04"
--,	sum( decode( TYPEKEY, 'LDMPADB        ', VALU ) ) as "S05"
--,	sum( decode( TYPEKEY, 'LDMPADE        ', VALU ) ) as "S06"
--,	sum( decode( TYPEKEY, 'LDMPADT        ', VALU ) ) as "S07"
--,	sum( decode( TYPEKEY, 'LDMLOAT        ', VALU ) ) as "S08"
from (
	select FLNU
		-- each field has a ased width of three chars, see used in decode()
	,	LOATAB.DSSN || LOATAB.TYPE || LOATAB.STYP || LOATAB.SSTP || LOATAB.SSST as TYPEKEY
	,	trim( LOATAB.VALU ) as VALU
	,	LOATAB.APC3
	from	LOATAB
	where LOATAB.FLNU in ( @@FLNUs )
	  and	LOATAB.FLNU > 0
	  and	Substr( trim( LOATAB.VALU ), 0, 1 ) between '0' and '9'	-- ugly, we need function Is_Number(); reason is 'NIL' found on some servers
)
group by APC3
, FLNU
--;