------------------------------------------------------------
--	GetFlightRecords
--	@@WHERE will be exchanged after loading the statement by a list of flight URNOs
--	double quoted strings like "xxxx" will be used in fieldlist; needed for ceda client db interface
--	the unquoted fields does not come into db_if fieldlist; they must be at the end of the list otherwise the order of fieldname and datacolumn will be confuse
--	20050902 JIM: 2 records for towings requested	
------------------------------------------------------------
--select null as AUID,		-- AUID will be set first in SetBillingRecord.sql
select URNO as "EXID"
,	"FLNO"
,	"ADID"	-- ADID: 'A'=1, 'D'=0
,	"CSGN"
,	"ALC2"
,	"ALC3"
,	"ACT3"
,	"ACT5"
,	"REGN"
,	"STYP"
,	"STOX"	-- ADID: 'A'=STOA, 'D'=STOD
,	"ETOX"	-- ADID: 'A'=ETOA, 'D'=ETOD
,	"TMOA"
,	"ACTI"	-- ADID: 'A'=LAND, 'D'=AIRB
,	"OXBL"	-- ADID: 'A'=ONBL, 'D'=OFBL
,	"CTOT"
,	"FTYP"
,	"PSTA"
,	"GTD1"
,	"RKEY"
,	"FLTI"
,	substr( REM1, 1, 200 ) as "REM1"
,	"APC4"	-- ADID: 'A'=ORG4, 'D'=DES4, null
,	"APC3"	-- additionally APC3 is needed for getting data from LOATAB.APC3; together with AFTTAB.URNO = LOATAB.FLNU
,	"APCI"	-- ADID: 'A'=ORG4, 'D'=DES4, null	-- will be filled by VIAL data
,	"VIA4"
,	null as "PAXT"
,	null as "PAXI"
,	null as "PAXD"
,	null as "PXTR"
,	null as "PXID"
,	null as "PAX1"
,	null as "PAXB"
,	null as "PAXE"
,	null as "PAXF"
,	null as "PAXM"
,	null as "PAXC"
,	null as "WTOT"
,	null as "WCAG"
,	null as "WMAI"
,	null as "WBAG"
,	null as "PU1B"
,	null as "PU1E"
,	null as "PU2B"
,	null as "PU2E"
,	null as "PU3B"
,	null as "PU3E"
--,	substr( VIAL, 1, 200 ) as "VIAL"
--	resulting length should be 45 and content should be only the 4 letter airport codes
,	VIAL as "VIALOLD"
,	substr( VIAL, 5,4 ) 
	|| ',' || trim( substr( VIAL, 120 + 5,4 ) )
	|| ',' || trim( substr( VIAL, 240 + 5,4 ) )
	|| ',' || trim( substr( VIAL, 360 + 5,4 ) )
	|| ',' || trim( substr( VIAL, 480 + 5,4 ) )
	|| ',' || trim( substr( VIAL, 600 + 5,4 ) )
	|| ',' || trim( substr( VIAL, 720 + 5,4 ) )
	|| ',' || trim( substr( VIAL, 840 + 5,4 ) )
	|| ',' || trim( substr( VIAL, 960 + 5,4 ) )
	|| ',' || trim( substr( VIAL,1080 + 5,4 ) ) as "VIAL"
,	null as "IFST"		-- set at writing record into db
,	null as "IFAC"		-- set at writing record into db
,	to_char( sysdate, 'yyyymmddhh24miss' ) as "CDAT"		-- creation date
,	null as "IDAT"
from(
	select URNO
	,	FLNO
	,	1 as ADID
	,	CSGN
	,	ALC2
	,	ALC3
	,	ACT3
	,	ACT5
	,	REGN
	,	STYP
	,	STOA as STOX
	,	ETOA as ETOX
	,	TMOA
	,	LAND as ACTI
	,	ONBL as OXBL
	,	CTOT
	,	FTYP
	,	PSTA
	,	GTA1 as GTD1
	,	RKEY
	,	FLTI
	,	REM1
	,	ORG4 as APC4
	,	DES3 as APC3		-- link to LOATAB
	,	ORG4 as APCI		-- filled with VIAL entries
	,	VIA4
	,	VIAL
	from	AFTTAB
	where URNO in ( @@FLNUs )
--	where TIFA between '2005052503' and '2005052504'
--	where	( TIFA between '20050524' and '20050525' and ADID = 'B' )
	  and	( ADID = 'A' or ADID = 'B' )
	  and	FTYP NOT IN ( 'S' , 'N' )
		-- Towing will get like a departure
		-- Ground movement will get like a departure
union
	select URNO
	,	FLNO
	,	0 as ADID
	,	CSGN
	,	ALC2
	,	ALC3
	,	ACT3
	,	ACT5
	,	REGN
	,	STYP
	,	STOD as STOX
	,	ETOD as ETOX
	,	TMOA
	,	AIRB as ACTI
	,	OFBL as OXBL
	,	CTOT
	,	FTYP
	,	PSTD as PSTA
	,	GTD1
	,	RKEY
	,	FLTI
	,	REM1
	,	DES4 as APC4
	,	DES3 as APC3		-- link to LOATAB
	,	DES4 as APCI		-- filled with VIAL entries
	,	VIA4
	,	VIAL
	from	AFTTAB
	where URNO in ( @@FLNUs )
--	where TIFA between '2005052503' and '2005052504'
--	where	( TIFA between '20050524' and '20050525' and ADID = 'B' )
-- 20050902: and ( ADID = 'D' or ( ADID = 'B' and not FTYP in ( 'T', 'G' ) ) )
	  and	( ADID = 'D' or ADID = 'B' )
	  and	FTYP NOT IN ( 'S' , 'N' )
		-- Towing will get like a departure
		-- Ground movement will get like a departure
)
--;
