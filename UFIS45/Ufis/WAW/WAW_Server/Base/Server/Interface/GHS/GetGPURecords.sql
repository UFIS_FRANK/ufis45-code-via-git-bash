------------------------------------------------------------
--	GetGPURecords / Ground Power Units
--	@@WHERE will be exchanged after loading the statement by a list of flight URNOs
--	double quoted strings like "xxxx" will be used in fieldlist; needed for ceda client db interface
--	the unquoted fields does not come into db_if fieldlist; they must be at the end of the list otherwise the order of fieldname and datacolumn will be confuse
------------------------------------------------------------
select trim( FLNU ) as "FLNU"
,	to_char( sum( trim( PU1B ) ) ) as "PU1B"
,	to_char( sum( trim( PU1E ) ) ) as "PU1E"
,	to_char( sum( trim( PU2B ) ) ) as "PU2B"
,	to_char( sum( trim( PU2E ) ) ) as "PU2E"
,	to_char( sum( trim( PU3B ) ) ) as "PU3B"
,	to_char( sum( trim( PU3E ) ) ) as "PU3E"
from(
	select FLNU
	,	GAAB as PU1B
	,	GAAE as PU1E
	,	null as PU2B
	,	null as PU2E
	,	null as PU3B
	,	null as PU3E
	from	GPATAB
	where FLNU in ( @@FLNUs )
	  and ASEQ = 1
union
	select FLNU
	,	null as PU1B
	,	null as PU1E
	,	GAAB as PU2B
	,	GAAE as PU2E
	,	null as PU3B
	,	null as PU3E
	from	GPATAB
	where FLNU in ( @@FLNUs )
	  and ASEQ = 2
union
	select FLNU
	,	null as PU1B
	,	null as PU1E
	,	null as PU2B
	,	null as PU2E
	,	GAAB as PU3B
	,	GAAE as PU3E
	from	GPATAB
	where FLNU in ( @@FLNUs )
	  and ASEQ = 3
)
group by FLNU
--;
