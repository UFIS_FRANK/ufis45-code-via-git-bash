---------------------------
-- show the load sums in columns regarding to available afttab flights having loads
---------------------------
select FLNU
--,	TYPEKEY
,	( VALU ) as TOTAL
,	APC3
,	( decode( TYPEKEY, 'LDMPAXS        ', VALU ) ) PAXT	-- F01
,	( decode( TYPEKEY, 'LDMPAX      I  ', VALU ) ) PAXI	-- F02
,	( decode( TYPEKEY, 'LDMPAXS  R     ', VALU ) ) PAXD	-- F03
,	( decode( TYPEKEY, 'PTMPAXS  TD    ', VALU ) ) PAXT	-- F04
,	( decode( TYPEKEY, 'LDMPADS        ', VALU ) ) PXID	-- F05
,	( decode( TYPEKEY, 'LDMPAXF        ', VALU ) ) PAX1	-- F06
,	( decode( TYPEKEY, 'LDMPAXB        ', VALU ) ) PAXB	-- F07
,	( decode( TYPEKEY, 'LDMPAXE        ', VALU ) ) PAXE	-- F08
,	( decode( TYPEKEY, 'LDMPAX      F  ', VALU ) ) PAXF	-- F09
,	( decode( TYPEKEY, 'LDMPAX      M  ', VALU ) ) PAXM	-- F10
,	( decode( TYPEKEY, 'LDMPAX      C  ', VALU ) ) PAXC	-- F11
,	( decode( TYPEKEY, 'LDMLOAS        ', VALU ) ) WTOT	-- F15
,	( decode( TYPEKEY, 'LDMLOAC        ', VALU ) ) WCAG	-- F16
,	( decode( TYPEKEY, 'LDMLOAM        ', VALU ) ) WMAI	-- F17
,	( decode( TYPEKEY, 'LDMLOAB        ', VALU ) ) WBAG	-- F18
	-- specials, found in testdata -> ask JIM/Jibbo M�ller
--,	( decode( TYPEKEY, 'MVTPAXT        ', VALU ) ) S01
--,	( decode( TYPEKEY, 'LDMPAX      A  ', VALU ) ) S02
--,	( decode( TYPEKEY, 'LDMPAXT        ', VALU ) ) S03
--,	( decode( TYPEKEY, 'LDMPADF        ', VALU ) ) S04
--,	( decode( TYPEKEY, 'LDMPADB        ', VALU ) ) S05
--,	( decode( TYPEKEY, 'LDMPADE        ', VALU ) ) S06
--,	( decode( TYPEKEY, 'LDMPADT        ', VALU ) ) S07
--,	( decode( TYPEKEY, 'LDMLOAT        ', VALU ) ) F08
from	(
	select AFTTAB.URNO as FLNU
		-- each field has a ased width of three chars, see used in decode()
	,	LOATAB.DSSN || LOATAB.TYPE || LOATAB.STYP || LOATAB.SSTP || LOATAB.SSST as TYPEKEY
	,	LOATAB.VALU
	,	LOATAB.APC3
	--,	AFTTAB.ORG3 || AFTTAB.DES3
	--,	AFTTAB.ORG4 || AFTTAB.DES4
	,	AFTTAB.ADID
	from	AFTTAB
	,	LOATAB
	where	LOATAB.FLNU <> 0		-- makes it a bit faster
	  and	LOATAB.FLNU = AFTTAB.URNO
	  and	(
		trim( LOATAB.APC3 ) is null
		or
		LOATAB.APC3 in (
			-- list of origin and destination airports not beeing home
			select distinct decode( AFTTAB.ADID
				, 'A', AFTTAB.ORG3
				, 'D', AFTTAB.DES3
				) as APC3
			from	AFTTAB
			,	LOATAB
			-- TODO: urnos for flight records comes in here
			--	now we get ALL possible entries!!! and this could vast long time
			-- where	AFTTAB.URNO in ()
			where	LOATAB.FLNU <> 0		-- makes it a bit faster
			  and	LOATAB.FLNU = AFTTAB.URNO
			)
		)
	)
-- group by FLNU
;


---------------------------
-- get APC3 codes from loadtab
---------------------------
select	distinct
	FLNU
,	APC3
from	LOATAB
;


---------------------------
-- get flights
---------------------------
select	distinct
	URNO
,	FLNO
,	STOA
,	STOD
from	AFTTAB
where	URNO in ( 12019638, 39925480, 46451481 )	-- WAW
;

->	39925480, 46451481		-- WAW.AFTTAB.URNO (Vancouver)


---------------------------
-- get some fligth Urnos which have loads
---------------------------
select	AFTTAB.URNO
,	AFTTAB.FLNO
,	LOATAB.APC3
,	substr( AFTTAB.VIAL, 1, 500 )
from	AFTTAB
,	LOATAB
where	AFTTAB.URNO = LOATAB.FLNU
  and	length( AFTTAB.VIAL ) > 100
  and	LOATAB.APC3 != ' '
and	rownum < 100
;

->	139664498, 172953656, 170117292, 139623427, 139681687		-> FCO.AFTTAB.URNO (Adr1)



---------------------------
-- show the load sums in columns regarding to available afttab flights having loads
---------------------------
select FLNU
,	TYPEKEY
,	( VALU ) as TOTAL
,	APC3
,	decode( TYPEKEY, 'LDMPAXS        ', VALU ) PAXT	-- F01
,	decode( TYPEKEY, 'LDMPAX      I  ', VALU ) PAXI	-- F02
,	decode( TYPEKEY, 'LDMPAXS  R     ', VALU ) PAXD	-- F03
,	decode( TYPEKEY, 'PTMPAXS  TD    ', VALU ) PAXT	-- F04
,	decode( TYPEKEY, 'LDMPADS        ', VALU ) PXID	-- F05
,	decode( TYPEKEY, 'LDMPAXF        ', VALU ) PAX1	-- F06
,	decode( TYPEKEY, 'LDMPAXB        ', VALU ) PAXB	-- F07
,	decode( TYPEKEY, 'LDMPAXE        ', VALU ) PAXE	-- F08
,	decode( TYPEKEY, 'LDMPAX      F  ', VALU ) PAXF	-- F09
,	decode( TYPEKEY, 'LDMPAX      M  ', VALU ) PAXM	-- F10
,	decode( TYPEKEY, 'LDMPAX      C  ', VALU ) PAXC	-- F11
,	decode( TYPEKEY, 'LDMLOAS        ', VALU ) WTOT	-- F15
,	decode( TYPEKEY, 'LDMLOAC        ', VALU ) WCAG	-- F16
,	decode( TYPEKEY, 'LDMLOAM        ', VALU ) WMAI	-- F17
,	decode( TYPEKEY, 'LDMLOAB        ', VALU ) WBAG	-- F18
	-- specials, found in testdata -> ask JIM/Jibbo M�ller
--,	decode( TYPEKEY, 'MVTPAXT        ', VALU ) S01
--,	decode( TYPEKEY, 'LDMPAX      A  ', VALU ) S02
--,	decode( TYPEKEY, 'LDMPAXT        ', VALU ) S03
--,	decode( TYPEKEY, 'LDMPADF        ', VALU ) S04
--,	decode( TYPEKEY, 'LDMPADB        ', VALU ) S05
--,	decode( TYPEKEY, 'LDMPADE        ', VALU ) S06
--,	decode( TYPEKEY, 'LDMPADT        ', VALU ) S07
--,	decode( TYPEKEY, 'LDMLOAT        ', VALU ) F08
from	LOATAB

where	LOATAB.FLNU in

	(
	select AFTTAB.URNO as FLNU
		-- each field has a ased width of three chars, see used in decode()
	,	LOATAB.DSSN || LOATAB.TYPE || LOATAB.STYP || LOATAB.SSTP || LOATAB.SSST as TYPEKEY
	,	LOATAB.VALU
	,	LOATAB.APC3
	--,	AFTTAB.ORG3 || AFTTAB.DES3
	--,	AFTTAB.ORG4 || AFTTAB.DES4
	,	AFTTAB.ADID
	from	AFTTAB
	,	LOATAB
	where	LOATAB.FLNU in ( 12019336, 12019337, 46477542, 39925480, 46451481 )
	)
-- group by FLNU
;




select	FLNU
--,	TYPEKEY
,	APC3
--,	VALU
,	sum( decode( TYPEKEY, 'LDMPAXS        ', VALU ) ) PAXT -- F01
,	sum( decode( TYPEKEY, 'LDMPAX      I  ', VALU ) ) PAXI -- F02
,	sum( decode( TYPEKEY, 'LDMPAXS  R     ', VALU ) ) PAXD -- F03
,	sum( decode( TYPEKEY, 'PTMPAXS  TD    ', VALU ) ) PAXT -- F04
,	sum( decode( TYPEKEY, 'LDMPADS        ', VALU ) ) PXID -- F05
,	sum( decode( TYPEKEY, 'LDMPAXF        ', VALU ) ) PAX1 -- F06
,	sum( decode( TYPEKEY, 'LDMPAXB        ', VALU ) ) PAXB -- F07
,	sum( decode( TYPEKEY, 'LDMPAXE        ', VALU ) ) PAXE -- F08
,	sum( decode( TYPEKEY, 'LDMPAX      F  ', VALU ) ) PAXF -- F09
,	sum( decode( TYPEKEY, 'LDMPAX      M  ', VALU ) ) PAXM -- F10
,	sum( decode( TYPEKEY, 'LDMPAX      C  ', VALU ) ) PAXC -- F11
,	sum( decode( TYPEKEY, 'LDMLOAS        ', VALU ) ) WTOT -- F15
,	sum( decode( TYPEKEY, 'LDMLOAC        ', VALU ) ) WCAG -- F16
,	sum( decode( TYPEKEY, 'LDMLOAM        ', VALU ) ) WMAI -- F17
,	sum( decode( TYPEKEY, 'LDMLOAB        ', VALU ) ) WBAG -- F18
	-- specials, found in testdata -> ask JIM/Jibbo M�ller
,	sum( decode( TYPEKEY, 'MVTPAXT        ', VALU ) ) S01
,	sum( decode( TYPEKEY, 'LDMPAX      A  ', VALU ) ) S02
,	sum( decode( TYPEKEY, 'LDMPAXT        ', VALU ) ) S03
,	sum( decode( TYPEKEY, 'LDMPADF        ', VALU ) ) S04
,	sum( decode( TYPEKEY, 'LDMPADB        ', VALU ) ) S05
,	sum( decode( TYPEKEY, 'LDMPADE        ', VALU ) ) S06
,	sum( decode( TYPEKEY, 'LDMPADT        ', VALU ) ) S07
,	sum( decode( TYPEKEY, 'LDMLOAT        ', VALU ) ) F08
from (
	select FLNU
		-- each field has a ased width of three chars, see used in decode()
	,	LOATAB.DSSN || LOATAB.TYPE || LOATAB.STYP || LOATAB.SSTP || LOATAB.SSST as TYPEKEY
	,	LOATAB.VALU
	,	LOATAB.APC3
	from	LOATAB
	-- where LOATAB.FLNU in ( 12019336, 12019337, 46477542, 39925480, 46451481 ) -- WAW
	where LOATAB.FLNU in ( 139664498, 172953656, 170117292, 139623427, 139681687 ) -- FCO
	  and LOATAB.VALU != 'NIL'
	)
group by APC3
, FLNU
;
