------------------------------------------------------------
--	Transfer Billing Records
--	- Remote Tablename on Vancouver: BILTAB_TOR (this is a link)
--	- Remote Tablename on WawUfis1: ghs_interface.TB_FLIGHTS_INTERFACE@GHS.WAWGHSDB
------------------------------------------------------------

--------------------------------------
-- record IDs that are not in remote table
--------------------------------------
drop table BILTAB_IDLIST_TEMP
;

-----------------------------------------------------------
-- prepare next transfer
-- records, which get updates during transfer, get 'N' again and will be 
-- transfered later
-----------------------------------------------------------
update BILTAB 
set IFST = 'T'
where	IFST in ( 'N', 'E' )
;

create table BILTAB_IDLIST_TEMP
as
(
--------------------------------------
-- record IDs that are not in remote table
-- which were 'N'ew or to whose a previous send has failed indicated by an 'E'rror
--	they can be inserted
--------------------------------------
select	AUID as ID
,	to_char( sysdate, 'yyyymmddhh24miss' ) as IDAT		-- import date
,	case when
		AUID in(
			select	AUTOID
--			from	BILTAB_TOR
			from	ghs_interface.TB_FLIGHTS_INTERFACE@GHS.WAWGHSDB where AUTOID>0
			)
		then 'U'
		else 'I'
	end as ACTION
from	BILTAB
where	IFST ='T' -- and rownum<100
)
;
--------------------------------------
-- create new records by copying
--------------------------------------
insert into ghs_interface.TB_FLIGHTS_INTERFACE@GHS.WAWGHSDB
(	AUTOID
,	EXTERNAL_ID
,	FLN
,	BOUND
,	CALLSIGN
,	AIRLINE_CODE_2L
,	AIRLINE_CODE_3L
,	AIRCRAFT_TYPE_3L
,	AIRCRAFT_TYPE_5L
,	AIRCRAFT_CODE
,	HANDLING_TYPE_CODE
,	ST
,	ET
,	TMT
,	AT
,	BT
,	CTOT
,	FLIGHT_STATUS_CODE
,	POSITION_CODE
,	GATE_CODE
,	OPN
,	FLIGHT_TYPE_CODE
,	REMARK
,	AIRPORT_CODE_ICAO
,	AIRPORT_PXINFO_CODE_ICAO
,	VIAPORT_CODE_ICAO
,	PAX_TOTAL
,	PAX_INFANTS
,	PAX_DIRECT_TRANSIT
,	PAX_TRANSFER
,	PAX_ID
,	PAX_FIRST
,	PAX_BUSINESS
,	PAX_ECONOMY
,	PAX_FEMALE
,	PAX_MALE
,	PAX_CHILDS
,	WEIGHT_TOTAL
,	WEIGHT_CARGO
,	WEIGHT_MAIL
,	WEIGHT_BAGGAGE
,	GPU_BEGIN1
,	GPU_END1
,	GPU_BEGIN2
,	GPU_END2
,	GPU_BEGIN3
,	GPU_END3
,	LIST_VIA_AIRPORT_CODE
,	INTERFACE_STATE
,	INTERFACE_ACTION
,	CREATE_TIME
,	IMPORT_TIME
)
-- values
(
select	AUID as AUTOID
,	EXID as EXTERNAL_ID
--,	case when FLNO is null then ' ' else FLNO end as FLN
-- 20050818 JIM: use CSGN, when  FLNO is empty:
-- 20050818 JIM: ,	decode( FLNO, null, ' ', FLNO ) as FLN
, decode( FLNO, null, CSGN,flno) as FLN
,	ADID as BOUND
,	CSGN as CALLSIGN
,	ALC2 as AIRLINE_CODE_2L
,	ALC3 as AIRLINE_CODE_3L
,	ACT3 as AIRCRAFT_TYPE_3L
,	ACT5 as AIRCRAFT_TYPE_5L
,	REGN as AIRCRAFT_CODE
,	STYP as HANDLING_TYPE_CODE
,	STOX as ST
,	ETOX as ET
,	TMOA as TMT
,	ACTI as AT
,	OXBL as BT
,	CTOT as CTOT
,	FTYP as FLIGHT_STATUS_CODE
,	PSTA as POSITION_CODE
,	GTD1 as GATE_CODE
,	RKEY as OPN
,	FLTI as FLIGHT_TYPE_CODE
,	REM1 as REMARK
,	APC4 as AIRPORT_CODE_ICAO
,	APCI as AIRPORT_PXINFO_CODE_ICAO
,	VIA4 as VIAPORT_CODE_ICAO
,	PAXT as PAX_TOTAL
,	PAXI as PAX_INFANTS
,	PAXD as PAX_DIRECT_TRANSIT
,	PXTR as PAX_TRANSFER
,	PXID as PAX_ID
,	PAX1 as PAX_FIRST
,	PAXB as PAX_BUSINESS
,	PAXE as PAX_ECONOMY
,	PAXF as PAX_FEMALE
,	PAXM as PAX_MALE
,	PAXC as PAX_CHILDS
,	WTOT as WEIGHT_TOTAL
,	WCAG as WEIGHT_CARGO
,	WMAI as WEIGHT_MAIL
,	WBAG as WEIGHT_BAGGAGE
,	PU1B as GPU_BEGIN1
,	PU1E as GPU_END1
,	PU2B as GPU_BEGIN2
,	PU2E as GPU_END2
,	PU3B as GPU_BEGIN3
,	PU3E as GPU_END3
,	VIAL as LIST_VIA_AIRPORT_CODE
,	'N' as INTERFACE_STATE		-- IFST
,	IFAC as INTERFACE_ACTION
,	substr( CDAT,1,14 ) as CREATE_TIME
,	BILTAB_IDLIST_TEMP.IDAT as IMPORT_TIME		-- set import date
from	BILTAB
,	BILTAB_IDLIST_TEMP
where	BILTAB_IDLIST_TEMP.ID = BILTAB.AUID
  and	BILTAB_IDLIST_TEMP.ACTION = 'I'
  and   decode( FLNO, null, CSGN,flno) is not null
)
;
--------------------------------------
-- update existing records by overwriting data
--------------------------------------
update ghs_interface.TB_FLIGHTS_INTERFACE@GHS.WAWGHSDB
set(	FLN
,	BOUND
,	CALLSIGN
,	AIRLINE_CODE_2L
,	AIRLINE_CODE_3L
,	AIRCRAFT_TYPE_3L
,	AIRCRAFT_TYPE_5L
,	AIRCRAFT_CODE
,	HANDLING_TYPE_CODE
,	ST
,	ET
,	TMT
,	AT
,	BT
,	CTOT
,	FLIGHT_STATUS_CODE
,	POSITION_CODE
,	GATE_CODE
,	OPN
,	FLIGHT_TYPE_CODE
,	REMARK
,	AIRPORT_CODE_ICAO
,	AIRPORT_PXINFO_CODE_ICAO
,	VIAPORT_CODE_ICAO
,	PAX_TOTAL
,	PAX_INFANTS
,	PAX_DIRECT_TRANSIT
,	PAX_TRANSFER
,	PAX_ID
,	PAX_FIRST
,	PAX_BUSINESS
,	PAX_ECONOMY
,	PAX_FEMALE
,	PAX_MALE
,	PAX_CHILDS
,	WEIGHT_TOTAL
,	WEIGHT_CARGO
,	WEIGHT_MAIL
,	WEIGHT_BAGGAGE
,	GPU_BEGIN1
,	GPU_END1
,	GPU_BEGIN2
,	GPU_END2
,	GPU_BEGIN3
,	GPU_END3
,	LIST_VIA_AIRPORT_CODE
,	INTERFACE_STATE
,	INTERFACE_ACTION
,	CREATE_TIME
,	IMPORT_TIME
) = (
-- select	case when FLNO is null then ' ' else FLNO end as FLNO
-- 20050818 JIM: use CSGN, when  FLNO is empty:
-- 20050818 JIM: select	decode( FLNO, null, ' ', FLNO ) as FLNO
select	decode( FLNO, null, CSGN,flno) as FLN
,	ADID
,	CSGN
,	ALC2
,	ALC3
,	ACT3
,	ACT5
,	REGN
,	STYP
,	STOX
,	ETOX
,	TMOA
,	ACTI
,	OXBL
,	CTOT
,	FTYP
,	PSTA
,	GTD1
,	RKEY
,	FLTI
,	REM1
,	APC4
,	APCI
,	VIA4
,	PAXT
,	PAXI
,	PAXD
,	PXTR
,	PXID
,	PAX1
,	PAXB
,	PAXE
,	PAXF
,	PAXM
,	PAXC
,	WTOT
,	WCAG
,	WMAI
,	WBAG
,	PU1B
,	PU1E
,	PU2B
,	PU2E
,	PU3B
,	PU3E
,	VIAL
,	'N'	-- IFST
,	IFAC
,	CDAT
,	to_char( sysdate, 'yyyymmddhh24miss' )	-- BILTAB_IDLIST_TEMP.IDAT
from	BILTAB
where	AUID = ghs_interface.TB_FLIGHTS_INTERFACE.AUTOID@GHS.WAWGHSDB
)
where	AUTOID in( 
	select	ID
	from	BILTAB_IDLIST_TEMP
	where	ACTION = 'U'
	)
;
--------------------------------------
-- remove records from remote marked as deleted in local table
--------------------------------------
delete ghs_interface.TB_FLIGHTS_INTERFACE@GHS.WAWGHSDB
--where ghs_interface.TB_FLIGHTS_INTERFACE@GHS.WAWGHSDB.AUTOID in ( 
where AUTOID in ( 
	select ID 
	from BILTAB_IDLIST_TEMP 
	where ACTION = 'D' 
	)
;
------------------------------------------------------------
--	set the source table data depending on if the previously done copying succeeds or fails
--	if failed the IDAT date is not the same than expected and an error 'E' is set
------------------------------------------------------------
-- cannot set I and E in one statement, because if remote record does not exist nothing can be set
--update	BILTAB
--set IFST = (
--  select 
--    decode( CREATE_TIME
--    , BILTAB.CDAT, 'I'	-- date existing and equal, copy must have been successful
--    , null, 'e'		-- no date, because it was a new record
--    ,'E' ) as IFST		-- else there was probably an error
--  from ghs_interface.TB_FLIGHTS_INTERFACE@GHS.WAWGHSDB
--  where ghs_interface.TB_FLIGHTS_INTERFACE@GHS.WAWGHSDB.AUTOID = BILTAB.AUID
--  )
--, IDAT = (
--  select IMPORT_TIME
--  from ghs_interface.TB_FLIGHTS_INTERFACE@GHS.WAWGHSDB
--  where ghs_interface.TB_FLIGHTS_INTERFACE@GHS.WAWGHSDB.AUTOID = BILTAB.AUID
--  )
--where BILTAB.AUID in ( select ID from BILTAB_IDLIST_TEMP )
--;
---------------------------------
-- records exist, import did not fail ... hopefully
-- TODO: for safer result compare with creation date, too
---------------------------------
-- 20060724 JIM: added commit:
commit
;

-- 20060724 JIM: added "where AUTOID>0"
update BILTAB 
set IFST = 'I'
where	IFST = 'T'
and BILTAB.AUID in ( 
	select AUTOID 
	from ghs_interface.TB_FLIGHTS_INTERFACE@GHS.WAWGHSDB where AUTOID>0
	)
;
-- 20060724 JIM: added commit:
commit
;
---------------------------------
-- records NOT exist, import probably failed
---------------------------------
-- 20060724 JIM: added "and rownum<10"
-- 20060724 JIM: added "where AUTOID>0"
update BILTAB
set IFST = 'E'
where	IFST = 'T' -- and rownum<50
and BILTAB.AUID NOT in ( 
	select AUTOID 
	from ghs_interface.TB_FLIGHTS_INTERFACE@GHS.WAWGHSDB where AUTOID>0
	)
;

--drop table BILTAB_IDLIST_TEMP
--;
commit
;
