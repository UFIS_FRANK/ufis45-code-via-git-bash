------------------------------------------------------------
--	SetBillingRecord
--	@@VALUES will be exchanged in preparation of data
--	double quoted strings like "xxxx" will be used in fieldlist; needed for ceda client db interface
--	the unquoted fields does not come into db_if fieldlist; they must be at the end of the list otherwise the order of fieldname and datacolumn will be confuse
--	remark: whether the field_list is evaluated depends on the code! so the quotes are sometimes quite egal
------------------------------------------------------------
insert into BILTAB
(	AUID
-- Attention: the order of fields is now another than originally defined
,	IFST		-- interface state, check against documents, may be confusing with action flag
,	IFAC		-- interface action, check against documents, may be confusing with state flag
--
,	"EXID"
,	"FLNO"
,	"ADID"
,	"CSGN"
,	"ALC2"
,	"ALC3"
,	"ACT3"
,	"ACT5"
,	"REGN"
,	"STYP"
,	"STOX"
,	"ETOX"
,	"TMOA"
,	"ACTI"
,	"OXBL"
,	"CTOT"
,	"FTYP"
,	"PSTA"
,	"GTD1"
,	"RKEY"
,	"FLTI"
,	"REM1"
,	"APC4"
,	"APCI"
,	"VIA4"
,	"PAXT"
,	"PAXI"
,	"PAXD"
,	"PXTR"
,	"PXID"
,	"PAX1"
,	"PAXB"
,	"PAXE"
,	"PAXF"
,	"PAXM"
,	"PAXC"
,	"WTOT"
,	"WCAG"
,	"WMAI"
,	"WBAG"
,	"PU1B"
,	"PU1E"
,	"PU2B"
,	"PU2E"
,	"PU3B"
,	"PU3E"
,	"VIAL"
--,	"IFST"		-- set value herein
--,	"IFAC"		-- set value herein
,	"CDAT"
,	"IDAT"
)
values(
	BILTAB_AUID.nextval
,	'N'		-- interface state "New"
,	'I'		-- interface action "Inserted"
,	@@VALUES
)
--;
