#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/WAW/WAW_Server/Base/Server/Interface/ghshdl.c 1.1 2006/08/25 17:48:06SGT jim Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/* 20030630 JIM: may core!: dbg(TRACE,"Command: <%s>,prlCmdblk->command")     */
/* 20050801 JIM: On URT and IRT data may include other fiekds than URNO.      */
/*               Avoid using other fields as URNO                             */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS44 (c) ABB AAT/I skeleton.c 44.1.0 / 11.12.2002 HEB";

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>

#include "AATArray.h"

/* #include "Helper.h"
   #include "Chain.h"
   #include "Table.h" */
#include "GHS/ghs.src"
#include "stdlib.h"

#ifdef _HPUX_SOURCE
	extern int daylight;
	extern long timezone;
	extern char *tzname[2];
#else
	extern int _daylight;
	extern long _timezone;
	extern char *_tzname[2];
#endif

/* int debug_level = 0;		// 050718 MVy: now defined in Helper, but who modifies the value to DEBUG ? */

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;

static char cgProcessName[20] = "\0";
static char  cgConfigFile[512] = "\0";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */

static long lgEvtCnt = 0;
static char pcgDataArea[12*1024];					/* temporary storing records */
/* static char pcgSQLBuffer[4*1024];*/						/* temporary storing sql statements */
static char pcgPathBuffer[1024];						/* temporary storing file and pathnames */
/*char pcSQLPath[1000];			// path for sql files from configuration */

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int  Init_Process();
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
			   char *pcpCfgBuffer);


static int	Reset(void);                       /* Reset program          */
static void	Terminate(int ipSleep);            /* Terminate program      */
static void	HandleSignal(int);                 /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void   HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/


static int	HandleData(EVENT *prpEvent);       /* Handles event data     */
static int  GetCommand(char *pcpCommand, int *pipCmd);

static int Test();
static int TransferBillingRecords();
static int CreateBillingRecords();
static void DebugPrintHelp();

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;
	int ilOldDebugLevel = 0;


	INITIALIZE;			/* General initialization	*/


	debug_level = TRACE;

	strcpy(cgProcessName,argv[0]);

	dbg(TRACE,"MAIN: version <%s>",sccs_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}else{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */

	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
			check_ret(ilRc);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}else{
		dbg(TRACE,"MAIN: init_db() OK!");
	} /* end of if */

	/* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

	sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */

	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */

	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		ilRc = Init_Process();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"Init_Process: init failed!");
		} /* end of if */

	} else {
		Terminate(30);
	}/* end of if */

	/*CEDAArrayInitialize( 2, 0 );*/		/* two tables, no index per table */

  /* get path for sql files from configuration
  //memset( pcSQLPath, 0, sizeof( pcSQLPath ) ); */
  if( ( ilRc = iGetConfigEntry( cgConfigFile, "MAIN", "SQLpath", CFG_STRING, (char *)sMyWorkingDirectory ) ) != RC_SUCCESS )
	  dbg( TRACE, "missing 'SQLpath' from configuration 'MAIN' in config file '%s'", cgConfigFile );
	InitPathIfPossible( sMyWorkingDirectory );

  if( ( ilRc = iGetConfigEntry( cgConfigFile, "MAIN", "DBGpath", CFG_STRING, (char *)sMyDebugOutputDirectory ) ) != RC_SUCCESS )
	  dbg( TRACE, "missing 'DBGpath' from configuration 'MAIN' in config file '%s'", cgConfigFile );
	InitPathIfPossible( sMyDebugOutputDirectory );

	Statements_Initialize();

	dbg(TRACE,"=====================");
	dbg(TRACE,"MAIN: initializing OK");
	dbg(TRACE,"=====================");

	for(;;)
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* Acknowledge the item */
		ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
		if( ilRc != RC_SUCCESS ) 
		{
			/* handle que_ack error */
			HandleQueErr(ilRc);
		} /* fi */

		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */

		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRc == RC_SUCCESS )
		{
			
			lgEvtCnt++;

			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
				Terminate(1);
				break;
					
			case	RESET		:
				ilRc = Reset();
				break;
					
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRc = HandleData(prgEvent);
					if(ilRc != RC_SUCCESS)
					{
						HandleErr(ilRc);
					}/* end of if */
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
				

				/* 050512 MVy: make a test request and show just a few data in logfile */
			/*case	999 :
			//	CreateBillingRecords();
			//	break; */

			case	998 :
				TransferBillingRecords();
				break;

			case	997 :
				GetBackDatedFlights( 2, 0 );		/* get urnos of 2 month old flights */
				break;

			case	996 :
				GetBackDatedFlights( 0, 5 );		/* get urnos of 5 days old flights */
				break;

			case	995 :
				Test();
				break;


			case	TRACE_ON :
				debug_level = DEBUG ;		/* 050718 MVy: now defined in Helper, but who modifies the value to DEBUG ? */
				dbg_handle_debug(prgEvent->command);
				break;

			case	TRACE_OFF :
				debug_level = TRACE ;		/* 050718 MVy: now defined in Helper, but who modifies the value to DEBUG ? */
				dbg_handle_debug(prgEvent->command);
				break;

			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				DebugPrintHelp();
				break;
			} /* end switch */

		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
		
	} /* end for */
	
} /* end of MAIN */


void DebugPrintHelp()
{
	TraceLine( "===================================================================================" );
	TraceLine( "  example usage information: " );
	TraceLine( "    qput '%s' 998      : transfer billing records", mod_name );
	TraceLine( "    qput '%s' 997      : create billing records for the last 2 months", mod_name );
	TraceLine( "    qput '%s' 996      : create billing records for the last 5 days", mod_name );
	TraceLine( "    qput '%s' cmd=URT tab=X fld=X sel=X dat=<comma separated flight urnos>  : update specified flight(s)", mod_name );
	TraceLine( "    qput '%s' cmd=IRT tab=X fld=X sel=X dat=<comma separated flight urnos>  : insert new billing record for specified flight(s)", mod_name );
	TraceLine( "    qput '%s' cmd=XFR  : transfer billing records", mod_name );
	TraceLine( "    qput '%s' cmd=INIT tab=X fld=X sel=X dat=\"<<months><,days>>\"  : create billing records for the last months and/or days", mod_name );
	TraceLine( "===================================================================================" );
}


/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/



static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
			   CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
	dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
	    clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}


static int Init_Process()
{
	int  ilRc = RC_SUCCESS;			/* Return code */
  char clSection[64] = "\0";
  char clKeyword[64] = "\0";
  int ilOldDebugLevel = 0;
	long pclAddFieldLens[12];
	char pclAddFields[256] = "\0";
	char pclSqlBuf[2560] = "\0";
	char pclSelection[1024] = "\0";
 	short slCursor = 0;
	short slSqlFunc = 0;
	char  clBreak[24] = "\0";


	if(ilRc == RC_SUCCESS)
	{
		/* read HomeAirPort from SGS.TAB */
		ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<Init_Process> EXTAB,SYS,HOMEAP not found in SGS.TAB");
			Terminate(30);
		} else {
			dbg(TRACE,"<Init_Process> home airport <%s>",cgHopo);
		}
	}

	
	if(ilRc == RC_SUCCESS)
	{

		ilRc = tool_search_exco_data("ALL","TABEND", cgTabEnd);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<Init_Process> EXTAB,ALL,TABEND not found in SGS.TAB");
			Terminate(30);
		} else {
			dbg(TRACE,"<Init_Process> table extension <%s>",cgTabEnd);
		}
	}

	if (ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"Init_Process failed");
	}

		return(ilRc);
	
} /* end of initialize */




/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");
	
	return ilRc;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	/* unset SIGCHLD ! DB-Child will terminate ! */
	Statements_Destroy();

	dbg(TRACE,"Terminate: now DB logoff ...");

	signal(SIGCHLD,SIG_IGN);

	logoff();

	dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


	dbg(TRACE,"Terminate: now leaving ...");

	fclose(outp);

	sleep(ipSleep < 1 ? 1 : ipSleep);
	
	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
	dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

	switch(pipSig)
	{
	default	:
		Terminate(1);
		break;
	} /* end of switch */

	exit(1);
	
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	

		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	

			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;	

			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;

			case	SHUTDOWN	:
				Terminate(1);
				break;
						
			case	RESET		:
				ilRc = Reset();
				break;
						
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;

			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;

			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	} while (ilBreakOut == FALSE);

			ilRc = Init_Process();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitDemhdl: init failed!");
			} 
     

} /* end of HandleQueues */
	



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
	int	   ilRc           = RC_SUCCESS;			/* Return code */
	int      ilCmd          = 0;

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;
	char    *pclRow          = NULL;
	char 		clUrnoList[2400];
	char 		clUrno[32];
	char 		clTable[34];
	int			ilUpdPoolJob = TRUE;
  int     ilFldNum = -1;

	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;


	strcpy(clTable,prlCmdblk->obj_name);

	dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

	/****************************************/
	DebugPrintBchead(TRACE,prlBchead);
	DebugPrintCmdblk(TRACE,prlCmdblk);
	/* 20030630 JIM: may core!: dbg(TRACE,"Command: <%s>,prlCmdblk->command"); */
	dbg(TRACE,"Command: <%s>",prlCmdblk->command);
	dbg(TRACE,"originator follows event = %p ",prpEvent);
	dbg(TRACE,"originator<%d>",prpEvent->originator);
	dbg(TRACE,"selection follows Selection = %p ",pclSelection);
	dbg(TRACE,"selection <%s>",pclSelection);
	dbg(TRACE,"fields    <%s>",pclFields);
	dbg(TRACE,"data      <%s>",pclData);
	/****************************************/

	if( !strcmp( prlCmdblk->command, "URT" ) )
	{
    ilFldNum = get_item_no(pclFields,"URNO",5) + 1;
    if (ilFldNum==0)
    {
       ilFldNum = get_item_no(pclFields,"FLNU",5) + 1;
    }
    if (ilFldNum==0)
    {
       ilFldNum = 1;
    }
    (void) get_real_item( clUrno, pclData, ilFldNum );
		TraceLine( "event command: UPDATE with URNO '%s', pos '%d'", clUrno , ilFldNum);
		UpdateBillingRecords( clUrno );
	}
	else
	if( !strcmp( prlCmdblk->command, "IRT" ) )
	{
    ilFldNum = get_item_no(pclFields,"URNO",5) + 1;
    if (ilFldNum==0)
    {
       ilFldNum = get_item_no(pclFields,"FLNU",5) + 1;
    }
    if (ilFldNum==0)
    {
       ilFldNum = 1;
    }
    (void) get_real_item( clUrno, pclData, ilFldNum );
		TraceLine( "event command: INSERT with URNO '%s', pos '%d'", clUrno , ilFldNum);
		CreateBillingRecords( clUrno );
	}
	else
	if( !strcmp( prlCmdblk->command, "XFR" ) )
	{
		TraceLine( "event command: TRANSFER to remote table" );
		TransferBillingRecords();
	}
	else
	if( !strcmp( prlCmdblk->command, "INIT" ) )
	{
		int months = 2 ;
		int days = 0 ;
		if( pclData )
		{
			char* posComma = strchr( pclData, ',' );
			if( posComma )
			{
				char save = *posComma ;
				*posComma = '\0' ;
				months = atoi( pclData );
				*posComma = save ;
				posComma++ ;
				if( *posComma )
					days = atoi( posComma );
			}
			else
				if( *pclData )
				{
					months = atoi( pclData );
					days = 0 ;
				};
		};
		TraceLine( "event command: INITIALIZE for all flights older than %d months and %d days", months, days );
		GetBackDatedFlights( months, days );
	}
	else
		TraceLine( "unknown event command %s", prlCmdblk->command );

	dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

	return(RC_SUCCESS);
	
} /* end of HandleData */


/*****************************************************************************
		little helpers
	****************************************************************************/

int sprintf2( char* pcBuffer, char* pcFormat, char* pcFrom, char* pcTo )
{
	return sprintf( pcBuffer, pcFormat, pcFrom, pcTo, pcFrom, pcTo );
}


/*****************************************************************************
		execute some sql statement
		the result is put into chain and return how many entries are in result
		TODO: the fieldnames should be evaluated automatically
	*****************************************************************************/
int DoSQL( SChain* pChain, char* pcStatement, char *pcFieldNames )
{
	int ilRC = DB_SUCCESS ;
	short slCursor = 0 ;
	short slFkt = START ;
	int iCount = 0 ;
	int iFieldsCount = 0 ;

	dbg( DEBUG, "=================================================" );
	dbg( DEBUG, "pcStatement='%s'", pcStatement );
	/* dbg( TRACE, "=================================================" ); */

	memset( pcgDataArea, 0, sizeof( pcgDataArea ) );
	ilRC = db_if( slFkt, &slCursor, pcStatement, pcgDataArea );
	TraceLine( "DoSQL:resultcode=%d", ilRC );
	/*if( (  ) == DB_SUCCESS )
	{
		BuildItemBuffer( pcgDataArea, "", iFieldsCount, "\t" );
		dbg( TRACE, "ok: ", pcgDataArea );
		Chain_Append( pChain, Link_CreateBy( pcgDataArea ) );		// need a copy of the data, removed in Chain_Destroy()
		iCount++ ;
		slFkt = NEXT ;
	}
	else
	{
		dbg( TRACE, "error %s", ilRC );
	};

	dbg( TRACE, "saved, %d =============================", iCount );
	*/
	close_my_cursor( &slCursor );
	return iCount ;
}

/*****************************************************************************
		load data by sql statement into chained list
		TODO: the fieldnames should be evaluated automatically
	*****************************************************************************/
void Load( SChain* pChain, char* pcStatement, char *pcFieldNames )
{
	int ilRC = DB_SUCCESS ;
	short slCursor = 0 ;
	short slFkt = START ;
	int iCount = 0 ;
	/* int iLen = 0 ; */
	int iFieldsCount = 0 ;

	/*	Chain_TestDummy();  dbg( TRACE, "Chain_TestDummy done" ); 	*/
	/* Chain_Append( &chain, Link_SetData( Link_New(), pcStatement ) );
	Chain_Append( &chain, Link_SetData( Link_New(), "\n---\n" ) ); */

	/* this is for help, but the column names are not a record but will be interpreted later as such, so do not use in online system */
	/*
	LPLink pLink = 0 ;
	Chain_Append( pChain, pLink = Link_CreateBy( pcFieldNames ) );		// need a copy of the data, removed in Chain_Destroy()
	{
		//char pcBuffer[ 4096 ];
		//sprintf( pcBuffer, "%d replacements", i );
		//Chain_Append( pChain, Link_CreateBy( pcBuffer ) ) ;
		char* pc = pLink->pData ;
		while( pc = strchr( pc, ',' ) ) *pc = '\t' ;		// replace ONE character by ONE other
	}
	/**/

	dbg( DEBUG, "=================================================" );
	dbg( DEBUG, "pcStatement='%s'", pcStatement );

	/* read all records from db and make each record to a tabbed string  */
	iFieldsCount = field_count( pcFieldNames );		/* performance: otherwise this must be done each record */
	dbg( DEBUG, "iFieldsCount=%d  pcFieldNames=%s", iFieldsCount, pcFieldNames );

	memset( pcgDataArea, 0, sizeof( pcgDataArea ) );
	if( DebuggingPathSet() ) WriteToFile( MakeMyDebugOutputFilename( "lastload.SQL" ), pcStatement );
	while( ( ilRC = db_if( slFkt, &slCursor, pcStatement, pcgDataArea ) ) == DB_SUCCESS )
	{
		BuildItemBuffer( pcgDataArea, "", iFieldsCount, "\t" );
		/* iLen = get_real_item( pcBuffer, pcgDataArea, 4 );
		if( iLen > 0 ) dbg( TRACE, "%d(%d): %s : %s ", iCount, iLen, pcgDataArea, pcBuffer );
		else dbg( TRACE, "%d(%d): %s : <<null>> ", iCount, iLen, pcgDataArea ); */
		Chain_Append( pChain, Link_CreateBy( pcgDataArea ) );		/* need a copy of the data, removed in Chain_Destroy() */
		iCount++ ;
		slFkt = NEXT ;
	};
	TraceLine( "%d records loaded =============================", iCount );
	close_my_cursor( &slCursor );
}


/******************************************************************************/
/* T � H � S T !
/******************************************************************************/
static int Test()
{
	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;
	char* pcPath = 0 ;

	prlBchead    = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;

	dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);
	DebugPrintBchead(TRACE,prlBchead);
	DebugPrintCmdblk(TRACE,prlCmdblk);
	dbg(TRACE,"Command: <%s>",prlCmdblk->command);
	dbg(TRACE,"originator follows event = %p ",prgEvent);
	dbg(TRACE,"originator<%d>",prgEvent->originator);
	dbg(TRACE,"selection follows Selection = %p ",pclSelection);
	dbg(TRACE,"selection <%s>",pclSelection);
	dbg(TRACE,"fields    <%s>",pclFields);
	dbg(TRACE,"data      <%s>",pclData);
	dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

	TraceDumpMemory( (char *)prgEvent, sizeof( EVENT ) + sizeof( BC_HEAD ) );

	TraceLine( "================= Test BEGIN =================" );
	if( DebuggingPathSet() ) TraceLine( "  debugging path is set" );
		else TraceLine( "  debugging path is NOT set" );

	pcPath = MakeMyDebugOutputFilename( "delete.this.file" );
	TraceLine( "  create testfile in debugging path: %s", pcPath );
	WriteToFile( pcPath, "I am a test, please delete me" );

	pcPath = MakeMyWorkingFilename( "delete.this.file" );
	TraceLine( "  create testfile in working path: %s", pcPath );
	WriteToFile( pcPath, "I am a test, please delete me" );

	TraceLine( "================= Test  END  =================" );
}


/******************************************************************************/
/* send billing records to billing server
/******************************************************************************/

static int TransferBillingRecords()
{
	int result = 0 ;
/*
	char* pcSQL_GetNewBillingRecords = 0 ;
	SChain chainBillingRecords_FieldNames ;
	STable tableBilling ;

	char* pcSQL_GetRemoteBillingRecords = 0 ;
	SChain chainRemoteBillingRecords_FieldNames ;
	STable tableRemoteBilling ;

	SChain chainAUID ;
	SChain chainAUTOID ;
*/
	char* pcSQL_CopyBillingRecords = 0 ;

	TraceLine( "TransferBillingRecords" );

	/* from BILTAB get all records with IFST=N|E
	// make a list of all AUIDs
	// from BILTAB_TOR get all records by AUIDs */

	IF_INITSQL( pcSQL_CopyBillingRecords, "TransferBillingRecords.sql", 4 )
	{
		unsigned uiNdx = 0 ;
		LPLink pIterator = 0 ;
		/* split into multiple statements and run each statement */
		SChain chainStatements ;
		Chain_Initialize( &chainStatements );

		RemoveSingleLineCommentsSQL( pcSQL_CopyBillingRecords );
		RemoveEmptyLines( pcSQL_CopyBillingRecords );
		GetSeparatedStrings( &chainStatements, pcSQL_CopyBillingRecords, ";", TRUE, TRUE );

		pIterator = Link_GetHead( &chainStatements );
		while( pIterator )
		{
			char cTemp[10];
			char* pcSQL = (char *)Link_GetData( pIterator );		/* get record as string */
			sprintf( cTemp, "CPY.%d.sql", uiNdx++ );
			if( DebuggingPathSet() ) WriteToFile( MakeMyDebugOutputFilename( cTemp ), pcSQL );
			pIterator = Chain_GetNext( &chainStatements, pIterator );
		};

		uiNdx = 1 ;
		pIterator = Link_GetHead( &chainStatements );
		while( pIterator )
		{
			char* pcSQL = (char *)Link_GetData( pIterator );		/* get record as string */
			TraceLine( "execute statement %d of %d:", uiNdx++, chainStatements.ulLength );
			DoSQL( 0, pcSQL, 0 );
			pIterator = Chain_GetNext( &chainStatements, pIterator );
		};

		Chain_Destroy( &chainStatements );
	};

	/* make it by sql: */
	/*
	IF_INITSQL( pcSQL_GetNewBillingRecords, "GetNewBillingRecords.sql", 1 )
	IF_INITSQL( pcSQL_GetRemoteBillingRecords, "GetRemoteBillingRecords.sql", 2 )
	IF_INITSQL( pcSQL_CopyBillingRecords, "TransferBillingRecords.sql", 3 )
	// up to now init process works fine
	{
		INITFIELDNAMES( chainBillingRecords_FieldNames, pcSQL_GetNewBillingRecords );
		INITFIELDNAMES( chainRemoteBillingRecords_FieldNames, pcSQL_GetRemoteBillingRecords );

		LoadRecords( pcSQL_GetNewBillingRecords, 0, &tableBilling, &chainBillingRecords_FieldNames, "BIL.NEW" );
		TraceLine( "billing data, loaded %d records", tableBilling.Records.ulLength );
		if( !Table_ColumnToChain( &tableBilling, "AUID", &chainAUID, FALSE ) )
		{
			TRACELN( "Table_ColumnToString of billing table AUID failed" );
		}
		else
		{
			char pcWhere[ 4 * 1024 ];
			Chain_ToString( &chainAUID, ",", "''", sg_pcTempBuffer );		// create string of ID list needed for sql statement
			if( DebuggingPathSet() )
				WriteToFile( MakeMyDebugOutputFilename2( "BILTAB.AUID", "dat" ), sg_pcTempBuffer );
			if( !sg_pcTempBuffer )
			{
				TRACELN( "Chain_ToString: no item string created" );
			}
			else
			{
				sprintf( pcWhere, "where	AUTOID in ( %s )", sg_pcTempBuffer );
				LoadRecords( pcSQL_GetRemoteBillingRecords, pcWhere, &tableRemoteBilling, &chainRemoteBillingRecords_FieldNames, "BIL.REM" );
				TraceLine( "remote billing data, loaded %d records", tableRemoteBilling.Records.ulLength );

				// all already found records must be updated, all others can be inserted
				if( tableRemoteBilling.Records.ulLength 
					&& !Table_ColumnToChain( &tableRemoteBilling, "AUTOID", &chainAUTOID, FALSE ) )
				{
					TRACELN( "Table_ColumnToString of remote billing table AUTOID failed" );
				}
				else
				{
					if( tableRemoteBilling.Records.ulLength )		// try removing items only if there are items available from remote billing table
					{
						unsigned int uiRemovals = Chain_RemoveByChain( &chainAUID, &chainAUTOID );		// remove all found AUTOIDs from AUID chain
						TraceLine( "Chain_RemoveByChain removed %d IDs from billing ID list already existing in remote ID list", uiRemovals );
					};

					if( chainAUID.ulLength )
					{
						// do a normal insert (copy them) on all records not in remote billing list
						sprintf( pcWhere, "where	AUTOID in ( %s )", sg_pcTempBuffer );
						SendSQL( pcSQL_CopyBillingRecords, pcWhere, "BIL.CPY" );
					};
				};
			};
		};

		// all is prepared, now start doing the job
		//GetNewBillingRecords
	};
	*/

/*	// get all records from current BILTAB that are marked as new
	// put them into BILTAB_TOR which is a link to a table in another database
	// reset send flags and set the senddate as current datetime 

	// 1.1	if BILTAB.IFST=N|E -> copy record to BILTAB_TOR
	// 1.2	set BILTAB.IFST=I, set BILTAB_TOR.IFST=N	*/
/*
	Table_Destroy( &tableBilling );
	Chain_Destroy( &chainBillingRecords_FieldNames );
	EXITSQL( pcSQL_GetNewBillingRecords );
*/

	EXITSQL( pcSQL_CopyBillingRecords );
}

/******************************************************************************/
/* create billing records
/******************************************************************************/
static int CreateBillingRecords( char* pcFURNs )
{
	TraceLine( "CreateBillingRecords()  START:" );
	if( !pcFURNs )
	{
		TraceLine( "  error: cannot run because no URNOs specified" );
		return RC_FAIL ;
	};

	if( !GHS_Initialize() )		/* init ok */
	{
		TraceLine( "  urnos=%s", pcFURNs );
		GHS_LoadFlightRecords( "@@FLNUs", pcFURNs );
		GHS_LoadFreightRecords( "@@FLNUs", pcFURNs );
		GHS_LoadGPURecords( "@@FLNUs", pcFURNs );
		GHS_Billing();		/* generates its own tables by the loaded data */
		GHS_SaveBillingRecords( "BILTAB" );
	};
	GHS_Destroy();

	TraceLine( "CreateBillingRecords()  END" );
	return RC_SUCCESS ;
}

/******************************************************************************/
/* compare with existing billing records
/******************************************************************************/
static int UpdateBillingRecords( char* pcFURNs )
{
	int result = 0 ;
	TraceLine( "UpdateBillingRecords()  START:" );

	if( !pcFURNs || !*pcFURNs )
	{
		TraceLine( "  error: cannot run because no URNOs specified" );
		return RC_FAIL ;
	};

	IF_INITSQL( pcSQL_UpdateBillingRecord, "UpdateBillingRecord.sql", 3 )
	/*  up to now init process works fine */
	{
		TraceLine( "  urnos=%s", pcFURNs );
		if( !GHS_Initialize() )		/* init ok */
		{
			GHS_LoadFlightRecords( "@@FLNUs", pcFURNs );
			GHS_LoadFreightRecords( "@@FLNUs", pcFURNs );
			GHS_LoadGPURecords( "@@FLNUs", pcFURNs );
			GHS_Billing();		/* generates its own tables by the loaded data */

			/* if( DebuggingPathSet() )
				Table_ToFile( &tableFlight, MakeMyDebugOutputFilename( "BIL.tab" ) ); */
			GHS_CheckForChangedBillings( pcFURNs );
		};
		GHS_Destroy();
	};
	EXITSQL( pcSQL_UpdateBillingRecord );

	TraceLine( "UpdateBillingRecords()  END" );
	return RC_SUCCESS ;
}


/******************************************************************************/
/* get urnos off all flights older than n month
/******************************************************************************/
char* pcSQL_GetBackDatedFlights = 0 ;
SChain chainBackDatedFlights_FieldNames ;
STable tableBackDatedFlights ;
unsigned int m_uiUrnosPerRequest = 1 ;/* 100 ;		// update 100 urnos each turn maximal */
static int GetBackDatedFlights( unsigned int months, unsigned int days )
{
	int result = 0 ;
	TraceLine( "GetBackDatedFlights() for %d months and %d days  START:", months, days );

	IF_INITSQL( pcSQL_GetBackDatedFlights, "GetBackDatedFlights.sql", 8 )
	{
		char* pc = 0 ;
		unsigned int uiFirstRow = 0 ;
		char* pcUrnos = 0 ;
		int imax = m_uiUrnosPerRequest ;
		char cBuffer[ 1024 * 10 ];

		char cTime[10];
		RemoveSingleLineCommentsSQL( pcSQL_GetBackDatedFlights );
		RemoveEmptyLines( pcSQL_GetBackDatedFlights );

		sprintf( cTime, "%d", months );
		String_Replace( pcSQL_GetBackDatedFlights, "@@MONTHS", cTime );

		sprintf( cTime, "%d", days );
		String_Replace( pcSQL_GetBackDatedFlights, "@@DAYS", cTime );

		INITFIELDNAMES( chainBackDatedFlights_FieldNames, pcSQL_GetBackDatedFlights );
		LoadRecords( pcSQL_GetBackDatedFlights, 0, &tableBackDatedFlights, &chainBackDatedFlights_FieldNames, "AFT.BKDT" );
		TraceLine( "  back dated flights, loaded %d records", tableBackDatedFlights.Records.ulLength );

		if( imax > tableBackDatedFlights.Records.ulLength ) imax = tableBackDatedFlights.Records.ulLength ;
		while( pcUrnos = Table_ColumnToString( &tableBackDatedFlights, "URNO", ", ", 0, cBuffer, uiFirstRow, imax ) )
		{	
			TraceLine( "  dealing with %d records starting at %d", m_uiUrnosPerRequest, uiFirstRow );
			TraceLine( "    URNOS='%s'", pcUrnos );
			result = UpdateBillingRecords( pcUrnos );
			if( result != RC_SUCCESS )
			{
				TraceLine( "  abort processing" );
				break ;
			};
			uiFirstRow += m_uiUrnosPerRequest ;
			result = RC_SUCCESS ;
		};

		Table_Destroy( &tableBackDatedFlights );
	};
	EXITSQL( pcSQL_GetBackDatedFlights );

	TraceLine( "GetBackDatedFlights()  END  with result=%d  ", result );
	return result ;
}
