#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/WAW/WAW_Server/Base/Server/Interface/bashdl.c 1.1 2006/08/25 17:48:05SGT jim Exp  $";
#endif /*_DEF_mks_version*/
/********************************************************************************/
/*                                                                            	*/
/* ABB ACE/FC Program bashdl.c                                               	*/
/*                                                                            	*/
/* Author      : Massimiliano Franceschini		                              	*/
/* Date        : 28/04/2003  		               		        				*/
/* Version     : 1.5	                                 		                */
/* Description : Processo integrato nel sistema Ufis con il compito di			*/
/* 						aggiornare la tabella BASTAB in Varsavia.				*/
/********************************************************************************/
/* Update history :   Prima stesura del 24/02/2003	               		      	*/
/* 18/03/03 : Modificati dei parametri nel file.cfg e rispettive variabili.		*/
/* 20/03/03 : Tradotte in Inglese tutti i messaggi di DEBUG e TRACE.			*/
/* 24/03/03 : Aggiunti alcuni parametri al File.cfg. Inserita la possibilita  	*/
/*					di eseguire eventi sul DB sia tramite SqlIf che Sqlhdl.		*/
/* 25/03/03 : Aggiornato secondo il relativo documento.							*/
/* 26/03/03 : Aggiornato secondo il relativo documento.							*/
/* 28/04/03 : Aggiunta la conversione della data in GMT nella funzione			*/
/*			  LoadStructFromBuffer(); Modificata molto la HandleGpatab();		*/
/* xx/05/03 : Inserita nuova funzione SearchInAfttab() ed eliminata la			*/ 
/*            SearchInTable().													*/
/* 05/05/03 : Loreto Cardarelli: Modificata la InsertBastab, occorre inserire 	*/
/*            anche il campo ADDR e nel campo TEXT va inserito solo i remark  	*/
/********************************************************************************/
/*                                                                            	*/
/* source-code-control-system version string                                  	*/
/*static char sccs_version[] = "UFIS 4.3 (c) Softlab bashdl 1.5.2 06/05/03 17.33-MF";*/
/********************************************************************************/
#include "db_if.h"
#include "ugccsma.h"
#include "bashdl.h"

/******************************************************************************/
/*                                                                            */
/* MAIN del processo bashdl                                                  	*/
/*                                                                            */
/******************************************************************************/
MAIN
{
  int	ilRC = RC_SUCCESS;			/* Return code	*/
  int	ilCnt = 0;
	
  INITIALIZE;			/* General initialization	*/
  /*dbg(TRACE,"<MAIN>: Version <%s>",sccs_version);*/
  dbg(TRACE,"<MAIN>: Version <%s>",mks_version);
  /* Attach to the MIKE queues */
  do
  {
    ilRC = init_que();
    if(ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"<MAIN> init_que() failed! waiting 6 sec ...");
      sleep(6);
      ilCnt++;
    }/* end of if */
    
  }while((ilCnt < 10) && (ilRC != RC_SUCCESS));
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"<MAIN> init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}else{
		dbg(TRACE,"<MAIN> init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */
	dbg(TRACE," initialising DB...");
   do
   {
       ilRC = init_db();
       if(ilRC != RC_SUCCESS)
       {
           dbg(TRACE,"...DB connect failed.");
           check_ret(ilRC);
           sleep(6);
           ilCnt++;
           if(ilCnt<10)
              dbg(TRACE,"...%d attempt(s) so far, trying again!!",ilCnt);
       }
       
   }while((ilCnt<10) && (ilRC!=RC_SUCCESS));
    
   if (ilRC!=RC_SUCCESS)
   {
      dbg(TRACE,"<MAIN> bashdl process %i: init_db failed!",mod_id);           
      Terminate(60);
   }
   else
      dbg(TRACE,"<MAIN> init_db() OK!!");
   
	sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),argv[0]);
	ilRC = TransferFile(cgConfigFile);
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */
	
	/* Trasferimento del file.cfg */
	/* uncomment if necessary */
	memset(cgConfigFile, 0x00, sizeof(cgConfigFile));
	sprintf(cgConfigFile,"%s/%s%i.cfg",getenv("CFG_PATH"),argv[0],mod_id);
	memset(szgProcName, 0x00, sizeof(szgProcName));
   strcpy(szgProcName, argv[0]);
   dbg(TRACE,"<MAIN> Process Name:(%s), Configuration File:(%s)",szgProcName,cgConfigFile);
   
   ilRC = TransferFile(cgConfigFile);
	if(ilRC != RC_SUCCESS) 
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} 
	ilRC = SendRemoteShutdown(mod_id);
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */
	
	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */
	
	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"<MAIN> initializing ...");
		if(igInitOK == FALSE)
		{
			ilRC = Init_bashdl();			
			if(ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"* <MAIN> Init_bashdl(): Inizialization FAILED *");
				HandleError(NULL, CFG_FILE_ERROR);
			} 
		}
	} 
	else
		Terminate(0);
	
	dbg(TRACE,"*********** <MAIN>: Inizialization OK ************");
	
	/* Loop Forever */
   for(;;)
   {
	   dbg(TRACE,"<MAIN>: LOOP => Waiting Message on Primary Queue...");
      ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
      /* depending on the size of the received item  */
      /* a realloc could be made by the que function */
      /* so do never forget to set event pointer !!! */
      prgEvent = (EVENT *) prgItem->text;
				
      if( ilRC == RC_SUCCESS )
      {
        	/* Acknowledge the item */
        	ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
        	if( ilRC != RC_SUCCESS ) 
        	{
	  			/* handle que_ack error */
	  			HandleQueErr(ilRC);
        	} 
	
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				dbg(DEBUG,"<MAIN>: Comando HSB_COMING_UP.");
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				dbg(DEBUG,"<MAIN>: Comando HSB_DOWN.");
				Terminate(0);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	SHUTDOWN	:
				/* process shutdown - maybe from cutil */		
				dbg(TRACE,"<MAIN>: Comando SHUTDOWN send from = %ld",prgItem->originator);
				Terminate(0);
				break;
			case	RESET		:
				ilRC = Reset();
				break;
										
			case	EVENT_DATA	:
				if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
				{
   		   	/******************************************************/
   		   	/******************************************************/
   		   	/* Gestione dei dati ricevuti: "INIZIO del PROGRAMMA" */
   		   	/******************************************************/
   		   	/******************************************************/
				  	if(igInitOK == TRUE)
				  		HandleData();
				  	else
				  		HandleError(NULL, CFG_FILE_ERROR);
				}
				else
				{
				  dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
				  DebugPrintItem(TRACE,prgItem);
				  DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
							
			case	TRACE_ON :
				dbg(DEBUG,"<MAIN>: Comando TRACE_ON.");
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg(DEBUG,"<MAIN>: Comando TRACE_OFF.");
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} /* end if(RC_SUCCESS) */
  		else 
  		{
			/* Handle queuing errors */
		  	HandleQueErr(ilRC);
  		} 
  } /* end for */
} /* end of MAIN */


/******************************************************************************/
/* Reset():                                                          */
/******************************************************************************/
static int Reset()
{
  	int ilRC = RC_SUCCESS;				/* Return code */
  		
  	dbg(TRACE,"<Reset> now resetting...");
	
   debug_level = TRACE;
   
   igInitOK = FALSE;
   
   /* Reinizializzo il processo */
	ilRC = Init_bashdl();
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"<Reset> Init_bashdl(): Re-inizialization FAILED");
		HandleError(NULL, CFG_FILE_ERROR);
	}
      	
  	return ilRC;	
} /* end of Reset */


/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ilSec)
{	
  	dbg(TRACE,"<Terminate> **************** START *****************");
  	
  	dbg(TRACE,"<Terminate> Logging off from Database.");
   sql_if(NOACTION, NULL, "LOGOFF", NULL);
   
   sleep(ilSec);
  	
  	dbg(TRACE,"<Terminate> NOW LEAVING.............................");
  	dbg(TRACE,"<Terminate> ***************** END ******************");
  
  	exit(0);	
} /* end of Terminate */


/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	int	ilRC = RC_SUCCESS;
	
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;
} /* end of HandleQueErr */


/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	
		if( ilRC == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRC != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRC);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleData();
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
 				HandleData();
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(0);
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				HandleData();
				break;	
			case	SHUTDOWN	:
				Terminate(0);
				break;
						
			case	RESET		:
				ilRC = Reset();
				break;
							
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				HandleData();
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRC);
		} /* end else */
	} while (ilBreakOut == FALSE);
	if(igInitOK == FALSE)
	{
			ilRC = Init_bashdl();
			if(ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"* <MAIN> Init_bashdl(): Inizialization FAILED *");
				HandleError(NULL, CFG_FILE_ERROR);
			}
	}
} /* end of HandleQueues */
	
	
/******************************************************************************/
/* HandleData(): routine principale del processo, in cui viene letto il msg   */
/*               residente nella coda e viene interpretato il comando ricevuto*/
/******************************************************************************/
static void HandleData()
{
  	int ilRC = RC_SUCCESS;
  	char     *pclSelection   = NULL;
  	char     *pclFields      = NULL;
  	char     *pclData        = NULL;
  	BC_HEAD  *prlBCHead      = NULL;
  	CMDBLK   *prlCmdblk      = NULL;
  	
  	dbg(DEBUG,"<HandleData> *************** START *****************");
  	
  	if (prgEvent == NULL)
  	{
  		/* Segnalo l'errore terminando il processo */
  	  	HandleError(NULL, EVENT_ERROR);
  	  	return;
  	}	
  	
  	prlBCHead = (BC_HEAD*)((char *)prgEvent + sizeof(EVENT));
  	
  	if (prlBCHead->rc != RC_SUCCESS && prlBCHead->rc != NETOUT_NO_ACK)
  	{
  	  /* this is an error */
  	  dbg(DEBUG,"<HandleData> BC_HEAD->rc: %d",prlBCHead->rc);
  	  dbg(DEBUG,"<HandleData>ERROR-MESSAGE: %s",prlBCHead->data);
  	  /* set local pointer */
  	  prlCmdblk    = (CMDBLK*)((char *)prlBCHead->data + strlen(prlBCHead->data) +1);
  	  pclSelection = (char*)prlCmdblk->data;
  	  pclFields    = (char*)pclSelection + strlen(pclSelection) + 1;
  	  pclData      = (char*)pclFields + strlen(pclFields) + 1;
  	  *pclData     = 0x00;
  	}
  	else
  	{
  	  /* set local pointer */
  	  prlCmdblk    = (CMDBLK*)((char *)prlBCHead->data);
  	  pclSelection = (char*)prlCmdblk->data;
  	  pclFields    = (char*)pclSelection + strlen(pclSelection) + 1;
  	  pclData      = (char*)pclFields + strlen(pclFields) + 1;
  	}
  	
  	dbg(DEBUG,"<HandleData> HID: Selection: <%s>",pclSelection);
  	dbg(DEBUG,"<HandleData> HID: Fields   : <%s>",pclFields);
 	/* dbg(DEBUG,"<HandleData> HID: Data     : <%s>",pclData);*/
  	
  	dbg(DEBUG,"<HandleData> Command = %s \tORG = %ld\tEvent Originator = %ld",prlCmdblk->command,prgItem->originator,prgEvent->originator);
  	/****************************************************************************/
  	/***************  ANALISI E GESTIONE DEL COMANDO RICEVUTO      **************/
  	/****************************************************************************/  	
   dbg(TRACE,"\n\t\t\t\t(((((((((((((((((((((((( START ))))))))))))))))))))))))");
   
  	/* Gestisco il messaggio Bas Ricevuto */ 					    			
  	BasDataHdl(pclData);  		
		
  	dbg(TRACE,"\n\t\t\t\t========================= END =========================");
} /* end of HandleData() */


/******************************************************************************/
/* Init_bashdl(): Legge tutto il file di Configurazione.  					  		*/
/*----------------------------------------------------------------------------*/
/* Ritorna: RC_SUCCESS o RC_FAIL.															*/
/******************************************************************************/
int Init_bashdl()
{
  	int ilDebugLevel = 0;
   
   dbg(DEBUG,"<Init_bashdl>********* START *********");
   
   /* Reset di tutte le variabili globali */
   memset(szgStringPlaceNum, 0x00, sizeof(szgStringPlaceNum));
   memset(szgHomeAirport, 0x00, sizeof(szgHomeAirport));
   memset(szgSqlhdlInsertCmd, 0x00, sizeof(szgSqlhdlInsertCmd));
   memset(szgSqlhdlUpdateCmd, 0x00, sizeof(szgSqlhdlUpdateCmd));
  	memset(szgDestinationProcessName, 0x00, sizeof(szgDestinationProcessName));
  	
  	igGpuRangeTimeFirst = 0;
  	igGpuRangeTimeSecond = 0;
  	igDBEventSystemFlag = 0;
  	igDestinationProcessID = 0;
  	
	/**********************************************************************/
  	/* following read configuration - gpu_range_time_first         	      */
  	/**********************************************************************/
  	if ((iGetConfigEntry(cgConfigFile, "PROCESS_ID", "gpu_range_time_first", CFG_INT, (char *)&igGpuRangeTimeFirst) != RC_SUCCESS) ||
  		(igGpuRangeTimeFirst == 0))
	{
		dbg(TRACE,"<Init_bashdl> gpu_range_time_first Missing => inserted Default Value(%i)", GPU_RANGE_TIME_DEFAULT);
		igGpuRangeTimeFirst = GPU_RANGE_TIME_DEFAULT;		
	}
	
	dbg(TRACE," gpu_range_time: <%i>", igGpuRangeTimeFirst);
	
	/**********************************************************************/
  	/* following read configuration - gpu_range_time_first         	      */
  	/**********************************************************************/
  	if ((iGetConfigEntry(cgConfigFile, "PROCESS_ID", "gpu_range_time_second", CFG_INT, (char *)&igGpuRangeTimeSecond) != RC_SUCCESS) ||
  		(igGpuRangeTimeSecond == 0))
	{
		dbg(TRACE,"<Init_bashdl> gpu_range_time_second Missing => inserted Default Value(%i)", GPU_RANGE_TIME_SECOND_DEFAULT);
		igGpuRangeTimeSecond = GPU_RANGE_TIME_SECOND_DEFAULT;		
	}
	
	dbg(TRACE," gpu_range_time: <%i>", igGpuRangeTimeSecond);
	
	/**********************************************************************/
  	/* following read configuration - string_for_gpu_msg                  */
  	/**********************************************************************/
  	if ((iGetConfigEntry(cgConfigFile, "PROCESS_ID", "string_for_gpu_msg",CFG_STRING,szgStringGpuMsg) != RC_SUCCESS) ||
  	   (strlen(szgStringGpuMsg) < 2))
  	{  	  
		dbg(TRACE,"<Init_bashdl> string_for_gpu_msg Missing => inserted Default Value(%s)", STRING_GPU_MSG_DEFAULT);
		strcpy(szgStringGpuMsg, STRING_GPU_MSG_DEFAULT);
  	}
  	
  	dbg(TRACE," string_for_gpu_msg: <%s>", szgStringGpuMsg);
  	
  	/**********************************************************************/
  	/* following read configuration - string_for_gpu_on                   */
  	/**********************************************************************/
  	if ((iGetConfigEntry(cgConfigFile, "PROCESS_ID", "string_for_gpu_on",CFG_STRING,szgStringGpuOn) != RC_SUCCESS) ||
  	   (strlen(szgStringGpuOn) < 2))
  	{  	  
		dbg(TRACE,"<Init_bashdl> string_for_gpu_on Missing => inserted Default Value(%s)", STRING_GPU_ON_DEFAULT);
		strcpy(szgStringGpuOn, STRING_GPU_ON_DEFAULT);
  	}
  	
  	dbg(TRACE," string_for_gpu_on: <%s>", szgStringGpuOn);
  	
  	/**********************************************************************/
  	/* following read configuration - string_for_gpu_off                  */
  	/**********************************************************************/
  	if ((iGetConfigEntry(cgConfigFile, "PROCESS_ID", "string_for_gpu_off",CFG_STRING,szgStringGpuOff) != RC_SUCCESS) ||
  	   (strlen(szgStringGpuOff) < 2))
  	{  	  
		dbg(TRACE,"<Init_bashdl> string_for_gpu_off Missing => inserted Default Value(%s)", STRING_GPU_OFF_DEFAULT);
		strcpy(szgStringGpuOff, STRING_GPU_OFF_DEFAULT);
  	}
  	
  	dbg(TRACE," string_for_gpu_off: <%s>", szgStringGpuOff);
  	
  	/**********************************************************************/
  	/* following read configuration - string_before_place_num             */
  	/**********************************************************************/
  	if ((iGetConfigEntry(cgConfigFile, "PROCESS_ID", "string_before_place_num",CFG_STRING,szgStringPlaceNum) != RC_SUCCESS) ||
  	   (strlen(szgStringPlaceNum) < 2))
  	{  	  
		dbg(TRACE,"<Init_bashdl> string_before_place_num Missing => inserted Default Value(%s)", STRING_PLACE_NUM_DEFAULT);
		strcpy(szgStringPlaceNum, STRING_PLACE_NUM_DEFAULT);
  	}
  	
  	dbg(TRACE," string_before_place_num: <%s>", szgStringPlaceNum);
  	
  	/**********************************************************************/
  	/* following read configuration - home_airport                        */
  	/**********************************************************************/
  	if ((iGetConfigEntry(cgConfigFile, "PROCESS_ID", "home_airport",CFG_STRING,szgHomeAirport) != RC_SUCCESS) ||
  	   (strlen(szgHomeAirport) < 3))
  	{  	  
		dbg(TRACE,"<Init_bashdl> home_airport Missing => inserted Default Value(%s)", HOME_AIRPORT_DEFAULT);
		strcpy(szgHomeAirport, HOME_AIRPORT_DEFAULT);   
  	}
  	
  	dbg(TRACE," home_airport: <%s>", szgHomeAirport);
  	
  	/*********************************************************************/
	/* following read configuration - db_event_system_flag               */
	/*********************************************************************/
	if (iGetConfigEntry(cgConfigFile,"PROCESS_ID","db_event_system_flag",CFG_INT,(char *)&igDBEventSystemFlag) != RC_SUCCESS)
  	{	
    	dbg(TRACE,"<Init_bashdl> db_event_system_flag Missing => inserted Default Value(0-SQLHDL)");
    	igDBEventSystemFlag = SQLHDL_EVENT_SYSTEM;
   }
  
	dbg(TRACE," db_event_system_flag: <%i>", igDBEventSystemFlag);
	
	/*********************************************************************/
  	/* following read configuration - sqlhdl_insert_cmd			         */
  	/*********************************************************************/
  	if ((iGetConfigEntry(cgConfigFile,"PROCESS_ID","sqlhdl_insert_cmd",CFG_STRING,szgSqlhdlInsertCmd) != RC_SUCCESS) ||
  	   (strlen(szgSqlhdlInsertCmd) < 2))
  	{
  		dbg(TRACE,"<Init_bashdl> sqlhdl_insert_cmd Missing => inserted Default Value(%s)", INSERT_CMD_DEFAULT);
		strcpy(szgSqlhdlInsertCmd, INSERT_CMD_DEFAULT);
  	}  
  	
  	dbg(TRACE," sqlhdl_insert_cmd: <%s>", szgSqlhdlInsertCmd);
  	
	/*********************************************************************/
  	/* following read configuration - sqlhdl_update_cmd			         */
  	/*********************************************************************/
  	if ((iGetConfigEntry(cgConfigFile,"PROCESS_ID","sqlhdl_update_cmd",CFG_STRING,szgSqlhdlUpdateCmd) != RC_SUCCESS) ||
  	   (strlen(szgSqlhdlUpdateCmd) < 2))
  	{
  		dbg(TRACE,"<Init_bashdl> sqlhdl_update_cmd Missing => inserted Default Value(%s)", UPDATE_CMD_DEFAULT);
		strcpy(szgSqlhdlUpdateCmd, UPDATE_CMD_DEFAULT);
  	}  
  	
  	dbg(TRACE," sqlhdl_update_cmd: <%s>", szgSqlhdlUpdateCmd);
  	
	/**********************************************************************/
  	/* following read configuration - destination_process_ID              */
  	/**********************************************************************/
  	if ((iGetConfigEntry(cgConfigFile, "PROCESS_ID", "destination_process_ID",CFG_INT,(char *)&igDestinationProcessID) != RC_SUCCESS) ||
  		(igDestinationProcessID == 0))
  	{
		dbg(TRACE,"<Init_bashdl> destination_process_ID Missing => inserted Default Value(%i)", DEST_PROCESS_ID_DEFAULT);
		igDestinationProcessID = DEST_PROCESS_ID_DEFAULT;
  	}
  
  	dbg(TRACE," destination_process_ID: <%i>", igDestinationProcessID);
  	
	/*********************************************************************/
  	/* following read configuration - destination_process_name           */
  	/*********************************************************************/
  	if ((iGetConfigEntry(cgConfigFile,"PROCESS_ID","destination_process_name",CFG_STRING,szgDestinationProcessName) != RC_SUCCESS) ||
  	   (strlen(szgDestinationProcessName) < 3))
  	{
  		dbg(TRACE,"<Init_bashdl> destination_process_name Missing => inserted Default Value(%s)", DEST_PROCESS_NAME_DEFAULT);
		strcpy(szgDestinationProcessName, DEST_PROCESS_NAME_DEFAULT);
  	}  
  	
  	dbg(TRACE," destination_process_name: <%s>", szgDestinationProcessName);
  	
  	/*********************************************************************/
	/* following read configuration - debug_level                        */
	/*********************************************************************/
	if ((iGetConfigEntry(cgConfigFile,"PROCESS_ID","debug_level",CFG_INT,(char *)&ilDebugLevel) != RC_SUCCESS) ||
		(ilDebugLevel == 0))
  	{	
    	dbg(TRACE,"<Init_bashdl> debug_level Missing => inserted Default Value(8-TRACE)");    	
    	ilDebugLevel = 8;
   }
  
	dbg(TRACE," debug_level: <%i>", ilDebugLevel);
	/* Imposto lo stato di debug impostato nel file.cfg */  	   
  	if (ilDebugLevel == 7)
   	debug_level = DEBUG;
  	else
   	debug_level = TRACE;
   
   igInitOK = TRUE;	
  	dbg(DEBUG,"********* Init_bashdl() ended correctly *********");
  	return(RC_SUCCESS);
}


/******************************************************************************/
/* BasDataHdl(): gestisce il messaggio BAS ricevuto sulla coda primaria.		*/
/*----------------------------------------------------------------------------*/
/* Parametri: Data Buffer.																		*/
/******************************************************************************/
void BasDataHdl(char *pszlData)
{	
	int i;
	int ilRecNum = 0;
	int ilRecLen = 0;	
    BUFFER_STR *stlBuffer;
	BAS_DATA_STR *stlBasData;
		
	dbg(DEBUG,"<BasDataHdl> *************** START ***************");
	/****************************************************************/
   /* Ciclo leggendo tutto il buffer d'origine una riga alla volta */
   /****************************************************************/
	/* Verifico quanti comandi BAS ho ricevuto */
	ilRecNum = get_recordcount(pszlData, "\n");
	
	for(i=0; i<ilRecNum; i++)
	{
		stlBuffer = (BUFFER_STR *) malloc(sizeof(BUFFER_STR));
		stlBasData = (BAS_DATA_STR *)malloc(sizeof(BAS_DATA_STR));
		if (stlBuffer == NULL || stlBasData == NULL)
		{
			dbg(TRACE,"<BasDataHdl> Allocation memory failed! Exit msg lose!");
			return;
		}
		
		/* Estraggo l'iesima riga di comando BAS */
		if ((ilRecLen = get_record(pszlData, "\n", i+1, (char*)stlBuffer)) < BAS_DATA_LENGHT)
		{			
			dbg(TRACE,"<BasDataHdl> Invalid Msg Len (%i)", ilRecLen);
			/* Esco segnalando che il record e' vuoto */
			HandleError((void*)ilRecLen, RECORD_LEN_ERROR);
		}
		else
	  	{	/* Carico la struttura dati con le informazioni del BAS */
	  		LoadStructFromBuffer(stlBuffer, stlBasData);
	  		
	  		/* Inserisco un nuovo record nella BASTAB */
	  		if (InsertBastab(stlBasData) == RC_SUCCESS)
	  		{
	  			/* Ricerca la stringa "400Hz" nel testo del Remark */
	  			if (strstr(stlBasData->szRemark, szgStringGpuMsg) != NULL)
	  			{
	  				/* Gestisco la ricerca del volo e l'aggiornamento della GPATAB */
	  				if (HandleGpatab(stlBasData) == RC_FAIL)
	  					HandleError((void*)stlBuffer, GPU_MSG_ERROR);
	  			}
	  		}
	  	}
	  	free(stlBuffer);
	  	free(stlBasData);
	}
	
  	dbg(DEBUG,"<BasDataHdl> ********************** END *****************");
}


/******************************************************************************/
/* InsertBastab(): gestisce l'inserimento di un record nella BASTAB.				*/
/******************************************************************************/
int InsertBastab(BAS_DATA_STR *pszlBasData)
{
	int ilRC = RC_SUCCESS;
	char szlValuedList[1024];
	char szAddress[27];
			
	dbg(DEBUG,"<InsertBastab> ********************** START *****************");
	
	memset(szlValuedList, 0x00, sizeof(szlValuedList));
	memset(szAddress, 0x00, sizeof(szAddress));
	
/*	sprintf(szlValuedList, "%s,%s %s,%s", pszlBasData->szTime, pszlBasData->szRemark, pszlBasData->szAddress, pszlBasData->szStatus);
	
	if ((ilRC = HandleInsertUpdate("TIME,TEXT,ALTY", szlValuedList, szgSqlhdlInsertCmd, NULL, BASTAB_NAME)) != RC_SUCCESS)
    	dbg(TRACE,"<InsertBastab> FAILED Insert in BASTAB: Fields <%s> Values <%s>", "TIME,TEXT,ALTY", szlValuedList);
*/	
	
	/*strncpy(szlValuedList, pszlBasData->szTime, 15);
	strcat(szlValuedList, ",");
	strncat(szlValuedList, pszlBasData->szRemark, 66);
	strcat(szlValuedList, ",");
	strncat(szlValuedList, pszlBasData->szAddress, 26);
	strcat(szlValuedList, ",");
	strncat(szlValuedList, pszlBasData->szStatus, 17);*/
	
	strncpy(szAddress, pszlBasData->szAddress, 26);
	strcat(szAddress, "\0");
	sprintf(szlValuedList, "%s,%s,%s,%s", pszlBasData->szTime, pszlBasData->szRemark, szAddress, pszlBasData->szStatus);
	
	if ((ilRC = HandleInsertUpdate("TIME,TEXT,ADDR,STAT", szlValuedList, szgSqlhdlInsertCmd, NULL, BASTAB_NAME)) != RC_SUCCESS)
    	dbg(TRACE,"<InsertBastab> FAILED Insert in BASTAB: Fields <%s> Values <%s>", "TIME,TEXT,ADDR,STAT", szlValuedList);

	dbg(DEBUG,"<InsertBastab> ********************** END ************(%i)", ilRC);
	return ilRC;
}


/******************************************************************************/
/* HandleGpatab(): gestisce l'aggiornamento della GPATAB sulla base delle		*/
/*						ricerche sulla AFTTAB.													*/
/*----------------------------------------------------------------------------*/
/* Parametri: struttura del BAS message													*/
/******************************************************************************/
int HandleGpatab(BAS_DATA_STR *pszlBasData)
{
	int ilRC = RC_SUCCESS;
	char szlAppTime[7];
	char szlRangeUpFirst[15];
	char szlRangeUpSecond[15];
	char szlRangeDownFirst[15];
	char szlRangeDownSecond[15];
	char szlWhere[516];
  	char szlValues[128];
	char szlRecordFound[2048];
	char szlPlaceNum[8];
	char szlArrUrno[11];
	char szAppoPar[11];
	GPATAB_FIELD_STR stlGpaRec;
	char *pszlString = NULL;
	
	dbg(DEBUG,"<HandleGpatab> ********** START **********");
				
	/*************************************************/
	/* Estraggo il numero della Piazzola dal BAS msg */
	if ((pszlString = strstr(pszlBasData->szRemark, szgStringPlaceNum)) != NULL)
	{
		/* Avanzo il puntatore fino alla prima cifra del numero della Piazzola */
		pszlString = (pszlString + strlen(szgStringPlaceNum) + 1);
		memset(szlPlaceNum, 0x00, sizeof(szlPlaceNum));
		memset(szAppoPar, 0x00, sizeof(szAppoPar));
		sprintf(szlPlaceNum, "%2.2s", pszlString);
		str_trm_all(szlPlaceNum, " ", TRUE);
		if (strlen(szlPlaceNum) == 1)
			sprintf(szAppoPar, "00%s", szlPlaceNum);
		if (strlen(szlPlaceNum) == 2)
			sprintf(szAppoPar, "0%s", szlPlaceNum);
		memset(szlPlaceNum, 0x00, sizeof(szlPlaceNum));
		sprintf(szlPlaceNum, "%s", szAppoPar);							
	}
	
	/* Verifico se ho trovato il numero della piazzola */
	if ((pszlString == NULL ) || (strlen(szlPlaceNum) == 0))
	{	
		/* Esco segnalando l'errore */
		HandleError((void*)pszlBasData->szRemark, PLACE_ERROR);
		return RC_FAIL;
	}
	
	dbg(DEBUG,"<HandleGpatab> PlaceNum(%s) Remark(%s)", szlPlaceNum, pszlBasData->szRemark);
		
	
	/* Compongo la stringa per gestire il Range di tempo con la sysdate */
	memset(szlRangeUpFirst, 0x00, sizeof(szlRangeUpFirst));
	memset(szlRangeUpSecond, 0x00, sizeof(szlRangeUpSecond));
	memset(szlRangeDownFirst, 0x00, sizeof(szlRangeDownFirst));
	memset(szlRangeDownSecond, 0x00, sizeof(szlRangeDownSecond));
	memset(szlAppTime, 0x00, sizeof(szlAppTime));
	/*************************************************/
	/* How many minute should Add/Sub to BAS Time msg */
	if (igGpuRangeTimeFirst >= 1440)
		sprintf(szlAppTime, "%2.2i%2.2i%2.2i", (igGpuRangeTimeFirst/1440), ((igGpuRangeTimeFirst%1440)/60), ((igGpuRangeTimeFirst%1440)%60));
	else if (igGpuRangeTimeFirst >= 60)
		sprintf(szlAppTime, "00%2.2i%2.2i", (igGpuRangeTimeFirst/60), (igGpuRangeTimeFirst%60));
	else
		sprintf(szlAppTime, "0000%2.2i", igGpuRangeTimeFirst);

	dbg(DEBUG,"<HandleGpatab> Gap On Time (%s) min(%i)", pszlBasData->szTime, igGpuRangeTimeFirst);
	
	AddDate(pszlBasData->szTime, szlAppTime, szlRangeUpFirst, SUBTRACTION);	
	AddDate(pszlBasData->szTime, szlAppTime, szlRangeDownSecond, ADDICTION);
	
	memset(szlAppTime, 0x00, sizeof(szlAppTime));
	/*************************************************/
	/* How many minute should Add/Sub to BAS Time msg */
	if (igGpuRangeTimeSecond >= 1440)
		sprintf(szlAppTime, "%2.2i%2.2i%2.2i", (igGpuRangeTimeSecond/1440), ((igGpuRangeTimeSecond%1440)/60), ((igGpuRangeTimeSecond%1440)%60));
	else if (igGpuRangeTimeSecond >= 60)
		sprintf(szlAppTime, "00%2.2i%2.2i", (igGpuRangeTimeSecond/60), (igGpuRangeTimeSecond%60));
	else
		sprintf(szlAppTime, "0000%2.2i", igGpuRangeTimeSecond);

	dbg(DEBUG,"<HandleGpatab> Gap On Time (%s) min(%i)", pszlBasData->szTime, igGpuRangeTimeSecond);
	
	AddDate(pszlBasData->szTime, szlAppTime, szlRangeDownFirst, SUBTRACTION);	
	AddDate(pszlBasData->szTime, szlAppTime, szlRangeUpSecond, ADDICTION);
	
	dbg(DEBUG,"<HandleGpatab> Original Date: <%s>", pszlBasData->szTime);
	dbg(DEBUG,"<HandleGpatab> New Date with first parameter: (-)<%s> == (+)<%s>", szlRangeUpFirst, szlRangeDownSecond);
	dbg(DEBUG,"<HandleGpatab> New Date with second parameter: (-)<%s> == (+)<%s>", szlRangeDownFirst, szlRangeUpSecond);
			
	/***************************************************************************/
	/*					 Ricerco il volo in Arrivo sulla AFTTAB 							*/
	/***************************************************************************/	
	memset(szlRecordFound, 0x00, sizeof(szlRecordFound));
	/* Ricerco il volo in Arrivo sulla AFTTAB */	
	if (SearchInAfttab(szlRangeUpFirst, szlRangeDownFirst, szlRangeUpSecond, szlRangeDownSecond, pszlBasData->szTime, szlPlaceNum, szlRecordFound) == RC_SUCCESS)
	{
		dbg(DEBUG,"<HandleGpatab> AFTTAB Flight FOUND-> (%s)", szlRecordFound);
		memset(szlArrUrno, 0x00, sizeof(szlArrUrno));
				
		/* Inserisco una virgola per dividere un campo dall'altro */
  		BuildItemBuffer(szlRecordFound, NULL, 7, ",");
		
  		/* Prelevo i dati del record suddividendoli fra loro. */
  		GetCharField(szlRecordFound, 1, szlArrUrno);
  		dbg(DEBUG,"<HandleGpatab> URNO(%s)", szlArrUrno);
   	/***************************************************************************/
		/*					 	 Verifico se si tratta di un GPU ON msg 						*/
		/***************************************************************************/
   	if (strstr(pszlBasData->szStatus, szgStringGpuOn) != NULL)
		{ 	 		
			dbg(DEBUG,"<HandleGpatab> GPU Status is ON-> Status(%s)", pszlBasData->szStatus);
			/********************************************/
			/* Compongo la WHERE Condition sulla GPATAB */
			memset(szlWhere, 0x00, sizeof(szlWhere));
			sprintf(szlWhere, "FLNU='%s' ORDER BY ASEQ DESC", szlArrUrno);
			dbg(DEBUG,"<HandleGpatab> GPATAB WHERE(%s)", szlWhere);
			
			/******************************************************/
			/* Ricerco l'eventuale record precedente sulla GPATAB */
			memset(szlRecordFound, 0x00, sizeof(szlRecordFound));
			if (SearchRecordInGpatab(szlWhere, szlRecordFound, &stlGpaRec) == RC_SUCCESS)
  	 		{
  	 			dbg(DEBUG,"<HandleGpatab> ON: Record FOUND in GPATAB-> URNO,ASEQ,GAAE(%s)", szlRecordFound);
  	 			/******************************************************/
  	 			/* Verifico se il record e' stato chiuso => la data di OFF e' valorizzata */
  	 			if (strlen(stlGpaRec.szGaae) == 14)
  	 			{
  	 				/* Verifico se ho gia' inserito il massimo numero di GPU USAGE == 3 */
  	 				if ((atoi(stlGpaRec.szAseq) + 1) > MAX_NUM_GPU_USAGE)
					{	
						/* Raggiunto il numero massimo di GPU record per il volo */
						HandleError((void*)stlGpaRec.szUrno, GPU_MSG_NUM_ERROR);
						ilRC = RC_FAIL;
					}
  	 				else
  	 				{	/* Posso inserire un altro GPU USAGE nella GPATAB */
  	 					/* Carico i valori dei campi "ASEQ,FLNU,GAAB" da inserire */
  	 					memset(szlValues, 0x00, sizeof(szlValues));
						sprintf(szlValues,"%i,%s,%s", (atoi(stlGpaRec.szAseq) + 1), szlArrUrno, pszlBasData->szTime);
					
						/******************************************/
						/* Inserisco il nuovo record nella GPATAB */
						if (HandleInsertUpdate("ASEQ,FLNU,GAAB", szlValues, szgSqlhdlInsertCmd, NULL, GPATAB_NAME) != RC_SUCCESS)
							dbg(TRACE,"<HandleGpatab> FAILED Insert in GPATAB: Fields <%s> Values <%s>", "ASEQ,FLNU,GAAB", szlValues);
  	 				}
  	 			}
  	 			else
  	 			{  	 				
					/* Record trovato in GPATAB non e' chiuso => GPU = ON */
					HandleError((void*)szlRecordFound, GPU_STILL_ON_WARNING);
					
					/* Aggiorno il record con l'ora di inizio utilizzo del GPU */
					if (HandleInsertUpdate("GAAB", pszlBasData->szTime, szgSqlhdlUpdateCmd, stlGpaRec.szUrno, GPATAB_NAME) != RC_SUCCESS)
						dbg(TRACE,"<HandleGpatab> FAILED Update in GPATAB: Fields <%s> Values <%s> URNO(%s)", "GAAB", pszlBasData->szTime, stlGpaRec.szUrno);
  	 			}
			}
			else
			{	/* Inserisco un nuovo record con ASEQ = 1 */
				/* Carico i valori dei campi "ASEQ,FLNU,GAAB" da inserire */
				memset(szlValues, 0x00, sizeof(szlValues));
				sprintf(szlValues,"1,%s,%s", szlArrUrno, pszlBasData->szTime);
			
				/******************************************/
				/* Inserisco il nuovo record nella GPATAB */
				if (HandleInsertUpdate("ASEQ,FLNU,GAAB", szlValues, szgSqlhdlInsertCmd, NULL, GPATAB_NAME) != RC_SUCCESS)
					dbg(TRACE,"<HandleGpatab> FAILED Insert in GPATAB: Fields <%s> Values <%s>", "ASEQ,FLNU,GAAB", szlValues);
			}
		} /* end of if(GPU_ON) */
			/***************************************************************************/
			/*					 	 Verifico se si tratta di un GPU OFF msg 						*/
			/***************************************************************************/
		else if (strstr(pszlBasData->szStatus, szgStringGpuOff) != NULL)
		{
			dbg(DEBUG,"<HandleGpatab> GPU Status is OFF-> Status(%s)", pszlBasData->szStatus);
			/********************************************/
			/* Compongo la WHERE Condition sulla GPATAB cercando di estrarre */
			/* record inserito in ordine di tempo => ASEQ piu' alto.			  */
			memset(szlWhere, 0x00, sizeof(szlWhere));
			sprintf(szlWhere, "FLNU='%s' ORDER BY ASEQ DESC", szlArrUrno);
			dbg(DEBUG,"<HandleGpatab> GPATAB WHERE(%s)", szlWhere);
			
			/******************************************************/
			/* Ricerco l'eventuale record precedente sulla GPATAB */
			memset(szlRecordFound, 0x00, sizeof(szlRecordFound));
  	 		if (SearchRecordInGpatab(szlWhere, szlRecordFound, &stlGpaRec) == RC_SUCCESS)
  	 		{
  	 			dbg(DEBUG,"<HandleGpatab> OFF: Record FOUND in GPATAB-> URNO,ASEQ,GAAE(%s)", szlRecordFound);
  	 			/******************************************************/
  	 			/* Verifico se il record e' stato chiuso => la data di OFF e' valorizzata */
  	 			if (strlen(stlGpaRec.szGaae) == 14)
  	 			{
					/* Record trovato in GPATAB e' gia' chiuso => GPU = OFF */
					HandleError((void*)szlRecordFound, GPU_ALREADY_OFF_WARNING);
  	 			}
  	 			
				/* Aggiorno il record con l'ora di fine utilizzo del GPU */
				if (HandleInsertUpdate("GAAE", pszlBasData->szTime, szgSqlhdlUpdateCmd, stlGpaRec.szUrno, GPATAB_NAME) != RC_SUCCESS)
					dbg(TRACE,"<HandleGpatab> FAILED Update in GPATAB: Fields <%s> Values <%s> URNO(%s)", "GAAE", pszlBasData->szTime, stlGpaRec.szUrno);
			}
			else
			{
				/* Record NON trovato in GPATAB => Nuovo insert con OFF ma privo di ON */
				HandleError((void*)szlWhere, GPU_REC_NOT_FOUND_WARNING);
					
				/* Record precedente NON trovato=> nessun Update ma nuovo Insert */
				/* Carico i valori dei campi "ASEQ,FLNU,GAAE" da inserire */
				memset(szlValues, 0x00, sizeof(szlValues));
				sprintf(szlValues,"1,%s,%s", szlArrUrno, pszlBasData->szTime);
				dbg(DEBUG,"<HandleGpatab> ASEQ,FLNU,GAAE Values for New record in GPATAB(%s)", szlValues);
				
				/***************************************************/
				/* Inserisco comunque un nuovo record nella GPATAB */
				if (HandleInsertUpdate("ASEQ,FLNU,GAAE", szlValues, szgSqlhdlInsertCmd, NULL, GPATAB_NAME) != RC_SUCCESS)
					dbg(TRACE,"<HandleGpatab> FAILED Insert in GPATAB: Fields <%s> Values <%s>", "ASEQ,FLNU,GAAE", szlValues);
			}
		} /* end of else if(GPU_OFF) */
		else
			dbg(DEBUG,"<HandleGpatab> GPU Status is not ON or OFF-> Status(%s)", pszlBasData->szStatus);
	} /* end of if(SearchAfttab()) */
	else
	{
		/* Record NON trovato in AFTTAB */
		HandleError(NULL, FLIGHT_NOT_FOUND_ERROR);
		ilRC = RC_FAIL;
	}

	dbg(DEBUG,"<HandleGpatab> *********** END ********(%i)", ilRC);
	return (ilRC);
}


/******************************************************************************/
/* SearchRecordInGpatab(): ricerca un record nella GPATAB e ritorna i campi	*/
/*									valorizzati del record trovato.							*/
/*----------------------------------------------------------------------------*/
/* Parametri: Where condition, Fields record found, Fields record struct		*/
/* Ritorna: RC_SUCCESS = Found; RC_FAIL = Not found									*/
/******************************************************************************/
int SearchRecordInGpatab(char *pszlWhere, char *pszlRecordFound, GPATAB_FIELD_STR *pstlGpaRec)
{
	int ilRC = RC_SUCCESS;
	char szlSelect[1024];
  	short cursor = 0;
  	
 	dbg(DEBUG,"<SearchRecordInGpatab> ****** START *****");
	memset(szlSelect,0x00,sizeof(szlSelect));
  
  	/* Preparo la select da effettuare sulla tabella in questione */
  	sprintf(szlSelect,"SELECT URNO,ASEQ,GAAE FROM %s WHERE %s", GPATAB_NAME, pszlWhere);
  	dbg(DEBUG,"<SearchRecordInGpatab> The query is <%s>",szlSelect);
	
   if ((ilRC = sql_if(START,&cursor,szlSelect,pszlRecordFound)) == RC_SUCCESS)
   {
		/* Inserisco una virgola per dividere un campo dall'altro */
  		BuildItemBuffer(pszlRecordFound, NULL, 3, ",");
		  		
  		memset(pstlGpaRec, 0x00, sizeof(GPATAB_FIELD_STR));  	 				
  		/******************************************************/
  		/* Prelevo i dati del record suddividendoli fra loro. */
  		GetCharField(pszlRecordFound, 1, pstlGpaRec->szUrno);
  		GetCharField(pszlRecordFound, 2, pstlGpaRec->szAseq);
  		GetCharField(pszlRecordFound, 3, pstlGpaRec->szGaae);
  	}
  	    	  
  	/* Chiudo il cursore */
  	close_my_cursor(&cursor);
   
   dbg(DEBUG,"<SearchRecordInGpatab> *******  END  ****(%i)", ilRC);	
   return (ilRC);
}


/******************************************************************************/
/* SearchInAfttab(): ricerca un record nella AFTTAB.									*/
/*----------------------------------------------------------------------------*/
/* Parametri: Range string, Num Piazzola, Valori ritornati							*/
/* Ritorna: RC_SUCCESS o RC_FAIL.															*/
/******************************************************************************/
int SearchInAfttab(char *pszlRangeUpFirst, char *pszlRangeDownFirst, char *pszlRangeUpSecond, char *pszlRangeDownSecond, char *pszlDateFromBAS, char *pszlPsta, char *pszlRecordFound)
{
	int 	ilRC = RC_SUCCESS;
	int		ilRCRecondFound = RC_FAIL;
	int 	iDiffTIFA = 0;
	int 	iDiffTIFD = 0;
	int		iBestTime = 0;
	short 	funct;
  	short 	cursor;
  	char 	szlSelect[3072];
	char 	szlApp[512];
  	char    szTempRow[47];
	char    szTempTIFA[15];
	char 	szTempTIFD[15];
	
	dbg(DEBUG,"<SearchInAfttab> ****** START *****");
	memset(szlSelect,0x00,sizeof(szlSelect));
	memset(szlApp,0x00,sizeof(szlApp));
  	
  	/**************************************************************/
  	/* Preparo la select da effettuare sulla tabella in questione */
  	strcpy(szlSelect, "SELECT a.rkey,a.flno,a.psta,a.tifa,b.flno,b.pstd,b.tifd FROM AFTTAB a, AFTTAB b");
  	sprintf(szlApp, " WHERE b.rkey in (SELECT a.urno from AFTTAB a WHERE a.tifa BETWEEN '%s' AND '%s' and a.adid = 'A' and a.onbl != ' ' and a.psta = '%s')", pszlRangeUpFirst, pszlRangeUpSecond, pszlPsta);
  	strcat(szlSelect, szlApp);
  	memset(szlApp,0x00,sizeof(szlApp));
  	sprintf(szlApp, " AND b.rkey = a.urno AND b.ADID != 'A' AND b.ftyp != 'S' AND b.ofbl = ' ' AND b.regn != ' ' AND b.pstd = '%s'", pszlPsta);
  	strcat(szlSelect, szlApp);
  	
   	strcat(szlSelect, " UNION");
	strcat(szlSelect, " SELECT a.rkey,a.flno,a.psta,a.tifa,' ' flno,' ' pstd,a.tifd FROM AFTTAB a");
	memset(szlApp,0x00,sizeof(szlApp));
	sprintf(szlApp, " WHERE a.tifa BETWEEN '%s' and '%s' AND a.ADID IN ('A','B') AND a.ftyp != 'S' AND a.onbl != ' ' AND a.regn != ' ' AND a.psta = '%s'", pszlRangeUpFirst, pszlRangeUpSecond,pszlPsta);
  	strcat(szlSelect, szlApp);
  	
  	strcat(szlSelect, " UNION");
  	strcat(szlSelect, " SELECT a.rkey,a.flno,a.psta,a.tifa,' ' flno,' ' pstd,a.tifd FROM AFTTAB a");
	memset(szlApp,0x00,sizeof(szlApp));
	sprintf(szlApp, " WHERE a.tifa BETWEEN '%s' and '%s' AND a.ADID IN ('D','B') AND a.ftyp != 'S' AND a.ofbl = ' ' AND a.regn != ' ' AND a.psta = '%s'",  pszlRangeDownFirst, pszlRangeDownSecond, pszlPsta);
  	strcat(szlSelect, szlApp);
  	
  	strcat(szlSelect, " ORDER BY tifa DESC");
	
  	dbg(DEBUG,"<SearchInAfttab> The query is <%s>",szlSelect);
  	/**************************************************************/
  	
  	/* Eseguo la ricerca */
   	
    /*Loreto Cardarelli -- Cerco il record con il tifd o il tifa piu'*/
    /*vicino al timestam che arriva dal BAS SYSTEM*/   	  
  	funct = START;
  	cursor = 0;
  	do
  	{
  		memset(szTempRow,0x00,sizeof(szTempRow));
		ilRC = sql_if(funct, &cursor, szlSelect, szTempRow);
		if (ilRC == RC_SUCCESS)
		{
			ilRCRecondFound = RC_SUCCESS;
			BuildItemBuffer(szTempRow, NULL, 7, ",");
			dbg(DEBUG,"<SearchInAfttab> Record from db: <%s>", szTempRow);
			memset(szTempTIFA,0x00,sizeof(szTempTIFA));
			memset(szTempTIFD,0x00,sizeof(szTempTIFD));
			get_real_item(szTempTIFA, szTempRow, 4);
			get_real_item(szTempTIFD, szTempRow, 7);
			
			dbg(DEBUG,"<SearchInAfttab> TIFA: <%s>", szTempTIFA);
			dbg(DEBUG,"<SearchInAfttab> TIFD: <%s>", szTempTIFD);
			/*Evaluete the different time between the BAS time and tifa and tifd from the record*/
			/*I use the "abs" function because the different could be negative					*/
			iDiffTIFA = abs(DifferentBetweenTwoUfisTime(pszlDateFromBAS, szTempTIFA)); 
			iDiffTIFD = abs(DifferentBetweenTwoUfisTime(pszlDateFromBAS, szTempTIFD));
			
			dbg(DEBUG,"<SearchInAfttab> Different between TIFA <%s> and BAS time <%s> is <%i> minutes", szTempTIFA, pszlDateFromBAS, iDiffTIFA);
			dbg(DEBUG,"<SearchInAfttab> Different between TIFD <%s> and BAS time <%s> is <%i> minutes", szTempTIFD, pszlDateFromBAS, iDiffTIFD);
									
			if (funct == START)
			{
				/*It is the first record that I retrive from db. I assume that is the best*/
				/*for the moment														  */
				strncpy(pszlRecordFound, szTempRow, 46);
				if (iDiffTIFA <= iDiffTIFD)
					iBestTime = iDiffTIFA;
				else
					iBestTime = iDiffTIFD;
			}
			else
			{
				if (iDiffTIFA < iBestTime)
				{
					iBestTime = iDiffTIFA;
					strncpy(pszlRecordFound, szTempRow, 46);
				}
				if (iDiffTIFD < iBestTime)
				{
					iBestTime = iDiffTIFD;
					strncpy(pszlRecordFound, szTempRow, 46);
				}
			}
			dbg(DEBUG,"<SearchInAfttab> Select record is: <%s>", pszlRecordFound);
			dbg(DEBUG,"<SearchInAfttab> Best time is: <%i>", iBestTime);
		}
		funct = NEXT;
  	}
  	while (ilRC == RC_SUCCESS);
  	/* Chiudo il cursore */
  	close_my_cursor(&cursor);
   dbg(TRACE, "<SearchInAfttab> Select record from GPATAB is: <%s>", pszlRecordFound);
   dbg(DEBUG, "<SearchInAfttab> **********  END  ****(%i)", ilRC);	
   return (ilRCRecondFound);
}

			
/******************************************************************************/
/* HandleInsertUpdate(): permette di eseguire un Insert o un Update su di una */
/*								tabella di CEDA tramite la funzione sql_if().			*/
/*----------------------------------------------------------------------------*/
/* Parametri: lista campi separati da virgola, lista valori separati da 		*/
/*					virgola, comando, URNO, Tabella											*/
/*	Ritorna: RC_SUCCESS o RC_FAIL																*/
/******************************************************************************/
int HandleInsertUpdate(char *pszlField, char *pszlValue, char *pszlCmd, char *pszlUpdUrno, char *pszlTab)
{
	int ilRC = RC_SUCCESS;
	int i;
	int ilItemNum = 0;
	short	cursor; 
  	short	funct;
  	char szlAppoField[520];
  	char szlAppoValue[520];
	char szlSqlifString[4096];
	char szlFieldList[1024];
	char szlValuedList[2048];
	char szlWhere[64];
	char szlTimeStamp[15];
	char szlRecordFound[128];
	char szlNewUrno[11];
	char error[128];
	BC_HEAD rlBCHeader;
  	CMDBLK  rlCmdblk;
	
	dbg(DEBUG, "<HandleInsertUpdate> *** START *** cmd<%s> tab(%s)", pszlCmd, pszlTab);
	
	memset(szlSqlifString, 0x00, sizeof(szlSqlifString));
	memset(szlFieldList, 0x00, sizeof(szlFieldList));
	memset(szlValuedList, 0x00, sizeof(szlValuedList));
	memset(szlWhere, 0x00, sizeof(szlWhere));
	memset(szlTimeStamp, 0x00, sizeof(szlTimeStamp));
	
	/* Estraggo la data di sistema */
	dbg(DEBUG, "<HandleInsertUpdate> Take the system time");
	GetUfisTime(szlTimeStamp);
	dbg(DEBUG, "<HandleInsertUpdate> The system time (Ufis Format) is: <%s>", szlTimeStamp);
	
	/* Verifico che tipo di comando ho ricevuto */
	if (strcmp(pszlCmd, szgSqlhdlInsertCmd) == 0)
	{	/* INSERT */
		/* Estraggo il nuovo URNO da inserire */
		dbg(DEBUG, "<HandleInsertUpdate> Insert - Take a new URNO");
		
		memset(szlNewUrno, 0x00, sizeof(szlNewUrno));
		GetNextValues(szlNewUrno, 1);
		dbg(DEBUG,"<HandleInsertUpdate> New Urno in(%s)=(%s)", pszlTab, szlNewUrno);
		   		
		/* Copio in locale i valori ricevuti e li completo */
		sprintf(szlFieldList, "%s,HOPO,CDAT,USEC,URNO", pszlField);
		sprintf(szlValuedList, "%s,%s,%s,%s,%s", pszlValue, szgHomeAirport, szlTimeStamp, szgProcName, szlNewUrno);
		
		dbg(DEBUG, "<HandleInsertUpdate> INSERT Fields <%s>, Values <%s>", szlFieldList, szlValuedList);
		
		/* Verifico se devo utilizzare la SQLIF */
		if (igDBEventSystemFlag == SQLIF_EVENT_SYSTEM)
		{
			/* Inserisco gli apici ad ogni dato sulla stessa stringa di input */
			FormatStringWithApes(szlValuedList, szlValuedList, ",");
			
			/* Compongo la stringa sql */
			sprintf(szlSqlifString,"INSERT INTO %s FIELDS (%s) VALUES (%s)", pszlTab, szlFieldList, szlValuedList);
			dbg(TRACE, "<HandleInsertUpdate> SQLIF Insert <%s>", szlSqlifString);
		}
	}
	else
	{	/* UPDATE */
		/* Copio in locale i valori ricevuti e li completo */
		dbg(DEBUG, "<HandleInsertUpdate> Update - Prepare the fields");
		sprintf(szlFieldList, "%s,LSTU,USEU", pszlField);
		sprintf(szlValuedList, "%s,%s,%s", pszlValue, szlTimeStamp, szgProcName);
		
		/* Preparo la Where */
   		sprintf(szlWhere, " WHERE URNO='%s'", pszlUpdUrno);
   	   	
		dbg(DEBUG, "<HandleInsertUpdate> UPDATE Fields <%s>, Values <%s>, Where <%s>", szlFieldList, szlValuedList, szlWhere);
		
		/* Verifico se devo utilizzare la SQLIF */
		if (igDBEventSystemFlag == SQLIF_EVENT_SYSTEM)
		{
			sprintf(szlSqlifString, "UPDATE %s SET ", pszlTab);
   			/***************************************/
   			/* Compongo le coppie campo = 'valore' */
   			ilItemNum = get_no_of_items(szlFieldList);
   	
   			for(i=0; i<ilItemNum; i++)
   			{
				if (i)
		   			strcat(szlSqlifString, ",");
			
				memset(szlAppoField, 0x00, sizeof(szlAppoField));
				memset(szlAppoValue, 0x00, sizeof(szlAppoValue));
			
			  	/* i+1 perche' i non puo' essere uguale a 0 */ 
   				get_real_item(szlAppoField, szlFieldList, i+1); 
   	  			get_real_item(szlAppoValue, szlValuedList, i+1);
   	  
   	  			strcat(szlSqlifString, szlAppoField);
   	  			strcat(szlSqlifString, "='");
   	  			strcat(szlSqlifString, szlAppoValue);
   	  			strcat(szlSqlifString, "'");
   			}
   		
   			/* Concateno la WHERE */
   			strcat(szlSqlifString, szlWhere);
   			dbg(TRACE, "<HandleInsertUpdate> SQLIF Update <%s>", szlSqlifString);
   		}
	}
	
	/**************************************************************/
	/* Verifico con quale modalita' devo eseguire l'Evento sul DB */
	if (igDBEventSystemFlag == SQLIF_EVENT_SYSTEM)
	{	/* SQLIF */
		memset(szlRecordFound, 0x00, sizeof(szlRecordFound));
		cursor = 0;
		funct = START|COMMIT;
	
		/* Eseguo l'evento sul Database */
		ilRC = sql_if(funct, &cursor, szlSqlifString, szlRecordFound);

		close_my_cursor(&cursor);
	}
	else
	{	/* SQLHDL */
  		dbg(DEBUG, "<HandleInsertUpdate> SQLHDL Event FieldList:<%s> ValueList:<%s> Where:<%s>", szlFieldList, szlValuedList, szlWhere);
  		memset(&rlBCHeader, 0x00, sizeof(BC_HEAD));
    	memset(&rlCmdblk, 0x00, sizeof(CMDBLK));
		/* Carico le strutture per l'invio tramite SendToQue() */
  		strcpy(rlBCHeader.dest_name, szgDestinationProcessName);
  		strcpy(rlBCHeader.orig_name, szgProcName);
  		rlBCHeader.rc = NETOUT_NO_ACK; /* Per non ricevere ACK dal router */
  		
  		/* Il valore 0 finale mi permette di gestire direttamente l'HOPO */
  		sprintf(rlCmdblk.tw_end, "%s,%s,%i,0", szgHomeAirport, "TAB", mod_id);
  		strcpy(rlCmdblk.obj_name, pszlTab);
  		strcpy(rlCmdblk.command, pszlCmd);
  		   	
  		/* Invio il comando al router */
  	 	ilRC = SendToQue(igDestinationProcessID, PRIORITY_3, &rlBCHeader, &rlCmdblk, szlWhere, szlFieldList, szlValuedList);  				
	}
	
	/* Verifico se l'evento ha avuto dei problemi */
	if (ilRC != RC_SUCCESS)
	{
   	dbg(TRACE,"<HandleInsertUpdate> Event FAILED-> Cmd<%s> Tab<%s>\n FieldList<%s>\n ValueList<%s>\n Where<%s>", pszlCmd, pszlTab, szlFieldList, szlValuedList, szlWhere);
   	memset(error, 0x00, sizeof(error));
   	get_ora_err(ilRC, error);
   	dbg(TRACE,"<HandleInsertUpdate> ora_error (%s)", error);
	}
	
	dbg(DEBUG, "<HandleInsertUpdate> **** END ****(%i)", ilRC);
	return (ilRC);
}


/******************************************************************************/
/* FormatStringWithApes(): riceve una stringa di dati separati da un carattere*/
/*								  e la riformatta inserendo un singolo apice per ogni	*/
/* dato. Necessaria per l'utilizzo della sql_if(). E' possibile modificare		*/
/* direttamente la stringa di input inserendo il suo puntatore sia nel primo	*/
/* che nel secondo parametro. Inoltre e' possibile definire il carattere		*/
/* separatore nel suo valore numerico (es. , = 44; - = 45; . = 46; ; = 59)		*/
/* Es.: oggi,domani diventa 'oggi','domani'												*/
/*----------------------------------------------------------------------------*/
/* Parametri: stringa in input, stringa in output modificata, delimitatore		*/
/* Ritorna: RC_SUCCESS o RC_FAIL																*/
/******************************************************************************/ 
int FormatStringWithApes(char *pszlOldString, char *pszlNewString, char *pszlDelimiter)
{
	int i;
	int ilLen = 0;
	char *pszlAppString = NULL;
	
	dbg(DEBUG, "<FormatStringWithApes> **** START ****");
	
	if (pszlNewString == NULL)
	{
		dbg(TRACE, "<FormatStringWithApes> ERROR-> Parameter 2 is NULL");
		return RC_FAIL;	
	}
	
	ilLen = (strlen(pszlOldString) + (2 * get_no_of_items(pszlOldString)) + 1);
	if ((pszlAppString = (char *) malloc(ilLen)) == NULL)
	{
		dbg(TRACE, "<FormatStringWithApes> ERROR-> Malloc Failed (%i) bytes", ilLen);
		return RC_FAIL;	
	}
	
	memset(pszlAppString, 0x00, ilLen);
	/* Inserisco il primo apice */
	sprintf(pszlAppString, "'");
	
	/*******************************************/
	/* Sostituisco ',' ad ogni singola virgola */
	for(i=0; (pszlOldString[i] != '\0'); i++)
	{
		if (strcmp(&pszlOldString[i], pszlDelimiter) == 0)
		{
			strcat(pszlAppString, "'");			
			strncat(pszlAppString, pszlDelimiter, strlen(pszlDelimiter));
			strcat(pszlAppString, "'");
		}
		else
			strncat(pszlAppString, &pszlOldString[i], 1);		
	}
	
	strcat(pszlAppString, "'");
	
	/* Ripulisco e ricopio la stringa modificata sul puntatore di ritorno */
	memset(pszlNewString, 0x00, ilLen);
	strcpy(pszlNewString, pszlAppString);
	
	free(pszlAppString);
	
	dbg(DEBUG,"<FormatStringWithApes> *** END ***<%s>", pszlNewString);
	return RC_SUCCESS;
}


/******************************************************************************/
/* ConvertLCLDateInGMTDate(): trasforma una data in formato Ufis, da Local 	*/
/*										time in GMT time. E' possibile trasformare date */
/* valide a partire dal 01/01/1970 al 19/01/2038. E' possibile, inoltre, 		*/
/* utilizzare la stessa variabile per avere la data modificata su se stessa!	*/
/* (Nota: in questo caso, pero', i valori printati a fine funzione risultano 	*/
/* uguali fra loro e corrispondono alla corretta data in GMT!!!!)					*/
/*----------------------------------------------------------------------------*/
/* Parametri: Data in Local Time (input), Data in GMT (output).					*/
/* Ritorna: RC_SUCCESS, RC_FAIL.																*/
/******************************************************************************/
int ConvertLCLDateInGMTDate(char *pszlLclDate, char *pszlGmtDate)
{
	int ilRC = RC_SUCCESS;
	char szlApp[5];
	struct tm stlTm;
	time_t stlTime_t;
	
	dbg(DEBUG,"<ConvertLCLDateInGMTDate> ********** START **********");
	memset(&stlTm, 0x00, sizeof(struct tm));
	memset(&stlTime_t, 0x00, sizeof(time_t));
	
	/****************************************************************/
	/* Verifico la correttezza dei parametri in input */
	if (strlen(pszlLclDate) < 14)
	{
		dbg(TRACE,"<ConvertLCLDateInGMTDate> Data in input NON Valida!");
		ilRC = RC_FAIL;
	}
	
	if (pszlGmtDate == NULL)
	{
		dbg(TRACE,"<ConvertLCLDateInGMTDate> Variabile per il ritorno GMTData NON Valida!");
		ilRC = RC_FAIL;
	}
	
	/****************************************************************/
	/****************************************************************/
	/* Carico la struttura tm con la data in Locale che ho ricevuto */
	/****************************************************************/
	/* Anno */
	memset(szlApp, 0x00, sizeof(szlApp));
	strncpy(szlApp, pszlLclDate, 4);
	stlTm.tm_year = (atoi(szlApp)-1900);
	/* Mese */
	memset(szlApp, 0x00, sizeof(szlApp));
	strncpy(szlApp, &pszlLclDate[4], 2);
	stlTm.tm_mon = (atoi(szlApp)-1);
	/* Giorno */
	memset(szlApp, 0x00, sizeof(szlApp));
	strncpy(szlApp, &pszlLclDate[6], 2);
	stlTm.tm_mday = atoi(szlApp);
	/* Ora */
	memset(szlApp, 0x00, sizeof(szlApp));
	strncpy(szlApp, &pszlLclDate[8], 2);
	stlTm.tm_hour = atoi(szlApp);
	/* Minuti */
	memset(szlApp, 0x00, sizeof(szlApp));
	strncpy(szlApp, &pszlLclDate[10], 2);
	stlTm.tm_min = atoi(szlApp);
	/* Secondi */
	memset(szlApp, 0x00, sizeof(szlApp));
	strncpy(szlApp, &pszlLclDate[12], 2);
	stlTm.tm_sec = atoi(szlApp);
	/* Calcola automaticamente la differenza oraria */	
	stlTm.tm_isdst = -1;
	
	/* Trasformo la data nella struttura time_t */
	if ((stlTime_t = mktime(&stlTm)) == RC_FAIL)
		ilRC = RC_FAIL;
	else	/* Ritorno la data in GMT in formato Ufis (YYYYMMDDhhmiss) */
		strftime(pszlGmtDate, 15, "%Y%m%d%H%M%S", gmtime(&stlTime_t));
	
	dbg(DEBUG,"<ConvertLCLDateInGMTDate> Lcl(%s) Gmt(%s)", pszlLclDate, pszlGmtDate);
	dbg(DEBUG,"<ConvertLCLDateInGMTDate> *********** END *********(%i)**", ilRC);
	return ilRC;
}


/******************************************************************************/
/* LoadStructFromBuffer(): carica i dati dell'intero iesimo buffer (linea),	*/
/*								ricevuto nella coda primaria, nella struttura.			*/
/*----------------------------------------------------------------------------*/
/* Parametri: Buffer di origine, Puntatore per Struttura								*/
/******************************************************************************/
void LoadStructFromBuffer(BUFFER_STR *pstlBuffer, BAS_DATA_STR *pstlBasData)
{
	dbg(TRACE,"<LoadStructFromBuffer> *** START *** Rec<%s>", (char *)pstlBuffer);
	
	/****************************/
	/* Carico la struttura dati */		
	memset(pstlBasData, 0x00, sizeof(BAS_DATA_STR));
		
	strncpy(pstlBasData->szTime, pstlBuffer->szTime, 14);
	/*sizeof(pstlBuffer->szTime));*/
	str_trm_all(pstlBasData->szTime, " ", TRUE);
	/* Modifico la data da Local a GMT time */
	ConvertLCLDateInGMTDate(pstlBasData->szTime, pstlBasData->szTime);
	dbg(DEBUG,"<LoadStructFromBuffer> Time(%s)", pstlBasData->szTime);
	
	strncpy(pstlBasData->szRemark, pstlBuffer->szRemark, 65);
	/*sizeof(pstlBuffer->szRemark));*/
	str_trm_all(pstlBasData->szRemark, " ", TRUE);
	dbg(DEBUG,"<LoadStructFromBuffer> Remark(%s)", pstlBasData->szRemark);
	
	strncpy(pstlBasData->szStatus, pstlBuffer->szStatus, 16);
	/*sizeof(pstlBuffer->szStatus));*/
	str_trm_all(pstlBasData->szStatus, " ", TRUE);
	dbg(DEBUG,"<LoadStructFromBuffer> Status(%s)", pstlBasData->szStatus);
	
	strncpy(pstlBasData->szAddress, pstlBuffer->szAddress, 25);
	/*sizeof(pstlBuffer->szAddress));*/
	str_trm_all(pstlBasData->szAddress, " ", TRUE);
	/* Elimino l'eventuale CR/LF come ultimo carattere */
	str_trm_rgt(pstlBasData->szAddress, "\n", TRUE);
	dbg(DEBUG,"<LoadStructFromBuffer> Address(%s)", pstlBasData->szAddress);
		
	dbg(DEBUG,"<LoadStructFromBuffer> *************  END  ***********");	
}

/******************************************************************************/
/* HandleError(): visualizza all'esterno il risultato dell'elaborazione.     	*/
/*----------------------------------------------------------------------------*/
/* Parametri: 																						*/
/*	Stringa specifica da aggiungere in output, Identificativo errore.				*/
/******************************************************************************/
static void HandleError(void *plInfo, int ilErrorFlag)
{
  switch(ilErrorFlag)
  {   	 						
    case CFG_FILE_ERROR:
           	dbg(TRACE,"<HandleError> **************** ERROR *****************");
           	dbg(TRACE,"              Missing Important Information");
           	dbg(TRACE," into the Configuration file of the Process (%s%i.cfg).", szgProcName, mod_id);
           	dbg(TRACE,"-------------- Process is Not Operative --------------");
           	dbg(TRACE,"******************************************************");
           	break;
                  
	 case EVENT_ERROR:
           	dbg(TRACE,"<HandleError> **************** ERROR *****************");
           	dbg(TRACE,"              Error on Reading Primary Input Queue");
           	dbg(TRACE,"-- Impossible to continue -->> -- Terminate Process --");
            dbg(TRACE,"******************************************************");
            
            Terminate(5);
           	break;             
        
    case RECORD_LEN_ERROR:
    			dbg(TRACE,"<HandleError> **************** ERROR *****************");
           	dbg(TRACE," Unhandled message for Invalid BAS Message Len (%i/%i)", (int*) plInfo, BAS_DATA_LENGHT);
           	dbg(TRACE,"******************************************************");
           	break;
    
    case PLACE_ERROR:
    			dbg(TRACE,"<HandleError> **************** ERROR *****************");
           	dbg(TRACE," Impossible to keep Place Number from BAS Message (%s)", (char*) plInfo);
           	dbg(TRACE,"********         BAS Message Discarded        ********");
           	break;
  
  	 case GPU_MSG_NUM_ERROR:
    			dbg(TRACE,"<HandleError> **************** ERROR *****************");
           	dbg(TRACE," Too many GPU messages for record (URNO = <%s>) in GPATAB", (char*) plInfo);
           	dbg(TRACE,"********         BAS Message Discarded        ********");
           	break;
    
  	 case FLIGHT_NOT_FOUND_ERROR:
    			dbg(TRACE,"<HandleError> **************** ERROR *****************");
           	dbg(TRACE," 			Arrival Flight NOT FOUND in AFTTAB");
           	dbg(TRACE,"********         BAS Message Discarded        ********");
           	break;
	 
  	 case GPU_MSG_ERROR:
           	dbg(TRACE," <%s>", (char*) plInfo);
           	dbg(TRACE,"******************************************************");
           	break;
           			
    case GPU_STILL_ON_WARNING:
    			dbg(TRACE,"<HandleError> *************** WARNING ****************");
           	dbg(TRACE," Record FOUND in GPATAB -URNO,ASEQ,GAAE(%s)- still in GPU ON", (char*) plInfo);
           	dbg(TRACE,"           Update GPATAB.GAAB record found");
           	dbg(TRACE,"******************************************************");
           	break;
     
    case GPU_ALREADY_OFF_WARNING:
    			dbg(TRACE,"<HandleError> *************** WARNING ****************");
           	dbg(TRACE," Record FOUND in GPATAB -URNO,ASEQ,GAAE(%s)- already in GPU OFF", (char*) plInfo);
           	dbg(TRACE,"           Update GPATAB.GAAE record found");
           	dbg(TRACE,"******************************************************");
           	break;
	 	 
    case GPU_REC_NOT_FOUND_WARNING:
    			dbg(TRACE,"<HandleError> *************** WARNING ****************");
           	dbg(TRACE," Record NOT FOUND in GPATAB (Where: <%s>)", (char*) plInfo);
           	dbg(TRACE,"         No Update but New Insert in GPATAB");
           	dbg(TRACE,"******************************************************");
           	break;
          
  	 default: break;
  }
} /* end of HandleError() */


/******************************************************************************/
/* get_record(): restituisce la lunghezza del record letto escluso il			*/
/*						carattere che termina il singolo record. I record vengono  	*/
/* contati a partire da 1 (il sistema NON e' zero-based).							*/
/*----------------------------------------------------------------------------*/
/* Parametri: stringa in input, carattere divisore in ASCII ('\n'), iesimo		*/
/*					record da prelevare, stringa con il record in output.				*/
/* Ritorna: lunghezza del record																*/
/******************************************************************************/
int get_record(char* szsource, char* szfindkey, int iRecNo, char* szrecord)
{
	int ilRC = 0;
	int iNumRec = 0;
	char* pOccurence;
	char* pStr;
	char* pStrStart;
	int i;
  	int iStringLen = 0;

	if(iRecNo<=0)
	{
		dbg(DEBUG,"<GetRecord> The record number was 0 (unexpected value)!");
		return(0);
	}		

	pStr = szsource;	

  	for(i = 1; i <= iRecNo; i++)	
	{
		pStrStart = pStr;
		if((pOccurence = (char*)strstr(pStr,szfindkey)) == 0x00)
		{
			ilRC = 0;
      		i++; /* Serve per entrare nella if successiva che cattura l'ultimo record */
			break;
		}
		
		pStr = pOccurence + strlen(szfindkey);
    	ilRC=1;
	}

  	if (ilRC==0 && i > iRecNo)
  	{	/* Catturo l'ultimo record che termina con '\0' */
  		strcpy(szrecord, pStrStart);		
      iStringLen = (int) strlen(szrecord);
  	}
  	else
  	{
  		strncpy(szrecord, pStrStart, pOccurence - pStrStart);		
      iStringLen = (int)(pOccurence - pStrStart);
  	}

	return(iStringLen);
}

/********************************************************************************/
/* get_recordcount(): dato un buffer di memoria (file, stringa ...) ed il		*/
/*									carattere che termina il singolo record, 	*/
/*					  ritorna il numero di record contenuti.					*/
/*------------------------------------------------------------------------------*/
/* Parametri: stringa in input, carattere divisore in ASCII ('\n')				*/
/* Ritorna: il numero di record													*/
/********************************************************************************/
int get_recordcount(char* szsource, char* szfindkey)
{
	/* function needed to do a record count;
	   "szsource" is the text to check and "szfindkey" is the key used to mark the end of record. */
	int iNumRec = 0;
	char* pOccurence;
	char* pStr = szsource;
		
	do
	{
		if ((pOccurence = (char*)strstr(pStr,szfindkey)) == 0x00)
    	{
			if (strlen(pStr))
		  	iNumRec++;
			break;
    	}
    	
		iNumRec++;
		pStr = pOccurence + strlen(szfindkey);
		
	}while(1);
	
	return(iNumRec);
}

/******************************************************************************/
/* GetUfisTime(): Restituisce data e ora attuale nel formato standard di		*/
/*							Ufis: YYYYMMGGHHMiSS (Local time).							  	*/
/*----------------------------------------------------------------------------*/
/* Parametri: stringa in input restituita valorizzata.								*/
/* Ritorna: RC_SUCCESS, RC_FAIL se la variabile di ritorno non e' sufficiente */
/******************************************************************************/
int GetUfisTime(char *szData)
{
	int ilRC = RC_SUCCESS;
	time_t alive;
	struct tm *aliveptr;
	
	dbg(DEBUG, "<GetUfisTime> Start function");
	
	time(&alive);
	aliveptr = localtime(&alive);
	sprintf(szData,"%4.4d%2.2d%2.2d%2.2d%2.2d%2.2d",aliveptr->tm_year+1900,aliveptr->tm_mon+1,aliveptr->tm_mday,aliveptr->tm_hour,aliveptr->tm_min,aliveptr->tm_sec);
	
	dbg(DEBUG, "<GetUfisTime> The ufis time is: <%s>", szData);
	
	if (strlen(szData) != 14)
	{
		dbg(TRACE,"<GetUfisTime> Problems in formatting date...");
		ilRC = RC_FAIL;
	}
	
	dbg(DEBUG, "<GetUfisTime> End function");
	
	return ilRC;
}


/******************************************************************************/
/* AddDate(): permette di aggiungere o togliere N giorni/ore/minuti ad una */
/* 				 data/ora che viene passata come parametro. 							*/
/*----------------------------------------------------------------------------*/
/* Parametri: data in input che deve essere modificata, stringa con i 			*/
/*	giorni/ore/minin (GGHHMi) che vanno sommati o sottratti, data risultante, 	*/
/* flag che indica l'azione da fare (0 = somma; 1 = sottrazione).					*/
/*	Ritorna: 1 se riuscita, 0 se andata male.												*/
/******************************************************************************/
int AddDate(char* szDate, char* szAdding, char* szResultDate, int operazione )
{
	/***************************************/
	/*            Dichiarazioni            */
	/***************************************/
	typedef struct strData
	{
		char year[4];
		char mon[2];
		char day[2];
		char hour[2];
		char min[2];
		char sec[2];
	} stDATE;

	typedef struct strAdding
	{
		char GG[2];
		char HH[2];
		char MM[2];
	} stADD;

	stDATE * pDate;
	stADD * pAdd;
	int iYear,iMon,iDay,iHour,iMin,iSec,iGG,iHH,iMM;
	int i,gior;
	char appo[10];
	char szAppDate[15];

	/********************************************************************/
   /* controllo che le date passate contengano solo caratteri numerici */
	/********************************************************************/
	dbg(DEBUG,"<AddDate> - Questa e la data che arriva: %s",szDate);
	dbg(DEBUG,"<AddDate> - Questo e il parametro da aggiungere: %s",szAdding);

   for(i=0; i<14; i++)
   {
		if (szDate[i] < '0' || szDate[i] > '9')
			{
				dbg(TRACE,"<AddDate> - Errore, la data contiene caratteri non numerici");
				return 0;
			}
	}
	
	for(i=0; i<6; i++)
	{
		if (szAdding[i] < '0' || szAdding[i] > '9')
			{
				dbg(TRACE,"<AddDate> - Errore, il parametro contiene caratteri non numerici");
				return 0;
			}
	}
	
	if (szResultDate == NULL)
	{
		dbg(TRACE,"<AddDate> - Errore, szResultDate = NULL");
		return 0;
	}

	if (operazione != ADDICTION && operazione != SUBTRACTION) 
	{
		dbg(TRACE,"<AddDate> - Errore, operazione non valida");
		return 0;
	}
	/* fine controllo */

	/*******************************************/
	/* Converto le date in variabili numeriche */
	/*******************************************/
	pDate = (stDATE*)malloc(sizeof(stDATE));
   pAdd = (stADD*)malloc(sizeof(stADD));
	memset(pDate,0x00,sizeof(stDATE));
	memset(pAdd,0x00,sizeof(stADD));

	memcpy(pDate, szDate, 14);
	memcpy(pAdd, szAdding, 6);

	memset(appo,0x00,sizeof(appo));
	memcpy(appo,pDate->year,4);
	iYear = atoi(appo);

	memset(appo,0x00,sizeof(appo));
	memcpy(appo,pDate->mon,2);
	iMon = atoi(appo);

	memcpy(appo,pDate->day,2);
	iDay = atoi(appo);

	memcpy(appo,pDate->hour,2);
	iHour = atoi(appo);

	memcpy(appo,pDate->min,2);
	iMin = atoi(appo);

	memcpy(appo,pDate->sec,2);
	iSec = atoi(appo);

	memcpy(appo,pAdd->GG,2);
	iGG = atoi(appo);

	memcpy(appo,pAdd->HH,2);
	iHH = atoi(appo);

	memcpy(appo,pAdd->MM,2);
	iMM = atoi(appo);

	/*****************************************************/
	/* Controllo che la data passata sia una data valida */
	/*****************************************************/
	if (iSec > 59 || iSec < 0 ||
	    iMin > 59 || iMin < 0 ||
	    iHour > 23 || iHour < 0 ||
	    iMon > 12 || iMon < 1 || iDay < 1 ||
	    ((iMon == 1 || iMon == 3 || iMon == 5 || iMon == 7 || iMon == 8 || iMon == 10 || iMon == 12) && iDay >31) ||
	    ((iMon == 4 || iMon == 6 || iMon == 9 || iMon == 11) && iDay > 30) || 
	    (iMon == 2 && (iYear%4 == 0 && (iYear%100 != 0 || iYear%400 == 0)) && iDay > 29) ||
	    (iMon == 2 && (iYear%4 != 0 || (iYear%100 == 0 && iYear%400 != 0)) && iDay > 28) )
	{
		dbg(TRACE,"<AddDate> - Errore, la data passata non e una data valida");
		return 0;
	}
	/* Fine controllo */

	/*****************************************/
	/* Operazione di addizione o sottrazione */
	/*****************************************/
	if (operazione == ADDICTION)
	{
		iHour += iHH;
		iMin += iMM;
	}
	else
	{
		iHour -= iHH;
		iMin -= iMM;
		iGG = 0-iGG;
	}
	
	if (iMin > 59)
	{
		iHour++;
		iMin -= 60;
	}
	
	if (iMin < 0)
	{
		iHour--;
		iMin += 60;
	}
	
	if (iHour > 23) 
	{
		gior = iHour/24;
		iGG += gior;
		iHour -= 24*gior;
	}			
	else if (iHour < 0)
	{
		iGG -= 1;
		iHour += 24;
	}

	if (iGG > 0)
	{
		for(i=0; i<iGG; i++)
		{
			if (((iMon == 1 || iMon == 3 || iMon == 5 || iMon == 7 || iMon == 8 || iMon == 10 || iMon == 12) && iDay < 31 ) ||
             ((iMon == 4 || iMon == 6 || iMon == 9 || iMon == 11) && iDay < 30 ) || 
              (iMon == 2 && (iYear%4 == 0 && (iYear%100 != 0 || iYear%400 == 0)) && iDay < 29) ||
              (iMon == 2 && (iYear%4 != 0 || (iYear%100 == 0 && iYear%400 != 0)) && iDay < 28))
			{
				iDay++;
			}
			else
			{
				iDay = 1;
				iMon++;
				if (iMon == 13)
				{
					iMon = 1;
					iYear++;
				}
			}
		}
	}
	else if (iGG < 0) 
	{
		for(i=0; i<-iGG; i++) 
		{
			if (iDay == 1)
			{
				if (iMon == 1 || iMon == 2 || iMon == 4 || iMon == 6 || iMon == 8 || iMon == 9 || iMon == 11)
				{
					iDay = 31;
					if (iMon == 1)
					{
						iMon = 13;
						iYear--;
					}
				}
				else if (iMon == 5 || iMon == 7 || iMon == 10 || iMon == 12)
					iDay = 30;
				else if (iMon == 3 && (iYear%4 == 0 && (iYear%100 != 0 || iYear%400 == 0)))
					iDay = 29;
				else if (iMon == 3 && (iYear%4 != 0 || (iYear%100 == 0 && iYear%400 != 0)))
					iDay = 28;
				iMon--;
			}
			else
			{
				iDay--;
			}
		}
	}

	/* Ora ho la data nuova */
	sprintf(appo, "%4.4d", iYear);
   strcpy(szAppDate,appo);

	sprintf(appo, "%2.2d", iMon);
   strcat(szAppDate, appo);     

	sprintf(appo, "%2.2d", iDay);
   strcat(szAppDate, appo);     

	sprintf(appo, "%2.2d", iHour);
   strcat(szAppDate, appo);     

	sprintf(appo, "%2.2d", iMin);
   strcat(szAppDate, appo);     

	sprintf(appo, "%2.2d", iSec);
   strcat(szAppDate, appo);     

	strcpy(szResultDate,szAppDate);
	dbg(DEBUG,"<AddDate> La ResultDate alla fine: (%s) ", szResultDate);
	return 1;
} /* end of AddDate() */


/******************************************************************************/
/* SendToQue(): permette di inviare un messaggio in coda, ID del primo			*/
/*						parametro, e di riceverne la risposta sulla coda primaria.	*/
/*----------------------------------------------------------------------------*/
/* Parametri: stringa in input, carattere divisore in ASCII ('\n'), iesimo		*/
/*					record da prelevare, stringa con il record in output.				*/
/* Ritorna: RC_SUCCESS, RC_FAIL																*/
/******************************************************************************/
int SendToQue(int ipModID, int ipPrio, BC_HEAD *prpBCHead, CMDBLK *prpCmdblk, char *pcpSelection, char *pcpFields, char *pcpData)
{
    int         ilRC;
    int         ilLen;
    EVENT       *prlOutEvent    = NULL;
    BC_HEAD     *prlOutBCHead   = NULL;
    CMDBLK      *prlOutCmdblk   = NULL;

    dbg(DEBUG,"<SendToQue> ----- START -----");

    /* calculate size of memory we need */
    ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 20;

    /* get memory for out event */
    if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
    {
        dbg(DEBUG,"<SendToQue> %05d malloc failed, can't allocate %d bytes", __LINE__, ilLen);
        dbg(DEBUG,"<SendToQue> ----- END -----");
        return RC_FAIL;
    }

    /* clear buffer */
    memset((void*)prlOutEvent, 0x00, ilLen);

    /* set structure members */
    prlOutEvent->type        = SYS_EVENT;
    prlOutEvent->command     = EVENT_DATA;
    prlOutEvent->originator  = mod_id;
    prlOutEvent->retry_count = 0;
    prlOutEvent->data_offset = sizeof(EVENT);
    prlOutEvent->data_length = ilLen - sizeof(EVENT);


    /* BCHead members */
    prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
    memcpy(prlOutBCHead, prpBCHead, sizeof(BC_HEAD));

    /* CMDBLK members */
    prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
    memcpy(prlOutCmdblk, prpCmdblk, sizeof(CMDBLK));

    /* Selection */
    strcpy(prlOutCmdblk->data, pcpSelection);

    /* fields */
    strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1, pcpFields);

    /* data */
    strcpy(prlOutCmdblk->data+strlen(pcpSelection)+strlen(pcpFields)+2, pcpData);

    /* send this message */
    dbg(DEBUG,"<SendToQue> sending to Mod-ID: %d", ipModID);
    if ((ilRC = que(QUE_PUT, ipModID, mod_id, ipPrio, ilLen, (char*)prlOutEvent)) != RC_SUCCESS)
    {
        dbg(DEBUG,"<SendToQue> %05d QUE_PUT returns: %d", __LINE__, ilRC);
        return RC_FAIL;
    }

    /* delete memory */
    free((void*)prlOutEvent);

    dbg(DEBUG,"<SendToQue> ----- END -----");

    /* bye bye */
    return RC_SUCCESS;
}


/******************************************************************************/
/* GetCharField(): estrae una singola informazione da un record (cioe' da 		*/
/*							una lista separata da virgole) e lo restituisce in 		*/
/* formato char. Se il valore e' vuoto viene restituito almeno un blank.		*/
/*----------------------------------------------------------------------------*/
/* Parametri:																						*/
/* Lista separata da virgole(record), posizione dell'informazione nella lista,*/
/* Stringa di destinazione(meglio se gia' pulita!).									*/
/* Ritorna: 																						*/
/*	RC_FAIL se non c'e' memoria per la stringa di destinazione; il numero di 	*/
/* caratteri dell'informazione trovata (compresi i blank cancellati !!!)		*/
/******************************************************************************/
int GetCharField(char *pszlRecordFound, int ilPosition, char *pszlDestString)
{  
	int ilLen = 0;
	
	/* Mi assicuro che il puntatore alla stringa di destinazione sia valido */
	if (pszlDestString == NULL)
	{
		dbg(TRACE,"<GetCharField> Terzo Parametro in input Non Valido!");
		return RC_FAIL;
	}
	
	/* Estraggo l'informazione richiesta e mi salvo il numero di */
	/* caratteri letti compresi tutti i blank che poi cancello ! */
   ilLen = get_real_item(pszlDestString, pszlRecordFound, ilPosition);
   
   /* Elimino tutti i blank */
   str_trm_all(pszlDestString, " ", TRUE);  
   
   /* Per evitare di gestire o creare campi con valori a NULL */
   /* verifico la lunghezza del campo dopo la trim.           */
   if (strlen(pszlDestString) == 0)
   	strcpy(pszlDestString, " ");	/* Se e' vuoto, inserisco almeno un blank */
      
   return (ilLen);
}

/********************************************************************************/
/* DifferentBetweenTwoUfisTime(): evaluete the differents (in minutes) between  */
/*                             two ufis time									*/
/*------------------------------------------------------------------------------*/
/* Parameter: Date of begin, date of end all the date are in ufis format        */
/* Return   : Minutes between two ufis time										*/
/********************************************************************************/
int DifferentBetweenTwoUfisTime(char *pTimeStart, char *pTimeEnd)
{ /* Calcola la durata in minuti di un periodo di tempo */
	int iDurata;
 	struct tm time1,time2;
 	time_t tm1, tm2;
 	double sec;
 
 	dbg(DEBUG,"<DifferentBetweenTwoUfisTime> Start function");
 	CedaDateToTimeStruct(&time1, pTimeStart);
 	CedaDateToTimeStruct(&time2, pTimeEnd);
 
 	tm1 = mktime(&time1);
 	tm2 = mktime(&time2);
 
 	sec = difftime(tm2,tm1);
 
 	iDurata = (int)sec/60;
/* 	
	if (iDurata < 0)
 	{
  		iDurata = 0;
 	}
*/ 
	dbg(DEBUG,"<DifferentBetweenTwoUfisTime> Diff time: <%i>", iDurata);
	dbg(DEBUG,"<DifferentBetweenTwoUfisTime> End function");
 	return iDurata;
}

/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
