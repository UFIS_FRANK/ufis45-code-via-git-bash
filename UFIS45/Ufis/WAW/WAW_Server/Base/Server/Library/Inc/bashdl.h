/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program  bashdl.h	                                             */
/*                                                                            */
/* Author         :   Massimiliano Franceschini                               */
/* Date           :   28/04/2003 16.33                       						*/
/* Version        :   1.5			                                             */
/* Description    :   File di supporto al bashdl.c                            */
/* Update history :   Prima stesura del 24/02/2003			                     */
/*																										*/
/* 18/03/03 : Aggiunti alcuni parametri al File.cfg								   */
/* 24/03/03 : Aggiunti alcuni parametri al File.cfg								   */
/* 25/03/03 : Aggiornato secondo il relativo documento.								*/
/* 28/04/03 : Aggiunte funzioni e modificati parametri anche nel file.cfg.		*/
/******************************************************************************/
/******************************************************************************/
#define TRUE      1
#define FALSE     0

#define COPY_CMD	"cp"
#define MOVE_CMD	"mv"

#define ADDICTION		0
#define SUBTRACTION	1

/* Numero di caratteri di un singolo BAS msg */
#define BAS_DATA_LENGHT	122

#define AFTTAB_NAME	"AFTTAB"
#define GPATAB_NAME	"GPATAB"
#define BASTAB_NAME	"BASTAB"

#define SQLHDL_EVENT_SYSTEM	0
#define SQLIF_EVENT_SYSTEM		1

/*************************************************/
/* Definizione dei comandi impostati per default */
/* in caso di mancata lettura sul file.cfg		 */
#define GPU_RANGE_TIME_DEFAULT			1440
#define GPU_RANGE_TIME_SECOND_DEFAULT   15
#define STRING_GPU_MSG_DEFAULT			"400Hz"
#define STRING_GPU_ON_DEFAULT			"ON"
#define STRING_GPU_OFF_DEFAULT			"OFF"
#define STRING_PLACE_NUM_DEFAULT		"STANOWISKO"
#define INSERT_CMD_DEFAULT				"IRT"
#define UPDATE_CMD_DEFAULT				"URT"
#define DEST_PROCESS_ID_DEFAULT			1200
#define DEST_PROCESS_NAME_DEFAULT		"ROUTER"
#define HOME_AIRPORT_DEFAULT			"WAW"


#define MAX_NUM_GPU_USAGE		3
/**************************************************************/
/* Definizioni degli errori e dei risultati dell'elaborazione */
/* utilizzate nella funzione HandleError().                   */
#define CFG_FILE_ERROR           	1
#define EVENT_ERROR              	2
#define RECORD_LEN_ERROR				3
#define PLACE_ERROR						4
#define GPU_MSG_NUM_ERROR				5
#define FLIGHT_NOT_FOUND_ERROR		6
#define GPU_MSG_ERROR					7
#define GPU_STILL_ON_WARNING			8
#define GPU_ALREADY_OFF_WARNING		9
#define GPU_REC_NOT_FOUND_WARNING	10

/*****************************************************/
/* Struttura che contiene i singoli dati del BAS msg */
/*****************************************************/
typedef struct
{
	char szTime[15];
	char szRemark[66];
	char szStatus[17];
	char szAddress[26];
} 
BAS_DATA_STR;

/********************************************************************************/
/* Struttura che contiene il record del messaggio BAS (120 bytes + 2 blank + LF)*/
/********************************************************************************/
/* Struttura identica al formato dati che arriva tramite coda primaria.   */
/* Le informazioni di lunghezza fissa, compresi i blank, vengono inserite */
/* in questa struttura prima di essere passate nella struttura di lavoro  */
/* BAS_DATA_STR che ha il corretto terminatore di stringa.					  */
typedef struct
{
	char szTime[14];
	char cBlank1;
	char szRemark[65];
	char cBlank2;
	char szStatus[16];
	char szAddress[26];
}
BUFFER_STR;


/*********************************************************/
/* Struttura che contiene i campi ricercati nella GPATAB */
/*********************************************************/
typedef struct
{
	char szUrno[11];
	char szAseq[2];
	char szGaae[15];
} 
GPATAB_FIELD_STR;

/**********************************************************/
/* 		Variabili globali per lo skeleton					 */
/**********************************************************/
static ITEM  *prgItem      = NULL;    /* The queue item pointer  */
static EVENT *prgEvent     = NULL;    /* The event pointer       */
static int   igItemLen     = 0;       /* length of incoming item */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];
static char  szgProcName[16];

/**********************************************************/
/* 						Variabili globali		 					 */
/**********************************************************/

/**********************************************************/
/* 		Variabili globali da file.cfg							 */
/**********************************************************/
int igGpuRangeTimeFirst = 0;
int igGpuRangeTimeSecond = 0;

char szgStringGpuMsg[16];
char szgStringGpuOn[16];
char szgStringGpuOff[16];
char szgStringPlaceNum[16];

char szgHomeAirport[4];
int igDBEventSystemFlag = 0;
char szgSqlhdlInsertCmd[8];
char szgSqlhdlUpdateCmd[8];
int igDestinationProcessID = 0;
char szgDestinationProcessName[8];

int debug_level = TRACE;

/*************************/
/* Prototipi di Funzione */
/*************************/
int AddDate(char *, char *, char *, int);
void BasDataHdl(char *);
int ConvertLCLDateInGMTDate(char *, char *);
int FormatStringWithApes(char *, char *, char *);
int get_record(char *, char *, int, char *);
int get_recordcount(char *, char *);
int GetCharField(char *, int, char *);
int GetUfisTime(char *);
static void HandleData();
static void HandleError(void *, int);
int HandleGpatab(BAS_DATA_STR *);
int HandleInsertUpdate(char *, char *, char *, char *, char *);
static void HandleQueErr(int);
static void HandleQueues();
int Init_bashdl();
int InsertBastab(BAS_DATA_STR *);
void LoadStructFromBuffer(BUFFER_STR *, BAS_DATA_STR *);
static int Reset();
int SearchInAfttab(char *, char *, char*, char *, char *, char *, char *);
int SearchRecordInGpatab(char *, char *, GPATAB_FIELD_STR *);
int SendToQue(int, int, BC_HEAD *, CMDBLK *, char *, char *, char *);
static void Terminate(int);
int DifferentBetweenTwoUfisTime(char *pTimeStart, char *pTimeEnd);
