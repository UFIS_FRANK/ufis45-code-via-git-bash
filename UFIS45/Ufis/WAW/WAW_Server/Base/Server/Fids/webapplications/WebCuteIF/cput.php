<?php 
/*
Copyright � 2004 by INFOMAP S.A.
This computer program is protected by copyright law
of Greece and International treaties. Unauthorized
reproduction of this program, or any portion of it,
may result in severe civil and criminal penalties, and
will be prosecuted to the maximum extent possible under
the law.
*/
	include("include/manager.php");

	include("include/class.LoginInit.php");
	include ("include/class.PHP_Timer.php"); 
	
	
	/* Set Timer Instance*/
	$timer = new PHP_timer;
	$timer->start();
	/* Set Timer Instance*/	


	
	/* Login Check */
	/* Set Login Instance*/
	$login = new  LoginInit;
	$logintype="Login_"	.$AuthType;				
	if (isset($Session["userid"])) { 
		// The user has already been Logged
		
	} else {
		$Query=	$login->$logintype($username,$password);

	}
	
	/* Start Debug  if requested */
	if ($debug=="true") {
		$tmpdebug="?debug=true";
		$db->debug=$debug;
		$dbMysql->debug=$debug;
	}else {
		$debug="false";
	}			 

//include('include/ErrorHandler.php');
//$error =& new ErrorHandler();
//$error->set_altdlog(__FILE__, FILE_LOG, 'logs/main.log');


?>

<html>
<title><?php echo $PageTitle ?></title>
 <META NAME="description" CONTENT="">
  <META NAME="author" CONTENT="Infomap - Team@Work - T. Dimitropoulos/e-mail:thimios@infomap.gr">
  <META NAME="dbauthor" CONTENT="Infomap - Team@Work - A.Papachrysanthou/e-mail:Anthony@infomap.gr">  
  <META NAME="author" CONTENT="Infomap - Team@Work - G. Fourlanos/e-mail:fou@infomap.gr">
  <META NAME="Art designer" CONTENT="Infomap - Team@Work - S. Rallis/e-mail:stratos@infomap.gr">  
  <META NAME="Art designer" CONTENT="Infomap - Team@Work - K. Xenou/e-mail:batigol@infomap.gr">  
  <META HTTP-EQUIV="Reply-to" CONTENT="webmaster@infomap.gr">
  <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1253">
  <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-7">
  <META HTTP-EQUIV="Expires" CONTENT="<?php echo $Cexpires ?>">
  <link rel="STYLESHEET" type="text/css" href="style/style.css">

<script language="JavaScript1.2" type="text/javascript">
function LogoAlert() {
	var msg="";
	msg="NO LOGO CHOSEN\u000D\u000D Please select a logo by executing the following steps:";
	msg=msg+"\u000D \u00091. Choose a logo from the Preview list on the left side.";
	msg=msg+"\u000D \u00092. Press the PREVIEW button on the left side.";
	msg=msg+"\u000D \u000D THANK YOU.";
	alert(msg);
}

function WaitAlert() {
	var msg="";
	msg="Please wait your last transaction has not finished yet.";
	alert(msg);
}

</script>
<?php

if ($Global_status=="Logo") {
	$Session["CheckinPredefinedSelection"] = false;
	$Session["logoSelection"] = true;
}	
if ($Global_status=="Remarks") {
	$Session["CheckinPredefinedSelection"] = true;
	$Session["logoSelection"] = false;

}

//echo $Global_status.'--'.$Session["CheckinPredefinedSelection"] ."<br>".$Session["logoSelection"] ;
//$PRemarkCode="";
//echo $Button ."|".$FlightURNO ."|".$PRemarkCode."|".$Text1."|".$Text2."|".$Session["AirlineURNO"]."|". $Session["PicName"];	
if (isset($FlightURNO)) {
    
	$Session["FlightURNO"] = $FlightURNO;
	if ($Session["Type"]=="GATE") {
	    if ($Button=="Refresh Data") {$Button="Refresh";$PRemarkCode="";}
		if ($Button=="Clear Gate") {$Button="Clear";$PRemarkCode="Clear";}
		if ($Button=="Gate Open") {$Button="Open";$PRemarkCode=$RemarkGateOpen;}
		if ($Button=="Boarding") {$Button="Transmit";$PRemarkCode=$RemarkGateBoarding;}
		if ($Button=="Final Call") {$Button="Transmit";$PRemarkCode=$RemarkGateFinalCall;}
		if ($Button=="Gate Closed") {$Button="Close";$PRemarkCode=$RemarkGateClose;}
	}


		if (ereg("Clear",$Button)==True || ereg("Open",$Button)==True || ereg("Update",$Button)==True || ereg("Transmit",$Button)==True || ereg("LogoTransmit",$Button)==True && $PRemarkCode != $RemarkGateClose) {
			$Session["AirlineURNO"]="0";
			$UpdateStatus=UpdateUfisLH($Button);
		
			if ($UpdateStatus==1){echo "<script language=\"JavaScript1.2\" type=\"text/javascript\">WaitAlert()</script>";}
		}
		
	
		if (ereg("Close",$Button)==True || $PRemarkCode == $RemarkGateClose) {
			// Close Counter
			
			CloseCounter();
			
			//if ($Session["Type"]!="GATE") {
			//	echo "<script language=\"JavaScript1.2\">parent.location.href=\"index.php\"</script>";
			//}
		}

	
	
	
}
		echo $Button ."|".$FlightURNO ."|".$PRemarkCode."|".$Text1."|".$Text2."|".$Session["AirlineURNO"]."|". $Session["PicName"];	
// and filename and line number so as to find the corresponding debug output easier
$error->debug($FlightURNO, '$FlightURNO', __FILE__, __LINE__);

?>

</head>

<body bgcolor="#ffffff" >

<?php
if ((ereg("Refresh",$Button)==True && $PRemarkCode == "") || (ereg("Clear",$Button)==True && $PRemarkCode="" )) {
	if ($Session["Type"]=="GATE") {
		echo "<script language=\"JavaScript1.2\">parent.Worksheet.location.href=\"mainGate.php?RButton=".$Button."\"</script>";
	} else {
		echo "<script language=\"JavaScript1.2\">parent.Worksheet.location.href=\"main.php?RButton=".$Button."\"</script>";
	}
	
} elseif (isset($FlightURNO) && (ereg("Close",$Button)!=True || $PRemarkCode != "GCO") ) {
	//Reload the Status Window

	//echo "<script language=\"JavaScript1.2\">parent.frames[1].location.href=\"status.php?RButton=".$Button."\"</script>";
        echo "<script language=\"JavaScript1.2\">window.setTimeout('parent.frames[1].location.href=\"status.php?RButton=".$Button."\"',240);\n</script>";
}



	$timer->stop();
	//$timer->debug();
	//$timer->showtime();

	

?>


</body>
</html>
 
