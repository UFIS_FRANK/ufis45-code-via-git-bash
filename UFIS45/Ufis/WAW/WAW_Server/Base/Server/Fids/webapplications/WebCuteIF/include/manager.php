<?php
/*
Copyright � 2004 by INFOMAP S.A.
This computer program is protected by copyright law
of Greece and International treaties. Unauthorized
reproduction of this program, or any portion of it,
may result in severe civil and criminal penalties, and
will be prosecuted to the maximum extent possible under
the law.
*/
session_save_path  ("/tmp" );
set_time_limit(240); // increase timeout
session_name("CuteIf");
session_start();
session_register("Session");


$ADODB_RootPath = 'include';	
define('ADODB_DIR',"include/");


include("include/config.php"); 



$HostName = $HTTP_SERVER_VARS["HTTP_HOST"];
//$Url = $HTTP_SERVER_VARS["HTTP_URL"];
$ClientIP=$HTTP_SERVER_VARS["REMOTE_ADDR"];

$devserver="172.17.1.30";

if ($HostName==$devserver)
{

  $mailto="";
  $WebApplicationName="/";
  if ($devserver==$Hostname)
  {
       $HostName=$HostName.".ccsdev.com";
  } 
}
  else
{

  $mailto="";
  $WebApplicationName="/";
} 

define('LOG4PHP_DIR', dirname(__FILE__).'/log4php');
define('LOG4PHP_CONFIGURATION', dirname(__FILE__).'/log4php.dailyfile.properties');

require_once LOG4PHP_DIR.'/LoggerManager.php';


//if (!$nofunction) {
//print "$ADODB_RootPath/adodb.inc.php";


	include("$ADODB_RootPath/adodb.inc.php");	
	$db = ADONewConnection($DatabaseType);
    $db->debug = false;
//    $db->debug = true;
	$db->connectSID = true;
	if ($db->Connect(false, $dbusername, $dbpassword, $database,false))
		$cna="true"; //connect to oracle
	else $cna="false";



if ($AuthType=="Mysql") {
	$dbMysql = ADONewConnection($DatabaseTypeMysql);
    $dbMysql->debug = false;
//   $dbMysql->debug = true;

	if ($dbMysql->Connect($servernameMysql, $dbusernameMysql, $dbpasswordMysql, $databaseMysql))
		$cnaMysql="true"; //connect to Mysql
	else $cnaMysql="false";
}
	
//echo $cna;
//exit;


if ($AjaxEnabled== true)  {
    $LoginAction = "cuteif_main.php";
} else {
    $LoginAction = "cuteif.php";
}

/*
Note: Since PHP 5.1.0 (when the date/time functions were rewritten), every call to a date/time function will generate a E_NOTICE if the timezone isn't valid, and/or a E_STRICT message if using the system settings or the TZ environment variable.
*/
// For the complete timezones have a look in the etc/TimeZones.txt
if (phpversion()>5.1) {
    date_default_timezone_set($timezone);
} else {
    putenv("TZ=".$timezone);
}

include("include/functions.php"); 
require 'include/smarty/Smarty.class.php';
	
$tdiminus=0;
$startdate = time();

$Cexpires=DateAdd("s" ,$Refresh,$startdate);
$Cexpires=date("D, d M Y H:i:s ",DateAdd("n" ,$tdiminus,$Cexpires))." GMT";



?>
