<?php 
/*
Copyright � 2004 by INFOMAP S.A.
This computer program is protected by copyright law
of Greece and International treaties. Unauthorized
reproduction of this program, or any portion of it,
may result in severe civil and criminal penalties, and
will be prosecuted to the maximum extent possible under
the law.
*/
include("include/manager.php");

include("include/class.LoginInit.php");
include ("include/class.PHP_Timer.php"); 


    /* Set Timer Instance*/
$timer = new PHP_timer;
$timer->start();
    /* Set Timer Instance*/



    /* Login Check */
    /* Set Login Instance*/
$login = new  LoginInit;
$logintype="Login_"	.$AuthType;				
if (isset($Session["userid"])) { 
    // The user has already been Logged

} else {
    $Query=	$login->$logintype($username,$password);

}

    /* Start Debug  if requested */
if ($debug=="true") {
    $tmpdebug="?debug=true";
    $db->debug=$debug;
    $dbMysql->debug=$debug;
}else {
    $debug="false";
}			 

//include('include/ErrorHandler.php');
//$error =& new ErrorHandler();
//$error->set_altdlog(__FILE__, FILE_LOG, 'logs/main.log');


?>

<html>
    <title><?php echo $PageTitle ?></title>
    <META NAME="description" CONTENT="">
    <META NAME="author" CONTENT="Infomap - Team@Work - T. Dimitropoulos/e-mail:thimios@infomap.gr">
    <META NAME="dbauthor" CONTENT="Infomap - Team@Work - A.Papachrysanthou/e-mail:Anthony@infomap.gr">
    <META NAME="author" CONTENT="Infomap - Team@Work - G. Fourlanos/e-mail:fou@infomap.gr">
    <META NAME="Art designer" CONTENT="Infomap - Team@Work - S. Rallis/e-mail:stratos@infomap.gr">
    <META NAME="Art designer" CONTENT="Infomap - Team@Work - K. Xenou/e-mail:batigol@infomap.gr">
    <META HTTP-EQUIV="Reply-to" CONTENT="webmaster@infomap.gr">
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1253">
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-7">
    <META HTTP-EQUIV="Expires" CONTENT="<?php echo $Cexpires ?>">
    <link rel="STYLESHEET" type="text/css" href="style/style.css">
    <!-- In head section we should include the style sheet for the grid -->
    <link rel="stylesheet" type="text/css" media="screen" href="include/jquery/themes/basic/grid.css" />

    <!-- Of course we should load the jquery library -->
    <script src="include/jquery/js/jquery.js" type="text/javascript"></script>

    <!-- and at end the jqGrid Java Script file -->
    <script src="include/jquery/js/jquery.jqGrid.js" type="text/javascript"></script>

    <script type="text/javascript">
        // We use a document ready jquery function.
        jQuery(document).ready(function(){
            jQuery("#FlightData").jqGrid({
                width:'500',
                // the url parameter tells from where to get the data from server
                // adding ?nd='+new Date().getTime() prevent IE caching
                url:'controller.php?task=Allocation&Display=1',
                // datatype parameter defines the format of data returned from the server
                // in this case we use a JSON data
                datatype: "json",
                // colNames parameter is a array in which we describe the names
                // in the columns. This is the text that apper in the head of the grid.
                colNames:['Urno','Flight Number', 'Schedule Time'],
                // colModel array describes the model of the column.
                // name is the name of the column,
                // index is the name passed to the server to sort data
                // note that we can pass here nubers too.
                // width is the width of the column
                // align is the align of the column (default is left)
                // sortable defines if this column can be sorted (default true)
                colModel:[
                    {name:'urno',index:'urno', width:1},
                    {name:'flno',index:'flno', width:90},
                    {name:'stod',index:'stod', width:90}
                ],
                // pager parameter define that we want to use a pager bar
                // in this case this must be a valid html element.
                // note that the pager can have a position where you want
                //pager: jQuery('#pager2'),
                // rowNum parameter describes how many records we want to
                // view in the grid. We use this in example.php to return
                // the needed data.
                rowNum:10,
                // rowList parameter construct a select box element in the pager
                //in wich we can change the number of the visible rows
                //rowList:[10,20,30],
                // path to mage location needed for the grid
                imgpath: 'include/jquery/themes/sand/images',
                // sortname sets the initial sorting column. Can be a name or number.
                // this parameter is added to the url
                sortname: 'flno',
                //viewrecords defines the view the total records from the query in the pager
                //bar. The related tag is: records in xml or json definitions.
                viewrecords: true,
                //sets the sorting order. Default is asc. This parameter is added to the url
                sortorder: "flno",
                // multiselect: true,
                caption: "Flight Data"
            });
            jQuery("#m1").click( function() {
                var s;
                s = jQuery("#FlightData").getGridParam('selarrrow');
                alert(s); });
            jQuery("#a1").click( function(){
                var urno = jQuery("#FlightData").getGridParam('selrow'); if (urno) { var ret = jQuery("#FlightData").getRowData(urno); alert("urno="+ret.urno+"..."); } else { alert("Please select row");} });

        });
    </script>
    </head>

    <body background="pics/workback.jpg" style="font-family:Arial">
        <!-- the grid definition in html is a table tag with class 'scroll' -->
        <table id="FlightData" class="scroll" cellpadding="0" cellspacing="0"></table>

        <a href="javascript:void(0)" id="g4" onclick="alert(jQuery('#FlightData').getGridParam('selrow'));">Get Selected Row</a>
        <a href="javascript:void(0)" id="m1">Get Selected id's</a>
        <a href="#" id="a1">Get data from selected row</a> <br />
        <!-- pager definition. class scroll tels that we want to use the same theme as grid -->
<!--<div id="pager2" class="scroll" style="text-align:center;"></div>-->

        <center>
            <form action="cput.php" method="post" target="Cput">
            <table>
                <tr>
                    <td colspan="2"><strong>Flight Nr.</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong> Airl.</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong> Sche. Time </strong></td>
                </tr>

                <?php
                $tmp="";
                if ($Session["STATUS"]==1) {
                    $tmp=" disabled";
                    //if ($Session["STATUS"]==1) {
                    echo "<input type=\"Hidden\" name=\"FlightURNO\" value=\"".$Session["FlightURNO"]."\" >";
                    //} else {
                    //	echo "<input type=\"Hidden\" name=\"FlightURNO\" value=\"\" >";
                    //}
                }

                ?>
                <tr>
                    <td valign="top">
                        <p><select name="FlightURNO" size=8 style="font-style:bold" style="font-family:Courier New" style="font-size:10pt" style="background-color:linen;" <%echo $tmp%>>
                                   <?php
                                   CheckAlloc(1);
                                   ?>
                                   </select>
                    </td>


                    <td valign="top">

                        <table>
                            <tr>
                                <td><input type="submit" name="Button" value="Open"></td>
                            </tr>
                            <?php
                            $bValue = "Close";
                            if ($RButton == "Close") {
                                $bValue = "Clear";
                            }
                            if ($Session["STATUS"]==1 || $RButton == "Close") {
                                ?>
                            <tr>
                                <td><input type="submit" name="Button" value="<%=$bValue%>"></td>
                            </tr>
                            <?php } ?>
                        </table>
                        <?php if ( ($Session["FldRows"]>1 && $Session["STATUS"]==1) || ($Session["OldCounterOpen"]==True && $Session["STATUS"]==1)  ) {?>
                        <table width="550">
                            <tr>
                                <td valign="top"><img src="pics/yeah.gif" alt="" border="0" align="top"><strong>Note:</strong></td>
                                <td>In case that the previous flight has not been closed the flight<br>
                                    has to be closed first before you can assign another one.<br><br>

                                    Click the '<strong>Close</strong>' button <br>
                                    or contact the <strong>ASOC</strong> Coordinator for more information<BR>
                                    <table>
                                        <tr>
                                            <td><strong>CHECK-IN </strong></td><td><strong> Tel.:</strong></td>
                                            <td>&nbsp;<strong>40003</strong> or </td>
                                        </tr>
                                        <tr>
                                            <td><strong>GATES </strong></td><td><strong> Tel.:</strong></td>
                                            <td>&nbsp;<strong>40002</strong></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                            <?php } ?>
                    </td>
                </tr>
            </table>
        </center>


        <?php

        $timer->stop();
        //$timer->debug();
        //$timer->showtime();

        ?>


    </body>
</html>

