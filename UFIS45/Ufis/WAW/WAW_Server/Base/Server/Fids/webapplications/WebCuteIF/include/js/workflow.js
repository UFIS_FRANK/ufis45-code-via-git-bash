
function workflow(ctype) {
    if (ctype=="GATE") {
        


        $.ajax({
            url: "controller.php?task=premarks",
            success: function(response){
                
                $("#Btn_GateOpen").addClass("redbutton");
                $("#Btn_GateClose").toggle();
                
               
            },
            dataType: "html"
        });

        
    } else {

     
    }
    return false;
}

function cput(myvalue,ctype) {
    //Get myvalue old Button
    
    var LogoSelect_val;
    var PRemarkSelect_val;
    var Text1_val,Text2_val;
    var FlightURNO;
    var post_variable;
    
    
    //Get the Logo
    var LogoSelect = document.getElementById("LogoSelect");
    if (LogoSelect != undefined) {
        if (LogoSelect.selectedIndex>=0){
            LogoSelect_val = LogoSelect.options[LogoSelect.selectedIndex].value
        }
    }

    //Get the Predefine Remark
    var PRemarkSelect = document.getElementById("PRemarkCode");
    if (PRemarkSelect != undefined) {
        if (PRemarkSelect.selectedIndex>=0){
            PRemarkSelect_val = PRemarkSelect.options[PRemarkSelect.selectedIndex].value
        }
    }

    //Get the Text1
    var Text1 = document.getElementById("Text1");
    if (Text1 != undefined) {
        Text1_val = Text1.value;
    }
    //Get The Text2
    var Text2 = document.getElementById("Text2");
    if (Text2 != undefined) {
        Text2_val = Text2.value;
    }
    
    //Get FLNO (from the Selected row from the GRID)
    var urno = jQuery("#FlightData").getGridParam('selrow');
    if (urno) {
        var ret = jQuery("#FlightData").getRowData(urno);
        FlightURNO = ret.urno;
    } else {
        alert("Please select Flight");
        return false;
    }

    post_variable = 'task=update&FlightURNO='+FlightURNO+'&Button='+myvalue+'&PRemarkCode='+PRemarkSelect_val+'&Text1='+Text1_val+"&Text2="+Text2_val+'&LogoSelect='+LogoSelect_val;
    //alert(ctype + '--'+post_variable);
    //button_workflow ('OPEN',ctype);
    
    $.ajax({
            url: "controller.php?"+post_variable ,
            success: function(response) {
            //    alert(response+'--'+ctype+'--'+myvalue)
                button_workflow(response,ctype,myvalue)
            },
            dataType: "html"
        });

    
   
    return true;
}

function button_workflow (response,ctype,myvalue){
  //   alert(response+'--'+ctype+'--'+myvalue)
    if (ctype=="GATE") {
        var btn_array = Array();
        btn_array[0] =  "Btn_GateOpen";
        btn_array[1] =  "Btn_GateBoarding";
        btn_array[2] =  "Btn_GateFinalCall";
        btn_array[3] =  "Btn_GateClose";

        if (response==" ") {
           var obj =  $("#"+btn_array[3]);
           //alert(obj);
           
           obj.hide();
        } else {
            for (i=0;i<GateRemarks.length;i++) {
                //The First Remark in the Array is the open
                
                // The last is the close
                if (response == GateRemarks[i]) {
                   // alert("Founded"+response + "--"+GateRemarks[i]+"--"+btn_array[i]);
                    var obj =  $("#"+btn_array[i]);
                    obj.addClass("redbutton");
                    var obj =  $("#"+btn_array[3]);
                    if (i==0 || i==3 ) {
                       
                        obj.hide();
                    } else {
                      
                        obj.show();
                    }
                } else {
                    //alert("Not Founded"+response + "--"+GateRemarks[i]+"--"+btn_array[i]);
                    var obj =  $("#"+btn_array[i]);

                    obj.removeClass("redbutton");

                }
            }
        }
            



    } else {
        //alert(myvalue)
         if (myvalue=="Open" || myvalue=="OPEN" ) {
             //$("#Checkin_BtnOpen").hide();
             $("#Checkin_BtnClose").show();
             $("#Checkin_BtnOpen").attr({value:"Transmit"});
             $("#Checkin_BtnClose").attr({value:"Close"});
         } else if (myvalue=="Close" ||myvalue=="CLOSE" ||myvalue=="CLOSED" )  {
            // $("#Checkin_BtnClose").attr({value:"Close"});
             $("#Checkin_BtnOpen").hide();
             //$("#Checkin_BtnClose").toggle();
             $("#Checkin_BtnClose").attr({value:"Clear"});
         } else if (myvalue=="Clear" || myvalue=="CLEAR")  {
             $("#Checkin_BtnClose").attr({value:"Close"});
             $("#Checkin_BtnOpen").show();
             //$("#Checkin_BtnClose").toggle();
            // $("#Checkin_BtnClose").attr({value:"Clear"});

//             $("#Checkin_BtnOpen").attr({value:"Open"});
//             $("#Checkin_BtnOpen").show();
//             $("#Checkin_BtnClose").hide();
         }

}

//Change the Status 
$("#Status").html(response);

gridReload();

}

function change_frameset(myvalue) {

    if (myvalue=="Enable Logo") {
        $("#LeftLogoSelection").show();
        $("#PRemarks").hide();
        $("#Btn_Frame").attr({value:"Enable Remarks"});
        Global_status="Logo";
    }
    if (myvalue=="Enable Remarks") {
        $("#LeftLogoSelection").hide();
        $("#PRemarks").show();
        $("#Btn_Frame").attr({value:"Enable Logo"});
        Global_status="Remarks";
    }

    $.ajax({
            url: "controller.php?task=change_frameset&Global_status="+Global_status ,
            dataType: "html"
        });

}






