<?php
/*
Copyright � 2004 by INFOMAP S.A.
This computer program is protected by copyright law
of Greece and International treaties. Unauthorized
reproduction of this program, or any portion of it,
may result in severe civil and criminal penalties, and
will be prosecuted to the maximum extent possible under
the law.
*/
//session_name("CuteIf");
//session_start();
//session_register("Session");


function IsNumeric($n)
{
    $p=trim($n);
    $l=strlen($p);

    for ($t=0; $t<$l; $t++)
    {
        $c=substr($p,$t,1);
        if ($c<'0' || $c>'9')
        { if ($c!='.') return 0; }
    }

    return 1;
}

function len($str) {
    return strlen($str);
}

function DateAdd ($interval,  $number, $date) {

    $date_time_array  = getdate($date);

    $hours =  $date_time_array["hours"];
    $minutes =  $date_time_array["minutes"];
    $seconds =  $date_time_array["seconds"];
    $month =  $date_time_array["mon"];
    $day =  $date_time_array["mday"];
    $year =  $date_time_array["year"];

    switch ($interval) {

        case "yyyy":
        $year +=$number;
        break;
        case "q":
        $year +=($number*3);
        break;
        case "m":
        $month +=$number;
        break;
        case "y":
        case "d":
        case "w":
        $day+=$number;
        break;
        case "ww":
        $day+=($number*7);
        break;
        case "h":
        $hours+=$number;
        break;
        case "n":
        $minutes+=$number;
        break;
        case "s":
        $seconds+=$number;
        break;

    }
    $timestamp =  mktime($hours ,$minutes, $seconds,$month ,$day, $year);
    return $timestamp;
}


Function DateDiffv1 ($interval, $date1,$date2) {

    // get the number of seconds between the two dates
    $timedifference =  $date2 - $date1;

    switch ($interval) {
        case "w":
        $retval  = bcdiv($timedifference ,604800);
        break;
        case "d":
        $retval  = bcdiv($timedifference,86400);
        break;
        case "h":
        $retval = bcdiv($timedifference,3600);
        break;
        case "n":
        $retval  = bcdiv($timedifference,60);
        break;
        case "s":
        $retval  = $timedifference;
        break;

    }
    return $retval;

}

function dateDiff($interval,$dateTimeBegin,$dateTimeEnd) {
    //Parse about any English textual datetime
    //$dateTimeBegin, $dateTimeEnd
/*
         $dateTimeBegin=strtotime($dateTimeBegin);
         if($dateTimeBegin === -1) {
           return("..begin date Invalid");
         }

         $dateTimeEnd=strtotime($dateTimeEnd);
         if($dateTimeEnd === -1) {
           return("..end date Invalid");
         }
                 */

    $dif=$dateTimeEnd - $dateTimeBegin;

    switch($interval) {
        case "s"://seconds
        return($dif);

        case "n"://minutes
        return(floor($dif/60)); //60s=1m

        case "h"://hours
        return(floor($dif/3600)); //3600s=1h

        case "d"://days
        return(floor($dif/86400)); //86400s=1d

        case "ww"://Week
        return(floor($dif/604800)); //604800s=1week=1semana

        case "m": //similar result "m" dateDiff Microsoft
        $monthBegin=(date("Y",$dateTimeBegin)*12)+
        date("n",$dateTimeBegin);
        $monthEnd=(date("Y",$dateTimeEnd)*12)+
        date("n",$dateTimeEnd);
        $monthDiff=$monthEnd-$monthBegin;
        return($monthDiff);

        case "yyyy": //similar result "yyyy" dateDiff Microsoft
        return(date("Y",$dateTimeEnd) - date("Y",$dateTimeBegin));

        default:
        return(floor($dif/86400)); //86400s=1d
    }

}


Function FindTDI () {
/* Time Diff */
    //Global $db,$hopo;
    $db = $GLOBALS["db"];
    $hopo = $GLOBALS["hopo"];

    $cmdTempCommandText = "SELECT TICH,TDI1,TDI2   from CEDA.APTTAB  where APC3='$hopo'";
    $Currenttimediff=&$db->Execute($cmdTempCommandText);
    if (! $Currenttimediff->EOF)  {

        /* TICH is used as local date */
        //$tich= ChopString ("n", 0,  $Currenttimediff->fields("TICH")) ;
        $str = $Currenttimediff->fields("TICH");
        $year = substr ($str, 0, 4);
        $month = substr ($str, 4,2);
        $day = substr ($str, 6, 2);
        $hour = substr ($str, 8, 2);
        $min = substr ($str, 10, 2);
        $sec = substr ($str, 12, 2);
        //Convert it to UTC
        $tich=gmmktime ($hour,$min,$sec,$month,$day,$year);

        $hh=strftime("%H",time());
        $mm=strftime("%M",time());
        $ss=strftime("%S",time());

        $Month=strftime("%m",time());
        $Day=strftime("%d",time());
        $Year=strftime("%Y",time());
        //Convert it to UTC
        $tnow=gmmktime($hh,$mm,$ss,$Month,$Day,$Year);

        /* use time diff. after date change */
        if ($tnow>$tich) {
            $tdi=  $Currenttimediff->fields("TDI2");
        } else {
        /* use time diff. before date change */
            $tdi=  $Currenttimediff->fields("TDI1");

        }


    }// If End
    if ($Currenttimediff) $Currenttimediff->Close();
    return $tdi;


}

function OracleDate() {
    //Global $db;
    $db = $GLOBALS["db"];

    $cmdTempCommandText = "Select TO_CHAR (SYSDATE , 'YYYYMMDDHH24MISS') as odate from dual";
    $rs=$db->Execute($cmdTempCommandText);
    $odate="";
    if (!$rs->EOF)  {
        $odate=$rs->fields("odate");
    } else {
        echo "NoDate";
    }// If End
    if ($rs) $rs->Close();


    return $odate;
}// End Function

function getCedaTime() {
    //Global $db,$Session;
    $db = $GLOBALS["db"];
    $Session = $GLOBALS["Session"];
    //Get Ceda Time
/*
        $StrString="";
        $cmdTempCommandText = "select TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS') as cedaTime from DUAL";
        $rs=$db->Execute($cmdTempCommandText);
        if (!$rs->EOF)  {
                $StrString=$rs->fields("cedaTime");
        }// If End
        if ($rs) $rs->Close();
*/	
    $hh=gmstrftime("%H",time());
    $mm=gmstrftime("%M",time());
    $ss=gmstrftime("%S",time());

    $Month=gmstrftime("%m",time());
    $Day=gmstrftime("%d",time());
    $Year=gmstrftime("%Y",time());

    $StrString = $Year.$Month.$Day.$hh.$mm.$ss;

    return $StrString;
}

function file_type($file){
    $path_chunks = explode("/", $file);
    $thefile = $path_chunks[count($path_chunks) - 1];
    $dotpos = strrpos($thefile, ".");
    return strtolower(substr($thefile, $dotpos + 1));
}


function ChopString ($interval,  $number, $str) {

    if (len($str)>0) {
        $year = substr ($str, 0, 4);
        $month = substr ($str, 4,2);
        $day = substr ($str, 6, 2);
        $hour = substr ($str, 8, 2);
        $min = substr ($str, 10, 2);
        $sec = substr ($str, 12, 2);
        $date=mktime ($hour,$min,$sec,$month,$day,$year);
    } else {$date="";}


    if ($number>0 && len($str)>0) {
        return DateAdd ($interval,  $number, $date) ;
    } else {
        return $date;
    }

}


function CedaSYSDATE ($interval,  $number) {

    $hh=gmstrftime("%H",time());
    $mm=gmstrftime("%M",time());
    $ss=gmstrftime("%S",time());

    $Month=gmstrftime("%m",time());
    $Day=gmstrftime("%d",time());
    $Year=gmstrftime("%Y",time());

    $utcdate = 	gmmktime($hh, $mm, $ss, $Month,$Day, $Year);

    //return gmdate("dmYHis",$utcdate);
    return "'".gmdate("YmdHis",DateAdd($interval ,$number,$utcdate))."'";



}

function CedaSYSTIME ($interval,  $number) {

    $hh=gmstrftime("%H",time());
    $mm=gmstrftime("%M",time());
    $ss=gmstrftime("%S",time());

    $Month=gmstrftime("%m",time());
    $Day=gmstrftime("%d",time());
    $Year=gmstrftime("%Y",time());

    $utcdate = 	gmmktime($hh, $mm, $ss, $Month,$Day, $Year);

    //return gmdate("dmYHis",$utcdate);
    return "'".gmdate("Hi",DateAdd($interval ,$number,$utcdate))."'";



}

function WeekDay ($interval,  $number) {

    $hh=gmstrftime("%H",time());
    $mm=gmstrftime("%M",time());
    $ss=gmstrftime("%S",time());

    $Month=gmstrftime("%m",time());
    $Day=gmstrftime("%d",time());
    $Year=gmstrftime("%Y",time());

    $utcdate = 	gmmktime($hh, $mm, $ss, $Month,$Day, $Year);

    //return gmdate("dmYHis",$utcdate);
    // Fix for the <  PHP 5
    $dayNum=gmdate("D",DateAdd($interval ,$number,$utcdate));

    $day = 1;

    switch ($dayNum) {
        case "Mon": $day = 1; break;
        case "Tue": $day = 2; break;
        case "Wed": $day = 3; break;
        case "Thu": $day = 4; break;
        case "Fri": $day = 5; break;
        case "Sat": $day = 6; break;
        case "Sun": $day = 7; break;
    }




    return $day;



}

function cmd ($ip,$cmd)
{
    //Global $RSCMD;
    $RSCMD = $GLOBALS["RSCMD"];

    $strreturn="";
    //You may need to provide the full path
    //If your web-server user has no environment 'path'
    $line = exec($RSCMD ." " . $ip. "  ". $cmd ." " , $output, $rsherror);

    while (list(,$line) = each($output)){
        $strreturn= $strreturn."".$line;
    }

    if ($rsherror){
        $strreturn="Unix error code: $rsherror<BR>\n";
    }
    return 	$strreturn;
}


function errorMsg() {
    //Global $Session,$Error;
    $Session = $GLOBALS["Session"];
    $Error = $GLOBALS["Error"];
    $TelNumber =  $GLOBALS["TelNumber"];
    $strlogin="";
    if ($Session["Type"]=="GATE") {
        $tmp="Gate";
    } else {
        $tmp="CheckIn";
    }
    $logger = LoggerManager::getRootLogger('CuteIf');
    $logger->debug("Error : ".$Error);

    
    switch ($Error) {
        case "1":
        $strlogin="You have entered a wrong password. <BR>Please try again.";
        break;
        case "2":
        $strlogin="You are not allowed to log in from this terminal.<BR> Please contact IT&T Help Desk for more information -><BR>Tel.: " .$TelNumber;
        break;
        case "3":
        $strlogin=$tmp." " .strtoupper($Session["Number"]). " is currently out of order.<BR><BR>Please contact  the Help Desk  for more information Tel.:".$TelNumber;
        break;
        case "4":
        $strlogin="No allocation planned for ".$tmp . " " .strtoupper($Session["Number"]). " -><BR>Please contact the Help Desk  for more information Tel.:".$TelNumber;
        break;
        case "5":
        $strlogin="You have entered a wrong password. <BR>Please try again.";
        break;
        case "6":
        $strlogin="The ".$tmp . " " .strtoupper($Session["Number"]). " is already opened From another user-><BR>Please contact the Help Desk  for more information Tel.:".$TelNumber;
        break;
        case "99":
        $strlogin="The system is currently not availiable .<BR> Please contact contact the Help Desk  for more information Tel.:".$TelNumber;
        break;
        default:
        $strlogin="Please log in.";
    }
    
    return $strlogin;
}


function strtoboolean($str) {
    if (is_bool($str)) {return $str;}

    $mystr = ''.$str;

    $rtr_str = false;

    if ($mystr == 'TRUE') {
        $rtr_str = true;
    }

    return $rtr_str;

}

?>
