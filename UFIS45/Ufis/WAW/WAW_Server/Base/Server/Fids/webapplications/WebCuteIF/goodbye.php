<?php 
include("include/manager.php");
include("include/maininclude.php");

if ($Session["userid"]) {
		session_unregister("Session");
		session_destroy();
}

$logger = LoggerManager::getRootLogger('CuteIf');



$smarty = new Smarty;

$smarty->caching=0;
$smarty->compile_check =true;
$smarty->debugging = false;
 
//Header
$smarty->assign("PageTitle",$PageTitle);
$smarty->assign("CharSet",$CharSet);
$smarty->assign("ContenetExpires",$Cexpires);
$smarty->assign("Type",$Session["Type"]);

$smarty->assign("RedGoodByeMsg",$RedGoodByeMsg);

$smarty->display('goodbye.tpl');


//Safely close all appenders with...

LoggerManager::shutdown();
?>