<?php

// For the complete timezones have a look in the etc/TimeZones.txt
$timezone="Europe/Warsaw";

$debug = "false";

$AjaxEnabled = true;

//----------------------- Connection Section ----------------------- 
//Define the Connection String
$DatabaseType="oci8";
$dbusername="ceda";
$dbpassword="ceda";
$database="UFIS";
$servername="localhost";



$DatabaseTypeMysql="mysqlt";
$dbusernameMysql="root";
$dbpasswordMysql="";
$databaseMysql="cute_fids";
// Oracle Home variable
//putenv("ORACLE_HOME=/opt/oracle/instantclient_10_2");
// For waw production
//putenv("ORACLE_HOME=/app/home/oracle_client/product/10.1.0.2");


//----------------------- Authorization   Section ----------------------- 
// AuthType Please uncoment what you want

//Authedication Type
//$AuthType="Mysql";
//$AuthType="Oracle";
//$AuthType="Variable"; // + File  , Set the Loginhopo
$AuthType="File";
//if AuthType== FILE || AuthType==Variable specifie the directory with the users,passwd file
$UserFilename="db/users.txt";

	
//THE HOPO  of the Airport
$hopo = "WAW";
$Loginhopo = "WAW"; // Must be provided from SITA

//----------------------- Logo   Section ----------------------- 
//Logo Selection 
$logoPath = "/cuteif_new/pics/logos/";
$logoRealPath = "/ceda/conf/fids/graphics";
//$logoRealPath = "/srv/www/htdocs/cuteIf/pics/logos";

$logoExt = "_L.GIF";
//$logoExt = array("_S.GIF");
$CheckinlogoSelection = true;
$GatelogoSelection = true;

//Possible Values DB , File ,DBflg
$logoSelectionType="DBflg";
$FLZTABuse = true;


//Use Oracle Function for the UTF strings (Function must be created in the ceda schema)
$UseUTFOracleFunction = false;

//Not Used yet
$LeftLogoFile = "";
//----------------------- FreeText  Remarks Section ----------------------- 
//FreeText 
$FreeTextSelection = false;

//----------------------- Checkin Predefined  Remarks Section ----------------------- 
//Predefined Remarks 
$CheckinPredefinedSelection = true;
$GatePredefinedSelection = true;

//Display Both Logo and Remarks if above  are true
// Enable / Disable Change Logos Predefine Remarks From the user (only for their curent session)
$Change_Frames = false;

//Display the logos assigned to specific Airline 
$DisplayLogoForSpecFlight = true;



//-----------------------REMT FOR FIDTAB Section ----------------------------------
$PRemarksRemTYPGate="'P'";  //The REMT value for GATES inside the FIDTAB
$PRemarksRemTYPCheckin="'C'";  //The REMT value for CHECKIN inside the FIDTAB


//Select Old Open Flights
$OldOpenFlights = false;

//-----------------------RTYP FOR FLDTAB Section ----------------------------------
$FLDRTYPGate ='GTD';
$FLDRTYPCheckin ='CIC';

$FLDUse = false;


// The Flight will be viewable for x minutes 
// After The user has put the Actual End Time for the Gate
$FlightTimeDiffGates = 5;

//Gate Check Allocation in Minutes (From - To) 
$GateFrom = -120;
$GateTo = 120;

//Checkin (dedicated) Check Allocation in Minutes (From - To) 
$CheckinFrom = -120;
$CheckinTo = 120;

//Checkin (Common) Check Allocation in Minutes (From - To)
$CommonCheckinFrom = -1;   // ********* IN DAYS *************
$CommonCheckinTo = 30;

//Checkin Alloc Check By TIF   (Not Used dor the Common Counters)
//The Allocation will be search via TIF
//field otherwise the schedule times / actuall open times will be used
$CheckinAllocTIF = false;

//Gate Alloc Check By TIF
//The Allocation will be search via TIF
//field otherwise the schedule times / actuall open times will be used
$GateAllocTIF = false;
// If the Allocation of the Gate is Outside the
// Timeframe
$GateAllocTIFExtraTime = 240;

// Flight Type FTYP 
$Ftyp = "'O'";


//Gates Remarks
$RemarkGateOpen ='GTO';
$RemarkGateBoarding ='BOA';
$RemarkGateFinalCall ='FNC';
$RemarkGateClose ='GCL';

//HTML CharSet
$CharSet = "iso-8859-2";

//----------------------- Cput   Section ----------------------- 
// The RSH server For the cput Command
// Leave blank ($RSHIP="";) if the cput command is in the local server

//$RSHIP="ufislh";
$RSHIP="";
// The comand tha we want to use in order to execute the remote script
//$RSCMD="/usr/bin/rexec -l username -p passwd ";
$RSCMD="/usr/bin/rsh -n ";

//$CputPath="/ceda/etc/cput45";
//The Cput File
$CputPath="/ceda/etc/kshCput";

// Page Title
$PageTitle="CUTE INTERFACE UFIS AS";

//----------------------- Messages ------------------------------



//$RedGoodByeMsg="PLEASE REMEMBER TO CLOSE THE CHECK-IN FLAP DOORS <br>THANK YOU !";
$RedGoodByeMsg="";
//HelpDesk TelNumber (it will be display in case of error or warning
$TelNumber = "0000";


//----------------------- No need to change anything below this Line ------------------------------

$RefreshRate = 60; //in sec

// Cput parameteres
//$wks="\'".$HostName."\'";
$wks="\'EXCO\'";
$cputuser="\'CUTEIF\'";   
$TW_START="\'0\'";
$TW_END="\'$hopo,TAB,CUTEIF\'";
$NoTimeout="\'2\'";


$mailfrom="cuteif";
$APPName="Web CUTE-IF";

$APPVersion="4.5.2.0";
$APPReleaseDate="08/10/2008";

?>
