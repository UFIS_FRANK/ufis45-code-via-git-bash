#error "\
This is no flight! \
If you want a new FLIGHT binary for ADR PS, create a sandbox of ADR LM and \
build and deliver that FLIGHT or replace this dummy text file by a link to \
the source file of ADR LM 'flight.c' by \
  'rm <path-of-PS-flight>/flight.c ; ln -s <path-of-LM-flight>/flight.c .' \
This can not be done by script, because it is not predictable, how you named \
the ADR LM and PS Sandboxes. \
If you want to proceed 'make' without FLIGHT binary, just remove this file \
from your sandbox (not from the project!)."
