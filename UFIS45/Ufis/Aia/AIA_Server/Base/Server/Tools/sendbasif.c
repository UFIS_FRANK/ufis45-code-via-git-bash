#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Aia/AIA_Server/Base/Server/Tools/sendbasif.c 1.1 2004/10/04 16:57:38SGT jim Exp  $";
#endif /* _DEF_mks_version */
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "ugccsma.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"

int   mod_id = 7601;
char  *mod_name = "basif";
int   debug_level = DEBUG;

/* main(int argc,char **argv) */
MAIN
{
	int ilRC = RC_SUCCESS;
	int ilEventSize = 0;

	EVENT   *prlEvent  = NULL;
	BC_HEAD *prlBchead = NULL;
	CMDBLK  *prlCmdblk = NULL;

	if(argc != 2)
	{
		printf("usage: sendbasif [READ_REGISTER|RESET|Command] \n");
		fflush(stdout);
		exit (1);
	} /* end if */
         
   
	ilRC = init_que();
	if(ilRC == RC_SUCCESS)
	{
		ilEventSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK);

		prlEvent = calloc(1,ilEventSize);
		if(prlEvent != NULL)
		{
			prlBchead = (BC_HEAD *) ((char *) prlEvent + sizeof(EVENT));
			prlCmdblk = (CMDBLK *) prlBchead->data;

			prlEvent->type = USR_EVENT;
			prlEvent->command = EVENT_DATA;
			prlEvent->originator = mod_id;
			prlEvent->retry_count = 0;
			prlEvent->data_offset = sizeof(EVENT);
			prlEvent->data_length = ilEventSize-sizeof(EVENT);

			sprintf(prlCmdblk->command,"%s",argv[1]);
		
			ilRC = que(QUE_PUT,7600,mod_id,PRIORITY_3,ilEventSize,(char *) prlEvent);
			if(ilRC != RC_SUCCESS)
			{
				printf("que(QUE_PUT failed <%d>\n",ilRC);
				fflush(stdout);
				exit (1);
			}/* end of if */
		}else{
			printf("alloc <%d> bytes failed\n",ilEventSize);
			fflush(stdout);
			exit (1);
		} /* end if */
	}else{
		printf("queue not available\n");
		fflush(stdout);
		exit (1);
	} /* end if */

	exit(0);

} /* end */

