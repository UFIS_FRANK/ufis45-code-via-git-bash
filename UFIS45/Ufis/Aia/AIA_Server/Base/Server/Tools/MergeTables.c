#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Aia/AIA_Server/Base/Server/Tools/MergeTables.c 1.1 2004/10/04 16:57:34SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ******************************************************************** */
/*									*/
/* CEDA Utility Program         					*/
/*									*/
/* Author		:				*/
/* Date			:					*/
/* Description		:						*/
/*									*/
/* Update history	:						*/
/*									*/
/* ******************************************************************** */

/* This program is a CEDA utility program */

/* ATTENTION: REMARK */
/* due to problems on HPUX (memory fault) */
/* all header files of a 'big' ceda process */
/* are included. also the use of macros MAIN */
/* and INITIALIZE is nessessary to avoid core dumps */

#define U_MAIN
#define UGCCS_PRG
#define STH_USE


#include <stdio.h>  
#include <malloc.h> 
#include <errno.h>  
#include <signal.h> 
#include <ctype.h>  
#include <string.h> 
#include "ugccsma.h"
#include "msgno.h"  
#include "glbdef.h" 
#include "quedef.h" 
#include "uevent.h" 
#include "timdef.h" 
#include "sthdef.h" 
#include "router.h" 
#include <time.h>   

#include <stdlib.h>
#include "db_if.h" 
#include "tools.h" 
#include "calutl.h"
#include "loghdl.h"

#include "queutil.h"      
#include "action_tools.h" 
#include "loghdl.h"       
#include "syslib.h"       

/* The master header file */

#include  "sqlhdl.h"


/* ******************************************************************** */
/* Global variables							*/
/* ******************************************************************** */
FILE *outp;
static char pcgSourceExt[16] = "XXX";
static char pcgDestExt[16] = "TAB";


/* ******************************************************************** */
/* External variables							*/
/* ******************************************************************** */
extern char sql_buf[];
extern char data_area[];


/* ******************************************************************** */
/* Function Prototypes							*/
/* ******************************************************************** */
extern int get_real_item(char *, char*, int);
extern void str_trm_all(char *, char *, int);
extern void str_chg_upc(char *);

static void init_report(void);
static void terminate(void);

static void CheckMaxUrno(char *pcpTblLst, int ipSet);
static void CheckDupUrnos(char *pcpTblLst);
static void SetNewUrnos(char *pcpTblLst);
static void CheckHopoValue(char *pcpTblLst);
static void FillHopoValue(char *pcpTblLst,int ipFlag);
static void DropImportedTables(char *pcpTblLst);
static void TruncateTables(char *pcpTblLst);
static void TransferRecords(char *pcpTblLst);

static void  str_app_str(char *, int *, char *, int, int, char *);
static void BuildItemBuffer(char *, char *, int, char *);
static void CheckShmData(void);

/* ******************************************************************** */
/*									*/
/* The MAIN program							*/
/*									*/
/* ******************************************************************** */
int mod_id=9999;
char *mod_name="MergeTables ";
int ctrl_sta=0;
int debug_level=0;

/* main(int argc,char **argv) */
MAIN
{
INITIALIZE; /* General initialization */
  if (argc < 2 )
  {
    printf("usage: MergeTables [-all] SourceTableExtension\n");
    printf("functioncalls:     [-seturno] \n");
    printf("                   [-transfer] \n");
    printf("                   [-sethopo] \n");
    printf("                   [-drop] \n");
    printf("                   [-maxurno] \n");
    printf("                   [-chkhopo] \n");
    printf("                   [-chkurno] \n");
    exit(0);
  } /* end if */

  if (argc < 3 )
  {
    printf("missing: SourceTableExtension\n");
    exit(0);
  } /* end if */

  if (strlen(argv[2]) != 3)
  {
    printf("Invalid SourceTableExtension\n");
    exit(0);
  } /* end if */

  strcpy(pcgSourceExt,argv[2]);
  str_chg_upc(pcgSourceExt);

  init_report();

  if (strcmp(pcgSourceExt,"TAB") == 0)
  {
    if (strstr("-setacnu",argv[1]) != NULL)
    {
      CheckMaxUrno("-all",TRUE);
    } /* end if */

    if (strstr("-maxurno-all",argv[1]) != NULL)
    {
      CheckMaxUrno("-all",FALSE);
    } /* end if */

    if (strstr("-chkurno-all",argv[1]) != NULL)
    {
      CheckDupUrnos("-all");
    } /* end if */

    if (strstr("-chkhopo-all",argv[1]) != NULL)
    {
      CheckHopoValue("-all");
    } /* end if */

    if (strstr("-trunc",argv[1]) != NULL)
    {
      TruncateTables("-all");
    } /* end if */

    if (strstr("-chkshm",argv[1]) != NULL)
    {
      CheckShmData();
    } /* end if */

  } /* end if */
  else
  {
    if (strstr("-seturno-all",argv[1]) != NULL)
    {
      SetNewUrnos("-all");
    } /* end if */

    if (strstr("-transfer-all",argv[1]) != NULL)
    {
      TransferRecords("-all");
    } /* end if */

    if (strstr("-sethopo-all",argv[1]) != NULL)
    {
      FillHopoValue("-all",TRUE);
    } /* end if */

    if (strstr("-drop",argv[1]) != NULL)
    {
      DropImportedTables("-all");
    } /* end if */

    if (strstr("-maxurno-all",argv[1]) != NULL)
    {
      CheckMaxUrno("-all",FALSE);
    } /* end if */

    if (strstr("-chkhopo",argv[1]) != NULL)
    {
      CheckHopoValue("-all");
    } /* end if */

    if (strstr("-chkurno",argv[1]) != NULL)
    {
      CheckDupUrnos("-all");
    } /* end if */

    if (strstr("-trunc",argv[1]) != NULL)
    {
      TruncateTables("-all");
    } /* end if */

  } /* end else */


  terminate();

} /* end of main */

/* ******************************************************************** */
/* The initialization routine						*/
/* ******************************************************************** */

static void init_report()
{
char *pclLogFile = "MergeTables.log";

       outp = fopen(pclLogFile,"w");                  
       if (outp != NULL)                             
       {                                                   
         printf("opened logfile <%s>\n\n",pclLogFile);
       } /* end if */                                      
       else                                                
       {                                                   
         outp = stdout;
       } /* end else */                                    


	while (init_db()) {
		sleep(5);		
	} /* end while */

  return;
} /* end of initialize */

/* ******************************************************************** */
/* ******************************************************************** */
static void DropImportedTables(char *pcpTblLst)
{
  int rc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  int i = 0;
  int ilChrPos = 0;
  int ilTblCnt = 0;
  int ilTbl = 0;
  int ilLen = 0;
  short slFkt = 0;
  short slCursor = 0;
  short slUrnoCursor = 0;
  char pclFldNam[64];
  char pclUsedTables[4096];
  char pclTableFields[4096];
  char pclSourceName[64];
  char pclDestName[64];
  char pclNewUrno[64];
  char pclMySqlBuf[128];

  fprintf(outp,"\nDROP TRANSFERRED TABLES BEGIN\n");
  fflush(outp);
  fprintf(outp,"TABLES: <%s>\n\n",pcpTblLst);
  fflush(outp);
  if (strcmp(pcpTblLst,"-all") == 0)
  {
    sprintf(sql_buf,"SELECT TABLE_NAME FROM USER_TABLES "
                    "WHERE TABLE_NAME LIKE '%%%s' "
                    "ORDER BY TABLE_NAME",pcgSourceExt);
    pclUsedTables[0] = 0x00;
    ilChrPos = 0;
    slCursor = 0;
    slFkt=START;
    while((rc=sql_if(slFkt,&slCursor,sql_buf,data_area)) == DB_SUCCESS )
    {
      str_trm_all(data_area," ",TRUE);
      str_app_str(pclUsedTables, &ilChrPos, data_area, 0, 0, ",");
      slFkt = NEXT;
    } /* end while */
    close_my_cursor(&slCursor);
    if (ilChrPos > 0)
    {
      ilChrPos--;
    } /* end if */
    pclUsedTables[ilChrPos] = 0x00;
    fprintf(outp,"TABLES: <%s>\n\n",pclUsedTables);
    fflush(outp);
    ilTblCnt = field_count(pclUsedTables);
    for (ilTbl = 1; ilTbl <= ilTblCnt; ilTbl++)
    {
      ilLen = get_real_item(pclSourceName,pclUsedTables,ilTbl);
      if (ilLen > 0)
      {
        sprintf(sql_buf,"DROP TABLE %s ", pclSourceName);
        fprintf(outp,"SQL: <%s>\n\n",sql_buf);
        fflush(outp);

        data_area[0] = 0x00;
        slCursor = 0;
        slFkt=START;
        rc = sql_if(0,&slCursor,sql_buf,data_area);
        if (rc == DB_SUCCESS)
        {
          commit_work();
        } /* end if */
        else
        {
          /* OUUUPS SORRY TO LATE*/
          rollback();
          fprintf(outp,"\nORACLE ERROR ???\n\n\n");
          fflush(outp);
        } /* end else */
        close_my_cursor(&slCursor);
      } /* end if */
    } /* end for */
  } /* end if */

  fprintf(outp,"\nDROP TRANSFERRED TABLES END\n\n");
  fflush(outp);

  return;
} /* End of DropImportedTables */

/* ******************************************************************** */
/* ******************************************************************** */
static void TruncateTables(char *pcpTblLst)
{
  int rc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  int i = 0;
  int ilChrPos = 0;
  int ilTblCnt = 0;
  int ilTbl = 0;
  int ilLen = 0;
  short slFkt = 0;
  short slCursor = 0;
  short slUrnoCursor = 0;
  char pclFldNam[64];
  char pclUsedTables[4096];
  char pclTableFields[4096];
  char pclSourceName[64];
  char pclDestName[64];
  char pclNewUrno[64];
  char pclMySqlBuf[128];

  fprintf(outp,"\nTRUNCATE TABLES BEGIN\n");
  fflush(outp);
  fprintf(outp,"TABLES: <%s>\n\n",pcpTblLst);
  fflush(outp);
  if (strcmp(pcpTblLst,"-all") == 0)
  {
    sprintf(sql_buf,"SELECT TABLE_NAME FROM USER_TABLES "
                    "WHERE TABLE_NAME LIKE '%%%s' "
                    "ORDER BY TABLE_NAME",pcgSourceExt);
    pclUsedTables[0] = 0x00;
    ilChrPos = 0;
    slCursor = 0;
    slFkt=START;
    while((rc=sql_if(slFkt,&slCursor,sql_buf,data_area)) == DB_SUCCESS )
    {
      str_trm_all(data_area," ",TRUE);
      str_app_str(pclUsedTables, &ilChrPos, data_area, 0, 0, ",");
      slFkt = NEXT;
    } /* end while */
    close_my_cursor(&slCursor);
    if (ilChrPos > 0)
    {
      ilChrPos--;
    } /* end if */
    pclUsedTables[ilChrPos] = 0x00;
    fprintf(outp,"TABLES: <%s>\n\n",pclUsedTables);
    fflush(outp);
    ilTblCnt = field_count(pclUsedTables);
    for (ilTbl = 1; ilTbl <= ilTblCnt; ilTbl++)
    {
      ilLen = get_real_item(pclSourceName,pclUsedTables,ilTbl);
      if (ilLen > 0)
      {
        sprintf(sql_buf,"TRUNCATE TABLE %s ", pclSourceName);
        fprintf(outp,"SQL: <%s>\n\n",sql_buf);
        fflush(outp);

        data_area[0] = 0x00;
        slCursor = 0;
        slFkt=START;
        rc = sql_if(0,&slCursor,sql_buf,data_area);
        if (rc == DB_SUCCESS)
        {
          commit_work();
        } /* end if */
        else
        {
          /* OUUUPS SORRY TO LATE*/
          rollback();
          fprintf(outp,"\nORACLE ERROR ???\n\n\n");
          fflush(outp);
        } /* end else */
        close_my_cursor(&slCursor);
      } /* end if */
    } /* end for */
  } /* end if */

  fprintf(outp,"\nTRUNCTE TABLES END\n\n");
  fflush(outp);

  return;
} /* end of TruncateTables */

/* ******************************************************************** */
/* ******************************************************************** */
static void SetNewUrnos(char *pcpTblLst)
{
  int rc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  int i = 0;
  int ilChrPos = 0;
  int ilTblCnt = 0;
  int ilTbl = 0;
  int ilLen = 0;
  short slFkt = 0;
  short slCursor = 0;
  short slUrnoCursor = 0;
  char pclFldNam[64];
  char pclUsedTables[4096];
  char pclTableFields[4096];
  char pclSourceName[64];
  char pclDestName[64];
  char pclNewUrno[64];
  char pclMySqlBuf[128];

  fprintf(outp,"\nSET NEW URNO's BEGIN\n");
  fflush(outp);
  fprintf(outp,"TABLES: <%s>\n\n",pcpTblLst);
  fflush(outp);

  if (strcmp(pcpTblLst,"-all") == 0)
  {
    sprintf(sql_buf,"SELECT TABLE_NAME FROM USER_TAB_COLUMNS "
                    "WHERE TABLE_NAME LIKE '%%%s' AND COLUMN_NAME='URNO' "
                    "ORDER BY TABLE_NAME",pcgSourceExt);
    pclUsedTables[0] = 0x00;
    ilChrPos = 0;
    slCursor = 0;
    slFkt=START;
    while((rc=sql_if(slFkt,&slCursor,sql_buf,data_area)) == DB_SUCCESS )
    {
      str_trm_all(data_area," ",TRUE);
      str_app_str(pclUsedTables, &ilChrPos, data_area, 0, 0, ",");
      slFkt = NEXT;
    } /* end while */
    close_my_cursor(&slCursor);
    if (ilChrPos > 0)
    {
      ilChrPos--;
    } /* end if */
    pclUsedTables[ilChrPos] = 0x00;
    fprintf(outp,"TABLES: <%s>\n\n",pclUsedTables);
    fflush(outp);
    ilTblCnt = field_count(pclUsedTables);
    for (ilTbl = 1; ilTbl <= ilTblCnt; ilTbl++)
    {
      ilLen = get_real_item(pclSourceName,pclUsedTables,ilTbl);
      if (ilLen > 0)
      {
        strcpy(pclDestName,pclSourceName);
        strcpy(&pclDestName[3],pcgDestExt);
        fprintf(outp,"\nCOMPARING <%s> <%s>\n",pclDestName,pclSourceName);
        fflush(outp);
        sprintf(sql_buf,"SELECT URNO FROM %s "
                        "WHERE URNO IN (SELECT URNO FROM %s)",
                         pclDestName,pclSourceName);
        slCursor = 0;
        slFkt=START;
        while((rc=sql_if(slFkt,&slCursor,sql_buf,data_area)) == DB_SUCCESS )
        {
          ilGetRc = GetNextValues(pclNewUrno, 1);
          sprintf(pclMySqlBuf,"UPDATE %s SET URNO=%s WHERE URNO=%s",
                               pclSourceName,pclNewUrno,data_area);
          fprintf(outp,"DUP URNO: <%s> SET TO <%s>\n",data_area,pclNewUrno);
          fflush(outp);
          data_area[0] = 0x00;
          slUrnoCursor = 0;
          ilGetRc = sql_if(0,&slUrnoCursor,pclMySqlBuf,data_area);
          if (ilGetRc == DB_SUCCESS)
          {
            commit_work();
          } /* end if */
          else
          {
            rollback();
            fprintf(outp,"\nORACLE ERROR ???\n\n\n");
            fflush(outp);
          } /* end else */
          close_my_cursor(&slUrnoCursor);
          slFkt = NEXT;
        } /* end while */
        close_my_cursor(&slCursor);

      } /* end if */
    } /* end for */
  } /* end if */
  fprintf(outp,"\nSET NEW URNO's END\n\n");
  fflush(outp);

  return;
} /* End of SetNewUrnos */

/* ******************************************************************** */
/* ******************************************************************** */
static void CheckDupUrnos(char *pcpTblLst)
{
  int rc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  int i = 0;
  int ilChrPos = 0;
  int ilTblCnt = 0;
  int ilTbl = 0;
  int ilLen = 0;
  short slFkt = 0;
  short slCursor = 0;
  short slUrnoCursor = 0;
  char pclFldNam[64];
  char pclUsedTables[4096];
  char pclTableFields[4096];
  char pclSourceName[64];
  char pclDestName[64];
  char pclNewUrno[64];
  char pclMySqlBuf[128];

  fprintf(outp,"\nCHECK DUP URNO BEGIN\n");
  fflush(outp);
  fprintf(outp,"TABLES: <%s>\n\n",pcpTblLst);
  fflush(outp);

  if (strcmp(pcpTblLst,"-all") == 0)
  {
    sprintf(sql_buf,"SELECT TABLE_NAME FROM USER_TAB_COLUMNS "
                    "WHERE TABLE_NAME LIKE '%%%s' AND COLUMN_NAME='URNO' "
                    "ORDER BY TABLE_NAME",pcgSourceExt);
    pclUsedTables[0] = 0x00;
    ilChrPos = 0;
    slCursor = 0;
    slFkt=START;
    while((rc=sql_if(slFkt,&slCursor,sql_buf,data_area)) == DB_SUCCESS )
    {
      str_trm_all(data_area," ",TRUE);
      str_app_str(pclUsedTables, &ilChrPos, data_area, 0, 0, ",");
      slFkt = NEXT;
    } /* end while */
    close_my_cursor(&slCursor);
    if (ilChrPos > 0)
    {
      ilChrPos--;
    } /* end if */
    pclUsedTables[ilChrPos] = 0x00;
    fprintf(outp,"TABLES: <%s>\n\n",pclUsedTables);
    fflush(outp);
    ilTblCnt = field_count(pclUsedTables);
    for (ilTbl = 1; ilTbl <= ilTblCnt; ilTbl++)
    {
      ilLen = get_real_item(pclSourceName,pclUsedTables,ilTbl);
      if (ilLen > 0)
      {
        strcpy(pclDestName,pclSourceName);
        strcpy(&pclDestName[3],pcgDestExt);
        fprintf(outp,"\nCOMPARING <%s> <%s>\n",pclDestName,pclSourceName);
        fflush(outp);
        sprintf(sql_buf,"SELECT URNO FROM %s "
                        "WHERE URNO IN (SELECT URNO FROM %s)",
                         pclDestName,pclSourceName);
        slCursor = 0;
        slFkt=START;
        while((rc=sql_if(slFkt,&slCursor,sql_buf,data_area)) == DB_SUCCESS )
        {
          fprintf(outp,"DUP URNO: <%s> \n",data_area);
          fflush(outp);
          slFkt = NEXT;
        } /* end while */
        close_my_cursor(&slCursor);

      } /* end if */
    } /* end for */
  } /* end if */

  fprintf(outp,"\nCHECK DUP URNO END\n\n");
  fflush(outp);
  return;
} /* End of CheckDupUrnos */

/* ******************************************************************** */
/* ******************************************************************** */
static void CheckMaxUrno(char *pcpTblLst, int ipSet)
{
  int rc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  int i = 0;
  int ilChrPos = 0;
  int ilTblCnt = 0;
  int ilTbl = 0;
  int ilLen = 0;
  long llActUrno = 0;
  long llMaxUrno = 0;
  short slFkt = 0;
  short slCursor = 0;
  short slUrnoCursor = 0;
  char pclFldNam[64];
  char pclUsedTables[4096];
  char pclTableFields[4096];
  char pclSourceName[64];
  char pclDestName[64];
  char pclNewUrno[64];
  char pclCmt[64];
  char pclMySqlBuf[128];

  fprintf(outp,"\nCHECK MAX URNO BEGIN\n");
  fflush(outp);
  fprintf(outp,"TABLES: <%s>\n\n",pcpTblLst);
  fflush(outp);

  if (strcmp(pcpTblLst,"-all") == 0)
  {
    sprintf(sql_buf,"SELECT TABLE_NAME FROM USER_TAB_COLUMNS "
                    "WHERE TABLE_NAME LIKE '%%%s' AND COLUMN_NAME='URNO' "
                    "ORDER BY TABLE_NAME",pcgDestExt);
    pclUsedTables[0] = 0x00;
    ilChrPos = 0;
    slCursor = 0;
    slFkt=START;
    while((rc=sql_if(slFkt,&slCursor,sql_buf,data_area)) == DB_SUCCESS )
    {
      str_trm_all(data_area," ",TRUE);
      str_app_str(pclUsedTables, &ilChrPos, data_area, 0, 0, ",");
      slFkt = NEXT;
    } /* end while */
    close_my_cursor(&slCursor);
    if (ilChrPos > 0)
    {
      ilChrPos--;
    } /* end if */
    pclUsedTables[ilChrPos] = 0x00;
    fprintf(outp,"TABLES: <%s>\n\n",pclUsedTables);
    fflush(outp);
    ilTblCnt = field_count(pclUsedTables);
    for (ilTbl = 1; ilTbl <= ilTblCnt; ilTbl++)
    {
      ilLen = get_real_item(pclSourceName,pclUsedTables,ilTbl);
      if (ilLen > 0)
      {
        sprintf(sql_buf,"SELECT MAX(TO_NUMBER(URNO)) FROM %s",pclSourceName);
        slCursor = 0;
        slFkt=START;
        if((rc=sql_if(slFkt,&slCursor,sql_buf,data_area)) == DB_SUCCESS)
        {
          pclCmt[0] = 0x00;
          if (strstr(data_area," ") != NULL)
          {
            strcpy(pclCmt,"URNO AS CHAR FIELD");
          } /* end if */
          fprintf(outp,"<%s> MAX URNO: <%s> %s \n",
                        pclSourceName,data_area,pclCmt);
          fflush(outp);
          llActUrno = atol(data_area);
          if (llActUrno > llMaxUrno)
          {
            llMaxUrno = llActUrno;
          } /* end if */
        } /* end if */
        else
        {
          fprintf(outp,"\nORACLE ERROR ???\n\n\n");
          fflush(outp);
        } /* end else */
        close_my_cursor(&slCursor);
      } /* end if */
    } /* end for */
   
    fprintf(outp,"\n<RESULT> MAX URNO: <%d> \n",llMaxUrno);
    fflush(outp);
    sprintf(sql_buf,"SELECT ACNU FROM NUMTAB WHERE KEYS='SNOTAB'");
    slCursor = 0;
    slFkt=START;
    if((rc=sql_if(slFkt,&slCursor,sql_buf,data_area)) == DB_SUCCESS)
    {
      fprintf(outp,"<NUMTAB> ACT ACNU: <%s> \n",data_area);
      fflush(outp);
      llActUrno = atol(data_area);
      if (llMaxUrno > llActUrno)
      {
        if (ipSet == TRUE)
        {
          llActUrno = llMaxUrno + 1000;
          sprintf(pclMySqlBuf,"UPDATE NUMTAB SET ACNU=%d WHERE KEYS='SNOTAB'",
                               llActUrno);
          data_area[0] = 0x00;
          slUrnoCursor = 0;
          ilGetRc = sql_if(0,&slUrnoCursor,pclMySqlBuf,data_area);
          if (ilGetRc == DB_SUCCESS)
          {
            fprintf(outp,"\nNUMTAB.ACNU UPDATED TO <%d>\n\n",llActUrno);
            fflush(outp);
            commit_work();
          } /* end if */
          else
          {
            rollback();
            fprintf(outp,"\nORACLE ERROR UPDATING NUMTAB.ACNU ???\n\n\n");
            fflush(outp);
          } /* end else */
          close_my_cursor(&slUrnoCursor);
        } /* end if */
        else
        {
          fprintf(outp,"\nWARNING: NUMTAB.ACNU SHOULD BE UPDATED\n\n");
          fflush(outp);
        } /* end else */
      } /* end if */
    } /* end if */
    else
    {
      fprintf(outp,"\nORACLE ERROR READING FROM NUMTAB ???\n\n\n");
      fflush(outp);
      if (ipSet == TRUE)
      {
        llActUrno = llMaxUrno + 1000;
        sprintf(pclMySqlBuf,"INSERT INTO NUMTAB (ACNU,FLAG,KEYS,MINN,MAXN)"
                            " VALUES "
                            "(%d,' ','SNOTAB',1000,9999999999)", llActUrno);
        data_area[0] = 0x00;
        slUrnoCursor = 0;
        ilGetRc = sql_if(0,&slUrnoCursor,pclMySqlBuf,data_area);
        if (ilGetRc == DB_SUCCESS)
        {
          fprintf(outp,"\nNUMTAB.ACNU INSERTED AS <%d>\n\n",llActUrno);
          fflush(outp);
          commit_work();
        } /* end if */
        else
        {
          rollback();
          fprintf(outp,"\nORACLE ERROR INSERTING NUMTAB.ACNU ???\n\n\n");
          fflush(outp);
        } /* end else */
        close_my_cursor(&slUrnoCursor);
      } /* end if */
    } /* end else */
    close_my_cursor(&slCursor);
  } /* end if */

  fprintf(outp,"\nCHECK MAX URNO END\n\n");
  fflush(outp);

  return;
} /* End of CheckMaxUrno */

/* ******************************************************************** */
/* ******************************************************************** */
static void CheckHopoValue(char *pcpTblLst)
{
  int rc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  int i = 0;
  int ilChrPos = 0;
  int ilTblCnt = 0;
  int ilTbl = 0;
  int ilLen = 0;
  short slFkt = 0;
  short slHopoFkt = 0;
  short slCursor = 0;
  short slHopoCursor = 0;
  char pclFldNam[64];
  char pclUsedTables[4096];
  char pclTableFields[4096];
  char pclSourceName[64];
  char pclDestName[64];
  char pclMySqlBuf[128];
  char pclMySqlDat[128];

  fprintf(outp,"\nCHECK HOPO VALUES BEGIN\n");
  fflush(outp);
  fprintf(outp,"TABLES: <%s>\n\n",pcpTblLst);
  fflush(outp);

  if (strcmp(pcpTblLst,"-all") == 0)
  {
    sprintf(sql_buf,
                    "SELECT TABLE_NAME FROM USER_TAB_COLUMNS "
                    "WHERE COLUMN_NAME='HOPO' "
                    "AND TABLE_NAME IN "
                    "(SELECT TABLE_NAME FROM USER_TAB_COLUMNS "
                    "WHERE TABLE_NAME LIKE '%%%s' "
                    "AND COLUMN_NAME='URNO') "
                    "ORDER BY TABLE_NAME",pcgDestExt);
    pclUsedTables[0] = 0x00;
    ilChrPos = 0;
    slCursor = 0;
    slFkt=START;
    while((rc=sql_if(slFkt,&slCursor,sql_buf,data_area)) == DB_SUCCESS )
    {
      str_trm_all(data_area," ",TRUE);
      str_app_str(pclUsedTables, &ilChrPos, data_area, 0, 0, ",");
      slFkt = NEXT;
    } /* end while */
    close_my_cursor(&slCursor);
    if (ilChrPos > 0)
    {
      ilChrPos--;
    } /* end if */
    pclUsedTables[ilChrPos] = 0x00;
    fprintf(outp,"TABLES: <%s>\n\n",pclUsedTables);
    fflush(outp);
    ilTblCnt = field_count(pclUsedTables);
    for (ilTbl = 1; ilTbl <= ilTblCnt; ilTbl++)
    {
      ilLen = get_real_item(pclSourceName,pclUsedTables,ilTbl);
      if (ilLen > 0)
      {
        fprintf(outp,"\n<%s> RECORDS: ",pclSourceName);
        fflush(outp);
        sprintf(sql_buf,"SELECT COUNT(*) FROM %s WHERE URNO>0", pclSourceName);
        data_area[0] = 0x00;
        slCursor = 0;
        slFkt=START;
        ilGetRc = sql_if(slFkt,&slCursor,sql_buf,data_area);
        if (ilGetRc == DB_SUCCESS)
        {
            fprintf(outp,"%8.8s",data_area);
            fflush(outp);
        } /* end if */
        fprintf(outp,"\n");
        fflush(outp);
        close_my_cursor(&slCursor);

        sprintf(pclMySqlBuf,"SELECT DISTINCT(HOPO) FROM %s WHERE URNO>0",
                             pclSourceName);
        slHopoCursor = 0;
        slHopoFkt=START;
        while((rc=sql_if(slHopoFkt,&slHopoCursor,pclMySqlBuf,pclMySqlDat)) ==
               DB_SUCCESS )
        {
          sprintf(sql_buf,"SELECT COUNT(*) FROM %s WHERE URNO>0 AND HOPO='%s'",
                           pclSourceName,pclMySqlDat);

          data_area[0] = 0x00;
          slCursor = 0;
          slFkt=START;
          ilGetRc = sql_if(slFkt,&slCursor,sql_buf,data_area);
          if (ilGetRc == DB_SUCCESS)
          {
              fprintf(outp,"   <%s> RECORDS: %8.8s\n",pclMySqlDat,data_area);
              fflush(outp);
          } /* end if */
          close_my_cursor(&slCursor);
          slHopoFkt = NEXT;
        } /* end while */
        close_my_cursor(&slHopoCursor);
      } /* end if */
    } /* end for */
  } /* end if */

  fprintf(outp,"\nCHECK HOPO VALUES END\n\n");
  fflush(outp);
  return;
} /* End of CheckHopoValue */

/* ******************************************************************** */
/* ******************************************************************** */
static void FillHopoValue(char *pcpTblLst, int ipFlag)
{
  int rc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  int i = 0;
  int ilChrPos = 0;
  int ilTblCnt = 0;
  int ilTbl = 0;
  int ilLen = 0;
  short slFkt = 0;
  short slCursor = 0;
  short slUrnoCursor = 0;
  char pclFldNam[64];
  char pclUsedTables[4096];
  char pclTableFields[4096];
  char pclSourceName[64];
  char pclDestName[64];
  char pclNewUrno[64];
  char pclMySqlBuf[128];

  fprintf(outp,"\nFILL HOPO VALUE BEGIN\n");
  fflush(outp);
  fprintf(outp,"TABLES: <%s>\n\n",pcpTblLst);
  fflush(outp);

  if (strcmp(pcpTblLst,"-all") == 0)
  {
    if (ipFlag == TRUE)
    {
      sprintf(sql_buf,"SELECT TABLE_NAME FROM USER_TABLES "
                      "WHERE TABLE_NAME LIKE '%%%s' "
                      "ORDER BY TABLE_NAME",pcgSourceExt);
    } /* end if */
    else
    {
      sprintf(sql_buf,"SELECT TABLE_NAME FROM USER_TAB_COLUMNS "
                    "WHERE TABLE_NAME LIKE '%%%s' AND COLUMN_NAME='HOPO' "
                    "ORDER BY TABLE_NAME",pcgDestExt);
    } /* end else */
    pclUsedTables[0] = 0x00;
    ilChrPos = 0;
    slCursor = 0;
    slFkt=START;
    while((rc=sql_if(slFkt,&slCursor,sql_buf,data_area)) == DB_SUCCESS )
    {
      str_trm_all(data_area," ",TRUE);
      str_app_str(pclUsedTables, &ilChrPos, data_area, 0, 0, ",");
      slFkt = NEXT;
    } /* end while */
    close_my_cursor(&slCursor);
    if (ilChrPos > 0)
    {
      ilChrPos--;
    } /* end if */
    pclUsedTables[ilChrPos] = 0x00;
    fprintf(outp,"TABLES: <%s>\n\n",pclUsedTables);
    fflush(outp);
    ilTblCnt = field_count(pclUsedTables);
    for (ilTbl = 1; ilTbl <= ilTblCnt; ilTbl++)
    {
      ilLen = get_real_item(pclSourceName,pclUsedTables,ilTbl);
      if (ilLen > 0)
      {
        if (ipFlag == TRUE)
        {
          strcpy(&pclSourceName[3],pcgDestExt);
        } /* end if */
        sprintf(sql_buf,"UPDATE %s SET HOPO='%s' WHERE HOPO=' '",
                         pclSourceName,pcgSourceExt);

        data_area[0] = 0x00;
        slCursor = 0;
        rc = sql_if(0,&slCursor,sql_buf,data_area);
        if (rc == DB_SUCCESS)
        {
          fprintf(outp,"SQL: <%s>\n\n",sql_buf);
          fflush(outp);
          commit_work();
        } /* end if */
        else
        {
          rollback();
          if (rc != ORA_NOT_FOUND)
          {
            fprintf(outp,"\nORACLE ERROR ???\n\n\n");
            fflush(outp);
          } /* end if */
        } /* end else */
        close_my_cursor(&slCursor);
      } /* end if */
    } /* end for */
  } /* end if */

  fprintf(outp,"\nFILL HOPO VALUE END\n\n");
  fflush(outp);

  return;
} /* End of FillHopoValue */

/* ******************************************************************** */
/* ******************************************************************** */
static void TransferRecords(char *pcpTblLst)
{
  int rc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  int ilInsRc = DB_SUCCESS;
  int ilDelRc = DB_SUCCESS;
  int i = 0;
  int ilChrPos = 0;
  int ilTblCnt = 0;
  int ilTbl = 0;
  int ilLen = 0;
  int ilCount = 0;
  int ilTotal = 0;
  short slFkt = 0;
  short slUrnoFkt = 0;
  short slCursor = 0;
  short slUrnoCursor = 0;
  char pclFldNam[64];
  char pclUsedTables[4096];
  char pclTableFields[4096];
  char pclSourceName[64];
  char pclDestName[64];
  char pclNewUrno[64];
  char pclMySqlBuf[128];

  fprintf(outp,"\nTRANSFER RECORDS BEGIN\n");
  fflush(outp);
  fprintf(outp,"TABLES: <%s>\n\n",pcpTblLst);
  fflush(outp);

  if (strcmp(pcpTblLst,"-all") == 0)
  {
    sprintf(sql_buf,"SELECT TABLE_NAME FROM USER_TABLES "
                    "WHERE TABLE_NAME LIKE '%%%s' "
                    "ORDER BY TABLE_NAME",pcgSourceExt);
    /*
    fprintf(outp,"SQL: <%s>\n\n",sql_buf);
    fflush(outp);
    */
    pclUsedTables[0] = 0x00;
    ilChrPos = 0;
    slCursor = 0;
    slFkt=START;
    while((rc=sql_if(slFkt,&slCursor,sql_buf,data_area)) == DB_SUCCESS )
    {
      str_trm_all(data_area," ",TRUE);
      str_app_str(pclUsedTables, &ilChrPos, data_area, 0, 0, ",");
      slFkt = NEXT;
    } /* end while */
    close_my_cursor(&slCursor);
    if (ilChrPos > 0)
    {
      ilChrPos--;
    } /* end if */
    pclUsedTables[ilChrPos] = 0x00;
    fprintf(outp,"TABLES: <%s>\n\n",pclUsedTables);
    fflush(outp);
  } /* end if */
  else 
  {
    strcpy(pclUsedTables,pcpTblLst);
  } /* end else */


  ilTblCnt = field_count(pclUsedTables);
  for (ilTbl = 1; ilTbl <= ilTblCnt; ilTbl++)
  {
    ilLen = get_real_item(pclSourceName,pclUsedTables,ilTbl);
    if (ilLen > 0)
    {
      strcpy(pclDestName,pclSourceName);
      strcpy(&pclDestName[3],pcgDestExt);

      data_area[0] = 0x00;
      sprintf(sql_buf,"SELECT COLUMN_NAME FROM USER_TAB_COLUMNS "
                      "WHERE TABLE_NAME='%s' ORDER BY COLUMN_NAME",
                       pclSourceName);
      strcpy(pclTableFields,"'");
      ilChrPos = 1;
      slCursor = 0;
      slFkt=START;
      while((rc=sql_if(slFkt,&slCursor,sql_buf,data_area)) == DB_SUCCESS )
      {
        str_trm_all(data_area," ",TRUE);
        str_app_str(pclTableFields, &ilChrPos, data_area, 0, 0, "','");
        slFkt = NEXT;
      } /* end while */
      close_my_cursor(&slCursor);
      if (ilChrPos > 1)
      {
        ilChrPos--;
        ilChrPos--;
      } /* end if */
      pclTableFields[ilChrPos] = 0x00;
      /*
      fprintf(outp,"FIELDS: <%s>\n\n",pclTableFields);
      fflush(outp);
      */

      if (ilChrPos > 1)
      {
        data_area[0] = 0x00;
        sprintf(sql_buf,"SELECT COLUMN_NAME FROM USER_TAB_COLUMNS "
                        "WHERE TABLE_NAME='%s' AND COLUMN_NAME IN (%s) "
                        "ORDER BY COLUMN_NAME",pclDestName,pclTableFields);
        /*
        fprintf(outp,"SQL: <%s>\n\n",sql_buf);
        fflush(outp);
        */

        pclTableFields[0] = 0x00;
        ilChrPos = 0;
        slCursor = 0;
        slFkt=START;
        while((rc=sql_if(slFkt,&slCursor,sql_buf,data_area)) == DB_SUCCESS )
        {
          str_trm_all(data_area," ",TRUE);
          str_app_str(pclTableFields, &ilChrPos, data_area, 0, 0, ",");
          slFkt = NEXT;
        } /* end while */
        close_my_cursor(&slCursor);
        if (ilChrPos > 0)
        {
          ilChrPos--;
        } /* end if */
        pclTableFields[ilChrPos] = 0x00;
        fprintf(outp,"<%s> --> <%s> TRANSFERRING FIELDS:\n<%s>\n",
                     pclSourceName,pclDestName,pclTableFields);
        fflush(outp);
        if (ilChrPos > 1)
        {
          ilCount = 0;

          sprintf(pclMySqlBuf,"SELECT URNO FROM %s ", pclSourceName);

          slUrnoCursor = 0;
          slUrnoFkt = START;
          while((ilGetRc =
                 sql_if(slUrnoFkt,&slUrnoCursor,pclMySqlBuf,pclNewUrno))
                 == DB_SUCCESS)
          {
            sprintf(sql_buf,"INSERT INTO %s (%s) "
                            "(SELECT %s FROM %s WHERE URNO=%s)",
                            pclDestName,pclTableFields,
                            pclTableFields,pclSourceName,pclNewUrno);
            /*
            fprintf(outp,"SQL: <%s>\n\n",sql_buf);
            fflush(outp);
            */

            data_area[0] = 0x00;
            slCursor = 0;
            ilInsRc = sql_if(0,&slCursor,sql_buf,data_area);
            close_my_cursor(&slCursor);

            ilDelRc = ilInsRc;
            if (ilInsRc == DB_SUCCESS)
            {
              sprintf(sql_buf,"DELETE FROM %s WHERE URNO=%s",
                               pclSourceName,pclNewUrno);
              data_area[0] = 0x00;
              slCursor = 0;
              ilDelRc = sql_if(0,&slCursor,sql_buf,data_area);
              close_my_cursor(&slCursor);
            } /* end if */
            if ((ilInsRc == DB_SUCCESS) && (ilDelRc == DB_SUCCESS))
            {
              ilCount++;
              commit_work();
            } /* end if */
            else
            {
              rollback();
              fprintf(outp,"\nORACLE ERROR ??? ROLLBACK !!!\n\n\n");
              fprintf(outp,"SQL: <%s>\n\n",sql_buf);
              fflush(outp);
            } /* end else */

            slUrnoFkt = NEXT;
          } /* end while */
          close_my_cursor(&slUrnoCursor);

          ilTotal += ilCount;
          fprintf(outp,"TRANSFERRED %d/%d RECORDS\n\n",ilCount,ilTotal);
          fflush(outp);
        } /* end if */
      } /* end if */
    } /* end if */
  } /* end for */

  fprintf(outp,"\nTRANSFER RECORDS END\n\n");
  fflush(outp);

  return;
} /* End of TransferRecords */

/* ******************************************************************** */
/* The Terminate routine						*/
/* ******************************************************************** */
static void terminate()
{
  fprintf(outp,"\n\n\n\n\nLOGOUT\n\n\n");
  fflush(outp);

	logoff();
	exit(1);
}

/* ******************************************************************** */
/* The append bytes routine						*/
/* ******************************************************************** */
static void str_app_str(char *dst, int *d_cp, char *src, int s_cp, int len, char *wrd)
{
	int i;
	*d_cp = strlen(dst);
	if (len < 1)
	{
		len = strlen(src) - s_cp;
	} /* end if */
	for (i=1;i<=len;i++){
		dst[*d_cp] = src[s_cp];
		(*d_cp)++;
		s_cp++;
	}/* end for */
	len = strlen(wrd);
	s_cp = 0;
	for (i=1;i<=len;i++){
		dst[*d_cp] = wrd[s_cp];
		(*d_cp)++;
		s_cp++;
	}/* end for */
	dst[*d_cp] = 0x00;
	return;
}/* end of str_app_str */

/* ******************************************************************** */
/* change NUL-Separator in an databuffer (form an Item-List)   */
/* ******************************************************************** */
static void BuildItemBuffer(char *pcpData, char *pcpFieldList, int ipNoOfFields,
                            char *pcpSepChr)
{
  int ilChrPos = 0;
  if (ipNoOfFields < 1)
  {
    ipNoOfFields = field_count(pcpFieldList);
  } /* end if */
  ipNoOfFields--;
  while (ipNoOfFields > 0)
  {
    if (pcpData[ilChrPos] == 0x00)
    {
      pcpData[ilChrPos] = pcpSepChr[0];
      ipNoOfFields--;
    } /* end if */
    ilChrPos++;
  } /* end while */
  return;
}/* end of BuildItemBuffer */

/* ******************************************************************** */
/* ******************************************************************** */
static void CheckShmData(void)
{
  int ilRC = RC_SUCCESS;
  int ilCount = 0;
  char pclResult[5*1024*1024];
  char pclOutFldLst[128];
  char pclTable[8];
  char pclKeyFld[128];
  char pclKeyDat[128];
  strcpy(pclTable,"ACRTAB");
  strcpy(pclOutFldLst,"REGN,ACT3,ACT5,ACTI,HOPO");
  strcpy(pclKeyFld,"HOPO");
  strcpy(pclKeyDat,"");
  ilRC = syslibSearchDbData(pclTable,pclKeyFld,pclKeyDat,
                            pclOutFldLst,pclResult,&ilCount,"\n");
  if(ilRC == RC_SUCCESS)
  {
    fprintf(outp,"\n-------------------\n%s\n-------------------\n",pclResult);
    fflush(outp);
  }
  else
  {
    fprintf(outp,"\nNO DATA FOUND!\n");
    fflush(outp);
  }
  return;
} /* end CheckShmData */



