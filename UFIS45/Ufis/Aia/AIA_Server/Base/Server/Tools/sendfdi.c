#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Aia/AIA_Server/Base/Server/Tools/sendfdi.c 1.1 2004/10/04 16:57:35SGT jim Exp  $";
#endif /* _DEF_mks_version */
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "netin.h"
/* #include "ctxhdl.h" */

int mod_id = 9999;
char  *mod_name = "sdm";
int debug_level = DEBUG;
FILE *outp = stdout;
EVENT	*out_event;
#define QUE_ID_OUT	8450

/* main(int argc,char **argv){ */
MAIN
{
  int rc;
  int len=0;
  FILE *fp;
  struct stat sta;

  BC_HEAD *bchd;		/* Broadcast header		*/
  CMDBLK *cmdblk;		/* Command block 		*/
  char 	*data;	/* Data Block			*/
  char 	selection[64];	/* Data Block			*/
  char 	fields[128];	/* Data Block			*/
  int msglen=0;      





  if ( argc != 4 ) {
    printf("\nUsage: send_a_file HdrTyp HOPO file_name\n\n");
    printf("       Header     HdrTyp\n");
    printf("       zczc          1  \n");
    printf("       Message RCV   2  \n");
    printf("       ^?RCV         3  \n");
    printf("       RCVD          4  \n");
    exit(0);
  } /* end if */

  if ( (fp=fopen(argv[3],"r"))== (FILE *)NULL){
    exit(printf("Error open %s \n",argv[3]));
  } /* end if */

  rc = fstat(fileno(fp),&sta);
	

  printf("Trying to send %s with %d bytes\n",argv[3],sta.st_size);

  init_que();

  sprintf(selection,"TELEX,%s",argv[1]);
  strcpy(fields,"WHAT EVER YOU NEED,1,2,3,4");

  len = sta.st_size + strlen(selection) + strlen(fields) + 3;
  msglen = len + sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 4;

  out_event = (EVENT *) calloc (1, msglen);
  if (out_event == NULL)
    {
      rc = RC_FAIL;
      printf("fil_send: Malloc:%s\n",strerror(errno));
    } /* fi */

  if (rc == RC_SUCCESS){

    bchd = (BC_HEAD *) (((char *)out_event) + sizeof(EVENT));
    cmdblk= (CMDBLK  *) ((char *)bchd->data);
    data = (char *) cmdblk->data;
	
    strcpy (cmdblk->command, "FDI");
    strcpy (cmdblk->tw_start, "TW,1,2,3");
    sprintf (cmdblk->tw_end, "%s,TAB,STXHDL",argv[2]);
    strcpy (bchd->recv_name, "EXCO1");
    strcpy (bchd->dest_name, "SITA1");


    strcpy (data, selection);
    data += strlen(data) + 1;
    strcpy (data, fields);
    data += strlen(data) + 1;

    rc = fread(data,sta.st_size,1,fp);

    bchd->rc = NETOUT_NO_ACK;
	
    out_event->data_length = msglen - sizeof(EVENT);

    out_event->command = EVENT_DATA;
    out_event->originator = 9999;
    out_event->data_offset = sizeof(EVENT);

    rc = que(QUE_PUT,QUE_ID_OUT,9999,PRIORITY_3,msglen,(char *)out_event);
	
  } /* fi */


  free (out_event);

  exit(printf("Rc from QUE_PUT = %d\n",rc));

}









