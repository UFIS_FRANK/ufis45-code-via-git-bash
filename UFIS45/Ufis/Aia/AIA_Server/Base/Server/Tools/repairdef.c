#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Aia/AIA_Server/Base/Server/Tools/repairdef.c 1.1 2004/10/04 16:57:36SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ******************************************************************** */
/*									*/
/* CEDA Utility Program         					*/
/*									*/
/* Author		:				*/
/* Date			:					*/
/* Description		:						*/
/*									*/
/* Update history	:						*/
/*									*/
/* ******************************************************************** */

/* This program is a CEDA utility program */

#define CEDA_PRG
#define STH_USE 

/* The master header file */

#include  "sqlhdl.h"
#include  "cli.h"


/* ******************************************************************** */
/* Global variables							*/
/* ******************************************************************** */
FILE *outp;
static char pcgTableFields[4096];
/* ******************************************************************** */
/* External variables							*/
/* ******************************************************************** */
extern char sql_buf[];
extern char data_area[];


/* ******************************************************************** */
/* Function Prototypes							*/
/* ******************************************************************** */
extern int get_real_item(char *, char*, int);
extern void str_trm_all(char *, char *, int);
extern void str_chg_upc(char *);

static void init_report(void);
static void handle_report(char *,char*);
static void terminate(void);

static void  str_app_str(char *, int *, char *, int, int, char *);
static void BuildItemBuffer(char *, char *, int, char *);

/* ******************************************************************** */
/*									*/
/* The MAIN program							*/
/*									*/
/* ******************************************************************** */
int mod_id=9999;
char *mod_name="repairdb ";
int ctrl_sta=0;
int debug_level=0;
char pcgPrjNam[64];

/* main(int argc,char **argv) */
MAIN
{
  INITIALIZE;
  STH_INIT();

	if (argc < 2 ) {
	  exit(printf("usage: repairdb [-all] [ListOfTables]\n"));
	} /* end if */

	init_report();
	handle_report(argv[1],argv[2]);
	terminate();

} /* end of main */

/* ******************************************************************** */
/* The initialization routine						*/
/* ******************************************************************** */

static void init_report()
{
  int ilRC = RC_SUCCESS;

	outp = stdout;
	while (init_db()) {
		sleep(5);		
	} /* end while */
  ilRC = tool_search_exco_data("ALL","TABEND",pcgPrjNam);
  if (ilRC != RC_SUCCESS)
  {
    fprintf(outp,"\n\nREADING TABEND FAILED: (%d)\n",ilRC);
  } /* end if */
  fprintf(outp,"\n\nTABEND: <%s>\n",pcgPrjNam);
  fflush(outp);

	return;
} /* end of initialize */

/* ******************************************************************** */
/* The handle routine						*/
/* ******************************************************************** */
static void handle_report(char *pcpTblLst, char *pcpActTyp)
{
  int	i,rc;
  int	cp = 0;
  int	fldCnt = 0;
  int	ilFldLen = 0;
  int	ilTblCnt = 0;
  int	ilTbl = 0;
  int	fndCnt;
  int   ilOutCnt = 0;
  int   ilDummy = 0;
  short   local_cursor,fkt;
  char pclFldNam[64];
  char pclFldDef[4096];
  char pclUsedTables[4096];
  char pclFldLen[16];
  char pclFldTyp[16];
  char pclShortName[64];
  char pclFullName[64];
  if (ilDummy == 0)
  {
    /* to avoid compiler warnings */
    strcpy(data_area,pcpActTyp);
  } /* end if */
  fprintf(outp,"\n\nTABLES: <%s>\n",pcpTblLst);
  fflush(outp);
  if (strcmp(pcpTblLst,"-all") == 0)
  {
    sprintf(sql_buf,"SELECT DISTINCT(TANA) FROM SYS%s ORDER BY TANA",pcgPrjNam);
    cp = 0;
    local_cursor = 0;
    fkt=START;
    while((rc=sql_if(fkt,&local_cursor,sql_buf,data_area)) == DB_SUCCESS )
    {
      str_trm_all(data_area," ",TRUE);
      str_app_str(pclUsedTables, &cp, data_area, 0, 0, ",");
      fkt = NEXT;
    } /* end while */
    close_my_cursor(&local_cursor);
    cp--;
    pclUsedTables[cp] = 0x00;
    fprintf(outp,"\n\nUSED TABLES:\n<%s>\n",pclUsedTables);
    fflush(outp);
  } /* end if */
  else
  {
    strcpy(pclUsedTables,pcpTblLst);
  } /* end else */
  ilTblCnt = field_count(pclUsedTables);
  for (ilTbl = 1; ilTbl <= ilTblCnt; ilTbl++)
  {
    (void)get_real_item(pclShortName,pclUsedTables,ilTbl);
    str_trm_all(pclShortName," ",TRUE);
    str_chg_upc(pclShortName);
    sprintf(pclFullName,"%s%s",pclShortName, pcgPrjNam);
    fprintf(outp,"\n\nFIELDS OF TABLE <%s>\n",pclFullName);
    fflush(outp);
    data_area[0] = 0x00;
    pcgTableFields[0] = 0x00;
    sprintf(sql_buf,"SELECT FINA FROM SYS%s WHERE TANA='%s'",
                     pcgPrjNam,pclShortName);
    ilOutCnt = 0;
    cp = 0;
    local_cursor = 0;
    fkt=START;
    while((rc=sql_if(fkt,&local_cursor,sql_buf,data_area)) == DB_SUCCESS )
    {
      str_trm_all(data_area," ",TRUE);
      str_app_str(pcgTableFields, &cp, data_area, 0, 0, ",");
      fprintf(outp,"%s,", data_area);
      fflush(outp);
      ilOutCnt++;
      if (ilOutCnt > 14)
      {
        fprintf(outp,"\n");
        fflush(outp);
        ilOutCnt = 0;
      } /* end if */
      fldCnt++;
      fkt = NEXT;
    } /* end while */
    close_my_cursor(&local_cursor);
    if (cp > 0)
    {
      cp--;
    } /* end if */
    pcgTableFields[cp] = 0x00;
    fprintf(outp,"\n\nNOW SETTING ORACLE DEFAULTS\n");
    fflush(outp);

    fldCnt = field_count(pcgTableFields);
    ilOutCnt = 0;
    cp = 0;
    for (i=1;i<=fldCnt;i++)
    {
      (void)get_real_item(pclFldNam,pcgTableFields,i);
      fprintf(outp,"%s,", pclFldNam);
      ilOutCnt++;
      if (ilOutCnt > 14)
      {
        fprintf(outp,"\n");
        ilOutCnt = 0;
      } /* end if */
      fflush(outp);
      sprintf(sql_buf,"SELECT DEFA,FELE,FITY FROM SYS%s "
                      "WHERE TANA='%s' AND FINA='%s'",
                       pcgPrjNam,pclShortName,pclFldNam);
      data_area[0] = 0x00;
      local_cursor = 0;
      rc = sql_if(START,&local_cursor,sql_buf,data_area);
      close_my_cursor(&local_cursor);
      BuildItemBuffer(data_area,"",3,",");
      (void)get_real_item(pclFldDef,data_area,1);
      (void)get_real_item(pclFldLen,data_area,2);
      (void)get_real_item(pclFldTyp,data_area,3);
      ilFldLen = atoi(pclFldLen);
      pclFldDef[ilFldLen] = 0x00;
      ilFldLen = strlen(pclFldDef);
      if (ilFldLen < 1)
      {
        if (pclFldTyp[0] != 'N')
        {
          strcpy(pclFldDef," ");
        } /* end if */
        else
        {
          strcpy(pclFldDef,"0");
        } /* end else */
      } /* end if */
      sprintf(sql_buf,"ALTER TABLE %s MODIFY (%s DEFAULT '%s')",
                           pclFullName, pclFldNam, pclFldDef);
      data_area[0] = 0x00;
      local_cursor = 0;
      rc = sql_if(START|REL_CURSOR|COMMIT,&local_cursor,sql_buf,data_area);
      if ( rc != DB_SUCCESS )
      {
        fprintf(outp,"\nSetting Field <%s> failed\n<%s>\n", pclFldNam,sql_buf);
        fflush(outp);
      } /* end if */
    } /* end for */
  } /* end for */
  fprintf(outp,"\n");
  fflush(outp);

  return;
} /* End of Handle Report */

/* ******************************************************************** */
/* The Terminate routine						*/
/* ******************************************************************** */
static void terminate()
{
	logoff();
	exit(1);
}

/* ******************************************************************** */
/* The append bytes routine						*/
/* ******************************************************************** */
static void str_app_str(char *dst, int *d_cp, char *src, int s_cp, int len, char *wrd)
{
	int i;
	*d_cp = strlen(dst);
	if (len < 1)
	{
		len = strlen(src) - s_cp;
	} /* end if */
	for (i=1;i<=len;i++){
		dst[*d_cp] = src[s_cp];
		(*d_cp)++;
		s_cp++;
	}/* end for */
	len = strlen(wrd);
	s_cp = 0;
	for (i=1;i<=len;i++){
		dst[*d_cp] = wrd[s_cp];
		(*d_cp)++;
		s_cp++;
	}/* end for */
	dst[*d_cp] = 0x00;
	return;
}/* end of str_app_str */

/* ******************************************************************** */
/* change NUL-Separator in an databuffer (form an Item-List)   */
/* ******************************************************************** */
static void BuildItemBuffer(char *pcpData, char *pcpFieldList, int ipNoOfFields,
                            char *pcpSepChr)
{
  int ilChrPos = 0;
  if (ipNoOfFields < 1)
  {
    ipNoOfFields = field_count(pcpFieldList);
  } /* end if */
  ipNoOfFields--;
  while (ipNoOfFields > 0)
  {
    if (pcpData[ilChrPos] == 0x00)
    {
      pcpData[ilChrPos] = pcpSepChr[0];
      ipNoOfFields--;
    } /* end if */
    ilChrPos++;
  } /* end while */
  return;
}/* end of BuildItemBuffer */
