#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Aia/AIA_Server/Base/Server/Kernel/cdiif.c 1.4 2006/03/28 19:02:52SGT akl Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* CCS Program Skeleton                                                       */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history : 24.02.98 Delete-Erweiterung                               */
/*						  13.03.98 Erweiterung fuer HAND										*/
/* 					  09.11.98 Aenderung bei GRS (Zeile 4724)                   */
/******************************************************************************/
/* This program is a CCS main program */
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_cdiif[] ="@(#) UFIS 4.4 (c) ABB AAT/I cdiif.c 44.3 / " __DATE__ "  " __TIME__ " / RBI-MCU";
/* be carefule with strftime or similar functions !!!                         */
/******************************************************************************/

#define U_MAIN
#define UGCCS_PRG
#define 	STH_USE
#define	__SHOW_PASSWD

/* The master header file */
#include  "cdiif.h"

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
FILE 	*outp       = NULL;
int  	debug_level = DEBUG;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */

/******************************************************************************/
/* my global variables                                                        */
/******************************************************************************/
/*-----------------------------------------------------------------------------
bit 2^0  -> 0x0001	-> memory for command structur
bit 2^1  -> 0x0002	-> memory for commands in command structur 
bit 2^2  -> 0x0004	-> memory for table structur 
bit 2^3  -> 0x0008	-> memory for field-pointer 
bit 2^4  -> 0x0010	-> memory for fields 
bit 2^5  -> 0x0020	-> memory for assignment structure  
bit 2^6  -> 0x0040	-> tcp/ip-socket for client
bit 2^7  -> 0x0080	-> memory for receive data 
bit 2^8  -> 0x0100	-> memory for data length
bit 2^9  -> 0x0200	->  
bit 2^10 -> 0x0400	-> memory for send data 
bit 2^11 -> 0x0800	-> memory for recovery 
bit 2^12 -> 0x1000	-> 
bit 2^13 -> 0x2000	-> 
bit 2^14 -> 0x4000	-> rprot file is open
bit 2^15 -> 0x8000	-> sprot file is open
-----------------------------------------------------------------------------*/
static UINT 		igStatus 			= iF_INITIALIZE;
static int  		igClientSocket 	= iF_INITIALIZE;
static int			igTcpSock			= iF_INITIALIZE;
static int			igRProtFile			= iF_INITIALIZE;
static int			igSProtFile			= iF_INITIALIZE;
static int			igCCOCheck			= iF_INITIALIZE;
static UINT			igLastCCONo			= iF_INITIALIZE;
static int			igRouterID			= iF_INITIALIZE;
static UINT			igDbgMode			= iF_INITIALIZE;
static int			igRestartTimeout  = 0;
static long			lgLastTeleNumber  = iINIT_TNUMBER;
static char			*pcgReceiveData 	= NULL;
static char			pcgFileName[iMIN];
static char			pcgServerName[iMIN];
static char			pcgCDIServerName[iMIN];
static char			pcgCCOName[iMIN];
static char			pcgDATName[iMIN];
static char			pcgCTLName[iMIN];
static char			pcgDELName[iMIN];
static char			pcgTABEnd[iMIN];
static char			pcgHomeAP[iMIN];
static char			pcgTmpTabName[iMIN];
static char			pcgDummy[iMIN];
static time_t		time1;
static time_t		time2;
static CDITMain		rgTM;
static TData		*prgData		= NULL;
static TRecover	*prgRecover	= NULL;
static TMix			*prgMix		= NULL;
static TFail		rgFail;

/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/
extern int  	init_que(void);                      /* Attach to CCS queues */
extern int  	que(int,int,int,int,int,char*);      /* CCS queuing routine  */
extern int		snap(char*, long, FILE*);
extern void 	str_trm_all(char *, char *, int);

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int  	init_cdiif(char *);				     /* Initialize */
static int		Reset(void);                       /* Reset program          */
static void		Terminate(UINT);			       	  /* Terminate program      */
static void		HandleSignal(int);                 /* Handles signals        */
static void		HandleErr(int);                    /* Handles general errors */
static void		HandleQueErr(int *);               /* Handles queuing errors */
static int		HandleData(void);                  /* Handles event data     */
static void 	HandleQueues(void);                /* Waiting for Sts.-switch*/

/******************************************************************************/
/* my function prototypes                                                     */
/******************************************************************************/
static int  	CDIServer(void);
static int 		ReceiveTcpIp(UINT);
static int	 	CheckQueStatus(int);
static int		CheckReceivedHeader(THeader *);
static int	 	HandleReceivedCommand(char *);
static TCCO 	*BuildCCO(char *);
static TEOD 	*BuildEOD(void);
static TEND 	*BuildEND(char *);
static void 	BuildMix(char *, char *, ULONG);
static void 	BuildData(char *, char *, char *, ULONG);
static void 	BuildHeader(THeader *, char *, ULONG);
static int	 	CheckConnection(void);
static int 		CreateTCPConnection(void);
static void 	CloseTCPConnection(void);
static int 		SearchCommandNumber(char *);
static int 		GetMaxLength(int, char, char *);
static int 		HandleMemoryRecovery(UINT, UINT *, TRecover *, TRecover **);
static int 		HandleFileRecovery(UINT, UINT *, TRecover *, TRecover **);
static int 		DeleteRowInFile(int, int);
static TERR		*BuildERR(char *);
static void 	ToRProtFile(UINT);
static void 	ToSProtFile(char *, UINT);
static int		WriteERRToClient(char *);
static TACK 	*BuildACK(void);
static int		WriteDataTelegramToClient(char *, int, int, 
													  char *, char *, int, ULONG);
static int		WriteMixTelegramToClient(int, int, char *, char *, int, ULONG);
static int		WriteDelTelegramToClient(int, int, int, 
													 char *, char *, int, ULONG);
static int 		SendFileAndWriteEOD(int, char *, char *);
static int		WriteGRSToClient(long);
static int		GetInitialBaggageData(void);
static int 		TcpWaitTimeout(int , int , int);
static int 		UtcToLocal(char *);
static int     LocalToUtc(char *oldstr, char *newstr);
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int				ilRC  = RC_SUCCESS;			
	int				ilCnt = 0;
	int				ilCDI = -1;
	char				pclTmpBuf[iMIN_BUF_SIZE];
	char				pclCfgFile[iMIN_BUF_SIZE];
	char				*pclCfgPath = NULL;

	/* General initialization	*/
	INITIALIZE;			

	/* for all signals */
	SetSignals(HandleSignal);

	/* which virtual CDIIF am I? */
	ilCDI = -1;
	memset ((void*)pclCfgFile, 0x00, iMIN_BUF_SIZE);
	if ((pclCfgPath = getenv("CFG_PATH")) == NULL)
	{
		/* environment not set */
		dbg(TRACE, "<init_cdiif> error reading environment (CFG_PATH)");	
		Terminate(30);
	}
	else
	{
		/* found environment-variable */
		/* build path/filename */
		sprintf(pclCfgFile, "%s/%s.cfg", pclCfgPath, argv[0]);
	}
	dbg(DEBUG,"<init_cdiif> virtual config file is: <%s>", pclCfgFile);

	if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "virtual_cdi_no", CFG_STRING, pclTmpBuf)) == RC_SUCCESS)
	{
		dbg(DEBUG,"<init_cdiif> virtual_cdi_no is: <%s>", pclTmpBuf);
		ilCDI = atoi(pclTmpBuf);
	}
	else
	{
		dbg(DEBUG,"<init_cdiif> no virtual_cdi_no configured");
	}

if (ilCDI <= 0)
{
	/* build name of entry in sgs.tab */
	memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
	sscanf(&argv[0][5], "%d", &ilCDI);
}
if (ilCDI <= 0)
{
	ilCDI = 1;
}
	sprintf(pclTmpBuf,"CDI%d", ilCDI);

	/* set name of this server (cdiif1, cdiif2, ...) */
	/* and name for CCO, DAT, and CTL-Telegrams */
	/* for GetUniqueNumber!! */
	memset((void*)pcgServerName, 0x00, iMIN);
	memset((void*)pcgCDIServerName, 0x00, iMIN);
	sprintf(pcgServerName, "CDITMP%d", ilCDI);
	sprintf(pcgCDIServerName, "CDIIF%d", ilCDI);

	/* only for debugging */
	dbg(DEBUG,"<MAIN> ServerName for GetUniqueNumber: <%s>", pcgServerName);
	dbg(DEBUG,"<MAIN> CDIServerName for BC-Head: <%s>", pcgCDIServerName);

	/* get info from sgs.tab */
	memset((void*)pcgFileName, 0x00, iMIN);
	if ((ilRC = tool_search_exco_data("CDI", pclTmpBuf, pcgFileName)) != RC_SUCCESS)
	{
		dbg(TRACE, "<MAIN> can't find entry (%s) in sgs.tab", pclTmpBuf);
		Terminate(30);
	}
dbg(TRACE,"<MAIN> pcgFileName = <%s>", pcgFileName);

	/* next info from SGS.TAB, search TABEND... */
	memset((void*)pcgTABEnd, 0x00, iMIN);
	if ((ilRC = tool_search_exco_data("ALL", "TABEND", pcgTABEnd)) != RC_SUCCESS)
	{
		dbg(TRACE, "<MAIN> can't find entry (\"TABEND\") in sgs.tab");
		Terminate(30);
	}
	dbg(DEBUG,"<MAIN> found TABEND: <%s>", pcgTABEnd);

	/* read HomeAirPort from SGS.TAB */
	memset((void*)pcgHomeAP, 0x00, iMIN);
	if ((ilRC = tool_search_exco_data("SYS", "HOMEAP", pcgHomeAP)) != RC_SUCCESS)
	{
		dbg(TRACE,"<MAIN> cannot find entry \"HOMEAP\" in SGS.TAB");
		Terminate(30);
	}
	dbg(DEBUG,"<MAIN> found HOME-AIRPORT: <%s>", pcgHomeAP); 

	/* Attach to the CCS queues */
	do
	{
		if((ilRC = init_que()) != RC_SUCCESS)
		{
			sleep(6);		/* Wait for QCP to create queues */
			ilCnt++;
		}
	}while((ilCnt < 10) && (ilRC != RC_SUCCESS));

	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"<MAIN> init_que failed!");
		Terminate(30);
	}

	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE))
	{
		dbg(TRACE,"<MAIN> waiting for status switch ...");
		HandleQueues();
	}

	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE))
	{
		dbg(DEBUG,"<MAIN> initializing ...");
		init_cdiif(pcgFileName);	
	} 
	else 
	{
		dbg(TRACE,"<MAIN> after HandleQueues & Status != Standalone || Aktive");
		Terminate(30);
	}

	/* only a message */
	dbg(TRACE,"<MAIN> initializing OK");

	/* forever */
	while (1)
	{
		switch (ctrl_sta)
		{
			/* only with an active or standby status */
			case HSB_ACTIVE:
			case HSB_STANDALONE:
				CDIServer();
				break;	

			/* other status */
			default:
				HandleQueues();
				break;	
		} 
	} 
} 

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int init_cdiif(char *pcpFile)
{
	int					i;
	int					j;
	int					k;
	int					ilRC;
	int					ilCounter;
	int					ilFieldCounter;
	int					ilAssignCounter;
	int					ilDataLengthCnt;
	int					ilCurDataLength; 
	int					ilCurMapField;
	int					ilMaxMapData;
	int					ilCurMapData;
	int					ilOtherData;
	char					*pclCfgPath = NULL;
	char					*pclPtr     = NULL;
	char					*pclS			= NULL;
	char					pclCfgFile[iMIN_BUF_SIZE];
	char					pclMapKeyWord[iMIN_BUF_SIZE];
	char					pclTmpBuf[iMIN_BUF_SIZE];
	char					pclTmpBuf1[iMIN_BUF_SIZE];
	char					pclTmpDBField[iMIN_BUF_SIZE];
	char					pclTmpIFField[iMIN_BUF_SIZE];
	char					pclCommandBuf[iMAX_BUF_SIZE];
	char					pclTableBuf[iMAX_BUF_SIZE];
	char					pclAssignBuf[iMAX_BUF_SIZE];
	char					pclFieldBuf[iMAXIMUM];
	char					pclDataLength[iMAXIMUM];
	char					pclMappingData[iMAXIMUM];
	
	if ((pclCfgPath = getenv("CFG_PATH")) == NULL)
	{
		/* environment not set */
		dbg(TRACE, "<init_cdiif> error reading environment (CFG_PATH)");	
		Terminate(30);
	}
	else
	{
		/* found environment-variable */
		/* build path/filename */
		memset ((void*)pclCfgFile, 0x00, iMIN_BUF_SIZE);
		sprintf(pclCfgFile, "%s/%s", pclCfgPath, pcpFile);

		/* CFG-Entries (simple types) */
		/*-------------------------------------------------------------------*/
		/*-------------------------------------------------------------------*/
		/*-------------------------------------------------------------------*/
		/* timeout before restart */
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "restart_timeout", CFG_INT, (char*)&igRestartTimeout)) != RC_SUCCESS)
		{
			dbg(DEBUG,"<init_cdiif> missing restart_timeout, using 30 seconds");
			igRestartTimeout = 30;
		}
		if (igRestartTimeout < 0 || igRestartTimeout > 3600)
		{
			dbg(TRACE,"<init_cdiif> invalid restart_timeout:%d, range between 0 and 3600 seconds, using 30 seconds", igRestartTimeout);
			igRestartTimeout = 30;
		}
		dbg(DEBUG,"<init_cdiif> restart_timeout:%d", igRestartTimeout);

		/* timeout information, seconds or microseconds... */
		memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "time_unit", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
		{
			dbg(DEBUG,"<init_cdiif> missing time_unit, using SECOND");
			strcpy(pclTmpBuf, "SECONDS");
		}

		/* convert to uppercase */
		StringUPR((UCHAR*)pclTmpBuf);

		/* check unit... */
		if (!strcmp(pclTmpBuf, "SECONDS"))
			rgTM.iTimeUnit = iSECONDS;	
		else if (!strcmp(pclTmpBuf, "MICRO_SECONDS"))
			rgTM.iTimeUnit = iMICRO_SECONDS;	
		else
		{
			dbg(DEBUG,"<init_cdiif> unkown time_unit, using SECOND");
			rgTM.iTimeUnit = iSECONDS;	
		}
		dbg(DEBUG,"<init_cdiif> TIME-UNIT: <%s>", pclTmpBuf);

		/* read all for connect information */
		/* this is the mod-id */
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "connect_mod_id", CFG_INT, (char*)&rgTM.iConnectModID)) == RC_SUCCESS)
		{
			dbg(DEBUG,"<init_cdiif> connect_mod_id is: %d", rgTM.iConnectModID);
		}
		else
			rgTM.iConnectModID = -1;

		/* following the open command */
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "open_connect_command", CFG_STRING, rgTM.pcOpenConnectCommand)) == RC_SUCCESS)
		{
			dbg(DEBUG,"<init_cdiif> open_connect_command is: <%s>", rgTM.pcOpenConnectCommand);
		}

		/* following the close command */
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "close_connect_command", CFG_STRING, rgTM.pcCloseConnectCommand)) == RC_SUCCESS)
		{
			dbg(DEBUG,"<init_cdiif> close_connect_command is: <%s>", rgTM.pcCloseConnectCommand);
		}

		/* read all telegram keys... */
		memset((void*)pcgCCOName, 0x00, iMIN);
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "cco_key",
						CFG_STRING, pcgCCOName)) != RC_SUCCESS)
		{
			dbg(TRACE,"<init_cdiif> missing CCO_KEY");
			Terminate(30);
		}

		memset((void*)pcgDATName, 0x00, iMIN);
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "dat_key",
						CFG_STRING, pcgDATName)) != RC_SUCCESS)
		{
			dbg(TRACE,"<init_cdiif> missing DAT_KEY");
			Terminate(30);
		}

		memset((void*)pcgCTLName, 0x00, iMIN);
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "ctl_key",
						CFG_STRING, pcgCTLName)) != RC_SUCCESS)
		{
			dbg(TRACE,"<init_cdiif> missing CTL_KEY");
			Terminate(30);
		}
		
		memset((void*)pcgDELName, 0x00, iMIN);
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "del_key",
						CFG_STRING, pcgDELName)) != RC_SUCCESS)
		{
			dbg(TRACE,"<init_cdiif> missing DEL_KEY");
			Terminate(30);
		}
		
		dbg(DEBUG,"<init_cdiif> CCOName for GetUniqueNumber: <%s>", pcgCCOName);
		dbg(DEBUG,"<init_cdiif> DATName for GetUniqueNumber: <%s>", pcgDATName);
		dbg(DEBUG,"<init_cdiif> CTLName for GetUniqueNumber: <%s>", pcgCTLName);
		dbg(DEBUG,"<init_cdiif> DELName for GetUniqueNumber: <%s>", pcgDELName);

		/* period we check connection */
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "check_connection",
						CFG_INT, (char*)&rgTM.iCheckConnect)) != RC_SUCCESS)
		{
			rgTM.iCheckConnect = iDEFAULT_CHECK_CONNECTIONS;
		}

		/* timeout for CCO if we have to check */
		if (rgTM.iCheckConnect)
		{
			if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "check_timeout",
							CFG_INT, (char*)&rgTM.iCheckTimeout)) != RC_SUCCESS)
			{
				rgTM.iCheckTimeout = iDEFAULT_CHECK_TIMEOUT;
			}
			else
			{
				if (rgTM.iCheckTimeout < iMIN_CHECK_TIMEOUT)
				{
					dbg(TRACE,"<init_cdiif> check_timeout < min_check_timeout-use default(%s)", pcpFile);
					rgTM.iCheckTimeout = iDEFAULT_CHECK_TIMEOUT;
				}
			}
		}
		
		/* acknowledgement yes/no */
		memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "acknowledgement",
						CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
		{
			rgTM.iAcknowledge = iDEFAULT_ACKNOWLEDGE;
		}
		else
		{
			/* convert to uppercase */
			StringUPR((UCHAR*)pclTmpBuf);

			if (!strcmp(pclTmpBuf, "YES"))
				rgTM.iAcknowledge = 1;
			else
			{
				if (!strcmp(pclTmpBuf, "NO"))
					rgTM.iAcknowledge = 0;
				else
				{
					dbg(DEBUG,"<init_cdiif> unkown entry for acknowledge-use default");
					rgTM.iAcknowledge = iDEFAULT_ACKNOWLEDGE;
				}
			}
		}

		/* use FTP Yes/No */
		memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "use_ftp",
						CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
		{
			rgTM.iUseFtp = iDEFAULT_USEFTP;
		}
		else
		{
			/* convert to uppercase */
			StringUPR((UCHAR*)pclTmpBuf);

			if (!strcmp(pclTmpBuf, "YES"))
				rgTM.iUseFtp = 1;
			else
			{
				if (!strcmp(pclTmpBuf, "NO"))
					rgTM.iUseFtp = 0;
				else
				{
					dbg(DEBUG,"<init_cdiif> unkown entry for use_ftp-use default");
					rgTM.iUseFtp = iDEFAULT_USEFTP;
				}
			}

			/* read size for ftp-using */
			if (rgTM.iUseFtp)
			{
				/* size */
				if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "ftp_bytes",
								CFG_INT, (char*)&rgTM.iFtpSize)) != RC_SUCCESS)
				{
					rgTM.iFtpSize = iDEFAULT_FTPSIZE;
				}
			}
		}

		/* use FTP for update Yes/No */
		memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", 
					"use_ftp_for_update", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
		{
			rgTM.iUseFtpForUpdate = iDEFAULT_USEFTP_FOR_UPDATE;
		}
		else
		{
			/* convert to uppercase */
			StringUPR((UCHAR*)pclTmpBuf);

			if (!strcmp(pclTmpBuf, "YES"))
				rgTM.iUseFtpForUpdate = 1;
			else
			{
				if (!strcmp(pclTmpBuf, "NO"))
					rgTM.iUseFtpForUpdate = 0;
				else
				{
					dbg(DEBUG,"<init_cdiif> unkown entry for use_ftp_for_update-use default");
					rgTM.iUseFtpForUpdate = iDEFAULT_USEFTP;
				}
			}
		}

		/* assignment separator */
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "assign_separator",
						CFG_STRING, rgTM.pcAssignSeparator)) != RC_SUCCESS)
		{
			strcpy(rgTM.pcAssignSeparator, sDEFAULT_ASSIGNSEP);
		}

		/* recovery for telegrams */
		memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "recover_type",
						CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
		{
			strcpy(pclTmpBuf, "FILE");
		}

		/* convert to uppercase */
		StringUPR((UCHAR*)pclTmpBuf);

		if (!strcmp(pclTmpBuf,"DATABASE")) 
		{
			/* use database for recovery */
			dbg(DEBUG,"<init_cdiif> Database recovery is not yet implemeted");
			dbg(DEBUG,"<init_cdiif> setting recovery to FILE");
			strcpy(pclTmpBuf, "FILE");
		}

		if (!strcmp(pclTmpBuf,"FILE"))
		{
			/* use file for recovery */
			rgTM.rRec.iRecoverType = iRECOVER_FILE;

			/* the recover file */
			if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "recover_file", CFG_STRING, rgTM.rRec.pcRecoverFile)) != RC_SUCCESS)
			{
				strcpy(rgTM.rRec.pcRecoverFile, sDEFAULT_RECOVER_FILE);
			}
			/* the recover tmp directory */
			if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "recover_tmp_dir", CFG_STRING, rgTM.rRec.pcRecoverTmpDir)) != RC_SUCCESS)
			{
				strcpy(rgTM.rRec.pcRecoverTmpDir, sDEFAULT_RECOVER_TMP_DIR);
			}
			dbg(TRACE,"Temporary directory for recover files is <%s>",rgTM.rRec.pcRecoverTmpDir);
		}
		else
		{
			if (!strcmp(pclTmpBuf,"MEMORY"))
			{
				/* use memory for recovery */
				rgTM.rRec.iRecoverType = iRECOVER_MEMORY;
			}
			else
			{
				dbg(DEBUG, "<init_cdiif> unknown entry for recover-type, use default (FILE)");
				rgTM.rRec.iRecoverType = iDEFAULT_RECOVERTYPE;
			}
		}

		memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "save_data",
						CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
		{
			strcpy(pclTmpBuf, "COUNTER");
		}
		else
		{
			StringUPR((UCHAR*)pclTmpBuf);
			if (!strcmp(pclTmpBuf, "COUNTER"))
			{
				/* use counter for recovery */
				rgTM.rRec.iSaveData = iCOUNTER;
			}
			else
			{
				if (!strcmp(pclTmpBuf, "TIME"))
				{
					/* use time for recovery */
					/********************
					rgTM.rRec.iSaveData = iTIME;
					********************/
					dbg(TRACE, "<init_cdiif> TIME-Option isn't yet implemented");
					rgTM.rRec.iSaveData = iCOUNTER;
				}
				else
				{
					/* what? */
					dbg(DEBUG, "<init_cdiif> unknown save_data-entry-use default (COUNTER)");
					rgTM.rRec.iSaveData = iCOUNTER;
				}
			}
		}

		/* anzahl recovery */
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "recover_counter",
						CFG_INT, (char*)&rgTM.rRec.iRecoverCounter)) != RC_SUCCESS)
		{
			rgTM.rRec.iRecoverCounter = iDEFAULT_RECOVER_COUNTER;
		}

		/* recover time */
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "recover_time",
						CFG_INT, (char*)&rgTM.rRec.iRecoverTime)) != RC_SUCCESS)
		{
			rgTM.rRec.iRecoverTime = iDEFAULT_RECOVER_TIME;
		}

		/* CCO-Sync */
		memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "cco_sync",
						CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
		{
			strcpy(pclTmpBuf, "YES");
		}
		else
		{
			StringUPR((UCHAR*)pclTmpBuf);
			if (!strcmp(pclTmpBuf, "YES"))
			{
				/* sync CCO */
				rgTM.iCCOSync = 1;
			}
			else
			{
				if (!strcmp(pclTmpBuf, "NO"))
				{
					/* sync not CCO */
					rgTM.iCCOSync = 0;
				}
				else
				{
					/* what? */
					dbg(DEBUG, "<init_cdiif> unknown cco_sync-entry-use default");
					rgTM.iCCOSync = 1;
				}
			}
		}

		/* max fail commands - only internal usage */
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "max_fail_cmds",
						CFG_INT, (char*)&rgTM.iMaxFailCmds)) != RC_SUCCESS)
		{
			rgTM.iMaxFailCmds = iDEFAULT_MAX_FAIL_CMDS;
		}

		/* should we send an EOD after update-telegram (after SFU) */
		memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
		if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "send_eod_after_update", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
		{
			strcpy(pclTmpBuf, "NO");
		}
		else
		{
			StringUPR((UCHAR*)pclTmpBuf);
			if (!strcmp(pclTmpBuf, "YES"))
			{
				/* sync CCO */
				rgTM.iSendEODAfterUpdate = 1;
			}
			else
			{
				if (!strcmp(pclTmpBuf, "NO"))
				{
					/* sync not CCO */
					rgTM.iSendEODAfterUpdate = 0;
				}
				else
				{
					/* what? */
					dbg(DEBUG, "<init_cdiif> unknown send_eod_after_update-entry-use default (NO)");
					rgTM.iSendEODAfterUpdate = 0;
				}
			}
		}

		/* CFG-Entries (Ftp-Types) */
		/*-------------------------------------------------------------------*/
		/*-------------------------------------------------------------------*/
		/*-------------------------------------------------------------------*/
		/* read size for ftp-using */
		if (rgTM.iUseFtp)
		{
			/* user for ftp */
			if ((ilRC = iGetConfigEntry(pclCfgFile, "FTP", "ftp_user",
							CFG_STRING, rgTM.rFtp.pcUser)) != RC_SUCCESS)
			{
				dbg(TRACE,"<init_cdiif> error reading ftp_user (%s)",pcpFile);
				Terminate(30);
			}

			/* passwd for user */
			if ((ilRC = iGetConfigEntry(pclCfgFile, "FTP", "ftp_pass",
							CFG_STRING, rgTM.rFtp.pcPass)) != RC_SUCCESS)
			{
				dbg(TRACE,"<init_cdiif> error reading ftp_login (%s)",pcpFile);
				Terminate(30);
			}
			else
			{
				/* decode password */
				pclPtr = rgTM.rFtp.pcPass;
#ifdef __SHOW_PASSWD
				dbg(DEBUG,"<init_cdiif> password before decoding: <%s>", rgTM.rFtp.pcPass);
#endif
				CDecode(&pclPtr);
#ifdef __SHOW_PASSWD
				dbg(DEBUG,"<init_cdiif> password after decoding : <%s>", pclPtr);
#endif
				strcpy(rgTM.rFtp.pcPass, pclPtr);
			}

			/* name of control file */
			strcpy(rgTM.rFtp.pcFtpCtlFile, pcpFile);
			pclPtr = strstr(rgTM.rFtp.pcFtpCtlFile, ".");
			*(pclPtr+1) = 0x00;
			strcat(rgTM.rFtp.pcFtpCtlFile, "ftp");
			dbg(DEBUG,"<init_cdiif> ftp-file-name: <%s>", rgTM.rFtp.pcFtpCtlFile);

			/* path for file */
			if ((ilRC = iGetConfigEntry(pclCfgFile, "FTP", "ftp_filepath",
							CFG_STRING, rgTM.rFtp.pcFtpFilePath)) != RC_SUCCESS)
			{
				strcpy(rgTM.rFtp.pcFtpFilePath, sDEFAULT_FTPPATH);
			}

			/* name of client */
			if ((ilRC = iGetConfigEntry(pclCfgFile, "FTP", "machine",
							CFG_STRING, rgTM.rFtp.pcMachine)) != RC_SUCCESS)
			{
				dbg(TRACE,"<init_cdiif> error reading machine (%s)",pcpFile);
				Terminate(30);
			}

			/* path for file (on client) */
			if ((ilRC = iGetConfigEntry(pclCfgFile, "FTP", "machine_path",
							CFG_STRING, rgTM.rFtp.pcMachinePath)) != RC_SUCCESS)
			{
				dbg(TRACE,"<init_cdiif> error reading Machine_Path (%s)",pcpFile);
				Terminate(30);
			}
		}

		/* CFG-Entries (Prot-Types) */
		/*-------------------------------------------------------------------*/
		/*-------------------------------------------------------------------*/
		/*-------------------------------------------------------------------*/
		memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
		if ((ilRC = iGetConfigEntry(pclCfgFile, "PROT", "rprot",
						CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
		{
			strcpy(pclTmpBuf, "ON");
		}
		else
		{
			/* to uppercase */
			StringUPR((UCHAR*)pclTmpBuf);

			if (!strcmp(pclTmpBuf, "ON"))
			{
				rgTM.iUseRProt = 1;
			}
			else if (!strcmp(pclTmpBuf, "OFF"))
			{
				rgTM.iUseRProt = 0;
			}
			else
			{
				dbg(DEBUG,"<init_cdiif> unknown entry in rprot-use defaulti(%s)", pcpFile);
				rgTM.iUseRProt = 1;
			}
		}

		memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
		if ((ilRC = iGetConfigEntry(pclCfgFile, "PROT", "sprot",
						CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
		{
			strcpy(pclTmpBuf, "ON");
		}
		else
		{
			/* to uppercase */
			StringUPR((UCHAR*)pclTmpBuf);

			if (!strcmp(pclTmpBuf, "ON"))
			{
				rgTM.iUseSProt = 1;
			}
			else if (!strcmp(pclTmpBuf, "OFF"))
			{
				rgTM.iUseSProt = 0;
			}
			else
			{
				dbg(DEBUG,"<init_cdiif> unknown entry in sprot-use defaulti(%s)", pcpFile);
				rgTM.iUseSProt = 1;
			}
		}

		/* searching for filenames, if necessary */
		if (rgTM.iUseRProt)
		{
			if ((ilRC = iGetConfigEntry(pclCfgFile, "PROT", "rprot_file", CFG_STRING, rgTM.rProt.pcRProtFile)) != RC_SUCCESS)
			{
				strcmp(rgTM.rProt.pcRProtFile, sRPROT_DEFAULT);
			}

			/* Timestamp and more... */
			pclPtr = GetTimeStamp();
			strcat(rgTM.rProt.pcRProtFile, &pclPtr[8]);

			/* Extension */
			strcat(rgTM.rProt.pcRProtFile, sPROT_FILEEXTENSION);
		}

		if (rgTM.iUseSProt)
		{
			if ((ilRC = iGetConfigEntry(pclCfgFile, "PROT", "sprot_file", CFG_STRING, rgTM.rProt.pcSProtFile)) != RC_SUCCESS)
			{
				strcmp(rgTM.rProt.pcSProtFile, sSPROT_DEFAULT);
			}

			/* Timestamp and more... */
			pclPtr = GetTimeStamp();
			strcat(rgTM.rProt.pcSProtFile, &pclPtr[8]);

			/* Extension */
			strcat(rgTM.rProt.pcSProtFile, sPROT_FILEEXTENSION);
		}
		
		/* CFG-Entries (TCP/IP-Types) */
		/*-------------------------------------------------------------------*/
		/*-------------------------------------------------------------------*/
		/*-------------------------------------------------------------------*/
		if ((ilRC = iGetConfigEntry(pclCfgFile, "TCP", "tcp_timeout",
						CFG_INT, (char*)&rgTM.rTCP.iTcpTimeout)) != RC_SUCCESS)
		{
			rgTM.rTCP.iTcpTimeout = iDEFAULT_TCP_TIMEOUT;
		}
		else
		{
			if (rgTM.rTCP.iTcpTimeout < iMIN_TCP_TIMEOUT)
			{
				dbg(DEBUG,"<init_cdiif> tcp_timeout < min_tcp_timeout-use default(%s)", pcpFile);
				rgTM.rTCP.iTcpTimeout = iDEFAULT_TCP_TIMEOUT;
			}
		}

		if ((ilRC = iGetConfigEntry(pclCfgFile, "TCP", "max_connections",
						CFG_INT, (char*)&rgTM.rTCP.iMaxConnections)) != RC_SUCCESS)
		{
			rgTM.rTCP.iMaxConnections = iDEFAULT_MAX_CONNECTIONS;
		}
		
		if ((ilRC = iGetConfigEntry(pclCfgFile, "TCP", "service_name",
						CFG_STRING, rgTM.rTCP.pcServiceName)) != RC_SUCCESS)
		{
			strcpy(rgTM.rTCP.pcServiceName, sDEFAULT_SERVICE_NAME);
		}

		if ((ilRC = iGetConfigEntry(pclCfgFile, "TCP", "bytes",
						CFG_INT, (char*)&rgTM.rTCP.iMaxBytes)) != RC_SUCCESS)
		{
			rgTM.rTCP.iMaxBytes = iMAX_BYTES;
		}
		else
		{
			/* is it valid */
			if (rgTM.rTCP.iMaxBytes > iMAX_BYTES)
			{
				dbg(DEBUG,"<init_cdiif> set max_bytes to %d", iMAX_BYTES);
				rgTM.rTCP.iMaxBytes = iMAX_BYTES;
			}
		}
		
		/* CFG-Entries (Server-Types) */
		/*-------------------------------------------------------------------*/
		/*-------------------------------------------------------------------*/
		/*-------------------------------------------------------------------*/
		if ((ilRC = iGetConfigEntry(pclCfgFile, "SERVER", "serversystem",
						CFG_STRING, rgTM.rServer.pcServerSystem)) != RC_SUCCESS)
		{
			dbg(TRACE, "<init_cdiif> error reading server-system (%s)", pcpFile);	
			Terminate(30);
		}

		if ((ilRC = iGetConfigEntry(pclCfgFile, "SERVER", "sender",
						CFG_STRING, rgTM.rServer.pcSender)) != RC_SUCCESS)
		{
			dbg(TRACE, "<init_cdiif> error reading sender (%s)", pcpFile);	
			Terminate(30);
		}

		if ((ilRC = iGetConfigEntry(pclCfgFile, "SERVER", "receiver",
						CFG_STRING, rgTM.rServer.pcReceiver)) != RC_SUCCESS)
		{
			dbg(TRACE, "<init_cdiif> error reading receiver (%s)", pcpFile);	
			Terminate(30);
		}

		/* CFG-Entries Commands --- complex types */
		/*-------------------------------------------------------------------*/
		/*-------------------------------------------------------------------*/
		/*-------------------------------------------------------------------*/
		rgTM.iNoCmd = 0;
		memset((void*)pclCommandBuf, 0x00, iMAX_BUF_SIZE);
		if ((ilRC = iGetConfigEntry(pclCfgFile, "COMMAND", "commands",
						CFG_STRING, pclCommandBuf)) != RC_SUCCESS)
		{
			dbg(TRACE, "<init_cdiif> error reading commands (%s)", pcpFile);	
			Terminate(30);
		}
		else
		{
			/* count number of commands */
			if ((ilCounter = GetNoOfElements(pclCommandBuf, cSEPARATOR)) < 0)
			{
				dbg(TRACE,"<init_cdiif> GetNoOfElements returns: %d", ilCounter);
				Terminate(30);
			}
			else
			{
				/* number of currently existing elements */
				rgTM.iNoCmd = ilCounter;

				/* found elements */
				/* get memory for all elements */
				if ((rgTM.prCmd = (TCommand*)malloc((size_t)(ilCounter*sizeof(TCommand)))) == NULL)
				{
					/* not enough memory to run */
					dbg(TRACE,"<init_cdiif> TCommand memory failure");
					rgTM.prCmd = NULL;
					Terminate(30);
				}
				else
				{
					/* memory is OK, set status */
					igStatus |= iF_CMD_STRUCT_MEMORY;

					/* store the commands */
					for (i=1; i<=ilCounter; i++)
					{
						memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
						if (flt_cp_nth_field(pclCommandBuf, i, pclTmpBuf) != RC_SUCCESS)
						{
							/* this is not a fatal error */
							dbg(TRACE,"<init_cdiif> cannot copy nth field-command (%d)", i);
							Terminate(30);
						}
						else
						{
							dbg(DEBUG,"<init_cdiif> COMMAND: <%s>", pclTmpBuf);
							/* check command */
							if (!strcmp(pclTmpBuf, "END") ||
								 !strcmp(pclTmpBuf, "EOD") ||
								 !strcmp(pclTmpBuf, "CCO") ||
								 !strcmp(pclTmpBuf, "ERR") ||
								 !strcmp(pclTmpBuf, "ACK"))
							{
								/* found a system command */
								dbg(TRACE,"<init_cdiif> found system command <%s>",
																						pclTmpBuf);
								Terminate(30);
							}

							/* store commands to structure */ 
							if ((rgTM.prCmd[i-1].pcCommand = strdup(pclTmpBuf)) == NULL)
							{
								dbg(TRACE,"<init_cdiif> not enough memory to store commands");
								rgTM.prCmd[i-1].pcCommand = NULL;
								Terminate(30);
							}
							else
							{
								/* set status */
								igStatus |= iF_CMD_COMMAND_MEMORY;
							}
						}
					}
				}
			}
		}

		/* if we are here, we know how many command are placed and 
		we should read the tables-entries 
		*/
		for (i=0; i<rgTM.iNoCmd; i++)
		{
			/* build section */
			/* first convert command to uppercase */
			StringUPR((UCHAR*)rgTM.prCmd[i].pcCommand);

			/* set flag to zero */
			rgTM.prCmd[i].iOpenDataFile = 0;

			/* set counter to zero */
			rgTM.prCmd[i].iDataCounter = 0;

			/* FTP-File with timestamp */
			memset((void*)pclTmpBuf1, 0x00, iMIN_BUF_SIZE);
			if ((ilRC = iGetConfigEntry(pclCfgFile, rgTM.prCmd[i].pcCommand, "filename_with_timestamp", CFG_STRING, pclTmpBuf1)) != RC_SUCCESS)
			{
				/* set default */
				strcpy(pclTmpBuf1, "NO");
			}
			
			/* convert to uppercase */
			StringUPR((UCHAR*)pclTmpBuf1);

			/* check th answer */
			if (!strcmp(pclTmpBuf1, "YES"))
				rgTM.prCmd[i].iFilenameWithTimestamp = 1;
			else if (!strcmp(pclTmpBuf1, "NO"))
				rgTM.prCmd[i].iFilenameWithTimestamp = 0;
			else
			{
				dbg(DEBUG,"<init_cdiif> unknown filename_with_timestamp - use default (YES)");
				rgTM.prCmd[i].iFilenameWithTimestamp = 0;
			}

			/* FTP-Filename */
			if ((ilRC = iGetConfigEntry(pclCfgFile, rgTM.prCmd[i].pcCommand, "file_name", CFG_STRING, rgTM.prCmd[i].pcFileName)) != RC_SUCCESS)
			{
				strcpy(rgTM.prCmd[i].pcFileName, sFTP_FILENAME);
			}

			/* FTP-Fileextension */
			if ((ilRC = iGetConfigEntry(pclCfgFile, rgTM.prCmd[i].pcCommand, "file_extension", CFG_STRING, rgTM.prCmd[i].pcFileExtension)) != RC_SUCCESS)
			{
				strcpy(rgTM.prCmd[i].pcFileExtension, sFTP_FILEEXTENT);
			}

			/*---------------------------------------------------------------*/
			/*---------------------------------------------------------------*/
			/*---------------------------------------------------------------*/
			/* the home airport */
			if ((ilRC = iGetConfigEntry(pclCfgFile, rgTM.prCmd[i].pcCommand, "home_airport", CFG_STRING, rgTM.prCmd[i].pcHomeAirport)) != RC_SUCCESS)
			{
				strcpy(rgTM.prCmd[i].pcHomeAirport, pcgHomeAP);
			}
			dbg(DEBUG,"<init_cdiif> Home-Airport: <%s>", rgTM.prCmd[i].pcHomeAirport);

			/* the table extension */
			if ((ilRC = iGetConfigEntry(pclCfgFile, rgTM.prCmd[i].pcCommand, "table_extension", CFG_STRING, rgTM.prCmd[i].pcTableExtension)) != RC_SUCCESS)
			{
				strcpy(rgTM.prCmd[i].pcTableExtension, pcgTABEnd);
			}
			dbg(DEBUG,"<init_cdiif> TableExtension: <%s>", rgTM.prCmd[i].pcTableExtension);
		
			memset((void*)pclTableBuf, 0x00, iMAX_BUF_SIZE);
			if ((ilRC = iGetConfigEntry(pclCfgFile, rgTM.prCmd[i].pcCommand, "tables", CFG_STRING, pclTableBuf)) != RC_SUCCESS)
			{
				/* there are no tables */
				rgTM.prCmd[i].iNoTables = 0;
			}
			else
			{
				/* get table info */
				/* count number of tables */
				if ((ilCounter = GetNoOfElements(pclTableBuf, cSEPARATOR)) < 0)
				{
					dbg(TRACE,"<init_cdiif> GetNoOfElements returns: %d", ilCounter);
					Terminate(30);
				}
				else
				{
					/* set number of tables */
					rgTM.prCmd[i].iNoTables = ilCounter;

					/* we know the tables combined with this command */
					/* get memory for all tables */
					if ((rgTM.prCmd[i].prTabDef = (TabDef*)malloc((size_t)(ilCounter*sizeof(TabDef)))) == NULL)
					{
						/* a real fatal error */
						dbg(TRACE,"<init_cdiif> not enough memory for tables");
						rgTM.prCmd[i].prTabDef = NULL;
						Terminate(30);
					}
					else
					{
						/* memory is OK */
						/* set status */
						igStatus |= iF_TAB_STRUCT_MEMORY;

						/* now read tables definitions */
						for (j=1; j<=ilCounter; j++)
						{
							/* tables */	
							memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
							if (flt_cp_nth_field(pclTableBuf, j, pclTmpBuf) != RC_SUCCESS)
							{
								/* this is not a fatal error */
								dbg(TRACE,"<init_cdiif> cannot copy nth field-tables (%d)", j);
								Terminate(30);
							}
							else
							{
								dbg(DEBUG,"<init_cdiif> ------- START <%s> ------", pclTmpBuf);
								/* convert table to uppercase */
								StringUPR((UCHAR*)pclTmpBuf);

								/* read entries for table 'pclTmpBuf' */
								/* read entry for 3- or 4-letter-code in VIAL */
								if ((ilRC = iGetConfigEntry(pclCfgFile, pclTmpBuf, "via", CFG_INT, (char*)&rgTM.prCmd[i].prTabDef[j-1].iVia34)) != RC_SUCCESS)
								{
									/* can't find entry -> use default (3-letter) */
									rgTM.prCmd[i].prTabDef[j-1].iVia34 = 3;
								}
								else
								{
									/* check via entry... */
									dbg(DEBUG,"<init_cdiif> found via: %d", rgTM.prCmd[i].prTabDef[j-1].iVia34);
									if (rgTM.prCmd[i].prTabDef[j-1].iVia34 != 3 && rgTM.prCmd[i].prTabDef[j-1].iVia34 != 4)
									{
										dbg(TRACE,"<init_cdiif> unkown entry for via-use 3-letter-code");
										rgTM.prCmd[i].prTabDef[j-1].iVia34 = 3;
									}
								}
								dbg(DEBUG,"<init_cdiif> use %d-letter-code for via", rgTM.prCmd[i].prTabDef[j-1].iVia34);

								/* shoud i use received selection? */
								memset((void*)pclTmpBuf1, 0x00, iMIN_BUF_SIZE);
								if ((ilRC = iGetConfigEntry(pclCfgFile, pclTmpBuf, "use_received_selection", CFG_STRING, pclTmpBuf1)) != RC_SUCCESS)
								{
									/* set default */
									strcpy(pclTmpBuf1, "YES");
								}
								
								/* convert to uppercase */
								StringUPR((UCHAR*)pclTmpBuf1);

								/* check th answer */
								if (!strcmp(pclTmpBuf1, "YES"))
									rgTM.prCmd[i].prTabDef[j-1].iUseReceivedSelection=1;
								else if (!strcmp(pclTmpBuf1, "NO"))
									rgTM.prCmd[i].prTabDef[j-1].iUseReceivedSelection=0;
								else
								{
									dbg(DEBUG,"<init_cdiif> unknown use_received_selection - use default (YES)");
									rgTM.prCmd[i].prTabDef[j-1].iUseReceivedSelection=1;
								}

								/* telegram type of this */
								memset((void*)pclTmpBuf1, 0x00, iMIN_BUF_SIZE);
								if ((ilRC = iGetConfigEntry(pclCfgFile, pclTmpBuf, "telegram", CFG_STRING, pclTmpBuf1)) != RC_SUCCESS)
								{
									/* set default */
									strcpy(pclTmpBuf1, sDATA_CLASS);
								}

								/* convert to uppercase */
								StringUPR((UCHAR*)pclTmpBuf1);

								/* only MIX or DATA or MDEL or DDEL */
								if (!strcmp(pclTmpBuf1, sDATA_CLASS))
									rgTM.prCmd[i].prTabDef[j-1].iTelegramClass = iDATA_CLASS;
								else if (!strcmp(pclTmpBuf1, sMIX_CLASS))
									rgTM.prCmd[i].prTabDef[j-1].iTelegramClass = iMIX_CLASS;
								else if (!strcmp(pclTmpBuf1, "MDEL"))
									rgTM.prCmd[i].prTabDef[j-1].iTelegramClass = iMDEL_CLASS;
								else if (!strcmp(pclTmpBuf1, "DDEL"))
									rgTM.prCmd[i].prTabDef[j-1].iTelegramClass = iDDEL_CLASS;
								else
								{
									dbg(DEBUG,"<init_cdiif> %s unknown telegram class-use default (DAT)", pclTmpBuf);
									rgTM.prCmd[i].prTabDef[j-1].iTelegramClass = iDATA_CLASS;
								}

								/* data type of this */
								if ((ilRC = iGetConfigEntry(pclCfgFile, pclTmpBuf, "data_type", CFG_STRING, rgTM.prCmd[i].prTabDef[j-1].pcDataType)) != RC_SUCCESS)
								{
									/* can't work without data type */
									dbg(TRACE,"<init_cdiif> %s without data_type", pclTmpBuf);
									Terminate(30);
								}

								/* the mod_id we send data to */
								if ((ilRC = iGetConfigEntry(pclCfgFile, pclTmpBuf, "mod_id", CFG_INT, (char*)&rgTM.prCmd[i].prTabDef[j-1].iModID)) != RC_SUCCESS)
								{
									/* we work with router... */
									rgTM.prCmd[i].prTabDef[j-1].iModID = -1;
								}
								dbg(DEBUG,"<init_cdiif> found MOD_ID: %d", rgTM.prCmd[i].prTabDef[j-1].iModID);

								/* 0th send command (cmbblk->command) */
								if ((ilRC = iGetConfigEntry(pclCfgFile, pclTmpBuf, "snd_cmd", CFG_STRING, rgTM.prCmd[i].prTabDef[j-1].pcSndCmd)) != RC_SUCCESS)
								{
									/* can't work without send command */
									dbg(TRACE,"<init_cdiif> %s without snd_cmd", pclTmpBuf);
									Terminate(30);
								}

								/* convert to uppercase */
								StringUPR((UCHAR*)rgTM.prCmd[i].prTabDef[j-1].pcSndCmd);

								/* 1st table name */
								if ((ilRC = iGetConfigEntry(pclCfgFile, pclTmpBuf, "name", CFG_STRING, rgTM.prCmd[i].prTabDef[j-1].pcTabName)) != RC_SUCCESS)
								{
									/* can't work without table name */
									dbg(TRACE,"<init_cdiif> %s without name", pclTmpBuf);
									Terminate(30);
								}

								/* convert to uppercase */
								StringUPR((UCHAR*)rgTM.prCmd[i].prTabDef[j-1].pcTabName);

								/* 2nd selection */
								if ((ilRC = iGetConfigRow(pclCfgFile, pclTmpBuf, "select", CFG_STRING, rgTM.prCmd[i].prTabDef[j-1].pcSelect)) != RC_SUCCESS)
								{
									/* set selection flag */
									rgTM.prCmd[i].prTabDef[j-1].iUseSelection = 0;
								}
								else
								{
									rgTM.prCmd[i].prTabDef[j-1].iUseSelection = 1;
								}

								/* 3rd order by */
								if ((ilRC = iGetConfigRow(pclCfgFile, pclTmpBuf, "order_by", CFG_STRING, rgTM.prCmd[i].prTabDef[j-1].pcOrderBy)) != RC_SUCCESS)
								{
									/* set selection flag */
									rgTM.prCmd[i].prTabDef[j-1].iUseOrderBy = 0;
								}
								else
								{
									rgTM.prCmd[i].prTabDef[j-1].iUseOrderBy = 1;
								}

								/* last the fields */
								memset((void*)pclFieldBuf, 0x00, iMAXIMUM);
								if ((ilRC = iGetConfigEntry(pclCfgFile, pclTmpBuf, "fields", CFG_STRING, pclFieldBuf)) != RC_SUCCESS)
								{
									/* can work without fields */
									dbg(TRACE,"<init_cdiif> %s without fields", pclTmpBuf);
									rgTM.prCmd[i].prTabDef[j-1].iNoFields = 0;
									Terminate(30);
								}

								/* convert fields to uppercase */
								StringUPR((UCHAR*)pclFieldBuf);

								/* count number of fields */
								if ((ilFieldCounter = (int)GetNoOfElements(pclFieldBuf, cSEPARATOR)) < 0)
								{
									dbg(TRACE,"<init_cdiif> GetNoOfElements returns: %d", ilCounter);
									Terminate(30);
								}
								else
								{
									/* set number of fields */
									rgTM.prCmd[i].prTabDef[j-1].iNoFields = ilFieldCounter;
									/* get 'pointer'memory for all fields */
									if ((rgTM.prCmd[i].prTabDef[j-1].pcFields = (char**)malloc((size_t)(ilFieldCounter*sizeof(char*)))) == NULL)
									{
										dbg(TRACE,"<init_cdiif> not enough memory for field-pointer");
										rgTM.prCmd[i].prTabDef[j-1].pcFields = NULL;
										Terminate(30);
									}
									else
									{
										/* set new status */
										igStatus |= iF_FIELD_POINTER_MEMORY;

										/* for all fields */
										rgTM.prCmd[i].prTabDef[j-1].iUrnoNo = 0;
										rgTM.prCmd[i].prTabDef[j-1].iViaInFieldList = 0;
										for (k=1; k<=ilFieldCounter; k++)
										{
											/* store fields in structure */
											memset((void*)pclTmpBuf1, 0x00, iMIN_BUF_SIZE);
											if (flt_cp_nth_field(pclFieldBuf, k, pclTmpBuf1) != RC_SUCCESS)
											{
												/* fatal error - can't work */
												dbg(TRACE,"<init_cdiif> Cannot work without field definition");
												Terminate(30);
											}
											else
											{
												/* look for urno position */
												if (!strcmp(pclTmpBuf1,"URNO"))
													rgTM.prCmd[i].prTabDef[j-1].iUrnoNo = k-1;

												/* check if system need VIAx */
												if (!strncmp(pclTmpBuf1,"VIA", 3))
													rgTM.prCmd[i].prTabDef[j-1].iViaInFieldList = 1;

												if ((rgTM.prCmd[i].prTabDef[j-1].pcFields[k-1] = strdup(pclTmpBuf1)) == NULL)
												{
													dbg(TRACE,"<init_cdiif> not enough memory for copying fields");
													rgTM.prCmd[i].prTabDef[j-1].pcFields[k-1] = NULL;
													Terminate(30);
												}
												else
												{
													/* set status */
													igStatus |= iF_FIELD_MEMORY;
												}
											}
										}
									}
								}

								/*----------- DataMapping ----------------------*/
								/* get memory for mapping (default for all fields) */
								if ((rgTM.prCmd[i].prTabDef[j-1].prDataMap = (DataMap**)malloc((size_t)(rgTM.prCmd[i].prTabDef[j-1].iNoFields*sizeof(DataMap*)))) == NULL)
								{
									rgTM.prCmd[i].prTabDef[j-1].prDataMap = NULL;
									dbg(TRACE,"<init_cdiif> malloc failure (Data Map)");
									Terminate(30);
								}

								/* get memory for counter (default for all fields) */
								if ((rgTM.prCmd[i].prTabDef[j-1].piNoMapData = (UINT*)malloc((size_t)(rgTM.prCmd[i].prTabDef[j-1].iNoFields*sizeof(UINT)))) == NULL)
								{
									rgTM.prCmd[i].prTabDef[j-1].piNoMapData = NULL;
									dbg(TRACE,"<init_cdiif> malloc failure (Data Map)");
									Terminate(30);
								}

								/* read (for each field) the mapping data */
								for (ilCurMapField=0; ilCurMapField<rgTM.prCmd[i].prTabDef[j-1].iNoFields; ilCurMapField++)
								{
									/* build keywords... */
									sprintf(pclMapKeyWord, "map_%s_data", rgTM.prCmd[i].prTabDef[j-1].pcFields[ilCurMapField]);

									/* convert to lowercase */
									StringLWR((UCHAR*)pclMapKeyWord); 
									dbg(DEBUG,"<init_cdiif> build mapping key word: <%s>", pclMapKeyWord);

									/* set pointer to NULL */
									rgTM.prCmd[i].prTabDef[j-1].prDataMap[ilCurMapField] = NULL;

									/* set counter to 0 */
									rgTM.prCmd[i].prTabDef[j-1].piNoMapData[ilCurMapField] = 0;
										
									/* read mapping data for this... */
									memset((void*)pclMappingData, 0x00, iMIN_BUF_SIZE);
									if ((ilRC = iGetConfigEntry(pclCfgFile, pclTmpBuf, pclMapKeyWord, CFG_STRING, pclMappingData)) == RC_SUCCESS)
									{
										dbg(DEBUG,"<init_cdiif> -- FIELD <%s> MAPPING --",rgTM.prCmd[i].prTabDef[j-1].pcFields[ilCurMapField]);

										/* count number of elements */
										if ((ilMaxMapData = GetNoOfElements(pclMappingData, ',')) < 0)
										{
											dbg(TRACE,"<init_cdiif> GetNoOfElements(mapping data) returns: %d", ilMaxMapData);
											Terminate(30);
										}
										dbg(DEBUG,"<init_cdiif> found %d mapping entries", ilMaxMapData);

										/* add new element */
										rgTM.prCmd[i].prTabDef[j-1].piNoMapData[ilCurMapField] = ilMaxMapData;

										/* memory for new elements */
										if ((rgTM.prCmd[i].prTabDef[j-1].prDataMap[ilCurMapField] = (DataMap*)malloc((size_t)(ilMaxMapData*sizeof(DataMap)))) == NULL)
										{
											dbg(TRACE,"<init_cdiif> malloc failure (data mapping)");
											Terminate(30);
										}

										/* store new elements */
										for (ilCurMapData=0, ilOtherData=iORG_DATA; ilCurMapData<ilMaxMapData; ilCurMapData++)
										{
											/* get element */
											if ((pclS = GetDataField(pclMappingData, ilCurMapData, cSEPARATOR)) == NULL)
											{
												dbg(TRACE,"<init_cdiif> GetDataField (mapping) returns NULL");
												Terminate(30);
											}
											dbg(DEBUG,"<init_cdiif> found <%s>", pclS);
	
											/* separate both elements */
											if ((ilRC = SeparateIt(pclS, rgTM.prCmd[i].prTabDef[j-1].prDataMap[ilCurMapField][ilCurMapData].pcOrgData, rgTM.prCmd[i].prTabDef[j-1].prDataMap[ilCurMapField][ilCurMapData].pcNewData, cASSIGNSEPARATOR)) < 0)
											{
												dbg(TRACE,"<init_cdiif> SeparateIt returns: %d", ilRC);
												Terminate(30);
											}

											/* set other data flag */
											if (!strcmp("OTHER_DATA", rgTM.prCmd[i].prTabDef[j-1].prDataMap[ilCurMapField][ilCurMapData].pcOrgData) && !strcmp("NULL", rgTM.prCmd[i].prTabDef[j-1].prDataMap[ilCurMapField][ilCurMapData].pcNewData))
											{
												dbg(DEBUG,"<init_cdiif> setting OTHER_DATA to NULL");
												ilOtherData = iNULL_DATA;
											}
										}

										/* set other data flag */
										for (ilCurMapData=0; ilCurMapData<ilMaxMapData; ilCurMapData++)
										{
											rgTM.prCmd[i].prTabDef[j-1].prDataMap[ilCurMapField][ilCurMapData].iOtherData = ilOtherData;
												
											dbg(DEBUG,"<init_cdiif> map from <%s> to <%s> other to <%d>", rgTM.prCmd[i].prTabDef[j-1].prDataMap[ilCurMapField][ilCurMapData].pcOrgData, rgTM.prCmd[i].prTabDef[j-1].prDataMap[ilCurMapField][ilCurMapData].pcNewData, rgTM.prCmd[i].prTabDef[j-1].prDataMap[ilCurMapField][ilCurMapData].iOtherData);
										}
										dbg(DEBUG,"<init_cdiif> -- MAPPING <%s> END --", rgTM.prCmd[i].prTabDef[j-1].pcFields[ilCurMapField]);
									}
								}
							
								/*----------- DataLength ----------------------*/
								memset((void*)pclDataLength, 0x00, iMAXIMUM);
								if ((ilRC = iGetConfigEntry(pclCfgFile, pclTmpBuf, "data_length", CFG_STRING, pclDataLength)) != RC_SUCCESS)
								{
									/* can work without fields */
									rgTM.prCmd[i].prTabDef[j-1].iDataLengthCounter = 0;
									rgTM.prCmd[i].prTabDef[j-1].piDataLength = NULL;
								}
								else
								{
									/* convert data_length to uppercase */
									StringUPR((UCHAR*)pclDataLength);

									/* count number of fields */
									if ((ilDataLengthCnt = (int)GetNoOfElements(pclDataLength, cSEPARATOR)) < 0)
									{
										dbg(TRACE,"<init_cdiif> GetNoOfElements (DataLength) returns: %d", ilDataLengthCnt);
										Terminate(30);
									}

									/* set number of fields */
									rgTM.prCmd[i].prTabDef[j-1].iDataLengthCounter = ilDataLengthCnt;

									/* get 'pointer'memory for all fields */
									if ((rgTM.prCmd[i].prTabDef[j-1].piDataLength = (UINT*)malloc((size_t)(ilDataLengthCnt*sizeof(UINT)))) == NULL)
									{
										dbg(TRACE,"<init_cdiif> not enough memory for data_length-pointer");
										rgTM.prCmd[i].prTabDef[j-1].piDataLength = NULL;
										Terminate(30);
									}

									/* set flag in status word */
									igStatus |= iF_DATA_LENGTH_MEMORY;

									/* for all data length entries */
									for (ilCurDataLength=0; 
										  ilCurDataLength<ilDataLengthCnt;
										  ilCurDataLength++)
									{
										/* get n-th entry */
										if ((pclPtr = GetDataField(pclDataLength, ilCurDataLength, cSEPARATOR)) == NULL)
										{
											dbg(TRACE,"<init_cdiif> GetDataField (pclDataLength) returns NULL");
											Terminate(30);
										}

										/* convert it to int and store it */
										rgTM.prCmd[i].prTabDef[j-1].piDataLength[ilCurDataLength] = (UINT)atoi(pclPtr);
									}
								}

								/* ------------- ASSIGNMENT --------------- */
								memset((void*)pclAssignBuf, 0x00, iMAX_BUF_SIZE);
								if ((ilRC = iGetConfigEntry(pclCfgFile, pclTmpBuf, "assignment", CFG_STRING, pclAssignBuf)) != RC_SUCCESS)
								{
									/* there is no assignment */
									rgTM.prCmd[i].prTabDef[j-1].iNoAssign = 0;
									rgTM.prCmd[i].prTabDef[j-1].prAssign = NULL;
								}
								else
								{
									/* get assignment info */
									/* convert to uppercase */
									StringUPR((UCHAR*)pclAssignBuf);

									/* count number of assign */
									if ((ilAssignCounter = (int)GetNoOfElements(pclAssignBuf, cSEPARATOR)) < 0)
									{
										dbg(TRACE,"<init_cdiif> GetNoOfElements returns: %d", ilAssignCounter);
										Terminate(30);
									}
									else
									{
										/* set number of assign */
										rgTM.prCmd[i].prTabDef[j-1].iNoAssign = ilAssignCounter;

										/* get memory for all assignment entries */
										if ((rgTM.prCmd[i].prTabDef[j-1].prAssign = (TAssign*)malloc((size_t)(ilAssignCounter*sizeof(TAssign)))) == NULL)
										{
											/* a real fatal error */
											dbg(TRACE,"<init_cdiif> not enough memory for assignment");
											rgTM.prCmd[i].prTabDef[j-1].prAssign = NULL;
											Terminate(30);
										}
										else
										{
											/* set status */
											igStatus |= iF_ASSIGN_STRUCT_MEMORY;

											/* now read tables definitions */
											for (k=1; k<=ilAssignCounter; k++)
											{
												/* tables */	
												memset((void*)pclTmpBuf1, 0x00, iMIN_BUF_SIZE);
												if (flt_cp_nth_field(pclAssignBuf, k, pclTmpBuf1) != RC_SUCCESS)
												{
													/* this is not a fatal error */
													dbg(TRACE,"<init_cdiif> cannot copy nth field-assignment (%d)", k);
													Terminate(30);
												}
												else
												{
													/* write DBField and IFField to sturcture */
													memset((void*)pclTmpDBField, 0x00, iMIN_BUF_SIZE);
													memset((void*)pclTmpIFField, 0x00, iMIN_BUF_SIZE);
													if (SeparateIt(pclTmpBuf1, 
																	 pclTmpDBField,
																	 pclTmpIFField,
																	 cASSIGNSEPARATOR))
													{
														/* not a correct format */
														dbg(DEBUG,"<init_cdiif> not a correct format in assignment");
													}
													else
													{
														/* store data in struct */
														strncpy(rgTM.prCmd[i].prTabDef[j-1].prAssign[k-1].pcDBField, pclTmpDBField, iDBFIELD_SIZE);
														strncpy(rgTM.prCmd[i].prTabDef[j-1].prAssign[k-1].pcIFField, pclTmpIFField, iIFFIELD_SIZE);

														/* store length of both fields */
														rgTM.prCmd[i].prTabDef[j-1].prAssign[k-1].iDBFieldSize = strlen(pclTmpDBField);
														rgTM.prCmd[i].prTabDef[j-1].prAssign[k-1].iIFFieldSize = strlen(pclTmpIFField);
													}
												}
											}
										}
									}
								}
								dbg(DEBUG,"<init_cdiif> ------- END <%s> ------", pclTmpBuf);
							}
						}
					}
				}
			}
		}
	}

	/* get memory for receive data */
	if ((pcgReceiveData = (char*)malloc((size_t)iMAX_RECEIVE_DATA)) == NULL)
	{
		/* cannot run without momory */
		dbg(TRACE, "<init_cdiif> not enough memory to allocate receive data memory");
		pcgReceiveData = NULL;
		Terminate(30);
	}
	else
	{
		/* set status */
		igStatus |= iF_RECEIVE_DATA_MEMORY;

		/* clear memory */
		memset((void*)pcgReceiveData, 0x00, iMAX_RECEIVE_DATA);
	}

	/* get memory for send data */
	if ((prgData =	(TData*)malloc((size_t)iMAX_SEND_DATA)) == NULL)
	{
		dbg(TRACE, "<init_cdiif> not enough memory to allocate send data memory");
		prgData = NULL;
		Terminate(30);
	}
	else
	{
		/* set status */
		igStatus |= iF_SEND_DATA_MEMORY;

		/* clear memory */
		memset((void*)prgData, 0x00, iMAX_SEND_DATA);
	}

	/* we use the same memory area for mix (gepaeck-, andocksystem) telegrams 
	and data telegrams */
	prgMix = (TMix*)prgData;

	/* memory for recovery, if necessary */
	if (rgTM.rRec.iRecoverType == iRECOVER_MEMORY)
	{
		if ((prgRecover = (TRecover*)malloc((size_t)(rgTM.rRec.iRecoverCounter*sizeof(TRecover)))) ==NULL)
		{
			dbg(TRACE, "<init_cdiif> not enough memory to allocate recover memory");
			prgRecover = NULL;
			Terminate(30);
		}
		else
		{
			/* set status */
			igStatus |= iF_RECOVER_MEMORY;

			/* clear memory */
			memset((void*)prgRecover, 0x00, rgTM.rRec.iRecoverCounter*sizeof(TRecover));
		}
	}

	/* initialize RecoverHandler */
	rgTM.rRec.RHandler[iRECOVER_FILE]     = HandleFileRecovery;
	rgTM.rRec.RHandler[iRECOVER_MEMORY]   = HandleMemoryRecovery;

	/* open rprot file (if it is not open) */
	if (!(igStatus & iF_RPROT_FILE))
	{
		if (rgTM.iUseRProt)
		{
			/* try to open file */
			if ((igRProtFile = open(rgTM.rProt.pcRProtFile,
								O_RDWR | O_CREAT | O_APPEND, iPERMS)) == -1)
			{
				/* cannot open file */
				dbg(TRACE,"<init_cdiif> can't open file %s", rgTM.rProt.pcRProtFile);
			}
			else
			{
				/* first set flag in status word */
				igStatus |= iF_RPROT_FILE;
			}
		}
	}

	/* open sprot file */
	if (!(igStatus & iF_SPROT_FILE))
	{
		if (rgTM.iUseSProt)
		{
			/* try to open file */
			if ((igSProtFile = open(rgTM.rProt.pcSProtFile,
								O_RDWR | O_CREAT | O_APPEND, iPERMS)) == -1)
			{
				/* cannot open file */
				dbg(TRACE,"<init_cdiif> can't open file %s", rgTM.rProt.pcSProtFile);
			}
			else
			{
				/* first set flag in status word */
				igStatus |= iF_SPROT_FILE;
			}
		}
	}

	/* fail structure */
	rgFail.iSendErrorCounter = 0;
	rgFail.iReceiveErrorCounter = 0;

	/* init telegram number... */
	lgLastTeleNumber  = iINIT_TNUMBER;

	/* reset flag */
	igCCOCheck = 0;

	/* get mod_id of router */
	if ((igRouterID = tool_get_q_id("router")) == RC_NOT_FOUND ||
		  igRouterID == RC_FAIL)
	{
		dbg(TRACE,"<init_cdiif> tool_get_q_id(router) returns: %d", igRouterID);
		Terminate(30);
	}

	/* -------------------------------------------------------------------*/
	/* -------------------------------------------------------------------*/
	/* -------------------------------------------------------------------*/
	/* -------------------------------------------------------------------*/
	/* delete all section (action.cfg) for all cdi-server commands... */
	for (i=0; i<rgTM.iNoCmd; i++)
	{
		for (j=0; j<rgTM.prCmd[i].iNoTables; j++)
		{
			/* delete selection for all commands in action configuiration */
			ilRC = HandleActionSection(iDELETE_SECTION, rgTM.prCmd[i].prTabDef[j].pcSndCmd, rgTM.prCmd[i].pcCommand, i, j);
			dbg(DEBUG,"<init_cdiif> %sdelelete Section(s) <%s> with command <%s>",	ilRC == RC_SUCCESS ? "" : "can't ", rgTM.prCmd[i].pcCommand, rgTM.prCmd[i].prTabDef[j].pcSndCmd);
		}
	}

	/* und tschuess */
	return RC_SUCCESS;
} 

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset(void)
{
	int		i;
	int		j;
	int		k;
	int		ilRC = RC_SUCCESS;

	dbg(DEBUG,"<Reset> now resetting");

	/* TCP/IP-Socket shutdown + close */
	CloseTCPConnection();
	
	/* free memory */
	if (prgItem != NULL)
	{
		dbg(DEBUG,"<Reset> free prgItem");
		free ((void*)prgItem);
		prgItem = NULL;
	}

	/* free structure memory */
	if (igStatus & iF_CMD_STRUCT_MEMORY)
	{
		if (igStatus & iF_CMD_COMMAND_MEMORY)
		{
			for (i=0; i<rgTM.iNoCmd; i++)
			{
				if (igStatus & iF_TAB_STRUCT_MEMORY)
				{
					for (j=0; j<rgTM.prCmd[i].iNoTables; j++)
					{
						if (igStatus & iF_FIELD_MEMORY)
						{
							for (k=0; k<rgTM.prCmd[i].prTabDef[j].iNoFields; k++)
							{
								if (rgTM.prCmd[i].prTabDef[j].pcFields[k] != NULL)
								{
									dbg(DEBUG,"<Reset> free fields");
									free((void*)rgTM.prCmd[i].prTabDef[j].pcFields[k]);
									rgTM.prCmd[i].prTabDef[j].pcFields[k] = NULL;
								}
							}
						}
						if (igStatus & iF_FIELD_POINTER_MEMORY)
						{
							if (rgTM.prCmd[i].prTabDef[j].pcFields != NULL)
							{
								dbg(DEBUG,"<Reset> free pointer to fields");
								free ((void*)rgTM.prCmd[i].prTabDef[j].pcFields);
								rgTM.prCmd[i].prTabDef[j].pcFields = NULL;
							}
						}
						if (igStatus & iF_ASSIGN_STRUCT_MEMORY)
						{
							if (rgTM.prCmd[i].prTabDef[j].prAssign != NULL)
							{
								dbg(DEBUG,"<Reset> free assignment");
								free ((void*)rgTM.prCmd[i].prTabDef[j].prAssign);
								rgTM.prCmd[i].prTabDef[j].prAssign = NULL;
							}
						}
						if (igStatus & iF_DATA_LENGTH_MEMORY)
						{
							if (rgTM.prCmd[i].prTabDef[j].piDataLength != NULL)
							{
								dbg(DEBUG,"<Reset> free data length");
								free ((void*)rgTM.prCmd[i].prTabDef[j].piDataLength);
								rgTM.prCmd[i].prTabDef[j].piDataLength = NULL;
							}
						}
						for (k=0; k<rgTM.prCmd[i].prTabDef[j].iNoFields; k++)
						{
							if (rgTM.prCmd[i].prTabDef[j].piNoMapData[k] > 0)
							{
								if (rgTM.prCmd[i].prTabDef[j].prDataMap[k] != NULL)
								{
									dbg(DEBUG,"<Reset> free prDataMap[%d]", k);
									free((void*)rgTM.prCmd[i].prTabDef[j].prDataMap[k]);
									rgTM.prCmd[i].prTabDef[j].prDataMap[k] = NULL;
								}	
							}	
						}
						if (rgTM.prCmd[i].prTabDef[j].piNoMapData != NULL)
						{
							dbg(DEBUG,"<Reset> free piNoMapData");
							free((void*)rgTM.prCmd[i].prTabDef[j].piNoMapData);
							rgTM.prCmd[i].prTabDef[j].piNoMapData= NULL;
						}	
						if (rgTM.prCmd[i].prTabDef[j].prDataMap != NULL)
						{
							dbg(DEBUG,"<Reset> free prDataMap");
							free((void*)rgTM.prCmd[i].prTabDef[j].prDataMap);
							rgTM.prCmd[i].prTabDef[j].prDataMap = NULL;
						}	
					}
					if (rgTM.prCmd[i].prTabDef != NULL)
					{
						dbg(DEBUG,"<Reset> free prTabDef");
						free ((void*)rgTM.prCmd[i].prTabDef);
						rgTM.prCmd[i].prTabDef = NULL;
					}
				}		
				if (rgTM.prCmd[i].pcCommand != NULL)
				{
					dbg(DEBUG,"<Reset> free pcCommand");
					free ((void*)rgTM.prCmd[i].pcCommand);
					rgTM.prCmd[i].pcCommand = NULL;
				}
			}
		}
		if (rgTM.prCmd != NULL)
		{
			dbg(DEBUG,"<Reset> free prCmd");
			free ((void*)rgTM.prCmd);
			rgTM.prCmd = NULL;
		}
	}

	/* free Receive-Data memory */
	if (igStatus & iF_RECEIVE_DATA_MEMORY)
		if (pcgReceiveData != NULL)
		{
			free ((void*)pcgReceiveData);
			pcgReceiveData = NULL;
		}

	/* free send data memory */
	if (igStatus & iF_SEND_DATA_MEMORY)
		if (prgData != NULL)
		{
			free ((void*)prgData);
			prgData = NULL;
		}

	/* free recover memory */
	if (igStatus & iF_RECOVER_MEMORY)
		if (prgRecover != NULL)
		{
			free ((void*)prgRecover);
			prgRecover = NULL;
		}

	/* create new logfile... */
	/* 
	REINITIALITE;
	*/

	/* reset status-word... */
	/* set flags for open prot-file */
	igStatus = iF_INITIALIZE | iF_RPROT_FILE | iF_SPROT_FILE;

	/* new initisation... */
	if ((ilRC = init_cdiif(pcgFileName)) != RC_SUCCESS)
	{
		dbg(TRACE,"<Reset> init_cdiif  returns: %d", ilRC);
		Terminate(0);
	}

	/* only a message */
	dbg(DEBUG,"<RESET> will return with: %d", ilRC);

	/* bye bye */
	return ilRC;
} 

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(UINT ipSleepTime)
{
	int		i;
	int		j;
	int		k;

	dbg(TRACE,"<Terminate> now terminating...");

	/* ignore all signals */
	(void) UnsetSignals();

	if (ipSleepTime > 0)
	{
		dbg(DEBUG,"<Terminate> sleep %d seconds...", ipSleepTime);
		sleep(ipSleepTime);
	}
	
	/* TCP/IP-Socket shutdown + close */
	dbg(DEBUG,"<Terminate> close TCP/IP...");
	CloseTCPConnection();
	
	/* free memory */
	if (prgItem != NULL)
	{
		dbg(DEBUG,"<Terminate> free prgItem");
		free ((void*)prgItem);
		prgItem = NULL;
	}

	/* free structure memory */
	if (igStatus & iF_CMD_STRUCT_MEMORY)
	{
		if (igStatus & iF_CMD_COMMAND_MEMORY)
		{
			for (i=0; i<rgTM.iNoCmd; i++)
			{
				if (igStatus & iF_TAB_STRUCT_MEMORY)
				{
					for (j=0; j<rgTM.prCmd[i].iNoTables; j++)
					{
						if (igStatus & iF_FIELD_MEMORY)
						{
							for (k=0; k<rgTM.prCmd[i].prTabDef[j].iNoFields; k++)
							{
								if (rgTM.prCmd[i].prTabDef[j].pcFields[k] != NULL)
								{
									dbg(DEBUG,"<Terminate> free fields");
									free((void*)rgTM.prCmd[i].prTabDef[j].pcFields[k]);
									rgTM.prCmd[i].prTabDef[j].pcFields[k] = NULL;
								}
							}
						}
						if (igStatus & iF_FIELD_POINTER_MEMORY)
						{
							if (rgTM.prCmd[i].prTabDef[j].pcFields != NULL)
							{
								dbg(DEBUG,"<Terminate> free pointer to fields");
								free ((void*)rgTM.prCmd[i].prTabDef[j].pcFields);
								rgTM.prCmd[i].prTabDef[j].pcFields = NULL;
							}
						}
						if (igStatus & iF_ASSIGN_STRUCT_MEMORY)
						{
							if (rgTM.prCmd[i].prTabDef[j].prAssign != NULL)
							{
								dbg(DEBUG,"<Terminate> free assignment");
								free ((void*)rgTM.prCmd[i].prTabDef[j].prAssign);
								rgTM.prCmd[i].prTabDef[j].prAssign = NULL;
							}
						}
						if (igStatus & iF_DATA_LENGTH_MEMORY)
						{
							if (rgTM.prCmd[i].prTabDef[j].piDataLength != NULL)
							{
								dbg(DEBUG,"<Terminate> free data length");
								free ((void*)rgTM.prCmd[i].prTabDef[j].piDataLength);
								rgTM.prCmd[i].prTabDef[j].piDataLength = NULL;
							}
						}
						for (k=0; k<rgTM.prCmd[i].prTabDef[j].iNoFields; k++)
						{
							if (rgTM.prCmd[i].prTabDef[j].piNoMapData[k] > 0)
							{
								if (rgTM.prCmd[i].prTabDef[j].prDataMap[k] != NULL)
								{
									dbg(DEBUG,"<Terminate> free prDataMap[%d]", k);
									free((void*)rgTM.prCmd[i].prTabDef[j].prDataMap[k]);
									rgTM.prCmd[i].prTabDef[j].prDataMap[k] = NULL;
								}	
							}	
						}
						if (rgTM.prCmd[i].prTabDef[j].piNoMapData != NULL)
						{
							dbg(DEBUG,"<Terminate> free piNoMapData");
							free((void*)rgTM.prCmd[i].prTabDef[j].piNoMapData);
							rgTM.prCmd[i].prTabDef[j].piNoMapData= NULL;
						}	
						if (rgTM.prCmd[i].prTabDef[j].prDataMap != NULL)
						{
							dbg(DEBUG,"<Terminate> free prDataMap");
							free((void*)rgTM.prCmd[i].prTabDef[j].prDataMap);
							rgTM.prCmd[i].prTabDef[j].prDataMap = NULL;
						}	
					}
					if (rgTM.prCmd[i].prTabDef != NULL)
					{
						dbg(DEBUG,"<Terminate> free prTabDef");
						free ((void*)rgTM.prCmd[i].prTabDef);
						rgTM.prCmd[i].prTabDef = NULL;
					}
				}		
				if (rgTM.prCmd[i].pcCommand != NULL)
				{
					dbg(DEBUG,"<Terminate> free pcCommand");
					free ((void*)rgTM.prCmd[i].pcCommand);
					rgTM.prCmd[i].pcCommand = NULL;
				}
			}
		}
		if (rgTM.prCmd != NULL)
		{
			dbg(DEBUG,"<Terminate> free prCmd");
			free ((void*)rgTM.prCmd);
			rgTM.prCmd = NULL;
		}
	}

	/* free Receive-Data memory */
	if (igStatus & iF_RECEIVE_DATA_MEMORY)
		if (pcgReceiveData != NULL)
		{
			dbg(DEBUG,"<Terminate> free prgReceiveData");
			free ((void*)pcgReceiveData);
			pcgReceiveData = NULL;
		}

	/* free send data memory */
	if (igStatus & iF_SEND_DATA_MEMORY)
		if (prgData != NULL)
		{
			dbg(DEBUG,"<Terminate> free prgData");
			free ((void*)prgData);
			prgData = NULL;
		}

	/* free recover memory */
	if (igStatus & iF_RECOVER_MEMORY)
		if (prgRecover != NULL)
		{
			dbg(DEBUG,"<Terminate> free prgRecover");
			free ((void*)prgRecover);
			prgRecover = NULL;
		}

	if (igStatus & iF_RPROT_FILE)
	{
		dbg(DEBUG, "<Terminate> close now rprot file");
		close (igRProtFile);
	}

	if (igStatus & iF_SPROT_FILE)
	{
		dbg(DEBUG, "<Terminate> close now sprot file");
		close (igSProtFile);
	}

	/* default error message */
	dbg(TRACE,"Terminate: now leaving ...");

	/* close LOG-File */
	CLEANUP;
	
	/* bye bye */
	exit(0);
} 

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
	/* write message if it is not alarm clock */
	if (pipSig != 14 && pipSig != 18)
		dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

	switch(pipSig)
	{
		case SIGALRM:
		case SIGCLD:
			/*
			SetSignals(HandleSignal);
			*/
			break;

		case SIGPIPE:
			Terminate(igRestartTimeout);
			break;

		default:
			exit(0);
			break;
	} 
	return;
}

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int ipErr)
{
	dbg(DEBUG,"<HandleErr> calling with ErrorCode: %d", ipErr);

	return;
}

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int *pipErr)
{
	switch (*pipErr) 
	{
		case	QUE_E_FUNC	:	/* Unknown function */
			dbg(TRACE,"<%d> : unknown function", *pipErr);
			break;

		case	QUE_E_MEMORY	:	/* Malloc reports no memory */
			dbg(TRACE,"<%d> : malloc failed", *pipErr);
			break;

		case	QUE_E_SEND	:	/* Error using msgsnd */
			dbg(TRACE,"<%d> : msgsnd failed", *pipErr);
			break;

		case	QUE_E_GET	:	/* Error using msgrcv */
			dbg(TRACE,"<%d> : msgrcv failed", *pipErr);
			break;

		case	QUE_E_EXISTS	:
			dbg(TRACE,"<%d> : route/queue already exists ", *pipErr);
			break;

		case	QUE_E_NOFIND	:
			dbg(TRACE,"<%d> : route not found ", *pipErr);
			break;

		case	QUE_E_ACKUNEX	:
			dbg(TRACE,"<%d> : unexpected ack received ", *pipErr);
			break;

		case	QUE_E_STATUS	:
			dbg(TRACE,"<%d> :  unknown queue status ", *pipErr);
			break;

		case	QUE_E_INACTIVE	:
			dbg(TRACE,"<%d> : queue is inaktive ", *pipErr);
			break;

		case	QUE_E_MISACK	:
			dbg(TRACE,"<%d> : missing ack ", *pipErr);
			break;

		case	QUE_E_NOQUEUES	:
			dbg(TRACE,"<%d> : queue does not exist", *pipErr);
			break;

		case	QUE_E_RESP	:	/* No response on CREATE */
			dbg(TRACE,"<%d> : no response on create", *pipErr);
			break;

		case	QUE_E_FULL	:
			dbg(TRACE,"<%d> : too many route destinations", *pipErr);
			break;

		case	QUE_E_NOMSG	:	/* No message on queue */
			/******
			dbg(TRACE,"<%d> : no messages on queue", *pipErr);
			******/
			*pipErr = RC_SUCCESS;
			break;

		case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
			dbg(TRACE,"<%d> : invalid originator=0", *pipErr);
			break;

		case	QUE_E_NOINIT	:	/* Queues is not initialized*/
			dbg(TRACE,"<%d> : queues are not initialized", *pipErr);
			break;

		case	QUE_E_ITOBIG	:
			dbg(TRACE,"<%d> : requestet itemsize to big ", *pipErr);
			break;

		case	QUE_E_BUFSIZ	:
			dbg(TRACE,"<%d> : receive buffer to small ", *pipErr);
			break;

		default			:	/* Unknown queue error */
			dbg(TRACE,"<%d> : unknown error", *pipErr);
			break;
	} 
	return;
}

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues(void)
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;

	/* init... */
	do
	{
		/* sleep n seconds... */
		sleep(2);

		/* free memory */
		if (prgItem != NULL)
		{
			free ((void*)prgItem);
			prgItem = NULL;
		}
	
		/* get data with no waiting... */
		ilRC = que(QUE_GETBIGNW, 0, mod_id, PRIORITY_3, 0,(char*)&prgItem);

		/* set pointer... */
		prgEvent = (EVENT*)prgItem->text;
		
		if (ilRC == RC_SUCCESS)
		{
			if ((ilRC = que(QUE_ACK, 0, mod_id, 0, 0, NULL)) != RC_SUCCESS)
			{
				HandleQueErr(&ilRC);
			} 
		
			switch (prgEvent->command)
			{
				case HSB_STANDBY:
				case HSB_COMING_UP:
					ctrl_sta = prgEvent->command;
					break;	
		
				case HSB_ACTIVE:
				case HSB_STANDALONE:
					ctrl_sta = prgEvent->command;
					ilBreakOut = TRUE;
					break;	

				case HSB_ACT_TO_SBY:
					ctrl_sta = prgEvent->command;
					break;	
		
				case HSB_DOWN:
					ctrl_sta = prgEvent->command;
					Terminate(0);
					break;	
		
				case SHUTDOWN:
					Terminate(0);
					break;
							
				case RESET:
					if ((ilRC = Reset()) != RC_SUCCESS)
						HandleErr(ilRC);
					break;
							
				case EVENT_DATA:
					dbg(DEBUG,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;
						
				case TRACE_ON:
					dbg_handle_debug(prgEvent->command);
					break;

				case TRACE_OFF:
					dbg_handle_debug(prgEvent->command);
					break;

				default:
					dbg(DEBUG,"HandleQueues: unknown event");
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;
			} 
		} 
		else 
		{
			HandleQueErr(&ilRC);
		} 

		/* free memory */
		if (prgItem != NULL)
		{
			free ((void*)prgItem);
			prgItem = NULL;
		}
	
	} while (ilBreakOut == FALSE);

	/* bye bye */
	return;
} 
	
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(void)
{
	int			i;
	int			ilRNo;
	int			ilFound;
	int			ilRC;
	int			ilCmdNo;
	int			ilTabNo;
	int			ilLines;
	int			ilAssignNo;
	int			ilNoOfReceivedFields;
	int			ilUrnoPos;
	int			ilMissingField;
	int			ilType;
	int			ilTimeDiff;
	int			ilFlg;
	int			ilTimeCnt;
	int			ilVialInList = -1;
	int			ilVialInData = -1;
	UINT			ilCurLine;
	long			lLen;
	long			lMin;
	long			lMax;
	long			lUniqueNumber;
	char			pclCfgFile[iMIN_BUF_SIZE];
	char			pclLocalTime[15];
	char			pclUtcTime[15];
	char			pclTmpKeyWord[iMIN];
	char			pclTmpBuf[iMAXIMUM];
	char			pclTmpFieldBuf[iMAXIMUM];
	char			pclVialNum[2] = "1";
	char			*pclCfgPath			= NULL;
	char			*pclS					= NULL;
	char			*pclTmpPtr			= NULL;
	BC_HEAD		*prlBCHead			= NULL;
	CMDBLK		*prlCmdblk			= NULL;
	char			*pclSelection		= NULL;
	char			*pclFields			= NULL;
	char			*pclData				= NULL;
	char			*pclVialBgn = NULL;
	char			*pclVialEnd = NULL;
	int old_debug_level=0;
	int ilErrCount = 0;

	/* from sending to other process */
	EVENT			*prlOutEvent		= NULL;
	BC_HEAD		*prlOutBCHead		= NULL;
	CMDBLK		*prlOutCmdblk		= NULL;
	char			pclOutSelection[iMAX_BUF_SIZE];
	char			pclOutFields[iMAXIMUM];
	char			pclOutData[iMAX_BUF_SIZE];

	/* init */
	ilCmdNo = -1;
	ilTabNo = -1;

	/* set pointer we need */
	prlBCHead 	 = (BC_HEAD*)((char*)prgEvent+sizeof(EVENT));
	if (prlBCHead->rc != RC_SUCCESS)
	{
		/* only a message */
		dbg(DEBUG,"<HandleData> BCHead returncode: %d", prlBCHead->rc);
		dbg(DEBUG,"<HandleData> String: <%s>", prlBCHead->data);

		/* send message to db, ceda.log... */
		memset((void*)pclTmpBuf, 0x00, iMAXIMUM);
		sprintf(pclTmpBuf, "%s;%d", prlBCHead->data, prgEvent->originator);
		pclS = pclTmpBuf;
		while (*pclS)
		{
			if (*pclS == ',')
				*pclS = '-';
			pclS++;	
		}
		if (strlen(pclTmpBuf) > 255)
		{
			/* field data in logtab has only 255 bytes... */
			pclTmpBuf[255] = 0x00;
			dbg(DEBUG,"<HandleData> cutting error buffer at position 255...");
		}
		
		/* send error message to message handler and write with max. prio DB */
		if ((ilRC = send_message(IPRIO_DB, iERROR_FROM_DEST, 0, 0, pclTmpBuf)) != RC_SUCCESS)
		{
			dbg(TRACE,"<HandleData> send_message returns: %d", ilRC);
		}

		/* check error message we received... */
		memset((void*)pclTmpBuf, 0x00, iMAXIMUM);
		strcpy(pclTmpBuf, prlBCHead->data);
		StringUPR((UCHAR*)pclTmpBuf);
		if (strstr(pclTmpBuf, "UNKNOWN CEDA COMMAND") != NULL)
		{
			/* this is an internal error... */
			dbg(DEBUG,"<HandleData> internal error: unkown router command...");
			return WriteERRToClient(sINTERNAL_ERROR);
		}
	
		/* must set pointer to command block */
		prlCmdblk = (CMDBLK*)((char*)prlBCHead->data + 
									 strlen((char*)prlBCHead->data) + 1);

		/* get Command- and Table-Number */
		dbg(DEBUG,"<HandleData> tw_start: <%s>", prlCmdblk->tw_start);
		ilCmdNo = atoi(GetDataField(prlCmdblk->tw_start, 1, ','));
		ilTabNo = atoi(GetDataField(prlCmdblk->tw_start, 2, ','));
		dbg(DEBUG,"<HandleData> CMDNO: %d, TABNO: %d", ilCmdNo, ilTabNo);

		/* check command- and table number */
		if (ilCmdNo < 0 || ilTabNo < 0)
		{
			dbg(TRACE,"<HandleData> invalid command and/or table number...");
			return WriteERRToClient(sINVALID_CMD);
		}

		/* incr. data counter */
		rgTM.prCmd[ilCmdNo].iDataCounter++;
		
		/* send last and/or write file, only for data telegrams */
		if (rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iTelegramClass == iDATA_CLASS)
		{
			if (rgTM.prCmd[ilCmdNo].iDataCounter == rgTM.prCmd[ilCmdNo].iNoTables)
			{
				/* set counter to zero */
				rgTM.prCmd[ilCmdNo].iDataCounter = 0;

				/* write file to machine... */ 
				return SendFileAndWriteEOD(ilCmdNo, rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcDataType, prlCmdblk->command);
			}
			else
				return WriteERRToClient(sNO_DATA);
		}
		else
			return WriteERRToClient(sNO_DATA);
	}

	/* set pointer */
	prlCmdblk 	 = (CMDBLK*)prlBCHead->data;
	pclSelection = prlCmdblk->data;
	pclFields 	 = prlCmdblk->data + strlen(pclSelection) + 1;
	pclData   	 = prlCmdblk->data + strlen(pclSelection) + 
						strlen(pclFields) + 2;

	pclVialBgn = strstr(pclFields, "VIAL");
	if (pclVialBgn != NULL)
	{
		dbg(TRACE,"FOUND VIAL IN RECEIVED FIELDS");
	}
	else
	{
		dbg(TRACE,"VIAL NOT FOUND IN RECEIVED FIELDS");
	}

	/* HARDCODED for xAircon!!! */
	if (!strcmp(prlCmdblk->command, "XAC"))
	{
		dbg(DEBUG,"<HandleData> ------- START <%s> -------", prlCmdblk->command);
		if ((pclCfgPath = getenv("CFG_PATH")) == NULL)
		{
			/* environment not set */
			dbg(TRACE, "<HandleData> error reading environment (CFG_PATH)");	
			Terminate(0);
		}
		else
		{
			/* found environment-variable */
			/* build path/filename */
			memset ((void*)pclCfgFile, 0x00, iMIN_BUF_SIZE);
			sprintf(pclCfgFile, "%s/%s", pclCfgPath, pcgFileName);
		}

		/* build selection */
		memset((void*)pclOutSelection, 0x00, iMAX_BUF_SIZE);
		if (rgTM.prCmd[0].prTabDef[0].iUseSelection)
		{
			strcpy(pclOutSelection, rgTM.prCmd[0].prTabDef[0].pcSelect);

			/* count strings between... */
			ilTimeCnt = 0;
						
			/* for all strings between... */
			while ((ilRC = GetAllBetween(rgTM.prCmd[0].prTabDef[0].pcSelect, '\'', ilTimeCnt, pclTmpKeyWord)) == 0)
			{
				/* is this minute, day or month? */
				if (strstr(pclTmpKeyWord, "minute") != NULL || 
					 strstr(pclTmpKeyWord, "MINUTE") != NULL)
				{
					/* this is minute */
					ilType = iMINUTES;
				}
				else
				{
					if (strstr(pclTmpKeyWord, "day") != NULL || 
						 strstr(pclTmpKeyWord, "DAY") != NULL)
					{
						/* this is day */
						ilType = iDAYS;
					}
					else
					{
						if (strstr(pclTmpKeyWord, "month") != NULL || 
							 strstr(pclTmpKeyWord, "MONTH") != NULL)
						{
							/* this is month */
							ilType = iMONTH;
						}
						else
						{
							/* can't convert it... */
							/* default for type... */
							ilType = -1;
						}
					}
				}

				/* get cfg entry on this types... */
				if (ilType == iMINUTES || ilType == iDAYS || ilType == iMONTH)
				{
					sprintf(pcgTmpTabName, "%sHAJ", prlCmdblk->command); /* Hardcoded for xAircon !!!! */
					if ((ilRC = iGetConfigEntry(pclCfgFile, pcgTmpTabName, pclTmpKeyWord, CFG_INT, (char*)&ilTimeDiff)) != RC_SUCCESS)
					{
						dbg(DEBUG,"<HandleData> can't find key word <%s> in CFG-File", pclTmpKeyWord);
						Terminate(0);
					}
				
					/* set start/end-flag for AddToTime... */
					if (strstr(pclTmpKeyWord,"start") != NULL)
					{
						ilFlg = iSTART;
					}
					else if (strstr(pclTmpKeyWord,"end") != NULL)
					{
						ilFlg = iEND;
					}
					else 
					{
						dbg(DEBUG,"<HandleData> missing start/end sign in key word <%s>, use default (start)", pclTmpKeyWord);
						ilFlg = iSTART;
					}

					/* calculate timestamp... */
					pclS = AddToCurrentUtcTime(ilType, ilTimeDiff, ilFlg);
					strcpy(pclLocalTime,	pclS);

					/* sub -1 hour from timestamp... */
					if (ilType != iMINUTES)
					{
						strcpy(pclUtcTime, s14_BLANKS);
						/* Fix start 
						strcpy(pclUtcTime, pclLocalTime);
						AddSecondsToCEDATime(pclUtcTime,-7200,1);
						strcpy(pclLocalTime, pclUtcTime);
						 Fix end */

						if ((ilRC = LocalToUtc(pclLocalTime,pclUtcTime)) != RC_SUCCESS)
						{
							dbg(TRACE,"<HandleData> LocalToUtc returns %d", ilRC);
							dbg(TRACE,"<HandleData> using old timestamp");
						}
						else
						{
							strcpy(pclLocalTime, pclUtcTime);
						}
					}

					
					/* search keyword and replace with timestamp... */
					if ((ilRC = SearchStringAndReplace(pclOutSelection, pclTmpKeyWord, pclLocalTime)) < 0)
					{
						dbg(TRACE,"<HandleData> SearchStringAndReplace returns %d", ilRC);
						Terminate(0);
					}
				}

				/* incr. cnt, this is necessary to get all strings between. */
				ilTimeCnt++;
			}
		}
		dbg(DEBUG,"<HandleData> build Selection: <%s>", pclOutSelection);

		/* build fieldlist for request */
		memset((void*)pclOutFields, 0x00, iMAXIMUM);
		for (i=0; i<rgTM.prCmd[0].prTabDef[0].iNoFields; i++)
		{
			strcat(pclOutFields, rgTM.prCmd[0].prTabDef[0].pcFields[i]);
			if (i<rgTM.prCmd[0].prTabDef[0].iNoFields-1)
				strcat(pclOutFields, ",");
		}
		dbg(DEBUG,"<HandleData> build fieldlist: <%s>", pclOutFields);

		/* calculate size of memory we need */
		lLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
				 strlen(pclOutSelection) + strlen(pclOutFields) + 
				 strlen(pclOutData) + 20; 

		/* get memory for out event */
		if ((prlOutEvent = (EVENT*)malloc((size_t)lLen)) == NULL)
		{
			dbg(TRACE,"<HandleData> malloc failed, can't allocate %ld bytes", lLen);
			prlOutEvent = NULL;
			Terminate(0);
		}

		/* clear buffer */
		memset((void*)prlOutEvent, 0x00, lLen);

		/* set structure members */
		prlOutEvent->type 		 = SYS_EVENT;
		prlOutEvent->command 	 = EVENT_DATA;
		prlOutEvent->originator  = mod_id;
		prlOutEvent->retry_count = 0;
		prlOutEvent->data_offset = sizeof(EVENT);
		prlOutEvent->data_length = lLen - sizeof(EVENT);

		/* BCHead members */
		prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
		prlOutBCHead->rc = RC_SUCCESS;
		strcpy(prlOutBCHead->dest_name, pcgCDIServerName);
		strcpy(prlOutBCHead->recv_name, "EXCO");

		/* CMDBLK members */
		prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
		strcpy(prlOutCmdblk->command, "GFRC");
		strcpy(prlOutCmdblk->obj_name, rgTM.prCmd[0].prTabDef[0].pcTabName);
		strcpy(prlOutCmdblk->tw_start, "XXX,0,0");
		sprintf(prlOutCmdblk->tw_end, "%s,%s,%s", rgTM.prCmd[0].pcHomeAirport, rgTM.prCmd[0].pcTableExtension, pcgCDIServerName);

		dbg(DEBUG,"<HandleData> table name: <%s>", prlOutCmdblk->obj_name);

		/* Selection */
		strcpy(prlOutCmdblk->data, pclOutSelection);

		/* fields */
		strcpy(prlOutCmdblk->data+strlen(pclOutSelection)+1, pclOutFields);

		/* data */
		strcpy(prlOutCmdblk->data+strlen(pclOutSelection)+strlen(pclOutFields)+2, pclOutData);

		/* send this message */
		if ((ilRC = que(QUE_PUT, igRouterID, mod_id, PRIORITY_4, lLen, (char*)prlOutEvent)) != RC_SUCCESS)
		{
			dbg(TRACE,"<HandleData> QUE_PUT returns: %d", ilRC);
			Terminate(0);
		}

		/* delete memory */
		free((void*)prlOutEvent);

		dbg(DEBUG,"<HandleData> ------- END <%s> -------", prlCmdblk->command);
		/* break here.... */
		return RC_SUCCESS;
	}

	/* check command... */
	if ((strstr(prlCmdblk->tw_start, "GOP") != NULL) ||
		 (strstr(prlCmdblk->tw_start, "GCC") != NULL) ||
		 (strstr(prlCmdblk->tw_start, "GLM") != NULL) ||
		 (strstr(prlCmdblk->tw_start, "AON") != NULL) ||
		 (strstr(prlCmdblk->tw_start, "AOF") != NULL) ||
		 (strstr(prlCmdblk->tw_start, "AOO") != NULL) ||
		 (strstr(prlCmdblk->tw_start, "ABA") != NULL) ||
		 (strstr(prlCmdblk->tw_start, "AEM") != NULL) ||
		 (strstr(prlCmdblk->tw_start, "XFD") != NULL) ||
		 !strcmp(prlCmdblk->command, "GMN"))
	{
		 /* It looks always good...  */
		 return RC_SUCCESS;
	}

	/* get Command- and Table-Number */
	dbg(DEBUG,"<HandleData> tw_start: <%s>", prlCmdblk->tw_start);
	ilCmdNo = atoi(GetDataField(prlCmdblk->tw_start, 1, ','));
	ilTabNo = atoi(GetDataField(prlCmdblk->tw_start, 2, ','));
	dbg(DEBUG,"<HandleData> CMDNO: %d, TABNO: %d", ilCmdNo, ilTabNo);
	dbg(DEBUG,"<HandleData> ---- START REQUEST/ANSWER FROM: %d ----", prgEvent->originator);
	dbg(DEBUG,"<HandleData> --------- CMD <%s> ---------", prlCmdblk->command);
	
	/* only for vaild command AND table numbers... */
	if (ilCmdNo >= 0 && ilTabNo >= 0)
	{
		memset((void*)pcgTmpTabName, 0x00, iMIN);
		sprintf(pcgTmpTabName, "CCA%s", rgTM.prCmd[ilCmdNo].pcTableExtension);
		if (!strcmp(rgTM.prCmd[ilCmdNo].pcCommand,"SFU") && !strcmp(rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcTabName, pcgTmpTabName))
		{
			/* check data we need... */
			for (i=0, ilMissingField=0; i<rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iNoFields; i++)
			{
				dbg(DEBUG,"<HandleData> compare: <%s> and <%s>", pclFields, rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcFields[i]);
				if (strstr(pclFields, rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcFields[i]) == NULL)
				{
					ilMissingField = 1;
					break;
				}
			}
			if (ilMissingField)
			{
				/* request for data */
				/* we need URNO for selection... */	
				if (strstr(pclFields, "URNO") != NULL)
				{
					memset((void*)pclOutSelection, 0x00, iMAX_BUF_SIZE);
					memset((void*)pclOutFields, 0x00, iMAXIMUM);
					memset((void*)pclOutData, 0x00, iMAX_BUF_SIZE);

					/* build fieldlist for request */
					for (i=0; i<rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iNoFields; i++)
					{
						strcat(pclOutFields, rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcFields[i]);
						if (i<rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iNoFields-1)
							strcat(pclOutFields, ",");
					}
					dbg(DEBUG,"<HandleData> build fieldlist: <%s>", pclOutFields);

					/* get position of urno */
					if ((ilUrnoPos = GetIndex(pclFields, "URNO", ',')) >= 0)
					{
						/* get urno itself */
						if ((pclS = GetDataField(pclData, ilUrnoPos, ',')) != NULL)
						{
							/* build selection */
							sprintf(pclOutSelection, "WHERE URNO=%s", pclS);
							dbg(DEBUG,"<HandleData> build Selection: <%s>", pclOutSelection);

							/* calculate size of memory we need */
							lLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
									 strlen(pclOutSelection) + strlen(pclOutFields) + 
									 strlen(pclOutData) + 20; 

							/* get memory for out event */
							if ((prlOutEvent = (EVENT*)malloc((size_t)lLen)) == NULL)
							{
								dbg(TRACE,"<HandleData> malloc failed, can't allocate %ld bytes", lLen);
								prlOutEvent = NULL;
								Terminate(0);
							}

							/* clear buffer */
							memset((void*)prlOutEvent, 0x00, lLen);

							/* set structure members */
							prlOutEvent->type 		 = SYS_EVENT;
							prlOutEvent->command 	 = EVENT_DATA;
							prlOutEvent->originator  = mod_id;
							prlOutEvent->retry_count = 0;
							prlOutEvent->data_offset = sizeof(EVENT);
							prlOutEvent->data_length = lLen - sizeof(EVENT);

							/* BCHead members */
							prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
							prlOutBCHead->rc = RC_SUCCESS;
							strcpy(prlOutBCHead->dest_name, pcgCDIServerName);
							strcpy(prlOutBCHead->recv_name, "EXCO");

							/* CMDBLK members */
							prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
							strcpy(prlOutCmdblk->command, "RTA");
							strcpy(prlOutCmdblk->obj_name, 
									 rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcTabName);
							strcpy(prlOutCmdblk->tw_start, prlCmdblk->tw_start);
							strcpy(prlOutCmdblk->tw_end, prlCmdblk->tw_end);

							dbg(DEBUG,"<HandleData> sending command: <RTA>");
							dbg(DEBUG,"<HandleData> table name: <%s>", prlOutCmdblk->obj_name);

							/* Selection */
							strcpy(prlOutCmdblk->data, pclOutSelection);

							/* fields */
							strcpy(prlOutCmdblk->data+strlen(pclOutSelection)+1, pclOutFields);

							/* data */
							strcpy(prlOutCmdblk->data+strlen(pclOutSelection)+strlen(pclOutFields)+2, pclOutData);

							/* send this message */
							if ((ilRC = que(QUE_PUT, igRouterID, mod_id, PRIORITY_4, lLen, (char*)prlOutEvent)) != RC_SUCCESS)
							{
								dbg(TRACE,"<HandleData> QUE_PUT returns: %d", ilRC);
								Terminate(0);
							}

							/* delete memory */
							free((void*)prlOutEvent);
						}
					}
				}
				else
				{
					dbg(DEBUG,"<HandleData> can't find URNO");
				}

				/* break here */
				return RC_SUCCESS;
			}
			else
			{
				dbg(DEBUG,"<HandleData> found all neccessary fields...");
				strcpy(prlCmdblk->command, "UFR");
			}
		}

		/* count number of current (received) fields */
		if ((ilNoOfReceivedFields = GetNoOfElements(pclFields, cSEPARATOR)) < 0)
		{
			dbg(TRACE,"<HandleData> GetNoOfElements (pclFields) returns: %d", ilNoOfReceivedFields);
			Terminate(0);
		}

		dbg(DEBUG,"<HandleData> received field: <%s>", pclFields);
		/* map received fields... */
		memset((void*)pclTmpFieldBuf, 0x00, iMAXIMUM);
		for (ilRNo=0; ilRNo<ilNoOfReceivedFields; ilRNo++)
		{
			if ((pclS = GetDataField(pclFields, (UINT)ilRNo, cSEPARATOR)) == NULL)
			{
				dbg(TRACE,"<HandleData> GetDataField (received fields) returns NULL");
				Terminate(0);
			}

			for (ilAssignNo=0, ilFound=0; 
				  ilAssignNo<rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iNoAssign;
				  ilAssignNo++)
			{
				/* is there an existing entry for ist field? */
				if (!strcmp(rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].prAssign[ilAssignNo].pcDBField, pclS))
				{
					/* found field for mapping... */	
					strcat(pclTmpFieldBuf, rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].prAssign[ilAssignNo].pcIFField);
					strcat(pclTmpFieldBuf, ",");
					ilFound=1;
					break;
				}
			}	
			if (!ilFound)
			{
				strcat(pclTmpFieldBuf, pclS);
				strcat(pclTmpFieldBuf, ",");
			}
		}

		/* delete last comma */
		if (pclTmpFieldBuf[strlen(pclTmpFieldBuf)-1] == ',')
			pclTmpFieldBuf[strlen(pclTmpFieldBuf)-1] = 0x00;
		dbg(DEBUG,"<HandleData> mapped field: <%s>", pclTmpFieldBuf);

		/* incr. data counter */
		rgTM.prCmd[ilCmdNo].iDataCounter++;

		/* assignment mapping */
		for (i=0; i<rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iNoFields; i++)
		{
			/* look for assignment! */
			for (ilAssignNo=0; 
				  ilAssignNo<rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iNoAssign;
				  ilAssignNo++)
			{
				/* is there an existing entry for ist field? */
				if (!strcmp(rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].prAssign[ilAssignNo].pcDBField, rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcFields[i]))
				{
					/* check memory for fields... */
					if (rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].prAssign[ilAssignNo].iDBFieldSize < rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].prAssign[ilAssignNo].iIFFieldSize)
					{
						/* length of db-field is snaller than if-field */
						/* free memory */
						free((void*)rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcFields[i]);

						/* copy assignment to field list */
						if ((rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcFields[i] = strdup(rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].prAssign[ilAssignNo].pcIFField)) == NULL)
						{
							/* mot enough memory */
							dbg(TRACE,"<HandleData> can't change fields1...(memory)");
							rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcFields[i] = NULL;
							Terminate(0);
						}

						/* set new length */
						rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].prAssign[ilAssignNo].iDBFieldSize = rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].prAssign[ilAssignNo].iIFFieldSize;
					}
					else
					{
						/* length of db-field is equal or greater than if-field */
						strcpy(rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcFields[i], rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].prAssign[ilAssignNo].pcIFField);
					}
				}
			}
		}

		/* count number of datablocks */
		if (strlen(pclData) > 0)
		{
			if ((ilLines = GetNoOfElements(pclData, '\n')) < 0)
			{
				dbg(TRACE,"<HandleData> GetNoOfElements returns: %d", ilLines);
				Terminate(0);
			}
			dbg(DEBUG,"<HandleData> found %d records...", ilLines);

			/* get next unique numbers */
			/* and set pointer to key... */
			switch (rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iTelegramClass)
			{
				case iMIX_CLASS:
				case iDATA_CLASS:
					pclS = pcgDATName;
					break;

				case iMDEL_CLASS:
				case iDDEL_CLASS:
					pclS = pcgDELName;
					break;

				default:
					dbg(TRACE,"<HandleData> %05d unknown telegram class: %d", __LINE__, rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iTelegramClass);
					Terminate(0);
					break;
			}

			/* get next numbers from correct number circle... */
			dbg(DEBUG,"<HandleData> GetUniqueNumber with class: <%s>", pclS);

/*HEB*/
			old_debug_level = debug_level;
			debug_level = TRACE;

			ilErrCount = 0;
			while (((ilRC = GetUniqueNumber(pcgServerName, pclS, "MAXOW", ilLines, &lUniqueNumber, &lMax, &lMin)) != RC_SUCCESS) && ilErrCount < 3)
			{
				/* fatal error */
				dbg(TRACE,"<HandleData> GetUniqueNumber returns: %d, ilErrCount = <%d>", ilRC,ilErrCount);
				ilErrCount++;

			}

			if (ilRC != RC_SUCCESS)
			{
				/* reset flag... */
				igStatus &= ~iF_TCPIP_CLIENT_SOCK;
				
				/* exit process */
				Terminate(0);

			}
			debug_level = old_debug_level;

				/* Modification for ATH only:                            */
				/* (All FIDS Flags in VIAL must be set automatically)    */
				/* Here we can patch the FIDS flag for the vias 1 - 8    */
				/* In order to do that we must:                          */
				/* Find VIAL in the received field list                  */
				/* Find the first byte position of VIAL in the data line */
				/* Patch the first byte of all valid entries in VIAL     */

			dbg(DEBUG,"FIELDS TO BE PROCESSED:\n<%s>",pclTmpFieldBuf);

			pclVialBgn = strstr(pclTmpFieldBuf, "VIAL");
			if (pclVialBgn != NULL)
			{
				/* must count commas before VIAL */
				/* temp-pointer to internal fields */
				pclTmpPtr = pclTmpFieldBuf;
				ilVialInList = 0;
				while (pclTmpPtr <= pclVialBgn)
				{
					if (*pclTmpPtr == ',')
					{
						ilVialInList++;
					}
					pclTmpPtr++;
				}
				dbg(DEBUG,"FOUND VIAL IN INTERNAL FIELDS AFTER %d COMMAS", ilVialInList);
			}
			else
			{
				ilVialInList = -1;
				dbg(DEBUG,"NO VIAL TO BE PROCESSED");
			}

			/* temp-pointer to data area */
			pclTmpPtr = pclData;

			/* over all lines... */
			for (ilCurLine=0; ilCurLine<ilLines; ilCurLine++, lUniqueNumber++)
			{
				/* set unique number */
				lUniqueNumber = UNIQUE(lUniqueNumber, lMax, lMin);	

				/* get data for writing */
				memset(pclTmpBuf, 0x00, iMAXIMUM);
				pclTmpPtr = CopyNextField(pclTmpPtr, '\n', pclTmpBuf);

				if (pclTmpBuf[strlen(pclTmpBuf)-1] == '\r')
					pclTmpBuf[strlen(pclTmpBuf)-1] = 0x00;

				if (ilVialInList >= 0)
				{
					/* Must process VIAL patching */
					/* Find begin of VIAL data */
					pclVialBgn = pclTmpBuf;

					ilVialInData = 0;
					while ((ilVialInData < ilVialInList) && (*pclVialBgn != '\0'))
					{
						if (*pclVialBgn == ',')
						{
							ilVialInData++;
						}
						pclVialBgn++;
					}
					dbg(DEBUG,"FOUND VIAL IN DATA BUFFER AFTER %d COMMAS", ilVialInData);

					if (ilVialInData == ilVialInList)
					{
						/* must find end of VIAL data */
						pclVialEnd = pclVialBgn;
						while ((*pclVialEnd != ',') && (*pclVialEnd != '\0'))
						{
							pclVialEnd++;
						}

						/* now we can step through */
						pclVialNum[0] = '1';
						while ((pclVialBgn < pclVialEnd) && (*pclVialBgn != '\0'))
						{
							/* check apc3 or apc4 */
							if ((pclVialBgn[1] != ' ') || (pclVialBgn[4] != ' '))
							{
								*pclVialBgn = pclVialNum[0];
								pclVialNum[0]++;
							}
							pclVialBgn += 120;
						}
						dbg(DEBUG,"DATALINE AFTER PATCHING VIAL:\n<%s>", pclTmpBuf);
					}
				}

				/* write it now... */
				switch (rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iTelegramClass)
				{
					case iDATA_CLASS:
						if ((ilRC = WriteDataTelegramToClient(prlCmdblk->command, ilCmdNo, ilTabNo, pclTmpBuf, pclTmpFieldBuf, ilNoOfReceivedFields, lUniqueNumber)) != RC_SUCCESS)
							return ilRC;
						break;

					case iMIX_CLASS:
						if ((ilRC = WriteMixTelegramToClient(ilCmdNo, ilTabNo, pclTmpBuf, pclTmpFieldBuf, ilNoOfReceivedFields, lUniqueNumber)) != RC_SUCCESS)
							return ilRC;
						break;

					case iMDEL_CLASS:
					case iDDEL_CLASS:
						dbg(DEBUG,"<HandleData> THIS IS %s_CLASS...", sDDEL_CLASS);
						if ((ilRC = WriteDelTelegramToClient(rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iTelegramClass, ilCmdNo, ilTabNo, pclTmpBuf, pclTmpFieldBuf, ilNoOfReceivedFields, lUniqueNumber)) != RC_SUCCESS)
							return ilRC;
						break;

					default:
						dbg(TRACE,"<HandleData> %05d unknown telegram class", __LINE__);
						Terminate(0);
						break;
				}
			}
		}

		/* mapping to 'normal' fields ... */
		for (i=0; i<rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iNoFields; i++)
		{
			/* look for assignment! */
			for (ilAssignNo=0; 
				  ilAssignNo<rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iNoAssign;
				  ilAssignNo++)
			{
				/* is there an existing entry for ist field? */
				if (!strcmp(rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].prAssign[ilAssignNo].pcIFField, rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcFields[i]))
				{
					/* length of db-field must be equal or greater than if-field */
					/* so copy simply database field to field list */
					strcpy(rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcFields[i], rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].prAssign[ilAssignNo].pcDBField);
				}
			}
		}

		dbg(DEBUG,"<HandleData> ---- END REQUEST/ANSWER FROM: %d ----", prgEvent->originator);
		/* send last and/or write file, only for data telegrams */
		if (rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iTelegramClass == iDATA_CLASS)
		{
			if (rgTM.prCmd[ilCmdNo].iDataCounter == rgTM.prCmd[ilCmdNo].iNoTables)
			{
				/* set counter to zero */
				rgTM.prCmd[ilCmdNo].iDataCounter = 0;

				/* write file to machine... */ 
				return SendFileAndWriteEOD(ilCmdNo, rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcDataType, prlCmdblk->command);
			}
			else
				return RC_SUCCESS;
		}
		else
			return RC_SUCCESS;
	}
	else
	{
		dbg(DEBUG,"<HandleData> can't find command and/or table number...");
		return RC_FAIL;
	}
} 

/******************************************************************************/
/* The CDIServer routine                                                      */
/******************************************************************************/
static int CDIServer(void)
{
	int			ilRC;
	int			ilBytes;
	UINT			ilTimeOut;
	char			pclCommand[iCOMMAND_SIZE+1];

	/* now create the TCP/IP-Connection */
	ilRC = CreateTCPConnection();
	if (ilRC > RC_SUCCESS)
	{
		dbg(TRACE, "<CDIServer> error %d creating TCP/IP-Connection", ilRC);
		Terminate(igRestartTimeout);
	}
	else if (ilRC < RC_SUCCESS)
	{
		dbg(DEBUG, "<CDIServer> return form CDIServer (ilRC = %d)", ilRC);
		return ilRC;
	}

	/* initialize time */
	time1 = time(NULL);

	/* forever */
	while (1)
	{
		/* set timeout */
		ilTimeOut = igCCOCheck ? rgTM.iCheckTimeout : rgTM.rTCP.iTcpTimeout;

		/*----------------------------------------------------------------
		look for TCP
		----------------------------------------------------------------*/
		if ((ilBytes = ReceiveTcpIp(ilTimeOut)) > 0)
		{
			/* write data to protocol file */
			ToRProtFile(ilBytes);

			if (((ilRC = CheckReceivedHeader((THeader*)pcgReceiveData)) >= iWRONG_SERVERSYSTEM && ilRC <= iUNKNOWN_RECEIVER) || ilRC == RC_FAIL)
			{
				dbg(DEBUG,"<CDIServer> error in header: %d", ilRC);

				/* reset now, and return with an error */	
				if ((ilRC = Reset()) != RC_SUCCESS)
					HandleErr(ilRC);

				/* back */
				return ilRC;
			}

			/* only CheckReceivedHeader returns with success */
			if (ilRC == RC_SUCCESS)
			{
				/* select command */
				memset ((void*)pclCommand, 0x00, iCOMMAND_SIZE+1);
				memmove((void*)pclCommand, 
						  (void*)(pcgReceiveData+iCOMMAND_POSITION), 
						  (size_t)iCOMMAND_SIZE);

				/* convert command to uppercase */
				StringUPR((UCHAR*)pclCommand);

				/* handle received command */
				if ((ilRC = HandleReceivedCommand(pclCommand)) < 0)
				{
					dbg(DEBUG,"<CDIServer> HRC returns: %d", ilRC);

					/* reset now, and return with an error */	
					if ((ilRC = Reset()) != RC_SUCCESS)
						HandleErr(ilRC);

					/* back */
					return ilRC;
				}
			}
		}
		else if (ilBytes < 0)
		{
			/* here we must create new TCP-IP Connection, close the old */
			/* reset now, and return with an error */	
			if ((ilRC = Reset()) != RC_SUCCESS)
				HandleErr(ilRC);

			/* back */
			return ilBytes;
		}
		else
		{
			/* we haven't received data via TCP/IP */
			if (igCCOCheck)
			{
				/* if we use CCO-Check -> this must be an timeout error */
				dbg(DEBUG,"<CDIServer> timeout error");

				/* in this case send an ERR-Command to client... */
				ilRC = WriteERRToClient(sTIMEOUT);

				/* reset now, and return with an error */	
				if ((ilRC = Reset()) != RC_SUCCESS)
					HandleErr(ilRC);

				/* return from CDIServer */
				return ilRC;
			}
		}

		/*----------------------------------------------------------------
		check que-status
		----------------------------------------------------------------*/
		ilRC = CheckQueStatus(mod_id);
		if (prgItem != NULL)
		{
			free((void*)prgItem);
			prgItem = NULL;
		}
		if (ilRC < 0)
			return ilRC;

		/*----------------------------------------------------------------
		check connection-status (watchdog) CCO-Command
		----------------------------------------------------------------*/
		if (rgTM.iCheckConnect)
		{
			if ((ilRC = CheckConnection()) < 0)
			{
				dbg(DEBUG,"<CDIServer> CheckConnection return: %d", ilRC);
				/* reset now, and return with an error */	
				if ((ilRC = Reset()) != RC_SUCCESS)
					HandleErr(ilRC);

				/* return from CDIServer */
				return ilRC;
			}
		}
	}
}

/******************************************************************************/
/* The ReceiveTcpIp routine                                                   */
/* the following funktion return the number of bytes received via				   */
/* TCP/IP - and sends an acknowledge if necessary 										*/
/******************************************************************************/
static int ReceiveTcpIp(UINT ipTimeout)
{
	int					ilRC;
	int					ilCnt;
	int					ilBytes;
	int					ilFirst;
	int					ilHaveToRead;
	UINT					ilLen;
	char					pclReceiveBuf[iRECEIVE_BUF_SIZE];
	char					pclTmpBuf[iMIN_BUF_SIZE];
	TACK					*prlAck;

	if (igDbgMode)
		dbg(DEBUG,"<RTCPIP> nt                  /----- START -----");

	/* bytecounter */
	ilBytes = 0;

	/* clear memory */
	memset ((void*)pcgReceiveData, 0x00, iMAX_RECEIVE_DATA);

	/* look for data */
	if ((ilRC = TcpWaitTimeout(igClientSocket, ipTimeout, 
								rgTM.iTimeUnit)) != RC_FAIL && ilRC != RC_NOT_FOUND)
	{
		/* TCP/IP message is available */
		/* for length */
		ilFirst = 1;

		/* number of bytes we have to read */
		ilLen = ilHaveToRead = iTHEADER_LENGTH;

		/* date to receive */
		do
		{
			/* Return code */
			ilRC = 0;

			/* clear temporary buffer */
			memset((void*)pclReceiveBuf, 0x00, iRECEIVE_BUF_SIZE);

			/* read the data */
			ilCnt = 0;
			do
			{
				dbg(DEBUG,"<RTCPIP> READ SOCK %d (SIZE %d) STEP %d WITH LEN=%d", igClientSocket,iRECEIVE_BUF_SIZE,ilCnt,ilLen);

				if (igDbgMode)
					dbg(DEBUG,"<RTCPIP> ----- START read (1) -----");
				alarm(10);
				ilRC = read(igClientSocket, pclReceiveBuf, ilLen);
				alarm(0);
				if (igDbgMode)
					dbg(DEBUG,"<RTCPIP> ----- END read (1) -----");

				if (++ilCnt >= 5)
				{
					dbg(DEBUG,"<RTCPIP> read returns: %d counter: %d errno: %d -> %s", ilRC, ilCnt, errno, strerror(errno));

					/* this shows function above the error */
					return iREAD_CNT_FAIL;
				}
			}
			while (ilRC <= 0);

			/* copy form temporary to static buffer */
			memmove((void*)(pcgReceiveData+ilBytes), 
				     (void*)pclReceiveBuf, (size_t)ilRC);

			/* increment Byte-Counter */
			ilBytes += ilRC;
			
			/* could we check length? */
			if (ilBytes >= iTHEADER_LENGTH && ilFirst)
			{
				/* never first */
				ilFirst = 0;

				/* we know the position of the following number of bytes */
				/* get number of following bytes */
				memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
				memmove((void*)pclTmpBuf, (void*)(pcgReceiveData+iLENGTH_POSITION), 
								(size_t)iLENGTH_SIZE);
				sscanf(pclTmpBuf, "%d", &ilRC);

				/* set new (read) counter */
				ilHaveToRead += ilRC;

				/* set new len */
				ilLen = ilRC;

				/* check the memory */
				if (ilHaveToRead > iMAX_RECEIVE_DATA)
				{
					/* try to allocate more memory */
					if ((pcgReceiveData = (char*)realloc(pcgReceiveData, ilHaveToRead*sizeof(char))) == NULL)
					{
						/* cannot get enough memory to run -> exit now */
						dbg(TRACE,"<RTCPIP> cannot realloc enough momory!");
						Terminate(0);
					}
				}
			}
			dbg(DEBUG,"GOT SO FAR:");
			snap(pcgReceiveData,ilBytes,outp);
			dbg(DEBUG,"<------");
			dbg(DEBUG,"CHECK LENGTH: (ilBytes=%d) < (ilHaveToRead=%d) ?",ilBytes,ilHaveToRead);
		} while (ilBytes < ilHaveToRead);
		dbg(DEBUG,"MESSAGE COMPLETE");
		/* acknowledge */
		if (rgTM.iAcknowledge)
		{
			if ((prlAck = BuildACK()) == NULL)
			{
				dbg(DEBUG,"<RTCPIP> cannot build acknowledge, change to no ack");
				rgTM.iAcknowledge = 0;	
			}
			else
			{
				/* Telegramnumber must be the same we received */
				memmove((void*)prlAck->rTH.pcTNumber,
						  (const void*)(pcgReceiveData+iTNUMBER_POSITION),
						  iTNUMBER_SIZE);
				
				/* write it to send protocol file */
				ToSProtFile((char*)prlAck, iTACK_LENGTH);

				/* write ACK to client */
				if ((ilRC = write (igClientSocket, prlAck, iTACK_LENGTH)) != iTACK_LENGTH)
				{
					dbg(TRACE,"<RTCPIP> write error - cannot acknowledge %d %d", ilRC, ilBytes);

					/* this shows function above the error */
					return iWRITE_FAIL;
				}
			}
		}
	}

	if (igDbgMode)
		dbg(DEBUG,"<RTCPIP> ----- END -----");
	/* und tschuess */
	return ilBytes;
}

/******************************************************************************/
/* The CheckQueStatus routine                                                 */
/******************************************************************************/
static int CheckQueStatus(int ipModID)
{
	int			ilRC;

	/* free memory */
	if (prgItem != NULL)
	{
		free ((void*)prgItem);
		prgItem = NULL;
	}

	/* this is for debbuging */
	if (igDbgMode)
		dbg(DEBUG,"<CheckQueStatus> %05d QUE_GETBIGNW with Mod-ID: %d", __LINE__, ipModID);

	/* without waiting... */
	ilRC = que(QUE_GETBIGNW, 0, ipModID, PRIORITY_3, 0, (char*)&prgItem);

	/* set global pointer */
	prgEvent = (EVENT*)prgItem->text;

	if (ilRC == RC_SUCCESS)
	{
		/* ack. the que */
		if ((ilRC = que(QUE_ACK, 0, ipModID, 0, 0, NULL)) != RC_SUCCESS)
		{
			HandleQueErr(&ilRC);
		}

		switch (prgEvent->command)
		{
			case HSB_STANDBY:
			case HSB_COMMING_UP:
				ctrl_sta = prgEvent->command;
				if ((ilRC = Reset()) != RC_SUCCESS)
					HandleErr(ilRC);
				ilRC = -1;
				break;

			case HSB_ACTIVE:
			case HSB_STANDALONE:
				ctrl_sta = prgEvent->command;
				ilRC = 0;
				break;

			case TRACE_ON:
			case TRACE_OFF:
				dbg_handle_debug(prgEvent->command);
				ilRC = 0;
				break;

			case HSB_ACT_TO_SBY:
				ctrl_sta = prgEvent->command;
				if ((ilRC = Reset()) != RC_SUCCESS)
					HandleErr(ilRC);
				ilRC = -2;
				break;

			case SHUTDOWN:
				Terminate(0);
				break;

			case RESET:
				if ((ilRC = Reset()) != RC_SUCCESS)
					HandleErr(ilRC);
				ilRC = -3;
				break;

			case EVENT_DATA:
				if (igStatus & iF_TCPIP_CLIENT_SOCK)
				{
					if ((ilRC = HandleData()) != RC_SUCCESS)
					{
						if ((ilRC = Reset()) != RC_SUCCESS)
							HandleErr(ilRC);
						ilRC = -4;
					}
					else
					{
						ilRC = 0;
					}
				}
				else
				{
					ilRC = 0;
				}
				break;

			case 1000:
				/* specially for debugging */
				igDbgMode = !igDbgMode;
				break;

			default:
				DebugPrintItem(TRACE, prgItem);
				DebugPrintEvent(TRACE, prgEvent);
				ilRC = 0;
				break;
		}
	}
	else
	{
		/* error getting data from que */
		HandleQueErr(&ilRC);
	}

	/* free memory */
	if (prgItem != NULL)
	{
		free ((void*)prgItem);
		prgItem = NULL;
	}

	/* return successfully */
	return ilRC;
}

/******************************************************************************/
/* The CheckReceivedHeader routine                                            */
/******************************************************************************/
static int CheckReceivedHeader(THeader *prpTH)
{
	int			ilRC;
	int			ilYear;
	int			ilMonth;
	int			ilDate;
	int			ilHour;
	int			ilMinute;
	int			ilSecond;
	char			pclTmpBuf[iMIN_BUF_SIZE];
	char			pclTmpBuf1[iMIN_BUF_SIZE];
	
	ilRC = RC_SUCCESS;
	memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
	if ((ilRC = memcmp((void*)prpTH->pcServerSystem, 
				           (void*)rgTM.rServer.pcServerSystem,
				           (size_t)iSERVERSYS_SIZE)) == 0)
	{
		if ((ilRC = memcmp((void*)prpTH->pcSender,
							     (void*)rgTM.rServer.pcReceiver,
							     (size_t)iSENDER_SIZE)) == 0)
		{
			if ((ilRC = memcmp((void*)prpTH->pcReceiver,
								     (void*)rgTM.rServer.pcSender,
								     (size_t)iRECEIVER_SIZE)) == 0)
			{
				if (!memcmp((void*)prpTH->pcTClass, (void*)sCONTROL_CLASS,
								(size_t)iTCLASS_SIZE)    ||
					 !memcmp((void*)prpTH->pcTClass, (void*)sDATA_CLASS,
							   (size_t)iTCLASS_SIZE) 	 ||
					 !memcmp((void*)prpTH->pcTClass, (void*)sDDEL_CLASS,
								(size_t)iTCLASS_SIZE)    ||
					 !memcmp((void*)prpTH->pcTClass, (void*)sMDEL_CLASS,
								(size_t)iTCLASS_SIZE)) 
				{
					/* check timestamp */
					memmove((void*)pclTmpBuf, (void*)prpTH->pcTimeStamp,
							  iTIMESTAMP_SIZE);

					memset((void*)pclTmpBuf1, 0x00, iMIN_BUF_SIZE);
					memmove((void*)pclTmpBuf1, (void*)pclTmpBuf, 4);
					ilYear = atoi(pclTmpBuf1);
						
					memset((void*)pclTmpBuf1, 0x00, iMIN_BUF_SIZE);
					memmove((void*)pclTmpBuf1, (void*)&pclTmpBuf[4], 2);
					ilMonth = atoi(pclTmpBuf1);
						
					memset((void*)pclTmpBuf1, 0x00, iMIN_BUF_SIZE);
					memmove((void*)pclTmpBuf1, (void*)&pclTmpBuf[6], 2);
					ilDate = atoi(pclTmpBuf1);

					memset((void*)pclTmpBuf1, 0x00, iMIN_BUF_SIZE);
					memmove((void*)pclTmpBuf1, (void*)&pclTmpBuf[8], 2);
					ilHour = atoi(pclTmpBuf1);

					memset((void*)pclTmpBuf1, 0x00, iMIN_BUF_SIZE);
					memmove((void*)pclTmpBuf1, (void*)&pclTmpBuf[10], 2);
					ilMinute = atoi(pclTmpBuf1);

					memset((void*)pclTmpBuf1, 0x00, iMIN_BUF_SIZE);
					memmove((void*)pclTmpBuf1, (void*)&pclTmpBuf[12], 2);
					ilSecond = atoi(pclTmpBuf1);

					if (ilYear   < 1900 || ilYear   > 2100 ||
						 ilMonth  < 1    || ilMonth  > 12   ||
						 ilDate   < 1    || ilDate   > 31   ||
						 ilHour   < 0    || ilHour   > 23   ||
						 ilMinute < 0    || ilMinute > 59   ||
						 ilSecond < 0    || ilSecond > 59)
					{
						/* wrong timestamp */
						ilRC = iWRONG_TIMESTAMP;
						strcpy(pclTmpBuf, sWRONG_TIMESTAMP);
					}
					else
					{
						/* Check Length */
						memset((void*)pclTmpBuf1, 0x00, iMIN_BUF_SIZE);
						memmove((void*)pclTmpBuf1, (void*)prpTH->pcLength,
								  iLENGTH_SIZE);

						if (atoi(pclTmpBuf1) < 3)
						{
							/* wrong length */
							ilRC = iWRONG_LENGTH;
							strcpy(pclTmpBuf, sWRONG_LENGTH);
						}
						else
						{
							/* check number */
							memset((void*)pclTmpBuf1, 0x00, iMIN_BUF_SIZE);
							memmove((void*)pclTmpBuf1, (void*)prpTH->pcTNumber,
									  iTNUMBER_SIZE);

							if (atoi(pclTmpBuf1) < 0)
							{
								/* wrong telegramnumber */
								ilRC = iWRONG_NUMBER;
								strcpy(pclTmpBuf, sWRONG_NUMBER);
							}
						}
					}
				}
				else
				{
					/* wrong class */
					ilRC = iUNKNOWN_CLASS;
					strcpy(pclTmpBuf, sUNKNOWN_CLASS);
				}
			}
			else
			{
				/* wrong receiver */
				ilRC = iUNKNOWN_RECEIVER;
				strcpy(pclTmpBuf, sUNKNOWN_RECEIVER);
			}
		}
		else
		{
			/* wrong sender */
			ilRC = iUNKNOWN_SENDER;
			strcpy(pclTmpBuf, sUNKNOWN_SENDER);
		}
	}
	else
	{
		/* wrong serversystem */
		ilRC = iWRONG_SERVERSYSTEM;	
		strcpy(pclTmpBuf, sWRONG_SERVERSYSTEM);
	}
	
	/* Error in header structure occured */
	if (ilRC)
	{
		/* in this case send an ERR-Command to client... */
		if (WriteERRToClient(pclTmpBuf) == RC_FAIL)
			ilRC = RC_FAIL;
	}

	/* tschuess */
	return ilRC;
}

/******************************************************************************/
/* The HandleReceivedCommand routine                                          */
/******************************************************************************/
static int HandleReceivedCommand(char *pcpCommand)
{
  int			i;
  int			j;
  int			ilRC;
  int			ilAnz;
  int			ilPrio;
  int			ilCmdNo;
  int			ilTabNo;
  int			ilCurDL;
  int			ilFieldNo;
  int			ilSendData;
  int			ilCurElement;
  int			ilSelectModus;
  int			ilAssignNo;
  UINT			ilSqlFlags;
  long			lLen;
  char			clSeparator;
  char			pclTmpBuf[iMIN_BUF_SIZE];
  char			pclFieldBuf[iMAXIMUM];
  char			pclTmpFieldBuffer[iMAXIMUM];
  char			pclDataBuf[iMAXIMUM];
  char			pclTmpDataBuffer[iMAXIMUM];
  char			pclSqlBuf[iMAX_BUF_SIZE];
  char			pclCCABuf[iMAX_BUF_SIZE];
  char			pclSqlBuf1[iMAX_BUF_SIZE];	
  char			pclUtcTime[15];
  char			*pclS				= NULL;
  char			*pclDataPtr		= NULL;
  TCCO			*prlCCO	 		= NULL;
  TSFS			*prlSFS			= NULL;
  TSFU			*prlSFU 			= NULL;
  TERR			*prlERR 			= NULL;
  TBXX			*prlBXX			= NULL;
  TMix			*prlMix			= NULL;
  EVENT			*prlOutEvent   = NULL;
  BC_HEAD		*prlOutBCHead  = NULL;
  CMDBLK		*prlOutCmdblk	= NULL;
  TRecover		*prlRecArea		= NULL;
  TRecover		rlRec;

  /* set number for command, if possible */
  ilCmdNo = SearchCommandNumber(pcpCommand);

  /*-------------------------------------------------------------------*/
  /*-------------------------------------------------------------------*/
  /*-------------------------------------------------------------------*/
  /*--------------------- fix command CCO -----------------------------*/
  if (!strcmp(pcpCommand, "CCO"))
    {
      /* important message */
      dbg(DEBUG, "<HRC> ---------- START CCO ----------");

      /* reset Fail-Structure */
      rgFail.iSendErrorCounter = 0;
      rgFail.iReceiveErrorCounter = 0;

      /* check CCOC or CCOA */
      prlCCO = (TCCO*)pcgReceiveData;

      /* is this acknowledge or check? */
      if (!strncmp(prlCCO->pcAQ, sACKNOWLEDGE, iAQ_SIZE))
	{
	  /* this is an acknowledge */
	  if (igCCOCheck)
	    {
				/* check telegramnumer - it has to be the same we wrote */
	      memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
	      memmove((void*)pclTmpBuf, 
		      (pcgReceiveData+iTNUMBER_POSITION), iTNUMBER_SIZE);

				/* not equal */
	      if (igLastCCONo != atoi(pclTmpBuf))
		{
		  /* in this case send an ERR-Command to client... */
		  if ((ilRC = WriteERRToClient(sCCO_TNUMBER)) == RC_FAIL)
		    return iWRITE_ERRFAIL;
		}
	    }
	}
      else
	{
	  if (!strncmp(prlCCO->pcAQ, sCHECK, iAQ_SIZE))
	    {
				/* this is an check */
				/* write CCOA */
	      if ((prlCCO = BuildCCO(sACKNOWLEDGE)) == NULL)
		{
		  dbg(TRACE, "<HRC> BuildCCO returns NULL");
		  Terminate(0);
		}

				/* Telegramnumber must be the same we received */
	      memmove((void*)prlCCO->rTH.pcTNumber,
		      (const void*)(pcgReceiveData+iTNUMBER_POSITION),
		      iTNUMBER_SIZE);

				/* write it to send protocol file */
	      ToSProtFile((char*)prlCCO, iTCCO_LENGTH);

				/* write it to socket */
	      if ((ilRC = write(igClientSocket, prlCCO, iTCCO_LENGTH)) != iTCCO_LENGTH)
		{
		  dbg(TRACE,"<HRC> write error - can't answer to CCO (%d)", ilRC);
		  return iWRITE_FAIL;
		}
	    }
	  else
	    {
				/* in this case send an ERR-Command to client... */
	      if ((ilRC = WriteERRToClient(sAC_ERROR)) == RC_FAIL)
		return iWRITE_ERRFAIL;
	    }
	}
      dbg(DEBUG, "<HRC> ---------- END CCO ----------");
    }
  /*-------------------------------------------------------------------*/
  /*-------------------------------------------------------------------*/
  /*-------------------------------------------------------------------*/
  /*---------------------- fix command END ----------------------------*/
  else if (!strcmp(pcpCommand, "END"))
    {
      /* end of connection */
      dbg(DEBUG, "<HRC> ---------- START END ----------");

      /* reset Fail-Structure */
      rgFail.iSendErrorCounter = 0;
      rgFail.iReceiveErrorCounter = 0;

      dbg(DEBUG, "<HRC> ---------- START END ----------");

      /* return with an error */
      return iEND_RECEIVED;
    }
  /*-------------------------------------------------------------------*/
  /*-------------------------------------------------------------------*/
  /*-------------------------------------------------------------------*/
  /*---------------------- fix command ACK ----------------------------*/
  else if (!strcmp(pcpCommand, "ACK"))
    {
      /* end of connection */
      dbg(DEBUG, "<HRC> ---------- START ACK ----------");

      /* reset Fail-Structure */
      rgFail.iSendErrorCounter = 0;
      rgFail.iReceiveErrorCounter = 0;

      dbg(DEBUG, "<HRC> ---------- END ACK ----------");
    }
  /*-------------------------------------------------------------------*/
  /*-------------------------------------------------------------------*/
  /*-------------------------------------------------------------------*/
  /*---------------------- fix command ERR ----------------------------*/
  else if (!strcmp(pcpCommand, "ERR"))
    {
      /* end of connection */
      prlERR = (TERR*)pcgReceiveData;
      dbg(DEBUG, "<HRC> ---------- START ERR (%s) ----------", prlERR->pcErrorCode);

      if (rgTM.iMaxFailCmds > 0)
	{
	  /* set fail-structure */
	  rgFail.iReceiveErrorCounter = INCR(rgFail.iReceiveErrorCounter);

	  /* check error counter */
	  if (rgFail.iReceiveErrorCounter > rgTM.iMaxFailCmds)
	    {
	      dbg(TRACE,"<HRC> ErrorCounter: %d and MaxFailCmds: %d",
		  rgFail.iReceiveErrorCounter, rgTM.iMaxFailCmds);
	      Terminate(0);
	    }
	}
      dbg(DEBUG, "<HRC> ---------- END ERR (%s) ----------", prlERR->pcErrorCode);
    }
  /*-------------------------------------------------------------------*/
  /*-------------------------------------------------------------------*/
  /*-------------------------------------------------------------------*/
  else if (    /* B A S I S D A T E N */
				/* Flugzeugtypen */
	   !strcmp(pcpCommand, "BAC") || 
				/* Fluggesellschaften */
	   !strcmp(pcpCommand, "BAL") ||
				/* Flughaefen */
	   !strcmp(pcpCommand, "BAP") ||
				/* LFZ-Kennzeichen */
	   !strcmp(pcpCommand, "BAR") ||
				/* Gates */
	   !strcmp(pcpCommand, "BGA") ||
				/* Positionen */
	   !strcmp(pcpCommand, "BPO") ||
				/* Warteraeume */
	   !strcmp(pcpCommand, "BWA") ||
				/* Check-In-Schalter */
	   !strcmp(pcpCommand, "BCK") ||
				/* Gepaeckbaender Abflug */
	   !strcmp(pcpCommand, "BBA") ||
				/* FIDS-Bemerkungen */
	   !strcmp(pcpCommand, "BPR") ||
				/* Verkehrsarten */
	   !strcmp(pcpCommand, "BVA") ||
				/* EXTHAJ */
	   !strcmp(pcpCommand, "BEX") ||

	   /* S E N D  F L I G T H  S C H E D U L E */
	   !strcmp(pcpCommand, "SFS"))
    {
      dbg(DEBUG, "<HRC> ---------- START <%s> ----------", pcpCommand);

      /* reset Fail-Structure */
      rgFail.iSendErrorCounter = 0;
      rgFail.iReceiveErrorCounter = 0;

      /* is this command available? */
      if (ilCmdNo == RC_FAIL)
	{
	  dbg(DEBUG,"<HRC> %s is not available at this CDI-Server", pcpCommand);
	  if ((ilRC = WriteERRToClient(sWRONG_COMMAND)) == RC_FAIL)
	    return iWRITE_ERRFAIL;
	}
      else
	{
	  /* if SFU is currently valid, set it to NOT VALID!!! */
	  if ((i = SearchCommandNumber("SFU")) != RC_FAIL)
	    {
	      for (j=0; j<rgTM.prCmd[i].iNoTables; j++)
		{
		  /* delete selection for SFU in action configuiration */
		  ilRC = HandleActionSection(iDELETE_SECTION, rgTM.prCmd[i].prTabDef[j].pcSndCmd, "SFU", i, j);
		  dbg(DEBUG,"<HRC> %sdelelete Section(s) <%s> with command <%s>",	ilRC == RC_SUCCESS ? "" : "can't ", rgTM.prCmd[i].pcCommand, rgTM.prCmd[i].prTabDef[j].pcSndCmd);
		}
	    }

	  /* clear flag */
	  ilSelectModus = 0x0000;

	  /* clear sql-buffer */
	  memset((void*)pclSqlBuf, 0x00, iMAX_BUF_SIZE);

	  /* get time informations.... */
	  if (!strcmp(pcpCommand, "SFS"))
	    {
				/* is this a SFS-Command */
				/* cast to send flight schedule */
	      prlSFS = (TSFS*)pcgReceiveData;

				/* build where-condition */
	      if (strlen(prlSFS->pcStartTime)	&& strncmp(prlSFS->pcStartTime, s14_BLANKS, iSTIME_SIZE) && !strlen(prlSFS->pcEndTime))
		{
		  /* we received starttime */
		  sprintf(pclSqlBuf, "(TIFD >= '%.14s' OR TIFA >= '%.14s')", 
			  prlSFS->pcStartTime, prlSFS->pcStartTime);	
		  /* for ccahaj */
		  sprintf(pclCCABuf, "(CKBA >= '%.14s' OR CKBS >= '%.14s')", 
			  prlSFS->pcStartTime, prlSFS->pcStartTime);	
		  /* set flag */
		  ilSelectModus |= 0x0001;
		}
	      else if (!strlen(prlSFS->pcStartTime) && strlen(prlSFS->pcEndTime) && strncmp(prlSFS->pcEndTime, s14_BLANKS, iETIME_SIZE))
		{
		  /* we received endtime */
		  sprintf(pclSqlBuf, "(TIFD <= '%.14s' OR TIFA <= '%.14s')", 
			  prlSFS->pcEndTime, prlSFS->pcEndTime);
		  /* for ccahaj... */
		  sprintf(pclCCABuf, "(CKBA <= '%.14s' OR CKBS <= '%.14s')", 
			  prlSFS->pcEndTime, prlSFS->pcEndTime);
		  /* set flag */
		  ilSelectModus |= 0x0002;
		}
	      else if (strlen(prlSFS->pcStartTime) && strncmp(prlSFS->pcStartTime, s14_BLANKS, iSTIME_SIZE) && strlen(prlSFS->pcEndTime) && strncmp(prlSFS->pcEndTime, s14_BLANKS, iETIME_SIZE))
		{
		  /* we received starttime and endtime */
		  sprintf(pclSqlBuf, "((TIFD BETWEEN '%.14s' AND '%.14s') OR (TIFA BETWEEN '%.14s' AND '%.14s'))", prlSFS->pcStartTime, prlSFS->pcEndTime, prlSFS->pcStartTime, prlSFS->pcEndTime);
		  dbg(DEBUG,"<HRC> %05d SqlBuf: <%s>", __LINE__, pclSqlBuf);

		  /* this is for ccahaj... */
		  sprintf(pclCCABuf, "((CKBA BETWEEN '%.14s' AND '%.14s') OR (CKBS BETWEEN '%.14s' AND '%.14s'))", prlSFS->pcStartTime, prlSFS->pcEndTime, prlSFS->pcStartTime, prlSFS->pcEndTime);
		  dbg(DEBUG,"<HRC> %05d CCABuf: <%s>", __LINE__, pclCCABuf);

		  /* set flag */
		  ilSelectModus |= 0x0004;
		}
	    }
	  else if (!strcmp(pcpCommand, "BAC") || 
		   !strcmp(pcpCommand, "BAL") ||
		   !strcmp(pcpCommand, "BAP") ||
		   !strcmp(pcpCommand, "BAR") ||
		   !strcmp(pcpCommand, "BGA") ||
		   !strcmp(pcpCommand, "BPO") ||
		   !strcmp(pcpCommand, "BWA") ||
		   !strcmp(pcpCommand, "BCK") ||
		   !strcmp(pcpCommand, "BBA") ||
		   !strcmp(pcpCommand, "BPR") ||
		   !strcmp(pcpCommand, "BEX"))
	    {
				/* cast to Stammdaten */
	      prlBXX = (TBXX*)pcgReceiveData;

	      if (strlen(prlBXX->pcUpdateTime) && strncmp(prlBXX->pcUpdateTime, s14_BLANKS, iUPDATE_SIZE))
		{
		  /* build where condition */
		  sprintf(pclSqlBuf, "(LSTU >= '%.14s' OR CDAT >= '%.14s')", 
			  prlBXX->pcUpdateTime, prlBXX->pcUpdateTime); 
		  ilSelectModus |= 0x0001;
		}
	    }

	  dbg(DEBUG,"<HRC> %05d SqlBuf: <%s>", __LINE__, pclSqlBuf);
	  dbg(DEBUG,"<HRC> %05d CCABuf: <%s>", __LINE__, pclCCABuf);

	  /* for all tables */
	  for (ilTabNo=0; ilTabNo<rgTM.prCmd[ilCmdNo].iNoTables; ilTabNo++)
	    {
				/* first build field list */
	      memset((void*)pclFieldBuf, 0x00, iMAXIMUM);
	      for (ilFieldNo=0; 
		   ilFieldNo<rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iNoFields; 
		   ilFieldNo++)
		{
		  if (!rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iViaInFieldList)
		    {
		      /* this is without VIA in field-list */
		      strcat(pclFieldBuf, rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcFields[ilFieldNo]); 
		      if (ilFieldNo < rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iNoFields-1) 
			strcat(pclFieldBuf, ",");
		    }
		  else
		    {
		      /* here we must handle VIAL, VIAM */
		      if (strncmp(rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcFields[ilFieldNo], "VIA", 3))
			{
			  /* add only fields witch is not a VIA */
			  strcat(pclFieldBuf, rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcFields[ilFieldNo]); 

			  if (ilFieldNo < rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iNoFields-1) 
			    {
			      if (pclFieldBuf[strlen(pclFieldBuf)-1] != ',')
				strcat(pclFieldBuf, ",");
			    }
			}
		    }
		}

				/* this is necessary for VIA's */
	      if (rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iViaInFieldList)
		{
		  /* add VIAL and VIAN to field-list!! */
		  if (pclFieldBuf[strlen(pclFieldBuf)-1] != ',')
		    strcat(pclFieldBuf, ",");
		  strcat(pclFieldBuf, "VIAN");
		  strcat(pclFieldBuf, ",");
		  strcat(pclFieldBuf, "VIAL");
		}

				/* order by is currently not used... */
	      if (rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iUseOrderBy)
		{
		  dbg(DEBUG,"<HRC> OrderBy is not yet implemented!!");
		}

				/* is selection specified? */
	      if (rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iUseSelection)
		{
		  lLen = strlen(rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcSelect)+1;
		}
	      else
		{
		  /* set len */
		  lLen = 0;
		}

				/* dynamic condition, received via TCP/IP */
	      if (ilSelectModus)
		lLen += strlen(pclSqlBuf) + strlen(pclCCABuf) + 30;

				/* calculate size of memory we need */
	      lLen += sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
		strlen(pclFieldBuf) + 20; 

				/* get memory for out event */
	      if ((prlOutEvent = (EVENT*)malloc((size_t)lLen)) == NULL)
		{
		  dbg(TRACE,"<HRC> malloc failed, can't allocate %ld bytes", lLen);
		  prlOutEvent = NULL;
		  Terminate(0);
		}

				/* clear buffer */
	      memset((void*)prlOutEvent, 0x00, lLen);

				/* set structure members */
	      prlOutEvent->type 		 = SYS_EVENT;
	      prlOutEvent->command 	 = EVENT_DATA;
	      prlOutEvent->originator  = mod_id;
	      prlOutEvent->retry_count = 0;
	      prlOutEvent->data_offset = sizeof(EVENT);
	      prlOutEvent->data_length = lLen - sizeof(EVENT);

				/* BCHead members */
	      prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
	      prlOutBCHead->rc = RC_SUCCESS;
	      strcpy(prlOutBCHead->dest_name, pcgCDIServerName);
	      strcpy(prlOutBCHead->recv_name, "EXCO");

				/* CMDBLK members */
	      prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
	      strcpy(prlOutCmdblk->command, 
		     rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcSndCmd);
	      strcpy(prlOutCmdblk->obj_name, 
		     rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcTabName);
	      sprintf(prlOutCmdblk->tw_start, "%s,%d,%d", pcpCommand, ilCmdNo, ilTabNo);
	      sprintf(prlOutCmdblk->tw_end, "%s,%s,%s", rgTM.prCmd[ilCmdNo].pcHomeAirport, rgTM.prCmd[ilCmdNo].pcTableExtension, pcgCDIServerName);

				/* Selection */
	      if (rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iUseSelection &&  
		  !ilSelectModus)
		{
		  /* only selection from config file */
		  strcpy(prlOutCmdblk->data, 
			 rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcSelect);

		  /* fields... */
		  strcpy(prlOutCmdblk->data+strlen(rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcSelect)+1, pclFieldBuf);
		}
	      else if (!rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iUseSelection &&  
		       ilSelectModus)
		{
		  /* only select from tcp-ip ... */
		  memset((void*)pclSqlBuf1, 0x00, iMAX_BUF_SIZE);
		  memset((void*)pcgTmpTabName, 0x00, iMIN);
		  sprintf(pcgTmpTabName, "CCA%s", rgTM.prCmd[ilCmdNo].pcTableExtension);
		  if (!strcmp(rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcTabName, pcgTmpTabName))
		    {
		      /* ... if we shoud use it */ 
		      if (rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iUseReceivedSelection)
			sprintf(pclSqlBuf1, "WHERE %s", pclCCABuf);
		    }
		  else
		    {
		      /* ... if we shoud use it */ 
		      if (rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iUseReceivedSelection)
			sprintf(pclSqlBuf1, "WHERE %s", pclSqlBuf);
		    }

		  /* to out event memory */
		  strcpy(prlOutCmdblk->data, pclSqlBuf1);

		  /* fields... */
		  strcpy(prlOutCmdblk->data+strlen(pclSqlBuf1)+1, pclFieldBuf);
		}
	      else if (rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iUseSelection &&  
		       ilSelectModus)
		{
		  /* combine selection from config file and tcp-ip */
		  memset((void*)pclSqlBuf1, 0x00, iMAX_BUF_SIZE);

		  /* ... if we shoud use received selection */ 
		  if (rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iUseReceivedSelection)
		    {
		      memset((void*)pcgTmpTabName, 0x00, iMIN);
		      sprintf(pcgTmpTabName, "CCA%s", rgTM.prCmd[ilCmdNo].pcTableExtension);
		      if (!strcmp(rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcTabName, pcgTmpTabName))
			{
			  sprintf(pclSqlBuf1, "%s AND %s", rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcSelect, pclCCABuf);
			}
		      else
			{
			  sprintf(pclSqlBuf1, "%s AND %s", rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcSelect, pclSqlBuf);
			}
		    }
		  else
		    {
		      sprintf(pclSqlBuf1, "%s", rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcSelect);
		    }

		  /* to out event memory */
		  strcpy(prlOutCmdblk->data, pclSqlBuf1);

		  /* fields... */
		  strcpy(prlOutCmdblk->data+strlen(pclSqlBuf1)+1, pclFieldBuf);
		}
	      else
		{
		  /* fields without selection */
		  strcpy(prlOutCmdblk->data+1, pclFieldBuf);
		}

				/* print the select-statement */
	      dbg(DEBUG,"<HRC> Selection: <%s>", pclSqlBuf1);

				/* send this message */
				/* mod_id we use depends on cfg-entry */
	      if (rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iModID > 0)
		{
		  dbg(DEBUG,"<HRC> sending to route: %d", rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iModID);
		  if ((ilRC = que(QUE_PUT, rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iModID, mod_id, PRIORITY_4, lLen, (char*)prlOutEvent)) != RC_SUCCESS)
		    {
		      dbg(TRACE,"<HRC> QUE_PUT returns: %d", ilRC);
		      Terminate(0);
		    }
		}
	      else
		{
		  dbg(DEBUG,"<HRC> sending to route: %d", igRouterID);
		  if ((ilRC = que(QUE_PUT, igRouterID, mod_id, PRIORITY_4, lLen, (char*)prlOutEvent)) != RC_SUCCESS)
		    {
		      dbg(TRACE,"<HRC> QUE_PUT returns: %d", ilRC);
		      Terminate(0);
		    }
		}

				/* delete memory we doesn't need */
	      if (prlOutEvent != NULL)
		free((void*)prlOutEvent);
	    }
	}
      dbg(DEBUG, "<HRC> ---------- END <%s> ----------", pcpCommand);
    }
  /*-------------------------------------------------------------------*/
  /*-------------------------------------------------------------------*/
  /*-------------------------------------------------------------------*/
  /*--------------- S E N D  F L I G T H  U P D A T E -----------------*/
  else if (!strcmp(pcpCommand, "SFU"))
    {
      dbg(DEBUG, "<HRC> ---------- START <SFU> ----------", pcpCommand);

      /* reset Fail-Structure */
      rgFail.iSendErrorCounter = 0;
      rgFail.iReceiveErrorCounter = 0;

      /* cast to flight update */
      prlSFU = (TSFU*)pcgReceiveData;

      /* get section number for command SFU */
      /* is this command available? */
      if (ilCmdNo == RC_FAIL)
	{
	  dbg(DEBUG,"<HRC> %s is not available at this CDI-Server", pcpCommand);
	  if ((ilRC = WriteERRToClient(sWRONG_COMMAND)) == RC_FAIL)
	    return iWRITE_ERRFAIL;
	}
      else
	{
	  /* if SFU is not currently valid */
	  if ((i = SearchCommandNumber("SFU")) != RC_FAIL)
	    {
	      for (j=0; j<rgTM.prCmd[i].iNoTables; j++)
		{
		  /* insert selection for SFU in action configuiration */
		  ilRC = HandleActionSection(iINSERT_SECTION, rgTM.prCmd[i].prTabDef[j].pcSndCmd, "SFU", i, j);
		  dbg(DEBUG,"<HRC> %sinsert Section(s) <%s> with command <%s>",	ilRC == RC_SUCCESS ? "" : "can't ", rgTM.prCmd[i].pcCommand, rgTM.prCmd[i].prTabDef[j].pcSndCmd);
		}
	    }
			
	  /* check sub-command and parameter of SFU */
	  if (!strncmp(prlSFU->pcSubCommand, "TIMESTAMP", iSUBCOMMAND_SIZE) && strlen(prlSFU->pcUpdate) && strncmp(prlSFU->pcUpdate, s14_BLANKS, iUPDATE_SIZE))
	    {
				/* sub-command is timestamp and timestamp is available */
	      dbg(DEBUG,"<HRC> SFU->SubCommand: <%s>", prlSFU->pcSubCommand);
	      dbg(DEBUG,"<HRC> SFU->Update: <%s>", prlSFU->pcUpdate);

				/* now copy data to structure */
	      strncpy(rlRec.pcTimeStamp, prlSFU->pcUpdate, iUPDATE_SIZE);

				/* read everything from recovery (at this timestamp) */
	      if ((ilRC = (*rgTM.rRec.RHandler[rgTM.rRec.iRecoverType])(iGET_ALL_AT_TIMESTAMP, (UINT*)&ilAnz, &rlRec, &prlRecArea)) != RC_SUCCESS)
		{
		  dbg(DEBUG,"<HRC> RecoverHandler with function %d returns: %d",
		      iGET_ALL_AT_TIMESTAMP, ilRC);
		}
	      else
		{
		  dbg(DEBUG,"<HRC> RHandler returns Anz: %d", ilAnz);
		  /* send this to client */
		  for (i=0; i<ilAnz; i++)
		    {
		      /* write it to send protocol file */
		      ToSProtFile(prlRecArea[i].pcData, 
				  (UINT)prlRecArea[i].lLength); 

		      /* now write to client socket */
		      if ((ilRC = write(igClientSocket, prlRecArea[i].pcData, (int)prlRecArea[i].lLength)) != (int)prlRecArea[i].lLength)
			{
			  dbg(TRACE,"<HRC> write error - can't write data to client %d", ilRC);

			  /* free memory we doesn't need */
			  if (prlRecArea != NULL)
			    {
			      free((void*)prlRecArea);
			      prlRecArea = NULL;
			    }
			  return iWRITE_FAIL;
			}
		    }

		  /* only a debug message */
		  dbg(DEBUG,"<HRC> ready with sending recover-data");

		  /* free memory we doesn't need */
		  if (prlRecArea != NULL)
		    {
		      free((void*)prlRecArea);
		      prlRecArea = NULL;
		    }
		}
	    }
	  else
	    {
	      if (!strncmp(prlSFU->pcSubCommand, "SER_NUMBER", iSUBCOMMAND_SIZE) && strlen(prlSFU->pcUpdate) && strncmp(prlSFU->pcUpdate, s14_BLANKS, iUPDATE_SIZE))
		{
		  /* sub-command is timestamp and timestamp is available */
		  dbg(DEBUG,"<HRC> SFU->SubCommand: <%s>", prlSFU->pcSubCommand);
		  dbg(DEBUG,"<HRC> SFU->Update: <%s>", prlSFU->pcUpdate);

		  /* now copy data to structure */
		  strncpy(rlRec.pcTeleNo, prlSFU->pcUpdate, iTNUMBER_SIZE);

		  /* read everything from recovery (at this timestamp) */
		  if ((ilRC = (*rgTM.rRec.RHandler[rgTM.rRec.iRecoverType])(iGET_ALL_AT_TELENO, (UINT*)&ilAnz, &rlRec, &prlRecArea)) != RC_SUCCESS)
		    {
		      dbg(DEBUG,"<HRC> RecoverHandler with function %d returns: %d",
			  iGET_ALL_AT_TELENO, ilRC);
		    }
		  else
		    {
		      dbg(DEBUG,"<HRC> RHandler returns Anz: %d", ilAnz);
		      /* send this to client */
		      for (i=0; i<ilAnz; i++)
			{
			  /* write it to send protocol file */
			  ToSProtFile(prlRecArea[i].pcData, 
				      (UINT)prlRecArea[i].lLength); 

			  /* now write to client socket */
			  if ((ilRC = write(igClientSocket, prlRecArea[i].pcData, (int)prlRecArea[i].lLength)) != (int)prlRecArea[i].lLength)
			    {
			      dbg(TRACE,"<HRC> write error - can't write data to client %d", ilRC);

			      /* free memory we doesn't need */
			      if (prlRecArea != NULL)
				{
				  free((void*)prlRecArea);
				  prlRecArea = NULL;
				}
			      return iWRITE_FAIL;
			    }
			}

		      /* only a debug message */
		      dbg(DEBUG,"<HRC> ready with sending recover-data");

		      /* free memory we doesn't need */
		      if (prlRecArea != NULL)
			{
			  free((void*)prlRecArea);
			  prlRecArea = NULL;
			}
		    }
		}
	      else
		{
		  /* this is an unknown sub-command */
		  dbg(DEBUG,"<HRC> unknown SubCommand: <%s> or Update: <%s>", 
		      prlSFU->pcSubCommand, prlSFU->pcUpdate);

		  /* write an error to client socket... */
		  if ((ilRC = WriteERRToClient(sWRONG_SUBCOMMAND)) == RC_FAIL)
		    return iWRITE_ERRFAIL;
		}
	    }
	}
      dbg(DEBUG, "<HRC> ---------- END <SFU> ----------", pcpCommand);
    }
  /*-------------------------------------------------------------------*/
  /*-------------------------------------------------------------------*/
  /*-------------------------------------------------------------------*/
  /*------ S O M E T H I N G  W I T H  M I X-T E L E G R A M ----------*/
  else if ( /* G E P A E C K S Y S T E M */
	   !strcmp(pcpCommand, "GOP") || 
	   !strcmp(pcpCommand, "GCC") ||
	   !strcmp(pcpCommand, "GLM") ||

	   /* A N D O C K S Y S T E M */
	   !strcmp(pcpCommand, "AON") || 
	   !strcmp(pcpCommand, "AOF") ||
	   !strcmp(pcpCommand, "AOO") ||
	   !strcmp(pcpCommand, "ABA") ||
	   !strcmp(pcpCommand, "AEM") ||
				
	   /* X A I R C O N */
	   !strcmp(pcpCommand, "XFD"))
    {
      /* in this case we handle all (received!) MIX-Telegram types!!! */
      dbg(DEBUG, "<HRC> ---------- START <%s> ----------", pcpCommand);

      /* here we can handle exactly ONE table, so set the internal variable
	 ilTabNo to zero!!! */
      ilTabNo = 0;

      /* reset Fail-Structure */
      rgFail.iSendErrorCounter = 0;
      rgFail.iReceiveErrorCounter = 0;

      /* set flag for sending data */
      ilSendData = 1;

      /* convert received data to mix-structure!! */
      prlMix = (TMix*)pcgReceiveData;

      /* check telegramnumber, this is neccessary for all Gxx-Commands */
      if (!strcmp(pcpCommand, "GOP") || 
	  !strcmp(pcpCommand, "GCC") ||
	  !strcmp(pcpCommand, "GLM"))
	{	
	  /* first of all, save and check last telegram number... */
	  if (lgLastTeleNumber != iINIT_TNUMBER)
	    {
				/* checking while connection */
				/* current telenumber must be previous + 1 */	
	      if (atoi(prlMix->rTH.pcTNumber) != lgLastTeleNumber + 1)
		{
		  /* set send flag to zero */
		  ilSendData = 0;

		  dbg(DEBUG,"<HRC> %05d wrong telegramnumber", __LINE__); 

		  /* write error message to client */
		  if ((ilRC = WriteERRToClient(sWRONG_NUMBER)) == RC_FAIL)
		    return RC_FAIL;

		  if (atoi(prlMix->rTH.pcTNumber) < lgLastTeleNumber + 1)
		    {
		      /* current < previous */
		      /* ignore this, because we received this telegram */
		      dbg(DEBUG,"<HRC> Telenumber current: %d < previous: %d", atoi(prlMix->rTH.pcTNumber), lgLastTeleNumber);
		    }
		  else
		    {
		      /* current > previous */
		      dbg(DEBUG,"<HRC> Telenumber current: %d > previous: %d", atoi(prlMix->rTH.pcTNumber), lgLastTeleNumber);

		      /* send GRS (RESEND DATA) to client with the last valid telenumber */
		      if ((ilRC = WriteGRSToClient(lgLastTeleNumber+1)) == RC_FAIL)
			return iWRITE_GRSFAIL;
		    }
		}
	      else
		{
		  /* this is a valid telenumber, because it's the next */
		  dbg(DEBUG,"<HRC> %05d setting lgLastTeleNumber to %d", __LINE__, atoi(prlMix->rTH.pcTNumber));
		  lgLastTeleNumber = atoi(prlMix->rTH.pcTNumber);
		}
	    }
	  else
	    {
				/* INITIALIZE, the first... */
	      dbg(DEBUG,"<HRC> %05d setting lgLastTeleNumber to %d", __LINE__, atoi(prlMix->rTH.pcTNumber));
	      lgLastTeleNumber = atoi(prlMix->rTH.pcTNumber);
	    }
	}

      /* here we know field-separator, the received field names and 
	 all data for this fields. We must write data to database, for this
	 we use SQLHDL
      */

      /* first get separator */
      clSeparator = prlMix->pcSeparator[0];
      dbg(DEBUG,"<HRC> found Separator: <%c>", clSeparator);

      /* data format must!! be fieldname#data#fieldname#data#...	 	*/
      /* (if it is an dynamic type) or										  	*/
      /* data format must be fieldname#data   #fieldname#data  #... 	*/
      /* (if it is an fix position type)   								  	*/

      /* set pointer to data area */
      pclDataPtr = prlMix->pcData;

      /* check last character, must!! be clSeparator */
      if (pclDataPtr[strlen(pclDataPtr)-1] != clSeparator)
	{
	  /* this is an error */
	  dbg(DEBUG,"<HRC> end separator missing");

	  /* write error message to client */
	  if ((ilRC = WriteERRToClient(sSEPARATOR_MISSING)) == RC_FAIL)
	    return iWRITE_ERRFAIL;
	}
      else
	{
	  /* count elements (fields and data) 
	     first delete last separator, because GetNoOfElements returns
	     number of separators + 1 !!! 
	  */
	  pclDataPtr[strlen(pclDataPtr)-1] = 0x00;
	  if ((ilFieldNo = GetNoOfElements(pclDataPtr, (char)clSeparator)) < 0)
	    {
	      dbg(TRACE,"<HRC> GetNoOfElements returns: %d", ilFieldNo);
	      Terminate(0);
	    }

	  /* only a message */
	  dbg(DEBUG,"<HRC> found %d elements in data part", ilFieldNo);

	  /* Command "ABA", there is no data area... */
	  if (!strcmp(pcpCommand, "ABA"))
	    if (ilFieldNo == 1)
	      ilFieldNo = 0;

	  /* check number of fields, this is neccessary for all Gxx-Commands */
	  if (!strcmp(pcpCommand, "GOP") || 
	      !strcmp(pcpCommand, "GCC") ||
	      !strcmp(pcpCommand, "GLM"))
	    {	
	      if (rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iNoFields != (ilFieldNo / 2))
		{
		  /* we cannot send data if we are here... */
		  ilSendData = 0;

		  /* onyl a debug message */
		  dbg(DEBUG,"<HRC> number of received fields is not equal to number of defined fields...");

		  /* write error message to client */
		  if ((ilRC = WriteERRToClient(sWRONG_NUMBER_OF_FIELDS)) == RC_FAIL)
		    return iWRITE_ERRFAIL;
		}
	    }

	  /* element number must be even */
	  if (!(ilFieldNo % 2))
	    {
				/* this is even */
				/* clear memory */
	      memset((void*)pclSqlBuf, 0x00, iMAX_BUF_SIZE);
	      memset((void*)pclFieldBuf, 0x00, iMAXIMUM);
	      memset((void*)pclDataBuf, 0x00, iMAXIMUM);
	      ilSqlFlags = 0x0000;

				/* for all elements */
	      for (ilCurElement=0;
		   ilCurElement<ilFieldNo && ilSendData; 
		   ilCurElement++)
		{
		  /* get n.th element */
		  if ((pclS = GetDataField(pclDataPtr, (UINT)ilCurElement, (char)clSeparator)) == NULL)
		    {
		      dbg(TRACE,"<HRC> GetDataField returns NULL");
		      Terminate(0);
		    }

		  /* convert to list types we can handle */
		  if (!(ilCurElement % 2))
		    {
		      /* build field list */
		      /* first to temp-buffer */
		      strcpy(pclTmpFieldBuffer, pclS);
		    }
		  else
		    {
		      dbg(DEBUG,"<HRC> looking for field <%s>", pclTmpFieldBuffer);
		      for (i=0, ilCurDL=-1; i<rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iNoFields && ilCurDL<0; i++)
			{
			  /* is this field defined in configuration-file? */
			  if (!strcmp(pclTmpFieldBuffer, rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcFields[i]))
			    {
			      /* yes, found this field in list... */
			      dbg(DEBUG,"<HRC> found field at pos: %d", i); 
			      ilCurDL = i;
			    }
			}		

		      /* field is in list, use it */
		      if (ilCurDL >= 0)
			{
			  /* if data length is defined, check it */
			  if (rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iDataLengthCounter)
			    {
			      dbg(DEBUG,"<HRC> data_length is : %d", rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].piDataLength[ilCurDL]); 

			      /* check length now... */
			      if (strlen(pclS) > rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].piDataLength[ilCurDL])
				{
				  /* set error flag... */
				  ilSendData = 0;

				  /* only a message */
				  dbg(DEBUG,"<HRC> %05d received data <%s> greater than data_length in CFG-File <%d>...", __LINE__, pclS, rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].piDataLength[ilCurDL]);

				  /* write error message to client */
				  if ((ilRC = WriteERRToClient(sWRONG_DATA)) == RC_FAIL)
				    return RC_FAIL;
				}
			    }

			  /* check if there is data for this field... */
			  strcpy(pclTmpDataBuffer, pclS);

			  /* delete left and right blanks */
			  /* but not for baggagesystem... */
			  dbg(DEBUG,"<HRC> first command character is: <%c>", pcpCommand[0]);
			  if (pcpCommand[0] != 'G')
			    {
			      dbg(DEBUG,"<HRC> calling str_trm_all..");
			      str_trm_all(pclTmpDataBuffer, " ", TRUE);

			      if (pcpCommand[0] == 'A')
				{
				  /* specially for Andocksystem */
				  if (!strcmp(pclTmpFieldBuffer, "TIME"))
				    {
				      dbg(DEBUG,"<HRC> Command: <%s> - LocalToUtc-Field: <%s>", pcpCommand, pclTmpFieldBuffer);
				      if (strlen(pclTmpDataBuffer) == 14)
					{
					  strcpy(pclUtcTime, s14_BLANKS);
					  dbg(DEBUG,"<HRC> Utc: <%s>, Local: <%s>", pclUtcTime, pclTmpDataBuffer);

					  /* start fix 
					  strcpy(pclUtcTime, pclTmpDataBuffer);
					  AddSecondsToCEDATime(pclUtcTime,-7200,1);
					  strcpy(pclTmpDataBuffer, pclUtcTime);
					   end of fix */

					  /* old stuff start*/
					  if ((ilRC = LocalToUtc( pclTmpDataBuffer,pclUtcTime)) != RC_SUCCESS)
					    {
					      dbg(DEBUG,"<HRC> LocalToUtc returns: %d", ilRC);
					    }
					  else
					    {
					      strcpy(pclTmpDataBuffer, pclUtcTime);
					    }
					  /* old stuff end*/
					  dbg(DEBUG,"<HRC> after LocalToUtc: <%s>", pclTmpDataBuffer);
					}
				    }
				}
			    }
			  else
			    {
			      dbg(DEBUG,"<HRC> don't call str_trm_all..");
			      if (!strlen(pclTmpDataBuffer))
				strcpy(pclTmpDataBuffer, " ");	

			      if (!strcmp(pclTmpFieldBuffer, "STOD") || 
				  !strcmp(pclTmpFieldBuffer, "ETOD") || 
				  !strcmp(pclTmpFieldBuffer, "BALS") || 
				  !strcmp(pclTmpFieldBuffer, "BALG") || 
				  !strcmp(pclTmpFieldBuffer, "TIEM") || 
				  !strcmp(pclTmpFieldBuffer, "TISC") || 
				  !strcmp(pclTmpFieldBuffer, "TIAU") || 
				  !strcmp(pclTmpFieldBuffer, "TIBE"))
				{
				  dbg(DEBUG,"<HRC> Command: <%s> LocalToUtc-Field: <%s>", pcpCommand, pclTmpFieldBuffer);
				  if (strlen(pclTmpDataBuffer) == 14)
				    {
				      strcpy(pclUtcTime, s14_BLANKS);
				      dbg(DEBUG,"<HRC> Utc: <%s>, Local: <%s>", pclUtcTime, pclTmpDataBuffer);
					  /* start fix 
					  strcpy(pclUtcTime, pclTmpDataBuffer);
					  AddSecondsToCEDATime(pclUtcTime,-7200,1);
					  strcpy(pclTmpDataBuffer, pclUtcTime);
					   end of fix */

				      /* start old stuff*/
				      if ((ilRC = LocalToUtc( pclTmpDataBuffer,pclUtcTime)) != RC_SUCCESS)
					{
					  dbg(DEBUG,"<HRC> LocalToUtc returns: %d", ilRC);
					}
				      else
					{
					  strcpy(pclTmpDataBuffer, pclUtcTime);
					}
				      /* end of old stuff */
				      dbg(DEBUG,"<HRC> after LocalToUtc: <%s>", pclTmpDataBuffer);
				    }
				}
			    }
			  dbg(DEBUG,"<HRC> pclTmpDataBuffer: <%s>", pclTmpDataBuffer);

			  /* build selection, for XFD it depends on the fields */	
			  if (!strcmp(pcpCommand, "XFD"))
			    {
			      if (strlen(pclTmpDataBuffer))
				{
				  if (!strcmp(pclTmpFieldBuffer, "FLNO"))
				    {
				      ilSqlFlags |= 0x0001;
				    }
				  else if (!strcmp(pclTmpFieldBuffer, "ETOA"))
				    {
				      ilSqlFlags |= 0x0002;
				    }
				  else if (!strcmp(pclTmpFieldBuffer, "RACO"))
				    {
				      ilSqlFlags |= 0x0004;
				    }
				}
			    }

			  /* add fieldname and data to local buffers */
			  if (strlen(pclTmpDataBuffer))
			    {
			      /* should we map this fieldname??? */
			      for (ilAssignNo=0; ilAssignNo<rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iNoAssign; ilAssignNo++)
				{
				  /* compare fieldnames */
				  if (!strcmp(rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].prAssign[ilAssignNo].pcDBField, pclTmpFieldBuffer))
				    {
				      /* found field */
				      strcpy(pclTmpFieldBuffer, rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].prAssign[ilAssignNo].pcIFField);
				    }
				}

			      /* we found data, use this field... */
			      strcat(pclFieldBuf, pclTmpFieldBuffer);
			      if (ilCurElement < ilFieldNo-2)
				strcat(pclFieldBuf, ",");

			      /* ... and this data */
			      strcat(pclDataBuf, pclTmpDataBuffer);
			      if (ilCurElement < ilFieldNo-1)
				strcat(pclDataBuf, ",");
			    }
			}
		      else
			{
			  /* don't know this field... */
			  dbg(DEBUG,"<HRC> cannot find field in CFG-File: <%s>", pclTmpFieldBuffer);
			}
		    }	
		}
				
				/* check last character... */
	      if (strlen(pclFieldBuf) > 0 && strlen(pclDataBuf) > 0)
		{
		  while (pclFieldBuf[strlen(pclFieldBuf)-1] == ',' && 
			 pclDataBuf[strlen(pclDataBuf)-1] == ',')
		    {
		      pclFieldBuf[strlen(pclFieldBuf)-1] = 0x00;
		      pclDataBuf[strlen(pclDataBuf)-1] = 0x00;
		    }
		}

				/* set selection for XFD... */
	      if (!strcmp(pcpCommand, "XFD"))
		{
		  /* this is for flighthandler... */
		  strcat(pclSqlBuf, pcpCommand);

		  if ((ilSqlFlags & 0x0001) || (ilSqlFlags & 0x0002))
		    {
		      /* here we received flno or etoa */
		      strcat(pclSqlBuf, ",UFR");
		    }
		  else
		    {
		      strcat(pclSqlBuf, ",???");
		    }
		  if (ilSqlFlags & 0x0004)
		    {
		      /* this is first radar contact... */
		      strcat(pclSqlBuf, ",FRC");
		    }

		  /* flight needs it */
		  strcat(pclSqlBuf, ",TIST,");
		  strncat(pclSqlBuf, prlMix->rTH.pcTimeStamp, 14);
		}

				/* got all lists */
	      dbg(DEBUG,"<HRC> Selection: <%s>", pclSqlBuf);
	      dbg(DEBUG,"<HRC> FieldList: <%s>", pclFieldBuf);
	      dbg(DEBUG,"<HRC> DataList: <%s>", pclDataBuf);

				/* we received command ABA (Andocksystem), here it's not necessary
				   to compute length of Field- and Databuffer 
				*/
	      if (strcmp(pcpCommand, "ABA"))
		if (!strlen(pclFieldBuf) || !strlen(pclDataBuf))
		  ilSendData = 0;

				/* check errors */
	      if (ilSendData)
		{
		  /* calculate size of memory we need */
		  lLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
		    strlen(pclSqlBuf) + strlen(pclFieldBuf) + 
		    strlen(pclDataBuf) + 20; 

		  /* get memory for out event */
		  if ((prlOutEvent = (EVENT*)malloc((size_t)lLen)) == NULL)
		    {
		      dbg(TRACE,"<HRC> malloc failed, can't allocate %ld bytes", lLen);
		      prlOutEvent = NULL;
		      Terminate(0);
		    }

		  /* clear buffer */
		  memset((void*)prlOutEvent, 0x00, lLen);

		  /* set structure members */
		  prlOutEvent->type 		 = SYS_EVENT;
		  prlOutEvent->command 	 = EVENT_DATA;
		  prlOutEvent->originator  = mod_id;
		  prlOutEvent->retry_count = 0;
		  prlOutEvent->data_offset = sizeof(EVENT);
		  prlOutEvent->data_length = lLen - sizeof(EVENT);

		  /* BCHead members */
		  prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
		  prlOutBCHead->rc = NETOUT_NO_ACK;

		  strcpy(prlOutBCHead->dest_name, pcgCDIServerName);
		  strcpy(prlOutBCHead->recv_name, "EXCO");

		  /* CMDBLK members */
		  prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
		  strcpy(prlOutCmdblk->command, 
			 rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcSndCmd);
		  strcpy(prlOutCmdblk->obj_name, 
			 rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].pcTabName);
		  sprintf(prlOutCmdblk->tw_start, "%s,%d,%d", pcpCommand, ilCmdNo, ilTabNo);
		  sprintf(prlOutCmdblk->tw_end, "%s,%s,%s", rgTM.prCmd[ilCmdNo].pcHomeAirport, rgTM.prCmd[ilCmdNo].pcTableExtension, pcgCDIServerName);

		  dbg(DEBUG,"<HRC> sending command: <%s>", prlOutCmdblk->command);
		  dbg(DEBUG,"<HRC> table name: <%s>", prlOutCmdblk->obj_name);

		  /* Selection */
		  strcpy(prlOutCmdblk->data, pclSqlBuf);

		  /* fields */
		  strcpy(prlOutCmdblk->data+strlen(pclSqlBuf)+1, pclFieldBuf);

		  /* data */
		  strcpy(prlOutCmdblk->data+strlen(pclSqlBuf)+strlen(pclFieldBuf)+2, pclDataBuf);

		  /* only for byggage system, set priority to 2 */
		  if (!strcmp(pcpCommand, "GOP") || !strcmp(pcpCommand, "GCC") ||
		      !strcmp(pcpCommand, "GLM"))
		    {
		      dbg(DEBUG,"<HRC> PRIO IS 2");
		      ilPrio = PRIORITY_2;
		    }
		  else
		    {
		      dbg(DEBUG,"<HRC> PRIO IS 4");
		      ilPrio = PRIORITY_4;
		    }

		  /* send this message */
		  if (rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iModID > 0)
		    {
		      dbg(DEBUG,"<HRC> sending to route: %d", rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iModID);
		      if ((ilRC = que(QUE_PUT, rgTM.prCmd[ilCmdNo].prTabDef[ilTabNo].iModID, mod_id, ilPrio, lLen, (char*)prlOutEvent)) != RC_SUCCESS)
			{
			  dbg(TRACE,"<HRC> QUE_PUT returns: %d", ilRC);
			  Terminate(0);
			}
		    }
		  else
		    {
		      dbg(DEBUG,"<HRC> sending to route: %d", igRouterID);
		      if ((ilRC = que(QUE_PUT, igRouterID, mod_id, ilPrio, lLen, (char*)prlOutEvent)) != RC_SUCCESS)
			{
			  dbg(TRACE,"<HRC> QUE_PUT returns: %d", ilRC);
			  Terminate(0);
			}
		    }

		  /* delete memory we doesn't need */
		  if (prlOutEvent != NULL)
		    free((void*)prlOutEvent);
		}
	      else
		{
		  dbg(DEBUG,"<HRC> %05d cannot send data ilSendData: %d, pclFieldBuf: <%s>, pclDataBuf: <%s>...", __LINE__, ilSendData, pclFieldBuf, pclDataBuf);
		}
	    }
	  else
	    {
				/* this is odd */
				/* error to client */
	      dbg(DEBUG,"<HRC> wrong data (missing fieldname or data element!)");

				/* write error message to client */
	      if ((ilRC = WriteERRToClient(sWRONG_DATA)) == RC_FAIL)
		return RC_FAIL;
	    }
	}
      dbg(DEBUG, "<HRC> ---------- END <%s> ----------", pcpCommand);
    }
  /*-------------------------------------------------------------------*/
  /*-------------------------------------------------------------------*/
  /*-------------------------------------------------------------------*/
  /*------ S O M E T H I N G  W I T H  M I X-T E L E G R A M ----------*/
  else if ( /* G E P A E C K S Y S T E M */
	   !strcmp(pcpCommand, "GRS"))
    {
      /* resend data */
      dbg(DEBUG, "<HRC> ---------- START <%s> ----------", pcpCommand);

      /* reset Fail-Structure */
      rgFail.iSendErrorCounter = 0;
      rgFail.iReceiveErrorCounter = 0;

      /* convert received data to mix-structure!! */
      prlMix = (TMix*)pcgReceiveData;

      /* first get separator */
      clSeparator = (char)prlMix->pcSeparator[0];

      /* set pointer to data area */
      pclDataPtr = prlMix->pcData;

      /* check last character, must!! be clSeparator */
      if (pclDataPtr[strlen(pclDataPtr)-1] != clSeparator)
	{
	  /* this is an error */
	  dbg(DEBUG,"<HRC> separator missing");

	  /* write error message to client */
	  if ((ilRC = WriteERRToClient(sSEPARATOR_MISSING)) == RC_FAIL)
	    return iWRITE_ERRFAIL;
	}
      else
	{
	  /* count elements (fields and data) 
	     first delete last separator, because GetNoOfElements returns
	     number of separators + 1!!! 
	  */
	  pclDataPtr[strlen(pclDataPtr)-1] = 0x00;
	  if ((ilFieldNo = GetNoOfElements(pclDataPtr, (char)clSeparator)) < 0)
	    {
	      dbg(TRACE,"<HRC> GetNoOfElements returns: %d", ilFieldNo);
	      Terminate(0);
	    }

	  /* only a message */
	  dbg(DEBUG,"<HRC> found %d elements in data part", ilFieldNo);

	  /* element number must be two */
	  if (ilFieldNo == 2)
	    {
				/* this is even */
				/* get first element (fieldname) */
	      if ((pclS = GetDataField(pclDataPtr, 0, (char)clSeparator)) == NULL)
		{
		  dbg(TRACE,"<HRC> GetDataField returns NULL");
		  Terminate(0);
		}

				/* convert to uppercase */
	      StringUPR((UCHAR*)pclS);

				/* fieldname must be 'TENO' */
	      if (strncmp(pclS, "TENO", 4))
		{
		  /* this is an error */
		  dbg(DEBUG,"<HRC> unknown fieldname");

		  /* write error message to client */
		  if ((ilRC = WriteERRToClient(sUNKNOWN_FIELDNAME)) == RC_FAIL)
		    return iWRITE_ERRFAIL;
		}

				/* get second element (teleno) */
	      if ((pclS = GetDataField(pclDataPtr, 1, (char)clSeparator)) == NULL)
		{
		  dbg(TRACE,"<HRC> GetDataField returns NULL");
		  Terminate(0);
		}

				/* convert to uppercase */
	      StringUPR((UCHAR*)pclS);

				/* here we go reading from recovery area */
				/* now copy telegramnumber to structure */
	      strncpy(rlRec.pcTeleNo, pclS, iTNUMBER_SIZE);


				/* A C H T U N G 
				   Aenderungen fuer Planar, jetzt wird nicht mehr ueber das Recovery 
				   gelesen. Hier werden ab jetzt immer Initialdaten geschickt!!
				*/

#if 0
				/* read everything from recovery (at this timestamp) */
	      if ((ilRC = (*rgTM.rRec.RHandler[rgTM.rRec.iRecoverType])(iGET_ALL_AT_TELENO, (UINT*)&ilAnz, &rlRec, &prlRecArea)) != RC_SUCCESS)
#endif
		if (1) /* it works !! */
		  {
#if 0
		    dbg(DEBUG,"<HRC> RecoverHandler with function %d returns: %d",
			iGET_ALL_AT_TELENO, ilRC);
		    /* in this case, reset this server and send initial data... */
		    if (ilRC == iANZ_FAIL || ilRC == iTELENO_FAIL)
#endif
		      if (1) /* it works also */
			{

			  memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
			  sprintf(pclTmpBuf, "%s,%s,SNU", rlRec.pcTeleNo, pcgDATName);
			  dbg(DEBUG,"<HRC> %05d GMN-DataArea: <%s>", __LINE__, pclTmpBuf);
			  if ((ilRC = tools_send_sql(igRouterID, "GMN", "NUMTAB", "", "", pclTmpBuf)) != RC_SUCCESS)
			    {
			      dbg(TRACE,"<HRC> %05d tools_send_sql returns: %d", __LINE__, ilRC);
			    }
			  else
			    {
			      if ((ilRC = GetInitialBaggageData()) != RC_SUCCESS)
				{
				  dbg(TRACE,"<HRC> %05d GetInitialBaggageData returns: %d", __LINE__, ilRC);
				}
			    }
			}
		  }
		else
		  {
		    dbg(DEBUG,"<HRC> RHandler returns Anz: %d", ilAnz);

		    /* send this to client */
		    for (i=0; i<ilAnz; i++)
		      {
			/* write it to send protocol file */
			ToSProtFile(prlRecArea[i].pcData, 
				    (UINT)prlRecArea[i].lLength); 

			/* now write to client socket */
			if ((ilRC = write(igClientSocket, prlRecArea[i].pcData, (int)prlRecArea[i].lLength)) != (int)prlRecArea[i].lLength)
			  {
			    dbg(TRACE,"<HRC> write error - can't write data to client %d", ilRC);

			    /* free memory we doesn't need */
			    if (prlRecArea != NULL)
			      {
				free((void*)prlRecArea);
				prlRecArea = NULL;
			      }
			    return iWRITE_FAIL;
			  }
		      }

		    /* only a debug message */
		    dbg(DEBUG,"<HRC> ready with sending recover-data");

		    /* free memory we doesn't need */
		    if (prlRecArea != NULL)
		      {
			free((void*)prlRecArea);
			prlRecArea = NULL;
		      }
		  }
	    }
	  else
	    {
				/* number of elements isn't 2 */
				/* this is an error */
	      dbg(DEBUG,"<HRC> wrong number of elements");

				/* write error message to client */
	      if ((ilRC = WriteERRToClient(sWRONG_ELEMENTS)) == RC_FAIL)
		return iWRITE_ERRFAIL;
	    }
	}
      dbg(DEBUG, "<HRC> ---------- END <%s> ----------", pcpCommand);
    }
  /*-------------------------------------------------------------------*/
  /*-------------------------------------------------------------------*/
  /*-------------------------------------------------------------------*/
  else
    {
      dbg(TRACE, "<HRC> unkown command <%s>", pcpCommand);

      /* in this case send an ERR-Command to client... */
      if ((ilRC = WriteERRToClient(sWRONG_COMMAND)) == RC_FAIL)
	return iWRITE_ERRFAIL;
    }

  /* reset flag */
  igCCOCheck = 0;

  /* set new time for check connection */
  if (rgTM.iCCOSync)
    time1 = time(NULL);

  /* return */
  return RC_SUCCESS;
}

/******************************************************************************/
/* The BuildHeader routine                                                    */
/******************************************************************************/
static void BuildHeader(THeader *prpTH, char *pcpClass, ULONG lpTeleNo)
{
	/* fill structures (Server System) */
	memmove((void*)prpTH->pcServerSystem, (void*)rgTM.rServer.pcServerSystem,
			  (size_t)iSERVERSYS_SIZE);

	/* Sender */
	memmove((void*)prpTH->pcSender, (void*)rgTM.rServer.pcSender,
			  (size_t)iSENDER_SIZE);

	/* Receiver */
	memmove((void*)prpTH->pcReceiver, (void*)rgTM.rServer.pcReceiver,
			  (size_t)iRECEIVER_SIZE);

	/* current TimeStamp */
	memmove((void*)prpTH->pcTimeStamp, (void*)GetTimeStamp(),
			  (size_t)iTIMESTAMP_SIZE);

	/* write new number to header structure */
	sprintf(prpTH->pcTNumber, "%-5.5ld", lpTeleNo);

	/* class types (CTL,DAT) */
	memmove((void*)prpTH->pcTClass, (void*)pcpClass, (size_t)iTCLASS_SIZE);

	/* that's all */
	return;
}

/******************************************************************************/
/* The BuildCCO routine                                                       */
/******************************************************************************/
static TCCO *BuildCCO(char *pcpType)
{
	int				ilRC;
	long				lMin;
	long				lMax;
	long				lUniqueNumber;
	static TCCO		rlCCO;
	
	/* answer check connection call */
	memset((void*)&rlCCO, 0x00, iTCCO_LENGTH);

	/* get next number only if type is sCHECK */
	if (!strncmp(pcpType, sCHECK, iAQ_SIZE))
	{
		/* get next unique number */
		if ((ilRC = GetUniqueNumber(pcgServerName, pcgCCOName, "MAXOW", 
								1, &lUniqueNumber, &lMax, &lMin)) != RC_SUCCESS)
		{
			/* fatal error */
			dbg(TRACE,"<BuildCCO> GetUniquNumber returns: %d", ilRC);

			/* reset flag... */
			igStatus &= ~iF_TCPIP_CLIENT_SOCK;
			
			/* exit process */
			Terminate(0);
		}

		/* set unique number */
		lUniqueNumber = UNIQUE(lUniqueNumber, lMax, lMin);	
	}
	else
		lUniqueNumber = 0;
	
	/* Build Header */
	BuildHeader(&rlCCO.rTH, sCONTROL_CLASS, (ULONG)lUniqueNumber);

	/* set CCO specifics (Length) */
	sprintf(rlCCO.rTH.pcLength, "%-4.4d", (iTCCO_LENGTH-iTHEADER_LENGTH));			
	/* Command CCO */
	memmove((void*)rlCCO.pcCommand, (void*)"CCO", (size_t)iCOMMAND_SIZE);

	/* set answer / question */
	memmove((void*)rlCCO.pcAQ, (void*)pcpType, (size_t)iAQ_SIZE);

	/* everything is fine */
	return &rlCCO;
}

/******************************************************************************/
/* The BuildEOD routine                                                       */
/******************************************************************************/
static TEOD *BuildEOD(void)
{
	int				ilRC;
	long				lMin;
	long				lMax;
	long				lUniqueNumber;
	static TEOD		rlEOD;
	
	/* answer check connection call */
	memset((void*)&rlEOD, 0x00, iTEOD_LENGTH);

	/* get next uniquw number */
	if ((ilRC = GetUniqueNumber(pcgServerName, pcgCTLName, "MAXOW", 
								1, &lUniqueNumber, &lMax, &lMin)) != RC_SUCCESS)
	{
		/* fatal error */
		dbg(TRACE,"<BuildEOD> GetUniqueNumber returns: %d", ilRC);

		/* reset flag... */
		igStatus &= ~iF_TCPIP_CLIENT_SOCK;
		
		/* exit process */
		Terminate(0);
	}

	/* set unique number */
	lUniqueNumber = UNIQUE(lUniqueNumber, lMax, lMin);	
	
	/* Build Header */
	BuildHeader(&rlEOD.rTH, sCONTROL_CLASS, (ULONG)lUniqueNumber);

	/* set CCO specifics (Length) */
	sprintf(rlEOD.rTH.pcLength, "%-4.4d", (iTEOD_LENGTH-iTHEADER_LENGTH));			
	/* Command CCO */
	memmove((void*)rlEOD.pcCommand, (void*)"EOD", (size_t)iCOMMAND_SIZE);

	/* everything is fine */
	return &rlEOD;
}

/******************************************************************************/
/* The BuildEND routine                                                       */
/******************************************************************************/
static TEND *BuildEND(char *pcpErrorCode)
{
	int				ilRC;
	long				lMin;
	long				lMax;
	long				lUniqueNumber;
	static TEND		rlEND;
	
	/* clear memory */
	memset ((void*)&rlEND, 0x00, iTEND_LENGTH);
	
	/* get next uniquw number */
	if ((ilRC = GetUniqueNumber(pcgServerName, pcgCTLName, "MAXOW", 
									1, &lUniqueNumber, &lMax, &lMin)) != RC_SUCCESS)
	{
		/* fatal error */
		dbg(TRACE,"<BuildEND> GetUniqueNumber returns: %d", ilRC);
		
		/* reset flag... */
		igStatus &= ~iF_TCPIP_CLIENT_SOCK;
		
		/* exit process */
		Terminate(0);
	}

	/* set unique number */
	lUniqueNumber = UNIQUE(lUniqueNumber, lMax, lMin);	

	/* build header for command */
	BuildHeader(&rlEND.rTH, sCONTROL_CLASS, (ULONG)lUniqueNumber);

	/* set END specifics (Length) */
	sprintf(rlEND.rTH.pcLength, "%-4.4d", (iTEND_LENGTH-iTHEADER_LENGTH));			
	/* Command END */
	memmove((void*)rlEND.pcCommand, "END", (size_t)iCOMMAND_SIZE);

	/* ErrorCode for END */
	memmove((void*)rlEND.pcErrorCode, pcpErrorCode, (size_t)iERRORCODE_SIZE);

	/* everything is fine */
	return &rlEND;
}

/******************************************************************************/
/* The BuildDATA routine                                                      */
/******************************************************************************/
static void BuildData(char *pcpType, char *pcpKey, 
							 char *pcpClassType, ULONG lpUniqueNumber)
{
	/* clear the whole send data memory */
	memset ((void*)prgData, 0x00, iMAX_SEND_DATA);

	/* build header for command */
	BuildHeader(&prgData->rTH, pcpClassType, lpUniqueNumber);

	/* set time stamp */
	memmove((void*)prgData->pcTimeStamp, (void*)GetTimeStamp(), 
													(size_t)iTIMESTAMP_SIZE);

	/* Origin - currently not used */
	memmove((void*)prgData->pcOrigin, (void*)"   ", (size_t)iORIGIN_SIZE);

	/* Type */
	memmove((void*)prgData->pcType, (void*)pcpType, (size_t)iTYPE_SIZE);

	/* Key */
	memmove((void*)prgData->pcKey, (void*)pcpKey, (size_t)iKEY_SIZE);	

	/* Separator */
	prgData->pcSeparator[0] = cDATA_SEPARATOR;  

	/* everything is fine */
	return;
}

/******************************************************************************/
/* The BuildMix routine                                                       */
/******************************************************************************/
static void BuildMix(char *pcpCommand, char *pcpClassType, ULONG lpUniqueNumber)
{
	/* clear the whole send data memory */
	memset ((void*)prgMix, 0x00, iMAX_SEND_DATA);

	/* build header for command */
	BuildHeader(&prgMix->rTH, pcpClassType, lpUniqueNumber);

	/* set command */
	memmove((void*)prgMix->pcCommand, (void*)pcpCommand, (size_t)iCOMMAND_SIZE);

	/* Separator */
	prgMix->pcSeparator[0] = cDATA_SEPARATOR;  

	/* everything is fine */
	return;
}

/******************************************************************************/
/* The CheckConnection routine                                                */
/******************************************************************************/
static int CheckConnection(void)
{
	int			ilRC;
	TCCO			*prlCCO;

	if (igDbgMode)
		dbg(DEBUG,"<CheckConnection> ----- START -----");

	/* current time */
	time2 = time(NULL);

	/* is difftime enough for new connection check? */
	if ((double)rgTM.iCheckConnect > difftime(time2, time1))
	{
		if (igDbgMode)
			dbg(DEBUG,"<CheckConnection> ----- END -----");
		return RC_SUCCESS;
	}

	/* set new time1 */
	time1 = time2;

	/* Build the CCO-Structure */
	if ((prlCCO = BuildCCO(sCHECK)) == NULL)
	{
		dbg(TRACE, "<CheckConnection> BuildCCO returns NULL");
		Terminate(0);
	}

	/* set Flag, we need it for different timeouts */
	igCCOCheck = 1;

	/* set current CCO-Number */
	igLastCCONo = atoi(prlCCO->rTH.pcTNumber);

	/* write it to send protocol file */
	ToSProtFile((char*)prlCCO, iTCCO_LENGTH); 

	/* write it to socket */
	if ((ilRC = write(igClientSocket, prlCCO, iTCCO_LENGTH)) != iTCCO_LENGTH)
	{
		dbg(TRACE,"<CheckConnection> write error - can't to client socket (%d)", ilRC);
		return iWRITE_FAIL;
	}

	if (igDbgMode)
		dbg(DEBUG,"<CheckConnection> ----- END -----");
	/* ciao */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The CreateTCPConnection routine                                            */
/******************************************************************************/
static int CreateTCPConnection(void)
{

#ifdef _SNI
	UINT								ilLen;
#else
	int								ilLen;
#endif

	int								i;
	int								j;
	int								ilRC;
	int								ilFlg;
	char								pclTmpBuf[iMIN_BUF_SIZE]; 
	struct in_addr					rlIn;
	static struct sockaddr_in	rlClient;

	/* first set current status */
	igStatus &= ~iF_TCPIP_CLIENT_SOCK;

	/* create socket */
	if ((igTcpSock = tcp_create_socket(SOCK_STREAM, 
						rgTM.rTCP.pcServiceName)) == RC_FAIL)
	{
		dbg(DEBUG,"<CTCPIPC> tcp_create_socket returns: %d", igTcpSock);
		return 1;
	}

	/* now listen for connections on a socket */
	if ((ilRC = listen(igTcpSock, rgTM.rTCP.iMaxConnections)) == -1)
	{
		dbg(DEBUG,"<CTCPIPC> listen returns %d (%s)", igTcpSock, strerror(errno));
		return 2;
	}

	do
	{
		/* set flag... */
		ilFlg = 0;

		/* accept the TCP/IP-Connection */
		ilLen = sizeof(rlClient);

		/* set alarm */
		alarm(5);

		/* accept this */
		igClientSocket = accept(igTcpSock, (struct sockaddr*)&rlClient, &ilLen);

		/* reset alarm */
		alarm(0);

		/* is this a valid socket? */
		if (igClientSocket >= 0)
		{
			/* ...and set flag */
			ilFlg = 1;

			dbg(DEBUG,"<CTCPIPC> after accept: IP-Addr <%x>", ntohl(rlClient.sin_addr.s_addr));

			/* should we use dynamic ftp-name?...*/
			strcpy(pclTmpBuf, rgTM.rFtp.pcMachine);
			StringUPR((UCHAR*)pclTmpBuf);
			if (!strcmp(pclTmpBuf, "SEARCH_DYNAMIC"))
			{
				rlIn.s_addr = ntohl(rlClient.sin_addr.s_addr);
				dbg(DEBUG,"<CTCPIPC> using dynamic ftp");
				strcpy(rgTM.rFtp.pcMachine, inet_ntoa(rlIn));
				dbg(DEBUG,"<CTCPIPC> machine is: <%s>", rgTM.rFtp.pcMachine);
			}
		}

		/* Check Que now */
		ilRC = CheckQueStatus(mod_id);
		if (prgItem != NULL)
		{
			free((void*)prgItem);
			prgItem = NULL;
		}
		if (ilRC < 0)
			return ilRC;

	} while (!ilFlg);

	/* shutdown (listen) socket */
	ilRC = shutdown (igTcpSock, 2);

	/* close (listen) socket */
	ilRC = close (igTcpSock);

	/* set status */
	igStatus |= iF_TCPIP_CLIENT_SOCK;

	/* send message to db, ceda.log... */
	dbg(DEBUG,"<CTCPIPC> calling send_message... ");
	memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
	strcpy(pclTmpBuf, rgTM.rTCP.pcServiceName);
	dbg(DEBUG,"<CTCPIPC> buffer for send_message: <%s>", pclTmpBuf);
	if ((ilRC = send_message(IPRIO_DB, iCREATE_CONNECTION, 0, 0, pclTmpBuf)) != RC_SUCCESS)
	{
		dbg(DEBUG,"<CTCPIPC> send_message returns: %d", ilRC);
	}

	/* insert all sections in action-configuration file... */
	/* -------------------------------------------------------------------*/
	/* -------------------------------------------------------------------*/
	/* -------------------------------------------------------------------*/
	/* -------------------------------------------------------------------*/
	for (i=0; i<rgTM.iNoCmd; i++)
	{
		for (j=0; j<rgTM.prCmd[i].iNoTables; j++)
		{
			/* delete selection for all commands in action configuiration */
			ilRC = HandleActionSection(iINSERT_SECTION, rgTM.prCmd[i].prTabDef[j].pcSndCmd, rgTM.prCmd[i].pcCommand, i, j);
			dbg(DEBUG,"<CTCPIPC> %sinsert Section(s) <%s> with command <%s>",	ilRC == RC_SUCCESS ? "" : "can't ", rgTM.prCmd[i].pcCommand, rgTM.prCmd[i].prTabDef[j].pcSndCmd);
		}
	}

	/* delete SFU-sections if it exist... */
	/* -------------------------------------------------------------------*/
	/* -------------------------------------------------------------------*/
	/* -------------------------------------------------------------------*/
	/* -------------------------------------------------------------------*/
	if ((i = SearchCommandNumber("SFU")) != RC_FAIL)
	{
		for (j=0; j<rgTM.prCmd[i].iNoTables; j++)
		{
			/* delete selection for SFU in action configuiration */
			ilRC = HandleActionSection(iDELETE_SECTION, rgTM.prCmd[i].prTabDef[j].pcSndCmd, "SFU", i, j);
			dbg(DEBUG,"<CTCPIPC> %sdelete Section(s) <%s> with command <%s>",	ilRC == RC_SUCCESS ? "" : "can't ", rgTM.prCmd[i].pcCommand, rgTM.prCmd[i].prTabDef[j].pcSndCmd);
		}
	}

	/* send OpenConnection to Andock-/Baggagehandler (someone else)... */
	if (rgTM.iConnectModID > 0)
	{
		/* here we send command */
		dbg(DEBUG,"<CTCPIPC> sending command: <%s> to <%d>...", rgTM.pcOpenConnectCommand, rgTM.iConnectModID);
		if ((ilRC = tools_send_sql(rgTM.iConnectModID, rgTM.pcOpenConnectCommand, "", "", "", "")) != RC_SUCCESS)
		{
			dbg(TRACE,"<CTCPIPC> %05d tools_send_sql returns: %d", __LINE__, ilRC);
		}
	}

	/* everything is fine */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The CloseTCPConnection routine                                             */
/******************************************************************************/
static void CloseTCPConnection(void)
{
	int		ilRC;
	char		pclTmpBuf[iMIN_BUF_SIZE]; 
	TEND		*prlEND;

	/* send an END to client */
	if (igStatus & iF_TCPIP_CLIENT_SOCK)
	{
		/* Build the END-Structure */
		if ((prlEND = BuildEND("SD")) == NULL)
		{
			dbg(DEBUG, "<CloseTCPConnection> BuildEND returns NULL");
		}
		else
		{
			/* write it to send protocol file */
			ToSProtFile((char*)prlEND, iTEND_LENGTH); 

			/* write it to socket */
			if ((ilRC = write(igClientSocket, prlEND, iTEND_LENGTH)) != iTEND_LENGTH)
			{
				dbg(TRACE,"<CloseTCPConnection> write error - can't write END to client socket (%d)", ilRC);
			}
		}

		/* send message to db, ceda.log... */
		dbg(DEBUG,"<CloseTCPConnection> calling send_message... ");
		memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
		strcpy(pclTmpBuf, rgTM.rTCP.pcServiceName);
		dbg(DEBUG,"<CloseTCPConnection> buffer for send_message: <%s>", pclTmpBuf);
		if ((ilRC = send_message(IPRIO_DB, iDELETE_CONNECTION, 0, 0, pclTmpBuf)) != RC_SUCCESS)
		{
			dbg(DEBUG,"<CloseTCPConnection> send_message returns: %d", ilRC);
		}

		/* send CloseConnection to Andock-/Baggagehandler (someone else)... */
		if (rgTM.iConnectModID > 0)
		{
			/* here we send command */
			dbg(DEBUG,"<CloseTCPConnection> sending command: <%s> to <%d>...", rgTM.pcCloseConnectCommand, rgTM.iConnectModID);
			if ((ilRC = tools_send_sql(rgTM.iConnectModID, rgTM.pcCloseConnectCommand, "", "", "", "")) != RC_SUCCESS)
			{
				dbg(TRACE,"<CloseTCPConnection> %05d tools_send_sql returns: %d", __LINE__, ilRC);
			}
		}
	}
	
	/* shutdown socket */
	ilRC = shutdown (igTcpSock, 2);

	/* close socket */
	ilRC = close (igTcpSock);

	/* shutdown socket */
	ilRC = shutdown (igClientSocket, 2);

	/* close socket */
	ilRC = close (igClientSocket);

	/* reset flag */
	igStatus &= ~iF_TCPIP_CLIENT_SOCK;

	/* alles war klasse */
	return;
}

/******************************************************************************/
/* The SearchCommandNumber routine                                            */
/******************************************************************************/
static int SearchCommandNumber(char *pcpCmd)
{
	int		i;
	
	for (i=0; i<rgTM.iNoCmd; i++)
	{
		if (!strcmp(rgTM.prCmd[i].pcCommand, pcpCmd))
		{
			/* found the command */
			return i;
		}
	}
	return RC_FAIL;
}

/******************************************************************************/
/* The GetMaxLength routine                                                   */
/******************************************************************************/
static int GetMaxLength(int ipMax, char cpSeparator, char *pcpData)
{
	int		ilMaxLength;
	int		ilSeparator;
	char		*s;

	/* init something */
	ilMaxLength = 0;
	ilSeparator = 0;
	s = pcpData;

	/* the whole string */
	while (*s)
	{
		if (*s == cpSeparator)
			ilSeparator++;
		
		if (++ilMaxLength >= ipMax)
		{
			while (*s != cpSeparator || ilSeparator % 2)
			{
				ilMaxLength--;
				if (*s == cpSeparator)
					ilSeparator--;
				DIRECTION(iBACKWARD, s);
			}
			break;
		}
		DIRECTION(iFORWARD, s);
	}

	/* bye bye */
	return ilMaxLength;
}

/******************************************************************************/
/* The HandleMemoryRecovery routine                                           */
/******************************************************************************/
static int HandleMemoryRecovery(UINT ipFunction, UINT *pipAnz,
										  TRecover *prpRecIn, TRecover **prpRecOut)
{
	int					i;
	static UINT			ilLevel;
	TRecover				*prlRecArea = NULL;

	if (!(igStatus & iF_RECOVER_MEMORY))
	{
		dbg(DEBUG,"<HMR> call HandleMemoryRecovery without memory");
		if ((prgRecover = 
			 (TRecover*)malloc((size_t)(rgTM.rRec.iRecoverCounter*sizeof(TRecover)))) ==NULL)
		{
			dbg(DEBUG, "<HMR> not enough memory to allocate recover memory");
			prgRecover = NULL;
			return iRECOVER_MEMORY_FAIL;
		}
		else
		{
			/* set status */
			igStatus |= iF_RECOVER_MEMORY;

			/* clear memory */
			memset((void*)prgRecover, 0x00, 
							rgTM.rRec.iRecoverCounter*sizeof(TRecover));
		}
	}
	
	switch(ipFunction)
	{
		case iCLEAR_ALL:
			memset((void*)prgRecover, 0x00, 
				  	 rgTM.rRec.iRecoverCounter*sizeof(TRecover));
			ilLevel = 0;
			return RC_SUCCESS;

		case iADD_CMD:
			if (rgTM.rRec.iSaveData == iCOUNTER)
			{
				if (ilLevel == rgTM.rRec.iRecoverCounter-1)
				{
					memset((void*)&prgRecover[0], 0x00, sizeof(TRecover));
					memmove((void*)prgRecover, (void*)&prgRecover[1],
							  (ilLevel-1)*sizeof(TRecover));
					memset((void*)&prgRecover[ilLevel-1], 0x00, sizeof(TRecover));

					memcpy((void*)&prgRecover[ilLevel],
							 (const void*)prpRecIn, sizeof(TRecover));
					return RC_SUCCESS;
				}
				else
				{
					memcpy((void*)&prgRecover[ilLevel],
							 (const void*)prpRecIn, sizeof(TRecover));
					ilLevel++;
					return RC_SUCCESS;
				}
			}
			else
			{
				/* Recovery with TIME-Option */
				return RC_SUCCESS;
			}

		case iGET_FIRST_CMD:
			if (ilLevel)
			{
				/* get memory */
				if ((prlRecArea = (TRecover*)malloc(sizeof(TRecover))) == NULL)
				{
					dbg(DEBUG,"<HMR> cannot malloc for recovery...");
					prlRecArea = NULL;
					return iFIRST_MALLOC_FAIL;	
				}
				else
				{
					memcpy((void*)prlRecArea,
							 (const void*)prgRecover, sizeof(TRecover));
					*pipAnz = 1;
					*prpRecOut = prlRecArea;
					return RC_SUCCESS;
				}
			}
			else
			{
				dbg(DEBUG, "<HMR> no entry in memory recovery-list");
				return iFIRST_CMD_FAIL;
			}

		case iGET_LAST_CMD:
			if (ilLevel)
			{
				/* get memory */
				if ((prlRecArea = (TRecover*)malloc(sizeof(TRecover))) == NULL)
				{
					dbg(DEBUG,"<HMR> cannot malloc for recovery...");
					prlRecArea = NULL;
					return iLAST_MALLOC_FAIL;	
				}
				else
				{
					memcpy((void*)prlRecArea,
							 (const void*)&prgRecover[ilLevel-1], sizeof(TRecover));
					*pipAnz = 1;
					*prpRecOut = prlRecArea;
					return RC_SUCCESS;
				}
			}
			else
			{
				dbg(DEBUG, "<HMR> no entry in memory recovery-list");
				return iLAST_CMD_FAIL;
			}

		case iGET_TELENO_CMD:
			for (i=0; i<ilLevel; i++)
			{
				if (!strcmp(prgRecover[i].pcTeleNo, prpRecIn->pcTeleNo))
				{
					/* found telenumber */
					/* get memory */
					if ((prlRecArea = (TRecover*)malloc(sizeof(TRecover))) == NULL)
					{
						dbg(DEBUG,"<HMR> cannot malloc for recovery...");
						prlRecArea = NULL;
						return iGET_TELENO_MALLOC_FAIL;	
					}
					else
					{
						memcpy((void*)prlRecArea,
							 (const void*)&prgRecover[i], sizeof(TRecover));
						*pipAnz = 1;
						*prpRecOut = prlRecArea;
						return RC_SUCCESS;
					}
				}
			}
			dbg(DEBUG,"<HMR> can't find telegramnumber");
			return iTELENO_FAIL;

		case iGET_ALL_AT_TELENO:
			/* first count number of entry we must return.. */
			for (i=0,*pipAnz=0; i<ilLevel; i++)
			{
				if (strcmp(prgRecover[i].pcTeleNo, prpRecIn->pcTeleNo) >= 0)
				{
					(*pipAnz)++;
				}
			}

			if (*pipAnz > 0)
			{
				/* get memory for all */
				if ((prlRecArea = (TRecover*)malloc(((*pipAnz)*sizeof(TRecover)))) == NULL)
				{
					dbg(DEBUG,"<HMR> cannot malloc for recovery...");
					prlRecArea = NULL;
					return iGET_ALL_AT_TELENO_MALLOC_FAIL;	
				}
				else
				{
					for (i=0,*pipAnz=0; i<ilLevel; i++)
					{
						if (strcmp(prgRecover[i].pcTeleNo, prpRecIn->pcTeleNo) >= 0)
						{
							memcpy((void*)&prlRecArea[(*pipAnz)],
								 (const void*)&prgRecover[i], sizeof(TRecover));
							(*pipAnz)++;
						}
					}
				}
			}

			if (*pipAnz)
			{
				*prpRecOut = prlRecArea;
				return RC_SUCCESS;
			}
			else
			{
				dbg(DEBUG,"<HMR> can't find telegramnumber");
				return iTELENO_FAIL;
			}

		case iGET_ALL_AT_TIMESTAMP:
			/* first count number of entry we must return.. */
			for (i=0,*pipAnz=0; i<ilLevel; i++)
			{
				if (strcmp(prgRecover[i].pcTimeStamp, prpRecIn->pcTimeStamp) >= 0)
				{
					(*pipAnz)++;
				}
			}

			if (*pipAnz > 0)
			{
				/* get memory for all */
				if ((prlRecArea = (TRecover*)malloc(((*pipAnz)*sizeof(TRecover)))) == NULL)
				{
					dbg(DEBUG,"<HMR> cannot malloc for recovery...");
					prlRecArea = NULL;
					return iGET_ALL_AT_TIMESTAMP_MALLOC_FAIL;	
				}
				else
				{
					for (i=0,*pipAnz=0; i<ilLevel; i++)
					{
						if (strcmp(prgRecover[i].pcTimeStamp, prpRecIn->pcTimeStamp) >= 0)
						{
							memcpy((void*)&prlRecArea[(*pipAnz)],
								 (const void*)&prgRecover[i], sizeof(TRecover));
							(*pipAnz)++;
						}
					}
				}
			}

			if (*pipAnz)
			{
				*prpRecOut = prlRecArea;
				return RC_SUCCESS;
			}
			else
			{
				dbg(DEBUG,"<HMR> can't find timestamp");
				return iTELENO_FAIL;
			}

		case iPRINT:
			dbg(DEBUG,"<HMR> PRINT");
			for (i=0; i<ilLevel; i++)
			{
				dbg(DEBUG,"<HMR> CmdNo: %d TabNo: %d TimeStamp: <%s> TeleNo: <%s>",
								prgRecover[i].iCmdNo, prgRecover[i].iTabNo, 
								prgRecover[i].pcTimeStamp, prgRecover[i].pcTeleNo);
			}
			return RC_SUCCESS;
	}
	return RC_SUCCESS;
}

/******************************************************************************/
/* The HandleFileRecovery routine                                             */
/******************************************************************************/
static int HandleFileRecovery(UINT ipFunction, UINT *pipAnz, 
										TRecover *prpRecIn, TRecover **prpRecOut)
{
	int					i;
	int					ilRC;
	int					ilLevel;
	TRecover				*prlRecArea = NULL;
	TRecover				rlRecTmp;
	FILE					*fh;
	 
	if ((fh = fopen((const char*)rgTM.rRec.pcRecoverFile,"r")) == NULL)
	{
		dbg(DEBUG,"<HFR> can't open file %s", rgTM.rRec.pcRecoverFile);
		ilLevel = 0;
	}
	else
	{
		/* get current entry-level */
		fseek(fh, 0L, SEEK_SET);
		fscanf(fh, "%d\n", &ilLevel);

		/* close file */
		fclose(fh);
	}

	switch(ipFunction)
	{
		case iCLEAR_ALL:
			/* open it again */
			if ((fh = fopen((const char*)rgTM.rRec.pcRecoverFile,"w")) == NULL)
			{
				dbg(DEBUG,"<HFR> can't open file %s", rgTM.rRec.pcRecoverFile);
				return iRECOVER_FOPEN_FAIL;
			}

			/* set current level */
			ilLevel = 0;

			/* write it to file */
			fseek(fh, 0L, SEEK_SET);
			fprintf(fh, "%08d\n", ilLevel);

			/* close file */
			fclose(fh);

			/* back to calling routine */
			return RC_SUCCESS;
			
		case iADD_CMD:
			if (rgTM.rRec.iSaveData == iCOUNTER)
			{
				if (ilLevel == rgTM.rRec.iRecoverCounter*2)
				{
					dbg(DEBUG,"<HFR> overwrite first %d elements",
													rgTM.rRec.iRecoverCounter);

					/* delete first row in file */
					if ((ilRC = DeleteRowInFile(1, rgTM.rRec.iRecoverCounter)) != RC_SUCCESS)
					{
						dbg(DEBUG,"<HFR> DeleteRowInFile returns: %d", ilRC);
						return ilRC;
					}
					else
						ilLevel -= (rgTM.rRec.iRecoverCounter - 1);
				}
				else
					ilLevel++;

				/* open file for appending */
				if ((fh = fopen((const char*)rgTM.rRec.pcRecoverFile,"a")) == NULL)
				{
					dbg(DEBUG,"<HFR> can't open %s", rgTM.rRec.pcRecoverFile);
					return iRECOVER_FOPEN_FAIL;
				}

				/* seek to end of file */
				fseek(fh, 0L, SEEK_END);
				if (ftell(fh) == 0L)
				{
					fprintf(fh, "%08d\n", ilLevel);
				}

				/* write data to file */
				fprintf(fh, "%d %d %ld %s %s ", prpRecIn->iCmdNo, prpRecIn->iTabNo, 
						prpRecIn->lLength, prpRecIn->pcTimeStamp, prpRecIn->pcTeleNo);
				fwrite((const void*)prpRecIn->pcData, prpRecIn->lLength, 1, fh);
				fprintf(fh, "\n");

				/* close recover file */
				fclose (fh);

				/* open it again for updating level */
				if ((fh = fopen((const char*)rgTM.rRec.pcRecoverFile,"r+")) == NULL)
				{
					dbg(DEBUG,"<HFR> can't open %s", rgTM.rRec.pcRecoverFile);
					return iRECOVER_FOPEN_FAIL;
				}

				/* seek to start of file */
				fseek(fh, 0L, SEEK_SET);

				/* write data to file */
				fseek(fh, 0L, SEEK_SET);
				fprintf(fh, "%08d\n", ilLevel);

				/* close recover file */
				fclose (fh);

				/* return */
				return RC_SUCCESS;
			}
			else
			{
				/* Recovery with TIME-Option */
				return RC_SUCCESS;
			}

		case iGET_FIRST_CMD:
			if ((fh = fopen((const char*)rgTM.rRec.pcRecoverFile,"r")) == NULL)
			{
				dbg(DEBUG,"<HFR> can't open file %s", rgTM.rRec.pcRecoverFile);
				return iFIRST_CMD_FAIL;
			}
			else
			{
				/* get current entry-level */
				fseek(fh, 0L, SEEK_SET);
				fscanf(fh, "%d\n", &ilLevel);

				if (ilLevel)
				{
					/* get memory for returning data */
					if ((prlRecArea = (TRecover*)malloc(sizeof(TRecover))) == NULL)
					{
						dbg(DEBUG,"<HFR> cannot malloc for recovery...");
						fclose(fh);
						prlRecArea = NULL;
						return iFIRST_MALLOC_FAIL;	
					}
					else
					{
						/* get first command */
						fscanf(fh,"%d %d %ld %s %s ", 
								&prlRecArea->iCmdNo, &prlRecArea->iTabNo, 
								&prlRecArea->lLength, prlRecArea->pcTimeStamp, 
								prlRecArea->pcTeleNo);
						fread(prlRecArea->pcData, prlRecArea->lLength, 1, fh);
						fscanf(fh,"\n");
						*pipAnz = 1;
						*prpRecOut = prlRecArea;
						return RC_SUCCESS;
					}
				}
				else
				{
					dbg(DEBUG,"<HFR> Level = 0");
					fclose(fh);
					return iLEVEL_FAIL;
				}
			}

		case iGET_LAST_CMD:
			if ((fh = fopen((const char*)rgTM.rRec.pcRecoverFile,"r")) == NULL)
			{
				dbg(DEBUG,"<HFR> can't open file %s", rgTM.rRec.pcRecoverFile);
				return iLAST_CMD_FAIL;
			}
			else
			{
				/* get current entry-level */
				fseek(fh, 0L, SEEK_SET);
				fscanf(fh, "%d\n", &ilLevel);

				if (ilLevel)
				{
					/* get memory for returning data */
					if ((prlRecArea = (TRecover*)malloc(sizeof(TRecover))) == NULL)
					{
						dbg(DEBUG,"<HFR> cannot malloc for recovery...");
						fclose(fh);
						prlRecArea = NULL;
						return iLAST_MALLOC_FAIL;	
					}
					else
					{
						/* seek to last command */
						for (i=0; i<ilLevel-2 ; i++)
							fscanf(fh,"%*[^\n]%*c");

						/* get last command */
						fscanf(fh,"%d %d %ld %s %s ", 
								&prlRecArea->iCmdNo, &prlRecArea->iTabNo, 
								&prlRecArea->lLength, prlRecArea->pcTimeStamp, 
								prlRecArea->pcTeleNo);
						fread(prlRecArea->pcData, prlRecArea->lLength, 1, fh);
						fscanf(fh,"\n");
						*pipAnz = 1;
						*prpRecOut = prlRecArea;
						return RC_SUCCESS;
					}
				}
				else
				{
					dbg(DEBUG,"<HFR> Level = 0");
					fclose(fh);
					return iLEVEL_FAIL;
				}
			}

		case iGET_TELENO_CMD:
			if ((fh = fopen((const char*)rgTM.rRec.pcRecoverFile,"r")) == NULL)
			{
				dbg(DEBUG,"<HFR> can't open file %s", rgTM.rRec.pcRecoverFile);
				return iTELENO_FAIL;
			}
			else
			{
				/* get current entry-level */
				fseek(fh, 0L, SEEK_SET);
				fscanf(fh, "%d\n", &ilLevel);

				if (ilLevel)
				{
					/* seek to last command */
					for (i=0; i<ilLevel ; i++)
					{
						/* get this command */
						fscanf(fh,"%d %d %ld %s %s ", 
								&rlRecTmp.iCmdNo, &rlRecTmp.iTabNo, 
								&rlRecTmp.lLength, rlRecTmp.pcTimeStamp, 
								rlRecTmp.pcTeleNo);
						fread(rlRecTmp.pcData, rlRecTmp.lLength, 1, fh);
						fscanf(fh,"\n");

						/* is this the number? */
						if (!strcmp(prpRecIn->pcTeleNo, rlRecTmp.pcTeleNo))
						{
							/* get memory for returning data */
							if ((prlRecArea = (TRecover*)malloc(sizeof(TRecover))) == NULL)
							{
								dbg(DEBUG,"<HFR> cannot malloc for recovery...");
								fclose(fh);
								prlRecArea = NULL;
								return iLAST_MALLOC_FAIL;	
							}
							else
							{
								memcpy((void*)prlRecArea, 
											(const void*)&rlRecTmp, sizeof(TRecover));
								*pipAnz = 1;
								*prpRecOut = prlRecArea;
								fclose(fh);
								return RC_SUCCESS;
							}
						}
					}
				}
				else
				{
					dbg(DEBUG,"<HFR> Level = 0");
					fclose(fh);
					return iLEVEL_FAIL;
				}
			}
			break;

		case iGET_ALL_AT_TELENO:
			if ((fh = fopen((const char*)rgTM.rRec.pcRecoverFile,"r")) == NULL)
			{
				dbg(DEBUG,"<HFR> can't open file %s", rgTM.rRec.pcRecoverFile);
				return iTELENO_FAIL;
			}
			else
			{
				/* get current entry-level */
				fseek(fh, 0L, SEEK_SET);
				fscanf(fh, "%d\n", &ilLevel);

				/* seek to last command */
				for (i=0, *pipAnz=0; i<ilLevel ; i++)
				{
					/* get last command */
					fscanf(fh,"%d %d %ld %s %s ", 
							&rlRecTmp.iCmdNo, &rlRecTmp.iTabNo, 
							&rlRecTmp.lLength, rlRecTmp.pcTimeStamp, 
							rlRecTmp.pcTeleNo);
					fread(rlRecTmp.pcData, rlRecTmp.lLength, 1, fh);
					fscanf(fh,"\n");

					/* is this the number? */
					if (strcmp(rlRecTmp.pcTeleNo, prpRecIn->pcTeleNo) >= 0)
					{
						(*pipAnz)++;
					}
				}

				if (*pipAnz > 0)
				{
					/* get memory for all */
					if ((prlRecArea = (TRecover*)malloc(((*pipAnz)*sizeof(TRecover)))) == NULL)
					{
						dbg(DEBUG,"<HFR> cannot malloc for recovery...");
						fclose(fh);
						prlRecArea = NULL;
						return iGET_ALL_AT_TELENO_MALLOC_FAIL;	
					}
					else
					{
						/* set to start of file */
						fseek(fh, 0L, SEEK_SET);

						/* read header */
						fscanf(fh, "%*d\n");

						for (i=0,*pipAnz=0; i<ilLevel; i++)
						{
							/* get last command */
							fscanf(fh,"%d %d %ld %s %s ", 
									&rlRecTmp.iCmdNo, &rlRecTmp.iTabNo, 
									&rlRecTmp.lLength, rlRecTmp.pcTimeStamp, 
									rlRecTmp.pcTeleNo);
							fread(rlRecTmp.pcData, rlRecTmp.lLength, 1, fh);
							fscanf(fh,"\n");

							/* is this the number? */
							if (strcmp(rlRecTmp.pcTeleNo, prpRecIn->pcTeleNo) >= 0)
							{
								memcpy((void*)&prlRecArea[(*pipAnz)], 
											(const void*)&rlRecTmp, sizeof(TRecover));
								(*pipAnz)++;
							}
						}
						fclose(fh);
						*prpRecOut = prlRecArea;
						return RC_SUCCESS;
					}
				}
				else
				{
					fclose(fh);
					return iANZ_FAIL;
				}
			}

		case iGET_ALL_AT_TIMESTAMP:
			if ((fh = fopen((const char*)rgTM.rRec.pcRecoverFile,"r")) == NULL)
			{
				dbg(DEBUG,"<HFR> can't open file %s", rgTM.rRec.pcRecoverFile);
				return iTELENO_FAIL;
			}
			else
			{
				/* get current entry-level */
				fseek(fh, 0L, SEEK_SET);
				fscanf(fh, "%d\n", &ilLevel);

				/* seek to last command */
				for (i=0, *pipAnz=0; i<ilLevel ; i++)
				{
					/* get last command */
					fscanf(fh,"%d %d %ld %s %s ", 
							&rlRecTmp.iCmdNo, &rlRecTmp.iTabNo, 
							&rlRecTmp.lLength, rlRecTmp.pcTimeStamp, 
							rlRecTmp.pcTeleNo);
					fread(rlRecTmp.pcData, rlRecTmp.lLength, 1, fh);
					fscanf(fh,"\n");

					/* is this the number? */
					if (strcmp(rlRecTmp.pcTimeStamp, prpRecIn->pcTimeStamp) >= 0)
					{
						(*pipAnz)++;
					}
				}

				if (*pipAnz > 0)
				{
					/* get memory for all */
					if ((prlRecArea = (TRecover*)malloc(((*pipAnz)*sizeof(TRecover)))) == NULL)
					{
						dbg(DEBUG,"<HFR> cannot malloc for recovery...");
						fclose(fh);
						prlRecArea = NULL;
						return iGET_ALL_AT_TELENO_MALLOC_FAIL;	
					}
					else
					{
						/* set to start of file */
						fseek(fh, 0L, SEEK_SET);

						/* read header */
						fscanf(fh, "%*d\n");

						for (i=0,*pipAnz=0; i<ilLevel; i++)
						{
							/* get last command */
							fscanf(fh,"%d %d %ld %s %s ", 
									&rlRecTmp.iCmdNo, &rlRecTmp.iTabNo, 
									&rlRecTmp.lLength, rlRecTmp.pcTimeStamp, 
									rlRecTmp.pcTeleNo);
							fread(rlRecTmp.pcData, rlRecTmp.lLength, 1, fh);
							fscanf(fh,"\n");

							/* is this the number? */
							if (strcmp(rlRecTmp.pcTimeStamp, prpRecIn->pcTimeStamp) >= 0)
							{
								memcpy((void*)&prlRecArea[(*pipAnz)], 
											(const void*)&rlRecTmp, sizeof(TRecover));
								(*pipAnz)++;
							}
						}
						fclose(fh);
						*prpRecOut = prlRecArea;
						return RC_SUCCESS;
					}
				}
				else
				{
					fclose(fh);
					return iANZ_FAIL;
				}
			}
			
		case iPRINT:
			dbg(DEBUG,"<HFR> print all file entries now");
			return RC_SUCCESS;
	}
	return RC_SUCCESS;
}

/******************************************************************************/
/* The DeleteRowInFile routine                                                */
/******************************************************************************/
static int DeleteRowInFile(int ipRowVon, int ipRowBis)
{
	int				c;
	int				cnt;
	int				ilRC;
	FILE				*fp;
	FILE				*fh;
	char				pclTmpBuf[iMIN_BUF_SIZE];

	/* open recover file for reading */
	if ((fh = fopen((const char*)rgTM.rRec.pcRecoverFile,"r")) == NULL)
	{
		dbg(DEBUG,"<HFR> can't open file %s", rgTM.rRec.pcRecoverFile);
		return iRECOVER_FOPEN_FAIL;
	}

	memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
	sprintf(pclTmpBuf, "%s/recover%s.tmp",rgTM.rRec.pcRecoverTmpDir, GetTimeStamp());
	if ((fp = fopen((const char*)pclTmpBuf, "w")) == NULL)
	{
		dbg(DEBUG,"<DRIF> connot open temporary file %s", pclTmpBuf);
		return iTMPFILE_FAIL;
	}
	else
	{
		/* linecounter */
		cnt = 0;

		/* set position in files */
		fseek(fp, 0L, SEEK_SET);
		fseek(fh, 0L, SEEK_SET);

		/* do until end of file */
		while ((c = fgetc(fh)) != EOF)
		{
			if (c == '\n') cnt++;
			if (cnt >= ipRowVon  && cnt <= ipRowBis) 
			{
				do
				{
					if ((c = fgetc(fh)) == '\n')
						cnt++;
				} while (cnt <= ipRowBis && c != EOF);
				fputc('\n', fp);
			}
			else
				fputc(c, fp);
		}

		/* close file */
		fclose(fh);

		/* close tmp-file */
		fclose(fp);

		/* copy tmp-file to recover file */
		errno = 0;
		if ((ilRC = rename((const char*)pclTmpBuf, 
								(const char*)rgTM.rRec.pcRecoverFile)) != 0)
		{
			dbg(DEBUG,"<DRiF> cannot rename file from <%s> to <%s> errno=%d",
				pclTmpBuf, rgTM.rRec.pcRecoverFile,errno);
			return RC_FAIL;
		}
		else
		{
			dbg(DEBUG,"<DRiF> rename file succesfull from <%s> to <%s>",
				pclTmpBuf, rgTM.rRec.pcRecoverFile);
		}
	}

	/* everything looks good */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The BuildERR routine                                                       */
/******************************************************************************/
static TERR	*BuildERR(char *pcpErrorCode)
{
	int				ilRC;
	long				lMin;
	long				lMax;
	long				lUniqueNumber;
	static TERR		rlERR;
	
	/* clear memory */
	memset ((void*)&rlERR, 0x00, iTERR_LENGTH);
	
	/* get next uniquw number */
	if ((ilRC = GetUniqueNumber(pcgServerName, pcgCTLName, "MAXOW", 
									1, &lUniqueNumber, &lMax, &lMin)) != RC_SUCCESS)
	{
		/* fatal error */
		dbg(TRACE,"<BuildERR> GetUniquNumber returns: %d", ilRC);

		/* reset flag... */
		igStatus &= ~iF_TCPIP_CLIENT_SOCK;
		
		/* exit process */
		Terminate(0);
	}
	
	/* set unique number */
	lUniqueNumber = UNIQUE(lUniqueNumber, lMax, lMin);	

	/* build header for command */
	BuildHeader(&rlERR.rTH, sCONTROL_CLASS, (ULONG)lUniqueNumber);

	/* set ERR specifics (Length) */
	sprintf(rlERR.rTH.pcLength, "%-4.4d", (iTERR_LENGTH-iTHEADER_LENGTH));			
	/* Command ERR */
	memmove((void*)rlERR.pcCommand, "ERR", (size_t)iCOMMAND_SIZE);

	/* ErrorCode for ERR */
	memmove((void*)rlERR.pcErrorCode, pcpErrorCode, (size_t)iERRORCODE_SIZE);

	/* everything is fine */
	return &rlERR;
}

/******************************************************************************/
/* The ToRProtFile routine                                                    */
/******************************************************************************/
static void ToRProtFile(UINT ipLen)
{
	int		ilRC;

	if (rgTM.iUseRProt)
	{
		if (igStatus & iF_RPROT_FILE)
		{
			if ((ilRC = write(igRProtFile, pcgReceiveData, ipLen)) != ipLen)
			{
				dbg(TRACE, "<ToRPF> can't write received data to file (%d)", ilRC);
			}
			else
			{
				/* write \n */
				write (igRProtFile, "\n", 1);
			}
		}
	}
	return;
}

/******************************************************************************/
/* The ToSProtFile routine                                                    */
/******************************************************************************/
static void ToSProtFile(char *pcpData, UINT	ipLen)
{
	int		ilRC;

	if (rgTM.iUseSProt)
	{
		if (igStatus & iF_SPROT_FILE)
		{
			if ((ilRC = write(igSProtFile, pcpData, ipLen)) != ipLen)
			{
				dbg(TRACE, "<ToSPF> can't write send data to file (%d)", ilRC);
			}
			else
			{
				/* write \n */
				write (igSProtFile, "\n", 1);
			}
		}
	}
	return;
}

/******************************************************************************/
/* The WriteERRToClient routine                                               */
/******************************************************************************/
static int WriteERRToClient(char *pcpErrCode)
{
	int		ilRC;
	char		pclTmpBuf[iMIN_BUF_SIZE];
	TERR		*prlERR = NULL;

	/* init something */
	ilRC = RC_SUCCESS;

	/* in this case send an ERR-Command to client... */
	if ((prlERR = BuildERR(pcpErrCode)) == NULL)
	{
		dbg(DEBUG,"<WERRToC> cannot create error command"); 
	}
	else
	{
		dbg(DEBUG,"<WERRToC> calling send_message...");
		memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
		if (!strcmp(pcpErrCode, "WY"))
			strcpy(pclTmpBuf, "WRONG_SERVERSYSTEM");
		else if (!strcmp(pcpErrCode, "US"))
			strcpy(pclTmpBuf, "UNKNOWN_SENDER");
		else if (!strcmp(pcpErrCode, "UR"))
			strcpy(pclTmpBuf, "UNKNOWN_RECEIVER");
		else if (!strcmp(pcpErrCode, "TO"))
			strcpy(pclTmpBuf, "TIMEOUT");
		else if (!strcmp(pcpErrCode, "WC"))
			strcpy(pclTmpBuf, "WRONG_COMMAND");
		else if (!strcmp(pcpErrCode, "UC"))
			strcpy(pclTmpBuf, "UNKNOWN_CLASS");
		else if (!strcmp(pcpErrCode, "WT"))
			strcpy(pclTmpBuf, "WRONG_TIMESTAMP");
		else if (!strcmp(pcpErrCode, "WL"))
			strcpy(pclTmpBuf, "WRONG_LENGTH");
		else if (!strcmp(pcpErrCode, "WN"))
			strcpy(pclTmpBuf, "WRONG_NUMBER");
		else if (!strcmp(pcpErrCode, "CN"))
			strcpy(pclTmpBuf, "CCO_TNUMBER");
		else if (!strcmp(pcpErrCode, "AC"))
			strcpy(pclTmpBuf, "AC_ERROR");
		else if (!strcmp(pcpErrCode, "ND"))
			strcpy(pclTmpBuf, "NO_DATA");
		else if (!strcmp(pcpErrCode, "WS"))
			strcpy(pclTmpBuf, "WRONG_SUBCOMMAND");
		else if (!strcmp(pcpErrCode, "SM"))
			strcpy(pclTmpBuf, "SEPARATOR_MISSING");
		else if (!strcmp(pcpErrCode, "WD"))
			strcpy(pclTmpBuf, "WRONG_DATA");
		else if (!strcmp(pcpErrCode, "UF"))
			strcpy(pclTmpBuf, "UNKNOWN_FIELDNAME");
		else if (!strcmp(pcpErrCode, "WE"))
			strcpy(pclTmpBuf, "WRONG_ELEMENTS");
		else if (!strcmp(pcpErrCode, "IC"))
			strcpy(pclTmpBuf, "INVALID_CMD");
		else if (!strcmp(pcpErrCode, "OF"))
			strcpy(pclTmpBuf, "WRONG_NUMBER_OF_FIELDS");
		else if (!strcmp(pcpErrCode, "IE"))
			strcpy(pclTmpBuf, "INTERNAL_ERROR");
		else
			strcpy(pclTmpBuf, "UNKNOWN_ERROR");
		
		dbg(DEBUG,"<WERRToC> buffer for send_message: <%s>", pclTmpBuf);
		if ((ilRC = send_message(IPRIO_DB, iWERR_TO_CLIENT, 0, 0, pclTmpBuf)) != RC_SUCCESS)
		{
			dbg(DEBUG,"<CTCPIPC> send_message returns: %d", ilRC);
		}

		/* write it to send protocol file */
		ToSProtFile((char*)prlERR, iTERR_LENGTH);

		/* write it to socket */
		if ((ilRC = write(igClientSocket, prlERR, iTERR_LENGTH)) != iTERR_LENGTH)
		{
			dbg(TRACE,"<WERRToC> write error - can't write ERR-command (%d)", ilRC);
			ilRC = RC_FAIL;
		}
		else
			ilRC = RC_SUCCESS;
	}

	if (rgTM.iMaxFailCmds > 0)
	{
		/* set fail-structure */
		rgFail.iSendErrorCounter = INCR(rgFail.iSendErrorCounter);

		/* check error counter */
		if (rgFail.iSendErrorCounter > rgTM.iMaxFailCmds)
		{
			dbg(TRACE,"<WERRToC> ErrorCounter: %d and MaxFailCmds: %d",
							rgFail.iSendErrorCounter, rgTM.iMaxFailCmds);
			Terminate(0);
		}
	}

	/* bye bye */
	return ilRC;
}

/******************************************************************************/
/* The BuildACK routine                                                       */
/******************************************************************************/
static TACK *BuildACK(void)
{
	int				ilRC;
	long				lMin;
	long				lMax;
	long				lUniqueNumber;
	static TACK		rlAck;
	
	/* answer check connection call */
	memset((void*)&rlAck, 0x00, iTACK_LENGTH);
	
	/* get next uniquw number */
	if ((ilRC = GetUniqueNumber(pcgServerName, pcgCTLName, "MAXOW", 
								1, &lUniqueNumber, &lMax, &lMin)) != RC_SUCCESS)
	{
		/* fatal error */
		dbg(TRACE,"<BuildACK> GetUniquNumber returns: %d", ilRC);

		/* reset flag... */
		igStatus &= ~iF_TCPIP_CLIENT_SOCK;
		
		/* exit process */
		Terminate(0);
	}
	
	/* set unique number */
	lUniqueNumber = UNIQUE(lUniqueNumber, lMax, lMin);	

	/* Build Header */
	BuildHeader(&rlAck.rTH, sCONTROL_CLASS, (ULONG)lUniqueNumber);

	/* set CCO specifics (Length) */
	sprintf(rlAck.rTH.pcLength, "%-4.4d", (iTACK_LENGTH-iTHEADER_LENGTH));			
	/* Command CCO */
	memmove((void*)rlAck.pcCommand, (void*)"ACK", (size_t)iCOMMAND_SIZE);

	/* everything is fine */
	return &rlAck;
}

/******************************************************************************/
/* The WriteDataTelegramToClinet routine                                      */
/******************************************************************************/
static int WriteDataTelegramToClient(char *pcpCommand, int ipCmdNo, int ipTabNo,
		char *pcpData, char *pcpFields, int ipNoFields, ULONG lpUniqueNumber)
{
	int			i;
	int			ilRC;
	int			ilUI;
	int			ilVianPos;
	int			ilVialPos;
	int			ilNoOfVias;
	int			ilCurMapNo;
	int			ilNoOfBytes;
	int			ilFoundData;
	int			ilOtherData;
	int			ilNoOfAllVias;
	int			ilUrnoPosition;
	UINT			ilIdx;
	UINT			ilInIdx;
	UINT			ilNoOfFields;
	ULONG			llBytes;
	ULONG			llDataLength;
	char			pclFixPosBuf[iMAXIMUM];
	char			pclTmpBuf[iMIN_BUF_SIZE];
	char			pclVial[iMAX_BUF_SIZE+1];
	char			pclFieldDataBuffer[iMAXIMUM];
	char			pclInternalFields[iMAXIMUM];
	char			pclInternalData[2*iMAXIMUM];
	char			*pclTmpPtr	= NULL;
	char			*pclS			= NULL;
	char			*pclPtr		= NULL;
	FILE			*fh			= NULL;
	TRecover		rlRecArea;
	int 			blHeaderIslBuilt;

	dbg(DEBUG,"<WDtC> ---------- START -----------");

	/* initialize this */
	ilRC = llBytes = llDataLength = ilNoOfVias = 0;

	/* check VIA's */
	if (rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].iViaInFieldList)
	{
		/* get VIAN and VIAL */
		if ((ilVianPos = GetIndex(pcpFields, "VIAN", cSEPARATOR)) >= 0)
		{
			/* this is VIAN */
			if ((pclPtr = GetDataField(pcpData, ilVianPos, cSEPARATOR)) != NULL)
			{
				/* convert number of via's to int */
				ilNoOfVias = atoi(pclPtr);
			}
		}

		if (ilNoOfVias > 0)
		{
			/* this is Vial */
			memset((void*)pclVial, 0x00, iMAX_BUF_SIZE+1);
			if ((ilVialPos = GetIndex(pcpFields, "VIAL", cSEPARATOR)) < 0)
				ilNoOfVias = 0;
			else
			{
				/* data for vial */
				if ((pclPtr = GetDataField(pcpData, ilVialPos, cSEPARATOR)) != NULL)
				{
					/* save Vial */
					strcpy(pclVial, pclPtr);
					dbg(DEBUG,"VIA LIST IS <%s>", pclVial);

					/* count vias... */
					for (i=0, ilRC=0, pclS=pclVial; i<ilNoOfVias; i++, pclS+=120)
					{
						if (*pclS != ' ')
							ilRC++;
					}
				} 
			}
		}
	}

	/* set correct counter... */
	ilNoOfAllVias = ilNoOfVias;
	ilNoOfVias = ilRC;

	/* here we know exactly number of vias */
	dbg(DEBUG,"<WDtC> found ilNoOfVias: %d and ilNoOfAllVias: %d", ilNoOfVias, ilNoOfAllVias);

	/* build new (internal) fieldlist */
	memset((void*)pclInternalFields, 0x00, iMAXIMUM);
	memset((void*)pclInternalData, 0x00, 2*iMAXIMUM);
	for (i=0; i<ipNoFields; i++)
	{
		/* get n.th field name */
		if ((pclS = GetDataField(pcpFields, i, cSEPARATOR)) == NULL)
		{
			dbg(TRACE,"<WDtC> Error creating internal field list...");
			Terminate(0);
		}

		/* is this VIAx ? */
		if (strstr(pclS, "VIA") == NULL)
		{
			/* no, so use it... */
			strcat(pclInternalFields, pclS);
			strcat(pclInternalFields, ",");

			/* now the data of this field */
			if ((pclS = GetDataField(pcpData, i, cSEPARATOR)) == NULL)
			{
				dbg(TRACE,"<WDtC> Error creating internal data list...");
				Terminate(0);
			}

			/* copy data to internal buffer */
			strcat(pclInternalData, pclS);
			strcat(pclInternalData, ",");
		}
	}

	/* delete all unnecassary commas */
	while (pclInternalFields[strlen(pclInternalFields)-1] == ',' &&
			 pclInternalData[strlen(pclInternalData)-1] == ',')
	{
		pclInternalFields[strlen(pclInternalFields)-1] = 0x00;
		pclInternalData[strlen(pclInternalData)-1] = 0x00;
	}

	/* if command is SFS, add always VIAs... */
	if (!strcmp(rgTM.prCmd[ipCmdNo].pcCommand, "SFS") && 
		 !strncmp(rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].pcTabName, "AFT", 3))
	{
		strcat(pclInternalFields,",VIA1,VIA2");
	}
	else
	{
		/* here we add via1, via2, viaX... */
		if (ilNoOfVias == 1)
		{
			strcat(pclInternalFields,",VIA1");
		}
		else if (ilNoOfVias == 2)
		{
			strcat(pclInternalFields,",VIA1,VIA2");
		}
	}

	/* this is out new fieldlist */
	dbg(DEBUG,"<WDtC> new Internal Fieldlist: <%s>", pclInternalFields);
	dbg(DEBUG,"<WDtC> new Internal Datalist: <%s>", pclInternalData);
	
	/* set pointer to internal Data */
	pclTmpPtr = pclInternalData;

	/* set new number of fields */
	ilNoOfFields = GetNoOfElements(pclInternalFields, cSEPARATOR);
	dbg(DEBUG,"<WDtC> there must be %d field...", ilNoOfFields);

	/* get URNO-Position */
	if ((ilUrnoPosition = GetIndex(pclInternalFields, "URNO", cSEPARATOR)) < 0)
	{
		/* can't work without URNO */
		dbg(TRACE,"<WDtC> Cannot find URNO in fieldlist..., can't work");
		/* Modification of ATH */
		/* Due to events from roghdl without URNOs */
		/* Terminate(0); */
		return RC_SUCCESS;
	}
	dbg(DEBUG,"<WDtC> found URNO at position: %d", ilUrnoPosition);

	/* data header not build yet */
	blHeaderIslBuilt = FALSE; 
	/* combine data and fields */
	for (ilIdx=0; ilIdx<ilNoOfFields; ilIdx++)
	{
		/* get n.th field of received field list */
		if ((pclPtr = GetDataField(pclInternalFields, ilIdx, cSEPARATOR)) == NULL)
		{
			dbg(TRACE,"<WDtC> GetDataField (pclInternalField) returns NULL");
			Terminate(0);
		}

		/* calculate position of this field in field-list */
		for (ilInIdx=0, ilUI=-1; ilInIdx<rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].iNoFields; ilInIdx++)
		{
			if (!strcmp(pclPtr, rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].pcFields[ilInIdx]))
			{
				/* found it... */
				ilUI = ilInIdx;
				break;
			}
		}

		/* check index */
		if (ilUI < 0)
		{
			dbg(DEBUG,"<WDtC> cannot find field <%s> in internal stucture",pclPtr);
			continue;
		}

		/* get URNO */
		/*if (!ilIdx)*/
	  if (blHeaderIslBuilt == FALSE )
		{
			if ((pclPtr = GetDataField(pclInternalData, ilUrnoPosition, cSEPARATOR)) != NULL)
			{
				/* set data for header etc... */
				BuildData(rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].pcDataType, pclPtr, sDATA_CLASS, lpUniqueNumber);
        blHeaderIslBuilt = TRUE;
			}
			else
			{
				/* error in searching field */
				dbg(TRACE, "<WDtC> GetDataField (URNO) returns NULL");

				/* don't know what to do in this case ... */
				Terminate(0);
			}
		}

		/* fieldname to buffer */
		strcat(prgData->pcData, rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].pcFields[ilUI]);	 					

		/* Data Separator */
		strcat(prgData->pcData, sDATA_SEPARATOR);

		/* Data from DB */
		if (rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].iViaInFieldList && (strstr(rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].pcFields[ilUI], "VIA") != NULL))
		{
			/* extract VIAx from VIAL */
			/* get last character, it's like via-number we must extract */
			if (ilNoOfVias > 0)
			{
				/* add 3- or 4-letter code */
				pclPtr = pclVial;
				pclS = rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].pcFields[ilUI];
				pclS += strlen(pclS) - 1;
				dbg(DEBUG,"<WDtC> looking for VIA%c", *pclS);
				dbg(DEBUG,"<WDtC> VIAL: <%s>", pclVial);

				for (i=0; i<ilNoOfAllVias; i++)
				{
					dbg(DEBUG,"<WDtC> compare %c and %c", *pclPtr, *pclS);
					if (*pclPtr == *pclS)
					{
						if (rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].iDataLengthCounter > 0)
						{
							/* must write a dynamic format */
							memset((void*)pclFixPosBuf, iBLANK, iMAXIMUM);
							
							if (rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].iVia34 == 3)
							{
								dbg(DEBUG,"<WDtC> use 3-letter-code");
								memmove((void*)pclFixPosBuf, (const void*)(pclPtr+1), 3);
							}
							else
							{
								dbg(DEBUG,"<WDtC> use 4-letter-code");
								memmove((void*)pclFixPosBuf, (const void*)(pclPtr+4), 4);
							}
							strncat(prgData->pcData, pclFixPosBuf, (int)rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].piDataLength[ilUI]);
						}
						else
						{
							/* normally -> dynamiy not fix */
							if (rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].iVia34 == 3)
							{
								/* copy 3-letter code to buffer */
								dbg(DEBUG,"<WDtC> use 3-letter-code");
								strncat(prgData->pcData, pclPtr+1, 3); 
							}
							else
							{
								/* copy 4-letter code to buffer */
								dbg(DEBUG,"<WDtC> use 4-letter-code");
								strncat(prgData->pcData, pclPtr+4, 4); 
							}
						}

						/* break here */
						i = ilNoOfAllVias;
					}
					else
					{
						pclPtr += 120;
					}
				}
			}
		}
		else
		{
			/* get next data field... */
			memset((void*)pclFieldDataBuffer, 0x00, iMAXIMUM);
			if ((pclS = GetDataField(pclTmpPtr, ilIdx, cSEPARATOR)) != NULL)
			{
				/* store data */
				strcpy(pclFieldDataBuffer, pclS);

				/* here we handle data mapping... */
				for (ilCurMapNo=0, ilFoundData=iNOT_FOUND, ilOtherData=-1; ilCurMapNo<rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].piNoMapData[ilUI] && ilFoundData == iNOT_FOUND; ilCurMapNo++)
				{
					/* set other data flag */
					ilOtherData = rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].prDataMap[ilUI][ilCurMapNo].iOtherData;

					/* compare original data here */
					if (!strcmp(pclFieldDataBuffer, rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].prDataMap[ilUI][ilCurMapNo].pcOrgData))
					{
						/* found it... */
						strcpy(pclFieldDataBuffer, rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].prDataMap[ilUI][ilCurMapNo].pcNewData);
						ilFoundData = iFOUND;
					}
				}

				/* check returncode */
				if (ilFoundData == iNOT_FOUND && ilOtherData == iNULL_DATA)
				{
					dbg(DEBUG,"<WDtC> delete pclFieldDataBuffer now...");
					memset((void*)pclFieldDataBuffer, 0x00, iMAXIMUM);
				}

				/* write a fix format */
				if (rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].iDataLengthCounter > 0)
				{
					memset((void*)pclFixPosBuf, iBLANK, iMAXIMUM);
					memmove((void*)pclFixPosBuf, (const void*)pclFieldDataBuffer, (size_t)strlen(pclFieldDataBuffer));
					strncat(prgData->pcData, pclFixPosBuf, (int)rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].piDataLength[ilUI]);
				}
				else
				{
					/* this is a (normal) dynamic format */
					strcat(prgData->pcData, pclFieldDataBuffer); 
				}
			}
			else
			{
				/* we terminate here */
				dbg(TRACE, "<WDtC> GetDataField returns NULL");
				Terminate(0);
			}
		}

		/* Data Separator */
		strcat(prgData->pcData, sDATA_SEPARATOR);
	}

	/* calculate number of bytes */
	llDataLength = (ULONG)strlen(prgData->pcData);
	llBytes = (ULONG)iTDATA_LENGTH + llDataLength - 1;

	/* should i use FTP? */
	dbg(DEBUG,"<WDtC> COMMAND IS: <%s>", pcpCommand);
	if ((rgTM.iUseFtp 						&& 
	    (llBytes>=(ULONG)rgTM.iFtpSize) && 
		 strcmp(pcpCommand, "UFR")) 		|| 
		 (rgTM.iUseFtpForUpdate 			&& 
		 !strcmp(pcpCommand, "UFR")))
	{
		/* use FTP, not datatelegram */
		/* produce filename */
		if (!rgTM.prCmd[ipCmdNo].iOpenDataFile)
		{
			/* clear temporary buffer */
			memset((void*)rgTM.prCmd[ipCmdNo].pcTotalFileName, 
																0x00, iMIN_BUF_SIZE);
			memset((void*)rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath, 
																0x00, iMIN_BUF_SIZE);
				
			/* copy path for file */
			strcpy(rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath, rgTM.rFtp.pcFtpFilePath);

			/* is last character '/'? */
			if (rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath[strlen(rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath)-1] != '/')
				strcat(rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath,"/");
		
			/* filename format is: CDI-Timestamp-.DAT, CDI120493.DAT */
			strcat(rgTM.prCmd[ipCmdNo].pcTotalFileName, 
					 rgTM.prCmd[ipCmdNo].pcFileName);
			strcat(rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath, 
					 rgTM.prCmd[ipCmdNo].pcFileName);

			/* timestamp? */
			if (rgTM.prCmd[ipCmdNo].iFilenameWithTimestamp)
			{
				pclPtr = GetTimeStamp();
				strcat(rgTM.prCmd[ipCmdNo].pcTotalFileName, &pclPtr[8]);
				strcat(rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath, &pclPtr[8]);
			}

			/* extension */
			strcat(rgTM.prCmd[ipCmdNo].pcTotalFileName, 
					 rgTM.prCmd[ipCmdNo].pcFileExtension);
			strcat(rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath, 
					 rgTM.prCmd[ipCmdNo].pcFileExtension);

			/* try to open file */
			dbg(DEBUG,"<OpenFile> <%s>", 
					rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath);
			if ((rgTM.prCmd[ipCmdNo].FtpFH = fopen(rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath, "w")) == NULL)
			{
				/* cannot open file */
				dbg(TRACE,"<WDtC> can't open file %s", 
							rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath);
				
				/* don't know what to do in this case ... */
				Terminate(0);
			}
			else
			{
				/* first set flag in status word */
				rgTM.prCmd[ipCmdNo].iOpenDataFile = 1;
			}

			/* open control-file */
			if ((fh = fopen(rgTM.rFtp.pcFtpCtlFile,"w")) == NULL)
			{
				dbg(TRACE,"<WDtC> cannot open file %s", rgTM.rFtp.pcFtpCtlFile);
				Terminate(0);
			}
			else
			{
				/* for login at machine ... */
				fprintf(fh,"user %s %s\n", rgTM.rFtp.pcUser, rgTM.rFtp.pcPass);
				fprintf(fh,"cd %s\n", rgTM.rFtp.pcMachinePath);
				fprintf(fh,"put %s %s\n", rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath, rgTM.prCmd[ipCmdNo].pcTotalFileName);
				fprintf(fh,"close\n");
				fprintf(fh,"quit\n");

				/* close file now */
				fclose(fh);
			}
		}

		if (rgTM.prCmd[ipCmdNo].iOpenDataFile)
		{
			/* now write data to file */
			/* set length of datatelegram */
			pclTmpBuf[0] = 0x00;
			sprintf(pclTmpBuf, "%-4.4d", (UINT)(llBytes - (ULONG)iTHEADER_LENGTH));
			memmove((void*)prgData->rTH.pcLength,
					  (const void*)pclTmpBuf, iLENGTH_SIZE);

			/* now write the data to the client */
			/* timestamp */
			pclTmpBuf[0] = 0x00;
			sprintf(pclTmpBuf, "%-14.14s", prgData->pcTimeStamp);
			if (fwrite((void*)pclTmpBuf, (size_t)1, (size_t)iTIMESTAMP_SIZE, rgTM.prCmd[ipCmdNo].FtpFH) != (size_t)iTIMESTAMP_SIZE)
			{
				dbg(DEBUG,"<WDtC> can't write timestamp to file");
				return iWRITE_FAIL;
			}

			/* origin */
			pclTmpBuf[0] = 0x00;
			sprintf(pclTmpBuf, "%-3.3s", prgData->pcOrigin);
			if (fwrite((void*)pclTmpBuf, (size_t)1, (size_t)iORIGIN_SIZE, rgTM.prCmd[ipCmdNo].FtpFH) != (size_t)iORIGIN_SIZE)
			{
				dbg(DEBUG,"<WDtC> can't write origin to file");
				return iWRITE_FAIL;
			}

			/* type */
			pclTmpBuf[0] = 0x00;
			sprintf(pclTmpBuf, "%-2.2s", prgData->pcType);
			if (fwrite((void*)pclTmpBuf, (size_t)1, (size_t)iTYPE_SIZE, rgTM.prCmd[ipCmdNo].FtpFH) != (size_t)iTYPE_SIZE)
			{
				dbg(DEBUG,"<WDtC> can't write type to file");
				return iWRITE_FAIL;
			}

			/* key */
			pclTmpBuf[0] = 0x00;
			sprintf(pclTmpBuf, "%-25.25s", prgData->pcKey);
			if (fwrite((void*)pclTmpBuf, (size_t)1, (size_t)iKEY_SIZE, rgTM.prCmd[ipCmdNo].FtpFH) != (size_t)iKEY_SIZE)
			{
				dbg(DEBUG,"<WDtC> can't write key to file");
				return iWRITE_FAIL;
			}

			/* field separator */
			if (fwrite((void*)prgData->pcSeparator, (size_t)1, (size_t)iSEPARATOR_SIZE, rgTM.prCmd[ipCmdNo].FtpFH) != (size_t)iSEPARATOR_SIZE)
			{
				dbg(DEBUG,"<WDtC> can't write separator to file");
				return iWRITE_FAIL;
			}

			/* data */
			if (fwrite((void*)prgData->pcData, (size_t)1, (size_t)llDataLength, rgTM.prCmd[ipCmdNo].FtpFH) != (size_t)llDataLength)
			{
				dbg(DEBUG,"<WDtC> can't write separator to file");
				return iWRITE_FAIL;
			}

			/* store data to recovery area */
			rlRecArea.iCmdNo  = ipCmdNo;
			rlRecArea.iTabNo  = ipTabNo;
			rlRecArea.lLength = llBytes;
			memcpy((void*)rlRecArea.pcTimeStamp, 
					 (const void*)prgData->rTH.pcTimeStamp, iTIMESTAMP_SIZE);
			memcpy((void*)rlRecArea.pcTeleNo, 
					 (const void*)prgData->rTH.pcTNumber, iTNUMBER_SIZE);
			memcpy((void*)rlRecArea.pcData, 
					 (const void*)prgData, iMAX_RECEIVE_DATA);

			if ((ilRC = (*rgTM.rRec.RHandler[rgTM.rRec.iRecoverType])(iADD_CMD, NULL, &rlRecArea, NULL)) != RC_SUCCESS)
			{
				dbg(DEBUG,"<WDtC> recover handler returns: %d", ilRC);
			}

			/* \n */
			if (fwrite((void*)"\n", (size_t)1, (size_t)1, rgTM.prCmd[ipCmdNo].FtpFH) != (size_t)1)
			{
				dbg(DEBUG,"<WDtC> can't write \n to file");
				return iWRITE_FAIL;
			}
		}
	}
	else
	{
		/* use datatelegram */
		if (llBytes > rgTM.rTCP.iMaxBytes)
		{
			/* write data with separate packages */
			/* length we can handle (max. 9999 Bytes) */	
			while ((ilNoOfBytes = GetMaxLength((rgTM.rTCP.iMaxBytes-iTDATA_LENGTH), cDATA_SEPARATOR, prgData->pcData)) > 0)
			{
				/* set length of datatelegram */
				pclTmpBuf[0] = 0x00;
				sprintf(pclTmpBuf, "%-4.4d", 
							(UINT)(ilNoOfBytes + iTDATA_LENGTH - iTHEADER_LENGTH - 1));		
				memmove((void*)prgData->rTH.pcLength,
						  (const void*)pclTmpBuf, iLENGTH_SIZE);

				/* write it to send protocol file */
				ToSProtFile((char*)prgData, ilNoOfBytes+iTDATA_LENGTH-1);

				/* store data to recovery area */
				rlRecArea.iCmdNo  = ipCmdNo;
				rlRecArea.iTabNo  = ipTabNo;
				rlRecArea.lLength = llBytes;
				memcpy((void*)rlRecArea.pcTimeStamp, 
						 (const void*)prgData->rTH.pcTimeStamp, iTIMESTAMP_SIZE);
				memcpy((void*)rlRecArea.pcTeleNo, 
						 (const void*)prgData->rTH.pcTNumber, iTNUMBER_SIZE);
				memcpy((void*)rlRecArea.pcData, 
						 (const void*)prgData, iMAX_RECEIVE_DATA);

				if ((ilRC = (*rgTM.rRec.RHandler[rgTM.rRec.iRecoverType])(iADD_CMD, NULL, &rlRecArea, NULL)) != RC_SUCCESS)
				{
					dbg(DEBUG,"<WDtC> recover handler returns: %d", ilRC);
				}

				/* write the data */
				if ((ilRC = write(igClientSocket, prgData, ilNoOfBytes+iTDATA_LENGTH-1)) != ilNoOfBytes+iTDATA_LENGTH-1)
				{
					/* cannot write to client socket */
					dbg(TRACE,"<WDtC> write error - cannot write to client socket");
					return iWRITE_FAIL;
				}

				/* move the data */
				memmove((void*)prgData->pcData,
						  (const void*)&prgData->pcData[ilNoOfBytes],
						  (size_t)((int)llDataLength-ilNoOfBytes));

				/* clear rest of data */
				memset((void*)&prgData->pcData[(int)llDataLength-ilNoOfBytes],
						 0x00, ilNoOfBytes);

				/* set new length */
				llDataLength = (ULONG)strlen(prgData->pcData);
			}
		}
		else
		{
			/* set length in structure */
			pclTmpBuf[0] = 0x00;
			sprintf(pclTmpBuf,"%-4.4d", (UINT)(llBytes-(ULONG)iTHEADER_LENGTH));
			memmove((void*)prgData->rTH.pcLength,
					  (const void*)pclTmpBuf, iLENGTH_SIZE);

			/* write it to send protocol file */
			ToSProtFile((char*)prgData, (UINT)llBytes);

			/* store data to recovery area */
			rlRecArea.iCmdNo  = ipCmdNo;
			rlRecArea.iTabNo  = ipTabNo;
			rlRecArea.lLength = llBytes;
			memcpy((void*)rlRecArea.pcTimeStamp, 
					 (const void*)prgData->rTH.pcTimeStamp, iTIMESTAMP_SIZE);
			memcpy((void*)rlRecArea.pcTeleNo, 
					 (const void*)prgData->rTH.pcTNumber, iTNUMBER_SIZE);
			memcpy((void*)rlRecArea.pcData, 
					 (const void*)prgData, iMAX_RECEIVE_DATA);

			if ((ilRC = (*rgTM.rRec.RHandler[rgTM.rRec.iRecoverType])(iADD_CMD, NULL, &rlRecArea, NULL)) != RC_SUCCESS)
			{
				dbg(DEBUG,"<WDtC> recover handler returns: %d", ilRC);
			}

			/* now write to client socket */
			if ((ilRC = write(igClientSocket, prgData, (int)llBytes)) != (int)llBytes)
			{
				dbg(TRACE,"<WDtC> write error - can't write data to client socket %d", ilRC);
				return iWRITE_FAIL;
			}
		}
	}

	/* clear data area */
	memset((void*)prgData->pcData, 0x00, llDataLength);

	dbg(DEBUG,"<WDtC> ---------- END -----------");
	/* everything looks good */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The WriteMixTelegramToClinet routine                                       */
/******************************************************************************/
static int WriteMixTelegramToClient(int ipCmdNo, int ipTabNo, char *pcpData, 
								char *pcpFields, int ipNoFields, ULONG lpUniqueNumber)
{
	int			i;
	int			ilRC;
	int			ilUI;
	int			ilVianPos;
	int			ilVialPos;
	int			ilCurMapNo;
	int			ilNoOfVias;
	int			ilNoOfBytes;
	int			ilFoundData;
	int			ilOtherData;
	int			ilNoOfFields;
	int			ilNoOfAllVias;
	UINT			ilIdx;
	UINT			ilInIdx;
	ULONG			llBytes;
	ULONG			llDataLength;
	char			pclFieldDataBuffer[iMAXIMUM];
	char			pclFixPosBuf[iMAXIMUM];
	char			pclTmpBuf[iMIN_BUF_SIZE];
	char			pclVial[iMAX_BUF_SIZE+1];
	char			pclInternalFields[iMAXIMUM];
	char			pclInternalData[2*iMAXIMUM];
	char			pclLocalTimeBuffer[15];
	char			*pclTmpPtr 	= NULL;
	char			*pclPtr		= NULL;
	char			*pclS			= NULL;
	TRecover		rlRecArea;

	dbg(DEBUG,"<WMtC> ---------- START -----------");

	/* initialize this */
	ilRC = llBytes = llDataLength = ilNoOfVias = 0;

	/* check VIA's */
	if (rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].iViaInFieldList)
	{
		/* get VIAN and VIAL */
		if ((ilVianPos = GetIndex(pcpFields, "VIAN", cSEPARATOR)) >= 0)
		{
			/* this is VIAN */
			if ((pclPtr = GetDataField(pcpData, ilVianPos, cSEPARATOR)) != NULL)
			{
				/* convert number of via's to int */
				ilNoOfVias = atoi(pclPtr);
			}
		}

		if (ilNoOfVias > 0)
		{
			/* this is Vial */
			memset((void*)pclVial, 0x00, iMAX_BUF_SIZE+1);
			if ((ilVialPos = GetIndex(pcpFields, "VIAL", cSEPARATOR)) < 0)
				ilNoOfVias = 0;
			else
			{
				/* data for vial */
				if ((pclPtr = GetDataField(pcpData, ilVialPos, cSEPARATOR)) != NULL)
				{
					/* save Vial */
					strcpy(pclVial, pclPtr);

					/* count vias... */
					for (i=0, ilRC=0, pclS=pclVial; i<ilNoOfVias; i++, pclS+=120)
					{
						if (*pclS != ' ')
							ilRC++;
					}
				} 
			}
		}
	}

	/* set correct counter... */
	ilNoOfAllVias = ilNoOfVias;
	ilNoOfVias = ilRC;

	/* here we know exactly number of vias */
	dbg(DEBUG,"<WMtC> found ilNoOfVias: %d and ilNoOfAllVias: %d", ilNoOfVias, ilNoOfAllVias);

	/* build new (internal) fieldlist */
	memset((void*)pclInternalFields, 0x00, iMAXIMUM);
	memset((void*)pclInternalData, 0x00, 2*iMAXIMUM);
	for (i=0; i<ipNoFields; i++)
	{
		/* get n.th field name */
		if ((pclS = GetDataField(pcpFields, i, cSEPARATOR)) == NULL)
		{
			dbg(TRACE,"<WMtC> Error creating internal field list...");
			Terminate(0);
		}

		/* is this VIAx ? */
		if (strstr(pclS, "VIA") == NULL)
		{
			/* no, so use it... */
			strcat(pclInternalFields, pclS);
			strcat(pclInternalFields, ",");

			/* now the data of this field */
			if ((pclS = GetDataField(pcpData, i, cSEPARATOR)) == NULL)
			{
				dbg(TRACE,"<WMtC> Error creating internal data list...");
				Terminate(0);
			}

			/* copy data to internal buffer */
			strcat(pclInternalData, pclS);
			strcat(pclInternalData, ",");
		}
	}

	/* delete all unnecassary commas */
	while (pclInternalFields[strlen(pclInternalFields)-1] == ',' &&
			 pclInternalData[strlen(pclInternalData)-1] == ',')
	{
		pclInternalFields[strlen(pclInternalFields)-1] = 0x00;
		pclInternalData[strlen(pclInternalData)-1] = 0x00;
	}

	/* here we add via1, via2, viaX... */
	if (ilNoOfVias == 1)
	{
		strcat(pclInternalFields,",VIA1");
	}
	else if (ilNoOfVias == 2)
	{
		strcat(pclInternalFields,",VIA1,VIA2");
	}

	/* this is out new fieldlist */
	dbg(DEBUG,"<WMtC> new Internal Fieldlist: <%s>", pclInternalFields);
	dbg(DEBUG,"<WMtC> new Internal Datalist: <%s>", pclInternalData);
	
	/* set pointer to internal Data */
	pclTmpPtr = pclInternalData;

	/* set new number of fields */
	ilNoOfFields = GetNoOfElements(pclInternalFields, cSEPARATOR);
	dbg(DEBUG,"<WMtC> there must be %d field...", ilNoOfFields);

	/* for all fields... */
	for (ilIdx=0; ilIdx<ilNoOfFields; ilIdx++)
	{
		/* get n.th field of received field list */
		if ((pclPtr = GetDataField(pclInternalFields, ilIdx, cSEPARATOR)) == NULL)
		{
			dbg(TRACE,"<WMtC> GetDataField (FieldIdx) returns NULL");
			Terminate(0);
		}

		/* calculate position of this field in field-list */
		ilUI = -1;
		for (ilInIdx=0; ilInIdx<rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].iNoFields; ilInIdx++)
		{
			if (!strcmp(pclPtr, rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].pcFields[ilInIdx]))
			{
				/* found it... */
				ilUI = ilInIdx;
				break;
			}
		}

		/* check index */
		if (ilUI < 0)
		{
			dbg(DEBUG,"<WMtC> cannot find field <%s>...", pclPtr);
			continue;
		}

		/* get the first field (URNO) */
		if (!ilIdx)
		{
			/* set data for header etc... */
			BuildMix(rgTM.prCmd[ipCmdNo].pcCommand, sCONTROL_CLASS, lpUniqueNumber);
		}

		/* fieldname to buffer */
		strcat(prgMix->pcData, rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].pcFields[ilUI]);	 					

		/* Data Separator */
		strcat(prgMix->pcData, sDATA_SEPARATOR);

		/* Data from DB */
		if (rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].iViaInFieldList && (strstr(rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].pcFields[ilUI], "VIA") != NULL))
		{
			/* extract VIAx from VIAL */
			/* get last character, it's like via-number we must extract */
			if (ilNoOfVias > 0)
			{
				/* add 3-letter code */
				pclPtr = pclVial;
				pclS = rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].pcFields[ilUI];
				pclS += strlen(pclS) - 1;
				dbg(DEBUG,"<WMtC> looking for VIA%c", *pclS);
				dbg(DEBUG,"<WMtC> VIAL: <%s>", pclVial);

				for (i=0; i<ilNoOfAllVias; i++)
				{
					dbg(DEBUG,"<WMtC> compare %c and %c", *pclPtr, *pclS);
					if (*pclPtr == *pclS)
					{
						if (rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].iDataLengthCounter > 0)
						{
							/* must write a dynamic file */
							memset((void*)pclFixPosBuf, iBLANK, iMAXIMUM);
							if (rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].iVia34 == 3)
							{
								dbg(DEBUG,"<WMtC> use 3-letter-code");
								memmove((void*)pclFixPosBuf, (const void*)(pclPtr+1), 3);
							}
							else
							{
								dbg(DEBUG,"<WMtC> use 4-letter-code");
								memmove((void*)pclFixPosBuf, (const void*)(pclPtr+4), 4);
							}
							strncat(prgMix->pcData, pclFixPosBuf, (int)rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].piDataLength[ilUI]);
						}
						else
						{
							/* normally -> dynamiy not fix */
							if (rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].iVia34 == 3)
							{
								/* copy 3-letter code to buffer */
								dbg(DEBUG,"<WMtC> use 3-letter-code");
								strncat(prgMix->pcData, pclPtr+1, 3); 
							}
							else
							{
								/* copy 4-letter code to buffer */
								dbg(DEBUG,"<WMtC> use 4-letter-code");
								strncat(prgMix->pcData, pclPtr+4, 4); 
							}
						}

						/* break here */
						i = ilNoOfAllVias;
					}
					else
					{
						pclPtr += 120;
					}
				}
			}
		}
		else
		{
			/* get next data field... */
			memset((void*)pclFieldDataBuffer, 0x00, iMAXIMUM);
			if ((pclS = GetDataField(pclTmpPtr, ilIdx, cSEPARATOR)) != NULL)
			{
				/* store data */
				strcpy(pclFieldDataBuffer, pclS);

				/* for GFL and Andocksystem only */
				if (!strcmp(rgTM.prCmd[ipCmdNo].pcCommand, "GFL") ||
					 !strcmp(rgTM.prCmd[ipCmdNo].pcCommand, "AAN") ||
					 !strcmp(rgTM.prCmd[ipCmdNo].pcCommand, "APB") ||
					 !strcmp(rgTM.prCmd[ipCmdNo].pcCommand, "ABE") ||
					 !strcmp(rgTM.prCmd[ipCmdNo].pcCommand, "AFR") ||
					 !strcmp(rgTM.prCmd[ipCmdNo].pcCommand, "ABR"))
				{
					if (!strcmp(rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].pcFields[ilUI], "STOD") || !strcmp(rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].pcFields[ilUI], "ETOD") || !strcmp(rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].pcFields[ilUI], "TIME"))
					{
						dbg(DEBUG,"<WMtC> Command: <%s>, Field: <%s>", rgTM.prCmd[ipCmdNo].pcCommand, rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].pcFields[ilUI]);
						if (strlen(pclFieldDataBuffer) == 14)
						{
							strcpy(pclLocalTimeBuffer, pclFieldDataBuffer);
							dbg(DEBUG,"<WMtC> calling UtcToLocal(%s)", pclLocalTimeBuffer);
							if ((ilRC = UtcToLocal(pclLocalTimeBuffer)) != RC_SUCCESS)
							{
								dbg(DEBUG,"<WMtC> %05d UtcToLocal returns: %d", __LINE__, ilRC);
							}
							else
							{
								strcpy(pclFieldDataBuffer, pclLocalTimeBuffer);
							}
							dbg(DEBUG,"<WMtC> after UtcToLocal, <%s>", pclFieldDataBuffer);
						}
					}
				}

				/* here we handle data mapping... */
				for (ilCurMapNo=0, ilFoundData=iNOT_FOUND, ilOtherData=-1; ilCurMapNo<rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].piNoMapData[ilUI] && ilFoundData == iNOT_FOUND; ilCurMapNo++)
				{
					/* set other data flag */
					ilOtherData = rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].prDataMap[ilUI][ilCurMapNo].iOtherData;

					/* compare original data here */
					if (!strcmp(pclFieldDataBuffer, rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].prDataMap[ilUI][ilCurMapNo].pcOrgData))
					{
						/* found it... */
						strcpy(pclFieldDataBuffer, rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].prDataMap[ilUI][ilCurMapNo].pcNewData);
						ilFoundData = iFOUND;
					}
				}

				/* check returncode */
				if (ilFoundData == iNOT_FOUND && ilOtherData == iNULL_DATA)
				{
					dbg(DEBUG,"<WMtC> delete pclFieldDataBuffer now...");
					memset((void*)pclFieldDataBuffer, 0x00, iMAXIMUM);
				}

				/* write a fix format */
				if (rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].iDataLengthCounter > 0)
				{
					memset((void*)pclFixPosBuf, iBLANK, iMAXIMUM);
					memmove((void*)pclFixPosBuf, (const void*)pclFieldDataBuffer, (size_t)strlen(pclFieldDataBuffer));
					strncat(prgMix->pcData, pclFixPosBuf, (int)rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].piDataLength[ilUI]);
				}
				else
					/* this is a (normal) dynamic format */
					strcat(prgMix->pcData, pclFieldDataBuffer); 
			}
			else
			{
				/* we terminate here */
				dbg(TRACE, "<WMtC> GetDataField returns NULL");
				Terminate(0);
			}
		}

		/* Data Separator */
		strcat(prgMix->pcData, sDATA_SEPARATOR);
	}

	/* calculate number of bytes */
	llDataLength = (ULONG)strlen(prgMix->pcData);
	llBytes = (ULONG)iTMIX_LENGTH + llDataLength - 1;

	/* use datatelegram */
	if (llBytes > rgTM.rTCP.iMaxBytes)
	{
		/* write data with separate packages */
		/* length we can handle (max. 9999 Bytes) */	
		while ((ilNoOfBytes = GetMaxLength((rgTM.rTCP.iMaxBytes-iTMIX_LENGTH), cDATA_SEPARATOR, prgMix->pcData)) > 0)
		{
			/* set length of datatelegram */
			pclTmpBuf[0] = 0x00;
			sprintf(pclTmpBuf, "%-4.4d", 
						(UINT)(ilNoOfBytes + iTDATA_LENGTH - iTHEADER_LENGTH - 1));		
			memmove((void*)prgMix->rTH.pcLength,
					  (const void*)pclTmpBuf, iLENGTH_SIZE);

			/* write it to send protocol file */
			ToSProtFile((char*)prgMix, ilNoOfBytes+iTMIX_LENGTH-1);

			/* store data to recovery area */
			rlRecArea.iCmdNo  = ipCmdNo;
			rlRecArea.iTabNo  = ipTabNo;
			rlRecArea.lLength = llBytes;
			memcpy((void*)rlRecArea.pcTimeStamp, 
					 (const void*)prgMix->rTH.pcTimeStamp, iTIMESTAMP_SIZE);
			memcpy((void*)rlRecArea.pcTeleNo, 
					 (const void*)prgMix->rTH.pcTNumber, iTNUMBER_SIZE);
			memcpy((void*)rlRecArea.pcData, 
					 (const void*)prgMix, iMAX_RECEIVE_DATA);

			if ((ilRC = (*rgTM.rRec.RHandler[rgTM.rRec.iRecoverType])(iADD_CMD, NULL, &rlRecArea, NULL)) != RC_SUCCESS)
			{
				dbg(DEBUG,"<WMtC> recover handler returns: %d", ilRC);
			}

			/* write the data */
			if ((ilRC = write(igClientSocket, prgMix, ilNoOfBytes+iTMIX_LENGTH-1)) != ilNoOfBytes+iTMIX_LENGTH-1)
			{
				/* cannot write to client socket */
				dbg(TRACE,"<WMtC> write error - cannot write to client socket");
				return iWRITE_FAIL;
			}

			/* move the data */
			memmove((void*)prgMix->pcData,
					  (const void*)&prgMix->pcData[ilNoOfBytes],
					  (size_t)((int)llDataLength-ilNoOfBytes));

			/* clear rest of data */
			memset((void*)&prgMix->pcData[(int)llDataLength-ilNoOfBytes],
					 0x00, ilNoOfBytes);

			/* set new length */
			llDataLength = (ULONG)strlen(prgMix->pcData);
		}
	}
	else
	{
		/* set length in structure */
		pclTmpBuf[0] = 0x00;
		sprintf(pclTmpBuf,"%-4.4d", (UINT)(llBytes-(ULONG)iTHEADER_LENGTH));
		memmove((void*)prgMix->rTH.pcLength,
				  (const void*)pclTmpBuf, iLENGTH_SIZE);

		/* write it to send protocol file */
		ToSProtFile((char*)prgMix, (UINT)llBytes);

		/* store data to recovery area */
		rlRecArea.iCmdNo  = ipCmdNo;
		rlRecArea.iTabNo  = ipTabNo;
		rlRecArea.lLength = llBytes;
		memcpy((void*)rlRecArea.pcTimeStamp, 
				 (const void*)prgMix->rTH.pcTimeStamp, iTIMESTAMP_SIZE);
		memcpy((void*)rlRecArea.pcTeleNo, 
				 (const void*)prgMix->rTH.pcTNumber, iTNUMBER_SIZE);
		memcpy((void*)rlRecArea.pcData, 
				 (const void*)prgMix, iMAX_RECEIVE_DATA);

		if ((ilRC = (*rgTM.rRec.RHandler[rgTM.rRec.iRecoverType])(iADD_CMD, NULL, &rlRecArea, NULL)) != RC_SUCCESS)
		{
			dbg(DEBUG,"<WMtC> recover handler returns: %d", ilRC);
		}

		/* now write to client socket */
		if ((ilRC = write(igClientSocket, prgMix, (int)llBytes)) != (int)llBytes)
		{
			dbg(TRACE,"<WMtC> write error - can't write data to client socket %d", ilRC);
			return iWRITE_FAIL;
		}
	}

	/* clear data area */
	memset((void*)prgMix->pcData, 0x00, llDataLength);

	dbg(DEBUG,"<WMtC> ---------- END -----------");
	/* everything looks good */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The SendFileAndWriteEOD routine                                            */
/******************************************************************************/
static int SendFileAndWriteEOD(int ipCmdNo, char *pcpType, char *pcpCommand)
{
	int			ilRC;
	int			ilLength;
	int			ilErrorCode;
	long			lMin;
	long			lMax;
	long			lUniqueNumber;
	char			pclTmpFile[iMIN_BUF_SIZE];
	char			pclTmpBuf[iMIN_BUF_SIZE];
	char			pclTmpBuf1[iMIN_BUF_SIZE];
	char			*pclTmpPath = NULL;
	char			*pclPtr		= NULL;			
	TEOD			*prlEOD  	= NULL;
	FILE			*fh			= NULL;

	dbg(DEBUG,"<SFtM> ---------- START -----------");
	/* set error code to OK */
	ilErrorCode = 0;

	/* if file does not exist, create an empty file */
	if (!rgTM.prCmd[ipCmdNo].iOpenDataFile && ((rgTM.iUseFtp && strcmp(pcpCommand, "UFR")) || (rgTM.iUseFtpForUpdate && !strcmp(pcpCommand, "UFR"))))
	{
		/* clear temporary buffer */
		memset ((void*)rgTM.prCmd[ipCmdNo].pcTotalFileName, 
															0x00, iMIN_BUF_SIZE);
		memset ((void*)rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath, 
															0x00, iMIN_BUF_SIZE);
			
		/* copy path for file */
		strcpy(rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath, rgTM.rFtp.pcFtpFilePath);

		/* is last character '/'? */
		if (rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath[strlen(rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath)-1] != '/')
			strcat(rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath,"/");
	
		/* filename format is: CDI-Timestamp-.DAT, CDI120493.DAT */
		strcat(rgTM.prCmd[ipCmdNo].pcTotalFileName, 
				 rgTM.prCmd[ipCmdNo].pcFileName);
		strcat(rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath, 
				 rgTM.prCmd[ipCmdNo].pcFileName);

		/* timestamp? */
		if (rgTM.prCmd[ipCmdNo].iFilenameWithTimestamp)
		{
			pclPtr = GetTimeStamp();
			strcat(rgTM.prCmd[ipCmdNo].pcTotalFileName, &pclPtr[8]);
			strcat(rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath, &pclPtr[8]);
		}

		/* extension */
		strcat(rgTM.prCmd[ipCmdNo].pcTotalFileName, 
				 rgTM.prCmd[ipCmdNo].pcFileExtension);
		strcat(rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath, 
				 rgTM.prCmd[ipCmdNo].pcFileExtension);

		/* try to open file */
		dbg(DEBUG,"<SFaWD>: open file <%s>>", 
				rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath);
		if ((rgTM.prCmd[ipCmdNo].FtpFH = fopen(rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath, "w")) == NULL)
		{
			/* cannot open file */
			dbg(TRACE,"<SFaWD> can't open file %s", 
						rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath);
			
			/* don't know what to do in this case ... */
			Terminate(0);
		}
		else
		{
			/* first set flag in status word */
			rgTM.prCmd[ipCmdNo].iOpenDataFile = 1;
		}

		/* open control-file */
		if ((fh = fopen(rgTM.rFtp.pcFtpCtlFile,"w")) == NULL)
		{
			dbg(TRACE,"<SFaWD> cannot open file %s", rgTM.rFtp.pcFtpCtlFile);
			Terminate(0);
		}
		else
		{
			/* for login at machine ... */
			fprintf(fh,"user %s %s\n", rgTM.rFtp.pcUser, rgTM.rFtp.pcPass);
			fprintf(fh,"cd %s\n", rgTM.rFtp.pcMachinePath);
			fprintf(fh,"put %s %s\n", rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath, rgTM.prCmd[ipCmdNo].pcTotalFileName);
			fprintf(fh,"close\n");
			fprintf(fh,"quit\n");

			/* close file now */
			fclose(fh);
		}
	}

	/* send and close ftp-file if necessary */
	if (rgTM.prCmd[ipCmdNo].iOpenDataFile && ((rgTM.iUseFtp && strcmp(pcpCommand, "UFR")) || (rgTM.iUseFtpForUpdate && !strcmp(pcpCommand, "UFR"))))
	{
		/* close it */
		dbg(DEBUG,"<SFaWD>: close file <%s>>", 
				rgTM.prCmd[ipCmdNo].pcTotalFileNameWithPath);
		fclose(rgTM.prCmd[ipCmdNo].FtpFH);

		/* reset status */
		rgTM.prCmd[ipCmdNo].iOpenDataFile = 0;

		/* clear buffer */
		memset((void*)pclTmpFile, 0x00, iMIN_BUF_SIZE);
		if ((pclTmpPath = getenv("TMP_PATH")) == NULL)
		{
			dbg(TRACE,"<SFaWD> missing $TMP_PATH...");
			sprintf(pclTmpFile, "./%s_ftp.log", pcgCDIServerName);
		}
		else
		{
			sprintf(pclTmpFile, "%s/%s_ftp.log", pclTmpPath, pcgCDIServerName);
		}

		/* clear memory */
		memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
		
		/* build command */
		sprintf(pclTmpBuf,"ftp -n %s < %s 2>&1 > %s",
							rgTM.rFtp.pcMachine, rgTM.rFtp.pcFtpCtlFile, pclTmpFile);

		/* execute command */
		dbg(DEBUG,"<SFaWD> System-String is <%s>", pclTmpBuf);
		alarm(90);
		ilRC = system(pclTmpBuf);
		alarm(0);
		dbg(DEBUG,"<SFaWD> System returns: %d", ilRC);

		/* now remove control file */
		if ((ilRC = remove((const char*)rgTM.rFtp.pcFtpCtlFile)) != 0)
		{
			dbg(DEBUG,"<SFaWD> cannot remove ctl-file \"%s\" (%d)", 
						rgTM.rFtp.pcFtpCtlFile, ilRC);
		}

		/* get next unique number */
		if ((ilRC = GetUniqueNumber(pcgServerName, pcgDATName, "MAXOW", 
							1, &lUniqueNumber, &lMax, &lMin)) != RC_SUCCESS)
		{
			/* fatal error */
			dbg(TRACE,"<SFaWD> GetUniquNumber returns: %d", ilRC);
			
			/* reset flag... */
			igStatus &= ~iF_TCPIP_CLIENT_SOCK;
			
			/* exit process */
			Terminate(0);
		}

		/* set unique number */
		lUniqueNumber = UNIQUE(lUniqueNumber, lMax, lMin);	

		/* send data-telegramm with filename and errorcode */
		BuildData(pcpType, "0000000000000000000000000", sDATA_CLASS, lUniqueNumber);

		/* set pseudofield, filename, errorcode */
		/* copy it to buffer */
		memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
		sprintf(pclTmpBuf,"FINA#%s#ERRC#%d#", 
					rgTM.prCmd[ipCmdNo].pcTotalFileName, ilErrorCode);

		/* calculate length of message */
		ilLength = iTDATA_LENGTH + strlen(pclTmpBuf) - 1;

		/* set length in structure */
		memset((void*)pclTmpBuf1, 0x00, iMIN_BUF_SIZE);
		sprintf(pclTmpBuf1,"%-4.4d", ilLength-iTHEADER_LENGTH);
		memmove((void*)prgData->rTH.pcLength,
				  (const void*)pclTmpBuf1, iLENGTH_SIZE);

		/* to buffer */
		memmove((void*)prgData->pcData,
				  (const void*)pclTmpBuf, (size_t)strlen(pclTmpBuf));

		/* write it to send protocol file */
		ToSProtFile((char*)prgData, ilLength); 

		/* write it to socket */
		if ((ilRC = write(igClientSocket, prgData, ilLength)) != ilLength)
		{
			dbg(TRACE,"<SFaWD> write error - can't write Filename to client socket (%d)", ilRC);
			return iWRITE_FAIL;
		}
	}

	/* send only EOD if it is not an Update or if Flag is set */
	if ((rgTM.iSendEODAfterUpdate && !strcmp(pcpCommand, "UFR")) ||
		  strcmp(pcpCommand, "UFR"))
	{
		/* write the End Of Data (EOD) to client */
		if ((prlEOD = BuildEOD()) == NULL)
		{
			dbg(TRACE, "<SFaWD> BuildEOD returns NULL");
			Terminate(0);
		}

		/* write it to send protocol file */
		ToSProtFile((char*)prlEOD, iTEOD_LENGTH); 

		/* write it to socket */
		if ((ilRC = write(igClientSocket, prlEOD, iTEOD_LENGTH)) != iTEOD_LENGTH)
		{
			dbg(TRACE,"<SFaWD> write error - can't write EOD to client socket (%d)", ilRC);
			return iWRITE_FAIL;
		}
	}

	dbg(DEBUG,"<SFtM> ---------- END -----------");
	/* everything is fine */
	return RC_SUCCESS;
}

/******************************************************************************/
/* The WriteGRSToClient routine                                               */
/******************************************************************************/
static int WriteGRSToClient(long lpLastTeleNumber)
{
	int		ilRC;
	int		ilGRSLength;
	char		pclLength[iMIN];

	dbg(DEBUG,"<WGRSToC> --------- START ---------");

	/* check parameter */
	if (lpLastTeleNumber < 0)
			return -1;

	/* set header structure for mix telegram */
	BuildMix("GRS", sCONTROL_CLASS, 0L);	
	
	/* set Fieldname this is always TENO and last valid telegramnumber */
	sprintf(prgMix->pcData, "TENO#%05ld#", lpLastTeleNumber);

	/* set length of next bytes... */
	memset((void*)pclLength, 0x00, iMIN);
	sprintf(pclLength, "%04d", 
		strlen(prgMix->pcData)+iCOMMAND_SIZE+iSEPARATOR_SIZE);

	/* copy it to stucture... */
	memcpy((void*)prgMix->rTH.pcLength, pclLength, 4);

	/* calculate length of message... */
	ilGRSLength = iTMIX_LENGTH + strlen(prgMix->pcData) - 1;
	dbg(DEBUG,"<WGRSToC> calculated length of GRS-Message: %d", ilGRSLength);

	/* write it to send protocol file */
	ToSProtFile((char*)prgMix, ilGRSLength); 

	/* write this message to client-socket... */
	dbg(DEBUG,"<WGRSToC> sending GRS...");
	if ((ilRC = write(igClientSocket, prgMix, (int)ilGRSLength)) != (int)ilGRSLength)
	{
		dbg(TRACE,"<WMtC> write error - can't write data to client socket %d", ilRC);
		return iWRITE_GRSFAIL;
	}
	dbg(DEBUG,"<WGRSToC> write returns...: %d", ilRC);
	
	dbg(DEBUG,"<WGRSToC> --------- END ---------");

	/* bye bye */
	return RC_SUCCESS;
}

/******************************************************************************/
/* the WriteDelTelegramToClient routine                                       */
/******************************************************************************/
static int WriteDelTelegramToClient(int ipTelegramClass, int ipCmdNo, 
											int ipTabNo, char *pcpData, char *pcpFields, 
											int ipNoOfReceivedFields, ULONG lpUniqueNumber)
{
	int			i;
	int			j;
	int			ilRC;
	int			ilLength;
	char			*pcgMsg	= NULL;
	char			*pclS 	= NULL;
	char			pclLength[iMIN];

	dbg(DEBUG,"<WDelTClient> --------- START ---------");
	dbg(DEBUG,"<WDelTClient> calling with TelgramCLASS <%s>", ipTelegramClass == iMDEL_CLASS ? "MDEL_CLASS" : "DDEL_CLASS");
	dbg(DEBUG,"<WDelTClient> Fields: <%s> and Data: <%s>", pcpFields, pcpData);

	/* check out telegram class... */
	switch (ipTelegramClass)
	{
		case iMDEL_CLASS:
			/* Build Header for Mix-Delete Telegams */
			BuildMix(rgTM.prCmd[ipCmdNo].pcCommand, sMDEL_CLASS, lpUniqueNumber);

			/* add fieldname and data */
			for (i=0; i<ipNoOfReceivedFields; i++)
			{
				pclS = GetDataField(pcpFields, i, cCOMMA);
				for (j=0; j<rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].iNoFields; j++)
				{
					if (!strcmp(pclS, rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].pcFields[j]))
					{
						/* found this field in field list... */
						strcat(prgMix->pcData, pclS);
						strcat(prgMix->pcData, "#");
						strcat(prgMix->pcData, GetDataField(pcpData, i, cCOMMA));
						strcat(prgMix->pcData, "#");
						break;
					}
				}
			}

			/* set length of next bytes... */
			memset((void*)pclLength, 0x00, iMIN);
			sprintf(pclLength, "%04d", 
				strlen(prgMix->pcData)+iCOMMAND_SIZE+iSEPARATOR_SIZE);

			/* copy it to stucture... */
			memcpy((void*)prgMix->rTH.pcLength, pclLength, 4);

			/* calculate length of message... */
			ilLength = iTMIX_LENGTH + strlen(prgMix->pcData) - 1;
			dbg(DEBUG,"<WDelTClient> calculated length of Del-Message: %d", ilLength);

			/* set temporaray pointer */
			pcgMsg = (char*)prgMix;
			break;

		case iDDEL_CLASS:
			/* Build Header for Mix-Delete Telegams */
			BuildData(rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].pcDataType, "0000000000000000000000000", sDDEL_CLASS, lpUniqueNumber);

			/* add fieldname and data */
			for (i=0; i<ipNoOfReceivedFields; i++)
			{
				pclS = GetDataField(pcpFields, i, cCOMMA);
				for (j=0; j<rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].iNoFields; j++)
				{
					if (!strcmp(pclS, rgTM.prCmd[ipCmdNo].prTabDef[ipTabNo].pcFields[j]))
					{
						/* found this field in field list... */
						strcat(prgMix->pcData, pclS);
						strcat(prgMix->pcData, "#");
						strcat(prgMix->pcData, GetDataField(pcpData, i, cCOMMA));
						strcat(prgMix->pcData, "#");
						break;
					}
				}
			}

			/* set length of next bytes... */
			memset((void*)pclLength, 0x00, iMIN);
			sprintf(pclLength, "%04d", iTDATA_LENGTH - iTHEADER_LENGTH + strlen(prgData->pcData) - 1);

			/* copy it to stucture... */
			memcpy((void*)prgData->rTH.pcLength, pclLength, 4);

			/* calculate length of message... */
			ilLength = iTDATA_LENGTH + strlen(prgData->pcData) - 1;
			dbg(DEBUG,"<WDelTClient> calculated length of Del-Message: %d", ilLength);

			/* set temporaray pointer */
			pcgMsg = (char*)prgData;
			break;

		default:
			dbg(TRACE,"<WDelTClient> unknown telegram class...");
			return RC_FAIL;
	}

	/* write it to send protocol file */
	ToSProtFile(pcgMsg, ilLength); 

	/* write this message to client-socket... */
	dbg(DEBUG,"<WDelTClient> sending del message...");
	if ((ilRC = write(igClientSocket, pcgMsg, (int)ilLength)) != (int)ilLength)
	{
		dbg(TRACE,"<WDelTClient> write error - can't write data to client socket %d", ilRC);
		return RC_FAIL;
	}
	dbg(DEBUG,"<WDelTClient> write returns...: %d", ilRC);
	dbg(DEBUG,"<WDelTClient> --------- END ---------");

	/* everything looks fine */
	return RC_SUCCESS;
}

/******************************************************************************/
/* the GetInitialBaggageData routine                                          */
/******************************************************************************/
static int GetInitialBaggageData(void)
{
	int		ilRC;

	dbg(DEBUG,"<GetInitialBaggageData> ----- START -----");

	if (rgTM.iConnectModID > 0)
	{
		dbg(DEBUG,"<GetInitialBaggageData> sending command: <%s> to <%d>...", rgTM.pcCloseConnectCommand, rgTM.iConnectModID);
		if ((ilRC = tools_send_sql(rgTM.iConnectModID, rgTM.pcCloseConnectCommand, "", "", "", "")) != RC_SUCCESS)
		{
			dbg(TRACE,"<GetInitialBaggageData> %05d tools_send_sql (close) returns: %d", __LINE__, ilRC);
		}

		dbg(DEBUG,"<GetInitailBaggageData> sending command: <%s> to <%d>...", rgTM.pcOpenConnectCommand, rgTM.iConnectModID);
		if ((ilRC = tools_send_sql(rgTM.iConnectModID, rgTM.pcOpenConnectCommand, "", "", "", "")) != RC_SUCCESS)
		{
			dbg(TRACE,"<GetInitailBaggageData> %05d tools_send_sql (close) returns: %d", __LINE__, ilRC);
		}
	}

	dbg(DEBUG,"<GetInitialBaggageData> ----- END -----");

	/* bye bye */
	return RC_SUCCESS;
}

/******************************************************************************/
/* Die interne TCP-Wait-Timeout-Routine, ist eigentlich eine LIB-Funktion     */
/* aber hier ist sie erweitert um dritten Parameter flg, kenner ob sekunden   */
/* oder microsekunden genommen werden sollen...     	                        */
/******************************************************************************/
static int TcpWaitTimeout(int sock, int t, int flg)
{
	int	rc = RC_SUCCESS;
	fd_set readfds;    
	struct timeval timeval;
	int cnt = 0;
	
	FD_ZERO(&readfds) ;
	FD_SET(sock,&readfds) ;
	if (flg)
	{
		timeval.tv_sec = t;
		timeval.tv_usec = 0;
	}
	else
	{
		timeval.tv_sec = 0;
		timeval.tv_usec = t;
	}

	#ifndef _HPUX_SOURCE
		cnt = select (150, &readfds, NULL, NULL, &timeval);
	#else
		cnt = select (150, (int *) &readfds, NULL, NULL, &timeval);
	#endif
	  
	if (cnt == -1)
	{
		dbg(TRACE, "TCP_WAIT_TIMEOUT: select err:sock=%d rc=%d : %s"
																 ,sock,rc,strerror(errno));  	
		rc = RC_FAIL;
	} /* end if */
 
	if (cnt == 0 )
	{
		rc = RC_NOT_FOUND;
	} /* fi */

	return rc ;
} 

/******************************************************************************/
/* the UtcToLocal routine                                                     */
/******************************************************************************/
static int UtcToLocal(char *pcpTime)
{
	struct tm *_tm;
	time_t    now;
	char	  _tmpc[6];
	int hour_gm,hour_local;
	int UtcDifference;

	now = time(NULL);
	_tm = (struct tm *)gmtime(&now);
	hour_gm = _tm->tm_hour;
	_tm = (struct tm *)localtime(&now);
	hour_local = _tm->tm_hour;
	if (hour_gm > hour_local)
	{
		UtcDifference = (hour_local+24-hour_gm)*3600;
	}
	else
	{
		UtcDifference = (hour_local-hour_gm)*3600;
	}

	dbg(DEBUG,"UtcToLocal <%s> UtcDiff: %d",pcpTime,UtcDifference);
	if (strlen(pcpTime) < 12 )
	{
		return (time_t) 0;
	} /* end if */

	dbg(DEBUG,"Utc: <%s>",pcpTime);
	now = time(0L);
	_tm = (struct tm *)localtime(&now);

	_tmpc[2] = '\0';
	strncpy(_tmpc,pcpTime+12,2);
	_tm -> tm_sec = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+10,2);
	_tm -> tm_min = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+8,2);
	_tm -> tm_hour = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+6,2);
	_tm -> tm_mday = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+4,2);
	_tm -> tm_mon = atoi(_tmpc)-1;
	strncpy(_tmpc,pcpTime,4);
	_tmpc[4] = '\0';
	_tm -> tm_year = atoi(_tmpc)-1900;
	_tm -> tm_wday = 0;
	_tm -> tm_yday = 0;

	now = mktime(_tm) + UtcDifference;
	_tm = (struct tm *)localtime(&now);
	sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
		_tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
		_tm->tm_min,_tm->tm_sec);
	dbg(DEBUG,"Local: <%s>",pcpTime);
	return RC_SUCCESS;
}
static int LocalToUtc(char *oldstr, char *newstr) /***** Convert a CEDA-Local timestamp into a CEDA-UTC timestamp *****/
{
  int c;
  char year[5], month[3], day[3], hour[3], minute[3],second[3];
  struct tm TimeBuffer, *final_result;
  time_t time_result;

  for(c=0; c<= 3; ++c) /********** Extract the Year off CEDA timestamp **********/
    {
      year[c] = oldstr[c];
    }
  year[4] = '\0';
  for(c=0; c <= 1; ++c) /********** Extract month, day, hour and minute off CEDA timestamp **********/
    {
      month[c]  = oldstr[c + 4];
      day[c]    = oldstr[c + 6];
      hour[c]   = oldstr[c + 8];
      minute[c] = oldstr[c + 10];
      second[c] = oldstr[c + 12];
    }
  month[2]  = '\0';    /********** Terminate the Buffer strings **********/
  day[2]    = '\0';
  hour[2]   = '\0';
  minute[2] = '\0';
  second[2] = '\0';

  TimeBuffer.tm_year  = atoi(year) - 1900; /***** Fill a broken-down time structure incl. string to integer *****/
  TimeBuffer.tm_mon   = atoi(month) - 1;
  TimeBuffer.tm_mday  = atoi(day);
  TimeBuffer.tm_hour  = atoi(hour);
  TimeBuffer.tm_min   = atoi(minute);
  TimeBuffer.tm_sec   = atoi(second);
  TimeBuffer.tm_isdst = -1;

  time_result  = mktime(&TimeBuffer);  /***** Create a secondbased timeformat *****/
  final_result = gmtime(&time_result); /***** Reconvert adding timezone and DST *****/
  sprintf(newstr,"%d%.2d%.2d%.2d%.2d%.2d",final_result->tm_year+1900,final_result->tm_mon+1,final_result->tm_mday,final_result->tm_hour,final_result->tm_min,final_result->tm_sec);

  return(RC_SUCCESS); /***** DONE WELL *****/
}
/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
