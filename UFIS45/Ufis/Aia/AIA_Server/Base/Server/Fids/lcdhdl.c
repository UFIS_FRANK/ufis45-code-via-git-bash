#ifndef _DEF_mks_version
	#define _DEF_mks_version
	#include "ufisvers.h"  /* sets UFIS_VERSION, must be done defore mks_version */
	static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Aia/AIA_Server/Base/Server/Fids/lcdhdl.c 1.3 2005/08/16 14:55:02SGT jwe Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB AAT/I Program lcdhdl.c    	                                            */
/*                                                                            */
/* Author         : Joern Weerts                                              */
/* Date           : 14.08.2000                                                */
/* Description    : control process for the AEG-MIS LCD boards        				*/
/* Update history :                                           								*/
/*                  jwe 14.08.2000                                            */
/*                  started with programming                                  */
/*                  date/who                                                  */
/*                  action                                                    */
/******************************************************************************/
/* Extended version for greece 19.07.01                                       */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
/* be carefule with strftime or similar functions !!!                         */
/* static char sccs_lcdhdl[]="@(#) UFIS 4.4 (c) ABB AAT/I lcdhdl.c SCM 44.14 / 00/11/28 14:00:58 / JWE"; */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <dirent.h>
#include <arpa/inet.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "db_if.h"
#include "sthdef.h"
#include "helpful.h"
#include "hsbsub.h"
#include "debugrec.h"
#include "netin.h"
#include "tcputil.h"
#include "list.h"
#include "ftptools.h"
#include "ftphdl.h"
#include "lcdhdl.h"

/* members for store the last time */
#define C_MAX_CNT_LT    10 

/******************************************************************************/
/* External variables and functions                                           */
/******************************************************************************/
FILE *outp       = NULL;
int  debug_level = TRACE;
int  igDebugLevel = 0;

extern int nap(unsigned long time);
extern int snap(char*,long,FILE*);
extern void BuildItemBuffer(char *, char *, int, char *);
extern int GetDataItem(char*,char*,int,char,char*,char*);
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static int igCnt = 0;
static int igSigCnt = 0;
static ITEM  *prgItem      = NULL;    		/* The queue item pointer  */
static int igItemLen     	 = 0;       		/* length of incoming item */
static EVENT *prgEvent     = NULL;    		/* The event pointer       */
static CFG  prgCfg;												/* pointer to cfg-file structure */
static char pcgProcessName[20];      			/* buffer for my own process name */ 
static char pcgHomeAp[20];      					/* buffer for name of the home-airport */
static char pcgTabEnd[20];      					/* buffer for name of the table extension */
static char pcgAftTable[20];							/* buffer for AFT-table to use */
static int igInitOK   		 = FALSE;				/* init flag */
static char pcgConfFile[S_BUFF];					/* buffer for filename */
static char *pcgSections = NULL;
static char *pcgSectionPointer = NULL;
static char *pcgDebugLevel = NULL;
static int igActSection = -1;
static char *pcgTryReconnect = NULL;
static char pcgLineTerm[2];
static char pcgGreekChars[S_BUFF];
static char pcgPolishChars[S_BUFF];
static char *pcgLang = NULL;
static char *pcgFlight_nr_format = NULL;
static int igLang=0;
static int igAlc=0;
static int igFlnr=0;
static int igShowSuffix=0;

static LPLISTHEADER prgGroupList = NULL;	/* pointer to internal data list */

static BOOL bgAlarm 					= FALSE;		/* global flag for timed-out socket-read */
static BOOL bgUseHopo					= FALSE;		/* global flag for use of HOPO database field */

static time_t tgNextConnect = 0;
static time_t tgNextTry = 0;
static time_t tgLastTimeArr[C_MAX_CNT_LT] ; 

static int    glExtJfnoAllowed = EXTD_OFF ; /* extended mode flag flno */  	
static int    glExtRemAllowed = EXTD_OFF ;  /* extended mode flag remark */ 
static int    glTestMode = 0 ;              /* test mode is off */ 
static char   glFile[64] ; 
static FILE   *ftest = NULL ; 
static int   	glTCPFile = 0 ; 


/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
/*----------------*/
/* MAIN functions */
/*----------------*/
static int  Init_lcdhdl(void);		/* main initializing */	
static int Reset(void);         /* Reset program          */
static int HandleInternalData(void);        /* Handles data from CEDA */
static int HandleExternalData(char *pcpData);/* handles data from LCD */
static void	Terminate(BOOL bpSleep);        /* Terminate program      */
static void	HandleSignal(int);              /* Handles signals        */
static void	HandleErr(int);                 /* Handles general errors */
static void	HandleQueErr(int);              /* Handles queuing errors */
static void HandleQueues(void);             /* Waiting for Sts.-switch*/
static void ConnectLCDs();
static void CloseTCP(int ipSock);
static void InitBoard(BOARDINFO *prpBoard);
static int InitReplacements(GROUPINFO *prpGroup);
static int ReadSections();
static int GetSection(char *pcpSection);
static int ReadCfg(char *pcpSection, int *ipGroup);
static int GetCfgEntry(char *pcpFile,char *pcpSection,char *pcpEntry,short spType,
												char **pcpDest,int ipValueType,char *pcpDefVal);
static int CheckValue(char *pcpEntry,char *pcpValue,int ipType);
static int CheckCfg(int ipGroup);
static int GetBoardInfos(int ipGroup);
static void FreeCfgMem(void);
/* static void FreeMemInFlights(LPLISTHEADER prlFlights) ; */ 
static int poll_q_and_sock ();
static int Send_data(BOARDINFO *prpBoard,char *pcpData,int ipLen);
/*---------------------*/
/* formating functions */
/*---------------------*/
static void TrimRight(char *pcpBuffer);
static void TrimAll(char *pcpBuffer);
static int StrToTime(char *pcpTime,time_t *pclTime);
static void TimeToStr(char *pcpTime,time_t lpTime,int ipType,int ipLocal);
static void CleanMessage(char *pcpMsg,int ipMsgLen,char cpChar, char cpRepl);
static void TrimNewLine(char *pcpString);

static int FormatData(GROUPINFO *prpGroup, char *pcpData);
static int ConvertData(char *pcpFlightUrno,GROUPINFO *prpGroup,char *pcpLine, 
							char *pcpEngResultBuf,char *pcpLocalResultBuf);
static void ReplaceEstTime(char *pcpEstBuf,char *pcpBuffer);
static void ReplaceBoardLetters(GROUPINFO *prpGroup,char *pcpBuffer);
static int FormatDate(char *pcpOldDate,char *pcpFormat,char *pcpNewDate);
static void StoreData(GROUPINFO *prpGroup,char *pclUrnoBuf,char *pcpEngData,
											char *pcpLocalData, char *pcpCSStr, int ipCSSet) ;
static int DistributeData(GROUPINFO *prpGroup);
static void CheckSwitching();
static void UpdateBoardData(char *pcpUrno,char *pcpSelection);
static int UtcToLocal(char *oldstr,char *newstr);
static void Convert_LCD_Chars(unsigned char *pcpString, int ipLang);
static void InitGreekCharset();
static int MakeExtFlno(char *pcpFlno);
static int Get_CCA_Counter(char *pcpFlightUrno,char *pcpMinMax,char *pcpData);
static void ReplaceCfgFormat(char *pcpBuffer,int ipFrom,int ipTo, int ipT1, int ipT2);
static int GetSubStr(char *pcpSRCStr, char * pcpSUBStr, int ipItem, int ipDiffLen) ; 
static void ChkCSAttach (GROUPINFO *prpGroup, char *pcpEngData, 
												 char *pcpLocalData, char *cpCSStr, int *ipCSSet) ;  
static void ChangeCSShow (GROUPINFO *prlGroup, int ipToDo) ; 
/*-------------------*/
/* logging functions */
/*-------------------*/
static void snapit(void *pcpBuffer,long lpDataLen,FILE *pcpDbgFile);
static void StoreGroup(char *pcpMsg); /* stores incoming messages */
static void HandleStoredData(int ipTimesToDo);/* Handles stored, received messages */
static void DumpBoardInfos(void);
/*-----------------------------------------*/
/* functions for internal created messages */
/*-----------------------------------------*/
static int PrepareBoardData(GROUPINFO *prpGroup);
static void MakeSingleEntry(char *pcpSrc,int ipLen,int ipAdjust,char cpFill,char *pcpDest);
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;
  int ilZ = 0 ; 

	INITIALIZE;			/* General initialization	*/

	/* --------------------------------------------------------------------*/
	/* gltestMode == 1 should all TCP IP Information ignores and send the  */
	/*                 TCP IP information to file ./LCDWRK.TST             */
	/* --------------------------------------------------------------------*/
	glTestMode = 0 ; 
	glTCPFile = 0 ; /* if == 1 -> store TCPIP Inf in ./debug/LCDWRK.TST */  
  if (glTestMode == 1) ftest = fopen ("/ceda/debug/LCDWRK.TST", "w") ; 
  if (glTCPFile == 1) ftest = fopen ("/ceda/debug/LCDWRK.TST", "w") ; 

	/* clear time array */ 
	for (ilZ = 0; ilZ < C_MAX_CNT_LT; ilZ++) tgLastTimeArr[ilZ] = time(0) ; 

	/* handles signal		*/
	(void)SetSignals(HandleSignal);

  /* copy process-name to global buffer */
  memset(pcgProcessName,0x00,sizeof(pcgProcessName));
	strcpy(pcgProcessName, argv[0]);                            

	/*dbg(TRACE,"MAIN: version <%s>",sccs_lcdhdl);*/
	dbg(TRACE,"MAIN: version <%s>",mks_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));
	
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}
	else
	{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}
	/* Attach to the DB */
	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
		dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
		sleep(6);
		ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRc != RC_SUCCESS));
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}
	else
	{
		dbg(TRACE,"MAIN: init_db()  OK!");
	} /* end of if */
	/**********************************************************/
	/* following lines for identical binaries on HSB-machines */
	/**********************************************************/
	*pcgConfFile = '\0'; 
	sprintf(pcgConfFile,"%s/%s",getenv("BIN_PATH"),pcgProcessName);
	ilRc = TransferFile(pcgConfFile); 
	if(ilRc != RC_SUCCESS) 
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
	}
	
	/********************************************************************/
	/*For identical cfg-files on HSB-machines uncomment following lines */
	/********************************************************************/
	*pcgConfFile = '\0';
	sprintf(pcgConfFile,"%s/%s.cfg",getenv("CFG_PATH"),pcgProcessName);
	ilRc = TransferFile(pcgConfFile); 
	if(ilRc != RC_SUCCESS) 
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
	} 
	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	}
	
	/* now waiting if I'm STANDBY */
	if ((ctrl_sta != HSB_STANDALONE) &&
			(ctrl_sta != HSB_ACTIVE) &&
			(ctrl_sta != HSB_ACT_TO_SBY))
	{
		HandleQueues();
	}

	dbg(TRACE,"--------------------------------");
	if ((ctrl_sta == HSB_STANDALONE) ||
			(ctrl_sta == HSB_ACTIVE) ||
			(ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		dbg(TRACE,"--------------------------------");
		if(igInitOK == FALSE)
		{
			ilRc = Init_lcdhdl();
			debug_level = TRACE;
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"--------------------------------");
				dbg(TRACE,"MAIN: Init_lcdhdl() failed! Terminating!");
				Terminate(TRUE);
			}
			DumpBoardInfos();
		}
	} 
	else
	{
		dbg(TRACE,"MAIN: wrong HSB-state! Terminating!");
		Terminate(FALSE);
	}
	
	if (ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: initializing OK");
		dbg(TRACE,"--------------------------------");
		debug_level = igDebugLevel;

		for(;;)
		{
			if ((ilRc = poll_q_and_sock()) == RC_SUCCESS)
			{
				dbg(TRACE,"MAIN: pll_q_and_sock() failed!");
			}
		}
	}
	else
	{
		dbg(TRACE,"MAIN: Init_lcdhdl() invalid! Terminating!");
	}
	Terminate(TRUE);
	return(0);
} /* end of MAIN */


/*********************************************************************
Function	 :Init_lcdhdl
Paramter	 :IN:                                                      
Returnvalue:RC_SUCCES,RC_FAIL
Description:initializes the interface with all necessary values
*********************************************************************/
static int Init_lcdhdl()
{
	int	ilRc = RC_FAIL;			/* Return code */
	int	ilGroup = 0;	
	char pclSection[XS_BUFF];
	char pclSystemTime[L_BUFF];
	
	/* reading default home-airport from sgs.tab */
	memset(pcgHomeAp,0x00,sizeof(pcgHomeAp));
	ilRc = tool_search_exco_data("SYS","HOMEAP",pcgHomeAp);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"Init_lcdhdl : No HOMEAP entry in sgs.tab: EXTAB! Please add!");
		return RC_FAIL;
	}
	else
	{
		dbg(TRACE,"Init_lcdhdl : HOMEAP = <%s>",pcgHomeAp);
	}
	
	/* reading default table-extension from sgs.tab */
	memset(pcgTabEnd,0x00,sizeof(pcgTabEnd));
	ilRc = tool_search_exco_data("ALL","TABEND",pcgTabEnd);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"Init_lcdhdl : No TABEND entry in sgs.tab: EXTAB! Please add!");
		return RC_FAIL;
	}
	else
	{
		*pcgAftTable = 0x00;
		strcpy(pcgAftTable,"AFT");
		dbg(TRACE,"Init_lcdhdl : TABEND = <%s>",pcgTabEnd);
		strcat(pcgAftTable,pcgTabEnd);
		dbg(TRACE,"Init_lcdhdl : AFT-table = <%s>",pcgAftTable);
		if (strcmp(pcgTabEnd,"TAB") == 0)
		{
			bgUseHopo = TRUE;
			dbg(TRACE,"Init_lcdhdl : use HOPO-field!");
		}
	}

	if ((prgGroupList=ListInit(prgGroupList,sizeof(GROUPINFO))) == NULL)
	{
		dbg(TRACE,"Init_lcdhdl : couldn't create internal data-list!");
		return RC_FAIL;
	}

	/* now reading from configfile */
	if (*pcgConfFile != 0x00)
	{
		if ((ilRc = ReadSections()) == RC_SUCCESS)
		{
			while(GetSection(pclSection) == RC_SUCCESS)
			{
				if ((ilRc = ReadCfg(pclSection, &ilGroup)) == RC_SUCCESS)
				{
					if ((ilRc = CheckCfg(ilGroup)) == RC_SUCCESS)
					{
						if ((ilRc = GetBoardInfos(ilGroup)) == RC_SUCCESS)
						{
							igInitOK = TRUE;
						}
						else
						{
							dbg(TRACE,"Init_lcdhdl : GetBoardInfos Section Nr.<%d> failed!",ilGroup);
						}
					}
					else
					{
						dbg(TRACE,"Init_lcdhdl : CheckCfg(%d) failed!",ilGroup);
					}
				}
				else
				{
					dbg(TRACE,"Init_lcdhdl : ReadCfg(%s) failed!",pclSection);
				}
			}
		}
	}

	/* creating the eol terminator */
	pcgLineTerm[0] = 0xD;
	pcgLineTerm[1] = 0xA;

	InitGreekCharset();
	return(ilRc);
} /* end of initialize */
/*********************************************************************
Function	 :Reset()
Paramter	 :IN:                                                   
Returnvalue:RC_SUCCESS,RC_FAIL
Description:rereads the config-file,reinitializes the recv. and send
						log-files and clears the receive and the send buffers
*********************************************************************/
static int Reset(void)
{
	int	ilRc = RC_SUCCESS;
	int	ilGroup = 0;
	char pclSection[XS_BUFF];
	
	dbg(TRACE,"Reset: now resetting ...");
	dbg(TRACE,"Reset: closing TCP-IP connection(s).",ilRc);

  CloseTCP(0);
  if (glTestMode == 1) { fflush(ftest) ; fclose (ftest) ; }  
  if (glTCPFile == 1) { fflush(ftest) ; fclose (ftest) ;  }  

	igActSection = 0;

	dbg(TRACE,"Reset: freeing CFG-Memory.",ilRc);

	FreeCfgMem();
	ListDestroy(prgGroupList);

	if ((prgGroupList=ListInit(prgGroupList,sizeof(GROUPINFO))) == NULL)
	{
		dbg(TRACE,"Init_lcdhdl : couldn't create internal Recv.-Msg-List!");
		return RC_FAIL;
	}

	if (*pcgConfFile != '\0')
	{
		if ((ilRc = ReadSections()) == RC_SUCCESS)
		{
			while(GetSection(pclSection) == RC_SUCCESS)
			{
				if ((ilRc = ReadCfg(pclSection, &ilGroup)) != RC_SUCCESS)
				{
					dbg(TRACE,"Reset: ReadCfg(%s) failed!",pclSection);
					ilRc = RC_FAIL;
				}
				else
				{
					if ((ilRc = CheckCfg(ilGroup)) != RC_SUCCESS)
					{
						dbg(TRACE,"Reset: CheckCfg(%d) failed!",ilGroup);
						ilRc = RC_FAIL;
					}
				}
			}
		}
	}
	dbg(TRACE,"Reset: ... finished!");
	return ilRc;
} 
/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(BOOL bpSleep)
{
	int ilRc = 0;
	if (bpSleep == TRUE)
	{
		dbg(TRACE,"Terminate: sleeping 60 sec. before terminating.");
		sleep(60);
	}

	if (igInitOK == TRUE)
	{
    CloseTCP(0);
    if (glTestMode == 1) { fflush(ftest) ; fclose (ftest) ; }  
    if (glTCPFile == 1) { fflush(ftest) ; fclose (ftest) ; }  

		dbg(TRACE,"Terminate: TCP/IP-connections closed.");

		dbg(TRACE,"Terminate: freeing CFG-Memory.",ilRc);
		FreeCfgMem();

		ListDestroy(prgGroupList);
		dbg(TRACE,"Terminate: internal data deleted.");
	}
	dbg(TRACE,"Terminate: now leaving ......");
	exit(0);
} 

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int ipSig)
{
	switch (ipSig)
	{
		case SIGPIPE:
			/*dbg(TRACE,"HandleSignal: Received Signal <%d>(SIGPIPE).",ipSig);*/
			/* Check connections should be called here */
			break;
		case SIGTERM:
			dbg(TRACE,"HandleSignal: Received Signal <%d>(SIGTERM).",ipSig);
			Terminate(FALSE);
			break;
		case SIGALRM:
			dbg(DEBUG,"HandleSignal: Received Signal <%d>(SIGALRM)",ipSig);
			bgAlarm = TRUE;
			break;
		case SIGCHLD:
			dbg(TRACE,"HandleSignal: Received Signal <%d>(SIGCHLD)",ipSig);
			Terminate(FALSE);
			break;
		default:
			dbg(TRACE,"HandleSignal: Received Signal <%d>. Terminating !",ipSig);
			Terminate(FALSE);
			break;
	} 
	if (igSigCnt > 100)
	{
		dbg(TRACE,"HandleSignal: Terminating. Received SIGPIPE more than 100 times!");
		Terminate(TRUE);
	}
} 

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr)
	{
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	case	QUE_E_PRIORITY:
		dbg(TRACE,"<%d> : invalid priority was send ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
	return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	dbg(TRACE,"HandleQueues: now entering ...");
	do
	{
		/*memset(prgItem,0x00,igItemLen);*/
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	
		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_STANDBY event!");
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_COMING_UP event!");
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_ACTIVE event!");
				dbg(TRACE,"HandleQueues: Terminating for new init ...");
				Terminate(FALSE);
				ilBreakOut = TRUE;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_ACT_TO_SBY event!");
				break;	
			case	HSB_DOWN	:
				/* 	whole system shutdown - do not further use que(), */
				/*	send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_DOWN event!");
				Terminate(FALSE);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_STANDALONE event!");
				dbg(TRACE,"HandleQueues: Terminating for new init ...");
				Terminate(FALSE);
				ilBreakOut = TRUE;
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				/*HandleRemoteDB(prgEvent);*/
				break;
			case	SHUTDOWN	:
				dbg(TRACE,"HandleQueues: received SHUTDOWN event!");
				Terminate(FALSE);
				break;
			case	RESET		:
				dbg(TRACE,"HandleQueues: received RESET event!");
				ilRc = Reset();
				if (ilRc == RC_FAIL)
				{
					Terminate(FALSE);
				}
				break;
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong HSB-status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		}
		else
		{
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	}while (ilBreakOut == FALSE);

	if(igInitOK == FALSE)
	{
			ilRc = Init_lcdhdl();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Init_lcdhdl: init phase 2 failed!");
			} /* end of if */
	}/* end of if */
	dbg(TRACE,"HandleQueues: ... now leaving");
} /* end of HandleQueues */
	

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleInternalData()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilDataLen = 0;			/* Return code */
	int	ilType = 0;			/* Return code */
	char *pclSelection = NULL;
	char *pclFields = NULL;
	char *pclData = NULL;
  BC_HEAD *bchd = NULL;		/* Broadcast header		*/
  CMDBLK  *cmdblk = NULL;	/* Command Block 		*/
	char pclTwStart[S_BUFF];
	char pclUrno[XXS_BUFF];

  bchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  cmdblk= (CMDBLK  *) ((char *)bchd->data);

	dbg(TRACE,"");
	dbg(TRACE,"--- END/START HandleInternalData ---");

	dbg(TRACE,"HID: From<%d> Cmd<%s> Table<%s> Twstart<%s>",
			prgEvent->originator,cmdblk->command,cmdblk->obj_name,cmdblk->tw_start);
	
	/*DebugPrintItem(DEBUG,prgItem);*/
	/*DebugPrintEvent(DEBUG,prgEvent);*/

	pclSelection = cmdblk->data;
	pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
	pclData = (char*)pclFields + strlen(pclFields) + 1;

	dbg(DEBUG,"HID: Selection: <%s>",pclSelection); 
	dbg(DEBUG,"HID: Fields   : <%s>",pclFields); 
	dbg(DEBUG,"HID: Data     : <%s>",pclData);

	/***************************************************************/
	/* receiving update from ACTION for sending a new info to LCD */
	/***************************************************************/
	TrimNewLine(pclFields);
	if (strncmp(cmdblk->command,"UFR",3) == 0 && strncmp(pclFields,"URNO",4) == 0 )
	{
		TrimNewLine(pclData);
		TrimNewLine(pclSelection);
		dbg(DEBUG,"HID: <%s> from ACTION, DATA=<%s>.",cmdblk->command,pclData); 
		memset(pclUrno,0x00,XXS_BUFF);
		GetDataItem(pclUrno,pclData,1,',',"","");
		UpdateBoardData(pclUrno,pclSelection);
	}
	else
	{
		dbg(TRACE,"HID: invalid field=<%s> received! expecting URNO as first field!",pclFields);
		dbg(TRACE,"HID: Assuming invalid ACTION configuration!"); 
	}
  return ilRc;
} 
/* ********************************************************************/
/* The TrimNewLine() routine																					 */
/* ********************************************************************/
static void TrimNewLine(char *pcpString)
{
	char *pclPointer = NULL;
	if ((pclPointer=strchr(pcpString,'\n')) != NULL)
	{
		*pclPointer = '\0';
	}
}
/* ********************************************************************/
/* The ReadCfg() routine																					 */
/* ********************************************************************/
static int ReadCfg(char *pcpSection, int *ipGroup)
{
	int ilRc = RC_SUCCESS;
	GROUPINFO rlGroup;

	memset(&rlGroup,0x00,sizeof(GROUPINFO));

	/* setting the group nr. */
	rlGroup.ilGroupNr = igActSection;
	*ipGroup = igActSection;

	/* filling CFG structure of group */
	dbg(TRACE,"--------------------------------");
	dbg(TRACE,"ReadCfg: file <%s> Section <%s>",pcgConfFile,pcpSection);
	dbg(TRACE,"--------------------------------");
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"REFRESH_CYCLE",CFG_STRING,&rlGroup.rlCfg.refresh_cycle,CFG_NUM,"300"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"SWITCH_CYCLE",CFG_STRING,&rlGroup.rlCfg.switch_cycle,CFG_NUM,"30"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"LCD_IPS",CFG_STRING,&rlGroup.rlCfg.lcd_ips,CFG_PRINT,"0.0.0.0"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"BOARD_COLS",CFG_STRING,&rlGroup.rlCfg.board_cols,CFG_PRINT,"0"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"BOARD_ROWS",CFG_STRING,&rlGroup.rlCfg.board_rows,CFG_PRINT,"0"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"SERVICE_PORT",CFG_STRING,&rlGroup.rlCfg.service_port,CFG_PRINT,"EXCO_LCD"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"USE_COUNTER_TABLE",CFG_STRING,&rlGroup.rlCfg.use_counter_table,CFG_ALPHA,"N"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"DB_EST_TIME_FIELD",CFG_STRING,&rlGroup.rlCfg.db_est_time_field,CFG_ALPHA,"ETOD"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"DB_TABLES",CFG_STRING,&rlGroup.rlCfg.db_tables,CFG_PRINT," "))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"DB_FIELDS",CFG_STRING,&rlGroup.rlCfg.db_fields,CFG_PRINT," "))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"START_MIN",CFG_STRING,&rlGroup.rlCfg.start_min,CFG_PRINT,"-5"))
			!= RC_SUCCESS)
		{}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"END_MIN",CFG_STRING,&rlGroup.rlCfg.end_min,CFG_PRINT,"+180"))
			!= RC_SUCCESS)
		{}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"T_PARA1",CFG_STRING,&rlGroup.rlCfg.t_para1,CFG_PRINT,"0"))
			!= RC_SUCCESS)
		{}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"T_PARA2",CFG_STRING,&rlGroup.rlCfg.t_para2,CFG_PRINT,"0"))
			!= RC_SUCCESS)
		{}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"DB_COND",CFG_STRING,&rlGroup.rlCfg.db_cond,CFG_PRINT," "))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"DB_FIELD_OFFSET",CFG_STRING,&rlGroup.rlCfg.db_field_offset,CFG_PRINT,"0"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"DB_FIELD_LEN",CFG_STRING,&rlGroup.rlCfg.db_field_len,CFG_PRINT,""))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"REPL_FIELDS",CFG_STRING,&rlGroup.rlCfg.repl_fields,CFG_PRINT,""))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"REPLACEMENT",CFG_STRING,&rlGroup.rlCfg.replacement,CFG_PRINT,""))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"REFERENCE",CFG_STRING,&rlGroup.rlCfg.reference,CFG_PRINT,""))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"REPL_TABLE",CFG_STRING,&rlGroup.rlCfg.repl_table,CFG_PRINT,""))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"LANG_FIELDS",CFG_STRING,&rlGroup.rlCfg.lang_fields,CFG_PRINT,""))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"LANG_REPLACEMENT",CFG_STRING,&rlGroup.rlCfg.lang_replacement,CFG_PRINT,""))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"LANG_REFERENCE",CFG_STRING,&rlGroup.rlCfg.lang_reference,CFG_PRINT,""))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"LANG_REPL_TABLE",CFG_STRING,&rlGroup.rlCfg.lang_repl_table,CFG_PRINT,""))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"DATE_FIELDS",CFG_STRING,&rlGroup.rlCfg.date_fields,CFG_PRINT,""))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"DATE_FORMAT",CFG_STRING,&rlGroup.rlCfg.date_format,CFG_PRINT,""))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"REPL_BOARD_LETTER",CFG_STRING,&rlGroup.rlCfg.repl_board_letter,CFG_PRINT,""))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"REPL_CHAR",CFG_STRING,&rlGroup.rlCfg.repl_char,CFG_PRINT,""))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	dbg(TRACE,"--------------------------------");

	/*-----------------------------------*/
	/* extended display information JFNO */
	/*-----------------------------------*/
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"EXTD_SWITCH_JFNO",CFG_STRING,
			 &rlGroup.rlCfg.extd_switch_jfno,CFG_NUM,"0")) != RC_SUCCESS)
	{                            /* extended JFNO DISPLAYING IS NOT ACTIVE */
	 dbg(DEBUG, "ReadCfg: : EXTD_SWITCH_JFNO not set - Mode disabled!");
	 glExtJfnoAllowed = EXTD_OFF ;  	
	}
	/* interpret all extended config parameters */
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"EXTD_SYNC",CFG_STRING,
		   &rlGroup.rlCfg.extd_sync,CFG_NUM,"1")) != RC_SUCCESS)
  {                            /* extended SYNC IS NOT SET */
	 dbg(DEBUG, "ReadCfg: : EXTD_SYNC not set - set dafault = 1 (sync)!") ;
	}

	if((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"EXTD_TIME_DIFF_JFNO",CFG_STRING,
		  &rlGroup.rlCfg.extd_time_diff_jfno,CFG_NUM,"10")) != RC_SUCCESS)
	{                            /* extended Time Diff IS NOT SET */
	 dbg(DEBUG, "ReadCfg: : EXTD_TIME_DIFF_JFNO not set - set default = 10!") ;
	}

	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"EXTD_MAX_JFNO",CFG_STRING,
		  &rlGroup.rlCfg.extd_max_jfno,CFG_NUM,"2")) != RC_SUCCESS)
	{                            /* extended MAX JFNO SHOW IS NOT SET */
	 dbg(DEBUG, "ReadCfg: : EXTD_MAX_JFNO not set - set default = 2!") ;
	}
	/*--------------------------------------*/
	/* extended display information REMARKS */
	/*--------------------------------------*/
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"EXTD_SWITCH_REM",CFG_STRING,
			 &rlGroup.rlCfg.extd_switch_rem,CFG_NUM,"0")) != RC_SUCCESS)
	{                            /* extended REM DISPLAYING IS NOT ACTIVE */
	 dbg(DEBUG, "ReadCfg: : EXTD_SWITCH_REM not set - Mode disabled!");
	 glExtRemAllowed = EXTD_OFF ;  	
	}

	if((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"EXTD_TIME_DIFF_REM",CFG_STRING,
		  &rlGroup.rlCfg.extd_time_diff_rem,CFG_NUM,"10")) != RC_SUCCESS)
	{                            /* extended Time Diff IS NOT SET */
	 dbg(DEBUG, "ReadCfg: : EXTD_TIME_DIFF_REM not set - set default = 10!") ;
	}

	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"EXTD_MAX_REM",CFG_STRING,
		  &rlGroup.rlCfg.extd_max_rem,CFG_NUM,"1")) != RC_SUCCESS)
	{                            /* extended MAX REM SHOW IS NOT SET */
	 dbg(DEBUG, "ReadCfg: : EXTD_MAX_REM not set - set default = 1!") ;
	}


	/* initializing board-list of group */
	if ((rlGroup.prlBoards=ListInit(rlGroup.prlBoards,sizeof(BOARDINFO))) == NULL)
	{
		dbg(TRACE,"ReadCfg: : couldn't create internal connection-list!");
		return RC_FAIL;
	}

	/* initializing flightinfo list of group */
	if ((rlGroup.prlFlightInfo=ListInit(rlGroup.prlFlightInfo,sizeof(FLIGHTINFO))) == NULL)
	{
		dbg(TRACE,"ReadCfg: : couldn't create internal connection-list!");
		return RC_FAIL;
	}

	/* appending group to global list of groups */
	if (ListAppend(prgGroupList,&rlGroup) == NULL)
	{
		dbg(TRACE,"ReadCfg: ListAppend failed!");
		Terminate(FALSE);
	}
	return ilRc;
}
/* *********************************************************************/
/* The GetCfgEntry() routine   																				 */
/* *********************************************************************/
static int GetCfgEntry(char *pcpFile,char *pcpSection,char *pcpEntry,short spType,
												char **pcpDest,int ipValueType,char *pcpDefVal)
{
	int ilRc = RC_SUCCESS;
	int ilLen = 0;
	char pclCfgLineBuffer[L_BUFF];
	
	memset(pclCfgLineBuffer,0x00,L_BUFF);
	if ((ilRc=iGetConfigRow(pcpFile,pcpSection,pcpEntry,spType,
													pclCfgLineBuffer)) != RC_SUCCESS)
	{
		dbg(TRACE,"GetCfgEntry: reading entry <%s> failed.",pcpEntry);
	}
	else
	{
		if (strlen(pclCfgLineBuffer) < 1)
		{
			dbg(TRACE,"GetCfgEntry: EMPTY %s! Use default <%s>.",pcpEntry,pcpDefVal);
			strcpy(pclCfgLineBuffer,pcpDefVal);
		}
		ilLen = strlen(pclCfgLineBuffer)+4; 
		*pcpDest = malloc(ilLen);
		/*memset(pcpDest,0x00,ilLen);*/
		strcpy(*pcpDest,pclCfgLineBuffer);
		dbg(TRACE,"GetCfgEntry: %s = <%s>",pcpEntry,*pcpDest);
		if ((ilRc = CheckValue(pcpEntry,*pcpDest,ipValueType)) != RC_SUCCESS)
		{
			dbg(TRACE,"GetCfgEntry: please correct value <%s>!",pcpEntry); 	
		}
	}
	return ilRc;
}
/* ******************************************************************** */
/* The CheckValue() routine																						*/
/* ******************************************************************** */
static int CheckValue(char *pcpEntry,char *pcpValue,int ipType)
{
	int ilRc = RC_SUCCESS;

	switch(ipType)
	{
		case CFG_NUM:
			while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
			{
				if (isdigit(*pcpValue)==0)
				{
					dbg(TRACE,"CheckValue : NOT A NUMBER! <%s>!"
						,pcpEntry); 
					ilRc = RC_FAIL;
				}
				pcpValue++;
			}
			break;
		case CFG_ALPHA:
			while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
			{
				if (isalpha(*pcpValue)==0)
				{
					dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!"
						,*pcpValue,pcpEntry); 
					ilRc = RC_FAIL;
				}
				pcpValue++;
			} 
			break;
		case CFG_ALPHANUM:
			while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
			{
				if (isalnum(*pcpValue)==0)
				{
					dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!"
						,*pcpValue,pcpEntry); 
					ilRc = RC_FAIL;
				}
				pcpValue++;
			}
			break;
		case CFG_PRINT:
			while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
			{
				if (isprint(*pcpValue)==0)
				{
					dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!"
						,*pcpValue,pcpEntry); 
					ilRc = RC_FAIL;
				}
				pcpValue++;
			}
			break;
		default:
			break;
	}
	return ilRc;
}
/* ******************************************************************** */
/* The CheckCfg(int ipGroup) routine																		*/
/* ******************************************************************** */
static int CheckCfg(int ipGroup)
{
	int ilRc = RC_SUCCESS;
	int ilCount = 0;
	int ilCount2 = 0;
	int ilCount3 = 0;
	int ilCount4 = 0;
	GROUPINFO *prlGroup = NULL;
	LPLISTELEMENT prlEle = NULL;
	char pclMaxOffset[XS_BUFF];
	char pclLastLen[XS_BUFF];
			
	prlEle = ListFindFirst(prgGroupList);
	while(prlEle!=NULL)
	{
		prlGroup = (GROUPINFO*)prlEle->Data;
		if (prlGroup->ilGroupNr == ipGroup)
		{
			dbg(DEBUG,"CheckCfg: Section Nr.<%d>"
				,prlGroup->ilGroupNr);
			if ((ilCount = (int)GetNoOfElements(prlGroup->rlCfg.lcd_ips,',')) > 0)
			{
				ilCount2 = (int)GetNoOfElements(prlGroup->rlCfg.board_cols,',');
				ilCount3 = (int)GetNoOfElements(prlGroup->rlCfg.board_rows,',');
				ilCount4 = (int)GetNoOfElements(prlGroup->rlCfg.service_port,',');
				if (ilCount!=ilCount2 || ilCount!=ilCount3 || ilCount!=ilCount4)
				{
				 dbg(TRACE,"CheckCfg: LCD_IPS,BOARD_COLS,BOARD_ROWS or SERVICE_PORT inconsistent!");
				 dbg(TRACE,"CheckCfg: setting section to NOT VALID!") ;
				 ilRc = RC_FAIL ;
				}
				ilCount = (int)GetNoOfElements(prlGroup->rlCfg.db_tables,',');
				ilCount2 = (int)GetNoOfElements(prlGroup->rlCfg.db_fields,',');

				ilCount3 = (int)GetNoOfElements(prlGroup->rlCfg.db_field_offset,',');
				memset(pclMaxOffset,0x00,XS_BUFF);
				GetDataItem(pclMaxOffset,prlGroup->rlCfg.db_field_offset,ilCount3,',',"","");
				prlGroup->ilMaxOffs = atoi(pclMaxOffset);
				ilCount4 = (int)GetNoOfElements(prlGroup->rlCfg.db_field_len,',');
				memset(pclLastLen,0x00,XS_BUFF);
				GetDataItem(pclLastLen,prlGroup->rlCfg.db_field_len,ilCount4,',',"","");
				prlGroup->ilLastLen = atoi(pclLastLen);

				if (ilCount<1)
				{
				 dbg(TRACE, "CheckCfg: none DB_TABLES specified!");
				 ilRc = RC_FAIL;
				}
				if (ilCount2!=ilCount3 || ilCount2!=ilCount4)
				{
			   dbg(TRACE,
					 "CheckCfg: DB_FIELDS,DB_FIELD_OFFSET or DB_FIELD_LEN inconsistent!");
				 dbg(TRACE,"CheckCfg: setting section to NOT VALID!");
				 ilRc = RC_FAIL;
				}
				ilCount = (int)GetNoOfElements(prlGroup->rlCfg.spec_fields,',');
				ilCount2 = (int)GetNoOfElements(prlGroup->rlCfg.spec_field_rule,',');
				ilCount3 = (int)GetNoOfElements(prlGroup->rlCfg.spec_field_offset,',');
				ilCount4 = (int)GetNoOfElements(prlGroup->rlCfg.spec_field_len,',');
				if (ilCount!=ilCount2 || ilCount!=ilCount3 || ilCount!=ilCount4)
				{
					dbg(TRACE,"CheckCfg: SPEC_FIELDS,SPEC_FIELD_RULE,SPEC_FIELD_OFFSET or SPEC_FIELD_LEN inconsistent!");
					dbg(TRACE,"CheckCfg: setting section to NOT VALID!");
					ilRc = RC_FAIL;
				}
				ilCount = (int)GetNoOfElements(prlGroup->rlCfg.repl_fields,',');
				ilCount2 = (int)GetNoOfElements(prlGroup->rlCfg.replacement,',');
				ilCount3 = (int)GetNoOfElements(prlGroup->rlCfg.reference,',');
				ilCount4 = (int)GetNoOfElements(prlGroup->rlCfg.repl_table,',');
				if (ilCount!=ilCount2 || ilCount!=ilCount3 || ilCount!=ilCount4)
				{
					dbg(TRACE,"CheckCfg: REPL_FIELDS,REPLACEMENT;REFERENCE or REPL_TABLE inconsistent!");
					dbg(TRACE,"CheckCfg: setting section to NOT VALID!");
					ilRc = RC_FAIL;
				}
				ilCount = (int)GetNoOfElements(prlGroup->rlCfg.date_fields,',');
				ilCount2 = (int)GetNoOfElements(prlGroup->rlCfg.date_format,',');
				if (ilCount!=ilCount2)
				{
					dbg(TRACE,"CheckCfg: DATE_FIELDS or DATE_FORMAT inconsistent!");
					dbg(TRACE,"CheckCfg: setting section to NOT VALID!");
					ilRc = RC_FAIL;
				}
				ilCount = (int)GetNoOfElements(prlGroup->rlCfg.db_est_time_field,',');
				if (ilCount>1)
				{
					dbg(TRACE,"CheckCfg: too much DB_EST_TIME_FIELD's specified!");
					dbg(TRACE,"CheckCfg: setting section to NOT VALID!");
					ilRc = RC_FAIL;
				}
				if (strncmp(prlGroup->rlCfg.use_counter_table,"Y",1) != 0 &&
				    strncmp(prlGroup->rlCfg.use_counter_table,"y",1) != 0 &&
				    strncmp(prlGroup->rlCfg.use_counter_table,"N",1) != 0 &&
				    strncmp(prlGroup->rlCfg.use_counter_table,"y",1) != 0)
				{
					dbg(TRACE,"CheckCfg: invalid USE_COUNTER_TABLE setting! Set to default \"N\"!");
				  strncpy(prlGroup->rlCfg.use_counter_table,"N",1);
				}

				/*---------------------------*/
        /* check extended parameters */
				/*---------------------------*/
				ilCount = (int)GetNoOfElements(prlGroup->rlCfg.extd_switch_jfno,',');
				if (ilCount>1)
				{
					dbg(TRACE,"CheckCfg: too much EXTD_SWITCH_JFNOs specified!");
					dbg(TRACE,"CheckCfg: setting section to NOT VALID!");
					ilRc = RC_FAIL;
				}
        else 
				{
				 glExtJfnoAllowed = EXTD_OFF ; 
				 if (strncmp(prlGroup->rlCfg.extd_switch_jfno,"0",1) != 0)  
				                                        glExtJfnoAllowed = EXTD_ON ; 
         if (glExtRemAllowed == EXTD_OFF)  
				 {
					dbg (DEBUG, "CheckCfg: Extended mode for REMARKS is OFF !!!") ; 
         } 
				 else 
				 {
					dbg (DEBUG, "CheckCfg: Extended mode for REMARKS is ON !!!") ; 
         } 
				}
				ilCount = (int)GetNoOfElements(prlGroup->rlCfg.extd_switch_rem,',');
				if (ilCount>1)
				{
					dbg(TRACE,"CheckCfg: too much EXTD_SWITCH_REMs specified!");
					dbg(TRACE,"CheckCfg: setting section to NOT VALID!");
					ilRc = RC_FAIL;
				}
        else 
				{
				 glExtRemAllowed = EXTD_OFF ; 
				 if (strncmp(prlGroup->rlCfg.extd_switch_rem,"0",1) != 0)  
				                                        glExtRemAllowed = EXTD_ON ; 
         if (glExtRemAllowed == EXTD_OFF)  
				 {
					dbg (DEBUG, "CheckCfg: Extended mode for REMARKS is OFF !!!") ; 
         } 
				 else 
				 {
					dbg (DEBUG, "CheckCfg: Extended mode for REMARKS is ON !!!") ; 
         } 
        }

				if ( (glExtRemAllowed == EXTD_ON) && (glExtJfnoAllowed == EXTD_ON) ) 
				{
   			 ilCount = (int)GetNoOfElements(prlGroup->rlCfg.extd_sync,',');
				 if (ilCount>1)
				 {
					dbg(TRACE,"CheckCfg: too much EXTD_SYNCs specified!");
					dbg(TRACE,"CheckCfg: setting section to NOT VALID!");
					ilRc = RC_FAIL;
				 }
        } 

				if ( glExtJfnoAllowed == EXTD_ON ) 
				{
   			 ilCount= (int)GetNoOfElements(prlGroup->rlCfg.extd_time_diff_jfno,',');
				 if (ilCount>1)
				 {
					dbg(TRACE,"CheckCfg: too much EXTD_TIME_DIFF_JFNO specified!");
					dbg(TRACE,"CheckCfg: setting section to NOT VALID!");
					ilRc = RC_FAIL;
				 }

   			 ilCount = (int)GetNoOfElements(prlGroup->rlCfg.extd_max_jfno,',');
				 if (ilCount>1)
				 {
					dbg(TRACE,"CheckCfg: too much EXTD_MAX_JFNO specified!");
					dbg(TRACE,"CheckCfg: setting section to NOT VALID!");
					ilRc = RC_FAIL;
				 }
        } 

				if ( glExtRemAllowed == EXTD_ON ) 
				{
   			 ilCount= (int)GetNoOfElements(prlGroup->rlCfg.extd_time_diff_rem,',');
				 if (ilCount>1)
				 {
					dbg(TRACE,"CheckCfg: too much EXTD_TIME_DIFF_REM specified!") ;
					dbg(TRACE,"CheckCfg: setting section to NOT VALID!") ;
					ilRc = RC_FAIL ;
				 }

   			 ilCount = (int)GetNoOfElements(prlGroup->rlCfg.extd_max_rem,',');
				 if (ilCount>1)
				 {
					dbg(TRACE,"CheckCfg: too much EXTD_MAX_REM specified!");
					dbg(TRACE,"CheckCfg: setting section to NOT VALID!");
					ilRc = RC_FAIL;
				 }
        } 

			}
			else
			{
				dbg(TRACE,"CheckCfg: LCD_IPS=none specified! Doing nothing !");
			}
			prlEle = NULL;
		}
		else
		{
			prlEle = ListFindNext(prgGroupList);
		}
	}
	if (ilRc == RC_FAIL)
	{
		prlGroup->ilValid = FALSE;
	}
	return ilRc;
}

/* ******************************************************************** */
/* The FreeCfgMem() routine	      	                                    */
/* ******************************************************************** */
static void FreeCfgMem(void)
{
	LPLISTELEMENT prlGroupEle = NULL;
	GROUPINFO *prlGroup = NULL;
	LPLISTHEADER prlBoards = NULL;
	LPLISTHEADER prlFlights = NULL;

	prlGroupEle = ListFindFirst(prgGroupList);
	while(prlGroupEle!=NULL)
	{
	 prlGroup = (GROUPINFO*)prlGroupEle->Data;
   prlBoards  = prlGroup->prlBoards ;    /* attemp to adresses */ 
   prlFlights = prlGroup->prlFlightInfo ; 
	 ListDestroy(prlBoards);
/*    FreeMemInFlights(prlFlights) ;  */
	 ListDestroy(prlFlights);   /* !!! to do! SUB memory allocated !!! */

	 /* entries from lcdhdl.cfg file*/
	 free(prlGroup->rlCfg.refresh_cycle);  /* time for refresh */
	 free(prlGroup->rlCfg.switch_cycle);   /* time for switch between the two languages */
	 free(prlGroup->rlCfg.lcd_ips);   /* IP-address(es) or hostname(s) of the LCD-boards */
		free(prlGroup->rlCfg.board_cols);		 /* columns of board */
		free(prlGroup->rlCfg.board_rows); 	 /* rows of board */
		free(prlGroup->rlCfg.service_port);  /* service name for receiving data */
		free(prlGroup->rlCfg.use_counter_table);   /* should ccatab be used to get checkin data or not (=AFTTAB)*/
		free(prlGroup->rlCfg.db_tables);      /* tables*/
		free(prlGroup->rlCfg.db_est_time_field); /* field list from db*/
		free(prlGroup->rlCfg.db_fields);      /* field list from db*/
		free(prlGroup->rlCfg.start_min);      /* where clause for db request*/
		free(prlGroup->rlCfg.end_min);        /* where clause for db request*/
		free(prlGroup->rlCfg.db_cond);        /* where clause for db request*/
		free(prlGroup->rlCfg.db_field_offset);/* offset on LCD board for display */
		free(prlGroup->rlCfg.db_field_len);   /* len to show */
		free(prlGroup->rlCfg.spec_fields);    /* special fields */
		free(prlGroup->rlCfg.spec_field_rule);/* rule for these fields*/
		free(prlGroup->rlCfg.spec_field_offset);/* offset on LCD boards for this fields */
		free(prlGroup->rlCfg.spec_field_len); /* len to display for special fields */
		free(prlGroup->rlCfg.repl_fields);    /* fields to replace with full-texts */
		free(prlGroup->rlCfg.replacement);    /* replacement for field */
		free(prlGroup->rlCfg.reference);      /* connection between the fields */
		free(prlGroup->rlCfg.repl_table);     /* table which is used for resolving */
		free(prlGroup->rlCfg.lang_fields);    /* fields to replace with full-texts */
		free(prlGroup->rlCfg.lang_replacement);    /* replacement for field */
		free(prlGroup->rlCfg.lang_reference);      /* connection between the fields */
		free(prlGroup->rlCfg.lang_repl_table);     /* table which is used for resolving */
		free(prlGroup->rlCfg.date_fields);    /* date fields */
		free(prlGroup->rlCfg.date_format);    /* format of date fields */
		free(prlGroup->rlCfg.repl_board_letter);    /* number of char to replace */
		free(prlGroup->rlCfg.repl_char);    /* char to use for replace */

		free(prlGroup->rlCfg.extd_switch_jfno) ;    /* extended switch jfno */
		free(prlGroup->rlCfg.extd_switch_rem) ;     /* extended switch rem  */
		free(prlGroup->rlCfg.extd_sync) ;           /* extended sync        */
		free(prlGroup->rlCfg.extd_time_diff_jfno) ; /* extended jfno time in secs */
		free(prlGroup->rlCfg.extd_time_diff_rem) ;  /* extended rem time in secs  */
		free(prlGroup->rlCfg.extd_max_jfno) ;       /* extended show count jfno   */
		free(prlGroup->rlCfg.extd_max_rem) ;        /* extended show count rem    */

		prlGroupEle = ListFindNext(prgGroupList);
	}
}







/* ************************************************************** */
/* The ConnectLCDs routine			                           				*/
/* ************************************************************** */
static void ConnectLCDs ()
{
  int	ilRc;      	/*Return code*/
  int	ilSock = 0;	/*Return code*/
	char pclTmpBuf[XS_BUFF];
	LPLISTELEMENT prlGroupEle = NULL;
	LPLISTELEMENT prlBoardEle = NULL;
	GROUPINFO *prlGroup = NULL;
	BOARDINFO *prlBoard = NULL;
	struct servent  *prlService;
	short slBcPort = 0;
	struct in_addr rlIn;
	time_t tlNow = 0;

	prlGroupEle = ListFindFirst(prgGroupList);
	while(prlGroupEle!=NULL)
	{
		prlGroup = (GROUPINFO*)prlGroupEle->Data;
		/***********************************/
		/* (re)connecting if time has come */
		/***********************************/
		tlNow = time(0);                         /* get act. time */
		if (prlGroup->tlNextConnect < tlNow)
		{
		 prlBoardEle = ListFindFirst(prlGroup->prlBoards);
		 while(prlBoardEle!=NULL)
		 {
			/* get the data pointer to the board-infos */
			prlBoard = (BOARDINFO*)prlBoardEle->Data;

	if (glTestMode != 1) 
	{   

			/* only connect not yet established sockets */
			if (prlBoard->ilSocket <= 0)
			{
			 dbg(TRACE,"ConnectLCDs: try to (re)establish board-connection!");
			 /* try to get the socket */
			 errno = 0;
			 ilSock = tcp_create_socket(SOCK_STREAM, NULL); /* create the socket*/
			 if (ilSock < 0)
			 {
				dbg(TRACE,"ConnectLCDs: Create socket failed. Reason <%d>=<%s>.",
																errno, strerror(errno));
				ilRc = RC_FAIL;
			 }
			 else
			 {
				prlBoard->ilSocket = ilSock;
				/* dbg (DEBUG, "ConnectLCDs: Create socket <ID=%d> successfull.", 
											 prlBoard->ilSocket);*/
				if ((prlService=getservbyport(prlBoard->ilPort,NULL)) == NULL)
				{
			   dbg(TRACE, "ConnectLCDs: OS-ERROR getservbyport(): <%d>!"
				    				,prlBoard->ilPort);
				}
				else
				{
			   /* dbg (DEBUG, "ConnectLCDs: LCD-Service=<%s>",prlService->s_name); */
         /* dbg (DEBUG, "ConnectLCDs: Address as value=<%lx>",prlBoard->ilIp);*/
				 rlIn.s_addr = (in_addr_t)(prlBoard->ilIp) ;
				 memset (pclTmpBuf, 0x00,XS_BUFF);
				 strcpy (pclTmpBuf, (char*)inet_ntoa(rlIn)) ;
				 dbg (DEBUG, "ConnectLCDs: Address = <%s>",pclTmpBuf ) ;
				 errno = 0 ;
         alarm (CONNECT_TIMEOUT);
         ilRc = tcp_open_connection(prlBoard->ilSocket,prlService->s_name,
																		pclTmpBuf);
         alarm(0) ;
				 if (ilRc != RC_SUCCESS)
				 {
					dbg(TRACE,
					"ConnectLCDs: CEDA-WARNING: connect to <%s> failed. Reason <%d>=<%s>",
									     	pclTmpBuf, errno, strerror(errno)) ; 
					shutdown(prlBoard->ilSocket,2);
					close(prlBoard->ilSocket);
					prlBoard->ilSocket = -1;
			   }
				 else
				 {
				  dbg (TRACE, "ConnectLCDs: got connection to <%s>.",pclTmpBuf); 
          InitBoard(prlBoard) ;
         }
        }
       }
      }
  }     /* glTestMode */

			prlBoardEle = ListFindNext(prlGroup->prlBoards);
     }
     tlNow = time(0);
     prlGroup->tlNextConnect = tlNow + tgNextTry;
		}

		/*******************************/
		/* refreshing if time has come */
		/*******************************/
		tlNow = time(0);
		if (prlGroup->tlNextRefresh < tlNow)
		{
			PrepareBoardData(prlGroup);
		}
		prlGroupEle = ListFindNext(prgGroupList);
	}
}  /* connectLDCs () */



/* ******************************************************************** */
/* Following the poll_q_and_sock function                               */
/* Waits for input on the socket and polls the QCP for messages         */
/* ******************************************************************** */
static int poll_q_and_sock () 
{
  int ilRc ;
  int ilRc_Connect = RC_FAIL ;
	time_t tlNow ; 

  do
	{
		nap(100) ;
		/*---------------------------*/
		/* now looking on ceda-queue */
		/*---------------------------*/
		ilRc = RC_NOT_FOUND ;
		if (ilRc == RC_NOT_FOUND)
    {
     while ((ilRc=que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen
											,(char *) &prgItem)) == RC_SUCCESS)
			{
				/* depending on the size of the received item  */
				/* a realloc could be made by the que function */
				/* so do never forget to set event pointer !!! */
				prgEvent = (EVENT *) prgItem->text;

				/* Acknowledge the item */
				ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
				if( ilRc != RC_SUCCESS ) 
				{
					/* handle que_ack error */
					HandleQueErr(ilRc);
				}

				switch(prgEvent->command)
				{
				case	HSB_STANDBY	:
					ctrl_sta = prgEvent->command;
					dbg(TRACE,"PQS: received HSB_STANDBY event!");
					HandleQueues();
					break;	
				case	HSB_COMING_UP	:
					ctrl_sta = prgEvent->command;
					dbg(TRACE,"PQS: received HSB_COMING_UP event!");
					HandleQueues();
					break;	
				case	HSB_ACTIVE	:
					ctrl_sta = prgEvent->command;
					dbg(TRACE,"PQS: received HSB_ACTIVE event!");
					break;	
				case	HSB_ACT_TO_SBY	:
					ctrl_sta = prgEvent->command;
					dbg(TRACE,"PQS: received HSB_ACT_TO_SBY event!");
					HandleQueues();
					break;	
				case	HSB_DOWN	:
					/* 	whole system shutdown - do not further use que(), */
					/*	send_message() or timsch() ! */
					ctrl_sta = prgEvent->command;
					dbg(TRACE,"PQS: received HSB_DOWN event!");
					Terminate(FALSE);
					break;	
				case	HSB_STANDALONE	:
					ctrl_sta = prgEvent->command;
					dbg(TRACE,"PQS: received HSB_STANDALONE event!");
					/*ResetDBCounter();*/
					break;	
				case	REMOTE_DB :
					/* ctrl_sta is checked inside */
					/*HandleRemoteDB(prgEvent);*/
					break;
				case	SHUTDOWN	:
					/* process shutdown - maybe from uutil */
					dbg(TRACE,"PQS: received SHUTDOWN event!");
					CloseTCP(0);
					Terminate(FALSE);
					break;
				case	RESET		:
					dbg(TRACE,"PQS: received RESET event!");
					ilRc = Reset();
					if (ilRc == RC_FAIL)
					{
						Terminate(FALSE);
					}
					break;
				case	EVENT_DATA	:
					if((ctrl_sta == HSB_STANDALONE) ||
						(ctrl_sta == HSB_ACTIVE) ||
						(ctrl_sta == HSB_ACT_TO_SBY))
					{
						ilRc = HandleInternalData();
						if(ilRc!=RC_SUCCESS)
						{
							dbg(TRACE,"PQS: HandleInternalData failed <%d>",ilRc);
							HandleErr(ilRc);
						}
					}
					else
					{
						dbg(TRACE,"poll_q_and_sock: wrong HSB-status <%d>",ctrl_sta);
						DebugPrintItem(TRACE,prgItem);
						DebugPrintEvent(TRACE,prgEvent);
					}/* end of if */
					break;
				case	TRACE_ON :
					dbg_handle_debug(prgEvent->command);
					break;
				case	TRACE_OFF :
					dbg_handle_debug(prgEvent->command);
					break;
				default			:
					dbg(TRACE,"MAIN: unknown event");
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;
				} /* end switch */
      } /* end while */ 
    }

		/* now checking for not connected boards */
		/* and for refresh-cycle of boards */
		ConnectLCDs();

		CheckSwitching();

		/* stay in loop */
		ilRc = RC_FAIL;

  }while (ilRc!=RC_SUCCESS);
	return ilRc;
} 





/* ***************************************************************** */
/* The CloseTCP routine                                              */
/* ***************************************************************** */
static void CloseTCP(int ipSock)
{
 LPLISTELEMENT prlGroupEle = NULL;
 LPLISTELEMENT prlBoardEle = NULL;
 GROUPINFO *prlGroup = NULL;
 BOARDINFO *prlBoard = NULL;
 time_t tlNow = 0;

 if (glTestMode == 1) return ; 

 dbg(DEBUG,"CloseTCP: closing socket <%d> connection(s) (0=ALL).");
 prlGroupEle = ListFindFirst(prgGroupList);
 while(prlGroupEle!=NULL)
 {
	prlGroup = (GROUPINFO*)prlGroupEle->Data;
	prlBoardEle = ListFindFirst(prlGroup->prlBoards);
	while(prlBoardEle!=NULL)
	{
   /* get the data pointer to the board-infos */
	 prlBoard = (BOARDINFO*)prlBoardEle->Data;

	 /* close all established sockets */
	  if (ipSock == 0 && prlBoard->ilSocket != 0)
	  {
		 dbg (DEBUG, "CloseTCP: closing TCP/IP connection - socket <%d>!",
								prlBoard->ilSocket);
		 shutdown(prlBoard->ilSocket,2);
		 close(prlBoard->ilSocket);
		 prlBoard->ilSocket = 0;
		 prlBoard->ilAct = -1;
    }
   /* only close a certain established socket */
	 if (ipSock != 0 && prlBoard->ilSocket == ipSock)
	 {
    dbg (DEBUG, "CloseTCP: closing TCP/IP connection - socket <%d>!",
								prlBoard->ilSocket) ;
		shutdown(prlBoard->ilSocket, 2) ;
		close(prlBoard->ilSocket);
		prlBoard->ilSocket = 0;
		prlBoard->ilAct = -1;
   }
	 if  (ipSock == 0)
   {
		prlBoardEle = ListFindNext(prlGroup->prlBoards);
   }
   else
   {
		prlBoardEle = NULL;
   }
	}
	prlGroupEle = ListFindNext(prgGroupList);
 }
}


/* **************************************************************** */
/* The snapit routine                                           */
/* snaps data if the debug-level is set to DEBUG                */
/* **************************************************************** */
static void snapit(void *pcpBuffer,long lpDataLen,FILE *pcpDbgFile)
{
	if (debug_level==DEBUG)
	{
		snap((char*)pcpBuffer,(long)lpDataLen,(FILE*)pcpDbgFile);
	}
}
/*********************************************************************
Function	 :CleanMessage(char *pcpMsg,int ipMsgLen)
Paramter	 :IN:pcpMsg = Message received directly after the socket read.
						IN:ipMsgLen=length of the message
Returnvalue:none
Description:Replaces unallowed bytes from the original telegram
						(especially '\0' and '_') with "allowed" blanks
*********************************************************************/
static void CleanMessage(char *pcpMsg,int ipMsgLen,char cpChar, char cpRepl)
{
 int ilCnt = 0;
 while(ilCnt < ipMsgLen)
 {
	if (pcpMsg[ilCnt]==cpChar)
	{
		pcpMsg[ilCnt]=cpRepl;
	}
	ilCnt++;
 }
}


/* **************************************************************** */
/* The HandleExternalData routine                                   */
/* Checks the data wich have been received from the ADS-system     */
/* **************************************************************** */
static int HandleExternalData(char *pcpData)
{	
	int ilRc = RC_SUCCESS;
	int ilRecvCmd = 0;

	dbg(TRACE,"--- END   HandleExternalData ---");
	return ilRc;
}



/* ******************************************************************** */
/* The FormatDate() routine                                         */
/* ******************************************************************** */
static int FormatDate(char *pcpOldDate,char *pcpFormat,char *pcpNewDate)
{
	int ilRc = RC_SUCCESS;
	int ilCfgChar = 0;
	int ilRightChar = 0;
	int ilDateChar = 0;
	char pclOldDate[20];
	char pclValidLetters[] = "YMDHIS";
	char pclRightFormat[20];
	char *pclTmpDate = NULL;
	
	TrimRight(pcpOldDate);
	memset(pclOldDate,0x00,sizeof(pclOldDate));
	memset(pclRightFormat,0x00,sizeof(pclRightFormat));
	*pcpNewDate = 0x00;
	
	if (strlen(pcpOldDate) > 14)
	{
		return RC_FAIL;	
	}else
	{
		strcpy(pclOldDate,pcpOldDate);
		TrimRight(pclOldDate);
	}

	/* adding '0' until the length of the date is 14byte */ 
	if (strlen(pclOldDate) < 14)
	{
		while(strlen(pclOldDate) < 14)
		{
			pclOldDate[strlen(pclOldDate)] = '0';
			pclOldDate[strlen(pclOldDate)+1] = 0x00;
		}
	}
	
	/* removing all unallowed letters from pcpFormat-string */
	ilCfgChar=0;
	while(ilCfgChar < (int)strlen(pcpFormat))
	{
		if (strchr(pclValidLetters,pcpFormat[ilCfgChar]) != NULL)
		{
			pclRightFormat[ilRightChar] = pcpFormat[ilCfgChar];
			ilRightChar++;
		}
		ilCfgChar++;
	}

	/* now formatting CEDA-time format from pclOldDate to right format */	
	if ((pclTmpDate =
			GetPartOfTimeStamp(pclOldDate,pclRightFormat)) != NULL)
	{
		/* now changing the layout like it is in the cfg-file */	
		ilCfgChar = 0;
		ilRightChar = 0;
		ilDateChar = 0;
		while(ilCfgChar < (int)strlen(pcpFormat))
		{
			if (strchr(pclValidLetters,pcpFormat[ilCfgChar]) != NULL)
			{
				(pcpNewDate)[ilDateChar] = pclTmpDate[ilRightChar];
				ilRightChar++;
				ilDateChar++;
			}
			else
			{
				(pcpNewDate)[ilDateChar] = pcpFormat[ilCfgChar];
				ilDateChar++;
			}
			ilCfgChar++;
		}
		(pcpNewDate)[strlen(pcpFormat)] = 0x00;
	}
	else
	{
		ilRc = RC_FAIL;
	}
	/* dbg(DEBUG,"FormatDate: Old=<%s> New=<%s>",pclOldDate,pcpNewDate);*/
	return ilRc;	
}

/* ******************************************************************** */
/* The TrimRight() routine																						*/
/* ******************************************************************** */
static void TrimRight(char *pcpBuffer)
{
    int i = 0;
    for (i = strlen(pcpBuffer); i > 0 && isspace(pcpBuffer[i-1]); i--)  
        ;
    pcpBuffer[i] = '\0';    
}

/* **************************************************************** */
/* The Send_data routine                                            */
/* Sends (header & data & tail) to the LCD-system                   */
/* **************************************************************** */
static int Send_data(BOARDINFO *prpBoard,char *pcpData,int ipLen)
{
  int  ilRc=RC_SUCCESS ;
  int  ilRc_2=RC_SUCCESS ;
  int  ilBytes = 0 ;
  char cpTstStr[2048] ; 

	/* ilBytes = number of bytes that have to be transmitted. */
	if (ipLen == 0)
	{
	 ilBytes = strlen(pcpData);
	}
	else
	{
	 ilBytes = ipLen;
	}

  if (glTestMode == 1)    /* Output to file LCD*.TST */ 
	{
	 memcpy (cpTstStr, pcpData, ilBytes) ;
	 cpTstStr[ilBytes] = 0x00 ; 
	 if (ilBytes > 79) cpTstStr[79] = 0x00 ; 
	 fprintf(ftest, "-> <%s>\n",cpTstStr) ; 
   fflush (ftest) ; 
	 /* dbg (DEBUG, "Send_data --> <%s>", pcpData) ; */ 
	 return (RC_SUCCESS) ;  
	}
	if (glTCPFile == 1) 
	{
	 memcpy (cpTstStr, pcpData, ilBytes) ;
	 cpTstStr[ilBytes] = 0x00 ; 
	 if (ilBytes > 79) cpTstStr[79] = 0x00 ; 
	 fprintf(ftest, "-> <%s>\n",cpTstStr) ; 
   fflush (ftest) ; 
  }

	if (prpBoard->ilSocket > 0)
	{
		errno = 0;
		alarm(WRITE_TIMEOUT);
		ilRc = write(prpBoard->ilSocket,pcpData,ilBytes);/*unix*/
		alarm(0);
		/* writing the end of line sign (cr,lf) to socket (board) */
		alarm(WRITE_TIMEOUT);
		ilRc_2 = write(prpBoard->ilSocket,pcgLineTerm,2);/*unix*/
		alarm(0);

		if (ilRc == -1)
		{
		 if (bgAlarm == FALSE)
		 {
			dbg(TRACE,"Send_data: Write failed: Socket <%d>! Reason <%d>=<%s> Board<%x>"
			,prpBoard->ilSocket,errno,strerror(errno),prpBoard); 
			ilRc = RC_FAIL;
			shutdown(prpBoard->ilSocket,2);
			close(prpBoard->ilSocket);
			prpBoard->ilSocket = -1;
		 }
			else
			{
				bgAlarm = FALSE;
				ilRc = RC_FAIL;
			}
		}
		else
		{
			/*dbg(DEBUG,"Send_data: wrote <%d> Bytes to socket Nr.<%d>.",ilRc,prpBoard->ilSocket); */
			/*snapit((char *)pcpData,ilBytes,outp);
			fflush(outp);*/
			ilRc = RC_SUCCESS;
		}
	}
	else
	{
		ilRc = RC_FAIL;
	}
  return ilRc;
} 
/* ****************************************************************** */
/* The PrepareBoardData routine                                       */
/* prepares the data after the command for sending                    */
/* ****************************************************************** */
static int PrepareBoardData(GROUPINFO *prpGroup)
{
  int	ilRc = RC_FAIL;	/*Return code*/
  int	ilSock = 0;
  int	ilCnt = 0;	
  int	ilFrom = 0;	
  int	ilTo = 0;	
  int	ilT1 = 0;	
  int	ilT2 = 0;	
  int	ilNrOfFields = 0;	
	time_t tlNow = time(0);
	short slFkt = 0;
	short slCursor = 0;
	char *pclResult = NULL;
	char pclSqlBuf[L_BUFF];
	char pclTmpSqlAnswer[XL_BUFF];
	char pclData[DATABLK_SIZE];
	LPLISTELEMENT prlBoardEle = NULL;
	BOARDINFO *prlBoard = NULL;

	dbg(DEBUG,"PrepareBoardData: --- SECTION NR. <%d> ---",prpGroup->ilGroupNr);
	sprintf(pclSqlBuf,"SELECT URNO,JFNO,%s,%s FROM %s WHERE %s"
		,prpGroup->rlCfg.db_est_time_field
		,prpGroup->rlCfg.db_fields
		,prpGroup->rlCfg.db_tables
		,prpGroup->rlCfg.db_cond);

	ilFrom = atoi(prpGroup->rlCfg.start_min);
	ilTo = atoi(prpGroup->rlCfg.end_min);
	ilT1 = atoi(prpGroup->rlCfg.t_para1);
	ilT2 = atoi(prpGroup->rlCfg.t_para2);
	ReplaceCfgFormat(pclSqlBuf,ilFrom,ilTo,ilT1,ilT2);
	dbg(TRACE,"PrepareBoardData: SQL <%s>",pclSqlBuf);
	slFkt = START; 
	slCursor = 0;
	memset(pclTmpSqlAnswer,0x00,XL_BUFF);
	memset(pclData,0x00,DATABLK_SIZE);
	ilCnt = 0;

	while ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))==RC_SUCCESS && ilCnt<prpGroup->ilGroupRows)
	{
		/* nr. of fields + 3 for (urno,etd) in string */
		ilNrOfFields = field_count(prpGroup->rlCfg.db_fields) + 3;
		BuildItemBuffer(pclTmpSqlAnswer,NULL,ilNrOfFields,",");
		strcat(pclTmpSqlAnswer,"\n");
		strcat(pclData,pclTmpSqlAnswer);
		slFkt = NEXT;
		ilCnt++;
	}
	close_my_cursor(&slCursor);
	pclData[strlen(pclData)-1] = 0x00;
	if ((ilRc = FormatData(prpGroup,pclData)) != RC_SUCCESS)
	{
		dbg(TRACE,"PrepareBoardData: FormatData() failed!");
	}
	else
	{
		if ((ilRc = DistributeData(prpGroup)) != RC_SUCCESS)
		{
			dbg(TRACE,"PrepareBoardData: DistributeData() failed!");
		}
	}
	tlNow = time(0);
	prpGroup->tlNextRefresh = tlNow 
												+ (time_t)(atoi(prpGroup->rlCfg.refresh_cycle));
	return ilRc;
} /* PrepareBoardData() */ 

/* ****************************************************************** */
/* The CheckSwitching routine                                       */
/* switches the languages according to the configuration              */
/* ****************************************************************** */
static void CheckSwitching()
{
	int ilRc = RC_SUCCESS;
	LPLISTELEMENT prlGroupEle = NULL;
	LPLISTELEMENT prlBoardEle = NULL;
	GROUPINFO *prlGroup = NULL;
	BOARDINFO *prlBoard = NULL;
	char pclTmpBuf[XS_BUFF];
	time_t tlNow = 0;
	time_t tlLastChangeTime = 0;
  int ilListCnt ;

	memset(pclTmpBuf,0x00,XS_BUFF);
	prlGroupEle = ListFindFirst(prgGroupList); 
	ilListCnt = -1 ;      /* member the list position from 0..x */ 
	while(prlGroupEle!=NULL)
	{
		ilListCnt++ ;      
		prlGroup = (GROUPINFO*)prlGroupEle->Data;
		tlNow = time(0);
		if ((prlGroup->tlNextPageSwitch < tlNow)
				|| (prlGroup->tlNextExtdFlnoChange < tlNow))
		{
		 if (prlGroup->tlNextPageSwitch < tlNow)
		 {
      dbg (DEBUG, "switching LANGUAGE is activ !!!") ; 
			/* switching the languages */
			if (prlGroup->ilLang  == ENGLISH)
			{
				/*dbg(DEBUG,"CheckSwitching: switch to LOCAL");*/
				prlGroup->ilLang  = LOCAL;
			}
			else
			{
				/*dbg(DEBUG,"CheckSwitching: switch to ENGLISH");*/
				prlGroup->ilLang  = ENGLISH;
			}

			prlBoardEle = ListFindFirst(prlGroup->prlBoards);
			while(prlBoardEle!=NULL)
			{
				prlBoard = (BOARDINFO*)prlBoardEle->Data;

				if ((prlBoard->ilSocket > 0) || (glTestMode == 1))
				{
				 sprintf(pclTmpBuf,
				 "PapiShowPage -boardname \"board1\" -sectorname \"sector1\" -page %d",
							 prlGroup->ilLang) ;
				 if ((ilRc = Send_data(prlBoard,pclTmpBuf,0)) != RC_SUCCESS)
				 {
				  dbg(TRACE,"CheckSwitching: Send_data() failed. Board<%x>",prlBoard);	
				 }
				}
				prlBoardEle = ListFindNext(prlGroup->prlBoards);
			}
		  /* ChangeCSShow (prlGroup, C_WREFRESH) ; now refresh in CCSS direct */ 
			prlGroup->tlNextPageSwitch 
								= tlNow + (time_t)(atoi(prlGroup->rlCfg.switch_cycle));
    
      if (atoi(prlGroup->rlCfg.extd_sync) == 1) 
			{
       /* reset also the time for prlBoard-> flno / remark */
       dbg (DEBUG, "switching SYNC: EXTENT is activ !!!") ; 
			 ChangeCSShow (prlGroup, C_WNEXT) ; 
       if (atoi(prlGroup->rlCfg.extd_time_diff_jfno) > 1 ) 
       {
        prlGroup->tlNextExtdFlnoChange = 
			  	tlNow + (time_t)(atoi(prlGroup->rlCfg.extd_time_diff_jfno)-1) ; 
       }
       else 
       {
        prlGroup->tlNextExtdFlnoChange = 
				    tlNow + (time_t)(atoi(prlGroup->rlCfg.extd_time_diff_jfno)) ; 
       }
       if ((ilListCnt >= 0) && (ilListCnt < C_MAX_CNT_LT))
			 {     /* member the time for the group in array */  
        tgLastTimeArr[ilListCnt] = prlGroup->tlNextExtdFlnoChange ; 
			 }
			}
     } /* if tlNextPageSwitch < tlNow */

		 /*----------------------------------------------*/
		 /* check if code share flight no must be change */
		 /*----------------------------------------------*/
		 if (prlGroup->tlNextExtdFlnoChange < tlNow)
		 {
      dbg (DEBUG, "switching EXTENT is activ !!!") ; 

			ChangeCSShow (prlGroup, C_WNEXT) ; 
      if (atoi(prlGroup->rlCfg.extd_time_diff_jfno) > 1 ) 
      {
       prlGroup->tlNextExtdFlnoChange = 
			 	tlNow + (time_t)(atoi(prlGroup->rlCfg.extd_time_diff_jfno)-1) ; 
      }
      else 
      {
       prlGroup->tlNextExtdFlnoChange = 
			 	tlNow + (time_t)(atoi(prlGroup->rlCfg.extd_time_diff_jfno)) ; 
      }
		 } /* if (prlGroup->tlNextExtdFlnoChange < tlNow) */ 

		} /* if ->tlNextPageSwitch || ->tlNextExtdFlnoChange */ 

		prlGroupEle = ListFindNext(prgGroupList);
	} /* while more groups */
} /* checkSwitching () */




/* ***************************************************************** */
/* The FormatData routine                                            */
/* ***************************************************************** */
static int FormatData(GROUPINFO *prpGroup, char *pcpData)
{
	int ilRc = RC_FAIL;
	int ilCSSet = 0 ; 
	int ilLines = 0;
	int ilCurLine = 0;
	int ilDataLen = 0;
	int ilBuffSize = 0;
	int ilLastByte = 0;
	char pclEstBuf[XS_BUFF];
	char pclUrnoBuf[XS_BUFF];
	char pclTmpBuf[XL_BUFF];
	char *pclTmpPointer = NULL;
	char pclEngResultBuf[XL_BUFF];
	char pclLocalResultBuf[XL_BUFF];
	char *pclTmpPtr = NULL;
	char pclCSStr[JF_BUFF] ;     /* buffer for CS jfno fields */ 

	/* count number of datalines */
	if ((ilLines = (int)GetNoOfElements(pcpData,'\n')) <= 0)
	{
   dbg (TRACE,"FD: GetNoOfElements() returns <%d>",ilLines);
	}
	else
	{
	 dbg (DEBUG,"FD: <%ld> record(s) found!",ilLines);

	 ilBuffSize = (prpGroup->ilGroupRows * (prpGroup->ilMaxOffs
							+ prpGroup->ilLastLen)) + prpGroup->ilGroupRows ;
	 memset(prpGroup->pclEngBuff,0x00,ilBuffSize);
	 memset(prpGroup->pclLocalBuff,0x00,ilBuffSize);

	 /* Clear the flightList */
	 /* FreeMemInFlights (prpGroup->prlFlightInfo) ; */  
	 while (ListDeleteFirst(prpGroup->prlFlightInfo) != NULL)
	 { ; }

	 /* set temporary pointer to data area */
	 pclTmpPtr = pcpData ; 
	 /* over all lines */
	 for (ilCurLine=0; ilCurLine<ilLines; ilCurLine++)
	 {
	  /* get next data for writing... */
	  memset((void*)pclTmpBuf, 0x00, XL_BUFF);
		if ((pclTmpPtr = CopyNextField(pclTmpPtr,'\n', pclTmpBuf)) == NULL)
		{
		 dbg(TRACE,"FD: CopyNextField returns NULL!") ;
		}
		else
		{
		 /* catching the urno from the line */
		 /* and moving buffer to "," after urno */ 
		 memset(pclUrnoBuf,0x00,XS_BUFF);
		 GetDataItem(pclUrnoBuf,pclTmpBuf,1,',',"","");
		 TrimAll(pclUrnoBuf);
		 /*dbg(DEBUG,"FD: URNO of flight <%s>", pclUrnoBuf);*/
		 pclTmpPointer = strchr(pclTmpBuf,',');	
		 pclTmpBuf[0] = 0x00;
		 strcat(pclTmpBuf,++pclTmpPointer);

		 /* catching the JFNO from the line */
		 /* and moving buffer to "," after urno */ 
		 memset(pclCSStr,0x00,JF_BUFF);
		 GetDataItem(pclCSStr,pclTmpBuf,1,',',"","");
		 TrimAll(pclCSStr);
		 dbg(DEBUG,"FD: CS STR of flight <%s>", pclCSStr);
		 pclTmpPointer = strchr(pclTmpBuf,',');	
		 pclTmpBuf[0] = 0x00;
		 strcat(pclTmpBuf,++pclTmpPointer);

		 /* catching the estimated time from the line */
		 /* and moving buffer to "," after estimated time */ 
		 memset(pclEstBuf,0x00,XS_BUFF);
		 GetDataItem(pclEstBuf,pclTmpBuf,1,',',"","");
		 TrimAll(pclEstBuf);
		 pclTmpPointer = strchr(pclTmpBuf,',');	
		 pclTmpBuf[0] = 0x00;
		 strcat(pclTmpBuf,++pclTmpPointer);

	   /* initial. the out buffer with blanks until the necessary output length */
		 ilLastByte = prpGroup->ilMaxOffs + prpGroup->ilLastLen;
		 memset(pclEngResultBuf,0x00,XL_BUFF);
		 memset(pclEngResultBuf,0x20,ilLastByte);
		 memset(pclLocalResultBuf,0x00,XL_BUFF);
		 memset(pclLocalResultBuf,0x20,ilLastByte);
		 if ((ilRc = ConvertData (pclUrnoBuf,prpGroup,pclTmpBuf,pclEngResultBuf,
														  pclLocalResultBuf)) == RC_SUCCESS)
		 {
			ReplaceEstTime(pclEstBuf,pclEngResultBuf);
			ReplaceBoardLetters(prpGroup,pclEngResultBuf);
			ReplaceEstTime(pclEstBuf,pclLocalResultBuf);
			ReplaceBoardLetters(prpGroup,pclLocalResultBuf);
			ChkCSAttach (prpGroup,pclEngResultBuf,pclLocalResultBuf,
									 pclCSStr,&ilCSSet);
			StoreData (prpGroup,pclUrnoBuf,pclEngResultBuf,pclLocalResultBuf,
								 pclCSStr, ilCSSet);
		 }
		 else
		 {
			dbg (DEBUG, "FD: ConvertData() failed!") ;
		 }
	   ilRc = RC_SUCCESS ;
		}
   }
	} 
	return ilRc;
}


/* *****************************************************************/
/* The ConvertData() routine                                       */
/* Converts CEDA-data to the LCD-data telegram format              */
/* *****************************************************************/
static int ConvertData(char* pcpFlightUrno,GROUPINFO *prpGroup,char *pcpLine,
											 char *pcpEngResultBuf,char *pcpLocalResultBuf)
{
	int ilRc = RC_SUCCESS;
	int ilCnt = 0;
	int ilCount = 0;
	int ilPosInFields = 0;
	int ilPosInRepl = 0;
	int ilPosInDate = 0;
	int ilPosInLang = 0;
	int ilPosInLine = 0;
	int ilLinePointer = 0;
	int ilCol = 0;
	int ilFromStart = 0;
	int ilDataLength = 0;
	int ilOffset = 0;

	char pclBuffer[XL_BUFF] ;
	char pclTmpBuf[XS_BUFF];
	char *pclTmpPtr1 = prpGroup->rlCfg.db_fields;
	char *pclTmpPtr2 = NULL;
	char pclData[L_BUFF];
	char pclTmpData[L_BUFF];
	char pclConversionData[L_BUFF];
	char pclDataLength[XS_BUFF];
	char pclLocalLangData[L_BUFF];
	char pclReplData[L_BUFF];
	char pclDate[L_BUFF];
	char pclTmpDate[L_BUFF];
	char pclNewTmpDate[L_BUFF];
	char pclDateFormat[L_BUFF];
	char pclNewDate[L_BUFF];
	char pclReplacement[L_BUFF];
	char pclReference[L_BUFF];
	char pclRepl_Table[L_BUFF];
	char pclSqlBuf[L_BUFF];
	char pclTmpSqlAnswer[XL_BUFF];
	char pclOffset[XS_BUFF];
	time_t tlDate = 0;

	short slFkt = 0;
	short slCursor = 0;

	dbg(DEBUG,"CD: --------------------------------------");
	dbg(DEBUG,"CD: original  line <%s>",pcpLine);
	memset(pclBuffer,0x00,XL_BUFF);

	ilCount = (int)GetNoOfElements(prpGroup->rlCfg.db_fields,',');
	for (ilCnt = 0; ilCnt<ilCount; ilCnt++)
	{
	 pclTmpPtr1 = CopyNextField(pclTmpPtr1,',',pclTmpBuf);
	 FindItemInList(prpGroup->rlCfg.db_fields,pclTmpBuf,',',&ilPosInFields,
									&ilCol,&ilFromStart);
	 FindItemInList(prpGroup->rlCfg.repl_fields,pclTmpBuf,',',&ilPosInRepl,
									&ilCol,&ilFromStart);
	 FindItemInList(prpGroup->rlCfg.date_fields,pclTmpBuf,',',&ilPosInDate,
									&ilCol,&ilFromStart);
	 FindItemInList(prpGroup->rlCfg.lang_fields,pclTmpBuf,',',&ilPosInLang,
									&ilCol,&ilFromStart);

	 GetDataItem(pclConversionData,pcpLine,ilPosInFields,',',"","");
	 dbg(DEBUG,"CD: FIELD<%s> POS<%ld> DATA<%s>", pclTmpBuf, ilPosInFields,
																							 pclConversionData);
	 /*******************************************/
	 /* getting data for local language string */
	 /*******************************************/
	 memset(pclLocalLangData,0x00,L_BUFF);
	 if (ilPosInLang > 0)
	 {
		if (ilPosInDate == 0)
		{
		 /* catching the infos */
		 memset(pclReplacement,0x00,L_BUFF);
		 GetDataItem(pclReplacement,prpGroup->rlCfg.lang_replacement,ilPosInLang,
								 ',',"","");
		 memset(pclReference,0x00,L_BUFF);
		 GetDataItem(pclReference,prpGroup->rlCfg.lang_reference,ilPosInLang,
								 ',',"","");
		 memset(pclRepl_Table,0x00,L_BUFF);
	   GetDataItem(pclRepl_Table,prpGroup->rlCfg.lang_repl_table,ilPosInLang,
								 ',',"","");

		 TrimAll(pclConversionData);
		 if (strlen(pclConversionData) > 0)
		 {
			memset(pclSqlBuf,0x00,L_BUFF);
			sprintf(pclSqlBuf,"SELECT %s FROM %s WHERE %s = '%s'"
				      ,pclReplacement,pclRepl_Table,pclReference,pclConversionData);
			slFkt = START; 
			slCursor = 0;
			memset(pclTmpSqlAnswer,0x00,XL_BUFF);
			dbg(DEBUG,"CD: SQL <%s>",pclSqlBuf);
			if((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))==RC_SUCCESS)
			{
			 TrimAll(pclTmpSqlAnswer);
			 /* converting windows codepage chars to aeg-mis codepage chars */
			 Convert_LCD_Chars((unsigned char*)pclTmpSqlAnswer,igLang);
			 strcpy(pclLocalLangData,pclTmpSqlAnswer);
			}
			close_my_cursor(&slCursor);
		 }
		}
		else
		{
		 dbg (TRACE, "CD: cannot convert date to full text!");
		}
	 }

	 /**********************************************************/
	 /* resolving the necessary fields according to the config */
	 /**********************************************************/
	 if (ilPosInRepl > 0)
	 {
	  if (ilPosInDate == 0)
	  {
		 /* catching the infos */
		 memset(pclReplacement,0x00,L_BUFF);
		 GetDataItem(pclReplacement,prpGroup->rlCfg.replacement,ilPosInRepl,
								 ',',"","");
		 memset(pclReference,0x00,L_BUFF);
		 GetDataItem(pclReference,prpGroup->rlCfg.reference,ilPosInRepl,',',"","");
		 memset(pclRepl_Table,0x00,L_BUFF);
		 GetDataItem(pclRepl_Table,prpGroup->rlCfg.repl_table,ilPosInRepl,
								 ',',"","");

		 TrimAll(pclConversionData);
		 if (strlen(pclConversionData) > 0)
		 {
			memset(pclSqlBuf,0x00,L_BUFF);
			sprintf(pclSqlBuf, "SELECT %s FROM %s WHERE %s = '%s'"
					   ,pclReplacement,pclRepl_Table,pclReference,pclConversionData);
			slFkt = START ; 
			slCursor = 0 ;
			memset(pclTmpSqlAnswer,0x00,XL_BUFF) ;
			dbg(DEBUG,"CD: SQL <%s>",pclSqlBuf);
			if((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))==RC_SUCCESS)
			{
			 TrimAll(pclTmpSqlAnswer);
			 if ((ilRc = SearchStringAndReplace(pcpLine,pclConversionData,
									pclTmpSqlAnswer)) != RC_SUCCESS)
			 {
				dbg(TRACE, "CD: SearchStringAndReplace() failed.");
			 }
			}
			else
			{
				dbg(TRACE, "CD: cannot convert data to fully qualified text!");
			}
			close_my_cursor (&slCursor);
		}
	 }
	}

	/********************/
	/* formatting dates */
	/********************/
	if (ilPosInDate > 0)
	{
	 if (ilPosInRepl == 0)
	 {
		/* get the date contents itself */
	  memset(pclDate,0x00,L_BUFF);
		memset(pclTmpDate,0x00,L_BUFF);
		GetDataItem(pclDate,pcpLine,ilPosInFields,',',"","");
		GetDataItem(pclTmpDate,pcpLine,ilPosInFields,',',"","");
		TrimAll(pclDate);
		TrimAll(pclTmpDate);
		if  (strlen(pclDate) > 0)
		{
			memset(pclNewTmpDate,0x00,L_BUFF);
			UtcToLocal(pclTmpDate,pclNewTmpDate);
			/* get the date format for the field */
			memset(pclDateFormat,0x00,L_BUFF);
			GetDataItem(pclDateFormat,prpGroup->rlCfg.date_format,ilPosInDate,
									',',"","");
			memset(pclNewDate,0x00,L_BUFF);
			if ((ilRc = FormatDate(pclNewTmpDate,pclDateFormat,pclNewDate)) 
								== RC_SUCCESS)
			{
		   if ((ilRc = SearchStringAndReplace(pcpLine,pclDate,pclNewDate))
								 != RC_SUCCESS)
			 {
				dbg(TRACE,"CD: SearchStringAndReplace() and replace failed.");
			 }
			}
		 }
		}
		else
		{
			dbg(TRACE,"CD: cannot convert text into a date format!");
		}
	 }

	/*************************************************/
	/* checking for counter info dedicated or common */
	/*************************************************/
	if ((strncmp(prpGroup->rlCfg.use_counter_table,"Y",1)==0 
			 || strncmp(prpGroup->rlCfg.use_counter_table,"y",1)==0) &&
			(strstr(pclTmpBuf,"CKIF")!=NULL || strstr(pclTmpBuf,"ckif")!=NULL))
	{
	 memset(pclTmpData,0x00,L_BUFF);
	 strcpy(pclTmpData,pclConversionData);
	 TrimAll(pclTmpData);
	      /*if (strlen(pclTmpData)<1) {*/
	 Get_CCA_Counter(pcpFlightUrno,"MIN",pclTmpData);
	 if ((ilRc = SearchStringAndReplace(pcpLine,pclConversionData,pclTmpData))
						 != RC_SUCCESS)
	 {
		dbg (TRACE, "CD: SearchStringAndReplace() and replace failed.");
	 }
			/*}*/
	}

	if ((strncmp(prpGroup->rlCfg.use_counter_table,"Y",1)==0 
			 || strncmp(prpGroup->rlCfg.use_counter_table,"y",1)==0) &&
				(strstr(pclTmpBuf,"CKIT")!=NULL || strstr(pclTmpBuf,"ckit")!=NULL))
	{
		memset(pclTmpData,0x00,L_BUFF);
		strcpy(pclTmpData,pclConversionData);
		TrimAll(pclTmpData);
         		/*if (strlen(pclTmpData)<1) {*/
		Get_CCA_Counter(pcpFlightUrno,"MAX",pclTmpData);
		if ((ilRc = SearchStringAndReplace(pcpLine,pclConversionData,pclTmpData))
							!= RC_SUCCESS)
		{
			dbg(TRACE,"CD: SearchStringAndReplace() and replace failed.");
		}
		           	/*}*/
	}

	/***********************************/
	/* reformat	possible flight-number */
	/***********************************/
	if (strstr (pclTmpBuf, "FLNO")!=NULL || strstr(pclTmpBuf,"flno")!=NULL)
	{
	 memset(pclTmpData,0x00,L_BUFF);
	 strcpy(pclTmpData,pclConversionData);
	 MakeExtFlno(pclTmpData);
	 if ((ilRc = SearchStringAndReplace(pcpLine,pclConversionData,pclTmpData))
							!= RC_SUCCESS)
	 {
		dbg(TRACE,"CD: SearchStringAndReplace() and replace failed.");
	 }
	}

	 /************************************************************/
	 /* setting the field to the configured length (for english) */
	 /************************************************************/
	 memset(pclOffset,0x00,XS_BUFF);
	 GetDataItem(pclOffset,prpGroup->rlCfg.db_field_offset,ilPosInFields,
							',',"","");
	 ilOffset = atoi(pclOffset) ;
	 memset(pclDataLength,0x00,XS_BUFF);
	 GetDataItem(pclDataLength,prpGroup->rlCfg.db_field_len,ilPosInFields,
							',',"","");
	 ilDataLength = atoi(pclDataLength);
	 memset(pclData,0x00,L_BUFF);
	 GetDataItem(pclData,pcpLine,ilPosInFields,',',"","");
	 TrimAll(pclData);

	 /*******************************************/
	 /* writting the english language string.   */
	 /*******************************************/
	 MakeSingleEntry(pclData,ilDataLength,LEFT,' ',
									 (char*)&pcpEngResultBuf[ilOffset]);

	 /************************************************************/
	 /* writting the normal data to the local language string.   */
	 /* if we have the local language we'll just owerwrite it    */
	 /************************************************************/
	 MakeSingleEntry(pclData,ilDataLength,LEFT,' ',
									 (char*)&pcpLocalResultBuf[ilOffset]);
	 if (strlen(pclLocalLangData) > 0)
	 {
	  MakeSingleEntry(pclLocalLangData,ilDataLength,LEFT,' ',
									 (char*)&pcpLocalResultBuf[ilOffset]);
	 }
		/*dbg(DEBUG,"CD: LINE<%s>",pcpLine);*/
 }
 return ilRc ;
} /* ConvertData() */




/* ******************************************************************** */
/*                                                												*/
/* ******************************************************************** */
static void StoreData(GROUPINFO *prpGroup,char *pclUrnoBuf,char *pcpEngData,
											char *pcpLocalData, char *pcpCSStr, int ipCSSet)
{
	int ilRc = RC_SUCCESS;
	int ilMaxCopy = 0;
	int ilAlreadyStoredRecords = 0;
	int ilStore = TRUE;
	char clDel = 0xD;
	FLIGHTINFO rlFlight;
	time_t tlNow = time(0);

	ilStore = TRUE ; 
	/**************************************/
	/* storeing english lines into buffer */
	/**************************************/
	ilAlreadyStoredRecords = GetNoOfElements(prpGroup->pclEngBuff,clDel);
	if (ilAlreadyStoredRecords == prpGroup->ilGroupRows+1)
	{
		dbg(DEBUG,"StoreData: Buffer Limit reached! Storeing rejected!");
		ilStore = FALSE;
	}
	else
	{
		ilMaxCopy = prpGroup->ilLastLen + prpGroup->ilMaxOffs;
		if (strlen(pcpEngData) >= ilMaxCopy)
		{
			strncat(prpGroup->pclEngBuff,pcpEngData,ilMaxCopy);
		}
		else
		{
			strcat(prpGroup->pclEngBuff,pcpEngData);
		}
		strcat(prpGroup->pclEngBuff,(char*)&clDel);
		dbg(DEBUG,"StoreData: ENG <%s>",pcpEngData);
	}

	/************************************************************/
	/* storeing local language lines into local language buffer */
	/************************************************************/
	ilAlreadyStoredRecords = GetNoOfElements(prpGroup->pclLocalBuff,clDel);
	if (ilAlreadyStoredRecords == prpGroup->ilGroupRows+1)
	{
		dbg(DEBUG,"StoreData: Buffer Limit reached! Storeing rejected!");
	}
	else
	{
		ilMaxCopy = prpGroup->ilLastLen + prpGroup->ilMaxOffs;
		if (strlen(pcpLocalData) >= ilMaxCopy)
		{
		 strncat(prpGroup->pclLocalBuff,pcpLocalData,ilMaxCopy);
		}
		else
		{
			strcat(prpGroup->pclLocalBuff,pcpLocalData);
		}
		strcat(prpGroup->pclLocalBuff,(char*)&clDel);
		dbg(DEBUG,"StoreData: LOC <%s>",pcpLocalData);
	}

	/*************************/
	/* storeing the urno,etd */
	/*************************/
	if (ilStore == TRUE)
	{
	 dbg (DEBUG, "StoreData: Store URNO ETD ");
	 memset(&rlFlight,0x00,sizeof(FLIGHTINFO));
	 TrimAll(pclUrnoBuf);
	 rlFlight.urno = atol(pclUrnoBuf);

   /* store remember for CS information */ 
	 rlFlight.prlCodeShareFlnos.ilActivItem = 0 ; 
	 memset (rlFlight.prlCodeShareFlnos.cpPJfno, 0x00, sizeof (JF_BUFF) ) ;
	 if (pcpCSStr[0] != 0x00)
	 {
	  dbg (DEBUG, "StoreData: copy 2 string JFNO = <%s>", pcpCSStr);
		strcpy (rlFlight.prlCodeShareFlnos.cpPJfno, pcpCSStr) ; 
    if (ipCSSet == 1) 
		{
  		rlFlight.prlCodeShareFlnos.ilActivItem = 1 ; /* start with 1.st */ 
      tlNow = time(0) ; 
		  if (atoi(prpGroup->rlCfg.extd_time_diff_jfno) > 1 ) 
		  {
		   prpGroup->tlNextExtdFlnoChange = 
						  	tlNow + (time_t)(atoi(prpGroup->rlCfg.extd_time_diff_jfno)-1) ; 
		  }
		  else 
		  {
		   prpGroup->tlNextExtdFlnoChange = 
							tlNow + (time_t)(atoi(prpGroup->rlCfg.extd_time_diff_jfno)) ; 
      }
		} 
   }

	 if ((ListAppend(prpGroup->prlFlightInfo,&rlFlight)) == NULL)
	 {
	  dbg (TRACE, "StoreData: ListAppend() failed!");
	  Terminate(TRUE) ;
	 }
	}
} /* StoreData () */



/* ********************************************************************* */
/* ChkCSAttach checks if for the LCD Board a code share look is avaiable */
/* and wanted                                                            */
/* cpCSStr is the found string for code share element in database        */
/* ipCSSet = 1 means  codeshare first element is set  else 0             */
/* ********************************************************************* */
static void ChkCSAttach (GROUPINFO *prpGroup, char *pcpEngData, 
												 char *pcpLocalData, char *cpCSStr, int *ipCSSet) 
{
 char cpConvStr[JF_BUFF] ;  
 int  ilRc ; 
 char pclTmpData[L_BUFF+1] ; 

 dbg (DEBUG, "ChkCSAttach: work with src str = <%s>", cpCSStr) ; 
 *ipCSSet = 0 ; 
 if (prpGroup == NULL) return ; 
 if (atoi(prpGroup->rlCfg.extd_switch_jfno) == 1) 
 {
	dbg (DEBUG, "ChkCSAttach: extd_switch_jfno is set") ; 
  if (atoi(prpGroup->rlCfg.extd_max_jfno) >= 1) 
  {
   ilRc = GetSubStr(cpCSStr, cpConvStr, 1, 9) ;  /* get substr */ 
   dbg (DEBUG, "ChkCSAttach: get 1.st SubString = <%s>", cpCSStr) ;
   if (ilRc != RC_SUCCESS)
	 {
    /************************/
    /* no JFNO substr exist */
    /************************/
 /* strcpy(cpCSStr, "A3 126  xOA 1234") ; / * dummy * / 
    *ipCSSet = 1 ;   / * dummy  * / 
    memset(pclTmpData, 0x00, L_BUFF) ;
    memcpy(pclTmpData, pcpEngData, 9) ; 
		strcat(pclTmpData, cpCSStr) ; 
		strcpy(cpCSStr, pclTmpData) ; 
		dbg (DEBUG, "ChkCSAttach: GET DUMMY CS Flight number <%s>", cpCSStr) ; 
*/
		dbg (TRACE, "ChkCSAttach: FOUND NO CODE SHARE SUBSTR") ; 
	 }
	 else /* append the flno to JFNO str */ 
	 {
    memset(pclTmpData, 0x00, L_BUFF) ;
    memcpy(pclTmpData, pcpEngData, 9) ; 
		strcat(pclTmpData, cpCSStr) ; 
		strcpy(cpCSStr, pclTmpData) ; 
    *ipCSSet = 1 ;
	  dbg (TRACE, "ChkCSAttach: GET REAL CS Flight number <%s>", cpCSStr) ; 
	 }
  } 
 } 
} /* ChkCSAttach () */ 


/* ******************************************************************** */
/* Look for code selecting switching                                    */
/* ******************************************************************** */
static void ChangeCSShow (GROUPINFO *prlGroup, int ipToDo)
{
	int ilRc = RC_FAIL;
	int ilNrOfFields;
	int ilLastByte;
	LPLISTELEMENT prlFlightEle = NULL;
	LPLISTELEMENT prlBoardEle = NULL;
	FLIGHTINFO *prlFlight = NULL;
	BOARDINFO  *prlBoard = NULL;
	long llUrnoFromAction = 0;
	char pclEnglData[XL_BUFF];
	char pclLocalData[XL_BUFF];
	char pclSendBuffer[XL_BUFF];
	char pclEstBuf[XS_BUFF];
	char pclTmpBuf[XL_BUFF];
	char *pclTmpPointer = NULL;
	char pclUrno[XXS_BUFF];
  char pclCSStr[JF_BUFF] ; 
  char cpConvStr[JF_BUFF] ;  
  char pclTmpData[L_BUFF+1] ; 
	int  ilAlreadyShownRecords = 0;
	char clDel = 0xD;
	int  ilBreak = FALSE ;
	int  ilNrOfRecords = 0 ;
	int  ilMaxShownRecords = 0 ;
	char pclTmpBuffer_2[XL_BUFF];
	char pclTmpBuffer_1[XL_BUFF];
  int  ilCnt ; 
	int  ilActNo ; 
	int  ilNewBoard = 0 ; 
  int  ilDataLength = 0 ;
	char pclDataLength[XS_BUFF] ; 

	if (prlGroup == NULL) return ;
	if (atoi(prlGroup->rlCfg.extd_switch_jfno) != 1) return ;  
  dbg (DEBUG, "ChangeCSShow -->") ; 

	/* search if any code share must be changed */
	prlFlightEle = ListFindFirst(prlGroup->prlFlightInfo);

	/* get the act. language information .. string */
	ilNrOfRecords = GetNoOfElements(prlGroup->pclEngBuff,clDel) -1;
	ilMaxShownRecords = prlGroup->ilGroupRows;	
	prlBoardEle = ListFindFirst(prlGroup->prlBoards);
  if (prlBoardEle == NULL) return ; 

  ilCnt = 0 ;    /* act line display line */
	ilAlreadyShownRecords = 0 ;
  ilBreak = FALSE ; 

	prlBoard = (BOARDINFO*)prlBoardEle->Data;
  if (prlBoard == NULL) return ;
	ilNewBoard = 0 ; 
	while((prlFlightEle!=NULL) && (prlBoard != NULL)) 
	{
	 prlFlight = (FLIGHTINFO*)prlFlightEle->Data;
	 dbg (DEBUG, "ChangeCSShow: List<%ld>", prlFlight->urno); 
	 /* get next language buffers */
	 if (ilAlreadyShownRecords < ilNrOfRecords)
	 {
	  GetDataItem (pclEnglData, prlGroup->pclEngBuff, ilAlreadyShownRecords+1,
	   						 clDel,"","");
	  GetDataItem (pclLocalData, prlGroup->pclLocalBuff, ilAlreadyShownRecords+1,
	  						 clDel,"","");
   } 
   dbg (DEBUG, "ChangeCSShow: Work with Data = <%s>", pclEnglData) ; 

	 if ((atoi(prlGroup->rlCfg.extd_max_jfno) >= 1) && (ilBreak == FALSE)) 
   {
    dbg (DEBUG, "ilAlready <%ld> < ilNrOfRecs <%ld>", ilAlreadyShownRecords,																		 ilNrOfRecords) ;  
    if (ilAlreadyShownRecords < ilNrOfRecords) 
		 {
      if (prlFlight->prlCodeShareFlnos.cpPJfno[0] != 0x00) 
		  {
       strcpy (pclCSStr, prlFlight->prlCodeShareFlnos.cpPJfno) ; 
       dbg (DEBUG, "ChangeCSShow: Code shares for this are = <%s>", pclCSStr); 
			 ilActNo = prlFlight->prlCodeShareFlnos.ilActivItem ;
       switch (ipToDo)
			 {
				 case C_WNEXT : dbg (DEBUG, "ChangeCSMode: WORK IN NEXT MODE") ;  
												if (ilActNo+1 > atoi(prlGroup->rlCfg.extd_max_jfno)) 
                  			{ 
				                 dbg(DEBUG,"ChangeCSMode: NEXT MODE > RESET TO START"); 
                  			 ilActNo = 1 ; 
		                    } 
			                  else 
			                  {
				                 dbg(DEBUG,"ChangeCSMode: NEXT MODE > SET NEXT POS"); 
			                   ilActNo++ ; 
		                    }
												break ;
         case C_WSTART: ilActNo = 1 ; 
				                dbg(DEBUG,"ChangeCSMode: START MODE > SET ON BEG."); 
												break ; 
         case C_WREFRESH :    /* use act number */
				                dbg(DEBUG,"ChangeCSMode: REFRS MODE > USE SAME NO"); 
												break ;
         default         : ilActNo = 1 ; 
												break ; 
			 } /* switch */

		   ilRc = GetSubStr (pclCSStr, cpConvStr, ilActNo, 9) ;  /* get substr */ 
       TrimAll(cpConvStr); 
			 if ((cpConvStr[0] == 0x00) || (ilRc == RC_FAIL)) 
			 {
        ilActNo = 1 ;  /* reset the substring, while no more CSs */  
		    ilRc = GetSubStr (pclCSStr, cpConvStr, ilActNo, 9) ;  /* get substr */ 
			 }

       if (ilRc == RC_SUCCESS)
	     {
			  /********************************************/
        /* reformat	JFNO substr like flight-number */
        /********************************************/
        memset(pclTmpData, 0x00, L_BUFF) ;
        strcpy(pclTmpData, cpConvStr) ;
        TrimAll(pclTmpData) ; 
				MakeExtFlno(pclTmpData) ;

				/* get changed length for FLNO on LCD */ 
				/* special case - change data on pos 1 = FLNO */
        memset (pclDataLength, 0x00, XS_BUFF) ; 
	    	GetDataItem(pclDataLength, prlGroup->rlCfg.db_field_len,1,',',"","") ; 
        ilDataLength = atoi(pclDataLength) ; 

				MakeSingleEntry(pclTmpData, ilDataLength, LEFT, ' ', pclEnglData) ; 
				MakeSingleEntry(pclTmpData, ilDataLength, LEFT, ' ', pclLocalData) ; 
  
				dbg (DEBUG, "ChangeCSShow: New LCD Str = <%s>", pclEnglData) ; 
	      dbg (DEBUG, "ChangeCSShow: Used Flight number <%s>", cpConvStr) ; 
		
		    ilNewBoard = 1 ;    /* refresh marker for: not a new board   */
				if (prlGroup->ilLang == ENGLISH)    /* gets a better refresh */ 
				{
         /* at first send data to local in the unvisible side */
				 strncpy (pclTmpBuffer_2, pclLocalData, prlBoard->ilCols) ;
				 sprintf (pclSendBuffer, "PapiChrOutText 0 %d -text \"%s\"", ilCnt,
																 pclTmpBuffer_2) ;
	       dbg (DEBUG, "Send 2 unvisible board-> = <%s>", pclSendBuffer) ; 	
				 Send_data (prlBoard, "PapiSelectPage -page 1", 0) ; 
				 Send_data (prlBoard, pclSendBuffer, 0) ; 

				 /* now send data to the visible (english) side */
				 strncpy (pclTmpBuffer_2, pclEnglData, prlBoard->ilCols) ;
				 sprintf (pclSendBuffer, "PapiChrOutText 0 %d -text \"%s\"", ilCnt,
																 pclTmpBuffer_2) ;
	       dbg (DEBUG, "Send 2 visible board-> = <%s>", pclSendBuffer) ; 	
				 Send_data (prlBoard, "PapiSelectPage -page 0", 0) ; 
				}  
				else /* Language = LOCAL */  
				{
         /* at first send data to english in the unvisible side */
				 strncpy (pclTmpBuffer_2, pclEnglData, prlBoard->ilCols) ;
				 sprintf (pclSendBuffer, "PapiChrOutText 0 %d -text \"%s\"", ilCnt,
																 pclTmpBuffer_2) ;
	       dbg (DEBUG, "Send 2 visible board-> = <%s>", pclSendBuffer) ; 	
				 Send_data (prlBoard, "PapiSelectPage -page 0", 0) ; 
				 Send_data (prlBoard, pclSendBuffer, 0) ; 

				 /* now send data to the visible (local) side */
				 strncpy (pclTmpBuffer_2, pclLocalData, prlBoard->ilCols) ;
				 sprintf (pclSendBuffer, "PapiChrOutText 0 %d -text \"%s\"", ilCnt,
																 pclTmpBuffer_2) ;
				 Send_data (prlBoard, "PapiSelectPage -page 1", 0) ; 
        }

				/* send line to LCD display */
				ilRc = Send_data (prlBoard, pclSendBuffer, 0) ; 
				if (ilRc != RC_SUCCESS)
				{
			   dbg(TRACE, "ChangeCSShow: Send_data() failed! Board<%x>",prlBoard);	
			 	 ilBreak = TRUE ;
				}
			  else          /* set to the next flight code share pos. */ 
				{
			   prlFlight->prlCodeShareFlnos.ilActivItem = ilActNo ;
        } 
       } /* if jfno information exists */
      } /* inf != NULL  */
     } /* recs aviable */
    } /* enabled JFNO */ 
		if (ilCnt == prlBoard->ilRows-1)
		{
  	 dbg (DEBUG,
		  "ChangeCSShow: Reached last board-line. Switching to next board.") ; 

     ilCnt = 0 ; 
		 prlBoardEle = ListFindNext(prlGroup->prlBoards) ;
     if (prlBoardEle == NULL)
		 {
		  ilBreak = TRUE ; 
     } 
		 else 
		 {
	    prlBoard = (BOARDINFO*) prlBoardEle->Data ;
		  ilNewBoard = 0 ;    /* reset marker for: found CS on Board */
     } 
		} 
		else
		{
     ilCnt++ ; 
    } 
	  ilAlreadyShownRecords++ ;

	  prlFlightEle = ListFindNext(prlGroup->prlFlightInfo);
	 } /* while */

} /* ChangeCSShow () */



/* ******************************************************************** */
/* GetSubStr                                     												*/
/* ******************************************************************** */
static int GetSubStr(char *pcpSRCStr, char * pcpSUBStr, int ipItem, 
										 int ipDiffLen)
{
 int ilStartPosition ; 
 int ilEndPosition ; 
 int ilLen ; 
 int ilRC = RC_SUCCESS ; 	

 ilStartPosition = (ipItem-1) * ipDiffLen ;
 ilLen = strlen (pcpSRCStr) ; 
 dbg (DEBUG, "LEN SRC STR = <%d>, startpos. = <%d>, DiffLen = <%d>", ilLen,
																	ilStartPosition, ipDiffLen ) ;  
 pcpSUBStr[0]=0x00 ; 
 if ((ilLen < ilStartPosition) || (ilLen == 0))
 {
	ilRC = RC_FAIL ; 
 } 
 else 
 {
  ilEndPosition = ilStartPosition + ipDiffLen - 1 ; 
	dbg (DEBUG, "EndPosition = <%d>", ilEndPosition ) ;  
  ilLen = strlen(&pcpSRCStr[ilStartPosition]) ; 

	if (ilLen > ilEndPosition) 
	{
   memcpy (pcpSUBStr, &pcpSRCStr[ilStartPosition], ipDiffLen) ; 
	 pcpSUBStr[ipDiffLen] = 0x00 ; 
   dbg (DEBUG, "COPY SUBSTR C = <%s>", pcpSUBStr) ; 
	}
	else 
	{
   memset (pcpSUBStr, ' ', ipDiffLen) ; 
   memcpy (pcpSUBStr, &pcpSRCStr[ilStartPosition], ilLen) ; 
	 pcpSUBStr[ipDiffLen] = 0x00 ; 
   dbg (DEBUG, "COPY SUBSTR = <%s>", pcpSUBStr) ; 
  } 
 } 
 return (ilRC) ; 
} /* GetSubStr () */


/* ******************************************************************** */
/*                                               												*/
/* ******************************************************************** */
static int DistributeData(GROUPINFO *prpGroup)
{
	LPLISTELEMENT prlFlightEle = NULL;
	FLIGHTINFO *prlFlight = NULL;
	LPLISTELEMENT prlBoardEle = NULL;
	BOARDINFO *prlBoard = NULL;
	int ilRc = RC_SUCCESS;
	int ilCnt = 0;
	int ilBreak = FALSE;
	int ilNrOfRecords = 0;
	int ilMaxShownRecords = 0;
	int ilAlreadyShownRecords = 0;
	char clDel = 0xD;
	char pclTmpBuffer[XL_BUFF];
	char pclTmpBuffer_2[XL_BUFF];
	char pclSendBuffer[XL_BUFF];
	char pclSpaceBuffer[XL_BUFF];
	struct in_addr rlIn;

	/********************/
	/* ENGLISH language */
	/********************/
	ilNrOfRecords = GetNoOfElements(prpGroup->pclEngBuff,clDel) -1;
	ilMaxShownRecords = prpGroup->ilGroupRows;	
	prlBoardEle = ListFindFirst(prpGroup->prlBoards);
	prlFlightEle = ListFindFirst(prpGroup->prlFlightInfo);
	dbg(TRACE,"DistributeData: ------------------------");
	while(prlBoardEle!=NULL)
	{
		prlBoard = (BOARDINFO*)prlBoardEle->Data;

		if ((prlBoard->ilSocket > 0) || (glTestMode == 1))
		{
	  	rlIn.s_addr = (in_addr_t)(prlBoard->ilIp) ;
		  dbg(TRACE,"DistributeData: send ENGLISH language => <%s>",
								(char*)inet_ntoa(rlIn));

			/* selecting the english page for update "page 0" = english */
			if ((ilRc = Send_data(prlBoard,"PapiSelectPage -page 0",0)) != RC_SUCCESS)
			{
				dbg(TRACE,"DistributeData: Send_data() failed! Board<%x>",prlBoard);	
			}
			else
			{
				ilBreak = FALSE;
				ilCnt = 0;
				while (ilCnt < prlBoard->ilRows && ilBreak == FALSE)
				{
					memset(pclTmpBuffer, 0x00,XL_BUFF);
					memset(pclSendBuffer, 0x00,XL_BUFF);
					if (ilAlreadyShownRecords < ilNrOfRecords)
					{
						GetDataItem(pclTmpBuffer,prpGroup->pclEngBuff,ilAlreadyShownRecords+1,clDel,"","");
						strncpy(pclTmpBuffer_2,pclTmpBuffer,prlBoard->ilCols);
						sprintf(pclSendBuffer,"PapiChrOutText 0 %d -text \"%s\"",ilCnt,pclTmpBuffer_2);
						ilAlreadyShownRecords++;
						if (prlFlightEle != NULL)
						{
							prlFlight = prlFlightEle->Data;
							prlFlight->ilRow = ilCnt;
							prlFlight->prlBoard = prlBoard;
							/*dbg(DEBUG,"DistributeData: FI rec<%d> row<%d> socket<%d>",ilCnt,prlFlight->ilRow,prlBoard->ilSocket);*/
							prlFlightEle = ListFindNext(prpGroup->prlFlightInfo);
						}
					}
					else
					{
						/*MakeSingleEntry(" ",prlBoard->ilCols,LEFT,' ',pclSpaceBuffer);*/
						memset(pclSpaceBuffer,0x20,XL_BUFF);
						pclSpaceBuffer[prlBoard->ilCols] = 0x00;
						sprintf(pclSendBuffer,"PapiChrOutText 0 %d -text \"%s\"",ilCnt,pclSpaceBuffer);
					}
					dbg(DEBUG,"DistributeData: Row<%d> col<0> data<%s>",ilCnt,pclSendBuffer);
					if (( ilRc = Send_data(prlBoard,pclSendBuffer,0)) != RC_SUCCESS)
					{
					 dbg(TRACE,"DistributeData: Send_data() failed! Board<%x>",prlBoard);	
					 ilBreak = TRUE;
					}
					if (ilCnt == prlBoard->ilRows-1)
					{
						/*dbg(DEBUG,"DistributeData: Reached last board-line. Switching to next board.");*/
						/* set new code share time ! */ 
            ilBreak = TRUE;
					}
					ilCnt++;
				}
			}
		}
		else
		{
			dbg(DEBUG,"DistributeData: socket not valid!");
		}
		/*prlBoardEle = NULL;*/	
		prlBoardEle = ListFindNext(prpGroup->prlBoards);
	}

	/********************/
	/* LOCAL language */
	/********************/
	ilAlreadyShownRecords = 0;
	ilNrOfRecords = GetNoOfElements(prpGroup->pclLocalBuff,clDel) -1;
	ilMaxShownRecords = prpGroup->ilGroupRows;	
	prlBoardEle = ListFindFirst(prpGroup->prlBoards);
	while(prlBoardEle!=NULL)
	{
		prlBoard = (BOARDINFO*)prlBoardEle->Data;

		if ((prlBoard->ilSocket > 0) || (glTestMode == 1))
		{
			rlIn.s_addr = (in_addr_t)(prlBoard->ilIp);
			dbg(TRACE,"DistributeData: send LOCAL   language => <%s>",
																							(char*)inet_ntoa(rlIn));

			/* selecting the english page for update "page 1" = local */
			if ((ilRc = Send_data(prlBoard,"PapiSelectPage -page 1",0)) != RC_SUCCESS)
			{
				dbg(TRACE,"DistributeData: Send_data() failed! Board<%x>",prlBoard);	
			}
			else
			{
				ilBreak = FALSE;
				ilCnt = 0;
				while (ilBreak == FALSE)
				{
					memset(pclTmpBuffer,0x00,XL_BUFF);
					memset(pclSendBuffer,0x00,XL_BUFF);
					if (ilAlreadyShownRecords < ilNrOfRecords)
					{
						GetDataItem(pclTmpBuffer,prpGroup->pclLocalBuff,ilAlreadyShownRecords+1,clDel,"","");
						strncpy(pclTmpBuffer_2,pclTmpBuffer,prlBoard->ilCols);
						sprintf(pclSendBuffer,"PapiChrOutText 0 %d -text \"%s\"",ilCnt,pclTmpBuffer_2);
						ilAlreadyShownRecords++;
					}
					else
					{
						/*MakeSingleEntry(" ",prlBoard->ilCols,LEFT,' ',pclSendBuffer);*/
						memset(pclSpaceBuffer,0x20,XL_BUFF);
						pclSpaceBuffer[prlBoard->ilCols] = 0x00;
						sprintf(pclSendBuffer,"PapiChrOutText 0 %d -text \"%s\"",ilCnt,pclSpaceBuffer);
					}
					dbg(DEBUG,"DistributeData: Row<%d> col<0> data<%s>",ilCnt,pclSendBuffer);
					if ((ilRc = Send_data(prlBoard,pclSendBuffer,0)) != RC_SUCCESS)
					{
						dbg(TRACE,"DistributeData: Send_data() failed! Board<%x>",prlBoard);	
						ilBreak = TRUE;
					}
					if (ilCnt == prlBoard->ilRows-1)
					{
						/*dbg(DEBUG,"DistributeData: Reached last board-line. Switching to next board.");*/
						ilBreak = TRUE;
       
						/* set new code share time ! */ 
					}
					ilCnt++;
				}
			}
		}
		else
		{
			dbg(DEBUG,"DistributeData: socket not valid!");
		}
		prlBoardEle = ListFindNext(prpGroup->prlBoards);
	}
	dbg(TRACE,"DistributeData: ------------------------");
	return ilRc;
}





/* ******************************************************************** */
/*                                              												*/
/*                                               												*/
/* ******************************************************************** */
static void MakeSingleEntry(char *pcpSrc,int ipLen,int ipAdjust,char cpFill,
														char *pcpDest)
{
	int ilSrcLen = 0;
	int ilCnt = 0;
	int ilNrOfFillChars = 0;

	ilSrcLen = (int)strlen(pcpSrc);
	if (ilSrcLen >= ipLen)
	{
		strncpy(pcpDest,pcpSrc,ipLen);
	}
	else
	{
		ilNrOfFillChars = ipLen - ilSrcLen;
		switch(ipAdjust)
		{
			case RIGHT:
				while (ilCnt < ilNrOfFillChars)
				{
					memcpy(&pcpDest[ilCnt],&cpFill,1);
					ilCnt++;
				}
				strncat(&pcpDest[ilCnt],pcpSrc,ilSrcLen);
				break;

			case LEFT:
				strncpy(&pcpDest[ilCnt],pcpSrc,ilSrcLen);
				while (ilCnt < ilNrOfFillChars)
				{
					memcpy(&pcpDest[ilSrcLen+ilCnt],&cpFill,1);
					ilCnt++;
				}
				break;
		}
	}
}
/* ******************************************************************** */
/* The StrToTime() routine																	*/
/* Format of Date has to be CEDA-Format - 14 Byte													*/
/* ******************************************************************** */
static int StrToTime(char *pcpTime,time_t *plpTime)
{
	struct tm *_tm;
	time_t now;
	char  _tmpc[6];

	/*dbg(DEBUG,"StrToTime, <%s>",pcpTime);*/
	if (strlen(pcpTime) < 12 )
	{
		*plpTime = time(0L);
		return RC_FAIL;
	} 

	now = time(0L);
	_tm = (struct tm *)localtime(&now);
	_tmpc[2] = '\0';
	_tm -> tm_sec = 0;
	strncpy(_tmpc,pcpTime+10,2);
	_tm -> tm_min = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+8,2);
	_tm -> tm_hour = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+6,2);
	_tm -> tm_mday = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+4,2);
	_tm -> tm_mon = atoi(_tmpc)-1;
	strncpy(_tmpc,pcpTime,4);
	_tmpc[4] = '\0';
	_tm -> tm_year = atoi(_tmpc)-1900;
	_tm -> tm_wday = 0;
	_tm -> tm_yday = 0;
	now = mktime(_tm);
	/*dbg(DEBUG,"StrToTime: %02d.%02d.%04d %02d:%02d Now: %ld",
	_tm->tm_mday,_tm->tm_mon+1,_tm->tm_year+1900,_tm->tm_hour,_tm->tm_min,now);*/
	if (now != (time_t) -1)
	{
		*plpTime = now;
		return RC_SUCCESS;
	}
	*plpTime = time(NULL);
	return RC_FAIL;
}
/* *********************************************************************/
/* The ReadSections() routine                                          */
/* *********************************************************************/
static int ReadSections()
{
	int   ilCnt=0;
	int   ilRc = RC_SUCCESS;
	char  pclNumber[XS_BUFF];

	/* reading [MAIN] section from cfg-file */
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","SECTIONS",CFG_STRING,&pcgSections,CFG_PRINT,""))
			== RC_SUCCESS)
	{
		pcgSectionPointer = pcgSections;
		/*igSections = GetNoOfElements(pcgSections,',');*/
  }
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","TRY_RECONNECT",CFG_STRING,&pcgTryReconnect,CFG_NUM,"60"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
		else
		{
			tgNextTry = (time_t)(atoi(pcgTryReconnect));	
		}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","LANG",CFG_STRING,&pcgLang,CFG_NUM,"0"))
			!= RC_SUCCESS)
		{igLang = 0;}
		else
		{igLang = (int)(atoi(pcgLang));}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","FLIGHT_NR_FORMAT",CFG_STRING,&pcgFlight_nr_format,CFG_PRINT,"3,5,1"))
			!= RC_SUCCESS)
	{
		igAlc=3;
		igFlnr=5;
		igShowSuffix=1;
	}
	else
	{
		for (ilCnt=1;ilCnt<=3;ilCnt++)
		{
			memset(pclNumber,0x00,XS_BUFF);
			GetDataItem(pclNumber,pcgFlight_nr_format,ilCnt,',',"","");
			if ((ilRc = CheckValue("FLIGHT_NR_FORMAT",pclNumber,CFG_NUM)) == RC_SUCCESS)
			{
				switch(ilCnt)
				{
					case 1:
						igAlc = atoi(pclNumber);
						if (igAlc < 0 && igAlc > 3)
						{
							dbg(TRACE,"ReadSections: Invalid length! Set length of airline part of flight-nr to default=3!"); 
						}
						else
						{
							dbg(DEBUG,"ReadSections: length Airline=<%d>",igAlc);	
						}
						break;
					case 2:
						igFlnr = atoi(pclNumber);
						if (igFlnr < 0 && igFlnr > 5)
						{
							dbg(TRACE,"ReadSections: Invalid length! Set length of numerical part of flight-nr to default=5!"); 
						}
						else
						{
							dbg(DEBUG,"ReadSections: length number=<%d>",igFlnr);	
						}
						break;
					case 3:
						igShowSuffix = atoi(pclNumber);
						if (igShowSuffix != 0 && igShowSuffix!=1)
						{
							dbg(TRACE,"ReadSections: Invalid value! Set ShowSuffix to default=1!"); 
						}
						else
						{
							dbg(DEBUG,"ReadSections: show suffix=<%d>",igShowSuffix);	
						}
						break;
				}
			}
		}
	}

	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","DEBUG_LEVEL",CFG_STRING,&pcgDebugLevel,CFG_NUM,"0"))
		== RC_SUCCESS)
	{
		if (atoi(pcgDebugLevel)==0)
		{
			dbg(TRACE,"GetCfgEntry: debug info = OFF after init-phase!");
			igDebugLevel = 0;
		}
		else
		{
			if (atoi(pcgDebugLevel)==1)
			{
				dbg(TRACE,"GetCfgEntry: debug info = TRACE after init-phase!");
				igDebugLevel = TRACE;
			}
			else
			{
				if (atoi(pcgDebugLevel)==2)
				{
					dbg(TRACE,"GetCfgEntry: debug info = DEBUG after init-phase!");
					igDebugLevel = DEBUG;
				}
				else
				{
					dbg(TRACE,"GetCfgEntry: debug info = OFF after init-phase!");
					igDebugLevel = 0;
				}
			}
		}
	}
	else
	{
		dbg(TRACE,"GetCfgEntry: debug info = OFF after init-phase!");
		igDebugLevel = 0;
	}
	return ilRc;
}
/* ******************************************************************** */
/* The GetSection() routine                                             */
/* ******************************************************************** */
static int GetSection(char *pcpSection)
{
	int ilRc = RC_SUCCESS;

	memset((char*)pcpSection,0x00,XS_BUFF);
	GetNextDataItem(pcpSection, &pcgSectionPointer, ",", "\0", "  ");
	if (pcpSection[0] == 0x00)
	{
		ilRc = RC_FAIL;
	}
	else
	{
		igActSection++;
		dbg(DEBUG,"GetSection : Section Nr.<%d>=<%s>",igActSection,pcpSection);
	}
	return ilRc;
}
/* ******************************************************************** */
/* The GetBoardInfos() routine                                             */
/* ******************************************************************** */
static int GetBoardInfos(int ipGroup)
{
	int ilRc = RC_SUCCESS;
	int ilRc_2 = RC_FAIL;
	int ilCount = 0;
	int ilCnt = 0;
	int ilBuffSize = 0;
	char pclTmpBuf[M_BUFF];
	char *pclPtr = NULL;
	char *pclTmpPtr1 = NULL;
	char *pclTmpPtr2 = NULL;
	char *pclTmpPtr3 = NULL;
	char *pclTmpPtr4 = NULL;
	struct servent  *prlService;
	short slBcPort = 0;

	LPLISTELEMENT prlGroupEle = NULL;
	LPLISTELEMENT prlBoardEle = NULL;
	GROUPINFO *prlGroup = NULL;
	BOARDINFO rlBoard;

	prlGroupEle = ListFindFirst(prgGroupList) ;
	while(prlGroupEle!=NULL)
	{
		prlGroup = (GROUPINFO*)prlGroupEle->Data;
		if (prlGroup->ilGroupNr == ipGroup)
		{
			/* fill string for replacing single chars on the boards */
			if ((ilRc = InitReplacements(prlGroup)) != RC_SUCCESS)
			{
				dbg(TRACE,"GetBoardInfos: InitReplacements() failed. Terminating!");	
				return RC_FAIL;
			}
			prlGroup->ilGroupRows = 0;
			pclTmpPtr1 = prlGroup->rlCfg.lcd_ips; 
			pclTmpPtr2 = prlGroup->rlCfg.board_cols; 
			pclTmpPtr3 = prlGroup->rlCfg.board_rows; 
			pclTmpPtr4 = prlGroup->rlCfg.service_port; 
			ilCount = GetNoOfElements(prlGroup->rlCfg.lcd_ips,',');
			dbg(DEBUG,"GetBoardInfos: --- SECTION NR.<%d> ---",prlGroup->ilGroupNr);	
			for (ilCnt=0;ilCnt<ilCount;ilCnt++)
			{
				memset(&rlBoard,0x00,sizeof(BOARDINFO));
				rlBoard.ilSocket = -1;
				memset(pclTmpBuf,0x00,M_BUFF);
				if ((pclTmpPtr1 = CopyNextField(pclTmpPtr1,',',pclTmpBuf)) != NULL)
				{
					TrimAll(pclTmpBuf);
					dbg(DEBUG,"GetBoardInfos: IP-ASCII <%s>",pclTmpBuf);	
					rlBoard.ilIp =(ULONG) inet_addr(pclTmpBuf);
				}
				memset(pclTmpBuf,0x00,M_BUFF);
				if ((pclTmpPtr2 = CopyNextField(pclTmpPtr2,',',pclTmpBuf)) != NULL)
				{
					TrimAll(pclTmpBuf);
					if ((ilRc = CheckValue("COLS",pclTmpBuf,CFG_NUM)) == RC_SUCCESS)
					{
						rlBoard.ilCols = atoi(pclTmpBuf);
						dbg(DEBUG,"GetBoardInfos: COLS <%d>",rlBoard.ilCols);	
					}
				}
				memset(pclTmpBuf,0x00,M_BUFF);
				if ((pclTmpPtr3 = CopyNextField(pclTmpPtr3,',',pclTmpBuf)) != NULL)
				{
					TrimAll(pclTmpBuf);
					if ((ilRc = CheckValue("ROWS",pclTmpBuf,CFG_NUM)) == RC_SUCCESS)
					{
						rlBoard.ilRows = atoi(pclTmpBuf);
						dbg(DEBUG,"GetBoardInfos: ROWS <%d>",rlBoard.ilRows);	
						prlGroup->ilGroupRows = prlGroup->ilGroupRows + rlBoard.ilRows;
						dbg(DEBUG,"GetBoardInfos: ALL ROWS <%d>",prlGroup->ilGroupRows);	
					}
				}
				memset(pclTmpBuf,0x00,M_BUFF);
				if ((pclTmpPtr4 = CopyNextField(pclTmpPtr4,',',pclTmpBuf)) != NULL)
				{
					TrimAll(pclTmpBuf);
					if ((ilRc = CheckValue("PORT",pclTmpBuf,CFG_NUM)) == RC_SUCCESS)
					{

if (glTestMode != 1) 
{
						if ((prlService=getservbyport(atoi(pclTmpBuf),NULL)) == NULL)
						{
							dbg(TRACE,"GetBoardInfos: OS-ERROR getservbyport(): <%d>!"
								,atoi(pclTmpBuf));
						}
						else
						{
							dbg(DEBUG,"GetBoardInfos: LCD-Service=<%s>",prlService->s_name);
								slBcPort = prlService->s_port;
							dbg(DEBUG,"GetBoardInfos: LCD-Port   =<%d>",prlService->s_port);
							rlBoard.ilPort = atoi(pclTmpBuf);
							ilRc_2 = RC_SUCCESS;
						}

} /* glTestMode */

					}
				}
				rlBoard.ilPlan = ilCnt;
				rlBoard.ilAct = -1;
				if ((ListAppend(prlGroup->prlBoards,&rlBoard)) == NULL)
				{
					dbg(TRACE,"GetBoardInfos: ListAppend() failed!");
					Terminate(TRUE);
				}
			}/* end for */	
			ilBuffSize =
				(prlGroup->ilGroupRows * (prlGroup->ilMaxOffs+prlGroup->ilLastLen)) + prlGroup->ilGroupRows;
			dbg(DEBUG,"GetBoardInfos: allocate <%d> bytes for <%d> board lines.",ilBuffSize,prlGroup->ilGroupRows);
			prlGroup->pclEngBuff = (char*)malloc(ilBuffSize);
			prlGroup->pclLocalBuff = (char*)malloc(ilBuffSize);
			prlGroup->tlNextRefresh = 0;
			prlGroup->tlNextConnect = 0;
			prlGroup->tlNextExtdFlnoChange = 0 ;
			prlGroup->tlNextExtdRemarkChange = 0 ;
			prlGroupEle = NULL ;
		}
		prlGroupEle = ListFindNext (prgGroupList);
	}
	return ilRc;
}
/* ******************************************************************** */
/* The TrimAll() routine                                            */
/* ******************************************************************** */
static void TrimAll(char *pcpBuffer)
{
	const char *p = pcpBuffer;
	int i = 0;

	for (i = strlen(pcpBuffer); i > 0 && isspace(pcpBuffer[i-1]); i--);
			pcpBuffer[i] = '\0';
	for ( *p ; isspace(*p); p++);
	while ((*pcpBuffer++ = *p++) != '\0');
}
/* ******************************************************************** */
/* The DumpBoardInfos() routine                                            */
/* ******************************************************************** */
static void DumpBoardInfos(void)
{
	LPLISTELEMENT prlGroupEle = NULL;
	LPLISTELEMENT prlBoardEle = NULL;
	GROUPINFO *prlGroup = NULL;
	BOARDINFO *prlBoard = NULL;
	struct in_addr rlIn;

	if (glTestMode == 1) return ; 

	prlGroupEle = ListFindFirst(prgGroupList);
	while(prlGroupEle!=NULL)
	{
		prlGroup = (GROUPINFO*)prlGroupEle->Data;
		prlBoardEle = ListFindFirst(prlGroup->prlBoards);
		while(prlBoardEle!=NULL)
		{
			prlBoard = (BOARDINFO*)prlBoardEle->Data;
			rlIn.s_addr = (in_addr_t)(prlBoard->ilIp);
			dbg(DEBUG,"BOARD: SOCK<%d>,PORT<%d>,IP<%s>,COLS<%d>,ROWS<%d>,PLAN<%d>,ACT<%d>"
				,prlBoard->ilSocket ,prlBoard->ilPort,(char*)inet_ntoa(rlIn),prlBoard->ilCols 
				,prlBoard->ilRows ,prlBoard->ilPlan ,prlBoard->ilAct);
			prlBoardEle = ListFindNext(prlGroup->prlBoards);
		}
		prlGroupEle = ListFindNext(prgGroupList);
	}
}
/* ******************************************************************** */
/* The InitBoard() routine                                            */
/* ******************************************************************** */
static void InitBoard(BOARDINFO *prpBoard)
{
	/* Restart terminal */
	Send_data(prpBoard,"PapiClearPanels",0);
	sleep(5);
	Send_data(prpBoard,"PapiGetPanelCfg -boardname \"board1\" -panelname \"panel1\"",0);
	Send_data(prpBoard,"PapiGetSoftwareVersion",0);
	Send_data(prpBoard,"PapiGetDefectiveLines -boardname \"board1\" -panelname \"panel1\"",0);
	Send_data(prpBoard,"PMSetPanelClearTimeout 30",0);

	#if 0
	/* Get Status */
	Send_data(prpBoard,"PapiGetOperativeStatus -boardname \"board1\" -panelname \"panel1\"",0);
	Send_data(prpBoard,"PapiSelectPage -page 0",0);
	Send_data(prpBoard,"PapiShowPage -boardname \"board1\" -sectorname \"sector1\" -page 0",0);
	Send_data(prpBoard,"PapiSelectPage -page 1",0);
	Send_data(prpBoard,"PapiShowPage -boardname \"board1\" -sectorname \"sector1\" -page 1",0);
	Send_data(prpBoard,"PapiProgPageSequence -boardname \"board1\" -sectorname \"sector1\" -page 0 -time 10000",0);
	Send_data(prpBoard,"PapiProgPageSequence -boardname \"board1\" -sectorname \"sector1\" -page 1 -time 10000",0);
	Send_data(prpBoard,"PapiActivatePageSequence",0);
	#endif
}

/* ******************************************************************** */
/* The UpdateBoardData() routine                                        */
/* ******************************************************************** */
static void UpdateBoardData(char *pcpUrno,char *pcpSelection)
{
	int ilRc = RC_FAIL;
	int ilNrOfFields;
	int ilLastByte;
	LPLISTELEMENT prlGroupEle = NULL;
	LPLISTELEMENT prlFlightEle = NULL;
	GROUPINFO *prlGroup = NULL;
	FLIGHTINFO *prlFlight = NULL;
	long llUrnoFromAction = 0;
	short slFkt = 0;
	short slCursor = 0;
	char pclSqlBuf[L_BUFF];
	char pclTmpSqlAnswer[XL_BUFF];
	char pclData[DATABLK_SIZE];
	char pclEngResultBuf[XL_BUFF];
	char pclLocalResultBuf[XL_BUFF];
	char pclSendBuffer[XL_BUFF];
	char pclEstBuf[XS_BUFF];
	char pclTmpBuf[XL_BUFF];
	char *pclTmpPointer = NULL;
	char pclUrno[XXS_BUFF];

	TrimAll(pclUrno);
	llUrnoFromAction = atol(pclUrno);
	prlGroupEle = ListFindFirst(prgGroupList);
	while(prlGroupEle!=NULL)
	{
		prlGroup = (GROUPINFO*)prlGroupEle->Data;
		prlFlightEle = ListFindFirst(prlGroup->prlFlightInfo);
		while(prlFlightEle!=NULL)
		{
			prlFlight = (FLIGHTINFO*)prlFlightEle->Data;
			/*dbg(DEBUG,"UpdateBoardData: List<%ld> Action<%d>",prlFlight->urno,llUrnoFromAction);*/
			if (prlFlight->urno == llUrnoFromAction)
			{
				sprintf(pclSqlBuf,"SELECT %s,%s FROM %s %s"
					,prlGroup->rlCfg.db_est_time_field
					,prlGroup->rlCfg.db_fields
					,prlGroup->rlCfg.db_tables
					,pcpSelection);
				dbg(DEBUG,"UpdateBoardData: SQL <%s>",pclSqlBuf);
				slFkt = START; 
				slCursor = 0;
				memset(pclTmpSqlAnswer,0x00,XL_BUFF);
				memset(pclData,0x00,DATABLK_SIZE);
				if ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))==RC_SUCCESS)
				{
					/* nr. of fields + 1 for estimated time in string */
					ilNrOfFields = field_count(prlGroup->rlCfg.db_fields) + 1;
					BuildItemBuffer(pclTmpSqlAnswer,NULL,ilNrOfFields,",");
					dbg(DEBUG,"UpdateBoardData: <%s>",pclTmpSqlAnswer);

					/* catching the estimated time from the line */
					/* and moving buffer to "," after estimated time */ 
					memset(pclEstBuf,0x00,XS_BUFF);
					GetDataItem(pclEstBuf,pclTmpSqlAnswer,1,',',"","");
					TrimAll(pclEstBuf);
					pclTmpPointer = strchr(pclTmpSqlAnswer,',');	
					pclTmpSqlAnswer[0] = 0x00;
					strcat(pclTmpSqlAnswer,++pclTmpPointer);

					/* initializing the out buffer with blanks until the necessary output length */
					ilLastByte = prlGroup->ilMaxOffs + prlGroup->ilLastLen;
					memset(pclEngResultBuf,0x00,XL_BUFF);
					memset(pclEngResultBuf,0x20,ilLastByte);
					memset(pclLocalResultBuf,0x00,XL_BUFF);
					memset(pclLocalResultBuf,0x20,ilLastByte);
					memset(pclUrno,0x00,XXS_BUFF);
					sprintf(pclUrno,"%s",prlFlight->urno);
					if ((ilRc = ConvertData(pclUrno,prlGroup,pclTmpSqlAnswer,
								 pclEngResultBuf,pclLocalResultBuf)) == RC_SUCCESS)
					{
						/***********/
						/* ENGLISH */
						/***********/
						/* Replacing time format chars with real data */
						ReplaceEstTime(pclEstBuf,pclEngResultBuf);
						ReplaceBoardLetters(prlGroup,pclEngResultBuf);

						/* selecting the english page for update "page 0" = english */
						if ((ilRc = Send_data(prlFlight->prlBoard,"PapiSelectPage -page 0",0)) != RC_SUCCESS)
						{	
							dbg(TRACE,"UpdateBoardData:  ENGLISH Send_data() failed! Board<%x>.",prlFlight->prlBoard);
						}
						else
						{
							memset(pclSendBuffer,0x00,XL_BUFF);
							sprintf(pclSendBuffer,"PapiChrOutText 0 %d -text \"%s\"",prlFlight->ilRow,pclEngResultBuf);
							dbg(DEBUG,"UpdateBoardData: ENGLISH Row<%d> col<0> data<%s>",prlFlight->ilRow,pclSendBuffer);
							if ((ilRc = Send_data(prlFlight->prlBoard,pclSendBuffer,0)) != RC_SUCCESS)
							{
								dbg(TRACE,"UpdateBoardData:  ENGLISH Send_data() failed! Board<%x>.",prlFlight->prlBoard);
							}
						}
						/*********/
						/* LOCAL */
						/*********/
						/* Replacing time format chars with real data */
						ReplaceEstTime(pclEstBuf,pclLocalResultBuf);
						ReplaceBoardLetters(prlGroup,pclLocalResultBuf);

						/* selecting the loacl page for update "page 1" = local */
						if ((ilRc = Send_data(prlFlight->prlBoard,"PapiSelectPage -page 1",0)) != RC_SUCCESS)
						{
							dbg(TRACE,"UpdateBoardData:  LOCAL Send_data() failed! Board<%x>.",prlFlight->prlBoard);
						}
						else
						{
							memset(pclSendBuffer,0x00,XL_BUFF);
							sprintf(pclSendBuffer,"PapiChrOutText 0 %d -text \"%s\"",prlFlight->ilRow,pclLocalResultBuf);
							dbg(DEBUG,"UpdateBoardData: LOCAL   Row<%d> col<0> data<%s>",prlFlight->ilRow,pclSendBuffer);
							if ((ilRc = Send_data(prlFlight->prlBoard,pclSendBuffer,0)) != RC_SUCCESS)
							{
								dbg(TRACE,"UpdateBoardData:  LOCAL Send_data() failed! Board<%x>.",prlFlight->prlBoard);
							}
						}
					}
				}
				close_my_cursor(&slCursor);
			}
			prlFlightEle = ListFindNext(prlGroup->prlFlightInfo);
		}
		/* ChangeCSShow (prlGroup, C_WREFRESH) ; */  
		prlGroupEle = ListFindNext(prgGroupList);
	}
}



/* ******************************************************************** */
/* The TimeToStr() routine																	*/
/* ******************************************************************** */
static void TimeToStr(char *pcpTime,time_t lpTime,int ipType,int ipLocal)
{
	int ilRc = 0;
	char pclTime[20];
	struct tm *_tm;
	struct tm rlTm;

	*pclTime = 0x00;
	*pcpTime = 0x00;
	if (lpTime == 0)
	{
		lpTime = time(NULL);
	}
	lpTime = lpTime;
	switch (ipLocal)
	{
		case 0:
			_tm = (struct tm *) localtime(&lpTime);
			break;
		case 1:
			_tm = (struct tm *) gmtime(&lpTime);
			break;
	}
	rlTm = *_tm;
	switch(ipType)
	{
		case 0: /* Returns "now" in CEDA-format YYYYMMDDHHMMSS */
			/* Unusal format because of sccs !! */
			strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTm);
			/*dbg (DEBUG,"NOW-TimeToStr : return <%s>",pcpTime);*/
			break;
		case 1:/* Returns lpTime in CEDA-format YYYYMMDDHHMMSS */
			/* Unusal format because of sccs !! */
			strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTm);
			dbg(DEBUG,"CEDA-TimeToStr: return <%s>",pcpTime);
			break;
		default:
			dbg(TRACE,"TimeToStr : CEDA-ERROR: unknown time-type received!");
			break;
	}
}

static void ReplaceEstTime(char *pcpEstBuf,char *pcpBuffer)
{
	char pclTmpEstTime[XS_BUFF];
	char pclNewEstTime[XS_BUFF];
	char pclTime[6];
	char *pclTmpPointer = NULL;
	char *pclTmpPointer2 = NULL;

	char *pclPointer  = strstr(pcpBuffer,"HH:MM");
	char *pclPointer1 = strstr(pcpBuffer,"hh:mm");
	char *pclPointer2 = strstr(pcpBuffer,"HH:mm");
	char *pclPointer3 = strstr(pcpBuffer,"hh:MM");

	char *pclPointer4 = strstr(pcpBuffer,"HHMM");
	char *pclPointer5 = strstr(pcpBuffer,"hhmm");
	char *pclPointer6 = strstr(pcpBuffer,"HHmm");
	char *pclPointer7 = strstr(pcpBuffer,"hhMM");

	if (pclPointer!= NULL)
	{
		pclTmpPointer = pclPointer;	
	}
	else
	{
		if (pclPointer1!= NULL)
		{
			pclTmpPointer = pclPointer1;	
		}
		else
		{
			if (pclPointer2!= NULL)
			{
				pclTmpPointer = pclPointer2;	
			}
			else
			{
				if (pclPointer3!= NULL)
				{
					pclTmpPointer = pclPointer3;	
				}
				else
				{
					pclTmpPointer = NULL;	
				}
			}
		}
	}

	if (pclPointer4!= NULL)
	{
		pclTmpPointer2 = pclPointer4;	
	}
	else
	{
		if (pclPointer5!= NULL)
		{
			pclTmpPointer2 = pclPointer5;	
		}
		else
		{
			if (pclPointer6!= NULL)
			{
				pclTmpPointer2 = pclPointer6;	
			}
			else
			{
				if (pclPointer7!= NULL)
				{
					pclTmpPointer2 = pclPointer7;	
				}
				else
				{
					pclTmpPointer2 = NULL;	
				}
			}
		}
	}

	if (pclTmpPointer != NULL)
	{
		if (strlen(pcpEstBuf)>0)
		{
			memset(pclTmpEstTime,0x00,XS_BUFF);
			strcpy(pclTmpEstTime,pcpEstBuf);
			memset(pclNewEstTime,0x00,XS_BUFF);
			UtcToLocal(pclTmpEstTime,pclNewEstTime);
			memset(pclTime,0x00,6);
			strncpy(pclTime,(char*)&pclNewEstTime[8],2);
			strncat(pclTime,":",1);
			strncat(pclTime,(char*)&pclNewEstTime[10],2);
			strncpy(pclTmpPointer,pclTime,5);
		}
		else
		{
			strncpy(pclTmpPointer,"     ",5);
		}
	}

	if (pclTmpPointer2 != NULL)
	{
		if (strlen(pcpEstBuf)>0)
		{
			memset(pclTmpEstTime,0x00,XS_BUFF);
			strcpy(pclTmpEstTime,pcpEstBuf);
			memset(pclNewEstTime,0x00,XS_BUFF);
			UtcToLocal(pclTmpEstTime,pclNewEstTime);
			memset(pclTime,0x00,6);
			strncpy(pclTime,(char*)&pclNewEstTime[8],2);
			strncat(pclTime,(char*)&pclNewEstTime[10],2);
			strncpy(pclTmpPointer2,pclTime,4);
		}
		else
		{
			strncpy(pclTmpPointer2,"     ",4);
		}
	}
}
static int InitReplacements(GROUPINFO *prpGroup)
{
	int ilRc = RC_SUCCESS;	
	int ilCount = 0;
	int ilByte = 0;
	int ilMaxLen = 0;
	int ilNrOfLetters = 0;
	char pclByte[M_BUFF];
	char pclChar[M_BUFF];

	ilMaxLen = (prpGroup->ilMaxOffs+prpGroup->ilLastLen);

	if ((prpGroup->pclReplacements =
		(char*)(malloc(ilMaxLen + 4))) == NULL)
	{
		dbg(TRACE,"InitReplacements: Not enough memory to at init-time.");
		dbg(TRACE,"Terminating");
		ilRc = RC_FAIL;
	}
	else
	{
		memset(prpGroup->pclReplacements,0x00,ilMaxLen+4);

		ilNrOfLetters = field_count(prpGroup->rlCfg.repl_board_letter);
		for (ilCount = 1; ilCount <= ilNrOfLetters; ilCount++)
		{
			memset(pclByte,0x00,M_BUFF);

			GetDataItem(pclByte,prpGroup->rlCfg.repl_board_letter
				,ilCount,',',"","");
				TrimAll(pclByte);

			if ((ilRc = CheckValue("REPL_BOARD_LETTER",pclByte,CFG_NUM))
				!= RC_SUCCESS)
			{
				dbg(TRACE,"IR: correct REPL_BOARD_LETTERS!");
			}
			else
			{
				ilByte = atoi(pclByte);
				if  (ilByte <= ilMaxLen)
				{
					memset(pclChar,0x00,M_BUFF);
					GetDataItem(pclChar,prpGroup->rlCfg.repl_char,
						ilCount,',',"","");
					TrimAll(pclChar);
					if (strlen(pclChar) > 0)
					{
						prpGroup->pclReplacements[ilByte] = pclChar[0];	
						dbg(TRACE,"InitReplacement: Board-letter Nr. <%d>=<%c>",ilByte,*pclChar);
					}
				}
				else
				{
					dbg(TRACE,"IR: Value to large. Cannot replace letter!");
				}
			}
		}
	}
	return ilRc;
}
static void ReplaceBoardLetters(GROUPINFO *prpGroup,char *pcpBuffer)
{
	int ilMaxLen = (prpGroup->ilMaxOffs+prpGroup->ilLastLen);
	int ilCnt = 0;

	while(ilCnt < ilMaxLen)
	{
		if (prpGroup->pclReplacements[ilCnt] != 0x00)
		{
			pcpBuffer[ilCnt] = prpGroup->pclReplacements[ilCnt];	
			dbg(DEBUG,"RBL: BUFF<%s>",pcpBuffer);
		}
		ilCnt++;	
	}
}
/* ******************************************************************** */
/* The UtcToLocal(char *pcpTime) routine 				*/
/* ******************************************************************** */
static int UtcToLocal(char *oldstr, char *newstr) 
{
  int c;
  char year[5], month[3], day[3], hour[3], minute[3],second[3];
  struct tm TimeBuffer, *final_result;
  time_t time_result;
	/*dbg(DEBUG,"UtcToLocal: IN <%s>",oldstr);*/

	/********** Extract the Year off CEDA timestamp **********/
  for(c=0; c<= 3; ++c)
    {
      year[c] = oldstr[c];
    }
  year[4] = '\0';
	/********** Extract month, day, hour and minute off CEDA timestamp **********/
  for(c=0; c <= 1; ++c)
    {
      month[c]  = oldstr[c + 4];
      day[c]    = oldstr[c + 6];
      hour[c]   = oldstr[c + 8];
      minute[c] = oldstr[c + 10];
      second[c] = oldstr[c + 12];
    }
	/********** Terminate the Buffer strings **********/
  month[2]  = '\0';
  day[2]    = '\0';
  hour[2]   = '\0';
  minute[2] = '\0';
  second[2] = '\0';


	/***** Fill a broken-down time structure incl. string to integer *****/
  TimeBuffer.tm_year  = atoi(year) - 1900;
  TimeBuffer.tm_mon   = atoi(month) - 1;
  TimeBuffer.tm_mday  = atoi(day);
  TimeBuffer.tm_hour  = atoi(hour);
  TimeBuffer.tm_min   = atoi(minute);
  TimeBuffer.tm_sec   = atoi(second);
  TimeBuffer.tm_isdst = 0;
	/***** Make secondbased timeformat and correct mktime *****/
  time_result = mktime(&TimeBuffer) - timezone;
	/***** Reconvert into broken-down time structure *****/
  final_result = localtime(&time_result);

  sprintf(newstr,"%d%.2d%.2d%.2d%.2d%.2d"
		,final_result->tm_year+1900
		,final_result->tm_mon+1
		,final_result->tm_mday
		,final_result->tm_hour
		,final_result->tm_min
		,final_result->tm_sec);

/*dbg(DEBUG,"UtcToLocal: OUT<%s>",newstr);*/
  return(0); /**** DONE WELL ****/
}
static void InitGreekCharset()
{
	dbg(DEBUG,"InitCharsets: character conversion table [GREEK]!");
	memset(pcgGreekChars,0x00,S_BUFF);
	/***********************************************/
	/* Uppercase conversion: Left= Dec. Nr in Windows-codepage  ------ right: Dec. AEG-LCD-codepage */
	/***********************************************/
	pcgGreekChars[124]=179;
	pcgGreekChars[162]=134;
	pcgGreekChars[163]=156;
	pcgGreekChars[166]=138;
	pcgGreekChars[167]=245;
	pcgGreekChars[169]=151;
	pcgGreekChars[172]=137;
	pcgGreekChars[173]=136;
	pcgGreekChars[175]=22;
	pcgGreekChars[177]=241;
	pcgGreekChars[178]=153;
	pcgGreekChars[179]=154;
	pcgGreekChars[180]=139;
	pcgGreekChars[181]=230;
	pcgGreekChars[182]=20;
	pcgGreekChars[184]=140;
	pcgGreekChars[185]=143;
	pcgGreekChars[188]=146;
	pcgGreekChars[189]=171;
	pcgGreekChars[190]=149;
	pcgGreekChars[192]=161;
	pcgGreekChars[193]=164;
	pcgGreekChars[194]=165;
	pcgGreekChars[195]=166;
	pcgGreekChars[196]=167;
	pcgGreekChars[197]=168;
	pcgGreekChars[198]=169;
	pcgGreekChars[199]=170;
	pcgGreekChars[200]=172;
	pcgGreekChars[201]=173;
	pcgGreekChars[202]=181;
	pcgGreekChars[203]=182;
	pcgGreekChars[204]=183;
	pcgGreekChars[205]=184;
	pcgGreekChars[206]=189;
	pcgGreekChars[207]=190;
	pcgGreekChars[208]=198;
	pcgGreekChars[209]=199;
	pcgGreekChars[211]=207;
	pcgGreekChars[212]=208;
	pcgGreekChars[213]=209;
	pcgGreekChars[214]=210;
	pcgGreekChars[215]=211;
	pcgGreekChars[216]=212;
	pcgGreekChars[217]=213;
	pcgGreekChars[218]=145;
	pcgGreekChars[219]=150;
	pcgGreekChars[220]=164;
	pcgGreekChars[221]=168;
	pcgGreekChars[222]=170;
	pcgGreekChars[223]=173;
	pcgGreekChars[224]=252;
	/* lowercase will be uppercase */
	pcgGreekChars[225]=164;
	pcgGreekChars[226]=165;
	pcgGreekChars[227]=166;
	pcgGreekChars[228]=167;
	pcgGreekChars[229]=168;
	pcgGreekChars[230]=169;
	pcgGreekChars[231]=170;
	pcgGreekChars[232]=172;
	pcgGreekChars[233]=173;
	pcgGreekChars[234]=181;
	pcgGreekChars[235]=182;
	pcgGreekChars[236]=183;
	pcgGreekChars[237]=184;
	pcgGreekChars[238]=189;
	pcgGreekChars[239]=190;
	pcgGreekChars[240]=198;
	pcgGreekChars[241]=199;
	pcgGreekChars[242]=207;
	pcgGreekChars[243]=207;
	pcgGreekChars[244]=208;
	pcgGreekChars[245]=209;
	pcgGreekChars[246]=210;
	pcgGreekChars[247]=211;
	pcgGreekChars[248]=212;
	pcgGreekChars[249]=213;
	pcgGreekChars[250]=145;
	pcgGreekChars[251]=150;
	pcgGreekChars[252]=190;
	pcgGreekChars[253]=209;
	pcgGreekChars[254]=213;
	/***********************************************/
	#if 0 /* lower and uppercase letter conversion */
	pcgGreekChars[45]=240;
	pcgGreekChars[124]=179;
	pcgGreekChars[162]=134;
	pcgGreekChars[163]=156;
	pcgGreekChars[166]=138;
	pcgGreekChars[167]=245;
	pcgGreekChars[169]=151;
	pcgGreekChars[172]=137;
	pcgGreekChars[173]=136;
	pcgGreekChars[175]=22;
	pcgGreekChars[175]=142;
	pcgGreekChars[177]=241;
	pcgGreekChars[178]=153;
	pcgGreekChars[179]=154;
	pcgGreekChars[180]=139;
	pcgGreekChars[181]=230;
	pcgGreekChars[182]=20;
	pcgGreekChars[184]=140;
	pcgGreekChars[185]=143;
	pcgGreekChars[188]=146;
	pcgGreekChars[189]=171;
	pcgGreekChars[190]=149;
	pcgGreekChars[192]=161;
	pcgGreekChars[193]=164;
	pcgGreekChars[194]=165;
	pcgGreekChars[195]=166;
	pcgGreekChars[196]=167;
	pcgGreekChars[197]=168;
	pcgGreekChars[198]=169;
	pcgGreekChars[199]=170;
	pcgGreekChars[200]=172;
	pcgGreekChars[201]=173;
	pcgGreekChars[202]=181;
	pcgGreekChars[203]=182;
	pcgGreekChars[204]=183;
	pcgGreekChars[205]=184;
	pcgGreekChars[206]=189;
	pcgGreekChars[207]=190;
	pcgGreekChars[208]=198;
	pcgGreekChars[209]=199;
	pcgGreekChars[211]=207;
	pcgGreekChars[212]=208;
	pcgGreekChars[213]=209;
	pcgGreekChars[214]=210;
	pcgGreekChars[215]=211;
	pcgGreekChars[216]=212;
	pcgGreekChars[217]=213;
	pcgGreekChars[218]=145;
	pcgGreekChars[219]=150;
	pcgGreekChars[222]=158;
	pcgGreekChars[223]=141;
	pcgGreekChars[223]=159;
	pcgGreekChars[224]=252;
	pcgGreekChars[225]=214;
	pcgGreekChars[226]=215;
	pcgGreekChars[228]=221;
	pcgGreekChars[229]=222;
	pcgGreekChars[230]=224;
	pcgGreekChars[231]=225;
	pcgGreekChars[234]=228;
	pcgGreekChars[235]=229;
	pcgGreekChars[236]=230;
	pcgGreekChars[237]=231;
	pcgGreekChars[238]=232;
	pcgGreekChars[239]=233;
	pcgGreekChars[240]=234;
	pcgGreekChars[241]=235;
	pcgGreekChars[242]=237;
	pcgGreekChars[243]=236;
	pcgGreekChars[244]=238;
	pcgGreekChars[245]=242;
	pcgGreekChars[246]=243;
	pcgGreekChars[247]=244;
	pcgGreekChars[248]=246;
	pcgGreekChars[249]=250;
	pcgGreekChars[250]=160;
	pcgGreekChars[251]=251;
	pcgGreekChars[252]=162;
	pcgGreekChars[253]=163;
	pcgGreekChars[254]=253;
	#endif
	dbg(DEBUG,"InitCharsets: -----------------------------------");
	snapit((char *)pcgGreekChars,257,outp);
	dbg(DEBUG,"InitCharsets: -----------------------------------");
	dbg(DEBUG,"InitCharsets: character conversion table [POLISH]!");

	#if 0
	memset(pcgPolishChars,0x00,S_BUFF);
	dbg(DEBUG,"InitCharsets: ------------------------------------");
	/* Left= Dec. Nr in Windows-codepage  ------ right: Dec. AEG-LCD-codepage */
	pcgPolishChars[]=;
	pcgPolishChars[]=;
	pcgPolishChars[]=;
	snapit((char *)pcgGreekChars,257,outp);
	dbg(DEBUG,"InitCharsets: ------------------------------------");
	#endif
}


static void Convert_LCD_Chars(unsigned char *pcpString, int ipLang)
{
	int ilCnt = 0;
	int ilLen = 0;
	/*dbg(DEBUG,"CLC: IN <%s>",pcpString);*/
	switch(ipLang)
	{
		case 0: /* No conversion */
			break;
		case 1: /* Greek LCD charset */
			ilLen = strlen((char*)pcpString);
			do
			{
				if (pcgGreekChars[(int)(pcpString[ilCnt])] != 0x00)
				{
					pcpString[ilCnt] = pcgGreekChars[(int)(pcpString[ilCnt])];
				}
				ilCnt++;
			}while(ilCnt<ilLen);
			break;
		case 2: /* polish LCD charset */
			ilLen = strlen((char*)pcpString);
			do
			{
				if (pcgPolishChars[(int)(pcpString[ilCnt])] != 0x00)
				{
					pcpString[ilCnt] = pcgPolishChars[(int)(pcpString[ilCnt])];
				}
				ilCnt++;
			}while(ilCnt<ilLen);
			break;
		default:
			dbg(TRACE,"CLC: invalid language for conversion <%d>!",igLang);
			break;
	}
	/*dbg(DEBUG,"CLC: OUT<%s>",pcpString);*/
}
/* ******************************************************************** */
/* The MakeExtFlno routine					*/
/* ******************************************************************** */
static int MakeExtFlno(char *pcpFlno)
{
  int ilCnt = 0;
  int ilAlcCnt = 0;
  int ilFnrCnt = 0;
  int ilBreak = FALSE;
  int ilSufCnt = 0;
  int ilRC = RC_FAIL;
  int ilLen = 0;
  int ilPos = 0;
  char pclTmpFlno[16];
  char pclAlc[10];
  char pclFnr[10];
  char pclTmpFnr[10];
  char pclTmpBuf[10];
  char pclSuffix[10];

  memset(pclAlc,0x00,10);
  memset(pclFnr,0x00,10);
  memset(pclTmpFnr,0x00,10);
  memset(pclTmpBuf,0x00,10);
  memset(pclSuffix,0x00,10);

	str_trm_all(pcpFlno," ",TRUE);
  ilLen = strlen(pcpFlno);
	if (ilLen > 3 && ilLen < 10)
	{
		do
		{
			/* getting the airline */
			if (ilCnt<3 && (isalpha(pcpFlno[ilCnt])!=0 || isdigit(pcpFlno[ilCnt])!=0))
			{
				pclAlc[ilAlcCnt] = pcpFlno[ilCnt];	
				ilAlcCnt++;
			}
			/* getting the flight-nr */
			if (ilCnt>1 && isdigit(pcpFlno[ilCnt])!=0)
			{
				pclFnr[ilFnrCnt] = pcpFlno[ilCnt];	
				ilFnrCnt++;
			}
			/* getting the suffix */
			if (ilCnt>3 && isdigit(pcpFlno[ilCnt])==0)
			{
				pclSuffix[ilSufCnt] = pcpFlno[ilCnt];	
				ilSufCnt++;
			}
			ilCnt++;
		}while(ilCnt < ilLen);
		/*dbg(DEBUG,"MakeExtFlno: AIRLINE   = <%s>",pclAlc);*/
		/* stripping leading zeros */
		ilCnt=0;
		do
		{
			if (pclFnr[ilCnt]!='0')
			{
				strcpy(pclTmpBuf,&pclFnr[ilCnt]);
				ilBreak = TRUE;	
			}
			ilCnt++;	
		}while(ilBreak==FALSE&&ilCnt<strlen(pclFnr));
    strcpy(pclTmpFnr,"00000");
    ilPos = 3 - strlen(pclTmpBuf);
    if (ilPos < 0)
    {
      ilPos = 0;
    } /* end if */
    strcpy(&pclTmpFnr[ilPos],pclTmpBuf);
		/*dbg(DEBUG,"MakeExtFlno: FLIGHT-NR = <%s>",pclTmpFnr);*/
		str_trm_all(pclSuffix," ",TRUE);
		/*dbg(DEBUG,"MakeExtFlno: SUFFIX    = <%s>",pclSuffix);*/
		if (strlen(pclAlc) > 3)
		{
			dbg(TRACE,"MakeExtFlno: Flight-Airline-code to big!");
		}
		else
		{
			while (strlen(pclAlc) <= 2)
			{
				strcat(pclAlc," ");
			}
			memset(pclTmpFlno,0x00,16);
			strncpy(pclTmpFlno,pclAlc,igAlc);
			strncat(pclTmpFlno,pclTmpFnr,igFlnr);
			ilRC = RC_SUCCESS;
			if (igShowSuffix==1)
			{
				strncat(pclTmpFlno,pclSuffix,1);
			}
			str_trm_all(pclTmpFlno," ",TRUE);
			/*dbg(DEBUG,"MakeExtFlno: Reformatted FLNO = <%s>",pclTmpFlno);*/
			strcpy(pcpFlno,pclTmpFlno);
			ilRC = RC_SUCCESS;
		}
	}
	else
	{
		dbg(TRACE,"MakeExtFlno: FLIGHT-NR to short(<3)/big(>9)! Ignoring!");
	}
  return ilRC;
} /* end MakeExtFlno */
/* ******************************************************************** */
/* The Get_CCA_Counter routine					*/
/* ******************************************************************** */
static int Get_CCA_Counter(char *pcpFlightUrno,char *pcpMinMax,char *pcpData)
{
	int ilRc = RC_SUCCESS;
	short slFkt = 0;
	short slCursor = 0;
	char pclSqlBuf[L_BUFF];
	char pclTmpSqlAnswer[XL_BUFF];
	char pclSqlBuf2[L_BUFF];
	char pclTmpSqlAnswer2[XL_BUFF];
	char pclNow[XXS_BUFF];

	/* means we have common checkin */
	if (strlen(pcpData) <= 0)
	{
		*pcpData = 0x00;
		memset(pclSqlBuf,0x00,L_BUFF);
		sprintf(pclSqlBuf,"SELECT ALC2 FROM AFT%s WHERE URNO =%s"
			,pcgTabEnd,pcpFlightUrno);
		slFkt = START|REL_CURSOR; 
		slCursor = 0;
		memset(pclTmpSqlAnswer,0x00,XL_BUFF);
		/*dbg(DEBUG,"GCC:SQL <%s>",pclSqlBuf);*/
		if ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))==RC_SUCCESS)
		{
			TrimAll(pclTmpSqlAnswer);
			if (strlen(pclTmpSqlAnswer) > 0)
			{
				dbg(DEBUG,"GCC: Airline=<%s>.",pclTmpSqlAnswer); 
				TimeToStr(pclNow,0,0,1);
				memset(pclSqlBuf2,0x00,L_BUFF);
				sprintf(pclSqlBuf2,"SELECT %s(CCA%s.CKIC) FROM CCA%s,ALT%s WHERE ALT%s.URNO=CCA%s.FLNU"
													 " AND ALT%s.ALC2='%s' AND CCA%s.HOPO='%s' AND CCA%s.CKBA < '%s' AND"
													 " (CCA%s.CKEA=' ' OR CCA%s.CKEA > '%s') AND CCA%s.CTYP='C' AND CCA%s.CKIC<>' '"
													,pcpMinMax,pcgTabEnd,pcgTabEnd,pcgTabEnd,pcgTabEnd,pcgTabEnd,pcgTabEnd,pclTmpSqlAnswer
													,pcgTabEnd,pcgHomeAp,pcgTabEnd,pclNow,pcgTabEnd,pcgTabEnd,pclNow,pcgTabEnd,pcgTabEnd);
				slFkt = START|REL_CURSOR; 
				slCursor = 0;
				memset(pclTmpSqlAnswer2,0x00,XL_BUFF);
				/*dbg(DEBUG,"GCC: SQL <%s>",pclSqlBuf2);*/
				if ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf2,pclTmpSqlAnswer2))==RC_SUCCESS)
				{
					TrimAll(pclTmpSqlAnswer2);
					dbg(DEBUG,"GCC: COMMON <%s>-Counter=<%s>.",pcpMinMax,pclTmpSqlAnswer2); 
					strcpy(pcpData,pclTmpSqlAnswer2);
				}
			}
		}
	}
	else /* we have a dedicated counter */
	{
		*pcpData = 0x00;
		TimeToStr(pclNow,0,0,1);
		memset(pclSqlBuf,0x00,L_BUFF);
		sprintf(pclSqlBuf,"SELECT %s(CCA%s.CKIC) FROM CCA%s WHERE CCA%s.FLNU=%s"
											 " AND CCA%s.HOPO='%s' AND CCA%s.CKBA < '%s' AND"
											 " (CCA%s.CKEA=' ' OR CCA%s.CKEA > '%s') AND CCA%s.CTYP=' ' AND CCA%s.CKIC<>' '"
											,pcpMinMax,pcgTabEnd,pcgTabEnd,pcgTabEnd,pcpFlightUrno,pcgTabEnd,pcgHomeAp,pcgTabEnd
											,pclNow,pcgTabEnd,pcgTabEnd,pclNow,pcgTabEnd,pcgTabEnd);
		slFkt = START|REL_CURSOR; 
		slCursor = 0;
		memset(pclTmpSqlAnswer,0x00,XL_BUFF);
		/*dbg(DEBUG,"GCC: SQL <%s>",pclSqlBuf);*/
		if ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))==RC_SUCCESS)
		{
			TrimAll(pclTmpSqlAnswer);
			dbg(DEBUG,"GCC: DEDICATED <%s>-Counter=<%s>.",pcpMinMax,pclTmpSqlAnswer); 
			strcpy(pcpData,pclTmpSqlAnswer);
		}
	}
	return ilRc;
}
/* ******************************************************************** */
/* The ReplaceCfgFormat routine					*/
/* ******************************************************************** */
static void ReplaceCfgFormat(char *pcpBuffer,int ipFrom, int ipTo, int ipT1, int ipT2)
{
	int ilRc = RC_SUCCESS;
	char pclNow[XXS_BUFF];
	time_t tlNow = time(0);

	tlNow = time(0);
	tlNow = tlNow + (ipFrom * SECONDS_PER_MINUTE);
	memset(pclNow,0x00,XXS_BUFF);
	TimeToStr(pclNow,tlNow,1,1);
	dbg(DEBUG,"ReplaceCfgFormat: START_MIN pclNow=<%s>",pclNow);
	while((ilRc = SearchStringAndReplace(pcpBuffer,"START_MIN",pclNow)) == RC_SUCCESS)
	{
		/*dbg(DEBUG,"RCF: New buffer=<%s>",pcpBuffer);*/
	}
	tlNow = time(0);
	tlNow = tlNow + (ipTo * SECONDS_PER_MINUTE);
	memset(pclNow,0x00,XXS_BUFF);
	TimeToStr(pclNow,tlNow,1,1);
	dbg(DEBUG,"ReplaceCfgFormat: END_MIN pclNow=<%s>",pclNow);
	while((ilRc = SearchStringAndReplace(pcpBuffer,"END_MIN",pclNow)) == RC_SUCCESS)
	{
		/*dbg(DEBUG,"RCF: New buffer=<%s>",pcpBuffer);*/
	}
	tlNow = time(0);
	tlNow = tlNow + (ipT1 * SECONDS_PER_MINUTE);
	memset(pclNow,0x00,XXS_BUFF);
	TimeToStr(pclNow,tlNow,1,1);
	dbg(DEBUG,"ReplaceCfgFormat: T_PARA1 pclNow=<%s>",pclNow);
	while((ilRc = SearchStringAndReplace(pcpBuffer,"T_PARA1",pclNow)) == RC_SUCCESS)
	{
		/*dbg(DEBUG,"RCF: New buffer=<%s>",pcpBuffer);*/
	}
	tlNow = time(0);
	tlNow = tlNow + (ipT2 * SECONDS_PER_MINUTE);
	memset(pclNow,0x00,XXS_BUFF);
	TimeToStr(pclNow,tlNow,1,1);
	dbg(DEBUG,"ReplaceCfgFormat: T_PARA2 pclNow=<%s>",pclNow);
	while((ilRc = SearchStringAndReplace(pcpBuffer,"T_PARA2",pclNow)) == RC_SUCCESS)
	{
		/*dbg(DEBUG,"RCF: New buffer=<%s>",pcpBuffer);*/
	}
}
