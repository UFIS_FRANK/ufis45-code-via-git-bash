#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h"  /* sets UFIS_VERSION, must be done defore mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Aia/AIA_Server/Base/Server/Fids/dsphdl.c 1.3 2005/04/01 20:39:57SGT akl Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB AAT/I DSPHDL.C                                                         */
/*                                                                            */
/* Author         : J�rn Weerts & Jochen Hiller                            */
/* Date           : 09. February 1999                                         */
/* Description    : display handler for controlling display device information*/
/*                                                                            */
/* Update history :                                                           */
/*            mos  30 nov 1999                                                */
/*            replaced 'SUCCESS' with 'RC_SUCCESS' in main (2x)               */
/*            and HandleQueues (2x)                                           */
/*                                                                            */
/*                                                                            */
/* Thanks to Rainer Westermann for consulting and catering                    */
/******************************************************************************/
/*                                                                            */
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/********************************************************
**********************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <arpa/inet.h>
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <dirent.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <time.h>
#include <locale.h>
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h" 
#include "AATArray.h"
#include "gt2hdl.h"

#include "dsphdl.h" 
/*  #include "dsplib.h" */
#include "lexdsp.h"
#include "tools.h"
#include "action.h"
#include "helpful.h"
#include "timdef.h"
#include "cedatime.h"

#define STR0(x)    x?x:"(NULL)"
#define TF(x)      x?"TRUE":"FALSE"
#define CS(x)      x?"YES":"NO"
 
/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
FILE *outp       = NULL;
int  debug_level = DEBUG;
int  igdebug_switch = FALSE;
int  igdebug_dzcommand = FALSE;
int  igdebug_selection = FALSE;
int  igshow_config = TRUE;
int  igshow_event = FALSE;
int  igslow_connection = FALSE;
int  igdebug_mode = 0;
int  igrefresh=TRUE;
int  igsend_small_logo=FALSE;
int  igPacketMax=5;
/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/
extern int dsplib_socket(int*k,int,char*,int,int);
extern void dsplib_shutdown(int,int);
extern void dsplib_close(int);
extern int SetSignals(void (*HandleSignal)(int));
extern int DebugPrintItem(int,ITEM *);
extern int DebugPrintEvent(int,EVENT *);
extern int init_db(void);
extern int  ResetDBCounter(void);
extern void HandleRemoteDB(EVENT*);
extern int  sql_if(short ,short* ,char* ,char* ); 
extern int close_my_cursor(short *cursor); 
extern snap(char*,int,FILE*);
extern int GetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel,char *pcpDef,char *pcpTrim);
extern gtBuildCommand(char*,char*,char*,int,char*,char*,int,char*);
extern int BuildItemBuffer( char *pcpData, char *pcpFieldList, int ipNoOfFields, char *pcpSepChr);
/* LEX prototypes */
extern int get_item_no(char *s, char *f, short elem_len);
extern long  nap(long);
extern void  GetServerTimeStamp(char*,int,long,char*);
extern UDBXPage *UDBXPage_CreateFromFile(char *pcFileName,PEDTable *);
extern void  DynStr_AppendStr(DynStr *pThis, char *s);
extern int GetQuotasForWhereClause(char* ,char* ,char* ,int ,char* );
/*replace a delimiter like 0x7c(|) with "{ITEM}"*/
extern void SetPipeToItem(char*,int);
extern  int tcp_send_datagram(int,char*,char*,char*, char*,int);
/*from dsplib*/
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;        /* The queue item pointer  */
static EVENT *prgEvent     = NULL;        /* The event pointer       */
static int   igItemLen     = 0;            /* length of incoming item */
static int   igInitOK      = FALSE;      /* Flag for init */
static char  pcgHomeAp[XS_BUFF];      /* buffer for home airport */
static char  pcgTabEnd[XS_BUFF];      /* buffer for TABEND */
static char  pcgTwEnd[XS_BUFF];          /* buffer for TABEND */
static char  pcgConfFile[S_BUFF];      /* buffer for config-file name */
static int   igUseHopo = FALSE;          /* flag for use of HOPO-field */
static int   igModID_Router  = 0;      /* MOD-ID of Router  */
static int   igStreamSock  = 0;          /* socket for tcp-SOCK_STREAM connect */

/***********GT2RESULTBUFFER***********/
static char pcgResBuf[RES_BUF_SIZE];
/***********GT2RESULTBUFFER***********/

static char      pcgHostName[XS_BUFF];
static char      pcgHostIp[XS_BUFF];
static char      pcgBcAddr[XS_BUFF];        /* for main broadcast address (char)*/
static UINT     uigBcAddr = 0;                /* for main broadcast address (long)*/
static char      *pcgUdpBCService = "UFIS_FIDS_SEND_UDP";    
static int       igDgramSock = 0;
static int       igRETRY_IDS_MESSAGE = 1;
static int       igAcceptRetry=1; 
            /* Socket ID for UDP */
static int       igTcpTimeOutAlarmFlag = FALSE;
static int       igConnectDeviceFlag  = FALSE;
static int       igAlarmCount = 0;
static SRV_DEV_INFO    *prgDevInfo = NULL;
 
/*ENTRY'S FROM CONFIGFILE*/
/*global config for Connect Devices*/
static int      igLISTENQ = 5;
static int      igBroadcastIntervall = 100; /*provids active connection*/
static int      igQueTimeOut = 0; /*enables a definded wait in que*/ 
static int      igTimeSync = 0;
static int      igRefreshCyclus = 0;
/*schedule some action*/
static int      igCheckDeviceState = 0;
static int      igKeepalive = 0;
static int      igTcpAcceptTimeOut = 0;
static int      igRefreshBelt = 0;
static int      igRefreshCounter = 0;
static int      igRefreshDeparture = 0;
static int      igRefreshArrival = 0;
static int      igRefreshBagsum = 0;
static int      igRefreshChute = 0;
static int      igDegaussIntervall =0;
/*global config for Fields*/
char     pcgBAGSUMFIELDS[L_BUFF]=BAGSUMFIELDS;
char     pcgCHUTEFIELDS[L_BUFF]=CHUTEFIELDS;
char     pcgFLDFIELDS[L_BUFF]=FLDFIELDS;
char     pcgFLVFIELDS[L_BUFF]=FLVFIELDS;
/*char     pcgFLV1FIELDS[L_BUFF]=FLV1FIELDS;*/
/*global config for Whereclause*/
char     pcgBELT[XS_BUFF]="BELT";
char     pcgCHECKIN[XS_BUFF]="CHECK";
char     pcgGATE[L_BUFF]="GATE";

char     *pcgOrderScheduled = "STOF";
char     *pcgOrderEstimated = "TIFF";
/*global config for Network*/
static char     pcgBCADDR[L_BUFF];
static char     pcgNETMASK[L_BUFF];
/*global config for Path to*/
static char     pcgPathToGif[L_BUFF];
static char     pcgPathToNewGif[L_BUFF];
static char     pcgPathToMpeg[L_BUFF];
static char     pcgPathToPages[L_BUFF];
static char     pcgPathToFonts[L_BUFF];


/*TIME PRARMETER BUFFER FROM CFGFILE*/
static time_t igBroadcastTimeBuff = 0;
static time_t igRefreshCyclusTimeBuff = 0;
static time_t igTimeSyncTimeBuff = 0;
static time_t igCheckDeviceStateTimeBuff = 0;
static time_t igKeepaliveTimeBuff = 0;
static time_t igRefreshBeltTimeBuff = 0;
static time_t igRefreshBagsumTimeBuff = 0;
static time_t igRefreshChuteTimeBuff = 0;
static time_t igRefreshCounterTimeBuff = 0;
static time_t igRefreshDepartureTimeBuff = 0;
static time_t igRefreshArrivalTimeBuff = 0;
static time_t igDegaussTimeBuff = 0;
/*MAX NUMBER OF CLUSTERMEMBERS*/
static int    igMaxClusterNo = 1;
static int    igUTCDIFF=0;
static int    igListenIntervall = 0;
static int    igQueCounter=0;
/******************************************************************************/
/* my global structures                                                       */
/******************************************************************************/
static PMStruct rgPG;
static DeviceStruct rgDV;
/********************************/
/* CEDAArray functions / values */
/********************************/
/* DEVICE-Table Array (DEV)*/
HANDLE    sgDevInfo = 0;
long         lgDevFldLen[DEV_MAXFIELDS];
char        *pcgDevArrayName = NULL; 
char        *pcgDevFields = NULL;
char        pcgDevTab[10];
char        pcgDevSel[XS_BUFF];

/* DISPLAY-Table Array (DSP) */
HANDLE      sgDspInfo = 0;
long        lgDspFldLen[DSP_MAXFIELDS];
char        *pcgDspArrayName = NULL;
char        *pcgDspFields = NULL;
char        pcgDspTab[10];
char        pcgDspSel[XS_BUFF];
  
/* PageCfgTable Array (PAGCFG) */
HANDLE      sgPagCfgInfo = 0;
long        lgPagCfgFldLen[DEV_MAXFIELDS];
char        *pcgPagCfgArrayName = NULL;
char        *pcgPagCfgFields = NULL;
char        pcgPagCfgTab[10];
char        pcgPagCfgSel[XS_BUFF];
 
/*GetPageInfoTable Array (PAGINFO) */
HANDLE      sgGetPagInfo = 0;
long        lgGetPagInfoFldLen[PAG_MAXFIELDS+1];
char        *pcgGetPagInfoArrayName = NULL;
char        pcgGetPagInfoFields[PAG_MAXFIELDS*5+100];
char        pcgGetPagInfoTab[10];
char        pcgGetPagInfoSel[L_BUFF]; 

static char pcgFileText[20*1024];

/******************************************************************************/
/* Function prototypes                                                          */
/******************************************************************************/
static int      Init_dsphdl();
static int    Reset(void);                        /* Reset program          */
static void    Terminate(int);                     /* Terminate program      */
static void    HandleSignal(int);                  /* Handles signals        */
static void    HandleErr(int);                     /* Handles general errors */
static void    HandleQueErr(int);                  /* Handles queuing errors */
static int    HandleInternalData(void);           /* Handles event data     */
static void     HandleQueues(void);                 /* Waiting for Sts.-switch*/
static void    DoNothing(void);
/******************************************************************************/
/* Function prototypes by JWE&JHI                                                    */
/******************************************************************************/
/* Init-functions  */
static int ReadCfg(void);
static int GetQueues();
static void PrvConn();
static int SetDevState(char *prpDeviceAdr,int ipState,char* pcpUrno);
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
                     char *pcpTwStart,char* pcpTwEnd,
		     char *pcpSelection,char *pcpFields,char *pcpData,
                     char *pcpAddStruct,int ipAddstructSize); 
static void DisconnectDevices();
static int GetPagInfos();
static int ScanFile(void);
static void TrimRight(char *pcpBuffer);
/* FIDAS 010610 jhe -- add table parm */
static int ReadPageCfg(char *pcpFile,char *pcpPagId, int ipCurPag, PEDTable *);
/* FIDAS 010610 jhe -- add decl of fct to read table */
PEDTable *ReadPEDTable(void);
static int SendLayoutToDZ(char* pcpField, int ipCurPag );

static int SendIniPage(int ipCurDev);
static int SendGraphicsDir(void);
static int SendGraphics(void);
static int SetInverseColor(void);
static int SetReverseColor(void);
/* tools */
static void snapit(void *pcpBuffer,long lpDataLen,FILE *pcpDbgFile,char *pcpWhat);
/* functions for CCSArray operations */
/****************************/
/*Funktion Prototypes by JHI*/
/****************************/
static int ConnectDevice();
static int SendKeepAlive(void);
static int UtcToLocal(char* pcpTime,char* pcpSeparator,char* pcpFormat);
static void DumpStructInfos();
static int SetDevLastDegaussTime(void); 
static int GetDataForPages(int ipCurPag);
static int InitDatabaseFieldInfos(PObjStruct*);
static int UpdateTable(int ipCurDev,int ipCurDevPag,int ipCurPag,int ipSocket);
static int UpdateLocation(int ipCurDev,int ipCurDevPag,int ipCurPag,int ipSocket);
/**/
static int SetPageData(int ipCurPag,int ipCurObj,FieldList *prRecords,char* pcpCommand, char* pcpResult,char* pcpFileBuf,int ipItem);
static int GetHostInfos();
static int CheckDeviceState();
static int RefreshDevice(int ipEvent,char* pcpTarget,char* pcpFields,char*pcpData,int ipCurDev);
static int RefreshCounter(int ipEvent,char* pcpTarget,char* pcpFields,char*pcpData,int ipCurDev,int piDseq);
static int SendTimeSync();
static int SetMonitorBrightness(void);
static int PlayMpeg(char* pcpFields,char* pcpData);
static int UpdateCounter(int ipCurDev,int ipCurDevPag,int ipCurPag,int ipSocket,char* pcpFields,char* pcpData,int piDseq );
static int UpdateGate(int ipCurDev,int ipCurDevPag,int ipCurPag,int ipSocket,char* pcpFields,char* pcpData,int piDseq );
static int SelectData(char* pcpFields,char* pcpData,int ipCurPag,int piDisplayFlag);
static int SelectLayout(int ipCurDev,int ipCurPag,int* pipNoOfFlights);
static int TimeFrame(char *pcpTimeFrame,int ipOffset,int ipOption);
extern void SetPipeToComma(char *pcpData,int ipHexChar);
static int SetAlarmPage(int ipOption);
static int SetDefaultPage(int ipOption,int ipCurDev,int ipCarousel);
static int StartCounter(int ipCurDev);
extern int MapToArabic(char* ,int);
static int GetItem(char* pcpTarget,char* pcpData,char* pcpFieldlist,char* pcpField,int piDeleteBlanks);
static void GetConfig(char* pcpFile,char* pcpSection,char* pcpTag,char* pcpTarget,char* pcpDefault);
static void GetConfigSwitch(char* pcpFile,char* pcpSection,char* pcpTag,int* piTarget,int piDefault);
static void GetConfigValue(char* pcpFile,char* pcpSection,char* pcpTag,int* piTarget,int piDefault);
static void DeleteLeadingZero(char *pcpTarget);
static void CloseDevice(int ipCurDev);
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN  
{
  int    ilRc = RC_SUCCESS;            /* Return code            */
  int    ilCnt = 0;
  int   ilItemFlag=TRUE; 
  int   debug_level1;
  time_t now = 0;
  INITIALIZE;            /* General initialization    */


  /* signal handling of SIGPIPE,SIGCHLD,SIGALRM,SIGTERM */
  SetSignals(HandleSignal);

  dbg(TRACE,"------------------------------------------");
  dbg(TRACE,"MAIN: version <%s>",mks_version);

  /* Attach to the MIKE queues */
  do{
    ilRc = init_que();
    if(ilRc != RC_SUCCESS)
      {
	dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
	sleep(6);
	ilCnt++;
      }/* end of if */
  }while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
      sleep(60);
      exit(1);
    }else{
      dbg(TRACE,"MAIN: init_que() OK!");
      dbg(TRACE,"MAIN: mod_id   <%d>",mod_id);
      dbg(TRACE,"MAIN: mod_name <%s>",mod_name);
    }/* end of if */
  do
    {
      ilRc = init_db();
      if (ilRc != RC_SUCCESS)
	{
	  dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
	  sleep(6);
	  ilCnt++;
	} /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
      sleep(60);
      exit(2);
    }else{
      dbg(TRACE,"MAIN: init_db()  OK!");
    } /* end of if */

  /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
  *pcgConfFile = 0x00;
  sprintf(pcgConfFile,"%s/%s",getenv("BIN_PATH"),mod_name);
  ilRc = TransferFile(pcgConfFile);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
    } /* end of if */
  dbg(TRACE,"MAIN: Binary-file = <%s>",pcgConfFile);

  sprintf(pcgConfFile,"%s/fids/%s.cfg",getenv("CFG_PATH"),mod_name);
  
  ilRc = TransferFile(pcgConfFile); 
  if(ilRc != RC_SUCCESS) 
    {
      dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile); 
    }
  dbg(TRACE,"MAIN: Config-file = <%s>",pcgConfFile);     

  ilRc = SendRemoteShutdown(mod_id);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    }

  if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: waiting for status switch ...");
      HandleQueues();
      dbg(TRACE,"MAIN: now running ...");
    }/* end of if */

  if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: initializing ...");
      dbg(TRACE,"------------------------------------------");
      if(igInitOK == FALSE)
	{
	  ilRc = Init_dsphdl();
	  if(ilRc == RC_SUCCESS)
	    {
	      dbg(TRACE,"");
	      dbg(TRACE,"------------------------------------------");
	      dbg(TRACE,"MAIN: initializing OK");
	      igInitOK = TRUE;
	    } 
	}
    }else{
      Terminate(1);
    }
  dbg(TRACE,"------------------------------------------");
    
  if (igInitOK == TRUE)
    {
      now = time(NULL);
      igBroadcastTimeBuff = now;
      if((ilRc = ConnectDevice())!=RC_SUCCESS)
	{
	  dbg(TRACE,"MAIN: ConnectDevice failed");
	}
      while(TRUE)
	{

	  ilItemFlag=FALSE;
	  debug_level1 = debug_level;
	  debug_level = 0;
	  memset(prgItem,0x00,igItemLen);
	  alarm(igQueTimeOut);/*wait only a few  seconds*/
	  ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
	  alarm(0);
	  debug_level= debug_level1;
	  /* dbg(DEBUG,"QUE Counter %d",++igQueCounter); */
	  /* depending on the size of the received item  */
	  /* a realloc could be made by the que function */
	  /* so do never forget to set event pointer !!! */
	  prgEvent = (EVENT *) prgItem->text;
                    
	  if( ilRc == RC_SUCCESS )
	    {
	      /* Acknowledge the item */
	      ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
	      if( ilRc != RC_SUCCESS ) 
		{
		  /* handle que_ack error */
		  HandleQueErr(ilRc);
		} /* fi */
	      switch( prgEvent->command )
		{
		case    HSB_STANDBY    :
		  ctrl_sta = prgEvent->command;
		  HandleQueues();
		  break;    
		case    HSB_COMING_UP    :
		  ctrl_sta = prgEvent->command;
		  HandleQueues();
		  break;    
		case    HSB_ACTIVE    :
		  ctrl_sta = prgEvent->command;
		  break;    
		case    HSB_ACT_TO_SBY    :
		  ctrl_sta = prgEvent->command;
		  /* CloseConnection(); */
		  HandleQueues();
		  break;    
		case    HSB_DOWN    :
		  /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
		  ctrl_sta = prgEvent->command;
		  Terminate(1);
		  break;    
		case    HSB_STANDALONE    :
		  ctrl_sta = prgEvent->command;
		  ResetDBCounter();
		  break;    
		case    REMOTE_DB :
		  /* ctrl_sta is checked inside */
		  HandleRemoteDB(prgEvent);
		  break;
		case    SHUTDOWN    :
		  /* process shutdown - maybe from uutil */
		  Terminate(0);
		  break;
		case    RESET        :
		  ilRc = Reset();
		  break;
		case    EVENT_DATA    :
		  if((ctrl_sta == HSB_STANDALONE) ||
		     (ctrl_sta == HSB_ACTIVE) ||
		     (ctrl_sta == HSB_ACT_TO_SBY))
		    {
		      ilItemFlag=TRUE;
		      ilRc = HandleInternalData();
		      if(igshow_event==TRUE)
			dbg(TRACE,"--- HandleInternalData done ---");
		      if(ilRc != RC_SUCCESS)
			{
			  HandleErr(ilRc);
			}/* end of if */
		    }else{
		      dbg(TRACE,"MAIN: wrong HSB-status <%d>",ctrl_sta);
		      DebugPrintItem(TRACE,prgItem);
		      DebugPrintEvent(TRACE,prgEvent);
		    }/* end of if */
		  break; 
		case    TRACE_ON :
		  dbg_handle_debug(prgEvent->command);
		  break;
		case    TRACE_OFF :
		  dbg_handle_debug(prgEvent->command);
		  break;
          
		  /*-----------------------testroutine by jhi ----------------------*/
		case 900:
		  CheckDeviceState();
		  ConnectDevice();
		  if(ilRc != RC_SUCCESS)
		    {
		      HandleErr(ilRc);
		    }/* end of if */           
		  break;
		case 901:
		  ConnectDevice();
		  if(ilRc != RC_SUCCESS)
		    {
		      HandleErr(ilRc);
		    }/* end of if */           
		  break;
		case 999:
		  SendTimeSync();           
		  dbg(TRACE,"altzone = <%d>",altzone);
		  dbg(TRACE,"timezone = <%d>",timezone);
		  dbg(TRACE,"daylight = <%d>",daylight);
		  dbg(TRACE,"igUTCDIFF = <%d>",igUTCDIFF);
		  break;
		case 1000:
		  {
		    SetMonitorBrightness();
		  }
		  break;
		case 1001:
           
		  SetAlarmPage(TRUE);
		  if(ilRc != RC_SUCCESS)
		    {
		      HandleErr(ilRc);
		    }/* end of if */           
		  break;
		  /*-----------------------testroutine by jhi------------------------*/
		case 1010:
		
		  break;
        
		case 1011:
          
		  SetInverseColor();
		  break;
		case 1012:
		  SetReverseColor();
		  break;
		case 1002:

		  SetAlarmPage(FALSE);
		  break;
		  /*general reading configfile*/
		case 1111: 
		  ReadCfg();
		  break;
		default            :
		  dbg(TRACE,"MAIN: unknown event");
		  DebugPrintItem(TRACE,prgItem);
		  DebugPrintEvent(TRACE,prgEvent);
		  break;
		} /* end switch */
	    }else{
	      /* Handle queuing errors */
	      HandleQueErr(ilRc);
	    } /* end else */
	  /**************************************************************/
	  /* time parameter for scheduled actions                       */
	  /**************************************************************/
	  now = time(NULL);
	  if(ilItemFlag == FALSE)
	    {
	      /*BROADCAST INTERVALL*/
	      if((igBroadcastTimeBuff+igBroadcastIntervall) <= now&&igBroadcastIntervall>0)
		{
		  if((ilRc = ConnectDevice())!=RC_SUCCESS)
		    {
		      dbg(TRACE,"MAIN: ConnectDevice failed");
		    }
		  if(igshow_event)
		    dbg(DEBUG,"MAIN: ConnectDevice after %d sec done ",now-igBroadcastTimeBuff);
		  igBroadcastTimeBuff = now;
		}
	      /*DATABASE REFRESH INTERVALL*/
	      if((igRefreshCyclusTimeBuff+igRefreshCyclus) <= now&&igRefreshCyclus>0)
		{
		  if((ilRc = RefreshDevice(FALSE,"","","",0))!=RC_SUCCESS)
		    {
		      dbg(DEBUG,"MAIN: RefreshDevice failed with <%d>",ilRc);
		    }
		  if(igshow_event)
		    dbg(DEBUG,"MAIN: Refresh after %d sec done   ",now-igRefreshCyclusTimeBuff);
		  igRefreshCyclusTimeBuff = now;
		}

	      if(igCheckDeviceState > 600||igCheckDeviceState==-2)
		{
		  if((igKeepaliveTimeBuff+igKeepalive) <= now)
		    {
		      if((ilRc = SendKeepAlive())!=RC_SUCCESS)
			{
			  dbg(TRACE,"MAIN: Keepalive  failed");
			}
		      if(igshow_event)
			dbg(DEBUG,"MAIN: CheckDeviceState after %d sec done ",now - igKeepaliveTimeBuff);
		      igKeepaliveTimeBuff = now;
		    }
		}


	      /*CHECK DEVICEMODE INTERVALL*/
	      if((igCheckDeviceStateTimeBuff+igCheckDeviceState) <= now&&igCheckDeviceState>0)
		{
		  if((ilRc = CheckDeviceState())!=RC_SUCCESS)
		    {
		      dbg(TRACE,"MAIN:  CheckDeviceState failed");
		    }
		  if(igshow_event)
		    dbg(DEBUG,"MAIN: CheckDeviceState after %d sec done ",now - igCheckDeviceStateTimeBuff);
		  igCheckDeviceStateTimeBuff = now;
		}
	      /*MONITOR TIME SYNCHRONISATION*/
	      if((igTimeSyncTimeBuff+igTimeSync) <= now&&igTimeSync>0)
		{
		  SendTimeSync();
		  if(igshow_event)
		    {
		      if((now - igTimeSyncTimeBuff)/60>60000)
			dbg(TRACE,"MAIN: Send initial Time Sync");
		      else
			dbg(TRACE,"MAIN: Send Time Sync. after %d min done",(now - igTimeSyncTimeBuff)/60);
		    }
		  igTimeSyncTimeBuff= now;
		}
	      /* time parameter for cyclic actions end                      */
	      /**************************************************************/
	    }
	} /* end while */
    }else{
      dbg(TRACE,"MAIN: Init_dsphdl() failed with <%d> Sleeping 30 sec.! Then terminating ...",ilRc);
      sleep(30);
    }
  exit(0);
  return 0;
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_dsphdl()
{
  int    ilRc = RC_SUCCESS;            /* Return code */
  char pclDspNames[8196];
  int  ilCurrentDevice = 0;
  time_t now = 0;
  char *pclListenQ;
  char *pcllocale=NULL;

  /*settung all timebuffers to now*/
  tzset();
  now = time(NULL);
  UtcToLocal(NULL,NULL,NULL);
  igBroadcastTimeBuff = now-igBroadcastIntervall;
  igRefreshCyclusTimeBuff = now;
  igCheckDeviceStateTimeBuff = now;
  igRefreshBeltTimeBuff = now;
  igRefreshCounterTimeBuff = now;
  igRefreshDepartureTimeBuff = now;
  igRefreshArrivalTimeBuff = now;
  /* reading queue-ID's from CEDA */
 

  pcllocale=setlocale(LC_TIME,NULL);
  if(pcllocale != NULL)
    {
      dbg(TRACE,"LOCALE <%s>",pcllocale);
    }

  if((ilRc = GetHostInfos())!= RC_SUCCESS)
    {
      dbg(TRACE,"Init_dsphdl: GetHostInfos() returns <%d>!",ilRc); 
      return RC_FAIL; 
    } 
  if ((ilRc = GetQueues()) != RC_SUCCESS) 
    { 
      dbg(TRACE,"Init_dsphdl: GetQueues() returns <%d>!",ilRc); 
      return RC_FAIL; 
    } 
  /* reading default home-airport from sgs.tab */
  memset(pcgHomeAp,0x00,sizeof(pcgHomeAp));
  ilRc = tool_search_exco_data("SYS","HOMEAP",pcgHomeAp);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_dsphdl : No HOMEAP entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_dsphdl : HOMEAP = <%s>",pcgHomeAp);
    }
  /* reading default table-extension from sgs.tab */
  memset(pcgTabEnd,0x00,sizeof(pcgTabEnd));
  ilRc = tool_search_exco_data("ALL","TABEND",pcgTabEnd);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_dsphdl : No TABEND entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_dsphdl : TABEND = <%s>",pcgTabEnd);
      memset(pcgTwEnd,0x00,XS_BUFF);
      sprintf(pcgTwEnd,"%s,%s,%s",pcgHomeAp,pcgTabEnd,mod_name);
      dbg(TRACE,"Init_dsphdl : TW_END = <%s>",pcgTwEnd);

      if (strcmp(pcgTabEnd,"TAB") == 0)
	{
	  igUseHopo = TRUE;
	  dbg(TRACE,"Init_dsphdl : use HOPO-field!");
	}
    }
  /*Get limits for file handles*/
  {
    int rc;
    struct rlimit rlim;
    rc = getrlimit(RLIMIT_NOFILE, &rlim);
    if (rc)
      perror("getrlimit");
    else
      dbg(TRACE,"rlim_cur=%ld\n",(long) rlim.rlim_cur);
  }
  pclListenQ=getenv("LISTENQ");
  if(pclListenQ != NULL)
    {
      igLISTENQ=atoi(pclListenQ);
    }
  dbg(TRACE,"Init_dsphdl: Listen backlog = %d", igLISTENQ);


  /* now reading from configfile */
  if ((ilRc = ReadCfg()) != RC_SUCCESS)
    {
      /*  dbg(TRACE,"Init_dsphdl: ReadCfg() failed! Can't continue!"); */
      dbg(TRACE,"Init_dsphdl: Default settings in use !!");
      ilRc = RC_SUCCESS;
    }else{
      dbg(TRACE,"Init_dsphdl : init CEDA-Array ....");
      ilRc = CEDAArrayInitialize (4,1);
      if (ilRc != RC_SUCCESS)
	{
	  dbg(TRACE, "Init_dsphdl: CEDAArrayInitialize failed with <%d>!",ilRc);
	}else{
	  /**********/
	  /* DEVTAB */
	  /**********/
	  pcgDevArrayName = "device";
	  /****************1****2****3****4****5****6****7****8****9****10***11***12***13***14***15***16*/
	  pcgDevFields = "dadr,stat,dpid,dbrf,dcof,dffd,dfco,drnp,drna,dter,ddgi,dldt,dprt,algc,dare,urno";
	  lgDevFldLen[0] = 15;
	  lgDevFldLen[1] = 1;
	  lgDevFldLen[2] = 12;   
	  lgDevFldLen[3] = 3; 
	  lgDevFldLen[4] = 3; 
	  lgDevFldLen[5] = 8;
	  lgDevFldLen[6] = 128; 
	  lgDevFldLen[7] = 10; 
	  lgDevFldLen[8] = 10; 
	  lgDevFldLen[9] = 5;
	  lgDevFldLen[10] = 4; 
	  lgDevFldLen[11] = 14;
	  lgDevFldLen[12] = 4;
	  lgDevFldLen[13] = 4;
	  lgDevFldLen[14] = 16;
	  lgDevFldLen[15] = 10;
	  memset(pcgDevTab,0x00,10);
	  /*   ilRc = AATArrayCreate(&sgDevInfo,pcgDevArrayName,TRUE,5,5,lgDevFldLen,pcgDevFields,NULL); */
	  /*        if (ilRc != RC_SUCCESS)  */
	  /*          { */
	  /*            dbg(TRACE,"Init_dsphd : AATArrayCreate for <%s> failed  with <%d>!",pcgDevArrayName, ilRc); */
	  /*          }        */
	  sprintf(pcgDevTab,"DEV%s",pcgTabEnd);
	  memset(pcgDevSel,0x00,XS_BUFF);
      
	  if (igUseHopo == TRUE)
	    sprintf(pcgDevSel,"WHERE GRPN = '%s' AND HOPO = '%s' ORDER BY DPID",mod_name,pcgHomeAp);
	  else
	    sprintf(pcgDevSel,"WHERE GRPN = '%s' ORDER BY DPID",mod_name);
    
       
	  ilRc = CEDAArrayCreateInitCount (&sgDevInfo,pcgDevArrayName,pcgDevTab,pcgDevSel,
					   NULL,NULL,pcgDevFields,lgDevFldLen,0,3); 
 
	  if (ilRc != RC_SUCCESS)
	    {
	      dbg(TRACE,"Init_dsphdl: CEDAArrayCreateInitCount for <%s> failed  with <%d>!",
		  pcgDevArrayName, ilRc);
	    }else{
	      if ((ilRc = CEDAArrayFill(&sgDevInfo,pcgDevArrayName,NULL)) != RC_SUCCESS)
		{
		  dbg(TRACE,"Init_dsphdl: CEDAArrayFill for <%s> failed  with <%d>!", pcgDevArrayName, ilRc);
		}                
      
	    }
	  /********************************************/
	  /*NEWBLOCK, here we get general device infos*/
	  /********************************************/
	  {
	    /*   int ilRc; */
	    int ilCnt = 1;
	    long llRow = ARR_FIRST;
	    char pclData[S_BUFF];
	    int ilCurDev = 0;
	    char pclTmp[S_BUFF];
	    int ilCurRec=0;
    
	    memset(pclData,0x00,S_BUFF);
	    rgDV.prDevPag = (DevPag*)malloc(sizeof(DevPag));
	    memset(&rgDV.prDevPag[0],0x00,sizeof(DevPag));
	    rgDV.iNoOfDevices = ilCurDev+1;
	    while ((ilRc = CEDAArrayGetRow(&sgDevInfo,pcgDevArrayName,llRow,',',S_BUFF,pclData))
		   == RC_SUCCESS)
	      { 
		if(llRow == ARR_NEXT)
		  {
		    ilCurDev++;
		    rgDV.iNoOfDevices = ilCurDev+1;
                
		    rgDV.prDevPag = (DevPag*)realloc(rgDV.prDevPag,rgDV.iNoOfDevices*sizeof(DevPag));
		  }
		memset(pclTmp,0x00,S_BUFF);
		dbg(DEBUG,"DevInfos: current number is <%d> ",ilCurDev);
		dbg(DEBUG,"DevInfos: current number of devices is <%d> ",rgDV.iNoOfDevices);
		dbg(DEBUG,"DevInfos: <%d> <%s>",ilCnt,pclData);
		/*init struct*/ 
		rgDV.prDevPag[ilCurDev].prRecord=(FieldList*) malloc(10*sizeof(FieldList));
		rgDV.prDevPag[ilCurDev].iNoOfRec =10;
		for(ilCurRec=0;ilCurRec<rgDV.prDevPag[ilCurDev].iNoOfRec;ilCurRec++)
		  {
     
		    rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].iDisplayFlag=FALSE;
		  }
		rgDV.prDevPag[ilCurDev].iLogo=0;
		rgDV.prDevPag[ilCurDev].pcLocationLogo[0]='\0';
		GetDataItem(rgDV.prDevPag[ilCurDev].pcIP,pclData,1,',',"","");
		DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcIP,cBLANK);
		GetDataItem(rgDV.prDevPag[ilCurDev].pcDeviceState,pclData,2,',',"","");
		StringUPR((UCHAR*)rgDV.prDevPag[ilCurDev].pcDeviceState);
		DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcDeviceState,cBLANK);
		GetDataItem(rgDV.prDevPag[ilCurDev].pcDisplayId,pclData,3,',',"","");
		DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcDisplayId,cBLANK);
		GetDataItem(rgDV.prDevPag[ilCurDev].pcBrightness,pclData,4,',',"","");
		DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcBrightness,cBLANK);
		GetDataItem(rgDV.prDevPag[ilCurDev].pcFilterField,pclData,6,',',"","");
		DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcFilterField,cBLANK);        
		GetDataItem(rgDV.prDevPag[ilCurDev].pcFilterContents,pclData,7,',',"","");
		DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcFilterContents,cBLANK);
		GetDataItem(rgDV.prDevPag[ilCurDev].pcRowInCluster,pclData,8,',',"","");
		DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcRowInCluster,cBLANK);
		GetDataItem(rgDV.prDevPag[ilCurDev].pcTerminal,pclData,10,',',"","");
		DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcTerminal,cBLANK);
		GetDataItem(rgDV.prDevPag[ilCurDev].pcDevUrno,pclData,16,',',"","");
		/*DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcDevUrno,cBLANK); */
		rgDV.prDevPag[ilCurDev].iLostConnections=0;
		/*prevent mult 0*/ 
		if(rgDV.prDevPag[ilCurDev].pcRowInCluster[0]=='\0')
		  {
		    strcpy(rgDV.prDevPag[ilCurDev].pcRowInCluster, "1");
		  }
		/*how many rows must we select?*/
		if(atoi(rgDV.prDevPag[ilCurDev].pcRowInCluster)> igMaxClusterNo)
		  {
		    igMaxClusterNo= atoi(rgDV.prDevPag[ilCurDev].pcRowInCluster);
		  }
		GetDataItem(rgDV.prDevPag[ilCurDev].pcDegaussIntervall,pclData,11,',',"","");
		DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcDegaussIntervall,cBLANK);
		GetDataItem(rgDV.prDevPag[ilCurDev].pcLastDegaussTime,pclData,12,',',"","");
		DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcLastDegaussTime,cBLANK);
		rgDV.prDevPag[ilCurDev].iInvertAirline = FALSE;
		rgDV.prDevPag[ilCurDev].iCodeShares = FALSE;
		GetDataItem(rgDV.prDevPag[ilCurDev].pcAirline,pclData,14,',',"","");
		DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcAirline,cBLANK);
		if(rgDV.prDevPag[ilCurDev].pcAirline[0]=='-')
		  {
		    rgDV.prDevPag[ilCurDev].iInvertAirline = TRUE;
		    DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcAirline,0x2d);
		  }
		if(rgDV.prDevPag[ilCurDev].pcAirline[0]=='+')
		  {
		    rgDV.prDevPag[ilCurDev].iCodeShares =TRUE;
		    DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcAirline,0x2b);
		  }
		GetDataItem(rgDV.prDevPag[ilCurDev].pcDeviceArea,pclData,15,',',"","");
		DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcDeviceArea,cBLANK);


		GetDataItem(pclTmp,pclData,13,',',"","");
		DeleteCharacterInString(pclTmp,cBLANK);
		rgDV.prDevPag[ilCurDev].sPort = (USHORT)atoi(pclTmp);
		/*set socket number to Null*/
		rgDV.prDevPag[ilCurDev].iSocket = 0;
		/*set INIT LAYOUT TO FALSE*/
		rgDV.prDevPag[ilCurDev].iInitialize = FALSE;
		/*set alarmflag to false*/
		rgDV.prDevPag[ilCurDev].iAlarmFlag=FALSE;
		/*set defaultflag to false*/
		rgDV.prDevPag[ilCurDev].iDefaultFlag=FALSE;
		memset(pclData,0x00,XS_BUFF);
		ilCnt++;
		llRow = ARR_NEXT;
	      }
	  }/*end of block*/
	  /**********/
	  /* DSPTAB */ 
	  /**********/
	  pcgDspArrayName = "display";
	  pcgDspFields = "dpid,dicf,dnop,dfpn,demt,dapn";
	  lgDspFldLen[0] = 12;
	  lgDspFldLen[1] = 12;
	  lgDspFldLen[2] = 3;
	  lgDspFldLen[3] = 3;
	  lgDspFldLen[4] = 3;
	  lgDspFldLen[5] = 3;
	  memset(pcgDspTab,0x00,10);
	  sprintf(pcgDspTab,"DSP%s",pcgTabEnd);
	  memset(pcgDspSel,0x00,XS_BUFF);
	  memset(pclDspNames,0x00,L_BUFF);
	  strcpy(pclDspNames," (");
	  for( ilCurrentDevice = 0; ilCurrentDevice < rgDV.iNoOfDevices; ilCurrentDevice++)
	    {
	      if(strstr(pclDspNames,rgDV.prDevPag[ilCurrentDevice].pcDisplayId)==NULL)
		{
		  strcat(pclDspNames,"DPID ='");
		  strcat(pclDspNames,rgDV.prDevPag[ilCurrentDevice].pcDisplayId);
		  strcat(pclDspNames,"'");
		  strcat(pclDspNames," or ");
        
		}
	    }
	  /*cut "  or "*/
	  pclDspNames[strlen(pclDspNames)-4]='\0';
	  strcat(pclDspNames,")");
	  if (igUseHopo == TRUE)
	    {
	      sprintf(pcgDevSel,"WHERE HOPO = '%s' AND%s",pcgHomeAp,pclDspNames);
	    }else{
	      sprintf(pcgDevSel,"WHERE %s ",pclDspNames);
	    }
	  dbg(DEBUG,"Init_dsphdl %05d: DSPSELECT : \n<%s>",__LINE__, pcgDevSel);
	  ilRc = CEDAArrayCreateInitCount(&sgDspInfo,pcgDspArrayName,pcgDspTab,pcgDspSel,NULL,NULL,pcgDspFields,lgDspFldLen,0,3);
	  if (ilRc != RC_SUCCESS)
	    {
	      dbg(TRACE,"Init_dsphdl: CEDAArrayCreateInitCount for <%s> failed  with <%d>!",
		  pcgDspArrayName, ilRc);
	    }else{
	      if ((ilRc = CEDAArrayFill(&sgDspInfo,pcgDspArrayName,NULL)) != RC_SUCCESS)
		{
		  dbg(TRACE,"Init_dsphdl: CEDAArrayFill for <%s> failed  with <%d>!",
		      pcgDspArrayName, ilRc);
		}
	    }
	  /******************************************/
	  /*now we add dsp infos to device structure*/
	  /*another block                           */
	  /******************************************/
	  {
	    /*   int ilRc; */
	    int ilCnt = 1;
	    long llRow = ARR_FIRST;
	    char pclData[S_BUFF];
	    int ilCurDev = 0;
	    char pclTmpData[XS_BUFF];
	    memset(pclData,0x00,S_BUFF); 
        
	    for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
	      {
                ilCnt = 1;
                llRow = ARR_FIRST;
		memset(pclData,0x00,S_BUFF);
		while ((ilRc = CEDAArrayGetRow(&sgDspInfo,pcgDspArrayName,llRow,',',S_BUFF,pclData))
		       == RC_SUCCESS)
		  {
		    memset(pclTmpData,0x00,XS_BUFF);
		    GetDataItem(pclTmpData,pclData,1,',',"","\0 ");
		    if(!strcmp(pclTmpData,rgDV.prDevPag[ilCurDev].pcDisplayId))
		      {
			/*target reached, now get infos*/
			GetDataItem(rgDV.prDevPag[ilCurDev].pcInitCfgFile,pclData,2,',',"","");
			DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcInitCfgFile,cBLANK);
			GetDataItem(rgDV.prDevPag[ilCurDev].pcNumberOfPages,pclData,3,',',"","");
                        DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcNumberOfPages,cBLANK);
			GetDataItem(rgDV.prDevPag[ilCurDev].pcFirstPageNumber,pclData,4,',',"","");
			DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcFirstPageNumber,cBLANK);
			/*default page if no flight*/
			GetDataItem(rgDV.prDevPag[ilCurDev].pcDefaultPage,pclData,5,',',"","");
			DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcDefaultPage,cBLANK);
			GetDataItem(rgDV.prDevPag[ilCurDev].pcAlarmPageNo,pclData,6,',',"","");
			/*to go directly to the alarmpage*/
			DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcAlarmPageNo,cBLANK);
			rgDV.prDevPag[ilCurDev].iNoOfPageInCarousel = atoi(rgDV.prDevPag[ilCurDev].pcNumberOfPages);
                        dbg(DEBUG,"Device %d has %d pages ",ilCurDev,rgDV.prDevPag[ilCurDev].iNoOfPageInCarousel);
		      }
		    memset(pclData,0x00,S_BUFF);
		    ilCnt++;
		    llRow = ARR_NEXT;
		  }
	      }/*end for*/
	  }/*end of this block*/

	}/*end else before DEVTAB*/

      /*******************************************/
      /*next step is analize the pageconfig files*/
      /*******************************************/
      /*parse and store infos from page config file*/
      if((ilRc = GetPagInfos()) != RC_SUCCESS)
	{
	  dbg(TRACE,"Init_dsphdl: GetPagInfos() failed with <%d>!",ilRc);
	}
      /*******************************************/
      /*now we add page infos to device structure*/
      /*third  block                             */
      /*******************************************/
      {
	int ilRc = RC_SUCCESS;
	int ilCnt = 1;
	long llRow = ARR_FIRST;
	char pclData[L_BUFF];
	char pclTmpData[XS_BUFF];
	int ilCurDev = 0;
	int ilCurDevPag = 0;
	int ilCurPage = 0;
	char pclTmpDefault[XS_BUFF];
	char pclTmpAlarm[XS_BUFF];
	char pclTmpDefaultNo[XS_BUFF];
	char pclTmpAlarmNo[XS_BUFF];
	int ilAlarmPage = -1;
	int ilDefaultPage = -1;
	memset(pclData,0x00,L_BUFF);
	memset(pclTmpDefault,0x00,XS_BUFF);
	memset(pclTmpAlarm,0x00,XS_BUFF);
	memset(pclTmpDefaultNo,0x00,XS_BUFF);
	memset(pclTmpAlarmNo,0x00,XS_BUFF);
	if(rgDV.iNoOfDevices==1&&rgDV.prDevPag[0].pcIP[0]=='\0')
	  {
	    dbg(TRACE,"NO DEVICE FOUND, DO NOTHING");
	    DoNothing();
	  }else{
	    dbg(TRACE,"=====================<ALL PAGES IN PAGESTRUCT>==========================");
	    for(ilCurPage=0;ilCurPage < rgPG.iNoOfPages;ilCurPage++) 
	      {
		dbg(TRACE,"<INIT_DSPHDL> Page Id         <%s> No in Pagestruct %d",rgPG.prPages[ilCurPage].pcPageId,ilCurPage);
	      }
	    dbg(TRACE,"=====================<ALL PAGES IN PAGESTRUCT END>==========================");
	    for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
	      {
		ilCnt = 1;
		llRow = ARR_FIRST;
		dbg(TRACE,"=====================<INIT_DSPHDL>==========================");
		dbg(TRACE,"<INIT_DSPHDL> Device Address         <%s> Device No %d",rgDV.prDevPag[ilCurDev].pcIP,ilCurDev);
		dbg(TRACE,"<INIT_DSPHDL> Device Filter Field    <%s>",rgDV.prDevPag[ilCurDev].pcFilterField );
		dbg(TRACE,"<INIT_DSPHDL> Device Filter Contents <%s>",rgDV.prDevPag[ilCurDev].pcFilterContents );
		/*initialize maximum counters*/
		rgDV.prDevPag[ilCurDev].iMaxNumberOfFlightsBelt=0;
		rgDV.prDevPag[ilCurDev].iPagiMaxNumberOfFlightsBelt=0;
		rgDV.prDevPag[ilCurDev].iCounterState=CLOSED;
		rgDV.prDevPag[ilCurDev].iGateState=CLOSED;

		if(atoi(rgDV.prDevPag[ilCurDev].pcDefaultPage)<= 0)
		  {
		    rgDV.prDevPag[ilCurDev].iDefaultPage=-1;
		    ilDefaultPage = -1;
		  }else{
		    ilDefaultPage=(atoi(rgDV.prDevPag[ilCurDev].pcDefaultPage)*2)-1;
		    dbg(DEBUG,"Defaultpage <%s> %d",rgDV.prDevPag[ilCurDev].pcDefaultPage,atoi(rgDV.prDevPag[ilCurDev].pcDefaultPage));
		  }
		if(atoi(rgDV.prDevPag[ilCurDev].pcAlarmPageNo) <= 0)
		  {
		    rgDV.prDevPag[ilCurDev].iAlarmPage=-1;
		    ilAlarmPage = -1;
		  }else{
		    ilAlarmPage=(atoi(rgDV.prDevPag[ilCurDev].pcAlarmPageNo)*2)-1;
		  }
		if(rgDV.prDevPag[ilCurDev].pcInitCfgFile[0]=='\0')
		  {
		    rgDV.prDevPag[ilCurDev].iConfigPage=-1;
		  }
		while((ilRc = CEDAArrayGetRow(&sgGetPagInfo,pcgGetPagInfoArrayName,llRow,',',L_BUFF,pclData))==RC_SUCCESS)
		  {
		    memset(pclTmpData,0x00,XS_BUFF);
		    GetDataItem(pclTmpData,pclData,PAG_MAXFIELDS+1,',',"","\0 ");
		    if(!strcmp(pclTmpData,rgDV.prDevPag[ilCurDev].pcDisplayId))
		      {
			dbg(DEBUG,"pclData <%s> -> <%s>",pclTmpData,rgDV.prDevPag[ilCurDev].pcDisplayId);
			/*store number of alarm and defaultpage from pagestructure to device structure*/
			if(ilDefaultPage > 0)
			  {
			    memset(pclTmpDefault,0x00,XS_BUFF);
			    GetDataItem(pclTmpDefault,pclData,ilDefaultPage,',',"","\0 ");
			    GetDataItem(pclTmpDefaultNo,pclData,ilDefaultPage+1,',',"","\0 ");
			  }
			if(ilAlarmPage > 0)
			  {
			    memset(pclTmpAlarm,0x00,XS_BUFF);
			    GetDataItem(pclTmpAlarm,pclData,ilAlarmPage,',',"","\0 ");
			    GetDataItem(pclTmpAlarmNo,pclData,ilAlarmPage+1,',',"","\0 ");
			  }
			for(ilCurPage=0;ilCurPage < rgPG.iNoOfPages;ilCurPage++) 
			  {
			    if(0 == strcmp(rgPG.prPages[ilCurPage].pcPageId,pclTmpDefault)&&ilDefaultPage!=-1) 
			      {
				rgDV.prDevPag[ilCurDev].iDefaultPage=ilCurPage;
				strcpy(rgDV.prDevPag[ilCurDev].pcDefaultPage,pclTmpDefaultNo);
			      }
			    if(0 == strcmp(rgPG.prPages[ilCurPage].pcPageId,pclTmpAlarm)&&ilAlarmPage!=-1)
			      {
				rgDV.prDevPag[ilCurDev].iAlarmPage=ilCurPage;
				strcpy(rgDV.prDevPag[ilCurDev].pcAlarmPageNo,pclTmpAlarmNo);
			      }
			    /*16.01.2001*****************************************/
			    DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcInitCfgFile,cBLANK);
			    /*16.01.2001*****************************************/
			    if(0 == strcmp(rgPG.prPages[ilCurPage].pcPageId,rgDV.prDevPag[ilCurDev].pcInitCfgFile)) 
			      {
				dbg(TRACE,"<INIT_DSPHDL> Configfile <%s> ilCurPage <%d>",rgDV.prDevPag[ilCurDev].pcInitCfgFile,ilCurPage);
				rgDV.prDevPag[ilCurDev].iConfigPage = ilCurPage;
				dbg(TRACE,"<INIT_DSPHDL> Page reference number <%d> in ilCurDev is <%d>\n ",rgDV.prDevPag[ilCurDev].iConfigPage,ilCurDev);
			      }
			  }
			if(rgDV.prDevPag[ilCurDev].iDefaultPage != -1)
			  {
			    dbg(TRACE,"<INIT_DSPHDL>LINE <%d> Defaultpage ID <%s> is  No <%d> in DSPTAB and No <%d> in Pagestruct", __LINE__ ,pclTmpDefaultNo,ilDefaultPage,rgDV.prDevPag[ilCurDev].iDefaultPage);
			  }
			if(rgDV.prDevPag[ilCurDev].iAlarmPage != -1)
			  {
			    dbg(TRACE,"<INIT_DSPHDL>LINE <%d> Alarmpage ID   <%s> is  No <%d> in DSPTAB and No <%d> in Pagestruct",__LINE__, pclTmpAlarmNo,ilAlarmPage,rgDV.prDevPag[ilCurDev].iAlarmPage);
			  }
			/*just to prevent a termination of dsphdl by wrong configuration*/
			if (rgDV.prDevPag[ilCurDev].iNoOfPageInCarousel==0)
			  {
			    dbg(TRACE,"<INIT_DSPHDL>LINE <%d> configuration in DSPTAB is wrong ",__LINE__);
			    rgDV.prDevPag[ilCurDev].iNoOfPageInCarousel=1;
			    DoNothing();
			  }
			if((rgDV.prDevPag[ilCurDev].pcPageId = (char**)malloc(rgDV.prDevPag[ilCurDev].iNoOfPageInCarousel*sizeof(char*))) == NULL)
			  {
			    dbg(TRACE,"<INIT_DSPHDL>LINE <%d> not enough memory to run",__LINE__);
			    Terminate(30);
			  }
			if((rgDV.prDevPag[ilCurDev].pcDisplayType = (char**)malloc(rgDV.prDevPag[ilCurDev].iNoOfPageInCarousel*sizeof(char*))) == NULL)
			  {
			    dbg(TRACE,"<INIT_DSPHDL>LINE <%d> not enough memory to run",__LINE__);
			    Terminate(30);
			  }
			if((rgDV.prDevPag[ilCurDev].pcPageObjectId = (char**)malloc(rgDV.prDevPag[ilCurDev].iNoOfPageInCarousel*sizeof(char*))) == NULL)
			  {
			    dbg(TRACE,"<INIT_DSPHDL>LINE <%d> not enough memory to run",__LINE__);
			    Terminate(30);
			  }
			if((rgDV.prDevPag[ilCurDev].piPagNo = (int*)malloc(rgDV.prDevPag[ilCurDev].iNoOfPageInCarousel*sizeof(int))) == NULL) 
			  {
			    dbg(TRACE,"<INIT_DSPHDL>LINE <%d> not enough memory to run",__LINE__);
			    Terminate(30);
			  }
			/*first we need the page identifiers...*/
			for(ilCurDevPag = 0;ilCurDevPag < rgDV.prDevPag[ilCurDev].iNoOfPageInCarousel;ilCurDevPag++)
			  {
			    memset(pclTmpData,0x00,XS_BUFF);
			    /*target reached, now get infos*/
			    GetDataItem(pclTmpData,pclData,(ilCurDevPag+1)*2-1,',',"","\0 ");
			    dbg(TRACE,"<INIT_DSPHDL> Page id <%s>",pclTmpData);
			    if((rgDV.prDevPag[ilCurDev].pcPageId[ilCurDevPag] = strdup(pclTmpData)) == NULL)
			      {
				dbg(TRACE,"<INIT_DSPHDL> not enough memory to run line %d",__LINE__);
				Terminate(30);
			      }
			    memset(pclTmpData,0x00,XS_BUFF);
			    /*target reached, now get infos*/
			    GetDataItem(pclTmpData,pclData,(ilCurDevPag+1)*2,',',"","\0 ");
			    dbg(TRACE,"<INIT_DSPHDL> PageObjectId <%s>",pclTmpData);
			    if((rgDV.prDevPag[ilCurDev].pcPageObjectId[ilCurDevPag] = strdup(pclTmpData)) == NULL)
			      {
				dbg(TRACE,"<INIT_DSPHDL> not enough memory to run line %d",__LINE__);
				Terminate(30);
			      }
			  }
			/*... then for faster search the reference number in page struct...*/
			for(ilCurDevPag = 0;ilCurDevPag < rgDV.prDevPag[ilCurDev].iNoOfPageInCarousel;ilCurDevPag++) 
			  {
			    /*target reached, now get infos*/
			    rgDV.prDevPag[ilCurDev].piPagNo[ilCurDevPag] = 0;
			    for(ilCurPage=0;ilCurPage < rgPG.iNoOfPages;ilCurPage++) 
			      {
				if(strcmp(rgPG.prPages[ilCurPage].pcDisplayType,"B")==0)
				  {
				    if (atoi(rgPG.prPages[ilCurPage].pcNumberOfFlights) >rgDV.prDevPag[ilCurDev].iMaxNumberOfFlightsBelt )
				      {
					rgDV.prDevPag[ilCurDev].iMaxNumberOfFlightsBelt   = atoi(rgPG.prPages[ilCurPage].pcNumberOfFlights);
				      }
				  }
				DeleteCharacterInString(rgPG.prPages[ilCurPage].pcPageId,cBLANK);
				DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcPageId[ilCurDevPag],cBLANK);
				if(0 == strcmp(rgPG.prPages[ilCurPage].pcPageId,rgDV.prDevPag[ilCurDev].pcPageId[ilCurDevPag])) 
				  {
				    dbg(TRACE,"<INIT_DSPHDL> PageId <%s> ilCurPage <%d>",rgDV.prDevPag[ilCurDev].pcPageId[ilCurDevPag],ilCurPage);
				    rgDV.prDevPag[ilCurDev].piPagNo[ilCurDevPag] = ilCurPage;
				    dbg(TRACE,"<INIT_DSPHDL> Page reference number in Pagestruct <%d> in ilCurDevPag is <%d> PageObjId (DSPTAB) is <%s>\n ",rgDV.prDevPag[ilCurDev].piPagNo[ilCurDevPag],ilCurDevPag,rgDV.prDevPag[ilCurDev].pcPageObjectId[ilCurDevPag]);
				    if((rgDV.prDevPag[ilCurDev].pcDisplayType[ilCurDevPag]=strdup(rgPG.prPages[ilCurPage].pcDisplayType)) == NULL)
				      {
					dbg(TRACE,"<INIT_DSPHDL> not enough memory to run line %d",__LINE__);
					Terminate(30);
				      }
				  }
			      }
			  }
		      }
		    memset(pclData,0x00,L_BUFF);
		    ilCnt++;
		    llRow = ARR_NEXT;
		  }
	      }/*end for*/
	    if(debug_level==DEBUG)
	      {
		dbg(TRACE,"=====================<CONFIGURATION SUMMARY>==========================\n");
		for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
		  {
		    dbg(TRACE," ");
		    dbg(TRACE,"==================== IP %s -> Device %d===============",rgDV.prDevPag[ilCurDev].pcIP,ilCurDev);
		    dbg(TRACE,"<INIT_DSPHDL>Defaultpage ID is  No <%d> in Pagestruct and No <%s> on Controller",
			rgDV.prDevPag[ilCurDev].iDefaultPage,
			rgDV.prDevPag[ilCurDev].pcDefaultPage);
		    dbg(TRACE,"<INIT_DSPHDL>Alarmpage ID   is  No <%d> in Pagestruct and No <%s> on Controller\n",
			rgDV.prDevPag[ilCurDev].iAlarmPage,
			rgDV.prDevPag[ilCurDev].pcAlarmPageNo);
		    for(ilCurDevPag = 0;ilCurDevPag < rgDV.prDevPag[ilCurDev].iNoOfPageInCarousel;ilCurDevPag++)
		      {
			dbg(TRACE,"<INIT_DSPHDL>Page ID in Device Struct is \t<%s> ",rgDV.prDevPag[ilCurDev].pcPageId[ilCurDevPag]);
			/*target reached, now get infos*/
			dbg(TRACE,"<INIT_DSPHDL>Page Object ID in Dev Struct is <%s> ",rgDV.prDevPag[ilCurDev].pcPageObjectId[ilCurDevPag]);
			dbg(TRACE,"<INIT_DSPHDL>Page %d Type <%s> CurDevPag is %d ",rgDV.prDevPag[ilCurDev].piPagNo[ilCurDevPag],rgDV.prDevPag[ilCurDev].pcDisplayType[ilCurDevPag],ilCurDevPag);
		      }
		  }
		dbg(TRACE,"=====================<CONFIGURATION SUMMARY END>==========================");
	      }
	  }
      }/*end of this block*/
      dbg(TRACE,"Init_dsphdl : ... ready.");
    }
  AATArrayDestroy(&sgPagCfgInfo,pcgPagCfgArrayName);
  AATArrayDestroy(&sgDspInfo,pcgDspArrayName);
  AATArrayDestroy(&sgDevInfo,pcgDevArrayName);
  if (igStreamSock <= 0)
    {
      dbg(TRACE,"Init_dsphdl : Port %d ",rgDV.prDevPag[0].sPort);


      if ((ilRc = dsplib_socket(&igStreamSock,SOCK_STREAM,NULL,rgDV.prDevPag[0].sPort,TRUE)) == RC_FAIL)
	{
	  dbg(TRACE,"HID: dsplib_socket returns: <%d>",ilRc);
	}else{
	  dbg(DEBUG,"HID: created SOCK_STREAM socket <%d>.",igStreamSock);
	}
    }
  if(ilRc==RC_SUCCESS)
    {
      if(( ilRc = listen(igStreamSock,100)!=RC_SUCCESS))
	{
	  dbg(TRACE,"<INIT_DSPHDL>LINE <%d> listen failed with %d,",__LINE__,ilRc);
	}else{
	  dbg(TRACE,"<INIT_DSPHDL>%05d listen initialized",__LINE__); 
	}
    }else{
      dbg(TRACE,"<INIT_DSPHDL>%05d NO LISTENER CREATED",__LINE__);
      /*to prevent terminating*/
      ilRc=RC_SUCCESS;
    }
  if(igdebug_mode==0)
    {
      dbg(TRACE,"");
      dbg(TRACE,"------------------------------------------");
      dbg(TRACE,"MAIN: initializing OK");
      dbg(TRACE,"------------------------------------------");
    }
  debug_level=igdebug_mode;
  return(ilRc);
} /* end of initialize */
/*********************************************************************
Function : GetQueues()
Paramter :
Return Code: RC_SUCCESS,RC_FAIL
Result:
Description: Gets all necessary queue-ID's for CEDA-internal
             communication!
*********************************************************************/
static int GetQueues()
{
  int ilRc = RC_FAIL;

  /* get mod-id of router */
  if ((igModID_Router = tool_get_q_id("router")) == RC_NOT_FOUND ||
      igModID_Router == RC_FAIL || igModID_Router == 0)
    {
      dbg(TRACE,"GetQueues   : tool_get_q_id(router) returns: <%d>",igModID_Router);
      ilRc = RC_FAIL;
    }else{
      dbg(TRACE,"GetQueues   : <router> mod_id <%d>",igModID_Router);
      ilRc = RC_SUCCESS;
    }
  return ilRc;
}
/* ********************************************************************/
/* The ReadCfg() routine                                              */
/* ********************************************************************/
static int ReadCfg(void)
{
  int ilRc = RC_SUCCESS;
  char pclTmp[L_BUFF];
  memset(pclTmp,0x00,L_BUFF);
  /*   memset(&prgCfg,0x00,sizeof(CFG)); */
  dbg(TRACE,"--------------------------------");
  dbg(TRACE,"ReadCfg: file <%s>",pcgConfFile);
  dbg(TRACE,"--------------------------------");
  /*******************************************************************/
  /*SECTION MAIN                                                     */
  /*******************************************************************/ 
  dbg(TRACE,"============== MAIN ReadCfg START===============================================");
  GetConfigSwitch(pcgConfFile,"MAIN","INITIAL_DEBUG_MODE",&igdebug_mode,0);
  GetConfigSwitch(pcgConfFile,"MAIN","GT2_DEBUG_SWITCH",&igdebug_switch,TRUE);
  GetConfigSwitch(pcgConfFile,"MAIN","REFRESH",&igrefresh, TRUE);
  GetConfigSwitch(pcgConfFile,"MAIN","DEBUG_DZCOMMAND",&igdebug_dzcommand , FALSE);
  GetConfigSwitch(pcgConfFile,"MAIN","DEBUG_SELECTION",&igdebug_selection,FALSE);
  GetConfigSwitch(pcgConfFile,"MAIN","SHOW_EVENT",&igshow_event,FALSE);
  GetConfigSwitch(pcgConfFile,"MAIN","SEND_SMALL_LOGO",&igsend_small_logo,FALSE);
  GetConfigValue(pcgConfFile,"MAIN","PACKET_PER_TAB",&igPacketMax,-1);
  /*******************************************************************/
  /*END OF SECTION                                                   */
  /*******************************************************************/ 
  if(igshow_config==TRUE)
    dbg(TRACE,"============== SERVERPARAMETER ===============================================");
  /*******************************************************************/
  /*SECTION SERVERPARAMETER                                           */
  /*******************************************************************/
  GetConfig(pcgConfFile,"SERVERPARAMETER","BCADDR",pcgBCADDR,"FFFFFFFF");
  GetConfig(pcgConfFile,"SERVERPARAMETER","NETMASK",pcgNETMASK,"FFFFFF00");
  /*******************************************************************/
  /*END OF SECTION                                                   */
  /*******************************************************************/
  if(igshow_config==TRUE)
    dbg(TRACE,"============== TIMEPARAMETER ===============================================");
  /*******************************************************************/
  /*SECTION TIMEPARAMETER                                            */
  /*******************************************************************/
  GetConfigValue(pcgConfFile,"TIMEPARAMETER","QUE_TIME_OUT",&igQueTimeOut,1);
  GetConfigValue(pcgConfFile,"TIMEPARAMETER","REFRESHBELT",&igRefreshBelt,60);
  GetConfigValue(pcgConfFile,"TIMEPARAMETER","REFRESHBAGSUM",&igRefreshBagsum,30);    
  GetConfigValue(pcgConfFile,"TIMEPARAMETER","REFRESHCHUTE",&igRefreshChute,30);
  GetConfigValue(pcgConfFile,"TIMEPARAMETER","REFRESHCOUNTER",&igRefreshCounter,600);
  GetConfigValue(pcgConfFile,"TIMEPARAMETER","REFRESHDEPARTURE",&igRefreshDeparture,60);
  GetConfigValue(pcgConfFile,"TIMEPARAMETER","REFRESHARRIVAL",&igRefreshArrival,60);
  GetConfigValue(pcgConfFile,"TIMEPARAMETER","REFRESH_CYCLUS",&igRefreshCyclus,300);
  GetConfigValue(pcgConfFile,"TIMEPARAMETER","DEGAUSS_INTERVALL",&igDegaussIntervall,2800);
  igDegaussIntervall*=60;
  /*******************************************************************/
  /*END OF SECTION                                                   */
  /*******************************************************************/
  if(igshow_config==TRUE)
    dbg(TRACE,"============== COMMUNICATION ===============================================");
  /*******************************************************************/
  /*SECTION COMMUNICATION                                            */
  /*******************************************************************/ 
  memset(pclTmp,0x00,L_BUFF);
  GetConfigSwitch(pcgConfFile,"COMMUNICATION","SLOW_CONNECTION",&igslow_connection,FALSE);
  GetConfigValue(pcgConfFile,"COMMUNICATION","BROADCAST_INTERVALL",&igBroadcastIntervall,120);
  GetConfigValue(pcgConfFile,"COMMUNICATION","TCP_ACCEPT_TIMEOUT",&igTcpAcceptTimeOut,TCP_ACCEPT_TIMEOUT);
  GetConfigValue(pcgConfFile,"COMMUNICATION","ACCEPT_RETRY",&igAcceptRetry,1);
  GetConfigValue(pcgConfFile,"COMMUNICATION","TIME_SYNC",&igTimeSync,900);
  igTimeSync *= 60;
  GetConfigValue(pcgConfFile, "COMMUNICATION", "RETRY_IDS_MESSAGE",&igRETRY_IDS_MESSAGE,1);
  if ((ilRc = iGetConfigEntry(pcgConfFile, "COMMUNICATION", "CHECK_DEVICE_STATE",CFG_STRING,pclTmp)) == RC_SUCCESS)
    {
      igCheckDeviceState = (time_t)atoi(pclTmp);
      igKeepalive= 120;
      if(igshow_config==TRUE)
	{
	  dbg(TRACE,"GetCfgEntry CHECK_DEVICE_STATE \tis <%d>sec.",igCheckDeviceState);
	  if(igCheckDeviceState>600||igCheckDeviceState==-2)
	    dbg(TRACE,"GetCfgEntry send Keepalive     \tis <%d>sec.",igKeepalive);
	}
    }else{
      if(igshow_config==TRUE)
	dbg(DEBUG,"GetCfgEntry failed with <%d>",ilRc);
      igCheckDeviceState = 120;
      igKeepalive = 0;
      if(igshow_config==TRUE)
	dbg(TRACE,"GetCfgEntry set CHECK_DEVICE_STATE \tto default<%d>sec.",igCheckDeviceState);
    }
  memset(pclTmp,0x00,L_BUFF);
  if ((ilRc = iGetConfigEntry(pcgConfFile, "COMMUNICATION", "BROADCAST_SERVICE",CFG_STRING,pclTmp)) == RC_SUCCESS)
    {
      pcgUdpBCService = NULL;
      pcgUdpBCService = strdup(pclTmp);
      if(igshow_config==TRUE)
	dbg(TRACE,"GetCfgEntry BROADCAST_SERVICE \tis <%s>.",pcgUdpBCService);
    }else{
      if(igshow_config==TRUE)
	{
	  dbg(TRACE,"GetCfgEntry set BROADCAST_SERVICE \tto default <%s>.",pcgUdpBCService);
	}
    }
  /*******************************************************************/
  /*END OF SECTION                                                   */
  /*******************************************************************/
  if(igshow_config==TRUE)
    dbg(TRACE,"============== PATH ===============================================");
  /*******************************************************************/
  /*SECTION PATH                                                     */
  /*******************************************************************/  
  GetConfig(pcgConfFile,"PATH","PATH_TO_GIF",pcgPathToGif,"/ceda/conf/fids/graphics/");
  GetConfig(pcgConfFile,"PATH","PATH_TO_NEW_GIF",pcgPathToNewGif,"/ceda/conf/fids/graphics/newgifs/");
  GetConfig(pcgConfFile,"PATH","PATH_TO_MPEG",pcgPathToMpeg,"/ceda/conf/fids/mpeg/");
  GetConfig(pcgConfFile,"PATH","PATH_TO_FONTS",pcgPathToFonts,"/ceda/conf/fids/fonts/");
  GetConfig(pcgConfFile,"PATH","PATH_TO_PAGE_FILES",pcgPathToPages,"/ceda/conf/fids/pages/");
  /*******************************************************************/
  /*END OF SECTION                                                   */
  /*******************************************************************/
  if(igshow_config==TRUE)
    dbg(TRACE,"============== PATH ===============================================");
  /*******************************************************************/
  /*SECTION PATH                                                     */
  /*******************************************************************/  
  GetConfig(pcgConfFile,"FIELD_DEFINES","CHECKINCOUNTER",pcgCHECKIN,"CHECK");
  GetConfig(pcgConfFile,"FIELD_DEFINES","BELT",pcgBELT,"BELT");
  GetConfig(pcgConfFile,"FIELD_DEFINES","GATE",pcgGATE,"GATE");
  /*******************************************************************/
  /*END OF SECTION                                                   */
  /*******************************************************************/
   GetConfigSwitch(pcgConfFile,"MAIN","SHOW_CONFIG",&igshow_config,FALSE);
  dbg(TRACE,"============== END GETCONFIGENTRY ===============================================");
  /********************************************************************************/
  return RC_SUCCESS;
}
/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
  int    ilRc = RC_SUCCESS;    /* Return code */
  dbg(TRACE,"Reset: now reseting ...");
  /* Disconnecting all devices */
  DisconnectDevices();
  /* closing SOCK_STREAM-socket */
  dbg(TRACE,"Reset: closing SOCK_STREAM socket <%d>.",igStreamSock);
  /*dsplib_close(igStreamSock); */
  close(igStreamSock);
  igStreamSock = 0;
  exit(0);    
  return ilRc;
} /* end of Reset */
/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
  int ilCurPag = 0;
  int ilCurObj = 0;
  /*untrigger action*/
  /* Disconnecting all devices */
  DisconnectDevices();
  /*closing SOCK_STREAM-socket */
  dsplib_close(igStreamSock);
  igStreamSock = 0;
  /* unset SIGCHLD ! DB-Child will terminate ! */
  dbg(TRACE,"Terminate: now leaving ...");
  sleep(ipSleep);
  exit(0);
} /* end of Terminate */
/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
  switch(pipSig)
    {
    case SIGALRM:
      if(igConnectDeviceFlag==TRUE&&igAlarmCount >igAcceptRetry)
	{
	  dbg(DEBUG,"SIGALARM");
	  igTcpTimeOutAlarmFlag=TRUE;
	}
      break;
    case SIGPIPE:
      dbg(DEBUG,"Broken pipe");
      break;
    case SIGCHLD:
      break;
    case SIGTERM:
      dbg(TRACE,"Term signal receved");
      Terminate(0);
      break;
    default    :
      dbg(TRACE,"Unknown signal receved");
      Terminate(10);
      break;
    } /* end of switch */
} /* end of HandleSignal */
/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
  /*    int    ilRc = RC_SUCCESS; */
    return;
} /* end of HandleErr */
/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRc = RC_SUCCESS;
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
            dbg(TRACE,"<%d> : msgsnd failed",pipErr);
            break;
    case    QUE_E_GET    :    /* Error using msgrcv */
            if(pipErr != 4)        
             dbg(DEBUG,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    : 
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :   unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        /*dbg(TRACE,"<%d> : no messages on queue",pipErr);*/
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    case    QUE_E_PRIORITY    :
        dbg(TRACE,"<%d> : wrong priority was send ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
    return;
} /* end of HandleQueErr */
/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
  int    ilRc = RC_SUCCESS;            /* Return code */
  int    ilBreakOut = FALSE;
  do{
    memset(prgItem,0x00,igItemLen);
    ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
    /* depending on the size of the received item  */
    /* a realloc could be made by the que function */
    /* so do never forget to set event pointer !!! */
    prgEvent = (EVENT *) prgItem->text;    
    if( ilRc == RC_SUCCESS )
      {
	/* Acknowledge the item */
	ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
	if( ilRc != RC_SUCCESS ) 
	  {
	    /* handle que_ack error */
	    HandleQueErr(ilRc);
	  } /* fi */
	switch( prgEvent->command )
	  {
	  case    HSB_STANDBY    :
	    ctrl_sta = prgEvent->command;
	    break;    
	  case    HSB_COMING_UP    :
	    ctrl_sta = prgEvent->command;
	    break;    
	  case    HSB_ACTIVE    :
	    ctrl_sta = prgEvent->command;
	    ilBreakOut = TRUE; 
	    break;    
	  case    HSB_ACT_TO_SBY    :
	    ctrl_sta = prgEvent->command;
	    break;    
	  case    HSB_DOWN    :
	    /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
	    ctrl_sta = prgEvent->command;
	    Terminate(10);
	    break;    
	  case    HSB_STANDALONE    :
	    ctrl_sta = prgEvent->command;
	    ResetDBCounter();
	    ilBreakOut = TRUE;
	    break;    
	  case    REMOTE_DB :
	    /* ctrl_sta is checked inside */
	    HandleRemoteDB(prgEvent);
	    break;
	  case    SHUTDOWN    :
	    Terminate(1);
	    break;
	  case    RESET        :
	    ilRc = Reset();
	    break;
	  case    EVENT_DATA    :
	    dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
	    DebugPrintItem(TRACE,prgItem);
	    DebugPrintEvent(TRACE,prgEvent);
	    break;
	  case    TRACE_ON :
	    dbg_handle_debug(prgEvent->command);
	    break;
	  case    TRACE_OFF :
	    dbg_handle_debug(prgEvent->command);
	    break;
	  default            :
	    dbg(TRACE,"HandleQueues: unknown event");
	    DebugPrintItem(TRACE,prgItem);
	    DebugPrintEvent(TRACE,prgEvent);
	    break;
	  } /* end switch */
      }else{
	/* Handle queuing errors */
	HandleQueErr(ilRc);
      } /* end else */
  } while (ilBreakOut == FALSE);
  if(igInitOK == FALSE)
    {
      ilRc = Init_dsphdl();
      if(ilRc == RC_SUCCESS)
	{
	  dbg(TRACE,"HandleQueues: Init_dsphdl() OK!");
	  igInitOK = TRUE;
	}else{ /* end of if */
	  dbg(TRACE,"HandleQueues: Init_dsphdl() failed!");
	  igInitOK = FALSE;
	} /* end of if */
    }/* end of if */
  /* OpenConnection(); */
} /* end of HandleQueues */
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleInternalData()
{
  int    ilRc = RC_SUCCESS;      /* Return code */
  char *pclSelection = NULL;
  char *pclFields = NULL;
  char *pclData = NULL;
  BC_HEAD *bchd = NULL;          /* Broadcast header*/
  CMDBLK  *cmdblk = NULL;      /* Command Block*/
  SRV_DEV_INFO *prlDevice = NULL; /* device info from dspsrv */
  char pclCuteFields[L_BUFF];
  char pclCuteData[L_BUFF];
  char pclFilterField[XS_BUFF];/*IP dotted dec.*/
  char pclFilterContents[128+1];
  char pclAlarmState[3];/*mostly 0 or 1*/
  short slFkt=0;
  short slCursor = 0;
  int ilItemNo = 0;
  int ilCurRec=0;
  int ilCurDev=0;
  int ilHit=RC_FAIL;
  int ilHitUrno=RC_FAIL;
  FieldList lrTmpFields;
  char pclOldData[L_BUFF];
  char pclURNO[XS_BUFF];
  char pclSelectBuf[M_BUFF];
  char pclTmpSqlAnswer[L_BUFF];
  char pclTmpUrno[XS_BUFF];
  char *pclTmpPtr=NULL;
  char pclDSEQ[10];
  bchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  cmdblk= (CMDBLK  *) ((char *)bchd->data);
  if(igshow_event==TRUE)
    dbg(TRACE,"--- HandleInternalData start ---");
  /*    dbg(DEBUG,"HID: From<%d> Cmd<%s> Table<%s> rc<%d>  data <%s>",prgEvent->originator,cmdblk->command, */
  /*        cmdblk->obj_name,bchd->rc, prgEvent->command); */
  /***********************************/
  /*    DebugPrintItem(DEBUG,prgItem);   */
  /*    DebugPrintEvent(DEBUG,prgEvent); */
  /***********************************/
  pclSelection = cmdblk->data;
  pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
  pclData = (char*)pclFields + strlen(pclFields) + 1;


  /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  if (strcmp(cmdblk->command,"TEXT") == 0)
    { 
      ScanFile();
    }
  /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  if (strcmp(cmdblk->command,"UPDATE") == 0)
    { 
      GetDataForPages(-1);
      RefreshDevice(FALSE,"","","",0);
    }
  /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  if (strcmp(cmdblk->command,"UPGIF") == 0)
    { 
      SendGraphicsDir();
    }
  /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  if (strcmp(cmdblk->command,"NEWGIF") == 0)
    { 
      SendGraphics();
    }
  /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  if (strcmp(cmdblk->command,"MPG") == 0)
    { 
      PlayMpeg(pclFields,pclData);
    }
  /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  /*CHECK DEVICEMODE INTERVALL*/
  if(strcmp(cmdblk->command,"CHKDEV") == 0)
    {
      if((ilRc = CheckDeviceState())!=RC_SUCCESS)
        {
          dbg(TRACE,"MAIN:  CheckDeviceState failed");
        }
      dbg(DEBUG,"MAIN: CheckDeviceState done ");
    }
  /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  /**********************LOCATION RELATED DEVICES HANDLED FROM FLDTAB**************/
  if(!strcmp(cmdblk->command,"LOCA"))
    {
      memset(pclCuteFields,0x00,L_BUFF);
      memset(pclCuteData,0x00,L_BUFF);
      memset(pclOldData,0x00,L_BUFF);
      memset(pclFilterField,0x00,XS_BUFF);
      memset(pclFilterContents,0x00,128+1);
      pclTmpPtr=NULL;
      strcpy(pclCuteData,pclData);
      pclTmpPtr=strstr(pclCuteData,"\n");
      if(pclTmpPtr !=NULL)
	{
	  strcpy(pclOldData,pclTmpPtr);
	  *pclTmpPtr='\0';
	}
      strcpy(pclCuteFields,pclFields);
      if(strlen(pclCuteFields)<1)
        {
	  dbg(DEBUG,"<HandleInternalData>%05d SECTION CUTE NO FIELDS FOUND",__LINE__);
	  return RC_FAIL;
        }
      /*UPCHAR ONLY FIELDNAMES*/
      StringUPR((UCHAR*)pclCuteFields);
      /*CHECK VALID DATA*/
      if(igshow_event==TRUE&&debug_level==DEBUG)
	{
	  dbg(DEBUG,"=======================FLD SECTION==========================================================");
	  dbg(DEBUG,"<HandleInternalData>%05d pclCuteFields <%s> ",__LINE__,pclCuteFields);
	  dbg(DEBUG,"\nNewData <%s>\nOldData <%s>\nSelection <%s>\n",pclCuteData,pclOldData,pclSelection);
	}
      pclDSEQ[0]='\0';
      GetItem(pclDSEQ,pclCuteData,pclCuteFields,"DSEQ",TRUE);
      if(!strcmp(pclDSEQ,"0"))
	{
	  return 0;
	}
      /*prepare data...and find table in global struct*/     
      memset(pclURNO,0x00,XS_BUFF);
      memset(pclSelectBuf,0x00,M_BUFF);
      memset(pclTmpSqlAnswer,0x00,L_BUFF);
      nap(150);
      ilRc=GetItem(pclURNO,pclCuteData,pclCuteFields,"URNO",TRUE);
      if(ilRc==RC_FAIL||pclURNO[0]=='\0')
	{
	  ilRc=GetItem(pclURNO,pclOldData,pclCuteFields,"URNO",TRUE);
	  if(ilRc==RC_FAIL||pclURNO[0]=='\0')
	    {
	      strcpy(pclURNO,pclSelection);
	      DeleteCharacterInString(pclURNO,cBLANK);
	      if(atoi(pclURNO)==0)
		{
		  dbg(TRACE,"No Urno Found !");
		  return RC_NOTFOUND;
		}
	    }
	}
      DeleteCharacterInString(pclURNO,'\n');
      sprintf(pclSelectBuf,"select %s from flvtab where urno =%s",pcgFLVFIELDS,pclURNO);
      if(igshow_event==TRUE&&igdebug_selection==TRUE)
        dbg(DEBUG,"<HandleInternalData> CUTE: %05d Select<%s>",__LINE__,pclSelectBuf);
      slFkt = START; 
      slCursor = 0;
      ilRc = sql_if(slFkt,&slCursor,pclSelectBuf,pclTmpSqlAnswer);
      close_my_cursor(&slCursor);
      pclDSEQ[0]='\0';
      if(ilRc==RC_SUCCESS)
	{

	  BuildItemBuffer(pclTmpSqlAnswer,pcgFLVFIELDS,0,",");
	  GetItem(pclDSEQ,pclTmpSqlAnswer,pcgFLVFIELDS,"DSEQ",TRUE);
	}
      /*ON UPDATE OR DELETE*/
      if(!strcmp(pclDSEQ,"0"))
	{
	  return 0;
	}
      if(igshow_event==TRUE)
	dbg(DEBUG,"<HandleInternalData> CUTE: %05d ilRc<%d>",__LINE__,ilRc);
      if(ilRc==RC_NOTFOUND||!strcmp(pclDSEQ,"-1"))
	{
	  /*********************Record deleted find counter and close it*********************/
	  for(ilCurDev=0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
	    {
	      if(!strcmp(rgDV.prDevPag[ilCurDev].pcFilterField,pcgCHECKIN)||
		 !strcmp(rgDV.prDevPag[ilCurDev].pcFilterField,pcgBELT)||
		 !strcmp(rgDV.prDevPag[ilCurDev].pcFilterField,pcgGATE))
		{/*if"D"*/
		  ilHit=FALSE;
		  ilHitUrno=FALSE;
		  for(ilCurRec=0;ilCurRec<rgDV.prDevPag[ilCurDev].iNoOfRec;ilCurRec++)
		    {
		      /*find location data*/
		      if(!strcmp(rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].pcUrno,pclURNO))
			{ 
			  ilHitUrno=TRUE;
			  ilHit=TRUE;
			  /*Last urno for device -> close it*/
			  rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].pcFields[0]='\0';
			  rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].pcRecordset[0]='\0';
			  rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].iDisplayFlag=FALSE;
			  rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].pcUrno[0]='\0';
			  if(ilCurRec>0)
			    {
			      if(rgDV.prDevPag[ilCurDev].prRecord[ilCurRec-1].pcUrno[0]!='\0')
				{
				  if((ilRc=RefreshCounter(TRUE,NULL,pcgFLVFIELDS,NULL,ilCurDev,0))!=RC_SUCCESS)
				    {
				      dbg(DEBUG,"<HandleInternalData> CUTE: %05d RefreshCounter failed with <%d>",__LINE__,ilRc);
				      /*this is not a fatal error*/
				      ilRc=RC_SUCCESS;
				    }
				}
			    }else
                            {
                              if(rgDV.prDevPag[ilCurDev].prRecord[ilCurRec+1].pcUrno[0]=='\0')
			      {
				SetDefaultPage(TRUE,ilCurDev,FALSE);
			      }
			    }
			}
		    }
		}
	    }
	}else{
	  if(igshow_event==TRUE&&igdebug_selection==TRUE)
	    dbg(DEBUG,"<HandleInternalData>%05d Data <%s>",__LINE__,pclTmpSqlAnswer);
	  GetItem(pclFilterContents,pclTmpSqlAnswer,pcgFLVFIELDS,"RNAM",TRUE);
	  GetItem(pclFilterField,pclTmpSqlAnswer,pcgFLVFIELDS,"RTYP",TRUE);
	  if(!strcmp(pcgBELT,pclFilterField))
	    {
#ifdef ATH
	      memset(pclSelectBuf,0x00,M_BUFF);
	      memset(pclTmpSqlAnswer,0x00,L_BUFF);
	      sprintf(pclSelectBuf,"select %s,flv2.dseq XYZ1 from flvtab flv1,flvtab flv2  where flv1.URNO = %s and flv1.rnam=flv2.rnam and flv2.rtyp='BELT' and flv2.dseq=0",FLV1FIELDS,pclURNO);
	      slFkt = START; 
	      slCursor = 0;
	      ilRc = sql_if(slFkt,&slCursor,pclSelectBuf,pclTmpSqlAnswer);
	      close_my_cursor(&slCursor);
	      BuildItemBuffer(pclTmpSqlAnswer," ",field_count(pcgFLVFIELDS)+1,",");
	      if(igshow_event==TRUE&&igdebug_selection==TRUE)
		dbg(DEBUG,"<HandleInternalData> CUTE: %05d Data <%s>",__LINE__,pclTmpSqlAnswer);
#else
  		memset(pclSelectBuf,0x00,M_BUFF);
              memset(pclTmpSqlAnswer,0x00,L_BUFF);
              sprintf(pclSelectBuf,"select %s from flvtab where URNO = %s" ,FLVFIELDS,pclURNO);
              slFkt = START;
              slCursor = 0;
              ilRc = sql_if(slFkt,&slCursor,pclSelectBuf,pclTmpSqlAnswer);
              close_my_cursor(&slCursor);
              BuildItemBuffer(pclTmpSqlAnswer," ",field_count(FLVFIELDS),",");
              if(igshow_event==TRUE&&igdebug_selection==TRUE)
                dbg(DEBUG,"<HandleInternalData> CUTE: %05d Data <%s>",__LINE__,pclTmpSqlAnswer);
#endif




	    }
	  for(ilCurDev=0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
	    {
	      /*find location data*/
	      if(!strcmp(rgDV.prDevPag[ilCurDev].pcFilterField,pclFilterField)&&!strcmp(rgDV.prDevPag[ilCurDev].pcFilterContents,pclFilterContents))
		{
		  for(ilCurRec=0;ilCurRec<rgDV.prDevPag[ilCurDev].iNoOfRec;ilCurRec++)
		    {
		      if(atoi(rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].pcUrno)==atoi(pclURNO))
			{
			  /*hit -> reset record*/
			  rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].pcFields[0]='\0';
			  rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].pcRecordset[0]='\0';
			  rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].pcUrno[0]='\0';
			  rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].iDisplayFlag=FALSE;
			}
		    }
		  for(ilCurRec=0;ilCurRec<rgDV.prDevPag[ilCurDev].iNoOfRec;ilCurRec++)
		    {
		      if(ilCurRec+1==atoi(pclDSEQ))
			{
			  /*hit -> update*/
			  /*sort due to displaysequence*/
			  rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].pcFields[0]='\0';
			  rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].pcRecordset[0]='\0';
			  rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].pcUrno[0]='\0';
			  strcpy(rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].pcFields,pcgFLVFIELDS);
			  strcpy(rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].pcRecordset,pclTmpSqlAnswer);
			  strcpy(rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].pcUrno,pclURNO);
			  rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].iDisplayFlag=TRUE;
			}
		    }
		  /*...prepare data*/
		  if((ilRc=RefreshCounter(TRUE,NULL,pcgFLVFIELDS,pclTmpSqlAnswer,ilCurDev,atoi(pclDSEQ)))!=RC_SUCCESS)
		    {
		      dbg(DEBUG,"<HandleInternalData> CUTE: %05d RefreshCounter failed with <%d>",__LINE__,ilRc);
		      /*this is not a fatal error*/
		      ilRc=RC_SUCCESS;
		    }
		}
	    }
	  return RC_SUCCESS;
	}/*end else found record in fldtab*/
    }/*end if LOCA*/
}/*end of function*/
/*********************************************************************
Function :GetHostInfos()
Paramter :IN:
Return     :RC_SUCCESS,RC_FAIL
Description:- reads the own IP-adress from the unix-system files 
                    according to the entries inside the CEDA-sgs.tab
                    hostname variable or according to the name used by the unix
                    system.
            - reads the broadcast address from sgs.tab or sets it to
                    the default value of 0xffffffff.
                - reads informations about the UDP-Service UFIS_FIDS_BC
*********************************************************************/
static int GetHostInfos()
{
  int                 ilRc;
  uint32_t              *uilHostIp;
  struct hostent     *hp;
  struct in_addr    rlIn;
  memset(pcgHostName,0x00,XS_BUFF);
  memset(pcgHostIp,0x00,XS_BUFF);
  if ((ilRc = tool_search_exco_data("NET","HOST",pcgHostName)) != RC_SUCCESS)
    {
      dbg(TRACE,"<GetHostInfos> %05d No HOST entry in sgs.tab: EXTAB! Please add!", __LINE__);
      ilRc = gethostname(pcgHostName,XS_BUFF);
    } 
  if (ilRc == RC_SUCCESS)
    {
      dbg(DEBUG,"<GetHostInfos> %05d Hostname=<%s>", __LINE__, pcgHostName);
      errno = 0;
      hp = gethostbyname(pcgHostName);
      if (hp == NULL)
	{
	  dbg(TRACE,"<GetHostInfos> %05d Gethostbyname <%s>=><%s>", __LINE__, pcgHostName, strerror(errno));
	  ilRc = RC_FAIL;
	}else{
	  uilHostIp = (uint32_t*)hp->h_addr;
	  rlIn.s_addr = *uilHostIp;
	  /*  dbg(DEBUG,"<GetHostInfos> %05d IP-Address=<%x> ", __LINE__, *uilHostIp); */
	  strcpy(pcgHostIp,(char*)inet_ntoa(rlIn)); 
	  dbg(DEBUG,"<GetHostInfos> %05d IP-Address=<%s> ", __LINE__, pcgHostIp);
	}
    }else{
      dbg(TRACE,"<GetHostInfos> %05d gethostname() failed!", __LINE__);
      ilRc = RC_FAIL;
    }    
  /* Now looking for main broadcast address */
  if ((ilRc = tool_search_exco_data("NET","BCADDR",pcgBcAddr)) != RC_SUCCESS)
    {
      dbg(TRACE,"<GetHostInfos> %05d BC-Address not set in sgs.tab!", __LINE__, pcgBcAddr);
      dbg(TRACE,"<GetHostInfos> %05d setting to default!", __LINE__);
      uigBcAddr = 0xffffffff;
      sscanf(pcgBcAddr,"%lx",&uigBcAddr);
    }else{
      dbg(DEBUG,"<GetHostInfos> %05d BC-Address from sgs.tab=<%s>", __LINE__, pcgBcAddr);
      sscanf(pcgBcAddr,"%lx",&uigBcAddr);    
      dbg(DEBUG,"<GetHostInfos> %05d BC-Address as value    =<%lx>", __LINE__, uigBcAddr);
    }
  dbg(TRACE,"- - - - - - - - - - - - - - - - - - - - - - - - - -", __LINE__);
  return ilRc;
}
/*************************************************************/
/* This funkiopn provides a active connection to the         */
/* controllers                                               */
/*************************************************************/
static int ConnectDevice()
{
  int ilCnt = 0;
  int                     ilRc = RC_SUCCESS;
  int                     ilOffset = 0;
  int                     ilResponseCreated = FALSE;
  int                     ilActiveDevice=0; 
  char                     pclResponseBuffer[XS_BUFF];
  char                     pclIp[XS_BUFF];
  int                      debug_level_save=0;/*workaround because tcp_send is corrupt (NULL in dbg)*/
  IDS_UDP_RESPONSE     rlResponse; 
  int ilCurDev = 0;
  int ilRetryCnt = 0;
  int ilTimeOffset = 10;
  ilOffset                = 0;
  rlResponse.id          = (USHORT*)&pclResponseBuffer[ilOffset];
  ilOffset               = sizeof(short);
  rlResponse.clientaddr  = (ULONG*)&pclResponseBuffer[ilOffset];
  ilOffset               = sizeof(short) + sizeof(long);
  rlResponse.serveraddr  = (ULONG*)&pclResponseBuffer[ilOffset];
  ilOffset               = sizeof(short) + (2*sizeof(long));
  rlResponse.tcpport     = (USHORT*)&pclResponseBuffer[ilOffset];
  ilOffset               = (2*sizeof(short)) + (2*sizeof(long));
  rlResponse.changenetworkpart = (USHORT*)&pclResponseBuffer[ilOffset];
  ilOffset                     = (3*sizeof(short)) + (2*sizeof(long));
  rlResponse.reserved          = &pclResponseBuffer[ilOffset];
  if (igDgramSock != 0)
    {
      /*dsplib_close(igDgramSock); */
      close(igDgramSock);
      igDgramSock =0;
    }
  do{
    if ((ilRc = dsplib_socket(&igDgramSock,SOCK_DGRAM,NULL,0,FALSE)) != RC_SUCCESS)
      {
        /*dsplib_close(igDgramSock);*/
        close(igDgramSock);
        dbg(TRACE,"<ConnectDevice> %05d dsplib_socket returns: <%d>.", __LINE__, ilRc);
      }else{
        dbg(DEBUG,"<ConnectDevice> %05d UDP-Socket = <%d>", __LINE__, igDgramSock);
      }
    ilRetryCnt++;
    nap(ilTimeOffset*ilRetryCnt);
    /*      dbg(TRACE,"<ConnectDevice>%05d RetryCnt = <%d> ilRc = <%d> igDgramSock <%d>",__LINE__,ilRetryCnt,ilRc,igDgramSock); */
  }while(ilRc!=RC_SUCCESS && ilRetryCnt < 10);
  ilActiveDevice =0;
  for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)  
    {/*for1*/
      rgDV.prDevPag[ilCurDev].iInitialize = FALSE;
      if(strcmp(rgDV.prDevPag[ilCurDev].pcDeviceState,"M") != 0) 
        {
	  ilActiveDevice++;
	  memset(pclIp,0x00,XS_BUFF);
	  *rlResponse.id = htons((USHORT) HEX_RESPONSE_ID);
	  *rlResponse.clientaddr =(ULONG) (inet_addr(rgDV.prDevPag[ilCurDev].pcIP));
	  *rlResponse.serveraddr = ((ULONG)(inet_addr(pcgHostIp)));
	  *rlResponse.tcpport = htons((USHORT) rgDV.prDevPag[ilCurDev].sPort);
	  *rlResponse.changenetworkpart = htons((USHORT)0);
	  ilResponseCreated = TRUE;
	  /* converting network-byte order to host byte order for "tcp_send_datagram" */
	  sprintf(pclIp,"%x",ntohl((ULONG)(inet_addr(rgDV.prDevPag[ilCurDev].pcIP))));
	  debug_level_save = debug_level;/*workaround because tcp_send is corrupt (NULL in dbg)*/
	  /*   dbg(TRACE,"===============RETRY_IDS_MESSAGE %d times=================",igRETRY_IDS_MESSAGE); */
	  debug_level = 0;/*workaround because tcp_send is corrupt (NULL in dbg)*/
	  for(ilCnt=0;ilCnt< igRETRY_IDS_MESSAGE;ilCnt++)
	    {
	      if ((ilRc=tcp_send_datagram(igDgramSock,pclIp,NULL,pcgUdpBCService, pclResponseBuffer,IDS_UDP_RESPONSE_SIZE))!=RC_SUCCESS)
		{
		  debug_level = debug_level_save;/*workaround because tcp_send is corrupt (NULL in dbg)*/
		  /*snapit(pclResponseBuffer,IDS_UDP_RESPONSE_SIZE,outp,"TO dZine:");*/
		  ilRc=RC_SUCCESS;
		  debug_level=0;/*workaround because tcp_send is corrupt (NULL in dbg)*/
		}
	    }
	  debug_level = debug_level_save;/*get org level*/
	  /*   dbg(TRACE,"===============RETRY_IDS_MESSAGE end  ================="); */
        }
    }
  /*close socket for other dsphdl's*/
  /*dsplib_close(igDgramSock); */
  close(igDgramSock);
  igDgramSock=0;
  igConnectDeviceFlag=TRUE;
  for(ilCurDev = 0;ilCurDev<=ilActiveDevice ;ilCurDev++)  
    {
      igAlarmCount++;
      if(igTcpTimeOutAlarmFlag==FALSE)
	{
	  dbg(DEBUG,"Start PrvConn");
	  PrvConn();
	}
    }
  igAlarmCount=0;
  igConnectDeviceFlag=FALSE;
  igTcpTimeOutAlarmFlag=FALSE;  
  for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
    {
      /*    dbg(TRACE,"IP %s,Socket %d Initialize %d",rgDV.prDevPag[ilCurDev].pcIP,rgDV.prDevPag[ilCurDev].iSocket,rgDV.prDevPag[ilCurDev].iInitialize); */
      if(rgDV.prDevPag[ilCurDev].iInitialize == TRUE)
        {
	/*    rgDV.prDevPag[ilCurDev].iInitialize = FALSE; */
	  debug_level_save=debug_level;
	  debug_level=TRACE;
	  if(rgDV.prDevPag[ilCurDev].iLostConnections==0)
	    {
	      dbg(TRACE,"Connect Device to IP %s Setup Pages %d DisplayId %s Filter %s -> %s"
	      ,rgDV.prDevPag[ilCurDev].pcIP
	      ,rgDV.prDevPag[ilCurDev].iLostConnections++
	      ,rgDV.prDevPag[ilCurDev].pcDisplayId
	      ,rgDV.prDevPag[ilCurDev].pcFilterField
	      ,rgDV.prDevPag[ilCurDev].pcFilterContents);
	    }else{
	      dbg(TRACE,"Connect Device to IP %s Lost Connections %d DisplayId %s Filter %s -> %s"
	      ,rgDV.prDevPag[ilCurDev].pcIP
	      ,rgDV.prDevPag[ilCurDev].iLostConnections++
	      ,rgDV.prDevPag[ilCurDev].pcDisplayId
	      ,rgDV.prDevPag[ilCurDev].pcFilterField
	      ,rgDV.prDevPag[ilCurDev].pcFilterContents);
	  /*startup the device*/
	    }
	  debug_level=debug_level_save;

	  SendLayoutToDZ("",ilCurDev);
	}
    }
  for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
    {
      if(rgDV.prDevPag[ilCurDev].iInitialize == TRUE)
        {
 	  rgDV.prDevPag[ilCurDev].iInitialize = FALSE;
	  /*ONLY FOR DEVICE NOT FOR PAGE*/
	  if(strcmp(rgDV.prDevPag[ilCurDev].pcFilterField,pcgCHECKIN)==0
	     &&strchr(rgDV.prDevPag[ilCurDev].pcFilterContents,0xb2)==NULL)
	    {
	      /*Initial CounterState is CLOSED*/
	      rgDV.prDevPag[ilCurDev].iCounterState=CLOSED;
	      if((ilRc=StartCounter(ilCurDev))!= RC_SUCCESS)
		{
		  ilRc= RC_SUCCESS;
		}
	    }
	  if(strcmp(rgDV.prDevPag[ilCurDev].pcFilterField,pcgBELT)==0
	     &&strchr(rgDV.prDevPag[ilCurDev].pcFilterContents,0xb2)==NULL)
	    {
	      /*Initial CounterState is CLOSED*/
	      rgDV.prDevPag[ilCurDev].iCounterState=CLOSED;
	      if((ilRc=StartCounter(ilCurDev))!= RC_SUCCESS)
		{
		  ilRc= RC_SUCCESS;
		}
	    }
	  if(strcmp(rgDV.prDevPag[ilCurDev].pcFilterField,pcgGATE)==0
	     &&strchr(rgDV.prDevPag[ilCurDev].pcFilterContents,0xb2)==NULL)
	    {
	      /*Initial CounterState is CLOSED*/

	      rgDV.prDevPag[ilCurDev].iCounterState=CLOSED;
	      if((ilRc=StartCounter(ilCurDev))!= RC_SUCCESS)
		{
		  ilRc=RC_SUCCESS;
		}
	    }
	}
    }/*end for*/
  return ilRc;
}
/******************************************************************/
/*The PrvConn() routine                                           */
/*tries to connect to all devices that belong to it's group-id    */
/******************************************************************/
static void PrvConn()
{
  int ilRc;
  int ilLen = 0;
  int ilFinalSock = 0;
  short slCursor = 0;
  short slFkt = 0;
  char pclTmpSqlAnswer[M_BUFF];
  char pclFieldBuf[XS_BUFF];
  char pclSqlBuf[M_BUFF];
  char pclTmpBuf[XS_BUFF];
  uint32_t     uilHostIp  ;
  struct sockaddr_in rlSockInfo;
  int ilCurDev = 0;
  char pclIPAddr[XS_BUFF];     
  char pclInitText[XS_BUFF];
  memset(pclInitText,0x00,XS_BUFF);
  memset(pclIPAddr,0x00,XS_BUFF);
  memset(pclSqlBuf,0x00,M_BUFF);
  memset(pclFieldBuf,0x00,XS_BUFF);
  memset(pclTmpBuf,0x00,XS_BUFF);
  memset(pclTmpSqlAnswer,0x00,M_BUFF);
  /***********************************************************************************/
  /*W A R N I N G !! W A R N I N G !! W A R N I N G !! W A R N I N G !! W A R N I N G*/
  /*never use prpDevice->dadr because its not shure that this device try to          */
  /*connect now in case of runtime problems. Use rlSockInfo.sin_addr with inet_ntoa()*/
  /*or pclIPAddr  from accept()                                                      */ 
  /***********************************************************************************/
  errno = 0;
  ilFinalSock=0;
  ilLen = sizeof(rlSockInfo);
  memset(&rlSockInfo,0x00,ilLen); 
  alarm(igTcpAcceptTimeOut);
#ifdef _SOLARIS
  ilFinalSock = accept(igStreamSock,(struct sockaddr*)&rlSockInfo,&ilLen);
#else 
  ilFinalSock = accept(igStreamSock,(struct sockaddr*)&rlSockInfo,(size_t*)&ilLen);
#endif
  alarm(0);
  uilHostIp = rlSockInfo.sin_addr.s_addr;
  /*snapit((char*)&rlSockInfo,50,outp,"Socket infos from accept");   */
  strcpy(pclIPAddr,(char*)inet_ntoa(rlSockInfo.sin_addr));
 /*   dbg(TRACE,"PrvConn: IP-Address  <%s>!",pclIPAddr);    */
  if(strcmp(pclIPAddr,"0.0.0.0")!=0&&ilFinalSock > 0)
    {
      /*sprintf(pclSqlBuf,"SELECT STAT FROM DEV%s WHERE DADR='%s'",pcgTabEnd,pclIPAddr); */
      sprintf(pclSqlBuf,"SELECT STAT FROM DEV%s WHERE DADR='%s' and grpn = '%s'",pcgTabEnd,pclIPAddr,mod_name);
      slFkt = START; 
      slCursor = 0;
      if((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))!=RC_SUCCESS)
        {
        dsplib_close(ilFinalSock);
        
        if(ilRc==1)
            dbg(TRACE,"<PRVCONN>%05d ip not found please restart dsphdl<%s>",__LINE__,pclIPAddr);
        close_my_cursor(&slCursor);
      if(ilRc == -1|| ilRc==1)
        {
          /*NOTFOUND can be correct*/
          ilRc=RC_SUCCESS;
        }else{
          dbg(TRACE,"<PRVCONN> sql_if in Line <%d> failed with <%d>",__LINE__,ilRc);
        }
    }else{  
      /*   dbg(TRACE,"<PRVCONN> sql_if in Line <%d> on ip <%s>",__LINE__,pclIPAddr); */
      DeleteCharacterInString(pclTmpSqlAnswer,cBLANK);
      close_my_cursor(&slCursor);
      StringUPR((UCHAR*)pclTmpSqlAnswer);
      if(strcmp(pclTmpSqlAnswer,"M")!=0)
        {/*"U"*/
          if (ilFinalSock > 0 && uilHostIp > 0x0)
            {/*ilFinalSock > 0*/
              for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
                {
                  if(rgDV.prDevPag[ilCurDev].iSocket == ilFinalSock)
                    {
                  /*this socket is no longer valid*/
                    rgDV.prDevPag[ilCurDev].iSocket = 0;
                    rgDV.prDevPag[ilCurDev].iInitialize = FALSE;
                    /*this is for statistics*/
                    debug_level=TRACE;
                    dbg(TRACE,"PrvConn: Found invalid Socket  <%d> for IP-Address <%s> EXIT NOW",ilFinalSock,rgDV.prDevPag[ilCurDev].pcIP);
                     exit(0);
                    }
                }/*end for*/
                /*get the current streamsocket for all connectet devices*/
                for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
                {
                 if(strcmp(rgDV.prDevPag[ilCurDev].pcIP,pclIPAddr)==0)
                  {
                    if(rgDV.prDevPag[ilCurDev].iSocket > 0)
                    {
                        dbg(TRACE,"PrvConn: Found Socket  <%d> for Device <%d> IP-Address <%s>  ",rgDV.prDevPag[ilCurDev].iSocket,ilCurDev,rgDV.prDevPag[ilCurDev].pcIP);
                        dsplib_close(rgDV.prDevPag[ilCurDev].iSocket);
                    }
                    rgDV.prDevPag[ilCurDev].iSocket = ilFinalSock;
                    rgDV.prDevPag[ilCurDev].iInitialize = TRUE;
                    ilRc = SetDevState(pclIPAddr,UP,rgDV.prDevPag[ilCurDev].pcDevUrno);
                  }
                 }/*end for*/
              /*init the first page*/
             }
        }else{
          dbg(DEBUG,"PrvConn: pclTmpSqlAnswer <%s>",pclTmpSqlAnswer);
          dsplib_close(ilFinalSock);
          for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
            {
             if(strcmp(rgDV.prDevPag[ilCurDev].pcIP,pclIPAddr)==0)
                {
                /*     dbg(TRACE,"PrvConn: pclIPAddr <%s>", pclIPAddr); */
                 rgDV.prDevPag[ilCurDev].iSocket = 0;
                 rgDV.prDevPag[ilCurDev].pcDeviceState[0]='\0';
                 strcpy(rgDV.prDevPag[ilCurDev].pcDeviceState,"M");
                }
            }
        }
    }
    } 
}
/* *********************************************************** ***** */
/* The snapit routine                                           */
/* snaps data if the cfg-file parameter is set to yes           */
/* **************************************************************** */
static void snapit(void *pcpBuffer,long lpDataLen,FILE *pcpDbgFile,char *pcpWhat)
{
  if (debug_level==DEBUG)
    {
      if (pcpWhat != NULL)
    dbg(DEBUG,"%s",pcpWhat);
      snap((char*)pcpBuffer,(long)lpDataLen,(FILE*)pcpDbgFile);
      fflush(outp);
    }
}                  
/*********************************************************************
Function : SendEvent()
Paramter : IN: pcpCmd = command for cmdblk->command
           IN: ipModID = process-ID where the event is send to
       IN: ipPriority = priority for sending ( 1- 5, usuallay 3)
       IN: pcpTable = Name (3 letters) of cmdblk->obj_name (if
           necessary), will be expanded with "pcgTabEnd".
       IN: pcpTwStart = cmdblk->twstart
       IN: pcpTwEnd = cmdblk->twend (always HOMEAP,TABEND,processname)
       IN: pcpSelection = selection for event (cmdblk->data)
       IN: pcpFields = fieldlist (corresponding to pcpdata)
       IN: pcpData = datalist (comma separated, corresponding to 
                                   pcpFields)
       IN: pcpAddStruct = additional structure to be transmitted
       IN: ipAddStructSize = size of the additional structure
Return Code: RC_SUCCESS, RC_FAIL
Result:
Description: Sends an event to another CEDA-process using que(QUE_PUT).
             Sends the event in standard CEDA-format (BCHEAD,CMDBLK,
         selection,fieldlist,datalist) or sends a different
         data structure (special) at CMDBLK->data. !! Sends always
         only one type, standard OR special, with one event !!
*********************************************************************/
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
             char *pcpTwStart, char *pcpTwEnd,
             char *pcpSelection,char *pcpFields,char *pcpData,
             char *pcpAddStruct,int ipAddStructSize)
{
  int     ilRc             = RC_FAIL;
  int     ilLen            = 0;
  EVENT   *prlOutEvent  = NULL;
  BC_HEAD *prlOutBCHead = NULL;
  CMDBLK  *prlOutCmdblk = NULL;
 
  if (pcpAddStruct == NULL)
    ipAddStructSize = 0;
  if (ipAddStructSize == 0) 
    pcpAddStruct = NULL;
  /* size-calculation for prlOutEvent */
  ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
    strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 
    ipAddStructSize + 128;
  /* memory for prlOutEvent */
  if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
    {
      dbg(TRACE,"SendEvent: cannot malloc <%d>-bytes for outgoing event!",ilLen);
      prlOutEvent = NULL;
    }else{
      /* clear whole outgoing event */
      memset((void*)prlOutEvent, 0x00, ilLen);
      /* set event structure... */
      prlOutEvent->type         = SYS_EVENT;
      prlOutEvent->command    = EVENT_DATA;
      prlOutEvent->originator   = (short)mod_id;
      prlOutEvent->retry_count  = 0;
      prlOutEvent->data_offset  = sizeof(EVENT);
      prlOutEvent->data_length  = ilLen - sizeof(EVENT); 
      /* BC_HEAD-Structure... */
      prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
      /* prlOutBCHead->rc = (short)RC_SUCCESS;*/
      prlOutBCHead->rc = (short)NETOUT_NO_ACK;/*spaeter nur bei disconnect*/
      strncpy(prlOutBCHead->dest_name,mod_name,10);
      strncpy(prlOutBCHead->recv_name, "EXCO",10);
      /* Cmdblk-Structure... */
      prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
      strcpy(prlOutCmdblk->command,pcpCmd);
      if (pcpTable != NULL)
	{
	  strcpy(prlOutCmdblk->obj_name,pcpTable);
	  strcat(prlOutCmdblk->obj_name,pcgTabEnd);
	}
      /* setting tw_x entries */
      strncpy(prlOutCmdblk->tw_start,pcpTwStart,32);
      strncpy(prlOutCmdblk->tw_end,pcpTwEnd,32);
      /* means that no additional structure is used */
      /* STANDARD CEDA-ipcs between CEDA-processes */
      if (pcpAddStruct == NULL)
	{
	  /* setting selection inside event */
	  strcpy(prlOutCmdblk->data,pcpSelection);
	  /* setting field-list inside event */
	  strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1,pcpFields);
	  /* setting data-list inside event */
	  strcpy((prlOutCmdblk->data + (strlen(pcpSelection)+1) + (strlen(pcpFields)+1)),pcpData);
	}else{
	  /*an additional structure is used and will be copied to */
	  /*cmdblk + sizeof(CMDBLK).!!! No STANDARD CEDA-ipcs is used !!!! */
	  memcpy(prlOutCmdblk->data,(char*)pcpAddStruct,ipAddStructSize);    
	}
      /*DebugPrintEvent(DEBUG,prlOutEvent);*/ 
      /*snapit((char*)prlOutEvent,ilLen,outp);*/
      dbg(DEBUG,"SendEvent: sending event to mod_id <%d>",ipModID);
      if (ipModID != 0)
	{
	  if ((ilRc = que(QUE_PUT,ipModID,mod_id,ipPriority,ilLen,(char*)prlOutEvent))
	      != RC_SUCCESS)
	    {
	      dbg(TRACE,"SendEvent: QUE_PUT returns: <%d>", ilRc);
	      exit(0);
	    }
	}else{
	  dbg(TRACE,"SendEvent: mod_id = <%d>! Can't send!",ipModID);
	}
      /* free memory */
      free((void*)prlOutEvent); 
    }
  return ilRc;
}
/*********************************************************************
Function : DisconnectDevices()
Paramter :
Return Code:
Result:
Description: performs a "shutdown;close" for the socket of the device
             and calls SetDevState() for updating the DB with 
                         connection info's
*********************************************************************/
static void DisconnectDevices()
{ 
  int ilRc = RC_SUCCESS;
  int ilCurDev = 0;
  for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
    {
      if(rgDV.prDevPag[ilCurDev].iSocket > 0)
    {
      /*   dsplib_shutdown(rgDV.prDevPag[ilCurDev].iSocket,2); */
      dsplib_close(rgDV.prDevPag[ilCurDev].iSocket);
    }
      rgDV.prDevPag[ilCurDev].iSocket = 0;
      if(strcmp(rgDV.prDevPag[ilCurDev].pcDeviceState,"M")!=0)
    {    
      if((ilRc =SetDevState(rgDV.prDevPag[ilCurDev].pcIP,DOWN,rgDV.prDevPag[ilCurDev].pcDevUrno)) == RC_SUCCESS)
        {
          dbg(TRACE,"DisconnectDevice: <%s> OK!",rgDV.prDevPag[ilCurDev].pcIP);
        }else{    
          dbg(TRACE,"DisconnectDevices: SetDevState() returns <%d>!",ilRc); 
        }
    } 
    }
}
/*********************************************************************
Function : SetDevState()
Paramter : IN: prpDevice = device-structure with informations about the device
                     IN: ipState = UP/DOWN state which should be set for the device
Return Code: RC_SUCCESS,RC_FAIL
Result:
Description: Sets the device state inside the database 
*********************************************************************/
static int SetDevState(char *prpDeviceAdr,int ipState,char *pcpUrno)
{

  char *pclFunc = "SetDevState";
  int ilRc = RC_SUCCESS;
  char pclDBSel[S_BUFF];
  char pclDBFields[S_BUFF];
  char pclDBData[S_BUFF];
  char pclDate[XS_BUFF];
  /*    char pclTwEnd[XS_BUFF]; */
  int  ilSend = FALSE;
  char pclTmpBuf[XS_BUFF];
  char *pclT;
  memset(pclDBSel,0x00,S_BUFF);
  memset(pclDBFields,0x00,S_BUFF);
  memset(pclDBData,0x00,S_BUFF);
  memset(pclDate,0x00,XS_BUFF);
  memset(pclTmpBuf,0x00,M_BUFF);
  GetServerTimeStamp("LOC",1,0,pclDate);
    
  /*sprintf(pclDBSel,"WHERE %s = '%s' and grpn = '%s'",DEV_DB_KEY,prpDeviceAdr,mod_name);*/
  sprintf(pclDBSel,"WHERE URNO = %s",pcpUrno);
  switch(ipState)
    {
    case UP:
      sprintf(pclDBFields,"%s",DEV_DB_UP);
      sprintf(pclDBData,"%s,U,%s",pclDate,pclDate);
      ilSend = TRUE;
      break;
    case DOWN:
      sprintf(pclDBFields,"%s",DEV_DB_DOWN);
      sprintf(pclDBData,"%s,D,%s",pclDate,pclDate);
      ilSend = TRUE;
      break;
    case REPLY:
      sprintf(pclDBFields,"%s",DEV_DB_REPLY);
      dbg(DEBUG,"pcgResBuf <%s>",pcgResBuf);
      DeleteCharacterInString(pcgResBuf,'\n');
      DeleteCharacterInString(pcgResBuf,'\r');
   /*     *(pcgResBuf+M_BUFF-1)='\0'; */
      pclT=strstr(pcgResBuf,"Version");
      if(pclT!=NULL)
	{
	  *(pclT+127)='\0';
	  strcpy(pclTmpBuf,pclT);
	  pclTmpBuf[127]='\0';
	  /*  dbg(TRACE,"pcgResBuf <%s>",pclTmpBuf); */
	  sprintf(pclDBData,"%s,%s",pclDate,pclTmpBuf);
	  ilSend = FALSE;
	  if ((ilRc = SendEvent("URT",igModID_Router,PRIORITY_4,"DEV","DEV_DB_UPD",pcgTwEnd,pclDBSel,
				pclDBFields,pclDBData,NULL,0)) != RC_SUCCESS)
	    {
	      dbg(TRACE,"%s: SendEvent() to <%d> returns <%d>!",pclFunc,igModID_Router,ilRc);
	    }
	}
      break;
    default:
      dbg(TRACE,"%s: Unknown state <%d> received!",pclFunc,ipState);
      break;
    }

  if (ilSend == TRUE)
    {
      if ((ilRc = SendEvent("URT",igModID_Router,PRIORITY_4,"DEV","DEV_DB_UPD",pcgTwEnd,pclDBSel,
                pclDBFields,pclDBData,NULL,0)) != RC_SUCCESS)
        {
            dbg(TRACE,"%s: SendEvent() to <%d> returns <%d>!",pclFunc,igModID_Router,ilRc);
        }
      }

  return ilRc;


} 
/*********************************************************************
Function : SetDevLastDegaussTime()
Paramter : IN: prpDevice = device-structure with informations about the device
Return Code: RC_SUCCESS,RC_FAIL
Result:
Description: Set the device last degauss time  inside the database 
*********************************************************************/
static int SetDevLastDegaussTime(void) 
{
#if defined (_WAW)|| defined (_ATH)|| defined (_TLL)
  char *pclFunc = "SetDevLastDegaussTime";
  int ilRc = RC_SUCCESS;
  char pclDBSel[S_BUFF];
  char pclDBFields[S_BUFF];
  char pclDBData[S_BUFF];
  char pclDate[XS_BUFF];
  int  ilSend = FALSE;
  int ilCurDev = 0;
  char pcgResBuf[RES_BUF_SIZE];
  memset(pcgResBuf,0x00,RES_BUF_SIZE);
  memset(pclDBSel,0x00,S_BUFF);
  memset(pclDBFields,0x00,S_BUFF);
  memset(pclDBData,0x00,S_BUFF);
  memset(pclDate,0x00,XS_BUFF);
  GetServerTimeStamp("LOC",1,0,pclDate);

  for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
    { 
      if(rgDV.prDevPag[ilCurDev].iSocket > 0&& atoi(rgDV.prDevPag[ilCurDev].pcDegaussIntervall)!=-1)
        {
	  memset(pcgResBuf,0x00,RES_BUF_SIZE);
	  gtBuildCommand("1","gtMonitorDegauss","",0,"","",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
	  sprintf(pclDBSel,"WHERE %s = '%s'",DEV_DB_KEY,rgDV.prDevPag[ilCurDev].pcIP);
	  sprintf(pclDBFields,"%s",DEV_DB_DEGAUSS);
	  sprintf(pclDBData,"%s,U,%s",pclDate,pclDate);
	  ilSend = TRUE;
	  if (ilSend == TRUE)
	    {

	      if ((ilRc = SendEvent("URT",igModID_Router,PRIORITY_4,"DEV","DEV_DB_UPD",pcgTwEnd,pclDBSel,
				    pclDBFields,pclDBData,NULL,0)) != RC_SUCCESS)
		{
		  dbg(TRACE,"%s: SendEvent() to <%d> returns <%d>!",pclFunc,igModID_Router,ilRc);
		}
	    }
	}
    }
  return ilRc;
#else
  return 0;
#endif

}/*end SetDevLastDegaussTime*/
/*********************************************************************
Function : ReadPageCfg
Paramter : char *pcpFile,char *pcpPagId,int ipCurPag
Return Code: ilRc
Result: reading page cfg file
Description: called by GetPagInfos
History:      FIDAS 010610 jhe -- added table parm
*********************************************************************/
static int ReadPageCfg(char *pcpFile,char *pcpPagId,int ipCurPag, PEDTable *pTable)
{
  int ilRc = RC_SUCCESS;
  FILE *pflPageCfg = NULL;
  char pclBuffer[L_BUFF];
  char *pclBuff = NULL;
  int debug_save_in=0; 
  int debug_save_out=0;
  char pclPageCfgFile[S_BUFF];
  memset(pclPageCfgFile,0x00,S_BUFF);
  strcpy(pclPageCfgFile,pcgPathToPages);
  strcat(pclPageCfgFile,pcpFile);
  dbg(TRACE,"=============================================================================");
  dbg(TRACE,"<ReadPageCfg>pclPageCfgFile is <%s> ilCurPag is <%d>",pclPageCfgFile,ipCurPag);
  errno = 0;
  /* FIDAS 010610 jhe -- added table parm to call */
  rgPG.prPages[ipCurPag].prPagefile= UDBXPage_CreateFromFile(pclPageCfgFile, pTable);
  return ilRc;    
}
/*****************************************************************************
following the functions for reading,storeing and handling of page informations
*****************************************************************************/
/*********************************************************************
*************************This is an init funktion*********************
**********************************************************************
Function : GetPagInfos()
Paramter : 
Return Code: RC_SUCCESS;RC_FAIL
Result:
Description: This is an init funktion
             Part 1 creates a request about page informations and
             sends it to the router.
         
         Part 2 handles the answer from a sqlhdl, stores the 
         informations in internal CEDAArray-structures
*********************************************************************/
static int GetPagInfos() 
{
  int ilRc = RC_SUCCESS;
  int ilCnt = 1;
  int ilNumberOfIniFiles = 0;
  long llFieldNum = 0;
  int ilPageCount = 0;
  int ilCurPag = 0;
  int ilCurDev = 0;
  int ilCurDev2 = 0;
  int ilHit=0;
  int ilFieldCount = 0;
  int ilFieldCount2 = 0;
  int ilNumberOfFlights =0;
  short slCursor = 0;
  short slFkt = 0;
  long llRow = ARR_FIRST;
  char pclDpid[XS_BUFF];
  char pclLastDpid[XS_BUFF];
  char pclDnop[XS_BUFF];
  char pclSqlBuf[XS_BUFF];
  char pclTmpFieldBuff[XS_BUFF]; 
  char pclData[XS_BUFF];
  char pclDataTmp[XS_BUFF];
  char pclDataTmp2[XS_BUFF];
  char pclDataDummy[L_BUFF];
  char pclFileNam[XS_BUFF];
  char pclTmpSqlAnswer[L_BUFF];
  char pclDspNames[L_BUFF];
  char pclDuplikateBuf[L_BUFF];
  /*16.01.2001*****************************************/
  char pclIniFileBuf[L_BUFF];
  char *pclTmp;
  char pclTmp1[XS_BUFF];
  /* FIDAS 010610 jhe -- decl Table */
  PEDTable *pTable;

  pcgGetPagInfoArrayName = "PageInfo";
  /*get a lot of fields...*/
  ilFieldCount2=0;
  pcgGetPagInfoFields[0]='\0';
  for(ilFieldCount=0;ilFieldCount< PAG_MAXFIELDS/2;ilFieldCount++)
    {
      memset(pclTmpFieldBuff,0x00,XS_BUFF);
      sprintf(pclTmpFieldBuff,"PI%02d,PN%02d",ilFieldCount+1,ilFieldCount+1);
      strcat(pcgGetPagInfoFields,pclTmpFieldBuff);
      strcat(pcgGetPagInfoFields,",");
      lgGetPagInfoFldLen[ilFieldCount2++] = 12;
      lgGetPagInfoFldLen[ilFieldCount2++] = 3;
    }
  strcat(pcgGetPagInfoFields,"DPID");
  lgGetPagInfoFldLen[ilFieldCount2++] = 12;
  /*16.01.2001*****************************************/
  strcat(pcgGetPagInfoFields,",DICF");/*if delete don't forget ++ in upperline*/ 
  lgGetPagInfoFldLen[ilFieldCount2] = 12;
  /******************************************/
  dbg(DEBUG,"GetPagInfos%05d: pcgGetPagInfoFields <%s>PAG_MAXFIELDS= %d",__LINE__,pcgGetPagInfoFields,PAG_MAXFIELDS);
  memset(pcgGetPagInfoTab,0x00,10);
  sprintf(pcgGetPagInfoTab,"DSP%s",pcgTabEnd);
  memset(pcgGetPagInfoSel,0x00,L_BUFF);
  memset(pclDspNames,0x00,L_BUFF);
  strcpy(pclDspNames," (");
  /*substring problem fixed dxb 01072001*/
  for(ilCurDev = 0;ilCurDev < rgDV.iNoOfDevices;ilCurDev++)
    {
      ilHit=TRUE;
      for(ilCurDev2 = ilCurDev+1;ilCurDev2 < rgDV.iNoOfDevices;ilCurDev2++)
        {
	  if(!strcmp(rgDV.prDevPag[ilCurDev].pcDisplayId,rgDV.prDevPag[ilCurDev2].pcDisplayId))
	    {
	      ilHit=FALSE;
	    }
        }
      if(ilHit==TRUE)
	{
	  strcat(pclDspNames,"DPID ='");
	  strcat(pclDspNames,rgDV.prDevPag[ilCurDev].pcDisplayId);
	  strcat(pclDspNames,"'");
	  strcat(pclDspNames," or ");
	}
    }
  pclDspNames[strlen(pclDspNames)-4]='\0';
  strcat(pclDspNames,")");
  if (igUseHopo == TRUE)
    sprintf(pcgGetPagInfoSel,"WHERE HOPO = '%s'AND%s",pcgHomeAp,pclDspNames);
  else
    sprintf(pcgGetPagInfoSel,"WHERE %s",pclDspNames);
  
  ilRc = CEDAArrayCreateInitCount(&sgGetPagInfo,pcgGetPagInfoArrayName,pcgGetPagInfoTab,pcgGetPagInfoSel,
				  NULL,NULL,pcgGetPagInfoFields,lgGetPagInfoFldLen,0,3);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"GetPagInfos: CEDAArrayCreateInitCount for <%s> failed  with <%d>!",
	  pcgGetPagInfoArrayName, ilRc);
    }else{
      if ((ilRc = CEDAArrayFill(&sgGetPagInfo,pcgGetPagInfoArrayName,NULL)) != RC_SUCCESS)
	{
	  dbg(TRACE,"GetPageInfos: CEDAArrayFill for <%s> failed  with <%d>!", pcgGetPagInfoArrayName, ilRc);
	}
    }
  /*...and now filter duplikates*/
  ilFieldCount = 0;
  ilPageCount = 0;
  llRow = ARR_FIRST; 
  memset(pclTmpFieldBuff,0x00,XS_BUFF); 
  memset(pclData,0x00,XS_BUFF);
  memset(pclDuplikateBuf,0x00,L_BUFF);
  /*16.01.2001*****************************************/
  memset(pclIniFileBuf,0x00,L_BUFF);
  /*init buffer*/
  while ((ilRc = CEDAArrayGetRow(&sgGetPagInfo,pcgGetPagInfoArrayName,llRow,',',L_BUFF,pclDataDummy) == RC_SUCCESS))
    { 
      memset(pclTmpFieldBuff,0x00,XS_BUFF); 
      ilFieldCount = 0;
      sprintf(pclTmpFieldBuff,"PI%02d",ilFieldCount+1);
      llRow = ARR_CURRENT;
      llFieldNum = ilFieldCount+1;
      while ((ilRc = CEDAArrayGetField(&sgGetPagInfo,pcgGetPagInfoArrayName,NULL,pclTmpFieldBuff ,XS_BUFF,llRow,pclData)
	      == RC_SUCCESS) && (ilFieldCount<PAG_MAXFIELDS))
        {
	  /*only valid fields*/
	  DeleteCharacterInString(pclData,cBLANK);
	  if(strlen(pclData) > 3)
            {
	      memset(pclDataTmp,0x00,XS_BUFF);
	      memset(pclDataTmp2,0x00,XS_BUFF);
	      /*to avoid substring problem dxb 01072001*/
	      sprintf(pclDataTmp2,",%s,",pclData);
	      sprintf(pclDataTmp,"%s,",pclData);
	      dbg(TRACE,"%05d pclTmpData <%s> , pclData <%s>",__LINE__,pclDataTmp,pclData);
	      if(strstr(pclDuplikateBuf,pclDataTmp) == NULL&&strstr(pclDuplikateBuf,pclDataTmp2) == NULL)
                {
		  /*to avoid substring problem dxb 01072001*/
		  strcat(pclDuplikateBuf,pclData);
		  strcat(pclDuplikateBuf,",");
		  ilPageCount++;
                }
            }
	  memset(pclTmpFieldBuff,0x00,XS_BUFF);
	  memset(pclData,0x00,XS_BUFF);
	  ilFieldCount++;
	  llFieldNum = ilFieldCount+1;
	  sprintf(pclTmpFieldBuff,"PI%02d",ilFieldCount+1);
        }
      /**16.01.2001*********************************************/
      strcpy(pclTmpFieldBuff,"DICF");
      if(ilRc = CEDAArrayGetField(&sgGetPagInfo,pcgGetPagInfoArrayName,NULL,pclTmpFieldBuff ,XS_BUFF,llRow,pclData)
	 == RC_SUCCESS)
        {
	  DeleteCharacterInString(pclData,cBLANK);
	  if(strlen(pclData) > 3)
            {
	      if(strstr(pclIniFileBuf,pclData) == NULL)
                {
		  strcat(pclIniFileBuf,pclData);
		  strcat(pclIniFileBuf,",");
		  ilPageCount++;
                }
            }
	  memset(pclData,0x00,XS_BUFF);
	  memset(pclTmpFieldBuff,0x00,XS_BUFF);
        }
      /************************************************************/
      llRow = ARR_NEXT; 
    }
  /*memory allocation for pages*/
  /*save number of pages and names, delete last ','*/
  pclDuplikateBuf[strlen(pclDuplikateBuf)-1]='\0';
  /*16.01.2001*****************************************/
  pclIniFileBuf[strlen(pclIniFileBuf)-1]='\0';
  dbg(TRACE,"<GetPagInfos> in DSPTAB we found  <%s>",pclDuplikateBuf);
  /*16.01.2001*****************************************/
  dbg(TRACE,"<GetPagInfos> in DSPTAB we found IniFiles  <%s>",pclIniFileBuf);
  if(pclIniFileBuf[0]=='\0')
    {
      ilNumberOfIniFiles = 0;
    }else{
      ilNumberOfIniFiles = field_count(pclIniFileBuf);
    }
  rgPG.iNoOfPages = ilPageCount;
  dbg(TRACE,"<GetPagInfos>in  DSPTAB we found <%d> different page id's",rgPG.iNoOfPages);
  dbg(TRACE,"<GetPagInfos>in  DSPTAB we found <%d> Ini File id's", ilNumberOfIniFiles );
  if((rgPG.prPages = (PObjStruct*)malloc(rgPG.iNoOfPages*sizeof(PObjStruct))) == NULL)
    {
      dbg(TRACE,"<GetPagInfos> %05d cannot malloc %d bytes", __LINE__, (int)rgPG.iNoOfPages*sizeof(PObjStruct));
      Terminate(30);
    }
  /* FIDAS 010610 jhe -- read in Table */
  pTable = ReadPEDTable();
  /*now fill with page id's*/ 
  for(ilCurPag = 0;ilCurPag<rgPG.iNoOfPages-ilNumberOfIniFiles;ilCurPag++)
    {
      if((rgPG.prPages[ilCurPag].pcPageId = (char*)malloc(12*sizeof(char))) == NULL)
	{
	  dbg(TRACE,"<GetPagInfos> not enough memory to run "); 
	  Terminate(30);
	}else{
	  GetDataItem(rgPG.prPages[ilCurPag].pcPageId,pclDuplikateBuf,ilCurPag+1,',',"","");
	  memset(pclSqlBuf,0x00,XS_BUFF);
	  memset(pclTmpFieldBuff,0x00,XS_BUFF);
	  GetDataItem(pclTmpFieldBuff,pclDuplikateBuf,ilCurPag+1,',',"","");
	  /*here we need some standard informations from from "PAGTAB" to create later a "WHERE" clause*/
	  sprintf(pclSqlBuf,"SELECT PCFN,PCTI,PDPT,PTFB,PTFE,PTD1,PTD2,PTDC,PRFN,PDSS,PNOF,PFNT,PDTI FROM PAG%s WHERE PAGI ='%s'",pcgTabEnd, pclTmpFieldBuff);
	  slFkt = START; 
	  slCursor = 0; 
	  memset(pclFileNam,0x00,XS_BUFF);
	  memset(pclTmpSqlAnswer,0x00,L_BUFF);
	  if ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))!=RC_SUCCESS)
	    {
	      dbg(TRACE,"<GetPagInfos> sql_if <%s> ",pclSqlBuf);
	      dbg(TRACE,"<GetPagInfos> sql_if in Line <%d> failed with <%d>",__LINE__,ilRc);
	      dbg(TRACE,"ERROR: WRONG PAGE ID CHECK CONFIGURATION IN DSPTAB OR PAGTAB !");
	      DoNothing(); 
	      ilRc=RC_SUCCESS;
	    }else{
	      if(igdebug_selection==TRUE)
		{
		  dbg(TRACE,"<GetPagInfos> sql_if <%s> ",pclSqlBuf);
		}
	      GetDataItem(pclFileNam,pclTmpSqlAnswer,1,'\0',"","");
	      DeleteCharacterInString(pclFileNam,cBLANK);
	      GetDataItem(rgPG.prPages[ilCurPag].pcCarouselTime,pclTmpSqlAnswer,2,'\0',"","");
	      DeleteCharacterInString(rgPG.prPages[ilCurPag].pcCarouselTime,cBLANK);
	      GetDataItem(rgPG.prPages[ilCurPag].pcDisplayType,pclTmpSqlAnswer,3,'\0',"","");
	      DeleteCharacterInString(rgPG.prPages[ilCurPag].pcDisplayType,cBLANK);
	      GetDataItem(rgPG.prPages[ilCurPag].pcTimeFrameBegin,pclTmpSqlAnswer,4,'\0',"","");
	      DeleteCharacterInString(rgPG.prPages[ilCurPag].pcTimeFrameBegin,cBLANK);
	      GetDataItem(rgPG.prPages[ilCurPag].pcTimeFrameEnd,pclTmpSqlAnswer,5,'\0',"","");
	      DeleteCharacterInString(rgPG.prPages[ilCurPag].pcTimeFrameEnd,cBLANK);
	      GetDataItem(rgPG.prPages[ilCurPag].pcTimeParamDel1,pclTmpSqlAnswer,6,'\0',"","");
	      DeleteCharacterInString(rgPG.prPages[ilCurPag].pcTimeParamDel1,cBLANK);
	      GetDataItem(rgPG.prPages[ilCurPag].pcTimeParamDel2,pclTmpSqlAnswer,7,'\0',"","");
	      DeleteCharacterInString(rgPG.prPages[ilCurPag].pcTimeParamDel2,cBLANK);
	      GetDataItem(rgPG.prPages[ilCurPag].pcTimeParamCancelled,pclTmpSqlAnswer,8,'\0',"","");
	      DeleteCharacterInString(rgPG.prPages[ilCurPag].pcTimeParamCancelled,cBLANK);
	      GetDataItem(rgPG.prPages[ilCurPag].pcReferenceField,pclTmpSqlAnswer,9,'\0',"","");
	      DeleteCharacterInString(rgPG.prPages[ilCurPag].pcReferenceField,cBLANK);
	      GetDataItem(rgPG.prPages[ilCurPag].pcDisplaySeq,pclTmpSqlAnswer,10,'\0',"","");
	      DeleteCharacterInString(rgPG.prPages[ilCurPag].pcDisplaySeq,cBLANK);
	      GetDataItem(rgPG.prPages[ilCurPag].pcFlightNature,pclTmpSqlAnswer,12,'\0',"","");
	      DeleteCharacterInString(rgPG.prPages[ilCurPag].pcFlightNature,cBLANK);
	      GetDataItem(rgPG.prPages[ilCurPag].pcDisplayTypeInternal,pclTmpSqlAnswer,13,'\0',"","");
	      DeleteCharacterInString(rgPG.prPages[ilCurPag].pcDisplayTypeInternal,cBLANK);
	      GetDataItem(rgPG.prPages[ilCurPag].pcNumberOfFlights,pclTmpSqlAnswer,11,'\0',"","");
	      DeleteCharacterInString(rgPG.prPages[ilCurPag].pcNumberOfFlights,cBLANK);
	      if(rgPG.prPages[ilCurPag].pcNumberOfFlights[0] == '\0')
		{
		  /*dummypage?*/
		  strcpy(rgPG.prPages[ilCurPag].pcNumberOfFlights,"0");
		}else{
		  if((pclTmp=strchr(rgPG.prPages[ilCurPag].pcNumberOfFlights,'.'))!=NULL)
		    { 
		      dbg(TRACE,"NumberOfFlights <%s> ",rgPG.prPages[ilCurPag].pcNumberOfFlights);
              
		      memset(pclTmp1,0x00,XS_BUFF);
		      *pclTmp='\0';
		      strcpy(pclTmp1,++pclTmp);
		      rgPG.prPages[ilCurPag].iNoOfObjPerRec=atoi(pclTmp1);
		      dbg(TRACE,"NumberOfFlights <%s> ,iNoOfObjPerRec <%d>",rgPG.prPages[ilCurPag].pcNumberOfFlights,rgPG.prPages[ilCurPag].iNoOfObjPerRec);

		    }
		}
	      GetDataItem(rgPG.prPages[ilCurPag].pcFlightNature,pclTmpSqlAnswer,12,'\0',"","");
	      DeleteCharacterInString(rgPG.prPages[ilCurPag].pcFlightNature,cBLANK);
	      if(!strcmp(rgPG.prPages[ilCurPag].pcDisplayTypeInternal,"S"))
		{
		  if (rgPG.prPages[ilCurPag].iNoOfObjPerRec > igMaxClusterNo)
		    {
		      igMaxClusterNo= rgPG.prPages[ilCurPag].iNoOfObjPerRec;
		    }
		}
	      /*set storige to Null*/
	      rgPG.prPages[ilCurPag].iTableVisibleRows = 0;
	      rgPG.prPages[ilCurPag].iTableRowsMax = 0;
	      rgPG.prPages[ilCurPag].iTableColumns = 0;
	      rgPG.prPages[ilCurPag].pcTableFirstId[0]= '\0';
	      memset(rgPG.prPages[ilCurPag].pcTableObjNoList,0x00,M_BUFF);
	      dbg(TRACE,"DisplayType <%s> ",rgPG.prPages[ilCurPag].pcDisplayType);
	      /* FIDAS 010610 jhe -- add table parm to call */
	      
	      if((ilRc = ReadPageCfg(pclFileNam,pclTmpFieldBuff,ilCurPag, pTable) != RC_SUCCESS))
		{
		  dbg(TRACE,"<GetPagInfos> ReadPageCfg failed with <%d>",ilRc);
		}
	          
	    }/*end else*/
	  close_my_cursor(&slCursor);
	  dbg(TRACE,"<GetPagInfos>%05d NumberOfFlights <%s>", __LINE__,rgPG.prPages[ilCurPag].pcNumberOfFlights);
	  rgPG.prPages[ilCurPag].prPageTable=NULL;
	  if(rgPG.prPages[ilCurPag].pcDisplayType[0] == 'A')
	    {
	      ilNumberOfFlights=0;
	      ilNumberOfFlights=atoi(rgPG.prPages[ilCurPag].pcNumberOfFlights);
	      if(ilNumberOfFlights==0||ilNumberOfFlights > 1000)
		{
		  ilNumberOfFlights=1;
		}
	      if((rgPG.prPages[ilCurPag].prPageTable = (FieldList*)malloc(igMaxClusterNo*ilNumberOfFlights * sizeof(FieldList)))==NULL)
		{
		  dbg(TRACE,"<GetPagInfos>%05d cannot malloc  bytes", __LINE__);
		  Terminate(30);
		}
	    }
	  /*BAGGAGESUMMARY*/
	  if(rgPG.prPages[ilCurPag].pcDisplayType[0] == 'U')
	    {
	      ilNumberOfFlights=0;
	      ilNumberOfFlights=atoi(rgPG.prPages[ilCurPag].pcNumberOfFlights);
	      if(ilNumberOfFlights==0||ilNumberOfFlights > 1000)
		{
		  ilNumberOfFlights=1;
		}
	      if((rgPG.prPages[ilCurPag].prPageTable = (FieldList*)malloc(igMaxClusterNo*ilNumberOfFlights * sizeof(FieldList)))==NULL)
		{
		  dbg(TRACE,"<GetPagInfos>%05d cannot malloc  bytes", __LINE__);
		  Terminate(30);
		}
	    }
	  if(rgPG.prPages[ilCurPag].pcDisplayType[0] == 'D')
	    {
	      ilNumberOfFlights=0;
	      ilNumberOfFlights=atoi(rgPG.prPages[ilCurPag].pcNumberOfFlights);
	      if(ilNumberOfFlights==0||ilNumberOfFlights > 1000)
		{
		  ilNumberOfFlights=1;
		}
	      if((rgPG.prPages[ilCurPag].prPageTable = (FieldList*)malloc(igMaxClusterNo*ilNumberOfFlights * sizeof(FieldList)))==NULL)
		{
		  dbg(TRACE,"<GetPagInfos>%05d cannot malloc  bytes", __LINE__);
		  Terminate(30);
		}
	    }
	  /*init buffer*/
	}
      memset(pclTmpFieldBuff,0x00,XS_BUFF); 
      memset(pclSqlBuf,0x00,XS_BUFF);
    }
  /*16.01.2001*****************************************/
  ilCnt = 1;
  for(ilCurPag = rgPG.iNoOfPages-ilNumberOfIniFiles;ilCurPag<rgPG.iNoOfPages;ilCurPag++)
    {
      if((rgPG.prPages[ilCurPag].pcPageId = (char*)malloc(12*sizeof(char))) == NULL)
	{
	  dbg(TRACE,"<GetPagInfos> not enough memory to run "); 
	  Terminate(30);
	}else{
	  GetDataItem(rgPG.prPages[ilCurPag].pcPageId,pclIniFileBuf,ilCnt,',',"","");
	  memset(pclFileNam,0x00,XS_BUFF);
	  memset(pclTmpFieldBuff,0x00,XS_BUFF);
	  GetDataItem(pclFileNam,pclIniFileBuf,ilCnt,',',"","");
	  /* FIDAS 010610 jhe -- add table parm to call */
	  if((ilRc = ReadPageCfg(pclFileNam,pclTmpFieldBuff,ilCurPag, pTable) != RC_SUCCESS))
	    {
	      dbg(TRACE,"<GetPagInfos> ReadPageCfg failed with <%d>",ilRc);
	    }
	  ilCnt++;
	}
    }
  /*doing some mappings*/
  DumpStructInfos();
  /************************************************************************/
  while ((ilRc = CEDAArrayGetField(&sgDevInfo,pcgDevArrayName,NULL,"dpid",XS_BUFF,llRow,pclDpid)
	  == RC_SUCCESS))
    {
      DeleteCharacterInString(pclDpid,cBLANK);
      if (strcmp(pclDpid,pclLastDpid) != 0)
	{
	  memset(pclLastDpid,0x00,XS_BUFF);
	  strcpy(pclLastDpid,pclDpid);
	  sprintf(pclSqlBuf,"SELECT DNOP FROM DSP%s WHERE DPID='%s'",pcgTabEnd,pclLastDpid);
	  slFkt = START; 
	  slCursor = 0;
	  memset(pclDnop,0x00,XS_BUFF);
	  if ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclDnop))==RC_SUCCESS)
	    {
	      DeleteCharacterInString(pclDnop,cBLANK);
	      sscanf(pclDnop,"%d",&ilPageCount);
	    }
	  close_my_cursor(&slCursor);
	  dbg(DEBUG,"GetPagInfos:<%d> <%s> -> %dxPages.",ilCnt,pclDpid,ilPageCount);
	  ilCnt++;
	}
      memset(pclDpid,0x00,XS_BUFF);
      llRow = ARR_NEXT;
    }
  /* FIDAS 010610 jhe -- delete table */
  PEDTable_Destroy(pTable); /* null safe fct */
  return ilRc;
} 

/*this funkion is only for testing structure "rgPG"*/
static void DumpStructInfos() 
{
  int ilCurPag = 0;
  int ilCurObj = 0;
  int ilCurDataField = 0;
  int ix, nx, fx;
  UDBXCommand *pclPageCommand=NULL;
  FieldDescription *pf;
  FieldDisplayType *prlDspType;
  char * pclDspType;
  char pclTmpTableObjListBuf[M_BUFF];
  char pcltmp[M_BUFF]; 
  int ilCurFlight=0;
  int iTableArr[50];
  for(ilCurPag = 0;ilCurPag < rgPG.iNoOfPages;ilCurPag++)
    {
      ilCurFlight=0;
      dbg(DEBUG,"==========================================================");
      dbg(DEBUG,"Page id is <%s>",rgPG.prPages[ilCurPag].pcPageId);
      if(rgPG.prPages[ilCurPag].iTableVisibleRows > 0)
        dbg(DEBUG,"TableVisibleRows = <%d>",rgPG.prPages[ilCurPag].iTableVisibleRows);
      if(rgPG.prPages[ilCurPag].iTableRowsMax > 0)
        dbg(DEBUG,"TableRowsMax = <%d>",rgPG.prPages[ilCurPag].iTableRowsMax);
      if(rgPG.prPages[ilCurPag].iTableColumns > 0)
        dbg(DEBUG,"TableVisibleRows = <%d>",rgPG.prPages[ilCurPag].iTableColumns);
      rgPG.prPages[ilCurPag].iTableColumns=0;
      if(rgPG.prPages[ilCurPag].pcTableFirstId[0] != '\0')
	{
	  dbg(DEBUG,"TableFirstId <%s>",rgPG.prPages[ilCurPag].pcTableFirstId);
	  rgPG.prPages[ilCurPag].pcTableObjNoList[0]='\0';
	}
      for(ix=0; ix<rgPG.prPages[ilCurPag].prPagefile->iNCommands; ++ix)
        {
          pclPageCommand = rgPG.prPages[ilCurPag].prPagefile->prCommands[ix];
          dbg(DEBUG,"\n Command # %d--------------\n",ix);
          dbg(DEBUG,"ObjectId=`%s'",STR0(pclPageCommand->pcObjectId));
          dbg(DEBUG,"Language=%d",pclPageCommand->iLanguage);
          dbg(DEBUG,"DZCommand=`%s'",STR0(pclPageCommand->pcDZCommand));
          dbg(DEBUG,"NDZParms=%d",pclPageCommand->iNDZParms);
          dbg(DEBUG,"NFieldDesc=%d", pclPageCommand->iNFieldDesc);

          for(nx=0; nx<pclPageCommand->iNFieldDesc; ++nx)
	    {
	      pf = pclPageCommand->prFieldDesc[nx];
	      dbg(DEBUG,"FieldDesc[%d]:", nx);
	      dbg(DEBUG,"..TableName=`%s'strlen tabname %d",STR0(pf->rFieldRef.pcTableName),strlen(pf->rFieldRef.pcTableName));
	      dbg(DEBUG,"..FieldName=`%s'",STR0(pf->rFieldRef.pcFieldName));
	      dbg(DEBUG,"..Start/End=%d/%d",pf->rFieldRef.iStart, pf->rFieldRef.iEnd);
	      prlDspType = &(pclPageCommand->prFieldDesc[nx]->rDpyType);
	      pclDspType = FieldDisplayType_GetByType(prlDspType->rAny.iType);
	      dbg(DEBUG,"..DisplayType=`%s'",STR0(pclDspType));
	      switch (prlDspType->rAny.iType)
		{
		case UDBX_DT_N:
		  dbg(DEBUG,"....(no data)");
		  break;
		case UDBX_DT_S:
		  dbg(DEBUG,"....(?static?)");
		  break;
		case UDBX_DT_L:
		  dbg(DEBUG,"....Extension=`%s'",STR0(prlDspType->rL.pcExtension));
		  break;
		case UDBX_DT_T:
		case UDBX_DT_D:
		case UDBX_DT_DT:
		case UDBX_DT_CT:
		case UDBX_DT_CD:
		case UDBX_DT_CDT:
		case UDBX_DT_TT:
		case UDBX_DT_TD:
		case UDBX_DT_TDT:
		  dbg(DEBUG,"....Format=`%s'",STR0(prlDspType->rT.pcFormat));
		  break;
		case UDBX_DT_R:
		  dbg(DEBUG,"....Separator=`%s'",STR0(prlDspType->rR.pcSeparator));
		  if (strchr(STR0(prlDspType->rR.pcSeparator),'$')!=NULL)
		    {
		      strcpy(prlDspType->rR.pcSeparator,",");
		    }
		  dbg(DEBUG,"....TableName=`%s'",STR0(prlDspType->rR.rFieldRef.pcTableName));
		  dbg(DEBUG,"....FieldName=`%s'",STR0(prlDspType->rR.rFieldRef.pcFieldName));
		  dbg(DEBUG,"....Start/End=%d/%d",prlDspType->rR.rFieldRef.iStart, prlDspType->rR.rFieldRef.iEnd);
		  break;
		case UDBX_DT_B:
		  dbg(DEBUG,"....Type=`%s'",STR0(prlDspType->rB.pcType));
		  dbg(DEBUG,"....BlinkColor=%d",prlDspType->rB.iBlinkColor);
		  dbg(DEBUG,"....TimeConversion=`%c'",prlDspType->rB.cTimeConversion);
		  dbg(DEBUG,"....NFieldNames=%d",prlDspType->rB.iNFieldNames);
		  for(fx=0; fx<prlDspType->rB.iNFieldNames; ++fx)
		    dbg(DEBUG,"......FieldNames[%d]=`%s'", fx, STR0(prlDspType->rB.p2cFieldNames[fx]));
		  break;
		case UDBX_DT_A:
		  dbg(DEBUG,"....NoOfAirports=%d",prlDspType->rA.iNoOfAirports);
		  dbg(DEBUG,"....Sequence=`%c'",prlDspType->rA.cSequence);
		  dbg(DEBUG,"....Separator=`%s'",STR0(prlDspType->rA.pcSeparator));
		  dbg(DEBUG,"....NFieldNames=%d",prlDspType->rA.iNFieldNames);
		  for(fx=0; fx<prlDspType->rA.iNFieldNames; ++fx)
		    dbg(DEBUG,"......FieldNames[%d]=`%s'", fx, STR0(prlDspType->rA.p2cFieldNames[fx]));
		  break;
		case UDBX_DT_AL:
		  dbg(DEBUG,"....FieldName=`%s'",STR0(prlDspType->rAL.pcFieldName));
		  break;
		default:
		  break;
		}
	      dbg(DEBUG,"..Size=%d",pf->iSize);
	    }
          memset(pcltmp,0x00,M_BUFF);
          for(fx=0; fx<pclPageCommand->iNDZParms; ++fx)
	    {
         
	      if(fx==0)
		strcpy(pcltmp,pclPageCommand->p2cDZParms[fx]);
	      else
		sprintf(pcltmp,"%s,%s",pcltmp,pclPageCommand->p2cDZParms[fx]);
	      dbg(DEBUG,"DZParms[%d]=`%s'",fx,STR0(pclPageCommand->p2cDZParms[fx]));
	      /*start infos about table*/
         
	      if(strcmp(STR0(pclPageCommand->pcDZCommand),"gtTableDefine")==0)
		{
		  switch(fx)
		    {
		    case 4:
		      rgPG.prPages[ilCurPag].iTableVisibleRows=atoi(pclPageCommand->p2cDZParms[fx]);
		      dbg(DEBUG,"iTableVisibleRows %d",rgPG.prPages[ilCurPag].iTableVisibleRows);
		      break;    
		    case 2:        
		      rgPG.prPages[ilCurPag].iTableRowsMax = atoi(pclPageCommand->p2cDZParms[fx]);
		      dbg(DEBUG,"iTableRowsMax  %d",rgPG.prPages[ilCurPag].iTableRowsMax);
		      break;    
		    case 3:        
		      rgPG.prPages[ilCurPag].iTableColumns =atoi(pclPageCommand->p2cDZParms[fx]);
		      dbg(DEBUG,"iTableColumns %d",rgPG.prPages[ilCurPag].iTableColumns);
		      break;    
		    case 7:        
		      strcpy(rgPG.prPages[ilCurPag].pcTableFirstId,pclPageCommand->p2cDZParms[fx]);
		      dbg(DEBUG,"pcTableFirstId <%s>",rgPG.prPages[ilCurPag].pcTableFirstId);
		      break;
		    default:
		      break;
		    }
		}
	      if(strstr(STR0(pclPageCommand->pcDZCommand),"gtFiler")!=NULL)
		{
		  if((strstr(pclPageCommand->p2cDZParms[fx],".GIF")!=NULL||strstr(pclPageCommand->p2cDZParms[fx],".gif")!=NULL))
		    { 
		      DynStr_Cpy(pclPageCommand->prDZFile,pcgPathToGif);
		      DynStr_Cat(pclPageCommand->prDZFile,pclPageCommand->p2cDZParms[fx]);
		      dbg(DEBUG,"pclPageCommand->pcDZFile <%s>",STR0(pclPageCommand->prDZFile->chars));
		    }
		  /*for mpegs*/
		  if((strstr(pclPageCommand->p2cDZParms[fx],".mp2")!=NULL||strstr(pclPageCommand->p2cDZParms[fx],".MP2")!=NULL||
		      strstr(pclPageCommand->p2cDZParms[fx],".mp1")!=NULL||strstr(pclPageCommand->p2cDZParms[fx],".MP1")!=NULL||
		      strstr(pclPageCommand->p2cDZParms[fx],".vob")!=NULL||strstr(pclPageCommand->p2cDZParms[fx],".VOB")!=NULL))
		    {
		      DynStr_Cpy(pclPageCommand->prDZFile,pcgPathToMpeg);
		      DynStr_Cat(pclPageCommand->prDZFile,pclPageCommand->p2cDZParms[fx]);
		      dbg(DEBUG,"pclPageCommand->pcDZFile <%s>",STR0(pclPageCommand->prDZFile->chars));
		    }
		  /*for fonts*/
		  if((strstr(pclPageCommand->p2cDZParms[fx],".GFT")!=NULL||strstr(pclPageCommand->p2cDZParms[fx],".gft")!=NULL)||
		     (strstr(pclPageCommand->p2cDZParms[fx],".TTF")!=NULL||strstr(pclPageCommand->p2cDZParms[fx],".ttf")!=NULL))
		    {
		      DynStr_Cpy(pclPageCommand->prDZFile,pcgPathToFonts);
		      DynStr_Cat(pclPageCommand->prDZFile,pclPageCommand->p2cDZParms[fx]);
		      dbg(DEBUG,"pclPageCommand->pcDZFile <%s>",STR0(pclPageCommand->prDZFile->chars));
		    }
		}
	      /*end set path for file transfer*/
	    }

          if((atoi(rgPG.prPages[ilCurPag].pcTableFirstId)<= atoi(pclPageCommand->pcObjectId)&&
	      (atoi(rgPG.prPages[ilCurPag].pcTableFirstId)+rgPG.prPages[ilCurPag].iTableColumns)>=atoi(pclPageCommand->pcObjectId))&&
	     ((strcmp(STR0(pclPageCommand->pcDZCommand),"gtGIFDefine")==0)||
	      (strcmp(STR0(pclPageCommand->pcDZCommand),"gtTextDefine")==0)||
	      (strcmp(STR0(pclPageCommand->pcDZCommand),"gtTextDefineWithAttributes")==0)))
	    {
	      iTableArr[atoi(pclPageCommand->pcObjectId)-atoi(rgPG.prPages[ilCurPag].pcTableFirstId)]=ix;
	      /* if(strcmp(rgPG.prPages[ilCurPag].pcTableFirstId,pclPageCommand->pcObjectId)==0) */
	      /*   if(rgPG.prPages[ilCurPag].pcTableObjNoList[0]=='\0') */
	      /*          { */
	      /*            memset(pclTmpTableObjListBuf,0x00,M_BUFF); */
	      /*            sprintf(pclTmpTableObjListBuf,"%d",ix); */
	      /*            strcpy(rgPG.prPages[ilCurPag].pcTableObjNoList,pclTmpTableObjListBuf); */
	      /*            dbg(DEBUG,"Found Table: pclTmpTableObjListBuf = %s",rgPG.prPages[ilCurPag].pcTableObjNoList); */
	      /*          }else{ */
	      /*            memset(pclTmpTableObjListBuf,0x00,M_BUFF); */
	      /*            sprintf(pclTmpTableObjListBuf,",%d",ix); */
	      /*            strcat(rgPG.prPages[ilCurPag].pcTableObjNoList,pclTmpTableObjListBuf); */
	      /*            dbg(DEBUG,"Found Table: pclTmpTableObjListBuf = %s",rgPG.prPages[ilCurPag].pcTableObjNoList); */
	      /*          } */
	    }else if(((strcmp(STR0(pclPageCommand->pcDZCommand),"gtGIFDefine")==0)||
		      (strcmp(STR0(pclPageCommand->pcDZCommand),"gtTextDefine")==0)||
		      (strcmp(STR0(pclPageCommand->pcDZCommand),"gtTextDefineWithAttributes")==0))&&
		     strlen(pf->rFieldRef.pcTableName) >= 3)
	      {
		if(rgPG.prPages[ilCurPag].pcTableObjNoList[0]=='\0')
		  {
		    memset(pclTmpTableObjListBuf,0x00,M_BUFF);
		    sprintf(pclTmpTableObjListBuf,"%d",ix);
		    strcpy(rgPG.prPages[ilCurPag].pcTableObjNoList,pclTmpTableObjListBuf);
		    dbg(DEBUG,"Found Ufis Field def's: pclTmpTableObjListBuf = %s",rgPG.prPages[ilCurPag].pcTableObjNoList);
		  }else{
		    memset(pclTmpTableObjListBuf,0x00,M_BUFF);
		    sprintf(pclTmpTableObjListBuf,",%d",ix);
		    strcat(rgPG.prPages[ilCurPag].pcTableObjNoList,pclTmpTableObjListBuf);
		    dbg(DEBUG,"Found Ufis Field def's: pclTmpTableObjListBuf = %s",rgPG.prPages[ilCurPag].pcTableObjNoList);
		  }
	      }
          dbg(DEBUG,"DZParms <%s>",pcltmp);
        }
      for(nx=0;nx< rgPG.prPages[ilCurPag].iTableColumns;nx++)
	{
	  if(nx==0) 
	    { 
	      memset(pclTmpTableObjListBuf,0x00,M_BUFF);
	      sprintf(pclTmpTableObjListBuf,"%d",iTableArr[nx]);
	      strcpy(rgPG.prPages[ilCurPag].pcTableObjNoList,pclTmpTableObjListBuf); 
         
	    }else{
	      memset(pclTmpTableObjListBuf,0x00,M_BUFF);
	      dbg(TRACE,"%d", iTableArr[nx]); 
	      sprintf(pclTmpTableObjListBuf,",%d",iTableArr[nx]); 
	      strcat(rgPG.prPages[ilCurPag].pcTableObjNoList,pclTmpTableObjListBuf); 
	    } 
	}
      dbg(DEBUG,"Found Table: pclTmpTableObjListBuf = %s",rgPG.prPages[ilCurPag].pcTableObjNoList); 
    }      
}   
/*Get database infos for initialisation from db*/
static int GetDataForPages(int ipCurPag)
{
  int ilRc = RC_SUCCESS;
  int ilCurPag = 0;
  if(ipCurPag==-1)
    {
      for(ilCurPag= 0;ilCurPag< rgPG.iNoOfPages;ilCurPag++) 
	{ /*for1*/
	  ilRc=InitDatabaseFieldInfos(&rgPG.prPages[ilCurPag]);
	}
    }else{
      ilRc=InitDatabaseFieldInfos(&rgPG.prPages[ipCurPag]);
    }
  return ilRc;
}

static int InitDatabaseFieldInfos(PObjStruct *prPG)
{
  int ilRc = RC_SUCCESS;

  int ilCurInit = 0;
  int ilCurObj = 0;
  int ilCurDataField = 0;
  int ilCurFld = 0;
  int ilFieldCount = 0;
  int ilMaxRows = 0;
  int ilMaxFields = 0;
  int ilRowCnt = 0;
  short slCursor = 0;
  short slFkt = 0;
  struct tm *_tm;
  time_t    now;
  char      _tmpc[6]; 
  int hour_gm,hour_local;
  char pclTimeNow[15];
  char pclTime1Now[15];
  Fields rlFld ;

  char pclUrnoBuf[M_BUFF];
  char pclUrnoList[M_BUFF];
  char pclTabBuff[XS_BUFF];
  char pclFldBuff[XS_BUFF];
  char pclSqlBuf[M_BUFF];
  char pclBuildSelectBuf[M_BUFF];
  char pclTmpSqlAnswer[M_BUFF];
  char pclTmpAnswer[XS_BUFF];
  char pclTimeFrameBegin[15];
  char pclTimeFrameEnd[15];
  char pclCountBuf[M_BUFF];
  char pclTmpCountAnswer[XS_BUFF];
  char pclTimeparamDel1[XS_BUFF];
  char pclTimeparamDel2[XS_BUFF];
  char pclTimeparamCnl[XS_BUFF];
  char pclRefField[XS_BUFF];
  memset(&rlFld,0x00,sizeof(Fields));
  now = time(NULL);
  _tm = (struct tm *)gmtime(&now);
  /*   _tm = (struct tm *)localtime(&now); */
  memset(pclTimeNow,0x00,15);
  sprintf(pclTimeNow,"%4d%02d%02d%02d%02d%02d", _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,_tm->tm_min,_tm->tm_sec);
  memset(pclTimeparamDel1,0x00,XS_BUFF);
  memset(pclTimeparamDel2,0x00,XS_BUFF);
  memset(pclTimeparamCnl,0x00,XS_BUFF);
  /*    dbg(TRACE,"igUTCDIFF is <%d>",igUTCDIFF );  */
  sprintf(pclTimeparamDel1,"%0.6f",-1*((float)igUTCDIFF+(float)atoi(prPG->pcTimeParamDel1))/1440);
  /*    dbg(TRACE,"TimeparamDel1 for <%s>",pclTimeparamDel1 );  */
  sprintf(pclTimeparamDel2,"%0.6f",-1*((float)igUTCDIFF+(float)atoi(prPG->pcTimeParamDel2))/1440);
  /*    dbg(TRACE,"TimeparamDel2 for <%s>",pclTimeparamDel2 ); */
  sprintf(pclTimeparamCnl,"%0.6f",-1*((float)igUTCDIFF+(float)atoi(prPG->pcTimeParamCancelled))/1440);
  /*    dbg(TRACE,"TimeparamCnl for <%s>",pclTimeparamCnl );  */
  /********************************************************/
  /**ARRIVALPART*******************************************/
  /********************************************************/
  strcpy(pclRefField,prPG->pcReferenceField);

  if(strchr(prPG->pcDisplayTypeInternal,'F')!=NULL&&prPG->pcDisplayType[0]!='\0')
    {
      if(prPG->prPageTable !=NULL)
	{
	  free((void*)prPG->prPageTable);
	  prPG->prPageTable=NULL;
	}
      /*THIS IS THE MASTER PART ALL FROM FDDTAB*/
      if(igdebug_selection==TRUE)
	dbg(TRACE,"Select for <%s>", prPG->pcDisplayType);
      /*clear buffer*/
      memset(pclSqlBuf,0x00,M_BUFF);
      memset(pclTmpCountAnswer,0x00,XS_BUFF);
      memset(pclBuildSelectBuf,0x00,M_BUFF);
      memset(pclTimeFrameBegin,0x00,15);
      memset(pclTimeFrameEnd,0x00,15);
      /*get parameter for timeframe*/
      strcpy(pclTimeFrameBegin,prPG->pcTimeFrameBegin);
      if(TimeFrame(pclTimeFrameBegin,0,0)!=RC_SUCCESS)
	{
	  dbg(TRACE,"InitDatabaseFieldInfos TimeFrame failed ");
	  dbg(TRACE,"InitDatabaseFieldInfos pclTimeFrameBegin is %s ",prPG->pcTimeFrameBegin);
	}
      /*get parameter for timeframe*/
      strcpy(pclTimeFrameEnd,prPG->pcTimeFrameEnd);
      if(TimeFrame(pclTimeFrameEnd,0,1)!=RC_SUCCESS)
	{
	  dbg(TRACE,"InitDatabaseFieldInfos TimeFrame failed");
	  dbg(TRACE,"InitDatabaseFieldInfos pclTimeFrameEnd is %s ",prPG->pcTimeFrameEnd);
	}
      /*get fields for ARRIVAL*/
      strcpy(pclSqlBuf,"SELECT ");
      strcat(pclSqlBuf,FDDFIELDS);
      memset(prPG->pcFieldsFromSelection,0x00,M_BUFF);
      strcpy(prPG->pcFieldsFromSelection,FDDFIELDS);
      /*create whereclause with timebounts*/
      if(strchr(prPG->pcDisplaySeq,'A')!= NULL)
	{
	  sprintf(pclBuildSelectBuf,FDDWHERE,pclTimeFrameBegin,pclTimeFrameEnd,prPG->pcDisplayType,pclRefField,pclRefField,pclTimeparamDel1,pclTimeparamCnl,pcgOrderEstimated);

	}else{
	  sprintf(pclBuildSelectBuf,FDDWHERE,pclTimeFrameBegin,pclTimeFrameEnd,prPG->pcDisplayType,pclRefField,pclRefField,pclTimeparamDel1,pclTimeparamCnl,pcgOrderScheduled);
	}
      /*built up select statement*/
      strcat(pclSqlBuf," FROM FDDTAB ");
      strcat(pclSqlBuf,pclBuildSelectBuf);
      if(igdebug_selection==TRUE)
	dbg(TRACE,"InitDatabaseFieldInfos statement is %s ",pclSqlBuf);
      /*we want to know how many flights are in these bounts...*/
      strcpy(pclCountBuf,"SELECT COUNT(*) FROM FDDTAB ");
      strcat(pclCountBuf,pclBuildSelectBuf);
      slFkt = START;
      slCursor = 0;
      if((ilRc = sql_if(slFkt,&slCursor,pclCountBuf,pclTmpCountAnswer))!=RC_SUCCESS)
	{
	  dbg(TRACE,"<InitDatabaseFieldInfos> sql_if in Line <%d> failed with <%d>",__LINE__,ilRc);
	}
      close_my_cursor(&slCursor);
      /*get maximum rows or  maximum recordsets */
      ilMaxRows = atoi(prPG->pcNumberOfFlights)*igMaxClusterNo;
      rlFld.iNoOfRows = atoi(pclTmpCountAnswer);
      if(rlFld.iNoOfRows > 0)
	{
	  if((prPG->prPageTable = (FieldList*)malloc( rlFld.iNoOfRows * sizeof(FieldList)))==NULL)
	    {
	      dbg(TRACE,"<GetPagInfos>%05d cannot malloc  bytes", __LINE__);
	      Terminate(30);
	    }
	  prPG->iTableRowsNow = atoi(pclTmpCountAnswer);
	  if(igdebug_selection==TRUE)
	    dbg(TRACE,"<InitDatabaseFieldInfos> sql_if counts <%d> flights ",rlFld.iNoOfRows);
	  /*...to allocate the space for selection*/
	  if((rlFld.prField = (FieldList*) malloc(rlFld.iNoOfRows*sizeof(FieldList))) == NULL)
	    {
	      dbg(TRACE,"<InitDatabaseFieldInfos>%05d cannot malloc  bytes", __LINE__);
	      Terminate(30);
	    }
	  ilMaxFields = field_count(FDDFIELDS);
	  slFkt = START;
	  slCursor = 0;
	  for(ilCurFld = 0;ilCurFld < rlFld.iNoOfRows;ilCurFld++)
	    {
	      memset(pclTmpSqlAnswer,0x00,M_BUFF);
	      if ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))!=RC_SUCCESS)
		{
		  if(ilRc != RC_FAIL)
		    {
		      ilRc=RC_SUCCESS;
		    }else{
		      dbg(TRACE,"<InitDatabaseFieldInfos> sql_if in Line <%d> failed with <%d>",__LINE__,ilRc);
		    }
		}else{
		  BuildItemBuffer(pclTmpSqlAnswer,FDDFIELDS,0,",");
		  strcpy(rlFld.prField[ilCurFld].pcRecordset,pclTmpSqlAnswer);
		  memset(prPG->prPageTable[ilCurFld].pcRecordset,0x00,M_BUFF);
		  memset(prPG->prPageTable[ilCurFld].pcFields,0x00,M_BUFF);
		  strcpy(prPG->prPageTable[ilCurFld].pcRecordset,rlFld.prField[ilCurFld].pcRecordset);
		  strcpy(prPG->prPageTable[ilCurFld].pcFields, FDDFIELDS);
		  if(igdebug_selection==TRUE)
		    dbg(DEBUG,"Flight recordset is <%s>",rlFld.prField[ilCurFld].pcRecordset);
		}
	      slFkt = NEXT;
	    }
	  close_my_cursor(&slCursor);
	  free((void*)rlFld.prField);
	}else{
	  prPG->iTableRowsNow=0;
	}
      /*end of fddtab part*/
    }else{
      /*begin STAFFPART*/
      if(strchr(prPG->pcDisplayTypeInternal,'S')!=NULL&&prPG->pcDisplayType[0]!='\0')
	{
	  if(prPG->prPageTable !=NULL)
	    {
	      free((void*)prPG->prPageTable);
	      prPG->prPageTable=NULL;
	    }
	  /*THIS IS THE MASTER PART ALL FROM FDDTAB*/
	  if(igdebug_selection==TRUE)
	    dbg(TRACE,"Select for <%s>", prPG->pcDisplayType);
	  /*clear buffer*/
	  memset(pclSqlBuf,0x00,M_BUFF);
	  memset(pclTmpCountAnswer,0x00,XS_BUFF);
	  memset(pclBuildSelectBuf,0x00,M_BUFF);
	  memset(pclTimeFrameBegin,0x00,15);
	  memset(pclTimeFrameEnd,0x00,15);
	  /*get parameter for timeframe*/
	  strcpy(pclTimeFrameBegin,prPG->pcTimeFrameBegin);
	  if(TimeFrame(pclTimeFrameBegin,0,0)!=RC_SUCCESS)
	    {
              dbg(TRACE,"InitDatabaseFieldInfos TimeFrame failed ");
              dbg(TRACE,"InitDatabaseFieldInfos pclTimeFrameBegin is %s ",prPG->pcTimeFrameBegin);
	    }
	  /*get parameter for timeframe*/
	  strcpy(pclTimeFrameEnd,prPG->pcTimeFrameEnd);
	  if(TimeFrame(pclTimeFrameEnd,0,1)!=RC_SUCCESS)
	    {
	      dbg(TRACE,"InitDatabaseFieldInfos TimeFrame failed");
	      dbg(TRACE,"InitDatabaseFieldInfos pclTimeFrameEnd is %s ",prPG->pcTimeFrameEnd);
	    }
	  /*get fields for ARRIVAL*/
	  strcpy(pclSqlBuf,"SELECT ");
	  strcat(pclSqlBuf,FDVFIELDS);
	  memset(prPG->pcFieldsFromSelection,0x00,M_BUFF);
	  strcpy(prPG->pcFieldsFromSelection,FDVFIELDS);
	  /*create whereclause with timebounts*/
	  sprintf(pclBuildSelectBuf,FDVWHERE,pclTimeFrameBegin,pclTimeFrameEnd,prPG->pcDisplayType,pclRefField,pclRefField,pclTimeparamDel1,pclTimeparamCnl,pcgOrderScheduled);
	  /*built up select statement*/
	  strcat(pclSqlBuf," FROM FDVTAB ");
	  strcat(pclSqlBuf,pclBuildSelectBuf);
	  if(igdebug_selection==TRUE)
	    dbg(TRACE,"InitDatabaseFieldInfos statement is %s ",pclSqlBuf);
	  /*we want to know how many flights are in these bounts...*/
	  strcpy(pclCountBuf,"SELECT COUNT(*) FROM FDVTAB ");
	  strcat(pclCountBuf,pclBuildSelectBuf);
	  slFkt = START;
	  slCursor = 0;
	  if((ilRc = sql_if(slFkt,&slCursor,pclCountBuf,pclTmpCountAnswer))!=RC_SUCCESS)
	    {
	      dbg(TRACE,"<InitDatabaseFieldInfos> sql_if in Line <%d> failed with <%d>",__LINE__,ilRc);
	    }
	  close_my_cursor(&slCursor);
	  /*get maximum rows or  maximum recordsets */
	  ilMaxRows = atoi(prPG->pcNumberOfFlights)*igMaxClusterNo;
	  rlFld.iNoOfRows = atoi(pclTmpCountAnswer);
	  if(rlFld.iNoOfRows > 0)
	    {
	      if((prPG->prPageTable = (FieldList*)malloc( rlFld.iNoOfRows * sizeof(FieldList)))==NULL)
		{
		  dbg(TRACE,"<GetPagInfos>%05d cannot malloc  bytes", __LINE__);
		  Terminate(30);
		}
	      prPG->iTableRowsNow = atoi(pclTmpCountAnswer);
	      if(igdebug_selection==TRUE)
		dbg(TRACE,"<InitDatabaseFieldInfos> sql_if counts <%d> flights ",rlFld.iNoOfRows);
	      /*...to allocate the space for selection*/
	      if((rlFld.prField = (FieldList*) malloc(rlFld.iNoOfRows*sizeof(FieldList))) == NULL)
		{
		  dbg(TRACE,"<InitDatabaseFieldInfos>%05d cannot malloc  bytes", __LINE__);
		  Terminate(30);
		}
	      ilMaxFields = field_count(FDVFIELDS);
	      slFkt = START;
	      slCursor = 0;
	      for(ilCurFld = 0;ilCurFld < rlFld.iNoOfRows;ilCurFld++)
		{
		  memset(pclTmpSqlAnswer,0x00,M_BUFF);
		  if ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))!=RC_SUCCESS)
		    {
		      if(ilRc != RC_FAIL)
			{
			  ilRc=RC_SUCCESS;
			}else{
			  dbg(TRACE,"<InitDatabaseFieldInfos> sql_if in Line <%d> failed with <%d>",__LINE__,ilRc);
			}
		    }else{
		      BuildItemBuffer(pclTmpSqlAnswer,FDVFIELDS,0,",");
		      /*    rlFld.prField[ilCurFld].pcRecordset[strlen(rlFld.prField[ilCurFld].pcRecordset)-1]='\0'; */
		      strcpy(rlFld.prField[ilCurFld].pcRecordset,pclTmpSqlAnswer);
		      memset(prPG->prPageTable[ilCurFld].pcRecordset,0x00,M_BUFF);
		      memset(prPG->prPageTable[ilCurFld].pcFields,0x00,M_BUFF);
		      strcpy(prPG->prPageTable[ilCurFld].pcRecordset,rlFld.prField[ilCurFld].pcRecordset);
		      strcpy(prPG->prPageTable[ilCurFld].pcFields,FDVFIELDS);
		      prPG->prPageTable[ilCurFld].iDisplayFlag=FALSE;
		      if(igdebug_selection==TRUE)
			dbg(DEBUG,"Flight recordset is <%s>",rlFld.prField[ilCurFld].pcRecordset);
		    }
		  slFkt = NEXT;
		}
	      close_my_cursor(&slCursor);
	      free((void*)rlFld.prField);
	    }else{
	      prPG->iTableRowsNow=0;
	    }
	}
      /*end STAFFPART*/
      /* baggage summary ....*/
      if(0 == strcmp(prPG->pcDisplayType,BAGGAGESUMMARY))
	{/*if arrival part...*/
	  if(prPG->prPageTable !=NULL)
	    {
	      free((void*)prPG->prPageTable);
	      prPG->prPageTable=NULL;
	    }
	  if(igdebug_selection==TRUE)
	    dbg(TRACE,"Select for <%s>", prPG->pcDisplayType);
	  /*clear buffer*/
	  memset(pclSqlBuf,0x00,M_BUFF);
	  memset(pclTmpCountAnswer,0x00,XS_BUFF);
	  memset(pclBuildSelectBuf,0x00,M_BUFF);
	  memset(pclTimeFrameBegin,0x00,15);
	  memset(pclTimeFrameEnd,0x00,15);
	  /*get fields for ARRIVAL*/
	  strcpy(pclSqlBuf,"SELECT ");
	  strcat(pclSqlBuf,pcgBAGSUMFIELDS);
	  memset(prPG->pcFieldsFromSelection,0x00,M_BUFF);
	  strcpy(prPG->pcFieldsFromSelection,pcgBAGSUMFIELDS);
	  /*create whereclause with timebounts*/
	  sprintf(pclBuildSelectBuf,BAGSUMWHERE);
	  /*built up select statement*/
	  strcat(pclSqlBuf," FROM FLVTAB ");
	  strcat(pclSqlBuf,pclBuildSelectBuf);
	  if(igdebug_selection==TRUE)
	    dbg(TRACE,"InitDatabaseFieldInfos statement is %s ",pclSqlBuf);
	  /*we want to know how many flights are in these bounts...*/
	  strcpy(pclCountBuf,"SELECT COUNT(*) FROM FLVTAB ");
	  strcat(pclCountBuf,pclBuildSelectBuf);
	  slFkt = START;
	  slCursor = 0;
	  if((ilRc = sql_if(slFkt,&slCursor,pclCountBuf,pclTmpCountAnswer))!=RC_SUCCESS)
	    {
	      dbg(TRACE,"<InitDatabaseFieldInfos> sql_if in Line <%d> failed with <%d>",__LINE__,ilRc);
	    }
	  close_my_cursor(&slCursor);
	  /*get maximum rows or  maximum recordsets */
	  ilMaxRows = atoi(prPG->pcNumberOfFlights)*igMaxClusterNo;
	  rlFld.iNoOfRows = atoi(pclTmpCountAnswer);
	  if(rlFld.iNoOfRows > 0)
	    {
	      prPG->iTableRowsNow = atoi(pclTmpCountAnswer);
	      if(igdebug_selection==TRUE)
		dbg(DEBUG,"<InitDatabaseFieldInfos> sql_if counts <%d> ",rlFld.iNoOfRows);
	      /*...to allocate the space for selection*/
	      if(rlFld.prField!=NULL)
		{
		  free((void*)rlFld.prField);
		  rlFld.prField=NULL;
		}
	      if((prPG->prPageTable = (FieldList*)malloc(rlFld.iNoOfRows * sizeof(FieldList)))==NULL)
		{
		  dbg(TRACE,"<GetPagInfos>%05d cannot malloc  bytes", __LINE__);
		  Terminate(30);
		}
	      if((rlFld.prField = (FieldList*) malloc(rlFld.iNoOfRows*sizeof(FieldList))) == NULL)
		{
		  dbg(TRACE,"<InitDatabaseFieldInfos>%05d cannot malloc  bytes", __LINE__);
		  Terminate(30);
		}
	      ilMaxFields = field_count(pcgBAGSUMFIELDS);
	      slFkt = START;
	      slCursor = 0;
	      for(ilCurFld = 0;ilCurFld < rlFld.iNoOfRows;ilCurFld++)
		{
		  memset(pclTmpSqlAnswer,0x00,M_BUFF);
		  if ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))!=RC_SUCCESS)
		    {
		      if(ilRc != RC_FAIL)
			{
			  ilRc=RC_SUCCESS;
			}else{
			  dbg(TRACE,"<InitDatabaseFieldInfos> sql_if in Line <%d> failed with <%d>",__LINE__,ilRc);
			}
		    }else{
		      /*1****2****3****4****5****6****7****8****9****10***11***12***13***14***15***16***17***18***19***20***21*/
		      /*URNO,FLNO,FLTN,FLNS,STOA,ORG3,VIA3,ETOA,ONBL,TTYP,REMP,BLT1,ACT3,JFNO,ETAI,TMB1,TIFA,TISA,B1BA,B1EA,LAND*/
		      /*  memset(pclTmpAnswer,0x00,XS_BUFF); */
		      BuildItemBuffer(pclTmpSqlAnswer,pcgBAGSUMFIELDS,0,",");
		      strcpy(rlFld.prField[ilCurFld].pcRecordset,pclTmpSqlAnswer);
		      memset(prPG->prPageTable[ilCurFld].pcRecordset,0x00,M_BUFF);
		      memset(prPG->prPageTable[ilCurFld].pcFields,0x00,M_BUFF);
		      strcpy(prPG->prPageTable[ilCurFld].pcRecordset,rlFld.prField[ilCurFld].pcRecordset);
		      strcpy(prPG->prPageTable[ilCurFld].pcFields,pcgBAGSUMFIELDS);
		      if(igdebug_selection==TRUE)
			dbg(TRACE,"Flight recordset is <%s>",rlFld.prField[ilCurFld].pcRecordset);
		    }
		  slFkt = NEXT;
		}
	      close_my_cursor(&slCursor);
	      if(rlFld.prField!=NULL)
		{
		  free((void*)rlFld.prField);
		  rlFld.prField=NULL;
		}
	    }else{
	      prPG->iTableRowsNow=0;
	    }
	}
      /* CHUTE PART ....*/
      if(0 == strcmp(prPG->pcDisplayType,CHUTE))
	{/*if arrival part...*/
	  if(prPG->prPageTable !=NULL)
	    {
	      free((void*)prPG->prPageTable);
	      prPG->prPageTable=NULL;
	    }
	  if(igdebug_selection==TRUE)
	    dbg(TRACE,"Select for <%s>", prPG->pcDisplayType);
	  /*clear buffer*/
	  memset(pclSqlBuf,0x00,M_BUFF);
	  memset(pclTmpCountAnswer,0x00,XS_BUFF);
	  memset(pclBuildSelectBuf,0x00,M_BUFF);
	  memset(pclTimeFrameBegin,0x00,15);
	  memset(pclTimeFrameEnd,0x00,15);
	  /*get fields for ARRIVAL*/
	  strcpy(pclSqlBuf,"SELECT ");
	  strcat(pclSqlBuf,pcgCHUTEFIELDS);
	  memset(prPG->pcFieldsFromSelection,0x00,M_BUFF);
	  strcpy(prPG->pcFieldsFromSelection,pcgCHUTEFIELDS);
	  /*create whereclause with timebounts*/
	  sprintf(pclBuildSelectBuf,CHUTEWHERE);
	  /*built up select statement*/
	  strcat(pclSqlBuf," FROM FLVTAB ");
	  strcat(pclSqlBuf,pclBuildSelectBuf);
	  if(igdebug_selection==TRUE)
	    dbg(TRACE,"InitDatabaseFieldInfos statement is %s ",pclSqlBuf);
	  /*we want to know how many flights are in these bounts...*/
	  strcpy(pclCountBuf,"SELECT COUNT(*) FROM FLVTAB ");
	  strcat(pclCountBuf,pclBuildSelectBuf);
	  slFkt = START;
	  slCursor = 0;
	  if((ilRc = sql_if(slFkt,&slCursor,pclCountBuf,pclTmpCountAnswer))!=RC_SUCCESS)
	    {
	      dbg(TRACE,"<InitDatabaseFieldInfos> sql_if in Line <%d> failed with <%d>",__LINE__,ilRc);
	    }
	  close_my_cursor(&slCursor);
	  /*get maximum rows or  maximum recordsets */
	  ilMaxRows = atoi(prPG->pcNumberOfFlights)*igMaxClusterNo;
	  rlFld.iNoOfRows = atoi(pclTmpCountAnswer);
	  if(rlFld.iNoOfRows > 0)
	    {
	      prPG->iTableRowsNow = atoi(pclTmpCountAnswer);
	      if(igdebug_selection==TRUE)
		dbg(DEBUG,"<InitDatabaseFieldInfos> sql_if counts <%d> ",rlFld.iNoOfRows);
	      /*...to allocate the space for selection*/
	      if(rlFld.prField!=NULL)
		{
		  free((void*)rlFld.prField);
		  rlFld.prField=NULL;
		}
	      if((prPG->prPageTable = (FieldList*)malloc(rlFld.iNoOfRows * sizeof(FieldList)))==NULL)
		{
		  dbg(TRACE,"<GetPagInfos>%05d cannot malloc  bytes", __LINE__);
		  Terminate(30);
		}

	      if((rlFld.prField = (FieldList*) malloc(rlFld.iNoOfRows*sizeof(FieldList))) == NULL)
		{
		  dbg(TRACE,"<InitDatabaseFieldInfos>%05d cannot malloc  bytes", __LINE__);
		  Terminate(30);
		}
	      ilMaxFields = field_count(pcgCHUTEFIELDS);
	      slFkt = START;
	      slCursor = 0;
	      for(ilCurFld = 0;ilCurFld < rlFld.iNoOfRows;ilCurFld++)
		{
		  memset(pclTmpSqlAnswer,0x00,M_BUFF);
		  if ((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))!=RC_SUCCESS)
		    {
		      if(ilRc != RC_FAIL)
			{
			  ilRc=RC_SUCCESS;
			}else{
			  dbg(TRACE,"<InitDatabaseFieldInfos> sql_if in Line <%d> failed with <%d>",__LINE__,ilRc);
			}
		    }else{
		      /*1****2****3****4****5****6****7****8****9****10***11***12***13***14***15***16***17***18***19***20***21*/
		      /*URNO,FLNO,FLTN,FLNS,STOA,ORG3,VIA3,ETOA,ONBL,TTYP,REMP,BLT1,ACT3,JFNO,ETAI,TMB1,TIFA,TISA,B1BA,B1EA,LAND*/
		      BuildItemBuffer(pclTmpSqlAnswer,pcgCHUTEFIELDS,0,",");
		      strcpy(rlFld.prField[ilCurFld].pcRecordset,pclTmpSqlAnswer);
		      memset(prPG->prPageTable[ilCurFld].pcRecordset,0x00,M_BUFF);
		      memset(prPG->prPageTable[ilCurFld].pcFields,0x00,M_BUFF);
		      strcpy(prPG->prPageTable[ilCurFld].pcRecordset,rlFld.prField[ilCurFld].pcRecordset);
		      strcpy(prPG->prPageTable[ilCurFld].pcFields,pcgCHUTEFIELDS);
		      if(igdebug_selection==TRUE)
			dbg(TRACE,"Flight recordset is <%s>",rlFld.prField[ilCurFld].pcRecordset);
		    }
		  slFkt = NEXT;
		}
	      close_my_cursor(&slCursor);
	      if(rlFld.prField!=NULL)
		{
		  free((void*)rlFld.prField);
		  rlFld.prField=NULL;
		}
	    }else{
	      prPG->iTableRowsNow=0;
	    }
	}
      /*...Chute part*/
    }/*end else fdd*/

  return ilRc;
}

static int SetAlarmPage(int ipOption)
{
  int ilRc = RC_FAIL;
  int ilCurPag = 0;
  int ilNoOfParam = 0;
  int ilCurInit = 0;
  int ilCurObj = 0;
  int ilSocket =0;
  int ilCurDev = 0;
  UDBXCommand *prlPageCommand=NULL;
  char pclCarouselBuf[XS_BUFF];
  char pclParameterBuf[XS_BUFF];
  char pclObjectNameBuf[XS_BUFF];
  char pclFilerBuf[XS_BUFF];
  char pclPageId[XS_BUFF];
  memset(pcgResBuf,0x00,RES_BUF_SIZE);
  if(ipOption==TRUE)
    {
      for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
	{/*for 1*/
	  /*only for connected sockets*/
	  rgDV.prDevPag[ilCurDev].iAlarmFlag=TRUE;
	  if(rgDV.prDevPag[ilCurDev].iSocket > 0&&rgDV.prDevPag[ilCurDev].iAlarmPage != -1)
	    {
	      /*get the alarmpage number*/
	      dbg(DEBUG,"SetAlarmPage connectet on socket  <%d>",rgDV.prDevPag[ilCurDev].iSocket) ;
	      /*startup the device*/
	      memset(pcgResBuf,0x00,RES_BUF_SIZE);
	      memset(pcgResBuf,0x00,RES_BUF_SIZE);
	      gtBuildCommand("1","gtFilerSetOutOfDateChecking","gtSYSTEM_FILER",1,"0","",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
	      
	      memset(pclPageId,0x00,XS_BUFF);
	      strcpy(pclPageId,rgDV.prDevPag[ilCurDev].pcAlarmPageNo);
	      /* sprintf(pclPageId,"%d",rgDV.prDevPag[ilCurDev].iAlarmPage); */
	      /* reference device -> pages*/
	      ilCurPag = rgDV.prDevPag[ilCurDev].iAlarmPage;
	      for(ilCurObj= 0;ilCurObj< rgPG.prPages[ilCurPag].prPagefile->iNCommands;ilCurObj++) 
		{ /*for 3*/
		  /*start the gtbuffer*/
		  prlPageCommand = rgPG.prPages[ilCurPag].prPagefile->prCommands[ilCurObj];
		  memset(pclObjectNameBuf,0x00,XS_BUFF);
		  if(strstr(prlPageCommand->pcDZCommand,"gtPage")!=NULL)
		    {
		      strcpy(pclObjectNameBuf,rgDV.prDevPag[ilCurDev].pcAlarmPageNo);
		    }else{
		      strcpy(pclObjectNameBuf,prlPageCommand->pcObjectId);
		    }
		  DeleteCharacterInString(pclObjectNameBuf,cBLANK);          
		  /*continuing gtbuffer*/
		  memset(pclParameterBuf,0x00,XS_BUFF);
		  memset(pclFilerBuf,0x00,XS_BUFF);
		  memset(pcgResBuf,0x00,RES_BUF_SIZE);
		  GetdZParameter(pclParameterBuf,prlPageCommand,XS_BUFF);
		  ilNoOfParam = 0;
		  ilNoOfParam = prlPageCommand->iNDZParms;
		  if(prlPageCommand->prDZFile->chars !=NULL)
		    strcpy(pclFilerBuf,prlPageCommand->prDZFile->chars);
		  ilRc= gtBuildCommand("1",prlPageCommand->pcDZCommand,pclObjectNameBuf,ilNoOfParam,pclParameterBuf,pclFilerBuf,rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
		  if(ilRc < RC_SUCCESS) 
		    { 
		      dbg(TRACE,"gtBuildCommand failed with <%d>",ilRc); 
		      dbg(TRACE,"gtBuildCommand resultbuf is<%s>",pcgResBuf);
		    } 
		}/*end for 3*/
	      memset(pcgResBuf,0x00,RES_BUF_SIZE);
	      gtBuildCommand("1","gtTerminalClearPageCarousel","gtSYSTEM_TERMINAL",0,"","",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
	      memset(pcgResBuf,0x00,RES_BUF_SIZE);
	      gtBuildCommand("1","gtPageDraw",pclPageId,0,"","",ilSocket,pcgResBuf);
	    }/*end if only for connected sockets*/
	}/*end for 1*/
    }
  if(ipOption==FALSE)
    {
      for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
	{/*for 1*/
	  /*only for connected sockets*/
	  rgDV.prDevPag[ilCurDev].iAlarmFlag=FALSE;
      
	  if(rgDV.prDevPag[ilCurDev].iSocket > 0)
	    {
	      SendLayoutToDZ(NULL,ilCurDev); 
	    }
	}

      for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
	{/*for 1*/
	  /*only for connected sockets*/
	  if(rgDV.prDevPag[ilCurDev].iSocket > 0)
	    {
	      /*ONLY FOR DEVICE NOT FOR PAGE*/
	      if(strcmp(rgDV.prDevPag[ilCurDev].pcFilterField,pcgCHECKIN)==0
		 &&strchr(rgDV.prDevPag[ilCurDev].pcFilterContents,0xb2)==NULL)
		{
		  /*Initial CounterState is CLOSED*/
		  rgDV.prDevPag[ilCurDev].iCounterState=CLOSED;
		  if((ilRc=StartCounter(ilCurDev))!= RC_SUCCESS)
		    {
		      ilRc= RC_SUCCESS;
		    }
		}
	      if(strcmp(rgDV.prDevPag[ilCurDev].pcFilterField,pcgBELT)==0
		 &&strchr(rgDV.prDevPag[ilCurDev].pcFilterContents,0xb2)==NULL)
		{
		  /*Initial CounterState is CLOSED*/
		  rgDV.prDevPag[ilCurDev].iCounterState=CLOSED;
		  if((ilRc=StartCounter(ilCurDev))!= RC_SUCCESS)
		    {
		      ilRc= RC_SUCCESS;
		    }
		}
	      if(strcmp(rgDV.prDevPag[ilCurDev].pcFilterField,pcgGATE)==0
		 &&strchr(rgDV.prDevPag[ilCurDev].pcFilterContents,0xb2)==NULL)
		{
		  /*Initial CounterState is CLOSED*/
		  rgDV.prDevPag[ilCurDev].iCounterState=CLOSED;
		  if((ilRc=StartCounter(ilCurDev))!= RC_SUCCESS)
		    {
		      ilRc=RC_SUCCESS;
		    }
		}
	    }
	}
    }
  return ilRc;
}
static int SetDefaultPage(int ipOption,int ipCurDev,int ipCarousel)
{
  int ilRc = RC_FAIL;
  int ilCurPag = 0;
  int ilNoOfParam = 0;
  int ilCurInit = 0;
  int ilCurObj = 0;
  int ilSocket =0;
  int ilCurDev = 0;
  int nx;
  UDBXCommand *prlPageCommand=NULL;
  FieldDescription *pf;
  FieldDisplayType *prlDspType;
  char pclCarouselBuf[XS_BUFF];
  char pclParameterBuf[XS_BUFF];
  char pclObjectNameBuf[XS_BUFF];
  char pclFilerBuf[XS_BUFF];
  char pclPageId[XS_BUFF];
  char pclSendParam[XS_BUFF+1];
  char pclSendObj[XS_BUFF];
  memset(pcgResBuf,0x00,RES_BUF_SIZE);
  ilCurDev=ipCurDev;
  ilSocket= rgDV.prDevPag[ilCurDev].iSocket;
  if(ipOption==TRUE)
    {
      rgDV.prDevPag[ilCurDev].iCarouselState=FALSE;
      /*only for connected sockets*/
      /* rgDV.prDevPag[ilCurDev].iDefaultFlag=TRUE; */
      if(rgDV.prDevPag[ilCurDev].iSocket > 0&& rgDV.prDevPag[ilCurDev].iDefaultPage != -1&&rgDV.prDevPag[ilCurDev].iCounterState==OPEND)
        {
	  rgDV.prDevPag[ilCurDev].iCounterState=CLOSED;
	  rgDV.prDevPag[ilCurDev].pcLocationLogo[0]='\0';
	  rgDV.prDevPag[ilCurDev].iLogo=0;
          /*startup the device*/
          memset(pcgResBuf,0x00,RES_BUF_SIZE);
	  if(atoi(rgPG.prPages[ilCurPag].pcCarouselTime)>0 )
            {
	      gtBuildCommand("1","gtTerminalClearPageCarousel","gtSYSTEM_TERMINAL",0,"","",ilSocket,pcgResBuf);
	    }
          memset(pclPageId,0x00,XS_BUFF);

          strcpy(pclPageId,rgDV.prDevPag[ilCurDev].pcDefaultPage);


	  /*reference device -> pages*/
          ilCurPag = rgDV.prDevPag[ilCurDev].iDefaultPage;

          memset(pcgResBuf,0x00,RES_BUF_SIZE);
	  if(igshow_event==TRUE)
            dbg(TRACE,"SetDefaultPage %s on %s %s -> %s Number <%s> ",
		rgPG.prPages[ilCurPag].pcPageId,
		rgDV.prDevPag[ilCurDev].pcFilterField,
		rgDV.prDevPag[ilCurDev].pcFilterContents,
		rgDV.prDevPag[ilCurDev].pcIP,pclPageId) ;
         
          gtBuildCommand("1","gtPageSetCurrent",pclPageId,1,"-1","",ilSocket,pcgResBuf);  
          memset(pcgResBuf,0x00,RES_BUF_SIZE);
	  if(igrefresh==TRUE)
	    {
	      gtBuildCommand("1","gtCollectionFreezeDrawing",pclPageId,0,"","",ilSocket,pcgResBuf);
	    } 
          for(ilCurObj= 0;ilCurObj< rgPG.prPages[ilCurPag].prPagefile->iNCommands;ilCurObj++) 
	    { /*for 3*/
	      /*start the gtbuffer*/
	      prlPageCommand = rgPG.prPages[ilCurPag].prPagefile->prCommands[ilCurObj];
	      pclObjectNameBuf[0]='\0';
	      if(strstr(prlPageCommand->pcDZCommand,"gtPage")!=NULL)
		{
		  strcpy(pclPageId,rgDV.prDevPag[ilCurDev].pcDefaultPage);
		}else{
		  strcpy(pclObjectNameBuf,prlPageCommand->pcObjectId);
		}
	      DeleteCharacterInString(pclObjectNameBuf,cBLANK);          
	      /*continuing gtbuffer*/
	      for(nx=0; nx<prlPageCommand->iNFieldDesc; ++nx)
		{
		  pf = prlPageCommand->prFieldDesc[nx];
		  if(!strcmp(STR0(pf->rFieldRef.pcTableName),"DEV"))
                    {
		      memset(pclSendParam,0x00,S_BUFF);
		      memset(pclSendObj,0x00,XS_BUFF);
		      if(!strcmp(STR0(pf->rFieldRef.pcFieldName),"DFCO"))
			{
			  strcpy(pclSendParam,rgDV.prDevPag[ilCurDev].pcFilterContents);
			  strcpy(pclSendObj,prlPageCommand->pcObjectId);
			  dbg(TRACE,"Send Layout Found Device Info <%s> -> <%s> Object Id <%s>",STR0(pf->rFieldRef.pcFieldName),pclSendParam,pclSendObj);
			}
		      if(!strcmp(STR0(pf->rFieldRef.pcFieldName),"DFFD"))
			{
			  strcpy(pclSendParam,rgDV.prDevPag[ilCurDev].pcFilterContents);
			  strcpy(pclSendObj,prlPageCommand->pcObjectId);
			  dbg(TRACE,"Send Layout Found Device Info <%s> -> <%s> Object Id <%s>",STR0(pf->rFieldRef.pcFieldName),pclSendParam,pclSendObj);
			}
		    }
		}
	      memset(pclParameterBuf,0x00,XS_BUFF);
	      memset(pclFilerBuf,0x00,XS_BUFF);
	      GetdZParameter(pclParameterBuf,prlPageCommand,XS_BUFF);
	      ilNoOfParam = 0;
	      ilNoOfParam = prlPageCommand->iNDZParms;
	      /*send only data for defaultpage*/
	      if(pclSendObj[0]!='\0')
		{
		  if(prlPageCommand->iLanguage == TRUE)
		    {
		      MapToArabic(pclSendParam,S_BUFF);
		    }
		  ilRc= gtBuildCommand("1","gtTextPutFormatted",pclSendObj,1,pclSendParam,"",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
		  pclSendObj[0]='\0';
		}
	    }/*end for 3*/
          memset(pcgResBuf,0x00,RES_BUF_SIZE);
	  if(igrefresh==TRUE)
	    {
	      gtBuildCommand("1","gtCollectionRedraw",pclPageId,0,"","",ilSocket,pcgResBuf);
	    }else{
	      gtBuildCommand("1","gtPageDraw",pclPageId,0,"","",ilSocket,pcgResBuf);
	    }  
        }/*end if only for connected sockets*/
    }
  if(rgDV.prDevPag[ilCurDev].iSocket > 0&&ipOption==FALSE&&ipCarousel==TRUE&&rgDV.prDevPag[ilCurDev].iCarouselState==FALSE)
    {
    
      ilCurPag = rgDV.prDevPag[ilCurDev].iDefaultPage;
      memset(pclCarouselBuf,0x00,XS_BUFF);
      strcpy(pclCarouselBuf,rgDV.prDevPag[ilCurDev].pcDefaultPage);
      strcat(pclCarouselBuf,",");
      strcat(pclCarouselBuf,rgPG.prPages[ilCurPag].pcCarouselTime);
      if(atoi(rgPG.prPages[ilCurPag].pcCarouselTime)>0 )
	{
	  dbg(TRACE,"pclCarouselBuf <%s>",pclCarouselBuf);
	  gtBuildCommand("1","gtTerminalAddPageCarousel","gtSYSTEM_TERMINAL",2,pclCarouselBuf,"",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
	  rgDV.prDevPag[ilCurDev].iCarouselState=TRUE;
	}
    }



  return ilRc;
}
static int SendLayoutToDZ(char* pcpField,int ipCurDev)
{
  int ilRc = RC_FAIL;
  int ilCurPag = 0;
  int ilCurDevPag = 0;
  int ilNoOfParam = 0;
  int ilCurInit = 0;
  int ilCurObj = 0;
  int ilSocket =0; 
  int ilCurDev = 0;
  int nx;
  UDBXCommand *prlPageCommand=NULL;
  FieldDescription *pf;
  FieldDisplayType *prlDspType;
  char *pclDspType=NULL;
  char pclCarouselBuf[XS_BUFF];
  char pclParameterBuf[M_BUFF];
  char pclObjectNameBuf[XS_BUFF];
  char pclFilerBuf[XS_BUFF];
  char pclUrno[M_BUFF];
  char pclSendParam[S_BUFF];
  char pclSendObj[XS_BUFF];
  memset(pcgResBuf,0x00,RES_BUF_SIZE);
  if(ipCurDev >=0)
    { 
      ilCurDev = ipCurDev;
      if(rgDV.prDevPag[ilCurDev].iSocket > 0)
	{/*if Socket*/
	  ilSocket=rgDV.prDevPag[ilCurDev].iSocket;
	  dbg(DEBUG,"SendLayoutToDZ connectet on socket  <%d>",rgDV.prDevPag[ilCurDev].iSocket) ;
	  /*startup the device*/
	  memset(pcgResBuf,0x00,RES_BUF_SIZE);
	  gtBuildCommand("1","gtTerminalInit","gtSYSTEM_TERMINAL",1,"0","",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);  
	  memset(pcgResBuf,0x00,RES_BUF_SIZE);
	  gtBuildCommand("1","gtMonitorStandby","gtSYSTEM_MONITOR",1,"0","",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
	  if(rgDV.prDevPag[ilCurDev].pcBrightness[0]!='\0')
	    {
	      gtBuildCommand("1","gtMonitorBrightness","gtSYSTEM_MONITOR",1,rgDV.prDevPag[ilCurDev].pcBrightness,"",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
	    }
	  memset(pcgResBuf,0x00,RES_BUF_SIZE);
	  gtBuildCommand("1","gtFilerSetOutOfDateChecking","gtSYSTEM_FILER",1,"0","",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
	  memset(pcgResBuf,0x00,RES_BUF_SIZE);
	  gtBuildCommand("1","gtTerminalStatusRequest","gtSYSTEM_TERMINAL",0,"","",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
	  if((ilRc =SetDevState(rgDV.prDevPag[ilCurDev].pcIP,REPLY,rgDV.prDevPag[ilCurDev].pcDevUrno)) != RC_SUCCESS)
	    {
	      dbg(TRACE,"DisconnectDevices: SetDevState() returns <%d>!",ilRc); 
	    }
	  /*reference device -> pages*/
	  if(rgDV.prDevPag[ilCurDev].iConfigPage != RC_FAIL)
	    {
	      if((ilRc = SendIniPage(ilCurDev)) < RC_SUCCESS)
		{
		  dbg(TRACE,"SendLayoutToDz %05d: SendIniPage failed with <%d>",__LINE__,ilRc);
		}
	    }
	  for(ilCurDevPag=0;ilCurDevPag< rgDV.prDevPag[ilCurDev].iNoOfPageInCarousel;ilCurDevPag++)
	    {/*for 2*/ 
	      /*just for good overview*/
	      /*ilCurDevPag <-> page in carousel*/
	      /*...piPagNo[...->reference to number in rgPG...*/
	      ilCurPag = rgDV.prDevPag[ilCurDev].piPagNo[ilCurDevPag];
	      if(debug_level==DEBUG)
		{ 
		  dbg(DEBUG,"SendLayoutToDZ ilCurPag is <%d> <%d>",ilCurPag,__LINE__) ;
		  dbg(DEBUG,"SendLayoutToDZ iNoOfPageInCarousel is <%d>",rgDV.prDevPag[ilCurDev].iNoOfPageInCarousel) ;
		  dbg(DEBUG,"SendLayoutToDZ Pagno is <%d>",rgDV.prDevPag[ilCurDev].piPagNo[ilCurDevPag]) ;
		  dbg(DEBUG,"SendLayoutToDZ PageObjectNumber is <%s>",rgDV.prDevPag[ilCurDev].pcPageObjectId[ilCurDevPag]) ;
		  dbg(DEBUG,"SendLayoutToDZ IP is <%s>",rgDV.prDevPag[ilCurDev].pcIP) ;
		}
	      for(ilCurObj= 0;ilCurObj<  rgPG.prPages[ilCurPag].prPagefile->iNCommands;ilCurObj++) 
		{ /*for 3*/
		  /*start the gtbuffer*/
		  /*set pointer to page infos*/
		  prlPageCommand = rgPG.prPages[ilCurPag].prPagefile->prCommands[ilCurObj];
		  memset(pclObjectNameBuf,0x00,XS_BUFF);
		  if(strstr(prlPageCommand->pcDZCommand,"gtPage")!=NULL)
		    {
		      strcpy(pclObjectNameBuf,rgDV.prDevPag[ilCurDev].pcPageObjectId[ilCurDevPag]);
		    }else{
		      strcpy(pclObjectNameBuf,prlPageCommand->pcObjectId);
		    }
		  DeleteCharacterInString(pclObjectNameBuf,cBLANK);          
		  /*continuing gtbuffer*/

		  for(nx=0; nx<prlPageCommand->iNFieldDesc; ++nx)
		    {
		      pf = prlPageCommand->prFieldDesc[nx];
		      if(!strcmp(STR0(pf->rFieldRef.pcTableName),"DEV"))
			{
			  memset(pclSendParam,0x00,S_BUFF);
			  memset(pclSendObj,0x00,XS_BUFF);
			  if(!strcmp(STR0(pf->rFieldRef.pcFieldName),"DFCO"))
			    {
			      strcpy(pclSendParam,rgDV.prDevPag[ilCurDev].pcFilterContents);
			      strcpy(pclSendObj,prlPageCommand->pcObjectId);
			      dbg(TRACE,"Send Layout Found Device Info <%s> -> <%s> Object Id <%s>",STR0(pf->rFieldRef.pcFieldName),pclSendParam,pclSendObj);
			    }
			  if(!strcmp(STR0(pf->rFieldRef.pcFieldName),"DFFD"))
			    {
			      strcpy(pclSendParam,rgDV.prDevPag[ilCurDev].pcFilterContents);
			      strcpy(pclSendObj,prlPageCommand->pcObjectId);
			      dbg(TRACE,"Send Layout Found Device Info <%s> -> <%s> Object Id <%s>",STR0(pf->rFieldRef.pcFieldName),pclSendParam,pclSendObj);
			    }
			}
		    }
		  memset(pclParameterBuf,0x00,XS_BUFF);
		  memset(pclFilerBuf,0x00,XS_BUFF);
		  GetdZParameter(pclParameterBuf,prlPageCommand,M_BUFF);
		  ilNoOfParam = 0;
		  ilNoOfParam = prlPageCommand->iNDZParms;
		  if(prlPageCommand->prDZFile->chars!=NULL)
		    strcpy(pclFilerBuf,prlPageCommand->prDZFile->chars);
		  memset(pcgResBuf,0x00,RES_BUF_SIZE);
		  ilRc= gtBuildCommand("1",prlPageCommand->pcDZCommand,pclObjectNameBuf,ilNoOfParam,pclParameterBuf,pclFilerBuf,rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
		  if(ilRc < RC_SUCCESS && igdebug_dzcommand==TRUE)
		    {
		      dbg(TRACE,"SendLayoutToDZ: gtBuildCommand failed with <%d>",ilRc);              
		      dbg(TRACE,"SendLayoutToDZ: gtBuildCommand resultbuf is<%s>",pcgResBuf);
		      dbg(TRACE,"SendLayoutToDZ: pcObjCmd=<%s>,pclObjectNameBuf=<%s>,ilNoOfParam=<%d>,pclParameterBuf=<%s>,pclFilerBuf=<%s>",prlPageCommand->pcDZCommand,pclObjectNameBuf,ilNoOfParam,pclParameterBuf,pclFilerBuf);
		    }/*end if 3*/
		  if(pclSendObj[0]!='\0')
		    {
		      if(prlPageCommand->iLanguage == TRUE)
			{
			  MapToArabic(pclSendParam,S_BUFF);
			}
		      ilRc= gtBuildCommand("1","gtTextPutFormatted",pclSendObj,1,pclSendParam,"",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
		      pclSendObj[0]='\0';
		    }
		}/*end for 3*/
	      memset(pclCarouselBuf,0x00,XS_BUFF);
	      strcpy(pclCarouselBuf,rgDV.prDevPag[ilCurDev].pcPageObjectId[ilCurDevPag]);
	      strcat(pclCarouselBuf,",");
	      strcat(pclCarouselBuf,rgPG.prPages[ilCurPag].pcCarouselTime);
	      if(igdebug_dzcommand==TRUE)
		{
		  dbg(TRACE,"SendLayoutToDZ%05d: Page <%s> No in Carousel=<%s>, CarouselTime=<%s> ",__LINE__,rgDV.prDevPag[ilCurDev].pcPageId[ilCurDevPag],pclCarouselBuf,rgPG.prPages[ilCurPag].pcCarouselTime); 
		}
	      if(rgPG.prPages[ilCurPag].pcCarouselTime[0]=='\0'||
		 !strcmp(rgPG.prPages[ilCurPag].pcCarouselTime,"0")||
		 !strcmp(rgDV.prDevPag[ilCurDev].pcDisplayType[ilCurDevPag],"C")||
		 !strcmp(rgDV.prDevPag[ilCurDev].pcDisplayType[ilCurDevPag],"L")||
		 !strcmp(rgDV.prDevPag[ilCurDev].pcDisplayType[ilCurDevPag],"G"))
		{
		  /*DO NOTHING*/
		}else{
		  gtBuildCommand("1","gtTerminalAddPageCarousel","gtSYSTEM_TERMINAL",2,pclCarouselBuf,"",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
		}
	      if(strcmp(rgDV.prDevPag[ilCurDev].pcDisplayType[ilCurDevPag],"A")==0 ||
		 strcmp(rgDV.prDevPag[ilCurDev].pcDisplayType[ilCurDevPag],"D")==0||
		 strcmp(rgDV.prDevPag[ilCurDev].pcDisplayType[ilCurDevPag],"H")==0||
		 strcmp(rgDV.prDevPag[ilCurDev].pcDisplayType[ilCurDevPag],"U")==0)
		{
		  rgDV.prDevPag[ilCurDev].iCounterState=OPEND;
		 /*   UpdateTable(ilCurDev,ilCurDevPag,ilCurPag,ilSocket);  */
		}    
	    }/*end for 2*/
#if 0
	  /*ONLY FOR DEVICE NOT FOR PAGE*/
	  if(strcmp(rgDV.prDevPag[ilCurDev].pcFilterField,pcgCHECKIN)==0)
	    {
	      /*Initial CounterState is CLOSED*/
	      rgDV.prDevPag[ilCurDev].iCounterState=CLOSED;
	      if((ilRc=StartCounter(ilCurDev))!= RC_SUCCESS)
		{
		  ilRc= RC_SUCCESS;
		}
	    }
	  if(strcmp(rgDV.prDevPag[ilCurDev].pcFilterField,pcgBELT)==0)
	    {
	      /*Initial CounterState is CLOSED*/
	      rgDV.prDevPag[ilCurDev].iCounterState=CLOSED;
	      if((ilRc=StartCounter(ilCurDev))!= RC_SUCCESS)
		{
		  ilRc= RC_SUCCESS;
		}
	    }
	  if(strcmp(rgDV.prDevPag[ilCurDev].pcFilterField,pcgGATE)==0)
	    {
	      /*Initial CounterState is CLOSED*/

	      rgDV.prDevPag[ilCurDev].iCounterState=CLOSED;
	      if((ilRc=StartCounter(ilCurDev))!= RC_SUCCESS)
		{
		  ilRc=RC_SUCCESS;
		}
	    }
#endif
	}/*end if only for connected sockets*/
    }else{
      dbg(TRACE,"NOT A VALID DEVICE");
    } 
  return ilRc; 
}
static int SendIniPage(int ipCurDev)
{
  int ilRc = RC_FAIL;
  int ilCurPag = 0;
  int ilNoOfParam = 0;
  int ilCurInit = 0;
  int ilCurObj = 0;
  int ilSocket =0;
  int ilCurDev = 0;
  int ildebug_level_save;
  UDBXCommand *prlPageCommand=NULL;
  char pclParameterBuf[XS_BUFF];
  char pclObjectNameBuf[XS_BUFF];
  char pclFilerBuf[XS_BUFF];
  char pclPageId[XS_BUFF];
  memset(pcgResBuf,0x00,RES_BUF_SIZE);
  ilCurDev=ipCurDev;
  /*only for connected sockets*/
  if(rgDV.prDevPag[ilCurDev].iSocket > 0)
    {
      memset(pclPageId,0x00,XS_BUFF);
      /*reference device -> pages*/
      ilCurPag = rgDV.prDevPag[ilCurDev].iConfigPage;
      for(ilCurObj= 0;ilCurObj<rgPG.prPages[ilCurPag].prPagefile->iNCommands ;ilCurObj++) 
	{ /*for 3*/
	  /*start the gtbuffer*/
	  prlPageCommand = rgPG.prPages[ilCurPag].prPagefile->prCommands[ilCurObj];
	  memset(pclObjectNameBuf,0x00,XS_BUFF);
	  strcpy(pclObjectNameBuf,prlPageCommand->pcObjectId);
	  DeleteCharacterInString(pclObjectNameBuf,cBLANK);          
	  /*continuing gtbuffer*/
	  memset(pclParameterBuf,0x00,XS_BUFF);
	  memset(pclFilerBuf,0x00,XS_BUFF);
	  memset(pcgResBuf,0x00,RES_BUF_SIZE);
	  GetdZParameter(pclParameterBuf,prlPageCommand,XS_BUFF);
	  ilNoOfParam = 0;
	  ilNoOfParam = prlPageCommand->iNDZParms;
	  if(prlPageCommand->prDZFile->chars!=NULL)
	  strcpy(pclFilerBuf,prlPageCommand->prDZFile->chars);
	  if(igdebug_switch==TRUE)
	    {
	      dbg(DEBUG,"SendIniPage %05d prlPageCommand->pcDZCommand = <%s>   ",__LINE__,prlPageCommand->pcDZCommand);
	      dbg(DEBUG,"SendIniPage %05d pclObjectNameBuf = <%s>",__LINE__,pclObjectNameBuf);
	      dbg(DEBUG,"SendIniPage %05d ilNoOfParam = %d   ",__LINE__,ilNoOfParam);
	      dbg(DEBUG,"SendIniPage %05d pclParameterBuf = <%s>   ",__LINE__,pclParameterBuf);
	      dbg(DEBUG,"SendIniPage %05d pclFilerBuf = <%s>   ",__LINE__,pclFilerBuf);
	      dbg(DEBUG,"SendIniPage %05d rgDV.prDevPag[ilCurDev].iSocket = %d   ",__LINE__,rgDV.prDevPag[ilCurDev].iSocket);
	    }
	  ilRc=gtBuildCommand("1",prlPageCommand->pcDZCommand,pclObjectNameBuf,ilNoOfParam,pclParameterBuf,pclFilerBuf,rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
	  if(ilRc < RC_SUCCESS&&igdebug_dzcommand==TRUE)
	    { 
	      dbg(DEBUG,"gtBuildCommand failed with <%d>",ilRc);
	      dbg(DEBUG,"gtBuildCommand resultbuf is<%s>",pcgResBuf);
	    } 
	}/*end for 3*/
    }/*end if only for connected sockets*/
  return ilRc;
}
/**********************************************************/
/*This funktion built the timeframe for a select statement*/
/*Input Frame in minutes , begin/end (0/1) option         */
/*Output TimeFrame in YYYYMMDDHHMM                        */
/**********************************************************/
static int TimeFrame(char *pcpTimeFrame,int ipOffset,int ipOption)
{
  int ilRc = RC_FAIL;
  time_t    now;
  time_t    Frame;
  struct tm *_tm;
  char pclTime[13];
  now = time(NULL);
  if(ipOffset==0)
    {
      if (ipOption == 1)
	{
	  Frame = now+(atoi(pcpTimeFrame)*60);
	  ilRc = RC_SUCCESS;
	}
      if (ipOption == 0)
	{
	  Frame = now-(atoi(pcpTimeFrame)*60);
	  ilRc = RC_SUCCESS; 
	}
    }else{
      Frame=now+ipOffset;
      ilRc = RC_SUCCESS;
    }
  if(ilRc == RC_SUCCESS)
    {
      memset(pcpTimeFrame,0x00,15);
      _tm = (struct tm *)gmtime(&Frame);
      
      sprintf(pcpTimeFrame,"%4d%02d%02d%02d%02d",_tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
	      _tm->tm_min);
    }
  return ilRc;
}
static int UtcToLocal(char* pcpTime,char* pcpSeparator,char* pcpFormat)
{
  struct tm *_tm;
  time_t    now;
  char      _tmpc[6]; 
  int hour_gm,hour_local;
  int UtcDifference;
 
  now = time(NULL);
  _tm = (struct tm *)gmtime(&now);
  hour_gm = _tm->tm_hour;
  _tm = (struct tm *)localtime(&now);
  hour_local = _tm->tm_hour;
  if(hour_gm > hour_local)
    {
      UtcDifference = (hour_local+24-hour_gm);
    }else{
      UtcDifference = (hour_local-hour_gm);
    }
  igUTCDIFF=UtcDifference*60;
  if(pcpTime==NULL )
    {
      dbg(TRACE,"pcpTime = NULL");
      return (time_t) 1;
    } /* end if */
  if((int)strlen(pcpTime) == 0 )
    {
      return RC_NOTFOUND;
    } /* end if */
  DeleteCharacterInString(pcpTime,cBLANK); 
  if(strlen(pcpTime) < 12 )
    {
      strcpy(pcpTime," ");
      return RC_SUCCESS;
    } /* end if */
  now = time(0L);
  _tm = (struct tm *)localtime(&now);
  _tmpc[2] = '\0';
  strncpy(_tmpc,pcpTime+12,2);
  _tm -> tm_sec = atoi(_tmpc);
  strncpy(_tmpc,pcpTime+10,2);
  _tm -> tm_min = atoi(_tmpc);
  strncpy(_tmpc,pcpTime+8,2);
  _tm -> tm_hour = atoi(_tmpc);
  strncpy(_tmpc,pcpTime+6,2);
  _tm -> tm_mday = atoi(_tmpc);
  strncpy(_tmpc,pcpTime+4,2);
  _tm -> tm_mon = atoi(_tmpc)-1;
  strncpy(_tmpc,pcpTime,4);
  _tmpc[4] = '\0';
  _tm -> tm_year = atoi(_tmpc)-1900;
  _tm -> tm_wday = 0;
  _tm -> tm_yday = 0;
  /*daylight saving time flag must be 0 for SCO and Solaris*/
  _tm -> tm_isdst=0; 
  now = mktime(_tm)-timezone;
  _tm = (struct tm *)localtime(&now);

  if(strcmp(pcpFormat,"NH:mm")==0)
    {
      *pcpTime='\0';
      sprintf(pcpTime,"{NL}%02d:%02d",_tm->tm_hour,_tm->tm_min);
     return RC_SUCCESS;
    }
  if(strcmp(pcpFormat,"HH:mm")==0)
    {
      sprintf(pcpTime,"%02d:%02d",_tm->tm_hour,_tm->tm_min);
      return RC_SUCCESS;
    }
  if(strcmp(pcpFormat,"HHmm")==0)
    {
      *pcpTime='\0';
      sprintf(pcpTime,"%02d%02d",_tm->tm_hour,_tm->tm_min);
      return RC_SUCCESS;
    }
  if(strcmp(pcpFormat,"HH:MM")==0)
    {
      *pcpTime='\0';
      sprintf(pcpTime,"%02d:%02d",_tm->tm_hour,_tm->tm_min);
      return RC_SUCCESS;
    }
  memset(pcpTime,0x00,strlen(pcpTime));
  sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d", _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
      _tm->tm_min,_tm->tm_sec);
  return RC_SUCCESS;
}
/****************************************************************************************/ 
/****************************************************************************************/
/**Next flight functionality for athens and others                                     **/
/****************************************************************************************/
/****************************************************************************************/
static int UpdateLocation(int ipCurDev,int ipCurDevPag,int ipCurPag,int ipSocket)
{
  int ilRc = RC_SUCCESS;
  int ilCurObj =0;
  int ilCurDev = 0;
  int ilCurDevPag=0;
   int ilCurPag = 0;
  int ilCurRec=0;
  int ilSocket =0;
  int ilNextItem = 0;
  int ilCurFlights =0;
  int ilNoOfFlights=0;
  int ilNoOfObjGroups=0;
  int ilItemNo=0;
  int ilObjCnt=0;
  int ilNoOfParam = 0;
  int  ilCurFld=0;
  char pclCarouselBuf[XS_BUFF];
  char pclObjectId[4];
  char pclPageId[4];
  char pclObjNumber[4];
  char pclTextPut[M_BUFF];
  char pclParameterBuf[XS_BUFF];
  char pclResult[M_BUFF];
  char pclCommand[M_BUFF];


  UDBXCommand *prlPageCommand=NULL;
  FieldDescription *prlFD;
  FieldDisplayType *prlDspType;
  FieldList *prlRecord;
  char pclFileBuf[XS_BUFF];

  /*init index*/
  ilCurPag = ipCurPag;
  ilCurDevPag = ipCurDevPag;
  ilCurDev = ipCurDev;
  ilSocket = ipSocket;
 

  memset(pclPageId,0x00,4);         
  memset(pclObjectId,0x00,4);
  memset(pclResult,0x00,M_BUFF);
  memset(pclParameterBuf,0x00,XS_BUFF);


  if((ilRc=SelectLayout(ilCurDev,ilCurPag,&ilCurFlights))!=RC_SUCCESS)
    {
      if(ilRc==RC_NOTFOUND) 
	{ 
	  return RC_NOTFOUND; 
	}
      return RC_SUCCESS;
    }
  /*number of displayed flights*/
  ilNoOfFlights = atoi(rgPG.prPages[ilCurPag].pcNumberOfFlights);
  /*number of selected flights*/
  /*buffer for Page Id*/
  strcpy(pclPageId,rgDV.prDevPag[ilCurDev].pcPageObjectId[ilCurDevPag]);
  if(igshow_event==TRUE)
    dbg(TRACE,"UpdateLocation: %s %s|CurDev:%d->IP:%s|CurDevPag:%d|CurPag:%d|Socket:%d|NoOfFlights:%d,CurFlights:%d",
	rgDV.prDevPag[ipCurDev].pcFilterField,
	rgDV.prDevPag[ipCurDev].pcFilterContents,
	ipCurDev,
	rgDV.prDevPag[ipCurDev].pcIP,
	ipCurDevPag,
	ipCurPag,
	ipSocket,
	ilNoOfFlights,
	ilCurFlights);
  /*   dbg(TRACE,"<%s>",rgPG.prPages[ilCurPag].pcTableObjNoList); */
  /*init page*/
  memset(pcgResBuf,0x00,RES_BUF_SIZE);
  gtBuildCommand("1","gtPageSetCurrent",pclPageId,1,"-1","",ilSocket,pcgResBuf); 
  memset(pcgResBuf,0x00,RES_BUF_SIZE);
  gtBuildCommand("1","gtCollectionFreezeDrawing",pclPageId,0,"","",ilSocket,pcgResBuf); 
  /*init counters*/
  ilItemNo=0;
  for(ilCurFld = 0;ilCurFld < ilNoOfFlights;ilCurFld++)
    {/*for rows*/
      if(ilCurFld< ilCurFlights)
	{/*only when data*/
	  /*get our records*/
	  if(igshow_event)
	    dbg(TRACE,"======================= %d. Flight ===========================",ilCurFld+1);
	  prlRecord=&rgDV.prDevPag[ilCurDev].prRecord[ilCurFld];
    	  ilObjCnt=0;
	  do{
	    memset(pclObjNumber,0x00,4);
	    ilNextItem = GetDataItem(pclObjNumber,rgPG.prPages[ilCurPag].pcTableObjNoList,ilItemNo+1,',',"","");
	    ilCurObj = atoi(pclObjNumber);
	    memset(pclObjectId,0x00,4);
	    prlPageCommand = rgPG.prPages[ilCurPag].prPagefile->prCommands[ilCurObj];
	    strcpy(pclObjectId,prlPageCommand->pcObjectId);
	    if(strcmp(prlPageCommand->pcDZCommand,"gtGIFDefine")==0)
	      {/*if "gtGIFDefine"*/
		memset(pclParameterBuf,0x00,XS_BUFF);
		memset(pclTextPut,0x00,M_BUFF);
		memset(pclFileBuf,0x00,XS_BUFF);
		memset(pclResult,0x00,M_BUFF);
		memset(pclCommand,0x00,M_BUFF);
		ilRc = SetPageData(ilCurPag,ilCurObj,prlRecord,pclCommand,pclTextPut,pclFileBuf,0);
		strcpy(pclParameterBuf,"0,0,-1,");
		strcat(pclParameterBuf,pclTextPut);
		memset(pcgResBuf,0x00,RES_BUF_SIZE);
		if(igdebug_dzcommand)
		  dbg(TRACE,"UpdateLocation START SENDING FILE");
		gtBuildCommand("1","gtFilerPutFile","gtSYSTEM_FILER",4,pclParameterBuf,pclFileBuf,ilSocket,pcgResBuf);
		memset(pclParameterBuf,0x00,XS_BUFF);
		strcpy(pclParameterBuf,"-1,");
		strcat(pclParameterBuf,pclTextPut); 
		memset(pcgResBuf,0x00,RES_BUF_SIZE);
		gtBuildCommand("1","gtFilerPutData","gtSYSTEM_FILER",2,pclParameterBuf,pclFileBuf,ilSocket,pcgResBuf);
		memset(pclFileBuf,0x00,XS_BUFF);
		GetdZParameter(pclParameterBuf,prlPageCommand,XS_BUFF);
		ilNoOfParam = 0;
		memset(pclResult,0x00,M_BUFF);
		ilNoOfParam =prlPageCommand->iNDZParms;
		if(prlPageCommand->prDZFile->chars!=NULL) 
		  strcpy(pclFileBuf,prlPageCommand->prDZFile->chars);
		memset(pcgResBuf,0x00,RES_BUF_SIZE);
	        gtBuildCommand("1",prlPageCommand->pcDZCommand,pclObjectId,ilNoOfParam,pclParameterBuf,pclFileBuf,rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
		memset(pcgResBuf,0x00,RES_BUF_SIZE);
		gtBuildCommand("1",pclCommand,pclObjectId,1,pclTextPut,pclFileBuf,ilSocket,pcgResBuf);
		if(igdebug_dzcommand)
		  dbg(TRACE,"UpdateLocation END SENDING FILE");
	      }else{
		memset(pclTextPut,0x00,M_BUFF);
		memset(pclFileBuf,0x00,XS_BUFF);
		memset(pclResult,0x00,M_BUFF);
		memset(pclCommand,0x00,M_BUFF);
		ilRc = SetPageData(ilCurPag,ilCurObj,prlRecord,pclCommand,pclTextPut,pclFileBuf,0);
		memset(pcgResBuf,0x00,RES_BUF_SIZE);
		gtBuildCommand("1",pclCommand,pclObjectId,1,pclTextPut,pclFileBuf,ilSocket,pcgResBuf);
	      }
	    ilObjCnt++;
	    ilItemNo++;
	    /*test next item*/  
	    ilNextItem = GetDataItem(pclObjNumber,rgPG.prPages[ilCurPag].pcTableObjNoList,ilItemNo+1,',',"","");
	  }while(ilObjCnt < rgPG.prPages[ilCurPag].iNoOfObjPerRec&&ilNextItem > 0);
	  /*end if data*/
	}else{
	  /*if no  data*/
	  ilObjCnt=0;
	  do{
	    memset(pclObjNumber,0x00,4);
	    ilNextItem = GetDataItem(pclObjNumber,rgPG.prPages[ilCurPag].pcTableObjNoList,ilItemNo+1,',',"","");
	    ilCurObj = atoi(pclObjNumber);
	    memset(pclObjectId,0x00,4);
	    prlPageCommand = rgPG.prPages[ilCurPag].prPagefile->prCommands[ilCurObj];
	    strcpy(pclObjectId,prlPageCommand->pcObjectId);
	    /*  	    dbg(TRACE,"UpdateLocation %05d ilNextItem <%d> ",__LINE__,ilNextItem); */
	    if(ilNextItem >= 0)
	      {
		/*  	        dbg(TRACE,"UpdateLocation %05d pclObjNumber <%s> ",__LINE__,pclObjNumber);  */
		if(strcmp(prlPageCommand->pcDZCommand,"gtGIFDefine")==0)
		  {/*if "gtGIFDefine"*/
		    memset(pclFileBuf,0x00,XS_BUFF);
		    memset(pclResult,0x00,M_BUFF);
		    /*undefine gif*/
		    memset(pcgResBuf,0x00,RES_BUF_SIZE);
		    gtBuildCommand("1","gtGIFUndefine",pclObjectId,0,"",pclFileBuf,ilSocket,pcgResBuf);
		  }else{
		    /*Text-> ' '*/
		    memset(pcgResBuf,0x00,RES_BUF_SIZE);
		    gtBuildCommand("1","gtTextPut",pclObjectId,1," ",pclFileBuf,ilSocket,pcgResBuf);
		  }
	      }
	    ilObjCnt++;
	    ilItemNo++;  
	    /*test next item*/  
	    ilNextItem = GetDataItem(pclObjNumber,rgPG.prPages[ilCurPag].pcTableObjNoList,ilItemNo+1,',',"","");
	  }while(ilObjCnt < rgPG.prPages[ilCurPag].iNoOfObjPerRec&&ilNextItem > 0); 
	} 
    }/*end for rows*/
  if(rgDV.prDevPag[ilCurDev].iAlarmFlag==TRUE||rgDV.prDevPag[ilCurDev].iDefaultFlag==TRUE)
    {
      memset(pcgResBuf,0x00,RES_BUF_SIZE);
      gtBuildCommand("1","gtCollectionRefresh",pclPageId,1,"-1","",ilSocket,pcgResBuf);
    }else{  
      memset(pcgResBuf,0x00,RES_BUF_SIZE);
      gtBuildCommand("1","gtCollectionRedraw",pclPageId,0,"","",ilSocket,pcgResBuf);
    }  
  if(rgPG.prPages[ilCurPag].pcCarouselTime[0]!='\0'&&atoi(rgPG.prPages[ilCurPag].pcCarouselTime)>0&&rgDV.prDevPag[ilCurDev].iCounterState==CLOSED)
    {
      memset(pclCarouselBuf,0x00,XS_BUFF);
      strcpy(pclCarouselBuf,rgDV.prDevPag[ilCurDev].pcPageObjectId[ilCurDevPag]);
      strcat(pclCarouselBuf,",");
      strcat(pclCarouselBuf,rgPG.prPages[ilCurPag].pcCarouselTime);
      gtBuildCommand("1","gtTerminalAddPageCarousel","gtSYSTEM_TERMINAL",2,pclCarouselBuf,"",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
      gtBuildCommand("1","gtPageSetCurrent",pclPageId,1,"-1","",ilSocket,pcgResBuf);
     
      SetDefaultPage(FALSE,ilCurDev,TRUE);
    }
  rgDV.prDevPag[ilCurDev].iCounterState=OPEND;
  return RC_SUCCESS;
}
/******************************************************************************************/
/*this function is a pagefile interpreter only for tables                                 */
/******************************************************************************************/

static int SetPageData(int ipCurPag,int ipCurObj,FieldList *prRecord,char* pcpCommand, char* pcpResult,char* pcpFileBuf,int ipItem)
{
  int ilRc = RC_NOTFOUND;
  int ilFieldCnt = 0;
  int ilItemNo = 0;
  int ilRow = 0;
  int ilCurTab = 0;
  short slCursor = 0;
  short slFkt = 0;
  int  ilFirstbyte = 0;
  int  ilLastbyte = 0;
  FILE *fp=NULL;
  UDBXCommand *prlPageCommand=NULL;
  FieldDescription *prlFD;
  FieldDisplayType *prlDspType;
  FieldReference *prFieldRef;
  char *pclDspType=NULL;
  char pclObjectId[4];
  char pclFieldBuf[XS_BUFF];
  char pclBuildSelectBuf[M_BUFF];
  char pclTmpSqlAnswer[M_BUFF];
  char pclSqlBuf[M_BUFF];
  char pclTab[7];
  char pclTmpBuf[XS_BUFF];
  char pclTmpBuf2[XS_BUFF];
  char pclTmpFirstField[XS_BUFF];
  char pclTmpVIA[XS_BUFF];
  char pclTmpVIASelect[XS_BUFF];
  char pclTmpVIAResult[XS_BUFF];
  char pclText1[XS_BUFF];
  char pclText2[XS_BUFF];
  char pclText3[XS_BUFF];
  char pclText4[XS_BUFF];
  char pclBlink[XS_BUFF];
  char *pclTimePtr=NULL;
  char pclTimeBuf[XS_BUFF];
  char *pclPtr1;
  memset(pclText1,0x00,XS_BUFF);
  memset(pclText2,0x00,XS_BUFF);
  memset(pclText3,0x00,XS_BUFF);
  memset(pclText4,0x00,XS_BUFF);
  memset(pclBlink,0x00,XS_BUFF);
  memset(pclTmpFirstField,0x00,XS_BUFF);
  memset(pclSqlBuf,0x00,M_BUFF);
  memset(pclBuildSelectBuf,0x00,M_BUFF);
  memset(pclTmpSqlAnswer,0x00,M_BUFF);
  memset(pclObjectId,0x00,4);
  memset(pclTab,0x00,7);
  memset(pclFieldBuf,0x00,XS_BUFF);
  memset(pclTmpVIA,0x00,XS_BUFF);
  memset(pclTmpVIAResult,0x00,XS_BUFF);
  memset(pclTmpVIASelect,0x00,XS_BUFF);
  memset(pclTmpBuf,0x00,XS_BUFF);
  memset(pclTmpBuf2,0x00,XS_BUFF);
  memset(pclTimeBuf,0x00,XS_BUFF);
  pclDspType=NULL;
  /*set pointer to page infos*/
  prlPageCommand = rgPG.prPages[ipCurPag].prPagefile->prCommands[ipCurObj];
  prFieldRef = &(prlPageCommand->prFieldDesc[ipItem]->rFieldRef);
  prlDspType = &(prlPageCommand->prFieldDesc[0]->rDpyType);
  pclDspType=FieldDisplayType_GetByType(prlDspType->rAny.iType);
  prlFD = (prlPageCommand->prFieldDesc[ipItem]);
  if(igdebug_dzcommand)
    {
      dbg(DEBUG,"SetPageData  rgPG.prPages[ipCurPag].pcDisplayType is<%s>",rgPG.prPages[ipCurPag].pcDisplayType);
      dbg(DEBUG,"SetPageData  field  dspType is<%s>",pclDspType);
    }
  if(strcmp(pclDspType,"R")==0)
    {
      memset(pclText1,0x00,XS_BUFF);   
      memset(pclText2,0x00,XS_BUFF);
      memset(pclText4,0x00,XS_BUFF);   
      memset(pcpResult,0x00,M_BUFF);   
      memset(pclFieldBuf,0x00,XS_BUFF);
      ilFirstbyte = prFieldRef->iStart;
      ilLastbyte = prFieldRef->iEnd;
      memset(pclText1,0x00,XS_BUFF);
      strcpy(pclTmpBuf,prFieldRef->pcFieldName);
      ilItemNo = get_item_no(prRecord->pcFields,pclTmpBuf,5);
      ilItemNo++;
      GetDataItem(pclText1,prRecord->pcRecordset,ilItemNo,',',"","\0 ");
      if(ilLastbyte>=ilFirstbyte && ilLastbyte>0&&pclText1[0]!='\0')
        {
          /*first cut the tail...*/
          pclText1[ilLastbyte] = '\0';
          /*...then the top*/
          strcpy(pcpResult,&pclText1[ilFirstbyte-1]);
	  strcpy(pclText4,pcpResult);
        }
#ifdef _TLL
      DeleteLeadingZero(pcpResult);
#endif
      memset(pclTmpBuf,0x00,XS_BUFF);
      ilFirstbyte = prlDspType->rR.rFieldRef.iStart;
      ilLastbyte = prlDspType->rR.rFieldRef.iEnd;
      memset(pclText2,0x00,XS_BUFF);
      strcpy(pclTmpBuf,prlDspType->rR.rFieldRef.pcFieldName);
      ilItemNo = get_item_no(prRecord->pcFields,pclTmpBuf,5);
      ilItemNo++;
      GetDataItem(pclText2,prRecord->pcRecordset,ilItemNo,',',"","\0 ");
      if(pclText2[0]!='\0'&&ilLastbyte>=ilFirstbyte)
	{
	 
          /*first cut the tail...*/
          pclText2[ilLastbyte] = '\0';
          /*...then the top*/
	  strcpy(pclText3,&pclText2[ilFirstbyte-1]);
#ifdef _TLL
	  DeleteLeadingZero(pclText3);
#endif	  
	  if(strcmp(pclText4,pclText3)!=0||(pclText1[0]=='\0'&&pclText3[0]!='\0'))
	     {
	       if(pcpResult[0]!= '\0')
		 {
		   strcat(pcpResult,prlDspType->rR.pcSeparator);
		 }
	       strcat(pcpResult,pclText3);
	     }
	}
      strcpy(pcpCommand,"gtTextPutFormatted");
      if(prlPageCommand->iLanguage==1)
	{
	  MapToArabic(pcpResult,M_BUFF);
	}
      if(igdebug_dzcommand) 
        dbg(TRACE,"SetPageData: <%s>  Command <%s>",pcpResult,pcpCommand);
      return RC_SUCCESS;
    }/*endif "R"*/
  /*******************************************************************************/
  /* fields from afttab*/
  /********************************************************/
  /**MAP DATA FROM AFTTAB TO DATA FROM AIRPORTTABLE (APT)**/
  /********************************************************/
  if(strcmp(pclDspType,"A")==0)
    {
      memset(pclTmpBuf,0x00,XS_BUFF);
      memset(pclFieldBuf,0x00,XS_BUFF);
      for(ilFieldCnt=0;ilFieldCnt <prlDspType->rA.iNFieldNames ;ilFieldCnt++)
	{
	  if(ilFieldCnt==0) 
	    {
	      strcpy(pclFieldBuf,prlDspType->rA.p2cFieldNames[ilFieldCnt]);
	    }else{
	      strcat(pclFieldBuf,",");
	      strcat(pclFieldBuf,prlDspType->rA.p2cFieldNames[ilFieldCnt]);
	    }
	}
      strcpy(pclTmpBuf,prFieldRef->pcFieldName);
      ilItemNo = get_item_no(prRecord->pcFields,pclTmpBuf,5);
      ilItemNo++;  
      if(igdebug_dzcommand)
	dbg(DEBUG,"SetPageData ITEM <%s> NUMBER IS <%d>",pclTmpBuf,ilItemNo);
      memset(pclTmpFirstField,0x00,XS_BUFF);
      GetDataItem(pclTmpFirstField,prRecord->pcRecordset,ilItemNo,',',"","");
      DeleteCharacterInString(pclTmpBuf,cBLANK);
      DeleteCharacterInString(pclTmpFirstField,cBLANK);
      if(strlen(pclTmpFirstField)>1)
	{
	  sprintf(pclSqlBuf,"SELECT %s FROM APT%s WHERE APC3='%s'",pclFieldBuf,pcgTabEnd,pclTmpFirstField);
	  slFkt = START; 
	  slCursor = 0;
	  if((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))!=RC_SUCCESS)
	    {
	      if(ilRc == RC_FAIL )
		{ 
		  dbg(TRACE,"<InitDatabaseFieldInfos> sql_if in Line <%d> failed with <%d>",__LINE__,ilRc);
		}  
	    }
	  close_my_cursor(&slCursor);
	  GetDataItem(pclText1,pclTmpSqlAnswer,1,'\0',"","\0 ");
	  if(prlDspType->rA.iNFieldNames==2)
	    {
	      GetDataItem(pclText2,pclTmpSqlAnswer,2,'\0',"","\0 ");
	    }
	  if(igdebug_dzcommand)
	    {
	      dbg(DEBUG,"SetPageData  pclText1 for code = <%s>",pclText1);    
	      dbg(DEBUG,"SetPageData  pclText2 for code = <%s>",pclText2);    
	    }
	}else{
	  strcpy(pclText1,pclTmpFirstField);
	  if(prlDspType->rA.iNFieldNames==2)
	    {
	      strcpy(pclText2,pclTmpFirstField);
	    }
	}
      if(prlPageCommand->iLanguage==1)
	{
	  if(pclText1[0]!='\0')
	    MapToArabic(pclText1,XS_BUFF);
	  if(pclText2[0]!='\0')
	    MapToArabic(pclText2,XS_BUFF);
	}
      /*VIA3->7*/
      pclTmpVIA[0]='\0';
      ilItemNo = get_item_no(prRecord->pcFields,"VIA3",5);
      ilItemNo++;  
      GetDataItem(pclTmpVIA,prRecord->pcRecordset,ilItemNo,',',"","");
      DeleteCharacterInString(pclTmpVIA,cBLANK);
      if(strlen(pclTmpVIA)>2&&prlDspType->rA.iNoOfAirports==2)
	{
	  slFkt = START; 
	  slCursor = 0;
	  sprintf(pclTmpVIASelect,"SELECT %s FROM APT%s WHERE APC3='%s'",pclFieldBuf,pcgTabEnd,pclTmpVIA);
	  if((ilRc = sql_if(slFkt,&slCursor,pclTmpVIASelect,pclTmpVIAResult))!=RC_SUCCESS)
	    {
	      dbg(TRACE,"<InitDatabaseFieldInfos> sql_if in Line <%d> failed with <%d>",__LINE__,ilRc);
	    }
	  close_my_cursor(&slCursor);
	  GetDataItem(pclText3,pclTmpVIAResult,1,'\0',"","\0 ");
	  if(prlDspType->rA.iNFieldNames==2)
	    {
	      GetDataItem(pclText4,pclTmpVIAResult,2,'\0',"","\0 ");
	    }
	  if(!strcmp(prlDspType->rA.pcSeparator,"0"))
	    {
	      memset(pclText3,0x00,XS_BUFF);
	      memset(pclText4,0x00,XS_BUFF);
	    }
	  if(prlPageCommand->iLanguage==1)
	    {
	      if(pclText3[0]!='\0')
		MapToArabic(pclText3,XS_BUFF);
	      if(pclText4[0]!='\0')
		MapToArabic(pclText2,XS_BUFF);
	    }
	  /****************************************************************************/
	  if(igdebug_dzcommand)
	    {
	      dbg(DEBUG,"SetPageData  VIA pclText3 for code = <%s>",pclText3);    
	      dbg(DEBUG,"SetPageData  VIA pclText4 for code = <%s>",pclText4);
	    }    
	  /****************************************************************************/
	  if((int)strlen(pclText1)+(int)strlen(pclText3)+(int)1 <= (int)prlFD->iSize)
	    {
	      if(pclText1[0]!='\0')
		{
		  /*separator for english via*/
		  if(pclText3[0]!='\0')
		    {
		      strcat(pclText1,prlDspType->rA.pcSeparator);
		    }
		}
	    }else{
	      if(pclText3[0]!='\0')
		{ 
		  strcat(pclText1,"{ITEM}");
		}
	    }
	  if((int)strlen(pclText2)+(int)strlen(pclText4)+(int)1 <= (int)prlFD->iSize)
	    { 
	      if(pclText2[0]!='\0')
		{
		  /*separator for domestic via*/
		  if(pclText4[0]!='\0') 
		    {
		      strcat(pclText2,prlDspType->rA.pcSeparator);
		    }
		}
	    }else{
	      if(pclText4[0]!='\0')
		{
		  strcat(pclText2,"{ITEM}"); 
		}
	    }
	}else{
	  if(prlDspType->rA.iNoOfAirports==2)
	    {
	      strcpy(pclText3,pclTmpVIA);
	      strcpy(pclText4,pclTmpVIA);
	    }
	}
      if((int)strlen(pclText1)+(int)strlen(pclText2)+(int)strlen(pclText3)+(int)strlen(pclText4)-(int)10 <= 2 * prlFD->iSize)
	{
	  strcat(pclText1,pclText3);
	  strcat(pclText2,pclText4);
	  memset(pcpResult,0x00,M_BUFF);
	  strcpy(pcpResult,pclText1);
	  if(prlDspType->rA.iNFieldNames>1)
	    { 
	      if(pclText2[0]!='\0')
		{
		  strcat(pcpResult,"{ITEM}");
		  strcat(pcpResult,pclText2);
		}
	    }
	}else{
	  if(strstr(pclText1,"{ITEM}")!=NULL)
	  {
	    strcat(pclText1,pclText2);
	  }else{
	    strcat(pclText1,"{ITEM}");
	    strcat(pclText1,pclText2);
	  }
	  if(strstr(pclText3,"{ITEM}")!=NULL)
	    {
	      strcat(pclText3,pclText4);
	    }else{
	      if(pclText4[0]!='\0')
		{
		  strcat(pclText3,"{ITEM}");
		  strcat(pclText3,pclText4);
		}
	    }
	  memset(pcpResult,0x00,M_BUFF);
	  strcpy(pcpResult,pclText1);
	  strcat(pcpResult,pclText3);
	}
      strcpy(pcpCommand,"gtTextPutFormatted");
      if(igdebug_dzcommand) 
	dbg(TRACE,"SetPageData: <%s>  Command <%s>",pcpResult,pcpCommand); 
      return RC_SUCCESS;
    }/*endif "A"*/

  /*START TYPE N */
  /********************************************************/
  /**NORMAL TEXT WITH FIRST AND LAST BYTE******************/
  /********************************************************/
  if(strcmp(pclDspType,"N")==0)
    {
      if(!strcmp(prFieldRef->pcFieldName,"TEXT")&& !strcmp(prFieldRef->pcTableName,"TXT"))
	{
	  memset(pcpCommand,0x00,M_BUFF);
	  strcpy(pcpCommand,"gtTextPutFormatted");
	  strcpy(pcpResult,pcgFileText);
	  if(igdebug_dzcommand)
	    dbg(TRACE,"SetPageData: Returns pcpResult <%s> pcp Command <%s>",pcpResult,pcpCommand);
	  return RC_SUCCESS;
	}
      memset(pclTmpBuf,0x00,XS_BUFF);
      strcpy(pclTmpBuf,prFieldRef->pcFieldName);
      ilItemNo = get_item_no(prRecord->pcFields,pclTmpBuf,5);
      ilItemNo++;  
      if(igdebug_dzcommand)
	dbg(DEBUG,"SetPageData ITEM <%s> NUMBER IS <%d>",pclTmpBuf,ilItemNo);
      memset(pclTmpFirstField,0x00,XS_BUFF);
      GetDataItem(pclTmpFirstField,prRecord->pcRecordset,ilItemNo,',',"","");
      ilFirstbyte = prFieldRef->iStart;
      ilLastbyte = prFieldRef->iEnd;
      if(ilLastbyte>=ilFirstbyte && ilLastbyte>0)
	{
	  /*first cut the tail...*/
	  pclTmpFirstField[ilLastbyte] = '\0';
	  /*...then the top*/
	  strcpy(pcpResult,&pclTmpFirstField[ilFirstbyte-1]);
	}else{
	  strcpy(pcpResult,pclTmpFirstField);
	}
      if(prlPageCommand->iLanguage==1)
	{
	  MapToArabicN(pcpResult,M_BUFF);
	}
      memset(pcpCommand,0x00,M_BUFF);
      strcpy(pcpCommand,"gtTextPutFormatted");
      if(igdebug_dzcommand)
	dbg(TRACE,"SetPageData: <%s>  Command <%s>",pcpResult,pcpCommand);
      return RC_SUCCESS;
    }/*endif "N"*/
  /********************************************************/
  /**NORMAL TEXT WITH FIRST AND LAST BYTE WITHOUT BLANK /M*/
  /********************************************************/
  if(strcmp(pclDspType,"M")==0)
    { 
      memset(pclTmpBuf,0x00,XS_BUFF);
      strcpy(pclTmpBuf,prFieldRef->pcFieldName);
      ilItemNo = get_item_no(prRecord->pcFields,pclTmpBuf,5);
      ilItemNo++;  
      if(igdebug_dzcommand)
	dbg(DEBUG,"SetPageData ITEM <%s>  NUMBER IS <%d>",pclTmpBuf,ilItemNo);
      memset(pclTmpFirstField,0x00,XS_BUFF);
      GetDataItem(pclTmpFirstField,prRecord->pcRecordset,ilItemNo,',',"","");
      ilFirstbyte = prFieldRef->iStart;
      ilLastbyte =  prFieldRef->iEnd;
      if(ilLastbyte>=ilFirstbyte && ilLastbyte>0)
	{
	  /*first cut the tail...*/
	  pclTmpFirstField[ilLastbyte] = '\0';
	  /*...then the top*/
	  strcpy(pcpResult,&pclTmpFirstField[ilFirstbyte-1]);
	}else{
	  strcpy(pcpResult,pclTmpFirstField);
	}
      if(prlPageCommand->iLanguage==1)
	{
	  MapToArabicN(pcpResult,M_BUFF);
	}
      memset(pcpCommand,0x00,M_BUFF);
      strcpy(pcpCommand,"gtTextPutFormatted");
      DeleteCharacterInString(pcpResult,cBLANK);
      if(igdebug_dzcommand)
	dbg(TRACE,"SetPageData: <%s>  Command <%s>",pcpResult,pcpCommand);
      return RC_SUCCESS;
    }/*endif "M"*/
  /*show codeshares only flightnumbers*/
  if(strcmp(pclDspType,"J")==0)
    {
      memset(pclTmpBuf,0x00,XS_BUFF);
      strcpy(pclTmpBuf,prFieldRef->pcFieldName);
      ilItemNo = get_item_no(prRecord->pcFields,pclTmpBuf,5);
      ilItemNo++;  
      if(igdebug_dzcommand)
	dbg(DEBUG,"SetPageData ITEM <%s> NUMBER IS <%d>",pclTmpBuf,ilItemNo);
      memset(pclTmpFirstField,0x00,XS_BUFF);
      GetDataItem(pclTmpFirstField,prRecord->pcRecordset,ilItemNo,',',"","");
      ilItemNo=get_item_no(prRecord->pcFields,"JFNO",5);
      ilItemNo++;
      GetDataItem(pclText1,prRecord->pcRecordset,ilItemNo,',',"","");
      ilItemNo=0;
      ilItemNo=get_item_no(prRecord->pcFields,"JCNT",5);
      ilItemNo++;
      GetDataItem(pclText2,prRecord->pcRecordset,ilItemNo,',',"","");
      ilItemNo=0;
      ilFirstbyte = prFieldRef->iStart;
      ilLastbyte =  prFieldRef->iEnd;
      if(ilLastbyte>=ilFirstbyte && ilLastbyte>0)
        {
	  /*first cut the tail...*/
	  pclTmpFirstField[ilLastbyte] = '\0';
	  /*...then the top*/
	  strcpy(pcpResult,&pclTmpFirstField[ilFirstbyte-1]);
        }else{
	  strcpy(pcpResult,pclTmpFirstField);
        }
      if(pclText1[0]!='\0')
        {   pclPtr1=pclText1;
                
	ilFieldCnt=atoi(pclText2);
	for(ilItemNo=0;ilItemNo<ilFieldCnt;ilItemNo++)
	  {
	    strcat(pcpResult,"{ITEM}");
	    strncpy(pclTmpFirstField,pclPtr1,9);
	    pclTmpFirstField[9]='\0';
	    TrimRight(pclTmpFirstField);
	    if(ilLastbyte>=ilFirstbyte && ilLastbyte>0)
	      {
                /*first cut the tail...*/
                pclTmpFirstField[ilLastbyte] = '\0';
                /*...then the top*/
                strcat(pcpResult,&pclTmpFirstField[ilFirstbyte-1]);
	      }else{
                strcat(pcpResult,pclTmpFirstField);
	      }
            if(strlen(pclPtr1)>9)
	      pclPtr1+=9;
	  }
        }
      if(prlPageCommand->iLanguage==1)
        {
	  MapToArabicN(pcpResult,M_BUFF);
        }
      memset(pcpCommand,0x00,M_BUFF);
      strcpy(pcpCommand,"gtTextPutFormatted");
      /*DeleteCharacterInString(pcpResult,cBLANK);*/
      if(igdebug_dzcommand)
	dbg(TRACE,"SetPageData: <%s>  Command <%s>",pcpResult,pcpCommand);
      return RC_SUCCESS;
    }/*endif "J"*/
  /*section for airline logos*/
  /********************************************************/
  /**SHOW A LOGO ******************************************/
  /********************************************************/
  if(strcmp(pclDspType,"L")==0)
    {
      memset(pclTmpBuf,0x00,XS_BUFF);
      strcpy(pclTmpBuf,prFieldRef->pcFieldName);
#ifdef _DXB
      /*field mapping because AOTI is not set by fldhdl*/
      if(!strcmp(pclTmpBuf,"AOTI"))
	strcpy(pclTmpBuf,"B1BA");
#endif
      ilItemNo = get_item_no(prRecord->pcFields,pclTmpBuf,5);
      ilItemNo++;  
      if(igdebug_dzcommand)
	dbg(DEBUG,"SetPageData ITEM  <%s>  NUMBER IS  <%d>",pclTmpBuf,ilItemNo);
      GetDataItem(pclTmpFirstField,prRecord->pcRecordset,ilItemNo,',',"","");
      ilFirstbyte = prFieldRef->iStart;
      ilLastbyte =  prFieldRef->iEnd;
      if(ilLastbyte>=ilFirstbyte && ilLastbyte>0)
	{
	  /*first cut the tail...*/
	  pclTmpFirstField[ilLastbyte] = '\0';
	  /*...then the top*/
	  strcpy(pcpResult,&pclTmpFirstField[ilFirstbyte-1]);
	  if(pcpResult[ilLastbyte-1]== 0x20 )
	    {
	      pcpResult[ilLastbyte-1]= '\0';
	    }
	}else{
	  strcpy(pcpResult,pclTmpFirstField);
	  if(pcpResult[ilLastbyte-1]== 0x20 )
	    {
	      pcpResult[ilLastbyte-1]= '\0';
	    }
#ifdef _TLL
	  TrimRight(pcpResult);
#endif	  
	} 
      /*setting path to gifs*/

      strcpy(pcpFileBuf,pcgPathToGif);
#ifdef _DXB
      if(!strcmp(pclTmpBuf,"B1BA"))
	{
	  TrimRight(pcpResult);
	  if(strlen(pcpResult)>10)
	    {
	      strcpy(pcpResult,prlDspType->rL.pcExtension);
	      strcat(pcpFileBuf,pcpResult);
	      /*INSERTFILER*/
	      strcpy(pcpCommand,"gtGIFPutFile");
	      if(igdebug_dzcommand)
		dbg(TRACE,"SetPageData: <%s> Command <%s>",pcpResult,pcpCommand);  
	      return RC_SUCCESS;
	    }else{
	      strcpy(pcpCommand,"gtGIFPutFile");
	      strcpy(pcpResult," ");
	      if(igdebug_dzcommand)
		dbg(TRACE,"SetPageData: <%s> Command <%s>",pcpResult,pcpCommand); 
	      return RC_SUCCESS;
	    }
	}
#endif
      strcat(pcpResult,prlDspType->rL.pcExtension);
      strcat(pcpFileBuf,pcpResult);
      /*INSERTFILER*/
      strcpy(pcpCommand,"gtGIFPutFile");
      if(igdebug_dzcommand)
	dbg(TRACE,"SetPageData: <%s>  Command <%s>",pcpResult,pcpCommand);  
      return RC_SUCCESS;
    }/*endif "L"*/
  if(strcmp(pclDspType,"LJ")==0)
    {
      memset(pclTmpBuf,0x00,XS_BUFF);
      strcpy(pclTmpBuf,prFieldRef->pcFieldName);
      DeleteCharacterInString(pclTmpBuf,cBLANK);
      ilItemNo = get_item_no(prRecord->pcFields,pclTmpBuf,5);
      ilItemNo++;  
      if(igdebug_dzcommand)
	dbg(DEBUG,"SetPageData ITEM  <%s>  NUMBER IS  <%d>",pclTmpBuf,ilItemNo);
      GetDataItem(pclTmpFirstField,prRecord->pcRecordset,ilItemNo,',',"","");
      if(strcmp(rgPG.prPages[ipCurPag].pcDisplayType,"C")==0||
	 strcmp(rgPG.prPages[ipCurPag].pcDisplayType,"G")==0||
	 strcmp(rgPG.prPages[ipCurPag].pcDisplayType,"L")==0)
	{
	  ilItemNo=get_item_no(prRecord->pcFields,"JFNO",5);
	  ilItemNo++;
	  GetDataItem(pclText1,prRecord->pcRecordset,ilItemNo,',',"","");
	  ilItemNo=0;
	  ilItemNo=get_item_no(prRecord->pcFields,"JCNT",5);
	  ilItemNo++;
	  GetDataItem(pclText2,prRecord->pcRecordset,ilItemNo,',',"","");
	  ilItemNo=0;
	}
      ilFirstbyte = prFieldRef->iStart;
      ilLastbyte =  prFieldRef->iEnd;
      if(ilLastbyte>=ilFirstbyte && ilLastbyte>0)
        {
	  /*first cut the tail...*/
	  pclTmpFirstField[ilLastbyte] = '\0';
	  /*...then the top*/
	  strcpy(pcpResult,&pclTmpFirstField[ilFirstbyte-1]);
	  if(pcpResult[ilLastbyte-1]== 0x20 )
            {
	      pcpResult[ilLastbyte-1]= '\0';
            }
        }else{
	  strcpy(pcpResult,pclTmpFirstField);
	  if(pcpResult[ilLastbyte-1]== 0x20 )
            {
	      pcpResult[ilLastbyte-1]= '\0';
            }
        }
      /*setting path to gifs*/
      strcpy(pcpFileBuf,pcgPathToGif);
      if(ilLastbyte>=ilFirstbyte && ilLastbyte>0)
        {
	  /*first cut the tail...*/
	  pclTmpFirstField[ilLastbyte] = '\0';
	  /*...then the top*/
	  strcpy(pcpResult,&pclTmpFirstField[ilFirstbyte-1]);
        }else{
	  strcpy(pcpResult,pclTmpFirstField);
        }
      strcpy(pclText3,pcpResult);
      DeleteCharacterInString(pclText3,cBLANK);
      if(pclText1[0]!='\0')
        {   pclPtr1=pclText1;
	ilFieldCnt=atoi(pclText2);
	for(ilItemNo=0;ilItemNo<ilFieldCnt;ilItemNo++)
	  {
	    strncpy(pclTmpFirstField,pclPtr1,9);
	    pclTmpFirstField[9]='\0';
	    TrimRight(pclTmpFirstField);
	    if(ilLastbyte>=ilFirstbyte && ilLastbyte>0)
	      {
                /*first cut the tail...*/
                pclTmpFirstField[ilLastbyte] = '\0';
                /*...then the top*/
                strcat(pcpResult,&pclTmpFirstField[ilFirstbyte-1]);
	      }else{
                strcat(pcpResult,pclTmpFirstField);
	      }
            if(strlen(pclPtr1)>9)
	      pclPtr1+=9;
	  }
        }
      DeleteCharacterInString(pcpResult,cBLANK);
      strcat(pcpResult,prlDspType->rLJ.pcExtension);
      strcat(pcpFileBuf,pcpResult);
      fp=NULL;
      fp=fopen(pcpFileBuf,"r");
      if(fp==NULL)
	{
	  strcpy(pcpResult,pclText3);
	  strcat(pcpResult,prlDspType->rLJ.pcExtension);
	  strcpy(pcpFileBuf,pcgPathToGif);
	  strcat(pcpFileBuf,pcpResult);
        }else{
	  fclose(fp);
        }
      /*INSERTFILER*/
      strcpy(pcpCommand,"gtGIFPutFile");
      if(igdebug_dzcommand)
	dbg(TRACE,"SetPageData: <%s>  Command <%s>",pcpResult,pcpCommand);  
      return RC_SUCCESS;
    }/*endif "LJ"*/
  /********************************************************/
  /**DISPLAY A TIME IN LOCAL TIME**************************/
  /********************************************************/
  if(strcmp(pclDspType,"T")==0)
    {
      memset(pclTmpBuf,0x00,XS_BUFF);
      strcpy(pclTmpBuf,prFieldRef->pcFieldName);
      /*changed for all display natures 08.03.2001 */
      ilItemNo = get_item_no(prRecord->pcFields,pclTmpBuf,5);
      ilItemNo++;  
      memset(pclTmpFirstField,0x00,XS_BUFF);
      GetDataItem(pclTmpFirstField,prRecord->pcRecordset,ilItemNo,',',"","");
      strcpy(pcpCommand,"gtTextPutFormatted");
      if(igdebug_dzcommand)
	{
	  dbg(DEBUG,"SetPageData UTCTIME = <%s>",pclTmpFirstField);
	  dbg(DEBUG,"SetPageData TIME FORMAT = <%s>",prlDspType->rT.pcFormat);
	}
      if((ilRc=UtcToLocal(pclTmpFirstField,NULL,prlDspType->rT.pcFormat))!=RC_SUCCESS)
	{
	  if(igdebug_dzcommand)
	    dbg(TRACE,"SetPageData  UtcToLocal failed with <%d> in Line %d",ilRc,__LINE__);
	  memset(pcpResult,0x00,M_BUFF);
	  strcpy(pcpResult,"--:--");
	}else{
	  memset(pcpResult,0x00,M_BUFF);
	  strcpy(pcpResult,pclTmpFirstField);
	  if(prlPageCommand->iLanguage==1)
	    {
	      MapToArabic(pcpResult,M_BUFF);
	    }
	  if(igdebug_dzcommand)
	    dbg(TRACE,"SetPageData: <%s>  Command <%s>",pcpResult,pcpCommand);
	}
      return RC_SUCCESS;
    }
  /**********************************************************/
  /**BLINK | TIME FROM AFTTAB *******************************/
  /**********************************************************/
  if(strcmp(pclDspType,"B")==0)
    {
      memset(pclText1,0x00,XS_BUFF);
      memset(pclText2,0x00,XS_BUFF);
      memset(pclText3,0x00,XS_BUFF);
      memset(pclText4,0x00,XS_BUFF);
      memset(pclBlink,0x00,XS_BUFF);
      memset(pclTmpBuf,0x00,XS_BUFF);
      memset(pclFieldBuf,0x00,XS_BUFF);

      for(ilFieldCnt=0;ilFieldCnt < prlDspType->rB.iNFieldNames;ilFieldCnt++)
	{
	  if(ilFieldCnt==0)
	    {
	      strcpy(pclFieldBuf,prlDspType->rB.p2cFieldNames[ilFieldCnt]);
	    }else{
	      strcat(pclFieldBuf,",");
	      strcat(pclFieldBuf,prlDspType->rB.p2cFieldNames[ilFieldCnt]);
	    }
	}
      memset(pclTmpBuf,0x00,XS_BUFF);
      strcpy(pclTmpBuf,prFieldRef->pcFieldName);
      /*changed for all display natures 08.03.2001 */
      ilItemNo = get_item_no(prRecord->pcFields,pclTmpBuf,5);
      ilItemNo++; 
      if(igdebug_dzcommand)
	dbg(DEBUG,"SetPageData ITEM <%s>  NUMBER IS  <%d>",pclTmpBuf,ilItemNo);
      memset(pclTmpFirstField,0x00,XS_BUFF);
      GetDataItem(pclTmpFirstField,prRecord->pcRecordset,ilItemNo,',',"","");
      DeleteCharacterInString(pclTmpBuf,cBLANK);
      DeleteCharacterInString(pclTmpFirstField,cBLANK);
#ifdef _DXB
      if(strcmp(rgPG.prPages[ipCurPag].pcDisplayType,"G")==0)
	{
	  if(pclTmpFirstField[0]=='\0')
	    {
	      strcpy(pclTmpFirstField,"CLR");
	    }
	}
#endif
      if(strlen(pclTmpFirstField)>1)
	{
	  sprintf(pclSqlBuf,"SELECT %s,BLKC FROM FID%s WHERE CODE='%s'",pclFieldBuf,pcgTabEnd,pclTmpFirstField);
	  slFkt = START; 
	  slCursor = 0;
	  if(igdebug_dzcommand)
	    dbg(DEBUG,"SetPageData  pclSqlBuf for code = <%s>",pclSqlBuf);      
	  if((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))!=RC_SUCCESS)
	    {
	      dbg(DEBUG,"<InitDatabaseFieldInfos> sql_if in Line <%d> failed with <%d>",__LINE__,ilRc);
	      close_my_cursor(&slCursor);
	    }else{
	      close_my_cursor(&slCursor);
	      if(prlDspType->rB.iNFieldNames > 1)
		{
		  GetDataItem(pclText1,pclTmpSqlAnswer,1,'\0',"","\0 ");
		  GetDataItem(pclText2,pclTmpSqlAnswer,2,'\0',"","\0 ");
		  GetDataItem(pclBlink,pclTmpSqlAnswer,3,'\0',"","\0 ");
		  StringUPR((UCHAR*)pclBlink);
		}
	      if(prlDspType->rB.iNFieldNames == 1)
		{
		  GetDataItem(pclText1,pclTmpSqlAnswer,1,'\0',"","\0 ");
		  GetDataItem(pclBlink,pclTmpSqlAnswer,2,'\0',"","\0 ");
		}
	    }
	  if(igdebug_dzcommand) 
	    {
	      if(prlDspType->rB.iNFieldNames > 1)
		{
		  dbg(DEBUG,"SetPageData pclText1 for code = <%s>",pclText1);    
		  dbg(DEBUG,"SetPageData pclText2 for code = <%s>",pclText2);    
		  dbg(DEBUG,"SetPageData pclBlink for code = <%s>",pclBlink);
		}
	      if(prlDspType->rB.iNFieldNames == 1)
		{
		  dbg(DEBUG,"SetPageData pclText1 for code = <%s>",pclText1);
		  dbg(DEBUG,"SetPageData pclBlink for code = <%s>",pclBlink);
		}
	    }      
	}else{
	  strcpy(pclText1,pclTmpFirstField);
	  strcpy(pclText2,pclTmpFirstField);
	}
      memset(pclText3,0x00,XS_BUFF);
      memset(pclText4,0x00,XS_BUFF);
      pclTimePtr=NULL;
      if(strstr(pclText1,"HH:MM")!=NULL)
        pclTimePtr=strstr(pclText1,"HH:MM");
      if(strstr(pclText1,"HH:mm")!=NULL)
        pclTimePtr=strstr(pclText1,"HH:mm");
      if(strstr(pclText1,"NH:mm")!=NULL)
        pclTimePtr=strstr(pclText1,"NH:mm");
      if(strstr(pclText1,"hh:mm")!=NULL)
        pclTimePtr=strstr(pclText1,"hh:mm");
      if(strstr(pclText1,"HHmm")!=NULL)
        pclTimePtr=strstr(pclText1,"HHmm");
      if(pclTimePtr != NULL)
        {/*if hh:mm*/
          if(prlPageCommand->iLanguage==1)
	    {
	      strncpy(pclTimeBuf,pclTimePtr,5);
	      pclTimePtr+=5;
	      strcpy(pclText1,pclTimePtr);
	      if(pclText1[0]==cBLANK)
		{
		  pclPtr1= pclText1;
		  strcpy(pclText1,++pclPtr1);
		  strcat(pclText1," ");
		}
	      pclTimeBuf[5]='\0';
     
	      /*dbg(TRACE,"DCPFI Remark pclTimeBuf = <%s>", pclTimeBuf);*/
	      /*dbg(TRACE,"DCPFI Remark pclText1 = <%s>",pclText1); */
	    }else{
	      strcpy(pclTimeBuf,pclTimePtr);
	      DeleteCharacterInString(pclTimeBuf,cBLANK);
	      *pclTimePtr='\0';
	    }
	  if(!strcmp(rgPG.prPages[ipCurPag].pcDisplayType,"A")||
	     !strcmp(rgPG.prPages[ipCurPag].pcDisplayType,"D"))
	    {
	      ilItemNo = get_item_no(prRecord->pcFields,"RMTI",5);
	      ilItemNo++;  
	      if(ilItemNo >0)
		{
		  GetDataItem(pclText3,prRecord->pcRecordset,ilItemNo,',',"","");
		  if(igdebug_dzcommand)
                    dbg(DEBUG,"SetPageData ITEM %s  NUMBER IS  <%d>",pclTmpBuf,ilItemNo);
		}
	    }
	  if(strcmp(rgPG.prPages[ipCurPag].pcDisplayType,"C")==0||
	     strcmp(rgPG.prPages[ipCurPag].pcDisplayType,"G")==0||
	     strcmp(rgPG.prPages[ipCurPag].pcDisplayType,"L")==0)
	    {
	      ilItemNo = get_item_no(prRecord->pcFields,"ETOD",5);
	      ilItemNo++;  
	      if(ilItemNo >0)
		{
		  GetDataItem(pclText3,prRecord->pcRecordset,ilItemNo,',',"","");
		  if(igdebug_dzcommand)
		    {
		      dbg(DEBUG,"SetPageData  field ITEM %s  NUMBER IS  <%d>",pclTmpBuf,ilItemNo);
		    }
		}
	    }

	  if(!strcmp(rgPG.prPages[ipCurPag].pcDisplayType,"L")&&!strcmp(rgPG.prPages[ipCurPag].pcDisplayTypeInternal,"B"))
	    {
	      pclText3[0]='\0';
	      if(strstr(pclTmpFirstField,"FBG")!=NULL)
		{
		  ilItemNo = get_item_no(prRecord->pcFields,"AOTI",5);
		  ilItemNo++;
		}
	      if(strstr(pclTmpFirstField,"LBG")!=NULL)
		{
		  ilItemNo = get_item_no(prRecord->pcFields,"ACTI",5);
		  ilItemNo++;
		}
	      if(ilItemNo >0)
		{
		  GetDataItem(pclText3,prRecord->pcRecordset,ilItemNo,',',"","");
		  if(igdebug_dzcommand)
                    dbg(DEBUG,"SetPageData ITEM %s  NUMBER IS  <%d>",pclTmpBuf,ilItemNo);
		}
	    }
	  DeleteCharacterInString(pclText3,cBLANK);
	  if(igdebug_dzcommand)
	    dbg(DEBUG,"UTCTIME => <%s>",pclText3);
	  if((ilRc=UtcToLocal(pclText3,NULL,pclTimeBuf))!=RC_SUCCESS)
	    {
	      dbg(TRACE,"SetPageData  UtcToLocal failed with <%d> in Line %d",ilRc,__LINE__);
	      memset(pcpResult,0x00,M_BUFF);
	      strcpy(pcpResult,"--:--");      
	    }
	}
      pclTimePtr=NULL;
      if(strstr(pclText2,"HH:MM")!=NULL)
        pclTimePtr=strstr(pclText2,"HH:MM");
      if(strstr(pclText2,"HH:mm")!=NULL)
        pclTimePtr=strstr(pclText2,"HH:mm");
      if(strstr(pclText2,"NH:mm")!=NULL)
        pclTimePtr=strstr(pclText2,"NH:mm");
      if(strstr(pclText2,"hh:mm")!=NULL)
        pclTimePtr=strstr(pclText2,"hh:mm");
      if(strstr(pclText2,"HHmm")!=NULL)
        pclTimePtr=strstr(pclText2,"HHmm");
      if(pclTimePtr != NULL) 
        {/*if hh:mm*/
          if(prlPageCommand->iLanguage==1)
	    {
	      strncpy(pclTimeBuf,pclTimePtr,5);
	      pclTimePtr+=5;
	      strcpy(pclText2,pclTimePtr);
	      if(pclText1[0]==cBLANK)
                {
		  pclPtr1= pclText2;
		  strcpy(pclText2,++pclPtr1);
		  strcat(pclText2," ");
                }
	      pclTimeBuf[5]='\0';
     
	      /*dbg(TRACE,"DCPFI Remark pclTimeBuf = <%s>", pclTimeBuf);*/
	      /*dbg(TRACE,"DCPFI Remark pclText1 = <%s>",pclText1); */
            }else{
	      strcpy(pclTimeBuf,pclTimePtr);
	      DeleteCharacterInString(pclTimeBuf,cBLANK);
	      *pclTimePtr='\0';
            }
	  if(!strcmp(rgPG.prPages[ipCurPag].pcDisplayType,"A")||
	     !strcmp(rgPG.prPages[ipCurPag].pcDisplayType,"D"))
	    {
	      ilItemNo = get_item_no(prRecord->pcFields,"RMTI",5);
	      ilItemNo++;  
	      if(ilItemNo >0)
		{
		  GetDataItem(pclText4,prRecord->pcRecordset,ilItemNo,',',"","");
		  if(igdebug_dzcommand)
                    dbg(DEBUG,"SetPageData ITEM %s  NUMBER IS  <%d>",pclTmpBuf,ilItemNo);
		}
	    }
	 
	  if(strcmp(rgPG.prPages[ipCurPag].pcDisplayType,"C")==0||
	     strcmp(rgPG.prPages[ipCurPag].pcDisplayType,"G")==0||
	     strcmp(rgPG.prPages[ipCurPag].pcDisplayType,"L")==0)
	    {
	      ilItemNo = get_item_no(prRecord->pcFields,"ETOD",5);
	      ilItemNo++;  
	      if(ilItemNo >0)
		{
		  GetDataItem(pclText4,prRecord->pcRecordset,ilItemNo,',',"","");
		  if(igdebug_dzcommand)
		    {
		      dbg(DEBUG,"SetPageData  field ITEM %s  NUMBER IS  <%d>",pclTmpBuf,ilItemNo);
		    }
		}
	    }

	  if(!strcmp(rgPG.prPages[ipCurPag].pcDisplayType,"L")&&!strcmp(rgPG.prPages[ipCurPag].pcDisplayTypeInternal,"B"))
	    {
	      pclText4[0]='\0';
	      if(strstr(pclTmpFirstField,"FBG")!=NULL)
		{
		  ilItemNo = get_item_no(prRecord->pcFields,"AOTI",5);
		  ilItemNo++;
		}
	      if(strstr(pclTmpFirstField,"LBG")!=NULL)
		{
		  ilItemNo = get_item_no(prRecord->pcFields,"ACTI",5);
		  ilItemNo++;
		}
	      if(ilItemNo >0)
		{
		  GetDataItem(pclText4,prRecord->pcRecordset,ilItemNo,',',"","");
		  if(igdebug_dzcommand)
                    dbg(DEBUG,"SetPageData ITEM %s  NUMBER IS  <%d>",pclTmpBuf,ilItemNo);
		}
	    }

	  DeleteCharacterInString(pclText4,cBLANK);
	  if(igdebug_dzcommand)
	    dbg(DEBUG,"UTCTIME => <%s>",pclText4);
	  if((ilRc=UtcToLocal(pclText4,NULL,pclTimeBuf))!=RC_SUCCESS)
	    {
	      dbg(TRACE,"SetPageData  UtcToLocal failed with <%d> in Line %d",ilRc,__LINE__);
	      memset(pcpResult,0x00,M_BUFF);
	      strcpy(pcpResult,"--:--");      
	    }
	}
      if(prlPageCommand->iLanguage==1)
        {
	  MapToArabic(pclText1,XS_BUFF);
	  MapToArabic(pclText3,XS_BUFF);
	  MapToArabic(pclText4,XS_BUFF);
        }
      memset(pcpResult,0x00,M_BUFF);
      if(prlPageCommand->iLanguage==1)
	{

	  if(strchr(pclBlink,'Y')!=NULL||strchr(pclBlink,'X')!=NULL)
	    {
	      strcpy(pcpResult,"{FG ");
	      sprintf(pcpResult,"%s %d",pcpResult,prlDspType->rB.iBlinkColor);
	      strcat(pcpResult,"}");
	      strcat(pcpResult,pclText3);
	      strcat(pcpResult,pclText1);
	      strcat(pcpResult,"{fg}");
	    }else{
	      strcpy(pcpResult,pclText3);
	      strcat(pcpResult,pclText1);
	    }  
	}else{
	  if(strchr(pclBlink,'Y')!=NULL||strchr(pclBlink,'X')!=NULL)
	    {
	      strcpy(pcpResult,"{FG ");
	      sprintf(pcpResult,"%s %d",pcpResult,prlDspType->rB.iBlinkColor);
	      strcat(pcpResult,"}");
	      strcat(pcpResult,pclText1);
	      strcat(pcpResult,pclText3);
	      strcat(pcpResult,"{fg}");
	      if(prlDspType->rB.iNFieldNames > 1)
		{
		  strcat(pcpResult,"{ITEM}");
		  strcat(pcpResult,"{FG ");
		  sprintf(pcpResult,"%s %d",pcpResult,prlDspType->rB.iBlinkColor);
		  strcat(pcpResult,"}");
		  strcat(pcpResult,pclText2);
		  strcat(pcpResult,pclText4);
		  strcat(pcpResult,"{fg}");
		}
	    }else{
	      strcpy(pcpResult,pclText1);
	      strcat(pcpResult,pclText3);
	      if(prlDspType->rB.iNFieldNames > 1)
		{
		  strcat(pcpResult,"{ITEM}");
		  strcat(pcpResult,pclText2);
		  strcat(pcpResult,pclText4);
		}
	    }
	}
      strcpy(pcpCommand,"gtTextPutFormatted");
      if(igdebug_dzcommand)
	dbg(TRACE,"SetPageData: <%s>  Command <%s>",pcpResult,pcpCommand);
      return RC_SUCCESS;
    }/*endif "B"*/
  /********************************************************/
  /**AIRLINE FULLNAME (INSTAED OF AIRLINELOGO)*************/
  /********************************************************/
  if(strcmp(pclDspType,"AL")==0)
    {
      memset(pclTmpBuf,0x00,XS_BUFF);
      memset(pclFieldBuf,0x00,XS_BUFF);
      strcpy(pclTmpBuf,prFieldRef->pcFieldName);
      ilItemNo = get_item_no(prRecord->pcFields,pclTmpBuf,5);
      ilItemNo++;  
      if(igdebug_dzcommand)
	dbg(DEBUG,"SetPageData ITEM <%s> NUMBER IS <%d>",pclTmpBuf,ilItemNo);
      memset(pclTmpFirstField,0x00,XS_BUFF);
      GetDataItem(pclTmpFirstField,prRecord->pcRecordset,ilItemNo,',',"","");
      ilFirstbyte = prFieldRef->iStart;
      ilLastbyte = prFieldRef->iEnd;
      pclTmpBuf[0]='\0';
      if(ilLastbyte>=ilFirstbyte && ilLastbyte>0)
	{
	  /*first cut the tail...*/
	  pclTmpFirstField[ilLastbyte] = '\0';
	  /*...then the top*/
	  strcpy(pclTmpBuf,&pclTmpFirstField[ilFirstbyte-1]);
	}else{
	  strcpy(pclTmpBuf,pclTmpFirstField);
	}
      strcpy(pclTmpFirstField,pclTmpBuf);
      TrimRight(pclTmpFirstField);
      ilRc=RC_FAIL;
      if(strlen(pclTmpFirstField)==2)
	{
	  sprintf(pclSqlBuf,"SELECT %s FROM ALT%s WHERE ALC2='%s'",prlDspType->rAL.pcFieldName,pcgTabEnd,pclTmpFirstField);
	  ilRc=RC_SUCCESS;
	}
      if(strlen(pclTmpFirstField)==3)
	{
	  sprintf(pclSqlBuf,"SELECT %s FROM ALT%s WHERE ALC3='%s'",prlDspType->rAL.pcFieldName,pcgTabEnd,pclTmpFirstField);
	  ilRc=RC_SUCCESS;
	}
      if(ilRc==RC_FAIL)
	{
	  strcpy(pcpResult," ");
	  strcpy(pcpCommand,"gtTextPutFormatted");
	  if(igdebug_dzcommand) 
	    dbg(TRACE,"SetPageData <%s>",pcpResult); 
	  return RC_SUCCESS;
	}
      slFkt = START; 
      slCursor = 0;
      ilRc=sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer);
      close_my_cursor(&slCursor);
      if(ilRc!=RC_SUCCESS)
	{
	  strcpy(pcpResult," ");
	  strcpy(pcpCommand,"gtTextPutFormatted");
	  if(igdebug_dzcommand) 
	    dbg(DEBUG,"SetPageData <%s>",pcpResult); 
	}else{
	  GetDataItem(pcpResult,pclTmpSqlAnswer,1,'\0',"","\0 ");
	}
      if(igdebug_dzcommand) 
	dbg(TRACE,"SetPageData <%s>  Command <%s>",pcpResult,pcpCommand); 
      strcpy(pcpCommand,"gtTextPutFormatted");
      return  RC_SUCCESS;
    }/*endif "AL"*/
  return RC_NOTFOUND; 
}
/*****************************************************/
/* Checking Devicestate for maintanance mode         */
/*****************************************************/
static int CheckDeviceState()
{
  int ilRc = 0;
  int ilRcgt = 0;
  short slFkt = 0;
  short slCursor = 0;
  char pclTmpSqlAnswer[L_BUFF];
  char pclSqlBuf[S_BUFF];
  char pclStat[XS_BUFF];
  int  ilCurDev=0;
  char pclLastDegaussTime[XS_BUFF];
  for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)  
    {/* for 1*/
      memset(pclSqlBuf,0x00,S_BUFF);
      memset(pclTmpSqlAnswer,0x00,L_BUFF);
      sprintf(pclSqlBuf,"SELECT STAT,DLDT  FROM DEV%s WHERE URNO='%s'",pcgTabEnd,rgDV.prDevPag[ilCurDev].pcDevUrno);
      slFkt = START; 
      slCursor = 0;
      if((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))!=RC_SUCCESS)
        {
	  close_my_cursor(&slCursor); 
	  if(ilRc == 1)
	    {
	      /*not found could be*/
	      ilRc=RC_SUCCESS;
	    }else{
	      dbg(TRACE,"<CheckDeviceState> sql_if in Line <%d> failed with <%d>",__LINE__,ilRc);
	      dbg(TRACE,"<CheckDeviceState> <%s>",pclSqlBuf);
	    }
        }else{
	  close_my_cursor(&slCursor);
	  BuildItemBuffer(pclTmpSqlAnswer,"STAT,DLDT",0,",");
	  StringUPR((UCHAR*)pclTmpSqlAnswer);
	  dbg(DEBUG,"<CheckDeviceState> Device State for dev %s is <%s>",rgDV.prDevPag[ilCurDev].pcIP,pclTmpSqlAnswer);
	  /*Kill Socket*/
	  if(strchr(pclTmpSqlAnswer,'M')!=NULL)
            {
	      /*kill device state*/
	      rgDV.prDevPag[ilCurDev].pcDeviceState[0]='\0';
	      /*set devicestate to Maintanance*/
	      strcpy(rgDV.prDevPag[ilCurDev].pcDeviceState,"M");
	      dbg(DEBUG,"<CheckDeviceState> dsplib_shutdown socket closed");
	      if(rgDV.prDevPag[ilCurDev].iSocket > 0)
                {
		  dbg(DEBUG,"<CheckDeviceState> dsplib_shutdown socket %d closed  ",rgDV.prDevPag[ilCurDev].iSocket);
		  if((ilRc=close(rgDV.prDevPag[ilCurDev].iSocket))!= RC_SUCCESS)
                    {
		      dbg(DEBUG,"<CheckDeviceState>%05d close returns %d",__LINE__,ilRc);
                    }
                }
	      rgDV.prDevPag[ilCurDev].iSocket = 0;
            }else if(strchr(pclTmpSqlAnswer,'R')!=NULL)
	      {
		rgDV.prDevPag[ilCurDev].pcDeviceState[0]='\0';
		/*set devicestate to Maintanance*/
		strcpy(rgDV.prDevPag[ilCurDev].pcDeviceState,"D");
		gtBuildCommand("1","gtTerminalInit","gtSYSTEM_TERMINAL",1,"1","",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
		if((ilRc =SetDevState(rgDV.prDevPag[ilCurDev].pcIP,DOWN,rgDV.prDevPag[ilCurDev].pcDevUrno)) != RC_SUCCESS)
		  {
		    dbg(TRACE,"DisconnectDevices: SetDevState() returns <%d>!",ilRc);
		  }
		dbg(DEBUG,"<CheckDeviceState> reboot device <%s>",rgDV.prDevPag[ilCurDev].pcIP);
		if(rgDV.prDevPag[ilCurDev].iSocket > 0)
		  {
		    dbg(DEBUG,"<CheckDeviceState> dsplib_shutdown socket %d closed for reboot ",rgDV.prDevPag[ilCurDev].iSocket);
		    if((ilRc=close(rgDV.prDevPag[ilCurDev].iSocket))!= RC_SUCCESS)
		      {
			dbg(DEBUG,"<CheckDeviceState>%05d close returns %d",__LINE__,ilRc);
		      }
		  }
		rgDV.prDevPag[ilCurDev].iSocket = 0;
	      }else if(pclTmpSqlAnswer[0]==cBLANK)
		{/* undefined status*/
		  if(rgDV.prDevPag[ilCurDev].iSocket == 0)
		    {
		      dbg(DEBUG,"<CheckDeviceState>Set device <%s> down",rgDV.prDevPag[ilCurDev].pcIP);
		      rgDV.prDevPag[ilCurDev].pcDeviceState[0]='\0';
		      /*set devicestate to Maintanance*/
		      strcpy(rgDV.prDevPag[ilCurDev].pcDeviceState,"D");
		      if((ilRc =SetDevState(rgDV.prDevPag[ilCurDev].pcIP,DOWN,rgDV.prDevPag[ilCurDev].pcDevUrno)) != RC_SUCCESS)
			{
			  dbg(TRACE,"DisconnectDevices: SetDevState() returns <%d>!",ilRc);
			}
		    }
		  
		  if(rgDV.prDevPag[ilCurDev].iSocket > 0)
		    {
		      ilRcgt = gtBuildCommand("1","gtTerminalStatusRequest","gtSYSTEM_TERMINAL",0,"","",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
		      if( ilRcgt == RC_SUCCESS)
			{
			  if((ilRc =SetDevState(rgDV.prDevPag[ilCurDev].pcIP,UP,rgDV.prDevPag[ilCurDev].pcDevUrno)) != RC_SUCCESS)
			    {
			      dbg(TRACE,"DisconnectDevices: SetDevState() returns <%d>!",ilRc);
			    }		
			  dbg(DEBUG,"<CheckDeviceState> dsplib_shutdown socket %d closed for reboot ",rgDV.prDevPag[ilCurDev].iSocket);
			}else{
			  dbg(DEBUG,"<CheckDeviceState>Set device <%s> down",rgDV.prDevPag[ilCurDev].pcIP);
			  rgDV.prDevPag[ilCurDev].pcDeviceState[0]='\0';
			  /*set devicestate to Maintanance*/
			  strcpy(rgDV.prDevPag[ilCurDev].pcDeviceState,"D");
			  if((ilRc =SetDevState(rgDV.prDevPag[ilCurDev].pcIP,DOWN,rgDV.prDevPag[ilCurDev].pcDevUrno)) != RC_SUCCESS)
			    {
			      dbg(TRACE,"DisconnectDevices: SetDevState() returns <%d>!",ilRc);
			    }
			  rgDV.prDevPag[ilCurDev].iSocket =0;
			}
		    }
		}else{
		  /*check socket*/
		  if(rgDV.prDevPag[ilCurDev].iSocket > 0)
		    {
		      memset(pcgResBuf,0x00,RES_BUF_SIZE);
		      ilRcgt= gtBuildCommand("1","gtTerminalStatusRequest","gtSYSTEM_TERMINAL",0,"","",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
		      dbg(DEBUG,"<CheckDeviceState> %05d gtTerminalStatusRequest: \n  ilRcgt=<%d>, ResultBuf <%s>",__LINE__,ilRcgt,pcgResBuf);
		      /*not connected or bad conection*/
		      if(ilRcgt==-1)
			{
			  if((ilRc=close(rgDV.prDevPag[ilCurDev].iSocket))!= RC_SUCCESS)
			    {
			      dbg(DEBUG,"<CheckDeviceState>%05d close returns %d",__LINE__,ilRc);
			    }
			  rgDV.prDevPag[ilCurDev].iSocket = 0;
			  rgDV.prDevPag[ilCurDev].pcDeviceState[0]='\0';
			  strcpy(rgDV.prDevPag[ilCurDev].pcDeviceState,"D");
			  if((ilRc =SetDevState(rgDV.prDevPag[ilCurDev].pcIP,DOWN,rgDV.prDevPag[ilCurDev].pcDevUrno)) != RC_SUCCESS)
			    {
			      dbg(TRACE,"DisconnectDevices: SetDevState() returns <%d>!",ilRc);
			    }
			}
		      if(ilRcgt==-9)
			{
			  /*just wait a little bit*/
			  nap(300);
			  memset(pcgResBuf,0x00,RES_BUF_SIZE);
			  ilRcgt= gtBuildCommand("1","gtTerminalStatusRequest","gtSYSTEM_TERMINAL",0,"","",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
			  dbg(DEBUG,"<CheckDeviceState> %05d Send again  ilRcgt=<%d>,",__LINE__,ilRcgt);
			  /*not connected or bad conection*/
			  if(ilRcgt==-9)
			    {
			      if((ilRc=close(rgDV.prDevPag[ilCurDev].iSocket))!= RC_SUCCESS)
				{
				  dbg(DEBUG,"<CheckDeviceState>%05d close returns %d",__LINE__,ilRc);
				}
			      rgDV.prDevPag[ilCurDev].iSocket = 0;
			      rgDV.prDevPag[ilCurDev].pcDeviceState[0]='\0';
			      strcpy(rgDV.prDevPag[ilCurDev].pcDeviceState,"D");
			      if((ilRc =SetDevState(rgDV.prDevPag[ilCurDev].pcIP,DOWN,rgDV.prDevPag[ilCurDev].pcDevUrno)) != RC_SUCCESS)
				{
				  dbg(TRACE,"DisconnectDevices: SetDevState() returns <%d>!",ilRc);
				}
			    }
			}
		    }else{
		      /*no or invalid Socket device is not connected*/
		      if(!strcmp (rgDV.prDevPag[ilCurDev].pcDeviceState,"U"))
			{
			  if((ilRc =SetDevState(rgDV.prDevPag[ilCurDev].pcIP,DOWN,rgDV.prDevPag[ilCurDev].pcDevUrno)) != RC_SUCCESS)
			    {
			      dbg(TRACE,"DisconnectDevices: SetDevState() returns <%d>!",ilRc);
			    }
			  strcpy(rgDV.prDevPag[ilCurDev].pcDeviceState,"D");
			}
		    }
		}
        }
    }
  return ilRc;
}
static int SendKeepAlive(void)
{
  int ilRc = 0;
  int ilRcgt = 0;
  int ilCurDev=0;
  for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
    {/* for 1*/
      if( rgDV.prDevPag[ilCurDev].iSocket > 0)
	{
	  memset(pcgResBuf,0x00,RES_BUF_SIZE);
	  ilRcgt= gtBuildCommand("1","gtTerminalStatusRequest","gtSYSTEM_TERMINAL",0,"","",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
	  dbg(DEBUG,"<SendKeepAlive> %05d gtTerminalStatusRequest: \n  ilRcgt=<%d>, ResultBuf <%s>",__LINE__,ilRcgt,pcgResBuf);
	  /*not connected or bad conection*/
	  if(ilRcgt==-1)
	    {
	      close(rgDV.prDevPag[ilCurDev].iSocket);
	      rgDV.prDevPag[ilCurDev].iSocket = 0;
	      rgDV.prDevPag[ilCurDev].pcDeviceState[0]='\0';
	      strcpy(rgDV.prDevPag[ilCurDev].pcDeviceState,"D");
	      dbg(TRACE,"KeepAlive found invalid Socket for Device %s",rgDV.prDevPag[ilCurDev].pcIP);
	      if((ilRc =SetDevState(rgDV.prDevPag[ilCurDev].pcIP,DOWN,rgDV.prDevPag[ilCurDev].pcDevUrno)) != RC_SUCCESS)
		{
		  dbg(TRACE,"SetDeviceState: SetDevState() returns <%d>!",ilRc);
		}
	    }
        }
    }
  return RC_SUCCESS;
}
/***************************************************/
/*RefreshDevice() handle all devices               */
/*                                                 */
/*                                                 */
/***************************************************/
static int RefreshDevice(int ipEvent,char* pcpTarget,char* pcpFields,char* pcpData,int ipCurDev)
{
  int ilRc = RC_SUCCESS;
  int ilCurDev = 0;
  int ilCurDevPag = 0;
  int ilCurPag = 0;
  int ilSocket = 0;
  int ilHit = 0;

  char pclCarouselBuf[XS_BUFF];
  time_t ilArrival = 0;
  time_t ilBagsum = 0;
  time_t ilChute = 0;
  time_t ilDeparture = 0;
  time_t ilBelt = 0;
  time_t ilCounter = 0;
  time_t ilDegauss = 0;
  time_t now = 0;
 
  ilArrival = igRefreshArrivalTimeBuff+igRefreshArrival;
  ilBagsum = igRefreshBagsumTimeBuff+igRefreshBagsum;
  ilChute = igRefreshChuteTimeBuff+igRefreshChute;
  ilDeparture = igRefreshDepartureTimeBuff+igRefreshDeparture;
  ilBelt = igRefreshBeltTimeBuff+igRefreshBelt;
  ilCounter = igRefreshCounterTimeBuff+igRefreshCounter;
  ilDegauss = igDegaussTimeBuff+ igDegaussIntervall;
  now = time(NULL);
  /*we need some data*/
  if(ipEvent == FALSE)
    {/*only timebased refreshes*/
      /*schedule degauss*/
      if(ilDegauss <= now&&igDegaussIntervall>0)
	{ 
	  igDegaussTimeBuff = now;
	  if((ilRc=SetDevLastDegaussTime())!=RC_SUCCESS)
	    {
	      dbg(DEBUG,"RefreshDevice:SetDevLastDegaussTime  failed with <%d>",ilRc);
	    }
	}
      if((ilRc = GetDataForPages(-1))!=RC_SUCCESS)
	{
	  dbg(DEBUG,"RefreshDevice: InitDatabaseFieldInfos failed with >%d>",ilRc);
	  ilRc=RC_SUCCESS;/*maybe there is nothing to refresh*/
	}
      for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
	{/*for 1 ilCurDev*/
	  /*only for connected sockets*/
	  if(rgDV.prDevPag[ilCurDev].iSocket > 0&& rgDV.prDevPag[ilCurDev].iAlarmFlag==FALSE)
	    {/*if Socket*/
	      if((!strcmp(rgDV.prDevPag[ilCurDev].pcFilterField,pcgCHECKIN)&&
		  strchr(rgDV.prDevPag[ilCurDev].pcFilterContents,0xb2)==NULL)||
		 (!strcmp(rgDV.prDevPag[ilCurDev].pcFilterField,pcgBELT)&&
		  strchr(rgDV.prDevPag[ilCurDev].pcFilterContents,0xb2)==NULL)||
		 (!strcmp(rgDV.prDevPag[ilCurDev].pcFilterField,pcgGATE)&&
		  strchr(rgDV.prDevPag[ilCurDev].pcFilterContents,0xb2)==NULL))
		{/*if"D"*/
		  if( ilCounter <= now&&igRefreshCounter>0)
		    {
		      igRefreshCounterTimeBuff = now;
		      if((ilRc=StartCounter(ilCurDev))!=RC_SUCCESS)
			{
			  dbg(DEBUG,"RefreshDevice: StartCounter failed with <%d>",ilRc);
			}
		      /* dbg(TRACE,"REFRESHCOUNTER DONE");*/
		    }
		}
	      for(ilCurDevPag = 0;ilCurDevPag< rgDV.prDevPag[ilCurDev].iNoOfPageInCarousel;ilCurDevPag++)
		{/*for 2 ilCurDevPag*/
		  /*just for good overview*/
		  /*ilCurDevPag <-> page in carousel*/
		  /*...piPagNo[...->reference to number in rgPG...*/
		  ilCurPag = rgDV.prDevPag[ilCurDev].piPagNo[ilCurDevPag];
		  ilSocket = rgDV.prDevPag[ilCurDev].iSocket;
		  if(strcmp(rgDV.prDevPag[ilCurDev].pcDisplayType[ilCurDevPag],"A")==0)
		    {/*if"A"*/ 
		      if(ilArrival <= now&&igRefreshArrival>0)
			{ 
			  igRefreshArrivalTimeBuff = now;
			  ilRc=UpdateTable(ilCurDev,ilCurDevPag,ilCurPag,ilSocket);
			  if(ilRc==RC_SUCCESS)
			    {
			      if(rgPG.prPages[ilCurPag].pcCarouselTime[0]!='\0'&&atoi(rgPG.prPages[ilCurPag].pcCarouselTime)>0)
				{
				  if(rgDV.prDevPag[ilCurDev].iCounterState==CLOSED)
				    {
				      memset(pclCarouselBuf,0x00,XS_BUFF);
				      strcpy(pclCarouselBuf,rgDV.prDevPag[ilCurDev].pcPageObjectId[ilCurDevPag]);
				      strcat(pclCarouselBuf,",");
				      strcat(pclCarouselBuf,rgPG.prPages[ilCurPag].pcCarouselTime);
				      gtBuildCommand("1","gtTerminalAddPageCarousel","gtSYSTEM_TERMINAL",2,pclCarouselBuf,"",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
				      gtBuildCommand("1","gtPageSetCurrent",rgDV.prDevPag[ilCurDev].pcPageObjectId[ilCurDevPag],1,"-1","",ilSocket,pcgResBuf);
				      /*  gtBuildCommand("1","gtCollectionRedraw",pclPageId,0,"","",ilSocket,pcgResBuf);  */
				      SetDefaultPage(FALSE,ilCurDev,TRUE);
				    }
				}
			      rgDV.prDevPag[ilCurDev].iCounterState=OPEND;
			    }
			  if(ilRc==RC_NOTFOUND)
			    {
			      ilRc=RC_SUCCESS;
			      SetDefaultPage(TRUE,ilCurDev,TRUE);
			    }
			}
		    }
		  /*baggagesummary*/
		  if(strcmp(rgDV.prDevPag[ilCurDev].pcDisplayType[ilCurDevPag],"U")==0)
		    {/*if"A"*/ 
		      if(ilBagsum <= now&&igRefreshBagsum>0)
			{
			  igRefreshBagsumTimeBuff = now;
			  ilRc=UpdateTable(ilCurDev,ilCurDevPag,ilCurPag,ilSocket);
			  if(ilRc==RC_SUCCESS)
			    {
			      if(rgPG.prPages[ilCurPag].pcCarouselTime[0]!='\0'&&atoi(rgPG.prPages[ilCurPag].pcCarouselTime)>0)
				{
				  if(rgDV.prDevPag[ilCurDev].iCounterState==CLOSED)
				    {
				      memset(pclCarouselBuf,0x00,XS_BUFF);
				      strcpy(pclCarouselBuf,rgDV.prDevPag[ilCurDev].pcPageObjectId[ilCurDevPag]);
				      strcat(pclCarouselBuf,",");
				      strcat(pclCarouselBuf,rgPG.prPages[ilCurPag].pcCarouselTime);
				      gtBuildCommand("1","gtTerminalAddPageCarousel","gtSYSTEM_TERMINAL",2,pclCarouselBuf,"",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
				      gtBuildCommand("1","gtPageSetCurrent",rgDV.prDevPag[ilCurDev].pcPageObjectId[ilCurDevPag],1,"-1","",ilSocket,pcgResBuf);
				      /*  gtBuildCommand("1","gtCollectionRedraw",pclPageId,0,"","",ilSocket,pcgResBuf);  */
				      SetDefaultPage(FALSE,ilCurDev,TRUE);
				    }
				}
			      rgDV.prDevPag[ilCurDev].iCounterState=OPEND;
			    }
			  if(ilRc==RC_NOTFOUND)
			    {
			      ilRc=RC_SUCCESS;
			      SetDefaultPage(TRUE,ilCurDev,TRUE);
			    }
			}
		    }
		  if(strcmp(rgDV.prDevPag[ilCurDev].pcDisplayType[ilCurDevPag],"H")==0)
		    {/*if"A"*/ 
		      if(ilChute <= now&&igRefreshChute>0)
			{
			  igRefreshChuteTimeBuff = now;
			  ilRc=UpdateTable(ilCurDev,ilCurDevPag,ilCurPag,ilSocket);
			  if(ilRc==RC_SUCCESS)
			    {
			      if(rgPG.prPages[ilCurPag].pcCarouselTime[0]!='\0'&&atoi(rgPG.prPages[ilCurPag].pcCarouselTime)>0)
				{
				  if(rgDV.prDevPag[ilCurDev].iCounterState==CLOSED)
				    {
				      memset(pclCarouselBuf,0x00,XS_BUFF);
				      strcpy(pclCarouselBuf,rgDV.prDevPag[ilCurDev].pcPageObjectId[ilCurDevPag]);
				      strcat(pclCarouselBuf,",");
				      strcat(pclCarouselBuf,rgPG.prPages[ilCurPag].pcCarouselTime);
				      gtBuildCommand("1","gtTerminalAddPageCarousel","gtSYSTEM_TERMINAL",2,pclCarouselBuf,"",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
				      gtBuildCommand("1","gtPageSetCurrent",rgDV.prDevPag[ilCurDev].pcPageObjectId[ilCurDevPag],1,"-1","",ilSocket,pcgResBuf);
				      /*  gtBuildCommand("1","gtCollectionRedraw",pclPageId,0,"","",ilSocket,pcgResBuf);  */
				      SetDefaultPage(FALSE,ilCurDev,TRUE);
				    }
				}
			      rgDV.prDevPag[ilCurDev].iCounterState=OPEND;
			    }
			  if(ilRc==RC_NOTFOUND)
			    {
			      ilRc=RC_SUCCESS;
			      SetDefaultPage(TRUE,ilCurDev,TRUE);
			    }
			}
		    }
		  if(strcmp(rgDV.prDevPag[ilCurDev].pcDisplayType[ilCurDevPag],"D")==0)
		    {/*if"D"*/
		      if( ilDeparture <= now&&igRefreshDeparture>0)
			{
			  igRefreshDepartureTimeBuff = now;
			  ilRc=UpdateTable(ilCurDev,ilCurDevPag,ilCurPag,ilSocket);
			  if(ilRc==RC_SUCCESS)
			    {
			      if(rgPG.prPages[ilCurPag].pcCarouselTime[0]!='\0'&&atoi(rgPG.prPages[ilCurPag].pcCarouselTime)>0)
				{
				  if(rgDV.prDevPag[ilCurDev].iCounterState==CLOSED)
				    {
				      memset(pclCarouselBuf,0x00,XS_BUFF);
				      strcpy(pclCarouselBuf,rgDV.prDevPag[ilCurDev].pcPageObjectId[ilCurDevPag]);
				      strcat(pclCarouselBuf,",");
				      strcat(pclCarouselBuf,rgPG.prPages[ilCurPag].pcCarouselTime);
				      gtBuildCommand("1","gtTerminalAddPageCarousel","gtSYSTEM_TERMINAL",2,pclCarouselBuf,"",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
				      gtBuildCommand("1","gtPageSetCurrent",rgDV.prDevPag[ilCurDev].pcPageObjectId[ilCurDevPag],1,"-1","",ilSocket,pcgResBuf);
				      /*  gtBuildCommand("1","gtCollectionRedraw",pclPageId,0,"","",ilSocket,pcgResBuf);  */
				      SetDefaultPage(FALSE,ilCurDev,TRUE);
				    }
				}
			      rgDV.prDevPag[ilCurDev].iCounterState=OPEND;
			    }
			  if(ilRc==RC_NOTFOUND)
			    {
			      ilRc=RC_SUCCESS;
			      SetDefaultPage(TRUE,ilCurDev,TRUE);
			    }
			}
		    }
		}
	    }
	}
      /*end of timebased refresh*/
      return ilRc;
    }
  /*event based refresh*/
  return RC_SUCCESS;
}
static int RefreshCounter(int ipEvent, char* pcpTarget, char* pcpFields, char* pcpData,int ipCurDev,int piDseq)
{
  int ilRc = RC_SUCCESS;
  int ilCurDev = 0;
  int ilCurDevPag = 0;
  int ilCurPag = 0;
  int ilSocket = 0;
  int ilHit = 0;
  ilCurDev = ipCurDev;
  if(rgDV.prDevPag[ilCurDev].iSocket>0&& rgDV.prDevPag[ilCurDev].iAlarmFlag == FALSE)
    {/*this is my connected device*/
      ilSocket = rgDV.prDevPag[ilCurDev].iSocket;
      /*what is it*/
      ilCurDevPag = 0;
      do{
	/*for 2 ilCurDevPag*/
	/*just for good overview*/
	/*ilCurDevPag <-> page in carousel*/
	/*...piPagNo[...->reference to number in rgPG...*/
	ilCurPag = rgDV.prDevPag[ilCurDev].piPagNo[ilCurDevPag];
	if(!strcmp(rgDV.prDevPag[ilCurDev].pcDisplayType[ilCurDevPag],"C"))
	  {/*it's a counter*/
	    ilRc=UpdateCounter(ilCurDev,ilCurDevPag,ilCurPag,ilSocket,pcpFields,pcpData,piDseq);
	  }
	if(!strcmp(rgDV.prDevPag[ilCurDev].pcDisplayType[ilCurDevPag],"G"))
	  {/*it's a gate*/
	    ilRc=UpdateGate(ilCurDev,ilCurDevPag,ilCurPag,ilSocket,pcpFields,pcpData,piDseq);
	  }
	if(!strcmp(rgDV.prDevPag[ilCurDev].pcDisplayType[ilCurDevPag],"L"))
	  {/*it's a Location*/
	    ilRc=UpdateLocation(ilCurDev,ilCurDevPag,ilCurPag,ilSocket);
	  }
	ilCurDevPag++;
	/*only one can hit!*/
      }while(ilRc!=RC_SUCCESS && ilCurDevPag< rgDV.prDevPag[ilCurDev].iNoOfPageInCarousel);/*end find page*/
      if(ilRc!=RC_SUCCESS)
        {
          SetDefaultPage(TRUE,ilCurDev,TRUE);
          if(igshow_event==TRUE)
            dbg(TRACE,"SetDefaultPage on %s %s -> %s done",
		rgDV.prDevPag[ilCurDev].pcFilterField,
		rgDV.prDevPag[ilCurDev].pcFilterContents,
		rgDV.prDevPag[ilCurDev].pcIP) ;
        }else{
	  if(igshow_event==TRUE)
            dbg(TRACE,"RefreshDevice: UpdateCounter done");
        }
    }else{
      /*every thing is ok but it's not my device or dev is not connected*/ 
      return RC_SUCCESS;
    }
  return RC_SUCCESS;
}
/*********************************************************************
Function : SendTimeSync()
Paramter :
Return Code: none
Result: 
Descrip tion: synchronizes all dZine devices with an UDP-broadcast of type
                         IDS_TYME_SYNC.
*********************************************************************/
static int SendTimeSync()
{
  int                 ilRc = 0;
  IDS_TIME_SYNC     rlTime;
  char                 pclIp[XS_BUFF];
  char                 pclTime[XS_BUFF];
  char                 pclSec[6];
  char                 pclMin[6];
  char                 pclHour[6];
  char                 pclDay[6];
  char                 pclMonth[6];
  char                 pclYear[6];
  unsigned char        pclNewTime[7];
  int                 ilYear = 0;
  int                           debug_level_save=0;/*workaround because tcp_send is corrupt (NULL in dbg)*/
  time_t             tlNow = 0;
  struct tm         *_tm;
  struct tm         rlTm;
  tlNow = time(NULL);
  /*  #if defined (_SOLARIS) */
  if (igDgramSock <= 0)
    {
      dsplib_close(igDgramSock);
      if ((ilRc = dsplib_socket(&igDgramSock,SOCK_DGRAM,pcgUdpBCService,0,FALSE)) == RC_FAIL )
	{
	  dbg(TRACE,"<ConnectDevice> %05d dsplib_socket returns: <%d>.", __LINE__, ilRc);
	}else{
	  dbg(DEBUG,"<ConnectDevice> %05d UDP-Socket = <%d>.", __LINE__, igDgramSock);
	}
    }
  memset(pclTime,0x00,XS_BUFF);
  memset((char*)&rlTime,0x00,sizeof(IDS_TIME_SYNC));
  rlTime.id = htonl(HEX_TIME_SYNC_ID);
  _tm = (struct tm*)localtime(&tlNow);
  rlTm = *_tm;  
  /* Unusal strftime format because of sccs !! */
  strftime(pclSec,6,"%" "S",&rlTm);
  strftime(pclMin,6,"%" "M",&rlTm);
  strftime(pclHour,6,"%" "H",&rlTm);
  strftime(pclDay,6,"%" "d",&rlTm);
  strftime(pclMonth,6,"%" "m",&rlTm);
  strftime(pclYear,6,"%" "Y",&rlTm);
  ilYear = (int)atoi(pclYear) - 1970;
  rlTime.currenttime.sec=(unsigned char)(atoi(pclSec));
  rlTime.currenttime.min=(unsigned char)(atoi(pclMin));
  rlTime.currenttime.hour=(unsigned char)(atoi(pclHour));
  rlTime.currenttime.day=(unsigned char)(atoi(pclDay));
  rlTime.currenttime.month=(unsigned char)(atoi(pclMonth));
  rlTime.currenttime.year=(unsigned char)ilYear;
  /*sprintf(pclIp,"%x",ntohl(uigBcAddr));*/
  if ((ilRc = tcp_send_datagram(igDgramSock,"FFFFFFFF",NULL,pcgUdpBCService, (char*)&rlTime,IDS_TIME_SYNC_SIZE)) != RC_SUCCESS)
    {
      dbg(DEBUG,"<SendTimeSync> tcp_send_datagram() returns <%d>", ilRc);
      snapit((char*)&rlTime,IDS_TIME_SYNC_SIZE,outp,"TimeSync");
    }
  dsplib_close(igDgramSock);
  igDgramSock=0;
  return RC_SUCCESS;
}


/***********************************************************************/
/*This function send's data to a counter device                        */
/***********************************************************************/
static int UpdateCounter(int ipCurDev,int ipCurDevPag,int ipCurPag,int ipSocket,char* pcpFields,char* pcpData,int piDseq )
{
  int ilRc = RC_SUCCESS;
  int ilRcFor=1;
  int ilRcDev=0;
  int ilId = 0;
  int ilCurObj = 0;
  int ilCurDev = 0;
  int ilCurDevPag = 0;
  int ilCurPag = 0;
  int ilCurRec = 0;
  int ilCurTabBuf = 0;
  int ilSocket = 0;
  int ilNoOfObjects = 0;
  int ilNextItem = 0 ;
  short slCursor = 0;
  short slFkt = 0;
  UDBXCommand *prlPageCommand=NULL;
  FieldDescription *prlFD;
  FieldDisplayType *prlDspType;
  FieldList *prlRecord;
  char *pclDspType=NULL;
  char pclObjectId[4];
  char pclPageId[4];
  char pclObjNumber[4];
  char pclTextPut[M_BUFF];
  char pclParameterBuf[XS_BUFF];
  char pclResult[M_BUFF];
  char pclCommand[M_BUFF];
  char pclBuildSelectBuf[M_BUFF];
  char pclTmpSqlAnswer[M_BUFF];
  char pclSqlBuf[M_BUFF];
  char pclTab[7];
  char pclFileBuf[XS_BUFF];
  char pclFindBuf[XS_BUFF];
  char pclFileName[XS_BUFF];
  char pclCarouselBuf[XS_BUFF];
  int ilExit=RC_FAIL;

  ilCurDev = ipCurDev;
  ilCurDevPag = ipCurDevPag;
  ilCurPag = ipCurPag;
  ilSocket = ipSocket;
 
  memset(pclSqlBuf,0x00,M_BUFF);
  memset(pclBuildSelectBuf,0x00,M_BUFF);
  memset(pclTmpSqlAnswer,0x00,M_BUFF);
  memset(pclObjectId,0x00,4);
  memset(pclResult,0x00,M_BUFF);
  memset(pclTab,0x00,7);
  memset(pclPageId,0x00,4);
  memset(pclObjectId,0x00,4);
  memset(pclParameterBuf,0x00,XS_BUFF);
  memset(pclFileBuf,0x00,XS_BUFF);
  memset(pclFileName,0x00,XS_BUFF);
  memset(pclObjNumber,0x00,4);
  memset(pcgResBuf,0x00,RES_BUF_SIZE);
  /*set pointer to page infos*/
  /* dbg(TRACE,"\nFields %s \nData %s",pcpFields,pcpData);  */
  if(rgDV.prDevPag[ilCurDev].iSocket==0)
    return 0;
  /*we need only the first*/
  prlRecord=&rgDV.prDevPag[ilCurDev].prRecord[0]; 
  if(prlRecord->iDisplayFlag==FALSE)
    {
      prlRecord->pcRecordset[0]='\0';
      prlRecord->pcUrno[0]='\0';
      /*nothing to display*/
      return RC_NOTFOUND;
    }
  if((ilRc=SelectData(prlRecord->pcFields, prlRecord->pcRecordset,ilCurPag,prlRecord->iDisplayFlag))!=RC_SUCCESS)
    {
      if(ilRc==RC_NOTFOUND)
	{
	  /*nothing to display*/
	  return RC_NOTFOUND;
	}
      /*wrong message*/
      return RC_SUCCESS;
    }
  if(igshow_event==TRUE)
    dbg(TRACE,"UpdateCounter: %s %s|CurDev:%d->IP:%s|CurDevPag:%d|CurPag:%d|Socket:%d",
	rgDV.prDevPag[ipCurDev].pcFilterField,
	rgDV.prDevPag[ipCurDev].pcFilterContents,
	ipCurDev,
	rgDV.prDevPag[ipCurDev].pcIP,
	ipCurDevPag,
	ipCurPag,
	ipSocket);
  sprintf(pclObjectId,"%d",ilCurPag);
  strcpy(pclPageId,rgDV.prDevPag[ilCurDev].pcPageObjectId[ilCurDevPag]);
  memset(pcgResBuf,0x00,RES_BUF_SIZE);
  gtBuildCommand("S","gtPageSetCurrent",pclPageId,1,"-1","",ilSocket,pcgResBuf);
  memset(pcgResBuf,0x00,RES_BUF_SIZE);
  if(igrefresh==TRUE)
    {
      gtBuildCommand("C","gtCollectionFreezeDrawing",pclPageId,0,"","",ilSocket,pcgResBuf);
    }
  ilNoOfObjects = field_count(rgPG.prPages[ilCurPag].pcTableObjNoList);
  /*all Objects are converted in item numbers*/
  ilRcDev=RC_SUCCESS;
  for(ilNextItem=0;ilNextItem<ilNoOfObjects&&ilRcFor>0;ilNextItem++)
    {/*for -> get next item*/ 
      memset(pclObjNumber,0x00,4);
      ilRcFor=GetDataItem(pclObjNumber,rgPG.prPages[ilCurPag].pcTableObjNoList,ilNextItem+1,',',"","");
      ilCurObj = atoi(pclObjNumber);
      memset(pclObjectId,0x00,4);
      prlPageCommand = rgPG.prPages[ipCurPag].prPagefile->prCommands[ilCurObj];
      strcpy(pclObjectId,prlPageCommand->pcObjectId);
      ilExit=RC_FAIL; 
      ilCurTabBuf=0;
      prlFD= prlPageCommand->prFieldDesc[0];
      if(strcmp(prlPageCommand->pcDZCommand,"gtGIFDefine")==0)
	{/*if "gtGIFDefine"*/
	  memset(pclParameterBuf,0x00,XS_BUFF);
	  memset(pclTextPut,0x00,M_BUFF);
	  memset(pclFileBuf,0x00,XS_BUFF);
	  memset(pclFindBuf,0x00,XS_BUFF);
	  memset(pclResult,0x00,M_BUFF);
	  memset(pclCommand,0x00,M_BUFF);
	  ilRc = SetPageData(ilCurPag,ilCurObj,prlRecord,pclCommand,pclTextPut,pclFileBuf,0);
	  if(strstr(rgDV.prDevPag[ilCurDev].pcLocationLogo,pclTextPut)==NULL)
	    { 
	      if(strlen(rgDV.prDevPag[ilCurDev].pcLocationLogo)+strlen(pclTextPut)+1< XS_BUFF)
		{
		  strcat(rgDV.prDevPag[ilCurDev].pcLocationLogo,pclTextPut);
		  rgDV.prDevPag[ilCurDev].iLogo++; 
		}else{
		  strcpy(rgDV.prDevPag[ilCurDev].pcLocationLogo,pclTextPut);
		  rgDV.prDevPag[ilCurDev].iLogo=1;
		}
	      strcpy(pclParameterBuf,"0,0,-1,");
	      strcat(pclParameterBuf,pclTextPut);
	      strcpy(pclFindBuf,"0,");
	      strcat(pclFindBuf,pclTextPut);
	      memset(pcgResBuf,0x00,RES_BUF_SIZE);
	      if(igshow_event==TRUE)
		dbg(TRACE,"UpdateCounter START SENDING %d. FILE <%s>",rgDV.prDevPag[ilCurDev].iLogo,pclTextPut);
	      ilRcDev=gtBuildCommand("E","gtPageSetCurrent",pclPageId,1,"-1","",ilSocket,pcgResBuf);
	      ilRcDev=gtBuildCommand("1","gtFilerPutFile","gtSYSTEM_FILER",4,pclParameterBuf,pclFileBuf,ilSocket,pcgResBuf);
	      memset(pclParameterBuf,0x00,XS_BUFF);
	      strcpy(pclParameterBuf,"-1,");
	      strcat(pclParameterBuf,pclTextPut); 
	      memset(pclFileName,0x00,XS_BUFF);
	      strcpy(pclFileName,"1,");
	      strcat(pclFileName,pclTextPut);
	      memset(pcgResBuf,0x00,RES_BUF_SIZE);
	      ilRc = gtBuildCommand("1","gtFilerPutData","gtSYSTEM_FILER",2,pclParameterBuf,pclFileBuf,ilSocket,pcgResBuf);
	      if(igshow_event==TRUE)
		dbg(TRACE,"UpdateCounter END SENDING FILE");
	      memset(pcgResBuf,0x00,RES_BUF_SIZE);
	      ilRcDev=gtBuildCommand("S",pclCommand,pclObjectId,1,pclTextPut,pclFileBuf,ilSocket,pcgResBuf);
	    }else{
	      memset(pcgResBuf,0x00,RES_BUF_SIZE);
	      ilRcDev=gtBuildCommand("C",pclCommand,pclObjectId,1,pclTextPut,pclFileBuf,ilSocket,pcgResBuf);
	    }
	  if(ilRcDev==RC_FAIL)
	    {
	      CloseDevice(ipCurDev);
	      return RC_SUCCESS;
	    }
	}else{
	  memset(pclTextPut,0x00,M_BUFF);
	  memset(pclFileBuf,0x00,XS_BUFF);
	  memset(pclResult,0x00,M_BUFF);
	  memset(pclCommand,0x00,M_BUFF);
	  ilRc = SetPageData(ilCurPag,ilCurObj,prlRecord,pclCommand,pclTextPut,pclFileBuf,0);
	  if(!strcmp(pclTextPut,"{ITEM}"))
	    {
	      strcpy(pclTextPut," {ITEM} ");
	    }
	  memset(pcgResBuf,0x00,RES_BUF_SIZE);
	  ilRcDev=gtBuildCommand("C",pclCommand,pclObjectId,1,pclTextPut,pclFileBuf,ilSocket,pcgResBuf);
	}
    }/*end for -> get next item*/
  memset(pcgResBuf,0x00,RES_BUF_SIZE);
  if(igrefresh==TRUE)
    {
      if(rgDV.prDevPag[ilCurDev].iAlarmFlag==TRUE)
	{
	 ilRcDev=gtBuildCommand("E","gtCollectionRefresh",pclPageId,1,"-1","",ilSocket,pcgResBuf);
	}else{ 
	 ilRcDev=gtBuildCommand("E","gtCollectionRedraw",pclPageId,0,"","",ilSocket,pcgResBuf);  
	}
    }
  if(ilRcDev==RC_FAIL)
    {
      CloseDevice(ipCurDev);
    }
  if(rgPG.prPages[ilCurPag].pcCarouselTime[0]!='\0'&&atoi(rgPG.prPages[ilCurPag].pcCarouselTime)>0&&rgDV.prDevPag[ilCurDev].iCounterState==CLOSED)
    {
      memset(pclCarouselBuf,0x00,XS_BUFF);
      strcpy(pclCarouselBuf,rgDV.prDevPag[ilCurDev].pcPageObjectId[ilCurDevPag]);
      strcat(pclCarouselBuf,",");
      strcat(pclCarouselBuf,rgPG.prPages[ilCurPag].pcCarouselTime);
      gtBuildCommand("1","gtTerminalAddPageCarousel","gtSYSTEM_TERMINAL",2,pclCarouselBuf,"",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
      gtBuildCommand("1","gtPageSetCurrent",pclPageId,1,"-1","",ilSocket,pcgResBuf);
      SetDefaultPage(FALSE,ilCurDev,TRUE);
    }
  rgDV.prDevPag[ilCurDev].iCounterState=OPEND;
  return RC_SUCCESS;
}
/*only pass throu*/
static int UpdateGate(int ipCurDev,int ipCurDevPag,int ipCurPag,int ipSocket,char* pcpFields,char* pcpData,int piDseq )
{
  int ilRc=RC_SUCCESS;
  char pclCarouselBuf[XS_BUFF];
  if((ilRc= UpdateCounter(ipCurDev,ipCurDevPag,ipCurPag,ipSocket,pcpFields,pcpData,piDseq))==RC_SUCCESS)
    {
    }
  return ilRc;
}

/***********************************************************/
/***********************************************************/
/*this function selects data for counter gate             **/
/*input data event from action                            **/
/*output all known tables for this URNO or CIKC           **/
/***********************************************************/
/***********************************************************/
static int SelectData(char* pcpFields,char* pcpData,int ipCurPag,int piDisplayFlag)
{
  int ilRc = RC_SUCCESS;
  int ilItemNo = 0;
  int ilCurPag=0;
  int ilCurTab=0;
  /*ATH*/
  int ilGateFlag= RC_FAIL;
  int ilNextFlight=RC_FAIL;
  int ilDelayedFlag=RC_FAIL;
  /*END ATH*/
  /*DXB*/
  int ilLogoFlag= RC_FAIL;
  int ilLogoFlight=RC_FAIL;
  /*END DXB*/
  char pclTmpBuf[XS_BUFF];
  char pclTmpBuf2[XS_BUFF];
  char pclTmpBuf3[XS_BUFF];
  char pclTmpBuf4[XS_BUFF];


  ilCurPag=ipCurPag;
  memset(pclTmpBuf,0x00,XS_BUFF);
  memset(pclTmpBuf2,0x00,XS_BUFF);  
#if defined (_DXB)|| defined (_TLL)
      ilItemNo = get_item_no(pcpFields,"FLG2",5);
      ilItemNo++;
      memset(pclTmpBuf2,0x00,XS_BUFF);
      GetDataItem(pclTmpBuf2,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTmpBuf2,cBLANK);
  /*dbg(TRACE,"pclTmpBuf2 <%s> ",pclTmpBuf2); */
      if(pclTmpBuf2[0]=='\0')
	{
	  ilLogoFlag=0;
	}else{
	  ilLogoFlag=1;
	}
#endif
#ifdef _ATH
      if(!strcmp(rgPG.prPages[ilCurPag].pcDisplayType,"C"))
	{
	  ilItemNo = get_item_no(pcpFields,"GTD1",5);
	  ilItemNo++;
	  memset(pclTmpBuf2,0x00,XS_BUFF);
	  GetDataItem(pclTmpBuf2,pcpData,ilItemNo,',',"","\0 ");
	  if(igshow_event==TRUE)
	    dbg(DEBUG,"Gate <%s> ",pclTmpBuf2); 
	  if(pclTmpBuf2[0]=='\0')
	    {
	      ilGateFlag=0;
	    }else{
	      ilGateFlag=1;
	    }
	}
      if(!strcmp(rgPG.prPages[ilCurPag].pcDisplayType,"G"))
	{
	  ilItemNo = get_item_no(pcpFields,"ETOD",5);
	  ilItemNo++;
	  memset(pclTmpBuf2,0x00,XS_BUFF);
	  GetDataItem(pclTmpBuf2,pcpData,ilItemNo,',',"","\0 ");
	  if(igshow_event==TRUE)
	    dbg(DEBUG,"Delayed <%s> ",pclTmpBuf2); 
	  if(pclTmpBuf2[0]=='\0')
	    {
	      ilDelayedFlag=0;
	    }else{
	      ilDelayedFlag=1;
	    }     
	}
#endif 

      ilItemNo=0;
      ilItemNo = get_item_no(pcpFields,"CTY1",5);
      ilItemNo++;
      memset(pclTmpBuf3,0x00,XS_BUFF);
      GetDataItem(pclTmpBuf3,pcpData,ilItemNo,',',"","");
      StringUPR((UCHAR*)pclTmpBuf3);
      if(igdebug_dzcommand)
        dbg(DEBUG,"<SelectData> %05d found counter <%s>",__LINE__,pclTmpBuf3);
#if defined (_DXB) || defined (_TLL)
  /*  dbg(TRACE,"SelectData %05d pclTmpBuf3 = <%s>pcNumberOfFlights = <%s>rgPG.prPages[ilCurPag].pcDisplayTypeInternal<%s>,LogoFlag %d",
      __ LINE__,
      pclTmpBuf3,
      rgPG.prPages[ilCurPag].pcNumberOfFlights,
      rgPG.prPages[ilCurPag].pcDisplayTypeInternal,
      ilLogoFlag);*/
  if(0 == strcmp(rgPG.prPages[ilCurPag].pcDisplayType,"C"))
    {
      if(strchr(pclTmpBuf3,'C')==NULL&&strcmp(rgPG.prPages[ilCurPag].pcNumberOfFlights,"1")==0)
        {
	  return RC_SUCCESS;
        }
      if(strchr(pclTmpBuf3,'C')!=NULL&&strcmp(rgPG.prPages[ilCurPag].pcNumberOfFlights,"0")==0&&ilLogoFlag==0&&
	 strstr(rgPG.prPages[ilCurPag].pcDisplaySeq,"C2")!=NULL)
        {
	  return RC_SUCCESS;
        }
      if(strchr(pclTmpBuf3,'C')!=NULL&&strcmp(rgPG.prPages[ilCurPag].pcNumberOfFlights,"0")==0&&ilLogoFlag==1&&
	 strstr(rgPG.prPages[ilCurPag].pcDisplaySeq,"C3")!=NULL)
        {
	  return RC_SUCCESS;
        }
    }

#endif
#ifdef  _DXB
#ifndef _TLL
  if(0 == strcmp(rgPG.prPages[ilCurPag].pcDisplayType,"G"))
    {
      if(igdebug_dzcommand)
	dbg(TRACE,"SelectData %05d pclTmpBuf3 = <%s>pcNumberOfFlights = <%s>", __LINE__,pclTmpBuf3,rgPG.prPages[ilCurPag].pcNumberOfFlights);

      if(strchr(pclTmpBuf3,'C')!=NULL&&strcmp(rgPG.prPages[ilCurPag].pcNumberOfFlights,"0")==0)
        {
	  return RC_SUCCESS;
        }
      
      if(strchr(pclTmpBuf3,'C')==NULL&&strcmp(rgPG.prPages[ilCurPag].pcNumberOfFlights,"1")==0)
        {
	  /*   dbg(TRACE,"SetPageData %05d", __LINE__); */
	  return RC_SUCCESS;
        }
    }
#endif
#endif
#ifdef _ATH    

  if(!strcmp(rgPG.prPages[ilCurPag].pcDisplayType,"C"))
    {
    
      if(strchr(pclTmpBuf3,'C')==NULL&&ilGateFlag==0&&!strcmp(rgPG.prPages[ilCurPag].pcDisplaySeq,"C1"))
        {
          return RC_SUCCESS;
        }
      if(strchr(pclTmpBuf3,'C')==NULL&&ilGateFlag==1&&!strcmp(rgPG.prPages[ilCurPag].pcDisplaySeq,"C2"))
        {
          return RC_SUCCESS;
        }
      if(strchr(pclTmpBuf3,'C')!=NULL&&!strcmp(rgPG.prPages[ilCurPag].pcDisplaySeq,"C3"))
        {
          return RC_SUCCESS;
        }
    }
   if(!strcmp(rgPG.prPages[ilCurPag].pcDisplayType,"G"))
    {
      if(!strcmp(rgPG.prPages[ilCurPag].pcDisplaySeq,"G1")&&ilDelayedFlag==0)
        {
          return RC_SUCCESS;
        }
      if(!strcmp(rgPG.prPages[ilCurPag].pcDisplaySeq,"G2")&&ilDelayedFlag==1)
        {
          return RC_SUCCESS;
        }
    }
#endif


  return RC_NOTFOUND;
}
static int StartCounter(int ipCurDev)
{
  int ilRc = RC_SUCCESS;
  int ilRcSql=RC_SUCCESS;
  int ilCurDev=0;
  int ilCurRec=0;
  int ilItemNo=0;
  short slCursor = 0;
  short slFkt = 0; 
  char pclFilterField[XS_BUFF];
  char pclFilterContents[128+1];
  char pclTmpSqlAnswer[L_BUFF];
  char pclSqlBuf[L_BUFF];
  char pclUrnoBuf[XS_BUFF];
  int  ilDseq;

  ilCurDev=ipCurDev;

  memset(pclSqlBuf,0x00,L_BUFF);
  memset(pclTmpSqlAnswer,0x00,M_BUFF);
  memset(pclFilterField,0x00,XS_BUFF);
  memset(pclFilterContents,0x00,128+1);
  memset(pclUrnoBuf,0x00,XS_BUFF);

  rgDV.prDevPag[ilCurDev].pcLocationLogo[0]='\0';
  rgDV.prDevPag[ilCurDev].iLogo=0;
  if(!strcmp(rgDV.prDevPag[ipCurDev].pcFilterField,pcgBELT))
    {
#ifdef ATH
      sprintf(pclSqlBuf,"select %s,flv2.dseq XYZ1 from flvtab flv1, flvtab flv2  where flv1.URNO > 0 and flv1.RNAM = '%s' and flv1.RTYP = '%s' and flv1.DSEQ > 0  and flv1.rnam=flv2.rnam and flv2.rtyp='BELT' and flv2.dseq=0 order by flv1.DSEQ",FLV1FIELDS,rgDV.prDevPag[ilCurDev].pcFilterContents,rgDV.prDevPag[ilCurDev].pcFilterField);
#else
      sprintf(pclSqlBuf,"select %s from flvtab where URNO > 0 and RNAM = '%s'and RTYP = '%s' and DSEQ > 0 order by DSEQ",pcgFLVFIELDS,rgDV.prDevPag[ilCurDev].pcFilterContents,rgDV.prDevPag[ilCurDev].pcFilterField);

#endif
    }else{
      sprintf(pclSqlBuf,"select %s from flvtab where URNO > 0 and RNAM = '%s'and RTYP = '%s' and DSEQ > 0 order by DSEQ",pcgFLVFIELDS,rgDV.prDevPag[ilCurDev].pcFilterContents,rgDV.prDevPag[ilCurDev].pcFilterField);
    }
  if(igdebug_selection==TRUE)
    dbg(TRACE,"<StartCounter> %05d <%s>",__LINE__,pclSqlBuf);
  slFkt = START; 
  slCursor = 0;
  ilRcSql=RC_SUCCESS;
  ilDseq=0;
  while(ilRcSql==RC_SUCCESS)
    {
      memset(pclTmpSqlAnswer,0x00,L_BUFF);
      if((ilRcSql = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer))!=RC_SUCCESS)
	{
	
	  if(ilRcSql == -1)
	    {
	      dbg(TRACE,"<StartCounter> sql_if in Line <%d> failed with <%d>",__LINE__,ilRc);
	    }
	  /*find location data*/
	  if(slFkt==START)
	    {
	      if(igdebug_selection==TRUE)
		dbg(TRACE,"<StartCounter> %05d FOUND NO FLIGHT ",__LINE__);
	      for(ilCurRec=0;ilCurRec<rgDV.prDevPag[ilCurDev].iNoOfRec;ilCurRec++)
		{
		  rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].pcFields[0]='\0';
		  rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].pcRecordset[0]='\0';
		  rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].pcUrno[0]='\0';
		  rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].iDisplayFlag=FALSE;
		}
	    }else{
#if 0
	      if((ilRc=RefreshCounter(TRUE,NULL,pcgFLVFIELDS,NULL,ilCurDev,0))!=RC_SUCCESS)
		{
		  /*this is not a fatal error*/ 
		  ilRc=RC_SUCCESS;
		}
#endif
	    }
	}else{
	  memset(pclUrnoBuf,XS_BUFF,0x00);
	  if(!strcmp(rgDV.prDevPag[ipCurDev].pcFilterField,pcgBELT))
	    {
	      BuildItemBuffer(pclTmpSqlAnswer," ",field_count(pcgFLVFIELDS)+1,",");
	    }else{
	      BuildItemBuffer(pclTmpSqlAnswer,pcgFLVFIELDS,0,",");
	    }
	  GetItem(pclUrnoBuf,pclTmpSqlAnswer,pcgFLVFIELDS,"URNO",TRUE);
	  if(igdebug_selection==TRUE)
	    dbg(TRACE,"<StartCounter> %05d FOUND FLIGHT <%s>",__LINE__,pclTmpSqlAnswer);
	  strcpy(rgDV.prDevPag[ilCurDev].prRecord[ilDseq].pcFields,pcgFLVFIELDS);
	  strcpy(rgDV.prDevPag[ilCurDev].prRecord[ilDseq].pcRecordset,pclTmpSqlAnswer);
	  strcpy(rgDV.prDevPag[ilCurDev].prRecord[ilDseq].pcUrno,pclUrnoBuf);
	  rgDV.prDevPag[ilCurDev].prRecord[ilDseq].iDisplayFlag=TRUE;
	  if(igdebug_selection==TRUE)
	    dbg(TRACE,"<StartCounter> %05d FOUND FDD URNO <%s>",__LINE__,rgDV.prDevPag[ilCurDev].prRecord[ilDseq].pcUrno);
	  ilDseq++;
	}
      slFkt=NEXT;
    }
  RefreshCounter(TRUE,NULL,pcgFLVFIELDS,pclTmpSqlAnswer,ilCurDev,ilDseq);
  close_my_cursor(&slCursor);
      
  return ilRc;
}
static int SelectLayout(int ipCurDev,int ipCurPag,int *pipNoOfFlights)
{
  int ilRc = RC_SUCCESS;
  int ilItemNo = 0;
  int ilCurPag=0;
  int ilCurDev=0;
  int ilCurRec=0;
  int ilFlightCnt=0;
  int ilNoOfFlights=0;
  int ilHit1 = FALSE;
  int ilNextItem = 0;
  ilCurPag=ipCurPag;
  ilCurDev=ipCurDev;

  if(rgDV.prDevPag[ilCurDev].pcFilterContents[0]=='\0')
    {
      return RC_FAIL;
    }
  ilNoOfFlights=atoi(rgPG.prPages[ilCurPag].pcNumberOfFlights);

  
  ilRc=RC_NOTFOUND;
  *pipNoOfFlights=0;

  for(ilCurRec=0;ilCurRec<rgDV.prDevPag[ilCurDev].iNoOfRec;ilCurRec++)
    {
      if(rgDV.prDevPag[ilCurDev].prRecord[ilCurRec].iDisplayFlag==TRUE)
	{
	  *pipNoOfFlights+=1;
	}
    }
  if(strstr(rgPG.prPages[ilCurPag].pcDisplaySeq,"B")!=NULL)
  {
    if(ilNoOfFlights == *pipNoOfFlights&&*pipNoOfFlights==1)
      {
	return RC_SUCCESS;
      }
    if( ilNoOfFlights > 1&&*pipNoOfFlights > 1)
      {
	return RC_SUCCESS;
      }
  }
  if(strstr(rgPG.prPages[ilCurPag].pcDisplaySeq,"G")!=NULL)
  {
   
    if( *pipNoOfFlights > 0)
      {
	return RC_SUCCESS;
      }
  }
  return RC_NOTFOUND;
}

static void DoNothing()
{
  int ilRc;
  int ilPrintOut=TRUE;
  dbg(TRACE,"\nDSPHDL SLEEPS NO DEVICE CONFIGURED\n");
  while(TRUE)
    {
      memset(prgItem,0x00,igItemLen);
      ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
      prgEvent = (EVENT *) prgItem->text;
                    
      if( ilRc == RC_SUCCESS )
    {
      /* Acknowledge the item */
      ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
      if( ilRc != RC_SUCCESS ) 
        {
          /* handle que_ack error */
          HandleQueErr(ilRc);
        } /* fi */
      switch( prgEvent->command )
        {
        case    HSB_STANDBY    :
          ctrl_sta = prgEvent->command;
          HandleQueues();
          break;    
        case    HSB_COMING_UP    :
          ctrl_sta = prgEvent->command;
          HandleQueues();
          break;    
        case    HSB_ACTIVE    :
          ctrl_sta = prgEvent->command;
          break;    
        case    HSB_ACT_TO_SBY    :
          ctrl_sta = prgEvent->command;
          /* CloseConnection(); */
          HandleQueues();
          break;    
        case    HSB_DOWN    :
          /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
          ctrl_sta = prgEvent->command;
          Terminate(1);
          break;    
        case    HSB_STANDALONE    :
          ctrl_sta = prgEvent->command;
          ResetDBCounter();
          break;    
        case    REMOTE_DB :
          /* ctrl_sta is checked inside */
          HandleRemoteDB(prgEvent);
          break;
        case    SHUTDOWN    :
          /* process shutdown - maybe from uutil */
          Terminate(1);
          break;
        case    RESET        :
           Terminate(1);
          break;
        case    EVENT_DATA    :
          if((ctrl_sta == HSB_STANDALONE) ||
         (ctrl_sta == HSB_ACTIVE) ||
         (ctrl_sta == HSB_ACT_TO_SBY))
        {
          if(ilPrintOut==TRUE)
            {
              dbg(TRACE,"NO DATA TO HANDLE DEVICE REQUIRED");
              ilPrintOut=FALSE;
            }  
        }
          break; 
        case    TRACE_ON :
          dbg_handle_debug(prgEvent->command);
          break;
        case    TRACE_OFF :
          dbg_handle_debug(prgEvent->command);
          break;
        default            :
          dbg(DEBUG,"MAIN: unknown event"); 
            DebugPrintItem(TRACE,prgItem); 
          DebugPrintEvent(TRACE,prgEvent);
          break;
        } /* end switch */
    }else{
      /* Handle queuing errors */
      HandleQueErr(ilRc);
    } /* end else */
    }/*end of loop*/
}

PEDTable *ReadPEDTable()
{
PEDTable *pTable;
PEDRec *pRec;
short sFct, sCursor;
char acRes[L_BUFF], acName[L_BUFF], acGrp[L_BUFF], acDesc[L_BUFF],
     acDef[L_BUFF], acLang[L_BUFF];

    pTable = PEDTable_Create();

    sFct = START;
    sCursor = 0;
    acRes[0] = '\0';

    while(sql_if(sFct, &sCursor, "SELECT SNAM,GNAM,DSCR,FSTR,LANG FROM PEDTAB", acRes) == RC_SUCCESS)
    {
        GetDataItem(acName, acRes, 1, '\0', "", "");
        GetDataItem(acGrp,  acRes, 2, '\0', "", "");
        GetDataItem(acDesc, acRes, 3, '\0', "", "");
        GetDataItem(acDef,  acRes, 4, '\0', "", "");
        GetDataItem(acLang, acRes, 5, '\0', "", "");
/*****
      dbg(TRACE, "ReadPEDTable: Nm=<%s> Grp=<%s> Desc=<%s> Def=<%s> Lang=<%s>\n",
    acName, acGrp, acDesc, acDef, acLang)              ;
*****/
        if ((pRec = PEDRec_CreateC(acName, acGrp, acDesc, acDef, acLang)))
            PEDTable_AddRec(pTable, pRec);
        else
            dbg(TRACE, "ReadPEDTable: bad record `%s��", acRes);
        sFct = NEXT;
    }

    close_my_cursor(&sCursor);

    return pTable;
}

static int SendGraphicsDir(void)
{
int ilRc=RC_SUCCESS;
int ilCurDev = 0;
int ilSocket;
char pclParameterBuf1[S_BUFF];
char pclParameterBuf2[S_BUFF];
char pclFileBuf[XS_BUFF];
DIR *prldirp;
struct dirent *prldirentp;
    if((prldirp = opendir(pcgPathToGif))!=NULL)
      {
        while((prldirentp = readdir(prldirp)) != NULL)
        {
            if(strstr(prldirentp->d_name,"_S.GIF")!=NULL)
            {
                 memset(pclParameterBuf1,0x00,S_BUFF);
                 /*send to nonvolatile storage*/
                 strcpy(pclParameterBuf1,"0,0,-1,");
                 strcat(pclParameterBuf1,prldirentp->d_name);
                 memset(pclParameterBuf2,0x00,XS_BUFF);
                 strcpy(pclParameterBuf2,"-1,");
                 strcat(pclParameterBuf2,prldirentp->d_name);
                 memset(pclFileBuf,0x00,XS_BUFF);
                 strcpy(pclFileBuf,pcgPathToGif);
		 strcat(pclFileBuf,prldirentp->d_name);
                 dbg(TRACE,"%s",pclFileBuf);
                 for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
                 {
                    if((ilSocket=rgDV.prDevPag[ilCurDev].iSocket) > 0)
                    {
                     dbg(TRACE,"Send File To <%s>",rgDV.prDevPag[ilCurDev].pcIP);
                     memset(pcgResBuf,0x00,RES_BUF_SIZE);
                     gtBuildCommand("1","gtFilerPutFile","gtSYSTEM_FILER",4,pclParameterBuf1,pclFileBuf,ilSocket,pcgResBuf);
                     memset(pcgResBuf,0x00,RES_BUF_SIZE);
                     gtBuildCommand("1","gtFilerPutData","gtSYSTEM_FILER",2,pclParameterBuf2,pclFileBuf,ilSocket,pcgResBuf);
                    }
                 }
            }
        }
      }
      closedir(prldirp);
      return (ilRc);
}

static int SendGraphics(void)
{
int ilRc=RC_SUCCESS;
int ilCurDev = 0;
int ilSocket;
char pclParameterBuf1[S_BUFF];
char pclParameterBuf2[S_BUFF];
char pclFileBuf[XS_BUFF];
DIR *prldirp;
struct dirent *prldirentp;
        if((prldirp = opendir(pcgPathToNewGif))!=NULL)
        {
            while((prldirentp = readdir(prldirp)) != NULL)
            {
             if(strstr(prldirentp->d_name,".GIF")!=NULL)
             {
                 memset(pclParameterBuf1,0x00,S_BUFF);
                 /*send to nonvolatile storage*/
                 strcpy(pclParameterBuf1,"0,0,-1,");
                 strcat(pclParameterBuf1,prldirentp->d_name);
                 memset(pclParameterBuf2,0x00,XS_BUFF);
                 strcpy(pclParameterBuf2,"-1,");
                 strcat(pclParameterBuf2,prldirentp->d_name);
                 memset(pclFileBuf,0x00,XS_BUFF);
                 strcpy(pclFileBuf,pcgPathToNewGif);
		 strcat(pclFileBuf,prldirentp->d_name);
                 dbg(TRACE,"%s",pclFileBuf);
                 for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
                 {
                    if((ilSocket=rgDV.prDevPag[ilCurDev].iSocket) > 0)
                    {
                     dbg(TRACE,"Send File To <%s>",rgDV.prDevPag[ilCurDev].pcIP);
                     memset(pcgResBuf,0x00,RES_BUF_SIZE);
                     gtBuildCommand("1","gtFilerPutFile","gtSYSTEM_FILER",4,pclParameterBuf1,pclFileBuf,ilSocket,pcgResBuf);
                     memset(pcgResBuf,0x00,RES_BUF_SIZE);
                     gtBuildCommand("1","gtFilerPutData","gtSYSTEM_FILER",2,pclParameterBuf2,pclFileBuf,ilSocket,pcgResBuf);
                    }
                 }
               }
            }
        }else{
                dbg(TRACE,"Dir %s does not exist, put right path in /ceda/conf/fids/%s.cfg \"PATH_TO_NEW_GIF = <PATH>\"",pcgPathToNewGif,mod_name);
        }
        closedir(prldirp);
        return (ilRc);
}

static int SetMonitorBrightness(void)
{
    int ilRc=RC_SUCCESS;
    int ilCurDev;
    short slFkt = START;
    short slCursor = 0;
    char pclSelectBuf[XS_BUFF];
    char pclTmpSqlAnswer[XS_BUFF];
    
    for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
    {

    slFkt = START; 
      slCursor = 0;
      sprintf(pclSelectBuf,"select dbrf from devtab where urno = '%s'",rgDV.prDevPag[ilCurDev].pcDevUrno);
      if((ilRc = sql_if(slFkt,&slCursor,pclSelectBuf,pclTmpSqlAnswer))!=RC_SUCCESS)
      close_my_cursor(&slCursor);
      strcpy(rgDV.prDevPag[ilCurDev].pcBrightness,pclTmpSqlAnswer);
      DeleteCharacterInString(rgDV.prDevPag[ilCurDev].pcBrightness,cBLANK);
      if(rgDV.prDevPag[ilCurDev].pcBrightness[0]!='\0')
      {
        ilRc=gtBuildCommand("1","gtMonitorBrightness","gtSYSTEM_MONITOR",1,rgDV.prDevPag[ilCurDev].pcBrightness,"",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);

      }
    }
    return ilRc;
}

static int SetInverseColor(void)
{
    int ilRc=RC_SUCCESS;
    int ilCurDev;
    for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
    {
        ilRc=gtBuildCommand("1","gtTerminalInverseImage","gtTERMINAL",1,"1","",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
    }
    return ilRc;
}
static int SetReverseColor(void)
{
    int ilRc=RC_SUCCESS;
    int ilCurDev;
    for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
    {
        ilRc=gtBuildCommand("1","gtTerminalInverseImage","gtTERMINAL",1,"0","",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
    }
    return ilRc;
}
static int PlayMpeg(char* pcpFields,char* pcpData)
{
    int ilRc=RC_SUCCESS;
    int ilCurDev;
    char pclUrno[XS_BUFF];
    char pclWhat[XS_BUFF];
    char pclMpegFile[XS_BUFF];
    char pclObject[XS_BUFF];
    char pclRep[XS_BUFF];
    char pclPage[XS_BUFF];
    char pclParameter[S_BUFF];

    memset(pclUrno,0x00,XS_BUFF);
    memset(pclWhat,0x00,XS_BUFF);
    memset(pclMpegFile,0x00,XS_BUFF);
    memset(pclObject,0x00,XS_BUFF);
    memset(pclRep,0x00,XS_BUFF);
    memset(pclPage,0x00,XS_BUFF);
    memset(pclParameter,0x00,S_BUFF);
   
    /* WHAT,URNO,DADR,DEVN,MPFN,PGNO,OBJN,REPC */
    GetDataItem(pclUrno,pcpData,2,',',"","\0 ");
    GetDataItem(pclWhat,pcpData,1,',',"","\0 ");
    GetDataItem(pclMpegFile,pcpData,5,',',"\0 ","");
    GetDataItem(pclObject,pcpData,7,',',"","");
    GetDataItem(pclRep,pcpData,8,',',"","");
    GetDataItem(pclPage,pcpData,6,',',"","");
    if(igshow_event==TRUE)
      {
	dbg(TRACE,"Mpeg Player Parameters <%s>",pcpData);
      }
    for(ilCurDev = 0;ilCurDev<rgDV.iNoOfDevices;ilCurDev++)
    {
      if(atoi(rgDV.prDevPag[ilCurDev].pcDevUrno)==atoi(pclUrno))
        {
           memset(pcgResBuf,0x00,RES_BUF_SIZE);
           if(!strcmp(pclWhat,"STOP"))
           {
                if(igshow_event==TRUE)
                {
                  dbg(TRACE,"Stop Mpeg <%s> on Device %s",pclMpegFile,rgDV.prDevPag[ilCurDev].pcIP);
                }
             gtBuildCommand("1","gtMpegVideoStop","gtSYSTEM_MPEG_OBJECT",1,"1","",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
           }else{
	     if(igshow_event==TRUE)
	       {
		 dbg(TRACE,"Play Mpeg <%s> on Device %s",pclMpegFile,rgDV.prDevPag[ilCurDev].pcIP);
	       }
           sprintf(pclParameter,"-1,%s,%s,gtMPEG_VIDEO_CLOSE_AT_END,-1,-1",pclRep,pclMpegFile);
	   gtBuildCommand("1","gtMpegVideoPlay","gtSYSTEM_MPEG_OBJECT",6,pclParameter,"",rgDV.prDevPag[ilCurDev].iSocket,pcgResBuf);
           }
         }
     }
     return RC_SUCCESS;
}
static int ScanFile(void)
{
  int ilRc=RC_SUCCESS;
  FILE *fh=NULL;

  fh = (FILE *)fopen("/ceda/conf/fids/file/FREETEXT.TXT","r"); /* open file and read it */
  if (fh == NULL)
    {

      pcgFileText[0]='\0';
      return RC_NOT_FOUND;
    }
  fscanf(fh,"%[^\n]%*c",pcgFileText);
  /*fread(pcgFileText,(size_t)1,(size_t)16*1024,fh); */
  fclose(fh);
  return RC_SUCCESS;
}

static void TrimRight(char *pcpBuffer)
{
    char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];
    if (strlen(pcpBuffer) == 0)
    {
       strcpy(pcpBuffer," ");
    }
    else
    {
       while(isspace(*pclBlank) && pclBlank != pcpBuffer)
       {
          *pclBlank = '\0';
          pclBlank--;
       }
    }
}/* end of TrimRight*/

void DeleteLeadingZero(char *pcpTarget)

{
  int d_len;
  int j;
  char pclDest[M_BUFF];
  if(strlen(pcpTarget)> M_BUFF)
    return ;

  strcpy(pclDest,pcpTarget);
  d_len = strlen(pclDest);
  j=0;
  while ('0'== pclDest[j]&&pclDest[j]!='\0')
    j++;
  
  strcpy(pcpTarget,&pclDest[j]);
                             /* end for */
}           

static int GetItem(char* pcpTarget,char* pcpData,char* pcpFieldlist,char* pcpField,int piDeleteBlanks)
{
int ilRc = RC_FAIL;
int ilItemNo=0;

    ilItemNo=get_item_no(pcpFieldlist,pcpField,5);
    ilItemNo++;
    ilRc=GetDataItem(pcpTarget,pcpData,ilItemNo,',',"","");
    if(piDeleteBlanks==TRUE)
    {
        DeleteCharacterInString(pcpTarget,cBLANK);
    }
  return ilRc;
}

static void GetConfig(char* pcpFile,char* pcpSection,char* pcpTag,char* pcpTarget,char* pcpDefault)
{
int ilRc = RC_FAIL;
char pclTmp[L_BUFF];
memset(pclTmp,0x00,L_BUFF);
if ((ilRc = iGetConfigEntry(pcpFile,pcpSection,pcpTag,CFG_STRING,pclTmp)) == RC_SUCCESS)
    {
      strcpy(pcpTarget,pclTmp);
      if(igshow_config==TRUE)
        dbg(TRACE,"GetCfgEntry %s \tis <%s>.",pcpTag,pclTmp);
    }else{
      strcpy(pcpTarget,pcpDefault);
      if(igshow_config==TRUE)
        dbg(TRACE,"GetCfgEntry set %s \tto default<%s>.",pcpTag,pcpTarget);
    }
}

static void GetConfigSwitch(char* pcpFile,char* pcpSection,char* pcpTag,int* piTarget,int piDefault)
{
  int ilRc = RC_FAIL;
  char pclTmp[L_BUFF];

  memset(pclTmp,0x00,L_BUFF);
  if(strstr(pcpTag,"MODE")!=NULL)
    {
      if ((ilRc = iGetConfigEntry(pcpFile,pcpSection,pcpTag,CFG_STRING,pclTmp)) == RC_SUCCESS)
	{
	  if(!strcmp(pclTmp,"DEBUG"))
	    {
	      *piTarget = DEBUG;
	    }else if(!strcmp(pclTmp,"TRACE")){
	      *piTarget = TRACE;
	    }else if(NULL!=strstr(pclTmp,"OF")){
	      *piTarget = 0;
	    }
	  dbg(TRACE,"GetCfgEntry %s is <%d>.",pcpTag,*piTarget);
	}else{
	  *piTarget = piDefault;
	  if(igshow_config==TRUE)
	    dbg(TRACE,"GetCfgEntry set %s to %d.",pcpTag,*piTarget);
	}
    }else{
      if ((ilRc = iGetConfigEntry(pcpFile,pcpSection,pcpTag,CFG_STRING,pclTmp)) == RC_SUCCESS)
	{
	  if(!strcmp(pclTmp,"ON")||!strcmp(pclTmp,"TRUE"))
	    {
	      *piTarget = TRUE;
	    }else{
	      *piTarget = FALSE;
	    }
	  dbg(TRACE,"GetCfgEntry %s is <%d>.",pcpTag,*piTarget);
	}else{
	  *piTarget = piDefault;
	  if(igshow_config==TRUE)
	    dbg(TRACE,"GetCfgEntry set %s to %d.",pcpTag,*piTarget);
	}
    }
}
static void GetConfigValue(char* pcpFile,char* pcpSection,char* pcpTag,int* piTarget,int piDefault)
{
  int ilRc = RC_FAIL;
  char pclTmp[L_BUFF];

  memset(pclTmp,0x00,L_BUFF);
  if ((ilRc = iGetConfigEntry(pcgConfFile,pcpSection,pcpTag,CFG_STRING,pclTmp)) == RC_SUCCESS)
    {
      *piTarget = (time_t)atoi(pclTmp);
      if(igshow_config==TRUE)
	dbg(TRACE,"GetCfgEntry %s \t\tis <%d>",pcpTag,*piTarget);
    }else{
      *piTarget = piDefault;
      if(igshow_config==TRUE)
	{
	  if(strlen(pcpTag)>10)
	    {
	      dbg(TRACE,"GetCfgEntry set %s \tto default<%d>",pcpTag,*piTarget);
	    }else{
	      dbg(TRACE,"GetCfgEntry set %s \t\tto default<%d>",pcpTag,*piTarget);
	    }
	}
    }
}


static int UpdateTable(int ipCurDev,int ipCurDevPag,int ipCurPag,int ipSocket)
{
  int ilRc = RC_SUCCESS;
  int ilRow = 0;
  int ilColumn = 0;                
  int ilId = 0;
  int ilStartId = 0;
  int ilCurObj = 0;
  int ilCurDev = 0;
  int ilCurDevPag = 0;
  int ilCurPag = 0;
  int ilSocket = 0;
  int ilPacketCnt=0;
  int ilPacket=0;
  int ilNoOfFlights=1;
  int ilCurRowInCluster=1;
  int ilClusterOffset = 0;
  int ilRowInSelection;
  int ilFilterFlag=0;
  int ilIndoFlag=0;
  int ilAirlineFlag=0;
  int ilDeviceAreaFlag=0;
  int ilStartFlag=0;
  int ilItemNo = 0;
  int ilTerm = 0;
  int ilAirl=0;
  int ilDevArea=0;
  int ilDseq;
  int ilRowCnt=0;
  int ilHit=FALSE;
  int ilStartCodeShare;
  int ilTableRowsNowSave=0;
  UDBXCommand *prlPageCommand=NULL;
  FieldDescription *prlFD;
  FieldDisplayType *prlDspType;
  FieldList *prlRecord;
  char pclObjectId[4];
  char pclPageId[4];
  char pclObjNumber[4];
  char pclTextPut[M_BUFF];
  char pclParameterBuf[S_BUFF];
  char pclResult[M_BUFF];
  char pclCommand[M_BUFF];
  char pclFileBuf[XS_BUFF];
  char pclFilterField[XS_BUFF];
  char pclFieldContents[128+1];
  char pclTerminal[XS_BUFF];
  char pclAirline[XS_BUFF];
  char pclDeviceArea[XS_BUFF];
  char pclDseq[XS_BUFF];
  int ilCodeshareCnt=0;
  int ilCodeshare=0;
  int ilRcDev=0;
  ilCurPag=ipCurPag;
  ilRowCnt=0;
  ilNoOfFlights=atoi(rgPG.prPages[ipCurPag].pcNumberOfFlights);
  memset(pclTerminal,0x00,XS_BUFF);
  memset(pclFieldContents,0x00,XS_BUFF);
  memset(pclDeviceArea,0x00,XS_BUFF);
  memset(pclAirline,0x00,XS_BUFF);

  if(!strcmp(rgPG.prPages[ipCurPag].pcDisplayTypeInternal,"S"))
    {
      if(rgPG.prPages[ipCurPag].iNoOfObjPerRec>0)
	{
	  /*Staff pages in carousel with more flights*/
	  ilCurRowInCluster= rgPG.prPages[ipCurPag].iNoOfObjPerRec;
	}else{
	  ilCurRowInCluster=atoi(rgDV.prDevPag[ipCurDev].pcRowInCluster);
	}
    }else{ 
      if(atoi(rgDV.prDevPag[ipCurDev].pcRowInCluster)>0)
	{ 
	  ilCurRowInCluster=atoi(rgDV.prDevPag[ipCurDev].pcRowInCluster);
	}else{
	  ilCurRowInCluster=1;
	}
    }
  ilClusterOffset=ilNoOfFlights*(ilCurRowInCluster-1);
  ilTableRowsNowSave=rgPG.prPages[ipCurPag].iTableRowsNow;
  if(igPacketMax>0)
    {
      ilPacket=ilNoOfFlights/igPacketMax;
    }
  /*dbg(TRACE,"UpdateTable start");*/
  if(igshow_event==TRUE)
    {
      dbg(TRACE,"UpdateTable:CurDev:%d->IP:%s|CurDevPag:%d|CurPag:%d|Socket:%d|Cluster Row:%d|FlightsNow %d",
	  ipCurDev,
	  rgDV.prDevPag[ipCurDev].pcIP,
	  ipCurDevPag,
	  ipCurPag,
	  ipSocket,
	  ilCurRowInCluster,
	  rgPG.prPages[ipCurPag].iTableRowsNow);
    }
  if(ipCurPag >= 0&& ipSocket> 0)
    {
      /*if 1*/ 
      ilSocket = ipSocket;
      ilCurPag = ipCurPag;
      ilCurDevPag = ipCurDevPag;
      ilCurDev=ipCurDev;
      ilFilterFlag=FALSE;
      ilIndoFlag=FALSE;
      ilDeviceAreaFlag=FALSE;
      ilAirlineFlag=FALSE;
      if(rgDV.prDevPag[ilCurDev].pcFilterField[0]!='\0' && rgDV.prDevPag[ilCurDev].pcFilterContents[0] !='\0')
	{
	  ilFilterFlag=TRUE;
	}
      if(rgDV.prDevPag[ilCurDev].pcTerminal[0]!='\0')
	{
	  ilIndoFlag=TRUE;
	}    
      if(rgDV.prDevPag[ilCurDev].pcAirline[0]!='\0') 
	{
	  ilAirlineFlag=TRUE;
	}    
      if(rgDV.prDevPag[ilCurDev].pcDeviceArea[0]!='\0')
	{
	  ilDeviceAreaFlag=TRUE;
	}
      if((ilIndoFlag==TRUE||
	  ilFilterFlag==TRUE||
	  ilDeviceAreaFlag==TRUE||
	  ilAirlineFlag==TRUE)
	 &&rgPG.prPages[ilCurPag].iTableRowsNow>0)
        {
	  /*Filter is available*/
	  /*if(ilClusterOffset>0)
	    {
	    dbg(TRACE,"UpdateTable %05d Found ClusterOffset = %d",__LINE__,ilClusterOffset);
	    }*/
	  strcpy(pclFilterField,rgDV.prDevPag[ipCurDev].pcFilterField);
	  ilItemNo = get_item_no(rgPG.prPages[ipCurPag].pcFieldsFromSelection,pclFilterField,5);
	  ilItemNo++;
	  ilTerm=get_item_no(rgPG.prPages[ipCurPag].pcFieldsFromSelection,"INDO",5);
	  ilTerm++;
	  ilAirl=get_item_no(rgPG.prPages[ipCurPag].pcFieldsFromSelection,"FLNO",5);
	  ilAirl++;
	  ilDseq=get_item_no(rgPG.prPages[ipCurPag].pcFieldsFromSelection,"DSEQ",5);
	  ilDseq++;
	  ilDevArea=get_item_no(rgPG.prPages[ipCurPag].pcFieldsFromSelection,"DACO",5);
	  ilDevArea++;
	  if(igshow_event==TRUE)
	    dbg(TRACE,"UpdateTable:Found Filter=<%s>-><%s>,Displaytype <%s>,Terminal <%s>,Airline <%s>,Area <%s>",
		pclFilterField,
		rgDV.prDevPag[ilCurDev].pcFilterContents,
		rgDV.prDevPag[ilCurDev].pcDisplayType[ipCurDevPag],
		rgDV.prDevPag[ilCurDev].pcTerminal,
		rgDV.prDevPag[ilCurDev].pcAirline,
		rgDV.prDevPag[ilCurDev].pcDeviceArea);
	  ilRowCnt=0;
	  ilStartFlag=FALSE;
	  for(ilRow = 0;ilRow < rgPG.prPages[ilCurPag].iTableRowsNow;ilRow++)
	    {
	      ilHit=TRUE;
	      rgPG.prPages[ilCurPag].prPageTable[ilRow].iDisplayFlag=FALSE;
	      pclTerminal[0]='\0';
	      GetDataItem(pclTerminal,rgPG.prPages[ilCurPag].prPageTable[ilRow].pcRecordset,ilTerm,',',"","");
	      pclFieldContents[0]='\0';
	      GetDataItem(pclFieldContents,rgPG.prPages[ilCurPag].prPageTable[ilRow].pcRecordset,ilItemNo,','," ","\0 ");
	      pclAirline[0]='\0';
	      GetDataItem(pclAirline,rgPG.prPages[ilCurPag].prPageTable[ilRow].pcRecordset,ilAirl,',',"","");
	      pclDeviceArea[0]='\0';
	      GetDataItem(pclDeviceArea,rgPG.prPages[ilCurPag].prPageTable[ilRow].pcRecordset,ilDevArea,',',"","");
	      pclDseq[0]='\0';
	      GetDataItem(pclDseq,rgPG.prPages[ilCurPag].prPageTable[ilRow].pcRecordset,ilDseq,',',"","");
	      /*   dbg(TRACE,"Record <%s><%s>",rgPG.prPages[ilCurPag].prPageTable[ilRow].pcRecordset,pclFieldContents);   */
	      if((strstr(rgDV.prDevPag[ilCurDev].pcFilterContents,pclFieldContents)==NULL&&
		  strstr(pclFieldContents,rgDV.prDevPag[ilCurDev].pcFilterContents)==NULL)
		 &&ilFilterFlag==TRUE)
		{
		  ilHit=FALSE;
		}
	      if(strstr(pclTerminal,rgDV.prDevPag[ilCurDev].pcTerminal)==NULL&&ilIndoFlag==TRUE)
		{
		  ilHit=FALSE;
		}
	      if(rgDV.prDevPag[ilCurDev].iInvertAirline==FALSE)
		{
		  if(strstr(pclAirline,rgDV.prDevPag[ilCurDev].pcAirline)==NULL&&ilAirlineFlag==TRUE)
		    {
		      if(atoi(pclDseq)>1)
			{
			  /*maybe its a code share -> check carrier*/
			  pclAirline[0]='\0';
			  GetDataItem(pclAirline,rgPG.prPages[ilCurPag].prPageTable[ilRow-atoi(pclDseq)+1].pcRecordset,ilAirl,',',"","");
			  if(strstr(pclAirline,rgDV.prDevPag[ilCurDev].pcAirline)==NULL)
			    {
			      ilHit=FALSE;
			    }
			}else{
			  ilHit=FALSE;
			}
		    }
		}else{
		  if(strstr(pclAirline,rgDV.prDevPag[ilCurDev].pcAirline)!=NULL&&ilAirlineFlag==TRUE)
		    {
		      ilHit=FALSE;
		    }else{
		      if(atoi(pclDseq)>1&&ilAirlineFlag==TRUE)
			{
			  /*maybe its a code share -> check carrier*/
			  pclAirline[0]='\0';
			  GetDataItem(pclAirline,rgPG.prPages[ilCurPag].prPageTable[ilRow-atoi(pclDseq)+1].pcRecordset,ilAirl,',',"","");
			  if(strstr(pclAirline,rgDV.prDevPag[ilCurDev].pcAirline)!=NULL)
			    {
			      ilHit=FALSE;
			    }
			}
		    }
		}
	      if(strstr(pclDeviceArea,rgDV.prDevPag[ilCurDev].pcDeviceArea)==NULL&&ilDeviceAreaFlag==TRUE)
		{
		  ilHit=FALSE;
		}
	      if(ilHit==TRUE)
		{
		  rgPG.prPages[ilCurPag].prPageTable[ilRow].iDisplayFlag=TRUE;
		  /*dbg(DEBUG,"ALL FILTERS ARE TRUE at row %d, row to display %d",ilRow,ilRowCnt);*/
		  ilRowCnt++;
		}
	      if(ilRowCnt-1==ilClusterOffset&&ilStartFlag==FALSE)
		{
		  ilStartFlag=TRUE;
		  ilClusterOffset=ilRow;
		  /*dbg(TRACE,"UpdateTable %05d Found ClusterOffset For Filter= %d",__LINE__,ilClusterOffset); */
		}
	    }
	  if(ilRowCnt==0||ilStartFlag==FALSE)
	    {
	      ilRc=RC_NOTFOUND;
	      if(igshow_event==TRUE)
		dbg(TRACE,"No Data Found");
	      rgPG.prPages[ilCurPag].iTableRowsNow=ilTableRowsNowSave;
	     /*   if(rgDV.prDevPag[ilCurDev].iCounterState==CLOSED) */
		return RC_NOTFOUND;
	    }
	}
      if(rgPG.prPages[ilCurPag].iTableRowsNow==0)
        {
	  ilRc=RC_NOTFOUND;
          if(igshow_event==TRUE)
            dbg(TRACE,"No Data Found");
	 /*   if(rgDV.prDevPag[ilCurDev].iCounterState==CLOSED) */
	    return RC_NOTFOUND;
        }
      memset(pclObjectId,0x00,4);
      memset(pclResult,0x00,M_BUFF);
      memset(pclPageId,0x00,4);
      memset(pclObjectId,0x00,4);
      memset(pclParameterBuf,0x00,XS_BUFF);
      memset(pclFileBuf,0x00,XS_BUFF);
      /*set first table objekt and number of columnes*/
      ilStartId = atoi(rgPG.prPages[ilCurPag].pcTableFirstId);
      sprintf(pclObjectId,"%d",ilCurPag);
      strcpy(pclPageId,rgDV.prDevPag[ilCurDev].pcPageObjectId[ilCurDevPag]);
      /*21.03.2001*/
      gtBuildCommand("1","gtPageSetCurrent",pclPageId,1,"-1","",ilSocket,pcgResBuf); 
      memset(pcgResBuf,0x00,RES_BUF_SIZE);
      if(igrefresh==TRUE)
	{
	  gtBuildCommand("1","gtCollectionFreezeDrawing",pclPageId,0,"","",ilSocket,pcgResBuf);
	}
      ilRowCnt=0;
      ilPacketCnt=0;
      ilRcDev=0;
      for(ilRow=0;ilRow < rgPG.prPages[ilCurPag].iTableRowsMax;ilRow++)	
	{
	  ilRowInSelection=0;          
	  /*now we need the offset for data in cluster x*/
	  if(ilFilterFlag==FALSE&&ilIndoFlag==FALSE&&ilAirlineFlag==FALSE&&ilDeviceAreaFlag==FALSE)
	    {
	      ilRowInSelection= ilRow+ilClusterOffset;
	      if(ilRowInSelection<rgPG.prPages[ilCurPag].iTableRowsNow)
		{
		  ilHit=TRUE;
		}else{
		  ilHit=FALSE;
		}
	     
	    }
	  if(ilFilterFlag==TRUE||ilIndoFlag==TRUE||ilAirlineFlag==TRUE||ilDeviceAreaFlag==FALSE)
	    {
	      ilRowInSelection = ilRowCnt+ilRow+ilClusterOffset;
	      ilHit=FALSE;
	      if(ilRowInSelection<rgPG.prPages[ilCurPag].iTableRowsNow)
		{
		  if(rgPG.prPages[ilCurPag].prPageTable[ilRowInSelection].iDisplayFlag==TRUE)
		    {
		      ilHit=TRUE;
		    }else{
		      /*skip*/
		      while(rgPG.prPages[ilCurPag].prPageTable[ilRowInSelection].iDisplayFlag==FALSE&&
			    ilRowInSelection<rgPG.prPages[ilCurPag].iTableRowsNow-1)
			{
			  /*   dbg(DEBUG,"UpdateTable Offset in cluster is <%d>",ilRowInSelection); */
			  ilRowCnt++;
			  ilRowInSelection = ilRowCnt+ilRow+ilClusterOffset;
			  if(rgPG.prPages[ilCurPag].prPageTable[ilRowInSelection].iDisplayFlag==TRUE)
			    {
			      /*  			      dbg(DEBUG,"UpdateTable Offset in cluster is <%d>",ilRowInSelection); */
			      ilHit=TRUE;
			    }
			}
		    }
		}else{
		  ilHit=FALSE;
		}
	    }
	  if(ilPacketCnt==ilPacket)
	    {
	      ilPacketCnt=0;
	    }
	  if(ilPacketCnt==0&&igPacketMax!=-1)
	    {
	      gtBuildCommand("S","gtPageSetCurrent",pclPageId,1,"-1","",ilSocket,pcgResBuf);
	    } 
	  prlRecord=&rgPG.prPages[ilCurPag].prPageTable[ilRowInSelection];
	  if(ilHit==TRUE)   
	    {
	      if(igdebug_dzcommand)
		dbg(TRACE,"======================= %d. ROW =========================",ilRow+1);
	      for(ilColumn = 0;ilColumn < rgPG.prPages[ilCurPag].iTableColumns;ilColumn++)
		{
		  memset(pclObjNumber,0x00,4);
		  GetDataItem(pclObjNumber,rgPG.prPages[ilCurPag].pcTableObjNoList,ilColumn+1,',',"","");
		  ilCurObj = atoi(pclObjNumber);
		  ilId = (ilRow*rgPG.prPages[ilCurPag].iTableColumns)+ilColumn+ilStartId;
		  memset(pclObjectId,0x00,4);
		  sprintf(pclObjectId,"%d",ilId);
		  if(igdebug_dzcommand&&debug_level==DEBUG)
		    { 
		      pclTerminal[0]='\0';
		      GetDataItem(pclTerminal,prlRecord->pcRecordset,ilTerm,',',"","");
		      pclFieldContents[0]='\0';
		      GetDataItem(pclFieldContents,prlRecord->pcRecordset,ilItemNo,','," ","\0 ");
		      pclAirline[0]='\0';
		      GetDataItem(pclAirline,prlRecord->pcRecordset,ilAirl,',',"","");
		      pclDeviceArea[0]='\0';
		      GetDataItem(pclDeviceArea,prlRecord->pcRecordset,ilDevArea,',',"","");
		      pclDseq[0]='\0';
		      GetDataItem(pclDseq,prlRecord->pcRecordset,ilDseq,',',"","");
		      dbg(DEBUG,"ObjId:<%s>|Filter:<%s>|AREA:<%s>|TERMINAL:<%s>|AIRLINE:<%s>CODESHARE:<%s>"
			  ,pclObjectId
			  ,pclFieldContents
			  ,pclDeviceArea
			  ,pclTerminal
			  ,pclAirline
			  ,CS(atoi(pclDseq)));
		    }
		  prlPageCommand = rgPG.prPages[ilCurPag].prPagefile->prCommands[ilCurObj];
		  if(strcmp(prlPageCommand->pcDZCommand,"gtGIFDefine")==0)
		    {
		      memset(pclParameterBuf,0x00,XS_BUFF);
		      memset(pclTextPut,0x00,M_BUFF);
		      memset(pclFileBuf,0x00,XS_BUFF);
		      memset(pclResult,0x00,M_BUFF);
		      memset(pclCommand,0x00,M_BUFF);
		      SetPageData(ilCurPag,ilCurObj,prlRecord,pclCommand,pclTextPut,pclFileBuf,0);
		      strcpy(pclParameterBuf,"0,0,-1,");
		      strcat(pclParameterBuf,pclTextPut);
		      if(igsend_small_logo==TRUE)
			{
			  if(igPacketMax!=-1)
			    ilRcDev= gtBuildCommand("E","gtPageSetCurrent",pclPageId,1,"-1","",ilSocket,pcgResBuf); 
			  memset(pcgResBuf,0x00,RES_BUF_SIZE);
			  ilRcDev= gtBuildCommand("1","gtFilerPutFile","gtSYSTEM_FILER",4,pclParameterBuf,pclFileBuf,ilSocket,pcgResBuf);
			}
		      memset(pclParameterBuf,0x00,XS_BUFF);
		      strcpy(pclParameterBuf,"-1,");
		      strcat(pclParameterBuf,pclTextPut);
		      if(igsend_small_logo==TRUE)
			{
			  memset(pcgResBuf,0x00,RES_BUF_SIZE);
			  ilRcDev= gtBuildCommand("1","gtFilerPutData","gtSYSTEM_FILER",2,pclParameterBuf,pclFileBuf,ilSocket,pcgResBuf);
			  if(igPacketMax!=-1)
			    ilRcDev= gtBuildCommand("S","gtPageSetCurrent",pclPageId,1,"-1","",ilSocket,pcgResBuf); 
			}
		      memset(pcgResBuf,0x00,RES_BUF_SIZE);
		      if(igPacketMax==-1)
			{
			  ilRcDev= gtBuildCommand("1",pclCommand,pclObjectId,1,pclTextPut,pclFileBuf,ilSocket,pcgResBuf);
			}else{
			  ilRcDev= gtBuildCommand("C",pclCommand,pclObjectId,1,pclTextPut,pclFileBuf,ilSocket,pcgResBuf);
			}
		    }else{
		      memset(pclTextPut,0x00,M_BUFF);
		      memset(pclFileBuf,0x00,XS_BUFF);
		      memset(pclResult,0x00,M_BUFF);
		      memset(pclCommand,0x00,M_BUFF);
		      SetPageData(ilCurPag,ilCurObj, prlRecord,pclCommand,pclTextPut,pclFileBuf,0);
		      memset(pcgResBuf,0x00,RES_BUF_SIZE);
		      if(igPacketMax!=-1)
			{
			  ilRcDev= gtBuildCommand("C",pclCommand,pclObjectId,1,pclTextPut,pclFileBuf,ilSocket,pcgResBuf);
			}else{
			  ilRcDev= gtBuildCommand("1",pclCommand,pclObjectId,1,pclTextPut,pclFileBuf,ilSocket,pcgResBuf);
			}
		    }
		  if(ilRcDev==RC_FAIL)
		    {
		      CloseDevice(ipCurDev);
		      return RC_SUCCESS;
		    }
		}/*end for columnes*/
	    }else{
	      for(ilColumn = 0;ilColumn < rgPG.prPages[ilCurPag].iTableColumns;ilColumn++)
		{
		  memset(pclObjNumber,0x00,4);
		  GetDataItem(pclObjNumber,rgPG.prPages[ilCurPag].pcTableObjNoList,ilColumn+1,',',"","");
		  ilCurObj = atoi(pclObjNumber);
		  ilId = (ilRow*rgPG.prPages[ilCurPag].iTableColumns)+ilColumn+ilStartId;
		  memset(pclObjectId,0x00,4);
		  sprintf(pclObjectId,"%d",ilId);
		  prlPageCommand = rgPG.prPages[ilCurPag].prPagefile->prCommands[ilCurObj];
		  /*  	  if(igdebug_dzcommand)  */
		  /*  		    dbg(DEBUG,"pclObjId <%s>Cmd <%s>",pclObjectId,prlPageCommand->pcDZCommand);  */
		  if(!strcmp(prlPageCommand->pcDZCommand,"gtGIFDefine"))
		    {/*if "gtGIFDefine"*/
		      memset(pcgResBuf,0x00,RES_BUF_SIZE);
		      if(igPacketMax==-1)
			{
			  ilRcDev=  gtBuildCommand("1","gtGIFPutFile",pclObjectId,1," ",pclFileBuf,ilSocket,pcgResBuf);
			}else{
			  ilRcDev= gtBuildCommand("C","gtGIFPutFile",pclObjectId,1," ",pclFileBuf,ilSocket,pcgResBuf);
			}  
		    }else{
		      memset(pcgResBuf,0x00,RES_BUF_SIZE);
		      if(igPacketMax==-1)
			{
			  ilRcDev= gtBuildCommand("1","gtTextPut",pclObjectId,1," ",pclFileBuf,ilSocket,pcgResBuf);
			}else{
			  ilRcDev= gtBuildCommand("C","gtTextPut",pclObjectId,1," ",pclFileBuf,ilSocket,pcgResBuf);
			}
		    }
		  if(ilRcDev==RC_FAIL)
		    {
		      CloseDevice(ipCurDev);
		      return RC_SUCCESS;
		    }
		}
	    }/*end for rows*/
	  ilPacketCnt++;
	  if(ilPacketCnt==ilPacket)
	    {
	      ilRcDev= gtBuildCommand("E","gtPageSetCurrent",pclPageId,1,"-1","",ilSocket,pcgResBuf);
	      if(ilRcDev==RC_FAIL)
		{
		  CloseDevice(ipCurDev);
		  return RC_SUCCESS;
		}
	    }
	}/*end for cloumns*/
      if(strcmp(rgPG.prPages[ipCurPag].pcDisplayTypeInternal,"S")==0)
	{ 
	  memset(pcgResBuf,0x00,RES_BUF_SIZE);
#ifdef _TLL
	  gtBuildCommand("1","gtCollectionRefresh",pclPageId,1,"-1","",ilSocket,pcgResBuf);
#else
	  gtBuildCommand("1","gtCollectionRedraw",pclPageId,0,"","",ilSocket,pcgResBuf);
#endif
	  /*gtBuildCommand("1","gtTerminalStatusRequest","gtSYSTEM_TERMINAL",0,"","",ilSocket,pcgResBuf);   */
	  /*dbg(TRACE,"<CheckDeviceState> %05d gtTerminalStatusRequest: \n  , ResultBuf <%s>",__LINE__,pcgResBuf); */
	}else{
	  if(rgDV.prDevPag[ilCurDev].iAlarmFlag==TRUE||rgDV.prDevPag[ilCurDev].iDefaultFlag==TRUE)
	    {
	      memset(pcgResBuf,0x00,RES_BUF_SIZE);
	      gtBuildCommand("1","gtCollectionRefresh",pclPageId,1,"-1","",ilSocket,pcgResBuf);
	    }else{
	      if(igrefresh==TRUE)
		{
		  memset(pcgResBuf,0x00,RES_BUF_SIZE);
#ifdef _ATH
		  gtBuildCommand("1","gtCollectionRedraw",pclPageId,0,"","",ilSocket,pcgResBuf);
#else		      
		  gtBuildCommand("1","gtCollectionRefresh",pclPageId,1,"-1","",ilSocket,pcgResBuf); 
#endif	      
		}else{
		  memset(pcgResBuf,0x00,RES_BUF_SIZE);
#ifndef _TLL
		  gtBuildCommand("1","gtCollectionRedraw",pclPageId,0,"","",ilSocket,pcgResBuf);
#endif

		}
	    }
	}
    }
  rgPG.prPages[ilCurPag].iTableRowsNow=ilTableRowsNowSave;
  if(igshow_event==TRUE)
    dbg(TRACE,"UpdateTable done");
  return ilRc;
}/*ifCurObj*/

static void CloseDevice(int ipCurDev)
{
  int ilRc=RC_SUCCESS;
  close(rgDV.prDevPag[ipCurDev].iSocket);
  rgDV.prDevPag[ipCurDev].iSocket = 0;
  rgDV.prDevPag[ipCurDev].pcDeviceState[0]='\0';
  strcpy(rgDV.prDevPag[ipCurDev].pcDeviceState,"D");
  dbg(TRACE,"Device %s does not response",rgDV.prDevPag[ipCurDev].pcIP);
  if((ilRc =SetDevState(rgDV.prDevPag[ipCurDev].pcIP,DOWN,rgDV.prDevPag[ipCurDev].pcDevUrno)) != RC_SUCCESS)
    {
      dbg(TRACE,"SetDeviceState: SetDevState() returns <%d>!",ilRc);
    }
}

















