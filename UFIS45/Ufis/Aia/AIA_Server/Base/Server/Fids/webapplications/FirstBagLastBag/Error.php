<html>
<head>
  <link rel=stylesheet href=styles/fblb.css type=text/css>
  <style>BODY{MARGIN-LEFT:0pt;MARGIN-RIGHT:-1pt;MARGIN-TOP:-1pt}</style>
  <title>Error Page</title>
</head>
<body>
  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" width="100%">
    <tr class="erHdr">
      <td width="60%" colspan="5">Error</td>
      <td width="20%"><img src="graphics/ufis.gif" onDragStart="event.returnValue=false" width="161" height="142"/></td>
    </tr>
    <tr class="infor" align="right">
      <td width="60%" colspan="5">&nbsp;</td>
      <td width="20%" colspan="6">IP Address:<?php echo $_SERVER['REMOTE_ADDR'];?></td>
    </tr>
  </table>
  <table border="0" cellpadding="0" cellspacing="2" style="border-collapse: collapse" width="100%">
    <tr class="tblHdr"><td>&nbsp;</td></tr>
  </table>
  <br>
  <p class="msgB">
  <?php
    $msg=$_GET['msg'];
    $erCode = substr(strstr($msg,'ORA-'),4,5);
    if ($erCode != '') {
      if ($erCode=='01034') {
        echo "Database not available. Please contact Administrator";
      } else if ($erCode=='01033') {
        echo "Database is being started or shut down. Please contact Administrator";
      } else {
        //echo "Database error: ORA-ERR [".$erCode."]";
        echo "Database error:<br>[".$msg."]";
      }
    }
  ?>
</body>
</html>
