<?php
	include("ApplicationSettings.php");
	include("./include/tohtml.inc.php");
	Global $HOPO,$oraconn,$db,$tdi,$DBExecTime;
	$file="db_if_fblb.php";
	
	function GetDBDataDirect($Dbg_level,$query)
	{
		Global $oraconn,$db,$DBExecTime;
		$ExecStart=time();
		$rs = $db->Execute($query);
		$ExecEnd=time();
		$DBExecTime=$ExecEnd-$ExecStart;
		if ($Dbg_level==DEBUG)
		{
						print_r("QUERY:[".$query."]</br>");
						print_r("CON:[".$rs->dataProvider."]</br>");
						print_r("SQL:[".$rs->sql."]</br>");
						print_r("FLD:[".$rs->fields."]</br>");
						print_r("REC:[".$rs->RecordCount()."]</br>");
						rs2html($rs,'border=2 cellpadding=3',''); 

						while (!$rs->EOF) {  
								for($x=1;$x<=$rs->RecordCount();$x++)
								{
									print "[".$x.".record]:<BR>";
									for($y=0;$y<$rs->FieldCount();$y++)
									{
										print "&nbsp;&nbsp[".$y.".field]:".$rs->fields[$y]."<BR>";
									}
								$rs->MoveNext();  
								}
						}
		}
		return $rs;
	}

	function DateAdd ($interval,  $number, $date) {

	$date_time_array  = getdate($date);
	    
	$hours =  $date_time_array["hours"];
	$minutes =  $date_time_array["minutes"];
	$seconds =  $date_time_array["seconds"];
	$month =  $date_time_array["mon"];
	$day =  $date_time_array["mday"];
	$year =  $date_time_array["year"];

	    switch ($interval) {
	    
		case "yyyy":
		    $year +=$number;
		    break;        
		case "q":
		    $year +=($number*3);
		    break;        
		case "m":
		    $month +=$number;
		    break;        
		case "y":
		case "d":
		case "w":
		     $day+=$number;
		    break;        
		case "ww":
		     $day+=($number*7);
		    break;        
		case "h":
		     $hours+=$number;
		    break;        
		case "n":
		     $minutes+=$number;
		    break;        
		case "s":
		     $seconds+=$number;
		    break;        

	    }    
		$timestamp =  mktime($hours ,$minutes, $seconds,$month ,$day, $year);
	    return $timestamp;
	}

	function ChopString ($interval,  $number, $str) {

		if (strlen($str)>0) {
		$year = substr ($str, 0, 4); 
		$month = substr ($str, 4,2); 
		$day = substr ($str, 6, 2); 		
		$hour = substr ($str, 8, 2); 
		$min = substr ($str, 10, 2); 
		$sec = substr ($str, 12, 2); 
		$date=mktime ($hour,$min,$sec,$month,$day,$year);			
		} else {$date="";}
			
		
		if ($number>0 && strlen($str)>0) {
			return DateAdd ($interval,  $number, $date) ;
		} else {
			return $date;	
		}
		
	}
	Function FindTDI () {
	 Global $db,$HOPO;
	 $cmdTempCommandText = "SELECT TICH,TDI1,TDI2   from CEDA.APTTAB  where APC3='".$HOPO."'";
	 $Currenttimediff=&$db->Execute($cmdTempCommandText);
		if (! $Currenttimediff->EOF)  {

		$tich= ChopString("n",0,$Currenttimediff->fields("TICH"));
		$hh=gmstrftime("%H",time());
		$mm=gmstrftime("%M",time());
		$ss=gmstrftime("%S",time());

		$Month=gmstrftime("%m",time());
		$Day=gmstrftime("%d",time());
		$Year=gmstrftime("%Y",time());

		$tnow=mktime($hh,$mm,$ss,$Month,$Day,$Year);

		    if ($tnow>$tich) {
			$tdi=  $Currenttimediff->fields("TDI2");
		    } else {
			$tdi=  $Currenttimediff->fields("TDI1");
		    }	
			return $tdi;		
				
		  }// If End
	    if ($timediff) $timediff->Close();
	}
	$tdi=FindTDI();
?>
