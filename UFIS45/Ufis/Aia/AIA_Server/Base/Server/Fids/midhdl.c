#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Aia/AIA_Server/Base/Server/Fids/midhdl.c 1.1 2004/10/04 16:57:04SGT jim Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB AAT/I Program midhdl.c    	                                            */
/*                                                                            */
/* Author         : Joern Weerts                                              */
/* Date           : 10.04.2001                                                */
/* Description    : control process for the AEG-MIS LCD boards        				*/
/* Update history :                                           								*/
/*                  jwe 10.04.2001                                            */
/*                  started with programming                                  */
/*                  jwe 10.05.2001                                            */
/*                  first release for athens                                  */
/*                  date/who                                                  */
/*                  action                                                    */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE

#include <arpa/inet.h>
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/resource.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "helpful.h"
#include "debugrec.h"
#include "netin.h"
#include "list.h"
#include "tcputil.h"
#include "midhdl.h"

/* main communication (TCP/UDP) defines */
#define REQUEST_NR(igReqNr) if(igReqNr >= 32767) {igReqNr=0;};igReqNr++;
#define SENDTO_TIMEOUT 3
#define ACCEPT_TIMEOUT 5

/* defines for bitmanipulation (turnaround of byteorder) for short-value and long-value */
#define TURN_SHORT_BO(s) ( (((s) & 0x00ff) << 8) | (((s) & 0xff00) >> 8) )
#define TURN_LONG_BO(s) ( (((s) & 0x000000ff) << 24) | (((s) & 0x0000ff00) << 8) | (((s) & 0x00ff0000) >> 8) | (((s) & 0xff000000) >> 24) )

/******************************************************************************/
/* External variables and functions                                           */
/******************************************************************************/
FILE *outp       = NULL;
int  debug_level = TRACE;
int  igDebugLevel = 0;
int  igdebug_switch = FALSE;
int  igslow_connection = FALSE;

extern int nap(unsigned long time);
extern int snap(char*,long,FILE*);
extern void BuildItemBuffer(char *, char *, int, char *);
extern int GetDataItem(char*,char*,int,char,char*,char*);
extern int GetNextDataItem(char*, char**,char*,char*,char*);
extern int gtBuildCommand(char *pcpInd, char *pcpCommand, char *pcpObjectId, int ipNoParam, char *pcpParam, char *pcpFilNam, int ipSocket, char *pcpResBuf);
extern int FindItemInList(char*,char*,char,int*,int*,int*); 
extern int dsplib_socket(int*,int,char*,int,int);
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;    		/* The queue item pointer  */
static int igItemLen     	 = 0;       		/* length of incoming item */
static EVENT *prgEvent     = NULL;    		/* The event pointer       */
static char pcgProcessName[20];      			/* buffer for my own process name */ 
static char pcgHomeAp[20];      					/* buffer for name of the home-airport */
static char pcgTabEnd[20];      					/* buffer for name of the table extension */
static int igInitOK = FALSE;							/* init flag */
static int igWriteQueue = 0;							/* queue for sequential writing */
static FILE *pgLogFile = NULL;						/* filepointer for all actions*/
static int igSigCnt = 0;									/* counter for receiving signals */
static int igButtonsPerFlight = 0;				/* number of buttons per one flight on each keypad */
static int igNumberOfMids = 1;	/* number of connected mids */

static int igMidUdpPort = 0;							/* global integer for the udp-port to use */
static int igStreamSock = -1;							/* global socket descriptor for the tcp-listen */
static int igMidTcpSock = -1;							/* global socket descriptor for tcp-communication  */
static int igMidTcpPort = 0;							/* global integer for the tcp-port to use */
static char pcgHostName[XS_BUFF];
static char pcgHostIp[XS_BUFF];
static char pcgBcAddr[XS_BUFF];
static char *pcgUdpBCService = "UFIS_BC";
static char pcgTcpService[XXS_BUFF];

static int igIncCount = 1;
static int igReqNr = 0;
static int igPollRate = 0;
static time_t tgNextPoll = 0;
static char pcgConfFile[S_BUFF];					/* buffer for filename */
static char *pcgSections = NULL;
static char *pcgSectionPointer = NULL;

static MID_INFO rgMid;
static LPLISTHEADER prgKeyPadList = NULL;	/* pointer to internal data list */

static BOOL bgAlarm 					= FALSE;		/* global flag for timed-out socket-read */
static BOOL bgUseHopo					= FALSE;		/* global flag for use of HOPO database field */
static time_t tgNextConnect = 0;
static time_t tgNextTry = 0;
/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int Init_midhdl(void);								/* main initializing */	
static int Reset(void);   						      /* Reset program          */
static int HandleInternalData(char *pcpResult);/* Handles data from CEDA */
static void	Terminate(BOOL bpSleep);        /* Terminate program      */
static void	HandleSignal(int);              /* Handles signals        */
static void	HandleErr(int);                 /* Handles general errors */
static void	HandleQueErr(int);              /* Handles queuing errors */
static void HandleQueues(void);             /* Waiting for Sts.-switch*/
static void ConnectDevice();
static void Accept_MID_Connection();
static void CloseTCP(int *ipSock);
static int ReadSections();
static int GetSection(char *pcpSection);
static int ReadCfg(char *pcpSection);
static int GetCfgEntry(char *pcpFile,char *pcpSection,char *pcpEntry,short spType,
												char **pcpDest,int ipValueType,char *pcpDefVal);
static int CheckValue(char *pcpEntry,char *pcpValue,int ipType);
static int CheckCfg();
static int GetKeyPadInfos();
static int GetNetInfos();
/*static int Identify_MID_Bc(char *pcpBuffer);*/
/*static int IdentifyDev(struct sockaddr_in *rpSockInfo);*/
static int poll_q_and_sock ();
static void TrimAll(char *pcpBuffer);
static void TimeToStr(char *pcpTime,time_t lpTime,int ipType,int ipLocal);
static void snapit(void *pcpBuffer,long lpDataLen,FILE *pcpDbgFile);
static int RequestStatus();
static int DecodeMidResult(char *pcpResultBuffer);
static int FindActions(char *pcpKeyPad,char *pcpButton,KEYPAD_INFO **prpKeyPad,
												char *pcpActions,int *ipRow,int *ipButton);
static int PrepareSingleActions(KEYPAD_INFO *prpKeyPad,char *pcpActions,int ipRow,int ipButton);
static int RunSingleAction(KEYPAD_INFO *prpKeyPad,char *pcpAction, 
														char *pcpTable,char *pcpField,int ipRow,int ipButton,int ipActionNr);
static int SendEvent(char *pcpCmd,char *pcpSelection,char *pcpFields, char *pcpData,
											char *pcpAddStruct, int ipAddLen,int ipModIdSend,int ipModIdRecv,
											int ipPriority,char *pcpTable,char *pcpType,char *pcpFile,char *pcpResult);
static void CheckButtonState(KEYPAD_INFO *prpKeyPad,int ipRow,int ipButton,int *ipNowActiveButton);
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;

	INITIALIZE;			/* General initialization	*/

	/* handles signal		*/
	(void)SetSignals(HandleSignal);

  /* copy process-name to global buffer */
  memset(pcgProcessName,0x00,sizeof(pcgProcessName));
	strcpy(pcgProcessName, argv[0]);                            

	dbg(TRACE,"MAIN: version <%s>",mks_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));
	
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}
	else
	{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}
	/**********************************************************/
	/* following lines for identical binaries on HSB-machines */
	/**********************************************************/
	*pcgConfFile = '\0'; 
	sprintf(pcgConfFile,"%s/%s",getenv("BIN_PATH"),pcgProcessName);
	ilRc = TransferFile(pcgConfFile); 
	if(ilRc != RC_SUCCESS) 
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
	}
	
	/********************************************************************/
	/*For identical cfg-files on HSB-machines uncomment following lines */
	/********************************************************************/
	*pcgConfFile = '\0';
	sprintf(pcgConfFile,"%s/%s.cfg",getenv("CFG_PATH"),pcgProcessName);
	ilRc = TransferFile(pcgConfFile); 
	if(ilRc != RC_SUCCESS) 
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
	} 
	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	}
	
	/* now waiting if I'm STANDBY */
	if ((ctrl_sta != HSB_STANDALONE) &&
			(ctrl_sta != HSB_ACTIVE) &&
			(ctrl_sta != HSB_ACT_TO_SBY))
	{
		HandleQueues();
	}

	dbg(TRACE,"--------------------------------");
	if ((ctrl_sta == HSB_STANDALONE) ||
			(ctrl_sta == HSB_ACTIVE) ||
			(ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		dbg(TRACE,"================================");
		if(igInitOK == FALSE)
		{
			ilRc = Init_midhdl();
			debug_level = TRACE;
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"MAIN: Init_midhdl() failed! Terminating!");
				dbg(TRACE,"================================");
				Terminate(TRUE);
			}
		}
	} 
	else
	{
		dbg(TRACE,"MAIN: wrong HSB-state! Terminating!");
		Terminate(FALSE);
	}
	
	if (ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"================================");
		dbg(TRACE,"MAIN: initializing OK");
		dbg(TRACE,"================================");
		dbg(TRACE,"MAIN: start working ....");
		debug_level = igDebugLevel;

		for(;;)
		{
			if ((ilRc = poll_q_and_sock()) == RC_SUCCESS)
			{
				dbg(TRACE,"MAIN: returned from poll_q_and_sock()");
			}
		}
	}
	else
	{
		dbg(TRACE,"MAIN: Init_midhdl() invalid! Terminating!");
	}
	Terminate(TRUE);
	return(0);
} /* end of MAIN */
/*********************************************************************
Function	 :Init_midhdl
Paramter	 :IN:                                                      
Returnvalue:RC_SUCCES,RC_FAIL
Description:initializes the interface with all necessary values
*********************************************************************/
static int Init_midhdl()
{
	int	ilRc = RC_FAIL;			/* Return code */
	char pclFileBuffer[L_BUFF];
	
	/* reading default home-airport from sgs.tab */
	memset(pcgHomeAp,0x00,sizeof(pcgHomeAp));
	ilRc = tool_search_exco_data("SYS","HOMEAP",pcgHomeAp);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"Init_midhdl : No HOMEAP entry in sgs.tab: EXTAB! Please add!");
		return RC_FAIL;
	}
	else
	{
		dbg(TRACE,"Init_midhdl : HOMEAP = <%s>",pcgHomeAp);
	}
	
	/* reading default table-extension from sgs.tab */
	memset(pcgTabEnd,0x00,sizeof(pcgTabEnd));
	ilRc = tool_search_exco_data("ALL","TABEND",pcgTabEnd);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"Init_midhdl : No TABEND entry in sgs.tab: EXTAB! Please add!");
		return RC_FAIL;
	}
	else
	{
		dbg(TRACE,"Init_midhdl : TABEND = <%s>",pcgTabEnd);
		if (strcmp(pcgTabEnd,"TAB") == 0)
		{
			bgUseHopo = TRUE;
			dbg(TRACE,"Init_midhdl : use HOPO-field!");
		}
	}

	/* now reading from configfile */
	if (*pcgConfFile != 0x00)
	{
		if ((ilRc = ReadCfg("MAIN")) == RC_SUCCESS)
		{
			if ((ilRc = CheckCfg()) == RC_SUCCESS)
			{
				GetNetInfos();

				if ((ilRc = GetKeyPadInfos()) == RC_SUCCESS)
				{
					igInitOK = TRUE;
				}
				else
				{
					dbg(TRACE,"Init_midhdl : GetKeyPadInfos() failed!");
				}

				if (strlen(rgMid.log) > 0)
				{
					pgLogFile = fopen(rgMid.log,"w");
					if ( ! pgLogFile )
					{
						dbg(TRACE,"Init_midhdl: Log: <%> not opened! fopen() returns <%s>."
							,rgMid.log,strerror(errno));
					}
					else
					{
						memset(pclFileBuffer,0x00,L_BUFF);
						sprintf(pclFileBuffer,"<---- START LOGGING ---->");
						fwrite(pclFileBuffer,sizeof(char),strlen(pclFileBuffer),pgLogFile);
						fwrite("\n",sizeof(char),1,pgLogFile);
						fflush(pgLogFile);
					}
				}
			}
			else
			{
				dbg(TRACE,"Init_midhdl : CheckCfg(MAIN) failed!");
			}
		}
		else
		{
			dbg(TRACE,"Init_midhdl : ReadCfg(MAIN) failed!");
		}
	}
	

	return(ilRc);
} /* end of initialize */
/*********************************************************************
Function	 :Reset()
Paramter	 :IN:                                                   
Returnvalue:RC_SUCCESS,RC_FAIL
Description:rereads the config-file,reinitializes the recv. and send
						log-files and clears the receive and the send buffers
*********************************************************************/
static int Reset(void)
{
	int	ilRc = RC_SUCCESS;
	char pclFileBuffer[L_BUFF];
	
	dbg(TRACE,"Reset: now resetting ...");
	dbg(TRACE,"Reset: closing TCP-IP connection(s).",ilRc);

	CloseTCP(&igMidTcpSock);
	
	dbg(TRACE,"Reset: freeing CFG-Memory.",ilRc);

	ListDestroy(prgKeyPadList);

	if ((prgKeyPadList=ListInit(prgKeyPadList,sizeof(MID_INFO))) == NULL)
	{
		dbg(TRACE,"Init_midhdl : couldn't create internal list for keypad-info(s)!");
		return RC_FAIL;
	}

	if (*pcgConfFile != '\0')
	{
		if ((ilRc = ReadCfg("MAIN")) != RC_SUCCESS)
		{
			dbg(TRACE,"Reset: ReadCfg(MAIN) failed!");
			ilRc = RC_FAIL;
		}
		else
		{
			if ((ilRc = CheckCfg()) != RC_SUCCESS)
			{
				dbg(TRACE,"Reset: CheckCfg(MAIN) failed!");
				ilRc = RC_FAIL;
			}
		}
	}

	if(pgLogFile != NULL)
	{
		memset(pclFileBuffer,0x00,L_BUFF);
		sprintf(pclFileBuffer,"<---- STOP LOGGING ---->");
		fwrite(pclFileBuffer,sizeof(char),strlen(pclFileBuffer),pgLogFile);
		fwrite("\n",sizeof(char),1,pgLogFile);
		fflush(pgLogFile);
		fclose(pgLogFile);

		pgLogFile = fopen(rgMid.log,"w");	
		if ( ! pgLogFile )
		{
			dbg(TRACE,"Reset: Log: <%> not opened! fopen() returns <%s>."
				,rgMid.log,strerror(errno));
		}
		else
		{
			memset(pclFileBuffer,0x00,L_BUFF);
			sprintf(pclFileBuffer,"<---- START LOGGING ---->");
			fwrite(pclFileBuffer,sizeof(char),strlen(pclFileBuffer),pgLogFile);
			fwrite("\n",sizeof(char),1,pgLogFile);
			fflush(pgLogFile);
		}
	}

	dbg(TRACE,"Reset: ... finished!");
	return ilRc;
} 
/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(BOOL bpSleep)
{
	int ilRc = 0;
	char pclFileBuffer[L_BUFF];
	if (bpSleep == TRUE)
	{
		dbg(TRACE,"Terminate: sleeping 60 sec. before terminating.");
		sleep(60);
	}

	if (igInitOK == TRUE)
	{
		if (igStreamSock != -1)
		{
			close(igStreamSock);
			igStreamSock = -1;
		}
		if (igMidTcpSock != -1)
		{
			close(igMidTcpSock);
			igMidTcpSock = -1;
		}
		dbg(TRACE,"Terminate: TCP/IP-connections closed.");

		dbg(TRACE,"Terminate: freeing CFG-Memory.",ilRc);

		ListDestroy(prgKeyPadList);
		dbg(TRACE,"Terminate: internal data deleted.");
	}
	dbg(TRACE,"Terminate: closing Log-file.");
	if(pgLogFile != NULL)
	{
		memset(pclFileBuffer,0x00,L_BUFF);
		sprintf(pclFileBuffer,"<---- STOP LOGGING ---->");
		fwrite(pclFileBuffer,sizeof(char),strlen(pclFileBuffer),pgLogFile);
		fwrite("\n",sizeof(char),1,pgLogFile);
		fflush(pgLogFile);
		fclose(pgLogFile);
	}
	dbg(TRACE,"Terminate: now leaving ......");
	exit(0);
} 

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int ipSig)
{
	switch (ipSig)
	{
		case SIGPIPE:
			/*dbg(TRACE,"HandleSignal: Received Signal <%d>(SIGPIPE).",ipSig);*/
			/* Check connections should be called here */
			CloseTCP(&igMidTcpSock);
			break;
		case SIGTERM:
			dbg(TRACE,"HandleSignal: Received Signal <%d>(SIGTERM).",ipSig);
			Terminate(FALSE);
			break;
		case SIGALRM:
			/*dbg(DEBUG,"HandleSignal: Received Signal <%d>(SIGALRM)",ipSig);*/
			bgAlarm = TRUE;
			break;
		case SIGCHLD:
			dbg(TRACE,"HandleSignal: Received Signal <%d>(SIGCHLD)",ipSig);
			Terminate(FALSE);
			break;
		case SIGTTIN:
			dbg(TRACE,"HandleSignal: Received Signal <%d>(SIGTTIN)",ipSig);
			CloseTCP(&igMidTcpSock);
			break;
		default:
			dbg(TRACE,"HandleSignal: Received Signal <%d>. Terminating !",ipSig);
			Terminate(FALSE);
			break;
	} 
	if (igSigCnt > 100)
	{
		dbg(TRACE,"HandleSignal: Terminating. Received SIGPIPE more than 100 times! Terminating ...!");
		Terminate(TRUE);
	}
} 

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr)
	{
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	case	QUE_E_PRIORITY:
		dbg(TRACE,"<%d> : invalid priority was send ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
	return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	dbg(TRACE,"HandleQueues: now entering ...");
	do
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	
		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_STANDBY event!");
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_COMING_UP event!");
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_ACTIVE event!");
				dbg(TRACE,"HandleQueues: Terminating for new init ...");
				Terminate(FALSE);
				ilBreakOut = TRUE;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_ACT_TO_SBY event!");
				break;	
			case	HSB_DOWN	:
				/* 	whole system shutdown - do not further use que(), */
				/*	send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_DOWN event!");
				Terminate(FALSE);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_STANDALONE event!");
				dbg(TRACE,"HandleQueues: Terminating for new init ...");
				Terminate(FALSE);
				ilBreakOut = TRUE;
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				/*HandleRemoteDB(prgEvent);*/
				break;
			case	SHUTDOWN	:
				dbg(TRACE,"HandleQueues: received SHUTDOWN event!");
				Terminate(FALSE);
				break;
			case	RESET		:
				dbg(TRACE,"HandleQueues: received RESET event!");
				ilRc = Reset();
				if (ilRc == RC_FAIL)
				{
					Terminate(FALSE);
				}
				break;
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong HSB-status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		}
		else
		{
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	}while (ilBreakOut == FALSE);

	if(igInitOK == FALSE)
	{
			ilRc = Init_midhdl();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Init_midhdl: init failed!");
			} /* end of if */
	}/* end of if */
	dbg(TRACE,"HandleQueues: ... now leaving");
} /* end of HandleQueues */
	

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleInternalData(char *pcpResult)
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilDataLen = 0;			/* Return code */
	int	ilType = 0;			/* Return code */
	char *pclSelection = NULL;
	char *pclFields = NULL;
	char *pclData = NULL;
  BC_HEAD *bchd = NULL;		/* Broadcast header		*/
  CMDBLK  *cmdblk = NULL;	/* Command Block 		*/
	char pclTwStart[S_BUFF];
	char pclUrno[XXS_BUFF];

  bchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  cmdblk= (CMDBLK  *) ((char *)bchd->data);

	dbg(TRACE,"");
	dbg(TRACE,"--- END/START HandleInternalData ---");

	dbg(TRACE,"HID: From<%d> Cmd<%s> Table<%s> Twstart<%s>",
			prgEvent->originator,cmdblk->command,cmdblk->obj_name,cmdblk->tw_start);
	
	/*DebugPrintItem(DEBUG,prgItem);*/
	/*DebugPrintEvent(DEBUG,prgEvent);*/

	pclSelection = cmdblk->data;
	pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
	pclData = (char*)pclFields + strlen(pclFields) + 1;

	dbg(DEBUG,"HID: Selection: <%s>",pclSelection); 
	dbg(DEBUG,"HID: Fields   : <%s>",pclFields); 
	dbg(DEBUG,"HID: Data     : <%s>",pclData);

	if (pcpResult == NULL)
	{
		dbg(DEBUG,"HID: No result desired! Don't copy result-data!");
	}
	else
	{
		strcpy(pcpResult,pclData);	
	}
  return ilRc;
} 
/* ********************************************************************/
/* The ReadCfg() routine																					 */
/* ********************************************************************/
static int ReadCfg(char *pcpSection)
{
	int ilRc = RC_SUCCESS;

	memset(&rgMid,0x00,sizeof(MID_INFO));

	/* filling CFG structure of mid-device */
	dbg(TRACE,"--------------------------------");
	dbg(TRACE,"ReadCfg: file <%s> Section <%s>",pcgConfFile,pcpSection);
	dbg(TRACE,"--------------------------------");
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"DEBUG_LEVEL",CFG_STRING,&rgMid.debug_level,CFG_NUM,"0"))
			== RC_SUCCESS)
	{
		if (atoi(rgMid.debug_level)==0)
		{
			dbg(TRACE,"GetCfgEntry: debug info = OFF after init-phase!");
			igDebugLevel = 0;
		}
		else
		{
			if (atoi(rgMid.debug_level)==1)
			{
				dbg(TRACE,"GetCfgEntry: debug info = TRACE after init-phase!");
				igDebugLevel = TRACE;
			}
			else
			{
				if (atoi(rgMid.debug_level)==2)
				{
					dbg(TRACE,"GetCfgEntry: debug info = DEBUG after init-phase!");
					igDebugLevel = DEBUG;
				}
				else
				{
					dbg(TRACE,"GetCfgEntry: debug info = OFF after init-phase!");
					igDebugLevel = 0;
				}
			}
		}
	}
	else
	{
		dbg(TRACE,"GetCfgEntry: debug info = OFF after init-phase!");
		dbg(TRACE,"GetCfgEntry: set DEBUG_LEVEL in configuration to change default!");
		igDebugLevel = 0;
	}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"LOG",CFG_STRING,&rgMid.log,CFG_PRINT,"/ceda/debug/mid_actions.log"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"WRITE_QUEUE",CFG_STRING,&rgMid.write_queue,CFG_NUM,"7150"))
			== RC_SUCCESS)
		{
			igWriteQueue=atoi(rgMid.write_queue);
		}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"MID_TYPE",CFG_STRING,&rgMid.mid_type,CFG_ALPHANUM," "))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"MID_UDPPORT",CFG_STRING,&rgMid.mid_udpport,CFG_NUM,"3351"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"MID_TCPPORT",CFG_STRING,&rgMid.mid_tcpport,CFG_NUM,"3450"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"MID_IP",CFG_STRING,&rgMid.mid_ip,CFG_PRINT," "))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"POLLRATE",CFG_STRING,&rgMid.pollrate,CFG_NUM,"5"))
			== RC_SUCCESS)
		{
			igPollRate = atoi(rgMid.pollrate);	
		}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"CLR_POLICY",CFG_STRING,&rgMid.clr_policy,CFG_PRINT," "))
			!= RC_SUCCESS)
		{}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"BUTTONS_PER_FLIGHT",CFG_STRING,&rgMid.buttons_per_flight,CFG_NUM,"0"))
			== RC_SUCCESS)
		{
			igButtonsPerFlight = atoi(rgMid.buttons_per_flight);	
		}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"BUTTON_OFFSET",CFG_STRING,&rgMid.button_offset,CFG_PRINT," "))
			!= RC_SUCCESS)
		{
			dbg(TRACE,"GetCfgEntry: invalid or none BUTTON_OFFSET defined can't continue !");
			return RC_FAIL;
		}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"NUMBER_OF_MIDS",CFG_STRING,&rgMid.number_of_mids,CFG_NUM,"1"))
			== RC_SUCCESS)
		{
			igNumberOfMids = atoi(rgMid.number_of_mids);	
		}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"KEYPADS",CFG_STRING,&rgMid.keypads,CFG_PRINT," "))
			!= RC_SUCCESS)
		{}
	dbg(TRACE,"--------------------------------");


	/* initializing board-list of group */
	if ((prgKeyPadList=ListInit(prgKeyPadList,sizeof(KEYPAD_INFO))) == NULL)
	{
		dbg(TRACE,"ReadCfg: couldn't create internal KEYPAD-list!");
		return RC_FAIL;
	}
	else
	{
		dbg(TRACE,"ReadCfg: internal list of KEYPAD's  initalized!");
	}
	return ilRc;
}
/* *********************************************************************/
/* The GetCfgEntry() routine   					 */
/* *********************************************************************/
static int GetCfgEntry(char *pcpFile,char *pcpSection,char *pcpEntry,short spType,
												char **pcpDest,int ipValueType,char *pcpDefVal)
{
	int ilRc = RC_SUCCESS;
	int ilLen = 0;
	char pclCfgLineBuffer[L_BUFF];
	
	memset(pclCfgLineBuffer,0x00,L_BUFF);
	if ((ilRc=iGetConfigRow(pcpFile,pcpSection,pcpEntry,spType,
													pclCfgLineBuffer)) != RC_SUCCESS)
	{
		dbg(TRACE,"GetCfgEntry: reading entry <%s> failed.",pcpEntry);
		dbg(TRACE,"GetCfgEntry: Use default <%s>.",pcpDefVal);
		strcpy(pclCfgLineBuffer,pcpDefVal);
	}

	if (strlen(pclCfgLineBuffer) < 1)
	{
		dbg(TRACE,"GetCfgEntry: EMPTY %s! Use default <%s>.",pcpEntry,pcpDefVal);
		strcpy(pclCfgLineBuffer,pcpDefVal);
	}
	ilLen = strlen(pclCfgLineBuffer)+4; 
	*pcpDest = malloc(ilLen);
	/*memset(pcpDest,0x00,ilLen);*/
	strcpy(*pcpDest,pclCfgLineBuffer);
	dbg(TRACE,"GetCfgEntry: %s = <%s>",pcpEntry,*pcpDest);
	if ((ilRc = CheckValue(pcpEntry,*pcpDest,ipValueType)) != RC_SUCCESS)
	{
		dbg(TRACE,"GetCfgEntry: please correct value <%s>!",pcpEntry); 	
	}
	return ilRc;
}
/* ******************************************************************** */
/* The CheckValue() routine																						*/
/* ******************************************************************** */
static int CheckValue(char *pcpEntry,char *pcpValue,int ipType)
{
	int ilRc = RC_SUCCESS;

	switch(ipType)
	{
		case CFG_NUM:
			while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
			{
				if (isdigit(*pcpValue)==0)
				{
					dbg(TRACE,"CheckValue : NOT A NUMBER! <%s>!"
						,pcpEntry); 
					ilRc = RC_FAIL;
				}
				pcpValue++;
			}
			break;
		case CFG_ALPHA:
			while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
			{
				if (isalpha(*pcpValue)==0)
				{
					dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!"
						,*pcpValue,pcpEntry); 
					ilRc = RC_FAIL;
				}
				pcpValue++;
			} 
			break;
		case CFG_ALPHANUM:
			while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
			{
				if (isalnum(*pcpValue)==0)
				{
					dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!"
						,*pcpValue,pcpEntry); 
					ilRc = RC_FAIL;
				}
				pcpValue++;
			}
			break;
		case CFG_PRINT:
			while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
			{
				if (isprint(*pcpValue)==0)
				{
					dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!"
						,*pcpValue,pcpEntry); 
					ilRc = RC_FAIL;
				}
				pcpValue++;
			}
			break;
		default:
			break;
	}
	return ilRc;
}
/* ******************************************************************** */
/* The CheckCfg(int ipGroup) routine																		*/
/* ******************************************************************** */
static int CheckCfg()
{
	int ilRc = RC_SUCCESS;
	int ilCount = 0;
	int ilCount2 = 0;
	int ilCount3 = 0;
	int ilCount4 = 0;
	LPLISTELEMENT prlEle = NULL;
			
	if (strncmp(rgMid.mid_type,"B",1) != 0 &&
			strncmp(rgMid.mid_type,"G",1) !=0)
	{
		dbg(TRACE,"CheckCfg: unknown type of MID-controller found! Doing nothing!");
		dbg(TRACE,"CheckCfg: valid types are B=Belt-mid & G=Gate-mid");
		return RC_FAIL;
	}

	if ((ilCount=(int)GetNoOfElements(rgMid.mid_ip,',')) != 1)
	{
		dbg(TRACE,"CheckCfg: invalid nr. of addresses in <MID_IP1> specified! Doing nothing!");
		return RC_FAIL;
	}
	if ((ilCount=(int)GetNoOfElements(rgMid.keypads,',')) < 1)
	{
		dbg(TRACE,"CheckCfg: no <KEYPADS> configured! Doing nothing!");
		return RC_FAIL;
	}
	return ilRc;
}
/* ******************************************************************** */
/* Following the poll_q_and_sock function				*/
/* Waits for input on the socket and polls the QCP for messages*/
/* ******************************************************************** */
static int poll_q_and_sock () 
{
  int ilRc;
  int ilRc_Connect = RC_FAIL;
  int ilRc_Request = 0;
	time_t tlNow; 
  int ilI;

  do
	{
		/*---------------------------*/
		/* now looking on ceda-queue */
		/*---------------------------*/
		ilRc = RC_NOT_FOUND;
		if (ilRc == RC_NOT_FOUND)
    {
			sleep(igPollRate);
			/* workaround to suppress not needed debug-info */
			debug_level = 0;
      while ((ilRc=que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen
											,(char *) &prgItem)) == RC_SUCCESS)
			{
				if (igDebugLevel != 0)
					debug_level = igDebugLevel;

				/* depending on the size of the received item  */
				/* a realloc could be made by the que function */
				/* so do never forget to set event pointer !!! */
				prgEvent = (EVENT *) prgItem->text;

				/* Acknowledge the item */
				ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
				if( ilRc != RC_SUCCESS ) 
				{
					/* handle que_ack error */
					HandleQueErr(ilRc);
				}

				switch(prgEvent->command)
				{
				case	HSB_STANDBY	:
					ctrl_sta = prgEvent->command;
					dbg(TRACE,"PQS: received HSB_STANDBY event!");
					HandleQueues();
					break;	
				case	HSB_COMING_UP	:
					ctrl_sta = prgEvent->command;
					dbg(TRACE,"PQS: received HSB_COMING_UP event!");
					HandleQueues();
					break;	
				case	HSB_ACTIVE	:
					ctrl_sta = prgEvent->command;
					dbg(TRACE,"PQS: received HSB_ACTIVE event!");
					break;	
				case	HSB_ACT_TO_SBY	:
					ctrl_sta = prgEvent->command;
					dbg(TRACE,"PQS: received HSB_ACT_TO_SBY event!");
					HandleQueues();
					break;	
				case	HSB_DOWN	:
					/* 	whole system shutdown - do not further use que(), */
					/*	send_message() or timsch() ! */
					ctrl_sta = prgEvent->command;
					dbg(TRACE,"PQS: received HSB_DOWN event!");
					Terminate(FALSE);
					break;	
				case	HSB_STANDALONE	:
					ctrl_sta = prgEvent->command;
					dbg(TRACE,"PQS: received HSB_STANDALONE event!");
					/*ResetDBCounter();*/
					break;	
				case	REMOTE_DB :
					/* ctrl_sta is checked inside */
					/*HandleRemoteDB(prgEvent);*/
					break;
				case	SHUTDOWN	:
					/* process shutdown - maybe from uutil */
					dbg(TRACE,"PQS: received SHUTDOWN event!");
					CloseTCP(&igMidTcpSock);
					Terminate(FALSE);
					break;
				case	RESET		:
					dbg(TRACE,"PQS: received RESET event!");
					ilRc = Reset();
					if (ilRc == RC_FAIL)
					{
						Terminate(FALSE);
					}
					break;
				case	EVENT_DATA	:
					if((ctrl_sta == HSB_STANDALONE) ||
						(ctrl_sta == HSB_ACTIVE) ||
						(ctrl_sta == HSB_ACT_TO_SBY))
					{
						ilRc = HandleInternalData(NULL);
						if(ilRc!=RC_SUCCESS)
						{
							dbg(TRACE,"PQS: HandleInternalData failed <%d>",ilRc);
							HandleErr(ilRc);
						}
					}
					else
					{
						dbg(TRACE,"poll_q_and_sock: wrong HSB-status <%d>",ctrl_sta);
						DebugPrintItem(TRACE,prgItem);
						DebugPrintEvent(TRACE,prgEvent);
					}/* end of if */
					break;
				case	TRACE_ON :
					dbg_handle_debug(prgEvent->command);
					igDebugLevel = debug_level;
					break;
				case	TRACE_OFF :
					dbg_handle_debug(prgEvent->command);
					igDebugLevel = debug_level;
					break;
				default			:
					dbg(TRACE,"MAIN: unknown event");
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;
				} /* end switch */
      } /* end while */ 
			if (igDebugLevel != 0)
					debug_level = igDebugLevel;
    }

		if (igMidTcpSock < 0)
		{
			dbg(TRACE,"poll_q_and_sock: Now Connect Device");
			ConnectDevice();
		}
		else	
		{
			tlNow = time(0);
			if (tlNow >= tgNextPoll)
			{
/*
				if ((ilRc_Request = RequestStatus()) != RC_SUCCESS)
				{
					dbg(TRACE,"poll_q_and_sock: returned from RequestStatus()!");
				}
*/
				for (ilI=0; ilI<=igNumberOfMids; ilI++)
				{
                                   ilRc_Request = RequestStatus();
				}
				tlNow = time(0);
				tgNextPoll = tlNow + (time_t)igPollRate;
			}
		}
		/* stay in loop */
		ilRc = RC_FAIL;

  }while (ilRc!=RC_SUCCESS);
	return ilRc;
} 
/*************************************************************/
/* This function tries an active connection to the	         */
/* controllers                                               */
/*************************************************************/
static void ConnectDevice()
{
  int ilCnt = 0;
  int ilCnt2 = 0;
  int ilRc = RC_SUCCESS;
  int ilOffset = 0;
  int ilBytes = 0;
  int ilResponseCreated = FALSE;
  int ilActiveDevice=0; 
  char pclTestBuffer[XS_BUFF];
  char pclReceiveBuffer[XS_BUFF];
  char pclResponseBuffer[XS_BUFF];
  char pclIp[XS_BUFF];
  MID_UDP_RESPONSE rlResponse; 
	struct sockaddr_in 	rlSockInfo;
  int ilRetryCnt = 0;
  int ilTimeOffset = 10;
  int ilLen = 0;
  int ilMidUdpSock = -1;

	/* initializing (offset definition) the response buffer */
	memset(pclResponseBuffer,0x00,XS_BUFF);
	ilOffset              	= 0;
	rlResponse.id          	= (USHORT*)&pclResponseBuffer[ilOffset];
	ilOffset               	= 2;
	rlResponse.count       	= (USHORT*)&pclResponseBuffer[ilOffset];
	ilOffset               	= 4;
	rlResponse.clientaddr  	= (ULONG*)&pclResponseBuffer[ilOffset];
	ilOffset               	= 8;
	rlResponse.serveraddr  	= (ULONG*)&pclResponseBuffer[ilOffset];
	ilOffset               	=	12;
	rlResponse.tcpport     	= (USHORT*)&pclResponseBuffer[ilOffset];

	#if 0 /* members below are not used */
	ilOffset               	= 14;
	rlResponse.time					= (USHORT*)&pclResponseBuffer[ilOffset];
	ilOffset                = 16;
	rlResponse.free     		= (USHORT)&pclResponseBuffer[ilOffset];
	#endif

	dbg(DEBUG,"ConnectDevice: send response-message to <%s>",rgMid.mid_ip);
	#ifdef _SOLARIS /* need to turn the byteorder because the MID-controller needs it this way */
		dbg(DEBUG,"ConnectDevice: changing byteorder! OS=SOLARIS!");
		*rlResponse.id 					= htons(HEX_CONNECT_RESPONSE);
		*rlResponse.count				= 0x0000;
		*rlResponse.clientaddr 	= TURN_LONG_BO(inet_addr(rgMid.mid_ip));
		*rlResponse.serveraddr 	= TURN_LONG_BO(inet_addr(pcgHostIp));
		*rlResponse.tcpport		 	= TURN_SHORT_BO((USHORT)(igMidTcpPort));
	#else
		dbg(DEBUG,"ConnectDevice: leave byteorder original! OS!=SOLARIS");
		*rlResponse.id 					= htons(HEX_CONNECT_RESPONSE);
		*rlResponse.count				= 0x0000;
		*rlResponse.clientaddr 	= htonl(inet_addr(rgMid.mid_ip));
		*rlResponse.serveraddr 	= htonl(inet_addr(pcgHostIp));
		*rlResponse.tcpport		 	= htons((USHORT)(igMidTcpPort));
	#endif

	#if 0 /* members below are not used */
	*rlResponse.time 				= 0x00;
	*rlResponse.free 				= 0x00;
	#endif

	if ((ilRc = dsplib_socket(&ilMidUdpSock,SOCK_DGRAM,pcgUdpBCService,igMidUdpPort,TRUE)) != RC_SUCCESS)
	{
		close(ilMidUdpSock);
		ilMidUdpSock = -1;
		dbg(TRACE,"ConnectDevice: dsplib_socket returns: <%d>.",ilRc);
	}
	else
	{
		dbg(TRACE,"ConnectDevice: UDP-Socket = <%d> created.",ilMidUdpSock);
		snapit(pclResponseBuffer,MID_UDP_RESPONSE_SIZE,outp);
		/* converting network-byte order to host byte order for "tcp_send_datagram" */
		sprintf(pclIp,"%x",ntohl((ULONG)(inet_addr(rgMid.mid_ip))));

    debug_level = 0;/*workaround because tcp_send is corrupt (NULL in dbg)*/
		alarm(SENDTO_TIMEOUT);
		if ((ilRc = tcp_send_datagram(ilMidUdpSock,pclIp,NULL,pcgUdpBCService,
								pclResponseBuffer,MID_UDP_RESPONSE_SIZE)) != RC_SUCCESS)
		{
			snapit(pclResponseBuffer,MID_UDP_RESPONSE_SIZE,outp);
			alarm(0);
		}
		alarm(0);
    debug_level = igDebugLevel;/*workaround because tcp_send is corrupt (NULL in dbg)*/

		close(ilMidUdpSock);
		ilMidUdpSock = -1;

		Accept_MID_Connection();
	}
	if (ilMidUdpSock != -1)
	{
		close(ilMidUdpSock);
		ilMidUdpSock = -1;
	}
}
/* ***************************************************************** */
/* The Accept_MID_Connection routine                                    */
/* ***************************************************************** */
static void Accept_MID_Connection()
{
  struct sockaddr_in rlSockInfo;
	int ilLen = 0;
	int ilFinalSock = -1;
	unsigned int uilHostIp = 0;
	char pclIPAddr[XXS_BUFF];

	dbg(TRACE,"Accept_MID_Connection: accepting connection for <%d> sec.",ACCEPT_TIMEOUT);
  errno = 0;
  ilLen = sizeof(rlSockInfo);
  alarm(ACCEPT_TIMEOUT);
	#ifdef _SOLARIS
		ilFinalSock = accept(igStreamSock,(struct sockaddr*)&rlSockInfo,&ilLen);
	#else 
		ilFinalSock = accept(igStreamSock,(struct sockaddr*)&rlSockInfo,(size_t*)&ilLen);
	#endif
  alarm(0);
	if (ilFinalSock >= 0)
	{
		uilHostIp = rlSockInfo.sin_addr.s_addr;
		memset(pclIPAddr,0x00,XXS_BUFF);
		strcpy(pclIPAddr,(char*)inet_ntoa(rlSockInfo.sin_addr));
		dbg(DEBUG,"Accept_MID_Connection: *uilHostIp <%x>!",uilHostIp); 
		dbg(DEBUG,"Accept_MID_Connection: IP-Adress  <%s>!",pclIPAddr);
		igMidTcpSock = ilFinalSock;
		dbg(TRACE,"Accept_MID_Connection: igMidTcpSock=<%d>. MID-contr. connected!",igMidTcpSock);
		dbg(TRACE,"Accept_MID_Connection: wait 5.sec. for socket to become ready!");
		sleep(5);
		dbg(TRACE,"Accept_MID_Connection: wait 10.sec. for connection of mids!");
		sleep(10);
		dbg(TRACE,"Accept_MID_Connection: ----------- START WORKING --------------");

	}
	else
	{
		dbg(TRACE,"Accept_MID_Connection: accept() failed! Error <%d>=<%s>",errno,strerror(errno));
	}
}
/* ***************************************************************** */
/* The CloseTCP routine                                    */
/* ***************************************************************** */
static void CloseTCP(int *ipSock)
{
	dbg(DEBUG,"CloseTCP: closing socket <%d> connection!",*ipSock);
	/*shutdown(igMidTcpSock,2); don't shutdown socket because dZine doesn't understand it */
	close(*ipSock);
	*ipSock = -1;
}
/* **************************************************************** */
/* The snapit routine                                           */
/* snaps data if the debug-level is set to DEBUG                */
/* **************************************************************** */
static void snapit(void *pcpBuffer,long lpDataLen,FILE *pcpDbgFile)
{
	if (debug_level==DEBUG)
	{
		snap((char*)pcpBuffer,(long)lpDataLen,(FILE*)pcpDbgFile);
		fflush(pcpDbgFile);
	}
}
/* ******************************************************************** */
/* The GetSection() routine                                             */
/* ******************************************************************** */
static int GetSection(char *pcpSection)
{
	int ilRc = RC_SUCCESS;

	memset((char*)pcpSection,0x00,XS_BUFF);
	GetNextDataItem(pcpSection, &pcgSectionPointer, ",", "\0", "  ");
	if (pcpSection[0] == 0x00)
	{
		ilRc = RC_FAIL;
	}
	else
	{
		dbg(DEBUG,"GetSection : Section <%s>",pcpSection);
	}
	return ilRc;
}
/* ******************************************************************** */
/* The GetKeyPadInfos() routine                                             */
/* ******************************************************************** */
static int GetKeyPadInfos()
{
	int ilRc = RC_SUCCESS;
	int ilCnt = 0;
	int ilCnt2 = 0;
	int ilPosInFields = 0;
	int ilCol = 0;
	int ilFromStart = 0;
	char pclTmpBuf[M_BUFF];
	KEYPAD_INFO rlKeyPad;

	ilCnt=(int)GetNoOfElements(rgMid.keypads,',');
	for (ilCnt2 = 0; ilCnt2 < ilCnt; ilCnt2++)
	{
		memset(pclTmpBuf,0x00,M_BUFF);
		GetDataItem(pclTmpBuf,rgMid.keypads,ilCnt2+1,',',"","");
		TrimAll(pclTmpBuf);
		dbg(TRACE,"GetKeyPadInfos: --- keypad <%s> size<%d> ---",pclTmpBuf,sizeof(KEYPAD_INFO));
		memset(&rlKeyPad,0x00,sizeof(KEYPAD_INFO));
		
		if ((ilRc=GetCfgEntry(pcgConfFile,pclTmpBuf,"ADDRESS",CFG_STRING,&rlKeyPad.address,CFG_PRINT," "))
				!= RC_SUCCESS)
			{return RC_FAIL;}
		if ((ilRc=GetCfgEntry(pcgConfFile,pclTmpBuf,"LOCATION",CFG_STRING,&rlKeyPad.location,CFG_PRINT," "))
				!= RC_SUCCESS)
			{return RC_FAIL;}
		if ((ilRc=GetCfgEntry(pcgConfFile,pclTmpBuf,"ACTIONS",CFG_STRING,&rlKeyPad.actions,CFG_PRINT," "))
				!= RC_SUCCESS)
			{return RC_FAIL;}
		if ((ilRc=GetCfgEntry(pcgConfFile,pclTmpBuf,"ACT_TABLE",CFG_STRING,&rlKeyPad.act_table,CFG_PRINT," "))
				!= RC_SUCCESS)
			{return RC_FAIL;}
		if ((ilRc=GetCfgEntry(pcgConfFile,pclTmpBuf,"ACT_FIELD",CFG_STRING,&rlKeyPad.act_field,CFG_PRINT," "))
				!= RC_SUCCESS)
			{return RC_FAIL;}

		if ((ilRc=FindItemInList(rlKeyPad.actions,"#CL",';',&ilPosInFields,&ilCol,&ilFromStart)) == RC_SUCCESS)
		{
			dbg(TRACE,"GetKeyPadInfos: #CL-action -> Row-pos.=<%d>",ilPosInFields);
			rlKeyPad.ilClearButton = ilPosInFields;	
		}
		else
		{
			rlKeyPad.ilClearButton = 0;	
		}

		/* appending  keypad to list of keypads */
		if (ListAppend(prgKeyPadList,&rlKeyPad) == NULL)
		{
			dbg(TRACE,"GetKeyPadInfos: ListAppend failed!");
			Terminate(FALSE);
		}
		else
		{
			dbg(TRACE,"GetKeyPadInfos: added keypad <%s> to list.",pclTmpBuf);
		}
	}
	return ilRc;
}
/* ******************************************************************** */
/* The TrimAll() routine                                            */
/* ******************************************************************** */
static void TrimAll(char *pcpBuffer)
{
	const char *p = pcpBuffer;
	int i = 0;

	for (i = strlen(pcpBuffer); i > 0 && isspace(pcpBuffer[i-1]); i--);
			pcpBuffer[i] = '\0';
	for ( *p ; isspace(*p); p++);
	while ((*pcpBuffer++ = *p++) != '\0');
}
/* ******************************************************************** */
/* The TimeToStr() routine																	*/
/* ******************************************************************** */
static void TimeToStr(char *pcpTime,time_t lpTime,int ipType,int ipLocal)
{
	int ilRc = 0;
	char pclTime[20];
	struct tm *_tm;
	struct tm rlTm;

	*pclTime = 0x00;
	*pcpTime = 0x00;
	if (lpTime == 0)
	{
		lpTime = time(NULL);
	}
	lpTime = lpTime;
	switch (ipLocal)
	{
		case 0:
			_tm = (struct tm *) localtime(&lpTime);
			break;
		case 1:
			_tm = (struct tm *) gmtime(&lpTime);
			break;
	}
	rlTm = *_tm;
	switch(ipType)
	{
		case 0: /* Returns "now" in CEDA-format YYYYMMDDHHMMSS */
			/* Unusal format because of sccs !! */
			strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTm);
			/*dbg (DEBUG,"NOW-TimeToStr : return <%s>",pcpTime);*/
			break;
		case 1:/* Returns lpTime in CEDA-format YYYYMMDDHHMMSS */
			/* Unusal format because of sccs !! */
			strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTm);
			/*dbg(DEBUG,"CEDA-TimeToStr: return <%s>",pcpTime);*/
			break;
		default:
			dbg(TRACE,"TimeToStr : CEDA-ERROR: unknown time-type received!");
			break;
	}
}
/*********************************************************************
Function :GetNetInfos()
Paramter :IN:
Return     :RC_SUCCESS,RC_FAIL
Description:- reads the own IP-adress from the unix-system files 
                    according to the entries inside the CEDA-sgs.tab
                    hostname variable or according to the name used by the unix
                    system.
            - reads the broadcast address from sgs.tab or sets it to
                    the default value of 0xffffffff.
                - reads informations about the UDP-Service UFIS_FIDS_BC
*********************************************************************/
static int GetNetInfos()
{

  int ilRc;
  uint32_t *uilHostIp;
  unsigned int uilBcAddr = 0;
  struct hostent *hp;
  struct in_addr rlIn;
	struct servent  *prlService;

  memset(pcgHostName,0x00,XS_BUFF);
  memset(pcgHostIp,0x00,XS_BUFF);

	/* getting the hostname */
  if ((ilRc = tool_search_exco_data("NET","HOST",pcgHostName)) != RC_SUCCESS)
  {
    dbg(TRACE,"GetNetInfos: No HOST entry in sgs.tab: EXTAB! Please add!");
    ilRc = gethostname(pcgHostName,XS_BUFF);
  } 

	/* getting my IP-address */
  if (ilRc == RC_SUCCESS)
  {
    dbg(TRACE,"GetNetInfos: Hostname=<%s>", pcgHostName);
    errno = 0;
    hp = gethostbyname(pcgHostName);
    if (hp == NULL)
  	{
   	 	dbg(TRACE,"GetNetInfos: Gethostbyname <%s>=><%s>",pcgHostName, strerror(errno));
   		ilRc = RC_FAIL;
  	}
  	else
  	{
			uilHostIp = (uint32_t*)hp->h_addr;
			rlIn.s_addr = *uilHostIp;
			strcpy(pcgHostIp,(char*)inet_ntoa(rlIn)); 
			dbg(TRACE,"GetNetInfos: IP-Address=<%s> ", pcgHostIp);
  	}
  }
	else
	{
      dbg(TRACE,"GetNetInfos: gethostname() failed!", __LINE__);
      ilRc = RC_FAIL;
  }    

  /* Now looking for main broadcast address */
 	if ((ilRc = tool_search_exco_data("NET","BCADDR",pcgBcAddr)) != RC_SUCCESS)
  {
      dbg(TRACE,"GetNetInfos: BC-Address not set in sgs.tab!",pcgBcAddr);
      dbg(TRACE,"GetNetInfos: setting to default!");
      uilBcAddr = 0xffffffff;
      sscanf(pcgBcAddr,"%lx",&uilBcAddr);
  }
	else
	{
      dbg(TRACE,"GetNetInfos: BC-Address from sgs.tab=<%s>",  pcgBcAddr);
      sscanf(pcgBcAddr,"%lx",&uilBcAddr);    
      dbg(TRACE,"GetNetInfos: BC-Address as value    =<%lx>", uilBcAddr);
  }

	/* reading the services/port information */
	if ((prlService=getservbyport(htons(atoi(rgMid.mid_udpport)),NULL)) == NULL)
	{
		dbg(TRACE,"GetNetInfos: OS-ERROR getservbyport(): <%d>!"
			,atoi(rgMid.mid_udpport));
			return RC_FAIL;
	}
	else
	{
		dbg(TRACE,"GetNetInfos: MID-UDP-Port   =<%d>",prlService->s_port);
		igMidUdpPort = htons(atoi(rgMid.mid_udpport));
		dbg(TRACE,"GetNetInfos: MID-UDP-Service=<%s>",prlService->s_name);
		/*strcpy(pcgUdpBCService,prlService->s_name);*/
	}

	if ((prlService=getservbyport(htons(atoi(rgMid.mid_tcpport)),NULL)) == NULL)
	{
		dbg(TRACE,"GetNetInfos: OS-ERROR getservbyport(): <%d>!"
			,atoi(rgMid.mid_tcpport));
			return RC_FAIL;
	}
	else
	{
		dbg(TRACE,"GetNetInfos: MID-TCP-Port   =<%d>",prlService->s_port);
		igMidTcpPort = htons(atoi(rgMid.mid_tcpport));
		dbg(TRACE,"GetNetInfos: MID-TCP-Service=<%s>",prlService->s_name);
		memset(pcgTcpService,0x00,XXS_BUFF);
		strcpy(pcgTcpService,prlService->s_name);

		if (igStreamSock < 0)
		{
			if ((ilRc = dsplib_socket(&igStreamSock,SOCK_STREAM,pcgTcpService,igMidTcpPort,TRUE)) == RC_FAIL)
			{
				dbg(TRACE,"SetNetInfos: CEDA-ERROR: dsplib_socket returns: <%d>",ilRc);
			}
			else
			{
				dbg(TRACE,"SetNetInfos: LISTEN socket <%d> created!",igStreamSock);
			}

			if(( ilRc = listen(igStreamSock,5)!=RC_SUCCESS))
			{
				dbg(TRACE,"SetNetInfos: OS-ERROR: listen() failed with <%d>,",ilRc);
			}
			else
			{
			 dbg(TRACE,"SetNetInfos: LISTEN socket <%d> initialized!",igStreamSock); 
			}
		}
	}
  return ilRc;
}
static int RequestStatus()
{
   int ilRc = RC_SUCCESS;
   char pclResultBuff[L_BUFF];
   char pclReqNr[XXS_BUFF];
   int ilReqNr;

   /*if you want to debug gt2hdl.c library this one must be set to true */
   /*igdebug_switch = TRUE*/;
   igdebug_switch = FALSE;
   memset(pclReqNr,0x00,XXS_BUFF);
   if (igIncCount == 1)
   {
      REQUEST_NR(igReqNr);
   }
   igReqNr = 1;
   sprintf(pclReqNr,"%d",igReqNr);
   memset(pclResultBuff,0x00,L_BUFF);
		if (igMidTcpSock >= 0)
	{
   ilRc = gtBuildCommand("1","gtSystemMidRequest",
                         "gtSYSTEM_MID",1,pclReqNr,"",igMidTcpSock,pclResultBuff);
   if (ilRc < RC_SUCCESS)
   {
      dbg(TRACE,"RequestStatus: ReqNr %d returns %d",igReqNr,ilRc);
      if (ilRc != RC_TIMEOUT)
      {
         dbg(TRACE,"RequestStatus: Closing Connection!",ilRc); 
         close(igMidTcpSock);
         igMidTcpSock = -1;
      }
   }
   else
   {
      /*dbg(DEBUG,"RequestStatus: --- RESULT ---");*/
      /*dbg(DEBUG,"RequestStatus: ReqNr %d returns %d Buf = <%s>",igReqNr,ilRc,pclResultBuff);*/ 
      igIncCount = 0;
      if (strlen(pclResultBuff) > 0)
      {
         snapit(pclResultBuff,16,outp);
         if (GetNoOfElements(pclResultBuff,',') >= 2)
         {
            DecodeMidResult(pclResultBuff);
         }
         else
         {
            ilReqNr = atoi(pclResultBuff);
/*
            if (ilReqNr != igReqNr)
            {
               igReqNr = ilReqNr;
               igIncCount = 1;
               ilRc = RequestStatus();
            }
*/
            igIncCount = 1;
         }
      }
   }
	}
   igdebug_switch = FALSE;
   return ilRc;
}
static int DecodeMidResult(char *pcpResultBuffer)
{
	int ilRc = RC_SUCCESS;
	int ilCnt = 0;
	int ilCount = 0;
	int ilRow = 0;
	int ilButton = 0;
	int ilNowActiveButton = 0;
	KEYPAD_INFO *prlKeyPad = NULL;
	char pclKeyPad[XXS_BUFF];
	char pclButton[XXS_BUFF];
	char pclActions[L_BUFF];

	if ((ilCount=(int)GetNoOfElements(pcpResultBuffer,',')) >= 2)
	{
		/*dbg(DEBUG,"DecodeMidResult: Found <%d> elements for decoding!",ilCount);*/
		for (ilCnt=1;ilCnt<=ilCount;ilCnt++)
		{
			memset(pclKeyPad,0x00,XXS_BUFF);
			memset(pclButton,0x00,XXS_BUFF);
			GetDataItem(pclKeyPad,pcpResultBuffer,ilCnt,',',"","");
			ilCnt++;
			GetDataItem(pclButton,pcpResultBuffer,ilCnt,',',"","");
			/*dbg(DEBUG,"DecodeMidResult: Keypad<%s> Button<%s> --> call FindActions()",pclKeyPad,pclButton);*/
			if ((ilRc = FindActions(pclKeyPad,pclButton,&prlKeyPad,pclActions,&ilRow,&ilButton)) == RC_SUCCESS)
			{
				/* step back clear policy S=SingleStep back */
				/*if (strncmp(rgMid.clr_policy,"S",1)==0 && strcmp(pclActions,"#CL") == 0)*/
				/*if (strncmp(rgMid.clr_policy,"S",1)==0)
				{*/
					CheckButtonState(prlKeyPad,ilRow,ilButton,&ilNowActiveButton);
					if (ilNowActiveButton != 0)
					{
						dbg(DEBUG,"DecodeMidResult: Changing status due to clear action!");
						sprintf(pclButton,"%d",ilNowActiveButton);
						if ((ilRc = FindActions(pclKeyPad,pclButton,&prlKeyPad,pclActions,&ilRow,&ilButton)) == RC_SUCCESS)
						{
							dbg(DEBUG,"DecodeMidResult: Prepare action(s) <%s>",pclActions);
							PrepareSingleActions(prlKeyPad,pclActions,ilRow,ilButton);
						}
						else
						{
							dbg(TRACE,"DecodeMidResult: Keypad<%s> button-offset=<%s> --> no action defined!",pclKeyPad,pclButton);
						}
					}
					else
					{
						dbg(DEBUG,"DecodeMidResult: Prepare action(s) <%s>",pclActions);
						PrepareSingleActions(prlKeyPad,pclActions,ilRow,ilButton);
					}
				#if 0
				}
				else  /*clear All policy -> no special action */
				{
					dbg(DEBUG,"DecodeMidResult: Prepare action(s) <%s>",pclActions);
					PrepareSingleActions(prlKeyPad,pclActions,ilRow,ilButton);
				} 
				#endif
			}
			else
			{
				dbg(TRACE,"DecodeMidResult: Keypad<%s> button-offset=<%s> --> no action defined!",pclKeyPad,pclButton);
			}
		}
	}
	else
	{
		ilRc = RC_FAIL;	
	}
	return ilRc;
}
	static int FindActions(char *pcpKeyPad,char *pcpButton,KEYPAD_INFO **prpKeyPad,
													char *pcpActions,int *ipRow,int *ipButton)
	{
		int ilRc = RC_FAIL;
		int ilKeyPadRows = 0;
		int ilCnt = 0;
		int ilRowButtons = 0;
		int ilCnt2 = 0;

		char pclRow[XS_BUFF];
		char pclButton[XS_BUFF];
		char pclAllButtonActions[L_BUFF];

		LPLISTELEMENT prlKeyPadEle = NULL;
		KEYPAD_INFO *prlKeyPad = NULL;

		prlKeyPadEle = ListFindFirst(prgKeyPadList);
		while(prlKeyPadEle!=NULL && ilRc == RC_FAIL)
		{
			/* referencing keypad structure */
			prlKeyPad = (KEYPAD_INFO*)prlKeyPadEle->Data;
			if (strcmp(prlKeyPad->address,pcpKeyPad) == 0)
			{
				/* checking for button definitions */
				if ((ilKeyPadRows=(int)GetNoOfElements(rgMid.button_offset,';')) > 0)
				{
					/* fetching button rows from config line */
					for (ilCnt=1;ilCnt<=ilKeyPadRows && ilRc==RC_FAIL;ilCnt++)
					{
						memset(pclRow,0x00,XS_BUFF);
						/* fetching single row from config line */
						GetDataItem(pclRow,rgMid.button_offset,ilCnt,';',"","");
						if ((ilRowButtons=(int)GetNoOfElements(pclRow,',')) > 0)
						{
							/* looking for button inside row */
							for (ilCnt2=1;ilCnt2<=ilRowButtons && ilRc==RC_FAIL;ilCnt2++)
							{
								memset(pclButton,0x00,XS_BUFF);
								GetDataItem(pclButton,pclRow,ilCnt2,',',"","");
								if (strcmp(pclButton,pcpButton) == 0)
								{
									/* found matching button inside config */
									memset(pclAllButtonActions,0x00,L_BUFF);
									GetDataItem(pclAllButtonActions,prlKeyPad->actions,ilCnt2,';',"","");	
									dbg(DEBUG,"FindActions: LOCATION<%s>=KEYPAD<%s> ROW<%d> BUTTON<%d> ==> ACTION(S)<%s>"
										,prlKeyPad->location,prlKeyPad->address,ilCnt,ilCnt2,pclAllButtonActions);
									strncpy(pcpActions,pclAllButtonActions,L_BUFF);
									*prpKeyPad = prlKeyPad;
									*ipRow = ilCnt;
									*ipButton = ilCnt2;
									ilRc = RC_SUCCESS;
								}
							}
						}
					}
				}
			}
			prlKeyPadEle = ListFindNext(prgKeyPadList);
		}
		return ilRc;
	}
	static int PrepareSingleActions(KEYPAD_INFO *prpKeyPad,char *pcpActions,int ipRow,int ipButton)
	{
		int ilRc = 0;
		int ilCnt = 0;
		int ilCnt2 = 0;
		int ilCnt3 = 0;
		int ilNrOfActions = 0;
		int ilNrOfAllTables = 0;
		int ilNrOfFields = 0;
		BOOL blSpecialAction = FALSE;
		BOOL blProcessState = FALSE;
		BOOL blStateAlreadySet = FALSE;
		char pclAction[S_BUFF];
		char pclTable[S_BUFF];
		char pclField[S_BUFF];
		char pclActionTables[S_BUFF];
		char pclActionFields[S_BUFF];
		char pclDBSelection[L_BUFF];
		char pclResult[L_BUFF];
		char pclNow[XXS_BUFF];

	if ((ilNrOfActions=(int)GetNoOfElements(pcpActions,',')) > 0)
	{
		memset(pclActionTables,0x00,S_BUFF);
		memset(pclActionFields,0x00,S_BUFF);
		GetDataItem(pclActionTables,prpKeyPad->act_table,ipButton,';',"","");	
		GetDataItem(pclActionFields,prpKeyPad->act_field,ipButton,';',"","");	
		/* looking for single actions */

		for (ilCnt=1;ilCnt<=ilNrOfActions;ilCnt++)
		{
			memset(pclAction,0x00,S_BUFF);
			memset(pclTable,0x00,S_BUFF);
			memset(pclField,0x00,S_BUFF);
			GetDataItem(pclAction,pcpActions,ilCnt,',',"","");	
			GetDataItem(pclTable,pclActionTables,ilCnt,',',"","");	
			GetDataItem(pclField,pclActionFields,ilCnt,',',"","");	
			dbg(DEBUG,"PrepareSingleActions: %d.ACTION<%s> TABLE<%s> FIELD<%s>",ilCnt,pclAction,pclTable,pclField);

			/*******************************************/
			/* checking and running predefined actions */
			/*******************************************/
			/* setting a utc timestamp */
			if (strcmp(pclAction,"#TS_UTC") == 0)
			{
				memset(pclNow,0x00,XXS_BUFF);
				TimeToStr(pclNow,0,0,1);
				strcpy(pclAction,pclNow);
				if ((ilRc = RunSingleAction(prpKeyPad,pclAction,pclTable,pclField,ipRow,ipButton,ilCnt)) != RC_SUCCESS)
				{
					dbg(TRACE,"PrepareSingleActions: RunSingleAction(#TS_UTC) failed!");
				}
				blSpecialAction = TRUE;
			}
			/*****************************/
			/* setting a local timestamp */
			/*****************************/
			if (strcmp(pclAction,"#TS_LOC") == 0)
			{
				memset(pclNow,0x00,XXS_BUFF);
				TimeToStr(pclNow,0,0,0);
				strcpy(pclAction,pclNow);
				if ((ilRc = RunSingleAction(prpKeyPad,pclAction,pclTable,pclField,ipRow,ipButton,ilCnt)) != RC_SUCCESS)
				{
					dbg(TRACE,"PrepareSingleActions: RunSingleAction(#TS_LOC) failed!");
				}
				blSpecialAction = TRUE;
			}
			/**********************************************/
			/* actions will be rolled back (set to blank) */
			/**********************************************/
			if (strcmp(pclAction,"#CL") == 0)
			{
				memset(pclAction,0x00,S_BUFF);
				strcpy(pclAction," ");	
				if ((ilRc = RunSingleAction(prpKeyPad,pclAction,pclTable,pclField,ipRow,ipButton,ilCnt)) != RC_SUCCESS)
				{
					dbg(TRACE,"PrepareSingleActions: RunSingleAction(#CL) failed!");
				}
				blSpecialAction = TRUE;
			}
			/*************************************/
			/* running none special actions here */
			/*************************************/
			if (blSpecialAction == FALSE)
			{
				if ((ilRc = RunSingleAction(prpKeyPad,pclAction,pclTable,pclField,ipRow,ipButton,ilCnt)) != RC_SUCCESS)
				{
					dbg(TRACE,"PrepareSingleActions: RunSingleAction(%s) failed!",pclAction);
				}
			}
			blSpecialAction = FALSE;
		}
	}
	return ilRc;
}
static int RunSingleAction(KEYPAD_INFO *prpKeyPad,char *pcpAction, 
														char *pcpTable,char *pcpField,int ipRow,int ipButton,int ipActionNr)
{
	int ilRc = 0;
	char pclDBSelection[L_BUFF];
	char pclDBSelection2[L_BUFF];
	char pclResult[M_BUFF];
	static char pclUrno[XXS_BUFF];
	static char pclAurn[XXS_BUFF];
	BOOL blActionDone = FALSE;

	dbg(DEBUG,"RunSingleAction: ##################### %d. action ###################",ipActionNr);
	/******************************************/
	/* setting the selection for the DB-query */
	/******************************************/
	memset(pclDBSelection,0x00,L_BUFF);
	if (bgUseHopo = TRUE) 
	{
		sprintf(pclDBSelection,"WHERE RTYP LIKE '%s%%' AND RNAM='%s' AND DSEQ=%d AND HOPO='%s'",
					rgMid.mid_type,prpKeyPad->location,ipRow,pcgHomeAp);	
	}
	else
	{
		sprintf(pclDBSelection,"WHERE RTYP LIKE '%s%%' AND RNAM='%s' AND DSEQ=%d",
						rgMid.mid_type,prpKeyPad->location,ipRow);	
	}
	if (ipActionNr == 1)
	{
		dbg(DEBUG,"RunSingleAction: cmd-RTA (URNO,AURN) <%s>",pclDBSelection);
		/*****************************************************************/
		/* getting FLD-URNO and AFT-URNO for updating the recors properly*/
		/* sqlhdl always needs URNO to process it completly => action    */
		/*****************************************************************/
		memset(pclResult,0x00,M_BUFF);
		if ((ilRc=SendEvent("RTA",pclDBSelection,"URNO,AURN"," ",NULL,0,igWriteQueue
												,mod_id,PRIORITY_4,"FLD","MID-AURN-REQ","",pclResult)) != RC_SUCCESS)
		{
			dbg(TRACE,"RunSingleAction: request for (URNO,AURN) on FLD%s failed with <%d>.",pcgTabEnd,ilRc);
		}
		else
		{
			memset(pclUrno,0x00,XXS_BUFF);
			memset(pclAurn,0x00,XXS_BUFF);
			GetDataItem(pclUrno,pclResult,1,',',"","");
			GetDataItem(pclAurn,pclResult,2,',',"","");
			dbg(DEBUG,"RunSingleAction: FLD-URNO=<%s>, FLD-AURN=<%s>",pclUrno,pclAurn);
		}
	}

	/*****************************************************************/
	/* depending on the table to update different actions are needed */
	/*****************************************************************/
	/* looking for AFT updates */
	if (strncmp(pcpTable,"AFT",3) == 0)
	{
		if (strlen(pclAurn) > 0)
		{
			dbg(DEBUG,"RunSingleAction: AFT-URNO for update <%s>.",pclAurn);
			memset(pclDBSelection,0x00,L_BUFF);
			sprintf(pclDBSelection,"WHERE URNO=%s",pclAurn);

			dbg(DEBUG,"RunSingleAction: cmd-UFR Table<%s>,Field<%s>,Data<%s>",pcpTable,pcpField,pcpAction);
			dbg(DEBUG,"RunSingleAction: cmd-UFR selection<%s>",pclDBSelection);
			if ((ilRc=SendEvent("UFR",pclDBSelection,pcpField,pcpAction,NULL,0,7800
													,mod_id,PRIORITY_4,pcpTable,"MID-UPDATE","",NULL)) != RC_SUCCESS)
			{
				dbg(TRACE,"RunSingleAction: UPDATE on %s%s failed with <%d>.",pcpTable,pcgTabEnd,ilRc);
			}
		}
		blActionDone = TRUE;
	}

	/* looking for FLD updates */
	if (strncmp(pcpTable,"FLD",3) == 0)
	{
		if (strlen(pclUrno) > 0)
		{
			memset(pclDBSelection,0x00,L_BUFF);
			dbg(DEBUG,"RunSingleAction: FLD-URNO for update <%s>.",pclUrno);
			sprintf(pclDBSelection,"WHERE URNO=%s" ,pclUrno);

			dbg(DEBUG,"RunSingleAction: cmd-URT Table<%s>,Field<%s>,Data<%s>",pcpTable,pcpField,pcpAction);
			dbg(DEBUG,"RunSingleAction: cmd-URT selection<%s>",pclDBSelection);
			if ((ilRc=SendEvent("URT",pclDBSelection,pcpField,pcpAction,NULL,0,igWriteQueue
													,mod_id,PRIORITY_4,pcpTable,"MID-UPDATE","",NULL)) != RC_SUCCESS)
			{
				dbg(TRACE,"RunSingleAction: UPDATE on %s%s failed with <%d>.",pcpTable,pcgTabEnd,ilRc);
			}
		}
		blActionDone = TRUE;
	}

	if (blActionDone == FALSE)
	{
		dbg(TRACE,"RunSingleAction: Don't know how to handle update on table <%s>. Doing nothing!",pcpTable);
		dbg(TRACE,"RunSingleAction: Currently only AFT-table/FLD-table configurations are supported!");
	}
	return ilRc;
}

/* **************************************************************** */
/* The SendEvent routine                                         */
/* prepares an internal CEDA-event                                  */
/* **************************************************************** */
static int SendEvent(char *pcpCmd,char *pcpSelection,char *pcpFields, char *pcpData,
											char *pcpAddStruct, int ipAddLen,int ipModIdSend,int ipModIdRecv,
											int ipPriority,char *pcpTable,char *pcpType,char *pcpFile,char *pcpResult)
{
	int     ilRc        		= RC_FAIL;
	int     ilLen        		= 0;
  EVENT   *prlOutEvent    = NULL;
  BC_HEAD *prlOutBCHead   = NULL;
  CMDBLK  *prlOutCmdblk   = NULL;
	char pclFileBuffer[L_BUFF];
	char pclNow[XXS_BUFF];

	if (ipModIdSend == 0)
	{
		dbg(TRACE,"SendEvent: Can't send event to ModID=<0> !");
	}
	else
	{
		/* size-calculation for prlOutEvent */
		ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) +
						strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + ipAddLen + 10;
		
		/*dbg(DEBUG,"Total-Len = %d+%d+%d+%d+%d+%d+%d+%d = %d",sizeof(EVENT),sizeof(BC_HEAD),sizeof(CMDBLK),strlen(pcpSelection),strlen(pcpFields),strlen(pcpData),ipAddLen,10,ilLen);*/

		/* memory for prlOutEvent */
		if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
		{
			dbg(TRACE,"SendEvent: cannot malloc <%d>-bytes for outgoing event!",ilLen);
			prlOutEvent = NULL;
		}
		else
		{
			/* clear whole outgoing event */
			memset((void*)prlOutEvent, 0x00, ilLen);

			/* set event structure... */
			prlOutEvent->type	     		= SYS_EVENT;
			prlOutEvent->command   		= EVENT_DATA;

			/*if (ipModIdRecv == igSecondQueue)*/
			if (ipModIdRecv == mod_id)
			{
				/*prlOutEvent->originator  = (short)igSecondQueue;*/
				prlOutEvent->originator  = (short)mod_id;
			}
			else
			{
				prlOutEvent->originator  = (short)mod_id;
			}
			prlOutEvent->retry_count  = 0;
			prlOutEvent->data_offset  = sizeof(EVENT);
			prlOutEvent->data_length  = ilLen - sizeof(EVENT); 

			/* BC_HEAD-Structure... */
			prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
			prlOutBCHead->rc = (short)RC_SUCCESS;
			strncpy(prlOutBCHead->dest_name,pcgProcessName,10);
			strncpy(prlOutBCHead->recv_name, "EXCO",10);

			/* Cmdblk-Structure... */
			prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
			strcpy(prlOutCmdblk->command,pcpCmd);
			strcpy(prlOutCmdblk->obj_name,pcpTable);
			strcat(prlOutCmdblk->obj_name,pcgTabEnd);
			
			/* setting tw_start entries */
			sprintf(prlOutCmdblk->tw_start,"%s,%s",pcpType,pcpFile);
			
			/* setting tw_end entries */
			sprintf(prlOutCmdblk->tw_end,"%s,%s,%s",pcgHomeAp,pcgTabEnd,pcgProcessName);
			
			/* setting selection inside event */
			strcpy(prlOutCmdblk->data,pcpSelection);

			/* setting field-list inside event */
			strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1,pcpFields);

			/* setting data-list inside event */
			strcpy((prlOutCmdblk->data +
						(strlen(pcpSelection)+1) +
						(strlen(pcpFields)+1)),pcpData);

			if (pcpAddStruct != NULL)
			{
				memcpy((prlOutCmdblk->data +
							(strlen(pcpSelection)+1) +
							(strlen(pcpFields)+1)) + 
							(strlen(pcpData)+1),pcpAddStruct,ipAddLen);
			}

			/*DebugPrintEvent(DEBUG,prlOutEvent);*/ 
			/*snapit((char*)prlOutEvent,ilLen,outp);*/

			dbg(DEBUG,"SendEvent: <%d> --<%s>--> <%d>",prlOutEvent->originator,pcpCmd,ipModIdSend);

			if ((ilRc = que(QUE_PUT,ipModIdSend,ipModIdRecv,ipPriority,ilLen,(char*)prlOutEvent))
				!= RC_SUCCESS)
			{
				dbg(TRACE,"SendEvent: QUE_PUT to <%d> returns: <%d>",ipModIdSend,ilRc);
				HandleQueErr(ilRc);
			}
			else
			{
				if (pgLogFile)
				{
					memset(pclNow,0x00,XXS_BUFF);
					TimeToStr(pclNow,0,0,0);
					memset(pclFileBuffer,0x00,L_BUFF);
					sprintf(pclFileBuffer,"%s: C=<%s> -> MODID<%d> T=<%s> F=<%s> S=<%s> D=<%s>"
						,pclNow,pcpCmd,ipModIdSend,pcpTable,pcpFields,pcpSelection,pcpData);
					fwrite(pclFileBuffer,sizeof(char),strlen(pclFileBuffer),pgLogFile);
					fwrite("\n",sizeof(char),1,pgLogFile);
					fflush(pgLogFile);
				}
			}
			/* free memory */
			free((void*)prlOutEvent); 

			/*if (ipModIdRecv == igSecondQueue)*/
			if (ipModIdRecv==mod_id && ilRc==RC_SUCCESS)
			{
				/*dbg(DEBUG,"SendEvent: LISTEN on <%d> for ACK.",igSecondQueue);*/
				dbg(DEBUG,"SendEvent: LISTEN on <%d> for ACK.",mod_id);
				do	
				{
					/*ilRc = que(QUE_GETBIG,0,igSecondQueue,PRIORITY_3,igItemLen,(char *)&prgItem);*/
					ilRc = que(QUE_GETBIG,0,mod_id,ipPriority,igItemLen,(char *)&prgItem);
					/* depending on the size of the received item  */
					/* a realloc could be made by the que function */
					/* so do never forget to set event pointer !!! */
					prgEvent = (EVENT *) prgItem->text;	
					if( ilRc == RC_SUCCESS )
					{
						/* Acknowledge the item */
						/*ilRc = que(QUE_ACK,0,igSecondQueue,0,0,NULL);*/
						ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
						if( ilRc != RC_SUCCESS ) 
						{
							/* handle que_ack error */
							HandleQueErr(ilRc);
						} /* fi */
					
						switch(prgEvent->command)
						{
							case	EVENT_DATA	:
								if((ctrl_sta == HSB_STANDALONE) ||
									(ctrl_sta == HSB_ACTIVE) ||
									(ctrl_sta == HSB_ACT_TO_SBY))
								{
									ilRc = HandleInternalData(pcpResult);
								}
								else
								{
									dbg(TRACE,"SendEvent: wrong HSB-status <%d>",ctrl_sta);
									DebugPrintItem(TRACE,prgItem);
									DebugPrintEvent(TRACE,prgEvent);
								}/* end of if */
								break;
							default	:
								dbg(TRACE,"SendEvent: unknown/invalid event for Ack! Ignoring!");
								DebugPrintItem(TRACE,prgItem);
								DebugPrintEvent(TRACE,prgEvent);
								break;
						}
					} 
					else
					{
						/* Handle queuing errors */
						HandleQueErr(ilRc);
					} /* end else */
				}while (ilRc != RC_SUCCESS);
			}
		}
	}
	return ilRc;
}
/* *********************/
/* The CheckButtonState  */
/* *********************/
static void CheckButtonState(KEYPAD_INFO *prpKeyPad,int ipRow,int ipButton,int *ipNowActiveButton)
{
	int ilRc = 0;
	int ilPosInFields = 0;
	int ilCol = 0;
	int ilFromStart = 0;
	int ilCnt = 0;
	int ilStateToDel = 0;
	int ilActiveButton = 0;
	char pclButton[XXS_BUFF];
	char pclRow[XS_BUFF];
	char pclTmpBuff[XS_BUFF];
	char pclClrAction[XS_BUFF];

	dbg(DEBUG,"CheckButtonState: KEYPAD<%s> ROW<%d> BUTTON<%d>",prpKeyPad->location,ipRow,ipButton);

	/* button is a usual button, not the clear one */
	if (ipButton != prpKeyPad->ilClearButton)
	{
		prpKeyPad->clKeyState[ipRow-1][ipButton-1] = 0x31;
	}
	else
	{

		for (ilCnt=1; ilCnt<ipButton; ilCnt++)	
		{
			if (prpKeyPad->clKeyState[ipRow-1][ilCnt-1] == 0x31)
			{
				ilStateToDel = ilCnt;
			}
		}

		if (ilStateToDel != 0)
		{
			dbg(DEBUG,"CheckButtonState: State to delete = ROW<%d> BUTTON<%d>",ipRow,ilStateToDel);
			prpKeyPad->clKeyState[ipRow-1][ilStateToDel-1] = 0x30;

			/* now search the action to take if this button is cleared */
			memset(pclButton,0x00,XXS_BUFF);
			memset(pclClrAction,0x00,XS_BUFF);
			memset(pclTmpBuff,0x00,XS_BUFF);

			sprintf(pclButton,"%d,",ilStateToDel);
			if ((ilRc=FindItemInList(rgMid.clr_policy,pclButton,';',&ilPosInFields,&ilCol,&ilFromStart)) == RC_SUCCESS)
			{
				GetDataItem(pclTmpBuff,rgMid.clr_policy,ilPosInFields,';',"","");
				GetDataItem(pclClrAction,pclTmpBuff,2,',',"","");
				dbg(DEBUG,"CheckButtonState: BUTTON<%d> #CL-Action => <%d>",ilPosInFields,atoi(pclClrAction));
				memset(pclRow,0x00,XS_BUFF);
				GetDataItem(pclRow,rgMid.button_offset,ipRow,';',"","");
				memset(pclButton,0x00,XXS_BUFF);
				GetDataItem(pclButton,pclRow,atoi(pclClrAction),',',"","");
				prpKeyPad->clKeyState[ipRow-1][atoi(pclClrAction)-1] = 0x31;
				*ipNowActiveButton = atoi(pclButton);
			}




			#if 0 /* was only used if the previous state had to be determined automatically */
			for (ilCnt=1; ilCnt<ilStateToDel; ilCnt++)	
			{
				/*dbg(DEBUG,"CheckButtonState: Check = BUTTON<%d>",ilCnt);*/
				if (prpKeyPad->clKeyState[ipRow-1][ilCnt-1] == 0x31)
				{
					ilActiveButton = ilCnt;
					/*dbg(DEBUG,"CheckButtonState: Found = BUTTON<%d>",ilCnt);*/
					memset(pclRow,0x00,XS_BUFF);
					GetDataItem(pclRow,rgMid.button_offset,ipRow,';',"","");
					memset(pclButton,0x00,XXS_BUFF);
					GetDataItem(pclButton,pclRow,ilCnt,',',"","");
					*ipNowActiveButton = atoi(pclButton);
				}
			}
			#endif

			dbg(DEBUG,"CheckButtonState: Active button   = ROW<%d>,BUTTON<%d>,ADRESS<%d>",ipRow,atoi(pclClrAction),*ipNowActiveButton);
		}
	}
	/*snapit((char*)&prpKeyPad->clOldKeyState[0][0],100,outp);*/
	snapit((char*)&prpKeyPad->clKeyState[0][0],100,outp);
}
/*********************************************************************/
/*********************************************************************/
/****************** FUNCTIONS ARE NOT NEEDED BUT *********************/
/****************** THEY MAY BE USED IN THE FUTURE *******************/
/*********************************************************************/
#if 0
/*********************************************************************/
Function : Identify_MID_Bc()
Paramter : IN: pcpBuffer = received broadcast message
Return Code: RC_SUCCESS, RC_FAIL
Result:
Description: identifies brodcasts from dZine and extracts the IP-address
             if it is from dZine.
*********************************************************************/
static int Identify_MID_Bc(char *pcpBuffer)
{
	int 				ilRc = RC_FAIL;
	USHORT 			slId;
	char 			pclBuff[XS_BUFF];
	char 			pclIp[XS_BUFF];
	char 			pclTime[XS_BUFF];
	struct in_addr		rlIn;

	memset(pclBuff,0x00,XS_BUFF);
	memcpy(pclBuff,(void*)pcpBuffer,2);

	if (pclBuff[0]==pcgMidId[0] && pclBuff[1]==pcgMidId[1])
	{
		ilRc = RC_SUCCESS;
	}
	return ilRc;
}
/*********************************************************************
Function : IdentifyDev()
Paramter : IN: pcpIpAddress = IP-address of the device that has to be
                              identified (if it is configured into the
															system or not)
Return Code: RC_SUCCESS, RC_FAIL
Result:
Description: checks if this device is the correct one  to get the connection
*********************************************************************/
static int IdentifyDev(struct sockaddr_in *rpSockInfo)
{
	dbg(DEBUG,"IdentifyDev: compare device <%s> and <%s>"
		,(char*)inet_ntoa(rpSockInfo->sin_addr),rgMid.mid_ip);
	if (strcmp((char*)inet_ntoa(rpSockInfo->sin_addr),rgMid.mid_ip)==0)
	{
		/* found it */
		dbg(DEBUG,"IdentifyDev: OK! <%s> will accept() on port<%s>."
			,pcgHostIp,rgMid.mid_tcpport);
		return RC_SUCCESS;
	}
	return RC_FAIL;
}
#endif
