
/**
 * Title:       Stringlist.class
 * Description: Contains methods for managing the data container Stringlist
 * Copyright:   Copyright (c) <p>
 * Company:     ABB AAT
 * @author		BSC
 * @version 	1.0
 */
 
 
import java.util.List;
import java.util.LinkedList;


/** Stringlist CLASS<BR><BR>
	This class provides methods for the use of the Stringlist data container.
*/
public class Stringlist {

  private List Data;

/** CONSTRUCTOR 
*/
  public Stringlist() {
    	Data = new LinkedList();
  }


/** DESTRUCTOR 
*/
  protected void finalize() {
    	Data = null;
  }

/** Method addString
	@param value: Value to be added.
	Adds a String to the given Stringlist.
*/
  public boolean addString(String value) {
    	return Data.add(value);
  }


/** Method removeString
	@param index: Position of String, which shall be removed.
	Removes a String from the given Stringlist.
*/
  public String removeString(int index) throws IndexOutOfBoundsException {
    	return ((String)Data.remove(index));
  }


/** Method getString
	@param index: Position of String, which shall be returned.
	Returns a String from the given Stringlist.
*/
  public String getString(int index) throws IndexOutOfBoundsException {
		try {
    		return ((String)Data.get(index));
    	}
    	catch (IndexOutOfBoundsException E) {
      		return " ";
    	}
  }


/** Method setString
	@param index: Position of String, which shall be returned.
	@param value: Value to be added.
	Sets a String in the given Stringlist at position index.
*/
  public void setString(int index, String value) throws IndexOutOfBoundsException {
    	Data.set(index, value);
  }


/** Method size
		Returns the size of the given Stringlist.
*/
  public int size() {
    	return Data.size();
  }
} // EO CLASS
 /***************************************************************************************/
 /*																						*/
 /* 				EO CLASS Stringlist			 										*/
 /*																						*/
 /***************************************************************************************/

