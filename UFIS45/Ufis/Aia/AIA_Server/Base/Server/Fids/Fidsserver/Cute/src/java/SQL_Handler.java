
/**
 * Title:       SQL_Handler.class
 * Description: Enable access to and from CEDA via the CDI API
 * Copyright:   Copyright (c) 2000<p>
 * Company:     ABB AAT
 * @author		MOS
 * @version 	1.0
 */


import java.sql.*;
import java.math.*;
import java.lang.Boolean;
import Log;
import java.util.*;
import java.io.*;
import sun.misc.*;
import com.javaexchange.dbConnectionBroker.*;




/** SQL_Handler CLASS<BR><BR>
	The SQL_Handler class contains methods for accessing the local databuffer
	(MYSQL) and the AODB (ORACLE). Its methods are mostly related to the
	in the local database defined objects, which have all one or two SQL statements.
	
	The DB access is done by retrieving a connection from one of the two databse broker pools
	(one for MYSQL and one for ORACLE). Both pools are initialized once in the 
	superclass SQL_HandlerSuperclass of the SQL_Handler class.
	
	By using this mean, database connection persistency as well as avoiding
	the overhead of creating a connection are ensured.
	
	The configuration files for both database pools are in the /home/ceda/tomcat/conf directory. 
*/
public class SQL_Handler extends SQL_HandlerSuperclass {

  /** Constructor 
  		Create Connections to databases with logging 
*/
  private Log lg;

 	public SQL_Handler(boolean fullconn, Log logger) throws SQLException,
                              ClassNotFoundException, IllegalAccessException,
                              InstantiationException {
     		lg = logger;

 } /* constructor */

	


  /** Appends blanks till the string is tolength long
   *  @param input Input string.
   *  @param Desired string length.
   */
 private String fill_with_blank(String input, int tolength) {
    	while (input.length() < tolength)
      		input = input + " ";
      	return input;
 } /* fill_with_blank */








/** Method, which queries the AODB and inserts the object data into the local databuffer
	@param	whereclause : Object id for which this operation shall be done
	@param	countername	: Name of Gate or Checkin Counter
	According to its parameters, the methods loads all object data or if parameters are given,
	only the selected objects will be actualised
*/
 public synchronized boolean fillTempDB(String whereclause, String countername) {
  	Connection MySQL		= null;
    	Statement 	Stmttemp 	= null;		
    	String 		SQL_Stmt, SQL_Clear_Stmt;
    	int 		i, j;
    	ListofStringlists extradata;

System.out.println("SQL_Handler fillTempDB STARTED..."  + whereclause);

    	try {
		MySQL = myBrokerMYSQL.getConnection();
     		Stmttemp = MySQL.createStatement();

    		if ((whereclause == null) || (whereclause.equals(""))) {
      				SQL_Stmt = "select pkobjectid from object order by pkobjectid";
      				SQL_Clear_Stmt = "delete from objectdata";
      				Stmttemp.executeUpdate(SQL_Clear_Stmt);
    		} else {
      				SQL_Stmt = "select pkobjectid from object where dtupdatecommand = \"" + whereclause + "\" order by pkobjectid";
  System.out.println("SQL_Handler fillTempDB STARTED 2 ..."  + countername);
 	
					// Retrieve the currently chosen LOGO from the FLDTAB
					//if ((whereclause.indexOf("LOGO") != -1) && ((countername != null) || (countername.equals("")))) {
					if ((whereclause.indexOf("LOGO") != -1) && (countername.length() > 0)) {
							if (whereclause.equals("LOGOC")) 
									extradata = executeQuery("SELECT FLGU FROM FLZTAB WHERE  FLDU in (SELECT URNO FROM FLDTAB WHERE RNAM=\'" + countername + "\' AND RTYP=\'CKIF\' and DSEQ>0)", 1);				
							else
									extradata = executeQuery("SELECT FLGU FROM FLZTAB WHERE  FLDU in (SELECT URNO FROM FLDTAB WHERE RNAM=\'" + countername + "\' AND RTYP=\'GTD1\' and DSEQ>0)", 1);				
		
							String sel_logo;

							if ((extradata == null) || (extradata.size(0) == 0) || (extradata.getString(0, 0) == null)) {
									sel_logo = "";
							} else {
									sel_logo = extradata.getString(0, 0);
							}
							if (whereclause.equals("LOGOC"))  {
								executeQuery("update user set dtlogo = \'" + sel_logo + "\' where dtcounter = \'" + countername + "\' and dttyp = \'C\'", 0);
							} else {
								executeQuery("update user set dtlogo = \'" + sel_logo + "\' where dtcounter = \'" + countername + "\' and dttyp = \'G\'", 0);
							}
							return true;
				
					} //LOGO


					// Retrieve the currently chosen REMARK from the FLDTAB, AFTAB, OR CCATAB
					//if ((whereclause.substring(0, 4).equalsIgnoreCase("REMA")) && (countername != null) && (!countername.equals(""))) {
					if ((whereclause.substring(0, 4).equalsIgnoreCase("REMA")) && (countername.length() > 0)) {
							if (whereclause.equals("REMARKC")) {
									extradata = executeQuery("SELECT CREC FROM FLDTAB WHERE RNAM=\'" + countername + "\' AND RTYP=\'CKIF\' and DSEQ>0", 1);				
									System.out.println("SQL_Handler filltempDB: Remarkc for counter: " + countername + " read");
							}
							else {
									/*extradata = executeQuery("select od.dt4 from user u, userobjects uo, object o, objectdata od where u.dtcounter = \'" +
															countername + "\' and u.pkuserid = uo.pkuserid and uo.pkobjectid = o.pkobjectid and o.fkobjectgroup " +
															"= \'Listbox\' and o.pkobjectid = od.fkobject_ID and od.dtselected=1", 0);*/
									/*if (extradata.size(0) != 0) */
									extradata = executeQuery("SELECT STAT FROM FLDTAB WHERE RNAM=\'" + countername + "\' AND RTYP=\'GTD1\' and DSEQ>0", 1);				
									System.out.println("SQL_Handler filltempDB: Remarkg for GATE: " + countername + " read");
							}		
		
				
							// REMARK ALREADY CHOSEN ??
							String sel_remp;
							if ((extradata == null) || (extradata.size(0) == 0) || (extradata.getString(0, 0) == null))
									sel_remp = "";
							else
									sel_remp = extradata.getString(0, 0);
			
							// Set the remark in the local data buffer
							if (whereclause.equals("REMARKC")) {
								executeQuery("update user set dtremark = \'" + sel_remp + "\' where dtcounter = \'" + countername + "\' and dttyp = \'C\'", 0);								
							} else {
								executeQuery("update user set dtremark = \'" + sel_remp + "\' where dtcounter = \'" + countername + "\' and dttyp = \'G\'", 0);								
							}
							return true;
					}// REMARK
		} // whereclause empty

      	ListofStringlists objectids = executeQuery(SQL_Stmt,0);
      	j = objectids.size(0);
      	System.out.println("SQL_Handler filltempDB: LIST SIZE " + j + " " + SQL_Stmt);
      
      	for (i = 0; i < j; i++) {
          		try {
            			lg.Log("SQL_Handler filltempDB: writing: " + objectids.getString(0,i) + "|" + SQL_Stmt + "|" + countername);
          		}
          		catch (Exception E) {System.out.println("fillTempDB: " + E.getMessage());}
	  
	  			// ONLY IF OBJECTNAME IS GIVEN
	  			if ((whereclause != null) && (!whereclause.equals(""))) {
          				Stmttemp.executeUpdate("delete from objectdata where fkobject_ID = \"" + objectids.getString(0,i) + "\"");
	  			}
        		setTempObjectdata(objectids.getString(0,i), getMainObjectdata(objectids.getString(0,i), 0),countername);
        }

   } catch (SQLException E) {
      System.out.println("SQLException: " + E.getMessage());
      System.out.println("SQLState:     " + E.getSQLState());
      System.out.println("VendorError:  " + E.getErrorCode());
      return false;
    }
	
	
    finally {
            	try{
		if(Stmttemp != null) {
			Stmttemp.close();}
		} catch(SQLException e1){};
            	// The connection is returned to the Broker
            	myBrokerMYSQL.freeConnection(MySQL);
		return true;
    }

   }




  /** Reads all data for the object identified by Objectid from the "main" (ceda)
   *  database. To achieve this it looks into the local database and fetches
   *  the sql statement needed from the "object" table.
   *  @param Objectid Object id in local database.
   *  @param Col_cnt Amount of Columns  to be returned by this method in case you don't
   *  need all of them.
   */

  public ListofStringlists getMainObjectdata(String Objectid, int Col_cnt) {

    	int 		i, j, ccnt,  urno, order;
    	boolean 	Flightlist 				= false, Status = false, FText = false, empty = true;
    	Statement 	Stmttemp				= null;
		Statement   Stmtmain				= null;
    	ListofStringlists result 			= null;
  		Connection MySQL1					= null;
  		Connection Oracle1					= null;

    	try {
      		if (Objectid.substring(0, 6).equalsIgnoreCase("Flight"))
        		Flightlist = true;
      		if (Objectid.substring(0, 6).equalsIgnoreCase("Status"))
        		Status = true;
      		if (Objectid.substring(0, 6).equalsIgnoreCase("FreeRe"))
        		FText = true;
    	}
    	catch (IndexOutOfBoundsException E) {
      		ccnt = Col_cnt;
    	}

    	try {
		
			// Get connections
			MySQL1 = myBrokerMYSQL.getConnection();
			Oracle1 = myBrokerORACLE.getConnection();

			//Prepare the statements
      		Stmttemp = MySQL1.createStatement();
      		Stmtmain = Oracle1.createStatement();

      		System.out.println("SQL_HANDLER: Statements prepared for " + Objectid);

      		if (Stmttemp.execute("select dtsqlstatement, dtsqlstatement2 from object where" +
        				" pkobjectid = \"" + Objectid + "\"")) {

        				ResultSet RSSql = Stmttemp.getResultSet(); // fehler abfangen
        				if (!RSSql.next()) {
          						System.err.println("SQL_HANDLER check local database with statement <" + 
										"select dtsqlstatement, dtsqlstatement2 from object where" +
        								" pkobjectid = \"" + Objectid + "\"" + ">");
          						return result;
        				}

        				String sql_stmt = RSSql.getString(1);
        				String sql_stmt2 = RSSql.getString(2);
        				if (sql_stmt == null) {
          						System.err.println("SQL_HANDLER No data found - check local database");
          						return result;
        				}

						System.out.println("SQL_HANDLER statement executed <" + sql_stmt + ">");
        				if ((Stmtmain.execute(sql_stmt))) {

          						ResultSet RS = Stmtmain.getResultSet();// fehler ??
          						ResultSetMetaData RSMeta = RS.getMetaData();

          						if (Col_cnt == 0)
            							ccnt = RSMeta.getColumnCount();
          						else
            							ccnt = Col_cnt;

          						if (ccnt > RSMeta.getColumnCount())
            							ccnt = RSMeta.getColumnCount();
          						i = 1;
          						try {
            							while (!RSMeta.getColumnName(i).equalsIgnoreCase("URNO")) {
              									System.out.println(RSMeta.getColumnName(i));
              									i++;
            							}
          						}
          						catch (SQLException E) {
	  									if (FText) 
											return fillfreeText(Objectid);
										else {
            								System.err.println("SQL_HANDLER URNO not found");
            								return null;
	    								}
          						}
          						urno = i;
	
          						if (Flightlist)
            							ccnt = 5; // with gate 1, 2

          						ListofStringlists data = new ListofStringlists(ccnt + 2);
          						data.addString("dtOrder", 1);
          						data.addString("dtSelected", 2);


          						if (Flightlist || Status) {
            							// auch unten �ndern
            							data.addString("URNO", 0);
            							j = 3;
            							for (i = 1; i <= ccnt; i++) {
              									if (i != urno) {
                											data.addString(RSMeta.getColumnName(i), j);
                											j++;
              									}
           								}
          						}
          						else {
            							data.addString("pkUrno", 0);
            							for (i = 1; i < ccnt; i++) {
              									data.addString("dt" + Integer.toString(i), i + 2);
            							}
          						}
          						order = 0;
								
								// Now insert the data, in case of a flight list just insert the first record
								// except in case of a CODE SHARE flight or flights with a stod within + 15 min
          						while (RS.next()) {
            							empty = false;
          								// write urno, order, selected
										System.out.println("SQL_HANDLER Urno: " + RS.getString(urno));
	    								if ((RS.wasNull()) && FText) {
	    										RS.close();
												Stmttemp.close();
												Stmtmain.close();
	    										return fillfreeText(Objectid);
										}

            							data.addString(RS.getString(urno), 0);
            							data.addString(Integer.toString(order), 1);
            							data.addString("0", 2);
            							order++;
            							j = 3;
            							for (i = 1; i <= ccnt; i++) {
              									if (i != urno) {
                										data.addString(RS.getString(i), j);
                										j++;
              									}
           								}

          						}
          						RS.close();
          						Stmttemp.close();

								// Now execute the second statement if the first statement didn't return any values
								// In case of the Data for the Checkin Counter Flight List execute both!
          						if (empty || (Objectid.indexOf("FLIGHTC")!=-1)) {
								// COMMON if (empty) {
            							if ((sql_stmt2 != "") && (sql_stmt2 != null)) {
												if (Objectid.indexOf("FLIGHTC")==-1) {
              										data = null;
												}
              									empty = true;
		System.out.println("SQL_Handler 2nd statement 1 " + sql_stmt2);
              									if ((Stmtmain.execute(sql_stmt2))) {
                												RS = Stmtmain.getResultSet();
                												RSMeta = RS.getMetaData();
                												if (Col_cnt == 0)
                  														ccnt = RSMeta.getColumnCount();
                												else
                  														ccnt = Col_cnt;
System.out.println("SQL_Handler 2nd statement 1 " + ccnt);
                									i = 1;
                									try {
                  										while (!RSMeta.getColumnName(i).equalsIgnoreCase("URNO")) {
                    											System.out.println("SQL_Handler 2nd statement:" + RSMeta.getColumnName(i));
                    											i++;
                  										}
                									}
                									catch (SQLException E) {
														if (FText) 
																return fillfreeText(Objectid);
														else {
                  												System.err.println("URNO not found");
                  												return null;
		  												}
                									}
                									urno = i;

                									if (Flightlist)
                  											ccnt = 5; // with gate 1, 2

													// If FLIGHTC object then add data to list of dedicated flights
													// if not create new ListofStringlists
													if (Objectid.indexOf("FLIGHTC")!=-1) {
															//ListofStringlists datacopy = new ListofStringlists(ccnt2);
															//datacopy = data;
															//data = null;
													} else {
                										data = new ListofStringlists(ccnt + 2);
														data.addString("dtOrder", 1);
                										data.addString("dtSelected", 2);
													}
													
													/* old version
                										data = new ListofStringlists(ccnt + 2);
														data.addString("dtOrder", 1);
                										data.addString("dtSelected", 2);
													*/
													

													//System.out.println("still ok - parsing oracle result");

                									if ((Flightlist || Status) && (Objectid.indexOf("FLIGHTC")==-1)) {
                  									// COMMON if (Flightlist || Status) {	
															data.addString("URNO", 0);
                  											j = 3;
                  											for (i = 1; i <= ccnt; i++) {
                    												if (i != urno) {
                      														data.addString(RSMeta.getColumnName(i), j);
																			//System.out.println("Metaindex: " + i);
                      														j++;
                    												}
                  											}
                									}
                									else if (Objectid.indexOf("FLIGHTC")==-1) {
													// COMMON else {
                  										data.addString("pkUrno", 0);
                  										for (i = 1; i < ccnt; i++) {
                    											data.addString("dt" + Integer.toString(i), i + 2);
                  										}
                									}
                									while (RS.next()) {
                  											empty = false;
															// PLEASE  CHECK THE FOLLOWING LINE
															if (data==null) {
													System.out.println("SQL_Handler COMMONTEST: null"); 		
															} else {
System.out.println("SQL_Handler COMMONTEST:" + "size" + data.size());
}
                  											data.addString(RS.getString(urno), 0);
		  													if ((RS.wasNull()) && FText) {
		  															RS.close();
																	Stmttemp.close();
																	Stmtmain.close();
	    															return fillfreeText(Objectid);
															}
                  											data.addString(Integer.toString(order), 1);
                  											data.addString("0", 2);
                  											order++;
                  											j = 3;
                  											for (i = 1; i <= ccnt; i++) {
                    													if (i != urno) {
                      															data.addString(RS.getString(i), j);
                      															j++;
                    													}
                  											}

                									}// for all datarecords
                									RS.close();
                									Stmttemp.close();
              									}
            							}
            							else {
              									System.err.println("No data available");
              									Flightlist = false;
            							}
        						}
        						Stmtmain.close();

								if (empty)
										System.out.println("SQL_HANDLER Empty");
								if (FText)
										System.out.println("SQL_HANDLER FreeText");		
								if (empty && FText)
        								result = fillfreeText(Objectid);
        						if (Flightlist)
          								result = extractMainFlightdata(data, Col_cnt);
       	 						else if (Status)
          								result = extractStatus(data, Col_cnt);
        						else
          								result = data;
        				}
      		}
      		return result;
    	}
    	catch (SQLException E) {
      		System.out.println("SQLException: " + E.getMessage());
      		System.out.println("SQLState:     " + E.getSQLState());
      		System.out.println("VendorError:  " + E.getErrorCode());
      		return null;
    	}

    finally {
            try{if(Stmttemp != null) {Stmttemp.close();}} catch(SQLException e1){};
            try{if(Stmtmain != null) {Stmtmain.close();}} catch(SQLException e1){};            
            // The connection is returned to the Broker
            myBrokerMYSQL.freeConnection(MySQL1);
            myBrokerORACLE.freeConnection(Oracle1);
        }

  } /* getMainObjectdata */




/** Method for retrieving the data of an object from the AODB
	The data will be returned in an ListofStringlists object
	@param	Objectid : object, for which the actual data shall be loaded
	@param	Col_cnt	 : No of Columns to retrieve
*/	
  public ListofStringlists getTempObjectdata(String Objectid, int Col_cnt) {

    	Statement 	Stmttemp 		= null;
    	String 		sql_stmt, empty = new String("");
    	int 		i, j, ccnt 		= 0;
    	ListofStringlists data 		= null;
  		Connection 	MySQL2			= null;


    	try {
		
		// Get connections
		MySQL2 = myBrokerMYSQL.getConnection();
      		Stmttemp = MySQL2.createStatement();

      		if (Stmttemp.execute("select og.* from objectgroup og, object o " +
            		"where og.pkobjectgroup = o.fkobjectgroup and o.pkobjectid = \"" +
            			Objectid + "\"")) {
        			ResultSet RS = Stmttemp.getResultSet();
        			if (RS.next()) {
          					i = 1;
          					j = RS.getMetaData().getColumnCount();
          					while ((i <= j) && (!empty.equals(RS.getString(i)))) {
            						i++;
            						if (!RS.wasNull())
             	 						ccnt++;
          					}
        			}
        			else {
          					System.err.println("SQL_HANDLER getTempObjectdata - please check the local database for assigned objects");
          					return data;
        			}
      		}
      		else {
        			System.err.println("SQL_HANDLER getTempObjectdata - please check the local database for assigned objects");
        			return data;
      		}


      		if (Col_cnt > ccnt)
        			Col_cnt = ccnt;

      		Stmttemp.close();


      		// TEMPORARY
      		ccnt -= 4;

      		Stmttemp = MySQL2.createStatement();
      		//System.out.println("Lines count: " + ccnt);
      		//System.out.flush();

      		sql_stmt = "select pkUrno, dtOrder, dtSelected";
      		for (i = 0; i < ccnt; i++)
        			sql_stmt += ", dt" + (i + 1);
      		sql_stmt += " from objectdata where fkObject_ID = \"" + Objectid +
                  "\" order by dtOrder asc";


     		if (Stmttemp.execute(sql_stmt)) {
        			ResultSet RS = Stmttemp.getResultSet();
        			ResultSetMetaData RSMeta = RS.getMetaData();
        			if (Col_cnt == 0)
          					Col_cnt = RSMeta.getColumnCount();
        			data = new ListofStringlists(Col_cnt);

					//System.out.println("still ok - parsing mysql result");
        			for (i = 0; i < Col_cnt; i++) {
          				data.addString(RSMeta.getColumnName(i + 1), i);
        			}
        			while (RS.next())
          				for (i = 0; i < Col_cnt; i++)
            					data.addString(RS.getString(i + 1), i);
       				RS.close();
        			Stmttemp.close();
      		}
      		return data;
    	}
    	catch (SQLException E) {
      		System.out.println("SQLException: " + E.getMessage());
      		System.out.println("SQLState:     " + E.getSQLState());
      		System.out.println("VendorError:  " + E.getErrorCode());
      		return null;
    	}
		
    finally {
            try{if(Stmttemp != null) {Stmttemp.close();}} catch(SQLException e1){};
            // The connection is returned to the Broker
            myBrokerMYSQL.freeConnection(MySQL2);
        }
		
  } /* getTempObjectdata */




/** Method, which inserts the new object data into the local databuffer.
	@param 	Objectid 	: Unique ID of the object, for which the data shall be inserted
	@param	data		: ListofStringlists, which contains the new data
*/	
public  void setTempObjectdata(String Objectid, ListofStringlists data, String countername) {

    	Statement Stmttemp	 			= null;
    	String Sql_statement;
    	int i, j, k, ccnt; 
    	ListofStringlists chosendata 			= null;
    	boolean blequals				= false;
    	StringTokenizer tokens 				= null;
    	int ilNoOfTokens 				= 0;
    	int ilCnt 					= 0;
	int ilflightposcheck 				= 0;
    	String token 					= "";
    	Connection MySQL3				= null;
  
    
    	i = 1;

    	try {
		// Get connections
		MySQL3 = myBrokerMYSQL.getConnection();
      		Stmttemp = MySQL3.createStatement();

      		if (Stmttemp.execute("select og.* from objectgroup og, object o where " +
           		 "o.pkobjectid = \"" + Objectid + "\" and o.fkobjectgroup = og.pkobjectgroup")) {
        			ccnt = (Stmttemp.getResultSet().getMetaData().getColumnCount() - 1);  // - pkobjectgroup column
      		} else {
        			System.err.println("Fehler in der lokalen Datenbank");
        			return;// false;
      		}

      Stmttemp.close();



		// First delete the old data for the related object
      	Stmttemp = MySQL3.createStatement();
	    Stmttemp.executeUpdate("delete from objectdata where fkObject_ID = \"" +  Objectid + "\"");	    
		System.out.println("SQL_Handler setTempObjectdata: Deleting old entry(ies) for " + Objectid );
     	Stmttemp.close();


		//In case of a cluster switch
		if (data==null) {return;}
		
		
      	if (ccnt > data.size())
        	ccnt = data.size();

      	j = data.size(0);
        System.out.println("inserting data: " + Integer.toString(j) + " times");
	
	// Compare the actual Urno with the urno from the
	ilflightposcheck = 0;

      	while (i < j) {

        		Stmttemp = MySQL3.createStatement();

        		if (Stmttemp.execute("select pkUrno from objectdata where pkUrno = \"" +
          				data.getString(0, i) + "\" and fkObject_ID = \"" + Objectid +
          				"\"")) {

          				Stmttemp.getResultSet().next();
        		}

          		Sql_statement = "insert into objectdata (fkObject_ID, pkUrno, dtOrder," +
                          " dtSelected";

				// Get the related counter	
          		for (k = 3; k < ccnt; k++)
            			Sql_statement += " ,dt" + (k - 2);

          			Sql_statement += ") values (\"";
          			Sql_statement += Objectid + "\", \"" +
          			data.getString(0, i).trim() + "\", " +
          			data.getString(1, i).trim() + ", ";

	  			// Now the dtselected field has to be set to 1 if the data has been chosen by any user
	  			// FLIGHT CHECKIN Object??, then check for the FLNO
	  			blequals=false;
	  			if (Objectid.indexOf("FLIGHTC")!=-1) {
	  				//TODO FIND C and extract number afterwards
					
	  				chosendata = executeQuery("select CTYP,AURN,RURN from fldtab where RNAM = \'" + Objectid.substring(Objectid.length()-(Objectid.length()-1-Objectid.indexOf("C"))) + "\' and RTYP = \'CKIF\' and DSEQ > 0",1);
					if (chosendata.size(0) ==0) { 
						// COMPARE WITH STATUS URNO IF STATUS=OPENED				
						Sql_statement += data.getString(2, i).trim();
					} else {
						// Now extract the CTYP content like M12 = Main and first and second code share flight
						ilNoOfTokens = chosendata.getString(0,0).trim().length();
							
						for (ilCnt=0; ilCnt<ilNoOfTokens; ilCnt++) {
							if ((data.getString(0, i).indexOf(chosendata.getString(1,0).trim())!=-1) || (data.getString(0, i).indexOf(chosendata.getString(2,0).trim())!=-1)) {					
								// Now check the psoition
								token = "" + chosendata.getString(0,0).trim().charAt(ilCnt);
								if (token.equals("M")) {token ="0";}
								if (token.equals(Integer.toString(ilflightposcheck))) {
									Sql_statement += 1;
									ilflightposcheck++;
									blequals = true;
									break;
								}
								ilflightposcheck++;
							} // equals
						}//all tokens
						if (blequals==false) {
							Sql_statement += data.getString(2, i).trim();
						}
					}	
					  		
	  			// FLIGHT GATE Object??, then check for the FLNO
	  			} else if (Objectid.indexOf("FLIGHTG")!=-1) {
	  					chosendata = executeQuery("select CTYP,AURN from fldtab where RNAM = \'" + Objectid.substring(Objectid.length()-3) + "\' and RTYP = \'GTD1\'  and DSEQ > 0",1);
 						if (chosendata.size(0) ==0) { 
							// COMPARE WITH STATUS URNO IF STATUS=OPENED
							Sql_statement += data.getString(2, i).trim();
						} else {
 							// Now extract the CTYP content like M12 = Main and first and second code share flight
							ilNoOfTokens = chosendata.getString(0,0).trim().length();
 							
							for (ilCnt=0; ilCnt<ilNoOfTokens; ilCnt++) {
								if (data.getString(0, i).indexOf(chosendata.getString(1,0).trim())!=-1) {					
									// Now check the psoition
									token = "" + chosendata.getString(0,0).trim().charAt(ilCnt);
									if (token.equals("M")) {token ="0";}
									if (token.equals(Integer.toString(ilflightposcheck))) {
										Sql_statement += 1;
										ilflightposcheck++;
										blequals = true;
										break;
									}
									ilflightposcheck++;
								} else {
									ilflightposcheck = 0;
								}
							}//all tokens
							if (blequals==false) {
								Sql_statement += data.getString(2, i).trim();
							}
						}
						chosendata = null;	  					
	  			// Remark?? then check for the remark string
	  			} else  {
	  	 				Sql_statement += data.getString(2, i).trim();
	  			}


				//
				for (k = 3; k < ccnt; k++)
            				Sql_statement += ", \"" + data.getString(k, i).trim() + "\"";

        			Sql_statement += ")";
 
System.out.println("SQL_Handler setTempObjectdata: " + Sql_statement);
        		if ((Stmttemp.executeUpdate(Sql_statement)) != 0) {
          			Stmttemp.close();
        		}
        		else {
          			System.out.println("Statement failed");
        		}
        		i++;
      	} // while
    }
    catch (SQLException E) {
      System.out.println("SQLException: " + E.getMessage());
      System.out.println("SQLState:     " + E.getSQLState());
      System.out.println("VendorError:  " + E.getErrorCode());

      try {
        lg.Log("Error in settempobjectdata");
      }
      catch (Exception exc) {}
    }
	
	
    finally {
            try{if(Stmttemp != null) {Stmttemp.close();}} catch(SQLException e1){};
            // The connection is returned to the Broker
            myBrokerMYSQL.freeConnection(MySQL3);
        }
	
  } /* setTempObjectdata */









/** Method, which returns the object owned by the given user
	@param pkobjectgroup : 	Group of object (eg Listbox, Free Text)
	@param IP:				IP of the current user
*/

  public Stringlist getUserobject(String IP, String pkobjectgroup) {

	Statement Stmttemp 		= null;
  	Stringlist userobject 	= new Stringlist();
 	Connection MySQL4		= null;
	
    try {

	// Get connections
	MySQL4 = myBrokerMYSQL.getConnection();
      	Stmttemp = MySQL4.createStatement();

      	if (Stmttemp.execute("select distinct o.pkobjectid, o.dtSection, o.dtCommand " +
          "from object o, user u, userobjects uo " +
          "where u.dtConnect = 1 and u.dtIP = \"" + IP + "\" and " +
          "u.pkuserid = uo.pkuserid and " +
          "uo.pkobjectid = o.pkobjectid and o.fkobjectgroup = \"" +
          pkobjectgroup + "\"")) {

        ResultSet RS = Stmttemp.getResultSet();

        if (RS.next()) {
          userobject.addString(RS.getString(1));
          userobject.addString(RS.getString(2));
          userobject.addString(RS.getString(3));
        }
      }
    }
    catch (SQLException E) {
      System.out.println("SQLException: " + E.getMessage());
      System.out.println("SQLState:     " + E.getSQLState());
      System.out.println("VendorError:  " + E.getErrorCode());
    }

    finally {
            try{if(Stmttemp != null) {Stmttemp.close();}} catch(SQLException e1){};
            // The connection is returned to the Broker
            myBrokerMYSQL.freeConnection(MySQL4);
      		return userobject;
    }
	
  } /* getUserobject */








/** Method, which returns all the data of the given user
	@param username : related user name
*/

  public Stringlist getUserdata(String userip) {

    int i, j;
    Stringlist data 	= null;
	Statement Stmttemp 	= null;
 	Connection MySQL5	= null;

    try {

		// Get connections
		MySQL5 = myBrokerMYSQL.getConnection();
      	Stmttemp = MySQL5.createStatement();

      	if (Stmttemp.execute("select * from user where" +
       	 	" dtIP = \"" + userip + "\"")) {

        	ResultSet RS = Stmttemp.getResultSet();
        	data = new Stringlist();

        	j = RS.getMetaData().getColumnCount() + 1;
        	if (RS.next())
          		for (i = 1; i < j; i++)
					if (RS.getString(i)==null) {
							data.addString(" ");
					} else {
							data.addString(RS.getString(i));
					}
      	}		

    }
    catch (SQLException E) {
      System.out.println("SQLException: " + E.getMessage());
      System.out.println("SQLState:     " + E.getSQLState());
      System.out.println("VendorError:  " + E.getErrorCode());
    }

    finally {
            try{if(Stmttemp != null) {Stmttemp.close();}} catch(SQLException e1){};
            // The connection is returned to the Broker
            myBrokerMYSQL.freeConnection(MySQL5);
			return data;
    }
	
  } /* getUserdata */










/** Method, which eturns required user data
	@param username : related user name
	Returnvalues: Password, IP
*/

  public Stringlist getUserlogindata(String username) {

    int i, j;
    Stringlist data 	= null;
	Statement Stmttemp 	= null;
 	Connection MySQL6	= null;

    try {

		// Get connections
		MySQL6 = myBrokerMYSQL.getConnection();

      	Stmttemp = MySQL6.createStatement();
System.err.println("looking for user: " + username);
System.err.println("select dtPwd, dtIP from user where" +
        " dtName = \"" + username + "\"");
      if (Stmttemp.execute("select dtPwd, dtIP from user where" +
        " dtName = \"" + username + "\"")) {

        ResultSet RS = Stmttemp.getResultSet();
        data = new Stringlist();

        j = RS.getMetaData().getColumnCount() + 1;
        if (RS.next())
          for (i = 1; i < j; i++)
            data.addString(RS.getString(i));
      }

    }
    catch (SQLException E) {
      System.out.println("SQLException: " + E.getMessage());
      System.out.println("SQLState:     " + E.getSQLState());
      System.out.println("VendorError:  " + E.getErrorCode());
    }

    finally {
            try{if(Stmttemp != null) {Stmttemp.close();}} catch(SQLException e1){};
            // The connection is returned to the Broker
            myBrokerMYSQL.freeConnection(MySQL6);
			return data;
    }

} /* getUserlogindata */







/** Method, which registers the user
	@param username : related user name
*/

  public void Connectuser(String username,String IP) {

    int i, j;
    Stringlist data 	= null;
	Statement Stmttemp 	= null;
 	Connection MySQL7	= null;

    try {

	// Get connections
	MySQL7 = myBrokerMYSQL.getConnection();
      	Stmttemp = MySQL7.createStatement();

      if (Stmttemp.executeUpdate("update user set dtConnect = 1 where" +
        " dtName = \"" + username + "\" and dtIp = \"" + IP + "\"") == 0) {

        /* error */
      }
    }
    catch (SQLException E) {
      System.out.println("SQLException: " + E.getMessage());
      System.out.println("SQLState:     " + E.getSQLState());
      System.out.println("VendorError:  " + E.getErrorCode());
    }
	
    finally {
            try{if(Stmttemp != null) {Stmttemp.close();}} catch(SQLException e1){};
            // The connection is returned to the Broker
            myBrokerMYSQL.freeConnection(MySQL7);
    }
	
  } /* Connectuser */







/** Method to return the main server time
*/

  public String getCedaTime() {

 	Connection Oracle2 = null;
	Statement Stmtmain = null;
	
    try {
		// Get connections
	  Oracle2 = myBrokerORACLE.getConnection();

      Stmtmain = Oracle2.createStatement();
	// Stmtmain.execute("select get_utctime(0,'ATH') from DUAL");
	Stmtmain.execute("select TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS') from DUAL");
      ResultSet RS = Stmtmain.getResultSet();
      RS.next();
	  String returnvalue = new String(RS.getString(1));
	  if (returnvalue.equals("null")) {
	  		returnvalue = " ";
	  }
      return returnvalue;   
    }
    catch (SQLException E) {
      System.out.println("SQLException: " + E.getMessage());
      System.out.println("SQLState:     " + E.getSQLState());
      System.out.println("VendorError:  " + E.getErrorCode());
      return " ";
    }
	
    finally {
            try{if(Stmtmain != null) {Stmtmain.close();}} catch(SQLException e1){};
            // The connection is returned to the Broker
            myBrokerORACLE.freeConnection(Oracle2);
    }
	
  } /* getCedaTime */











/** Method to return a desired time
	@param offset: Offset time in - or +
*/
  public String getTime(int offset) {
  
	Statement Stmtmain = null;
 	Connection Oracle3 = null;
	String soffset;
    try {
	// Get connections
	Oracle3 = myBrokerORACLE.getConnection();

	
      	Stmtmain = Oracle3.createStatement();
	soffset = "" + (offset/1440);
	 
	Stmtmain.execute("select TO_CHAR(SYSDATE + " + soffset + ",'YYYYMMDDHH24MISS') from DUAL");
      	ResultSet RS = Stmtmain.getResultSet();
     	RS.next();
	String returnvalue = new String(RS.getString(1));
	if (returnvalue.equals("null")) {
	  		returnvalue = " ";
	}
      return returnvalue;  
    }
    catch (SQLException E) {
      System.out.println("SQLException: " + E.getMessage());
      System.out.println("SQLState:     " + E.getSQLState());
      System.out.println("VendorError:  " + E.getErrorCode());
      return " ";
    }
	
    finally {
            try{if(Stmtmain != null) {Stmtmain.close();}} catch(SQLException e1){};
            // The connection is returned to the Broker
            myBrokerORACLE.freeConnection(Oracle3);
    }
	
  } /* getTime */









/** Method, which assigns the three received data columns for the flight lists<BR>
	and converts them to one. Code share flights will be mnarked with a J
	@param data	: 	ListofStringlists, which contains the result of the JDBC call
	@param Col_cnt:	No of records to return
*/		
  public ListofStringlists extractMainFlightdata(ListofStringlists data, int Col_cnt) {

    	int 	i, j, k, order, jfno, flno, alc, urno, gatenr;
    	String 	Flight, Tempstr;
		String 	airline	 	= new String("");
	

		// FIND CODE SHARE FLIGHT
    	i = 0;
    	try {	
     		while (!data.getString(i, 0).equals("JFNO")) {
        		i++;
      		}
    	}
    	catch (IndexOutOfBoundsException E) {
      		System.err.println("JFNO not found");
      		return null;
    	}
    	jfno = i;


		// FIND MAIN FLIGHT
    	i = 0;
    	try {
      		while (!data.getString(i, 0).equals("FLNO"))
      			i++;
    	}
    	catch (IndexOutOfBoundsException E) {
      		System.err.println("FLNO not found");
      		return null;
    	}
    	flno = i;


		// FIND AIRLINE 
    	i = 0;
    	try {
      		while (!data.getString(i, 0).equals("ALC3") && !data.getString(i, 0).equals("ALC2")) {
       	 		i++;
      		}
    	}
    	catch (IndexOutOfBoundsException E) {
      		System.err.println("ALC not found");
      		return null;
    	}
    	alc = i;


		// FIND URNO
    	i = 0;
    	try {
      		while (!data.getString(i, 0).equals("URNO"))
        		i++;
    	}
    	catch (IndexOutOfBoundsException E) {
      		System.err.println("URNO not found");
      		return null;
    	}
    	urno = i;
    
		// FIND TAG for GATES. This is necessary fro separating gate1 and gate2 values (gtd1, gtd2)	
    	i = 0;
    	try {
      		while (!data.getString(i, 0).equals("CMD"))
        		i++;
    	}
    	catch (IndexOutOfBoundsException E) {
      		System.err.println("URNO not found");
      		return null;
    	}
    	gatenr = i;


		// Catch wrong column amounts
    	if (Col_cnt == 0)
      		Col_cnt = 7;
    	else if (Col_cnt > 7)
        	Col_cnt = 7;


		// Now create SQL statement for accessing the local data buffer
    	ListofStringlists outdata = new ListofStringlists(Col_cnt);

    	outdata.addString("pkUrno", 0);
    	outdata.addString("dtOrder", 1);
    	outdata.addString("dtSelected", 2);

    	k = data.size();
    	for (i = 3; i < k; i++)
      		outdata.addString("dt" + (i - 2), i);

    	order = 0;

		// Process Main Flights
    	j = data.size(0);
    	for (i = 1; i < j; i++) {

      	outdata.addString(data.getString(urno, i), 0);
      	outdata.addString(Integer.toString(order), 1);
      	outdata.addString("0", 2);
      	outdata.addString("", 3);
      	outdata.addString(data.getString(flno, i), 4);
		
	  	// Process commmon checkin which have no airline
	  	airline = "";
	  	if (data.getString(alc, i) != null) {
	  			airline = "";
				System.out.println("\nEXTRACT COMMON <" + airline +">");
	  	} else {
	  			airline = "null";
				System.out.println("\nEXTRACT COMMON <" + airline +"> + <" + data.getString(flno, i).length() +">");
	  	}
	  	if ((data.getString(flno, i).length()==1) && (airline.equals("null"))) {
	  			airline = "COMMON";
	  	} else {
	  			airline = data.getString(alc, i);
	  	}	  

      	outdata.addString(airline, 5);
      	outdata.addString(data.getString(gatenr, i), 6);

      	order++;

		// Process Code Share Flights
		try {
        	if (data.getString(jfno, i).length() > 1) {
        		Flight = "";
        		Tempstr = data.getString(jfno, i);
        		if (Tempstr == null)
         			Tempstr = "";
        		while (Tempstr.length() > 0) {
          			if (Tempstr.length() > 9) {
            				Flight = Tempstr.substring(0, 9);
            				Tempstr = Tempstr.substring(9, Tempstr.length());
          			}
          			else {
            			Flight = Tempstr;
            			Tempstr = "";
          			}

          			outdata.addString(data.getString(urno, i), 0);
          			outdata.addString(Integer.toString(order), 1);
          			outdata.addString("0", 2);
          			outdata.addString("J", 3);
          			outdata.addString(Flight, 4);
          			outdata.addString(data.getString(alc, i), 5);
	  				outdata.addString(data.getString(gatenr, i), 6);
          			order++;
        		} // while
      		} // length > 1 ?
		} catch (Exception E) {
				System.out.println("ERROR IN extractMainFlightdata <" + E +">");
		}
    } // For all main flights
    return outdata;
  } /* extractMainFlightdata */




/** Method, which retrieves the actual status of the counter.<BR>
	This will be done by accessing the local databuffer.
	@param data	: 	ListofStringlists, which contains the result of the JDBC call
	@param Col_cnt:	No of records to return
*/	
  public ListofStringlists extractStatus(ListofStringlists data, int Col_cnt) {

    	int i, k, urno;


		// Catch wrong column amounts
    	if (Col_cnt == 0)
      			Col_cnt = 5;
    	if (Col_cnt > 5)
      			Col_cnt = 5;

		// Now create SQL statement for accessing the local data buffer
    	ListofStringlists outdata = new ListofStringlists(5); //Col_cnt);

    	outdata.addString("pkUrno", 0);
    	outdata.addString("dtOrder", 1);
    	outdata.addString("dtSelected", 2);

    	i = 0;
    	try {
      		while (!data.getString(i, 0).equals("URNO")) {
        		i++;
      		}
    	}
    	catch (IndexOutOfBoundsException E) {
     		urno = 0;
    	}
	
    	urno = i;
    	k = data.size();

    	for (i = 3; i < 5; i++)
      			outdata.addString("dt" + (i - 2), i);

		// Add data
    	if (data.size(0) > 1)
      		outdata.addString(data.getString(urno, 1), 0);
    	else
      		outdata.addString("Closed", 0);
    	outdata.addString("0", 1);      	// Order not needed
    	outdata.addString("0", 2);      	// Selected not needed
    	outdata.addString("60", 4);    		// Refresh time (1 min)

    	if (data.size(0) > 1)
      		outdata.addString("1", 3);		// STATUS = OPENED
    	else
      		outdata.addString("0", 3);		// STATUS = CLOSED

    	i = outdata.size();
    	while (i > Col_cnt) {
      		outdata.removeList(i - 1);
      		i--;
    	}
    return outdata;
  } /* extractStatus */





/** Method to replace null values in the local databuffer
	@param Objectid : unique id of free text object
*/
  public ListofStringlists fillfreeText(String Objectid) {

		ListofStringlists result = new ListofStringlists(4);

		result.addString("pkUrno", 0);
    	result.addString("dtOrder", 1);
    	result.addString("dtSelected", 2);
    	result.addString("dt1", 3);

    	result.addString(Objectid, 0);
    	result.addString("0", 1);
    	result.addString("0", 2);
    	result.addString(" ", 3);

    	return result;
  } /* fillfreeText */




/**
  Executes any SQL  statements on the local database
  @param SQL		: 	Statement to execute
  @param database	: 	0 = MYSQL
  						1 = ORACLE
  */
  public ListofStringlists executeQuery(String SQL,int database) {

    int ilCols 				= 0;
    int ilRows 				= 1;
    int i;
    ListofStringlists data 	= null;
    Statement Stmttemp 		= null;
 	Connection Oracle5 = null;
 	Connection MySQL8 = null;
	
	
	try {
	
    	if ((SQL == null) || (SQL.equals(""))) {
      		return data;
    	} else {
			if (database==0) {
				// Get connections
				MySQL8 = myBrokerMYSQL.getConnection();
      				Stmttemp = MySQL8.createStatement();
			} else {
				// Get connections
				Oracle5 = myBrokerORACLE.getConnection();
	
      				Stmttemp = Oracle5.createStatement();	
			}
          	if (SQL.substring(0, 6).equalsIgnoreCase("Update")) {
            		Stmttemp.executeUpdate(SQL);
            		return null;
          	}
		
      		if (Stmttemp.execute(SQL)) {
        			ResultSet RS = Stmttemp.getResultSet();
					//RS.next();
			
					// How man columns??
					ResultSetMetaData RSMeta = RS.getMetaData();
			
					// Return if none are retrieved
					ilCols = RSMeta.getColumnCount();
			
					// Create the lists
					data = new ListofStringlists(ilCols);
			
					// Get data from all those cols
					if (ilCols==0) {return null;}
			
					while(RS.next()) {							
          				for (i=0;i<ilCols;i++) {
							data.addString(RS.getString(i+1),i);
						}
						ilRows++;
					} //while
					RS.close();
        			Stmttemp.close();
	        } //if
			return data;
	 	}// if SQL not null
    } catch (SQLException E) {
      		System.out.println("SQLException: " + E.getMessage());
      		System.out.println("SQLState:     " + E.getSQLState());
      		System.out.println("VendorError:  " + E.getErrorCode());
      		return null;
    }

    finally {
            try{if(Stmttemp != null) {Stmttemp.close();}} catch(SQLException e1){};
            // The connection is returned to the Broker
			if ((SQL != null) && (!SQL.equals(""))) {
				if (database==0) {
            		myBrokerMYSQL.freeConnection(MySQL8);
				} else {
            		myBrokerORACLE.freeConnection(Oracle5);
				}
			}
    }
 }
/* End Of Function executeQuery */


/** method Testuser
	returns the state of connection of the user with the given IP<BR>
	@param IP: IP address of the current client
*/
public int Testuser(String IP) {

    int i, j;
    Stringlist data 		= null;
	Statement Stmttemp 		= null;
 	Connection MySQL9 = null;

    try {
	// Get connections
	MySQL9 = myBrokerMYSQL.getConnection();

      	Stmttemp = MySQL9.createStatement();

      	if (Stmttemp.execute("select dtConnect from user where dtIP = \"" +
        	IP + "\"")) {

        	ResultSet RS = Stmttemp.getResultSet();
        	if (RS.next()) {
          		if (RS.getInt(1) == 1)
            		return 1;
          	else
            	return 0;
        	}
        	else
          		return -1;
      	}
      	else
        	return -2;
    }
    catch (SQLException E) {
      System.out.println("SQLException: " + E.getMessage());
      System.out.println("SQLState:     " + E.getSQLState());
      System.out.println("VendorError:  " + E.getErrorCode());
      return -1;
    }
	
    finally {
            try{if(Stmttemp != null) {Stmttemp.close();}} catch(SQLException e1){};
            // The connection is returned to the Broker
            myBrokerMYSQL.freeConnection(MySQL9);
    }
	
  } /* Connectuser */





/** Method to retrieve a Urno from the number circle SNOTAB
*/

  public synchronized String getUrno() {

 	Connection Oracle4 = null;
	Statement Stmtmain = null;
	String MINN;
	String MAXN;
	String returnvalue;
	int ilurno;	
	
    try {
	// Get connections
	Oracle4 	= myBrokerORACLE.getConnection();
	Oracle4.setAutoCommit(false);
      	Stmtmain 	= Oracle4.createStatement();
	Stmtmain.executeUpdate("SELECT ACNU,MINN,MAXN FROM NUMTAB WHERE KEYS = 'SNOTAB' FOR UPDATE");
      	ResultSet RS 	= Stmtmain.getResultSet();
      	RS.next();
	returnvalue 	= new String(RS.getString(1));
	MINN 		= new String(RS.getString(2));
	MAXN 		= new String(RS.getString(3));
	ilurno 		= Integer.parseInt(returnvalue);
	RS.close();
	
	
	if (returnvalue.equals("null")) {
		Stmtmain 	= Oracle4.createStatement();
	  	returnvalue 	= " ";
	} else {
		ilurno++;
		if (ilurno > Long.parseLong(MAXN)) {
			ilurno 	= Integer.parseInt(MINN);
		}
		Stmtmain 	= Oracle4.createStatement();
		Stmtmain.executeUpdate("UPDATE NUMTAB SET ACNU = "+ ilurno + " WHERE KEYS = 'SNOTAB'");
	}
      	return returnvalue;  
    }
    catch (SQLException E) {
      System.out.println("SQLException: " + E.getMessage());
      System.out.println("SQLState:     " + E.getSQLState());
      System.out.println("VendorError:  " + E.getErrorCode());
      return " ";
    }
	
    finally {
            try{if(Stmtmain != null) {Stmtmain.close();}} catch(SQLException e1){};
            try{Oracle4.commit();} catch(SQLException e2){};
            try{Oracle4.setAutoCommit(true);} catch(SQLException e3){};
	    
            // The connection is returned to the Broker
            myBrokerORACLE.freeConnection(Oracle4);
    }
	
  } /* getUrno */



} /* class */
 /***************************************************************************************/
 /*																						*/
 /* 				EO CLASS SQL_Handler			 									*/
 /*																						*/
 /***************************************************************************************/

