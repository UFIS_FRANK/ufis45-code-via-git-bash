
/**
 * Title:       Jsptest.class
 * Description: Contains methods, which are used in the Java Server pages
 * Copyright:   Copyright (c) <p>
 * Company:     ABB AAT
 * @author		BSC
 * @version 	1.0
 */

package 	status;
import 		SQL_Handler;
import 		ListofStringlists;
import 		Stringlist;
import 		Log;


/** Jsptest CLASS<BR><BR>
	This class provides methods which are being used from the Java Server pages.
	They all return data which is shown in HTML objects in the server pages.	
*/
public class Jsptest extends Jsp_Superclass {

  /*private String userip;*/
  private int error;



/** Method to retrieve the flights or airlines, which will be shown in the flight lists.<BR>
		@param userip: IP address of requesting client<BR>
			The method queries the local databuffer for flights and airlines.<BR>
			In case the counter or gate is opened, flights or airlines with the value dtseletcted = 1 are
			shown in the list.
			In case of a closed gate or counter (dtseletcted = 0), all airlines or flights
			will be shown.<BR>
			Code share flights are marked with an initial J.<BR>
			Common checkins without airline are shown as COMMON.<BR>
			in case of an inconsitency (Counter or gate is opened and no values with dtseletcted = 1 are available)
			the actual data from CEDA are requested and the dtseletcted field is set again.
			
			Called from work.jsp
*/
  public String getFlight(String userip) {

    	String 	answer, empty 		= new String("");
    	int 	i, j, k, l;
		boolean blflightselected 	= false;
		
		
		
    	try {
			// Retrieve the status from the local databuffer
			// If stat = true then gate or counter is openend
			// In this case the list will be disabled for selctions
      		boolean stat = sh.getTempObjectdata(sh.getUserobject(userip, "Status").getString(0), 0).getString(3, 1).equals("1");
    		if (stat) {
      			answer = " disabled>";
    		} else {
      			answer = ">";
	  		}
			
			
			// Now retrieve the flight  data and the amount of data
    		ListofStringlists list = sh.getTempObjectdata(sh.getUserobject(userip, "Listbox").getString(0), 0);
			
    		j = list.size(0);
lg.Log("JSPtest LISTTEST START for <" + sh.getUserobject(userip, "Listbox").getString(0) + "> with status = <" + stat + "> and size <" + j + ">");
lg.Log("JSPtest LISTTEST START for <" + sh.executeQuery("select dtrequestfinished from object where pkobjectid=\"FLIGHTC002\"",0).getString(0,0) + ">");

			//Are there flights??
			if (j > 1) {
				//For all flights create list
    			for (i = 1; i < j; i++) {
	lg.Log("JSPtest LISTTEST FLIGHT LOOP " + " list0 <" + list.getString(0, i) + ">" + " list1 <" + list.getString(1, i) + ">" + " list2 <" + list.getString(2, i) + ">");
      				if (list.getString(2, i).equals("1") || !stat) {
					
						//	Prepare the value urno_flno or urno_alc3
						//	this is required for the hanlding of code share flights.
						// 	If field list.getString(4, i) is empty then create urno_alc3
        				if ((list.getString(4, i) == null) || (list.getString(4, i).equals(""))) {
          					answer += "<option value=\"" + list.getString(0, i) + "_" + list.getString(5, i) + "\""; // Urno reicht nicht -> combined flights							
        				} else {
          					answer += "<option value=\"" + list.getString(0, i) + "_" + list.getString(4, i) + "\""; // Urno reicht nicht -> combined flights
						}

						
        				answer += "> ";
					
						// Code share flight J
        				if ((list.getString(3, i) == null) || list.getString(3, i).equals("")) {
          					answer += "&nbsp;&nbsp;&nbsp;";
       					} else {
          					answer += "&nbsp;" + list.getString(3, i) + "&nbsp;";
						}
					
						// Now insert the FLNO
        				answer += "&nbsp;&nbsp;&nbsp;";
        				answer += list.getString(4, i);
					
						// 	Flag, if counter is opened and flights are selected
						// 	This flag is used in the section below in case of an opened gate or counter and
						//	no flights?airlines with dtselected  = 1
						if (stat) {
							blflightselected = true;
						}
						
						// 	Fill the flight  list with a predifined amount of spaces
						//	The used font is Courier New
        				l = 9 - list.getString(4, i).length();
       					for (k = 0; k < l; k++) {
				          	answer += "&nbsp;";
		  				}
						// now format the string for porper alignment
       					answer += "&nbsp;&nbsp;&nbsp;&nbsp;";
					
						// Show the airline
        				answer += list.getString(5, i) + "&nbsp;&nbsp;";
        				answer += System.getProperty("line.separator");
lg.Log("JSPtest LISTTEST answer 2<" + answer + ">" + " at <" + i + ">");
      				}
				
    			} //for all selected flights
			
			//No flights are shown despite the fact that the counter is opened
			// Retrieve the values from the AODB
			if ((blflightselected == false) && (stat)) {
			
				//get the urno from the status
				String userlistobject = new String(sh.getUserobject(userip, "Listbox").getString(0));
				lg.Log("JSPtest  NO FLIGHTS 1<" + userlistobject.substring(7, 10) +"> stat: <" + stat +">" + " list0 <" + list.getString(0, 2) + ">" + " list1 <" + list.getString(1, 2) + ">" + " list <" + list.getString(2, 2) + ">");
				
				// Reload the data from CEDA
				sh.setTempObjectdata(userlistobject, sh.getMainObjectdata(userlistobject, 0)," ");
				sh.setTempObjectdata(sh.getUserobject(userip, "Status").getString(0), sh.getMainObjectdata(sh.getUserobject(userip, "Status").getString(0), 0)," ");

				// Now get the urno from the actual flight, for which the counter is opened and update the flightlist
				String urno = sh.getTempObjectdata(sh.getUserobject(userip, "Status").getString(0), 0).getString(0, 1);
				ListofStringlists updateflights = sh.executeQuery("update objectdata set dtselected = 1 where pkUrno = \'" + urno + "\' and fkobject_id = \'" + userlistobject + "\'", 0);
				updateflights = null;

				// now check local databuffer		
				ListofStringlists selectedflights = sh.executeQuery("select dtselected from objectdata where pkurno='" + urno + "\' and fkobject_id = \'" + userlistobject + "\'", 0);

				if (selectedflights == null) { 
					// if flight not in list reload list and check again
						answer += "<option> A counter or gate is still opened";
						answer += "<option> from the previous days.";
						answer += "<option> FLIGHT URNO " + urno;
						answer += "<option> For technical problems call IT&T Help Desk";
						answer += "<option> Tel.: 348 (and select 'UFIS FIDS'";
						answer += "<option> after hearing the voice machine.)";
						lg.Log("JSPtest NO FLIGHTS: Flight not available. Pos 1.");
						return answer;	
				} else {
					if (selectedflights.getString(0, 0).equals("") || selectedflights.getString(0, 0).equals("0")) { 
						// if flight not in list reload list and check again
						answer += "<option> A counter or gate is still opened";
						answer += "<option> from the previous days.";
						answer += "<option> FLIGHT URNO " + urno;
                                                answer += "<option> For technical problems call IT&T Help Desk";
						answer += "<option> Tel.: 348 (and select 'UFIS FIDS'";
						answer += "<option> after hearing the voice machine.)";
						lg.Log("JSPtest NO FLIGHTS: Flight not available. Pos 2.");
						return answer;	
					} else {
						lg.Log("JSPtest NO FLIGHTS: Flight available" + selectedflights.getString(0, 0));
					 	selectedflights = sh.executeQuery("update objectdata set dtselected = 1 where pkUrno = \'" + urno + "\' and fkobject_id = \'" + userlistobject + "\'", 0);
								
					}
				}
				
				// NOW RECREATE THE FLIGHT LIST
				list = null;
				list = sh.getTempObjectdata(sh.getUserobject(userip, "Listbox").getString(0), 0);
			
    			j = list.size(0);
				lg.Log("JSPtest LISTTEST list.size <" + j + ">");

					//For all flights create list
    				for (i = 1; i < j; i++) {
				
      					if (list.getString(2, i).equals("1") || !stat) {


							//	Prepare the value urno_flno or urno_alc3
							//	this is required for the hanlding of code share flights.
							// 	If field list.getString(4, i) is empty then create urno_alc3
        					if ((list.getString(4, i) == null) || (list.getString(4, i).equals(""))) {
          						answer += "<option value=\"" + list.getString(0, i) + "_" + list.getString(5, i) + "\""; // Urno reicht nicht -> combined flights							
        					} else {
          						answer += "<option value=\"" + list.getString(0, i) + "_" + list.getString(4, i) + "\""; // Urno reicht nicht -> combined flights
							}

        					answer += "> ";
					
							// Code share flight J
        					if ((list.getString(3, i) == null) || list.getString(3, i).equals("")) {
          						answer += "&nbsp;&nbsp;&nbsp;";
       						} else {
          						answer += "&nbsp;" + list.getString(3, i) + "&nbsp;";
							};
						
							// Now insert the FLNO
        					answer += "&nbsp;&nbsp;&nbsp;";
        					answer += list.getString(4, i);
					

							// 	Fill the flight  list with a predifined amount of spaces
							//	The used font is Courier New
        					l = 9 - list.getString(4, i).length();
       						for (k = 0; k < l; k++) {
				          		answer += "&nbsp;";
		  					}
							// now format the string for porper alignment
       						answer += "&nbsp;&nbsp;&nbsp;&nbsp;";
					
							// Show the airline
        					answer += list.getString(5, i) + "&nbsp;&nbsp;";
        					answer += System.getProperty("line.separator");
      					}
				
    				} //for all selected flights
				}// if no flights are selected and counter open
		} else {
			//IF NO FLIGHTS ARE SELECTED
			// SHOW NO ALLOCATION PAGE
			answer += "<option> No allocation planned ->";
			answer += "<option> Please contact the ASOC Coordinator for more information";
			answer += "<option> CHECK-IN Tel.: 40003";
                        answer += "<option> GATES Tel.: 40002";
		}
    	return answer;
    }
    catch (Exception E) {
      return "<option><option>For technical problems contact IT&T Help Des.<option> Tel.: 348 (and select 'UFIS FIDS'<option> after hearing the voice machine.)" ;

    }
  }







/** Method to retrieve the remark, which will be shown in the drop down predefined remark list.<BR>
		@param userip: IP address of requesting client<BR>
			The method queries the local databuffer for gates or checkin related remarks<BR>
			and sets the currently shown remark to selected, if the gate or the counter are opened.
			
			Called from work.jsp
*/
  public String getRemark(String userip) {

    String answer 	= new String("");
	String empty 	= new String("");
	String remark;
    int i, j;

    try {
		// Get values from local databuffer
		// All remark values first
    	ListofStringlists list = sh.getTempObjectdata(sh.getUserobject(userip, "Select").getString(0), 0);
		// Then the actually shown remark
		ListofStringlists selremark = sh.executeQuery("select dtremark from user where dtIP='" + userip + "'",0);
		if (selremark.size(0) == 0)
			remark = list.getString(4, 1);
		else
			remark = selremark.getString(0, 0);
			
    	j = list.size(0);

		// Now create the HTML string
    	for (i = 1; i < j; i++) {
      		answer += "<option value=\"" + list.getString(0, i) + "\"";
      		if (list.getString(4, i).equals(remark.trim()))
        			answer += " selected";
      		answer += "> ";
      		answer += list.getString(3, i);
      		answer += System.getProperty("line.separator");
    	}
    	return answer;
    }
    catch (Exception E) {
      return "";
    }
  }






/** Method to retrieve the free texts.<BR>
		@param userip: 	IP address of requesting client<BR>
		@param nr: 		1 = free text no 1; 2 = free text no 2<BR>
			The method queries the local databuffer for free text 1 or 2.
			
			Called from work.jsp
*/
  public String getText(String userip, int nr) {

    try {
    	String text;
    	if (nr == 1) {
      		try {
        		text = sh.getTempObjectdata(sh.getUserobject(userip, "Text1").getString(0), 0).getString(3, 1);
      		}
      		catch (Exception E) {
        		text = "";
      		}
      		return "value=\"" + text + "\"";
    	} else {
      		try {
        		text = sh.getTempObjectdata(sh.getUserobject(userip, "Text2").getString(0), 0).getString(3, 1);
      		}
      		catch (Exception E) {
        		text = "";
      		}	
      		return "value=\"" + text + "\"";
    	}
    }
    catch (Exception E) {
      return "value=\"\"";
    }
  }


/** Method to retrieve the refresh time of the status page.<BR>
		@param userip: 	IP address of requesting client<BR>
			The method queries the local databuffer for the refresh time.
			Currently this feature is disactivated.
			
			Called from status.jsp
*/
  public String getTime(String userip) {

    try {
      ListofStringlists list = sh.getTempObjectdata(sh.getUserobject(userip, "Status").getString(0), 0);
      return list.getString(4, 1);
    }
    catch (Exception E) {
      return " ";
    }
  }



/** Method to retrieve the rstatus of the gate or counter.<BR>
		@param userip: 	IP address of requesting client<BR>
			The method queries the local databuffer for the status.
			Returnvalue 1 means opened, 0 means closed.
			
			Called from status.jsp
*/
  public String getStatus(String userip) {
	
    try {
	String countertype 		= new String("");
      	ListofStringlists list 		= sh.getTempObjectdata(sh.getUserobject(userip, "Status").getString(0), 0);
    	Stringlist userdata 		= sh.getUserdata(userip);

	// CHECKIN or GATE
	if (userdata.getString(5).indexOf("G")!=-1) {
		countertype = " GATE ";
	} else {
		countertype = " CHECK-IN ";
	}
		
	// Now set the OPEN or CLOSED String
      	if (list.getString(3, 1).equals("1"))
        	return "- " + countertype + userdata.getString(8).trim() + "&nbsp;OPENED -";
      	else
        	return "- " + countertype + userdata.getString(8).trim() + "&nbsp;CLOSED -";
    }
    catch(Exception e) {
			//TODO show user friendly message
      		return "PLEASE CALL THE SYSTEM ADMINISTRATOR";
    }
  }





/** Method to retrieve the name of the currently show logo.<BR>
		@param userip: 	IP address of requesting client<BR>
			The method queries the local databuffer for the currently show logo.
			If the resource is closed, then None.gif is shown.
			Otherwise the logo as stored in tehh user table in the local datbuffer
			is shown
			
			Called from logo.jsp
*/
  public String getCurrlogo(String userip) {

     	String returnvalue = new String(" ");
    
    	try {
		// Try first to get the logo from user.dtlogo	
	      	ListofStringlists data = sh.executeQuery("select dtlogo from user where dtIP='" + userip + "'",0);
	
 		if (data.getString(0, 0).equals("")) {
			//get the first logo	
          		returnvalue = "SRC=\"/logo/None.gif\"";
    		} else {
			ListofStringlists logo = sh.executeQuery("select distinct dt3 from objectdata where dt2='" + data.getString(0, 0) + "'",0);	
			returnvalue = "SRC=\"/logo/"+ logo.getString(0, 0) + "\"";
		} //entry empty		
			
		return returnvalue;

    	} catch (Exception e) {
     	 		return "SRC=\"/logo/None.gif\"";
   		}
  }//getCurrlogo




/** Method to retrieve the names of the logos.<BR>
		@param userip: 	IP address of requesting client<BR>
			The method queries the local databuffer for logo names.
			If a logo is already shown for an opened resource
			then this one will be preselected.
						
			Called from logo.jsp
*/
  public String getLogos(String userip) {

    String answer = new String(">"), empty = new String("");
    String dummy;
    int i, j;

    try {
      	ListofStringlists list = sh.getTempObjectdata(sh.getUserobject(userip, "Logo").getString(0), 0);
     	ListofStringlists logo = sh.executeQuery("select dtlogo from user where dtIP='" + userip + "'",0);
		if (logo.size(0) ==0) {
			dummy="WHEN MOSES GOES TO EGYPT LAND";
		} else {
			dummy=logo.getString(0, 0);
		}
      	j = list.size(0);

      	for (i = 1; i < j; i++) {
        	answer += "<option value=\"" + list.getString(4, i) + "\"";
        	if (list.getString(4, i).equals(dummy))
          		answer += " selected";
        	answer += "> ";
        	answer += list.getString(3, i);
        	answer += System.getProperty("line.separator");
      	}
		
      	return answer;
    }
    catch (Exception e) {
	try {lg.Log(" LOGO EXCEPTION <" + e + ">");} catch (Exception l) {};
      return "disabled><option> No Logos available";
    }
  }


} // EO Class
 /***************************************************************************************/
 /*																						*/
 /* 				EO CLASS Jsptest	 												*/
 /*																						*/
 /***************************************************************************************/

