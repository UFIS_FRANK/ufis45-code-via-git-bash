
/**
 * Title:       MainServlet3.class
 * Description: Servlet, which processes the client requests 
 * Copyright:   Copyright (c) 2000<p>
 * Company:     ABB AAT
 * @author		MOS/BSC
 * @version 	1.0
 */

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import Stringlist;
import Log;
import java.sql.SQLException;


/** MainServlet3 CLASS<BR><BR>
	The MainServlet3 class is inherited from the MainServlet_Superclass,
	which is inherited by the HttpServlet class.
	On start of the servlet container, the super class will be loaded and according
	to the number of incoming client requests, several instances of the MainServlet3
	class are being created by the servlet container.
	The super class will be initialised only once.
	
	The MainServlet3 class contains the methods for processing client requests and sending
	the ouptut of the requests back to the clients.
	
	Mainly two functionalities are given by this class:
		login handling
		telegram creation for interaction with CEDA
*/

public class MainServlet3 extends MainServlet_Superclass {


  private int error; // 0 = ok, 1 = no db conn., 2 = not logging poss.,
                            // 3 = sql error


/**	method login
		@param  request:	HttpServletRequest object, which encapsulates the communication from the client to the server
		@param	response	HttpServletResponse object, which encapsulates the communication from the server to the client
			The login method runs through three steps:
				1. User authentication
				2. Resource availabilty check (checkin or gate out of order??)
				3. Data reload and redirection to the index.jsp
				
*/
  private void Login(HttpServletRequest request, HttpServletResponse response)
                                        throws IOException, ServletException
  	{
    	Stringlist ud = null;

		////////////////////////////////////////////////////////////
		//
		//		PART 1: USER AUTHENTICATION
		//
		///////////////////////////////////////////////////////////
		// GET THE PASSWORD AND USER NAME
    	try {
      		ud = sh.getUserlogindata(request.getParameter("username"));
	}
    	catch (Exception E) {
 	     	System.err.println("Exception in getUserlogindata");
    	}

	//NOW REACT ACCORDING TO THE THE USER ENTIRES
    	if (ud == null) {
      		try {
        		// Set the attribute and Forward back to login.jsp
        		request.setAttribute("msg", "Your username does not exist. Please try again <BR>or <BR>call IT&T Help Desk -><BR>Tel.:348 (and select 'UFIS FIDS' after hearing the voice machine.)");
        		getServletConfig().getServletContext().getContext("/cuteif/jsp/" +
          			"login/login.jsp").getRequestDispatcher("/jsp/login/login.jsp").forward(request, response);
      		}
        	catch (Exception E) {
        		E.printStackTrace ();
        		return;
      		}
      		return;
    	}// USER DOESN'T EXIST

	// RIGHT PASSWORD AND NAME??
      	if (ud.size() == 2)
        	if (ud.getString(0) == null) {
          		ud.setString(0, new String(""));
			}

      	if ((ud.size() < 2) ||
          	!(ud.getString(0).toUpperCase().equals(request.getParameter("password").toUpperCase()))) {
        	try {
          		// Set the attribute and Forward back to login.jsp
          		request.setAttribute("msg", "You have entered a wrong password. <BR>Please try again");
          		getServletConfig().getServletContext().getContext("/cuteif/jsp/" +
            		"login/login.jsp").getRequestDispatcher("/jsp/login/login.jsp").forward(request, response);
        	} catch (Exception E) {
          		E.printStackTrace ();
         		return;
        	}
      } else {
        	if (!(ud.getString(1).equals(request.getRemoteAddr()))) {
          		try {
            		// Set the attribute and Forward back to login.jsp
            		request.setAttribute("msg", "You are not allowed to log in from this terminal.<BR> Please contact " +
					"IT&T Help Desk for more information -><BR>Tel.: 348 (and select 'UFIS FIDS' after hearing the voice machine.)");
            		getServletConfig().getServletContext().getContext("/cuteif/jsp/" +
            		"login/login.jsp").getRequestDispatcher("/jsp/login/login.jsp").forward(request, response);
          		} catch (Exception E) {
            		E.printStackTrace ();
            		return;
          		}
        	}
			// Now password and user are correct
        	else {
			// Save the user as connected user in the local databuffer
          		sh.Connectuser(request.getParameter("username"),request.getRemoteAddr());
		////////////////////////////////////////////////////////////
		//
		//		PART 2: RESOURCE AVAILABILITY CHECK
		//
		///////////////////////////////////////////////////////////
					
				String counter = sh.executeQuery("select dtcounter from user where dtName = \'" + request.getParameter("username") + "\'", 0).getString(0, 0);
				String typ = sh.executeQuery("select dttyp from user where dtName = \'" + request.getParameter("username") + "\'", 0).getString(0, 0);
				System.out.println("LOGIN: " + counter);
				String flightid = sh.getUserobject(request.getRemoteAddr(), "Listbox").getString(0);
			
				// IF THE COUNTER IS NOT VALID THEN SHOW ERROR PAGE
				// GATES
				
				if  (typ.indexOf("G")!=-1) {
						ListofStringlists maintenance = sh.executeQuery("select BLKTAB.resn,BLKTAB.burn, TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS') from BLKTAB,GATTAB where " +
							" (((BLKTAB.NAFR< TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS') and BLKTAB.nafr<>' ') AND (BLKTAB.NATO>= TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS') or BLKTAB.nato=' ')) and " +
							" (BLKTAB.BURN=GATTAB.URNO) AND GATTAB.GNAM=\'" + counter + "\')", 1);
						if ((maintenance != null) && (maintenance.size(0) != 0) && (maintenance.getString(1, 0) != null)) {
							System.out.println("MAIN " + maintenance.getString(0,0) + "|" + counter);
									request.setAttribute("msg", "Counter/Gate " + counter + " is currently out of order.<BR>" + 
									maintenance.getString(0,0) + "<BR>" + "Please contact  the ASOC Coordinator for more information<BR>CHECK-IN Tel.: 40003 and GATES Tel.: 40002");
		          					getServletConfig().getServletContext().getContext("/cuteif/jsp/" +
        		    				"error/error.jsp").getRequestDispatcher("/jsp/error/error.jsp").forward(request, response);
						}
				//CHECKIN
				} else {
						ListofStringlists maintenance = sh.executeQuery("select BLKTAB.resn,BLKTAB.burn, TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS') from BLKTAB,CICTAB where " +
							" (((BLKTAB.NAFR< TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS') and BLKTAB.nafr<>' ') AND (BLKTAB.NATO>= TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS') or BLKTAB.nato=' ')) and " +
							" (BLKTAB.BURN=CICTAB.URNO) AND CICTAB.CNAM=\'" + counter + "\')", 1);
						if ((maintenance != null) && (maintenance.size(0) != 0) && (maintenance.getString(1, 0) != null)) {
							System.out.println("MAIN " + maintenance.getString(0,0) + "|" + counter);
									request.setAttribute("msg", "Counter /Gate" + counter + " is currently out of order.<BR>" + 
									maintenance.getString(0,0) + "<BR>" + "Please contact the ASOC Coordinator for more information<BR>CHECK-IN Tel.: 40003 and GATES Tel.: 40002");
		          					getServletConfig().getServletContext().getContext("/cuteif/jsp/" +
        		    				"error/error.jsp").getRequestDispatcher("/jsp/error/error.jsp").forward(request, response);
						}
				}
				
				//First reset the user settings from the previous session
				if (flightid != null) {
						sh.executeQuery("update object set dtrequestfinished=0 where pkobjectid = \'" + flightid + "\'", 0);
				}
		


		////////////////////////////////////////////////////////////
		//
		//		PART 3: DATA RELOAD
		//
		///////////////////////////////////////////////////////////

			////////////////////////////////////////////////////////////
			//
			// FIRST UPDATE FLDTAB IF COUNTER IS CLOSED: SET DSEQ FOR ACTUAL ENTRIES = -1
			// THIS APPLIES IF A RESOURCES HAVE BEEN OPENED AND NOT CLOSED!
			//
			///////////////////////////////////////////////////////////
		    	Stringlist userdata 		= sh.getUserdata(request.getRemoteAddr());
      			Stringlist statusmeta 		= sh.getUserobject(request.getRemoteAddr(), "Status");
      			ListofStringlists status 	= sh.getTempObjectdata(statusmeta.getString(0), 0);
			
			if (status.getString(3, 1).equals("0")) { 
						
				ListofStringlists fldtaburno = sh.executeQuery("SELECT URNO FROM FLDTAB WHERE RNAM='" + userdata.getString(8) +"' AND RTYP = 'CKIF' AND DSEQ > 0", 1);
				if (fldtaburno != null) {				
				   if (fldtaburno.size(0) > 0) {
					String fldurno = fldtaburno.getString(0,0);
											
 					// Add values to list:[0]=section,[1]=command,[2]=values
					Stringlist selflight = new Stringlist();
					selflight.addString("SETDATA");				
					selflight.addString("FUC");
				
					// ADD VALUES
					// LSTU,DSEQ,ACTI,USEU
					if (typ.indexOf("G")!=-1) {
						selflight.addString(sh.getCedaTime() + ",-1," + sh.getCedaTime() + ",CUTE-IF," + fldurno.trim());
					} else {
						selflight.addString(sh.getCedaTime() + ",-1, ,CUTE-IF," + fldurno.trim());
					}
					//Send that stuff
            				ch.put(selflight);
				
					// RESET VALUES
					fldtaburno 	= null;
					selflight 	= null;
					
					// LOAD DATA FIRST AND A SECOND TIME
					// THIS IS A RUNTIME ISSUE, THREAD:SLEEP(1000) DOESN'T WORK HERE
					if (typ.indexOf("G")!=-1) {
						sh.fillTempDB("LOGOG", counter);
						sh.fillTempDB("REMARKG", counter);
						sh.fillTempDB("FREEREMARKG1" + counter, counter);
						sh.fillTempDB("FREEREMARKG2" + counter, counter);
						sh.fillTempDB("FLIGHTG" + counter, counter);
						sh.fillTempDB("STATUSG" + counter, counter);
					} else {
						sh.fillTempDB("LOGOC", counter);
						sh.fillTempDB("REMARKC", counter);
						sh.fillTempDB("FREEREMARKC1" + counter, counter);
						sh.fillTempDB("FREEREMARKC2" + counter, counter);
						sh.fillTempDB("FLIGHTC" + counter, counter);
						sh.fillTempDB("STATUSC" + counter, counter);
					}
				  } //fldtaburno.size(0)
				} //fldtaburno != null	
			}

			if (typ.indexOf("G")!=-1) {
						sh.fillTempDB("LOGOG", counter);
						sh.fillTempDB("REMARKG", counter);
						sh.fillTempDB("FREEREMARKG1" + counter, counter);
						sh.fillTempDB("FREEREMARKG2" + counter, counter);
						sh.fillTempDB("FLIGHTG" + counter, counter);
						sh.fillTempDB("STATUSG" + counter, counter);
			} else {
						sh.fillTempDB("LOGOC", counter);
						sh.fillTempDB("REMARKC", counter);
						sh.fillTempDB("FREEREMARKC1" + counter, counter);
						sh.fillTempDB("FREEREMARKC2" + counter, counter);
						sh.fillTempDB("FLIGHTC" + counter, counter);
						sh.fillTempDB("STATUSC" + counter, counter);
			}

				// Now send the main page if flight data is assigned and the info page if not
          		try {
      				ListofStringlists flightdata = sh.getTempObjectdata(flightid, 0);
					if (flightdata.size(0)!=1) {
		          				getServletConfig().getServletContext().getContext("/cuteif/jsp/" +
       		    				"index/index.jsp").getRequestDispatcher("/jsp/index/index.jsp").forward(request, response);
					} else {
							request.setAttribute("msg", "No allocation planned for Counter or Gate " + counter + " -><BR>" + 
								"Please contact the ASOC Coordinator for more information<BR>CHECK-IN Tel.: 40003 and GATES Tel.: 40002");
		          				getServletConfig().getServletContext().getContext("/cuteif/jsp/" +
        		    				"error/error.jsp").getRequestDispatcher("/jsp/error/error.jsp").forward(request, response);
					}
          		} catch (Exception E) {
            			E.printStackTrace ();
            			return;
          		} // send the page
        	}// correct user and password
      }// wrong password
    }




/**	Method work
		@param  request:	HttpServletRequest object, which encapsulates the communication from the client to the server
		@param	response	HttpServletResponse object, which encapsulates the communication from the server to the client
			The work method is responsible for the creation of the telegrams for all CEDA related actions.
			The data will be stored in ListofStringlists objects, where
				Stringlist No 1 contains the section 	(eg: INFO1)
				Stringlist No 2 contains the command	(eg: UI1)
				Stringlist No 3 contains the data to send	(eg: LH 1111,LH,20001110120000,172.30.0.30,B70)
			
			All sections and commands are configured in the /home/ceda/tomcat/conf/NATIVE_CDI.cfg on the client side
			and /ceda/conf/cdi2_1.cfg,cdi2_3.cfg on the server side.
			
			The related actions are
				Close a resource:
								Update FLDTAB table for the actual resourcename (set DSEQ = 0)
								Update AFTTAB (Gate) or CCATAB (CHECKIN) [Setting of closing times]
				Open a resource:
								Insert data in FLDTAB, FLXTAB, FLZTAB and update AFTTAB (Gate) or CCATAB (CHECKIN) [Setting of opening times]
				Actualize a resource:
								Update entries in the FLDTAB table for the actual flight and resourcename
								Update FLXTAB and FLZTAB for the actual FLDTAB URNO
								Update AFTTAB (Gate) or CCATAB (CHECKIN) [Setting of remarks]
								
			The processing of the clients data will be finished, when the dtrequestfinished field in the local USER table is set to 0.
			
			In case of an error during the sending of update commands from the cdi server, the method waits max. 3.5 secs
			until it returns the previous settings to the user.
			
			For all the updates it is required, that the urno of the datarecord is sent together with the new values.	
			Another requirement is the monitorip, which has to be sent that the dsphdl process is able to 
			identify the destination of the new values.
					
*/
  private void Work(HttpServletRequest request, HttpServletResponse response)
                                        throws IOException, ServletException
  {
    	int i, j, k, l, m, n 				= 1;
    	int ilretries					= 0;
    	int ilFlightCnt					= 0;
    	int ilFlightCnt2				= 0;
    	String userip 					= request.getRemoteAddr();
    	String command 					= request.getParameter("Button");
    	Stringlist senddata 				= null;
    	Stringlist userdata 				= null;
    	String akt_urno 				= "";
    	String resultworkinprogress 			= "1";
    	String updateurno				= "";
	String plainflsc				= "";
	String ccaurno					= "";
	String fldurno					= "";
	String bufferurno				= "";
	String inserturno				= "";
	String checkintype				= "";
	String AOTI					= "";
	String ABTI					= "";
	String AFTI					= "";
	String CKBS					= "";
	String CKES					= "";
	String GD1B					= "";
	String GD1E					= "";
	String flsc			 		= "";
	String remarkcode		 		= "";
	String remarkremi		 		= "";
	String gate1_or_2				= "";
	boolean  blflddata	 			= false;
		
    	Stringlist flightmeta 				= null; 
	Stringlist remarkmeta 				= null; 
	Stringlist text1meta 				= null;
    	Stringlist text2meta 				= null; 
	Stringlist statusmeta 				= null;
    	ListofStringlists flights 			= null; 
	ListofStringlists remark 			= null;
    	ListofStringlists status 			= null;
   
    	ListofStringlists workinprogress 		= null;
    	Stringlist selflight 				= null;


    	// MOS Get the user data for sending the and the counter (like 01 or A01) for the FLDTAB
    	// This is required by the CDIINterface class (action filtering)
    	userdata = sh.getUserdata(userip);

	lg.Log("\n\n***************************************************************************************");
	lg.Log("	PROCESSING NEW REQUEST FROM <" + userip +">	");
	lg.Log("******************************************\n");
		
    	// Get actual UTC time from server
    	String currenttime = sh.getCedaTime();
	
	// Get Resourcename
	String RNAM = userdata.getString(8);
	
	////////////////////////////////////////////////////////////
	//
	//		EXIT BUTTON
	//
	///////////////////////////////////////////////////////////
    	if (command.equals("Exit")) {
		// Disconnect user
		workinprogress = sh.executeQuery("update user set dtConnect=0 where  dtIP=\"" + userip + "\"",0);

  		// SEND GOOD BYE PAGE
    		getServletConfig().getServletContext().getRequestDispatcher("/jsp/" +
            	"goodbye/goodbye.html").forward(request, response);
				
  		return;
    	}



	////////////////////////////////////////////////////////////
	//
	//		CLOSE COUNTER
	//
	///////////////////////////////////////////////////////////
   	else if (command.equals("Close")) {
	
		// IS COUNTER OPENED?? IF not return
      		statusmeta = sh.getUserobject(userip, "Status");
      		status = sh.getTempObjectdata(statusmeta.getString(0), 0);

    	//********************************//
	//Disable multiple button clicking
    	//********************************//
		
		
		// get data from listbox
      		flightmeta = sh.getUserobject(userip, "Listbox");
      		flights = sh.getTempObjectdata(flightmeta.getString(0), 0);

		// How many flights??
      		j = flights.size(0);

		if (status.getString(3, 1).equals("0")) {
			if (j==1) {
				request.setAttribute("msg", "No allocation planned for Counter or Gate" + RNAM.trim() + " -><BR>" + 
							"Please contact the ASOC Coordinator for more information<BR>CHECK-IN Tel.: 40003 and GATES Tel.: 40002");
		       		getServletConfig().getServletContext().getContext("/cuteif/jsp/" +
        		    				"error/error.jsp").getRequestDispatcher("/jsp/error/error.jsp").forward(request, response);
			} else {				
    				getServletConfig().getServletContext().getRequestDispatcher("/jsp/" +
           		 		"index/index.jsp").forward(request, response);				
			}
			return;	
		}

    	//********************************//
	//Disable multiple button clicking END
    	//********************************//




		// Set work in progress flag
		workinprogress = sh.executeQuery("update object set dtrequestfinished=1 where  pkobjectid=\"" + flightmeta.getString(0) + "\"",0);

		// Create CDI Values: different for Checkin and Gate!!!
      		String insertcountertime;

		
		insertcountertime = currenttime + "," + currenttime;
		


      		akt_urno = " ";
      		for (i = 1; i < j; i++) {

        		if ((!akt_urno.equals(flights.getString(0, i))) || (i == j)) {
          			if (flights.getString(2, i).equals("1")) {
            				akt_urno =  flights.getString(0, i);

	 				////////////////////////////////////////////////////////////
					// SELECT URNO FROM CCATAB
					///////////////////////////////////////////////////////////		
     		 			if (statusmeta.getString(2).equals("CCA")) {
						ListofStringlists ccataburno = sh.executeQuery("SELECT URNO FROM CCATAB WHERE CKIC='" + userdata.getString(8) +"' and (FLNU='" + akt_urno + "' or URNO='" + akt_urno + "') and ((ckba=' ') or ((ckba< TO_CHAR(SYSDATE+30/1440,'YYYYMMDDHH24MISS') and ckba<>' ') and (ckea=' ' or ckea> TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS'))))", 1);
						ccaurno = "";
							
		 				if (ccataburno.size(0) == 0) {
    							getServletConfig().getServletContext().getRequestDispatcher("/jsp/" +
           		 										"index/index.jsp").forward(request, response);	
							return;													
						} else {
							ccaurno = ccataburno.getString(0,0);							
						}	
						ccataburno = null;						
 					}



	 				////////////////////////////////////////////////////////////
					//
					// FIRST UPDATE FLDTAB: SET DSEQ FOR ACTUAL ENTRIES = -1
					//
					///////////////////////////////////////////////////////////
					// Add values to list:[0]=section,[1]=command,[2]=values
					selflight = null;						
					selflight = new Stringlist();	
					selflight.addString(statusmeta.getString(1));
					
					selflight.addString("FUC");

					// CHECKIN OR GATE?
					if (statusmeta.getString(2).equals("CCA")) {
						ListofStringlists fldtaburno = sh.executeQuery("SELECT URNO FROM FLDTAB WHERE RNAM='" + RNAM +"' AND RTYP = 'CKIF' AND DSEQ > 0 AND RURN=" + ccaurno, 1);
							
		 				if (fldtaburno.size(0) == 0) {
   							getServletConfig().getServletContext().getRequestDispatcher("/jsp/" +
           		 										"index/index.jsp").forward(request, response);	
							return;													
						} else {
							fldurno = fldtaburno.getString(0,0);							
						}	
						// ADD VALUES
						// LSTU,DSEQ,ACTI,USEU
						selflight.addString(currenttime + ",-1, ,CUTE-IF," + fldurno.trim());
						fldtaburno = null;
							
					} else {
						ListofStringlists fldtaburno = sh.executeQuery("SELECT URNO FROM FLDTAB WHERE RNAM='" + RNAM +"' AND RTYP = 'GTD1' AND DSEQ > 0 AND AURN=" + akt_urno, 1);
		 				if (fldtaburno.size(0) == 0) {
    							getServletConfig().getServletContext().getRequestDispatcher("/jsp/" +
           		 										"index/index.jsp").forward(request, response);	
							return;													
						} else {
							fldurno = fldtaburno.getString(0,0);							
						}
						
						// ADD VALUES
						// LSTU,DSEQ,ACTI,USEU
						selflight.addString(currenttime + ",-1," + currenttime + ",CUTE-IF," + fldurno.trim());
						fldtaburno = null;							

					}
						
					
					//Send that stuff
            				ch.put(selflight);
					
					
					
	 				////////////////////////////////////////////////////////////
					//
					// THEN UPDATE AFTTAB (GATE) OR CCATAB (CHECKIN)
					//
					///////////////////////////////////////////////////////////						
					
					
	     				// Add values to list:[0]=section,[1]=command,[2]=values
      	    				// DIVIDE CHECKIN AND GATE COUNTER
					selflight = null;
					selflight = new Stringlist();
									
					// Now set the closing times
            				selflight.addString(statusmeta.getString(1));



					// Set gate flag
					workinprogress = sh.executeQuery("select dt4 from objectdata where  fkobject_id=\"" + flightmeta.getString(0) + "\" and dtselected = 1 and pkUrno =\"" + status.getString(0,1) + "\"",0);
					if (workinprogress.size(0) !=0) {
						gate1_or_2 = workinprogress.getString(0,0);
					}


            				if (new String("AG1").equalsIgnoreCase(gate1_or_2)) {
              					selflight.addString("G1Y");
            				} else if (new String("AG2").equalsIgnoreCase(gate1_or_2)) {
              					selflight.addString("G2Y");
	    				} else {
						// Checkin Close command
	    					selflight.addString("CCC");
	    				}//            selflight.addString(statusmeta.getString(2));
					
	        			// PREPARE COMMANDS AND DATA FOR CDI
        				if (statusmeta.getString(2).equalsIgnoreCase("CCA")) {
						// UPDATE CCATAB CKES,LSTU,USEU
						selflight.addString(insertcountertime + ",CUTE-IF," + ccaurno.trim());
        				} else {
						// UPDATE AFTTAB GD1Y or GD2Y,LSTU,USEU
          					selflight.addString(insertcountertime + ",CUTE-IF," + akt_urno.trim());
        				}
         							
					//Send that stuff
            				ch.put(selflight);
				
					lg.Log("WORK PUT CLOSE COUNTER:" + selflight.getString(0) + selflight.getString(1) + selflight.getString(2));
            				selflight = null;
          			  } // openend ??	
      				} // CODE SHARE??
      			} //for
				
				//NOW ACTUALIZE THE FREETEXT AND REMARK / LOGO
				if (statusmeta.getString(2).equalsIgnoreCase("CCA")) {
					sh.fillTempDB("FreeRemarkC1" + RNAM,RNAM);
					sh.fillTempDB("FreeRemarkC2" + RNAM,RNAM);
					sh.fillTempDB("REMARKC",RNAM);
					sh.fillTempDB("LOGOC",RNAM);
					//sh.fillTempDB("FreeRemarkC1" + RNAM,RNAM);
				} else {
					sh.fillTempDB("FreeRemarkG1" + RNAM,RNAM);
					sh.fillTempDB("FreeRemarkG2" + RNAM,RNAM);
					sh.fillTempDB("REMARKG",RNAM);
					sh.fillTempDB("LOGOG",RNAM);
					//sh.fillTempDB("FreeRemarkG1" + RNAM,RNAM);
				}
				
				
				
			
    	} // CLOSE COUNTER

	////////////////////////////////////////////////////////////
	//
	//		TRANSMIT BUTTON
	//
	///////////////////////////////////////////////////////////
    	else {
      		statusmeta = sh.getUserobject(userip, "Status");
      		status = sh.getTempObjectdata(statusmeta.getString(0), 0);

      		flightmeta = sh.getUserobject(userip, "Listbox");
      		flights = sh.getTempObjectdata(flightmeta.getString(0), 0);
				
		// Set work in progress flag
		workinprogress = sh.executeQuery("update object set dtrequestfinished=1 where  pkobjectid=\"" + flightmeta.getString(0) + "\"",0);


		////////////////////////////////////////////////////////////
		//
		//		SELECTED REMARK
		//
		///////////////////////////////////////////////////////////
        	String sp = request.getParameter("Select");
       		remarkmeta = sh.getUserobject(userip, "Select");	
        	remark = sh.executeQuery("Select dt2,dt3 from objectdata where pkUrno = \"" + sp + "\" and fkobject_ID=\"" + remarkmeta.getString(0) + "\"",0);

		////////////////////////////////////////////////////////////
		//
		//		FREE TEXT 1 AND FREE TEXT 2
		//
		///////////////////////////////////////////////////////////
        	String tp1 = request.getParameter("Text1");
        	String tp2 = request.getParameter("Text2");
		// Replace any occurence of ',' with ' '
		tp1 = tp1.replace(',',' ');
		tp2 = tp2.replace(',',' ');

		
        	text1meta = sh.getUserobject(userip, "Text1");
        	text2meta = sh.getUserobject(userip, "Text2");

        	String updparam 		= "";
        	Stringlist sendtext1 		= null;
        	Stringlist sendtext2 		= null;
        	String insertcountertime	= "";

 		////////////////////////////////////////////////////////////
		//
		//		FLIGHT LISTBOX FOR FLDTAB
		//
		//	If the counter is opened just send the values to the
		//	FLDTAB. The Flight list to show will be queried from 
		//	the AODB.
		//	In case of a closed counter create this flight list
		//	from the user's listbox selections and send the string
		//	aling with the other formular values to the
		//	FLDTAB.
		///////////////////////////////////////////////////////////
       		String[] lp = request.getParameterValues("Listbox");
				
				
		//Any flights selected??
				
		if ((lp==null) && (status.getString(3, 1).equals("0"))) {
		          getServletConfig().getServletContext().getContext("/cuteif/jsp/" +
       		    		"index/index.jsp").getRequestDispatcher("/jsp/index/index.jsp").forward(request, response);
				return;				
		}
				
		// If counter is open, then fill this list with the values from the local databuffer
		// else take the given values
		// GENERATE CTYP INFO: M for main flight + no for code share flights (eg M12)
		if (status.getString(3, 1).equals("1")) {
			ilFlightCnt = 0;
			j = flights.size(0);
			for (i = 1; i < j; i++) {
           			if (flights.getString(2, i).equals("1")) {
					ilFlightCnt++;
				} // if flight is chosen
			}// for all selected flights
			
			ListofStringlists fldtabctyp;		
			if (statusmeta.getString(2).equalsIgnoreCase("CCA")) {
						fldtabctyp = sh.executeQuery("SELECT DISTINCT CTYP FROM FLDTAB WHERE RNAM='" + RNAM +"' AND RTYP = 'CKIF' AND DSEQ > 0", 1);
			} else {
						fldtabctyp = sh.executeQuery("SELECT DISTINCT CTYP FROM FLDTAB WHERE RNAM='" + RNAM +"' AND RTYP = 'GTD1' AND DSEQ > 0", 1);
			}
		 	if (fldtabctyp.size(0) != 0) {
				flsc = fldtabctyp.getString(0,0).trim();
 			} else {
				flsc = " ";
			}						
			fldtabctyp = null;

			lp = new String[ilFlightCnt];
			ilFlightCnt = 0;							
			for (i = 1; i < j; i++) {
           			 if (flights.getString(2, i).equals("1")) {
					if (flights.getString(4, i).equals("")) {
						lp[ilFlightCnt] = flights.getString(0, i) + "_" + flights.getString(5, i);
					} else {
						lp[ilFlightCnt] = flights.getString(0, i) + "_" + flights.getString(4, i);
					}
					ilFlightCnt++;
				} // if flight is chosen
			 }// for all selected flights							
		} else {
			ilFlightCnt2	= lp.length;
			ilFlightCnt 	= 0;
			j 		= flights.size(0);
			
			for (i = 1; i < j; i++) {
           			for (n = 0; n < ilFlightCnt2; n++) {
					// Compare selected urno with databuffer urno
					if (lp[n].indexOf(flights.getString(0, i)) > -1) {
						// compare name						
						if ((lp[n].indexOf(flights.getString(4, i)) > -1) && (flights.getString(4, i).length()>0)) {
							// First flight
							if (i == 1) {
								flsc = "M";
							} else {
								// Main or Code Share ??
								// Compare urnos of actual and previous flight
								if (flights.getString(0, i).equals(flights.getString(0, i-1))) {
									flsc += ilFlightCnt;
								} else {
									flsc = "M";
								}
							}
						} // flight name is equal
						ilFlightCnt++;
					} // if urno is equal
				} // for all selected flights
			}// for all available flights
			if (flsc.equals("")) {
				flsc = " ";
			}
		} 	// Get list from user entries if closed and calculate ctyp
			// if opened get flights from local databuffer  and calculate ctyp
			
					
	        l = lp.length;

        	
		
		//Check first if counter is already opened, if yes get the FLSC and FLSG from the AODB
		// Create List of Flights to show, like LH 4711| KLM 1111| LOT 2222		
		if (l==0) {
			request.setAttribute("msg", "No allocation planned for Counter or Gate" + RNAM.trim() + " -><BR>" + 
							"Please contact the ASOC Coordinator for more information<BR>CHECK-IN Tel.: 40003 and GATES Tel.: 40002");
		       	getServletConfig().getServletContext().getContext("/cuteif/jsp/" +
        		    				"error/error.jsp").getRequestDispatcher("/jsp/error/error.jsp").forward(request, response);
			return;

		}

				


		////////////////////////////////////////////////////////////
		//
		// GET LOGO AND REMARK VALUES
		// PRODUCE ERROR MESSAGE IF NO LOGO IS SELECTED
		//
		///////////////////////////////////////////////////////////
		ListofStringlists logo = sh.executeQuery("select dtlogo from user where dtIP='" + userip + "'",0);
		String logovalue 	= new String(logo.getString(0,0).trim());
		
		// If no logo is selected, get the value from the select list		
		if ((logovalue.indexOf("None")!=-1) || (logovalue.length()<2)) {
    			/* forward to index.jsp */
			request.setAttribute("msg", "javascript:alert('NO LOGO CHOSEN\\u000D\\u000D" + 
							"Please select a logo by executing the following steps:\\u000D" + 
							"\\u00091. Choose a logo from the Preview list on the left side.\\u000D" + 
							"\\u00092. Press the PREVIEW button on the left side.\\u000D\\u000D" +
							" THANK YOU.')");
   			getServletConfig().getServletContext().getRequestDispatcher("/jsp/" +
            			"index/index.jsp").forward(request, response);
			return;
		}
		
		if (statusmeta.getString(2).equalsIgnoreCase("CCA")) {
 			remarkremi 	= new String(remark.getString(1,0).trim());
 			remarkcode 	= new String(remark.getString(0,0).trim());
		} else {
 			remarkremi 	= new String(remark.getString(0,0).trim());
 			remarkcode 	= new String(remark.getString(1,0).trim());
		}
		
	
        	selflight = new Stringlist();               // Flights
				
		//Extract the first flight
		akt_urno = lp[0].substring(0, lp[0].indexOf('_'));
				
		////////////////////////////////////////////////////////////
		//
		// NOW PROCESS ALL SELECTED FLIGHTS
		//
		///////////////////////////////////////////////////////////				
        	// for all flights
       	 	for (i = 0; i < l; i++) {
			if ((!akt_urno.equals(lp[i].substring(0, lp[0].indexOf('_')))) || (i == 0)) {
													
					// Extract the actual urno
        				akt_urno = lp[i].substring(0, lp[0].indexOf('_'));


	 				////////////////////////////////////////////////////////////
					//
					// SELECT VALUES FROM CCATAB
					//
					///////////////////////////////////////////////////////////		
					if (statusmeta.getString(2).equalsIgnoreCase("CCA")) {
						ListofStringlists ccataburno = sh.executeQuery("SELECT URNO,CKBS,CKES FROM CCATAB WHERE CKIC='" + RNAM +"' and (FLNU='" + akt_urno + "' or URNO='" + akt_urno + "') and ((ckba=' ') or ((ckba< TO_CHAR(SYSDATE+30/1440,'YYYYMMDDHH24MISS') and ckba<>' ') and (ckea=' ' or ckea> TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS'))))", 1);
						ccaurno = "";
							
		 				if (ccataburno.size(0) == 0) {
    							getServletConfig().getServletContext().getRequestDispatcher("/jsp/" +
           		 					"index/index.jsp").forward(request, response);	
							return;										
						} else {
							ccaurno = ccataburno.getString(0,0).trim();
							CKBS 	= ccataburno.getString(1,0).trim();
							CKES	= ccataburno.getString(2,0).trim();						
						}
						ccataburno = null;
					}

	 				////////////////////////////////////////////////////////////
					//
					// SELECT DATES FROM AFTTAB FOR GATES
					//
					///////////////////////////////////////////////////////////		
					if (!statusmeta.getString(2).equalsIgnoreCase("CCA")) {
						ListofStringlists afttaburno = sh.executeQuery("SELECT GD1B,GD1E,GD2B,GD2E FROM AFTTAB WHERE  URNO= " + akt_urno, 1);
							
		 				if (afttaburno.size(0) == 0) {
    							getServletConfig().getServletContext().getRequestDispatcher("/jsp/" +
           		 					"index/index.jsp").forward(request, response);	
							return;										
						} else {
        						if ("AG1".equalsIgnoreCase(flights.getString(6, n))) {
								GD1B	= afttaburno.getString(0,0).trim();						
 								GD1E	= afttaburno.getString(1,0).trim();						
       							} else {
								GD1B	= afttaburno.getString(2,0).trim();						
 								GD1E	= afttaburno.getString(3,0).trim();						
							}
						}
						afttaburno = null;
					}



	 				////////////////////////////////////////////////////////////
					//
					// SELECT URNO FROM FLDTAB
					//
					///////////////////////////////////////////////////////////
					ListofStringlists fldtaburno;		
					if (statusmeta.getString(2).equalsIgnoreCase("CCA")) {
						fldtaburno = sh.executeQuery("SELECT URNO,AOTI,ABTI,AFTI FROM FLDTAB WHERE RNAM='" + RNAM +"' AND RTYP = 'CKIF' AND DSEQ > 0 AND RURN='" + ccaurno + "'", 1);
					} else {
						fldtaburno = sh.executeQuery("SELECT URNO,AOTI,ABTI,AFTI FROM FLDTAB WHERE RNAM='" + RNAM +"' AND RTYP = 'GTD1' AND DSEQ > 0 AND AURN='" + akt_urno + "'", 1);
					}
					updateurno = "";
		 			if (fldtaburno.size(0) != 0) {
							updateurno 	= fldtaburno.getString(0,0).trim();
							AOTI		= fldtaburno.getString(1,0).trim();
							ABTI		= fldtaburno.getString(2,0).trim();
							AFTI 		= fldtaburno.getString(3,0).trim();
 					} else {
						blflddata 	= true;
					}
					
					lg.Log("WORK Insert or Update for <" + userip + "> blflddata <" + blflddata + "> with size <" + fldtaburno.size(0) + "> and flag <" + statusmeta.getString(2) + ">");																
					fldtaburno = null;
					
					////////////////////////////////////////////////////////////
					//
					//		Fill FLDTAB
					//
					///////////////////////////////////////////////////////////
					// Fill the Stringlist to sent
					// If a record exists in FLDTAB the send just an update command
					// otherwise send an insert command
					selflight 		= new Stringlist();
        				selflight.addString(flightmeta.getString(1)); //SECTION
					if (blflddata 	== false)  {														
		        			selflight.addString("UFL"); //COMMAND
					} else {
        					selflight.addString("IFL"); //COMMAND
						// For this service a urno is required.
						// To avoid a data integrity error when inserting values in FLZTAB and FXTTAB
						// an urno will be directly received from the AODB.
						// If the requests fails three times then the user will receive the mask without 
						// having made any changes to the AODB
						for (n=1; n<=3;n++) {
							inserturno = sh.getUrno();
							// Quit if urno is valid (blank is non valid urno)
							if (inserturno.length() > 1) {
								break;
							}
						}
						if (inserturno.length() <= 1) {
    							getServletConfig().getServletContext().getRequestDispatcher("/jsp/" +
           		 					"index/index.jsp").forward(request, response);	
							return;						
						}
						updateurno = inserturno; 
					}
					
 						
					// Set buffers to default AODB value (blank) if no data available
 					if (remarkcode.equals("")) {
						remarkcode = " ";
					}
 					if (remarkremi.equals("")) {
						remarkremi = " ";
					}
 					if (tp1.equals("")) {
						tp1 = " ";
					}
 					if (tp2.equals("")) {
						tp2 = " ";
					}
 					if (AOTI.equals("")) {
						AOTI = " ";
					}
 					if (ABTI.equals("")) {
						ABTI = " ";
					}
					if (AFTI.equals("")) {
						AFTI = " ";
					}
						
					// CHECKIN VALUES IF COMMMAND = FFC
					if (flightmeta.getString(2).equalsIgnoreCase("FFC")) {	
					
						// Get the checkin info
						n 		= flights.size(0);			
						for (m = 1; m < n; m++) {
							if (akt_urno.indexOf(flights.getString(0, m)) > -1) {
								// get info if common or dedicated checkin					
								checkintype = flights.getString(6, m);
							}
						}// for all available flights
							
						if (blflddata == false) {
							// UPDATE VALUES
							if (checkintype.indexOf("C") > -1) {
							// COMMON CHECKIN
								// LSTU,ABTI,AFTI,AURN,CREC,CTYP,DSEQ,RTAB,RURN,STAT,USEU 								
        							selflight.addString(currenttime + ", , ,0," + remarkcode + "," + flsc + ",0,CCA," + ccaurno + ", ,CUTE-IF," + updateurno); //DATA FOR CHECKIN									
							} else {
							// DEDICATED CHECKIN
        							selflight.addString(currenttime + ", , ," + akt_urno + "," + remarkcode + "," + flsc + ",0,CCA," + ccaurno + ", ,CUTE-IF," + updateurno); //DATA FOR CHECKIN									
							}
						} else {
							// INSERT VALUES
							// CDAT,AOTI,ABTI,AFTI,AURN,CREC,CTYP,DOTI,DCTI,DSEQ,HOPO,RNAM,RTAB,RTYP,RURN,SCTI,SOTI,STAT,USEC,URNO 
							if (checkintype.indexOf("C") > -1) {
								//Common Checkin
        							selflight.addString(currenttime + ", , , ,0," + remarkcode + "," + flsc + "," + CKBS + "," + CKES + ",0,ATH,"  + RNAM + ",CCA,CKIF," + ccaurno + "," + CKBS + "," + CKES + ", ,CUTE-IF," + inserturno); //DATA FOR CHECKIN
							} else {
								//Dedicated Checkin
        							selflight.addString(currenttime + ", , , ," + akt_urno + "," + remarkcode + "," + flsc + "," + CKBS + "," + CKES + ",0,ATH,"  + RNAM + ",CCA,CKIF," + ccaurno + "," + CKBS + "," + CKES + ", ,CUTE-IF," + inserturno); //DATA FOR CHECKIN
							}
						}
					} else {
						if ((remarkcode.equals("BOA")) && (ABTI.length() < 2)) {
							ABTI = currenttime;
						} else if ((remarkcode.equals("FCL")) && (AFTI.length() < 2)) {
							AFTI = currenttime;
						} 
						
						if (blflddata == false) {
							// UPDATE VALUES
							// LSTU,ABTI,AFTI,AURN,CREC,CTYP,DSEQ,RTAB,RURN,STAT,USEU 
        						selflight.addString(currenttime + "," + ABTI + "," + AFTI + "," + akt_urno + "," + remarkcode + "," + flsc + ",0, ,0," + remarkremi + ",CUTE-IF," + updateurno); //DATA FOR CHECKIN									
						} else {
							// INSERT VALUES
							// CDAT,AOTI,ABTI,AFTI,AURN,CREC,CTYP,DOTI,DCTI,DSEQ,HOPO,RNAM,RTAB,RTYP,RURN,SCTI,SOTI,STAT,USEC,URNO 
        						selflight.addString(currenttime + "," + currenttime + "," + ABTI + "," + AFTI + "," + akt_urno + "," + remarkcode + "," + flsc + "," + GD1B + "," + GD1E + ",0,ATH,"  + userdata.getString(8) + ", ,GTD1,0," + GD1B + "," + GD1E + "," + remarkremi +  ",CUTE-IF," + inserturno); //DATA FOR CHECKIN
						}
					}//CHECKIN OR GATE??
						
       		 			ch.put(selflight);
 					lg.Log("WORK PUT FLDTAB: " + selflight.getString(1) + " |" + selflight.getString(2));
        				selflight = null;
					blflddata = false;
		
	 				////////////////////////////////////////////////////////////
					//
					// SEND GATE REMARKS FOR UPDATE IN AFTTAB
					//
					///////////////////////////////////////////////////////////
					if (status.getString(3, 1).equals("1")) {
    						if (flights.getString(6, n).indexOf("AG")!=-1) {
							selflight = new Stringlist(); 
              						selflight.addString("SETDATA");
              						selflight.addString("UAR");

              						selflight.addString(remarkcode + "," + currenttime + ",CUTE-IF," + akt_urno);
         						ch.put(selflight);
          						selflight = null;
						}
					} //Gate opened??
						
 					////////////////////////////////////////////////////////////
					//
					// SEND CHECKIN REMARKS FOR INSERT IN CCATAB
					//
					///////////////////////////////////////////////////////////
					// CKEA,LSTU,USEU
    					if (remarkmeta.getString(2).equalsIgnoreCase("CCD")) {
						selflight = new Stringlist(); 
						selflight.addString(remarkmeta.getString(1));
						selflight.addString("UCR");
										
						selflight.addString(remarkcode + "," + currenttime + ",CUTE-IF," + ccaurno);

						ch.put(selflight);
          					selflight = null;
					} // CHECKIN REMARK IN CCATAB
		
					////////////////////////////////////////////////////////////
					//
					// SEND LOGOS INTO FLZTAB
					//
					///////////////////////////////////////////////////////////
					// SELECT URNO FROM FLZTAB where fldu = fldtab.urno
					fldtaburno = sh.executeQuery("SELECT URNO FROM FLZTAB WHERE FLDU='" + updateurno +"' AND SORT = 1", 1);
					bufferurno = "";
					selflight = new Stringlist(); 
					selflight.addString("SETDATA");
		 			if (fldtaburno.size(0) != 0) {
						bufferurno = fldtaburno.getString(0,0).trim();
						selflight.addString("UFZ");
						
						// UPDATE VALUES: FLGU				
						selflight.addString(logovalue + "," + bufferurno);

						ch.put(selflight);
					} else {
						selflight.addString("IFZ");
						
						// INSERT VALUES: FLDU,FLGU,SORT				
						selflight.addString(updateurno + "," + logovalue + ",1");

						ch.put(selflight);
					}
          				selflight = null;
					fldtaburno = null;


					////////////////////////////////////////////////////////////
					//
					// SEND FREE TEXTES INTO FXTTAB
					//
					///////////////////////////////////////////////////////////
					// SELECT URNO FROM FXTTAB where fldu = fldtab.urno
					
					// FREE TEXT 1
					fldtaburno = sh.executeQuery("SELECT URNO FROM FXTTAB WHERE FLDU='" + updateurno +"' AND SORT = 1", 1);
					bufferurno = "";
					selflight = new Stringlist(); 
					selflight.addString("SETDATA");
		 			if (fldtaburno.size(0) != 0) {
						bufferurno = fldtaburno.getString(0,0).trim();
						selflight.addString("UFX");
						
						// UPDATE VALUES: TEXT				
						selflight.addString(tp1.trim() + "," + bufferurno);

						ch.put(selflight);
					} else {
						selflight.addString("IFX");
						
						// INSERT VALUES: FLDU,TEXT,SORT				
						selflight.addString(updateurno + "," + tp1.trim() + ",1");

						ch.put(selflight);
					}
          				selflight = null;
					fldtaburno = null;
		
					// FREE TEXT 2
					fldtaburno = sh.executeQuery("SELECT URNO FROM FXTTAB WHERE FLDU='" + updateurno +"' AND SORT = 2", 1);
					bufferurno = "";
					selflight = new Stringlist(); 
					selflight.addString("SETDATA");
		 			if (fldtaburno.size(0) != 0) {
						bufferurno = fldtaburno.getString(0,0).trim();
						selflight.addString("UFX");
						
						// UPDATE VALUES: TEXT				
						selflight.addString(tp2.trim() + "," + bufferurno);

						ch.put(selflight);
					} else {
						selflight.addString("IFX");
						
						// INSERT VALUES: FLDU,TEXT,SORT				
						selflight.addString(updateurno + "," + tp2.trim() + ",2");

						ch.put(selflight);
					}
          				selflight = null;
					fldtaburno = null;
		
							
		
					////////////////////////////////////////////////////////////
					//
					//		COUNTER CLOSED -> OPENING IT
					//
					///////////////////////////////////////////////////////////
     					if (status.getString(3, 1).equals("0")) {
     	     					if (lp != null) {

							// NOW PREPARE THE OPENING
							// CHECKIN OR GATE COUNTER
     					 		if (statusmeta.getString(2).equals("CCA")) {
      								//CHECKIN
	 		 	  				insertcountertime = currenttime + ", ," + currenttime;
      							} else {
      								//GATE COUNTER
          							insertcountertime = currenttime+ "," + currenttime;
								
								// Set gate flag
								workinprogress = null;
								workinprogress = new ListofStringlists();
								workinprogress = sh.executeQuery("select dt4 from objectdata where  fkobject_id=\"" + flightmeta.getString(0) + "\" and dtselected = 0 and pkUrno =\"" + akt_urno + "\"",0);
								if (workinprogress.size(0) !=0) {
									gate1_or_2 = workinprogress.getString(0,0);
								}								
        						}

	
 							////////////////////////////////////////////////////////////
							//
							//	FLIGHT LISTBOX FOR CCATAB or AFTTAB (GATES)
							//
							///////////////////////////////////////////////////////////
							// BOTH WITH LSTU AND USEU
        						selflight = new Stringlist();   
        						selflight.addString(statusmeta.getString(1));
						
							// UPDATE GATE 1 OR GATE2 OR CHECKIN
        						if ("AG1".equalsIgnoreCase(gate1_or_2)) {
         							selflight.addString("G1X");
       							} else if ("AG2".equalsIgnoreCase(gate1_or_2)) {
        		  		  			selflight.addString("G2X");
        						} else {
           					 		selflight.addString("UCO");
							}
							
	 						// CREATE TELEGRAM
      							if (statusmeta.getString(2).equalsIgnoreCase("CCA")) {
         							selflight.addString(insertcountertime + ",CUTE-IF," + ccaurno);
      							} else {
        							selflight.addString(insertcountertime + ",CUTE-IF," + akt_urno);
      							}
        						ch.put(selflight);
         						selflight = null;

							if (!statusmeta.getString(2).equalsIgnoreCase("CCA")) {	
                                                		if (flights.getString(6, n).indexOf("AG")!=-1) {
                                                        		selflight = new Stringlist();
                                                        		selflight.addString("SETDATA");
                                                        		selflight.addString("UAR");

                                                        		selflight.addString(remarkcode + "," + currenttime + ",CUTE-IF," + akt_urno)
;
                                                        		ch.put(selflight);
                                                        		selflight = null;
                                                		}
							}
																										
            					}//lp 0
      					} // counter closed -> opening it 																			

					////////////////////////////////////////////////////////////
					//
					// UPDATE DSEQ IN FLDTAB
					//
					///////////////////////////////////////////////////////////
					selflight = new Stringlist(); 
					selflight.addString("SETDATA");
					selflight.addString("UFD");
					if (flightmeta.getString(2).equalsIgnoreCase("FFC")) {								
						// UPDATE VALUES: DSEQ,RTYP,LSTU.USEU				
						selflight.addString((i+1) + ",CKIF," + currenttime + ",CUTE-IF," + updateurno);
					} else {
						// UPDATE VALUES: DSEQ,RTYP,LSTU.USEU				
						selflight.addString((i+1) + ",GTD1," + currenttime + ",CUTE-IF," + updateurno);
					}
					
					ch.put(selflight);
 					lg.Log("WORK PUT DSEQ FOR UPDATE: " + selflight.getString(2));
					selflight = null;
					
				} //ignore transmission for code share flights

        		} // for all given flights

 		} // TRANSMIT BUTTON PRESSED

    	/* forward to index.jsp */
		// Now wait till the update service has returned the inserted data
		while (resultworkinprogress.equals("1") && (ilretries<8)) {
				// Set work in progress flag
				System.out.println("LOOP <" + ilretries + ">");
				// Wait a half sec
				try {
						lg.Log("LOOP waiting for dtrequestfinished <" + ilretries + "> seconds");
						Thread.sleep(1000);
				} catch (Exception e) {
				}
				workinprogress = sh.executeQuery("select dtrequestfinished from object where  pkobjectid=\"" +  flightmeta.getString(0) +"\"",0);
				if (workinprogress.size(0) !=0) {
						resultworkinprogress = workinprogress.getString(0,0);
				}
				ilretries++;
				if (ilretries==8) {
					workinprogress = sh.executeQuery("update object set dtrequestfinished=0 where  pkobjectid=\"" + flightmeta.getString(0) + "\"",0);
					//NOW ACTUALIZE THE FREETEXT AND REMARK / LOGO
					if (statusmeta.getString(2).equalsIgnoreCase("CCA")) {						
						sh.fillTempDB("FreeRemarkC1" + RNAM,RNAM);
						sh.fillTempDB("FreeRemarkC2" + RNAM,RNAM);
						sh.fillTempDB("REMARKC",RNAM);
						sh.fillTempDB("LOGOC",RNAM);
						sh.fillTempDB("FLIGHTC" + RNAM,RNAM);
						sh.fillTempDB("STATUSC" + RNAM,RNAM);
					} else {
						sh.fillTempDB("FreeRemarkG1" + RNAM,RNAM);
						sh.fillTempDB("FreeRemarkG2" + RNAM,RNAM);
						sh.fillTempDB("REMARKG",RNAM);
						sh.fillTempDB("LOGOG",RNAM);
						sh.fillTempDB("FLIGHTG" + RNAM,RNAM);
						sh.fillTempDB("STATUSG" + RNAM,RNAM);
					}

				}
		} // while waiting for response
		
		// Now send the main page if flight data is assigned and the info page if not
         try {
      				ListofStringlists flightdata = sh.getTempObjectdata(sh.getUserobject(request.getRemoteAddr(), "Listbox").getString(0), 0);
					if (flightdata.size(0)>1) {
		          				getServletConfig().getServletContext().getContext("/cuteif/jsp/" +
       		    				"index/index.jsp").getRequestDispatcher("/jsp/index/index.jsp").forward(request, response);
					} else {
								request.setAttribute("msg", "No allocation planned for Counter or Gate" + RNAM + " -><BR>" + 
								"Please contact the ASOC Coordinator for more information<BR>CHECK-IN Tel.: 40003 and GATES Tel.: 40002");
		          				getServletConfig().getServletContext().getContext("/cuteif/jsp/" +
        		    				"error/error.jsp").getRequestDispatcher("/jsp/error/error.jsp").forward(request, response);
					}
         } catch (Exception E) {
            			E.printStackTrace ();
            			return;
         } // send the page

  } // END OF WORK




/**	Method Logo
		@param  request:	HttpServletRequest object, which encapsulates the communication from the client to the server
		@param	response	HttpServletResponse object, which encapsulates the communication from the server to the client
			The logo method will store the currently shown preview logo in the user table.

*/
  private void Logo(HttpServletRequest request, HttpServletResponse response)
                                        throws IOException, ServletException
  {
    int i, j;
    Stringlist sendlogo;

    String userip 		= request.getRemoteAddr();
    String sp 			= request.getParameter("Select");
    String button 		= request.getParameter("Button");
    Stringlist userdata = null;
    String currenttime;

	// Store actual chosen logo in user table
    sh.executeQuery("Update user set dtlogo='" + sp + "' where dtIP='" + userip + "'",0);
 
    /* forward to logo.jsp */
   getServletConfig().getServletContext().getRequestDispatcher("/jsp/" +
            "logo/logo.jsp").forward(request, response);
  }




/**	Method doPost
		@param  request:	HttpServletRequest object, which encapsulates the communication from the client to the server
		@param	response	HttpServletResponse object, which encapsulates the communication from the server to the client
			The logo method will store the currently shown preview logo in the user table.

*/
  public void doPost(HttpServletRequest request, HttpServletResponse response)
                                        throws IOException, ServletException
  {

    String params 	= "";
    String req;
    int usr 		= 0;
    long distance	= 0;

    try {
      	lg.Log("Serving request for <" + request.getRemoteAddr() + ">");
    }
    catch (IOException E) {
      	System.err.println("File not found");
      	error = 2;
    }

    req = request.getParameter("Request");

    	//********************************//
	//Disable multiple button clicking
    	//********************************//
	HttpSession session 	= request.getSession(true);
	Date actualdate 	= new Date ();

 	if ((String)session.getValue("lastAccess")!=null) {
	 	distance	= actualdate.getTime() - Long.parseLong((String)session.getValue("lastAccess"));
	} else {
		distance	= 0;
	}
	
    	if ((req != null) && (request.getParameter("Button")!=null)) {			
		if (request.getParameter("Button").equalsIgnoreCase("Transmit")) {
			session.putValue("lastAccess",String.valueOf(actualdate.getTime()));
			if ((distance < 7000) && (distance != 0)) {
		      		try {
					Thread.sleep(7000);
        				getServletConfig().getServletContext().getContext("/cuteif/jsp/" +
            				"index/index.jsp").getRequestDispatcher("/jsp/index/index.js" +
            				"p").forward(request, response);
					return;
	      			}
	        		catch (Exception E) {
        				E.printStackTrace ();
      				}
			}
		}
		if ((request.getParameter("Button").equalsIgnoreCase("Close"))) {
			session.putValue("lastAccess",String.valueOf(actualdate.getTime()));
			if ((distance < 7000) && (distance != 0)) {
		      		try {
					Thread.sleep(7000);
	      			}
	        		catch (Exception E) {
        				E.printStackTrace ();
      				}
			}
		}
	}  
    	//********************************//
	// EOF multiple button clicking
    	//********************************//
	
 	////////////////////////////////////////////////////////////
	//
	//		HAS AN ERROR OCCURED ?
	//
	///////////////////////////////////////////////////////////
	// Get the 
    if (error == 0) {
      	try {
       	 	usr = sh.Testuser(request.getRemoteAddr());
      	}
      	catch (Exception E) {
        	error = 1;
      	}
    	if (usr == -2)
      		error = 3;
    }

	// react in case of an error
    if (error != 0) {
      	// Set the attribute and Forward back to error.jsp
      	System.err.println("Error found");
      	switch (error) {
        	case 1: request.setAttribute("msg", "Internal error: Database is not available. Please call IT&T Help Desk: 348 (and select 'UFIS FIDS' after hearing the voice machine.)"); break;
        	case 2: request.setAttribute("msg", "Internal error: Logging is not available. Please call IT&T Help Desk: 348 (and select 'UFIS FIDS' after hearing the voice machine.)"); break;
        	case 3: request.setAttribute("msg", "Internal error: Error during database access. Please call IT&T Help Desk: 348 (and select 'UFIS FIDS' after hearing the voice machine.)"); break;
      	}

      	try {
        	getServletConfig().getServletContext().getContext("/cuteif/jsp/" +
            	"error/error.jsp").getRequestDispatcher("/jsp/error/error.js" +
            	"p").forward(request, response);
      	}
        catch (Exception E) {
        	E.printStackTrace ();
      	}
    }
    else {
 	////////////////////////////////////////////////////////////
	//
	//		USER NAME AND PASSWORD PROVIDED FOR LOGIN?
	//
	///////////////////////////////////////////////////////////
      	boolean fwd = false;
      	if ((usr != 1) && (!(req.equalsIgnoreCase("Login") && (usr == 0))))
        	fwd = true;
      	else
        	fwd = false;
			
      	if (fwd) {
        	if (usr == -1) {
          		request.setAttribute("msg", "You are not allowed to log in from this terminal.");
        	}
        	try {
          		getServletConfig().getServletContext().getContext("/cuteif/jsp/" +
            		"login/login.jsp").getRequestDispatcher("/jsp/login/login.js" +
            		"p").forward(request, response);
        	}
        	catch (Exception E) {
          		E.printStackTrace ();
        	}
      	}
    	else {
 	////////////////////////////////////////////////////////////
	//
	//		PROCESS WORK, LOGIN, OR LOGO FUNCTIONALITIES
	//
	///////////////////////////////////////////////////////////
      		if (req != null) {
        		if (req.equalsIgnoreCase("Login"))
          				Login(request, response);
        		else if (req.equalsIgnoreCase("Work"))
          			try {
             			Work(request, response);
          			}
          			catch (Exception E) {
		  					request.setAttribute("msg", "The System is currently not available<BR>" + 
							"Please call IT&T Help Desk for more information<BR> Tel.: 348 (and select 'UFIS FIDS' after hearing the voice machine.)");
		          			getServletConfig().getServletContext().getContext("/cuteif/jsp/" +
        		    		"error/error.jsp").getRequestDispatcher("/jsp/error/error.jsp").forward(request, response);

            				lg.Log("MAINSERVLET: Method Work failed with <" + E.getMessage() + ">");
          			}
          		else if (req.equalsIgnoreCase("Logo"))
            			Logo(request, response);
      		}
      	}
    }
  } /* doPost */


/**	Method doGet
		@param  request:	HttpServletRequest object, which encapsulates the communication from the client to the server
		@param	response	HttpServletResponse object, which encapsulates the communication from the server to the client
			Redirects to doPost() method.

*/
  public void doGet(HttpServletRequest request, HttpServletResponse response)
                                        throws IOException, ServletException
  {
    doPost(request, response);
  }
  
} // EO CLASS
 /***************************************************************************************/
 /*																						*/
 /* 				EO CLASS MainServlet3 												*/
 /*																						*/
 /***************************************************************************************/



