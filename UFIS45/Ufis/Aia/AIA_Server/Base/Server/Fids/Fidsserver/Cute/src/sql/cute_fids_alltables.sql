# MySQL dump 8.2
#
# Host: localhost    Database: cute_fids
#--------------------------------------------------------
# Server version	3.22.34-shareware-debug

#
# Table structure for table 'object'
#

CREATE TABLE object (
  pkobjectid varchar(59) DEFAULT '' NOT NULL,
  dtname varchar(100),
  dtrequestfinished int(11),
  dtupdatecommand varchar(20),
  fkobjectgroup varchar(50),
  dtsqlstatement text,
  dtsqlstatement2 text,
  dtSection varchar(15),
  dtCommand char(3),
  PRIMARY KEY (pkobjectid)
);


#
# Table structure for table 'objectdata'
#

CREATE TABLE objectdata (
  fkobject_ID varchar(59) DEFAULT '' NOT NULL,
  pkUrno varchar(30) DEFAULT '' NOT NULL,
  dtOrder int(11) DEFAULT '0' NOT NULL,
  dtSelected int(11) DEFAULT '0' NOT NULL,
  dt2 varchar(19),
  dt3 varchar(19),
  dt4 varchar(19),
  dt5 varchar(19),
  dt1 varchar(255),
  PRIMARY KEY (fkobject_ID,pkUrno,dtOrder)
);


#
# Table structure for table 'objectgroup'
#

CREATE TABLE objectgroup (
  pkobjectgroup varchar(50) DEFAULT '' NOT NULL,
  dt1 varchar(19),
  dt2 varchar(19),
  dt3 varchar(19),
  dt4 varchar(19),
  dt5 varchar(19),
  dt6 varchar(19),
  dt7 varchar(19),
  dt8 varchar(19),
  PRIMARY KEY (pkobjectgroup)
);


#
# Table structure for table 'user'
#

CREATE TABLE user (
  pkuserid varchar(100) DEFAULT '' NOT NULL,
  dtIP varchar(15) DEFAULT '' NOT NULL,
  dtName varchar(200) DEFAULT '' NOT NULL,
  dtPwd varchar(20),
  dtConnect int(11) DEFAULT '0' NOT NULL,
  dttyp varchar(20),
  dtlogo varchar(10),
  dtremark varchar(10),
  dtcounter varchar(20),
  PRIMARY KEY (pkuserid)
);

#
# Table structure for table 'userobjects'
#

CREATE TABLE userobjects (
  pkuserid varchar(100) DEFAULT '' NOT NULL,
  pkobjectid varchar(100) DEFAULT '' NOT NULL,
  PRIMARY KEY (pkuserid,pkobjectid)
);


#
# Dumping data for table 'objectgroup'
#

INSERT INTO objectgroup VALUES ('Listbox','pkUrno','dtOrder','dtSelected','dt1','dt2','dt3','dt4',NULL);
INSERT INTO objectgroup VALUES ('Status','pkUrno','dtOrder','dtSelected','dt1','dt2',NULL,NULL,NULL);
INSERT INTO objectgroup VALUES ('Select','pkUrno','dtOrder','dtSelected','dt1','dt2','dt3',NULL,NULL);
INSERT INTO objectgroup VALUES ('Text1','pkUrno','dtOrder','dtSelected','dt1',NULL,NULL,NULL,NULL);
INSERT INTO objectgroup VALUES ('Text2','pkUrno','dtOrder','dtSelected','dt1',NULL,NULL,NULL,NULL);
INSERT INTO objectgroup VALUES ('Logo','pkUrno','dtOrder','dtSelected','dt1','dt2','dt3',NULL,NULL);



INSERT INTO object VALUES ('REMARKC','REMARKC',NULL,'REMARKC','Select','Select distinct beme,code,remi,urno from fidtab where remt=\'C\' and beme<>\' \' and hopo=\'ATH\' order by beme',NULL,'SETDATA','CCD');
INSERT INTO object VALUES ('LOGOC','LOGOC CHECKIN',NULL,'LOGOC','Logo','Select lgfn,URNO,urno,lgsn from flgtab order by lgfn',NULL,'SETDATA','LGC');
INSERT INTO object VALUES ('REMARKG','REMARKG',NULL,'REMARKG','Select','Select distinct beme,remi,code,urno from fidtab where remt=\'G\' and beme<>\' \' and hopo=\'ATH\' order by beme',NULL,'SETDATA','AG3');
INSERT INTO object VALUES ('LOGOG','LOGOG GATE',NULL,'LOGOG','Logo','Select lgfn,URNO,urno,lgsn from flgtab order by lgfn',NULL,'SETDATA','LGG');


###########################################################################################################
