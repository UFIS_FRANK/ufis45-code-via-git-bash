##########################################################################################################
##		SQL STATEMENTS FOR GATE xxx OBJECTS CUTE INTERFACE					##
##########################################################################################################

####### TABLE OBJECT #######
INSERT INTO object VALUES ('FLIGHTGxxx','FLIGHTGxxx',NULL,'FLIGHTGxxx','Listbox','SELECT jfno,flno,alc3,urno,\'AG1\' as cmd,tifd FROM afttab WHERE ((gtd1 = \'xxx\')  AND ((DECODE(GD1Y,\'              \', GD1E, GD1Y) > TO_CHAR(SYSDATE,\'YYYYMMDDHH24MISS\')) OR (GD1Y LIKE \'  %\'))  AND   ((DECODE(GD1X,\'              \', GD1B, GD1X) BETWEEN  TO_CHAR(SYSDATE-600/1440,\'YYYYMMDDHH24MISS\') AND TO_CHAR(SYSDATE+30/1440,\'YYYYMMDDHH24MISS\')) AND  (GD1B <> \' \')) AND FTYP=\'O\' AND (TIFD BETWEEN TO_CHAR(SYSDATE-30/1440,\'YYYYMMDDHH24MISS\') AND TO_CHAR(SYSDATE+720/1440,\'YYYYMMDDHH24MISS\'))) UNION SELECT jfno,flno,alc3,urno,\'AG2\' as cmd,tifd FROM afttab WHERE ((gtd2 = \'xxx\')  AND ((DECODE(GD2Y,\'              \', GD2E, GD2Y) > TO_CHAR(SYSDATE,\'YYYYMMDDHH24MISS\')) OR (GD2Y LIKE \'  %\'))  AND   ((DECODE(GD2X,\'              \', GD2B, GD2X) BETWEEN TO_CHAR(SYSDATE-600/1440,\'YYYYMMDDHH24MISS\') AND TO_CHAR(SYSDATE+30/1440,\'YYYYMMDDHH24MISS\')) OR  (GD2B LIKE \' %\')) AND FTYP=\'O\' AND  (TIFD BETWEEN TO_CHAR(SYSDATE-30/1440,\'YYYYMMDDHH24MISS\') AND TO_CHAR(SYSDATE+720/1440,\'YYYYMMDDHH24MISS\')))  ORDER by TIFD',NULL,'SETDATA','FFG');
INSERT INTO object VALUES ('STATUSGxxx','STATUSGxxx',NULL,'STATUSGxxx','Status','SELECT AURN as URNO FROM FLDTAB WHERE (RNAM = \'xxx\') AND (RTYP = \'GTD1\') AND (DSEQ > 0)',NULL,'SETDATA','AG');

INSERT INTO object VALUES ('FreeRemarkG1xxx','FreeRemarkG1xxx',NULL,'FreeRemarkG1xxx','Text1','SELECT TEXT,FLDU AS URNO FROM FXTTAB WHERE  SORT=\'1\' AND FLDU IN (SELECT URNO FROM FLDTAB WHERE RTYP = \'GTD1\' AND RNAM = \'xxx\' AND DSEQ > 0)',NULL,'SETDATA','FG1');
INSERT INTO object VALUES ('FreeRemarkG2xxx','FreeRemarkG2xxx',NULL,'FreeRemarkG2xxx','Text2','SELECT TEXT,FLDU AS URNO FROM FXTTAB WHERE  SORT=\'2\' AND FLDU IN (SELECT URNO FROM FLDTAB WHERE RTYP = \'GTD1\' AND RNAM = \'xxx\' AND DSEQ > 0)',NULL,'SETDATA','FG2');


####### TABLE USER #######
INSERT INTO user VALUES ('Gxxx',' ','Gatexxx','test',0,'G',NULL,NULL,'xxx');

####### TABLE USEROBJECTS #######
INSERT INTO userobjects VALUES ('Gxxx','FLIGHTGxxx');
INSERT INTO userobjects VALUES ('Gxxx','STATUSGxxx');
INSERT INTO userobjects VALUES ('Gxxx','REMARKG');
INSERT INTO userobjects VALUES ('Gxxx','FreeRemarkG1xxx');
INSERT INTO userobjects VALUES ('Gxxx','FreeRemarkG2xxx');
INSERT INTO userobjects VALUES ('Gxxx','LOGOG');

##########################################################################################################
##		END SECTION GATE xxx 								##
##########################################################################################################
