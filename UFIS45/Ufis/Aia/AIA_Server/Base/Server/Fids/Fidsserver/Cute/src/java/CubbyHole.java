
/**
 * Title:       CubbyHole.class
 * Description: Shared Object for exchange of data between Servlet Threads and the CDIInterface (Update, Insert) Thread<p>
 * Copyright:   Copyright (c) 2000<p>
 * Company:     ABB AAT
 * @author		BSC
 * @version 	1.0
 */

// data container
import Stringlist;


/** CubbyHole CLASS<BR><BR>
	This class provides methods to control the use of the CDI service for
	deletes, inserts, and updates.<BR>
	In the CDIInterface class the get mehtod is called. If a request
	is send from a client, the put method sets a flag to true and the 
	get method returns the data container.
	All participating processes will be notified about any changes of the flag.	
*/

public class CubbyHole {

	// data to be processed
  	private Stringlist contents;
	// flag for state of data procession
  	private boolean available 	= false;


	/** Method get<BR>
			The calling method will remain in this method until a client 
			has called the put method. Then get will return with the data list,
			which has been sent by the client.
	*/		
  	public synchronized Stringlist get() {
System.out.println("CDIInterface get()");
    	while (available == false) {
      		try {
        			// wait for Producer to put value
        			wait();
        			System.out.println("CDIInterface wait succeded");
      			} catch (InterruptedException e) { }
    	}
    	return contents;
  	}

	/** Method declareready<BR>
			The method resets the flag and informs all related processes.
	*/		
	public synchronized void declareready() {
		// reset flag
    	available 	= false;
    	// notify Producer that value has been retrieved
    	notifyAll();
  	}


	/** Method put<BR>
		@param data: 	Data container, which contains the section, command and data to be sent to CEDA<BR>
			The calling method sets the flag to true and returns with the data list. 
			Afterwards all related processes will be informed.
	*/		
  	public synchronized void put(Stringlist data) {
    	while (available == true) {
      		try {
       				// wait for Consumer to get value
        			wait();
      		} catch (InterruptedException e) { }
    	}
    	contents 	= data;
    	available 	= true;
    	// notify Consumer that value has been set
    	notifyAll();
  }
}
 /***************************************************************************************/
 /*																						*/
 /* 				EO CLASS CubbyHole  												*/
 /*																						*/
 /***************************************************************************************/
