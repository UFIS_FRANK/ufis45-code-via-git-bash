
/**
 * Title:       MainServlet_Superclass.class
 * Description: Super class for the MainServlet3 class.
 * Copyright:   Copyright (c) <p>
 * Company:     ABB AAT
 * @author		MOS
 * @version 	1.0
 */


import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import Stringlist;
import Log;
import java.sql.SQLException;


/** MainServlet_Superclass CLASS<BR><BR>
	This class is the super class of the MainServlet3 class. Its purpose is to intiialize
	the SQL_Handler, the CDIInterface/Cubbyhole and the log class only once. 
	This allows the MainServlet3 to be run in mutlithreaded mode.
*/
public class MainServlet_Superclass extends HttpServlet {

  	protected static Log lg;
  	protected static SQL_Handler sh;
  	protected static CubbyHole ch;
  	protected static CDIInterface writedb 		= null;
  	protected static CDIInterface updateservice	= null;


/** Servlet Constructor
		Creates an instance of the SQL_Handler class, the CDIInterface class
		and the log class, if the  CDIInterface instance is not yet created.
		
		The init creates first the log class instance, then the SQL_Handler, and together
		with these objects the cubbyhole witgh the CDIInterface class instance.
		
		After completion both CDI services are being started, the one for updates
		and the one for setting data in the AODB.
		
		Finally the lcoal databuffer will be actualised.
		
		In case all the classes are already initialised, only a SQL Handler class instance
		will be created.
*/  

  public void init(ServletConfig config) throws ServletException {
    	// Store the ServletConfig object and log the initialization
    	super.init(config);

    	if (writedb == null) {
 
      		try {
        		FileOutputStream 	fos = new FileOutputStream("/home/ceda/tomcat/logs/stderr.log", true);
        		PrintStream 		ps 	= new PrintStream(fos);
        		System.setErr(ps);
      		}
      		catch (FileNotFoundException E) {}

      		// Open file

      		try {
        		lg = new Log("/home/ceda/tomcat/logs/CUTE_MAINSERVLET.log");
        		lg.Log("MAINSERVLET INIT: Logging started");

      		}
      		catch (IOException E) {
        		System.err.println("File not found");
        		return;
      		}

      		try {
        		try {
          			sh = new SQL_Handler(true, lg);
					lg.Log("MAINSERVLET INIT: SQL Handler initialised");
        		}
        		catch (SQLException E) {
          			System.out.println("SQLException: " + E.getMessage());
          			System.out.println("SQLState:     " + E.getSQLState());
          			System.out.println("VendorError:  " + E.getErrorCode());
          			return;
        		}
      		}
      		catch (Exception E) {
        		sh = null;
        		System.err.println("MAINSERVLET INIT: Sql_Handler not available");
       		return;
      		}

      		// create cubbyhole to exchange data with the other threads
      		ch = new CubbyHole();

      		// create updatethread and start it
	    	writedb = new CDIInterface(ch, lg);
    	  	writedb.start();

      		// create updateservicethread and start it
      		updateservice = new CDIInterface("INFO1", "UI1", lg);
      		updateservice.start();

      		// create updateservicethread2 and start it
      		//updateservice2 = new CDIInterface("INFO1", "UI1", lg);
      		//updateservice2.start();

      		// load data from main database
      		//sh.fillTempDB("","");


      		// create usermonitorthread and start it
      		try {
        			lg.Log("MAINSERVLET INIT: first instance");
      		}
      		catch (Exception e)
      		{}
    	}
    	else
      		try {
        		try {
          			sh = new SQL_Handler(true, lg);
					lg.Log("MAINSERVLET INIT: SQL Handler initialised without CDI");
        		}
        		catch (SQLException E) {
          			System.out.println("SQLException: " + E.getMessage());
          			System.out.println("SQLState:     " + E.getSQLState());
          			System.out.println("VendorError:  " + E.getErrorCode());
           			return;
        		}
      		}		
      		catch (Exception E) {
        			sh = null;
        			System.err.println("MAINSERVLET INIT: Sql_Handler not available");
        			return;
      		}
  }// init




/** Servlet Destructor
		
*/
  public void destroy() {

    try {
      lg.close();
	  writedb 		= null;
	  updateservice	= null;
    }
    catch (IOException E) {
    }
    sh = null;
  }


} // MainServlet_Superclass
 /***************************************************************************************/
 /*																						*/
 /* 				EO CLASS MainServlet_Superclass	 									*/
 /*																						*/
 /***************************************************************************************/
