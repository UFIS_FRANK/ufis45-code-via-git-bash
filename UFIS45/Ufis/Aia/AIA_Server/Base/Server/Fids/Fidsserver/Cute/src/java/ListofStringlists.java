
/**
 * Title:       ListofStringlists.class
 * Description: Contains methods for managing the data container ListofStringlists
 * Copyright:   Copyright (c) <p>
 * Company:     ABB AAT
 * @author		BSC
 * @version 	1.0
 */
 
 
import java.util.List;
import java.util.LinkedList;


/** ListofStringlists CLASS<BR><BR>
	This class provides methods for the use of the ListofStringlists data container.
*/
public class ListofStringlists {

  private List Data;


/** CONSTRUCTOR 1
*/
  public ListofStringlists() {
    	Data = new LinkedList();
  }


/** CONSTRUCTOR 2
	@param sublists: No of initial sublists.
*/
  public ListofStringlists(int sublists) {
    	int i;

    	Data = new LinkedList();
    	for (i = 0; i < sublists; i++)
      		Data.add(new Stringlist());
  }


/** DESTRUCTOR 
*/
  protected void finalize() {
    	int i, j;

    	j = Data.size();
    	for (i = 0; i < j; i++)
      		Data.set(i, null);

    	Data = null;
  } /* finalize */


/*
  public ListofStringlists addList(ListofStringlists data,int addcols) {
    Data = new LinkedList();
	int i;
	int listsize;
	
	listsize = data.size() + addcols;
	
    for (i = 0; i < listsize; i++)
      Data.add(new Stringlist());

	listsize = data.size();
    for (i = 0; i < listsize; i++)
      Data.add(data.getSubList(i));
	


    return Data;
  }
*/
/** Method addList
	Adds a sublist to the given ListofStringlist.
*/
  public boolean addList() {
    	return Data.add(new Stringlist());
  }

/** Method removeList
	@param index: No of sublist to be removed.
		Removes a sublist from the given ListofStringlist.
*/
  public Stringlist removeList(int index) throws IndexOutOfBoundsException {
    	return ((Stringlist)Data.remove(index));
  }


/** Method removeString
	@param Listindex: 	No of sublist which contains the string.
	@param Stringindex: Position of String.
		Removes a String from the given ListofStringlist.
*/
  public String removeString(int Listindex, int Stringindex) {
   		return ((Stringlist)Data.get(Listindex)).removeString(Stringindex);
  }


/** Method addString
	@param value: 	String value to be inserted into data container.
	@param Listindex: 	No of sublist which contains the string.
		Adds a String to the given ListofStringlist.
*/
  public boolean addString(String value, int Listindex) throws IndexOutOfBoundsException {
    	return ((Stringlist)Data.get(Listindex)).addString(value);
  }


/** Method getSubList
	@param Listindex: No of sublist to be returned.
		Returns a sublist from the given ListofStringlist.
*/
  public Stringlist getSubList(int Listindex) throws IndexOutOfBoundsException {
    	return ((Stringlist)Data.get(Listindex));
  }


/** Method getString
	@param Listindex: 	No of sublist which contains the string.
	@param Stringindex: Position of String.
		Returns a String from the given ListofStringlist.
	EG:
		FLNO	STOD	ALC3	=	0,0		0,1		0,2	
		LH 11	12:00	DLH			1,0		1,1		1,2	
		KL 22	13:00	KLM			2,0		2,1		2,2
*/
  public String getString(int Listindex, int Stringindex) throws IndexOutOfBoundsException {
    	return ((Stringlist)Data.get(Listindex)).getString(Stringindex);
  }


/** Method size
		Returns the size of the given ListofStringlist.
*/
  public int size() {
    	return Data.size();
  }



/** Method size
	@param index: 	No of sublist.
		Returns the sublist size of the given ListofStringlist.
*/
  public int size(int index) {
    	return ((Stringlist)Data.get(index)).size();
  }
  
} // EO Class
 /***************************************************************************************/
 /*																						*/
 /* 				EO CLASS ListofStringlists	 										*/
 /*																						*/
 /***************************************************************************************/
