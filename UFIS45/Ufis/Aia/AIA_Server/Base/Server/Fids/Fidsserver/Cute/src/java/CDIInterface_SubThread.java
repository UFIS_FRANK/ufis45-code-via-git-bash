
/**
 * Title:       CDIInterface_SubThread.class
 * Description: Used by the CDIInterface class for processing the incoming data from CEDA
 * Copyright:   Copyright (c) 2000<p>
 * Company:     ABB AAT
 * @author		MOS
 * @version 	1.0
 */


import java.util.*;
import java.io.*;
import Stringlist;


/** CDIInterface_SubThread CLASS<BR><BR>
	This class provides a event processing functionality in its run() method.
	The events wiil be filtered and the data for the related object will be loaded 
	from the AODB.
*/
public class CDIInterface_SubThread extends Thread {

  	private String values;
  	private String separator;
  
	/** Instance of classes */
	public SQL_Handler sqlclass;
    private Log lg; // BSC

	/**
		Constructor with the telegram from the CDISERVER and the separator,
		which is used in this telegram.
		@param incomingvalues: 	telegram from cdiserver
		@param usedseparator:	separator in telegram
		@param logger:			Logfiles for debugging entries
	*/
	public CDIInterface_SubThread (String incomingvalues, String usedseparator, Log logger) {
		try {
			values 		= incomingvalues;
			separator	= usedseparator;
			lg 		= logger;
			sqlclass 	= new SQL_Handler(true,lg);
		} catch (Exception E) {
          		System.out.println("CDIInterface_SubThread Constructor: " + E.getMessage());
          		return;
        }

	}


     	/** Method, which is called from the CDIINTERFACE CLASS function
	   As parameters, the sent used separator and data will be sent, if configured.
	   The method will cause the sqlhdl.class to reload the object's data from the AODB by the use of  JDBC.
	   
	   The method also filters the incoming events, which are being sent via the action and the cdiserver.
	   Because of limitations of the action process filtering ability, the filterinhg will be done here for the
	   objects as specified in Athens.
	   The method extracts first all components and creates then the related objectids. AT last, the fillTempDB method 
	   from the SQL_Handler class is called with this objectid as parameter.
	   The important messages are:
	   	CKIF and CKIT
		CKIC
		RNAM
		GDT1 and GTD2
	   	
		
	 A retrieved data telegram can look like this:<BR><BR>
	 000000000000000000000#REFRESH#STATUSG#GDT2#A03#:<BR><BR>
	 
	 or:<BR><BR>
	 
	 000000000000000000000#REFRESH#FLIGHTG#URNO#22778899#GDT1#A01#GDT2#A07#<BR><BR>
	 
	 or just:<BR><BR>
	 
	 000000000000000000000#REFRESH#FLIGHTG#URNO# <BR>which will be ignored<BR>
	 
	*/

	public void run() {


		int ilCnt,ilNoOfTokens;

		StringTokenizer tokens;

		String token 		= new String();
		String CKIF 		= new String();
		String CKIT 		= new String();
		String CKIC 		= new String();
		String RNAM 		= new String();
		String RTYP 		= new String();
		String GTD1 		= new String();
		String GTD2 		= new String();
		String OBJECT 		= new String();
		String objectend 	= new String();
		String objectfullname 	= new String();
		String countervalue	= new String();
		ListofStringlists workinprogress = null;
		boolean eventfilter	= false;
		
		
		try {
		System.out.println("\n\nTHREAD pcmd STARTED <" +  values + ">  <" +  separator + ">");	
		 if ((values!=null) && (separator!=null) && ((values.indexOf("STATUS")!=-1) || (values.indexOf("BASIC")!=-1))) {
			
			tokens 	= new StringTokenizer(values,separator);
			//lg.Log("\n\nCDIInterface pcmd STARTED <" +  values + ">  <" +  separator + ">");

			// Process the data telegram
			ilNoOfTokens = tokens.countTokens();
			for (ilCnt=1; ilCnt<ilNoOfTokens; ilCnt++) {
				// Get next token
				token = tokens.nextToken(separator);
				
				// Get the name of the related object
				if (token.compareTo("REFRESH")==0) {
					OBJECT = tokens.nextToken(separator);
					token = tokens.nextToken(separator);
					ilCnt++;
				}

				
				if (token.compareTo("RNAM")==0) {
					RNAM = tokens.nextToken(separator);
					ilCnt++;
					eventfilter	= true;
				}
				
				if (token.compareTo("CKIC")==0) {
					RNAM = tokens.nextToken(separator);
					ilCnt++;
					eventfilter	= true;
				}
				if (token.compareTo("GTD1")==0) {
					RNAM = tokens.nextToken(separator);
					ilCnt++;
					eventfilter	= true;
				}
				
				if (token.compareTo("GTD2")==0) {
					RNAM = tokens.nextToken(separator);
					ilCnt++;
					eventfilter	= true;
				}

				// Logo basic Data treatment
				if (token.compareTo("BASIC")==0) {
					RNAM = "BASIC";
					ilCnt++;
					eventfilter	= true;
				}
				
			}
			System.out.println("\nTHREAD pcmd eventfilter <" + eventfilter + ">\n");	
			if ((OBJECT!=null) && (eventfilter == true) && (RNAM.indexOf("BASIC")==-1)) {
			System.out.println("THREAD pcmd OBJECT available <" + OBJECT + ">");	
		
					//Now retrieve the object name
					//Is it a checkin object ??
					if (OBJECT.indexOf("C")!=-1) {									
						//LOAD ALL THE OBJECT'S DATA						
						sqlclass.fillTempDB("LOGOC",RNAM);
						sqlclass.fillTempDB("REMARKC",RNAM);
						sqlclass.fillTempDB("FreeRemarkC1" + RNAM,RNAM);
						sqlclass.fillTempDB("FreeRemarkC2" + RNAM,RNAM);
						sqlclass.fillTempDB("STATUSC" + RNAM,RNAM);
						sqlclass.fillTempDB("FLIGHTC" + RNAM,RNAM);
						lg.Log("\n\n\nUPDATING <" + RNAM + ">\n\n\n");

						// NOW set the object's dtrequestfinished field back to 0
						workinprogress = sqlclass.executeQuery("update object set dtrequestfinished=0 where  pkobjectid=\'FLIGHTC" + RNAM + "\'",0);
					} else {
						//LOAD ALL THE OBJECT'S DATA
						sqlclass.fillTempDB("LOGOG",RNAM);
						sqlclass.fillTempDB("STATUSG" + RNAM,RNAM);
						sqlclass.fillTempDB("FLIGHTG" + RNAM,RNAM);
						sqlclass.fillTempDB("REMARKG",RNAM);
						sqlclass.fillTempDB("FreeRemarkG1" + RNAM,RNAM);
						sqlclass.fillTempDB("FreeRemarkG2" + RNAM,RNAM);
						
						// NOW set the object's dtrequestfinished field back to 0
						workinprogress = sqlclass.executeQuery("update object set dtrequestfinished=0 where  pkobjectid=\'FLIGHTG" + RNAM + "\'",0);
					} // END STATUS
			}// if objectid not ""
			else if ((OBJECT!=null) && (eventfilter == true) && (RNAM.indexOf("BASIC")!=-1)) {
						sqlclass.fillTempDB(OBJECT,"");
			}
		 }// if parameters not null
		 
		 // Free the sqlhdl class instance
		 sqlclass = null;
					
		}
		catch(Exception e) {
		
					// NOW set the object's dtrequestfinished field back to 0
					//workinprogress = sqlclass.executeQuery("update object set dtrequestfinished=0 where  pkobjectid=\'" + objectfullname + "\'",0);
					//System.out.println("EXCEPTION in pcmd " + e.getMessage());					
		}

		
	}
	
} // END OF CLASS
 /***************************************************************************************/
 /*																						*/
 /* 				EO CLASS CDIInterface_SubThread  												*/
 /*																						*/
 /***************************************************************************************/
