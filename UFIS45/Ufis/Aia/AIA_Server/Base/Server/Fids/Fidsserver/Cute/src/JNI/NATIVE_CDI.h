#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Aia/AIA_Server/Base/Server/Fids/Fidsserver/Cute/src/JNI/NATIVE_CDI.h 1.1 2004/10/04 16:57:10SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ************************************************** 	*/
/*	HEADER FILE FOR THE NATIVE CDI INTERFACE 			*/
/* ************************************************** 	*/

/* General includes */
#include 	"cdilib.h"
#include 	<limits.h>
#ifdef  	WIN32 
#include 	<process.h>
#endif

/*Java Native interface includes */
#include 	<jni.h>
#include 	"CDIInterface.h"



/* Local defines */
#define 	MAX_ERRORS		20						/* Max. number of consecutive errors */
#define 	CONFIG_FILE     "/home/ceda/tomcat/conf/NATIVE_CDI.cfg"
#define 	RC_FAIL		1;
#define 	RC_SUCCESS	0;
#define 	cCOMMA		',';


typedef struct {
	_TGRAM_HDR	*prgRcdTgram;					/* Rcd telegram header pointers */
	_TGRAM_HDR	*prgSndTgram;					/* Snd telegram header pointers */
	_CLIENT		rgCDIClient;					/* _CLIENT struct array*/


	int			igDescr; 						/* client descriptors  */
	int			ilRC;
	int			ilCond;
	int			igErr; 							/* No of consequetive errors */
	int			igDATFlag;
	int			igCnx;							/* Client connection status			*/
													/* FREE: CONNECTION NOT ESTABLISHED */
													/* USED: CONNECTION ESTABLISHED 		*/

	int			igRcdTgramSize;					/* Current size (in bytes) of prgRcdTgram */
	int			igSndTgramSize;					/* Current size (in bytes) of prgSndTgram */
	int			igSocket;

	int			igCtlCounter; 					/* CTL  Telegram counter      */
	int			igDatCounter; 					/* DAT  Telegram counter      */
	int			igErrCounter; 					/* Error counter	      */ 

	char		pcglen[5];
	char		pcgFile[8];
	
}_NATIVE_CDI_VARS;

/************************************************************************************************/
/*																								*/
/*			EO NATIVE_CDI.h																		*/
/*																								*/
/************************************************************************************************/
