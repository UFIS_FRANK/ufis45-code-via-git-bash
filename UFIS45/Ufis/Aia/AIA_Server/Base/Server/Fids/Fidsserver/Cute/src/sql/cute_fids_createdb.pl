#!/usr/bin/perl


###################################################################################
#																																									#
#																																									#
#		SCRIPT FOR FILLING THE LOCAL DATABASE FOR THE CUTE INTERFACE ATHENS						#
#																																									#
#																																									#
###################################################################################
open(DATA, ">cute.sql") || die "File cute.sql couldn't be created.\n";

	open(TEMPLATE, "<cute_fids_alltables.sql") || die "File cute_fids_alltables.sql not found.\n";
	while(<TEMPLATE>)
	{
	   print DATA;
	}
	close(TEMPLATE);

###################################################################################
#	Create entries for checkins from 001 to 160																																								#
###################################################################################																																									#

for ($i=1;$i<=160;$i++) {
	if ($i < 10) {
	   $itext = "00".$i;
	   $dtext = "00".$i;
	}
	else {
		if ($i < 100) {
		   $itext = "0".$i;
	       $dtext = $i;		   
		}
		else {
		   $itext = $i;
		   $dtext = $i;
		}
	}
	
	$ctext = "C".$itext;	
	$ctext1 = "C1".$itext;
	$ctext2 = "C2".$itext;	

	open(TEMPLATE, "<cute_fids_templateCHECKIN.sql") || die "File cute_fids_templateCHECKIN.sql not found.\n";
	while(<TEMPLATE>)
	{
	   $_ =~ s/Cxxx/$ctext/gi;
	   $_ =~ s/C1xxx/$ctext1/gi;
	   $_ =~ s/C2xxx/$ctext2/gi;
	   $_ =~ s/xxx/$itext/gi;	   

	   print DATA $_;
	}
	close(TEMPLATE);
}

###################################################################################
#	Create entries for gate from A01 to A39 and B01 to B28																																								#
###################################################################################																																									#

for ($i=1;$i<=69;$i++) {
  if ($i < 40) {
    if ($i < 10) {
	  $dtext = "A0".$i;
    }
    else {
      if ($i < 100) {
        $dtext = "A".$i;
      }
      else {
        $dtext = "A".$j;
      }
    }
  }
  else {
	if ($j < 70) {
	$j = $i - 39; 
	} else { $j++;}
	if ($j < 10) {
   	   $dtext = "B0".$j;
	}
	else {
		if ($j <= 28) {
		   $dtext = "B".$j;
		}
		else {
			if ($j < 70) {
			$j = 70;}
		   $dtext = "B".$j;
		}
	  }
	}
	
	if ($i < 10) {
      $itext = "00".$i;
    }
    else {
      if ($i < 100) {
        $itext = "0".$i;
      }
      else {
        $itext = $i;
      }
    }

	$gtext = "G".$dtext;	
	$gtext1 = "G1".$dtext;
	$gtext2 = "G2".$dtext;	

	
	open(TEMPLATE, "<cute_fids_templateGATE.sql") || die "File cute_fids_templateGATE.sql not found.\n";
	while(<TEMPLATE>)
	{

	   $_ =~ s/Gxxx/$gtext/gi;
	   $_ =~ s/G1xxx/$gtext1/gi;
	   $_ =~ s/G2xxx/$gtext2/gi;
	   $_ =~ s/xxx/$dtext/gi;	   
	   print DATA $_;
	}
	close(TEMPLATE);
}

###################################################################################
#	Create entries for busgates																																								#
###################################################################################																																									#

for ($i=1;$i<=12;$i++) {
  if ($i < 5) {
  	$k = $i + 19;
	$dtext = "A".$k."A";
  }
  else {
    
  	if ($i<10) {$k = $i + 15;}
  	if ($i >= 10) {$k = $i + 16;}
	$dtext = "B".$k."A";
    }

	$gtext = "G".$dtext;	
	$gtext1 = "G1".$dtext;
	$gtext2 = "G2".$dtext;	

	
	open(TEMPLATE, "<cute_fids_templateGATE.sql") || die "File cute_fids_templateGATE.sql not found.\n";
	while(<TEMPLATE>)
	{

	   $_ =~ s/Gxxx/$gtext/gi;
	   $_ =~ s/G1xxx/$gtext1/gi;
	   $_ =~ s/G2xxx/$gtext2/gi;
	   $_ =~ s/xxx/$dtext/gi;	   
	   print DATA $_;
	}
	close(TEMPLATE);
}

close(DATA);
