
/**
 * Title:       SQL_HandlerSuperclass.class
 * Description: Super class for the SQL_Handler class.
 * Copyright:   Copyright (c) <p>
 * Company:     ABB AAT
 * @author		MOS
 * @version 	1.0
 */


import java.sql.*;
import java.io.*;
import sun.misc.*;
import java.util.*;
import com.javaexchange.dbConnectionBroker.*;


/** SQL_HandlerSuperclass CLASS<BR><BR>
	This class creates a two-tier database connection pool that can be shared
	by many SQL_Handler classes through inheritance.. 
	The pool is configured in the files<BR><BR>
			DbConnectionORACLE.dat<BR>
			DbConnectionMYSQL.dat<BR><BR>
	in the /home/ceda/tomcat/conf/ directory.
	
	The connections are rebuild in configurable intervals. The user won't 
	fell any influnece by the refreshing, because one connection at a time
	will be rebuild and requests will be handled by the other connection during the rebuild time.
	
	There is a pool for the ORACLE connections and one for the MYSQL connections.
	
	Once a pool is initialized, it will remain for the use of many instances of the SQL_Handler class.
*/
 
public class SQL_HandlerSuperclass {

    protected static 	int igClassInits;
    protected static 	int igClassCount;
	protected static   	DbConnectionBroker myBrokerMYSQL, myBrokerORACLE;


/**  Constructor
		if not already done, an instance of the ORACLE DB connection pool and one of 
		the MYSQL DB connection pool are created.
*/	public SQL_HandlerSuperclass() {
		Properties p = new Properties();
		
		try {
			// Initialise connection parameters
		    String dbDriver;
	    	String dbServer;
	    	String dbLogin;
	    	String dbPassword;
	    	int minConns;		// Min. amounts of connections
	    	int maxConns; 		// Max. amounts of connections
	    	String logFileString;
	    	double maxConnTime; // Frequency of connection restart (0.1 = 10 times per day)
 	
		if (myBrokerMYSQL==null) {
			
	    	p.load(new FileInputStream("/home/ceda/tomcat/conf/DBConnectionMYSQL.dat"));

	    	dbDriver 		= (String) p.get("dbDriver");
	    	dbServer 		= (String) p.get("dbServer");
	    	dbLogin  		= (String) p.get("dbLogin");
	    	dbPassword 		= (String) p.get("dbPassword");
	    	minConns   			= Integer.parseInt((String) p.get("minConns"));
	    	maxConns   			= Integer.parseInt((String) p.get("maxConns"));
	    	logFileString 	= (String) p.get("logFileString");
	    	maxConnTime   	= (new Double((String)p.get("maxConnTime"))).doubleValue();
     		System.out.println("\n\nSQL_HandlerSuperclass: \ndbDriver: <" + dbDriver + ">\ndbServer: <" + dbServer + ">\nminConns: <" + minConns + ">\nmaxConns: <" +maxConns + ">\nlogFileString: <" +logFileString+ ">\nmaxConnTime: <" +maxConnTime + ">\n\n");
	   		myBrokerMYSQL 	= new DbConnectionBroker(dbDriver,dbServer,dbLogin,dbPassword,minConns,maxConns,logFileString,maxConnTime);
		}
			
			
		if (myBrokerORACLE==null) {			
			p = null;
			
			p = new Properties();
	    	p.load(new 	FileInputStream("/home/ceda/tomcat/conf/DBConnectionORACLE.dat"));
			
			dbDriver 		= (String) p.get("dbDriver");
	    	dbServer 		= (String) p.get("dbServer");
	    	dbLogin  		= (String) p.get("dbLogin");
	    	dbPassword 		= (String) p.get("dbPassword");
	    	minConns   		= Integer.parseInt((String) p.get("minConns"));
	    	maxConns   		= Integer.parseInt((String) p.get("maxConns"));
	    	logFileString 	= (String) p.get("logFileString");
	    	maxConnTime   	= (new Double((String)p.get("maxConnTime"))).doubleValue();
     		System.out.println("\n\nSQL_HandlerSuperclass: \ndbDriver: <" + dbDriver + ">\ndbServer: <" + dbServer + ">\nminConns: <" + minConns + ">\nmaxConns: <" +maxConns + ">\nlogFileString: <" +logFileString+ ">\nmaxConnTime: <" +maxConnTime + ">\n\n");
			myBrokerORACLE	= new DbConnectionBroker(dbDriver,dbServer,dbLogin,dbPassword,minConns,maxConns,logFileString,maxConnTime);
		}
	}
	catch (FileNotFoundException f) {}
	catch (IOException e) {}
    } 
	
	
}
 /***************************************************************************************/
 /*																						*/
 /* 				EO CLASS SQL_HandlerSuperclass	 									*/
 /*																						*/
 /***************************************************************************************/
