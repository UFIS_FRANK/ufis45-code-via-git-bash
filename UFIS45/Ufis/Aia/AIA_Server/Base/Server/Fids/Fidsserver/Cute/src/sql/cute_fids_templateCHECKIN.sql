##########################################################################################################
##		SQL STATEMENTS FOR CHECKIN xxx OBJECTS CUTE INTERFACE					##
##########################################################################################################

####### TABLE OBJECT #######
INSERT INTO object VALUES ('FLIGHTCxxx','FLIGHTCxxx',NULL,'FLIGHTCxxx','Listbox','SELECT FLNO, JFNO, ALC3, URNO, \'D\' as cmd from AFTTAB WHERE (TIFD BETWEEN TO_CHAR(SYSDATE -30/1440,\'YYYYMMDDHH24MISS\') AND TO_CHAR(SYSDATE+720/1440,\'YYYYMMDDHH24MISS\')) AND FTYP=\'O\'  AND (URNO IN  (SELECT FLNU from CCATAB  WHERE  (((CKIC = \'xxx\')  AND ((DECODE(CKBA,\'              \', CKBS, CKBA) < TO_CHAR(SYSDATE+30/1440,\'YYYYMMDDHH24MISS\') AND CKBS<>\' \')) AND ((CKEA > TO_CHAR(SYSDATE,\'YYYYMMDDHH24MISS\')) OR ((CKES LIKE \' %\') AND (CKEA LIKE \' %\'))  OR (CKEA LIKE \' %\'))))))  ORDER by tifd','SELECT ccatab.urno as urno, \' \' as jfno, \' \' as flno, alttab.alc2, \'C\' as cmd FROM alttab,ccatab WHERE  ccatab.flnu=alttab.urno(+)  AND   ccatab.ckic=\'xxx\'  AND   ccatab.ctyp=\'C\' AND ( ccatab.CKBS BETWEEN  TO_CHAR(SYSDATE-1,\'YYYYMMDDHH24MISS\') AND TO_CHAR(SYSDATE+30/1440,\'YYYYMMDDHH24MISS\')) AND  (ccatab.CKES > TO_CHAR(SYSDATE,\'YYYYMMDDHH24MISS\')) AND (CKBA=\' \')AND (CKEA=\' \') union all SELECT ccatab.urno as urno, \' \' as jfno, \' \' as flno, alttab.alc2, \'C\' as  cmd FROM alttab,ccatab WHERE  ccatab.flnu=alttab.urno(+)  AND   ccatab.ckic=\'xxx\'  AND   ccatab.ctyp=\'C\' AND ccatab.urno in (select rurn from fldtab where dseq>0 and aurn=0 and rnam=\'xxx\')','SETDATA','FFC');
INSERT INTO object VALUES ('STATUSCxxx','STATUSCxxx',NULL,'STATUSCxxx','Status','SELECT DECODE(AURN,0,RURN,AURN) as URNO FROM FLDTAB WHERE (RNAM = \'xxx\') AND (RTYP = \'CKIF\') AND (DSEQ > 0)',NULL,'SETDATA','CCA');

INSERT INTO object VALUES ('FreeRemarkC1xxx','FreeRemarkC1xxx',NULL,'FreeRemarkC1xxx','Text1','SELECT TEXT,FLDU AS URNO FROM FXTTAB WHERE  SORT=\'1\' AND FLDU IN (SELECT URNO FROM FLDTAB WHERE RTYP = \'CKIF\' AND RNAM = \'xxx\' AND DSEQ > 0)',NULL,'SETDATA','FC1');
INSERT INTO object VALUES ('FreeRemarkC2xxx','FreeRemarkC2xxx',NULL,'FreeRemarkC2xxx','Text2','SELECT TEXT,FLDU AS URNO FROM FXTTAB WHERE  SORT=\'2\' AND FLDU IN (SELECT URNO FROM FLDTAB WHERE RTYP = \'CKIF\' AND RNAM = \'xxx\' AND DSEQ > 0)',NULL,'SETDATA','FC2');



####### TABLE USER #######
INSERT INTO user VALUES ('Cxxx',' ','CHECKINxxx','test',0,'C',NULL,NULL,'xxx');

####### TABLE USEROBJECTS #######
INSERT INTO userobjects VALUES ('Cxxx','FLIGHTCxxx');
INSERT INTO userobjects VALUES ('Cxxx','STATUSCxxx');
INSERT INTO userobjects VALUES ('Cxxx','REMARKC');
INSERT INTO userobjects VALUES ('Cxxx','LOGOC');
INSERT INTO userobjects VALUES ('Cxxx','FreeRemarkC1xxx');
INSERT INTO userobjects VALUES ('Cxxx','FreeRemarkC2xxx');

##########################################################################################################
##		END SECTION CHECKIN xxx 								##
##########################################################################################################
