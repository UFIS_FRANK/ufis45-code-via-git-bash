(v_urno in fldtab.urno%TYPE, v_num in number)
return char
is
rc_txt char(128);
CURSOR fxttab_curs (p_urno in fldtab.urno%TYPE) IS SELECT text FROM fxttab WHERE fxttab.fldu = p_urno;
BEGIN
	open fxttab_curs (v_urno);
	for i in 1..v_num loop
		fetch fxttab_curs into rc_txt;
		IF fxttab_curs%NOTFOUND THEN
			rc_txt := ' ';
		END IF;
		EXIT WHEN fxttab_curs%NOTFOUND;
	END LOOP;
	close fxttab_curs;
	RETURN (rtrim(rc_txt,' '));
END;
