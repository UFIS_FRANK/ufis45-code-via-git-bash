
/**
 * Title:       Log.class
 * Description: Logging functionality
 * Copyright:   Copyright (c) <p>
 * Company:     ABB AAT
 * @author		BSC
 * @version 	1.0
 */

import java.io.RandomAccessFile;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.text.*;


/** Log CLASS<BR><BR>
 * Log may be used by an arbitrary number of threads.
 */
public class Log extends RandomAccessFile {

  private SimpleDateFormat dtformat = new SimpleDateFormat();

/** CONSTRUCTOR 
	@param name: log file name including path
*/
  public Log(String name) throws FileNotFoundException {

    super(name, "rw");
    try {
      	seek(length());
    }
    catch (IOException E) {}
    dtformat.applyPattern("yyyy.MM.dd HH:mm:ss zzzz: ");
  }

/** Method Log
	@param Logentry:	String to be written into the file.
*/
  public synchronized void Log(String Logentry) throws IOException {
    Date date = new Date();
    writeBytes(dtformat.format(date) + Logentry + System.getProperty("line.separator"));
  }
} // EO CLass
 /***************************************************************************************/
 /*																						*/
 /* 				EO CLASS Log	 													*/
 /*																						*/
 /***************************************************************************************/
