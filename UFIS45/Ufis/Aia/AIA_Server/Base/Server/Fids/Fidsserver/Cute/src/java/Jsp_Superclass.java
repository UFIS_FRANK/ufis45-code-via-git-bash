
/**
 * Title:       Jsp_Superclass.class
 * Description: Super class for the Jsptest class.
 * Copyright:   Copyright (c) <p>
 * Company:     ABB AAT
 * @author		MOS
 * @version 	1.0
 */


package status;


import java.io.*;
import java.util.*;
import Stringlist;
import Log;
import SQL_Handler;


/** Jsp_Superclass CLASS<BR><BR>
	This class is the super class of the Jsptest class. Its purpose is to intiialize
	the SQL_Handler and the log class only once. The Jsptest class itself is 
	used for every call of one of its methods by the Java Server pages.	
*/
public class Jsp_Superclass {

  	protected static Log lg;
  	protected static SQL_Handler sh;

  

/** Constructor
		Creates an instance of the SQL_Handler class
		and the log class, if they are not yet created.
*/  
  public Jsp_Superclass() {
    try {
      	if (lg == null) {
      		lg = new Log("/home/ceda/tomcat/logs/CUTE_JSP.log");
      		//lg = new Log("/home/ceda/tomcat/logs/CUTE_MAINSERVLET.log");
		}
      	if (sh == null) {		
      		sh = new SQL_Handler(false,lg);
      		System.out.println("Jsp_Superclass INIT DONE.");
		}
    }
    catch (Exception E) {}
  } // constructor



/** Destructor
		Frees the instance of the SQL_Handler class
		and the log class.
*/  
  public void destroy() {

    try {
      lg.close();
    }
    catch (IOException E) {
    }
    sh = null;
  }


} // Jsp_Superclass
 /***************************************************************************************/
 /*																						*/
 /* 				EO CLASS Jsp_Superclass	 											*/
 /*																						*/
 /***************************************************************************************/
