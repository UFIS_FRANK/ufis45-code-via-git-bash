
/**
 * Title:       CDIInterface.class
 * Description: Enable access to and from CEDA via the CDI API
 * Copyright:   Copyright (c) 2000<p>
 * Company:     ABB AAT
 * @author		MOS
 * @version 	1.0
 */


import java.util.*;
import Stringlist;


/** CEDA INTERFACE CLASS<BR><BR>
	This class provides methods to send data (update, delete or insert) to CEDA
	by the use of the CEDA DATA INTERFACE.<BR>
	This means, that all CEDA processes will be informed about the executed
	data changes.<BR>
	One of the methods provides also a possiblity to start an update information service,
	which will cause an actualisation of the locallly stored data related to an object.
*/
public class CDIInterface extends Thread {

	/**********************************/
	/***			 				***/
	/*   	DECLARATIONS		  	  */
	/***							***/
	/**********************************/

	/** NATIVE METHODS from shared library libNATIVE_CDI.so<BR>
		This library contains functions from the CDI (CEDA DATA INTERFACE) API<BR>
	*/
    private native String NativeCDIUpdateInfo(String sectionname, String commandname);
    private native String NativeCDISetData(String sectionname, String objectname, String parameterlist);

	// Class for channeling the requests for the CDI update, inserts, and delete connection
    private CubbyHole ch; 
	// Logging class
    private Log lg; 
	// Command and Section which are configured in /home/ceda/tomcat/conf/NATIVE_CDI.cfg
	private String Section, Command; // BSC


	/**********************************/
	/***			 				***/
	/*   	PRIVATE FUNCTIONS	  	  */
	/***							***/
	/**********************************/


     	/** Method, which is called from the NativeCDIUpdateInfo function in the
	   shared library. 
	   As parameters, the used separator and data will be sent, if configured.
	   The method will cause the sqlhdl.class to reload the object's data from the AODB by the use of  JDBC.
	   
	   The method also filters the incoming events, which are being sent via the action and the cdiserver.
	   Because of limitations of the action process' filtering ability, the filterinhg will be done here for the
	   objects as specified in Athens.
	   The method extracts first all components and creates then the related objectids. AT last, the fillTempDB method 
	   from the SQL_Handler class is called with this objectid as parameter.
	   The important messages are:
	   	CKIF and CKIT
		CKIC
		CONU
		GDT1 and GTD2
	   	
		
	 A retrieved data telegram can look like this:
	 000000000000000000000#REFRESH#STATUSG#GDT2#A03
	 
	 or
	 
	 000000000000000000000#REFRESH#FLIGHTG#URNO#22778899#GDT1#A01#GDT2#A07
	 
	 
	 The functionality described above is performed in a thread because of performance issues.
	*/
    	private String pcmd (String values, String separator) {

    		// Process the incoming event
	    	CDIInterface_SubThread processevent = new CDIInterface_SubThread(values, separator, lg);
    	  	processevent.start();
			return "OK";
    	}
	/********************+ EO pcmd function  ****************************************/



     	/** Method, which is called from the NativeCDIUpdateInfo function in the
	   shared library. As parameters, the sql string is given
	   The method will cause the sqlhdl.class to execute the statement by the use of  JDBC.
	   It is used from the NativeCDIUpdateInfo to set the current IP of the CEDA server
	   and to set the status back to OK if any connection try has been successful.
	*/
    	private void psql (String sql) {
		try {
			//sqlclass.executeQuery(sql,0);
		}
		catch(NullPointerException e) {
					System.out.println(e.getMessage());
		}
    	}
	/********************+ EO pip function  ****************************************/


	/**********************************/
	/***			 				***/
	/*   	PUBLIC FUNCTIONS	  	  */
	/***							***/
	/**********************************/


	/** Constructor 1 CDIInterface<BR>
		@param cubbyhole: 	bottleneck for CDI requests to CEDA<BR>
		@param logger:		Log file.cfg<BR>
	*/		
        public CDIInterface(CubbyHole cubbyhole, Log logger) {

          	super();
          	ch = cubbyhole;
          	lg = logger;
	  	/*try {
			//System.out.println("CDIInterface Constructor1: SQL_Handler init");
	  		//sqlclass = new SQL_Handler(true,lg);
	   	} catch (Exception E) {
          		System.out.println("CDIInterface Constructor: " + E.getMessage());
          		return;
        	}*/

        }

	/** Constructor 2 CDIInterface<BR>
		@param logger:		Log file.cfg<BR>
		@param Section:		Section in /home/ceda/tomcat/conf/NATIVE_CDI.cfg.cfg<BR>
		@param Command:		Command in /home/ceda/tomcat/conf/NATIVE_CDI.cfg.cfg<BR>
	*/		
	public CDIInterface(String Sect, String Comm, Log logger) {
		
		super();
		ch = null;
		lg = logger;
		Section = Sect;
		Command = Comm;
		//try {
			//System.out.println("CDIInterface Constructor2: SQL_Handler init");
	  		//sqlclass = new SQL_Handler(true,lg);
	   //	} catch (Exception E) {
          	//	System.out.println("CDIInterface Constructor: " + E.getMessage());
          	//	return;
        	//}
	}


	/** Method writetoDB<BR>
			If data will be sent to CEDA then one request after the other has to 
			be processsed, because one CDI client con connect only to one CDi server.
			The cubbyhole class collects all the requests and releases them one after the
			other for sending their data.
	*/		
        public void writetoDB() {
          Stringlist data;
          for(;;) {
		  		// Get the data of one request
            	data = ch.get();  

				// If requests is meant ot send data to CEDA then call CDIInsert
            	if (data.getString(0).equalsIgnoreCase("setdata"))
             			 CDIInsert(data.getString(0), data.getString(1), data.getString(2)); // insert into DB
						 
				// Release blocking of CDIINsert
            	ch.declareready();  
          }
        }


	/** Method run<BR>
		Method to run update info service as thread
	*/		
        public void run() {

          if (ch == null) {
            	CDIUpdateservice(Section, Command); 
          } else {
            	writetoDB();
          }
	} // void run
	
	
	
	
	/** Method to insert Values in the AODB.<BR>
		@param sectionname: related sction in NATIVE_CDI.cfg<BR>
		@param command:	command to execute in NATIVE_CDI.cfg<BR>
		@param parameterlist: list of comma separated values with exactly<BR>
					as many parameters as configured in the INS section
					of the NATIVE_CDI.cfg file<BR><BR>
			Example: NATIVE_CDI.cfg <BR> [CLIENT_1]<BR>
						valid_cmds =  INF,INS<BR>
						[CLIENT_1_END]<BR><BR>

						[INS]<BR>
						cmd_type = 2<BR>
						arg_format = <BR>
						table = CCA<BR>
						fields = FLNU,CKIC,CKBS,CKES<BR>
						separator = #<BR>
						[INS_END]<BR><BR>

				String s = new String(cdi.CDIInsert("CLIENT_1", "21885626,23,20000728150000,20000728160000"));<BR>

				will cause an insert into the CCATAB with the values as given to the method
	*/
 		public String CDIInsert (String sectionname, String command, String parameterlist) {
			try {
					String s = new String(NativeCDISetData(sectionname, command, parameterlist));
					return s;
			}
				catch (Exception e)
			{
					String s = new String(e.getMessage());
					return s;
			}
		}
	/********************+ EO CDIInsert function  ****************************************/








	/** Method to start an update information service.<BR>
		@param sectionname: related sction in NATIVE_CDI.cfg<BR>
		@param parameterlist: related update info command<BR>
					as many parameters as configured in the related section
					of the NATIVE_CDI.cfg file<BR><BR>
			Example: NATIVE_CDI.cfg <BR> [CLIENT_1]<BR>
						valid_cmds =  INF,INS<BR>
						[CLIENT_1_END]<BR><BR>

						[INF]<BR>
						cmd_type = 2<BR>
						arg_format = <BR>
						table = CCA<BR>
						fields = URNO,ALC2,ALC3,ALFN<BR>
						separator = #<BR>
						[INF_END]<BR><BR>

				String s = new String(cdi.CDIUpdateservice("CLIENT_1", "INF"));<BR>

				The method will handle all incoming telegrams, also the
				watchdog telegrams. In case that a data telegram will be sent from the
				CDI server (which means that data for an object has been changed in the AODB),
				the private method CDIInterface.pmc will cause an reload of data.
				This will be done by the sqlhdl.class to reload the object's data from the AODB by the use of  JDBC.
				The connection will be closed, when the class instance will be deleted.
	*/

 		public String CDIUpdateservice (String sectionname, String command) {
			int ilNoofInterrupts;
			int ilNoofRetries;
			boolean blstatus;
			try {
					/* LOOP in which the information service is started and */
					/* will be restarted a configurable amount of retires	*/
					/* untill the status in the system table will be set to	*/
					/* system error.					*/
					/* If there is a HSB switch then the IP of the currently*/
					/* active server will be set into the system table as 	*/
					/* for the right JDBC connection			*/
					ilNoofInterrupts	= 0;
					blstatus 		= true;
					ilNoofRetries		= 5;
					/*ilNoofRetries = tostring(sqlclass.executequery("Select noofretries from system"));*/
					/* INSERT DEFAULT VALUE INTO SYSTEM for IP */
					/*sqlclass.executequery("UPDATE system set IP =" + );*/
					for (;;)
					{
						System.out.println("UPDATE SERVICE " + sectionname + " STARTED ...");
						String s = new String(NativeCDIUpdateInfo(sectionname, command));
						ilNoofInterrupts++;
						Thread.sleep(1000);
						if (ilNoofInterrupts>=ilNoofRetries)
						{
							blstatus		= false;
							ilNoofInterrupts	= 0;							
							/*sqlclass.executeQuery("UPDATE system set IP = /'ERROR/'");*/
						}/* if max amount of retries achieved */
					}
			}
			catch (Exception e)
			{
					String s = new String(e.getMessage());
					return s;
			}
		}
	/********************+ EO CDIUpdateservice function  ****************************************/







	/** Method to update values in the AODB.<BR>
		@param sectionname: related sction in cdiclient.cfg<BR>
		@param command:	command to execute in NATIVE_CDI.cfg<BR>
		@param parameterlist: list of comma separated values with exactly<BR>
					as many parameters as configured in the UPD section
					of the NATIVE_CDI.cfg file<BR>
					PLUS one parameter for the SELECTION CRITERIA.
					Currently only one selection criteria is possible as argument: the URNO
					This is configured in the cdiserver configuration file on the CEDA side, which contains an entry:<BR>
					select = urno = %s<BR><BR>
			Example: NATIVE_CDI.cfg <BR> [CLIENT_1]<BR>
						valid_cmds =  UPD<BR>
						[CLIENT_1_END]<BR><BR>

						[UPD]<BR>
						cmd_type = 2<BR>
						arg_format = <BR>
						table = AFT<BR>
						fields = CKIF,CKIT,REMP<BR>
						separator = #<BR>
						[UPD_END]<BR><BR>

				String s = new String(cdi.CDIUpdate("UPDATE", "AFT", "23,25,BCL,21885626"));<BR>

				will cause an update of the record with urno 21885626 in the AFTTAB, where the fields CKIF,CKIT,REMP<BR>
				will get filled with the values 23,25,BCL.
	*/

 		public String CDIUpdate (String sectionname, String command, String parameterlist) {

			try {
					String s = new String(NativeCDISetData(sectionname, command, parameterlist));
					return s;
			}
			catch (Exception e)
			{
					String s = new String(e.getMessage());
					return s;
			}

		}
	/********************+ EO CDIUpdate function  ****************************************/



	/**********************************/
	/***			 				***/
	/*   LOADING OF SHARED LIBRARY	  */
	/***							***/
	/**********************************/
   	static {
   		System.out.println("LOADING lib_NATIVE_CDI.so ...\n");
		  try {
        		System.loadLibrary("NATIVE_CDI");
		  }
		  catch (Exception e)
		  {
    		System.out.println("ERROR LOADING lib_NATIVE_CDI.so:\n");
			  e.printStackTrace();
		  }
    	System.out.println("LOADING lib_NATIVE_CDI.so DONE!\n");
    }
 }
 /***************************************************************************************/
 /*																						*/
 /* 				EO CLASS CDIINTERFACE 												*/
 /*																						*/
 /***************************************************************************************/
