#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Aia/AIA_Server/Base/Server/Fids/Fidsserver/Cute/src/JNI/NATIVE_CDI.c 1.1 2004/10/04 16:57:10SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ************************************************** 	*/
/* JNI - CDI client API shared library                 	*/
/*                                                    	*/
/* Author: MOS                                        	*/
/*                                                    	*/
/* Description: The library contains methods			*/
/*		for the following CDI tasks:					*/
/*			insert/update/delete data					*/
/*			run update info service						*/
/*		The lib is designed for the use from 			*/
/*		Java programs by reqruiting the Java			*/
/*		Native Interface (JNI)							*/
/*		The shared lib is part of a Framework			*/
/*		which allows access to CEDA data				*/
/*		from applets/Java applications					*/
/*                                                    	*/
/*		The library is designed for the purpose of		*/
/*		minimal adminstrative intervention.				*/
/*		The initialisation of the method for delete		*/
/*		insert and update service is done once and as	*/
/*		long as the socket is active, onlky the sending	*/
/*		of telegrams is performed.						*/
/*														*/
/*		The update info service is meant to run 		*/
/*		permanently. The connections are automatically	*/
/*		rebuild if an error occurs. This catches also 	*/
/*		a server switch.								*/
/*       	                                            */
/* Date: 20-July-2000  	Initial version            		*/
/*														*/
/*		 06-Nov -2000  	Advanced trial version			*/
/*						with auto reconnect and			*/
/*						connection persistency			*/
/*														*/
/*		TODO: SET CEDASTATE IF SERVER SWITCHES			*/
/*														*/
/* ************************************************** 	*/

/* **************************************** */
/* Include file							    */
/* **************************************** */
#include "NATIVE_CDI.h"


/* **************************************** */
/* Local function definitions		        */
/* **************************************** */

	/* *************** 	*/
	/* Handling incoming telegrams	*/
	/* *************** 	*/
char	*LOCALHandle_CdiCmd(_NATIVE_CDI_VARS	*prgVars); 

	/* *************** 	*/
	/* Main CDI routines	*/
	/* *************** 	*/
int		LOCALInit(_NATIVE_CDI_VARS *prgVars, const char *pcpsectionname); 
char 	*LOCALSetData(_NATIVE_CDI_VARS *prgVars,  const char *pclcommandname, const char *pcpSQLBuffer); 


	/* *************** 	*/
	/* Support routines	*/
	/* *************** 	*/
char	*LOCALget_time(void); 				/* Get time stamp */
char	*LOCALHandle_Error(char *pcpError); 
int 	LOCALGetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel,char *pcpDef,char *pcpTrim);
int 	LOCALAddSecondsToCEDATime (char *pcpCEDATime, time_t tpSeconds, int ipFormat);


/* **************************************** */
/* Global variables definitions		        */
/* **************************************** */
/* 	Global instance for INSERTING, DELETING, and UPDATING ACTIONS	*/
/* 	This is used for connection persistency, because a connection 	*/
/*	rebuild for every telegram takes too much time					*/
_NATIVE_CDI_VARS	prgNATIVE_CDI_VARS; 


/* ******************************************************************/
/* CDI Connection data change routine (JNI)							*/
/*																	*/
/*	This routine can be called from the java servlets				*/
/*	to establish a connection and to send an 						*/
/*	update, delete or insert command along with the 				*/
/*	data to CEDA.													*/
/*																	*/
/*	Note, that for the update command								*/
/*	additional values besides the values							*/
/*	which are to be updated have to be sent.						*/
/*	Those will create the SELECTION part							*/
/*	of the telegram data string										*/
/*																	*/
/*	EG (ckbs,ckes fields are configured):							*/
/*		insert: 20000808120000#20000808140000#						*/
/*		update:	20000808120000#20000808140000#SELECTION#987654321	*/
/*			on the cdiserver side a 'select = where urno=%s'		*/
/*			will cause to execute an update for the datarecord		*/
/*			with urno 987654321										*/
/*																	*/
/*	EG (cdat is configured as dummy field for deletes)				*/
/*		update:	20000808120000#SELECTION#987654321					*/
/*			on the cdiserver side a 'select = where urno=%s'		*/
/*			will cause to execute an delete for the datarecord		*/
/*			with urno 987654321										*/
/*																	*/
/*	PLEASE NOTE THAT FOR CEDA INTEGRITRY DELETES AND UPDATES		*/
/*	SHALL ONLY BE DONE WITH THE URNO AS SELECTION PARAMETER			*/
/*	INSERTS CAN BE DONE BY THE USE OF THE IBT SQLHDL COMMAND		*/
/*	WHICH AUTOMATICALLY GENERATES A NEW URNO						*/
/*																	*/
/* ******************************************************************/
JNIEXPORT jobject JNICALL 
Java_CDIInterface_NativeCDISetData(JNIEnv *jnienv, jobject object, jstring sectionname, jstring commandname, jstring parameterlist)
{

	char	pclreturnstring[iMAX_READ_BUF];
	/* Process java string objects to c char */
	const char *pclsectionname		= (*jnienv)->GetStringUTFChars(jnienv,sectionname,0);
	const char *pclcommandname		= (*jnienv)->GetStringUTFChars(jnienv,commandname,0);
	const char *pclparameterlist	= (*jnienv)->GetStringUTFChars(jnienv,parameterlist,0);
	int ilRC;
	int ilConnectretries;

	/* initialize */
	ilRC 	= CDI_SUCCESS;
	memset(pclreturnstring,0x00,iMAX_READ_BUF);

	/* INIT ONLY ONCE */
	/* GET THE VALUES FOR THIS CONNECTION AND OPEN IT ONLY IF THE SOCKET ID IS 0*/
	ilRC 	= CDIGetClient(prgNATIVE_CDI_VARS.igDescr,&prgNATIVE_CDI_VARS.rgCDIClient);
	
	dbg(DEBUG,"\n\n\n\nNativeCDISetData: BEFORE with \n<%s> and socket <%d> at address<%x>",pclparameterlist,prgNATIVE_CDI_VARS.rgCDIClient.socket,&prgNATIVE_CDI_VARS);
	dbg(DEBUG,"NativeCDISetData: BEFORE with \nvalid cmdsn<%s> and cmd <%d> ",prgNATIVE_CDI_VARS.rgCDIClient.valid_cmds,pclcommandname);

	if ((prgNATIVE_CDI_VARS.rgCDIClient.socket <= 0) || (strstr(prgNATIVE_CDI_VARS.rgCDIClient.valid_cmds,pclcommandname)==NULL)){
		ilRC 	= LOCALInit(&prgNATIVE_CDI_VARS, pclsectionname);
		ilRC 	= CDIGetClient(prgNATIVE_CDI_VARS.igDescr,&prgNATIVE_CDI_VARS.rgCDIClient);
		dbg(TRACE,"\n\nNativeCDISetData: LOCALInit");
	} /* connection established??*/
	
	if ((strstr(prgNATIVE_CDI_VARS.rgCDIClient.valid_cmds,pclcommandname)==NULL)){
		dbg(TRACE,"\n\nNativeCDISetData:RETURNED <%s>\n\n",prgNATIVE_CDI_VARS.rgCDIClient.valid_cmds);
		return (*jnienv)->NewStringUTF(jnienv,pclreturnstring);
	} /* not for this section??*/
	
	if (ilRC!=CDI_SUCCESS) {
		sprintf(pclreturnstring,"NativeCDISetData  CDI INIT FAILURE");
		for (;;) {
			sleep(15);
			ilRC = LOCALInit(&prgNATIVE_CDI_VARS, pclsectionname);
			
	      	if(ilRC==CDI_SUCCESS)
		  	{
				prgNATIVE_CDI_VARS.igCnx=USED;
				break;
		 	}
			dbg(TRACE,"NativeCDISetData:LOCALInit retry no <%d>",ilConnectretries);
				
			if (ilConnectretries>prgNATIVE_CDI_VARS.rgCDIClient.max_connect_retries) 
			{
				ilConnectretries = 0;
			}
			ilConnectretries++;						
			
		} /* idle loop for getting connection */
	}
	
	/*	Now create a telegram with the data and send it */
	sprintf(pclreturnstring,LOCALSetData(&prgNATIVE_CDI_VARS, pclcommandname, pclparameterlist));
	
	/* Release the resources for the java strings before leaving */
	(*jnienv)->ReleaseStringUTFChars(jnienv,sectionname,pclsectionname);
	(*jnienv)->ReleaseStringUTFChars(jnienv,commandname,pclcommandname);
	(*jnienv)->ReleaseStringUTFChars(jnienv,parameterlist,pclparameterlist);

	if (strcmp(pclreturnstring,"DAT") == 0) {
		return (*jnienv)->NewStringUTF(jnienv,prgNATIVE_CDI_VARS.prgRcdTgram->data);
	}
	else {
		return (*jnienv)->NewStringUTF(jnienv,pclreturnstring);;
	}
} /* *******  EO Java_CUTESERVLET_CDIInterface_NativeCDISetData ************** */




/* ******************************************************************/
/* CDI Connection data update routine (JNI)							*/
/*																	*/
/*	This routine can be called from the java servlets				*/
/*	to establish a connection, send an								*/
/*	update info command and waits for								*/
/*	incoming data from ceda.										*/
/*																	*/
/*	After retrieveing the telegram with the updated data, 			*/
/*	a JAVA method is called	to process the actual data				*/
/*																	*/
/*	A telegram looks like following:								*/
/*		...#REFRESH#FLIGHTC#CONU#100#								*/
/*	The pcmd method in the CDIInterface.class will load				*/
/*	then the data for the FLIGHTC100 object from CEDA				*/
/*																	*/
/*	The routine catches connection losses by querying the socket id	*/
/*	If this ID is 0 then the connection will be rebuild.			*/
/*																	*/
/* ******************************************************************/
JNIEXPORT jobject JNICALL
Java_CDIInterface_NativeCDIUpdateInfo(JNIEnv *jnienv, jobject object, jstring sectionname, jstring commandname)
{
	/*Enable Call of JAVA methods*/
	jclass cls ;
	jmethodID mid;
	/*	
	jclass cls_sql ;
	jmethodID mid_sql;
	*/
	
	/* VARIABLE SECTION */
	_NATIVE_CDI_VARS	prgVars;				/* struct containing the variables for the functions */
	char	pclreturnstring[iMAX_READ_BUF];
	const char *pclsectionname	= (*jnienv)->GetStringUTFChars(jnienv,sectionname,0);
	const char *pclcommandname		= (*jnienv)->GetStringUTFChars(jnienv,commandname,0);
	char	pcltelegramstring[iMAX_READ_BUF];
	char	pclcedatime[iMAX];
	char 	pcldatastrlen[5];
	int 	ilRC = RC_SUCCESS;
	
	/* reset variables*/
	memset(pcltelegramstring,0x00,iMAX_READ_BUF);
	memset(pclreturnstring,0x00,iMAX_READ_BUF);
	memset(pclcedatime,0x00,iMAX);
	memset(pcldatastrlen,0x00,5);

	/*Enable Call of JAVA methods*/
	/*method mid for setting the actualised values for objects*/
	cls = (*jnienv)->GetObjectClass(jnienv,object);
	mid = (*jnienv)->GetMethodID(jnienv,cls,"pcmd","(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;");
	
	/*method 2 for executing a sql statement for setting the CEDASTATE*
	cls_sql = (*jnienv)->GetObjectClass(jnienv,object);
	mid_sql = (*jnienv)->GetMethodID(jnienv,cls_sql,"psql","(Ljava/lang/String;)V");
	*/
	
	/*Enabled ??*/
	if (mid == 0) {
		sprintf(pclreturnstring,"METHOD OR CLASS NOT FOUND!! ");
		return (*jnienv)->NewStringUTF(jnienv,pclreturnstring);
	}
	/*
	if (mid_sql == 0) {
		sprintf(pclreturnstring,"METHOD OR CLASS NOT FOUND!! mid_sql <%d>",mid_sql);
		return (*jnienv)->NewStringUTF(jnienv,pclreturnstring);
	}
	*/
	/* INIT CONNECTIONS*/
	ilRC = LOCALInit(&prgVars, pclsectionname);
	
	/* GET THE VALUES FOR THIS CONNECTION */
	CDIGetClient(prgVars.igDescr,&prgVars.rgCDIClient);
		
	/*	SEND UPDATE INFO COMMAND */
	/*  This function is only called in the init process of the whole 	*/
	/*	application. Therefore no loops are required.					*/
	ilRC=CDIBuildHeader(prgVars.igDescr,prgVars.prgSndTgram,4,"CTL");
	if (ilRC!=CDI_SUCCESS)
	{
		dbg(TRACE,": ERROR BUILDING JUP HEADER!!ilRc=<%d>",ilRC);
		sprintf(pclreturnstring," ERROR BUILDING JUP HEADER!!ilRc=<%d>",ilRC);
		return (*jnienv)->NewStringUTF(jnienv,pclreturnstring);
	}
	else
	{
		/*** CREATE TELEGRAM **/
		sprintf(pclcedatime, "%s", LOCALget_time());
		LOCALAddSecondsToCEDATime(pclcedatime,3600,1);
		sprintf(prgVars.prgSndTgram->data,"%s%s%s%s",pclcommandname,"TIMESTAMP          ",LOCALget_time(),pclcedatime);

		/*** GET TELEGRAM LENGTH **/
		sprintf(pcldatastrlen,"%d",strlen(prgVars.prgSndTgram->data));
		sprintf(prgVars.prgSndTgram->len,"%s",pcldatastrlen);	
		dbg(DEBUG,"\n\nTELEGRAM <%s>",prgVars.prgSndTgram->data);
		/*** SEND THE TELEGRAM **/
		ilRC=CDISendData(prgVars.igDescr,prgVars.prgSndTgram,prgVars.prgSndTgram->data,51);		
	}/* end if */
	
	/* Now WAITING for incoming telegrams and PROCESSING them */
	for (;;)
	{
		/* reset telegram buffer */
		memset(prgVars.prgRcdTgram,0x00,sizeof(_TGRAM_HDR)+MAX_TGRAM_SIZE);
		
		/* wait for the data (descriptor, max .no of seconds to wait)*/
		/* wait for one day */
		ilRC = CDITcpWaitTimeOut(prgVars.igDescr,86400);
		CDIGetClient(prgVars.igDescr,&prgVars.rgCDIClient);	
		dbg(TRACE,"START... with ilRC <%d> and rgCDIClient.socket <%d>",ilRC,prgVars.rgCDIClient.socket);

		switch(ilRC)
		{
			case  CDI_SERR:
					ilRC	= CDI_SUCCESS;					
					if (prgVars.rgCDIClient.socket <= 0){
						ilRC=CDICloseConnection(prgVars.igDescr,0,prgVars.prgRcdTgram,"SD");
						sleep(2);
	      				ilRC=CDIOpenConnection(prgVars.igDescr);
					}
	      			if(ilRC==CDI_SUCCESS)
		  			{
							ilRC=CDISendData(prgVars.igDescr,prgVars.prgSndTgram,prgVars.prgSndTgram->data,51);
	      					if(ilRC==CDI_SUCCESS)
		  					{									
									prgVars.igCnx=USED;
							}/* SEND SUCCESSFUL?*/
		 			}							
					dbg(TRACE,"\n\nUPDATE CDI_SERR:");
	      			break;
			case  CDI_FAIL:
					ilRC	= CDI_SUCCESS;
					if (prgVars.rgCDIClient.socket <= 0){
						ilRC=CDICloseConnection(prgVars.igDescr,0,prgVars.prgRcdTgram,"SD");
						sleep(2);
	      				ilRC=CDIOpenConnection(prgVars.igDescr);
					}
	      			if(ilRC==CDI_SUCCESS)
		  			{
							ilRC=CDISendData(prgVars.igDescr,prgVars.prgSndTgram,prgVars.prgSndTgram->data,51);
	      					if(ilRC==CDI_SUCCESS)
		  					{									
									prgVars.igCnx=USED;
							}/* SEND SUCCESSFUL?*/
		 			}							
					dbg(TRACE,"\n\nUPDATE INFO  CDI_FAIL:");
	      			break;
			case CDI_SUCCESS: {
				ilRC = CDIReceiveData(prgVars.igDescr,&prgVars.prgRcdTgram,&prgVars.igRcdTgramSize);	
				switch(ilRC)
				{
					case CDI_SUCCESS:					
	      					sprintf(pclreturnstring,"%s",LOCALHandle_CdiCmd(&prgVars));						
						if(strcmp(&prgVars.prgRcdTgram->data[3],"SD")==0)
						{
							/* Reopen the connection for the max amount of retries */
							for (;;) {
							ilRC=CDICloseConnection(prgVars.igDescr,0,prgVars.prgRcdTgram,"SD");
							sleep(2);
							dbg(TRACE,"\n\nUPDATE INFO RECONNECT <%d>\n",prgVars.igDescr);
	      							ilRC=CDIOpenConnection(prgVars.igDescr);
								sleep(1);
	      						if(ilRC==CDI_SUCCESS)
		  						{
									ilRC=CDISendData(prgVars.igDescr,prgVars.prgSndTgram,prgVars.prgSndTgram->data,51);
	      							if(ilRC==CDI_SUCCESS)
		  							{									
										prgVars.igCnx=USED;
										break;
									}/* SEND SUCCESSFUL?*/
		 						}							
							} /* for */
							
						} else {	
							dbg(TRACE,"\n\nUPDATE INFO CALLING JAVA WITH <%s>\n",pclreturnstring);					
							(*jnienv)->CallObjectMethod(jnienv,object,mid,(*jnienv)->NewStringUTF(jnienv, pclreturnstring),(*jnienv)->NewStringUTF(jnienv, prgVars.rgCDIClient.cmd_defn[0].separator));
							dbg(TRACE,"UPDATE INFO CALLING JAVA WITH <%s> DONE...\n\n",pclreturnstring);
							
							/* RESEND TELEGRAM FOR MAINTAINING THE UPDATE SERVICE*/
							/*** CREATE TELEGRAM **/
							sprintf(pclcedatime, "%s", LOCALget_time());
							LOCALAddSecondsToCEDATime(pclcedatime,3600,1);
							sprintf(prgVars.prgSndTgram->data,"%s%s%s%s",pclcommandname,"TIMESTAMP          ",LOCALget_time(),pclcedatime);

							/*** GET TELEGRAM LENGTH **/
							sprintf(pcldatastrlen,"%d",strlen(prgVars.prgSndTgram->data));
							sprintf(prgVars.prgSndTgram->len,"%s",pcldatastrlen);	
							dbg(DEBUG,"\n\nTELEGRAM <%s>",prgVars.prgSndTgram->data);
							/*** SEND THE TELEGRAM **/
							ilRC=CDISendData(prgVars.igDescr,prgVars.prgSndTgram,prgVars.prgSndTgram->data,51);		
							 
						} /*if(strcmp(&prgVars.prgRcdTgram->data[3],"SD")==0)*/
	     					 break;
					case CDI_ECONN: /* Connect error */	
							for (;;) {
								ilRC=CDICloseConnection(prgVars.igDescr,0,prgVars.prgRcdTgram,"SD");
								sleep(2);
								dbg(TRACE,"\n\nUPDATE INFO RECONNECT <%d>\n",prgVars.igDescr);
	      						ilRC=CDIOpenConnection(prgVars.igDescr);
								sleep(1);
	      						if(ilRC==CDI_SUCCESS)
		  						{
									ilRC=CDISendData(prgVars.igDescr,prgVars.prgSndTgram,prgVars.prgSndTgram->data,51);
	      							if(ilRC==CDI_SUCCESS)
		  							{									
										prgVars.igCnx=USED;
										break;
									}/* SEND SUCCESSFUL?*/
		 						}							
							} /* for */
							dbg(TRACE,"UPDATE INFO Connect error");						
						
	      				break;
					case CDI_SERR:
					
							for (;;) {
							ilRC=CDICloseConnection(prgVars.igDescr,0,prgVars.prgRcdTgram,"SD");
							sleep(2);
							dbg(TRACE,"\n\nUPDATE INFO RECONNECT <%d>\n",prgVars.igDescr);
	      							ilRC=CDIOpenConnection(prgVars.igDescr);
								sleep(2);
	      						if(ilRC==CDI_SUCCESS)
		  						{
									ilRC=CDISendData(prgVars.igDescr,prgVars.prgSndTgram,prgVars.prgSndTgram->data,51);
	      							if(ilRC==CDI_SUCCESS)
		  							{									
										prgVars.igCnx=USED;
										break;
									}/* SEND SUCCESSFUL?*/
		 						}							
							} /* for */
							dbg(TRACE,"UPDATE INFO SOCKET ERROR!");	
						break;
					case CDI_FAIL:
							for (;;) {
							ilRC=CDICloseConnection(prgVars.igDescr,0,prgVars.prgRcdTgram,"SD");
							sleep(2);
							dbg(TRACE,"\n\nUPDATE INFO RECONNECT <%d>\n",prgVars.igDescr);
	      							ilRC=CDIOpenConnection(prgVars.igDescr);
								sleep(1);
	      						if(ilRC==CDI_SUCCESS)
		  						{
									ilRC=CDISendData(prgVars.igDescr,prgVars.prgSndTgram,prgVars.prgSndTgram->data,51);
	      							if(ilRC==CDI_SUCCESS)
		  							{									
										prgVars.igCnx=USED;
										break;
									}/* SEND SUCCESSFUL?*/
		 						}							
							} /* for */
							dbg(TRACE,"UPDATE INFO FAILED");	
						break;
					default:
							for (;;) {
							ilRC=CDICloseConnection(prgVars.igDescr,0,prgVars.prgRcdTgram,"SD");
							sleep(2);
							dbg(TRACE,"\n\nUPDATE INFO RECONNECT <%d>\n",prgVars.igDescr);
	      							ilRC=CDIOpenConnection(prgVars.igDescr);
								sleep(1);
	      						if(ilRC==CDI_SUCCESS)
		  						{
									ilRC=CDISendData(prgVars.igDescr,prgVars.prgSndTgram,prgVars.prgSndTgram->data,51);
	      							if(ilRC==CDI_SUCCESS)
		  							{									
										prgVars.igCnx=USED;
										break;
									}/* SEND SUCCESSFUL?*/
		 						}							
							} /* for */
	      					break;
	    			} /* end switch CDITcpReceiveData*/
				}
			default:					
					if (prgVars.rgCDIClient.socket <= 0){
						ilRC=CDICloseConnection(prgVars.igDescr,0,prgVars.prgRcdTgram,"SD");
						sleep(2);
	      				ilRC=CDIOpenConnection(prgVars.igDescr);
					}
	      			if(ilRC==CDI_SUCCESS)
		  			{
							ilRC=CDISendData(prgVars.igDescr,prgVars.prgSndTgram,prgVars.prgSndTgram->data,51);
	      					if(ilRC==CDI_SUCCESS)
		  					{									
									prgVars.igCnx=USED;
							}/* SEND SUCCESSFUL?*/
		 			}						

	      			break;
	    	} /* end switch CDITcpWaitTimeOut*/
		
	
	} /* end for idle loop */
	
	
	(*jnienv)->ReleaseStringUTFChars(jnienv,sectionname,pclsectionname);
	(*jnienv)->ReleaseStringUTFChars(jnienv,commandname,pclcommandname);
	return (*jnienv)->NewStringUTF(jnienv,"INFO SERVICE ENDED");
		
} /* *******  EO Java_CUTESERVLET_CDIInterface_NativeCDIUpdateInfo ************** */




/* ******************************************************************/
/* Initialisation routine											*/
/*																	*/
/*	This routine is called from every JNI method					*/
/*	A connection will be opened and all related vlaues will be 		*/
/*	stored in the prgVars structure.								*/
/*																	*/
/* ******************************************************************/
int LOCALInit(_NATIVE_CDI_VARS	*prgVars, const char *pcpsectionname) 
{
	char 	pcldummybuffer[iMAX_READ_BUF];
	int 	ilRC = RC_SUCCESS;
	
	/* ********************* */
    /* INITIALIZE structure  */
    /* ********************* */
	memset(pcldummybuffer,0x00,iMAX_READ_BUF);

	prgVars->igRcdTgramSize		= MAX_TGRAM_SIZE;
	prgVars->igSndTgramSize		= MAX_TGRAM_SIZE; 
	prgVars->igDATFlag			=	CDI_FALSE; 	
	prgVars->igCtlCounter		=	(int)2000;		/* CTL (general) Tgram counter */
    prgVars->igDatCounter		=	(int)2000;		/* DAT (general) Tgram counter */ 
    prgVars->igErrCounter		=	9000;			/* Error counter */ 
    prgVars->igErr				=	0;              /* No of consequetive errors */

	prgVars->prgRcdTgram		= (_TGRAM_HDR *)calloc(1,sizeof(_TGRAM_HDR)+MAX_TGRAM_SIZE);

    if(prgVars->prgRcdTgram==NULL)
	{
	  	ilRC = RC_FAIL;
		return ilRC;
	}/* end if */

    prgVars->prgSndTgram	=(_TGRAM_HDR *)calloc(1,MAX_TGRAM_SIZE);
    if(prgVars->prgSndTgram==NULL)
	{
		ilRC = RC_FAIL;
		return ilRC;
	} /* end if */


	/* ********************* */
    /* INITIALIZE CDI Client */
    /* ********************* */
    sprintf(pcldummybuffer,"%s",pcpsectionname);
    prgVars->igDescr=CDIInitClient(CONFIG_FILE,pcldummybuffer);
    if(prgVars->igDescr==CDI_FAIL)
	{
		ilRC = RC_FAIL;
		return ilRC;
	} 
    	else 
	{
		prgVars->igCnx=FREE;			/* Connection not yet established */
	}  /* end if */

    /* **************** */
    /* OPEN CONNECTION */
      /* **************** */
   	 ilRC=CDIOpenConnection(prgVars->igDescr);
    	if(ilRC==CDI_FAIL)
	{
		ilRC = RC_FAIL;
		return ilRC;
	} 
    	else 
	{
		prgVars->igCnx=USED;		 	/* Connection established */
	} /* end if */

  return ilRC;
}
/* *******  EO LOCALInit() ************** */


/* ******************************************************************/
/* CDI Connection data processing routine 							*/
/*																	*/
/*	This routine sends update, delete, or insert telegrams			*/
/*	to the cdiserver process in the CEDA environment				*/
/*	and causes the cdiserver to execute the	related task			*/
/* 																	*/
/*	Note, that for the update command								*/
/*	additional values besides the values							*/
/*	which are to be updated have to be sent.						*/
/*	Those will create the SELECTION part							*/
/*	of the telegram data string										*/
/*																	*/
/*	EG (ckbs,ckes fields are configured):							*/
/*		insert: 20000808120000#20000808140000#						*/
/*		update:	20000808120000#20000808140000#SELECTION#987654321	*/
/*			on the cdiserver side a 'select = where urno=%s'		*/
/*			will cause to execute an update for the datarecord		*/
/*			with urno 987654321										*/
/*																	*/
/*	EG (cdat is configured as dummy field for deletes)				*/
/*		update:	20000808120000#SELECTION#987654321					*/
/*			on the cdiserver side a 'select = where urno=%s'		*/
/*			will cause to execute an delete for the datarecord		*/
/*			with urno 987654321										*/
/*																	*/
/*	PLEASE NOTE THAT FOR CEDA INTEGRITRY DELETES AND UPDATES		*/
/*	SHALL ONLY BE DONE WITH THE URNO AS SELECTION PARAMETER			*/
/*	INSERTS CAN BE DONE BY THE USE OF THE IBT SQLHDL COMMAND		*/
/*	WHICH AUTOMATICALLY GENERATES A NEW URNO						*/
/*																	*/
/*																	*/
/* ******************************************************************/
char *LOCALSetData(_NATIVE_CDI_VARS	*prgVars,  const char* pclcommandname, const char *pcpSQLBuffer) 
{
	char	pclreturnstring[iMAX_READ_BUF];
	char	pcltelegramstring[iMAX_READ_BUF];
	char 	pcldatastrlen[5];
	char	pclsqlbufferdummy[iMAX_READ_BUF];
	char	pclCmd[10];
	char	pclData[100];
	char	pclFieldname[50];
	int		ilNoofCmds		= 0;
	int		ilNoofFields	= 0;
	int		ilCnt,ilCnt2;
	int		ilRC 			= RC_SUCCESS;
	
	/* ********************* */
    /* INITIALIZE structure  */
    /* ********************* */	
	memset(pcltelegramstring,0x00,iMAX_READ_BUF);
	memset(pclreturnstring,0x00,iMAX_READ_BUF);
	memset(pclsqlbufferdummy,0x00,iMAX_READ_BUF);
	memset(pcldatastrlen,0x00,5);
	memset(pclCmd,0x00,10);
	memset(pclData,0x00,100);
	memset(pclFieldname,0x00,50);
	sprintf(pclsqlbufferdummy,"%s",pcpSQLBuffer);


	/* GET THE VALUES FOR THIS CONNECTION */
	CDIGetClient(prgVars->igDescr,&prgVars->rgCDIClient);
	
	/* ********************* */
    /* CREATE TELEGRAM    */
    /* ********************* */
	/* for all configured data fields, extract the data items from the parameterstring and add them */
      /* to the telegram string */
	ilNoofCmds = field_count(prgVars->rgCDIClient.valid_cmds);
	for (ilCnt=0;ilCnt<ilNoofCmds;ilCnt++) {
		LOCALGetDataItem(pclCmd,prgVars->rgCDIClient.valid_cmds,ilCnt+1,',',"","\0");
		if (strstr(pclcommandname,pclCmd)!=NULL) {
			ilNoofFields = field_count(prgVars->rgCDIClient.cmd_defn[ilCnt].fields);
			sprintf(pcltelegramstring,"%s",&prgVars->rgCDIClient.cmd_defn[ilCnt].separator);
			for (ilCnt2=0;ilCnt2<ilNoofFields;ilCnt2++) {
					LOCALGetDataItem(pclData,pcpSQLBuffer,ilCnt2+1,','," ","\0");
					LOCALGetDataItem(pclFieldname,prgVars->rgCDIClient.cmd_defn[ilCnt].fields,ilCnt2+1,',',"","\0");
					sprintf(pcltelegramstring,"%s%s",pcltelegramstring,pclFieldname);
					sprintf(pcltelegramstring,"%s%s",pcltelegramstring,&prgVars->rgCDIClient.cmd_defn[ilCnt].separator);
					sprintf(pcltelegramstring,"%s%s",pcltelegramstring,pclData);
					sprintf(pcltelegramstring,"%s%s",pcltelegramstring,&prgVars->rgCDIClient.cmd_defn[ilCnt].separator);
			}
			break;
		}		
	}
	
	/* FOR UPDATE COMMANDS A SELECTION PART FOLLOWS in THE TELEGRAM*/
	ilCnt2 = ilCnt2+1;
	
	LOCALGetDataItem(pclData,pclsqlbufferdummy,ilCnt2,',',"","\0");

	if (strlen(pclData)!=0) {
		sprintf(pcltelegramstring,"%sSELECTION%s",pcltelegramstring,&prgVars->rgCDIClient.cmd_defn[ilCnt].separator);
	} /*UPDATE TELEGRAM??*/
	while (strlen(pclData)!=0) {
		sprintf(pcltelegramstring,"%s%s",pcltelegramstring,pclData);
		sprintf(pcltelegramstring,"%s%s",pcltelegramstring,&prgVars->rgCDIClient.cmd_defn[ilCnt].separator);
		ilCnt2++;
		LOCALGetDataItem(pclData,pclsqlbufferdummy,ilCnt2,',',"","\0");
	}
	dbg(TRACE,"LOCALSetData telegram data: <%s>\n\n",pcltelegramstring);

	/* ********************* */
    /* SEND TELEGRAM    */
    /* ********************* */
	/*** BUILD HEADER **/
	ilRC=CDIBuildHeader(prgVars->igDescr,prgVars->prgSndTgram,4,"DAT");

	/*** CREATE TELEGRAM **/
	sprintf(prgVars->prgSndTgram->data,"%s%s%s%s",pclcommandname,LOCALget_time(),"RGFFL0000000000000000000000000",pcltelegramstring);

	/*** GET TELEGRAM LENGTH **/
	sprintf(pcldatastrlen,"%d",strlen(prgVars->prgSndTgram->data));
	sprintf(prgVars->prgSndTgram->len,"%s",pcldatastrlen);

	if (ilRC==CDI_FAIL)
    	{
		strcpy(pclreturnstring,"LOCALSetData: ERROR BUILDING TELEGRAM!!");
		return pclreturnstring;
    	}  
	else
    	{	
		/*** SEND DATA **/	
		ilRC=CDISendData(prgVars->igDescr,prgVars->prgSndTgram,prgVars->prgSndTgram->data,strlen(prgVars->prgSndTgram->data));
		if (ilRC==CDI_SUCCESS)
		{
			prgVars->igErr=0;  /* Reset error counter */
			strcpy(pclreturnstring,"LOCALSetData: TELEGRAM SUCCESSFULLY SENT!!");
			return pclreturnstring;
  		} else if (ilRC==CDI_SERR) {
			/* Close associated socket */
			ilRC=CDICloseConnection(prgVars->igDescr,4,prgVars->prgSndTgram,NULL); 
			sleep(5);
			ilRC=CDIOpenConnection(prgVars->igDescr);
			ilRC=CDISendData(prgVars->igDescr,prgVars->prgSndTgram,prgVars->prgSndTgram->data,strlen(prgVars->prgSndTgram->data));
			strcpy(pclreturnstring,"LOCALSetData: SOCKET ERROR!!");
			return pclreturnstring;		
		} else if (ilRC==CDI_FAIL) {
			/* Close associated socket */
			ilRC=CDICloseConnection(prgVars->igDescr,4,prgVars->prgSndTgram,NULL); 
			sleep(5);
			ilRC=CDIOpenConnection(prgVars->igDescr);
			ilRC=CDISendData(prgVars->igDescr,prgVars->prgSndTgram,prgVars->prgSndTgram->data,strlen(prgVars->prgSndTgram->data));
			strcpy(pclreturnstring,"LOCALSetData: FAILED!!");
			return pclreturnstring;		
		} else {
			ilRC=CDICloseConnection(prgVars->igDescr,4,prgVars->prgSndTgram,NULL); 
			sleep(5);
			ilRC=CDIOpenConnection(prgVars->igDescr);
			ilRC=CDISendData(prgVars->igDescr,prgVars->prgSndTgram,prgVars->prgSndTgram->data,strlen(prgVars->prgSndTgram->data));
			return pclreturnstring;				
		} /* end if */
	} /* if (ilRC==CDI_FAIL) */
}
/* *******  EO LOCALSetData() ************** */



/* ******************************************************************/
/* 	The LOCALHandle_CdiCmd() routine	         	 				*/
/* 	Performs general handling of CDI telegrams 						*/
/*	This method is used for the update info service to process		*/
/*	the incoming telegrams according to their type.					*/
/*																	*/
/*	EG watchdogs telegrams with signature CCO will be answered		*/
/*	innediately.													*/
/*	Data telegrams will be filtered and the data part will be 		*/
/*	returned.														*/
/*																	*/
/* ******************************************************************/
char *LOCALHandle_CdiCmd(_NATIVE_CDI_VARS *prgVars) 
{
   int	ilRC;
   char	pclError[10];
   char	pclreturnstring[iMAX_READ_BUF];
   
   
   memset(pclreturnstring,0x00,iMAX_READ_BUF);
   
   /* ********************* */
   /* CHECK HEADER OF       */
   /* RECEIVED TELEGRAM		*/
   /* ********************* */
   if ((ilRC=CDICheckTgramHdr(prgVars->igDescr,prgVars->prgRcdTgram,pclError))!= CDI_SUCCESS) {
		switch (ilRC)
		{
		case CDI_ERROR:
			sprintf(pclreturnstring,"LOCALHandle_CdiCmd: Corrupt telegram header!! <%s>",prgVars->prgRcdTgram->data);
			CDIPrintTgramHdr(prgVars->prgRcdTgram,52);
			return pclreturnstring;
		case CDI_FAIL:
			sprintf(pclreturnstring,"LOCALHandle_CdiCmd: <null> pointer detected in parameter list !!");
			return pclreturnstring;
		default:
			sprintf(pclreturnstring,"LOCALHandle_CdiCmd: Unknown error in TGRAM header!!");
			return pclreturnstring;
		}/* End Switch */
   } /* if (CDICheckTgramHdr*/ 
	
   /* ********************* */
   /* RECEIVED TELEGRAM IS  */
   /* CONTROL TELEGRAM		*/
   /* ********************* */
  if (strncmp(prgVars->prgRcdTgram->tgram_class,"CTL",3) == 0) 
   {
		/* ********************* */
		/* WATCHDOG TELEGRAM     */
		/* ********************* */
		if (strncmp(prgVars->prgRcdTgram->data,"CCO",3)==0) 
		{
			if (prgVars->prgRcdTgram->data[3]=='C') 
			{
				memmove(prgVars->prgSndTgram,prgVars->prgRcdTgram,sizeof(_TGRAM_HDR)+5);
				ilRC = CDIAnswerCCO(prgVars->igDescr,prgVars->prgSndTgram);
			} 
			else 
			{
				ilRC = CDISendErr(prgVars->igDescr,prgVars->igErrCounter+1,prgVars->prgSndTgram,"AC");
			} /* end if */
			sprintf(pclreturnstring,"LOCALHandle_CdiCmd: Handled watchdog!!");
			return pclreturnstring;

		} /* end if */
		/* ********************* */
		/* ERROR TELEGRAM        */
		/* ********************* */
		else if (strncmp(prgVars->prgRcdTgram->data,"ERR",3)==0) 
		{
			sprintf(pclreturnstring,"LOCALHandle_CdiCmd: Received error telegram!!");
			return pclreturnstring;
		}      
		/* ******************************* */
		/* END OF CONNECTION TELEGRAM      */
		/* ******************************* */		
		else if (strncmp(prgVars->prgRcdTgram->data,"END",3)==0) 
		{
			if(strcmp(&prgVars->prgRcdTgram->data[3],"SD")==0)
			{
				if(prgVars->igCnx==USED)
				{
					ilRC=CDICloseConnection(prgVars->igDescr,4,prgVars->prgSndTgram,"");
					if(strcmp(&prgVars->prgRcdTgram->data[3],"SD")==0)
					{
						sprintf(pclreturnstring,"LOCALHandle_CdiCmd: System shutdown!!");
						prgVars->igCnx=FREE;
						return pclreturnstring;
					}
					else if(strcmp(&prgVars->prgRcdTgram->data[3],"SS")==0)
					{
						sprintf(pclreturnstring,"LOCALHandle_CdiCmd: System state switch (HSB)!!");
						prgVars->igCnx=FREE;
						return pclreturnstring;
					}
				}								
			}
		} 
		/* ******************************* */
		/* END OF DATA TELEGRAM            */
		/* ******************************* */
		else if(strncmp(prgVars->prgRcdTgram->data,"EOD",3)==0)
		{
			ilRC=CDIBuildHeader(prgVars->igDescr,prgVars->prgSndTgram,4,"CTL");
			if (ilRC!=CDI_SUCCESS)
			{
				dbg(TRACE,"LOCALHandle_CdiCmd: ERROR BUILDING JUP HEADER!!ilRc=<%d>",ilRC);
				sprintf(pclreturnstring,"LOCALHandle_CdiCmd: ERROR BUILDING JUP HEADER!!");
			}
			else
			{
				ilRC=CDISendData(prgVars->igDescr,prgVars->prgSndTgram,"SFUTIMESTAMP           2000101100000019981014235959",51);
				sprintf(pclreturnstring,"NEW");
			}/* end if */
			return pclreturnstring;
		}/* end if */
    }/* end if */
   /* ********************* */
   /* RECEIVED TELEGRAM IS  */
   /* DATA TELEGRAM		    */
   /* ********************* */
	else if (strncmp(prgVars->prgRcdTgram->tgram_class,"DAT",3) == 0)
    {
		prgVars->igErr=0; /* Reset error counter */
		sprintf(pclreturnstring,&prgVars->prgRcdTgram->data);
		return pclreturnstring;
    }
} /* *******  EO LOCALHandle_CdiCmd  ************** */






/*********************************************************/
/* The LOCALget_time routine                            */
/*********************************************************/
char *LOCALget_time(void)
{
  time_t	_CurTime;
  struct tm     rlCurTime; 
  static char  s[64];
  
  memset(s,'\0',1);
  _CurTime = time(0L);
  memcpy(&rlCurTime,(struct tm *)localtime(&_CurTime),sizeof(struct tm));


#if defined(_WINNT) || defined(WIN32)
    strftime(s,15,"%" "Y%" "m%" "d%" "H%" "M%" "S%",&rlCurTime);
#else
    strftime(s,15,"%" "Y%" "m%" "d%" "H%" "M%" "S%",&rlCurTime);
#endif
  dbg(DEBUG,"TIME <%s>",s);
  return s;
}


/* ******************************************************************
 * Function:   LOCALGetDataItem
 * Parameter:  OUT     char *pcpResult
 *             IN/OUT  char *pcpInput
 *             IN      int   ipNum     pos. of item in list
 *             IN      char  cpDel     Delimiter
 *             IN      char *pcpDef    Default Result if token is empty
 *             IN      char *pcpTrim   " \0" = trim left with space
 *                                     "\0 " = trim right with space
 *                                     "::"  = trim left and right colon
 *                                     "\0\0" = no trim
 *
 * Return Code: >=0          log. number of char. in result string
 *                           (blanks does not count as character )
 *                           pcpinput points behind hit
 *              -1..-5       <number> argument is NULL
 *                           pcpinput not changed
 *                           empty pcpResult string
 *
 * Result:      *prpResult contains found string
 *
 * Description: Get <ipNum>th item in <cpDel> seperated list
 *              and return the number of chars as return value
 *              and the item in <pcpResult>.
 *              If the '\0' is used
 *              as delimiter the number of the requested item
 *              must not exceed the total number of items, to
 *              avoid a coredump. Every other delimiter is not
 *              not critical, because end of string is check.
 *
 *UFIS 4.4 (c) ABB AAT/I fditools.c  44.1 / 00/01/06 11:20:36 / SMI";
* ******************************************************************/

int LOCALGetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel,char *pcpDef,char *pcpTrim)
{
  int ilRC;
  int ilLastChar;
  int i,ilPos;


  ilRC		= RC_SUCCESS;
  ilLastChar	= 0;
  ilPos		= 0;
  i		= 0;

  if      ( pcpResult == NULL ){
    ilRC = -1;
  }
  else if ( pcpInput == NULL ){
    ilRC = -2;
  }
  else if (ipNum == 0){
    ilRC = -3;
  }
  else if (pcpTrim == NULL){
    ilRC = -4;
  }
  else
    {
      while(ipNum > 1  && (*pcpInput != '\0' || cpDel == '\0'))
	{
	  if (*pcpInput == cpDel ){
	    if ( cpDel != pcpTrim[0] || pcpInput[1] != cpDel){
	      ipNum--;
	    }
	  }
	  pcpInput++;
	}/* end while*/

      pcpResult[0] = '\0';
      ilPos=ilLastChar=0;

      if (pcpTrim[0] != '\0')
	{
	  while (*pcpInput == pcpTrim[0])
	    pcpInput++;
	}
      /* Search first Delimiter character */
      while((*pcpInput != cpDel) && (*pcpInput != '\0') )
	{
	  pcpResult[ilPos] = *pcpInput;

	  if((pcpTrim != '\0' ) && (*pcpInput != pcpTrim[1]))
	    ilLastChar = ilPos+1 ;
	  pcpInput++;
	  ilPos++;
 	}
      if (*pcpInput == cpDel)
	pcpInput++;

      if ( pcpTrim[1] != '\0')
	ilPos = ilLastChar;

      pcpResult[ilPos]='\0';
      ilRC = ilPos;

      if((ilPos == 0)  && (*pcpDef != '\0'))
	{
	  strcpy(pcpResult,pcpDef);
	  ilRC = strlen(pcpDef)-1;
	  while((ilRC >= 0 ) && (pcpResult[ilRC] == ' '))
	    ilRC--;
	}
    } /* else end */

  return ilRC;
} /* end LOCALGetDataItem */


/*********************************************************************
Function   : LOCALAddSecondsToCEDATime
Paramter   : IN/OUT: pcpCEDATime  = String in CEDATimeformat where to add
             IN    : tpSeconds    = Number of seconds (+/-)
					   IN    : ipFormat     = Number of desired format
                                    (1 = CEDATimeformat)
ReturnCode : RC_SUCCESS, RC_FAIL
Description: returns a timestamp with the desired format in a string buffer
             Current formats are:
						 1 = CEDA-timestamp (YYYYMMDDHHMISS)
*UFIS 4.4 (c) ABB AAT/I cedatime.c 44.6 / 00/02/29 12:12:57 / TWE";
*********************************************************************/
int LOCALAddSecondsToCEDATime (char *pcpCEDATime, time_t tpSeconds, int ipFormat)
{
int        ilRC;
int        ilSummerFlag;
int        ilNumHundred;
long       llCnt;
long       llLen;
time_t     llSecVal;
char       pclBuf[8];
char       clNum;
struct tm  rlTimeStruct;
struct tm  rlTimeStruct2;
struct tm  *prlTimeStruct;


	ilRC          = RC_SUCCESS;
	ilSummerFlag  = 0;
	ilNumHundred  = 0;
	llCnt         = 0;
	llLen         = 0;
	llSecVal      = 0;

  /**** CHECK pcpCEDATime  ****/
  if (pcpCEDATime == NULL)                               /* Check if not NULL */
  {
     dbg(TRACE, "LOCALAddSecondsToCEDATime: No Time to add to (NULL)");
     ilRC = RC_FAIL;
  }
  else
  {
     llLen = strlen(pcpCEDATime);
     if (llLen != 14)                         /* Check for CEDA-Format Length */
     {
        dbg(TRACE, "LOCALAddSecondsToCEDATime: <%s> is no valid CEDATIME (14 Signs)",
                    pcpCEDATime);
        ilRC = RC_FAIL;
     }
     else
     {
        for (llCnt = 0;llCnt < llLen; llCnt++)
        {
           clNum = pcpCEDATime[llCnt];
           if (!(isdigit(clNum)))              /* Check if only numbers exist */
           {
              	ilRC = RC_FAIL;
		break;
           }
        }
     }
  }

  /***  Action part ***/
  if (ilRC == 0)
  {
     /* fill timestruct from CEDATIME */
     prlTimeStruct = &rlTimeStruct;

     memset(pclBuf,0x00,8);
/* year 2000 problem */
     memcpy(pclBuf,&pcpCEDATime[0],2);
     ilNumHundred = atoi(pclBuf) - 19;
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[2],2);
     prlTimeStruct->tm_year = atoi(pclBuf) + 100*ilNumHundred;
/******/
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[4],2);
     prlTimeStruct->tm_mon  = atoi(pclBuf) - 1;
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[6],2);
     prlTimeStruct->tm_mday = atoi(pclBuf);
     memset(pclBuf,0x00,8);
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[8],2);
     prlTimeStruct->tm_hour = atoi(pclBuf);
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[10],2);
     prlTimeStruct->tm_min  = atoi(pclBuf);
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[12],2);
     prlTimeStruct->tm_sec  = atoi(pclBuf);
     prlTimeStruct->tm_isdst = 0;
     prlTimeStruct->tm_yday  = 0;
     prlTimeStruct->tm_wday  = 0;

     /* get seconds from CEDATime */
     llSecVal = mktime (prlTimeStruct);
     if (llSecVal == -1)
     {
        dbg(TRACE,"LOCALAddSecondsToCEDATime: mktime of pcpCEDATime failed");
        ilRC = RC_FAIL;
        strcpy(pcpCEDATime, " ");
     }
     else
     {
        if (prlTimeStruct->tm_isdst == 1)   /* summertime not inserted before */
        {
           /* 1 hour back because struct was changed by mktime) */
           llSecVal -= 3600;
        }
        llSecVal += tpSeconds;                               /* add seconds */
     }                 /*
  }

	if (ilRC == RC_SUCCESS)
	{
	*/
     		/* init timestruct */
     		prlTimeStruct->tm_year = 0;
                prlTimeStruct->tm_mon  = 0;
                prlTimeStruct->tm_mday = 0;
                prlTimeStruct->tm_hour = 0;
                prlTimeStruct->tm_min  = 0;
                prlTimeStruct->tm_sec  = 0;
                prlTimeStruct->tm_isdst = 0;
                prlTimeStruct->tm_yday  = 0;
                prlTimeStruct->tm_wday  = 0;
                strcpy(pcpCEDATime, " ");
    	
                /* get struct with new time */
                prlTimeStruct = localtime(&llSecVal);
                rlTimeStruct2 = *prlTimeStruct;

                /* get CEDATIME from timestruct */
                if (ilRC == 0)
                {
           		switch(ipFormat)
          		{
           		  	case 1: /* CEDA-format */
           					   strftime(pcpCEDATime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S", &rlTimeStruct2);
           					   break;
           			default:
           					   dbg(TRACE,"LOCALAddSecondsToCEDATime: unknown format <%d>!",ipFormat);
                   	}
                }
       }

  return ilRC;

} /* end of LOCALAddSecondsToCEDATime */

/************************************************************************************************/
/*																								*/
/*			EO NATIVE_CDI.c																		*/
/*																								*/
/************************************************************************************************/
