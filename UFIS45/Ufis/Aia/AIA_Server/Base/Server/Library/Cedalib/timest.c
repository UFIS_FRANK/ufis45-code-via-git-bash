#ifndef _DEF_mks_version_timest_c
  #define _DEF_mks_version_timest_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_timest_c[] = "@(#) "UFIS_VERSION" $Id: Ufis/Aia/AIA_Server/Base/Server/Library/Cedalib/timest.c 1.1 2004/10/04 16:57:52SGT jim Exp  $";
#endif /* _DEF_mks_version_timest_c */

#include <stdio.h>
#include <string.h>

#include "timest.h"
#include "calutl.h"


time_t TimeStamp2Unix(char *cpStamp)
{
  char clDatTim[32];
  int ilCntDay;
  int ilCntMin;
  int ilWkDay;
  int ilRc;

  strcpy(clDatTim,cpStamp);
  if (clDatTim[0] == ' ')
  {
     return (-1);
  }
  ilRc = GetFullDay(clDatTim,&ilCntDay,&ilCntMin,&ilWkDay);
  ilCntMin *= 60;
  return ilCntMin;
}

char *TimeUnix2Stamp(time_t ipTime)
{
  char clDatTim[32];
  int ilCntMin;
  int ilRc;

  ilCntMin = ipTime / 60;
  ilRc = FullTimDate(clDatTim,ilCntMin);
  strcpy(&clDatTim[12],"00");
  return clDatTim;
}

char *TimeStampNow(void)
{
  time_t ilNow;
  static char clRes[15];
  struct tm *prlTm;

  ilNow = time(0L);
  prlTm = (struct tm *)localtime(&ilNow);
  strftime(clRes,15,"%""Y%""m%""d%""H%""M%""S",prlTm);
  return clRes;
}

time_t TimeDDMMMYYYY2Unix(char *pcpDateIn)
{
  char *pclPos;
  char clMonth[4];
  static char *clMonth3        = "JANFEBMAEAPRMAIJUNJULAUGSEPOKTNOVDEZ";
  static char *clMonth3English = "JANFEBMARAPRMAYJUNJULAUGSEPOCTNOVDEC";
  int ilYear = 0, ilMonth = 0, ilDay = 0;
  char clDatTim[32];
  int ilCntDay;
  int ilCntMin;
  int ilWkDay;
  int ilRc;

  clMonth[0] = pcpDateIn[2];
  clMonth[1] = pcpDateIn[3];
  clMonth[2] = pcpDateIn[4];
  clMonth[3] = '\0';

  pclPos = strstr(clMonth3, clMonth);
  if (pclPos)
    ilMonth = (pclPos - clMonth3) / 3 + 1;
  else
  {
    pclPos = strstr(clMonth3English, clMonth);
    if (pclPos)
      ilMonth = (pclPos - clMonth3English) / 3 + 1;
  }

  if (ilMonth == 0)
    return 0;
  
  sprintf(clMonth,"%.2d",ilMonth);
  strncpy(clDatTim,&pcpDateIn[5],4);
  strncpy(&clDatTim[4],clMonth,2);
  strncpy(&clDatTim[6],pcpDateIn,2);
  strcpy(&clDatTim[8],"000000");
  ilRc = GetFullDay(clDatTim,&ilCntDay,&ilCntMin,&ilWkDay); 
  ilCntMin *= 60;
  return ilCntMin;
}
