#ifndef _DEF_mks_version_gt2hdl_c
  #define _DEF_mks_version_gt2hdl_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_gt2hdl_c[] = "@(#) "UFIS_VERSION" $Id: Ufis/Aia/AIA_Server/Base/Server/Library/Cedalib/gt2hdl.c 1.1 2004/10/04 16:57:51SGT jim Exp  $";
#endif /* _DEF_mks_version_gt2hdl_c */
/******************************************************************************
*                                                                             *
*     GT2HDL.C - The GT2 handler                                              *
*     (LIB-functions for the DZINE GT(GraphicTerminal) communication)         *
******************************************************************************/

#include <string.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/select.h>
#include "glbdef.h"              
#include "gt2hdl.h"              
#ifndef min
#define min(a,b) ((a) < (b) ? (a) : (b))
#endif
#ifndef max
#define max(a,b) ((a) > (b) ? (a) : (b))
#endif

#define READSET 1
#define WRITESET 2

/******************************************************************************
*     External Variables                                                      *
******************************************************************************/

char *pcgGt2Buf = NULL;   /* Output Buffer for Controller */
long lgCurBufSiz;         /* Current Output Buffer Size   */
FILE *fh;                 /* File Handle                  */
char *pcgFbuf = NULL;     /* File Buffer                  */
long lgFsiz;              /* File Size                    */
int  igAnswer;            /* Answer Required              */
int  igStatReq;           /* Status Request Required      */

extern FILE *outp;
extern int errno;

extern int igdebug_switch;
extern  int  debug_level;
extern int igslow_connection;
/******************************************************************************
*                                                                             *
*     Command List                                                            *
*                                                                             *
******************************************************************************/

COM_LIST_STRUCT comList[] =
{
   {"gtFontDefine",gtFONT,gtFONT_DEFINE,1,{-1,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtFontUndefine",gtFONT,gtFONT_UNDEFINE,0,{0,0,0,0,0,0,0,0,0,0,0,0}},

   {"gtTrueTypeFontDefine", gtTRUETYPEFONTDEFINE, gtTRUE_TYPE_FONT_DEFINE, 10, {-1, 2, 2, 2, 2, -1, -1, 4, 2, 2, 0, 0} },
   {"gtTrueTypeFontUndefine", gtTRUETYPEFONTDEFINE, gtTRUE_TYPE_FONT_UNDEFINE, 0, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} },

   {"gtTextStyleDefine",gtTEXTSTYLE,gtTEXTSTYLE_DEFINE,6,{2,2,2,2,1,1,0,0,0,0,0,0}},
   {"gtTextStyleUndefine",gtTEXTSTYLE,gtTEXTSTYLE_UNDEFINE,0,{0,0,0,0,0,0,0,0,0,0,0,0}},

   {"gtTextDefine",gtTEXT,gtTEXT_DEFINE,3,{2,2,2,0,0,0,0,0,0,0,0,0}},
   {"gtTextDefineWithAttributes",gtTEXT,gtTEXT_DEFINE_WITH_ATTRIBUTES,9,{2,2,2,2,2,2,2,1,1,0,0,0}},
   {"gtTextUndefine",gtTEXT,gtTEXT_UNDEFINE,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtTextPut",gtTEXT,gtTEXT_PUT,1,{-1,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtTextPutFormatted",gtTEXT,gtTEXT_PUT_FORMATTED,1,{-1,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtTextPutCopy",gtTEXT,gtTEXT_PUT_COPY,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtTextScroll",gtTEXT,gtTEXT_SCROLL,4,{2,2,2,2,0,0,0,0,0,0,0,0}},

   {"gtLineDefine",gtLINE,gtLINE_DEFINE,7,{2,2,2,2,2,2,2,0,0,0,0,0}},
   {"gtLineUndefine",gtLINE,gtLINE_UNDEFINE,0,{0,0,0,0,0,0,0,0,0,0,0,0}},

   {"gtRectDefine",gtRECT,gtRECT_DEFINE,8,{2,2,2,2,2,2,2,2,0,0,0,0}},
   {"gtRectUndefine",gtRECT,gtRECT_UNDEFINE,0,{0,0,0,0,0,0,0,0,0,0,0,0}},

   {"gtEllipseDefine",gtELLIPSE,gtELLIPSE_DEFINE,6,{2,2,2,2,2,2,0,0,0,0,0,0}},
   {"gtEllipseUndefine",gtELLIPSE,gtELLIPSE_UNDEFINE,0,{0,0,0,0,0,0,0,0,0,0,0,0}},

   {"gtCollectionSetCurrent",gtPAGE,gtCOLLECTION_SET_CURRENT,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtTableSetCurrent",gtTABLE,gtTABLE_SET_CURRENT,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtWindowSetCurrent",gtWINDOW,gtWINDOW_SET_CURRENT,1,{2,0,0,0,0,0,0,0,0,0,0,0}},

   {"gtCollectionFreezeDrawing",gtPAGE,gtCOLLECTION_FREEZE_DRAWING,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtCollectionRedraw",gtPAGE,gtCOLLECTION_REDRAW,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtCollectionBringToFront",gtPAGE,gtCOLLECTION_BRING_TO_FRONT,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtTableBringToFront",gtTABLE,gtTABLE_BRING_TO_FRONT,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtWindowBringToFront",gtWINDOW,gtWINDOW_BRING_TO_FRONT,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtCollectionSendToBack",gtPAGE,gtCOLLECTION_SEND_TO_BACK,0,{0,0,0,0,0,0,0,0,0,0,0,0}},

   {"gtCollectionRefresh",gtPAGE,gtCOLLECTION_REFRESH,1,{2,0,0,0,0,0,0,0,0,0,0,0}},

   {"gtPageDefine",gtPAGE,gtPAGE_DEFINE,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtPageUndefine",gtPAGE,gtPAGE_UNDEFINE,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtPageSetCurrent",gtPAGE,gtCOLLECTION_SET_CURRENT,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtPageSetVideoMode",gtPAGE,gtPAGE_SET_VIDEO_MODE,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtPageDraw",gtPAGE,gtPAGE_DRAW,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtPageSetColor",gtPAGE,gtPAGE_SET_COLOR,4,{2,2,2,2,0,0,0,0,0,0,0,0}},
   {"gtPageSetBlinking",gtPAGE,gtPAGE_SET_BLINKING,4,{2,2,2,2,0,0,0,0,0,0,0,0}},
   {"gtPageLockColor",gtPAGE,gtPAGE_LOCK_COLOR,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtPageUnlockColors",gtPAGE,gtPAGE_UNLOCK_COLORS,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtPageConvertPictures",gtPAGE,gtPAGE_CONVERT_PICTURES,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtPageSetCarouselTime",gtPAGE,gtPAGE_SET_CAROUSEL_TIME,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtPageSetTextItem",gtPAGE,gtPAGE_SET_TEXT_ITEM,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtPageAddTextCarousel",gtPAGE,gtPAGE_ADD_TEXT_CAROUSEL,2,{2,2,0,0,0,0,0,0,0,0,0,0}},
   {"gtPageClearTextCarousel",gtPAGE,gtPAGE_CLEAR_TEXT_CAROUSEL,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtPageAddTextCarouselEffect",gtPAGE,gtPAGE_ADD_TEXT_CAROUSEL_EFFECT,6,{2,2,2,2,2,2,0,0,0,0,0,0}},
   {"gtPageSetTextCarouselMode",gtPAGE,gtPAGE_SET_TEXT_CAROUSEL_MODE,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtPageSetGIFItem",gtPAGE,gtPAGE_SET_GIF_ITEM,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtPageAddGIFCarousel",gtPAGE,gtPAGE_ADD_GIF_CAROUSEL,2,{2,2,0,0,0,0,0,0,0,0,0,0}},
   {"gtPageClearGIFCarousel",gtPAGE,gtPAGE_CLEAR_GIF_CAROUSEL,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtPageAddGIFCarouselEffect",gtPAGE,gtPAGE_ADD_GIF_CAROUSEL_EFFECT,6,{2,2,2,2,2,2,0,0,0,0,0,0}},
   {"gtPageSetGIFCarouselMode",gtPAGE,gtPAGE_SET_GIF_CAROUSEL_MODE,1,{2,0,0,0,0,0,0,0,0,0,0,0}},

   {"gtWindowDefine",gtWINDOW,gtWINDOW_DEFINE,8,{2,2,2,2,2,2,2,2,0,0,0,0}},
   {"gtWindowUndefine",gtWINDOW,gtWINDOW_UNDEFINE,0,{0,0,0,0,0,0,0,0,0,0,0,0}},

   {"gtSaWindowDefine",gtSAWINDOW,gtSAWINDOW_DEFINE,8,{2,2,2,2,2,2,2,2,0,0,0,0}},
   {"gtSaWindowUndefine",gtSAWINDOW,gtSAWINDOW_UNDEFINE,0,{0,0,0,0,0,0,0,0,0,0,0,0}},

   {"gtTableDefine",gtTABLE,gtTABLE_DEFINE,12,{2,2,2,2,2,2,2,2,2,2,2,2}},
   {"gtTableDefineView",gtTABLE,gtTABLE_DEFINE_VIEW,8,{2,2,2,2,2,2,2,2,0,0,0,0}},
   {"gtTableUndefine",gtTABLE,gtTABLE_UNDEFINE,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtTableAdjustColumn",gtTABLE,gtTABLE_ADJUST_COLUMN,2,{2,2,0,0,0,0,0,0,0,0,0,0}},
   {"gtTablePut",gtTABLE,gtTABLE_PUT,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtTableClearRow",gtTABLE,gtTABLE_CLEAR_ROW,2,{2,2,0,0,0,0,0,0,0,0,0,0}},
   {"gtTableClearColumn",gtTABLE,gtTABLE_CLEAR_COLUMN,2,{2,2,0,0,0,0,0,0,0,0,0,0}},
   {"gtTableDeleteRow",gtTABLE,gtTABLE_DELETE_ROW,2,{2,2,0,0,0,0,0,0,0,0,0,0}},
   {"gtTableInsertRow",gtTABLE,gtTABLE_INSERT_ROW,2,{2,2,0,0,0,0,0,0,0,0,0,0}},
   {"gtTableCarousel",gtTABLE,gtTABLE_CAROUSEL,2,{2,2,0,0,0,0,0,0,0,0,0,0}},
   {"gtTableCarouselEffect",gtTABLE,gtTABLE_CAROUSEL_EFFECT,6,{2,2,2,2,2,2,0,0,0,0,0,0}},

   {"gtAreaDefine",gtAREA,gtAREA_DEFINE,8,{2,2,2,2,2,2,2,2,0,0,0,0}},
   {"gtAreaUndefine",gtAREA,gtAREA_UNDEFINE,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtAreaSet",gtAREA,gtAREA_SET,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtAreaAddCarousel",gtAREA,gtAREA_ADD_CAROUSEL,2,{2,2,0,0,0,0,0,0,0,0,0,0}},
   {"gtAreaClearCarousel",gtAREA,gtAREA_CLEAR_CAROUSEL,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtAreaAddCarouselEffect",gtAREA,gtAREA_ADD_CAROUSEL_EFFECT,6,{2,2,2,2,2,2,0,0,0,0,0,0}},

   {"gtGIFDefine",gtGIF,gtGIF_DEFINE,4,{2,2,2,2,0,0,0,0,0,0,0,0}},
   {"gtGIFUndefine",gtGIF,gtGIF_UNDEFINE,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtGIFPut",gtGIF,gtGIF_PUT,2,{2,2,0,0,0,0,0,0,0,0,0,0}},
   {"gtGIFPutFile",gtGIF,gtGIF_PUT_FILE,1,{-1,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtGIFPutTmpFile",gtGIF,gtGIF_PUT_TMP_FILE,1,{-1,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtGIFPutCopy",gtGIF,gtGIF_PUT_COPY,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtGIFAdd",gtGIF,gtGIF_ADD,2,{2,-1,0,0,0,0,0,0,0,0,0,0}},
   {"gtGIFAddFile",gtGIF,gtGIF_ADD_FILE,1,{-1,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtGIFAddCopy",gtGIF,gtGIF_ADD_COPY,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtGIFAddTmpFile",gtGIF,gtGIF_ADD_TMP_FILE,1,{-1,0,0,0,0,0,0,0,0,0,0,0}},

   {"gtMpegVideoDefine",gtMPEG_VIDEO,gtMPEG_VIDEO_DEFINE,8,{2,2,2,2,2,2,2,2,0,0,0,0}},
   {"gtMpegVideoUndefine",gtMPEG_VIDEO,gtMPEG_VIDEO_UNDEFINE,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtMpegVideoPlay",gtMPEG_VIDEO,gtMPEG_VIDEO_PLAY,6,{2,2,-1,2,2,2,0,0,0,0,0,0}},
   {"gtMpegVideoRescale",gtMPEG_VIDEO,gtMPEG_VIDEO_RESCALE,8,{2,2,2,2,2,2,2,2,0,0,0,0}},
   {"gtMpegVideoHold",gtMPEG_VIDEO,gtMPEG_VIDEO_HOLD,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtMpegVideoContinue",gtMPEG_VIDEO,gtMPEG_VIDEO_CONTINUE,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtMpegVideoStop",gtMPEG_VIDEO,gtMPEG_VIDEO_STOP,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtMpegVideoBrightness",gtMPEG_VIDEO,gtMPEG_VIDEO_BRIGHTNESS,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtMpegVideoStatusRequest",gtMPEG_VIDEO,gtMPEG_VIDEO_STATUS_REQUEST,1,{2,0,0,0,0,0,0,0,0,0,0,0}},

   {"gtRealTimeVideoDefine",gtREAL_TIME_VIDEO,gtREAL_TIME_VIDEO_DEFINE,8,{2,2,2,2,2,2,2,2,0,0,0,0}},
   {"gtRealTimeVideoUndefine",gtREAL_TIME_VIDEO,gtREAL_TIME_VIDEO_UNDEFINE,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtRealTimeVideoPlay",gtREAL_TIME_VIDEO,gtREAL_TIME_VIDEO_PLAY,2,{2,4,0,0,0,0,0,0,0,0,0,0}},
   {"gtRealTimeVideoRescale",gtREAL_TIME_VIDEO,gtREAL_TIME_VIDEO_RESCALE,8,{2,2,2,2,2,2,2,2,0,0,0,0}},
   {"gtRealTimeVideoStop",gtREAL_TIME_VIDEO,gtREAL_TIME_VIDEO_STOP,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtRealTimeVideoBrightness",gtREAL_TIME_VIDEO,gtREAL_TIME_VIDEO_BRIGHTNESS,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtRealTimeVideoAdjustVideo",gtREAL_TIME_VIDEO,gtREAL_TIME_VIDEO_ADJUST_VIDEO,4,{2,2,2,2,0,0,0,0,0,0,0,0}},
   {"gtRealTimeVideoStatusRequest",gtREAL_TIME_VIDEO,gtREAL_TIME_VIDEO_STATUS_REQUEST,0,{0,0,0,0,0,0,0,0,0,0,0,0}},

   {"gtMonitorBrightness",gtMONITOR,gtMONITOR_BRIGHTNESS,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtMonitorContrast",gtMONITOR,gtMONITOR_CONTRAST,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtMonitorStandby",gtMONITOR,gtMONITOR_STANDBY,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtMonitorDegauss",gtMONITOR,gtMONITOR_DEGAUSS,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtMonitorStatusRequest",gtMONITOR,gtMONITOR_STATUS_REQUEST,0,{0,0,0,0,0,0,0,0,0,0,0,0}},

   {"gtTerminalInit",gtTERMINAL,gtTERMINAL_INIT,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtTerminalSetIntegrity",gtTERMINAL,gtTERMINAL_SET_INTEGRITY,1,{4,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtTerminalGetIntegrity",gtTERMINAL,gtTERMINAL_GET_INTEGRITY,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtTerminalSetDefaultVideoMode",gtTERMINAL,gtTERMINAL_SET_DEFAULT_VIDEO_MODE,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtTerminalStatusRequest",gtTERMINAL,gtTERMINAL_STATUS_REQUEST,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtTerminalCarousel",gtTERMINAL,gtTERMINAL_CAROUSEL,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtTerminalSetLogLevel",gtTERMINAL,gtTERMINAL_SET_LOG_LEVEL,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtTerminalSetTextItem",gtTERMINAL,gtTERMINAL_SET_TEXT_ITEM,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtTerminalSetKeyboardMode",gtTERMINAL,gtTERMINAL_SET_KEYBOARD_MODE,2,{2,2,0,0,0,0,0,0,0,0,0,0}},
   {"gtTerminalSetSystemMessage",gtTERMINAL,gtTERMINAL_SET_SYSTEM_MESSAGE,2,{2,-1,0,0,0,0,0,0,0,0,0,0}},
   {"gtTerminalSetBlinkingRate",gtTERMINAL,gtTERMINAL_SET_BLINKING_RATE,2,{2,2,0,0,0,0,0,0,0,0,0,0}},
   {"gtTerminalSetStartupPage",gtTERMINAL,gtTERMINAL_SET_STARTUP_PAGE,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtTerminalSetOfflinePage",gtTERMINAL,gtTERMINAL_SET_OFFLINE_PAGE,2,{2,2,0,0,0,0,0,0,0,0,0,0}},
   {"gtTerminalAddPageCarousel",gtTERMINAL,gtTERMINAL_ADD_PAGE_CAROUSEL,2,{2,2,0,0,0,0,0,0,0,0,0,0}},
   {"gtTerminalClearPageCarousel",gtTERMINAL,gtTERMINAL_CLEAR_PAGE_CAROUSEL,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtTerminalPreventClear",gtTERMINAL,gtTERMINAL_PREVENT_CLEAR,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtTerminalInverseImage",gtTERMINAL,gtTERMINAL_INVERSE_IMAGE,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtTerminalScreenSaver",gtTERMINAL,gtTERMINAL_SCREENSAVER,3,{-1,2,2,0,0,0,0,0,0,0,0,0}},

   {"gtFilerSetOutOfDateChecking",gtFILER,gtFILER_SET_OUT_OF_DATE_CHECKING,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtFilerPutFile",gtFILER,gtFILER_PUT_FILE,4,{4,2,-2,-1,0,0,0,0,0,0,0,0}},
   {"gtFilerPutData",gtFILER,gtFILER_PUT_DATA,2,{-3,-4,0,0,0,0,0,0,0,0,0,0}},
   {"gtFilerFindFirst",gtFILER,gtFILER_FIND_FIRST,2,{2,-1,0,0,0,0,0,0,0,0,0,0}},
   {"gtFilerFindNext",gtFILER,gtFILER_FIND_NEXT,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtFilerCreateDirectory",gtFILER,gtFILER_CREATE_DIRECTORY,1,{-1,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtFilerRemoveDirectory",gtFILER,gtFILER_REMOVE_DIRECTORY,1,{-1,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtFilerRemoveFile",gtFILER,gtFILER_REMOVE_FILE,2,{2,-1,0,0,0,0,0,0,0,0,0,0}},
   {"gtFilerStatusRequest",gtFILER,gtFILER_STATUS_REQUEST,1,{2,0,0,0,0,0,0,0,0,0,0,0}},

   {"gtReplyLog",gtREPLY,gtREPLY_LOG,1,{-1,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtReplyError",gtREPLY,gtREPLY_ERROR,4,{2,2,2,2,0,0,0,0,0,0,0,0}},
   {"gtReplyMpegVideoStatusRequest",gtREPLY,gtREPLY_MPEG_VIDEO_STATUS_REQUEST,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtReplyRealTimeVideoStatusRequest",gtREPLY,gtREPLY_REAL_TIME_VIDEO_STATUS_REQUEST,0,{0,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtReplyMonitorStatusRequest",gtREPLY,gtREPLY_MONITOR_STATUS_REQUEST,5,{2,2,2,2,-1,0,0,0,0,0,0,0}},
   {"gtReplyMonitorServiceMessage",gtREPLY,gtREPLY_MONITOR_SERVICE_MESSAGE,2,{2,-1,0,0,0,0,0,0,0,0,0,0}},
   {"gtReplyTerminalStatusRequest",gtREPLY,gtREPLY_TERMINAL_STATUS_REQUEST,7,{2,2,2,2,2,2,2,0,0,0,0,0}},
   {"gtReplyTerminalIntegrity",gtREPLY,gtREPLY_TERMINAL_INTEGRITY,1,{4,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtReplyFilerOperation",gtREPLY,gtREPLY_FILER_OPERATION,2,{2,2,0,0,0,0,0,0,0,0,0,0}},
   {"gtReplyFilerInfo",gtREPLY,gtREPLY_FILER_INFO,4,{2,4,4,-1,0,0,0,0,0,0,0,0}},
   {"gtReplyFilerStatusRequest",gtREPLY,gtREPLY_FILER_STATUS_REQUEST,3,{2,4,4,0,0,0,0,0,0,0,0,0}},
   {"gtReplyPage",gtREPLY,gtREPLY_PAGE,1,{2,0,0,0,0,0,0,0,0,0,0,0}},
   {"gtReplyKey",gtREPLY,gtREPLY_KEY,1,{2,0,0,0,0,0,0,0,0,0,0,0}},

   {"gtEnde",0,0,0,{0,0,0,0,0,0,0,0,0,0,0,0}}
};



/******************************************************************************
*                                                                             *
*     Constant Name List                                                      *
*                                                                             *
******************************************************************************/

CONST_NAME_STRUCT constList[] =
{
   {"gtFONT",gtFONT},
   {"gtTEXTSTYLE",gtTEXTSTYLE},
   {"gtTEXT",gtTEXT},
   {"gtLINE",gtLINE},
   {"gtRECT",gtRECT},
   {"gtELLIPSE",gtELLIPSE},
   {"gtPAGE",gtPAGE},
   {"gtWINDOW",gtWINDOW},
   {"gtTABLE",gtTABLE},
   {"gtGIF",gtGIF},
   {"gtMONITOR",gtMONITOR},
   {"gtTERMINAL",gtTERMINAL},
   {"gtREPLY",gtREPLY},
   {"gtFILER",gtFILER},
   {"gtAREA",gtAREA},
   {"gtSAWINDOW",gtSAWINDOW},
   {"gtMPEG_VIDEO",gtMPEG_VIDEO},
   {"gtREAL_TIME_VIDEO",gtREAL_TIME_VIDEO},
   {"gtTRUETYPEFONTDEFINE", gtTRUETYPEFONTDEFINE},

   {"gtFONT_DEFINE",gtFONT_DEFINE},
   {"gtFONT_UNDEFINE",gtFONT_UNDEFINE},
                                
   {"gtTEXTSTYLE_DEFINE",gtTEXTSTYLE_DEFINE},
   {"gtTEXTSTYLE_UNDEFINE",gtTEXTSTYLE_UNDEFINE},

   {"gtTEXT_DEFINE",gtTEXT_DEFINE},
   {"gtTEXT_DEFINE_WITH_ATTRIBUTES",gtTEXT_DEFINE_WITH_ATTRIBUTES},
   {"gtTEXT_UNDEFINE",gtTEXT_UNDEFINE},
   {"gtTEXT_PUT",gtTEXT_PUT},
   {"gtTEXT_PUT_FORMATTED",gtTEXT_PUT_FORMATTED},
   {"gtTEXT_PUT_COPY",gtTEXT_PUT_COPY},
   {"gtTEXT_SCROLL",gtTEXT_SCROLL},

   {"gtLINE_DEFINE",gtLINE_DEFINE},
   {"gtLINE_UNDEFINE",gtLINE_UNDEFINE},

   {"gtRECT_DEFINE",gtRECT_DEFINE},
   {"gtRECT_UNDEFINE",gtRECT_UNDEFINE},

   {"gtELLIPSE_DEFINE",gtELLIPSE_DEFINE},
   {"gtELLIPSE_UNDEFINE",gtELLIPSE_UNDEFINE},

   {"gtCOLLECTION_SET_CURRENT",gtCOLLECTION_SET_CURRENT},
   {"gtTABLE_SET_CURRENT",gtTABLE_SET_CURRENT},
   {"gtWINDOW_SET_CURRENT",gtWINDOW_SET_CURRENT},
   {"gtCOLLECTION_FREEZE_DRAWING",gtCOLLECTION_FREEZE_DRAWING},
   {"gtCOLLECTION_REDRAW",gtCOLLECTION_REDRAW},
   {"gtCOLLECTION_BRING_TO_FRONT",gtCOLLECTION_BRING_TO_FRONT},
   {"gtTABLE_BRING_TO_FRONT",gtTABLE_BRING_TO_FRONT},
   {"gtWINDOW_BRING_TO_FRONT",gtWINDOW_BRING_TO_FRONT},
   {"gtCOLLECTION_SEND_TO_BACK",gtCOLLECTION_SEND_TO_BACK},
   {"gtCOLLECTION_REFRESH",gtCOLLECTION_REFRESH},
                     
   {"gtPAGE_DEFINE",gtPAGE_DEFINE},
   {"gtPAGE_UNDEFINE",gtPAGE_UNDEFINE},
   {"gtPAGE_DRAW",gtPAGE_DRAW},
   {"gtPAGE_SET_COLOR",gtPAGE_SET_COLOR},
   {"gtPAGE_SET_BLINKING",gtPAGE_SET_BLINKING},
   {"gtPAGE_LOCK_COLOR",gtPAGE_LOCK_COLOR},
   {"gtPAGE_UNLOCK_COLORS",gtPAGE_UNLOCK_COLORS},
   {"gtPAGE_CONVERT_PICTURES",gtPAGE_CONVERT_PICTURES},
   {"gtPAGE_SET_VIDEO_MODE",gtPAGE_SET_VIDEO_MODE},
   {"gtPAGE_SET_CAROUSEL_TIME",gtPAGE_SET_CAROUSEL_TIME},
   {"gtPAGE_SET_TEXT_ITEM",gtPAGE_SET_TEXT_ITEM},
   {"gtPAGE_ADD_TEXT_CAROUSEL",gtPAGE_ADD_TEXT_CAROUSEL},
   {"gtPAGE_CLEAR_TEXT_CAROUSEL",gtPAGE_CLEAR_TEXT_CAROUSEL},
   {"gtPAGE_ADD_TEXT_CAROUSEL_EFFECT",gtPAGE_ADD_TEXT_CAROUSEL_EFFECT},
   {"gtPAGE_SET_TEXT_CAROUSEL_MODE",gtPAGE_SET_TEXT_CAROUSEL_MODE},
   {"gtPAGE_SET_GIF_ITEM",gtPAGE_SET_GIF_ITEM},
   {"gtPAGE_ADD_GIF_CAROUSEL",gtPAGE_ADD_GIF_CAROUSEL},
   {"gtPAGE_CLEAR_GIF_CAROUSEL",gtPAGE_CLEAR_GIF_CAROUSEL},
   {"gtPAGE_ADD_GIF_CAROUSEL_EFFECT",gtPAGE_ADD_GIF_CAROUSEL_EFFECT},
   {"gtPAGE_SET_GIF_CAROUSEL_MODE",gtPAGE_SET_GIF_CAROUSEL_MODE},

   {"gtWINDOW_DEFINE",gtWINDOW_DEFINE},
   {"gtWINDOW_UNDEFINE",gtWINDOW_UNDEFINE},

   {"gtSAWINDOW_DEFINE",gtSAWINDOW_DEFINE},
   {"gtSAWINDOW_UNDEFINE",gtSAWINDOW_UNDEFINE},

   {"gtTABLE_DEFINE",gtTABLE_DEFINE},
   {"gtTABLE_DEFINE_VIEW",gtTABLE_DEFINE_VIEW},
   {"gtTABLE_UNDEFINE",gtTABLE_UNDEFINE},
   {"gtTABLE_RESERVED1",gtTABLE_RESERVED1},
   {"gtTABLE_ADJUST_COLUMN",gtTABLE_ADJUST_COLUMN},
   {"gtTABLE_CLEAR_ROW",gtTABLE_CLEAR_ROW},
   {"gtTABLE_CLEAR_COLUMN",gtTABLE_CLEAR_COLUMN},
   {"gtTABLE_DELETE_ROW",gtTABLE_DELETE_ROW},
   {"gtTABLE_RESERVED2",gtTABLE_RESERVED2},
   {"gtTABLE_INSERT_ROW",gtTABLE_INSERT_ROW},
   {"gtTABLE_RESERVED3",gtTABLE_RESERVED3},
   {"gtTABLE_PUT",gtTABLE_PUT},
   {"gtTABLE_CAROUSEL",gtTABLE_CAROUSEL},
   {"gtTABLE_CAROUSEL_EFFECT",gtTABLE_CAROUSEL_EFFECT},

   {"gtAREA_DEFINE",gtAREA_DEFINE},
   {"gtAREA_UNDEFINE",gtAREA_UNDEFINE},
   {"gtAREA_SET",gtAREA_SET},
   {"gtAREA_ADD_CAROUSEL",gtAREA_ADD_CAROUSEL},
   {"gtAREA_CLEAR_CAROUSEL",gtAREA_CLEAR_CAROUSEL},
   {"gtAREA_ADD_CAROUSEL_EFFECT",gtAREA_ADD_CAROUSEL_EFFECT},

   {"gtGIF_DEFINE",gtGIF_DEFINE},
   {"gtGIF_UNDEFINE",gtGIF_UNDEFINE},
   {"gtGIF_PUT",gtGIF_PUT},
   {"gtGIF_PUT_FILE",gtGIF_PUT_FILE},
   {"gtGIF_PUT_COPY",gtGIF_PUT_COPY},
   {"gtGIF_PUT_TMP_FILE",gtGIF_PUT_TMP_FILE},
   {"gtGIF_ADD",gtGIF_ADD},
   {"gtGIF_ADD_FILE",gtGIF_ADD_FILE},
   {"gtGIF_ADD_COPY",gtGIF_ADD_COPY},
   {"gtGIF_ADD_TMP_FILE",gtGIF_ADD_TMP_FILE},

   {"gtMPEG_VIDEO_DEFINE",gtMPEG_VIDEO_DEFINE},
   {"gtMPEG_VIDEO_UNDEFINE",gtMPEG_VIDEO_UNDEFINE},
   {"gtMPEG_VIDEO_PLAY",gtMPEG_VIDEO_PLAY},
   {"gtMPEG_VIDEO_STOP",gtMPEG_VIDEO_STOP},
   {"gtMPEG_VIDEO_RESCALE",gtMPEG_VIDEO_RESCALE},
   {"gtMPEG_VIDEO_BRIGHTNESS",gtMPEG_VIDEO_BRIGHTNESS},
   {"gtMPEG_VIDEO_STATUS_REQUEST",gtMPEG_VIDEO_STATUS_REQUEST},
   {"gtMPEG_VIDEO_HOLD",gtMPEG_VIDEO_HOLD},
   {"gtMPEG_VIDEO_CONTINUE",gtMPEG_VIDEO_CONTINUE},

   {"gtREAL_TIME_VIDEO_DEFINE",gtREAL_TIME_VIDEO_DEFINE},
   {"gtREAL_TIME_VIDEO_UNDEFINE",gtREAL_TIME_VIDEO_UNDEFINE},
   {"gtREAL_TIME_VIDEO_PLAY",gtREAL_TIME_VIDEO_PLAY},
   {"gtREAL_TIME_VIDEO_STOP",gtREAL_TIME_VIDEO_STOP},
   {"gtREAL_TIME_VIDEO_RESCALE",gtREAL_TIME_VIDEO_RESCALE},
   {"gtREAL_TIME_VIDEO_BRIGHTNESS",gtREAL_TIME_VIDEO_BRIGHTNESS},
   {"gtREAL_TIME_VIDEO_STATUS_REQUEST",gtREAL_TIME_VIDEO_STATUS_REQUEST},
   {"gtREAL_TIME_VIDEO_ADJUST_VIDEO",gtREAL_TIME_VIDEO_ADJUST_VIDEO},

   {"gtMONITOR_BRIGHTNESS",gtMONITOR_BRIGHTNESS},
   {"gtMONITOR_CONTRAST",gtMONITOR_CONTRAST},
   {"gtMONITOR_STANDBY",gtMONITOR_STANDBY},
   {"gtMONITOR_DEGAUSS",gtMONITOR_DEGAUSS},
   {"gtMONITOR_WHITE_POINT",gtMONITOR_WHITE_POINT},
   {"gtMONITOR_STATUS_REQUEST",gtMONITOR_STATUS_REQUEST},
   {"gtMONITOR_SERVICE_MESSAGE",gtMONITOR_SERVICE_MESSAGE},

   {"gtTERMINAL_INIT",gtTERMINAL_INIT},
   {"gtTERMINAL_SET_INTEGRITY",gtTERMINAL_SET_INTEGRITY},
   {"gtTERMINAL_GET_INTEGRITY",gtTERMINAL_GET_INTEGRITY},
   {"gtTERMINAL_SET_LOG_LEVEL",gtTERMINAL_SET_LOG_LEVEL},
   {"gtTERMINAL_STATUS_REQUEST",gtTERMINAL_STATUS_REQUEST},
   {"gtTERMINAL_SET_DEFAULT_VIDEO_MODE",gtTERMINAL_SET_DEFAULT_VIDEO_MODE},
   {"gtTERMINAL_CAROUSEL",gtTERMINAL_CAROUSEL},
   {"gtTERMINAL_SET_TEXT_ITEM",gtTERMINAL_SET_TEXT_ITEM},
   {"gtTERMINAL_SET_KEYBOARD_MODE",gtTERMINAL_SET_KEYBOARD_MODE},
   {"gtTERMINAL_SET_SYSTEM_MESSAGE",gtTERMINAL_SET_SYSTEM_MESSAGE},
   {"gtTERMINAL_SET_BLINKING_RATE",gtTERMINAL_SET_BLINKING_RATE},
   {"gtTERMINAL_SET_STARTUP_PAGE",gtTERMINAL_SET_STARTUP_PAGE},
   {"gtTERMINAL_SET_SET_OFFLINE_PAGE",gtTERMINAL_SET_OFFLINE_PAGE},
   {"gtTERMINAL_ADD_PAGE_CAROUSEL",gtTERMINAL_ADD_PAGE_CAROUSEL},
   {"gtTERMINAL_CLEAR_PAGE_CAROUSEL",gtTERMINAL_CLEAR_PAGE_CAROUSEL},

   {"gtREPLY_LOG",gtREPLY_LOG},
   {"gtREPLY_ERROR",gtREPLY_ERROR},
   {"gtREPLY_MONITOR_STATUS_REQUEST",gtREPLY_MONITOR_STATUS_REQUEST},
   {"gtREPLY_MONITOR_SERVICE_MESSAGE",gtREPLY_MONITOR_SERVICE_MESSAGE},
   {"gtREPLY_TERMINAL_STATUS_REQUEST",gtREPLY_TERMINAL_STATUS_REQUEST},
   {"gtREPLY_TERMINAL_INTEGRITY",gtREPLY_TERMINAL_INTEGRITY},
   {"gtREPLY_FILER_OPERATION",gtREPLY_FILER_OPERATION},
   {"gtREPLY_FILER_INFO",gtREPLY_FILER_INFO},
   {"gtREPLY_FILER_STATUS_REQUEST",gtREPLY_FILER_STATUS_REQUEST},
   {"gtREPLY_PAGE",gtREPLY_PAGE},
   {"gtREPLY_KEY",gtREPLY_KEY},
   {"gtREPLY_MPEG_VIDEO_STATUS_REQUEST",gtREPLY_MPEG_VIDEO_STATUS_REQUEST},
   {"gtREPLY_REAL_TIME_VIDEO_STATUS_REQUEST",gtREPLY_REAL_TIME_VIDEO_STATUS_REQUEST},

   {"gtFILER_SET_OUT_OF_DATE_CHECKING",gtFILER_SET_OUT_OF_DATE_CHECKING},
   {"gtFILER_PUT_FILE",gtFILER_PUT_FILE},
   {"gtFILER_PUT_DATA",gtFILER_PUT_DATA},
   {"gtFILER_FIND_FIRST",gtFILER_FIND_FIRST},
   {"gtFILER_FIND_NEXT",gtFILER_FIND_NEXT},
   {"gtFILER_CREATE_DIRECTORY",gtFILER_CREATE_DIRECTORY},
   {"gtFILER_REMOVE_DIRECTORY",gtFILER_REMOVE_DIRECTORY},
   {"gtFILER_REMOVE_FILE",gtFILER_REMOVE_FILE},
   {"gtFILER_STATUS_REQUEST",gtFILER_STATUS_REQUEST},

   {"gtSYSTEM_FONT",gtSYSTEM_FONT},
   {"gtSYSTEM_TEXTSTYLE",gtSYSTEM_TEXTSTYLE},
   {"gtSYSTEM_MONITOR",gtSYSTEM_MONITOR},
   {"gtSYSTEM_TERMINAL",gtSYSTEM_TERMINAL},
   {"gtSYSTEM_FILER",gtSYSTEM_FILER},
   {"gtSYSTEM_MPEG_OBJECT", gtSYSTEM_MPEG_OBJECT},

   {"gtNORMAL",gtNORMAL},
   {"gtBOLD",gtBOLD},
   {"gtUNDERLINE",gtUNDERLINE},
   {"gtTRANSPARENT",gtTRANSPARENT},
   {"gtNORUND",gtNORMAL+gtUNDERLINE},
   {"gtNORTRANS",gtNORMAL+gtTRANSPARENT},
   {"gtNORUNDTRANS",gtNORMAL+gtUNDERLINE+gtTRANSPARENT},
   {"gtBOLDUND",gtBOLD+gtUNDERLINE},
   {"gtBOLDTRANS",gtBOLD+gtTRANSPARENT},
   {"gtBOLDUNDTRANS",gtBOLD+gtUNDERLINE+gtTRANSPARENT},
   {"gtBOLD|gtUNDERLINE",gtBOLD+gtUNDERLINE},
   {"gtSMOOTH",gtSMOOTH},
   {"gtTRANSPARENT|gtSMOOTH",gtTRANSPARENT+gtSMOOTH},

   {"gtLEFT",gtLEFT},
   {"gtCENTER",gtCENTER},
   {"gtRIGHT",gtRIGHT},

   {"gtBOTTOM",gtBOTTOM},
   {"gtBASE",gtBASE},
   {"gtMIDDLE",gtMIDDLE},
   {"gtTOP",gtTOP},

   {"gtSOLID_LINE",gtSOLID_LINE},
   {"gtDASHED_LINE",gtDASHED_LINE},
   {"gtDOTTED_LINE",gtDOTTED_LINE},

   {"gtBLACK",gtBLACK},
   {"gtBLUE",gtBLUE},
   {"gtGREEN",gtGREEN},
   {"gtCYAN",gtCYAN},
   {"gtRED",gtRED},
   {"gtMAGENTA",gtMAGENTA},
   {"gtBROWN",gtBROWN},
   {"gtWHITE",gtWHITE},
   {"gtGRAY",gtGRAY},
   {"gtLIGHTBLUE",gtLIGHTBLUE},
   {"gtLIGHTGREEN",gtLIGHTGREEN},
   {"gtLIGHTCYAN",gtLIGHTCYAN},
   {"gtLIGHTRED",gtLIGHTRED},
   {"gtLIGHTMAGENTA",gtLIGHTMAGENTA},
   {"gtYELLOW",gtYELLOW},
   {"gtLIGHTWHITE",gtLIGHTWHITE},

   {"gtCOLOR_ALICEBLUE"         ,  gtCOLOR_ALICEBLUE},           
    {"gtCOLOR_ANTIQUEWHITE"      ,  gtCOLOR_ANTIQUEWHITE},           
    {"gtCOLOR_AQUA"              ,  gtCOLOR_AQUA },               
    {"gtCOLOR_AQUAMARINE"        ,  gtCOLOR_AQUAMARINE},          
    {"gtCOLOR_AZURE"             ,  gtCOLOR_AZURE },              
    {"gtCOLOR_BEIGE"             ,  gtCOLOR_BEIGE},               
    {"gtCOLOR_BISQUE"            ,  gtCOLOR_BISQUE },             
    {"gtCOLOR_BLACK"             ,  gtCOLOR_BLACK },              
    {"gtCOLOR_BLANCHEDALMOND"    ,  gtCOLOR_BLANCHEDALMOND },     
    {"gtCOLOR_BLUE"            ,  gtCOLOR_BLUE},                
    {"gtCOLOR_BLUEVIOLET"      ,    gtCOLOR_BLUEVIOLET},          
    {"gtCOLOR_BROWN"           ,    gtCOLOR_BROWN },              
    {"gtCOLOR_BURLYWOOD"       ,    gtCOLOR_BURLYWOOD},           
    {"gtCOLOR_CADETBLUE"       ,    gtCOLOR_CADETBLUE},           
    {"gtCOLOR_CHARTREUSE"      ,    gtCOLOR_CHARTREUSE},          
    {"gtCOLOR_CHOCOLATE"       ,    gtCOLOR_CHOCOLATE},           
    {"gtCOLOR_CORAL"           ,    gtCOLOR_CORAL },              
    {"gtCOLOR_CORNFLOWERBLUE"  ,    gtCOLOR_CORNFLOWERBLUE},      
    {"gtCOLOR_CORNSILK"        ,    gtCOLOR_CORNSILK},            
    {"gtCOLOR_CRIMSON"         ,    gtCOLOR_CRIMSON},             
    {"gtCOLOR_CYAN"            ,    gtCOLOR_CYAN },               
    {"gtCOLOR_DARKBLUE"        ,    gtCOLOR_DARKBLUE},            
    {"gtCOLOR_DARKCYAN"        ,    gtCOLOR_DARKCYAN},            
    {"gtCOLOR_DARKGOLDENROD"   ,    gtCOLOR_DARKGOLDENROD},       
    {"gtCOLOR_DARKGRAY"        ,   gtCOLOR_DARKGRAY },           
    {"gtCOLOR_DARKGREEN"       ,    gtCOLOR_DARKGREEN},           
    {"gtCOLOR_DARKKHAKI"       ,    gtCOLOR_DARKKHAKI},           
    {"gtCOLOR_DARKMAGENTA"     ,    gtCOLOR_DARKMAGENTA },        
    {"gtCOLOR_DARKOLIVEGREEN"  ,    gtCOLOR_DARKOLIVEGREEN},      
    {"gtCOLOR_DARKORANGE"      ,    gtCOLOR_DARKORANGE},          
    {"gtCOLOR_DARKORCHID"      ,    gtCOLOR_DARKORCHID},          
    {"gtCOLOR_DARKRED"         ,    gtCOLOR_DARKRED },            
    {"gtCOLOR_DARKSALMON"      ,    gtCOLOR_DARKSALMON },         
    {"gtCOLOR_DARKSEAGREEN"    ,    gtCOLOR_DARKSEAGREEN},        
    {"gtCOLOR_DARKSLATEBLUE"   ,    gtCOLOR_DARKSLATEBLUE},       
    {"gtCOLOR_DARKSLATEGRAY"   ,    gtCOLOR_DARKSLATEGRAY},       
    {"gtCOLOR_DARKTURQUOISE"   ,    gtCOLOR_DARKTURQUOISE},       
    {"gtCOLOR_DARKVIOLET"      ,    gtCOLOR_DARKVIOLET },         
    {"gtCOLOR_DEEPPINK"        ,    gtCOLOR_DEEPPINK },           
    {"gtCOLOR_DEEPSKYBLUE"     ,    gtCOLOR_DEEPSKYBLUE},         
    {"gtCOLOR_DIMGRAY"         ,    gtCOLOR_DIMGRAY},             
    {"gtCOLOR_DODGERBLUE"      ,    gtCOLOR_DODGERBLUE},          
    {"gtCOLOR_FIREBRICK"       ,    gtCOLOR_FIREBRICK},           
    {"gtCOLOR_FLORALWHITE"     ,    gtCOLOR_FLORALWHITE},         
    {"gtCOLOR_FORESTGREEN"     ,    gtCOLOR_FORESTGREEN},         
    {"gtCOLOR_FUCHSIA"         ,    gtCOLOR_FUCHSIA},             
    {"gtCOLOR_GAINSBORO"       ,    gtCOLOR_GAINSBORO},           
    {"gtCOLOR_GHOSTWHITE"      ,    gtCOLOR_GHOSTWHITE},          
    {"gtCOLOR_GOLD"            ,    gtCOLOR_GOLD },               
    {"gtCOLOR_GOLDENROD"       ,    gtCOLOR_GOLDENROD},           
    {"gtCOLOR_GRAY"            ,    gtCOLOR_GRAY},                
    {"gtCOLOR_GREEN"           ,    gtCOLOR_GREEN},               
    {"gtCOLOR_GREENYELLOW"     ,    gtCOLOR_GREENYELLOW},         
    {"gtCOLOR_HONEYDEW"        ,    gtCOLOR_HONEYDEW},            
    {"gtCOLOR_HOTPINK"         ,    gtCOLOR_HOTPINK },            
    {"gtCOLOR_INDIANRED"       ,    gtCOLOR_INDIANRED},           
    {"gtCOLOR_INDIGO"          ,    gtCOLOR_INDIGO},              
    {"gtCOLOR_IVORY"           ,    gtCOLOR_IVORY },              
    {"gtCOLOR_KHAKI"            ,   gtCOLOR_KHAKI },              
    {"gtCOLOR_LAVENDER"         ,   gtCOLOR_LAVENDER },           
    {"gtCOLOR_LAVENDERBLUSH"    ,   gtCOLOR_LAVENDERBLUSH},         
    {"gtCOLOR_LAWNGREEN"        ,   gtCOLOR_LAWNGREEN},           
    {"gtCOLOR_LEMONCHIFFON"     ,   gtCOLOR_LEMONCHIFFON },       
    {"gtCOLOR_LIGHTBLUE"         ,  gtCOLOR_LIGHTBLUE},           
    {"gtCOLOR_LIGHTCORAL"        ,  gtCOLOR_LIGHTCORAL },         
    {"gtCOLOR_LIGHTCYAN"         ,  gtCOLOR_LIGHTCYAN},           
    {"gtCOLOR_LIGHTGOLDENRODYELLOW",gtCOLOR_LIGHTGOLDENRODYELLOW},
    {"gtCOLOR_LIGHTGREEN"         , gtCOLOR_LIGHTGREEN},          
    {"gtCOLOR_LIGHTGREY"          , gtCOLOR_LIGHTGREY},           
    {"gtCOLOR_LIGHTPINK"          , gtCOLOR_LIGHTPINK},           
    {"gtCOLOR_LIGHTSALMON"        , gtCOLOR_LIGHTSALMON},         
    {"gtCOLOR_LIGHTSEAGREEN"      , gtCOLOR_LIGHTSEAGREEN},       
    {"gtCOLOR_LIGHTSKYBLUE"       , gtCOLOR_LIGHTSKYBLUE },       
    {"gtCOLOR_LIGHTSLATEGRAY"     , gtCOLOR_LIGHTSLATEGRAY},      
    {"gtCOLOR_LIGHTSTEELBLUE"     , gtCOLOR_LIGHTSTEELBLUE},      
    {"gtCOLOR_LIGHTYELLOW"        , gtCOLOR_LIGHTYELLOW },        
    {"gtCOLOR_LIME"               , gtCOLOR_LIME},                
    {"gtCOLOR_LIMEGREEN"          , gtCOLOR_LIMEGREEN},           
    {"gtCOLOR_LINEN"              , gtCOLOR_LINEN },              
    {"gtCOLOR_MAGENTA"            , gtCOLOR_MAGENTA},             
    {"gtCOLOR_MAROON"             , gtCOLOR_MAROON},              
    {"gtCOLOR_MEDIUMAQUAMARINE"   , gtCOLOR_MEDIUMAQUAMARINE},    
    {"gtCOLOR_MEDIUMBLUE"         , gtCOLOR_MEDIUMBLUE},          
    {"gtCOLOR_MEDIUMORCHID"       , gtCOLOR_MEDIUMORCHID},        
    {"gtCOLOR_MEDIUMPURPLE"       , gtCOLOR_MEDIUMPURPLE },       
    {"gtCOLOR_MEDIUMSEAGREEN"     , gtCOLOR_MEDIUMSEAGREEN},      
    {"gtCOLOR_MEDIUMSLATEBLUE"    , gtCOLOR_MEDIUMSLATEBLUE},     
    {"gtCOLOR_MEDIUMSPRINGGREEN"  , gtCOLOR_MEDIUMSPRINGGREEN},   
    {"gtCOLOR_MEDIUMTURQUOISE"    , gtCOLOR_MEDIUMTURQUOISE},     
    {"gtCOLOR_MEDIUMVIOLETRED"    , gtCOLOR_MEDIUMVIOLETRED},     
    {"gtCOLOR_MIDNIGHTBLUE"       , gtCOLOR_MIDNIGHTBLUE},        
    {"gtCOLOR_MINTCREAM"          , gtCOLOR_MINTCREAM},           
    {"gtCOLOR_MISTYROSE"          , gtCOLOR_MISTYROSE},           
    {"gtCOLOR_MOCCASIN"           , gtCOLOR_MOCCASIN},            
    {"gtCOLOR_NAVAJOWHITE"        , gtCOLOR_NAVAJOWHITE},         
    {"gtCOLOR_NAVY"               , gtCOLOR_NAVY},                
    {"gtCOLOR_NAVYBLUE"           , gtCOLOR_NAVYBLUE},            
    {"gtCOLOR_OLDLACE"            , gtCOLOR_OLDLACE},             
    {"gtCOLOR_OLIVE"              , gtCOLOR_OLIVE },              
    {"gtCOLOR_OLIVEDRAB"          , gtCOLOR_OLIVEDRAB},           
    {"gtCOLOR_ORANGE"             , gtCOLOR_ORANGE},              
    {"gtCOLOR_ORANGERED"          , gtCOLOR_ORANGERED},           
    {"gtCOLOR_ORCHID"             , gtCOLOR_ORCHID},              
    {"gtCOLOR_PALEGOLDENROD"      , gtCOLOR_PALEGOLDENROD},       
    {"gtCOLOR_PALEGREEN"          , gtCOLOR_PALEGREEN},           
    {"gtCOLOR_PALETURQUOISE"      , gtCOLOR_PALETURQUOISE },      
    {"gtCOLOR_PALEVIOLETRED"      , gtCOLOR_PALEVIOLETRED},       
    {"gtCOLOR_PAPAYAWHIP"         , gtCOLOR_PAPAYAWHIP },         
    {"gtCOLOR_PEACHPUFF"          , gtCOLOR_PEACHPUFF},           
    {"gtCOLOR_PERU"              ,  gtCOLOR_PERU},                
    {"gtCOLOR_PINK"              ,  gtCOLOR_PINK},                
    {"gtCOLOR_PLUM"              ,  gtCOLOR_PLUM},                
    {"gtCOLOR_POWDERBLUE"        ,  gtCOLOR_POWDERBLUE},          
    {"gtCOLOR_PURPLE"             , gtCOLOR_PURPLE },             
    {"gtCOLOR_RED"                , gtCOLOR_RED},                 
    {"gtCOLOR_ROSYBROWN"          , gtCOLOR_ROSYBROWN},           
    {"gtCOLOR_ROYALBLUE"          , gtCOLOR_ROYALBLUE},           
    {"gtCOLOR_SADDLEBROWN"        , gtCOLOR_SADDLEBROWN},         
    {"gtCOLOR_SALMON"             , gtCOLOR_SALMON },             
    {"gtCOLOR_SANDYBROWN"         , gtCOLOR_SANDYBROWN},          
    {"gtCOLOR_SEAGREEN"           , gtCOLOR_SEAGREEN},            
    {"gtCOLOR_SEASHELL"           , gtCOLOR_SEASHELL},            
    {"gtCOLOR_SIENNA"             , gtCOLOR_SIENNA},              
    {"gtCOLOR_SILVER"             , gtCOLOR_SILVER },             
    {"gtCOLOR_SKYBLUE"            , gtCOLOR_SKYBLUE },            
    {"gtCOLOR_SLATEBLUE"          , gtCOLOR_SLATEBLUE},           
    {"gtCOLOR_SLATEGRAY"         ,  gtCOLOR_SLATEGRAY},           
    {"gtCOLOR_SNOW"              ,  gtCOLOR_SNOW },               
    {"gtCOLOR_SPRINGGREEN"       ,  gtCOLOR_SPRINGGREEN},         
    {"gtCOLOR_STEELBLUE"         ,  gtCOLOR_STEELBLUE},           
    {"gtCOLOR_TAN"               ,  gtCOLOR_TAN},                 
    {"gtCOLOR_TEAL"              ,  gtCOLOR_TEAL},                
    {"gtCOLOR_THISTLE"           ,  gtCOLOR_THISTLE},             
    {"gtCOLOR_TOMATO"            ,  gtCOLOR_TOMATO},              
    {"gtCOLOR_TURQUOISE"         ,  gtCOLOR_TURQUOISE},           
    {"gtCOLOR_VIOLET"            ,  gtCOLOR_VIOLET},              
    {"gtCOLOR_WHEAT"             ,  gtCOLOR_WHEAT},               
    {"gtCOLOR_WHITE"             ,  gtCOLOR_WHITE},            
    {"gtCOLOR_WHITESMOKE"       ,   gtCOLOR_WHITESMOKE},          
    {"gtCOLOR_YELLOW"            ,  gtCOLOR_YELLOW},              
    {"gtCOLOR_YELLOWGREEN"       ,  gtCOLOR_YELLOWGREEN},         














   {"gtEFFECT_NONE",gtEFFECT_NONE},
   {"gtEFFECT_UP",gtEFFECT_UP},
   {"gtEFFECT_DOWN",gtEFFECT_DOWN},
   {"gtEFFECT_LEFT",gtEFFECT_LEFT},
   {"gtEFFECT_RIGHT",gtEFFECT_RIGHT},

   {"gtBLINK_SLOW",gtBLINK_SLOW},
   {"gtBLINK_NORMAL",gtBLINK_NORMAL},
   {"gtBLINK_FAST",gtBLINK_FAST},

   {"gtLOG_LEVEL_ALL",gtLOG_LEVEL_ALL},
   {"gtLOG_LEVEL_DEBUG",gtLOG_LEVEL_DEBUG},
   {"gtLOG_LEVEL_INFO",gtLOG_LEVEL_INFO},
   {"gtLOG_LEVEL_NOTICE",gtLOG_LEVEL_NOTICE},
   {"gtLOG_LEVEL_WARNING",gtLOG_LEVEL_WARNING},
   {"gtLOG_LEVEL_ERROR",gtLOG_LEVEL_ERROR},
   {"gtLOG_LEVEL_CRITICAL",gtLOG_LEVEL_CRITICAL},
   {"gtLOG_LEVEL_ALERT",gtLOG_LEVEL_ALERT},
   {"gtLOG_LEVEL_EMERGENCY",gtLOG_LEVEL_EMERGENCY},
   {"gtLOG_LEVEL_NONE",gtLOG_LEVEL_NONE},

   {"gtV640x480x256",gtV640x480x256},
   {"gtV800x600x256",gtV800x600x256},
   {"gtV800x600x64K",gtV800x600x64K},
   {"gtV1024x768x256",gtV1024x768x256},
   {"gtV1024x576x256",gtV1024x576x256},
   {"gtV856x480x256",gtV856x480x256},

   {"gtKEYBOARD_TABLE_SCROLL",gtKEYBOARD_TABLE_SCROLL},
   {"gtKEYBOARD_AREA_SCROLL",gtKEYBOARD_AREA_SCROLL},
   {"gtKEYBOARD_PAGE_BROWSE",gtKEYBOARD_PAGE_BROWSE},
   {"gtKEYBOARD_PAGE_RECALL",gtKEYBOARD_PAGE_RECALL},
   {"gtKEYBOARD_FUNCTION_KEYS",gtKEYBOARD_FUNCTION_KEYS},

   {"gtSYSTEM_MESSAGE_SUNDAY",gtSYSTEM_MESSAGE_SUNDAY},
   {"gtSYSTEM_MESSAGE_MONDAY",gtSYSTEM_MESSAGE_MONDAY},
   {"gtSYSTEM_MESSAGE_THURSDAY",gtSYSTEM_MESSAGE_THURSDAY},
   {"gtSYSTEM_MESSAGE_WEDNESDAY",gtSYSTEM_MESSAGE_WEDNESDAY},
   {"gtSYSTEM_MESSAGE_THURSDAY",gtSYSTEM_MESSAGE_THURSDAY},
   {"gtSYSTEM_MESSAGE_FRIDAY",gtSYSTEM_MESSAGE_FRIDAY},
   {"gtSYSTEM_MESSAGE_SATURDAY",gtSYSTEM_MESSAGE_SATURDAY},
   {"gtSYSTEM_MESSAGE_JANUARY",gtSYSTEM_MESSAGE_JANUARY},
   {"gtSYSTEM_MESSAGE_FEBRUARY",gtSYSTEM_MESSAGE_FEBRUARY},
   {"gtSYSTEM_MESSAGE_MARCH",gtSYSTEM_MESSAGE_MARCH},
   {"gtSYSTEM_MESSAGE_APRIL",gtSYSTEM_MESSAGE_APRIL},
   {"gtSYSTEM_MESSAGE_MAY",gtSYSTEM_MESSAGE_MAY},
   {"gtSYSTEM_MESSAGE_JUNE",gtSYSTEM_MESSAGE_JUNE},
   {"gtSYSTEM_MESSAGE_JULY",gtSYSTEM_MESSAGE_JULY},
   {"gtSYSTEM_MESSAGE_AUGUST",gtSYSTEM_MESSAGE_AUGUST},
   {"gtSYSTEM_MESSAGE_SEPTEMBER",gtSYSTEM_MESSAGE_SEPTEMBER},
   {"gtSYSTEM_MESSAGE_OCTOBER",gtSYSTEM_MESSAGE_OCTOBER},
   {"gtSYSTEM_MESSAGE_NOVEMBER",gtSYSTEM_MESSAGE_NOVEMBER},
   {"gtSYSTEM_MESSAGE_DECEMBER",gtSYSTEM_MESSAGE_DECEMBER},
   {"gtSYSTEM_MESSAGE_PAGE_RECALL",gtSYSTEM_MESSAGE_PAGE_RECALL},

   {"gtFILER_OK",gtFILER_OK},
   {"gtFILER_END",gtFILER_END},
   {"gtFILER_OUT_OF_DATE",gtFILER_OUT_OF_DATE},
   {"gtFILER_ERROR",gtFILER_ERROR},

   {"gtMPEG_VIDEO_DEFAULT",gtMPEG_VIDEO_DEFAULT},
   {"gtMPEG_VIDEO_AUDIO",gtMPEG_VIDEO_AUDIO},
   {"gtMPEG_VIDEO_NO_RESTART",gtMPEG_VIDEO_NO_RESTART},
   {"gtMPEG_VIDEO_CLOSE_AT_END",gtMPEG_VIDEO_CLOSE_AT_END},
   {"gtMPEG_VIDEO_PREPARE_STATUS",gtMPEG_VIDEO_PREPARE_STATUS},
   {"gtMPEG_VIDEO_PLAY_STATUS",gtMPEG_VIDEO_PLAY_STATUS},
   {"gtMPEG_VIDEO_PREPARE_NO_RESULT_AVAIL",gtMPEG_VIDEO_PREPARE_NO_RESULT_AVAIL},
   {"gtMPEG_VIDEO_PREPARE_IN_PROGRESS",gtMPEG_VIDEO_PREPARE_IN_PROGRESS},
   {"gtMPEG_VIDEO_PREPARE_ENDED",gtMPEG_VIDEO_PREPARE_ENDED},
   {"gtMPEG_VIDEO_PREPARE_ABORTED",gtMPEG_VIDEO_PREPARE_ABORTED},
   {"gtMPEG_VIDEO_PREPARE_UNRECOV_ERROR",gtMPEG_VIDEO_PREPARE_UNRECOV_ERROR},
   {"gtMPEG_VIDEO_PLAY_NO_RESULT_AVAIL",gtMPEG_VIDEO_PLAY_NO_RESULT_AVAIL},
   {"gtMPEG_VIDEO_PLAY_IN_PROGRESS",gtMPEG_VIDEO_PLAY_IN_PROGRESS},
   {"gtMPEG_VIDEO_PLAY_STOPPED",gtMPEG_VIDEO_PLAY_STOPPED},
   {"gtMPEG_VIDEO_PLAY_HOLDED",gtMPEG_VIDEO_PLAY_HOLDED},
   {"gtMPEG_VIDEO_PLAY_UNRECOV_ERROR",gtMPEG_VIDEO_PLAY_UNRECOV_ERROR},

   {"gtREAL_TIME_VIDEO_TUNER",gtREAL_TIME_VIDEO_TUNER},
   {"gtREAL_TIME_VIDEO_CVBS",gtREAL_TIME_VIDEO_CVBS},
   {"gtREAL_TIME_VIDEO_TUNED",gtREAL_TIME_VIDEO_TUNED},
   {"gtREAL_TIME_VIDEO_NOT_TUNED",gtREAL_TIME_VIDEO_NOT_TUNED},

   {"gtERROR_NON_EXISTING_OBJECT",gtERROR_NON_EXISTING_OBJECT},
   {"gtERROR_WORKPAGE_NEEDED",gtERROR_WORKPAGE_NEEDED},
   {"gtERROR_INVALID_CLASS_OR_FUNCTION_ID",gtERROR_INVALID_CLASS_OR_FUNCTION_ID},
   {"gtERROR_WRONG_CLASS_ID",gtERROR_WRONG_CLASS_ID},
   {"gtERROR_INVALID_PARAMETER_LIST",gtERROR_INVALID_PARAMETER_LIST},
   {"gtERROR_FUNCTION_NOT_ALLOWED",gtERROR_FUNCTION_NOT_ALLOWED},
   {"gtERROR_PUT_FAILED",gtERROR_PUT_FAILED},
   {"gtERROR_VIDEO_FAILED",gtERROR_VIDEO_FAILED},

   {"gtERROR_MONITOR_DOWN",gtERROR_MONITOR_DOWN},
   {"gtERROR_MONITOR_UP",gtERROR_MONITOR_UP},
   {"gtERROR_MONITOR_BRIGHTNESS",gtERROR_MONITOR_BRIGHTNESS},
   {"gtERROR_MONITOR_CONTRAST",gtERROR_MONITOR_CONTRAST},
   {"gtERROR_MONITOR_WHITEPOINT",gtERROR_MONITOR_WHITEPOINT},
   {"gtERROR_MONITOR_STANDBY",gtERROR_MONITOR_STANDBY},
   {"gtERROR_MONITOR_DEGAUSS",gtERROR_MONITOR_DEGAUSS},
   {"gtERROR_MONITOR_STATUS_REQUEST",gtERROR_MONITOR_STATUS_REQUEST},
   {"gtERROR_MONITOR_SERVICE_MESSAGE",gtERROR_MONITOR_SERVICE_MESSAGE},

   {"gtERROR_OUT_OF_MEMORY",gtERROR_OUT_OF_MEMORY},
   {"gtERROR_REPLY_FAILED",gtERROR_REPLY_FAILED},

   {"gtEnde",0}
};
/******************************************************************************
*     Function Prototypes                                                     *
******************************************************************************/

extern void snap(char *pcpBuf, int ipLen, FILE *outp);
static int newsnap(int ipLevel,char *pcpBuf, int ipLen, FILE *outp);
int gtCheckMessage(char *pcpAckMsg, int ipBufSiz, int ipSocket,
                   char *pcpResBuf);





/******************************************************************************
*                                                                             *
*     Function:     BuildCommand                                              *
*                                                                             *
*     Purpose:      Build a GT2 Command Buffer and send it to the Controller, *
*                   if Parameter ipInd = 'E' or '1', or buffer is full        *
*                                                                             *
*     Parameters:   pcpInd        - 'S' = Start Build Command Buffer          *
*                                 - 'C' = Continue Build Command Buffer       *
*                                 - 'E' = End Build Command Buffer and Send   *
*                                         Data to Controller                  *
*                                 - '1' = Build Command Buffer for a single   *
*                                         Command and Send Data to Controller * 
*                                                                             *
*                   pcpCommand    - Command String                            *
*                                                                             *
*                   pcpObjectId   - Object Id String                          *
*                                                                             *
*                   ipNoParam     - Number of Parameters                      *
*                                                                             *
*                   pcpParam      - Parameter String                          *
*                                                                             *
*                   pcpFilNam     - File Name                                 *
*                                                                             *
*                   ipSocket      - Socket Number                             *
*                                                                             *
*                   pcpResBuf     - Result Buffer                             *
*                                                                             *
*     Return:       RC_SUCCESS    OK                                          *
*                   RC_NOT_FOUND  Invalid Data                                *
*                   RC_DATA_CUT   Invalid Number of Parameters                *
*                                                                             *
******************************************************************************/

int gtBuildCommand(char *pcpInd, char *pcpCommand, char *pcpObjectId,
                   int ipNoParam, char *pcpParam, char *pcpFilNam,
                   int ipSocket, char *pcpResBuf)
{
  int ilRc = RC_SUCCESS;          /* Return Code */
  long llValL, llHtonsValL;
  short slValS, slHtonsValS;
  char clValC;
  char pclPar[512];
  int ilFound, ilFound1;
  int ilCp;
  int i, j, k, l;
  char pclAckMsg[MAX_RCV_SIZE];
  int ilMaxLpCnt= 5;
  long llRemain;
  long llDataLen;
  int ilBufIdx, ilBufLen;
  long llBufSiz;
  int ilCnt;
  int ilMsgRc;
  int ilSscMode;
  char pclSscData[16];
  int ilSscDataLen;
  int ilSscCurLen;

  if(igdebug_switch == TRUE)    
    dbg(DEBUG,"Params = Ind- %s ,Cmd- %s ,Id- %s ,Param- %s, %x,NoP %d",
	pcpInd,pcpCommand,pcpObjectId,pcpParam,pcpParam[0],ipNoParam);

  if (pcgGt2Buf == NULL)                    /* allocate output buffer */
    {
      pcgGt2Buf = (char *)malloc(MAX_BUF_SIZE);
      if (pcgGt2Buf == NULL)
	{
	  if(igdebug_switch == TRUE)
	    dbg(TRACE,"pcgGt2Buf allocation failed");
	  return RC_FAIL;
	}
      else
	{
	  /*      if(igdebug_switch == TRUE) */
	  /*   dbg(DEBUG,"pcgGt2Buf allocation done"); */
	}
    }
        
  if (pcgFbuf == NULL)                      /* allocate file buffer */
    {
      pcgFbuf = (char *)malloc(MAX_FIL_SIZE);
      if (pcgFbuf == NULL)
	{
	  if(igdebug_switch == TRUE)
	    dbg(TRACE,"file buffer allocation failed");
	  return RC_FAIL;
	}
      else
	{
	  /*    dbg(DEBUG,"file buffer allocation done"); */
	}
    }

  if (pcpInd[0] == 'S' || pcpInd[0] == '1')
    {                                        /* init work data */
      llBufSiz = MAX_BUF_SIZE;
      memset(pcgGt2Buf,0,(unsigned int)llBufSiz);
      lgCurBufSiz = 0;
      pcgGt2Buf[lgCurBufSiz] = '\000';   lgCurBufSiz++;
      pcgGt2Buf[lgCurBufSiz] = '\000';   lgCurBufSiz++;
      pcgGt2Buf[lgCurBufSiz] = '\000';   lgCurBufSiz++;
      igAnswer = 0;                         /* no answer required */
      igStatReq = 0;
    }
    
  if (strcmp(pcpCommand,"gtSystemMidRequest") == 0)
    {                                        /* Mid Request */
      ilSscMode = 1;
      pcgGt2Buf[lgCurBufSiz] = '\000';   lgCurBufSiz++;
      pcgGt2Buf[lgCurBufSiz] = 80;       lgCurBufSiz++;
      slValS = atoi(pcpParam);
      slHtonsValS = htons(slValS);
      /* if(igdebug_switch == TRUE) 
	 dbg(DEBUG,"Param = <%s>%d,%d",pcpParam,slValS,slHtonsValS); */
      memcpy(&pcgGt2Buf[lgCurBufSiz],(char *)&slHtonsValS,2); /* sequence no */
      lgCurBufSiz += 2;
      ilFound = 1;
      igAnswer = 1;
    }
  else
    {                                        /* GT2 Command */
      ilSscMode = 0;
      i = 0;
      ilFound = 0;
      while (strcmp(comList[i].commandId,"gtEnde") != 0 && ilFound == 0)
	{
	  if (strcmp(comList[i].commandId,pcpCommand) == 0)
	    {
	      ilFound = 1;
	      pcgGt2Buf[lgCurBufSiz] = comList[i].gtClassId;   /* class id */
	      lgCurBufSiz++;
	      if (strncmp(pcpObjectId,"gt",2) == 0)
		{
		  j = 0;
		  ilFound1 = 0;
		  while (strcmp(constList[j].constName,"gtEnde") != 0 &&
			 ilFound1 == 0)
		    {
		      if (strcmp(constList[j].constName,pcpObjectId) == 0)
			{
			  ilFound1 = 1;
			  slValS = constList[j].constValue;
			}
		      j++;
		    }
		  if (ilFound1 == 0)
		    {
		      return RC_NOT_FOUND;
		    }
		}
	      else
		{
		  slValS = (short) atoi(pcpObjectId);
		}
	      slHtonsValS = htons(slValS);
	      memcpy(&pcgGt2Buf[lgCurBufSiz],(char *)&slHtonsValS,2); /* object */
	      lgCurBufSiz += 2;
	      slHtonsValS = htons(comList[i].gtFunctionId);
	      memcpy(&pcgGt2Buf[lgCurBufSiz],(char *)&slHtonsValS,2); /*function*/
	      lgCurBufSiz += 2;
	      if(igdebug_switch == TRUE)           
		dbg(DEBUG,"Classid = %d , Objectid = %d , Functionid = %d",comList[i].gtClassId, slValS, comList[i].gtFunctionId);
	      if (strcmp(pcpCommand,"gtFilerPutFile") == 0)
		{
		  fh = (FILE *)fopen(pcpFilNam,"r"); /* open file and read it */
		  if (fh == NULL)
		    {

		      if(igdebug_switch == TRUE)
			dbg(TRACE,"File %s not found",pcpFilNam);
		      return RC_NOT_FOUND;
		    }
		  lgFsiz = fread(pcgFbuf,(size_t)1,(size_t)MAX_FIL_SIZE,fh);
		  /*      
			  if(igdebug_switch == TRUE)
			  dbg(DEBUG,"%ld Data Read From File %s", lgFsiz, pcpFilNam); */
		  fclose(fh);
		  /*newsnap(DEBUG,pcgFbuf,lgFsiz,outp);*/
		}

	      if (strcmp(pcpCommand,"gtFilerPutFile") == 0 ||
		  strcmp(pcpCommand,"gtFilerPutData") == 0 ||
		  strcmp(pcpCommand,"gtMonitorStatusRequest") == 0 ||
		  strcmp(pcpCommand,"gtTerminalStatusRequest") == 0 ||
		  strcmp(pcpCommand,"gtFilerStatusRequest") == 0 ||
		  strcmp(pcpCommand,"gtFilerRemoveFile") == 0 ||
		  strcmp(pcpCommand,"gtFilerFindFirst") == 0 ||
		  strcmp(pcpCommand,"gtFilerFindNext") == 0 ||
		  strcmp(pcpCommand,"gtFilerCreateDirectory") == 0 ||
		  strcmp(pcpCommand,"gtFilerRemoveDirectory") == 0 ||
		  strcmp(pcpCommand,"gtTerminalGetIntegrity") == 0)
		{
                  igAnswer++;   /* answer required */
		}

	      if (strcmp(pcpCommand,"gtTerminalStatusRequest") == 0)
		{
		  igStatReq++;   /* status request required */
		}

	      if (strcmp(pcpCommand,"gtTextPut") == 0 ||
		  strcmp(pcpCommand,"gtTextPutFormatted") == 0)
		{
		  if (ipNoParam == 0)
		    {                                           
		      ipNoParam = 1;
		    }
		}

	      if (ipNoParam != comList[i].noParam)
		{
		  return RC_DATA_CUT;
		}

	      k = 0;
	      for (ilCp=0; ilCp<ipNoParam; ilCp++)
		{
		  if (ipNoParam == 1)
		    {
		      if (strcmp(pcpCommand,"gtTextPut") == 0 ||
			  strcmp(pcpCommand,"gtTextPutFormatted") == 0)
			{   
			  if (pcpParam[0] == 0)
			    {
			      strcpy(pclPar," ");
			    }
			  else
			    {
			      strcpy(pclPar,pcpParam);
			    }
			}
		      else
			{
			  strcpy(pclPar,pcpParam);
			}
		    }
		  else
		    {
		      l = 0;
		      while (pcpParam[k] != ',' && k < (int) strlen(pcpParam))
			{
			  pclPar[l] = pcpParam[k];
			  l++;   k++;
			}
		      pclPar[l] = '\000';
		    }
		  k++;
		  switch(comList[i].param[ilCp])
		    {
		    case -1 :
		      strcpy(&pcgGt2Buf[lgCurBufSiz],pclPar);
		      lgCurBufSiz += strlen(pclPar);
		      pcgGt2Buf[lgCurBufSiz] = '\000';   lgCurBufSiz++;
		      break;
		    case -2 :
		      llHtonsValL = htonl((long)lgFsiz);
		      memcpy(&pcgGt2Buf[lgCurBufSiz],(char *)&llHtonsValL,4);
		      lgCurBufSiz += 4;
		      break;
		    case -3 :
		      slHtonsValS = htons((short)lgFsiz);
		      memcpy(&pcgGt2Buf[lgCurBufSiz],(char *)&slHtonsValS,2);
		      lgCurBufSiz += 2;
		      break;
		    case -4 :
		      memcpy(&pcgGt2Buf[lgCurBufSiz],pcgFbuf,lgFsiz);
		      lgCurBufSiz += lgFsiz;
		      break;
		    default :
		      if (strncmp(pclPar,"gt",2) == 0)
			{
			  j = 0;
			  ilFound1 = 0;
			  while (strcmp(constList[j].constName,"gtEnde") != 0 && ilFound1 == 0)
			    {
			      if (strcmp(constList[j].constName,pclPar) == 0)
				{
				  ilFound1 = 1;
				  if (comList[i].param[ilCp] == 1)
				    {
				      clValC = (char) constList[j].constValue;
				      pcgGt2Buf[lgCurBufSiz] = clValC;
				      lgCurBufSiz++;
				    }
				  else
				    {
				      if (comList[i].param[ilCp] == 2)
					{
					  slValS = constList[j].constValue;
					  slHtonsValS = htons(slValS);
					  memcpy(&pcgGt2Buf[lgCurBufSiz],(char *)&slHtonsValS,2);
					  lgCurBufSiz += 2;
					}
				      else
					{
					  llValL = constList[j].constValue;
					  llHtonsValL = htonl(llValL);
					  memcpy(&pcgGt2Buf[lgCurBufSiz],(char *)&llHtonsValL,4);
					  lgCurBufSiz += 4;
					}
				    }
				}
			      j++;
			    }
			  if (ilFound1 == 0)
			    {
			      return RC_NOT_FOUND;
			    }
			}
		      else
			{
			  if (comList[i].param[ilCp] == 1)
			    {
			      clValC = (char) atoi(pclPar);
			      pcgGt2Buf[lgCurBufSiz] = clValC;
			      lgCurBufSiz++;
			    }
			  else
			    {
			      if (comList[i].param[ilCp] == 2)
				{
				  slValS = (short) atoi(pclPar);
				  slHtonsValS = htons(slValS);
				  memcpy(&pcgGt2Buf[lgCurBufSiz],(char *)&slHtonsValS,2);
				  lgCurBufSiz += 2;
				}
			      else
				{
				  llValL = (long) atoi(pclPar);
				  llHtonsValL = htonl(llValL);
				  memcpy(&pcgGt2Buf[lgCurBufSiz],(char *)&llHtonsValL,4);
				  lgCurBufSiz += 4;
				}
			    }
			}
		    }
		}   /* end for */
	    }   /* end if */
	  i++;
	}   /* end while */
    }   /* end if MidRequest */
  if (ilFound == 0)
    {
      return RC_NOT_FOUND;
    }
  if (ilSscMode == 1)
    {
      slHtonsValS = htons((unsigned int)lgCurBufSiz);
    }
  else
    {
      slHtonsValS = htons((unsigned int)(lgCurBufSiz-2));
    }
  memcpy(&pcgGt2Buf[0],(char *)&slHtonsValS,2);   /* message length */

  if (pcpInd[0] == 'E' || pcpInd[0] == '1')
    {
      newsnap(DEBUG,pcgGt2Buf, max(min((unsigned int)lgCurBufSiz,256),16),outp); 
      llRemain = lgCurBufSiz;
      llDataLen = llRemain;
      while (llRemain > 0)
	{
	  if (llRemain > PACKET_LEN)
	    {
	      slHtonsValS = htons(PACKET_LEN-2);
	      memcpy(&pcgGt2Buf[0],(char *)&slHtonsValS,2);
	      slHtonsValS = htons(DATA_PACKET_LEN);
	      memcpy(&pcgGt2Buf[8],(char *)&slHtonsValS,2);
	      llDataLen = PACKET_LEN;
	      newsnap(DEBUG,pcgGt2Buf, max(min(llDataLen,128),16),outp);
	    }
	  if(igdebug_switch == TRUE)
	    {
	      if(llDataLen>200)
		dbg(TRACE,"Send %d Bytes to controller %d",(unsigned int)llDataLen, ipSocket);
	      else
		dbg(DEBUG,"Send %d Bytes to controller %d",(unsigned int)llDataLen, ipSocket);
	    }
	  ilRc = write(ipSocket,pcgGt2Buf,llDataLen);
	  if (ilRc != llDataLen)
	    {
	      if(igdebug_switch == TRUE)
		dbg(TRACE,"Error SendBuffer to controller %d: %d",ipSocket,ilRc);
	      return RC_FAIL;
	    }
	  /*  if(igdebug_switch == TRUE) 
	      dbg(DEBUG,"SendBuffer to controller %d successful",ipSocket); */
	  if (igAnswer > 0)
	    {
	      i = 0;
	      memset(pclAckMsg,0x00,MAX_RCV_SIZE);
	      ilRc = RC_TIMEOUT;
	      while (ilRc == RC_TIMEOUT && i < ilMaxLpCnt)
		{
		  memset(pclAckMsg,0x00,MAX_RCV_SIZE);
		  alarm(1);
		  ilRc = read(ipSocket,pclAckMsg,MAX_RCV_SIZE);
		  alarm(0);
		  if (ilRc > 0)
		    {/*If*/
		      if(igdebug_switch == TRUE)
			dbg(TRACE,"%d Bytes received (%d)",ilRc,i); 
		      newsnap(DEBUG,pclAckMsg,max(ilRc,16),outp);
		      if (strcmp(pcpCommand,"gtSystemMidRequest") != 0)
			{
			  if(igdebug_switch == TRUE)
			    dbg(DEBUG,"Line %d gtCheckMessage strlen(pcpResBuf)= %d",__LINE__,strlen(pcpResBuf));
			  ilMsgRc = gtCheckMessage(pclAckMsg,ilRc,ipSocket,pcpResBuf);   /* Interpret received message */
			}else{
			  pcpResBuf[0] = '\000';
			  if (pclAckMsg[4] == '\001')
			    {     /* send Session open accept */
			      pclAckMsg[4] = '\002';
			      llDataLen = ilRc;
			      if(igdebug_switch == TRUE)
				dbg(DEBUG,"Send respose to ssc");
			      newsnap(DEBUG,pclAckMsg,max(16,(int)llDataLen),outp);
			      ilRc = write(ipSocket,pclAckMsg,llDataLen);
			      if (ilRc != llDataLen) 
				{
				  if(igdebug_switch == TRUE)
				    dbg(DEBUG,"Error Send Respose to SSC: %d",ilRc);
				  ilRc = RC_FAIL;
				}
			    }else{
			      ilSscDataLen = ilRc;
			      ilSscCurLen = 0;
			      while (ilSscCurLen < ilSscDataLen)
				{
				  if (pclAckMsg[ilSscCurLen+4] == 0x32)
				    {
				      if (strlen(pcpResBuf) > 0)
					{
					  sprintf(pclSscData,",%d,%d",
						  pclAckMsg[ilSscCurLen+6],
						  pclAckMsg[ilSscCurLen+7]);
					}else{
					  sprintf(pclSscData,"%d,%d",
						  pclAckMsg[ilSscCurLen+6],
						  pclAckMsg[ilSscCurLen+7]);
					}
                      
				      if(igdebug_switch == TRUE)
					dbg(DEBUG,"Data received: %s",pclSscData);
				      strcat(pcpResBuf,pclSscData);
                      
				      if(igdebug_switch == TRUE)
					dbg(DEBUG,"Data received so far: %s",pcpResBuf);
				    }
				  ilSscCurLen += pclAckMsg[ilSscCurLen+1];
				}
			    }
			}

		      if (igStatReq > 0)
			{
			  ilBufLen = ilRc;
			  ilRc = RC_TIMEOUT;
			  for (ilBufIdx=0; ilBufIdx<ilBufLen-4; ilBufIdx++)
			    {
			      if (pclAckMsg[ilBufIdx] == gtREPLY &&
				  pclAckMsg[ilBufIdx+4] == gtREPLY_TERMINAL_STATUS_REQUEST)
				{
				  ilRc = RC_SUCCESS;
				}
			    }
			}else{
			  ilRc = RC_SUCCESS;
			}
		    }else{
		      ilRc = RC_TIMEOUT;
		      i++;
		      sleep(1);
		    }
		}
	      if (ilRc != RC_SUCCESS)
		{
          
		  if(igdebug_switch == TRUE)
		    dbg(TRACE,"...but no answer after %d seconds",ilMaxLpCnt);
		  return RC_TIMEOUT;
		}
	      ilRc = RC_SUCCESS;    
	    }

	  if (llRemain > PACKET_LEN)
	    {
	      llRemain -= DATA_PACKET_LEN;
	      memcpy(&pcgGt2Buf[10],&pcgGt2Buf[PACKET_LEN],llRemain);
	      slHtonsValS = htons((unsigned int)(llRemain-2));
	      memcpy(&pcgGt2Buf[0],(char *)&slHtonsValS,2);
	      slHtonsValS = htons((unsigned int)(llRemain-10));
	      memcpy(&pcgGt2Buf[8],(char *)&slHtonsValS,2);
	      llDataLen = llRemain;
	      if(igdebug_switch == TRUE)
		newsnap(DEBUG,pcgGt2Buf, max(min(llDataLen,128),16),outp);
	    }
	  else
	    {
	      llRemain = 0;
	    }
	  /*  
	      if(igdebug_switch == TRUE)
	      dbg(DEBUG,"Remaining bytes %ld",llRemain); */
	}   /* end while */
      igAnswer = 0;
      igStatReq = 0;
    }

  return ilRc;
}

/******************************************************************************
*                                                                             *
*     Function:    gtCheckMessage                                             *
*                                                                             *
*     Purpose:     Interpret Received Message from Controller                 *
*                                                                             *
*     Parameter:   pcpAckMsg:   Buffer Address                                *
*                  ipBufSiz:    Buffer Size                                   *
*                  ipSocket:    Socket Number                                 *
*                  pcpResBuf:   Result Buffer                                 *
*                                                                             *
*     Return:      RC_SUCCESS   OK                                            *
*                  RC_FAIL      Internal Error                                *
*                                                                             *
******************************************************************************/

int gtCheckMessage(char *pcpAckMsg, int ipBufSiz, int ipSocket,
                          char *pcpResBuf)
{

int   ilRc;
short slBufSiz;
int   ilBufIdx ;
short slFunctId;
short slObjId;
short slShort1, slShort2, slShort3, slShort4, slShort5;
long  llLong1, llLong2;
long  llYear, llMonth, llDate, llHour, llMinute, llSecond;
unsigned char  clChar1;

   ilBufIdx = 0; 
   memcpy(&slBufSiz,&pcpAckMsg[ilBufIdx],2);
   slBufSiz = ntohs(slBufSiz);
   ilBufIdx += 2;
   clChar1 = pcpAckMsg[ilBufIdx];
   ilBufIdx++;
   ilBufIdx += clChar1;

   if(igdebug_switch == TRUE)
     dbg(DEBUG,"Answer from Controller: %d",ipSocket);
   sprintf(&pcpResBuf[strlen(pcpResBuf)],"Answer from Controller: %d\n",
           ipSocket);
   while (ilBufIdx-2 < slBufSiz)
   {

      clChar1 = pcpAckMsg[ilBufIdx];
      if (clChar1 != '\000')
      {                             /* New Class-Id Found */
         if (clChar1 == gtREPLY)
         {
            ilBufIdx++;
            memcpy(&slObjId,&pcpAckMsg[ilBufIdx], 2);
            slObjId = ntohs(slObjId);
            ilBufIdx += 2;
        
        if(igdebug_switch == TRUE)
          dbg(DEBUG,"   Class-Id: gtREPLY , Object-Id: %d",slObjId);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"   Class-Id: gtREPLY , Object-Id: %d\n",slObjId);
         }
         else
         {
       
       if(igdebug_switch == TRUE)
         dbg(DEBUG,"   Unknown Class-Id: %d",(short)clChar1);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"   Unknown Class-Id: %d\n",(short)clChar1);
            return RC_FAIL;
         }
      }
      memcpy(&slFunctId, &pcpAckMsg[ilBufIdx], 2);
      slFunctId = ntohs(slFunctId);
      ilBufIdx += 2;
      switch (slFunctId)
      {
         case gtREPLY_LOG:
       
       if(igdebug_switch == TRUE)
         dbg(DEBUG,"   Function-Id: gtREPLY_LOG:");
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"      Function-Id: gtREPLY_LOG:\n");
        
        if(igdebug_switch == TRUE)           
          dbg(DEBUG,"  Log Message:   %s",&pcpAckMsg[ilBufIdx]);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Log Message:   %s\n",&pcpAckMsg[ilBufIdx]);
            ilBufIdx += strlen(&pcpAckMsg[ilBufIdx]);
            ilBufIdx++;
            break;

         case gtREPLY_ERROR:
            memcpy(&slShort1, &pcpAckMsg[ilBufIdx], 2);
            slShort1 = ntohs(slShort1);
            ilBufIdx += 2;
            memcpy(&slShort2, &pcpAckMsg[ilBufIdx], 2);
            slShort2 = ntohs(slShort2);
            ilBufIdx += 2;
            memcpy(&slShort3, &pcpAckMsg[ilBufIdx], 2);
            slShort3 = ntohs(slShort3);
            ilBufIdx += 2;
            memcpy(&slShort4, &pcpAckMsg[ilBufIdx], 2);
            slShort4 = ntohs(slShort4);
            ilBufIdx += 2;
        
        if(igdebug_switch == TRUE)
          dbg(DEBUG,"   Function-Id: gtREPLY_ERROR:");
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"      Function-Id: gtREPLY_ERROR:\n");
        if(igdebug_switch == TRUE)           
          dbg(DEBUG,"         Error Code:    %d",slShort1);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Error Code:    %d\n",slShort1);
            
        if(igdebug_switch == TRUE)
          dbg(DEBUG,"         Class Id:      %d",slShort2);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Class Id:      %d\n",slShort2);
            
        if(igdebug_switch == TRUE)
          dbg(DEBUG,"         Object Id:     %d",slShort3);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Object Id:     %d\n",slShort3);
        
        if(igdebug_switch == TRUE)           
          dbg(DEBUG,"         Function Id:   %d",slShort4);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Function Id:   %d\n",slShort4);
            break;

         case gtREPLY_MPEG_VIDEO_STATUS_REQUEST:
            memcpy(&slShort1, &pcpAckMsg[ilBufIdx], 2);
            slShort1 = ntohs(slShort1);
            ilBufIdx += 2;
        
        if(igdebug_switch == TRUE)
          dbg(DEBUG,"      Function-Id: gtREPLY_MPEG_VIDEO_STATUS_REQUEST:");
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"      Function-Id: gtREPLY_MPEG_VIDEO_STATUS_REQUEST:\n");
        
        if(igdebug_switch == TRUE)            
          dbg(DEBUG,"         Status Code:   %d",slShort1);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Status Code:   %d\n",slShort1);
            break;

         case gtREPLY_REAL_TIME_VIDEO_STATUS_REQUEST:
       
       if(igdebug_switch == TRUE)
         dbg(DEBUG,"      Function-Id: gtREPLY_REAL_TIME_VIDEO_STATUS_REQUEST:");
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"      Function-Id: gtREPLY_REAL_TIME_VIDEO_STATUS_REQUEST:\n");
            break;

         case gtREPLY_MONITOR_STATUS_REQUEST:
            memcpy(&slShort1, &pcpAckMsg[ilBufIdx], 2);
            slShort1 = ntohs(slShort1);
            ilBufIdx += 2;
            memcpy(&slShort2, &pcpAckMsg[ilBufIdx], 2);
            slShort2 = ntohs(slShort2);
            ilBufIdx += 2;
            memcpy(&slShort3, &pcpAckMsg[ilBufIdx], 2);
            slShort3 = ntohs(slShort3);
            ilBufIdx += 2;
            memcpy(&slShort4, &pcpAckMsg[ilBufIdx], 2);
            slShort4 = ntohs(slShort4);
            ilBufIdx += 2;
        
        if(igdebug_switch == TRUE)
          dbg(DEBUG,"      Function-Id: gtREPLY_MONITOR_STATUS_REQUEST:");
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"      Function-Id: gtREPLY_MONITOR_STATUS_REQUEST:\n");
        
        if(igdebug_switch == TRUE)
          dbg(DEBUG,"         Brightness:    %d",slShort1);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Brightness:    %d\n",slShort1);
        
        if(igdebug_switch == TRUE)           
          dbg(DEBUG,"         Contrast:      %d",slShort2);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Contrast:      %d\n",slShort2);
        
        if(igdebug_switch == TRUE)           
          dbg(DEBUG,"         Status:        %.2x",slShort3);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Status:        %.2x\n",slShort3);
            if ((slShort3 & 0x0001) != 0)
            {
          if(igdebug_switch == TRUE)
        
               dbg(DEBUG,"         Standby Mode:  On");
               sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Standby Mode:  On\n");
            }
            else
            {
          
          if(igdebug_switch == TRUE)
        dbg(DEBUG,"         Standby Mode:  Off");
               sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Standby Mode:  Off\n");
            }
            if ((slShort3 & 0x0002) != 0)
            { 
          
          if(igdebug_switch == TRUE)
        dbg(DEBUG,"         Degauss:       Possible");
               sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Degauss:       Possible\n");
            }
            else
            { 
          
          if(igdebug_switch == TRUE)
               dbg(DEBUG,"         Degauss:       Not Possible");
               sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Degauss:       Not Possible\n");
            }
            if ((slShort3 & 0x0004) != 0)
            {
          
          if(igdebug_switch == TRUE)
               dbg(DEBUG,"         Service Mode:  On");
               sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Service Mode:  On\n");
            }
            else
            {
          
          if(igdebug_switch == TRUE)
        dbg(DEBUG,"         Service Mode:  Off");
               sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Service Mode:  Off\n");
            }
            if ((slShort3 & 0x0008) != 0)
            {
          
          if(igdebug_switch == TRUE)
        dbg(DEBUG,"         Geometry Mode: VGA65");
               sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Geometry Mode: VGA65\n");
            }
            else
            {
          
          if(igdebug_switch == TRUE)
        dbg(DEBUG,"         Geometry Mode: VGA100");
               sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Geometry Mode: VGA100\n");
            }
            if ((slShort3 & 0x0010) != 0)
            {
          
          if(igdebug_switch == TRUE)
        dbg(DEBUG,"         White Point:   D65");
               sprintf(&pcpResBuf[strlen(pcpResBuf)],"         White Point:   D65\n");
            }
            else
            {
          
          if(igdebug_switch == TRUE)
               dbg(DEBUG,"         White Point:   D76");
               sprintf(&pcpResBuf[strlen(pcpResBuf)],"         White Point:   D76\n");
            }
        
        if(igdebug_switch == TRUE)
         
          dbg(DEBUG,"         No of Errors:  %d",slShort4);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         No of Errors:  %d\n",slShort4);
            break;

         case gtREPLY_MONITOR_SERVICE_MESSAGE:
            memcpy(&slShort1, &pcpAckMsg[ilBufIdx], 2);
            slShort1 = ntohs(slShort1);
            ilBufIdx += 2;
        
        if(igdebug_switch == TRUE)
          dbg(DEBUG,"      Function-Id: gtREPLY_MONITOR_SERVICE_MESSAGE:");
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"      Function-Id: gtREPLY_MONITOR_SERVICE_MESSAGE:\n");
        
        if(igdebug_switch == TRUE)
          dbg(DEBUG,"         Service Message:   %s",&pcpAckMsg[ilBufIdx]);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Service Message:   %s\n",&pcpAckMsg[ilBufIdx]);
            ilBufIdx += slShort1;
            break;

         case gtREPLY_TERMINAL_STATUS_REQUEST:
            memcpy(&slShort1, &pcpAckMsg[ilBufIdx], 2);
            slShort1 = ntohs(slShort1);
            ilBufIdx += 2;
            memcpy(&slShort2, &pcpAckMsg[ilBufIdx], 2);
            slShort2 = ntohs(slShort2);
            ilBufIdx += 2;
            memcpy(&slShort3, &pcpAckMsg[ilBufIdx], 2);
            slShort3 = ntohs(slShort3);
            ilBufIdx += 2;
            memcpy(&slShort4, &pcpAckMsg[ilBufIdx], 2);
            slShort4 = ntohs(slShort4);
            ilBufIdx += 2;
            memcpy(&slShort5, &pcpAckMsg[ilBufIdx], 2);
            slShort5 = ntohs(slShort5);
            ilBufIdx += 6;
        
        if(igdebug_switch == TRUE)
          dbg(DEBUG,"      Function-Id: gtREPLY_TERMINAL_STATUS_REQUEST:");
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"      Function-Id: gtREPLY_TERMINAL_STATUS_REQUEST:\n");
        
        if(igdebug_switch == TRUE)           
          dbg(DEBUG,"         Version:       %d-%d-%d",slShort1,slShort2,slShort3);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Version:       %d-%d-%d\n",slShort1,slShort2,slShort3);
        
        if(igdebug_switch == TRUE)           
          dbg(DEBUG,"         Board Version: %d-%d",slShort4,slShort5);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Board Version: %d-%d\n",slShort4,slShort5);
            break;

         case gtREPLY_TERMINAL_INTEGRITY:
            memcpy(&llLong1, &pcpAckMsg[ilBufIdx], 4);
            llLong1 = ntohl(llLong1);
            ilBufIdx += 4;
        
        if(igdebug_switch == TRUE)
          dbg(DEBUG,"      Function-Id: gtREPLY_TERMINAL_INTEGRITY:");
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"      Function-Id: gtREPLY_TERMINAL_INTEGRITY:\n");
        
            if(igdebug_switch == TRUE)
          dbg(DEBUG,"         Integrity:     %ld",llLong1);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Integrity:     %ld\n",llLong1);
            break;

         case gtREPLY_FILER_OPERATION:
            memcpy(&slShort1, &pcpAckMsg[ilBufIdx], 2);
            slShort1 = ntohs(slShort1);
            ilBufIdx += 2;
            memcpy(&slShort2, &pcpAckMsg[ilBufIdx], 2);
            slShort2 = ntohs(slShort2);
            ilBufIdx += 2;
        
        if(igdebug_switch == TRUE)
          dbg(DEBUG,"      Function-Id: gtREPLY_FILER_OPERATION:"); 
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"      Function-Id: gtREPLY_FILER_OPERATION:\n");
        
        if(igdebug_switch == TRUE)
          dbg(DEBUG,"         Result:        %d",slShort1); 
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Result:        %d\n",slShort1);
        
        if(igdebug_switch == TRUE)
          dbg(DEBUG,"         DOS Error:     %d",slShort2); 
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         DOS Error:     %d\n",slShort2);
            break;

         case gtREPLY_FILER_INFO:
            memcpy(&slShort1, &pcpAckMsg[ilBufIdx], 2);
            slShort1 = ntohs(slShort1);
            ilBufIdx += 2;
            memcpy(&llLong1, &pcpAckMsg[ilBufIdx], 4);
            llLong1 = ntohl(llLong1);
            ilBufIdx += 4;
            memcpy(&llLong2, &pcpAckMsg[ilBufIdx], 4);
            llLong2 = ntohl(llLong2);
            ilBufIdx += 4;
            if (llLong2 > 0)
            {
               llYear = llLong1 & 0xfe000000;
               llYear >>= 25;
               llYear += 1980;
               llMonth = llLong1 & 0x01e00000;
               llMonth >>= 21;
               llDate = llLong1 & 0x001f0000;
               llDate >>= 16;
               llHour = llLong1 & 0x0000f800;
               llHour >>= 11;
               llMinute = llLong1 & 0x000007e0;
               llMinute >>= 5;
               llSecond = llLong1 & 0x0000001f;
               llSecond *= 2;
            }
        
        if(igdebug_switch == TRUE)
          dbg(DEBUG,"      Function-Id: gtREPLY_FILER_INFO:");
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"      Function-Id: gtREPLY_FILER_INFO:\n");
        
        if(igdebug_switch == TRUE)           
          dbg(DEBUG,"         Directory:     %d",slShort1);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Directory:     %d\n",slShort1);
            if (llLong2 > 0)
            {
          
          if(igdebug_switch == TRUE)
               dbg(DEBUG,"         File Time:     %2ld.%2ld.%4ld %2ld:%2ld:%2ld",
                   llDate,llMonth,llYear,llHour,llMinute,llSecond);
               sprintf(&pcpResBuf[strlen(pcpResBuf)],"         File Time:     %2ld.%2ld.%4ld %2ld:%2ld:%2ld\n",
                       llDate,llMonth,llYear,llHour,llMinute,llSecond);
            }
            else
            {
          
          if(igdebug_switch == TRUE)
        dbg(DEBUG,"         File Time:     -");
               sprintf(&pcpResBuf[strlen(pcpResBuf)],"         File Time:     -\n");
            }
        
        if(igdebug_switch == TRUE)
          dbg(DEBUG,"         File Size:     %ld",llLong2);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         File Size:     %ld\n",llLong2);
            if (llLong2 > 0)
            {
          
          if(igdebug_switch == TRUE)
        dbg(DEBUG,"         File Name:     %s",&pcpAckMsg[ilBufIdx]);
               sprintf(&pcpResBuf[strlen(pcpResBuf)],"         File Name:     %s\n",&pcpAckMsg[ilBufIdx]);
               ilBufIdx += strlen(&pcpAckMsg[ilBufIdx]);
            }
            ilBufIdx++;
            break;

         case gtREPLY_FILER_STATUS_REQUEST:
            memcpy(&slShort1, &pcpAckMsg[ilBufIdx], 2);
            slShort1 = ntohs(slShort1);
            ilBufIdx += 2;
            memcpy(&llLong1, &pcpAckMsg[ilBufIdx], 4);
            llLong1 = ntohl(llLong1);
            ilBufIdx += 4;
            memcpy(&llLong2, &pcpAckMsg[ilBufIdx], 4);
            llLong2 = ntohl(llLong2);
            ilBufIdx += 4;
        
        if(igdebug_switch == TRUE)
          dbg(TRACE,"      Function-Id: gtREPLY_FILER_STATUS_REQUEST:");
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"      Function-Id: gtREPLY_FILER_STATUS_REQUEST:\n");
        
         if(igdebug_switch == TRUE)
           dbg(TRACE,"         Device:        %d",slShort1);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Device:        %d\n",slShort1);
        
            if(igdebug_switch == TRUE)
          dbg(TRACE,"         Total Bytes:   %d",llLong1);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Total Bytes:   %ld\n",llLong1);
        
            if(igdebug_switch == TRUE)
          dbg(TRACE,"         Free Bytes:    %d",llLong2);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Free Bytes:    %ld\n",llLong2);
            break;

         case gtREPLY_PAGE:
            memcpy(&slShort1, &pcpAckMsg[ilBufIdx], 2);
            slShort1 = ntohs(slShort1);
            ilBufIdx += 2;
        if(igdebug_switch == TRUE)
          dbg(DEBUG,"      Function-Id: gtREPLY_PAGE:");
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"      Function-Id: gtREPLY_PAGE:\n");
        if(igdebug_switch == TRUE)
          dbg(DEBUG,"         Page:          %d",slShort1);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Page:          %d\n",slShort1);
            break;

         case gtREPLY_KEY:
            memcpy(&slShort1, &pcpAckMsg[ilBufIdx], 2);
            slShort1 = ntohs(slShort1);
            ilBufIdx += 2;
        
        if(igdebug_switch == TRUE)
          dbg(DEBUG,"      Function-Id: gtREPLY_KEY:");
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"      Function-Id: gtREPLY_KEY:\n");
        
        if(igdebug_switch == TRUE)
          dbg(DEBUG,"         Key:           %d",slShort1);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"         Key:           %d\n",slShort1);
            break;

         default:
       
       if(igdebug_switch == TRUE)
         dbg(DEBUG,"      Unknown Function-Id: %d",slFunctId);
            sprintf(&pcpResBuf[strlen(pcpResBuf)],"      Unknown Function-Id: %d\n",slFunctId);
            return RC_FAIL;
      }
   }

   if(ilBufIdx < ipBufSiz)
   {
     if(igdebug_switch == TRUE)
      dbg(DEBUG,"Interpret next part");
      ilRc = gtCheckMessage(&pcpAckMsg[ilBufIdx],ipBufSiz-ilBufIdx,
                          ipSocket,pcpResBuf);   /* Interpret Next Part */
   }

   return RC_SUCCESS;
}


static int newsnap(int ipLevel,char *pcpBuf, int ipLen, FILE *outp)

{

  int ilRc = 0;


  if(ipLevel == debug_level)
    {
      if(igdebug_switch == TRUE)
    dbg(DEBUG,"LINE %d",__LINE__);
      if(igdebug_switch == TRUE)
    snap(pcpBuf, ipLen,outp);
    }


  return ilRc;
}
/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/
int CheckSocket(int ipSocket, int ipSet, long lpSec, long lpMsec)
{
    int ilRc_Select = RC_FAIL;
    int ilRc = 0;
    fd_set    rset;
    fd_set    wset;
    fd_set    eset;
    struct timeval rlTimeToWait;

    FD_ZERO(&rset);
    FD_ZERO(&wset);
    FD_ZERO(&eset);

    FD_SET(ipSocket,&rset);
    FD_SET(ipSocket,&wset);
    FD_SET(ipSocket,&eset);

    rlTimeToWait.tv_sec = lpSec;
    rlTimeToWait.tv_usec = lpMsec;

    errno = 0;
    switch(ipSet)
    {
        case READSET:
            ilRc_Select = select(ipSocket+1, &rset, NULL, &eset, &rlTimeToWait); 
            break;
        case WRITESET:
            ilRc_Select = select(ipSocket+1, NULL, &wset, &eset, &rlTimeToWait); 
            break;
        default:
            dbg(TRACE,"CheckSocket: Unknown Descr.-Set specified!");
            break;
    }

    if (ilRc_Select > 0)
    {
        if (FD_ISSET(ipSocket,&eset))
        {
            dbg(TRACE,"CheckSocket: Exception for socket<%d> pending! Return RC_FAIL!",ipSocket);
            ilRc = RC_FAIL;
        }
        else
        {
            switch (ipSet)
            {
            case READSET:
                if (FD_ISSET(ipSocket,&rset))
                {
                    /*dbg(DEBUG,"CheckSocket: read-set-SELECT returns ready for socket<%d>",ipSocket); */
                    ilRc = RC_SUCCESS;
                }
                else
                {
                    dbg(TRACE,"CheckSocket: socket <%d> not ready for READ!",ipSocket);
                    ilRc = -100;
                }
            break;
            case WRITESET:
                if (FD_ISSET(ipSocket,&wset))
                {
                    /*dbg(DEBUG,"CheckSocket: write-set-SELECT returns ready for socket<%d>",ipSocket); */
                    ilRc =RC_SUCCESS;
                }
                else
                {
                    dbg(TRACE,"CheckSocket: socket <%d> not ready for WRITE!",ipSocket);
                    ilRc = -10;
                }
            }
        }
    }
    else
    {
        if(errno != 0)
        {
            dbg(TRACE,"CheckSocket: SELECT for socket<%d> failed! Errno=<%d> => <%s>"
            ,ipSocket,errno,strerror(errno));
            ilRc = RC_FAIL;
        }else{
            ilRc = RC_FAIL;
        }
    }
    if (ilRc != RC_SUCCESS)
        dbg(DEBUG,"CheckSocket: SELECT for socket<%d> returns <%d>!",ipSocket,ilRc);
    return ilRc;
}
/******************************************************************************
*                                                                             *
*     Function:     MyGtBuildCommand                                          *
*                                                                             *
*     Purpose:      Build a GT2 Command Buffer and send it to the Controller, *
*                   if Parameter ipInd = 'E' or '1', or buffer is full        *
*                                                                             *
*     Parameters:   pcpInd        - 'S' = Start Build Command Buffer          *
*                                 - 'C' = Continue Build Command Buffer       *
*                                 - 'E' = End Build Command Buffer and Send   *
*                                         Data to Controller                  *
*                                 - '1' = Build Command Buffer for a single   *
*                                         Command and Send Data to Controller * 
*                                                                             *
*                   pcpCommand    - Command String                            *
*                                                                             *
*                   pcpObjectId   - Object Id String                          *
*                                                                             *
*                   ipNoParam     - Number of Parameters                      *
*                                                                             *
*                   pcpParam      - Parameter String                          *
*                                                                             *
*                   pcpFilNam     - File Name                                 *
*                                                                             *
*                   ipSocket      - Socket Number                             *
*                                                                             *
*                   pcpResBuf     - Result Buffer                             *
*                                                                             *
*     Return:       RC_SUCCESS    OK                                          *
*                   RC_NOT_FOUND  Invalid Data                                *
*                   RC_DATA_CUT   Invalid Number of Parameters                *
*                                                                             *
******************************************************************************/

int gtMyBuildCommand(char *pcpInd, char *pcpCommand, char *pcpObjectId,
                   int ipNoParam, char *pcpParam, char *pcpFilNam,
                   int ipSocket, char *pcpResBuf)
{
  int ilRc = RC_SUCCESS;          /* Return Code */
  int ilCheck = RC_SUCCESS;
  long llValL, llHtonsValL;
  short slValS, slHtonsValS;
  char clValC;
  char pclPar[512];
  int ilFound, ilFound1;
  int ilCp;
  int i, j, k, l;
  char pclAckMsg[MAX_RCV_SIZE];
  int ilMaxLpCnt= 3;
  long llRemain;
  long llDataLen;
  int ilBufIdx, ilBufLen;
  long llBufSiz;
  int ilCnt;
  int ilMsgRc;
  int ilSscMode;
  char pclSscData[16];
  int ilSscDataLen;
  int ilSscCurLen;

  if(igdebug_switch == TRUE)    
    dbg(DEBUG,"Params = Ind- %s ,Cmd- %s ,Id- %s ,Param- %s, %x,NoP %d",
    pcpInd,pcpCommand,pcpObjectId,pcpParam,pcpParam[0],ipNoParam);

  if (pcgGt2Buf == NULL)                    /* allocate output buffer */
    {
      pcgGt2Buf = (char *)malloc(MAX_BUF_SIZE);
      if (pcgGt2Buf == NULL)
    {
      if(igdebug_switch == TRUE)
        dbg(TRACE,"pcgGt2Buf allocation failed");
      return RC_FAIL;
    }
      else
    {
      /*      if(igdebug_switch == TRUE) */
      /*   dbg(DEBUG,"pcgGt2Buf allocation done"); */
    }
    }
        
  if (pcgFbuf == NULL)                      /* allocate file buffer */
    {
      pcgFbuf = (char *)malloc(MAX_FIL_SIZE);
      if (pcgFbuf == NULL)
    {
      if(igdebug_switch == TRUE)
        dbg(TRACE,"file buffer allocation failed");
      return RC_FAIL;
    }
      else
    {
      /*    dbg(DEBUG,"file buffer allocation done"); */
    }
    }

  if (pcpInd[0] == 'S' || pcpInd[0] == '1')
    {                                        /* init work data */
      llBufSiz = MAX_BUF_SIZE;
      memset(pcgGt2Buf,0,(unsigned int)llBufSiz);
      lgCurBufSiz = 0;
      pcgGt2Buf[lgCurBufSiz] = '\000';   lgCurBufSiz++;
      pcgGt2Buf[lgCurBufSiz] = '\000';   lgCurBufSiz++;
      pcgGt2Buf[lgCurBufSiz] = '\000';   lgCurBufSiz++;
      igAnswer = 0;                         /* no answer required */
      igStatReq = 0;
    }
    
  if (strcmp(pcpCommand,"gtSystemMidRequest") == 0)
    {                                        /* Mid Request */
      ilSscMode = 1;
      pcgGt2Buf[lgCurBufSiz] = '\000';   lgCurBufSiz++;
      pcgGt2Buf[lgCurBufSiz] = 80;       lgCurBufSiz++;
      slValS = atoi(pcpParam);
      slHtonsValS = htons(slValS);
      /* if(igdebug_switch == TRUE) 
     dbg(DEBUG,"Param = <%s>%d,%d",pcpParam,slValS,slHtonsValS); */
      memcpy(&pcgGt2Buf[lgCurBufSiz],(char *)&slHtonsValS,2); /* sequence no */
      lgCurBufSiz += 2;
      ilFound = 1;
      igAnswer = 1;
    }
  else
    {                                        /* GT2 Command */
      ilSscMode = 0;
      i = 0;
      ilFound = 0;
      while (strcmp(comList[i].commandId,"gtEnde") != 0 && ilFound == 0)
    {
      if (strcmp(comList[i].commandId,pcpCommand) == 0)
        {
          ilFound = 1;
          pcgGt2Buf[lgCurBufSiz] = comList[i].gtClassId;   /* class id */
          lgCurBufSiz++;
          if (strncmp(pcpObjectId,"gt",2) == 0)
        {
          j = 0;
          ilFound1 = 0;
          while (strcmp(constList[j].constName,"gtEnde") != 0 &&
             ilFound1 == 0)
            {
              if (strcmp(constList[j].constName,pcpObjectId) == 0)
            {
              ilFound1 = 1;
              slValS = constList[j].constValue;
            }
              j++;
            }
          if (ilFound1 == 0)
            {
              return RC_NOT_FOUND;
            }
        }
          else
        {
          slValS = (short) atoi(pcpObjectId);
        }
          slHtonsValS = htons(slValS);
          memcpy(&pcgGt2Buf[lgCurBufSiz],(char *)&slHtonsValS,2); /* object */
          lgCurBufSiz += 2;
          slHtonsValS = htons(comList[i].gtFunctionId);
          memcpy(&pcgGt2Buf[lgCurBufSiz],(char *)&slHtonsValS,2); /*function*/
          lgCurBufSiz += 2;
          if(igdebug_switch == TRUE)           
        dbg(DEBUG,"Classid = %d , Objectid = %d , Functionid = %d",
            comList[i].gtClassId, slValS, comList[i].gtFunctionId);
          if (strcmp(pcpCommand,"gtFilerPutFile") == 0)
        {
          fh = (FILE *)fopen(pcpFilNam,"r"); /* open file and read it */
          if (fh == NULL)
            {

              if(igdebug_switch == TRUE)
          
            dbg(DEBUG,"File %s not found",pcpFilNam);
              return RC_NOT_FOUND;
            }
          lgFsiz = fread(pcgFbuf,(size_t)1,(size_t)MAX_FIL_SIZE,fh);
          /*      
              if(igdebug_switch == TRUE)
              dbg(DEBUG,"%ld Data Read From File %s", lgFsiz, pcpFilNam); */
          fclose(fh);
          /*newsnap(DEBUG,pcgFbuf,lgFsiz,outp);*/
        }

          if (strcmp(pcpCommand,"gtFilerPutFile") == 0 ||
          strcmp(pcpCommand,"gtFilerPutData") == 0 ||
          strcmp(pcpCommand,"gtMonitorStatusRequest") == 0 ||
          strcmp(pcpCommand,"gtTerminalStatusRequest") == 0 ||
          strcmp(pcpCommand,"gtFilerStatusRequest") == 0 ||
          strcmp(pcpCommand,"gtFilerRemoveFile") == 0 ||
          strcmp(pcpCommand,"gtFilerFindFirst") == 0 ||
          strcmp(pcpCommand,"gtFilerFindNext") == 0 ||
          strcmp(pcpCommand,"gtFilerCreateDirectory") == 0 ||
          strcmp(pcpCommand,"gtFilerRemoveDirectory") == 0 ||
          strcmp(pcpCommand,"gtTerminalGetIntegrity") == 0)
        {
                  igAnswer++;   /* answer required */
        }

          if (strcmp(pcpCommand,"gtTerminalStatusRequest") == 0)
        {
          igStatReq++;   /* status request required */
        }

          if (strcmp(pcpCommand,"gtTextPut") == 0 ||
          strcmp(pcpCommand,"gtTextPutFormatted") == 0)
        {
          if (ipNoParam == 0)
            {                                           
              ipNoParam = 1;
            }
        }

          if (ipNoParam != comList[i].noParam)
        {
          return RC_DATA_CUT;
        }

          k = 0;
          for (ilCp=0; ilCp<ipNoParam; ilCp++)
        {
          if (ipNoParam == 1)
            {
              if (strcmp(pcpCommand,"gtTextPut") == 0 ||
              strcmp(pcpCommand,"gtTextPutFormatted") == 0)
            {   
              if (pcpParam[0] == 0)
                {
                  strcpy(pclPar," ");
                }
              else
                {
                  strcpy(pclPar,pcpParam);
                }
            }
              else
            {
              strcpy(pclPar,pcpParam);
            }
            }
          else
            {
              l = 0;
              while (pcpParam[k] != ',' && k < (int) strlen(pcpParam))
            {
              pclPar[l] = pcpParam[k];
              l++;   k++;
            }
              pclPar[l] = '\000';
            }
          k++;
          switch(comList[i].param[ilCp])
            {
            case -1 :
              strcpy(&pcgGt2Buf[lgCurBufSiz],pclPar);
              lgCurBufSiz += strlen(pclPar);
              pcgGt2Buf[lgCurBufSiz] = '\000';   lgCurBufSiz++;
              break;
            case -2 :
              llHtonsValL = htonl((long)lgFsiz);
              memcpy(&pcgGt2Buf[lgCurBufSiz],(char *)&llHtonsValL,4);
              lgCurBufSiz += 4;
              break;
            case -3 :
              slHtonsValS = htons((short)lgFsiz);
              memcpy(&pcgGt2Buf[lgCurBufSiz],(char *)&slHtonsValS,2);
              lgCurBufSiz += 2;
              break;
            case -4 :
              memcpy(&pcgGt2Buf[lgCurBufSiz],pcgFbuf,lgFsiz);
              lgCurBufSiz += lgFsiz;
              break;
            default :
              if (strncmp(pclPar,"gt",2) == 0)
            {
              j = 0;
              ilFound1 = 0;
              while (strcmp(constList[j].constName,"gtEnde") != 0 && ilFound1 == 0)
                {
                  if (strcmp(constList[j].constName,pclPar) == 0)
                {
                  ilFound1 = 1;
                  if (comList[i].param[ilCp] == 1)
                    {
                      clValC = (char) constList[j].constValue;
                      pcgGt2Buf[lgCurBufSiz] = clValC;
                      lgCurBufSiz++;
                    }
                  else
                    {
                      if (comList[i].param[ilCp] == 2)
                    {
                      slValS = constList[j].constValue;
                      slHtonsValS = htons(slValS);
                      memcpy(&pcgGt2Buf[lgCurBufSiz],(char *)&slHtonsValS,2);
                      lgCurBufSiz += 2;
                    }
                      else
                    {
                      llValL = constList[j].constValue;
                      llHtonsValL = htonl(llValL);
                      memcpy(&pcgGt2Buf[lgCurBufSiz],(char *)&llHtonsValL,4);
                      lgCurBufSiz += 4;
                    }
                    }
                }
                  j++;
                }
              if (ilFound1 == 0)
                {
                  return RC_NOT_FOUND;
                }
            }
              else
            {
              if (comList[i].param[ilCp] == 1)
                {
                  clValC = (char) atoi(pclPar);
                  pcgGt2Buf[lgCurBufSiz] = clValC;
                  lgCurBufSiz++;
                }
              else
                {
                  if (comList[i].param[ilCp] == 2)
                {
                  slValS = (short) atoi(pclPar);
                  slHtonsValS = htons(slValS);
                  memcpy(&pcgGt2Buf[lgCurBufSiz],(char *)&slHtonsValS,2);
                  lgCurBufSiz += 2;
                }
                  else
                {
                  llValL = (long) atoi(pclPar);
                  llHtonsValL = htonl(llValL);
                  memcpy(&pcgGt2Buf[lgCurBufSiz],(char *)&llHtonsValL,4);
                  lgCurBufSiz += 4;
                }
                }
            }
            }
        }   /* end for */
        }   /* end if */
      i++;
    }   /* end while */
    }   /* end if MidRequest */
  if (ilFound == 0)
    {
      return RC_NOT_FOUND;
    }
    
  if (ilSscMode == 1)
    {
      slHtonsValS = htons((unsigned int)lgCurBufSiz);
    }
  else
    {
      slHtonsValS = htons((unsigned int)(lgCurBufSiz-2));
    }
  memcpy(&pcgGt2Buf[0],(char *)&slHtonsValS,2);   /* message length */

  if (pcpInd[0] == 'E' || pcpInd[0] == '1')
    {

      newsnap(DEBUG,pcgGt2Buf, max(min((unsigned int)lgCurBufSiz,256),16),outp); 
      llRemain = lgCurBufSiz;
      llDataLen = llRemain;
      while (llRemain > 0)
    {
      if (llRemain > PACKET_LEN)
        {
          slHtonsValS = htons(PACKET_LEN-2);
          memcpy(&pcgGt2Buf[0],(char *)&slHtonsValS,2);
          slHtonsValS = htons(DATA_PACKET_LEN);
          memcpy(&pcgGt2Buf[8],(char *)&slHtonsValS,2);
          llDataLen = PACKET_LEN;
          newsnap(DEBUG,pcgGt2Buf, max(min(llDataLen,128),16),outp); 
        }
      if(igdebug_switch == TRUE)
        dbg(DEBUG,"Send %d Bytes to controller %d",
        (unsigned int)llDataLen, ipSocket);
        if(igslow_connection == TRUE)
        {
          ilCheck=CheckSocket(ipSocket,WRITESET,1,200000);
        }else{
          ilCheck=CheckSocket(ipSocket,WRITESET,0,100000);
        }
        if (ilCheck != RC_SUCCESS)
        {
            dbg(TRACE,"MyGtBuildCommand: SELECT (WRITESET) failed!");
            return RC_FAIL;
        }
      alarm(3);
      ilRc = write(ipSocket,pcgGt2Buf,llDataLen);
      alarm(0);
      if (ilRc != llDataLen)
        {
         /* if(igdebug_switch == TRUE) */
            dbg(TRACE,"Error SendBuffer to controller %d: %d",
            ipSocket,ilRc);
          return RC_FAIL;
        }
      /*  if(igdebug_switch == TRUE) 
          dbg(DEBUG,"SendBuffer to controller %d successful",ipSocket); */
      if (igAnswer > 0)
        {
          i = 0;
          memset(pclAckMsg,0x00,MAX_RCV_SIZE);
          ilRc = RC_TIMEOUT;

          /*while (ilRc == RC_TIMEOUT && i < ilMaxLpCnt)*/
         /* while (ilRc == RC_TIMEOUT && i < 1) */
          /*  {  */
                if(igslow_connection==TRUE)
                {
                    ilCheck=CheckSocket(ipSocket,READSET,5,750000);
                }else{
                    ilCheck=CheckSocket(ipSocket,READSET,2,500000);
                }
                if(ilCheck != RC_SUCCESS)
                {

                    ilRc = RC_TIMEOUT;
                    i++;
                }
                else
                {
                    memset(pclAckMsg,0x00,MAX_RCV_SIZE);
                    alarm(3);
                    ilRc = read(ipSocket,pclAckMsg,MAX_RCV_SIZE);
                    alarm(0);
                    if (ilRc > 0)
                    {/*If*/
                        if(igdebug_switch == TRUE)
                        {
                            dbg(DEBUG,"%d Bytes received (%d)",ilRc,i);
                            newsnap(DEBUG,pclAckMsg,max(ilRc,16),outp);
                        }
                        if (strcmp(pcpCommand,"gtSystemMidRequest") != 0)
                        {
                            if(igdebug_switch==TRUE)
                                dbg(DEBUG,"Line %d gtCheckMessage strlen(pcpResBuf)= %d",__LINE__,strlen(pcpResBuf));
                            ilMsgRc = gtCheckMessage(pclAckMsg,ilRc,ipSocket,pcpResBuf);   /* Interpret received message */
                        }else{
                            pcpResBuf[0] = '\000';
                            if (pclAckMsg[4] == '\001')
                            {     /* send Session open accept */
                                pclAckMsg[4] = '\002';
                                llDataLen = ilRc;
                                if(igdebug_switch == TRUE)
                                {
                                    dbg(DEBUG,"Send respose to ssc");
                                    newsnap(DEBUG,pclAckMsg,max(16,(int)llDataLen),outp);
                                }


                                 if(igslow_connection == TRUE)
                                 {
                                    ilCheck=CheckSocket(ipSocket,WRITESET,1,200000);
                                 }else{
                                    ilCheck=CheckSocket(ipSocket,WRITESET,0,100000);
                                 }
       
                                if (ilCheck!=RC_SUCCESS)
                                {
                                    dbg(TRACE,"MyGtBuildCommand: SELECT (WRITESET) failed!");
                                    ilRc = RC_FAIL;
                                }
                                else
                                {
                                    alarm(3);
                                    ilRc = write(ipSocket,pclAckMsg,llDataLen);
                                    alarm(0);
                                    if (ilRc != llDataLen)
                                    {
                                        if(igdebug_switch == TRUE)
                                            dbg(DEBUG,"Error Send Respose to SSC: %d",ilRc);
                                        ilRc = RC_FAIL;
                                    }
                                }
                            }else{
                                ilSscDataLen = ilRc;
                                ilSscCurLen = 0;
                                while (ilSscCurLen < ilSscDataLen)
                                {
                                    if (pclAckMsg[ilSscCurLen+4] == 0x32)
                                    {
                                        if (strlen(pcpResBuf) > 0)
                                        {
                                            sprintf(pclSscData,",%d,%d",
                                            pclAckMsg[ilSscCurLen+6],
                                            pclAckMsg[ilSscCurLen+7]);
                                        }else{
                                            sprintf(pclSscData,"%d,%d",
                                            pclAckMsg[ilSscCurLen+6],
                                            pclAckMsg[ilSscCurLen+7]);
                                        }
                      
                                        if(igdebug_switch == TRUE)
                                            dbg(DEBUG,"Data received: %s",pclSscData);
                                        strcat(pcpResBuf,pclSscData);
                      
                                        if(igdebug_switch == TRUE)
                                            dbg(DEBUG,"Data received so far: %s",pcpResBuf);
                                    }
                                    ilSscCurLen += pclAckMsg[ilSscCurLen+1];
                                }
                            }
                        }

              if (igStatReq > 0)
                {
                    ilBufLen = ilRc;
                    ilRc = RC_TIMEOUT;
                    for (ilBufIdx=0; ilBufIdx<ilBufLen-4; ilBufIdx++)
                    {
                        if (pclAckMsg[ilBufIdx] == gtREPLY &&
                            pclAckMsg[ilBufIdx+4] == gtREPLY_TERMINAL_STATUS_REQUEST)
                        {
                            ilRc = RC_SUCCESS;
                        }
                    }
                }else{
                    ilRc = RC_SUCCESS;
                }
            }else{
              ilRc = RC_TIMEOUT;
              i++;
              /*sleep(1);*/
            }
          /*} */ /*end while*/
        }
          if (ilRc != RC_SUCCESS)
        {
          
          if(igdebug_switch == TRUE)
            dbg(DEBUG,"...but no answer after %d tries!",ilMaxLpCnt);

          return RC_TIMEOUT;
         /*return RC_SUCCESS; */
        }
          ilRc = RC_SUCCESS;    
        }

      if (llRemain > PACKET_LEN)
        {
          llRemain -= DATA_PACKET_LEN;
          memcpy(&pcgGt2Buf[10],&pcgGt2Buf[PACKET_LEN],llRemain);
          slHtonsValS = htons((unsigned int)(llRemain-2));
          memcpy(&pcgGt2Buf[0],(char *)&slHtonsValS,2);
          slHtonsValS = htons((unsigned int)(llRemain-10));
          memcpy(&pcgGt2Buf[8],(char *)&slHtonsValS,2);
          llDataLen = llRemain;
          if(igdebug_switch == TRUE)
            newsnap(DEBUG,pcgGt2Buf, max(min(llDataLen,128),16),outp);
        }
      else
        {
          llRemain = 0;
        }
      /*  
          if(igdebug_switch == TRUE)
          dbg(DEBUG,"Remaining bytes %ld",llRemain); */
    }   /* end while */
      igAnswer = 0;
      igStatReq = 0;
    }

  return ilRc;
}







