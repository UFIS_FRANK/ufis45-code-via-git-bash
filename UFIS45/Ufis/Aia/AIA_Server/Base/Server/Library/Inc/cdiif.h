/* ********************************************************************** */
/*  The Master include file.						  */
/*									  */
/*  Program	  :     						  */
/*  Revision date :							  */
/*  Author    	  :							  */
/*  									  */
/*  NOTE : This should be the only include file for your program          */
/* ********************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <memory.h>
#include <unistd.h>

#include "ugccsma.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "tools.h"
#include "helpful.h"
#include "netin.h"		/* murks->tcputil.h needs it!!!! */
#include "tcputil.h"
#include "new_catch.h"
#include "send_tools.h"
#include "hsbsub.h"
#include "l_ccstime.h"

#ifndef _CDIIF_INC
#define _CDIIF_INC

static char sccs_cdiif_inc[]="@(#) UFIS 4.4 (c) ABB AAT/I cdiif.h 44.1 / 00/01/06 16:31:32 / RBI";

extern char *strdup(const char*);

/* defines for send_message */
#define	iERROR_FROM_DEST				513
#define	iCREATE_CONNECTION			514
#define	iDELETE_CONNECTION			515
#define	iWERR_TO_CLIENT				516

/* global defines */
#define	cCOMMA							','
#define	cBLANK							' '
#define	cPOINT							'.'
#define	iBLANK							32		/* ASCII 32 -> Blank */
#define 	cSEPARATOR						','
#define 	cASSIGNSEPARATOR				'='
#define 	cDATA_SEPARATOR				'#'
#define	sDATA_SEPARATOR				"#"
#define 	sCONTROL_CLASS					"CTL"
#define 	iCONTROL_CLASS					0	
#define 	sDATA_CLASS						"DAT"
#define 	iDATA_CLASS						1	
#define 	sMIX_CLASS						"MIX"
#define 	iMIX_CLASS						2	
#define	sDDEL_CLASS						"DEL"
#define	iDDEL_CLASS						3
#define	sMDEL_CLASS						"DEL"
#define	iMDEL_CLASS						4
#define 	sFTP_FILENAME					"CDI" /* format cdi-timestamp.dat */
#define 	sFTP_FILEEXTENT				".DAT"
#define 	iPERMS							0666
#define 	iMAX_BYTES						9900	/* remaining 99 Bytes! */
#define 	iFORWARD							0
#define	iBACKWARD						1
#define  iRECOVER_FILE					0
#define  iRECOVER_MEMORY				1
#define  iCLEAR_ALL						0
#define 	iADD_CMD							1
#define  iGET_FIRST_CMD					2
#define 	iGET_LAST_CMD					3
#define  iGET_TELENO_CMD				4
#define  iPRINT							5
#define	iGET_ALL_AT_TELENO			6
#define	iGET_ALL_AT_TIMESTAMP		7
#define 	iCOUNTER							0
#ifdef iTIME
	#undef iTIME
	#define 	iTIME								1
#endif
#define 	iRECOVER_MEM_STEP				iDEFAULT_RECOVER_COUNTER
#define  sRPROT_DEFAULT					"./rprot"
#define  sSPROT_DEFAULT					"./sprot"
#define	sPROT_FILEEXTENSION			".log"
#define	sACKNOWLEDGE					"A"
#define	sCHECK							"C"
#define	iLEFT								0
#define	iRIGHT							1
#define	iMIN_CHECK_TIMEOUT			1
#define	iMIN_TCP_TIMEOUT				1
#define	iVALID							0
#define	iNOT_VALID						1
#define	s14_BLANKS						"              "
#define	iINIT_TNUMBER					-1
#define	iORG_DATA						0
#define	iNULL_DATA						1
#define	iFOUND							0
#define	iNOT_FOUND						1

/* errorcodes for ERR-Command */
#define	sWRONG_SERVERSYSTEM			"WY"
#define	iWRONG_SERVERSYSTEM			1	
#define	sUNKNOWN_SENDER				"US"
#define	iUNKNOWN_SENDER				2	
#define	sUNKNOWN_RECEIVER				"UR"
#define	iUNKNOWN_RECEIVER				3	
#define	sTIMEOUT							"TO"
#define	iTIMEOUT							4
#define	sWRONG_COMMAND					"WC"
#define	iWRONG_COMMAND					5	
#define	sUNKNOWN_CLASS					"UC"
#define	iUNKNOWN_CLASS					6	
#define	sWRONG_TIMESTAMP				"WT"
#define	iWRONG_TIMESTAMP				7	
#define	sWRONG_LENGTH					"WL"
#define	iWRONG_LENGTH					8	
#define	sWRONG_NUMBER					"WN"
#define	iWRONG_NUMBER					9	
#define	sCCO_TNUMBER					"CN"
#define	iCCO_TNUMBER					10	
#define  sAC_ERROR						"AC"
#define  iAC_ERROR						11
#define	sNO_DATA							"ND"
#define	iNO_DATA							12	
#define	sWRONG_SUBCOMMAND				"WS"
#define	iWRONG_SUBCOMMAND				13	
#define	sSEPARATOR_MISSING			"SM"
#define	iSEPARATOR_MISSING			14	
#define	sWRONG_DATA						"WD"
#define	iWRONG_DATA						15	
#define	sUNKNOWN_FIELDNAME			"UF"
#define	iUNKNOWN_FIELDNAME			16	
#define	sWRONG_ELEMENTS				"WE"
#define	iWRONG_ELEMENTS				17	
#define	sINVALID_CMD					"IC"
#define	iINVALID_CMD					18
#define	sWRONG_NUMBER_OF_FIELDS		"OF"
#define	iWRONG_NUMBER_OF_FIELDS		19
#define	sINTERNAL_ERROR				"IE"
#define	iINTERNAL_ERROR				20

/* defaults */
#define 	iDEFAULT_CHECK_CONNECTIONS	60
#define 	iDEFAULT_CHECK_TIMEOUT		2
#define 	iDEFAULT_ACKNOWLEDGE			1
#define 	iDEFAULT_TCP_TIMEOUT			2
#define 	iDEFAULT_MAX_CONNECTIONS	1
#define 	sDEFAULT_SERVICE_NAME		"UFIS_CDI"  /* remember /etc/services.. */
#define 	iDEFAULT_USEFTP				0				/* No */
#define 	iDEFAULT_USEFTP_FOR_UPDATE	0				/* No */
#define 	iDEFAULT_FTPSIZE				0				/* every time */
#define 	sDEFAULT_ASSIGNSEP			"="
#define 	sDEFAULT_FTPPATH				"./"
#define  iDEFAULT_RECOVER_TIME		60	
#define  iDEFAULT_RECOVER_COUNTER	5000
#define  sDEFAULT_RECOVER_TABLE		"RECTAB"
#define  sDEFAULT_RECOVER_FILE		"./rfile.dat"
#define  sDEFAULT_RECOVER_TMP_DIR		"/ceda/debug"
#define  iDEFAULT_RECOVERTYPE			iRECOVER_FILE
#define	iDEFAULT_MAX_FAIL_CMDS		10		/* 10 tries */

/* returncodes */
#define 	iRECOVER_MEMORY_FAIL						-1
#define 	iSEARCH_FAIL								-2
#define  iRECOVER_REALLOC_FAIL					-3
#define 	iRECOVER_FOPEN_FAIL						-4
#define	iRECOVER_CONNECTION_FAIL				-5
#define 	iTMPFILE_FAIL								-6
#define	iTELENO_FAIL								-7
#define	iTIMESTAMP_FAIL							-8
#define	iFIRST_MALLOC_FAIL						-9
#define	iFIRST_CMD_FAIL							-10
#define	iLAST_MALLOC_FAIL							-11
#define	iLAST_CMD_FAIL								-12
#define	iGET_TELENO_MALLOC_FAIL					-13
#define	iGET_ALL_AT_TELENO_MALLOC_FAIL		-14
#define	iGET_ALL_AT_TIMESTAMP_MALLOC_FAIL	-15
#define	iLEVEL_FAIL									-16
#define	iANZ_FAIL									-17
#define	iREAD_CNT_FAIL								-18
#define	iEND_RECEIVED								-19
#define	iWRITE_FAIL									-20
#define	iWRITE_ERRFAIL								-21
#define	iWRITE_GRSFAIL								-22

/* buffersize */
#define 	iMAX_RECEIVE_DATA				0x3000 	/* 12 kB */
#define 	iMAX_SEND_DATA					0x3000	/* 12 kB */
#define 	iRECEIVE_BUF_SIZE				2048
#define	iFORMAT_BUF_SIZE				2048
#define	iSERVICENAME_SIZE				10
#define	iTABNAME_SIZE					10
#define	iSELECT_SIZE					128
#define	iORDERBY_SIZE					128
#define	iDBFIELD_SIZE					10
#define	iIFFIELD_SIZE					10
#define	iSERVERSYS_SIZE				3
#define	iSENDER_SIZE					10
#define	iRECEIVER_SIZE					10
#define 	iTIMESTAMP_SIZE				14
#define 	iTNUMBER_SIZE					5
#define 	iTCLASS_SIZE					3
#define 	iLENGTH_SIZE					4
#define	iCOMMAND_SIZE					3
#define	iSUBCOMMAND_SIZE				20	
#define	iSTIME_SIZE						14
#define	iETIME_SIZE						14
#define	iUPDATE_SIZE					14	
#define	iERRORCODE_SIZE				2
#define	iORIGIN_SIZE					3
#define	iTYPE_SIZE						2
#define	iKEY_SIZE						25
#define	iSEPARATOR_SIZE				1
#define 	iASSIGNSEP_SIZE				2
#define  iTABLE_SIZE						10
#define  iRFILE_SIZE						iMIN_BUF_SIZE
#define	iPROTFILE_SIZE					iMIN_BUF_SIZE
#define	iAQ_SIZE							1
#define	iHANDLERCMD_SIZE				4
#define	iMAX_TN							20
#define	iSECONDS							1
#define	iMICRO_SECONDS					0

/* positions */
#define 	iSERVERSYS_POSITION			0
#define 	iSENDER_POSITION				iSERVERSYS_POSITION+iSERVERSYS_SIZE
#define 	iRECEIVER_POSITION			iSENDER_POSITION+iSENDER_SIZE
#define 	iTIMESTAMP_POSITION			iRECEIVER_POSITION+iRECEIVER_SIZE
#define 	iTNUMBER_POSITION				iTIMESTAMP_POSITION+iTIMESTAMP_SIZE
#define 	iTCLASS_POSITION				iTNUMBER_POSITION+iTNUMBER_SIZE
#define 	iLENGTH_POSITION				iTCLASS_POSITION+iTCLASS_SIZE
#define 	iCOMMAND_POSITION				iLENGTH_POSITION+iLENGTH_SIZE

/* bitmask (16 bit word) */
#define	iF_INITIALIZE					0x0000
#define 	iF_CMD_STRUCT_MEMORY			0x0001
#define 	iF_CMD_COMMAND_MEMORY		0x0002
#define 	iF_TAB_STRUCT_MEMORY			0x0004
#define 	iF_FIELD_POINTER_MEMORY		0x0008
#define 	iF_FIELD_MEMORY				0x0010
#define 	iF_ASSIGN_STRUCT_MEMORY		0x0020
#define 	iF_TCPIP_CLIENT_SOCK			0x0040
#define 	iF_RECEIVE_DATA_MEMORY		0x0080
#define 	iF_DATA_LENGTH_MEMORY		0x0100 
/* #define 	iF_WRITE_END_TO_CLIENT		0x0200 */
#define 	iF_SEND_DATA_MEMORY			0x0400
#define 	iF_RECOVER_MEMORY				0x0800
/* #define  iF_DB_CONNECT					0x1000 */
/* #define  iF_FTP_FILE						0x2000 */
#define  iF_RPROT_FILE					0x4000
#define	iF_SPROT_FILE					0x8000

/* macros */
#define 	DIRECTION(a,b) ((a) == iFORWARD ? (b++) : (b--))	
#define	INCR(a) ((a) == 99999 ? 0 : ((a)++))
#define	DECR(a) ((a) == 0 ? 99999 : ((a)--))
#define	MOD(a,b) ((a) %= ((b)+1))
#define  UNIQUE(a,b,c) (((a)%((b)+1)) < (c) ? (c) : ((a)%((b)+1)))

/* structure/union */
/* ---------------------------------------------------------------------------*/
/* ---------------------------------------------------------------------------*/
/* ---------------------------------------------------------------------------*/
typedef struct _datamap
{
	UINT		iOtherData;	/* internal flag */
	char		pcOrgData[iMIN_BUF_SIZE]; /* the original data */
	char		pcNewData[iMIN_BUF_SIZE]; /* the new data */
} DataMap;

typedef struct _tfail
{
	UINT		iSendErrorCounter;
	UINT		iReceiveErrorCounter;
} TFail;

typedef struct _trecover
{
	int		iCmdNo; /* command number of this */
	int		iTabNo;	/* table number of this */
	ULONG		lLength;	/* length of recover entry */
	char		pcTimeStamp[iTIMESTAMP_SIZE+1]; /* timestamp of telegram */
	char		pcTeleNo[iTNUMBER_SIZE+1];	/* number of telegram */
	char		pcData[iMAX_RECEIVE_DATA]; /* data of this telegram */
} TRecover;

/* pointer to function */
typedef int (*RecoverHandler)(UINT, UINT *, TRecover *, TRecover **);

typedef struct _ttcp
{
	UINT		iMaxBytes;					/* like iMAX_BYTES */
	UINT		iTcpTimeout;				/* timeout for tcp/ip-connection */
	UINT		iMaxConnections;			/* max. connections */
	char		pcServiceName[iSERVICENAME_SIZE];  /* service name (TCP/IP) */
} TTcp;

typedef struct _tserver
{
	char		pcServerSystem[iSERVERSYS_SIZE+1]; /* name of server system */
	char		pcSender[iSENDER_SIZE+1]; /* name of sender */
	char		pcReceiver[iRECEIVER_SIZE+1]; /* name of receiver */
} TServer;

typedef struct _tassign
{
	UINT		iDBFieldSize; 	/* size of database field */	
	UINT		iIFFieldSize;	/* size (length) of interface field */
	char		pcDBField[iDBFIELD_SIZE]; /* name of dbfield */
	char		pcIFField[iIFFIELD_SIZE]; /* name of interfacefield */
} TAssign;

typedef struct _tabdef
{
	int		iModID;	/* the mod_id we use for sending */
	UINT		*piNoMapData; /* how many data for mapping */
	DataMap	**prDataMap; /* the data */
	UINT		iVia34;	/* should i use 3- or 4-letter-code for VIAx */
	UINT		iUseReceivedSelection; 	/* shoud i use selection received via TCP */
	UINT		iTelegramClass; /* telegram class for this table (MIX,DAT,DEL) */
	char		pcDataType[iMIN]; /* type of data (BD,FD...) */
	char		pcSndCmd[iMIN]; /* command for sending (QUE) */
	char		pcTabName[iTABNAME_SIZE];	/* Tablename */
	UINT		iUseSelection;	/* should we use selection */	
	char		pcSelect[iSELECT_SIZE*3];		/* selection */
	UINT		iUseOrderBy/* should we use order by */;
	char		pcOrderBy[iORDERBY_SIZE];	/* order by ... */
	UINT		iUrnoNo;		/* position of urno in field list */
	UINT		iViaInFieldList;	/* is there VIAx in field list? */
	UINT		iDataLengthCounter;	/* how many length-entries */
	UINT		*piDataLength;	/* the data length'ens */
	UINT		iNoFields;	/* number of fields */
	char		**pcFields;		/* all fields */
	UINT		iNoAssign;	/* number of assignments */
	TAssign	*prAssign;	/* assign DB-Field to IF-Field */
} TabDef;
    
typedef struct _tcommand
{
	char		pcHomeAirport[iMIN]; /* the HomeAirport */
	char		pcTableExtension[iMIN]; /* the table externsion */
	FILE		*FtpFH;	/* for this data file */
	UINT		iOpenDataFile;	/* is this data file open? */
	UINT		iFilenameWithTimestamp; /* ftp-file with timestamp */
	char		pcFileName[iMIN];	/* filename for ftp-file */
	char		pcFileExtension[iMIN]; 	/* extension for ftp-file */
	char		pcTotalFileName[iMIN_BUF_SIZE];	/* the complete filename */
	char		pcTotalFileNameWithPath[iMIN_BUF_SIZE]; /* filename and path */
	UINT		iDataCounter;	/* how many tables (data) in file? */
	UINT		iNoTables;	/* number of tables per command */
	char		*pcCommand;	/* possible commands */
	TabDef	*prTabDef;	/* combine tables and commands */
} TCommand;

typedef struct _trec
{
	UINT					iRecoverType;			/* DB, File, Memory */
	char					pcRecoverTable[iTABLE_SIZE]; /* name of db-table */
	char					pcRecoverFile[iRFILE_SIZE]; /* name of recover file */
	char					pcRecoverTmpDir[iRFILE_SIZE]; /* name of recover file */
	UINT					iSaveData;				/* counter, time */
	UINT					iRecoverCounter; /* counter */
	UINT					iRecoverTime;	/* time to recover */
	RecoverHandler		RHandler[2];	/* function handler */
} TRec;

typedef struct _tprot
{
	char		pcRProtFile[iPROTFILE_SIZE]; /* filename of rprot-file */
	char		pcSProtFile[iPROTFILE_SIZE]; /* filename of sprot-file */
} TProt;

typedef struct _tftp
{
	char		pcUser[iMIN]; /* ftp user */
	char		pcPass[iMIN]; /* password for user */
	char		pcFtpCtlFile[iMIN]; /* name of control-file */
	char		pcFtpFilePath[iMIN_BUF_SIZE]; /* path for control-file */
	char		pcMachine[iMIN]; /* machine name to copy at */
	char		pcMachinePath[iMIN_BUF_SIZE]; /* path at machine */
} TFtp;

/* the master cfg-structure */
typedef struct _cditmain
{
	UINT		iTimeUnit; /* seconds or microseconds... */
	char		pcCloseConnectCommand[iMIN]; /* the connect command we send */
	char		pcOpenConnectCommand[iMIN]; /* the connect command we send */
	int		iConnectModID; /* the mod_id we send command to */
	UINT		iCCOSync;				/* CCO-Sync */
	UINT		iMaxFailCmds;			/* max fail commands */
	int		iCheckConnect;			/* check connect all n seconds */	
	UINT		iCheckTimeout;			/* timeout for CCO */
	UINT		iAcknowledge;			/* ack. yes - no */
	UINT		iUseFtp;					/* should i use FTP? */
	UINT		iUseFtpForUpdate;		/* if we receive UFR use FTP or not */
	UINT		iFtpSize;				/* size for ftp-using */
	UINT		iUseRProt;				/* receive protocol */
	UINT		iUseSProt;				/* send protocol */
	UINT		iSendEODAfterUpdate;	/* send an EndOfData after Update-Telegram */
	char		pcAssignSeparator[iASSIGNSEP_SIZE];		
	TFtp		rFtp;						/* something to FTP */
	TProt		rProt;					/* something to protocol */
	TRec		rRec;						/* something to recovery */
	TTcp		rTCP;						/* something to TCP/IP */
	TServer	rServer;					/* something to server */
	UINT		iNoCmd;					/* number of Command-Structures */
	TCommand	*prCmd;					/* something to Command and tables */
} CDITMain;

/* ---------------------------------------------------------------------------*/
/* ---------------------------------------------------------------------------*/
/* ---------------------------------------------------------------------------*/
/* following structures for data exchange */
/* header part of every telegram */
typedef struct _theader
{
	char		pcServerSystem[iSERVERSYS_SIZE];
	char		pcSender[iSENDER_SIZE];
	char		pcReceiver[iRECEIVER_SIZE];
	char		pcTimeStamp[iTIMESTAMP_SIZE];
	char		pcTNumber[iTNUMBER_SIZE];
	char		pcTClass[iTCLASS_SIZE];
	char		pcLength[iLENGTH_SIZE];
} THeader;

/* send flight schedule */
typedef struct _tsfs
{
	THeader 	rTH;
	char		pcCommand[iCOMMAND_SIZE];
	char		pcSubCommand[iSUBCOMMAND_SIZE];
	char		pcStartTime[iSTIME_SIZE];
	char		pcEndTime[iETIME_SIZE];
} TSFS;

/* End of Data */
typedef struct _teod
{
	THeader 	rTH;
	char		pcCommand[iCOMMAND_SIZE];
} TEOD;

/* Acknowledge */
typedef struct _tack
{
	THeader 	rTH;
	char		pcCommand[iCOMMAND_SIZE];
} TACK;

/* send flight update */
typedef struct _tsfu
{
	THeader 	rTH;
	char		pcCommand[iCOMMAND_SIZE];
	char		pcSubCommand[iSUBCOMMAND_SIZE];
	char		pcUpdate[iUPDATE_SIZE];
} TSFU;

/* check connection */
typedef struct _tcco
{
	THeader 	rTH;
	char		pcCommand[iCOMMAND_SIZE];
	char		pcAQ[iAQ_SIZE];
} TCCO;

/* close connection */
typedef struct _tend
{
	THeader 	rTH;
	char		pcCommand[iCOMMAND_SIZE];
	char		pcErrorCode[iERRORCODE_SIZE];
} TEND;

/* Data-Telegramm */
typedef struct _tdata
{
	THeader	rTH;
	char		pcTimeStamp[iTIMESTAMP_SIZE];
	char		pcOrigin[iORIGIN_SIZE];
	char		pcType[iTYPE_SIZE];
	char		pcKey[iKEY_SIZE];
	char		pcSeparator[iSEPARATOR_SIZE];
	char		pcData[1];
} TData;

/* Mix-Telegramm (CTL+DAT) */
typedef struct _tmix
{
	THeader	rTH;
	char		pcCommand[iCOMMAND_SIZE];
	char		pcSeparator[iSEPARATOR_SIZE];
	char		pcData[1];
} TMix;

/* Error-Telegramm */
typedef struct _terr
{
	THeader 	rTH;
	char		pcCommand[iCOMMAND_SIZE];
	char		pcErrorCode[iERRORCODE_SIZE];
} TERR;

/* alle Arten von Stammdaten */
typedef struct _tbxx
{
	THeader 	rTH;
	char		pcCommand[iCOMMAND_SIZE];
	char		pcUpdateTime[iTIMESTAMP_SIZE];
} TBXX;

/* ---------------------------------------------------------------------------*/
/* ---------------------------------------------------------------------------*/
/* ---------------------------------------------------------------------------*/
/* Length of structures */
#define 	iTHEADER_LENGTH 		sizeof(THeader)
#define 	iTSFS_LENGTH			sizeof(TSFS)
#define 	iTEOD_LENGTH			sizeof(TEOD)
#define 	iTSFU_LENGTH			sizeof(TSFU)
#define 	iTCCO_LENGTH			sizeof(TCCO)
#define 	iTEND_LENGTH			sizeof(TEND)
#define 	iTDATA_LENGTH			sizeof(TData)
#define	iTMIX_LENGTH			sizeof(TMix)
#define	iTERR_LENGTH			sizeof(TERR)
#define	iTACK_LENGTH			sizeof(TACK)
#define 	iTBXX_LENGTH			sizeof(TBXX)

#endif /* _CDIIF_H */

