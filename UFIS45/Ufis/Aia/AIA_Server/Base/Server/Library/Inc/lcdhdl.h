#ifndef _DEF_mks_version_lcdhdl_h
  #define _DEF_mks_version_lcdhdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_lcdhdl_h[] = "@(#) "UFIS_VERSION" $Id: Ufis/Aia/AIA_Server/Base/Server/Library/Inc/lcdhdl.h 1.1 2004/10/04 16:57:50SGT jim Exp  $";
#endif /* _DEF_mks_version_lcdhdl_h */
/*********************************************************/
/* LCDHDL (AEG-MIS LCD board controller handler )        */
/* header file                                           */
/*********************************************************/
#ifndef BOOL
#define BOOL int
#endif

/***************/
/* ALL DEFINES */
/***************/
#define XXS_BUFF  32
#define XS_BUFF  128
#define S_BUFF  512
#define M_BUFF  1024
#define L_BUFF  2048
#define XL_BUFF 4096
#define JF_BUFF  200

#ifndef MAX_BUFFER_LEN
	#define MAX_BUFFER_LEN 2048
#endif
#define DATABLK_SIZE  524288 /* Maximum space for sql_if data_area&sql-buffer*/
#define CONNECT_TIMEOUT 2
#define READ_TIMEOUT 2
#define WRITE_TIMEOUT 2

#define RIGHT 1100
#define LEFT 1101

#define CFG_ALPHA 1200
#define CFG_NUM		1201
#define CFG_ALPHANUM	1202
#define CFG_IGNORE	1203
#define CFG_PRINT	1204

#define ENGLISH 0
#define LOCAL	1

#define EXTD_OFF 0
#define EXTD_ON  1 

#define C_WSTART   1
#define C_WREFRESH 2
#define C_WNEXT    3

/************************/
/* CFG-File - STRUCTURE */
/************************/
typedef struct 
{
	/* MAIN section in .cfg file*/    
	char *refresh_cycle;		/* refresh-time for board-group */ 
	char *switch_cycle;			/* switch-time for board-group */ 
	char *lcd_ips;					/* IP-address(es) or hostname(s) of the LCD-boards */ 
	char *board_cols;				/* columns of board */
	char *board_rows;				/* rows of board */
	char *service_port; 		/* service name for receiving data */
	char *use_counter_table;	/* service name for receiving data */
	char *db_tables; 				/* tables */
	char *db_est_time_field;	/* field to use forestimated times */
	char *db_fields; 				/* field list from db*/
	char *start_min; 	/* number of minutes to substract from time for DB-select */
	char *end_min; 	  /* number of minutes to add to time for DB-select */
	char *t_para1; 							/* time paramter 1 for select */
	char *t_para2; 							/* time paramter 2 for select */
	char *db_cond; 							/* where clause for db request*/
	char *db_field_offset;			/* offset on LCD board for display */
	char *db_field_len; 				/* len to show */
	char *spec_fields; 					/* special fields */
	char *spec_field_rule;			/* rule for these fields*/
	char *spec_field_offset;		/* offset on LCD boards for this fields */
	char *spec_field_len;				/* len to display for special fields */
	char *repl_fields; 					/* fields to replace with full-texts */
	char *replacement; 					/* replacement for field */
	char *reference; 						/* connection between the fields */
	char *repl_table; 					/* table which is used for resolving */
	char *lang_fields; 	/* fields to replace with full-texts in local language */
	char *lang_replacement; 		/* replacement for field in local language */
	char *lang_reference; 			/* connection between the fields */
	char *lang_repl_table; /* table which is used for resolv the local language */
	char *date_fields; 					/* date fields */
	char *date_format; 					/* format of date fields */
	char *repl_board_letter;  	/* number of letter to replace */
	char *repl_char; 				  	/* char to use for replacement */
	char *extd_switch_jfno;     /* switch for display at first code share flno  */
	char *extd_jfno_offs;	/* position of code share flno inside output string */
	char *extd_jfno_len;	/* position of code share flno inside output string */
	char *extd_switch_rem;      /* switch for display extended remark inf.      */
	char *extd_sync;            /* switch for synchronize flno and remark       */
	char *extd_time_diff_jfno;  /* information for switch jfno information      */
	char *extd_time_diff_rem;   /* information for switch remark information    */
	char *extd_max_jfno;        /* inf. for how much code share flnos be shown  */
	char *extd_max_rem;         /* inf. for how much remarks would be shown     */
}CFG;

typedef struct
{
  time_t tlNextConnect ;
	time_t tlNextRefresh ;
	time_t tlNextPageSwitch ;
	time_t tlNextExtdFlnoChange ;
	time_t tlNextExtdRemarkChange ; 
	int ilLang;
	int ilGroupNr;
	int ilGroupRows;
	int ilMaxOffs;
	int ilLastLen;
	int ilValid;
	CFG rlCfg;
	int ilJfnoLen;
	int ilJfnoOffs;
	LPLISTHEADER prlBoards;
	LPLISTHEADER prlFlightInfo;
	char *pclReplacements;
	char *pclEngBuff;
	char *pclLocalBuff;
} GROUPINFO ;

typedef struct
{
	int ilSocket;
	unsigned long ilIp;
	int ilPort;
	int ilCols;
	int ilRows;
	int ilPlan;
	int ilAct;
}BOARDINFO;


typedef struct
{
	int  ilActivItem;
	char cpPJfno[JF_BUFF];
}CODE_SHARE_FLNO ;

typedef struct
{
	int  ilActivItem;
	char *cpPRem ;
}REMARKS ;


typedef struct
{
	long urno;
	int ilRow;
	BOARDINFO       *prlBoard;
	CODE_SHARE_FLNO prlCodeShareFlnos;
	REMARKS         *prlRemarks;
}FLIGHTINFO;
