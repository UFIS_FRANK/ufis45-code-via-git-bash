#ifndef _DEF_mks_version_gt2hdl_h
  #define _DEF_mks_version_gt2hdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_gt2hdl_h[] = "@(#) "UFIS_VERSION" $Id: Ufis/Aia/AIA_Server/Base/Server/Library/Inc/gt2hdl.h 1.1 2004/10/04 16:57:50SGT jim Exp  $";
#endif /* _DEF_mks_version_gt2hdl_h */

/******************************************************************************
*                                                                             *
*     GT2 API Definitions                                                     *
*                                                                             *
*     This file contains all structure and constant definitions               *
*     for the GT2 handler                                                     *
*                                                                             *
******************************************************************************/

#ifndef    GT2HDL_INCLUDED
#define    GT2HDL_INCLUDED     /* include only once */



/******************************************************************************
*                                                                             *
*     GT2 API type definitions                                                *
*                                                                             *
******************************************************************************/

typedef char  gtClassId;
typedef short gtObjectId;
typedef short gtFunctionId;

struct gtDosTime          /* MS-DOS time format (used in gtFiler) */
{ 
   unsigned Sec2   : 5;   /* Number of 2-second increments (0-29) */
   unsigned Min    : 6;   /* Minutes (0-59)                       */
   unsigned Hour   : 5;   /* Hours (0-23)                         */
   unsigned Day    : 5;   /* Day of Month (1-31)                  */
   unsigned Month  : 4;   /* Month (1-12)                         */
   unsigned Year   : 7;   /* Year (relative to 1980)              */
};



/******************************************************************************
*                                                                             *
*     gtClassId Values                                                        *
*                                                                             *
******************************************************************************/

#define gtFONT            (1)
#define gtTEXTSTYLE       (2)
#define gtTEXT            (3)
#define gtLINE            (4)
#define gtRECT            (5)
#define gtELLIPSE         (6)
#define gtPAGE            (7)
#define gtWINDOW          (8)
#define gtTABLE           (9)
#define gtGIF             (10)
#define gtMONITOR         (11)
#define gtTERMINAL        (12)
#define gtREPLY           (13)
#define gtFILER           (14)
#define gtAREA            (15)
#define gtSAWINDOW        (19)
#define gtMPEG_VIDEO      (20)
#define gtREAL_TIME_VIDEO (21)
#define gtTRUETYPEFONTDEFINE    (22)


/******************************************************************************
*                                                                             *
*     gtFunctionId Values                                                     *
*                                                                             *
******************************************************************************/

#define gtFONT_DEFINE                      (1)
#define gtFONT_UNDEFINE                    (0)

#define gtTRUE_TYPE_FONT_DEFINE           (1)
#define gtTRUE_TYPE_FONT_UNDEFINE       (0)
 
#define gtTEXTSTYLE_DEFINE                 (1)
#define gtTEXTSTYLE_UNDEFINE               (0)

#define gtTEXT_DEFINE                      (1)
#define gtTEXT_DEFINE_WITH_ATTRIBUTES      (2)
#define gtTEXT_UNDEFINE                    (0)
#define gtTEXT_PUT                         (21)
#define gtTEXT_PUT_FORMATTED               (22)
#define gtTEXT_PUT_COPY                    (23)
#define gtTEXT_SCROLL                      (24)

#define gtLINE_DEFINE                      (1)
#define gtLINE_UNDEFINE                    (0)

#define gtRECT_DEFINE                      (1)
#define gtRECT_UNDEFINE                    (0)

#define gtELLIPSE_DEFINE                   (1)
#define gtELLIPSE_UNDEFINE                 (0)

#define gtCOLLECTION_SET_CURRENT           (255)
#define gtTABLE_SET_CURRENT                (255)
#define gtWINDOW_SET_CURRENT               (255)
#define gtCOLLECTION_FREEZE_DRAWING        (254)
#define gtCOLLECTION_REDRAW                (253)
#define gtTABLE_BRING_TO_FRONT             (252)
#define gtCOLLECTION_BRING_TO_FRONT        (252)
#define gtWINDOW_BRING_TO_FRONT            (252)
#define gtCOLLECTION_SEND_TO_BACK          (251)
#define gtCOLLECTION_REFRESH               (249)
 
#define gtPAGE_DEFINE                      (1)
#define gtPAGE_UNDEFINE                    (0)
#define gtPAGE_DRAW                        (21)
#define gtPAGE_SET_COLOR                   (22)
#define gtPAGE_SET_BLINKING                (23)
#define gtPAGE_LOCK_COLOR                  (24)
#define gtPAGE_UNLOCK_COLORS               (25)
#define gtPAGE_CONVERT_PICTURES            (26)
#define gtPAGE_SET_VIDEO_MODE              (27)
#define gtPAGE_SET_CAROUSEL_TIME           (28)
#define gtPAGE_SET_TEXT_ITEM               (29)
#define gtPAGE_ADD_TEXT_CAROUSEL           (30)
#define gtPAGE_CLEAR_TEXT_CAROUSEL         (31)
#define gtPAGE_ADD_TEXT_CAROUSEL_EFFECT    (32)
#define gtPAGE_SET_TEXT_CAROUSEL_MODE      (33)
#define gtPAGE_SET_GIF_ITEM                (34)
#define gtPAGE_ADD_GIF_CAROUSEL            (35)
#define gtPAGE_CLEAR_GIF_CAROUSEL          (36)
#define gtPAGE_ADD_GIF_CAROUSEL_EFFECT     (37)
#define gtPAGE_SET_GIF_CAROUSEL_MODE       (38)

#define gtWINDOW_DEFINE                    (1)
#define gtWINDOW_UNDEFINE                  (0)




#define gtSAWINDOW_DEFINE                  (1)
#define gtSAWINDOW_UNDEFINE                (0)

#define gtTABLE_DEFINE                     (1)
#define gtTABLE_DEFINE_VIEW                (2)
#define gtTABLE_UNDEFINE                   (0)
#define gtTABLE_RESERVED1                  (21)
#define gtTABLE_ADJUST_COLUMN              (22)
#define gtTABLE_CLEAR_ROW                  (23)
#define gtTABLE_CLEAR_COLUMN               (24)
#define gtTABLE_DELETE_ROW                 (25)
#define gtTABLE_RESERVED2                  (26)
#define gtTABLE_INSERT_ROW                 (27)
#define gtTABLE_RESERVED3                  (28)
#define gtTABLE_PUT                        (29)
#define gtTABLE_CAROUSEL                   (30)
#define gtTABLE_CAROUSEL_EFFECT            (31)

#define gtAREA_DEFINE                      (1)
#define gtAREA_UNDEFINE                    (0)
#define gtAREA_SET                         (21)
#define gtAREA_ADD_CAROUSEL                (22)
#define gtAREA_CLEAR_CAROUSEL              (23)
#define gtAREA_ADD_CAROUSEL_EFFECT         (24)

#define gtGIF_DEFINE                       (1)
#define gtGIF_UNDEFINE                     (0)
#define gtGIF_PUT                          (21)
#define gtGIF_PUT_FILE                     (22)
#define gtGIF_PUT_COPY                     (23)
#define gtGIF_PUT_TMP_FILE                 (24)
#define gtGIF_ADD                          (25)
#define gtGIF_ADD_FILE                     (26)
#define gtGIF_ADD_COPY                     (27)
#define gtGIF_ADD_TMP_FILE                 (28)

#define gtMPEG_VIDEO_DEFINE                (1)
#define gtMPEG_VIDEO_UNDEFINE              (0)
#define gtMPEG_VIDEO_PLAY                  (21)
#define gtMPEG_VIDEO_STOP                  (22)
#define gtMPEG_VIDEO_RESCALE               (23)
#define gtMPEG_VIDEO_BRIGHTNESS            (24)
#define gtMPEG_VIDEO_STATUS_REQUEST        (25)
#define gtMPEG_VIDEO_HOLD                  (26)
#define gtMPEG_VIDEO_CONTINUE              (27)

#define gtREAL_TIME_VIDEO_DEFINE           (1)
#define gtREAL_TIME_VIDEO_UNDEFINE         (0)
#define gtREAL_TIME_VIDEO_PLAY             (21)
#define gtREAL_TIME_VIDEO_STOP             (22)
#define gtREAL_TIME_VIDEO_RESCALE          (23)
#define gtREAL_TIME_VIDEO_BRIGHTNESS       (24)
#define gtREAL_TIME_VIDEO_STATUS_REQUEST   (25)
#define gtREAL_TIME_VIDEO_ADJUST_VIDEO     (26)

#define gtMONITOR_BRIGHTNESS               (21)
#define gtMONITOR_CONTRAST                 (22)
#define gtMONITOR_STANDBY                  (23)
#define gtMONITOR_DEGAUSS                  (24)
#define gtMONITOR_WHITE_POINT              (25)
#define gtMONITOR_STATUS_REQUEST           (26)
#define gtMONITOR_SERVICE_MESSAGE          (27)

#define gtTERMINAL_INIT                    (21)
#define gtTERMINAL_SET_INTEGRITY           (22)
#define gtTERMINAL_GET_INTEGRITY           (23)
#define gtTERMINAL_SET_LOG_LEVEL           (24)
#define gtTERMINAL_STATUS_REQUEST          (25)
#define gtTERMINAL_SET_DEFAULT_VIDEO_MODE  (26)
#define gtTERMINAL_CAROUSEL                (27)
#define gtTERMINAL_SET_TEXT_ITEM           (28)
#define gtTERMINAL_SET_KEYBOARD_MODE       (29)
#define gtTERMINAL_SET_SYSTEM_MESSAGE      (30)
#define gtTERMINAL_SET_BLINKING_RATE       (31)
#define gtTERMINAL_SET_STARTUP_PAGE        (32)
#define gtTERMINAL_SET_OFFLINE_PAGE        (33)
#define gtTERMINAL_ADD_PAGE_CAROUSEL       (34)
#define gtTERMINAL_CLEAR_PAGE_CAROUSEL     (35)
#define gtTERMINAL_PREVENT_CLEAR           (36)
#define gtTERMINAL_INVERSE_IMAGE           (37)
#define gtTERMINAL_ADD_PAGE_CAROUSEL_EFFECT (38)    /* New for ver. 3.00 */
#define gtTERMINAL_SCREENSAVER             (39)
#define gtTERMINAL_DUMP                     (40)    /* New for ver. 3.00 */
#define gtTERMINAL_SET_OPTIONS              (41)    /* New for ver. 3.00 */
#define gtTERMINAL_SET_DEFAULT_VOLUME       (42)    /* New for ver. 3.00 */
#define gtTERMINAL_SET_DEFAULT_BASS_TREBLE  (43)    /* New for ver. 3.00 */
#define gtTERMINAL_HOLD_CAROUSEL            (44)    /* New for ver. 3.00 */
#define gtTERMINAL_CONTINUE_CAROUSEL        (45)    /* New for ver. 3.00 */
#define gtTERMINAL_SCREEN_DUMP              (46)    /* New for ver. 3.03 */
#define gtTERMINAL_GET_CONFIGURATION        (47)    /* New for ver. 3.xx */
#define gtTERMINAL_SET_CONFIGURATION        (48)    /* New for ver. 3.xx */
#define gtTERMINAL_GET_OPERATING_SYSTEM     (49)    /* New for ver. 3.03 */




#define gtREPLY_LOG                              (21)
#define gtREPLY_ERROR                            (22)
#define gtREPLY_MONITOR_STATUS_REQUEST           (23)
#define gtREPLY_MONITOR_SERVICE_MESSAGE          (24)
#define gtREPLY_TERMINAL_STATUS_REQUEST          (25)
#define gtREPLY_TERMINAL_INTEGRITY               (26)
#define gtREPLY_FILER_OPERATION                  (27)
#define gtREPLY_FILER_INFO                       (28)
#define gtREPLY_FILER_STATUS_REQUEST             (29)
#define gtREPLY_PAGE                             (30)
#define gtREPLY_KEY                              (31)
#define gtREPLY_MPEG_VIDEO_STATUS_REQUEST        (32)
#define gtREPLY_REAL_TIME_VIDEO_STATUS_REQUEST   (33)
#define gtREPLY_VIDEO_ERROR						 (34)    /* New for ver. 1.52 */
#define gtREPLY_TEMPERATURE_ERROR                (35)    /* New for ver. 1.54 */
#define gtREPLY_FAN_ERROR                        (36)    /* New for ver. 1.54 */
#define gtREPLY_TERMINAL_DUMP                    (37)    /* New for ver. 3.00 */
#define gtREPLY_FILER_DATA                       (38)    /* New for ver. 3.00 */
#define gtREPLY_TERMINAL_OPERATING_SYSTEM        (39)    /* New for ver. 3.03 */
#define gtREPLY_DEBUG_REGISTER                  (100)    /*** debug only ***/




#define gtFILER_SET_OUT_OF_DATE_CHECKING   (21)
#define gtFILER_PUT_FILE                   (22)
#define gtFILER_PUT_DATA                   (23)
#define gtFILER_FIND_FIRST                 (24)
#define gtFILER_FIND_NEXT                  (25)
#define gtFILER_CREATE_DIRECTORY           (26)
#define gtFILER_REMOVE_DIRECTORY           (27)
#define gtFILER_REMOVE_FILE                (28)
#define gtFILER_STATUS_REQUEST             (29)
#define gtFILER_SET_RESPONSE_LEVELS         (30)       /* New for ver. 3.00 */
#define gtFILER_GET_FILE                    (31)       /* New for ver. 3.00 */
#define gtFILER_GET_DATA                    (32)       /* New for ver. 3.00 */





#define gtSTYLE_DEFINE                      (1)        /* New for ver. 3.00 */
#define gtSTYLE_UNDEFINE                    (0)        /* New for ver. 3.00 */
#define gtSTYLE_SET_SOLID_FILL              (21)       /* New for ver. 3.00 */
#define gtSTYLE_SET_GRADIENT_FILL           (22)       /* New for ver. 3.00 */
#define gtSTYLE_SET_IMAGE_FILL              (23)       /* New for ver. 3.00 */
#define gtSTYLE_SET_DROP_SHADOW             (24)       /* New for ver. 3.00 */
#define gtSTYLE_SET_TRANSPARENCY            (25)       /* New for ver. 3.00 */
#define gtSTYLE_SET_OUTLINE                 (26)       /* New for ver. 3.00 */
#define gtSTYLE_SET_EMBOSS                  (27)       /* New for ver. 3.00 */
#define gtSTYLE_SET_BEVEL                   (28)       /* New for ver. 3.00 */
#define gtSTYLE_SET_3D_OUTLINE              (29)       /* New for ver. 3.00 */
#define gtSTYLE_SET_SPACING                 (30)       /* New for ver. 3.00 */

#define gtBROWSER_DEFINE                    (1)        /* Only supported in Windows version */
#define gtBROWSER_UNDEFINE                  (0)        /* Only supported in Windows version */
#define gtBROWSER_SHOW_URL                  (21)       /* Only supported in Windows version */

#define gtDISPLAY_GROUP_DEFINE              (1)        /* New for ver. 3.00 */
#define gtDISPLAY_GROUP_UNDEFINE            (0)        /* New for ver. 3.00 */

#define gtEFFECT_DEFINE                     (1)
#define gtEFFECT_UNDEFINE                   (0)
#define gtEFFECT_SET_WIPING                 (21)
#define gtEFFECT_SET_FADING                 (22)
#define gtEFFECT_SET_MOVING                 (23)
#define gtEFFECT_SET_SCALING                (24)

#define gtREQUEST_FILE_REQUEST              (21)

#define gtDEBUG_REGISTER                    (1)        /*** debug only ***/

/******************************************************************************
*                                                                             *
*     Global GT Objects                                                       *
*                                                                             *
******************************************************************************/

#define gtSYSTEM_FONT        (-2)   /* Uses 8x16 character cell,          */ 
                                    /* fixed spacing, char descender = 3  */
#define gtSYSTEM_TEXTSTYLE   (-2)   /* Uses gtSYSTEMFONT, white on black, */
                                    /* left alligned, base alligned       */
                                    /* normal face                        */
#define gtSYSTEM_MONITOR     (-2)
#define gtSYSTEM_TERMINAL    (-2)
#define gtSYSTEM_FILER       (-2)

#define gtSYSTEM_MPEG_OBJECT (-1)

/******************************************************************************
*                                                                             *
*     Font Facing Attributes                                                  *
*                                                                             *
******************************************************************************/

#define gtNORMAL             (0)
#define gtBOLD               (1)
#define gtUNDERLINE          (4)
#define gtTRANSPARENT        (32)
#define gtRIGHT_TO_LEFT		 (128)     /* New for ver. 3.00 */
#define gtSMOOTH             (256)


/******************************************************************************
*                                                                             *
*     Text Alignments                                                         *
*                                                                             *
******************************************************************************/

#define gtLEFT               (1)
#define gtCENTER             (2)
#define gtRIGHT              (4)

#define gtBOTTOM             (1)
#define gtBASE               (2)
#define gtMIDDLE             (4)
#define gtTOP                (8)



/******************************************************************************
*                                                                             *
*     Linestyles                                                              *
*                                                                             *
******************************************************************************/

#define gtSOLID_LINE         ((short)0xffff)
#define gtDASHED_LINE        ((short)0xff00)
#define gtDOTTED_LINE        ((short)0xcccc)



/******************************************************************************
*                                                                             *
*     First 16 Default Colors                                                 *
*                                                                             *
******************************************************************************/

                                /* Red   Green Blue */
#define gtBLACK          (0)    /* 0x00  0x00  0x00 */
#define gtBLUE           (1)    /* 0x00  0x00  0xab */
#define gtGREEN          (2)    /* 0x00  0xab  0x00 */
#define gtCYAN           (3)    /* 0x00  0xab  0xab */
#define gtRED            (4)    /* 0xab  0x00  0x00 */
#define gtMAGENTA        (5)    /* 0xab  0x00  0xab */
#define gtBROWN          (6)    /* 0xab  0x57  0x00 */
#define gtWHITE          (7)    /* 0xab  0xab  0xab */
#define gtGRAY           (8)    /* 0x57  0x57  0x57 */
#define gtLIGHTBLUE      (9)    /* 0x57  0x57  0xff */
#define gtLIGHTGREEN     (10)   /* 0x57  0xff  0x57 */
#define gtLIGHTCYAN      (11)   /* 0x57  0xff  0xff */
#define gtLIGHTRED       (12)   /* 0xff  0x57  0x57 */
#define gtLIGHTMAGENTA   (13)   /* 0xff  0x57  0xff */
#define gtYELLOW         (14)   /* 0xff  0xff  0x57 */
#define gtLIGHTWHITE     (15)   /* 0xff  0xff  0xff */



/*------------------------------------------------------------------------*/
/*                                Named colors                            */
/*------------------------------------------------------------------------*/


#define gtCOLOR_ALICEBLUE            (1)
#define gtCOLOR_ANTIQUEWHITE         (2)
#define gtCOLOR_AQUA                 (3)
#define gtCOLOR_AQUAMARINE           (4)
#define gtCOLOR_AZURE                (5)
#define gtCOLOR_BEIGE                (6)
#define gtCOLOR_BISQUE               (7)
#define gtCOLOR_BLACK                (8)
#define gtCOLOR_BLANCHEDALMOND       (9)
#define gtCOLOR_BLUE                 (10)
#define gtCOLOR_BLUEVIOLET           (11)
#define gtCOLOR_BROWN                (12)
#define gtCOLOR_BURLYWOOD            (13)
#define gtCOLOR_CADETBLUE            (14)
#define gtCOLOR_CHARTREUSE           (15)
#define gtCOLOR_CHOCOLATE            (16)
#define gtCOLOR_CORAL                (17)
#define gtCOLOR_CORNFLOWERBLUE       (18)
#define gtCOLOR_CORNSILK             (19)
#define gtCOLOR_CRIMSON              (20)
#define gtCOLOR_CYAN                 (21)
#define gtCOLOR_DARKBLUE             (22)
#define gtCOLOR_DARKCYAN             (23)
#define gtCOLOR_DARKGOLDENROD        (24)
#define gtCOLOR_DARKGRAY             (25)
#define gtCOLOR_DARKGREEN            (26)
#define gtCOLOR_DARKKHAKI            (27)
#define gtCOLOR_DARKMAGENTA          (28)
#define gtCOLOR_DARKOLIVEGREEN       (29)
#define gtCOLOR_DARKORANGE           (30)
#define gtCOLOR_DARKORCHID           (31)
#define gtCOLOR_DARKRED              (32)
#define gtCOLOR_DARKSALMON           (33)
#define gtCOLOR_DARKSEAGREEN         (34)
#define gtCOLOR_DARKSLATEBLUE        (35)
#define gtCOLOR_DARKSLATEGRAY        (36)
#define gtCOLOR_DARKTURQUOISE        (37)
#define gtCOLOR_DARKVIOLET           (38)
#define gtCOLOR_DEEPPINK             (39)
#define gtCOLOR_DEEPSKYBLUE          (40)
#define gtCOLOR_DIMGRAY              (41)
#define gtCOLOR_DODGERBLUE           (42)
#define gtCOLOR_FIREBRICK            (43)
#define gtCOLOR_FLORALWHITE          (44)
#define gtCOLOR_FORESTGREEN          (45)
#define gtCOLOR_FUCHSIA              (46)
#define gtCOLOR_GAINSBORO            (47)
#define gtCOLOR_GHOSTWHITE           (48)
#define gtCOLOR_GOLD                 (49)
#define gtCOLOR_GOLDENROD            (50)
#define gtCOLOR_GRAY                 (51)
#define gtCOLOR_GREEN                (52)
#define gtCOLOR_GREENYELLOW          (53)
#define gtCOLOR_HONEYDEW             (54)
#define gtCOLOR_HOTPINK              (55)
#define gtCOLOR_INDIANRED            (56)
#define gtCOLOR_INDIGO               (57)
#define gtCOLOR_IVORY                (58)
#define gtCOLOR_KHAKI                (59)
#define gtCOLOR_LAVENDER             (60)
#define gtCOLOR_LAVENDERBLUSH        (61)
#define gtCOLOR_LAWNGREEN            (62)
#define gtCOLOR_LEMONCHIFFON         (63)
#define gtCOLOR_LIGHTBLUE            (64)
#define gtCOLOR_LIGHTCORAL           (65)
#define gtCOLOR_LIGHTCYAN            (66)
#define gtCOLOR_LIGHTGOLDENRODYELLOW (67)
#define gtCOLOR_LIGHTGREEN           (68)
#define gtCOLOR_LIGHTGREY            (69)
#define gtCOLOR_LIGHTPINK            (70)
#define gtCOLOR_LIGHTSALMON          (71)
#define gtCOLOR_LIGHTSEAGREEN        (72)
#define gtCOLOR_LIGHTSKYBLUE         (73)
#define gtCOLOR_LIGHTSLATEGRAY       (74)
#define gtCOLOR_LIGHTSTEELBLUE       (75)
#define gtCOLOR_LIGHTYELLOW          (76)
#define gtCOLOR_LIME                 (77)
#define gtCOLOR_LIMEGREEN            (78)
#define gtCOLOR_LINEN                (79)
#define gtCOLOR_MAGENTA              (80)
#define gtCOLOR_MAROON               (81)
#define gtCOLOR_MEDIUMAQUAMARINE     (82)
#define gtCOLOR_MEDIUMBLUE           (83)
#define gtCOLOR_MEDIUMORCHID         (84)
#define gtCOLOR_MEDIUMPURPLE         (85)
#define gtCOLOR_MEDIUMSEAGREEN       (86)
#define gtCOLOR_MEDIUMSLATEBLUE      (87)
#define gtCOLOR_MEDIUMSPRINGGREEN    (88)
#define gtCOLOR_MEDIUMTURQUOISE      (89)
#define gtCOLOR_MEDIUMVIOLETRED      (90)
#define gtCOLOR_MIDNIGHTBLUE         (91)
#define gtCOLOR_MINTCREAM            (92)
#define gtCOLOR_MISTYROSE            (93)
#define gtCOLOR_MOCCASIN             (94)
#define gtCOLOR_NAVAJOWHITE          (95)
#define gtCOLOR_NAVY                 (96)
#define gtCOLOR_NAVYBLUE             (97)
#define gtCOLOR_OLDLACE              (98)
#define gtCOLOR_OLIVE                (99)
#define gtCOLOR_OLIVEDRAB            (100)
#define gtCOLOR_ORANGE               (101)
#define gtCOLOR_ORANGERED            (102)
#define gtCOLOR_ORCHID               (103)
#define gtCOLOR_PALEGOLDENROD        (104)
#define gtCOLOR_PALEGREEN            (105)
#define gtCOLOR_PALETURQUOISE        (106)
#define gtCOLOR_PALEVIOLETRED        (107)
#define gtCOLOR_PAPAYAWHIP           (108)
#define gtCOLOR_PEACHPUFF            (109)
#define gtCOLOR_PERU                 (110)
#define gtCOLOR_PINK                 (111)
#define gtCOLOR_PLUM                 (112)
#define gtCOLOR_POWDERBLUE           (113)
#define gtCOLOR_PURPLE               (114)
#define gtCOLOR_RED                  (115)
#define gtCOLOR_ROSYBROWN            (116)
#define gtCOLOR_ROYALBLUE            (117)
#define gtCOLOR_SADDLEBROWN          (118)
#define gtCOLOR_SALMON               (119)
#define gtCOLOR_SANDYBROWN           (120)
#define gtCOLOR_SEAGREEN             (121)
#define gtCOLOR_SEASHELL             (122)
#define gtCOLOR_SIENNA               (123)
#define gtCOLOR_SILVER               (124)
#define gtCOLOR_SKYBLUE              (125)
#define gtCOLOR_SLATEBLUE            (126)
#define gtCOLOR_SLATEGRAY            (127)
#define gtCOLOR_SNOW                 (128)
#define gtCOLOR_SPRINGGREEN          (129)
#define gtCOLOR_STEELBLUE            (130)
#define gtCOLOR_TAN                  (131)
#define gtCOLOR_TEAL                 (132)
#define gtCOLOR_THISTLE              (133)
#define gtCOLOR_TOMATO               (134)
#define gtCOLOR_TURQUOISE            (135)
#define gtCOLOR_VIOLET               (136)
#define gtCOLOR_WHEAT                (137)
#define gtCOLOR_WHITE                (138)
#define gtCOLOR_WHITESMOKE           (139)
#define gtCOLOR_YELLOW               (140)
#define gtCOLOR_YELLOWGREEN          (141)



/******************************************************************************
*                                                                             *
*     GT Screen Effects                                                       *
*                                                                             *
******************************************************************************/

#define gtEFFECT_NONE    (0)
#define gtEFFECT_UP      (1)
#define gtEFFECT_DOWN    (2)
#define gtEFFECT_LEFT    (4)
#define gtEFFECT_RIGHT   (8)
#define gtEFFECT_ROLL_IN    (0x0010)            /* New for ver. 1.40 */
#define gtEFFECT_ROLL_OUT   (0x0020)            /* New for ver. 1.40 */
#define gtEFFECT_CROSSFADE  (0x0080)            /* New for ver. 1.40 */
#define gtEFFECT_FADE_IN    (0x0100)            /* New for ver. 3.02 */
#define gtEFFECT_FADE_OUT   (0x0200)            /* New for ver. 3.02 */
#define gtEFFECT_INVERT     (0x4000)            /* Invert directions */
#define gtEFFECT_USE_ID     (0x8000)            /* Use predefined effect */

#define gtCROSSFADE_DEFAULT (0x0040)            /* New for ver. 1.40 */

/*------------------------------------------------------------------------*/
/*                          Pagecarousel options                          */
/*------------------------------------------------------------------------*/
#define TRIGGER_ON_MPEG_FINISHED    (-2)

/*------------------------------------------------------------------------*/
/*                          Gradient options                              */
/*------------------------------------------------------------------------*/
#define gtGRADIENT_LEFTTORIGHT (1)
#define gtGRADIENT_TOPTOBOTTOM (0)
#define gtGRADIENT_CIRCULAR    (2)
#define gtGRADIENT_DIAGONAL    (4)
#define gtGRADIENT_HYPERBOLIC  (3)
#define gtGRADIENT_CORNER      (8)
#define gtGRADIENT_THREECOLORS (16)
#define gtGRADIENT_TWOCOLORS   (0)
#define gtGRADIENT_RADIAL      (32)


/******************************************************************************
*                                                                             *
*     GT Blinking Rates                                                       *
*                                                                             *
******************************************************************************/

#define gtBLINK_SLOW     (0)
#define gtBLINK_NORMAL   (1)
#define gtBLINK_FAST     (2)



/******************************************************************************
*                                                                             *
*     GT Logging Levels                                                       *
*                                                                             *
******************************************************************************/

#define gtLOG_LEVEL_ALL         (0xff)
#define gtLOG_LEVEL_DEBUG       (0x80)
#define gtLOG_LEVEL_INFO        (0x40)
#define gtLOG_LEVEL_NOTICE      (0x20)
#define gtLOG_LEVEL_WARNING     (0x10)
#define gtLOG_LEVEL_ERROR       (0x08)
#define gtLOG_LEVEL_CRITICAL    (0x04)
#define gtLOG_LEVEL_ALERT       (0x02)
#define gtLOG_LEVEL_EMERGENCY   (0x01)
#define gtLOG_LEVEL_NONE        (0x00)



/******************************************************************************
*                                                                             *
*     GT Video Modes                                                          *
*                                                                             *
******************************************************************************/
#if 0
#define gtV640x480x256          (0x0101)
#define gtV800x600x256          (0x0103)
#define gtV800x600x64K          (0x0114)
#define gtV1024x768x256         (0x0105)
#define gtV1024x576x256         (0x1105)
#define gtV856x480x256          (0x1101)
#endif
#define gtV640x480x256          (0x0101)
#define gtV856x480x256          (0x4101)
#define gtV664x249x256          (0x2101)
#define gtV664x498x256          (0x3101)
#define gtV848x480x256          (0x4101)
#define gtV480x848x256          (0x7101)
#define gtV640x480x256i         (0x8101)
#define gtV800x600x256          (0x0103)
#define gtV800x600x64K          (0x0114)
#define gtV800x450x64K          (0x1114)
#define gtV1024x768x256         (0x0105)
#define gtV1024x576x256         (0x1105)

/******************************************************************************
*                                                                             *
*     Keyboard Functions                                                      *
*                                                                             *
******************************************************************************/

#define gtKEYBOARD_TABLE_SCROLL    (0)
#define gtKEYBOARD_AREA_SCROLL     (1)
#define gtKEYBOARD_PAGE_BROWSE     (2)
#define gtKEYBOARD_PAGE_RECALL     (3)
#define gtKEYBOARD_FUNCTION_KEYS   (4)



/******************************************************************************
*                                                                             *
*     System Messages                                                         *
*                                                                             *
******************************************************************************/

#define gtSYSTEM_MESSAGE_SUNDAY      (0)
#define gtSYSTEM_MESSAGE_MONDAY      (1)
#define gtSYSTEM_MESSAGE_TUESDAY     (2)
#define gtSYSTEM_MESSAGE_WEDNESDAY   (3)
#define gtSYSTEM_MESSAGE_THURSDAY    (4)
#define gtSYSTEM_MESSAGE_FRIDAY      (5)
#define gtSYSTEM_MESSAGE_SATURDAY    (6)

#define gtSYSTEM_MESSAGE_JANUARY     (7)
#define gtSYSTEM_MESSAGE_FEBRUARY    (8)
#define gtSYSTEM_MESSAGE_MARCH       (9)
#define gtSYSTEM_MESSAGE_APRIL       (10)
#define gtSYSTEM_MESSAGE_MAY         (11)
#define gtSYSTEM_MESSAGE_JUNE        (12)
#define gtSYSTEM_MESSAGE_JULY        (13)
#define gtSYSTEM_MESSAGE_AUGUST      (14)
#define gtSYSTEM_MESSAGE_SEPTEMBER   (15)
#define gtSYSTEM_MESSAGE_OCTOBER     (16)
#define gtSYSTEM_MESSAGE_NOVEMBER    (17)
#define gtSYSTEM_MESSAGE_DECEMBER    (18)

#define gtSYSTEM_MESSAGE_PAGE_RECALL (1000)



/******************************************************************************
*                                                                             *
*     gtFILER Constants                                                       *
*                                                                             *
******************************************************************************/

#define gtFILER_OK            (0)
#define gtFILER_END           (1)
#define gtFILER_OUT_OF_DATE   (2)
#define gtFILER_ERROR         (3)



/*------------------------------------------------------------------------*/
/*                           gtMpegVideo constants                        */
/*------------------------------------------------------------------------*/
/* treatment*/
#define gtMPEG_VIDEO_DEFAULT                  (0)         /* New for ver. 1.50 */ 
#define gtMPEG_VIDEO_AUDIO                    (1)         /* New for ver. 1.50 */
#define gtMPEG_VIDEO_NO_RESTART               (2)         /* New for ver. 1.50 */
#define gtMPEG_VIDEO_CLOSE_AT_END             (4)         /* New for ver. 1.50 */
#define gtMPEG_VIDEO_NO_WINDOW                (8)         /* New for ver. 3.00 */
/* type of status request */
#define gtMPEG_VIDEO_PREPARE_STATUS           (0)         /* New for ver. 1.50 */ 
#define gtMPEG_VIDEO_PLAY_STATUS              (1)         /* New for ver. 1.50 */ 
/* status */
#define gtMPEG_VIDEO_PREPARE_NO_RESULT_AVAIL  (1)         /* New for ver. 1.50 */ 
#define gtMPEG_VIDEO_PREPARE_IN_PROGRESS      (2)         /* New for ver. 1.50 */ 
#define gtMPEG_VIDEO_PREPARE_ENDED            (3)         /* New for ver. 1.50 */ 
#define gtMPEG_VIDEO_PREPARE_ABORTED          (4)         /* New for ver. 1.50 */ 
#define gtMPEG_VIDEO_PREPARE_UNRECOV_ERROR    (5)         /* New for ver. 1.50 */ 

#define gtMPEG_VIDEO_PLAY_NO_RESULT_AVAIL     (11)        /* New for ver. 1.50 */ 
#define gtMPEG_VIDEO_PLAY_IN_PROGRESS         (12)        /* New for ver. 1.50 */ 
#define gtMPEG_VIDEO_PLAY_STOPPED             (13)        /* New for ver. 1.50 */ 
#define gtMPEG_VIDEO_PLAY_HOLDED              (14)        /* New for ver. 1.50 */ 
#define gtMPEG_VIDEO_PLAY_UNRECOV_ERROR       (15)        /* New for ver. 1.50 */ 

/*------------------------------------------------------------------------*/
/*                          gtFilerGetFile options                        */
/*------------------------------------------------------------------------*/
#define gtFILER_NO_HANDSHAKE            (1)
#define gtFILER_VOLATILE                (2)

/*------------------------------------------------------------------------*/
/*                          gtFilerGetFile options                        */
/*------------------------------------------------------------------------*/
#define gtVOLATILE                      (1)
#define gtNON_VOLATILE                  (0)


/*------------------------------------------------------------------------*/
/*                          GT filer reply levels                         */
/*------------------------------------------------------------------------*/
#define gtRESPONSE_NONE                  (0x0000)
#define gtRESPONSE_TRANSFER_STARTED      (0x0001)
#define gtRESPONSE_TRANSFER_ERROR        (0x0002)
#define gtRESPONSE_TRANSFER_BUSY         (0x0004)
#define gtRESPONSE_TRANSFER_FINISHED     (0x0008)
#define gtRESPONSE_ALL                   (0xffff)

/*------------------------------------------------------------------------*/
/*               temperature error reply constants                        */
/*------------------------------------------------------------------------*/
#define gtSAFE_TEMPERATURE               (0)
#define gtCRITICAL_TEMPERATURE           (1)
#define gtALERT_TEMPERATURE              (2)

/*------------------------------------------------------------------------*/
/*                           Video constants                              */
/*------------------------------------------------------------------------*/
#define gtHORIZONTAL_INTERPOLATION       (0x0001)
#define gtVERTICAL_INTERPOLATION         (0x0100)

/*------------------------------------------------------------------------*/
/*                      Flags used for text effects                       */
/*------------------------------------------------------------------------*/
#define gtDOMINANCE_NONE                 (0)
#define gtDOMINANCE_COLOR1               (1)
#define gtDOMINANCE_COLOR2               (2)

/*------------------------------------------------------------------------*/
/*        Default value for gtFontSetSpacing and gtStyleSetSpacing        */
/*------------------------------------------------------------------------*/
#define gtLINESPACING_DEFAULT            (0)
#define gtCHARSPACING_DEFAULT            (0x7FFF)

/*------------------------------------------------------------------------*/
/*                    Flags for gtTrueTypeGFontDefine                     */
/*------------------------------------------------------------------------*/

#define gtTRUETYPE_FLAG_NO_HINTING       (1)


/*------------------------------------------------------------------------*/
/*                     DisplayGroup monitor selections                    */
/*------------------------------------------------------------------------*/
#define gtMONITOR_1     (0x0001)
#define gtMONITOR_2     (0x0002)
#define gtMONITOR_3     (0x0004)
#define gtMONITOR_4     (0x0008)
#define gtMONITOR_5     (0x0010)
#define gtMONITOR_6     (0x0020)
#define gtMONITOR_7     (0x0040)
#define gtMONITOR_8     (0x0080)
#define gtMONITOR_9     (0x0100)

#define gtSCREEN_CONFIG_1_1     (0x00)
#define gtSCREEN_CONFIG_1_2     (0x01)
#define gtSCREEN_CONFIG_1_3     (0x02)
#define gtSCREEN_CONFIG_1_4     (0x03)
#define gtSCREEN_CONFIG_1_5     (0x04)
#define gtSCREEN_CONFIG_1_6     (0x05)
#define gtSCREEN_CONFIG_1_7     (0x06)
#define gtSCREEN_CONFIG_1_8     (0x07)
#define gtSCREEN_CONFIG_1_9     (0x08)
#define gtSCREEN_CONFIG_2_1     (0x09)
#define gtSCREEN_CONFIG_2_2     (0x0a)
#define gtSCREEN_CONFIG_2_3     (0x0b)
#define gtSCREEN_CONFIG_2_4     (0x0c)
#define gtSCREEN_CONFIG_3_1     (0x0d)
#define gtSCREEN_CONFIG_3_2     (0x0e)
#define gtSCREEN_CONFIG_3_3     (0x0f)
#define gtSCREEN_CONFIG_4_1     (0x10)
#define gtSCREEN_CONFIG_4_2     (0x11)
#define gtSCREEN_CONFIG_5_1     (0x12)
#define gtSCREEN_CONFIG_6_1     (0x13)
#define gtSCREEN_CONFIG_7_1     (0x14)
#define gtSCREEN_CONFIG_8_1     (0x15)
#define gtSCREEN_CONFIG_9_1     (0x16)

/******************************************************************************
*                                                                             *
*     gtRealTimeVideo Constants                                               *
*                                                                             *
******************************************************************************/

#define gtREAL_TIME_VIDEO_TUNER       (0)
#define gtREAL_TIME_VIDEO_CVBS        (1)

#define gtREAL_TIME_VIDEO_TUNED       (1)
#define gtREAL_TIME_VIDEO_NOT_TUNED   (2)



/******************************************************************************
*                                                                             *
*     GT Error Codes (gtREPLY_ERROR)                                          *
*                                                                             *
******************************************************************************/

#define gtERROR_NON_EXISTING_OBJECT            (1) /* Message Related Errors */
#define gtERROR_WORKPAGE_NEEDED                (2)
#define gtERROR_INVALID_CLASS_OR_FUNCTION_ID   (3)
#define gtERROR_WRONG_CLASS_ID                 (4)
#define gtERROR_INVALID_PARAMETER_LIST         (5)
#define gtERROR_FUNCTION_NOT_ALLOWED           (6)
#define gtERROR_PUT_FAILED                     (7)
#define gtERROR_VIDEO_FAILED                   (8)

#define gtERROR_MONITOR_DOWN                (1001) /* Monitor Related Errors */
#define gtERROR_MONITOR_UP                  (1002)
#define gtERROR_MONITOR_BRIGHTNESS          (1003)
#define gtERROR_MONITOR_CONTRAST            (1004)
#define gtERROR_MONITOR_WHITEPOINT          (1005)
#define gtERROR_MONITOR_STANDBY             (1006)
#define gtERROR_MONITOR_DEGAUSS             (1007)
#define gtERROR_MONITOR_STATUS_REQUEST      (1008)
#define gtERROR_MONITOR_SERVICE_MESSAGE     (1009)

#define gtERROR_OUT_OF_MEMORY               (9001)
#define gtERROR_REPLY_FAILED                (9002)



/******************************************************************************
*                                                                             *
*     General Constants                                                       *
*                                                                             *
******************************************************************************/

#define MAX_PARAM         12        /* Max Number of Message Parameters */
#define MAX_BUF_SIZE      512000    /* Max Buffer Size                  */
#define MAX_FIL_SIZE      500000    /* Max File Size                    */
#define MAX_RCV_SIZE      512       /* Max Receive Buffer Size          */
#undef  PACKET_LEN
#ifndef PACKET_LEN
#define PACKET_LEN        4096     /* Max Packet Size   1024               */
#endif
#define DATA_PACKET_LEN   4096 - 10   /* Max Data Packet Size  1024-10           */



/******************************************************************************
*                                                                             *
*     Command List Structure                                                  *
*                                                                             *
******************************************************************************/

typedef struct _COM_LIST_STRUCT
{
   char           commandId[64];      /* Command              */
   unsigned char  gtClassId;          /* Class Id             */
   unsigned short gtFunctionId;       /* Function Id          */
   short          noParam;            /* Number of Parameters */
   short          param[MAX_PARAM];   /* Parameters           */
}COM_LIST_STRUCT;



/******************************************************************************
*                                                                             *
*     Constant Name Structure                                                 *
*                                                     
                        *
******************************************************************************/

typedef struct _CONST_NAME_STRUCT
{
   char  constName[64];   /* Constant Name  */
   short constValue;      /* Constant Value */
}CONST_NAME_STRUCT;



extern int CheckSocket(int ipSocket,int ipSet,long lpSec, long lpMsec);
extern int gtBuildCommand(char *pcpInd, char *pcpCommand, char *pcpObjectId,
                   int ipNoParam, char *pcpParam, char *pcpFilNam,
                   int ipSocket, char *pcpResBuf);

#endif











