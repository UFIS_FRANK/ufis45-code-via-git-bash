using System;
using System.Drawing;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
//using System.Reflection;
using Microsoft.Win32;
using System.Data;
using Ufis.Utils;
using Ufis.Data;

namespace Data_Changes
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmMain : System.Windows.Forms.Form
	{
		#region ------ MyRegion

		private frmData DataForm = null;
		public string [] args;
		private IDatabase myDB = null;
		private ITable logTAB = null;
		private bool myIsTimeInUtc = true;
		private bool myIsInit = false;
		private bool myboolDoingLogtab = false;
		private string myStrWhereTimeframe = "";
		private string myStrWhereALOCIn = "";
		private string myStrCcaWhereALOCIn = "";
		private string myStrWhereFLNUIn = "";
		private string myStrCcaWhereFLNUIn = "";
		private string myStrSortOrder = " ORDER BY TIME ASC, SEQN ASC";
		private string mySelection = "";
		private string mySelectionLOGTABKeyf = "";
		private string m_ViewMode = "LOCATIONS";
		private string m_TimeMode = "U";
		private string m_TableName = "";
		private string m_FieldName = "";
		private string m_FieldType = "";
		private string m_Listener = "";
		private string m_Urno = "0";
//		private string argsL = "";
		private string m_PST_Fields = "'PSTA','PSTD' ";
		private string m_GAT_Fields = "'GTD1','GTD2','GTA1','GTA2' ";
		private string m_BLT_Fields = "'BLT1','BLT2' ";
		private string m_WRO_Fields = "'WRO1' ";
		private string m_CCA_Fields = "'CKIC' ";
		private string m_CCA_ExtFields = "'CKIC','CKBS','CKES','CKBA','CKEA' ";
		private string myNeedCCA = "";
		private string m_CcaLogtab = "";
		private string m_AftLogtab = "";
		private string m_ActiveField = "";
		private string m_FlightCount = "";
		public static frmMain myThis;

		#endregion ----- MyRegion

		private System.Windows.Forms.ImageList imageList1;
		private Ufis.Utils.BCBlinker bcBlinker1;
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.ImageList imageButtons;
		private System.Windows.Forms.Panel panelLabel;
		private System.Windows.Forms.Panel panelTab;
		private System.Windows.Forms.PictureBox pictureBoxTab;
		private AxTABLib.AxTAB tabLOG;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.CheckBox cbUTC;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Button btnApply;
		private System.Windows.Forms.Button btnNow;
		private System.Windows.Forms.DateTimePicker dateTimePickerTo;
		private System.Windows.Forms.Label lblTo;
		private System.Windows.Forms.DateTimePicker dateTimePickerFrom;
		private System.Windows.Forms.Label lblFrom;
		private System.Windows.Forms.Button btnPrint;
		private System.Windows.Forms.CheckBox cbView;
		private System.Windows.Forms.CheckBox cbTimer;
		public System.Windows.Forms.Label lblLocations;
		public System.Timers.Timer timer2;
		private AxAATLOGINLib.AxAatLogin LoginControl;
		private AxUFISCOMLib.AxUfisCom axUfisCom1;
		private AxUFISCOMLib.AxUfisCom u;
		private System.Windows.Forms.NumericUpDown numericUpDown1;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.Label lblTimer;
		private System.Windows.Forms.Label lblInMinutes;
		private System.ComponentModel.IContainer components;

		public frmMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			myThis = this;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				UT.DisposeMemDB();
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		public static frmMain GetThis()
		{
			return myThis;
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmMain));
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.bcBlinker1 = new Ufis.Utils.BCBlinker();
			this.panelTop = new System.Windows.Forms.Panel();
			this.lblInMinutes = new System.Windows.Forms.Label();
			this.lblTimer = new System.Windows.Forms.Label();
			this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
			this.u = new AxUFISCOMLib.AxUfisCom();
			this.axUfisCom1 = new AxUFISCOMLib.AxUfisCom();
			this.LoginControl = new AxAATLOGINLib.AxAatLogin();
			this.cbTimer = new System.Windows.Forms.CheckBox();
			this.btnPrint = new System.Windows.Forms.Button();
			this.imageButtons = new System.Windows.Forms.ImageList(this.components);
			this.cbView = new System.Windows.Forms.CheckBox();
			this.btnNow = new System.Windows.Forms.Button();
			this.dateTimePickerTo = new System.Windows.Forms.DateTimePicker();
			this.lblTo = new System.Windows.Forms.Label();
			this.dateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
			this.lblFrom = new System.Windows.Forms.Label();
			this.cbUTC = new System.Windows.Forms.CheckBox();
			this.btnClose = new System.Windows.Forms.Button();
			this.btnApply = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.panelLabel = new System.Windows.Forms.Panel();
			this.lblLocations = new System.Windows.Forms.Label();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabLOG = new AxTABLib.AxTAB();
			this.pictureBoxTab = new System.Windows.Forms.PictureBox();
			this.timer2 = new System.Timers.Timer();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.panelTop.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.u)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.axUfisCom1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.LoginControl)).BeginInit();
			this.panelLabel.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabLOG)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.timer2)).BeginInit();
			this.SuspendLayout();
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(19, 19);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.White;
			// 
			// bcBlinker1
			// 
			this.bcBlinker1.BackColor = System.Drawing.SystemColors.Control;
			this.bcBlinker1.Location = new System.Drawing.Point(608, 0);
			this.bcBlinker1.Name = "bcBlinker1";
			this.bcBlinker1.Size = new System.Drawing.Size(44, 24);
			this.bcBlinker1.TabIndex = 4;
			// 
			// panelTop
			// 
			this.panelTop.Controls.Add(this.lblInMinutes);
			this.panelTop.Controls.Add(this.lblTimer);
			this.panelTop.Controls.Add(this.numericUpDown1);
			this.panelTop.Controls.Add(this.u);
			this.panelTop.Controls.Add(this.axUfisCom1);
			this.panelTop.Controls.Add(this.LoginControl);
			this.panelTop.Controls.Add(this.cbTimer);
			this.panelTop.Controls.Add(this.btnPrint);
			this.panelTop.Controls.Add(this.cbView);
			this.panelTop.Controls.Add(this.btnNow);
			this.panelTop.Controls.Add(this.dateTimePickerTo);
			this.panelTop.Controls.Add(this.lblTo);
			this.panelTop.Controls.Add(this.dateTimePickerFrom);
			this.panelTop.Controls.Add(this.lblFrom);
			this.panelTop.Controls.Add(this.cbUTC);
			this.panelTop.Controls.Add(this.btnClose);
			this.panelTop.Controls.Add(this.btnApply);
			this.panelTop.Controls.Add(this.pictureBox1);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(1112, 40);
			this.panelTop.TabIndex = 5;
			// 
			// lblInMinutes
			// 
			this.lblInMinutes.BackColor = System.Drawing.Color.Transparent;
			this.lblInMinutes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblInMinutes.Location = new System.Drawing.Point(628, 16);
			this.lblInMinutes.Name = "lblInMinutes";
			this.lblInMinutes.Size = new System.Drawing.Size(68, 16);
			this.lblInMinutes.TabIndex = 107;
			this.lblInMinutes.Text = "in minutes";
			// 
			// lblTimer
			// 
			this.lblTimer.BackColor = System.Drawing.Color.Transparent;
			this.lblTimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblTimer.Location = new System.Drawing.Point(524, 16);
			this.lblTimer.Name = "lblTimer";
			this.lblTimer.Size = new System.Drawing.Size(36, 16);
			this.lblTimer.TabIndex = 106;
			this.lblTimer.Text = "Timer: ";
			// 
			// numericUpDown1
			// 
			this.numericUpDown1.Location = new System.Drawing.Point(588, 12);
			this.numericUpDown1.Maximum = new System.Decimal(new int[] {
																		   60,
																		   0,
																		   0,
																		   0});
			this.numericUpDown1.Minimum = new System.Decimal(new int[] {
																		   1,
																		   0,
																		   0,
																		   0});
			this.numericUpDown1.Name = "numericUpDown1";
			this.numericUpDown1.Size = new System.Drawing.Size(40, 20);
			this.numericUpDown1.TabIndex = 105;
			this.toolTip1.SetToolTip(this.numericUpDown1, "Set the automatic reload timer in minutes");
			this.numericUpDown1.Value = new System.Decimal(new int[] {
																		 2,
																		 0,
																		 0,
																		 0});
			this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
			// 
			// u
			// 
			this.u.ContainingControl = this;
			this.u.Enabled = true;
			this.u.Location = new System.Drawing.Point(840, 0);
			this.u.Name = "u";
			this.u.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("u.OcxState")));
			this.u.Size = new System.Drawing.Size(40, 24);
			this.u.TabIndex = 104;
			// 
			// axUfisCom1
			// 
			this.axUfisCom1.ContainingControl = this;
			this.axUfisCom1.Enabled = true;
			this.axUfisCom1.Location = new System.Drawing.Point(728, 0);
			this.axUfisCom1.Name = "axUfisCom1";
			this.axUfisCom1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axUfisCom1.OcxState")));
			this.axUfisCom1.Size = new System.Drawing.Size(48, 24);
			this.axUfisCom1.TabIndex = 103;
			// 
			// LoginControl
			// 
			this.LoginControl.ContainingControl = this;
			this.LoginControl.Enabled = true;
			this.LoginControl.Location = new System.Drawing.Point(784, 0);
			this.LoginControl.Name = "LoginControl";
			this.LoginControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("LoginControl.OcxState")));
			this.LoginControl.Size = new System.Drawing.Size(48, 24);
			this.LoginControl.TabIndex = 102;
			this.LoginControl.Visible = false;
			// 
			// cbTimer
			// 
			this.cbTimer.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbTimer.BackColor = System.Drawing.Color.Transparent;
			this.cbTimer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.cbTimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbTimer.ImageIndex = 16;
			this.cbTimer.ImageList = this.imageList1;
			this.cbTimer.Location = new System.Drawing.Point(564, 12);
			this.cbTimer.Name = "cbTimer";
			this.cbTimer.Size = new System.Drawing.Size(20, 20);
			this.cbTimer.TabIndex = 101;
			this.cbTimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.toolTip1.SetToolTip(this.cbTimer, "Activate/Dectivate timer for automatic relad each <n> minutes");
			this.cbTimer.CheckedChanged += new System.EventHandler(this.cbTimer_CheckedChanged);
			// 
			// btnPrint
			// 
			this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnPrint.BackColor = System.Drawing.Color.Transparent;
			this.btnPrint.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnPrint.ImageIndex = 2;
			this.btnPrint.ImageList = this.imageButtons;
			this.btnPrint.Location = new System.Drawing.Point(952, 0);
			this.btnPrint.Name = "btnPrint";
			this.btnPrint.Size = new System.Drawing.Size(80, 40);
			this.btnPrint.TabIndex = 100;
			this.btnPrint.Text = "&Print";
			this.toolTip1.SetToolTip(this.btnPrint, "Print the current loaded data");
			this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
			// 
			// imageButtons
			// 
			this.imageButtons.ImageSize = new System.Drawing.Size(19, 19);
			this.imageButtons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageButtons.ImageStream")));
			this.imageButtons.TransparentColor = System.Drawing.Color.White;
			// 
			// cbView
			// 
			this.cbView.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbView.BackColor = System.Drawing.Color.Transparent;
			this.cbView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.cbView.ImageIndex = 10;
			this.cbView.ImageList = this.imageList1;
			this.cbView.Location = new System.Drawing.Point(80, 0);
			this.cbView.Name = "cbView";
			this.cbView.Size = new System.Drawing.Size(80, 40);
			this.cbView.TabIndex = 99;
			this.cbView.Text = "&View";
			this.cbView.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.toolTip1.SetToolTip(this.cbView, "Open the view definition dialog");
			this.cbView.CheckedChanged += new System.EventHandler(this.cbView_CheckedChanged);
			// 
			// btnNow
			// 
			this.btnNow.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btnNow.ImageIndex = 4;
			this.btnNow.ImageList = this.imageList1;
			this.btnNow.Location = new System.Drawing.Point(468, 20);
			this.btnNow.Name = "btnNow";
			this.btnNow.Size = new System.Drawing.Size(18, 18);
			this.btnNow.TabIndex = 98;
			this.toolTip1.SetToolTip(this.btnNow, "Change the time to now");
			this.btnNow.Click += new System.EventHandler(this.btnNow_Click);
			// 
			// dateTimePickerTo
			// 
			this.dateTimePickerTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerTo.Location = new System.Drawing.Point(340, 19);
			this.dateTimePickerTo.Name = "dateTimePickerTo";
			this.dateTimePickerTo.Size = new System.Drawing.Size(128, 20);
			this.dateTimePickerTo.TabIndex = 97;
			// 
			// lblTo
			// 
			this.lblTo.BackColor = System.Drawing.Color.Transparent;
			this.lblTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblTo.Location = new System.Drawing.Point(248, 20);
			this.lblTo.Name = "lblTo";
			this.lblTo.Size = new System.Drawing.Size(88, 16);
			this.lblTo.TabIndex = 96;
			this.lblTo.Text = "Changes To:";
			// 
			// dateTimePickerFrom
			// 
			this.dateTimePickerFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerFrom.Location = new System.Drawing.Point(340, -1);
			this.dateTimePickerFrom.Name = "dateTimePickerFrom";
			this.dateTimePickerFrom.Size = new System.Drawing.Size(128, 20);
			this.dateTimePickerFrom.TabIndex = 95;
			// 
			// lblFrom
			// 
			this.lblFrom.BackColor = System.Drawing.Color.Transparent;
			this.lblFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblFrom.Location = new System.Drawing.Point(248, 2);
			this.lblFrom.Name = "lblFrom";
			this.lblFrom.Size = new System.Drawing.Size(88, 16);
			this.lblFrom.TabIndex = 94;
			this.lblFrom.Text = "Changes From:";
			// 
			// cbUTC
			// 
			this.cbUTC.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbUTC.BackColor = System.Drawing.Color.Transparent;
			this.cbUTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbUTC.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.cbUTC.ImageIndex = 16;
			this.cbUTC.ImageList = this.imageList1;
			this.cbUTC.Location = new System.Drawing.Point(0, 0);
			this.cbUTC.Name = "cbUTC";
			this.cbUTC.Size = new System.Drawing.Size(80, 40);
			this.cbUTC.TabIndex = 76;
			this.cbUTC.Text = "&UTC";
			this.cbUTC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.toolTip1.SetToolTip(this.cbUTC, "Switch between UTC and local Times");
			this.cbUTC.CheckedChanged += new System.EventHandler(this.cbUTC_CheckedChanged);
			// 
			// btnClose
			// 
			this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClose.BackColor = System.Drawing.Color.Transparent;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnClose.ImageIndex = 0;
			this.btnClose.ImageList = this.imageButtons;
			this.btnClose.Location = new System.Drawing.Point(1032, 0);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(80, 40);
			this.btnClose.TabIndex = 75;
			this.btnClose.Text = "&Close";
			this.toolTip1.SetToolTip(this.btnClose, "Close the application");
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// btnApply
			// 
			this.btnApply.BackColor = System.Drawing.Color.Transparent;
			this.btnApply.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnApply.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnApply.ImageIndex = 1;
			this.btnApply.ImageList = this.imageButtons;
			this.btnApply.Location = new System.Drawing.Point(160, 0);
			this.btnApply.Name = "btnApply";
			this.btnApply.Size = new System.Drawing.Size(80, 40);
			this.btnApply.TabIndex = 74;
			this.btnApply.Text = "&Apply";
			this.toolTip1.SetToolTip(this.btnApply, "Reload the current active view filter");
			this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
			// 
			// pictureBox1
			// 
			this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
			this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pictureBox1.Location = new System.Drawing.Point(0, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(1112, 40);
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
			// 
			// panelLabel
			// 
			this.panelLabel.BackColor = System.Drawing.Color.Transparent;
			this.panelLabel.Controls.Add(this.lblLocations);
			this.panelLabel.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelLabel.Location = new System.Drawing.Point(0, 40);
			this.panelLabel.Name = "panelLabel";
			this.panelLabel.Size = new System.Drawing.Size(1112, 24);
			this.panelLabel.TabIndex = 6;
			// 
			// lblLocations
			// 
			this.lblLocations.BackColor = System.Drawing.Color.Black;
			this.lblLocations.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblLocations.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblLocations.ForeColor = System.Drawing.Color.Lime;
			this.lblLocations.Location = new System.Drawing.Point(0, 0);
			this.lblLocations.Name = "lblLocations";
			this.lblLocations.Size = new System.Drawing.Size(1112, 24);
			this.lblLocations.TabIndex = 0;
			this.lblLocations.Text = "Active Locations : ";
			this.lblLocations.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.lblLocations.Click += new System.EventHandler(this.label1_Click);
			// 
			// panelTab
			// 
			this.panelTab.BackColor = System.Drawing.Color.Transparent;
			this.panelTab.Controls.Add(this.tabLOG);
			this.panelTab.Controls.Add(this.pictureBoxTab);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 64);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(1112, 485);
			this.panelTab.TabIndex = 7;
			// 
			// tabLOG
			// 
			this.tabLOG.ContainingControl = this;
			this.tabLOG.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabLOG.Location = new System.Drawing.Point(0, 0);
			this.tabLOG.Name = "tabLOG";
			this.tabLOG.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabLOG.OcxState")));
			this.tabLOG.Size = new System.Drawing.Size(1112, 485);
			this.tabLOG.TabIndex = 1;
			this.tabLOG.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabLOG_SendLButtonDblClick);
			// 
			// pictureBoxTab
			// 
			this.pictureBoxTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pictureBoxTab.Location = new System.Drawing.Point(0, 0);
			this.pictureBoxTab.Name = "pictureBoxTab";
			this.pictureBoxTab.Size = new System.Drawing.Size(1112, 485);
			this.pictureBoxTab.TabIndex = 0;
			this.pictureBoxTab.TabStop = false;
			this.pictureBoxTab.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxTab_Paint);
			// 
			// timer2
			// 
			this.timer2.Interval = 120000;
			this.timer2.SynchronizingObject = this;
			this.timer2.Elapsed += new System.Timers.ElapsedEventHandler(this.timer2_Elapsed);
			// 
			// frmMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(1112, 549);
			this.Controls.Add(this.panelTab);
			this.Controls.Add(this.panelLabel);
			this.Controls.Add(this.panelTop);
			this.Controls.Add(this.bcBlinker1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmMain";
			this.Text = "Location Changes:";
			this.Resize += new System.EventHandler(this.frmMain_Resize);
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmMain_Closing);
			this.Load += new System.EventHandler(this.frmMain_Load);
			this.panelTop.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.u)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.axUfisCom1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.LoginControl)).EndInit();
			this.panelLabel.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabLOG)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.timer2)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
/*		
		[STAThread]
		static void Main() 
		{
			Application.Run(new frmMain()); 
		}
*/		
		[STAThread]
		static void Main(string [] args) 
		{
			frmMain olDlg = new frmMain(); 
			olDlg.args = args;
			Application.Run(olDlg);
		}

		private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			//UT.DrawGradient(e.Graphics, 90f, pictureBox1, Color.WhiteSmoke, Color.LightGray);
			UT.DrawGradient(e.Graphics, 90f, pictureBox1, Color.AliceBlue, Color.LightSteelBlue);//Color.CornflowerBlue);

		}

		private bool LoginProcedure()
		{
			string LoginAnsw = "";
			string tmpRegStrg = "";
			string strRet = "";
			LoginControl.ApplicationName = "LocationChanges";
			LoginControl.InfoCaption = "Info about Location Changes";
			LoginControl.InfoButtonVisible = true;
			LoginControl.InfoUfisVersion = "Ufis Version 4.5";
			LoginControl.UserNameLCase = true;
			tmpRegStrg = "LocationChanges" + ",";
			tmpRegStrg += "InitModu,InitModu,Initialisieren (InitModu),B,-";
			LoginControl.RegisterApplicationString = tmpRegStrg;
			strRet = LoginControl.ShowLoginDialog();
			LoginAnsw = LoginControl.GetPrivileges("InitModu");
			if( LoginAnsw != "" && strRet != "CANCEL" )
					return true; 
			else  
				return false;
		}

		private bool InitFormWithArgs()
		{
			if(args.Length != 0) 
			{
				string [] argList = args[0].Split(',');
				if(argList.Length == 8)
				{
					m_Urno				= argList[0];
					m_TableName			= argList[1];
					m_FieldName			= argList[2];
					m_ViewMode			= argList[3];
					m_TimeMode			= argList[4];
					m_Listener			= argList[5];
					m_FieldType			= argList[6];

					UT.UserName = argList[7];

					if (m_FieldName.Equals("XXXX"))
					{
						m_FieldName = "ALL FIELDS";
						myNeedCCA = ",'CCA'";
					}
				}
				else
				{
					MessageBox.Show(this, "Data Changes has been called with unexpected parameters:  " + args[0], "Error in parameterlist for Data Changes", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					Application.Exit();
					return false;
				}
			}
			else
			{
				if(LoginProcedure() == false)
				{
					Application.Exit();
					return false;
				}

				//if called without args: mode is used for afttab and location changes, no information will be sended
				m_ViewMode = "LOCATIONS";
				m_TableName	= "AFT";
				m_Listener = "";
				m_FieldType = "C";
			}

			return true;
		}

		private void PreFormatForm()
		{
			cbView.Parent = pictureBox1;
			cbUTC.Parent = pictureBox1;
			btnApply.Parent = pictureBox1;
			btnPrint.Parent = pictureBox1;
			btnClose.Parent = pictureBox1;
			lblTimer.Parent = pictureBox1;
			lblInMinutes.Parent = pictureBox1;

			lblFrom.Parent = pictureBox1;
			lblTo.Parent = pictureBox1;
			btnNow.Parent = pictureBox1;
			cbTimer.Parent = pictureBox1;
		
			dateTimePickerFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dateTimePickerFrom.Value = new DateTime(dateTimePickerFrom.Value.Year, dateTimePickerFrom.Value.Month, dateTimePickerFrom.Value.Day, 0,0,0,0);
			dateTimePickerTo.CustomFormat = "dd.MM.yyyy - HH:mm";
			dateTimePickerTo.Value = new DateTime(dateTimePickerTo.Value.Year, dateTimePickerTo.Value.Month, dateTimePickerTo.Value.Day, 23,59,59,0);

			// set form to requested timemode
			if (m_TimeMode == "U")
				cbUTC.Checked = true;
			else
				cbUTC.Checked = false;
		}

		private void InitUtils()
		{
			//Read ceda.ini and set it global to UT (UfisTools)
			String strServer="";
			String strHopo = "";
			Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
			strServer = myIni.IniReadValue("GLOBAL", "HOSTNAME");
			strHopo = myIni.IniReadValue("GLOBAL", "HOMEAIRPORT");
			UT.ServerName = strServer;
			UT.Hopo = strHopo;
			if (UT.UserName.Length == 0)
				UT.UserName = LoginControl.GetUserName();
			UT.IsTimeInUtc = true;

			//DE.InitDBObjects(bcBlinker1);
			myDB = UT.GetMemDB();
			bool result = myDB.Connect(UT.ServerName,UT.UserName,UT.Hopo,"TAB","DATACHNG");
		}

		private void CreateLOGTAB()
		{
			//login table for m_TableName
			string strLoginTable = "";
			string strShortTable = "";
			IUfisComWriter myUfisCom = UT.GetUfisCom();
			int ilRet = myUfisCom.CallServer("RT", "SYSTAB", "LOGD", "", "WHERE TANA = '" + m_TableName + "'", "230");
			long ilCount = myUfisCom.GetBufferCount();
			if (ilCount != 0)
			{
				string strBuffer = myUfisCom.GetDataBuffer(false);
				strLoginTable = myUfisCom.GetBufferLine(0);
				strShortTable = strLoginTable.Substring(0,3);
				m_AftLogtab = strShortTable;

				myUfisCom.ClearDataBuffer();
			}

			if(ilRet != 0)
			{
				string strERR = myUfisCom.LastErrorMessage;
				MessageBox.Show(this, strERR);
			}

			// create logTAB and define tabLOG
			
			logTAB = myDB.Bind("L0G", strShortTable, "Flight,Location,Tran,Old Value,New Value,Time of Change,Changed by User,Urno,Keyf,STOX", "10,10,10,10,10,14,10,10,10,14", "UREF,FINA,TRAN,h_tab_old_value(tana,fina,uref,time),VALU,TIME,USEC,URNO,KEYF,STOX");
			logTAB.TimeFields = "TIME,STOA,STOD,STOX";
			if (m_FieldType.Equals("D"))
				logTAB.TimeFields = "TIME,STOA,STOD,VALU,OVAL,STOX";
			logTAB.TimeFieldsCurrentlyInUtc = true;
			logTAB.TimeFieldsInitiallyInUtc = true;
			
			tabLOG.ResetContent();
			//tabLOG.HeaderString = "  0 , 1 , 2 , 3 ,   4  ,  5,  6,  7    ,    8    ,     9        ,        10        ,11  ,12  , 13
			tabLOG.HeaderString = "Flight,STA,STD,A/D,Field,Code,Tran,Old Value,New Value,Time of Change,Changed by User,Urno,Keyf,STOX";
			tabLOG.LogicalFieldList = "UREF,STOA,STOD,ADID,ADDI,FINA,TRAN,OVAL,VALU,TIME,USEC,URNO,KEYF,STOX";//tabLOG.HeaderString;
			tabLOG.HeaderLengthString  = "90,150,150,40,200,50,40,150,150,170,120,-1,-1,-1";
			tabLOG.ColumnWidthString = "10,14,14,6,6,10,6,14,14,14,10,-1,-1,-1";
			tabLOG.ColumnAlignmentString = "L,L,L,C,L,L,C,L,L,L,L,L,L,L";
			tabLOG.LifeStyle = true;
			tabLOG.LineHeight = 20;
			tabLOG.FontName = "Courier New"; //"Arial"
			tabLOG.FontSize = 16;
			tabLOG.HeaderFontSize = 16;
			string strColor = UT.colBlue + "," + UT.colBlue;
			tabLOG.CursorDecoration(tabLOG.LogicalFieldList, "B,T", "2,2", strColor);
			tabLOG.DefaultCursor = false;
			tabLOG.InplaceEditUpperCase = false;
			tabLOG.EnableHeaderSizing(true);
			tabLOG.ShowHorzScroller(true);
			tabLOG.AutoSizeByHeader = true;


			//Formatting of date time columns
			int idx = 0;
			idx = UT.GetItemNo(tabLOG.LogicalFieldList, "STOA");
			tabLOG.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm");
			idx = UT.GetItemNo(tabLOG.LogicalFieldList, "STOD");
			tabLOG.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm");
			idx = UT.GetItemNo(tabLOG.LogicalFieldList, "TIME");
			tabLOG.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm':'ss");
			idx = UT.GetItemNo(tabLOG.LogicalFieldList, "STOX");
			tabLOG.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm':'ss");
			if (m_FieldType.Equals("D"))
			{
				idx = UT.GetItemNo(tabLOG.LogicalFieldList, "VALU");
				tabLOG.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm");
				idx = UT.GetItemNo(tabLOG.LogicalFieldList, "OVAL");
				tabLOG.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm");
			}

			// show tabLOG
			tabLOG.SetInternalLineBuffer(true);
			tabLOG.Refresh();
		}

		private void LoginTableForCCA()
		{
			string strLoginTable = "";
			IUfisComWriter myUfisCom = UT.GetUfisCom();

			int ilRet = myUfisCom.CallServer("RT", "SYSTAB", "LOGD", "", "WHERE TANA = 'CCA'", "230");
			long ilCount = myUfisCom.GetBufferCount();
			if (ilCount != 0)
			{
				string strBuffer = myUfisCom.GetDataBuffer(false);
				strLoginTable = myUfisCom.GetBufferLine(0);
				m_CcaLogtab = strLoginTable.Substring(0,3);
				myUfisCom.ClearDataBuffer();
			}

			if(ilRet != 0)
			{
				string strERR = myUfisCom.LastErrorMessage;
				MessageBox.Show(this, strERR);
			}
		}

		private void HandleViewMode()
		{
			// handle the requested viewmode
			if (m_ViewMode == "LOCATIONS")
			{
				// set text
				this.Text = "Location Changes";
				// load all necessary tables for the viewdialog
				DataForm = new frmData();
//				DataForm.Show();
				DataForm.Hide();
				DataForm.LoadData();
				//init view
				FillSelection();
			}
			else if (m_ViewMode == "FIELD")
			{
				// set text
				this.Text = "Data Changes";
				m_ActiveField = "Active Field: " + m_FieldName;

				// fill selection for flightdata
				string urno = m_Urno.Replace(";", ",");
				string selection = "WHERE URNO IN (" + urno + ")";
				//MessageBox.Show(this, selection, "Updating AFTTAB", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

				// don't select flightdata via view
				//cbView.BackColor = Color.Red;
				cbView.Enabled = false;

				// load flightdata from calling urnos
				DataForm = new frmData();
//				DataForm.Show();
				DataForm.Hide();
				//only flightdata is needed
				DataForm.LoadAFTData(selection);
				DataForm.LoadSYSData();

				if (m_TableName.Equals("CCA"))
//					DataForm.LoadXXXData("CCA", "CheckIn-Counter", "URNO,FLNO,CNAM,FLNU", "30,30,30,30", "FLNU", "FLNU,CNAM", "URNO,FLNU");
					DataForm.LoadCCAData("CCA");

				//additional for logtabs
				myStrWhereALOCIn = "WHERE TANA = '" + m_TableName + "'" + " AND FINA = '" + m_FieldName + "'";
				if (m_FieldName.Equals("ALL FIELDS"))
				{
					//myStrWhereALOCIn = "WHERE TANA = '" + m_TableName + "'";
					DataForm.LoadCCAData("CCA");
					myStrWhereALOCIn = "WHERE TANA IN ('" + m_TableName + "'" + myNeedCCA + ")";
				}
				myStrSortOrder = " ORDER BY TIME ASC, SEQN ASC";

				//automatic selection and view
				FillSelection();

				//handle active timemode
				HandleUTC();
			}
		}

		private void frmMain_Load(object sender, System.EventArgs e)
		{
			if (!InitFormWithArgs())
				return;
			PreFormatForm();
			InitUtils();
			CreateLOGTAB();
			LoginTableForCCA();
			HandleViewMode();
			ReadRegistry();
		}

		private void WriteRegistry()
		{
			string theKey = "Software\\UFIS\\DATACHANGES\\MainWindow";
			RegistryKey rk = Registry.CurrentUser.CreateSubKey(theKey);
			if(rk == null)
			{
				rk.CreateSubKey(theKey);
				rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			}

			rk.SetValue("X", this.Left.ToString());
			rk.SetValue("Y", this.Top.ToString());
			rk.SetValue("Width", this.Width.ToString());
			rk.SetValue("Height", this.Height.ToString());
			rk.Close();
		}

		private void ReadRegistry()
		{
			int myX=-1, myY=-1, myW=-1, myH=-1;
			string regVal = "";
			string theKey = "Software\\UFIS\\DATACHANGES\\MainWindow";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk != null)
			{
				regVal = rk.GetValue("X", "-1").ToString();
				myX = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Y", "-1").ToString();
				myY = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Width", "-1").ToString();
				myW = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Height", "-1").ToString();
				myH = Convert.ToInt32(regVal);
			}
			if(myW != -1 && myH != -1)
			{
				this.Left = myX;
				this.Top = myY;
				this.Width = myW;
				this.Height = myH;
			}
		}

		private void tabLOG_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			tabLOG_SortColumn(e.lineNo, e.colNo);
		}

		private void tabLOG_SortColumn(int lineNo, int colNo)
		{
			if(lineNo == -1)
			{
				if (colNo == tabLOG.CurrentSortColumn)
				{
					if( tabLOG.SortOrderASC == true)
					{
						tabLOG.Sort(colNo.ToString(), false, true);
					}
					else
					{
						tabLOG.Sort(colNo.ToString(), true, true);
					}
				}
				else
				{
					tabLOG.Sort(colNo.ToString(), true, true);
				}
				tabLOG.Refresh();
			}
		}


		private void FillUrefLogTAB()
		{
			ArrayList arrayUREF = new ArrayList(200);
			for( int i = 0; i < logTAB.Count; i++)
			{
				string strUref = logTAB[i]["Flight"];
//				string strUref = tabLOG.GetFieldValue(i, "UREF");
				if (!arrayUREF.Contains(strUref))
					arrayUREF.Add(strUref);

			}

			string strUREF = "";
			for( int j = 0; j < arrayUREF.Count; j++)
			{
				if (j > 0)
					strUREF += ",";
				
				strUREF += arrayUREF[j];
			}

			if (strUREF.Length > 0)
				myStrWhereFLNUIn = " AND UREF IN (" + strUREF + ")";
			else
				myStrWhereFLNUIn = "";
		}

		private void DoDeletedFlights()
		{
			//clear table
			tabLOG.ResetContent();

			//part of selection for UREF
			m_FieldName = "ALL FIELDS";
			m_ActiveField = "Active Field: " + m_FieldName;
			myNeedCCA = ",'CCA'";
			string Selection = mySelectionLOGTABKeyf + myStrWhereTimeframe;

			logTAB.Clear();
//			MessageBox.Show(this, mySelection, "Updating LOGATB", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			logTAB.Load(Selection);

			return;
		}

		private void cbView_CheckedChanged(object sender, System.EventArgs e)
		{
			//if red m_ViewMode is "FIELD", don#t use viewdialog because flight is set via args[]
			if (cbView.BackColor == Color.Red || m_ViewMode.Equals("FIELD"))
			{
				this.DialogResult = DialogResult.None; 
				return;
			}

			if(cbView.Checked == true)
			{
				cbView.BackColor = Color.LightGreen;
				cbView.Refresh();
				//call viewdialog
				frmView Dlg = new frmView();
				if(Dlg.ShowDialog(this) == DialogResult.OK)
				{
					this.Cursor = Cursors.WaitCursor;

					this.Refresh();
					tabLOG.Refresh();
					this.Text = Dlg.myWindowText;
/*
					this.dateTimePickerFrom.Value = Dlg.dateTimePickerFrom.Value;
					this.dateTimePickerTo.Value = Dlg.dateTimePickerTo.Value;
					this.dateTimePickerFrom.Refresh();
					this.dateTimePickerTo.Refresh();
*/					
//#####


					myboolDoingLogtab = false;
					mySelectionLOGTABKeyf = "";
					string fields = "";
					if (Dlg.mySelection.StartsWith("WHERE KEYF"))
					{
						myboolDoingLogtab = true;
						mySelectionLOGTABKeyf = Dlg.mySelection;
						m_FieldName = "ALL FIELDS";
						myNeedCCA = ",'CCA'";
					}
					else
					{
//#####


						//No Flights found for mySelection
						if (DataForm.LoadAFTData(Dlg.mySelection) == false)
						{
							this.Cursor = Cursors.Arrow;
							MessageBox.Show(this, Dlg.mySelection, "No Flights found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
							cbView.Checked = false;
							cbView.BackColor = Color.Transparent;
//							return;
						}

						//set the requested locations
						m_ActiveField = "Active Locations: ";
						myNeedCCA = "";
//						string fields = "";
						for( int i = 0; i < Dlg.myLocation.Count; i++)
						{
							if (Dlg.myLocation[i].ToString() == "PST")
							{
								m_ActiveField += "  Position  ";
								fields += m_PST_Fields;
							}
							if (Dlg.myLocation[i].ToString() == "GAT")
							{
								m_ActiveField += "  Gate  ";
								fields += m_GAT_Fields;
							}
							if (Dlg.myLocation[i].ToString() == "BLT")
							{
								m_ActiveField += "  Belt  ";
								fields += m_BLT_Fields;
							}
							if (Dlg.myLocation[i].ToString() == "WRO")
							{
								m_ActiveField += "  Lounge  ";
								fields += m_WRO_Fields;
							}
							if (Dlg.myLocation[i].ToString() == "CCA")
							{
								m_ActiveField += "  Check-In";
								//also can be used for an another table
								fields += m_CCA_Fields;
								myNeedCCA = ",'CCA'";
								//							DataForm.LoadXXXData("CCA", "CheckIn-Counter", "URNO,FLNO,CNAM,FLNU", "30,30,30,30", "FLNU", "FLNU,CNAM", "URNO,FLNU");
								DataForm.LoadCCAData("CCA");
							}
						}
					}

					//part of the selection for TANA and FINA
					string tmp = fields.Replace(" ", ",");
					if (tmp.Length > 0)
						m_FieldName = tmp.Substring(0, tmp.Length - 1);

					if (!m_FieldName.Equals(""))
					{
						string strFina =  "AND FINA IN (" + m_FieldName + ")";
						string strCcaFina =  "AND FINA IN (" + m_CCA_ExtFields + ")";
						if (m_FieldName.Equals("ALL FIELDS"))
							strFina = "";

						myStrWhereALOCIn = "WHERE TANA IN ('" + m_TableName + "'" + myNeedCCA + ")" + strFina;

						myStrCcaWhereALOCIn = "WHERE TANA IN ('" + m_TableName + "'" + myNeedCCA + ")" + strCcaFina;
					}
					else
					{
						myStrWhereALOCIn = "";
					}

					//retrive data from the logTabs
					FillSelection();
					this.Cursor = Cursors.Arrow;

				}
				cbView.Checked = false;
				cbView.BackColor = Color.Transparent;
			}
		}

		private void frmMain_Resize(object sender, System.EventArgs e)
		{
			pictureBox1.Invalidate();
			pictureBoxTab.Invalidate();
		}

		private void label1_Click(object sender, System.EventArgs e)
		{
		
		}

		private void pictureBoxTab_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, pictureBoxTab, Color.WhiteSmoke, Color.LightGray);
		}

		private void cbUTC_CheckedChanged(object sender, System.EventArgs e)
		{
			HandleUTC();
		}

		//if more forms needs to handle the utc/local use a delegate
		private void HandleUTC()
		{
			if(cbUTC.Checked == true)
				//cbUTC.BackColor = Color.LightGreen;
				cbUTC.Text = "UTC";
			else
				//cbUTC.BackColor = Color.Transparent;
				cbUTC.Text = "Local";

			cbUTC.Refresh();

			if (!myIsInit)
				return;

			if ((cbUTC.Checked == true && myIsTimeInUtc == false) || (cbUTC.Checked == false && myIsTimeInUtc == true))
			{
				// conversion needed
			}
			else
				return;
				

			// convert timefields if necesarry
			string strTimeFields = logTAB.TimeFields;
			string[] arrAftTimeFields = strTimeFields.Split(',');
			DateTime dat;
			string strTmp = "";

			for(int i = 0; i < tabLOG.GetLineCount(); i++)
			{
				for(int j = 0; j < arrAftTimeFields.Length; j++)
				{
					string test = arrAftTimeFields[j];
					strTmp = tabLOG.GetFieldValue(i, test);
					if(strTmp.Equals(" ") || strTmp.Equals(""))
						continue;
					{
						if (strTmp.Length == 12)
							strTmp += "00";
						dat = UT.CedaFullDateToDateTime(strTmp);

						if (cbUTC.Checked == true && myIsTimeInUtc == false)
							dat = UT.LocalToUtc(dat);
						if (cbUTC.Checked == false && myIsTimeInUtc == true)
							dat = UT.UtcToLocal(dat);

						strTmp = UT.DateTimeToCeda(dat);
						tabLOG.SetFieldValues(i, test, strTmp);
					}
				}
			}

			tabLOG.Refresh();

			// set viewing timemode
			if(cbUTC.Checked == true)
				myIsTimeInUtc = true;
			else
				myIsTimeInUtc = false;

		}

		private void cbTimer_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbTimer.Checked == true)
			{
				cbTimer.BackColor = Color.LightGreen;
				timer2.Enabled = true;
				timer2.Interval = (double) numericUpDown1.Value * 60000;
				numericUpDown1.Enabled = false;
			}
			else
			{
				cbTimer.BackColor = Color.Transparent;
				timer2.Enabled = false;
				numericUpDown1.Enabled = true;
			}
			cbTimer.Refresh();
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			DataForm.Close();
			//Store current geometry in Registry
			WriteRegistry();

			//send close (-1) to m_Listener
			if (m_Listener.Length != 0)
			{
				IUfisComWriter myUfisCom = UT.GetUfisCom();
				int ilRet = myUfisCom.CallServer("SBC", "BCLOCC", "LOC_CHANGES", "-1", m_Listener, "230");
				if(ilRet != 0)
				{
					string strERR = myUfisCom.LastErrorMessage;
					MessageBox.Show(this, strERR);
				}
			}
			this.Close();
		}

		private void btnNow_Click(object sender, System.EventArgs e)
		{
			DateTime olNow;
			if(myIsTimeInUtc == true)
			{
				olNow = DateTime.UtcNow;
			}
			else
			{
				olNow = DateTime.Now;
			}
			dateTimePickerTo.Value = olNow;
		}

		private void timer2_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			cbTimer.BackColor = Color.HotPink;
			btnNow.BackColor = Color.HotPink;
			cbTimer.Refresh();
			btnNow.Refresh();
			btnNow_Click(sender, e);		
			FillSelection();
			cbTimer.BackColor = Color.LightGreen;
			btnNow.BackColor = Color.Transparent;
			cbTimer.Refresh();
			btnNow.Refresh();
		}

		private void FillTIME()
		{
			//requested timeframe for changes
			string strFrom = UT.DateTimeToCeda( dateTimePickerFrom.Value );
			string strTo   = UT.DateTimeToCeda( dateTimePickerTo.Value );

			DateTime datFrom = dateTimePickerFrom.Value;
			DateTime datTo   = dateTimePickerTo.Value;
			if (myIsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(datFrom);
				datTo   = UT.LocalToUtc(datTo);
			}
			strFrom = UT.DateTimeToCeda( datFrom );
			strTo   = UT.DateTimeToCeda( datTo );

			//part of selection for TIME
			myStrWhereTimeframe = " AND (TIME BETWEEN '" + strFrom + "' AND '" + strTo + "')";
		}

		private void FillUREF(string strTable, string strField)
		{
			//requested changes for flights (loaded with the selection of frmView or via args)
			ITable aftTable = myDB[strTable];

			if (strTable.Equals("LOG"))
				aftTable = logTAB;
			
			if(aftTable == null)
				return;

			//part of selection for UREF
			ArrayList arrayUREF = new ArrayList(200);
			myStrWhereFLNUIn = "";
			myStrCcaWhereFLNUIn = "";
			string strUREF = "";
			string strFLNU = "";
			for( int i = 0; i < aftTable.Count; i++)
			{
				string strUref = aftTable[i][strField];
				if (strUref.Length > 0 && !arrayUREF.Contains(strUref))
					arrayUREF.Add(strUref);
				else
					continue;
			}

			if (strTable.Equals("AFT") || strTable.Equals("LOG"))
				m_FlightCount = arrayUREF.Count.ToString();

			strUREF = " AND UREF IN (";
			strFLNU = "WHERE FLNU IN (";
			for( int j = 0; j < arrayUREF.Count; j++)
			{
				string strUref = "";
				strUref += arrayUREF[j];
				if( (j % 300) == 0 && j > 0)
				{
					strUREF += strUref + ")#";
					strFLNU += strUref + ")#";
					myStrWhereFLNUIn += strUREF;
					myStrCcaWhereFLNUIn += strFLNU;
					strUREF = " AND UREF IN (";
					strFLNU = "WHERE FLNU IN (";
				}
				else
				{
					if( j == (arrayUREF.Count - 1))
					{
						strUREF += strUref + ")";
						strFLNU += strUref + ")";
					}
					else
					{
						strUREF += strUref + ",";
						strFLNU += strUref + ",";
					}
				}
			}

			if( !strUREF.Equals(" AND UREF IN (") )
				myStrWhereFLNUIn += strUREF;

			if( !strFLNU.Equals("WHERE FLNU IN (") )
				myStrCcaWhereFLNUIn += strFLNU;

			if( arrayUREF.Count == 0 )
			{
				myStrWhereFLNUIn = "";
				myStrCcaWhereFLNUIn = "";
			}
		}

		private void BuildLine(int i)
		{
			if (myboolDoingLogtab)
			{
				string strKEYF = logTAB[i]["Keyf"];
				string strFlno = "";
				string strDay = "";
				string strAdid = "";
				if (strKEYF.Length == 18)
				{
					string strFlnu = strKEYF.Substring(0,5);
					string strAlc3 = strKEYF.Substring(5,3);
					string strFlns = strKEYF.Substring(8,1);
					strDay  = strKEYF.Substring(9,8);
					strAdid = strKEYF.Substring(17,1);

					strFlno = strAlc3 + " " + strFlnu + " ";
					if (!strFlns.Equals("#"))
						strFlno += strFlns;
				}

				string strSta = "";
				string strStd = "";
				string strLine = "";
				if (strAdid.Equals("A"))
					strLine = strFlno + "," + logTAB[i]["STOX"] + "," + strStd + "," + strAdid + "," + "," + logTAB[i]["Location"] + "," + logTAB[i]["Tran"] + "," + logTAB[i]["Old Value"] + "," + logTAB[i]["New Value"] + "," + logTAB[i]["Time of Change"] + "," + logTAB[i]["Changed by User"] + "," + logTAB[i]["Urno"] + "," + logTAB[i]["Keyf"] + "," + logTAB[i]["STOX"];
				if (strAdid.Equals("D"))
					strLine = strFlno + "," + strSta + "," + logTAB[i]["STOX"] + "," + strAdid + "," + "," + logTAB[i]["Location"] + "," + logTAB[i]["Tran"] + "," + logTAB[i]["Old Value"] + "," + logTAB[i]["New Value"] + "," + logTAB[i]["Time of Change"] + "," + logTAB[i]["Changed by User"] + "," + logTAB[i]["Urno"] + "," + logTAB[i]["Keyf"] + "," + logTAB[i]["STOX"];

				tabLOG.InsertTextLine(strLine, false);
			}
			else
			{
				string strLine = "";
				strLine = logTAB[i]["Flight"] + ", , , , ," + logTAB[i]["Location"] + "," + logTAB[i]["Tran"] + "," + logTAB[i]["Old Value"] + "," + logTAB[i]["New Value"] + "," + logTAB[i]["Time of Change"] + "," + logTAB[i]["Changed by User"] + "," + logTAB[i]["Urno"] + "," + logTAB[i]["Keyf"] + "," + logTAB[i]["STOX"];
				tabLOG.InsertTextLine(strLine, false);

				//search for the information of flights (loaded in "AFT") and fill the line
				ITable aftTable = myDB["AFT"];
				if (aftTable == null)
					return;

				IRow [] rows = aftTable.RowsByIndexValue("URNO", tabLOG.GetFieldValue(i, "UREF"));
				if(rows.Length > 0)
				{
					tabLOG.SetFieldValues(i, "UREF", rows[0]["FLNO"]);
					tabLOG.SetFieldValues(i, "ADID", rows[0]["ADID"]);
					if (rows[0]["ADID"].Equals("A"))
						tabLOG.SetFieldValues(i, "STOA", rows[0]["STOA"]);
					else if (rows[0]["ADID"].Equals("D"))
						tabLOG.SetFieldValues(i, "STOD", rows[0]["STOD"]);
					else
					{
						tabLOG.SetFieldValues(i, "STOA", rows[0]["STOA"]);
						tabLOG.SetFieldValues(i, "STOD", rows[0]["STOD"]);
					}
				}
			}
		}

		private void ReadSelection()
		{
			logTAB.Clear();
			if (m_FieldName.Equals(""))
				return;

			this.Cursor = Cursors.WaitCursor;
			//load selection
			//also possible to do a selection for all UREF-entries if (""), but not requested yet
			string [] strArrUREF;
			strArrUREF = myStrWhereFLNUIn.Split('#');
			if (myStrWhereFLNUIn.Equals(""))
			{
				this.Cursor = Cursors.Arrow;
				return;
			}
			//build the whole selection (TIME,UREF,TANA,FINA)
			for(int i = 0; i < strArrUREF.Length; i++)
			{
				myStrWhereFLNUIn = strArrUREF[i];
				mySelection = myStrWhereALOCIn;
				mySelection += myStrWhereTimeframe;
				mySelection += myStrWhereFLNUIn;
				mySelection += myStrSortOrder;
//				MessageBox.Show(this, mySelection, "Updating LOGATB", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				logTAB.Load(mySelection);
			}
			this.Cursor = Cursors.Arrow;

			//build table
			for( int i = 0; i < logTAB.Count; i++)
			{
				BuildLine(i);

				string strTRAN = tabLOG.GetFieldValue(i, "TRAN");
				if (strTRAN.Equals("I"))
					tabLOG.SetLineColor(i, -1, UT.colLightGreen);
				else if (strTRAN.Equals("U"))
					tabLOG.SetLineColor(i, -1, UT.colLightYellow);
				else if (strTRAN.Equals("D"))
				{
					tabLOG.SetLineColor(i, -1, UT.colLightOrange);
					tabLOG.SetFieldValues(i, "VALU", "FLIGHT HAS BEEN DELETED");
				}

				ITable sysTable = myDB["SYS"];
				if (sysTable == null)
					return;

				IRow [] rows = sysTable.RowsByIndexValue("FINA", tabLOG.GetFieldValue(i, "FINA"));
				if(rows.Length > 0)
				{
					tabLOG.SetFieldValues(i, "ADDI", rows[0]["ADDI"]);
				}
			}
		}

		private void ReadSelectionCCATAB()
		{
			if (m_CCA_Fields.Equals(""))
				return;

			ITable logTable = myDB["L0G"];
			//remove old LoginTable
			myDB.Unbind("L0G");
			//bind new LoginTable
			logTable = myDB.Bind("L0G", m_CcaLogtab, "Flight,Location,Tran,Old Value,New Value,Time of Change,Changed by User,Urno,Keyf,STOX", "10,10,10,10,10,14,10,10,10,14", "UREF,FINA,TRAN,h_tab_old_value(tana,fina,uref,time),VALU,TIME,USEC,URNO,KEYF,STOX");
			//load selection
			this.Cursor = Cursors.WaitCursor;

			string [] strArrUREF;
			strArrUREF = myStrWhereFLNUIn.Split('#');
			if (myStrWhereFLNUIn.Equals(""))
			{
				this.Cursor = Cursors.Arrow;
				return;
			}
			//build the whole selection (TIME,UREF,TANA,FINA)
			for(int i = 0; i < strArrUREF.Length; i++)
			{
				myStrWhereFLNUIn = strArrUREF[i];
				mySelection = myStrCcaWhereALOCIn;
				mySelection += myStrWhereTimeframe;
				mySelection += myStrWhereFLNUIn;
				mySelection += myStrSortOrder;
//				MessageBox.Show(this, mySelection, "Updating LOGATB", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				logTable.Load(mySelection);
			}

			this.Cursor = Cursors.Arrow;

			//build table
			for( int i = 0; i < logTable.Count; i++)
			{
				int j = tabLOG.GetLineCount();
				string strLine = "";
				strLine = logTable[i]["Flight"] + ", , , , ," + logTable[i]["Location"] + "," + logTable[i]["Tran"] + "," + logTable[i]["Old Value"] + "," + logTable[i]["New Value"] + "," + logTable[i]["Time of Change"] + "," + logTable[i]["Changed by User"] + "," + logTable[i]["Urno"] + "," + logTable[i]["Keyf"] + "," + logTable[i]["STOX"];
				tabLOG.InsertTextLine(strLine, false);

				//search for the information of flights (loaded in "AFT") and fill the line
				ITable aftTable = myDB["AFT"];
				ITable ccaTable = myDB["CCA"];
				if (aftTable == null || ccaTable == null)
					return;

				IRow [] CcaRows = ccaTable.RowsByIndexValue("URNO", tabLOG.GetFieldValue(j, "UREF"));
				if(CcaRows.Length > 0)
				{
					//FLNU in CCA-record -> record of URNO in AFT
					IRow [] AftRows = aftTable.RowsByIndexValue("URNO", CcaRows[0]["FLNU"]);
					if(AftRows.Length > 0)
					{
						tabLOG.SetFieldValues(j, "UREF", AftRows[0]["FLNO"]);
						tabLOG.SetFieldValues(j, "ADID", AftRows[0]["ADID"]);
						if (AftRows[0]["ADID"].Equals("A"))
							tabLOG.SetFieldValues(j, "STOA", AftRows[0]["STOA"]);
						else if (AftRows[0]["ADID"].Equals("D"))
							tabLOG.SetFieldValues(j, "STOD", AftRows[0]["STOD"]);
						else
						{
							tabLOG.SetFieldValues(j, "STOA", AftRows[0]["STOA"]);
							tabLOG.SetFieldValues(j, "STOD", AftRows[0]["STOD"]);
						}
					}
					else
					{
						string strFLNO = CcaRows[0]["FLNO"];
						string strFlno = BuildFlightnumber(strFLNO);
						tabLOG.SetFieldValues(j, "UREF", strFlno);
						tabLOG.SetFieldValues(j, "ADID", "D");
						tabLOG.SetFieldValues(j, "STOD", CcaRows[0]["STOD"]);
					}
				}

				string strTRAN = tabLOG.GetFieldValue(j, "TRAN");
				if (strTRAN.Equals("I"))
					tabLOG.SetLineColor(j, -1, UT.colLightGreen);
				else if (strTRAN.Equals("U"))
					tabLOG.SetLineColor(j, -1, UT.colLightYellow);
				else if (strTRAN.Equals("D"))
				{
					tabLOG.SetLineColor(j, -1, UT.colLightOrange);
					tabLOG.SetFieldValues(j, "VALU", "CHECKIN HAS BEEN DELETED");
				}

				ITable sysTable = myDB["SYS"];
				if (sysTable == null)
					return;

				IRow [] rows = sysTable.RowsByIndexValue("FINA", tabLOG.GetFieldValue(j, "FINA"));
				if(rows.Length > 0)
				{
					string strADDI = rows[0]["ADDI"];
					tabLOG.SetFieldValues(j, "ADDI", rows[0]["ADDI"]);
				}
			}
			tabLOG.Refresh();
		}

		private string BuildFlightnumber(string strFLNO)
		{
			string strFlno = "";
			string strFlnuExt = "";
			string strTmpAlc = strFLNO.Substring(0,3);
			string strTmp = strFLNO.Substring(3,strFLNO.Length-3);
			string strTmpFlnu = "";
			string strTmpFlns = "";

			string [] strArr = strTmp.Split(' ');
			if (strArr.Length > 1)
			{
				strTmpFlnu = strArr[0];
				strTmpFlns = strArr[strArr.Length-1];
			}
			else
			{
				if (strTmp.Length == 6)
				{
					strTmpFlnu = strTmp.Substring(0,5);
					strTmpFlns = strTmp.Substring(5,1);
				}
				else
				{
					strTmpFlnu = strFLNO.Substring(3,strFLNO.Length-3);
					strTmpFlns = "";
				}
			}

			if (strTmpAlc.EndsWith(" "))
				strTmpAlc = strTmpAlc.Substring(0,2);

			if (strTmpFlnu.Length < 5)
			{
				int ilPos = 5 - strTmpFlnu.Length;
				for (int i=0; i<ilPos; i++)
				{
					strFlnuExt += "0";
				}

				strTmpFlnu = strFlnuExt + strTmpFlnu;

			}

			if (strTmpAlc.Length == 2)
			{
				ITable AltTab = myDB["ALT"];
				IRow [] rows1 = AltTab.RowsByIndexValue("ALC2", strTmpAlc);
				if(rows1.Length == 1)
					strTmpAlc = rows1[0]["ICAO-Code"];
			}

			strFlno = strTmpAlc + " " + strTmpFlnu + " " + strTmpFlns;
			return strFlno;
		}

		private void InformListener()
		{
			tabLOG.IndexCreate("FINA",5);
			tabLOG.SetInternalLineBuffer(true);

			// calculate number of changes
			string sumChanges = tabLOG.GetLineCount().ToString();
			lblLocations.Text = "Loaded Changes: " + sumChanges + "  (for " + m_FlightCount + " Flights) / " + m_ActiveField;

			// inform Listener, send SBC
			if (m_Listener.Length != 0)
			{
				string [] strFields;
				int i = 0;
				//Pst
				int myCount = 0;
				string fields = m_PST_Fields.Substring(0, m_PST_Fields.Length-1);
				strFields = fields.Split(',');
				for(i = 0; i < strFields.Length; i++)
				{
					string strRet="";
					string tmp = strFields[i].Replace("'","");
					strRet = tabLOG.GetLinesByIndexValue("FINA", tmp, 0);
					if(strRet != "")
					{
						myCount += Convert.ToInt32(strRet);
					}
				}
				string sumPST = myCount.ToString();

				//Gatt
				myCount = 0;
				fields = m_GAT_Fields.Substring(0, m_GAT_Fields.Length-1);
				strFields = fields.Split(',');
				for(i = 0; i < strFields.Length; i++)
				{
					string strRet="";
					string tmp = strFields[i].Replace("'","");
					strRet = tabLOG.GetLinesByIndexValue("FINA", tmp, 0);
					if(strRet != "")
					{
						myCount += Convert.ToInt32(strRet);
					}
				}
				string sumGAT = myCount.ToString();

				//Blt
				myCount = 0;
				fields = m_BLT_Fields.Substring(0, m_BLT_Fields.Length-1);
				strFields = fields.Split(',');
				for(i = 0; i < strFields.Length; i++)
				{
					string strRet="";
					string tmp = strFields[i].Replace("'","");
					strRet = tabLOG.GetLinesByIndexValue("FINA", tmp, 0);
					if(strRet != "")
					{
						myCount += Convert.ToInt32(strRet);
					}
				}
				string sumBLT = myCount.ToString();

				//WRO
				myCount = 0;
				fields = m_WRO_Fields.Substring(0, m_WRO_Fields.Length-1);
				strFields = fields.Split(',');
				for(i = 0; i < strFields.Length; i++)
				{
					string strRet="";
					string tmp = strFields[i].Replace("'","");
					strRet = tabLOG.GetLinesByIndexValue("FINA", tmp, 0);
					if(strRet != "")
					{
						myCount += Convert.ToInt32(strRet);
					}
				}
				string sumWRO = myCount.ToString();

				//Cca
				myCount = 0;
				fields = m_CCA_Fields.Substring(0, m_CCA_Fields.Length);
				strFields = fields.Split(',');
				for(i = 0; i < strFields.Length; i++)
				{
					string strRet="";
					string tmp = strFields[i].Replace("'","");
					strRet = tabLOG.GetLinesByIndexValue("FINA", tmp, 0);
					if(strRet != "")
					{
						myCount += Convert.ToInt32(strRet);
					}
				}
				string sumCCA = myCount.ToString();	
				string strListener = sumChanges + "," + sumPST  + "," + sumGAT + "," + sumBLT + "," + sumWRO + "," + sumCCA;
				//MessageBox.Show(this, strListener);
				

				IUfisComWriter myUfisCom = UT.GetUfisCom();
				int ilRet = myUfisCom.CallServer("SBC", "BCLOCC", "LOC_CHANGES", strListener, m_Listener, "230");
				if(ilRet != 0)
				{
					string strERR = myUfisCom.LastErrorMessage;
					MessageBox.Show(this, strERR);
				}
			}
		}


		private void FillSelection()
		{
			//clear table
			tabLOG.ResetContent();

			//part of selection for TIME
			FillTIME();

			if (myboolDoingLogtab)
				DoDeletedFlights();

			//part of selection for UREF
  			if (m_ViewMode == "FIELD")
			{
				FillUREF(m_TableName,"URNO");
				if (m_TableName.Equals("CCA"))
					ReadSelectionCCATAB();
				else
				{
					ReadSelection();
					if (myNeedCCA.Length != 0)
					{
						FillUREF("CCA","URNO");
						ReadSelectionCCATAB();
					}
				}
			}
			else if (m_ViewMode == "LOCATIONS")
			{
				if (myboolDoingLogtab)
					FillUREF("LOG","Flight");
				else
					FillUREF(m_TableName,"URNO");

				ReadSelection();
				if (myNeedCCA.Length != 0)
				{
					if (myboolDoingLogtab)
					{
						DataForm.LoadCCAData(myStrCcaWhereFLNUIn);
					}

					FillUREF("CCA","URNO");
					ReadSelectionCCATAB();
				}
			}

			//show complete table
			tabLOG.Refresh();

			//sort table by FLNO
			if (m_ViewMode == "LOCATIONS")
				tabLOG_SortColumn(-1, 0);

			// after selection has been done all data is init in utc-times
			myIsTimeInUtc = true;
			HandleUTC();
			myIsInit = true;

			tabLOG.AutoSizeColumns();
			// inform Listener, send SBC
			InformListener();
		}

		private void btnApply_Click(object sender, System.EventArgs e)
		{
			FillSelection();
		}

		private void btnPrint_Click(object sender, System.EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;
			string strTimeframe = " - Timeframe : " + dateTimePickerFrom.Value.ToShortDateString() + " / " + dateTimePickerFrom.Value.ToShortTimeString() + " - " + dateTimePickerTo.Value.ToShortDateString() + " / " + dateTimePickerTo.Value.ToShortTimeString();
			string strUtcLocal = "LOCAL";
			if (myIsTimeInUtc == true)
				strUtcLocal = "UTC";
			strUtcLocal += strTimeframe;

			string strTitel = "Data Changes: " + m_FieldName;
			if (m_ViewMode == "LOCATIONS")
				strTitel = "Location Changes (" + lblLocations.Text + ")";

			ActiveReport1 rpt = new ActiveReport1(tabLOG, strUtcLocal, strTitel, m_FieldType, UT.UserName);
			frmPrintPreview preview = new frmPrintPreview(rpt);
			preview.Show();
			this.Cursor = Cursors.Arrow;
		}

		private void frmMain_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			DataForm.Close();
			//Store current geometry in Registry
			WriteRegistry();

			//send close (-1) to m_Listener
			if (m_Listener.Length != 0)
			{
				IUfisComWriter myUfisCom = UT.GetUfisCom();
				int ilRet = myUfisCom.CallServer("SBC", "BCLOCC", "LOC_CHANGES", "-1", m_Listener, "230");
				if(ilRet != 0)
				{
					string strERR = myUfisCom.LastErrorMessage;
					MessageBox.Show(this, strERR);
				}
			}
		}

		private void lblChanges_Click(object sender, System.EventArgs e)
		{
		
		}

		private void numericUpDown1_ValueChanged(object sender, System.EventArgs e)
		{

		}
	}
}
