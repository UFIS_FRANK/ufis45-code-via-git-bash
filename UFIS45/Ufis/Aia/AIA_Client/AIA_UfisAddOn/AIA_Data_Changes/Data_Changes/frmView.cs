using System;
using System.IO;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Microsoft.Win32;
using Ufis.Utils;
using Ufis.Data;

namespace Data_Changes
{
	/// <summary>
	/// Summary description for frmView.
	/// </summary>
	public class frmView : System.Windows.Forms.Form
	{
		#region ----- myMembers

		IDatabase myDB = null;
		string myEndSign = "'";
		string myEndSignLike = "%'";
		public string mySelection = "";
		public string myWindowText = "";
		public ArrayList myLocation = new ArrayList();
//		string mySelRotation = " [ROTATIONS]";
		string mySelFlnu = " AND FLTN like '";
		string mySelFlns = " AND FLNS = '";
		string mySelAlc2 = " AND ALC2 = '";
		string mySelAlc3 = " AND ALC3 = '";
		string mySelAct3 = " AND ACT3 = '";
		string mySelAct5 = " AND ACT5 = '";
		string mySelTtyp = " AND TTYP = '";
		string mySelStyp = " AND STYP = '";
		string mySelRegn = " AND REGN = '";
		string mySaveFile = "C:\\Ufis\\System\\DATACHANGE_VIEW.txt";
		public static frmView myThis;
		private bool m_ACRloaded = false;
		#endregion ----- myMembers

		private System.Windows.Forms.ImageList imageList2;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.ImageList imageButtons;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.PictureBox pictureBoxButton;
		private System.Windows.Forms.TextBox txtFLNUValue;
		private System.Windows.Forms.Button btnNow;
		public System.Windows.Forms.DateTimePicker dateTimePickerTo;
		private System.Windows.Forms.Label lblTo;
		public System.Windows.Forms.DateTimePicker dateTimePickerFrom;
		private System.Windows.Forms.Label lblFrom;
		private System.Windows.Forms.CheckBox cbPosition;
		private System.Windows.Forms.Button btnACR;
		private System.Windows.Forms.Button btnSTY;
		private System.Windows.Forms.Button btnNAT;
		private System.Windows.Forms.Button btnALC;
		private System.Windows.Forms.TextBox txtFLTSValue;
		private System.Windows.Forms.TextBox txtREGNValue;
		private System.Windows.Forms.TextBox txtSTYValue;
		private System.Windows.Forms.TextBox txtNATValue;
		private System.Windows.Forms.Label lblREGN;
		private System.Windows.Forms.Label lblSTY;
		private System.Windows.Forms.Label lblNAT;
		private System.Windows.Forms.Label lblFLNU;
		private System.Windows.Forms.Label lblALC;
		private System.Windows.Forms.CheckBox cbUTC;
		private System.Windows.Forms.CheckBox cbCheckin;
		private System.Windows.Forms.CheckBox cbLounge;
		private System.Windows.Forms.CheckBox cbBelt;
		private System.Windows.Forms.CheckBox cbGate;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Button btnNew;
		private System.Windows.Forms.Button btnApply;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.TextBox txtACTUrno;
		private System.Windows.Forms.Button btnACT;
		private System.Windows.Forms.TextBox txtACTValue;
		private System.Windows.Forms.Label lblACT;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtALTValue;
		private System.Windows.Forms.TextBox txtALT3Value;
		private System.Windows.Forms.TextBox txtACT5Value;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.CheckBox chDeleted;
		private System.Windows.Forms.CheckBox cbArrival;
		private System.Windows.Forms.CheckBox cbDeparture;
		private System.ComponentModel.IContainer components;
		public frmView()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			myThis = this;
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		public static frmView GetThis()
		{
			return myThis;
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmView));
			this.imageList2 = new System.Windows.Forms.ImageList(this.components);
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.imageButtons = new System.Windows.Forms.ImageList(this.components);
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.pictureBoxButton = new System.Windows.Forms.PictureBox();
			this.txtFLNUValue = new System.Windows.Forms.TextBox();
			this.btnNow = new System.Windows.Forms.Button();
			this.dateTimePickerTo = new System.Windows.Forms.DateTimePicker();
			this.lblTo = new System.Windows.Forms.Label();
			this.dateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
			this.lblFrom = new System.Windows.Forms.Label();
			this.cbPosition = new System.Windows.Forms.CheckBox();
			this.btnACR = new System.Windows.Forms.Button();
			this.btnSTY = new System.Windows.Forms.Button();
			this.btnNAT = new System.Windows.Forms.Button();
			this.btnALC = new System.Windows.Forms.Button();
			this.txtFLTSValue = new System.Windows.Forms.TextBox();
			this.txtREGNValue = new System.Windows.Forms.TextBox();
			this.txtALTValue = new System.Windows.Forms.TextBox();
			this.txtSTYValue = new System.Windows.Forms.TextBox();
			this.txtNATValue = new System.Windows.Forms.TextBox();
			this.lblREGN = new System.Windows.Forms.Label();
			this.lblSTY = new System.Windows.Forms.Label();
			this.lblNAT = new System.Windows.Forms.Label();
			this.lblFLNU = new System.Windows.Forms.Label();
			this.lblALC = new System.Windows.Forms.Label();
			this.cbUTC = new System.Windows.Forms.CheckBox();
			this.cbCheckin = new System.Windows.Forms.CheckBox();
			this.cbLounge = new System.Windows.Forms.CheckBox();
			this.cbBelt = new System.Windows.Forms.CheckBox();
			this.cbGate = new System.Windows.Forms.CheckBox();
			this.btnClose = new System.Windows.Forms.Button();
			this.btnNew = new System.Windows.Forms.Button();
			this.btnApply = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.txtACTUrno = new System.Windows.Forms.TextBox();
			this.btnACT = new System.Windows.Forms.Button();
			this.txtACTValue = new System.Windows.Forms.TextBox();
			this.lblACT = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.txtALT3Value = new System.Windows.Forms.TextBox();
			this.txtACT5Value = new System.Windows.Forms.TextBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.chDeleted = new System.Windows.Forms.CheckBox();
			this.cbArrival = new System.Windows.Forms.CheckBox();
			this.cbDeparture = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// imageList2
			// 
			this.imageList2.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit;
			this.imageList2.ImageSize = new System.Drawing.Size(19, 19);
			this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
			this.imageList2.TransparentColor = System.Drawing.Color.White;
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(19, 19);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.White;
			// 
			// imageButtons
			// 
			this.imageButtons.ImageSize = new System.Drawing.Size(19, 19);
			this.imageButtons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageButtons.ImageStream")));
			this.imageButtons.TransparentColor = System.Drawing.Color.White;
			// 
			// pictureBox1
			// 
			this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
			this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pictureBox1.Location = new System.Drawing.Point(0, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(400, 430);
			this.pictureBox1.TabIndex = 99;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
			// 
			// pictureBoxButton
			// 
			this.pictureBoxButton.BackColor = System.Drawing.Color.Transparent;
			this.pictureBoxButton.Dock = System.Windows.Forms.DockStyle.Top;
			this.pictureBoxButton.Location = new System.Drawing.Point(0, 0);
			this.pictureBoxButton.Name = "pictureBoxButton";
			this.pictureBoxButton.Size = new System.Drawing.Size(400, 36);
			this.pictureBoxButton.TabIndex = 98;
			this.pictureBoxButton.TabStop = false;
			this.pictureBoxButton.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxButton_Paint);
			// 
			// txtFLNUValue
			// 
			this.txtFLNUValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtFLNUValue.Location = new System.Drawing.Point(136, 184);
			this.txtFLNUValue.Name = "txtFLNUValue";
			this.txtFLNUValue.Size = new System.Drawing.Size(96, 20);
			this.txtFLNUValue.TabIndex = 2;
			this.txtFLNUValue.Tag = "FLNU_VAL";
			this.txtFLNUValue.Text = "";
			this.txtFLNUValue.TextChanged += new System.EventHandler(this.txtFLNUValue_TextChanged);
			// 
			// btnNow
			// 
			this.btnNow.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btnNow.ImageIndex = 4;
			this.btnNow.ImageList = this.imageList1;
			this.btnNow.Location = new System.Drawing.Point(272, 136);
			this.btnNow.Name = "btnNow";
			this.btnNow.Size = new System.Drawing.Size(20, 20);
			this.btnNow.TabIndex = 31;
			this.btnNow.TabStop = false;
			this.toolTip1.SetToolTip(this.btnNow, "Change the time to now");
			this.btnNow.Click += new System.EventHandler(this.btnNow_Click);
			// 
			// dateTimePickerTo
			// 
			this.dateTimePickerTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerTo.Location = new System.Drawing.Point(136, 136);
			this.dateTimePickerTo.Name = "dateTimePickerTo";
			this.dateTimePickerTo.Size = new System.Drawing.Size(128, 20);
			this.dateTimePickerTo.TabIndex = 1;
			// 
			// lblTo
			// 
			this.lblTo.BackColor = System.Drawing.Color.Transparent;
			this.lblTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblTo.Location = new System.Drawing.Point(48, 136);
			this.lblTo.Name = "lblTo";
			this.lblTo.Size = new System.Drawing.Size(64, 16);
			this.lblTo.TabIndex = 24;
			this.lblTo.Text = "Flights To:";
			// 
			// dateTimePickerFrom
			// 
			this.dateTimePickerFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerFrom.Location = new System.Drawing.Point(136, 112);
			this.dateTimePickerFrom.Name = "dateTimePickerFrom";
			this.dateTimePickerFrom.Size = new System.Drawing.Size(128, 20);
			this.dateTimePickerFrom.TabIndex = 0;
			// 
			// lblFrom
			// 
			this.lblFrom.BackColor = System.Drawing.Color.Transparent;
			this.lblFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblFrom.Location = new System.Drawing.Point(48, 112);
			this.lblFrom.Name = "lblFrom";
			this.lblFrom.Size = new System.Drawing.Size(80, 16);
			this.lblFrom.TabIndex = 23;
			this.lblFrom.Text = "Flights From:";
			// 
			// cbPosition
			// 
			this.cbPosition.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbPosition.BackColor = System.Drawing.Color.Transparent;
			this.cbPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbPosition.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.cbPosition.ImageIndex = 2;
			this.cbPosition.Location = new System.Drawing.Point(0, 48);
			this.cbPosition.Name = "cbPosition";
			this.cbPosition.Size = new System.Drawing.Size(80, 24);
			this.cbPosition.TabIndex = 18;
			this.cbPosition.TabStop = false;
			this.cbPosition.Text = "&Position";
			this.cbPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.toolTip1.SetToolTip(this.cbPosition, "Select/deselect the position source");
			this.cbPosition.CheckedChanged += new System.EventHandler(this.cbPosition_CheckedChanged);
			// 
			// btnACR
			// 
			this.btnACR.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btnACR.ImageIndex = 15;
			this.btnACR.ImageList = this.imageList2;
			this.btnACR.Location = new System.Drawing.Point(272, 344);
			this.btnACR.Name = "btnACR";
			this.btnACR.Size = new System.Drawing.Size(20, 20);
			this.btnACR.TabIndex = 36;
			this.btnACR.TabStop = false;
			this.toolTip1.SetToolTip(this.btnACR, "Open look-up list for Registrations");
			this.btnACR.Click += new System.EventHandler(this.btnACR_Click);
			// 
			// btnSTY
			// 
			this.btnSTY.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btnSTY.ImageIndex = 15;
			this.btnSTY.ImageList = this.imageList2;
			this.btnSTY.Location = new System.Drawing.Point(272, 312);
			this.btnSTY.Name = "btnSTY";
			this.btnSTY.Size = new System.Drawing.Size(20, 20);
			this.btnSTY.TabIndex = 35;
			this.btnSTY.TabStop = false;
			this.toolTip1.SetToolTip(this.btnSTY, "Open look-up list for Service types");
			this.btnSTY.Click += new System.EventHandler(this.btnSTY_Click);
			// 
			// btnNAT
			// 
			this.btnNAT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btnNAT.ImageIndex = 15;
			this.btnNAT.ImageList = this.imageList2;
			this.btnNAT.Location = new System.Drawing.Point(272, 280);
			this.btnNAT.Name = "btnNAT";
			this.btnNAT.Size = new System.Drawing.Size(20, 20);
			this.btnNAT.TabIndex = 34;
			this.btnNAT.TabStop = false;
			this.toolTip1.SetToolTip(this.btnNAT, "Open look-up list for Nature codes");
			this.btnNAT.Click += new System.EventHandler(this.btnNAT_Click);
			// 
			// btnALC
			// 
			this.btnALC.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btnALC.ImageIndex = 15;
			this.btnALC.ImageList = this.imageList2;
			this.btnALC.Location = new System.Drawing.Point(272, 216);
			this.btnALC.Name = "btnALC";
			this.btnALC.Size = new System.Drawing.Size(20, 20);
			this.btnALC.TabIndex = 32;
			this.btnALC.TabStop = false;
			this.toolTip1.SetToolTip(this.btnALC, "Open look-up list for Airlines");
			this.btnALC.Click += new System.EventHandler(this.btnALC_Click);
			// 
			// txtFLTSValue
			// 
			this.txtFLTSValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtFLTSValue.Location = new System.Drawing.Point(240, 184);
			this.txtFLTSValue.Name = "txtFLTSValue";
			this.txtFLTSValue.Size = new System.Drawing.Size(24, 20);
			this.txtFLTSValue.TabIndex = 3;
			this.txtFLTSValue.Tag = "FLTS_VAL";
			this.txtFLTSValue.Text = "";
			// 
			// txtREGNValue
			// 
			this.txtREGNValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtREGNValue.Location = new System.Drawing.Point(136, 344);
			this.txtREGNValue.Name = "txtREGNValue";
			this.txtREGNValue.Size = new System.Drawing.Size(128, 20);
			this.txtREGNValue.TabIndex = 10;
			this.txtREGNValue.Tag = "REGNU_VAL";
			this.txtREGNValue.Text = "";
			this.txtREGNValue.Leave += new System.EventHandler(this.txtREGNValue_Leave);
			// 
			// txtALTValue
			// 
			this.txtALTValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtALTValue.Location = new System.Drawing.Point(136, 216);
			this.txtALTValue.Name = "txtALTValue";
			this.txtALTValue.Size = new System.Drawing.Size(64, 20);
			this.txtALTValue.TabIndex = 4;
			this.txtALTValue.Tag = "ALTU_VAL";
			this.txtALTValue.Text = "";
			this.txtALTValue.Leave += new System.EventHandler(this.txtALTValue_Leave);
			// 
			// txtSTYValue
			// 
			this.txtSTYValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtSTYValue.Location = new System.Drawing.Point(136, 312);
			this.txtSTYValue.Name = "txtSTYValue";
			this.txtSTYValue.Size = new System.Drawing.Size(128, 20);
			this.txtSTYValue.TabIndex = 9;
			this.txtSTYValue.Tag = "STYU_VAL";
			this.txtSTYValue.Text = "";
			this.txtSTYValue.Leave += new System.EventHandler(this.txtSTYValue_Leave);
			// 
			// txtNATValue
			// 
			this.txtNATValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtNATValue.Location = new System.Drawing.Point(136, 280);
			this.txtNATValue.Name = "txtNATValue";
			this.txtNATValue.Size = new System.Drawing.Size(128, 20);
			this.txtNATValue.TabIndex = 8;
			this.txtNATValue.Tag = "NATU_VAL";
			this.txtNATValue.Text = "";
			this.txtNATValue.Leave += new System.EventHandler(this.txtNATValue_Leave);
			// 
			// lblREGN
			// 
			this.lblREGN.BackColor = System.Drawing.Color.Transparent;
			this.lblREGN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblREGN.Location = new System.Drawing.Point(48, 344);
			this.lblREGN.Name = "lblREGN";
			this.lblREGN.Size = new System.Drawing.Size(72, 16);
			this.lblREGN.TabIndex = 30;
			this.lblREGN.Text = "Registration:";
			// 
			// lblSTY
			// 
			this.lblSTY.BackColor = System.Drawing.Color.Transparent;
			this.lblSTY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblSTY.Location = new System.Drawing.Point(48, 312);
			this.lblSTY.Name = "lblSTY";
			this.lblSTY.Size = new System.Drawing.Size(72, 16);
			this.lblSTY.TabIndex = 29;
			this.lblSTY.Text = "Servie Type:";
			// 
			// lblNAT
			// 
			this.lblNAT.BackColor = System.Drawing.Color.Transparent;
			this.lblNAT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblNAT.Location = new System.Drawing.Point(48, 280);
			this.lblNAT.Name = "lblNAT";
			this.lblNAT.Size = new System.Drawing.Size(80, 16);
			this.lblNAT.TabIndex = 28;
			this.lblNAT.Text = "Nature Code:";
			// 
			// lblFLNU
			// 
			this.lblFLNU.BackColor = System.Drawing.Color.Transparent;
			this.lblFLNU.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblFLNU.Location = new System.Drawing.Point(48, 184);
			this.lblFLNU.Name = "lblFLNU";
			this.lblFLNU.Size = new System.Drawing.Size(80, 16);
			this.lblFLNU.TabIndex = 25;
			this.lblFLNU.Text = "Flightnumber:";
			// 
			// lblALC
			// 
			this.lblALC.BackColor = System.Drawing.Color.Transparent;
			this.lblALC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblALC.Location = new System.Drawing.Point(48, 216);
			this.lblALC.Name = "lblALC";
			this.lblALC.Size = new System.Drawing.Size(64, 16);
			this.lblALC.TabIndex = 26;
			this.lblALC.Text = "Airline:";
			// 
			// cbUTC
			// 
			this.cbUTC.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbUTC.BackColor = System.Drawing.Color.Transparent;
			this.cbUTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbUTC.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.cbUTC.ImageIndex = 16;
			this.cbUTC.ImageList = this.imageList1;
			this.cbUTC.Location = new System.Drawing.Point(0, -1);
			this.cbUTC.Name = "cbUTC";
			this.cbUTC.Size = new System.Drawing.Size(80, 36);
			this.cbUTC.TabIndex = 12;
			this.cbUTC.TabStop = false;
			this.cbUTC.Text = "&UTC";
			this.cbUTC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.toolTip1.SetToolTip(this.cbUTC, "Switch to UTC/local times");
			this.cbUTC.CheckedChanged += new System.EventHandler(this.cbUTC_CheckedChanged);
			// 
			// cbCheckin
			// 
			this.cbCheckin.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbCheckin.BackColor = System.Drawing.Color.Transparent;
			this.cbCheckin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbCheckin.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.cbCheckin.ImageIndex = 2;
			this.cbCheckin.Location = new System.Drawing.Point(320, 48);
			this.cbCheckin.Name = "cbCheckin";
			this.cbCheckin.Size = new System.Drawing.Size(80, 24);
			this.cbCheckin.TabIndex = 22;
			this.cbCheckin.TabStop = false;
			this.cbCheckin.Text = "&Check-In";
			this.cbCheckin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.toolTip1.SetToolTip(this.cbCheckin, "Select/deselect the Check-in source");
			this.cbCheckin.CheckedChanged += new System.EventHandler(this.cbCheckin_CheckedChanged);
			// 
			// cbLounge
			// 
			this.cbLounge.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbLounge.BackColor = System.Drawing.Color.Transparent;
			this.cbLounge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbLounge.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.cbLounge.ImageIndex = 2;
			this.cbLounge.Location = new System.Drawing.Point(240, 48);
			this.cbLounge.Name = "cbLounge";
			this.cbLounge.Size = new System.Drawing.Size(80, 24);
			this.cbLounge.TabIndex = 21;
			this.cbLounge.TabStop = false;
			this.cbLounge.Text = "&Lounge";
			this.cbLounge.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.toolTip1.SetToolTip(this.cbLounge, "Select/deselect the Lounge source");
			this.cbLounge.CheckedChanged += new System.EventHandler(this.cbLounge_CheckedChanged);
			// 
			// cbBelt
			// 
			this.cbBelt.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbBelt.BackColor = System.Drawing.Color.Transparent;
			this.cbBelt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbBelt.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.cbBelt.ImageIndex = 2;
			this.cbBelt.Location = new System.Drawing.Point(160, 48);
			this.cbBelt.Name = "cbBelt";
			this.cbBelt.Size = new System.Drawing.Size(80, 24);
			this.cbBelt.TabIndex = 20;
			this.cbBelt.TabStop = false;
			this.cbBelt.Text = "&Baggage";
			this.cbBelt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.toolTip1.SetToolTip(this.cbBelt, "Select/deselect the Baggage source");
			this.cbBelt.CheckedChanged += new System.EventHandler(this.cbBelt_CheckedChanged);
			// 
			// cbGate
			// 
			this.cbGate.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbGate.BackColor = System.Drawing.Color.Transparent;
			this.cbGate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbGate.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.cbGate.ImageIndex = 2;
			this.cbGate.Location = new System.Drawing.Point(80, 48);
			this.cbGate.Name = "cbGate";
			this.cbGate.Size = new System.Drawing.Size(80, 24);
			this.cbGate.TabIndex = 19;
			this.cbGate.TabStop = false;
			this.cbGate.Text = "&Gate";
			this.cbGate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.toolTip1.SetToolTip(this.cbGate, "Select/deselect the Gate source");
			this.cbGate.CheckedChanged += new System.EventHandler(this.cbGate_CheckedChanged);
			// 
			// btnClose
			// 
			this.btnClose.BackColor = System.Drawing.Color.Transparent;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnClose.ImageIndex = 0;
			this.btnClose.ImageList = this.imageButtons;
			this.btnClose.Location = new System.Drawing.Point(320, -1);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(80, 36);
			this.btnClose.TabIndex = 16;
			this.btnClose.TabStop = false;
			this.btnClose.Text = "&Close";
			this.toolTip1.SetToolTip(this.btnClose, "Close the view window");
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// btnNew
			// 
			this.btnNew.BackColor = System.Drawing.Color.Transparent;
			this.btnNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnNew.ImageIndex = 3;
			this.btnNew.ImageList = this.imageButtons;
			this.btnNew.Location = new System.Drawing.Point(240, -1);
			this.btnNew.Name = "btnNew";
			this.btnNew.Size = new System.Drawing.Size(80, 36);
			this.btnNew.TabIndex = 15;
			this.btnNew.TabStop = false;
			this.btnNew.Text = "&New";
			this.toolTip1.SetToolTip(this.btnNew, "Define a new current view");
			this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
			// 
			// btnApply
			// 
			this.btnApply.BackColor = System.Drawing.Color.Transparent;
			this.btnApply.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnApply.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnApply.ImageIndex = 1;
			this.btnApply.ImageList = this.imageButtons;
			this.btnApply.Location = new System.Drawing.Point(160, -1);
			this.btnApply.Name = "btnApply";
			this.btnApply.Size = new System.Drawing.Size(80, 36);
			this.btnApply.TabIndex = 14;
			this.btnApply.TabStop = false;
			this.btnApply.Text = "&Apply";
			this.toolTip1.SetToolTip(this.btnApply, "Apply the current view definition and load the data");
			this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
			// 
			// btnSave
			// 
			this.btnSave.BackColor = System.Drawing.Color.Transparent;
			this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnSave.ImageIndex = 5;
			this.btnSave.ImageList = this.imageButtons;
			this.btnSave.Location = new System.Drawing.Point(80, -1);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(80, 36);
			this.btnSave.TabIndex = 13;
			this.btnSave.TabStop = false;
			this.btnSave.Text = "&Save";
			this.toolTip1.SetToolTip(this.btnSave, "Save the current view definition");
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// txtACTUrno
			// 
			this.txtACTUrno.Location = new System.Drawing.Point(336, 248);
			this.txtACTUrno.Name = "txtACTUrno";
			this.txtACTUrno.Size = new System.Drawing.Size(64, 20);
			this.txtACTUrno.TabIndex = 11;
			this.txtACTUrno.TabStop = false;
			this.txtACTUrno.Tag = "ACTU";
			this.txtACTUrno.Text = "";
			this.txtACTUrno.Visible = false;
			// 
			// btnACT
			// 
			this.btnACT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btnACT.ImageIndex = 15;
			this.btnACT.ImageList = this.imageList2;
			this.btnACT.Location = new System.Drawing.Point(272, 248);
			this.btnACT.Name = "btnACT";
			this.btnACT.Size = new System.Drawing.Size(20, 20);
			this.btnACT.TabIndex = 33;
			this.btnACT.TabStop = false;
			this.toolTip1.SetToolTip(this.btnACT, "Open look-up list for A/C Types");
			this.btnACT.Click += new System.EventHandler(this.btnACT_Click);
			// 
			// txtACTValue
			// 
			this.txtACTValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtACTValue.Location = new System.Drawing.Point(136, 248);
			this.txtACTValue.Name = "txtACTValue";
			this.txtACTValue.Size = new System.Drawing.Size(64, 20);
			this.txtACTValue.TabIndex = 6;
			this.txtACTValue.Tag = "ACTU_VAL";
			this.txtACTValue.Text = "";
			this.txtACTValue.Leave += new System.EventHandler(this.txtACTValue_Leave);
			// 
			// lblACT
			// 
			this.lblACT.BackColor = System.Drawing.Color.Transparent;
			this.lblACT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblACT.Location = new System.Drawing.Point(48, 248);
			this.lblACT.Name = "lblACT";
			this.lblACT.Size = new System.Drawing.Size(80, 16);
			this.lblACT.TabIndex = 27;
			this.lblACT.Text = "Aircraft Type:";
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.SystemColors.ControlText;
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.Lime;
			this.label1.Location = new System.Drawing.Point(0, 36);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(400, 14);
			this.label1.TabIndex = 17;
			this.label1.Text = "Include Location Changes for :";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.label1.Click += new System.EventHandler(this.label1_Click);
			// 
			// txtALT3Value
			// 
			this.txtALT3Value.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtALT3Value.Location = new System.Drawing.Point(200, 216);
			this.txtALT3Value.Name = "txtALT3Value";
			this.txtALT3Value.Size = new System.Drawing.Size(64, 20);
			this.txtALT3Value.TabIndex = 5;
			this.txtALT3Value.Tag = "ALT3U_VAL";
			this.txtALT3Value.Text = "";
			this.txtALT3Value.TextChanged += new System.EventHandler(this.txtALT3Value_TextChanged);
			this.txtALT3Value.Leave += new System.EventHandler(this.txtALT3Value_Leave);
			// 
			// txtACT5Value
			// 
			this.txtACT5Value.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtACT5Value.Location = new System.Drawing.Point(200, 248);
			this.txtACT5Value.Name = "txtACT5Value";
			this.txtACT5Value.Size = new System.Drawing.Size(64, 20);
			this.txtACT5Value.TabIndex = 7;
			this.txtACT5Value.Tag = "ACT5U_VAL";
			this.txtACT5Value.Text = "";
			this.txtACT5Value.Leave += new System.EventHandler(this.txtACT5Value_Leave);
			// 
			// chDeleted
			// 
			this.chDeleted.Location = new System.Drawing.Point(136, 160);
			this.chDeleted.Name = "chDeleted";
			this.chDeleted.Size = new System.Drawing.Size(120, 24);
			this.chDeleted.TabIndex = 100;
			this.chDeleted.Text = "All Changes for ...";
			this.chDeleted.CheckedChanged += new System.EventHandler(this.chDeleted_CheckedChanged);
			// 
			// cbArrival
			// 
			this.cbArrival.Location = new System.Drawing.Point(272, 160);
			this.cbArrival.Name = "cbArrival";
			this.cbArrival.TabIndex = 101;
			this.cbArrival.Text = "Arrival";
			this.cbArrival.CheckedChanged += new System.EventHandler(this.cbArrival_CheckedChanged);
			// 
			// cbDeparture
			// 
			this.cbDeparture.Location = new System.Drawing.Point(272, 184);
			this.cbDeparture.Name = "cbDeparture";
			this.cbDeparture.TabIndex = 102;
			this.cbDeparture.Text = "Departure";
			this.cbDeparture.CheckedChanged += new System.EventHandler(this.cbDeparture_CheckedChanged);
			// 
			// frmView
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(400, 430);
			this.Controls.Add(this.cbDeparture);
			this.Controls.Add(this.cbArrival);
			this.Controls.Add(this.chDeleted);
			this.Controls.Add(this.txtACT5Value);
			this.Controls.Add(this.txtALT3Value);
			this.Controls.Add(this.txtFLNUValue);
			this.Controls.Add(this.txtFLTSValue);
			this.Controls.Add(this.txtREGNValue);
			this.Controls.Add(this.txtALTValue);
			this.Controls.Add(this.txtSTYValue);
			this.Controls.Add(this.txtNATValue);
			this.Controls.Add(this.txtACTUrno);
			this.Controls.Add(this.txtACTValue);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btnNow);
			this.Controls.Add(this.dateTimePickerTo);
			this.Controls.Add(this.lblTo);
			this.Controls.Add(this.dateTimePickerFrom);
			this.Controls.Add(this.lblFrom);
			this.Controls.Add(this.cbPosition);
			this.Controls.Add(this.btnACR);
			this.Controls.Add(this.btnSTY);
			this.Controls.Add(this.btnNAT);
			this.Controls.Add(this.btnALC);
			this.Controls.Add(this.lblREGN);
			this.Controls.Add(this.lblSTY);
			this.Controls.Add(this.lblNAT);
			this.Controls.Add(this.lblFLNU);
			this.Controls.Add(this.lblALC);
			this.Controls.Add(this.cbUTC);
			this.Controls.Add(this.cbCheckin);
			this.Controls.Add(this.cbLounge);
			this.Controls.Add(this.cbBelt);
			this.Controls.Add(this.cbGate);
			this.Controls.Add(this.btnClose);
			this.Controls.Add(this.btnNew);
			this.Controls.Add(this.btnApply);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.btnACT);
			this.Controls.Add(this.lblACT);
			this.Controls.Add(this.pictureBoxButton);
			this.Controls.Add(this.pictureBox1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmView";
			this.Text = "Location Changes: Define Flights and Locations";
			this.Resize += new System.EventHandler(this.frmView_Resize);
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmView_Closing);
			this.Load += new System.EventHandler(this.frmView_Load);
			this.Activated += new System.EventHandler(this.frmView_Activated);
			this.ResumeLayout(false);

		}
		#endregion

		private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			//UT.DrawGradient(e.Graphics, 90f, pictureBox1, Color.WhiteSmoke, Color.LightGray);
			UT.DrawGradient(e.Graphics, 90f, pictureBox1, Color.AliceBlue, Color.LightSteelBlue);//Color.CornflowerBlue);

		}

		private void frmView_Resize(object sender, System.EventArgs e)
		{
			pictureBox1.Invalidate();
			pictureBoxButton.Invalidate();
		}

		private void frmView_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();

			cbUTC.Parent = pictureBoxButton;
			btnSave.Parent = pictureBoxButton;
			btnApply.Parent = pictureBoxButton;
			btnNew.Parent = pictureBoxButton;
			btnClose.Parent = pictureBoxButton;

			btnNow.Parent = pictureBox1;
			btnALC.Parent = pictureBox1;
			btnACT.Parent = pictureBox1;
			btnNAT.Parent = pictureBox1;
			btnSTY.Parent = pictureBox1;
			btnACR.Parent = pictureBox1;

			lblFrom.Parent = pictureBox1;
			lblTo.Parent = pictureBox1;
			lblFLNU.Parent = pictureBox1;
			lblALC.Parent = pictureBox1;
			lblACT.Parent = pictureBox1;
			lblNAT.Parent = pictureBox1;
			lblSTY.Parent = pictureBox1;
			lblREGN.Parent = pictureBox1;

			cbPosition.Parent = pictureBox1;
			cbGate.Parent = pictureBox1;
			cbBelt.Parent = pictureBox1;
			cbLounge.Parent = pictureBox1;
			cbCheckin.Parent = pictureBox1;
			chDeleted.Parent = pictureBox1;
			cbArrival.Parent = pictureBox1;
			cbDeparture.Parent = pictureBox1;

			dateTimePickerFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dateTimePickerFrom.Value = new DateTime(dateTimePickerFrom.Value.Year, dateTimePickerFrom.Value.Month, dateTimePickerFrom.Value.Day, 0,0,0,0);
			dateTimePickerTo.CustomFormat = "dd.MM.yyyy - HH:mm";
			dateTimePickerTo.Value = new DateTime(dateTimePickerTo.Value.Year, dateTimePickerTo.Value.Month, dateTimePickerTo.Value.Day, 23,59,59,0);
			InitWithFile();
			CheckChecked();
			Check_chDeleted();
			ReadRegistry();
			this.Focus();
			dateTimePickerFrom.Focus();
		}

		private void WriteRegistry()
		{
			string theKey = "Software\\UFIS\\DATACHANGES\\ViewDialog";
			RegistryKey rk = Registry.CurrentUser.CreateSubKey(theKey);
			if(rk == null)
			{
				rk.CreateSubKey(theKey);
				rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			}

			rk.SetValue("X", this.Left.ToString());
			rk.SetValue("Y", this.Top.ToString());
			rk.SetValue("Width", this.Width.ToString());
			rk.SetValue("Height", this.Height.ToString());
			rk.Close();
		}

		private void ReadRegistry()
		{
			int myX=-1, myY=-1, myW=-1, myH=-1;
			string regVal = "";
			string theKey = "Software\\UFIS\\DATACHANGES\\ViewDialog";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk != null)
			{
				regVal = rk.GetValue("X", "-1").ToString();
				myX = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Y", "-1").ToString();
				myY = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Width", "-1").ToString();
				myW = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Height", "-1").ToString();
				myH = Convert.ToInt32(regVal);
			}
			if(myW != -1 && myH != -1)
			{
				this.Left = myX;
				this.Top = myY;
				this.Width = myW;
				this.Height = myH;
			}
		}

		private void InitWithFile()
		{
			string txtLine;
			string[] txtArray;
			UT.IsTimeInUtc = false;


			FileStream fin;
			try
			{
				fin = new FileStream(mySaveFile, FileMode.OpenOrCreate);
			}
			catch (IOException exc)
			{
				MessageBox.Show(this, mySaveFile, exc.Message + "Read Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return;
			}
					

			StreamReader fstr_in = new StreamReader(fin);
//			StreamReader datei = File.OpenText(mySaveFile);
//			while (datei.Peek() != -1)
			while ((txtLine = fstr_in.ReadLine()) != null)
			{
				cbUTC.Text = "Local";
//				txtLine = datei.ReadLine();
				txtArray = txtLine.Split(new char[] {'|'});

				if (txtArray.Length >= 13)
				{
					string[] cbArray = txtArray[1].Split(new char[] {','});
					for (int i = 0; i < cbArray.Length; i++)
					{
						if (cbArray.GetValue(i).ToString() == "1")
						{
							if (i == 0)
							{
								cbUTC.Checked = true;
								UT.IsTimeInUtc = true;
								cbUTC.Text = "UTC";
							}
							if (i == 1)
								cbPosition.Checked = true;
							if (i == 2)
								cbGate.Checked = true;
							if (i == 3)
								cbBelt.Checked = true;
							if (i == 4)
								cbLounge.Checked = true;
							if (i == 5)
								cbCheckin.Checked = true;
						}
					}
														   
					dateTimePickerFrom.Value = UT.CedaTimeToDateTime(txtArray[2]);
					dateTimePickerTo.Value = UT.CedaTimeToDateTime(txtArray[3]);
					txtFLNUValue.Text  = txtArray[4];
					txtFLTSValue.Text  = txtArray[5];
					txtALTValue.Text   = txtArray[6];
					txtALT3Value.Text  = txtArray[7];
					txtACTValue.Text   = txtArray[8];
					txtACT5Value.Text  = txtArray[9];
					txtNATValue.Text   = txtArray[10];
					txtSTYValue.Text   = txtArray[11];
					txtREGNValue.Text  = txtArray[12];
				}

				chDeleted.Checked = false;
				cbArrival.Checked = false;
				cbDeparture.Checked = false;
				if (txtArray.Length == 16)
				{
					if (txtArray[13] == "1")
						chDeleted.Checked = true;
					if (txtArray[14] == "1")
						cbArrival.Checked = true;
					if (txtArray[15] == "1")
						cbDeparture.Checked = true;
				}

				if (chDeleted.Checked == false)
				{
					cbArrival.Visible = false;
					cbDeparture.Visible = false;
				}
			}
			Check_chDeleted();
			CheckChecked();
//			datei.Close();
			fstr_in.Close();
		}

		private void btnACT_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.currTable = "ACT";
			olDlg.lblCaption.Text = "Aircrafts";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtACTUrno.Text = strUrno;
				txtACTValue.Text = olDlg.tabList.GetColumnValue(sel,1);
				txtACTValue.ForeColor = Color.Black;
				txtACT5Value.Text = olDlg.tabList.GetColumnValue(sel,2);
				txtACT5Value.ForeColor = Color.Black;
			}
			CheckValid();
		}

		private void txtACTValue_Leave(object sender, System.EventArgs e)
		{
			string strValue = txtACTValue.Text;
			if(strValue == "")
			{
				txtACTUrno.Text = "";
				txtACTValue.ForeColor = Color.Black;
				return; //Need no check
			}
			ITable myTab = myDB["ACT"];
			bool blFound = false;
			for(int i = 0; i < myTab.Count && blFound == false; i++)
			{
				if(strValue == myTab[i]["IATA-Code"])
				{
					txtACTUrno.Text = myTab[i]["URNO"];
					blFound = true;
					break;
				}
			}

			if(blFound == true)
				txtACTValue.ForeColor = Color.Black;
			else
				txtACTValue.ForeColor = Color.Red;

			CheckValid();
		}

		private void btnNow_Click(object sender, System.EventArgs e)
		{
			DateTime olNow;
			if(UT.IsTimeInUtc == true)
			{
				olNow = DateTime.UtcNow;
			}
			else
			{
				olNow = DateTime.Now;
			}
			dateTimePickerTo.Value = olNow;
		}

		private void CheckChecked()
		{
			if(cbPosition.Checked || cbGate.Checked || cbBelt.Checked || cbLounge.Checked || cbCheckin.Checked)
				label1.ForeColor = Color.Lime;
			else
				label1.ForeColor = Color.Red;

			CheckValid();
		}

		private bool CheckValid()
		{
			btnApply.Enabled = true;
			btnSave.Enabled = true;

			if (chDeleted.Checked == true)
			{
				if (cbArrival.Checked == false && cbDeparture.Checked == false)
				{
					MessageBox.Show(this, "Arrival and/or Departure has to be checked");
					btnApply.Enabled = false;
					btnSave.Enabled = false;
					return false;
				}

				if (txtFLNUValue.Text.Equals("") || txtALT3Value.Text.Equals(""))
				{
					MessageBox.Show(this, "Flightnumber and Airline(ICAO-Code) has to be filled");
					btnApply.Enabled = false;
					btnSave.Enabled = false;
					return false;
				}

				if (txtFLNUValue.ForeColor  == Color.Red ||	txtALT3Value.ForeColor  == Color.Red)
				{
					btnApply.Enabled = false;
					btnSave.Enabled = false;
					return false;
				}

			}
			else
			{
				if (label1.ForeColor == Color.Red
					||	txtFLNUValue.ForeColor  == Color.Red
					||	txtFLTSValue.ForeColor  == Color.Red
					||	txtALTValue.ForeColor   == Color.Red
					||	txtALT3Value.ForeColor  == Color.Red
					||	txtACTValue.ForeColor   == Color.Red
					||	txtACT5Value.ForeColor  == Color.Red
					||	txtNATValue.ForeColor   == Color.Red
					||	txtSTYValue.ForeColor   == Color.Red
					||	txtREGNValue.ForeColor  == Color.Red )
				{
					btnApply.Enabled = false;
					btnSave.Enabled = false;
					//btnApply.BackColor = Color.Red;
					//btnSave.BackColor = Color.Red;
					return false;
				}
			}

//			btnApply.Enabled = true;
//			btnSave.Enabled = true;
//			btnApply.BackColor = Color.Transparent;
//			btnSave.BackColor = Color.Transparent;
			return true;
		}

		private void cbPosition_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbPosition.Checked == true)
				cbPosition.BackColor = Color.LightGreen;
			else
				cbPosition.BackColor = Color.Transparent;

			cbPosition.Refresh();
			CheckChecked();
		}

		private void cbGate_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbGate.Checked == true)
				cbGate.BackColor = Color.LightGreen;
			else
				cbGate.BackColor = Color.Transparent;

			cbGate.Refresh();
			CheckChecked();
		}

		private void cbBelt_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbBelt.Checked == true)
				cbBelt.BackColor = Color.LightGreen;
			else
				cbBelt.BackColor = Color.Transparent;

			cbBelt.Refresh();
			CheckChecked();
		}

		private void cbLounge_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbLounge.Checked == true)
				cbLounge.BackColor = Color.LightGreen;
			else
				cbLounge.BackColor = Color.Transparent;

			cbLounge.Refresh();
			CheckChecked();
		}

		private void cbCheckin_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbCheckin.Checked == true)
				cbCheckin.BackColor = Color.LightGreen;
			else
				cbCheckin.BackColor = Color.Transparent;

			cbCheckin.Refresh();
			CheckChecked();
		}

		private void pictureBoxButton_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			//UT.DrawGradient(e.Graphics, 90f, pictureBoxButton, Color.WhiteSmoke, Color.LightGray);
			UT.DrawGradient(e.Graphics, 90f, pictureBoxButton, Color.AliceBlue, Color.LightSteelBlue);//Color.CornflowerBlue);

		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void cbUTC_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbUTC.Checked == true)
			{
				cbUTC.Text = "UTC";
				//cbUTC.BackColor = Color.LightGreen;
				UT.IsTimeInUtc = true;
			}
			else
			{
				cbUTC.Text = "Local";
				//cbUTC.BackColor = Color.Transparent;
				UT.IsTimeInUtc = false;
			}

			cbUTC.Refresh();
		}

		private void txtNATValue_Leave(object sender, System.EventArgs e)
		{
			string strValue = txtNATValue.Text;
			if(strValue == "")
			{
				txtACTUrno.Text = "";
				txtNATValue.ForeColor = Color.Black;
				return; //Need no check
			}
			ITable myTab = myDB["NAT"];
			bool blFound = false;

			string [] strArr = strValue.Split('/');
			string strValueTtyp = "";
			if (strArr.Length >= 1)
				strValueTtyp = strArr[0];

			for(int i = 0; i < myTab.Count && blFound == false; i++)
			{
				string t = myTab[i]["TTYP"];
				if(strValueTtyp == myTab[i]["Type"])
				{
					txtACTUrno.Text = myTab[i]["URNO"];
					blFound = true;
					break;
				}
			}
			if(blFound == true)
				txtNATValue.ForeColor = Color.Black;
			else
				txtNATValue.ForeColor = Color.Red;

			CheckValid();
		}

		private void txtSTYValue_Leave(object sender, System.EventArgs e)
		{
			string strValue = txtSTYValue.Text;
			if(strValue == "")
			{
				txtACTUrno.Text = "";
				txtSTYValue.ForeColor = Color.Black;
				return; //Need no check
			}
			ITable myTab = myDB["STY"];
			bool blFound = false;

			string [] strArr = strValue.Split('/');
			string strValueStyp = "";
			if (strArr.Length >= 1)
				strValueStyp = strArr[0];

			for(int i = 0; i < myTab.Count && blFound == false; i++)
			{
				if(strValueStyp == myTab[i]["Type"])
				{
					txtACTUrno.Text = myTab[i]["URNO"];
					blFound = true;
					break;
				}
			}
			if(blFound == true)
				txtSTYValue.ForeColor = Color.Black;
			else
				txtSTYValue.ForeColor = Color.Red;

			CheckValid();
		}

		private void btnALC_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.currTable = "ALT";
			olDlg.lblCaption.Text = "Airlines";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtACTUrno.Text = strUrno;
				txtALTValue.Text = olDlg.tabList.GetColumnValue(sel,1);
				txtALTValue.ForeColor = Color.Black;
				txtALT3Value.Text = olDlg.tabList.GetColumnValue(sel,2);
				txtALT3Value.ForeColor = Color.Black;
			}
			CheckValid();
		}

		private void btnNAT_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.currTable = "NAT";
			olDlg.lblCaption.Text = "Natures";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtACTUrno.Text = strUrno;
				txtNATValue.Text = olDlg.tabList.GetColumnValue(sel,1) + "/" + olDlg.tabList.GetColumnValue(sel,2);
				txtNATValue.ForeColor = Color.Black;
			}
			CheckValid();
		}

		private void btnSTY_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.currTable = "STY";
			olDlg.lblCaption.Text = "Services";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtACTUrno.Text = strUrno;
				txtSTYValue.Text = olDlg.tabList.GetColumnValue(sel,1) + "/" + olDlg.tabList.GetColumnValue(sel,2);
				txtSTYValue.ForeColor = Color.Black;
			}
			CheckValid();
		}

		private void btnACR_Click(object sender, System.EventArgs e)
		{
			if (!m_ACRloaded)
			{
				DialogResult results = 	MessageBox.Show(this, "Will you load the ACRTAB ? (This could take some time!)", "Loading ACRTAB ?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
				if(results == DialogResult.Yes)
				{
					this.Cursor = Cursors.WaitCursor;
					m_ACRloaded = true;
					frmData DataForm = new frmData();
					DataForm.Hide();
					DataForm.LoadACRData();
					this.Cursor = Cursors.Arrow;
				}
			}

			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.currTable = "ACR";
			olDlg.lblCaption.Text = "Registrations";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtACTUrno.Text = strUrno;
				string val1 = olDlg.tabList.GetColumnValue(sel,1);
				string val2 = olDlg.tabList.GetColumnValue(sel,2);
				string val3 = olDlg.tabList.GetColumnValue(sel,3);
				string strValue = val1 + "/" + val2 + "-" + val3;
				txtREGNValue.Text = strValue;
				txtREGNValue.ForeColor = Color.Black;
			}
			CheckValid();
		}

		private void txtALTValue_Leave(object sender, System.EventArgs e)
		{
			string strValue = txtALTValue.Text;
			if(strValue == "")
			{
				txtACTUrno.Text = "";
				txtALTValue.ForeColor = Color.Black;
				return; //Need no check
			}
			ITable myTab = myDB["ALT"];
			bool blFound = false;
			for(int i = 0; i < myTab.Count && blFound == false; i++)
			{
				if(strValue == myTab[i]["IATA-Code"])
				{
					txtALT3Value.Text = myTab[i]["ICAO-Code"];
					txtACTUrno.Text = myTab[i]["URNO"];
					blFound = true;
					break;
				}
			}
			if(blFound == true)
				txtALTValue.ForeColor = Color.Black;
			else
				txtALTValue.ForeColor = Color.Red;

			CheckValid();
		}

		private void txtREGNValue_Leave(object sender, System.EventArgs e)
		{
			string strValue = txtREGNValue.Text;
			if(strValue == "")
			{
				txtACTUrno.Text = "";
				txtREGNValue.ForeColor = Color.Black;
				return; //Need no check
			}
			ITable myTab = myDB["ACR"];
			bool blFound = false;

			if (myTab == null)
				return;

			string [] strArr = strValue.Split('/');
			string strValueRegn = "";
			if (strArr.Length >= 1)
				strValueRegn = strArr[0];

			for(int i = 0; i < myTab.Count && blFound == false; i++)
			{
				if(strValueRegn == myTab[i]["Registration"])
				{
					txtACTUrno.Text = myTab[i]["URNO"];
					blFound = true;
					break;
				}
			}
			if(blFound == true)
				txtREGNValue.ForeColor = Color.Black;
			else
				txtREGNValue.ForeColor = Color.Red;

			CheckValid();
		}

		private void label1_Click(object sender, System.EventArgs e)
		{
			if (label1.ForeColor == Color.Red)
			{
				cbPosition.Checked = true;
				cbBelt.Checked = true;
				cbGate.Checked = true;
				cbLounge.Checked = true;
				cbCheckin.Checked = true;
				label1.ForeColor = Color.Lime;
			}
			else
			{
				cbPosition.Checked = false;
				cbBelt.Checked = false;
				cbGate.Checked = false;
				cbLounge.Checked = false;
				cbCheckin.Checked = false;
				label1.ForeColor = Color.Red;
			}
			CheckChecked();
		}

		private void btnNew_Click(object sender, System.EventArgs e)
		{
			txtACTValue.Text = "";
			txtACT5Value.Text = "";
			txtALTValue.Text = "";
			txtALT3Value.Text = "";
			txtFLNUValue.Text = "";
			txtFLTSValue.Text = "";
			txtNATValue.Text = "";
			txtREGNValue.Text = "";
			txtSTYValue.Text = "";
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			if (!CheckValid())
			{
				this.DialogResult = DialogResult.None; 
				return;
			}
			else
				this.DialogResult = DialogResult.OK; 

			this.Hide();

			string txtSep  = "|";
			string txtChecked = "";
			string txtView = "<Default>";
			txtChecked	+=	cbUTC.Checked		? "1," : "0,";
			txtChecked	+=	cbPosition.Checked	? "1," : "0,";
			txtChecked	+=	cbGate.Checked		? "1," : "0,";
			txtChecked	+=	cbBelt.Checked		? "1," : "0,";
			txtChecked	+=	cbLounge.Checked	? "1," : "0,";
			txtChecked	+=	cbCheckin.Checked	? "1"  : "0";

			string strFrom = UT.DateTimeToCeda( dateTimePickerFrom.Value );
			string strTo   = UT.DateTimeToCeda( dateTimePickerTo.Value );

			string txtFile = txtView + txtSep + txtChecked + txtSep + strFrom + txtSep + strTo + txtSep
				+ txtFLNUValue.Text + txtSep + txtFLTSValue.Text  + txtSep
				+ txtALTValue.Text  + txtSep + txtALT3Value.Text  + txtSep 
				+ txtACTValue.Text  + txtSep + txtACT5Value.Text  + txtSep
				+ txtNATValue.Text  + txtSep + txtSTYValue.Text   + txtSep
				+ txtREGNValue.Text + txtSep;

			txtChecked = "";
			txtChecked	+=	chDeleted.Checked		? "1" : "0";
			txtChecked += txtSep;
			txtChecked	+=	cbArrival.Checked		? "1" : "0";
			txtChecked += txtSep;
			txtChecked	+=	cbDeparture.Checked		? "1" : "0";
			txtFile += txtChecked;


			File.Delete(mySaveFile);
/*
  			StreamWriter datei = File.AppendText(mySaveFile);

			try
			{
				datei.WriteLine(txtFile);
			}
			catch (IOException exc)
			{
				MessageBox.Show(this, mySaveFile, exc.Message + "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
*/
			FileStream fout = null;
			try
			{
				fout = new FileStream(mySaveFile, FileMode.OpenOrCreate);
			}
			catch(IOException exc)
			{
				MessageBox.Show(this, mySaveFile, exc.Message + "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}

			StreamWriter fstr_out = new StreamWriter(fout);
			try
			{
				fstr_out.Write(txtFile);
			}
			catch(IOException exc)
			{
				MessageBox.Show(this, mySaveFile, exc.Message + "Write Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
			fstr_out.Close();
//			datei.Close();

			if (chDeleted.Checked == false)
			{
				FillSelection();
			}
			else
			{
				FillSelectionDeleted();
			}
		}

		private void FillSelectionDeleted()
		{
			string strFkey = "'";
			int ilPos = 5 - txtFLNUValue.Text.Length;
			for (int i=0; i<ilPos; i++)
			{
				strFkey += "0";
			}

			strFkey += txtFLNUValue.Text;


			strFkey += txtALT3Value.Text;

			string strFlts = txtFLTSValue.Text;
			if (txtFLTSValue.Text.Equals(""))
			{
				strFlts = "#";
			}
			strFkey += strFlts;

			DateTime datFrom = dateTimePickerFrom.Value;
			DateTime datTo = dateTimePickerTo.Value;
			DateTime datWhile = dateTimePickerFrom.Value;

			StringBuilder strFKEYS = new StringBuilder(1000);
			while(datWhile.Month <= datTo.Month && datWhile.Day <= datTo.Day && datWhile.Year <= datTo.Year)
			{
				string strMonth = datWhile.Month.ToString();
				string strDay   = datWhile.Day.ToString();
				if (strMonth.Length == 1)
					strMonth = "0" + strMonth;
				if (strDay.Length == 1)
					strDay = "0" + strDay;
				string strFrom = datWhile.Year.ToString() + strMonth + strDay;
				string strFkeyWhile = strFkey + strFrom;
				if (cbArrival.Checked == true)
					strFKEYS.Append("," + strFkeyWhile + "A'"); 
				if (cbDeparture.Checked == true)
					strFKEYS.Append("," + strFkeyWhile + "D'"); 

				datWhile = datWhile.AddDays(1);
			}

			if (strFKEYS.ToString().Length == 0)
				return;

			strFkey = strFKEYS.ToString().Substring(1, strFKEYS.ToString().Length-1);

			mySelection = "WHERE KEYF IN (" + strFkey + ")";
//			mySelection = "WHERE KEYF like '00111OAL#20050208%'";
			int z = 0;
/*
			"%05s%s%c%sD", prpDFlight->Fltn, prpDFlight->Alc3, clFlns, olSTAD.Format("%Y%m%d"));

			if (txtFLNUValue.Text != "")
				mySelection += mySelFlnu + txtFLNUValue.Text + myEndSignLike;
			if (txtFLTSValue.Text != "")
				mySelection += mySelFlns + txtFLTSValue.Text + myEndSign;
			if (txtALTValue.Text != "")
				mySelection += mySelAlc2 + txtALTValue.Text + myEndSign;
			if (txtALT3Value.Text != "")
				mySelection += mySelAlc3 + txtALT3Value.Text + myEndSign;
*/
			//mySelection = "WHERE ((TIFA BETWEEN '" + strFrom + "' AND '" + strTo + "') OR (TIFD BETWEEN '" + strFrom + "' and '" + strTo + "'))";
		}

		private void FillSelection()
		{
			string strFrom = UT.DateTimeToCeda( dateTimePickerFrom.Value );
			string strTo   = UT.DateTimeToCeda( dateTimePickerTo.Value );

			string txtTimeStyle = " [LOCAL] ";
			if (UT.IsTimeInUtc == true)
				txtTimeStyle = " [UTC] ";

			myWindowText = "Location Change:" + txtTimeStyle + "Timeframe for Flights [" + dateTimePickerFrom.Value.ToString() + "] - [" + 
				dateTimePickerTo.Value.ToString() + "]";

			DateTime datFrom = dateTimePickerFrom.Value;
			DateTime datTo   = dateTimePickerTo.Value;
			if (UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(datFrom);
				datTo   = UT.LocalToUtc(datTo);
			}
			strFrom = UT.DateTimeToCeda( datFrom );
			strTo   = UT.DateTimeToCeda( datTo );

			mySelection = "WHERE ((TIFA BETWEEN '" + strFrom + "' AND '" + strTo + "') OR (TIFD BETWEEN '" + strFrom + "' and '" + strTo + "'))";
			if (txtFLNUValue.Text != "")
//				mySelection += mySelFlnu + txtFLNUValue.Text + myEndSignLike;
				mySelection += mySelFlnu + txtFLNUValue.Text + myEndSign;
			if (txtFLTSValue.Text != "")
				mySelection += mySelFlns + txtFLTSValue.Text + myEndSign;
			if (txtALTValue.Text != "")
				mySelection += mySelAlc2 + txtALTValue.Text + myEndSign;
			if (txtALT3Value.Text != "")
				mySelection += mySelAlc3 + txtALT3Value.Text + myEndSign;
			if (txtACTValue.Text != "")
				mySelection += mySelAct3 + txtACTValue.Text + myEndSign;
			if (txtACT5Value.Text != "")
				mySelection += mySelAct5 + txtACT5Value.Text + myEndSign;
			if (txtNATValue.Text != "")
			{
				string [] strArr = txtNATValue.Text.Split('/');
				string strValueTtyp = "";
				if (strArr.Length >= 1)
					strValueTtyp = strArr[0];
				else
					strValueTtyp = txtNATValue.Text;

				mySelection += mySelTtyp + strValueTtyp + myEndSign;
			}
			if (txtSTYValue.Text != "")
			{
				string [] strArr = txtSTYValue.Text.Split('/');
				string strValueStyp = "";
				if (strArr.Length >= 1)
					strValueStyp = strArr[0];
				else
					strValueStyp = txtSTYValue.Text;

				mySelection += mySelStyp + strValueStyp + myEndSign;
			}
			if (txtREGNValue.Text != "")
			{
				string [] strArr = txtREGNValue.Text.Split('/');
				string strValueRegn = "";
				if (strArr.Length >= 1)
					strValueRegn = strArr[0];
				else
					strValueRegn = txtREGNValue.Text;

				mySelection += mySelRegn + strValueRegn + myEndSign;
			}

//			mySelection += mySelRotation;
//			mySelection += "";

			myLocation.Clear();
			if (cbPosition.Checked)
				myLocation.Add("PST");
			if (cbGate.Checked)
				myLocation.Add("GAT");
			if (cbBelt.Checked)
				myLocation.Add("BLT");
			if (cbLounge.Checked)
				myLocation.Add("WRO");
			if (cbCheckin.Checked)
				myLocation.Add("CCA");
		}


		private void txtALT3Value_Leave(object sender, System.EventArgs e)
		{
			string strValue = txtALT3Value.Text;
			if(strValue == "")
			{
				txtACTUrno.Text = "";
				txtALT3Value.ForeColor = Color.Black;
				return; //Need no check
			}
			ITable myTab = myDB["ALT"];
			bool blFound = false;
			for(int i = 0; i < myTab.Count && blFound == false; i++)
			{
				if(strValue == myTab[i]["ICAO-Code"])
				{
					txtALTValue.Text = myTab[i]["IATA-Code"];
					txtACTUrno.Text = myTab[i]["URNO"];
					blFound = true;
					break;
				}
			}

			if(blFound == true)
				txtALT3Value.ForeColor = Color.Black;
			else
				txtALT3Value.ForeColor = Color.Red;

			CheckValid();
		}

		private void txtACT5Value_Leave(object sender, System.EventArgs e)
		{
			string strValue = txtACT5Value.Text;
			if(strValue == "")
			{
				txtACTUrno.Text = "";
				txtACT5Value.ForeColor = Color.Black;
				return; //Need no check
			}
			ITable myTab = myDB["ACT"];
			bool blFound = false;
			for(int i = 0; i < myTab.Count && blFound == false; i++)
			{
				if(strValue == myTab[i]["ICAO-Code"])
				{
					txtACTUrno.Text = myTab[i]["URNO"];
					blFound = true;
					break;
				}
			}

			if(blFound == true)
				txtACT5Value.ForeColor = Color.Black;
			else
				txtACT5Value.ForeColor = Color.Red;

			CheckValid();
		}

		private void btnApply_Click(object sender, System.EventArgs e)
		{
			if (!CheckValid())
			{
				this.DialogResult = DialogResult.None; 
				return;
			}
			else
				this.DialogResult = DialogResult.OK; 


			this.Hide();
			if (chDeleted.Checked == false)
			{
				FillSelection();
			}
			else
			{
				FillSelectionDeleted();
			}

		}

		private void frmView_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			WriteRegistry();
		}

		private void frmView_Activated(object sender, System.EventArgs e)
		{
			dateTimePickerFrom.Focus();
		}

		private void Check_chDeleted()
		{
			if(chDeleted.Checked == true)
			{
				if (label1.ForeColor == Color.Red)
				{
					label1.ForeColor = Color.Lime;
				}
				label1.Enabled = false;
				btnApply.Enabled = true;
				btnSave.Enabled = true;
				cbPosition.Checked = true;
				cbGate.Checked = true;
				cbBelt.Checked = true;
				cbLounge.Checked = true;
				cbCheckin.Checked = true;

				btnApply.Enabled = true;
				btnSave.Enabled = true;

				cbPosition.Enabled = false;
				cbGate.Enabled = false;
				cbBelt.Enabled = false;
				cbLounge.Enabled = false;
				cbCheckin.Enabled = false;
				txtFLNUValue.BackColor = Color.Yellow;
				//				txtALTValue.BackColor = Color.Yellow;
				txtALT3Value.BackColor = Color.Yellow;
				cbArrival.Visible = true;
				cbDeparture.Visible = true;
				btnACR.Enabled = false;
				btnACT.Enabled = false;
				btnNAT.Enabled = false;
				btnSTY.Enabled = false;
				txtSTYValue.Enabled = false;
				txtNATValue.Enabled = false;
				txtACTValue.Enabled = false;
				txtACT5Value.Enabled = false;
				txtREGNValue.Enabled = false;
			}
			else
			{
				cbPosition.Enabled = true;
				cbGate.Enabled = true;
				cbBelt.Enabled = true;
				cbLounge.Enabled = true;
				cbCheckin.Enabled = true;
				label1.Enabled = true;
				CheckChecked();

				txtFLNUValue.BackColor = Color.White;
				//				txtALTValue.BackColor = Color.White;
				txtALT3Value.BackColor = Color.White;
				cbArrival.Visible = false;
				cbDeparture.Visible = false;
				btnACR.Enabled = true;
				btnACT.Enabled = true;
				btnNAT.Enabled = true;
				btnSTY.Enabled = true;
				txtSTYValue.Enabled = true;
				txtNATValue.Enabled = true;
				txtACTValue.Enabled = true;
				txtACT5Value.Enabled = true;
				txtREGNValue.Enabled = true;
			}
		}

		private void chDeleted_CheckedChanged(object sender, System.EventArgs e)
		{
			Check_chDeleted();
		}

		private void cbArrival_CheckedChanged(object sender, System.EventArgs e)
		{
			CheckValid();		
		}

		private void cbDeparture_CheckedChanged(object sender, System.EventArgs e)
		{
			CheckValid();		
		}

		private void txtFLNUValue_TextChanged(object sender, System.EventArgs e)
		{
			CheckValid();		
		}

		private void txtALT3Value_TextChanged(object sender, System.EventArgs e)
		{
			CheckValid();		
		}
	}
}
