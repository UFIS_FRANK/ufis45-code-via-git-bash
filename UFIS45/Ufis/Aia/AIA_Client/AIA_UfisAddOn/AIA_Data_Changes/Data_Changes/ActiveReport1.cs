using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using Ufis.Utils;

namespace Data_Changes
{
	public class ActiveReport1 : ActiveReport
	{
		AxTABLib.AxTAB myTab = null;
		private int currLine = 0;
		private string strTimeframe = "";
		private string strTitel = "";
		private string strValuType = "";
		private string strUser = "";

		public ActiveReport1(AxTABLib.AxTAB pTab, string sTimeframe, string sTitel, string sValuType, string sUser)
		{
			strTimeframe = sTimeframe;
			strTitel = sTitel;
			strValuType = sValuType;
			strUser = "by User: " + sUser;
			myTab = pTab;
			InitializeReport();
		}

		private void ActiveReport1_FetchData(object sender, DataDynamics.ActiveReports.ActiveReport.FetchEventArgs eArgs)
		{
			string strTime = "";
			string strDate = "";
			string strOval = "";
			string strValu = "";

			FLNO.Text = myTab.GetFieldValue(currLine, "UREF");

			string stoad = myTab.GetFieldValue(currLine, "STOA");
			if (stoad.Equals(" "))
				stoad = myTab.GetFieldValue(currLine, "STOD");
			if(!stoad.Equals(" "))
			{
				int length = stoad.Length;
				if (stoad.Length == 12)
					stoad += "00";
				DateTime d = UT.CedaFullDateToDateTime(stoad);
				strDate = d.ToShortDateString();
				strTime = d.ToShortTimeString();
				STOA.Text = strDate + " - " + strTime;
			}
			else
				STOA.Text = "";

			ADID.Text = myTab.GetFieldValue(currLine, "ADID");
			FINA.Text = myTab.GetFieldValue(currLine, "ADDI");
			OVAL.Text = myTab.GetFieldValue(currLine, "OVAL");
			VALU.Text = myTab.GetFieldValue(currLine, "VALU");
			TRAN.Text = myTab.GetFieldValue(currLine, "TRAN");

			if (strValuType.Equals("D"))
			{
				strOval = myTab.GetFieldValue(currLine, "OVAL");
				if(strOval != "")
				{
					DateTime d = UT.CedaFullDateToDateTime(strOval);
					strDate = d.ToShortDateString();
					strTime = d.ToShortTimeString();
					OVAL.Text = strDate + " - " + strTime;
				}
				else
					OVAL.Text = "";

				strValu = myTab.GetFieldValue(currLine, "VALU");
				if(strValu != "")
				{
					DateTime d = UT.CedaFullDateToDateTime(strValu);
					strDate = d.ToShortDateString();
					strTime = d.ToShortTimeString();
					VALU.Text = strDate + " - " + strTime;
				}
				else
					VALU.Text = "";
			}


			strTime = myTab.GetFieldValue(currLine, "TIME");
			if(strTime != "")
			{
				DateTime d = UT.CedaFullDateToDateTime(strTime);
				strDate = d.ToShortDateString();
				strTime = d.ToLongTimeString();
				TIME.Text = strDate + " - " + strTime;
			}
			else
				TIME.Text = "";

			USEC.Text = myTab.GetFieldValue(currLine, "USEC");

			if(currLine == myTab.GetLineCount())
			{
				//Hier mu� der abbruch gesetzt werden
				eArgs.EOF = true;
			}
			else
			{
				//OK weiter machen
				eArgs.EOF = false;
				currLine++;
			}
		}

		private void ActiveReport1_ReportStart(object sender, System.EventArgs eArgs)
		{
			DateTime d		= DateTime.Now;
			txtDate.Text	= d.ToString();
			TIMEFRAME.Text  = strTimeframe;
			txtUser.Text	= strUser;

			TITEL.Text = strTitel;
		}

		private void PageHeader_Format(object sender, System.EventArgs eArgs)
		{
			
		}

		#region ActiveReports Designer generated code
		private PageHeader PageHeader = null;
		private Picture Picture1 = null;
		private Label Label1 = null;
		private Label Label2 = null;
		private Label Label3 = null;
		private Label Label4 = null;
		private Label Label5 = null;
		private Label Label6 = null;
		private Label Label7 = null;
		private Label Label8 = null;
		private Label TITEL = null;
		private Label TIMEFRAME = null;
		private Label Label36 = null;
		private Detail Detail = null;
		private TextBox FLNO = null;
		private TextBox STOA = null;
		private TextBox ADID = null;
		private TextBox FINA = null;
		private TextBox OVAL = null;
		private TextBox VALU = null;
		private TextBox TIME = null;
		private TextBox USEC = null;
		private TextBox TRAN = null;
		private PageFooter PageFooter = null;
		private Label Label34 = null;
		private TextBox txtDate = null;
		private Label Label33 = null;
		private TextBox TextBox2 = null;
		private TextBox TextBox3 = null;
		private Label Label35 = null;
		private Label txtUser = null;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "Data_Changes.ActiveReport1.rpx");
			this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
			this.Picture1 = ((DataDynamics.ActiveReports.Picture)(this.PageHeader.Controls[0]));
			this.Label1 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[1]));
			this.Label2 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[2]));
			this.Label3 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[3]));
			this.Label4 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[4]));
			this.Label5 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[5]));
			this.Label6 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[6]));
			this.Label7 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[7]));
			this.Label8 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[8]));
			this.TITEL = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[9]));
			this.TIMEFRAME = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[10]));
			this.Label36 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[11]));
			this.FLNO = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[0]));
			this.STOA = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[1]));
			this.ADID = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[2]));
			this.FINA = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[3]));
			this.OVAL = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[4]));
			this.VALU = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[5]));
			this.TIME = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[6]));
			this.USEC = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[7]));
			this.TRAN = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[8]));
			this.Label34 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[0]));
			this.txtDate = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[1]));
			this.Label33 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[2]));
			this.TextBox2 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[3]));
			this.TextBox3 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[4]));
			this.Label35 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[5]));
			this.txtUser = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[6]));
			// Attach Report Events
			this.FetchData += new DataDynamics.ActiveReports.ActiveReport.FetchEventHandler(this.ActiveReport1_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport1_ReportStart);
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
		}

		#endregion
	}
}
