using System;
using System.Collections;
using System.Data;


using Microsoft.ApplicationBlocks.UIProcess;
using Ufis.BL.Services;


namespace Ufis.BL.Common
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class Controller : ControllerBase
	{
		static Basic	myBasicService;
		static TG		myTGService;

		public Controller(Navigator navi) : base(navi) 
		{

			if (myBasicService == null) 
			{
				myBasicService = new Basic(true);
			}
		}

		
		public void SelectService(string serviceName) 
		{

			this.State.NavigateValue = serviceName;
			this.Navigate();

		}

		
		public void RegisterService() 
		{

			myTGService	   = new TG(myBasicService);
		}


		public bool LoginToServer(string username,string password,out string loginError) 
		{

			LoginData();
			return myBasicService.LoginToServer(this.State["LOGIN_DATABASE"].ToString(),this.State["LOGIN_HOPO"].ToString(),this.State["LOGIN_TABEXT"].ToString(),username,password,"UIPAB_TG",out loginError);
		}


		public string WhereClause 
		{
			get 
			{
				if (this.State.Contains("LOAD_WHERECLAUSE")) 
				{
					return (string) this.State["LOAD_WHERECLAUSE"];
				} 
				else 
				{
					return "";
				}
			} 
			
			set 
			{

				if (this.State.Contains("LOAD_WHERECLAUSE")) 
				{
					  this.State["LOAD_WHERECLAUSE"] = value;
				} 
				else 
				{
					  this.State.Add("LOAD_WHERECLAUSE",value);
				}
			}
		}

		public string LastUsedFile 
		{
			get 
			{
				if (this.State.Contains("LOAD_LASTUSEDFILE")) 
				{
					return (string) this.State["LOAD_LASTUSEDFILE"];
				} 
				else 
				{
					return null;
				}
			} set 
			  {
				  if (this.State.Contains("LOAD_LASTUSEDFILE")) 
				  {
					  this.State["LOAD_LASTUSEDFILE"] = value;
				  } 
				  else 
				  {
					  this.State.Add("LOAD_LASTUSEDFILE",value);
				  }
			  }
		}

		public DateTime LoadFrom 
		{
			get 
			{
				if (this.State.Contains("LOAD_FROM")) 
				{
					return (DateTime) this.State["LOAD_FROM"];
				} 
				else 
				{
					DateTime dateTime = new DateTime(System.DateTime.UtcNow.Year , System.DateTime.UtcNow.Month , System.DateTime.UtcNow.Day , 0 , 0 , 0);
					return dateTime;
				}
			} set 
			  {


				  if (this.State.Contains("LOAD_FROM")) 
				  {
					  this.State["LOAD_FROM"] = value;
				  } 
				  else 
				  {
					  this.State.Add("LOAD_FROM",value);
				  }
			  }
		}

		public DateTime LoadTo 
		{
			get 
			{
				if (this.State.Contains("LOAD_TO")) 
				{
					return (DateTime) this.State["LOAD_TO"];
				} 
				else 
				{
					DateTime dateTime = new DateTime(System.DateTime.UtcNow.Year , System.DateTime.UtcNow.Month , System.DateTime.UtcNow.Day , 23 , 59 , 59);
					return dateTime;
				}
			} set 
			  {
				  if (this.State.Contains("LOAD_TO")) 
				  {
					  this.State["LOAD_TO"] = value;
				  } 
				  else 
				  {				  
					  this.State.Add("LOAD_TO",value);
				  }
			  }
		}


		public void LoginData()
		{
			
			if (this.State.Contains("LOGIN_DATABASE")) 
			{
				this.State["LOGIN_DATABASE"] = myBasicService.GetValuesFromIniFile("Global","HostName");
			} 
			else 
			{
				this.State.Add("LOGIN_DATABASE",myBasicService.GetValuesFromIniFile("Global","HostName"));
			}

			if (this.State.Contains("LOGIN_HOPO")) 
			{
				this.State["LOGIN_HOPO"] = myBasicService.GetValuesFromIniFile("Global","HomeAirport");
			} 
			else 
			{
				this.State.Add("LOGIN_HOPO",myBasicService.GetValuesFromIniFile("Global","HomeAirport"));
			}

			if (this.State.Contains("LOGIN_TABEXT")) 
			{
				this.State["LOGIN_TABEXT"] = myBasicService.GetValuesFromIniFile("Global","TABLEEXTENSION");
			} 
			else 
			{
				this.State.Add("LOGIN_TABEXT",myBasicService.GetValuesFromIniFile("Global","TABLEEXTENSION"));
			}
		}


		public DataRow CurrentRotation 
		{
			get 
			{
				if (this.State.Contains("CurrentRotationRow")) 
				{
					return (DataRow) this.State["CurrentRotationRow"];
				} 
				else 
				{
					return null;
				}
			} set 
			  {
				  if (this.State.Contains("CurrentRotationRow")) 
				  {
					  this.State["CurrentRotationRow"] = null;
					  this.State["CurrentRotationRow"] = value;
				  } 
				  else 
				  {
					  this.State.Add("CurrentRotationRow",value);
				  }
			  }
		}



		public DataSet InputDataSet 
		{
			get 
			{
				if (this.State.Contains("InputDataSet")) 
				{
					return (DataSet) this.State["InputDataSet"];
				} 
				else 
				{
					return null;
				}
			} set 
			  {
				  
				  if (this.State.Contains("InputDataSet")) 
				  {
					  this.State["InputDataSet"] = value;
				  } 
				  else 
				  {
					  this.State.Add("InputDataSet",value);
				  }

				  
			  }
		}
		
		public DataSet AODBDataSet 
		{
			get 
			{
				if (this.State.Contains("AODBDataSet")) 
				{
					return (DataSet) this.State["AODBDataSet"];
				} 
				else 
				{
					return null;
				}
			} set 
			  {
				  if (this.State.Contains("AODBDataSet")) 
				  {
					  this.State["AODBDataSet"] = value;
				  } 
				  else 
				  {
					  this.State.Add("AODBDataSet",value);
				  }
			  }
		}
	

		public bool LoadDatabase(object obj)
		{
			myTGService.SetTimeFrame(DateTime.Parse(this.LoadFrom.ToString()),DateTime.Parse(this.LoadTo.ToString()));
			return myTGService.LoadDatabase(obj);
		}
		
		public void CleanDatabase()
		{
			myBasicService.CleanDatabase();
		}

	
		public bool GetBusinessCaseDataInput(string businessCase)
		{
			DataSet ds = new DataSet();
			myTGService.GetBusinessCaseData(businessCase,out ds);

			if(this.InputDataSet != null)
				this.InputDataSet.Clear();
			
			this.InputDataSet = ds;

			return true;
		}

		public bool GetBusinessCaseDataAODB(string businessCase)
		{
			DataSet ds = new DataSet();
			myTGService.GetBusinessCaseData(businessCase,out ds);

			if(this.AODBDataSet != null)
				this.AODBDataSet.Clear();
			
			this.AODBDataSet = ds;
			return true;
		}


		public bool LoadAODB(object obj)
		{
			LoadDatabase(obj);	
			GetBusinessCaseDataAODB("LocationManagement");
			return true;
		}

		public bool Compare(string tableName, out DataTable dataTableImport, out DataTable dataTableAODB)
		{
			return myTGService.Compare(this.InputDataSet.Tables[tableName], this.AODBDataSet.Tables[tableName], out dataTableImport, out dataTableAODB);
		}


		public bool Validate(DataRow row,out ArrayList errorMessages)
		{
			return myTGService.Validate(row,out errorMessages);
		}

		
		public bool Save(DataSet ds,out ArrayList errorMessages)
		{
			return myTGService.Save(ds,out errorMessages);
		}

		
		public bool GetBusinessDataPattern(DataColumn column,out string pattern)
		{
			return myTGService.GetBusinessDataPattern(column,out pattern);			
		}

		public bool ConvertDataTablesDateTimes(DataTable input, out DataTable output)
		{
			return myTGService.ConvertDataTablesDateTimes(input, out output);
		}
		
		public bool InsertRow(DataTable dt,out ArrayList errorMessages)
		{
			if (myTGService.InsertRow(dt,out errorMessages))
			{
				/*				DataRow[] rows = dt.Select("","",DataViewRowState.Added);
								if (rows.Length == 1)
								{
									this.CurrentFlight = rows[0];
									SelectService("Insert");
									return true;
								}
								else
				*/					return false;
			}
			else

				return false;
		}

		
		public bool DeleteRow(DataRow row,out ArrayList errorMessages)
		{
			return myTGService.DeleteRow(row,out errorMessages);
		}

		
		public bool DisposeApplication()
		{
			return myBasicService.DisposeApplication();
		}

		
		public bool WriteXML(string location, DataSet ds)
		{
			return myBasicService.WriteXML(location, ds);
		}

		
		public bool LoadXML(string location, string businessCase)
		{
			if(this.InputDataSet != null)
				for(int i = 0; i < this.InputDataSet.Tables.Count; i++)
					this.InputDataSet.Tables[i].Clear();

			myTGService.SetBusinessDataSet(this.InputDataSet);

			myTGService.LoadXML(location, businessCase, this.InputDataSet);
			return true;
		}



		public bool FillRotationTable()
		{
			myTGService.SetBusinessDataSet(this.InputDataSet);
			myTGService.FillRotationTable(WhereClause);
			DataSet ds = new DataSet();
			myTGService.GetBusinessDataSet(out ds);
			ds = this.InputDataSet;
			return true;
		}

		public bool GetCorrespondingFlights(out DataRow dataRowArrival, out DataRow dataRowDeparture)
		{
			return 	myTGService.GetCorrespondingFlights(this.CurrentRotation, out dataRowArrival, out dataRowDeparture);
		}

		
		public DateTime DateTimeFromString(string dateTimeString)
		{
			DateTime output;
			myTGService.DateTimeFromString(dateTimeString, out output);
			return output;
		}

		public string StringFromDateTime(string dateTimeString)
		{
			string output = "";
			if(dateTimeString == "") 
				return output;
			DateTime dt;
			myTGService.DateTimeFromString(dateTimeString, out dt);
			
			output = dt.ToShortDateString() + " " + dt.ToShortTimeString();
			return output;
		}



		public bool SaveValueToCurrentInput(DataRow row, string column, string input)
		{
			return myTGService.SaveValueToCurrentInput(row, column, input);
		}

		public bool DeleteRotation()
		{
			return myTGService.DeleteFlightsFromDataSet(this.CurrentRotation);
		}

		

		public string DateTimeToString(DateTime inputDate)
		{
			string output = "";
			myTGService.DateTimeToString(inputDate, inputDate, out output);
			return output;
		}

		public string DateTimeToString(DateTime inputDate, DateTime inputTime)
		{
			string output = "";
			myTGService.DateTimeToString(inputDate, inputTime, out output);
			return output;
		}


		public bool SetInputTimeFrame()
		{
			DataView dataViewArrivalSorted = new DataView(InputDataSet.Tables["Flight"],"Type = 65","TimeframeArrival ASC",DataViewRowState.CurrentRows);
			DataView dataViewDepartureSorted = new DataView(InputDataSet.Tables["Flight"],"Type = 68","TimeframeDeparture ASC",DataViewRowState.CurrentRows);
			
			int stoaRow = 0;
			int stodRow = 0;
			//find first stoa and stod
			for(int i = 0; i < dataViewArrivalSorted.Count; i++)
			{
				if(dataViewArrivalSorted[i]["TimeframeArrival"].ToString() != "")
				{
					stoaRow = i;
					break;
				}
			}

			for(int i = 0; i < dataViewDepartureSorted.Count; i++)
			{
				if(dataViewDepartureSorted[i]["TimeframeDeparture"].ToString() != "")
				{
					stodRow = i;
					break;
				}
			}
			
			if(Int64.Parse(dataViewArrivalSorted[stoaRow]["TimeframeArrival"].ToString()) >= Int64.Parse(dataViewDepartureSorted[stodRow]["TimeframeDeparture"].ToString()))
				LoadFrom = DateTimeFromString(dataViewDepartureSorted[stodRow]["TimeframeDeparture"].ToString());
			else
				LoadFrom = DateTimeFromString(dataViewArrivalSorted[stoaRow]["TimeframeArrival"].ToString());

			if(Int64.Parse(dataViewArrivalSorted[dataViewArrivalSorted.Count-1]["TimeframeArrival"].ToString()) >= Int64.Parse(dataViewDepartureSorted[dataViewDepartureSorted.Count-1]["TimeframeDeparture"].ToString()))
				LoadTo = DateTimeFromString(dataViewArrivalSorted[dataViewArrivalSorted.Count-1]["TimeframeArrival"].ToString());
			else
				LoadTo = DateTimeFromString(dataViewDepartureSorted[dataViewDepartureSorted.Count-1]["TimeframeDeparture"].ToString());

			return true;
		}

		public bool Expand(DateTime importBegin, DateTime importEnd, DateTime outputBegin, DateTime outputEnd, int monday, int tuesday, int wednesday, int thursday, int friday, int saturday, int sunday)
		{
			return myTGService.Expand(WhereClause,importBegin, importEnd, outputBegin, outputEnd, monday, tuesday, wednesday, thursday, friday, saturday, sunday);
		}

		public bool DateTimeCompare(DateTime firstInput, DateTime secondInput, string operation)
		{
			return myTGService.DateTimeCompare(firstInput, secondInput, operation);
		}

		public bool GetFreeUrnoForSnapshot(DataTable table, out long urno)
		{
			return myTGService.GetFreeUrnoForSnapshot(table, out urno);
		}

	}
}
