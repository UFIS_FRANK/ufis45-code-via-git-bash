using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Ufis.BL.Common;
using Ufis.BL.Entities;

namespace Ufis.Snapshot.Win
{
	//public class TG : System.Windows.Forms.Form
	public class TG : Microsoft.ApplicationBlocks.UIProcess.WindowsFormView
	{
		private DataView dvLeft = null;
		private DataView dvRight = null;
		private System.Windows.Forms.DataGrid dataGridLeft;
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private DataSet	  dsLeft;
		private System.Windows.Forms.DataGrid dataGridRight;
		private System.Windows.Forms.MenuItem menuItem8;
		private System.Windows.Forms.MenuItem menuItem9;
		private System.Windows.Forms.MenuItem menuItem10;
		private System.Windows.Forms.MenuItem menuItem11;
		private System.Windows.Forms.MenuItem menuItemNew;
		private System.Windows.Forms.MenuItem menuItemOpen;
		private System.Windows.Forms.MenuItem menuItemClose;
		private System.Windows.Forms.MenuItem menuItemSave;
		private System.Windows.Forms.MenuItem menuItemExport;
		private System.Windows.Forms.MenuItem menuItemQuit;
		private System.Windows.Forms.MenuItem menuItemInsert;
		private System.Windows.Forms.MenuItem menuItemDelete;
		private System.Windows.Forms.MenuItem menuItemFilter;
		private System.Windows.Forms.MenuItem menuItemCompare;
		private System.Windows.Forms.StatusBar statusBar;
		private DataSet   dsRight;


		public TG()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			MyController.DisposeApplication();

			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dataGridLeft = new System.Windows.Forms.DataGrid();
			this.dataGridRight = new System.Windows.Forms.DataGrid();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItemNew = new System.Windows.Forms.MenuItem();
			this.menuItemOpen = new System.Windows.Forms.MenuItem();
			this.menuItemClose = new System.Windows.Forms.MenuItem();
			this.menuItemSave = new System.Windows.Forms.MenuItem();
			this.menuItemExport = new System.Windows.Forms.MenuItem();
			this.menuItemQuit = new System.Windows.Forms.MenuItem();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.menuItem8 = new System.Windows.Forms.MenuItem();
			this.menuItem9 = new System.Windows.Forms.MenuItem();
			this.menuItem10 = new System.Windows.Forms.MenuItem();
			this.menuItem11 = new System.Windows.Forms.MenuItem();
			this.menuItemInsert = new System.Windows.Forms.MenuItem();
			this.menuItemDelete = new System.Windows.Forms.MenuItem();
			this.menuItemFilter = new System.Windows.Forms.MenuItem();
			this.menuItemCompare = new System.Windows.Forms.MenuItem();
			this.statusBar = new System.Windows.Forms.StatusBar();
			((System.ComponentModel.ISupportInitialize)(this.dataGridLeft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridRight)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridLeft
			// 
			this.dataGridLeft.DataMember = "";
			this.dataGridLeft.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridLeft.Location = new System.Drawing.Point(16, 32);
			this.dataGridLeft.Name = "dataGridLeft";
			this.dataGridLeft.Size = new System.Drawing.Size(928, 288);
			this.dataGridLeft.TabIndex = 0;
			this.dataGridLeft.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataGridLeft_MouseUp);
			// 
			// dataGridRight
			// 
			this.dataGridRight.DataMember = "";
			this.dataGridRight.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridRight.Location = new System.Drawing.Point(16, 360);
			this.dataGridRight.Name = "dataGridRight";
			this.dataGridRight.Size = new System.Drawing.Size(928, 288);
			this.dataGridRight.TabIndex = 26;
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem1,
																					  this.menuItem8,
																					  this.menuItem9,
																					  this.menuItem10});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItemNew,
																					  this.menuItemOpen,
																					  this.menuItemClose,
																					  this.menuItemSave,
																					  this.menuItemExport,
																					  this.menuItemQuit});
			this.menuItem1.Text = "File";
			// 
			// menuItemNew
			// 
			this.menuItemNew.Index = 0;
			this.menuItemNew.Text = "New...";
			// 
			// menuItemOpen
			// 
			this.menuItemOpen.Index = 1;
			this.menuItemOpen.Text = "Open...";
			this.menuItemOpen.Click += new System.EventHandler(this.menuItemOpen_Click);
			// 
			// menuItemClose
			// 
			this.menuItemClose.Index = 2;
			this.menuItemClose.Text = "Close";
			this.menuItemClose.Click += new System.EventHandler(this.menuItemClose_Click);
			// 
			// menuItemSave
			// 
			this.menuItemSave.Index = 3;
			this.menuItemSave.Text = "Save";
			this.menuItemSave.Click += new System.EventHandler(this.menuItemSave_Click);
			// 
			// menuItemExport
			// 
			this.menuItemExport.Index = 4;
			this.menuItemExport.Text = "Export";
			this.menuItemExport.Click += new System.EventHandler(this.menuItemExport_Click);
			// 
			// menuItemQuit
			// 
			this.menuItemQuit.Index = 5;
			this.menuItemQuit.Text = "Quit";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(16, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(80, 23);
			this.label1.TabIndex = 12;
			this.label1.Text = "AODB View";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(16, 336);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(88, 23);
			this.label2.TabIndex = 13;
			this.label2.Text = "Export View";
			// 
			// menuItem8
			// 
			this.menuItem8.Index = 1;
			this.menuItem8.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItemInsert,
																					  this.menuItemDelete,
																					  this.menuItemFilter,
																					  this.menuItemCompare});
			this.menuItem8.Text = "Edit";
			// 
			// menuItem9
			// 
			this.menuItem9.Index = 2;
			this.menuItem9.Text = "View";
			// 
			// menuItem10
			// 
			this.menuItem10.Index = 3;
			this.menuItem10.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.menuItem11});
			this.menuItem10.Text = "Help";
			// 
			// menuItem11
			// 
			this.menuItem11.Index = 0;
			this.menuItem11.Text = "About";
			// 
			// menuItemInsert
			// 
			this.menuItemInsert.Index = 0;
			this.menuItemInsert.Text = "Insert";
			this.menuItemInsert.Click += new System.EventHandler(this.menuItemInsert_Click);
			// 
			// menuItemDelete
			// 
			this.menuItemDelete.Index = 1;
			this.menuItemDelete.Text = "Delete";
			this.menuItemDelete.Click += new System.EventHandler(this.menuItemDelete_Click);
			// 
			// menuItemFilter
			// 
			this.menuItemFilter.Index = 2;
			this.menuItemFilter.Text = "Filter";
			this.menuItemFilter.Click += new System.EventHandler(this.menuItemFilter_Click);
			// 
			// menuItemCompare
			// 
			this.menuItemCompare.Index = 3;
			this.menuItemCompare.Text = "Compare";
			this.menuItemCompare.Click += new System.EventHandler(this.menuItemCompare_Click);
			// 
			// statusBar
			// 
			this.statusBar.Location = new System.Drawing.Point(0, 657);
			this.statusBar.Name = "statusBar";
			this.statusBar.Size = new System.Drawing.Size(960, 24);
			this.statusBar.TabIndex = 27;
			// 
			// TG
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(960, 681);
			this.Controls.Add(this.statusBar);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.dataGridRight);
			this.Controls.Add(this.dataGridLeft);
			this.Menu = this.mainMenu1;
			this.Name = "TG";
			this.Text = "UFIS Snapshot-Tool";
			this.Load += new System.EventHandler(this.TG_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataGridLeft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridRight)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion



		private void TG_Load(object sender, EventArgs e)
		{
			string header = "";
			string lenstring = "";

			if (!MyController.LoadDatabase())
				return;

			
			if (MyController.GetBusinessCaseData("LocationManagement",out this.dsLeft))
			{
				this.dsLeft.AcceptChanges();
				
				this.dvLeft = new DataView(this.dsLeft.Tables["Flight"], "", "", DataViewRowState.CurrentRows);
				
				this.dataGridLeft.DataSource = this.dvLeft;
				//this.dataGridLeft.DataSource = this.dsLeft;

			}

			for(int i = 0; i < dsLeft.Tables["Flight"].Columns.Count; i++)
			{
				if( i+1 < dsLeft.Tables["Flight"].Columns.Count)
				{
					header += dsLeft.Tables["Flight"].Columns[i].Caption + ",";
					lenstring += "120,";
				}
				else
				{
					header += dsLeft.Tables["Flight"].Columns[i].Caption;
					lenstring += "120";
				}
			}



			for(int i = 0; i < dsLeft.Tables["Flight"].Rows.Count; i++)
			{
				string str="";
				for(int j = 0; j < dsLeft.Tables["Flight"].Columns.Count; j++)
				{
					switch (dsLeft.Tables["Flight"].Columns[j].DataType.FullName)
					{
						case "System.Int32":
							if( j+1 < dsLeft.Tables["Flight"].Columns.Count)
							{
								if (str.Length > 0)
									str += ',';
								str += ((int)dsLeft.Tables["Flight"].Rows[i][dsLeft.Tables["Flight"].Columns[j]]).ToString();
							}
							break;
						case "System.String":
							if( j+1 < dsLeft.Tables["Flight"].Columns.Count)
							{
								if (str.Length > 0)
									str += ',';
								str += dsLeft.Tables["Flight"].Rows[i][dsLeft.Tables["Flight"].Columns[j]];
							}
							break;
						case "System.DateTime":
							if( j+1 < dsLeft.Tables["Flight"].Columns.Count)
							{
								if (str.Length > 0)
									str += ',';
								str += dsLeft.Tables["Flight"].Rows[i][dsLeft.Tables["Flight"].Columns[j]];
							}
							break;
						default:
							//							if (dsLeft.Tables["Flight"].Columns[j].DataType is typeof(Enum))
							if (dsLeft.Tables["Flight"].Rows[i][dsLeft.Tables["Flight"].Columns[j]] != System.DBNull.Value ) 
							{
								byte valu = (byte)dsLeft.Tables["Flight"].Rows[i][dsLeft.Tables["Flight"].Columns[j]];
								if (str.Length > 0)
									str += ',';
								str += Enum.GetName(dsLeft.Tables["Flight"].Columns[j].DataType,valu);
							}
							break;
					}
				}
				
			}
			

		}

		private void tabFlights_SendLButtonDblClick(object sender, System.EventArgs e)
		{

		}





		private void selection_Click(object sender, System.EventArgs e)
		{
			this.dataGridLeft.Select(1);
		
		}

		private void dataGridLeft_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			System.Drawing.Point pt = new Point(e.X, e.Y);
			DataGrid.HitTestInfo hti = this.dataGridLeft.HitTest(pt);
			if(hti.Type == DataGrid.HitTestType.Cell)
			{
				this.dataGridLeft.CurrentCell = new DataGridCell(hti.Row,hti.Column);
				this.dataGridLeft.Select(hti.Row);	
			}
			markSelection();
		}

		private void markSelection()
		{
			this.dataGridLeft.SelectionForeColor = System.Drawing.Color.Blue;
			this.dataGridLeft.SelectionBackColor = System.Drawing.Color.White;
		}
		










		private void menuItemDelete_Click(object sender, System.EventArgs e)
		{		
			/*
			ArrayList errorMessages;
			
			if (!MyController.DeleteRow(this.dsLeft.Tables["Flight"].Rows[line],out errorMessages))
			{
				string msg = "";
				foreach(string errMsg in errorMessages)
				{
					if (msg.Length > 0)
					{
						msg += "\n";
					}

					msg += errMsg;
				}

				MessageBox.Show(this,msg);
				
			}
			else
			{

			}
			*/
		}

		private void menuItemInsert_Click(object sender, System.EventArgs e)
		{
			ArrayList errorMessages;

			if(this.dataGridLeft.Focus() == true) 
			{

				if (!MyController.InsertRow(this.dsLeft.Tables["Flight"],out errorMessages))
				{
					string msg = "";
					foreach(string errMsg in errorMessages)
					{
						if (msg.Length > 0)
						{
							msg += "\n";
						}

						msg += errMsg;
					}

					MessageBox.Show(this,msg);
				
				}
				else
				{
					DataRow row = MyController.CurrentFlight;
					string str="";
					for(int j = 0; j < dsLeft.Tables["Flight"].Columns.Count; j++)
					{
						if( j+1 < dsLeft.Tables["Flight"].Columns.Count)
						{
							str += row[dsLeft.Tables["Flight"].Columns[j].Caption] + ",";
						}
						else
						{
							str += row[dsLeft.Tables["Flight"].Columns[j].Caption];
						}
					}
				}
			}

			if(this.dataGridRight.Focus() == true) 
			{

				if (!MyController.InsertRow(this.dsLeft.Tables["Flight"],out errorMessages))
				{
					string msg = "";
					foreach(string errMsg in errorMessages)
					{
						if (msg.Length > 0)
						{
							msg += "\n";
						}

						msg += errMsg;
					}

					MessageBox.Show(this,msg);
				
				}
				else
				{
					DataRow row = MyController.CurrentFlight;
					string str="";
					for(int j = 0; j < dsLeft.Tables["Flight"].Columns.Count; j++)
					{
						if( j+1 < dsLeft.Tables["Flight"].Columns.Count)
						{
							str += row[dsLeft.Tables["Flight"].Columns[j].Caption] + ",";
						}
						else
						{
							str += row[dsLeft.Tables["Flight"].Columns[j].Caption];
						}
					}
				}
			}
		}

		private void menuItemFilter_Click(object sender, System.EventArgs e)
		{
			try
			{
				string filterLeft = "";
			
				this.dvLeft = new DataView(this.dsLeft.Tables["Flight"], filterLeft, "", DataViewRowState.CurrentRows);
				this.dataGridLeft.DataSource = this.dvLeft;
			
				this.statusBar.Text = this.dvLeft.Count.ToString() + " rows selected";

				string filterRight = "";

				this.dvRight = new DataView(this.dsRight.Tables["Flight"], filterRight, "", DataViewRowState.CurrentRows);
				this.dataGridRight.DataSource = this.dvRight;
				
				this.statusBar.Text = this.dvRight.Count.ToString() + " rows selected";

			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void menuItemCompare_Click(object sender, System.EventArgs e)
		{
		
		}

		private void menuItemOpen_Click(object sender, System.EventArgs e)
		{
			MyController.LoadXML("LocationManagement", out dsRight);
			
			this.dvRight = new DataView(this.dsRight.Tables["Flight"], "", "", DataViewRowState.CurrentRows);
			this.dataGridRight.DataSource = this.dvRight;
		}

		private void menuItemClose_Click(object sender, System.EventArgs e)
		{
			this.dvRight.Dispose();
		}

		private void menuItemSave_Click(object sender, System.EventArgs e)
		{
			ArrayList errorMessages;
			if (!MyController.Save(this.dsLeft,out errorMessages))
			{
				string msg = "";
				foreach(string errMsg in errorMessages)
				{
					if (msg.Length > 0)
					{
						msg += "\n";
					}

					msg += errMsg;
				}

				MessageBox.Show(this,msg);
			}
		
		}

		private void menuItemExport_Click(object sender, System.EventArgs e)
		{
			MyController.WriteXML(this.dsLeft);
		}

		private Controller MyController
		{
			get
			{
				return (Controller)base.Controller;
			}
		}
	}
}
