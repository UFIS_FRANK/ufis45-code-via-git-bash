using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Ufis.BL.Common;

namespace Ufis.Snapshot.Win
{
	/// <summary>
	/// Summary description for SnapshotExpanderLayout.
	/// </summary>
	public class SnapshotExpanderLayout : Microsoft.ApplicationBlocks.UIProcess.WindowsFormView
	{
		private System.Windows.Forms.Button buttonCreate;
		private System.Windows.Forms.Button buttonClose;
		private System.Windows.Forms.Button buttonUndo;
		private System.Windows.Forms.Button buttonCompare;
		private System.Windows.Forms.Button buttonCharts;
		private System.Windows.Forms.StatusBar statusBar1;
		private UfisDataGrid dataGridInputArrivalFlights;
		private UfisDataGrid dataGridInputDepartureFlights;
		private System.Data.DataSet dataSetInput;
		private System.Data.DataView dataViewArrival;
		private System.Data.DataView dataViewDeparture;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.DateTimePicker dateTimePickerImportFilterEnd;
		private System.Windows.Forms.DateTimePicker dateTimePickerImportFilterBegin;
		private bool init = false;
		private System.Windows.Forms.GroupBox groupBoxOutputTimeFrame;
		private System.Windows.Forms.GroupBox groupBoxTimeFrame;
		private System.Windows.Forms.GroupBox groupBoxMapping;
		private System.Windows.Forms.GroupBox groupBoxImportPossibleTimeFrame;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private DateTime dateTimeImportBegin;
		private DateTime dateTimeImportEnd;
		private DateTime dateTimeExpansionBegin;
		private DateTime dateTimeExpansionEnd;
		private TimeSpan timeSpanImport = new TimeSpan(6,23,59,59,0);
		private System.Windows.Forms.DateTimePicker dateTimePickerOutputTimeframeEnd;
		private System.Windows.Forms.DateTimePicker dateTimePickerOutputTimeframeBegin;
		private UfisDataGrid dataGridOutputArrivalFlights;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.ComboBox comboBoxMonday;
		private System.Windows.Forms.ComboBox comboBoxTuesday;
		private System.Windows.Forms.ComboBox comboBoxWednesday;
		private System.Windows.Forms.ComboBox comboBoxThursday;
		private System.Windows.Forms.ComboBox comboBoxFriday;
		private System.Windows.Forms.ComboBox comboBoxSaturday;
		private System.Windows.Forms.ComboBox comboBoxSunday;
		private UfisDataGrid dataGridOutputDepartureFlights;
		private DateTime dateTimeImportTimeframeStart;
		private DateTime dateTimeImportTimeframeEnd;
		private int monday = 0;
		private int tuesday = 0;
		private int wednesday = 0;
		private int thursday = 0;
		private int friday = 0;
		private int saturday = 0;
		private int sunday = 0;
		private Type type;
		private System.Windows.Forms.TextBox dateTimeToDate;
		private System.Windows.Forms.TextBox dateTimeFromDate;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public SnapshotExpanderLayout()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// 

		private Controller MyController
		{
			get
			{
				return (Controller)base.Controller;
			}
		}

		public bool Init
		{
			get
			{
				return this.init;
			}
			set
			{
				this.init = value;
			}
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonCreate = new System.Windows.Forms.Button();
			this.buttonClose = new System.Windows.Forms.Button();
			this.buttonUndo = new System.Windows.Forms.Button();
			this.buttonCompare = new System.Windows.Forms.Button();
			this.buttonCharts = new System.Windows.Forms.Button();
			this.dataGridInputArrivalFlights = new Ufis.Snapshot.Win.UfisDataGrid();
			this.dataGridInputDepartureFlights = new Ufis.Snapshot.Win.UfisDataGrid();
			this.statusBar1 = new System.Windows.Forms.StatusBar();
			this.dataGridOutputArrivalFlights = new Ufis.Snapshot.Win.UfisDataGrid();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBoxOutputTimeFrame = new System.Windows.Forms.GroupBox();
			this.dateTimePickerOutputTimeframeEnd = new System.Windows.Forms.DateTimePicker();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.dateTimePickerOutputTimeframeBegin = new System.Windows.Forms.DateTimePicker();
			this.groupBoxTimeFrame = new System.Windows.Forms.GroupBox();
			this.dateTimePickerImportFilterEnd = new System.Windows.Forms.DateTimePicker();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.dateTimePickerImportFilterBegin = new System.Windows.Forms.DateTimePicker();
			this.label3 = new System.Windows.Forms.Label();
			this.dataGridOutputDepartureFlights = new Ufis.Snapshot.Win.UfisDataGrid();
			this.label6 = new System.Windows.Forms.Label();
			this.groupBoxMapping = new System.Windows.Forms.GroupBox();
			this.label17 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.comboBoxSunday = new System.Windows.Forms.ComboBox();
			this.comboBoxSaturday = new System.Windows.Forms.ComboBox();
			this.comboBoxFriday = new System.Windows.Forms.ComboBox();
			this.comboBoxThursday = new System.Windows.Forms.ComboBox();
			this.comboBoxWednesday = new System.Windows.Forms.ComboBox();
			this.comboBoxTuesday = new System.Windows.Forms.ComboBox();
			this.comboBoxMonday = new System.Windows.Forms.ComboBox();
			this.groupBoxImportPossibleTimeFrame = new System.Windows.Forms.GroupBox();
			this.dateTimeToDate = new System.Windows.Forms.TextBox();
			this.dateTimeFromDate = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.dataGridInputArrivalFlights)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridInputDepartureFlights)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridOutputArrivalFlights)).BeginInit();
			this.groupBoxOutputTimeFrame.SuspendLayout();
			this.groupBoxTimeFrame.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridOutputDepartureFlights)).BeginInit();
			this.groupBoxMapping.SuspendLayout();
			this.groupBoxImportPossibleTimeFrame.SuspendLayout();
			this.SuspendLayout();
			// 
			// buttonCreate
			// 
			this.buttonCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonCreate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonCreate.Location = new System.Drawing.Point(2, 2);
			this.buttonCreate.Name = "buttonCreate";
			this.buttonCreate.Size = new System.Drawing.Size(65, 23);
			this.buttonCreate.TabIndex = 2;
			this.buttonCreate.Text = "Create";
			this.buttonCreate.Click += new System.EventHandler(this.buttonCreate_Click);
			// 
			// buttonClose
			// 
			this.buttonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonClose.Location = new System.Drawing.Point(274, 2);
			this.buttonClose.Name = "buttonClose";
			this.buttonClose.Size = new System.Drawing.Size(65, 23);
			this.buttonClose.TabIndex = 3;
			this.buttonClose.Text = "Close";
			this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
			// 
			// buttonUndo
			// 
			this.buttonUndo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonUndo.Location = new System.Drawing.Point(206, 2);
			this.buttonUndo.Name = "buttonUndo";
			this.buttonUndo.Size = new System.Drawing.Size(65, 23);
			this.buttonUndo.TabIndex = 4;
			this.buttonUndo.Text = "Undo";
			this.buttonUndo.Click += new System.EventHandler(this.buttonUndo_Click);
			// 
			// buttonCompare
			// 
			this.buttonCompare.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonCompare.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonCompare.Location = new System.Drawing.Point(70, 2);
			this.buttonCompare.Name = "buttonCompare";
			this.buttonCompare.Size = new System.Drawing.Size(65, 23);
			this.buttonCompare.TabIndex = 6;
			this.buttonCompare.Text = "Compare";
			this.buttonCompare.Click += new System.EventHandler(this.buttonCompare_Click);
			// 
			// buttonCharts
			// 
			this.buttonCharts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonCharts.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonCharts.Location = new System.Drawing.Point(138, 2);
			this.buttonCharts.Name = "buttonCharts";
			this.buttonCharts.Size = new System.Drawing.Size(65, 23);
			this.buttonCharts.TabIndex = 7;
			this.buttonCharts.Text = "Charts";
			this.buttonCharts.Click += new System.EventHandler(this.buttonCharts_Click);
			// 
			// dataGridInputArrivalFlights
			// 
			this.dataGridInputArrivalFlights.AllowNavigation = false;
			this.dataGridInputArrivalFlights.DataMember = "";
			this.dataGridInputArrivalFlights.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridInputArrivalFlights.Location = new System.Drawing.Point(4, 200);
			this.dataGridInputArrivalFlights.Name = "dataGridInputArrivalFlights";
			this.dataGridInputArrivalFlights.SelectionBackColor = System.Drawing.Color.Blue;
			this.dataGridInputArrivalFlights.SelectionForeColor = System.Drawing.Color.White;
			this.dataGridInputArrivalFlights.Size = new System.Drawing.Size(494, 292);
			this.dataGridInputArrivalFlights.TabIndex = 8;
			this.dataGridInputArrivalFlights.CurrentCellChanged += new System.EventHandler(this.dataGridInputArrivalFlights_CurrentCellChanged);
			// 
			// dataGridInputDepartureFlights
			// 
			this.dataGridInputDepartureFlights.AllowNavigation = false;
			this.dataGridInputDepartureFlights.DataMember = "";
			this.dataGridInputDepartureFlights.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridInputDepartureFlights.Location = new System.Drawing.Point(4, 520);
			this.dataGridInputDepartureFlights.Name = "dataGridInputDepartureFlights";
			this.dataGridInputDepartureFlights.SelectionBackColor = System.Drawing.Color.Blue;
			this.dataGridInputDepartureFlights.SelectionForeColor = System.Drawing.Color.White;
			this.dataGridInputDepartureFlights.Size = new System.Drawing.Size(494, 292);
			this.dataGridInputDepartureFlights.TabIndex = 9;
			this.dataGridInputDepartureFlights.CurrentCellChanged += new System.EventHandler(this.dataGridInputDepartureFlights_CurrentCellChanged);
			// 
			// statusBar1
			// 
			this.statusBar1.Location = new System.Drawing.Point(0, 815);
			this.statusBar1.Name = "statusBar1";
			this.statusBar1.Size = new System.Drawing.Size(1128, 22);
			this.statusBar1.TabIndex = 10;
			// 
			// dataGridOutputArrivalFlights
			// 
			this.dataGridOutputArrivalFlights.DataMember = "";
			this.dataGridOutputArrivalFlights.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridOutputArrivalFlights.Location = new System.Drawing.Point(502, 200);
			this.dataGridOutputArrivalFlights.Name = "dataGridOutputArrivalFlights";
			this.dataGridOutputArrivalFlights.Size = new System.Drawing.Size(626, 292);
			this.dataGridOutputArrivalFlights.TabIndex = 11;
			this.dataGridOutputArrivalFlights.CurrentCellChanged += new System.EventHandler(this.dataGridOutputArrivalFlights_CurrentCellChanged);
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(4, 180);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(156, 16);
			this.label1.TabIndex = 12;
			this.label1.Text = "Import Arrival Flights";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(4, 500);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(160, 16);
			this.label2.TabIndex = 13;
			this.label2.Text = "Import Departure Flights";
			// 
			// groupBoxOutputTimeFrame
			// 
			this.groupBoxOutputTimeFrame.Controls.Add(this.dateTimePickerOutputTimeframeEnd);
			this.groupBoxOutputTimeFrame.Controls.Add(this.label5);
			this.groupBoxOutputTimeFrame.Controls.Add(this.label4);
			this.groupBoxOutputTimeFrame.Controls.Add(this.dateTimePickerOutputTimeframeBegin);
			this.groupBoxOutputTimeFrame.Location = new System.Drawing.Point(496, 28);
			this.groupBoxOutputTimeFrame.Name = "groupBoxOutputTimeFrame";
			this.groupBoxOutputTimeFrame.Size = new System.Drawing.Size(272, 72);
			this.groupBoxOutputTimeFrame.TabIndex = 14;
			this.groupBoxOutputTimeFrame.TabStop = false;
			this.groupBoxOutputTimeFrame.Text = "Output Timeframe";
			// 
			// dateTimePickerOutputTimeframeEnd
			// 
			this.dateTimePickerOutputTimeframeEnd.CustomFormat = "dddd d. MMMM yyyy  -  HH:mm";
			this.dateTimePickerOutputTimeframeEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerOutputTimeframeEnd.Location = new System.Drawing.Point(40, 44);
			this.dateTimePickerOutputTimeframeEnd.Name = "dateTimePickerOutputTimeframeEnd";
			this.dateTimePickerOutputTimeframeEnd.Size = new System.Drawing.Size(228, 20);
			this.dateTimePickerOutputTimeframeEnd.TabIndex = 3;
			this.dateTimePickerOutputTimeframeEnd.ValueChanged += new System.EventHandler(this.dateTimePickerOutputTimeframeEnd_ValueChanged);
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 40);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(30, 23);
			this.label5.TabIndex = 2;
			this.label5.Text = "End:";
			this.label5.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(4, 16);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(36, 23);
			this.label4.TabIndex = 1;
			this.label4.Text = "Begin:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			// 
			// dateTimePickerOutputTimeframeBegin
			// 
			this.dateTimePickerOutputTimeframeBegin.CustomFormat = "dddd d. MMMM yyyy  -  HH:mm";
			this.dateTimePickerOutputTimeframeBegin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerOutputTimeframeBegin.Location = new System.Drawing.Point(40, 20);
			this.dateTimePickerOutputTimeframeBegin.Name = "dateTimePickerOutputTimeframeBegin";
			this.dateTimePickerOutputTimeframeBegin.Size = new System.Drawing.Size(228, 20);
			this.dateTimePickerOutputTimeframeBegin.TabIndex = 0;
			this.dateTimePickerOutputTimeframeBegin.ValueChanged += new System.EventHandler(this.dateTimePickerOutputTimeframeBegin_ValueChanged);
			// 
			// groupBoxTimeFrame
			// 
			this.groupBoxTimeFrame.Controls.Add(this.dateTimePickerImportFilterEnd);
			this.groupBoxTimeFrame.Controls.Add(this.label7);
			this.groupBoxTimeFrame.Controls.Add(this.label8);
			this.groupBoxTimeFrame.Controls.Add(this.dateTimePickerImportFilterBegin);
			this.groupBoxTimeFrame.Location = new System.Drawing.Point(170, 28);
			this.groupBoxTimeFrame.Name = "groupBoxTimeFrame";
			this.groupBoxTimeFrame.Size = new System.Drawing.Size(284, 72);
			this.groupBoxTimeFrame.TabIndex = 15;
			this.groupBoxTimeFrame.TabStop = false;
			this.groupBoxTimeFrame.Text = "Import Timeframe (max. 1 Week)";
			// 
			// dateTimePickerImportFilterEnd
			// 
			this.dateTimePickerImportFilterEnd.CustomFormat = "dddd d. MMMM yyyy  -  HH:mm";
			this.dateTimePickerImportFilterEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerImportFilterEnd.Location = new System.Drawing.Point(40, 44);
			this.dateTimePickerImportFilterEnd.Name = "dateTimePickerImportFilterEnd";
			this.dateTimePickerImportFilterEnd.Size = new System.Drawing.Size(236, 20);
			this.dateTimePickerImportFilterEnd.TabIndex = 7;
			this.dateTimePickerImportFilterEnd.ValueChanged += new System.EventHandler(this.dateTimePickerImportFilterEnd_ValueChanged);
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(8, 40);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(30, 23);
			this.label7.TabIndex = 6;
			this.label7.Text = "End:";
			this.label7.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(4, 16);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(36, 23);
			this.label8.TabIndex = 5;
			this.label8.Text = "Begin:";
			this.label8.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			// 
			// dateTimePickerImportFilterBegin
			// 
			this.dateTimePickerImportFilterBegin.CustomFormat = "dddd d. MMMM yyyy  -  HH:mm";
			this.dateTimePickerImportFilterBegin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerImportFilterBegin.Location = new System.Drawing.Point(40, 20);
			this.dateTimePickerImportFilterBegin.Name = "dateTimePickerImportFilterBegin";
			this.dateTimePickerImportFilterBegin.Size = new System.Drawing.Size(236, 20);
			this.dateTimePickerImportFilterBegin.TabIndex = 4;
			this.dateTimePickerImportFilterBegin.ValueChanged += new System.EventHandler(this.dateTimePickerImportFilterBegin_ValueChanged);
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(500, 180);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(160, 16);
			this.label3.TabIndex = 16;
			this.label3.Text = "Expanded Arrival Flights";
			// 
			// dataGridOutputDepartureFlights
			// 
			this.dataGridOutputDepartureFlights.DataMember = "";
			this.dataGridOutputDepartureFlights.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridOutputDepartureFlights.Location = new System.Drawing.Point(502, 520);
			this.dataGridOutputDepartureFlights.Name = "dataGridOutputDepartureFlights";
			this.dataGridOutputDepartureFlights.Size = new System.Drawing.Size(626, 292);
			this.dataGridOutputDepartureFlights.TabIndex = 17;
			this.dataGridOutputDepartureFlights.CurrentCellChanged += new System.EventHandler(this.dataGridOutputDepartureFlights_CurrentCellChanged);
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label6.Location = new System.Drawing.Point(500, 500);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(180, 16);
			this.label6.TabIndex = 18;
			this.label6.Text = "Expanded Departure Flights";
			// 
			// groupBoxMapping
			// 
			this.groupBoxMapping.Controls.Add(this.label17);
			this.groupBoxMapping.Controls.Add(this.label16);
			this.groupBoxMapping.Controls.Add(this.label15);
			this.groupBoxMapping.Controls.Add(this.label14);
			this.groupBoxMapping.Controls.Add(this.label13);
			this.groupBoxMapping.Controls.Add(this.label12);
			this.groupBoxMapping.Controls.Add(this.label11);
			this.groupBoxMapping.Controls.Add(this.comboBoxSunday);
			this.groupBoxMapping.Controls.Add(this.comboBoxSaturday);
			this.groupBoxMapping.Controls.Add(this.comboBoxFriday);
			this.groupBoxMapping.Controls.Add(this.comboBoxThursday);
			this.groupBoxMapping.Controls.Add(this.comboBoxWednesday);
			this.groupBoxMapping.Controls.Add(this.comboBoxTuesday);
			this.groupBoxMapping.Controls.Add(this.comboBoxMonday);
			this.groupBoxMapping.Location = new System.Drawing.Point(774, 28);
			this.groupBoxMapping.Name = "groupBoxMapping";
			this.groupBoxMapping.Size = new System.Drawing.Size(288, 162);
			this.groupBoxMapping.TabIndex = 21;
			this.groupBoxMapping.TabStop = false;
			this.groupBoxMapping.Text = "Mapping";
			// 
			// label17
			// 
			this.label17.Location = new System.Drawing.Point(12, 140);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(160, 16);
			this.label17.TabIndex = 13;
			this.label17.Text = "Expanded Sunday like Import";
			// 
			// label16
			// 
			this.label16.Location = new System.Drawing.Point(12, 120);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(168, 16);
			this.label16.TabIndex = 12;
			this.label16.Text = "Expanded Saturday like Import";
			// 
			// label15
			// 
			this.label15.Location = new System.Drawing.Point(12, 100);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(160, 16);
			this.label15.TabIndex = 11;
			this.label15.Text = "Expanded Friday like Import";
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(12, 80);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(164, 16);
			this.label14.TabIndex = 10;
			this.label14.Text = "Expanded Thursday like Import";
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(12, 60);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(176, 16);
			this.label13.TabIndex = 9;
			this.label13.Text = "Expanded Wednesday like Import";
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(12, 40);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(160, 16);
			this.label12.TabIndex = 8;
			this.label12.Text = "Expanded Tuesday like Import";
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(12, 20);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(156, 16);
			this.label11.TabIndex = 7;
			this.label11.Text = "Expanded Monday like Import";
			// 
			// comboBoxSunday
			// 
			this.comboBoxSunday.Location = new System.Drawing.Point(188, 136);
			this.comboBoxSunday.Name = "comboBoxSunday";
			this.comboBoxSunday.Size = new System.Drawing.Size(92, 21);
			this.comboBoxSunday.TabIndex = 6;
			this.comboBoxSunday.SelectedValueChanged += new System.EventHandler(this.comboBoxSunday_SelectedValueChanged);
			// 
			// comboBoxSaturday
			// 
			this.comboBoxSaturday.Location = new System.Drawing.Point(188, 116);
			this.comboBoxSaturday.Name = "comboBoxSaturday";
			this.comboBoxSaturday.Size = new System.Drawing.Size(92, 21);
			this.comboBoxSaturday.TabIndex = 5;
			this.comboBoxSaturday.SelectedValueChanged += new System.EventHandler(this.comboBoxSaturday_SelectedValueChanged);
			// 
			// comboBoxFriday
			// 
			this.comboBoxFriday.Location = new System.Drawing.Point(188, 96);
			this.comboBoxFriday.Name = "comboBoxFriday";
			this.comboBoxFriday.Size = new System.Drawing.Size(92, 21);
			this.comboBoxFriday.TabIndex = 4;
			this.comboBoxFriday.SelectedValueChanged += new System.EventHandler(this.comboBoxFriday_SelectedValueChanged);
			// 
			// comboBoxThursday
			// 
			this.comboBoxThursday.Location = new System.Drawing.Point(188, 76);
			this.comboBoxThursday.Name = "comboBoxThursday";
			this.comboBoxThursday.Size = new System.Drawing.Size(92, 21);
			this.comboBoxThursday.TabIndex = 3;
			this.comboBoxThursday.SelectedValueChanged += new System.EventHandler(this.comboBoxThursday_SelectedValueChanged);
			// 
			// comboBoxWednesday
			// 
			this.comboBoxWednesday.Location = new System.Drawing.Point(188, 56);
			this.comboBoxWednesday.Name = "comboBoxWednesday";
			this.comboBoxWednesday.Size = new System.Drawing.Size(92, 21);
			this.comboBoxWednesday.TabIndex = 2;
			this.comboBoxWednesday.SelectedValueChanged += new System.EventHandler(this.comboBoxWednesday_SelectedValueChanged);
			// 
			// comboBoxTuesday
			// 
			this.comboBoxTuesday.Location = new System.Drawing.Point(188, 36);
			this.comboBoxTuesday.Name = "comboBoxTuesday";
			this.comboBoxTuesday.Size = new System.Drawing.Size(92, 21);
			this.comboBoxTuesday.TabIndex = 1;
			this.comboBoxTuesday.SelectedValueChanged += new System.EventHandler(this.comboBoxTuesday_SelectedValueChanged);
			// 
			// comboBoxMonday
			// 
			this.comboBoxMonday.Location = new System.Drawing.Point(188, 16);
			this.comboBoxMonday.Name = "comboBoxMonday";
			this.comboBoxMonday.Size = new System.Drawing.Size(92, 21);
			this.comboBoxMonday.TabIndex = 0;
			this.comboBoxMonday.SelectedValueChanged += new System.EventHandler(this.comboBoxMonday_SelectedValueChanged);
			// 
			// groupBoxImportPossibleTimeFrame
			// 
			this.groupBoxImportPossibleTimeFrame.Controls.Add(this.dateTimeToDate);
			this.groupBoxImportPossibleTimeFrame.Controls.Add(this.dateTimeFromDate);
			this.groupBoxImportPossibleTimeFrame.Controls.Add(this.label9);
			this.groupBoxImportPossibleTimeFrame.Controls.Add(this.label10);
			this.groupBoxImportPossibleTimeFrame.Location = new System.Drawing.Point(2, 28);
			this.groupBoxImportPossibleTimeFrame.Name = "groupBoxImportPossibleTimeFrame";
			this.groupBoxImportPossibleTimeFrame.Size = new System.Drawing.Size(164, 72);
			this.groupBoxImportPossibleTimeFrame.TabIndex = 23;
			this.groupBoxImportPossibleTimeFrame.TabStop = false;
			this.groupBoxImportPossibleTimeFrame.Text = "Possible Timeframe";
			// 
			// dateTimeToDate
			// 
			this.dateTimeToDate.Location = new System.Drawing.Point(40, 48);
			this.dateTimeToDate.Name = "dateTimeToDate";
			this.dateTimeToDate.ReadOnly = true;
			this.dateTimeToDate.Size = new System.Drawing.Size(116, 20);
			this.dateTimeToDate.TabIndex = 107;
			this.dateTimeToDate.Text = "";
			// 
			// dateTimeFromDate
			// 
			this.dateTimeFromDate.Location = new System.Drawing.Point(40, 16);
			this.dateTimeFromDate.Name = "dateTimeFromDate";
			this.dateTimeFromDate.ReadOnly = true;
			this.dateTimeFromDate.Size = new System.Drawing.Size(116, 20);
			this.dateTimeFromDate.TabIndex = 106;
			this.dateTimeFromDate.Text = "";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(8, 40);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(30, 23);
			this.label9.TabIndex = 6;
			this.label9.Text = "End:";
			this.label9.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(4, 16);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(36, 23);
			this.label10.TabIndex = 5;
			this.label10.Text = "Begin:";
			this.label10.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			// 
			// SnapshotExpanderLayout
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(1128, 837);
			this.Controls.Add(this.groupBoxImportPossibleTimeFrame);
			this.Controls.Add(this.groupBoxMapping);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.dataGridOutputDepartureFlights);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.groupBoxTimeFrame);
			this.Controls.Add(this.groupBoxOutputTimeFrame);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.dataGridOutputArrivalFlights);
			this.Controls.Add(this.statusBar1);
			this.Controls.Add(this.dataGridInputDepartureFlights);
			this.Controls.Add(this.dataGridInputArrivalFlights);
			this.Controls.Add(this.buttonCharts);
			this.Controls.Add(this.buttonCompare);
			this.Controls.Add(this.buttonUndo);
			this.Controls.Add(this.buttonClose);
			this.Controls.Add(this.buttonCreate);
			this.Name = "SnapshotExpanderLayout";
			this.Text = "SnapshotExpanderLayout";
			this.Load += new System.EventHandler(this.SnapshotExpanderLayout_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataGridInputArrivalFlights)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridInputDepartureFlights)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridOutputArrivalFlights)).EndInit();
			this.groupBoxOutputTimeFrame.ResumeLayout(false);
			this.groupBoxTimeFrame.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridOutputDepartureFlights)).EndInit();
			this.groupBoxMapping.ResumeLayout(false);
			this.groupBoxImportPossibleTimeFrame.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion



		internal void PrepareImportDataGrids()
		{
			this.dataGridInputArrivalFlights.DataBindings.Clear();
			this.dataViewArrival = new DataView( this.dataSetInput.Tables["Flight"], 
				"Type = 65 AND TimeframeArrival >= " + 
				MyController.DateTimeToString(this.dateTimeImportBegin) 
				+ " AND TimeframeArrival <= " + 
				MyController.DateTimeToString(this.dateTimeImportEnd),
				"" , DataViewRowState.CurrentRows);
			


			DataTable dt = new DataTable();
			dt = this.dataSetInput.Tables["Flight"].Clone();
			for(int i = 0; i < this.dataViewArrival.Count; i++)
			{
				DataRow dataRow = dt.NewRow();
				foreach(DataColumn column in dataRow.Table.Columns)
				{
					if(this.dataViewArrival[i][column.ColumnName].ToString().Length > 0)
						dataRow[column.ColumnName] = this.dataViewArrival[i][column.ColumnName].ToString();
				}

				dt.Rows.Add(dataRow);
			}
			MyController.ConvertDataTablesDateTimes(dt, out dt);
			this.dataViewArrival = new DataView(dt,MyController.WhereClause,"",DataViewRowState.CurrentRows);

			DataGridTableStyle tableStyle = new DataGridTableStyle();
			tableStyle.MappingName = "Flight";

			int numCols = this.dataSetInput.Tables["Flight"].Columns.Count;
			DataGridEnableTextBoxColumn aColumnTextColumn ;
			for(int i = 0; i < numCols; ++i)
			{
				aColumnTextColumn = new DataGridEnableTextBoxColumn(i);
				aColumnTextColumn.HeaderText = this.dataSetInput.Tables["Flight"].Columns[i].ColumnName;
				aColumnTextColumn.MappingName = this.dataSetInput.Tables["Flight"].Columns[i].ColumnName;
				aColumnTextColumn.CheckCellEnabled += new EnableCellEventHandler(SetEnableValues);
				tableStyle.GridColumnStyles.Add(aColumnTextColumn);
			}

			this.dataGridInputArrivalFlights.TableStyles.Clear();
			this.dataGridInputArrivalFlights.TableStyles.Add(tableStyle);
			this.dataGridInputArrivalFlights.AllowNavigation = false;
			this.dataViewArrival.AllowNew = false;
			this.dataGridInputArrivalFlights.DataSource = this.dataViewArrival;

			

			this.dataGridInputDepartureFlights.DataBindings.Clear();
			this.dataViewDeparture = new DataView( this.dataSetInput.Tables["Flight"], 
				"Type = 68 AND TimeframeDeparture >= " + 
				MyController.DateTimeToString(this.dateTimeImportBegin) 
				+ " AND TimeframeDeparture <= " + 
				MyController.DateTimeToString(this.dateTimeImportEnd),
				"" , DataViewRowState.CurrentRows);
			


			dt = new DataTable();
			dt = this.dataSetInput.Tables["Flight"].Clone();
			for(int i = 0; i < this.dataViewDeparture.Count; i++)
			{
				DataRow dataRow = dt.NewRow();
				foreach(DataColumn column in dataRow.Table.Columns)
				{
					if(this.dataViewDeparture[i][column.ColumnName].ToString().Length > 0)
						dataRow[column.ColumnName] = this.dataViewDeparture[i][column.ColumnName].ToString();
				}

				dt.Rows.Add(dataRow);
			}
			MyController.ConvertDataTablesDateTimes(dt, out dt);
			this.dataViewDeparture = new DataView(dt,MyController.WhereClause,"",DataViewRowState.CurrentRows);


			tableStyle = new DataGridTableStyle();
			tableStyle.MappingName = "Flight";

			numCols = this.dataSetInput.Tables["Flight"].Columns.Count;
			
			for(int i = 0; i < numCols; ++i)
			{
				aColumnTextColumn = new DataGridEnableTextBoxColumn(i);
				aColumnTextColumn.HeaderText = this.dataSetInput.Tables["Flight"].Columns[i].ColumnName;
				aColumnTextColumn.MappingName = this.dataSetInput.Tables["Flight"].Columns[i].ColumnName;
				aColumnTextColumn.CheckCellEnabled += new EnableCellEventHandler(SetEnableValues);
				tableStyle.GridColumnStyles.Add(aColumnTextColumn);
			}

			this.dataGridInputDepartureFlights.TableStyles.Clear();
			this.dataGridInputDepartureFlights.TableStyles.Add(tableStyle);
			this.dataGridInputDepartureFlights.AllowNavigation = false;
			this.dataViewDeparture.AllowNew = false;
			this.dataGridInputDepartureFlights.DataSource = this.dataViewDeparture;

			
		}

		internal void PrepareOutputDataGrids()
		{
			this.dataGridOutputArrivalFlights.DataBindings.Clear();
			this.dataViewArrival = new DataView( this.dataSetInput.Tables["Flight"], 
				"Type = 65 AND TimeframeArrival >= " + 
				MyController.DateTimeToString(this.dateTimeExpansionBegin) 
				+ " AND TimeframeArrival <= " + 
				MyController.DateTimeToString(this.dateTimeExpansionEnd),
				"" , DataViewRowState.CurrentRows);
			

			DataTable dt = new DataTable();
			dt = this.dataSetInput.Tables["Flight"].Clone();
			for(int i = 0; i < this.dataViewArrival.Count; i++)
			{
				DataRow dataRow = dt.NewRow();
				foreach(DataColumn column in dataRow.Table.Columns)
				{
					if(this.dataViewArrival[i][column.ColumnName].ToString().Length > 0)
						dataRow[column.ColumnName] = this.dataViewArrival[i][column.ColumnName].ToString();
				}

				dt.Rows.Add(dataRow);
			}
			MyController.ConvertDataTablesDateTimes(dt, out dt);
			this.dataViewArrival = new DataView(dt,"","",DataViewRowState.CurrentRows);

			DataGridTableStyle tableStyle = new DataGridTableStyle();
			tableStyle.MappingName = "Flight";

			int numCols = this.dataSetInput.Tables["Flight"].Columns.Count;
			DataGridEnableTextBoxColumn aColumnTextColumn ;
			for(int i = 0; i < numCols; ++i)
			{
				aColumnTextColumn = new DataGridEnableTextBoxColumn(i);
				aColumnTextColumn.HeaderText = this.dataSetInput.Tables["Flight"].Columns[i].ColumnName;
				aColumnTextColumn.MappingName = this.dataSetInput.Tables["Flight"].Columns[i].ColumnName;
				aColumnTextColumn.CheckCellEnabled += new EnableCellEventHandler(SetEnableValues);
				tableStyle.GridColumnStyles.Add(aColumnTextColumn);
			}

			this.dataGridOutputArrivalFlights.TableStyles.Clear();
			this.dataGridOutputArrivalFlights.TableStyles.Add(tableStyle);
			this.dataGridOutputArrivalFlights.AllowNavigation = false;
			this.dataViewArrival.AllowNew = false;
			this.dataGridOutputArrivalFlights.DataSource = this.dataViewArrival;



//####################



			this.dataGridOutputDepartureFlights.DataBindings.Clear();
			this.dataViewDeparture = new DataView( this.dataSetInput.Tables["Flight"], 
				"Type = 68 AND TimeframeDeparture >= " + 
				MyController.DateTimeToString(this.dateTimeExpansionBegin) 
				+ " AND TimeframeDeparture <= " + 
				MyController.DateTimeToString(this.dateTimeExpansionEnd),
				"" , DataViewRowState.CurrentRows);
			

			dt = new DataTable();
			dt = this.dataSetInput.Tables["Flight"].Clone();
			for(int i = 0; i < this.dataViewDeparture.Count; i++)
			{
				DataRow dataRow = dt.NewRow();
				foreach(DataColumn column in dataRow.Table.Columns)
				{
					if(this.dataViewDeparture[i][column.ColumnName].ToString().Length > 0)
						dataRow[column.ColumnName] = this.dataViewDeparture[i][column.ColumnName].ToString();
				}

				dt.Rows.Add(dataRow);
			}
			MyController.ConvertDataTablesDateTimes(dt, out dt);
			this.dataViewDeparture = new DataView(dt,"","",DataViewRowState.CurrentRows);

			tableStyle = new DataGridTableStyle();
			tableStyle.MappingName = "Flight";

			numCols = this.dataSetInput.Tables["Flight"].Columns.Count;
			for(int i = 0; i < numCols; ++i)
			{
				aColumnTextColumn = new DataGridEnableTextBoxColumn(i);
				aColumnTextColumn.HeaderText = this.dataSetInput.Tables["Flight"].Columns[i].ColumnName;
				aColumnTextColumn.MappingName = this.dataSetInput.Tables["Flight"].Columns[i].ColumnName;
				aColumnTextColumn.CheckCellEnabled += new EnableCellEventHandler(SetEnableValues);
				tableStyle.GridColumnStyles.Add(aColumnTextColumn);
			}

			this.dataGridOutputDepartureFlights.TableStyles.Clear();
			this.dataGridOutputDepartureFlights.TableStyles.Add(tableStyle);
			this.dataGridOutputDepartureFlights.AllowNavigation = false;
			this.dataViewDeparture.AllowNew = false;
			this.dataGridOutputDepartureFlights.DataSource = this.dataViewDeparture;



			
		}

		public void SetEnableValues(object sender, DataGridEnableEventArgs e)
		{
			e.EnableValue = false;

			if(this.dataSetInput.Tables["Flight"].Rows[e.Row].HasErrors)
				e.Color = "Red";
		}

		private void SnapshotExpanderLayout_Load(object sender, System.EventArgs e)
		{

			this.dateTimePickerImportFilterBegin.CustomFormat = " ";
			this.dateTimePickerImportFilterEnd.CustomFormat = " ";
			this.dateTimePickerImportFilterBegin.Enabled = false;
			this.dateTimePickerImportFilterEnd.Enabled = false;


			Init = false;

			//get dataset and fill arrival / departure datagrid
			this.dataSetInput = MyController.InputDataSet;
			this.dataSetInput.AcceptChanges();

			ArrayList arrayListMonday = new ArrayList();
			ArrayList arrayListTuesday = new ArrayList();
			ArrayList arrayListWednesday = new ArrayList();
			ArrayList arrayListThursday = new ArrayList();
			ArrayList arrayListFriday = new ArrayList();
			ArrayList arrayListSaturday = new ArrayList();
			ArrayList arrayListSunday = new ArrayList();


			type = this.dataSetInput.Tables["Flight"].Columns["DayArrival"].DataType;
			string[] names = Enum.GetNames(type);
								
			for (int j = 0; j < names.Length; j++)
			{						
				Enum e2 = (Enum)Enum.Parse(type,names[j]);
				arrayListMonday.Add(e2);
				arrayListTuesday.Add(e2);
				arrayListWednesday.Add(e2);
				arrayListThursday.Add(e2);
				arrayListFriday.Add(e2);
				arrayListSaturday.Add(e2);
				arrayListSunday.Add(e2);					
			}





			this.comboBoxMonday.DataSource = arrayListMonday;
			this.comboBoxTuesday.DataSource = arrayListTuesday;
			this.comboBoxWednesday.DataSource = arrayListWednesday;
			this.comboBoxThursday.DataSource = arrayListThursday;
			this.comboBoxFriday.DataSource = arrayListFriday;	
			this.comboBoxSaturday.DataSource = arrayListSaturday;
			this.comboBoxSunday.DataSource = arrayListSunday;	

			this.comboBoxMonday.Text = "";
			this.comboBoxTuesday.Text = "";
			this.comboBoxWednesday.Text = "";
			this.comboBoxThursday.Text = "";
			this.comboBoxFriday.Text = "";
			this.comboBoxSaturday.Text = "";
			this.comboBoxSunday.Text = "";


			
			this.dataViewArrival = new DataView( this.dataSetInput.Tables["Flight"], "Type = 65", "" , DataViewRowState.CurrentRows);
			this.dataViewDeparture = new DataView( this.dataSetInput.Tables["Flight"], "Type = 68", "" , DataViewRowState.CurrentRows);

			this.dateTimeFromDate.Text = MyController.LoadFrom.ToString();
			this.dateTimeToDate.Text = MyController.LoadTo.ToString();

			this.dateTimeImportTimeframeStart = MyController.LoadFrom;
			this.dateTimeImportTimeframeEnd = MyController.LoadTo;

			DateTime maxDate = MyController.LoadTo;
			DateTime minDate = MyController.LoadFrom;

			this.dateTimePickerImportFilterBegin.CustomFormat = "dddd d. MMMM yyyy  -  HH:mm";
			this.dateTimePickerImportFilterEnd.CustomFormat = "dddd d. MMMM yyyy  -  HH:mm";

			this.dateTimePickerImportFilterBegin.MaxDate = maxDate;
			this.dateTimePickerImportFilterBegin.MinDate = minDate;
			this.dateTimePickerImportFilterBegin.Value = minDate;
			this.dateTimePickerImportFilterEnd.MaxDate = maxDate;
			this.dateTimePickerImportFilterEnd.MinDate = minDate;

			this.dateTimeImportBegin = minDate;
		
			if(minDate.Add(timeSpanImport) > maxDate)
			{
				this.dateTimeImportEnd = maxDate;
			}
			else
			{
				this.dateTimeImportEnd = minDate.Add(timeSpanImport);
			}

			this.dateTimePickerImportFilterEnd.Value = this.dateTimeImportEnd;

			this.dateTimePickerImportFilterBegin.Enabled = true;
			this.dateTimePickerImportFilterEnd.Enabled = true;


			
			this.dateTimeExpansionBegin = System.DateTime.UtcNow;
			this.dateTimeExpansionEnd = System.DateTime.UtcNow;

			this.dateTimePickerOutputTimeframeBegin.MinDate = this.dateTimeExpansionBegin;
			this.dateTimePickerOutputTimeframeEnd.MinDate = this.dateTimeExpansionBegin;

			

			PrepareImportDataGrids();

			Init = true;

		}

		private void buttonClose_Click(object sender, System.EventArgs e)
		{
			MyController.SelectService("DataExtractViewer");
			this.Dispose();
		}

		private void dateTimePickerImportFilterBegin_ValueChanged(object sender, EventArgs e)
		{
			
			if(Init)
			{			
				DateTime dt = new DateTime();
				dt = this.dateTimePickerImportFilterBegin.Value;
				dt = dt.Add(timeSpanImport);
				if(dt <= this.dateTimePickerImportFilterEnd.MaxDate)
					this.dateTimeImportEnd = dt;
				else
					this.dateTimeImportEnd = this.dateTimePickerImportFilterEnd.MaxDate;

				this.dateTimePickerImportFilterEnd.Value = this.dateTimeImportEnd;	
				this.dateTimeImportBegin = this.dateTimePickerImportFilterBegin.Value;


				PrepareImportDataGrids();
			}
		}

		private void dateTimePickerImportFilterEnd_ValueChanged(object sender, EventArgs e)
		{
			

			if(Init)
			{		
				

				DateTime dt = new DateTime();
				dt = this.dateTimePickerImportFilterEnd.Value;
				dt = dt.Subtract(timeSpanImport);
				if(dt >= this.dateTimePickerImportFilterBegin.MinDate)
					this.dateTimeImportBegin = dt;
				else
					this.dateTimeImportBegin = this.dateTimePickerImportFilterBegin.MinDate;
					
				this.dateTimePickerImportFilterBegin.Value = this.dateTimeImportBegin;
				this.dateTimeImportEnd = this.dateTimePickerImportFilterEnd.Value;


				PrepareImportDataGrids();
			
			}
		}

		private void buttonCreate_Click(object sender, System.EventArgs e)
		{
			try
			{
				this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
				MyController.Expand(this.dateTimeImportBegin, 
					this.dateTimeImportEnd, 
					this.dateTimeExpansionBegin, 
					this.dateTimeExpansionEnd, 
					this.monday,
					this.tuesday, 
					this.wednesday, 
					this.thursday, 
					this.friday, 
					this.saturday, 
					this.sunday);
				PrepareOutputDataGrids();
			}
			finally
			{
				this.Cursor = System.Windows.Forms.Cursors.Default;
			}
		}



		private void dateTimePickerOutputTimeframeEnd_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimeExpansionEnd = this.dateTimePickerOutputTimeframeEnd.Value;
		}

		private void dateTimePickerOutputTimeframeBegin_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimeExpansionBegin = this.dateTimePickerOutputTimeframeBegin.Value;
		}

		private void buttonCompare_Click(object sender, System.EventArgs e)
		{
			MyController.SelectService("CompareFunctionViewer");
		}

		private void buttonUndo_Click(object sender, System.EventArgs e)
		{
			try
			{
				this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
				this.dataSetInput.RejectChanges();
			}
			finally
			{
				this.Cursor = System.Windows.Forms.Cursors.Default;
			}
		}



		private void buttonCharts_Click(object sender, System.EventArgs e)
		{
			MyController.SelectService("GanttChartLayout");
		}

		private void dataGridInputArrivalFlights_CurrentCellChanged(object sender, EventArgs e)
		{
			this.dataGridInputArrivalFlights.Select(this.dataGridInputArrivalFlights.CurrentCell.RowNumber); 						
		}

		private void dataGridInputDepartureFlights_CurrentCellChanged(object sender, EventArgs e)
		{
			this.dataGridInputDepartureFlights.Select(this.dataGridInputDepartureFlights.CurrentCell.RowNumber); 
		}

		private void dataGridOutputArrivalFlights_CurrentCellChanged(object sender, EventArgs e)
		{
			this.dataGridOutputArrivalFlights.Select(this.dataGridOutputArrivalFlights.CurrentCell.RowNumber); 
		}

		private void dataGridOutputDepartureFlights_CurrentCellChanged(object sender, EventArgs e)
		{
			this.dataGridOutputDepartureFlights.Select(this.dataGridOutputDepartureFlights.CurrentCell.RowNumber); 
		}

		private void comboBoxSunday_SelectedValueChanged(object sender, EventArgs e)
		{
			if(this.comboBoxSunday.SelectedValue.ToString() != "")
				this.sunday = (int)Enum.Parse(type, this.comboBoxSunday.Text.ToString());
			else
				this.sunday = 0;
		}

		private void comboBoxSaturday_SelectedValueChanged(object sender, EventArgs e)
		{
			if(this.comboBoxSaturday.SelectedValue.ToString() != "")
				this.saturday = (int)Enum.Parse(type, this.comboBoxSaturday.Text.ToString());
			else
				this.saturday = 0;
		}

		private void comboBoxFriday_SelectedValueChanged(object sender, EventArgs e)
		{
			if(this.comboBoxFriday.SelectedValue.ToString() != "")
				this.friday = (int)Enum.Parse(type, this.comboBoxFriday.Text.ToString());
			else
				this.friday = 0;
		}

		private void comboBoxThursday_SelectedValueChanged(object sender, EventArgs e)
		{
			if(this.comboBoxThursday.SelectedValue.ToString() != "")
				this.thursday = (int)Enum.Parse(type, this.comboBoxThursday.Text.ToString());
			else
				this.thursday = 0;
		}

		private void comboBoxWednesday_SelectedValueChanged(object sender, EventArgs e)
		{
			if(this.comboBoxWednesday.SelectedValue.ToString() != "")
				this.wednesday = (int)Enum.Parse(type, this.comboBoxWednesday.Text.ToString());
			else
				this.wednesday = 0;
		}

		private void comboBoxTuesday_SelectedValueChanged(object sender, EventArgs e)
		{
			if(this.comboBoxTuesday.SelectedValue.ToString() != "")
				this.tuesday = (int)Enum.Parse(type, this.comboBoxTuesday.Text.ToString());
			else
				this.tuesday = 0;
		}

		private void comboBoxMonday_SelectedValueChanged(object sender, EventArgs e)
		{
			if(this.comboBoxMonday.SelectedValue.ToString() != "")
				this.monday = (int)Enum.Parse(type, this.comboBoxMonday.Text.ToString());
			else
				this.monday = 0;
		}

	}
}
