using Ufis.BL.Common;
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Ufis.Utils;
using Ufis.Data;

namespace Ufis.Snapshot.Win
{
	/// <summary>
	/// Summary description for GanttChartLayout.
	/// </summary>
	public class GanttChartLayout : Microsoft.ApplicationBlocks.UIProcess.WindowsFormView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		/// 
		private int selectedLine = -1;
		private System.ComponentModel.Container components = null;
		private AxUGANTTLib.AxUGantt mainChart;
		private AxUGANTTLib.AxUGantt detailChart;
		private System.Windows.Forms.ComboBox comboBoxResource;
		private UfisDataGrid dataGridInput;
		private UfisDataGrid dataGridAODB;
		private DataSet dataSetFlightRecords;
		private DataSet dataSetAODB;

		public GanttChartLayout()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		private Controller MyController
		{
			get
			{
				return (Controller)base.Controller;
			}
		}


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(GanttChartLayout));
			this.mainChart = new AxUGANTTLib.AxUGantt();
			this.detailChart = new AxUGANTTLib.AxUGantt();
			this.dataSetFlightRecords = new System.Data.DataSet();
			this.dataSetAODB = new DataSet();
			this.comboBoxResource = new System.Windows.Forms.ComboBox();
			this.dataGridInput = new UfisDataGrid();
			this.dataGridAODB = new UfisDataGrid();
			((System.ComponentModel.ISupportInitialize)(this.mainChart)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.detailChart)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSetFlightRecords)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridInput)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridAODB)).BeginInit();
			this.SuspendLayout();
			// 
			// mainChart
			// 
			this.mainChart.Location = new System.Drawing.Point(8, 72);
			this.mainChart.Name = "mainChart";
			this.mainChart.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("mainChart.OcxState")));
			this.mainChart.Size = new System.Drawing.Size(1016, 392);
			this.mainChart.TabIndex = 1;
			this.mainChart.OnLButtonDownBar += new AxUGANTTLib._DUGanttEvents_OnLButtonDownBarEventHandler(this.mainChart_OnLButtonDownBar);
			// 
			// detailChart
			// 
			this.detailChart.Location = new System.Drawing.Point(8, 488);
			this.detailChart.Name = "detailChart";
			this.detailChart.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("detailChart.OcxState")));
			this.detailChart.Size = new System.Drawing.Size(472, 288);
			this.detailChart.TabIndex = 2;
			// 
			// dataSetFlightRecords
			// 
			this.dataSetFlightRecords.DataSetName = "NewDataSet";
			this.dataSetFlightRecords.Locale = new System.Globalization.CultureInfo("de-DE");
			// 
			// comboBoxResource
			// 
			this.comboBoxResource.Location = new System.Drawing.Point(504, 488);
			this.comboBoxResource.Name = "comboBoxResource";
			this.comboBoxResource.Size = new System.Drawing.Size(520, 21);
			this.comboBoxResource.TabIndex = 3;
			this.comboBoxResource.SelectedValueChanged += new System.EventHandler(this.comboBoxResource_SelectedValueChanged);
			// 
			// dataGridInput
			// 
			this.dataGridInput.DataMember = "";
			this.dataGridInput.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridInput.Location = new System.Drawing.Point(504, 520);
			this.dataGridInput.Name = "dataGridInput";
			this.dataGridInput.Size = new System.Drawing.Size(520, 120);
			this.dataGridInput.TabIndex = 4;
			this.dataGridInput.Scroll +=new EventHandler(dataGridInput_Scroll);
			// 
			// dataGridAODB
			// 
			this.dataGridAODB.DataMember = "";
			this.dataGridAODB.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridAODB.Location = new System.Drawing.Point(504, 648);
			this.dataGridAODB.Name = "dataGridAODB";
			this.dataGridAODB.Size = new System.Drawing.Size(520, 128);
			this.dataGridAODB.TabIndex = 5;
			this.dataGridAODB.Scroll +=new EventHandler(dataGridAODB_Scroll);
			// 
			// GanttChartLayout
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(1144, 837);
			this.Controls.Add(this.dataGridAODB);
			this.Controls.Add(this.dataGridInput);
			this.Controls.Add(this.comboBoxResource);
			this.Controls.Add(this.detailChart);
			this.Controls.Add(this.mainChart);
			this.Name = "GanttChartLayout";
			this.Text = "GanttChartLayout";
			this.Load += new System.EventHandler(this.GanttChartLayout_Load);
			((System.ComponentModel.ISupportInitialize)(this.mainChart)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.detailChart)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSetFlightRecords)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridInput)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridAODB)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void GanttChartLayout_Load(object sender, System.EventArgs e)
		{
			ProgressWindow progress = new ProgressWindow();
			progress.Text = "Work";
			progress.Show(); 
			MyController.LoadAODB(progress);
			this.dataSetAODB = MyController.AODBDataSet;

			InitDetailGantt();
			this.dataSetFlightRecords = MyController.InputDataSet;
			InitMainGantt();
			AllocateMainGantt();

			for(int k = 0; k < this.dataSetFlightRecords.Tables["Flight"].ChildRelations.Count; k++)
			{	
				this.comboBoxResource.Items.Add(this.dataSetFlightRecords.Tables["Flight"].ChildRelations[k].RelationName.ToString());
			}
			
		}

		public void InitMainGantt()
		{
			mainChart.ResetContent();
			mainChart.TabHeaderLengthString = "100";                  //init the width of the tab-columns
			mainChart.TabSetHeaderText ("Flights");               //set the tab-headlines
			mainChart.SplitterPosition = 100;                         //the splitterposition depends on the
			mainChart.TabBodyFontName = "Arial";
			mainChart.TabFontSize = 14;								
			mainChart.TabHeaderFontName = "Arial";
			mainChart.TabHeaderFontSize = 14;
			mainChart.BarOverlapOffset = 10;
			mainChart.BarNumberExpandLine = 0;
			mainChart.TabBodyFontBold = true;
			mainChart.TabHeaderFontBold = true;
			mainChart.LifeStyle = true;
			mainChart.TimeFrameFrom = MyController.LoadFrom.Subtract(new TimeSpan(1, 0, 0, 0, 0));
			mainChart.TimeFrameTo = MyController.LoadTo.AddHours(24);
			
		}

		public void InitDetailGantt()
		{
			detailChart.ResetContent();
			detailChart.TabHeaderLengthString = "100";                  //init the width of the tab-columns
			detailChart.TabSetHeaderText ("Locations");               //set the tab-headlines
			detailChart.SplitterPosition = 100;                         //the splitterposition depends on the
			detailChart.TabBodyFontName = "Arial";
			detailChart.TabFontSize = 14;								
			detailChart.TabHeaderFontName = "Arial";
			detailChart.TabHeaderFontSize = 14;
			detailChart.BarOverlapOffset = 10;
			detailChart.BarNumberExpandLine = 0;
			detailChart.TabBodyFontBold = true;
			detailChart.TabHeaderFontBold = true;
			detailChart.LifeStyle = true;	
		}

		public void AllocateMainGantt()
		{
			DateTime myFrom = DateTime.UtcNow;
			DateTime myTo = DateTime.UtcNow;
			int colBody = UT.colLightGray;
			int leftTriaColor = UT.colLightGray;
			int rightTriaColor = UT.colLightGray;


			mainChart.TimeScaleDuration = 24;
			mainChart.ScrollTo(MyController.LoadFrom.Subtract(new TimeSpan(0,2,0,0,0))); 
						
			for(int i = 0; i < this.dataSetFlightRecords.Tables["Flight"].Rows.Count; i++)
			{
				colBody = UT.colLightGray;
				leftTriaColor = UT.colLightGray;
				rightTriaColor = UT.colLightGray; 

				this.mainChart.AddLineAt(i,i.ToString(),this.dataSetFlightRecords.Tables["Flight"].Rows[i]["FlightNumber"].ToString());
				
				if(this.dataSetFlightRecords.Tables["Flight"].Rows[i]["Type"].ToString() == "65" )
				{
					myTo = UT.CedaFullDateToDateTime(this.dataSetFlightRecords.Tables["Flight"].Rows[i]["TimeframeArrival"].ToString());  
					myFrom =  UT.CedaFullDateToDateTime(this.dataSetFlightRecords.Tables["Flight"].Rows[i]["TimeframeDeparture"].ToString()); 
					colBody =UT.colLightGray; //Cyan
					leftTriaColor = UT.colBlue;
				}
				if(this.dataSetFlightRecords.Tables["Flight"].Rows[i]["Type"].ToString() == "68")
				{
					myTo = UT.CedaFullDateToDateTime(this.dataSetFlightRecords.Tables["Flight"].Rows[i]["TimeframeArrival"].ToString()); 
					myFrom = UT.CedaFullDateToDateTime(this.dataSetFlightRecords.Tables["Flight"].Rows[i]["TimeframeDeparture"].ToString());
					colBody = UT.colLightGray; //colLightBlue
					rightTriaColor = UT.colBlue;
				}

				string strFlno = this.dataSetFlightRecords.Tables["Flight"].Rows[i]["FlightNumber"].ToString();
				string strBarKey = i.ToString();
				mainChart.DeleteBarByKey(strBarKey);

				mainChart.AddBarToLine(i, strBarKey, myFrom, myTo, 
					strFlno, 1,
					UT.colCyan, UT.colBlack, UT.colRed, 
					UT.colBlack, UT.colBlack, 
					"", "", UT.colBlack, UT.colBlack);
				mainChart.SetBarColor(strBarKey, colBody);
				string strTriaID = leftTriaColor.ToString() + rightTriaColor.ToString();
				mainChart.SetBarTriangles(strBarKey, strTriaID, leftTriaColor, rightTriaColor, true);
				mainChart.SetLineSeparator(i, 1, UT.colBlack, 2, 1, 0);
			}
			mainChart.Refresh();
		}

		public int SelectedLine
		{
			get
			{
				return this.selectedLine;
			}
			set
			{
				this.selectedLine = value;
			}
		}

		private void mainChart_OnLButtonDownBar(object sender, AxUGANTTLib._DUGanttEvents_OnLButtonDownBarEvent e)
		{
			InitDetailGantt();
			int line = Int32.Parse(e.key);
			SelectedLine = line;
			DateTime from = DateTime.UtcNow;
			DateTime to = DateTime.UtcNow;
			int colBody = UT.colLightGray;

			detailChart.ScrollTo(MyController.LoadFrom.Subtract(new TimeSpan(0,2,0,0,0))); 

			if(this.dataSetFlightRecords.Tables["Flight"].Rows[line]["Type"].ToString() == "65" )
			{
				from = UT.CedaFullDateToDateTime(this.dataSetFlightRecords.Tables["Flight"].Rows[line]["StandardTimeArrival"].ToString());  
				to =  UT.CedaFullDateToDateTime(this.dataSetFlightRecords.Tables["Flight"].Rows[line]["StandardTimeDeparture"].ToString()); 
			}
			if(this.dataSetFlightRecords.Tables["Flight"].Rows[line]["Type"].ToString() == "68")
			{
				to = UT.CedaFullDateToDateTime(this.dataSetFlightRecords.Tables["Flight"].Rows[line]["StandardTimeArrival"].ToString()); 
				from = UT.CedaFullDateToDateTime(this.dataSetFlightRecords.Tables["Flight"].Rows[line]["StandardTimeDeparture"].ToString());
			}		
			
			detailChart.TimeFrameFrom = from.Subtract(new TimeSpan(0, 1, 0, 0, 0));
			detailChart.TimeFrameTo = to.AddHours(1);
			detailChart.TimeScaleDuration = 4;
			int lineCount = 0;


			for(int k = 0; k < this.dataSetFlightRecords.Tables["Flight"].Rows[line].Table.ChildRelations.Count; k++)
			{
				
				string relationName = this.dataSetFlightRecords.Tables["Flight"].Rows[line].Table.ChildRelations[k].RelationName;
				string tableName = this.dataSetFlightRecords.Tables["Flight"].Rows[line].Table.ChildRelations[k].ChildTable.TableName;
				DataRow[] dataRowChildRelation = this.dataSetFlightRecords.Tables["Flight"].Rows[line].GetChildRows(relationName);

				string strName = "";
				string resName = "";

				if(tableName == "ParkingStands")
				{
					resName = "ParkingStand";
					if(this.dataSetFlightRecords.Tables["Flight"].Rows[line]["Type"].ToString() == "65" )
					{
						strName = dataRowChildRelation[0]["ParkingStandArrivalName"].ToString();
						from = UT.CedaFullDateToDateTime(dataRowChildRelation[0]["ParkingStandArrivalBegin"].ToString());  
						to =  UT.CedaFullDateToDateTime(dataRowChildRelation[0]["ParkingStandArrivalEnd"].ToString()); 
						colBody = UT.colLightGray; //Cyan
					}
					if(this.dataSetFlightRecords.Tables["Flight"].Rows[line]["Type"].ToString() == "68")
					{
						strName = dataRowChildRelation[0]["ParkingStandDepartureName"].ToString();
						from = UT.CedaFullDateToDateTime(dataRowChildRelation[0]["ParkingStandDepartureBegin"].ToString());  
						to =  UT.CedaFullDateToDateTime(dataRowChildRelation[0]["ParkingStandDepartureEnd"].ToString()); 
						colBody = UT.colLightGray; //Cyan
					}
				
					if(strName != "")
					{

						string strBarKey = lineCount.ToString();
						this.detailChart.AddLineAt(lineCount, lineCount.ToString(),resName);
						detailChart.AddBarToLine(lineCount, strBarKey, from, to, 
							strName, 1,
							UT.colCyan, UT.colBlack, UT.colRed, 
							UT.colBlack, UT.colBlack, 
							"", "", UT.colBlack, UT.colBlack);
						detailChart.SetBarColor(strBarKey, colBody);
						detailChart.SetLineSeparator(lineCount, 1, UT.colBlack, 2, 1, 0);
						lineCount++;
					
					}
				}

				if(tableName == "Gates")
				{
					resName = "Gate";
					if(this.dataSetFlightRecords.Tables["Flight"].Rows[line]["Type"].ToString() == "65" )
					{
						strName = dataRowChildRelation[0]["GatesArrivalFirstName"].ToString();
						from = UT.CedaFullDateToDateTime(dataRowChildRelation[0]["GatesArrivalFirstBegin"].ToString());  
						to =  UT.CedaFullDateToDateTime(dataRowChildRelation[0]["GatesArrivalFirstEnd"].ToString()); 
						colBody = UT.colLightGray; //Cyan
					}
					if(this.dataSetFlightRecords.Tables["Flight"].Rows[line]["Type"].ToString() == "68")
					{
						strName = dataRowChildRelation[0]["GatesDepartureFirstName"].ToString();
						from = UT.CedaFullDateToDateTime(dataRowChildRelation[0]["GatesDepartureFirstBegin"].ToString());  
						to =  UT.CedaFullDateToDateTime(dataRowChildRelation[0]["GatesDepartureFirstEnd"].ToString()); 
						colBody = UT.colLightGray; //Cyan
					}
				
					if(strName != "")
					{

						string strBarKey = lineCount.ToString();
						this.detailChart.AddLineAt(lineCount, lineCount.ToString(),resName);
						detailChart.AddBarToLine(lineCount, strBarKey, from, to, 
							strName, 1,
							UT.colCyan, UT.colBlack, UT.colRed, 
							UT.colBlack, UT.colBlack, 
							"", "", UT.colBlack, UT.colBlack);
						detailChart.SetBarColor(strBarKey, colBody);
						detailChart.SetLineSeparator(lineCount, 1, UT.colBlack, 2, 1, 0);
						lineCount++;
					}

					strName = "";

					if(this.dataSetFlightRecords.Tables["Flight"].Rows[line]["Type"].ToString() == "65" )
					{
						strName = dataRowChildRelation[0]["GatesArrivalSecondName"].ToString();
						from = UT.CedaFullDateToDateTime(dataRowChildRelation[0]["GatesArrivalSecondBegin"].ToString());  
						to =  UT.CedaFullDateToDateTime(dataRowChildRelation[0]["GatesArrivalSecondEnd"].ToString()); 
						colBody = UT.colLightGray; //Cyan
					}
					if(this.dataSetFlightRecords.Tables["Flight"].Rows[line]["Type"].ToString() == "68")
					{
						strName = dataRowChildRelation[0]["GatesDepartureSecondName"].ToString();
						from = UT.CedaFullDateToDateTime(dataRowChildRelation[0]["GatesDepartureSecondBegin"].ToString());  
						to =  UT.CedaFullDateToDateTime(dataRowChildRelation[0]["GatesDepartureSecondEnd"].ToString()); 
						colBody = UT.colLightGray; //Cyan
					}
				
					if(strName != "")
					{

						string strBarKey = lineCount.ToString();
						this.detailChart.AddLineAt(lineCount, lineCount.ToString(),resName);
						detailChart.AddBarToLine(lineCount, strBarKey, from, to, 
							strName, 1,
							UT.colCyan, UT.colBlack, UT.colRed, 
							UT.colBlack, UT.colBlack, 
							"", "", UT.colBlack, UT.colBlack);
						detailChart.SetBarColor(strBarKey, colBody);
						detailChart.SetLineSeparator(lineCount, 1, UT.colBlack, 2, 1, 0);
						lineCount++;
					
					}
				}

				if(tableName == "BaggageBelts")
				{
					resName = "BaggageBelt";
					if(this.dataSetFlightRecords.Tables["Flight"].Rows[line]["Type"].ToString() == "65" )
					{
						strName = dataRowChildRelation[0]["BaggageBeltsFirstName"].ToString();
						from = UT.CedaFullDateToDateTime(dataRowChildRelation[0]["BaggageBeltsFirstBegin"].ToString());  
						to =  UT.CedaFullDateToDateTime(dataRowChildRelation[0]["BaggageBeltsFirstEnd"].ToString()); 
						colBody = UT.colLightGray; //Cyan
					}
				
					if(strName != "")
					{

						string strBarKey = lineCount.ToString();
						this.detailChart.AddLineAt(lineCount, lineCount.ToString(),resName);
						detailChart.AddBarToLine(lineCount, strBarKey, from, to, 
							strName, 1,
							UT.colCyan, UT.colBlack, UT.colRed, 
							UT.colBlack, UT.colBlack, 
							"", "", UT.colBlack, UT.colBlack);
						detailChart.SetBarColor(strBarKey, colBody);
						detailChart.SetLineSeparator(lineCount, 1, UT.colBlack, 2, 1, 0);
						lineCount++;
					}

					strName = "";

					if(this.dataSetFlightRecords.Tables["Flight"].Rows[line]["Type"].ToString() == "65" )
					{
						strName = dataRowChildRelation[0]["BaggageBeltsSecondName"].ToString();
						from = UT.CedaFullDateToDateTime(dataRowChildRelation[0]["BaggageBeltsSecondBegin"].ToString());  
						to =  UT.CedaFullDateToDateTime(dataRowChildRelation[0]["BaggageBeltsSecondEnd"].ToString()); 
						colBody = UT.colLightGray; //Cyan
					}
				
					if(strName != "")
					{

						string strBarKey = lineCount.ToString();
						this.detailChart.AddLineAt(lineCount, lineCount.ToString(),resName);
						detailChart.AddBarToLine(lineCount, strBarKey, from, to, 
							strName, 1,
							UT.colCyan, UT.colBlack, UT.colRed, 
							UT.colBlack, UT.colBlack, 
							"", "", UT.colBlack, UT.colBlack);
						detailChart.SetBarColor(strBarKey, colBody);
						detailChart.SetLineSeparator(lineCount, 1, UT.colBlack, 2, 1, 0);
						lineCount++;
					
					}
				}

				if(tableName == "WaitingRooms")
				{
					resName = "WaitingRoom";
					if(this.dataSetFlightRecords.Tables["Flight"].Rows[line]["Type"].ToString() == "68" )
					{
						strName = dataRowChildRelation[0]["WaitingRoomsFirstName"].ToString();
						from = UT.CedaFullDateToDateTime(dataRowChildRelation[0]["WaitingRoomsFirstBegin"].ToString());  
						to =  UT.CedaFullDateToDateTime(dataRowChildRelation[0]["WaitingRoomsFirstEnd"].ToString()); 
						colBody = UT.colLightGray; //Cyan
					}
				
					if(strName != "")
					{

						string strBarKey = lineCount.ToString();
						this.detailChart.AddLineAt(lineCount, lineCount.ToString(),resName);
						detailChart.AddBarToLine(lineCount, strBarKey, from, to, 
							strName, 1,
							UT.colCyan, UT.colBlack, UT.colRed, 
							UT.colBlack, UT.colBlack, 
							"", "", UT.colBlack, UT.colBlack);
						detailChart.SetBarColor(strBarKey, colBody);
						detailChart.SetLineSeparator(lineCount, 1, UT.colBlack, 2, 1, 0);
						lineCount++;
					}

					strName = "";

					if(this.dataSetFlightRecords.Tables["Flight"].Rows[line]["Type"].ToString() == "68" )
					{
						strName = dataRowChildRelation[0]["WaitingRoomsSecondName"].ToString();
						from = UT.CedaFullDateToDateTime(dataRowChildRelation[0]["WaitingRoomsSecondBegin"].ToString());  
						to =  UT.CedaFullDateToDateTime(dataRowChildRelation[0]["WaitingRoomsSecondEnd"].ToString()); 
						colBody = UT.colLightGray; //Cyan
					}
				
					if(strName != "")
					{

						string strBarKey = lineCount.ToString();
						this.detailChart.AddLineAt(lineCount, lineCount.ToString(),resName);
						detailChart.AddBarToLine(lineCount, strBarKey, from, to, 
							strName, 1,
							UT.colCyan, UT.colBlack, UT.colRed, 
							UT.colBlack, UT.colBlack, 
							"", "", UT.colBlack, UT.colBlack);
						detailChart.SetBarColor(strBarKey, colBody);
						detailChart.SetLineSeparator(lineCount, 1, UT.colBlack, 2, 1, 0);
						lineCount++;
					
					}
				}
			}
			this.detailChart.AddLineAt(lineCount, lineCount.ToString(),"HALLO");

			detailChart.Refresh();

			FillResourceDataGrids();
		}

		private void comboBoxResource_SelectedValueChanged(object sender, EventArgs e)
		{
			FillResourceDataGrids();
		}

		internal void FillResourceDataGrids()
		{
			if(SelectedLine >= 0)
			{
				string relationName = this.comboBoxResource.SelectedItem.ToString();
				DataRow[] dataRowChildRelation = new DataRow[this.dataSetFlightRecords.Tables["Flight"].Rows[SelectedLine].GetChildRows(relationName).Length];

			
				dataRowChildRelation = this.dataSetFlightRecords.Tables["Flight"].Rows[SelectedLine].GetChildRows(relationName);

				DataTable table = new DataTable();
				table = this.dataSetFlightRecords.Tables["Flight"].ChildRelations[relationName].ChildTable.Clone();

				if(dataRowChildRelation.Length > 0)
				{
					for(int i = 0; i < dataRowChildRelation.Length; i++)
					{
						DataRow dataRow = table.NewRow();

						foreach(DataColumn column in dataRow.Table.Columns)
						{
							if(dataRowChildRelation[i][column.ColumnName].ToString().Length > 0)
								dataRow[column.ColumnName] = dataRowChildRelation[i][column.ColumnName].ToString();
						}

						table.Rows.Add(dataRow);
					}
				}

				MyController.ConvertDataTablesDateTimes(table, out table);

				DataView dv = new DataView(table, "","",DataViewRowState.CurrentRows);
				dv.AllowNew = false;


				DataGridTableStyle tableStyle = new DataGridTableStyle();
				tableStyle.MappingName = table.TableName.ToString();

				int numCols = table.Columns.Count;
				DataGridEnableTextBoxColumn aColumnTextColumn ;
				for(int i = 0; i < numCols; ++i)
				{
					aColumnTextColumn = new DataGridEnableTextBoxColumn(i);
					aColumnTextColumn.HeaderText = table.Columns[i].ColumnName;
					aColumnTextColumn.MappingName = table.Columns[i].ColumnName;
					aColumnTextColumn.CheckCellEnabled += new EnableCellEventHandler(SetEnableValues);
					aColumnTextColumn.Width = table.Columns[i].MaxLength;
					tableStyle.GridColumnStyles.Add(aColumnTextColumn);
				}

				this.dataGridInput.TableStyles.Clear();
				this.dataGridInput.TableStyles.Add(tableStyle);
				this.dataGridInput.AllowNavigation = false;

				this.dataGridInput.DataSource = dv;


				//################################

				DataTable tableInput = new DataTable();
				DataTable tableAODB = new DataTable();
				MyController.Compare(this.dataSetFlightRecords.Tables["Flight"].TableName.ToString(),out tableInput, out tableAODB);
				int countRow = 0;
				bool foundRow = false;
				for(; countRow < tableInput.Rows.Count && countRow < tableAODB.Rows.Count; countRow++)
				{
					if(tableInput.Rows[countRow]["FlightId"].ToString() == this.dataSetFlightRecords.Tables["Flight"].Rows[SelectedLine]["FlightId"].ToString())
					{
						if(tableInput.Rows[countRow]["FlightId"].ToString() != "" && tableAODB.Rows[countRow]["FlightId"].ToString() != "")
							foundRow = true;
					
						break;
					}
				}


				DataTable tableCompare = new DataTable();
				tableCompare = this.dataSetFlightRecords.Tables["Flight"].ChildRelations[relationName].ChildTable.Clone();

					MyController.ConvertDataTablesDateTimes(this.dataSetAODB.Tables["Flight"].ChildRelations[relationName].ChildTable, out tableCompare);
					string where = "-1";
					where = tableAODB.Rows[countRow]["FlightId"].ToString();
					DataView dvCompare = new DataView(tableCompare, "FlightId = '" + where + "'","",DataViewRowState.CurrentRows);
					dvCompare.AllowNew = false;


				if(foundRow)
				{
					tableStyle = new DataGridTableStyle();
					tableStyle.MappingName = tableCompare.TableName.ToString();

					numCols = table.Columns.Count;
					for(int i = 0; i < numCols; ++i)
					{
						aColumnTextColumn = new DataGridEnableTextBoxColumn(i);
						aColumnTextColumn.HeaderText = tableCompare.Columns[i].ColumnName;
						aColumnTextColumn.MappingName = tableCompare.Columns[i].ColumnName;
						aColumnTextColumn.CheckCellEnabled += new EnableCellEventHandler(SetEnableValuesAODB);
						aColumnTextColumn.Width = tableCompare.Columns[i].MaxLength;
						tableStyle.GridColumnStyles.Add(aColumnTextColumn);
					}

					this.dataGridAODB.TableStyles.Clear();
					this.dataGridAODB.TableStyles.Add(tableStyle);
					this.dataGridAODB.AllowNavigation = false;

					this.dataGridAODB.DataSource = dvCompare;
				}
				else
				{
					
					this.dataGridAODB.DataSource = dvCompare;
				}
			
			}
			
		}

		public void SetEnableValues(object sender, DataGridEnableEventArgs e)
		{
			e.EnableValue = false;

			if(this.dataGridInput.DataSource is DataView)  
			{  
				DataView dataView = (DataView)this.dataGridInput.DataSource; 
				if(dataView[e.Row].Row.HasErrors)
					e.Color = "Red";
			} 
		}

		public void SetEnableValuesAODB(object sender, DataGridEnableEventArgs e)
		{
			e.EnableValue = false;

			if(this.dataGridAODB.DataSource is DataView)  
			{  
				DataView dataView = (DataView)this.dataGridAODB.DataSource; 
				if(dataView[e.Row].Row.HasErrors)
					e.Color = "Red";
			} 
		}

		private void dataGridInput_Scroll(object sender, EventArgs e)
		{
			if(this.dataGridInput.HScrollBar.Value <= this.dataGridAODB.HScrollBar.Maximum && this.dataGridInput.HScrollBar.Value >= this.dataGridAODB.HScrollBar.Minimum)
			{
				this.dataGridAODB.HScrollBar.Value = this.dataGridInput.HScrollBar.Value;
				this.dataGridAODB.ScrollToColumn(this.dataGridInput.HScrollBar.Value);
			}
			if(this.dataGridInput.VScrollBar.Value <= this.dataGridAODB.VScrollBar.Maximum && this.dataGridInput.VScrollBar.Value >= this.dataGridAODB.VScrollBar.Minimum)
			{
				this.dataGridAODB.VScrollBar.Value = this.dataGridInput.VScrollBar.Value;
				this.dataGridAODB.ScrollToRow(this.dataGridInput.VScrollBar.Value);
			}
			
		}

		private void dataGridAODB_Scroll(object sender, EventArgs e)
		{
			if(this.dataGridAODB.HScrollBar.Value <= this.dataGridInput.HScrollBar.Maximum && this.dataGridAODB.HScrollBar.Value >= this.dataGridInput.HScrollBar.Minimum)
			{
				this.dataGridInput.HScrollBar.Value = this.dataGridAODB.HScrollBar.Value;
				this.dataGridInput.ScrollToColumn(this.dataGridAODB.HScrollBar.Value);
			}
			if(this.dataGridAODB.VScrollBar.Value <= this.dataGridInput.VScrollBar.Maximum && this.dataGridAODB.VScrollBar.Value >= this.dataGridInput.VScrollBar.Minimum)
			{
				this.dataGridInput.VScrollBar.Value = this.dataGridAODB.VScrollBar.Value;
				this.dataGridInput.ScrollToRow(this.dataGridAODB.VScrollBar.Value);
			}
		}
	}
}
