using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Drawing.Drawing2D;
using System.Collections;
using System.ComponentModel;
using Ufis.BL.Common;
using Ufis.Utils;

namespace Ufis.Snapshot.Win
{

	public class UfisDataGrid : System.Windows.Forms.DataGrid
	{
		public ScrollBar HScrollBar
		{
			get
			{
				return HorizScrollBar;
			}
		}
		public ScrollBar VScrollBar
		{
			get
			{
				return VertScrollBar;
			}
		}

		public void ScrollToRow(int row)
		{
			if(this.DataSource != null)
				this.GridVScrolled(this, new ScrollEventArgs(ScrollEventType.LargeIncrement, row));
		}

		public void ScrollToColumn(int column)
		{
			if(this.DataSource != null)
				this.GridHScrolled(this, new ScrollEventArgs(ScrollEventType.LargeIncrement, column));
		}



		protected override void OnPaint(PaintEventArgs e)  
		{  
			if(this.DataSource is DataTable)  
			{  
				AutoSizeColumnsDataTable(this, 0);	
			}  
			if(this.DataSource is DataView)  
			{  
 				AutoSizeColumnsDataView(this, 0);
			}  
			
			base.OnPaint(e);  
	
		}  
  
		protected static void AutoSizeColumnsDataTable(DataGrid dataGrid, int iStyle)  
		{  
			if(dataGrid.DataSource is DataTable)  
			{  
				DataTable dataTable = (DataTable)dataGrid.DataSource;  
				System.Drawing.Graphics g = dataGrid.CreateGraphics();  
				const int padding = 8;  
				Font boldFont = new Font(dataGrid.Font.Name, dataGrid.Font.Size, FontStyle.Bold);  
  
				for(int col = 0; col < dataTable.Columns.Count; col++)  
				{  
					// don't resize hidden columns  
					if(dataGrid.TableStyles[iStyle].GridColumnStyles[col].Width > 0)  
					{  
						float widest = g.MeasureString(dataGrid.TableStyles[0].GridColumnStyles[col].HeaderText, boldFont).Width;  
  
						for(int i = 0; i < dataTable.Rows.Count; ++ i)  
						{  		
							float testWidth = g.MeasureString(dataGrid[i, col].ToString(), dataGrid.Font).Width;  
							
							if(widest < testWidth)  
								widest = testWidth; 
						}
						dataGrid.TableStyles[iStyle].GridColumnStyles[col].Width = (int)widest + padding;  
					}  
				}  
			}  
		} 


		protected static void AutoSizeColumnsDataView(DataGrid dataGrid, int iStyle)  
		{  
			if(dataGrid.DataSource is DataView)  
			{  
				DataView dataView = (DataView)dataGrid.DataSource;  

				System.Drawing.Graphics g = dataGrid.CreateGraphics();  
				const int padding = 8;  
				Font boldFont = new Font(dataGrid.Font.Name, dataGrid.Font.Size, FontStyle.Bold);  
  
				for(int col = 0; col < dataView.Table.Columns.Count; col++)  
				{  
					// don't resize hidden columns  
					if(dataGrid.TableStyles[iStyle].GridColumnStyles[col].Width > 0)  
					{  
						float widest = g.MeasureString(dataGrid.TableStyles[0].GridColumnStyles[col].HeaderText, boldFont).Width;  
  
						for(int i = 0; i < dataView.Count; i++)  
						{  		
							float testWidth = g.MeasureString(dataGrid[i, col].ToString(), dataGrid.Font).Width;  
							
							if(widest < testWidth)  
								widest = testWidth; 
						}

							dataGrid.TableStyles[iStyle].GridColumnStyles[col].Width = (int)widest + padding; 
					}  
				}  
			}  
		} 
	}






	public class DataGridEnableEventArgs : EventArgs
	{
		private int _column;
		private int _row;
		private bool _enablevalue;
		private string _colorvalue;

		public DataGridEnableEventArgs(int row, int col, bool val, string cva)
		{
			_row = row;
			_column = col;
			_enablevalue = val;
			_colorvalue = cva;
		}

		public int Column
		{
			get{ return _column;}
			set{ _column = value;}
		}
		public int Row
		{
			get{ return _row;}
			set{ _row = value;}
		}
		public bool EnableValue
		{
			get{ return _enablevalue;}
			set{ _enablevalue = value;}
		}
		public string Color
		{
			get{ return _colorvalue;}
			set{ _colorvalue = value;}
		}
	}



	public delegate void EnableCellEventHandler(object sender, DataGridEnableEventArgs e);


	public class DataGridEnableTextBoxColumn : DataGridTextBoxColumn
	{
		public event EnableCellEventHandler CheckCellEnabled;
		
		private int _col;

		public DataGridEnableTextBoxColumn(int column)
		{
			_col = column;
		}

		protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle bounds, System.Windows.Forms.CurrencyManager source, int rowNum, System.Drawing.Brush backBrush, System.Drawing.Brush foreBrush, bool alignToRight)
		{
			bool enabled = true;
			string color = "";

			if(CheckCellEnabled != null)
			{
				DataGridEnableEventArgs e = new DataGridEnableEventArgs(rowNum, _col, enabled, color);
				CheckCellEnabled(this, e);

				if(e.Color == "Red")
				{
					backBrush = new LinearGradientBrush(bounds, 
						Color.FromArgb(255, 200, 200), 
						Color.FromArgb(128, 20, 20),
						LinearGradientMode.BackwardDiagonal);
					foreBrush = new SolidBrush(Color.White);
				}

				if(e.Color == "Green")
				{
					backBrush = new LinearGradientBrush(bounds, 
						Color.FromArgb(200, 255, 200), 
						Color.FromArgb(20, 128, 20),
						LinearGradientMode.BackwardDiagonal);
					foreBrush = new SolidBrush(Color.White);
				}

				if(e.Color == "Yellow")
				{
					backBrush = new LinearGradientBrush(bounds, 
						Color.FromArgb(255, 255, 200), 
						Color.FromArgb(128, 128, 20),
						LinearGradientMode.BackwardDiagonal);
					foreBrush = new SolidBrush(Color.White);
				}

				if(e.Color == "Blue")
				{
					backBrush = new LinearGradientBrush(bounds, 
						Color.FromArgb(200, 200, 255), 
						Color.FromArgb(20, 128, 128),
						LinearGradientMode.BackwardDiagonal);
					foreBrush = new SolidBrush(Color.White);
				}

				if(e.Color == "Grey")
				{
					backBrush = new LinearGradientBrush(bounds, 
						Color.FromArgb(255, 255, 255), 
						Color.FromArgb(128, 128, 128),
						LinearGradientMode.BackwardDiagonal);
					foreBrush = new SolidBrush(Color.White);
				}
			}

			base.Paint(g, bounds, source, rowNum, backBrush, foreBrush, alignToRight);
		}

		protected override void Edit(System.Windows.Forms.CurrencyManager source, int rowNum, System.Drawing.Rectangle bounds, bool readOnly, string instantText, bool cellIsVisible)
		{
			bool enabled = true;
			if(CheckCellEnabled != null)
			{
				DataGridEnableEventArgs e = new DataGridEnableEventArgs(rowNum, _col, enabled, "");
				CheckCellEnabled(this, e);
				enabled = e.EnableValue;
			}
			if(enabled)
				base.Edit(source, rowNum, bounds, readOnly, instantText, cellIsVisible);
		}
	}
}
