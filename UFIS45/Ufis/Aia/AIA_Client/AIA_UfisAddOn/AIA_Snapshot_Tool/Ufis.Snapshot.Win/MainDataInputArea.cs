using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Ufis.BL.Common;


namespace Ufis.Snapshot.Win
{
	/// <summary>
	/// Summary description for MainDataInputArea.
	/// </summary>
	public class MainDataInputArea : Microsoft.ApplicationBlocks.UIProcess.WindowsFormView
	{
		private System.Windows.Forms.Button buttonRestore;
		private System.Windows.Forms.Button buttonAODB;
		private System.Windows.Forms.Button buttonOpen;
		private System.Windows.Forms.Button buttonExtract;
		private System.Windows.Forms.Button buttonExit;
		private System.Windows.Forms.Button buttonSaveAs;
		private System.Windows.Forms.Button buttonCellEdit;
		private System.Windows.Forms.StatusBar statusBar1;
		private System.Data.DataSet dataSetInputFlightData;
		private System.Data.DataView dataViewRotation;
		private System.Windows.Forms.DataGrid dataGridCounter;
		private UfisDataGrid dataGridRotation;
		private DataRow dataRowArrival;
		private DataRow dataRowDeparture;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.GroupBox groupBoxArrivalGates;
		private System.Windows.Forms.GroupBox groupBoxArrivalBaggageBelts;
		private System.Windows.Forms.GroupBox groupBoxArrivalParkingStand;
		private System.Windows.Forms.Label labelArrival;
		private System.Windows.Forms.Label labelDeparture;
		private System.Windows.Forms.GroupBox groupBoxDepartureGates;
		private System.Windows.Forms.GroupBox groupBoxDepartureWaitingRooms;
		private System.Windows.Forms.GroupBox groupBoxDepartureParkingStand;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox textBoxArrivalParkingStandName;
		private System.Windows.Forms.TextBox textBoxArrivalParkingStandBegin;
		private System.Windows.Forms.TextBox textBoxArrivalParkingStandEnd;
		private System.Windows.Forms.TextBox textBoxArrivalBaggageBeltFirstName;
		private System.Windows.Forms.TextBox textBoxArrivalBaggageBeltFirstBegin;
		private System.Windows.Forms.TextBox textBoxArrivalBaggageBeltFirstEnd;
		private System.Windows.Forms.TextBox textBoxArrivalBaggageBeltSecondName;
		private System.Windows.Forms.TextBox textBoxArrivalBaggageBeltSecondBegin;
		private System.Windows.Forms.TextBox textBoxArrivalBaggageBeltSecondEnd;
		private System.Windows.Forms.TextBox textBoxArrivalGateFirstName;
		private System.Windows.Forms.TextBox textBoxArrivalGateFirstBegin;
		private System.Windows.Forms.TextBox textBoxArrivalGateFirstEnd;
		private System.Windows.Forms.TextBox textBoxArrivalGateSecondName;
		private System.Windows.Forms.TextBox textBoxArrivalGateSecondBegin;
		private System.Windows.Forms.TextBox textBoxArrivalGateSecondEnd;
		private System.Windows.Forms.TextBox textBoxDepartureParkingStandName;
		private System.Windows.Forms.TextBox textBoxDepartureParkingStandBegin;
		private System.Windows.Forms.TextBox textBoxDepartureParkingStandEnd;
		private System.Windows.Forms.TextBox textBoxDepartureWaitingRoomFirstName;
		private System.Windows.Forms.TextBox textBoxDepartureWaitingRoomFirstBegin;
		private System.Windows.Forms.TextBox textBoxDepartureWaitingRoomFirstEnd;
		private System.Windows.Forms.TextBox textBoxDepartureWaitingRoomSecondName;
		private System.Windows.Forms.TextBox textBoxDepartureWaitingRoomSecondBegin;
		private System.Windows.Forms.TextBox textBoxDepartureWaitingRoomSecondEnd;
		private System.Windows.Forms.TextBox textBoxDepartureGateFirstName;
		private System.Windows.Forms.TextBox textBoxDepartureGateFirstBegin;
		private System.Windows.Forms.TextBox textBoxDepartureGateFirstEnd;
		private System.Windows.Forms.TextBox textBoxDepartureGateSecondName;
		private System.Windows.Forms.TextBox textBoxDepartureGateSecondBegin;
		private System.Windows.Forms.TextBox textBoxDepartureGateSecondEnd;
		private int selectedRow = 0;
		private bool init = false;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label20;
		private DataRow[] dataRowArrivalParkingStand;
		private DataRow[] dataRowArrivalBaggageBelts;
		private DataRow[] dataRowArrivalGates;
		private DataRow[] dataRowDepartureParkingStand;
		private DataRow[] dataRowDepartureWaitingRooms;
		private DataRow[] dataRowDepartureGates;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.GroupBox groupBoxTimeFrame;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.TextBox dateTimeFromDate;
		private System.Windows.Forms.TextBox dateTimeToDate;
		private DataRow[] dataRowDepartureCounters;

		private Controller MyController
		{
			get
			{
				return (Controller)base.Controller;
			}
		}

		public MainDataInputArea()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonRestore = new System.Windows.Forms.Button();
			this.buttonAODB = new System.Windows.Forms.Button();
			this.buttonOpen = new System.Windows.Forms.Button();
			this.buttonExtract = new System.Windows.Forms.Button();
			this.buttonExit = new System.Windows.Forms.Button();
			this.buttonSaveAs = new System.Windows.Forms.Button();
			this.buttonCellEdit = new System.Windows.Forms.Button();
			this.dataGridRotation = new UfisDataGrid();
			this.statusBar1 = new System.Windows.Forms.StatusBar();
			this.dataGridCounter = new System.Windows.Forms.DataGrid();
			this.groupBoxArrivalGates = new System.Windows.Forms.GroupBox();
			this.label15 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.textBoxArrivalGateSecondName = new System.Windows.Forms.TextBox();
			this.textBoxArrivalGateSecondBegin = new System.Windows.Forms.TextBox();
			this.textBoxArrivalGateSecondEnd = new System.Windows.Forms.TextBox();
			this.textBoxArrivalGateFirstName = new System.Windows.Forms.TextBox();
			this.textBoxArrivalGateFirstBegin = new System.Windows.Forms.TextBox();
			this.textBoxArrivalGateFirstEnd = new System.Windows.Forms.TextBox();
			this.groupBoxArrivalBaggageBelts = new System.Windows.Forms.GroupBox();
			this.label13 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.textBoxArrivalBaggageBeltSecondName = new System.Windows.Forms.TextBox();
			this.textBoxArrivalBaggageBeltSecondBegin = new System.Windows.Forms.TextBox();
			this.textBoxArrivalBaggageBeltSecondEnd = new System.Windows.Forms.TextBox();
			this.textBoxArrivalBaggageBeltFirstName = new System.Windows.Forms.TextBox();
			this.textBoxArrivalBaggageBeltFirstBegin = new System.Windows.Forms.TextBox();
			this.textBoxArrivalBaggageBeltFirstEnd = new System.Windows.Forms.TextBox();
			this.groupBoxArrivalParkingStand = new System.Windows.Forms.GroupBox();
			this.textBoxArrivalParkingStandName = new System.Windows.Forms.TextBox();
			this.textBoxArrivalParkingStandBegin = new System.Windows.Forms.TextBox();
			this.textBoxArrivalParkingStandEnd = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.labelArrival = new System.Windows.Forms.Label();
			this.labelDeparture = new System.Windows.Forms.Label();
			this.groupBoxDepartureGates = new System.Windows.Forms.GroupBox();
			this.label20 = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.textBoxDepartureGateSecondName = new System.Windows.Forms.TextBox();
			this.textBoxDepartureGateSecondBegin = new System.Windows.Forms.TextBox();
			this.textBoxDepartureGateSecondEnd = new System.Windows.Forms.TextBox();
			this.textBoxDepartureGateFirstName = new System.Windows.Forms.TextBox();
			this.textBoxDepartureGateFirstBegin = new System.Windows.Forms.TextBox();
			this.textBoxDepartureGateFirstEnd = new System.Windows.Forms.TextBox();
			this.groupBoxDepartureWaitingRooms = new System.Windows.Forms.GroupBox();
			this.label18 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.textBoxDepartureWaitingRoomSecondName = new System.Windows.Forms.TextBox();
			this.textBoxDepartureWaitingRoomSecondBegin = new System.Windows.Forms.TextBox();
			this.textBoxDepartureWaitingRoomSecondEnd = new System.Windows.Forms.TextBox();
			this.textBoxDepartureWaitingRoomFirstName = new System.Windows.Forms.TextBox();
			this.textBoxDepartureWaitingRoomFirstBegin = new System.Windows.Forms.TextBox();
			this.textBoxDepartureWaitingRoomFirstEnd = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.groupBoxDepartureParkingStand = new System.Windows.Forms.GroupBox();
			this.label16 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.textBoxDepartureParkingStandName = new System.Windows.Forms.TextBox();
			this.textBoxDepartureParkingStandBegin = new System.Windows.Forms.TextBox();
			this.textBoxDepartureParkingStandEnd = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label23 = new System.Windows.Forms.Label();
			this.groupBoxTimeFrame = new System.Windows.Forms.GroupBox();
			this.dateTimeToDate = new System.Windows.Forms.TextBox();
			this.dateTimeFromDate = new System.Windows.Forms.TextBox();
			this.label22 = new System.Windows.Forms.Label();
			this.label24 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.dataGridRotation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridCounter)).BeginInit();
			this.groupBoxArrivalGates.SuspendLayout();
			this.groupBoxArrivalBaggageBelts.SuspendLayout();
			this.groupBoxArrivalParkingStand.SuspendLayout();
			this.groupBoxDepartureGates.SuspendLayout();
			this.groupBoxDepartureWaitingRooms.SuspendLayout();
			this.groupBoxDepartureParkingStand.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupBoxTimeFrame.SuspendLayout();
			this.SuspendLayout();
			// 
			// buttonRestore
			// 
			this.buttonRestore.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonRestore.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonRestore.Location = new System.Drawing.Point(70, 2);
			this.buttonRestore.Name = "buttonRestore";
			this.buttonRestore.Size = new System.Drawing.Size(65, 23);
			this.buttonRestore.TabIndex = 1;
			this.buttonRestore.Text = "Restore";
			this.buttonRestore.Click += new System.EventHandler(this.buttonRestore_Click);
			// 
			// buttonAODB
			// 
			this.buttonAODB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonAODB.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonAODB.Location = new System.Drawing.Point(138, 2);
			this.buttonAODB.Name = "buttonAODB";
			this.buttonAODB.Size = new System.Drawing.Size(65, 23);
			this.buttonAODB.TabIndex = 2;
			this.buttonAODB.Text = "AODB";
			this.buttonAODB.Click += new System.EventHandler(this.buttonAODB_Click);
			// 
			// buttonOpen
			// 
			this.buttonOpen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonOpen.Location = new System.Drawing.Point(2, 2);
			this.buttonOpen.Name = "buttonOpen";
			this.buttonOpen.Size = new System.Drawing.Size(65, 23);
			this.buttonOpen.TabIndex = 5;
			this.buttonOpen.Text = "Open";
			this.buttonOpen.Click += new System.EventHandler(this.buttonOpen_Click);
			// 
			// buttonExtract
			// 
			this.buttonExtract.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonExtract.Location = new System.Drawing.Point(206, 2);
			this.buttonExtract.Name = "buttonExtract";
			this.buttonExtract.Size = new System.Drawing.Size(65, 23);
			this.buttonExtract.TabIndex = 10;
			this.buttonExtract.Text = "Extract";
			this.buttonExtract.Click += new System.EventHandler(this.buttonExtract_Click);
			// 
			// buttonExit
			// 
			this.buttonExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonExit.Location = new System.Drawing.Point(410, 2);
			this.buttonExit.Name = "buttonExit";
			this.buttonExit.Size = new System.Drawing.Size(65, 23);
			this.buttonExit.TabIndex = 15;
			this.buttonExit.Text = "Exit";
			this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
			// 
			// buttonSaveAs
			// 
			this.buttonSaveAs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonSaveAs.Location = new System.Drawing.Point(342, 2);
			this.buttonSaveAs.Name = "buttonSaveAs";
			this.buttonSaveAs.Size = new System.Drawing.Size(65, 23);
			this.buttonSaveAs.TabIndex = 16;
			this.buttonSaveAs.Text = "Save As";
			this.buttonSaveAs.Click += new System.EventHandler(this.buttonSaveAs_Click);
			// 
			// buttonCellEdit
			// 
			this.buttonCellEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonCellEdit.Location = new System.Drawing.Point(274, 2);
			this.buttonCellEdit.Name = "buttonCellEdit";
			this.buttonCellEdit.Size = new System.Drawing.Size(65, 23);
			this.buttonCellEdit.TabIndex = 21;
			this.buttonCellEdit.Text = "Edit";
			this.buttonCellEdit.Click += new System.EventHandler(this.buttonCellEdit_Click);
			// 
			// dataGridRotation
			// 
			this.dataGridRotation.DataMember = "";
			this.dataGridRotation.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridRotation.Location = new System.Drawing.Point(2, 56);
			this.dataGridRotation.Name = "dataGridRotation";
			this.dataGridRotation.SelectionBackColor = System.Drawing.Color.Blue;
			this.dataGridRotation.SelectionForeColor = System.Drawing.Color.White;
			this.dataGridRotation.Size = new System.Drawing.Size(720, 752);
			this.dataGridRotation.TabIndex = 22;
			this.dataGridRotation.CurrentCellChanged += new System.EventHandler(this.dataGridRotation_CurrentCellChanged);
			// 
			// statusBar1
			// 
			this.statusBar1.Location = new System.Drawing.Point(0, 815);
			this.statusBar1.Name = "statusBar1";
			this.statusBar1.Size = new System.Drawing.Size(1128, 22);
			this.statusBar1.TabIndex = 23;
			// 
			// dataGridCounter
			// 
			this.dataGridCounter.DataMember = "";
			this.dataGridCounter.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridCounter.Location = new System.Drawing.Point(8, 16);
			this.dataGridCounter.Name = "dataGridCounter";
			this.dataGridCounter.ReadOnly = true;
			this.dataGridCounter.SelectionBackColor = System.Drawing.Color.Blue;
			this.dataGridCounter.SelectionForeColor = System.Drawing.Color.White;
			this.dataGridCounter.Size = new System.Drawing.Size(380, 208);
			this.dataGridCounter.TabIndex = 39;
			// 
			// groupBoxArrivalGates
			// 
			this.groupBoxArrivalGates.Controls.Add(this.label15);
			this.groupBoxArrivalGates.Controls.Add(this.label14);
			this.groupBoxArrivalGates.Controls.Add(this.label6);
			this.groupBoxArrivalGates.Controls.Add(this.label5);
			this.groupBoxArrivalGates.Controls.Add(this.textBoxArrivalGateSecondName);
			this.groupBoxArrivalGates.Controls.Add(this.textBoxArrivalGateSecondBegin);
			this.groupBoxArrivalGates.Controls.Add(this.textBoxArrivalGateSecondEnd);
			this.groupBoxArrivalGates.Controls.Add(this.textBoxArrivalGateFirstName);
			this.groupBoxArrivalGates.Controls.Add(this.textBoxArrivalGateFirstBegin);
			this.groupBoxArrivalGates.Controls.Add(this.textBoxArrivalGateFirstEnd);
			this.groupBoxArrivalGates.Location = new System.Drawing.Point(728, 256);
			this.groupBoxArrivalGates.Name = "groupBoxArrivalGates";
			this.groupBoxArrivalGates.Size = new System.Drawing.Size(396, 68);
			this.groupBoxArrivalGates.TabIndex = 68;
			this.groupBoxArrivalGates.TabStop = false;
			this.groupBoxArrivalGates.Text = "Gates";
			// 
			// label15
			// 
			this.label15.Location = new System.Drawing.Point(232, 40);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(40, 16);
			this.label15.TabIndex = 112;
			this.label15.Text = "End:";
			this.label15.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(232, 16);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(40, 16);
			this.label14.TabIndex = 111;
			this.label14.Text = "End:";
			this.label14.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(80, 40);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(40, 16);
			this.label6.TabIndex = 108;
			this.label6.Text = "Begin:";
			this.label6.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(80, 16);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(40, 16);
			this.label5.TabIndex = 107;
			this.label5.Text = "Begin:";
			this.label5.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// textBoxArrivalGateSecondName
			// 
			this.textBoxArrivalGateSecondName.Location = new System.Drawing.Point(8, 40);
			this.textBoxArrivalGateSecondName.Name = "textBoxArrivalGateSecondName";
			this.textBoxArrivalGateSecondName.ReadOnly = true;
			this.textBoxArrivalGateSecondName.Size = new System.Drawing.Size(56, 20);
			this.textBoxArrivalGateSecondName.TabIndex = 104;
			this.textBoxArrivalGateSecondName.Text = "";
			// 
			// textBoxArrivalGateSecondBegin
			// 
			this.textBoxArrivalGateSecondBegin.Location = new System.Drawing.Point(120, 40);
			this.textBoxArrivalGateSecondBegin.Name = "textBoxArrivalGateSecondBegin";
			this.textBoxArrivalGateSecondBegin.ReadOnly = true;
			this.textBoxArrivalGateSecondBegin.Size = new System.Drawing.Size(112, 20);
			this.textBoxArrivalGateSecondBegin.TabIndex = 106;
			this.textBoxArrivalGateSecondBegin.Text = "";
			// 
			// textBoxArrivalGateSecondEnd
			// 
			this.textBoxArrivalGateSecondEnd.Location = new System.Drawing.Point(272, 40);
			this.textBoxArrivalGateSecondEnd.Name = "textBoxArrivalGateSecondEnd";
			this.textBoxArrivalGateSecondEnd.ReadOnly = true;
			this.textBoxArrivalGateSecondEnd.Size = new System.Drawing.Size(112, 20);
			this.textBoxArrivalGateSecondEnd.TabIndex = 105;
			this.textBoxArrivalGateSecondEnd.Text = "";
			// 
			// textBoxArrivalGateFirstName
			// 
			this.textBoxArrivalGateFirstName.Location = new System.Drawing.Point(8, 16);
			this.textBoxArrivalGateFirstName.Name = "textBoxArrivalGateFirstName";
			this.textBoxArrivalGateFirstName.ReadOnly = true;
			this.textBoxArrivalGateFirstName.Size = new System.Drawing.Size(56, 20);
			this.textBoxArrivalGateFirstName.TabIndex = 101;
			this.textBoxArrivalGateFirstName.Text = "";
			// 
			// textBoxArrivalGateFirstBegin
			// 
			this.textBoxArrivalGateFirstBegin.Location = new System.Drawing.Point(120, 16);
			this.textBoxArrivalGateFirstBegin.Name = "textBoxArrivalGateFirstBegin";
			this.textBoxArrivalGateFirstBegin.ReadOnly = true;
			this.textBoxArrivalGateFirstBegin.Size = new System.Drawing.Size(112, 20);
			this.textBoxArrivalGateFirstBegin.TabIndex = 103;
			this.textBoxArrivalGateFirstBegin.Text = "";
			// 
			// textBoxArrivalGateFirstEnd
			// 
			this.textBoxArrivalGateFirstEnd.Location = new System.Drawing.Point(272, 16);
			this.textBoxArrivalGateFirstEnd.Name = "textBoxArrivalGateFirstEnd";
			this.textBoxArrivalGateFirstEnd.ReadOnly = true;
			this.textBoxArrivalGateFirstEnd.Size = new System.Drawing.Size(112, 20);
			this.textBoxArrivalGateFirstEnd.TabIndex = 102;
			this.textBoxArrivalGateFirstEnd.Text = "";
			// 
			// groupBoxArrivalBaggageBelts
			// 
			this.groupBoxArrivalBaggageBelts.Controls.Add(this.label13);
			this.groupBoxArrivalBaggageBelts.Controls.Add(this.label12);
			this.groupBoxArrivalBaggageBelts.Controls.Add(this.label4);
			this.groupBoxArrivalBaggageBelts.Controls.Add(this.label3);
			this.groupBoxArrivalBaggageBelts.Controls.Add(this.textBoxArrivalBaggageBeltSecondName);
			this.groupBoxArrivalBaggageBelts.Controls.Add(this.textBoxArrivalBaggageBeltSecondBegin);
			this.groupBoxArrivalBaggageBelts.Controls.Add(this.textBoxArrivalBaggageBeltSecondEnd);
			this.groupBoxArrivalBaggageBelts.Controls.Add(this.textBoxArrivalBaggageBeltFirstName);
			this.groupBoxArrivalBaggageBelts.Controls.Add(this.textBoxArrivalBaggageBeltFirstBegin);
			this.groupBoxArrivalBaggageBelts.Controls.Add(this.textBoxArrivalBaggageBeltFirstEnd);
			this.groupBoxArrivalBaggageBelts.Location = new System.Drawing.Point(728, 176);
			this.groupBoxArrivalBaggageBelts.Name = "groupBoxArrivalBaggageBelts";
			this.groupBoxArrivalBaggageBelts.Size = new System.Drawing.Size(396, 68);
			this.groupBoxArrivalBaggageBelts.TabIndex = 67;
			this.groupBoxArrivalBaggageBelts.TabStop = false;
			this.groupBoxArrivalBaggageBelts.Text = "Baggage Belts";
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(232, 40);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(40, 16);
			this.label13.TabIndex = 110;
			this.label13.Text = "End:";
			this.label13.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(232, 16);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(40, 16);
			this.label12.TabIndex = 109;
			this.label12.Text = "End:";
			this.label12.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(80, 40);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(40, 16);
			this.label4.TabIndex = 108;
			this.label4.Text = "Begin:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(80, 16);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(40, 16);
			this.label3.TabIndex = 107;
			this.label3.Text = "Begin:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// textBoxArrivalBaggageBeltSecondName
			// 
			this.textBoxArrivalBaggageBeltSecondName.Location = new System.Drawing.Point(8, 40);
			this.textBoxArrivalBaggageBeltSecondName.Name = "textBoxArrivalBaggageBeltSecondName";
			this.textBoxArrivalBaggageBeltSecondName.ReadOnly = true;
			this.textBoxArrivalBaggageBeltSecondName.Size = new System.Drawing.Size(56, 20);
			this.textBoxArrivalBaggageBeltSecondName.TabIndex = 104;
			this.textBoxArrivalBaggageBeltSecondName.Text = "";
			// 
			// textBoxArrivalBaggageBeltSecondBegin
			// 
			this.textBoxArrivalBaggageBeltSecondBegin.Location = new System.Drawing.Point(120, 40);
			this.textBoxArrivalBaggageBeltSecondBegin.Name = "textBoxArrivalBaggageBeltSecondBegin";
			this.textBoxArrivalBaggageBeltSecondBegin.ReadOnly = true;
			this.textBoxArrivalBaggageBeltSecondBegin.Size = new System.Drawing.Size(112, 20);
			this.textBoxArrivalBaggageBeltSecondBegin.TabIndex = 106;
			this.textBoxArrivalBaggageBeltSecondBegin.Text = "";
			// 
			// textBoxArrivalBaggageBeltSecondEnd
			// 
			this.textBoxArrivalBaggageBeltSecondEnd.Location = new System.Drawing.Point(272, 40);
			this.textBoxArrivalBaggageBeltSecondEnd.Name = "textBoxArrivalBaggageBeltSecondEnd";
			this.textBoxArrivalBaggageBeltSecondEnd.ReadOnly = true;
			this.textBoxArrivalBaggageBeltSecondEnd.Size = new System.Drawing.Size(112, 20);
			this.textBoxArrivalBaggageBeltSecondEnd.TabIndex = 105;
			this.textBoxArrivalBaggageBeltSecondEnd.Text = "";
			// 
			// textBoxArrivalBaggageBeltFirstName
			// 
			this.textBoxArrivalBaggageBeltFirstName.Location = new System.Drawing.Point(8, 16);
			this.textBoxArrivalBaggageBeltFirstName.Name = "textBoxArrivalBaggageBeltFirstName";
			this.textBoxArrivalBaggageBeltFirstName.ReadOnly = true;
			this.textBoxArrivalBaggageBeltFirstName.Size = new System.Drawing.Size(56, 20);
			this.textBoxArrivalBaggageBeltFirstName.TabIndex = 101;
			this.textBoxArrivalBaggageBeltFirstName.Text = "";
			// 
			// textBoxArrivalBaggageBeltFirstBegin
			// 
			this.textBoxArrivalBaggageBeltFirstBegin.Location = new System.Drawing.Point(120, 16);
			this.textBoxArrivalBaggageBeltFirstBegin.Name = "textBoxArrivalBaggageBeltFirstBegin";
			this.textBoxArrivalBaggageBeltFirstBegin.ReadOnly = true;
			this.textBoxArrivalBaggageBeltFirstBegin.Size = new System.Drawing.Size(112, 20);
			this.textBoxArrivalBaggageBeltFirstBegin.TabIndex = 103;
			this.textBoxArrivalBaggageBeltFirstBegin.Text = "";
			// 
			// textBoxArrivalBaggageBeltFirstEnd
			// 
			this.textBoxArrivalBaggageBeltFirstEnd.Location = new System.Drawing.Point(272, 16);
			this.textBoxArrivalBaggageBeltFirstEnd.Name = "textBoxArrivalBaggageBeltFirstEnd";
			this.textBoxArrivalBaggageBeltFirstEnd.ReadOnly = true;
			this.textBoxArrivalBaggageBeltFirstEnd.Size = new System.Drawing.Size(112, 20);
			this.textBoxArrivalBaggageBeltFirstEnd.TabIndex = 102;
			this.textBoxArrivalBaggageBeltFirstEnd.Text = "";
			// 
			// groupBoxArrivalParkingStand
			// 
			this.groupBoxArrivalParkingStand.Controls.Add(this.textBoxArrivalParkingStandName);
			this.groupBoxArrivalParkingStand.Controls.Add(this.textBoxArrivalParkingStandBegin);
			this.groupBoxArrivalParkingStand.Controls.Add(this.textBoxArrivalParkingStandEnd);
			this.groupBoxArrivalParkingStand.Controls.Add(this.label1);
			this.groupBoxArrivalParkingStand.Controls.Add(this.label2);
			this.groupBoxArrivalParkingStand.Location = new System.Drawing.Point(728, 120);
			this.groupBoxArrivalParkingStand.Name = "groupBoxArrivalParkingStand";
			this.groupBoxArrivalParkingStand.Size = new System.Drawing.Size(396, 44);
			this.groupBoxArrivalParkingStand.TabIndex = 66;
			this.groupBoxArrivalParkingStand.TabStop = false;
			this.groupBoxArrivalParkingStand.Text = "Parking Stand";
			// 
			// textBoxArrivalParkingStandName
			// 
			this.textBoxArrivalParkingStandName.Location = new System.Drawing.Point(8, 16);
			this.textBoxArrivalParkingStandName.Name = "textBoxArrivalParkingStandName";
			this.textBoxArrivalParkingStandName.ReadOnly = true;
			this.textBoxArrivalParkingStandName.Size = new System.Drawing.Size(56, 20);
			this.textBoxArrivalParkingStandName.TabIndex = 0;
			this.textBoxArrivalParkingStandName.Text = "";
			// 
			// textBoxArrivalParkingStandBegin
			// 
			this.textBoxArrivalParkingStandBegin.Location = new System.Drawing.Point(120, 16);
			this.textBoxArrivalParkingStandBegin.Name = "textBoxArrivalParkingStandBegin";
			this.textBoxArrivalParkingStandBegin.ReadOnly = true;
			this.textBoxArrivalParkingStandBegin.Size = new System.Drawing.Size(112, 20);
			this.textBoxArrivalParkingStandBegin.TabIndex = 100;
			this.textBoxArrivalParkingStandBegin.Text = "";
			// 
			// textBoxArrivalParkingStandEnd
			// 
			this.textBoxArrivalParkingStandEnd.Location = new System.Drawing.Point(272, 16);
			this.textBoxArrivalParkingStandEnd.Name = "textBoxArrivalParkingStandEnd";
			this.textBoxArrivalParkingStandEnd.ReadOnly = true;
			this.textBoxArrivalParkingStandEnd.Size = new System.Drawing.Size(112, 20);
			this.textBoxArrivalParkingStandEnd.TabIndex = 100;
			this.textBoxArrivalParkingStandEnd.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(80, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(40, 16);
			this.label1.TabIndex = 100;
			this.label1.Text = "Begin:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(232, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(40, 16);
			this.label2.TabIndex = 101;
			this.label2.Text = "End:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// labelArrival
			// 
			this.labelArrival.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelArrival.Location = new System.Drawing.Point(896, 96);
			this.labelArrival.Name = "labelArrival";
			this.labelArrival.Size = new System.Drawing.Size(224, 16);
			this.labelArrival.TabIndex = 69;
			this.labelArrival.Text = "Arrival Resources and Locations";
			this.labelArrival.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// labelDeparture
			// 
			this.labelDeparture.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelDeparture.Location = new System.Drawing.Point(848, 336);
			this.labelDeparture.Name = "labelDeparture";
			this.labelDeparture.Size = new System.Drawing.Size(272, 16);
			this.labelDeparture.TabIndex = 70;
			this.labelDeparture.Text = "Departure Resources and Locations";
			this.labelDeparture.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// groupBoxDepartureGates
			// 
			this.groupBoxDepartureGates.Controls.Add(this.label20);
			this.groupBoxDepartureGates.Controls.Add(this.label19);
			this.groupBoxDepartureGates.Controls.Add(this.label11);
			this.groupBoxDepartureGates.Controls.Add(this.label10);
			this.groupBoxDepartureGates.Controls.Add(this.textBoxDepartureGateSecondName);
			this.groupBoxDepartureGates.Controls.Add(this.textBoxDepartureGateSecondBegin);
			this.groupBoxDepartureGates.Controls.Add(this.textBoxDepartureGateSecondEnd);
			this.groupBoxDepartureGates.Controls.Add(this.textBoxDepartureGateFirstName);
			this.groupBoxDepartureGates.Controls.Add(this.textBoxDepartureGateFirstBegin);
			this.groupBoxDepartureGates.Controls.Add(this.textBoxDepartureGateFirstEnd);
			this.groupBoxDepartureGates.Location = new System.Drawing.Point(728, 496);
			this.groupBoxDepartureGates.Name = "groupBoxDepartureGates";
			this.groupBoxDepartureGates.Size = new System.Drawing.Size(396, 68);
			this.groupBoxDepartureGates.TabIndex = 98;
			this.groupBoxDepartureGates.TabStop = false;
			this.groupBoxDepartureGates.Text = "Gates";
			// 
			// label20
			// 
			this.label20.Location = new System.Drawing.Point(232, 40);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(40, 16);
			this.label20.TabIndex = 113;
			this.label20.Text = "End:";
			this.label20.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// label19
			// 
			this.label19.Location = new System.Drawing.Point(232, 16);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(40, 16);
			this.label19.TabIndex = 112;
			this.label19.Text = "End:";
			this.label19.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(80, 40);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(40, 16);
			this.label11.TabIndex = 111;
			this.label11.Text = "Begin:";
			this.label11.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(80, 16);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(40, 16);
			this.label10.TabIndex = 110;
			this.label10.Text = "Begin:";
			this.label10.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// textBoxDepartureGateSecondName
			// 
			this.textBoxDepartureGateSecondName.Location = new System.Drawing.Point(8, 40);
			this.textBoxDepartureGateSecondName.Name = "textBoxDepartureGateSecondName";
			this.textBoxDepartureGateSecondName.ReadOnly = true;
			this.textBoxDepartureGateSecondName.Size = new System.Drawing.Size(56, 20);
			this.textBoxDepartureGateSecondName.TabIndex = 107;
			this.textBoxDepartureGateSecondName.Text = "";
			// 
			// textBoxDepartureGateSecondBegin
			// 
			this.textBoxDepartureGateSecondBegin.Location = new System.Drawing.Point(120, 40);
			this.textBoxDepartureGateSecondBegin.Name = "textBoxDepartureGateSecondBegin";
			this.textBoxDepartureGateSecondBegin.ReadOnly = true;
			this.textBoxDepartureGateSecondBegin.Size = new System.Drawing.Size(112, 20);
			this.textBoxDepartureGateSecondBegin.TabIndex = 109;
			this.textBoxDepartureGateSecondBegin.Text = "";
			// 
			// textBoxDepartureGateSecondEnd
			// 
			this.textBoxDepartureGateSecondEnd.Location = new System.Drawing.Point(272, 40);
			this.textBoxDepartureGateSecondEnd.Name = "textBoxDepartureGateSecondEnd";
			this.textBoxDepartureGateSecondEnd.ReadOnly = true;
			this.textBoxDepartureGateSecondEnd.Size = new System.Drawing.Size(112, 20);
			this.textBoxDepartureGateSecondEnd.TabIndex = 108;
			this.textBoxDepartureGateSecondEnd.Text = "";
			// 
			// textBoxDepartureGateFirstName
			// 
			this.textBoxDepartureGateFirstName.Location = new System.Drawing.Point(8, 16);
			this.textBoxDepartureGateFirstName.Name = "textBoxDepartureGateFirstName";
			this.textBoxDepartureGateFirstName.ReadOnly = true;
			this.textBoxDepartureGateFirstName.Size = new System.Drawing.Size(56, 20);
			this.textBoxDepartureGateFirstName.TabIndex = 104;
			this.textBoxDepartureGateFirstName.Text = "";
			// 
			// textBoxDepartureGateFirstBegin
			// 
			this.textBoxDepartureGateFirstBegin.Location = new System.Drawing.Point(120, 16);
			this.textBoxDepartureGateFirstBegin.Name = "textBoxDepartureGateFirstBegin";
			this.textBoxDepartureGateFirstBegin.ReadOnly = true;
			this.textBoxDepartureGateFirstBegin.Size = new System.Drawing.Size(112, 20);
			this.textBoxDepartureGateFirstBegin.TabIndex = 106;
			this.textBoxDepartureGateFirstBegin.Text = "";
			// 
			// textBoxDepartureGateFirstEnd
			// 
			this.textBoxDepartureGateFirstEnd.Location = new System.Drawing.Point(272, 16);
			this.textBoxDepartureGateFirstEnd.Name = "textBoxDepartureGateFirstEnd";
			this.textBoxDepartureGateFirstEnd.ReadOnly = true;
			this.textBoxDepartureGateFirstEnd.Size = new System.Drawing.Size(112, 20);
			this.textBoxDepartureGateFirstEnd.TabIndex = 105;
			this.textBoxDepartureGateFirstEnd.Text = "";
			// 
			// groupBoxDepartureWaitingRooms
			// 
			this.groupBoxDepartureWaitingRooms.Controls.Add(this.label18);
			this.groupBoxDepartureWaitingRooms.Controls.Add(this.label17);
			this.groupBoxDepartureWaitingRooms.Controls.Add(this.label9);
			this.groupBoxDepartureWaitingRooms.Controls.Add(this.textBoxDepartureWaitingRoomSecondName);
			this.groupBoxDepartureWaitingRooms.Controls.Add(this.textBoxDepartureWaitingRoomSecondBegin);
			this.groupBoxDepartureWaitingRooms.Controls.Add(this.textBoxDepartureWaitingRoomSecondEnd);
			this.groupBoxDepartureWaitingRooms.Controls.Add(this.textBoxDepartureWaitingRoomFirstName);
			this.groupBoxDepartureWaitingRooms.Controls.Add(this.textBoxDepartureWaitingRoomFirstBegin);
			this.groupBoxDepartureWaitingRooms.Controls.Add(this.textBoxDepartureWaitingRoomFirstEnd);
			this.groupBoxDepartureWaitingRooms.Controls.Add(this.label8);
			this.groupBoxDepartureWaitingRooms.Location = new System.Drawing.Point(728, 416);
			this.groupBoxDepartureWaitingRooms.Name = "groupBoxDepartureWaitingRooms";
			this.groupBoxDepartureWaitingRooms.Size = new System.Drawing.Size(396, 68);
			this.groupBoxDepartureWaitingRooms.TabIndex = 97;
			this.groupBoxDepartureWaitingRooms.TabStop = false;
			this.groupBoxDepartureWaitingRooms.Text = "Waiting Rooms";
			// 
			// label18
			// 
			this.label18.Location = new System.Drawing.Point(232, 40);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(40, 16);
			this.label18.TabIndex = 112;
			this.label18.Text = "End:";
			this.label18.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// label17
			// 
			this.label17.Location = new System.Drawing.Point(232, 16);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(40, 16);
			this.label17.TabIndex = 111;
			this.label17.Text = "End:";
			this.label17.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(80, 40);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(40, 16);
			this.label9.TabIndex = 110;
			this.label9.Text = "Begin:";
			this.label9.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// textBoxDepartureWaitingRoomSecondName
			// 
			this.textBoxDepartureWaitingRoomSecondName.Location = new System.Drawing.Point(8, 40);
			this.textBoxDepartureWaitingRoomSecondName.Name = "textBoxDepartureWaitingRoomSecondName";
			this.textBoxDepartureWaitingRoomSecondName.ReadOnly = true;
			this.textBoxDepartureWaitingRoomSecondName.Size = new System.Drawing.Size(56, 20);
			this.textBoxDepartureWaitingRoomSecondName.TabIndex = 107;
			this.textBoxDepartureWaitingRoomSecondName.Text = "";
			// 
			// textBoxDepartureWaitingRoomSecondBegin
			// 
			this.textBoxDepartureWaitingRoomSecondBegin.Location = new System.Drawing.Point(120, 40);
			this.textBoxDepartureWaitingRoomSecondBegin.Name = "textBoxDepartureWaitingRoomSecondBegin";
			this.textBoxDepartureWaitingRoomSecondBegin.ReadOnly = true;
			this.textBoxDepartureWaitingRoomSecondBegin.Size = new System.Drawing.Size(112, 20);
			this.textBoxDepartureWaitingRoomSecondBegin.TabIndex = 109;
			this.textBoxDepartureWaitingRoomSecondBegin.Text = "";
			// 
			// textBoxDepartureWaitingRoomSecondEnd
			// 
			this.textBoxDepartureWaitingRoomSecondEnd.Location = new System.Drawing.Point(272, 40);
			this.textBoxDepartureWaitingRoomSecondEnd.Name = "textBoxDepartureWaitingRoomSecondEnd";
			this.textBoxDepartureWaitingRoomSecondEnd.ReadOnly = true;
			this.textBoxDepartureWaitingRoomSecondEnd.Size = new System.Drawing.Size(112, 20);
			this.textBoxDepartureWaitingRoomSecondEnd.TabIndex = 108;
			this.textBoxDepartureWaitingRoomSecondEnd.Text = "";
			// 
			// textBoxDepartureWaitingRoomFirstName
			// 
			this.textBoxDepartureWaitingRoomFirstName.Location = new System.Drawing.Point(8, 16);
			this.textBoxDepartureWaitingRoomFirstName.Name = "textBoxDepartureWaitingRoomFirstName";
			this.textBoxDepartureWaitingRoomFirstName.ReadOnly = true;
			this.textBoxDepartureWaitingRoomFirstName.Size = new System.Drawing.Size(56, 20);
			this.textBoxDepartureWaitingRoomFirstName.TabIndex = 104;
			this.textBoxDepartureWaitingRoomFirstName.Text = "";
			// 
			// textBoxDepartureWaitingRoomFirstBegin
			// 
			this.textBoxDepartureWaitingRoomFirstBegin.Location = new System.Drawing.Point(120, 16);
			this.textBoxDepartureWaitingRoomFirstBegin.Name = "textBoxDepartureWaitingRoomFirstBegin";
			this.textBoxDepartureWaitingRoomFirstBegin.ReadOnly = true;
			this.textBoxDepartureWaitingRoomFirstBegin.Size = new System.Drawing.Size(112, 20);
			this.textBoxDepartureWaitingRoomFirstBegin.TabIndex = 106;
			this.textBoxDepartureWaitingRoomFirstBegin.Text = "";
			// 
			// textBoxDepartureWaitingRoomFirstEnd
			// 
			this.textBoxDepartureWaitingRoomFirstEnd.Location = new System.Drawing.Point(272, 16);
			this.textBoxDepartureWaitingRoomFirstEnd.Name = "textBoxDepartureWaitingRoomFirstEnd";
			this.textBoxDepartureWaitingRoomFirstEnd.ReadOnly = true;
			this.textBoxDepartureWaitingRoomFirstEnd.Size = new System.Drawing.Size(112, 20);
			this.textBoxDepartureWaitingRoomFirstEnd.TabIndex = 105;
			this.textBoxDepartureWaitingRoomFirstEnd.Text = "";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(80, 16);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(40, 16);
			this.label8.TabIndex = 105;
			this.label8.Text = "Begin:";
			this.label8.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// groupBoxDepartureParkingStand
			// 
			this.groupBoxDepartureParkingStand.Controls.Add(this.label16);
			this.groupBoxDepartureParkingStand.Controls.Add(this.label7);
			this.groupBoxDepartureParkingStand.Controls.Add(this.textBoxDepartureParkingStandName);
			this.groupBoxDepartureParkingStand.Controls.Add(this.textBoxDepartureParkingStandBegin);
			this.groupBoxDepartureParkingStand.Controls.Add(this.textBoxDepartureParkingStandEnd);
			this.groupBoxDepartureParkingStand.Location = new System.Drawing.Point(728, 360);
			this.groupBoxDepartureParkingStand.Name = "groupBoxDepartureParkingStand";
			this.groupBoxDepartureParkingStand.Size = new System.Drawing.Size(396, 44);
			this.groupBoxDepartureParkingStand.TabIndex = 96;
			this.groupBoxDepartureParkingStand.TabStop = false;
			this.groupBoxDepartureParkingStand.Text = "Parking Stand";
			// 
			// label16
			// 
			this.label16.Location = new System.Drawing.Point(232, 16);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(40, 16);
			this.label16.TabIndex = 111;
			this.label16.Text = "End:";
			this.label16.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(80, 16);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(40, 16);
			this.label7.TabIndex = 104;
			this.label7.Text = "Begin:";
			this.label7.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// textBoxDepartureParkingStandName
			// 
			this.textBoxDepartureParkingStandName.Location = new System.Drawing.Point(8, 16);
			this.textBoxDepartureParkingStandName.Name = "textBoxDepartureParkingStandName";
			this.textBoxDepartureParkingStandName.ReadOnly = true;
			this.textBoxDepartureParkingStandName.Size = new System.Drawing.Size(56, 20);
			this.textBoxDepartureParkingStandName.TabIndex = 101;
			this.textBoxDepartureParkingStandName.Text = "";
			// 
			// textBoxDepartureParkingStandBegin
			// 
			this.textBoxDepartureParkingStandBegin.Location = new System.Drawing.Point(120, 16);
			this.textBoxDepartureParkingStandBegin.Name = "textBoxDepartureParkingStandBegin";
			this.textBoxDepartureParkingStandBegin.ReadOnly = true;
			this.textBoxDepartureParkingStandBegin.Size = new System.Drawing.Size(112, 20);
			this.textBoxDepartureParkingStandBegin.TabIndex = 103;
			this.textBoxDepartureParkingStandBegin.Text = "";
			// 
			// textBoxDepartureParkingStandEnd
			// 
			this.textBoxDepartureParkingStandEnd.Location = new System.Drawing.Point(272, 16);
			this.textBoxDepartureParkingStandEnd.Name = "textBoxDepartureParkingStandEnd";
			this.textBoxDepartureParkingStandEnd.ReadOnly = true;
			this.textBoxDepartureParkingStandEnd.Size = new System.Drawing.Size(112, 20);
			this.textBoxDepartureParkingStandEnd.TabIndex = 102;
			this.textBoxDepartureParkingStandEnd.Text = "";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.dataGridCounter);
			this.groupBox1.Location = new System.Drawing.Point(728, 576);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(396, 232);
			this.groupBox1.TabIndex = 99;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Counters";
			// 
			// label23
			// 
			this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label23.Location = new System.Drawing.Point(2, 34);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(156, 16);
			this.label23.TabIndex = 105;
			this.label23.Text = "Flights as Rotations";
			// 
			// groupBoxTimeFrame
			// 
			this.groupBoxTimeFrame.Controls.Add(this.dateTimeToDate);
			this.groupBoxTimeFrame.Controls.Add(this.dateTimeFromDate);
			this.groupBoxTimeFrame.Controls.Add(this.label22);
			this.groupBoxTimeFrame.Controls.Add(this.label24);
			this.groupBoxTimeFrame.Location = new System.Drawing.Point(958, 4);
			this.groupBoxTimeFrame.Name = "groupBoxTimeFrame";
			this.groupBoxTimeFrame.Size = new System.Drawing.Size(166, 72);
			this.groupBoxTimeFrame.TabIndex = 106;
			this.groupBoxTimeFrame.TabStop = false;
			this.groupBoxTimeFrame.Text = "Loaded Timeframe";
			// 
			// dateTimeToDate
			// 
			this.dateTimeToDate.Location = new System.Drawing.Point(44, 44);
			this.dateTimeToDate.Name = "dateTimeToDate";
			this.dateTimeToDate.ReadOnly = true;
			this.dateTimeToDate.Size = new System.Drawing.Size(116, 20);
			this.dateTimeToDate.TabIndex = 105;
			this.dateTimeToDate.Text = "";
			// 
			// dateTimeFromDate
			// 
			this.dateTimeFromDate.Location = new System.Drawing.Point(44, 16);
			this.dateTimeFromDate.Name = "dateTimeFromDate";
			this.dateTimeFromDate.ReadOnly = true;
			this.dateTimeFromDate.Size = new System.Drawing.Size(116, 20);
			this.dateTimeFromDate.TabIndex = 104;
			this.dateTimeFromDate.Text = "";
			// 
			// label22
			// 
			this.label22.Location = new System.Drawing.Point(8, 36);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(30, 23);
			this.label22.TabIndex = 6;
			this.label22.Text = "End:";
			this.label22.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// label24
			// 
			this.label24.Location = new System.Drawing.Point(4, 12);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(36, 23);
			this.label24.TabIndex = 5;
			this.label24.Text = "Begin:";
			this.label24.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			// 
			// MainDataInputArea
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(1128, 837);
			this.Controls.Add(this.groupBoxTimeFrame);
			this.Controls.Add(this.label23);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.groupBoxDepartureGates);
			this.Controls.Add(this.groupBoxDepartureWaitingRooms);
			this.Controls.Add(this.groupBoxDepartureParkingStand);
			this.Controls.Add(this.labelDeparture);
			this.Controls.Add(this.labelArrival);
			this.Controls.Add(this.groupBoxArrivalGates);
			this.Controls.Add(this.groupBoxArrivalBaggageBelts);
			this.Controls.Add(this.groupBoxArrivalParkingStand);
			this.Controls.Add(this.statusBar1);
			this.Controls.Add(this.dataGridRotation);
			this.Controls.Add(this.buttonCellEdit);
			this.Controls.Add(this.buttonSaveAs);
			this.Controls.Add(this.buttonExit);
			this.Controls.Add(this.buttonExtract);
			this.Controls.Add(this.buttonOpen);
			this.Controls.Add(this.buttonAODB);
			this.Controls.Add(this.buttonRestore);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.Name = "MainDataInputArea";
			this.Text = "MainDataInputArea";
			this.Load += new System.EventHandler(this.MainDataInputArea_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataGridRotation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridCounter)).EndInit();
			this.groupBoxArrivalGates.ResumeLayout(false);
			this.groupBoxArrivalBaggageBelts.ResumeLayout(false);
			this.groupBoxArrivalParkingStand.ResumeLayout(false);
			this.groupBoxDepartureGates.ResumeLayout(false);
			this.groupBoxDepartureWaitingRooms.ResumeLayout(false);
			this.groupBoxDepartureParkingStand.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBoxTimeFrame.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion


		// Open the last used Snapshot Backup file
		private void buttonRestore_Click(object sender, System.EventArgs e)
		{
			string filelocation = MyController.LastUsedFile;
			if(filelocation != null)
			{
				try
				{
					this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
					MyController.LoadXML(filelocation,"LocationManagement");
					this.dataSetInputFlightData = MyController.InputDataSet;
					PrepareDataGrid();
					ClearArrivalControls();
					ClearDepartureControls();
				}
				finally
				{
					this.Cursor = System.Windows.Forms.Cursors.Default;
				}
			} 
			else
			{
				MessageBox.Show("Nothing to restore...");
			}
		}

		// will open the record filter preset dialog in order to define the desired condition
		// for the scenario that shall be exported from the server
		private void buttonAODB_Click(object sender, System.EventArgs e)
		{
			MyController.InputDataSet = null;
			MyController.SelectService("RecordFilterDialog");
			if(MyController.InputDataSet != null)
			{
				this.dataSetInputFlightData = MyController.InputDataSet;
				PrepareDataGrid();
				ClearArrivalControls();
				ClearDepartureControls();
				MyController.SelectService("DataExtractViewer");
			}
			else
			{
				MessageBox.Show("No input available to extract...");
			}
			
		}

		// enables the user to open an existing file (same as select button)
		private void buttonSnapshot_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog();
		}

		private void MainDataInputArea_Load(object sender, System.EventArgs e)
		{
			MyController.RegisterService();
		}

		// select and open one of the existing snapshot files
		private void buttonSelect_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog();
		}

		// identify the overnight flight records (light grey)
		private void buttonShrink_Click(object sender, System.EventArgs e)
		{
		
		}

		// re-open the last selected file or select one
		private void buttonOpen_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog();
		}

		// basic data and other validation checks
		private void buttonCheck_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show("Checking data...");	
		}


		// opens the extract viewer and the filter dialog
		private void buttonExtract_Click(object sender, System.EventArgs e)
		{
			if(this.dataSetInputFlightData != null)
			{
				MyController.SelectService("RecordFilterDialog");
				if(MyController.InputDataSet != null)
				{	
					this.dataSetInputFlightData = MyController.InputDataSet;
					PrepareDataGrid();
					ClearArrivalControls();
					ClearDepartureControls();
					MyController.SelectService("DataExtractViewer");
				}		
			}
			else
			{
				MessageBox.Show("No input available to extract...");
			}
		}

		// quit program
		private void buttonExit_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		// show help
		private void buttonHelp_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show("Showing Help");
		
		}

		// show about
		private void buttonAbout_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show("Showing About");	
		}

		// disabled
		private void buttonConfig_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show("Showing Config");
		
		}

		// Opens a dialog to define the desired export file type
		private void buttonSaveAs_Click(object sender, System.EventArgs e)
		{
			if(this.dataSetInputFlightData != null)
			{
				try
				{
					this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
					SaveFileDialog();
				}
				finally
				{
					this.Cursor = System.Windows.Forms.Cursors.Default;
				}
			}
			else
			{
				MessageBox.Show("Nothing to save...");
			}
		}

		internal void SaveFileDialog()
		{
			using (SaveFileDialog dlg = new SaveFileDialog())
			{
				dlg.Title = "Save Export File";
				dlg.Filter = "XML Files (*.xml)" + "|*.xml|All Files (*.*)|*.*";

				try
				{
					if (dlg.ShowDialog() == DialogResult.OK)
					{
						this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
						MyController.LastUsedFile = dlg.FileName;
						MyController.WriteXML(dlg.FileName,this.dataSetInputFlightData);
						this.Cursor = System.Windows.Forms.Cursors.Default;
					}
				}
				catch (Exception)
				{
					MessageBox.Show("Unable to save "
						+ dlg.FileName,
						"Export Error",
						MessageBoxButtons.OK,
						MessageBoxIcon.Error);
				}
			}	
		}

		internal void OpenFileDialog()
		{
			using (OpenFileDialog dlg = new OpenFileDialog())
			{
				dlg.Title = "Open Export File";
				dlg.Filter = "XML Files (*.xml)" + "|*.xml|All Files (*.*)|*.*";

				try
				{
					if (dlg.ShowDialog() == DialogResult.OK)
					{
						this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
						MyController.LastUsedFile = dlg.FileName;			
						MyController.GetBusinessCaseDataInput("LocationManagement");
						MyController.LoadXML(dlg.FileName,"LocationManagement");
						this.dataSetInputFlightData = MyController.InputDataSet;
						PrepareDataGrid();
					}
				}
				catch (Exception)
				{
					MessageBox.Show("Unable to open "
						+ dlg.FileName,
						"Open Error",
						MessageBoxButtons.OK,
						MessageBoxIcon.Error);
				}
				finally
				{
					this.Cursor = System.Windows.Forms.Cursors.Default;
				}
			}	
		}





		internal void PrepareDataGrid()
		{

			MyController.SetInputTimeFrame();

			MyController.FillRotationTable();

			DataTable dt = new DataTable();
			MyController.ConvertDataTablesDateTimes(this.dataSetInputFlightData.Tables["Rotation"], out dt);
			this.dataViewRotation = new DataView(dt, "", "", DataViewRowState.CurrentRows);
			this.dataViewRotation.AllowNew = false;
			
			this.dateTimeFromDate.Text = MyController.LoadFrom.ToString();
			this.dateTimeToDate.Text = MyController.LoadTo.ToString();

			DataGridTableStyle tableStyle = new DataGridTableStyle();
			tableStyle.MappingName = "Rotation";

			int numCols = this.dataSetInputFlightData.Tables["Rotation"].Columns.Count;
			DataGridEnableTextBoxColumn aColumnTextColumn ;
			for(int i = 0; i < numCols; ++i)
			{
				aColumnTextColumn = new DataGridEnableTextBoxColumn(i);
				aColumnTextColumn.HeaderText = this.dataSetInputFlightData.Tables["Rotation"].Columns[i].ColumnName;
				aColumnTextColumn.MappingName = this.dataSetInputFlightData.Tables["Rotation"].Columns[i].ColumnName;
				aColumnTextColumn.CheckCellEnabled += new EnableCellEventHandler(SetEnableValues);
				aColumnTextColumn.Width = this.dataSetInputFlightData.Tables["Rotation"].Columns[i].MaxLength;
				tableStyle.GridColumnStyles.Add(aColumnTextColumn);
			}

			this.dataGridRotation.TableStyles.Clear();
			this.dataGridRotation.TableStyles.Add(tableStyle);
			this.dataGridRotation.AllowNavigation = false;

			this.dataGridRotation.DataSource = this.dataViewRotation;



		}


		public void SetEnableValues(object sender, DataGridEnableEventArgs e)
		{
			e.EnableValue = false;

			if(this.dataSetInputFlightData.Tables["Rotation"].Rows[e.Row].HasErrors)
				e.Color = "Red";
		}




		public bool Init
		{
			get
			{
				return this.init;
			}
			set
			{
				this.init = value;
			}
		}

		internal void ClearArrivalControls()
		{
			this.textBoxArrivalParkingStandName.ResetText();
			this.textBoxArrivalParkingStandBegin.ResetText();
			this.textBoxArrivalParkingStandEnd.ResetText();
    

			this.textBoxArrivalBaggageBeltFirstName.ResetText();
			this.textBoxArrivalBaggageBeltFirstBegin.ResetText();
			this.textBoxArrivalBaggageBeltFirstEnd.ResetText();
    

			this.textBoxArrivalBaggageBeltSecondName.ResetText();
			this.textBoxArrivalBaggageBeltSecondBegin.ResetText();
			this.textBoxArrivalBaggageBeltSecondEnd.ResetText();
    

			this.textBoxArrivalGateFirstName.ResetText();
			this.textBoxArrivalGateFirstBegin.ResetText();
			this.textBoxArrivalGateFirstEnd.ResetText();
   

			this.textBoxArrivalGateSecondName.ResetText();
			this.textBoxArrivalGateSecondBegin.ResetText();
			this.textBoxArrivalGateSecondEnd.ResetText();
	
		}

		internal void ClearDepartureControls()
		{
			this.textBoxDepartureParkingStandName.ResetText();
			this.textBoxDepartureParkingStandBegin.ResetText();
			this.textBoxDepartureParkingStandEnd.ResetText();
    

			this.textBoxDepartureWaitingRoomFirstName.ResetText();
			this.textBoxDepartureWaitingRoomFirstBegin.ResetText();
			this.textBoxDepartureWaitingRoomFirstEnd.ResetText();
    

			this.textBoxDepartureWaitingRoomSecondName.ResetText();
			this.textBoxDepartureWaitingRoomSecondBegin.ResetText();
			this.textBoxDepartureWaitingRoomSecondEnd.ResetText();
    

			this.textBoxDepartureGateFirstName.ResetText();
			this.textBoxDepartureGateFirstBegin.ResetText();
			this.textBoxDepartureGateFirstEnd.ResetText();
   

			this.textBoxDepartureGateSecondName.ResetText();
			this.textBoxDepartureGateSecondBegin.ResetText();
			this.textBoxDepartureGateSecondEnd.ResetText();
		}

		internal void FillArrivalControls()
		{
			this.textBoxArrivalParkingStandName.Text = this.dataRowArrivalParkingStand[0]["ParkingStandArrivalName"].ToString();
			this.textBoxArrivalParkingStandBegin.Text = MyController.StringFromDateTime(this.dataRowArrivalParkingStand[0]["ParkingStandArrivalBegin"].ToString());
			this.textBoxArrivalParkingStandEnd.Text = MyController.StringFromDateTime(this.dataRowArrivalParkingStand[0]["ParkingStandArrivalEnd"].ToString());   

			this.textBoxArrivalBaggageBeltFirstName.Text = this.dataRowArrivalBaggageBelts[0]["BaggageBeltsFirstName"].ToString();
			this.textBoxArrivalBaggageBeltFirstBegin.Text = MyController.StringFromDateTime(this.dataRowArrivalBaggageBelts[0]["BaggageBeltsFirstBegin"].ToString());
			this.textBoxArrivalBaggageBeltFirstEnd.Text = MyController.StringFromDateTime(this.dataRowArrivalBaggageBelts[0]["BaggageBeltsFirstEnd"].ToString());   

			this.textBoxArrivalBaggageBeltSecondName.Text = this.dataRowArrivalBaggageBelts[0]["BaggageBeltsSecondName"].ToString();
			this.textBoxArrivalBaggageBeltSecondBegin.Text = MyController.StringFromDateTime(this.dataRowArrivalBaggageBelts[0]["BaggageBeltsSecondBegin"].ToString());
			this.textBoxArrivalBaggageBeltSecondEnd.Text = MyController.StringFromDateTime(this.dataRowArrivalBaggageBelts[0]["BaggageBeltsSecondEnd"].ToString());

			this.textBoxArrivalGateFirstName.Text = this.dataRowArrivalGates[0]["GatesArrivalFirstName"].ToString();
			this.textBoxArrivalGateFirstBegin.Text = MyController.StringFromDateTime(this.dataRowArrivalGates[0]["GatesArrivalFirstBegin"].ToString());
			this.textBoxArrivalGateFirstEnd.Text = MyController.StringFromDateTime(this.dataRowArrivalGates[0]["GatesArrivalFirstEnd"].ToString());
   
			this.textBoxArrivalGateSecondName.Text = this.dataRowArrivalGates[0]["GatesArrivalSecondName"].ToString();
			this.textBoxArrivalGateSecondBegin.Text = MyController.StringFromDateTime(this.dataRowArrivalGates[0]["GatesArrivalSecondBegin"].ToString());
			this.textBoxArrivalGateSecondEnd.Text = MyController.StringFromDateTime(this.dataRowArrivalGates[0]["GatesArrivalSecondEnd"].ToString());
		}

		internal void FillDepartureControls()
		{
			this.textBoxDepartureParkingStandName.Text = this.dataRowDepartureParkingStand[0]["ParkingStandDepartureName"].ToString();
			this.textBoxDepartureParkingStandBegin.Text = MyController.StringFromDateTime(this.dataRowDepartureParkingStand[0]["ParkingStandDepartureBegin"].ToString());
			this.textBoxDepartureParkingStandEnd.Text = MyController.StringFromDateTime(this.dataRowDepartureParkingStand[0]["ParkingStandDepartureEnd"].ToString());

			this.textBoxDepartureWaitingRoomFirstName.Text = dataRowDepartureWaitingRooms[0]["WaitingRoomsFirstName"].ToString();
			this.textBoxDepartureWaitingRoomFirstBegin.Text = MyController.StringFromDateTime(this.dataRowDepartureWaitingRooms[0]["WaitingRoomsFirstBegin"].ToString());
			this.textBoxDepartureWaitingRoomFirstEnd.Text = MyController.StringFromDateTime(this.dataRowDepartureWaitingRooms[0]["WaitingRoomsFirstEnd"].ToString());

			this.textBoxDepartureWaitingRoomSecondName.Text = dataRowDepartureWaitingRooms[0]["WaitingRoomsSecondName"].ToString();
			this.textBoxDepartureWaitingRoomSecondBegin.Text = MyController.StringFromDateTime(this.dataRowDepartureWaitingRooms[0]["WaitingRoomsSecondBegin"].ToString());
			this.textBoxDepartureWaitingRoomSecondEnd.Text = MyController.StringFromDateTime(this.dataRowDepartureWaitingRooms[0]["WaitingRoomsSecondEnd"].ToString());

			this.textBoxDepartureGateFirstName.Text = dataRowDepartureGates[0]["GatesDepartureFirstName"].ToString();
			this.textBoxDepartureGateFirstBegin.Text = MyController.StringFromDateTime(this.dataRowDepartureGates[0]["GatesDepartureFirstBegin"].ToString());
			this.textBoxDepartureGateFirstEnd.Text = MyController.StringFromDateTime(this.dataRowDepartureGates[0]["GatesDepartureFirstEnd"].ToString());

			this.textBoxDepartureGateSecondName.Text = dataRowDepartureGates[0]["GatesDepartureSecondName"].ToString();
			this.textBoxDepartureGateSecondBegin.Text = MyController.StringFromDateTime(this.dataRowDepartureGates[0]["GatesDepartureSecondBegin"].ToString());
			this.textBoxDepartureGateSecondEnd.Text = MyController.StringFromDateTime(this.dataRowDepartureGates[0]["GatesDepartureSecondEnd"].ToString());
	
			
		}

		private void buttonCellEdit_Click(object sender, System.EventArgs e)
		{
			if(this.dataGridRotation.IsSelected(SelectedRow))
			{
				try
				{
					this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
					MyController.SelectService("RecordDetail");	
					if(MyController.InputDataSet.HasChanges())
					{
						MyController.FillRotationTable();
						ClearArrivalControls();
						ClearDepartureControls();
					}
				}
				finally
				{
					this.Cursor = System.Windows.Forms.Cursors.Default;
				}
				
			}
			else
			{
				MessageBox.Show("No row selected");
			}
		}

		public int SelectedRow
		{
			get
			{
				return this.selectedRow;
			}
			set
			{
				this.selectedRow = value;
			}
		}

		private void dataGridRotation_CurrentCellChanged(object sender, EventArgs e)
		{

			this.dataGridRotation.Select(this.dataGridRotation.CurrentCell.RowNumber); 	

			SelectedRow = this.dataGridRotation.CurrentCell.RowNumber;

			//determine selected row in datatable "rotation"
			int rowId = this.dataViewRotation.Table.Columns["RotationId"].Ordinal;
			DataGridCell dgc = new DataGridCell(this.dataGridRotation.CurrentCell.RowNumber,rowId);
			string selectedRotationId = this.dataGridRotation[dgc].ToString();
			
			DataView dv = new DataView(this.dataSetInputFlightData.Tables["Rotation"], "RotationId = " + selectedRotationId, "", DataViewRowState.CurrentRows);
			
			MyController.CurrentRotation = dv[0].Row;

			MyController.GetCorrespondingFlights(out this.dataRowArrival, out this.dataRowDeparture);
		
			ClearArrivalControls();
			ClearDepartureControls();


		
			if(this.dataRowDeparture.RowState != DataRowState.Detached)
			{
				this.dataRowDepartureParkingStand = this.dataRowDeparture.GetChildRows("FlightRelatedParkingStands");
				this.dataRowDepartureWaitingRooms = this.dataRowDeparture.GetChildRows("FlightRelatedWaitingRooms");
				this.dataRowDepartureGates = this.dataRowDeparture.GetChildRows("FlightRelatedGates");
				this.dataRowDepartureCounters = this.dataRowDeparture.GetChildRows("FlightRelatedCounters");

				FillDepartureControls();
			}

			if(this.dataRowArrival.RowState != DataRowState.Detached)
			{
				this.dataRowArrivalParkingStand = this.dataRowArrival.GetChildRows("FlightRelatedParkingStands");
				this.dataRowArrivalBaggageBelts = this.dataRowArrival.GetChildRows("FlightRelatedBaggageBelts");
				this.dataRowArrivalGates = this.dataRowArrival.GetChildRows("FlightRelatedGates");
				
				FillArrivalControls();
			}				
		}
	}
}
