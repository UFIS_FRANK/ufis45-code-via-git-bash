using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Ufis.Utils;
using Ufis.BL.Common;

namespace Ufis.Snapshot.Win
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class RecordDetail : Microsoft.ApplicationBlocks.UIProcess.WindowsFormView
	{
		private System.Windows.Forms.ErrorProvider errorProvider1;
		private System.Windows.Forms.Panel panelArrival;
		private System.Windows.Forms.Label labelArrivalFlightNumber;
		private System.Windows.Forms.TextBox textBoxArrivalFlightNumber;
		private System.Windows.Forms.TextBox textBoxArrivalCallsign;
		private System.Windows.Forms.Label labelArrivalCallsign;
		private System.Windows.Forms.Label labelArrivalStandardTime;
		private System.Windows.Forms.Label labelArrivalOrigin;
		private System.Windows.Forms.Label labelArrivalVia;
		private System.Windows.Forms.Label labelArrivalAircraftCode;
		private System.Windows.Forms.ComboBox comboBoxArrivalOrigin;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalStandardDate;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalStandardTime;
		private System.Windows.Forms.ComboBox comboBoxArrivalVia;
		private System.Windows.Forms.ComboBox comboBoxArrivalAircraftCode;
		private System.Windows.Forms.Label labelArrivalFlightNature;
		private System.Windows.Forms.ComboBox comboBoxArrivalFlightNature;
		private System.Windows.Forms.ComboBox comboBoxArrivalServiceType;
		private System.Windows.Forms.Label labelArrivalServiceType;
		private System.Windows.Forms.ComboBox comboBoxArrivalFlightIdentification;
		private System.Windows.Forms.Label labelArrivalFlightIdentification;
		private System.Data.DataSet dataSetFlightRecords;
		private System.Data.DataRow dataRowCurrentRotation;
		private System.Windows.Forms.Label labelArrival;
		private System.Windows.Forms.Label labelDeparture;

		private DataRow[] dataRowArrivalParkingStand;
		private DataRow[] dataRowArrivalBaggageBelts;
		private DataRow[] dataRowArrivalGates;
		private DataRow[] dataRowDepartureParkingStand;
		private DataRow[] dataRowDepartureWaitingRooms;
		private DataRow[] dataRowDepartureGates;
		private DataRow[] dataRowDepartureCounters;
		private DataRow dataRowArrival;
		private DataRow dataRowDeparture;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private bool delete = false;
		private bool init = false;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalParkingStandBeginTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalParkingStandBeginDate;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalParkingStandEndTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalParkingStandEndDate;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalBaggageBeltFirstEndTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalBaggageBeltFirstEndDate;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalBaggageBeltFirstBeginTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalBaggageBeltFirstBeginDate;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalBaggageBeltSecondEndTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalBaggageBeltSecondEndDate;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalBaggageBeltSecondBeginTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalBaggageBeltSecondBeginDate;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalGateSecondEndTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalGateSecondEndDate;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalGateSecondBeginTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalGateSecondBeginDate;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalGateFirstEndTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalGateFirstEndDate;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalGateFirstBeginTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerArrivalGateFirstBeginDate;
		private System.Windows.Forms.ComboBox comboBoxArrivalParkingStand;
		private System.Windows.Forms.ComboBox comboBoxArrivalBaggageBeltFirst;
		private System.Windows.Forms.ComboBox comboBoxArrivalBaggageBeltSecond;
		private System.Windows.Forms.ComboBox comboBoxArrivalGateFirst;
		private System.Windows.Forms.ComboBox comboBoxArrivalGateSecond;
		private System.Windows.Forms.ComboBox comboBoxDepartureGateSecond;
		private System.Windows.Forms.ComboBox comboBoxDepartureGateFirst;
		private System.Windows.Forms.ComboBox comboBoxDepartureWaitingRoomSecond;
		private System.Windows.Forms.ComboBox comboBoxDepartureWaitingRoomFirst;
		private System.Windows.Forms.DataGrid dataGridDepartureCounters;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureGateSecondEndTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureGateSecondEndDate;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureGateSecondBeginTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureGateSecondBeginDate;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureGateFirstEndTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureGateFirstEndDate;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureGateFirstBeginTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureGateFirstBeginDate;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureWaitingRoomSecondEndTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureWaitingRoomSecondEndDate;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureWaitingRoomSecondBeginTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureWaitingRoomSecondBeginDate;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureWaitingRoomFirstEndTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureWaitingRoomFirstEndDate;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureWaitingRoomFirstBeginTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureWaitingRoomFirstBeginDate;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureParkingStandEndTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureParkingStandEndDate;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureParkingStandBeginTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureParkingStandBeginDate;
		private System.Windows.Forms.ComboBox comboBoxDepartureFlightIdentification;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.ComboBox comboBoxDepartureServiceType;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.ComboBox comboBoxDepartureFlightNature;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.ComboBox comboBoxDepartureAircraftCode;
		private System.Windows.Forms.ComboBox comboBoxDepartureDestination;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureStandardTime;
		private System.Windows.Forms.DateTimePicker dateTimePickerDepartureStandardDate;
		private System.Windows.Forms.ComboBox comboBoxDepartureVia;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.Label labelDepartureCallSign;
		private System.Windows.Forms.TextBox textBoxDepartureCallsign;
		private System.Windows.Forms.TextBox textBoxDepartureFlightNumber;
		private System.Windows.Forms.Label labelDepartureFlightNumber;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.ComboBox comboBoxDepartureParkingStand;
		private System.Windows.Forms.Panel panelDeparture;
		private System.Windows.Forms.GroupBox groupBoxDepartureParkingStand;
		private System.Windows.Forms.GroupBox groupBoxDepartureWaitingRooms;
		private System.Windows.Forms.GroupBox groupBoxDepartureGates;
		private System.Windows.Forms.GroupBox groupBoxArrivalParkingStand;
		private System.Windows.Forms.GroupBox groupBoxArrivalBaggageBelts;
		private System.Windows.Forms.GroupBox groupBoxArrivalGates;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.Button buttonClear;
		private System.Windows.Forms.Button buttonApply;
		
	

		public RecordDetail()
		{
			InitializeComponent();
		}



		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.labelArrivalFlightNumber = new System.Windows.Forms.Label();
			this.labelArrivalAircraftCode = new System.Windows.Forms.Label();
			this.buttonCancel = new System.Windows.Forms.Button();
			this.errorProvider1 = new System.Windows.Forms.ErrorProvider();
			this.labelArrival = new System.Windows.Forms.Label();
			this.labelDeparture = new System.Windows.Forms.Label();
			this.panelArrival = new System.Windows.Forms.Panel();
			this.groupBoxArrivalGates = new System.Windows.Forms.GroupBox();
			this.dateTimePickerArrivalGateFirstBeginDate = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerArrivalGateSecondBeginDate = new System.Windows.Forms.DateTimePicker();
			this.comboBoxArrivalGateFirst = new System.Windows.Forms.ComboBox();
			this.dateTimePickerArrivalGateSecondEndTime = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerArrivalGateSecondEndDate = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerArrivalGateSecondBeginTime = new System.Windows.Forms.DateTimePicker();
			this.comboBoxArrivalGateSecond = new System.Windows.Forms.ComboBox();
			this.dateTimePickerArrivalGateFirstEndTime = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerArrivalGateFirstEndDate = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerArrivalGateFirstBeginTime = new System.Windows.Forms.DateTimePicker();
			this.groupBoxArrivalBaggageBelts = new System.Windows.Forms.GroupBox();
			this.comboBoxArrivalBaggageBeltSecond = new System.Windows.Forms.ComboBox();
			this.dateTimePickerArrivalBaggageBeltSecondEndTime = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerArrivalBaggageBeltSecondEndDate = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerArrivalBaggageBeltSecondBeginTime = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerArrivalBaggageBeltSecondBeginDate = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerArrivalBaggageBeltFirstEndTime = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerArrivalBaggageBeltFirstEndDate = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerArrivalBaggageBeltFirstBeginTime = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerArrivalBaggageBeltFirstBeginDate = new System.Windows.Forms.DateTimePicker();
			this.comboBoxArrivalBaggageBeltFirst = new System.Windows.Forms.ComboBox();
			this.groupBoxArrivalParkingStand = new System.Windows.Forms.GroupBox();
			this.comboBoxArrivalParkingStand = new System.Windows.Forms.ComboBox();
			this.dateTimePickerArrivalParkingStandEndTime = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerArrivalParkingStandEndDate = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerArrivalParkingStandBeginTime = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerArrivalParkingStandBeginDate = new System.Windows.Forms.DateTimePicker();
			this.comboBoxArrivalFlightIdentification = new System.Windows.Forms.ComboBox();
			this.labelArrivalFlightIdentification = new System.Windows.Forms.Label();
			this.comboBoxArrivalServiceType = new System.Windows.Forms.ComboBox();
			this.labelArrivalServiceType = new System.Windows.Forms.Label();
			this.comboBoxArrivalFlightNature = new System.Windows.Forms.ComboBox();
			this.labelArrivalFlightNature = new System.Windows.Forms.Label();
			this.comboBoxArrivalAircraftCode = new System.Windows.Forms.ComboBox();
			this.comboBoxArrivalVia = new System.Windows.Forms.ComboBox();
			this.dateTimePickerArrivalStandardTime = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerArrivalStandardDate = new System.Windows.Forms.DateTimePicker();
			this.comboBoxArrivalOrigin = new System.Windows.Forms.ComboBox();
			this.labelArrivalVia = new System.Windows.Forms.Label();
			this.labelArrivalOrigin = new System.Windows.Forms.Label();
			this.labelArrivalStandardTime = new System.Windows.Forms.Label();
			this.labelArrivalCallsign = new System.Windows.Forms.Label();
			this.textBoxArrivalCallsign = new System.Windows.Forms.TextBox();
			this.textBoxArrivalFlightNumber = new System.Windows.Forms.TextBox();
			this.buttonClear = new System.Windows.Forms.Button();
			this.buttonApply = new System.Windows.Forms.Button();
			this.comboBoxDepartureGateSecond = new System.Windows.Forms.ComboBox();
			this.comboBoxDepartureGateFirst = new System.Windows.Forms.ComboBox();
			this.comboBoxDepartureWaitingRoomSecond = new System.Windows.Forms.ComboBox();
			this.comboBoxDepartureWaitingRoomFirst = new System.Windows.Forms.ComboBox();
			this.dataGridDepartureCounters = new System.Windows.Forms.DataGrid();
			this.dateTimePickerDepartureGateSecondEndTime = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerDepartureGateSecondEndDate = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerDepartureGateSecondBeginTime = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerDepartureGateSecondBeginDate = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerDepartureGateFirstEndTime = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerDepartureGateFirstEndDate = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerDepartureGateFirstBeginTime = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerDepartureGateFirstBeginDate = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerDepartureWaitingRoomSecondEndTime = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerDepartureWaitingRoomSecondEndDate = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerDepartureWaitingRoomSecondBeginTime = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerDepartureWaitingRoomSecondBeginDate = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerDepartureWaitingRoomFirstEndTime = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerDepartureWaitingRoomFirstEndDate = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerDepartureWaitingRoomFirstBeginTime = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerDepartureWaitingRoomFirstBeginDate = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerDepartureParkingStandEndTime = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerDepartureParkingStandEndDate = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerDepartureParkingStandBeginTime = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerDepartureParkingStandBeginDate = new System.Windows.Forms.DateTimePicker();
			this.comboBoxDepartureFlightIdentification = new System.Windows.Forms.ComboBox();
			this.label26 = new System.Windows.Forms.Label();
			this.comboBoxDepartureServiceType = new System.Windows.Forms.ComboBox();
			this.label27 = new System.Windows.Forms.Label();
			this.comboBoxDepartureFlightNature = new System.Windows.Forms.ComboBox();
			this.label28 = new System.Windows.Forms.Label();
			this.comboBoxDepartureAircraftCode = new System.Windows.Forms.ComboBox();
			this.comboBoxDepartureDestination = new System.Windows.Forms.ComboBox();
			this.dateTimePickerDepartureStandardTime = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerDepartureStandardDate = new System.Windows.Forms.DateTimePicker();
			this.comboBoxDepartureVia = new System.Windows.Forms.ComboBox();
			this.label29 = new System.Windows.Forms.Label();
			this.label30 = new System.Windows.Forms.Label();
			this.label31 = new System.Windows.Forms.Label();
			this.labelDepartureCallSign = new System.Windows.Forms.Label();
			this.textBoxDepartureCallsign = new System.Windows.Forms.TextBox();
			this.textBoxDepartureFlightNumber = new System.Windows.Forms.TextBox();
			this.labelDepartureFlightNumber = new System.Windows.Forms.Label();
			this.label34 = new System.Windows.Forms.Label();
			this.comboBoxDepartureParkingStand = new System.Windows.Forms.ComboBox();
			this.panelDeparture = new System.Windows.Forms.Panel();
			this.groupBoxDepartureGates = new System.Windows.Forms.GroupBox();
			this.groupBoxDepartureWaitingRooms = new System.Windows.Forms.GroupBox();
			this.groupBoxDepartureParkingStand = new System.Windows.Forms.GroupBox();
			this.panelArrival.SuspendLayout();
			this.groupBoxArrivalGates.SuspendLayout();
			this.groupBoxArrivalBaggageBelts.SuspendLayout();
			this.groupBoxArrivalParkingStand.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridDepartureCounters)).BeginInit();
			this.panelDeparture.SuspendLayout();
			this.groupBoxDepartureGates.SuspendLayout();
			this.groupBoxDepartureWaitingRooms.SuspendLayout();
			this.groupBoxDepartureParkingStand.SuspendLayout();
			this.SuspendLayout();
			// 
			// labelArrivalFlightNumber
			// 
			this.labelArrivalFlightNumber.Location = new System.Drawing.Point(8, 8);
			this.labelArrivalFlightNumber.Name = "labelArrivalFlightNumber";
			this.labelArrivalFlightNumber.Size = new System.Drawing.Size(76, 16);
			this.labelArrivalFlightNumber.TabIndex = 3;
			this.labelArrivalFlightNumber.Text = "FlightNumber";
			// 
			// labelArrivalAircraftCode
			// 
			this.labelArrivalAircraftCode.Location = new System.Drawing.Point(8, 88);
			this.labelArrivalAircraftCode.Name = "labelArrivalAircraftCode";
			this.labelArrivalAircraftCode.Size = new System.Drawing.Size(76, 16);
			this.labelArrivalAircraftCode.TabIndex = 4;
			this.labelArrivalAircraftCode.Text = "Aircraft";
			// 
			// buttonCancel
			// 
			this.buttonCancel.Location = new System.Drawing.Point(252, 428);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.TabIndex = 12;
			this.buttonCancel.Text = "Cancel";
			this.buttonCancel.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// errorProvider1
			// 
			this.errorProvider1.ContainerControl = this;
			// 
			// labelArrival
			// 
			this.labelArrival.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelArrival.Location = new System.Drawing.Point(12, 8);
			this.labelArrival.Name = "labelArrival";
			this.labelArrival.Size = new System.Drawing.Size(100, 16);
			this.labelArrival.TabIndex = 13;
			this.labelArrival.Text = "Arrival";
			// 
			// labelDeparture
			// 
			this.labelDeparture.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelDeparture.Location = new System.Drawing.Point(436, 8);
			this.labelDeparture.Name = "labelDeparture";
			this.labelDeparture.Size = new System.Drawing.Size(100, 16);
			this.labelDeparture.TabIndex = 14;
			this.labelDeparture.Text = "Departure";
			// 
			// panelArrival
			// 
			this.panelArrival.Controls.Add(this.groupBoxArrivalGates);
			this.panelArrival.Controls.Add(this.groupBoxArrivalBaggageBelts);
			this.panelArrival.Controls.Add(this.groupBoxArrivalParkingStand);
			this.panelArrival.Controls.Add(this.comboBoxArrivalFlightIdentification);
			this.panelArrival.Controls.Add(this.labelArrivalFlightIdentification);
			this.panelArrival.Controls.Add(this.comboBoxArrivalServiceType);
			this.panelArrival.Controls.Add(this.labelArrivalServiceType);
			this.panelArrival.Controls.Add(this.comboBoxArrivalFlightNature);
			this.panelArrival.Controls.Add(this.labelArrivalFlightNature);
			this.panelArrival.Controls.Add(this.comboBoxArrivalAircraftCode);
			this.panelArrival.Controls.Add(this.comboBoxArrivalVia);
			this.panelArrival.Controls.Add(this.dateTimePickerArrivalStandardTime);
			this.panelArrival.Controls.Add(this.dateTimePickerArrivalStandardDate);
			this.panelArrival.Controls.Add(this.comboBoxArrivalOrigin);
			this.panelArrival.Controls.Add(this.labelArrivalVia);
			this.panelArrival.Controls.Add(this.labelArrivalOrigin);
			this.panelArrival.Controls.Add(this.labelArrivalStandardTime);
			this.panelArrival.Controls.Add(this.labelArrivalCallsign);
			this.panelArrival.Controls.Add(this.textBoxArrivalCallsign);
			this.panelArrival.Controls.Add(this.textBoxArrivalFlightNumber);
			this.panelArrival.Controls.Add(this.labelArrivalFlightNumber);
			this.panelArrival.Controls.Add(this.labelArrivalAircraftCode);
			this.panelArrival.Location = new System.Drawing.Point(4, 28);
			this.panelArrival.Name = "panelArrival";
			this.panelArrival.Size = new System.Drawing.Size(408, 336);
			this.panelArrival.TabIndex = 16;
			// 
			// groupBoxArrivalGates
			// 
			this.groupBoxArrivalGates.Controls.Add(this.dateTimePickerArrivalGateFirstBeginDate);
			this.groupBoxArrivalGates.Controls.Add(this.dateTimePickerArrivalGateSecondBeginDate);
			this.groupBoxArrivalGates.Controls.Add(this.comboBoxArrivalGateFirst);
			this.groupBoxArrivalGates.Controls.Add(this.dateTimePickerArrivalGateSecondEndTime);
			this.groupBoxArrivalGates.Controls.Add(this.dateTimePickerArrivalGateSecondEndDate);
			this.groupBoxArrivalGates.Controls.Add(this.dateTimePickerArrivalGateSecondBeginTime);
			this.groupBoxArrivalGates.Controls.Add(this.comboBoxArrivalGateSecond);
			this.groupBoxArrivalGates.Controls.Add(this.dateTimePickerArrivalGateFirstEndTime);
			this.groupBoxArrivalGates.Controls.Add(this.dateTimePickerArrivalGateFirstEndDate);
			this.groupBoxArrivalGates.Controls.Add(this.dateTimePickerArrivalGateFirstBeginTime);
			this.groupBoxArrivalGates.Location = new System.Drawing.Point(8, 264);
			this.groupBoxArrivalGates.Name = "groupBoxArrivalGates";
			this.groupBoxArrivalGates.Size = new System.Drawing.Size(396, 68);
			this.groupBoxArrivalGates.TabIndex = 65;
			this.groupBoxArrivalGates.TabStop = false;
			this.groupBoxArrivalGates.Text = "Gates";
			// 
			// dateTimePickerArrivalGateFirstBeginDate
			// 
			this.dateTimePickerArrivalGateFirstBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalGateFirstBeginDate.Location = new System.Drawing.Point(100, 16);
			this.dateTimePickerArrivalGateFirstBeginDate.Name = "dateTimePickerArrivalGateFirstBeginDate";
			this.dateTimePickerArrivalGateFirstBeginDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerArrivalGateFirstBeginDate.TabIndex = 48;
			this.dateTimePickerArrivalGateFirstBeginDate.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalGateFirstBeginDate_ValueChanged);
			// 
			// dateTimePickerArrivalGateSecondBeginDate
			// 
			this.dateTimePickerArrivalGateSecondBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalGateSecondBeginDate.Location = new System.Drawing.Point(100, 40);
			this.dateTimePickerArrivalGateSecondBeginDate.Name = "dateTimePickerArrivalGateSecondBeginDate";
			this.dateTimePickerArrivalGateSecondBeginDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerArrivalGateSecondBeginDate.TabIndex = 54;
			this.dateTimePickerArrivalGateSecondBeginDate.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalGateSecondBeginDate_ValueChanged);
			// 
			// comboBoxArrivalGateFirst
			// 
			this.comboBoxArrivalGateFirst.Location = new System.Drawing.Point(8, 16);
			this.comboBoxArrivalGateFirst.Name = "comboBoxArrivalGateFirst";
			this.comboBoxArrivalGateFirst.Size = new System.Drawing.Size(72, 21);
			this.comboBoxArrivalGateFirst.TabIndex = 61;
			this.comboBoxArrivalGateFirst.SelectedValueChanged += new System.EventHandler(this.comboBoxArrivalGateFirst_SelectedValueChanged);
			// 
			// dateTimePickerArrivalGateSecondEndTime
			// 
			this.dateTimePickerArrivalGateSecondEndTime.CustomFormat = "HH:mm";
			this.dateTimePickerArrivalGateSecondEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalGateSecondEndTime.Location = new System.Drawing.Point(332, 40);
			this.dateTimePickerArrivalGateSecondEndTime.Name = "dateTimePickerArrivalGateSecondEndTime";
			this.dateTimePickerArrivalGateSecondEndTime.ShowUpDown = true;
			this.dateTimePickerArrivalGateSecondEndTime.Size = new System.Drawing.Size(52, 20);
			this.dateTimePickerArrivalGateSecondEndTime.TabIndex = 57;
			this.dateTimePickerArrivalGateSecondEndTime.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalGateSecondEndTime_ValueChanged);
			// 
			// dateTimePickerArrivalGateSecondEndDate
			// 
			this.dateTimePickerArrivalGateSecondEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalGateSecondEndDate.Location = new System.Drawing.Point(252, 40);
			this.dateTimePickerArrivalGateSecondEndDate.Name = "dateTimePickerArrivalGateSecondEndDate";
			this.dateTimePickerArrivalGateSecondEndDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerArrivalGateSecondEndDate.TabIndex = 56;
			this.dateTimePickerArrivalGateSecondEndDate.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalGateSecondEndDate_ValueChanged);
			// 
			// dateTimePickerArrivalGateSecondBeginTime
			// 
			this.dateTimePickerArrivalGateSecondBeginTime.CustomFormat = "HH:mm";
			this.dateTimePickerArrivalGateSecondBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalGateSecondBeginTime.Location = new System.Drawing.Point(180, 40);
			this.dateTimePickerArrivalGateSecondBeginTime.Name = "dateTimePickerArrivalGateSecondBeginTime";
			this.dateTimePickerArrivalGateSecondBeginTime.ShowUpDown = true;
			this.dateTimePickerArrivalGateSecondBeginTime.Size = new System.Drawing.Size(52, 20);
			this.dateTimePickerArrivalGateSecondBeginTime.TabIndex = 55;
			this.dateTimePickerArrivalGateSecondBeginTime.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalGateSecondBeginTime_ValueChanged);
			// 
			// comboBoxArrivalGateSecond
			// 
			this.comboBoxArrivalGateSecond.Location = new System.Drawing.Point(8, 40);
			this.comboBoxArrivalGateSecond.Name = "comboBoxArrivalGateSecond";
			this.comboBoxArrivalGateSecond.Size = new System.Drawing.Size(72, 21);
			this.comboBoxArrivalGateSecond.TabIndex = 62;
			this.comboBoxArrivalGateSecond.SelectedValueChanged += new System.EventHandler(this.comboBoxArrivalGateSecond_SelectedValueChanged);
			// 
			// dateTimePickerArrivalGateFirstEndTime
			// 
			this.dateTimePickerArrivalGateFirstEndTime.CustomFormat = "HH:mm";
			this.dateTimePickerArrivalGateFirstEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalGateFirstEndTime.Location = new System.Drawing.Point(332, 16);
			this.dateTimePickerArrivalGateFirstEndTime.Name = "dateTimePickerArrivalGateFirstEndTime";
			this.dateTimePickerArrivalGateFirstEndTime.ShowUpDown = true;
			this.dateTimePickerArrivalGateFirstEndTime.Size = new System.Drawing.Size(52, 20);
			this.dateTimePickerArrivalGateFirstEndTime.TabIndex = 51;
			this.dateTimePickerArrivalGateFirstEndTime.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalGateFirstEndTime_ValueChanged);
			// 
			// dateTimePickerArrivalGateFirstEndDate
			// 
			this.dateTimePickerArrivalGateFirstEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalGateFirstEndDate.Location = new System.Drawing.Point(252, 16);
			this.dateTimePickerArrivalGateFirstEndDate.Name = "dateTimePickerArrivalGateFirstEndDate";
			this.dateTimePickerArrivalGateFirstEndDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerArrivalGateFirstEndDate.TabIndex = 50;
			this.dateTimePickerArrivalGateFirstEndDate.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalGateFirstEndDate_ValueChanged);
			// 
			// dateTimePickerArrivalGateFirstBeginTime
			// 
			this.dateTimePickerArrivalGateFirstBeginTime.CustomFormat = "HH:mm";
			this.dateTimePickerArrivalGateFirstBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalGateFirstBeginTime.Location = new System.Drawing.Point(180, 16);
			this.dateTimePickerArrivalGateFirstBeginTime.Name = "dateTimePickerArrivalGateFirstBeginTime";
			this.dateTimePickerArrivalGateFirstBeginTime.ShowUpDown = true;
			this.dateTimePickerArrivalGateFirstBeginTime.Size = new System.Drawing.Size(52, 20);
			this.dateTimePickerArrivalGateFirstBeginTime.TabIndex = 49;
			this.dateTimePickerArrivalGateFirstBeginTime.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalGateFirstBeginTime_ValueChanged);
			// 
			// groupBoxArrivalBaggageBelts
			// 
			this.groupBoxArrivalBaggageBelts.Controls.Add(this.comboBoxArrivalBaggageBeltSecond);
			this.groupBoxArrivalBaggageBelts.Controls.Add(this.dateTimePickerArrivalBaggageBeltSecondEndTime);
			this.groupBoxArrivalBaggageBelts.Controls.Add(this.dateTimePickerArrivalBaggageBeltSecondEndDate);
			this.groupBoxArrivalBaggageBelts.Controls.Add(this.dateTimePickerArrivalBaggageBeltSecondBeginTime);
			this.groupBoxArrivalBaggageBelts.Controls.Add(this.dateTimePickerArrivalBaggageBeltSecondBeginDate);
			this.groupBoxArrivalBaggageBelts.Controls.Add(this.dateTimePickerArrivalBaggageBeltFirstEndTime);
			this.groupBoxArrivalBaggageBelts.Controls.Add(this.dateTimePickerArrivalBaggageBeltFirstEndDate);
			this.groupBoxArrivalBaggageBelts.Controls.Add(this.dateTimePickerArrivalBaggageBeltFirstBeginTime);
			this.groupBoxArrivalBaggageBelts.Controls.Add(this.dateTimePickerArrivalBaggageBeltFirstBeginDate);
			this.groupBoxArrivalBaggageBelts.Controls.Add(this.comboBoxArrivalBaggageBeltFirst);
			this.groupBoxArrivalBaggageBelts.Location = new System.Drawing.Point(8, 188);
			this.groupBoxArrivalBaggageBelts.Name = "groupBoxArrivalBaggageBelts";
			this.groupBoxArrivalBaggageBelts.Size = new System.Drawing.Size(396, 68);
			this.groupBoxArrivalBaggageBelts.TabIndex = 64;
			this.groupBoxArrivalBaggageBelts.TabStop = false;
			this.groupBoxArrivalBaggageBelts.Text = "Baggage Belts";
			// 
			// comboBoxArrivalBaggageBeltSecond
			// 
			this.comboBoxArrivalBaggageBeltSecond.Location = new System.Drawing.Point(8, 40);
			this.comboBoxArrivalBaggageBeltSecond.Name = "comboBoxArrivalBaggageBeltSecond";
			this.comboBoxArrivalBaggageBeltSecond.Size = new System.Drawing.Size(72, 21);
			this.comboBoxArrivalBaggageBeltSecond.TabIndex = 60;
			this.comboBoxArrivalBaggageBeltSecond.SelectedValueChanged += new System.EventHandler(this.comboBoxArrivalBaggageBeltSecond_SelectedValueChanged);
			// 
			// dateTimePickerArrivalBaggageBeltSecondEndTime
			// 
			this.dateTimePickerArrivalBaggageBeltSecondEndTime.CustomFormat = "HH:mm";
			this.dateTimePickerArrivalBaggageBeltSecondEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalBaggageBeltSecondEndTime.Location = new System.Drawing.Point(332, 40);
			this.dateTimePickerArrivalBaggageBeltSecondEndTime.Name = "dateTimePickerArrivalBaggageBeltSecondEndTime";
			this.dateTimePickerArrivalBaggageBeltSecondEndTime.ShowUpDown = true;
			this.dateTimePickerArrivalBaggageBeltSecondEndTime.Size = new System.Drawing.Size(52, 20);
			this.dateTimePickerArrivalBaggageBeltSecondEndTime.TabIndex = 45;
			this.dateTimePickerArrivalBaggageBeltSecondEndTime.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalBaggageBeltSecondEndTime_ValueChanged);
			// 
			// dateTimePickerArrivalBaggageBeltSecondEndDate
			// 
			this.dateTimePickerArrivalBaggageBeltSecondEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalBaggageBeltSecondEndDate.Location = new System.Drawing.Point(252, 40);
			this.dateTimePickerArrivalBaggageBeltSecondEndDate.Name = "dateTimePickerArrivalBaggageBeltSecondEndDate";
			this.dateTimePickerArrivalBaggageBeltSecondEndDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerArrivalBaggageBeltSecondEndDate.TabIndex = 44;
			this.dateTimePickerArrivalBaggageBeltSecondEndDate.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalBaggageBeltSecondEndDate_ValueChanged);
			// 
			// dateTimePickerArrivalBaggageBeltSecondBeginTime
			// 
			this.dateTimePickerArrivalBaggageBeltSecondBeginTime.CustomFormat = "HH:mm";
			this.dateTimePickerArrivalBaggageBeltSecondBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalBaggageBeltSecondBeginTime.Location = new System.Drawing.Point(180, 40);
			this.dateTimePickerArrivalBaggageBeltSecondBeginTime.Name = "dateTimePickerArrivalBaggageBeltSecondBeginTime";
			this.dateTimePickerArrivalBaggageBeltSecondBeginTime.ShowUpDown = true;
			this.dateTimePickerArrivalBaggageBeltSecondBeginTime.Size = new System.Drawing.Size(52, 20);
			this.dateTimePickerArrivalBaggageBeltSecondBeginTime.TabIndex = 43;
			this.dateTimePickerArrivalBaggageBeltSecondBeginTime.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalBaggageBeltSecondBeginTime_ValueChanged);
			// 
			// dateTimePickerArrivalBaggageBeltSecondBeginDate
			// 
			this.dateTimePickerArrivalBaggageBeltSecondBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalBaggageBeltSecondBeginDate.Location = new System.Drawing.Point(100, 40);
			this.dateTimePickerArrivalBaggageBeltSecondBeginDate.Name = "dateTimePickerArrivalBaggageBeltSecondBeginDate";
			this.dateTimePickerArrivalBaggageBeltSecondBeginDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerArrivalBaggageBeltSecondBeginDate.TabIndex = 42;
			this.dateTimePickerArrivalBaggageBeltSecondBeginDate.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalBaggageBeltSecondBeginDate_ValueChanged);
			// 
			// dateTimePickerArrivalBaggageBeltFirstEndTime
			// 
			this.dateTimePickerArrivalBaggageBeltFirstEndTime.CustomFormat = "HH:mm";
			this.dateTimePickerArrivalBaggageBeltFirstEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalBaggageBeltFirstEndTime.Location = new System.Drawing.Point(332, 16);
			this.dateTimePickerArrivalBaggageBeltFirstEndTime.Name = "dateTimePickerArrivalBaggageBeltFirstEndTime";
			this.dateTimePickerArrivalBaggageBeltFirstEndTime.ShowUpDown = true;
			this.dateTimePickerArrivalBaggageBeltFirstEndTime.Size = new System.Drawing.Size(52, 20);
			this.dateTimePickerArrivalBaggageBeltFirstEndTime.TabIndex = 39;
			this.dateTimePickerArrivalBaggageBeltFirstEndTime.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalBaggageBeltFirstEndTime_ValueChanged);
			// 
			// dateTimePickerArrivalBaggageBeltFirstEndDate
			// 
			this.dateTimePickerArrivalBaggageBeltFirstEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalBaggageBeltFirstEndDate.Location = new System.Drawing.Point(252, 16);
			this.dateTimePickerArrivalBaggageBeltFirstEndDate.Name = "dateTimePickerArrivalBaggageBeltFirstEndDate";
			this.dateTimePickerArrivalBaggageBeltFirstEndDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerArrivalBaggageBeltFirstEndDate.TabIndex = 38;
			this.dateTimePickerArrivalBaggageBeltFirstEndDate.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalBaggageBeltFirstEndDate_ValueChanged);
			// 
			// dateTimePickerArrivalBaggageBeltFirstBeginTime
			// 
			this.dateTimePickerArrivalBaggageBeltFirstBeginTime.CustomFormat = "HH:mm";
			this.dateTimePickerArrivalBaggageBeltFirstBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalBaggageBeltFirstBeginTime.Location = new System.Drawing.Point(180, 16);
			this.dateTimePickerArrivalBaggageBeltFirstBeginTime.Name = "dateTimePickerArrivalBaggageBeltFirstBeginTime";
			this.dateTimePickerArrivalBaggageBeltFirstBeginTime.ShowUpDown = true;
			this.dateTimePickerArrivalBaggageBeltFirstBeginTime.Size = new System.Drawing.Size(52, 20);
			this.dateTimePickerArrivalBaggageBeltFirstBeginTime.TabIndex = 37;
			this.dateTimePickerArrivalBaggageBeltFirstBeginTime.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalBaggageBeltFirstBeginTime_ValueChanged);
			// 
			// dateTimePickerArrivalBaggageBeltFirstBeginDate
			// 
			this.dateTimePickerArrivalBaggageBeltFirstBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalBaggageBeltFirstBeginDate.Location = new System.Drawing.Point(100, 16);
			this.dateTimePickerArrivalBaggageBeltFirstBeginDate.Name = "dateTimePickerArrivalBaggageBeltFirstBeginDate";
			this.dateTimePickerArrivalBaggageBeltFirstBeginDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerArrivalBaggageBeltFirstBeginDate.TabIndex = 36;
			this.dateTimePickerArrivalBaggageBeltFirstBeginDate.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalBaggageBeltFirstBeginDate_ValueChanged);
			// 
			// comboBoxArrivalBaggageBeltFirst
			// 
			this.comboBoxArrivalBaggageBeltFirst.Location = new System.Drawing.Point(8, 16);
			this.comboBoxArrivalBaggageBeltFirst.Name = "comboBoxArrivalBaggageBeltFirst";
			this.comboBoxArrivalBaggageBeltFirst.Size = new System.Drawing.Size(72, 21);
			this.comboBoxArrivalBaggageBeltFirst.TabIndex = 59;
			this.comboBoxArrivalBaggageBeltFirst.SelectedValueChanged += new System.EventHandler(this.comboBoxArrivalBaggageBeltFirst_SelectedValueChanged);
			// 
			// groupBoxArrivalParkingStand
			// 
			this.groupBoxArrivalParkingStand.Controls.Add(this.comboBoxArrivalParkingStand);
			this.groupBoxArrivalParkingStand.Controls.Add(this.dateTimePickerArrivalParkingStandEndTime);
			this.groupBoxArrivalParkingStand.Controls.Add(this.dateTimePickerArrivalParkingStandEndDate);
			this.groupBoxArrivalParkingStand.Controls.Add(this.dateTimePickerArrivalParkingStandBeginTime);
			this.groupBoxArrivalParkingStand.Controls.Add(this.dateTimePickerArrivalParkingStandBeginDate);
			this.groupBoxArrivalParkingStand.Location = new System.Drawing.Point(8, 136);
			this.groupBoxArrivalParkingStand.Name = "groupBoxArrivalParkingStand";
			this.groupBoxArrivalParkingStand.Size = new System.Drawing.Size(396, 44);
			this.groupBoxArrivalParkingStand.TabIndex = 63;
			this.groupBoxArrivalParkingStand.TabStop = false;
			this.groupBoxArrivalParkingStand.Text = "Parking Stand";
			// 
			// comboBoxArrivalParkingStand
			// 
			this.comboBoxArrivalParkingStand.Location = new System.Drawing.Point(8, 16);
			this.comboBoxArrivalParkingStand.Name = "comboBoxArrivalParkingStand";
			this.comboBoxArrivalParkingStand.Size = new System.Drawing.Size(72, 21);
			this.comboBoxArrivalParkingStand.TabIndex = 58;
			this.comboBoxArrivalParkingStand.SelectedValueChanged += new System.EventHandler(this.comboBoxArrivalParkingStand_SelectedValueChanged);
			// 
			// dateTimePickerArrivalParkingStandEndTime
			// 
			this.dateTimePickerArrivalParkingStandEndTime.CustomFormat = "HH:mm";
			this.dateTimePickerArrivalParkingStandEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalParkingStandEndTime.Location = new System.Drawing.Point(332, 16);
			this.dateTimePickerArrivalParkingStandEndTime.Name = "dateTimePickerArrivalParkingStandEndTime";
			this.dateTimePickerArrivalParkingStandEndTime.ShowUpDown = true;
			this.dateTimePickerArrivalParkingStandEndTime.Size = new System.Drawing.Size(52, 20);
			this.dateTimePickerArrivalParkingStandEndTime.TabIndex = 33;
			this.dateTimePickerArrivalParkingStandEndTime.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalParkingStandEndTime_ValueChanged);
			// 
			// dateTimePickerArrivalParkingStandEndDate
			// 
			this.dateTimePickerArrivalParkingStandEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalParkingStandEndDate.Location = new System.Drawing.Point(252, 16);
			this.dateTimePickerArrivalParkingStandEndDate.Name = "dateTimePickerArrivalParkingStandEndDate";
			this.dateTimePickerArrivalParkingStandEndDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerArrivalParkingStandEndDate.TabIndex = 32;
			this.dateTimePickerArrivalParkingStandEndDate.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalParkingStandEndDate_ValueChanged);
			// 
			// dateTimePickerArrivalParkingStandBeginTime
			// 
			this.dateTimePickerArrivalParkingStandBeginTime.CustomFormat = "HH:mm";
			this.dateTimePickerArrivalParkingStandBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalParkingStandBeginTime.Location = new System.Drawing.Point(180, 16);
			this.dateTimePickerArrivalParkingStandBeginTime.Name = "dateTimePickerArrivalParkingStandBeginTime";
			this.dateTimePickerArrivalParkingStandBeginTime.ShowUpDown = true;
			this.dateTimePickerArrivalParkingStandBeginTime.Size = new System.Drawing.Size(52, 20);
			this.dateTimePickerArrivalParkingStandBeginTime.TabIndex = 30;
			this.dateTimePickerArrivalParkingStandBeginTime.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalParkingStandBeginTime_ValueChanged);
			// 
			// dateTimePickerArrivalParkingStandBeginDate
			// 
			this.dateTimePickerArrivalParkingStandBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalParkingStandBeginDate.Location = new System.Drawing.Point(100, 16);
			this.dateTimePickerArrivalParkingStandBeginDate.Name = "dateTimePickerArrivalParkingStandBeginDate";
			this.dateTimePickerArrivalParkingStandBeginDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerArrivalParkingStandBeginDate.TabIndex = 29;
			this.dateTimePickerArrivalParkingStandBeginDate.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalParkingStandBeginDate_ValueChanged);
			// 
			// comboBoxArrivalFlightIdentification
			// 
			this.comboBoxArrivalFlightIdentification.Location = new System.Drawing.Point(284, 108);
			this.comboBoxArrivalFlightIdentification.Name = "comboBoxArrivalFlightIdentification";
			this.comboBoxArrivalFlightIdentification.Size = new System.Drawing.Size(100, 21);
			this.comboBoxArrivalFlightIdentification.TabIndex = 23;
			this.comboBoxArrivalFlightIdentification.SelectedValueChanged += new System.EventHandler(this.comboBoxArrivalFlightIdentification_SelectedValueChanged);
			// 
			// labelArrivalFlightIdentification
			// 
			this.labelArrivalFlightIdentification.Location = new System.Drawing.Point(208, 112);
			this.labelArrivalFlightIdentification.Name = "labelArrivalFlightIdentification";
			this.labelArrivalFlightIdentification.Size = new System.Drawing.Size(76, 16);
			this.labelArrivalFlightIdentification.TabIndex = 22;
			this.labelArrivalFlightIdentification.Text = "Identification";
			// 
			// comboBoxArrivalServiceType
			// 
			this.comboBoxArrivalServiceType.Location = new System.Drawing.Point(284, 84);
			this.comboBoxArrivalServiceType.Name = "comboBoxArrivalServiceType";
			this.comboBoxArrivalServiceType.Size = new System.Drawing.Size(100, 21);
			this.comboBoxArrivalServiceType.TabIndex = 21;
			this.comboBoxArrivalServiceType.SelectedValueChanged += new System.EventHandler(this.comboBoxArrivalServiceType_SelectedValueChanged);
			// 
			// labelArrivalServiceType
			// 
			this.labelArrivalServiceType.Location = new System.Drawing.Point(208, 88);
			this.labelArrivalServiceType.Name = "labelArrivalServiceType";
			this.labelArrivalServiceType.Size = new System.Drawing.Size(76, 16);
			this.labelArrivalServiceType.TabIndex = 20;
			this.labelArrivalServiceType.Text = "Servicetype";
			// 
			// comboBoxArrivalFlightNature
			// 
			this.comboBoxArrivalFlightNature.Location = new System.Drawing.Point(84, 108);
			this.comboBoxArrivalFlightNature.Name = "comboBoxArrivalFlightNature";
			this.comboBoxArrivalFlightNature.Size = new System.Drawing.Size(100, 21);
			this.comboBoxArrivalFlightNature.TabIndex = 19;
			this.comboBoxArrivalFlightNature.SelectedValueChanged += new System.EventHandler(this.comboBoxArrivalFlightNature_SelectedValueChanged);
			// 
			// labelArrivalFlightNature
			// 
			this.labelArrivalFlightNature.Location = new System.Drawing.Point(8, 112);
			this.labelArrivalFlightNature.Name = "labelArrivalFlightNature";
			this.labelArrivalFlightNature.Size = new System.Drawing.Size(76, 16);
			this.labelArrivalFlightNature.TabIndex = 18;
			this.labelArrivalFlightNature.Text = "Nature";
			// 
			// comboBoxArrivalAircraftCode
			// 
			this.comboBoxArrivalAircraftCode.Location = new System.Drawing.Point(84, 84);
			this.comboBoxArrivalAircraftCode.Name = "comboBoxArrivalAircraftCode";
			this.comboBoxArrivalAircraftCode.Size = new System.Drawing.Size(100, 21);
			this.comboBoxArrivalAircraftCode.TabIndex = 17;
			this.comboBoxArrivalAircraftCode.SelectedValueChanged += new System.EventHandler(this.comboBoxArrivalAircraftCode_SelectedValueChanged);
			// 
			// comboBoxArrivalVia
			// 
			this.comboBoxArrivalVia.Location = new System.Drawing.Point(284, 60);
			this.comboBoxArrivalVia.Name = "comboBoxArrivalVia";
			this.comboBoxArrivalVia.Size = new System.Drawing.Size(100, 21);
			this.comboBoxArrivalVia.TabIndex = 16;
			this.comboBoxArrivalVia.SelectedValueChanged += new System.EventHandler(this.comboBoxArrivalVia_SelectedValueChanged);
			// 
			// dateTimePickerArrivalStandardTime
			// 
			this.dateTimePickerArrivalStandardTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
			this.dateTimePickerArrivalStandardTime.Location = new System.Drawing.Point(164, 36);
			this.dateTimePickerArrivalStandardTime.Name = "dateTimePickerArrivalStandardTime";
			this.dateTimePickerArrivalStandardTime.ShowUpDown = true;
			this.dateTimePickerArrivalStandardTime.Size = new System.Drawing.Size(68, 20);
			this.dateTimePickerArrivalStandardTime.TabIndex = 15;
			this.dateTimePickerArrivalStandardTime.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalStandardTime_ValueChanged);
			// 
			// dateTimePickerArrivalStandardDate
			// 
			this.dateTimePickerArrivalStandardDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalStandardDate.Location = new System.Drawing.Point(84, 36);
			this.dateTimePickerArrivalStandardDate.Name = "dateTimePickerArrivalStandardDate";
			this.dateTimePickerArrivalStandardDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerArrivalStandardDate.TabIndex = 14;
			this.dateTimePickerArrivalStandardDate.ValueChanged += new System.EventHandler(this.dateTimePickerArrivalStandardDate_ValueChanged);
			// 
			// comboBoxArrivalOrigin
			// 
			this.comboBoxArrivalOrigin.Location = new System.Drawing.Point(84, 60);
			this.comboBoxArrivalOrigin.Name = "comboBoxArrivalOrigin";
			this.comboBoxArrivalOrigin.Size = new System.Drawing.Size(100, 21);
			this.comboBoxArrivalOrigin.TabIndex = 13;
			this.comboBoxArrivalOrigin.SelectedValueChanged += new System.EventHandler(this.comboBoxArrivalOrigin_SelectedValueChanged);
			// 
			// labelArrivalVia
			// 
			this.labelArrivalVia.Location = new System.Drawing.Point(208, 64);
			this.labelArrivalVia.Name = "labelArrivalVia";
			this.labelArrivalVia.Size = new System.Drawing.Size(76, 16);
			this.labelArrivalVia.TabIndex = 12;
			this.labelArrivalVia.Text = "VIA";
			// 
			// labelArrivalOrigin
			// 
			this.labelArrivalOrigin.Location = new System.Drawing.Point(8, 64);
			this.labelArrivalOrigin.Name = "labelArrivalOrigin";
			this.labelArrivalOrigin.Size = new System.Drawing.Size(76, 16);
			this.labelArrivalOrigin.TabIndex = 10;
			this.labelArrivalOrigin.Text = "ORG";
			// 
			// labelArrivalStandardTime
			// 
			this.labelArrivalStandardTime.Location = new System.Drawing.Point(8, 40);
			this.labelArrivalStandardTime.Name = "labelArrivalStandardTime";
			this.labelArrivalStandardTime.Size = new System.Drawing.Size(76, 16);
			this.labelArrivalStandardTime.TabIndex = 7;
			this.labelArrivalStandardTime.Text = "STA";
			// 
			// labelArrivalCallsign
			// 
			this.labelArrivalCallsign.Location = new System.Drawing.Point(208, 8);
			this.labelArrivalCallsign.Name = "labelArrivalCallsign";
			this.labelArrivalCallsign.Size = new System.Drawing.Size(76, 16);
			this.labelArrivalCallsign.TabIndex = 6;
			this.labelArrivalCallsign.Text = "Callsign";
			// 
			// textBoxArrivalCallsign
			// 
			this.textBoxArrivalCallsign.Location = new System.Drawing.Point(284, 4);
			this.textBoxArrivalCallsign.Name = "textBoxArrivalCallsign";
			this.textBoxArrivalCallsign.TabIndex = 5;
			this.textBoxArrivalCallsign.Text = "";
			this.textBoxArrivalCallsign.TextChanged += new System.EventHandler(this.textBoxArrivalCallsign_TextChanged);
			// 
			// textBoxArrivalFlightNumber
			// 
			this.textBoxArrivalFlightNumber.Location = new System.Drawing.Point(84, 4);
			this.textBoxArrivalFlightNumber.Name = "textBoxArrivalFlightNumber";
			this.textBoxArrivalFlightNumber.TabIndex = 4;
			this.textBoxArrivalFlightNumber.Text = "";
			this.textBoxArrivalFlightNumber.TextChanged += new System.EventHandler(this.textBoxArrivalFlightNumber_TextChanged);
			// 
			// buttonClear
			// 
			this.buttonClear.Location = new System.Drawing.Point(92, 428);
			this.buttonClear.Name = "buttonClear";
			this.buttonClear.TabIndex = 18;
			this.buttonClear.Text = "Clear ";
			this.buttonClear.Click += new System.EventHandler(this.buttonDelete_Click);
			// 
			// buttonApply
			// 
			this.buttonApply.Location = new System.Drawing.Point(172, 428);
			this.buttonApply.Name = "buttonApply";
			this.buttonApply.TabIndex = 20;
			this.buttonApply.Text = "Apply";
			this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
			// 
			// comboBoxDepartureGateSecond
			// 
			this.comboBoxDepartureGateSecond.ItemHeight = 13;
			this.comboBoxDepartureGateSecond.Location = new System.Drawing.Point(8, 40);
			this.comboBoxDepartureGateSecond.Name = "comboBoxDepartureGateSecond";
			this.comboBoxDepartureGateSecond.Size = new System.Drawing.Size(72, 21);
			this.comboBoxDepartureGateSecond.TabIndex = 92;
			this.comboBoxDepartureGateSecond.SelectedValueChanged += new System.EventHandler(this.comboBoxDepartureGateSecond_SelectedValueChanged);
			// 
			// comboBoxDepartureGateFirst
			// 
			this.comboBoxDepartureGateFirst.ItemHeight = 13;
			this.comboBoxDepartureGateFirst.Location = new System.Drawing.Point(8, 16);
			this.comboBoxDepartureGateFirst.Name = "comboBoxDepartureGateFirst";
			this.comboBoxDepartureGateFirst.Size = new System.Drawing.Size(72, 21);
			this.comboBoxDepartureGateFirst.TabIndex = 91;
			this.comboBoxDepartureGateFirst.SelectedValueChanged += new System.EventHandler(this.comboBoxDepartureGateFirst_SelectedValueChanged);
			// 
			// comboBoxDepartureWaitingRoomSecond
			// 
			this.comboBoxDepartureWaitingRoomSecond.ItemHeight = 13;
			this.comboBoxDepartureWaitingRoomSecond.Location = new System.Drawing.Point(8, 40);
			this.comboBoxDepartureWaitingRoomSecond.Name = "comboBoxDepartureWaitingRoomSecond";
			this.comboBoxDepartureWaitingRoomSecond.Size = new System.Drawing.Size(72, 21);
			this.comboBoxDepartureWaitingRoomSecond.TabIndex = 90;
			this.comboBoxDepartureWaitingRoomSecond.SelectedValueChanged += new System.EventHandler(this.comboBoxDepartureWaitingRoomSecond_SelectedValueChanged);
			// 
			// comboBoxDepartureWaitingRoomFirst
			// 
			this.comboBoxDepartureWaitingRoomFirst.ItemHeight = 13;
			this.comboBoxDepartureWaitingRoomFirst.Location = new System.Drawing.Point(8, 16);
			this.comboBoxDepartureWaitingRoomFirst.Name = "comboBoxDepartureWaitingRoomFirst";
			this.comboBoxDepartureWaitingRoomFirst.Size = new System.Drawing.Size(72, 21);
			this.comboBoxDepartureWaitingRoomFirst.TabIndex = 89;
			this.comboBoxDepartureWaitingRoomFirst.SelectedValueChanged += new System.EventHandler(this.comboBoxDepartureWaitingRoomFirst_SelectedValueChanged);
			// 
			// dataGridDepartureCounters
			// 
			this.dataGridDepartureCounters.DataMember = "";
			this.dataGridDepartureCounters.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridDepartureCounters.Location = new System.Drawing.Point(8, 336);
			this.dataGridDepartureCounters.Name = "dataGridDepartureCounters";
			this.dataGridDepartureCounters.Size = new System.Drawing.Size(396, 140);
			this.dataGridDepartureCounters.TabIndex = 88;
			// 
			// dateTimePickerDepartureGateSecondEndTime
			// 
			this.dateTimePickerDepartureGateSecondEndTime.CustomFormat = "HH:mm";
			this.dateTimePickerDepartureGateSecondEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureGateSecondEndTime.Location = new System.Drawing.Point(332, 40);
			this.dateTimePickerDepartureGateSecondEndTime.Name = "dateTimePickerDepartureGateSecondEndTime";
			this.dateTimePickerDepartureGateSecondEndTime.ShowUpDown = true;
			this.dateTimePickerDepartureGateSecondEndTime.Size = new System.Drawing.Size(52, 20);
			this.dateTimePickerDepartureGateSecondEndTime.TabIndex = 87;
			this.dateTimePickerDepartureGateSecondEndTime.Value = new System.DateTime(2004, 11, 11, 9, 42, 59, 890);
			this.dateTimePickerDepartureGateSecondEndTime.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureGateSecondEndTime_ValueChanged);
			// 
			// dateTimePickerDepartureGateSecondEndDate
			// 
			this.dateTimePickerDepartureGateSecondEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureGateSecondEndDate.Location = new System.Drawing.Point(252, 40);
			this.dateTimePickerDepartureGateSecondEndDate.Name = "dateTimePickerDepartureGateSecondEndDate";
			this.dateTimePickerDepartureGateSecondEndDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerDepartureGateSecondEndDate.TabIndex = 86;
			this.dateTimePickerDepartureGateSecondEndDate.Value = new System.DateTime(2004, 11, 11, 9, 42, 59, 906);
			this.dateTimePickerDepartureGateSecondEndDate.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureGateSecondEndDate_ValueChanged);
			// 
			// dateTimePickerDepartureGateSecondBeginTime
			// 
			this.dateTimePickerDepartureGateSecondBeginTime.CustomFormat = "HH:mm";
			this.dateTimePickerDepartureGateSecondBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureGateSecondBeginTime.Location = new System.Drawing.Point(180, 40);
			this.dateTimePickerDepartureGateSecondBeginTime.Name = "dateTimePickerDepartureGateSecondBeginTime";
			this.dateTimePickerDepartureGateSecondBeginTime.ShowUpDown = true;
			this.dateTimePickerDepartureGateSecondBeginTime.Size = new System.Drawing.Size(52, 20);
			this.dateTimePickerDepartureGateSecondBeginTime.TabIndex = 85;
			this.dateTimePickerDepartureGateSecondBeginTime.Value = new System.DateTime(2004, 11, 11, 9, 42, 59, 906);
			this.dateTimePickerDepartureGateSecondBeginTime.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureGateSecondBeginTime_ValueChanged);
			// 
			// dateTimePickerDepartureGateSecondBeginDate
			// 
			this.dateTimePickerDepartureGateSecondBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureGateSecondBeginDate.Location = new System.Drawing.Point(100, 40);
			this.dateTimePickerDepartureGateSecondBeginDate.Name = "dateTimePickerDepartureGateSecondBeginDate";
			this.dateTimePickerDepartureGateSecondBeginDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerDepartureGateSecondBeginDate.TabIndex = 84;
			this.dateTimePickerDepartureGateSecondBeginDate.Value = new System.DateTime(2004, 11, 11, 9, 42, 59, 921);
			this.dateTimePickerDepartureGateSecondBeginDate.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureGateSecondBeginDate_ValueChanged);
			// 
			// dateTimePickerDepartureGateFirstEndTime
			// 
			this.dateTimePickerDepartureGateFirstEndTime.CustomFormat = "HH:mm";
			this.dateTimePickerDepartureGateFirstEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureGateFirstEndTime.Location = new System.Drawing.Point(332, 16);
			this.dateTimePickerDepartureGateFirstEndTime.Name = "dateTimePickerDepartureGateFirstEndTime";
			this.dateTimePickerDepartureGateFirstEndTime.ShowUpDown = true;
			this.dateTimePickerDepartureGateFirstEndTime.Size = new System.Drawing.Size(52, 20);
			this.dateTimePickerDepartureGateFirstEndTime.TabIndex = 81;
			this.dateTimePickerDepartureGateFirstEndTime.Value = new System.DateTime(2004, 11, 11, 9, 42, 59, 921);
			this.dateTimePickerDepartureGateFirstEndTime.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureGateFirstEndTime_ValueChanged);
			// 
			// dateTimePickerDepartureGateFirstEndDate
			// 
			this.dateTimePickerDepartureGateFirstEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureGateFirstEndDate.Location = new System.Drawing.Point(252, 16);
			this.dateTimePickerDepartureGateFirstEndDate.Name = "dateTimePickerDepartureGateFirstEndDate";
			this.dateTimePickerDepartureGateFirstEndDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerDepartureGateFirstEndDate.TabIndex = 80;
			this.dateTimePickerDepartureGateFirstEndDate.Value = new System.DateTime(2004, 11, 11, 9, 42, 59, 937);
			this.dateTimePickerDepartureGateFirstEndDate.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureGateFirstEndDate_ValueChanged);
			// 
			// dateTimePickerDepartureGateFirstBeginTime
			// 
			this.dateTimePickerDepartureGateFirstBeginTime.CustomFormat = "HH:mm";
			this.dateTimePickerDepartureGateFirstBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureGateFirstBeginTime.Location = new System.Drawing.Point(180, 16);
			this.dateTimePickerDepartureGateFirstBeginTime.Name = "dateTimePickerDepartureGateFirstBeginTime";
			this.dateTimePickerDepartureGateFirstBeginTime.ShowUpDown = true;
			this.dateTimePickerDepartureGateFirstBeginTime.Size = new System.Drawing.Size(52, 20);
			this.dateTimePickerDepartureGateFirstBeginTime.TabIndex = 79;
			this.dateTimePickerDepartureGateFirstBeginTime.Value = new System.DateTime(2004, 11, 11, 9, 42, 59, 937);
			this.dateTimePickerDepartureGateFirstBeginTime.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureGateFirstBeginTime_ValueChanged);
			// 
			// dateTimePickerDepartureGateFirstBeginDate
			// 
			this.dateTimePickerDepartureGateFirstBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureGateFirstBeginDate.Location = new System.Drawing.Point(100, 16);
			this.dateTimePickerDepartureGateFirstBeginDate.Name = "dateTimePickerDepartureGateFirstBeginDate";
			this.dateTimePickerDepartureGateFirstBeginDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerDepartureGateFirstBeginDate.TabIndex = 78;
			this.dateTimePickerDepartureGateFirstBeginDate.Value = new System.DateTime(2004, 11, 11, 9, 42, 59, 953);
			this.dateTimePickerDepartureGateFirstBeginDate.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureGateFirstBeginDate_ValueChanged);
			// 
			// dateTimePickerDepartureWaitingRoomSecondEndTime
			// 
			this.dateTimePickerDepartureWaitingRoomSecondEndTime.CustomFormat = "HH:mm";
			this.dateTimePickerDepartureWaitingRoomSecondEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureWaitingRoomSecondEndTime.Location = new System.Drawing.Point(332, 40);
			this.dateTimePickerDepartureWaitingRoomSecondEndTime.Name = "dateTimePickerDepartureWaitingRoomSecondEndTime";
			this.dateTimePickerDepartureWaitingRoomSecondEndTime.ShowUpDown = true;
			this.dateTimePickerDepartureWaitingRoomSecondEndTime.Size = new System.Drawing.Size(52, 20);
			this.dateTimePickerDepartureWaitingRoomSecondEndTime.TabIndex = 75;
			this.dateTimePickerDepartureWaitingRoomSecondEndTime.Value = new System.DateTime(2004, 11, 11, 9, 42, 59, 953);
			this.dateTimePickerDepartureWaitingRoomSecondEndTime.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureWaitingRoomSecondEndTime_ValueChanged);
			// 
			// dateTimePickerDepartureWaitingRoomSecondEndDate
			// 
			this.dateTimePickerDepartureWaitingRoomSecondEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureWaitingRoomSecondEndDate.Location = new System.Drawing.Point(252, 40);
			this.dateTimePickerDepartureWaitingRoomSecondEndDate.Name = "dateTimePickerDepartureWaitingRoomSecondEndDate";
			this.dateTimePickerDepartureWaitingRoomSecondEndDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerDepartureWaitingRoomSecondEndDate.TabIndex = 74;
			this.dateTimePickerDepartureWaitingRoomSecondEndDate.Value = new System.DateTime(2004, 11, 11, 9, 42, 59, 968);
			this.dateTimePickerDepartureWaitingRoomSecondEndDate.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureWaitingRoomSecondEndDate_ValueChanged);
			// 
			// dateTimePickerDepartureWaitingRoomSecondBeginTime
			// 
			this.dateTimePickerDepartureWaitingRoomSecondBeginTime.CustomFormat = "HH:mm";
			this.dateTimePickerDepartureWaitingRoomSecondBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureWaitingRoomSecondBeginTime.Location = new System.Drawing.Point(180, 40);
			this.dateTimePickerDepartureWaitingRoomSecondBeginTime.Name = "dateTimePickerDepartureWaitingRoomSecondBeginTime";
			this.dateTimePickerDepartureWaitingRoomSecondBeginTime.ShowUpDown = true;
			this.dateTimePickerDepartureWaitingRoomSecondBeginTime.Size = new System.Drawing.Size(52, 20);
			this.dateTimePickerDepartureWaitingRoomSecondBeginTime.TabIndex = 73;
			this.dateTimePickerDepartureWaitingRoomSecondBeginTime.Value = new System.DateTime(2004, 11, 11, 9, 42, 59, 968);
			this.dateTimePickerDepartureWaitingRoomSecondBeginTime.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureWaitingRoomSecondBeginTime_ValueChanged);
			// 
			// dateTimePickerDepartureWaitingRoomSecondBeginDate
			// 
			this.dateTimePickerDepartureWaitingRoomSecondBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureWaitingRoomSecondBeginDate.Location = new System.Drawing.Point(100, 40);
			this.dateTimePickerDepartureWaitingRoomSecondBeginDate.Name = "dateTimePickerDepartureWaitingRoomSecondBeginDate";
			this.dateTimePickerDepartureWaitingRoomSecondBeginDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerDepartureWaitingRoomSecondBeginDate.TabIndex = 72;
			this.dateTimePickerDepartureWaitingRoomSecondBeginDate.Value = new System.DateTime(2004, 11, 11, 9, 42, 59, 984);
			this.dateTimePickerDepartureWaitingRoomSecondBeginDate.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureWaitingRoomSecondBeginDate_ValueChanged);
			// 
			// dateTimePickerDepartureWaitingRoomFirstEndTime
			// 
			this.dateTimePickerDepartureWaitingRoomFirstEndTime.CustomFormat = "HH:mm";
			this.dateTimePickerDepartureWaitingRoomFirstEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureWaitingRoomFirstEndTime.Location = new System.Drawing.Point(332, 16);
			this.dateTimePickerDepartureWaitingRoomFirstEndTime.Name = "dateTimePickerDepartureWaitingRoomFirstEndTime";
			this.dateTimePickerDepartureWaitingRoomFirstEndTime.ShowUpDown = true;
			this.dateTimePickerDepartureWaitingRoomFirstEndTime.Size = new System.Drawing.Size(52, 20);
			this.dateTimePickerDepartureWaitingRoomFirstEndTime.TabIndex = 69;
			this.dateTimePickerDepartureWaitingRoomFirstEndTime.Value = new System.DateTime(2004, 11, 11, 9, 43, 0, 0);
			this.dateTimePickerDepartureWaitingRoomFirstEndTime.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureWaitingRoomFirstEndTime_ValueChanged);
			// 
			// dateTimePickerDepartureWaitingRoomFirstEndDate
			// 
			this.dateTimePickerDepartureWaitingRoomFirstEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureWaitingRoomFirstEndDate.Location = new System.Drawing.Point(252, 16);
			this.dateTimePickerDepartureWaitingRoomFirstEndDate.Name = "dateTimePickerDepartureWaitingRoomFirstEndDate";
			this.dateTimePickerDepartureWaitingRoomFirstEndDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerDepartureWaitingRoomFirstEndDate.TabIndex = 68;
			this.dateTimePickerDepartureWaitingRoomFirstEndDate.Value = new System.DateTime(2004, 11, 11, 9, 43, 0, 0);
			this.dateTimePickerDepartureWaitingRoomFirstEndDate.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureWaitingRoomFirstEndDate_ValueChanged);
			// 
			// dateTimePickerDepartureWaitingRoomFirstBeginTime
			// 
			this.dateTimePickerDepartureWaitingRoomFirstBeginTime.CustomFormat = "HH:mm";
			this.dateTimePickerDepartureWaitingRoomFirstBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureWaitingRoomFirstBeginTime.Location = new System.Drawing.Point(180, 16);
			this.dateTimePickerDepartureWaitingRoomFirstBeginTime.Name = "dateTimePickerDepartureWaitingRoomFirstBeginTime";
			this.dateTimePickerDepartureWaitingRoomFirstBeginTime.ShowUpDown = true;
			this.dateTimePickerDepartureWaitingRoomFirstBeginTime.Size = new System.Drawing.Size(52, 20);
			this.dateTimePickerDepartureWaitingRoomFirstBeginTime.TabIndex = 67;
			this.dateTimePickerDepartureWaitingRoomFirstBeginTime.Value = new System.DateTime(2004, 11, 11, 9, 43, 0, 0);
			this.dateTimePickerDepartureWaitingRoomFirstBeginTime.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureWaitingRoomFirstBeginTime_ValueChanged);
			// 
			// dateTimePickerDepartureWaitingRoomFirstBeginDate
			// 
			this.dateTimePickerDepartureWaitingRoomFirstBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureWaitingRoomFirstBeginDate.Location = new System.Drawing.Point(100, 16);
			this.dateTimePickerDepartureWaitingRoomFirstBeginDate.Name = "dateTimePickerDepartureWaitingRoomFirstBeginDate";
			this.dateTimePickerDepartureWaitingRoomFirstBeginDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerDepartureWaitingRoomFirstBeginDate.TabIndex = 66;
			this.dateTimePickerDepartureWaitingRoomFirstBeginDate.Value = new System.DateTime(2004, 11, 11, 9, 43, 0, 15);
			this.dateTimePickerDepartureWaitingRoomFirstBeginDate.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureWaitingRoomFirstBeginDate_ValueChanged);
			// 
			// dateTimePickerDepartureParkingStandEndTime
			// 
			this.dateTimePickerDepartureParkingStandEndTime.CustomFormat = "HH:mm";
			this.dateTimePickerDepartureParkingStandEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureParkingStandEndTime.Location = new System.Drawing.Point(332, 16);
			this.dateTimePickerDepartureParkingStandEndTime.Name = "dateTimePickerDepartureParkingStandEndTime";
			this.dateTimePickerDepartureParkingStandEndTime.ShowUpDown = true;
			this.dateTimePickerDepartureParkingStandEndTime.Size = new System.Drawing.Size(52, 20);
			this.dateTimePickerDepartureParkingStandEndTime.TabIndex = 63;
			this.dateTimePickerDepartureParkingStandEndTime.Value = new System.DateTime(2004, 11, 11, 9, 43, 0, 15);
			this.dateTimePickerDepartureParkingStandEndTime.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureParkingStandEndTime_ValueChanged);
			// 
			// dateTimePickerDepartureParkingStandEndDate
			// 
			this.dateTimePickerDepartureParkingStandEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureParkingStandEndDate.Location = new System.Drawing.Point(252, 16);
			this.dateTimePickerDepartureParkingStandEndDate.Name = "dateTimePickerDepartureParkingStandEndDate";
			this.dateTimePickerDepartureParkingStandEndDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerDepartureParkingStandEndDate.TabIndex = 62;
			this.dateTimePickerDepartureParkingStandEndDate.Value = new System.DateTime(2004, 11, 11, 9, 43, 0, 31);
			this.dateTimePickerDepartureParkingStandEndDate.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureParkingStandEndDate_ValueChanged);
			// 
			// dateTimePickerDepartureParkingStandBeginTime
			// 
			this.dateTimePickerDepartureParkingStandBeginTime.CustomFormat = "HH:mm";
			this.dateTimePickerDepartureParkingStandBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureParkingStandBeginTime.Location = new System.Drawing.Point(180, 16);
			this.dateTimePickerDepartureParkingStandBeginTime.Name = "dateTimePickerDepartureParkingStandBeginTime";
			this.dateTimePickerDepartureParkingStandBeginTime.ShowUpDown = true;
			this.dateTimePickerDepartureParkingStandBeginTime.Size = new System.Drawing.Size(52, 20);
			this.dateTimePickerDepartureParkingStandBeginTime.TabIndex = 61;
			this.dateTimePickerDepartureParkingStandBeginTime.Value = new System.DateTime(2004, 11, 11, 9, 43, 0, 31);
			this.dateTimePickerDepartureParkingStandBeginTime.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureParkingStandBeginTime_ValueChanged);
			// 
			// dateTimePickerDepartureParkingStandBeginDate
			// 
			this.dateTimePickerDepartureParkingStandBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureParkingStandBeginDate.Location = new System.Drawing.Point(100, 16);
			this.dateTimePickerDepartureParkingStandBeginDate.Name = "dateTimePickerDepartureParkingStandBeginDate";
			this.dateTimePickerDepartureParkingStandBeginDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerDepartureParkingStandBeginDate.TabIndex = 60;
			this.dateTimePickerDepartureParkingStandBeginDate.Value = new System.DateTime(2004, 11, 11, 9, 43, 0, 31);
			this.dateTimePickerDepartureParkingStandBeginDate.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureParkingStandBeginDate_ValueChanged);
			// 
			// comboBoxDepartureFlightIdentification
			// 
			this.comboBoxDepartureFlightIdentification.ItemHeight = 13;
			this.comboBoxDepartureFlightIdentification.Location = new System.Drawing.Point(284, 108);
			this.comboBoxDepartureFlightIdentification.Name = "comboBoxDepartureFlightIdentification";
			this.comboBoxDepartureFlightIdentification.Size = new System.Drawing.Size(100, 21);
			this.comboBoxDepartureFlightIdentification.TabIndex = 23;
			this.comboBoxDepartureFlightIdentification.SelectedValueChanged += new System.EventHandler(this.comboBoxDepartureFlightIdentification_SelectedValueChanged);
			// 
			// label26
			// 
			this.label26.Location = new System.Drawing.Point(208, 112);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(76, 16);
			this.label26.TabIndex = 22;
			this.label26.Text = "Identification";
			// 
			// comboBoxDepartureServiceType
			// 
			this.comboBoxDepartureServiceType.ItemHeight = 13;
			this.comboBoxDepartureServiceType.Location = new System.Drawing.Point(284, 84);
			this.comboBoxDepartureServiceType.Name = "comboBoxDepartureServiceType";
			this.comboBoxDepartureServiceType.Size = new System.Drawing.Size(100, 21);
			this.comboBoxDepartureServiceType.TabIndex = 21;
			this.comboBoxDepartureServiceType.SelectedValueChanged += new System.EventHandler(this.comboBoxDepartureServiceType_SelectedValueChanged);
			// 
			// label27
			// 
			this.label27.Location = new System.Drawing.Point(208, 88);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(76, 16);
			this.label27.TabIndex = 20;
			this.label27.Text = "Servicetype";
			// 
			// comboBoxDepartureFlightNature
			// 
			this.comboBoxDepartureFlightNature.ItemHeight = 13;
			this.comboBoxDepartureFlightNature.Location = new System.Drawing.Point(84, 108);
			this.comboBoxDepartureFlightNature.Name = "comboBoxDepartureFlightNature";
			this.comboBoxDepartureFlightNature.Size = new System.Drawing.Size(100, 21);
			this.comboBoxDepartureFlightNature.TabIndex = 19;
			this.comboBoxDepartureFlightNature.SelectedValueChanged += new System.EventHandler(this.comboBoxDepartureFlightNature_SelectedValueChanged);
			// 
			// label28
			// 
			this.label28.Location = new System.Drawing.Point(8, 112);
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size(76, 16);
			this.label28.TabIndex = 18;
			this.label28.Text = "Nature";
			// 
			// comboBoxDepartureAircraftCode
			// 
			this.comboBoxDepartureAircraftCode.ItemHeight = 13;
			this.comboBoxDepartureAircraftCode.Location = new System.Drawing.Point(84, 84);
			this.comboBoxDepartureAircraftCode.Name = "comboBoxDepartureAircraftCode";
			this.comboBoxDepartureAircraftCode.Size = new System.Drawing.Size(100, 21);
			this.comboBoxDepartureAircraftCode.TabIndex = 17;
			this.comboBoxDepartureAircraftCode.SelectedValueChanged += new System.EventHandler(this.comboBoxDepartureAircraftCode_SelectedValueChanged);
			// 
			// comboBoxDepartureDestination
			// 
			this.comboBoxDepartureDestination.ItemHeight = 13;
			this.comboBoxDepartureDestination.Location = new System.Drawing.Point(284, 60);
			this.comboBoxDepartureDestination.Name = "comboBoxDepartureDestination";
			this.comboBoxDepartureDestination.Size = new System.Drawing.Size(100, 21);
			this.comboBoxDepartureDestination.TabIndex = 16;
			this.comboBoxDepartureDestination.SelectedValueChanged += new System.EventHandler(this.comboBoxDepartureDestination_SelectedValueChanged);
			// 
			// dateTimePickerDepartureStandardTime
			// 
			this.dateTimePickerDepartureStandardTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
			this.dateTimePickerDepartureStandardTime.Location = new System.Drawing.Point(164, 36);
			this.dateTimePickerDepartureStandardTime.Name = "dateTimePickerDepartureStandardTime";
			this.dateTimePickerDepartureStandardTime.ShowUpDown = true;
			this.dateTimePickerDepartureStandardTime.Size = new System.Drawing.Size(68, 20);
			this.dateTimePickerDepartureStandardTime.TabIndex = 15;
			this.dateTimePickerDepartureStandardTime.Value = new System.DateTime(2004, 11, 11, 9, 43, 0, 109);
			this.dateTimePickerDepartureStandardTime.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureStandardTime_ValueChanged);
			// 
			// dateTimePickerDepartureStandardDate
			// 
			this.dateTimePickerDepartureStandardDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureStandardDate.Location = new System.Drawing.Point(84, 36);
			this.dateTimePickerDepartureStandardDate.Name = "dateTimePickerDepartureStandardDate";
			this.dateTimePickerDepartureStandardDate.Size = new System.Drawing.Size(80, 20);
			this.dateTimePickerDepartureStandardDate.TabIndex = 14;
			this.dateTimePickerDepartureStandardDate.Value = new System.DateTime(2004, 11, 11, 9, 43, 0, 125);
			this.dateTimePickerDepartureStandardDate.ValueChanged += new System.EventHandler(this.dateTimePickerDepartureStandardDate_ValueChanged);
			// 
			// comboBoxDepartureVia
			// 
			this.comboBoxDepartureVia.ItemHeight = 13;
			this.comboBoxDepartureVia.Location = new System.Drawing.Point(84, 60);
			this.comboBoxDepartureVia.Name = "comboBoxDepartureVia";
			this.comboBoxDepartureVia.Size = new System.Drawing.Size(100, 21);
			this.comboBoxDepartureVia.TabIndex = 13;
			this.comboBoxDepartureVia.SelectedValueChanged += new System.EventHandler(this.comboBoxDepartureVia_SelectedValueChanged);
			// 
			// label29
			// 
			this.label29.Location = new System.Drawing.Point(208, 64);
			this.label29.Name = "label29";
			this.label29.Size = new System.Drawing.Size(76, 16);
			this.label29.TabIndex = 12;
			this.label29.Text = "DES";
			// 
			// label30
			// 
			this.label30.Location = new System.Drawing.Point(8, 64);
			this.label30.Name = "label30";
			this.label30.Size = new System.Drawing.Size(76, 16);
			this.label30.TabIndex = 10;
			this.label30.Text = "VIA";
			// 
			// label31
			// 
			this.label31.Location = new System.Drawing.Point(8, 40);
			this.label31.Name = "label31";
			this.label31.Size = new System.Drawing.Size(76, 16);
			this.label31.TabIndex = 7;
			this.label31.Text = "STD";
			// 
			// labelDepartureCallSign
			// 
			this.labelDepartureCallSign.Location = new System.Drawing.Point(208, 8);
			this.labelDepartureCallSign.Name = "labelDepartureCallSign";
			this.labelDepartureCallSign.Size = new System.Drawing.Size(76, 16);
			this.labelDepartureCallSign.TabIndex = 6;
			this.labelDepartureCallSign.Text = "Callsign";
			// 
			// textBoxDepartureCallsign
			// 
			this.textBoxDepartureCallsign.Location = new System.Drawing.Point(284, 4);
			this.textBoxDepartureCallsign.Name = "textBoxDepartureCallsign";
			this.textBoxDepartureCallsign.TabIndex = 5;
			this.textBoxDepartureCallsign.Text = "";
			this.textBoxDepartureCallsign.TextChanged += new System.EventHandler(this.textBoxDepartureCallsign_TextChanged);
			// 
			// textBoxDepartureFlightNumber
			// 
			this.textBoxDepartureFlightNumber.Location = new System.Drawing.Point(84, 4);
			this.textBoxDepartureFlightNumber.Name = "textBoxDepartureFlightNumber";
			this.textBoxDepartureFlightNumber.TabIndex = 4;
			this.textBoxDepartureFlightNumber.Text = "";
			this.textBoxDepartureFlightNumber.TextChanged += new System.EventHandler(this.textBoxDepartureFlightNumber_TextChanged);
			// 
			// labelDepartureFlightNumber
			// 
			this.labelDepartureFlightNumber.Location = new System.Drawing.Point(8, 8);
			this.labelDepartureFlightNumber.Name = "labelDepartureFlightNumber";
			this.labelDepartureFlightNumber.Size = new System.Drawing.Size(76, 16);
			this.labelDepartureFlightNumber.TabIndex = 3;
			this.labelDepartureFlightNumber.Text = "FlightNumber";
			// 
			// label34
			// 
			this.label34.Location = new System.Drawing.Point(8, 88);
			this.label34.Name = "label34";
			this.label34.Size = new System.Drawing.Size(76, 16);
			this.label34.TabIndex = 4;
			this.label34.Text = "Aircraft";
			// 
			// comboBoxDepartureParkingStand
			// 
			this.comboBoxDepartureParkingStand.ItemHeight = 13;
			this.comboBoxDepartureParkingStand.Location = new System.Drawing.Point(8, 16);
			this.comboBoxDepartureParkingStand.Name = "comboBoxDepartureParkingStand";
			this.comboBoxDepartureParkingStand.Size = new System.Drawing.Size(72, 21);
			this.comboBoxDepartureParkingStand.TabIndex = 63;
			this.comboBoxDepartureParkingStand.SelectedValueChanged += new System.EventHandler(this.comboBoxDepartureParkingStand_SelectedValueChanged);
			// 
			// panelDeparture
			// 
			this.panelDeparture.Controls.Add(this.groupBoxDepartureGates);
			this.panelDeparture.Controls.Add(this.groupBoxDepartureWaitingRooms);
			this.panelDeparture.Controls.Add(this.groupBoxDepartureParkingStand);
			this.panelDeparture.Controls.Add(this.dataGridDepartureCounters);
			this.panelDeparture.Controls.Add(this.comboBoxDepartureFlightIdentification);
			this.panelDeparture.Controls.Add(this.label26);
			this.panelDeparture.Controls.Add(this.comboBoxDepartureServiceType);
			this.panelDeparture.Controls.Add(this.label27);
			this.panelDeparture.Controls.Add(this.comboBoxDepartureFlightNature);
			this.panelDeparture.Controls.Add(this.label28);
			this.panelDeparture.Controls.Add(this.comboBoxDepartureAircraftCode);
			this.panelDeparture.Controls.Add(this.comboBoxDepartureDestination);
			this.panelDeparture.Controls.Add(this.dateTimePickerDepartureStandardTime);
			this.panelDeparture.Controls.Add(this.dateTimePickerDepartureStandardDate);
			this.panelDeparture.Controls.Add(this.comboBoxDepartureVia);
			this.panelDeparture.Controls.Add(this.label29);
			this.panelDeparture.Controls.Add(this.label30);
			this.panelDeparture.Controls.Add(this.label31);
			this.panelDeparture.Controls.Add(this.labelDepartureCallSign);
			this.panelDeparture.Controls.Add(this.textBoxDepartureCallsign);
			this.panelDeparture.Controls.Add(this.textBoxDepartureFlightNumber);
			this.panelDeparture.Controls.Add(this.labelDepartureFlightNumber);
			this.panelDeparture.Controls.Add(this.label34);
			this.panelDeparture.Location = new System.Drawing.Point(428, 28);
			this.panelDeparture.Name = "panelDeparture";
			this.panelDeparture.Size = new System.Drawing.Size(408, 484);
			this.panelDeparture.TabIndex = 17;
			// 
			// groupBoxDepartureGates
			// 
			this.groupBoxDepartureGates.Controls.Add(this.dateTimePickerDepartureGateSecondBeginDate);
			this.groupBoxDepartureGates.Controls.Add(this.dateTimePickerDepartureGateSecondEndDate);
			this.groupBoxDepartureGates.Controls.Add(this.comboBoxDepartureGateFirst);
			this.groupBoxDepartureGates.Controls.Add(this.dateTimePickerDepartureGateSecondEndTime);
			this.groupBoxDepartureGates.Controls.Add(this.comboBoxDepartureGateSecond);
			this.groupBoxDepartureGates.Controls.Add(this.dateTimePickerDepartureGateFirstEndTime);
			this.groupBoxDepartureGates.Controls.Add(this.dateTimePickerDepartureGateFirstBeginTime);
			this.groupBoxDepartureGates.Controls.Add(this.dateTimePickerDepartureGateFirstEndDate);
			this.groupBoxDepartureGates.Controls.Add(this.dateTimePickerDepartureGateSecondBeginTime);
			this.groupBoxDepartureGates.Controls.Add(this.dateTimePickerDepartureGateFirstBeginDate);
			this.groupBoxDepartureGates.Location = new System.Drawing.Point(8, 264);
			this.groupBoxDepartureGates.Name = "groupBoxDepartureGates";
			this.groupBoxDepartureGates.Size = new System.Drawing.Size(396, 68);
			this.groupBoxDepartureGates.TabIndex = 95;
			this.groupBoxDepartureGates.TabStop = false;
			this.groupBoxDepartureGates.Text = "Gates";
			// 
			// groupBoxDepartureWaitingRooms
			// 
			this.groupBoxDepartureWaitingRooms.Controls.Add(this.dateTimePickerDepartureWaitingRoomFirstBeginTime);
			this.groupBoxDepartureWaitingRooms.Controls.Add(this.dateTimePickerDepartureWaitingRoomFirstEndTime);
			this.groupBoxDepartureWaitingRooms.Controls.Add(this.dateTimePickerDepartureWaitingRoomSecondBeginDate);
			this.groupBoxDepartureWaitingRooms.Controls.Add(this.dateTimePickerDepartureWaitingRoomSecondEndTime);
			this.groupBoxDepartureWaitingRooms.Controls.Add(this.dateTimePickerDepartureWaitingRoomSecondBeginTime);
			this.groupBoxDepartureWaitingRooms.Controls.Add(this.comboBoxDepartureWaitingRoomSecond);
			this.groupBoxDepartureWaitingRooms.Controls.Add(this.dateTimePickerDepartureWaitingRoomFirstEndDate);
			this.groupBoxDepartureWaitingRooms.Controls.Add(this.dateTimePickerDepartureWaitingRoomFirstBeginDate);
			this.groupBoxDepartureWaitingRooms.Controls.Add(this.comboBoxDepartureWaitingRoomFirst);
			this.groupBoxDepartureWaitingRooms.Controls.Add(this.dateTimePickerDepartureWaitingRoomSecondEndDate);
			this.groupBoxDepartureWaitingRooms.Location = new System.Drawing.Point(8, 188);
			this.groupBoxDepartureWaitingRooms.Name = "groupBoxDepartureWaitingRooms";
			this.groupBoxDepartureWaitingRooms.Size = new System.Drawing.Size(396, 68);
			this.groupBoxDepartureWaitingRooms.TabIndex = 94;
			this.groupBoxDepartureWaitingRooms.TabStop = false;
			this.groupBoxDepartureWaitingRooms.Text = "Waiting Rooms";
			// 
			// groupBoxDepartureParkingStand
			// 
			this.groupBoxDepartureParkingStand.Controls.Add(this.comboBoxDepartureParkingStand);
			this.groupBoxDepartureParkingStand.Controls.Add(this.dateTimePickerDepartureParkingStandEndDate);
			this.groupBoxDepartureParkingStand.Controls.Add(this.dateTimePickerDepartureParkingStandEndTime);
			this.groupBoxDepartureParkingStand.Controls.Add(this.dateTimePickerDepartureParkingStandBeginDate);
			this.groupBoxDepartureParkingStand.Controls.Add(this.dateTimePickerDepartureParkingStandBeginTime);
			this.groupBoxDepartureParkingStand.Location = new System.Drawing.Point(8, 136);
			this.groupBoxDepartureParkingStand.Name = "groupBoxDepartureParkingStand";
			this.groupBoxDepartureParkingStand.Size = new System.Drawing.Size(396, 44);
			this.groupBoxDepartureParkingStand.TabIndex = 93;
			this.groupBoxDepartureParkingStand.TabStop = false;
			this.groupBoxDepartureParkingStand.Text = "Parking Stand";
			// 
			// RecordDetail
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(840, 513);
			this.Controls.Add(this.buttonApply);
			this.Controls.Add(this.buttonClear);
			this.Controls.Add(this.panelDeparture);
			this.Controls.Add(this.panelArrival);
			this.Controls.Add(this.labelDeparture);
			this.Controls.Add(this.labelArrival);
			this.Controls.Add(this.buttonCancel);
			this.Name = "RecordDetail";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "RecordDetail";
			this.TopMost = true;
			this.Load += new System.EventHandler(this.RecordDetail_Load);
			this.Closed += new System.EventHandler(this.RecordDetail_Closed);
			this.panelArrival.ResumeLayout(false);
			this.groupBoxArrivalGates.ResumeLayout(false);
			this.groupBoxArrivalBaggageBelts.ResumeLayout(false);
			this.groupBoxArrivalParkingStand.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridDepartureCounters)).EndInit();
			this.panelDeparture.ResumeLayout(false);
			this.groupBoxDepartureGates.ResumeLayout(false);
			this.groupBoxDepartureWaitingRooms.ResumeLayout(false);
			this.groupBoxDepartureParkingStand.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void RecordDetail_Closed(object sender, System.EventArgs e)
		{
				this.Dispose();		
		}



		public bool Delete
		{
			get
			{
				return this.delete;
			}
			set
			{
				this.delete = value;
			}
		}

		public bool Init
		{
			get
			{
				return this.init;
			}
			set
			{
				this.init = value;
			}
		}

		internal void FillArrivalControls()
		{
			if(dataRowArrival.Table.Rows.Count > 0)
			{


				//flight number and callsign
				this.textBoxArrivalFlightNumber.DataBindings.Add("Text",this.dataRowArrival["FlightNumber"].ToString(),"");
				this.textBoxArrivalCallsign.DataBindings.Add("Text",this.dataRowArrival["CallSign"].ToString(),"");
			
				//origin
				this.comboBoxArrivalOrigin.DataBindings.Add("Text",this.dataRowArrival["Origin"].ToString(),"");
				for(int i = 0; i < this.dataSetFlightRecords.Tables["Airports"].Rows.Count; i++)
					this.comboBoxArrivalOrigin.Items.Add(this.dataSetFlightRecords.Tables["Airports"].Rows[i]["Code3"].ToString());

				//via
				this.comboBoxArrivalVia.DataBindings.Add("Text",this.dataRowArrival["Via"].ToString(),"");
				for(int i = 0; i < this.dataSetFlightRecords.Tables["Airports"].Rows.Count; i++)
					this.comboBoxArrivalVia.Items.Add(this.dataSetFlightRecords.Tables["Airports"].Rows[i]["Code3"].ToString());

				//aircraft code
				this.comboBoxArrivalAircraftCode.DataBindings.Add("Text",this.dataRowArrival["AcCode"].ToString(),"");
				for(int i = 0; i < this.dataSetFlightRecords.Tables["AircraftTypes"].Rows.Count; i++)
					this.comboBoxArrivalAircraftCode.Items.Add(this.dataSetFlightRecords.Tables["AircraftTypes"].Rows[i]["Code3"].ToString());

				//service type
				this.comboBoxArrivalServiceType.DataBindings.Add("Text",this.dataRowArrival["ServiceType"].ToString(),"");
				for(int i = 0; i < this.dataSetFlightRecords.Tables["ServiceTypes"].Rows.Count; i++)
					this.comboBoxArrivalServiceType.Items.Add(this.dataSetFlightRecords.Tables["ServiceTypes"].Rows[i]["Code2"].ToString());

				//flight nature
				this.comboBoxArrivalFlightNature.DataBindings.Add("Text",this.dataRowArrival["FlightNature"].ToString(),"");
				for(int i = 0; i < this.dataSetFlightRecords.Tables["FlightNatures"].Rows.Count; i++)
					this.comboBoxArrivalFlightNature.Items.Add(this.dataSetFlightRecords.Tables["FlightNatures"].Rows[i]["Code5"].ToString());

				//flight identification
				int valu = (int)this.dataRowArrival["FlightIdentification"];
				Type type = this.dataRowArrival.Table.Columns["FlightIdentification"].DataType;
				string[] names = Enum.GetNames(type);
				this.comboBoxArrivalFlightIdentification.DataBindings.Add("Text",Enum.GetName(type,valu).ToString(),"");
				for(int i = 0; i < names.Length; i++)
					this.comboBoxArrivalFlightIdentification.Items.Add(names[i]);
				
				//sta 
				this.dateTimePickerArrivalStandardDate.Value = MyController.DateTimeFromString(this.dataRowArrival["StandardTimeArrival"].ToString());
				this.dateTimePickerArrivalStandardTime.Value = MyController.DateTimeFromString(this.dataRowArrival["StandardTimeArrival"].ToString());
		
			
				//parking stand
				this.comboBoxArrivalParkingStand.DataBindings.Add("Text",this.dataRowArrivalParkingStand[0]["ParkingStandArrivalName"].ToString(),"");
				
				for(int i = 0; i < this.dataSetFlightRecords.Tables["AirportParkingStands"].Rows.Count; i++)
					this.comboBoxArrivalParkingStand.Items.Add(this.dataSetFlightRecords.Tables["AirportParkingStands"].Rows[i]["Name"].ToString());
				
				this.dateTimePickerArrivalParkingStandBeginDate.Value = MyController.DateTimeFromString(this.dataRowArrivalParkingStand[0]["ParkingStandArrivalBegin"].ToString());
				this.dateTimePickerArrivalParkingStandBeginTime.Value = MyController.DateTimeFromString(this.dataRowArrivalParkingStand[0]["ParkingStandArrivalBegin"].ToString());
				this.dateTimePickerArrivalParkingStandBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
				this.dateTimePickerArrivalParkingStandBeginTime.CustomFormat = "HH:mm";

				if(this.dataRowArrivalParkingStand[0]["ParkingStandArrivalBegin"].ToString() == "")
					ClearArrivalParkingStandBeginDateTime();
	
				this.dateTimePickerArrivalParkingStandEndDate.Value = MyController.DateTimeFromString(this.dataRowArrivalParkingStand[0]["ParkingStandArrivalEnd"].ToString());
				this.dateTimePickerArrivalParkingStandEndTime.Value = MyController.DateTimeFromString(this.dataRowArrivalParkingStand[0]["ParkingStandArrivalEnd"].ToString());
				this.dateTimePickerArrivalParkingStandEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
				this.dateTimePickerArrivalParkingStandEndTime.CustomFormat = "HH:mm";

				if(this.dataRowArrivalParkingStand[0]["ParkingStandArrivalEnd"].ToString() == "")
					ClearArrivalParkingStandEndDateTime();			


				// baggage belts
				this.comboBoxArrivalBaggageBeltFirst.DataBindings.Add("Text",this.dataRowArrivalBaggageBelts[0]["BaggageBeltsFirstName"].ToString(),"");
				
				for(int i = 0; i < this.dataSetFlightRecords.Tables["AirportBaggageBelts"].Rows.Count; i++)
					this.comboBoxArrivalBaggageBeltFirst.Items.Add(this.dataSetFlightRecords.Tables["AirportBaggageBelts"].Rows[i]["Name"].ToString());


				this.dateTimePickerArrivalBaggageBeltFirstBeginDate.Value = MyController.DateTimeFromString(this.dataRowArrivalBaggageBelts[0]["BaggageBeltsFirstBegin"].ToString());
				this.dateTimePickerArrivalBaggageBeltFirstBeginTime.Value = MyController.DateTimeFromString(this.dataRowArrivalBaggageBelts[0]["BaggageBeltsFirstBegin"].ToString());
				this.dateTimePickerArrivalBaggageBeltFirstBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
				this.dateTimePickerArrivalBaggageBeltFirstBeginTime.CustomFormat = "HH:mm";

				if(this.dataRowArrivalBaggageBelts[0]["BaggageBeltsFirstBegin"].ToString() == "")
					ClearArrivalBaggageBeltFirstBeginDateTime();
				
				this.dateTimePickerArrivalBaggageBeltFirstEndDate.Value = MyController.DateTimeFromString(this.dataRowArrivalBaggageBelts[0]["BaggageBeltsFirstEnd"].ToString());
				this.dateTimePickerArrivalBaggageBeltFirstEndTime.Value = MyController.DateTimeFromString(this.dataRowArrivalBaggageBelts[0]["BaggageBeltsFirstEnd"].ToString());
				this.dateTimePickerArrivalBaggageBeltFirstEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
				this.dateTimePickerArrivalBaggageBeltFirstEndTime.CustomFormat = "HH:mm";

				if(this.dataRowArrivalBaggageBelts[0]["BaggageBeltsFirstEnd"].ToString() == "")
					ClearArrivalBaggageBeltFirstEndDateTime();


				this.comboBoxArrivalBaggageBeltSecond.DataBindings.Add("Text",this.dataRowArrivalBaggageBelts[0]["BaggageBeltsSecondName"].ToString(),"");
				
				for(int i = 0; i < this.dataSetFlightRecords.Tables["AirportBaggageBelts"].Rows.Count; i++)
					this.comboBoxArrivalBaggageBeltSecond.Items.Add(this.dataSetFlightRecords.Tables["AirportBaggageBelts"].Rows[i]["Name"].ToString());

				this.dateTimePickerArrivalBaggageBeltSecondBeginDate.Value = MyController.DateTimeFromString(this.dataRowArrivalBaggageBelts[0]["BaggageBeltsSecondBegin"].ToString());
				this.dateTimePickerArrivalBaggageBeltSecondBeginTime.Value = MyController.DateTimeFromString(this.dataRowArrivalBaggageBelts[0]["BaggageBeltsSecondBegin"].ToString());
				this.dateTimePickerArrivalBaggageBeltSecondBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
				this.dateTimePickerArrivalBaggageBeltSecondBeginTime.CustomFormat = "HH:mm";

				if(this.dataRowArrivalBaggageBelts[0]["BaggageBeltsSecondBegin"].ToString() == "")
					ClearArrivalBaggageBeltSecondBeginDateTime();
				
				this.dateTimePickerArrivalBaggageBeltSecondEndDate.Value = MyController.DateTimeFromString(this.dataRowArrivalBaggageBelts[0]["BaggageBeltsSecondEnd"].ToString());
				this.dateTimePickerArrivalBaggageBeltSecondEndTime.Value = MyController.DateTimeFromString(this.dataRowArrivalBaggageBelts[0]["BaggageBeltsSecondEnd"].ToString());
				this.dateTimePickerArrivalBaggageBeltSecondEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
				this.dateTimePickerArrivalBaggageBeltSecondEndTime.CustomFormat = "HH:mm";

				if(this.dataRowArrivalBaggageBelts[0]["BaggageBeltsSecondEnd"].ToString() == "")
					ClearArrivalBaggageBeltSecondEndDateTime();







				//gates
				this.comboBoxArrivalGateFirst.DataBindings.Add("Text",this.dataRowArrivalGates[0]["GatesArrivalFirstName"].ToString(),"");
				
				for(int i = 0; i < this.dataSetFlightRecords.Tables["AirportGates"].Rows.Count; i++)
					this.comboBoxArrivalGateFirst.Items.Add(this.dataSetFlightRecords.Tables["AirportGates"].Rows[i]["Name"].ToString());

				this.dateTimePickerArrivalGateFirstBeginDate.Value = MyController.DateTimeFromString(this.dataRowArrivalGates[0]["GatesArrivalFirstBegin"].ToString());
				this.dateTimePickerArrivalGateFirstBeginTime.Value = MyController.DateTimeFromString(this.dataRowArrivalGates[0]["GatesArrivalFirstBegin"].ToString());
				this.dateTimePickerArrivalGateFirstBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
				this.dateTimePickerArrivalGateFirstBeginTime.CustomFormat = "HH:mm";

				if(this.dataRowArrivalGates[0]["GatesArrivalFirstBegin"].ToString() == "")
					ClearArrivalGateFirstBeginDateTime();
				
				this.dateTimePickerArrivalGateFirstEndDate.Value = MyController.DateTimeFromString(this.dataRowArrivalGates[0]["GatesArrivalFirstEnd"].ToString());
				this.dateTimePickerArrivalGateFirstEndTime.Value = MyController.DateTimeFromString(this.dataRowArrivalGates[0]["GatesArrivalFirstEnd"].ToString());
				this.dateTimePickerArrivalGateFirstEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
				this.dateTimePickerArrivalGateFirstEndTime.CustomFormat = "HH:mm";

				if(this.dataRowArrivalGates[0]["GatesArrivalFirstEnd"].ToString() == "")
					ClearArrivalGateFirstEndDateTime();

				this.comboBoxArrivalGateSecond.DataBindings.Add("Text",this.dataRowArrivalGates[0]["GatesArrivalSecondName"].ToString(),"");
				
				for(int i = 0; i < this.dataSetFlightRecords.Tables["AirportGates"].Rows.Count; i++)
					this.comboBoxArrivalGateSecond.Items.Add(this.dataSetFlightRecords.Tables["AirportGates"].Rows[i]["Name"].ToString());

				this.dateTimePickerArrivalGateSecondBeginDate.Value = MyController.DateTimeFromString(this.dataRowArrivalGates[0]["GatesArrivalSecondBegin"].ToString());
				this.dateTimePickerArrivalGateSecondBeginTime.Value = MyController.DateTimeFromString(this.dataRowArrivalGates[0]["GatesArrivalSecondBegin"].ToString());
				this.dateTimePickerArrivalGateSecondBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
				this.dateTimePickerArrivalGateSecondBeginTime.CustomFormat = "HH:mm";

				if(this.dataRowArrivalGates[0]["GatesArrivalSecondBegin"].ToString() == "")
					ClearArrivalGateSecondBeginDateTime();
				
				this.dateTimePickerArrivalGateSecondEndDate.Value = MyController.DateTimeFromString(this.dataRowArrivalGates[0]["GatesArrivalSecondEnd"].ToString());
				this.dateTimePickerArrivalGateSecondEndTime.Value = MyController.DateTimeFromString(this.dataRowArrivalGates[0]["GatesArrivalSecondEnd"].ToString());
				this.dateTimePickerArrivalGateSecondEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
				this.dateTimePickerArrivalGateSecondEndTime.CustomFormat = "HH:mm";

				if(this.dataRowArrivalGates[0]["GatesArrivalSecondEnd"].ToString() == "")
					ClearArrivalGateSecondEndDateTime();

			}
		}

		internal void FillDepartureControls()
		{
			if(this.dataRowDeparture.Table.Rows.Count > 0)
			{

				this.textBoxDepartureFlightNumber.DataBindings.Add("Text",dataRowDeparture["FlightNumber"].ToString(),"");
				this.textBoxDepartureCallsign.DataBindings.Add("Text",this.dataRowDeparture["CallSign"].ToString(),"");

				this.comboBoxDepartureDestination.DataBindings.Add("Text",this.dataRowDeparture["Destination"].ToString(),"");
				for(int i = 0; i < this.dataSetFlightRecords.Tables["Airports"].Rows.Count; i++)
					this.comboBoxDepartureDestination.Items.Add(this.dataSetFlightRecords.Tables["Airports"].Rows[i]["Code3"].ToString());

				this.comboBoxDepartureVia.DataBindings.Add("Text",this.dataRowDeparture["Via"].ToString(),"");
				for(int i = 0; i < this.dataSetFlightRecords.Tables["Airports"].Rows.Count; i++)
					this.comboBoxDepartureVia.Items.Add(this.dataSetFlightRecords.Tables["Airports"].Rows[i]["Code3"].ToString());

				this.comboBoxDepartureAircraftCode.DataBindings.Add("Text",this.dataRowDeparture["AcCode"].ToString(),"");
				for(int i = 0; i < this.dataSetFlightRecords.Tables["AircraftTypes"].Rows.Count; i++)
					this.comboBoxDepartureAircraftCode.Items.Add(this.dataSetFlightRecords.Tables["AircraftTypes"].Rows[i]["Code3"].ToString());

				this.comboBoxDepartureServiceType.DataBindings.Add("Text",this.dataRowDeparture["ServiceType"].ToString(),"");
				for(int i = 0; i < this.dataSetFlightRecords.Tables["ServiceTypes"].Rows.Count; i++)
					this.comboBoxDepartureServiceType.Items.Add(this.dataSetFlightRecords.Tables["ServiceTypes"].Rows[i]["Code2"].ToString());

				this.comboBoxDepartureFlightNature.DataBindings.Add("Text",this.dataRowDeparture["FlightNature"].ToString(),"");
				for(int i = 0; i < this.dataSetFlightRecords.Tables["FlightNatures"].Rows.Count; i++)
					this.comboBoxDepartureFlightNature.Items.Add(this.dataSetFlightRecords.Tables["FlightNatures"].Rows[i]["Code5"].ToString());


				int valu = (int)this.dataRowDeparture["FlightIdentification"];
				Type type = this.dataRowDeparture.Table.Columns["FlightIdentification"].DataType;
				string[] names = Enum.GetNames(type);
				this.comboBoxDepartureFlightIdentification.DataBindings.Add("Text",Enum.GetName(type,valu).ToString(),"");
				for(int i = 0; i < names.Length; i++)
					this.comboBoxDepartureFlightIdentification.Items.Add(names[i]);


				this.dateTimePickerDepartureStandardDate.Value = MyController.DateTimeFromString(this.dataRowDeparture["StandardTimeDeparture"].ToString());		
				this.dateTimePickerDepartureStandardTime.Value = MyController.DateTimeFromString(this.dataRowDeparture["StandardTimeDeparture"].ToString());






				//parking stand
				this.comboBoxDepartureParkingStand.DataBindings.Add("Text",this.dataRowDepartureParkingStand[0]["ParkingStandDepartureName"].ToString(),"");
				
				for(int i = 0; i < this.dataSetFlightRecords.Tables["AirportParkingStands"].Rows.Count; i++)
					this.comboBoxDepartureParkingStand.Items.Add(this.dataSetFlightRecords.Tables["AirportParkingStands"].Rows[i]["Name"].ToString());


				this.dateTimePickerDepartureParkingStandBeginDate.Value = MyController.DateTimeFromString(this.dataRowDepartureParkingStand[0]["ParkingStandDepartureBegin"].ToString());
				this.dateTimePickerDepartureParkingStandBeginTime.Value = MyController.DateTimeFromString(this.dataRowDepartureParkingStand[0]["ParkingStandDepartureBegin"].ToString());
				this.dateTimePickerDepartureParkingStandBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
				this.dateTimePickerDepartureParkingStandBeginTime.CustomFormat = "HH:mm";

				if(this.dataRowDepartureParkingStand[0]["ParkingStandDepartureBegin"].ToString() == "")
					ClearDepartureParkingStandBeginDateTime();
				
				this.dateTimePickerDepartureParkingStandEndDate.Value = MyController.DateTimeFromString(this.dataRowDepartureParkingStand[0]["ParkingStandDepartureEnd"].ToString());
				this.dateTimePickerDepartureParkingStandEndTime.Value = MyController.DateTimeFromString(this.dataRowDepartureParkingStand[0]["ParkingStandDepartureEnd"].ToString());
				this.dateTimePickerDepartureParkingStandEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
				this.dateTimePickerDepartureParkingStandEndTime.CustomFormat = "HH:mm";
				
				if(this.dataRowDepartureParkingStand[0]["ParkingStandDepartureEnd"].ToString() == "")
					ClearDepartureParkingStandEndDateTime();



				//waiting rooms
				this.comboBoxDepartureWaitingRoomFirst.DataBindings.Add("Text",this.dataRowDepartureWaitingRooms[0]["WaitingRoomsFirstName"].ToString(),"");
				
				for(int i = 0; i < this.dataSetFlightRecords.Tables["AirportWaitingRooms"].Rows.Count; i++)
					this.comboBoxDepartureWaitingRoomFirst.Items.Add(this.dataSetFlightRecords.Tables["AirportWaitingRooms"].Rows[i]["Name"].ToString());

				this.dateTimePickerDepartureWaitingRoomFirstBeginDate.Value = MyController.DateTimeFromString(this.dataRowDepartureWaitingRooms[0]["WaitingRoomsFirstBegin"].ToString());
				this.dateTimePickerDepartureWaitingRoomFirstBeginTime.Value = MyController.DateTimeFromString(this.dataRowDepartureWaitingRooms[0]["WaitingRoomsFirstBegin"].ToString());
				this.dateTimePickerDepartureWaitingRoomFirstBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
				this.dateTimePickerDepartureWaitingRoomFirstBeginTime.CustomFormat = "HH:mm";

				if(this.dataRowDepartureWaitingRooms[0]["WaitingRoomsFirstBegin"].ToString() == "")
					ClearDepartureWaitingRoomFirstBeginDateTime();
				
				this.dateTimePickerDepartureWaitingRoomFirstEndDate.Value = MyController.DateTimeFromString(this.dataRowDepartureWaitingRooms[0]["WaitingRoomsFirstEnd"].ToString());
				this.dateTimePickerDepartureWaitingRoomFirstEndTime.Value = MyController.DateTimeFromString(this.dataRowDepartureWaitingRooms[0]["WaitingRoomsFirstEnd"].ToString());
				this.dateTimePickerDepartureWaitingRoomFirstEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
				this.dateTimePickerDepartureWaitingRoomFirstEndTime.CustomFormat = "HH:mm";

				if(this.dataRowDepartureWaitingRooms[0]["WaitingRoomsFirstEnd"].ToString() == "")
					ClearDepartureWaitingRoomFirstEndDateTime();

				this.comboBoxDepartureWaitingRoomSecond.DataBindings.Add("Text",this.dataRowDepartureWaitingRooms[0]["WaitingRoomsSecondName"].ToString(),"");
				
				for(int i = 0; i < this.dataSetFlightRecords.Tables["AirportWaitingRooms"].Rows.Count; i++)
					this.comboBoxDepartureWaitingRoomSecond.Items.Add(this.dataSetFlightRecords.Tables["AirportWaitingRooms"].Rows[i]["Name"].ToString());

				this.dateTimePickerDepartureWaitingRoomSecondBeginDate.Value = MyController.DateTimeFromString(this.dataRowDepartureWaitingRooms[0]["WaitingRoomsSecondBegin"].ToString());
				this.dateTimePickerDepartureWaitingRoomSecondBeginTime.Value = MyController.DateTimeFromString(this.dataRowDepartureWaitingRooms[0]["WaitingRoomsSecondBegin"].ToString());
				this.dateTimePickerDepartureWaitingRoomSecondBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
				this.dateTimePickerDepartureWaitingRoomSecondBeginTime.CustomFormat = "HH:mm";
	
				if(this.dataRowDepartureWaitingRooms[0]["WaitingRoomsSecondBegin"].ToString() == "")
					ClearDepartureWaitingRoomSecondBeginDateTime();
				
				this.dateTimePickerDepartureWaitingRoomSecondEndDate.Value = MyController.DateTimeFromString(this.dataRowDepartureWaitingRooms[0]["WaitingRoomsSecondEnd"].ToString());
				this.dateTimePickerDepartureWaitingRoomSecondEndTime.Value = MyController.DateTimeFromString(this.dataRowDepartureWaitingRooms[0]["WaitingRoomsSecondEnd"].ToString());
				this.dateTimePickerDepartureWaitingRoomSecondEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
				this.dateTimePickerDepartureWaitingRoomSecondEndTime.CustomFormat = "HH:mm";

				if(this.dataRowDepartureWaitingRooms[0]["WaitingRoomsSecondEnd"].ToString() == "")
					ClearDepartureWaitingRoomSecondEndDateTime();


				//gates
				this.comboBoxDepartureGateFirst.DataBindings.Add("Text",this.dataRowDepartureGates[0]["GatesDepartureFirstName"].ToString(),"");
				
				for(int i = 0; i < this.dataSetFlightRecords.Tables["AirportGates"].Rows.Count; i++)
					this.comboBoxDepartureGateFirst.Items.Add(this.dataSetFlightRecords.Tables["AirportGates"].Rows[i]["Name"].ToString());

				this.dateTimePickerDepartureGateFirstBeginDate.Value = MyController.DateTimeFromString(this.dataRowDepartureGates[0]["GatesDepartureFirstBegin"].ToString());
				this.dateTimePickerDepartureGateFirstBeginTime.Value = MyController.DateTimeFromString(this.dataRowDepartureGates[0]["GatesDepartureFirstBegin"].ToString());
				this.dateTimePickerDepartureGateFirstBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
				this.dateTimePickerDepartureGateFirstBeginTime.CustomFormat = "HH:mm";
	
				if(this.dataRowDepartureGates[0]["GatesDepartureFirstBegin"].ToString() == "")
					ClearDepartureGateFirstBeginDateTime();
				
				this.dateTimePickerDepartureGateFirstEndDate.Value = MyController.DateTimeFromString(this.dataRowDepartureGates[0]["GatesDepartureFirstEnd"].ToString());
				this.dateTimePickerDepartureGateFirstEndTime.Value = MyController.DateTimeFromString(this.dataRowDepartureGates[0]["GatesDepartureFirstEnd"].ToString());
				this.dateTimePickerDepartureGateFirstEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
				this.dateTimePickerDepartureGateFirstEndTime.CustomFormat = "HH:mm";
	
				if(this.dataRowDepartureGates[0]["GatesDepartureFirstEnd"].ToString() == "")
					ClearDepartureGateFirstEndDateTime();

				this.comboBoxDepartureGateSecond.DataBindings.Add("Text",this.dataRowDepartureGates[0]["GatesDepartureSecondName"].ToString(),"");
				
				for(int i = 0; i < this.dataSetFlightRecords.Tables["AirportGates"].Rows.Count; i++)
					this.comboBoxDepartureGateSecond.Items.Add(this.dataSetFlightRecords.Tables["AirportGates"].Rows[i]["Name"].ToString());

				this.dateTimePickerDepartureGateSecondBeginDate.Value = MyController.DateTimeFromString(this.dataRowDepartureGates[0]["GatesArrivalSecondBegin"].ToString());
				this.dateTimePickerDepartureGateSecondBeginTime.Value = MyController.DateTimeFromString(this.dataRowDepartureGates[0]["GatesArrivalSecondBegin"].ToString());
				this.dateTimePickerDepartureGateSecondBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
				this.dateTimePickerDepartureGateSecondBeginTime.CustomFormat = "HH:mm";
	
				if(this.dataRowDepartureGates[0]["GatesDepartureSecondBegin"].ToString() == "")
					ClearDepartureGateSecondBeginDateTime();
				
				this.dateTimePickerDepartureGateSecondEndDate.Value = MyController.DateTimeFromString(this.dataRowDepartureGates[0]["GatesDepartureSecondEnd"].ToString());
				this.dateTimePickerDepartureGateSecondEndTime.Value = MyController.DateTimeFromString(this.dataRowDepartureGates[0]["GatesDepartureSecondEnd"].ToString());
				this.dateTimePickerDepartureGateSecondEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
				this.dateTimePickerDepartureGateSecondEndTime.CustomFormat = "HH:mm";

				if(this.dataRowDepartureGates[0]["GatesDepartureSecondEnd"].ToString() == "")
					ClearDepartureGateSecondEndDateTime();					
			}
		}


		internal void ClearDepartureParkingStandBeginDateTime()
		{
			this.dateTimePickerDepartureParkingStandBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureParkingStandBeginDate.CustomFormat = " ";
			this.dateTimePickerDepartureParkingStandBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureParkingStandBeginTime.CustomFormat = " ";
		}

		internal void ClearDepartureParkingStandEndDateTime()
		{
			this.dateTimePickerDepartureParkingStandEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureParkingStandEndDate.CustomFormat = " ";
			this.dateTimePickerDepartureParkingStandEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureParkingStandEndTime.CustomFormat = " ";
		}

		internal void ClearDepartureWaitingRoomFirstBeginDateTime()
		{
			this.dateTimePickerDepartureWaitingRoomFirstBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureWaitingRoomFirstBeginDate.CustomFormat = " ";
			this.dateTimePickerDepartureWaitingRoomFirstBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureWaitingRoomFirstBeginTime.CustomFormat = " ";
		}

		internal void ClearDepartureWaitingRoomFirstEndDateTime()
		{
			this.dateTimePickerDepartureWaitingRoomFirstEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureWaitingRoomFirstEndDate.CustomFormat = " ";
			this.dateTimePickerDepartureWaitingRoomFirstEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureWaitingRoomFirstEndTime.CustomFormat = " ";
		}

		internal void ClearDepartureWaitingRoomSecondBeginDateTime()
		{
			this.dateTimePickerDepartureWaitingRoomSecondBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureWaitingRoomSecondBeginDate.CustomFormat = " ";
			this.dateTimePickerDepartureWaitingRoomSecondBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureWaitingRoomSecondBeginTime.CustomFormat = " ";
		}

		internal void ClearDepartureWaitingRoomSecondEndDateTime()
		{
			this.dateTimePickerDepartureWaitingRoomSecondEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureWaitingRoomSecondEndDate.CustomFormat = " ";
			this.dateTimePickerDepartureWaitingRoomSecondEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureWaitingRoomSecondEndTime.CustomFormat = " ";
		}

		internal void ClearDepartureGateFirstBeginDateTime()
		{
			this.dateTimePickerDepartureGateFirstBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureGateFirstBeginDate.CustomFormat = " ";
			this.dateTimePickerDepartureGateFirstBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureGateFirstBeginTime.CustomFormat = " ";
		}

		internal void ClearDepartureGateFirstEndDateTime()
		{
			this.dateTimePickerDepartureGateFirstEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureGateFirstEndDate.CustomFormat = " ";
			this.dateTimePickerDepartureGateFirstEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureGateFirstEndTime.CustomFormat = " ";
		}

		internal void ClearDepartureGateSecondBeginDateTime()
		{
			this.dateTimePickerDepartureGateSecondBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureGateSecondBeginDate.CustomFormat = " ";
			this.dateTimePickerDepartureGateSecondBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureGateSecondBeginTime.CustomFormat = " ";
		}

		internal void ClearDepartureGateSecondEndDateTime()
		{
			this.dateTimePickerDepartureGateSecondEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureGateSecondEndDate.CustomFormat = " ";
			this.dateTimePickerDepartureGateSecondEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureGateSecondEndTime.CustomFormat = " ";
		}

		internal void ClearDepartureStandardTime()
		{
			this.dateTimePickerDepartureStandardDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureStandardDate.CustomFormat = " ";
			this.dateTimePickerDepartureStandardTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;		
			this.dateTimePickerDepartureStandardTime.CustomFormat = " ";
		}



		internal void ClearArrivalControls()
		{				
			this.textBoxArrivalFlightNumber.DataBindings.Clear();
			this.textBoxArrivalCallsign.DataBindings.Clear();
			this.comboBoxArrivalOrigin.DataBindings.Clear();
			this.comboBoxArrivalVia.DataBindings.Clear();
			this.comboBoxArrivalAircraftCode.DataBindings.Clear();
			this.comboBoxArrivalServiceType.DataBindings.Clear();
			this.comboBoxArrivalFlightNature.DataBindings.Clear();
			this.comboBoxArrivalFlightIdentification.DataBindings.Clear();	

			this.textBoxArrivalFlightNumber.Clear();
			this.textBoxArrivalCallsign.Clear();
			this.comboBoxArrivalOrigin.ResetText();
			this.comboBoxArrivalVia.ResetText();
			this.comboBoxArrivalAircraftCode.ResetText();
			this.comboBoxArrivalServiceType.ResetText();
			this.comboBoxArrivalFlightNature.ResetText();
			this.comboBoxArrivalFlightIdentification.ResetText();

			ClearArrivalStandardDateTime();

			this.comboBoxArrivalParkingStand.DataBindings.Clear();

			ClearArrivalParkingStandBeginDateTime();
			ClearArrivalParkingStandEndDateTime();
			ClearArrivalBaggageBeltFirstBeginDateTime();
			ClearArrivalBaggageBeltFirstEndDateTime();
			ClearArrivalBaggageBeltSecondBeginDateTime();
			ClearArrivalBaggageBeltSecondEndDateTime();
			ClearArrivalGateFirstBeginDateTime();
			ClearArrivalGateFirstEndDateTime();
			ClearArrivalGateSecondBeginDateTime();
			ClearArrivalGateSecondEndDateTime();

			this.comboBoxArrivalParkingStand.ResetText();
			this.comboBoxArrivalBaggageBeltFirst.ResetText();
			this.comboBoxArrivalBaggageBeltSecond.ResetText();
			this.comboBoxArrivalGateFirst.ResetText();
			this.comboBoxArrivalGateSecond.ResetText();
		}


		internal void ClearArrivalStandardDateTime()
		{
			this.dateTimePickerArrivalStandardDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalStandardDate.CustomFormat = " ";
			this.dateTimePickerArrivalStandardTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;		
			this.dateTimePickerArrivalStandardTime.CustomFormat = " ";
		}
		internal void ClearArrivalParkingStandBeginDateTime()
		{
			this.dateTimePickerArrivalParkingStandBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalParkingStandBeginDate.CustomFormat = " ";
			this.dateTimePickerArrivalParkingStandBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalParkingStandBeginTime.CustomFormat = " ";
		}

		internal void ClearArrivalParkingStandEndDateTime()
		{
			
			this.dateTimePickerArrivalParkingStandEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalParkingStandEndDate.CustomFormat = " ";
			this.dateTimePickerArrivalParkingStandEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalParkingStandEndTime.CustomFormat = " ";
		}

		internal void ClearArrivalBaggageBeltFirstBeginDateTime()
		{
			this.dateTimePickerArrivalBaggageBeltFirstBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalBaggageBeltFirstBeginDate.CustomFormat = " ";
			this.dateTimePickerArrivalBaggageBeltFirstBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalBaggageBeltFirstBeginTime.CustomFormat = " ";
		}

		internal void ClearArrivalBaggageBeltFirstEndDateTime()
		{
			this.dateTimePickerArrivalBaggageBeltFirstEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalBaggageBeltFirstEndDate.CustomFormat = " ";
			this.dateTimePickerArrivalBaggageBeltFirstEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalBaggageBeltFirstEndTime.CustomFormat = " ";
		}

		internal void ClearArrivalBaggageBeltSecondBeginDateTime()
		{
			this.dateTimePickerArrivalBaggageBeltSecondBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalBaggageBeltSecondBeginDate.CustomFormat = " ";
			this.dateTimePickerArrivalBaggageBeltSecondBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalBaggageBeltSecondBeginTime.CustomFormat = " ";
		}

		internal void ClearArrivalBaggageBeltSecondEndDateTime()
		{
			this.dateTimePickerArrivalBaggageBeltSecondEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalBaggageBeltSecondEndDate.CustomFormat = " ";
			this.dateTimePickerArrivalBaggageBeltSecondEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalBaggageBeltSecondEndTime.CustomFormat = " ";
		}

		internal void ClearArrivalGateFirstBeginDateTime()
		{
			this.dateTimePickerArrivalGateFirstBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalGateFirstBeginDate.CustomFormat = " ";
			this.dateTimePickerArrivalGateFirstBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalGateFirstBeginTime.CustomFormat = " ";
		}

		internal void ClearArrivalGateFirstEndDateTime()
		{
			this.dateTimePickerArrivalGateFirstEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalGateFirstEndDate.CustomFormat = " ";
			this.dateTimePickerArrivalGateFirstEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalGateFirstEndTime.CustomFormat = " ";
		}

		internal void ClearArrivalGateSecondBeginDateTime()
		{
			this.dateTimePickerArrivalGateSecondBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalGateSecondBeginDate.CustomFormat = " ";
			this.dateTimePickerArrivalGateSecondBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalGateSecondBeginTime.CustomFormat = " ";
		}

		internal void ClearArrivalGateSecondEndDateTime()
		{
			this.dateTimePickerArrivalGateSecondEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalGateSecondEndDate.CustomFormat = " ";
			this.dateTimePickerArrivalGateSecondEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalGateSecondEndTime.CustomFormat = " ";
		}


		internal void ClearDepartureControls()
		{				
			this.textBoxDepartureFlightNumber.DataBindings.Clear();			
			this.textBoxDepartureCallsign.DataBindings.Clear();		
			this.comboBoxDepartureDestination.DataBindings.Clear();				
			this.comboBoxDepartureVia.DataBindings.Clear();				
			this.comboBoxDepartureAircraftCode.DataBindings.Clear();				
			this.comboBoxDepartureServiceType.DataBindings.Clear();				
			this.comboBoxDepartureFlightNature.DataBindings.Clear();				
			this.comboBoxDepartureFlightIdentification.DataBindings.Clear();		
			
			this.textBoxDepartureFlightNumber.Clear();			
			this.textBoxDepartureCallsign.Clear();		
			this.comboBoxDepartureDestination.ResetText();				
			this.comboBoxDepartureVia.ResetText();				
			this.comboBoxDepartureAircraftCode.ResetText();				
			this.comboBoxDepartureServiceType.ResetText();				
			this.comboBoxDepartureFlightNature.ResetText();				
			this.comboBoxDepartureFlightIdentification.ResetText();	

			ClearDepartureStandardTime();
			
			ClearDepartureParkingStandBeginDateTime();
			ClearDepartureParkingStandEndDateTime();
			ClearDepartureWaitingRoomFirstBeginDateTime();
			ClearDepartureWaitingRoomFirstEndDateTime();
			ClearDepartureWaitingRoomSecondBeginDateTime();
			ClearDepartureWaitingRoomSecondEndDateTime();
			ClearDepartureGateFirstBeginDateTime();
			ClearDepartureGateFirstEndDateTime();
			ClearDepartureGateSecondBeginDateTime();
			ClearDepartureGateSecondEndDateTime();

			this.comboBoxDepartureParkingStand.ResetText();
			this.comboBoxDepartureWaitingRoomFirst.ResetText();
			this.comboBoxDepartureWaitingRoomSecond.ResetText();
			this.comboBoxDepartureGateFirst.ResetText();
			this.comboBoxDepartureGateSecond.ResetText();
		}



		internal void EnableArrivalControls(bool valu)
		{
			this.textBoxArrivalFlightNumber.Enabled = valu;
			this.textBoxArrivalCallsign.Enabled = valu;
			this.comboBoxArrivalOrigin.Enabled = valu;
			this.comboBoxArrivalVia.Enabled = valu;
			this.comboBoxArrivalAircraftCode.Enabled = valu;
			this.comboBoxArrivalServiceType.Enabled = valu;
			this.comboBoxArrivalFlightNature.Enabled = valu;
			this.comboBoxArrivalFlightIdentification.Enabled = valu;
			this.dateTimePickerArrivalStandardDate.Enabled = valu;
			this.dateTimePickerArrivalStandardTime.Enabled = valu;


			this.dateTimePickerArrivalParkingStandBeginDate.Enabled = valu;
			this.dateTimePickerArrivalParkingStandBeginTime.Enabled = valu;
			this.dateTimePickerArrivalParkingStandEndDate.Enabled = valu;
			this.dateTimePickerArrivalParkingStandEndTime.Enabled = valu;

			this.dateTimePickerArrivalBaggageBeltFirstBeginDate.Enabled = valu;
			this.dateTimePickerArrivalBaggageBeltFirstBeginTime.Enabled = valu;
			this.dateTimePickerArrivalBaggageBeltFirstEndDate.Enabled = valu;
			this.dateTimePickerArrivalBaggageBeltFirstEndTime.Enabled = valu;
			this.dateTimePickerArrivalBaggageBeltSecondBeginDate.Enabled = valu;
			this.dateTimePickerArrivalBaggageBeltSecondBeginTime.Enabled = valu;
			this.dateTimePickerArrivalBaggageBeltSecondEndDate.Enabled = valu;
			this.dateTimePickerArrivalBaggageBeltSecondEndTime.Enabled = valu;


			this.dateTimePickerArrivalGateFirstBeginDate.Enabled = valu;
			this.dateTimePickerArrivalGateFirstBeginTime.Enabled = valu;
			this.dateTimePickerArrivalGateFirstEndDate.Enabled = valu;
			this.dateTimePickerArrivalGateFirstEndTime.Enabled = valu;
			this.dateTimePickerArrivalGateSecondBeginDate.Enabled = valu;
			this.dateTimePickerArrivalGateSecondBeginTime.Enabled = valu;
			this.dateTimePickerArrivalGateSecondEndDate.Enabled = valu;
			this.dateTimePickerArrivalGateSecondEndTime.Enabled = valu;

			this.comboBoxArrivalParkingStand.Enabled = valu;
			this.comboBoxArrivalBaggageBeltFirst.Enabled = valu;
			this.comboBoxArrivalBaggageBeltSecond.Enabled = valu;
			this.comboBoxArrivalGateFirst.Enabled = valu;
			this.comboBoxArrivalGateSecond.Enabled = valu;

		}

		internal void EnableDepartureControls(bool valu)
		{
			this.textBoxDepartureFlightNumber.Enabled = valu;
			this.textBoxDepartureCallsign.Enabled = valu;
			this.comboBoxDepartureDestination.Enabled = valu;
			this.comboBoxDepartureVia.Enabled = valu;
			this.comboBoxDepartureAircraftCode.Enabled = valu;
			this.comboBoxDepartureServiceType.Enabled = valu;
			this.comboBoxDepartureFlightNature.Enabled = valu;
			this.comboBoxDepartureFlightIdentification.Enabled = valu;
			this.dateTimePickerDepartureStandardDate.Enabled = valu;
			this.dateTimePickerDepartureStandardTime.Enabled = valu;


			this.dateTimePickerDepartureParkingStandBeginDate.Enabled = valu;
			this.dateTimePickerDepartureParkingStandBeginTime.Enabled = valu;
			this.dateTimePickerDepartureParkingStandEndDate.Enabled = valu;
			this.dateTimePickerDepartureParkingStandEndTime.Enabled = valu;

			this.dateTimePickerDepartureWaitingRoomFirstBeginDate.Enabled = valu;
			this.dateTimePickerDepartureWaitingRoomFirstBeginTime.Enabled = valu;
			this.dateTimePickerDepartureWaitingRoomFirstEndDate.Enabled = valu;
			this.dateTimePickerDepartureWaitingRoomFirstEndTime.Enabled = valu;
			this.dateTimePickerDepartureWaitingRoomSecondBeginDate.Enabled = valu;
			this.dateTimePickerDepartureWaitingRoomSecondBeginTime.Enabled = valu;
			this.dateTimePickerDepartureWaitingRoomSecondEndDate.Enabled = valu;
			this.dateTimePickerDepartureWaitingRoomSecondEndTime.Enabled = valu;


			this.dateTimePickerDepartureGateFirstBeginDate.Enabled = valu;
			this.dateTimePickerDepartureGateFirstBeginTime.Enabled = valu;
			this.dateTimePickerDepartureGateFirstEndDate.Enabled = valu;
			this.dateTimePickerDepartureGateFirstEndTime.Enabled = valu;
			this.dateTimePickerDepartureGateSecondBeginDate.Enabled = valu;
			this.dateTimePickerDepartureGateSecondBeginTime.Enabled = valu;
			this.dateTimePickerDepartureGateSecondEndDate.Enabled = valu;
			this.dateTimePickerDepartureGateSecondEndTime.Enabled = valu;


			this.comboBoxDepartureParkingStand.Enabled = valu;
			this.comboBoxDepartureWaitingRoomFirst.Enabled = valu;
			this.comboBoxDepartureWaitingRoomSecond.Enabled = valu;
			this.comboBoxDepartureGateFirst.Enabled = valu;
			this.comboBoxDepartureGateSecond.Enabled = valu;
		}



		private void RecordDetail_Load(object sender, System.EventArgs e)
		{

			this.dataRowCurrentRotation = MyController.CurrentRotation;
			this.dataSetFlightRecords = MyController.InputDataSet;

			this.dataSetFlightRecords.AcceptChanges();
			this.dataRowCurrentRotation.AcceptChanges();

			MyController.GetCorrespondingFlights(out this.dataRowArrival, out this.dataRowDeparture);
				
			ClearArrivalControls();
			ClearDepartureControls();
			
			if(this.dataRowDeparture.RowState != DataRowState.Detached)
			{
				this.dataRowDepartureParkingStand = this.dataRowDeparture.GetChildRows("FlightRelatedParkingStands");
				this.dataRowDepartureWaitingRooms = this.dataRowDeparture.GetChildRows("FlightRelatedWaitingRooms");
				this.dataRowDepartureGates = this.dataRowDeparture.GetChildRows("FlightRelatedGates");
				this.dataRowDepartureCounters = this.dataRowDeparture.GetChildRows("FlightRelatedCounters");

				FillDepartureControls();
			}

			if(this.dataRowArrival.RowState != DataRowState.Detached)
			{
				this.dataRowArrivalParkingStand = this.dataRowArrival.GetChildRows("FlightRelatedParkingStands");
				this.dataRowArrivalBaggageBelts = this.dataRowArrival.GetChildRows("FlightRelatedBaggageBelts");
				this.dataRowArrivalGates = this.dataRowArrival.GetChildRows("FlightRelatedGates");
				
				FillArrivalControls();
			}
				

			if(this.dataRowArrival.RowState == DataRowState.Detached)
				EnableArrivalControls(false);

			if(this.dataRowDeparture.RowState == DataRowState.Detached)
				EnableDepartureControls(false);

			Delete = false;
			Init = true;
			
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{	
			this.dataSetFlightRecords.RejectChanges();
			this.Close();
		}



		private Controller MyController
		{
			get
			{
				return (Controller)base.Controller;
			}
		}





		private void buttonDelete_Click(object sender, System.EventArgs e)
		{
			Delete = true;
			ClearArrivalControls();
			ClearDepartureControls();
			EnableArrivalControls(false);
			EnableDepartureControls(false);
			MyController.DeleteRotation();
		}




		private void buttonApply_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}


	

		private void dateTimePickerArrivalStandardDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerArrivalStandardDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalStandardTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrival, 
					"StandardTimeArrival", 
					MyController.DateTimeToString(this.dateTimePickerArrivalStandardDate.Value,
					this.dateTimePickerArrivalStandardTime.Value));
		}

		private void dateTimePickerArrivalStandardTime_ValueChanged(object sender, EventArgs e)
		{	
			this.dateTimePickerArrivalStandardTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalStandardTime.CustomFormat = "HH:mm";

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrival, 
					"StandardTimeArrival", 
					MyController.DateTimeToString(this.dateTimePickerArrivalStandardDate.Value,
					this.dateTimePickerArrivalStandardTime.Value));		
		}

		private void dateTimePickerArrivalParkingStandBeginDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerArrivalParkingStandBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalParkingStandBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
	
			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrivalParkingStand[0], 
					"ParkingStandArrivalBegin", 
					MyController.DateTimeToString(this.dateTimePickerArrivalParkingStandBeginDate.Value,
					this.dateTimePickerArrivalParkingStandBeginTime.Value));		
		}

		private void dateTimePickerArrivalParkingStandBeginTime_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerArrivalParkingStandBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalParkingStandBeginTime.CustomFormat = "HH:mm";

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrivalParkingStand[0], 
					"ParkingStandArrivalBegin", 
					MyController.DateTimeToString(this.dateTimePickerArrivalParkingStandBeginDate.Value,
					this.dateTimePickerArrivalParkingStandBeginTime.Value));
		}

		private void dateTimePickerArrivalParkingStandEndDate_ValueChanged(object sender, EventArgs e)
		{ 
			this.dateTimePickerArrivalParkingStandEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalParkingStandEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
		
			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrivalParkingStand[0], 
					"ParkingStandArrivalEnd", 
					MyController.DateTimeToString(this.dateTimePickerArrivalParkingStandEndDate.Value, 
					this.dateTimePickerArrivalParkingStandEndTime.Value));
		}

		private void dateTimePickerArrivalParkingStandEndTime_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerArrivalParkingStandEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalParkingStandEndTime.CustomFormat = "HH:mm";

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrivalParkingStand[0], 
					"ParkingStandArrivalEnd", 
					MyController.DateTimeToString(this.dateTimePickerArrivalParkingStandEndDate.Value, 
					this.dateTimePickerArrivalParkingStandEndTime.Value));
		}

		private void dateTimePickerArrivalBaggageBeltFirstBeginDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerArrivalBaggageBeltFirstBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalBaggageBeltFirstBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
		
			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrivalBaggageBelts[0], 
					"BaggageBeltsFirstBegin", 
					MyController.DateTimeToString(this.dateTimePickerArrivalBaggageBeltFirstBeginDate.Value, 
					this.dateTimePickerArrivalBaggageBeltFirstBeginTime.Value));
		
		}

		private void dateTimePickerArrivalBaggageBeltFirstBeginTime_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerArrivalBaggageBeltFirstBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalBaggageBeltFirstBeginTime.CustomFormat = "HH:mm";

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrivalBaggageBelts[0], 
					"BaggageBeltsFirstBegin", 
					MyController.DateTimeToString(this.dateTimePickerArrivalBaggageBeltFirstBeginDate.Value, 
					this.dateTimePickerArrivalBaggageBeltFirstBeginTime.Value));
		}

		private void dateTimePickerArrivalBaggageBeltFirstEndDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerArrivalBaggageBeltFirstEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalBaggageBeltFirstEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrivalBaggageBelts[0], 
					"BaggageBeltsFirstEnd", 
					MyController.DateTimeToString(this.dateTimePickerArrivalBaggageBeltFirstEndDate.Value, 
					this.dateTimePickerArrivalBaggageBeltFirstEndTime.Value));
		
		}

		private void dateTimePickerArrivalBaggageBeltFirstEndTime_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerArrivalBaggageBeltFirstEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalBaggageBeltFirstEndTime.CustomFormat = "HH:mm";

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrivalBaggageBelts[0], 
					"BaggageBeltsFirstEnd", 
					MyController.DateTimeToString(this.dateTimePickerArrivalBaggageBeltFirstEndDate.Value, 
					this.dateTimePickerArrivalBaggageBeltFirstEndTime.Value));
		}

		private void dateTimePickerArrivalBaggageBeltSecondBeginDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerArrivalBaggageBeltSecondBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalBaggageBeltSecondBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrivalBaggageBelts[0], 
					"BaggageBeltsSecondBegin", 
					MyController.DateTimeToString(this.dateTimePickerArrivalBaggageBeltSecondBeginDate.Value, 
					this.dateTimePickerArrivalBaggageBeltSecondBeginTime.Value));
		}

		private void dateTimePickerArrivalBaggageBeltSecondBeginTime_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerArrivalBaggageBeltSecondBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalBaggageBeltSecondBeginTime.CustomFormat = "HH:mm";

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrivalBaggageBelts[0], 
					"BaggageBeltsSecondBegin", 
					MyController.DateTimeToString(this.dateTimePickerArrivalBaggageBeltSecondBeginDate.Value, 
					this.dateTimePickerArrivalBaggageBeltSecondBeginTime.Value));
		}

		private void dateTimePickerArrivalBaggageBeltSecondEndDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerArrivalBaggageBeltSecondEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalBaggageBeltSecondEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrivalBaggageBelts[0], 
					"BaggageBeltsSecondEnd", 
					MyController.DateTimeToString(this.dateTimePickerArrivalBaggageBeltSecondEndDate.Value, 
					this.dateTimePickerArrivalBaggageBeltSecondEndTime.Value));		
		}

		private void dateTimePickerArrivalBaggageBeltSecondEndTime_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerArrivalBaggageBeltSecondEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalBaggageBeltSecondEndTime.CustomFormat = "HH:mm";

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrivalBaggageBelts[0], 
					"BaggageBeltsSecondEnd", 
					MyController.DateTimeToString(this.dateTimePickerArrivalBaggageBeltSecondEndDate.Value, 
					this.dateTimePickerArrivalBaggageBeltSecondEndTime.Value));
		}

		private void dateTimePickerArrivalGateSecondBeginDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerArrivalGateSecondBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalGateSecondBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrivalGates[0], 
					"GatesArrivalSecondBegin", 
					MyController.DateTimeToString(this.dateTimePickerArrivalGateSecondBeginDate.Value, 
					this.dateTimePickerArrivalGateSecondBeginTime.Value));		
		}

		private void dateTimePickerArrivalGateSecondBeginTime_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerArrivalGateSecondBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalGateSecondBeginTime.CustomFormat = "HH:mm";

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrivalGates[0], 
					"GatesArrivalSecondBegin", 
					MyController.DateTimeToString(this.dateTimePickerArrivalGateSecondBeginDate.Value, 
					this.dateTimePickerArrivalGateSecondBeginTime.Value));
		}

		private void dateTimePickerArrivalGateSecondEndDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerArrivalGateSecondEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalGateSecondEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrivalGates[0], 
					"GatesArrivalSecondEnd", 
					MyController.DateTimeToString(this.dateTimePickerArrivalGateSecondEndDate.Value, 
					this.dateTimePickerArrivalGateSecondEndTime.Value));
		}

		private void dateTimePickerArrivalGateSecondEndTime_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerArrivalGateSecondEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalGateSecondEndTime.CustomFormat = "HH:mm";

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrivalGates[0], 
					"GatesArrivalSecondEnd", 
					MyController.DateTimeToString(this.dateTimePickerArrivalGateSecondEndDate.Value, 
					this.dateTimePickerArrivalGateSecondEndTime.Value));
		}

		private void dateTimePickerArrivalGateFirstEndDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerArrivalGateFirstEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalGateFirstEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrivalGates[0], 
					"GatesArrivalFirstEnd", 
					MyController.DateTimeToString(this.dateTimePickerArrivalGateFirstEndDate.Value, 
					this.dateTimePickerArrivalGateFirstEndTime.Value));
		}
		
		private void dateTimePickerArrivalGateFirstEndTime_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerArrivalGateFirstEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalGateFirstEndTime.CustomFormat = "HH:mm";

			if(Init)
			MyController.SaveValueToCurrentInput(this.dataRowArrivalGates[0], 
				"GatesArrivalFirstEnd", 
				MyController.DateTimeToString(this.dateTimePickerArrivalGateFirstEndDate.Value, 
				this.dateTimePickerArrivalGateFirstEndTime.Value));
		}

		private void dateTimePickerArrivalGateFirstBeginDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerArrivalGateFirstBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerArrivalGateFirstBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
		
			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrivalGates[0], 
					"GatesArrivalFirstBegin", 
					MyController.DateTimeToString(this.dateTimePickerArrivalGateFirstBeginDate.Value, 
					this.dateTimePickerArrivalGateFirstBeginTime.Value));
		}
	
		private void dateTimePickerArrivalGateFirstBeginTime_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerArrivalGateFirstBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerArrivalGateFirstBeginTime.CustomFormat = "HH:mm";

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowArrivalGates[0], 
					"GatesArrivalFirstBegin", 
					MyController.DateTimeToString(this.dateTimePickerArrivalGateFirstBeginDate.Value, 
					this.dateTimePickerArrivalGateFirstBeginTime.Value));
		}
		
	

	

		private void dateTimePickerDepartureGateFirstBeginDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureGateFirstBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureGateFirstBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
		
			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDepartureGates[0], 
					"GatesDepartureFirstBegin", 
					MyController.DateTimeToString(this.dateTimePickerDepartureGateFirstBeginDate.Value, 
					this.dateTimePickerDepartureGateFirstBeginTime.Value));
		
		}

		private void dateTimePickerDepartureGateFirstBeginTime_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureGateFirstBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureGateFirstBeginTime.CustomFormat = "HH:mm";

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDepartureGates[0], 
					"GatesDepartureFirstBegin", 
					MyController.DateTimeToString(this.dateTimePickerDepartureGateFirstBeginDate.Value, 
					this.dateTimePickerDepartureGateFirstBeginTime.Value));
		}

		private void dateTimePickerDepartureGateFirstEndDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureGateFirstEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureGateFirstEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
			
			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDepartureGates[0], 
					"GatesDepartureFirstEnd", 
					MyController.DateTimeToString(this.dateTimePickerDepartureGateFirstEndDate.Value, 
					this.dateTimePickerDepartureGateFirstEndTime.Value));	
		}

		private void dateTimePickerDepartureGateFirstEndTime_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureGateFirstEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureGateFirstEndTime.CustomFormat = "HH:mm";

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDepartureGates[0], 
					"GatesDepartureFirstEnd", 
					MyController.DateTimeToString(this.dateTimePickerDepartureGateFirstEndDate.Value, 
					this.dateTimePickerDepartureGateFirstEndTime.Value));
		}

		private void dateTimePickerDepartureGateSecondBeginDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureGateSecondBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureGateSecondBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
		
			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDepartureGates[0], 
					"GatesDepartureSecondBegin", 
					MyController.DateTimeToString(this.dateTimePickerDepartureGateSecondBeginDate.Value, 
					this.dateTimePickerDepartureGateSecondBeginTime.Value));		
		}

		private void dateTimePickerDepartureGateSecondBeginTime_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureGateSecondBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureGateSecondBeginTime.CustomFormat = "HH:mm";

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDepartureGates[0], 
					"GatesDepartureSecondBegin", 
					MyController.DateTimeToString(this.dateTimePickerDepartureGateSecondBeginDate.Value, 
					this.dateTimePickerDepartureGateSecondBeginTime.Value));
		}

		private void dateTimePickerDepartureGateSecondEndDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureGateSecondEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureGateSecondEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
		
			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDepartureGates[0], 
					"GatesDepartureSecondEnd", 
					MyController.DateTimeToString(this.dateTimePickerDepartureGateSecondEndDate.Value, 
					this.dateTimePickerDepartureGateSecondEndTime.Value));	
		}

		private void dateTimePickerDepartureGateSecondEndTime_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureGateSecondEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureGateSecondEndTime.CustomFormat = "HH:mm";

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDepartureGates[0], 
					"GatesDepartureSecondEnd", 
					MyController.DateTimeToString(this.dateTimePickerDepartureGateSecondEndDate.Value, 
					this.dateTimePickerDepartureGateSecondEndTime.Value));
		}

		private void dateTimePickerDepartureParkingStandBeginDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureParkingStandBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureParkingStandBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
		
			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDepartureParkingStand[0], 
					"ParkingStandDepartureBegin", 
					MyController.DateTimeToString(this.dateTimePickerDepartureParkingStandBeginDate.Value, 
					this.dateTimePickerDepartureParkingStandBeginTime.Value));		
		}

		private void dateTimePickerDepartureParkingStandBeginTime_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureParkingStandBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureParkingStandBeginTime.CustomFormat = "HH:mm";

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDepartureParkingStand[0], 
					"ParkingStandDepartureBegin", 
					MyController.DateTimeToString(this.dateTimePickerDepartureParkingStandBeginDate.Value, 
					this.dateTimePickerDepartureParkingStandBeginTime.Value));
		}

		private void dateTimePickerDepartureParkingStandEndDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureParkingStandEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureParkingStandEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDepartureParkingStand[0], 
					"ParkingStandDepartureEnd", 
					MyController.DateTimeToString(this.dateTimePickerDepartureParkingStandEndDate.Value, 
					this.dateTimePickerDepartureParkingStandEndTime.Value));
		}

		private void dateTimePickerDepartureParkingStandEndTime_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureParkingStandEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureParkingStandEndTime.CustomFormat = "HH:mm";

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDepartureParkingStand[0], 
					"ParkingStandDepartureEnd", 
					MyController.DateTimeToString(this.dateTimePickerDepartureParkingStandEndDate.Value, 
					this.dateTimePickerDepartureParkingStandEndTime.Value));
		}

		private void dateTimePickerDepartureWaitingRoomFirstBeginDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureWaitingRoomFirstBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureWaitingRoomFirstBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
		
			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDepartureWaitingRooms[0], 
					"WaitingRoomsFirstBegin", 
					MyController.DateTimeToString(this.dateTimePickerDepartureWaitingRoomFirstBeginDate.Value, 
					this.dateTimePickerDepartureWaitingRoomFirstBeginTime.Value));
		}

		private void dateTimePickerDepartureWaitingRoomFirstBeginTime_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureWaitingRoomFirstBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureWaitingRoomFirstBeginTime.CustomFormat = "HH:mm";

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDepartureWaitingRooms[0], 
					"WaitingRoomsFirstBegin", 
					MyController.DateTimeToString(this.dateTimePickerDepartureWaitingRoomFirstBeginDate.Value, 
					this.dateTimePickerDepartureWaitingRoomFirstBeginTime.Value));
		}

		private void dateTimePickerDepartureWaitingRoomFirstEndDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureWaitingRoomFirstEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureWaitingRoomFirstEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
	
			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDepartureWaitingRooms[0], 
					"WaitingRoomsFirstEnd", 
					MyController.DateTimeToString(this.dateTimePickerDepartureWaitingRoomFirstEndDate.Value, 
					this.dateTimePickerDepartureWaitingRoomFirstEndTime.Value));
		}

		private void dateTimePickerDepartureWaitingRoomFirstEndTime_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureWaitingRoomFirstEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureWaitingRoomFirstEndTime.CustomFormat = "HH:mm";

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDepartureWaitingRooms[0], 
					"WaitingRoomsFirstEnd", 
					MyController.DateTimeToString(this.dateTimePickerDepartureWaitingRoomFirstEndDate.Value, 
					this.dateTimePickerDepartureWaitingRoomFirstEndTime.Value));
		}

		private void dateTimePickerDepartureWaitingRoomSecondBeginDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureWaitingRoomSecondBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureWaitingRoomSecondBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
		
			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDepartureWaitingRooms[0], 
					"WaitingRoomsSecondBegin", 
					MyController.DateTimeToString(this.dateTimePickerDepartureWaitingRoomSecondBeginDate.Value, 
					this.dateTimePickerDepartureWaitingRoomSecondBeginTime.Value));
		}

		private void dateTimePickerDepartureWaitingRoomSecondBeginTime_ValueChanged(object sender, EventArgs e)
		{

			this.dateTimePickerDepartureWaitingRoomSecondBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureWaitingRoomSecondBeginTime.CustomFormat = "HH:mm";

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDepartureWaitingRooms[0], 
					"WaitingRoomsSecondBegin", 
					MyController.DateTimeToString(this.dateTimePickerDepartureWaitingRoomSecondBeginDate.Value, 
					this.dateTimePickerDepartureWaitingRoomSecondBeginTime.Value));

		}

		private void dateTimePickerDepartureWaitingRoomSecondEndDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureWaitingRoomSecondEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureWaitingRoomSecondEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
		
			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDepartureWaitingRooms[0], 
					"WaitingRoomsSecondEnd", 
					MyController.DateTimeToString(this.dateTimePickerDepartureWaitingRoomSecondEndDate.Value, 
					this.dateTimePickerDepartureWaitingRoomSecondEndTime.Value));

		}

		private void dateTimePickerDepartureWaitingRoomSecondEndTime_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureWaitingRoomSecondEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureWaitingRoomSecondEndTime.CustomFormat = "HH:mm";

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDepartureWaitingRooms[0], 
					"WaitingRoomsSecondEnd", 
					MyController.DateTimeToString(this.dateTimePickerDepartureWaitingRoomSecondEndDate.Value, 
					this.dateTimePickerDepartureWaitingRoomSecondEndTime.Value));

		}

		private void dateTimePickerDepartureStandardDate_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureStandardDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerDepartureStandardTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;

			if(Init)
				MyController.SaveValueToCurrentInput(this.dataRowDeparture, 
				"StandardTimeDeparture", 
				MyController.DateTimeToString(this.dateTimePickerDepartureStandardDate.Value, 
				this.dateTimePickerDepartureStandardTime.Value));

		}

		private void dateTimePickerDepartureStandardTime_ValueChanged(object sender, EventArgs e)
		{
			this.dateTimePickerDepartureStandardTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerDepartureStandardTime.CustomFormat = "HH:mm";

			if(Init)
			MyController.SaveValueToCurrentInput(this.dataRowDeparture, 
				"StandardTimeDeparture", 
				MyController.DateTimeToString(this.dateTimePickerDepartureStandardDate.Value, 
				this.dateTimePickerDepartureStandardTime.Value));

		}










		private void textBoxArrivalFlightNumber_TextChanged(object sender, EventArgs e)
		{
			if (this.textBoxArrivalFlightNumber.Text.Length > 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowArrival,
					"FlightNumber",
					this.textBoxArrivalFlightNumber.Text);
			}
		}

		private void comboBoxDepartureFlightIdentification_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxDepartureFlightIdentification.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowDeparture,
					"FlightIdentification",
					this.comboBoxDepartureFlightIdentification.Text);
			}
		}

		private void comboBoxDepartureServiceType_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxDepartureServiceType.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowDeparture,
					"ServiceType",
					this.comboBoxDepartureServiceType.Text);
			}
		}

		private void comboBoxDepartureFlightNature_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxDepartureFlightNature.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowDeparture,
					"FlightNature",
					this.comboBoxDepartureFlightNature.Text);
			}
		}

		private void comboBoxDepartureAircraftCode_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxDepartureAircraftCode.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowDeparture,
					"AcCode",
					this.comboBoxDepartureAircraftCode.Text);
			}
		}

		private void comboBoxDepartureDestination_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxDepartureDestination.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowDeparture,
					"Destination",
					this.comboBoxDepartureDestination.Text);
			}
		}

		private void comboBoxDepartureVia_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxDepartureVia.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowDeparture,
					"Via", 
					this.comboBoxDepartureVia.Text);
			}
		}

		private void textBoxDepartureFlightNumber_TextChanged(object sender, EventArgs e)
		{
			if (this.textBoxDepartureFlightNumber.Text.Length > 0 && this.textBoxDepartureFlightNumber.Text.Length < 10)
			{
				MyController.SaveValueToCurrentInput(this.dataRowDeparture, 
					"FlightNumber", 
					this.textBoxDepartureFlightNumber.Text);
			}
		}

		private void textBoxDepartureCallsign_TextChanged(object sender, EventArgs e)
		{
			if (this.textBoxDepartureCallsign.Text.Length > 0 && this.textBoxDepartureCallsign.Text.Length < 10)
			{
				MyController.SaveValueToCurrentInput(this.dataRowDeparture, 
					"CallSign", 
					this.textBoxDepartureCallsign.Text);
			}
		}

		private void comboBoxArrivalOrigin_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxArrivalOrigin.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowArrival, 
					"Origin", 
					this.comboBoxArrivalOrigin.Text);
			}
		}

		private void comboBoxArrivalFlightIdentification_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxArrivalFlightIdentification.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowArrival, 
					"FlightIdentification", 
					this.comboBoxArrivalFlightIdentification.Text);
			}
		}

		private void comboBoxArrivalServiceType_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxArrivalServiceType.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowArrival, 
					"ServiceType", 
					this.comboBoxArrivalServiceType.Text);
			}
		}

		private void comboBoxArrivalFlightNature_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxArrivalFlightNature.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowArrival, 
					"FlightNature", 
					this.comboBoxArrivalFlightNature.Text);
			}
		}

		private void comboBoxArrivalAircraftCode_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxArrivalAircraftCode.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowArrival, 
					"AcCode", 
					this.comboBoxArrivalAircraftCode.Text);
			}
		}

		private void comboBoxArrivalVia_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxArrivalVia.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowArrival, 
					"Via", 
					this.comboBoxArrivalVia.Text);
			}
		}

		private void textBoxArrivalCallsign_TextChanged(object sender, EventArgs e)
		{
			if (this.textBoxArrivalCallsign.Text.Length > 0 && this.textBoxArrivalCallsign.Text.Length < 10)
			{
				MyController.SaveValueToCurrentInput(this.dataRowArrival, 
					"CallSign", 
					this.textBoxArrivalCallsign.Text);
			}
		}

		private void comboBoxArrivalParkingStand_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxArrivalParkingStand.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowArrivalParkingStand[0], 
					"ParkingStandArrivalName", 
					this.comboBoxArrivalParkingStand.Text);
			}
		}

		private void comboBoxArrivalBaggageBeltFirst_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxArrivalBaggageBeltFirst.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowArrivalBaggageBelts[0], 
					"BaggageBeltsFirstName", 
					this.comboBoxArrivalBaggageBeltFirst.Text);
			}
		}

		private void comboBoxArrivalBaggageBeltSecond_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxArrivalBaggageBeltSecond.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowArrivalBaggageBelts[0], 
					"BaggageBeltsSecondName", 
					this.comboBoxArrivalBaggageBeltSecond.Text);
			}
		}

		private void comboBoxArrivalGateFirst_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxArrivalGateFirst.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowArrivalGates[0], 
					"GatesArrivalFirstName", 
					this.comboBoxArrivalGateFirst.Text);
			}
		}

		private void comboBoxArrivalGateSecond_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxArrivalGateSecond.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowArrivalGates[0], 
					"GatesArrivalSecondName", 
					this.comboBoxArrivalGateSecond.Text);
			}
		}

		private void comboBoxDepartureParkingStand_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxDepartureParkingStand.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowDepartureParkingStand[0], 
					"ParkingStandDepartureName", 
					this.comboBoxDepartureParkingStand.Text);
			}
		}

		private void comboBoxDepartureWaitingRoomFirst_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxDepartureWaitingRoomFirst.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowDepartureWaitingRooms[0], 
					"WaitingRoomsFirstName", 
					this.comboBoxDepartureWaitingRoomFirst.Text);
			}
		}

		private void comboBoxDepartureWaitingRoomSecond_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxDepartureWaitingRoomSecond.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowDepartureWaitingRooms[0], 
					"WaitingRoomsSecondName", 
					this.comboBoxDepartureWaitingRoomSecond.Text);
			}
		}
		private void comboBoxDepartureGateFirst_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxDepartureGateFirst.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowDepartureGates[0], 
					"GatesDepartureFirstName", 
					this.comboBoxDepartureGateFirst.Text);
			}
		}

		private void comboBoxDepartureGateSecond_SelectedValueChanged(object sender, EventArgs e)
		{
			if (this.comboBoxDepartureGateSecond.SelectedIndex >= 0)
			{
				MyController.SaveValueToCurrentInput(this.dataRowDepartureGates[0], 
					"GatesDepartureSecondName", 
					this.comboBoxDepartureGateSecond.Text);
			}
		}


	}
}

